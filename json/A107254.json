{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A107254",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 107254,
			"data": "1,1,12,8640,870912000,22122558259200000,222531556847250309120000000,1280394777025250130271722799104000000000,5746332926632566442385615219551212618645504000000000000",
			"name": "a(n) = SF(2n-1)/SF(n-1)^2 where SF = A000178.",
			"comment": [
				"Inverse product of all matrix elements of n X n Hilbert Matrix M(i,j) = 1/(i+j-1) (i,j = 1..n). - _Alexander Adamchuk_, Apr 12 2006"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A107254/b107254.txt\"\u003eTable of n, a(n) for n = 0..20\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/HilbertMatrix.html\"\u003eHilbert Matrix\u003c/a\u003e."
			],
			"formula": [
				"a(n) = n!*(n+1)!*(n+2)!*...*(2n-1)!/(0!*1!*2!*3!*...*(n-1)!) = A000178(2n-1)/A000178(n-1)^2 = A079478(n)/A000984(n) = A079478(n-1)*A009445(n-1) = A107252(n)*A000142(n) = A088020(n)/A039622(n).",
				"a(n) = 1/Product_{j=1..n} ( Product_{i=1..n} 1/(i+j-1) ). - _Alexander Adamchuk_, Apr 12 2006",
				"a(n) = 2^(n*(n-1)) * A136411(n) for n \u003e 0 . - _Robert Coquereaux_, Apr 06 2013",
				"a(n) = A136411(n) * A053763(n) for n \u003e 0. [Following remark from _Robert Coquereaux_] - _M. F. Hasler_, Apr 06 2013",
				"a(n) ~ A * 2^(2*n^2-1/12) * n^(n^2+1/12) / exp(3*n^2/2+1/12), where A = 1.28242712910062263687534256886979... is the Glaisher-Kinkelin constant (see A074962). - _Vaclav Kotesovec_, Feb 10 2015",
				"a(n) = Product_{k=1..n} rf(k,n) where rf denotes the rising factorial. - _Peter Luschny_, Nov 29 2015",
				"a(n) = (n! * G(2*n+1))/(G(n+1)*G(n+2)), where G(n) is the Barnes G - function. - _G. C. Greubel_, Apr 21 2021"
			],
			"example": [
				"a(3) = 1!*2!*3!*4!*5!/(1!*2!*1!*2!) = 34560/4 = 8640.",
				"n = 2: HilbertMatrix[n,n]",
				"  1/1 1/2",
				"  1/2 1/3",
				"so a(2) = 1 / (1 * 1/2 * 1/2 * 1/3) = 12.",
				"The n X n Hilbert matrix begins:",
				"  1/1 1/2 1/3 1/4  1/5  1/6  1/7  1/8 ...",
				"  1/2 1/3 1/4 1/5  1/6  1/7  1/8  1/9 ...",
				"  1/3 1/4 1/5 1/6  1/7  1/8  1/9 1/10 ...",
				"  1/4 1/5 1/6 1/7  1/8  1/9 1/10 1/11 ...",
				"  1/5 1/6 1/7 1/8  1/9 1/10 1/11 1/12 ...",
				"  1/6 1/7 1/8 1/9 1/10 1/11 1/12 1/13 ..."
			],
			"maple": [
				"a:= n-\u003e mul((n+i)!/i!, i=0..n-1):",
				"seq(a(n), n=0..10);  # _Alois P. Heinz_, Jul 23 2012"
			],
			"mathematica": [
				"Table[Product[(i+j-1),{i,1,n},{j,1,n}], {n,1,10}] - _Alexander Adamchuk_, Apr 12 2006",
				"Table[n!*BarnesG[2n+1]/(BarnesG[n+2]*BarnesG[n+1]), {n,0,12}] (* _G. C. Greubel_, Apr 21 2021 *)"
			],
			"program": [
				"(Sage)",
				"a = lambda n: prod(rising_factorial(k,n) for k in (1..n))",
				"print([a(n) for n in (0..10)]) # _Peter Luschny_, Nov 29 2015",
				"(MAGMA)",
				"A107254:= func\u003c n | n eq 0 select 1 else (\u0026*[Factorial(n+j)/Factorial(j): j in [0..n-1]]) \u003e;",
				"[A107254(n): n in [0..12]]; // _G. C. Greubel_, Apr 21 2021"
			],
			"xref": [
				"Cf. A000178, A002457, A005249, A098118."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Henry Bottomley_, May 14 2005",
			"references": 5,
			"revision": 32,
			"time": "2021-04-22T04:49:16-04:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}