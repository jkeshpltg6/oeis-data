{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A273866",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 273866,
			"data": "1,1,1,1,1,1,1,1,2,2,1,1,2,2,2,1,1,3,5,5,3,1,1,3,6,7,6,3,1,1,4,9,13,13,9,4,1,1,4,10,17,20,17,10,4,1,1,5,15,30,42,42,30,15,5,1,1,5,16,36,57,66,57,36,16,5,1",
			"name": "Coefficients a(k,m) of polynomials a{k}(h) appearing in the product Product_{k \u003e= 1} (1 - a{k}(h)*x^k) = 1 - h*x/(1-x).",
			"comment": [
				"The a(k,m) form a table where each row has k-1 elements starting from 2 and the a(1,1) = 1."
			],
			"link": [
				"Giedrius Alkauskas, \u003ca href=\"http://arxiv.org/abs/0801.0805\"\u003eOne curious proof of Fermat's little theorem\u003c/a\u003e, arXiv:0801.0805 [math.NT], 2008.",
				"Giedrius Alkauskas, \u003ca href=\"https://www.jstor.org/stable/40391097\"\u003eA curious proof of Fermat's little theorem\u003c/a\u003e, Amer. Math. Monthly 116(4) (2009), 362-364.",
				"H. Gingold, H. W. Gould, and Michael E. Mays, \u003ca href=\"https://www.researchgate.net/publication/268023169_Power_product_expansions\"\u003ePower Product Expansions\u003c/a\u003e, Utilitas Mathematica 34 (1988), 143-161.",
				"H. Gingold and A. Knopfmacher, \u003ca href=\"http://dx.doi.org/10.4153/CJM-1995-062-9\"\u003eAnalytic properties of power product expansions\u003c/a\u003e, Canad. J. Math. 47 (1995), 1219-1239.",
				"W. Lang, \u003ca href=\"/A157162/a157162.txt\"\u003eRecurrences for the general problem\u003c/a\u003e."
			],
			"formula": [
				"a(k,m) = a(k, k-m).",
				"For prime p: Sum_{m = 1..p-1} a(p, m) = (2^p - 2)/p.",
				"a{k}(h) satisfies Sum_{d|k} (1/d)*(a{k/d}(h))^d = ((h+1)^k - 1)/k. [Corrected by _Petros Hadjicostas_, Oct 04 2019]",
				"For prime p: a{p}(h) = ((h+1)^p - h^p - 1)/p.",
				"See A273873 for the definition of strict tree. Then a(n,m) = Sum_t (-1)^{v(t)-1} where the sum is over all strict trees of weight n with m leaves, and v(t) is the number of nodes in t (including the leaves, which are positive integers). See example 2 and the first Mathematica program. - _Gus Wiseman_, Nov 14 2016"
			],
			"example": [
				"a{1}(h) = h,",
				"a{2}(h) = h,",
				"a{3}(h) = h^2 + h,",
				"a{4}(h) = h^3 + h^2 + h,",
				"a{5}(h) = h^4 + 2*h^3 + 2*h^2 + h,",
				"a{6}(h) = h^5 + 2*h^4 + 2*h^3 + 2*h^2 + h,",
				"a{7}(h) = h^6 + 3*h^5 + 5*h^4 + 5*h^3 + 3*h^2 + h,",
				"a{8}(h) = h^7 + 3*h^6 + 6*h^5 + 7*h^4 + 6*h^3 + 3*h^2 + h,",
				"a{9}(h) = h^8 + 4*h^7 + 9*h^6 + 13*h^5 + 13*h^4 + 9*h^3 + 4*h^2 + h",
				"...",
				"and the corresponding a(k,m) table is:",
				"  1,",
				"  1,",
				"  1,  1,",
				"  1,  1,  1,",
				"  1,  2,  2,  1,",
				"  1,  2,  2,  2,  1,",
				"  1,  3,  5,  5,  3,  1,",
				"  1,  3,  6,  7,  6,  3,  1,",
				"  1,  4,  9, 13, 13,  9,  4,  1,",
				"  ...",
				"a(7,3) = 5 because there are six strict trees contributing positive one {{5,1},1}, {{4,2},1}, {{4,1},2}, {{3,2},2}, {4,{2,1}}, {{3,1},3} and there is one strict tree contributing negative one {4,2,1}. - _Gus Wiseman_, Nov 14 2016"
			],
			"maple": [
				"with(ListTools), with(numtheory), with(combinat);",
				"L := product(1-a[k]*x^k, k = 1 .. 600);",
				"S := Flatten([seq(-h, i = 1 .. 100)]);",
				"Sabs := Flatten([seq(i, i = 1 .. 100)]);",
				"seq(assign(a[i] = solve(coeff(L, x^i) = `if`(is(i in Sabs), S[Search(i, Sabs)], 0), a[i])), i = 1 .. 20);",
				"map(coeffs, [seq(simplify(a[i]), i = 1 .. 20)]);"
			],
			"mathematica": [
				"strictrees[n_Integer?Positive]:=Prepend[Join@@Function[ptn,Tuples[strictrees/@ptn]]/@Select[IntegerPartitions[n],And[Length[#]\u003e1,UnsameQ@@#]\u0026],n];",
				"Table[Sum[(-1)^(Count[tree,_,{0,Infinity}]-1),{tree,Select[strictrees[n],Length[Flatten[{#}]]===m\u0026]}],{n,1,9},{m,1,n-1/.(0-\u003e1)}] (* _Gus Wiseman_, Nov 14 2016 *)",
				"(* second program *)",
				"A[m_, n_] :=",
				"  A[m, n] =",
				"   Which[m == 1, -h, m \u003e n \u003e= 1, 0, True,",
				"    A[m - 1, n] - A[m - 1, m - 1]*A[m, n - m + 1]];",
				"a[n_] := Expand[-A[n, n]];",
				"a /@ Range[1, 25] (* _Petros Hadjicostas_, Oct 04 2019, courtesy of _Jean-François Alcover_ *)"
			],
			"xref": [
				"Cf. A196545, A220418, A220420, A273866, A273873, A279785, A290261, A290262, A290971, A295632."
			],
			"keyword": "nonn,tabf",
			"offset": "1,9",
			"author": "_Gevorg Hmayakyan_, Jun 01 2016",
			"references": 16,
			"revision": 62,
			"time": "2019-10-06T02:37:54-04:00",
			"created": "2016-06-09T04:44:21-04:00"
		}
	]
}