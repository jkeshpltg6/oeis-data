{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A350686",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 350686,
			"data": "12,17,19,20,26,31,211,716,1226,1436,2306,2731,2971,5636,8011,12146,12721,16921,18266,19441,24481,24691,25796,28316,30026,34651,35876,37171,45986,49681,51691,56036,58676,61561,67531,77276,98731,98996,104161,104756,108571",
			"name": "Numbers k such that tau(k) + tau(k+1) + tau(k+2) + tau(k+3) = 16, where tau is the number of divisors function A000005.",
			"comment": [
				"It can be shown that if tau(k) + tau(k+1) + tau(k+2) + tau(k+3) = 16, the quadruple (tau(k), tau(k+1), tau(k+2), tau(k+3)) must be one of the following, each of which might plausibly occur infinitely often:",
				"  (2, 4, 4, 6), which first occurs at k = 12721, 16921, 19441, 24481, ... (A163573);",
				"  (2, 6, 4, 4), which first occurs at k = 19, 31, 211, 2731, ...;",
				"  (4, 4, 6, 2), which first occurs at k = 26, 1226, 2306, 12146, ...;",
				"  (6, 4, 4, 2), which first occurs at k = 20, 716, 1436, 5636, ...; ({A247347(n)-3}, other than its first term)",
				"or one of the following, each of which occurs only once:",
				"  (2, 6, 2, 6), which occurs only at k = 17; and",
				"  (6, 2, 4, 4), which occurs only at k = 12.",
				"Tau(k) + tau(k+1) + tau(k+2) + tau(k+3) \u003e= 16 for all sufficiently large k; the only numbers k for which tau(k) + tau(k+1) + tau(k+2) + tau(k+3) \u003c 16 are 1..11, 13, 14, and 16."
			],
			"link": [
				"Jon E. Schoenfield, \u003ca href=\"/A350686/b350686.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"{ k : tau(k) + tau(k+1) + tau(k+2) + tau(k+3) = 16 }."
			],
			"example": [
				"The table below lists each term k with a pattern (tau(k), ..., tau(k+3)) that appears only once (these appear at n = 1 and 2), as well as each term k that is the smallest one having a pattern that appears repeatedly for large k (these are at n = 3, 4, 5, and 17). It also includes k = a(9) = 1226, which is the smallest k having its pattern that also has each of k, ..., k+3 divisible by a prime \u003e 4. (k = a(5) = 26 is a special case in that, while it and k = 1226 share the same pattern of tau values, i.e., (4, 4, 6, 2), their prime signatures differ at k+1: both 26+1=27 and 1226+1=1227 have 4 divisors, but 27 is a cube.)",
				"Each of the repeatedly occurring patterns corresponds to one of the four possible orders in which the multipliers m=1..4 can appear among 4 consecutive integers of the form m*prime; e.g., k=19 begins a run of 4 consecutive integers having the form (p, 4*q, 3*r, 2*s), where p, q, r, and s are distinct primes \u003e 4.",
				"For each of the patterns of tau values that does not occur repeatedly, and also for the special case k = 26, one or more of the four consecutive integers in k..k+3 has no prime factor \u003e 4; each such integer appears in parentheses in the columns on the right.",
				".",
				"                                factorization as",
				"                # divisors of     m*(prime \u003e 4)",
				"   n  a(n)=k    k  k+1 k+2 k+3    k  k+1 k+2 k+3",
				"   -  ------   --- --- --- ---   --- --- --- ---",
				"   1      12    6   2   4   4    (12)  q  2r  3s",
				"   2      17    2   6   2   6      p (18)  r  4s",
				"   3      19    2   6   4   4      p  4q  3r  2s",
				"   4      20    6   4   4   2     4p  3q  2r   s",
				"   5      26    4   4   6   2     2p (27) 4r   s",
				"   9    1226    4   4   6   2     2p  3q  4r   s",
				"  17   12721    2   4   4   6      p  2q  3r  4s"
			],
			"mathematica": [
				"Position[Plus @@@ Partition[Array[DivisorSigma[0, #] \u0026 , 10^5], 4, 1], 16] // Flatten (* _Amiram Eldar_, Jan 12 2022 *)"
			],
			"program": [
				"(PARI) isok(k) = numdiv(k) + numdiv(k+1) + numdiv(k+2) + numdiv(k+3) == 16; \\\\ _Michel Marcus_, Jan 12 2022",
				"(Python) from labmath import divcount",
				"for k in range (1,110000):",
				"    if divcount(k) + divcount(k+1) + divcount(k+2) + divcount(k+3) == 16:",
				"        print (k, end=\", \") # _Karl-Heinz Hofmann_, Jan 12 2022"
			],
			"xref": [
				"Cf. A000005, A163573, A247347.",
				"Numbers k such that Sum_{j=0..N-1} tau(k+j) = 2*Sum_{k=1..N} tau(k): A000040 (N=1), A350593 (N=2), A350675 (N=3), (this sequence) (N=4), A350699 (N=5), A350769 (N=6)."
			],
			"keyword": "nonn,new",
			"offset": "1,1",
			"author": "_Jon E. Schoenfield_, Jan 11 2022",
			"references": 0,
			"revision": 28,
			"time": "2022-01-16T17:47:21-05:00",
			"created": "2022-01-14T23:20:39-05:00"
		}
	]
}