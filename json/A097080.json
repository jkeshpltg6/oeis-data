{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A097080",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 97080,
			"data": "3,7,15,27,43,63,87,115,147,183,223,267,315,367,423,483,547,615,687,763,843,927,1015,1107,1203,1303,1407,1515,1627,1743,1863,1987,2115,2247,2383,2523,2667,2815,2967,3123,3283,3447,3615,3787,3963,4143,4327,4515,4707",
			"name": "a(n) = 2*n^2 - 2*n + 3.",
			"comment": [
				"The rational numbers may be totally ordered, first by height (see A002246) and then by magnitude; every rational number of height n appears in this ordering at a position \u003c= a(n).",
				"This ordering of the rationals is given in A113136/A113137.",
				"The old entry with this sequence number was a duplicate of A027356.",
				"This is also the sum of the pairwise averages of five consecutive triangular numbers in A000217. Start with A000217(0): (0+1)/2 + (1+3)/2 + (3+6)/2 + (6+10)/2 = 15, which is the third term of this sequence. Simply continue to create this sequence. - _J. M. Bergot_, Jun 13 2012",
				"2*a(n) is inverse radius (curvature) of the touching circle of the large semicircle (radius 1) and the n-th and (n-1)-st circles of the Pappus chain of the symmetric Arbelos. One can use Descartes three circle theorem to find the solution 4*n^2 - 4*n + 6, n \u003e= 1. Note that the circle with curvature 6 is also the third circle of the clockwise Pappus chain, which is A059100(2) (by symmetry). See the illustration. - _Wolfdieter Lang_ and _Kival Ngaokrajang_, Jul 01 2015",
				"Numbers k such that 2*k - 5 is a square. - _Bruno Berselli_, Nov 08 2017"
			],
			"reference": [
				"Steven Edwards and W. Griffiths, Generalizations of Delannoy and cross polytope numbers, Fib. Q., 55 (2017), 356-366.",
				"M. N. Huxley, Area, Lattice Points and Exponential Sums, Oxford, 1996; p. 7."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A097080/b097080.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Steven Edwards and William Griffiths, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL23/Griffiths/griffiths51.html\"\u003eOn Generalized Delannoy Numbers\u003c/a\u003e, J. Int. Seq., Vol. 23 (2020), Article 20.3.6.",
				"Kival Ngaokrajang, \u003ca href=\"/A097080/a097080.pdf\"\u003eIllustration of the Pappus chain of the symmetric Arbelos\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-3,1)."
			],
			"formula": [
				"a(n) = 4*(n-1) + a(n-1) for n\u003e1, a(1)=3. - _Vincenzo Librandi_, Nov 16 2010",
				"a(n) = A046092(n) + 3. - _Reinhard Zumkeller_, Dec 15 2013",
				"G.f.: x*(3 - 2*x + 3*x^2)/(1 - x)^3. - _Vincenzo Librandi_, Aug 03 2014",
				"a(n) = A027575(n-2)/2. - _Michel Marcus_, Nov 11 2015"
			],
			"mathematica": [
				"Table[2n^2-2n+3,{n,50}] (* or *) LinearRecurrence[{3,-3,1},{3,7,15},50] (* _Harvey P. Dale_, Aug 02 2014 *)",
				"CoefficientList[Series[(3 - 2 x + 3 x^2)/(1 - x)^3, {x, 0, 50}], x] (* _Vincenzo Librandi_, Aug 03 2014 *)"
			],
			"program": [
				"(PARI) a(n)=2*n^2-2*n+3 \\\\ _Charles R Greathouse IV_, Jun 13 2012",
				"(PARI) Vec(x*(3-2*x+3*x^2)/(1-x)^3 + O(x^50)) \\\\ _Altug Alkan_, Nov 11 2015",
				"(Haskell)",
				"a097080 n = 2 * n * (n - 1) + 3  -- _Reinhard Zumkeller_, Dec 15 2013"
			],
			"xref": [
				"Cf. A001845, A002246, A059100, A113136, A113137."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_, Nov 02 2008",
			"references": 22,
			"revision": 72,
			"time": "2020-07-16T02:37:08-04:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}