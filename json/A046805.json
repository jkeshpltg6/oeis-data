{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A046805",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 46805,
			"data": "1,1,1,2,1,2,1,2,3,2,1,3,2,2,3,4,2,3,2,4,3,2,2,4,5,2,3,4,3,5,3,4,3,3,5,6,3,3,3,5,4,6,3,4,5,4,3,6,7,5,4,4,4,6,5,7,4,4,4,6,5,4,7,8,5,6,5,4,4,7,5,8,5,5,5,5,7,6,5,8,9,5,5,7,6,5,5,8,5,9,7,6,5,5,5,8,6,7,9,10,5,6,6,8",
			"name": "If n=sum a_i b_i, (a_i,b_i positive integers) then a(n)=max value of min(all a_i and b_i).",
			"comment": [
				"From _Robert Israel_, Aug 29 2018: (Start)",
				"a(n) \u003c= sqrt(n), with equality if n is a square.",
				"a(n) \u003e= A033676(n).",
				"a(m+n) \u003e= min(a(m), a(n)). (End)"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A046805/b046805.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"a(13)=2 since 13=2*2+3*3."
			],
			"maple": [
				"A046805 := proc(n)",
				"    local p,a,abmin,divmin;",
				"    a := 0 ;",
				"    for p in combinat[partition](n) do",
				"        abmin := 1+n ;",
				"        for abprod in p do",
				"            divmin := A033676(abprod) ;",
				"            abmin := min(abmin,divmin) ;",
				"        end do:",
				"        a := max(a,abmin) ;",
				"    end do:",
				"    a ;",
				"end proc: # _R. J. Mathar_, Oct 12 2015",
				"# alternative program:",
				"f:= proc(n) option remember; local v,a,b,vmax;",
				"  if issqr(n) then return sqrt(n) fi;",
				"  vmax:= 1;",
				"  for a from floor(sqrt(n)) by -1 while a \u003e vmax do",
				"    for b from a to n/a do",
				"      v:= min(a, procname(n - a*b));",
				"      vmax:= max(vmax,v);",
				"  od od;",
				"  vmax",
				"end proc:",
				"f(0):= infinity:",
				"map(f, [$1..200]); # _Robert Israel_, Aug 29 2018"
			],
			"mathematica": [
				"f[n_] := f[n] = Module[{v, a, b, vMax}, If[IntegerQ[Sqrt[n]], Return[ Sqrt[n]]]; vMax = 1; For[a = Floor[Sqrt[n]], a \u003e vMax, a--, For[b = a, b \u003c= n/a, b++, v = Min[a, f[n - a b]]; vMax = Max[vMax, v]]]; vMax];",
				"f[0] = Infinity;",
				"Array[f, 200] (* _Jean-François Alcover_, Jun 23 2020, after _Robert Israel_ *)"
			],
			"xref": [
				"Cf. A033676."
			],
			"keyword": "easy,nice,nonn,look",
			"offset": "1,4",
			"author": "_Erich Friedman_",
			"references": 2,
			"revision": 17,
			"time": "2020-06-24T04:58:47-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}