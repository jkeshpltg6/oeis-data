{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A346632",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 346632,
			"data": "1,0,0,0,1,0,0,1,2,0,0,1,2,0,0,0,1,2,3,0,0,0,1,2,6,6,0,0,0,1,2,9,12,0,0,0,0,1,2,12,18,10,0,0,0,0,1,2,15,24,30,20,0,0,0,0,1,2,18,30,60,60,0,0,0,0,0,1,2,21,36,100,120,35,0,0,0,0",
			"name": "Triangle read by rows giving the main diagonals of the matrices counting integer compositions by length and alternating sum (A345197).",
			"comment": [
				"The matrices (A345197) count the integer compositions of n of length k with alternating sum i, where 1 \u003c= k \u003c= n, and i ranges from -n + 2 to n in steps of 2. The alternating sum of a sequence (y_1,...,y_k) is Sum_i (-1)^(i-1) y_i."
			],
			"example": [
				"Triangle begins:",
				"   1",
				"   0   0",
				"   0   1   0",
				"   0   1   2   0",
				"   0   1   2   0   0",
				"   0   1   2   3   0   0",
				"   0   1   2   6   6   0   0",
				"   0   1   2   9  12   0   0   0",
				"   0   1   2  12  18  10   0   0   0",
				"   0   1   2  15  24  30  20   0   0   0",
				"   0   1   2  18  30  60  60   0   0   0   0",
				"   0   1   2  21  36 100 120  35   0   0   0   0",
				"   0   1   2  24  42 150 200 140  70   0   0   0   0",
				"   0   1   2  27  48 210 300 350 280   0   0   0   0   0",
				"   0   1   2  30  54 280 420 700 700 126   0   0   0   0   0"
			],
			"mathematica": [
				"ats[y_]:=Sum[(-1)^(i-1)*y[[i]],{i,Length[y]}];",
				"Table[Table[Length[Select[Join@@Permutations/@IntegerPartitions[n,{k}],k==(n+ats[#])/2\u0026]],{k,n}],{n,0,15}]"
			],
			"xref": [
				"The first nonzero element in each column appears to be A001405.",
				"These are the diagonals of the matrices given by A345197.",
				"Anti-diagonals of the same matrices are A345907.",
				"Row sums are A345908.",
				"A011782 counts compositions.",
				"A097805 counts compositions by alternating (or reverse-alternating) sum.",
				"A103919 counts partitions by sum and alternating sum (reverse: A344612).",
				"A316524 gives the alternating sum of prime indices (reverse: A344616).",
				"Other diagonals are A008277 of A318393 and A055884 of A320808.",
				"Compositions of n, 2n, or 2n+1 with alternating/reverse-alternating sum k:",
				"- k = 0:  counted by A088218, ranked by A344619/A344619.",
				"- k = 1:  counted by A000984, ranked by A345909/A345911.",
				"- k = -1: counted by A001791, ranked by A345910/A345912.",
				"- k = 2:  counted by A088218, ranked by A345925/A345922.",
				"- k = -2: counted by A002054, ranked by A345924/A345923.",
				"- k \u003e= 0: counted by A116406, ranked by A345913/A345914.",
				"- k \u003c= 0: counted by A058622(n-1), ranked by A345915/A345916.",
				"- k \u003e 0:  counted by A027306, ranked by A345917/A345918.",
				"- k \u003c 0:  counted by A294175, ranked by A345919/A345920.",
				"- k != 0: counted by A058622, ranked by A345921/A345921.",
				"- k even: counted by A081294, ranked by A053754/A053754.",
				"- k odd:  counted by A000302, ranked by A053738/A053738.",
				"Cf. A000070, A000346, A007318, A008549, A025047, A163493, A344610."
			],
			"keyword": "nonn,tabl",
			"offset": "0,9",
			"author": "_Gus Wiseman_, Jul 26 2021",
			"references": 2,
			"revision": 12,
			"time": "2021-08-11T05:48:40-04:00",
			"created": "2021-07-27T21:18:38-04:00"
		}
	]
}