{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007495",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7495,
			"id": "M0237",
			"data": "1,1,2,2,2,4,5,4,8,8,7,11,8,13,4,11,12,8,12,2,13,7,22,2,8,13,26,4,26,29,17,27,26,7,33,20,16,22,29,4,13,22,25,14,22,37,18,46,42,46,9,41,12,7,26,42,24,5,44,53,52,58,29,22,12,48,27,30,58,52,49,57,13,14,32,24,75,8,67",
			"name": "Josephus problem: survivors.",
			"comment": [
				"If, in a circle of k persons, every n-th person is removed, the survivor is t(k,n) + 1. So the recurrence generates a sequence of survivors. See the formula. For more details see the \"Proof of the formula\". - _Gerhard Kirchner_, Oct 23 2016",
				"The recurrence formula looks like a simple congruential generator for pseudo-random numbers. Is a(n) pseudo-random? It seems so, see: \"Stochastic aspects\". I used the formula for extending a(n) up to n=2^20. - _Gerhard Kirchner_, Nov 10 2016"
			],
			"reference": [
				"Friend H. Kierstead, Jr., Computer Challenge Corner, J. Rec. Math., 10 (1977), see p. 124.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe and Seiichi Manyama, \u003ca href=\"/A007495/b007495.txt\"\u003eTable of n, a(n) for n = 1..5000\u003c/a\u003e (first 1000 terms from T. D. Noe)",
				"Gerhard Kirchner, \u003ca href=\"/A007495/a007495_2.txt\"\u003eProof of the formula\u003c/a\u003e.",
				"Gerhard Kirchner, \u003ca href=\"/A007495/a007495.pdf\"\u003eStochastic aspects\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/JosephusProblem.html\"\u003e Josephus Problem\u003c/a\u003e.",
				"Robert G. Wilson v, \u003ca href=\"/A007495/a007495_1.pdf\"\u003eNotes, n.d.\u003c/a\u003e.",
				"\u003ca href=\"/index/J#Josephus\"\u003eIndex entries for sequences related to the Josephus Problem\u003c/a\u003e"
			],
			"formula": [
				"Let t(k,n) = (t(k-1,n) + n) mod k and t(1,n) = 0; then a(n) = t(n,n) + 1. - _Gerhard Kirchner_, Oct 23 2016"
			],
			"example": [
				"From _Gerhard Kirchner_, Oct 23 2016: (Start)",
				"If n = 4 we have that:",
				"t(1,4) = 0.",
				"t(2,4) = (0+4) mod 2 = 0.",
				"t(3,4) = (0+4) mod 3 = 1.",
				"t(4,4) = (1+4) mod 4 = 1.",
				"So a(4) = 1 + 1 = 2. (End)"
			],
			"mathematica": [
				"(* First do *) Needs[\"Combinatorica`\"] (* then *) f[n_] := Last@ InversePermutation@ Josephus[n, n]; Array[f, 80] (* _Robert G. Wilson v_, Jul 31 2010 *)",
				"t[k_, n_] := t[k, n] = Mod[t[k-1, n]+n, k]; t[1, _] = 0; a[n_] := t[n, n]+1; Array[a, 1000] (* _Jean-François Alcover_, Oct 23 2016, after _Gerhard Kirchner_ *)"
			],
			"xref": [
				"Cf. A032434, A054995."
			],
			"keyword": "easy,nonn",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_, _Robert G. Wilson v_",
			"ext": [
				"More terms from _Robert G. Wilson v_, Jul 31 2010"
			],
			"references": 10,
			"revision": 63,
			"time": "2020-06-23T08:58:58-04:00",
			"created": "1994-09-19T03:00:00-04:00"
		}
	]
}