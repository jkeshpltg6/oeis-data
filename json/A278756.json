{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A278756",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 278756,
			"data": "1,0,6,1,28,3,120,7,496,15,2016,31,8128,63,32640,127,130816,255,523776,511,2096128,1023,8386560,2047,33550336,4095,134209536,8191,536854528,16383,2147450880,32767,8589869056,65535,34359607296,131071,137438691328,262143",
			"name": "Decimal representation of the x-axis, from the origin to the right edge, of the n-th stage of growth of the two-dimensional cellular automaton defined by \"Rule 65\", based on the 5-celled von Neumann neighborhood.",
			"comment": [
				"Initialized with a single black (ON) cell at stage zero."
			],
			"reference": [
				"S. Wolfram, A New Kind of Science, Wolfram Media, 2002; p. 170."
			],
			"link": [
				"Robert Price, \u003ca href=\"/A278756/b278756.txt\"\u003eTable of n, a(n) for n = 0..126\u003c/a\u003e",
				"Robert Price, \u003ca href=\"/A278756/a278756.tmp.txt\"\u003eDiagrams of first 20 stages\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"http://arxiv.org/abs/1503.01168\"\u003eOn the Number of ON Cells in Cellular Automata\u003c/a\u003e, arXiv:1503.01168 [math.CO], 2015",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ElementaryCellularAutomaton.html\"\u003eElementary Cellular Automaton\u003c/a\u003e",
				"S. Wolfram, \u003ca href=\"http://wolframscience.com/\"\u003eA New Kind of Science\u003c/a\u003e",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_2D_5-Neighbor_Cellular_Automata\"\u003eIndex to 2D 5-Neighbor Cellular Automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_Elementary_Cellular_Automata\"\u003eIndex to Elementary Cellular Automata\u003c/a\u003e",
				"Robert Price, \u003ca href=\"/A278756/a278756.tmp.txt\"\u003eDiagrams of first 20 stages\u003c/a\u003e"
			],
			"formula": [
				"Conjectures from _Chai Wah Wu_, Jun 15 2020: (Start)",
				"a(n) = 7*a(n-2) - 14*a(n-4) + 8*a(n-6) for n \u003e 5.",
				"G.f.: (4*x^5 - x^3 + x^2 - 1)/(8*x^6 - 14*x^4 + 7*x^2 - 1). (End)"
			],
			"mathematica": [
				"CAStep[rule_,a_]:=Map[rule[[10-#]]\u0026,ListConvolve[{{0,2,0},{2,1,2},{0,2,0}},a,2],{2}];",
				"code=65; stages=128;",
				"rule=IntegerDigits[code,2,10];",
				"g=2*stages+1; (* Maximum size of grid *)",
				"a=PadLeft[{{1}},{g,g},0,Floor[{g,g}/2]]; (* Initial ON cell on grid *)",
				"ca=a;",
				"ca=Table[ca=CAStep[rule,ca],{n,1,stages+1}];",
				"PrependTo[ca,a];",
				"(* Trim full grid to reflect growth by one cell at each stage *)",
				"k=(Length[ca[[1]]]+1)/2;",
				"ca=Table[Table[Part[ca[[n]][[j]],Range[k+1-n,k-1+n]],{j,k+1-n,k-1+n}],{n,1,k}];",
				"Table[FromDigits[Part[ca[[i]][[i]],Range[i,2*i-1]],2], {i,1,stages-1}]"
			],
			"xref": [
				"Cf. A278753, A278754, A278755."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Robert Price_, Nov 27 2016",
			"references": 4,
			"revision": 11,
			"time": "2020-06-15T10:09:01-04:00",
			"created": "2016-11-27T22:00:35-05:00"
		}
	]
}