{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A086193",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 86193,
			"data": "1,0,1,18,1699,592260,754179301,3562635108438,63770601591579079,4405870283636411477640,1190873924687350003735546441,1270602397076493907445608866890778,5381240610642043789096251476993474339179",
			"name": "Number of n X n matrices with entries in {0,1} with no zero row, no zero column and with zero main diagonal.",
			"comment": [
				"Also the number of simple labeled digraphs on n nodes for which every vertex has indegree at least one and outdegree at least one.",
				"Also the number of edge covers on the n-crown graph. - _Eric W. Weisstein_, May 19 2017"
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A086193/b086193.txt\"\u003eTable of n, a(n) for n = 0..50\u003c/a\u003e",
				"R. W. Robinson. \u003ca href=\"http://cobweb.cs.uga.edu/~rwr/publications/components.ps\"\u003eCounting digraphs with restrictions on the strong components\u003c/a\u003e, (1996). W(n).",
				"R. W. Robinson, \u003ca href=\"/A086193/a086193.pdf\"\u003eCounting digraphs with restrictions on the strong components\u003c/a\u003e, 1996 [Local copy, with permission]",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CrownGraph.html\"\u003eCrown Graph\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/EdgeCover.html\"\u003eEdge Cover\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Sum_{r=0..n} (-1)^(n-r)*binomial(n, r)*(2^(r-1)-1)^r*(2^r-1)^(n-r). - _Vladeta Jovovic_, Aug 27 2003",
				"a(n) = sum( f(n, r), r=0..n ) where f(n, r) = binomial(n, r) (-1)^r (1-2^(-n+r+1))^(n-r) (1-2^(-n+r))^r 2^((n-r)(n-1)). - _Brendan McKay_, Aug 27 2003",
				"E.g.f.: Sum_{k\u003e=0} (2^(n-1)-1)^n*exp((1-2^n)*x)*x^n/n!. - _Vladeta Jovovic_, Feb 23 2008",
				"a(n) ~ 2^(n*(n-1)). - _Vaclav Kotesovec_, May 04 2015"
			],
			"mathematica": [
				"Table[ it = (Partition[ #1, n ] \u0026) /@ IntegerDigits[ Range[ 0, -1 + 2^n^2 ], 2, n^2 ]; Count[ it, (q_)?MatrixQ /; Tr[ q ] === 0 \u0026\u0026 (Times @@ (Plus @@@ q)) \u003e 0 \u0026\u0026 (Times @@ (Plus @@@ Transpose[ q ]) \u003e 0) ], {n, 1, 4} ] (* _Wouter Meeussen_, Aug 25 2003 *)",
				"Table[Sum[(-1)^(n-r)*Binomial[n, r]*(2^(r-1)-1)^r*(2^r-1)^(n-r), {r,0,n}],{n,1,15}] (* _Vaclav Kotesovec_, May 04 2015 after _Vladeta Jovovic_ *)"
			],
			"program": [
				"(PARI) a(n)={sum(r=0, n, (-1)^(n-r)*binomial(n, r)*(2^(r-1)-1)^r*(2^r-1)^(n-r))} \\\\ _Andrew Howroyd_, Sep 09 2018"
			],
			"xref": [
				"Cf. A048291."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_W. Edwin Clark_, Aug 25 2003",
			"ext": [
				"More terms from _Brendan McKay_, Aug 27 2003",
				"a(0)=1 prepended by _Andrew Howroyd_, Sep 09 2018"
			],
			"references": 5,
			"revision": 36,
			"time": "2020-11-25T09:24:40-05:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}