{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A129062",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 129062,
			"data": "1,0,1,0,2,1,0,6,6,1,0,26,36,12,1,0,150,250,120,20,1,0,1082,2040,1230,300,30,1,0,9366,19334,13650,4270,630,42,1,0,94586,209580,166376,62160,11900,1176,56,1,0,1091670,2562354,2229444,952728,220500,28476,2016,72,1",
			"name": "T(n, k) = [x^k] Sum_{k=0..n} Stirling2(n, k)*RisingFactorial(x, k), triangle read by rows, for n \u003e= 0 and 0 \u003c= k \u003c= n.",
			"comment": [
				"Matrix product of Stirling2 with unsigned Stirling1 triangle.",
				"For the subtriangle without column nr. m=0 and row nr. n=0 see A079641.",
				"The reversed matrix product |S1|. S2 is given in A111596.",
				"As a product of lower triangular Jabotinsky matrices this is a lower triangular Jabotinsky matrix. See the D. E. Knuth references given in A039692 for Jabotinsky type matrices.",
				"E.g.f. for row polynomials P(n,x):=sum(a(n,m)*x^m,m=0..n) is 1/(2-exp(z))^x. See the e.g.f. for the columns given below.",
				"A048993*A132393 as infinite lower triangular matrices. - _Philippe Deléham_, Nov 01 2009",
				"Triangle T(n,k), read by rows, given by (0,2,1,4,2,6,3,8,4,10,5,...) DELTA (1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, Nov 19 2011.",
				"Also the Bell transform of A000629. For the definition of the Bell transform see A264428. - _Peter Luschny_, Jan 27 2016"
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A129062/b129062.txt\"\u003eTable of n, a(n) for n = 0..11475\u003c/a\u003e (rows 0 \u003c= n \u003c= 150, flattened).",
				"Olivier Bodini, Antoine Genitrini, Cécile Mailler, Mehdi Naima, \u003ca href=\"https://hal.archives-ouvertes.fr/hal-02865198\"\u003eStrict monotonic trees arising from evolutionary processes: combinatorial and probabilistic study\u003c/a\u003e, hal-02865198 [math.CO] / [math.PR] / [cs.DS] / [cs.DM], 2020.",
				"Marin Knežević, Vedran Krčadinac, and Lucija Relić, \u003ca href=\"https://arxiv.org/abs/2012.15307\"\u003eMatrix products of binomial coefficients and unsigned Stirling numbers\u003c/a\u003e, arXiv:2012.15307 [math.CO], 2020.",
				"W. Lang, \u003ca href=\"/A129062/a129062.txt\"\u003eFirst ten rows and more.\u003c/a\u003e"
			],
			"formula": [
				"a(n,m) = sum(S2(n,k)*|S1(k,m)|, k=m..n), n\u003e=0; S2=A048993, S1=A048994.",
				"E.g.f. column nr. m (with leading zeros): (f(x)^m)/m! with f(x):= -log(1-(exp(x)-1)) = -log(2-exp(x)).",
				"Sum_{0\u003c=k\u003c=n} T(n,k)*x^k = A153881(n+1), A000007(n), A000670(n), A005649(n) for x = -1,0,1,2 respectively. - _Philippe Deléham_, Nov 19 2011"
			],
			"example": [
				"Triangle begins:",
				"1;",
				"0,    1;",
				"0,    2,    1;",
				"0,    6,    6,    1;",
				"0,   26,   36,   12,   1;",
				"0,  150,  250,  120,  20,  1;",
				"0, 1082, 2040, 1230, 300, 30,  1;"
			],
			"maple": [
				"# The function BellMatrix is defined in A264428.",
				"BellMatrix(n -\u003e polylog(-n,1/2), 9); # _Peter Luschny_, Jan 27 2016"
			],
			"mathematica": [
				"rows = 9;",
				"t = Table[PolyLog[-n, 1/2], {n, 0, rows}]; T[n_, k_] := BellY[n, k, t];",
				"Table[T[n, k], {n, 0, rows}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Jun 22 2018, after _Peter Luschny_ *)",
				"p[n_] := Sum[StirlingS2[n, k] Pochhammer[x, k], {k, 0, n}];",
				"Table[CoefficientList[FunctionExpand[p[n]], x], {n, 0, 9}] // Flatten (* _Peter Luschny_, Jun 27 2019 *)"
			],
			"program": [
				"(Sage)",
				"def a_row(n):",
				"    s = sum(stirling_number2(n,k)*rising_factorial(x,k) for k in (0..n))",
				"    return expand(s).list()",
				"[a_row(n) for n in (0..9)] # _Peter Luschny_, Jun 28 2019"
			],
			"xref": [
				"Cf. A000629, A000670, A005649, A079641, A325872, A325873."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "0,5",
			"author": "_Wolfdieter Lang_, May 04 2007",
			"ext": [
				"New name by _Peter Luschny_, Jun 27 2019"
			],
			"references": 10,
			"revision": 53,
			"time": "2021-04-17T01:39:19-04:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}