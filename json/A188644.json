{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A188644",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 188644,
			"data": "1,1,1,1,7,1,1,97,17,1,1,1351,577,31,1,1,18817,19601,1921,49,1,1,262087,665857,119071,4801,71,1,1,3650401,22619537,7380481,470449,10081,97,1,1,50843527,768398401,457470751,46099201,1431431,18817,127,1",
			"name": "Array of (k^n + k^(-n))/2 where k = (sqrt(x^2-1) + x)^2 for integers x \u003e= 1.",
			"comment": [
				"Conjecture: Given the function f(x,y) = (sqrt(x^2+y) + x)^2 and constant k=f(x,y), then for all integers x \u003e= 1 and y=[+-]1, k may be irrational, but (k^n + k^(-n))/2 always produces integer sequences; y=-1 results shown here; y=1 results are A188645.",
				"Also square array A(n,k), n \u003e= 1, k \u003e= 0, read by antidiagonals, where A(n,k) is Chebyshev polynomial of the first kind T_{2*k}(x), evaluated at x=n. - _Seiichi Manyama_, Dec 30 2018"
			],
			"link": [
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Chebyshev_polynomials\"\u003eChebyshev polynomials\u003c/a\u003e.",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e"
			],
			"formula": [
				"A(n,k) = (A188646(n,k-1) + A188646(n,k))/2.",
				"A(n,k) = Sum_{j=0..k} binomial(2*k,2*j)*(n^2-1)^(k-j)*n^(2*j). - _Seiichi Manyama_, Jan 01 2019"
			],
			"example": [
				"Row 2 gives {( (2+sqrt(3))^(2*n) + (2-sqrt(3))^(2*n) )/2}.",
				"Square array begins:",
				"     | 0    1       2          3             4",
				"-----+---------------------------------------------",
				"   1 | 1,   1,      1,         1,            1, ...",
				"   2 | 1,   7,     97,      1351,        18817, ...",
				"   3 | 1,  17,    577,     19601,       665857, ...",
				"   4 | 1,  31,   1921,    119071,      7380481, ...",
				"   5 | 1,  49,   4801,    470449,     46099201, ...",
				"   6 | 1,  71,  10081,   1431431,    203253121, ...",
				"   7 | 1,  97,  18817,   3650401,    708158977, ...",
				"   8 | 1, 127,  32257,   8193151,   2081028097, ...",
				"   9 | 1, 161,  51841,  16692641,   5374978561, ...",
				"  10 | 1, 199,  79201,  31521799,  12545596801, ...",
				"  11 | 1, 241, 116161,  55989361,  26986755841, ...",
				"  12 | 1, 287, 164737,  94558751,  54276558337, ...",
				"  13 | 1, 337, 227137, 153090001, 103182433537, ...",
				"  14 | 1, 391, 305761, 239104711, 186979578241, ...",
				"  15 | 1, 449, 403201, 362074049, 325142092801, ...",
				"  ..."
			],
			"mathematica": [
				"max = 9; y = -1; t = Table[k = ((x^2 + y)^(1/2) + x)^2; ((k^n) + (k^(-n)))/2 // FullSimplify, {n, 0, max - 1}, {x, 1, max}]; Table[ t[[n - k + 1, k]], {n, 1, max}, {k, 1, n}] // Flatten (* _Jean-François Alcover_, Jul 17 2013 *)"
			],
			"xref": [
				"Row 2 is A011943, row 3 is A056771, row 8 is A175633, (row 2)*2 is A067902, (row 9)*2 is A089775.",
				"Column 0-5 give A000012, A056220, A144130, A243132, A243134, A243136.",
				"(column 1)*2 is A060626.",
				"Cf. A188645 (f(x, y) as above with y=1).",
				"Diagonals give A173129, A322899.",
				"Cf. A188646, A322836."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Charles L. Hohn_, Apr 06 2011",
			"ext": [
				"Edited by _Seiichi Manyama_, Dec 30 2018",
				"More terms from _Seiichi Manyama_, Jan 01 2019"
			],
			"references": 10,
			"revision": 65,
			"time": "2019-01-01T12:01:14-05:00",
			"created": "2011-04-16T10:08:04-04:00"
		}
	]
}