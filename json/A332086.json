{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A332086",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 332086,
			"data": "1,1,1,1,1,2,2,1,2,2,2,3,3,2,3,3,4,4,4,4,3,4,4,6,5,5,4,4,4,4,6,6,6,6,7,6,7,8,7,7,6,6,8,7,8,7,8,10,9,9,10,9,9,8,9,10,9,8,8,8,7,9,10,10,9,10,11,11,11,11,11,11,12,12,12,12,13,13,13,13",
			"name": "a(n) = pi(prime(n) + n) - n, where pi is the prime counting function.",
			"comment": [
				"This sequence is related to a theorem of Lu and Deng (see LINKS): “The prime gap of a prime number is less than or equal to the prime count of the prime number”, which is equivalent to “There exists at least one prime number between p and p+pi(p)+1”, or pi(p+pi(p)) - pi(p) \u003e 1, where pi is prime counting function. The n-th term of the sequence, a(n), is the number of prime number between the n-th prime number p_n and p_n + pi(p_n) + 1. According to the theorem, a(n) \u003e= 1."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A332086/b332086.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Ya-Ping Lu and Shu-Fang Deng, \u003ca href=\"https://arxiv.org/abs/2007.15282\"\u003eAn upper bound for the prime gap\u003c/a\u003e, arXiv:2007.15282 [math.GM], 2020."
			],
			"formula": [
				"a(n) = pi(prime(n) + n) - n.",
				"a(n) = A000720(A014688(n)) - n. - _Michel Marcus_, Aug 23 2020"
			],
			"example": [
				"a(1) = pi(p_1 + 1) - 1 = pi(2 + 1) - 1 = 2 - 1 = 1;",
				"a(2) = pi(p_2 + 2) - 2 = pi(3 + 2) - 2 = 3 - 2 = 1;",
				"a(6) = pi(p_6 + 6) - 6 = pi(13 + 6) - 6 = 8 - 6 = 2;",
				"a(80) = pi(p_80 + 80) - 80 = pi(409 + 80) - 80 = 93 - 80 = 13."
			],
			"maple": [
				"f:= n -\u003e numtheory:-pi(ithprime(n)+n)-n:",
				"map(f, [$1..100]); # _Robert Israel_, Sep 08 2020"
			],
			"mathematica": [
				"a[n_] := PrimePi[Prime[n] + n] - n; Array[a, 100] (* _Amiram Eldar_, Aug 23 2020 *)"
			],
			"program": [
				"(Python)",
				"from sympy import prime, primepi",
				"for n in range(1, 1001):",
				"    a = primepi(prime(n) + n) - n",
				"    print(a)",
				"(PARI) a(n) = primepi(prime(n) + n) - n; \\\\ _Michel Marcus_, Aug 23 2020"
			],
			"xref": [
				"Cf. A000720 (pi), A014688 (prime(n)+n)."
			],
			"keyword": "nonn",
			"offset": "1,6",
			"author": "_Ya-Ping Lu_, Aug 22 2020",
			"ext": [
				"Name edited by _Michel Marcus_, Sep 02 2020"
			],
			"references": 6,
			"revision": 27,
			"time": "2020-09-11T11:24:40-04:00",
			"created": "2020-08-31T04:31:42-04:00"
		}
	]
}