{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A166189",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 166189,
			"data": "1,7,29,90,232,524,1072,2030,3613,6111,9905,15484,23464,34608,49848,70308,97329,132495,177661,234982,306944,396396,506584,641186,804349,1000727,1235521,1514520,1844144,2231488,2684368,3211368",
			"name": "Number of 3 X 3 X 3 triangular nonnegative integer arrays with all sums of an element and its neighbors \u003c= n.",
			"comment": [
				"a(n) gives the number of hexagons that have vertices at the lattice points and sides on lattice lines of a triangular lattice with sides n+3. Note that the hexagons can be non-regular. This problem appeared as ConvexHexagons in Single Round Match 455 in TopCoder. - _Dmitry Kamenetsky_, Dec 17 2009"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A166189/b166189.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Jun-Ming Zhu, \u003ca href=\"http://arxiv.org/abs/1012.4058\"\u003eThe number of convex pentagons and hexagons in an n-triangular net\u003c/a\u003e, arXiv:1012.4058 [math.CO], 2010; See H(n), formula 3, on page 4.",
				"\u003ca href=\"/index/Rec#order_08\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (6,-14,14,0,-14,14,-6,1)."
			],
			"formula": [
				"From _G. C. Greubel_, Jul 02 2021: (Start)",
				"a(n) = (1/1920)*(4*n^6 +72*n^5 +530*n^4 +2040*n^3 +4296*n^2 +4608*n +1905 +15*(-1)^n).",
				"a(2*n+1) = (1/15)*binomial(n+2, 2)*binomial(n+3, 2)*(8*n^2 + 32*n + 35).",
				"a(2*n) = (1/30)*binomial(n+2, 2)*(8*n^4 + 48*n^3 + 105*n^2 + 99*n + 30).",
				"G.f.: (1 - x^3)/((1-x^2)*(1-x)^7).",
				"E.g.f.: (1/1920)*((1905 +2*x*(5775 +7665*x +3690*x^2 +755*x^3 +66*x^4 +2*x^5))*exp(x) + 15*exp(-x)). (End)",
				"a(n) = A001779(n)-A001779(n-3). - _R. J. Mathar_, Jul 04 2021"
			],
			"mathematica": [
				"LinearRecurrence[{6,-14,14,0,-14,14,-6,1}, {1,7,29,90,232,524,1072,2030}, 51] (* _G. C. Greubel_, Jul 02 2021 *)"
			],
			"program": [
				"(MAGMA)",
				"R\u003cx\u003e:=PowerSeriesRing(Integers(), 50);",
				"Coefficients(R!( (1-x^3)/((1-x^2)*(1-x)^7) )); // _G. C. Greubel_, Jul 02 2021",
				"(Sage)",
				"def a(n): return (n+2)*(n+4)*(2*n^4 +24*n^3 +105*n^2 +198*n +120)/960 if (n%2==0) else (n+1)*(n+3)^2*(n+5)*(2*n*(n+6) +21)/960",
				"[a(n) for n in (0..50)] # _G. C. Greubel_, Jul 02 2021",
				"(PARI) \\\\ using Zhu expressions",
				"f(k) = (8*k^6 + 24*k^5 + 25*k^4 + 10*k^3 - 3*k^2 -4*k)/60;",
				"g(k) = (8*k^6 - 5*k^4 - 3*k^2)/60;",
				"a(n) = n+=3; if (n%2, f((n-1)/2), g(n/2)); \\\\ _Michel Marcus_, Jul 04 2021"
			],
			"xref": [
				"Cf. A176646, A011888 (first differences)."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_R. H. Hardin_, Oct 09 2009",
			"references": 2,
			"revision": 24,
			"time": "2021-07-04T06:44:24-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}