{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A059024",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 59024,
			"data": "1,1,1,1,1,1,126,1,462,1,1254,1,3003,1,6721,1,14443,126126,1,30251,1009008,1,62322,5309304,1,127024,23075052,1,257108,89791416,1,518092,325355316,488864376,1,1041029,1122632043,6844101264,1,2088043",
			"name": "Triangle of Stirling numbers of order 5.",
			"comment": [
				"The number of partitions of the set N, |N|=n, into k blocks, all of cardinality greater than or equal to 5. This is the 5-associated Stirling number of the second kind.",
				"This is entered as a triangular array. The entries S_5(n,k) are zero for 5k\u003en, so these values are omitted. Initial entry in sequence is S_5(5,1).",
				"Rows are of lengths 1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,..."
			],
			"reference": [
				"L. Comtet, Advanced Combinatorics, Reidel, 1974, p. 222.",
				"J. Riordan, An Introduction to Combinatorial Analysis, Wiley, 1958, p. 76."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A059024/b059024.txt\"\u003eRows n = 5..320, flattened\u003c/a\u003e",
				"A. E. Fekete, \u003ca href=\"http://www.jstor.org/stable/2974533\"\u003eApropos two notes on notation\u003c/a\u003e, Amer. Math. Monthly, 101 (1994), 771-778."
			],
			"formula": [
				"S_r(n+1, k) = k S_r(n, k) + binomial(n, r-1) S_r(n-r+1, k-1); for this sequence, r=5.",
				"G.f.: sum(S_r(n, k)u^k ((t^n)/(n!)), n=0..infty, k=0..infty) = exp(u(e^t-sum(t^i/i!, i=0..r-1)))."
			],
			"example": [
				"There are 126 ways of partitioning a set N of cardinality 10 into 2 blocks each of cardinality at least 5, so S_5(10,2) = 126.",
				"Triangle begins:",
				"1;",
				"1;",
				"1;",
				"1;",
				"1;",
				"1,    126;",
				"1,    462;",
				"1,   1254;",
				"1,   3003;",
				"1,   6721;",
				"1,  14443,    126126;",
				"1,  30251,   1009008;",
				"1,  62322,   5309304;",
				"1, 127024,  23075052;",
				"1, 257108,  89791416;",
				"1, 518092, 325355316, 488864376;",
				"..."
			],
			"maple": [
				"T:= proc(n,k) option remember; `if`(k\u003c1 or k\u003en/5, 0,",
				"      `if`(k=1, 1, k*T(n-1, k)+binomial(n-1, 4)*T(n-5, k-1)))",
				"    end:",
				"seq(seq(T(n, k), k=1..n/5), n=5..25);  # _Alois P. Heinz_, Aug 18 2017"
			],
			"mathematica": [
				"S5[n_ /; 5 \u003c= n \u003c= 9, 1] = 1; S5[n_, k_] /; 1 \u003c= k \u003c= Floor[n/5] := S5[n, k] = k*S5[n-1, k] + Binomial[n-1, 4]*S5[n-5, k-1]; S5[_, _] = 0; Flatten[ Table[ S5[n, k], {n, 5, 25}, {k, 1, Floor[n/5]}]] (* _Jean-François Alcover_, Feb 21 2012 *)"
			],
			"xref": [
				"Cf. A008299, A059022, A059023, A059025."
			],
			"keyword": "nonn,tabf,nice",
			"offset": "5,7",
			"author": "Barbara Haas Margolius (margolius(AT)math.csuohio.edu), Dec 14 2000",
			"references": 5,
			"revision": 18,
			"time": "2017-08-18T18:40:03-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}