{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A285893",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 285893,
			"data": "45,5,313,1,356067536821,36721681",
			"name": "Least number to start a run of exactly n nondecreasing values of sigma (sum of divisors, A000203).",
			"comment": [
				"a(6) = 36721681, see also A028965.",
				"The analogous sequence based on tau = A000005 instead of sigma is A284597."
			],
			"example": [
				"We have the following values of sigma for n = 1..10:",
				"         n   1   2   3   4   5   6   7   8   9  10  ...",
				"   sigma(n)  0   1   1   2   1   2   1   3   2   2  ...",
				"We see a run of 4 nondecreasing values starting at 1, ending at 4, therefore a(4) = 1. There is a run of 2 nondecreasing values starting at 5, ending at 6, therefore a(2) = 5.",
				"Correspondingly, a run of length 1 corresponds to a number n such that sigma(n-1) \u003e sigma(n) \u003e sigma(n+1). This happens first at a(1) = 45."
			],
			"mathematica": [
				"Function[s, {45}~Join~Map[Function[r, Select[s, Last@ # == r \u0026][[1, 1]]], Range[2, Max[s[[All, -1]] ] ]]]@ Map[{#[[1, 1]], Length@ # + 1} \u0026, DeleteCases[SplitBy[#, #[[-1]] \u003e= 0 \u0026], k_ /; k[[1, -1]] \u003c 0]] \u0026@ MapIndexed[{First@ #2, #1} \u0026, Differences@ Array[DivisorSigma[1, #] \u0026, 10^6]] (* _Michael De Vlieger_, May 06 2017 *)"
			],
			"program": [
				"(PARI) alias(A,A285893);A=vector(19);apply(scan(N,s=1,t=sigma(s))=for(k=s+1,N,t\u003e(t=sigma(k))||next;k-s\u003e#A||A[k-s]||printf(\"a(%d)=%d,\",k-s,s)||A[k-s]=s;s=k);done,[10^8]) \\\\ Search may be extended using scan(END,START)."
			],
			"xref": [
				"Cf. A000005, A000203, A028965, A284597 (analog for sigma_0), A286287 (analog for omega = A001221), A286288 (analog for bigomega = A001222)."
			],
			"keyword": "nonn,more,hard",
			"offset": "1,1",
			"author": "_M. F. Hasler_, May 06 2017",
			"ext": [
				"a(5)-a(6) from _Giovanni Resta_, May 07 2017"
			],
			"references": 6,
			"revision": 30,
			"time": "2017-05-18T12:42:23-04:00",
			"created": "2017-05-06T20:10:00-04:00"
		}
	]
}