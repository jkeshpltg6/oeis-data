{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A064986",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 64986,
			"data": "1,1,2,2,3,3,5,5,7,7,9,9,12,12,15,15,18,18,22,22,26,26,30,30,36,36,42,42,48,48,56,56,64,64,72,72,82,82,92,92,102,102,114,114,126,126,138,138,153,153,168,168,183,183,201,201,219,219,237,237,258,258,279,279",
			"name": "Number of partitions of n into factorial parts (0! not allowed).",
			"comment": [
				"a(2*n+1) = a(2*n) = A117930(n). [_Reinhard Zumkeller_, Dec 04 2011]"
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A064986/b064986.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e (terms 0..250 from Reinhard Zumkeller)",
				"Youkow Homma, Jun Hwan Ryu and Benjamin Tong, \u003ca href=\"http://sumry.yale.edu/sites/default/files/files/Sequence_nonsquashing_partitions.pdf\"\u003eSequence non-squashing partitions\u003c/a\u003e, Slides from a talk, Jul 24 2014.",
				"Igor Pak, \u003ca href=\"https://arxiv.org/abs/1803.06636\"\u003eComplexity problems in enumerative combinatorics\u003c/a\u003e, arXiv:1803.06636 [math.CO], 2018.",
				"\u003ca href=\"/index/Fa#factorial\"\u003eIndex entries for sequences related to factorial numbers\u003c/a\u003e"
			],
			"formula": [
				"G.f.: 1/Product_{i\u003e=1} (1-x^(i!)).",
				"G.f.: 1 + Sum_{n\u003e0} x^(n!) / Product_{k=1..n} (1 - x^(k!)). - _Seiichi Manyama_, Oct 12 2019",
				"G.f.: 1 + x/(1-x) + x^2/((1-x)*(1-x^2)) + x^6/((1-x)*(1-x^2)*(1-x^6)) + ... . - _Seiichi Manyama_, Oct 12 2019"
			],
			"example": [
				"a(3) = 2 because we can write 3 = 2!+1! = 1!+1!+1!.",
				"a(10) = 9 because 10 = 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1 = 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1 + 2 = 1 + 1 + 1 + 1 + 1 + 1 + 2 + 2 = 1 + 1 + 1 + 1 + 2 + 2 + 2 = 1 + 1 + 2 + 2 + 2 + 2 = 2 + 2 + 2 + 2 + 2 = 1 + 1 + 1 + 1 + 6 = 1 + 1 + 2 + 6 = 2 + 2 + 6."
			],
			"mathematica": [
				"b[n_, i_] := b[n, i] = If[n==0 || i==1, 1, b[n, i-1] + If[i!\u003en, 0, b[n-i!, i]]];",
				"c[n_] := Module[{i}, For[i = 1, i!\u003c2n, i++]; b[2n, i]];",
				"a[n_] := If[OddQ[n], c[(n-1)/2], c[n/2]];",
				"a /@ Range[0, 100] (* _Jean-François Alcover_, Feb 04 2020, after _Alois P. Heinz_ in A117930 *)",
				"Table[Length@IntegerPartitions[n, All, Factorial[Range[6]]], {n, 0, 63}] (* _Robert Price_, Jun 04 2020 *)"
			],
			"program": [
				"(Haskell)",
				"a064986 = p (tail a000142_list) where",
				"   p _          0             = 1",
				"   p fs'@(f:fs) m | m \u003c f     = 0",
				"                  | otherwise = p fs' (m - f) + p fs m",
				"-- _Reinhard Zumkeller_, Dec 04 2011",
				"(PARI) N=66; x='x+O('x^N); m=1; while(N\u003e=m!, m++); Vec(1/prod(k=1, m-1, 1-x^k!)) \\\\ _Seiichi Manyama_, Oct 13 2019",
				"(PARI) N=66; x='x+O('x^N); m=1; while(N\u003e=m!, m++); Vec(1+sum(i=1, m-1, x^i!/prod(j=1, i, 1-x^j!))) \\\\ _Seiichi Manyama_, Oct 13 2019"
			],
			"xref": [
				"Cf. A000142, A064985, A115944, A197182.",
				"Bisection gives A090632."
			],
			"keyword": "easy,nonn",
			"offset": "0,3",
			"author": "_Naohiro Nomoto_, Oct 30 2001",
			"ext": [
				"More terms from _Vladeta Jovovic_ and _Don Reble_, Nov 02 2001"
			],
			"references": 22,
			"revision": 53,
			"time": "2020-06-04T19:31:50-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}