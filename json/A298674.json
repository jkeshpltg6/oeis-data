{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A298674",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 298674,
			"data": "1,1,2,1,-2,-1,1,2,2,3,1,-2,-1,-1,2,1,2,-1,3,2,-2,1,-2,2,-1,-3,-4,2,1,2,-1,3,2,-2,2,4,1,-2,-1,-1,-3,2,2,0,-3,1,2,2,3,2,4,2,4,3,4,1,-2,-1,-1,2,2,-5,0,-3,-4,2,1,2,-1,3,-3,-2,2,4,-3,-6,2,-3",
			"name": "Square matrix read by antidiagonals up. Matrix of Dirichlet series associated with Sum_{n\u003c=X} MangoldtLambda(n) * MangoldtLambda(n+2).",
			"comment": [
				"For n \u003e 1: Sum_{k\u003e=1} T(n,k) = log(A014963(n))*log(A014963(n+2)).",
				"Triangular submatrix of this matrix is A298824.",
				"Row sums of A298824 are found in A298825.  A298825(n)/n = A298826(n). A298826 appears to be relevant to the heuristic for the twin prime conjecture.",
				"By varying the prime gap \"h\" in the program it appears that prime gaps that are powers of \"h\" have the same row sums of the triangular submatrix, which in turn seems to imply that prime gaps equal to powers of \"h\" have the same density."
			],
			"link": [
				"Terence Tao, \u003ca href=\"https://terrytao.wordpress.com/2017/07/06/correlations-of-the-von-mangoldt-and-higher-divisor-functions-i-long-shift-ranges/\"\u003eCorrelations of the von Mangoldt and higher divisor functions I. Long shift ranges\u003c/a\u003e. See second formula."
			],
			"formula": [
				"Let h = 2.",
				"Let A(n,k) = 1 if n mod k = 0, otherwise 0.",
				"Let B(n,k) = A008683(n)*n if k mod n = 0, otherwise 0.",
				"Let T = A.B (where \".\" is matrix multiplication).",
				"Take the Dirichlet convolution of a row in T(n,k) and a row in T(n+h,k) for n=1,2,3,4,5,... infinity, and form this matrix from the first columns of the convolutions. See Mathematica program for more precise description."
			],
			"example": [
				"The square matrix starts:",
				"{",
				"  {1,  2, -1,  3,  2, -2,  2,  4, -3,  4,  2, -3},",
				"  {1, -2,  2, -1,  2, -4,  2,  0,  3, -4,  2, -2},",
				"  {1,  2, -1,  3, -3, -2,  2,  4, -3, -6,  2, -3},",
				"  {1, -2, -1, -1,  2,  2,  2,  0, -3, -4,  2,  1},",
				"  {1,  2,  2,  3, -3,  4, -5,  4,  3, -6,  2,  6},",
				"  {1, -2, -1, -1,  2,  2,  2,  0, -3, -4,  2,  1},",
				"  {1,  2, -1,  3,  2, -2, -5,  4, -3,  4,  2, -3},",
				"  {1, -2,  2, -1, -3, -4,  2,  0,  3,  6,  2, -2},",
				"  {1,  2, -1,  3,  2, -2,  2,  4, -3,  4, -9, -3},",
				"  {1, -2, -1, -1, -3,  2,  2,  0, -3,  6,  2,  1}",
				"}"
			],
			"mathematica": [
				"h = 2; nn = 14;",
				"A = Table[If[Mod[n, k] == 0, 1, 0], {n, nn}, {k, nn}];",
				"B = Table[If[Mod[k, n] == 0, MoebiusMu[n]*n, 0], {n, nn}, {k, nn}];",
				"T = (A.B);",
				"TableForm[TwinMangoldt = Table[a = T[[All, kk]];",
				"    F1 = Table[If[Mod[n, k] == 0, a[[n/k]], 0], {n, nn}, {k, nn}];",
				"    b = T[[All, kk + h]];",
				"    F2 = Table[If[Mod[n, k] == 0, b[[n/k]], 0], {n, nn}, {k, nn}];",
				"    (F1.F2)[[All, 1]], {kk, nn - h}]];",
				"Flatten[Table[TwinMangoldt[[n - k + 1, k]], {n, nn - h}, {k, n}]]"
			],
			"xref": [
				"Cf. A191898, A298824, A001694, A298825, A298826."
			],
			"keyword": "sign,tabl",
			"offset": "1,3",
			"author": "_Mats Granvik_, Jan 24 2018",
			"references": 3,
			"revision": 38,
			"time": "2020-02-07T20:49:01-05:00",
			"created": "2018-04-15T18:50:08-04:00"
		}
	]
}