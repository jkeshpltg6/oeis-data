{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A347839",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 347839,
			"data": "2,5,8,11,20,32,14,44,80,128,17,56,176,320,512,23,68,224,704,1280,2048,26,92,272,896,2816,5120,8192,29,104,368,1088,3584,11264,20480,32768,35,116,416,1472,4352,14336,45056,81920,131072,38,140,464,1664,5888,17408,57344,180224,327680,524288",
			"name": "An array of the positive integers congruent to 2 modulo 3 (A016789), read by antidiagonals upwards, giving the present triangle.",
			"comment": [
				"This array a = (a(k, n))_{k \u003e= 1,n \u003e= 0} is underlying array A of A347834. See the first formula. It has a simple recurrence for the rows k, given the first column a(k, 0) = A347838(k), which lists the positive integers congruent to {2, 5, 11} modulo 12.",
				"In the array one can add the negative of the powers of 4 as row for k = 0, i.e., -A000302(n), for n \u003e= 0.",
				"All positive numbers congruent to 2 modulo 3 (A017617) appear once in this array. Proof from the array A of A347834 of the positive integers congruent to {1,3,5,7} modulo 8, and the present first formula: The members of column n = 0 give all the positive integers congruent to {2, 5, 11} modulo 12 once, and the members of columns n \u003e= 1 give all the positive integers congruent to 8 modulo 12 (A017617) once. These members combined lead to the positive integers congruent to 2 modulo 3."
			],
			"formula": [
				"Array a:",
				"a(k, n) = (3*A(k, n) + 1)/2, with the array A from A347834, for k \u003e= 1, and n \u003e= 0.",
				"a(k, n) = 4^n*A347838(k) = 4^n*(2 + 3*k + 3*floor((k + 1)/3)).",
				"Recurrence for rows k: a(k, n) = 4*a(k, n-1), for n \u003e= 1, with a(k, 0) = A347838(k).",
				"O.g.f.: expansion in z gives the o.g.f.s for rows k, also for k = 0: -A000302; expansion in x gives the o.g.f.s for columns n.",
				"G(z, x) = (-1 + 3*z + 3*z^2 + 7*z^3)/((1 - z)*(1 - z^3)*(1 - 4*x)).",
				"Triangle t:",
				"t(k, n) = a(k-n, n), for k \u003e= 1, and n = 0, 1, ..., k-1."
			],
			"example": [
				"The array a(k, n) begins:",
				"k \\ n  0   1   2    3    4     5      6      7       8       9       10 ...",
				"---------------------------------------------------------------------------",
				"1:     2   8  32  128  512  2048   8192  32768  131072  524288  2097152 ...",
				"2:     5  20  80  320 1280  5120  20480  81920  327680 1310720  5242880 ...",
				"3:    11  44 176  704 2816 11264  45056 180224  720896 2883584 11534336 ...",
				"4:    14  56 224  896 3584 14336  57344 229376  917504 3670016 14680064 ...",
				"5:    17  68 272 1088 4352 17408  69632 278528 1114112 4456448 17825792 ...",
				"6:    23  92 368 1472 5888 23552  94208 376832 1507328 6029312 24117248 ...",
				"7:    26 104 416 1664 6656 26624 106496 425984 1703936 6815744 27262976 ...",
				"8:    29 116 464 1856 7424 29696 118784 475136 1900544 7602176 30408704 ...",
				"9:    35 140 560 2240 8960 35840 143360 573440 2293760 9175040 36700160 ...",
				"10:   38 152 608 2432 9728 38912 155648 622592 2490368 9961472 39845888 ...",
				"...",
				"----------------------------------------------------------------------------",
				"The triangle t(n,k) begins:",
				"k \\ n  0   1   2    3    4     5     6      7      8      9 ...",
				"---------------------------------------------------------------",
				"1:     2",
				"2:     5   8",
				"3:    11  20  32",
				"4:    14  44  80  128",
				"5:    17  56 176  320  512",
				"6:    23  68 224  704 1280  2048",
				"7:    26  92 272  896 2816  5120  8192",
				"8:    29 104 368 1088 3584 11264 20480  32768",
				"9:    35 116 416 1472 4352 14336 45056  81920 131072",
				"10:   38 140 464 1664 5888 17408 57344 180224 327680 524288",
				"...",
				"-----------------------------------------------------------------"
			],
			"maple": [
				"A := (n, k) -\u003e 4^n*(3*(k + iquo(k, 3)) - 1):",
				"for k from 1 to 10 do seq(A(n, k), n = 0..10) od;",
				"# Alternatively:",
				"gf  := n -\u003e (4^n*((z*(z*(7*z + 3) + 3) - 1)))/((z - 1)^2*(1 + z + z^2)):",
				"ser := n -\u003e series(gf(n), z, 12):",
				"col := (n, len) -\u003e seq(coeff(ser(n), z, k), k = 1..len):",
				"seq(print(col(n, 10)), n = 0..10); # _Peter Luschny_, Oct 26 2021"
			],
			"mathematica": [
				"A[n_, k_] := 4^n (3(k + Quotient[k, 3]) - 1);",
				"Table[A[n-k, k], {n, 1, 10}, {k, n, 1, -1}] // Flatten (* _Jean-François Alcover_, Nov 07 2021, from Maple code *)"
			],
			"xref": [
				"Cf. A347834, A016789, A017617.",
				"The rows k are given by -A000302 (for k=0), A004171, A003947(n+1), A002089, 2*A002042, ...",
				"The columns n are given by 4^n*A347838 for n \u003e= 0."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "1,1",
			"author": "_Wolfdieter Lang_, Oct 21 2021",
			"references": 3,
			"revision": 18,
			"time": "2021-11-27T11:58:26-05:00",
			"created": "2021-10-27T00:02:37-04:00"
		}
	]
}