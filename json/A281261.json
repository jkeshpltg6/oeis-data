{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A281261",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 281261,
			"data": "1,2,2,1,5,2,5,9,2,1,15,14,2,7,35,20,2,1,28,70,27,2,9,84,126,35,2,1,45,210,210,44,2,11,165,462,330,54,2,1,66,495,924,495,65,2,13,286,1287,1716,715,77,2,1,91,1001,3003,3003,1001,90,2,15,455,3003,6435,5005,1365,104,2,1,120,1820,8008,12870,8008,1820,119,2",
			"name": "Triangle T(n,k) read by rows: coefficients of polynomials P_n(t) defined in Formula section.",
			"comment": [
				"Row n\u003e1 contains floor((n+3)/2) terms."
			],
			"link": [
				"Gheorghe Coserea, \u003ca href=\"/A281261/b281261.txt\"\u003eRows n = 1..202, flattened\u003c/a\u003e",
				"F. Chapoton, F. Hivert, J.-C. Novelli, \u003ca href=\"http://arxiv.org/abs/1307.0092\"\u003eA set-operad of formal fractions and dendriform-like sub-operads\u003c/a\u003e, arXiv preprint arXiv:1307.0092 [math.CO], 2013."
			],
			"formula": [
				"A(x;t) = Sum{n\u003e=1} P_n(t)*x^n = x*((1-t)*x^3 + (t^2-2*t-1)*x^2 + (2*t-1)*x + 1)/((t-1)*x^3 + (3-t)*x^2 - 3*x + 1).",
				"A278457(x;t) = serreverse(A(-x;t))(-x).",
				"A151821(n) = P_n(1), A213667(n) = P_n(2).",
				"P_n(t^2) = ((1+t)^(n+1) + (1-t)^(n+1))/2 - t^2 + 1, for n\u003e1."
			],
			"example": [
				"A(x;t) = x + (2*t+2)*x^2 + (t^2+5*t+2)*x^3 + (5*t^2+9*t+2)*x^4 + ...",
				"Triangle starts:",
				"n\\k  [1]      [2]      [3]      [4]      [5]      [6]      [7]      [8]",
				"[1]  1;",
				"[2]  2,       2;",
				"[3]  1,       5,       2;",
				"[4]  5,       9,       2;",
				"[5]  1,       15,      14,      2;",
				"[6]  7,       35,      20,      2;",
				"[7]  1,       28,      70,      27,      2;",
				"[8]  9,       84,      126,     35,      2;",
				"[9]  1,       45,      210,     210,     44,      2;",
				"[10] 11,      165,     462,     330,     54,      2;",
				"[11] 1,       66,      495,     924,     495,     65,      2;",
				"[12] 13,      286,     1287,    1716,    715,     77,      2;",
				"[13] 1,       91,      1001,    3003,    3003,    1001,    90,      2;",
				"[14] 15,      455,     3003,    6435,    5005,    1365,    104,     2;",
				"[15] ..."
			],
			"mathematica": [
				"Reverse[CoefficientList[#, t]]\u0026 /@ CoefficientList[x*((1-t)*x^3 + (t^2 - 2*t - 1)*x^2 + (2*t - 1)*x + 1)/((t-1)*x^3 + (3-t)*x^2 - 3*x + 1) + O[x]^16, x] // Rest // Flatten (* _Jean-François Alcover_, Feb 18 2019 *)"
			],
			"program": [
				"(PARI)",
				"N=16; x='x+O('x^N); concat(apply(p-\u003eVec(p),  Vec(Ser(x*((1-t)*x^3 + (t^2-2*t-1)*x^2 + (2*t-1)*x + 1)/((t-1)*x^3 + (3-t)*x^2 - 3*x + 1)))))",
				"(PARI)",
				"N = 14; concat(1, concat(vector(N, n, Vec(substpol(((1+t)^(n+2) + (1-t)^(n+2))/2 - t^2 + 1, t^2, t)))))"
			],
			"keyword": "nonn,tabf",
			"offset": "1,2",
			"author": "_Gheorghe Coserea_, Jan 18 2017",
			"references": 1,
			"revision": 29,
			"time": "2019-02-18T16:08:48-05:00",
			"created": "2017-01-18T21:46:41-05:00"
		}
	]
}