{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A009000",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 9000,
			"data": "5,10,13,15,17,20,25,25,26,29,30,34,35,37,39,40,41,45,50,50,51,52,53,55,58,60,61,65,65,65,65,68,70,73,74,75,75,78,80,82,85,85,85,85,87,89,90,91,95,97,100,100,101,102,104,105,106,109,110,111,113,115,116,117,119,120",
			"name": "Ordered hypotenuse numbers (squares are sums of 2 distinct nonzero squares).",
			"comment": [
				"The largest member 'c' of the Pythagorean triples (a,b,c) ordered by increasing c.",
				"If c^2 = a^2 + b^2 (a \u003c b \u003c c) then c^2 = (n^2 + m^2)/2 with n = b - a, m = b + a. - _Zak Seidov_, Mar 03 2011",
				"Numbers n such that A083025(n) \u003e 0, i.e., n is divisible by at least one prime of the form 4k+1. - _Max Alekseyev_, Oct 24 2008",
				"A number appears only once in the sequence if and only if it is divisible by exactly one prime of the form 4k+1 with multiplicity one (cf. A084645). - _Jean-Christophe Hervé_, Nov 11 2013",
				"If c^2 = a^2 + b^2 with a and b \u003e 0, then a \u003c\u003e b: the sum of 2 equal squares cannot be a square because sqrt(2) is not rational. - _Jean-Christophe Hervé_, Nov 11 2013"
			],
			"reference": [
				"W. L. Schaaf, Recreational Mathematics, A Guide To The Literature, \"The Pythagorean Relationship\", Chapter 6 pp. 89-99 NCTM VA 1963.",
				"W. L. Schaaf, A Bibliography of Recreational Mathematics, Vol. 2, \"The Pythagorean Relation\", Chapter 6 pp. 108-113 NCTM VA 1972.",
				"W. L. Schaaf, A Bibliography of Recreational Mathematics, Vol. 3, \"Pythagorean Recreations\", Chapter 6 pp. 62-6 NCTM VA 1973."
			],
			"link": [
				"Zak Seidov and T. D. Noe, \u003ca href=\"/A009000/b009000.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (Zak Seidov entered the first 1981 terms).",
				"Anonymous, \u003ca href=\"http://www.tssi.com/Pythagoras%20Connections.html\"\u003eLinks to Pythagorean Theorem Proofs\u003c/a\u003e",
				"H. Bottomley, \u003ca href=\"http://www.se16.info/hgb/pyth.htm\"\u003ePythagoras's theorem (animated proof)\u003c/a\u003e",
				"Dept. of Pure Math., Univ. Sheffield, \u003ca href=\"http://www.shef.ac.uk/~puremath/theorems/pythag.html\"\u003eAnimated Proof of Pythagoras Theorem\u003c/a\u003e [Broken link?]",
				"T. Eveilleau, \u003ca href=\"http://therese.eveilleau.pagesperso-orange.fr/pages/truc_mat/pythagor/textes/euclide.htm\"\u003eAnimated proofs of the Pythagorean theorem:Sample Ancient Proofs (Text in French)\u003c/a\u003e",
				"T. Eveilleau, \u003ca href=\"http://perso.orange.fr/therese.eveilleau/pages/truc_mat/pythagor/textes/triangles-chinois.htm\"\u003eMore Animated proofs of the Pythagorean theorem (Text in French)\u003c/a\u003e [broken link]",
				"T. Eveilleau, \u003ca href=\"http://therese.eveilleau.pagesperso-orange.fr/pages/truc_mat/pythagor/textes/vasques.html\"\u003eAn Experimental Illustration of the Pythagorean Theorem\u003c/a\u003e, (requires a flash player)",
				"Kangourou Math Website, \u003ca href=\"http://www.mathkang.org/swf/pythagore2.html\"\u003eL'animation du theoreme de Pythagore\u003c/a\u003e",
				"Ron Knott, \u003ca href=\"http://www.maths.surrey.ac.uk/hosted-sites/R.Knott/Pythag/pythag.html\"\u003ePythagorean Triples and Online Calculators\u003c/a\u003e",
				"Ron Knott, \u003ca href=\"http://www.maths.surrey.ac.uk/hosted-sites/R.Knott/Pythag/pythag.html\"\u003eRight-angled Triangles and Pythagoras' Theorem\u003c/a\u003e",
				"I. Kobayashi et al., \u003ca href=\"https://web.archive.org/web/20120213104859/http://www.ies.co.jp/math/java/geo/pythagoras.html\"\u003ePythagorean Theorem (Java Interactive Proofs, Applications and Explorations)\u003c/a\u003e",
				"Mathematical Database, Poster, \u003ca href=\"http://mathdb.org/gallery/poster/description/e_poster_01.htm\"\u003e7 Ways to prove the Pythagorean Theorem\u003c/a\u003e",
				"B. Richmond, \u003ca href=\"http://www.wku.edu/~tom.richmond/Pythag.html\"\u003eThe Pythagorean Theorem\u003c/a\u003e",
				"M. Shepperd, \u003ca href=\"http://www.teachers.ash.org.au/mikemath/resources/pythagoras.html\"\u003eWeb Resources on Pythagoras' Theorem\u003c/a\u003e",
				"J. S. Silverman, A Friendly Introduction to Number Theory, \u003ca href=\"https://www.math.brown.edu/johsilve/frintch1ch6.pdf\"\u003eChapters 1 to 6\u003c/a\u003e (see Chapters 2 and 3).",
				"G. Villemin's Almanach of Numbers, \u003ca href=\"http://villemin.gerard.free.fr/Wwwgvmm/Addition/ThPythag.htm\"\u003eTriangles \u0026 Triplets de Pythagore\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PythagoreanTriple.html\"\u003ePythagorean Triple\u003c/a\u003e",
				"\u003ca href=\"/index/Su#ssq\"\u003eIndex entries for sequences related to sums of squares\u003c/a\u003e"
			],
			"mathematica": [
				"max = 120; hypotenuseQ[n_] := For[k = 1, True, k++, p = Prime[k]; Which[Mod[p, 4] == 1 \u0026\u0026 Divisible[n, p], Return[True], p \u003e n, Return[False]]]; hypotenuses = Select[Range[max], hypotenuseQ]; red[c_] := {a, b, c} /. {ToRules[ Reduce[0 \u003c a \u003c= b \u0026\u0026 a^2 + b^2 == c^2, {a, b}, Integers]]}; A009000 = Flatten[red /@ hypotenuses, 1][[All, -1]] (* _Jean-François Alcover_, May 23 2012, after _Max Alekseyev_ *)"
			],
			"program": [
				"(PARI) list(lim)=my(v=List(),m2,s2,h2,h); for(middle=4,lim-1, m2=middle^2; for(small=1,middle, s2=small^2; if(issquare(h2=m2+s2,\u0026h), if(h\u003elim, break); listput(v, h)))); vecsort(Vec(v)) \\\\ _Charles R Greathouse IV_, Jun 23 2017",
				"(PARI) list(lim) = {my(lh = List()); for(u = 2, sqrtint(lim), for(v = 1, u, if (u^2+v^2 \u003e lim, break); if ((gcd(u,v) == 1) \u0026\u0026 (0 != (u-v)%2), for (i = 1, lim, if (i*(u^2+v^2) \u003e lim, break); /* if (u^2 - v^2 \u003c 2*u*v, w = [i*(u^2 - v^2), i*2*u*v, i*(u^2+v^2)], w = [i*2*u*v, i*(u^2 - v^2), i*(u^2+v^2)]); */ listput(lh, i*(u^2+v^2)););););); vecsort(Vec(lh));} \\\\ _Michel Marcus_, Apr 10 2021",
				"(Python)",
				"from math import isqrt",
				"def aupto(limit):",
				"  s = [i*i for i in range(1, limit+1)]",
				"  s2 = sorted(a+b for i, a in enumerate(s) for b in s[i+1:])",
				"  return [isqrt(k) for k in s2 if k in s]",
				"print(aupto(120)) # _Michael S. Branicky_, May 10 2021"
			],
			"xref": [
				"Cf. A009012, A009003, A024507, A004431, A046083, A046084, A004144, A083025, A084645, A084646, A084647, A084648, A084649, A006339."
			],
			"keyword": "nonn,nice,easy",
			"offset": "1,1",
			"author": "_David W. Wilson_",
			"references": 35,
			"revision": 75,
			"time": "2021-05-10T11:20:32-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}