{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A177383",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 177383,
			"data": "1,1,-3,18,-145,1398,-15330,186004,-2455893,34903110,-529903726,8547005820,-145847288730,2624318836412,-49658727391620,985921121129832,-20496980960236365,445413655950148710,-10100793273684125430",
			"name": "G.f. satisfies: [x^n] A(x)^(n+1) = [x^n] A(x)^(n+2) for n\u003e1 with A'(0)=A(0)=1.",
			"link": [
				"Vaclav Kotesovec, \u003ca href=\"/A177383/b177383.txt\"\u003eTable of n, a(n) for n = 0..400\u003c/a\u003e"
			],
			"formula": [
				"G.f. satisfies: A(x) = 1 + x * (A(x) - x*A'(x)) / A(x)^3. - _Paul D. Hanna_, Aug 04 2014",
				"Let G(x) be the g.f. of A177384, then",
				". a(n) = [x^n] G(x)^(-n+1)/(-n+1) for n\u003e1,",
				". A(x) = G(x/A(x)) so that A(x) = x/Series_Reversion(x*G(x))",
				"where G(x) = 1 + x/(G(x) + x*G'(x)).",
				"a(n) ~ c * (-1)^(n+1) * n! * n^4, where c = 0.0147556981601927885048672335828437... (same as for A182304). - _Vaclav Kotesovec_, Jul 28 2014",
				"a(n) / A177384(n) ~ exp(1). - _Vaclav Kotesovec_, Mar 06 2020"
			],
			"example": [
				"G.f.: A(x) = 1 + x - 3*x^2 + 18*x^3 - 145*x^4 + 1398*x^5 +...",
				"Coefficients in the initial powers of A(x) begin:",
				"[1,. 1, -3,. 18,. -145,. 1398,. -15330,. 186004,. -2455893,...];",
				"[1,. 2, -5,. 30,. -245,. 2398,. -26670,. 327740,. -4376445,...];",
				"[1,. 3,(-6), 37,. -309,. 3081,. -34806,. 433470,. -5855301,...];",
				"[1,. 4,(-6),(40), -345,. 3516,. -40398,. 510120,. -6971325,...];",
				"[1,. 5, -5, (40),(-360), 3761,. -43995,. 563460,. -7790675,...];",
				"[1,. 6, -3,. 38, (-360),(3864), -46049,. 598266,. -8368635,...];",
				"[1,. 7,. 0,. 35,. -350, (3864),(-46928), 618465,. -8751225,...];",
				"[1,. 8,. 4,. 32,. -334,. 3792, (-46928),(627264), -8976609,...];",
				"[1,. 9,. 9,. 30,. -315,. 3672,. -46284, (627264),(-9076320),...];",
				"[1, 10, 15,. 30,. -295,. 3522,. -45180,. 620560, (-9076320),...];",
				"where the above terms in parenthesis illustrate the property",
				"that the coefficients of x^n in A(x)^(n+1) and A(x)^(n+2) are equal.",
				"Also, the same terms derive A177384(n) = [x^n] A(x)^(n+1)/(n+1):",
				"[1, 1, -2, 10, -72, 644, -6704, 78408, -1008480, 14065744, ...]",
				"where g.f. G(x) of A177384 satisfies 1/x*d/dx[xG(x)]^2/2 = x+d/dx xG(x):",
				"d/dx x*G(x) = 1 + 2*x - 6*x^2 + 40*x^3 - 360*x^4 + 3864*x^5 -...",
				"d/dx [x*G(x)]^2/2 = x + 3*x^2 - 6*x^3 + 40*x^4 - 360*x^5 + 3864*x^6 -..."
			],
			"program": [
				"(PARI) {a(n)=local(G=1+x+x*O(x^n),H);for(i=1,n,G=1+x/(G+x*deriv(G)+x*O(x^n))); H=x/serreverse(x*G);polcoeff(H,n)}",
				"for(n=0,20,print1(a(n),\", \"))",
				"(PARI) /* From A(x) = 1 + x*(A(x) - x*A'(x)) / A(x)^3 : */",
				"{a(n)=local(A=1+x+x*O(x^n));for(i=1,n,A=1+x*(A-x*A')/(A^3 +x*O(x^n)));polcoeff(A,n)}",
				"for(n=0,20,print1(a(n),\", \"))"
			],
			"xref": [
				"Cf. A177384, A158883, A182304."
			],
			"keyword": "sign",
			"offset": "0,3",
			"author": "_Paul D. Hanna_, May 15 2010",
			"references": 2,
			"revision": 12,
			"time": "2020-03-06T17:14:21-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}