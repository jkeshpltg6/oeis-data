{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A243353",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 243353,
			"data": "1,2,4,3,9,8,6,5,25,18,16,27,15,12,10,7,49,50,36,75,81,32,54,125,35,30,24,45,21,20,14,11,121,98,100,147,225,72,150,245,625,162,64,243,375,108,250,343,77,70,60,105,135,48,90,175,55,42,40,63,33,28,22,13,169,242,196,363,441,200,294,605,1225,450,144",
			"name": "Permutation of natural numbers which maps between the partitions as encoded in A227739 (binary based system, zero-based) to A112798 (prime-index based system, one-based).",
			"comment": [
				"Note the indexing: the domain includes zero, but the range starts from one."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A243353/b243353.txt\"\u003eTable of n, a(n) for n = 0..8192\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A005940(1+A003188(n)).",
				"a(n) = A241909(1+A075157(n)). [With A075157's original starting offset]",
				"For all n \u003e= 0, A243354(a(n)) = n.",
				"A227183(n) = A056239(a(n)). [Maps between the corresponding sums ...]",
				"A227184(n) = A003963(a(n)). [... and products of parts of each partition].",
				"For n \u003e= 0, a(A037481(n)) = A002110(n). [Also \"triangular partitions\", the fixed points of Bulgarian solitaire, A226062 \u0026 A242424].",
				"For n \u003e= 1, a(A227451(n+1)) = 4*A243054(n)."
			],
			"mathematica": [
				"f[n_, i_, x_] := Which[n == 0, x, EvenQ@ n, f[n/2, i + 1, x], True, f[(n - 1)/2, i, x Prime@ i]]; Table[f[BitXor[n, Floor[n/2]], 1, 1], {n, 0, 74}] (* _Michael De Vlieger_, May 09 2017 *)"
			],
			"program": [
				"(Scheme) (define (A243353 n) (A005940 (+ 1 (A003188 n))))",
				"(Python)",
				"from sympy import prime",
				"import math",
				"def A(n): return n - 2**int(math.floor(math.log(n, 2)))",
				"def b(n): return n + 1 if n\u003c2 else prime(1 + (len(bin(n)[2:]) - bin(n)[2:].count(\"1\"))) * b(A(n))",
				"def a005940(n): return b(n - 1)",
				"def a003188(n): return n^int(n/2)",
				"def a243353(n): return a005940(1 + a003188(n)) # _Indranil Ghosh_, May 07 2017"
			],
			"xref": [
				"A243354 gives the inverse mapping.",
				"Cf. A227739, A112798, A075157, A241909, A005940, A003188, A226062, A242424."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Antti Karttunen_, Jun 05 2014",
			"references": 12,
			"revision": 21,
			"time": "2017-05-09T22:40:40-04:00",
			"created": "2014-06-29T00:07:53-04:00"
		}
	]
}