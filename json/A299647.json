{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A299647",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 299647,
			"data": "3,8,14,19,25,30,36,41,47,52,58,63,69,74,80,85,91,96,102,107,113,118,124,129,135,140,146,151,157,162,168,173,179,184,190,195,201,206,212,217,223,228,234,239,245,250,256,261,267,272,278,283,289,294,300,305,311,316",
			"name": "Positive solutions to x^2 == -2 (mod 11).",
			"comment": [
				"Positive numbers congruent to {3, 8} mod 11. Equivalently, interleaving of A017425 and A017485."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A299647/b299647.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,1,-1)."
			],
			"formula": [
				"O.g.f.: x*(3 + 5*x + 3*x^2)/((1 + x)*(1 - x)^2).",
				"E.g.f.: (-1 + 12*exp(x) - 11*exp(2*x) + 22*x*exp(2*x))*exp(-x)/4.",
				"a(n) = -a(-n+1) = a(n-1) + a(n-2) - a(n-3).",
				"a(n) = 5*n - 2 + (2*n - (-1)^n - 3)/4.",
				"a(n) = 4*n - 1 + floor((n - 1)/2) + floor((3*n - 1)/3).",
				"a(n+k) - a(n) = 11*k/2 + (1 - (-1)^k)*(-1)^n/4.",
				"a(n+k) + a(n) = 11*(2*n + k - 1)/2 - (1 + (-1)^k)*(-1)^n/4."
			],
			"mathematica": [
				"Table[5 n - 2 + (2 n - (-1)^n - 3)/4, {n, 1, 60}]",
				"CoefficientList[ Series[(3 + 5x + 3x^2)/((x - 1)^2 (x + 1)), {x, 0, 57}], x] (* or *)",
				"LinearRecurrence[{1, 1, -1}, {3, 8, 14}, 58] (* _Robert G. Wilson v_, Mar 08 2018 *)"
			],
			"program": [
				"(PARI) vector(60, n, nn; 5*n-2+(2*n-(-1)^n-3)/4)",
				"(Sage) [5*n-2+(2*n-(-1)^n-3)/4 for n in (1..60)]",
				"(Maxima) makelist(5*n-2+(2*n-(-1)^n-3)/4, n, 1, 60);",
				"(GAP) List([1..60], n -\u003e 5*n-2+(2*n-(-1)^n-3)/4);",
				"(MAGMA) [5*n-2+(2*n-(-1)^n-3)/4: n in [1..60]];",
				"(Python) [5*n-2+(2*n-(-1)**n-3)/4 for n in range(1, 60)]",
				"(Julia) [(11(2n-1)-(-1)^n)\u003e\u003e2 for n in 1:60] # _Peter Luschny_, Mar 07 2018"
			],
			"xref": [
				"Subsequence of A106252, A279000.",
				"Cf. A017425, A017485.",
				"Cf. A017497: positive solutions to x == -2 (mod 11).",
				"Cf. A017437: positive solutions to x^3 == -2 (mod 11).",
				"Nonnegative solutions to x^2 == -2 (mod j): A005843 (j=2), A001651 (j=3), A047235 (j=6), A156638 (j=9), this sequence (j=11)."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_Bruno Berselli_, Mar 06 2018",
			"references": 1,
			"revision": 27,
			"time": "2019-12-07T12:18:29-05:00",
			"created": "2018-03-07T13:26:54-05:00"
		}
	]
}