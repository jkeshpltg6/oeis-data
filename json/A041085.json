{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A041085",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 41085,
			"data": "1,14,197,2772,39005,548842,7722793,108667944,1529074009,21515704070,302748930989,4260000737916,59942759261813,843458630403298,11868363584907985,167000548819115088,2349876047052519217,33065265207554384126,465263588952813896981",
			"name": "Denominators of continued fraction convergents to sqrt(50).",
			"comment": [
				"For positive n, a(n) equals the permanent of the n X n tridiagonal matrix with 14's along the main diagonal, and 1's along the superdiagonal and the subdiagonal. - _John M. Campbell_, Jul 08 2011",
				"a(n) equals the number of words of length n on alphabet {0,1,...,14} avoiding runs of zeros of odd lengths. - _Milan Janjic_, Jan 28 2015"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A041085/b041085.txt\"\u003eTable of n, a(n) for n = 0..800\u003c/a\u003e",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (14,1)."
			],
			"formula": [
				"a(n) = round((7+5*sqrt(2))*a(n-1)). - _Vladeta Jovovic_, Jun 15 2003",
				"a(n) = A000129(3n+3)/5. a(n) = (1+sqrt(2))^(3*n)*(1/2+7*sqrt(2)/20)+(1-sqrt(2))^(3*n)*(1/2-7*sqrt(2)/20). a(n) = sum{i=0..n, sum{j=0..n, (n!/(i!j!(n-i-j)!)*A000129(2n-i)/5}}. - _Paul Barry_, Feb 06 2004",
				"a(n) = F(n, 14), the n-th Fibonacci polynomial evaluated at x=14. - _T. D. Noe_, Jan 19 2006",
				"From _Philippe Deléham_, Nov 03 2008: (Start)",
				"a(n) = 14*a(n-1)+a(n-2); a(0)=1, a(1)=14.",
				"G.f.: 1/(1-14*x-x^2). (End)",
				"a(n) = ((7+5*sqrt(2))^(n+1)-(7-5*sqrt(2))^(n+1))/(10*sqrt(2)). - _Gerry Martens_, Jul 11 2015"
			],
			"maple": [
				"with(combinat): seq(fibonacci(3*n+3,2)/5, n=0..17); # _Zerinvary Lajos_, Apr 20 2008"
			],
			"mathematica": [
				"LinearRecurrence[{14, 1}, {1, 14}, 30] (* _Vincenzo Librandi_, Nov 17 2012 *)",
				"Table[Fibonacci[3n + 3, 2]/5, {n, 0, 20}] (* _Vladimir Reshetnikov_, Sep 16 2016 *)"
			],
			"program": [
				"(MAGMA) I:=[1, 14]; [n le 2 select I[n] else 14*Self(n-1) +Self(n-2): n in [1..30]]; // _Vincenzo Librandi_, Nov 17 2012"
			],
			"xref": [
				"Cf. A041084, A040042, A020807."
			],
			"keyword": "nonn,cofr,easy,frac",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Additional term from _Colin Barker_, Nov 12 2013"
			],
			"references": 14,
			"revision": 49,
			"time": "2021-06-24T03:31:23-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}