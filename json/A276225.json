{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A276225",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 276225,
			"data": "3,2,6,17,42,107,273,695,1770,4508,11481,29240,74469,189659,483027,1230182,3133050,7979309,20321850,51756059,131813277,335704463,854978262,2177474264,5545631253,14123715032,35970535581,91610417447,233315085507,594211124042,1513347751038,3854221711625,9816002298330",
			"name": "a(n+3) = 2*a(n+2) + a(n+1) + a(n) with a(0)=3, a(1)=2, a(2)=6.",
			"comment": [
				"Also the number of maximal independent vertex sets (and minimal vertex covers) on the 2n-crossed prism graph. - _Eric W. Weisstein_, Jun 15 2017",
				"Also the number of irredundant sets in the n-sun graph. - _Eric W. Weisstein_, Aug 07 2017",
				"Let {x,y,z} be the simple roots of P(x) = x^3 + u*x^2 + v*x + w. For n\u003e=0, let p(n) = x^n/((x-y)(x-z)) + y^n/((y-x)(y-z)) + z^n/((z-x)(z-y)), q(n) =  x^n + y^n + z^n. Then for n \u003e= 0, q(n) = 3*p(n+2) + 2*u*p(n+1) + v*p(n). In this case, P(x) = x^3 - 2*x^2 - x - 1, q(n) = a(n), p(n) = A077939(n). - _Kai Wang_, Apr 15 2020"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A276225/b276225.txt\"\u003eTable of n, a(n) for n = 0..2450\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CrossedPrismGraph.html\"\u003eCrossed Prism Graph\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/IrredundantSet.html\"\u003eIrredundant Set\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/MaximalIndependentVertexSet.html\"\u003eMaximal Independent Vertex Set\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/MinimalVertexCover.html\"\u003eMinimal Vertex Cover\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/SunGraph.html\"\u003eSun Graph\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,1,1)."
			],
			"formula": [
				"Let p = (4*(61 + 9*sqrt(29)))^(1/3), q = (4*(61 - 9*sqrt(29)))^(1/3), and x = (1/6)*(4 + p + q) then x^n = (1/6)*(2*a(n) + A276226(n)*(p + q) + A077939(n-1)*(p^2 + q^2)).",
				"G.f.: (3 - 4*x - x^2)/(1 - 2*x - x^2 - x^3).",
				"a(n) = b^n + c^n + d^n, where (b, c, d) are the three roots of the cubic equation x^3 = 2*x^2 + x + 1.",
				"a(n) = 3*A077939(n+2) - 4*A077939(n+1) - A077939(n). - _Kai Wang_, Apr 15 2020"
			],
			"maple": [
				"f:= gfun:-rectoproc({a(n+3) = 2*a(n+2) + a(n+1) + a(n), a(0)=3, a(1)=2, a(2)=6},a(n),remember):",
				"map(f, [$0..40]); # _Robert Israel_, Aug 29 2016"
			],
			"mathematica": [
				"LinearRecurrence[{2, 1, 1}, {3, 2, 6}, 50]",
				"CoefficientList[Series[(3 - 4 x - x^2)/(1 - 2 x - x^2 - x^3), {x, 0, 32}], x] (* _Michael De Vlieger_, Aug 25 2016 *)",
				"Table[RootSum[-1 - #1 - 2 #1^2 + #1^3 \u0026, #^n \u0026], {n, 20}] (* _Eric W. Weisstein_, Jun 15 2017 *)"
			],
			"program": [
				"(MAGMA) I:=[3,2,6]; [n le 3 select I[n] else 2*Self(n-1)+ Self(n-2)+Self(n-3): n in [1..40]]; // _Vincenzo Librandi_, Aug 25 2016",
				"(PARI) Vec((3-4*x-x^2)/(1-2*x-x^2-x^3) + O(x^99)) \\\\ _Altug Alkan_, Aug 25 2016"
			],
			"xref": [
				"Cf. A077939, A276226."
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "_G. C. Greubel_, Aug 24 2016",
			"references": 4,
			"revision": 71,
			"time": "2021-01-30T21:02:06-05:00",
			"created": "2016-08-25T17:37:44-04:00"
		}
	]
}