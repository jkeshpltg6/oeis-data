{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A299990",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 299990,
			"data": "-1,-2,-2,-3,-2,-3,-2,-4,-3,-2,-2,-4,-2,-2,-3,-5,-2,-2,-2,-4,-3,-1,-2,-5,-3,-1,-4,-4,-2,2,-2,-6,-2,0,-3,-4,-2,0,-2,-5,-2,3,-2,-3,-4,0,-2,-5,-3,0,-2,-3,-2,0,-3,-5,-2,0,-2,2,-2,0,-4,-7,-3,6,-2,-2",
			"name": "a(n) = A243822(n) - A000005(n).",
			"comment": [
				"Since A010846(n) = A000005(n) + A243822(n), this sequence examines the balance of the two components among \"regular\" numbers.",
				"Value of a(n) is generally less frequently negative as n increases.",
				"a(1) = -1.",
				"For primes p, a(p) = -2 since 1 | p and the cototient is restricted to the divisor p.",
				"For perfect prime powers p^e, a(p^e) = -(e + 1), since all m \u003c p^e in the cototient of p^e that do not have a prime factor q coprime to p^e are powers p^k with 1 \u003c p^k \u003c= p^e; all such p^k divide p^e.",
				"Generally for n with A001221(n) = 1, a(n) = -1 * A000005(n), since the cototient is restricted to divisors, and in the case of p^e \u003e 4, divisors and numbers in A272619(p^e) not counted by A010846(p^e).",
				"For m \u003e= 3, a(A002110(m)) is positive.",
				"For m \u003e= 9, a(A244052(m)) is positive."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A299990/b299990.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Michael De Vlieger, \u003ca href=\"/A299990/a299990_1.txt\"\u003eExamination of the relationships of the species of numbers enumerated in A010846\u003c/a\u003e."
			],
			"formula": [
				"a(n) = A010846(n) - 2*A000005(n)."
			],
			"example": [
				"a(6) = -3 since 6 has 4 divisors, and 4 | 6^2; A243822(6) = 1 and A000005(6) = 4; 1 - 4 = -3. Alternatively, A010846(6) = 5; 5 - 2*4 = -3.",
				"a(30) = 2 since 30 has 8 divisors and the numbers {4, 8, 9, 12, 16, 18, 20, 24, 25, 27} divide 30^e with e \u003e 1; A243822(30) = 10 and A000005(30) = 8; 10 - 8 = 2. Alternatively, A010846(30) = 18; 18 - 2*8 = 2.",
				"Some values of a(n) and related sequences:",
				"   n  a(n) A010846(n) A243822(n) A000005(n) A272618(n)",
				"  ----------------------------------------------------",
				"   1   -1          1          0          1  0",
				"   2   -2          2          0          2  0",
				"   3   -2          2          0          2  0",
				"   4   -3          3          0          3  0",
				"   5   -2          2          0          2  0",
				"   6   -3          5          1          4  {4}",
				"   7   -2          2          0          2  0",
				"   8   -4          4          0          4  0",
				"   9   -3          3          0          3  0",
				"  10   -2          6          2          4  {4,8}",
				"  11   -2          2          0          2  0",
				"  12   -4          8          2          6  {8,9}",
				"  ...",
				"  30    2         18         10          8  {4,8,9,12,16,18,20,24,25,27}",
				"  ...",
				"  34    0          8          4          4  {4,8,16,32}",
				"  ..."
			],
			"mathematica": [
				"Table[Count[Range[n], _?(PowerMod[n, Floor@ Log2@ n, #] == 0 \u0026)] - 2 DivisorSigma[0, n], {n, 68}]"
			],
			"xref": [
				"Cf. A000005, A002110, A010846, A243822, A272618, A272619, A299991, A299992, A300155, A300156, A300157."
			],
			"keyword": "sign",
			"offset": "1,2",
			"author": "_Michael De Vlieger_, Feb 25 2018",
			"references": 6,
			"revision": 12,
			"time": "2018-03-03T14:58:18-05:00",
			"created": "2018-02-25T22:53:37-05:00"
		}
	]
}