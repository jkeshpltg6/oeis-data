{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A279470",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 279470,
			"data": "1,2,2,12,5,58,8,243,22,1006,42,4036,81,16302,138,65332,369,261854,722,1047788,1453,4193246,2642,16773388,5229,67101182,11522,268422392,20111,1073711992,41803,4294908838,97934,17179755306,179088,68719308495,293624",
			"name": "Decimal representation of the x-axis, from the left edge to the origin, of the n-th stage of growth of the two-dimensional cellular automaton defined by \"Rule 157\", based on the 5-celled von Neumann neighborhood.",
			"comment": [
				"Initialized with a single black (ON) cell at stage zero."
			],
			"reference": [
				"S. Wolfram, A New Kind of Science, Wolfram Media, 2002; p. 170."
			],
			"link": [
				"Robert Price, \u003ca href=\"/A279470/b279470.txt\"\u003eTable of n, a(n) for n = 0..126\u003c/a\u003e",
				"Robert Price, \u003ca href=\"/A279470/a279470.tmp.txt\"\u003eDiagrams of first 20 stages\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"http://arxiv.org/abs/1503.01168\"\u003eOn the Number of ON Cells in Cellular Automata\u003c/a\u003e, arXiv:1503.01168 [math.CO], 2015",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ElementaryCellularAutomaton.html\"\u003eElementary Cellular Automaton\u003c/a\u003e",
				"S. Wolfram, \u003ca href=\"http://wolframscience.com/\"\u003eA New Kind of Science\u003c/a\u003e",
				"Wolfram Research, \u003ca href=\"http://atlas.wolfram.com/\"\u003eWolfram Atlas of Simple Programs\u003c/a\u003e",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_2D_5-Neighbor_Cellular_Automata\"\u003eIndex to 2D 5-Neighbor Cellular Automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_Elementary_Cellular_Automata\"\u003eIndex to Elementary Cellular Automata\u003c/a\u003e"
			],
			"mathematica": [
				"CAStep[rule_, a_] := Map[rule[[10 - #]] \u0026, ListConvolve[{{0, 2, 0},{2, 1, 2}, {0, 2, 0}}, a, 2],{2}];",
				"code = 157; stages = 128;",
				"rule = IntegerDigits[code, 2, 10];",
				"g = 2 * stages + 1; (* Maximum size of grid *)",
				"a = PadLeft[{{1}}, {g, g}, 0,Floor[{g, g}/2]]; (* Initial ON cell on grid *)",
				"ca = a;",
				"ca = Table[ca = CAStep[rule, ca], {n, 1, stages + 1}];",
				"PrependTo[ca, a];",
				"(* Trim full grid to reflect growth by one cell at each stage *)",
				"k = (Length[ca[[1]]] + 1)/2;",
				"ca = Table[Table[Part[ca[[n]] [[j]],Range[k + 1 - n, k - 1 + n]], {j, k + 1 - n, k - 1 + n}], {n, 1, k}];",
				"Table[FromDigits[Part[ca[[i]] [[i]], Range[1, i]], 2], {i, 1, stages - 1}]"
			],
			"xref": [
				"Cf. A279468, A279469, A279471."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Robert Price_, Dec 12 2016",
			"references": 4,
			"revision": 8,
			"time": "2016-12-13T11:21:34-05:00",
			"created": "2016-12-13T11:21:34-05:00"
		}
	]
}