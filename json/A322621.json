{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A322621",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 322621,
			"data": "1,1,2,1,1,16,30,16,1,1,96,615,1040,615,96,1,1,512,10220,43904,68390,43904,10220,512,1,1,2560,147645,1482240,4998210,7322112,4998210,1482240,147645,2560,1,1,12288,1948650,43310080,291662415,831080448,1161583500,831080448,291662415,43310080,1948650,12288,1,1,57344,24180611,1145417728,14692638961,75654971392,190145878923,256124504064,190145878923,75654971392,14692638961,1145417728,24180611,57344,1",
			"name": "E.g.f.: C(x,y) = cosh(x)*cosh(y) / (1 - sinh(x)*sinh(y)), where C(x,y) = Sum_{n\u003e=0} Sum_{k=0..2*n} T(n,k) * x^(2*n-k)*y^k/(2*n)!, as a triangle of coefficients T(n,k) read by rows.",
			"comment": [
				"See A322193 for another description of the e.g.f. of this sequence."
			],
			"link": [
				"Paul D. Hanna, \u003ca href=\"/A322621/b322621.txt\"\u003eTable of n, a(n) for n = 0..2600 terms of this triangle read by rows 0..50.\u003c/a\u003e"
			],
			"formula": [
				"E.g.f.: C(x,y) and related series S(x,y) satisfy the following identities.",
				"(1) C(x,y)^2 - S(x,y)^2 = 1.",
				"(2a) C(x,y) = cosh(x) * cosh(y) / (1 - sinh(x)*sinh(y)).",
				"(2b) S(x,y) = (sinh(x) + sinh(y)) / (1 - sinh(x)*sinh(y)).",
				"(3a) cosh(x) = C(x,y) * cosh(y) / (1 + sinh(y)*S(x,y)).",
				"(3b) sinh(x) = (S(x,y) - sinh(y)) / (1 + sinh(y)*S(x,y)).",
				"(3c) cosh(y) = C(x,y) * cosh(x) / (1 + sinh(x)*S(x,y)).",
				"(3d) sinh(y) = (S(x,y) - sinh(x)) / (1 + sinh(x)*S(x,y)).",
				"(4a) exp(x) = (C(x,y)*cosh(y) + S(x,y) - sinh(y)) / (1 + sinh(y)*S(x,y)).",
				"(4b) exp(y) = (C(x,y)*cosh(x) + S(x,y) - sinh(x)) / (1 + sinh(x)*S(x,y)).",
				"(5a) exp(x) = (C(x,y) + S(x,y)*cosh(y)) * (cosh(y) - sinh(y)*C(x,y)) / (1 - sinh(y)^2*S(x,y)^2).",
				"(5b) exp(y) = (C(x,y) + S(x,y)*cosh(x)) * (cosh(x) - sinh(x)*C(x,y)) / (1 - sinh(x)^2*S(x,y)^2).",
				"(5c) C(x,y) + S(x,y) = (cosh(x) + sinh(x)*cosh(y)) * (cosh(y) + sinh(y)*cosh(x)) / (1 - sinh(x)^2*sinh(y)^2).",
				"(6a) exp(x) = (C(x,y) + S(x,y)*cosh(y)) / (cosh(y) + sinh(y)*C(x,y)).",
				"(6b) exp(y) = (C(x,y) + S(x,y)*cosh(x)) / (cosh(x) + sinh(x)*C(x,y)).",
				"(6c) C(x,y) + S(x,y) = (cosh(x) + sinh(x)*cosh(y)) / (cosh(y) - sinh(y)*cosh(x)).",
				"(6d) C(x,y) + S(x,y) = (cosh(y) + sinh(y)*cosh(x)) / (cosh(x) - sinh(x)*cosh(y)).",
				"SPECIAL ARGUMENTS.",
				"C(x, y=0) = cosh(x).",
				"C(x, y=x) = cosh(x)^2 / (1 - sinh(x)^2).",
				"C(x, y=-x) = 1."
			],
			"example": [
				"E.g.f.: C(x,y) = 1 + (1*x^2 + 2*x*y + 1*y^2)/2! + (1*x^4 + 16*x^3*y + 30*x^2*y^2 + 16*x*y^3 + 1*y^4)/4! + (1*x^6 + 96*x^5*y + 615*x^4*y^2 + 1040*x^3*y^3 + 615*x^2*y^4 + 96*x*y^5 + 1*y^6)/6! + (1*x^8 + 512*x^7*y + 10220*x^6*y^2 + 43904*x^5*y^3 + 68390*x^4*y^4 + 43904*x^3*y^5 + 10220*x^2*y^6 + 512*x*y^7 + 1*y^8)/8! + ...",
				"where C(x,y) = cosh(x)*cosh(y) / (1 - sinh(x)*sinh(y)).",
				"This irregular triangle of coefficients of x^(2*n-k)*y^k/(2*n)! in C(x,y) begins",
				"1;",
				"1, 2, 1;",
				"1, 16, 30, 16, 1;",
				"1, 96, 615, 1040, 615, 96, 1;",
				"1, 512, 10220, 43904, 68390, 43904, 10220, 512, 1;",
				"1, 2560, 147645, 1482240, 4998210, 7322112, 4998210, 1482240, 147645, 2560, 1;",
				"1, 12288, 1948650, 43310080, 291662415, 831080448, 1161583500, 831080448, 291662415, 43310080, 1948650, 12288, 1;",
				"1, 57344, 24180611, 1145417728, 14692638961, 75654971392, 190145878923, 256124504064, 190145878923, 75654971392, 14692638961, 1145417728, 24180611, 57344, 1; ...",
				"RELATED SERIES.",
				"The series S(x,y), such that C(x,y)^2 - S(x,y)^2 = 1, begins",
				"S(x,y) = (1*x + 1*y) + (1*x^3 + 6*x^2*y + 6*x*y^2 + 1*y^3)/3! + (1*x^5 + 40*x^4*y + 140*x^3*y^2 + 140*x^2*y^3 + 40*x*y^4 + 1*y^5)/5! + (1*x^7 + 224*x^6*y + 2562*x^5*y^2 + 7000*x^4*y^3 + 7000*x^3*y^4 + 2562*x^2*y^5 + 224*x*y^6 + 1*y^7)/7! + ...",
				"The e.g.f. may be written with coefficients of x^(2*n-k)*y^k/((2*n-k)!*k!), as follows:",
				"C(x,y) = 1 + (1*x^2/2! + 1*x*y + 1*y^2/2!) + (1*x^4/4! + 4*x^3*y/3! + 5*x^2*y^2/(2!*2!) + 4*x*y^3/3! + 1*y^4/4!) + (1*x^6/6! + 16*x^5*y/5! + 41*x^4*y^2/(4!*2!) + 52*x^3*y^3/(3!*3!) + 41*x^2*y^4/(2!*4!) + 16*x*y^5/5! + 1*y^6/6!) + (1*x^8/8! + 64*x^7*y/7! + 365*x^6*y^2/(6!*2!) + 784*x^5*y^3/(5!*3!) + 977*x^4*y^4/(4!*4!) + 784*x^3*y^5/(3!*5!) + 365*x^2*y^6/(2!*6!) + 64*x*y^7/7! + 1*y^8/8!) + ...",
				"these coefficients are described by triangle A322193."
			],
			"program": [
				"(PARI) {T(n, k) = my(X=x+x*O(x^(2*n-k)), Y=y+y*O(y^k));",
				"C = cosh(X)*cosh(Y)/(1 - sinh(X)*sinh(Y));",
				"(2*n)!*polcoeff(polcoeff(C, 2*n-k, x), k, y)}",
				"/* Print as a triangle */",
				"for(n=0, 10, for(k=0, 2*n, print1( T(n, k), \", \")); print(\"\"))"
			],
			"xref": [
				"Cf. A322620 (C + S), A322622 (S), A322624 (main diagonal), A322193."
			],
			"keyword": "nonn,tabf",
			"offset": "0,3",
			"author": "_Paul D. Hanna_, Dec 20 2018",
			"references": 5,
			"revision": 19,
			"time": "2019-01-01T09:59:28-05:00",
			"created": "2018-12-20T23:56:43-05:00"
		}
	]
}