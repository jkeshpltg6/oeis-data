{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A298982",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 298982,
			"data": "0,0,1,0,0,4,5,7,0,1,0,0,0,2,0,0,3,0,0,4,0,5,0,0,0,7,0,8,0,9,0,0,11,0,0,13,14,0,0,16,17,18,0,0,0,0,0,0,0,25,0,0,0,0,0,0,33,34,35,36,0,39,4,41,0,44,45,0,48,49,51,52,54,55,0,58,6,61,63,64,66,68,69,71,73,74,76,78,8,81,83,85,87,89,91,93,95,97,0,1",
			"name": "a(n) is the least k for which the most significant decimal digits of k/n (disregarding any leading zeros) are n, or 0 if no such k exists.",
			"comment": [
				"By decimal digits we mean those of the fractional part of k/n. Otherwise said, we require floor(10^m*k/n) = n for some k \u003c n and m.",
				"Indices of 0's are listed in A298981, indices of the other terms are listed in A298980.",
				"It appears that the asymptotic density of 0's is slightly below 45%: The number of 0's among a(1..10^k) is (5, 42, 461, 4553, 45423, 451315, 4506142, 45017570, ...). Is there a simple estimate for the exact value? - _M. F. Hasler_, Feb 01 2018",
				"There may be no asymptotic density: the fraction of 0's fluctuates too much.  See the linked plot."
			],
			"link": [
				"M. F. Hasler, \u003ca href=\"/A298982/b298982.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Robert Israel, \u003ca href=\"/A298982/a298982_1.png\"\u003ePlot: Fraction of 0's in a(1) to a(n)\u003c/a\u003e"
			],
			"example": [
				"a(1) = 0 since there does not exist any k such that k/1 has a decimal digit which begins with 1 (cf. comment).",
				"a(6) = 4 since 4/6 = 0.666... and its decimal digit begins with 6.",
				"a(28) = 8 since 8/28 = 0.28571428571428... even though 1/28 = 0.0357142857142857... has \"28\" as a subsequence."
			],
			"maple": [
				"f:= proc (n) local m, k;",
				"  for m from ceil(log[10](n^2)) by -1 to 1 do",
				"     k := ceil(n^2/10^m);",
				"     if n \u003c= k then return 0 end if;",
				"     if k \u003c n*(n+1)/10^m then return k end if",
				"  end do;",
				"  0",
				"end proc:",
				"map(f, [$1..200]); # _Robert Israel_, Feb 09 2018"
			],
			"mathematica": [
				"f = Compile[{{n, _Integer}}, Block[{k = 1, il = IntegerLength@ n}, While[m = 10^il*k/n; While[ IntegerLength@ Floor@ m \u003c il, m *= 10]; k \u003c n \u0026\u0026 Floor[m] != n, k++]; If[k \u003c n, k, 0]]]; Array[f, 100]"
			],
			"program": [
				"(PARI) A298982(n,k=(n^2-1)\\10^(logint(n,10)+1)+1)={k*10^(logint((n^2-(n\u003e1))\\k, 10)+1)\\n==n \u0026\u0026 return(k\\10^valuation(k,10))} \\\\ _M. F. Hasler_, Feb 01 2018"
			],
			"xref": [
				"Cf. A298232, A051626, A298980, A298981."
			],
			"keyword": "easy,nonn,base",
			"offset": "1,6",
			"author": "_Eric Angelini_, _M. F. Hasler_ and _Robert G. Wilson v_, Jan 30 2018",
			"references": 4,
			"revision": 29,
			"time": "2021-07-03T07:19:57-04:00",
			"created": "2018-01-30T21:12:57-05:00"
		}
	]
}