{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A032190",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 32190,
			"data": "0,1,1,2,2,4,4,7,9,14,18,30,40,63,93,142,210,328,492,765,1169,1810,2786,4340,6712,10461,16273,25414,39650,62074,97108,152287,238837,375166,589526,927554,1459960,2300347,3626241,5721044,9030450,14264308,22542396",
			"name": "Number of cyclic compositions of n into parts \u003e= 2.",
			"comment": [
				"Number of ways to partition n elements into pie slices each with at least 2 elements.",
				"Hackl and Prodinger (2018) indirectly refer to this sequence because their Proposition 2.1 contains the g.f. of this sequence. In the paragraph before this proposition, however, they refer to sequence A000358(n) = a(n) + 1. - _Petros Hadjicostas_, Jun 04 2019"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A032190/b032190.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Ricardo Gómez Aíza, \u003ca href=\"https://arxiv.org/abs/2009.02669\"\u003eSymbolic dynamical scales: modes, orbitals, and transversals\u003c/a\u003e, arXiv:2009.02669 [math.DS], 2020.",
				"C. G. Bower, \u003ca href=\"/transforms2.html\"\u003eTransforms (2)\u003c/a\u003e",
				"Daryl DeFord, \u003ca href=\"https://www.fq.math.ca/Papers1/52-5/DeFord.pdf\"\u003eEnumerating distinct chessboard tilings\u003c/a\u003e, Fibonacci Quart. 52 (2014), 102-116; see formula (5.3) in Theorem 5.2, p. 111.",
				"Benjamin Hackl and Helmut Prodinger, \u003ca href=\"https://arxiv.org/abs/1801.09934\"\u003eThe Necklace Process: A Generating Function Approach\u003c/a\u003e, arXiv:1801.09934 [math.PR], 2018.",
				"Benjamin Hackl and Helmut Prodinger, \u003ca href=\"https://doi.org/10.1016/j.spl.2018.06.010\"\u003eThe Necklace Process: A Generating Function Approach\u003c/a\u003e, Statistics and Probability Letters 142 (2018), 57-61.",
				"P. Hadjicostas, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL19/Hadjicostas/hadji2.html\"\u003eCyclic Compositions of a Positive Integer with Parts Avoiding an Arithmetic Sequence\u003c/a\u003e, Journal of Integer Sequences, 19 (2016), Article 16.8.2.",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=764\"\u003eEncyclopedia of Combinatorial Structures 764\u003c/a\u003e",
				"\u003ca href=\"/index/Ne#necklaces\"\u003eIndex entries for sequences related to necklaces\u003c/a\u003e"
			],
			"formula": [
				"\"CIK\" (necklace, indistinct, unlabeled) transform of 0, 1, 1, 1...",
				"From _Petros Hadjicostas_, Sep 10 2017: (Start)",
				"For all the formulas below, assume n \u003e= 1. Here, phi(n) = A000010(n) is Euler's totient function.",
				"a(n) = (1/n) * Sum_{d|n} b(d)*phi(n/d), where b(n) = A001610(n-1).",
				"a(n) = (1/n) * Sum_{d|n} phi(n/d)*(Fibonacci(d-1) + Fibonacci(d+1) - 1) (because of the equation a(n) = A000358(n) - 1 stated in the CROSSREFS section below).",
				"G.f.: -x/(1-x) + Sum_(k\u003e=1} phi(k)/k * log(1/(1-B(x^k))) where B(x) = x*(1+x). (This is a modification of a formula due to _Joerg Arndt_.)",
				"G.f.: Sum_{k\u003e=1} phi(k)/k * log((1-x^k)/(1-B(x^k))), which agrees with the one in the Encyclopedia of Combinatorial Structures, #764, above. (We have Sum_{n\u003e=1} (phi(n)/n)*log(1-x^n) = -x/(1-x), which follows from the Lambert series Sum_{n\u003e=1} phi(n)*x^n/(1-x^n) = x/(1-x)^2.)",
				"Sum_{d|n} a(d)*d = n*Sum_{d|n} b(d)/d, where b(n) = A001610(n-1).",
				"(End)",
				"a(n) = Sum_{1 \u003c= i \u003c= ceiling((n-1)/2)} [ (1/(n - i)) * Sum_{d|gcd(i, n-i)} phi(d) * binomial((n - i)/d, i/d) ]. (This is a slight variation of DeFord's formula for the number of distinct Lucas tilings of a 1 X n bracelet up to symmetry, where we exclude the case with i = 0 dominoes.) - _Petros Hadjicostas_, Jun 07 2019"
			],
			"maple": [
				"# formula (5.3) of Daryl Deford for \"Number of distinct Lucas tilings of a 1 X n",
				"# bracelet up to symmetry\" in \"Enumerating distinct chessboard tilings\"",
				"A032190 := proc(n)",
				"    local a,i,d ;",
				"    a := 0 ;",
				"    for i from 0 to ceil((n-1)/2) do",
				"        for d in numtheory[divisors](i) do",
				"            if modp(igcd(i,n-i),d) = 0 then",
				"                a := a+(numtheory[phi](d)*binomial((n-i)/d,i/d))/(n-i) ;",
				"            end if;",
				"        end do:",
				"    end do:",
				"    a ;",
				"end proc:",
				"seq(A032190(n),n=1..60) ; # _R. J. Mathar_, Nov 27 2014"
			],
			"mathematica": [
				"nn=40;Apply[Plus,Table[CoefficientList[Series[CycleIndex[CyclicGroup[n],s]/.Table[s[i]-\u003ex^(2i)/(1-x^i),{i,1,n}],{x,0,nn}],x],{n,1,nn/2}]] (* _Geoffrey Critzer_, Aug 10 2013 *)",
				"A032190[n_] := Module[{a=0, i, d, j, dd}, For[i=1, i \u003c= Ceiling[(n-1)/2], i++, For[dd = Divisors[i]; j=1, j \u003c= Length[dd], j++, d=dd[[j]]; If[Mod[GCD[i, n-i], d] == 0, a = a + (EulerPhi[d]*Binomial[(n-i)/d, i/d])/(n-i)]]]; a]; Table[A032190[n], {n, 1, 60}] (* _Jean-François Alcover_, Nov 27 2014, after _R. J. Mathar_ *)"
			],
			"xref": [
				"a(n) = A000358(n) - 1. Cf. A008965."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Christian G. Bower_",
			"ext": [
				"Better name from _Geoffrey Critzer_, Aug 10 2013"
			],
			"references": 4,
			"revision": 73,
			"time": "2020-12-09T19:57:20-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}