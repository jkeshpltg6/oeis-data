{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A053253",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 53253,
			"data": "1,2,3,4,6,8,10,14,18,22,29,36,44,56,68,82,101,122,146,176,210,248,296,350,410,484,566,660,772,896,1038,1204,1391,1602,1846,2120,2428,2784,3182,3628,4138,4708,5347,6072,6880,7784,8804,9940,11208,12630",
			"name": "Coefficients of the '3rd order' mock theta function omega(q)",
			"comment": [
				"Empirical: a(n) is the number of integer partitions mu of 2n+1 such that the diagram of mu has an odd number of cells in each row and in each column. - _John M. Campbell_, Apr 24 2020"
			],
			"reference": [
				"Srinivasa Ramanujan, The Lost Notebook and Other Unpublished Papers, Narosa Publishing House, New Delhi, 1988, pp. 15, 17, 31."
			],
			"link": [
				"Vaclav Kotesovec, \u003ca href=\"/A053253/b053253.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e (terms 0..1000 from Seiichi Manyama)",
				"Leila A. Dragonette, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9947-1952-0049927-8\"\u003eSome asymptotic formulas for the mock theta series of Ramanujan\u003c/a\u003e, Trans. Amer. Math. Soc., 72 (1952) 474-500.",
				"John F. R. Duncan, Michael J. Griffin and Ken Ono, \u003ca href=\"http://arxiv.org/abs/1503.01472\"\u003eProof of the Umbral Moonshine Conjecture\u003c/a\u003e, arXiv:1503.01472 [math.RT], 2015.",
				"George N. Watson, \u003ca href=\"https://doi.org/10.1112/jlms/s1-11.1.55\"\u003eThe final problem: an account of the mock theta functions\u003c/a\u003e, J. London Math. Soc., 11 (1936) 55-80."
			],
			"formula": [
				"G.f.: omega(q) = Sum_{n\u003e=0} q^(2*n*(n+1))/((1-q)*(1-q^3)*...*(1-q^(2*n+1)))^2.",
				"G.f.: Sum_{k\u003e=0} x^k/((1-x)(1-x^3)...(1-x^(2k+1))). - _Michael Somos_, Aug 18 2006",
				"G.f.: (1 - G(0) )/(1-x) where G(k) = 1 - 1/(1-x^(2*k+1))/(1-x/(x-1/G(k+1) )); (continued fraction). - _Sergei N. Gladkovskii_, Jan 18 2013",
				"a(n) ~ exp(Pi*sqrt(n/3)) / (4*sqrt(n)). - _Vaclav Kotesovec_, Jun 10 2019"
			],
			"mathematica": [
				"Series[Sum[q^(2n(n+1))/Product[1-q^(2k+1), {k, 0, n}]^2, {n, 0, 6}], {q, 0, 100}]"
			],
			"program": [
				"(PARI) {a(n)=local(A); if(n\u003c0, 0, A=1+x*O(x^n); polcoeff( sum(k=0, (sqrtint(2*n+1)-1)\\2, A*=(x^(4*k)/(1-x^(2*k+1))^2 +x*O(x^(n-2*(k^2-k))))), n))} /* _Michael Somos_, Aug 18 2006 */",
				"(PARI) {a(n)=local(A); if(n\u003c0, 0, n++; A=1+x*O(x^n); polcoeff( sum(k=0, n-1, A*=(x/(1-x^(2*k+1)) +x*O(x^(n-k)))), n))} /* _Michael Somos_, Aug 18 2006 */"
			],
			"xref": [
				"Other '3rd order' mock theta functions are at A000025, A053250, A053251, A053252, A053254, A053255, A261401.",
				"Cf. A095913(n)=a(n-3).",
				"Cf. A259094."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Dean Hickerson_, Dec 19 1999",
			"references": 14,
			"revision": 41,
			"time": "2020-05-13T10:37:55-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}