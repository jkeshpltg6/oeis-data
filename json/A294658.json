{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A294658",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 294658,
			"data": "1,1,2,0,2,1,1,4,3,5,1,0,2,2,2,1,1,4,3,1,1,1,2,2,2,1,1,2,2,1,1,1,1,2,2,1,1,3,3,3,1,1,1,2,3,1,1,2,4,3,1,1,2,2,2,1,1,4,1,4,1,1,2,2,2,1,1,5,1,3,1,1,2,3,2,1,1,4,2,2,1,1,2,2,1,1",
			"name": "Number of steps required to reach either 5 or 13, starting with n, when iterating the map A125256: x -\u003e smallest odd prime divisor of n^2+1; or a(n) = -1 in case 5 is never reached.",
			"comment": [
				"The orbit or trajectory under A125256 appears to end in the cycle 5 -\u003e 13 -\u003e 5 -\u003e etc. for any initial value n.",
				"Sequence A294656 gives the size of the complete orbit of n under the map A125256, including the two elements 5 and 13 of the terminating cycle. Thus a(n) is 2 less than A294656(n) for all n. This is confirmed by careful examination of special cases - assuming, of course, that all trajectories end in the cycle (5, 13)."
			],
			"link": [
				"Ray Chandler, \u003ca href=\"/A294658/b294658.txt\"\u003eTable of n, a(n) for n = 2..20001\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A294656(n) - 2."
			],
			"example": [
				"For n = 1 the map A125256 is not defined.",
				"a(2) = 1 because under A125256, 2 -\u003e 2^2+1 = 5 (= its smallest odd prime factor), so 5 is reached after just a(2) = 1 iteration of this map.",
				"a(3) = 1 because A125256(3) = 5, least odd prime factor of 3^2+1 = 10 = 2*5, so here again 5 is reached after just a(2) = 1 iteration of A125256.",
				"a(4) = 2 because A125256(4) = 4^2 + 1 = 17, and A125256(17) = 5 = least odd prime factor of 17^2 + 1 = 289 + 1 = 2*5*29, so 5 is reached after a(4) = 2 iterations of A125256.",
				"a(5) = a(13) = 0 because for these initial values 5 and 13, no iteration is needed until either 5 or 13 is reached."
			],
			"program": [
				"(PARI) A294658(n)=for(k=0,oo,bittest(8224,n)\u0026\u0026return(k);n=A125256(n)) \\\\ 8224 = 2^5 + 2^13. One could add 2^0 + 2^1 = 3 to avoid an error message for initial values 0 and 1, for which A125256 is not defined."
			],
			"xref": [
				"Cf. A125256, A294656, A294657."
			],
			"keyword": "nonn",
			"offset": "2,3",
			"author": "_M. F. Hasler_, Nov 06 2017",
			"references": 3,
			"revision": 10,
			"time": "2017-11-06T12:06:17-05:00",
			"created": "2017-11-06T11:31:22-05:00"
		}
	]
}