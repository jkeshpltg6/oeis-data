{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A004189",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 4189,
			"data": "0,1,10,99,980,9701,96030,950599,9409960,93149001,922080050,9127651499,90354434940,894416697901,8853812544070,87643708742799,867583274883920,8588189040096401,85014307126080090,841554882220704499",
			"name": "a(n) = 10*a(n-1) - a(n-2); a(0) = 0, a(1) = 1.",
			"comment": [
				"Indices of square numbers which are also generalized pentagonal numbers.",
				"If t(n) denotes the n-th triangular number, t(A105038(n))=a(n)*a(n+1). - Robert Phillips (bobanne(AT)bellsouth.net), May 25 2008",
				"The n-th term is a(n) = ((5+sqrt(24))^n - (5-sqrt(24))^n)/(2*sqrt(24)). - _Sture Sjöstedt_, May 31 2009",
				"Number of units of a(n) belongs to a periodic sequence: 0, 1, 0, 9. We conclude that a(n) and a(n+4) have the same number of units. - _Mohamed Bouhamida_, Sep 05 2009",
				"For n \u003e= 2, a(n) equals the permanent of the (n-1) X (n-1) tridiagonal matrix with 10's along the main diagonal, and i's along the superdiagonal and the subdiagonal (i is the imaginary unit). - _John M. Campbell_, Jul 08 2011",
				"a(n) and b(n) (A001079) are the nonnegative proper solutions of the Pell equation b(n)^2 - 6*(2*a(n))^2 = +1. See the cross reference to A001079 below. - _Wolfdieter Lang_, Jun 26 2013",
				"For n \u003e= 1, a(n) equals the number of 01-avoiding words of length n-1 on alphabet {0,1,...,9}. - _Milan Janjic_, Jan 25 2015",
				"For n \u003e 1, this also gives the number of (n-1)-decimal-digit numbers which avoid a particular two-digit number with distinct digits. For example, there are a(5) = 9701 4-digit numbers which do not include \"39\" as a substring; see Wikipedia link. - _Charles R Greathouse IV_, Jan 14 2016"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A004189/b004189.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"K. Andersen, L. Carbone, and D. Penta, \u003ca href=\"https://pdfs.semanticscholar.org/8f0c/c3e68d388185129a56ed73b5d21224659300.pdf\"\u003eKac-Moody Fibonacci sequences, hyperbolic golden ratios, and real quadratic fields\u003c/a\u003e, Journal of Number Theory and Combinatorics, Vol 2, No. 3 pp 245-278, 2011. See Section 9.",
				"D. Birmajer, J. B. Gil, and M. D. Weiner, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL19/Gil/gil6.html\"\u003en the Enumeration of Restricted Words over a Finite Alphabet\u003c/a\u003e, J. Int. Seq. 19 (2016) # 16.1.3, Example 12.",
				"E. I. Emerson, \u003ca href=\"http://www.fq.math.ca/Scanned/7-3/emerson.pdf\"\u003eRecurrent Sequences in the Equation DQ^2=R^2+N\u003c/a\u003e, Fib. Quart., 7 (1969), pp. 231-242.",
				"D. Fortin, \u003ca href=\"http://ijpam.eu/contents/2012-77-1/11/11.pdf\"\u003eB-spline Toeplitz inverse under corner perturbations\u003c/a\u003e, International Journal of Pure and Applied Mathematics, Volume 77, No. 1, 2012, 107-118. - From _N. J. A. Sloane_, Oct 22 2012",
				"A. F. Horadam, \u003ca href=\"http://www.fq.math.ca/Scanned/5-5/horadam.pdf\"\u003eSpecial properties of the sequence W_n(a,b; p,q)\u003c/a\u003e, Fib. Quart., 5.5 (1967), 424-434. Case a=0,b=1; p=10, q=-1.",
				"M. Janjic, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL18/Janjic/janjic63.html\"\u003eOn Linear Recurrence Equations Arising from Compositions of Positive Integers\u003c/a\u003e, Journal of Integer Sequences, Vol. 18 (2015), Article 15.4.7.",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"Pablo Lam-Estrada, Myriam Rosalía Maldonado-Ramírez, José Luis López-Bonilla, and Fausto Jarquín-Zárate, \u003ca href=\"https://arxiv.org/abs/1904.13002\"\u003eThe sequences of Fibonacci and Lucas for each real quadratic fields Q(Sqrt(d))\u003c/a\u003e, arXiv:1904.13002 [math.NT], 2019.",
				"Wolfdieter Lang, \u003ca href=\"http://www.fq.math.ca/Scanned/38-5/lang.pdf\"\u003eOn polynomials related to powers of the generating function of Catalan's numbers\u003c/a\u003e, Fib. Quart. 38,5 (2000) 408-419; Eq.(44), lhs, m=12.",
				"Roger B. Nelson, \u003ca href=\"http://www.jstor.org/stable/10.4169/math.mag.89.3.159\"\u003eMulti-Polygonal Numbers\u003c/a\u003e, Mathematics Magazine, Vol. 89, No. 3 (June 2016), pp. 159-164.",
				"Robert Phillips, \u003ca href=\"https://web.archive.org/web/20100713033314/http://www.usca.edu/math/~mathdept/bobp/pdf/polgonal.pdf\"\u003ePolynomials of the form 1+4ke+4ke^2\u003c/a\u003e, 2008.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Curse_of_39\"\u003eCurse of 39\u003c/a\u003e",
				"Jianqiang Zhao, \u003ca href=\"http://arxiv.org/abs/1507.04917\"\u003eFinite Multiple zeta Values and Finite Euler Sums\u003c/a\u003e, arXiv:1507.04917 [math.NT], 2015",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (10,-1)."
			],
			"formula": [
				"a(n) = S(2*n-1, sqrt(12))/sqrt(12) = S(n-1, 10); S(n, x) := U(n, x/2), Chebyshev polynomials of 2nd kind, A049310. S(-1, x) := 0.",
				"A001079(n) = sqrt{24*[a(n)^2]+1}, that is a(n) = sqrt((A001079(n)^2-1)/24).",
				"From _Barry E. Williams_, Aug 18 2000: (Start)",
				"a(n) = ( (5+2*sqrt(6))^n - (5-2*sqrt(6))^n )/(4*sqrt(6)).",
				"G.f.: x/(1-10*x+x^2). (End)",
				"a(-n) = -a(n). - _Michael Somos_, Sep 05 2006",
				"From _Mohamed Bouhamida_, May 26 2007: (Start)",
				"a(n) = 9*(a(n-1) + a(n-2)) - a(n-3).",
				"a(n) = 11*(a(n-1) - a(n-2)) + a(n-3).",
				"a(n) = 10*a(n-1) - a(n-2). (End)",
				"a(n+1) = Sum_{k=0..n} A101950(n,k)*9^k. - _Philippe Deléham_, Feb 10 2012",
				"Product {n \u003e= 1} (1 + 1/a(n)) = 1/2*(2 + sqrt(6)). - _Peter Bala_, Dec 23 2012",
				"Product {n \u003e= 2} (1 - 1/a(n)) = 1/5*(2 + sqrt(6)). - _Peter Bala_, Dec 23 2012",
				"a(n) = (A054320(n-1) + A072256(n))/2. - _Richard R. Forberg_, Nov 21 2013",
				"a(2*n - 1) = A046173(n)."
			],
			"example": [
				"a(2)=10 and (3(-8)^2-(-8))/2=10^2, a(3)=99 and (3(81)^2-(81))/2=99^2. - _Michael Somos_, Sep 05 2006",
				"G.f. = x + 10*x^2 + 99*x^3 + 980*x^4 + 9701*x^5 + 96030*x^6 + ..."
			],
			"maple": [
				"A004189 := proc(n)",
				"    option remember;",
				"    if n \u003c= 1 then",
				"        n ;",
				"    else",
				"        10*procname(n-1)-procname(n-2) ;",
				"    end if;",
				"end proc:",
				"seq(A004189(n),n=0..20) ; # _R. J. Mathar_, Apr 30 2017",
				"seq( simplify(ChebyshevU(n-1, 5)), n=0..20); # _G. C. Greubel_, Dec 23 2019"
			],
			"mathematica": [
				"Table[GegenbauerC[n-1,1,5], {n,0,30}] (* _Vladimir Joseph Stephan Orlovsky_, Sep 11 2008; modified by _G. C. Greubel_, Jun 06 2019 *)",
				"LinearRecurrence[{10, -1}, {0, 1}, 20] (* _Jean-François Alcover_, Nov 15 2017 *)",
				"ChebyshevU[Range[21] -2, 5] (* _G. C. Greubel_, Dec 23 2019 *)"
			],
			"program": [
				"(PARI) {a(n) = subst(poltchebi(n+1) - 5*poltchebi(n), 'x, 5) / 24}; /* _Michael Somos_, Sep 05 2006 */",
				"(PARI) a(n)=([9,1;8,1]^(n-1)*[1;1])[1,1] \\\\ _Charles R Greathouse IV_, Jan 14 2016",
				"(PARI) vector(21, n, polchebyshev(n-2, 2, 5) ) \\\\ _G. C. Greubel_, Dec 23 2019",
				"(Sage) [lucas_number1(n,10,1) for n in range(22)] # _Zerinvary Lajos_, Jun 25 2008",
				"(Sage) [chebyshev_U(n-1,5) for n in (0..20)] # _G. C. Greubel_, Dec 23 2019",
				"(MAGMA) [ n eq 1 select 0 else n eq 2 select 1 else 10*Self(n-1)-Self(n-2): n in [1..20] ]; // _Vincenzo Librandi_, Aug 19 2011",
				"(GAP) m:=5;; a:=[0,1];; for n in [3..20] do a[n]:=2*m*a[n-1]-a[n-2]; od; a; # _G. C. Greubel_, Dec 23 2019"
			],
			"xref": [
				"Cf. A001109, A001353, A001906, A004187, A004254, A018913, A046173, A108741 (squares).",
				"Chebyshev sequence U(n, m): A000027 (m=1), A001353 (m=2), A001109 (m=3), A001090 (m=4), this sequence (m=5), A004191 (m=6), A007655 (m=7), A077412 (m=8), A049660 (m=9), A075843 (m=10), A077421 (m=11), A077423 (m=12), A097309 (m=13), A097311 (m=14), A097313 (m=15), A029548 (m=16), A029547 (m=17), A144128 (m=18), A078987 (m=19), A097316 (m=33).",
				"Cf. A323182."
			],
			"keyword": "easy,nonn",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"references": 57,
			"revision": 125,
			"time": "2021-12-23T06:09:36-05:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}