{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A164300",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 164300,
			"data": "1,12,82,488,2756,15216,83144,452128,2453008,13294272,72012064,389976704,2111644736,11433484032,61904845952,335169991168,1814692086016,9825156811776,53195565289984,288012326955008",
			"name": "a(n) = ((1+4*sqrt(2))*(4+sqrt(2))^n + (1-4*sqrt(2))*(4-sqrt(2))^n)/2.",
			"comment": [
				"Binomial transform of A164299. Fourth binomial transform of A164587. Inverse binomial transform of A164301.",
				"This sequence is part of a class of sequences defined by the recurrence a(n,m) = 2*(m+1)*a(n-1,m) - ((m+1)^2 -2)*a(n-2,m) with a(0) = 1 and a(1) = m+9. The generating function is Sum_{n\u003e=0} a(n,m)*x^n = (1 - (m-7)*x)/(1 - 2*(m+1)*x + ((m+1)^2 - 2)*x^2) and has a series expansion in terms of Pell-Lucas numbers defined by a(n, m) = (1/2)*Sum_{k=0..n} binomial(n,k)*m^(n-k)*(5*Q(k) + 4*Q(k-1)). - _G. C. Greubel_, Mar 12 2021"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A164300/b164300.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (8,-14)."
			],
			"formula": [
				"a(n) = 8*a(n-1) - 14*a(n-2) for n \u003e 1; a(0) = 1, a(1) = 12.",
				"G.f.: (1+4*x)/(1-8*x+14*x^2).",
				"E.g.f.: (4*sqrt(2)*sinh(sqrt(2)*x) + cosh(sqrt(2)*x))*exp(4*x). - _Ilya Gutkovskiy_, Jun 24 2016",
				"From _G. C. Greubel_, Mar 12 2021: (Start)",
				"a(n) = 2*A083879(n) + 8*A081180(n).",
				"a(n) = (1/2)*Sum_{k=0..n} binomial(n,k)*3^(n-k)*(5*Q(k) + 4*Q(k-1)), where Q(n) = Pell-Lucas(n) = A002203(n). (End)"
			],
			"mathematica": [
				"LinearRecurrence[{8,-14},{1,12},30] (* _Harvey P. Dale_, Apr 13 2012 *)"
			],
			"program": [
				"(MAGMA) Z\u003cx\u003e:=PolynomialRing(Integers()); N\u003cr\u003e:=NumberField(x^2-2); S:=[ ((1+4*r)*(4+r)^n+(1-4*r)*(4-r)^n)/2: n in [0..19] ]; [ Integers()!S[j]: j in [1..#S] ]; // _Klaus Brockhaus_, Aug 17 2009",
				"(PARI) my(x='x+O('x^50)); Vec((1+4*x)/(1-8*x+14*x^2)) \\\\ _G. C. Greubel_, Sep 13 2017",
				"(Sage) [( (1+4*x)/(1-8*x+14*x^2) ).series(x,n+1).list()[n] for n in (0..30)] # _G. C. Greubel_, Mar 12 2021"
			],
			"xref": [
				"Sequences in the class a(n, m): A164298 (m=1), A164299 (m=2), this sequence (m=3), A164301 (m=4), A164598 (m=5), A164599 (m=6), A081185 (m=7), A164600 (m=8).",
				"Cf. A081180, A083879, A164587."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "Al Hakanson (hawkuu(AT)gmail.com), Aug 12 2009",
			"ext": [
				"Edited and extended beyond a(5) by _Klaus Brockhaus_, Aug 17 2009"
			],
			"references": 8,
			"revision": 28,
			"time": "2021-03-12T23:48:25-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}