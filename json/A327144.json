{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A327144",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 327144,
			"data": "0,1,1,0,1,1,1,1,1,0,0,0,0,0,0,0,1,1,0,0,1,1,1,1,1,1,0,0,1,1,1,1,1,0,1,0,1,1,1,1,1,0,1,0,1,1,1,1,1,1,1,1,2,2,2,2,1,1,1,1,2,2,2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2",
			"name": "Spanning edge-connectivity of the set-system with BII-number n.",
			"comment": [
				"A binary index of n is any position of a 1 in its reversed binary expansion. The binary indices of n are row n of A048793. We define the set-system with BII-number n to be obtained by taking the binary indices of each binary index of n. Every set-system (finite set of finite nonempty sets) has a different BII-number. For example, 18 has reversed binary expansion (0,1,0,0,1), and since the binary indices of 2 and 5 are {2} and {1,3} respectively, the BII-number of {{2},{1,3}} is 18. Elements of a set-system are sometimes called edges.",
				"The spanning edge-connectivity of a set-system is the minimum number of edges that must be removed (without removing incident vertices) to obtain a set-system that is disconnected or covers fewer vertices."
			],
			"example": [
				"Positions of first appearances of each integer together with the corresponding set-systems:",
				"     0: {}",
				"     1: {{1}}",
				"    52: {{1,2},{1,3},{2,3}}",
				"   116: {{1,2},{1,3},{2,3},{1,2,3}}",
				"  3952: {{1,3},{2,3},{1,4},{2,4},{3,4},{1,2,3},{1,2,4}}",
				"  8052: {{1,2},{1,3},{2,3},{1,4},{2,4},{3,4},{1,2,3},{1,2,4},{1,3,4}}"
			],
			"mathematica": [
				"bpe[n_]:=Join@@Position[Reverse[IntegerDigits[n,2]],1];",
				"csm[s_]:=With[{c=Select[Tuples[Range[Length[s]],2],And[OrderedQ[#],UnsameQ@@#,Length[Intersection@@s[[#]]]\u003e0]\u0026]},If[c=={},s,csm[Sort[Append[Delete[s,List/@c[[1]]],Union@@s[[c[[1]]]]]]]]];",
				"spanEdgeConn[vts_,eds_]:=Length[eds]-Max@@Length/@Select[Subsets[eds],Union@@#!=vts||Length[csm[#]]!=1\u0026];",
				"Table[spanEdgeConn[Union@@bpe/@bpe[n],bpe/@bpe[n]],{n,0,100}]"
			],
			"xref": [
				"Dominated by A327103.",
				"The same for cut-connectivity is A326786.",
				"The same for non-spanning edge-connectivity is A326787.",
				"The same for vertex-connectivity is A327051.",
				"Positions of 1's are A327111.",
				"Positions of 2's are A327108.",
				"Positions of first appearance of each integer are A327147.",
				"Cf. A000120, A048793, A070939, A322338, A323818, A326031, A327041, A327069, A327076, A327130, A327145."
			],
			"keyword": "nonn",
			"offset": "0,53",
			"author": "_Gus Wiseman_, Aug 31 2019",
			"references": 20,
			"revision": 6,
			"time": "2019-09-01T08:41:53-04:00",
			"created": "2019-09-01T08:41:53-04:00"
		}
	]
}