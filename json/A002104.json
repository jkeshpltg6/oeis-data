{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002104",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2104,
			"id": "M2749 N1105",
			"data": "0,1,3,8,24,89,415,2372,16072,125673,1112083,10976184,119481296,1421542641,18348340127,255323504932,3809950977008,60683990530225,1027542662934915,18430998766219336,349096664728623336",
			"name": "Logarithmic numbers.",
			"comment": [
				"Prime p divides a(p+1). - _Alexander Adamchuk_, Jul 05 2006",
				"Also number of lists of elements from {1,..,n} with (1st element) = (smallest element), where a list means an ordered subset (cf. A000262), see also Haskell program. - _Reinhard Zumkeller_, Oct 26 2010",
				"a(n+1) = p_n(-1)  where p_n(x) is the unique degree-n polynomial such that p_n(k) = A133942(k) for k = 0, 1, ..., n. - _Michael Somos_, Apr 30 2012",
				"a(n) = A006231(n) + n. - _Geoffrey Critzer_, Oct 04 2012"
			],
			"reference": [
				"J. M. Gandhi, On logarithmic numbers, Math. Student, 31 (1963), 73-83.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A002104/b002104.txt\"\u003eTable of n, a(n) for n = 0..100\u003c/a\u003e (corrected by Michel Marcus, Jan 19 2019)",
				"J. M. Gandhi, \u003ca href=\"/A002741/a002741.pdf\"\u003eOn logarithmic numbers\u003c/a\u003e, Math. Student, 31 (1963), 73-83. [Annotated scanned copy]",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=116\"\u003eEncyclopedia of Combinatorial Structures 116\u003c/a\u003e",
				"J. C. Tiernan, \u003ca href=\"http://dx.doi.org/10.1145/362814.362819\"\u003eAn efficient search algorithm to find the elementary circuits of a graph\u003c/a\u003e, Commun. ACM, 13 (1970), 722-726.",
				"\u003ca href=\"/index/Lo#logarithmic\"\u003eIndex entries for sequences related to logarithmic numbers\u003c/a\u003e"
			],
			"formula": [
				"E.g.f.: -log(1 - x) * exp(x).",
				"a(n) = Sum_{k=1..n} Sum_{i=0..n-k} (n-k)!/i!.",
				"a(n) = Sum_{k=1..n} n(n-1)...(n-k+1)/k = A006231(n) + n - Avi Peretz (njk(AT)netvision.net.il), Mar 24 2001",
				"a(n+1) - a(n) = A000522(n).",
				"a(n) = sum{k=0..n-1, binomial(n, k)*(n-k-1)!}, row sums of A111492. - _Paul Barry_, Aug 26 2004",
				"a(n) = Sum[Sum[m!/k!,{k,0,m}],{m,0,n-1}]. a(n) = Sum[A000522(m),{m,0,n-1}]. - _Alexander Adamchuk_, Jul 05 2006",
				"For n \u003e 1, the arithmetic mean of the first n terms is a(n-1) + 1. - _Franklin T. Adams-Watters_, May 20 2010",
				"a(n) = n * 3F1((1,1,1-n); (2); -1). - _Jean-François Alcover_, Mar 29 2011",
				"Conjecture: a(n) +(-n-1)*a(n-1) +2*(n-1)*a(n-2) +(-n+2)*a(n-3)=0. - _R. J. Mathar_, Dec 02 2012",
				"From _Emanuele Munarini_, Dec 16 2017: (Start)",
				"The generating series A(x) = -exp(x)*log(1-x) satisfies the differential equations:",
				"  (1-x)*A'(x) - (1-x)*A(x) = exp(x)",
				"  (1-x)*A''(x) - (3-2*x)*A'(x) + (2-x)*A(x) = 0.",
				"  From the first one, we have the recurrence reported below by R. R. Forberg. From the second one, we have the recurrence conjectured above. (End)",
				"G.f.: conjecture: T(0)*x/(1-2*x)/(1-x), where T(k) = 1 - x^2*(k+1)^2/(x^2*(k+1)^2 - (1 - 2*x*(k+1))*(1 - 2*x*(k+2))/T(k+1) ); (continued fraction). - _Sergei N. Gladkovskii_, Nov 18 2013",
				"a(n) ~ exp(1)*(n-1)!. - _Vaclav Kotesovec_, Mar 10 2014",
				"a(n) = n*a(n-1) - (n-1)*a(n-2) + 1, a(0) = 0, a(1) = 1. - _Richard R. Forberg_, Dec 15 2014",
				"a(n) = A007526(n) + A006231(n+1) - A030297(n). - _Anton Zakharov_, Sep 05 2016",
				"0 =  +a(n)*(+a(n+1) -4*a(n+2) +4*a(n+3) -a(n+4)) +a(n+1)*(+2*a(n+2) -5*a(n+3) +2*a(n+4)) +a(n+2)*(+2*a(n+2) -a(n+3) -a(n+4)) +a(n+3)*(+a(n+3)) for all n\u003e=0. - _Michael Somos_, May 08 2019"
			],
			"example": [
				"From _Reinhard Zumkeller_, Oct 26 2010: (Start)",
				"a(3) = #{[1], [1,2], [1,2,3], [1,3], [1,3,2], [2], [2,3], [3]} = 8;",
				"a(4) = #{[1], [1,2], [1,2,3], [1,2,3,4], [1,2,4], [1,2,4,3], [1,3], [1,3,2], [1,3,2,4], [1,3,4], [1,3,4,2], [1,4], [1,4,2], [1,4,2,3], [1,4,3], [1,4,3,2], [2], [2,3], [2,3,4], [2,4], [2,4,3], [3], [3,4], [4]} = 24. (End)",
				"G.f. = x + 3*x^2 + 8*x^3 + 24*x^4 + 89*x^5 + 415*x^6 + 2372*x^7 + ..."
			],
			"mathematica": [
				"Table[Sum[Sum[m!/k!,{k,0,m}],{m,0,n-1}],{n,1,30}] (* _Alexander Adamchuk_, Jul 05 2006 *)",
				"a[n_] = n*(HypergeometricPFQ[{1, 1, 1-n}, {2}, -1]); Table[a[n], {n, 1, 20}] (* _Jean-François Alcover_, Mar 29 2011 *)"
			],
			"program": [
				"(Haskell)",
				"import Data.List (subsequences, permutations)",
				"a002104 = length . filter (\\xs -\u003e head xs == minimum xs) .",
				"                   tail . choices . enumFromTo 1",
				"   where choices = concat . map permutations . subsequences",
				"-- _Reinhard Zumkeller_, Feb 21 2012, Oct 25 2010",
				"(PARI) x='x+O('x^99); concat([0], Vec(serlaplace(-log(1-x)*exp(x)))) \\\\ _Altug Alkan_, Dec 17 2017",
				"(PARI) {a(n) = sum(k=0, n-1, binomial(n, k) * (n-k-1)!)}; /* _Michael Somos_, May 08 2019 */"
			],
			"xref": [
				"Cf. A001338, A006231, A007526, A030297, A133942."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from Larry Reeves (larryr(AT)acm.org), Mar 27 2001"
			],
			"references": 28,
			"revision": 100,
			"time": "2019-05-08T14:44:12-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}