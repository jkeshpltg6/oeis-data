{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A225816",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 225816,
			"data": "1,1,1,1,1,1,1,2,1,1,1,6,4,1,1,1,24,36,8,1,1,1,120,576,216,16,1,1,1,720,14400,13824,1296,32,1,1,1,5040,518400,1728000,331776,7776,64,1,1,1,40320,25401600,373248000,207360000,7962624,46656,128,1,1",
			"name": "Square array A(n,k) = (k!)^n, n\u003e=0, k\u003e=0, read by antidiagonals.",
			"comment": [
				"A(n,k) is the determinant of the k X k matrix M = [Stirling2(n+i,j)] for 1\u003c=i,j\u003c=k.  A(2,3) = det([1,3,1; 1,7,6; 1,15,25]) = 36.",
				"A(n,k) is the determinant of the symmetric k X k matrix M = [sigma_n(gcd(i,j))] for 1\u003c=i,j\u003c=k.  A(2,3) = det([1,1,1; 1,5,1; 1,1,10]) = 36.",
				"A(n,k) is (-1)^(n*k) times the determinant of the n X n matrix M = [Stirling1(k+i,j)] for 1\u003c=i,j\u003c=n.  A(2,3) = (-1)^(2+3) * det([-6,11; 24,-50]) = 36.",
				"A(n,k) is the number of lattice paths from {n}^k to {0}^k using steps that decrement one component by 1 such that for each point (p_1,p_2,...,p_k) we have abs(p_i-p_j) \u003c= 1 for 1\u003c=i,j\u003c=k.  A(2,3) = 36:",
				"          (1,2,2)-(1,1,2)         (0,1,1)-(0,0,1)",
				"         /       X       \\       /       X       \\",
				"  (2,2,2)-(2,1,2) (1,2,1)-(1,1,1)-(1,0,1) (0,1,0)-(0,0,0).",
				"         \\       X       /       \\       X       /",
				"          (2,2,1) (2,1,1)         (1,1,0) (1,0,0)",
				"A(n,k) is the number of set partitions of [k*(n+1)] into k blocks of size n+1 such that the elements of each block are distinct mod n+1.  A(2,3) = 36: 123|456|789, 126|345|789, ..., 189|234|567, 189|246|357."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A225816/b225816.txt\"\u003eAntidiagonals n = 0..36, flattened\u003c/a\u003e"
			],
			"formula": [
				"A(n,k) = (k!)^n.",
				"A(n,k) = k^n * A(n,k-1) for k\u003e0, A(n,0) = 1.",
				"A(n,k) = k!  * A(n-1,k) for n\u003e0, A(0,k) = 1.",
				"G.f. of column k: 1/(1-k!*x)."
			],
			"example": [
				"Square array A(n,k) begins:",
				"  1, 1,  1,    1,       1,           1, ...",
				"  1, 1,  2,    6,      24,         120, ...",
				"  1, 1,  4,   36,     576,       14400, ...",
				"  1, 1,  8,  216,   13824,     1728000, ...",
				"  1, 1, 16, 1296,  331776,   207360000, ...",
				"  1, 1, 32, 7776, 7962624, 24883200000, ..."
			],
			"maple": [
				"A:= (n, k)-\u003e k!^n:",
				"seq(seq(A(n,d-n), n=0..d), d=0..12);"
			],
			"xref": [
				"Columns k=0+1, 2-4 give: A000012, A000079, A000400, A009968.",
				"Rows n=0-4 give: A000012, A000142, A001044, A000442, A134375.",
				"Main diagonal gives: A036740.",
				"Cf. A008275, A048994, A008277, A048993, A003989, A109004, A109974."
			],
			"keyword": "nonn,tabl",
			"offset": "0,8",
			"author": "_Alois P. Heinz_, Jul 29 2013",
			"references": 10,
			"revision": 35,
			"time": "2018-10-04T18:28:04-04:00",
			"created": "2013-08-02T17:48:58-04:00"
		}
	]
}