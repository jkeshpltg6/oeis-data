{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000989",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 989,
			"data": "0,0,1,0,0,2,1,1,2,0,0,1,0,0,3,2,2,3,1,1,2,1,1,3,2,2,3,0,0,1,0,0,2,1,1,2,0,0,1,0,0,4,3,3,4,2,2,3,2,2,4,3,3,4,1,1,2,1,1,3,2,2,3,1,1,2,1,1,4,3,3,4,2,2,3,2,2,4,3,3",
			"name": "3-adic valuation of binomial(2*n, n): largest k such that 3^k divides binomial(2*n, n).",
			"comment": [
				"a(n) = 0 if and only if n is in A005836. - _Charles R Greathouse IV_, May 19 2013",
				"sign(a(n+1) - a(n)) is repeat [0, 1, -1]. - _Filip Zaludek_, Oct 29 2016",
				"By Kummer's theorem, number of carries when adding n + n in base 3. - _Robert Israel_, Oct 30 2016"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A000989/b000989.txt\"\u003eTable of n, a(n) for n = 0..2000\u003c/a\u003e",
				"Michael Gilleland, \u003ca href=\"/selfsimilar.html\"\u003eSome Self-Similar Integer Sequences\u003c/a\u003e.",
				"E. E. Kummer, \u003ca href=\"https://doi.org/10.1515/crll.1852.44.93\"\u003eÜber die Ergänzungssätze zu den allgemeinen Reciprocitätsgesetzen\u003c/a\u003e, Journal für die reine und angewandte Mathematik, Vol. 44 (1852), pp. 93-146; \u003ca href=\"https://eudml.org/doc/147500\"\u003ealternative link\u003c/a\u003e.",
				"Dorel Miheţ, \u003ca href=\"https://doi.org/10.1007/s12045-010-0123-4\"\u003eLegendre's and Kummer's theorems again\u003c/a\u003e, Resonance, Vol. 15, No. 12 (2010), pp. 1111-1121; \u003ca href=\"https://www.ias.ac.in/public/Volumes/reso/015/12/1111-1121.pdf\"\u003ealternative link\u003c/a\u003e.",
				"Armin Straub, Victor H. Moll and Tewodros Amdeberhan, \u003ca href=\"https://eudml.org/doc/278348\"\u003eThe p-adic valuation of k-central binomial coefficients\u003c/a\u003e, Acta Arithmetica, Vol. 140, No. 1 (2009), pp. 31-42.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Kummer\u0026#39;s_theorem\"\u003eKummer's theorem\u003c/a\u003e."
			],
			"formula": [
				"a(n) = Sum_{k\u003e=0} floor(2*n/3^k) - 2*Sum_{k\u003e=0} floor(n/3^k). - _Benoit Cloitre_, Aug 26 2003",
				"a(n) = A007949(A000984(n)). - _Reinhard Zumkeller_, Nov 19 2015",
				"From _Robert Israel_, Oct 30 2016: (Start)",
				"If 2*n \u003c 3^k then a(3^k+n) = a(n).",
				"If n \u003c 3^k \u003c 2*n then a(3^k+n) = a(n)+1.",
				"If n \u003c 3^k then a(2*3^k+n) = a(n)+1. (End)",
				"a(n) = A053735(n) - A053735(2*n)/2. - _Amiram Eldar_, Feb 12 2021"
			],
			"maple": [
				"f:= proc(n) option remember; local k,m,d;",
				"   k:= floor(log[3](n));",
				"   d:= floor(n/3^k);",
				"   m:= n-d*3^k;",
				"   if d = 2 or 2*m \u003e 3^k then procname(m)+1",
				"   else procname(m)",
				"   fi",
				"end proc:",
				"f(0):= 0:",
				"map(f, [$0..100]); # _Robert Israel_, Oct 30 2016"
			],
			"mathematica": [
				"p=3; Array[ If[ Mod[ bi=Binomial[ 2#, # ], p ]==0, Select[ FactorInteger[ bi ], Function[ q, q[ [ 1 ] ]==p ], 1 ][ [ 1, 2 ] ], 0 ]\u0026, 27*3, 0 ]",
				"Table[ IntegerExponent[ Binomial[2 n, n], 3], {n, 0, 100}] (* _Jean-François Alcover_, Feb 15 2016 *)"
			],
			"program": [
				"(PARI) a(n) = valuation(binomial(2*n, n), 3)",
				"(PARI) a(n)=my(N=2*n,s);while(N\\=3,s+=N);while(n\\=3,s-=2*n);s \\\\ _Charles R Greathouse IV_, May 19 2013",
				"(Haskell)",
				"a000989 = a007949 . a000984  -- _Reinhard Zumkeller_, Nov 19 2015"
			],
			"xref": [
				"Cf. A000984, A005836, A007949, A053735."
			],
			"keyword": "nonn,easy",
			"offset": "0,6",
			"author": "_N. J. A. Sloane_, _R. K. Guy_",
			"references": 6,
			"revision": 51,
			"time": "2021-02-12T04:03:21-05:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}