{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A331807",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 331807,
			"data": "3,13,5,31,7,71,73,109,11,199,157,313,197,241,17,307,19,419,401,463,23,599,577,701,677,757,29,929,991,1117,1153,1123,1259,1471,37,1481,1483,1873,41,1723,43,1979,2069,2161,47,2351,2593,2549,2551,2857,53,2969,2917,3191,3137",
			"name": "a(n) is the smallest prime number p \u003e n, not yet in the sequence, such that p is a palindrome when written in base n.",
			"comment": [
				"Using a representation where the digits of the prime are written between \"[\" and \"]_\" separated by commas with the base following the \"_\" then by checking up to a base of 7000 (where the lowest prime palindrome is [1, 1]_7000):",
				"1) Either the palindrome is [1, 1]_n where n is one less than a prime number, or [1, X, 1]_n where X \u003c\u003c n, asymptotically.",
				"2) A conjecture: No lowest primes need more than three digits.",
				"3) The terms a(12) and a(30) differ from the similar sequence A331806 as these terms in A331806 are the same as the earlier terms a(3) and a(5)."
			],
			"link": [
				"Chai Wah Wu, \u003ca href=\"/A331807/b331807.txt\"\u003eTable of n, a(n) for n = 2..10000\u003c/a\u003e"
			],
			"example": [
				"a(2)=3 which is 11 in binary, a(3)=13 which is 111 in ternary, a(4)=5 which is 11 in quaternary, a(16)=17 which is 11 in hexadecimal.",
				"If we use the representation described earlier, then:",
				"   a(2)  = 3    is [1, 1]_2,",
				"   a(3)  = 13   is [1, 1, 1]_3,",
				"   a(4)  = 5    is [1, 1]_4,",
				"   a(11) = 199  is [1, 7, 1]_11,",
				"   a(13) = 313  is [1, 11, 1]_13,",
				"   a(16) = 17   is [1, 1]_16,",
				"   a(48) = 2593 is [1, 6, 1]_48."
			],
			"mathematica": [
				"Array[Block[{p = Prime[PrimePi[#] + 1]}, While[! PalindromeQ@ IntegerDigits[p, #], p = NextPrime@ p]; p] \u0026, 55, 2] (* _Michael De Vlieger_, Feb 25 2020 *)"
			],
			"xref": [
				"A331806 is a similar sequence where repeated terms are allowed.",
				"Cf. A006093 (prime(n) - 1)."
			],
			"keyword": "nonn,base,easy",
			"offset": "2,1",
			"author": "_Colin M Ready_, Feb 22 2020",
			"references": 1,
			"revision": 15,
			"time": "2020-07-03T02:37:57-04:00",
			"created": "2020-07-02T15:37:54-04:00"
		}
	]
}