{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A181289",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 181289,
			"data": "1,0,2,0,3,4,0,4,12,8,0,5,25,36,16,0,6,44,102,96,32,0,7,70,231,344,240,64,0,8,104,456,952,1040,576,128,0,9,147,819,2241,3400,2928,1344,256,0,10,200,1372,4712,9290,11040,7840,3072,512,0,11,264,2178,9108,22363",
			"name": "Triangle read by rows: T(n,k) is the number of 2-compositions of n having length k (0 \u003c= k \u003c= n).",
			"comment": [
				"A 2-composition of n is a nonnegative matrix with two rows, such that each column has at least one nonzero entry and whose entries sum up to n. The length of the 2-composition is the number of columns.",
				"The sum of entries in row n is A003480(n). Sum_{k=0..n} k*T(n,k) = A181290(n).",
				"From _Tom Copeland_, Sep 06 2011: (Start)",
				"R(t,z) = (1-z)^2 / ((1+t)*(1-z)^2-1) = 1/(t - (2*z + 3*z^2 + 4*z^3 + 5*z^4 + ...)) = 1/t + (1/t)^2*2*z + (1/t)^3*(4+3t)*z^2 + (1/t)^4*(8+12*t+4*t^2)*z^3 + ... gives row reversed polynomials of A181289 with G(t,z) = R(1/t,z)/t.",
				"R(t,z) is related to generators for A033282 and A001003 (t=1) and can be umbrally extended to give a partition generator for A133437. (End)",
				"A refined, reverse version of this array is given in A253722. - _Tom Copeland_, May 02 2015",
				"The infinitesimal generator (infinigen) for the face polynomials of associahedra A086810/A033282, read as decreasing powers, (and for the dual simplicial complex read as increasing powers) can be formed from the row polynomials P(n,t) of this entry. This type of infinigen is presented in A145271 for general sets of binomial Sheffer polynomials. This specific infinigen is presented in analytic form in A086810. Given the column vector of row polynomials V = (P(0,t) = 1, P(1,y) = 2 t, P(2,y) = 3 t + 4 t^2, P(3,y) = 4 t + 12 t^2 + 8 t^3, ...), form the lower triangular matrix M(n,k) = V(n-k,n-k), i.e., diagonally multiply the matrix with all ones on the diagonal and below by the components of V. Form the matrix MD by multiplying A132440^Transpose = A218272 = D (representing derivation of o.g.f.s) by M, i.e., MD = M*D. The non-vanishing component of the first row of (MD)^n * V / (n+1)! is the n-th face polynomial. - _Tom Copeland_, Dec 11 2015"
			],
			"link": [
				"G. Castiglione, A. Frosini, E. Munarini, A. Restivo and S. Rinaldi, \u003ca href=\"http://dx.doi.org/10.1016/j.ejc.2006.06.020\"\u003eCombinatorial aspects of L-convex polyominoes\u003c/a\u003e, European J. Combin. 28 (2007), no. 6, 1724-1741.",
				"Y-h. Guo, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL15/Guo/guo4.html\"\u003eSome n-Color Compositions\u003c/a\u003e, J. Int. Seq. 15 (2012) 12.1.2, eq. (11)."
			],
			"formula": [
				"T(n,k) = Sum_{j=0..k} (-1)^j*2^(k-j)*binomial(k,j)*binomial(n+k-j-1, 2k-1) (0 \u003c= k \u003c= n).",
				"G.f.: G(t,x) = (1-x)^2/((1-x)^2 - t*x(2-x)).",
				"G.f. of column k = x^k*(2-x)^k/(1-x)^{2k} (k\u003e=1) (we have a Riordan array).",
				"Recurrences satisfied by the numbers u_{n,k}=T(n,k) can be found in the Castiglione et al. reference.",
				"T(n,k) = 2*T(n-1,k) + 2*T(n-1,k-1) - T(n-2,k) - T(n-2,k-1), T(0,0)=1, T(1,0)=0, T(1,1)=2, T(2,0)=0, T(1,1)=3, T(2,2)=4, T(n,k)=0, if k \u003c 0 or if k \u003e n. - _Philippe Deléham_, Nov 29 2013"
			],
			"example": [
				"Triangle starts:",
				"  1;",
				"  0,  2;",
				"  0,  3,   4;",
				"  0,  4,  12,    8;",
				"  0,  5,  25,   36,   16;",
				"  0,  6,  44,  102,   96,    32;",
				"  0,  7,  70,  231,  344,   240,    64;",
				"  0,  8, 104,  456,  952,  1040,   576,   128;",
				"  0,  9, 147,  819, 2241,  3400,  2928,  1344,   256;",
				"  0, 10, 200, 1372, 4712,  9290, 11040,  7840,  3072,  512;",
				"  0, 11, 264, 2178, 9108, 22363, 34332, 33488, 20224, 6912, 1024;"
			],
			"maple": [
				"T := proc (n, k) if k \u003c= n then sum((-1)^j*2^(k-j)*binomial(k, j)*binomial(n+k-j-1, 2*k-1), j = 0 .. k) else 0 end if end proc: for n from 0 to 10 do seq(T(n, k), k = 0 .. n) end do; # yields sequence in triangular form"
			],
			"mathematica": [
				"Table[Sum[(-1)^j*2^(k - j) Binomial[k, j] Binomial[n + k - j - 1, 2 k - 1], {j, 0, k}], {n, 0, 10}, {k, 0, n}] // Flatten (* _Michael De Vlieger_, Dec 11 2015 *)"
			],
			"xref": [
				"Cf. A003480, A181290.",
				"Cf. A253722, A033282, A001003, A133437.",
				"Cf. A086810, A132440, A145271, A218272.",
				"Cf. A000297 (column 3), A006636 (column 4), A006637 (column 5)."
			],
			"keyword": "nonn,tabl",
			"offset": "0,3",
			"author": "_Emeric Deutsch_, Oct 12 2010",
			"references": 11,
			"revision": 38,
			"time": "2019-11-03T19:40:45-05:00",
			"created": "2010-10-20T03:00:00-04:00"
		}
	]
}