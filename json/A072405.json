{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A072405",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 72405,
			"data": "1,1,1,1,1,1,1,2,2,1,1,3,4,3,1,1,4,7,7,4,1,1,5,11,14,11,5,1,1,6,16,25,25,16,6,1,1,7,22,41,50,41,22,7,1,1,8,29,63,91,91,63,29,8,1,1,9,37,92,154,182,154,92,37,9,1,1,10,46,129,246,336,336,246,129,46,10,1,1,11,56,175,375,582,672,582,375,175,56,11,1",
			"name": "Triangle T(n, k) = C(n,k) - C(n-2,k-1) for n \u003e= 3 and T(n, k) = 1 otherwise, read by rows.",
			"comment": [
				"Starting 1,0,1,1,1,... this is the Riordan array ((1-x+x^2)/(1-x), x/(1-x)). Its diagonal sums are A006355. Its inverse is A106509. - _Paul Barry_, May 04 2005"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A072405/b072405.txt\"\u003eRows n = 0..50 of the triangle, flattened\u003c/a\u003e",
				"Paul Barry, \u003ca href=\"https://arxiv.org/abs/2104.01644\"\u003eCentered polygon numbers, heptagons and nonagons, and the Robbins numbers\u003c/a\u003e, arXiv:2104.01644 [math.CO], 2021."
			],
			"formula": [
				"T(n, k) = C(n,k) - C(n-2,k-1) for n \u003e= 3 and T(n, k) = 1 otherwise.",
				"T(n, k) = T(n-1, k-1) + T(n-1, k) starting with T(2, 0) = T(2, 1) = T(2, 2) = 1 and T(n, 0) = T(n, n) = 1.",
				"G.f.: (1-x^2*y) / (1 - x*(1+y)). - _Ralf Stephan_, Jan 31 2005",
				"From _G. C. Greubel_, Apr 28 2021: (Start)",
				"Sum_{k=0..n} T(n, k) = (n+1)*[n\u003c3] + 3*2^(n-2)*[n\u003e=3].",
				"T(n, k, q) = q*[n=2] + Sum_{j=0..5} q^j*binomial(n-2*j, k-j)*[n\u003e2*j] with T(n,0) = T(n,n) = 1 for q = -1. (End)"
			],
			"example": [
				"Rows start as:",
				"  1;",
				"  1, 1;",
				"  1, 1,  1; (key row for starting the recurrence)",
				"  1, 2,  2,  1;",
				"  1, 3,  4,  3,  1;",
				"  1, 4,  7,  7,  4, 1;",
				"  1, 5, 11, 14, 11, 5, 1;"
			],
			"mathematica": [
				"t[2, 1] = 1; t[n_, n_] = t[_, 0] = 1; t[n_, k_] := t[n, k] = t[n-1, k-1] + t[n-1, k]; Table[t[n, k], {n, 0, 12}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Nov 28 2013, after _Ralf Stephan_ *)"
			],
			"program": [
				"(Magma)",
				"T:= func\u003c n,k | n lt 3 select 1 else Binomial(n,k) - Binomial(n-2,k-1) \u003e;",
				"[T(n,k): k in [0..n], n in [0..12]]; // _G. C. Greubel_, Apr 28 2021",
				"(Sage)",
				"def T(n,k): return 1 if n\u003c3 else binomial(n,k) - binomial(n-2,k-1)",
				"flatten([[T(n,k) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, Apr 28 2021"
			],
			"xref": [
				"Row sums give essentially A003945, A007283, or A042950.",
				"Cf. A072406 for number of odd terms in each row.",
				"Cf. A051597, A096646, A122218.",
				"Cf. A007318 (q=0), A072405 (q= -1), A173117 (q=1), A173118 (q=2), A173119 (q=3), A173120 (q=-4)."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "0,8",
			"author": "_Henry Bottomley_, Jun 16 2002",
			"references": 16,
			"revision": 25,
			"time": "2021-06-30T18:59:23-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}