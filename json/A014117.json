{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A014117",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 14117,
			"data": "1,2,6,42,1806",
			"name": "Numbers n such that m^(n+1) == m (mod n) holds for all m.",
			"comment": [
				"\"Somebody incorrectly remembered Fermat's little theorem as saying that the congruence a^{n+1} = a (mod n) holds for all a if n is prime\" (Zagier). The sequence gives the set of integers n for which this property is in fact true.",
				"If i == j (mod n), then m^i == m^j (mod n) for all m. The latter congruence generally holds for any (m, n)=1 with i == j (mod k), k being the order of m modulo n, i.e., the least power k for which m^k == 1 (mod n). - _Lekraj Beedassy_, Jul 04 2002",
				"Also, numbers n such that n divides denominator of the n-th Bernoulli number B(n) (cf. A106741). Also, numbers n such that 1^n + 2^n + 3^n + ... + n^n == 1 (mod n). Equivalently, numbers n such that B(n)*n == 1 (mod n). Equivalently, Sum_{prime p, (p-1) divides n} n/p == -1 (mod n). It is easy to see that for n \u003e 1, n must be an even squarefree number. Moreover, the set P of prime divisors of all such n satisfies the property: if p is in P, then p-1 is the product of distinct elements of P. This set is P = {2, 3, 7, 43}, implying that the sequence is finite and complete. - _Max Alekseyev_, Aug 25 2013",
				"In 2005, B. C. Kellner proved E. W. Weisstein's conjecture that denom(B_n) = n only if n = 1806. - _Jonathan Sondow_, Oct 14 2013"
			],
			"link": [
				"M. A. Alekseyev, J. M. Grau, and A. M. Oller-Marcen. Computing solutions to the congruence 1^n + 2^n + ... + n^n == p (mod n). Discrete Applied Mathematics, 2018. doi:\u003ca href=\"http://doi.org/10.1016/j.dam.2018.05.022\"\u003e10.1016/j.dam.2018.05.022\u003c/a\u003e arXiv:\u003ca href=\"http://arxiv.org/abs/1602.02407\"\u003e1602.02407\u003c/a\u003e [math.NT], 2016-2018.",
				"John H. Castillo and Jhony Fernando Caranguay Mainguez, \u003ca href=\"https://arxiv.org/abs/1708.06812\"\u003eThe set of k-units modulo n\u003c/a\u003e, arXiv:1708.06812 [math.NT], 2017.",
				"Yongyi Chen and Tae Kyu Kim, \u003ca href=\"https://arxiv.org/abs/2103.04883\"\u003eOn Generalized Carmichael Numbers\u003c/a\u003e, arXiv:2103.04883 [math.NT], 2021.",
				"J. Dyer-Bennet, \u003ca href=\"http://www.jstor.org/stable/2304216\"\u003eA Theorem in Partitions of the Set of Positive Integers\u003c/a\u003e, Amer. Math. Monthly, 47(1940) pp. 152-4.",
				"L. Halbeisen and N. Hungerbühler, \u003ca href=\"https://hal.archives-ouvertes.fr/hal-01109575\"\u003eOn generalised Carmichael numbers\u003c/a\u003e, Hardy-Ramanujan Society, 1999, 22 (2), pp.8-22. (hal-01109575)",
				"J. M. Grau, A. M. Oller-Marcen, and J. Sondow, \u003ca href=\"http://arxiv.org/abs/1309.7941\"\u003eOn the congruence 1^m + 2^m + ... + m^m == n (mod m) with n|m\u003c/a\u003e, arXiv 1309.7941 [math.NT], 2013, Monatsh. Math., 177 (2015), 421-436.",
				"B. C. Kellner, \u003ca href=\"http://bernoulli.org/~bk/denombneqn.pdf\"\u003eThe equation denom(B_n) = n has only one solution\u003c/a\u003e, preprint 2005.",
				"Don Reble, \u003ca href=\"/A014117/a014117.pdf\"\u003eA014117 and related OEIS sequences\u003c/a\u003e (shows there are no other terms)",
				"J. Sondow and K. MacMillan, \u003ca href=\"http://arxiv.org/abs/1011.2154\"\u003eReducing the Erdos-Moser equation 1^n + 2^n + … + k^n = (k+1)^n modulo k and k^2\u003c/a\u003e, arXiv:1011.2154 [math.NT], 2010; see Prop. 2; Integers, 11 (2011), article A34.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/BernoulliNumber.html\"\u003eBernoulli Number\u003c/a\u003e",
				"D. Zagier, \u003ca href=\"http://www-groups.mcs.st-andrews.ac.uk/~john/Zagier/Problems.html\"\u003eProblems posed at the St Andrews Colloquium, 1996\u003c/a\u003e"
			],
			"formula": [
				"For n \u003c= 5, a(n) = a(n-1)^2 + a(n-1) with a(0) = 1. - _Raphie Frank_, Nov 12 2012",
				"a(n+1) = A007018(n) = A054377(n) = A100016(n) for n = 1, 2, 3, 4. - _Jonathan Sondow_, Oct 01 2013"
			],
			"mathematica": [
				"r[n_] := Reduce[ Mod[m^(n+1) - m, n] == 0, m, Integers]; ok[n_] := Range[n]-1 === Simplify[ Mod[ Flatten[ m /. {ToRules[ r[n][[2]] ]}], n], Element[C[1], Integers]]; ok[1] = True; A014117 = {}; Do[ If[ok[n], Print[n]; AppendTo[ A014117, n] ], {n, 1, 2000}] (* _Jean-François Alcover_, Dec 21 2011 *)",
				"Select[Range@ 2000, Function[n, Times @@ Boole@ Map[Function[m, PowerMod[m, n + 1, n] == Mod[m, n]], Range@ n] \u003e 0]] (* _Michael De Vlieger_, Dec 30 2016 *)"
			],
			"xref": [
				"Squarefree terms of A124240. - _Robert Israel_ and _Thomas Ordowski_, Jun 23 2017",
				"Cf. A007018, A031971, A054377, A100016, A242927."
			],
			"keyword": "nonn,fini,full,nice",
			"offset": "1,2",
			"author": "_David Broadhurst_",
			"references": 42,
			"revision": 99,
			"time": "2021-12-27T07:43:44-05:00",
			"created": "1998-06-14T03:00:00-04:00"
		}
	]
}