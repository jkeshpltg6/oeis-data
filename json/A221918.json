{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A221918",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 221918,
			"data": "1,2,1,3,6,3,4,4,12,2,5,10,15,20,5,6,3,2,12,30,3,7,14,21,28,35,42,7,8,8,24,8,40,24,56,4,9,18,9,36,45,18,63,72,9,10,5,30,20,10,15,70,40,90,5,11,22,33,44,55,66,77,88,99,110,11,12,12,12,3,60,4,84,24,36,60,132,6",
			"name": "Triangle of denominators of sum of two unit fractions: 1/n + 1/m, n \u003e= m \u003e= 1.",
			"comment": [
				"The corresponding triangle of numerators is A221919.",
				"The law for the electrical resistance in a parallel circuit with two resistors R1 and R2 is 1/R = 1/R1 + 1/R2. Here we take 1/R(n,m) =  1/n + 1/m, with n \u003e= m\u003e =1, and R(n,m) = a(n,m)/A221919(n,m).",
				"The reduced mass mu in a two body problem with masses m1 and m2 is given by 1/mu = 1/m1 + 1/m2.",
				"The radius R of the twin circles of Archimedes' arbelos with the radii of the two small half-circles r1 and r2 is given by 1/R = 1/r1 +1/r2. The large half-circle has radius r =  r1 + r2. See, e.g., the Bankoff reference (according to which one should speak of a triple of such radius R circles). There are much more such radius R circles. See the Arbelos references given by Schoch, especially reference [3].",
				"The columns give A000027, A145979(n-2), A221920, A221921, A222463 for m = 1, 2, ..., 5.",
				"This and the companion entry resulted from a remark on the twin circles in Archimedes' arbelos in the Strick reference, p. 13, and the obvious question about their radii and centers. See the MathWorld link, also for more references.",
				"The rationals R(n,m) = a(n,m)/A221919(n,m) (in lowest terms) equal H(n,m)/2, where H(n,m) = A227041(n,m)/A227042(n,m) is the harmonic mean of m and n. - _Wolfdieter Lang_, Jul 02 2013"
			],
			"reference": [
				"L. Bankoff, Are the Twin Circles of Archimedes Really Twins?, Mathematics Mag. 47,4 (1974) 214-218.",
				"H. K. Strick, Geschichten aus der Mathematik, Spektrum der Wissenschaft - Spezial 2/2009."
			],
			"link": [
				"T. Schoch, \u003ca href=\"http://www.retas.de/thomas/arbelos/references.html#ref3\"\u003eArbelos References\u003c/a\u003e.",
				"Eric W. Weisstein, \u003ca href=\"http://mathworld.wolfram.com/Arbelos.html\"\u003eArbelos\u003c/a\u003e (MathWorld)."
			],
			"formula": [
				"a(n,m) = denominator(1/n +1/m) = numerator(n*m/(n+m)), n \u003e= m \u003e= 1 and 0 otherwise.",
				"a(n,m)/A221919(n,m) = R(n,m) = n*m/(n+m). 1/R(n,m) = 1/n + 1/m."
			],
			"example": [
				"The triangle a(n,m) begins:",
				"n\\m    1    2    3    4    5    6   7   8   9   10   11  12 ...",
				"1:     1",
				"2:     2    1",
				"3:     3    6    3",
				"4:     4    4   12    2",
				"5:     5   10   15   20    5",
				"6:     6    3    2   12   30    3",
				"7:     7   14   21   28   35   42   7",
				"8:     8    8   24    8   40   24  56   4",
				"9:     9   18    9   36   45   18  63  72   9",
				"10:   10    5   30   20   10   15  70  40  90    5",
				"11:   11   22   33   44   55   66  77  88  99  110   11",
				"12:   12   12   12    3   60    4  84  24  36   60  132   6",
				"...",
				"a(n,1) = n because 1/R(n,1) =  1/n +1/1 = (n+1)/n, hence a(n,1) = denominator(1/n +1/1/) = n =  numerator(R(n,1)).",
				"a(5,3) = denominator(1/5 + 1/3) = denominator(8/15 ) = 15.",
				"a(6,3) = denominator(1/6 + 1/3) = denominator(9/18 ) = denominator(1/2) = 2.",
				"The triangle of rationals R(n,m) = n*m/(n+m) = a(n,m)/A221919(n,m) given by 1/R(n,m) = 1/n + 1/m starts:",
				"n\\m    1    2      3     4      5     6     7     8     9   10",
				"1:    1/2",
				"2:    2/3    1",
				"3:    3/4   6/5   3/2",
				"4:    4/5   4/3  12/7    2",
				"5:    5/6  10/7  15/8   20/9   5/2",
				"6:    6/7   3/2    2    12/5  30/11   3",
				"7:    7/8  14/9  21/10  28/11 35/12 42/13  7/2",
				"8:    8/9   8/5  24/11   8/3  40/13 24/7  56/15   4",
				"9:   9/10  18/11  9/4   36/13 45/14 18/5  63/16 72/17  9/2",
				"10:  10/11  5/3  30/13  20/7  10/3  15/4  70/17 40/9  90/19  5",
				"..."
			],
			"mathematica": [
				"a[n_, m_] := Denominator[1/n + 1/m]; Table[a[n, m], {n, 1, 12}, {m, 1, n}] // Flatten  (* _Jean-François Alcover_, Feb 25 2013 *)"
			],
			"xref": [
				"Cf. A221919 (companion)."
			],
			"keyword": "nonn,easy,tabl,frac",
			"offset": "1,2",
			"author": "_Wolfdieter Lang_, Feb 21 2013",
			"references": 8,
			"revision": 21,
			"time": "2019-01-19T04:15:43-05:00",
			"created": "2013-02-21T14:54:57-05:00"
		}
	]
}