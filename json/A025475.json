{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A025475",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 25475,
			"data": "1,4,8,9,16,25,27,32,49,64,81,121,125,128,169,243,256,289,343,361,512,529,625,729,841,961,1024,1331,1369,1681,1849,2048,2187,2197,2209,2401,2809,3125,3481,3721,4096,4489,4913,5041,5329,6241,6561,6859,6889,7921,8192",
			"name": "1 and the prime powers p^m where m \u003e= 2, thus excluding the primes.",
			"comment": [
				"Also nonprime n such that sigma(n)*phi(n) \u003e (n-1)^2. - _Benoit Cloitre_, Apr 12 2002",
				"If p is a term of the sequence, then the index n for which a(n) = p is given by n := b(p) := 1 + Sum_{k\u003e=2} PrimePi(p^(1/k)). Here, the sum has floor(log_2(p)) positive terms. For any m \u003e 0, the greatest number n such that a(n) \u003c= m is also given by b(m), thus, b(m) is the number of such prime powers \u003c= m. - _Hieronymus Fischer_, May 31 2013",
				"That 8 and 9 are the only two consecutive integers in this sequence is known as Catalan's Conjecture and was proved in 2002 by Preda Mihăilescu. - _Geoffrey Critzer_, Nov 15 2015"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A025475/b025475.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Romeo Meštrović, \u003ca href=\"http://arxiv.org/abs/1305.1867\"\u003eGeneralizations of Carmichael numbers I,\u003c/a\u003e arXiv:1305.1867v1 [math.NT], May 4, 2013.",
				"Preda Mihăilescu, \u003ca href=\"https://web.archive.org/web/20070221085421/https://www.dpmms.cam.ac.uk/seminars/Kuwait/abstracts/L30.pdf\"\u003eOn Catalan's Conjecture\u003c/a\u003e, Kuwait Foundation Lecture 30 - April 28, 2003.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PrimePower.html\"\u003ePrime Power\u003c/a\u003e."
			],
			"formula": [
				"A005171(a(n))*A010055(a(n)) = 1. - _Reinhard Zumkeller_, Nov 01 2009",
				"A192280(a(n)) = 0 for n \u003e 1. - _Reinhard Zumkeller_, Aug 26 2011",
				"A014963(a(n)) - A089026(a(n)) = A014963(a(n)) - 1. - _Eric Desbiaux_, May 18 2013",
				"From _Hieronymus Fischer_, May 31 2013: (Start)",
				"The greatest number n such that a(n) \u003c= m is given by 1 + Sum_{k\u003e=2} A000720(floor(m^(1/k))).",
				"Example 1:  m = 10^10 ==\u003e n = 10085;",
				"Example 2:  m = 10^11 ==\u003e n = 28157;",
				"Example 3:  m = 10^12 ==\u003e n = 80071;",
				"Example 4:  m = 10^15 ==\u003e n = 1962690. (End)",
				"Sum_{n\u003e=2} 1/a(n) = Sum_{p prime} 1/(p*(p-1)) = A136141. - _Amiram Eldar_, Oct 11 2020",
				"From _Amiram Eldar_, Jan 28 2021: (Start)",
				"Product_{n\u003e=2} (1 + 1/a(n)) = Product_{k\u003e=2} zeta(k)/zeta(2*k) = 2.0729553047...",
				"Product_{n\u003e=2} (1 - 1/a(n)) = A068982. (End)"
			],
			"maple": [
				"with(numtheory); A025475:=proc(q) local n; print(1);",
				"for n from 2 to q do if not isprime(n) then",
				"  if type(phi(n)/(n-phi(n)),integer) then print(n);",
				"fi; fi; od; end: A025475(10^6); # _Paolo P. Lava_, May 23 2013",
				"# alternative implementation",
				"isA025475 := proc(n)",
				"    if n \u003c 1 then",
				"        false;",
				"    elif n = 1 then",
				"        true;",
				"    elif isprime(n) then",
				"        false;",
				"    elif nops(numtheory[factorset](n)) = 1 then",
				"        true;",
				"    else",
				"        false;",
				"    end if;",
				"end proc:",
				"A025475 := proc(n)",
				"    option remember;",
				"    local a;",
				"    if n = 1 then",
				"        1;",
				"    else",
				"        for a from procname(n-1)+1 do",
				"            if isA025475(a) then",
				"                return a;",
				"            end if;",
				"        end do:",
				"    end if;",
				"end proc:",
				"# _R. J. Mathar_, Jun 06 2013",
				"# another alternative",
				"N:= 10^5: # to get all terms \u003c= N",
				"Primes:= select(isprime, [2,(2*i+1 $ i = 1 .. floor((sqrt(N)-1)/2))]):",
				"sort([1,seq(seq(p^i, i=2..floor(log[p](N))),p=Primes)]); # _Robert Israel_, Jul 27 2015"
			],
			"mathematica": [
				"A025475 = Select[ Range[ 2, 10000 ], ! PrimeQ[ # ] \u0026\u0026 Mod[ #, # - EulerPhi[ # ] ] == 0 \u0026 ]",
				"A025475 = Sort[ Flatten[ Table[ Prime[n]^i, {n, 1, PrimePi[ Sqrt[10^4]]}, {i, 2, Log[ Prime[n], 10^4]}]]]",
				"{1}~Join~Select[Range[10^4], And[! PrimeQ@ #, PrimePowerQ@ #] \u0026] (* _Michael De Vlieger_, Jul 04 2016 *)"
			],
			"program": [
				"(PARI) for(n=1,10000,if(sigma(n)*eulerphi(n)*(1-isprime(n))\u003e(n-1)^2,print1(n,\",\")))",
				"(PARI) is_A025475(n)={ ispower(n,,\u0026p) \u0026\u0026 isprime(p) || n==1 }  \\\\ _M. F. Hasler_, Sep 25 2011",
				"(PARI) list(lim)=my(v=List([1]),L=log(lim+.5));forprime(p=2,(lim+.5)^(1/3),for(e=3,L\\log(p),listput(v,p^e))); vecsort(concat(Vec(v), apply(n-\u003en^2,primes(primepi(sqrtint(lim\\1)))))) \\\\ _Charles R Greathouse IV_, Nov 12 2012",
				"(PARI) list(lim)=my(v=List([1])); for(m=2,logint(lim\\=1,2), forprime(p=2,sqrtnint(lim,m), listput(v, p^m))); Set(v) \\\\ _Charles R Greathouse IV_, Aug 26 2015",
				"(Haskell)",
				"a025475 n = a025475_list !! (n-1)",
				"a025475_list = filter ((== 0) . a010051) a000961_list",
				"-- _Reinhard Zumkeller_, Jun 22 2011",
				"(Python)",
				"from sympy import primerange",
				"A025475_list, m = [1], 10*2",
				"m2 = m**2",
				"for p in primerange(1,m):",
				"    a = p**2",
				"    while a \u003c m2:",
				"        A025475_list.append(a)",
				"        a *= p",
				"A025475_list = sorted(A025475_list) # _Chai Wah Wu_, Sep 08 2014"
			],
			"xref": [
				"Subsequence of A000961. - _Reinhard Zumkeller_, Jun 22 2011",
				"Cf. A001597, A000720, A068982, A136141, A193166.",
				"Differences give A053707.",
				"Cf. A076048 (number of terms \u003c 10^n).",
				"There are four different sequences which may legitimately be called \"prime powers\": A000961 (p^k, k \u003e= 0), A246655 (p^k, k \u003e= 1), A246547 (p^k, k \u003e= 2), A025475 (p^k, k=0 and k \u003e= 2). When you refer to \"prime powers\", be sure to specify which of these you mean. Also A001597 is the sequence of nontrivial powers n^k, n \u003e= 1, k \u003e= 2. - _N. J. A. Sloane_, Mar 24 2018"
			],
			"keyword": "nonn,easy,nice",
			"offset": "1,2",
			"author": "_David W. Wilson_",
			"ext": [
				"Edited by _Daniel Forgues_, Aug 18 2009"
			],
			"references": 181,
			"revision": 135,
			"time": "2021-01-28T03:42:54-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}