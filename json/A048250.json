{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A048250",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 48250,
			"data": "1,3,4,3,6,12,8,3,4,18,12,12,14,24,24,3,18,12,20,18,32,36,24,12,6,42,4,24,30,72,32,3,48,54,48,12,38,60,56,18,42,96,44,36,24,72,48,12,8,18,72,42,54,12,72,24,80,90,60,72,62,96,32,3,84,144,68,54,96,144,72,12,74",
			"name": "Sum of the squarefree divisors of n.",
			"comment": [
				"Also sum of divisors of the squarefree kernel of n: a(n) = A000203(A007947(n)). - _Reinhard Zumkeller_, Jul 19 2002",
				"The absolute values of the Dirichlet inverse of A001615. - _R. J. Mathar_, Dec 22 2010",
				"Row sums of the triangle in A206778. - _Reinhard Zumkeller_, Feb 12 2012"
			],
			"reference": [
				"D. Suryanarayana, On the core of an integer, Indian J. Math. 14 (1972) 65-74."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A048250/b048250.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Steven R. Finch, \u003ca href=\"http://www.people.fas.harvard.edu/~sfinch/\"\u003eUnitarism and infinitarism\u003c/a\u003e.",
				"Steven R. Finch, \u003ca href=\"/A007947/a007947.pdf\"\u003eUnitarism and Infinitarism\u003c/a\u003e, February 25, 2004. [Cached copy, with permission of the author]",
				"\u003ca href=\"/index/Su#ssq\"\u003eIndex entries for sequences related to sums of squares\u003c/a\u003e"
			],
			"formula": [
				"If n = Product p_i^e_i, a(n) = Product (p_i + 1). - _Vladeta Jovovic_, Apr 19 2001",
				"Dirichlet g.f.: zeta(s)*zeta(s-1)/zeta(2*s-2). - _Michael Somos_, Sep 08 2002",
				"a(n) = Sum_{d|n} mu(d)^2*d. - _Benoit Cloitre_, Dec 09 2002",
				"Pieter Moree (moree(AT)mpim-bonn.mpg.de), Feb 20 2004 can show that Sum_{n \u003c= x} a(n) = x^2/2 + O(x*sqrt{x}) and adds: \"As S. R. Finch pointed out to me, in Suryanarayana's paper this is proved under the Riemann hypothesis with error term O(x^{7/5+epsilon})\".",
				"a(n) = psi(rad(n)) = A001615(A007947(n)). - _Enrique Pérez Herrero_, Aug 24 2010",
				"a(n) = rad(n)*psi(n)/n = A001615(n)*A007947(n)/n. - _Enrique Pérez Herrero_, Aug 31 2010",
				"G.f.: Sum_{k\u003e=1} mu(k)^2*k*x^k/(1 - x^k). - _Ilya Gutkovskiy_, Jan 03 2017",
				"Lim_{n-\u003eoo} (1/n) * Sum_{k=1..n} a(k)/k = 1. - _Amiram Eldar_, Jun 10 2020"
			],
			"example": [
				"For n=1000, out of the 16 divisors, four are squarefree: {1,2,5,10}. Their sum is 18. Or, 1000 = 2^3*5^3 hence a(1000) = (2+1)*(5+1) = 18."
			],
			"maple": [
				"A048250 := proc(n) local ans, i:ans := 1: for i from 1 to nops(ifactors(n)[ 2 ]) do ans := ans*(1+ifactors(n)[ 2 ][ i ] [ 1 ]): od: RETURN(ans) end:",
				"# alternative:",
				"seq(mul(1+p, p = numtheory:-factorset(n)), n=1..1000); # _Robert Israel_, Mar 18 2015"
			],
			"mathematica": [
				"sumOfSquareFreeDivisors[ n_ ] := Plus @@ Select[ Divisors[ n ], MoebiusMu[ # ] != 0 \u0026 ]; Table[ sumOfSquareFreeDivisors[ i ], {i, 85} ]",
				"Table[Total[Select[Divisors[n],SquareFreeQ]],{n,80}] (* _Harvey P. Dale_, Jan 25 2013 *)",
				"a[1] = 1; a[n_] := Times@@(1 + FactorInteger[n][[;;,1]]); Array[a, 100] (* _Amiram Eldar_, Dec 19 2018 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c1,0,sumdiv(n,d,if(core(d)==d,d)))",
				"(PARI) a(n)=if(n\u003c1,0,direuler(p=2,n,(1+p*X)/(1-X))[n])",
				"(PARI) a(n)=sumdiv(n,d,moebius(d)^2*d); \\\\ _Joerg Arndt_, Jul 06 2011",
				"(PARI) a(n)=my(f=factor(n)); for(i=1,#f~,f[i,2]=1); sigma(f) \\\\ _Charles R Greathouse IV_, Sep 09 2014",
				"(Haskell)",
				"a034448 = sum . a206778_row  -- _Reinhard Zumkeller_, Feb 12 2012",
				"(Sage)",
				"def A048250(n): return mul(map(lambda p: p+1, prime_divisors(n)))",
				"[A048250(n) for n in (1..73)]  # _Peter Luschny_, May 23 2013"
			],
			"xref": [
				"Cf. A003557, A007947, A023900, A034444, A034448, A206787, A309192."
			],
			"keyword": "nonn,easy,nice,mult",
			"offset": "1,2",
			"author": "_Labos Elemer_",
			"references": 115,
			"revision": 74,
			"time": "2021-05-20T00:20:59-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}