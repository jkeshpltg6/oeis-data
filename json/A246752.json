{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A246752",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 246752,
			"data": "1,-1,-2,0,2,3,-2,0,1,-2,-2,0,2,0,-2,0,3,-2,0,0,2,3,-2,0,2,-2,-2,0,0,0,-4,0,2,-1,-2,0,2,6,0,0,1,-2,-2,0,4,0,-2,0,0,-2,-2,0,2,0,-2,0,3,-2,-2,0,2,0,0,0,2,-3,-2,0,0,6,-2,0,4,0,-2,0,2,0",
			"name": "Expansion of phi(-x) * chi(x) * psi(-x^3) in powers of x where phi(), psi(), chi() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A246752/b246752.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of phi(-x) * f(x^1, x^5) in powers of x where phi(), f() are Ramanujan theta functions.",
				"Expansion of q^(-1/3) * eta(q) * eta(q^2) * eta(q^3) * eta(q^12) / (eta(q^4) * eta(q^6)) in powers of q.",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (144 t)) = 768^(1/2) (t/i) g(t) where q = exp(2 Pi i t) and g() is the g.f. for A246838.",
				"a(n) = (-1)^n * A246650(n).",
				"Convolution of A002448 and A089801.",
				"a(2*n) = A129451(n). a(4*n) = A123884(n). a(4*n + 1) = - A122861(n). a(4*n + 2) = - 2 * A121361(n). a(4*n + 3) = 0."
			],
			"example": [
				"G.f. = 1 - x - 2*x^2 + 2*x^4 + 3*x^5 - 2*x^6 + x^8 - 2*x^9 - 2*x^10 + ...",
				"G.f. = q - q^4 - 2*q^7 + 2*q^13 + 3*q^16 - 2*q^19 + q^25 - 2*q^28 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ x] QPochhammer[ x^2] QPochhammer[ x^3] QPochhammer[ x^12] / (QPochhammer[ x^4] QPochhammer[ x^6]), {x, 0, n}];",
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 4, 0, x] QPochhammer[ -x, x^6] QPochhammer[ -x^5, x^6] QPochhammer[ x^6], {x, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A) * eta(x^2 + A) * eta(x^3 + A) * eta(x^12 + A) / (eta(x^4 + A) * eta(x^6 + A)), n))};"
			],
			"xref": [
				"Cf. A002448, A089801, A121361, A122861, A123884, A129451, A246650, A246838."
			],
			"keyword": "sign",
			"offset": "0,3",
			"author": "_Michael Somos_, Sep 02 2014",
			"references": 3,
			"revision": 14,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2014-09-02T13:51:50-04:00"
		}
	]
}