{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A014600",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 14600,
			"data": "1,1,1,1,1,1,2,1,1,2,3,2,1,1,3,2,2,2,4,2,1,3,5,2,2,2,4,4,3,2,4,2,1,4,7,2,2,3,5,4,3,4,6,2,2,3,8,4,2,2,5,6,3,3,8,2,2,6,10,4,2,3,5,4,5,4,6,4,3,6,10,4,2,2,7,6,4,4,10,4,1,8,11,4,4,3,6,6,5,4,8,4,2,5,13,4,4",
			"name": "Class numbers h(D) of imaginary quadratic orders with discriminant D == 0 or 1 mod 4, D\u003c0.",
			"comment": [
				"The sequence consists of class numbers of imaginary quadratic \"orders\", not imaginary quadratic \"fields\". The difference is that an imaginary quadratic order may be a non-maximal order, but a class number of an imaginary quadratic field always refers to the class number of the maximal order within that imaginary quadratic field. - _David Jao_, Sep 13 2020"
			],
			"reference": [
				"H. Cohen, Course in Computational Alg. No. Theory, Springer, 1993, pp. 514-5."
			],
			"link": [
				"Charles R Greathouse IV, \u003ca href=\"/A014600/b014600.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"S. R. Finch, \u003ca href=\"http://www.people.fas.harvard.edu/~sfinch/\"\u003eClass number theory\u003c/a\u003e",
				"Steven R. Finch, \u003ca href=\"/A000924/a000924.pdf\"\u003eClass number theory\u003c/a\u003e [Cached copy, with permission of the author]",
				"Rick L. Shepherd, \u003ca href=\"http://libres.uncg.edu/ir/uncg/f/Shepherd_uncg_0154M_11099.pdf\"\u003eBinary quadratic forms and genus theory\u003c/a\u003e, Master of Arts Thesis, University of North Carolina at Greensboro, 2013."
			],
			"mathematica": [
				"ClassList[n_?Negative] :=",
				"Select[Flatten[#, 1] \u0026@Table[",
				"    {i, j, (j^2 - n)/(4 i)}, {i, Sqrt[-n/3]}, {j, 1 - i, i}],",
				"  Mod[#3, 1] == 0 \u0026\u0026 #3 \u003e= # \u0026\u0026",
				"      GCD[##] == 1 \u0026\u0026 ! (# == #3 \u0026\u0026 #2 \u003c 0) \u0026 @@ # \u0026]",
				"a[n_] := Length[ClassList[Floor[n/2]*-4 - Mod[n,2] - 3]] (* _David Jao_, Sep 14 2020 *)"
			],
			"program": [
				"(PARI) a(n)=qfbclassno(n\\2*-4-n%2-3) \\\\ _Charles R Greathouse IV_, Apr 25 2013",
				"(PARI) a(n)=quadclassunit(n\\2*-4-n%2-3).no \\\\ _Charles R Greathouse IV_, Apr 25 2013"
			],
			"keyword": "nonn",
			"offset": "0,7",
			"author": "Eric Rains (rains(AT)caltech.edu)",
			"ext": [
				"Name corrected by _David Jao_, Sep 13 2020"
			],
			"references": 5,
			"revision": 39,
			"time": "2020-09-26T11:06:32-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}