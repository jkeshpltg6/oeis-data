{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A324315",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 324315,
			"data": "231,561,1001,1045,1105,1122,1155,1729,2002,2093,2145,2465,2821,3003,3315,3458,3553,3570,3655,3927,4186,4199,4522,4774,4845,4862,5005,5187,5565,5642,5681,6006,6118,6270,6279,6545,6601,6670,6734,7337,7395,7735,8177,8211,8265,8294,8323,8463,8645,8789,8855,8911,9282,9361,9435,9690,9867",
			"name": "Squarefree integers m \u003e 1 such that if prime p divides m, then the sum of the base p digits of m is at least p.",
			"comment": [
				"The sequence is infinite, because it contains all Carmichael numbers (A002997).",
				"If m is a term and p is a prime factor of m, then p \u003c= a*sqrt(m) with a = sqrt(11/21) = 0.7237..., where the bound is sharp.",
				"A term m must have at least 3 prime factors if m is odd, and must have at least 4 prime factors if m is even.",
				"m is a term if and only if m \u003e 1 divides denominator(Bernoulli_m(x) - Bernoulli_m) = A195441(m-1).",
				"A term m is a Carmichael number iff s_p(m) == 1 (mod p-1) whenever prime p divides m, where s_p(m) is the sum of the base p digits of m.",
				"See Kellner and Sondow 2019."
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A324315/b324315.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Bernd C. Kellner and Jonathan Sondow, \u003ca href=\"https://doi.org/10.4169/amer.math.monthly.124.8.695\"\u003ePower-Sum Denominators\u003c/a\u003e, Amer. Math. Monthly, 124 (2017), 695-709; arXiv:\u003ca href=\"https://arxiv.org/abs/1705.03857\"\u003e1705.03857\u003c/a\u003e [math.NT], 2017.",
				"Bernd C. Kellner and Jonathan Sondow, \u003ca href=\"https://arxiv.org/abs/1902.10672\"\u003eOn Carmichael and polygonal numbers, Bernoulli polynomials, and sums of base-p digits\u003c/a\u003e, arXiv:1902.10672 [math.NT], 2019."
			],
			"formula": [
				"a_1 + a_2 + ... + a_k \u003e= p for m = a_1 * p + a_2 * p^2 + ... + a_k * p^k with 0 \u003c= a_i \u003c= p-1 for i = 1, 2, ..., k (note that a_0 = 0)."
			],
			"example": [
				"231 = 3 * 7 * 11 is squarefree, and 231 in base 3 is 22120_3 = 2 * 3^4 + 2 * 3^3 + 1 * 3^2 + 2 * 3 + 0 with 2+2+1+2+0 = 7 \u003e= 3, and 231 = 450_7 with 4+5+0 = 9 \u003e= 7, and 231 = 1a0_11 with 1+a+0 = 1+10+0 = 11 \u003e= 11, so 231 is a member."
			],
			"mathematica": [
				"SD[n_, p_] := If[n \u003c 1 || p \u003c 2, 0, Plus @@ IntegerDigits[n, p]];",
				"LP[n_] := Transpose[FactorInteger[n]][[1]];",
				"TestS[n_] := (n \u003e 1) \u0026\u0026 SquareFreeQ[n] \u0026\u0026 VectorQ[LP[n], SD[n, #] \u003e= # \u0026];",
				"Select[Range[10^4], TestS[#] \u0026]"
			],
			"xref": [
				"Cf. A002997, A005117, A195441, A324316, A324317, A324318, A324319, A324320, A324369, A324370, A324371, A324404, A324405."
			],
			"keyword": "nonn,base",
			"offset": "1,1",
			"author": "_Bernd C. Kellner_ and _Jonathan Sondow_, Feb 21 2019",
			"references": 15,
			"revision": 36,
			"time": "2020-12-05T04:53:13-05:00",
			"created": "2019-02-28T14:37:51-05:00"
		}
	]
}