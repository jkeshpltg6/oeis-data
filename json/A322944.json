{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A322944",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 322944,
			"data": "1,1,1,2,6,1,6,38,15,1,24,272,188,28,1,120,2200,2340,580,45,1,720,19920,30280,11040,1390,66,1,5040,199920,413560,206920,37450,2842,91,1,40320,2204160,5989760,3931200,955920,102816,5208,120,1",
			"name": "Coefficients of a family of orthogonal polynomials. Triangle read by rows, T(n, k) for 0 \u003c= k \u003c= n.",
			"comment": [
				"The polynomials represent a family of orthogonal polynomials which obey a recurrence of the form p(n, x) = (x+r(n))*p(n-1, x) - s(n)*p(n-2, x) + t(n)*p(n-3, x) - u(n)*p(n-4, x). For the details see the Maple program.",
				"We conjecture that the polynomials have only negative and simple real roots."
			],
			"link": [
				"Peter Luschny, \u003ca href=\"/A322944/a322944.jpg\"\u003ePlot of the polynomials\u003c/a\u003e."
			],
			"formula": [
				"Let R be the inverse of the Riordan square [see A321620] of (1 - 3*x)^(-1/3) then T(n, k) = (-1)^(n-k)*R(n, k)."
			],
			"example": [
				"Triangle starts:",
				"[0]    1;",
				"[1]    1,      1;",
				"[2]    2,      6,      1;",
				"[3]    6,     38,     15,      1;",
				"[4]   24,    272,    188,     28,     1;",
				"[5]  120,   2200,   2340,    580,    45,    1;",
				"[6]  720,  19920,  30280,  11040,  1390,   66,  1;",
				"[7] 5040, 199920, 413560, 206920, 37450, 2842, 91, 1;",
				"Production matrix starts:",
				"   1;",
				"   1,    1;",
				"   3,    5,    1;",
				"   6,   18,    9,    1;",
				"   6,   42,   45,   13,    1;",
				"   0,   48,  132,   84,   17,    1;",
				"   0,    0,  180,  300,  135,   21,    1;",
				"   0,    0,    0,  480,  570,  198,   25,    1;"
			],
			"maple": [
				"P := proc(n) option remember; local a, b, c, d;",
				"a := n -\u003e 4*n-3; b := n -\u003e 3*(n-1)*(2*n-3);",
				"c := n -\u003e (n-1)*(n-2)*(4*n-9); d := n -\u003e (n-2)*(n-1)*(n-3)^2;",
				"if n = 0 then return 1 fi;",
				"if n = 1 then return x + 1 fi;",
				"if n = 2 then return x^2 + 6*x + 2 fi;",
				"if n = 3 then return x^3 + 15*x^2 + 38*x + 6 fi;",
				"expand((x+a(n))*P(n-1) - b(n)*P(n-2) + c(n)*P(n-3) - d(n)*P(n-4)) end:",
				"seq(print(P(n)), n=0..9); # Computes the polynomials."
			],
			"mathematica": [
				"a[n_] := 4n - 3;",
				"b[n_] := 3(n - 1)(2n - 3);",
				"c[n_] := (n - 1)(n - 2)(4n - 9);",
				"d[n_] := (n - 2)(n - 1)(n - 3)^2;",
				"P[n_] := P[n] = Switch[n, 0, 1, 1, x + 1, 2, x^2 + 6x + 2, 3, x^3 + 15x^2 + 38x + 6, _, Expand[(x + a[n]) P[n - 1] - b[n] P[n - 2] + c[n] P[n - 3] - d[n] P[n - 4]]];",
				"Table[CoefficientList[P[n], x], {n, 0, 9}] (* _Jean-François Alcover_, Jun 15 2019, from Maple *)"
			],
			"program": [
				"(Sage) # uses[riordan_square from A321620]",
				"R = riordan_square((1 - 3*x)^(-1/3), 9, True).inverse()",
				"for n in (0..8): print([(-1)^(n-k)*c for (k, c) in enumerate(R.row(n)[:n+1])])"
			],
			"xref": [
				"p(n, 1) = A322943(n) (row sums); p(n, 0) = n! = A000142(n).",
				"A321966 (m=2), this sequence (m=3).",
				"Cf. A321620."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_Peter Luschny_, Jan 02 2019",
			"references": 2,
			"revision": 20,
			"time": "2020-03-25T07:55:33-04:00",
			"created": "2019-01-02T11:37:12-05:00"
		}
	]
}