{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A300291",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 300291,
			"data": "5,1,13,17,1,25,1,29,1,41,37,1,1,1,61,1,53,1,65,1,85,65,1,73,1,89,1,113,1,85,1,97,1,1,1,145,101,1,109,1,1,1,149,1,181,1,125,1,137,1,157,1,185,1,221,145,1,1,1,169,1,193,1,1,1,265,1,173,1,185,1,205,1,233,1,269,1,313,197,1,205,1,221,1,1,1,277,1,317,1,365",
			"name": "Triangle T read by rows: T is used to obtain the denominators of all fractional values for x = cos(phi) and y = sin(phi) with (x, y) on the unit circle for 0 \u003c phi \u003c Pi/2.",
			"comment": [
				"All rational values of sin(phi) = p/q and cos(phi) = r/s (gcd(p, q) = 1 = gcd(r, s), p and r nonnegative integer, q and s positive integer) with sin(phi)^2 + cos(phi)^2 = 1, and phi from [0, Pi/2] are the integer values 0 and 1, and the fractional values (q \u003e= 2, s \u003e= 2) with (p = p(n, m), r = r(n, m)) = (X(n, m), Y(n, m)) or  (Y(n, m), X(n, m)) and  q = q(n, m) = Zhat(n, m) = T(n, m), disregarding all p = 0 and r = 0 values. Here X and Y are the triangles A225950 and A225952 (with 0's) known from the legs of primitive Pythagorean triangles with odd X and even Y, respectively, and the corresponding triangle Z for the hypotenuses is A222946.",
				"The present triangle T = Zhat is obtained from triangle Z by replacing all entries 0 (those with gcd(n, m) not 1 or (-1)^(n+m) = +1) by 1.",
				"In each row n the positive values for X(n, m)/Zhat(n, m) decrease and for Y(n, m)/Zhat(n, m) they increase.",
				"The maximal values of X(n, m)/Zhat(n, m) decreases from row n to row n+1 if n is even and it increases if n is odd. The maximal values of Y(n, m)/Zhat(n, m) increase from row n to row n+1.",
				"The positive minimal values of X(n, m)/Zhat(n, m) decreases from row n to row n+1. The positive minimal values of Y(n, m)/Zhat(n, m) increase from row n to row n+1 if n is even, and they decrease if n is odd."
			],
			"link": [
				"Wolfdieter Lang, \u003ca href=\"/A300291/a300291_2.pdf\"\u003eTriangles X/Zhat and Y/Zhat for fractional values of sine (or cosine) for the unit circle, for n = 2..20.\u003c/a\u003e"
			],
			"formula": [
				"T(n, m) = 0 if n \u003c m + 1, and T(n, m) = n^2 + m^2 if gcd(n, m) = 1 and (-1)^(n+m) = -1, and T(n, m) = 1 otherwise."
			],
			"example": [
				"The triangle T = Zhat begins (0's are not shown):",
				"n\\m    1   2    3   4   5   6    7    8    9   10   11 ...",
				"2:     5",
				"3:     1  13",
				"4:    17   1   25",
				"5:     1  29    1  41",
				"6:    37   1    1   1  61",
				"7:     1  53    1  65   1  85",
				"8:    65   1   73   1  89   1  113",
				"9:     1  85    1  97   1   1    1  145",
				"10:  101   1  109   1   1   1  149    1  181",
				"11:    1 125    1 137   1 157    1  185    1  221",
				"12:  145   1    1   1 169   1  193    1    1    1  265",
				"...",
				"-----------------------------------------------------------------------------",
				"For the sin(phi) = p/q values from triangle X(n, m)/Zhat(n, m) = A225950(n, m)/T(n, m), and from triangle Y(n, m)/Zhat(n, m) = A225952(n, m)/T(n, m), for n = 2..20, see the attached link.",
				"-----------------------------------------------------------------------------",
				"The approximate values (3 digits) for  X(n, m)/Zhat(n, m) begin:",
				"n\\m    1     2      3       4      5       6     7     8     9 ...",
				"2:  .600",
				"3:     0   .385",
				"4:  .882      0   .280",
				"5:     0   .724      0   .220",
				"6:  .946      0.     0      0    .180",
				"7:     0   .849      0   .508       0   .153",
				"8:  .969      0   .753      0    .438      0  .133",
				"9:     0   .906      0   .670      0       0     0  .117",
				"10  .980      0   .835      0      0       0  .342     0  .105",
				"...",
				"The approximate values (3 digits) of Y(n, m)/Zhat(n, m) begin:",
				"n\\m     1     2      3      4      5       6     7     8     9 ...",
				"2:   .800",
				"3:      0  .923",
				"4:   .471     0   .960",
				"5:      0  .690      0   .976",
				"6:   .324     0      0      0   .984",
				"7:      0  .528      0   .862      0    .988",
				"8:   .246     0   .658      0   .899       0   .99",
				"9:     0   .424      0   .742      0       0     0  .993",
				"10  .198      0   .550      0      0       0  .940     0  .994",
				"...",
				"-----------------------------------------------------------------------------"
			],
			"xref": [
				"Cf. A222946, A225950, A225952."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "2,1",
			"author": "_Wolfdieter Lang_, Mar 13 2018",
			"references": 1,
			"revision": 21,
			"time": "2020-04-07T23:20:27-04:00",
			"created": "2018-03-16T05:58:34-04:00"
		}
	]
}