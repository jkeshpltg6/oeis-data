{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A300219",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 300219,
			"data": "1,1,1,1,1,3,2,1,5,2,2,1,3,3,1,1,2,2,2,1,8,3,2,3,4,3,4,2,8,5,4,1,7,6,4,5,1,3,6,2,9,6,3,2,8,4,2,1,5,3,7,3,4,6,3,3,7,4,5,1,3,5,3,1,2,9,4,2,11,3,6,2,6,7,3,2,4,5,4,1",
			"name": "Number of ways to write n^2 as x^2 + y^2 + z^2 + w^2 with x,y,z,w nonnegative integers and z \u003c= w such that both x and 4*x - 3*y are powers of 4 (including 4^0 = 1).",
			"comment": [
				"Conjecture: (i) a(n) \u003e 0 for all n \u003e 0, and a(n) = 1 only for n = 4^k*m (k = 0,1,2,... and m = 1, 2, 3, 5, 15, 37, 83, 263). Also, for each n = 2,3,... we can write n^2 as x^2 + y^2 + z^2 + w^2 with x,y,z,w nonnegative integers such that both x and 4*x - 3*y lie in the set {2^(2k+1): k = 0,1,...}.",
				"(ii) Let r be 0 or 1, and let n \u003e r. Then n^2 can be written as x^2 + y^2 + z^2 + w^2 with x,y,z,w nonnegative integers such that both x and x + 3*y belong to the set {2^(2k+r): k = 0,1,2,...}, unless n has the form 2^(2k+r)*81503 with k a nonnegative integer and hence n^2 = (2^(2k+r)*28^2)^2 + (2^(2k+r)*80)^2 + (2^(2k+r)*55937)^2 + (2^(2k+r)*59272)^2 with 2^(2k+r)*28^2 = 2^r*(2^k*28)^2 and 2^(2k+r)*28^2 + 3*(2^(2k+r)*80) = 2^(2(k+5)+r). So we always can write n^2 as x^2 + y^2 + z^2 + w^2 with x,y,z,w nonnegative integers such that x/2^r is a square and (x+3*y)/2^r is a power of 4.",
				"In arXiv:1701.05868 the author proved that for each r = 0,1 and n \u003e r we can write n^2 as (2^(2k+r))^2 + x^2 + y^2 + z^2 with k,x,y,z nonnegative integers.",
				"We have verified both parts of the conjecture for n up to 10^7."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A300219/b300219.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2016.11.008\"\u003eRefining Lagrange's four-square theorem\u003c/a\u003e, J. Number Theory 175(2017), 167-190.",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1701.05868\"\u003eRestricted sums of four squares\u003c/a\u003e, arXiv:1701.05868 [math.NT], 2017-2018."
			],
			"example": [
				"a(2) = 1 since 2^2 = 1^2 + 1^2 + 1^2 + 1^2 with 1 = 4^0 and 4*1 - 3*1 = 4^0.",
				"a(3) = 1 since 3^2 = 1^2 + 0^2 + 2^2 + 2^2 with 1 = 4^0 and 4*1 - 3*0 = 4^1.",
				"a(5) = 1 since 5^2 = 4^2 + 0^2 + 0^2 + 3^2 with 4 = 4^1 and 4*4 - 3*0 = 4^2.",
				"a(15) = 1 since 15^2 = 4^2 + 4^2 + 7^2 + 12^2 with 4 = 4^1 and 4*4 - 3*4 = 4^1.",
				"a(37) = 1 since 37^2 = 16^2 + 16^2 + 4^2 + 29^2 with 16 = 4^2 and 4*16 - 3*16 = 4^2.",
				"a(83) = 1 since 83^2 = 4^2 + 4^2 + 56^2 + 61^2 with 4 = 4^1 and 4*4 - 3*4 = 4^1.",
				"a(263) = 1 since 263^2 = 4^2 + 5^2 + 22^2 + 262^2 with 4 = 4^1 and 4*4 - 3*5 = 4^0."
			],
			"mathematica": [
				"SQ[n_]:=SQ[n]=IntegerQ[Sqrt[n]];",
				"Do[r=0;Do[If[SQ[n^2-16^k-((4^(k+1)-4^m)/3)^2-z^2],r=r+1],{k,0,Log[4,n]},{m,Ceiling[Log[4,Max[1,4^(k+1)-3*Sqrt[n^2-16^k]]]],k+1},{z,0,Sqrt[(n^2-16^k-((4^(k+1)-4^m)/3)^2)/2]}];Print[n,\" \",r];Label[aa],{n,1,80}]"
			],
			"xref": [
				"Cf. A000118, A271518, A279612, A281976, A299924, A300365, A300360."
			],
			"keyword": "nonn",
			"offset": "1,6",
			"author": "_Zhi-Wei Sun_, Feb 28 2018",
			"references": 35,
			"revision": 20,
			"time": "2018-03-03T22:50:39-05:00",
			"created": "2018-03-01T03:42:43-05:00"
		}
	]
}