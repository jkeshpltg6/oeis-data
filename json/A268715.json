{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A268715",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 268715,
			"data": "0,1,1,2,3,2,3,6,6,3,4,2,5,2,4,5,12,7,7,12,5,6,4,15,6,15,4,6,7,7,13,13,13,13,7,7,8,5,4,12,9,12,4,5,8,9,24,12,5,11,11,5,12,24,9,10,8,27,4,14,10,14,4,27,8,10,11,11,25,25,10,15,15,10,25,25,11,11,12,9,8,24,29,14,12,14,29,24,8,9,12,13,13,24,9,31,31,13,13,31,31,9,24,13,13",
			"name": "Square array A(i,j) = A003188(A006068(i) + A006068(j)), read by antidiagonals as A(0,0), A(0,1), A(1,0), A(0,2), A(1,1), A(2,0), ...",
			"comment": [
				"Each row n is row A006068(n) of array A268820 without its A006068(n) initial terms."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A268715/b268715.txt\"\u003eTable of n, a(n) for n = 0..15050; the first 173 antidiagonals of the array\u003c/a\u003e"
			],
			"formula": [
				"A(i,j) = A003188(A006068(i) + A006068(j)) = A003188(A268714(i,j)).",
				"A(row,col) = A268820(A006068(row), (A006068(row)+col))."
			],
			"example": [
				"The top left [0 .. 15] x [0 .. 15] section of the array:",
				"   0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15",
				"   1,  3,  6,  2, 12,  4,  7,  5, 24,  8, 11,  9, 13, 15, 10, 14",
				"   2,  6,  5,  7, 15, 13,  4, 12, 27, 25,  8, 24, 14, 10,  9, 11",
				"   3,  2,  7,  6, 13, 12,  5,  4, 25, 24,  9,  8, 15, 14, 11, 10",
				"   4, 12, 15, 13,  9, 11, 14, 10, 29, 31, 26, 30,  8, 24, 27, 25",
				"   5,  4, 13, 12, 11, 10, 15, 14, 31, 30, 27, 26,  9,  8, 25, 24",
				"   6,  7,  4,  5, 14, 15, 12, 13, 26, 27, 24, 25, 10, 11,  8,  9",
				"   7,  5, 12,  4, 10, 14, 13, 15, 30, 26, 25, 27, 11,  9, 24,  8",
				"   8, 24, 27, 25, 29, 31, 26, 30, 17, 19, 22, 18, 28, 20, 23, 21",
				"   9,  8, 25, 24, 31, 30, 27, 26, 19, 18, 23, 22, 29, 28, 21, 20",
				"  10, 11,  8,  9, 26, 27, 24, 25, 22, 23, 20, 21, 30, 31, 28, 29",
				"  11,  9, 24,  8, 30, 26, 25, 27, 18, 22, 21, 23, 31, 29, 20, 28",
				"  12, 13, 14, 15,  8,  9, 10, 11, 28, 29, 30, 31, 24, 25, 26, 27",
				"  13, 15, 10, 14, 24,  8, 11,  9, 20, 28, 31, 29, 25, 27, 30, 26",
				"  14, 10,  9, 11, 27, 25,  8, 24, 23, 21, 28, 20, 26, 30, 29, 31",
				"  15, 14, 11, 10, 25, 24,  9,  8, 21, 20, 29, 28, 27, 26, 31, 30"
			],
			"mathematica": [
				"A003188[n_] := BitXor[n, Floor[n/2]]; A006068[n_] := BitXor @@ Table[Floor[ n/2^m], {m, 0, Log[2, n]}]; A006068[0]=0; A[i_, j_] := A003188[A006068[i] + A006068[j]]; Table[A[i-j, j], {i, 0, 13}, {j, 0, i}] // Flatten (* _Jean-François Alcover_, Feb 17 2016 *)"
			],
			"program": [
				"(Scheme)",
				"(define (A268715 n) (A268715bi (A002262 n) (A025581 n)))",
				"(define (A268715bi row col) (A003188 (+ (A006068 row) (A006068 col))))",
				";; Alternatively, extracting data from array A268820:",
				"(define (A268715bi row col) (A268820bi (A006068 row) (+ (A006068 row) col)))",
				"(Python)",
				"def a003188(n): return n^(n\u003e\u003e1)",
				"def a006068(n):",
				"    s=1",
				"    while True:",
				"        ns=n\u003e\u003es",
				"        if ns==0: break",
				"        n=n^ns",
				"        s\u003c\u003c=1",
				"    return n",
				"def T(n, k): return a003188(a006068(n) + a006068(k))",
				"for n in range(21): print([T(n - k, k) for k in range(n + 1)]) # _Indranil Ghosh_, Jun 07 2017"
			],
			"xref": [
				"Cf. A003188, A006068, A268714, A268820.",
				"Main diagonal: A001969.",
				"Row 0, column 0: A001477.",
				"Row 1, column 1: A268717.",
				"Antidiagonal sums: A268837.",
				"Cf. A268719 (the lower triangular section).",
				"Cf. also A268725."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_Antti Karttunen_, Feb 12 2016",
			"references": 11,
			"revision": 24,
			"time": "2021-03-22T03:42:38-04:00",
			"created": "2016-02-17T17:19:21-05:00"
		}
	]
}