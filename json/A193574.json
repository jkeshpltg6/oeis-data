{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A193574",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 193574,
			"data": "3,2,7,2,4,2,3,13,3,2,7,2,3,2,31,2,13,2,3,2,3,2,5,31,3,2,8,2,4,2,3,2,3,2,7,2,3,2,3,2,4,2,3,2,3,2,31,3,3,2,7,2,4,2,3,2,3,2,7,2,3,2,127,2,4,2,3,2,3,2,5,2,3,2,5,2,4,2,3",
			"name": "Smallest divisor of sigma(n) that does not divide n.",
			"comment": [
				"a(n) = 2 iff n is an odd number that is not a perfect square.",
				"From _Hartmut F. W. Hoft_, May 05 2017: (Start)",
				"(1)  Every a(n) \u003e n is a prime: Because of the minimality of a(n), a(n) = u*v with gcd(u,v)=1 leads to the contradiction (u*v)|n. Similarly, a(n)=p^k with p prime an k\u003e1 leads to the contradiction (p^k-1)/(p-1) | n.",
				"(2)  n=p^(2*k), k\u003e=1 and 2*k+1 prime, when a(n) = sigma(n) for n\u003e2: Because n having two distinct prime factors implies sigma(n) composite, and if n is an odd power of a prime then 2|sigma(n). Finally, if 2*k+1=u*v  with  u,v \u003e 1 then sigma(p^(u-1)) divides sigma(p^(2*k)), but not p^(2k), for any prime p, contradicting minimality of a(n). For example, no number sigma(p^8) for any prime p is in the sequence.",
				"(3)  The converse of (2) is false since, e.g. sigma(7^2) = 3*19 so that a(7^2) = 3, and sigma(2^10) = 23*89 so that a(2^10) = 23.",
				"(4)  Conjecture: a(n) \u003e n implies a(n) = sigma(n); tested through n = 20000000.",
				"(5)  Subsequences are: A053183 (sigma(p^2) is prime for prime p), A190527 (sigma(p^4) is prime for prime p), A194257 (sigma(p^6) is prime for prime p), A286301 (sigma(p^10) is prime for prime p)",
				"(6)  Subsequences are: A000668 (primes of form 2^p-1), A076481 (primes of form (3^p-1)/2), A086122 (primes of form (5^p-1)/4), A102170 (primes of form (7^p-1)/6), all when p is prime.",
				"(End)",
				"Up to n = 10^6, there are 89 distinct elements. For those n, a(n) is prime. If it's not, it's a power of 2, a power of 3 or a perfect square \u003c= 121. - _David A. Corneth_, May 10 2017"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A193574/b193574.txt\"\u003eTable of n, a(n) for n = 2..10000\u003c/a\u003e"
			],
			"mathematica": [
				"a193574[n_] := First[Select[Divisors[DivisorSigma[1, n]], Mod[n, #]!=0\u0026]]",
				"Map[a193574, Range[2, 80]] (* data *) (* _Hartmut F. W. Hoft_, May 05 2017 *)"
			],
			"program": [
				"(PARI) a(n)=local(ds);ds=divisors(sigma(n));for(k=2,#ds,if(n%ds[k],return(ds[k])))",
				"(Haskell)",
				"import Data.List ((\\\\))",
				"a193574 n = head [d | d \u003c- [1..sigma] \\\\ nDivisors, mod sigma d == 0]",
				"   where nDivisors = a027750_row n",
				"         sigma = sum nDivisors",
				"-- _Reinhard Zumkeller_, May 20 2015, Aug 28 2011"
			],
			"xref": [
				"Cf. A000203, A007978, A027750, A135718.",
				"Subsequences: A000668, A053183, A076481, A086122, A102170, A190527, A194257, A286301."
			],
			"keyword": "nonn",
			"offset": "2,1",
			"author": "_Franklin T. Adams-Watters_, Aug 27 2011",
			"references": 5,
			"revision": 35,
			"time": "2017-05-12T01:01:34-04:00",
			"created": "2011-08-27T13:24:49-04:00"
		}
	]
}