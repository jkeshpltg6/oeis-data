{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005802",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5802,
			"id": "M1666",
			"data": "1,1,2,6,23,103,513,2761,15767,94359,586590,3763290,24792705,167078577,1148208090,8026793118,56963722223,409687815151,2981863943718,21937062144834,162958355218089,1221225517285209,9225729232653663,70209849031116183,537935616492552297",
			"name": "Number of permutations in S_n with longest increasing subsequence of length \u003c= 3 (i.e., 1234-avoiding permutations); vexillary permutations (i.e., 2143-avoiding).",
			"comment": [
				"Also the dimension of SL(3)-invariants in V^n tensor (V^*)^n, where V is the standard 3-dimensional representation of SL(3) and V^* is its dual. - Alec Mihailovs (alec(AT)mihailovs.com), Aug 14 2005",
				"Also the number of doubly-alternating permutations of length 2n with no four-term increasing subsequence (i.e., 1234-avoiding doubly-alternating permutations). The doubly-alternating permutations (counted by sequence A007999) are those permutations w such that both w and w^(-1) have descent set {2, 4, 6, ...}. - _Joel B. Lewis_, May 21 2009",
				"Any permutation without an increasing subsequence of length 4 has a decreasing subsequence of length \u003e= n/3, where n is the length of the sequence, by the Erdős-Szekeres theorem. - _Charles R Greathouse IV_, Sep 26 2012",
				"Also the number of permutations of length n simultaneously avoiding patterns 1324 and 3416725 (or 1324 and 3612745). - _Alexander Burstein_, Jan 31 2014",
				"For any integer n \u003e 0, we have (n+2)^2*a(n) - n^2*a(n-1} = 4*A086618(n). - _Zhi-Wei Sun_, Nov 16 2017"
			],
			"reference": [
				"Eric S. Egge, Defying God: The Stanley-Wilf Conjecture, Stanley-Wilf Limits, and a Two-Generation Explosion of Combinatorics, pp. 65-82 of \"A Century of Advancing Mathematics\", ed. S. F. Kennedy et al., MAA Press 2015.",
				"S. Kitaev, Patterns in Permutations and Words, Springer-Verlag, 2011. see p. 399 Table A.7.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence).",
				"R. P. Stanley, Enumerative Combinatorics, Cambridge, Vol. 2, 1999; see Problem 7.16(e), p. 453."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A005802/b005802.txt\"\u003eTable of n, a(n) for n = 0..1060\u003c/a\u003e",
				"Michael Albert and Mireille Bousquet-Mélou, \u003ca href=\"https://arxiv.org/abs/1312.4487\"\u003ePermutations sortable by two stacks in parallel and quarter plane walks\u003c/a\u003e, arXiv:1312.4487 [math.CO], 2014-2015.",
				"Michael Albert and Mireille Bousquet-Mélou, \u003ca href=\"https://doi.org/10.1016/j.ejc.2014.08.024\"\u003ePermutations sortable by two stacks in parallel and quarter plane walks\u003c/a\u003e, European Journal of Combinatorics 43 (2015), 131-164.",
				"Alin Bostan, Andrew Elvey Price, Anthony John Guttmann, and Jean-Marie Maillard, \u003ca href=\"https://arxiv.org/abs/2001.00393\"\u003eStieltjes moment sequences for pattern-avoiding permutations\u003c/a\u003e, arXiv:2001.00393 [math.CO], 2020.",
				"A. Burstein and J. Pantone, \u003ca href=\"https://arxiv.org/abs/1402.3842\"\u003eTwo examples of unbalanced Wilf-equivalence\u003c/a\u003e, arXiv preprint arXiv:1402.3842 [math.CO], 2014.",
				"CombOS - Combinatorial Object Server, \u003ca href=\"http://combos.org/jump\"\u003eGenerate pattern-avoiding permutations\u003c/a\u003e",
				"Andrew R. Conway and Anthony J. Guttmann, \u003ca href=\"https://doi.org/10.1016/j.aam.2014.12.004\"\u003eOn 1324-avoiding permutations\u003c/a\u003e, Advances in Applied Mathematics, Volume 64, March 2015, p. 50-69.",
				"Tom Denton, \u003ca href=\"https://arxiv.org/abs/1303.3767\"\u003eAlgebraic and Affine Pattern Avoidance\u003c/a\u003e, arXiv preprint arXiv:1303.3767 [math.CO], 2013.",
				"Eric S. Egge, \u003ca href=\"/A000139/a000139.pdf\"\u003eDefying God: The Stanley-Wilf Conjecture, Stanley-Wilf Limits, and a Two-Generation Explosion of Combinatorics\u003c/a\u003e, MAA Focus, August/September 2015, pp. 33-34. [Annotated scanned copy]",
				"Shalosh B. Ekhad, Nathaniel Shar, and Doron Zeilberger, \u003ca href=\"https://arxiv.org/abs/1504.02513\"\u003eThe number of 1...d-avoiding permutations of length d+r for SYMBOLIC d but numeric r\u003c/a\u003e, arXiv:1504.02513 [math.CO], 2015.",
				"Steven Finch, \u003ca href=\"https://web.archive.org/web/20160511035941/http://www.people.fas.harvard.edu/~sfinch/csolve/av.pdf\"\u003ePattern-Avoiding Permutations\u003c/a\u003e [Cached copy at the Wayback Machine]",
				"Steven Finch, \u003ca href=\"/A240885/a240885.pdf\"\u003ePattern-Avoiding Permutations\u003c/a\u003e [Cached copy, with permission]",
				"A. L. L. Gao, S. Kitaev, and P. B. Zhang, \u003ca href=\"https://arxiv.org/abs/1605.05490\"\u003eOn pattern avoiding indecomposable permutations\u003c/a\u003e, arXiv:1605.05490 [math.CO], 2016.",
				"Ira M. Gessel, \u003ca href=\"https://doi.org/10.1016/0097-3165(90)90060-A\"\u003eSymmetric functions and P-recursiveness\u003c/a\u003e, J. Combin. Theory A 53 (1990), 257-285.",
				"O. Guibert and E. Pergola, \u003ca href=\"https://doi.org/10.1016/S0012-365X(00)00139-4\"\u003eEnumeration of vexillary involutions which are equal to their mirror/complement\u003c/a\u003e, Discrete Math., Vol. 224 (2000), pp. 281-287.",
				"Elizabeth Hartung, Hung Phuc Hoang, Torsten Mütze, and Aaron Williams, \u003ca href=\"https://arxiv.org/abs/1906.06069\"\u003eCombinatorial generation via permutation languages. I. Fundamentals\u003c/a\u003e, arXiv:1906.06069 [cs.DM], 2019.",
				"Eric Marberg, \u003ca href=\"https://arxiv.org/abs/1203.5738\"\u003eCrossings and nestings in colored set partitions\u003c/a\u003e, arXiv preprint arXiv:1203.5738 [math.CO], 2012.",
				"J. Noonan and D. Zeilberger, \u003ca href=\"http://sites.math.rutgers.edu/~zeilberg/mamarim/mamarimPDF/forbid.pdf\"\u003eThe Enumeration of Permutations With A Prescribed Number of \"Forbidden\" Patterns\u003c/a\u003e.",
				"J. Noonan and D. Zeilberger, \u003ca href=\"https://doi.org/10.1006/aama.1996.0016\"\u003eThe Enumeration of Permutations With A Prescribed Number of \"Forbidden\" Patterns\u003c/a\u003e, Advances in Applied Mathematics, Vol. 17, 1996, pp. 381-407.",
				"J. Noonan and D. Zeilberger, \u003ca href=\"https://arxiv.org/abs/math/9808080\"\u003eThe Enumeration of Permutations With a Prescribed Number of \"Forbidden\" Patterns\u003c/a\u003e, arXiv:math/9808080 [math.CO], 1998.",
				"Erik Ouchterlony, \u003ca href=\"http://garsia.math.yorku.ca/fpsac06/papers/83.pdf\"\u003ePattern avoiding doubly alternating permutations\u003c/a\u003e",
				"Nathaniel Shar, \u003ca href=\"https://doi.org/10.7282/T3BK1FKF\"\u003eExperimental methods in permutation patterns and bijective proof\u003c/a\u003e, PhD Dissertation, Mathematics Department, Rutgers University, May 2016.",
				"Anders Bjorner and Richard P. Stanley, \u003ca href=\"http://www-math.mit.edu/~rstan/papers/comb.pdf\"\u003eA combinatorial miscellany\u003c/a\u003e",
				"R. P. Stanley, \u003ca href=\"https://doi.org/10.1090/S0273-0979-02-00966-7\"\u003eRecent Progress in Algebraic Combinatorics\u003c/a\u003e, Bull. Amer. Math. Soc., 40 (2003), 55-68."
			],
			"formula": [
				"a(n) = 2 * Sum_{k=0..n} binomial(2*k, k) * (binomial(n, k))^2 * (3*k^2 + 2*k+1 - n - 2*k*n)/((k+1)^2 * (k+2) * (n-k+1)).",
				"(4*n^2 - 2*n + 1)*(n + 2)^2*(n + 1)^2*a(n) = (44*n^3 - 14*n^2 - 11*n + 8)*n*(n + 1)^2*a(n - 1) - (76*n^4 + 42*n^3 - 49*n^2 - 24*n + 24)*(n - 1)^2*a(n - 2) + 9*(4*n^2 + 6*n + 3)*(n - 1)^2*(n - 2)^2*a(n - 3). - _Vladeta Jovovic_, Jul 16 2004",
				"a(0) = 1, a(1) = 1, (n^2 + 8*n + 16)*a(n + 2) = (10*n^2 + 42*n + 41) a(n + 1) - (9*n^2 + 18*n + 9) a(n). - Alec Mihailovs (alec(AT)mihailovs.com), Aug 14 2005",
				"a(n) = ((18*n+45)*A002893(n) - (7+2*n)*A002893(n+1)) / (6*(n+2)^2). - _Mark van Hoeij_, Jul 02 2010",
				"G.f.: (1+5*x-(1-9*x)^(3/4)*(1-x)^(1/4)*hypergeom([-1/4, 3/4],[1],64*x/((x-1)*(1-9*x)^3)))/(6*x^2). - _Mark van Hoeij_, Oct 25 2011",
				"a(n) ~ 3^(2*n+9/2)/(16*Pi*n^4). - _Vaclav Kotesovec_, Jul 29 2013",
				"a(n) = Sum_{k=0..n} binomial(2k,k)*binomial(n+1,k+1)*binomial(n+2,k+1)/((n+1)^2*(n+2)). [Conway and Guttmann, Adv. Appl. Math. 64 (2015) 50]",
				"a(n) = hypergeom([1/2, -1 - n, -n], [2, 2], 4) / (n+1). - _Vaclav Kotesovec_, Jun 07 2021"
			],
			"maple": [
				"a:= n-\u003e 2*add(binomial(2*k,k)*(binomial(n,k))^2*(3*k^2+2*k+1-n-2*k*n)/ (k+1)^2/(k+2)/(n-k+1),k=0..n);",
				"A005802:=rsolve({a(0) = 1, a(1) = 1, (n^2 + 8*n + 16)*a(n + 2) = (10*n^2 + 42*n + 41)*a(n + 1) - (9*n^2 + 18*n + 9)*a(n)},a(n),makeproc): # Alec Mihailovs (alec(AT)mihailovs.com), Aug 14 2005"
			],
			"mathematica": [
				"a[n_] := 2Sum[Binomial[2k, k]Binomial[n, k]^2(3k^2+2k+1-n-2k*n)/((k+1)^2(k+2)(n-k+1)), {k, 0, n}]",
				"(* Second program:*)",
				"a[0] = a[1] = 1; a[n_] := a[n] = ((10*n^2+2*n-3)*a[n-1] + (-9*n^2+18*n-9)* a[n-2])/(n+2)^2; Table[a[n], {n, 0, 25}] (* _Jean-François Alcover_, Feb 20 2017 *)",
				"Table[HypergeometricPFQ[{1/2, -1 - n, -n}, {2, 2}, 4] / (n+1), {n, 0, 25}] (* _Vaclav Kotesovec_, Jun 07 2021 *)"
			],
			"program": [
				"(PARI) a(n)=2*sum(k=0,n,binomial(2*k,k)*binomial(n,k)^2*(3*k^2+2*k+1-n-2*k*n)/(k+1)^2/(k+2)/(n-k+1)) \\\\ _Charles R Greathouse IV_, Sep 26 2012"
			],
			"xref": [
				"A column of A047888. See also A224318, A223034, A223905.",
				"Column k=3 of A214015.",
				"A005802, A022558, A061552 are representatives for the three Wilf classes for length-four avoiding permutations (cf. A099952)."
			],
			"keyword": "nonn,nice",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_, _Simon Plouffe_",
			"ext": [
				"Additional comments from _Emeric Deutsch_, Dec 06 2000",
				"More terms from _Naohiro Nomoto_, Jun 18 2001",
				"Edited by _Dean Hickerson_, Dec 10 2002",
				"More terms from Alec Mihailovs (alec(AT)mihailovs.com), Aug 14 2005"
			],
			"references": 30,
			"revision": 153,
			"time": "2021-11-19T17:31:03-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}