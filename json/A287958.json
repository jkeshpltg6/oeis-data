{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A287958",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 287958,
			"data": "1,2,2,3,2,3,4,6,6,4,5,4,3,4,5,6,10,12,12,10,6,7,6,15,4,15,6,7,8,14,6,20,20,6,14,8,9,8,21,12,5,12,21,8,9,10,18,24,28,30,30,28,24,18,10,11,10,9,64,35,6,35,64,9,10,11,12,22,30,36,40,42,42,40",
			"name": "Table read by antidiagonals: T(n, k) = least recursive multiple of n and k; n \u003e 0 and k \u003e 0.",
			"comment": [
				"We say that m is a recursive multiple of d iff d is a recursive divisor of m (as described in A282446).",
				"More informally, the prime tower factorization of T(n, k) is the union of the prime tower factorizations of n and k (the prime tower factorization of a number is defined in A182318).",
				"This sequence has connections with the classical LCM (A003990).",
				"For any i \u003e 0, j \u003e 0 and k \u003e 0:",
				"- A007947(T(i, j)) = A007947(lcm(i, j)),",
				"- T(i, j) \u003e= 1,",
				"- T(i, j) \u003e= max(i, j),",
				"- T(i, j) \u003e= lcm(i, j),",
				"- T(i, 1) = i,",
				"- T(i, i) = i,",
				"- T(i, j) = T(j, i) (the sequence is commutative),",
				"- T(i, T(j, k)) = T(T(i, j), k) (the sequence is associative),",
				"- T(i, i*j) \u003e= i*j,",
				"- if gcd(i, j) = 1 then T(i, j) = i*j.",
				"See also A287957 for the GCD equivalent."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A287958/b287958.txt\"\u003eFirst 100 antidiagonals of array, flattened\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A287958/a287958.pdf\"\u003eIllustration of the first terms\u003c/a\u003e"
			],
			"example": [
				"Table starts:",
				"n\\k|     1   2   3   4   5   6   7   8   9  10",
				"---+-----------------------------------------------",
				"1  |     1   2   3   4   5   6   7   8   9  10  ...",
				"2  |     2   2   6   4  10   6  14   8  18  10  ...",
				"3  |     3   6   3  12  15   6  21  24   9  30  ...",
				"4  |     4   4  12   4  20  12  28  64  36  20  ...",
				"5  |     5  10  15  20   5  30  35  40  45  10  ...",
				"6  |     6   6   6  12  30   6  42  24  18  30  ...",
				"7  |     7  14  21  28  35  42   7  56  63  70  ...",
				"8  |     8   8  24  64  40  24  56   8  72  40  ...",
				"9  |     9  18   9  36  45  18  63  72   9  90  ...",
				"10 |    10  10  30  20  10  30  70  40  90  10  ...",
				"...",
				"T(4, 8) = T(2^2, 2^3) = 2^(2*3) = 2^6 = 64."
			],
			"program": [
				"(PARI) T(n,k) = if (n*k==0, return (max(n,k))); my (g=factor(lcm(n,k))); return (prod(i=1, #g~, g[i,1]^T(valuation(n, g[i,1]), valuation(k, g[i,1]))))"
			],
			"xref": [
				"Cf. A003990, A007947, A182318, A282446, A287957."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Rémy Sigrist_, Jun 03 2017",
			"references": 3,
			"revision": 12,
			"time": "2017-06-04T16:39:08-04:00",
			"created": "2017-06-04T16:39:08-04:00"
		}
	]
}