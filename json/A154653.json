{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A154653",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 154653,
			"data": "1,1,1,1,6,1,1,15,15,1,1,45,210,210,1,1,66,495,924,66,1,1,120,1820,8008,8008,1820,1,1,153,3060,18564,43758,18564,153,1,1,231,7315,74613,646646,646646,74613,7315,1,1,378,20475,376740,13123110,30421755,30421755,13123110,376740,1",
			"name": "Triangular T(n,k) = binomial(prime(n+1) - 1, prime(k+1) - 1)) with T(n,0) = 1, read by rows.",
			"comment": [
				"Row sums are: {1, 2, 8, 32, 467, 1553, 19778, 84254, 1457381, 87864065, 354929117, ...}."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A154653/b154653.txt\"\u003eRows n = 0..100 of triangle, flattened\u003c/a\u003e",
				"A. Lakhtakia, R. Messier, V. K. Varadan, V. V. Varadan, \u003ca href=\"http://dx.doi.org/10.1103/PhysRevA.34.2501\"\u003eUse of combinatorial algebra for diffusion on fractals\u003c/a\u003e, Physical Review A, volume 34, Number 3 (1986) p. 2503 (7b)."
			],
			"formula": [
				"T(n,k) = binomial(prime(n+1) - 1, prime(k+1) - 1)) with T(n,0) = 1."
			],
			"example": [
				"Triangle begins as:",
				"  1;",
				"  1,   1;",
				"  1,   6,    1;",
				"  1,  15,   15,     1;",
				"  1,  45,  210,   210,      1;",
				"  1,  66,  495,   924,     66,      1;",
				"  1, 120, 1820,  8008,   8008,   1820,     1;",
				"  1, 153, 3060, 18564,  43758,  18564,   153,    1;",
				"  1, 231, 7315, 74613, 646646, 646646, 74613, 7315, 1;"
			],
			"maple": [
				"seq(seq( `if`(k=0, 1, binomial(ithprime(n+1)-1, ithprime(k+1)-1) ), k=0..n), n=0..10); # _G. C. Greubel_, Dec 02 2019"
			],
			"mathematica": [
				"T[n_, k_]:= If[k==0, 1, Binomial[Prime[n+1] -1, Prime[k+1] -1]]; Table[T[n, k], {n,0,10}, {k,0,n}]//Flatten"
			],
			"program": [
				"(PARI) T(n,k) = if(k==0, 1, binomial(prime(n+1)-1, prime(k+1)-1) ); \\\\ _G. C. Greubel_, Dec 02 2019",
				"(MAGMA) [k eq 0 select 1 else Binomial(NthPrime(n+1)-1, NthPrime(k+1)-1): k in [0..n], n in [0..10]]; // _G. C. Greubel_, Dec 02 2019",
				"(Sage)",
				"def T(n, k):",
				"    if (k==0): return 1",
				"    else: return binomial(nth_prime(n+1)-1, nth_prime(k+1)-1)",
				"[[T(n, k) for k in (0..n)] for n in (0..10)] # _G. C. Greubel_, Dec 02 2019"
			],
			"xref": [
				"Cf. A154652."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Roger L. Bagula_, Jan 13 2009",
			"ext": [
				"Edited by _G. C. Greubel_, Dec 02 2019"
			],
			"references": 2,
			"revision": 16,
			"time": "2019-12-03T10:54:45-05:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}