{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A182700",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 182700,
			"data": "0,1,1,4,2,2,9,6,3,3,20,12,8,4,4,35,25,15,10,5,5,66,42,30,18,12,6,6,105,77,49,35,21,14,7,7,176,120,88,56,40,24,16,8,8,270,198,135,99,63,45,27,18,9,9,420,300,220,150,110,70,50,30,20,10,10,616,462,330,242,165,121,77,55,33",
			"name": "Triangle T(n,k) = n*A000041(n-k), 0\u003c=k\u003c=n, read by rows.",
			"comment": [
				"T(n,k) is the sum of the parts of all partitions of n that contain k as a part, assuming that all partitions of n have 0 as a part: Thus, column 0 gives the sum of the parts of all partitions of n.",
				"By definition all entries in row n\u003e0 are divisible by n.",
				"Row sums are 0, 2, 8, 21, 48, 95, 180, 315, 536, 873, 1390, 2145,...",
				"The partitions of n+k that contain k as a part can be obtained by adding k to every partition of n assuming that all partitions of n have 0 as a part.",
				"For example, the partitions of 6+k that contain k as a part are",
				"k + 6",
				"k + 3 + 3",
				"k + 4 + 2",
				"k + 2 + 2 + 2",
				"k + 5 + 1",
				"k + 3 + 2 + 1",
				"k + 4 + 1 + 1",
				"k + 2 + 2 + 1 + 1",
				"k + 3 + 1 + 1 + 1",
				"k + 2 + 1 + 1 + 1 + 1",
				"k + 1 + 1 + 1 + 1 + 1 + 1",
				"The partition number A000041(n) is also the number of partitions of m*(n+k) into parts divisible by m and that contain m*k as a part, with k\u003e=0, m\u003e=1, n\u003e=0 and assuming that all partitions of n have 0 as a part."
			],
			"link": [
				"Robert Price, \u003ca href=\"/A182700/b182700.txt\"\u003eTable of n, a(n) for n = 0..5150\u003c/a\u003e (First 100 rows)"
			],
			"formula": [
				"T(n,0) = A066186(n).",
				"T(n,k) = A182701(n,k), n\u003e=1 and k\u003e=1.",
				"T(n,n) = n = min { T(n,k); 0\u003c=k\u003c=n }."
			],
			"example": [
				"For n=7 and k=4 there are 3 partitions of 7 that contain 4 as a part. These partitions are (4+3)=7, (4+2+1)=7 and (4+1+1+1)=7. The sum is 7+7+7 = 7*3 = 21. By other way, the partition number of 7-4 is A000041(3) = p(3)=3, then 7*3 = 21, so T(7,4) = 21.",
				"Triangle begins with row n=0 and columns 0\u003c=k\u003c=n :",
				"0,",
				"1, 1,",
				"4, 2, 2,",
				"9, 6, 3, 3,",
				"20,12,8, 4, 4,",
				"35,25,15,10,5, 5,",
				"66,42,30,18,12,6, 6"
			],
			"maple": [
				"A182700 := proc(n,k) n*combinat[numbpart](n-k) ; end proc:",
				"seq(seq(A182700(n,k),k=0..n),n=0..15) ;"
			],
			"mathematica": [
				"Table[n*PartitionsP[n-k], {n, 0, 11}, {k, 0, n}] // Flatten (* _Robert Price_, Jun 23 2020 *)"
			],
			"program": [
				"(PARI) A182700(n,k) = n*numbpart(n-k)"
			],
			"xref": [
				"Cf. A000041, A027293, A135010, A138121.",
				"Two triangles that are essentially the same as this are A027293 and A140207. - _N. J. A. Sloane_, Nov 28 2010",
				"Row sums give A182704."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_Omar E. Pol_, Nov 27 2010",
			"references": 8,
			"revision": 24,
			"time": "2020-06-23T19:03:52-04:00",
			"created": "2010-11-27T17:24:30-05:00"
		}
	]
}