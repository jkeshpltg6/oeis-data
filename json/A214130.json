{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A214130",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 214130,
			"data": "1,0,1,1,1,1,2,1,2,2,3,3,4,4,5,6,7,7,9,9,11,12,14,15,18,19,23,24,28,30,35,37,43,46,52,56,64,68,77,84,93,101,113,121,135,146,161,174,193,207,229,247,272,292,322,346,379,408,446,479,524,562,613,659",
			"name": "Partitions of n into parts congruent to +-2, +-3 (mod 13).",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A214130/b214130.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of f(-x^13)^2 / (f(-x^2, -x^11) * f(-x^3, -x^10)) in powers of x where f() is Ramanujan's two-variable theta function.",
				"Euler transform of period 13 sequence [ 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, ...].",
				"G.f.: 1 / (Product_{k\u003e0} (1 - x^(13*k - 2)) * (1 - x^(13*k - 3)) * (1 - x^(13*k - 10)) * (1 - x^(13*k - 11))).",
				"A214129(n) = a(n) + A214131(n-1)."
			],
			"example": [
				"1 + x^2 + x^3 + x^4 + x^5 + 2*x^6 + x^7 + 2*x^8 + 2*x^9 + 3*x^10 + ...",
				"q^-1 + q^11 + q^17 + q^23 + q^29 + 2*q^35 + q^41 + 2*q^47 + 2*q^53 + ..."
			],
			"maple": [
				"with (numtheory):",
				"a:= proc(n) option remember; `if`(n=0, 1, add(add(",
				"      `if`(irem(d, 13) in [2, 3, 10, 11], d, 0),",
				"          d=divisors(j)) *a(n-j), j=1..n)/n)",
				"    end:",
				"seq(a(n), n=0..100);  # _Alois P. Heinz_, Oct 23 2013"
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ 1 / (QPochhammer[ q^2, q^13] QPochhammer[ q^3, q^13] QPochhammer[ q^10, q^13] QPochhammer[ q^11, q^13]), {q, 0, n}]",
				"a[n_] := a[n] = If[n == 0, 1, Sum[Sum[If[MemberQ[{2, 3, 10, 11}, Mod[d, 13]], d, 0], {d, Divisors[j]}]*a[n-j], {j, 1, n}]/n]; Table[a[n], {n, 0, 100}] (* _Jean-François Alcover_, Nov 11 2015, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, 0, polcoeff( 1 / prod( k=1, n, 1 - [ 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0][k%13 + 1] * x^k, 1 + x * O(x^n)), n))}"
			],
			"xref": [
				"Cf. A214129, A214131."
			],
			"keyword": "nonn",
			"offset": "0,7",
			"author": "_Michael Somos_, Jul 04 2012",
			"references": 3,
			"revision": 20,
			"time": "2021-03-12T22:24:46-05:00",
			"created": "2012-07-05T04:47:30-04:00"
		}
	]
}