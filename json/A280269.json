{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A280269",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 280269,
			"data": "0,0,1,0,1,0,1,1,0,1,0,1,1,2,1,0,1,0,1,1,1,0,1,1,0,1,2,1,3,1,0,1,0,1,1,1,1,2,2,1,0,1,0,1,2,1,3,1,0,1,1,2,1,0,1,1,1,1,0,1,0,1,1,2,1,3,1,2,4,1,0,1,0,1,1,1,2,1,2,1,0,1,1,2,1,0,1,2,3,1,4,1,0,1,0,1,1,1,1,1",
			"name": "Irregular triangle T(n,m) read by rows: smallest power e of n that is divisible by m = term k in row n of A162306.",
			"comment": [
				"This table eliminates the negative values in row n of A279907.",
				"Let k = A162306(n,m), i.e., the value in column m of row n.",
				"T(n,1) = 0 since 1 | n^0.",
				"T(n,p) = 1 for prime divisors p of n since p | n^1.",
				"T(n,d) = 1 for divisors d \u003e 1 of n since d | n^1.",
				"Row n for prime p have two terms, {0,1}, the maximum value 1, since all k \u003c p are coprime to p, and k | p^1 only when k = p.",
				"Row n for prime power p^i have (i+1) terms, one zero and i ones, since all k that appear in corresponding row n of A162306 are divisors d of p^i.",
				"Values greater than 1 pertain only to composite k of composite n \u003e 4, but not in all cases. T(n,k) = 1 for squarefree kernels k of composite n.",
				"Numbers k \u003e 1 coprime to n and numbers that are products of at least one prime q coprime to n and one prime p | n do not appear in A162306; these do not divide n^e evenly.",
				"T(n,k) is nonnegative for all numbers k for which n^k (mod k) = 0, i.e., all the prime divisors p of k also divide n.",
				"The largest possible value s in row n of T = floor(log_2(n)), since the largest possible multiplicity of any number m \u003c= n pertains to perfect powers of 2, as 2 is the smallest prime. This number s first appears at T(2^s + 2, 2^s) for s \u003e 1.",
				"1/k terminates T(n,k) digits after the radix point in base n for values of k that appear in row n of A162306.",
				"Originally from _Robert Israel_ at A279907: (Start)",
				"T(a*b,c*d) = max(T(a,c),T(b,d)) if GCD(a,b)=1, GCD(b,d)=1,T(a,c)\u003e=0 and T(b,d)\u003e=0.",
				"T(n,a*b) = max(T(n,a),T(n,b)) if GCD(a,b)=1 and T(n,a)\u003e=0 and T(n,b)\u003e=0.",
				"(End)"
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A280269/b280269.txt\"\u003eTable of n, a(n) for n = 1..10202\u003c/a\u003e (rows 1 \u003c= n \u003c= 660)"
			],
			"example": [
				"Triangle T(n,m) begins:  Triangle A162036(n,k):",
				"1:  0                    1",
				"2:  0  1                 1  2",
				"3:  0  1                 1  3",
				"4:  0  1  1              1  2  4",
				"5:  0  1                 1  5",
				"6:  0  1  1  2  1        1  2  3  4  6",
				"7:  0  1                 1  7",
				"8:  0  1  1  1           1  2  4  8",
				"9:  0  1  1              1  3  9",
				"10: 0  1  2  1  3  1     1  2  4  5  8  10",
				"..."
			],
			"mathematica": [
				"Table[SelectFirst[Range[0, #], PowerMod[n, #, k] == 0 \u0026] /. m_ /; MissingQ@ m -\u003e Nothing \u0026@ Floor@ Log2@ n, {n, 24}, {k, n}] // Flatten (* Version 10.2, or *)",
				"DeleteCases[#, -1] \u0026 /@ Table[If[# == {}, -1, First@ #] \u0026@ Select[Range[0, #], PowerMod[n, #, k] == 0 \u0026] \u0026@ Floor@ Log2@ n, {n, 24}, {k, n}] // Flatten (* or *)",
				"DeleteCases[#, -1] \u0026 /@ Table[Boole[k == 1] + (Boole[#[[-1, 1]] == 1] (-1 + Length@ #) /. 0 -\u003e -1) \u0026@ NestWhileList[Function[s, {#1/s, s}]@ GCD[#1, #2] \u0026 @@ # \u0026, {k, n}, And[First@# != 1, ! CoprimeQ @@ #] \u0026], {n, 24}, {k, n}] // Flatten"
			],
			"xref": [
				"Cf. A162306, A279907 (T(n,k) with values for all 1 \u003c= k \u003c= n), A280274 (maximum values in row n), A010846 (number of nonnegative k in row n), A051731 (k with e \u003c= 1), A000005 (number of k in row n with e \u003c= 1), A272618 (k with e \u003e 1), A243822 (number of k in row n with e \u003e 1), A007947."
			],
			"keyword": "nonn,tabf,easy",
			"offset": "1,14",
			"author": "_Michael De Vlieger_, Dec 30 2016",
			"references": 4,
			"revision": 12,
			"time": "2021-06-27T03:40:05-04:00",
			"created": "2016-12-31T01:31:34-05:00"
		}
	]
}