{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A080856",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 80856,
			"data": "1,5,25,61,113,181,265,365,481,613,761,925,1105,1301,1513,1741,1985,2245,2521,2813,3121,3445,3785,4141,4513,4901,5305,5725,6161,6613,7081,7565,8065,8581,9113,9661,10225,10805,11401,12013,12641,13285,13945,14621",
			"name": "a(n) = 8*n^2 - 4*n + 1.",
			"comment": [
				"The old definition of this sequence was \"Generalized polygonal numbers\".",
				"Row T(4,n) of A080853.",
				"{a(k): 0 \u003c= k \u003c 3} = divisors of 25. - _Reinhard Zumkeller_, Jun 17 2009",
				"Let A be the Hessenberg matrix of order n, defined by: A[1,j]=1, A[i,i]:=4, (i\u003e1), A[i,i-1]=-1, and A[i,j]=0 otherwise. Then, for n\u003e=3, a(n-1)= coeff(charpoly(A,x),x^(n-2)). - _Milan Janjic_, Jan 27 2010",
				"Also sequence found by reading the segment (1, 5) together with the line from 5, in the direction 5, 25,..., in the square spiral whose vertices are the generalized hexagonal numbers A000217. - _Omar E. Pol_, Nov 05 2012",
				"For n \u003e 0: A049061(a(n)) = 0, when the triangle of \"signed Eulerian numbers\" in A049061 is seen as flattened sequence. - _Reinhard Zumkeller_, Jan 31 2013"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A080856/b080856.txt\"\u003eTable of n, a(n) for n = 0..5000\u003c/a\u003e",
				"R. Zumkeller, \u003ca href=\"/A161700/a161700.txt\"\u003eEnumerations of Divisors\u003c/a\u003e.",
				"M. Janjic, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL13/Janjic/janjic33.html\"\u003eHessenberg Matrices and Integer Sequences \u003c/a\u003e, J. Int. Seq. 13 (2010) # 10.7.8",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-3,1)."
			],
			"formula": [
				"G.f.: (1+2*x+13*x^2)/(1-x)^3.",
				"a(n) = A060820(n), n\u003e0. - _R. J. Mathar_, Sep 18 2008",
				"a(n) = C(n,0) + 4*C(n,1) + 16*C(n,2). - _Reinhard Zumkeller_, Jun 17 2009",
				"a(n) = 16*n+a(n-1)-12 with n\u003e0, a(0)=1. - _Vincenzo Librandi_, Aug 08 2010",
				"E.g.f.: (8*x^2 + 4*x + 1)*exp(x). - _G. C. Greubel_, Jun 16 2017"
			],
			"maple": [
				"A080856:=n-\u003e8*n^2 - 4*n + 1: seq(A080856(n), n=0..100); # _Wesley Ivan Hurt_, Jul 16 2017"
			],
			"mathematica": [
				"LinearRecurrence[{3, -3, 1}, {1, 5, 25}, 80] (* _Vladimir Joseph Stephan Orlovsky_, Feb 17 2012 *)"
			],
			"program": [
				"(PARI) a(n)=8*n^2-4*n+1 \\\\ _Charles R Greathouse IV_, Sep 24 2015"
			],
			"xref": [
				"Cf. A005408, A000124, A016813, A086514, A000125, A058331, A002522, A161701, A161702, A161703, A000127, A161704, A161706, A161707, A161708, A161710, A161711, A161712, A161713, A161715, A006261."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Paul Barry_, Feb 23 2003",
			"ext": [
				"Definition replaced with the closed form by _Bruno Berselli_, Jan 16 2013"
			],
			"references": 25,
			"revision": 36,
			"time": "2017-07-17T02:13:25-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}