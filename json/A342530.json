{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A342530",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 342530,
			"data": "1,2,2,3,2,6,2,6,3,6,2,12,2,6,6,9,2,12,2,12,6,6,2,28,3,6,6,12,2,26,2,14,6,6,6,31,2,6,6,28,2,26,2,12,12,6,2,52,3,12,6,12,2,28,6,28,6,6,2,66,2,6,12,25,6,26,2,12,6,26,2,76,2,6,12,12,6,26",
			"name": "Number of strict chains of divisors ending with n and having distinct first quotients.",
			"comment": [
				"The first quotients of a sequence are defined as if the sequence were an increasing divisor chain, so for example the quotients of (6,3,1) are (1/2,1/3)."
			],
			"link": [
				"Gus Wiseman, \u003ca href=\"/A069916/a069916.txt\"\u003eSequences counting and ranking partitions and compositions by their differences and quotients\u003c/a\u003e."
			],
			"example": [
				"The a(1) = 1 through a(12) = 12 chains (reversed):",
				"  1  2    3    4    5    6      7    8      9    10      11    12",
				"     2/1  3/1  4/1  5/1  6/1    7/1  8/1    9/1  10/1    11/1  12/1",
				"               4/2       6/2         8/2    9/3  10/2          12/2",
				"                         6/3         8/4         10/5          12/3",
				"                         6/2/1       8/2/1       10/2/1        12/4",
				"                         6/3/1       8/4/1       10/5/1        12/6",
				"                                                               12/2/1",
				"                                                               12/3/1",
				"                                                               12/4/1",
				"                                                               12/4/2",
				"                                                               12/6/1",
				"                                                               12/6/2",
				"Not counted under a(12) are: 12/4/2/1, 12/6/2/1, 12/6/3, 12/6/3/1."
			],
			"mathematica": [
				"cmi[n_]:=Prepend[Prepend[#,n]\u0026/@Join@@cmi/@Most[Divisors[n]],{n}];",
				"Table[Length[Select[cmi[n],UnsameQ@@Divide@@@Partition[#,2,1]\u0026]],{n,100}]"
			],
			"xref": [
				"The version for weakly increasing first quotients is A057567.",
				"The version for equal first quotients is A169594.",
				"The case of chains starting with 1 is A254578.",
				"The version for strictly increasing first quotients is A342086.",
				"A001055 counts factorizations (strict: A045778, ordered: A074206).",
				"A067824 counts strict chains of divisors ending with n.",
				"A167865 counts strict chains of divisors \u003e 1 summing to n.",
				"A253249 counts strict chains of divisors.",
				"A334997 counts chains of divisors of n by length.",
				"A342495/A342529 count compositions with equal/distinct quotients.",
				"A342496/A342514 count partitions with equal/distinct quotients.",
				"A342515/A342520 count strict partitions with equal/distinct quotients.",
				"A342522/A342521 rank partitions with equal/distinct quotients.",
				"Cf. A000009, A003238, A003242, A122651, A179254, A318991, A318992, A325545."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Gus Wiseman_, Mar 25 2021",
			"references": 6,
			"revision": 8,
			"time": "2021-03-25T12:44:13-04:00",
			"created": "2021-03-25T12:44:13-04:00"
		}
	]
}