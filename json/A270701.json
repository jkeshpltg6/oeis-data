{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A270701",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 270701,
			"data": "1,1,3,2,4,9,5,9,16,30,15,25,41,67,112,52,82,127,195,299,463,203,307,456,670,979,1429,2095,877,1283,1845,2623,3702,5204,7307,10279,4140,5894,8257,11437,15717,21485,29278,39848,54267,21147,29427,40338,54692,73561,98367,131007,174029,230884,306298",
			"name": "Total sum T(n,k) of the sizes of all blocks with maximal element k in all set partitions of {1,2,...,n}; triangle T(n,k), n\u003e=1, 1\u003c=k\u003c=n, read by rows.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A270701/b270701.txt\"\u003eRows n = 1..141, flattened\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Partition_of_a_set\"\u003ePartition of a set\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = A270702(n,n-k+1)."
			],
			"example": [
				"Row n=3 is [2, 4, 9] = [0+0+0+1+1, 0+2+1+0+1, 3+1+2+2+1] because the set partitions of {1,2,3} are: 123, 12|3, 13|2, 1|23, 1|2|3.",
				"Triangle T(n,k) begins:",
				":     1;",
				":     1,    3;",
				":     2,    4,    9;",
				":     5,    9,   16,    30;",
				":    15,   25,   41,    67,   112;",
				":    52,   82,  127,   195,   299,   463;",
				":   203,  307,  456,   670,   979,  1429,  2095;",
				":   877, 1283, 1845,  2623,  3702,  5204,  7307, 10279;",
				":  4140, 5894, 8257, 11437, 15717, 21485, 29278, 39848, 54267;"
			],
			"maple": [
				"b:= proc(n, m, t) option remember; `if`(n=0, [1, 0], add(",
				"     `if`(t=1 and j\u003c\u003em+1, 0, (p-\u003ep+`if`(j=-t or t=1 and j=m+1,",
				"      [0, p[1]], 0))(b(n-1, max(m, j), `if`(t=1 and j=m+1, -j,",
				"     `if`(t\u003c0, t, `if`(t\u003e0, t-1, 0)))))), j=1..m+1))",
				"    end:",
				"T:= (n, k)-\u003e b(n, 0, max(0, 1+n-k))[2]:",
				"seq(seq(T(n, k), k=1..n), n=1..12);"
			],
			"mathematica": [
				"b[n_, m_, t_] := b[n, m, t] = If[n == 0, {1, 0}, Sum[If[t == 1 \u0026\u0026 j != m+1, 0, Function[p, p + If[j == -t || t == 1 \u0026\u0026 j == m+1, {0, p[[1]]}, 0]][b[ n-1, Max[m, j], If[t == 1 \u0026\u0026 j == m+1, -j, If[t \u003c 0, t, If[t \u003e 0, t-1, 0] ]]]]], {j, 1, m+1}]];",
				"T[n_, k_] := b[n, 0, Max[0, 1+n-k]][[2]];",
				"Table[Table[T[n, k], {k, 1, n}], {n, 1, 12}] // Flatten (* _Jean-François Alcover_, Apr 24 2016, translated from Maple *)"
			],
			"xref": [
				"Columns k=1-10 give: A000110(n-1), A270756, A270757, A270758, A270759, A270760, A270761, A270762, A270763, A270764.",
				"Main and lower diagonals give: A124427, A270765, A270766, A270767, A270768, A270769, A270770, A270771, A270772, A270773.",
				"Row sums give A070071.",
				"Reflected triangle gives A270702.",
				"T(2n-1,n) gives A270703."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Alois P. Heinz_, Mar 21 2016",
			"references": 23,
			"revision": 22,
			"time": "2016-04-24T05:36:34-04:00",
			"created": "2016-03-22T13:31:52-04:00"
		}
	]
}