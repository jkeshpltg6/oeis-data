{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A342720",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 342720,
			"data": "0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,2,0,1,3,1,2,2,3,1,4,2,4,2,5,3,7,1,2,4,3,13,7,20,12,5,3,7,10,3,8,2,14,12,10,15,17,8,11,10,20,13,15,10,45,9,18,25,46,38,18,2,25,20,30,18,32,17,32,43",
			"name": "a(n) is the number of concave integer quadrilaterals (up to congruence) with integer side lengths a,b,c,d with n=Max(a,b,c,d) and integer diagonals e,f.",
			"comment": [
				"Without loss of generality we assume that a is the largest side length and that the diagonal e divides the concave quadrilateral into two triangles with sides a,b,e and c,d,e. Then e \u003c a is a necessary condition for concavity. The triangle inequality further implies e \u003e a-b and abs(e-c) \u003c d \u003c e+c."
			],
			"example": [
				"a(15)=1 because the only concave integer quadrilateral with longest edge length 15 has a=15, b=13, c=13, d=15 and diagonals e=4 and f=24. a(20)=3 because there are three solutions (a,b,c,d,e,f): (20,13,15,18,9,26), (20,13,13,20,11,24) and {20,15,15,20,7,24}."
			],
			"mathematica": [
				"an={};",
				"he[a_,b_,e_]:=1/(2 e) Sqrt[(-((a-b-e) (a+b-e) (a-b+e) (a+b+e)))]",
				"paX[e_]:={e,0} (*vertex A coordinate*)",
				"pbX[a_,b_,e_]:={(-a^2+b^2+e^2)/(2 e),he[a,b,e]}(*vertex B coordinate*)",
				"pc={0,0};(*vertex C coordinate*)",
				"pdX[c_,d_,e_]:={(c^2-d^2+e^2)/(2 e),-he[c,d,e]}(*vertex D coordinate*)",
				"concaveQ[{bx_,by_},{dx_,dy_},e_]:=If[by dx-bx dy\u003c0||by dx-bx dy\u003e(by-dy) e,True,False]",
				"gQ[x_,y_]:=Module[{z=x-y,res=False},Do[If[z[[i]]\u003e0,res=True;Break[],",
				"  If[z[[i]]\u003c0,Break[]]],{i,1,4}];res]",
				"canonicalQ[{a_,b_,c_,d_}]:=Module[{m={a,b,c,d}},If[(gQ[{b,a,d,c},m]||gQ[{d,c,b,a},m]||gQ[{c,d,a,b},m]),False,True]]",
				"Do[cnt=0;",
				"Do[pa=paX[e];pb=pbX[a,b,e];pd=pdX[c,d,e];",
				"If[(f=Sqrt[(pb-pd).(pb-pd)];IntegerQ[f])\u0026\u0026concaveQ[pb,pd,e]\u0026\u0026canonicalQ[{a,b,c,d}],cnt++",
				"(*;Print[{{a,b,c,d,e,f},Graphics[Line[{pa,pb,pc,pd,pa}]]}]*)],",
				"{b,1,a},{e,a-b+1,a-1},{c,1,a},{d,Abs[e-c]+1,Min[a,e+c-1]}];",
				"AppendTo[an,cnt],",
				"{a,1,75}",
				"]",
				"an"
			],
			"xref": [
				"Cf. A340858 for trapezoids, A342721 for concave integer quadrilaterals with integer area."
			],
			"keyword": "nonn",
			"offset": "1,17",
			"author": "_Herbert Kociemba_, Mar 19 2021",
			"references": 5,
			"revision": 13,
			"time": "2021-04-16T00:12:59-04:00",
			"created": "2021-04-16T00:12:59-04:00"
		}
	]
}