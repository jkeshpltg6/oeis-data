{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A328176",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 328176,
			"data": "1,0,1,2,1,2,1,0,3,0,1,2,1,2,1,4,1,2,1,4,3,2,1,4,5,0,1,4,1,4,1,0,3,0,5,6,1,2,1,0,1,6,1,2,3,2,1,4,7,0,1,4,1,2,1,4,3,0,1,4,1,2,1,8,5,2,1,2,3,4,1,8,1,0,5,2,3,4,1,8,9,0,1,6,1,2,1",
			"name": "a(n) is the maximal value of the expression d AND (n/d) where d runs through the divisors of n and AND denotes the bitwise AND operator.",
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A328176/b328176.txt\"\u003eTable of n, a(n) for n = 1..16384\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A328176/a328176.png\"\u003eScatterplot of the first 2^16 terms\u003c/a\u003e"
			],
			"formula": [
				"a(n)^2 \u003c= n with equality iff n is a square.",
				"a(n) = 1 for any odd prime number p.",
				"a(n) \u003c= A327987(n).",
				"a(n) = 0 iff n belongs to A327988."
			],
			"example": [
				"For n = 12:",
				"- we have the following values:",
				"    d   12/d  d AND (12/d)",
				"    --  ----  ------------",
				"     1    12             0",
				"     2     6             2",
				"     3     4             0",
				"     4     3             0",
				"     6     2             2",
				"    12     1             0",
				"- hence a(12) = max({0, 2}) = 2."
			],
			"maple": [
				"a:= n-\u003e max(map(d-\u003e Bits[And](d, n/d), numtheory[divisors](n))):",
				"seq(a(n), n=1..100);  # _Alois P. Heinz_, Oct 09 2019"
			],
			"program": [
				"(PARI) a(n) = vecmax(apply(d -\u003e bitand(d, n/d), divisors(n)))"
			],
			"xref": [
				"See A328177 and A328178 for similar sequences.",
				"Cf. A327987, A327988."
			],
			"keyword": "nonn,base",
			"offset": "1,4",
			"author": "_Rémy Sigrist_, Oct 06 2019",
			"references": 3,
			"revision": 19,
			"time": "2019-10-12T08:34:11-04:00",
			"created": "2019-10-09T04:19:42-04:00"
		}
	]
}