{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A292371",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 292371,
			"data": "0,1,0,0,2,3,2,2,0,1,0,0,0,1,0,0,4,5,4,4,6,7,6,6,4,5,4,4,4,5,4,4,0,1,0,0,2,3,2,2,0,1,0,0,0,1,0,0,0,1,0,0,2,3,2,2,0,1,0,0,0,1,0,0,8,9,8,8,10,11,10,10,8,9,8,8,8,9,8,8,12,13,12,12,14,15,14,14,12,13,12,12,12,13,12,12,8,9,8,8,10,11,10,10,8,9,8,8,8,9,8,8,8",
			"name": "A binary encoding of 1-digits in the base-4 representation of n.",
			"link": [
				"Antti Karttunen, \u003ca href=\"/A292371/b292371.txt\"\u003eTable of n, a(n) for n = 0..65536\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"https://practical-ardinghelli-959d8f.netlify.app/a292371\"\u003eInteractive scatterplot of (a(n), A292372(n), A292373(n)) for n=0..4^8-1\u003c/a\u003e [provided your web browser supports the Plotly library, you should see icons on the top right corner of the page:  if you choose \"Orbital rotation\", then you will be able to rotate the plot alongside three axes, the 3D plot here corresponds to a Sierpiński triangle-based pyramid]",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A059905(A292272(n)) = A059905(n AND A003188(n)), where AND is bitwise-AND (A004198).",
				"For all n \u003e= 0, A000120(a(n)) = A160381(n)."
			],
			"example": [
				"   n      a(n)     base-4(n)  binary(a(n))",
				"                  A007090(n)  A007088(a(n))",
				"  --      ----    ----------  ------------",
				"   1        1          1           1",
				"   2        0          2           0",
				"   3        0          3           0",
				"   4        2         10          10",
				"   5        3         11          11",
				"   6        2         12          10",
				"   7        2         13          10",
				"   8        0         20           0",
				"   9        1         21           1",
				"  10        0         22           0",
				"  11        0         23           0",
				"  12        0         30           0",
				"  13        1         31           1",
				"  14        0         32           0",
				"  15        0         33           0",
				"  16        4        100         100",
				"  17        5        101         101",
				"  18        4        102         100"
			],
			"mathematica": [
				"Table[FromDigits[IntegerDigits[n, 4] /. k_ /; IntegerQ@ k :\u003e If[k == 1, 1, 0], 2], {n, 0, 112}] (* _Michael De Vlieger_, Sep 21 2017 *)"
			],
			"program": [
				"(Scheme, with memoization-macro definec)",
				"(definec (A292371 n) (if (zero? n) n (let ((d (modulo n 4))) (+ (if (= 1 d) 1 0) (* 2 (A292371 (/ (- n d) 4)))))))",
				"(Python)",
				"from sympy.ntheory.factor_ import digits",
				"def a(n):",
				"    k=digits(n, 4)[1:]",
				"    return 0 if n==0 else int(\"\".join('1' if i==1 else '0' for i in k), 2)",
				"print([a(n) for n in range(116)]) # _Indranil Ghosh_, Sep 21 2017"
			],
			"xref": [
				"Cf. A003188, A004198, A007088, A007090, A059905, A160381, A292272, A292370, A292372, A292373.",
				"Cf. A289813 (analogous sequence for base 3)."
			],
			"keyword": "nonn,base,changed",
			"offset": "0,5",
			"author": "_Antti Karttunen_, Sep 15 2017",
			"references": 7,
			"revision": 37,
			"time": "2022-01-08T12:28:59-05:00",
			"created": "2017-09-18T09:24:54-04:00"
		}
	]
}