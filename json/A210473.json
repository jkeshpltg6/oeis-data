{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A210473",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 210473,
			"data": "3,0,1,0,9,3,1,7,6,3,5,8,3,9,9,8,9,4",
			"name": "Decimal expansion of Sum_{n\u003e=1} 1/(prime(n)*prime(n+1)).",
			"comment": [
				"Sum of reciprocals of products of successive primes. Differs from A209329 only by the initial term 1/(2*3) = 1/6 = 0.16666...",
				"Since prime(n+1) \u003e prime(n), we have prime(n)*prime(n+1) \u003e prime(n)^2 and 1/(prime(n)*prime(n+1)) \u003c 1/prime(n)^2. Because of the convergence of Sum_{n\u003e=1} 1/prime(n)^2 (A085548), Sum_{n\u003e=1} 1/(prime(n)*prime(n+1)) also converges. - _Paolo P. Lava_, May 21 2013"
			],
			"formula": [
				"Equals 1/6 + A209329."
			],
			"example": [
				"0.3010931763... = Sum_{n\u003e=1} 1/(prime(n)*prime(n+1)).",
				"= 1/(2*3) + 1/(3*5) + 1/(5*7)",
				"+ 0.03731790933454338 (primes 10 \u003c p(n+1) \u003c 100)",
				"+ 0.0017430141479028 (primes 100 \u003c p(n+1) \u003c 10^3)",
				"+ 0.00011767024549033 (primes 10^3 \u003c p(n+1) \u003c 10^4)",
				"+ 9.018426684045269 e-6 (primes 10^4 \u003c p(n+1) \u003c 10^5)",
				"+ 7.3452282601302 e-7 (primes 10^5 \u003c p(n+1) \u003c 10^6)",
				"+ 6.19161299373 e-8 (primes 10^6 \u003c p(n+1) \u003c 10^7)",
				"+ 5.3439026467 e-9 (primes 10^7 \u003c p(n+1) \u003c 10^8)",
				"+ 4.70035656 e-10 (primes 10^8 \u003c p(n+1) \u003c 10^9) + ..."
			],
			"maple": [
				"A210473:=proc(q) local n;",
				"print(evalf(add(1/(ithprime(n)*ithprime(n+1)),n=1..q),200));",
				"end: A210473(10^6); # _Paolo P. Lava_, May 21 2013"
			],
			"mathematica": [
				"digits = 10;",
				"f[n_Integer] := 1/(Prime[n]*Prime[n+1]);",
				"s = NSum[f[n], {n, 1, Infinity}, Method -\u003e \"WynnEpsilon\", NSumTerms -\u003e 2*10^6, WorkingPrecision -\u003e MachinePrecision];",
				"RealDigits[s, 10, digits][[1]] (* _Jean-François Alcover_, Sep 05 2017 *)"
			],
			"program": [
				"(PARI) S(L=10^9,start=3)={my(s=0,q=1/precprime(start));forprime(p=1/q+1,L,s+=q*q=1./p);s} \\\\ Using 1./p is maybe a little less precise, but using s=0. and 1/p takes about 50% more time.",
				"(PARI) {my( tee(x)=printf(\"%g,\",x);x ); t=vector(8,n,tee(S(10^(n+1),10^n))); s=1/2/3+1/3/5+1/5/7; vector(#t,n,s+=t[n])} \\\\ Shows contribution of sums over (n+1)-digit primes (vector t) and the vector of partial sums; the final value is in s."
			],
			"xref": [
				"Cf. A085548, A209329, A185380."
			],
			"keyword": "nonn,cons,more",
			"offset": "0,1",
			"author": "_M. F. Hasler_, Jan 23 2013",
			"ext": [
				"Corrected and extended by _Hans Havermann_, Mar 17 2013 using the additional terms of A209329 from _R. J. Mathar_, Feb 08 2013"
			],
			"references": 4,
			"revision": 34,
			"time": "2019-01-28T02:26:21-05:00",
			"created": "2013-01-23T10:42:27-05:00"
		}
	]
}