{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A145596",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 145596,
			"data": "1,2,2,3,8,3,4,20,20,4,5,40,75,40,5,6,70,210,210,70,6,7,112,490,784,490,112,7,8,168,1008,2352,2352,1008,168,8,9,240,1890,6048,8820,6048,1890,240,9,10,330,3300,13860,27720,27720,13860,3300,330,10",
			"name": "Triangular array of generalized Narayana numbers: T(n, k) = 2*binomial(n + 1, k + 1)*binomial(n + 1, k - 1)/(n + 1).",
			"comment": [
				"T(n,k) is the number of walks of n unit steps on the square lattice (i.e., each step in the direction either up (U), down (D), right (R) or left (L)) starting from (0,0) and finishing at points on the horizontal line y = 1, which remain in the upper half-plane y \u003e= 0. An example is given in the Example section below.",
				"The current array is the case r = 1 of the generalized Narayana numbers N_r(n, k) := (r + 1)/(n + 1)*binomial(n + 1, k + r)*binomial(n + 1, k - 1), which count walks of n steps from the origin to points on the horizontal line y = r that remain in the upper half-plane. Case r = 0 gives the table of Narayana numbers A001263 (but with row numbering starting at n = 0). For other cases see A145597 (r = 2), A145598 (r = 3) and A145599 (r = 4).",
				"T(n,k) is the number of preimages of the permutation 21345...(n+2) under West's stack-sorting map that have exactly k descents. - _Colin Defant_, Sep 15 2018"
			],
			"link": [
				"F. Cai, Q.-H. Hou, Y. Sun, A. L. B. Yang, \u003ca href=\"http://arxiv.org/abs/1808.05736\"\u003eCombinatorial identities related to 2x2 submatrices of recursive matrices\u003c/a\u003e, arXiv:1808.05736 [math.CO],2018; Table 2.1 for k=1.",
				"C. Defant, \u003ca href=\"https://arxiv.org/abs/1511.05681\"\u003ePreimages under the stack-sorting algorithm\u003c/a\u003e, arXiv:1511.05681 [math.CO], 2015-2018; Graphs Combin., 33 (2017), 103-122.",
				"C. Defant, \u003ca href=\"https://arxiv.org/abs/1809.03123\"\u003eStack-sorting preimages of permutation classes\u003c/a\u003e, arXiv:1809.03123 [math.CO], 2018.",
				"R. K. Guy, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL3/GUY/catwalks.html\"\u003eCatwalks, sandsteps and Pascal pyramids\u003c/a\u003e, J. Integer Sequences, Vol. 3 (2000), Article #00.1.6.",
				"T. Mansour, Y. Sun, \u003ca href=\"http://arxiv.org/abs/0805.1274\"\u003eIdentities involving Narayana polynomials and Catalan numbers\u003c/a\u003e, arXiv:0805.1274 [math.CO], 2008."
			],
			"formula": [
				"T(n,k) = 2/(n + 1)*binomial(n + 1,k + 1)*binomial(n + 1,k - 1) for 1 \u003c= k \u003c= n. In the notation of [Guy], T(n,k) equals w_n(x,y) at (x,y) = (2*k - n - 1,1).",
				"O.g.f. for column (k + 2): 2/(k + 1) * y^(k+2)/(1 - y)^(k+4) * Jacobi_P(k,2,1,(1 + y)/(1 - y)). The column generating functions begin: column 2: 2*y^2/(1 - y)^4; column 3: y^3*(3 + 2*y)/(1 - y)^6; column 4: y^4*(4 + 8*y + 2*y^2)/(1 - y)^8; the polynomials in the numerators are the row generating polynomials of array A108838.",
				"O.g.f. for array: 1/(2*x*y^3) * (((1 + x)*y - 1)*sqrt(1 - 2*(1 + x)*y + (y - x*y)^2) + x^2*y^2 - 2*x*y + (1 - y)^2) = x*y + (2*x + 2*x^2)*y^2 + (3*x + 8*x^2 + 3*x^3)*y^3 + (4*x + 20*x^2 + 20*x^3 + 4*x^4)*y^4 + ....",
				"Row sums A002057.",
				"Identities for row polynomials R_n(x) = Sum_{k = 1..n} T(n,k)*x^k (compare with the results in section 1 of [Mansour \u0026 Sun]):",
				"x*R_(n-1)(x) = 2*(n - 1)/((n + 1)*(n + 2)) * Sum_{k = 0..n} binomial(n + 2,k) * binomial(2*n - k,n) * (x - 1)^k;",
				"R_n(x) = Sum_{k = 0..floor((n-1)/2)} binomial(n, 2*k + 1) * Catalan(k + 1) * x^(k+1)*(1 + x)^(n-2k-1);",
				"Sum {k = 1..n} (-1)^(n-k)*binomial(n,k)*R_k(x)*(1 + x)^(n-k) = x^m*Catalan(m) if n = 2*m - 1 is odd, otherwise the sum is zero.",
				"Sum {k = 1..n} (-1)^(k+1)*binomial(n,k)*R_k(x^2)*(1 + x)^(2*(n-k)) = R_n(1)*x^(n+1) = 4/(n + 3)*binomial(2*n + 1,n - 1)*x^(n+1) = A002057(n-1)*x^(n+1).",
				"Row generating polynomial R_(n+1)(x) = 2/(n + 2)*x*(1 - x)^n * Jacobi_P(n,2,2,(1 + x)/(1 - x)). - _Peter Bala_, Oct 31 2008",
				"G.f. satisfies x^3*y*A(x,y)^2-A(x,y)*(x^2*y^2+(-2)*x*y+x^2+(-2)*x+1)+x=0. - _Vladimir Kruchinin_, Oct 11 2020"
			],
			"example": [
				"n\\k|..1.....2....3.....4.....5.....6",
				"====================================",
				".1.|..1",
				".2.|..2.....2",
				".3.|..3.....8....3",
				".4.|..4....20...20.....4",
				".5.|..5....40...75....40.....5",
				".6.|..6....70..210...210....70.....6",
				"...",
				"Row 3 entries:",
				"T(3,1) = 3: the 3 walks from (0,0) to (-2,1) of three steps are LLU, LUL and ULL.",
				"T(3,2) = 8: the 8 walks from (0,0) to (0,1) of three steps are UDU, UUD, ULR, URL, RLU, LRU, RUL and LUR.",
				"T(3,3) = 3: the 3 walks from (0,0) to (2,1) of three steps are RRU, RUR and URR.",
				".",
				".",
				"*......*......*......y......*......*......*",
				".",
				".",
				"*......3......*......8......*......3......*",
				".",
				".",
				"*......*......*......o......*......*......* x-axis",
				".",
				"."
			],
			"maple": [
				"T:= (n, k) -\u003e 2/(n+1)*binomial(n+1, k+1)*binomial(n+1, k-1):",
				"for n from 1 to 10 do seq(T(n,k), k = 1..n) end do;"
			],
			"mathematica": [
				"t[n_, k_]:=2/(n+1) Binomial[n+1, k+1] Binomial[n+1, k-1]; Table[t[n, k], {n, 3, 10}, {k, n}]//Flatten (* _Vincenzo Librandi_, Sep 15 2018 *)"
			],
			"program": [
				"(MAGMA) /* As triangle */  [[2/(n+1)*Binomial(n+1,k+1)*Binomial(n+1,k-1): k in [1..n]]: n in [1.. 15]]; // _Vincenzo Librandi_, Sep 15 2018"
			],
			"xref": [
				"Cf. A002057 (row sums), A001263, A108838, A145597, A145598, A145599, A145600."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "1,2",
			"author": "_Peter Bala_, Oct 14 2008",
			"references": 8,
			"revision": 38,
			"time": "2020-10-14T11:05:54-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}