{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A097108",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 97108,
			"data": "1,2,3,6,9,9,16,20,18,30,36,30,49,56,45,72,81,63,100,110,84,132,144,108,169,182,135,210,225,165,256,272,198,306,324,234,361,380,273,420,441,315,484,506,360,552,576,408,625,650,459,702,729,513,784,812,570,870",
			"name": "If a geodesic dome is made by dividing each triangle of an icosahedron into n^2 identical equilateral triangles and the vertices of those newly created triangles are pushed out from the center to lie on the surface of the sphere in which the icosahedron is inscribed, then this sequence gives the number of different strut lengths that are required to build the dome.",
			"comment": [
				"From _Gonzalo Rodríguez Whipple_, May 30 2010: (Start)",
				"The sequence consists of two series, one of which consists of two subseries.",
				"1. If n is a multiple of 3: n^2/6+n/2.",
				"2.1 If n=1 or n+1 is a multiple of 6 or n-1 is a multiple of 6: n^2/4+n/2+1/4.",
				"2.2 Otherwise: n^2/4+n/2.",
				"This means that frequencies (n) that are multiples of 3 assure a higher symmetry and need a smaller number of different strut lengths. (End)"
			],
			"reference": [
				"Mohammad K. Azarian, A Trigonometric Characterization of  Equilateral Triangle, Problem 336, Mathematics and Computer Education, Vol. 31, No. 1, Winter 1997, p. 96.  Solution published in Vol. 32, No. 1, Winter 1998, pp. 84-85.",
				"Mohammad K. Azarian, Equating Distances and Altitude in an Equilateral Triangle, Problem 316, Mathematics and Computer Education, Vol. 28, No. 3, Fall 1994, p. 337.  Solution published in Vol. 29, No. 3, Fall 1995, pp. 324-325."
			],
			"link": [
				"Tara Landry, \u003ca href=\"http://www.desertdomes.com\"\u003eDesert Domes\u003c/a\u003e.",
				"\u003ca href=\"/index/Rec#order_12\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,0,2,0,0,0,0,0,-2,0,0,1)."
			],
			"formula": [
				"Satisfies a linear recurrence with characteristic polynomial (1+x^3)(1-x^3)^3.",
				"From _Gonzalo Rodríguez Whipple_, May 30 2010: (Start)",
				"a(n) = if(n mod 3, (n^2)/6+n/2, 0)",
				"+ if(((n+1) mod 6) or ((n-1) mod 6), (n^2)/4+n/2+1/4), 0)",
				"+ if(((n+2) mod 6) or ((n-2) mod 6), (n^2)/4+n/2), 0). (End)",
				"G.f.: -x*(2*x^7+4*x^6+3*x^5+5*x^4+4*x^3+3*x^2+2*x+1)/((x-1)^3*(x+1)*(x^2-x+1)*(x^2+x+1)^3). [_Colin Barker_, Oct 21 2012]",
				"a(n) = ((2*n^2+6*n)*(1-(n^2 mod 3))+3*(n+1)^2*(n^2 mod (5-(-1)^n)/2)+(3*n^2 +6*n)*((n+3)^2 mod (5+(-1)^n)/2))/12. - _Wesley Ivan Hurt_, Mar 11 2015",
				"A045943(n) = a(3*(n-1)), n\u003c\u003e1. - _Gonzalo Rodríguez Whipple_, May 30 2010"
			],
			"example": [
				"a(4) = 6 since we can build a \"4V\" dome of radius 1 using 30 struts of length 0.25318, 30 struts of length 0.29453, 70 of length 0.31287, 30 of length 0.32492 and 30 of length 0.29859. The number 6 indicates the number of different jig settings we'd have to use to manufacture all the struts for this dome.",
				"a(299)=22500, a(300)=15150, a(301)=22801. [_Gonzalo Rodríguez Whipple_, May 30 2010]"
			],
			"mathematica": [
				"Table[If[Divisible[n, 3], (n^2)/6 + n/2, 0] + If[Divisible[n + 1, 6] || Divisible[n - 1, 6], (n^2)/4 + n/2 + 1/4, 0] + If[Divisible[n + 2, 6] || Divisible[n - 2, 6], (n^2)/4 + n/2, 0], {n, 100}] (* _Gonzalo Rodríguez Whipple_, May 30 2010 *)"
			],
			"xref": [
				"Cf. A045943."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "Tom Davis (tomrdavis(AT)earthlink.net), Sep 15 2004",
			"references": 0,
			"revision": 32,
			"time": "2015-06-13T00:51:23-04:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}