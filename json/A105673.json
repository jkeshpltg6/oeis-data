{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A105673",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 105673,
			"data": "2,2,0,2,4,0,0,2,2,4,0,0,4,0,0,2,4,2,0,4,0,0,0,0,6,4,0,0,4,0,0,2,0,4,0,2,4,0,0,4,4,0,0,0,4,0,0,0,2,6,0,4,4,0,0,0,0,4,0,0,4,0,0,2,8,0,0,4,0,0,0,2,4,4,0,0,0,0,0,4,2,4,0,0,8,0,0,0,4,4,0,0,0,0,0,0,4,2,0",
			"name": "One-half of theta series of square lattice (or half the number of ways of writing n \u003e 0 as a sum of 2 squares), without the constant term, which is 1/2.",
			"comment": [
				"This is the Jacobi elliptic function K(q)/Pi - 1/2 [see Fine]."
			],
			"reference": [
				"N. J. Fine, Basic Hypergeometric Series and Applications, Amer. Math. Soc., 1988; Eq. (34.4)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A105673/b105673.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2), A(x^4)) where f(u, v, w) = (u-v)^2 - (v-w) * (4*w + 2). - _Michael Somos_, May 13 2005",
				"a(n) = 2 * A002654(n). - _Michael Somos_, Jan 25 2017"
			],
			"example": [
				"G.f. = 2*q + 2*q^2 + 2*q^4 + 4*q^5 + 2*q^8 + 2*q^9 + 4*q^10 + 4*q^13 + 2*q^16 + ..."
			],
			"mathematica": [
				"CoefficientList[Series[(EllipticTheta[3, 0, x]^2 - 1)/(2 x), {x, 0, 100}], x] (* _Jan Mangaldan_, Jan 04 2017 *)",
				"a[ n_] := If[ n \u003c 1, 0, SquaresR[ 2, n] / 2]; (* _Michael Somos_, Jan 25 2017 *)",
				"a[ n_] := If[ n \u003c 1, 0, 2 DivisorSum[ n, KroneckerSymbol[ -4, #] \u0026]]; (* _Michael Somos_, Jan 25 2017 *)",
				"a[ n_] := SeriesCoefficient[ (EllipticTheta[ 3, 0, q]^2 - 1) / 2, {q, 0, n}]; (* _Michael Somos_, Jan 25 2017 *)"
			],
			"program": [
				"(PARI) qfrep([1, 0; 0, 1], 100)",
				"(PARI) {a(n) = if( n\u003c1, 0, qfrep([1, 0; 0, 1], n)[n])}; /* _Michael Somos_, May 13 2005 */",
				"(PARI) {a(n) = if( n\u003c1, 0, 2 * sumdiv( n, d, (d%4==1) - (d%4==3)))}; /* _Michael Somos_, Jan 25 2017 */"
			],
			"xref": [
				"(Theta_3)^2 is given in A004018.",
				"Equals A004018(n)/2 for n \u003e 0."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_, May 05 2005",
			"references": 5,
			"revision": 21,
			"time": "2017-12-11T02:40:00-05:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}