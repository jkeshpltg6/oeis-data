{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A257505",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 257505,
			"data": "1,4,2,18,12,3,96,72,16,5,600,480,90,22,6,4320,3600,576,114,48,7,35280,30240,4200,696,360,52,8,322560,282240,34560,4920,2880,378,60,9,3265920,2903040,317520,39600,25200,2976,432,64,10,36288000,32659200,3225600,357840,241920,25800,3360,450,66,11,439084800,399168000,35925120,3588480,2540160,246240,28800,3456,456,70,13",
			"name": "Square array A(row,col): A(row,1) = A256450(row-1), and for col \u003e 1, A(row,col) = A255411(A(row,col-1)); Dispersion of factorial base shift A255411.",
			"comment": [
				"The array is read by downward antidiagonals: A(1,1), A(1,2), A(2,1), A(1,3), A(2,2), A(3,1), etc.",
				"In Kimberling's terminology, this array is called the dispersion of sequence A255411 (when started from its first nonzero term, 4). The left column is the complement of that sequence, which is A256450."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A257505/b257505.txt\"\u003eTable of n, a(n) for n = 1..1275; the first 50 antidiagonals of array\u003c/a\u003e",
				"Clark Kimberling, \u003ca href=\"http://faculty.evansville.edu/ck6/integer/intersp.html\"\u003eInterspersions and Dispersions\u003c/a\u003e.",
				"Clark Kimberling, \u003ca href=\"http://www.jstor.org/discover/10.2307/2159163\"\u003eInterspersions and Dispersions\u003c/a\u003e, Proceedings of the American Mathematical Society, 117 (1993) 313-321.",
				"\u003ca href=\"/index/Fa#facbase\"\u003eIndex entries for sequences related to factorial base representation\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"A(row,1) = A256450(row-1), and for col \u003e 1, A(row,col) = A255411(A(row,col-1))."
			],
			"example": [
				"The top left corner of the array:",
				"   1,   4,  18,   96,   600,   4320,   35280,   322560,   3265920",
				"   2,  12,  72,  480,  3600,  30240,  282240,  2903040,  32659200",
				"   3,  16,  90,  576,  4200,  34560,  317520,  3225600,  35925120",
				"   5,  22, 114,  696,  4920,  39600,  357840,  3588480,  39553920",
				"   6,  48, 360, 2880, 25200, 241920, 2540160, 29030400, 359251200",
				"   7,  52, 378, 2976, 25800, 246240, 2575440, 29352960, 362517120",
				"   8,  60, 432, 3360, 28800, 272160, 2822400, 31933440, 391910400",
				"   9,  64, 450, 3456, 29400, 276480, 2857680, 32256000, 395176320",
				"  10,  66, 456, 3480, 29520, 277200, 2862720, 32296320, 395539200",
				"  11,  70, 474, 3576, 30120, 281520, 2898000, 32618880, 398805120",
				"  13,  76, 498, 3696, 30840, 286560, 2938320, 32981760, 402433920",
				"  14,  84, 552, 4080, 33840, 312480, 3185280, 35562240, 431827200",
				"  15,  88, 570, 4176, 34440, 316800, 3220560, 35884800, 435093120",
				"  17,  94, 594, 4296, 35160, 321840, 3260880, 36247680, 438721920",
				"  19, 100, 618, 4416, 35880, 326880, 3301200, 36610560, 442350720",
				"  20, 108, 672, 4800, 38880, 352800, 3548160, 39191040, 471744000",
				"  21, 112, 690, 4896, 39480, 357120, 3583440, 39513600, 475009920",
				"  23, 118, 714, 5016, 40200, 362160, 3623760, 39876480, 478638720",
				"  ..."
			],
			"program": [
				"(Scheme)",
				"(define (A257505 n) (A257505bi (A002260 n) (A004736 n)))",
				"(define (A257505bi row col) (if (= 1 col) (A256450 (- row 1)) (A255411 (A257505bi row (- col 1)))))"
			],
			"xref": [
				"Transpose: A257503.",
				"Inverse permutation: A257506.",
				"Row index: A257681, Column index: A257679.",
				"Columns 1-3: A256450, A257692, A257693.",
				"Rows 1-3: A001563, A062119, A130744 (without their initial zero-terms).",
				"Row 4: A213167 (without the initial one).",
				"Row 5: A052571 (without initial zeros).",
				"Cf. A007623, A256450, A255411.",
				"Cf. also permutations A255565, A255566.",
				"Thematically similar arrays: A035513, A054582, A246279."
			],
			"keyword": "nonn,base,tabl",
			"offset": "1,2",
			"author": "_Antti Karttunen_, Apr 27 2015",
			"ext": [
				"Formula changed because of the changed starting offset of A256450 - _Antti Karttunen_, May 30 2016"
			],
			"references": 15,
			"revision": 31,
			"time": "2016-09-20T13:25:43-04:00",
			"created": "2015-05-07T23:11:45-04:00"
		}
	]
}