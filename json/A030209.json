{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A030209",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 30209,
			"data": "1,-2,-3,4,6,6,-16,-8,9,-12,12,-12,38,32,-18,16,-126,-18,20,24,48,-24,168,24,-89,-76,-27,-64,30,36,-88,-32,-36,252,-96,36,254,-40,-114,-48,42,-96,-52,48,54,-336,-96,-48,-87,178,378,152,198,54,72,128",
			"name": "Expansion of (eta(q) * eta(q^2) * eta(q^3) * eta(q^6))^2 in powers of q.",
			"comment": [
				"Identical to table 1, p. 493, of Alaca citation. - _Jonathan Vos Post_, May 24 2007",
				"Unique cusp form of weight 4 for congruence group Gamma_1(6). - _Michael Somos_, Aug 11 2011",
				"Number 14 of the 74 eta-quotients listed in Table I of Martin (1996).",
				"The table 1, p. 493 of Alaca reference is the first 50 values of c_6(n). - _Michael Somos_, May 17 2015"
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A030209/b030209.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Saban Alaca and Kenneth Williams, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2006.10.004\"\u003eEvaluation of the convolution sums sum_{l+6m=n} sigma(l)*sigma(m) and sum_{2l+3m=n} sigma(l)*sigma(m)\u003c/a\u003e, J. Number Theory 124 (2007), no. 2, 491-510. MR2321376 (2008a:11005)",
				"K. Bringmann and K. Ono, \u003ca href=\"http://www.pnas.org/content/104/10/3725.abstract\"\u003eLifting cusp forms to Maass forms with an application to partitions\u003c/a\u003e, Proc. Natl. Acad. Sci. USA, 104 (2010), 3725-3731.",
				"Brian Conrey and David Farmer (?), \u003ca href=\"http://www.math.okstate.edu/~loriw/degree2/degree2hm/eta2/eta2.html\"\u003eEta Products and Quotients which are Newforms\u003c/a\u003e.",
				"M. Koike, \u003ca href=\"http://projecteuclid.org/euclid.nmj/1118787564\"\u003eOn McKay's conjecture\u003c/a\u003e, Nagoya Math. J., 95 (1984), 85-89.",
				"LMFDB, \u003ca href=\"http://www.lmfdb.org/ModularForm/GL2/Q/holomorphic/6/4/0/\"\u003eNewforms of weight 4 on Gamma_0(6)\u003c/a\u003e.",
				"Y. Martin, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9947-96-01743-6\"\u003eMultiplicative eta-quotients\u003c/a\u003e, Trans. Amer. Math. Soc. 348 (1996), no. 12, 4825-4856, see page 4852 Table I.",
				"Michael Somos, \u003ca href=\"/A030203/a030203.txt\"\u003eIndex to Yves Martin's list of 74 multiplicative eta-quotients and their A-numbers\u003c/a\u003e"
			],
			"formula": [
				"Euler transform of period 6 sequence [ -2, -4, -4, -4, -2, -8, ...]. - _Michael Somos_, Feb 13 2006",
				"a(n) is multiplicative with a(p^e) = (-p)^e if p\u003c5, a(p^e) = a(p) * a(p^(e-1)) - p^3 * a(p^(e-2)) otherwise. - _Michael Somos_, Feb 13 2006",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (6 t)) = 36 (t/i)^4 f(t) where q = exp(2 Pi i t). - _Michael Somos_, Aug 11 2011",
				"G.f.: x * (Product_{k\u003e0} (1 - x^k) * (1 - x^(2*k)) * (1 - x^(3*k)) * (1 - x^(6*k)))^2.",
				"a(2*n) = -2 * a(n). Convolution square of A030188. - _Michael Somos_, May 27 2012",
				"Convolution with A181102 is A186100. - _Michael Somos_, Jul 07 2015"
			],
			"example": [
				"G.f. = q - 2*q^2 - 3*q^3 + 4*q^4 + 6*q^5 + 6*q^6 - 16*q^7 - 8*q^8 + 9*q^9 - 12*q^10 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ q (QPochhammer[ q] QPochhammer[ q^2] QPochhammer[ q^3] QPochhammer[ q^6])^2, {q, 0, n}]; (* _Michael Somos_, Aug 11 2011 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c1, 0, n--; A = x * O(x^n); polcoeff( (eta(x + A) * eta(x^2 + A) * eta(x^3 + A) * eta(x^6 + A))^2, n))}; /* _Michael Somos_, Feb 14 2006 */",
				"(Sage) CuspForms( Gamma1(6), 4, prec = 57).0; # _Michael Somos_, Aug 11 2011",
				"(MAGMA) Basis( CuspForms( Gamma1(6), 4), 57) [1]; /* _Michael Somos_, May 17 2015 */"
			],
			"xref": [
				"Cf. A030188, A181101, A186100."
			],
			"keyword": "sign,mult",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_.",
			"references": 6,
			"revision": 55,
			"time": "2018-11-22T21:50:11-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}