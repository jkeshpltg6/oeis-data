{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A053985",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 53985,
			"data": "0,1,-2,-1,4,5,2,3,-8,-7,-10,-9,-4,-3,-6,-5,16,17,14,15,20,21,18,19,8,9,6,7,12,13,10,11,-32,-31,-34,-33,-28,-27,-30,-29,-40,-39,-42,-41,-36,-35,-38,-37,-16,-15,-18,-17,-12,-11,-14,-13,-24,-23,-26,-25,-20,-19",
			"name": "Replace 2^k with (-2)^k in binary expansion of n.",
			"comment": [
				"Base 2 representation for n (in lexicographic order) converted from base -2 to base 10.",
				"Maps natural numbers uniquely onto integers; within each group of positive values, maximum is in A002450; a(n)=n iff n can be written only with 1's and 0's in base 4 (A000695).",
				"a(n) = A004514(n) - n. - _Reinhard Zumkeller_, Dec 27 2003",
				"Schroeppel gives formula n = (a(n) + b) XOR b where b = binary ...101010, and notes this formula is reversible.  The reverse a(n) = (n XOR b) - b is a bit twiddle to transform 1 bits to -1.  Odd position 0 or 1 in n is flipped by \"XOR b\" to 1 or 0, then \"- b\" gives 0 or -1.  Only odd position 1's are changed, so b can be any length sure to cover those. - _Kevin Ryde_, Jun 26 2020"
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A053985/b053985.txt\"\u003eTable of n, a(n) for n = 0..8191\u003c/a\u003e",
				"Michael Beeler, R. William Gosper, Richard Schroeppel, \u003ca href=\"https://dspace.mit.edu/handle/1721.1/6086\"\u003eHAKMEM\u003c/a\u003e, MIT Artificial Intelligence Laboratory report AIM-239, February 1972, item 128 by Schroeppel, page 62.  Also \u003ca href=\"http://www.inwap.com/pdp10/hbaker/hakmem/Figure7.html\"\u003eHTML transcription\u003c/a\u003e.  Figure 7 path drawn 0, 1, -2, -1, 4, ... is the present sequence.",
				"Dana G. Korssjoen, Biyao Li, Stefan Steinerberger, Raghavendra Tripathi, and Ruimin Zhang, \u003ca href=\"https://arxiv.org/abs/2012.04625\"\u003eFinding structure in sequences of real numbers via graph theory: a problem list\u003c/a\u003e, arXiv:2012.04625, Dec 08, 2020",
				"Ralf Stephan, \u003ca href=\"/somedcgf.html\"\u003eSome divide-and-conquer sequences ...\u003c/a\u003e",
				"Ralf Stephan, \u003ca href=\"/A079944/a079944.ps\"\u003eTable of generating functions\u003c/a\u003e"
			],
			"formula": [
				"From _Ralf Stephan_, Jun 13 2003: (Start)",
				"G.f.: (1/(1-x)) * Sum_{k\u003e=0} (-2)^k*x^2^k/(1+x^2^k).",
				"a(0) = 0, a(2*n) = -2*a(n), a(2*n+1) = -2*a(n)+1. (End)",
				"a(n) = Sum_{k\u003e=0} A030308(n,k)*A122803(k). - _Philippe Deléham_, Oct 15 2011",
				"a(n) = (n XOR b) - b where b = binary ..101010 [Schroeppel].  Any b of this form (A020988) with bitlength(b) \u003e= bitlength(n) suits. - _Kevin Ryde_, Jun 26 2020"
			],
			"example": [
				"a(9)=-7 because 9 is written 1001 base 2 and (-2)^3 + (-2)^0 = -8 + 1 = -7.",
				"Or by Schroeppel's formula, b = binary 1010 then a(9) = (1001 XOR 1010) - 1010 = decimal -7. - _Kevin Ryde_, Jun 26 2020"
			],
			"mathematica": [
				"f[n_Integer, b_Integer] := Block[{l = IntegerDigits[n]}, Sum[l[[ -i]]*(-b)^(i - 1), {i, 1, Length[l]}]]; a = Table[ FromDigits[ IntegerDigits[n, 2]], {n, 0, 80}]; b = {}; Do[b = Append[b, f[a[[n]], 2]], {n, 1, 80}]; b",
				"(* Second program: *)",
				"Array[FromDigits[IntegerDigits[#, 2], -2] \u0026, 62, 0] (* _Michael De Vlieger_, Jun 27 2020 *)"
			],
			"program": [
				"(PARI) a(n) = fromdigits(binary(n), -2) \\\\ _Rémy Sigrist_, Sep 01 2018"
			],
			"xref": [
				"Inverse of A005351. Cf. A039724, A007088, A065369, A073791, A073792, A073793, A073794, A073795, A073796 and A073835."
			],
			"keyword": "base,easy,sign",
			"offset": "0,3",
			"author": "_Henry Bottomley_, Apr 03 2000",
			"references": 21,
			"revision": 43,
			"time": "2020-12-09T01:52:06-05:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}