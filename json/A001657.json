{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001657",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1657,
			"id": "M4568 N1945",
			"data": "1,8,104,1092,12376,136136,1514513,16776144,186135312,2063912136,22890661872,253854868176,2815321003313,31222272414424,346260798314872,3840089017377228,42587248616222024,472299787252290712,5237885063192296801,58089034826620525728",
			"name": "Fibonomial coefficients: column 5 of A010048.",
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A001657/b001657.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"A. Brousseau, \u003ca href=\"http://www.fq.math.ca/Scanned/6-1/brousseau3.pdf\"\u003eA sequence of power formulas\u003c/a\u003e, Fib. Quart., 6 (1968), 81-83.",
				"Alfred Brousseau, \u003ca href=\"http://www.fq.math.ca/fibonacci-tables.html\"\u003eFibonacci and Related Number Theoretic Tables\u003c/a\u003e, Fibonacci Association, San Jose, CA, 1972. See p. 17.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (8,40,-60,-40,8,1)."
			],
			"formula": [
				"a(n) = A010048(5+n, 5) (or fibonomial(5+n, 5)).",
				"G.f.: 1/(1-8*x-40*x^2+60*x^3+40*x^4-8*x^5-x^6) = 1/((1-x-x^2)*(1+4*x-x^2)*(1-11*x-x^2)) (see Comments to A055870).",
				"a(n) = 11*a(n-1)+a(n-2)+((-1)^n)*fibonomial(n+3, 3), n \u003e= 2; a(0)=1, a(1)=8; fibonomial(n+3, 3)= A001655(n).",
				"a(n) = Fibonacci(n+3)*(Fibonacci(n+3)^4-1)/30. - _Gary Detlefs_, Apr 24 2012",
				"a(n) = (A049666(n+3) + 2*(-1)^n*A001076(n+3) - 3*A000045(n+3))/150, n \u003e= 0, with A049666(n) = F(5*n)/5, A001076(n) = F(3*n)/2 and A000045(n) = F(n). From the partial fraction decomposition of the o.g.f. and recurrences. - _Wolfdieter Lang_, Aug 23 2012",
				"a(n) = a(-6-n) * (-1)^n for all n in Z. - _Michael Somos_, Sep 19 2014",
				"0 = a(n)*(-a(n+1) - 3*a(n+2)) + a(n+1)*(-8*a(n+1) + a(n+2)) for all n in Z. - _Michael Somos_, Sep 19 2014"
			],
			"example": [
				"G.f. = 1 + 8*x + 104*x^2 + 1092*x^3 + 12376*x^4 + 136136*x^5 + 1514513*x^6 + ..."
			],
			"maple": [
				"with(combinat) : a:=n-\u003e 1/30*fibonacci(n)*fibonacci(n+1)*fibonacci(n+2)*fibonacci(n+3)*fibonacci(n+4): seq(a(n), n=1..19); # _Zerinvary Lajos_, Oct 07 2007",
				"A001657:=-1/(z**2+11*z-1)/(z**2-4*z-1)/(z**2+z-1); # [_Simon Plouffe_ in his 1992 dissertation.]"
			],
			"mathematica": [
				"f[n_] := Times @@ Fibonacci[Range[n + 1, n + 5]]/30; t = Table[f[n], {n, 0, 20}] (* _Vladimir Joseph Stephan Orlovsky_, Feb 12 2010 *)",
				"LinearRecurrence[{8,40,-60,-40,8,1},{1,8,104,1092,12376,136136},20] (* _Harvey P. Dale_, Nov 30 2019 *)"
			],
			"program": [
				"(PARI) a(n)=(n-\u003e(n^5-n)/30)(fibonacci(n+3)) \\\\ _Charles R Greathouse IV_, Apr 24 2012",
				"(PARI) b(n, k)=prod(j=1, k, fibonacci(n+j)/fibonacci(j));",
				"vector(20, n, b(n-1, 5))  \\\\ _Joerg Arndt_, May 08 2016"
			],
			"xref": [
				"Cf. A010048, A001654-A001658, A065563."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_.",
			"ext": [
				"Corrected and extended by _Wolfdieter Lang_, Jun 27 2000"
			],
			"references": 7,
			"revision": 63,
			"time": "2021-03-12T22:32:35-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}