{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A110143",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 110143,
			"data": "1,1,4,11,43,161,901,5579,43206,378360,3742738,40853520,488029621,6323154547,88308425755,1322120265238,21122364398761,358647945023885,6449299885654827,122436442904193940,2447046870232798369,51358050784584629338,1129314001779283063606",
			"name": "Row sums of triangle A110141.",
			"comment": [
				"Row n of triangle A110141 lists the denominators of unit fraction coefficients of the products of {c_k}, in ascending order by indices of {c_k}, in the coefficient of x^n in exp(Sum_{k\u003e=1} c_k/k*x^k). There are A000041(n) terms in row n of triangle A110141.",
				"Also, number of orbits of Sym(n)^2 where Sym_n acts by conjugation.  Compare the MathOverflow discussion, also Bogaerts-Dukes 2014, and A241584, A241585. - _Peter J. Dukes_, May 12 2014",
				"Number of isomorphism classes of n-fold coverings of a connected graph with circuit rank 2 [Kwak and Lee]. - _Álvar Ibeas_, Mar 25 2015"
			],
			"reference": [
				"P. A. MacMahon, The expansion of determinants and permanents in terms of symmetric functions, in Proc. ICM Toronto (ed. J. C. Fields), Toronto University Press, 1924, vol 1, 319-330.",
				"J. H. Kwak and J. Lee, Enumeration of graph coverings, surface branched coverings and related group theory, in Combinatorial and Computational Mathematics (Pohang, 2000), ed. S. Hong et al., World Scientific, Singapore 2001, pp. 97-161. Appears to contain this sequence in Table 2. [Added by _N. J. A. Sloane_, Nov 12 2009]"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A110143/b110143.txt\"\u003eTable of n, a(n) for n = 0..450\u003c/a\u003e",
				"Mathieu Bogaerts and Peter Dukes, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2014.03.002\"\u003eSemidefinite programming for permutation codes\u003c/a\u003e, Discrete Math. 326 (2014), 34--43. MR3188985.",
				"J. B. Geloun, S. Ramgoolam, \u003ca href=\"http://arxiv.org/abs/1307.6490\"\u003eCounting Tensor Model Observables and Branched Covers of the 2-Sphere\u003c/a\u003e, arXiv:1307.6490 [hep-th], 2013.",
				"Joseph Ben Geloun, Sanjaye Ramgoolam, \u003ca href=\"https://arxiv.org/abs/2106.01470\"\u003eAll-orders asymptotics of tensor model observables from symmetries of restricted partitions\u003c/a\u003e, arXiv:2106.01470 [hep-th], Jun 02 2021.",
				"J. H. Kwak and J. Lee, \u003ca href=\"http://dx.doi.org/10.4153/CJM-1990-039-3\"\u003eIsomorphism classes of graph bundles\u003c/a\u003e. Can. J. Math., 42(4), 1990, pp. 747-761.",
				"MathOverflow, \u003ca href=\"http://mathoverflow.net/questions/41337/a-general-formula-for-the-number-of-conjugacy-classes-of-mathbbs-n-times-m/\"\u003eA general formula for the number of conjugacy classes of S_n×S_n acted on by S_n\u003c/a\u003e [From Peter Dukes, May 12 2014]",
				"Igor Pak, Greta Panova, Damir Yeliussizov, \u003ca href=\"https://arxiv.org/abs/1804.04693\"\u003eOn the largest Kronecker and Littlewood-Richardson coefficients\u003c/a\u003e, arXiv:1804.04693 [math.CO], 2018."
			],
			"formula": [
				"G.f.: B(x)*B(2*x^2)*B(3*x^3)*..., where B(x) is g.f. of A000142. - _Vladeta Jovovic_, Feb 18 2007",
				"a(n) ~ n! * (1 + 2/n^2 + 5/n^3 + 23/n^4 + 106/n^5 + 537/n^6 + 3143/n^7 + 20485/n^8 + 143747/n^9 + 1078660/n^10), for coefficients see A279819. - _Vaclav Kotesovec_, Mar 16 2015"
			],
			"maple": [
				"# Using a function from _Alois P. Heinz_ in A279038:",
				"b:= proc(n, i) option remember; `if`(n=0, [1],",
				"      `if`(i\u003c1, [], [seq(map(x-\u003e x*i^j*j!,",
				"      b(n-i*j, i-1))[], j=0..n/i)]))",
				"    end:",
				"seq(add(i, i=b(n$2)), n=0..22); # _Peter Luschny_, Dec 19 2016"
			],
			"mathematica": [
				"Table[Total[Apply[Times, Tally[#]/.{a_Integer,b_}-\u003ea^b b!]\u0026 /@ IntegerPartitions[n]],{n,0,21}] (* _Wouter Meeussen_, Oct 17 2014 *)",
				"b[n_, i_] := b[n, i] = If[n == 0, {1}, If[i \u003c 1, {}, Flatten[ Table[ Map[ #*i^j*j!\u0026, b[n-i*j, i-1]], {j, 0, n/i}]]]]; Table[Sum[i, {i, b[n, n]}], {n, 0, 22}] (* _Jean-François Alcover_, Jul 10 2017, after _Alois P. Heinz_ *)",
				"nmax = 25; CoefficientList[Series[Product[Sum[k!*j^k*x^(j*k), {k, 0, nmax/j}], {j, 1, nmax}], {x, 0, nmax}], x] (* _Vaclav Kotesovec_, Aug 08 2019 *)",
				"m = 30; CoefficientList[Series[Product[-Gamma[0, -1/(x^j*j)] * Exp[-1/(x^j*j)], {j, 1, m}] / (x^(m*(m + 1)/2)*m!), {x, 0, m}], x] (* _Vaclav Kotesovec_, Dec 07 2020 *)"
			],
			"program": [
				"(Sage)",
				"def A110143(n):",
				"    return sum(p.aut() for p in Partitions(n))",
				"[A110143(n) for n in range(9)]",
				"# _Álvar Ibeas_, Mar 26 2015"
			],
			"xref": [
				"Cf. A110141, A000041, A057005, A096161, A241584, A241585, A279819.",
				"Third column of A160449."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Paul D. Hanna_, Jul 14 2005",
			"references": 19,
			"revision": 77,
			"time": "2021-06-04T06:44:22-04:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}