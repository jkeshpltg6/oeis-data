{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A238128",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 238128,
			"data": "1,1,0,2,0,0,3,1,0,0,5,4,1,0,0,7,13,5,1,0,0,11,37,21,6,1,0,0,15,100,78,31,7,1,0,0,22,265,292,133,43,8,1,0,0,30,694,1028,586,215,57,9,1,0,0,42,1828,3691,2453,1073,325,73,10,1,0,0,56,4815,13004,10357,5058,1836,467,91,11,1,0,0",
			"name": "Triangle read by rows: T(n,k) gives the number of ballot sequences of length n having largest descent k, n\u003e=0, 0\u003c=k\u003c=n.",
			"comment": [
				"Also number of standard Young tableaux with a pair of cells (v,v+1) such that v lies k rows above v+1, and no pair (u,u+1) with a larger such separation exists."
			],
			"link": [
				"Joerg Arndt and Alois P. Heinz, \u003ca href=\"/A238128/b238128.txt\"\u003eTable of n, a(n) for n = 0..35, flattened\u003c/a\u003e"
			],
			"example": [
				"Triangle starts:",
				"00:  1;",
				"01:  1,     0;",
				"02:  2,     0,     0;",
				"03:  3,     1,     0,     0;",
				"04:  5,     4,     1,     0,     0;",
				"05:  7,    13,     5,     1,     0,    0;",
				"06: 11,    37,    21,     6,     1,    0,    0;",
				"07: 15,   100,    78,    31,     7,    1,    0,   0;",
				"08: 22,   265,   292,   133,    43,    8,    1,   0,   0;",
				"09: 30,   694,  1028,   586,   215,   57,    9,   1,   0,  0;",
				"10: 42,  1828,  3691,  2453,  1073,  325,   73,  10,   1,  0, 0;",
				"11: 56,  4815, 13004, 10357,  5058, 1836,  467,  91,  11,  1, 0, 0;",
				"12: 77, 12867, 46452, 43462, 23953, 9631, 2941, 645, 111, 12, 1, 0, 0;",
				"..."
			],
			"maple": [
				"b:= proc(n, v, l) option remember; `if`(n\u003c1, 1, expand(add(",
				"      `if`(i=1 or l[i-1]\u003el[i], (p-\u003e`if`(i\u003cv, add(coeff(p, x, h)*",
				"      `if`(h\u003cv-i, x^(v-i), x^h), h=0..max(v-i, degree(p))), p))",
				"       (b(n-1, i, subsop(i=l[i]+1, l))), 0), i=1..nops(l))+",
				"       b(n-1, nops(l)+1, [l[], 1])))",
				"    end:",
				"T:= n-\u003e (p-\u003eseq(coeff(p, x, i), i=0..n))(b(n-1, 1, [1])):",
				"seq(T(n), n=0..12);"
			],
			"mathematica": [
				"b[n_, v_, l_List] := b[n, v, l] = If[n\u003c1, 1, Expand[Sum[If[i == 1 || l[[i-1]] \u003e l[[i]], Function[{p}, If[i\u003cv, Sum[Coefficient[p, x, h]* If[h \u003c v-i, x^(v-i), x^h], {h, 0, Max[v-i, Exponent[p, x]]}], p]][b[n-1, i, ReplacePart[l, i -\u003e l[[i]]+1]]], 0], {i, 1, Length[l]}] + b[n-1, Length[l]+1, Append[l, 1]]]]; T[n_] := Function[{p}, Table[Coefficient[p, x, i], {i, 0, n}]][b[n-1, 1, {1}]]; Table[T[n], {n, 0, 12}] // Flatten (* _Jean-François Alcover_, Jan 07 2015, translated from Maple *)"
			],
			"xref": [
				"Columns k=0-10 give: A000041, A244197, A244198, A244199, A244200, A244201, A244202, A244203, A244204, A244205, A244206.",
				"Row sums are A000085.",
				"Cf. A238129."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_Joerg Arndt_ and _Alois P. Heinz_, Feb 21 2014",
			"references": 12,
			"revision": 21,
			"time": "2015-01-07T05:41:00-05:00",
			"created": "2014-02-23T05:06:05-05:00"
		}
	]
}