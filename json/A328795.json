{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A328795",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 328795,
			"data": "1,2,1,0,0,2,2,0,2,2,1,0,2,6,2,0,3,6,4,0,4,8,4,0,7,14,7,0,6,16,10,0,11,20,11,0,14,32,16,0,17,38,21,0,22,46,24,0,32,66,34,0,34,78,44,0,49,96,50,0,60,130,66,0,72,154,84,0,90,186,98,0,117,244",
			"name": "Expansion of (chi(x) * chi(-x^3))^2 in powers of x where chi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Convolution square of A328802.",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (36 t)) = 2 g(t) where q = exp(2 Pi i t) and g() is g.f. for A328789."
			],
			"link": [
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Euler transform of period 12 sequence [2, -2, 0, 0, 2, -2, 2, 0, 0, -2, 2, 0, ...].",
				"G.f.: Product_{k\u003e=1} (1 + x^(2*k-1))^2 * (1 - x^(6*k-3))^2.",
				"a(n) = (-1)^n * A328797(n). a(2*n) = A112206(n).",
				"a(4*n) = A328789(n). a(4*n + 1) = 2 * A328798(n). a(4*n + 2) = A328790(n). a(4*n + 3) = 0."
			],
			"example": [
				"G.f. = 1 + 2*x + x^2 + 2*x^5 + 2*x^6 + 2*x^8 + 2*x^9 + x^10 + ...",
				"G.f. = q^-1 + 2*q^2 + q^5 + 2*q^14 + 2*q^17 + 2*q^23 + 2*q^26 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ (QPochhammer[ -x, x^2] QPochhammer[ x^3, x^6])^2, {x, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n \u003c 0, 0, A = x * O(x^n); polcoeff( (eta(x^2 + A)^2 * eta(x^3 + A))^2 / (eta(x+ A) * eta(x^4 + A) * eta(x^6 + A))^2, n))};"
			],
			"xref": [
				"Cf. A112206, A328789, A328790, A328797, A328798, A328802."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Michael Somos_, Oct 28 2019",
			"references": 3,
			"revision": 7,
			"time": "2020-03-09T19:03:30-04:00",
			"created": "2019-10-28T11:48:44-04:00"
		}
	]
}