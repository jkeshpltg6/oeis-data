{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A347705",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 347705,
			"data": "0,1,1,1,1,2,1,3,1,2,1,4,1,2,2,3,1,4,1,4,2,2,1,7,1,2,3,4,1,5,1,7,2,2,2,7,1,2,2,7,1,5,1,4,4,2,1,12,1,4,2,4,1,7,2,7,2,2,1,11,1,2,4,8,2,5,1,4,2,5,1,16,1,2,4,4,2,5,1,12,3,2,1,11,2",
			"name": "Number of factorizations of n with reverse-alternating product \u003e 1.",
			"comment": [
				"A factorization of n is a weakly increasing sequence of positive integers \u003e 1 with product n.",
				"We define the alternating product of a sequence (y_1,...,y_k) to be Product_i y_i^((-1)^(i-1)). The reverse-alternating product is the alternating product of the reversed sequence."
			],
			"formula": [
				"a(n) = A001055(n) - A347438(n)."
			],
			"example": [
				"The a(n) factorizations for n = 2, 6, 8, 12, 24, 30, 48, 60:",
				"  2   6     8       12      24        30      48          60",
				"      2*3   2*4     2*6     3*8       5*6     6*8         2*30",
				"            2*2*2   3*4     4*6       2*15    2*24        3*20",
				"                    2*2*3   2*12      3*10    3*16        4*15",
				"                            2*2*6     2*3*5   4*12        5*12",
				"                            2*3*4             2*3*8       6*10",
				"                            2*2*2*3           2*4*6       2*5*6",
				"                                              3*4*4       3*4*5",
				"                                              2*2*12      2*2*15",
				"                                              2*2*2*6     2*3*10",
				"                                              2*2*3*4     2*2*3*5",
				"                                              2*2*2*2*3"
			],
			"mathematica": [
				"facs[n_]:=If[n\u003c=1,{{}},Join@@Table[Map[Prepend[#,d]\u0026,Select[facs[n/d],Min@@#\u003e=d\u0026]],{d,Rest[Divisors[n]]}]];",
				"revaltprod[q_]:=Product[q[[-i]]^(-1)^(i-1),{i,Length[q]}];",
				"Table[Length[Select[facs[n],revaltprod[#]\u003e1\u0026]],{n,100}]"
			],
			"xref": [
				"Positions of 1's are A000430.",
				"The weak version (\u003e= instead of \u003e) is A001055, non-reverse A347456.",
				"The non-reverse version is A339890, strict A347447.",
				"The version for reverse-alternating product 1 is A347438.",
				"Allowing any integer reciprocal alternating product gives A347439.",
				"The even-length case is A347440, also the opposite reverse version.",
				"Allowing any integer rev-alt product gives A347442, non-reverse A347437.",
				"The version for partitions is A347449, non-reverse A347448.",
				"A001055 counts factorizations (strict A045778, ordered A074206).",
				"A038548 counts possible rev-alt products of factorizations, integer A046951.",
				"A103919 counts partitions by sum and alternating sum, reverse A344612.",
				"A292886 counts knapsack factorizations, by sum A293627.",
				"A347707 counts possible integer reverse-alternating products of partitions.",
				"Cf. A028983, A119620, A339846, A347441, A347443, A347450, A347463, A347466."
			],
			"keyword": "nonn",
			"offset": "1,6",
			"author": "_Gus Wiseman_, Oct 12 2021",
			"references": 10,
			"revision": 6,
			"time": "2021-10-27T22:23:57-04:00",
			"created": "2021-10-27T22:23:57-04:00"
		}
	]
}