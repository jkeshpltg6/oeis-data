{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A126988",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 126988,
			"data": "1,2,1,3,0,1,4,2,0,1,5,0,0,0,1,6,3,2,0,0,1,7,0,0,0,0,0,1,8,4,0,2,0,0,0,1,9,0,3,0,0,0,0,0,1,10,5,0,0,2,0,0,0,0,1,11,0,0,0,0,0,0,0,0,0,1,12,6,4,3,0,2,0,0,0,0,0,1",
			"name": "Triangle read by rows: T(n,k) = n/k if k is a divisor of n; T(n,k) = 0 if k is not a divisor of n (1 \u003c= k \u003c= n).",
			"comment": [
				"Row sums = A000203, sigma(n).",
				"k-th column (k=0,1,2,...) is (1,2,3,...) interspersed with n consecutive zeros starting after the \"1\".",
				"The nonzero entries of row n are the divisors of n in decreasing order. - _Emeric Deutsch_, Jan 17 2007",
				"Alternating row sums give A000593. - _Omar E. Pol_, Feb 11 2018",
				"T(n,k) is the number of k's in the partitions of n into equal parts. - _Omar E. Pol_, Nov 25 2019"
			],
			"reference": [
				"David Wells, \"Prime Numbers, the Most Mysterious Figures in Math\", John Wiley \u0026 Sons, Inc, 2005, Appendix B."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A126988/b126988.txt\"\u003eRows n = 1..125 of triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"From _Emeric Deutsch_, Jan 17 2007: (Start)",
				"G.f. of column k: z^k/(1-z^k)^2 (k=1,2,...).",
				"G.f.: G(t,z) = Sum_{k\u003e=1} t^k*z^k/(1-z^k)^2. (End)",
				"G.f.: F(x,z) = log(1/(Product_{n \u003e= 1} (1 - x*z^n))) = Sum_{n \u003e= 1} (x*z)^n/(n*(1 - z^n)) = x*z + (2*x + x^2)*z^2/2 + (3*x + x^3)*z^3/3 + .... Note, exp(F(x,z)) is a g.f. for A008284 (with an additional term T(0,0) equal to 1). - _Peter Bala_, Jan 13 2015",
				"T(n,k) = A010766(n,k)*A051731(n,k), k=1..n. - _Reinhard Zumkeller_, Jan 20 2014"
			],
			"example": [
				"First few rows of the triangle are:",
				"   1;",
				"   2, 1;",
				"   3, 0, 1;",
				"   4, 2, 0, 1;",
				"   5, 0, 0, 0, 1;",
				"   6, 3, 2, 0, 0, 1;",
				"   7, 0, 0, 0, 0, 0, 1;",
				"   8, 4, 0, 2, 0, 0, 0, 1;",
				"   9, 0, 3, 0, 0, 0, 0, 0, 1;",
				"  10, 5, 0, 0, 2, 0, 0, 0, 0, 1;",
				"  ...",
				"sigma(12) = A000203(n) = 28.",
				"sigma(12) = 28, from 12th row = (12 + 6 + 4 + 3 + 2 + 1), deleting the zeros, from left to right.",
				"For n = 6 the partitions of 6 into equal parts are [6], [3,3], [2,2,2], [1,1,1,1,1,1], so the number of k's are [6, 3, 2, 0, 0, 1] respectively, equaling the 6th row of triangle. - _Omar E. Pol_, Nov 25 2019"
			],
			"maple": [
				"A126988:=proc(n,k) if type(n/k, integer)=true then n/k else 0 fi end: for n from 1 to 12 do seq(A126988(n,k),k=1..n) od; # yields sequence in triangular form - _Emeric Deutsch_, Jan 17 2007"
			],
			"mathematica": [
				"Table[If[Mod[n, m]==0, n/m, 0], {n,1,12}, {m,1,n}]//Flatten (* _Roger L. Bagula_, Sep 06 2008, simplified by _Franklin T. Adams-Watters_, Aug 24 2011 *)"
			],
			"program": [
				"(Haskell)",
				"a126988 n k = a126988_tabl !! (n-1) !! (k-1)",
				"a126988_row n = a126988_tabl !! (n-1)",
				"a126988_tabl = zipWith (zipWith (*)) a010766_tabl a051731_tabl",
				"-- _Reinhard Zumkeller_, Jan 20 2014",
				"(PARI) {T(n,k) = if(n%k==0, n/k, 0)}; \\\\ _G. C. Greubel_, May 29 2019",
				"(MAGMA) [[(n mod k) eq 0 select n/k else 0: k in [1..n]]: n in [1..12]]; // _G. C. Greubel_, May 29 2019",
				"(Sage)",
				"def T(n, k):",
				"    if (n%k==0): return n/k",
				"    else: return 0",
				"[[T(n, k) for k in (1..n)] for n in (1..12)] # _G. C. Greubel_, May 29 2019"
			],
			"xref": [
				"Cf. A000203, A008284, A127446, A244051, A328361."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "1,2",
			"author": "_Gary W. Adamson_, Dec 31 2006",
			"ext": [
				"Edited by _N. J. A. Sloane_, Jan 24 2007",
				"Comment from _Emeric Deutsch_ made name by _Franklin T. Adams-Watters_, Aug 24 2011"
			],
			"references": 60,
			"revision": 59,
			"time": "2019-12-03T07:12:14-05:00",
			"created": "2007-01-12T03:00:00-05:00"
		}
	]
}