{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A239145",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 239145,
			"data": "1,1,0,1,1,0,1,2,1,0,1,5,3,1,0,1,13,8,3,1,0,1,39,22,10,3,1,0,1,120,65,32,10,3,1,0,1,401,208,103,37,10,3,1,0,1,1385,703,344,136,37,10,3,1,0,1,5069,2517,1206,501,151,37,10,3,1,0,1,19170,9390,4421,1890,622,151,37,10,3,1,0",
			"name": "Number T(n,k) of self-inverse permutations p on [n] where the minimal transposition distance equals k (k=0 for the identity permutation); triangle T(n,k), n\u003e=0, 0\u003c=k\u003c=n, read by rows.",
			"comment": [
				"Columns k=0 and k=1 respectively give A000012 and A000085(n)-A170941(n).",
				"Row sums give A000085.",
				"Diagonal T(2n,n) gives A005493(n-1) for n\u003e0.",
				"Reversed rows converge to A005493."
			],
			"link": [
				"Joerg Arndt and Alois P. Heinz, \u003ca href=\"/A239145/b239145.txt\"\u003eRows n = 0..30, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = A239144(n,k-1) - A239144(n,k) for k\u003e0, T(n,0) = 1."
			],
			"example": [
				"T(4,0) = 1: 1234.",
				"T(4,1) = 5: 1243, 1324, 2134, 2143, 4321.",
				"T(4,2) = 3: 1432, 3214, 3412.",
				"T(4,3) = 1: 4231.",
				"Triangle T(n,k) begins:",
				"00:   1;",
				"01:   1,    0;",
				"02:   1,    1,    0;",
				"03:   1,    2,    1,    0;",
				"04:   1,    5,    3,    1,   0;",
				"05:   1,   13,    8,    3,   1,   0;",
				"06:   1,   39,   22,   10,   3,   1,  0;",
				"07:   1,  120,   65,   32,  10,   3,  1,  0;",
				"08:   1,  401,  208,  103,  37,  10,  3,  1, 0;",
				"09:   1, 1385,  703,  344, 136,  37, 10,  3, 1, 0;",
				"10:   1, 5069, 2517, 1206, 501, 151, 37, 10, 3, 1, 0;"
			],
			"maple": [
				"b:= proc(n, k, s) option remember; `if`(n=0, 1, `if`(n in s,",
				"      b(n-1, k, s minus {n}), b(n-1, k, s) +add(`if`(i in s, 0,",
				"      b(n-1, k, s union {i})), i=1..n-k-1)))",
				"    end:",
				"T:= (n, k)-\u003e `if`(k=0, 1, b(n, k-1, {})-b(n, k, {})):",
				"seq(seq(T(n, k), k=0..n), n=0..14);"
			],
			"mathematica": [
				"b[n_, k_, s_List] := b[n, k, s] = If[n == 0, 1, If[MemberQ[s, n], b[n-1, k, s ~Complement~ {n}], b[n-1, k, s] + Sum[If[MemberQ[s, i], 0, b[n-1, k, s ~Union~ {i}]], {i, 1, n - k - 1}]]] ; T[n_, k_] := If[k == 0, 1, b[n, k-1, {}] - b[n, k, {}]]; Table[Table[T[n, k], {k, 0, n}], {n, 0, 14}] // Flatten (* _Jean-François Alcover_, Jan 22 2015, after Maple *)"
			],
			"keyword": "nonn,tabl",
			"offset": "0,8",
			"author": "_Joerg Arndt_ and _Alois P. Heinz_, Mar 11 2014",
			"references": 4,
			"revision": 26,
			"time": "2015-01-22T05:22:23-05:00",
			"created": "2014-03-12T16:24:54-04:00"
		}
	]
}