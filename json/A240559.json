{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A240559",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 240559,
			"data": "0,0,1,-3,-5,45,61,-1113,-1385,42585,50521,-2348973,-2702765,176992725,199360981,-17487754833,-19391512145,2195014332465,2404879675441,-341282303124693,-370371188237525,64397376340013805,69348874393137901,-14499110277050234553",
			"name": "a(n) = -2^n*(E(n, 1/2) + E(n, 1) + (n mod 2)*2*(E(n+1, 1/2) + E(n+1, 1))), where E(n, x) are the Euler polynomials.",
			"link": [
				"L. Seidel, \u003ca href=\"http://publikationen.badw.de/de/003384831/pdf/CC%20BY\"\u003eÜber eine einfache Entstehungsweise der Bernoullischen Zahlen und einiger verwandten Reihen\u003c/a\u003e, Sitzungsberichte der mathematisch-physikalischen Classe der königlich bayerischen Akademie der Wissenschaften zu München, Vol. 7 (1877), 157-187."
			],
			"formula": [
				"a(2*n) = (-1)^(n+1)*A147315(2*n,1) = (-1)^(n+1)*A186370(2*n,2*n) =(-1)^(n+1)*A000364(n) for n\u003e0.",
				"a(2*n+1) = (-1)^n*A147315(2*n+1,2) = (-1)^n*A186370(2*n,2*n-1) = A241242(n).",
				"a(n) = Sum_{k=0..n} (-1)^(n-k)*2^k*binomial(n,k)*(E(k,1/2) + 2*E(k+1,0)) where E(n,x) are the Euler polynomials.",
				"a(n) = Sum_{k=0..n} (-1)^(n-k)*binomial(n,k)*(skp(k,0) + skp(k+1,-1)), where skp(n, x) are the Swiss-Knife polynomials A153641.",
				"a(n) = A239322(n) + A239005(n+1) - A239005(n). - _Paul Curtz_, Apr 18 2014",
				"E.g.f.: 1 - sech(x) - tanh(x) + sinh(x)*sech(x)^2 = ((exp(-x)-1)*sech(x))^2 / 2. - _Sergei N. Gladkovskii_, Nov 20 2014",
				"E.g.f.: (1 - sech(x)) * (1 - tanh(x)). - _Michael Somos_, Nov 22 2014"
			],
			"example": [
				"G.f. = x^2 - 3*x^3 - 5*x^4 + 45*x^5 + 61*x^6 - 1113*x^7 - 1385*x^8 + ..."
			],
			"maple": [
				"A240559 := proc(n) euler(n,1/2) + euler(n,1); if n mod 2 = 1 then % + 2*(euler(n+1,1/2)+euler(n+1,1)) fi; -2^n*% end: seq(A240559(n),n=0..19);"
			],
			"mathematica": [
				"skp[n_, x_] := Sum[Binomial[n, k]*EulerE[k]*x^(n-k), {k, 0, n}]; skp[n_, x0_?NumericQ] := skp[n, x] /. x -\u003e x0; a[n_] := Sum[(-1)^(n-k)*Binomial[n, k]*(skp[k, 0] + skp[k+1, -1]), {k, 0, n}]; Table[a[n], {n, 0, 23}] (* _Jean-François Alcover_, Dec 09 2014, after _Peter Luschny_ *)"
			],
			"program": [
				"(Sage)",
				"# Efficient computation with L. Seidel's boustrophedon transformation.",
				"def A240559_list(n) :",
				"    A = [0]*(n+1); A[0] = 1; R = [0]",
				"    k = 0; e = 1; x = -1; s = -1",
				"    for i in (0..n):",
				"        Am = 0; A[k + e] = 0; e = -e;",
				"        for j in (0..i): Am += A[k]; A[k] = Am; k += e",
				"        if e == 1: x += 1; s = -s",
				"        v = -A[-x] if e == 1 else A[-x] - A[x]",
				"        if i \u003e 1: R.append(s*v)",
				"    return R",
				"A240559_list(24)"
			],
			"xref": [
				"Cf. A000364, A147315, A186370, A241242.",
				"Cf. A239005, A239322."
			],
			"keyword": "sign",
			"offset": "0,4",
			"author": "_Peter Luschny_, Apr 17 2014",
			"references": 4,
			"revision": 54,
			"time": "2021-02-22T02:52:55-05:00",
			"created": "2014-04-28T04:02:31-04:00"
		}
	]
}