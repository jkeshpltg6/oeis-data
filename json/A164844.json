{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A164844",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 164844,
			"data": "1,1,10,1,11,100,1,12,111,1000,1,13,123,1111,10000,1,14,136,1234,11111,100000,1,15,150,1370,12345,111111,1000000,1,16,165,1520,13715,123456,1111111,10000000,1,17,181,1685,15235,137171,1234567,11111111,100000000,1,18,198,1866,16920,152406,1371738,12345678,111111111,1000000000,1,19,216,2064,18786,169326,1524144,13717416,123456789,1111111111,10000000000",
			"name": "Generalized Pascal Triangle - satisfying the same recurrence as Pascal's triangle, but with a(n,0)=1 and a(n,n)=10^n (instead of both being 1).",
			"comment": [
				"Like with Pascal's triangle, the columns grown polynomially. For example, a(n,1)=10+n, a(n,2)=(1/2)*(180+19n+n^2), a(n,3)=(1/6)*(5400 + 569n + 30n^2 + n^3). Likewise, diagonals grow exponentially: a(n,n)=10^n, a(n,n-1) = (10^n-1) / 9. [_Kellen Myers_, Jan 24 2010]"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A164844/b164844.txt\"\u003eTable of n, a(n) for n = 0..10152\u003c/a\u003e (rows 0 to 141, flattened)"
			],
			"formula": [
				"From  _Kellen Myers_, Jan 24 2010: (Start)",
				"a(n,k) = Sum_{i = 0..k} 10^i * binomial(n-i-1, n-k-1)), for 0\u003c=k\u003c=n.",
				"a(n,0) = 1, a(n,n) = 10^n, a(n,k) = a(n-1,k-1)+a(n-1,k). (End)",
				"T(n,k) = T(n-1,k)+11*T(n-1,k-1)-10*T(n-2,k-1)-10*T(n-2,k-2), T(0,0)=1, T(1,0)=1, T(1,1)=10, T(n,k)=0 if k\u003c0 or if k\u003en. - _Philippe Deléham_, Dec 27 2013",
				"G.f. of triangle: g(x,y) = (1-xy)/((1-10xy)(1-x-xy)). - _Robert Israel_, Jul 01 2016"
			],
			"example": [
				"Triangle begins:",
				"1",
				"1,10",
				"1,11,100",
				"1,12,111,1000",
				"1,13,123,1111,10000",
				"1,14,136,1234,11111,100000"
			],
			"maple": [
				"f:= proc(n,k) option remember;",
				"if k=n then 10^n elif k=0 then 1 else procname(n-1,k-1)+procname(n-1,k) fi",
				"end proc:",
				"seq(seq(f(n,k),k=0..n),n=0..10); # _Robert Israel_, Jul 01 2016"
			],
			"mathematica": [
				"f[r_, k_] := Sum[10^i*Binomial[r - i - 1, r - k - 1], {i, 0, k}]; TableForm[Table[f[n, k], {n, 0, 15}, {k, 0, n}]] (* _Alex Meiburg_, Aug 21 2010 *)",
				"a[n_, k_] := a[n, k] = Piecewise[{{0, k \u003e n || k \u003c 0}, {1, k == 0}, {10^n, k == n}}, a[n - 1, k - 1] + a[n - 1, k]]; TableForm[Table[a[n, k], {n, 0, 10}, {k, 0, n}]] (* _Kellen Myers_, Jan 24 2010 *)"
			],
			"xref": [
				"Cf. A007318, A093645, A011557, A228196."
			],
			"keyword": "nonn,tabl",
			"offset": "0,3",
			"author": "_Mark Dols_, Aug 28 2009",
			"ext": [
				"Definition clarified, more terms, and revision of Meiburg's Mathematica code by _Kellen Myers_, Jan 24 2010"
			],
			"references": 9,
			"revision": 33,
			"time": "2016-12-11T03:07:35-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}