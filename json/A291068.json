{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A291068",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 291068,
			"data": "6,5,4,15,14,13,26,25,24,39,38,37,54,53,52,69,68,67,86,85,84,103,102,101,120,119,118,139,138,137,158,157,156,177,176,175,196,195,194,215,214,213,236,235,234,257,256,255,278,277",
			"name": "Largest number of distinct words arising in Watanabe's tag system {00, 1110} applied to a binary word w, over all starting words w of length n.",
			"comment": [
				"Watanabe's tag system {00, 1110} maps a word w over {0,1} to w', where if w begins with 0, w' is obtained by appending 00 to w and deleting the first three letters, or if w begins with 1, w' is obtained by appending 1110 to w and deleting the first three letters.",
				"The empty word is included in the count.",
				"Comment from _Don Reble_, Aug 25 2017: (Start)",
				"The following comment applies to both the 3-shift tag systems {00,1110} (A291068) and {00,0111} (A291069). Number the bits in a binary word w starting at the left with bit 0. For the trajectory of w under the tag system, only bits numbered 0,3,6,9,... are important, the others (the unimportant bits) having no effect on the outcome.",
				"An important 1 bit produces 0111 or 1110, and exactly one of those new 1 bits is important. The number of important 1's never changes. So the number of initial words of length n that terminate (the analog of A289670) is just 2^(number-of-unimportant-bits) = 2^(floor(2*n/3)) = A291778.",
				"The number that end in a cycle is 2^n - 2^(floor(2*n/3)) = A291779.",
				"Furthermore, the number of important zeros is eventually bounded.",
				"Proof. If a word has A important zeros and B important ones, then after A+B steps, there will be at most 2A+4B bits, and at most (2A+4B+2)/3 important bits. B of them are important ones, so at most (2A+B+2)/3 are important zeros.",
				"If A \u003e= B+3, then (2A+B+2)/3 \u003c= (2A+A-1)/3 \u003c A. If A \u003c B+3, then (2A+B+2)/3 \u003c (3B+8)/3 = B+2. The first kind must shrink; the second kind can't grow past A+B+2. QED",
				"Ultimately, a word with B important ones has at most A+B+2 important bits, so can't diverge. So the word \"finite\" in the definition was unnecessary and has been omitted. (End)"
			],
			"link": [
				"Shigeru Watanabe, \u003ca href=\"/A284116/a284116.pdf\"\u003ePeriodicity of Post's normal process of tag\u003c/a\u003e, in Jerome Fox, ed., Proceedings of Symposium on Mathematical Theory of Automata, New York, April 1962, Polytechnic Press, Polytechnic Institute of Brooklyn, 1963, pp. 83-99. [Annotated scanned copy]",
				"N. J. A. Sloane, \u003ca href=\"/A291067/a291067.txt\"\u003eMaple programs that compute first 7 terms for each of A284116, A291067, A291068, A291069\u003c/a\u003e"
			],
			"example": [
				"Examples of strings that achieve these records: \"1\", \"10\", \"000\", \"1001\", \"10010\", \"100100\", \"1001001\"."
			],
			"maple": [
				"See link."
			],
			"xref": [
				"For the 3-shift tag systems {00,1101}, {00, 1011}, {00, 1110}, {00, 0111} see A284116, A291067, A291068, A291069 respectively (as well as the cross-referenced entries mentioned there).",
				"Cf. A291073."
			],
			"keyword": "nonn,more",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_, Aug 18 2017",
			"ext": [
				"a(8)-(50) from _Lars Blomberg_, Sep 16 2017"
			],
			"references": 7,
			"revision": 28,
			"time": "2021-09-23T01:27:10-04:00",
			"created": "2017-08-18T14:58:00-04:00"
		}
	]
}