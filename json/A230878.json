{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A230878",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 230878,
			"data": "1,0,2,0,0,8,32,16,0,0,0,48,720,2880,4992,4608,2304,512,0,0,0,0,384,13824,143872,739328,2320896,4964352,7659520,8749056,7421952,4587520,1966080,524288,65536,0,0,0,0,0,3840,268800,5504000,57068800,372416000",
			"name": "Irregular triangle read by rows: T(n,k) = number of 2-packed n X n matrices with exactly k nonzero entries (0 \u003c= k \u003c= n^2).",
			"comment": [
				"A k-packed matrix of size n X n is a matrix with entries in the alphabet A_k = {0,1, ..., k} such that each row and each column contains at least one nonzero entry."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A230878/b230878.txt\"\u003eTable of n, a(n) for n = 0..662\u003c/a\u003e (rows 0..12 flattened)",
				"H. Cheballah, S. Giraudo, R. Maurice, \u003ca href=\"http://arxiv.org/abs/1306.6605\"\u003eCombinatorial Hopf algebra structure on packed square matrices\u003c/a\u003e, arXiv preprint arXiv:1306.6605 [math.CO], 2013."
			],
			"formula": [
				"From _Andrew Howroyd_, Sep 20 2017: (Start)",
				"T(n, k) = Sum_{i=0..n} Sum_{j=0..n} (-1)^(i+j) * binomial(n,i) * binomial(n,j) * binomial(i*j,k) * 2^k.",
				"T(n, k) = 0 for n \u003e k.",
				"T(n, n) = A000165(n).",
				"(End)"
			],
			"example": [
				"Triangle begins:",
				"1",
				"0 2",
				"0 0 8 32 16",
				"0 0 0 48 720 2880 4992 4608 2304 512",
				"..."
			],
			"mathematica": [
				"p[k_, n_, l_] := Sum[(-1)^(i+j)*Binomial[n, i]*Binomial[n,j]*Binomial[i*j, l]*k^l, {i, 0, n}, {j, 0, n}];",
				"T[n_, k_] := p[2, n, k];",
				"Table[T[n, k], {n, 0, 5}, {k, 0, n^2}] // Flatten (* _Jean-François Alcover_, Oct 08 2017, translated from PARI *)"
			],
			"program": [
				"(PARI) \\\\ T(n,k) = p(2,n,k) (see Cheballah et al. ref).",
				"p(k,n,l) = {sum(i=0, n, sum(j=0, n, (-1)^(i+j) * binomial(n,i) * binomial(n,j) * binomial(i*j,l) * k^l))}",
				"for (n=0,5, for(k=0,n^2, print1(p(2,n,k), \", \")); print); \\\\ _Andrew Howroyd_, Sep 20 2017"
			],
			"xref": [
				"Row sums are A230879.",
				"Column sums are A230880.",
				"Cf. A000165, A055599."
			],
			"keyword": "nonn,tabf",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_, Nov 09 2013",
			"ext": [
				"Terms a(18) and beyond from _Andrew Howroyd_, Sep 20 2017"
			],
			"references": 3,
			"revision": 16,
			"time": "2017-10-08T08:56:43-04:00",
			"created": "2013-11-09T12:08:57-05:00"
		}
	]
}