{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A139032",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 139032,
			"data": "1,1,0,0,-1,0,0,0,0,0,2,0,0,-2,0,0,-1,0,0,4,0,0,-4,0,0,-1,0,0,8,0,0,-8,0,0,-2,0,0,14,0,0,-14,0,0,-4,0,0,24,0,0,-23,0,0,-6,0,0,40,0,0,-38,0,0,-10,0,0,63,0,0,-60,0,0,-16,0,0,98,0,0,-92,0,0,-24,0,0,150,0,0,-140,0,0,-36,0,0,224,0,0,-208",
			"name": "Expansion of 1 + c(q^6) / c(q^3) where c() is a cubic AGM theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A139032/b139032.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of 1 + eta(q^3) * eta(q^18)^3 / (eta(q^6) * eta(q^9)^3) in powers of q.",
				"Expansion of 1 + q * chi(-q^3) / chi(-q^9)^3 = psi(q) * chi(-q^3) / phi(-q^9) in powers of q where phi(), psi(), chi() are Ramanujan theta functions.",
				"Expansion of eta(q^2)^2 * eta(q^3) * eta(q^18) / (eta(q) * eta(q^6) * eta(q^9)^2) in powers of q.",
				"Euler transform of period 18 sequence [ 1, -1, 0, -1, 1, -1, 1, -1, 2, -1, 1, -1, 1, -1, 0, -1, 1, 0, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (18 t)) = (3/2) * g(t) where q = exp(2 Pi i t) and g() is g.f. for A145977.",
				"G.f.: Product_{k\u003e0} P(2, x^k)^2 * P(18, x^k) / (P(3, x^k) * P(9, x^k)) where P(n, x) is n-th cyclotomic polynomial.",
				"a(3*n) = 0 unless n = 0. a(3*n + 2) = 0. a(3*n + 1) = A092848(n). Convolution inverse of A145977."
			],
			"example": [
				"1 + q - q^4 + 2*q^10 - 2*q^13 - q^16 + 4*q^19 - 4*q^22 - q^25 + 8*q^28 + ..."
			],
			"mathematica": [
				"eta[x_] := QPochhammer[x]; A139032[n_] := SeriesCoefficient[eta[q^2]^2 *eta[q^3]*eta[q^18]/(eta[q]*eta[q^6]*eta[q^9]^2), {q, 0, n}];",
				"Table[A139032[n], {n, 0, 20}] (* _G. C. Greubel_, Aug 09 2017 *)"
			],
			"program": [
				"(PARI) {a(n) = local(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A)^2 * eta(x^3 + A) * eta(x^18 + A) / (eta(x + A) * eta(x^6 + A) * eta(x^9 + A)^2), n))}"
			],
			"xref": [
				"Cf. A092848, A145977."
			],
			"keyword": "sign",
			"offset": "0,11",
			"author": "_Michael Somos_, Apr 07 2008",
			"references": 5,
			"revision": 13,
			"time": "2021-03-12T22:24:45-05:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}