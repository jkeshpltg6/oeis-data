{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A048272",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 48272,
			"data": "1,0,2,-1,2,0,2,-2,3,0,2,-2,2,0,4,-3,2,0,2,-2,4,0,2,-4,3,0,4,-2,2,0,2,-4,4,0,4,-3,2,0,4,-4,2,0,2,-2,6,0,2,-6,3,0,4,-2,2,0,4,-4,4,0,2,-4,2,0,6,-5,4,0,2,-2,4,0,2,-6,2,0,6,-2,4,0,2,-6,5,0,2,-4,4,0,4,-4,2,0,4,-2,4",
			"name": "Number of odd divisors of n minus number of even divisors of n.",
			"comment": [
				"abs(a(n))= 1/2* number of pairs (i,j) satisfying n=i^2-j^2 and -n \u003c= i,j \u003c= n. - _Benoit Cloitre_, Jun 14 2003",
				"As A001227(n) is the number of ways to write n as the difference of 3-gonal numbers, a(n) describes the number of ways to write n as the difference of e-gonal numbers for e in {0,1,4,8}. If pe(n):=1/2*n*((e-2)*n+(4-e)) is the n-th e-gonal number, then 4*a(n) = |{(m,k) of Z X Z; pe(-1)(m+k)-pe(m-1)=n}| for e=1, 2*a(n) = |{(m,k) of Z X Z; pe(-1)(m+k)-pe(m-1)=n}| for e in {0,4} and for a(n) itself is a(n) = |{(m,k) of Z X Z; pe(-1)(m+k)-pe(m-1)=n}| for e=8. (Same for e=-1 see A035218.) - Volker Schmitt (clamsi(AT)gmx.net), Nov 09 2004",
				"a(A008586(n)) \u003c 0; a(A005843(a(n)) \u003c= 0; a(A016825(n)) = 0; a(A042968(n)) \u003e= 0; a(A005408(n)) \u003e 0. - _Reinhard Zumkeller_, Jan 21 2012",
				"An argument by Gareth McCaughan suggests that the average of this sequence is log(2). - _Hans Havermann_, Feb 10 2013"
			],
			"reference": [
				"L. Comtet, Advanced Combinatorics, Reidel, 1974, p. 162, #16, (6), first formula.",
				"S. Ramanujan, Notebooks, Tata Institute of Fundamental Research, Bombay 1957 Vol. 1, see page 97, 7(ii)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A048272/b048272.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"J. W. L. Glaisher, \u003ca href=\"https://books.google.com/books?id=bLs9AQAAMAAJ\u0026amp;pg=RA1-PA1\"\u003eOn the representations of a number as the sum of two, four, six, eight, ten, and twelve squares\u003c/a\u003e, Quart. J. Math. 38 (1907), 1-62 (see p. 4 and p. 8).",
				"P. A. MacMahon, \u003ca href=\"http://dx.doi.org/10.1112/plms/s2-19.1.75\"\u003eDivisors of numbers and their continuations in the theory of partitions\u003c/a\u003e, Proc. London Math. Soc., 19 (1921), 75-113; Coll. Papers II, pp. 303-341.",
				"Mircea Merca, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2015.08.014\"\u003eCombinatorial interpretations of a recent convolution for the number of divisors of a positive integer\u003c/a\u003e, Journal of Number Theory, Volume 160, March 2016, Pages 60-75, function tau_{o-e}(n).",
				"\u003ca href=\"/index/Ge#Glaisher\"\u003eIndex entries for sequences mentioned by Glaisher\u003c/a\u003e"
			],
			"formula": [
				"Coefficients in expansion of Sum_{n \u003e= 1} x^n/(1+x^n) = Sum_{n \u003e= 1} (-1)^(n-1)*x^n/(1-x^n). Expand Sum 1/(1+x^n) in powers of 1/x.",
				"If n = 2^p1*3^p2*5^p3*7^p4*11^p5*..., a(n) = (1-p1)*Product_{i\u003e=2} (1+p_i).",
				"Multiplicative with a(2^e) = 1 - e and a(p^e) = 1 + e if p \u003e 2. - _Vladeta Jovovic_, Jan 27 2002",
				"a(n) = (-1)*Sum_{d|n} (-1)^d. - _Benoit Cloitre_, May 12 2003",
				"Moebius transform is period 2 sequence [1, -1, ...]. - _Michael Somos_, Jul 22 2006",
				"G.f.: Sum_{k\u003e0} -(-1)^k * x^(k^2) * (1 + x^(2*k)) / (1 - x^(2*k)) [Ramanujan]. - _Michael Somos_, Jul 22 2006",
				"Equals A051731 * [1, -1, 1, -1, 1, ...]. - _Gary W. Adamson_, Nov 07 2007",
				"a(n) = A001227(n) - A183063(n). - _Reinhard Zumkeller_, Jan 21 2012",
				"a(n) = Sum_{k=0..n} A081362(k)*A015723(n-k). - _Mircea Merca_, Feb 26 2014",
				"abs(a(n)) = A112329(n) = A094572(n) / 2. - _Ray Chandler_, Aug 23 2014",
				"From _Peter Bala_, Jan 07 2015: (Start)",
				"Logarithmic g.f.: log( Product_{n \u003e= 1} (1 + x^n)^(1/n) ) = Sum_{n \u003e= 1} a(n)*x^n/n.",
				"a(n) = A001227(n) - A183063(n). By considering the logarithmic generating functions of these three sequences we obtain the identity",
				"( Product_{n \u003e= 0} (1 - x^(2*n+1))^(1/(2*n+1)) )^2 = Product_{n \u003e= 1} ( (1 - x^n)/(1 + x^n) )^(1/n). (End)",
				"Dirichlet g.f.: zeta(s)*eta(s) = zeta(s)^2*(1-2^(-s+1)). - _Ralf Stephan_, Mar 27 2015",
				"a(2*n - 1) = A099774(n). - _Michael Somos_, Aug 12 2017",
				"From _Paul D. Hanna_, Aug 10 2019: (Start)",
				"G.f.: Sum_{n\u003e=0} x^n * Sum_{k=0..n} binomial(n,k) * (x^(n+1) - x^k)^(n-k)  =  Sum_{n\u003e=0} a(n)*x^(2*n).",
				"G.f.: Sum_{n\u003e=0} x^n * Sum_{k=0..n} binomial(n,k) * (x^(n+1) + x^k)^(n-k) * (-1)^k  =  Sum_{n\u003e=0} a(n)*x^(2*n). (End)",
				"a(n) = 2*A000005(2n) - 3*A000005(n). - _Ridouane Oudra_, Oct 15 2019"
			],
			"example": [
				"a(20) = -2 because 20 = 2^2*5^1 and (1-2)*(1+1) = -2.",
				"G.f. = x + 2*x^3 - x^4 + 2*x^5 + 2*x^7 - 2*x^8 + 3*x^9 + 2*x^11 - 2*x^12 + ..."
			],
			"maple": [
				"add(x^n/(1+x^n), n=1..60): series(%,x,59);",
				"A048272 := proc(n)",
				"    local a;",
				"    a := 1 ;",
				"    for pfac in ifactors(n)[2] do",
				"        if pfac[1] = 2 then",
				"            a := a*(1-pfac[2]) ;",
				"        else",
				"            a := a*(pfac[2]+1) ;",
				"        end if;",
				"    end do:",
				"    a ;",
				"end proc: # Schmitt, sign corrected _R. J. Mathar_, Jun 18 2016",
				"# alternative Maple program:",
				"a:= n-\u003e -add((-1)^d, d=numtheory[divisors](n)):",
				"seq(a(n), n=1..100);  # _Alois P. Heinz_, Feb 28 2018"
			],
			"mathematica": [
				"Rest[ CoefficientList[ Series[ Sum[x^k/(1 - (-x)^k), {k, 111}], {x, 0, 110}], x]] (* _Robert G. Wilson v_, Sep 20 2005 *)",
				"dif[n_]:=Module[{divs=Divisors[n]},Count[divs,_?OddQ]-Count[ divs, _?EvenQ]]; Array[dif,100] (* _Harvey P. Dale_, Aug 21 2011 *)",
				"a[n]:=Sum[-(-1)^d,{d,Divisors[n]}] (* _Steven Foster Clark_, May 04 2018 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, 0, -sumdiv(n, d, (-1)^d))}; /* _Michael Somos_, Jul 22 2006 */",
				"(PARI)",
				"N=17; default(seriesprecision,N); x=z+O(z^(N+1))",
				"c=sum(j=1,N,j*x^j); \\\\ log case",
				"s=-log(prod(j=1,N,(1+x^j)^(1/j)));",
				"s=serconvol(s,c)",
				"v=Vec(s) \\\\ _Joerg Arndt_, May 03 2008",
				"(PARI) a(n)=my(o=valuation(n,2),f=factor(n\u003e\u003eo)[,2]);(1-o)*prod(i=1,#f,f[i]+1) \\\\ _Charles R Greathouse IV_, Feb 10 2013",
				"(PARI) a(n)=direuler(p=1,n,if(p==2,(1-2*X)/(1-X)^2,1/(1-X)^2))[n] /* _Ralf Stephan_, Mar 27 2015 */",
				"(PARI) {a(n) = my(d = n -\u003e if(frac(n), 0, numdiv(n))); if( n\u003c1, 0, if( n%4, 1, -1) * (d(n) - 2*d(n/2) + 2*d(n/4)))}; /* _Michael Somos_, Aug 11 2017 */",
				"(Haskell)",
				"a048272 n = a001227 n - a183063 n  -- _Reinhard Zumkeller_, Jan 21 2012",
				"(MAGMA) [\u0026+[(-1)^(d+1):d in Divisors(n)] :n in [1..95] ]; // _Marius A. Burtea_, Aug 10 2019"
			],
			"xref": [
				"Cf. A048298. A diagonal of A060184.",
				"First differences of A059851.",
				"Cf. A001227, A035218, A094572, A099774, A112329, A183063, A000005."
			],
			"keyword": "easy,sign,nice,mult",
			"offset": "1,3",
			"author": "_Adam Kertesz_",
			"ext": [
				"New definition from _Vladeta Jovovic_, Jan 27 2002"
			],
			"references": 76,
			"revision": 103,
			"time": "2019-10-16T01:48:25-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}