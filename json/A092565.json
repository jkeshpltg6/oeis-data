{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A092565",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 92565,
			"data": "1,1,1,1,2,2,3,2,1,3,5,8,7,6,3,1,5,10,19,22,22,16,10,4,1,8,20,42,58,69,63,49,30,15,5,1,13,38,89,142,191,206,191,146,95,50,21,6,1,21,71,182,327,491,602,637,573,447,296,167,77,28,7,1,34,130,363,722,1191,1626",
			"name": "Triangle of coefficients T(n,k) (n\u003e=0, 0\u003c=k\u003c=2n), read by rows, where the n-th row polynomial equals the numerator of the n-th convergent of the continued fraction [1+x+x^2;1+x+x^2,1+x+x^2,...] for n\u003e0, with the zeroth row defined as T(0,0)=1.",
			"comment": [
				"T(n,k) is the number of lattice paths from (0,0) to (n,k) using steps (1,0),(2,0),(1,1),(1,2). [Joerg Arndt, Jul 01 2011]",
				"Diagonal forms A092566, row sums form A006190. Column T(n,0) forms Fibonacci numbers A000045, T(n,1) forms A001629."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A092565/b092565.txt\"\u003eRows n = 0..100, flattened\u003c/a\u003e"
			],
			"formula": [
				"n-th row polynomial R(n) = sum_{k=0..n} A037027(n, k)*x^k*(1+x)^k; R(n+1)/R(n) = [1+x+x^2;1+x+x^2, ...(n+1)times..., 1+x+x^2] for n\u003e=0; R(0)=1."
			],
			"example": [
				"Ratio of row polynomials R(3)/R(2) = (3+5*x+8*x^2+7*x^3+6*x^4+3*x^5+x^6)/(2+2*x+3*x^2+2*x^3+x^4) = [1+x+x^2;1+x+x^2,1+x+x^2].",
				"Rows begin:",
				"1;",
				"1, 1, 1;",
				"2, 2, 3, 2, 1;",
				"3, 5, 8, 7, 6, 3, 1;",
				"5, 10, 19, 22, 22, 16, 10, 4, 1;",
				"8, 20, 42, 58, 69, 63, 49, 30, 15, 5, 1;",
				"13, 38, 89, 142, 191, 206, 191, 146, 95, 50, 21, 6, 1;",
				"21, 71, 182, 327, 491, 602, 637, 573, 447, 296, 167, 77, 28, 7, 1;",
				"34, 130, 363, 722, 1191, 1626, 1921, 1958, 1752, 1366, 931, 546, 273, 112, 36, 8, 1;",
				"..."
			],
			"maple": [
				"T:= proc(x, y) option remember; `if`(y\u003c0 or y\u003e2*x, 0, `if`(x=0, 1,",
				"      add(T(x-l[1], y-l[2]), l=[[1, 0], [2, 0], [1, 1], [1, 2]])))",
				"    end:",
				"seq(seq(T(n,k), k=0..2*n), n=0..10); # _Alois P. Heinz_, Apr 16 2013"
			],
			"mathematica": [
				"A037027[n_, k_] := Sum[Binomial[k+j, k]*Binomial[j, n-j-k], {j, 0, n-k}]; A037027[n_, 0] = Fibonacci[n + 1]; row[n_] := CoefficientList[ Sum[A037027[n, k] x^k (1+x)^k, {k, 0, n}], x]; Flatten[Table[row[n], {n, 0, 8}]][[1 ;; 70]] (* _Jean-François Alcover_, Jul 18 2011 *)"
			],
			"program": [
				"(PARI) T(n,k)=if(2*n\u003ck || k\u003c0,0,polcoeff(contfracpnqn(vector(n,i,1+x+x^2))[1,1],k,x))",
				"(PARI) /* same as in A092566, but last line (output) replaced by the following */",
				"/* show as triangle (0\u003c=k\u003c=2*n): */",
				"{for (n=1,N, for (k=1,2*n-1, print1(M[n,k],\", \"); ); print(); );}",
				"/* Joerg Arndt, Jul 01 2011 */"
			],
			"xref": [
				"Cf. A092566, A037027."
			],
			"keyword": "nonn,tabf",
			"offset": "0,5",
			"author": "_Paul D. Hanna_, Feb 28 2004",
			"references": 2,
			"revision": 20,
			"time": "2017-06-13T21:52:38-04:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}