{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A074455",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 74455,
			"data": "5,2,5,6,9,4,6,4,0,4,8,6,0,5,7,6,7,8,0,1,3,2,8,3,8,3,8,8,6,9,0,7,6,9,2,3,6,6,1,9,0,1,7,2,3,7,1,8,3,2,1,4,8,5,7,5,0,9,8,7,9,6,7,8,7,7,7,1,0,9,3,4,6,7,3,6,8,2,0,2,7,2,8,1,7,7,2,0,2,3,8,4,8,9,7,9,2,4,6,9,2,6",
			"name": "Consider volume of unit sphere as a function of the dimension d; maximize this as a function of d (considered as a continuous variable); sequence gives decimal expansion of the best d.",
			"comment": [
				"From _David W. Wilson_, Jul 12 2007: (Start)",
				"For an integer d, the volume of a d-dimensional unit ball is v(d) = Pi^(d/2)/(d/2)! and its surface area is area(d) = d*Pi^(d/2)/(d/2)! = d*v(d). If we interpolate n! = gamma(n+1) we can define v(d) and area(d) as continuous functions for (at least) d \u003e= 0.",
				"A074457 purports to minimize area(d). Since area(d+2) = 2*Pi*v(d), area() is minimized at y = x+2; therefore A074457 coincides with the current sequence except at the first term. (End)"
			],
			"reference": [
				"J. H. Conway and N. J. A. Sloane, \"Sphere Packings, Lattices and Groups\", Springer-Verlag, p. 9."
			],
			"link": [
				"Brain Hayes, \u003ca href=\"https://doi.org/10.1515/9781400844678-006\"\u003eAn Adventure in the Nth Dimension\u003c/a\u003e, pp. 30-42 of M. Pitici, editor, The Best Writing on Mathematics 2012, Princeton Univ. Press, 2012. See p. 42. - From _N. J. A. Sloane_, Jan 13 2013; \u003ca href=\"http://www.peterbeerli.com/classes/images/2/20/AmSci2011Hayes2.pdf\"\u003ealternative link\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Ball.html\"\u003eBall\u003c/a\u003e."
			],
			"formula": [
				"d = root of Psi((1/2)*d + 1) = log(Pi).",
				"d is 2 less than the number with decimal digits A074457 (the hypersphere dimension that maximizes hypersurface area). - _Eric W. Weisstein_, Dec 02 2014"
			],
			"example": [
				"5.256946404860576780132838388690769236619017237183214857509879678777109..."
			],
			"mathematica": [
				"x /. FindRoot[ PolyGamma[1 + x/2] == Log[Pi], {x, 5}, WorkingPrecision -\u003e 105] // RealDigits // First (* _Jean-François Alcover_, Mar 28 2013 *)"
			],
			"program": [
				"(PARI)",
				"hyperspheresurface(d)=2*Pi^(d/2)/gamma(d/2)",
				"hyperspherevolume(d)=hyperspheresurface(d)/d",
				"FindMax(fn_x,lo,hi)=",
				"{",
				"local(oldprecision, x, y, z);",
				"oldprecision = default(realprecision);",
				"default(realprecision, oldprecision+10);",
				"while (hi-lo \u003e 10^-oldprecision,",
				"while (1,",
				"z = vector(2, i, lo*(3-i)/3 + hi*i/3);",
				"y = vector(2, i, eval(Str(\"x = z[\" i \"]; \" fn_x)));",
				"if (abs(y[1]-y[2]) \u003e 10^(5-default(realprecision)), break);",
				"default(realprecision, default(realprecision)+10);",
				");",
				"if (y[1] \u003c y[2], lo = z[1], hi = z[2]);",
				");",
				"default(realprecision, oldprecision);",
				"(lo + hi) / 2.",
				"}",
				"default(realprecision, 105);",
				"A074455=FindMax(\"hyperspherevolume(x)\", 1, 9)",
				"A074457=FindMax(\"hyperspheresurface(x)\", 1, 9)",
				"A074454=hyperspherevolume(A074455)",
				"A074456=hyperspheresurface(A074457)",
				"/* David W. Cantrell */"
			],
			"xref": [
				"Cf. A074457.",
				"The volume is given by A074454. Cf. A072345 \u0026 A072346."
			],
			"keyword": "cons,nonn",
			"offset": "1,1",
			"author": "_Robert G. Wilson v_, Aug 22 2002",
			"ext": [
				"Corrected by _Eric W. Weisstein_, Aug 31 2003",
				"Corrected by _Martin Fuller_, Jul 12 2007"
			],
			"references": 7,
			"revision": 35,
			"time": "2021-04-08T07:23:48-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}