{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A089637",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 89637,
			"data": "3,17,41,107,71,2267,1091,461,1319,1151,347,5741,2999,5279,10139,1487,9461,881,659,13007,9041,15359,8627,28751,83717,13397,18539,14627,44771,54011,60257,59669,142157,77711,61559,178931,26261,122867,293261,89069,24419,167861",
			"name": "Smallest member of a pair of consecutive twin prime pairs that have exactly n primes between them.",
			"comment": [
				"If this sequence is well defined then the Twin Prime Conjecture is true. - _David A. Corneth_, Dec 27 2019"
			],
			"link": [
				"David A. Corneth, \u003ca href=\"/A089637/b089637.txt\"\u003eTable of n, a(n) for n = 0..342\u003c/a\u003e (terms n = 1..226 from and terms \u003e 10^12 from Amiram Eldar)",
				"David A. Corneth and Amiram Eldar, \u003ca href=\"/A089637/a089637_1.gp.txt\"\u003eTerms \u003c= 1.5*10^12 (0 indicates the term is \u003e 1.5*10^12\u003c/a\u003e (terms \u003e 10^12 from Amiram Eldar)"
			],
			"example": [
				"a(0) = 3 since there is no prime between the twin primes (3, 5) and (5, 7). - _David A. Corneth_, Dec 27 2019",
				"a(1) = 17 since there is one prime, 23, between the twin primes (17, 19) and (29, 31).",
				"a(2) = 41 since there are 2 primes, 47 and 53, between the twin primes (41, 43) and (59, 61)."
			],
			"mathematica": [
				"countPrimes[pin_] := Module[{prv = pin, c = 0, p}, p = NextPrime[prv]; While[p != prv + 2, c++; prv = p; p = NextPrime[p]]; {c-1, p}]; p = 13; mx = 20; c = 0; seq = Table[0, {mx}]; While[c \u003c mx, cp = countPrimes[p]; i = cp[[1]]; If[i \u003e 0 \u0026\u0026 i \u003c= mx \u0026\u0026 seq[[i]] == 0, c++; seq[[i]] = p - 2]; p = cp[[2]]]; seq (* _Amiram Eldar_, Dec 26 2019 *)"
			],
			"program": [
				"(PARI) pbetweentw(n) = /* p is the number of primes between */ { for(p=0, 100, forstep(x1=1, n, 1, my(c=0, t1 = twin[x1], t2 = twin[x1+1]); for(y=t1+4, t2-1, if(isprime(y), c++) ); if(c==p, print1(t1\", \"); break) ) ) }",
				"savetwins(n) = /* Build a twin prime table of lower bounds */ { twin = vector(n); my(c=1); forprime(x=3, n*10, if(isprime(x+2), twin[c]=x; c++; ) ) }"
			],
			"xref": [
				"Cf. A001097, A001359, A006512."
			],
			"keyword": "nonn",
			"offset": "0,1",
			"author": "_Cino Hilliard_, Jan 01 2004",
			"ext": [
				"Offset corrected and data corrected and expanded by _Amiram Eldar_, Dec 26 2019",
				"a(0) = 3 prepended by _David A. Corneth_, Dec 27 2019"
			],
			"references": 2,
			"revision": 33,
			"time": "2020-01-08T20:44:19-05:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}