{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A317023",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 317023,
			"data": "1,1,0,1,2,0,1,3,2,0,1,4,6,2,0,1,5,10,9,2,0,1,6,15,20,12,2,0,1,7,21,35,34,15,2,0,1,8,28,56,70,52,18,2,0,1,9,36,84,126,125,74,21,2,0,1,10,45,120,210,252,205,100,24,2,0,1,11,55,165,330,462,461,315,130,27,2,0,1,12,66",
			"name": "Square array A(n,k), n \u003e= 0, k \u003e= 0, read by ascending antidiagonals, where the sequence of row n is the expansion of (1-x^(n+1))/((1-x)^(n+1)).",
			"comment": [
				"Conjecture: alternating row sums of the triangle give A106510 for n \u003e= 0."
			],
			"formula": [
				"A(n,k) = binomial(n+k,k) - binomial(k-1,k-1-n) for n \u003e= 0 and k \u003e= 0 with binomial(i,j) = 0 if i \u003c j or j \u003c 0.",
				"G.f.: Sum_{k\u003e=0,n\u003e=0} A(n,k)*x^k*y^n = ((1-x)^2)/((1-x-y)*(1-x-x*y)).",
				"Seen as a triangle T(n,k) = A(n-k,k) = binomial(n,k)-binomial(k-1,2*k-1-n) for 0 \u003c= k \u003c= n with binomial(i,j) = 0 if i \u003c j or j \u003c 0.",
				"Mirror image of the triangle equals A173265 except column 0."
			],
			"example": [
				"The square array A(n,k) begins:",
				"  n\\k |  0  1  2   3    4    5    6     7     8     9     10",
				"  ====+=====================================================",
				"   0  |  1  0  0   0    0    0    0     0     0     0      0",
				"   1  |  1  2  2   2    2    2    2     2     2     2      2",
				"   2  |  1  3  6   9   12   15   18    21    24    27     30",
				"   3  |  1  4 10  20   34   52   74   100   130   164    202",
				"   4  |  1  5 15  35   70  125  205   315   460   645    875",
				"   5  |  1  6 21  56  126  252  461   786  1266  1946   2877",
				"   6  |  1  7 28  84  210  462  924  1715  2996  4977   7924",
				"   7  |  1  8 36 120  330  792 1716  3432  6434 11432  19412",
				"   8  |  1  9 45 165  495 1287 3003  6435 12870 24309  43749",
				"   9  |  1 10 55 220  715 2002 5005 11440 24310 48620  92377",
				"  10  |  1 11 66 286 1001 3003 8008 19448 43758 92378 184756",
				"  etc.",
				"The triangle T(n,k) begins:",
				"  n\\k |  0  1  2   3   4   5   6   7   8   9 10 11 12",
				"  ====+==============================================",
				"   0  |  1",
				"   1  |  1  0",
				"   2  |  1  2  0",
				"   3  |  1  3  2   0",
				"   4  |  1  4  6   2   0",
				"   5  |  1  5 10   9   2   0",
				"   6  |  1  6 15  20  12   2   0",
				"   7  |  1  7 21  35  34  15   2   0",
				"   8  |  1  8 28  56  70  52  18   2   0",
				"   9  |  1  9 36  84 126 125  74  21   2   0",
				"  10  |  1 10 45 120 210 252 205 100  24   2  0",
				"  11  |  1 11 55 165 330 462 461 315 130  27  2  0",
				"  12  |  1 12 66 220 495 792 924 786 460 164 30  2  0",
				"  etc."
			],
			"mathematica": [
				"Table[SeriesCoefficient[(1 - x^(# + 1))/((1 - x)^(# + 1)), {x, 0, k}] \u0026[n - k], {n, 0, 12}, {k, 0, n}] // Flatten (* _Michael De Vlieger_, Jul 20 2018 *)"
			],
			"program": [
				"(GAP) nmax:=15;; A:=List([0..nmax],n-\u003eList([0..nmax],k-\u003eBinomial(n+k,k)-Binomial(k-1,k-1-n)));;   b:=List([2..nmax],n-\u003eOrderedPartitions(n,2));;",
				"a:=Flat(List([1..Length(b)],i-\u003eList([1..Length(b[i])],j-\u003eA[b[i][j][2]][b[i][j][1]]))); # _Muniru A Asiru_, Jul 20 2018",
				"(PARI) T(n,k) = binomial(n+k,k) - binomial(k-1,k-1-n); \\\\ _Michel Marcus_, Aug 07 2018"
			],
			"xref": [
				"Row sums of the triangle give A099036 for n \u003e= 0.",
				"Cf. A000984 (main diagonal), A000012 (column 0), A087156 (column 1).",
				"Cf. A099036, A106510.",
				"In the square array; row 0..12 are: A000007, A040000, A008486, A005893, A008487, A008488, A008489, A008490, A008491, A008492, A008493, A008494, A008495.",
				"A173265 is based on the same square array, but is read by descending antidiagonals with special treatment of column 0."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "0,5",
			"author": "_Werner Schulte_, Jul 19 2018",
			"references": 0,
			"revision": 29,
			"time": "2018-08-23T17:23:15-04:00",
			"created": "2018-08-23T17:22:48-04:00"
		}
	]
}