{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A224216",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 224216,
			"data": "1,-1,-2,3,4,-6,-8,11,15,-20,-26,34,44,-56,-72,91,114,-143,-178,220,272,-334,-408,498,605,-732,-884,1064,1276,-1528,-1824,2171,2580,-3058,-3616,4269,5028,-5910,-6936,8124,9498,-11088,-12922,15034,17468,-20264",
			"name": "Expansion of q * f(-q,-q^7)^2 / (phi(q^2) * psi(-q)) in powers of q where phi(), psi(), f(,) are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A224216/b224216.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q * (f(-q^8) / f(q^2))^2 * f(-q,-q^7) / f(-q^3,-q^5) = q * f(-q,-q^7) * f(-q^2,-q^6)^2 / (f(-q^3,-q^5) * f(-q^4,-q^4)^2) in powers of q where f() is Ramanujan's theta function.",
				"Euler transform of period 8 sequence [ -1, -2, 1, 4, 1, -2, -1, 0, ...].",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2)) where f(u, v) = (u - v) * (1 + 2*u) - (u + u^2) * (1 - 4*v - 4*v^2).",
				"a(n) = (-1)^floor(n \\ 2) * A115671(n) unless n=0."
			],
			"example": [
				"q - q^2 - 2*q^3 + 3*q^4 + 4*q^5 - 6*q^6 - 8*q^7 + 11*q^8 + 15*q^9 + ..."
			],
			"mathematica": [
				"a[ n_] := (-1)^Floor[ n / 2] SeriesCoefficient[ (QPochhammer[ -q] / QPochhammer[ q] - 1) / 2, {q, 0, n}]",
				"a[ n_] := SeriesCoefficient[ q^(1/2) QPochhammer[ -q] EllipticTheta[ 2, 0, q^2] / EllipticTheta[ 4, 0, q^4]^2 QPochhammer[ q, q^8]^2 QPochhammer[ q^7, q^8]^2 / 2, {q, 0, n}]"
			],
			"program": [
				"(PARI) {a(n) = local(A); if( n\u003c1, 0, A = x * O(x^n); (-1)^(n \\ 2) * polcoeff( (-1 + eta(x^2 + A)^3 / eta(x + A)^2 / eta(x^4 + A)) / 2, n))}"
			],
			"xref": [
				"Cf. A115671, A208856."
			],
			"keyword": "sign",
			"offset": "1,3",
			"author": "_Michael Somos_, Apr 01 2013",
			"references": 5,
			"revision": 11,
			"time": "2021-03-12T22:24:46-05:00",
			"created": "2013-04-01T19:09:18-04:00"
		}
	]
}