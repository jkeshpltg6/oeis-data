{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A145010",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 145010,
			"data": "6,30,60,210,210,180,630,330,1320,1560,2340,990,2730,840,4620,3570,5610,4290,1710,7980,2730,6630,10920,12540,4080,8970,14490,18480,9690,3900,11550,25200,26910,30600,34650,32130,37050,7980,23460,6090,29580,49140,35700",
			"name": "a(n) = area of Pythagorean triangle with hypotenuse p, where p = A002144(n) = n-th prime == 1 (mod 4).",
			"comment": [
				"Pythagorean primes, i.e., primes of the form p = 4k+1 = A002144(n), have exactly one representation as sum of two squares: A002144(n) = x^2+y^2 = A002330(n+1)^2+A002331(n+1)^2. The corresponding (primitive) integer-sided right triangle with sides { 2xy, |x^2-y^2| } = { A002365(n), A002366(n) } has area xy|x^2-y^2| = a(n). For n\u003e1 this is a(n) = 30*A068386(n)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A145010/b145010.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A002365(n)*A002366(n)/2.",
				"a(n) = x*y*(x^2-y^2), where x = A002330(n+1), y = A002331(n+1)."
			],
			"example": [
				"The following table shows the relationship between several closely related sequences:",
				"Here p = A002144 = primes == 1 (mod 4), p = a^2+b^2 with a \u003c b;",
				"a = A002331, b = A002330, t_1 = ab/2 = A070151;",
				"p^2 = c^2+d^2 with c \u003c d; c = A002366, d = A002365,",
				"t_2 = 2ab = A145046, t_3 = b^2-a^2 = A070079,",
				"with {c,d} = {t_2, t_3}, t_4 = cd/2 = ab(b^2-a^2).",
				"  ---------------------------------",
				"   p  a  b  t_1  c   d t_2 t_3  t_4",
				"  ---------------------------------",
				"   5  1  2   1   3   4   4   3    6",
				"  13  2  3   3   5  12  12   5   30",
				"  17  1  4   2   8  15   8  15   60",
				"  29  2  5   5  20  21  20  21  210",
				"  37  1  6   3  12  35  12  35  210",
				"  41  4  5  10   9  40  40   9  180",
				"  53  2  7   7  28  45  28  45  630"
			],
			"mathematica": [
				"Reap[For[p = 2, p \u003c 500, p = NextPrime[p], If[Mod[p, 4] == 1, area = x*y/2 /. ToRules[Reduce[0 \u003c x \u003c= y \u0026\u0026 p^2 == x^2 + y^2, {x, y}, Integers]]; Sow[area]]]][[2, 1]] (* _Jean-François Alcover_, Feb 04 2015 *)"
			],
			"program": [
				"(PARI) forprime(p=1,499, p%4==1 | next; t=[p,lift(-sqrt(Mod(-1,p)))]; while(t[1]^2\u003ep,t=[t[2],t[1]%t[2]]); print1(t[1]*t[2]*(t[1]^2-t[2]^2)\",\"))",
				"(PARI) {Q=Qfb(1,0,1);forprime(p=1,499,p%4==1|next;t=qfbsolve(Q,p); print1(t[1]*t[2]*(t[1]^2-t[2]^2)\",\"))} \\\\ _David Broadhurst_"
			],
			"xref": [
				"Cf. A002144, A002365, A002366, A144954."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_M. F. Hasler_, Feb 24 2009",
			"references": 3,
			"revision": 19,
			"time": "2021-09-05T22:07:09-04:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}