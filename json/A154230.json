{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A154230",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 154230,
			"data": "1,1,1,1,100,1,1,455,455,1,1,1435,98810,1435,1,1,3711,1135370,1135370,3711,1,1,8388,7849141,464306300,7849141,8388,1,1,17161,40410421,10431621081,10431621081,40410421,17161,1,1,32495,169040786,130822910455,7140071740062,130822910455,169040786,32495,1",
			"name": "Triangle T(n, k) = T(n-1, k) + T(n-1, k-1) + ((n+1)*(n+2)*(2*n+3)*(3*n^2+9*n+5)/30)*T(n-2, k-1), read by rows.",
			"comment": [
				"Row sums are: {1, 2, 102, 912, 101682, 2278164, 480021360, 20944097328, ...}.",
				"The row sums of this class of sequences (see cross references) is given by the following. Let S(n) be the row sum then S(n) = 2*S(n-1) + f(n)*S(n-2) for a given f(n). For this sequence f(n) = (n+1)*(n+2)*(2*n+3)*(3*n^2+9*n+5)/30 = A000538(n+1). - _G. C. Greubel_, Mar 02 2021"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A154230/b154230.txt\"\u003eRows n = 0..50 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = T(n-1, k) + T(n-1, k-1) + ((n+1)*(n+2)*(2*n+3)*(3*n^2+9*n+5)/30)*T(n-2, k-1) with T(n, 0) = T(n, n) = 1."
			],
			"example": [
				"Triangle begins as:",
				"  1;",
				"  1,     1;",
				"  1,   100,        1;",
				"  1,   455,      455,           1;",
				"  1,  1435,    98810,        1435,           1;",
				"  1,  3711,  1135370,     1135370,        3711,        1;",
				"  1,  8388,  7849141,   464306300,     7849141,     8388,     1;",
				"  1, 17161, 40410421, 10431621081, 10431621081, 40410421, 17161, 1;"
			],
			"maple": [
				"T:= proc(n, k) option remember;",
				"      if k=0 or k=n then 1",
				"    else T(n-1, k) +T(n-1, k-1) +((n+1)*(n+2)*(2*n+3)*(3*n^2+9*n+5)/30)*T(n-2, k-1)",
				"      fi; end:",
				"seq(seq(T(n, k), k=0..n), n=0..12); # _G. C. Greubel_, Mar 02 2021"
			],
			"mathematica": [
				"T[n_, k_]:= T[n,k]= If[k==0 || k==n, 1, T[n-1, k] + T[n-1, k-1] + ((n+1)*(n+2)*(2*n+3)*(3*n^2+9*n+5)/30)*T[n-2, k-1] ];",
				"Table[T[n, k], {n,0,12}, {k,0,n}]//Flatten (* modified by _G. C. Greubel_, Mar 02 2021 *)"
			],
			"program": [
				"(Sage)",
				"def f(n): return (n+1)*(n+2)*(2*n+3)*(3*n^2+9*n+5)/30",
				"def T(n,k):",
				"    if (k==0 or k==n): return 1",
				"    else: return T(n-1, k) + T(n-1, k-1) + f(n)*T(n-2, k-1)",
				"flatten([[T(n,k) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, Mar 02 2021",
				"(Magma)",
				"f:= func\u003c n | (n+1)*(n+2)*(2*n+3)*(3*n^2+9*n+5)/30 \u003e;",
				"function T(n,k)",
				"  if k eq 0 or k eq n then return 1;",
				"  else return T(n-1, k) + T(n-1, k-1) + f(n)*T(n-2, k-1);",
				"  end if; return T;",
				"end function;",
				"[T(n,k): k in [0..n], n in [0..12]]; // _G. C. Greubel_, Mar 02 2021"
			],
			"xref": [
				"Cf. A154227, A154228, A154229, A154231, A154233.",
				"Cf. A000538."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "0,5",
			"author": "_Roger L. Bagula_, Jan 05 2009",
			"ext": [
				"Edited by _G. C. Greubel_, Mar 02 2021"
			],
			"references": 6,
			"revision": 12,
			"time": "2021-03-04T07:04:40-05:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}