{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A025147",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 25147,
			"data": "1,0,1,1,1,2,2,3,3,5,5,7,8,10,12,15,17,21,25,29,35,41,48,56,66,76,89,103,119,137,159,181,209,239,273,312,356,404,460,522,591,669,757,853,963,1085,1219,1371,1539,1725,1933,2164,2418,2702,3016,3362,3746,4171,4637,5155",
			"name": "Number of partitions of n into distinct parts \u003e= 2.",
			"comment": [
				"From _R. J. Mathar_, Jul 31 2008: (Start)",
				"These \"partitions of n into distinct parts \u003e= k\" and \"partitions of n into distinct parts, the least being k-1\" come in pairs of similar, almost shifted but not identical, sequences:",
				"A025147, A096765 (k=2)",
				"A025148, A096749 (k=3)",
				"A025149, A026824 (k=4)",
				"A025150, A026825 (k=5)",
				"A025151, A026826 (k=6)",
				"A025152, A026827 (k=7)",
				"A025153, A026828 (k=8)",
				"A025154, A026829 (k=9)",
				"A025155, A026830 (k=10)",
				"A096740, A026831 (k=11)",
				"The distinction in the definitions is that \"distinct parts \u003e= k\" sets a lower bound to all parts, whereas \"the least being ...\" means that the lower limit must be attained by one of the parts. (End)",
				"From _N. J. A. Sloane_, Sep 28 2008: (Start)",
				"Generating functions and Maple programs for the sequences in the first and second columns of the above list are respectively:",
				"For A025147, A025148, etc.:",
				"f:=proc(k) product(1+x^j, j=k..100): series(%,x,100): seriestolist(%); end;",
				"For A096765, A096749, etc.:",
				"g:=proc(k) x^(k-1)*product(1+x^j, j=k..100): series(%,x,100): seriestolist(%); end; (End)",
				"Also number of partitions of n+1 into distinct parts, the least being 1.",
				"Number of different sums from 1+[1,3]+[1,4]+...+[1,n]. - _Jon Perry_, Jan 01 2004",
				"Also number of partitions of n such that if k is the largest part, then all parts from 1 to k occur, k occurring at least twice. Example: a(7)=3 because we have [2,2,2,1],[2,2,1,1,1] and [1,1,1,1,1,1,1]. - _Emeric Deutsch_, Apr 09 2006",
				"Also number of partitions of n+1 such that if k is the largest part, then all parts from 1 to k occur, k occurring exactly once. Example: a(7)=3 because we have [3,2,2,1],[3,2,1,1,1] and [2,1,1,1,1,1,1] (there is a simple bijection with the partitions defined before). - _Emeric Deutsch_, Apr 09 2006",
				"Also number of partitions of n+1 into distinct parts where the number of parts is itself a part. - _Reinhard Zumkeller_, Nov 04 2007",
				"Partial sums give A038348 (observed by _Jonathan Vos Post_, proved by several correspondents).",
				"Trivially, number of partitions of n into distinct parts (as ascending lists) such that the first part is not 1, the second not 2, the third not 3, etc., see example. - _Joerg Arndt_, Jun 10 2013",
				"Convolution with A033999 gives A270144 (apart from the offset). - _R. J. Mathar_, Jun 18 2016"
			],
			"reference": [
				"Mohammad K. Azarian, A Generalization of the Climbing Stairs Problem, Mathematics and Computer Education, Vol. 31, No. 1, pp. 24-28, Winter 1997.  MathEduc Database (Zentralblatt MATH, 1997c.01891).",
				"Mohammad K. Azarian, A Generalization of the Climbing Stairs Problem II,   Missouri Journal of Mathematical Sciences, Vol. 16, No. 1, Winter 2004, pp. 12-17.  Zentralblatt MATH, Zbl 1071.05501."
			],
			"link": [
				"Reinhard Zumkeller and Alois P. Heinz, \u003ca href=\"/A025147/b025147.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e (terms n = 0..100 from Reinhard Zumkeller)",
				"Rebekah Ann Gilbert, \u003ca href=\"http://www.math.uiuc.edu/~rgilber1/AFineRediscovery_Gilbert.pdf\"\u003eA Fine Rediscovery\u003c/a\u003e, 2014.",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=798\"\u003eEncyclopedia of Combinatorial Structures 798\u003c/a\u003e",
				"Mircea Merca, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2015.08.014\"\u003eCombinatorial interpretations of a recent convolution for the number of divisors of a positive integer\u003c/a\u003e, Journal of Number Theory, Volume 160, March 2016, Pages 60-75, function q_1(n), see Corollary 3.7."
			],
			"formula": [
				"G.f.: Product_{k=2..infinity} (1+x^k).",
				"a(n) = A000009(n)-a(n-1) = Sum_{0\u003c=k\u003c=n} (-1)^k*A000009(n-k). - _Henry Bottomley_, May 09 2002",
				"a(n)=t(n, 1), where t(n, k)=1+Sum_{i\u003ej\u003ek and i+j=n} t(i, j), 2\u003c=k\u003c=n. - _Reinhard Zumkeller_, Jan 01 2003",
				"G.f.: 1 + Sum_{k=1..infinity} (x^(k*(k+3)/2) / Product_{j=1..k} (1-x^j)). - _Emeric Deutsch_, Apr 09 2006",
				"The previous g.f. is a special case of the g.f. for partitions into distinct parts \u003e= L, Sum_{n\u003e=0} ( x^(n*(n+2*L-1)/2) / Product_{k=1..n} (1-x^k) ). - _Joerg Arndt_, Mar 24 2011",
				"G.f.: Sum_{n\u003e=1} ( x^(n*(n+1)/2-1) / Product_{k=1..n-1} (1-x^k) ), a special case of the g.f. for partitions into distinct parts \u003e= L, Sum_{n\u003e=L-1} ( x^(n*(n+1)/2-L*(L-1)/2) / Product_{k=1..n-(L-1)} (1-x^k) ). - _Joerg Arndt_, Mar 27 2011",
				"a(n) = Sum_{1\u003ck\u003c=floor((n+2)/2)} A060016(n-k+1,k-1), for n\u003e0. - _Reinhard Zumkeller_, Nov 04 2007",
				"a(n) = A096765(n+1). - _R. J. Mathar_, Jul 31 2008",
				"From _Vaclav Kotesovec_, Aug 16 2015: (Start)",
				"a(n) ~ 1/2 * A000009(n).",
				"a(n) ~ exp(Pi*sqrt(n/3)) / (8*3^(1/4)*n^(3/4)).",
				"(End)"
			],
			"example": [
				"a(7) = 3, from {{3, 4}, {2, 5}, {7}}",
				"From _Joerg Arndt_, Jun 10 2013: (Start)",
				"There are a(17) = 21 partitions of 17 into distinct parts \u003e=2:",
				"01:  [ 2 3 4 8 ]",
				"02:  [ 2 3 5 7 ]",
				"03:  [ 2 3 12 ]",
				"04:  [ 2 4 5 6 ]",
				"05:  [ 2 4 11 ]",
				"06:  [ 2 5 10 ]",
				"07:  [ 2 6 9 ]",
				"08:  [ 2 7 8 ]",
				"09:  [ 2 15 ]",
				"10:  [ 3 4 10 ]",
				"11:  [ 3 5 9 ]",
				"12:  [ 3 6 8 ]",
				"13:  [ 3 14 ]",
				"14:  [ 4 5 8 ]",
				"15:  [ 4 6 7 ]",
				"16:  [ 4 13 ]",
				"17:  [ 5 12 ]",
				"18:  [ 6 11 ]",
				"19:  [ 7 10 ]",
				"20:  [ 8 9 ]",
				"21:  [ 17 ]",
				"(End)"
			],
			"maple": [
				"g:=product(1+x^j,j=2..65): gser:=series(g,x=0,62): seq(coeff(gser,x,n),n=0..57); # _Emeric Deutsch_, Apr 09 2006",
				"with(combstruct):ZL := {L = PowerSet(Sequence(Z,card\u003e=2)) },unlabeled:seq(count([L,ZL],size=i),i=0..57); # _Zerinvary Lajos_, Mar 09 2007"
			],
			"mathematica": [
				"CoefficientList[Series[Product[1+q^n, {n, 2, 60}], {q, 0, 60}], q]",
				"FoldList[ PartitionsQ[ #2+1 ]-#1\u0026, 0, Range[ 64 ] ]",
				"(* also *)",
				"d[n_] := Select[IntegerPartitions[n], Max[Length /@ Split@#] == 1 \u0026\u0026 Min[#] \u003e= 2 \u0026]; Table[d[n], {n, 12}] (* strict partitions, parts \u003e= 2 *)",
				"Table[Length[d[n]], {n, 40}] (* A025147 for n \u003e= 1 *)",
				"(* _Clark Kimberling_, Mar 07 2014 *)",
				"p[_, 0] = 1; p[k_, m_] := p[k, m] = If[m \u003c k, 0, p[k+1, m-k] + p[k+1, m]]; Table[p[2, m], {m, 0, 59}] (* _Jean-François Alcover_, Apr 17 2014, after _Reinhard Zumkeller_ *)"
			],
			"program": [
				"(Haskell)",
				"a025147 = p 2 where",
				"   p _ 0 = 1",
				"   p k m = if m \u003c k then 0 else p (k + 1) (m - k) + p (k + 1) m",
				"-- _Reinhard Zumkeller_, Dec 28 2011",
				"(PARI) a(n)=if(n,my(v=partitions(n));sum(i=1,#v,v[i][1]\u003e1\u0026\u0026v[i]==vecsort(v[i],,8)),1) \\\\ _Charles R Greathouse IV_, Nov 20 2012"
			],
			"xref": [
				"Cf. A015744, A015745, A015746, A015750, A015753, A015754, A015755, A002865."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,6",
			"author": "_Clark Kimberling_",
			"ext": [
				"Corrected and extended by _Dean Hickerson_, Oct 10 2001"
			],
			"references": 78,
			"revision": 79,
			"time": "2020-09-24T15:28:23-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}