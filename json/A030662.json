{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A030662",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 30662,
			"data": "1,5,19,69,251,923,3431,12869,48619,184755,705431,2704155,10400599,40116599,155117519,601080389,2333606219,9075135299,35345263799,137846528819,538257874439,2104098963719,8233430727599,32247603683099,126410606437751,495918532948103",
			"name": "Number of combinations of n things from 1 to n at a time, with repeats allowed.",
			"comment": [
				"Add terms of an increasingly bigger diamond-shaped part of Pascal's triangle:",
				".......................... 1",
				"............ 1 .......... 1 1",
				".. 1 ...... 1 1 ........ 1 2 1",
				". 1 1 =5 . 1 2 1 =19 .. 1 3 3 1 =69",
				".. 2 ...... 3 3 ........ 4 6 4",
				"............ 6 ......... 10 10",
				".......................... 20",
				"-  _Ralf Stephan_, May 17 2004",
				"The prime p divides a((p-1)/2) for p = 5,13,17,29,37,41,53,61,73,89,97.. = A002144[n] Pythagorean primes: primes of form 4n+1. - _Alexander Adamchuk_, Jul 04 2006",
				"Also, number of square submatrices of a square matrix. - Jono Henshaw (jjono(AT)hotmail.com), Apr 22 2008",
				"Partial sums of A051924. - _J. M. Bergot_, Jun 22 2013",
				"Number of partitions with Ferrers diagrams that fit in an n X n box (excluding the empty partition of 0). - _Michael Somos_, Jun 02 2014"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A030662/b030662.txt\"\u003eTable of n, a(n) for n = 1..500\u003c/a\u003e",
				"J. D. Horton and A. Kurn, Counting sequences with complete increasing subsequences, Congress Numerantium, 33 (1981), 75-80. \u003ca href=\"http://www.ams.org/mathscinet-getitem?mr=681905\"\u003eMR 681905\u003c/a\u003e",
				"M. Janjic and B. Petkovic, \u003ca href=\"http://arxiv.org/abs/1301.4550\"\u003eA Counting Function\u003c/a\u003e, arXiv preprint arXiv:1301.4550 [math.CO], 2013. - From _N. J. A. Sloane_, Feb 13 2013",
				"M. Janjic, B. Petkovic, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Janjic/janjic45.html\"\u003eA Counting Function Generalizing Binomial Coefficients and Some Other Classes of Integers\u003c/a\u003e, J. Int. Seq. 17 (2014) # 14.3.5",
				"Jianqiang Zhao, \u003ca href=\"http://arxiv.org/abs/1412.8044\"\u003eUniform Approach to Double Shuffle and Duality Relations of Various q-Analogs of Multiple Zeta Values via Rota-Baxter Algebras\u003c/a\u003e, arXiv preprint arXiv:1412.8044 [math.NT], 2014."
			],
			"formula": [
				"a(n) = A000984(n) - 1.",
				"a(n) = 2*(2*n-1)!/(n!*(n-1)!)-1.",
				"a(n) = Sum_{k=1..n} binomial(n, k)^2. - _Benoit Cloitre_, Aug 20 2002",
				"a(n) = Sum_{j=0..n} Sum_{i=j..n+j} binomial(i, j). - Klaus Strassburger (strass(AT)ddfi.uni-duesseldorf.de), Jul 23 2003",
				"a(n) = Sum_{i=0..n-1} Sum_{j=0..n-1} binomial(i+j, i). - _N. J. A. Sloane_, Jan 31 2009",
				"Also for n\u003e1: a(n)=(2*n)!/(n!)^2-1 - _Hugo Pfoertner_, Feb 10 2004",
				"a(n) = Sum[Sum[(2n-i-j)!/(n-i)!/(n-j)!,{i,1,n}],{j,1,n}]. - _Alexander Adamchuk_, Jul 04 2006",
				"a(n) = A115112(n) + 1. - Jono Henshaw (jjono(AT)hotmail.com), Apr 22 2008",
				"G.f.: Q(0)*(1-4*x)/x - 1/x/(1-x), where Q(k)= 1 + 4*(2*k+1)*x/( 1 - 1/(1 + 2*(k+1)/Q(k+1))); (continued fraction). - _Sergei N. Gladkovskii_, May 11 2013",
				"D-finite with recurrence: n*a(n) +2*(-3*n+2)*a(n-1) +(9*n-14)*a(n-2) +2*(-2*n+5)*a(n-3)=0. - _R. J. Mathar_, Jun 25 2013",
				"0 = a(n)*(+16*a(n+1) - 70*a(n+2) + 68*a(n+3) - 14*a(n+4)) + a(n+1)*(-2*a(n+1) + 61*a(n+2) - 96*a(n+3) + 23*a(n+4)) + a(n+2)*(-6*a(n+2) + 31*a(n+3)  - 10*a(n+4)) + a(n+3)*(-2*a(n+3) + a(n+4)) for all n in Z. - _Michael Somos_, Jun 02 2014",
				"From _Ilya Gutkovskiy_, Jan 25 2017: (Start)",
				"O.g.f.: (1 - x - sqrt(1 - 4*x))/((1 - x)*sqrt(1 - 4*x)).",
				"E.g.f.: exp(x)*(exp(x)*BesselI(0,2*x) - 1). (End)"
			],
			"example": [
				"G.f. = x + 5*x^2 + 19*x^3 + 69*x^4 + 251*x^5 + 923*x^6 + 3431*x^7 + ..."
			],
			"maple": [
				"seq(sum((binomial(n,m))^2,m=1..n),n=1..23); # _Zerinvary Lajos_, Jun 19 2008",
				"f:=n-\u003eadd( add( binomial(i+j,i), i=0..n),j=0..n); [seq(f(n),n=0..12)]; # _N. J. A. Sloane_, Jan 31 2009"
			],
			"mathematica": [
				"Table[Sum[Sum[(2n-i-j)!/(n-i)!/(n-j)!,{i,1,n}],{j,1,n}],{n,1,20}] (* _Alexander Adamchuk_, Jul 04 2006 *)",
				"a[n_] := 2*(2*n-1)!/(n*(n-1)!^2)-1; Table[a[n], {n, 1, 26}] (* _Jean-François Alcover_, Oct 11 2012, from first formula *)"
			],
			"program": [
				"(Sage)",
				"def a(n) : return binomial(2*n,n) - 1",
				"[a(n) for n in (1..26)] # _Peter Luschny_, Apr 21 2012",
				"(PARI) a(n)=binomial(2*n,n)-1 \\\\ _Charles R Greathouse IV_, Jun 26 2013"
			],
			"xref": [
				"2*A001700 - 1.",
				"Column k=2 of A047909.",
				"Cf. A091908, A144660, A002144.",
				"Central column of triangle A014473.",
				"Right-hand column 2 of triangle A102541."
			],
			"keyword": "nonn,nice",
			"offset": "1,2",
			"author": "Donald Mintz (djmintz(AT)home.com)",
			"references": 26,
			"revision": 66,
			"time": "2020-02-20T04:44:05-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}