{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A047842",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 47842,
			"data": "10,11,12,13,14,15,16,17,18,19,1011,21,1112,1113,1114,1115,1116,1117,1118,1119,1012,1112,22,1213,1214,1215,1216,1217,1218,1219,1013,1113,1213,23,1314,1315,1316,1317,1318,1319,1014,1114,1214,1314,24,1415,1416",
			"name": "Describe n (count digits in order of increasing value, ignoring missing digits).",
			"comment": [
				"Digit count of n. The digit count numerically summarizes the frequency of digits 0 through 9 in that order when they occur in a number. - _Lekraj Beedassy_, Jan 11 2007",
				"Numbers which are digital permutations of one another have the same digit count. Compare with first entries of \"Look And Say \" or LS sequence A045918. As in the latter, a(n) has first odd-numbered-digit entry occurring at n=1111111111 with digit count 101, but a(n) has first ambiguous term 1011. For digit count invariants, i.e., n such that a(n)=n, see A047841. - _Lekraj Beedassy_, Jan 11 2007"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A047842/b047842.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Onno M. Cain, Sela T. Enin, \u003ca href=\"https://arxiv.org/abs/2004.00209\"\u003eInventory Loops (i.e. Counting Sequences) have Pre-period 2 max S_1 + 60\u003c/a\u003e, arXiv:2004.00209 [math.NT], 2020.",
				"Andre Kowacs, \u003ca href=\"https://arxiv.org/abs/1708.06452\"\u003eStudies on the Pea Pattern Sequence\u003c/a\u003e, arXiv:1708.06452 [math.HO], 2017."
			],
			"formula": [
				"a(a(n)) = A235775(n).",
				"a(A010785(n)) = A244112(A010785(n)). - _Reinhard Zumkeller_, Nov 11 2014"
			],
			"example": [
				"a(31)=1113 because (one 1, one 3) make up 31.",
				"101 contains one 0 and two 1's, so a(101)=1021.",
				"a(131)=2113."
			],
			"mathematica": [
				"dc[n_] :=FromDigits@Flatten@Select[Table[{DigitCount[n, 10, k], k}, {k, 0, 9}], #[[1]] \u003e 0 \u0026];Table[dc[n], {n, 0, 46}] (* _Ray Chandler_, Jan 09 2009 *)",
				"Array[FromDigits@ Flatten@ Map[Reverse, Tally@ Sort@ IntegerDigits@ #] \u0026, 46] (* _Michael De Vlieger_, Jul 15 2020 *)"
			],
			"program": [
				"(Haskell)",
				"import Data.List (sort, group); import Data.Function (on)",
				"a047842 :: Integer -\u003e Integer",
				"a047842 n = read $ concat $",
				"   zipWith ((++) `on` show) (map length xs) (map head xs)",
				"   where xs = group $ sort $ map (read . return) $ show n",
				"-- _Reinhard Zumkeller_, Jan 15 2014",
				"(Python)",
				"def A047842(n):",
				"    s, x = '', str(n)",
				"    for i in range(10):",
				"        y = str(i)",
				"        c = str(x.count(y))",
				"        if c != '0':",
				"            s += c+y",
				"    return int(s) # _Chai Wah Wu_, Jan 03 2015",
				"(PARI) A047842(n,c=1,S=\"\")={for(i=2,#n=vecsort(digits(n)),n[i]==n[i-1]\u0026\u0026c++\u0026\u0026next;S=Str(S,c,n[i-1]);c=1);eval(Str(S,c,if(n,n[#n])))} \\\\ _M. F. Hasler_, Feb 25 2018"
			],
			"xref": [
				"Cf. A005151, A047841, A047843, A127354, A127355.",
				"Cf. A235775.",
				"Cf. A244112 (the same but in order of decreasing value of digits), A010785.",
				"Cf. A005150 (Look and Say: describe the number digit-wise instead of overall count)"
			],
			"keyword": "nonn,easy,base,nice",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Edited by _N. J. A. Sloane_, Jul 03 2008 at the suggestion of _R. J. Mathar_"
			],
			"references": 25,
			"revision": 37,
			"time": "2020-07-16T02:34:54-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}