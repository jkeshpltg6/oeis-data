{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A060006",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 60006,
			"data": "1,3,2,4,7,1,7,9,5,7,2,4,4,7,4,6,0,2,5,9,6,0,9,0,8,8,5,4,4,7,8,0,9,7,3,4,0,7,3,4,4,0,4,0,5,6,9,0,1,7,3,3,3,6,4,5,3,4,0,1,5,0,5,0,3,0,2,8,2,7,8,5,1,2,4,5,5,4,7,5,9,4,0,5,4,6,9,9,3,4,7,9,8,1,7,8,7,2,8,0,3,2,9,9,1",
			"name": "Decimal expansion of real root of x^3 - x - 1 (the plastic constant).",
			"comment": [
				"Has been also called the silver number, also the plastic number.",
				"This is the smallest Pisot-Vijayaraghavan number.",
				"The name \"plastic number\" goes back to the Dutch Benedictine monk and architect Dom Hans van der Laan, who gave this name 4 years after the discovery of the number by the French engineer Gérard Cordonnier in 1924, who used the name \"radiant number\". - _Hugo Pfoertner_, Oct 07 2018",
				"Sometimes denoted by the symbol rho. - _Ed Pegg Jr_, Feb 01 2019",
				"Also the solution of 1/x + 1/(1+x+x^2) = 1. - _Clark Kimberling_, Jan 02 2020",
				"Given any complex p such that real(p)\u003e-1, this constant is the only real solution of the equation z^p+z^(p+1)=z^(p+3), and the only attractor of the complex mapping z-\u003eM(z,p), where M(z,p)=(z^p+z^(p+1))^(1/(p+3)), convergent from any complex plane point. - _Stanislav Sykora_, Oct 14 2021"
			],
			"reference": [
				"Steven R. Finch, Mathematical Constants, Cambridge, 2003, Section 1.2.2.",
				"Midhat J. Gazalé, Gnomon: From Pharaohs to Fractals, Princeton University Press, Princeton, NJ, 1999, see Chap. VII.",
				"Donald E. Knuth, The Art of Computer Programming, Vol. 4A, Section 7.1.4, p. 236.",
				"Ian Stewart, Tales of a Neglected Number, Scientific American, No. 6, June 1996, pp. 92-93.",
				"Ian Stewart, A Guide to Computer Dating (Feedback), Scientific American, Vol. 275 No. 5, November 1996, p. 118.",
				"Dom Hans van der Laan, Le nombre plastique: Quinze leçons sur l’ordonnance architectonique, Brill Academic Pub., Leiden, 1960."
			],
			"link": [
				"Harry J. Smith, \u003ca href=\"/A060006/b060006.txt\"\u003eTable of n, a(n) for n = 1..20000\u003c/a\u003e",
				"Alex Bellos, \u003ca href=\"http://www.theguardian.com/science/alexs-adventures-in-numberland/2015/jan/13/golden-ratio-beautiful-new-curve-harriss-spiral\"\u003eThe golden ratio has spawned a beautiful new curve: the Harriss spiral\u003c/a\u003e, The Guardian, Jan 13 2015.",
				"Gamaliel Cerda-Morales, \u003ca href=\"https://arxiv.org/abs/1904.05492\"\u003eNew Identities for Padovan Numbers\u003c/a\u003e, arXiv:1904.05492 [math.CO], 2019.",
				"Brady Haran and Edmund Harriss, \u003ca href=\"https://www.youtube.com/watch?v=PsGUEj4w9Cc\"\u003eThe Plastic Ratio\u003c/a\u003e, Numberphile video (2019).",
				"Ed Pegg Jr., \u003ca href=\"/A060006/a060006.jpg\"\u003ePictures based on the plastic constant\u003c/a\u003e",
				"Simon Plouffe, \u003ca href=\"http://www.plouffe.fr/simon/constants/pisotv.txt\"\u003eSmallest Pisot-Vijayaraghavan number to 50000 digits\u003c/a\u003e",
				"Simon Plouffe, \u003ca href=\"https://web.archive.org/web/20150912063058/http://www.worldwideschool.org/library/books/sci/math/MiscellaneousMathematicalConstants/chap76.html\"\u003eThe Smallest Pisot-Vijayaraghavan number\u003c/a\u003e",
				"F. Rothelius, \u003ca href=\"https://web.archive.org/web/20010628042609/http://w1.875.telia.com/~u87509703/mathez/v2v3v4.gif\"\u003eFormulae\u003c/a\u003e.",
				"Ian Stewart, \u003ca href=\"http://wayback.archive.org/web/20120320051231/http://members.fortunecity.com/templarser/padovan.html\"\u003eTales of a Neglected Number\u003c/a\u003e.",
				"Michel Waldschmidt, \u003ca href=\"http://www.math.jussieu.fr/~miw/articles/pdf/MZV2011IMSc.pdf\"\u003eLectures on Multiple Zeta Values\u003c/a\u003e, IMSC 2011.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Pisot-VijayaraghavanConstant.html\"\u003ePisot-Vijayaraghavan Constant\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PisotNumber.html\"\u003ePisot Number\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PlasticConstant.html\"\u003ePlastic Constant\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Plastic_number\"\u003ePlastic number\u003c/a\u003e."
			],
			"formula": [
				"Equals (1/2+sqrt(23/108))^(1/3) + (1/2-sqrt(23/108))^(1/3). - _Henry Bottomley_, May 22 2003",
				"Equals CubeRoot(1 + CubeRoot(1 + CubeRoot(1 + CubeRoot(1 + ...)))). - _Gerald McGarvey_, Nov 26 2004",
				"Equals sqrt(1+1/sqrt(1+1/sqrt(1+1/sqrt(1+...)))). - _Gerald McGarvey_, Mar 18 2006",
				"Equals (1/2 +sqrt(23/3)/6)^(1/3) + (1/2-sqrt(23/3)/6)^(1/3). - _Eric Desbiaux_, Oct 17 2008",
				"Equals Sum_{k \u003e= 0} 27^(-k)/k!*(Gamma(2*k+1/3)/(9*Gamma(k+4/3)) - Gamma(2*k-1/3)/(3*Gamma(k+2/3))). - _Robert Israel_, Jan 13 2015",
				"Equals sqrt(Phi) = sqrt(1.754877666246...) (see A109134). - _Philippe Deléham_, Sep 29 2020",
				"Equals cosh(arccosh(3*c)/3)/c, where c = sqrt(3)/2 (A010527). - _Amiram Eldar_, May 15 2021"
			],
			"example": [
				"1.32471795724474602596090885447809734..."
			],
			"maple": [
				"(1/2 +sqrt(23/3)/6)^(1/3) + (1/2-sqrt(23/3)/6)^(1/3) ; evalf(%,130) ; # _R. J. Mathar_, Jan 22 2013"
			],
			"mathematica": [
				"RealDigits[ Solve[x^3 - x - 1 == 0, x][[1, 1, 2]], 10, 111][[1]] (* _Robert G. Wilson v_, Sep 30 2009 *)",
				"s = Sqrt[23/108]; RealDigits[(1/2 + s)^(1/3) + (1/2 - s)^(1/3), 10, 111][[1]] (* _Robert G. Wilson v_, Dec 12 2017 *)",
				"RealDigits[Root[x^3-x-1,1],10,120][[1]] (* or *) RealDigits[(Surd[9-Sqrt[69],3]+Surd[9+Sqrt[69],3])/(Surd[2,3]Surd[9,3]),10,120][[1]] (* _Harvey P. Dale_, Sep 04 2018 *)"
			],
			"program": [
				"(PARI) allocatemem(932245000); default(realprecision, 20080); x=solve(x=1, 2, x^3 - x - 1); for (n=1, 20000, d=floor(x); x=(x-d)*10; write(\"b060006.txt\", n, \" \", d)); \\\\ _Harry J. Smith_, Jul 01 2009",
				"(PARI) (1/2 +sqrt(23/3)/6)^(1/3) + (1/2-sqrt(23/3)/6)^(1/3) \\\\ _Altug Alkan_, Apr 10 2016",
				"(PARI) polrootsreal(x^3-x-1)[1] \\\\ _Charles R Greathouse IV_, Aug 28 2016",
				"(MAGMA) SetDefaultRealField(RealField(100)); ((3+Sqrt(23/3))/6)^(1/3) + ((3-Sqrt(23/3))/6)^(1/3); // _G. C. Greubel_, Mar 15 2019",
				"(Sage) numerical_approx(((3+sqrt(23/3))/6)^(1/3) + ((3-sqrt(23/3))/6)^(1/3), digits=100) # _G. C. Greubel_, Mar 15 2019"
			],
			"xref": [
				"Cf. A001622. A072117 gives continued fraction.",
				"Cf. A006888, A010527, A051016, A051017, A084252, A075778 (inverse), A126772.",
				"Other Pisot numbers: A086106, A092526, A228777, A293506, A293508, A293509, A293557."
			],
			"keyword": "cons,nice,nonn",
			"offset": "1,2",
			"author": "_Fabian Rothelius_, Mar 14 2001",
			"ext": [
				"Edited and extended by _Robert G. Wilson v_, Aug 03 2002",
				"Removed incorrect comments, _Joerg Arndt_, Apr 10 2016"
			],
			"references": 68,
			"revision": 164,
			"time": "2021-10-15T11:25:46-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}