{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A085365",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 85365,
			"data": "1,1,4,9,4,2,0,4,4,8,5,3,2,9,6,2,0,0,7,0,1,0,4,0,1,5,7,4,6,9,5,9,8,7,4,2,8,3,0,7,9,5,3,3,7,2,0,0,8,6,3,5,1,6,8,4,4,0,2,3,3,9,6,5,1,8,9,6,6,0,1,2,8,2,5,3,5,3,0,5,1,1,7,7,9,4,0,7,7,2,4,8,4,9,8,5,8,3,6,9,9,3,7,6,3,4",
			"name": "Decimal expansion of the Kepler-Bouwkamp or polygon-inscribing constant.",
			"comment": [
				"Inscribe an equilateral triangle in a circle of unit radius. Inscribe a circle in the triangle. Inscribe a square in the second circle and inscribe a circle in the square. Inscribe a regular pentagon in the third circle and so on. The radii of the circles converge to Product_{ k = 3..infinity } cos(Pi/k), which is this number. - _N. J. A. Sloane_, Feb 10 2008",
				"\"It is stated in Kasner and Newman's 'Mathematics and the Imagination' (pp. 269-270 in the Pelican edition) that P=Product{k=3..infinity} cos(Pi/k) is approximately equal to 1/12. Not so! ..., so that a very good approximation to P is 10/87 ...\", by Grimstone. - _Robert G. Wilson v_, Dec 22 2013",
				"Named after the German astronomer and mathematician Johannes Kepler (1571 - 1630) and the Dutch mathematician Christoffel Jacob Bouwkamp (1915 - 2003). - _Amiram Eldar_, Aug 21 2020"
			],
			"reference": [
				"Dick Katz, Problem 91:24, in R. K. Guy, ed., Western Number Theory Problems, 1992-12-19 \u0026 22.",
				"S. R. Finch, Mathematical Constants. Cambridge University Press (2003). MR 2003519.",
				"Clifford A. Pickover, The Math Book, From Pythagoras to the 57th Dimension, 250 Milestones in the History of Mathematics, Sterling Publ., NY, 2009, p. 382."
			],
			"link": [
				"Vaclav Kotesovec, \u003ca href=\"/A085365/b085365.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"C. J. Bouwkamp, \u003ca href=\"https://core.ac.uk/download/pdf/82376060.pdf\"\u003eAn infinite product\u003c/a\u003e, Proceedings of the Koninklijke Nederlandse Akademie van Wetenschappen: Series A: Mathematical Sciences, Vol. 68 (1965), pp. 40-46.",
				"Hugo Brandt, \u003ca href=\"https://doi.org/10.1111/j.1949-8594.1953.tb06870.x\"\u003eProblem 2356\u003c/a\u003e, solved by Julian H. Braun, School Science and Mathematics, Vol. 53, No. 7 (1953), pp. 575-576.",
				"Marc Chamberland and Armin Straub, \u003ca href=\"https://doi.org/10.1016/j.aam.2013.07.003\"\u003eOn gamma quotients and infinite products\u003c/a\u003e, Advances in Applied Mathematics, Vol. 51, No. 5 (2013), pp. 546-562, \u003ca href=\"http://arxiv.org/abs/1309.3455\"\u003epreprint\u003c/a\u003e, arXiv:1309.3455 [math.NT], 2013. See Section 4.",
				"Tamara Curnow, \u003ca href=\"http://www.appliedprobability.org/content.aspx?Group=ms\u0026amp;Page=MS264\"\u003eFalling down a polygonal well, Mathematical Spectrum, Vol. 26, No. 4 (1994), pp. 110-118.",
				"Tomislav Doslic, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Doslic/doslic3.html\"\u003eKepler-Bouwkamp Radius of Combinatorial Sequences\u003c/a\u003e, J. Int. Seq. 17 (2014) # 14.11.3.",
				"Steven R. Finch, \u003ca href=\"http://arxiv.org/abs/2001.00578\"\u003eErrata and Addenda to Mathematical Constants\u003c/a\u003e, p. 58.",
				"Clive J. Grimstone, \u003ca href=\"http://www.jstor.org/stable/3615085\"\u003eA product of cosines\u003c/a\u003e, Math. Gaz. 64 (428) (1980) 120-121.",
				"Johannes Kepler, \u003ca href=\"https://archive.org/details/1596-kepler-prodromus-dissertationum-cosmographicarum-continens-mysterium-cosmographicum/page/38/mode/2up\"\u003eMysterium Cosmographicum\u003c/a\u003e, Tübingen, 1596. See p. 39.",
				"M. H. Lietzke and C. W. Nestor, Jr., \u003ca href=\"https://www.jstor.org/stable/2310734\"\u003eProblem 4793\u003c/a\u003e, The American Mathematical Monthly, Vol. 65, No. 6 (1958), pp. 451-452, \u003ca href=\"https://www.jstor.org/stable/2309535\"\u003eAn Infinite Sequence of Inscribed Polygons, solution to Problem 4793\u003c/a\u003e, solved by Julian Braun and others, ibid., Vol. 66, No. 3 (1959), pp. 242-243.",
				"Kival Ngaokrajang, \u003ca href=\"/A085365/a085365.jpg\"\u003eIllustration of polygon inscribing\u003c/a\u003e.",
				"David Singmaster, \u003ca href=\"http://www.appliedprobability.org/content.aspx?Group=ms\u0026amp;Page=MS273\"\u003eLetter to the Editor: Kepler's polygonal well\u003c/a\u003e, Mathematical Spectrum, Vol. 27, No. 3 (1995), pp. 63-64.",
				"E. Stephens, \u003ca href=\"https://www.jstor.org/stable/3618092\"\u003e79.52 Slowly convergent infinite products\u003c/a\u003e, The Mathematical Gazette, Vol. 79, No. 486 (1995), pp. 561-565.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PolygonInscribing.html\"\u003ePolygon Inscribing\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Kepler-Bouwkamp_constant\"\u003eKepler-Bouwkamp constant\u003c/a\u003e."
			],
			"formula": [
				"The log of this constant is equal to Sum_{n=1..infinity} -((2^(2*n)-1)/n) * zeta(2*n) * (zeta(2*n)-1-1/2^(2*n)). [Richard McIntosh] - _N. J. A. Sloane_, Feb 10 2008",
				"Equals 1/A051762. - _M. F. Hasler_, May 18 2014"
			],
			"example": [
				"0.1149420448532..."
			],
			"maple": [
				"evalf(1/(product(sec(Pi/k), k=3..infinity)), 104) # _Vaclav Kotesovec_, Sep 20 2014"
			],
			"mathematica": [
				"(* The naive approach, N[ Product[ Cos[ Pi/n], {n, 3, Infinity}], 111], yields only 27 correct decimals. - _Vaclav Kotesovec_, Sep 20 2014 *)",
				"Block[{$MaxExtraPrecision = 1000}, Do[Print[N[Exp[Sum[-(2^(2*n)-1)/n * Zeta[2*n]*(Zeta[2*n] - 1 - 1/2^(2*n)), {n, 1, m}]], 110]], {m, 250, 300}]] (* over 100 decimal places are correct, _Vaclav Kotesovec_, Sep 20 2014 *)"
			],
			"program": [
				"(PARI) exp(sumpos(n=3,log(cos(Pi/n)))) \\\\ _M. F. Hasler_, May 18 2014"
			],
			"xref": [
				"Equals 1/A051762.",
				"Cf. A131671."
			],
			"keyword": "nonn,cons",
			"offset": "0,3",
			"author": "_Eric W. Weisstein_, Jun 25 2003",
			"ext": [
				"Edited by _M. F. Hasler_, May 18 2014",
				"First formula corrected (missing sign) by _Vaclav Kotesovec_, Sep 20 2014",
				"Terms since 27 corrected by _Vaclav Kotesovec_, Sep 20 2014 (recomputed with higher precision)"
			],
			"references": 9,
			"revision": 66,
			"time": "2020-08-21T05:47:53-04:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}