{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A022169",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 22169,
			"data": "1,1,1,1,6,1,1,31,31,1,1,156,806,156,1,1,781,20306,20306,781,1,1,3906,508431,2558556,508431,3906,1,1,19531,12714681,320327931,320327931,12714681,19531,1,1,97656,317886556",
			"name": "Triangle of Gaussian binomial coefficients [ n,k ] for q = 5.",
			"comment": [
				"The coefficients of the matrix inverse are apparently given by T^(-1)(n,k) = (-1)^n*A157832(n,k). - _R. J. Mathar_, Mar 12 2013"
			],
			"reference": [
				"F. J. MacWilliams and N. J. A. Sloane, The Theory of Error-Correcting Codes, Elsevier-North Holland, 1978, p. 698.",
				"M. Sved, Gaussians and binomials, Ars. Combinatoria, 17A (1984), 325-351."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A022169/b022169.txt\"\u003eRows n=0..50 of triangle, flattened\u003c/a\u003e",
				"R. Mestrovic, \u003ca href=\"http://arxiv.org/abs/1409.3820\"\u003eLucas' theorem: its generalizations, extensions and applications (1878--2014)\u003c/a\u003e, arXiv preprint arXiv:1409.3820 [math.NT], 2014.",
				"Kent E. Morrison, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL9/Morrison/morrison37.html\"\u003eInteger Sequences and Matrices Over Finite Fields\u003c/a\u003e, Journal of Integer Sequences, Vol. 9 (2006), Article 06.2.1.",
				"\u003ca href=\"/index/Ga#Gaussian_binomial_coefficients\"\u003eIndex entries for sequences related to Gaussian binomial coefficients\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = T(n-1,k-1) + q^k * T(n-1,k). - _Peter A. Lawrence_, Jul 13 2017"
			],
			"example": [
				"1;",
				"1, 1;",
				"1, 6, 1;",
				"1, 31, 31, 1;",
				"1, 156, 806, 156, 1;",
				"1, 781, 20306, 20306, 781, 1;",
				"1, 3906, 508431, 2558556, 508431, 3906, 1;",
				"1, 19531, 12714681, 320327931, 320327931, 12714681, 19531, 1,"
			],
			"maple": [
				"A027872 := proc(n)",
				"        mul( 5^i-1, i=1..n) ;",
				"end proc:",
				"A022169 := proc(n, m)",
				"        A027872(n)/A027872(n-m)/A027872(m) ;",
				"end proc: # _R. J. Mathar_, Mar 12 2013"
			],
			"mathematica": [
				"p[n_] := Product[5^i-1, {i, 1, n}]; t[n_, k_] := p[n]/(p[k]*p[n-k]); Table[t[n, k], {n, 0, 8}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Jan 14 2014 *)",
				"Table[QBinomial[n,k,5], {n,0,10}, {k,0,n}]//Flatten (* or *) q:= 5; T[n_, 0]:= 1; T[n_,n_]:= 1; T[n_,k_]:= T[n,k] = If[k \u003c 0 || n \u003c k, 0, T[n-1, k -1] +q^k*T[n-1,k]]; Table[T[n,k], {n,0,10}, {k,0,n}] // Flatten  (* _G. C. Greubel_, May 27 2018 *)"
			],
			"program": [
				"(PARI) {q=5; T(n,k) = if(k==0,1, if (k==n, 1, if (k\u003c0 || n\u003ck, 0, T(n-1,k-1) + q^k*T(n-1,k))))};",
				"for(n=0,10, for(k=0,n, print1(T(n,k), \", \"))) \\\\ _G. C. Greubel_, May 27 2018"
			],
			"xref": [
				"Cf. A003462 (column k=1), A006111 (k=2), A006112 (k=3).",
				"Row sums give A006119."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_",
			"references": 20,
			"revision": 40,
			"time": "2019-05-13T15:25:28-04:00",
			"created": "1998-06-14T03:00:00-04:00"
		}
	]
}