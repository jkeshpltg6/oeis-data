{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000905",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 905,
			"id": "M0736 N0275",
			"data": "2,3,5,11,47,923,409619,83763206255,3508125906290858798171,6153473687096578758448522809275077520433167,18932619208894981833333582059033329370801266249535902023330546944758507753065602135843",
			"name": "Hamilton numbers.",
			"comment": [
				"a(n) is the minimal degree of an equation from which n successive terms after the first can be removed (by a series of transformation comparable to Tschirnhaus') without requiring the solution of an equation of degree greater than n (and excluding cases where an equation of degree greater than n is needed but is in fact factorizable into several equations of degree all less than n). Hamilton computed the first six terms of this sequence (see reference). That is the reason Sylvester and Hammond named them \"Hamilton numbers\". - _Olivier Gérard_, Oct 17 2007",
				"Named after the Irish mathematician William Rowan Hamilton (1805-1865). - _Amiram Eldar_, Jun 19 2021"
			],
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A000905/b000905.txt\"\u003eTable of n, a(n) for n = 1..14\u003c/a\u003e",
				"Tigran Ananyan and Melvin Hochster, \u003ca href=\"https://arxiv.org/abs/2003.00589\"\u003eUniversal Lex Ideal Approximations of Extended Hilbert Functions and Hamilton Numbers\u003c/a\u003e, arXiv:2003.00589 [math.AC], 2020.",
				"Alexander Chen, Y. H. He and J. McKay, \u003ca href=\"https://arxiv.org/abs/1711.09253\"\u003eErland Samuel Bring's \"Transformation of Algebraic Equations\"\u003c/a\u003e, arXiv preprint arXiv:1711.09253 [math.HO], 2017. See page 6.",
				"Raymond Garver, \u003ca href=\"http://www.jstor.org/stable/1968002\"\u003eThe Tschirnhaus transformation\u003c/a\u003e, The Annals of Mathematics, 2nd Ser., Vol. 29, No. 1/4. (1927 - 1928), pp. 329.",
				"William Rowan Hamilton, \u003ca href=\"https://emis.de/classics/Hamilton/Jerrard.pdf\"\u003eInquiry Into the Validity of a Method Recently Proposed by George B. Jerrard, Esq. for Transforming and Resolving Equations of Elevated Degrees Undertaken at the Request of the Association. Richard and John E. Taylor\u003c/a\u003e, Report of the Sixth Meeting of the British Association for the Advancement of Science, held at Bristol in August 1836 (London: John Murray, Albemarle St., 1837), pp. 295-348.",
				"Edouard Lucas, \u003ca href=\"http://gallica.bnf.fr/ark:/12148/bpt6k29021h\"\u003eThéorie des Nombres\u003c/a\u003e, Gauthier-Villars, Paris, 1891, Vol. 1, p. 496.",
				"Edouard Lucas, \u003ca href=\"/A000904/a000904.pdf\"\u003eThéorie des Nombres\u003c/a\u003e, Gauthier-Villars, Paris, 1891, Vol. 1. [Annotated scan of pages 488-499 only]",
				"Alexander J. Sutherland, \u003ca href=\"https://arxiv.org/abs/2107.08139\"\u003eUpper Bounds on Resolvent Degree and Its Growth Rate\u003c/a\u003e, arXiv:2107.08139 [math.AG], 2021.",
				"James Joseph Sylvester and James Hammond, \u003ca href=\"http://www.jstor.org/stable/90558\"\u003eOn Hamilton's numbers\u003c/a\u003e, Philosophical Transactions of the Royal Society of London A, Vol. 178 (1887), pp. 285-312, \u003ca href=\"https://archive.org/details/philtrans02248142\"\u003ealternative link\u003c/a\u003e.",
				"Jesse Wolfson, \u003ca href=\"https://arxiv.org/abs/2001.06515\"\u003eTschirnhaus transformations after Hilbert\u003c/a\u003e, arXiv:2001.06515 [math.AG], 2020."
			],
			"example": [
				"a(1)=2 is the familiar fact than one can always remove the linear term of a quadratic equation.",
				"a(2)=3 because one can put any cubic equation in the form x^3-a=0 by a Tschirnhaus transformation based on the solutions of a quadratic equation.",
				"a(4)=11 because one can remove the 4 terms after the first term in a polynomial of degree 11 without having to solve a quintic."
			],
			"maple": [
				"A000905 := proc(n) option remember; local i; if n=1 then 2 else 2+add((-1)^(i+1)*binomial(A000905(n-i),i+1),i=1..n-1); fi; end;"
			],
			"mathematica": [
				"a[1]=2; a[n_] := a[n] = 2+Sum[(-1)^(i+1)*Product[a[n-i] - k, {k, 0, i}]/(i+1)!, {i, 1, n-1}]; Table[a[n], {n, 1, 11}] (* _Jean-François Alcover_, May 17 2011, after Maple prog. *)"
			],
			"xref": [
				"Cf. A001660.",
				"Equals A006719(n) - 1.",
				"Cf. A134294."
			],
			"keyword": "nonn,nice,easy",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"The formula given by Lucas on p. 498 is slightly in error - see Maple program given here."
			],
			"references": 5,
			"revision": 64,
			"time": "2021-12-02T12:56:08-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}