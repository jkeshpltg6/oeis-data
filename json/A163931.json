{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A163931",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 163931,
			"data": "0,9,7,8,4,3,1,9,7,2,1,6,6,7,0,1,7,9,3,2,5,5,3,7,7,8,9,0,4,5,2,8,0,0,8,2,7,6,9,5,8,2,2,6,9,5,3,0,2,6,5,7,6,5,5,7,4,4,2,1,2,4,2,4,5,4,4,7,1,3,7,6,2,6,1,4,0,9,0,4,8,8,7,3,6,9,6,0,4,8,9,1,8,5,5,5,0,8,9,4,5,4,6,7,0",
			"name": "Decimal expansion of the higher-order exponential integral E(x, m=2, n=1) at x=1.",
			"comment": [
				"We define the higher-order exponential integrals by E(x,m,n) = x^(n-1)*Integral_{t=x..infinity} E(t,m-1,n)/t^n for m \u003e= 1 and n \u003e= 1 with E(x,m=0,n) = exp(-x), see Meijer and Baken.",
				"The properties of the E(x,m,n) are analogous to those of the well-known exponential integrals E(x,m=1,n), see Abramowitz and Stegun and the formulas.",
				"The series expansions of the higher-order exponential integrals are dominated by the constants alpha(k,n), see A163927, and gamma(k,n) = G(k,n), see A163930.",
				"For information about the asymptotic expansion of the E(x,m,n) see A163932.",
				"Values of E(x,m,n) can be evaluated with the Maple program."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A163931/b163931.txt\"\u003eTable of n, a(n) for n = 0..5000\u003c/a\u003e",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972, Chapter 5, pp. 227-251.",
				"J. W. Meijer and N. H. G. Baken, \u003ca href=\"http://dx.doi.org/10.1016/0167-7152(87)90041-1\"\u003eThe Exponential Integral Distribution\u003c/a\u003e, Statistics and Probability Letters, Volume 5, No.3, April 1987. pp 209-211.",
				"M. S. Milgram, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1985-0777276-4\"\u003eThe generalized integro-exponential function\u003c/a\u003e, Math. of Computation, Vol. 44, pp. 443-458, 1985.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ExponentialIntegral.html\"\u003eThe Exponential Integral\u003c/a\u003e."
			],
			"formula": [
				"E(x=1,m=2,n=1) = gamma^2/2 + Pi^2/12 + Sum_{k\u003e=1} ((-1)^k/(k^2*k!)).",
				"E(x=0,n,m) = (1/(n-1))^m for n \u003e= 2.",
				"Integral_{t=0..x} E(t,m,n) = 1/n^m - E(x,n,n+1).",
				"dE(x,m,n+1)/dx = - E(x,m,n).",
				"E(x,m,n+1) = (1/n)*(E(x,m-1,n+1) - x*E(x,m,n)).",
				"E(x,m,n) = (-1)^m * ((-x)^(n-1)/(n-1)!) * Sum_{kz=0..floor(m/2)}(alpha (kz, n)*G(m-2*kz, n)) + (-1) ^m * ((-x)^(n-1)/(n-1)!) * Sum_{kz=0..floor(m/2)}(Sum_{i=1..m-2*kz}(alpha (kz, n) *G(m-2*kz-i, n)*log(x)^i/i!)) + (-1)^m * Sum_{ kx=0..n-2}((-x)^kx/((kx-n+1)^m*kx!) + (-1)^m * Sum_{ky\u003e=n}((-x)^ky /(( ky-n+1)^m*ky!))."
			],
			"example": [
				"E(1,2,1) = 0.09784319721667017932553778904528008276958226953026576557442124245...."
			],
			"maple": [
				"E:= proc(x,m,n) local nmax, kmax, EI, k1, k2, n1, n2; option remember: nmax:=20; kmax:=20; k1:=0: for n1 from 0 to nmax do alpha(k1,n1):=1 od: for k1 from 1 to kmax do for n1 from 1 to nmax do alpha(k1,n1) := (1/k1)*sum(sum(p^(-2*(k1-i1)),p=0..n1-1)*alpha(i1, n1),i1=0..k1-1) od; od: for n2 from 0 to kmax do G(0,n2):=1 od: for n2 from 1 to nmax do for k2 from 1 to kmax do G(k2,n2):=(1/k2)*(((gamma-sum(p^(-1),p=1..n2-1))*G(k2-1,n2)+ sum((Zeta(k2-i2)-sum(p^(-(k2-i2)), p=1..n2-1))*G(i2,n2),i2=0..k2-2))) od; od: EI:= evalf((-1)^m*((-x)^(n-1)/(n-1)!*sum(alpha(kz,n)*(G(m-2*kz,n)+sum(G(m-2*kz-i,n)*ln(x)^i/i!,i=1..m-2*kz)), kz=0..floor(m/2)) + sum((-x)^kx/((kx-n+1)^m*kx!),kx=0..n-2) + sum((-x)^ky/((ky-n+1)^m*ky!),ky=n..infinity))); return(EI): end:"
			],
			"mathematica": [
				"Join[{0}, RealDigits[ N[ EulerGamma^2/2 + Pi^2/12 - HypergeometricPFQ[{1, 1, 1}, {2, 2, 2}, -1], 104]][[1]]] (* _Jean-François Alcover_, Nov 07 2012, from 1st formula *)"
			],
			"program": [
				"(PARI) t=1; Euler^2/2 + Pi^2/12 + sumalt(k=1, t*=k; (-1)^k/(k^2*t)) \\\\ _Charles R Greathouse IV_, Nov 07 2016"
			],
			"xref": [
				"Cf. A163927 (alpha(k,n)), A163930 (gamma(k,n) = G(k,n)), A163932.",
				"Cf. A068985 (E(x=1,m=0,n) = exp(-1)) and A099285 (E(x=1,m=1,n=1)).",
				"Cf. A001563 (n*n!), A002775 (n^2*n!), A091363 (n^3*n!) and A091364 (n^4*n!)."
			],
			"keyword": "cons,easy,nonn",
			"offset": "0,2",
			"author": "_Johannes W. Meijer_ and _Nico Baken_, Aug 13 2009, Aug 17 2009",
			"references": 72,
			"revision": 36,
			"time": "2019-12-06T21:43:01-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}