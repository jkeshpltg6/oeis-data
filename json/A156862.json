{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A156862",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 156862,
			"data": "2,2,2,3,2,3,6,2,2,6,13,3,0,3,13,28,7,-3,-3,7,28,59,18,-6,-14,-6,18,59,122,44,-6,-32,-32,-6,44,122,249,101,4,-58,-80,-58,4,101,249,504,221,39,-90,-162,-162,-90,39,221,504,1015,468,130,-119,-292,-356,-292,-119,130,468,1015",
			"name": "Triangle read by rows: T(n, k) = 2^k - binomial(n, k+1) + 2^(n-k) - binomial(n, n-k+1).",
			"comment": [
				"Row sums are 2^(n+1): {2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, ...}."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A156862/b156862.txt\"\u003eRows n = 0..100 of triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = 2^k - binomial(n, k+1) + 2^(n-k) - binomial(n, n-k+1)."
			],
			"example": [
				"Triangle begins as:",
				"     2;",
				"     2,   2;",
				"     3,   2,   3;",
				"     6,   2,   2,    6;",
				"    13,   3,   0,    3,   13;",
				"    28,   7,  -3,   -3,    7,   28;",
				"    59,  18,  -6,  -14,   -6,   18,   59;",
				"   122,  44,  -6,  -32,  -32,   -6,   44,  122;",
				"   249, 101,   4,  -58,  -80,  -58,    4,  101, 249;",
				"   504, 221,  39,  -90, -162, -162,  -90,   39, 221, 504;",
				"  1015, 468, 130, -119, -292, -356, -292, -119, 130, 468, 1015;"
			],
			"maple": [
				"f(n,m):= 2^m - binomial(n, m+1); seq(seq( f(n,k) + f(n,n-k), k=0..n), n=0..10); # _G. C. Greubel_, Dec 01 2019"
			],
			"mathematica": [
				"f[n_, m_]:= 2^m - Binomial[n, m+1]; T[n_,k_]:= f[n,k] + f[n,n-k]; Table[T[n, k], {n, 0, 10}, {k, 0, n}]//Flatten"
			],
			"program": [
				"(PARI) T(n,k) = my(f(n,m)=2^m - binomial(n, m+1)); f(n,k) + f(n,n-k); \\\\ _G. C. Greubel_, Dec 01 2019",
				"(MAGMA)",
				"f:= func\u003c n, m | 2^m - Binomial(n, m+1) \u003e;",
				"[f(n,k)+f(n,n-k): k in [0..n], n in [0..10]]; // _G. C. Greubel_, Dec 01 2019",
				"(Sage)",
				"def f(n,m): return 2^m - binomial(n, m+1)",
				"def T(n,k): return f(n,k) + f(n,n-k)",
				"[[T(n,k) for k in (0..n)] for n in (0..10)] # _G. C. Greubel_, Dec 01 2019",
				"(GAP) Flat(List([0..10], n-\u003e List([0..n], k-\u003e 2^k - Binomial(n,k+1) + 2^(n-k) - Binomial(n,n-k+1) ))); # _G. C. Greubel_, Dec 01 2019"
			],
			"keyword": "sign,tabl",
			"offset": "0,1",
			"author": "_Roger L. Bagula_, Feb 17 2009",
			"references": 1,
			"revision": 5,
			"time": "2019-12-01T23:16:57-05:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}