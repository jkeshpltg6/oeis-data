{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A048883",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 48883,
			"data": "1,3,3,9,3,9,9,27,3,9,9,27,9,27,27,81,3,9,9,27,9,27,27,81,9,27,27,81,27,81,81,243,3,9,9,27,9,27,27,81,9,27,27,81,27,81,81,243,9,27,27,81,27,81,81,243,27,81,81,243,81,243,243,729,3,9,9,27,9,27,27,81,9,27,27,81,27,81",
			"name": "a(n) = 3^wt(n), where wt(n) = A000120(n).",
			"comment": [
				"Or, a(n)=number of 1's (\"live\" cells) at stage n of a 2-dimensional cellular automata evolving by the rule: 1 if NE+NW+S=1, else 0.",
				"This is the odd-rule cellular automaton defined by OddRule 013 (see Ekhad-Sloane-Zeilberger \"Odd-Rule Cellular Automata on the Square Grid\" link). - _N. J. A. Sloane_, Feb 25 2015",
				"Or, start with S=[1]; replace S by [S, 3*S]; repeat ad infinitum.",
				"Fixed point of the morphism 1 -\u003e 13, 3 -\u003e 39, 9 -\u003e 9(27), ... = 3^k -\u003e 3^k 3^(k+1), ... starting from a(0) = 1; 1 -\u003e 13 -\u003e 1339 -\u003e = 1339399(27) -\u003e 1339399(27)399(27)9(27)(27)(81) -\u003e ..., . - _Robert G. Wilson v_, Jan 24 2006",
				"Equals row sums of triangle A166453 (the square of Sierpiński's gasket, A047999). - _Gary W. Adamson_, Oct 13 2009",
				"First bisection of A169697=1,5,3,19,3,. a(2n+2)+a(2n+3)=12,12,36,=12*A147610 ? Distribution of terms (in A000244): A011782=1,A000079 for first array, A000079 for second. - _Paul Curtz_, Apr 20 2010",
				"a(A000225(n)) = A000244(n) and a(m) != A000244(n) for m \u003c A000225(n). - _Reinhard Zumkeller_, Nov 14 2011",
				"This sequence pertains to phenotype Punnett square mathematics. Start with X=1. Each hybrid cross involves the equation X:3X. Therefore, the ratio in the first (mono) hybrid cross is X=1:3X=3(1) or 3; or 3:1. When you move up to the next hybridization level, replace the previous cross ratio with X. X now represents 2 numbers-1:3. Therefore, the ratio in the second (di) hybrid cross is X=(1:3):3X=[3(1):3(3)] or (3:9). Put it together and you get 1:3:3:9. Each time you move up a hybridization level, replace the previous ratio with X, and use the same equation-X:3X to get its ratio. - John Michael Feuk, Dec 10 2011"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A048883/b048883.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"David Applegate, \u003ca href=\"/A139250/a139250.anim.html\"\u003eThe movie version\u003c/a\u003e",
				"David Applegate, Omar E. Pol and N. J. A. Sloane, \u003ca href=\"/A000695/a000695_1.pdf\"\u003eThe Toothpick Sequence and Other Sequences from Cellular Automata\u003c/a\u003e, Congressus Numerantium, Vol. 206 (2010), 157-191. [There is a typo in Theorem 6: (13) should read u(n) = 4.3^(wt(n-1)-1) for n \u003e= 2.]",
				"Shalosh B. Ekhad, N. J. A. Sloane, and  Doron Zeilberger, \u003ca href=\"http://arxiv.org/abs/1503.01796\"\u003eA Meta-Algorithm for Creating Fast Algorithms for Counting ON Cells in Odd-Rule Cellular Automata\u003c/a\u003e, arXiv:1503.01796 [math.CO], 2015; see also the \u003ca href=\"http://www.math.rutgers.edu/~zeilberg/mamarim/mamarimhtml/CAcount.html\"\u003eAccompanying Maple Package\u003c/a\u003e.",
				"Shalosh B. Ekhad, N. J. A. Sloane, and  Doron Zeilberger, \u003ca href=\"http://arxiv.org/abs/1503.04249\"\u003eOdd-Rule Cellular Automata on the Square Grid\u003c/a\u003e, arXiv:1503.04249 [math.CO], 2015.",
				"Tanya Khovanova, \u003ca href=\"http://arxiv.org/abs/1410.2193\"\u003eThere are no coincidences\u003c/a\u003e, arXiv preprint 1410.2193 [math.CO], 2014.",
				"Tanya Khovanova and Joshua Xiong, \u003ca href=\"http://arxiv.org/abs/1405.5942\"\u003eNim Fractals\u003c/a\u003e, arXiv:1405.594291 [math.CO] (2014), p. 10. and \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Khovanova/khova6.html\"\u003eJ. Int. Seq. 17 (2014) # 14.7.8\u003c/a\u003e.",
				"T. Pisanski and T. W. Tucker, \u003ca href=\"http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.28.5375\"\u003eGrowth in Repeated Truncations of Maps\u003c/a\u003e, Atti. Sem. Mat. Fis. Univ. Modena, Vol. 49 (2001), 167-176. (\u003ca href=\"http://www.imfm.si/preprinti/PDF/00696.pdf\"\u003epreprint\u003c/a\u003e)",
				"Omar E. Pol, \u003ca href=\"http://www.polprimos.com/imagenespub/polca012.jpg\"\u003eIllustration of initial terms: Fig. 1. Neighbors of the vertices\u003c/a\u003e, \u003ca href=\"http://www.polprimos.com/imagenespub/polca014.jpg\"\u003eFig. 2. Overlapping squares\u003c/a\u003e, \u003ca href=\"http://www.polprimos.com/imagenespub/polca016.jpg\"\u003eFig. 3. One-step bishop\u003c/a\u003e, (Nov 06 2009)",
				"N. J. A. Sloane, \u003ca href=\"/A048883/a048883.png\"\u003eIllustration of a(15) = 81 corresponding to number of ON cells in Odd-rule 013 CA at generation 15\u003c/a\u003e",
				"N. J. A. Sloane, On the No. of ON Cells in Cellular Automata, Video of talk in Doron Zeilberger's Experimental Math Seminar at Rutgers University, Feb. 05 2015: \u003ca href=\"https://vimeo.com/119073818\"\u003ePart 1\u003c/a\u003e, \u003ca href=\"https://vimeo.com/119073819\"\u003ePart 2\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/wiki/Catalog_of_Toothpick_and_CA_Sequences_in_OEIS\"\u003eCatalog of Toothpick and Cellular Automata Sequences in the OEIS\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"http://arxiv.org/abs/1503.01168\"\u003eOn the Number of ON Cells in Cellular Automata\u003c/a\u003e, arXiv:1503.01168 [math.CO], 2015.",
				"R. Stephan, \u003ca href=\"http://arXiv.org/abs/math.CO/0307027\"\u003eDivide-and-conquer generating functions. I. Elementary sequences\u003c/a\u003e, arXiv:math/0307027 [math.CO], 2003.",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e",
				"\u003ca href=\"/index/Fi#FIXEDPOINTS\"\u003eIndex entries for sequences that are fixed points of mappings\u003c/a\u003e"
			],
			"formula": [
				"a(n) = product{k=0..log_2(n), 3^b(n, k)}, b(n, k)=coefficient of 2^k in binary expansion of n(offset 0). - _Paul D. Hanna_",
				"a(n) = 3a(n/2) if n is even, else a(n)=a((n+1)/2).",
				"G.f.: Prod_{k\u003e=0} (1+3*x^(2^k)). The generalization k^A000120 has generating function (1 + kx)(1 + kx^2)(1 + kx^4) ...",
				"a(n+1) = sum(i=0, n, {binomial(n, i) (mod 2)}*sum(j=0, i, {binomial(i, j) (mod 2)})). - _Benoit Cloitre_, Nov 16 2003",
				"1) a(4n),a(4n+1),a(4n+2),a(4n+3)=a(n)*(period 4:repeat 1,3,3,9) ? ,a(0)=1. 2) a(n)=1,3*A147610(n). Note A147582=1,4,4,12,4,12,12,36,=1,4*A147610. - _Paul Curtz_, Apr 20 2010",
				"a(0)=1, a(n) = 3*a(n-A053644(n)) for n\u003e0. - _Joe Slater_, Jan 31 2016",
				"G.f. A(x) satisfies: A(x) = (1 + 3*x) * A(x^2). - _Ilya Gutkovskiy_, Jul 09 2019"
			],
			"example": [
				"From _Omar E. Pol_, Jun 07 2009: (Start)",
				"Triangle begins:",
				"1;",
				"3;",
				"3,9;",
				"3,9,9,27;",
				"3,9,9,27,9,27,27,81;",
				"3,9,9,27,9,27,27,81,9,27,27,81,27,81,81,243;",
				"3,9,9,27,9,27,27,81,9,27,27,81,27,81,81,243,9,27,27,81,27,81,81,243,27,...",
				"Or",
				"1;",
				"3,3;",
				"9,3,9,9;",
				"27,3,9,9,27,9,27,27;",
				"81,3,9,9,27,9,27,27,81,9,27,27,81,27,81,81;",
				"243,3,9,9,27,9,27,27,81,9,27,27,81,27,81,81,243,9,27,27,81,27,81,81,243,27...",
				"(End)"
			],
			"mathematica": [
				"Nest[ Join[#, 3#] \u0026, {1}, 6] (* _Robert G. Wilson v_, Jan 24 2006 and modified Jul 27 2014*)",
				"a[n_] := 3^DigitCount[n, 2, 1]; Array[a, 80, 0] (* _Jean-François Alcover_, Nov 15 2017 *)"
			],
			"program": [
				"(PARI) a(n)=n=binary(n);3^sum(i=1,#n,n[i])",
				"(Haskell)",
				"a048883 = a000244 . a000120  -- _Reinhard Zumkeller_, Nov 14 2011"
			],
			"xref": [
				"For generating functions Prod_{k\u003e=0} (1+a*x^(b^k)) for the following values of (a,b) see: (1,2) A000012 and A000027, (1,3) A039966 and A005836, (1,4) A151666 and A000695, (1,5) A151667 and A033042, (2,2) A001316, (2,3) A151668, (2,4) A151669, (2,5) A151670, (3,2) A048883, (3,3) A117940, (3,4) A151665, (3,5) A151671, (4,2) A102376, (4,3) A151672, (4,4) A151673, (4,5) A151674.",
				"A generalization of A001316. Cf. A102376.",
				"Partial sums give A130665. - _David Applegate_, Jun 11 2009",
				"Cf. A000079, A122018, A166453."
			],
			"keyword": "nonn,nice,easy,hear",
			"offset": "0,2",
			"author": "_John W. Layman_",
			"ext": [
				"Corrected by _Ralf Stephan_, Jun 19 2003",
				"Entry revised by _N. J. A. Sloane_, May 30 2009",
				"Offset changed to 0, Jun 11 2009"
			],
			"references": 53,
			"revision": 113,
			"time": "2021-02-24T02:48:17-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}