{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A333592",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 333592,
			"data": "1,2,14,146,1742,22252,296438,4063866,56884430,808970960,11649069764,169444272692,2485268015414,36707034407396,545386280953262,8144809577111146,122177689609022670,1839933272106181720,27804610617723365072,421476329309967621504,6406685024966332359492",
			"name": "a(n) = Sum_{k = 0..n} binomial(n + k - 1, k)^2.",
			"comment": [
				"Compare with the closed-form evaluation Sum_{k = 0..n} C(n+k-1,k) = C(2*n,n) = A000984(n) (see the first comment in A001700 for a proof).",
				"It is well-known that Sum_{k = 0..n} C(n,k)^2 = C(2*n,n). Here, we consider by analogy Sum_{k = 0..n} C(-n,k)^2, where C(-n,k) = (-1)^k*C(n+k-1,k) for integer n and nonnegative integer k.",
				"The sequence b(n) = C(2*n,n) of central binomial coefficients satisfies the congruences b(n*p^k) = b(n*p^(k-1)) ( mod p^(3*k) ) for all prime p \u003e= 5 and any positive integers n and k -  see Mestrovic. We conjecture that the present sequence also satisfies these congruences. Some examples of the congruences are given below.",
				"More generally, calculation suggests that for positive integer A and integer B, the sequence a(A,B;n) := Sum_{k = 0..A*n} C(B*n+k-1,k)^2 may satisfy the same congruences.",
				"The sequence (a(p) - 2)/(2*p^3) for prime p \u003e= 5 begins [89, 5924, 63652995, 8353899501, 187251503369243, 30724327840061789, 937835335872800013431, ...]. Cf. A034602."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A333592/b333592.txt\"\u003eTable of n, a(n) for n = 0..500\u003c/a\u003e",
				"R. Mestrovic, \u003ca href=\"http://arxiv.org/abs/1111.3057\"\u003eWolstenholme's theorem: Its Generalizations and Extensions in the last hundred and fifty years (1862-2011)\u003c/a\u003e, arXiv:1111.3057 [math.NT], 2011."
			],
			"formula": [
				"a(n) ~ 2^(4*n) / (3*Pi*n). - _Vaclav Kotesovec_, Mar 28 2020"
			],
			"example": [
				"Examples of congruences:",
				"a(11) - a(1) = 169444272692 - 2 = 2*(3^2)*5*7*(11^3)*397*509 == 0 ( mod 11^3 ).",
				"a(2*7) - a(2) = 545386280953262 - 14 = (2^5)*(3^2)*(7^4)*788714021 == 0 ( mod 7^3 ).",
				"a(5^2) - a(5) = 5375188503768783714940459752 - 22252 = (2^2)*(5^6)*(31^2)* 89493252924350197127 == 0 ( mod 5^6 )."
			],
			"maple": [
				"seq( add( binomial(n+k-1,k)^2, k = 0..n ), n = 0..25);"
			],
			"mathematica": [
				"Table[Binomial[2*n-1, n]^2 * HypergeometricPFQ[{1, -n, -n}, {1 - 2*n, 1 - 2*n}, 1], {n, 1, 20}] (* _Vaclav Kotesovec_, Mar 28 2020 *)"
			],
			"program": [
				"(PARI) a(n) = sum(k=0, n, binomial(n+k-1, k)^2); \\\\ _Michel Marcus_, Mar 29 2020."
			],
			"xref": [
				"Cf. A000984, A034602, A333593."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Peter Bala_, Mar 27 2020",
			"references": 2,
			"revision": 23,
			"time": "2021-10-06T14:35:11-04:00",
			"created": "2020-03-30T08:31:35-04:00"
		}
	]
}