{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A071044",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 71044,
			"data": "1,3,2,6,2,6,4,12,2,6,4,12,4,12,8,24,2,6,4,12,4,12,8,24,4,12,8,24,8,24,16,48,2,6,4,12,4,12,8,24,4,12,8,24,8,24,16,48,4,12,8,24,8,24,16,48,8,24,16,48,16,48,32,96,2,6,4,12,4,12,8,24,4,12,8,24,8,24,16,48",
			"name": "Number of ON cells at generation n of 1-D CA defined by Rule 22, starting with a single ON cell.",
			"comment": [
				"Number of 1's in n-th row of triangle in A071029."
			],
			"reference": [
				"S. Wolfram, A New Kind of Science, Wolfram Media, 2002; Chapter 3."
			],
			"link": [
				"Robert Price, \u003ca href=\"/A071044/b071044.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Kari Eloranta, \u003ca href=\"http://dx.doi.org/10.1088/0951-7715/6/6/010\"\u003ePartially permutive cellular automata\u003c/a\u003e, Nonlinearity 6.6 (1993): 1009. (Further information about Rule 22)",
				"Peter Grassberger, \u003ca href=\"http://dx.doi.org/10.1007/BF01033074\"\u003eLong-range effects in an elementary cellular automaton\u003c/a\u003e, Journal of Statistical Physics, 45.1-2 (1986): 27-39. (Further information about Rule 22)",
				"A. J. Macfarlane, \u003ca href=\"http://www.damtp.cam.ac.uk/user/ajm/Papers2016/GFsForCAsOfEvenRuleNo.ps\"\u003eGenerating functions for integer sequences defined by the evolution of cellular automata...\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/A071044/a071044.png\"\u003eIllustration of first 21 generations\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"http://arxiv.org/abs/1503.01168\"\u003eOn the Number of ON Cells in Cellular Automata\u003c/a\u003e, arXiv:1503.01168 [math.CO], 2015.",
				"S. Wolfram, \u003ca href=\"http://dx.doi.org/10.1103/RevModPhys.55.601\"\u003eStatistical mechanics of cellular automata\u003c/a\u003e, Rev. Mod. Phys., 55 (1983), 601--644.",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e"
			],
			"formula": [
				"If the binary expansion of n is b_{r-1} b_{r-2} ... b_2 b_1 b_0, then a(n) = 3^b_0 * Prod_{i=1..r-1} 2^b_i = 2^wt(n) if n is even, or (3/2)*2^wt(n) if n is odd (cf. A000120). - _N. J. A. Sloane_, Aug 09 2014",
				"G.f. = (1+3*x)*Prod_{k \u003e= 1} (1+2*x^(2^k)). - _N. J. A. Sloane_, Aug 09 2014"
			],
			"example": [
				"From _Michael De Vlieger_, Oct 05 2015: (Start)",
				"First 8 rows, replacing \"0\" with \".\" for better visibility of ON cells, total of ON cells in each row to the left of the diagram:",
				"1                  1",
				"3                1 1 1",
				"2              1 . . . 1",
				"6            1 1 1 . 1 1 1",
				"2          1 . . . . . . . 1",
				"6        1 1 1 . . . . . 1 1 1",
				"4      1 . . . 1 . . . 1 . . . 1",
				"12   1 1 1 . 1 1 1 . 1 1 1 . 1 1 1",
				"2  1 . . . . . . . . . . . . . . . 1",
				"(End)"
			],
			"mathematica": [
				"ArrayPlot[CellularAutomaton[22, {{1}, 0}, 20]] (* _N. J. A. Sloane_, Aug 15 2014 *)",
				"Total /@ CellularAutomaton[22, {{1}, 0}, 80] (* _Michael De Vlieger_, Oct 05 2015 *)"
			],
			"xref": [
				"Cf. A071029."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Hans Havermann_, May 26 2002",
			"ext": [
				"Better description from _N. J. A. Sloane_, Aug 15 2014"
			],
			"references": 3,
			"revision": 46,
			"time": "2017-03-16T14:13:11-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}