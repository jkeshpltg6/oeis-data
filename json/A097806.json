{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A097806",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 97806,
			"data": "1,1,1,0,1,1,0,0,1,1,0,0,0,1,1,0,0,0,0,1,1,0,0,0,0,0,1,1,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,1,1",
			"name": "Riordan array (1+x, 1) read by rows.",
			"comment": [
				"Pair sum operator. Columns have g.f. (1+x)*x^k. Row sums are A040000. Diagonal sums are (1,1,1,....). Riordan inverse is (1/(1+x), 1). A097806 = B*A059260^(-1), where B is the binomial matrix.",
				"Triangle T(n,k), 0\u003c=k\u003c=n, read by rows given by [1, -1, 0, 0, 0, 0, 0, ...] DELTA [1, 0, 0, 0, 0, 0, 0, ...] where DELTA is the operator defined in A084938. - _Philippe Deléham_, May 01 2007",
				"Table T(n,k) read by antidiagonals. T(n,1) = 1, T(n,2) = 1, T(n,k) = 0, k \u003e 2. - _Boris Putievskiy_, Jan 17 2013"
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A097806/b097806.txt\"\u003eTable of n, a(n) for n = 0..10010\u003c/a\u003e (Rows 0 \u003c= n \u003c= 140)",
				"Boris Putievskiy, \u003ca href=\"http://arxiv.org/abs/1212.2732\"\u003eTransformations [of] Integer Sequences And Pairing Functions\u003c/a\u003e arXiv:1212.2732 [math.CO], 2012."
			],
			"formula": [
				"T(n, k) = if(n=k or n-k=1, 1, 0).",
				"a(n) = A103451(n+1). - _Philippe Deléham_, Oct 16 2007",
				"From _Boris Putievskiy_, Jan 17 2013: (Start)",
				"a(n) = floor((A002260(n)+2)/(A003056(n)+2)), n \u003e 0.",
				"a(n) = floor((i+2)/(t+2)), n \u003e 0,",
				"where i=n-t*(t+1)/2, t=floor((-1+sqrt(8*n-7))/2). (End)",
				"G.f.: (1+x)/(1-x*y). - _R. J. Mathar_, Aug 11 2015"
			],
			"example": [
				"Rows begin {1}, {1,1}, {0,1,1}, {0,0,1,1}...",
				"From _Boris Putievskiy_, Jan 17 2013: (Start)",
				"The start of the sequence as table:",
				"1..1..0..0..0..0..0...",
				"1..1..0..0..0..0..0...",
				"1..1..0..0..0..0..0...",
				"1..1..0..0..0..0..0...",
				"1..1..0..0..0..0..0...",
				"1..1..0..0..0..0..0...",
				"1..1..0..0..0..0..0...",
				". . .",
				"The start of the sequence as triangle array read by rows:",
				"  1;",
				"  1, 1;",
				"  0, 1, 1;",
				"  0, 0, 1, 1;",
				"  0, 0, 0, 1, 1;",
				"  0, 0, 0, 0, 1, 1;",
				"  0, 0, 0, 0, 0, 1, 1;",
				"  0, 0, 0, 0, 0, 0, 1, 1; . . .",
				"Row number r (r\u003e4) contains (r-2) times '0' and 2 times '1'. (End)"
			],
			"maple": [
				"A097806 := proc(n,k)",
				"    if k =n or k=n-1 then",
				"        1;",
				"    else",
				"        0;",
				"    end if;",
				"end proc: # _R. J. Mathar_, Jun 20 2015"
			],
			"mathematica": [
				"Table[Boole[n \u003c= # \u003c= n+1] \u0026 /@ Range[n+1], {n, 0, 15}] // Flatten (* or *)",
				"Table[Floor[(# +2)/(n+2)] \u0026 /@ Range[n+1], {n, 0, 15}] // Flatten (* _Michael De Vlieger_, Jul 21 2016 *)"
			],
			"program": [
				"(PARI) T(n, k) = if(k==n || k==n-1, 1, 0); \\\\ _G. C. Greubel_, Jul 11 2019",
				"(MAGMA) [k eq n or k eq n-1 select 1 else 0: k in [0..n], n in [0..15]]; // _G. C. Greubel_, Jul 11 2019",
				"(Sage)",
				"def T(n, k):",
				"    if (k==n or k==n-1): return 1",
				"    else: return 0",
				"[[T(n, k) for k in (0..n)] for n in (0..15)] # _G. C. Greubel_, Jul 11 2019"
			],
			"keyword": "easy,nonn,tabl",
			"offset": "0,1",
			"author": "_Paul Barry_, Aug 25 2004",
			"references": 46,
			"revision": 41,
			"time": "2019-07-11T08:23:04-04:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}