{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A137576",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 137576,
			"data": "1,3,5,7,13,11,13,17,17,19,31,23,41,55,29,31,41,61,37,49,41,43,85,47,85,57,53,81,73,59,61,73,73,67,111,71,73,141,151,79,217,83,89,113,89,109,131,145,97,211,101,103,169,107,109,145,113,221,133,193,221,141,301,127",
			"name": "a(n) = A002326(n) * A006694(n) + 1.",
			"comment": [
				"Composite numbers n for which a((n-1)/2)=n are called overpseudoprimes to base 2 (A141232).",
				"Theorem. If p and q are odd primes then the equality a((pq-1)/2)=pq is valid if and only if A002326((p-1)/2)=A002326((q-1)/2). Example: A002326(11) = A002326(44). Since 23 and 89 are primes then a((23*89-1)/2)=23*89.",
				"A generalization: If p_1\u003cp_2\u003c...\u003cp_m are distinct odd primes then a(((p_1*p_2*...*p_m)-1)/2)=p_1*p_2*...*p_m if and only if A002326((p_1-1)/2)= A002326((p_2-1)/2)=...=A002326((p_m-1)/2).",
				"Moreover, if n is an odd squarefree number and a((n-1)/2)=n then also all divisors d of n satisfy a((d-1)/2)=d and d divides 2^d-2. Thus the sequence of such n is a subsequence of A050217."
			],
			"link": [
				"Ray Chandler, \u003ca href=\"/A137576/b137576.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Vladimir Shevelev, \u003ca href=\"http://arXiv.org/abs/0804.3682\"\u003eExact exponent of remainder term of Gelfond's digit theorem in binary case\u003c/a\u003e, arXiv:0804.3682 [math.NT], 2008.",
				"Vladimir Shevelev, \u003ca href=\"https://doi.org/10.4064/aa136-1-7\"\u003eExact exponent in the remainder term of Gelfond's digit theorem in the binary case\u003c/a\u003e, Acta Arithmetica 136 (2009), 91-100."
			],
			"formula": [
				"It can be shown that if p is an odd prime then a((p^k-1)/2)=1+k*phi(p^k).",
				"a(n) = ord(2,2*n+1) * ((Sum_{d|(2n+1)} phi(d)/ord(2,d)) - 1) + 1, where phi = A000010 and ord(2,d) is the multiplicative order of 2 modulo d. - _Jianing Song_, Nov 13 2021"
			],
			"mathematica": [
				"a[n_] := (t = MultiplicativeOrder[2, 2n+1])*DivisorSum[2n+1, EulerPhi[#] / MultiplicativeOrder[2, #]\u0026]-t+1; Table[a[n], {n, 0, 70}] (* _Jean-François Alcover_, Dec 04 2015, adapted from PARI *)"
			],
			"program": [
				"(PARI) a(n)=my(t);sumdiv(2*n+1, d, eulerphi(d)/(t=znorder(Mod(2, d))))*t-t+1 \\\\ _Charles R Greathouse IV_, Feb 20 2013"
			],
			"xref": [
				"Cf. A002326, A006694, A138193, A138217, A138227, A141232, A195468."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Vladimir Shevelev_, Apr 26 2008, Apr 28 2008, May 03 2008, Jun 12 2008",
			"ext": [
				"Edited and extended by _Ray Chandler_, May 08 2008"
			],
			"references": 18,
			"revision": 30,
			"time": "2021-11-14T01:08:15-05:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}