{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A100258",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 100258,
			"data": "1,0,1,-1,0,3,0,-3,0,5,3,0,-30,0,35,0,15,0,-70,0,63,-5,0,105,0,-315,0,231,0,-35,0,315,0,-693,0,429,35,0,-1260,0,6930,0,-12012,0,6435,0,315,0,-4620,0,18018,0,-25740,0,12155,-63,0,3465,0,-30030,0,90090,0,-109395,0,46189",
			"name": "Triangle of coefficients of normalized Legendre polynomials, with increasing exponents.",
			"comment": [
				"For a relation to Jacobi quartic elliptic curves, see the MathOverflow link. For a self-convolution of the polynomials relating them to the Chebyshev and Fibonacci polynomials, see A049310 and A053117. For congruences and connections to other polynomials (Jacobi, Gegenbauer, and Chebyshev) see the Allouche et al. link. For relations to elliptic cohomology and modular forms, see references in Copeland link.- _Tom Copeland_, Feb 04 2016"
			],
			"reference": [
				"M. Abramowitz and I. A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, 1964 (and various reprintings), p. 798."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A100258/b100258.txt\"\u003eRows n=0..100 of triangle, flattened\u003c/a\u003e",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"J. Allouche and G. Skordev, \u003ca href=\"http://dx.doi.org/10.1016/S0012-365X(99)00195-8\"\u003eSchur congruences, Carlitz sequences of polynomials and automaticity\u003c/a\u003e, Discrete Mathematics, Vol. 214, Issue 1-3, 21 March 2000, p. 21-49.",
				"Tom Copeland, \u003ca href=\"https://tcjpn.wordpress.com/2015/10/12/the-elliptic-lie-triad-kdv-and-ricattt-equations-infinigens-and-elliptic-genera/\"\u003eThe Elliptic Lie Triad: Riccati and KdV Equations, Infinigens, and Elliptic Genera\u003c/a\u003e",
				"H. N. Laden, \u003ca href=\"https://drum.lib.umd.edu/handle/1903/16857\"\u003eAn historical, and critical development of the theory of Legendre polynomials before 1900\u003c/a\u003e, Master of Arts Thesis, University of Maryland 1938.",
				"Shi-Mei Ma, \u003ca href=\"http://arxiv.org/abs/1304.6654\"\u003eOn gamma-vectors and the derivatives of the tangent and secant functions\u003c/a\u003e, arXiv:1304.6654 [math.CO], 2013.",
				"MathOverflow, \u003ca href=\"http://mathoverflow.net/questions/82597/geometric-picture-of-invariant-differential-of-an-elliptic-curve\"\u003eGeometric picture of invariant differential of an elliptic curve\u003c/a\u003e, Dec 4 2011."
			],
			"formula": [
				"The n-th normalized Legendre polynomial is generated by 2^(-n-a(n)) (d/dx)^n (x^2-1)^n / n! with a(n) = A005187(n/2) for n even and a(n) = A005187((n-1)/2) for n odd. The non-normalized polynomials have the o.g.f. 1 / sqrt(1 - 2xz + z^2). - _Tom Copeland_, Feb 07 2016",
				"The consecutive nonzero entries in the m-th row are, in order, (c+b)!/(c!(m-b)!(2b-m)!*A048896(m-1)) with sign (-1)^b where c = m/2-1, m/2, m/2+1, ..., (m-1) and b = c+1 if m is even and sign (-1)^c with c = (m-1)/2, (m-1)/2+1, (m-1)/2+2, ..., (m-1) with b = c+1 if m is odd. For the 9th row the 5 consecutive nonzero entries are 315, -4620, 18018, -25740, 12155 given by c = 4,5,6,7,8 and b = 5,6,7,8,9. - _Richard Turk_, Aug 22 2017"
			],
			"example": [
				"Triangle begins:",
				"   1;",
				"   0,   1;",
				"  -1,   0,     3;",
				"   0,  -3,     0,   5;",
				"   3,   0,   -30,   0,   35;",
				"   0,  15,     0, -70,    0,   63;",
				"  -5,   0,   105,   0, -315,    0,    231;",
				"   0, -35,     0, 315,    0, -693,      0, 429;",
				"  35,   0, -1260,   0, 6930,    0, -12012,   0, 6435;",
				"  ..."
			],
			"mathematica": [
				"row[n_] := CoefficientList[ LegendreP[n, x], x]*2^IntegerExponent[n!, 2]; Table[row[n], {n, 0, 10}] // Flatten (* _Jean-François Alcover_, Jan 15 2015 *)"
			],
			"program": [
				"(PARI) a(k,n)=polcoeff(pollegendre(k,x),n)*2^valuation(k!,2)",
				"(Python)",
				"from mpmath import *",
				"mp.dps=20",
				"def a007814(n):",
				"    return 1 + bin(n - 1)[2:].count('1') - bin(n)[2:].count('1')",
				"for n in range(11):",
				"    y=2**sum(a007814(i) for i in range(2, n+1))",
				"    l=chop(taylor(lambda x: legendre(n, x), 0, n))",
				"    print([int(i*y) for i in l]) # _Indranil Ghosh_, Jul 02 2017"
			],
			"xref": [
				"Without zeros: A008316. Row sums are A060818.",
				"Columns (with interleaved zeros and signs) include A001790, A001803, A100259. Diagonals include A001790, A001800, A001801, A001802.",
				"Cf. A049310, A005187, A049600, A053117."
			],
			"keyword": "sign,tabl",
			"offset": "0,6",
			"author": "_Ralf Stephan_, Nov 13 2004",
			"references": 14,
			"revision": 46,
			"time": "2021-03-17T04:20:21-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}