{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A345135",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 345135,
			"data": "1,1,1,2,1,4,6,4,1,8,28,56,70,56,28,8,1,16,120,560,1820,4368,8008,11440,12870,11440,8008,4368,1820,560,120,16,1,32,496,4960,35960,201376,906192,3365856,10518300,28048800,64512240,129024480,225792840,347373600,471435600",
			"name": "Number of ordered rooted binary trees with n leaves and with minimal Sackin tree balance index.",
			"comment": [
				"Ordered rooted binary trees are trees with two descendants per inner node where left and right are distinguished.",
				"The Sackin tree balance index is also known as total external path length.",
				"a(0) = 1 by convention."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A345135/b345135.txt\"\u003eTable of n, a(n) for n = 0..4096\u003c/a\u003e",
				"Mareike Fischer, \u003ca href=\"https://doi.org/10.1007/s00026-021-00539-2\"\u003eExtremal Values of the Sackin Tree Balance Index.\u003c/a\u003e Ann. Comb. 25, 515-541 (2021), Theorem 7."
			],
			"formula": [
				"a(n) = binomial(2^(ceiling(log_2(n))-1),n-2^(ceiling(log_2(n))-1)).",
				"a(n) = binomial(A053644(n),n-A053644(n)).",
				"a(2^n) = 1.",
				"a(2^n-1) = A011782(n).",
				"a(2^n+1) = A000079(n).",
				"From _Alois P. Heinz_, Jun 09 2021: (Start)",
				"max({ a(k) | k = 2^n..2^(n+1) }) = A037293(n).",
				"Sum_{i=2^n..2^(n+1)-1} a(i) = 2^(2^n) - 1 = A051179(n). (End)"
			],
			"example": [
				"a(1) = a(2) = 1 because there are only the trees (o) and (o,o) which get counted. a(3) = 2 because the trees ((o,o),o) and (o,(o,o)) get counted. a(4) = 1 because only the tree ((o,o),(o,o)) is counted. Note that the other possible rooted binary ordered trees with four leaves, namely the different orderings of (((o,o),o),o), are not Sackin minimal. a(5) = 4 because the following trees get counted: (((o,o),o),(o,o)), ((o,(o,o)),(o,o)), ((o,o),((o,o),o)), ((o,o),(o,(o,o)))."
			],
			"maple": [
				"a:= n-\u003e (b-\u003e binomial(b, n-b))(2^ilog2(n)):",
				"seq(a(n), n=0..46);  # _Alois P. Heinz_, Jun 09 2021"
			],
			"mathematica": [
				"a[0] := 1; a[n_] := Module[{k = 2^(BitLength[n] - 1)}, Binomial[k, n - k]];",
				"Table[a[n], {n, 0, 46}]"
			],
			"xref": [
				"Cf. A000079, A000108, A011782, A037293, A051179, A053644, A299037."
			],
			"keyword": "nonn,easy",
			"offset": "0,4",
			"author": "_Mareike Fischer_, Jun 09 2021",
			"references": 2,
			"revision": 35,
			"time": "2021-06-10T11:13:19-04:00",
			"created": "2021-06-09T23:32:13-04:00"
		}
	]
}