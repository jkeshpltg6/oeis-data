{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A264789",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 264789,
			"data": "0,1,1,2,6,1,12,3,5,7,9,2,11,13,13,4,15,6,17,8,9,10,10,3,12,12,12,14,17,14,14,5,17,16,16,7,8,17,18,9,9,10,11,11,20,11,11,4,8,13,17,13,13,13,6,15,15,17,49,15,15,15,8,6,8,18,17,17,17,17,44,8",
			"name": "Number of steps needed to reach 1 or to enter the cycle in the \"sqrt(3)*x+1\" problem.",
			"comment": [
				"The sqrt(3)*x+1 problem is as follows: start with a number x. If x is even, divide it by 2, otherwise multiply it by sqrt(3) and add 1, and then take the integer part.",
				"There are three possible behaviors for such trajectories when n\u003e0:",
				"(i) The trajectory reaches 1 (and enters the \"trivial\" cycle 2-1-2-1-2...).",
				"(ii) Cyclic trajectory. The trajectory becomes periodic and the period does not contain a 1.",
				"(iii) The trajectory is divergent trajectory (I conjecture that this cannot occur).",
				"For many numbers, the element of the trivial cycle is 1, except for the numbers: 3, 6, 12, 19, 21, 24, 29, 33, 37, 38, 42, 43, 48, 49, 51, 55, 57, 58, ... where the elements of the nontrivial cycle  are respectively 6, 3, 3, 38, 74, 3, 58, 19, 74, 76, 74, 37, 3, 98, 29, 6, 37, 33, ..."
			],
			"link": [
				"Michel Lagneau, \u003ca href=\"/A264789/b264789.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"a(3) = 1 because 3 -\u003e 6 -\u003e 3 -\u003e 6 ...",
				"a(7) = 12 because 7 -\u003e 13 -\u003e 23 -\u003e 40 -\u003e 20 -\u003e 10 -\u003e 5 -\u003e 9 -\u003e 16 -\u003e 8 -\u003e 4 -\u003e 2 -\u003e 1 where:",
				"  13 = floor(7*sqrt(3)+1);",
				"  23 = floor(13*sqrt(3)+1);",
				"  40 = floor(23*sqrt(3)+1);",
				"  20 = 40/2;",
				"  10 = 20/2;",
				"   5 = 10/2;",
				"   9 = floor(5*sqrt(3)+1);",
				"  16 = floor(9*sqrt(3)+1);",
				"   8 = 16/2; 4 = 8/2; 2 = 4/2 and 1 = 2/2 is the end of the cycle."
			],
			"maple": [
				"A264789 := proc(n)",
				"    local cyc,x;",
				"    x := n;",
				"    cyc := {x} ;",
				"    for s from 0 do",
				"        if 1 in cyc then",
				"            return s;",
				"        end if;",
				"        if type(x,'even') then",
				"            x := x/2 ;",
				"        else",
				"            x := floor(sqrt(3)*x+1) ;",
				"        end if;",
				"        if x in cyc and s \u003e 0 then",
				"            return s;",
				"        end if;",
				"        cyc := cyc union {x} ;",
				"    end do:",
				"end proc: # _R. J. Mathar_, Nov 27 2015"
			],
			"mathematica": [
				"Table[Length@ NestWhileList[If[EvenQ@ #, #/2, Floor[# Sqrt@ 3 + 1]] \u0026, n, UnsameQ, All] - 2, {n, 0, 72}] (* _Michael De Vlieger_, Nov 25 2015 *)"
			],
			"xref": [
				"Cf. A006577."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Michel Lagneau_, Nov 25 2015",
			"references": 3,
			"revision": 26,
			"time": "2016-12-10T19:40:37-05:00",
			"created": "2015-12-04T23:34:14-05:00"
		}
	]
}