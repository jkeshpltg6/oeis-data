{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A192870",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 192870,
			"data": "0,122,3113,719377,15467683",
			"name": "The maximum integer M such that there are no prime n-tuplets of any possible pattern between M^2 and (M+1)^2, or -1 if no such maximum M exists.",
			"comment": [
				"All terms are conjectural. A prime n-tuplet is defined as the densest permissible prime constellation containing n primes. The term a(2) corresponds to twin primes, a(3) to prime triplets, a(4) to prime quadruplets, etc. Extensive computational evidence suggests that these terms are valid. However, there is no proof that the greatest integer M exists - not even for a subset of values of n. If one could find a constructive existence proof, then Twin Prime Conjecture as well as Legendre's Conjecture would require just a trivial additional step. - Edited by _Hugo Pfoertner_, Sep 15 2021",
				"Note that, for some n, a prime (n+1)-tuple must include a prime n-tuple; e.g., prime quadruplets include prime triples. Thus, if any term is -1,  subsequent terms may be -1, too. - _Franklin T. Adams-Watters_ and _Alexei Kourbatov_, Jul 14 2011",
				"However, for other n, a prime (n+1)-tuple does NOT include a prime n-tuple; e.g. 7-tuples {p, p + 2, p + 6, p + 8, p + 12, p + 18, p + 20} do not contain 6-tuples {p-4, p, p + 2, p + 6, p + 8, p + 12}; see List of all possible patterns of prime k-tuplets by Tony Forbes.",
				"Assuming the Hardy-Littlewood k-tuple conjecture, the average distance between k-tuples grows slower than the distance between consecutive squares. This is an indication (but not a proof) that the maximum integer M in A192870 does exist for all n."
			],
			"link": [
				"Tony Forbes and Norman Luhn, \u003ca href=\"http://www.pzktupel.de/ktpatt.html\"\u003eList of all possible patterns of prime k-tuplets (up to k=50)\u003c/a\u003e",
				"A. Kourbatov, \u003ca href=\"http://arxiv.org/abs/1301.2242\"\u003eMaximal gaps between prime k-tuples: a statistical approach\u003c/a\u003e, arXiv preprint arXiv:1301.2242, 2013 and \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL16/Kourbatov/kourbatov3.html\"\u003eJ. Int. Seq. 16 (2013) #13.5.2\u003c/a\u003e",
				"G. H. Hardy and J. E. Littlewood, \u003ca href=\"https://dx.doi.org/10.1007/BF02403921\"\u003eSome problems of 'Partitio numerorum'; III: on the expression of a number as a sum of primes\u003c/a\u003e, Acta Mathematica, Vol. 44, pp. 1-70, 1923.",
				"Eric W. Weisstein, \u003ca href=\"http://mathworld.wolfram.com/k-TupleConjecture.html\"\u003ek-Tuple Conjecture\u003c/a\u003e"
			],
			"example": [
				"The term a(4)=719377 means that there are no prime quadruplets between 719377^2 and 719378^2, but there are prime quadruplets between m^2 and (m+1)^2 for m \u003e 719377."
			],
			"xref": [
				"Cf. A091592: Numbers n such that there are no twin primes between n^2 and (n+1)^2; A008407: Minimal width of prime n-tuplet. Cf. A020497."
			],
			"keyword": "nonn,hard,more",
			"offset": "1,2",
			"author": "_Alexei Kourbatov_, Jul 11 2011",
			"ext": [
				"First term, 0, added and offset changed by _Zak Seidov_, Jul 11 2011",
				"Clarification regarding patterns in the title added by _Hugo Pfoertner_, Sep 15 2021"
			],
			"references": 2,
			"revision": 52,
			"time": "2021-09-18T01:27:24-04:00",
			"created": "2011-07-15T11:51:25-04:00"
		}
	]
}