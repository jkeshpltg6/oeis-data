{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A103431",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 103431,
			"data": "1,1,2,3,2,3,1,4,2,5,1,6,4,5,7,2,7,5,6,3,8,5,8,4,9,1,10,3,10,7,8,11,4,11,7,10,6,11,2,13,9,10,7,12,1,14,2,15,8,13,4,15,1,16,10,13,9,14,5,16,2,17,12,13,11,14,9,16,5,18,8,17,19,7,18,10,17,6,19,1,20,3,20,14,15,12,17",
			"name": "Subsequence of the Gaussian primes, where only Gaussian primes a+bi with a\u003e0, b\u003e=0 are listed. Ordered by the norm N(a+bi)=a^2+b^2 and the size of the real part, when the norms are equal. a(n) is the real part of the Gaussian prime. Sequence A103432 gives the imaginary parts.",
			"comment": [
				"Definition of Gaussian primes (Pieper, Die komplexen Zahlen, p. 122): 1) i+i, norm N(i+i) = 2. 2) Natural primes p with p = 3 mod 4, norm N(p) = p^2. 3) primes a+bi, a\u003e0, b\u003e0 with a^2 + b^2 = p = 1 mod 4, p natural prime. Norm N(a+bi) = p. b+ai is a different Gaussian prime number, b+ai cannot be factored into a+bi and a unit. 4) All complex numbers from 1) to 3) multiplied by the units -1,i,-i, these are the associated numbers. The sequence contains all the Gaussian primes mentioned in 1) - 3).",
				"Every complex number can be factored completely into the Gaussian prime numbers defined by the sequence, an additional unit as factor can be necessary. This factorization can be used to calculate the complex sigma, as defined by Spira. The elements a(n) are ordered by the size of their norm. If the two different primes a+bi and b+ai have the same norm, they are ordered by the size of the real part of the complex prime number. So a+bi follows b+ai in the sequence, if a \u003e b.",
				"Of course this is not the only possible definition. As primes p = 1 mod 4 can be factored in p = (-i)(a+bi)(b+ai) and the norm N(a+bi) = N(b+ai) = p, these primes a+bi occur much earlier in the sequence than p does in the sequence of natural primes. 4+5i with norm 41 occurs before prime 7 with norm 49."
			],
			"reference": [
				"H. Pieper, \"Die komplexen Zahlen\", Verlag Harri Deutsch, p. 122"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A103431/b103431.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Sven Simon, \u003ca href=\"/A103431/a103431_1.txt\"\u003eList with Gaussian primes (extended) of A103431/A103432\u003c/a\u003e",
				"R. Spira, \u003ca href=\"http://www.jstor.org/stable/2312472\"\u003eThe Complex Sum Of Divisors\u003c/a\u003e, American Mathematical Monthly, 1961 Vol. 68, pp. 120-124.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Table_of_Gaussian_integer_factorizations\"\u003eTable of Gaussian integer factorizations\u003c/a\u003e"
			],
			"maple": [
				"N:= 100: # to get all terms with norm \u003c= N",
				"p1:= select(isprime,[seq(i,i=3..N,4)]):",
				"p2:= select(isprime,[seq(i,i=1..N^2,4)]):",
				"p2:= map(t -\u003e GaussInt:-GIfactors(t)[2][1][1],p2):",
				"p3:= sort( [1+I, op(p1),op(p2)],(a,b) -\u003e Re(a)^2 + Im(a)^2  \u003c Re(b)^2 + Im(b)^2):",
				"g:= proc(z)",
				"    local a,b;",
				"    a:= Re(z); b:= Im(z);",
				"    if b = 0 then z",
				"    else",
				"      a:= abs(a);",
				"      b:= abs(b);",
				"      if a = b then a",
				"      elif a \u003c b then a,b",
				"      else b,a",
				"      fi",
				"    fi",
				"end proc:",
				"map(g, p3); # _Robert Israel_, Feb 23 2016"
			],
			"mathematica": [
				"maxNorm = 500;",
				"norm[z_] := Re[z]^2 + Im[z]^2;",
				"m = Sqrt[maxNorm] // Ceiling;",
				"gp = Select[Table[a + b I, {a, 1, m}, {b, 0, m}] // Flatten, norm[#] \u003c= maxNorm \u0026\u0026 PrimeQ[#, GaussianIntegers -\u003e True]\u0026];",
				"SortBy[gp, norm[#] maxNorm + Abs[Re[#]]\u0026] // Re (* _Jean-François Alcover_, Mar 04 2019 *)"
			],
			"xref": [
				"Cf. A103432, A055025."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Sven Simon_, Feb 05 2005; corrected Feb 20 2005 and again on Aug 06 2006",
			"ext": [
				"Edited (mostly to correct meaning of norm) by _Franklin T. Adams-Watters_, Mar 04 2011",
				"a(48) corrected by _Robert Israel_, Feb 23 2016"
			],
			"references": 15,
			"revision": 43,
			"time": "2019-03-04T12:18:46-05:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}