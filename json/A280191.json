{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A280191",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 280191,
			"data": "0,0,4,5,5,4,5,6,6,7,23,24,120,103,341,326,814,793,1795,1780,3796,3771,7841,7818,15978,15949,32303,32304,65008,64975,130477,130446,261478,261441,523547,523516,1047756,1047715,2096249,2096210,4193314,4193269,8387527,8387496,16776040,16775991",
			"name": "Essential dimension of the spin group Spin_n over an algebraically closed field of characteristic different from 2.",
			"comment": [
				"For n \u003c= 14, due to Markus Rost. For n \u003e 14, see references."
			],
			"reference": [
				"S. Garibaldi, \"Cohomological invariants: exceptional groups and spin groups\", Memoirs of the AMS #937 (2009).",
				"A. Merkurjev, Essential dimension, Quadratic forms-algebra, arithmetic, and geometry (R. Baeza, W.K. Chan, D.W. Hoffmann, and R. Schulze-Pillot, eds.), Contemp. Math., vol. 493, 2009, pp. 299-325."
			],
			"link": [
				"P. Brosnan, Z. Reichstein, and A. Vistoli, \u003ca href=\"http://dx.doi.org/10.4007/annals.2010.171.533\"\u003eEssential dimension, spinor groups, and quadratic forms\u003c/a\u003e, Annals of Math. vol 171 (2010), 533-544.",
				"V. Chernousov and A.S. Merkurjev, \u003ca href=\"http://www.math.ucla.edu/~merkurev/papers/i3.pdf\"\u003eEssential dimension of spinor and Clifford groups\u003c/a\u003e, Algebra \u0026 Number Theory 8 (2014), no. 2, 457-472.",
				"S. Garibaldi and R.M. Guralnick, \u003ca href=\"https://arxiv.org/abs/1601.00590\"\u003eSpinors and essential dimension\u003c/a\u003e, arXiv:1601.00590 [math.GR], 2016.",
				"Alexander S. Merkurjev, \u003ca href=\"https://doi.org/10.1090/bull/1564\"\u003eEssential dimension\u003c/a\u003e, Bull. Amer. Math. Soc., 54 (Oct. 2017), 635-661."
			],
			"example": [
				"a(14) = 7, meaning that Spin_14 has essential dimension 7, reflecting a cohomological invariant of degree 7 constructed using the G2 X G2 semidirect mu_4 subgroup."
			],
			"mathematica": [
				"a[n_] := If[n\u003e14, Which[Mod[n, 2] == 1, 2^((n-1)/2)-n(n-1)/2, Mod[n, 4] == 2, 2^((n-2)/2)-n(n-1)/2, Mod[n, 4] == 0, 2^IntegerExponent[n, 2]-n(n-1)/2 + 2^((n-2)/2)], If[n \u003e= 5, {0, 0, 4, 5, 5, 4, 5, 6, 6, 7}[[n-4]]]];",
				"Table[a[n], {n, 5, 50}] (* _Jean-François Alcover_, Feb 18 2019, from Python *)"
			],
			"program": [
				"(Python)",
				"def a(n):",
				"    if n \u003e 14:",
				"        if n%2 == 1:",
				"            return 2**((n-1)/2) - n*(n-1)/2",
				"        if n%4 == 2:",
				"            return 2**((n-2)/2) - n*(n-1)/2",
				"        if n%4 == 0:",
				"            return 2**((n-2)/2) - n*(n-1)/2 + biggestdivisor(n,2)",
				"    elif n \u003e= 5:",
				"        return [0,0,4,5,5,4,5,6,6,7][n-5]",
				"    return \"Error\"",
				"def biggestdivisor(n,d): # return largest power of d dividing n",
				"    if n%d != 0:",
				"        return 1;",
				"    else:",
				"        return d*biggestdivisor(n/d, d);"
			],
			"xref": [
				"Agrees with sequence A163417 for n \u003e 15 and not divisible by 4. First term of agreement is a(17) = 120."
			],
			"keyword": "nonn",
			"offset": "5,3",
			"author": "_Skip Garibaldi_, Dec 28 2016",
			"ext": [
				"More terms from _Jean-François Alcover_, Mar 12 2019"
			],
			"references": 1,
			"revision": 25,
			"time": "2019-03-12T04:07:20-04:00",
			"created": "2016-12-29T05:27:20-05:00"
		}
	]
}