{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A280831",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 280831,
			"data": "1,1,1,2,1,3,2,1,2,1,1,1,4,2,1,4,5,3,3,1,3,2,3,2,6,5,3,4,4,3,12,6,2,7,5,3,10,4,5,2,7,5,4,5,3,8,2,2,3,4,6,7,8,1,5,2,6,9,6,5,9,9,4,6,1,4,14,5,4,12,3,11,12,1,4,8,6,7,4,6,7",
			"name": "Number of ways to write 8*n+7 as x^2 + y^2 + z^2 + w^2 with x^4 + 1680*y^3*z a square, where x,y,z,w are positive integers.",
			"comment": [
				"Conjecture: Let a and b be nonzero integers with gcd(a,b) squarefree. Then any natural number can be written as x^2 + y^2 + z^2 + w^2 with x,y,z,w nonnegative integers and a*x^4 + b*y^3*z a square, if and only if (a,b) is among the ordered pairs (1,1), (1,15), (1,20), (1,36), (1,60), (1,1680) and (9,260).",
				"If a natural number n is not of the form 4^k*(8m+7) (k,m = 0,1,...), then by the Gauss-Legendre theorem, there are nonnegative integers w,x,y such that n = w^2 + x^2 + y^2 + 0^2 and hence x^4 + 1680*y^3*0 is a square. Thus, the conjecture for (a,b) = (1,1680) has the following equivalent form: a(n) \u003e 0 for all n = 0,1,...",
				"See also A272336 for a similar conjecture.",
				"Concerning the author's 1680-conjecture which states that each n = 0,1,... can be written as x^2 + y^2 + z^2 + w^2 with x,y,z,w nonnegative integers such that x^4 + 1680*y^3*z is a square, Qing-Hu Hou at Tianjin Univ. has verified it for n up to 10^8. The author would like to offer 1680 RMB as the prize for the first complete solution of the 1680-conjecture. - _Zhi-Wei Sun_, Jun 22 2020"
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A280831/b280831.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1604.06723\"\u003eRefining Lagrange's four-square theorem\u003c/a\u003e, arXiv:1604.06723 [math.NT], 2016.",
				"Zhi-Wei Sun, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2016.11.008\"\u003eRefining Lagrange's four-square theorem\u003c/a\u003e, J. Number Theory 175 (2017), 167-190. (Cf. Conjecture 4.10(iv).)"
			],
			"example": [
				"a(0) = 1 since 8*0+7 = 1^2 + 1^2 + 1^2 + 2^2 with 1^4 + 1680*1^3*1 = 41^2.",
				"a(11) = 1 since 8*11 + 7 = 95 = 6^2 + 3^2 + 1^2 + 7^2 with 6^4 + 1680*3^3*1 = 216^2.",
				"a(244) = 1 since 8*244 + 7 = 1959 = 13^2 + 13^2 + 39^2 + 10^2 with 13^4 + 1680*13^3*39 = 11999^2.",
				"a(289) = 1 since 8*289 + 7 = 2319 = 14^2 + 7^2 + 45^2 + 7^2 with 14^4 + 1680*7^3*45 = 5096^2.",
				"a(664) = 1 since 8*664 + 7 = 5319 = 3^2 + 6^2 + 45^2 + 57^2 with 3^4 + 1680*6^3*45 = 4041^2.",
				"a(749) = 1 since 8*749 + 7 = 5999 = 31^2 + 18^2 + 15^2 + 67^2",
				"with 31^4 + 1680*18^3*15 = 12161^2.",
				"a(983) = 1 since 8*983 + 7 = 7871 = 27^2 + 54^2 + 1^2 + 65^2 with 27^4 + 1680*54^3*1 = 16281^2.",
				"a(1228) = 1 since 8*1228 + 7 = 9831 = 35^2 + 10^2 + 91^2 + 15^2 with 35^4 + 1680*10^3*91 = 12425^2.",
				"a(1819) = 1 since 8*1819 + 7 = 14559 = 34^2 + 1^2 + 39^2 + 109^2 with 34^4 + 1680*1^3*39 = 1184^2.",
				"a(2503) = 1 since 8*2503 + 7 = 20031 = 97^2 + 7^2 + 13^2 + 102^2 with 97^4 + 1680*7^3*13 = 9799^2.",
				"a(2506) = 1 since 8*2506 + 7 = 20055 = 47^2 + 6^2 + 77^2 + 109^2 with 47^4 + 1680*6^3*77 = 5729^2."
			],
			"mathematica": [
				"SQ[n_]:=SQ[n]=IntegerQ[Sqrt[n]];",
				"Do[r=0;Do[If[SQ[8n+7-x^2-y^2-z^2]\u0026\u0026SQ[x^4+1680y^3*z],r=r+1],{x,1,Sqrt[8n+6]},{y,1,Sqrt[8n+6-x^2]},{z,1,Sqrt[8n+6-x^2-y^2]}];Print[n,\" \",r];Continue,{n,0,80}]"
			],
			"xref": [
				"Cf. A000118, A000290, A000578, A000583, A272336."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_Zhi-Wei Sun_, Jan 08 2017",
			"references": 2,
			"revision": 19,
			"time": "2020-07-14T23:26:54-04:00",
			"created": "2017-01-09T04:18:33-05:00"
		}
	]
}