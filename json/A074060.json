{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A074060",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 74060,
			"data": "1,1,1,1,5,1,1,16,16,1,1,42,127,42,1,1,99,715,715,99,1,1,219,3292,7723,3292,219,1,1,466,13333,63173,63173,13333,466,1,1,968,49556,429594,861235,429594,49556,968,1,1,1981,173570,2567940,9300303,9300303,2567940,173570,1981,1",
			"name": "Graded dimension of the cohomology ring of the moduli space of n-pointed curves of genus 0 satisfying the associativity equations of physics (also known as the WDVV equations).",
			"comment": [
				"Combinatorial interpretations of Lagrange inversion (A134685) and the 2-Stirling numbers of the first kind (A049444 and A143491) provide a combinatorial construction for A074060 (see first Copeland link). For relations of A074060 to other arrays see second Copeland link page 19. - _Tom Copeland_, Sep 28 2008",
				"These Poincare polynomials for the compactified moduli space of rational curves are presented on p. 5 of Lando and Zvonkin as well as those for the non-compactified Poincare polynomials of A049444 in factorial form. - _Tom Copeland_, Jun 13 2021"
			],
			"link": [
				"Tom Copeland, \u003ca href=\"http://tcjpn.wordpress.com/2008/09/28/combinatorics-of-oeis-a074060/\"\u003eCombinatorics of OEIS-A074060\u003c/a\u003e, Posted Sept. 2008.",
				"Tom Copeland, \u003ca href=\"http://tcjpn.wordpress.com/2008/06/12/mathemagical-forests/\"\u003eMathemagical Forests v2\u003c/a\u003e, Posted June 2008.",
				"S. Keel, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9947-1992-1034665-0\"\u003eIntersection theory of moduli space of stable n-pointed curves of genus zero\u003c/a\u003e, Trans. Amer. Math. Soc. 330 (1992), 545-574.",
				"M. Kontsevich and Y. Manin, \u003ca href=\"http://dx.doi.org/10.1007/s002220050055\"\u003eQuantum cohomology of a product\u003c/a\u003e, (with Appendix by R. Kaufmann), Inv. Math. 124, f. 1-3 (1996) 313-339.",
				"M. Kontsevich and Y. Manin, \u003ca href=\"http://arxiv.org/abs/q-alg/9502009\"\u003eQuantum cohomology of a product\u003c/a\u003e, arXiv:q-alg/9502009, 1995.",
				"S. Lando and A. Zvonkin, \u003ca href=\"http://dx.doi.org/10.1007/978-3-540-38361-1\"\u003eGraphs on surfaces and their applications\u003c/a\u003e, Encyclopaedia of Mathematical Sciences, 141, Springer, 2004.",
				"Y. Manin, \u003ca href=\"http://arxiv.org/abs/alg-geom/9407005\"\u003eGenerating functions in algebraic geometry and sums over trees\u003c/a\u003e, arXiv:alg-geom/9407005, 1994. - _Tom Copeland_, Dec 10 2011",
				"M. A. Readdy, \u003ca href=\"http://www.ms.uky.edu/~readdy/Papers/pre_WDVV.pdf\"\u003eThe pre-WDVV ring of physics and its topology\u003c/a\u003e, preprint, 2002."
			],
			"formula": [
				"Define offset to be 0 and P(n,t) = (-1)^n Sum_{j=0..n-2} a(n-2,j)*t^j with P(1,t) = -1 and P(0,t) = 1, then H(x,t) = -1 + exp(P(.,t)*x) is the compositional inverse in x about 0 of G(x,t) in A049444. H(x,0) = exp(-x) - 1, H(x,1) = -1 + exp( 2 + W( -exp(-2) * (2-x) ) ) and H(x,2) = 1 - (1+2*x)^(1/2), where W is a branch of the Lambert function such that W(-2*exp(-2)) = -2. - _Tom Copeland_, Feb 17 2008",
				"Let offset=0 and g(x,t) = (1-t)/((1+x)^(t-1)-t), then the n-th row polynomial of the table is given by [(g(x,t)*D_x)^(n+1)]x with the derivative evaluated at x=0. - _Tom Copeland_, Jun 01 2008",
				"With the notation in Copeland's comments, dH(x,t)/dx = -g(H(x,t),t). - _Tom Copeland_, Sep 01 2011",
				"The term linear in x of [x*g(d/dx,t)]^n 1 gives the n-th row polynomial with offset 1. (See A134685.) - _Tom Copeland_, Oct 21 2011"
			],
			"example": [
				"Viewed as a triangular array, the values are",
				"  1;",
				"  1,   1;",
				"  1,   5,   1;",
				"  1,  16,  16,   1;",
				"  1,  42, 127,  42,   1; ..."
			],
			"maple": [
				"DA:=((1+t)*A(u,t)+u)/(1-t*A(u,t)): F:=0: for k from 1 to 10 do F:=map(simplify,int(series(subs(A(u,t)=F,DA),u,k),u)); od: # Eric Rains, Apr 02 2005"
			],
			"mathematica": [
				"DA = ((1+t) A[u, t] + u)/(1 - t A[u, t]); F = 0;",
				"Do[F = Integrate[Series[DA /. A[u, t] -\u003e F, {u, 0, k}], u], {k, 1, 10}];",
				"(cc = CoefficientList[#, t]; cc Denominator[cc[[1]]])\u0026 /@ Drop[ CoefficientList[F, u], 2] // Flatten (* _Jean-François Alcover_, Oct 15 2019, after Eric Rains *)"
			],
			"xref": [
				"Cf. A074059. 2nd diagonal is A002662."
			],
			"keyword": "nonn,tabl",
			"offset": "3,5",
			"author": "_Margaret A. Readdy_, Aug 16 2002",
			"ext": [
				"More terms from Eric Rains, Apr 02 2005"
			],
			"references": 5,
			"revision": 58,
			"time": "2021-06-27T16:23:12-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}