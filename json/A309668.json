{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A309668",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 309668,
			"data": "1,2,5,4,3,10,9,8,7,6,11,21,19,18,17,16,15,14,13,12,24,23,22,42,20,39,37,36,35,34,33,32,31,30,29,28,27,26,52,25,49,48,47,46,45,44,43,85,41,40,80,78,38,75,74,73,71,70,69,68,67,66,65,64,63,62,61",
			"name": "a(n) is the least positive number of the form floor(2^k/n) for some k \u003e= 0 not yet in the sequence.",
			"comment": [
				"The sequence is well defined as for any n \u003e 0, there are infinitely many positive numbers of the form floor(2^k/n) with k \u003e= 0.",
				"The sequence is a permutation of the natural numbers, with inverse A309734:",
				"- for any m \u003e 0, floor(2^k/A300475(m)) = m for some k,",
				"- also, for any u \u003e 0, floor(2^(k-u)/(A300475(m)*2^u)) = m,",
				"- so the set S_m = { v such that floor(2^k/v) = m for some k \u003e= 0 } is infinite",
				"- and eventually a(n) = m for some n in S_m, QED."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A309668/b309668.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A309668/a309668.png\"\u003eColored representation of the first 2^16 terms\u003c/a\u003e (where the color is function of the parity of A070939(n) - A070939(a(n)))",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"example": [
				"The first terms, alongside the binary representations of a(n) and of 1/n (with that of a(n) in parentheses), are:",
				"  --  ----  ---------  ---------------------",
				"   1     1          1  (1).00000000000000...",
				"   2     2         10  0.(10)000000000000...",
				"   3     5        101  0.0(101)0101010101...",
				"   4     4        100  0.0(100)0000000000...",
				"   5     3         11  0.00(11)0011001100...",
				"   6    10       1010  0.00(1010)10101010...",
				"   7     9       1001  0.00(1001)00100100...",
				"   8     8       1000  0.00(1000)00000000...",
				"   9     7        111  0.000(111)00011100...",
				"  10     6        110  0.000(110)01100110...",
				"  11    11       1011  0.000(1011)1010001...",
				"  12    21      10101  0.000(10101)010101...",
				"  13    19      10011  0.000(10011)101100...",
				"  14    18      10010  0.000(10010)010010...",
				"  15    17      10001  0.000(10001)000100..."
			],
			"program": [
				"(PARI) s=1; for (n=1, 67, q=1/n; while (bittest(s, f=floor(q)), q*=2); print1 (f \", \"); s+=2^f)"
			],
			"xref": [
				"Cf. A070939, A300475, A309734 (inverse)."
			],
			"keyword": "nonn,look,base",
			"offset": "1,2",
			"author": "_Rémy Sigrist_, Aug 11 2019",
			"references": 2,
			"revision": 17,
			"time": "2019-08-16T15:14:41-04:00",
			"created": "2019-08-16T00:43:55-04:00"
		}
	]
}