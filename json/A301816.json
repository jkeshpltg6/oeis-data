{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A301816",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 301816,
			"data": "2,7,5,4,3,4,7,2,4,5,6,3,9,2,0,0,7,9,9,5,5,2,8,7,8,7,7,7,9,7,8,0,6,8,3,5,7,9,8,7,0,2,3,2,3,8,8,6,3,0,7,4,8,7,3,7,3,3,2,1,1,4,7,5,1,3,3,0,6,3,4,4,1,7,3,0,6,4,6,8,8,2,2,3,5,9,2",
			"name": "Decimal expansion of the real Stieltjes gamma function at x = 1/2.",
			"comment": [
				"Define the real Stieltjes gamma function (this is not a standard notion) as Sti(x) = -2*Pi*I(x+1)/(x+1) where I(x) = Integral_{-infinity..+infinity} log(1/2+i*z)^x/(exp(-Pi*z) + exp(Pi*z))^2 dz and i is the imaginary unit. We look here at the real part of Sti(x)."
			],
			"link": [
				"Iaroslav V. Blagouchine, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2014.08.009\"\u003eA theorem for the closed-form evaluation of the first generalized Stieltjes constant at rational arguments and some related summations\u003c/a\u003e, Journal of Number Theory, vol. 148, pp. 537-592 and vol. 151, pp. 276-277, 2015. \u003ca href=\"http://arxiv.org/abs/1401.3724\"\u003earXiv version\u003c/a\u003e, arXiv:1401.3724 [math.NT], 2014.",
				"Peter Luschny, \u003ca href=\"/A301816/a301816_1.pdf\"\u003eIllustration of the real Stieltjes gamma function.\u003c/a\u003e"
			],
			"formula": [
				"c = -Re((4/3)*Pi*Integral_{-oo..oo} log(1/2+i*z)^(3/2)/(exp(-Pi*z)+exp(Pi*z))^2 dz)."
			],
			"example": [
				"0.2754347245639200799552878777978068357987023238863074873733211475133063441..."
			],
			"maple": [
				"Sti := x -\u003e (-4*Pi/(x + 1))*int(log(1/2 + I*z)^(x + 1)/(exp(-Pi*z) + exp(Pi*z))^2, z=0..64): Sti(1/2): Re(evalf(%, 100)); # Note that this is an approximation which needs a larger domain of integration and higher precision if used for more values than are in the Data section."
			],
			"xref": [
				"Sti(0) = A001620 (Euler's constant gamma) (cf. A262235/A075266),",
				"Sti(1/2) = A301816,",
				"Sti(1) = A082633 (Stieltjes constant gamma_1) (cf. A262382/A262383),",
				"Sti(3/2) = A301817,",
				"Sti(2) = A086279 (Stieltjes constant gamma_2) (cf. A262384/A262385),",
				"Sti(3) = A086280 (Stieltjes constant gamma_3) (cf. A262386/A262387),",
				"Sti(4) = A086281, Sti(5) = A086282, Sti(6) = A183141, Sti(7) = A183167,",
				"Sti(8) = A183206, Sti(9) = A184853, Sti(10) = A184854."
			],
			"keyword": "nonn,cons",
			"offset": "0,1",
			"author": "_Peter Luschny_, Apr 09 2018",
			"references": 8,
			"revision": 28,
			"time": "2018-04-12T04:57:00-04:00",
			"created": "2018-04-09T13:22:30-04:00"
		}
	]
}