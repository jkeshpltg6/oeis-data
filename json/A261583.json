{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A261583",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 261583,
			"data": "1,755,4648,1335,1096,14708,5964,636,1063,13019,9808,2776,2580,2797,6421,1573,2432,4790,862,1855,566,2145,18554,35634,5264,1293,39402,1445,2397,17930,586,2526,24571,18403,5480,366,5159,9710,179,4469,6757,7866,263,1701,2941,477,5032,10705,3494,8597,953,11954,2586,689,9456,1309,8651,12538,4106,13762",
			"name": "Least positive integer k such that prime(prime(prime(k)))+ prime(prime(prime(k*n))) = 2*prime(prime(p)) for some prime p.",
			"comment": [
				"Conjecture: (i) Any positive rational number r can be written as m/n, where m and n are positive integers such that (prime(prime(prime(m))) + prime(prime(prime(n))))/2 = prime(prime(p)) for some prime p.",
				"(ii) Let p(1,n) = prime(n), and p(m+1,n) = p(m,prime(n)) for m,n = 1,2,3,.... Then, for any integers m \u003e 0 and k \u003e 2, the sequence p(m,n) (n = 1,2,3,...) contains infinitely many nontrivial k-term arithmetic progressions.",
				"(iii) Let m be any positive integer. Then the sequence p(m,n)^(1/n) (n = 1,2,3,...) is strictly decreasing. Also, for any relatively prime integers q \u003e 0 and r, there are infinitely many n \u003e 0 such that p(m,n) == r (mod q).",
				"Note that part (ii) of the conjecture extends the Green-Tao theorem and the third part of the conjecture in A261462. Also, part (iii) in the case m = 1 reduces to the Firoozbakht conjecture on primes and Dirichlet's theorem on primes in arithmetic progressions."
			],
			"reference": [
				"Zhi-Wei Sun, Problems on combinatorial properties of primes, in: M. Kaneko, S. Kanemitsu and J. Liu (eds.), Number Theory: Plowing and Starring through High Wave Forms, Proc. 7th China-Japan Seminar (Fukuoka, Oct. 28 - Nov. 1, 2013), Ser. Number Theory Appl., Vol. 11, World Sci., Singapore, 2015, pp. 169-187."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A261583/b261583.txt\"\u003eTable of n, a(n) for n = 1..200\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"/A261583/a261583.txt\"\u003eChecking part (i) of the conjecture for r = a/b with a,b = 1..100\u003c/a\u003e",
				"B. Green and T. Tao, \u003ca href=\"http://dx.doi.org/10.4007/annals.2008.167.481 \"\u003eThe primes contain arbitrary long arithmetic progressions\u003c/a\u003e, Annals of Math. 167(2008), 481-547.",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1402.6641\"\u003eProblems on combinatorial properties of primes\u003c/a\u003e, arXiv:1402.6641 [math.NT], 2014."
			],
			"example": [
				"a(2) = 755 since prime(prime(prime(755))) + prime(prime(prime(755*2))) = prime(prime(5741)) + prime(prime(12641)) = prime(56611) + prime(135671) = 700897 + 1808581 = 2*1254739 = 2*prime(96797) = 2*prime(prime(9319)) with 9319 prime."
			],
			"mathematica": [
				"f[n_]:=Prime[Prime[Prime[n]]]",
				"PQ[p_]:=PrimeQ[p]\u0026\u0026PrimeQ[PrimePi[p]]\u0026\u0026PrimeQ[PrimePi[PrimePi[p]]]",
				"Do[k=0;Label[bb];k=k+1;If[PQ[(f[k]+f[k*n])/2],Goto[aa],Goto[bb]];Label[aa];Print[n,\" \", k];Continue,{n,1,60}]"
			],
			"xref": [
				"Cf. A000040, A006450, A261437, A261462."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Zhi-Wei Sun_, Aug 25 2015",
			"references": 1,
			"revision": 9,
			"time": "2015-08-25T05:45:59-04:00",
			"created": "2015-08-25T05:45:59-04:00"
		}
	]
}