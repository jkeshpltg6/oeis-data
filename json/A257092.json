{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A257092",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 257092,
			"data": "0,1,1,2,0,2,3,4,4,3,4,5,0,5,4,5,2,6,6,2,5,6,3,1,0,1,3,6,7,8,8,8,8,8,8,7,8,9,3,1,0,1,3,9,8,9,6,10,2,6,6,2,10,6,9,10,7,5,11,5,0,5,11,5,7,10,11,12,12,4,12,4,4,12,4,12,12,11,12,13,7,13,3,13,0,13,3,13,7,13,12,13,10,14,14,10,2,14,14,2,10,14,14,10,13,14,11,9,7,9,11,1,0,1,11,9,7,9,11,14",
			"name": "Square array read by antidiagonals: Nimsum function for \"Take-or-Break\" Nim where a legal move is defined as: 1) Remove a nonzero number of counters from any pile up to the size of the selected pile  OR 2) Split any pile of size greater than one into two nonzero piles (removing no counters from the board).",
			"comment": [
				"Observe that many \"safe\" three-pile positions in Nim (1-2-3, 1-4-5, 2-4-6, etc.) consist of one pile whose size is the sum of the sizes of the other two.  The \"Break\" move is designed to trivially defeat these positions by breaking the large pile into copies of the other two. This leaves a clear winning position where every pile has a \"twin\".",
				"Like the standard Nimsum function defined in A003987, this relation constitutes an Abelian group over nonnegative integers where every element is its own inverse."
			],
			"formula": [
				"NSum(x,y) = Flip(Flip(x) XOR Flip(y))",
				"Where XOR is the bitwise exclusive OR characteristic of A003987.",
				"Flip(n) = 0    if n == 0.",
				"        = n+1  if n is odd.",
				"        = n-1  if n is even."
			],
			"example": [
				"The square table defining the relation begins:",
				"0  1  2  3  4  5  6  7  8  9   ...",
				"1  0  4  5  2  3  8  9  6  7   ...",
				"2  4  0  6  1  8  3 10  5 12   ...",
				"3  5  6  0  8  1  2 11  4 13   ...",
				"4  2  1  8  0  6  5 12  3 10   ...",
				"5  3  8  1  6  0  4 13  2 11   ...",
				"6  8  3  2  5  4  0 14  1 16   ...",
				"7  9 10 11 12 13 14  0 16  1   ...",
				"8  6  5  4  3  2  1 16  0 14   ...",
				"9  7 12 13 10 11 16  1 14  0   ...",
				".  .  .  .  .  .  .  .  .  .",
				"Reading from the table, 1-2-4, 1-3-5 and 2-3-6 are safe positions in Take-or-Break Nim."
			],
			"program": [
				"(PARI) flip(x) = if (x==0, 0, if (x % 2, x+1, x-1));",
				"tabl(nn) = {for (n=0, nn, for (k=0, nn, print1(flip(bitxor(flip(n), flip(k))), \", \");); print(););} \\\\ _Michel Marcus_, Apr 23 2015"
			],
			"xref": [
				"Cf. A003987."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_Patrick McKinley_, Apr 19 2015",
			"references": 0,
			"revision": 26,
			"time": "2015-08-10T17:39:08-04:00",
			"created": "2015-04-23T13:42:05-04:00"
		}
	]
}