{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A125173",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 125173,
			"data": "0,1,1,2,2,2,2,3,2,3,3,3,3,3,3,4,3,3,3,4,3,4,4,4,4,4,3,4,4,4,4,4,4,4,4,4,4,4,4,5,4,4,4,5,4,5,4,5,4,5,4,5,5,4,4,5,4,5,5,5,5,5,4,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,4,5,5,5,5,5,5,6,5,5,5,5,5,5,5,5,5,5,5,6,5,5,5,6",
			"name": "Minimum number of \"k-splits\" required to transform {n} to {1}.",
			"comment": [
				"A \"k-split\" is a transformation T(k) of a set of positive integers obtained by replacing the maximum element m of the set by k and m-2k, where 1\u003c=k\u003c=m/2, unless m = 2k in which case m is simply replaced by k.",
				"Examples: T(4)({1,2,12}) = {1,2,4}, T(5)({1,2,12}) = {1,2,5}, T(6)({1,2,12}) = {1,2,6}.",
				"Is this the same as the sequence of the minimum number of \"d-swaps\" required to reverse a word of length n, which was introduced by D. E. Knuth in Problem 11264 in the American Mathematical Monthly?",
				"Almost, see A241216! - _Joerg Arndt_, Apr 17 2014"
			],
			"link": [
				"Joerg Arndt and Alois P. Heinz, \u003ca href=\"/A125173/b125173.txt\"\u003eTable of n, a(n) for n = 1..200\u003c/a\u003e (first 162 terms from Joerg Arndt)",
				"Donald E. Knuth, \u003ca href=\"https://www.jstor.org/stable/27642122\"\u003eProblem 11264\u003c/a\u003e, Amer. Math. Monthly, vol.114, no.1, p.77, (2007).",
				"Richard Stong, \u003ca href=\"https://www.jstor.org/stable/40391081\"\u003eReversal by Swaps (Solution to Problem 11264)\u003c/a\u003e, Amer. Math. Monthly, vol.116, no.3, p.277-278, (March 2009)."
			],
			"example": [
				"a(9) = 2, as shown by the sequence of 2 k-splits:",
				"  T(3)({9}) = {3}, followed by T(1)({3}) = {1}.",
				"a(44) \u003c= 5, as shown by the 5 k-splits:",
				"  T(15)({44}) = {14,15}, T(7)({14,15}) = {1,7,14}, T(7)({1,7,14}) = {1,7}, T(3)({1,7}) = {1,3} and finally T(1)({1,3}) = {1}.",
				"Exhaustive calculation shows that no sequence of fewer k-splits will suffice for {44}, so a(44) = 5."
			],
			"maple": [
				"b:= proc(s) option remember; local m; m:= max(s[]);",
				"      `if`(m=1, 0, 1 +min(seq(b((s union {k, m-2*k})",
				"       minus {m, 0}), k=1..m/2)))",
				"    end:",
				"a:= n-\u003e b({n}):",
				"seq(a(n), n=1..50);  # _Alois P. Heinz_, Apr 12 2014"
			],
			"mathematica": [
				"b[s_List] := b[s] = Module[{m = Max[s]}, If[m == 1, 0, 1 + Min[Table[b[s ~Union~ {k, m-2k} ~Complement~ {m, 0}], {k, 1, m/2}]]]];",
				"a[n_] := b[{n}];",
				"Array[a, 50] (* _Jean-François Alcover_, Nov 05 2020, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(Sage)",
				"def split(k, S):",
				"    m = max( S )",
				"    S = S.difference( [m] )",
				"    p = m - 2 * k;",
				"#    if p \u003c 0:  return(\"invalid split\")",
				"    S = S.union(Set([k]))",
				"    if p!=0:  S = S.union(Set([p]))",
				"    return S",
				"@CachedFunction",
				"def min_split(S):",
				"    m = max( S )",
				"    if m \u003c= 2:  return  int(m \u003e 1)",
				"    ct = 0",
				"    mct = 999",
				"    k = 1",
				"    while k \u003c= m/2:",
				"        ct = 1 + min_split( split(k,S) )",
				"        if ct \u003c mct:  mct = ct",
				"        k += 1",
				"    return mct",
				"def a(n):  return min_split( Set( [n] ) )",
				"for n in [1..1100]: print(a(n))",
				"# _Joerg Arndt_, Apr 12 2014"
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_John W. Layman_, Jan 12 2007",
			"ext": [
				"Terms recomputed and terms a(58) and beyond added by _Joerg Arndt_, Apr 12 2014"
			],
			"references": 3,
			"revision": 52,
			"time": "2020-11-05T15:02:25-05:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}