{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A124768",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 124768,
			"data": "0,1,1,2,1,2,1,3,1,2,2,3,1,2,2,4,1,2,2,3,1,3,2,4,1,2,2,3,2,3,3,5,1,2,2,3,2,3,2,4,1,2,3,4,2,3,3,5,1,2,2,3,1,3,2,4,2,3,3,4,3,4,4,6,1,2,2,3,2,3,2,4,1,3,3,4,2,3,3,5,1,2,2,3,2,4,3,5,2,3,3,4,3,4,4,6,1,2,2,3,2,3,2,4,1",
			"name": "Number of strictly increasing runs for compositions in standard order.",
			"comment": [
				"The standard order of compositions is given by A066099.",
				"A composition of n is a finite sequence of positive integers summing to n. The k-th composition in standard order (row k of A066099) is obtained by taking the set of positions of 1's in the reversed binary expansion of k, prepending 0, taking first differences, and reversing again. a(n) is the number of maximal strictly increasing runs in this composition. Alternatively, a(n) is one plus the number of weak descents in the same composition. For example, the strictly increasing runs of the 1234567th composition are ((3),(2),(1,2),(2),(1,2,5),(1),(1),(1)), so a(1234567) = 8. The 7 weak descents together with the strict ascents are: 3 \u003e= 2 \u003e= 1 \u003c 2 \u003e= 2 \u003e= 1 \u003c 2 \u003c 5 \u003e= 1 \u003e= 1 \u003e= 1. - _Gus Wiseman_, Apr 08 2020"
			],
			"formula": [
				"a(0) = 0, a(n) = A124763(n) + 1 for n \u003e 0."
			],
			"example": [
				"Composition number 11 is 2,1,1; the strictly increasing runs are 2; 1; 1; so a(11) = 3.",
				"The table starts:",
				"  0",
				"  1",
				"  1 2",
				"  1 2 1 3",
				"  1 2 2 3 1 2 2 4",
				"  1 2 2 3 1 3 2 4 1 2 2 3 2 3 3 5",
				"  1 2 2 3 2 3 2 4 1 2 3 4 2 3 3 5 1 2 2 3 1 3 2 4 2 3 3 4 3 4 4 6"
			],
			"mathematica": [
				"stc[n_]:=Differences[Prepend[Join@@Position[Reverse[IntegerDigits[n,2]],1],0]]//Reverse;",
				"Table[Length[Split[stc[n],Less]],{n,0,100}] (* _Gus Wiseman_, Apr 08 2020 *)"
			],
			"xref": [
				"Cf. A066099, A124763, A011782 (row lengths).",
				"Compositions of n with k weak descents are A333213.",
				"All of the following pertain to compositions in standard order (A066099):",
				"- Length is A000120.",
				"- Partial sums from the right are A048793.",
				"- Sum is A070939.",
				"- Weakly decreasing compositions are A114994.",
				"- Adjacent equal pairs are counted by A124762.",
				"- Weakly decreasing runs are counted by A124765.",
				"- Weakly increasing runs are counted by A124766.",
				"- Equal runs are counted by A124767.",
				"- Strictly increasing runs are counted by A124768 (this sequence).",
				"- Strictly decreasing runs are counted by A124769.",
				"- Weakly increasing compositions are A225620.",
				"- Reverse is A228351 (triangle).",
				"- Strict compositions are A233564.",
				"- Initial intervals are A246534.",
				"- Constant compositions are A272919.",
				"- Normal compositions are A333217.",
				"- Permutations are A333218.",
				"- Heinz number is A333219.",
				"- Strictly decreasing compositions are A333255.",
				"- Strictly increasing compositions are A333256.",
				"- Anti-runs are A333489.",
				"Cf. A029931, A124760, A124761, A124764, A238343, A333220."
			],
			"keyword": "easy,nonn,tabf",
			"offset": "0,4",
			"author": "_Franklin T. Adams-Watters_, Nov 06 2006",
			"references": 22,
			"revision": 9,
			"time": "2020-04-09T00:55:44-04:00",
			"created": "2006-12-06T03:00:00-05:00"
		}
	]
}