{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A090884",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 90884,
			"data": "1,1,2,1,9,2,125,1,4,9,2401,2,161051,125,18,1,4826809,4,410338673,9,250,2401,16983563041,2,81,161051,8,125,1801152661463,18,420707233300201,1,4802,4826809,1125,4,25408476896404831,410338673,322102,9",
			"name": "There exists an isomorphism from the positive rationals under multiplication to Z[x] under addition, defined by f(q) = e1 + (e2)x + (e3)(x^2) +...+ (ek)(x^(k-1)) + ... (where e_i is the exponent of the i-th prime in q's prime factorization) The a(n) above are calculated by a(n) = f^(-1)[d/dx f(n)] (In other words: differentiate n's image in Z[x] and return to Q).",
			"comment": [
				"a(n) is the derivative of n via transport of structure from polynomials.",
				"With primes noted p_0 = 2, p_1 = 3, etc., let f be the function that maps n = Product_{i=0..d} p_i^e_i to P = Sum_{i=0..d} e_i*X^i; and let g be the inverse function of f. a(n) is by definition g(P') = g((f(n))'). - _Luc Rousseau_, Aug 06 2018"
			],
			"reference": [
				"Joseph J. Rotman, The Theory of Groups: An Introduction, 2nd ed. Boston: Allyn and Bacon, Inc. 1973. Page 9, problem 1.26."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A090884/b090884.txt\"\u003eTable of n, a(n) for n = 1..500\u003c/a\u003e",
				"Sam Alexander, \u003ca href=\"http://tinyurl.com/yzjw\"\u003ePost to sci.math\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Transport_of_structure\"\u003eTransport of structure\u003c/a\u003e"
			],
			"formula": [
				"Completely multiplicative with a(2) = 1, a(prime(i+1)) = prime(i)^i for i \u003e 0. - _Andrew Howroyd_, Jul 31 2018"
			],
			"example": [
				"504 = 2^3 * 3^2 * 7 is mapped to polynomial 3+2X+X^3, whose derivative is 2+3X^2, which is mapped to 2^2 * 5^3 = 500. Then, a(504) = 500. - _Luc Rousseau_, Aug 06 2018"
			],
			"program": [
				"(PARI) a(n)={my(f=factor(n)); prod(i=1, #f~, my([p,e]=f[i,]); if(p==2, 1, precprime(p-1)^(e*primepi(p-1))))} \\\\ _Andrew Howroyd_, Jul 31 2018"
			],
			"xref": [
				"Cf. A001222, A048675, A054841, A090880, A090881, A090882, A090883."
			],
			"keyword": "easy,nonn,mult",
			"offset": "1,3",
			"author": "_Sam Alexander_, Dec 12 2003",
			"ext": [
				"More terms from _Ray Chandler_, Dec 20 2003"
			],
			"references": 7,
			"revision": 11,
			"time": "2018-09-24T11:49:12-04:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}