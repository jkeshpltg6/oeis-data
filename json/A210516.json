{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A210516",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 210516,
			"data": "0,1,2,7,3,3,2,0,3,6,5,4,15,7,5,8,9,3,11,6,7,16,1,0,8,2,7,4,3,4,16,5,7,25,4,17,19,5,13,12,6,7,17,18,8,6,7,3,0,3,22,4,3,8,31,14,10,6,9,11,26,12,19,21,32,10,9,10,1,31,8,7,18,2,8,16,11,76",
			"name": "The length-1 of the Collatz (3k+1) sequence for all odd fractions and integers.",
			"comment": [
				"This sequence is the unification, in the limit, of the length of Collatz sequences for all fractions whose denominator is odd, and also all integers.",
				"The sequence A210483 gives the triangle read by rows giving trajectory of k/(2n+1) in Collatz problem, k = 1..2n, but particular attention should be paid to numbers in the triangle T(n,k) = (n-k)/(2k+1) for n = 1,2,... and k = 0..n-1.",
				"The example shown below gives a general idea of this regular triangle. This contains all fractions whose denominator is odd and all integers. Now, from T(n,k) we could introduce a 3D triangle in order to produce a complete Collatz sequence starting from each rational T(n,k).",
				"Remark: a(A000124(n)) = A006577(n) because the first column of this triangle generates A006577.",
				"The triangle T(n,k) begins",
				"  1;",
				"  2,  1/3;",
				"  3,  2/3,  1/5;",
				"  4,  3/3,  2/5,  1/7;",
				"  5,  4/3,  3/5,  2/7,  1/9;",
				"  6,  5/3,  4/5,  3/7,  2/9, 1/11;",
				"  ..."
			],
			"link": [
				"Michel Lagneau, \u003ca href=\"/A210516/b210516.txt\"\u003eRows n = 1..100, flattened\u003c/a\u003e",
				"J. C. Lagarias, \u003ca href=\"http://pldml.icm.edu.pl:80/mathbwn/element/bwmeta1.element.bwnjournal-article-aav56i1p33bwm?q=bwmeta1.element.bwnjournal-number-aa-1990-56-1\u0026amp;qt=CHILDREN-STATELESS\"\u003eThe set of rational cycles for the 3x+1 problem,\u003c/a\u003e Acta Arith. 56 (1990), 33-53."
			],
			"example": [
				"The triangle of lengths begins",
				"  0;",
				"  1,  2;",
				"  7,  3,  3;",
				"  2,  0,  3,  6;",
				"  5,  4, 15,  7,  5;",
				"  ...",
				"Individual numbers have the following Collatz sequences:",
				"[1] =\u003e [0] (0 iteration);",
				"[2  1/3] =\u003e [1, 2] because: 2 -\u003e 1  =\u003e 1 iteration;  1/3 -\u003e 2 -\u003e 1 =\u003e 2 iterations;",
				"[3  2/3  1/5] =\u003e [7, 3, 3] because: 3-\u003e10-\u003e5-\u003e16-\u003e8-\u003e4-\u003e2-\u003e1 =\u003e 7 iterations; 2/3 -\u003e 1/3 -\u003e 2 -\u003e 1 =\u003e 3 iterations; 1/5 -\u003e 8/5 -\u003e 4/5 -\u003e 2/5 =\u003e 3 iterations."
			],
			"mathematica": [
				"Collatz2[n_] := Module[{lst = NestWhileList[If[EvenQ[Numerator[#]], #/2, 3 # + 1] \u0026, n, Unequal, All]}, If[lst[[-1]] == 1, lst = Drop[lst, -3], If[lst[[-1]] == 2, lst = Drop[lst, -2], If[lst[[-1]] == 4, lst = Drop[lst, -1], If[MemberQ[Rest[lst], lst[[-1]]], lst = Drop[lst, -1]]]]]]; t = Table[s = Collatz2[(n - k)/(2*k + 1)]; Length[s] - 1, {n, 12}, {k, 0, n - 1}]; Flatten[t] (* _T. D. Noe_, Jan 28 2013 *)"
			],
			"xref": [
				"Cf. A210483, A210468, A210688."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Michel Lagneau_, Jan 26 2013",
			"references": 4,
			"revision": 33,
			"time": "2017-06-13T03:53:42-04:00",
			"created": "2013-01-30T16:20:51-05:00"
		}
	]
}