{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A058543",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 58543,
			"data": "1,-2,1,-4,8,-6,10,-16,18,-26,33,-40,58,-74,82,-112,147,-166,212,-268,316,-392,476,-560,695,-838,967,-1184,1430,-1648,1970,-2352,2731,-3236,3803,-4404,5206,-6080,6984,-8192,9553,-10942,12709,-14736,16886,-19506,22448,-25648,29552",
			"name": "McKay-Thompson series of class 18e for the Monster group.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"Vaclav Kotesovec, \u003ca href=\"/A058543/b058543.txt\"\u003eTable of n, a(n) for n = 0..5000\u003c/a\u003e",
				"D. Ford, J. McKay and S. P. Norton, \u003ca href=\"http://dx.doi.org/10.1080/00927879408825127\"\u003eMore on replicable functions\u003c/a\u003e, Commun. Algebra 22, No. 13, 5175-5193 (1994).",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e",
				"\u003ca href=\"/index/Mat#McKay_Thompson\"\u003eIndex entries for McKay-Thompson series for Monster simple group\u003c/a\u003e"
			],
			"formula": [
				"Expansion of chi(-x)^2 * chi(-x^3)^2 in powers of x where chi() is a Ramanujan theta function. - _Michael Somos_, Aug 18 2007",
				"Expansion of q^(-1/3) * (eta(q) * eta(q^3) / (eta(q^2) * eta(q^6)))^2 in powers of q. - _Michael Somos_, Aug 18 2007",
				"Euler transform of period 6 sequence [ -2, 0, -4, 0, -2, 0, ...]. - _Michael Somos_, Aug 18 2007",
				"Given g.f. A(x), then B(x) = A(x^3) / x satisfies 0 = f(B(x), B(x^2)) where f(u, v) = v^2 - u^2 * v - 4 * u. - _Michael Somos_, Aug 18 2007",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (6 t)) = 4 / f(t) where q = exp(2 Pi i t). - _Michael Somos_, Aug 18 2007",
				"a(n) ~ (-1)^n * exp(2*Pi*sqrt(n)/3) / (2*sqrt(3)*n^(3/4)). - _Vaclav Kotesovec_, Sep 08 2017"
			],
			"example": [
				"G.f. = 1 - 2*x + x^2 - 4*x^3 + 8*x^4 - 6*x^5 + 10*x^6 - 16*x^7 + 18*x^8 - ...",
				"T18e = 1/q - 2*q^2 + q^5 - 4*q^8 + 8*q^11 - 6*q^14 + 10*q^17 - 16*q^20 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ (QPochhammer[ q, q^2] QPochhammer[ q^3, q^6])^2, {q, 0, n}]; (* _Michael Somos_, Jul 11 2011 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( (eta(x + A) * eta(x^3 + A) / (eta(x^2 + A) * eta(x^6 + A)))^2, n))}; /* _Michael Somos_, Aug 18 2007 */"
			],
			"xref": [
				"Cf. A000521, A007240, A014708, A007241, A007267, A045478, etc."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Nov 27 2000",
			"references": 2,
			"revision": 25,
			"time": "2021-03-12T22:24:42-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}