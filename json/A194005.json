{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A194005",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 194005,
			"data": "1,1,1,1,2,1,1,3,2,1,1,4,3,3,1,1,5,4,6,3,1,1,6,5,10,6,4,1,1,7,6,15,10,10,4,1,1,8,7,21,15,20,10,5,1,1,9,8,28,21,35,20,15,5,1,1,10,9,36,28,56,35,35,15,6,1,1,11,10,45,36,84,56,70,35,21,6,1",
			"name": "Triangle of the coefficients of an (n+1)-th order differential equation associated with A103631.",
			"comment": [
				"This triangle is a companion to Parks' triangle A103631.",
				"The coefficients of triangle A103631(n,k) appear in appendix 2 of Park’s remarkable article “A new proof of the Routh-Hurwitz stability criterion using the second method of Liapunov” if we assume that the b(n) coefficients are all equal to 1, see the second Maple program.",
				"The a(n,k) coefficients of the triangle given above are related to the coefficients of a linear (n+1)-th order differential equation for the case b(n)=1, see the examples.",
				"a(n,k) is also the number of symmetric binary strings of odd length n with Hamming weight k\u003e0 and no consecutive 1's. - _Christian Barrientos_ and _Sarah Minion_, Feb 27 2018"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A194005/b194005.txt\"\u003eRows n = 0..150 of triangle, flattened\u003c/a\u003e",
				"Henry W. Gould, \u003ca href=\"http://www.fq.math.ca/3-4.html\"\u003e A Variant of Pascal's Triangle \u003c/a\u003e, The Fibonacci Quarterly, Vol. 3, Nr. 4, Dec. 1965, p. 257-271.",
				"P.C. Parks, \u003ca href=\"https://doi.org/10.1017/S030500410004072X\"\u003e A new proof of the Routh-Hurwitz stability criterion using the second method of Liapunov \u003c/a\u003e, Math. Proc. of the Cambridge Philosophical Society, Vol. 58, Issue 04 (1962) p. 694-702.",
				"Chris Zheng, Jeffrey Zheng, \u003ca href=\"https://doi.org/10.1007/978-981-13-2282-2_4\"\u003eTriangular Numbers and Their Inherent Properties\u003c/a\u003e, Variant Construction from Theoretical Foundation to Applications, Springer, Singapore, 51-65.",
				"\u003ca href=\"/index/Pas#Pascal\"\u003eIndex entries for triangles and arrays related to Pascal's triangle\u003c/a\u003e"
			],
			"formula": [
				"a(n,k) = binomial(floor((2*n+1-k)/2), n-k).",
				"a(n,k) = sum(A103631(n1,k), n1=k..n), 0\u003c=k\u003c=n and n\u003e=0.",
				"a(n,k) = sum(binomial(floor((2*n1-k-1)/2), n1-k), n1=k..n).",
				"T(n,0) = T(n,n) = 1, T(n,k) = T(n-2,k-2) + T(n-1,k), 0 \u003c k \u003c n. - _Reinhard Zumkeller_, Nov 23 2012"
			],
			"example": [
				"For the 5th-order linear differential equation the coefficients a(k) are: a(0) = 1, a(1) = a(4,0) = 1, a(2) = a(4,1) = 4, a(3) = a(4,2) = 3, a(4) = a(4,3) = 3 and a(5) = a(4,4) = 1.",
				"The corresponding Hurwitz matrices A(k) are, see Parks: A(5) = Matrix([[a(1),a(0),0,0,0], [a(3),a(2),a(1),a(0),0], [a(5),a(4),a(3),a(2),a(1)], [0,0,a(5),a(4),a(3)], [0,0,0,0,a(5)]]), A(4) = Matrix([[a(1),a(0),0,0], [a(3),a(2),a(1),a(0)], [a(5),a(4),a(3),a(2)], [0,0,a(5),a(4)]]), A(3) = Matrix([[a(1),a(0),0], [a(3),a(2),a(1)], [a(5),a(4),a(3)]]), A(2) = Matrix([[a(1),a(0)], [a(3),a(2)]]) and A(1) = Matrix([[a(1)]]).",
				"The values of b(k) are, see Parks: b(1) = d(1), b(2) = d(2)/d(1), b(3) = d(3)/(d(1)*d(2)), b(4) = d(1)*d(4)/(d(2)*d(3)) and b(5) = d(2)*d(5)/(d(3)*d(4)).",
				"These a(k) values lead to d(k) = 1 and subsequently to b(k) = 1 and this confirms our initial assumption, see the comments."
			],
			"maple": [
				"A194005 := proc(n,k): binomial(floor((2*n+1-k)/2),n-k) end: for n from 0 to 11 do seq(A194005(n,k), k=0..n) od; seq(seq(A194005(n,k), k=0..n), n=0..11);",
				"nmax:=11: for n from 0 to nmax+1 do b(n):=1 od: A103631 := proc(n,k) option remember: local j: if k=0 and n=0 then b(1) elif k=0 and n\u003e=1 then 0 elif k=1 then b(n+1) elif k=2 then b(1)*b(n+1) elif k\u003e=3 then expand(b(n+1)*add(procname(j,k-2), j=k-2..n-2)) fi: end: for n from 0 to nmax do for k from 0 to n do A194005(n,k):= add(A103631(n1,k), n1=k..n) od: od: seq(seq(A194005(n,k),k=0..n), n=0..nmax);"
			],
			"mathematica": [
				"Flatten[Table[Binomial[Floor[(2n+1-k)/2],n-k],{n,0,20},{k,0,n}]] (* _Harvey P. Dale_, Apr 15 2012 *)"
			],
			"program": [
				"(Haskell)",
				"a194005 n k = a194005_tabl !! n !! k",
				"a194005_row n = a194005_tabl !! n",
				"a194005_tabl = [1] : [1,1] : f [1] [1,1] where",
				"   f row' row = rs : f row rs where",
				"     rs = zipWith (+) ([0,1] ++ row') (row ++ [0])",
				"-- _Reinhard Zumkeller_, Nov 22 2012"
			],
			"xref": [
				"Cf. A065941 and A103631.",
				"Triangle sums (see A180662): A000071 (row sums; alt row sums), A075427 (Kn22), A000079 (Kn3), A109222(n+1)-1 (Kn4), A000045 (Fi1), A034943 (Ca3), A001519 (Gi3), A000930 (Ze3)",
				"Interesting diagonals: T(n,n-4) = A189976(n+5) and T(n,n-5) = A189980(n+6)",
				"Cf. A052509."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "0,5",
			"author": "_Johannes W. Meijer_ \u0026 A. Hirschberg (a.hirschberg(AT)tue.nl), Aug 11 2011",
			"references": 9,
			"revision": 48,
			"time": "2019-02-07T19:31:52-05:00",
			"created": "2011-08-11T17:58:56-04:00"
		}
	]
}