{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A097839",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 97839,
			"data": "1,83,6888,571621,47437655,3936753744,326703123097,27112422463307,2250004361331384,186723249568041565,15495779709786118511,1285962992662679794848,106719432611292636853873,8856426943744626179076611,734976716898192680226504840",
			"name": "Chebyshev polynomials S(n,83).",
			"comment": [
				"Used for all positive integer solutions of Pell equation x^2 - 85*y^2 = -4. See A097840 with A097841."
			],
			"link": [
				"Indranil Ghosh, \u003ca href=\"/A097839/b097839.txt\"\u003eTable of n, a(n) for n = 0..520\u003c/a\u003e",
				"Hacène Belbachir, Soumeya Merwa Tebtoub, and László Németh, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL23/Nemeth/nemeth7.html\"\u003eEllipse Chains and Associated Sequences\u003c/a\u003e, J. Int. Seq., Vol. 23 (2020), Article 20.8.5.",
				"R. Flórez, R. A. Higuita, and A. Mukherjee, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Mukherjee/mukh2.html\"\u003eAlternating Sums in the Hosoya Polynomial Triangle\u003c/a\u003e, Article 14.9.5 Journal of Integer Sequences, Vol. 17 (2014).",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (83,-1)."
			],
			"formula": [
				"a(n) = S(n, 83) = U(n, 83/2) = S(2*n+1, sqrt(85))/sqrt(85) with S(n, x) = U(n, x/2) Chebyshev's polynomials of the second kind, A049310. S(-1, x) = 0 = U(-1, x).",
				"a(n) = 83*a(n-1) - a(n-2), n \u003e= 1, a(-1)=0, a(0)=1, a(1)=83.",
				"a(n) = (ap^(n+1) - am^(n+1))/(ap - am) with ap = (83+9*sqrt(85))/2 and am = (83-9*sqrt(85))/2 = 1/ap.",
				"G.f.: 1/(1-83*x+x^2)."
			],
			"mathematica": [
				"CoefficientList[Series[1/(1-83x+x^2),{x,0,20}],x] (* or *) LinearRecurrence[{83,-1},{1,83},20] (* _Harvey P. Dale_, Oct 11 2012 *)"
			],
			"program": [
				"(PARI) my(x='x+O('x^20)); Vec(1/(1-83*x+x^2)) \\\\ _G. C. Greubel_, Jan 13 2019",
				"(MAGMA) m:=20; R\u003cx\u003e:=PowerSeriesRing(Integers(), m); Coefficients(R!( 1/(1-83*x+x^2) )); // _G. C. Greubel_, Jan 13 2019",
				"(Sage) (1/(1-83*x+x^2)).series(x, 20).coefficients(x, sparse=False) # _G. C. Greubel_, Jan 13 2019",
				"(GAP) a:=[1,83];; for n in [3..20] do a[n]:=83*a[n-1]-a[n-2]; od; a; # _G. C. Greubel_, Jan 13 2019"
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Sep 10 2004",
			"ext": [
				"More terms from _Harvey P. Dale_, Oct 11 2012"
			],
			"references": 5,
			"revision": 29,
			"time": "2021-01-01T03:23:55-05:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}