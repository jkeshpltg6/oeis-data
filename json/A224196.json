{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A224196",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 224196,
			"data": "0,2,8,2,5,1,7,6,4,1,6,0,0,6,7,9,3,7,8,7,3,2,1,0,7,3,2,9,9,6,2,9,8,9,8,5,1,5,4,2,7,0,2,0,2,0,1,8,1,6,0,9,9,1,7,7,1,6,9,1,9,4,8,2,9,4,4,6,3,6,3,7,2,3,3,3,0,5,7,5,1,4,9,3,7,4,7",
			"name": "Decimal expansion of the 3rd du Bois Reymond constant.",
			"comment": [
				"From _Jon E. Schoenfield_, Aug 17 2014: (Start)",
				"Evaluating the partial sums",
				"   Sum_{k=1..j} 2*(1 + x_k ^ 2)^(-3/2)",
				"(where x_k is the k-th root of tan(t)=t; see the Mathworld link) at j = 1, 2, 4, 8, 16, 32, ..., it becomes apparent that they approach",
				"   c0 + c2/j^2 + c3/j^3 + c4/j^4 + ...",
				"where",
				"   c0 = 0.02825176416006793787321073299629898515427...",
				"and c2 and c3 are -4/Pi^3 and 16/Pi^3, respectively.",
				"(The k-th root of tan(t)=t is",
				"   r - d_1/r - d_2/r^3 - d_3/r^5 - d_4/r^7 - d_5/r^9 - ...",
				"where r = (k+1/2) * Pi and d_j = A079330(j)/A088989(j).) (End)",
				"d_n = A079330(n)/A088989(n) ~ GAMMA(1/3) / (2^(2/3) * 3^(1/6) * Pi^(5/3)) * (Pi/2)^(2*n) / n^(4/3). - _Vaclav Kotesovec_, Aug 19 2014"
			],
			"reference": [
				"S. R. Finch, Mathematical Constants, Cambridge, 2003, pp. 237-239."
			],
			"link": [
				"Jon E. Schoenfield and Vaclav Kotesovec, \u003ca href=\"/A224196/b224196.txt\"\u003eTable of n, a(n) for n = 0..450\u003c/a\u003e (first 100 terms from Jon E. Schoenfield)",
				"Eric Weisstein's MathWorld, \u003ca href=\"http://mathworld.wolfram.com/duBois-ReymondConstants.html\"\u003eDu Bois Reymond Constants\u003c/a\u003e"
			],
			"example": [
				"0.028251764..."
			],
			"mathematica": [
				"digits = 16; m0 = 10^5; dm = 10^5; Clear[xi, c3]; xi[n_?NumericQ] := xi[n] = x /. FindRoot[x == Tan[x], {x, n*Pi + Pi/2 - 1/(4*n)}, WorkingPrecision -\u003e digits + 5]; c3[m_] := c3[m] = 2*Sum[1/(1 + xi[n]^2)^(3/2), {n, 1, m}] - 2*PolyGamma[2, m + 1]/(2*Pi^3); c3[m0] ; c3[m = m0 + dm]; While[RealDigits[c3[m], 10, digits] != RealDigits[c3[m - dm], 10, digits], Print[\"m = \", m, \" \", c3[m]]; m = m + dm]; RealDigits[c3[m], 10, digits] // First"
			],
			"xref": [
				"Cf. A062546 (2nd du Bois Reymond constant).",
				"Cf. A207528 (4th du Bois Reymond constant).",
				"Cf. A243108 (5th du Bois Reymond constant).",
				"Cf. A245333 (6th du Bois Reymond constant)."
			],
			"keyword": "nonn,cons",
			"offset": "0,2",
			"author": "_Jean-François Alcover_, Apr 15 2013",
			"ext": [
				"a(8)-a(15) from _Robert G. Wilson v_, Nov 06 2013",
				"More terms from _Jon E. Schoenfield_, Aug 17 2014"
			],
			"references": 11,
			"revision": 76,
			"time": "2019-07-14T21:37:28-04:00",
			"created": "2013-04-15T11:59:14-04:00"
		}
	]
}