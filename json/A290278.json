{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A290278",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 290278,
			"data": "0,4,3,6,4,6,3,4,0,0,4,12,15,22,24,30,31,36,36,40,39,42,40,42,39,40,36,36,31,30,33,40,42,48,49,54,54,58,57,60,58,60,57,58,54,54,49,48,42,40,42,48,49,54,54,58,57,60,58,60,57,58,54,54,49,48,42,40",
			"name": "a(n) = Sum_{k=0..n} (A007953(5*k) - A007953(k)).",
			"comment": [
				"This sequence is closely related to A289411.",
				"The scatterplots of this sequence and of A289411 have similarites, including the same type of symmetry on the first 10^k terms for k \u003e 0.",
				"For b \u003e 1, let d_b be the digital sum in base b:",
				"- we have for example d_10 = A007953,",
				"- also, d_b(b*n) = d_b(n) for any n \u003e= 0,",
				"- and d_b(n + m) = d_b(n) + d_b(m) iff n and m can be added without carry in base b,",
				"- hence if i divides b and k \u003e 0 and 0 \u003c= n \u003c= b^k-1, then d_b(i * n) + d_b(i * (b^k-1 - n)) = k*(b-1) (as i * n and i * (b^k-1 - n) can be added without carry in base b),",
				"- if i and j divides b and k \u003e 0 and 0 \u003c= n \u003c= b^k-1, then d_b(i * n) - d_b(j * n) = d_b(j * (b^k-1 - n)) - d_b(i * (b^k-1 - n)) (this implies the conjecture about the symmetry of A289411),",
				"- also, if i and j divides b and k \u003e 0, Sum_{m=0..b^k-1} (d_b(i * m) - d_b(j * m)) = 0.",
				"For b \u003e 1, i \u003e 0 and j \u003e 0 such that neither i nor j are divisible by b, let G(b,i,j) be the function defined by n -\u003e Sum_{k=0..n} (d_b(i*k) - d_b(j*k)):",
				"- we have for example G(10,5,1) = a (this sequence),",
				"- G(b,i,i) = 0, G(b,i,j) = -G(b,j,i), G(b,i,j) + G(b,j,k) = G(b,i,k),",
				"- if i and j divide b and k \u003e 0 and 0 \u003c= n \u003c= b^k-2, then G(b,i,j)(n) = G(b,i,j)(b^k-2 - n) (in other words, the sequence G(b,i,j) restricted to the first b^k-1 terms is symmetrical), and G(b,i,j)(b^k-2) = 0 (in other words, G(b,i,j) has infinitely many zeros)."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A290278/b290278.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e"
			],
			"mathematica": [
				"Block[{nn = 68, k = 5, s}, s = Table[Total@ IntegerDigits[k n] - Total@ IntegerDigits@ n, {n, 0, nn}]; Table[Total@ Take[s, n], {n, nn}]] (* _Michael De Vlieger_, Jul 31 2017 *)"
			],
			"program": [
				"(PARI) s = 0; for (n=0, 67, s += sum digits(5*n) - sum digits(n); print1 (s \", \"))"
			],
			"xref": [
				"Cf. A289411."
			],
			"keyword": "nonn,base,look",
			"offset": "0,2",
			"author": "_Rémy Sigrist_, Jul 25 2017",
			"references": 1,
			"revision": 17,
			"time": "2017-08-07T03:32:17-04:00",
			"created": "2017-08-06T22:55:20-04:00"
		}
	]
}