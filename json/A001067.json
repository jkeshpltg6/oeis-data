{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001067",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1067,
			"data": "1,-1,1,-1,1,-691,1,-3617,43867,-174611,77683,-236364091,657931,-3392780147,1723168255201,-7709321041217,151628697551,-26315271553053477373,154210205991661,-261082718496449122051,1520097643918070802691,-2530297234481911294093",
			"name": "Numerator of Bernoulli(2*n)/(2*n).",
			"comment": [
				"Also numerator of \"modified Bernoulli number\" b(2n) = Bernoulli(2*n)/(2*n*n!). Denominators are in A057868.",
				"Ramanujan incorrectly conjectured that the sequence contains only primes (and 1) - _Jud McCranie_. See A112548, A119766.",
				"a(n) = A046968(n) if n \u003c 574; a(574) = 37 * A046968(574). - _Michael Somos_, Feb 01 2004",
				"Absolute values give denominators of constant terms of Fourier series of meromorphic modular forms E_k/Delta, where E_k is the normalized k th Eisenstein series [cf. Gunning or Serre references] and Delta is the normalized unique weight-twelve cusp form for the full modular group (the generating function of Ramanujan's tau function.) - Barry Brent (barrybrent(AT)iphouse.com), Jun 01 2009",
				"|a(n)| is a product of powers of irregular primes (A000928), with the exception of n = 1,2,3,4,5,7. - _Peter Luschny_, Jul 28 2009",
				"Conjecture: If there is a prime p such that 2*n+1 \u003c p and p divides a(n), then p^2 does not divide a(n). This conjecture is true for p \u003c 12 million. - _Seiichi Manyama_, Jan 21 2017"
			],
			"reference": [
				"M. Abramowitz and I. A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, 1964 (and various reprintings), p. 259, (6.3.18) and (6.3.19); also p. 810.",
				"L. V. Ahlfors, Complex Analysis, McGraw-Hill, 1979, p. 205",
				"R. C. Gunning, Lectures on Modular Forms. Princeton Univ. Press, Princeton, NJ, 1962, p. 53.",
				"R. Kanigel, The Man Who Knew Infinity, pp. 91-92.",
				"J. W. Milnor and J. D. Stasheff, Characteristic Classes, Princeton, 1974, p. 285.",
				"J.-P. Serre, A Course in Arithmetic, Springer-Verlag, 1973, p. 93."
			],
			"link": [
				"T. D. Noe and Seiichi Manyama, \u003ca href=\"/A001067/b001067.txt\"\u003eTable of n, a(n) for n = 1..314\u003c/a\u003e (first 100 terms from T. D. Noe)",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards Applied Math.Series 55, Tenth Printing, 1972, p. 259, (6.3.18) and (6.3.19).",
				"D. Bar-Natan, T. T. Q. Le and D. P. Thurston, \u003ca href=\"https://arxiv.org/abs/math/0204311\"\u003eTwo applications of elementary knot theory to Lie algebras and Vassiliev invariants\u003c/a\u003e, arXiv:math/0204311 [math.QA], 2002-2003; Geometry and Topology 7-1 (2003) 1-31.",
				"G. Everest, A. J. van der Poorten, Y. Puri and T. Ward, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL5/Ward/ward2.html\"\u003eInteger Sequences and Periodic Points\u003c/a\u003e, Journal of Integer Sequences, Vol. 5 (2002), Article 02.2.3.",
				"E. Z. Goren, \u003ca href=\"http://www.math.mcgill.ca/goren/ZetaValues/Riemann.html\"\u003eTables of values of Riemann zeta functions\u003c/a\u003e",
				"M. Janjic, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL13/Janjic/janjic33.html\"\u003eHessenberg Matrices and Integer Sequences\u003c/a\u003e, J. Int. Seq. 13 (2010) # 10.7.8, section 3.",
				"J. Sondow and E. W. Weisstein, \u003ca href=\"http://mathworld.wolfram.com/RiemannZetaFunction.html\"\u003eMathWorld: Riemann Zeta Function\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/EisensteinSeries.html\"\u003eEisenstein Series.\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/BernoulliNumber.html\"\u003eBernoulli Number.\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ModifiedBernoulliNumber.html\"\u003eModified Bernoulli Number.\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Kummer%E2%80%93Vandiver_conjecture\"\u003eKummer-Vandiver conjecture\u003c/a\u003e",
				"\u003ca href=\"/index/Be#Bernoulli\"\u003eIndex entries for sequences related to Bernoulli numbers\u003c/a\u003e"
			],
			"formula": [
				"Zeta(1-2*n) = - Bernoulli(2*n)/(2*n).",
				"G.f.: numerators of coefficients of z^(2*n) in z/(exp(z)-1). - _Benoit Cloitre_, Jun 02 2003",
				"For 2 \u003c= k \u003c= 1000 and k != 7, the 2-order of the full constant term of E_k/Delta = 3 + ord_2(k - 7). - Barry Brent (barrybrent(AT)iphouse.com), Jun 01 2009",
				"G.f. for Bernoulli(2*n)/(2*n) = a(n)/A006953(n): (-1)^n/((2*Pi)^(2*n)*(2*n))*integral(log(1-1/t)^(2*n) dt,t=0,1). - _Gerry Martens_, May 18 2011",
				"E.g.f.: a(n) = numerator((2*n+1)!*[x^(2*n+1)](1/(1-1/exp(x)))). - _Peter Luschny_, Jul 12 2012",
				"|a(n)| = Numerator of Integral_{r=0..1} HurwitzZeta(1-n, r)^2. More general: |Bernoulli(2*n)| = binomial(2*n,n)*n^2*I(n) for n\u003e=1 where I(n) denotes the integral. - _Peter Luschny_, May 24 2015"
			],
			"example": [
				"The sequence Bernoulli(2*n)/(2*n) (n \u003e= 1) begins 1/12, -1/120, 1/252, -1/240, 1/132, -691/32760, 1/12, -3617/8160, ...",
				"The sequence of modified Bernoulli numbers begins 1/48, -1/5760, 1/362880, -1/19353600, 1/958003200, -691/31384184832000, ..."
			],
			"maple": [
				"A001067_list := proc(n) 1/(1-1/exp(z)); series(%,z,2*n+4);",
				"seq(numer((2*i+1)!*coeff(%,z,2*i+1)),i=0..n) end:",
				"A001067_list(21); # _Peter Luschny_, Jul 12 2012"
			],
			"mathematica": [
				"Table[ Numerator[ BernoulliB[2n]/(2n)], {n, 1, 22}] (* _Robert G. Wilson v_, Feb 03 2004 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, 0, numerator( bernfrac(2*n) / (2*n)))}; /* _Michael Somos_, Feb 01 2004 */",
				"(Sage)",
				"@CachedFunction",
				"def S(n, k) :",
				"    if k == 0 :",
				"        if n == 0 : return 1",
				"        else: return 0",
				"    return S(n, k-1) + S(n-1, n-k)",
				"def BernoulliDivN(n) :",
				"    if n == 0 : return 1",
				"    return (-1)^n*S(2*n-1,2*n-1)/(4^n-16^n)",
				"[BernoulliDivN(n).numerator() for n in (1..22)]",
				"# _Peter Luschny_, Jul 08 2012",
				"(Sage) [numerator(bernoulli(2*n)/(2*n)) for n in (1..25)] # _G. C. Greubel_, Sep 19 2019",
				"(MAGMA) [Numerator(Bernoulli(2*n)/(2*n)):n in [1..40]]; // _Vincenzo Librandi_, Sep 17 2015",
				"(GAP) List([1..25], n-\u003e NumeratorRat(Bernoulli(2*n)/(2*n)));  # _G. C. Greubel_, Sep 19 2019"
			],
			"xref": [
				"Similar to but different from A046968. See A090495, A090496.",
				"Denominators given by A006953.",
				"Cf. A000367, A006863, A033563, A046968.",
				"Cf. A141590."
			],
			"keyword": "sign,frac,nice",
			"offset": "1,6",
			"author": "_N. J. A. Sloane_, Richard E. Borcherds (reb(AT)math.berkeley.edu)",
			"references": 39,
			"revision": 109,
			"time": "2021-01-04T08:30:16-05:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}