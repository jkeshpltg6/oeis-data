{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A271254",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 271254,
			"data": "1,5,36,188,876,3788,15756,64268,259596,1043468,4184076,16756748,67067916,268353548,1073577996,4294639628",
			"name": "Number of active (ON,black) cells at stage 2^n-1 of the two-dimensional cellular automaton defined by \"Rule 323\", based on the 5-celled von Neumann neighborhood.",
			"comment": [
				"Initialized with a single black (ON) cell at stage zero.",
				"Similar to A270084."
			],
			"reference": [
				"S. Wolfram, A New Kind of Science, Wolfram Media, 2002; p. 170."
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"http://arxiv.org/abs/1503.01168\"\u003eOn the Number of ON Cells in Cellular Automata\u003c/a\u003e, arXiv:1503.01168 [math.CO], 2015",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ElementaryCellularAutomaton.html\"\u003eElementary Cellular Automaton\u003c/a\u003e",
				"S. Wolfram, \u003ca href=\"http://wolframscience.com/\"\u003eA New Kind of Science\u003c/a\u003e",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_2D_5-Neighbor_Cellular_Automata\"\u003eIndex to 2D 5-Neighbor Cellular Automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_Elementary_Cellular_Automata\"\u003eIndex to Elementary Cellular Automata\u003c/a\u003e"
			],
			"formula": [
				"Conjecture: a(n) = 4*4^n - 10*2^n + 12, n\u003e1. - _Lars Blomberg_, Jun 14 2016",
				"Conjectures from _Colin Barker_, Dec 01 2016: (Start)",
				"a(n) = 7*a(n-1) - 14*a(n-2) + 8*a(n-3) for n\u003e4.",
				"G.f.: (1 - 2*x + 15*x^2 - 2*x^3 + 24*x^4) / ((1 - x) * (1 - 2*x) * (1 - 4*x)).",
				"(End)"
			],
			"mathematica": [
				"CAStep[rule_,a_]:=Map[rule[[10-#]]\u0026,ListConvolve[{{0,2,0},{2,1,2},{0,2,0}},a,2],{2}];",
				"code=323; stages=128;",
				"rule=IntegerDigits[code,2,10];",
				"g=2*stages+1; (* Maximum size of grid *)",
				"a=PadLeft[{{1}},{g,g},0,Floor[{g,g}/2]]; (* Initial ON cell on grid *)",
				"ca=a;",
				"ca=Table[ca=CAStep[rule,ca],{n,1,stages+1}];",
				"PrependTo[ca,a];",
				"(* Trim full grid to reflect growth by one cell at each stage *)",
				"k=(Length[ca[[1]]]+1)/2;",
				"ca=Table[Table[Part[ca[[n]][[j]],Range[k+1-n,k-1+n]],{j,k+1-n,k-1+n}],{n,1,k}];",
				"on=Map[Function[Apply[Plus,Flatten[#1]]],ca] (* Count ON cells at each stage *)",
				"Part[on,2^Range[0,Log[2,stages]]] (* Extract relevant terms *)"
			],
			"xref": [
				"Cf. A270084, A270569."
			],
			"keyword": "nonn,more",
			"offset": "0,2",
			"author": "_Robert Price_, Apr 02 2016",
			"ext": [
				"a(8)-a(15) from _Lars Blomberg_, Jun 14 2016"
			],
			"references": 1,
			"revision": 12,
			"time": "2016-12-01T06:04:41-05:00",
			"created": "2016-04-03T10:07:21-04:00"
		}
	]
}