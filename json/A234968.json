{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A234968",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 234968,
			"data": "1,2,3,3,3,5,5,5,6,7,5,9,6,9,13,11,7,16,14,14,16,19,14,23,24,21,27,32,21,39,39,32,38,51,45,56,60,51,62,87,61,82,101,83,98,129,104,120,152,137,145,196,157,178,248,207,209,293,248,275,353,310,325,441,388,389,528,471,463,656,573,567,766,696,691,934",
			"name": "Number of totally symmetric partitions of n of any dimension.",
			"comment": [
				"a(n) is the sum over d from 1 to inf of the number of totally symmetric d-dimensional Ferrers diagrams with n nodes.",
				"A d-dimensional Ferrers diagram is totally symmetric if and only if whenever X=(x1,x2,...,xd) is a node, then so are all nodes which can be specified by permuting the coordinates of X.",
				"Since a(1)=inf, the sequence above begins on n=2. All other terms are finite."
			],
			"link": [
				"Graham H. Hawkes, \u003ca href=\"/A234968/b234968.txt\"\u003eTable of n, a(n) for n = 2..90\u003c/a\u003e",
				"Graham H. Hawkes, \u003ca href=\"/A234968/a234968_2.txt\"\u003eTable of TS FD for dim 1...7\u003c/a\u003e"
			],
			"example": [
				"a(1)=inf because for each dimension, d, the trivial Ferrer's diagram given by the single node (1,1,1,... ,1) is a totally symmetric d-dimensional partition of 1.",
				"For n\u003e2, a(n)\u003cinf. This means that for n\u003e2, there are at most a finite number of dimensions, d, for which the number of totally symmetric d-dimensional partitions of n is nonzero (and that for any dimension, d, there are at most a finite number of totally symmetric d-dimensional partitions of n).",
				"a(2)=1. Indeed the only totally symmetric partition of 2 occurs in dimension 1. The corresponding 1-dimensional totally symmetric Ferrers diagram (TS FD) is given by the following two nodes (specified by the 1-dimensional coordinates): (2) and (1).",
				"a(8)=5.",
				"There is one 1-dimensional TS FD of 8:",
				"{(8),(7),(6),(5),(4),(3),(2),(1)}",
				"There are two 2-dimensional TS FD of 8:",
				"{(3,2),(2,3),(3,1),(2,2),(1,3),(2,1),(1,2),(1,1) and",
				"{(4,1),(1,4),(3,1),(2,2),(1,3),(2,1),(1,2),(1,1)}",
				"There is one 3-dimensional TS FD of 8:",
				"{(2,2,2),(2,2,1),(2,1,2),(1,2,2),(2,1,1),(1,2,1),(1,1,2),(1,1,1)}",
				"There is one 7-dimensional TS FD of 8:",
				"{(2,1,1,1,1,1,1),(1,2,1,1,1,1,1),(1,1,2,1,1,1,1),(1,1,1,2,1,1,1),(1,1,1,1,2,1,1),(1,1,1,1,1,2,1),(1,1,1,1,1,1,2),(1,1,1,1,1,1,1)}",
				"There are no TS FD of 8 of any other dimension. Hence a(8)=1+2+1+1=5",
				"a(72)=573",
				"The TS FD of 72 are:",
				"Dim 1: 1",
				"Dim 2: 471",
				"Dim 3: 85",
				"Dim 4: 11",
				"Dim 5: 3",
				"Dim 6: 1",
				"Dim 71: 1",
				"(For n\u003e1)there is always exactly 1 TS FD of dimension 1 and 1 TS FD of dimension n-1.  If n\u003e2, these two dimensions are not equal, so there must be at least two TS FD. Hence a(n)\u003e=2 for n\u003e2."
			],
			"xref": [
				"The number of TS FD of dimensions 2, 3, and 4 are given by sequences A000700, A048141, and A097516 respectively."
			],
			"keyword": "nonn,nice",
			"offset": "2,2",
			"author": "_Graham H. Hawkes_, Jan 02 2014",
			"references": 1,
			"revision": 19,
			"time": "2015-08-11T01:22:33-04:00",
			"created": "2014-01-20T10:24:44-05:00"
		}
	]
}