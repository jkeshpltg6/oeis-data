{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A205965",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 205965,
			"data": "1,1,4,3,10,16,26,21,102,110,178,288,466,754,2440,987,3194,7752,8362,13530,43784,35422,57314,92736,225075,242786,785672,635622,1028458,3328160,2692538,2178309,14098312,11405774,36909860,44791056,48315634,78176338,252983944",
			"name": "a(n) = Fibonacci(n)*A001227(n) for n\u003e=1, where A001227(n) is the number of odd divisors of n.",
			"comment": [
				"Compare g.f. to the Lambert series of A001227: Sum_{n\u003e=1} x^(2*n-1)/(1 - x^(2*n-1))."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A205965/b205965.txt\"\u003eTable of n, a(n) for n = 1..2500\u003c/a\u003e"
			],
			"formula": [
				"G.f.: Sum_{n\u003e=1} Fibonacci(2*n-1)*x^(2*n-1)/(1 - Lucas(2*n-1)*x^(2*n-1)-x^(4*n-2))."
			],
			"example": [
				"G.f.: A(x) = x + x^2 + 4*x^3 + 3*x^4 + 10*x^5 + 16*x^6 + 26*x^7 + 21*x^8 +...",
				"where A(x) = 1*1*x + 1*1*x^2 + 2*2*x^3 + 3*1*x^4 + 5*2*x^5 + 8*2*x^6 + 13*2*x^7 + 21*1*x^8 +...+ Fibonacci(n)*A001227(n)*x^n +...",
				"The g.f. is also given by the identity:",
				"A(x) = 1*x/(1-x-x^2) + 2*x^3/(1-4*x^3-x^6) + 5*x^5/(1-11*x^5-x^10) + 13*x^7/(1-29*x^7-x^14) + 34*x^9/(1-76*x^9-x^18) + 89*x^11/(1-199*x^11-x^22) +...",
				"which involves odd-indexed Fibonacci and Lucas numbers."
			],
			"mathematica": [
				"A001227[n_]:= DivisorSum[n, Mod[#, 2] \u0026]; Table[A001227[n]*Fibonacci[n], {n, 1, 50}] (* _G. C. Greubel_, Jul 17 2018 *)"
			],
			"program": [
				"(PARI) {Lucas(n)=fibonacci(n-1)+fibonacci(n+1)}",
				"{a(n)=polcoeff(sum(m=1,n,fibonacci(2*m-1)*x^(2*m-1)/(1-Lucas(2*m-1)*x^(2*m-1)-x^(4*m-2)+x*O(x^n))),n)}",
				"for(n=1,40,print1(a(n),\", \"))",
				"(PARI) a(n) = fibonacci(n)*sumdiv(n, d, d%2); \\\\ _Michel Marcus_, Jul 18 2018"
			],
			"xref": [
				"Cf. A001227, A205964, A205966, A203847, A000204 (Lucas).",
				"Cf. A209445 (Pell variant)."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Paul D. Hanna_, Feb 03 2012",
			"references": 4,
			"revision": 22,
			"time": "2018-07-18T06:43:28-04:00",
			"created": "2012-02-04T07:37:18-05:00"
		}
	]
}