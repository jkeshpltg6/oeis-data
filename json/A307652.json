{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A307652",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 307652,
			"data": "8,12,40,52,72,88,136,160,216,244,320,356,408,448,544,592,704,756,888,948,1088,1156,1304,1376,1504,1584,1736,1820,1984,2076,2288,2384,2536,2640,2912,3024,3200,3316,3624,3748,3976,4104,4392,4528,4824,4968,5216,5364,5664,5820,6088,6248,6616",
			"name": "The number of grains of sand in the identity element for the sandpile group on an (n+1) X (n+1) square grid.",
			"comment": [
				"The Abelian sandpile model considers the behavior of grains of sand on a square grid when a square topples sand to its nearest neighbors when the number of grains in the square is greater than or equal to 4. Squares on the edge of the board lose sand from the grid when toppling thus a stable configuration for the grid will always occur after a finite number of topples. Starting with the maximal stable grid consisting of 3 grains of sand in all squares, adding sand to one or more squares, and then performing topple stabilization results in a set of recurrent configurations which form the elements of the sandpile group for the given grid size. This group includes one configuration which acts as the identity element for the group, i.e., adding the identity configuration to any chosen group element and then performing topple stabilization results in the chosen group element.",
				"This sequence {a(n)} is the number of sand grains in the identity element of the sandpile group on a square grid of size (n+1) X (n+1)."
			],
			"link": [
				"Scott R. Shannon, \u003ca href=\"/A307652/b307652.txt\"\u003eTable of n, a(n) for n = 1..500\u003c/a\u003e",
				"Yvan Le Borgne, Dominique Rossin, \u003ca href=\"https://doi.org/10.1016/S0012-365X(02)00347-3\"\u003eOn the identity of the sandpile group\u003c/a\u003e. Discrete Mathematics, 256 (2002) 775-790.",
				"Luis David Garcia-Puente and Brady Haran, \u003ca href=\"https://youtu.be/1MtEUErz7Gg\"\u003eSandpiles\u003c/a\u003e, Numberphile video, YouTube.com, Jan. 13, 2017.",
				"Alexander E. Holroyd et al., \u003ca href=\"https://arxiv.org/abs/0801.3306\"\u003eChip-firing and Rotor-Routing on Directed Graphs\u003c/a\u003e. arXiv:0801.3306v4 [math.CO], 2013.",
				"Scott R. Shannon, \u003ca href=\"/A307652/a307652.png\"\u003eIdentity for the 50x50 grid\u003c/a\u003e. For this, and other images, black=0, yellow=1, blue=2, red=3 grains.",
				"Scott R. Shannon, \u003ca href=\"/A307652/a307652_1.png\"\u003eIdentity for the 51x51 grid\u003c/a\u003e. This shows the crossed line pattern through the center of the grid which is typical of grids with odd numbered side lengths.",
				"Scott R. Shannon, \u003ca href=\"/A307652/a307652_2.png\"\u003eIdentity for the 1000x1000 grid\u003c/a\u003e.",
				"Scott R. Shannon, \u003ca href=\"/A307652/a307652_3.png\"\u003eIdentity for the 4000x4000 grid\u003c/a\u003e. This contains 37246680 grains.",
				"Scott R. Shannon, \u003ca href=\"/A307652/a307652_1.java.txt\"\u003eSimplified Java code for finding the identity element and the sequence a(n)\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Abelian_sandpile_model\"\u003eAbelian Sandpile Model\u003c/a\u003e."
			],
			"formula": [
				"Identity element = ([6n] - ([6n])*)* , where [6n] is the all 6's grid of size (n+1) X (n+1), and (x)* represents the topple stabilization of the grid x.",
				"The sequence is closely fitted by the quadratic a(n) ~ 2.32*n^2, where 2.32 corresponds to the approximate grains per square density of the identity element configurations."
			],
			"example": [
				"a(1) = 2 X 2 grid.",
				"       Identity: | 2 2 |",
				"                 | 2 2 | = 8 grains.",
				"a(2) = 3 X 3 grid.",
				"       Identity: | 2 1 2 |",
				"                 | 1 0 1 |",
				"                 | 2 1 2 | = 12 grains.",
				"a(3) = 4 X 4 grid.",
				"       Identity: | 2 3 3 2 |",
				"                 | 3 2 2 3 |",
				"                 | 3 2 2 3 |",
				"                 | 2 3 3 2 | = 40 grains.",
				"a(4) = 5 X 5 grid.",
				"       Identity: | 2 3 2 3 2 |",
				"                 | 3 2 1 2 3 |",
				"                 | 2 1 0 1 2 |",
				"                 | 3 2 1 2 3 |",
				"                 | 2 3 2 3 2 | = 52 grains."
			],
			"xref": [
				"Cf. A259013, A180230, A300006.",
				"Cf. A007341 (order of the sandpile group of the (n-1)X(n-1) grid graph)."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Scott R. Shannon_, Apr 20 2019",
			"references": 2,
			"revision": 63,
			"time": "2021-02-12T12:16:00-05:00",
			"created": "2019-04-22T10:09:51-04:00"
		}
	]
}