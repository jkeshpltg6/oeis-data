{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A334442",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 334442,
			"data": "1,2,1,1,3,1,2,1,1,1,4,1,3,2,2,1,1,2,1,1,1,1,5,1,4,2,3,1,1,3,1,2,2,1,1,1,2,1,1,1,1,1,6,1,5,2,4,3,3,1,1,4,1,2,3,2,2,2,1,1,1,3,1,1,2,2,1,1,1,1,2,1,1,1,1,1,1,7,1,6,2,5,3,4,1,1,5",
			"name": "Irregular triangle whose reversed rows are all integer partitions sorted first by sum, then by length, and finally reverse-lexicographically.",
			"comment": [
				"First differs from A036036 for reversed partitions of 9. Namely, this sequence has (2,2,5) before (1,4,4), while A036036 has (1,4,4) before (2,2,5)."
			],
			"link": [
				"Wikiversity, \u003ca href=\"https://en.wikiversity.org/wiki/Lexicographic_and_colexicographic_order\"\u003e Lexicographic and colexicographic order\u003c/a\u003e"
			],
			"example": [
				"The sequence of all partitions begins:",
				"  ()         (2,3)        (1,1,1,1,2)    (1,1,1,2,2)",
				"  (1)        (1,1,3)      (1,1,1,1,1,1)  (1,1,1,1,1,2)",
				"  (2)        (1,2,2)      (7)            (1,1,1,1,1,1,1)",
				"  (1,1)      (1,1,1,2)    (1,6)          (8)",
				"  (3)        (1,1,1,1,1)  (2,5)          (1,7)",
				"  (1,2)      (6)          (3,4)          (2,6)",
				"  (1,1,1)    (1,5)        (1,1,5)        (3,5)",
				"  (4)        (2,4)        (1,2,4)        (4,4)",
				"  (1,3)      (3,3)        (1,3,3)        (1,1,6)",
				"  (2,2)      (1,1,4)      (2,2,3)        (1,2,5)",
				"  (1,1,2)    (1,2,3)      (1,1,1,4)      (1,3,4)",
				"  (1,1,1,1)  (2,2,2)      (1,1,2,3)      (2,2,4)",
				"  (5)        (1,1,1,3)    (1,2,2,2)      (2,3,3)",
				"  (1,4)      (1,1,2,2)    (1,1,1,1,3)    (1,1,1,5)",
				"This sequence can also be interpreted as the following triangle:",
				"                  0",
				"                 (1)",
				"               (2)(11)",
				"             (3)(12)(111)",
				"        (4)(13)(22)(112)(1111)",
				"  (5)(14)(23)(113)(122)(1112)(11111)",
				"Taking Heinz numbers (A334438) gives:",
				"   1",
				"   2",
				"   3   4",
				"   5   6   8",
				"   7  10   9  12  16",
				"  11  14  15  20  18  24  32",
				"  13  22  21  25  28  30  27  40  36  48  64",
				"  17  26  33  35  44  42  50  45  56  60  54  80  72  96 128"
			],
			"mathematica": [
				"revlensort[f_,c_]:=If[Length[f]!=Length[c],Length[f]\u003cLength[c],OrderedQ[{c,f}]];",
				"Join@@Reverse/@Join@@Table[Sort[IntegerPartitions[n],revlensort],{n,0,8}]"
			],
			"program": [
				"(PARI) A334442_row(n)=vecsort(partitions(n),p-\u003econcat(#p,-Vecrev(p))) \\\\ Rows of triangle defined in EXAMPLE (all partitions of n). Wrap into [Vec(p)|p\u003c-...] to avoid \"Vecsmall\". - _M. F. Hasler_, May 14 2020"
			],
			"xref": [
				"Row lengths are A036043.",
				"The version for reversed partitions is A334301.",
				"The version for colex instead of revlex is A334302.",
				"Taking Heinz numbers gives A334438.",
				"The version with rows reversed is A334439.",
				"Ignoring length gives A335122.",
				"Lexicographically ordered reversed partitions are A026791.",
				"Reversed partitions in Abramowitz-Stegun (sum/length/lex) order are A036036.",
				"Partitions in increasing-length colex order (sum/length/colex) are A036037.",
				"Reverse-lexicographically ordered partitions are A080577.",
				"Lexicographically ordered partitions are A193073.",
				"Partitions in colexicographic order (sum/colex) are A211992.",
				"Sorting partitions by Heinz number gives A296150.",
				"Cf. A026791, A112798, A124734, A129129, A185974, A228100, A228531, A296774, A334433, A334435, A334436."
			],
			"keyword": "nonn,tabf",
			"offset": "0,2",
			"author": "_Gus Wiseman_, May 07 2020",
			"references": 19,
			"revision": 17,
			"time": "2020-05-31T07:02:43-04:00",
			"created": "2020-05-10T19:27:21-04:00"
		}
	]
}