{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002314",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2314,
			"id": "M1314 N0503",
			"data": "2,5,4,12,6,9,23,11,27,34,22,10,33,15,37,44,28,80,19,81,14,107,89,64,16,82,60,53,138,25,114,148,136,42,104,115,63,20,143,29,179,67,109,48,208,235,52,118,86,24,77,125,35,194,154,149,106,58,26,135,96,353,87,39",
			"name": "Minimal integer square root of -1 modulo p, where p is the n-th prime of the form 4k+1.",
			"comment": [
				"In other words, if p is the n-th prime == 1 (mod 4), a(n) is the smallest positive integer k such that k^2 + 1 == 0 (mod p).",
				"The 4th roots of unity mod p, where p = n-th prime == 1 (mod 4), are +1, -1, a(n) and p-a(n).",
				"Related to Stormer numbers.",
				"Comment from Igor Shparlinski, Mar 12 2007 (writing to the Number Theory List): Results about the distribution of roots (for arbitrary quadratic polynomials) are given by W. Duke, J. B. Friedlander and H. Iwaniec and A. Toth.",
				"Comment from Emmanuel Kowalski, Mar 12 2007 (writing to the Number Theory List): It is known (Duke, Friedlander, Iwaniec, Annals of Math. 141 (1995)) that the fractional part of a(n)/p(n) is equidistributed in [0,1/2] for p(n)\u003cX and X going to infinity. So a positive proportion of p have a between xp and yp for 0\u003cx\u003cy\u003c1/2, but equidistribution in smaller sets is not known.",
				"From _Artur Jasinski_, Dec 10 2008: (Start)",
				"If we take the four numbers 1, A002314(n), A152676(n), and A152680(n), then their multiplication table modulo A002144(n) is isomorphic to the Latin square",
				"    1 2 3 4",
				"    2 4 1 3",
				"    3 1 4 2",
				"    4 3 2 1",
				"and isomorphic to the multiplication table of {1, i, -i, -1} where i=sqrt(-1), A152680(n) is isomorphic to -1, A002314(n) with i or -i and A152676(n) vice versa -i or i.",
				"1, A002314(n), A152676(n), A152680(n) are subfield of Galois Field [A002144(n)]. (End)",
				"It is found empirically that the solutions of the Diophantine equation X^4 + Y^2 == 0 (mod P) (where P is a prime of the form P=4k+1) are integer points on parabolas Y = (+-(X^2 - P*X) + P*i)/C(P) where C(P) is the term corresponding to a prime P in this sequence. - _Seppo Mustonen_, Sep 22 2020"
			],
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Jon E. Schoenfield, \u003ca href=\"/A002314/b002314.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (first 1000 terms from T. D. Noe)",
				"W. Duke, J. B. Friedlander and H. Iwaniec, \u003ca href=\"https://www.jstor.org/stable/2118527\"\u003eEquidistribution of roots of a quadratic congruence to prime moduli\u003c/a\u003e, Annals of Math, 141 (1995), 423-441.",
				"Seppo Mustonen, \u003ca href=\"https://www.survo.fi/demos/index.html#ex138\"\u003eRoots of Diophantine equations mod(X^4+Y^2,P)=0\u003c/a\u003e, 2020",
				"Seppo Mustonen, \u003ca href=\"https://youtu.be/FS1dGHf5RPk\"\u003eRoots of Diophantine equations mod(X^4+Y^2,P)=0\u003c/a\u003e, Youtube video, 2020",
				"J. Todd, \u003ca href=\"https://www.jstor.org/stable/2305526\"\u003eA problem on arc tangent relations\u003c/a\u003e, Amer. Math. Monthly, 56 (1949), 517-528.",
				"A. Toth, \u003ca href=\"https://doi.org/10.1155/S1073792800000404\"\u003eRoots of quadratic congruences\u003c/a\u003e, Intern. Math. Research Notices, 2000 (2000), 719-739."
			],
			"maple": [
				"f:=proc(n) local i,j,k; for i from 1 to (n-1)/2 do if i^2 +1 mod n = 0 then RETURN(i); fi od: -1; end;",
				"t1:=[]; M:=40; for n from 1 to M do q:=ithprime(n); if q mod 4 = 1 then t1:=[op(t1),f(q)]; fi; od: t1;"
			],
			"mathematica": [
				"aa = {}; Do[If[Mod[Prime[n], 4] == 1, k = 1; While[ ! Mod[k^2 + 1, Prime[n]] == 0, k++ ]; AppendTo[aa, k]], {n, 1, 100}]; aa (* _Artur Jasinski_, Dec 10 2008 *)"
			],
			"program": [
				"(PARI) first_N_terms(N) = my(v=vector(N), i=0); forprime(p=5, oo, if(p%4==1, i++; v[i] = lift(sqrt(Mod(-1,p)))); if(i==N, break())); v \\\\ _Jianing Song_, Apr 17 2021"
			],
			"xref": [
				"Cf. A002313, A005528, A047818, A002144, A152676, A152680. Subsequence of A057756."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Better description from Tony Davie (ad(AT)dcs.st-and.ac.uk), Feb 07 2001",
				"More terms from _Jud McCranie_, Mar 18 2001"
			],
			"references": 18,
			"revision": 50,
			"time": "2021-04-17T12:00:48-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}