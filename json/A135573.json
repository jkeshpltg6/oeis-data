{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A135573",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 135573,
			"data": "1,3,1,10,2,2,35,5,3,5,126,14,6,6,14,462,42,14,10,14,42,1716,132,36,20,20,36,132,6435,429,99,45,35,45,99,429,24310,1430,286,110,70,70,110,286,1430,92378,4862,858,286,154,126,154,286,858,4862",
			"name": "Array T(n,m) of super ballot numbers read along ascending antidiagonals.",
			"comment": [
				"First row is A000108. 2nd row is A007054. 3rd row and 4th column are essentially A007272.",
				"1st column is A001700. 2nd column is essentially A000108. 3rd column is A007054.",
				"Main diagonal is A000984."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A135573/b135573.txt\"\u003eTable of n, a(n) for the first 50 antidiagonals\u003c/a\u003e",
				"E. Allen and I. Gheorghiciuc, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Allen/gheo.html\"\u003eA Weighted Interpretation for the Super Catalan Numbers\u003c/a\u003e, J. Int. Seq. 17 (2014) # 14.10.7, Table 1.",
				"Ira M. Gessel, \u003ca href=\"http://dx.doi.org/10.1016/0747-7171(92)90034-2\"\u003eSuper ballot numbers\u003c/a\u003e, J. Symb. Comput. vol 14, iss 2-3 (1992) pp 179-194."
			],
			"formula": [
				"T(n, m) = (2*n + 1)!*(2*m)! / (n!*m!*(m + n + 1)!).",
				"From _Peter Luschny_, Nov 03 2021: (Start)",
				"T(n, m) = (1/(2*Pi))*Integral_{x=0..4} x^m*(4 - x)^(n + 1/2)*x^(-1/2). These are integral representations of the n-th moment of a positive function on [0, 4]. The representations are unique.",
				"T(n, m) = 4^(m + n)*hypergeom([1/2 + n, 1/2 - m], [3/2 + n], 1)/((2*n + 1)*Pi).",
				"For fixed n and m -\u003e oo: T(n, m) ~ (1/(2*Pi))*4^(n + m + 1)*(Gamma(3/2 + n) / m^(3/2 + n))*(1 - (2*n + 3)^2 / (8*m)) . (End)",
				"T(n, m) = (-1)^m*4^(n + 1 + m)*binomial(n + 1/2, n + 1 + m)/2. - _Peter Luschny_, Nov 04 2021"
			],
			"example": [
				"Array with rows n \u003e= 0 and columns m \u003e= 0 starts:",
				"[n\\m]  0    1    2    3    4    5    6     7     8  ...",
				"-------------------------------------------------------",
				"[0]    1    1    2    5   14   42  132   429  1430  ...  [A000108]",
				"[1]    3    2    3    6   14   36   99   286   858  ...  [A007054]",
				"[2]   10    5    6   10   20   45  110   286   780  ...  [A007272]",
				"[3]   35   14   14   20   35   70  154   364   910  ...  [A348893]",
				"[4]  126   42   36   45   70  126  252   546  1260  ...  [A348898]",
				"[5]  462  132   99  110  154  252  462   924  1980  ...  [A348899]",
				"[6] 1716  429  286  286  364  546  924  1716  3432  ...",
				"...",
				"Seen as a triangle:",
				"[0] 1;",
				"[1] 3,    1;",
				"[2] 10,   2,   2;",
				"[3] 35,   5,   3,  5;",
				"[4] 126,  14,  6,  6,  14;",
				"[5] 462,  42,  14, 10, 14, 42;",
				"[6] 1716, 132, 36, 20, 20, 36, 132;",
				"[7] 6435, 429, 99, 45, 35, 45, 99,  429.",
				".",
				"T(20, 100000) = 2.442634...*10^60129. Asymptotic formula: 2.442627..*10^60129."
			],
			"maple": [
				"T := proc(n,m) (2*n+1)!/n!*(2*m)!/m!/(m+n+1)! ; end proc:",
				"for d from 0 to 12 do for c from 0 to d do printf(\"%d, \",T(d-c,c)) ; od: od:",
				"# Alternatively, printed as rows:",
				"A135573 := (n, m) -\u003e (1/(2*Pi))*int(x^m*(4-x)^(n+1/2)*x^(-1/2), x=0..4):",
				"for n from 0 to 9 do seq(A135573(n, m), m = 0..9) od; # _Peter Luschny_, Nov 03 2021"
			],
			"mathematica": [
				"T[n_, m_] := (2*n+1)!/n!*(2*m)!/m!/(m+n+1)!; Table[T[n-m, m], {n, 0, 12}, {m, 0, n}] // Flatten (* _Jean-François Alcover_, Jan 06 2014, after Maple *)",
				"T[n_, m_] := 4^(m+n) Hypergeometric2F1[1/2+n, 1/2-m, 3/2+n, 1] / ((2 n + 1) Pi);",
				"Table[T[n - m + 1, m], {n, 0, 9}, {m, 0, n}] // Flatten (* _Peter Luschny_, Nov 03 2021 *)"
			],
			"program": [
				"(Sage)",
				"def T(n, m): return (-1)^m*4^(n + 1 + m)*binomial(n + 1/2, n + 1 + m)/2",
				"for n in range(7): print([T(n, m) for m in range(9)]) # _Peter Luschny_, Nov 04 2021"
			],
			"xref": [
				"Cf. A000108, A007054, A000984, A348893, A348898, A348899.",
				"Cf. A000984 (main diagonal), A001700 (column 0), A082590 (sum of antidiagonals)."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "0,2",
			"author": "_R. J. Mathar_, Feb 23 2008",
			"references": 6,
			"revision": 31,
			"time": "2021-11-04T15:51:33-04:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}