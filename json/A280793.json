{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A280793",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 280793,
			"data": "1,-4,1616,-10233664,605781862656,-195074044306023424,226963189334487889924096,-745095268828143694162593398784,5876637899238904537105181354518183936,-99252790021186158091252679600581668608671744,3289325814605557759161838756845047127645003816370176,-199648823584758446510667095055905800597628128606583525474304",
			"name": "E.g.f. A(x) satisfies: A( arctan( A( arctanh(x) ) ) ) = x.",
			"comment": [
				"The series reversion of the e.g.f. is defined by A280791."
			],
			"link": [
				"Paul D. Hanna, \u003ca href=\"/A280793/b280793.txt\"\u003eTable of n, a(n) for n = 1..50\u003c/a\u003e"
			],
			"formula": [
				"E.g.f. A(x) = Sum_{n\u003e=1} a(n) * x^(4*n-3)/(4*n-3)! satisfies:",
				"(1) A( arctan( A( arctanh(x) ) ) ) = x.",
				"(2) A( arctanh( A( arctan(x) ) ) ) = x.",
				"(3) arctan( A( arctanh( A(x) ) ) ) = x.",
				"(4) arctanh( A( arctan( A(x) ) ) ) = x.",
				"(5) A( arctanh(A(x)) ) = tan(x).",
				"(6) A( arctan(A(x)) ) = tanh(x).",
				"(7) Series_Reversion( A(x) ) = arctan( A(arctanh(x)) ) = arctanh( A(arctan(x)) )."
			],
			"example": [
				"E.g.f.: A(x) = x - 4*x^5/5! + 1616*x^9/9! - 10233664*x^13/13! + 605781862656*x^17/17! - 195074044306023424*x^21/21! + 226963189334487889924096*x^25/25! - 745095268828143694162593398784*x^29/29! + 5876637899238904537105181354518183936*x^33/33! - 99252790021186158091252679600581668608671744*x^37/37! + 3289325814605557759161838756845047127645003816370176*x^41/41! + ...",
				"such that A( arctan( A( arctanh(x) ) ) ) = x.",
				"Note that A( A( arctan( arctanh(x) ) ) ) is NOT equal to x; the composition of these functions is not commutative.",
				"The e.g.f. as a series with reduced fractional coefficients begins:",
				"A(x) = x - 1/30*x^5 + 101/22680*x^9 - 22843/13899600*x^13 + 788778467/463134672000*x^17 - 190501996392601/49893498214560000*x^21 + 55410934896115207501/3786916514485104000000*x^25 - 15159002051353834923555367/179886108271071410208000000*x^29 + ...",
				"RELATED SERIES.",
				"A( arctanh(x) ) = x + 2*x^3/3! + 20*x^5/5! + 440*x^7/7! + 16400*x^9/9! + 944800*x^11/11! + 82388800*x^13/13! + 9583600000*x^15/15! + 1041175200000*x^17/17! + 136472188736000*x^19/19! + 168221708270720000*x^21/21! + 77192574087699200000*x^23/23! - 152078345729585600000000*x^25/25! + ...",
				"The series reversion of A( arctanh(x) ) equals A( arctan(x) ), which begins:",
				"A( arctan(x) ) = x - 2*x^3/3! + 20*x^5/5! - 440*x^7/7! + 16400*x^9/9! - 944800*x^11/11! + 82388800*x^13/13! - 9583600000*x^15/15! + ...",
				"arctanh( A(x) ) = x + 2*x^3/3! + 20*x^5/5! + 552*x^7/7! + 29840*x^9/9! + 2520352*x^11/11! + 302768960*x^13/13! + 51218036352*x^15/15! + 12015036698880*x^17/17! + 3457794697175552*x^19/19! + 1042442536703513600*x^21/21! + 437297928076611069952*x^23/23! + 444983819928674567557120*x^25/25! + ...",
				"The series reversion of arctanh( A(x) ) equals arctan( A(x) ), which begins:",
				"arctan( A(x) ) = x - 2*x^3/3! + 20*x^5/5! - 552*x^7/7! + 29840*x^9/9! - 2520352*x^11/11! + 302768960*x^13/13! - 51218036352*x^15/15! + ...",
				"The series reversion of A(x) begins:",
				"Series_Reversion( A(x) ) = x + 4*x^5/5! + 400*x^9/9! + 5364800*x^13/13! - 367374176000*x^17/17! + 143449000888960000*x^21/21! - 181899009894595069440000*x^25/25! +...+ A280791(n)*x^(4*n-3)/(4*n-3)! + ..."
			],
			"program": [
				"(PARI) {a(n) = my(A=x +x*O(x^(4*n+1))); for(i=1,2*n, A = A + (x - subst( atan(A) ,x, atanh(A) ) )/2; ); (4*n-3)!*polcoeff(A,4*n-3)}",
				"for(n=1,20,print1(a(n),\", \"))"
			],
			"xref": [
				"Cf. A280790, A280791, A280792."
			],
			"keyword": "sign",
			"offset": "1,2",
			"author": "_Paul D. Hanna_, Jan 09 2017",
			"references": 6,
			"revision": 15,
			"time": "2019-02-21T08:21:23-05:00",
			"created": "2017-01-09T17:21:41-05:00"
		}
	]
}