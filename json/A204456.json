{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A204456",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 204456,
			"data": "1,1,1,4,1,1,2,4,2,1,1,2,2,4,2,2,1,1,2,2,2,2,4,2,2,2,2,1,1,2,2,2,2,2,4,2,2,2,2,2,1,1,2,2,2,2,2,2,2,4,2,2,2,2,2,2,2,1,1,2,2,2,2,2,2,2,2,4,2,2,2,2,2,2,2,2,1,1,2,2,2,2,2,2,2,2,2,2,4,2,2,2,2,2,2,2,2,2,2,1",
			"name": "Coefficient array of numerator polynomials of the o.g.f.s for the sequence of odd numbers not divisible by a given prime.",
			"comment": [
				"The row length sequence of this array is p(m) = A000040(m) (the primes).",
				"Row m, for m \u003e= 1, lists the coefficients of the numerator polynomials N(p(m);x) = Sum_{k=0..p(m)-1} a(m,k)*x^k for the o.g.f. G(p(m);x) = x*N(p(m);x)/((1-x^(p(m)-1))*(1-x)) for the sequence a(p(m);n) of odd numbers not divisible by p(n). For m=1 one has a(2;n)=2*n-1, n \u003e= 1, and for m \u003e 1 one has a(p(m);n) = 2*n+1 + floor((n-(p(m)+1)/2)/(p(m)-1)), n \u003e= 1, and a(p(m);0):=0. See A204454 for the m=5 sequence a(11;n), also for more details.",
				"The rows of this array are symmetric. For m \u003e 1 they are symmetric around the central 4.",
				"The first (p(m)+1)/2 numbers of row number m, for m \u003e= 2, are given by the first differences of the corresponding sequence {a(p(m);n)}, with a(p(m),0):=0. See a formula below. The proof is trivial for m=1, and clear for m \u003e= 2 from a(p(m);n), for n=0,...,(p(m)+1)/2, which is {0,1,3,...,p-2,p+2}. - _Wolfdieter Lang_, Jan 26 2012"
			],
			"formula": [
				"a(m,k) = [x^k]N(p(m);x), m\u003e=1, k=0,...,p(m)-1, with the numerator polynomial N(p(m);x) for the o.g.f. G(p(m);x) of the sequence of odd numbers not divisible by the m-th prime p(m)=A000040(m). See the comment above.",
				"Row m has the number pattern (exponents on a number indicate how many times this number appears consecutively):",
				"  m=1, p(1)=2: 1 1, and for m\u003e=2:",
				"  m, p(m): 1 2^((p(m)-3)/2) 4 2^((p(m)-3)/2) 1.",
				"a(m,k) = a(p(m);k+1) - a(p(m);k), m\u003e=2, k=0,...,(p(m)-1)/2,",
				"with the corresponding sequence {a(p(m);n)} of the odd numbers not divisible by p(m), with a(p(m);0):=0. For m=1: a(1,0) = a(2;1)-a(2;0). By symmetry around the center: a(m,(p(m)-1)/2+k) = a(m,(p(m)-1)/2-k), k=1,...,(p(m)-1)/2, m\u003e=2. For m=1: a(1,1)=a(1,0). See a comment above. - _Wolfdieter Lang_, Jan 26 2012"
			],
			"example": [
				"The array starts",
				"m,p\\k  0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 ...",
				"1,2:   1  1",
				"2,3:   1  4  1",
				"3,5:   1  2  4  2  1",
				"4,7:   1  2  2  4  2  2  1",
				"5,11:  1  2  2  2  2  4  2  2  2  2  1",
				"6,13:  1  2  2  2  2  2  4  2  2  2  2  2  1",
				"7,17:  1  2  2  2  2  2  2  2  4  2  2  2  2  2  2  2  1",
				"...",
				"N(p(4);x) = N(7;x) = 1 + 2*x + 2*x^2 + 4*x^3 + 2*x^4 + 2*x^5 + x^6 = (1+x^2)*(1+2*x+x^2+2*x^3+x^4).",
				"G(p(4);x) = G(7;x) = x*N(7;x)/((1-x^6)*(1-x)), the o.g.f. of",
				"A162699. Compare this with the o.g.f. given there by _R. J. Mathar_, where the numerator is factorized also.",
				"First difference rule: m=4: {a(7;n)} starts {0,1,3,5,9,...},",
				"the first differences are {1,2,2,4,...}, giving the first (7+1)/2=4 entries of row number m=4 of the array. The other entries follow by symmetry. - _Wolfdieter Lang_, Jan 26 2012"
			],
			"xref": [
				"Cf. A000040, A005408 (p=2), A007310 (p=3), A045572 (p=5), A162699 (p=7), A204454 (p=11)."
			],
			"keyword": "nonn,easy,tabf",
			"offset": "1,4",
			"author": "_Wolfdieter Lang_, Jan 24 2012",
			"references": 3,
			"revision": 17,
			"time": "2018-09-01T21:30:05-04:00",
			"created": "2012-01-25T13:40:19-05:00"
		}
	]
}