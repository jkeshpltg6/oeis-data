{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A291198",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 291198,
			"data": "1,1,-4,44,-704,14208,-338304,9165696,-276371712,9139825152,-328127311872,12691848990720,-525849764782080,23229272756912128,-1089877362984222720,54133294598753206272,-2838256094009499844608,156685554517473518682112,-9086394132461874613059584,552356609566876038974144512,-35128905989147663752065187840,2333138175889736609287142113280",
			"name": "G.f. A(x) satisfies: A( 3*x - 2*A(x) ) = x - x^2.",
			"comment": [
				"For what fixed integers {a, b, c, d} does there exist a formal power series G(x) with constant term 0 that satisfies G( a*x + b*G(x) ) = c*x + d*x^2 and consists solely of integer coefficients?",
				"Necessary condition: there is an integer g_1 with b*g_1^2 + a*g_1 - c = 0, and (a+1)*b*g[1]+a^2+b*c divides d. - _Robert Israel_, Feb 01 2018"
			],
			"link": [
				"Paul D. Hanna, \u003ca href=\"/A291198/b291198.txt\"\u003eTable of n, a(n) for n = 1..300\u003c/a\u003e"
			],
			"formula": [
				"G.f. A(x) satisfies: A( 3*C(x) - 2*A(C(x)) ) = x where C(x) = x + C(x)^2 is a g.f. of the Catalan numbers (A000108).",
				"a(n) ~ (-1)^n * c * 2^n * n^(log(2)) * n! / (log(2))^n, where c = 0.023113811466383650559... - _Vaclav Kotesovec_, Aug 28 2017"
			],
			"example": [
				"G.f.: A(x) = x + x^2 - 4*x^3 + 44*x^4 - 704*x^5 + 14208*x^6 - 338304*x^7 + 9165696*x^8 - 276371712*x^9 + 9139825152*x^10 - 328127311872*x^11 + 12691848990720*x^12 - 525849764782080*x^13 + 23229272756912128*x^14 - 1089877362984222720*x^15 + 54133294598753206272*x^16 +...",
				"such that A( 3*x - 2*A(x) ) = x - x^2.",
				"RELATED SERIES.",
				"Define Ai(x) such that Ai(A(x)) = x, then Ai(x) begins:",
				"Ai(x) = x - x^2 + 6*x^3 - 69*x^4 + 1114*x^5 - 22426*x^6 + 530636*x^7 - 14269341*x^8 + 426991250*x^9 - 14018277742*x^10 + 499882371412*x^11 - 19217292112866*x^12 + 791861696758020*x^13 - 34810523545942356*x^14 + 1626245849943319704*x^15 - 80468924895927327693*x^16 +...",
				"where Ai(x - x^2) = 3*x - 2*A(x).",
				"Thus Ai(x) = 3*C(x) - 2*A(C(x)), where C(x) = x + C(x)^2."
			],
			"maple": [
				"eq:=A(3*x-2*A(x))+x^2-x:",
				"An:= x:",
				"for iter from 1 to 6 do",
				"  R:= map(normal,series(eval(eq, A = unapply(An + add(a[i]*x^i,i=2^(iter-1)+1..2^iter),x)),x,2^iter+1));",
				"  S:= solve({seq(coeff(R,x,i),i=2^(iter-1)..2^iter)});",
				"  An:= subs(S, An + add(a[i]*x^i,i=2^(iter-1)+1..2^iter))",
				"od:",
				"seq(coeff(An,x,j),j=1..2^6); # _Robert Israel_, Feb 01 2018"
			],
			"mathematica": [
				"nmax = 22; sol = {a[1] -\u003e 1};",
				"Do[A[x_] = Sum[a[k] x^k, {k, 1, n}] /. sol; eq = CoefficientList[A[3x - 2 A[x]] - x + x^2 + O[x]^(n+1), x] == 0 /. sol; sol = sol ~Join~ Solve[eq] [[1]], {n, 1, nmax}];",
				"sol /. Rule -\u003e Set;",
				"a /@ Range[1, nmax] (* _Jean-François Alcover_, Nov 01 2019 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A=x,V=[1,1]); for(i=1,n, V = concat(V,0); A=x*Ser(V); V[#V] = Vec( subst(A,x, 3*x - 2*A) )[#V] );V[n]}",
				"for(n=1,30,print1(a(n),\", \"))"
			],
			"xref": [
				"Cf. A292812, A292813, A292814, A293454, A293455, A293456."
			],
			"keyword": "sign",
			"offset": "1,3",
			"author": "_Paul D. Hanna_, Aug 20 2017",
			"references": 8,
			"revision": 27,
			"time": "2019-11-01T11:47:54-04:00",
			"created": "2017-08-20T16:17:32-04:00"
		}
	]
}