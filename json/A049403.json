{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A049403",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 49403,
			"data": "1,1,1,0,3,1,0,3,6,1,0,0,15,10,1,0,0,15,45,15,1,0,0,0,105,105,21,1,0,0,0,105,420,210,28,1,0,0,0,0,945,1260,378,36,1,0,0,0,0,945,4725,3150,630,45,1,0,0,0,0,0,10395,17325,6930,990,55,1,0,0,0,0,0,10395,62370",
			"name": "A triangle of numbers related to triangle A030528; array a(n,m), read by rows (1 \u003c= m \u003c= n).",
			"comment": [
				"a(n,1) = A019590(n) = A008279(1,n). a(n,m) =: S1(-1; n,m), a member of a sequence of lower triangular Jabotinsky matrices, including S1(1; n,m) = A008275 (signed Stirling first kind), S1(2; n,m) = A008297(n,m) (signed Lah numbers). a(n,m) matrix is inverse to signed matrix ((-1)^(n-m))*A001497(n-1,m-1) (signed Bessel triangle). The monic row polynomials E(n,x) := Sum_{m=1..n} a(n,m)*x^m, E(0,x) := 1 are exponential convolution polynomials (see A039692 for the definition and a Knuth reference).",
				"Exponential Riordan array [1+x, x(1+x/2)]. T(n,k) = A001498(k+1, n-k). - _Paul Barry_, Jan 15 2009"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A049403/b049403.txt\"\u003eTable of n, a(n) for the first 50 rows, flattened\u003c/a\u003e",
				"Wolfdieter Lang, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL3/LANG/lang.html\"\u003eOn generalizations of Stirling number triangles\u003c/a\u003e, J. Integer Seqs., Vol. 3 (2000), #00.2.4.",
				"Wolfdieter Lang, \u003ca href=\"/A049403/a049403.txt\"\u003eFirst 10 rows of the array and more.\u003c/a\u003e"
			],
			"formula": [
				"a(n, m) = n!*A030528(n, m)/(m!*2^(n-m)) for n \u003e= m \u003e= 1.",
				"a(n, m) = (2*m-n+1)*a(n-1, m) + a(n-1, m-1) for n \u003e= m \u003e= 1 with a(n, m) = 0 for n \u003c m, a(n, 0) := 0, and a(1, 1) = 1. [The 0th column does not appear in this array. - _Petros Hadjicostas_, Oct 28 2019]",
				"E.g.f. for the m-th column: (x*(1 + x/2))^m/m!.",
				"a(n,m) = A122848(n,m). - _R. J. Mathar_, Jan 14 2011"
			],
			"example": [
				"Triangle a(n,m) (with rows n \u003e= 1 and columns m \u003e= 1) begins as follows:",
				"  1;                 with row polynomial E(1,x) = x;",
				"  1, 1;              with row polynomial E(2,x) = x^2 + x;",
				"  0, 3,  1;          with row polynomial E(3,x) = 3*x^2 + x^3;",
				"  0, 3,  6,   1;     with row polynomial E(4,x) = 3*x^2 + 6*x^3 + x^4;",
				"  0, 0, 15,  10,   1;",
				"  0, 0, 15,  45,  15,   1;",
				"  0, 0,  0, 105, 105,  21,  1;",
				"  0, 0,  0, 105, 420, 210, 28, 1;",
				"  ..."
			],
			"maple": [
				"# The function BellMatrix is defined in A264428.",
				"# Adds (1,0,0,0, ..) as column 0.",
				"BellMatrix(n -\u003e `if`(n\u003c2,1,0), 9); # _Peter Luschny_, Jan 28 2016"
			],
			"mathematica": [
				"t[n_, k_] := k!*Binomial[n, k]/((2 k - n)!*2^(n - k)); Table[ t[n, k], {n, 11}, {k, n}] // Flatten",
				"(* Second program: *)",
				"BellMatrix[f_Function, len_] := With[{t = Array[f, len, 0]}, Table[BellY[n, k, t], {n, 0, len-1}, {k, 0, len-1}]];",
				"rows = 13;",
				"M = BellMatrix[If[#\u003c2, 1, 0]\u0026, rows];",
				"Table[M[[n, k]], {n, 2, rows}, {k, 2, n}] // Flatten (* _Jean-François Alcover_, Jun 23 2018, after _Peter Luschny_ *)"
			],
			"xref": [
				"Cf. A000085 (row sums), A001497, A001498, A008275, A008279, A008297, A019590, A030528, A039692.",
				"Variations of this array: A096713, A104556, A122848, A130757."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "1,5",
			"author": "_Wolfdieter Lang_",
			"references": 11,
			"revision": 48,
			"time": "2019-10-29T06:04:32-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}