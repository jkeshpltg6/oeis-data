{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A067764",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 67764,
			"data": "1,1,3,13,73,167,4051,37633,43817,4596553,58941091,274691047,12470162233,202976401213,1178339174801,65573803186921,99264170666917,994319127823939,588633468315403843,13564373693588558173,109232642628695218147,752832094524169066031",
			"name": "Numerators of the coefficients in exp(x/(1-x)) power series.",
			"comment": [
				"Define c(n) = a(n)/A067653(n). For a given sequence s(n) consider P[s(n)](z) := e^(-z/(1-z))*Sum_{k\u003e=0} s(k)c(k)z^k. Regarding complex-valued abelian limitation the following holds true: if s(n) is convergent (to the limit s) then lim_{z-\u003e+1} P[s(n)](z)=s in a certain sub-domain D of the unit circle. There are two constraints: (1) D contains the line [0,1[. (2) There is a d \u003e 0 such that the intersection of {w|Re(w) \u003e 1-d} and D is a nonempty subset of a generalized Stolz set defined by {w||Im(w)| \u003c= t*(1 - Re(w))^(3/2)}, t \u003c 1. If z tends to +1 from outside such a domain, that limit doesn't exist in general. - _Hieronymus Fischer_, Oct 20 2010",
				"The ratio sequence given by c(n) = a(n)/A067653(n) also occurs in certain row and column sums related to Pascal's triangle, as in the two formulas given below. - _Richard R. Forberg_, Dec 26 2013"
			],
			"reference": [
				"O. Perron, Über das infinitäre Verhalten der Koeffizienten einer gewissen Potenzreihe, Archiv d. Math. u. Phys. (3), Vol. 22, pp. 329-340, 1914.",
				"H. Fischer, Eine Theorie komplexwertiger Abelscher Limitierungsmethoden (A theory of complex valued abelian limitation methods), Dissertation (1987), pp. 29-32.",
				"K. Zeller, W. Beekmann, Theorie der Limitierungsverfahren, Springer-Verlag, Berlin (1970)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A067764/b067764.txt\"\u003eTable of n, a(n) for n = 0..445\u003c/a\u003e",
				"Richard P. Brent, M. L. Glasser, Anthony J. Guttmann, \u003ca href=\"https://arxiv.org/abs/1812.00316\"\u003eA Conjectured Integer Sequence Arising From the Exponential Integral\u003c/a\u003e, arXiv:1812.00316 [math.NT], 2018.",
				"D. Borwein, \u003ca href=\"http://dx.doi.org/10.1017/S0080454100007561\"\u003eOn methods of summability based on power series\u003c/a\u003e, Proc. Royal Soc. Edinburgh, Sect. A, Vol. 64(04), Jan. 1957, pp 342-349.",
				"K. Knopp, \u003ca href=\"https://archive.org/details/theoryandapplica031692mbp/page/n561\"\u003eTheory and Application of Infinite Series\u003c/a\u003e, Dover, 1954, p. 547."
			],
			"formula": [
				"a(n) is the numerator of Sum_{i=1..n} binomial(n-1, i-1)/i!.",
				"a(n) is also the numerator of (Sum_{m\u003e=0} binomial(n+m-1,n)/m!)/e, with A067653(n) as the denominator. See as example A000332 = binomial(n,4) below. - _Richard R. Forberg_, Dec 26 2013",
				"a(n) = numerator(hypergeom([1 - n], [2], -1)) for n \u003e 0. - _Peter Luschny_, Feb 02 2019"
			],
			"example": [
				"Example for first formula. 1/1! + 3/2! + 3/3! + 1/4! = 73/24.",
				"Example for 2nd formula. A000332 = 0, 0, 0, 0, 1, 5, 15, 35, 70, 126, ...; a(4) = 0/0! + 1/1! + 5/2! + 15/3! + 35/4! + 70/5! + 126/6! + ... = 73*e/24.",
				"exp(x/(1-x)) = 1 + x + 3/2*x^2 + 13/6*x^3 + 73/24*x^4 + 167/40*x^5 + 4051/720*x^6 + 37633/5040*x^7 + 43817/4480*x^8 + 4596553/362880*x^9 + ... ."
			],
			"maple": [
				"b:= proc(n) option remember; `if`(n=0, 1,",
				"      add((n-k)*b(k), k=0..n-1)/n)",
				"    end:",
				"a:= n-\u003e numer(b(n)):",
				"seq(a(n), n=0..25);  # _Alois P. Heinz_, May 12 2016"
			],
			"mathematica": [
				"Table[Numerator@ SeriesCoefficient[Exp[x/(1 - x)], {x, 0, n}], {n, 19}] (* _Michael De Vlieger_, Dec 14 2015 *)",
				"r[n_] := If[n == 0, 1, Hypergeometric1F1[1 - n, 2, -1]]; Table[Numerator@ r[n], {n, 0, 21}] (* _Peter Luschny_, Feb 02 2019 *)"
			],
			"program": [
				"(PARI) a(n) = numerator(sum(k=1, n, binomial(n-1, k-1)/k!)); \\\\ _Altug Alkan_, Dec 14 2015",
				"(MAGMA) m:=30; R\u003cx\u003e:=PowerSeriesRing(Rationals(), m); b:=Coefficients(R!(Exp(x/(1-x)))); [Numerator(b[n]): n in [1..m]]; // _G. C. Greubel_, Dec 04 2018",
				"(Sage) [1] + [numerator(sum(binomial(n-1, j-1)/factorial(j) for j in (1..n))) for n in (1..30)] # _G. C. Greubel_, Dec 04 2018"
			],
			"xref": [
				"Cf. A067653."
			],
			"keyword": "nonn,frac",
			"offset": "0,3",
			"author": "_Benoit Cloitre_, Feb 03 2002",
			"ext": [
				"a(0)=1 prepended by _Alois P. Heinz_, May 12 2016"
			],
			"references": 9,
			"revision": 46,
			"time": "2019-02-02T15:51:47-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}