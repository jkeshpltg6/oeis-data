{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A317050",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 317050,
			"data": "0,1,-1,-2,2,3,5,4,-4,-3,-5,-6,-10,-9,-7,-8,8,9,7,6,10,11,13,12,20,21,19,18,14,15,17,16,-16,-15,-17,-18,-14,-13,-11,-12,-20,-19,-21,-22,-26,-25,-23,-24,-40,-39,-41,-42,-38,-37,-35,-36,-28,-27,-29,-30",
			"name": "a(0) = 0 and for any n \u003e= 0, a(n+1) is obtained by changing the rightmost possible digit in the negabinary representation of a(n) so as to get a value not yet in the sequence.",
			"comment": [
				"Binary Gray code, interpreted as negabinary number.",
				"This sequence is a bijection from nonnegative integers to signed integers.",
				"This sequence has similarities with A317018; in both sequences, the negabinary representations of consecutive terms differ exactly by one digit."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A317050/b317050.txt\"\u003eTable of n, a(n) for n = 0..16383\u003c/a\u003e",
				"Eric Weisstein, \u003ca href=\"http://mathworld.wolfram.com/Negabinary.html\"\u003eNegabinary\u003c/a\u003e (MathWorld)",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Negative_base\"\u003eNegative base\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A053985(A003188(n))."
			],
			"example": [
				"The first terms, alongside their negabinary representation, are:",
				"  n   a(n)  nega(a(n))",
				"  --  ----  ----------",
				"   0     0        0",
				"   1     1        1",
				"   2    -1       11",
				"   3    -2       10",
				"   4     2      110",
				"   5     3      111",
				"   6     5      101",
				"   7     4      100",
				"   8    -4     1100",
				"   9    -3     1101",
				"  10    -5     1111",
				"  11    -6     1110",
				"  12   -10     1010",
				"  13    -9     1011",
				"  14    -7     1001",
				"  15    -8     1000",
				"  16     8    11000",
				"  17     9    11001",
				"  18     7    11011",
				"  19     6    11010",
				"  20    10    11110",
				"a(8) = -4 because nega(a(7)) = 100. Changing the rightmost digit gives 101 of which the decimal value in the sequence. Similarily, changing to 110 and 000 gives no new term. Changing to 1100 does so a(8) is the decimal value of 1100 which is -4. - _David A. Corneth_, Jul 22 2018"
			],
			"program": [
				"(PARI) a(n) = fromdigits(binary(bitxor(n, n\u003e\u003e1)), -2)"
			],
			"xref": [
				"Cf. A003188, A039724, A053985, A212529, A317018."
			],
			"keyword": "sign,look,base",
			"offset": "0,4",
			"author": "_Rémy Sigrist_, Jul 20 2018",
			"references": 3,
			"revision": 22,
			"time": "2020-10-20T19:02:46-04:00",
			"created": "2018-07-21T15:54:13-04:00"
		}
	]
}