{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A046660",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 46660,
			"data": "0,0,0,1,0,0,0,2,1,0,0,1,0,0,0,3,0,1,0,1,0,0,0,2,1,0,2,1,0,0,0,4,0,0,0,2,0,0,0,2,0,0,0,1,1,0,0,3,1,1,0,1,0,2,0,2,0,0,0,1,0,0,1,5,0,0,0,1,0,0,0,3,0,0,1,1,0,0,0,3,3,0,0,1,0,0,0,2,0,1,0,1,0,0,0,4,0,1,1,2,0,0,0,2,0,0,0,3,0,0,0",
			"name": "Excess of n = number of prime divisors (with multiplicity) - number of prime divisors (without multiplicity).",
			"comment": [
				"a(n) depends only on prime signature of n (cf. A025487). So a(24) = a(375) since 24 = 2^3 * 3 and 375 = 3 * 5^3 both have prime signature (3, 1).",
				"a(n) = 0 for squarefree n.",
				"A162511(n) = (-1)^a(n). - _Reinhard Zumkeller_, Jul 08 2009",
				"a(n) = the number of divisors of n that are each a composite power of a prime. - _Leroy Quet_, Dec 02 2009",
				"a(A005117(n)) = 0; a(A060687(n)) = 1; a(A195086(n)) = 2; a(A195087(n)) = 3; a(A195088(n)) = 4; a(A195089(n)) = 5; a(A195090(n)) = 6; a(A195091(n)) = 7; a(A195092(n)) = 8; a(A195093(n)) = 9; a(A195069(n)) = 10. - _Reinhard Zumkeller_, Nov 29 2015"
			],
			"link": [
				"Charles R Greathouse IV, \u003ca href=\"/A046660/b046660.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"M. Kac, \u003ca href=\"http://www.gibbs.if.usp.br/~marchett/estocastica/MarkKac-Statistical-Independence.pdf\"\u003eStatistical Independence in Probability, Analysis and Number Theory\u003c/a\u003e, Carus Monograph 12, Math. Assoc. Amer., 1959, see p. 64."
			],
			"formula": [
				"a(n) = Omega(n) - omega(n) = A001222(n) - A001221(n).",
				"Additive with a(p^e) = e - 1.",
				"a(n) = Sum_{k = 1..A001221(n)} (A124010(n,k) - 1). - _Reinhard Zumkeller_, Jan 09 2013",
				"G.f.: Sum_{p prime, k\u003e=2} x^(p^k)/(1 - x^(p^k)). - _Ilya Gutkovskiy_, Jan 06 2017",
				"Asymptotic mean: lim_{m-\u003eoo} (1/m) Sum_{k=1..m} a(k) =  Sum_{p prime} 1/(p*(p-1)) =  0.773156... (A136141). - _Amiram Eldar_, Jul 28 2020"
			],
			"maple": [
				"with(numtheory); A046660:=n-\u003ebigomega(n)-nops(factorset(n)); seq(A046660(k), k=1..100); # _Wesley Ivan Hurt_, Oct 27 2013"
			],
			"mathematica": [
				"Table[PrimeOmega[n] - PrimeNu[n], {n, 50}] (* or *) muf[n_] := Module[{fi = FactorInteger[n]}, Total[Transpose[fi][[2]]] - Length[fi]]; Array[muf, 50] (* _Harvey P. Dale_, Sep 07 2011. The second program is several times faster than the first program for generating large numbers of terms. *)"
			],
			"program": [
				"(PARI) a(n)=bigomega(n)-omega(n) \\\\ _Charles R Greathouse IV_, Nov 14 2012",
				"(PARI) a(n)=my(f=factor(n)[,2]); vecsum(f)-#f \\\\ _Charles R Greathouse IV_, Aug 01 2016",
				"(Haskell)",
				"import Math.NumberTheory.Primes.Factorisation (factorise)",
				"a046660 n = sum es - length es where es = snd $ unzip $ factorise n",
				"-- _Reinhard Zumkeller_, Nov 28 2015, Jan 09 2013"
			],
			"xref": [
				"Not the same as A066301.",
				"Cf. A001222, A001221, A136141, A257851, A261256, A264959, A005117, A060687, A195086, A195087, A195088, A195089, A195090, A195091, A195092, A195093, A195069, A275699."
			],
			"keyword": "nonn,easy,nice",
			"offset": "1,8",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _David W. Wilson_"
			],
			"references": 86,
			"revision": 60,
			"time": "2020-07-28T11:11:58-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}