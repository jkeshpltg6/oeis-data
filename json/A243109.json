{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A243109",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 243109,
			"data": "0,1,1,3,2,3,5,7,4,6,9,7,10,11,13,15,8,12,17,14,18,19,21,15,20,22,25,23,26,27,29,31,16,24,33,28,34,35,37,30,36,38,41,39,42,43,45,31,40,44,49,46,50,51,53,47,52,54,57,55,58,59,61,63,32,48,65,56,66,67,69",
			"name": "a(n) is the largest number smaller than n and having the same Hamming weight as n, or n if no such number exist.",
			"comment": [
				"To calculate a(n), some bits of n are rearranged.  The lowest 1-bit which can move down is the 1 of the lowest 10 bit pair in n.  This pair becomes 01 in a(n) and any 1's below there move up to immediately below so the decrease is as small as possible.  If n has no 10 bit pair (n = 2^k-1) then nothing smaller is possible and a(n) = n. - _Kevin Ryde_, Mar 01 2021"
			],
			"link": [
				"Philippe Beaudoin, \u003ca href=\"/A243109/b243109.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Joerg Arndt, \u003ca href=\"http://www.jjj.de/fxt/#fxtbook\"\u003eMatters Computational (The Fxtbook)\u003c/a\u003e, section 1.24.1 Co-lexicographic (colex) order, function prev_colex_comb(), and second implementation in FXT library bitcombcolex.h (both 0 when no previous rather than a(n) = n)."
			],
			"formula": [
				"a(n) = t - 2^(A007814(t) - A007814(n+1) - 1) if t!=0, or a(n) = n if t=0, where t = A129760(n+1) is n with any trailing 1's cleared to 0's and A007814 is the 2-adic valuation. - _Kevin Ryde_, Mar 01 2021"
			],
			"example": [
				"From _Kevin Ryde_, Mar 01 2021: (Start)",
				"                           v    vv",
				"     n = 1475 = binary 10111000011    lowest 10 of n",
				"  a(n) = 1464 = binary 10110111000    becomes 01 and",
				"                            ^^^       other 1's below",
				"(End)"
			],
			"program": [
				"(PARI) a(n) = {my(hn = hammingweight(n)); forstep(k=n-1, 1, -1, if (hammingweight(k) == hn, return (k)); ); return (n); } \\\\ _Michel Marcus_, Aug 20 2014",
				"(PARI) a(n) = my(s=n+1,t=bitand(n,s)); if(t==0,n, t - 1\u003c\u003c(valuation(t,2)-valuation(s,2)-1)); \\\\ _Kevin Ryde_, Mar 01 2021"
			],
			"xref": [
				"Cf. A057168 (next of same weight), A066884 (array by weight), A241816 (lowest 10-\u003e01)."
			],
			"keyword": "nonn,base,easy",
			"offset": "0,4",
			"author": "_Philippe Beaudoin_, Aug 20 2014",
			"references": 2,
			"revision": 20,
			"time": "2021-03-02T06:02:56-05:00",
			"created": "2014-08-20T13:17:10-04:00"
		}
	]
}