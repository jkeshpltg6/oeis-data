{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002251",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2251,
			"data": "0,2,1,5,7,3,10,4,13,15,6,18,20,8,23,9,26,28,11,31,12,34,36,14,39,41,16,44,17,47,49,19,52,54,21,57,22,60,62,24,65,25,68,70,27,73,75,29,78,30,81,83,32,86,33,89,91,35,94,96,37,99,38,102,104,40,107,109",
			"name": "Start with the nonnegative integers; then swap L(k) and U(k) for all k \u003e= 1, where L = A000201, U = A001950 (lower and upper Wythoff sequences).",
			"comment": [
				"(n,a(n)) are Wythoff pairs: (0,0), (1,2), (3,5), (4,7), ..., where each difference occurs once.",
				"Self-inverse when considered as a permutation or function, i.e. a(a(n)) = n. - _Howard A. Landman_, Sep 25 2001",
				"If the offset is 1, the sequence can also be obtained by rearranging the natural numbers so that sum of n terms is a multiple of n, or equivalently so that the arithmetic mean of the first n terms is an integer. - _Amarnath Murthy_, Aug 16 2002",
				"For n = 1, 2, 3, ..., let p(n)=least natural number not already an a(k), q(n) = n + p(n); then a(p(n)) = q(n), a(q(n)) = p(n). - _Clark Kimberling_",
				"Also, indices of powers of 2 in A086482. - _Amarnath Murthy_, Jul 26 2003"
			],
			"reference": [
				"E. R. Berlekamp, J. H. Conway and R. K. Guy, Winning Ways, Academic Press, NY, 2 vols., 1982, see p. 76."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A002251/b002251.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"F. Michel Dekking, Jeffrey Shallit, and N. J. A. Sloane, \u003ca href=\"https://www.combinatorics.org/ojs/index.php/eljc/article/view/v27i1p52/8039\"\u003eQueens in exile: non-attacking queens on infinite chess boards\u003c/a\u003e, Electronic J. Combin., 27:1 (2020), #P1.52.",
				"Eric Duchene, Aviezri S. Fraenkel, Vladimir Gurvich, Nhan Bao Ho, Clark Kimberling, and Urban Larsson, \u003ca href=\"http://www.wisdom.weizmann.ac.il/~fraenkel/Papers/WythoffWisdomJune62016.pdf\"\u003eWythoff Wisdom\u003c/a\u003e, 43 pages, no date, unpublished.",
				"Eric Duchene, Aviezri S. Fraenkel, Vladimir Gurvich, Nhan Bao Ho, Clark Kimberling, and Urban Larsson, \u003ca href=\"/A001950/a001950.pdf\"\u003eWythoff Wisdom\u003c/a\u003e, unpublished, no date [Cached copy, with permission]",
				"Alex Meadows, B. Putman, \u003ca href=\"https://arxiv.org/abs/1606.06819\"\u003eA New Twist on Wythoff's Game\u003c/a\u003e, arXiv preprint arXiv:1606.06819 [math.CO], 2016.",
				"Gabriel Nivasch, \u003ca href=\"http://www.msri.org/people/staff/levy/files/Book56/43nivasch.pdf\"\u003eMore on the Sprague-Grundy function for Wythoff’s game\u003c/a\u003e, pages 377-410 in \"Games of No Chance 3, MSRI Publications Volume 56, 2009.",
				"R. Silber, \u003ca href=\"http://www.fq.math.ca/Scanned/15-1/silber2.pdf\"\u003eWythoff's Nim and Fibonacci Representations\u003c/a\u003e, Fibonacci Quarterly #14 (1977), pp. 85-88.",
				"N. J. A. Sloane, \u003ca href=\"/A002251/a002251.pdf\"\u003eScatterplot of first 100 terms\u003c/a\u003e [The points are symmetrically placed about the diagonal, although that is hard to see here because the scales on the axes are different]",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A019444(n+1) - 1."
			],
			"mathematica": [
				"With[{n = 42}, {0}~Join~Take[Values@ #, LengthWhile[#, # == 1 \u0026] \u0026@ Differences@ Keys@ #] \u0026@ Sort@ Flatten@ Map[{#1 -\u003e #2, #2 -\u003e #1} \u0026 @@ # \u0026, Transpose@ {Array[Floor[# GoldenRatio] \u0026, n], Array[Floor[# GoldenRatio^2] \u0026, n]}]] (* _Michael De Vlieger_, Nov 14 2017 *)"
			],
			"program": [
				"(PARI) A002251_upto(N,c=0,A=Vec(0,N))={for(n=1,N, A[n]||(#A\u003cA[n]=n+c++)|| A[n+c]=n); A} \\\\ The resulting vector starts with A002251[1]=2, a(0)=0 is not included. - _M. F. Hasler_, Nov 27 2019, replacing earlier code from Sep 17 2014"
			],
			"xref": [
				"The sequence maps between A000201 and A001950, in that a(A000201(n)) = A001950(n), a(A001950(n)) = A000201(n).",
				"Row 0 of A018219.",
				"Cf. A073869, A342297."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,2",
			"author": "_Michael Kleber_",
			"ext": [
				"Edited by _Christian G. Bower_, Oct 29 2002"
			],
			"references": 22,
			"revision": 65,
			"time": "2021-03-14T20:36:45-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}