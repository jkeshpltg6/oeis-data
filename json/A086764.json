{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A086764",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 86764,
			"data": "1,0,1,1,1,1,2,3,2,1,9,11,7,3,1,44,53,32,13,4,1,265,309,181,71,21,5,1,1854,2119,1214,465,134,31,6,1,14833,16687,9403,3539,1001,227,43,7,1,133496,148329,82508,30637,8544,1909,356,57,8,1,1334961,1468457,808393",
			"name": "Triangle T(n, k), read by row, related to Euler's difference table A068106 (divide k-th diagonal of A068106 by k!).",
			"comment": [
				"The k-th column sequence, k \u003e= 0, without leading zeros, enumerates the ways to distribute n beads, n \u003e= 1, labeled differently from 1 to n, over a set of (unordered) necklaces, excluding necklaces with exactly one bead, and k+1 indistinguishable, ordered, fixed cords, each allowed to have any number of beads. Beadless necklaces as well as beadless cords each contribute a factor 1, hence for n=0 one has 1. See A000255 for the description of a fixed cord with beads. This comment derives from a family of recurrences found by Malin Sjodahl for a combinatorial problem for certain quark and gluon diagrams (Feb 27 2010). - _Wolfdieter Lang_, Jun 02 2010"
			],
			"link": [
				"Indranil Ghosh, \u003ca href=\"/A086764/b086764.txt\"\u003eRows 0..50, flattened\u003c/a\u003e",
				"W. Y. C. Chen et al., \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2011.06.006\"\u003eHigher-order log-concavity in Euler's difference table\u003c/a\u003e, Discrete Math., 311 (2011), 2128-2134. (These are the numbers d^k_n.)",
				"Fanja Rakotondrajao, \u003ca href=\"http://www.emis.de/journals/INTEGERS/papers/h36/h36.Abstract.html\"\u003ek-Fixed-Points-Permutations\u003c/a\u003e, Integers: Electronic journal of combinatorial number theory 7 (2007) A36."
			],
			"formula": [
				"T(n, n) = 1; T(n+1, n) = n;",
				"T(n+2, n) = A002061(n+1) = n^2 + n + 1; T(n+3, n) = n^3 + 3*n^2 + 5*n + 2.",
				"T(n, k) = (k + 1)*T(n, k + 1)-T(n-1, k); T(n, n) = 1; T(n, k) = 0, if k \u003e n.",
				"T(n, k) = (n-1)*T(n-1, k) + (n-k-1)*T(n-2, k).",
				"k!*T(n, k) = A068106(n+1, k+1).",
				"Sum_{k\u003e=0} T(n, k) = A003470(n+1).",
				"T(n, k) = (1/k!) * Sum_{j\u003e=0} (-1)^j*binomial(n-k, j)*(n-j)!. - _Philippe Deléham_, Jun 13 2005",
				"From _Peter Bala_, Aug 14 2008: (Start)",
				"The following remarks all relate to the array read as a square array: e.g.f for column k: exp(-y)/(1-y)^(k+1); e.g.f. for array: exp(-y)/(1-x-y) = (1 + x + x^2 + x^3 + ...) + (x + 2*x^2 + 3*x^3 + 4*x^4 + ...)*y + (1 + 3*x + 7*x^2 + 13*x^3 + ...)*y^2/2! + ... .",
				"This table is closely connected to the constant e. The row, column and diagonal entries of this table occur in series formulas for e.",
				"Row n for n \u003e= 2: e = n!*(1/T(n,0) + (-1)^n*[1/(1!*T(n,0)*T(n,1)) + 1/(2!*T(n,1)*T(n,2)) + 1/(3!*T(n,2)*T(n,3)) + ...]). For example, row 3 gives e = 6*(1/2 - 1/(1!*2*11) - 1/(2!*11*32) - 1/(3!*32*71) - ...). See A095000.",
				"Column 0: e = 2 + Sum_{n\u003e=2} (-1)^n*n!/(T(n,0)*T(n+1,0)) = 2 + 2!/(1*2) - 3 !/(2*9) + 4!/(9*44) - ... .",
				"Column k, k \u003e= 1: e = (1 + 1/1! + 1/2! + ... + 1/k!) + 1/k!*Sum_{n \u003e= 0} (-1)^n*n!/(T(n,k)*T(n+1,k)). For example, column 3 gives e = 8/3 + 1/6*(1/(1*3) - 1/(3*13) + 2/(13*71) - 6/(71*465) + ...).",
				"Main diagonal: e = 1 + 2*(1/(1*1) - 1/(1*7) + 1/(7*71) - 1/(71*1001) + ...).",
				"First subdiagonal: e = 8/3 + 5/(3*32) - 7/(32*465) + 9/(465*8544) - ... .",
				"Second subdiagonal: e = 2*(1 + 2^2/(1*11) - 3^2/(11*181) + 4^2/(181*3539) - ...). See A143413.",
				"Third subdiagonal: e = 3 - (2*3*5)/(2*53) + (3*4*7)/(53*1214) - (4*5*9)/(1214*30637) + ... .",
				"For the corresponding results for the constants 1/e, sqrt(e) and 1/sqrt(e) see A143409, A143410 and A143411 respectively. For other arrays similarly related to constants see A008288 (for log(2)), A108625 (for zeta(2)) and A143007 (for zeta(3)). (End)",
				"G.f. for column k is hypergeom([1,k+1],[],x/(x+1))/(x+1). - _Mark van Hoeij_, Nov 07 2011",
				"T(n, k) = (n!/k!)*hypergeom([k-n], [-n], -1). - _Peter Luschny_, Oct 05 2017"
			],
			"example": [
				"1; 0, 1; 1, 1, 1; 2, 3, 2, 1; 9, 11, 7, 3, 1; 44, 53, 32, 13, 4, 1; ...",
				"Formatted as a square array:",
				"      1      3     7    13   21   31  43 57 which equals A002061",
				"      2     11    32    71  134  227 356 which equals A094792",
				"      9     53   181   465 1001 1909 which equals A094793",
				"     44    309  1214  3539 8544 which equals A094794",
				"    265   2119  9403 30637 which equals A023043",
				"   1854  16687 82508 which equals A023044",
				"  14833 148329 which equals A023045",
				"Formatted as a triangular array (mirror of A076731):",
				"       1",
				"       0      1",
				"       1      1     1",
				"       2      3     2     1",
				"       9     11     7     3    1",
				"      44     53    32    13    4    1",
				"     265    309   181    71   21    5   1",
				"    1854   2119  1214   465  134   31   6  1",
				"   14833  16687  9403  3539 1001  227  43  7 1",
				"  133496 148329 82508 30637 8544 1909 356 57 8 1"
			],
			"mathematica": [
				"T[n_,k_]:=(1/k!)*Sum[(-1)^j*Binomial[n-k,j]*(n-j)!,{j,0,n}];Flatten[Table[T[n,k],{n,0,11},{k,0,n}]] (* _Indranil Ghosh_, Feb 20 2017 *)",
				"T[n_, k_] := (n!/k!) HypergeometricPFQ[{k-n},{-n},-1];",
				"Table[T[n,k], {n,0,9}, {k,0,n}] // Flatten (* _Peter Luschny_, Oct 05 2017 *)"
			],
			"xref": [
				"Columns: A000166, A000155, A000153, A000261, A001909, A001910, A176732 - A176736.",
				"Cf. A068106, A003470, A002061. Mirror image of A076731.",
				"Cf. A143409, A143410, A143411, A143413."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "0,7",
			"author": "_Philippe Deléham_, Aug 02 2003",
			"ext": [
				"More terms from _David Wasserman_, Mar 28 2005",
				"Additional comments from _Zerinvary Lajos_, Mar 30 2006",
				"Edited by _N. J. A. Sloane_, Sep 24 2011"
			],
			"references": 20,
			"revision": 52,
			"time": "2017-10-06T08:35:47-04:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}