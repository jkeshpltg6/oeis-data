{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A080670",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 80670,
			"data": "1,2,3,22,5,23,7,23,32,25,11,223,13,27,35,24,17,232,19,225,37,211,23,233,52,213,33,227,29,235,31,25,311,217,57,2232,37,219,313,235,41,237,43,2211,325,223,47,243,72,252,317,2213,53,233,511,237,319,229,59,2235",
			"name": "Literal reading of the prime factorization of n.",
			"comment": [
				"Exponents equal to 1 are omitted and therefore this sequence differs from A067599.",
				"Here the first duplicate (ambiguous) term appears already with a(8)=23=a(6), in A067599 this happens only much later. - _M. F. Hasler_, Oct 18 2014",
				"The number n = 13532385396179 = 13·53^2·3853·96179 = a(n) is (maybe the first?) nontrivial fixed point of this sequence, making it the first known index of a -1 in A195264. - _M. F. Hasler_, Jun 06 2017"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A080670/b080670.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Tony Padilla and Brady Haran, \u003ca href=\"https://www.youtube.com/watch?v=3IMAUm2WY70\"\u003e13532385396179\u003c/a\u003e, Numberphile Video, 2017",
				"N. J. A. Sloane, \u003ca href=\"/A195264/a195264.pdf\"\u003eConfessions of a Sequence Addict (AofA2017)\u003c/a\u003e, slides of invited talk given at AofA 2017, Jun 19 2017, Princeton. Mentions this sequence.",
				"N. J. A. Sloane, Three (No, 8) Lovely Problems from the OEIS, Experimental Mathematics Seminar, Rutgers University, Oct 05 2017, \u003ca href=\"https://vimeo.com/237029685\"\u003ePart I\u003c/a\u003e, \u003ca href=\"https://vimeo.com/237030304\"\u003ePart 2\u003c/a\u003e, \u003ca href=\"https://oeis.org/A290447/a290447_slides.pdf\"\u003eSlides.\u003c/a\u003e (Mentions this sequence)"
			],
			"example": [
				"8=2^3, which reads 23, hence a(8)=23; 12=2^2*3, which reads 223, hence a(12)=223."
			],
			"maple": [
				"ifsSorted := proc(n)",
				"        local fs,L,p ;",
				"        fs := sort(convert(numtheory[factorset](n),list)) ;",
				"        L := [] ;",
				"        for p in fs do",
				"                L := [op(L),[p,padic[ordp](n,p)]] ;",
				"        end do;",
				"        L ;",
				"end proc:",
				"A080670 := proc(n)",
				"        local a,p ;",
				"        if n = 1 then",
				"                return 1;",
				"        end if;",
				"        a := 0 ;",
				"        for p in ifsSorted(n) do",
				"                a := digcat2(a,op(1,p)) ;",
				"                if op(2,p) \u003e 1 then",
				"                        a := digcat2(a,op(2,p)) ;",
				"                end if;",
				"        end do:",
				"        a ;",
				"end proc: # _R. J. Mathar_, Oct 02 2011",
				"# second Maple program:",
				"a:= proc(n) option remember; `if`(n=1, 1, (l-\u003e",
				"      parse(cat(seq(`if`(l[i, 2]=1, l[i, 1], [l[i, 1],",
				"      l[i, 2]][]), i=1..nops(l)))))(sort(ifactors(n)[2])))",
				"    end:",
				"seq(a(n), n=1..100);  # _Alois P. Heinz_, Mar 17 2020"
			],
			"mathematica": [
				"f[n_] := FromDigits[ Flatten@ IntegerDigits[ Flatten[ FactorInteger@ n /. {1 -\u003e {}}]]]; f[1] = 1; Array[ f, 60] (* _Robert G. Wilson v_, Mar 02 2003 and modified Jul 22 2014 *)"
			],
			"program": [
				"(PARI) A080670(n)=if(n\u003e1, my(f=factor(n),s=\"\"); for(i=1,#f~,s=Str(s,f[i,1],if(f[i,2]\u003e1, f[i,2],\"\"))); eval(s),1) \\\\ _Charles R Greathouse IV_, Oct 27 2013; case n=1 added by _M. F. Hasler_, Oct 18 2014",
				"(PARI) A080670(n)=if(n\u003e1,eval(concat(apply(f-\u003eStr(f[1],if(f[2]\u003e1,f[2],\"\")),Vec(factor(n)~)))),1) \\\\ _M. F. Hasler_, Oct 18 2014",
				"(Haskell)",
				"import Data.Function (on)",
				"a080670 1 = 1",
				"a080670 n = read $ foldl1 (++) $",
				"zipWith (c `on` show) (a027748_row n) (a124010_row n) :: Integer",
				"where c ps es = if es == \"1\" then ps else ps ++ es",
				"-- _Reinhard Zumkeller_, Oct 27 2013",
				"(Python)",
				"import sympy",
				"[int(''.join([str(y) for x in sorted(sympy.ntheory.factorint(n).items()) for y in x if y != 1])) for n in range(2,100)] # compute a(n) for n \u003e 1",
				"# _Chai Wah Wu_, Jul 15 2014"
			],
			"xref": [
				"Cf. A037276, A067599, A230305, A230625, A027748, A124010, A288818.",
				"See A195330, A195331 for those n for which a(n) is a contraction.",
				"See also home primes, A037271.",
				"See A195264 for what happens when k -\u003e a(k) is repeatedly applied to n.",
				"Partial sums: A287881, A287882."
			],
			"keyword": "nonn,base,look",
			"offset": "1,2",
			"author": "_Jon Perry_, Mar 02 2003",
			"ext": [
				"Edited and extended by _Robert G. Wilson v_, Mar 02 2003"
			],
			"references": 26,
			"revision": 77,
			"time": "2020-03-17T17:04:26-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}