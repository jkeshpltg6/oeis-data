{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A097899",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 97899,
			"data": "1,0,1,1,6,19,109,588,4033,29485,246042,2228203,22162249,237997032,2757055393,34191395785,452480427678,6360924613699,94691284984405,1487846074481172,24608991911033377,427379047337272213,7775688853750498386,147900024951747279643",
			"name": "Number of permutations of [n] with no runs of length 1. (The permutation 3574162 has two runs of length 1: 357/4/16/2).",
			"reference": [
				"Ira. M. Gessel, Generating functions and enumeration of sequences, Ph. D. Thesis, MIT, 1977, p. 52."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A097899/b097899.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"Paul Barry, \u003ca href=\"https://arxiv.org/abs/1802.03443\"\u003eOn a transformation of Riordan moment sequences\u003c/a\u003e, arXiv:1802.03443 [math.CO], 2018."
			],
			"formula": [
				"a(n) = A000142(n) - A228614(n).",
				"E.g.f.: (sqrt(3)/2)exp(-x/2)/cos(sqrt(3)x/2 + Pi/6).",
				"E.g.f.: 1/(1-x^2/2!-x^3/3! +x^5/5! + x^6/6! - x^8/8! -x^9/9! + ... ) - _Ira M. Gessel_, Jul 27 2014",
				"a(n) ~ n! * exp(-Pi*sqrt(3)/9) * (3*sqrt(3)/(2*Pi))^(n+1). - _Vaclav Kotesovec_, Oct 08 2013",
				"G.f.: T(0), where T(k) = 1 - x^2*(k+1)^2/( x^2*(k+1)^2 - (1-x*(k+1))*(1-x*k)/T(k+1) ); (continued fraction). - _Sergei N. Gladkovskii_, Dec 03 2013"
			],
			"example": [
				"Example: a(4)=6 because 1234, 1324, 1423, 2314, 2413, 3412 are the only permutations of [4] with no runs of length 1."
			],
			"maple": [
				"G:=sqrt(3)*exp(-x/2)/2/cos(sqrt(3)*x/2+Pi/6): Gser:=series(G, x, 26): seq(n!*coeff(Gser, x, n), n=0..25);"
			],
			"mathematica": [
				"FullSimplify[CoefficientList[Series[(Sqrt[3]/2)*E^(-x/2)/Cos[Sqrt[3]*x/2 + Pi/6], {x, 0, 20}], x]* Range[0, 20]!] (* _Vaclav Kotesovec_, Oct 08 2013 *)",
				"g[u_, o_] := g[u, o] = If[u + o \u003c 2, u,",
				"     Sum[b[u - i, o + i - 1], {i, u}] +",
				"     Sum[g[u + i - 1, o - i], {i, o}]];",
				"b[u_, o_] := b[u, o] = If[u + o \u003c 2, 1 - o, u*(u + o - 1)! +",
				"     Sum[g[u + i - 1, o - i], {i, o}]] ;",
				"a[n_] := n! - Sum[b[j - 1, n - j], {j, n}];",
				"Table[a[n], {n, 0, 25}] (* _Jean-François Alcover_, Aug 30 2021, after _Alois P. Heinz_ in A228614 *)"
			],
			"xref": [
				"Cf. A186735."
			],
			"keyword": "nonn",
			"offset": "0,5",
			"author": "_Emeric Deutsch_ and _Ira M. Gessel_, Sep 03 2004",
			"references": 3,
			"revision": 29,
			"time": "2021-08-30T03:10:52-04:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}