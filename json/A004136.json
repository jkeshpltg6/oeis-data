{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A004136",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 4136,
			"id": "M2639",
			"data": "1,3,7,13,21,31,48,57,73,91,120,133,168,183,255,255,273,307",
			"name": "Additive bases: a(n) is the least integer k such that in the cyclic group Z_k there is a subset of n elements all pairs (of not necessarily distinct elements) of which add up to a different sum (in Z_k).",
			"comment": [
				"a(n) \u003e= n^2-n+1 by a volume bound. A difference set construction by Singer shows that equality holds when n-1 is a prime power. When n is a prime power, a difference set construction by Bose shows that a(n) \u003c= n^2-1. By computation, equality holds in the latter bound at least for 7, 11, 13 and 16.",
				"From _Fausto A. C. Cariboni_, Aug 13 2017: (Start)",
				"Lexicographically first basis that yields a(n) for n=15..18:",
				"a(15) = 255 from {0,1,3,7,15,26,31,53,63,98,107,127,140,176,197}",
				"a(16) = 255 from {0,1,3,7,15,26,31,53,63,98,107,127,140,176,197,215}",
				"a(17) = 273 from {0,1,3,7,15,31,63,90,116,127,136,181,194,204,233,238,255}",
				"a(18) = 307 from {0,1,3,21,25,31,68,77,91,170,177,185,196,212,225,257,269,274}",
				"(End)",
				"Such sets are also known as modular Golomb rulers, or circular Golomb rulers. - _Andrey Zabolotskiy_, Sep 11 2017"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Bela Bajnok, \u003ca href=\"https://arxiv.org/abs/1705.07444\"\u003eAdditive Combinatorics: A Menu of Research Problems\u003c/a\u003e, arXiv:1705.07444 [math.NT], May 2017. See p. 162.",
				"R. L. Graham and N. J. A. Sloane, \u003ca href=\"http://neilsloane.com/doc/RLG/073.pdf\"\u003eOn Additive Bases and Harmonious Graphs\u003c/a\u003e, SIAM J. Algebraic and Discrete Methods, 1 (1980), 382-404 (v_delta).",
				"H. Haanpaa, A. Huima and Patric R. J. Östergård, \u003ca href=\"https://doi.org/10.1016/S0166-218X(03)00273-7\"\u003eSets in Z_n with Distinct Sums of Pairs\u003c/a\u003e, in Optimal discrete structures and algorithms (ODSA 2000). Discrete Appl. Math. 138 (2004), no. 1-2, 99-106.",
				"H. Haanpaa, A. Huima and Patric R. J. Östergård, \u003ca href=\"/A004135/a004135.pdf\"\u003eSets in Z_n with Distinct Sums of Pairs\u003c/a\u003e, in Optimal discrete structures and algorithms (ODSA 2000). Discrete Appl. Math. 138 (2004), no. 1-2, 99-106. [Annotated scanned copies of four pages only from preprint of paper above]",
				"Z. Skupien, A. Zak, Pair-sums packing and rainbow cliques, in \u003ca href=\"http://www.math.uiuc.edu/~kostochk/Zykov90-Topics_in_Graph_Theory.pdf\"\u003eTopics In Graph Theory\u003c/a\u003e, A tribute to A. A. and T. E. Zykovs on the occasion of A. A. Zykov's 90th birthday, ed. R. Tyshkevich, Univ. Illinois, 2013, pages 131-144, (in English and Russian)."
			],
			"example": [
				"a(3)=7: the set {0,1,3} is such a subset of Z_7, since 0+0, 0+1, 0+3, 1+1, 1+3 and 3+3 are all distinct in Z_7; also, no such 3-element set exists in any smaller cyclic group."
			],
			"xref": [
				"Cf. A004133, A004135, A260998, A260999, A003022."
			],
			"keyword": "nonn,nice,more",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms and comments from Harri Haanpaa (Harri.Haanpaa(AT)hut.fi), Oct 30 2000",
				"a(15)-a(18) from _Fausto A. C. Cariboni_, Aug 13 2017"
			],
			"references": 5,
			"revision": 41,
			"time": "2020-02-02T20:10:55-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}