{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A156608",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 156608,
			"data": "1,1,1,1,-1,1,1,1,1,1,1,1,-1,1,1,1,-2,2,2,-2,1,1,1,2,2,2,1,1,1,1,-1,2,2,-1,1,1,1,-2,2,2,-4,2,2,-2,1,1,1,2,2,2,2,2,2,1,1,1,1,-1,2,2,-1,2,2,-1,1,1",
			"name": "Triangle T(n, k, m) = round( t(n,m)/(t(k,m)*t(n-k,m)) ), with T(0, k, m) = 1, where t(n, k) = Product_{j=1..n} A129862(k+1, j), t(n, 0) = n!, and m = 2, read by rows.",
			"comment": [
				"The original definition of this sequence said it was based on the Cartan matrix of type D_n, so that matrix is somehow implicitly involved. - _N. J. A. Sloane_, Jun 25 2021"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A156608/b156608.txt\"\u003eRows n = 0..100 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k, m) = round( t(n,m)/(t(k,m)*t(n-k,m)) ), with T(0, k, m) = 1, where t(n, k) = Product_{j=1..n} A129862(k+1, j), t(n, 0) = n!, and m = 2."
			],
			"example": [
				"Triangle begins:",
				"  1;",
				"  1,  1;",
				"  1, -1,  1;",
				"  1,  1,  1, 1;",
				"  1,  1, -1, 1,  1;",
				"  1, -2,  2, 2, -2,  1;",
				"  1,  1,  2, 2,  2,  1, 1;",
				"  1,  1, -1, 2,  2, -1, 1,  1;",
				"  1, -2,  2, 2, -4,  2, 2, -2,  1;",
				"  1,  1,  2, 2,  2,  2, 2,  2,  1, 1;",
				"  1,  1, -1, 2,  2, -1, 2,  2, -1, 1, 1;"
			],
			"mathematica": [
				"(* First program *)",
				"b[n_, k_, d_]:= If[n==k, 2, If[(k==d \u0026\u0026 n==d-2) || (n==d \u0026\u0026 k==d-2), -1, If[(k==n- 1 || k==n+1) \u0026\u0026 n\u003c=d-1 \u0026\u0026 k\u003c=d-1, -1, 0]]];",
				"M[d_]:= Table[b[n, k, d], {n, d}, {k, d}];",
				"p[x_, n_]:= If[n==0, 1, CharacteristicPolynomial[M[n], x]];",
				"f = Table[p[x, n], {n, 0, 20}];",
				"t[n_, k_]:= If[k==0, n!, Product[f[[j+1]], {j, n-1}]]/.x -\u003e k+1;",
				"T[n_, k_, m_]:= Round[t[n, m]/(t[k, m]*t[n-k, m])];",
				"Table[T[n, k, 2], {n,0,15}, {k, 0, n}]//Flatten (* modified by _G. C. Greubel_, Jun 23 2021 *)",
				"(* Second program *)",
				"f[n_, x_]:= f[n,x]= If[n\u003c2, (2-x)^n, (2-x)*LucasL[2*(n-1), Sqrt[-x]] ];",
				"t[n_, k_]:= t[n,k]= If[k==0, n!, Product[f[j, x], {j, n-1}]]/.x -\u003e (k+1);",
				"T[n_, k_, m_]:= T[n,k,m]= Round[t[n,m]/(t[k,m]*t[n-k,m])];",
				"Table[T[n, k, 2], {n,0,15}, {k,0,n}]//Flatten (* _G. C. Greubel_, Jun 23 2021 *)"
			],
			"program": [
				"(Sage)",
				"@CachedFunction",
				"def f(n,x): return (2-x)^n if (n\u003c2) else 2*(2-x)*sum( ((n-1)/(2*n-j-2))*binomial(2*n-j-2, j)*(-x)^(n-j-1) for j in (0..n-1) )",
				"def g(n,k): return factorial(n) if (k==0) else product( f(j, k+1) for j in (1..n-1) )",
				"def T(n,k,m): return round( g(n,m)/(g(k,m)*g(n-k,m)) )",
				"flatten([[T(n,k,2) for k in (0..n)] for n in (0..15)]) # _G. C. Greubel_, Jun 23 2021"
			],
			"xref": [
				"Cf. A129862, A007318 (m=0), this sequence (m=2), A156609 (m=3), A156610 (m=4), A156612."
			],
			"keyword": "sign,tabl",
			"offset": "0,17",
			"author": "_Roger L. Bagula_, Feb 11 2009",
			"ext": [
				"Edited by _G. C. Greubel_, Jun 23 2021"
			],
			"references": 5,
			"revision": 12,
			"time": "2021-06-25T03:35:33-04:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}