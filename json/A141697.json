{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A141697",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 141697,
			"data": "1,1,1,1,16,1,1,59,59,1,1,158,426,158,1,1,369,2054,2054,369,1,1,804,8247,16792,8247,804,1,1,1687,29925,109123,109123,29925,1687,1,1,3466,102088,617302,1092910,617302,102088,3466,1,1,7037,334664,3185840,9171722,9171722,3185840,334664,7037,1",
			"name": "T(n,k) = (q*Sum_{j=0..k+1} (-1)^j*binomial(n+1, j)*(k+1-j)^n - p*binomial(n-1, k))/2 where p=12 and q=14.",
			"comment": [
				"Row n is made of coefficients from 7*(1 - x)^(n+1) * polylog(-n,x)/x - 6*(1 + x)^(n-1). - _Thomas Baruchel_, Jun 03 2018"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A141697/b141697.txt\"\u003eRows n = 1..100 of triangle, flattened\u003c/a\u003e",
				"Thomas Baruchel, \u003ca href=\"https://math.stackexchange.com/q/2806590/\"\u003eA conjectured formula for the polylogarithm of a negative integer order\u003c/a\u003e, Math Stack Exchange question, Jun 04 2018."
			],
			"formula": [
				"p=12; q=14; T(n,k) = (q*Sum_{j=0..k+1} (-1)^j*binomial(n+1, j)*(k+1-j)^n  - p*binomial(n-1, k))/2.",
				"a(n) = 3*A168524(n) - 2*A154337(n). - _Thomas Baruchel_, Jun 08 2018"
			],
			"example": [
				"Triangle begins as:",
				"  1;",
				"  1,    1;",
				"  1,   16,      1;",
				"  1,   59,     59,       1;",
				"  1,  158,    426,     158,       1;",
				"  1,  369,   2054,    2054,     369,       1;",
				"  1,  804,   8247,   16792,    8247,     804,       1;",
				"  1, 1687,  29925,  109123,  109123,   29925,    1687,      1;"
			],
			"maple": [
				"T:= proc(n, k): 7*add((-1)^j*binomial(n+1, j)*(k-j+1)^n, j = 0..k+1) - 6*binomial(n-1, k); end proc; seq(seq(T(n,k), k=0..n-1), n=1..10); # _G. C. Greubel_, Nov 13 2019"
			],
			"mathematica": [
				"i=12; l=14; Table[Table[(l*Sum[(-1)^j*Binomial[n+1, j](k+1-j)^n, {j, 0, k+1}] - i*Binomial[n-1, k])/2, {k,0,n-1}], {n,10}]//Flatten"
			],
			"program": [
				"(PARI) T(n,k) = 7*sum(j=0, k+1, (-1)^j*binomial(n+1,j)*(k-j+1)^n) - 6* binomial(n-1,k);",
				"for(n=1,10, for(k=0,n-1, print1(T(n,k), \", \"))) \\\\ _G. C. Greubel_, Jun 03 2018",
				"(PARI) row(n) = Vec(7*(1 - x)^(n+1)*polylog(-n,x)/x - 6*(1 + x)^(n-1)); \\\\ _Michel Marcus_, Jun 08 2018",
				"(MAGMA) [ 7*(\u0026+[(-1)^j*Binomial(n+1,j)*(k-j+1)^n: j in [0..k+1]]) - 6*Binomial(n-1,k): k in [0..n-1], n in [1..10]]; // _G. C. Greubel_, Nov 13 2019",
				"(Sage) [[ 7*sum( (-1)^j*binomial(n+1,j)*(k-j+1)^n for j in (0..k+1)) - 6*binomial(n-1,k) for k in (0..n-1)] for n in (1..10)] # _G. C. Greubel_, Nov 13 2019"
			],
			"xref": [
				"Cf. Eulerian numbers (A008292) and Pascal's triangle (A007318).",
				"Cf. A141696."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "1,5",
			"author": "_Roger L. Bagula_, Sep 11 2008",
			"ext": [
				"Edited by _G. C. Greubel_, Nov 13 2019"
			],
			"references": 2,
			"revision": 40,
			"time": "2019-11-14T10:51:21-05:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}