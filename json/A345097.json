{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A345097",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 345097,
			"data": "0,1,1,2,3,5,8,13,4,17,8,25,7,32,5,37,10,47,11,58,13,71,8,79,16,95,14,109,9,118,9,127,9,136,9,145,9,154,9,163,9,172,9,181,9,190,9,199,18,217,8,225,7,232,5,237,10,247,11,258,13,271,8,279,16,295,14,309,9,318,9,327",
			"name": "a(n) is the sum of the two preceding terms if n is odd, or of the two preceding digits if n is even, with a(0) = 0, a(1) = 1.",
			"comment": [
				"Considering the terms modulo 100, the sequence becomes periodic with period length 40 after the first 9 terms. The period is the same as in A345095, where it starts only after the first 32 terms. This property leads to a first order recurrence and an explicit formula for a(n), see Formula section.",
				"Differs from the Fibonacci sequence A000045 from a(8) = 4 on.",
				"Starting with a(9) = 13, every other term a(2k-1) has at least two digits, so the next term a(2k) is equal to the sum of the last two digits of a(2k-1).",
				"Similarly, the graph of this sequence has two components: even indexed terms repeating the pattern [8, 7, 5, 10, 11, 13, 8, 16, 14, 9, ..., 9, 18] of length 20, and odd indexed terms evolving around the straight line y(n) = 5n - 32.25, with first differences equal to the even indexed terms."
			],
			"link": [
				"Eric Angelini, \u003ca href=\"https://mailman.xmission.com/hyperkitty/list/math-fun@mailman.xmission.com/thread/ERBI7PWVRAJJ5ZTGSKX2C7F4PTAVTEOR/\"\u003eFibonacci alternated\u003c/a\u003e, math-fun discussion list on xmission.com, Jul 04 2021",
				"\u003ca href=\"/index/Rec#order_42\"\u003eIndex to entries for linear recurrences with constant coefficients, order 42\u003c/a\u003e, signature (0, 1, 0, ..., 0, 1, 0, -1)"
			],
			"formula": [
				"a(n+1) = a(n) + a(n-1) if n is even or n \u003c 6, = a(n)%10 + floor(a(n)/10)%10 if n is odd and n \u003e 6, where % is the binary modulo (or remainder) operator.",
				"a(n+40) = a(n) for even n \u003e 9, a(n+40) = a(n) + 200 for odd n \u003e= 9, hence:",
				"a(n) = a((n-9)%40 + 9) + [floor((n-9)/40)*200 if n odd] for n \u003e= 9, giving any term explicitly in terms of a(0..48).",
				"a(n) = a(n-2) + a(n-40) - a(n-42) for n \u003e= 51.",
				"O.g.f. x*(Sum_{k=0..49} c_k x^k)/(1 - x^2 - x^40 + x^42), where c = (1, 1, 1, 2, 3, 5, 8, -4, 4, 4, 8, -1, 7, -2, 5, 5, 10, 1, 11, 2, 13, -5, 8, 8, 16, -2, 14, -5, 9, 0, 9, 0, 9, 0, 9, 0, 9, 0, 9, 0, 8, -1, 8, -2, 6, -5, 1, 13, 14, -14)."
			],
			"example": [
				"Up to a(7) = 13, we have the Fibonacci sequence A000045. Then:",
				"a(8) = 1 + 3 = 4 is the sum of the two preceding digits: those of a(7).",
				"a(9) = 13 + 4  = 17 is the sum of the two preceding terms, a(7) + a(8).",
				"a(10) = 1 + 7 = 8 is the sum of the two preceding digits: those of a(9).",
				"a(11) = 17 + 8  = 25 is the sum of the two preceding terms, a(9) + a(10),",
				"and so on."
			],
			"mathematica": [
				"a[0]=0;a[1]=a[2]=1;a[n_]:=a[n]=If[OddQ@n,a[n-1]+a[n-2],Total[Flatten[IntegerDigits/@Array[a,n-1]][[-2;;]]]];Array[a,100,0] (* _Giorgos Kalogeropoulos_, Jun 08 2021 *)"
			],
			"program": [
				"(PARI) A345097_vec(N=99,a=List([0,1]))={ for(n=2,N, listput(a, if(n%2 || a[n]\u003c10, a[n-1]+a[n], sumdigits(a[n]%100))));Vec(a)} \\\\ Compute the vector a(0..N)",
				"M345097=A345097_vec(49); A345097(n)=if(n\u003c9, M345097[n+1], n=divrem(n-9,40); M345097[n[2]+10]+!(n[2]%2)*n[1]*200) \\\\ Instantly computes any term."
			],
			"xref": [
				"Cf. A000045, A345095 (same with rule for odd/even indexed terms exchanged)."
			],
			"keyword": "nonn,base,easy",
			"offset": "0,4",
			"author": "_M. F. Hasler_ and _Eric Angelini_, Jun 07 2021",
			"references": 2,
			"revision": 28,
			"time": "2021-06-12T14:13:05-04:00",
			"created": "2021-06-12T14:13:05-04:00"
		}
	]
}