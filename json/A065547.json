{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A065547",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 65547,
			"data": "1,0,1,0,-1,1,0,3,-3,1,0,-17,17,-6,1,0,155,-155,55,-10,1,0,-2073,2073,-736,135,-15,1,0,38227,-38227,13573,-2492,280,-21,1,0,-929569,929569,-330058,60605,-6818,518,-28,1,0,28820619,-28820619,10233219,-1879038,211419,-16086,882,-36,1,0,-1109652905",
			"name": "Triangle of Salie numbers.",
			"comment": [
				"Coefficients of polynomials H(n,x) related to Euler polynomials through H(n,x(x-1)) = E(2n,x)."
			],
			"link": [
				"D. Dumont and J. Zeng, \u003ca href=\"http://math.univ-lyon1.fr/homes-www/zeng/public_html/paper/publication.html\"\u003ePolynomes d'Euler et les fractions continues de Stieltjes-Rogers\u003c/a\u003e, Ramanujan J. 2 (1998) 3, 387-410.",
				"J. M. Hammersley, \u003ca href=\"http://www.appliedprobability.org/data/files/TMS%20articles/14_1_1.pdf\"\u003eAn undergraduate exercise in manipulation\u003c/a\u003e, Math. Scientist, 14 (1989), 1-23.",
				"Ira M. Gessel and X. G. Viennot, \u003ca href=\"http://people.brandeis.edu/~gessel/homepage/papers/pp.pdf\"\u003eDeterminants, paths and plane partitions\u003c/a\u003e, 1989, p. 27, eqn 12.1.",
				"A. F. Horadam, \u003ca href=\"http://www.fq.math.ca/Scanned/30-3/horadam.pdf\"\u003eGeneration of Genocchi polynomials of first order by recurrence relation\u003c/a\u003e, Fib. Quart. 2 (1992), 239-243."
			],
			"formula": [
				"E.g.f.: Sum((n, k=0..inf) T(n, k) t^k x^(2n)/(2n)! = cosh(sqrt(1+4t) x/2)/ cosh(x/2).",
				"T(k, n) = Sum[i=0..n-k, A028296(i)/4^(n-k)*C(2n, 2i)*C(n-l, n-k-l)], or 0 if n\u003ck.",
				"Polynomial recurrences: x^n = Sum[0\u003c=2i\u003c=n, C(n, 2i)*H(n-i, x)]; (1/4+x)^n = Sum[m=0..n, C(2n, 2m)*(1/4)^(n-m)*H(m, x)].",
				"Dumont/Zeng give a continued fraction and other formulas.",
				"Triangle T(n, k) read by rows; given by [0, -1, -2, -4, -6, -9, -12, -16, ...] DELTA A000035, where DELTA is Deléham's operator defined in A084938.",
				"Sum_{k=0..n} (-4)^(n-k)*T(n,k) = A000364(n) (Euler numbers). - _Philippe Deléham_, Oct 25 2006"
			],
			"example": [
				"Triangle begins:",
				" 1;",
				" 0,   1;",
				" 0,  -1,    1;",
				" 0,   3,   -3,  1;",
				" 0, -17,   17, -6,   1;",
				" 0, 155, -155, 55, -10, 1;",
				" ..."
			],
			"mathematica": [
				"h[n_, x_] := Sum[c[k]*x^k, {k, 0, n}]; eq[n_] := SolveAlways[h[n, x*(x - 1)] == EulerE[2*n, x], x]; row[n_] := Table[c[k], {k, 0, n}] /. eq[n] // First; Table[row[n], {n, 0, 10}] // Flatten (* _Jean-François Alcover_, Oct 02 2013 *)"
			],
			"program": [
				"(PARI) { S2(n, k) = (1/k!)*sum(i=0,k,(-1)^(k-i)*binomial(k,i)*i^n) }{ Eu(n) = sum(m=0,n,(-1)^m*m!*S2(n+1,m+1)*(-1)^floor(m/4)*2^-floor(m/2)*((m+1)%4!=0)) } T(n,k)=if(n\u003ck,0,sum(l=0,n-k,Eu(2*l)/2^(2*(n-k))*binomial(2*n,2*l)*binomial(n-l,n-k-l))) \\\\ _Ralf Stephan_"
			],
			"xref": [
				"Sum_{k\u003e=0} (-1)^(n+k)*2^(n-k)*T(n, k) = A005647(n). Sum_{k\u003e=0} (-1)^(n+k)*2^(2n-k)*T(n, k) = A000795(n). Sum_{k\u003e=0} (-1)^(n+k)*T(n, k) = A006846(n), where A006846 = Hammersley's polynomial p_n(1). - _Philippe Deléham_, Feb 26 2004.",
				"Column sequences (without leading zeros) give, for k=1..10: A065547 (twice), A095652-9.",
				"Cf. A000795, A005647, A000035.",
				"See A085707 for unsigned and transposed version.",
				"See A098435 for negative values of n, k."
			],
			"keyword": "sign,tabl",
			"offset": "0,8",
			"author": "_Wouter Meeussen_, Dec 02 2001",
			"ext": [
				"Edited by _Ralf Stephan_, Sep 08 2004"
			],
			"references": 16,
			"revision": 42,
			"time": "2019-06-09T19:18:35-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}