{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A328477",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 328477,
			"data": "1,2,3,4,5,6,7,4,8,9,10,11,12,6,10,11,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,4,8,9,10,11,28,9,29,30,31,32,33,11,31,32,34,35,36,18,37,38,39,40,41,24,42,43,44,45,46,6,10,11,13,14,33,11,31,32,34,35,47,14,34,35,48,49,50,20,39,40,51,52,53,26,44,45,54,55,56,16,17,18,19,20,36,18",
			"name": "Lexicographically earliest infinite sequence such that a(i) = a(j) =\u003e A328469(A276086(i)) = A328469(A276086(j)) for all i, j.",
			"comment": [
				"Restricted growth sequence transform of function f(n) = A328469(A276086(n)), or equally, of the function g(0) = 0; n \u003e 0, g(n) = [A053669(n), A278226(n)], where in the ordered pair A053669(n) gives the smallest prime not dividing n, while A278226(n) gives the prime signature of A276086(n), i.e., a signature of the multiset of nonzero digits in the primorial base expansion of n. Note that A000720(A053669(n)) = A055396(A276086(n)) is one more than the number of trailing zeros in the primorial base expansion for n \u003e 0."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A328477/b328477.txt\"\u003eTable of n, a(n) for n = 0..32768\u003c/a\u003e",
				"\u003ca href=\"/index/Pri#primorialbase\"\u003eIndex entries for sequences related to primorial base\u003c/a\u003e"
			],
			"example": [
				"When written in primorial base (A049345), numbers 42 (\"1200\" as 42 = 1*A002110(3) + 2*A002110(2) + 0*A002110(1) + 0*A002110(0) = 1*30 + 2*6 + 0*2 + 0*1), 66 (\"2100\" as 66 = 2*30 + 1*6 + 0*2 + 0*1) and 222 (\"10200\" as 222 = 1*210 + 0*30 + 2*6 + 0*2 + 0*1) all have {1, 2} as their multiset of nonzero digits, and all have exactly two trailing zeros, thus they get an equal value in this sequence, namely a(42) = a(66) = a(222) = 33, where 33 is a running number allotted by the restricted growth sequence transform."
			],
			"program": [
				"(PARI)",
				"up_to = 32768;",
				"rgs_transform(invec) = { my(om = Map(), outvec = vector(length(invec)), u=1); for(i=1, length(invec), if(mapisdefined(om,invec[i]), my(pp = mapget(om, invec[i])); outvec[i] = outvec[pp] , mapput(om,invec[i],i); outvec[i] = u; u++ )); outvec; };",
				"A020639(n) = if(n\u003e1, if(n\u003en=factor(n, 0)[1, 1], n, factor(n)[1, 1]), 1);",
				"A046523(n) = { my(f=vecsort(factor(n)[, 2], , 4), p); prod(i=1, #f, (p=nextprime(p+1))^f[i]); }; \\\\ From A046523",
				"A276086(n) = { my(m=1, p=2); while(n, m *= (p^(n%p)); n = n\\p; p = nextprime(1+p)); (m); };",
				"Aux328469(n) = [A020639(n), A046523(n)];",
				"Aux328477(n) = Aux328469(A276086(n));",
				"v328477 = rgs_transform(vector(1+up_to, n, Aux328477(n-1)));",
				"A328477(n) = v328477[1+n];"
			],
			"xref": [
				"Cf. A020639, A046523, A049345, A053669, A276086, A278226, A286626, A328399, A328469."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Antti Karttunen_, Oct 19 2019",
			"references": 5,
			"revision": 15,
			"time": "2019-10-19T21:27:56-04:00",
			"created": "2019-10-19T21:27:56-04:00"
		}
	]
}