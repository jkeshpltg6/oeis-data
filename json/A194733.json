{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A194733",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 194733,
			"data": "0,1,0,2,4,1,4,0,4,8,2,7,12,4,10,1,8,15,4,12,0,9,18,4,14,24,8,19,2,14,26,7,20,33,12,26,4,19,34,10,26,1,18,35,8,26,44,15,34,4,24,44,12,33,0,22,44,9,32,55,18,42,4,29,54,14,40,66,24,51,8,36,64,19,48,2,32",
			"name": "Number of k \u003c n such that {k*r} \u003e {n*r}, where { } = fractional part, r = (1+sqrt(5))/2 (the golden ratio).",
			"comment": [
				"The maximum possible value of a(n) is n-1. - Michael B. Porter, Jan 29 2012"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A194733/b194733.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e"
			],
			"formula": [
				"a(n)+A019587(n)=n."
			],
			"example": [
				"r = 1.618, 2r = 3.236, 3r = 4.854, and 4r = 6.472, where r=(1+sqrt(5))/2.  The fractional part of 4r is 0.472, which is less than the fractional parts of two of {r, 2r, 3r}, so a(4) = 2. - Michael B. Porter, Jan 29 2012"
			],
			"maple": [
				"Digits := 100;",
				"A194733 := proc(n::posint)",
				"    local a,k,phi,kfrac,nfrac ;",
				"    phi := (1+sqrt(5))/2 ;",
				"    a :=0 ;",
				"    nfrac := n*phi-floor(n*phi) ;",
				"    for k from 1 to n-1 do",
				"        kfrac := k*phi-floor(k*phi) ;",
				"        if evalf(kfrac-nfrac)  \u003e 0 then",
				"            a := a+1 ;",
				"        end if;",
				"    end do:",
				"    a ;",
				"end proc:",
				"seq(A194733(n),n=1..100) ;  # _R. J. Mathar_, Aug 13 2021"
			],
			"mathematica": [
				"r = GoldenRatio; p[x_] := FractionalPart[x];",
				"u[n_, k_] := If[p[k*r] \u003c= p[n*r], 1, 0]",
				"v[n_, k_] := If[p[k*r] \u003e p[n*r], 1, 0]",
				"s[n_] := Sum[u[n, k], {k, 1, n}]",
				"t[n_] := Sum[v[n, k], {k, 1, n}]",
				"Table[s[n], {n, 1, 100}]  (* A019587 *)",
				"Table[t[n], {n, 1, 100}]  (* A194733 *)"
			],
			"program": [
				"(Haskell)",
				"a194733 n = length $ filter (nTau \u003c) $",
				"            map (snd . properFraction . (* tau) . fromInteger) [1..n]",
				"   where (_, nTau) = properFraction (tau * fromInteger n)",
				"         tau = (1 + sqrt 5) / 2",
				"-- _Reinhard Zumkeller_, Jan 28 2012"
			],
			"xref": [
				"Cf. A019587, A194738."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Clark Kimberling_, Sep 02 2011",
			"references": 4,
			"revision": 21,
			"time": "2021-08-13T08:52:54-04:00",
			"created": "2011-09-02T17:41:32-04:00"
		}
	]
}