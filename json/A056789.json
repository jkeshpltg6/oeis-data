{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A056789",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 56789,
			"data": "1,3,10,19,51,48,148,147,253,253,606,352,1015,738,960,1171,2313,1263,3250,1869,2803,3028,5820,2784,6301,5073,6814,5458,11775,4798,14416,9363,11505,11563,14898,9343,24643,16248,19276,14797,33621,14013,38830",
			"name": "a(n) = Sum_{k=1..n} lcm(k,n)/gcd(k,n).",
			"comment": [
				"For prime p, a(p) = 1 + p^2*(p-1)/2.",
				"a(n) \u003e n^2*phi(n)/2. - _Thomas Ordowski_, Nov 08 2014",
				"We note lcm(k,n) = k*n iff gcd(k,n) = 1 (and in general lcm(k,n) equals k*n/gcd(k,n)), and so for these values LCM/GCD = k*n. From A023896, we have that Sum_{k=1..n-1: gcd(k,n)=1} k = n*phi(n)/2, and so Sum_{k=1..n-1: gcd(k,n)=1} k*n = n * Sum_{k=1..n-1: gcd(k,n)=1} k = n^2*phi(n)/2. As this is true, certainly Sum_{k=1..n} lcm(k,n)/gcd(k,n) \u003e n^2*phi(n)/2. - _Jon Perry_, Nov 09 2014 [Edited by _Petros Hadjicostas_, May 27 2020]",
				"Conjecture: for prime p, a(p^n) = 1 + (1/2)*(p - 1)*p^2*(p^(3*n) - 1)/(p^3 - 1) for n = 1,2,3,.... Cf. A339384. - _Peter Bala_, Dec 04 2020",
				"The conjecture can be proven by splitting up the sum like this: a(p^n) = 1 + Sum_{1 \u003c= r \u003c p^n if gcd(p,r) = 1} lcm(p^n,r)/gcd(p^n,r) + Sum_{1 \u003c= r \u003c p^(n-1) if gcd(p,r) = 1} lcm(p^n,p*r)/gcd(p^n,p*r) + … + Sum_{1 \u003c= r \u003c p if gcd(p,r) = 1} lcm(p^n,p^(n-1)*r)/gcd(p^n,p^(n-1)*r) = 1 + Sum_{1 \u003c= r \u003c p^n if gcd(p,r) = 1} p^n*r + Sum_{1 \u003c= r \u003c p^(n-1) if gcd(p,r) = 1} p^(n-1)*r + … + Sum_{1 \u003c= r \u003c p if gcd(p,r) = 1} p*r = 1 + p^n*(1/2)*p^n*phi(p^n) +  p^(n-1)*(1/2)*p^(n-1)*phi(p^(n-1)) + … + p*(1/2)*p*phi(p) = 1 + (1/2)*(p-1)*Sum_{k=1..n} p^(3k-1) = 1 + (1/2)*(p-1)*p^2*(p^(3*n)-1)/(p^3-1). - _Sebastian Karlsson_, Dec 07 2020"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A056789/b056789.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Sum_{k=1..n} k*n/gcd(k,n)^2. - _Thomas Ordowski_, Nov 08 2014",
				"a(n) = (1/2)*Sum_{d|n} d^2*(d+1) Sum_{j|n/d} mu(j)*j^2. - _Felix A. Pahl_, Nov 23 2019",
				"a(n) = 1 + Sum_{d|n, d \u003e 1} phi(d^3)/2. - _Daniel Suteu_, Dec 10 2020"
			],
			"example": [
				"a(6) = 6/1 + 6/2 + 6/3 + 12/2 + 30/1 + 6/6 = 48."
			],
			"mathematica": [
				"Table[ Sum[ LCM[k, n] / GCD[k, n], {k, 1, n}], {n, 1, 50}]"
			],
			"program": [
				"(Haskell)",
				"a056789 = sum . a051537_row  -- _Reinhard Zumkeller_, Jul 07 2013",
				"(PARI) vector(50, n, sum(k=1, n, lcm(k,n)/gcd(k,n))) \\\\ _Michel Marcus_, Nov 08 2014",
				"(PARI) a(n) = sumdiv(n, d, if(d\u003e1, d^2*eulerphi(d)/2, 1)); \\\\ _Daniel Suteu_, Dec 10 2020"
			],
			"xref": [
				"Row sums of triangle in A051537.",
				"Cf. A023896, A339384."
			],
			"keyword": "nonn,nice",
			"offset": "1,2",
			"author": "_Leroy Quet_, Aug 20 2000",
			"ext": [
				"Additional comments from _Amarnath Murthy_, May 09 2002"
			],
			"references": 10,
			"revision": 57,
			"time": "2020-12-12T19:54:09-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}