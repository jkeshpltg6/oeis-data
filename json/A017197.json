{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A017197",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 17197,
			"data": "3,12,21,30,39,48,57,66,75,84,93,102,111,120,129,138,147,156,165,174,183,192,201,210,219,228,237,246,255,264,273,282,291,300,309,318,327,336,345,354,363,372,381,390,399,408,417,426,435,444,453,462,471,480",
			"name": "a(n) = 9*n + 3.",
			"comment": [
				"Numbers whose digital root is 3. - _Cino Hilliard_, Dec 26 2006",
				"a(n)^2 = A017198(n). - _Reinhard Zumkeller_, Jul 13 2010"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A017197/b017197.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"Luis Manuel Rivera, \u003ca href=\"http://arxiv.org/abs/1406.3081\"\u003eInteger sequences and k-commuting permutations\u003c/a\u003e, arXiv preprint arXiv:1406.3081 [math.CO], 2014-2015.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,-1)."
			],
			"formula": [
				"a(n) = a(n-1) + 9.",
				"a(n) = 3*A016777(n).",
				"a(n) = A092292(n) + A092293(n) + A092296(n).",
				"From _Philippe Deléham_, Mar 10 2004: (Start)",
				"Sum_{n\u003e=0} (-1)^n / a(n) = (Pi / sqrt(3) + log(2))/9.",
				"G.f.: 3*(1+2*x)/(1-x)^2. (End)",
				"a(n) = 3*(6*n-1) - a(n-1) with a(0)=3. - _Vincenzo Librandi_, Nov 20 2010",
				"E.g.f.: 3*(1 + 3*x)*exp(x). - _G. C. Greubel_, Dec 03 2019"
			],
			"maple": [
				"seq(9*n+3, n=0..60); # _G. C. Greubel_, Dec 03 2019"
			],
			"mathematica": [
				"3*(3*Range[60] -2) (* _G. C. Greubel_, Dec 03 2019 *)"
			],
			"program": [
				"(PARI) vector(60, n, 3*(3*n-2) ) \\\\ _G. C. Greubel_, Dec 03 2019",
				"(Sage) [i+3 for i in range(480) if gcd(i,9) == 9] # _Zerinvary Lajos_, May 20 2009",
				"(Haskell)",
				"a017197 = (+ 3) . (* 9)",
				"a017197_list = [3, 12 ..]  -- _Reinhard Zumkeller_, Jun 04 2015",
				"(MAGMA) [9*n+3: n in [0..60]]; // _G. C. Greubel_, Dec 03 2019",
				"(GAP) List([0..60], n-\u003e 9*n+3); # _G. C. Greubel_, Dec 03 2019"
			],
			"xref": [
				"Cf. A092292, A092293, A092296.",
				"Cf. sequences with general form q*(q*n+1): A016825 (q=2), this sequence (q=3), A119413 (q=4), ... - _Vladimir Joseph Stephan Orlovsky_, Feb 16 2009",
				"Cf. A016777."
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Cino Hilliard_, Dec 26 2006"
			],
			"references": 15,
			"revision": 51,
			"time": "2020-04-12T08:25:19-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}