{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A145392",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 145392,
			"data": "1,2,2,4,4,6,4,8,7,10,6,14,8,12,12,16,10,20,10,22,16,18,12,30,17,22,20,28,16,36,16,32,24,28,24,46,20,30,28,46,22,48,22,42,40,36,24,62,29,48,36,50,28,60,36,60,40,46,30,84,32,48,52,64,44,72,34,64,48,72",
			"name": "Number of inequivalent sublattices of index n in square lattice, where two sublattices are considered equivalent if one can be rotated by a multiple of Pi/2 to give the other.",
			"comment": [
				"From _Andrey Zabolotskiy_, Mar 12 2018: (Start)",
				"The parent lattice of the sublattices under consideration has Patterson symmetry group p4, and two sublattices are considered equivalent if they are related via a symmetry from that group [Rutherford]. For other 2D Patterson groups, the analogous sequences are A000203 (p2), A069734 (p2mm), A145391 (c2mm), A145393 (p4mm), A145394 (p6), A003051 (p6mm).",
				"If we count sublattices related by parent-lattice-preserving reflection as equivalent, we get A145393 instead of this sequence. If we count sublattices related by rotation of the sublattice only (but not parent lattice; equivalently, sublattices related by rotation by angle which is not a multiple of Pi/2; see illustration in links) as equivalent, we get A054345. If we count sublattices related by any rotation or reflection as equivalent, we get A054346.",
				"Rutherford says at p. 161 that a(n) != A054345(n) only when A002654(n) \u003e 1, but actually these two sequences differ at other terms, too, for example, at n = 15 (see illustration). (End)"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A145392/b145392.txt\"\u003eTable of n, a(n) for n = 1..16384\u003c/a\u003e",
				"John S. Rutherford, \u003ca href=\"http://dx.doi.org/10.1107/S010876730804333X\"\u003eSublattice enumeration. IV. Equivalence classes of plane sublattices by parent Patterson symmetry and colour lattice group type\u003c/a\u003e, Acta Cryst. (2009). A65, 156-163. [See Table 2; beware the typo in a(13).]",
				"Andrey Zabolotskiy, \u003ca href=\"/A145392/a145392.pdf\"\u003eSublattices of the square lattice\u003c/a\u003e (illustrations for n = 1..6, 15, 25)",
				"\u003ca href=\"/index/Su#sublatts\"\u003eIndex entries for sequences related to sublattices\u003c/a\u003e",
				"\u003ca href=\"/index/Sq#sqlatt\"\u003eIndex entries for sequences related to square lattice\u003c/a\u003e"
			],
			"formula": [
				"a(n) = (A000203(n) + A002654(n))/2. [Rutherford] - _N. J. A. Sloane_, Mar 13 2009",
				"a(n) = Sum_{ m: m^2|n } A000089(n/m^2) + A157224(n/m^2) = A002654(n) + Sum_{ m: m^2|n } A157224(n/m^2). - _Andrey Zabolotskiy_, May 07 2018",
				"a(n) = Sum_{ d|n } A004525(d). - _Andrey Zabolotskiy_, Aug 29 2019"
			],
			"program": [
				"(PARI)",
				"A002654(n) = sumdiv(n, d, (d%4==1) - (d%4==3));",
				"A145392(n) = ((sigma(n) + A002654(n))/2); \\\\ _Antti Karttunen_, Nov 23 2017"
			],
			"xref": [
				"Cf. A000203, A002654, A069734, A145391, A145393, A145394, A003051, A054345, A054346, A000089, A157224, A004525."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, Feb 23 2009",
			"ext": [
				"New name from _Andrey Zabolotskiy_, Mar 12 2018"
			],
			"references": 10,
			"revision": 29,
			"time": "2021-04-04T08:36:36-04:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}