{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A119407",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 119407,
			"data": "1,3,7,15,31,62,122,238,462,894,1727,3333,6429,12397,23901,46076,88820,171212,330028,636156,1226237,2363655,4556099,8782171,16928187,32630138,62896622,121237146,233692122,450456058,868281979,1673667337,3226097529,6218502937,11986549817,23104817656",
			"name": "Number of nonempty subsets of {1,2,...,n} with no gap of length greater than 4 (a set S has a gap of length d if a and b are in S but no x with a \u003c x \u003c b is in S, where b-a=d).",
			"comment": [
				"The numbers of subsets of {1,2,...,n} with no gap of length greater than d, for d=1,2 and 3, seem to be given in A000217, A001924 and A062544, respectively."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A119407/b119407.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-2,0,0,-1,1)."
			],
			"formula": [
				"G.f. for number of nonempty subsets of {1,2,...,n} with no gap of length greater than d is x/((1-x)*(1-2*x+x^(d+1))). - _Vladeta Jovovic_, Apr 27 2008",
				"From _Michael Somos_, Dec 28 2012: (Start)",
				"G.f.: x/((1-x)^2*(1-x-x^2-x^3-x^4)) = x/((1-x)*(1-2*x+x^5)).",
				"First difference is A107066. (End)",
				"a(n-3) = Sum_{k=0..n} (n-k)*A000078(k) for n\u003e3. - _Greg Dresden_, Jan 01 2021"
			],
			"example": [
				"G.f. = x + 3*x^2 + 7*x^3 + 15*x^4 + 31*x^5 + 62*x^6 + 122*x^7 + 238*x^8 + 462*x^9 + ..."
			],
			"mathematica": [
				"Rest@CoefficientList[Series[x/((1-x)*(1-2*x+x^5)), {x, 0, 40}], x] (* _G. C. Greubel_, Jun 05 2019 *)",
				"LinearRecurrence[{3,-2,0,0,-1,1},{1,3,7,15,31,62},40] (* _Harvey P. Dale_, Dec 04 2019 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, n = -n; polcoeff( x^5 / ((1 - x)^2 * (1 + x + x^2 + x^3 - x^4)) + x * O(x^n), n), polcoeff( x / ((1 - x)^2 * (1 - x - x^2 - x^3 - x^4)) + x * O(x^n), n))} /* _Michael Somos_, Dec 28 2012 */",
				"(PARI) my(x='x+O('x^40)); Vec(x/((1-x)*(1-2*x+x^5))) \\\\ _G. C. Greubel_, Jun 05 2019",
				"(MAGMA) R\u003cx\u003e:=PowerSeriesRing(Integers(), 40); Coefficients(R!( x/((1-x)*(1-2*x+x^5)) )); // _G. C. Greubel_, Jun 05 2019",
				"(Sage) a=(x/((1-x)*(1-2*x+x^5))).series(x, 40).coefficients(x, sparse=False); a[1:] # _G. C. Greubel_, Jun 05 2019"
			],
			"xref": [
				"Cf. A000217, A001924, A062544, A107066."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_John W. Layman_, Jul 25 2006",
			"ext": [
				"Terms a(25) onward added by _G. C. Greubel_, Jun 05 2019"
			],
			"references": 3,
			"revision": 30,
			"time": "2021-01-03T17:31:38-05:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}