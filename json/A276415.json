{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A276415",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 276415,
			"data": "0,1,3,3,2,2,2,2,1,1,3,3,3,3,2,1,1,3,5,4,3,2,2,3,2,2,3,2,4,5,5,4,3,2,3,1,3,4,4,3,3,2,3,3,5,4,4,4,2,3,2,1,3,4,3,3,2,2,3,4,4,4,2,2,2,2,5,5,5,4,4,4,2,4,5,3,3,2,2,5",
			"name": "Number of ways to write n as p + x^3 + y^4, where p is a prime, and x and y are nonnegative integers.",
			"comment": [
				"Conjecture: (i) a(n) \u003e 0 for all n \u003e 1, and a(n) = 1 only for n = 2, 9, 10, 16, 17, 36, 52, 502.",
				"(ii) Any integer n \u003e 1 can be written as p + x^3 + 2*y^3, where p is a prime, and x and y are nonnegative integers.",
				"(iii) Any integer n \u003e 2 can be written as p + ((q-1)/2)^2 + x^4, where p and q are primes, and x is a nonnegative integer.",
				"(iv) Any integer n \u003e 5 can be written as p + q^2 + x^2, where p and q are primes, and x is a nonnegative integer.",
				"(v) Any integer n \u003e 5 can be written as p + q^2 + ((r-3)/2)^3, where p and q are primes, and r is an odd prime.",
				"Ju. V. Linnik proved in 1960 that any sufficiently large integer can be written as the sum of a prime and two squares."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A276415/b276415.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Ju. V. Linnik, \u003ca href=\"http://mi.mathnet.ru/eng/izv3674\"\u003eAn asymptotic formula in an additive problem of Hardy-Littlewood\u003c/a\u003e, Izv. Akad. Nauk SSSR Ser. Mat., 24 (1960), 629-706 (Russian)."
			],
			"example": [
				"a(2) = 1 since 2 = 2 + 0^3 + 0^4 with 2 prime.",
				"a(6) = 2 since 6 = 5 + 0^3 + 1^4 = 5 + 1^3 + 0^4 with 5 prime.",
				"a(9) = 1 since 9 = 7 + 1^3 + 1^4 with 7 prime.",
				"a(10) = 1 since 10 = 2 + 2^3 + 0^4 with 2 prime.",
				"a(16) = 1 since 16 = 7 + 2^3 + 1^4 with 7 prime.",
				"a(17) = 1 since 17 = 17 + 0^3 + 0^4 with 17 prime.",
				"a(36) = 1 since 36 = 19 + 1^3 + 2^4 with 19 prime.",
				"a(52) = 1 since 52 = 43 + 2^3 + 1^4 with 43 prime.",
				"a(502) = 1 since 502 = 421 + 0^3 + 3^4 with 421 prime."
			],
			"maple": [
				"N:= 1000: # to get a(1) to a(N)",
				"A:= Vector(N):",
				"for p in select(isprime, [2,seq(i,i=3..N,2)]) do",
				"  for x from 0 while p + x^3 \u003c= N do",
				"    for y from 0 while p + x^3 + y^4 \u003c= N do",
				"       r:= p+x^3+y^4;",
				"       A[r]:= A[r]+1",
				"od od od:",
				"convert(A,list); # _Robert Israel_, Oct 05 2016"
			],
			"mathematica": [
				"Do[r=0;Do[If[PrimeQ[n-x^3-y^4],r=r+1],{x,0,n^(1/3)},{y,0,(n-x^3)^(1/4)}];Print[n,\" \",r];Continue,{n,1,80}]"
			],
			"xref": [
				"Cf. A000040, A000290, A000578, A000583."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Zhi-Wei Sun_, Sep 27 2016",
			"references": 3,
			"revision": 26,
			"time": "2016-10-06T02:44:23-04:00",
			"created": "2016-09-27T05:31:24-04:00"
		}
	]
}