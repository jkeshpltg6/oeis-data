{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A289814",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 289814,
			"data": "0,0,1,0,0,1,2,2,3,0,0,1,0,0,1,2,2,3,4,4,5,4,4,5,6,6,7,0,0,1,0,0,1,2,2,3,0,0,1,0,0,1,2,2,3,4,4,5,4,4,5,6,6,7,8,8,9,8,8,9,10,10,11,8,8,9,8,8,9,10,10,11,12,12,13,12,12,13,14,14,15,0",
			"name": "A binary encoding of the twos in ternary representation of n (see Comments for precise definition).",
			"comment": [
				"The ones in the binary representation of a(n) correspond to the twos in the ternary representation of n; for example: ternary(42) = 1120 and binary(a(42)) = 10 (a(42) = 2).",
				"See A289813 for the sequence encoding the ones in ternary representation of n and additional comments."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A289814/b289814.txt\"\u003eTable of n, a(n) for n = 0..6560\u003c/a\u003e"
			],
			"formula": [
				"a(0) = 0.",
				"a(3*n) = 2 * a(n).",
				"a(3*n+1) = 2 * a(n).",
				"a(3*n+2) = 2 * a(n) + 1.",
				"Also, a(n) = A289813(A004488(n)).",
				"A053735(n) = A000120(A289813(n)) + 2*A000120(a(n)). - _Antti Karttunen_, Jul 20 2017"
			],
			"example": [
				"The first values, alongside the ternary representation of n, and the binary representation of a(n), are:",
				"n       a(n)    ternary(n)  binary(a(n))",
				"--      ----    ----------  ------------",
				"0       0       0           0",
				"1       0       1           0",
				"2       1       2           1",
				"3       0       10          0",
				"4       0       11          0",
				"5       1       12          1",
				"6       2       20          10",
				"7       2       21          10",
				"8       3       22          11",
				"9       0       100         0",
				"10      0       101         0",
				"11      1       102         1",
				"12      0       110         0",
				"13      0       111         0",
				"14      1       112         1",
				"15      2       120         10",
				"16      2       121         10",
				"17      3       122         11",
				"18      4       200         100",
				"19      4       201         100",
				"20      5       202         101",
				"21      4       210         100",
				"22      4       211         100",
				"23      5       212         101",
				"24      6       220         110",
				"25      6       221         110",
				"26      7       222         111"
			],
			"mathematica": [
				"Table[FromDigits[#, 2] \u0026[IntegerDigits[n, 3] /. d_ /; d \u003e 0 :\u003e d - 1], {n, 0, 81}] (* _Michael De Vlieger_, Jul 20 2017 *)"
			],
			"program": [
				"(PARI) a(n) = my (d=digits(n,3)); fromdigits(vector(#d, i, if (d[i]==2, 1, 0)), 2)",
				"(Python)",
				"from sympy.ntheory.factor_ import digits",
				"def a(n):",
				"    d = digits(n, 3)[1:]",
				"    return int(\"\".join('1' if i == 2 else '0' for i in d), 2)",
				"print([a(n) for n in range(101)]) # _Indranil Ghosh_, Jul 20 2017"
			],
			"xref": [
				"Cf. A000120, A053735, A289813."
			],
			"keyword": "nonn,base,look",
			"offset": "0,7",
			"author": "_Rémy Sigrist_, Jul 12 2017",
			"references": 40,
			"revision": 29,
			"time": "2020-05-03T06:02:51-04:00",
			"created": "2017-07-14T10:33:32-04:00"
		}
	]
}