{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A210258",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 210258,
			"data": "1,1,-2,1,-3,3,1,-4,4,2,-4,1,-5,5,5,-5,-5,5,1,-6,6,9,-6,-12,6,-2,6,3,-6,1,-7,7,14,-7,-21,7,-7,14,7,-7,7,-7,-7,7,1,-8,8,20,-8,-32,8,-16,24,12,-8,24,-16,-16,8,2,-8,-8,8,8,8,-8,1,-9,9,27,-9,-45",
			"name": "The coefficients of the Girard-Waring formula; irregular array T(n,k), read by rows, for n \u003e= 1 and 1 \u003c= k \u003c= A000041(n).",
			"comment": [
				"Assume we have N \u003c= n variables x_1, x_2, ..., x_N, and let S_n^{(N)} = x_1^n + x_2^n + ... + x_N^n be the n-th power sum of these variables. Following Gould (1999, p. 135), define the elementary symmetric polynomials e_1^{(N)}, e^2^{(N)}, ..., e_N^{(N)}, where e_j^{(N)} is the sum of all products of x_1, x_2, ..., x_N taken j at a time.",
				"Since N \u003c= n, without loss of generality, we may assume we have the extra variables x_{N+1}, ..., x_n, define e_1^{(n)}, e_2^{(n)}, ..., e_N^{(n)}, e_{N+1}^{(n)}, ..., e_n^{(n)}, and then set x_{N+1} = x_{N+2} = ... = x_n = 0. In such a case, we get e_j^{(N)} = e_j^{(n)} for j = 1..N, and e_j^{(n)} = 0 for j = (N+1)..n. Thus, we may drop the superscripts N and n on power sum and on the elementary symmetric polynomials and write S_n, and e_1, e_2, ..., e_n with the understanding that e_{N+1} = ... = e_n = 0. (See also the comments for A115131.)",
				"The numbers T(n,k) are the coefficients of the power sum expansion in terms of the elementary symmetric polynomials, which is the Girard-Waring formula. That is, S_n = Sum(c(t_1,...,t_n) * e_1^t_1 * e_2^t_2 *...* e_n^t_n), summed over integer partitions of n, t_1 + 2*t_2 + ... + n*t_n = n with t_i \u003e= 0 for i = 1..n. Here c(t_1,t_2,...,t_n) = n * (-1)^(t_2 + t_4 + ... + t_{2*floor(n/2)}) * (t_1 + ... + t_n - 1)!/(t_1!*...*t_n!).",
				"Given a partition (t_1,...,t_n) of n (as defined above), we may use only those j's in {1,...,n} for which t_j \u003e 0 and write the partition in the usual notation by repeating each such j t_j times (and place these j's in a non-descending order). E.g., the partition 1*3 + 2*1 + 3*0 + 4*0 + 5*0 of 5 can be written as [1,1,1,2]. Similarly, the partition 1*5 + 2*0 + 3*0 + 4*0 + 5*0 of 5 can be written as [1,1,1,1,1].",
				"Instead of using the Abramowitz-Stegun order of partitions (as it is done in A115131), we use the usual notation for partitions (in terms of the positive integers that add up to n) and order them in lexicographic order. Unfortunately, this order of partitions does not correspond to any of the orders in the web link below.",
				"If we let a(m) be the m-th term of the array, read as sequence, then",
				"a(1) = T(1,1) = c([1]) = c(1),",
				"a(2) = T(2,1) = c([1,1]) = c(2,0) with sign(T(2,1)) = (-1)^0 = 1,",
				"a(3) = T(2,2) = c([2]) = c(0,1) with sign(T(2,2)) = (-1)^1 = -1,",
				"a(4) = T(3,1) = c([1,1,1]) = c(3,0,0) with sign(T(3,1)) = (-1)^0 = 1,",
				"a(5) = T(3,2) = c([1,2]) = c(1,1,0) with sign(T(4,1)) = (-1)^1 = -1,",
				"a(6) = T(3,3) = c([3]) = c(0,0,1) with sign(T(3,3)) = (-1)^0 = 1,",
				"a(7) = T(4,1) = c([1,1,1,1]) = c(4,0,0,0) with sign(T(4,1)) = (-1)^(0+0) = 1,",
				"a(8) = T(4,2) = c([1,1,2]) = c(2,1,0,0) with sign(T(4,2)) = (-1)^(1+0) = -1,",
				"a(9) = T(4,3) = c([1,3]) = c(1,0,1,0) with sign(T(4,3)) = (-1)^(0+0) = 1,",
				"a(10) = T(4,4) = c([2,2]) = c(0,2,0,0) with sign(T(4,4)) = (-1)^(2+0) = 1,",
				"a(11) = T(4,5) = c([4]) = c(0,0,0,1) with sign(T(4,5)) = (-1)^(0+1) = -1,",
				"a(12) = T(5,1) = c([1,1,1,1,1]) = c(5,0,0,0,0),",
				"a(13) = T(5,2) = c([1,1,1,2]) = c(3,1,0,0,0),",
				"a(14) = T(5,3) = c([1,1,3]) = c(2,0,1,0,0),",
				"a(15) = T(5,4) = c([1,2,2]) = c(1,2,0,0,0),",
				"a(16) = T(5,5) = c([1,4]) = c(1,0,0,1,0),",
				"a(17) = T(5,6) = c([2,3]) = c(0,1,1,0,0),",
				"a(18) = T(5,7) = c([5]) = c(0,0,0,0,1), ...",
				"(ascending ordered compositions in lexicographic order)."
			],
			"link": [
				"Kahtan H. Alzubaidy, \u003ca href=\"https://www.maplesoft.com/applications/view.aspx?SID=153837\"\u003eSymmetric polynomials by Maple\u003c/a\u003e, 2015 (requires Maple 13).",
				"Henri W. Gould, \u003ca href=\"http://www.fq.math.ca/Scanned/37-2/gould.pdf\"\u003eThe Girard-Waring power sum formulas for symmetric functions and Fibonacci sequences\u003c/a\u003e, Fibonacci Quart. 37(2) (1999), 135-140. [He uses a signed version of the elementary symmetric polynomials: a_j = (-1)^j*e_j. This is why here we have (-1)^(t_2 + t_4 + ... + t_{2*floor(n/2)}) in the formula for c(t_1,...,t_n) rather than (-1)^(t_1 + ... + t_n).]",
				"OEIS, \u003ca href=\"https://oeis.org/wiki/Orderings_of_partitions\"\u003eOrderings of partitions\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Newton%27s_identities\"\u003eNewton's identities\u003c/a\u003e."
			],
			"example": [
				"Array T(n,k) (with rows n \u003e= 1 and columns k \u003e= 1) begins as follows:",
				"  S_1: 1;",
				"  S_2: 1, -2;",
				"  S_3: 1, -3, 3;",
				"  S_4: 1, -4, 4,  2, -4;",
				"  S_5: 1, -5, 5,  5, -5,  -5, 5;",
				"  S_6: 1, -6, 6,  9, -6, -12, 6, -2,  6, 3, -6;",
				"  S_7: 1, -7, 7, 14, -7, -21, 7, -7, 14, 7, -7, 7, -7, -7, 7;",
				"  ...",
				"With N = n = 6, we have S_6 = 1*(e_1)^6 - 6*(e_1)^4*(e_2) + 6*(e_1)^3*(e_3) + 9*(e_1)^2*(e_2)^2 - 6*(e_1)^2*(e_4) - 12*(e_1)*(e_2)*(e_3) + 6*(e_1)*(e_5) - 2*(e_2)^3 + 6*(e_2)*(e_4) + 3*(e_3)^2 - 6*(e_6) = Sum_{i = 1..6} x_i^6.",
				"If N = 4 \u003c n = 6, we set e_5 = e_6 = 0 in the above expression, and we get that S_6 = 1*(e_1)^6 - 6*(e_1)^4*(e_2) + 6*(e_1)^3*(e_3) + 9*(e_1)^2*(e_2)^2 - 6*(e_1)^2*(e_4) - 12*(e_1)*(e_2)*(e_3) - 2*(e_2)^3 + 6*(e_2)*(e_4) + 3*(e_3)^2 = Sum_{i = 1..4} x_i^6."
			],
			"xref": [
				"Cf. A115131 (Abramowitz-Stegun order of partitions)."
			],
			"keyword": "sign,tabf",
			"offset": "1,3",
			"author": "_Mircea Merca_, Mar 19 2012",
			"ext": [
				"Various sections edited by _Petros Hadjicostas_, Dec 14 2019"
			],
			"references": 13,
			"revision": 65,
			"time": "2019-12-21T18:26:48-05:00",
			"created": "2012-03-20T13:52:17-04:00"
		}
	]
}