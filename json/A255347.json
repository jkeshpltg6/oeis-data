{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A255347",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 255347,
			"data": "0,1,2,3,5,5,6,7,6,9,10,11,15,13,14,15,12,17,18,19,25,21,22,23,18,25,26,27,35,29,30,31,24,33,34,35,45,37,38,39,30,41,42,43,55,45,46,47,36,49,50,51,65,53,54,55,42,57,58,59,75,61,62,63,48,65,66",
			"name": "a(n) = n * (1 - (-1)^(n/4) / 4) if n divisible by 4, a(n) = n otherwise.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A255347/b255347.txt\"\u003eTable of n, a(n) for n = 0..2500\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"http://grail.eecs.csuohio.edu/~somos/rfmc.txt\"\u003eRational Function Multiplicative Coefficients\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_10\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,-1,0,-2,4,-2,0,-1,2,-1)."
			],
			"formula": [
				"Euler transform of length 10 sequence [2, 0, 1, -2, 1, -1, 0, 2, 0, -1].",
				"a(n) is multiplicative with a(2) = 2, a(4) = 5, a(2^e) = 3*2^(e-1) if e\u003e2, a(p^e) = p^e otherwise.",
				"G.f.: f(x) - f(-x^4) where f(x) := x / (1 - x)^2.",
				"G.f.: x * (1 + x^3) * (1 + x^5) / ((1 - x)^2 * (1 + x^4)^2).",
				"a(n) = -a(-n) for all n in Z."
			],
			"example": [
				"G.f. = x + 2*x^2 + 3*x^3 + 5*x^4 + 5*x^5 + 6*x^6 + 7*x^7 + 6*x^8 + 9*x^9 + ..."
			],
			"mathematica": [
				"a[ n_] := n {1, 1, 1, 5/4, 1, 1, 1, 3/4}[[Mod[ n, 8, 1]]];",
				"a[ n_] := n If[ Divisible[ n, 4], 1 - (-1)^(n/4) / 4, 1];",
				"LinearRecurrence[{2,-1,0,-2,4,-2,0,-1,2,-1},{0,1,2,3,5,5,6,7,6,9},70] (* _Harvey P. Dale_, Jul 28 2018 *)",
				"CoefficientList[Series[x*(1+x^3)*(1+x^5)/((1-x)^2*(1+x^4)^2), {x,0,60}], x] (* _G. C. Greubel_, Aug 02 2018 *)"
			],
			"program": [
				"(PARI) {a(n) = n * if( n%4, 1, 1 - (-1)^(n/4) / 4)};",
				"(PARI) {a(n) = n * [3/4, 1, 1, 1, 5/4, 1, 1, 1][n%8 + 1]};",
				"(PARI) x='x+O('x^60); concat([0], Vec(x*(1+x^3)*(1+x^5)/((1-x)^2*(1 + x^4)^2))) \\\\ _G. C. Greubel_, Aug 02 2018",
				"(MAGMA) m:=60; R\u003cx\u003e:=PowerSeriesRing(Integers(), m); [0] cat Coefficients(R!(x*(1+x^3)*(1+x^5)/((1-x)^2*(1+x^4)^2))); // _G. C. Greubel_, Aug 02 2018"
			],
			"keyword": "nonn,mult,easy",
			"offset": "0,3",
			"author": "_Michael Somos_, May 04 2015",
			"references": 1,
			"revision": 48,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2015-05-04T22:04:10-04:00"
		}
	]
}