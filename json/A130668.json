{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A130668",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 130668,
			"data": "0,0,1,-2,5,-11,23,-48,102,-220,476,-1024,2184,-4624,9744,-20480,42976,-90048,188352,-393216,819328,-1704192,3539200,-7340032,15203840,-31456256,65010688,-134217728,276826112,-570429440,1174409216",
			"name": "Diagonal of A129819.",
			"comment": [
				"This sequence is connected to A124072. To see this, change the sign of every negative term and consider the differences of every line. Hence for the second line, and following lines, the four terms form periodic sequences:",
				"    0   1   0   1   0",
				"    1   0   0   1   1",
				"    1   0   1   2   1",
				"    1   1   3   3   1",
				"    2   4   6   4   2",
				"    6  10  10   6   6",
				"   16  20  16  12  16",
				"   36  36  28  28  36",
				"   72  64  56  64  72",
				"  136 120 120 136 136",
				"  256 240 256 272 256.",
				"The lines are connected as seen by the examples: (3rd line connected to 2nd, from right to left) 1+1=2, 1+0=1, 0+0=0, 0+1=1; (11th line connected to 10th) 136+136=272, 136+120=256, 120+120=240, 120+136=256.",
				"The 4 columns are almost known (must the first line be suppressed?): A038503 (without the first 1), A000749 (without the first 0), A038505, A038504. Like the present sequence, every sequence of A124072 beginning with a negative number (-2, -11, ...) is a \"twisted\" sequence (see A129339 comments, A129961 and the present 4 columns). Periodic with period 2^n.",
				"Inverse binomial transform of A129819. - _R. J. Mathar_, Feb 25 2009"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A130668/b130668.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (-6,-14,-16,-8)."
			],
			"formula": [
				"From _R. J. Mathar_, Feb 25 2009: (Start)",
				"G.f.: x^2*(1+x)*(1 + 3*x + 4*x^2 + 3*x^3)/((1 + 2*x + 2*x^2)*(1+2*x)^2).",
				"a(n) = ((-1)^n*A001787(n+1) - 4*A108520(n) + 4*A122803(n))/32, n \u003e 2. (End)",
				"a(n) = -6*a(n-1) - 14*a(n-2) - 16*a(n-3) - 8*a(n-4) for n \u003e= 7. - _G. C. Greubel_, Mar 24 2019"
			],
			"mathematica": [
				"gf = x^2*(1+x)*(1+3*x+4*x^2+3*x^3)/((1+2*x+2*x^2)*(1+2*x)^2); CoefficientList[Series[gf, {x, 0, 30}], x] (* _Jean-François Alcover_, Dec 16 2014, after _R. J. Mathar_ *)",
				"Join[{0, 0, 1}, LinearRecurrence[{-6,-14,-16,-8}, {-2,5,-11,23}, 30]] (* _Jean-François Alcover_, Feb 15 2016 *)"
			],
			"program": [
				"(PARI) my(x='x+O('x^30)); concat([0,0], Vec(x^2*(1+x)*(1+3*x+4*x^2+3*x^3 )/((1+2*x +2*x^2)*(1+2*x)^2))) \\\\ _G. C. Greubel_, Mar 24 2019",
				"(MAGMA) I:=[-2,5,-11,23]; [0,0,1] cat [n le 4 select I[n] else -6*Self(n-1) - 14*Self(n-2)-16*Self(n-3)-8*Self(n-4): n in [1..30]]; // _G. C. Greubel_, Mar 24 2019",
				"(Sage) (x^2*(1+x)*(1+3*x+4*x^2+3*x^3)/((1+2*x+2*x^2)*(1+2*x)^2 )).series(x, 30).coefficients(x, sparse=False) # _G. C. Greubel_, Mar 24 2019",
				"(GAP) a:=[-2,5,-11,23];; for n in [5..30] do a[n]:=-6*a[n-1]+-14*a[n-2] -16*a[n-3]-8*a[n-4]; od; Concatenation([0,0,1], a); # _G. C. Greubel_, Mar 24 2019"
			],
			"keyword": "sign",
			"offset": "0,4",
			"author": "_Paul Curtz_, Jun 27 2007",
			"ext": [
				"Extended by _R. J. Mathar_, Feb 25 2009"
			],
			"references": 2,
			"revision": 26,
			"time": "2019-04-19T11:07:39-04:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}