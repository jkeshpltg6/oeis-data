{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002760",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2760,
			"data": "0,1,4,8,9,16,25,27,36,49,64,81,100,121,125,144,169,196,216,225,256,289,324,343,361,400,441,484,512,529,576,625,676,729,784,841,900,961,1000,1024,1089,1156,1225,1296,1331,1369,1444,1521,1600,1681,1728,1764,1849",
			"name": "Squares and cubes.",
			"comment": [
				"Catalan's Conjecture states that 8 and 9 are the only pair of consecutive numbers in this sequence. The conjecture was established in 2003 by Mihilescu.",
				"Subsequence of A022549. - _Reinhard Zumkeller_, Jul 17 2010"
			],
			"reference": [
				"Clifford A. Pickover, A Passion for Mathematics, Wiley, 2005; see p. 68.",
				"Clifford A. Pickover, The Math Book, Sterling, NY, 2009; see p. 236."
			],
			"link": [
				"Zak Seidov and Michael De Vlieger, \u003ca href=\"/A002760/b002760.txt\"\u003eTable of n, a(n) for n = 1..10443\u003c/a\u003e (First 1000 terms from Zak Seidov)",
				"Yuri F. Bilu, \u003ca href=\"http://www.numdam.org/book-part/SB_2002-2003__45__1_0/\"\u003eCatalan's Conjecture (After Mihilescu)\u003c/a\u003e, Astérisque, No. 294, 1-26, 2004.",
				"Yuri F. Bilu, \u003ca href=\"http://dx.doi.org/10.5802/jtnb.478\"\u003eCatalan Without Logarithmic Forms (after Bugeaud, Hanrot and Mihailescu)\u003c/a\u003e, J. Théor. Nombres Bordeaux 17, 69-85, 2005.",
				"David Masser, \u003ca href=\"https://arxiv.org/abs/2010.10256\"\u003eAlan Baker\u003c/a\u003e, arXiv:2010.10256 [math.HO], 2020. See p. 4.",
				"Tauno Metsänkylä, \u003ca href=\"http://dx.doi.org/10.1090/S0273-0979-03-00993-5\"\u003eCatalan's conjecture: another old Diophantine problem solved\u003c/a\u003e, Bull. Amer. Math. Soc. (NS), Vol. 41, No. 1 (2004), pp. 43-57.",
				"Preda Mihǎilescu, \u003ca href=\"http://dx.doi.org/10.1016/S0022-314X(02)00101-4\"\u003eA Class Number Free Criterion for Catalan's Conjecture\u003c/a\u003e, J. Number Th. 99 225-231, 2003.",
				"Preda Mihǎilescu, \u003ca href=\"http://dx.doi.org/10.1515/crll.2004.048\"\u003ePrimary Cyclotomic Units and a Proof of Catalan's Conjecture\u003c/a\u003e, J. Reine angew. Math. 572 (2004): 167-195. MR 2076124.",
				"Paulo Ribenboim, \u003ca href=\"http://www.numdam.org/item?id=SPHM_1994___6_A1_0\"\u003eCatalan's Conjecture\u003c/a\u003e, Séminaire de Philosophie et Mathématiques, 6 (1994), p. 1-11.",
				"Paulo Ribenboim, \u003ca href=\"http://www.jstor.org/stable/2974663\"\u003eCatalan's Conjecture\u003c/a\u003e, Amer. Math. Monthly, Vol. 103(7) Aug-Sept 1996, pp. 529-538."
			],
			"formula": [
				"Sum_{n\u003e=2} 1/a(n) = zeta(2) + zeta(3) - zeta(6). - _Amiram Eldar_, Dec 19 2020"
			],
			"mathematica": [
				"nMax=2000;Union[Range[0,nMax^(1/2)]^2,Range[0,nMax^(1/3)]^3] (* _Vladimir Joseph Stephan Orlovsky_, Apr 11 2011 *)",
				"nxt[n_] := Min[ Floor[1 + Sqrt[n]]^2, Floor[1 + n^(1/3)]^3]; NestList[ nxt, 0, 55] (* _Robert G. Wilson v_, Aug 16 2014 *)"
			],
			"program": [
				"(MAGMA) [n: n in [0..1600] | IsIntegral(n^(1/3)) or IsIntegral(n^(1/2))]; // _Bruno Berselli_, Feb 09 2016",
				"(PARI) isok(n) = issquare(n) || ispower(n, 3); \\\\ _Michel Marcus_, Mar 29 2016"
			],
			"xref": [
				"Cf. A131799; union of A000290 and A000578.",
				"First differences in A075052. [From _Zak Seidov_, May 10 2010]",
				"Cf. A002117, A013661, A013664."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_.",
			"references": 12,
			"revision": 45,
			"time": "2020-12-19T03:40:09-05:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}