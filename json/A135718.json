{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A135718",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 135718,
			"data": "4,9,8,25,4,49,16,27,4,121,8,169,4,9,32,289,4,361,8,9,4,529,9,125,4,81,8,841,4,961,64,9,4,25,8,1369,4,9,16,1681,4,1849,8,25,4,2209,9,343,4,9,8,2809,4,25,16,9,4,3481,8,3721,4,27,128,25,4,4489,8,9,4,5041",
			"name": "a(n) = smallest divisor of n^2 that is not a divisor of n.",
			"comment": [
				"All terms are in A025475. - _David A. Corneth_, Jun 24 2017"
			],
			"link": [
				"Vincenzo Librandi and David A. Corneth, \u003ca href=\"/A135718/b135718.txt\"\u003eTable of n, a(n) for n = 2..10001\u003c/a\u003e (first 1999 terms from Vincenzo Librandi)"
			],
			"formula": [
				"If n = product{p=primes, p|n} p^b(n,p), where each b(n,p) is a positive integer, then a(n) = the minimum value of a p^(b(n,p)+1) where p is a prime that divides n. Example: 24 has the prime factorization of 2^3 *3^1. So a(24) = the minimum of 2^(3+1) and 3^(1+1) = the minimum of 16 and 9, which is 9.",
				"a(p) = p^2 for p prime. - _Michel Marcus_, Jun 13 2017"
			],
			"example": [
				"The divisors of 12 are 1,2,3,4,6,12. The divisors of 12^2 = 144 are 1,2,3,4,6,8,9,12,16,18,24,36,48,72,144. So the smallest divisor of 144 that is not a divisor of 12 is 8."
			],
			"maple": [
				"with(numtheory): a:=proc(n) options operator, arrow: op(1, `minus`(divisors(n^2), divisors(n))) end proc: seq(a(n),n=2..60); # _Emeric Deutsch_, May 18 2008"
			],
			"mathematica": [
				"a135718[n_] := Map[First[Complement[Divisors[#^2], Divisors[#]]]\u0026, Range[2, n]]",
				"a135718[60] (* data *) (* _Hartmut F. W. Hoft_, Jun 13 2017 *)",
				"Table[Min@ Map[Apply[Power, # + {0, 1}] \u0026, FactorInteger@ n], {n, 2, 60}] (* _Michael De Vlieger_, Jun 23 2017 *)"
			],
			"program": [
				"(PARI) a(n) = fordiv(n^2, x, if (n % x, return (x))); \\\\ _Michel Marcus_, Jun 13 2017",
				"(PARI) a(n) = my(f=factor(n)); vecmin(vector(#f~, i, f[i,1]^(f[i,2]+1))) \\\\ _David A. Corneth_, Jun 28 2017",
				"(PARI) first(n) = {n++; my(v = vector(n-1), l = List()); forprime(p = 2, n, v[p-1] = p^2); forprime(p = 2, sqrtint(n), pp = p; j = 1; while(pp\u003cn, pp*=p; j++; listput(l, [pp, p, j])); listsort(l); for(i=1,#l, for(c = 0, n\\l[i][1], for(d = 1, l[i][2]-1, t = c*l[i][1] + d*(l[i][1]\\l[i][2]) - 1; if(t\u003cn \u0026\u0026 v[t]==0, v[t]=l[i][1]))));v} \\\\ this prog uses A025475. _David A. Corneth_, Jun 30 2017"
			],
			"xref": [
				"Cf. A001597, A025475."
			],
			"keyword": "nonn,easy",
			"offset": "2,1",
			"author": "_Leroy Quet_, May 10 2008",
			"ext": [
				"More terms from _Emeric Deutsch_, May 18 2008"
			],
			"references": 2,
			"revision": 39,
			"time": "2019-09-05T12:05:09-04:00",
			"created": "2007-12-09T03:00:00-05:00"
		}
	]
}