{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A116395",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 116395,
			"data": "1,2,1,6,5,1,20,22,8,1,70,93,47,11,1,252,386,244,81,14,1,924,1586,1186,500,124,17,1,3432,6476,5536,2794,888,176,20,1,12870,26333,25147,14649,5615,1435,237,23,1,48620,106762,112028,73489,32714,10135,2168,307,26,1",
			"name": "Riordan array (1/sqrt(1-4*x), (1/sqrt(1-4*x)-1)/2).",
			"comment": [
				"Row sums are A007854. Diagonal sums are A116396.",
				"Triangle T(n,k), 0 \u003c= k \u003c= n, read by rows given by [2,1,1,1,1,1,1,...] DELTA [1,0,0,0,0,0,0,...] where DELTA is the operator defined in A084938. - _Philippe Deléham_, Jun 05 2007",
				"Inverse of Riordan array (1/(1+2*x), x*(1+x)/(1+2*x)^2) (see A123876). - _Philippe Deléham_, Oct 25 2007"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A116395/b116395.txt\"\u003eRows n = 0..100 of triangle, flattened\u003c/a\u003e",
				"Joseph Pappe, Digjoy Paul and Anne Schilling, \u003ca href=\"https://arxiv.org/abs/2109.06300\"\u003eAn area-depth symmetric q,t-Catalan polynomial\u003c/a\u003e, arXiv:2109.06300 [math.CO], 2021. See Remark 2.4 p. 4."
			],
			"formula": [
				"Number triangle T(n,k) = (4^n/2^k)*Sum_{j=0..k} C(k,j)*C(n+(j-1)/2,n)*(-1)^(k-j).",
				"Sum_{k=0..n} (-1)^k*T(n,k) = A000108(n), Catalan numbers. - _Philippe Deléham_, Nov 07 2006",
				"T(n,k) = Sum_{j\u003e=0} A039599(n,j)*binomial(j,k). - _Philippe Deléham_, Mar 30 2007",
				"Sum_{k=0..n} T(n,k)*x^k = A127053(n), A126985(n), A127016(n), A127017(n), A126987(n), A126986(n), A126982(n), A126984(n), A126983(n), A000007(n), A000108(n), A000984(n), A007854(n), A076035(n), A076036(n), A127628(n), A126694(n), A115970(n) for x = -11, -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6 respectively. - _Philippe Deléham_, Oct 25 2007"
			],
			"example": [
				"Triangle begins:",
				"    1;",
				"    2,   1;",
				"    6,   5,   1;",
				"   20,  22,   8,  1;",
				"   70,  93,  47, 11,  1;",
				"  252, 386, 244, 81, 14, 1;"
			],
			"mathematica": [
				"T[n_, k_]:= (4^n/2^k)*Sum[(-1)^(k-j)*Binomial[k, j]*Binomial[n+(j-1)/2, n], {j, 0, k}]; Table[T[n, k], {n, 0, 12}, {k, 0, n}]//Flatten (* _G. C. Greubel_, May 28 2019 *)"
			],
			"program": [
				"(PARI) {T(n,k) = (4^n/2^k)*sum(j=0, k, (-1)^(k-j)*binomial(k, j)* binomial(n+(j-1)/2, n))}; \\\\ _G. C. Greubel_, May 28 2019",
				"(MAGMA) [[ Round((4^n/2^k)*(\u0026+[ (-1)^(k-j)*Binomial(k, j)*Gamma(n+(j+1)/2)/(Factorial(n)*Gamma((j+1)/2)) : j in [0..k]])) : k in [0..n]]: n in [0..12]]; // _G. C. Greubel_, May 28 2019",
				"(Sage) [[(4^n/2^k)*sum( (-1)^(k-j)*binomial(k, j)* binomial(n+(j-1)/2, n) for j in (0..k)) for k in (0..n)] for n in (0..12)] # _G. C. Greubel_, May 28 2019"
			],
			"keyword": "easy,nonn,tabl",
			"offset": "0,2",
			"author": "_Paul Barry_, Feb 12 2006",
			"references": 8,
			"revision": 18,
			"time": "2021-09-15T03:29:06-04:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}