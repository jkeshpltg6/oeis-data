{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A090995",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 90995,
			"data": "10,18,32,58,104,188,338,610,1098,1980,3566,6428,11580,20870,37602,67762,122096,220018,396448,714388,1287266,2319594,4179738,7531660,13571542,24455124,44066548,79405254,143083226,257827186,464588384",
			"name": "Number of meaningful differential operations of the n-th order on the space R^10.",
			"comment": [
				"Also (starting 6,10,...) the number of zig-zag paths from top to bottom of a rectangle of width 6. - _Joseph Myers_, Dec 23 2008",
				"Number of walks of length n on the path graph P_6. - _Andrew Howroyd_, Apr 17 2017"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A090995/b090995.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"B. Malesevic, \u003ca href=\"https://www.jstor.org/stable/43666958\"\u003eSome combinatorial aspects of differential operation composition on the space R^n\u003c/a\u003e, Univ. Beograd, Publ. Elektrotehn. Fak., Ser. Mat. 9 (1998), 29-33.",
				"Branko Malesevic, \u003ca href=\"http://arxiv.org/abs/0704.0750\"\u003eSome combinatorial aspects of differential operation compositions on space R^n\u003c/a\u003e, arXiv:0704.0750 [math.DG], 2007.",
				"Joseph Myers, \u003ca href=\"http://www.polyomino.org.uk/publications/2008/bmo1-2009-q1.pdf\"\u003eBMO 2008--2009 Round 1 Problem 1---Generalisation\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,2,-1)."
			],
			"formula": [
				"Equals 2 * A090990.",
				"a(k+6) = 5*a(k+4) - 6*a(k+2) + a(k).",
				"From _Colin Barker_, May 03 2012: (Start)",
				"a(n) = a(n-1) + 2*a(n-2) - a(n-3).",
				"G.f.: 2*x*(5+4*x-3*x^2)/(1-x-2*x^2+x^3). (End)"
			],
			"maple": [
				"NUM := proc(k :: integer) local i,j,n,Fun,Identity,v,A; n := 10; # \u003c- DIMENSION Fun := (i,j)-\u003epiecewise(((j=i+1) or (i+j=n+1)),1,0); Identity := (i,j)-\u003epiecewise(i=j,1,0); v := matrix(1,n,1); A := piecewise(k\u003e1,(matrix(n,n,Fun))^(k-1),k=1,matrix(n,n,Identity)); return(evalm(v\u0026*A\u0026*transpose(v))[1,1]); end:"
			],
			"mathematica": [
				"a[n_ /; n \u003c= 6] := {10, 18, 32, 58, 104, 188}[[n]]; a[n_] := a[n] = 5*a[n-2] - 6*a[n-4] + a[n-6]; Array[a, 31] (* _Jean-François Alcover_, Oct 07 2017 *)",
				"2*LinearRecurrence[{1,2,-1}, {5,9,16}, 40] (* _G. C. Greubel_, Feb 02 2019 *)"
			],
			"program": [
				"(PARI) my(x='x+O('x^40)); Vec(2*x*(5+4*x-3*x^2)/(1-x-2*x^2+x^3)) \\\\ _G. C. Greubel_, Feb 02 2019",
				"(MAGMA) m:=40; R\u003cx\u003e:=PowerSeriesRing(Integers(), m); Coefficients(R!(  2*x*(5+4*x-3*x^2)/(1-x-2*x^2+x^3) )); // _G. C. Greubel_, Feb 02 2019",
				"(Sage) a=(2*x*(5+4*x-3*x^2)/(1-x-2*x^2+x^3)).series(x, 40).coefficients(x, sparse=False); a[1:] # _G. C. Greubel_, Feb 02 2019",
				"(GAP) a:=[10,18,32];; for n in [4..30] do a[n]:=a[n-1]+2*a[n-2]-a[n-3]; od; a; # _G. C. Greubel_, Feb 02 2019"
			],
			"xref": [
				"Cf. A090989-A090994.",
				"Column 6 of A220062."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Branko Malesevic_, Feb 29 2004",
			"ext": [
				"More terms from _Joseph Myers_, Dec 23 2008"
			],
			"references": 13,
			"revision": 28,
			"time": "2019-03-21T11:49:03-04:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}