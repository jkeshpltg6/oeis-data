{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A270920",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 270920,
			"data": "1,2,2,3,3,3,4,2,2,5,5,3,2,3,4,4,3,4,6,3,2,4,3,3,5,5,3,3,4,5,6,7,2,2,4,6,9,9,7,6,3,5,4,4,7,8,6,3,5,7,8,7,7,6,6,5,4,5,7,7,5,5,6,9,5,3,5,4,9,11,10,6,2,6,4,3,6,7,5,5",
			"name": "Number of ordered ways to write n as the sum of a positive triangular number, a positive square, and a fifth power whose absolute value does not exceed n.",
			"comment": [
				"Conjecture: （i) a(n) \u003e 0 for all n \u003e 0, and a(n) = 1 only for n = 1, 112, 770, 801, 1593, 1826, 2320, 2334, 2849, 7561.",
				"(ii) Let T(x) = x*(x+1)/2 and pen(x) = x*(3x+1)/2. Any natural number n can be written as P(x,y) + z^5, where x, y and z are integers with |z^5| \u003c= n, and the polynomial P(x,y) is either of the following ones: T(x)+2*T(y), T(x)+2*pen(y), x^2+pen(y), x^2+y(5y+1)/2, 2*T(x)+pen(y), pen(x)+pen(y), pen(x)+y(3y+j) (j = 1,2), pen(x)+6*T(y), pen(x)+y(7y+j)/2 (j = 1,3,5), pen(x)+y(4y+j) (j = 1,3), pen(x)+y(5y+j) (j = 1,2,3,4), pen(x)+y(13y+7)/2, x(5x+i)/2+y(3y+j) (i = 1,3; j = 1,2), x(5x+j)/2+y(7y+5)/2 (j = 1,3)."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A270920/b270920.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Z.-W. Sun, \u003ca href=\"http://dx.doi.org/10.4064/aa127-2-1\"\u003eMixed sums of squares and triangular numbers\u003c/a\u003e, Acta Arith. 127(2007), 103-113.",
				"Z.-W. Sun, \u003ca href=\"http://math.scichina.com:8081/sciAe/EN/abstract/abstract517007.shtml\"\u003eOn universal sums of polygonal numbers\u003c/a\u003e, Sci. China Math. 58(2015), no. 7, 1367-1396.",
				"Z.-W. Sun, \u003ca href=\"http://arxiv.org/abs/1502.03056\"\u003eOn universal sums ax^2+by^2+f(z), aT_x+bT_y+f(z) and zT_x+by^2+f(z)\u003c/a\u003e, preprint, arXiv:1502.03056 [math.NT], 2015."
			],
			"example": [
				"a(1) = 1 since 1 = 1*2/2 + 1^2 + (-1)^5 with |(-1)^5| \u003c= 1.",
				"a(112) = 1 since 112 = 10*11/2 + 5^2 + 2^5.",
				"a(770) = 1 since 770 = 28*29/2 + 11^2 + 3^5.",
				"a(801) = 1 since 801 = 45*46/2 + 3^2 + (-3)^5 with |(-3)^5| \u003c 801.",
				"a(1593) = 1 since 1593 = 49*50/2 + 20^2 + (-2)^5 with |(-2)^5| \u003c 1593.",
				"a(1826) = 1 since 1826 = 55*56/2 + 23^2 + (-3)^5 with |(-3)^5| \u003c 1826.",
				"a(2320) = 1 since 2320 = 5*6/2 + 48^2 + 1^5.",
				"a(2334) = 1 since 2334 = 11*12/2 + 45^2 + 3^5.",
				"a(2849) = 1 since 2849 = 70*71/2 + 11^2 + 3^5.",
				"a(7561) = 1 since 7561 = 97*98/2 + 53^2 + (-1)^5 with |(-1)^5| \u003c 7561."
			],
			"mathematica": [
				"TQ[n_]:=TQ[n]=n\u003e0\u0026\u0026IntegerQ[Sqrt[8n+1]]",
				"Do[r=0;Do[If[TQ[n-(-1)^k*x^5-y^2],r=r+1],{k,0,1},{x,0,n^(1/5)},{y,1,Sqrt[n-(-1)^k*x^5]}];Print[n,\" \",r];Continue,{n,1,80}]"
			],
			"xref": [
				"Cf. A000217, A000290, A000584, A001318, A262813, A262815, A262816, A262827, A270469, A270488, A270516, A270533, A270559, A270566, A270921."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Zhi-Wei Sun_, Mar 25 2016",
			"references": 8,
			"revision": 11,
			"time": "2016-03-25T22:59:07-04:00",
			"created": "2016-03-25T22:59:07-04:00"
		}
	]
}