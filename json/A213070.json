{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A213070",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 213070,
			"data": "31,0,0,165,27,32,8,0,0,720,187,236,104,30,108,3431,992,1179,746,251,580,920,352,1210,16608,4361,5027,4361,1094,2043,5027,2043,6268,76933,17601,20009,21068,3675,7213,26181,9258,26414,25090,10048,32132",
			"name": "Irregular array T(n,k) of the numbers of non-extendable (complete) non-self-adjacent simple paths ending at each of a minimal subset of nodes within a square lattice bounded by rectangles with nodal dimensions n and 6, n \u003e= 2.",
			"comment": [
				"The subset of nodes is contained in the top left-hand quarter of the rectangle and has nodal dimensions floor((n+1)/2) and 3 to capture all geometrically distinct counts.",
				"The quarter-rectangle is read by rows.",
				"The irregular array of numbers is:",
				"...k.....1.....2.....3.....4.....5.....6.....7.....8.....9....10....11....12",
				".n",
				".2......31.....0.....0",
				".3.....165....27....32.....8.....0.....0",
				".4.....720...187...236...104....30...108",
				".5....3431...992..1179...746...251...580...920...352..1210",
				".6...16608..4361..5027..4361..1094..2043..5027..2043..6268",
				".7...76933.17601.20009.21068..3675..7213.26181..9258.26414.25090.10048.32132",
				"where k indicates the position of the end node in the quarter-rectangle.",
				"For each n, the maximum value of k is 3*floor((n+1)/2).",
				"Reading this array by rows gives the sequence."
			],
			"link": [
				"C. H. Gribble, \u003ca href=\"https://oeis.org/wiki/Complete_non-self-adjacent_paths:Results_for_Square_Lattice\"\u003eComputed characteristics of complete non-self-adjacent paths in a square lattice bounded by various sizes of rectangle.\u003c/a\u003e",
				"C. H. Gribble, \u003ca href=\"https://oeis.org/wiki/Complete non-self-adjacent paths:Program\"\u003eComputes characteristics of complete non-self-adjacent paths in square and cubic lattices bounded by various sizes of rectangle and rectangular cuboid respectively.\u003c/a\u003e"
			],
			"example": [
				"When n = 2, the number of times (NT) each node in the rectangle is the end node (EN) of a complete non-self-adjacent simple path is",
				"EN  0  1  2  3  4  5",
				"    6  7  8  9 10 11",
				"NT 31  0  0  0  0 31",
				"   31  0  0  0  0 31",
				"To limit duplication, only the top left-hand corner 31 and the two zeros to its right are stored in the sequence, i.e. T(2,1) = 31, T(2,2) = 0 and T(2,3) = 0."
			],
			"xref": [
				"Cf. A213106, A213249, A213379, A214025, A214119, A214121, A214122, A214359."
			],
			"keyword": "nonn,tabf",
			"offset": "2,1",
			"author": "_Christopher Hunt Gribble_, Jul 13 2012",
			"ext": [
				"Comment corrected by _Christopher Hunt Gribble_, Jul 22 2012"
			],
			"references": 4,
			"revision": 25,
			"time": "2012-07-23T12:46:36-04:00",
			"created": "2012-07-18T21:04:54-04:00"
		}
	]
}