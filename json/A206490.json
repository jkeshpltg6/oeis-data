{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A206490",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 206490,
			"data": "0,2,6,6,14,14,9,9,24,24,24,19,19,19,38,12,19,29,12,31,31,38,29,24,54,29,36,24,31,45,38,15,54,31,47,34,24,24,45,38,29,36,24,47,54,36,45,29,38,61,47,36,15,41,74,29,38,45,31,52,34,54,43,18,63,63,24,38,54,54,38,39,36,34,70,29,65,52",
			"name": "The eccentric connectivity index of the rooted tree with Matula-Goebel number n.",
			"comment": [
				"The Matula-Goebel number of a rooted tree can be defined in the following recursive manner: to the one-vertex tree there corresponds the number 1; to a tree T with root degree 1 there corresponds the t-th prime number, where t is the Matula-Goebel number of the tree obtained from T by deleting the edge emanating from the root; to a tree T with root degree m\u003e=2 there corresponds the product of the Matula-Goebel numbers of the m branches of T.",
				"The eccentric connectivity index of a simple connected graph G is defined as the sum over all vertices i of G of the product E(i) D(i), where E(i) is the eccentricity and D(i) is the degree of vertex i."
			],
			"reference": [
				"F. Goebel, On a 1-1 correspondence between rooted trees and natural numbers, J. Combin. Theory, B 29 (1980), 141-143.",
				"I. Gutman and A. Ivic, On Matula numbers, Discrete Math., 150, 1996, 131-142.",
				"I. Gutman and Y-N. Yeh, Deducing properties of trees from their Matula numbers, Publ. Inst. Math., 53 (67), 1993, 17-22.",
				"D. W. Matula, A natural rooted tree enumeration by prime factorization, SIAM Review, 10, 1968, 273.",
				"V. Sharma, R. Goswami, and A. K. Madan, Eccentric Connectivity index: a novel highly discriminating topological descriptor for structure-property and structure-activity studies, J. Chem. Inf. Comput. Sci., 37, 1997, 273-282."
			],
			"link": [
				"E. Deutsch, \u003ca href=\"http://arxiv.org/abs/1111.4288\"\u003e Rooted tree statistics from Matula numbers\u003c/a\u003e, arXiv:1111.4288.",
				"\u003ca href=\"/index/Mat#matula\"\u003eIndex entries for sequences related to Matula-Goebel numbers\u003c/a\u003e"
			],
			"formula": [
				"Explanation of the Maple program: \"V\" finds recursively the number of vertices (needed later); \"d\" finds recursively the distance matrix; \"a\" finds the adjacency matrix from the distance matrix; \"RS\" finds the vector of the row sums of any matrix (will be applied to the adjacency matrix to yield the vertex degrees); \"MX\" finds the vector of the largest row entries of any matrix (will be applied to the distance matrix to yield the vertex eccentricities); \"ECI\" finds the eccentric connectivity index by taking the dot product of the two vectors just mentioned."
			],
			"example": [
				"a(7)=9 because the rooted tree with Matula-Goebel number 7 is Y; the 3 pendant vertices have degree 1 and eccentricity 2 and the 4th vertex has degree 3 and eccentricity 1; 1*2 + 1*2 + 1*2 + 3*1 = 9."
			],
			"maple": [
				"with(numtheory): with(LinearAlgebra): V := proc (n) local r, s: r := proc (n) options operator, arrow: op(1, factorset(n)) end proc: s := proc (n) options operator, arrow: n/r(n) end proc: if n = 1 then 1 elif bigomega(n) = 1 then 1+V(pi(n)) else V(r(n))+V(s(n))-1 end if end proc: d := proc (n) local r, s, dt, drs: r := proc (n) options operator, arrow: op(1, factorset(n)) end proc: s := proc (n) options operator, arrow: n/r(n) end proc: dt := proc (i, j) if i = 1 and j = 1 then 0 elif i = 1 and 1 \u003c j then 1+dd[pi(n)][1, j-1] elif 1 \u003c i and j = 1 then 1+dd[pi(n)][i-1, 1] elif 1 \u003c i and 1 \u003c j then dd[pi(n)][i-1, j-1] else  end if end proc: drs := proc (i, j) if 1 \u003c= i and 1 \u003c= j and i \u003c= V(r(n)) and j \u003c= V(r(n)) then dd[r(n)][i, j] elif 1+V(r(n)) \u003c= i and 1+V(r(n)) \u003c= j and i \u003c= V(n) and j \u003c= V(n) then dd[s(n)][i-V(r(n))+1, j-V(r(n))+1] elif 1 \u003c= i and i \u003c= V(r(n)) and 1+V(r(n)) \u003c= j and j \u003c= n then dd[r(n)][i, 1]+dd[s(n)][1, j-V(r(n))+1] else dd[r(n)][1, j]+dd[s(n)][i-V(r(n))+1, 1] end if end proc: if n = 1 then Matrix(1, 1, [0]) elif bigomega(n) = 1 then Matrix(V(n), V(n), dt) else Matrix(V(n), V(n), drs) end if end proc: for n to 1000 do dd[n] := d(n) end do: \u003e a := proc (n) local ddd, aa: ddd := proc (n) options operator, arrow: d(n) end proc: aa := proc (i, j) if ddd(n)[i, j] = 1 then 1 else 0 end if end proc: Matrix(RowDimension(ddd(n)), RowDimension(ddd(n)), aa) end proc: \u003e RS := proc (m) local dim: dim := RowDimension(m): Matrix(1, dim, [seq(add(m[i, j], j = 1 .. dim), i = 1 .. dim)]) end proc: \u003e MX := proc (m) local dim: dim := RowDimension(m): Matrix(1, dim, [seq(max(seq(m[i, j], j = 1 .. dim)), i = 1 .. dim)]) end proc: \u003e ECI := proc (n) options operator, arrow: MatrixMatrixMultiply(RS(a(n)), Transpose(MX(d(n))))[1, 1] end proc: seq(ECI(n), n = 1 .. 77);"
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Emeric Deutsch_, May 08 2012",
			"references": 1,
			"revision": 11,
			"time": "2017-03-07T11:26:08-05:00",
			"created": "2012-05-09T12:10:45-04:00"
		}
	]
}