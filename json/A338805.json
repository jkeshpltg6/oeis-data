{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A338805",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 338805,
			"data": "1,2,1,4,6,1,18,28,12,1,48,170,100,20,1,480,988,870,260,30,1,1440,7896,7588,3150,560,42,1,20160,60492,73808,37408,9100,1064,56,1,120960,555264,764524,460656,140448,22428,1848,72,1,1451520,5819904,8448120,5952700,2162160,436296,49140,3000,90,1",
			"name": "Triangle T(n,k) defined by Sum_{k=1..n} T(n,k)*u^k*x^n/n! = Product_{j\u003e0} (1-x^j)^(-u/j).",
			"comment": [
				"Also the Bell transform of A318249."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A338805/b338805.txt\"\u003eRows n = 1..100, flattened\u003c/a\u003e",
				"Peter Luschny, \u003ca href=\"https://oeis.org/wiki/User:Peter_Luschny/BellTransform\"\u003eThe Bell transform\u003c/a\u003e."
			],
			"formula": [
				"E.g.f.: exp(Sum_{n\u003e0} u*d(n)*x^n/n), where d(n) is the number of divisors of n.",
				"T(n; u) = Sum_{k=1..n} T(n, k)*u^k is given by T(n; u) = u * (n-1)! * Sum_{k=1..n} d(k)*T(n-k; u)/(n-k)!, T(0; u) = 1.",
				"T(n, k) = (n!/k!) * Sum_{i_1,i_2,...,i_k \u003e 0 and i_1+i_2+...+i_k=n} Product_{j=1..k} d(i_j)/i_j."
			],
			"example": [
				"exp(Sum_{n\u003e0} u*d(n)*x^n/n) = 1 + u*x + (2*u+u^2)*x^2/2! + (4*u+6*u^2+u^3)*x^3/3! + ... .",
				"Triangle begins:",
				"      1;",
				"      2,     1;",
				"      4,     6,     1;",
				"     18,    28,    12,     1;",
				"     48,   170,   100,    20,    1;",
				"    480,   988,   870,   260,   30,    1;",
				"   1440,  7896,  7588,  3150,  560,   42,  1;",
				"  20160, 60492, 73808, 37408, 9100, 1064, 56, 1;"
			],
			"mathematica": [
				"a[n_] := a[n] = If[n == 0, 0, (n - 1)! * DivisorSigma[0, n]]; T[n_, k_] := T[n, k] = If[k == 0, Boole[n == 0], Sum[a[j] * Binomial[n - 1, j - 1] * T[n - j, k - 1], {j, 0, n - k + 1}]]; Table[T[n, k], {n, 1, 10}, {k, 1, n}] // Flatten (* _Amiram Eldar_, Apr 28 2021 *)"
			],
			"program": [
				"(PARI) {T(n, k) = my(u='u); n!*polcoef(polcoef(prod(j=1, n, (1-x^j+x*O(x^n))^(-u/j)), n), k)}",
				"(PARI) a(n) = if(n\u003c1, 0, (n-1)!*numdiv(n));",
				"T(n, k) = if(k==0, 0^n, sum(j=0, n-k+1, binomial(n-1, j-1)*a(j)*T(n-j, k-1)))"
			],
			"xref": [
				"Column k=1..3 give A318249, A338810, A338811.",
				"Row sums give A028342.",
				"Cf. A000005 (d(n)), A008298."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Seiichi Manyama_, Nov 10 2020",
			"references": 7,
			"revision": 33,
			"time": "2021-04-28T02:03:29-04:00",
			"created": "2020-11-10T09:33:48-05:00"
		}
	]
}