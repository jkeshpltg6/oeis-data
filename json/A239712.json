{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A239712",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 239712,
			"data": "2,5,11,17,19,23,47,67,71,79,131,191,257,263,271,383,1031,1039,1087,1151,1279,2063,2111,4099,4111,4127,4159,5119,6143,8447,16447,20479,32771,32783,32831,33023,33791,65537,65539,65543,65551,65599,66047,73727,81919,262147,262151,262271,262399,263167",
			"name": "Primes of the form m = 2^i + 2^j - 1, where i \u003e j \u003e= 0.",
			"comment": [
				"Numbers m such that b = 2 is the only base such that the base-b digital sum of m + 1 is equal to b.",
				"Example: 5 + 1 = 110_2 which implies ds_2(5 + 1) = 2 = b, where ds_b = digital sum in base-b. However, ds_3(6) = 2 \u003c\u003e 3, ds_4(6) = 3 \u003c\u003e 4, ds_5(6) = 2 \u003c\u003e 5, ds_6(6) = 1 \u003c\u003e 6. For all other bases \u003e 6 we have ds_b(6) = 6 \u003c\u003e b. It follows that b = 2 is the only such base.",
				"The base-2 representation of a term 2^i + 2^j - 1 has a base-2 digital sum of 1 + j.",
				"In base-2 representation the first terms are 10, 101, 1011, 10001, 10011, 10111, 101111, 1000011, 1000111, 1001111, 10000011, 10111111, 100000001, 100000111, 100001111, 101111111, 10000000111, 10000001111, 10000111111, 10001111111, ...",
				"Numbers m = 2^i + 2^j - 1 with odd i and j are not terms. Example: 10239 = 2^13 + 2^11 - 1 is not a prime."
			],
			"link": [
				"Hieronymus Fischer, \u003ca href=\"/A239712/b239712.txt\"\u003eTable of n, a(n) for n = 1..250\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A239708(n) - 1.",
				"a(n+1) = min(A018900(k) \u003e a(n)| A018900(k) - 1 is prime,  k \u003e= 1) - 1."
			],
			"example": [
				"a(1) = 2, since 2 = 2^1 + 2^0 - 1 is prime.",
				"a(5) = 19, since 19 = 2^4 + 2^2 - 1 is prime."
			],
			"mathematica": [
				"Select[Union[Total/@(2^#\u0026/@Subsets[Range[0,20],{2}])-1],PrimeQ] (* _Harvey P. Dale_, Aug 08 2014 *)"
			],
			"program": [
				"(Smalltalk)",
				"A239712",
				"\"Answers the n-th term of A239712.",
				"  Usage: n A239712",
				"  Answer: a(n)\"",
				"  | a b i k m p q terms |",
				"  terms := OrderedCollection new.",
				"  b := 2.",
				"  p := 1.",
				"  k := 0.",
				"  m := 0.",
				"  [k \u003c self] whileTrue:",
				"         [m := m + 1.",
				"         p := b * p.",
				"         q := 1.",
				"         i := 0.",
				"         [i \u003c m and: [k \u003c self]] whileTrue:",
				"                   [i := i + 1.",
				"                   a := p + q - 1.",
				"                   a isPrime",
				"                        ifTrue:",
				"                            [k := k + 1.",
				"                            terms add: a].",
				"                   q := b * q]].",
				"  ^terms at: self",
				"[by _Hieronymus Fischer_, Apr 22 2014]",
				"-----------",
				"(Smalltalk)",
				"floorPrimesWhichAreDistinctPowersOf: b withOffset: d",
				"  \"Answers an array which holds the primes \u003c n that obey b^i + b^j + d, i\u003ej\u003e=0,",
				"  where n is the receiver. b \u003e 1 (here: b = 2, d = -1).",
				"  Uses floorDistinctPowersOf: from A018900",
				"  Usage:",
				"  n floorPrimesWhichAreDistinctPowersOf: b withOffset: d",
				"  Answer: #(2 5 11 17 19 23 ...) [terms \u003c n]\"",
				"  ^((self - d floorDistinctPowersOf: b)",
				"  collect: [:i | i + d]) select: [:i | i isPrime]",
				"[by _Hieronymus Fischer_, Apr 22 2014]",
				"------------",
				"(Smalltalk)",
				"primesWhichAreDistinctPowersOf: b withOffset: d",
				"  \"Answers an array which holds the n primes of the form b^i + b^j + d, i\u003ej\u003e=0, where n is the receiver.",
				"  Direct calculation by scanning b^i + b^j + d in increasing order and selecting terms which are prime.",
				"  b \u003e 1; this sequence: b = 2, d = 1.",
				"  Usage:",
				"  n primesWhichAreDistinctPowersOf: b withOffset: d",
				"  Answer: #(2 5 11 17 19 23 ...) [a(1) ... a(n)]\"",
				"  | a k p q terms n |",
				"  terms := OrderedCollection new.",
				"  n := self.",
				"  k := 0.",
				"  p := b.",
				"  [k \u003c n] whileTrue:",
				"         [q := 1.",
				"         [q \u003c p and: [k \u003c n]] whileTrue:",
				"                   [a := p + q + d.",
				"                   a isPrime",
				"                        ifTrue:",
				"                            [k := k + 1.",
				"                            terms add: a].",
				"                   q := b * q].",
				"         p := b * p].",
				"  ^terms asArray",
				"[by _Hieronymus Fischer_, Apr 22 2014]"
			],
			"xref": [
				"Cf. A007953, A018900, A081091, A008864, A187813.",
				"Cf. A239703, A239708, A239709, A239713 - A239720."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Hieronymus Fischer_, Mar 28 2014 and Apr 22 2014",
			"ext": [
				"Examples moved from Maple field to Examples field by _Harvey P. Dale_, Aug 08 2014"
			],
			"references": 14,
			"revision": 28,
			"time": "2017-05-14T12:02:07-04:00",
			"created": "2014-04-15T02:51:18-04:00"
		}
	]
}