{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A084101",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 84101,
			"data": "1,3,3,1,1,3,3,1,1,3,3,1,1,3,3,1,1,3,3,1,1,3,3,1,1,3,3,1,1,3,3,1,1,3,3,1,1,3,3,1,1,3,3,1,1,3,3,1,1,3,3,1,1,3,3,1,1,3,3,1,1,3,3,1,1,3,3,1,1,3,3,1,1,3,3,1,1,3,3,1,1,3,3,1,1,3,3,1,1,3,3,1,1,3,3,1,1,3,3,1,1,3,3,1,1",
			"name": "Expansion of (1+x)^2/((1-x)*(1+x^2)).",
			"comment": [
				"Partial sums of A084099. Inverse binomial transform of A000749 (without leading zeros).",
				"From _Klaus Brockhaus_, May 31 2010: (Start)",
				"Periodic sequence: Repeat 1, 3, 3, 1.",
				"Interleaving of A010684 and A176040.",
				"Continued fraction expansion of (7 + 5*sqrt(29))/26.",
				"Decimal expansion of 121/909.",
				"a(n) = A143432(n+3) + 1 = 2*A021913(n+1) + 1 = 2*A133872(n+3) + 1.",
				"a(n) = A165207(n+1) - 1.",
				"First differences of A047538.",
				"Binomial transform of A084102. (End)",
				"From _Wolfdieter Lang_, Feb 09 2012: (Start)",
				"a(n) = A045572(n+1) (Modd 5) := A203571(A045572(n+1)), n \u003e= 0.",
				"For general Modd n (not to be confused with mod n) see a comment on A203571. The nonnegative members of the five residue classes Modd 5, called [m] for m=0,1,...,4, are shown in the array A090298 if there the last row is taken as class [0] after inclusion of 0.",
				"(End)"
			],
			"link": [
				"Guenther Schrack, \u003ca href=\"/A084101/b084101.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1, -1, 1)."
			],
			"formula": [
				"a(n) = binomial(3, n mod 4). - _Paul Barry_, May 25 2003",
				"From _Klaus Brockhaus_, May 31 2010: (Start)",
				"a(n) = a(n-4) for n \u003e 3; a(0) = a(3) = 1, a(1) = a(2) = 3.",
				"a(n) = (4 - (1+i)*i^n - (1-i)*(-i)^n)/2 where i = sqrt(-1). (End)",
				"E.g.f.: 2*exp(x) + sin(x) - cos(x). - _Arkadiusz Wesolowski_, Nov 04 2017",
				"a(n) = 2 - (-1)^(n*(n+1)/2). - _Guenther Schrack_, Feb 26 2019"
			],
			"example": [
				"From _Wolfdieter Lang_, Feb 09 2012: (Start)",
				"Modd 5 of nonnegative odd numbers restricted mod 5:",
				"A045572: 1, 3, 7, 9, 11, 13, 17, 19, 21, 23, ...",
				"Modd 5:  1, 3, 3, 1,  1,  3,  3,  1,  1,  3, ...",
				"(End)"
			],
			"mathematica": [
				"CoefficientList[Series[(1+x)^2/((1-x)(1+x^2)),{x,0,110}],x] (* or *) PadRight[{},110,{1,3,3,1}] (* _Harvey P. Dale_, Nov 21 2012 *)"
			],
			"program": [
				"(PARI) x='x+O('x^100); Vec((1+x)^2/((1-x)*(1+x^2))) \\\\ _Altug Alkan_, Dec 24 2015",
				"(MAGMA) R\u003cx\u003e:=PowerSeriesRing(Integers(), 100); Coefficients(R!( (1+x)^2/((1-x)*(1+x^2)) )); // _G. C. Greubel_, Feb 28 2019",
				"(Sage) ((1+x)^2/((1-x)*(1+x^2))).series(x, 100).coefficients(x, sparse=False) # _G. C. Greubel_, Feb 28 2019"
			],
			"xref": [
				"Cf. A084102.",
				"Cf. A010684 (repeat 1, 3), A176040 (repeat 3, 1), A178593 (decimal expansion of (7+5*sqrt(29))/26), A143432 (expansion of (1+x^4)/((1-x)*(1+x^2))), A021913 (repeat 0, 0, 1, 1), A133872 (repeat 1, 1, 0, 0), A165207 (repeat 2, 2, 4, 4), A047538 (congruent to 0, 1, 4 or 7 mod 8), A084099 (expansion of (1+x)^2/(1+x^2)), A000749 (expansion of x^3/((1-x)^4-x^4)). - _Klaus Brockhaus_, May 31 2010"
			],
			"keyword": "easy,nonn",
			"offset": "0,2",
			"author": "_Paul Barry_, May 15 2003",
			"references": 10,
			"revision": 50,
			"time": "2019-03-25T04:29:42-04:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}