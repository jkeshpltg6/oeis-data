{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A257874",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 257874,
			"data": "3,6,8,10,12,15,17,21,24,27,31,33,38,41,43,48,50,52,54,57,59,62,66,69,71,73,75,78,80,82,85,90,93,95,97,99,101,103,105,111,115,117,119,124,127,131,133,136,138,141,143,145,147,150,153,155,157,162,164,168",
			"name": "Numbers n such that the largest prime divisor of n^4 + n^2 + 1 is equal to the largest prime divisor of (n+1)^4 + (n+1)^2 + 1.",
			"comment": [
				"The sequence is infinite. Proof:",
				"Let p(n) denote the largest prime divisor of n^4 + n^2 + 1 and let q(n) denote the largest prime divisor of n^2 + n + 1. Then p(n) = q(n^2), and from",
				"n^4 + n^2 + 1 = (n^2+1)^2 - n^2 = (n^2-n+1)(n^2+n+1)= ((n-1)^2 + (n-1)+1)(n^2+n+1) it follows that p(n) = max{q(n),q(n-1)} for n\u003e=2.",
				"Keeping in mind that n^2-n+1 is odd, we have",
				"gcd(n^2+n+1,n^2-n+1) = gcd(2n,n^2-n+1)= gcd(n,n^2-n+1)= 1.",
				"Therefore q(n) is different from q(n-1).",
				"To prove the result, it suffices to show that the set",
				"S = {n in Z | n\u003e=2 and q(n) \u003e q(n-1) and q(n) \u003e q(n+1)}",
				"is infinite, since for each n in S one has",
				"p(n) = max{q(n),q(n-1)} = q(n) = max{q(n),q(n+1)} = p(n+1).",
				"Suppose on the contrary that S is finite. Since q(2) = 7 \u003c 13 = q(3) and q(3) = 13 \u003e 7 = q(4), the set S is nonempty. Since it is finite, we can consider its largest element, say m.",
				"Note that it is impossible that q(m)\u003eq(m+1)\u003eq(m+2)\u003e... because all these numbers are positive integers, so there exists a number k\u003e=m such that q(k)\u003cq(k+1) (recall that q(k) is different from q(k+1)). Next observe that it is impossible to have q(k)\u003cq(k+1)\u003cq(k+2)\u003c... because q((k+1)^2) = p(k+1) = max{q(k),q(k+1)}, so let us take the smallest l \u003e= k+1 such that q(l)\u003eq(l+1). By the minimality of l, we have q(l-1)\u003cq(l), so l is in S. Since l \u003e= k + 1 \u003e k \u003e= m, this contradicts the maximality of m, and hence S is indeed infinite."
			],
			"link": [
				"Charles R Greathouse IV, \u003ca href=\"/A257874/b257874.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"3 is in the sequence because 3^4+3^2+1 = 91 = 13*7 and 4^4+4^2+1 = 273 = 13*7*3, so 13 is the greatest prime factor of both expressions."
			],
			"maple": [
				"with(numtheory):for n from 1 to 400 do:x1:=n^4 + n^2 + 1:x2:=(n+1)^4 + (n+1)^2+1:y1:=factorset(x1):n1:=nops(y1):y2:=factorset(x2):n2:=nops(y2):if y1[n1]=y2[n2] then printf(`%d, `,n):else fi:od:"
			],
			"mathematica": [
				"fQ[n_]:=Last[FactorInteger[n^4+n^2+1]][[1]]==Last[FactorInteger[(n+1)^4+(n+1)^2+1]][[1]];Select[Range[168],fQ[#]\u0026] (* _Ivan N. Ianakiev_, Jun 11 2015 *)"
			],
			"program": [
				"(PARI) gpf(n)=if(n\u003c4, return(n)); my(f=factor(n)[,1]); f[#f]",
				"is(n)=my(a=n^4+n^2+1, b=(n+1)^4 +(n+1)^2+1, g=gcd(a,b), p=gpf(g)); g\u003e1 \u0026\u0026 p\u003e=gpf(a/g) \u0026\u0026 p\u003e=gpf(b/g) \\\\ _Charles R Greathouse IV_, May 11 2015"
			],
			"xref": [
				"Cf. A002061, A059826."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Michel Lagneau_, May 11 2015",
			"references": 1,
			"revision": 19,
			"time": "2015-12-11T21:55:48-05:00",
			"created": "2015-06-11T10:27:37-04:00"
		}
	]
}