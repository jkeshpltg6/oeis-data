{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A234514",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 234514,
			"data": "0,0,0,1,2,2,1,1,1,0,1,0,2,2,2,3,4,2,4,2,3,3,3,2,2,3,1,4,2,1,4,2,4,2,5,3,4,1,5,6,4,2,5,5,5,3,5,4,6,3,5,7,10,2,4,5,6,5,5,2,3,5,6,6,4,2,5,3,7,4,5,3,8,7,2,5,9,3,3,2,9,9,6,6,7,6,9,4,7,4,10,8,6,11,11,4,6,4,9,7",
			"name": "Number of ways to write n = k + m with k \u003e 0 and m \u003e 0 such that p = k + phi(m)/2 and q(p) + 1 are both prime, where phi(.) is Euler's totient function, and q(.) is the strict partition function (A000009).",
			"comment": [
				"Conjecture: (i) a(n) \u003e 0 for all n \u003e 12.",
				"(ii) For any integer n \u003e 4, there is a prime p \u003c n - 2 such that q(p + phi(n-p)/2) + 1 is prime.",
				"Clearly, part (i) of the conjecture implies that there are infinitely many primes p with q(p) + 1 prime (cf. A234530).",
				"We have verified part (i) for n up to 10^5."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A234514/b234514.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"a(11) = 1 since 11 = 1 + 10 with 1 + phi(10)/2 = 3 and q(3) + 1 = 3 both prime.",
				"a(27) = 1 since 27 = 7 + 20 with 7 + phi(20)/2 = 11 and q(11) + 1 = 13 both prime.",
				"a(30) = 1 since 30 = 8 + 22 with 8 + phi(22)/2 = 13 and q(13) + 1 = 19 both prime.",
				"a(38) = 1 since 38 = 21 + 17 with 21 + phi(17)/2 = 29 and q(29) + 1 = 257 both prime.",
				"a(572) = 1 since 572 = 77 + 495 with 77 + phi(495)/2 = 197 and q(197) + 1 = 406072423 both prime.",
				"a(860) = 1 since 860 = 523 + 337 with 523 + phi(337)/2 = 691 and q(691) + 1 = 712827068077888961 both prime."
			],
			"mathematica": [
				"f[n_,k_]:=k+EulerPhi[n-k]/2",
				"q[n_,k_]:=PrimeQ[f[n,k]]\u0026\u0026PrimeQ[PartitionsQ[f[n,k]]+1]",
				"a[n_]:=Sum[If[q[n,k],1,0],{k,1,n-1}]",
				"Table[a[n],{n,1,100}]"
			],
			"xref": [
				"Cf. A000009, A000010, A000040, A229835, A233307, A233390, A233417, A234451, A234470, A234475, A234503, A234504, A234530"
			],
			"keyword": "nonn",
			"offset": "1,5",
			"author": "_Zhi-Wei Sun_, Dec 27 2013",
			"references": 16,
			"revision": 19,
			"time": "2013-12-27T12:03:21-05:00",
			"created": "2013-12-27T12:03:21-05:00"
		}
	]
}