{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A325775",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 325775,
			"data": "102,25,20,27,22,29,24,90,26,0,104,10,40,30,42,49,44,70,28,3,23,5,12,7,2,9,4,19,6,14,106,21,60,32,62,47,46,50,48,13,52,15,45,17,34,37,36,39,16,38,108,41,80,43,64,54,66,69,68,33,72,35,74,55,67,57,56,59,58,18,128,61,82,63,84,65,86,76,88,53,92,73,94,75,96",
			"name": "Numbers that bring back the last digit of n on its first digit, using the \"boomerang protocol\" explained in A308306, with no duplicate term (except the -1 terms). See the Comments and Example sections for details.",
			"comment": [
				"All the nonnegative integers will appear in this sequence, except the terms of A308306.",
				"The \"boomerang protocol\" sends 1 to the left (as 1 is odd - the even digits move to the right), jumping over exactly 1 cell. To \"bring back\" 1 to its initial cell, the smallest integer is 102. Let's see how:",
				"Our initial 1 starts for instance here (dots are cells):",
				"....1....",
				"and ends there (S is the starting cell):",
				"..1.S....",
				"We have this pattern now for the \"bring back\" integer (S is the new start, A is the Arrival cell we must reach - which was the starting cell of 1):",
				"..S.A....",
				"The smallest integer starting on S and ending on A is 102:",
				"..1.A....",
				"0...A....",
				".2..A....",
				"We see that 1 jumps to the left over 1 cell, 0 to the right over 0 cell (thus moving to this cell), 2 jumps over 2 cells and lands precisely on the Arrival cell.",
				"Note that many integers can \"bring back\" 1 in its initial cell, 120 is one of them, for instance, or 1410."
			],
			"link": [
				"Jean-Marc Falcoz, \u003ca href=\"/A325775/b325775.txt\"\u003eTable of n, a(n) for n = 1..12000\u003c/a\u003e"
			],
			"example": [
				"The sequence starts with 102,25,20,27,22,29,24,90,... We see that:",
				"a(1) = 102 means that 102 will bring 1 back in its initial cell;",
				"a(2) = 25 means that 25 will bring 2 back in its initial cell;",
				"a(3) = 20 means that 20 will bring 3 back in its initial cell;",
				"a(4) = 27 means that 27 will bring 4 back in its initial cell;",
				"a(5) = 22 means that 22 will bring 5 back in its initial cell;",
				"The general formula being that a(n) brings back (n) in its initial cell.",
				"a(100) = -1 means that 100 is a \"boomerang number\": it \"comes back\" by itself without any external help. Those numbers are listed in A308306."
			],
			"xref": [
				"Cf. A308306 (the \"boomerang numbers\") and A325776 where duplicate terms are admitted."
			],
			"keyword": "sign,base",
			"offset": "1,1",
			"author": "_Eric Angelini_ and _Jean-Marc Falcoz_, May 20 2019",
			"references": 3,
			"revision": 16,
			"time": "2019-05-25T12:10:20-04:00",
			"created": "2019-05-25T12:10:20-04:00"
		}
	]
}