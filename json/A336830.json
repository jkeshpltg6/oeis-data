{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A336830",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 336830,
			"data": "0,1,2,5,9,4,10,3,11,20,30,19,7,91,77,62,46,29,47,28,8,168,146,123,99,74,48,21,49,78,108,139,107,140,106,71,35,72,34,73,33,1353,1311,1268,1224,1179,1133,1086,1038,989,939,888,836,783,729,674,618,561,503,444,384,323,261",
			"name": "The Opera House sequence: a(0) = 0; for n \u003e 0, a(n) = min(a(n-1)/n if n|a(n-1), a(n-1)-n) where a(n) is nonnegative and not already in the sequence. Otherwise a(n) = min(a(n-1)+n,a(n-1)*n) where a(n) is not already in the sequence. Otherwise a(n) = a(n-1)+n.",
			"comment": [
				"This sequence is similar to the Recamán sequence A005132 except that division and multiplication by n are also permitted. This leads to larger variations in the term's values while minimizing the repetition of previously visited terms.",
				"To determine a(n) initially a(n-1)-n is calculated if a(n-1)-n is nonnegative, along with a(n-1)/n if n|a(n-1). If one or both of these have not already appeared in the sequence then a(n) is set to the minimum of these candidates. If neither are candidates then both a(n-1)+n and a(n-1)*n are calculated. If one or both of these have not already appeared in the sequence then a(n) is set to the minimum of these candidates. If neither are candidates, i.e., all of a(n-1)-n, a(n-1)/n, a(n-1)+n, a(n-1)*n are either invalid or have already been visited, then a(n) = a(n-1)+n. However for the first 100 million terms no instance is found where all four options are unavailable, although it is unknown if this eventually occurs for very large n.",
				"For the first 100 million terms the smallest value not appearing is 6. Like the Recamán sequence it is unknown if this and other small unseen terms eventually appear. The largest term is a(50757703) = 6725080695952885. In the same range the number of times division, subtraction, addition, and multiplication are chosen for the next term are 38, 99965692, 34188, and 81 times respectively."
			],
			"link": [
				"Scott R. Shannon, \u003ca href=\"/A336830/a336830.png\"\u003eLine graph of the first 10 million terms\u003c/a\u003e.",
				"\u003ca href=\"/index/Rea#Recaman\"\u003eIndex entries for sequences related to Recamán's sequence\u003c/a\u003e"
			],
			"example": [
				"a(2) = 2. As a(1) = 1, which is not divisible by 2 nor greater than 2, a(2) must be the minimum of 1*2=2 and 1+2=3, so the multiplication is chosen.",
				"a(5) = 4. As a(4) = 9, which is not divisible by 5, and 4 has not appeared previously in the sequence, a(5) = a(4)-5 = 9-5 = 4.",
				"a(82) = 52. As a(81) = 4264 one candidate is 4264-82 = 4182. However 82|4264 and 4264/82 = 52. Neither of these candidates has previously appeared in the sequence, but 52 is the minimum of the two. This is the first time a division operation is used for a(n)."
			],
			"xref": [
				"Cf. A005132, A171884, A330791."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Scott R. Shannon_, Aug 05 2020",
			"references": 1,
			"revision": 21,
			"time": "2021-01-09T22:45:26-05:00",
			"created": "2020-08-09T22:39:58-04:00"
		}
	]
}