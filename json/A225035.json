{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A225035",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 225035,
			"data": "13,17,31,37,71,73,79,97,101,103,107,109,113,127,131,137,139,149,157,163,167,173,179,181,191,193,197,199,239,241,251,271,277,281,283,293,307,311,313,317,331,337,347,349,359,367,373,379,389,397,401,419,421",
			"name": "Primes such that at least one nontrivial permutation of its digits is prime.",
			"comment": [
				"Subset of A055387. All members of A055387 are present here except one digit primes and all primes involving repunits like 11 because of the constraint of nontrivial permutation.",
				"According to Roberts, Richert calls numbers permutation primes when all permutations of the digits are prime. Tattersall uses the definition of this sequence. - _T. D. Noe_, Apr 25 2013",
				"Permutations producing leading zeros are allowed: thus 101 is in the sequence because a nontrivial permutation of its digits is 011. - _Robert Israel_, Aug 13 2019"
			],
			"reference": [
				"H.-E. Richert, On permutation prime numbers, Norsk. Mat. Tidsskr. 33 (1951), p. 50-53.",
				"Joe Roberts, Lure of the Integers, Math. Assoc. of Amer., 1992, p. 293.",
				"James J. Tattersall, Elementary Number Theory in Nine Chapters, Second Edition, Cambridge University Press, p. 121."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A225035/b225035.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"13 is a member since a nontrivial permutation of its digits yield 31 which is also a prime."
			],
			"maple": [
				"dmax:=3: # for all terms of up to dmax digits",
				"Res:= {}:",
				"p:= 1:",
				"do",
				"  p:= nextprime(p);",
				"  if p \u003e 10^dmax then break fi;",
				"  L:= sort(convert(p,base,10),`\u003e`);",
				"  m:= add(L[i]*10^(i-1),i=1..nops(L));",
				"  if assigned(A[m]) then",
				"    if ilog10(A[m])=ilog10(p) then",
				"      Res:= Res union {A[m], p}",
				"    else Res:= Res union {p}",
				"    fi",
				"  else A[m]:= p",
				"  fi",
				"od:",
				"sort(convert(Res,list)); # _Robert Israel_, Aug 13 2019"
			],
			"mathematica": [
				"t={}; Do[p = Prime[n]; list1 = Permutations[IntegerDigits[p]]; If[Length[ Select[Table[FromDigits[n], {n,list1}], PrimeQ]] \u003e 1, AppendTo[t,p]], {n,84}]; t"
			],
			"xref": [
				"Cf. A052902, A007933, A007935, A055387."
			],
			"keyword": "nonn,base",
			"offset": "1,1",
			"author": "_Jayanta Basu_, Apr 24 2013",
			"references": 1,
			"revision": 21,
			"time": "2019-08-13T14:39:16-04:00",
			"created": "2013-04-25T13:21:03-04:00"
		}
	]
}