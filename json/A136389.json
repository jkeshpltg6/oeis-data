{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A136389",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 136389,
			"data": "1,-3,2,3,-7,4,-1,9,-16,8,-5,25,-36,16,1,-19,66,-80,32,7,-63,168,-176,64,-1,33,-192,416,-384,128,-9,129,-552,1008,-832,256,1,-51,450,-1520,2400,-1792,512,11,-231,1452,-4048,5632,-3840,1024,-1,73,-912,4424,-10496,13056,-8192,2048",
			"name": "Triangle read by rows of coefficients of Chebyshev-like polynomials P_{n,3}(x) with 0 omitted (exponents in increasing order).",
			"comment": [
				"If U_n(x), T_n(x) are Chebyshev's polynomials then U_n(x)=P_{n,0}(x), T_n(x)=P_{n,1}(x).",
				"Let n\u003e=3 and k be of the same parity. Consider a set X consisting of (n+k)/2-3 blocks of the size 2 and an additional block of the size 3, then (-1)^((n-k)/2)a(n,k) is the number of n-3-subsets of X intersecting each block of the size 2."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A136389/b136389.txt\"\u003eTable of n, a(n) for n = 3..10197\u003c/a\u003e (rows 3 \u003c= n \u003c= 200), flattened).",
				"Milan Janjic, \u003ca href=\"http://www.pmfbl.org/janjic\"\u003eTwo enumerative functions\u003c/a\u003e.",
				"M. Janjic, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL11/Janjic/janjic19.html\"\u003eOn a class of polynomials with integer coefficients\u003c/a\u003e, JIS 11 (2008) 08.5.2",
				"Milan Janjić, \u003ca href=\"https://arxiv.org/abs/1905.04465\"\u003eOn Restricted Ternary Words and Insets\u003c/a\u003e, arXiv:1905.04465 [math.CO], 2019."
			],
			"formula": [
				"If n\u003e=3 and k are of the same parity then a(n,k)= (-1)^((n-k)/2)*sum((-1)^i*binomial((n+k)/2-3, i)*binomial(n+k-3-2*i, n-3), i=0..(n+k)/2-3) and a(n,k)=0 if n and k are of different parity."
			],
			"example": [
				"Rows are (1),(-3,2),(3,-7,4),(-1,9,-16,8),(-5,25,-36,16),...",
				"since P_{3,3}=x^3, P_{4,3}=-3x^2+2x^4, P_{5,3}=3x-7x^3+4x^5,..."
			],
			"maple": [
				"if modp(n-k, 2)=0 then a[n, k]:=(-1)^((n-k)/2)*sum((-1)^i*binomial((n+k)/2-3, i)*binomial(n+k-3-2*i, n-3), i=0..(n+k)/2-3); end if;"
			],
			"mathematica": [
				"DeleteCases[#, 0] \u0026@ Flatten@ Table[(-1)^((n - k)/2)*Sum[(-1)^i*Binomial[(n + k)/2 - 3, i] Binomial[n + k - 3 - 2 i, n - 3], {i, 0, (n + k)/2 - 3}], {n, 3, 14}, {k, 0 + Boole[OddQ@ n], n, 2}] (* _Michael De Vlieger_, Jul 05 2019 *)"
			],
			"xref": [
				"Cf. A008310, A053117."
			],
			"keyword": "sign,tabf",
			"offset": "3,2",
			"author": "_Milan Janjic_, Mar 30 2008, revised Apr 05 2008",
			"references": 1,
			"revision": 13,
			"time": "2019-07-05T21:01:22-04:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}