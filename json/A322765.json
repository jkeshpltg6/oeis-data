{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A322765",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 322765,
			"data": "1,1,2,2,4,9,5,11,26,66,15,36,92,249,712,52,135,371,1075,3274,10457,203,566,1663,5133,16601,56135,198091,877,2610,8155,26683,91226,325269,1207433,4659138,4140,13082,43263,149410,537813,2014321,7837862,31638625,132315780",
			"name": "Array read by upwards antidiagonals: T(m,n) = number of set partitions of the multiset consisting of one copy each of x_1, x_2, ..., x_m, and two copies each of y_1, y_2, ..., y_n, for m \u003e= 0, n \u003e= 0.",
			"reference": [
				"D. E. Knuth, The Art of Computer Programming, Vol. 4A, Table A-1, page 778."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A322765/b322765.txt\"\u003eAntidiagonals n = 0..139, flattened\u003c/a\u003e"
			],
			"formula": [
				"Knuth p. 779 gives a recurrence using the Bell numbers A000110 (see Maple program).",
				"From _Alois P. Heinz_, Jul 21 2021: (Start)",
				"A(n,k) = A001055(A002110(n+k)*A002110(k)).",
				"A(n,k) = A346500(n+k,k). (End)"
			],
			"example": [
				"The array begins:",
				"    1,    2,     9,     66,      712,     10457,      198091, ...",
				"    1,    4,    26,    249,     3274,     56135,     1207433, ...",
				"    2,   11,    92,   1075,    16601,    325269,     7837862, ...",
				"    5,   36,   371,   5133,    91226,   2014321,    53840640, ...",
				"   15,  135,  1663,  26683,   537813,  13241402,   389498179, ...",
				"   52,  566,  8155, 149410,  3376696,  91914202,  2955909119, ...",
				"  203, 2610, 43263, 894124, 22451030, 670867539, 23456071495, ...",
				"  ..."
			],
			"maple": [
				"B := n -\u003e combinat[bell](n):",
				"P := proc(m,n) local k; global B; option remember;",
				"if n = 0 then B(m)  else",
				"(1/2)*( P(m+2,n-1) + P(m+1,n-1) + add( binomial(n-1,k)*P(m,k), k=0..n-1) ); fi; end; # P(m,n) (which is Knuth's notation) is T(m,n)"
			],
			"mathematica": [
				"P[m_, n_] := P[m, n] = If[n == 0, BellB[m], (1/2)(P[m+2, n-1] + P[m+1, n-1] + Sum[Binomial[n-1, k] P[m, k], {k, 0, n-1}])];",
				"Table[P[m-n, n], {m, 0, 8}, {n, 0, m}] // Flatten (* _Jean-François Alcover_, Jan 02 2019, from Maple *)"
			],
			"program": [
				"(PARI) {T(n, k) = if(k==0, sum(j=0, n, stirling(n, j, 2)), (T(n+2, k-1)+T(n+1, k-1)+sum(j=0, k-1, binomial(k-1, j)*T(n, j)))/2)} \\\\ _Seiichi Manyama_, Nov 21 2020"
			],
			"xref": [
				"Rows include A020555, A322766, A322767.",
				"Columns include A000110, A035098, A322764, A322768.",
				"Main diagonal is A322769.",
				"See A322770 for partitions into distinct parts.",
				"Cf. A001055, A002110, A346500."
			],
			"keyword": "nonn,tabl",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_, Dec 30 2018",
			"references": 9,
			"revision": 42,
			"time": "2021-07-21T14:41:17-04:00",
			"created": "2018-12-30T21:50:09-05:00"
		}
	]
}