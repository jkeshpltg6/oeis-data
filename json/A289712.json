{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A289712",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 289712,
			"data": "1,3,15,22,12,36,24,66,126,420,90,364,270,264,240,210,672,780,864,1050,672,720,924,1092,1344,3240,3312,1260,3600,1200,8910,1080,27104,5940,1680,8568,8910,14280,6384,5670,5544,9600,43092,42900,5280,3360,9504,8580,21600,54288",
			"name": "Smallest integer such that the sum of its n smallest divisors is a square.",
			"comment": [
				"The first corresponding squares are 1, 4, 9, 36, 16, 25, 36, 144, 81, ...",
				"The first squares in the sequence are 1, 36, 3600, ..."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A289712/b289712.txt\"\u003eTable of n, a(n) for n = 1..241\u003c/a\u003e"
			],
			"example": [
				"a(4)=22 because the sum of the first 4 divisors of 22, i.e., 1 + 2 + 11 + 22 = 36, is a square, and 22 is the smallest integer with this property."
			],
			"maple": [
				"N:= 5*10^5: # to get terms before the first term \u003e N",
				"for k from 1 to N do",
				"  d:= sort(convert(numtheory:-divisors(k),list));",
				"  s:= ListTools:-PartialSums(d);",
				"  for m from 1 to nops(d) do",
				"    if not assigned(A[m]) and issqr(s[m]) then A[m]:= k fi",
				"  od",
				"od:",
				"iA:= map(op,{indices(A)}):",
				"seq(A[i],i=1..min({$1..max(iA)+1} minus iA)-1); # _Robert Israel_, Oct 01 2017"
			],
			"mathematica": [
				"Table[k=1;While[Nand[Length@#\u003e=n,IntegerQ[Sqrt[Total@Take[PadRight[#,n],n]]]]\u0026@Divisors@k,k++];k,{n,1,50}] (* Program from _Michael De Vlieger_ adapted for this sequence. See A289776. *)"
			],
			"program": [
				"(PARI) isok(k, n) = {my(v = divisors(k)); if (#v \u003c n, return(0)); issquare(sum(j=1, n, v[j]));}",
				"a(n) = {my(k = 1); while(!isok(k,n), k++); k;} \\\\ _Michel Marcus_, Sep 04 2017"
			],
			"xref": [
				"Cf. A000290, A027750, A240698, A289776."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Michel Lagneau_, Sep 02 2017",
			"references": 2,
			"revision": 22,
			"time": "2019-10-13T06:14:41-04:00",
			"created": "2017-09-21T10:06:12-04:00"
		}
	]
}