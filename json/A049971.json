{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A049971",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 49971,
			"data": "1,3,2,9,24,42,90,213,597,984,1974,3981,8133,17037,37071,87198,244557,401919,803844,1607721,3215613,6431997,12866991,25747038,51564237,103443195,208092192,421008660,861332361,1800360879,3918287223,9215926665,25847419116,42478911570,84957823146",
			"name": "a(n) = a(1) + a(2) + ... + a(n-1) + a(m) for n \u003e= 4, where m = 2*n - 2 - 2^(p+1) and p is the unique integer such that 2^p \u003c n - 1 \u003c= 2^(p+1), starting with a(1) = 1, a(2) = 3, and a(3) = 2.",
			"link": [
				"David A. Corneth, \u003ca href=\"/A049971/b049971.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e"
			],
			"example": [
				"a(5) = a(1) + a(2) + a(3) + a(4) + a(m) = 1 + 3 + 2 + 9 + a(m) = 15 + a(m). where m = 2*n - 2 - 2^(p+1) and 2^p \u003c n - 1 = 4 \u003c= 2^(p+1). We have p = 1 giving m = 2*5 - 2 - 4 = 4. As a(m) = a(4) = 9, we have a(5) = 15 + 9 = 24. - _David A. Corneth_, Apr 26 2020"
			],
			"maple": [
				"s := proc(n) option remember; `if`(n \u003c 1, 0, a(n) + s(n - 1)) end proc:",
				"a := proc(n) option remember;",
				"`if`(n \u003c 4, [1, 3, 2][n], s(n - 1) + a(-2^ceil(log[2](n - 1)) + 2*n - 2)):",
				"end proc:",
				"seq(a(n), n = 1..40); # _Petros Hadjicostas_, Apr 25 2020",
				"# Alternative, uses A062050:",
				"a := proc(n) option remember; if n \u003c 4 then [1, 3, 2][n] else",
				"add(a(i), i = 1..n-1 ) + a(2*A062050(n-2)) fi end:",
				"seq(a(n), n = 1..35); # _Peter Luschny_, Aug 06 2021"
			],
			"mathematica": [
				"a[1] = 1; a[2] = 3; a[3] = 2; a[n_] := a[n] = Sum[a[k], {k, 1, n - 1}] + a[2*n - 2 - 2^Floor[1 + Log[2, n - 2]]]; Table[a[n], {n, 1, 30}] (* _Vaclav Kotesovec_, Apr 26 2020 *)"
			],
			"program": [
				"(PARI) lista(nn) = { my(va = vector(nn)); va[1] = 1; va[2] = 3; va[3] = 2; my(sa = vecsum(va)); for (n=4, nn, va[n] = sa + va[2*n - 2 - 2*2^logint(n-2, 2)]; sa += va[n]; ); va; } \\\\ _Michel Marcus_, Apr 26 2020 (with nn \u003e 2)",
				"(PARI) first(n) = {n = max(n, 3); my(res = vector(n), s = 6, p = 1); res[1]  = 1; res[2] = 3; res[3] = 2; for(i = 4, n, if(i - 1 \u003e 1 \u003c\u003c (p + 1), p++); res[i] = s + res[2*i-2-2^(p+1)]; s += res[i]) ; res} \\\\ _David A. Corneth_, Apr 26 2020"
			],
			"xref": [
				"Cf. A049922 (similar with minus a(m/2)), A049923 (similar with minus a(m)), A049970 (similar with plus a(m/2)).",
				"Cf A062050."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Clark Kimberling_",
			"ext": [
				"Name edited by _Petros Hadjicostas_, Apr 25 2020",
				"More terms from _David A. Corneth_, Apr 26 2020"
			],
			"references": 4,
			"revision": 53,
			"time": "2021-08-06T10:50:44-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}