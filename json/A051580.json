{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A051580",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 51580,
			"data": "1,8,80,960,13440,215040,3870720,77414400,1703116800,40874803200,1062744883200,29756856729600,892705701888000,28566582460416000,971263803654144000,34965496931549184000,1328688883398868992000",
			"name": "a(n) = (2*n+6)!!/6!!, related to A000165 (even double factorials).",
			"comment": [
				"Row m=6 of the array A(3; m,n) := (2*n+m)!!/m!!, m \u003e= 0, n \u003e= 0."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A051580/b051580.txt\"\u003eTable of n, a(n) for n = 0..400\u003c/a\u003e",
				"A. N. Stokes, \u003ca href=\"https://doi.org/10.1017/S0004972700005219\"\u003eContinued fraction solutions of the Riccati equation\u003c/a\u003e, Bull. Austral. Math. Soc. Vol. 25 (1982), 207-214."
			],
			"formula": [
				"a(n) = (2*n+6)!!/6!!.",
				"E.g.f.: 1/(1-2*x)^4.",
				"a(n) = n!*2^(n-4)/3, n\u003e=3. - _Zerinvary Lajos_, Sep 23 2006",
				"G.f.: G(0)/2, where G(k)= 1 + 1/(1 - x/(x + 1/(2*k+8)/G(k+1))); (continued fraction). - _Sergei N. Gladkovskii_, Jun 02 2013",
				"From _Peter Bala_, May 26 2017: (Start)",
				"a(n+1) = (2*n + 8)*a(n) with a(0) = 1.",
				"O.g.f. satisfies the Riccati differential equation 2*x^2*A(x)' = (1 - 8*x)*A(x) - 1 with A(0) = 1.",
				"G.f. as an S-fraction: A(x) = 1/(1 - 8*x/(1 - 2*x/(1 - 10*x/(1 - 4*x/(1 - 12*x/(1 - 6*x/(1 - ... - (2*n + 6)*x/(1 - 2*n*x/(1 - ...))))))))) (by Stokes 1982).",
				"Reciprocal as an S-fraction: 1/A(x) = 1/(1 + 8*x/(1 - 10*x/(1 - 2*x/(1 - 12*x/(1 - 4*x/(1 - 14*x/(1 - 6*x/(1 - ... - (2*n + 8)*x/(1 - 2*n*x/(1 - ...)))))))))). (End)"
			],
			"maple": [
				"seq( mul(2*j+6, j=1..n), n=0..20); # _G. C. Greubel_, Nov 11 2019"
			],
			"mathematica": [
				"Table[2^n*Pochhammer[4, n], {n,0,20}] (* _G. C. Greubel_, Nov 11 2019 *)"
			],
			"program": [
				"(PARI) vector(20, n, prod(j=1,n-1, 2*j+6) ) \\\\ _G. C. Greubel_, Nov 11 2019",
				"(MAGMA) [1] cat [(\u0026*[2*j+6: j in [1..n]]): n in [1..20]]; // _G. C. Greubel_, Nov 11 2019",
				"(Sage) [product( (2*j+6) for j in (1..n)) for n in (0..20)] # _G. C. Greubel_, Nov 11 2019",
				"(GAP) List([0..20], n-\u003e Product([1..n], j-\u003e 2*j+6) ); # _G. C. Greubel_, Nov 11 2019"
			],
			"xref": [
				"Cf. A000165, A001147(n+1), A002866(n+1), A051577, A051578, A051579 (rows m=0..5), A051581, A051582, A051583."
			],
			"keyword": "easy,nonn",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_",
			"references": 10,
			"revision": 24,
			"time": "2019-11-12T04:14:09-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}