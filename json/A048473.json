{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A048473",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 48473,
			"data": "1,5,17,53,161,485,1457,4373,13121,39365,118097,354293,1062881,3188645,9565937,28697813,86093441,258280325,774840977,2324522933,6973568801,20920706405,62762119217,188286357653,564859072961",
			"name": "a(0)=1, a(n) = 3*a(n-1) + 2; a(n) = 2*3^n - 1.",
			"comment": [
				"The number of triangles (of all sizes, including holes) in Sierpiński's triangle after n inscriptions. - Lee Reeves, May 10 2004",
				"The sequence is not only related to Sierpiński's triangle, but also to \"Floret's cube\" and the quaternion factor space Q X Q / {(1,1), (-1,-1)}. It can be written as a_n = ves((A+1)x)^n) as described at the Math Forum Discussions link. - _Creighton Dement_, Jul 28 2004",
				"Relation to C(n) = Collatz function iteration using only odd steps: If we look for record subsequences where C(n) \u003e n, this subsequence starts at 2^n - 1 and stops at the local maximum of 2*3^n - 1. Examples: [3,5], [7,11,17], [15,23,35,53], ..., [127,191,287,431,647,971,1457]. - Lambert Klasen, Mar 11 2005",
				"Group the natural numbers so that the (2n-1)-th group sum is a multiple of the (2n)-th group containing one term. (1,2),(3),(4,5,6,7,8,9,10,11),(12),(13,14,15,16,17,18,19,...38),(39),(40,41,...,118,119),(120), (121,122,123,...) ... a(n) = {the sum of the terms of (2n-1)-th group}/{the term of (2n)th group}. The first term of the odd numbered group is given by A003462. The only term of even numbered group is given by A029858. - _Amarnath Murthy_, Aug 01 2005",
				"a(n)+1 = A008776(n); it appears that this gives the number of terms in the (n+1)-th \"gap\" of numbers missing in A171884. - _M. F. Hasler_, May 09 2013",
				"Sum of n-th row of triangle of powers of 3: 1; 1 3 1; 1 3 9 3 1; 1 3 9 27 9 3 1; ... - _Philippe Deléham_, Feb 23 2014",
				"For n \u003e= 3, also the number of dominating sets in the n-helm graph. - _Eric W. Weisstein_, May 28 2017",
				"The number of elements of length \u003c= n in the free group on two generators. - _Anton Mellit_, Aug 10 2017"
			],
			"reference": [
				"Theoni Pappas, Math Stuff, Wide World Publ/Tetra, San Carlos CA, page 15, 2002"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A048473/b048473.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"C. Dement, \u003ca href=\"http://mathforum.org/kb/thread.jspa?forumID=13\u0026amp;threadID=1962419\"\u003eA paper on floretions and quaternions, some questions\u003c/a\u003e, The Math Forum.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/DominatingSet.html\"\u003eDominating Set\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/HelmGraph.html\"\u003eHelm Graph\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-3)."
			],
			"formula": [
				"n-th difference of a(n), a(n-1), ..., a(0) is 2^(n+1) for n=1, 2, 3, ...",
				"a(0)=1, a(n) = a(n-1) + 3^n + 3^(n-1). - Lee Reeves, May 10 2004",
				"a(n) = (3^n + 3^(n+1) - 2)/2. - _Creighton Dement_, Jul 31 2004",
				"(1, 5, 17, 53, 161, ...) = Ternary (1, 12, 122, 1222, 12222, ...). - _Gary W. Adamson_, May 02 2005",
				"Row sums of triangle A134347. Also, binomial transform of A046055: (1, 4, 8, 16, 32, 64, ...); and double binomial transform of A010684: (1, 3, 1, 3, 1, 3, ...). - _Gary W. Adamson_, Oct 21 2007",
				"G.f.:(1+x)/((1-3*x)(1-x)). - _Zerinvary Lajos_, Jan 11 2009, _R. J. Mathar_, Jan 21 2009",
				"a(0)=1, a(1)=5, a(n) = 4*a(n-1) - 3*a(n-2). - _Harvey P. Dale_, Mar 06 2012",
				"a(n) = Sum_{k=0..n} A112468(n,k)*4^k. - _Philippe Deléham_, Feb 23 2014"
			],
			"example": [
				"a(0) = 1;",
				"a(1) = 1 + 3 + 1 = 5;",
				"a(2) = 1 + 3 + 9 + 3 + 1 = 17;",
				"a(3) = 1 + 3 + 9 + 27 + 9 + 3 + 1 = 53; etc. - _Philippe Deléham_, Feb 23 2014"
			],
			"maple": [
				"g:=x*((1+x)/(1-3*x)/(1-x)): gser:=series(g, x=0, 43): seq(coeff(gser, x, n), n=1..30); # _Zerinvary Lajos_, Jan 11 2009"
			],
			"mathematica": [
				"NestList[3 # + 2 \u0026, 1, 30] (* _Harvey P. Dale_, Mar 06 2012 *)",
				"LinearRecurrence[{4, -3}, {1, 5}, 30] (* _Harvey P. Dale_, Mar 06 2012 *)",
				"Table[2 3^n - 1, {n, 20}] (* _Eric W. Weisstein_, May 28 2017 *)",
				"2 3^Range[20] - 1 (* _Eric W. Weisstein_, May 28 2017 *)"
			],
			"program": [
				"(MAGMA) [2*3^n - 1: n in [0..30]]; // _Vincenzo Librandi_, Sep 23 2011",
				"(PARI) first(m)=vector(m,n,n--;2*3^n - 1) \\\\ _Anders Hellström_, Dec 11 2015"
			],
			"xref": [
				"a(n)=T(2, n), array T given by A048471.",
				"Cf. A003462, A029858. A column of A119725.",
				"Cf. A134347, A046055, A010684.",
				"Cf. A112468, A112739."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Clark Kimberling_",
			"ext": [
				"Better description from _Amarnath Murthy_, May 27 2001",
				"Divided g.f. by x to match the offset - _R. J. Mathar_, Jan 21 2009",
				"Typo in Maple program fixed by Marko Mihaily, Mar 07 2009"
			],
			"references": 52,
			"revision": 79,
			"time": "2018-12-16T04:19:03-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}