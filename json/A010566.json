{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A010566",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 10566,
			"id": "N1903",
			"data": "0,8,24,112,560,2976,16464,94016,549648,3273040,19781168,121020960,748039552,4664263744,29303071680,185307690240,1178635456752,7535046744864,48392012257184,312061600211680,2019822009608592,13117263660884768,85447982919036736",
			"name": "Number of 2n-step 2-dimensional closed self-avoiding paths on square lattice.",
			"comment": [
				"a(n) = 4n*A002931(n). There are (2n) choices for the starting point and 2 choices for the orientation, in order to produce self-avoiding closed paths from a polygon of perimeter 2n. - _Philippe Flajolet_, Nov 22 2003"
			],
			"reference": [
				"B. D. Hughes, Random Walks and Random Environments, Oxford 1995, vol. 1, p. 461.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence)."
			],
			"link": [
				"Felix A. Pahl, \u003ca href=\"/A010566/b010566.txt\"\u003eTable of n, a(n) for n = 1..55\u003c/a\u003e (from Iwan Jensen's computations of A002931, using a(n)=4n*A002931(n))",
				"M. E. Fisher and D. S. Gaunt, \u003ca href=\"http://dx.doi.org/10.1103/PhysRev.133.A224\"\u003eIsing model and self-avoiding walks on hypercubical lattices and high density expansions\u003c/a\u003e, Phys. Rev. 133 (1964) A224-A239.",
				"M. E. Fisher and M. F. Sykes, \u003ca href=\"http://dx.doi.org/10.1103/PhysRev.114.45\"\u003eExcluded-volume problem and the Ising model of ferromagnetism\u003c/a\u003e, Phys. Rev. 114 (1959), 45-58.",
				"P. Flajolet and R. Sedgewick, \u003ca href=\"http://algo.inria.fr/flajolet/Publications/books.html\"\u003eAnalytic Combinatorics\u003c/a\u003e, 2009; see page 364.",
				"A. J. Guttmann and I. G. Enting, \u003ca href=\"https://doi.org/10.1088/0305-4470/21/3/009\"\u003eThe size and number of rings on the square lattice\u003c/a\u003e, J. Phys. A 21 (1988), L165-L172.",
				"Brian Hayes, \u003ca href=\"http://bit-player.org/wp-content/extras/bph-publications/AmSci-1998-07-Hayes-self-avoidance.pdf\"\u003eHow to avoid yourself\u003c/a\u003e, American Scientist 86 (1998) 314-319.",
				"B. J. Hiley and M. F. Sykes, \u003ca href=\"http://dx.doi.org/10.1063/1.1701041\"\u003eProbability of initial ring closure in the restricted random-walk model of a macromolecule\u003c/a\u003e, J. Chem. Phys., 34 (1961), 1531-1537.",
				"Iwan Jensen, \u003ca href=\"https://web.archive.org/web/20151222163324/http://www.ms.unimelb.edu.au/~iwan/saw/SAW_ser.html\"\u003eSeries Expansions for Self-Avoiding Walks\u003c/a\u003e",
				"G. S. Rushbrooke and J. Eve, \u003ca href=\"http://dx.doi.org/10.1063/1.1730595\"\u003eOn Noncrossing Lattice Polygons\u003c/a\u003e, Journal of Chemical Physics, 31 (1959), 1333-1334."
			],
			"mathematica": [
				"A002931 = Cases[Import[\"https://oeis.org/A002931/b002931.txt\", \"Table\"], {_, _}][[All, 2]]; a[n_] := 4n A002931[[n]];",
				"a /@ Range[55] (* _Jean-François Alcover_, Jan 11 2020 *)"
			],
			"xref": [
				"Cf. A002931."
			],
			"keyword": "nonn,nice,walk",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"references": 15,
			"revision": 35,
			"time": "2020-01-11T09:00:40-05:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}