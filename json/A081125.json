{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A081125",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 81125,
			"data": "1,1,2,6,12,60,120,840,1680,15120,30240,332640,665280,8648640,17297280,259459200,518918400,8821612800,17643225600,335221286400,670442572800,14079294028800,28158588057600,647647525324800,1295295050649600",
			"name": "a(n) = n! / floor(n/2)!.",
			"comment": [
				"Product of the largest parts in the partitions of n+1 into exactly two parts, n \u003e 0. - _Wesley Ivan Hurt_, Jan 26 2013 (Clarified on Apr 20 2016)"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A081125/b081125.txt\"\u003eTable of n, a(n) for n = 0..400\u003c/a\u003e",
				"Peter Luschny, \u003ca href=\"/A180000/a180000.pdf\"\u003eDie schwingende Fakultät und Orbitalsysteme\u003c/a\u003e, August 2011.",
				"\u003ca href=\"/index/Di#divseq\"\u003eIndex to divisibility sequences\u003c/a\u003e"
			],
			"formula": [
				"E.g.f.: (1+x)*exp(x^2). - _Vladeta Jovovic_, Sep 24 2003",
				"From _Peter Luschny_, Aug 07 2009: (Start)",
				"a(n) = sqrt(n!*n$) where n$ denotes the swinging factorial (A056040).",
				"a(n) = 2^n Gamma((n+1+(n mod 2))/2)/sqrt(Pi). (End)",
				"E.g.f.: E(0) where E(k) = 1 + x/(1 - x/(x + (k+1)/E(k+1))) ; (continued fraction, 3rd kind, 3-step). - _Sergei N. Gladkovskii_, Sep 20 2012",
				"G.f.: G(0) where G(k) =  1 + x*(2*k+1)/(1 - 2*x/(2*x + 1/G(k+1))); (continued fraction, 3-step). - _Sergei N. Gladkovskii_, Nov 18 2012",
				"Conjecture: a(n) +2*a(n-1) -2*n*a(n-2) +4*(-n+2)*a(n-3) = 0. - _R. J. Mathar_, Nov 26 2012",
				"From _Wesley Ivan Hurt_, Jun 06 2013: (Start)",
				"a(n) = n!/(n-floor((n+1)/2))!.",
				"a(n) = Product_{i = ceiling(n/2)..(n-1)} i. [Note: empty product = 1]",
				"a(n) = P( n, floor((n+1)/2) ), where P(n,k) are the number of k-permutations of n objects. (End)",
				"a(n) = n$*floor(n/2)! where n$ denotes the swinging factorial (A056040). - _Peter Luschny_, Oct 28 2013"
			],
			"example": [
				"a(3) = 6 since 3+1 = 4 has two partitions into two parts, (3,1) and (2,2), and the product of the largest parts is 6. - _Wesley Ivan Hurt_, Jan 26 2013 (Clarified on Apr 20 2016)"
			],
			"maple": [
				"Method 1)  a:=n-\u003en!/floor(n/2)!; seq(a(k),k=0..40); # _Wesley Ivan Hurt_, Jun 03 2013",
				"Method 2)  with(combinat, numbperm); seq(numbperm(k, floor((k+1)/2)), k = 0..40); # _Wesley Ivan Hurt_, Jun 06 2013"
			],
			"mathematica": [
				"Table[n!/Floor[n/2]!, {n, 0, 30}] (* _Wesley Ivan Hurt_, Apr 20 2016 *)"
			],
			"program": [
				"(MAGMA) [Factorial(n)/(Factorial(Floor(n/2))): n in [0..30]]; // _Vincenzo Librandi_, Sep 13 2011",
				"(PARI) a(n)=n!/(n\\2)! \\\\ _Charles R Greathouse IV_, Sep 13 2011",
				"(Sage)",
				"def a(n): return rising_factorial(ceil(n/2),floor(n/2))",
				"[a(n) for n in range(26)]  # _Peter Luschny_, Oct 09 2013"
			],
			"xref": [
				"Cf. A004526, A056040, A081123."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Paul Barry_, Mar 07 2003",
			"references": 6,
			"revision": 70,
			"time": "2020-05-08T17:25:41-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}