{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A268629",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 268629,
			"data": "3,5,7,13,17,19,23,31,41,43,47,61,71,73,79,97,103,127,191,193,223,239,241,311,313,337,409,433,439,457,479,601,719,769,839,911,1009,1031,1033,1129,1151,1201,1249,1319,1321,1559,1801,2089,2281,2521,2689,2999,3049,3361,3529,3889",
			"name": "Primes p that have no squareful primitive roots less than p.",
			"link": [
				"Robert Israel, \u003ca href=\"/A268629/b268629.txt\"\u003eTable of n, a(n) for n = 1..114\u003c/a\u003e",
				"Stephen D. Cohen, Tim Trudgian, \u003ca href=\"http://arxiv.org/abs/1602.02440\"\u003eOn the least square-free primitive root modulo p\u003c/a\u003e, arXiv:1602.02440 [math.NT], 2016."
			],
			"example": [
				"The primitive roots of 7 less than 7 are 3 and 5. None of them are squareful so 7 is in the sequence.",
				"8 is a primitive root of 11, and 8 is squareful, so 11 is not in the sequence."
			],
			"maple": [
				"N:= 10^6: # for terms \u003c= N",
				"S:= {1}: p:= 1:",
				"do",
				"  p:= nextprime(p);",
				"  if p^2 \u003e N then break fi;",
				"  S:= S union map(t -\u003e seq(t*p^i, i=2..floor(log[p](N/t))), select(`\u003c=`,S,N/p^2));",
				"od:",
				"S:= sort(convert(S,list)):",
				"nS:= nops(S):",
				"filter:= proc(p) local i;",
				"  if not isprime(p) then return false fi;",
				"  for i from 1 to nS while S[i] \u003c p do",
				"    if numtheory:-order(S[i],p) = p-1 then return false fi",
				"  od;",
				"  true",
				"end proc:",
				"select(filter, [seq(i,i=3..N,2)]); # _Robert Israel_, Oct 27 2020"
			],
			"mathematica": [
				"selQ[p_] := NoneTrue[PrimitiveRootList[p], #\u003cp \u0026\u0026 AllTrue[FactorInteger[#], #[[2]] \u003e= 2\u0026]\u0026];",
				"Select[Prime[Range[2, 500]], selQ] (* _Jean-François Alcover_, Sep 28 2018 *)"
			],
			"program": [
				"(PARI) ar(p) = my(r, pr, j); r=vector(eulerphi(p-1)); pr=znprimroot(p); for(i=1, p-1, if(gcd(i, p-1)==1, r[j++]=lift(pr^i))); vecsort(r) ; \\\\ from A060749",
				"isok(p) = {my(v = ar(p)); for (i=1, #v, if (ispowerful(v[i]), return(0));); 1;}",
				"lista(nn) = forprime(p=1, nn, if (isok(p), print1(p, \", \")));"
			],
			"xref": [
				"Cf. A001694, A001918, A060749."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Michel Marcus_, Feb 09 2016",
			"references": 1,
			"revision": 15,
			"time": "2020-10-28T03:57:08-04:00",
			"created": "2016-02-10T03:00:21-05:00"
		}
	]
}