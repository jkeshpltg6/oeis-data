{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A053261",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 53261,
			"data": "1,1,1,1,1,1,2,1,1,2,2,2,2,2,2,3,3,3,3,3,4,4,4,4,5,5,5,6,6,6,7,7,7,8,8,9,10,10,10,11,11,12,13,13,14,15,16,16,17,18,19,20,20,21,23,24,25,26,27,28,30,31,32,34,35,37,39,40,41,44,45,47,50,51,53,56,58,60,63,65",
			"name": "Coefficients of the '5th-order' mock theta function psi_1(q).",
			"comment": [
				"Number of partitions of n such that each part occurs at most twice and if k occurs as a part then all smaller positive integers occur.",
				"Strictly unimodal compositions with rising range 1, 2, 3, ..., m where m is the largest part and distinct parts in the falling range (this follows trivially from the comment above). [_Joerg Arndt_, Mar 26 2014]"
			],
			"reference": [
				"Srinivasa Ramanujan, Collected Papers, Chelsea, New York, 1962, pp. 354-355.",
				"Srinivasa Ramanujan, The Lost Notebook and Other Unpublished Papers, Narosa Publishing House, New Delhi, 1988, pp. 19, 21, 22."
			],
			"link": [
				"Vaclav Kotesovec, \u003ca href=\"/A053261/b053261.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e (terms 0..1000 from Alois P. Heinz)",
				"George E. Andrews, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9947-1986-0814916-2\"\u003eThe fifth and seventh order mock theta functions\u003c/a\u003e, Trans. Amer. Math. Soc., 293 (1986) 113-134.",
				"George E. Andrews and Frank G. Garvan, \u003ca href=\"http://dx.doi.org/10.1016/0001-8708(89)90070-4\"\u003eRamanujan's \"lost\" notebook VI: The mock theta conjectures\u003c/a\u003e, Advances in Mathematics, 73 (1989) 242-255.",
				"William J. Keith, \u003ca href=\"https://arxiv.org/abs/1911.04755\"\u003ePartitions into parts simultaneously regular, distinct, and/or flat\u003c/a\u003e, Proceedings of CANT 2016; arXiv:1911.04755 [math.CO], 2019. Mentions this sequence.",
				"George N. Watson, \u003ca href=\"http://plms.oxfordjournals.org/content/s2-42/1/274.extract\"\u003eThe mock theta functions (2)\u003c/a\u003e, Proc. London Math. Soc., series 2, 42 (1937) 274-304."
			],
			"formula": [
				"G.f.: psi_1(q) = Sum_{n\u003e=0} q^(n*(n+1)/2) * Product_{k=1..n} (1 + q^k).",
				"a(n) ~ sqrt(phi) * exp(Pi*sqrt(n/15)) / (2*5^(1/4)*sqrt(n)), where phi = A001622 = (1+sqrt(5))/2 is the golden ratio. - _Vaclav Kotesovec_, Jun 12 2019"
			],
			"maple": [
				"b:= proc(n, i) option remember; `if`(n=0, 1,",
				"      `if`(i\u003en, 0, add(b(n-i*j, i+1), j=1..min(2, n/i))))",
				"    end:",
				"a:= n-\u003e b(n, 1):",
				"seq(a(n), n=0..100);  # _Alois P. Heinz_, Mar 26 2014"
			],
			"mathematica": [
				"Series[Sum[q^(n(n+1)/2) Product[1+q^k, {k, 1, n}], {n, 0, 13}], {q, 0, 100}]",
				"(* Second program: *)",
				"b[n_, i_] := b[n, i] = If[n == 0, 1, If[i \u003e n, 0, Sum[b[n - i*j, i + 1], {j, 1, Min[2, n/i]}]]];",
				"a[n_] := b[n, 1];",
				"Table[a[n], {n, 0, 100}] (* _Jean-François Alcover_, Jun 09 2018, after _Alois P. Heinz_ *)",
				"nmax = 100; CoefficientList[Series[Sum[x^(k*(k+1)/2) * Product[1+x^j, {j, 1, k}], {k, 0, Floor[Sqrt[2*nmax]]}], {x, 0, nmax}], x] (* _Vaclav Kotesovec_, Jun 12 2019 *)"
			],
			"program": [
				"(PARI) N = 66;  x = 'x + O('x^N); gf = sum(n=0,N, x^(n*(n+1)/2) * prod(k=1,n,1+x^k) ); v = Vec(gf) /* _Joerg Arndt_, Apr 21 2013 */"
			],
			"xref": [
				"Other '5th-order' mock theta functions are at A053256, A053257, A053258, A053259, A053260, A053262, A053263, A053264, A053265, A053266, A053267."
			],
			"keyword": "nonn,easy",
			"offset": "0,7",
			"author": "_Dean Hickerson_, Dec 19 1999",
			"references": 17,
			"revision": 38,
			"time": "2021-02-02T21:57:33-05:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}