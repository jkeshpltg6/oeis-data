{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A090086",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 90086,
			"data": "4,341,91,15,4,35,6,9,4,9,10,65,4,15,14,15,4,25,6,21,4,21,22,25,4,9,26,9,4,49,6,25,4,15,9,35,4,39,38,39,4,205,6,9,4,9,46,49,4,21,10,51,4,55,6,15,4,57,15,341,4,9,62,9,4,65,6,25,4,69,9,85,4,15,74,15,4,77,6,9,4,9,21,85,4,15,86,87,4,91,6",
			"name": "Smallest pseudoprime to base n, not necessarily exceeding n (cf. A007535).",
			"comment": [
				"If n-1 is composite, then a(n) \u003c n. - _Thomas Ordowski_, Aug 08 2018",
				"Conjecture: a(n) = A007535(n) for finitely many n. For n \u003e 2; if a(n) \u003e n, then n-1 is prime (find all these primes). - _Thomas Ordowski_, Aug 09 2018",
				"It seems that if a(2^p) = p^2, then 2^p-1 is prime. - _Thomas Ordowski_, Aug 10 2018"
			],
			"link": [
				"Robert G. Wilson v, \u003ca href=\"/A090086/b090086.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (first 1024 terms from Eric Chen)",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Pseudoprime\"\u003ePseudoprime\u003c/a\u003e",
				"\u003ca href=\"/index/Ps#pseudoprimes\"\u003eIndex entries for sequences related to pseudoprimes\u003c/a\u003e"
			],
			"formula": [
				"a(n) = LeastComposite{x; n^(x-1) mod x = 1}."
			],
			"example": [
				"From _Robert G. Wilson v_, Feb 26 2015: (Start)",
				"a(n) = 4 for n = 1 + 4*k, k \u003e= 0.",
				"a(n) = 6 for n = 7 + 12*k, k \u003e= 0.",
				"a(n) = 9 for n = 8 + 18*k, 10 + 18*k, 35 + 36*k, k \u003e= 0.",
				"(End)",
				"a(n) = 10 for n = 51 + 60*k, 11 + 180*k, 131 + 180*k, k \u003e= 0."
			],
			"mathematica": [
				"f[n_] := Block[{k = 1}, While[ GCD[n, k] \u003e 1 || PrimeQ[k] || PowerMod[n, k - 1, k] != 1, j = k++]; k]; Array[f, 91] (* _Robert G. Wilson v_, Feb 26 2015 *)"
			],
			"program": [
				"(PARI) /* a(n) \u003c= 2000 is sufficient up to n = 10000 */",
				"a(n) = for(k=2,2000,if((n^(k-1))%k==1 \u0026\u0026 !isprime(k), return(k))) \\\\ _Eric Chen_, Feb 22 2015",
				"(PARI) a(n) = {forcomposite(k=2, , if (Mod(n,k)^(k-1) == 1, return (k)););} \\\\ _Michel Marcus_, Mar 02 2015"
			],
			"xref": [
				"Cf. A007535, A250200, A090085, A090087, A000790, A239293, A293203.",
				"Cf. A001567, A005935, A005936, A005937, A005938, A005939, A020136 - A020228."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Labos Elemer_, Nov 25 2003",
			"references": 16,
			"revision": 171,
			"time": "2019-07-23T21:08:57-04:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}