{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A138559",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 138559,
			"data": "1,1,-2,-1,1,-1,-1,-1,2,2,-2,0,1,1,-1,0,3,1,-3,-2,3,0,-2,-1,3,2,-4,-2,2,1,-4,-2,5,3,-6,-1,5,1,-5,-3,6,3,-6,-3,7,2,-6,-2,9,5,-10,-5,9,3,-9,-4,11,6,-12,-4,11,5,-12,-5,14,6,-16,-7,15,5,-16,-7,19,9,-20,-8,19,7,-20,-10,24,11,-25,-11,24,9,-26,-11,29,13,-31,-13",
			"name": "Expansion of phi(x) * chi(-x) in powers of x where phi(), chi() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"Vaclav Kotesovec, \u003ca href=\"/A138559/b138559.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e (terms 0..1000 from G. C. Greubel)",
				"O-Yeat Chan, \u003ca href=\"https://doi.org/10.4064/aa120-2-1\"\u003eSome asymptotics for cranks\u003c/a\u003e, Acta Arithmetica 120 (2005), p. 107-143.",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of phi(x) * chi(-x) = phi(-x^2) * chi(x) = psi(x) * chi(-x^2)^2 = f(-x) * chi(x)^2 = f(x) * chi(-x^2) = f(x)^2 / psi(x) = phi(-x^2)^2 / f(-x) if powers of x where phi(), psi(), chi(), f() are Ramanujan theta functions.",
				"Expansion of q^(1/24) * eta(q^2)^4 / (eta(q^4)^2 * eta(q)) in powers of q.",
				"Euler transform of period 4 sequence [ 1, -3, 1, -1, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (2304 t)) = 96^(1/2) (t/i)^(1/2) g(t) where q = exp(2 Pi i t) and g() is the g.f. for A096920.",
				"G.f.: Product_{k\u003e0} (1 - x^k) * (1 + x^(2*k-1))^2.",
				"a(n) ~ c1(n) * exp(Pi*sqrt(2*(n - 1/24)/3)/4) / (2*sqrt(2*(n - 1/24))), where c1(n) = (-1)^(n/2) * 1.847759... if n is even and c1(n) = -(-1)^((n+1)/2) * 0.765366... if n is odd [Chan, 2005, formula 2.2 and 2.7, where a1(n) = a(n) if n is even and a1(n) = -a(n) if n is odd]. - _Vaclav Kotesovec_, May 08 2020",
				"In closed form, abs(c1(n)) = sqrt(2 + (-1)^n*sqrt(2)). - _Vaclav Kotesovec_, May 08 2020"
			],
			"example": [
				"G.f. = 1 + x - 2*x^2 - x^3 + x^4 - x^5  x^6 - x^7 + 2*x^8 + 2*x^9 - 2*x^10 + ...",
				"G.f. = 1/q + q^23 - 2*q^47 - q^71 + q^95 - q^119 - q^143 - q^167 + 2*q^191 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 3, 0, x] QPochhammer[ x, x^2], {x, 0, n}]; (* _Michael Somos_, Aug 31 2014 *)",
				"CoefficientList[Series[QPochhammer[-x] * QPochhammer[x^2] / QPochhammer[x^4], {x, 0, 100}], x] (* _Vaclav Kotesovec_, May 08 2020 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A)^4 / (eta(x + A) * eta(x^4 + A)^2), n))};"
			],
			"xref": [
				"Cf. A096920."
			],
			"keyword": "sign",
			"offset": "0,3",
			"author": "_Michael Somos_, Mar 24 2008",
			"references": 2,
			"revision": 19,
			"time": "2021-03-12T22:24:45-05:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}