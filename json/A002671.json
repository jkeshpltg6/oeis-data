{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002671",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2671,
			"id": "M5174 N2246",
			"data": "1,24,1920,322560,92897280,40874803200,25505877196800,21424936845312000,23310331287699456000,31888533201572855808000,53572735778642397757440000,108431217215972213061058560000",
			"name": "a(n) = 4^n*(2*n+1)!.",
			"comment": [
				"From _Sanjar Abrarov_, Mar 30 2019: (Start)",
				"There is a formula for numerical integration (see MATLAB Central file ID# 71037):",
				"Integral_{x=0..1} f(x) dx = 2*Sum_{m=1..M} Sum_{n\u003e=0} 1/((2*M)^(2*n + 1)*(2*n + 1)!)*f^(2*n)(x)|_x = (m - 1/2)/M, where the notation f^(2*n)(x)|_x = (m - 1/2)/M is the (2*n)-th derivative of the function f(x) at the points x = (m - 1/2)/M.",
				"When we choose M = 1, then the corresponding coefficients are generated as 2*1/(2^(2*n + 1)*(2*n + 1)!) = 1/(4^n*(2*n + 1)!).",
				"Therefore, this sequence also occurs in the denominator of the numerical integration formula at M = 1. (End)",
				"From _Peter Bala_, Oct 03 2019: (Start)",
				"Denominators in the expansion of 2*sinh(x/2) = x + x^3/24 + x^5/1920 + x^7/322560 + ....",
				"If f(x) is a polynomial in x then the central difference f(x+1/2) - f(x-1/2) = 2*sinh(D/2)(f(x)) = D(f(x)) + (1/24)*D^3(f(x)) + (1/1920)*D^5(f(x)) + ..., where D denotes the differential operator d/dx. Formulas for higher central differences in terms of powers of the operator D can be obtained from the expansion of powers of the function 2*sinh(x/2). For example, the expansion (2*sinh(x/2))^2 = x^2 + (1/12)*x^4 + (1/360)*x^6 + .. leads to the second central difference formula f(x+1) - 2*f(x) + f(x-1) = D^2(f(x)) + (1/12)*D^4(f(x)) + (1/360)* D^6(f(x)) + .... See A002674. (End)"
			],
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"S. M. Abrarov and B. M. Quine, \u003ca href=\"https://www.mathworks.com/matlabcentral/fileexchange/71037-array-numerical-integration-by-enhanced-midpoint-rule\"\u003e Array numerical integration by enhanced midpoint rule\u003c/a\u003e, MATLAB Central file ID #: 71037.",
				"H. E. Salzer, \u003ca href=\"https://doi.org/10.1002/sapm1963421162\"\u003eTables of coefficients for obtaining central differences from the derivatives\u003c/a\u003e, Journal of Mathematics and Physics (this journal is also called Studies in Applied Mathematics), 42 (1963), 162-165, plus several inserted tables. [Note that there is a mistake in the definition of this sequence on line 2 of page 164.]",
				"H. E. Salzer, \u003ca href=\"/A002673/a002673.png\"\u003eAnnotated scanned copy of left side of Table I\u003c/a\u003e.",
				"E. W. Weisstein, \u003ca href=\"http://mathworld.wolfram.com/CentralDifference.html\"\u003eCentral Difference\u003c/a\u003e. From MathWorld--A Wolfram Web Resource.",
				"\u003ca href=\"/index/Di#divseq\"\u003eIndex to divisibility sequences\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 16^n * Pochhammer(1,n) * Pochhammer(3/2,n). - _Roger L. Bagula_, Apr 26 2013"
			],
			"program": [
				"(PARI) a(n)=4^n*(2*n+1)!"
			],
			"xref": [
				"A bisection of A002866 and (apart from initial term) also a bisection of A007346.",
				"Row sums of A225076. - _Roger L. Bagula_, Apr 27 2013",
				"Cf. A002672, A002673, A002674, A002675, A002676, A002677."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_ and _Simon Plouffe_",
			"ext": [
				"More terms from _Michael Somos_"
			],
			"references": 15,
			"revision": 61,
			"time": "2019-10-04T09:21:16-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}