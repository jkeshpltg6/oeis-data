{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A287798",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 287798,
			"data": "159,6,5,10,20,40,80,160,320,640,1280,2560,5120,10240,20480,40960,81920,163840,327680,655360,1310720,2621440,5242880,10485760,20971520,41943040,83886080,167772160,335544320,671088640,1342177280,2684354560,5368709120,10737418240",
			"name": "Least k such that A006667(k)/A006577(k) = 1/n.",
			"comment": [
				"A006667: number of tripling steps to reach 1 in '3x+1' problem.",
				"A006577: number of halving and tripling steps to reach 1 in '3x+1' problem.",
				"a(n) = {159, 6} union {A020714}."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A287798/b287798.txt\"\u003eTable of n, a(n) for n = 3..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_01\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2)."
			],
			"formula": [
				"For n \u003e= 5, a(n) = 5*2^n/32. - _David A. Corneth_, Jun 01 2017",
				"From _Colin Barker_, Jun 01 2017: (Start)",
				"G.f.: x^3*(159 - 312*x - 7*x^2) / (1 - 2*x).",
				"a(n) = 2*a(n-1) for n\u003e5.",
				"(End)"
			],
			"example": [
				"a(3) = 159 because A006667(159)/A006577(159) = 18/54 = 1/3."
			],
			"maple": [
				"nn:=10^12:",
				"for n from 3 to 35 do:",
				"ii:=0:",
				"for k from 2 to 10^6 while(ii=0) do:",
				"  m:=k:s1:=0:s2:=0:",
				"   for i from 1 to nn while(m\u003c\u003e1) do:",
				"    if irem(m,2)=0",
				"     then",
				"     s2:=s2+1:m:=m/2:",
				"     else",
				"     s1:=s1+1:m:=3*m+1:",
				"    fi:",
				"   od:",
				"    if n*s1=s1+s2",
				"     then",
				"     ii:=1: printf(`%d, `,k):",
				"     else",
				"    fi:",
				"od:od:"
			],
			"mathematica": [
				"f[u_]:=Module[{a=u,k=0},While[a!=1,k++;If[EvenQ[a],a=a/2,a=a*3+1]];k];Table[f[u],{u,10^7}];g[v_]:=Count[Differences[NestWhileList[If[EvenQ[#],#/2,3#+1]\u0026,v,#\u003e1\u0026]],_?Positive];Table[g[v],{v,10^7}];Do[k=3;While[g[k]/f[k]!=1/n,k++];Print[n,\" \",k],{n,3,35}]"
			],
			"program": [
				"(PARI) a(n) = if(n \u003c 5, [0,0,159,6][n], 5\u003c\u003c(n-5)) \\\\ _David A. Corneth_, Jun 01 2017",
				"(PARI) Vec(x^3*(159 - 312*x - 7*x^2) / (1 - 2*x) + O(x^50)) \\\\ _Colin Barker_, Jun 01 2017"
			],
			"xref": [
				"Cf. A006577, A006666, A006667. Essentially the same as A020714, A084215, A146523 and A257113."
			],
			"keyword": "nonn,easy",
			"offset": "3,1",
			"author": "_Michel Lagneau_, Jun 01 2017",
			"references": 2,
			"revision": 20,
			"time": "2017-07-17T07:19:13-04:00",
			"created": "2017-06-29T13:46:41-04:00"
		}
	]
}