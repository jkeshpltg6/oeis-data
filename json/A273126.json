{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A273126",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 273126,
			"data": "1,1,2,1,2,2,4,1,2,2,4,2,5,4,6,1,2,2,4,2,5,4,6,2,7,5,5,4,5,6,8,1,2,2,4,2,5,4,6,2,7,5,5,4,5,6,8,2,9,7,6,5,6,5,6,4,8,5,7,6,6,8,10,1,2,2,4,2,5,4,6,2,7,5,5,4,5,6,8,2,9,7,6,5,6,5,6,4,8,5,7,6,6,8,10,2,11,9,7,7,8,6,9,5,7,6,8,5,7,6,9,4,9,8,11,5,6,7,7,6,7,6,8,8,9,10,12",
			"name": "Length of \"continued logarithm\" expansion of n.",
			"comment": [
				"It is known that a(n) is bounded by 2 log_2 (n) + 2; see my preprint linked below. - _Jeffrey Shallit_, Jun 14 2016"
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A273126/b273126.txt\"\u003eTable of n, a(n) for n = 1..16384\u003c/a\u003e",
				"Jon Borwein, Neil Calkin, Scott Lindstrom, and Andrew Mattingly, \u003ca href=\"https://www.carma.newcastle.edu.au/jon/clogs.pdf\"\u003eContinued logarithms and associated continued fractions\u003c/a\u003e, preprint, 2016.",
				"J. Shallit, \u003ca href=\"http://arxiv.org/abs/1606.03881\"\u003eLength of the continued logarithm algorithm on rational inputs\u003c/a\u003e, arXiv:1606.03881 [math.NT], June 13 2016."
			],
			"example": [
				"The expansions for n=2 to 19 are [1], [1,1], [2], [2,2], [2,1], [2,0,1,1], [3], [3,3], [3,2], [3,1,1,1], [3,1], [3,0,0,0,1], [3,0,1,1], [3,0,2,0,1,1], [4], [4,4], [4,3], [4,2,1,1]. - _R. J. Mathar_, Jun 02 2016",
				"Displayed as a table with row lengths A000079 as suggested by a(A000079(k)) =1: - _R. J. Mathar_, Jun 04 2016",
				"1,",
				"1,2,",
				"1,2,2,4,",
				"1,2,2,4,2,5,4,6,",
				"1,2,2,4,2,5,4,6,2,7,5,5,4,5,6,8,",
				"1,2,2,4,2,5,4,6,2,7,5,5,4,5,6,8,2,9,7,6,5,6,5,6,4,8,5,7,6,6,8,10,",
				"1,2,2,4,2,5,4,6,2,7,5,5,4,5,6,8,2,9,7,6,5,6,5,6,4,8,5,7,6,6,8,10,2,11,9,7,7,8,6,9,5,7,6,8,5,7,6,9,4,9,8,11,5,6,7,7,6,7,6,8,8,9,10,12"
			],
			"maple": [
				"A273126 := proc(n)",
				"    local a ,x,cf;",
				"    if n = 1 then",
				"        return 1;",
				"    end if;",
				"    cf := [] ;",
				"    x := n ;",
				"    while x \u003e 1 do",
				"        a := ilog2(x) ;",
				"        cf := [op(cf),a] ;",
				"        x := x/2^a ;",
				"        if x = 1 then",
				"            break;",
				"        end if;",
				"        x := 1/(x-1) ;",
				"    end do:",
				"    nops(cf) ;",
				"end proc:",
				"seq(A273126(n),n=1..80) ; # _R. J. Mathar_, Jun 02 2016"
			],
			"program": [
				"(PARI) a(n) = my (x=n); for (w=1, oo, while (x\u003e=2, x /= 2); if (x==1, return (w)); x = 1/(x-1); if (x\u003c=1, return (w))) \\\\ _Rémy Sigrist_, Sep 09 2018"
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Jeffrey Shallit_, May 16 2016",
			"references": 2,
			"revision": 22,
			"time": "2018-09-09T08:14:43-04:00",
			"created": "2016-05-16T08:17:56-04:00"
		}
	]
}