{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000774",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 774,
			"data": "1,2,5,17,74,394,2484,18108,149904,1389456,14257440,160460640,1965444480,26029779840,370643938560,5646837369600,91657072281600,1579093018675200,28779361764249600,553210247226470400,11185850044938240000,237335752951879680000",
			"name": "a(n) = n!*(1 + Sum_{i=1..n} 1/i).",
			"comment": [
				"Number of {12,12*,21}-avoiding signed permutations in the hyperoctahedral group.",
				"Let M be the n X n matrix with M( i, i ) = i+1, other entries = 1. Then a(n) = det(M); example: a(3) = 17 = det([2, 1, 1; 1, 3, 1; 1, 1, 4]). - _Philippe Deléham_, Jun 13 2005.",
				"With offset 1: number of permutations of the n-set into at most two cycles. - _Joerg Arndt_, Jun 22 2009",
				"A ball goes with probability 1/(k+1) from place k to a place j with j=0..k; a(n)/n! is the average number of steps from place n to place 0. - _Paul Weisenhorn_, Jun 03 2010",
				"a(n) is a multiple of A025527(n). - _Charles R Greathouse IV_, Oct 16 2012"
			],
			"link": [
				"Nathaniel Johnston, \u003ca href=\"/A000774/b000774.txt\"\u003eTable of n, a(n) for n = 0..250\u003c/a\u003e",
				"Jean-Christophe Aval, Samuele Giraudo, Théo Karaboghossian, and Adrian Tanasa, \u003ca href=\"https://arxiv.org/abs/1912.06563\"\u003eGraph operads: general construction and natural extensions of canonical operads\u003c/a\u003e, arXiv:1912.06563 [math.CO], 2019.",
				"Jean-Christophe Aval, Samuele Giraudo, Théo Karaboghossian, Adrian Tanasa, \u003ca href=\"https://arxiv.org/abs/2002.10926\"\u003eGraph insertion operads\u003c/a\u003e, arXiv:2002.10926 [math.CO], 2020.",
				"Brant Jones, Katelynn D. Kochalski, Sarah Loeb, and Julia C. Walk, \u003ca href=\"https://arxiv.org/abs/2107.04872\"\u003eStrategy-indifferent games of best choice\u003c/a\u003e, arXiv:2107.04872 [math.CO], 2021.",
				"Sergey Kitaev and Jeffrey Remmel, \u003ca href=\"http://arxiv.org/abs/1201.1323\"\u003eSimple marked mesh patterns\u003c/a\u003e, arXiv preprint arXiv:1201.1323 [math.CO], 2012.",
				"Sergey Kitaev and Jeffrey Remmel, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL15/Kitaev/kitaev5.html\"\u003eQuadrant Marked Mesh Patterns\u003c/a\u003e, J. Int. Seq. 15 (2012) # 12.4.7.",
				"C. Lenormand, \u003ca href=\"/A003095/a003095.pdf\"\u003eArbres et permutations II\u003c/a\u003e, see p. 9.",
				"T. Mansour and J. West, \u003ca href=\"https://arxiv.org/abs/math/0207204\"\u003eAvoiding 2-letter signed patterns\u003c/a\u003e, arXiv:math/0207204 [math.CO], 2002.",
				"J. R. Stembridge, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9947-97-01805-9\"\u003eSome combinatorial aspects of reduced words in finite Coxeter groups\u003c/a\u003e, Trans. Amer. Math. Soc. 349 (1997), no. 4, 1285-1332."
			],
			"formula": [
				"E.g.f.: A(x) = (1-x)^-1 * (1 - log(1-x)).",
				"a(n+1) = (n+1)*a(n) + n!. - _Jon Perry_, Sep 26 2004",
				"a(n) = A000254(n) + n!. - _Mark van Hoeij_, Jul 06 2010",
				"G.f.: 1+x = Sum_{n\u003e=0} a(n) * x^n / Product_{k=1..n+1} (1 + k*x). - _Paul D. Hanna_, Mar 01 2012",
				"a(n) = Sum_{k=0..n} (k+1)*|s(n,k)|, where s(n,k) are Stirling numbers of the first kind (A008275). - _Peter Luschny_, Oct 16 2012",
				"Conjecture: a(n) +(-2*n+1)*a(n-1) +(n-1)^2*a(n-2)=0. - _R. J. Mathar_, Nov 26 2012"
			],
			"example": [
				"(1-x)^-1 * (1 - log(1-x)) = 1 + 2*x + 5/2*x^2 + 17/6*x^3 + ...",
				"G.f.: 1+x = 1/(1+x) + 2*x/((1+x)*(1+2*x)) + 5*x^2/((1+x)*(1+2*x)*(1+3*x)) + 17*x^3/((1+x)*(1+2*x)*(1+3*x)*(1+4*x)) + 74*x^4/((1+x)*(1+2*x)*(1+3*x)*(1+4*x)*(1+5*x)) +..."
			],
			"maple": [
				"A000774 := proc(n) local i,j; j := 0; for i to n do j := j+1/i od; (j+1)*n! end;",
				"ZL :=[S, {S = Set(Cycle(Z),3 \u003e card)}, labelled]: seq(combstruct[count](ZL, size=n), n=1..20); # _Zerinvary Lajos_, Mar 25 2008",
				"a[0]:=1: p:=1: for n from 1 to 20 do",
				"a[n]:=n*a[n-1]+p: p:=p*n: end do: # _Paul Weisenhorn_, Jun 03 2010"
			],
			"mathematica": [
				"Table[n!(1+Sum[1/i,{i,n}]),{n,0,30}] (* _Harvey P. Dale_, Oct 03 2011 *)"
			],
			"program": [
				"(PARI)  a(n)=n!*(1+sum(j=1,n, 1/j ));",
				"(PARI) {a(n)=if(n==0, 1, polcoeff(1+x-sum(k=0, n-1, a(k)*x^k/prod(j=1, k+1, (1+j*x+x*O(x^n)) )), n))} /* _Paul D. Hanna_, Mar 01 2012 */"
			],
			"xref": [
				"Cf. A000254, A000776. Same as A081046 apart from signs."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 15,
			"revision": 80,
			"time": "2021-11-11T18:50:05-05:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}