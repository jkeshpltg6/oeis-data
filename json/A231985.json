{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A231985",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 231985,
			"data": "1,0,0,0,0,1,2,6,9,2,3,4,4,1,6,3,3,7,9,1,6,0,6,0,3,6,3,3,3,5,8,6,6,1,7,7,8,6,3,9,6,5,2,1,8,5,2,8,7,7,6,6,6,4,9,0,3,5,0,7,8,1,3,6,4,3,8,2,8,4,3,2,4,1,8,9,7,4,7,5,1,7,2,2,4,0,2,4,1,2,1,1,9,0,2,4,6,7,9,8,8,5,9,2,0",
			"name": "Decimal expansion of the side length (in degrees) of the spherical square whose solid angle is exactly one deg^2.",
			"comment": [
				"This answers the inverse problem of A231984 (not to be confused with its inverse value): what is the side arc-length of a spherical square required to subtend exactly 1 deg^2. Since the solid angle of a spherical square with side s (in rads) is Omega = 4*arcsin(sin(s/2)^2) (in sr), we have s = 2*arcsin(sqrt(Omega/4)). Converting Omega = 1 deg^2 into steradians (A231982), applying the formula, and converting the result from radians to degrees (A072097), one obtains the result."
			],
			"reference": [
				"G. V. Brummelen, Heavenly Mathematics: The Forgotten Art of Spherical Trigonometry, Princeton University Press, 2012, ISBN 978-0691148922."
			],
			"link": [
				"Stanislav Sykora, \u003ca href=\"/A231985/b231985.txt\"\u003eTable of n, a(n) for n = 1..2000\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Solid_angle#Pyramid\"\u003eSolid angle\u003c/a\u003e, Section 3.3 (Pyramid)",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Square_degree\"\u003eSquare degree\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Steradian\"\u003eSteradian\u003c/a\u003e"
			],
			"formula": [
				"(360/Pi)*arcsin(sqrt(sin((Pi/360)^2)))."
			],
			"example": [
				"1.0000126923441633791606036333586617786396521852877666490350781364..."
			],
			"mathematica": [
				"RealDigits[(360/Pi)*ArcSin[Sqrt[Sin[(Pi/360)^2]]],10,120][[1]] (* _Harvey P. Dale_, Jun 09 2021 *)"
			],
			"program": [
				"(PARI)",
				"default(realprecision, 120);",
				"(360/Pi)*asin(sqrt(sin((Pi/360)^2))) \\\\ or",
				"(180/Pi)*solve(x = 0, 1, 4*asin(sin(x/2)^2) - (Pi/180)^2) \\\\ _Rick L. Shepherd_, Jan 29 2014"
			],
			"xref": [
				"Cf. A000796 (Pi), A072097 (rad/deg), A019685 (deg/rad), A231981 (sr/deg^2), A231982 (deg^2/sr), A231983, A231984 (inverse problem), A231986, A231985, A231987 (same problem for 1sr)."
			],
			"keyword": "nonn,cons,easy",
			"offset": "1,7",
			"author": "_Stanislav Sykora_, Nov 17 2013",
			"references": 5,
			"revision": 16,
			"time": "2021-06-09T13:22:06-04:00",
			"created": "2013-11-17T10:48:22-05:00"
		}
	]
}