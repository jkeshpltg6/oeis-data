{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A229827",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 229827,
			"data": "0,0,0,4,0,24,0,0,0,5308,0,123884,0,0,0,147288372,0,7238567052,0,0,0",
			"name": "Number of circular permutations i_1,...,i_n of 1,...,n such that the n numbers i_1^2+i_2, i_2^2+i_3, ..., i_{n-1}^2+i_n, i_n^2+i_1 form a complete system of residues modulo n.",
			"comment": [
				"Conjecture: (i) a(p) \u003e 0 for any prime p \u003e 3. Moreover, for any finite field F(q) with q elements and a polynomial P(x) over F(q) of degree smaller than q-1, if P(x) is not of the form c-x, then there is a circular permutation a_1, ..., a_q of all the elements of F(q) with",
				"   {P(a_1)+a_2, P(a_2)+a_3, ..., P(a_{q-1})+a_q, P(a_q)+a_1}",
				"  equal to F(q).",
				"   (ii) Let F be any field with |F| \u003e 7, and let A be a finite subset of F with |A| = n \u003e 2. Let P(x) be a polynomial over F whose degree is smaller than p-1 if F is of prime characteristic p. If P(x) is not of the form c-x, then there is a circular permutation a_1, ..., a_n of all the elements of A such that the n sums P(a_1)+a_2, P(a_2)+a_3, ..., P(a_{n-1})+a_n, P(a_n)+a_1 are pairwise distinct.",
				"Clearly, if a(n) \u003e 0, then we must have 1^2+2^2+...+n^2 == 0 (mod n), i.e., (n+1)*(2n+1) == 0 (mod 6). Thus, when n is divisible by 2 or 3, we must have a(n) = 0.",
				"It is well known that for any finite field F(q) with q elements we have sum_{x in F(q)} x^k = 0 for every k = 0, ..., q-2.",
				"Verified for n \u003c 256: a(n) \u003e 0 iff n is not divisible by 2 or 3. - _Bert Dobbelaere_, Apr 23 2021"
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"http://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;c27cfe4c.1309\"\u003eA combinatorial conjecture on finite fields\u003c/a\u003e, a message to Number Theory List, Sept. 30, 2013.",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1309.1679\"\u003eSome new problems in additive combinatorics\u003c/a\u003e , preprint, arXiv:1309.1679 [math.NT], 2013-2014."
			],
			"example": [
				"a(5) = 4 due to the circular permutations",
				"    (1,3,4,5,2), (1,4,2,3,5), (1,5,2,4,3), (1,5,4,2,3).",
				"a(7) \u003e 0 due to the circular permutation (1,2,3,7,4,6,5).",
				"a(11) \u003e 0 due to the circular permutation",
				"    (1,2,3,4,6,5,9,11,10,8,7)."
			],
			"mathematica": [
				"(* A program to compute desired circular permutations for n = 7. *)",
				"  V[i_]:=Part[Permutations[{2,3,4,5,6,7}],i]",
				"m=0",
				"Do[If[Length[Union[Table[Mod[If[j==0,1,Part[V[i],j]]^2+If[j\u003c6,Part[V[i],j+1],1],7],{j,0,6}]]]\u003c7,Goto[aa]];",
				"m=m+1;Print[m,\":\",\" \",1,\" \",Part[V[i],1],\" \",Part[V[i],2],\" \",Part[V[i],3],\" \",Part[V[i],4],\" \",Part[V[i],5],\" \",Part[V[i],6]];Label[aa];Continue,{i,1,6!}]"
			],
			"xref": [
				"Cf. A227456, A229082, A229141."
			],
			"keyword": "nonn,more,hard",
			"offset": "2,4",
			"author": "_Zhi-Wei Sun_, Sep 30 2013",
			"ext": [
				"a(11)-a(22) from _Bert Dobbelaere_, Apr 23 2021"
			],
			"references": 0,
			"revision": 48,
			"time": "2021-04-23T01:55:45-04:00",
			"created": "2013-09-30T12:33:28-04:00"
		}
	]
}