{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A189914",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 189914,
			"data": "1,2,2,4,8,16,24,64,64,192,160,1024,192,4096,896,3840,2048,65536,1152,262144,5120,86016,22528,4194304,6144,5242880,106496,2359296,114688,268435456,7680,1073741824,1048576,34603008,2228224,587202560,147456,68719476736,9961472",
			"name": "a(n) is 2^phi(n) times the least common multiple of the proper divisors of n.",
			"comment": [
				"The sequence relates arithmetic properties of roots of unity in the complex plane with number theoretic properties of integers. This connection often appears as intriguing identities showing products of specific values of the sine function or the gamma function reducing to simple values (see for instance the first formula below)."
			],
			"link": [
				"Charles R Greathouse IV, \u003ca href=\"/A189914/b189914.txt\"\u003eTable of n, a(n) for n = 0..3322\u003c/a\u003e",
				"Peter Luschny and Stefan Wehmeier, \u003ca href=\"http://arxiv.org/abs/0909.1838\"\u003eThe lcm(1,2,...,n) as a product of sine values sampled over the points in Farey sequences\u003c/a\u003e, arXiv:0909.1838 [math.CA], Sep 2009.",
				"Albert Nijenhuis, \u003ca href=\"http://www.jstor.org/stable/10.4169/000298910x515802\"\u003eShort Gamma Products with Simple Values\u003c/a\u003e, The American Mathematical Monthly, Vol. 117, No. 8, Oct 2010, pp. 733-737.",
				"J. Sándor and L. Tóth, \u003ca href=\"http://gdz.sub.uni-goettingen.de/dms/load/img/?PID=GDZPPN002082217\"\u003eA remark on the gamma function\u003c/a\u003e, Elemente der Mathematik, 44 (1989), pp. 73-76, Birkhäuser."
			],
			"formula": [
				"Let R(n) = {k | gcd(n,k) = 1, k = 1..floor(n/2)} and b(n) = product_{R(n)} sin(Pi*k/n) then a(n) = n / b(n)^2 for n \u003e 1.",
				"a(n) = A066781(n)*A048671(n)."
			],
			"maple": [
				"A189914 := n -\u003e 2^numtheory[phi](n)*ilcm(op(numtheory[divisors](n) minus {1,n})): seq(A189914(n), n=0..35);"
			],
			"mathematica": [
				"a[n_] := 2^EulerPhi[n] * LCM @@ Most[Divisors[n]]; a[0] = 1; a[1] = 2; Table[a[n], {n, 0, 38}] (* _Jean-François Alcover_, Jan 22 2014 *)"
			],
			"program": [
				"(PARI) a(n)=if(n,my(p=n); if(isprime(n)||(ispower(n, , \u0026p)\u0026\u0026isprime(p)), n/p, n)\u003c\u003ceulerphi(n),1) \\\\ _Charles R Greathouse IV_, Jun 24 2011"
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Peter Luschny_, Jun 22 2011",
			"references": 1,
			"revision": 22,
			"time": "2017-09-30T05:45:59-04:00",
			"created": "2011-07-05T02:18:14-04:00"
		}
	]
}