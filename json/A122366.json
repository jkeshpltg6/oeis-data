{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A122366",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 122366,
			"data": "1,1,3,1,5,10,1,7,21,35,1,9,36,84,126,1,11,55,165,330,462,1,13,78,286,715,1287,1716,1,15,105,455,1365,3003,5005,6435,1,17,136,680,2380,6188,12376,19448,24310,1,19,171,969,3876,11628,27132,50388,75582,92378,1,21",
			"name": "Triangle read by rows: T(n,k) = binomial(2*n+1,k), 0 \u003c= k \u003c= n.",
			"comment": [
				"Sum of n-th row = A000302(n) = 4^n.",
				"Central terms give A052203.",
				"Reversal of A111418. - _Philippe Deléham_, Mar 22 2007",
				"Coefficient triangle for the expansion of one half of odd powers of 2*x in terms of Chebyshev's T-polynomials: ((2*x)^(2*n+1))/2 = Sum_{k=0..n} a(n,k)*T(2*(n-k)+1,x) with Chebyshev's T-polynomials. See A053120. - _Wolfdieter Lang_, Mar 07 2007",
				"The signed triangle T(n,k)*(-1)^(n-k) appears in the formula (2*sin(phi))^(2*n+1))/2 = Sum_{k=0..n} ((-1)^(n-k))*a(n,k)*sin((2*(n-k)+1)*phi). - _Wolfdieter Lang_, Mar 07 2007",
				"The signed triangle T(n,k)*(-1)^(n-k) appears therefore in the formula (4-x^2)^n = Sum_{k=0..n} ((-1)^(n-k))*a(n,k)*S(2*(n-k),x) with Chebyshev's S-polynomials. See A049310 for S(n,x). - _Wolfdieter Lang_, Mar 07 2007",
				"From _Wolfdieter Lang_, Sep 18 2012: (Start)",
				"The triangle T(n,k) appears also in the formula F(2*l+1)^(2*n+1) = (1/5^n)*Sum_{k=0..n} T(n,k)*F((2*(n-k)+1)*(2*l+1)), l \u003e= 0, n \u003e= 0, with F=A000045 (Fibonacci).",
				"The signed triangle Ts(n,k):=T(n,k)*(-1)^k appears also in the formula",
				"  F(2*l)^(2*n+1) = (1/5^n)*Sum_{k=0..n} Ts(n,k)*F((2(n-k)+1)*2*l), l \u003e= 0, n \u003e= 0, with F=A000045 (Fibonacci).",
				"This is Lemma 2 of the K. Ozeki reference, p. 108, written for odd and even indices separately.",
				"(End)"
			],
			"reference": [
				"T. J. Rivlin, Chebyshev polynomials: from approximation theory to algebra and number theory, 2nd ed., Wiley, New York, 1990, pp. 54-55, Ex. 1.5.31."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A122366/b122366.txt\"\u003eRows n = 0..125 of triangle, flattened\u003c/a\u003e",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards Applied Math. Series 55, Tenth Printing, 1972, p. 795.",
				"C. Lanczos, \u003ca href=\"/A002457/a002457.pdf\"\u003eApplied Analysis\u003c/a\u003e (Annotated scans of selected pages)",
				"K. Ozeki, \u003ca href=\"http://www.fq.math.ca/Papers1/46_47-2/Ozeki.pdf\"\u003eOn Melham's sum\u003c/a\u003e, The Fibonacci Quart. 46/47 (2008/2009), no. 2, 107-110.",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e",
				"\u003ca href=\"/index/Pas#Pascal\"\u003eIndex entries for triangles and arrays related to Pascal's triangle\u003c/a\u003e"
			],
			"formula": [
				"T(n,0)=1; T(n,k) = T(n-1,k-1)*2*n*(2*n+1)/(k*(2*n-k+1)) for k \u003e 0.",
				"T(n,0)=1; for n \u003e 0: T(n,1)=n+2; for n \u003e 1: T(n,n) = T(n-1,n-2) + 3*T(n-1,n-1), T(n,k) = T(n-1,k-2) + 2*T(n-1,k-1) + T(n-1,k), 1 \u003c k \u003c n.",
				"T(n,n) = A001700(n).",
				"T(n,k) = A034868(2*n+1,k) = A007318(2*n+1,k), 0 \u003c= k \u003c= n;",
				"G.f.: (2*y)/((y-1)*sqrt(1-4*x*y)-4*x*y^2+(1-4*x)*y+1). - _Vladimir Kruchinin_, Oct 30 2020"
			],
			"example": [
				".......... / 1 \\ .......... =A062344(0,0)=A034868(0,0),",
				"......... / 1 . \\ ......... =T(0,0)=A034868(1,0),",
				"........ / 1 2 . \\ ........ =A062344(1,0..1)=A034868(2,0..1),",
				"....... / 1 3 ... \\ ....... =T(1,0..1)=A034868(3,0..1),",
				"...... / 1 4 6 ... \\ ...... =A062344(2,0..2)=A034868(4,0..2),",
				"..... / 1 5 10 .... \\ ..... =T(2,0..2)=A034868(5,0..2),",
				".... / 1 6 15 20 ... \\ .... =A062344(3,0..3)=A034868(6,0..3),",
				"... / 1 7 21 35 ..... \\ ... =T(3,0..3)=A034868(7,0..3),",
				".. / 1 8 28 56 70 .... \\ .. =A062344(4,0..4)=A034868(8,0..4),",
				". / 1 9 36 84 126 ..... \\ . =T(4,0..4)=A034868(9,0..4).",
				"Row n=2:[1,5,10] appears in the expansion ((2*x)^5)/2 = T(5,x)+5*T(3,x)+10*T(1,x).",
				"Row n=2:[1,5,10] appears in the expansion ((2*cos(phi))^5)/2 = cos(5*phi)+5*cos(3*phi)+10*cos(1*phi).",
				"The signed row n=2:[1,-5,10] appears in the expansion ((2*sin(*phi))^5)/2 = sin(5*phi)-5*sin(3*phi)+10*sin(phi).",
				"The signed row n=2:[1,-5,10] appears therefore in the expansion (4-x^2)^2 = S(4,x)-5*S(2,x)+10*S(0,x).",
				"Triangle T(n,k) starts:",
				"n\\k 0  1   2   3    4     5     6     7     8     9  ...",
				"0   1",
				"1   1  3",
				"2   1  5  10",
				"3   1  7  21  35",
				"4   1  9  36  84  126",
				"5   1 11  55 165  330   462",
				"6   1 13  78 286  715  1287  1716",
				"7   1 15 105 455 1365  3003  5005  6435",
				"8   1 17 136 680 2380  6188 12376 19448 24310",
				"9   1 19 171 969 3876 11628 27132 50388 75582 92378",
				"...  - _Wolfdieter Lang_, Sep 18 2012",
				"Row n=2, with F(n)=A000045(n) (Fibonacci number), l \u003e= 0, see a comment above:",
				"F(2*l)^5   = (1*F(10*l) - 5*F(6*l) + 10*F(2*l))/25,",
				"F(2*l+1)^5 = (1*F(10*l+5) + 5*F(6*l+3) + 10*F(2*l+1))/25.",
				"- _Wolfdieter Lang_, Sep 19 2012"
			],
			"mathematica": [
				"T[_, 0] = 1;",
				"T[n_, k_] := T[n, k] = T[n-1, k-1] 2n(2n+1)/(k(2n-k+1));",
				"Table[T[n, k], {n, 0, 10}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Dec 01 2021 *)"
			],
			"program": [
				"(Haskell)",
				"a122366 n k = a122366_tabl !! n !! k",
				"a122366_row n = a122366_tabl !! n",
				"a122366_tabl = f 1 a007318_tabl where",
				"   f x (_:bs:pss) = (take x bs) : f (x + 1) pss",
				"-- _Reinhard Zumkeller_, Mar 14 2014"
			],
			"xref": [
				"Cf. A062344.",
				"Odd numbered rows of A008314. Even numbered rows of A008314 are A127673."
			],
			"keyword": "nonn,tabl",
			"offset": "0,3",
			"author": "_Reinhard Zumkeller_, Aug 30 2006",
			"ext": [
				"Chebyshev and trigonometric comments from _Wolfdieter Lang_, Mar 07 2007.",
				"Typo in comments fixed, thanks to _Philippe Deléham_, who indicated this."
			],
			"references": 11,
			"revision": 55,
			"time": "2021-12-01T08:33:29-05:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}