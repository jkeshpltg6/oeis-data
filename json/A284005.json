{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A284005",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 284005,
			"data": "1,2,4,6,8,12,18,24,16,24,36,48,54,72,96,120,32,48,72,96,108,144,192,240,162,216,288,360,384,480,600,720,64,96,144,192,216,288,384,480,324,432,576,720,768,960,1200,1440,486,648,864,1080,1152,1440,1800,2160,1536,1920,2400,2880,3000",
			"name": "a(0) = 1, and for n \u003e 1, a(n) = (1 + A000120(n))*a(floor(n/2)); also a(n) = A000005(A283477(n)).",
			"link": [
				"Antti Karttunen, \u003ca href=\"/A284005/b284005.txt\"\u003eTable of n, a(n) for n = 0..8191\u003c/a\u003e",
				"Mikhail Kurkov, \u003ca href=\"/A284005/a284005.txt\"\u003eComments on A284005\u003c/a\u003e",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A000005(A283477(n)).",
				"From _Mikhail Kurkov_, Nov 10 2019: (Start)",
				"a(n) = (1 + A000120(n))*a(floor(n/2)) for n\u003e0 with a(0)=1.",
				"a(n) = Product_{k=0..floor(log_2(n))} (1 + A000120(floor(n/2^k))) for n\u003e0, a(0)=1.",
				"a(n) = 2*a(f(n)) + Sum_{k=0..floor(log_2(n))-1} a(f(n) + 2^k*(1 - T(n,k))) for n\u003e1 with a(0) = 1, a(1) = 2, f(n) = A053645(n), T(n,k) = floor(n/2^k) mod 2. (End)",
				"From _Mikhail Kurkov_, Aug 23 2021: (Start)",
				"a(2n+1) = a(n) + a(2n) for n \u003e= 0.",
				"a(2n) = a(n) + a(2n - 2^A007814(n)) for n \u003e 0 with a(0) = 1. (End)"
			],
			"example": [
				"From _Mikhail Kurkov_, Feb 27 2020: (Start)",
				"a(1) = 2 because the binary expansion of 2 is 10 and there are 2 open biased rook's tours, namely 12 and 21.",
				"a(2) = 4 because the binary expansion of 4 is 100 and there are 4 open biased rook's tours, namely 132, 213, 231 and 321.",
				"a(3) = 6 because the binary expansion of 6 is 110 and there are 6 open biased rook's tours, namely 123, 132, 213, 231, 312 and 321.",
				"(End)"
			],
			"mathematica": [
				"Table[DivisorSigma[0, #] \u0026@ Apply[Times, Map[#1^#2 \u0026 @@ # \u0026, FactorInteger[#] /. {p_, e_} /; e == 1 :\u003e {Times @@ Prime@ Range@ PrimePi@ p, e}]] \u0026[Times @@ Prime@ Flatten@ Position[#, 1] \u0026@ Reverse@ IntegerDigits[n, 2]], {n, 0, 71}] (* _Michael De Vlieger_, Mar 18 2017 *)"
			],
			"program": [
				"(Scheme) (define (A284005 n) (A000005 (A283477 n)))",
				"(PARI) A284005(n) = numdiv(A283477(n)); \\\\ edited by _Michel Marcus_, May 01 2019, _M. F. Hasler_, Nov 10 2019",
				"(PARI) a(n) = my(k=if(n,logint(n,2)),s=1); prod(i=0,k, s+=bittest(n,k-i)); \\\\ _Kevin Ryde_, Jan 20 2021"
			],
			"xref": [
				"Cf. A000005, A283477, A284001.",
				"Similar recurrences: A124758, A243499, A329369, A341392."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Antti Karttunen_, Mar 18 2017",
			"ext": [
				"Made _Mikhail Kurkov_'s Nov 10 2019 formula the new primary name of this sequence - _Antti Karttunen_, Dec 30 2020"
			],
			"references": 19,
			"revision": 77,
			"time": "2021-09-17T19:06:41-04:00",
			"created": "2017-03-19T01:10:51-04:00"
		}
	]
}