{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A077421",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 77421,
			"data": "1,22,483,10604,232805,5111106,112211527,2463542488,54085723209,1187422368110,26069206375211,572335117886532,12565303387128493,275864339398940314,6056450163389558415,132966039255171344816",
			"name": "Chebyshev sequence U(n,11)=S(n,22) with Diophantine property.",
			"comment": [
				"b(n)^2 - 30*(2*a(n))^2 = 1 with the companion sequence b(n)=A077422(n+1).",
				"For positive n, a(n) equals the permanent of the n X n tridiagonal matrix with 22's along the main diagonal, and i's along the subdiagonal and the superdiagonal (i is the imaginary unit). - _John M. Campbell_, Jul 08 2011",
				"For n\u003e=2, a(n) equals the number of 01-avoiding words of length n-1 on alphabet {0,1,...,21}. - _Milan Janjic_, Jan 25 2015"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A077421/b077421.txt\"\u003eTable of n, a(n) for n = 0..700\u003c/a\u003e",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (22,-1).",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 22*a(n-1) - a(n-1), a(-1)=0, a(0)=1.",
				"a(n) = S(n, 22) with S(n, x) := U(n, x/2) Chebyshev's polynomials of the 2nd kind. See A049310.",
				"a(n) = (ap^(n+1) - am^(n+1))/(ap - am) with ap := 11+2*sqrt(30) and am := 11-2*sqrt(30).",
				"a(n) = Sum_{k=0..floor(n/2)} (-1)^k*binomial(n-k, k)*22^(n-2*k).",
				"a(n) = sqrt((A077422(n+1)^2-1)/30)/2.",
				"G.f.: 1/(1-22*x+x^2). - _Philippe Deléham_, Nov 18 2008",
				"a(n) = Sum_{k, 0\u003c=k\u003c=n} A101950(n,k)*21^k. - _Philippe Deléham_, Feb 10 2012",
				"Product {n \u003e= 0} (1 + 1/a(n)) = 1/5*(5 + sqrt(30)). - _Peter Bala_, Dec 23 2012",
				"Product {n \u003e= 1} (1 - 1/a(n)) = 1/11*(5 + sqrt(30)). - _Peter Bala_, Dec 23 2012"
			],
			"maple": [
				"seq( simplify(ChebyshevU(n, 11)), n=0..20); # _G. C. Greubel_, Dec 23 2019"
			],
			"mathematica": [
				"Table[GegenbauerC[n, 1, 11], {n,0,20}] (* _Vladimir Joseph Stephan Orlovsky_, Sep 11 2008 *)",
				"CoefficientList[Series[1/(1-22x+x^2), {x,0,20}], x] (* _Vincenzo Librandi_, Dec 24 2012 *)",
				"ChebyshevU[Range[21] -1, 11] (* _G. C. Greubel_, Dec 23 2019 *)"
			],
			"program": [
				"(Sage) [lucas_number1(n,22,1) for n in range(1,20)] # _Zerinvary Lajos_, Jun 25 2008",
				"(Sage) [chebyshev_U(n,11) for n in (0..20)] # _G. C. Greubel_, Dec 23 2019",
				"(MAGMA) I:=[1, 22]; [n le 2 select I[n] else 22*Self(n-1)-Self(n-2): n in [1..20]]; // _Vincenzo Librandi_, Dec 24 2012",
				"(PARI) vector( 21, n, polchebyshev(n-1, 2, 11) ) \\\\ _G. C. Greubel_, Dec 23 2019",
				"(GAP) m:=11;; a:=[1,2*m];; for n in [3..20] do a[n]:=2*m*a[n-1]-a[n-2]; od; a; # _G. C. Greubel_, Dec 23 2019"
			],
			"xref": [
				"Chebyshev sequence U(n, m): A000027 (m=1), A001353 (m=2), A001109 (m=3), A001090 (m=4), A004189 (m=5), A004191 (m=6), A007655 (m=7), A077412 (m=8), A049660 (m=9), A075843 (m=10), this sequence (m=11), A077423 (m=12), A097309 (m=13), A097311 (m=14), A097313 (m=15), A029548 (m=16), A029547 (m=17), A144128 (m=18), A078987 (m=19), A097316 (m=33).",
				"Cf. A323182."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Nov 29 2002",
			"references": 28,
			"revision": 57,
			"time": "2021-09-01T09:13:51-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}