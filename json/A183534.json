{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A183534",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 183534,
			"data": "1,1,2,1,2,3,1,2,3,4,1,2,3,6,6,1,2,3,4,9,8,1,2,3,4,10,10,11,1,2,3,4,5,16,11,13,1,2,3,4,5,15,17,12,16,1,2,3,4,5,6,25,18,28,18,1,2,3,4,5,6,21,26,19,29,26,1,2,3,4,5,6,7,36,27,22,30,28,1,2,3,4,5,6,7,28,37,28,64,53,36",
			"name": "Square array of generalized Ulam numbers U(n,k), n\u003e=1, k\u003e=2, read by antidiagonals: U(n,k) = n if n\u003c=k; for n\u003ek, U(n,k) = least number \u003e U(n-1,k) which is a unique sum of k distinct terms U(i,k) with i\u003cn.",
			"comment": [
				"The columns are Ulam-type sequences - see A002858 for further information.  Some of these sequences - but not all - seem to have quite simple generating functions.",
				"U(k+1,k)   = k*(k+1)/2.",
				"U(k+2+j,k) = k^2+j for k\u003e=3 and 0\u003c=j\u003ck.",
				"U(2*k+2,k) = k*(3*k-1)/2 for k\u003e=3."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A183534/b183534.txt\"\u003eAntidiagonals n = 1..87\u003c/a\u003e",
				"\u003ca href=\"/index/U#Ulam_num\"\u003eIndex entries for Ulam numbers\u003c/a\u003e"
			],
			"example": [
				"Square array U(n,k) begins:",
				"  1,  1,  1,  1,  1,  1,  ...",
				"  2,  2,  2,  2,  2,  2,  ...",
				"  3,  3,  3,  3,  3,  3,  ...",
				"  4,  6,  4,  4,  4,  4,  ...",
				"  6,  9, 10,  5,  5,  5,  ...",
				"  8, 10, 16, 15,  6,  6,  ..."
			],
			"maple": [
				"b:= proc(n,i,k,h) option remember;",
				"      local t;",
				"      if n\u003c0 or h\u003c0 then 0",
				"    elif n=0 then `if`(h=0, 1, 0)",
				"    elif i=0 or h=0 then 0",
				"    elif h=1 then t:= v(n, k);",
				"                  `if`(t\u003e0 and t\u003c=i, 1, 0)",
				"             else t:= b(n -U(i, k), i-1, k, h-1);",
				"                  t+ `if`(t\u003e1, 0, b(n, i-1, k, h))",
				"      fi",
				"    end:",
				"v:= proc() 0 end:",
				"U:= proc(n,k) option remember;",
				"      local m;",
				"      if n\u003c=k then v(n,k):= n",
				"              else for m from U(n-1, k)+1",
				"                     while b(m, n-1, k, k)\u003c\u003e1 do od;",
				"                   v(m,k):= n; m",
				"      fi",
				"    end:",
				"seq(seq(U(n, 2+d-n), n=1..d), d=1..12);"
			],
			"mathematica": [
				"b[n_, i_, k_, h_] := b[n, i, k, h] = Module[{t}, Which[n \u003c 0 || h \u003c 0, 0, n == 0, If[h == 0, 1, 0], i == 0 || h == 0, 0, h == 1, t = v[n, k]; If[t \u003e 0 \u0026\u0026 t \u003c= i, 1, 0], True, t = b[n-U[i, k], i-1, k, h-1]; t+If[t \u003e 1, 0, b[n, i-1, k, h]] ] ]; v[_, _] = 0; U[n_, k_] := U[n, k] = Module[{m}, If[n \u003c= k, v[n, k] = n, For[m = U[n-1, k]+1, b[m, n-1, k, k] != 1, m++]; v[m, k] = n; m] ]; Table[Table[U[n, 2+d-n], {n, 1, d}], {d, 1, 12}] // Flatten (* _Jean-François Alcover_, Dec 23 2013, translated from Maple *)"
			],
			"program": [
				"(PARI)",
				"Ulam(N,k=2,v=0)={ my( a=vector(k,i,i), c );",
				"for( n=k,N-1, for( t=1+a[n],9e9, c=0;",
				"  forvec(v=vector(k,i,[i,n]),sum(j=1,k,a[v[j]])==t \u0026 c++\u003e1 \u0026 next(2),2);",
				"  c|next; v\u0026print1(t\",\"); a=concat(a,t); break;",
				")); a}",
				"/* _M. F. Hasler_ */"
			],
			"xref": [
				"Columns k=2-10 give: A002858, A007086, A183527, A183528, A183529, A183530, A183531, A183532, A183533.",
				"Cf. A135737."
			],
			"keyword": "nonn,tabl,look",
			"offset": "1,3",
			"author": "_Jonathan Vos Post_ and _Alois P. Heinz_, Jan 05 2011",
			"references": 10,
			"revision": 31,
			"time": "2018-10-04T19:52:06-04:00",
			"created": "2011-01-05T10:33:22-05:00"
		}
	]
}