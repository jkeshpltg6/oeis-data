{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A020650",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 20650,
			"data": "1,2,1,3,1,3,2,4,1,4,3,5,2,5,3,5,1,5,4,7,3,7,4,7,2,7,5,8,3,8,5,6,1,6,5,9,4,9,5,10,3,10,7,11,4,11,7,9,2,9,7,12,5,12,7,11,3,11,8,13,5,13,8,7,1,7,6,11,5,11,6,13,4,13,9,14,5,14,9,13,3,13,10,17,7,17,10,15,4,15,11,18,7,18",
			"name": "Numerators in recursive bijection from positive integers to positive rationals (the bijection is f(1) = 1, f(2n) = f(n)+1, f(2n+1) = 1/(f(n)+1)).",
			"comment": [
				"The fractions are given in their reduced form, thus gcd(a(n), A020651(n)) = 1 for all n. - _Antti Karttunen_, May 26 2004",
				"From _Yosu Yurramendi_, Jul 13 2014 : (Start)",
				"If the terms (n\u003e0) are written as an array (left-aligned fashion) with rows of length 2^m, m = 0,1,2,3,...",
				"1,",
				"2,1,",
				"3,1,3,2,",
				"4,1,4,3,5,2,5,3,",
				"5,1,5,4,7,3,7,4, 7,2, 7,5, 8,3, 8,5,",
				"6,1,6,5,9,4,9,5,10,3,10,7,11,4,11,7,9,2,9,7,12,5,12,7,11,3,11,8,13,5,13,8,",
				"then the sum of the m-th row is 3^m (m = 0,1,2,), and each column is an arithmetic sequence.",
				"If the rows are written in a right-aligned fashion:",
				"                                                                        1,",
				"                                                                      2,1,",
				"                                                                 3,1, 3,2,",
				"                                                       4,1, 4,3, 5,2, 5,3,",
				"                                    5,1,5,4, 7,3, 7,4, 7,2, 7,5, 8,3, 8,5,",
				"6,1,6,5,9,4,9,5,10,3,10,7,11,4,11,7,9,2,9,7,12,5,12,7,11,3,11,8,13,5,13,8,",
				"each column k is a Fibonacci sequence.",
				"(End)",
				"a(n2^m+1) = a(2n+1), n \u003e 0, m \u003e 0. - _Yosu Yurramendi_, Jun 04 2016"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A020650/b020650.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"D. N. Andreev, \u003ca href=\"http://www.mathnet.ru/eng/mp12\"\u003eOn a Wonderful Numbering of Positive Rational Numbers\u003c/a\u003e, Matematicheskoe Prosveshchenie, Series 3, volume 1, 1997, pages 126-134 (in Russian).  a(n) = numerator of r(n).",
				"Shen Yu-Ting, \u003ca href=\"https://doi.org/10.2307/2320374\"\u003eA Natural Enumeration of Non-Negative Rational Numbers -- An Informal Discussion\u003c/a\u003e, American Mathematical Monthly, volume 87, number 1, January 1980, pages 25-29.  a(n) = numerator of gamma_n.",
				"\u003ca href=\"/index/Fo#fraction_trees\"\u003eIndex entries for fraction trees\u003c/a\u003e"
			],
			"formula": [
				"a(1) = 1, a(2n) = a(n) + A020651(n), a(2n+1) = A020651(2n) = A020651(n). - _Antti Karttunen_, May 26 2004",
				"a(2n) = A020651(2n+1). - _Yosu Yurramendi_, Jul 17 2014",
				"a((2*n+1)*2^m + 1) = A086592(n), n \u003e 0, m \u003e 0. For n = 0, A086592(0) = 1 is needed. For m = 0, a((2*(n+1)) = A086592(n+1). - _Yosu Yurramendi_, Feb 19 2017",
				"a(n) = A002487(1+A231551(n)), n \u003e 0. - _Yosu Yurramendi_, Aug 07 2021"
			],
			"example": [
				"1, 2, 1/2, 3, 1/3, 3/2, 2/3, 4, 1/4, 4/3, ..."
			],
			"maple": [
				"A020650 := n -\u003e `if`((n \u003c 2),n, `if`(type(n,even), A020650(n/2)+A020651(n/2), A020651(n-1)));"
			],
			"mathematica": [
				"f[1] = 1; f[n_?EvenQ] := f[n] = f[n/2]+1; f[n_?OddQ] := f[n] = 1/(f[(n-1)/2]+1); a[n_] := Numerator[f[n]]; Table[a[n], {n, 1, 94}] (* _Jean-François Alcover_, Nov 22 2011 *)",
				"a[1]=1; a[2]=2; a[3]=1; a[n_] := a[n] = Switch[Mod[n, 4], 0, a[n/2+1] + a[n/2], 1, a[(n-1)/2+1], 2, a[(n-2)/2+1] + a[(n-2)/2], 3, a[(n-3)/2]]; Array[a, 100] (* _Jean-François Alcover_, Jan 22 2016, after _Yosu Yurramendi_ *)"
			],
			"program": [
				"(Haskell)",
				"import Data.List (transpose); import Data.Ratio (numerator)",
				"a020650_list = map numerator ks where",
				"   ks = 1 : concat (transpose [map (+ 1) ks, map (recip . (+ 1)) ks])",
				"-- _Reinhard Zumkeller_, Feb 22 2014",
				"(R)",
				"N \u003c- 25 # arbitrary",
				"a \u003c- c(1,2,1)",
				"for(n in 1:N){",
				"  a[4*n]   \u003c- a[2*n] + a[2*n+1]",
				"  a[4*n+1] \u003c-          a[2*n+1]",
				"  a[4*n+2] \u003c- a[2*n] + a[2*n+1]",
				"  a[4*n+3] \u003c- a[2*n]",
				"}",
				"a",
				"# _Yosu Yurramendi_, Jul 13 2014"
			],
			"xref": [
				"Cf. A020651.",
				"Bisection: A086592."
			],
			"keyword": "nonn,easy,frac,nice",
			"offset": "1,2",
			"author": "_David W. Wilson_",
			"references": 19,
			"revision": 56,
			"time": "2021-12-22T00:12:28-05:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}