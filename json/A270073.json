{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A270073",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 270073,
			"data": "1,2,2,1,2,3,2,2,2,4,3,1,2,3,1,1,2,4,4,2,4,4,2,1,2,5,4,3,2,3,3,2,2,4,3,1,4,4,3,4,3,4,3,1,2,7,2,3,2,4,5,2,4,4,6,4,1,3,2,2,3,6,1,4,2,8,4,1,5,7,4,3,4,7,3,4,2,3,2,1,4",
			"name": "Number of ordered ways to write n as x^2 + y^2 + z^2 + w^2 with x*y + 2*z*w a square, where x,y,z are nonnegative integers and w is an integer with x \u003c= y and z \u003e= |w|.",
			"comment": [
				"Conjecture: (i) a(n) \u003e 0 for all n = 0,1,2,..., and a(n) = 1 only for n = 0, 3, 11, 15, 23, 43, 67, 79, 155, 211, 331, 347, 403, 427, 659, 899, 1443, 1955, 2^k*m (k = 0,1,2,... and m = 14, 35, 62, 158, 382).",
				"(ii) Each n = 0,1,2,... can be written as x^2 + y^2 + z^2 + w^2 with x*y + z*w/2 a square, where x,y,z are nonnegative integers and w is an integer with 2 | z*w and x \u003c= y \u003e= |w| \u003c= z.",
				"We have verified that a(n) \u003e 0 for all n = 0,...,10^5.",
				"For more refinements of Lagrange's four-square theorem, see arXiv:1604.06723."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A270073/b270073.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1604.06723\"\u003eRefining Lagrange's four-square theorem\u003c/a\u003e, arXiv:1604.06723 [math.GM], 2016.",
				"Zhi-Wei Sun, \u003ca href=\"http://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;852b9c4a.1604\"\u003eRefine Lagrange's four-square theorem\u003c/a\u003e, a message to Number Theory List, April 26, 2016."
			],
			"example": [
				"a(3) = 1 since 3 = 1^2 + 1^2 + 1^2 + 0^2 with 1 = 1, 1 \u003e 0 and 1*1 + 2*1*0 = 1^2.",
				"a(11) = 1 since 11 = 1^2 + 1^2 + 3^2 + 0^2 with 1 = 1, 3 \u003e 0 and 1*1 + 2*3*0 = 1^2.",
				"a(14) = 1 since 14 = 0^2 + 3^2 + 2^2 + 1^2 with 0 \u003c 3, 2 \u003e 1 and 0*3 + 2*2*1 = 2^2.",
				"a(15) = 1 since 15 = 2^2 + 3^2 + 1^2 + (-1)^2 with 2 \u003c 3, 1 = |-1| and 2*3 + 2*1*(-1) = 2^2.",
				"a(23) = 1 since 23 = 2^2 + 3^2 + 3^2 + (-1)^2 with 2 \u003c 3, 3 \u003e |-1| and 2*3 + 2*3*(-1) = 0^2.",
				"a(35) = 1 since 35 = 3^2 + 3^2 + 4^2 + (-1)^2 with 3 = 3, 4 \u003e |-1| and 3*3 + 2*4*(-1) = 1^2.",
				"a(43) = 1 since 43 = 3^2 + 3^2 + 5^2 + 0^2 with 3 = 3, 5 \u003e 0 and 3*3 + 2*5*0 = 3^2.",
				"a(62) = 1 since 62 = 3^2 + 4^2 + 6^2 + (-1)^2 with 3 \u003c 4, 6 \u003e |-1| and 3*4 + 2*6*(-1) = 0^2.",
				"a(67) = 1 since 67 = 3^2 + 3^2 + 7^2 + 0^2 with 3 = 3, 7 \u003e 0 and 3*3 + 2*7*0 = 3^2.",
				"a(79) = 1 since 79 = 2^2 + 7^2 + 5^2 + (-1)^2 with 2 \u003c 7, 5 \u003e |-1| and 2*7 + 2*5*(-1) = 2^2.",
				"a(155) = 1 since 155 = 3^2 + 11^2 + 4^2 + (-3)^2 with 3 \u003c 11, 4 \u003e |-3| and 3*11 + 2*4*(-3) = 3^2.",
				"a(158) = 1 since 158 = 1^2 + 12^2 + 3^2 + (-2)^2 with 1 \u003c 12, 3 \u003e |-2| and 1*12 + 2*3*(-2) = 0^2.",
				"a(211) = 1 since 211 = 9^2 + 9^2 + 7^2 + 0^2 with 9 = 9, 7 \u003e 0 and 9*9 + 2*7*0 = 9^2.",
				"a(331) = 1 since 331 = 9^2 + 9^2 + 13^2 + 0^2 with 9 = 9, 13 \u003e 0 and 9*9 + 2*13*0 = 9^2.",
				"a(347) = 1 since 347 = 13^2 + 13^2 + 3^2 + 0^2 with 13 = 13, 3 \u003e 0 and 13*13 + 2*3*0 = 13^2.",
				"a(382) = 1 since 382 = 5^2 + 16^2 + 10^2 + 1^2 with 5 \u003c 16, 10 \u003e 1 and 5*16 + 2*10*1 = 10^2.",
				"a(403) = 1 since 403 = 13^2 + 13^2 + 7^2 + 4^2 with 13 = 13, 7 \u003e 4 and 13*13 + 2*7*4 = 15^2.",
				"a(427) = 1 since 427 = 11^2 + 11^2 + 13^2 + 4^2 with 11 = 11, 13 \u003e 4 and 11*11 + 2*13*4 = 15^2.",
				"a(659) = 1 since 659 = 17^2 + 17^2 + 9^2 + 0^2 with 17 = 17, 9 \u003e 0 and 17*17 + 2*9*0 = 17^2.",
				"a(899) = 1 since 899 = 7^2 + 15^2 + 24^2 + 7^2 with 7 \u003c 15, 24 \u003e 7 and 7*15 + 2*24*7 = 21^2.",
				"a(1443) = 1 since 1443 = 7^2 + 31^2 + 17^2 + 12^2 with 7 \u003c 31, 17 \u003e 12 and 7*31 + 2*17*12 = 25^2.",
				"a(1955) = 1 since 1955 = 19^2 + 27^2 + 28^2 + (-9)^2 with 19 \u003c 27, 28 \u003e |-9| and 19*27 + 2*28*(-9) = 3^2."
			],
			"mathematica": [
				"SQ[n_]:=SQ[n]=IntegerQ[Sqrt[n]]",
				"Do[r=0; Do[If[SQ[n-x^2-y^2-z^2]\u0026\u0026SQ[x*y+2*z*Sqrt[n-x^2-y^2-z^2]], r=r+1], {x, 0, Sqrt[n/2]}, {y, x, Sqrt[n-x^2]},{z, Ceiling[-Sqrt[(n-x^2-y^2)/2]], Sqrt[(n-x^2-y^2)/2]}]; Print[n, \" \", r]; Continue, {n, 0, 80}]"
			],
			"xref": [
				"Cf. A000118, A000290, A260625, A261876, A262357, A267121, A268197, A268507, A269400, A271510, A271513, A271518, A271608, A271665, A271714, A271721, A271724, A271775, A271778, A271824, A272084, A272332, A272351, A272620."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Zhi-Wei Sun_, May 07 2016",
			"ext": [
				"All statements in the examples checked by _Rick L. Shepherd_, May 27 2016"
			],
			"references": 20,
			"revision": 65,
			"time": "2016-05-28T12:55:31-04:00",
			"created": "2016-05-07T09:20:23-04:00"
		}
	]
}