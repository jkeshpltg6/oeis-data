{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A168377",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 168377,
			"data": "1,-1,1,1,0,1,-1,2,1,1,1,3,4,2,1,-1,11,10,7,3,1,1,31,32,21,11,4,1,-1,101,100,69,37,16,5,1,1,328,329,228,128,59,22,6,1,-1,1102,1101,773,444,216,88,29,7,1,1,3760,3761,2659,1558,785,341,125,37,8,1",
			"name": "Riordan array (1/(1 + x), x*c(x)), where c(x) is the o.g.f. of Catalan numbers A000108.",
			"link": [
				"Emeric Deutsch, Luca Ferrari, and Simone Rinaldi, \u003ca href=\"https://arxiv.org/abs/math/0702638\"\u003eProduction matrices and Riordan arrays\u003c/a\u003e, arXiv:math/0702638 [math.CO], 2007.",
				"Emeric Deutsch, Luca Ferrari, and Simone Rinaldi, \u003ca href=\"https://doi.org/10.1007/s00026-009-0013-1\"\u003eProduction matrices and Riordan arrays\u003c/a\u003e, Annals of Combinatorics, 13 (2009), 65-85.",
				"L. W. Shapiro, S. Getu, W.-J. Woan, and L. C. Woodson, \u003ca href=\"https://doi.org/10.1016/0166-218X(91)90088-E\"\u003eThe Riordan group\u003c/a\u003e, Discrete Applied Mathematics, 34(1-3) (1991), 229-239.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Riordan_array\"\u003eRiordan array\u003c/a\u003e."
			],
			"formula": [
				"T(n,0) = (-1)^n and T(n,n) = 1.",
				"Sum_{0 \u003c= k \u003c= n} T(n,k) = A032357(n).",
				"From _Petros Hadjicostas_, Aug 08 2020: (Start)",
				"T(n,k) = T(n,k-1) - T(n-1,k-2) for 2 \u003c= k \u003c= n with initial conditions T(n,0) = (-1)^n (n \u003e= 0) and T(n,1) = A032357(n-1) (n \u003e= 1).",
				"T(n,2) = A033297(n).",
				"T(n,n-1) = n - 2 for n \u003e= 1.",
				"|T(n,k)| = |A096470(n,n-k)| for 0 \u003c= k \u003c= n.",
				"Bivariate o.g.f.: 1/((1 + x)*(1 - x*y*c(x))), where c(x) is the o.g.f. of A000108.",
				"Bivariate o.g.f.: (1 - y + x*y*c(x))/((1 + x)*(1 - y + x*y^2)).",
				"Bivariate o.g.f. of |T(n,k)|: (o.g.f. of T(n,k)) + 2*x/(1 - x^2). (End)"
			],
			"example": [
				"Triangle T(n,k) (with rows n \u003e= 0 and columns k = 0..n) begins:",
				"   1;",
				"  -1,   1;",
				"   1,   0,   1;",
				"  -1,   2,   1,  1;",
				"   1,   3,   4,  2,  1;",
				"  -1,  11,  10,  7,  3,  1;",
				"   1,  31,  32, 21, 11,  4, 1;",
				"  -1, 101, 100, 69, 37, 16, 5, 1;",
				"  ...",
				"From _Philippe Deléham_, Sep 14 2014: (Start)",
				"Production matrix begins:",
				"  -1, 1",
				"   0, 1, 1",
				"   0, 1, 1, 1",
				"   0, 1, 1, 1, 1",
				"   0, 1, 1, 1, 1, 1",
				"   0, 1, 1, 1, 1, 1, 1",
				"   0, 1, 1, 1, 1, 1, 1, 1",
				"   0, 1, 1, 1, 1, 1, 1, 1, 1",
				"   ... (End)"
			],
			"program": [
				"(PARI) A000108(n) = binomial(2*n, n)/(n+1);",
				"A032357(n) = sum(k=0, n, (-1)^(n-k)*A000108(k));",
				"T(n, k) = if ((k==0), (-1)^n, if ((n\u003c0) || (k\u003c0), 0, if (k==1, A032357(n-1), if (n \u003e k-1, T(n, k-1) - T(n-1, k-2), 0))));",
				"for(n=0, 10, for (k=0, n, print1(T(n, k), \", \")); print); \\\\ _Petros Hadjicostas_, Aug 08 2020"
			],
			"xref": [
				"Cf. A000012, A000108, A000124, A023443, A032357, A033297, A033999, A091491, A096470, A106566, A127540."
			],
			"keyword": "sign,tabl",
			"offset": "0,8",
			"author": "_Philippe Deléham_, Nov 24 2009",
			"references": 2,
			"revision": 35,
			"time": "2020-08-09T01:18:07-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}