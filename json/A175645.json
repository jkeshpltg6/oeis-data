{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A175645",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 175645,
			"data": "0,0,3,6,0,0,4,2,3,3,4,6,9,4,2,9,5,8,9,5,7,4,7,6,9,4,7,6,2,9,2,3,8,4,6,4,9,4,2,4,9,5,1,6,5,1,3,6,9,4,3,9,1,5,4,8,1,0,3,5,8,7,3,5,1,0,7,4,1,2,0,2,5,3,5,0,4,4,6,1,2,9,2,7,0,6,8,5,0,9,7,5,9,5,3,2,0,7,9,1,7,2,9,6,7,7",
			"name": "Decimal expansion of the sum 1/p^3 over primes == 1 (mod 3).",
			"comment": [
				"The Prime Zeta modulo function at 3 for primes of the form 3k+1, which is Sum_{prime p in A002476} 1/p^3 = 1/7^3 + 1/13^3 + 1/19^3 + 1/31^3 + ...",
				"The complementary sum, Sum_{prime p in A003627} 1/p^3 is given by P_{3,2}(3) = A085541 - 1/3^3 - (this value here) = 0.13412517891546354042859932999943119899..."
			],
			"link": [
				"Jean-François Alcover, \u003ca href=\"/A175645/b175645.txt\"\u003eTable of n, a(n) for n = 0..1008\u003c/a\u003e",
				"R. J. Mathar, \u003ca href=\"http://arxiv.org/abs/1008.2547\"\u003eTable of Dirichlet L-series and Prime Zeta Modulo Functions for Small Moduli\u003c/a\u003e, arXiv:1008.2547 [math.NT], 2010-2015.",
				"\u003ca href=\"/index/Z#zeta_function\"\u003eOEIS index to entries related to the (prime) zeta function\u003c/a\u003e."
			],
			"example": [
				"P_{3,1}(3) = 0.00360042334694295895747694762923846494249516..."
			],
			"mathematica": [
				"(* A naive solution yielding 12 correct digits: *) s1 = s2 = 0.; Do[Switch[Mod[n, 3], 1, If[PrimeQ[n], s1 += 1/n^3], 2, If[PrimeQ[n], s2 += 1/n^3]], {n, 10^7}]; Join[{0, 0}, RealDigits[(PrimeZetaP[3] + s1 - s2 - 1/27)/2, 10, 12][[1]]] (* _Jean-François Alcover_, Mar 15 2018 *)",
				"With[{s=3}, Do[Print[N[1/2 * Sum[(MoebiusMu[2*n + 1]/(2*n + 1)) * Log[(Zeta[s + 2*n*s]*(Zeta[s + 2*n*s, 1/6] - Zeta[s + 2*n*s, 5/6])) / ((1 + 2^(s + 2*n*s))*(1 + 3^(s + 2*n*s)) * Zeta[2*(1 + 2*n)*s])], {n, 0, m}], 120]], {m, 100, 500, 100}]] (* _Vaclav Kotesovec_, Jan 13 2021 *)",
				"S[m_, n_, s_] := (t = 1; sums = 0; difs = 1; While[Abs[difs] \u003e 10^(-digits - 5) || difs == 0, difs = (MoebiusMu[t]/t) * Log[If[s*t == 1, DirichletL[m, n, s*t], Sum[Zeta[s*t, j/m]*DirichletCharacter[m, n, j]^t, {j, 1, m}]/m^(s*t)]]; sums = sums + difs; t++]; sums);",
				"P[m_, n_, s_] := 1/EulerPhi[m] * Sum[Conjugate[DirichletCharacter[m, r, n]] * S[m, r, s], {r, 1, EulerPhi[m]}] + Sum[If[GCD[p, m] \u003e 1 \u0026\u0026 Mod[p, m] == n, 1/p^s, 0], {p, 1, m}];",
				"$MaxExtraPrecision = 1000; digits = 121; Join[{0,0}, RealDigits[Chop[N[P[3, 1, 3], digits]], 10, digits-1][[1]]] (* _Vaclav Kotesovec_, Jan 22 2021 *)"
			],
			"program": [
				"(PARI) s=0; forprimestep(p=1,1e8,3,s+=1./p^3);s \\\\ for illustration only: primes up to 10^N give about 2N+2 correct digits. - _M. F. Hasler_, Apr 22 2021",
				"A175645_upto(N=100)=localprec(N+5);digits((PrimeZeta31(3)+1)\\.1^N)[^1] \\\\ Cf. A175644 for PrimeZeta31. - _M. F. Hasler_, Apr 23 2021"
			],
			"xref": [
				"Cf. A086033 (P_{4,1}(3): same for p==1 (mod 4)), A175644 (P_{3,1}(2): same for 1/p^2), A343613 (P_{3,2}(3): same for p==2 (mod 3)), A085541 (PrimeZeta(3))."
			],
			"keyword": "cons,nonn",
			"offset": "0,3",
			"author": "_R. J. Mathar_, Aug 01 2010",
			"ext": [
				"More digits from _Vaclav Kotesovec_, Jun 27 2020"
			],
			"references": 12,
			"revision": 37,
			"time": "2021-08-25T12:54:23-04:00",
			"created": "2010-08-27T03:00:00-04:00"
		}
	]
}