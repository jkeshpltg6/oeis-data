{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A128583",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 128583,
			"data": "1,1,1,2,1,2,1,1,1,0,3,1,1,1,2,2,1,2,0,1,2,1,0,1,2,3,0,1,1,1,3,2,1,1,1,1,2,0,2,1,2,0,1,0,1,4,1,2,0,1,2,1,2,1,1,3,0,1,2,3,1,0,1,0,0,2,2,1,1,2,2,1,1,2,0,1,2,0,1,1,6,1,1,1,0,2,1",
			"name": "Expansion of chi(x) * psi(x^2) * phi(-x^6) in powers of x where phi(), psi(), chi() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A128583/b128583.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(-5/24) * eta(q^2) * eta(q^4) * eta(q^6)^2 / (eta(q) * eta(q^12)) in powers of q.",
				"Euler transform of period 12 sequence [ 1, 0, 1, -1, 1, -2, 1, -1, 1, 0, 1, -2, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (288 t)) = 6^(1/2) (t/i) g(t) where q = exp(2 Pi i t) and g() is the g.f. for A229723.",
				"a(n) = A128582(4*n) = A259895(3*n) = A260118(4*n). 2 * a(n) = A190615(12*n + 2). - _Michael Somos_, Nov 15 2015",
				"-2 * a(n) = A128580(12*n + 2). - _Michael Somos_, Dec 22 2016"
			],
			"example": [
				"G.f. = 1 + x + x^2 + 2*x^3 + x^4 + 2*x^5 + x^6 + x^7 + x^8 + 3*x^10 + ...",
				"G.f. = q^5 + q^29 + q^53 + 2*q^77 + q^101 + 2*q^125 + q^149 + q^173 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ x^2] QPochhammer[ x^4] QPochhammer[ x^6]^2 / (QPochhammer[ x] QPochhammer[ x^12] ), {x, 0, n}];",
				"a[ n_] := SeriesCoefficient[ QPochhammer[ -x, x^2] EllipticTheta[ 4, 0, x^6] EllipticTheta[ 2, 0, x] / (2 x^(1/4)), {x, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A) * eta(x^4 + A) * eta(x^6 + A)^2 / (eta(x + A) * eta(x^12 + A)), n))};"
			],
			"xref": [
				"Cf. A128580, A128582, A190615, A229723, A259895, A260118."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_Michael Somos_, Mar 11 2007",
			"references": 7,
			"revision": 16,
			"time": "2021-03-12T22:24:44-05:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}