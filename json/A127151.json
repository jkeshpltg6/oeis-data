{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A127151",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 127151,
			"data": "1,2,4,1,8,6,16,26,32,100,64,364,1,128,1288,14,256,4488,118,512,15504,780,1024,53296,4466,2048,182688,23276,4096,625184,113620,8192,2137408,528840,16384,7303360,2375100,1,32768,24946816,10378056,30,65536",
			"name": "Triangle read by rows: T(n,k) is the number of full binary trees with n internal vertices and Strahler number k.",
			"comment": [
				"The Strahler number of a full binary tree is obtained by the following process: label the external vertices (i.e., leaves) by 1 and label an internal vertex recursively as a function of the labels of its sons: if they are distinct, take the largest of the two and if they are equal, increase them by 1. The Strahler number is the label of the root.",
				"Row n has floor(log_2(n+1)) terms.",
				"Row sums yield the Catalan numbers (A000108).",
				"Sum_{k\u003e=1} k*T(n,k) = A127152(n)."
			],
			"link": [
				"P. Flajolet, J.-C. Raoult, and J. Vuillemin, \u003ca href=\"http://dx.doi.org/10.1016/0304-3975(79)90009-4\"\u003eThe number of registers required for evaluating arithmetic expressions\u003c/a\u003e, Theoret. Comput. Sci. 9 (1979), no. 1, 99-125.",
				"J. Francon, \u003ca href=\"https://doi.org/10.1051/ita/1984180403551\"\u003eSur le nombre de registres nécessaires à l'évaluation d'une expression arithmétique\u003c/a\u003e, RAIRO Inform. Theor, 18, 1984, 355-364.",
				"Xavier Gérard Viennot, \u003ca href=\"http://dx.doi.org/10.1016/S0012-365X(01)00265-5\"\u003eA Strahler bijection between Dyck paths and planar trees\u003c/a\u003e, FPSAC (Barcelona, 1999), Discrete Math. 246 (2002), no. 1-3, 317-329. MR1887493 (2003b:05013).",
				"D. Zeilberger, \u003ca href=\"https://doi.org/10.1016/0012-365X(90)90047-L\"\u003eA bijection from ordered trees to binary trees that sends the pruning order to the Strahler number\u003c/a\u003e, Discrete Math., 82, 1990, 89-92."
			],
			"formula": [
				"The column g.f. F[k]=F[k](z) (k=1,2,...) are defined recursively by F[k]=zF[k-1]^2 + 2zF[k](F[0]+F[1]+...+F[k-1]), where F[0]=1. For example, F[1][z]=z/(1-2z); F[2](z) = z^3/[(1-2z)(1-4z+2z^2)]."
			],
			"example": [
				"Triangle starts:",
				"   1;",
				"   2;",
				"   4,   1;",
				"   8,   6;",
				"  16,  26;",
				"  32, 100;",
				"  64, 364,   1;"
			],
			"maple": [
				"F[0]:=1: for k from 1 to 4 do F[k]:=simplify(z*F[k-1]^2/(1-2*z*sum(F[j],j=0..k-1))) od: for k from 1 to 4 do Fser[k]:=series(F[k],z=0,30) od: T:=(n,k)-\u003ecoeff(Fser[k],z,n): for n from 1 to 18 do seq(T(n,k),k=1..floor(simplify(log[2](n+1)))) od; # yields sequence in triangular form"
			],
			"mathematica": [
				"max = 17; f[0] = 1; f[k_] := f[k] = Simplify[z*(f[k-1]^2/(1 - 2*z*Sum[f[j], {j, 0, k-1}]))]; col[k_] := CoefficientList[ Series[f[k], {z, 0, max}], z]; Flatten[ Drop[ Transpose[ DeleteCases[ Table[ col[k], {k, 1, Floor[Log[2, max]]}], {}]] //. {n__, 0} -\u003e {n}, 1]] (* _Jean-François Alcover_, Oct 05 2011 *)"
			],
			"xref": [
				"Cf. A000108, A127152."
			],
			"keyword": "nonn,tabf",
			"offset": "1,2",
			"author": "_Emeric Deutsch_, Jan 06 2007",
			"references": 1,
			"revision": 15,
			"time": "2017-07-22T08:53:53-04:00",
			"created": "2007-01-12T03:00:00-05:00"
		}
	]
}