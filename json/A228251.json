{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A228251",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 228251,
			"data": "-3,12,60,-420,4620,60060,1021020,-19399380,446185740,12939386460,-401120980260,-14841476269620,-608500527054420,26165522663340060,-1229779565176982820,-65178316954380089460,3845520700308425278140,234576762718813941966540,-15716643102160534111758180",
			"name": "Fundamental discriminant of least absolute value with class group of 2-rank n.",
			"comment": [
				"Equivalently, fundamental discriminant of least absolute value with genus group of order 2^n (each such genus group is isomorphic to Z_2 x ... x Z_2 with exactly n copies of Z_2).",
				"The n-th term is the product of n + 1 prime discriminants that are pairwise relatively prime. As the prime discriminants are exactly -8, -4, 8, and +-p for each odd prime p depending upon whether p == 1 (mod 4) or p == 3 (mod 4), respectively, |a(n)| = 2*A002110(n+1) for all n \u003e 1 because usage of -4 precludes usage of +-8 (since the least such product in absolute value is wanted)."
			],
			"link": [
				"Rick L. Shepherd, \u003ca href=\"/A228251/b228251.txt\"\u003eTable of n, a(n) for n = 0..348\u003c/a\u003e",
				"\u003ca href=\"/index/Qua#quadfield\"\u003eIndex entries for sequences related to quadratic fields\u003c/a\u003e"
			],
			"formula": [
				"a(n) = ((-1)^k)*2*A002110(n+1) for n \u003e 1, where k is the number of 1 terms from A100672(3) through A100672(n+1) inclusive; a(0) = -3; a(1) = 12."
			],
			"example": [
				"The term a(0) = -3 because -3 is the fundamental discriminant of least absolute value whose corresponding class group, the trivial group, has 2-rank 0 (and its genus group is thus also the trivial group). Being negative, -3 is the discriminant of an imaginary quadratic field.",
				"The term a(2) = 60 (=(-3)(-4)(5)) because its corresponding class group has 2-rank 2 (one fewer than the number of 60's prime discriminant factors); in this case the genus group is isomorphic to Z_2 x Z_2 (as the class group also happens to be here).  As 60 is positive, it is the discriminant of a real quadratic field."
			],
			"program": [
				"(PARI) {fd = -3; for(n = 0, 348, if(n \u003e 1, pd = prime(n + 1); if(pd%4 == 3, pd = -pd); fd *= pd, if(n, fd = 12)); write(\"b228251.txt\", n, \" \", fd))}"
			],
			"xref": [
				"Cf. A003657, A003658, A002110, A100672."
			],
			"keyword": "sign,easy",
			"offset": "0,1",
			"author": "_Rick L. Shepherd_, Aug 18 2013",
			"references": 1,
			"revision": 6,
			"time": "2013-08-19T13:19:25-04:00",
			"created": "2013-08-19T13:19:25-04:00"
		}
	]
}