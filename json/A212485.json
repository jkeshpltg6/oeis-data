{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A212485",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 212485,
			"data": "1,2,4,3,6,8,12,24,31,62,124,13,16,26,39,48,52,78,104,156,208,312,624,11,22,44,71,142,284,781,1562,3124,7,9,14,18,21,28,36,42,56,63,72,84,93,126,168,186,217,248,252,279,372,434,504,558,651,744,868,1116",
			"name": "Triangle T(n,k) of orders of degree-n irreducible polynomials over GF(5) listed in ascending order.",
			"comment": [
				"The elements m of row n, are also solutions to the equation: multiplicative order of 5 mod m = n, with gcd(m,5) = 1, cf. A050977."
			],
			"reference": [
				"R. Lidl and H. Niederreiter, Finite Fields, 2nd ed., Cambridge Univ. Press, 1997, Table C, pp. 557-560."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A212485/b212485.txt\"\u003eRows n = 1..32, flattened\u003c/a\u003e",
				"V. I. Arnol'd, \u003ca href=\"https://doi.org/10.1070/RM2003v058n04ABEH000641\"\u003eTopology and statistics of formulas of arithmetics\u003c/a\u003e, Uspekhi Mat. Nauk, 58:4(352) (2003), 3-28"
			],
			"formula": [
				"T(n,k) = k-th smallest element of M(n) with M(n) = {d : d | (5^n-1)} \\ (M(1) U M(2) U ... U M(i-1)) for n\u003e1, M(1) = {1,2,4}.",
				"|M(n)| = Sum_{d|n} mu(n/d)*tau(5^d-1) = A059887."
			],
			"example": [
				"Triangle T(n,k) begins:",
				"1,   2,   4;",
				"3,   6,   8, 12,  24;",
				"31, 62, 124;",
				"13, 16,  26, 39,  48,  52,  78,  104,  156, 208, 312, 624;",
				"11, 22,  44, 71, 142, 284, 781, 1562, 3124;"
			],
			"maple": [
				"with(numtheory):",
				"M:= proc(n) option remember;",
				"      `if`(n=1, {1, 2, 4}, divisors(5^n-1) minus U(n-1))",
				"    end:",
				"U:= proc(n) option remember;",
				"      `if`(n=0, {}, M(n) union U(n-1))",
				"    end:",
				"T:= n-\u003e sort([M(n)[]])[]:",
				"seq(T(n), n=1..8);"
			],
			"mathematica": [
				"M[n_] := M[n] = If[n == 1, {1, 2, 4}, Divisors[5^n-1] ~Complement~ U[n-1]];",
				"U[n_] := U[n] = If[n == 0, {}, M[n] ~Union~ U[n - 1]];",
				"T[n_] := Sort[M[n]]; Array[T, 8] // Flatten (* _Jean-François Alcover_, Jun 10 2018, from Maple *)"
			],
			"xref": [
				"Cf. A212906, A059912, A058944, A059499, A059886-A059892.",
				"Column k=3 of A212737.",
				"Column k=1 gives: A218357."
			],
			"keyword": "easy,nonn,look,tabf",
			"offset": "1,2",
			"author": "_Boris Putievskiy_, Jun 02 2012",
			"references": 6,
			"revision": 34,
			"time": "2018-06-10T04:32:05-04:00",
			"created": "2012-06-02T21:04:24-04:00"
		}
	]
}