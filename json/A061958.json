{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A061958",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 61958,
			"data": "1,32,304,639,704,903,3151,4112,6713,34992,96977,2628797,3070243,3661679,8665399",
			"name": "Numbers n such that n divides the (left) concatenation of all numbers \u003c= n written in base 5 (most significant digit on right).",
			"comment": [
				"This sequence differs from A029522 in that all least significant zeros are kept during concatenation.",
				"No more terms \u003c 10^7."
			],
			"link": [
				"\u003ca href=\"/index/N#concat\"\u003eIndex entries for related sequences\u003c/a\u003e"
			],
			"example": [
				"See A061955 for example."
			],
			"mathematica": [
				"b = 5; c = {}; Select[Range[10^4], Divisible[FromDigits[c = Join[Reverse[IntegerDigits[#, b]], c], b], #] \u0026] (* _Robert Price_, Mar 07 2020 *)"
			],
			"xref": [
				"Cf. A029447-A029470, A029471-A029494, A029495-A029518, A029519-A029542, A061931-A061954, A061955-A061978."
			],
			"keyword": "nonn,base,more",
			"offset": "1,2",
			"author": "Larry Reeves (larryr(AT)acm.org), May 24 2001",
			"ext": [
				"Edited and updated by Larry Reeves (larryr(AT)acm.org), Apr 12 2002",
				"a(12)-a(15) from _Lars Blomberg_, Aug 26 2011"
			],
			"references": 1,
			"revision": 13,
			"time": "2020-03-07T20:38:58-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}