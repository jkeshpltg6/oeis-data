{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A335037",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 335037,
			"data": "1,2,2,3,2,4,2,4,3,3,2,6,1,3,4,4,1,5,1,4,3,3,1,8,2,2,3,4,1,6,1,4,3,2,3,8,1,2,2,5,1,5,1,4,5,2,1,8,2,3,2,3,1,5,3,5,2,2,1,8,1,2,4,4,2,5,1,3,2,4,1,10,1,2,4,3,3,4,1,5,3,2,1,7,2,2,2,5",
			"name": "a(n) is the number of divisors of n that are themselves divisible by the product of their digits.",
			"comment": [
				"Inspired by A332268.",
				"A number that is divisible by the product of its digits is called Zuckerman number (A007602); e.g., 24 is a Zuckerman number because it is divisible by 2*4=8 (see links).",
				"a(n) = 1 iff n = 1 or n is prime not repunit \u003e= 13.",
				"a(n) = 2 iff n is prime = 2, 3, 5, 7 or a prime repunit.",
				"Numbers 1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 15, 24, 111111111111111111111 (repunit with 19 times 1's) have all divisors Zuckerman numbers. The sequence of numbers with all Zuckerman divisors is infinite iff there are infinitely many repunit primes (see A004023)."
			],
			"link": [
				"David A. Corneth, \u003ca href=\"/A335037/b335037.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Giovanni Resta, \u003ca href=\"http://www.numbersaplenty.com/set/Zuckerman_number/\"\u003eZuckerman numbers\u003c/a\u003e, Numbers Aplenty.",
				"Gérard Villemin, \u003ca href=\"http://villemin.gerard.free.fr/Wwwgvmm/Decompos/Zuckerma.htm#Zuck\"\u003eNombres de Zuckerman\u003c/a\u003e, Types de nombres."
			],
			"example": [
				"For n = 4, the divisors are 1, 2, 4 and they are all Zuckerman numbers, so a(4) = 3.",
				"For n = 14, the divisors are 1, 2, 7 and 14. Only 1, 2 and 7 are Zuckerman numbers, so a(14) = 3."
			],
			"mathematica": [
				"zuckQ[n_] := (prodig = Times @@ IntegerDigits[n]) \u003e 0\u0026\u0026 Divisible[n, prodig]; a[n_] := Count[Divisors[n], _?(zuckQ[#] \u0026)]; Array[a, 100] (* _Amiram Eldar_, Jun 03 2020 *)"
			],
			"program": [
				"(PARI) iszu(n) = my(p=vecprod(digits(n))); p \u0026\u0026 !(n % p);",
				"a(n) = sumdiv(n, d, iszu(d)); \\\\ _Michel Marcus_, Jun 03 2020"
			],
			"xref": [
				"Cf. A000005, A004023, A007602, A335038.",
				"Similar with: A001227 (odd divisors), A087990 (palindromic divisors), A087991 (non-palindromic divisors), A242627 (divisors \u003c 10), A332268 (Niven divisors)."
			],
			"keyword": "nonn,base",
			"offset": "1,2",
			"author": "_Bernard Schott_, Jun 03 2020",
			"references": 4,
			"revision": 40,
			"time": "2021-03-28T15:51:23-04:00",
			"created": "2020-06-10T17:44:39-04:00"
		}
	]
}