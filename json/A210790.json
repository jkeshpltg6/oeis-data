{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A210790",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 210790,
			"data": "1,1,2,1,2,3,1,4,5,5,1,4,10,10,8,1,6,14,24,20,13,1,6,21,38,52,38,21,1,8,27,65,96,109,71,34,1,8,36,92,176,224,220,130,55,1,10,44,136,280,446,500,434,235,89,1,10,55,180,440,772,1066,1074,839,420,144,1",
			"name": "Triangle of coefficients of polynomials v(n,x) jointly generated with A210789; see the Formula section.",
			"comment": [
				"Row n starts with 1 and ends with F(n+1), where F=A000045 (Fibonacci numbers).",
				"Column 2: 2,2,4,4,6,6,8,8,...",
				"Row sums: A105476.",
				"Alternating row sums: signed Fibonacci numbers.",
				"For a discussion and guide to related arrays, see A208510.",
				"Subtriangle of the triangle given by (1, 0, -1, 0, 0, 0, 0, 0, 0, 0, ...) DELTA (0, 2, -1/2, -1/2, 0, 0, 0, 0, 0, 0, 0, ...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, Mar 28 2012"
			],
			"formula": [
				"u(n,x) = u(n-1,x) + x*v(n-1,x),",
				"v(n,x) = (x+2)*u(n-1,x) + (x-1)*v(n-1,x),",
				"where u(1,x)=1, v(1,x)=1.",
				"From _Philippe Deléham_, Mar 28 2012: (Start)",
				"As DELTA-triangle T(n,k) with 0 \u003c= k \u003c= n:",
				"G.f.: (1+x-y*x-y^2*x^2)/(1-y*x-x^2-y*x^2-y^2*x^2).",
				"T(n,k) = T(n-1,k-1) + T(n-2,k) + T(n-2,k-1) + T(n-2,k-2), T(0,0) = T(1,0) = T(2,0) = 1, T(2,1) = 2, T(1,1) = T(2,2) = 0 and T(n,k) = 0 if k \u003c 0 or if k \u003e n. (End)"
			],
			"example": [
				"First five rows:",
				"  1;",
				"  1,  2;",
				"  1,  2,  3;",
				"  1,  4,  5,  5;",
				"  1,  4, 10, 10,  8;",
				"First three polynomials v(n,x):",
				"  1",
				"  1 + 2x",
				"  1 + 2x + 3x^2.",
				"From _Philippe Deléham_, Mar 28 2012: (Start)",
				"(1, 0, -1, 0, 0, ...) DELTA (0, 2, -1/2, -1/2, 0, 0, ...) begins:",
				"  1;",
				"  1,  0;",
				"  1,  2,  0;",
				"  1,  2,  3,  0;",
				"  1,  4,  5,  5,  0;",
				"  1,  4, 10, 10,  8,  0; (End)"
			],
			"mathematica": [
				"u[1, x_] := 1; v[1, x_] := 1; z = 16;",
				"u[n_, x_] := u[n - 1, x] + (x + j)*v[n - 1, x] + c;",
				"d[x_] := h + x; e[x_] := p + x;",
				"v[n_, x_] := d[x]*u[n - 1, x] + e[x]*v[n - 1, x] + f;",
				"j = 0; c = 0; h = 2; p = -1; f = 0;",
				"Table[Expand[u[n, x]], {n, 1, z/2}]",
				"Table[Expand[v[n, x]], {n, 1, z/2}]",
				"cu = Table[CoefficientList[u[n, x], x], {n, 1, z}];",
				"TableForm[cu]",
				"Flatten[%]    (* A210789 *)",
				"cv = Table[CoefficientList[v[n, x], x], {n, 1, z}];",
				"TableForm[cv]",
				"Flatten[%]    (* A210790 *)",
				"Table[u[n, x] /. x -\u003e 1, {n, 1, z}]   (* A006138 *)",
				"Table[v[n, x] /. x -\u003e 1, {n, 1, z}]   (* A105476 *)",
				"Table[u[n, x] /. x -\u003e -1, {n, 1, z}] (* [A000045] *)",
				"Table[v[n, x] /. x -\u003e -1, {n, 1, z}] (* [A000045] *)"
			],
			"xref": [
				"Cf. A210789, A208510."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Clark Kimberling_, Mar 26 2012",
			"references": 3,
			"revision": 17,
			"time": "2020-01-27T01:33:38-05:00",
			"created": "2012-03-27T21:08:09-04:00"
		}
	]
}