{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A336280",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 336280,
			"data": "1,1,2,1,4,3,2,3,2,3,1,5,6,2,6,3,3,3,1,6,3,5,5,7,5,5,6,7,4,7,5,10,10,4,4,6,10,3,4,12,11,5,7,8,7,8,11,4,4,4,14,7,11,7,13,11,13,7,18,18,6,7,17,12,9,7,9,14,12,9,16,14,11,13,10",
			"name": "Number of consecutive primes of the form k*prime(n) + 1, starting with the least such prime A035095(n), that divides the Wendt determinant A048954(prime(n)).",
			"comment": [
				"Michael B Rees has conjectured that:",
				"1. for every prime p, the Wendt determinant Wendt(p) has all its prime factors that are greater than p of the form k*p + 1.",
				"2. for every prime p = prime(n) and its corresponding Wendt determinant W(p) there exists a finite number of m consecutive primes (p_1,p_2,..,p_m) of the form  k*p + 1 that will divide Wendt(p) where p_1 is always the least prime of the form k*p + 1.",
				"This sequence gives the value m for each p = prime(n)."
			],
			"link": [
				"Gerard P. Michon, \u003ca href=\"http://www.numericana.com/data/wendt.htm\"\u003eFactorization of Wendt's Determinant\u003c/a\u003e (table for n=1 to 114).",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"https://mathworld.wolfram.com/CirculantMatrix.html\"\u003eCirculant matrix \u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Circulant_matrix\"\u003eCirculant matrix \u003c/a\u003e."
			],
			"example": [
				"a(6) = 3 gives p = prime(6) = 13 and W(13) = 3^6*53^2*79^2*131^2*521^2*8191. The sequence of primes of the form q = k*13 + 1, starting with the least such prime 53 that divide W(11) is (53, 79, 131). The sequence has 3 terms."
			],
			"mathematica": [
				"w[n_] := Module[{x}, Resultant[x^n-1, (1+x)^n-1, x]]; k[n_, m_] := Module[{p=Prime@n, q=0, lst={}}, Do[q++; While[! PrimeQ[p*q+1], q++]; AppendTo[lst, q], {m}]; lst];",
				"lst1 = {}; Do[lst=k[n, 50]*Prime[n]+1; m = 1; Do[If[IntegerQ[w[Prime[n]]/lst[[m]]]\u0026\u0026m\u003c=Length@lst, m++, Break[]], {Length@lst}]; AppendTo[lst1, m-1], {n, 1, 75}]; lst1"
			],
			"xref": [
				"Cf. A035095, A048954."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Frank M Jackson_ and Michael B Rees, Jul 15 2020",
			"references": 1,
			"revision": 11,
			"time": "2020-07-25T10:41:01-04:00",
			"created": "2020-07-25T10:41:01-04:00"
		}
	]
}