{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A122407",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 122407,
			"data": "1,-3,-2,12,-4,-9,6,-12,8,3,4,12,-16,33,-24,-60,7,27,8,24,44,-24,18,-36,-34,-48,-12,84,-40,9,24,48,-33,21,-16,0,72,-111,-6,36,50,-96,-8,-120,8,111,-24,96,-16,195,32,-132,-76,48,-66,24,-54,-123,48,-144,-32,102,120,-24,176,117,-14,-12,-28,-192,-54",
			"name": "Expansion of q^(-2/3) * b(q) * b(q^2) * c(q^2) / 3 in powers of q where b(), c() are cubic AGM theta functions.",
			"comment": [
				"Cubic AGM theta functions: a(q) (see A004016), b(q) (A005928), c(q) (A005882)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A122407/b122407.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"N. Elkies, \u003ca href=\"http://mathoverflow.net/questions/84443/\"\u003eComplexity of computing expansion of a newform level 18 weight 3 and character [3] - OEIS A116418\u003c/a\u003e, MathOverflow.",
				"LMFDB, \u003ca href=\"http://www.lmfdb.org/ModularForm/GL2/Q/holomorphic/18/3/17/a/\"\u003eNewform 18.3.17.a\u003c/a\u003e."
			],
			"formula": [
				"Expansion of q^(-2/3) * eta(q)^3 * eta(q^2)^2 * eta(q^6)^2 / eta(q^3) in powers of q.",
				"Euler transform of period 6 sequence [-3, -5, -2, -5, -3, -6, ...].",
				"Expansion of a newform level 18 weight 3 and character chi_18(17,.). - _Michael Somos_, Jan 08 2017",
				"a(n) = sqrt(-2) * b(3*n + 2) where b() is multiplicative with b(p^e) = b(p) * b(p^(e-1)) - Kronecker(-12, p) * p^2 * b(p^(e-2)). - _Michael Somos_, Jan 08 2017",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (18 t)) = 972^(1/2) (t/i)^3 g(t) where q = exp(2 Pi i t) and g() is the g.f. for A208385.",
				"G.f.: Product_{k\u003e0} (1 - x^k)^6 * (1 + x^k)^4 * (1 - x^k + x^(2*k))^2  *(1 + x^k + x^(2*k)).",
				"a(n) = A208384(3*n + 2). -2 * a(n) = A208385(3*n + 2).",
				"-2 * a(n) = A116418(2*n + 1) = a(4*n + 2). a(2*n) = A116418(n). - _Michael Somos_, Jan 08 2017"
			],
			"example": [
				"G.f. = 1 - 3*x - 2*x^2 + 12*x^3 - 4*x^4 - 9*x^5 + 6*x^6 - 12*x^7 + 8*x^8 + ...",
				"G.f. = q^2 - 3*q^5 - 2*q^8 + 12*q^11 - 4*q^14 - 9*q^17 + 6*q^20 - 12*q^23 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ x]^3 QPochhammer[ x^2]^2 QPochhammer[ x^6]^2 / QPochhammer[ x^3], {x, 0, n}]; (* _Michael Somos_, Jan 08 2017 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A)^3 * eta(x^2 + A)^2 * eta(x^6 + A)^2 / eta(x^3 + A), n))};",
				"(MAGMA) A := Basis( CuspForms( Gamma1(18), 3), 211); A[2] - 3*A[5] - 2*A[8]; /* _Michael Somos_, Jan 08 2017 */"
			],
			"xref": [
				"Cf. A116418, A208384, A208385."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Sep 02 2006",
			"references": 4,
			"revision": 19,
			"time": "2018-09-06T02:45:43-04:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}