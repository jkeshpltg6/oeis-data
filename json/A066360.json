{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A066360",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 66360,
			"data": "0,0,1,0,1,0,1,1,1,0,2,0,1,1,2,1,2,0,2,1,2,0,3,2,1,2,2,0,3,0,3,2,2,1,4,1,1,2,4,2,4,0,2,2,2,1,5,2,2,2,4,1,3,2,4,4,2,0,6,0,3,3,4,2,4,2,2,3,4,0,7,2,2,4,4,2,4,0,5,4,3,1,6,2,2,4,6,2,6,2,4,2,2,3,8,4,2,3,4,1",
			"name": "Number of unordered solutions in positive integers of xy + xz + yz = n with gcd(x,y,z) = 1.",
			"comment": [
				"These correspond to Descartes quadruples (-s, s+x+y, s+x+z, s+y+z) where s = sqrt(n), which are primitive if n is a perfect square.",
				"Many empirical regularities are known, e.g., for n = 2^(2k) or n=2^(2k-1), (2 \u003c= k \u003c= 10 and even k \u003c= 20), a(n) = 2^(k-2).",
				"It appears that a(n) \u003e 0 for n \u003e 462. An upper bound on the number of solutions appears to be 1.5*sqrt(n). - _T. D. Noe_, Jun 14 2006"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A066360/b066360.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"a(81) = 3 because we have the triples (x,y,z) = (1,1,40),(2,3,15),(3,6,7) (and not (3,3,12) because this is not primitive)."
			],
			"mathematica": [
				"Table[cnt=0; Do[z=(n-x*y)/(x+y); If[IntegerQ[z] \u0026\u0026 GCD[x,y,z]==1, cnt++ ], {x,Sqrt[n/3]}, {y,x,Sqrt[x^2+n]-x}]; cnt, {n,100}] (* _T. D. Noe_, Jun 14 2006 *)"
			],
			"program": [
				"(Haskell)",
				"a066360 n = length [(x,y,z) | x \u003c- [1 .. a000196 n],",
				"                              y \u003c- [x .. div n x],",
				"                              z \u003c- [y .. n - x*y],",
				"                              x*y+(x+y)*z == n, gcd (gcd x y) z == 1]",
				"-- _Reinhard Zumkeller_, Mar 23 2012"
			],
			"xref": [
				"Cf. A060790, A062536 (and A007875 for xy = n).",
				"Cf. A000196, A066955."
			],
			"keyword": "nonn,nice",
			"offset": "1,11",
			"author": "_Colin Mallows_, Dec 20 2001",
			"ext": [
				"Corrected and extended by _T. D. Noe_, Jun 14 2006"
			],
			"references": 3,
			"revision": 22,
			"time": "2019-10-19T04:10:52-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}