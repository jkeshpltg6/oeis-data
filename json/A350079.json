{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A350079",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 350079,
			"data": "1,1,3,1,17,1,9,142,19,27,68,1569,201,135,510,710,21576,2921,3465,2890,6390,9414,355081,50233,63630,20230,84490,98847,151032,6805296,1004599,1196181,918680,705740,1493688,1812384,2840648,148869153,22872097,26904339,23943752,6351660,28072548,30810528,38348748,61247664",
			"name": "Triangle read by rows: T(n,k) is the number of endofunctions on [n] whose second-smallest component has size exactly k; n \u003e= 0, 0 \u003c= k \u003c= max(0,n-1).",
			"comment": [
				"An endofunction on [n] is a function from {1,2,...,n} to {1,2,...,n}.",
				"If the mapping has no second component, then its second-smallest component is defined to have size 0."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A350079/b350079.txt\"\u003eRows n = 0..141, flattened\u003c/a\u003e"
			],
			"example": [
				"Triangle begins:",
				"       1;",
				"       1;",
				"       3,     1;",
				"      17,     1,     9;",
				"     142,    19,    27,    68;",
				"    1569,   201,   135,   510,   710;",
				"   21576,  2921,  3465,  2890,  6390,  9414;",
				"  355081, 50233, 63630, 20230, 84490, 98847, 151032;",
				"  ..."
			],
			"maple": [
				"g:= proc(n) option remember; add(n^(n-j)*(n-1)!/(n-j)!, j=1..n) end:",
				"b:= proc(n, l) option remember; `if`(n=0, x^subs(infinity=0, l)[2],",
				"      add(b(n-i, sort([l[], i])[1..2])*g(i)*binomial(n-1, i-1), i=1..n))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=0..degree(p)))(b(n, [infinity$2])):",
				"seq(T(n), n=0..12);  # _Alois P. Heinz_, Dec 17 2021"
			],
			"mathematica": [
				"g[n_] := g[n] = Sum[n^(n - j)*(n - 1)!/(n - j)!, {j, 1, n}];",
				"b[n_, l_] := b[n, l] = If[n == 0, x^(l /. Infinity -\u003e 0)[[2]], Sum[b[n - i, Sort[Append[l, i]][[1;;2]]]*g[i]*Binomial[n - 1, i - 1], {i, 1, n}]];",
				"T[n_] := With[{p = b[n, {Infinity, Infinity}]}, Table[Coefficient[p, x, i], {i, 0, Exponent[p, x]}]];",
				"Table[T[n], {n, 0, 12}] // Flatten (* _Jean-François Alcover_, Dec 28 2021, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Column 0 gives gives 1 together with A001865.",
				"Row sums give A000312."
			],
			"keyword": "nonn,tabf",
			"offset": "0,3",
			"author": "_Steven Finch_, Dec 12 2021",
			"ext": [
				"More terms (two rows) from _Alois P. Heinz_, Dec 15 2021"
			],
			"references": 1,
			"revision": 29,
			"time": "2021-12-28T14:11:30-05:00",
			"created": "2021-12-12T20:23:50-05:00"
		}
	]
}