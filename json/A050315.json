{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A050315",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 50315,
			"data": "1,1,1,2,1,2,2,5,1,2,2,5,2,5,5,15,1,2,2,5,2,5,5,15,2,5,5,15,5,15,15,52,1,2,2,5,2,5,5,15,2,5,5,15,5,15,15,52,2,5,5,15,5,15,15,52,5,15,15,52,15,52,52,203,1,2,2,5,2,5,5,15,2,5,5,15,5,15,15,52,2,5,5,15,5,15",
			"name": "Main diagonal of A050314.",
			"comment": [
				"Also, a(n) is the number of odd multinomial coefficients n!/(k_1!...k_m!) with 1 \u003c= k_1 \u003c= ... \u003c= k_m and k_1 + ... + k_m = n. - _Pontus von Brömssen_, Mar 23 2018",
				"From _Gus Wiseman_, Mar 30 2019: (Start)",
				"Also the number of strict integer partitions of n with no binary carries. The Heinz numbers of these partitions are given by A325100. A binary carry of two positive integers is an overlap of the positions of 1's in their reversed binary expansion. For example, the a(1) = 1 through a(15) = 15 strict integer partitions with no binary carries are:",
				"(1) (2) (3)  (4) (5)  (6)  (7)   (8) (9)  (A)  (B)   (C)  (D)   (E)   (F)",
				"        (21)     (41) (42) (43)      (81) (82) (83)  (84) (85)  (86)  (87)",
				"                           (52)                (92)       (94)  (A4)  (96)",
				"                           (61)                (A1)       (C1)  (C2)  (A5)",
				"                           (421)               (821)      (841) (842) (B4)",
				"                                                                      (C3)",
				"                                                                      (D2)",
				"                                                                      (E1)",
				"                                                                      (843)",
				"                                                                      (852)",
				"                                                                      (861)",
				"                                                                      (942)",
				"                                                                      (A41)",
				"                                                                      (C21)",
				"                                                                      (8421)",
				"(End)"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A050315/b050315.txt\"\u003eTable of n, a(n) for n = 0..16383\u003c/a\u003e",
				"Michael Gilleland, \u003ca href=\"/selfsimilar.html\"\u003eSome Self-Similar Integer Sequences\u003c/a\u003e"
			],
			"formula": [
				"Bell number of number of 1's in binary: a(n) = A000110(A000120(n))."
			],
			"maple": [
				"a:= n-\u003e combinat[bell](add(i,i=convert(n, base, 2))):",
				"seq(a(n), n=0..100);  # _Alois P. Heinz_, Apr 08 2019"
			],
			"mathematica": [
				"binpos[n_]:=Join@@Position[Reverse[IntegerDigits[n,2]],1];",
				"stableQ[u_,Q_]:=!Apply[Or,Outer[#1=!=#2\u0026\u0026Q[#1,#2]\u0026,u,u,1],{0,1}];",
				"Table[Length[Select[IntegerPartitions[n],UnsameQ@@#\u0026\u0026stableQ[#,Intersection[binpos[#1],binpos[#2]]!={}\u0026]\u0026]],{n,0,20}] (* _Gus Wiseman_, Mar 30 2019 *)",
				"a[n_] := BellB[DigitCount[n, 2, 1]];",
				"a /@ Range[0, 100] (* _Jean-François Alcover_, May 21 2021 *)"
			],
			"xref": [
				"Cf. A000110, A000120, A050314.",
				"Cf. A070939, A080572, A247935, A267610.",
				"Cf. A325093, A325095, A325096, A325099, A325100, A325103, A325110, A325123.",
				"Main diagonal of A307431 and of A307505."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_Christian G. Bower_, Sep 15 1999",
			"references": 23,
			"revision": 25,
			"time": "2021-05-21T08:11:19-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}