{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A349550",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 349550,
			"data": "1,2,1,3,3,2,5,4,3,1,8,7,5,4,2,13,11,8,5,4,3,21,18,13,9,6,4,1,34,29,21,14,10,7,5,2,55,47,34,23,16,11,6,5,3,89,76,55,37,26,18,11,7,5,4,144,123,89,60,42,29,17,12,8,5,1,233,199,144,97,68,47",
			"name": "Meta-Wythoff array based on A097285: M = (M(n,k)), by downward antidiagonals; every row of M is eventually a row of the Wythoff array, W = A035513, and every row of W is a row of M; see Comments.",
			"comment": [
				"Suppose that (s(1), s(2), ...) is a sequence satisfying s(k) = s(k-1) + s(k-2) for k \u003e= 3.  If s(1) and s(2) are positive integers, then there is an index n such that (s(n), s(n+1), ...) is a row of A035513. The n-th row of M is the sequence (s(1), s(2), ...), where (s(1), s(2)) are the n-th pair described in A097285.",
				"Every row of W is a row of M; indeed, M consists of all tails of all rows of W."
			],
			"example": [
				"Corner:",
				"  1, 2, 3,  5,  8, 13, 21, 34,  55,  89, 144, 233",
				"  1, 3, 4,  7, 11, 18, 29, 47,  76, 123, 199, 322",
				"  2, 3, 5,  8, 13, 21, 34, 55,  89, 144, 233, 377",
				"  1, 4, 5,  9, 14, 23, 37, 60,  97, 157, 254, 411",
				"  2, 4, 6, 10, 16, 26, 42, 68, 110, 178, 288, 466",
				"  3, 4, 7, 11, 18, 29, 47, 76, 123, 199, 322, 521",
				"  1, 5, 6, 11, 17, 28, 45, 73, 118, 191, 309, 500",
				"  2, 5, 7, 12, 19, 31, 50, 81, 131, 212, 343, 555",
				"  3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610",
				"  4, 5, 9, 14, 23, 37, 60, 97, 157, 254, 411, 665",
				"Example: The first 7 pairs in A097285 are (1,2), (1,3), (2,3), (1,4), (2,4), (3,4), (1,5), so that the first 7 rows of M are",
				"(1,2,3,5,8,...) = (row 1 of W) = Fibonacci numbers, A000045;",
				"(1,3 4,7,11,...), which includes row 2 of W, the Lucas numbers, A000032;",
				"(2,3,5,8,13,...), a tail of row 1 of W;",
				"(1,4,5,9,14,...), which includes row 4 of W;",
				"(2,4,6,10,16,...), which includes row 3 of W;",
				"(3,4,7,11,18,...), which includes row 2 of W;",
				"(1,5,6,11,17,...), which includes row 7 of W."
			],
			"mathematica": [
				"z1 = 30; zc = 20; zr = 20;",
				"t1 = {1, 2}; Do[t1 = Join[t1, Riffle[Range[n - 1], n], {n}], {n, 3, z1}]; (* A097285 *)",
				"t = Partition[t1, 2];",
				"f[n_] := Fibonacci[n]; r = (1 + Sqrt[5])/2;",
				"s[h_, k_] := Table[h*f[n - 1] + k*f[n], {n, 2, zc}];",
				"w = Table[Join[{h = t[[n]][[1]], k = t[[n]][[2]]}, s[h, k]], {n, 1, zr}]",
				"TableForm[w]  (* A349550 array *)",
				"w1[n_, k_] := w[[n]][[k]];",
				"Table[w1[n - k + 1, k], {n, 13}, {k, n, 1, -1}] // Flatten (* A349550 sequence *)"
			],
			"xref": [
				"Cf. A000032, A000045, A035513, A097285, A349551, A349552, A349553, A349554."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Nov 21 2021",
			"references": 1,
			"revision": 8,
			"time": "2021-12-15T02:28:32-05:00",
			"created": "2021-12-15T02:28:32-05:00"
		}
	]
}