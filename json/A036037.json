{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A036037",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 36037,
			"data": "1,2,1,1,3,2,1,1,1,1,4,3,1,2,2,2,1,1,1,1,1,1,5,4,1,3,2,3,1,1,2,2,1,2,1,1,1,1,1,1,1,1,6,5,1,4,2,3,3,4,1,1,3,2,1,2,2,2,3,1,1,1,2,2,1,1,2,1,1,1,1,1,1,1,1,1,1,7,6,1,5,2,4,3,5,1,1,4,2,1,3,3,1,3,2,2,4,1,1",
			"name": "Triangle read by rows in which row n lists all the parts of all the partitions of n, sorted first by length and then colexicographically.",
			"comment": [
				"First differs from A334439 for partitions of 9. Namely, this sequence has (4,4,1) before (5,2,2), while A334439 has (5,2,2) before (4,4,1). - _Gus Wiseman_, May 08 2020",
				"This is also a list of all the possible prime signatures of a number, arranged in graded colexicographic ordering. - _N. J. A. Sloane_, Feb 09 2014",
				"This is also the Abramowitz-Stegun ordering of reversed partitions (A036036) if the partitions are reversed again after sorting. Partitions sorted first by sum and then colexicographically are A211992. - _Gus Wiseman_, May 08 2020"
			],
			"link": [
				"Robert Price, \u003ca href=\"/A036037/b036037.txt\"\u003eTable of n, a(n) for n = 1..3615, 15 rows.\u003c/a\u003e",
				"Wikiversity, \u003ca href=\"https://en.wikiversity.org/wiki/Lexicographic_and_colexicographic_order\"\u003eLexicographic and colexicographic order\u003c/a\u003e"
			],
			"example": [
				"First five rows are:",
				"{{1}}",
				"{{2}, {1, 1}}",
				"{{3}, {2, 1}, {1, 1, 1}}",
				"{{4}, {3, 1}, {2, 2}, {2, 1, 1}, {1, 1, 1, 1}}",
				"{{5}, {4, 1}, {3, 2}, {3, 1, 1}, {2, 2, 1}, {2, 1, 1, 1}, {1, 1, 1, 1, 1}}",
				"Up to the fifth row, this is exactly the same as the reverse lexicographic ordering A080577. The first row which differs is the sixth one, which reads ((6), (5,1), (4,2), (3,3), (4,1,1), (3,2,1), (2,2,2), (3,1,1,1), (2,2,1,1), (2,1,1,1,1), (1,1,1,1,1,1)). - _M. F. Hasler_, Jan 23 2020",
				"From _Gus Wiseman_, May 08 2020: (Start)",
				"The sequence of all partitions begins:",
				"  ()         (3,2)        (2,1,1,1,1)",
				"  (1)        (3,1,1)      (1,1,1,1,1,1)",
				"  (2)        (2,2,1)      (7)",
				"  (1,1)      (2,1,1,1)    (6,1)",
				"  (3)        (1,1,1,1,1)  (5,2)",
				"  (2,1)      (6)          (4,3)",
				"  (1,1,1)    (5,1)        (5,1,1)",
				"  (4)        (4,2)        (4,2,1)",
				"  (3,1)      (3,3)        (3,3,1)",
				"  (2,2)      (4,1,1)      (3,2,2)",
				"  (2,1,1)    (3,2,1)      (4,1,1,1)",
				"  (1,1,1,1)  (2,2,2)      (3,2,1,1)",
				"  (5)        (3,1,1,1)    (2,2,2,1)",
				"  (4,1)      (2,2,1,1)    (3,1,1,1,1)",
				"(End)"
			],
			"mathematica": [
				"Reverse/@Join@@Table[Sort[Reverse/@IntegerPartitions[n]],{n,8}] (* _Gus Wiseman_, May 08 2020 *)",
				"- or -",
				"colen[f_,c_]:=OrderedQ[{Reverse[f],Reverse[c]}];",
				"Join@@Table[Sort[IntegerPartitions[n],colen],{n,8}] (* _Gus Wiseman_, May 08 2020 *)"
			],
			"xref": [
				"See A036036 for the graded reflected colexicographic (\"Abramowitz and Stegun\" or Hindenburg) ordering.",
				"See A080576 for the graded reflected lexicographic (\"Maple\") ordering.",
				"See A080577 for the graded reverse lexicographic (\"Mathematica\") ordering: differs from a(48) on!",
				"See A228100 for the Fenner-Loizou (binary tree) ordering.",
				"See also A036038, A036039, A036040: (multinomial coefficients).",
				"Partition lengths are A036043.",
				"Reversing all partitions gives A036036.",
				"The number of distinct parts is A103921.",
				"Taking Heinz numbers gives A185974.",
				"The version ignoring length is A211992.",
				"The version for revlex instead of colex is A334439.",
				"Lexicographically ordered reversed partitions are A026791.",
				"Reverse-lexicographically ordered partitions are A080577.",
				"Sorting partitions by Heinz number gives A296150.",
				"Cf. A000041, A124734, A193073, A228100, A228531, A296774, A334301, A334433, A334436, A334437, A334442."
			],
			"keyword": "nonn,easy,tabf",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Name corrected by _Gus Wiseman_, May 12 2020",
				"Mathematica programs corrected to reflect offset of one and not zero by _Robert Price_, Jun 04 2020"
			],
			"references": 61,
			"revision": 54,
			"time": "2020-06-05T08:21:23-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}