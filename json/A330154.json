{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A330154",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 330154,
			"data": "1,4,6,8,2,9,3,10,12,5,14,15,16,7,18,20,21,22,11,24,25,26,13,27,28,30,32,17,33,34,35,19,36,38,39,40,23,42,44,45,46,48,49,50,29,51,52,31,54,55,56,57,58,60,62,37,63,64,65,66,41,68,69,70,43,72,74,75,76,47,77",
			"name": "Each term is visited according to the rule: move left (resp. right) a(n) places if a(n) is prime (resp. not prime), starting at n = 1: lexicographically earliest such sequence of distinct positive integers.",
			"comment": [
				"Inspired by A329423, which is constructed \"following the journey\" and not lexicographically earliest.",
				"Conjectured to be a permutation of the positive integers: see Example for computational results.",
				"\"Each term is visited\" means here more precisely that each term has a successor and each term a(n) \u003e 1 has a \"predecessor\" a(m) such that m + a(m)*(-1)^isprime(a(m)) = n (see examples), and there are no loops. The trajectory from a(1) to, e.g., a(3) is not yet known.",
				"However, for given N we can construct a sequence which equals this sequence for the first N terms, and then deviates from minimality in order to make an effort to \"connect\" the trajectories which are so far not yet connected. This can be achieved using the method outlined in A329423: following the trajectory of 1, once beyond index N, one always chooses the largest possible prime in order to drive the journey back to small numbers; if no prime is possible one uses the smallest possible composite. We have verified for several N that we get indeed a path from a(1) = 1 through a(7) = 3 -\u003e a(4) = 8 and a(5) = 2 -\u003e a(3) = 6 etc., see examples for more. The current sequence can be seen as the limit of all these manifestly \"connected\" sequences, as N -\u003e oo."
			],
			"example": [
				"a(1) = 1 is the smallest available choice at that point and does not lead to a contradiction. It means that after this term, the term at index 1 + 1 = 2 must be visited.",
				"a(2) cannot be a prime since these are \u003e= 2 and would \"point\" to a nonpositive index. The smallest available choice is a(2) = 4, which means that after this term, the term at index 2 + 4 = 6 must be visited.",
				"a(3) also cannot be prime, because 2 would lead back to 3 - 2 = 1 and give a loop, and other primes are too large to specify a valid step to the left. Thus a(3) = 6 is the smallest possible choice, leading to index 3 + 6 = 9 after this term is visited.",
				"Similarly a(4) = 8, leading to index 4 + 8 = 12 after this term.",
				"Then a(5) can be equal to the smallest available number, 2, leading to index 5 - 2 = 3 after this term is visited.",
				"Therefore a(6) cannot be 3, which would lead to 6 - 3 = 3, but a(3) already has a(5) as predecessor. Larger primes aren't possible either, so the smallest possible choice is a(6) = 9, leading to 6 + 9 = 15.",
				"And so on.",
				"After 1000 terms, the smallest unused number is the prime 787, and the earliest term which does not yet have a predecessor is a(226) = 238.",
				"After 2000 terms, the smallest unused number is the prime 1583, and the earliest term which does not yet have a predecessor is a(420).",
				"After 10^4 terms, the smallest unused number is the prime 8219, and the earliest term which does not yet have a predecessor is a(1784).",
				"It appears that these limits increase roughly linearly, which justifies the conjecture that all numbers will eventually appear and have a predecessor.",
				"The trajectories involving the first few terms are:",
				"a(1)=1 -\u003e a(2)=4 -\u003e a(6)=9 -\u003e a(15)=18 -\u003e a(33)=36 -\u003e a(69)=76 -\u003e a(145)=103 -\u003e a(42)=48 -\u003e a(90)=96 -\u003e a(186)=200 -\u003e a(386)=404 -\u003e a(790)=820 -\u003e a(1610)=1666 -\u003e a(3276)=3369 -\u003e a(6645)=6807 -\u003e ...",
				"... -\u003e a(6461)=5261 -\u003e a(1200)=937 -\u003e a(263)=193 -\u003e a(70)=47 -\u003e a(23)=13 -\u003e",
				"  a(10)=5 -\u003e a(5)=2 -\u003e a(3)=6 -\u003e a(9)=12 -\u003e a(21)=25 -\u003e a(46)=51 -\u003e a(97)=67 -\u003e",
				"  a(30)=34 -\u003e a(64)=70 -\u003e a(134)=144 -\u003e a(278)=294 -\u003e a(572)=596 -\u003e",
				"  a(1168)=1210 -\u003e a(2378)=2451 -\u003e a(4829)=3907 -\u003e a(922)=957 -\u003e a(1879)=1939 -\u003e",
				"  a(3818)=3922 -\u003e a(7740)=7917 -\u003e ...",
				"... -\u003e a(4286)=3461 -\u003e a(825)=858 -\u003e a(1683)=1737 -\u003e a(3420)=2741 -\u003e a(679)=708 -\u003e",
				"  a(1387)=1087 -\u003e a(300)=223 -\u003e a(77)=53 -\u003e a(24)=27 -\u003e a(51)=56 -\u003e",
				"  a(107)=116 -\u003e a(223)=163 -\u003e a(60)=66 -\u003e a(126)=89 -\u003e a(37)=23 -\u003e a(14)=7 -\u003e",
				"  a(7)=3 -\u003e a(4)=8 -\u003e a(12)=15 -\u003e a(27)=32 -\u003e a(59)=65 -\u003e a(124)=133 -\u003e",
				"  a(257)=273 -\u003e a(530)=552 -\u003e a(1082)=1124 -\u003e a(2206)=2271 -\u003e a(4477)=4593 -\u003e",
				"  a(9070)=9275 -\u003e ...",
				"We can modify the sequence from a certain index on, in order connect the trajectories through a(3) = 6 and a(4) = 8 earlier. For example, one variant which has the same terms up to a(10) but connects all these quite early yields (breaking lines before \"local minima\"):",
				"a(1)=1 -\u003e a(2)=4 -\u003e a(6)=9 -\u003e a(15)=7 -\u003e a(8)=10 -\u003e a(18)=11 -\u003e",
				"  a(7)=3 -\u003e a(4)=8 -\u003e a(12)=14 -\u003e a(26)=13 -\u003e a(13)=15 -\u003e a(28)=17 -\u003e",
				"  a(11)=16 -\u003e a(27)=18 -\u003e a(45)=31 -\u003e",
				"  a(14)=20 -\u003e a(34)=21 -\u003e a(55)=23 -\u003e a(32)=22 -\u003e a(54)=37 -\u003e a(17)=24 -\u003e",
				"  a(41)=19 -\u003e a(22)=25 -\u003e a(47)=26 -\u003e a(73)=53 -\u003e",
				"  a(20)=28 -\u003e a(48)=29 -\u003e a(19)=27 -\u003e a(46)=30 -\u003e a(76)=47 -\u003e",
				"  a(29)=32 -\u003e a(61)=33 -\u003e a(94)=71 -\u003e a(23)=34 -\u003e a(57)=41 -\u003e",
				"  a(16)=35 -\u003e a(51)=36 -\u003e a(87)=43 -\u003e a(44)=38 -\u003e a(82)=39 -\u003e a(121)=97 -\u003e",
				"  a(24)=40 -\u003e a(64)=42 -\u003e a(106)=73 -\u003e a(33)=44 -\u003e a(77)=67 -\u003e a(10)=5 -\u003e",
				"  a(5)=2 -\u003e a(3)=6 -\u003e a(9)=12 -\u003e a(21)=45 -\u003e ..."
			],
			"program": [
				"(PARI) A330154_upto(N,U=[0],V=[1])={vector(N, n, my(t); for(k=U[1]+1,oo, setsearch(U,k) || setsearch(V,t=n+(-1)^isprime(k)*k) || t \u003c= V[1] || break); if(V[1]+1\u003ct, V=setunion(V,[t]), V[1]=t; while(#V\u003e1 \u0026\u0026 V[2]==V[1]+1,V=V[^1])); if(U[1]+1\u003ct=abs(t-n), U=setunion(U,[t]), U[1]=t; while(#U\u003e1 \u0026\u0026 U[2]==U[1]+1,U=U[^1])); t)}"
			],
			"xref": [
				"Cf. A329423."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_M. F. Hasler_, Dec 03 2019",
			"references": 1,
			"revision": 21,
			"time": "2019-12-06T23:31:47-05:00",
			"created": "2019-12-05T17:24:19-05:00"
		}
	]
}