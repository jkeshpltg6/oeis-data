{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A019952",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 19952,
			"data": "1,3,7,6,3,8,1,9,2,0,4,7,1,1,7,3,5,3,8,2,0,7,2,0,9,5,8,1,9,1,0,8,8,7,6,7,9,5,2,5,8,9,9,3,3,6,0,0,8,1,5,8,6,6,3,3,6,5,6,7,5,7,6,5,6,1,9,0,9,5,1,9,3,7,6,7,1,7,2,9,8,5,0,6,5,9,5,2,9,9,3,1,1,0,0,7,0,1,9",
			"name": "Decimal expansion of tangent of 54 degrees.",
			"comment": [
				"Also the decimal expansion of cotangent of 36 degrees. - _Mohammad K. Azarian_, Jun 30 2013",
				"A quartic number with denominator 5. - _Charles R Greathouse IV_, Aug 27 2017",
				"Conjecture: Product (2/3) * (8/7) * (12/13) * (18/17) * (22/23) * (32/33) * ... * (a_n/b_n) = sqrt(25 + 10*sqrt(5))/5 = tan(3*Pi/10) = A019952, where a_n even, a_n + b_n = a(n), |a_n - b_n| = 1, n \u003e= 0. - _Dimitris Valianatos_, Feb 14 2020",
				"Also the limiting value of the distance between the lines F(n)*x + F(n+1)*y = 0 and F(n)*x + F(n+1)*y = F(n+2) (where F(n)=A000045(n) are the Fibonacci numbers and n\u003e0). - _Burak Muslu_, Apr 03 2021",
				"Decimal expansion of the radius of an inscribed sphere in a rhombic triacontahedron with unit edge length. - _Wesley Ivan Hurt_, May 11 2021"
			],
			"link": [
				"Ivan Panchenko, \u003ca href=\"/A019952/b019952.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Exact_trigonometric_constants\"\u003eExact trigonometric constants\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Rhombic_triacontahedron\"\u003eRhombic triacontahedron\u003c/a\u003e"
			],
			"formula": [
				"Equals A019863/A019845 = 1/A019934. - _R. J. Mathar_, Jul 26 2010",
				"The largest positive solution of cos(4*arctan(1/x)) = cos(6*arctan(1/x)). - _Thomas Olson_, Oct 03 2014",
				"Equals sqrt(25 + 10*sqrt(5))/5. - _G. C. Greubel_, Nov 22 2018",
				"Equals sqrt(2 + sqrt(5))/5^(1/4). - _Burak Muslu_, Apr 03 2021",
				"From _Wesley Ivan Hurt_, May 11 2021: (Start)",
				"Equals phi^2/sqrt(1+phi^2) where phi is the golden ratio.",
				"Equals sqrt(1+2/sqrt(5)). (End)"
			],
			"example": [
				"1.376381920471173538..."
			],
			"maple": [
				"Digits:=100: evalf(tan(3*Pi/10)); # _Wesley Ivan Hurt_, Oct 07 2014"
			],
			"mathematica": [
				"RealDigits[Tan[3*Pi/10], 10, 100][[1]] (* _Wesley Ivan Hurt_, Oct 07 2014 *)",
				"RealDigits[Tan[54 Degree],10,120][[1]] (* _Harvey P. Dale_, Jul 16 2016 *)"
			],
			"program": [
				"(PARI) tan(3*Pi/10) \\\\ _Charles R Greathouse IV_, Aug 27 2017",
				"(MAGMA) SetDefaultRealField(RealField(100)); R:= RealField(); Tan(3*Pi(R)/10); // _G. C. Greubel_, Nov 22 2018",
				"(Sage) numerical_approx(tan(3*pi/10), digits=100) # _G. C. Greubel_, Nov 22 2018",
				"(Python)",
				"from sympy import sqrt",
				"[print(i, end=', ') for i in str(sqrt(1+2/sqrt(5)).n(110)) if i!='.'] # _Karl V. Keller, Jr._, Jun 19 2020"
			],
			"xref": [
				"Cf. A019845, A019863, A019934.",
				"Cf. A344171 (rhombic triacontahedron surface area).",
				"Cf. A344172 (rhombic triacontahedron volume).",
				"Cf. A344212 (rhombic triacontahedron midradius)."
			],
			"keyword": "nonn,cons",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"references": 8,
			"revision": 60,
			"time": "2021-05-11T18:21:01-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}