{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A182632",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 182632,
			"data": "0,3,9,21,33,45,69,105,129,141,165,213,273,321,381,465,513,525,549,597,657,717,801,933,1065,1137,1197,1317,1485,1629,1785,1977,2073,2085,2109,2157,2217,2277,2361,2493,2625,2709,2793,2949,3177,3405,3633",
			"name": "Toothpick sequence on the hexagonal net starting from a node.",
			"comment": [
				"A connected network of toothpicks is constructed by the following iterative procedure. At stage 1, place three toothpicks each of length 1 on a hexagonal net, as a propeller, joined at a node. At each subsequent stage, add two toothpicks (which could be called a single V-toothpick with a 120 degree corner) adjacent to each node which is the endpoint of a single toothpick.",
				"The exposed endpoints of the toothpicks of the old generation are touched by the endpoints of the toothpicks of the new generation. In the graph, the edges of the hexagons become edges of the graph, and the graph grows such that the nodes that were 1-connected in the old generation are 3-connected in the new generation.",
				"It turns out heuristically that this growth does not show frustration, ie., a free edge is never claimed by two adjacent exposed endpoints at the same stage; the rule of growing the network does apparently not need specifications to address such cases.",
				"The sequence gives the number of toothpicks in the toothpick structure after n-th stage. A182633 (the first differences) gives the number of toothpicks added at n-th stage.",
				"a(n) is also the number of components after n-th stage in a toothpick structure starting with a single Y-toothpick in stage 1 and adding only V-toothpicks in stages \u003e=2. For example: consider that in A161644 a V-toothpick is also a polytoothpick with two components or toothpicks and a Y-toothpick is also a polytoothpick with three components or toothpicks. For more information about this comment see A161206, A160120 and A161644.",
				"Has a behavior similar to A151723, A182840. - _Omar E. Pol_, Mar 07 2013"
			],
			"link": [
				"David Applegate, \u003ca href=\"/A139250/a139250.anim.html\"\u003eThe movie version\u003c/a\u003e",
				"David Applegate, Omar E. Pol and N. J. A. Sloane, \u003ca href=\"/A000695/a000695_1.pdf\"\u003eThe Toothpick Sequence and Other Sequences from Cellular Automata\u003c/a\u003e, Congressus Numerantium, Vol. 206 (2010), 157-191. [There is a typo in Theorem 6: (13) should read u(n) = 4.3^(wt(n-1)-1) for n \u003e= 2.]",
				"N. J. A. Sloane, \u003ca href=\"/wiki/Catalog_of_Toothpick_and_CA_Sequences_in_OEIS\"\u003eCatalog of Toothpick and Cellular Automata Sequences in the OEIS\u003c/a\u003e",
				"\u003ca href=\"/index/To#toothpick\"\u003eIndex entries for sequences related to toothpick sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 3*A182634(n).",
				"a(n) = 1 + 2*A161644(n), n \u003e= 1. - _Omar E. Pol_, Mar 07 2013"
			],
			"example": [
				"a(0)=0. At stage 1 we place 3 toothpicks connected to the initial grid point of the structure. Note that there are 3 exposed endpoints. At stage 2 we place 6 toothpicks, so a(2)=3+6=9, etc."
			],
			"xref": [
				"Cf. A139250, A160120, A161206, A161644, A182617, A182633, A182634, A182836, A182840."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Omar E. Pol_, Dec 07 2010",
			"references": 14,
			"revision": 27,
			"time": "2021-02-24T02:48:19-05:00",
			"created": "2010-11-22T19:57:28-05:00"
		}
	]
}