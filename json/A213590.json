{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A213590",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 213590,
			"data": "1,5,1,15,6,2,36,20,11,3,76,51,35,17,5,148,112,87,55,28,8,273,224,188,138,90,45,13,485,421,372,300,225,145,73,21,839,758,694,596,488,363,235,118,34,1424,1324,1243,1115,968,788,588,380,191,55,2384,2263,2163,2001,1809,1564,1276,951,615,309,89",
			"name": "Rectangular array:  (row n) = b**c, where b(h) = h^2, c(h) = F(n-1+h), F = A000045 (Fibonacci numbers), n\u003e=1, h\u003e=1, and ** = convolution.",
			"comment": [
				"Principal diagonal: A213504.",
				"Antidiagonal sums: A213557.",
				"Row 1,  (1,4,9,16,...)**(1,1,2,3,5,...): A053808.",
				"Row 2,  (1,4,9,16,...)**(1,2,3,5,8,...): A213586.",
				"Row 3,  (1,4,9,16,...)**(2,3,5,8,13,...).",
				"For a guide to related arrays, see A213500."
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A213590/b213590.txt\"\u003eAntidiagonals n = 1..60, flattened\u003c/a\u003e"
			],
			"formula": [
				"Rows:  T(n,k) = 4*T(n,k-1) -5*T(n,k-2) +*T(n,k-3) +2*T(n,k-4) -T(n,k-5).",
				"Columns:  T(n,k) = T(n-1,k) + T(n-2,k).",
				"G.f. for row n:  f(x)/g(x), where f(x) = F(n) + F(n+1)*x + F(n-1)*x^2 and g(x) = (1 - x - x^2)*(1 - x )^3.",
				"T(n, k) = Fibonacci(n+k+6) - Fibonacci(n+6) - 2*k*Fibonacci(n+3) - k^2*Fibonacci(n+1). - _G. C. Greubel_, Jul 05 2019"
			],
			"example": [
				"Northwest corner (the array is read by falling antidiagonals):",
				"1....5....15....36....76.....148",
				"1....6....20....51....112....224",
				"2....11...35....87....188....372",
				"3....17...55....138...300....596",
				"5....28...90....225...488....868",
				"8....45...145...363...788....1564",
				"13...73...235...588...1276...2532"
			],
			"mathematica": [
				"(* First program *)",
				"b[n_]:= n^2; c[n_]:= Fibonacci[n];",
				"T[n_, k_]:= Sum[b[k-i] c[n+i], {i, 0, k-1}]",
				"TableForm[Table[T[n, k], {n, 1, 10}, {k, 1, 10}]]",
				"Flatten[Table[T[n-k+1, k], {n, 12}, {k, n, 1, -1}]] (* A213590 *)",
				"r[n_]:= Table[T[n, k], {k, 40}]  (* columns of antidiagonal triangle *)",
				"Table[T[n, n], {n, 1, 40}] (* A213504 *)",
				"s[n_]:= Sum[T[i, n+1-i], {i, 1, n}]",
				"Table[s[n], {n, 1, 50}] (* A213557 *)",
				"(* Second program *)",
				"t[n_, k_]:= Fibonacci[n+7] - Fibonacci[k+6] - 2*(n-k+1)*Fibonacci[k+3] - (n-k+1)^2*Fibonacci[k+1]; Table[t[n, k], {n, 1, 12}, {k, 1, n}]//Flatten (* _G. C. Greubel_, Jul 05 2019 *)"
			],
			"program": [
				"(PARI) f=fibonacci; t(n,k) = f(n+7) -f(k+6) -2*(n-k+1)*f(k+3) -(n-k+1)^2 *f(k+1);",
				"for(n=1,12, for(k=1,n, print1(t(n,k), \", \"))) \\\\ _G. C. Greubel_, Jul 05 2019",
				"(MAGMA) F:=Fibonacci; [[F(n+7) -F(k+6) -2*(n-k+1)*F(k+3) -(n-k+1)^2 *F(k+1): k in [1..n]]: n in [1..12]]; // _G. C. Greubel_, Jul 05 2019",
				"(Sage) f=fibonacci; [[f(n+7) -f(k+6) -2*(n-k+1)*f(k+3) - (n-k+1)^2* f(k+1) for k in (1..n)] for n in (1..12)] # _G. C. Greubel_, Jul 05 2019",
				"(GAP) F:=Fibonacci;; Flat(List([1..12],n-\u003e List([1..n],k-\u003e F(n+7)-F(k+6) -2*(n-k+1)*F(k+3)-(n-k+1)^2*F(k+1) ))) # _G. C. Greubel_, Jul 05 2019"
			],
			"xref": [
				"Cf. A213500, A213587."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Jun 19 2012",
			"references": 7,
			"revision": 17,
			"time": "2019-07-06T18:33:16-04:00",
			"created": "2012-06-21T22:27:27-04:00"
		}
	]
}