{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A285296",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 285296,
			"data": "1,4,2,6,3,8,5,9,7,12,10,14,16,11,18,13,20,15,21,24,17,25,19,27,22,26,28,23,32,29,36,30,33,39,40,31,44,34,38,42,35,45,37,48,41,49,43,50,46,52,47,54,51,56,53,60,55,63,57,64,58,62,66,68,59,72,61",
			"name": "Lexicographically earliest sequence of distinct positive terms such that the product of two consecutive terms is divisible by p^2 for some prime p.",
			"comment": [
				"The sequence can always be extended with a number that is not squarefree (say a multiple of 4); after a term that is not squarefree, we can extend the sequence with the least unused number; as there are infinitely many multiples of 4, this sequence is a permutation of the natural numbers (with inverse A285297).",
				"Conjecturally, a(n) ~ n.",
				"This sequence has similarities with A075380: here we consider the product of consecutive terms, there the sum of consecutive terms.",
				"For any k\u003e0, let b_k be the lexicographically earliest sequence of distinct terms such that the product of two consecutive terms is divisible by p^k for some prime p; in particular we have:",
				"- b_1 = A000027 (the natural numbers),",
				"- b_2 = a (this sequence),",
				"- b_3 = A285299,",
				"- b_4 = A285386,",
				"- b_5 = A285417.",
				"For any k\u003e0, b_k is a permutation of the natural numbers.",
				"For any k\u003e0, b_k(1)=1 and b_k(2)=2^k.",
				"Graphically, the sequences from b_2 to b_5 differ."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A285296/b285296.txt\"\u003eTable of n, a(n) for n = 1..2000\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A285296/a285296.gp.txt\"\u003ePARI program for A285296\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"example": [
				"The first terms, alongside the primes p such that p^2 divides a(n)*a(n+1), are:",
				"n       a(n)    p",
				"--      ----    -",
				"1       1       2",
				"2       4       2",
				"3       2       2",
				"4       6       3",
				"5       3       2",
				"6       8       2",
				"7       5       3",
				"8       9       3",
				"9       7       2",
				"10      12      2",
				"11      10      2",
				"12      14      2",
				"13      16      2",
				"14      11      3",
				"15      18      3",
				"16      13      2",
				"17      20      2, 5",
				"18      15      3",
				"19      21      2, 3",
				"20      24      2"
			],
			"xref": [
				"Cf. A000027, A075380, A285297 (inverse)."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Rémy Sigrist_, Apr 16 2017",
			"references": 6,
			"revision": 21,
			"time": "2017-06-14T02:46:49-04:00",
			"created": "2017-04-20T12:33:12-04:00"
		}
	]
}