{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A215829",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 215829,
			"data": "3,-3,27,-99,531,-2403,11691,-55107,263331,-1250883,5957307,-28339875,134882739,-641835171,3054430539,-14535159939,69169849155,-329162695299,1566411248475,-7454188455651,35472778517331,-168806797907427,803312835011307",
			"name": "a(n) = -3*a(n-1) + 9*a(n-2) + 3*a(n-3), with a(0)=3, a(1)=-3, a(2)=27.",
			"comment": [
				"The Berndt-type sequence number 8 for the argument 2*Pi/9 defined by the trigonometric relations from the Formula section below.",
				"From the general recurrence relation: b(n) = -3*b(n-1) + 9*b(n-2) + 3*b(n-3), i.e., b(n) - b(n-2) = 8*b(n-2) + 3(b(n-3) - b(n-1)) the following summation formulas can be easily deduced: b(2*n+1) + 3*b(2*n) - 3*b(0) - b(1) = 8*Sum_{k=1..n} b(2*k-1) and b(2*n+2) + 3*b(2*n+1) - b(2) - 3*b(1) = 8*Sum_{k=1..n} b(2*k). Hence it follows that (a(2*n+1) + 3*a(2*n))/2 are all integers congruent to 3 modulo 4, and (a(2*n+2) + 3*a(2*n+1))/2 are all integers congruent to 1 modulo 4.",
				"We note that all numbers 3^(-1-floor(n/3))*a(n) = A215831(n) and 3^(-n-2)*a(3*n+2) are integers.",
				"The following decomposition holds true: (X - k(1)^n)*(X - (-k(2))^n)*(X - k(3)^n) = X^3 - sqrt(3)^(-n)*a(n)*X^2 + sqrt(3)^(-n)*T(n) - sqrt(3)^(-n), where T(2*n+1) = sqrt(3)*A215945(n) and T(2*n) = A215948(n). [_Roman Witula_, Aug 30 2012]"
			],
			"reference": [
				"D. Chmiela and R. Witula, Two parametric quasi-Fibonacci numbers of the nine order, (submitted, 2012)."
			],
			"link": [
				"Roman Witula, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL12/Witula/witula17.html\"\u003eRamanujan Type Trigonometric Formulas: The General Form for the Argument 2*Pi/7\u003c/a\u003e, Journal of Integer Sequences, Vol. 12 (2009), Article 09.8.5",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (-3, 9, 3)."
			],
			"formula": [
				"a(n) = (k(1)^n + (-k(2))^n + k(4)^n)*(sqrt(3))^n = (-1+4*c(1))^n + (-1+4*c(2))^n + (-1+4*c(4))^n, where k(j) := cot(2*Pi*j/9) and c(j) := cos(2*Pi*j/9).",
				"G.f.: (3 + 6*x - 9*x^2)/(1 + 3*x - 9*x^2 - 3*x^3). [corrected by _Georg Fischer_, May 10 2019]"
			],
			"example": [
				"We have k(1)^3 - k(2)^3 + k(4)^3 = -11*sqrt(3)."
			],
			"mathematica": [
				"LinearRecurrence[{-3, 9, 3}, {3, -3, 27}, 50]"
			],
			"xref": [
				"Cf. A215945, A215948, A216034, A215455, A215634, A215635, A215636, A215664, A215665, A215666, A215831, A215575, A108716, A215794, A215828."
			],
			"keyword": "sign,easy",
			"offset": "0,1",
			"author": "_Roman Witula_, Aug 24 2012",
			"references": 7,
			"revision": 43,
			"time": "2019-05-10T23:26:49-04:00",
			"created": "2012-08-24T10:52:01-04:00"
		}
	]
}