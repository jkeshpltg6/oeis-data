{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A106108",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 106108,
			"data": "7,8,9,10,15,18,19,20,21,22,33,36,37,38,39,40,41,42,43,44,45,46,69,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,141,144,145,150,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168",
			"name": "Rowland's prime-generating sequence: a(1) = 7; for n \u003e 1, a(n) = a(n-1) + gcd(n, a(n-1)).",
			"comment": [
				"The title refers to the sequence of first differences, A132199.",
				"Setting a(1) = 4 gives A084662.",
				"Rowland proves that the first differences are all 1's or primes. The prime differences form A137613.",
				"See A137613 for additional comments, links and references. - _Jonathan Sondow_, Aug 14 2008",
				"\"This recurrence was discovered at the 2003 NKS Summer School by a group led by Matt Frank. This Demonstration allows initial conditions. a(1) \u003e= 4. For 1 \u003c= a(1) \u003c= 3, a(n) - a(n-1) is 1 for n \u003e= 3.\" See Wolfram hyperlink. - _Robert G. Wilson v_, Sep 10 2008",
				"Not all starting values generate differences of all 1's or primes. The following a(1) generate composite differences: 532, 533, 534, 535, 698, 699, 706, 707, 708, 709, 712, 713, 714, 715, ... - _Dmitry Kamenetsky_, Jul 18 2015",
				"The same results are obtained if 2's are removed from n when gcd is performed, so the following is also true: a(1) = 7; for n \u003e 1, a(n) = a(n-1) + gcd(A000265(n), a(n-1)). - _David Morales Marciel_, Sep 14 2016"
			],
			"reference": [
				"Eric S. Rowland, A simple prime-generating recurrence, Abstracts Amer. Math. Soc., 29 (No. 1, 2008), p. 50 (Abstract 1035-11-986)."
			],
			"link": [
				"Indranil Ghosh, \u003ca href=\"/A106108/b106108.txt\"\u003eTable of n, a(n) for n = 1..25000\u003c/a\u003e (terms 1..1000 from T. D. Noe)",
				"Fernando Chamizo, Dulcinea Raboso and Serafin Ruiz-Cabello, \u003ca href=\"http://www.combinatorics.org/Volume_18/Abstracts/v18i2p10.html\"\u003eOn Rowland's sequence\u003c/a\u003e, Electronic J. Combin., Vol. 18(2), 2011, #P10.",
				"Brian Hayes, \u003ca href=\"http://bit-player.org/2015/pumping-the-primes\"\u003ePumping the Primes\u003c/a\u003e, bit-player, 19 August 2015.",
				"Eric S. Rowland, \u003ca href=\"http://arXiv.org/abs/0710.3217\"\u003eA simple prime-generating recurrence\u003c/a\u003e, arXiv:0710.3217 [math.NT], 2007-2008.",
				"Eric S. Rowland, \u003ca href=\"http://demonstrations.wolfram.com/PrimeGeneratingRecurrence/\"\u003e Prime-Generating Recurrence\u003c/a\u003e, Wolfram Demonstrations Project. - _Robert G. Wilson v_, Sep 10 2008"
			],
			"maple": [
				"S:=7; f:= proc(n) option remember; global S; if n=1 then RETURN(S); else RETURN(f(n-1)+gcd(n,f(n-1))); fi; end; [seq(f(n),n=1..200)];"
			],
			"mathematica": [
				"a[1] = 7; a[n_] := a[n] = a[n - 1] + GCD[n, a[n - 1]]; Array[a, 66] (* _Robert G. Wilson v_, Sep 10 2008 *)"
			],
			"program": [
				"(PARI) a=vector(100);a[1]=7;for(n=2,#a,a[n]=a[n-1]+gcd(n,a[n-1]));a \\\\ _Charles R Greathouse IV_, Jul 15 2011",
				"(Haskell)",
				"a106108 n = a106108_list !! (n-1)",
				"a106108_list =",
				"   7 : zipWith (+) a106108_list (zipWith gcd a106108_list [2..])",
				"-- _Reinhard Zumkeller_, Nov 15 2013",
				"(MAGMA) [n le 1 select 7 else Self(n-1) + Gcd(n, Self(n-1)): n in [1..70]]; // _Vincenzo Librandi_, Jul 19 2015"
			],
			"xref": [
				"Cf. A084662, A084663, A132199, A134734, A134736, A134743, A134744, A134162, A137613, A221869.",
				"Cf. A230504."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_, Jan 28 2008",
			"references": 61,
			"revision": 50,
			"time": "2017-02-02T08:20:23-05:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}