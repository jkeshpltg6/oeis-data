{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A317338",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 317338,
			"data": "1,1,0,-3,-5,10,58,23,-557,-1421,4094,28316,-52,-449150,-970286,5908939,31046627,-49583353,-750617284,-544416915,15819383275,46795708732,-288245326872,-1808819140124,3784215933076,57664747490276,14416027504376,-1664155475303224,-3937904190952656,43893853942734810,219165998056699650",
			"name": "O.g.f. A(x) satisfies: [x^n] exp( n*x*A(x) ) * (n+1 - n*A(x)) = 0 for n \u003e= 1.",
			"comment": [
				"Compare: the factorial series F(x) = Sum_{n\u003e=0} n!*x^n satisfies",
				"(1) [x^n] exp( x*F(x) ) * (n + 1 - F(x)) = 0 for n \u003e= 1,",
				"(2) [x^n] exp( n*x*F(x) ) * (2 - F(x)) = 0 for n \u003e= 1,",
				"(3) [x^n] exp( n^2*x*F(x) ) * (n + 1 - n*F(x)) = 0 for n \u003e= 1.",
				"It is remarkable that this sequence should consist entirely of integers."
			],
			"link": [
				"Paul D. Hanna, \u003ca href=\"/A317338/b317338.txt\"\u003eTable of n, a(n) for n = 0..400\u003c/a\u003e"
			],
			"example": [
				"O.g.f.: A(x) = 1 + x - 3*x^3 - 5*x^4 + 10*x^5 + 58*x^6 + 23*x^7 - 557*x^8 - 1421*x^9 + 4094*x^10 + 28316*x^11 - 52*x^12 - 449150*x^13 - 970286*x^14 + ...",
				"ILLUSTRATION OF DEFINITION.",
				"The table of coefficients of x^k/k! in exp( n*x*A(x) ) * (n+1 - n*A(x)) begins:",
				"n=1: [1, 0, 1, 16, 117, -704, -35075, -200304, 17660041, ...];",
				"n=2: [1, 0, 0, 20, 288, 912, -51200, -888480, 19165440, ...];",
				"n=3: [1, 0, -3, 0, 333, 3888, -27135, -1471824, 4665465, ...];",
				"n=4: [1, 0, -8, -56, 0, 5344, 33280, -1317312, -15647744, ...];",
				"n=5: [1, 0, -15, -160, -1035, 0, 81325, -180000, -25008375, ...];",
				"n=6: [1, 0, -24, -324, -3168, -20304, 0, 1156896, -10209024, ...];",
				"n=7: [1, 0, -35, -560, -6867, -67088, -422975, 0, 19205305, ...];",
				"n=8: [1, 0, -48, -880, -12672, -155712, -1525760, -9408384, 0, ...];",
				"n=9: [1, 0, -63, -1296, -21195, -305856, -3806595, -37346832, -230393079, 0, ...]; ...",
				"in which the coefficients of x^n in row n form a diagonal of zeros."
			],
			"program": [
				"(PARI) {a(n) = my(A=[1], m); for(i=1, n, A=concat(A, 0); m=#A; A[m] = Vec( exp( (m-1)*x*Ser(A) ) * (m - (m-1)*Ser(A)) )[m]/(m-1) ); A[n+1]}",
				"for(n=0, 30, print1(a(n), \", \"))"
			],
			"xref": [
				"Cf. A317337, A305110, A305114, A305115, A305116."
			],
			"keyword": "sign",
			"offset": "0,4",
			"author": "_Paul D. Hanna_, Aug 01 2018",
			"references": 2,
			"revision": 15,
			"time": "2018-08-01T23:10:27-04:00",
			"created": "2018-08-01T22:14:09-04:00"
		}
	]
}