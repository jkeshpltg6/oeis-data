{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A268555",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 268555,
			"data": "1,6,78,1260,22470,424116,8305836,166929048,3419932230,71109813060,1496053026468,31777397077608,680354749147164,14664155597771400,317877850826299800,6924815555276838960,151505459922479997510,3327336781596164286180",
			"name": "Diagonal of the rational function of six variables 1/((1 - w - u v - u v w) * (1 - z - x y)).",
			"comment": [
				"Also diagonal of rational function R(x,y,z) = 1 /(1 - x - y - z - x*y + x*z).",
				"Annihilating differential operator: x*(16*x^2-24*x+1)*(d/dx)^2 + (48*x^2-48*x+1)*(d/dx) + 12*x-6."
			],
			"link": [
				"Vaclav Kotesovec, \u003ca href=\"/A268555/b268555.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"A. Bostan, S. Boukraa, J.-M. Maillard, J.-A. Weil, \u003ca href=\"http://arxiv.org/abs/1507.03227\"\u003eDiagonals of rational functions and selected differential Galois groups\u003c/a\u003e, arXiv preprint arXiv:1507.03227 [math-ph], 2015.",
				"Jacques-Arthur Weil, \u003ca href=\"http://www.unilim.fr/pages_perso/jacques-arthur.weil/diagonals/\"\u003eSupplementary Material for the Paper \"Diagonals of rational functions and selected differential Galois groups\"\u003c/a\u003e"
			],
			"formula": [
				"D-finite with recurrence: n^2*a(n) -6*(2*n-1)^2*a(n-1) +4*(2*n-1)*(2*n-3)*a(n-2)=0. - _R. J. Mathar_, Mar 10 2016",
				"a(n) ~ sqrt(4+3*sqrt(2)) * 2^(2*n-3/2) * (1+sqrt(2))^(2*n) / (Pi*n). - _Vaclav Kotesovec_, Jul 01 2016",
				"G.f.: hypergeom([1/12, 5/12],[1],6912*x^4*(1-24*x+16*x^2)/(1-24*x+48*x^2)^3)/(1-24*x+48*x^2)^(1/4).",
				"0 = x*(16*x^2-24*x+1)*y'' + (48*x^2-48*x+1)*y' + (12*x-6)*y, where y is g.f.",
				"a(n) = A000984(n)*A001850(n) = C(2*n,n)*Sum_{k = 0..n} C(n,k)*C(n+k,k). - _Peter Bala_, Mar 19 2018"
			],
			"example": [
				"G.f. = 1 + 6*x + 78*x^2 + 1260*x^3 + 22470*x^4 + 424116*x^5 + 8305836*x^6 + ..."
			],
			"maple": [
				"A268555 := proc(n)",
				"    1/(1-w-u*v-u*v*w)/(1-z-x*y) ;",
				"    coeftayl(%,x=0,n) ;",
				"    coeftayl(%,y=0,n) ;",
				"    coeftayl(%,z=0,n) ;",
				"    coeftayl(%,u=0,n) ;",
				"    coeftayl(%,v=0,n) ;",
				"    coeftayl(%,w=0,n) ;",
				"end proc:",
				"seq(A268555(n),n=0..40) ; # _R. J. Mathar_, Mar 10 2016",
				"seq(binomial(2*n,n)*add(binomial(n,k)*binomial(n+k,k), k = 0..n), n = 0..20); # _Peter Bala_, Mar 19 2018"
			],
			"mathematica": [
				"sc = SeriesCoefficient;",
				"a[n_] := 1/(1-w-u*v-u*v*w)/(1-z-x*y) // sc[#, {x, 0, n}]\u0026 // sc[#, {y, 0, n}]\u0026 // sc[#, {z, 0, n}]\u0026 // sc[#, {u, 0, n}]\u0026 // sc[#, {v, 0, n}]\u0026 // sc[#, {w, 0, n}]\u0026;",
				"Table[a[n], {n, 0, 40}] (* _Jean-François Alcover_, Nov 14 2017 *)",
				"a[n_] := Product[Hypergeometric2F1[-n, -n, 1, i], {i, 1, 2}];",
				"Table[a[n], {n, 0, 17}]  (* _Peter Luschny_, Mar 19 2018 *)"
			],
			"program": [
				"(PARI) \\\\ system(\"wget http://www.jjj.de/pari/hypergeom.gpi\");",
				"read(\"hypergeom.gpi\");",
				"N = 18; x = 'x + O('x^N);",
				"Vec(hypergeom([1/12, 5/12],[1],6912*x^4*(1-24*x+16*x^2)/(1-24*x+48*x^2)^3, N)/(1-24*x+48*x^2)^(1/4)) \\\\ _Gheorghe Coserea_, Jul 05 2016",
				"(PARI) {a(n) = if( n\u003c1, n==0, my(A = vector(n+1)); A[1] = 1; A[2] = 6; for(k=2, n, A[k+1] = (6*(2*k-1)^2*A[k] - 4*(2*k-1)*(2*k-3)*A[k-1]) / k^2); A[n+1])}; /* _Michael Somos_, Jan 22 2017 */",
				"(PARI)",
				"diag(expr, N=22, var=variables(expr)) = {",
				"  my(a = vector(N));",
				"  for (k = 1, #var, expr = taylor(expr, var[#var - k + 1], N));",
				"  for (n = 1, N, a[n] = expr;",
				"    for (k = 1, #var, a[n] = polcoeff(a[n], n-1)));",
				"  return(a);",
				"};",
				"diag(1/(1 - x - y - z - x*y + x*z), 18)",
				"\\\\ test: diag(1/(1-x-y-z-x*y+x*z)) == diag(1/((1-w-u*v-u*v*w)*(1-z-x*y)))",
				"\\\\ _Gheorghe Coserea_, Jun 15 2018",
				"(GAP) List([0..20],n-\u003eBinomial(2*n,n)*Sum([0..n],k-\u003eBinomial(n,k)*Binomial(n+k,k))); # _Muniru A Asiru_, Mar 19 2018"
			],
			"xref": [
				"Cf. A268545-A268555. Cf. A000984, A001850."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Feb 29 2016",
			"references": 73,
			"revision": 62,
			"time": "2020-02-21T07:27:55-05:00",
			"created": "2016-02-29T10:53:24-05:00"
		}
	]
}