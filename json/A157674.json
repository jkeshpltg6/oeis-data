{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A157674",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 157674,
			"data": "1,1,1,-1,-3,1,9,1,-27,-13,81,67,-243,-285,729,1119,-2187,-4215,6561,15505,-19683,-56239,59049,202309,-177147,-724499,531441,2589521,-1594323,-9254363,4782969,33111969,-14348907,-118725597,43046721",
			"name": "G.f.: A(x) = 1 + x/exp( Sum_{k\u003e=1} (A((-1)^k*x) - 1)^k/k ).",
			"link": [
				"Muniru A Asiru, \u003ca href=\"/A157674/b157674.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e"
			],
			"formula": [
				"G.f.: A(x) = sqrt(1+4*x^2)/(sqrt(1+4*x^2) - x).",
				"a(n) = Sum(k=1..n-1, (k*Sum(j=0..n, j*2^j*(-1)^j*binomial(n,j)* binomial(2*(n-k)-j-1,n-k-1)))/(n*(n-k)))+(-1)^(n-1) n\u003e0, a(0)=1. - _Vladimir Kruchinin_, Apr 17 2011",
				"a(2*n) = (-3)*a(2*n-2) = (-3)^(n-1), n \u003e= 1; a(2*n+1) = (-3)*a(2*n-1) - 2*(-1)^n*A000108(n-1). - _Philippe Deléham_, Feb 02 2012",
				"a(2*n+1) = (-1)^n * A137720(n). - _Vaclav Kotesovec_, Jul 31 2014"
			],
			"example": [
				"G.f.: A(x) = 1 + x + x^2 - x^3 - 3*x^4 + x^5 + 9*x^6 + x^7 - 27*x^8 - ...",
				"ILLUSTRATION OF G.F.:",
				"A(x) = 1 + x/exp((A(-x)-1) + (A(x)-1)^2/2 + (A(-x)-1)^3/3 + (A(x)-1)^4/4 + ...)",
				"RELATED EXPANSION:",
				"Coefficients of 1/A(x) include central binomial coefficients:",
				"[1, -1, 0, 2, 0, -6, 0, 20, 0, -70, 0, 252, 0, -924, ...].",
				"From _Philippe Deléham_, Feb 02 2012: (Start)",
				"a(2) = 1,",
				"a(4) = (-3)*1 = -3,",
				"a(6) = (-3)*(-3) = 9,",
				"a(8) = (-3)*9 = -27,",
				"a(10) = (-3)*(-27) = 81,",
				"a(12) = (3)*81 = -243, etc.",
				"a(1) = 1,",
				"a(3) = (-3)*1 + 2*1 = -1,",
				"a(5) = (-3)*(-1)- 2*1 = 1,",
				"a(7) = (-3)*1 + 2*2 = 1,",
				"a(9) = (-3)*1 - 2*5 = -13,",
				"a(11) = (-3)*(-13) + 2*14 = 67,",
				"a(13) = (-3)*67 - 2*42 = -285,",
				"a(15) = (-3)*(-285) + 2*132 = 1119, etc. (End)"
			],
			"maple": [
				"1,seq(sum(k*sum(j*2^j*(-1)^j*binomial(n,j)*binomial(2*(n-k)-j-1,n-k-1)/(n*(n-k)),j=0..n),k=1..n-1) +(-1)^(n-1),n=1..200); # _Muniru A Asiru_, Feb 04 2018"
			],
			"mathematica": [
				"CoefficientList[Series[Sqrt[1+4*x^2]/(Sqrt[1+4*x^2] -x), {x, 0, 40}], x] (* _G. C. Greubel_, Nov 17 2018 *)"
			],
			"program": [
				"(PARI) {a(n)=local(A=1+x+x*O(x^n)); for(i=1,n,A=1+x*exp(-sum(k=1,n,(subst(A,x,(-1)^k*x+x*O(x^n))-1)^k/k))); polcoeff(A,n)}",
				"(Maxima) a(n):=sum((k*sum(j*2^j*(-1)^j*binomial(n,j)*binomial(2*(n-k)-j-1,n-k-1),j,0,n))/(n*(n-k)),k,1,n-1)+(-1)^(n-1); /* _Vladimir Kruchinin_, Apr 17 2011 */",
				"(GAP) Concatenation([1],List([1..200],n-\u003eSum([1..n-1], k-\u003ek*Sum([0..n], j-\u003ej*2^j*(-1)^j*Binomial(n,j)*Binomial(2*(n-k)-j-1,n-k-1))/(n*(n-k)))+(-1)^(n-1))); # _Muniru A Asiru_, Feb 04 2018",
				"(Sage) s= (sqrt(1+4*x^2)/(sqrt(1+4*x^2) - x)).series(x,40); s.coefficients(x, sparse=False) # _G. C. Greubel_, Nov 17 2018"
			],
			"xref": [
				"Cf. A000108, A000984, A156909, A137720."
			],
			"keyword": "sign",
			"offset": "0,5",
			"author": "_Paul D. Hanna_, Mar 05 2009",
			"references": 5,
			"revision": 27,
			"time": "2018-11-20T05:12:53-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}