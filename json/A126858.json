{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A126858",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 126858,
			"data": "0,0,1,8,30,80,180,336,620,960,1590,2200,3416,4368,6440,7920,11160,13056,18333,20520,27860,31360,41052,44528,59760,62400,80990,87120,109872,113680,147960,148800,188976,196416,240210,243040,311910,303696,376580,385840",
			"name": "Coefficients in quasimodular form F_2(q) of level 1 and weight 6.",
			"comment": [
				"This is also (5*E_2^3 - 3*E_2*E_4 - 2*E_6)/51840, where E_2, E_4, E_6 are the Eisenstein series shown in A006352, A004009, A013973, respectively. - _N. J. A. Sloane_, Feb 06 2017",
				"This is also ((q*(d/dq)E_4)/240 + q*(d/dq)(q*(d/dq)E_2)/24)/6, where E_2 and E_4 are the Eisenstein series shown in A006352 and A004009, respectively. - _Seiichi Manyama_, Feb 08 2017"
			],
			"reference": [
				"B. Mazur, Perturbations, deformations and variations ..., Bull. Amer. Math. Soc., 41 (2004), 307-336."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A126858/b126858.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e"
			],
			"formula": [
				"F_2(q) = (5*E(2)^3-3*E(2)*E(4)-2*E(6))/51840 where E(k) is the normalized Eisenstein series of weight k (cf. A006352, etc.).",
				"Expansion of (L1 * L2 - L3) / 720 where L1 = E2 (A006352), L2 = q * dL1/dq, L3 = q * dL2/dq in powers of q where E2 is an Eisenstein series. - _Michael Somos_, Jan 08 2012",
				"a(n) = (A145094(n)/240 - A282154(n)/24)/6 = (A281372(n) - A282097(n))/6. - _Seiichi Manyama_, Feb 08 2017"
			],
			"example": [
				"F_2(q) = q^2 + 8*q^3 + 30*q^4 + 80*q^5 + 180*q^6 + 336*q^7 + 620*q^8 + 960*q^9 + 1590*q^10 + 2200*q^11 + ..."
			],
			"maple": [
				"with(numtheory); M:=100;",
				"E := proc(k) local n, t1; global M;",
				"t1 := 1-(2*k/bernoulli(k))*add(sigma[k-1](n)*q^n, n=1..M+1);",
				"series(t1, q, M+1); end;",
				"e2:=E(2); e4:=E(4); e6:=E(6);",
				"series((5*e2^3-3*e2*e4-2*e6)/51840,q,M+1);",
				"seriestolist(%); # from _N. J. A. Sloane_, Feb 06 2017"
			],
			"mathematica": [
				"terms = 40; Ei[n_] = 1 - (2 n/BernoulliB[n]) Sum[k^(n-1) x^k/(1-x^k), {k, 1, terms}]; S = 5 Ei[2]^3 - 3 Ei[2] Ei[4] - 2 Ei[6]; CoefficientList[S + O[x]^terms, x]/SeriesCoefficient[S, {x, 0, 2}] (* _Jean-François Alcover_, Feb 28 2018 *)"
			],
			"program": [
				"(PARI) {a(n) = local(L1, L2, L3); if( n\u003c0, 0, L1 = 1 - 24 * sum( k = 1, n, sigma(k) * x^k, x * O(x^n)); L2 = x * L1'; L3 = x * L2'; polcoeff( (L1 * L2 - L3) / 720, n))} /* _Michael Somos_, Jan 08 2012 */"
			],
			"xref": [
				"Cf. A006352 (E_2), A004009 (E_4), A013973 (E_6), A145094 (q*(d/dq)E_4), A281372, A282097, A282154 (-q*(d/dq)(q*(d/dq)E_2))."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_, Mar 15 2007",
			"references": 5,
			"revision": 28,
			"time": "2018-02-28T16:22:03-05:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}