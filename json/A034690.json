{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A034690",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 34690,
			"data": "1,3,4,7,6,12,8,15,13,9,3,19,5,15,15,22,9,30,11,15,14,9,6,33,13,15,22,29,12,27,5,27,12,18,21,46,11,24,20,27,6,33,8,21,33,18,12,52,21,21,18,26,9,48,18,48,26,27,15,42,8,15,32,37,21,36,14,36,24,36,9,69,11,24,34",
			"name": "Sum of digits of all the divisors of n.",
			"comment": [
				"For first occurrence of k, or 0 if k never appears, see A191000.",
				"The only fixed points are 1 and 15. These are also the only loops of iterations of A034690: see the SeqFan thread \"List the divisors...\". - _M. F. Hasler_, Nov 08 2015",
				"The following sequence is composed of numbers n such that the sum of digits of all divisors of n equals 15: 8, 14, 15, 20, 26, 59, 62, ... It actually depicts the positions of number 15 in this sequence: see the SeqFan thread \"List the divisors...\". - _V.J. Pohjola_, Nov 09 2015"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A034690/b034690.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"H. Havermann et al, in reply to E. Angelini, \u003ca href=\"http://list.seqfan.eu/pipermail/seqfan/2015-November/015581.html\"\u003eList the dividers, sum the digits\u003c/a\u003e, SeqFan list, Nov. 2015.",
				"Maxwell Schneider and Robert Schneider, \u003ca href=\"https://arxiv.org/abs/1807.06710\"\u003eDigit sums and generating functions\u003c/a\u003e, arXiv:1807.06710 [math.NT], 2018-2020. See (22) p. 6."
			],
			"example": [
				"a(15) = 1 + 3 + 5 + (1+5) = 15. - _M. F. Hasler_, Nov 08 2015"
			],
			"maple": [
				"with(numtheory); read transforms; f:=proc(n) local t1, t2, i; t1:=divisors(n); t2:=0; for i from 1 to nops(t1) do t2:=t2+digsum(t1[i]); od: t2; end;",
				"# Alternative:",
				"sd:= proc(n) option remember; local k; k:= n mod 10; k + procname((n-k)/10) end proc:",
				"for n from 0 to 9 do sd(n):= n od:",
				"a:= n -\u003e add(sd(d), d=numtheory:-divisors(n)):",
				"map(a, [$1..100]); # _Robert Israel_, Nov 17 2015"
			],
			"mathematica": [
				"Table[Plus @@ Flatten@ IntegerDigits@ Divisors@n, {n, 75}] (* _Robert G. Wilson v_, Sep 30 2006 *)"
			],
			"program": [
				"(Haskell)",
				"a034690 = sum . map a007953 . a027750_row",
				"-- _Reinhard Zumkeller_, Jan 20 2014",
				"(PARI) vector(100, n, sumdiv(n, d, sumdigits(d))) \\\\ _Michel Marcus_, Jun 28 2015",
				"(PARI) A034690(n)=sumdiv(n,d,sumdigits(d)) \\\\ For use in other sequences. - _M. F. Hasler_, Nov 08 2015",
				"(Python)",
				"from sympy import divisors",
				"def sd(n): return sum(map(int, str(n)))",
				"def a(n): return sum(sd(d) for d in divisors(n))",
				"print([a(n) for n in range(1, 76)]) # _Michael S. Branicky_, Oct 06 2021"
			],
			"xref": [
				"Cf. A000005, A000203, A007953, A086793, A191000.",
				"Cf. A093653 (binary equivalent)"
			],
			"keyword": "base,easy,nonn",
			"offset": "1,2",
			"author": "_Erich Friedman_",
			"references": 21,
			"revision": 48,
			"time": "2021-10-06T16:19:50-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}