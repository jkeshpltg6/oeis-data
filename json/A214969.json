{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A214969",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 214969,
			"data": "1,1,5,2,7,2,1,2,8,3,5,4,0,5,8,2,9,0,6,8,0,8,3,0,3,3,0,1,9,9,0,9,6,4,3,5,6,8,0,1,4,2,5,7,5,7,6,5,6,3,7,6,1,8,5,5,2,7,1,1,2,9,2,6,0,1,1,1,8,1,8,5,1,4,3,4,2,0,2,4,8,4,5,3,6,4,6,8,7,2,7,6,6,5,7,6,7,6,0",
			"name": "Decimal representation of Sum{d(i)*3^i: i=0,1,...}, where Sum{d(i)*2^i: i=0,1,...} is the base 2 representation of sqrt(2).",
			"comment": [
				"This constant can be used to illustrate a fractal-type \"change-of-base function\".  Suppose that b\u003e1 and c\u003e1, and for x\u003e=0 given by the greedy algorithm as x = sum{d(i)*b^i}, define f(x) = sum{d(i)*c^i}.  The self-similarity of the graph of y = f(x) is given by the equation f(x/b) = (1/c)*f(x).  If b\u003cc, then f is strictly increasing; if b\u003ec, then f is not monotonic on any open interval.  The self-similarity is illustrated graphically by the second Mathematica program, for which b=2 and c=3."
			],
			"reference": [
				"Clark Kimberling, Fractal change-of-base functions, Advances and Applications in Mathematical Sciences, 12 (2013), 255-261."
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A214969/b214969.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"1.1527212835405829068083033019909643568...",
				"= 1 + 1/3^2 + 1/3^3 + 1/3^5 + ... obtained from",
				"sqrt(2) = 1 + 1/2^2 + 1/2^3 + 1/2^5 + ... ."
			],
			"mathematica": [
				"f[x_, b_, c_, d_] := FromDigits[RealDigits[x, b, d], c]",
				"N[f[Sqrt[2], 2, 3, 500], 120]",
				"RealDigits[%]  (* A214969 *)",
				"(* second program:  self-similar (fractal) graphs *)",
				"f[x_, b_, c_, digits_] := FromDigits[RealDigits[x, b, digits], c]",
				"Plot[f[x, 2, 3, 150], {x, 0, 1}, PlotPoints -\u003e 300]",
				"Plot[f[x, 2, 3, 150], {x, 0, 1/2}, PlotPoints -\u003e 300]",
				"Plot[f[x, 2, 3, 150], {x, 0, (1/2)^2}, PlotPoints -\u003e 300]",
				"Plot[f[x, 2, 3, 150], {x, 0, (1/2)^3}, PlotPoints -\u003e 300]"
			],
			"xref": [
				"Cf. A214970"
			],
			"keyword": "nonn,cons,base",
			"offset": "1,3",
			"author": "_Clark Kimberling_, Sep 01 2012",
			"references": 4,
			"revision": 15,
			"time": "2016-01-29T11:10:45-05:00",
			"created": "2012-09-02T08:27:41-04:00"
		}
	]
}