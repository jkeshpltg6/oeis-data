{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A275439",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 275439,
			"data": "0,0,0,2,2,6,12,22,42,78,140,252,448,788,1380,2402,4158,7170,12316,21082,35982,61246,103992,176184,297888,502728,846984,1424738,2393114,4014270,6725196,11253694,18810930,31410894,52400132,87335604,145438624,242001692",
			"name": "Sum of the asymmetry degrees of all compositions of n with parts in {1,2}.",
			"comment": [
				"The asymmetry degree of a finite sequence of numbers is defined to be the number of pairs of symmetrically positioned distinct entries. Example: the asymmetry degree of (2,7,6,4,5,7,3) is 2, counting the pairs (2,3) and (6,5).",
				"A sequence is palindromic if and only if its asymmetry degree is 0."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A275439/b275439.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,2,1,-2,-3,-1)."
			],
			"formula": [
				"G.f.: g(z) = 2z^3/((1+z+z^2)(1-z-z^2)^2). In the more general situation of compositions into a[1]\u003ca[2]\u003ca[3]\u003c..., denoting F(z) = Sum(z^{a[j]},j\u003e=1}, we have g(z)=(F(z)^2-F(z^2))/((1+F(z))(1-F(z))^2).",
				"a(n) = (n+1)/2-(3/2)*floor((n+2)/3)+(3/5)*(n+1)f(n)-(1/10)*(2n+5)f(n+1), where f(j)= A000045(j) are the Fibonacci numbers.",
				"a(n) = Sum_{k\u003e=0} k*A275438(n,k)."
			],
			"example": [
				"a(5) = 6 because the compositions of 5 with parts in {1,2} are 122, 212, 221, 1112, 1121, 1211, 2111, and 11111 and the sum of their asymmetry degrees is 1 + 0 + 1 + 1 + 1 + 1 + 1 + 0 = 6."
			],
			"maple": [
				"f := n -\u003e combinat:-fibonacci(n):",
				"a := n -\u003e (n+1)/2-(3/2)*floor((n+2)/3)+(3/5)*(n+1)*f(n)-(1/10)*(2*n+5)*f(n+1):",
				"seq(a(n), n = 0..40);",
				"# alternative program:",
				"g := 2*z^3/((1+z+z^2)*(1-z-z^2)^2):",
				"gser := series(g, z=0, 45):",
				"seq(coeff(gser, z, n), n = 0..40);"
			],
			"mathematica": [
				"Join[{0}, Table[Total@ Map[Total, Map[BitXor[Take[# - 1, Ceiling[Length[#]/2]], Reverse@ Take[# - 1, -Ceiling[Length[#]/2]]] \u0026,",
				"Flatten[Map[Permutations, DeleteCases[IntegerPartitions@ n, {a_, ___} /; a \u003e 2]], 1]]], {n, 30}]] (* _Michael De Vlieger_, Aug 17 2016 *)"
			],
			"program": [
				"(PARI) concat(vector(3), Vec(2*x^3/((1+x+x^2)*(1-x-x^2)^2) + O(x^50))) \\\\ _Colin Barker_, Aug 28 2016"
			],
			"xref": [
				"Cf. A000045, A275438."
			],
			"keyword": "nonn,easy",
			"offset": "0,4",
			"author": "_Emeric Deutsch_, Aug 16 2016",
			"references": 2,
			"revision": 20,
			"time": "2016-08-28T13:01:42-04:00",
			"created": "2016-08-16T13:23:07-04:00"
		}
	]
}