{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A052365",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 52365,
			"data": "1,1,4,10,24,51,114,219,424,768,1352,2278,3759,5978,9328,14181,21164,30943,44560,63063,88088,121321,165152,222157,295857,389948,509456,659697,847552,1080452,1367814,1719652,2148596,2668107,3294676,4046069",
			"name": "Number of nonnegative integer 3 X 3 matrices with sum of elements equal to n, under row and column permutations.",
			"comment": [
				"Also Molien series for group of structure S_3 X S_3 = (Z_3 X Z_3).O_2^+(3) and order 36, corresponding to complete weight enumerators of Hermitian self-dual GF(3)-linear codes over GF(9) containing the all-ones vector."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A052365/b052365.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"G. Nebe, E. M. Rains and N. J. A. Sloane, \u003ca href=\"http://neilsloane.com/doc/cliff2.html\"\u003eSelf-Dual Codes and Invariant Theory\u003c/a\u003e, Springer, Berlin, 2006.",
				"\u003ca href=\"/index/Mo#Molien\"\u003eIndex entries for Molien series\u003c/a\u003e"
			],
			"formula": [
				"G.f.: -(x^10+2*x^8+x^7+7*x^6-3*x^5+4*x^3+x^2-2*x+1) / ((x^4-x^3+x-1)*(x^3-1)^3*(x+1)^3*(x-1)^5).",
				"Another form for g.f.: u1/u2, where u1 := 1 + x + 2*x^3 + 10*x^4 + 17*x^5 + 19*x^6 + 20*x^7 + 29*x^8 + 37*x^9 + 34*x^10 + 23*x^11 + 12*x^12 + 7*x^13 + 3*x^14 + x^15 u2 := (1-x^2)^4*(1-x^3)^4*(1-x^6);"
			],
			"mathematica": [
				"permcount[v_List] := Module[{m = 1, s = 0, k = 0, t}, For[i = 1, i \u003c= Length[v], i++, t = v[[i]]; k = If[i \u003e 1 \u0026\u0026 t == v[[i - 1]], k + 1, 1]; m *= t*k; s += t]; s!/m];",
				"c[p_List, q_List, k_] := SeriesCoefficient[1/Product[(1 - x^LCM[p[[i]], q[[j]]])^GCD[p[[i]], q[[j]]], {j, 1, Length[q]}, {i, 1, Length[p]}], {x, 0, k}];",
				"M[m_, n_, k_] := Module[{s = 0}, Do[Do[s += permcount[p]*permcount[q]*c[p, q, k], {q, IntegerPartitions[n]}], {p, IntegerPartitions[m]}]; s/(m!*n!)];",
				"a[n_] := M[3, 3, n];",
				"a /@ Range[0, 40] (* _Jean-François Alcover_, Sep 03 2019, after _Andrew Howroyd_ in A318795 *)"
			],
			"xref": [
				"Row 3 of A318795.",
				"Cf. A002724, A053307, A052366, A052267, A092091."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Vladeta Jovovic_, Mar 08 2000",
			"references": 8,
			"revision": 18,
			"time": "2019-09-03T09:56:30-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}