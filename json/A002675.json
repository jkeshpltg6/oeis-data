{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002675",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2675,
			"id": "M5035 N2173",
			"data": "1,1,1,17,31,1,5461,257,73,1271,60787,241,22369621,617093,49981,16843009,5726623061,7957,91625968981,61681,231927781,50991843607,499069107643,4043309297,1100586419201,5664905191661,1672180312771",
			"name": "Numerators of coefficients for central differences M_{4}^(2*n).",
			"comment": [
				"From _Peter Bala_, Oct 03 2019: (Start)",
				"Numerators in the expansion of (2*sinh(x/2))^4 = x^4 + (1/6)*x^6 + (1/80)*x^8 + (17/30240)*x^10 + ....",
				"Let f(x) be a polynomial in x. The expansion of (2*sinh(x/2))^4 leads to a formula for the fourth central differences: f(x+2) - 4*f(x+1) + 6*f(x) - 4*f(x-1) + f(x-2) = (2*sinh(D/2))^4(f(x)) = D^4(f(x)) + (1/6)*D^6(f(x)) + (1/80)* D^8(f(x)) + (17/30240)*D^10(f(x)) + ..., where D denotes the differential operator d/dx. (End)"
			],
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"H. E. Salzer, \u003ca href=\"https://doi.org/10.1002/sapm1963421162\"\u003eTables of coefficients for obtaining central differences from the derivatives\u003c/a\u003e, Journal of Mathematics and Physics (this journal is also called Studies in Applied Mathematics), 42 (1963), 162-165, plus several inserted tables.",
				"H. E. Salzer, \u003ca href=\"/A002675/a002675.png\"\u003eAnnotated scanned copy of left side of Table II\u003c/a\u003e.",
				"H. E. Salzer, \u003ca href=\"/A002677/a002677.png\"\u003eAnnotated scanned copy of left side of Table III\u003c/a\u003e",
				"E. W. Weisstein, \u003ca href=\"http://mathworld.wolfram.com/CentralDifference.html\"\u003eCentral Difference\u003c/a\u003e. From MathWorld--A Wolfram Web Resource."
			],
			"maple": [
				"gf := (sinh(2*sqrt(x)) - 2*sinh(sqrt(x)))*sqrt(x):",
				"ser := series(gf, x, 40): seq(numer(coeff(ser,x,n)), n=2..28); # _Peter Luschny_, Oct 05 2019"
			],
			"xref": [
				"Cf. A002676 and A002677 (two different choices for denominators).",
				"Also equals A002430/A002431.",
				"Cf. A002671, A002672, A002673, A002674."
			],
			"keyword": "nonn,frac",
			"offset": "2,4",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Sean A. Irvine_, Dec 20 2016"
			],
			"references": 10,
			"revision": 34,
			"time": "2019-10-06T06:51:08-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}