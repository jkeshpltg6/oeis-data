{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A210850",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 210850,
			"data": "2,1,2,1,3,4,2,3,0,3,2,2,0,4,1,3,2,4,0,4,3,4,0,4,1,2,4,1,4,1,1,3,1,4,1,4,2,0,1,1,3,3,2,2,4,0,4,2,4,0,3,1,2,4,0,3,3,0,3,0,0,0,3,1,3,1,1,0,3,0,0,3,4,1,3,3,3,4,0,2,2,0,2,0,1,0,4,1,1,4,4,2,1,0,2,0,0,3,0,4",
			"name": "Digits of one of the two 5-adic integers sqrt(-1).",
			"comment": [
				"See A048898 for the successive approximations to this 5-adic integer, called there u.",
				"The digits of -u, the other 5-adic integer sqrt(-1), are given in A210851.",
				"a(n) is the unique solution of the linear congruence 2*A048898(n)*a(n) + A210848(n) == 0 (mod 5), n\u003e=1. Therefore only the values 0, 1, 2, 3 and 4 appear. See the Nagell reference given in A210848, eq. (6) on p. 86 adapted to this case. a(0)=2 follows from the formula given below.",
				"If n\u003e0, a(n) == A210848(n) (mod 5), since A048898(n) == 2 (mod 5). - _Álvar Ibeas_, Feb 21 2017",
				"If a(n)=0 then A048899(n+1) and A048899(n) coincide.",
				"a(n) + A210851(n) = 4 for n \u003e= 1. - _Robert Israel_, Mar 04 2016"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A210850/b210850.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = (b(n+1) - b(n))/5^n, n\u003e=0, with b(n):=A048898(n) computed from its recurrence. A Maple program for b(n) is given there.",
				"A048898(n+1) = sum(a(k)*5^k, k=0..n), n\u003e=0."
			],
			"example": [
				"a(4) = 3 because 2*182*3 + 53 = 1145 == 0 (mod 5).",
				"A048898(5) = 2057 = 2*5^0 + 1*5^1 + 2*5^2 + 1*5^3 + 3*5^4.",
				"a(8) = 0, therefore A048898(9) = A048898(8) = sum(a(k)*5^k, k=0..7) = 280182."
			],
			"maple": [
				"R:= select(t -\u003e padic:-ratvaluep(t,1)=2,[padic:-rootp(x^2+1,5,10001)]):",
				"op([1,1,3],R); # _Robert Israel_, Mar 04 2016"
			],
			"program": [
				"(PARI) a(n) = truncate(sqrt(-1+O(5^(n+1))))\\5^n; \\\\ _Michel Marcus_, Mar 05 2016"
			],
			"xref": [
				"Cf. A048898, A210848, A048899, A210849, A210851."
			],
			"keyword": "nonn,base,easy",
			"offset": "0,1",
			"author": "_Wolfdieter Lang_, Apr 30 2012",
			"ext": [
				"Keyword \"base\" added by _Jianing Song_, Feb 17 2021"
			],
			"references": 26,
			"revision": 23,
			"time": "2021-02-17T20:30:00-05:00",
			"created": "2012-05-01T00:09:27-04:00"
		}
	]
}