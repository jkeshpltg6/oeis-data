{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A298591",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 298591,
			"data": "12,13,17,20,24,25,29,31,33,34,35,36,40,41,43,44,45,48,52,53,57,58,59,61,63,65,68,72,73,74,76,80,81,85,89,90,91,96,97,100,104,106,108,109,113,116,117,125,127,129,130,132,133,134,136,137,141,144,145,146,148,149,150",
			"name": "Numbers which are the sum of two distinct perfect powers x^k + y^m with x, y, k, m \u003e= 2.",
			"comment": [
				"The number of terms between 2^(n-1) and 2^n-1 is, for n = 1, 2, 3, ...: 0, 0, 0, 2, 6, 17, 24, 69, 129*, 215, 425, 891, 1571, 2994, 5655*, 10535, 20132, 38840, 73510, 140730, 268438*, 514262, ... (For terms with * the next larger power of 2 is in the sequence, so it would be, e.g., ..., 130, 214, ... if we count from 2^n+1 to 2^(n+1).) At 2^22 this corresponds to a density of about 25%, decreasing by about 1% at each power of 2."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A298591/b298591.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Andrew Lohr, \u003ca href=\"https://arxiv.org/abs/1805.00076\"\u003eSeveral Topics in Experimental Mathematics\u003c/a\u003e, arXiv:1805.00076 [math.CO], 2018."
			],
			"example": [
				"12 = 2^2 + 2^3, 13 = 2^2 + 3^2, 17 = 2^3 + 3^2, ..."
			],
			"maple": [
				"N:= 1000: # for all terms \u003c= N",
				"PP:= {seq(seq(x^k,k=2..floor(log[x](N))),x=2..floor(sqrt(N)))}:",
				"sort(convert(select(`\u003c=`,{seq(seq(PP[i]+PP[j],i=1..j-1),j=2..nops(PP))},N),list)); # _Robert Israel_, May 27 2018"
			],
			"mathematica": [
				"max = 150; Table[If[x^k == y^m, Nothing, x^k + y^m], {x, 2, Sqrt[max-4]}, {y, x, Sqrt[max-4]}, {k, 2, Log[2, max-4]}, {m, 2, Log[2, max-4]}] // Flatten // Select[#, # \u003c= max \u0026]\u0026 // Union (* _Jean-François Alcover_, Sep 18 2018 *)"
			],
			"program": [
				"(PARI) is(n,A=A076467,s=sum2sqr(n))={for(i=1,#s, vecmin(s[i])\u003e1 \u0026\u0026 s[i][1]!=s[i][2] \u0026\u0026 return(1)); for(i=2,#A, n\u003eA[i]||return; ispower(n-A[i]) \u0026\u0026 A[i]*2!=n \u0026\u0026 return(1)) \\\\ A076467 must be computed up to limit n."
			],
			"xref": [
				"Cf. A076467, A111231, A304433, A304434, A304435, A304436."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_M. F. Hasler_, May 26 2018",
			"references": 1,
			"revision": 22,
			"time": "2018-09-18T09:06:38-04:00",
			"created": "2018-05-26T22:59:23-04:00"
		}
	]
}