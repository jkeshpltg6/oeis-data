{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A214128",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 214128,
			"data": "0,0,0,0,1,0,1,0,0,6,5,0,1,8,6,0,1,0,1,16,15,16,2,0,6,14,0,8,23,6,1,0,27,18,1,0,1,20,27,16,18,36,1,16,36,2,37,0,43,6,18,40,44,0,16,8,39,52,5,36,9,32,36,0,1,60,14,52,48,36,6,0,1,38,6,20,71",
			"name": "a(n) = 6^(6^6) mod n.",
			"comment": [
				"The indices of zeros in this sequence, i.e., divisors of 6^(6^6), are all numbers of the form 2^i * 3^j, with 0 \u003c= i, j \u003c= 6^6. [Edited by _M. F. Hasler_, Feb 25 2018]",
				"If c and N are any positive integers, and p^k is the largest prime power divisor of c, then the divisors of c^N less than p^(k*N+1) are precisely those numbers in that range whose prime factorization includes only primes that divide c. This is the case c = 6, N = 6^6, so p^k = 2^1 = 2; so the first difference in the divisor list from A003586 is for A003586(n) = 2^(6^6+1). _Franklin T. Adams-Watters_, Jul 12 2012",
				"Eventually constant: see formula. - _M. F. Hasler_, Feb 24 2018",
				"If n \u003e 1 is coprime to 6 and A000010(n) divides 6^6 then a(n)=1. - _Robert Israel_, Nov 27 2019"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A214128/b214128.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"\u003ca href=\"/index/Con#constant\"\u003eIndex entries for eventually constant sequences\u003c/a\u003e."
			],
			"formula": [
				"a(n) = 0 if and only if n = 2^i 3^j, 0 \u003c= i, j \u003c= 6^6; after the last of these zeros at n = 6^6^6, a(n) = 6^6^6 for all n \u003e 6^6^6 ~ 2.659*10^36305. - _M. F. Hasler_, Feb 24 2018"
			],
			"example": [
				"a(1) = 6^(6^6) mod 1 = 0.",
				"a(2) = 6^(6^6) mod 2 = 0.",
				"a(3) = 6^(6^6) mod 3 = 0.",
				"a(4) = 6^(6^6) mod 4 = 0."
			],
			"maple": [
				"seq(6 \u0026^ (6^6) mod n, n=1..100); # _Robert Israel_, Nov 27 2019"
			],
			"mathematica": [
				"Table[PowerMod[6, 6^6, n], {n, 100}]"
			],
			"program": [
				"(PARI) a(n)=lift(Mod(6,n)^6^6) \\\\ _Charles R Greathouse IV_, Jul 05 2012"
			],
			"xref": [
				"Cf. A129810 (9^9^9 mod n), A003586."
			],
			"keyword": "nonn",
			"offset": "1,10",
			"author": "_Marvin Ray Burns_, Jul 04 2012",
			"references": 2,
			"revision": 31,
			"time": "2019-11-27T02:57:35-05:00",
			"created": "2012-07-12T13:13:53-04:00"
		}
	]
}