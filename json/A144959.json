{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A144959",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 144959,
			"data": "1,0,1,2,5,11,30,78,223,658,2026,6429,21015,70233,239360,829224,2912947,10356334,37205121,134887153,493000086,1814902409,6724595543,25061885217,93899071368,353514105817,1336822098961,5075833932200",
			"name": "A134955(n) - A134955(n-1). Number of hyperforests spanning n unlabeled nodes without isolated vertices.",
			"comment": [
				"a(n) is the number of hyperforests with n unlabeled nodes without isolated vertices. This follows from the fact that for n\u003e0 A134955(n-1) counts the hyperforests of order n with one or more isolated nodes."
			],
			"formula": [
				"Euler transform of b(1) = 0, b(n \u003e 1) = A035053(n). - _Gus Wiseman_, May 21 2018"
			],
			"example": [
				"From _Gus Wiseman_, May 21 2018: (Start)",
				"Non-isomorphic representatives of the a(5) = 11 hyperforests are the following:",
				"  {{1,2,3,4,5}}",
				"  {{1,2},{3,4,5}}",
				"  {{1,5},{2,3,4,5}}",
				"  {{1,2,5},{3,4,5}}",
				"  {{1,2},{2,5},{3,4,5}}",
				"  {{1,2},{3,5},{4,5}}",
				"  {{1,4},{2,5},{3,4,5}}",
				"  {{1,5},{2,5},{3,4,5}}",
				"  {{1,3},{2,4},{3,5},{4,5}}",
				"  {{1,4},{2,5},{3,5},{4,5}}",
				"  {{1,5},{2,5},{3,5},{4,5}}",
				"(End)"
			],
			"mathematica": [
				"etr[p_] := etr[p] = Module[{b}, b[n_] := b[n] = If[n==0, 1, Sum[Sum[d*p[d], {d, Divisors[j]}]*b[n-j], {j, 1, n}]/n]; b];",
				"b[0] = 0; b[n_] := b[n] = etr[etr[b]][n-1];",
				"c[1] = 0; c[n_] := b[n] + etr[b][n] - Sum[b[k]*etr[b][n-k], {k, 0, n}];",
				"a = etr[c];",
				"Table[a[n], {n, 0, 27}] (* _Jean-François Alcover_, Jul 12 2018, after _Alois P. Heinz_'s code for A035053 *)"
			],
			"program": [
				"(PARI) \\\\ here b is A007563 as vector",
				"EulerT(v)={Vec(exp(x*Ser(dirmul(v,vector(#v,n,1/n))))-1, -#v)}",
				"b(n)={my(v=[1]);for(i=2, n, v=concat([1], EulerT(EulerT(v)))); v}",
				"seq(n)={my(u=b(n)); concat([1], EulerT(concat([0], Vec(Ser(EulerT(u))*(1-x*Ser(u))-1))))} \\\\ _Andrew Howroyd_, May 22 2018"
			],
			"xref": [
				"Cf. A030019, A035053, A048143, A054921, A134954, A134955, A134957, A144958 (unlabeled forests without isolated vertices), A144959, A304716, A304717, A304867, A304911."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_Washington Bomfim_, Sep 27 2008",
			"references": 19,
			"revision": 16,
			"time": "2018-07-12T08:55:17-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}