{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A090822",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 90822,
			"data": "1,1,2,1,1,2,2,2,3,1,1,2,1,1,2,2,2,3,2,1,1,2,1,1,2,2,2,3,1,1,2,1,1,2,2,2,3,2,2,2,3,2,2,2,3,3,2,1,1,2,1,1,2,2,2,3,1,1,2,1,1,2,2,2,3,2,1,1,2,1,1,2,2,2,3,1,1,2,1,1,2,2,2,3,2,2,2,3,2,2,2,3,3,2,2,2,3,2,1",
			"name": "Gijswijt's sequence: a(1) = 1; for n\u003e1, a(n) = largest integer k such that the word a(1)a(2)...a(n-1) is of the form xy^k for words x and y (where y has positive length), i.e., the maximal number of repeating blocks at the end of the sequence so far.",
			"comment": [
				"Here xy^k means the concatenation of the words x and k copies of y.",
				"The name \"Gijswijt's sequence\" is due to _N. J. A. Sloane_, not the author!",
				"Fix n and suppose a(n) = k. Let len_y(n) = length of shortest y for this k and let len_x = n-1 - k*len_y(n) = corresponding length of x. A091407 and A091408 give len_y and len_x. For the subsequence when len_x = 0 see A091410 and A091411.",
				"The first 4 occurs at a(220) (see A091409).",
				"The first 5 appears around term 10^(10^23).",
				"We believe that for all N \u003e= 6, he first time N appears is at about position 2^(2^(3^(4^(5^...^(N-1))))). - _N. J. A. Sloane_ and _Allan Wilks_, Mar 14 2004",
				"In the first 100000 terms the fraction of [1's, 2's, 3's, 4's] seems to converge, to about [.287, .530, .179, .005] respectively. - _Allan Wilks_, Mar 04 2004",
				"When k=12 is reached, say, it is treated as the number 12, not as 1,2. This is not a base-dependent sequence.",
				"Does this sequence have a finite average? Does anyone know the exact value? - _Franklin T. Adams-Watters_, Jan 23 2008",
				"Answer: Given that \"...the fraction of [1's, 2's, 3's, 4's] seems to converge, to about [.287, .530, .179, .005]...\" that average should be the dot product of these vectors, i.e., about 1.904. - _M. F. Hasler_, Jan 24 2008",
				"Which is the first step with two consecutive 4's? Or the shortest run found so far between two 4's? - _Sergio Pimentel_, Oct 10 2016"
			],
			"reference": [
				"N. J. A. Sloane, Seven Staggering Sequences, in Homage to a Pied Puzzler, E. Pegg Jr., A. H. Schoen and T. Rodgers (editors), A. K. Peters, Wellesley, MA, 2009, pp. 93-110."
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/A090822/b090822.txt\"\u003eTable of n, a(n) for n = 1..24000\u003c/a\u003e",
				"Andreas Abel and Andres Loeh, \u003ca href=\"/A090822/a090822.hs.txt\"\u003eHaskell program for A090822\u003c/a\u003e",
				"F. J. van de Bult, D. C. Gijswijt, J. P. Linderman, N. J. A. Sloane and Allan Wilks, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL10/Sloane/sloane55.html\"\u003eA Slow-Growing Sequence Defined by an Unusual Recurrence\u003c/a\u003e, J. Integer Sequences, Vol. 10 (2007), #07.1.2.",
				"B. Chaffin, J. P. Linderman, N. J. A. Sloane and Allan Wilks, \u003ca href=\"http://arxiv.org/abs/1212.6102\"\u003eOn Curling Numbers of Integer Sequences\u003c/a\u003e, arXiv:1212.6102 [math.CO], Dec 25 2012.",
				"B. Chaffin, J. P. Linderman, N. J. A. Sloane and Allan Wilks, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL16/Sloane/sloane3.html\"\u003eOn Curling Numbers of Integer Sequences\u003c/a\u003e, Journal of Integer Sequences, Vol. 16 (2013), Article 13.4.3.",
				"Benjamin Chaffin and N. J. A. Sloane, \u003ca href=\"http://neilsloane.com/doc/CNC.pdf\"\u003eThe Curling Number Conjecture\u003c/a\u003e, preprint.",
				"D. C. Gijswijt, \u003ca href=\"https://pyth.eu/uploads/user/ArchiefPDF/Pyth55-3.pdf\"\u003eKrulgetallen\u003c/a\u003e, Pythagoras, 55ste Jaargang, Nummer 3, Jan 2016, (Article in Dutch about this sequence, see pages 10-13, cover and back cover).",
				"N. J. A. Sloane, \u003ca href=\"http://neilsloane.com/doc/g4g7.pdf\"\u003eSeven Staggering Sequences\u003c/a\u003e.",
				"N. J. A. Sloane, \u003ca href=\"/A195264/a195264.pdf\"\u003eConfessions of a Sequence Addict (AofA2017)\u003c/a\u003e, slides of invited talk given at AofA 2017, Jun 19 2017, Princeton. Mentions this sequence.",
				"\u003ca href=\"/index/Ge#Gijswijt\"\u003eIndex entries for sequences related to Gijswijt's sequence\u003c/a\u003e",
				"\u003ca href=\"/index/Cu#curling_numbers\"\u003eIndex entries for sequences related to curling numbers\u003c/a\u003e"
			],
			"maple": [
				"K:= proc(L)",
				"local n,m,k,i,b;",
				"m:= 0;",
				"n:= nops(L);",
				"for k from 1 do",
				"  if k*(m+1) \u003e n then return(m) fi;",
				"  b:= L[-k..-1];",
				"  for i from 1 while i*k \u003c= n and L[-i*k .. -(i-1)*k-1] = b do od:",
				"  m:= max(m, i-1);",
				"od:",
				"end proc:",
				"A[1]:= 1:",
				"for i from 2 to 220 do",
				"  A[i]:= K([seq(A[j],j=1..i-1)])",
				"od:",
				"seq(A[i],i=1..220); # _Robert Israel_, Jul 02 2015"
			],
			"mathematica": [
				"ClearAll[a]; reversed = {a[2]=1, a[1]=1}; blocs[len_] := Module[{bloc1, par, pos}, bloc1 = Take[reversed, len]; par = Partition[ reversed, len]; pos = Position[par, bloc_ /; bloc != bloc1, 1, 1]; If[pos == {}, Length[par], pos[[1, 1]] - 1]]; a[n_] := a[n] = Module[{an}, an = Table[{blocs[len], len}, {len, 1, Quotient[n-1, 2]}] // Sort // Last // First; PrependTo[ reversed, an]; an]; A090822 = Table[a[n], {n, 1, 99}] (* _Jean-François Alcover_, Aug 13 2012 *)"
			],
			"program": [
				"(Haskell)  see link.",
				"(PARI) A090822(n,A=[])={while(#A\u003cn, my(k=1,L=0,m=k); while((k+1)*(L+1)\u003c=#A, for(N=L+1,#A/(m+1), A[-m*N..-1]==A[-(m+1)*N..-N-1]\u0026\u0026(m+=1)\u0026\u0026break);m\u003ek||break; k=m);A=concat(A,k));A} \\\\ _M. F. Hasler_, Aug 08 2018"
			],
			"xref": [
				"Cf. A091407, A091408, A091409 (records), A091410, A091411, A091579, A091586, A091970, A093955-A093958.",
				"A091412 gives lengths of runs. A091413 gives partial sums.",
				"Cf. also A094004, A160766, A217206, A217207.",
				"Generalizations: A094781, A091975, A091976, A092331-A092335."
			],
			"keyword": "nonn,nice",
			"offset": "1,3",
			"author": "_Dion Gijswijt_, Feb 27 2004",
			"references": 80,
			"revision": 93,
			"time": "2019-02-03T18:05:36-05:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}