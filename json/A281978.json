{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A281978",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 281978,
			"data": "1,4,2,6,3,15,5,20,10,40,8,24,12,36,9,54,18,90,30,120,60,180,45,135,27,162,81,324,108,216,72,144,16,64,32,96,48,240,80,320,160,640,128,384,192,576,288,864,432,1296,648,1944,243,972,486,1458,729,3645,405",
			"name": "Lexicographically earliest sequence of distinct terms such that, for any n\u003e0, a(2n) is divisible by a(2n-1) and by a(2n+1).",
			"comment": [
				"To compute a(2n) and a(2n+1): we take the least unseen multiple of a(2n-1) with an unseen proper divisor: the multiple gives a(2n) and the least proger divisor gives a(2n+1).",
				"The first multiple of 2 occurs at n=2: a(2)=4, and a(3)=2.",
				"The first multiple of 3 occurs at n=4: a(4)=6, and a(5)=3,",
				"The first multiple of 5 occurs at n=6: a(6)=15, and a(7)=5.",
				"The first multiple of 7 occurs at n=454: a(454)=5511240, and a(455)=7.",
				"The first multiple of 11 occurs at n=889838: a(889838)=627667978163491186346557440000000000000, and a(889839)=11.",
				"For n\u003e1, let b(n)=least k\u003e0 such that a(n+k)\u003c\u003ea(n)*a(k+1); the first records for b are:",
				"n       b(n)     a(n)",
				"------  -------  ----",
				"2             1  2^2",
				"7             3  5",
				"19            4  2*3*5",
				"33           14  2^4",
				"73           27  5^2",
				"455         243  7",
				"1439        248  7^2",
				"3069        275  7^3",
				"10567       276  7^5",
				"41709       768  7^8",
				"85179      1169  7^10",
				"889839  \u003e110162  11",
				"Conjectures:",
				"- All prime numbers appear in this sequence, in increasing order,",
				"- The derived sequence b is unbounded,",
				"- This sequence is a permutation of the natural numbers."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A281978/b281978.txt\"\u003eTable of n, a(n) for n = 1..25000\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A281978/a281978.gp.txt\"\u003ePARI program for A281978\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A281978/a281978.png\"\u003eLogarithmic scatterplot of the first million terms\u003c/a\u003e"
			],
			"example": [
				"The first terms, alongside their p-adic valuations with respect to p=2, 3, 5 and 7 (with 0's omitted), are:",
				"n       a(n)  v2  v3  v5  v7",
				"---  -------  --  --  --  --",
				"1          1",
				"2          4   2",
				"3          2   1",
				"4          6   1   1",
				"5          3       1",
				"6         15       1   1",
				"7          5           1",
				"8         20   2       1",
				"9         10   1       1",
				"10        40   3       1",
				"11         8   3",
				"12        24   3   1",
				"13        12   2   1",
				"14        36   2   2",
				"15         9       2",
				"16        54   1   3",
				"17        18   1   2",
				"18        90   1   2   1",
				"19        30   1   1   1",
				"20       120   3   1   1",
				"21        60   2   1   1",
				"22       180   2   2   1",
				"23        45       2   1",
				"24       135       3   1",
				"...",
				"451   524880   4   8   1",
				"452  1574640   4   9   1",
				"453   787320   3   9   1",
				"454  5511240   3   9   1   1",
				"455        7               1",
				"456       28   2           1",
				"457       14   1           1",
				"458       42   1   1       1"
			],
			"xref": [
				"Cf. A036552 (a(2n) is divisible by a(2n-1))."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Rémy Sigrist_, Feb 04 2017",
			"references": 9,
			"revision": 10,
			"time": "2017-02-07T04:04:38-05:00",
			"created": "2017-02-07T04:04:38-05:00"
		}
	]
}