{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A297473",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 297473,
			"data": "1,2,5,16,11,90,17,512,625,550,23,6480,31,1666,2695,65536,41,101250,47,110000,10285,5566,59,1866240,14641,10478,1953125,653072,67,1212750,73,33554432,19435,23698,31603,65610000,83,33934,44795,88000000,97,9071370,103",
			"name": "For any number n \u003e 0, let f(n) be the polynomial of a single indeterminate x where the coefficient of x^e is the prime(1+e)-adic valuation of n (where prime(k) denotes the k-th prime); f establishes a bijection between the positive numbers and the polynomials of a single indeterminate x with nonnegative integer coefficients; let g be the inverse of f; a(n) = g(f(n)^2).",
			"comment": [
				"This sequence is the main diagonal of A297845.",
				"This sequence has similarities with A296857."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A297473/b297473.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A297473/a297473.png\"\u003eColored logarithmic scatterplot of the first 100000 terms\u003c/a\u003e (where the color is function of A001222(n))"
			],
			"formula": [
				"For any n \u003e 0 and k \u003e 0:",
				"- A001221(a(n)) \u003c= A001221(n)^2,",
				"- A001222(a(n)) = A001222(n)^2,",
				"- A055396(a(n)) = 2*A055396(n)-1 + [n=1],",
				"- A061395(a(n)) = 2*A061395(n)-1 + [n=1],",
				"- a(A000040(n)) = A031368(n),",
				"- a(A000040(n)^k) = A031368(n)^(k^2)."
			],
			"example": [
				"For n = 12:",
				"- 12 = 2^2 * 3 = prime(1+0)^2 * prime(1+1),",
				"- f(12) = 2 + x,",
				"- f(12)^2 = 4 + 4*x + x^2,",
				"- a(12) = prime(1+0)^4 * prime(1+1)^4 * prime(1+2) = 2^4 * 3^4 * 5 = 6480."
			],
			"program": [
				"(PARI) a(n) = my (f=factor(n), p=apply(primepi, f[,1]~)); prod (i=1, #p, prod(j=1, #p, prime(p[i]+p[j]-1)^(f[i,2]*f[j,2])))"
			],
			"xref": [
				"Cf. A000040, A001221, A001222, A031368, A055396, A061395, A296857, A297845."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Rémy Sigrist_, Dec 30 2017",
			"references": 3,
			"revision": 19,
			"time": "2019-07-06T01:50:26-04:00",
			"created": "2018-01-13T14:04:37-05:00"
		}
	]
}