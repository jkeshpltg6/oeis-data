{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A203019",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 203019,
			"data": "0,0,1,1,1,2,4,8,17,37,82,185,423,978,2283,5373,12735,30372,72832,175502,424748,1032004,2516347,6155441,15101701,37150472,91618049,226460893,560954047,1392251012,3461824644,8622571758,21511212261,53745962199",
			"name": "Number of elevated peakless Motzkin paths.",
			"comment": [
				"Essentially the same as A004148: a(0)=a(1)=0 and a(n) = A004148(n-2) for n\u003e=2."
			],
			"reference": [
				"A. Panayotopoulos and P. Tsikouras, Properties of meanders, JCMCC 46 (2003), 181-190.",
				"A. Panayotopoulos and P. Vlamos, Meandric Polygons, Ars Combinatoria 87 (2008), 147-159."
			],
			"link": [
				"Muniru A Asiru, \u003ca href=\"/A203019/b203019.txt\"\u003eTable of n, a(n) for n = 0..300\u003c/a\u003e",
				"I. Jensen, \u003ca href=\"http://arxiv.org/abs/cond-mat/9910313\"\u003eEnumeration of plane meanders\u003c/a\u003e, arXiv:cond-mat/9910313 [cond-mat.stat-mech], 1999.",
				"S. K. Lando and A. K. Zvonkin, \u003ca href=\"http://dx.doi.org/10.1016/0304-3975(93)90316-L\"\u003ePlane and projective meanders\u003c/a\u003e, Theoretical Computer Science Vol. 117 (1993) p. 232.",
				"A. Panayotopoulos and P. Tsikouras, \u003ca href=\"https://msh.revues.org/2808\"\u003eThe multimatching property of nested sets\u003c/a\u003e, Math. \u0026 Sci. Hum. 149 (2000), 23-30.",
				"A. Panayotopoulos and P. Tsikouras, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL7/Panayotopoulos/panayo4.html\"\u003eMeanders and Motzkin Words\u003c/a\u003e, J. Integer Seqs., Vol. 7, 2004.",
				"A. Panayotopoulos and P. Vlamos, \u003ca href=\"http://dx.doi.org/10.1007/978-3-642-33412-2_49\"\u003eCutting Degree of Meanders\u003c/a\u003e, Artificial Intelligence Applications and Innovations, IFIP Advances in Information and Communication Technology, Volume 382, 2012, pp 480-489; DOI 10.1007/978-3-642-33412-2_49. - From _N. J. A. Sloane_, Dec 29 2012"
			],
			"formula": [
				"G.f.: x^2 / (1 - x / (1 - x^2 / (1 - x / (1 - x^2 / (1 - x / (1 - x^2 / ...)))))). - _Michael Somos_, May 12 2012",
				"G.f. A(x) =: y satisfies y / x = x + y / (1 - y). - _Michael Somos_, Jan 31 2014",
				"G.f. A(x) =: y satisfies y = x^2 + (x - x^2)*y + y*y. - _Michael Somos_, Jan 31 2014",
				"Given g.f. A(x), then B(x) = A(x)/x satisfies B(-B(-x)) = x. - _Michael Somos_, Jan 31 2014",
				"a(n) = Sum_{m=0..(n-1)/2}((binomial(2*m+1,m)*Sum_{k=0..n-2*m-2}(binomial(k,n-2*m-k-2)*binomial(2*m+k,k)*(-1)^(n-k)))/(2*m+1)). - _Vladimir Kruchinin_, Mar 12 2016",
				"a(n) ~ 5^(1/4) * phi^(2*n - 2) / (2*sqrt(Pi)*n^(3/2)), where phi = A001622 = (1 + sqrt(5))/2 is the golden ratio. - _Vaclav Kotesovec_, Aug 14 2018"
			],
			"example": [
				"G.f. = x^2 + x^3 + x^4 + 2*x^5 + 4*x^6 + 8*x^7 + 17*x^8 + 37*x^9 + ..."
			],
			"mathematica": [
				"terms = 34;",
				"A[_] = 0; Do[A[x_] = x (x - A[x] / (A[x] - 1)) + O[x]^terms, {terms}];",
				"CoefficientList[A[x], x] (* _Jean-François Alcover_, Jul 27 2018, after _Michael Somos_ *)",
				"Table[Sum[Binomial[2*m + 1, m]*Sum[(Binomial[k, n - 2*m - k - 2]* Binomial[2*m + k, k]*(-1)^(n - k))/(2*m + 1), {k, 0, n - 2*m - 2}], {m, 0, Floor[(n - 1)/2]}], {n, 0, 50}] (* _G. C. Greubel_, Aug 12 2018 *)"
			],
			"program": [
				"(PARI) {a(n) = local(A); A = O(x); for( k=1, ceil(n / 3), A = x^2 / (1 - x / (1 - A))); polcoeff( A, n)} /* _Michael Somos_, May 12 2012 */",
				"(Maxima)",
				"a(n):=sum((binomial(2*m+1,m)*sum(binomial(k,n-2*m-k-2)*binomial(2*m+k,k)*(-1)^(n-k),k,0,n-2*m-2))/(2*m+1),m,0,(n-1)/2); /* _Vladimir Kruchinin_, Mar 12 2016 */",
				"(GAP) List([0..40],n-\u003eSum([0..Int((n-1)/2)],m-\u003eBinomial(2*m+1,m)*Sum([0..n-2*m-2],k-\u003e(Binomial(k,n-2*m-k-2)*Binomial(2*m+k,k)*(-1)^(n-k))/(2*m+1)))); # _Muniru A Asiru_, Aug 13 2018"
			],
			"keyword": "nonn",
			"offset": "0,6",
			"author": "_Panayotis Vlamos_ and _Antonios Panayotopoulos_, Dec 27 2011",
			"references": 2,
			"revision": 48,
			"time": "2018-08-14T09:03:13-04:00",
			"created": "2012-01-03T19:31:42-05:00"
		}
	]
}