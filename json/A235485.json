{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A235485",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 235485,
			"data": "0,1,2,4,3,7,8,6,5,16,14,17,12,19,9,28,10,13,32,11,21,24,34,53,20,49,38,64,18,43,56,59,15,68,26,42,48,37,22,76,35,67,36,23,51,112,106,107,40,27,98,52,57,29,128,119,30,44,86,41,84,131,118,96,25,133,136,31,39,212,63,73,80",
			"name": "Permutation of natural numbers: a(n) = A235201(A235487(n)).",
			"comment": [
				"The permutation satisfies A000040(a(n)) = a(A000040(n)) for all positive n except n=2 and n=4.",
				"This permutation has only finite cycles: numbers 0, 1, 2, 3, ... are in the cycles of size: 1, 1, 1, 2, 2, 4, 4, 4, 4, 4, 4, 4, 1, 4, 4, 4, 4, 4, 4, 4, 3, 3, 4, 4, 3, 4, 4, 4, 4, 4, 2, 4, 4, 4, 4, 5, 5, 1, 4, 4, 5, 4, 5, 4, 4, 2, 4, 4, 5, 4, 4, 4, 4, 4, 4, 4, 2, 4, 4, 4, 7, 4, 4, 7, 4, 4, 4, 4, 4, 4, 7, 3, 7, 3, 1, ...",
				"The first number with cycle size 1 (i.e., fixed point) is 0, the first in a 2-cycle is 3 (as a(3) = 4, a(4) = 3), the first in 3-cycle is 20, the first in 4-cycle is 5, the first in 5-cycle is 35, in 6-cycle 213, in 7-cycle 60, in 8-cycle and 9-cycle (no terms among 0..10080), the first in 10-cycle: 447, the first in 12-cycle: 220, in 14-cycle: 843, in 15-cycle: 2485, in 20-cycle: 385.",
				"Please compare to the cycle structure of A235493/A235494.",
				"Also of interest is the number of separate cycles (orbits) and fixed points among each A000081(n) rooted non-oriented trees when this bijection is applied to them, as trees encoded by Matula-Goebel numbers (cf. A061773)."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A235485/b235485.txt\"\u003eTable of n, a(n) for n = 0..10080\u003c/a\u003e",
				"\u003ca href=\"/index/Mat#matula\"\u003eIndex entries for sequences related to Matula-Goebel numbers\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A235201(A235487(n)).",
				"As a recurrence:",
				"  a(0)=0, a(1)=1, a(2)=2,",
				"  a(3*n) = 4*a(n),",
				"  a(8*n) = 5*a(n),",
				"  a(4*n) = 3*a(n) [when n is odd],",
				"  a(14*n) = 9*a(n),",
				"  a(49*n) = 27*a(n),",
				"  a(7*n) = 6*a(n) [when n is odd and not divisible by 7],",
				"  a(p_i) = p_{a(i)} for primes whose index i is neither 2 nor 4 [primes other than 3 or 7],",
				"and",
				"  a(u * v) = a(u) * a(v) for other composite cases."
			],
			"program": [
				"(Scheme) (define (A235485 n) (A235201 (A235487 n)))",
				";; Alternative implementation based on the given direct recurrence. Needs _Antti Karttunen_'s IntSeq-library:",
				"(definec (A235485 n) (cond ((\u003c n 3) n) ((zero? (modulo n 3)) (* 4 (A235485 (/ n 3)))) ((zero? (modulo n 8)) (* 5 (A235485 (/ n 8)))) ((zero? (modulo n 4)) (* 3 (A235485 (/ n 4)))) ((zero? (modulo n 14)) (* 9 (A235485 (/ n 14)))) ((zero? (modulo n 49)) (* 27 (A235485 (/ n 49)))) ((zero? (modulo n 7)) (* 6 (A235485 (/ n 7)))) ((= 1 (A010051 n)) (A000040 (A235485 (A000720 n)))) (else (reduce * 1 (map A235485 (ifactor n))))))"
			],
			"xref": [
				"Inverse: A235486. Cf. also A235201, A235487, A235493/A235494, A234743/A234744, A000081, A061773."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Antti Karttunen_, Jan 11 2014",
			"ext": [
				"Name and incorrect claim about multiplicativity corrected by _Antti Karttunen_, Feb 12 2018"
			],
			"references": 10,
			"revision": 28,
			"time": "2018-02-12T17:41:45-05:00",
			"created": "2014-01-22T17:55:22-05:00"
		}
	]
}