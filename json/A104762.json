{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A104762",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 104762,
			"data": "1,1,1,2,1,1,3,2,1,1,5,3,2,1,1,8,5,3,2,1,1,13,8,5,3,2,1,1,21,13,8,5,3,2,1,1,34,21,13,8,5,3,2,1,1,55,34,21,13,8,5,3,2,1,1,89,55,34,21,13,8,5,3,2,1,1,144,89,55,34,21,13,8,5,3,2,1,1,233,144,89,55,34,21,13,8,5,3,2,1",
			"name": "Triangle read by rows: row n contains first n nonzero Fibonacci numbers in decreasing order.",
			"comment": [
				"Sum of n-th row = F(n+2) - 1; sequence A000071 starting (1, 2, 4, 7, 12, 20,...).",
				"Riordan array (1/(1-x-x^2),x) . [_Philippe Deléham_, Apr 23 2009] [with offset 0]",
				"Sequence B is called a reverse reluctant sequence of sequence A, if B is triangle array read by rows: row number k lists first k elements of the sequence A in reverse order. Sequence A104762 is the reverse reluctant sequence of Fibonacci numbers (A000045), except 0. - _Boris Putievskiy_, Dec 13 2012"
			],
			"link": [
				"Harvey P. Dale, \u003ca href=\"/A104762/b104762.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Boris Putievskiy, \u003ca href=\"http://arxiv.org/abs/1212.2732\"\u003eTransformations Integer Sequences And Pairing Functions\u003c/a\u003e arXiv:1212.2732 [math.CO]"
			],
			"formula": [
				"In every column, (1, 1, 2, 3, 5,...); the nonzero Fibonacci numbers, A000045.",
				"a(n,k) = A000045(n-k+1). - _R. J. Mathar_, Jun 23 2006",
				"a(n)=A000045(m), where m=(t*t+3*t+4)/2-n, t=floor[(-1+sqrt(8*n-7))/2]. - _Boris Putievskiy_, Dec 13 2012",
				"Let P denote Pascal's triangle. Then P*A104762*P^(-1 ) = A121461. - _Peter Bala_, Apr 11 2013",
				"a(n,k) = |round[(r^n)*(s^k)/sqrt(5)|, where r = golden ratio = (1+ sqrt(5))/2, s = (1 - sqrt(5))/2, 1 \u003c = k \u003c= n-1, n \u003e = 2. - _Clark Kimberling_, May 01 2016",
				"G.f. of triangle: G(x,y) = x*y/((1-x-x^2)*(1-x*y)). - _Robert Israel_, May 01 2016"
			],
			"example": [
				"First few rows of the triangle are:",
				"1;",
				"1, 1;",
				"2, 1, 1;",
				"3, 2, 1, 1;",
				"5, 3, 2, 1, 1;",
				"8, 5, 3, 2, 1, 1;",
				"...",
				"Production matrix begins:",
				"1, 1",
				"1, 0, 1",
				"0, 0, 0, 1",
				"0, 0, 0, 0, 1",
				"0, 0, 0, 0, 0, 1",
				"0, 0, 0, 0, 0, 0, 1",
				"0, 0, 0, 0, 0, 0, 0, 1",
				"... - _Philippe Deléham_, Oct 07 2014"
			],
			"maple": [
				"seq(seq(combinat:-fibonacci(n-i),i=0..n-1),n=1..20); # _Robert Israel_, May 01 2016"
			],
			"mathematica": [
				"r = N[(1 + Sqrt[5])/2, 100]; s = N[(1 - Sqrt[5])/2, 100];",
				"t = Table[Abs[Round[(r^n)*(s^k)/Sqrt[5]]], {n, 2, 15}, {k, 1, n - 1}]",
				"Flatten[t]",
				"TableForm[t]",
				"(* _Clark Kimberling_, May 01 2016 *)",
				"Table[Reverse[Fibonacci[Range[n]]],{n,15}]//Flatten (* _Harvey P. Dale_, Jan 28 2019 *)"
			],
			"xref": [
				"Cf. A000045, A000071, A271355 (analogous Lucas triangle).",
				"Companion triangle A104763, Fibonacci sequence in each row starting from the left. A121461."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "1,4",
			"author": "_Gary W. Adamson_, Mar 23 2005, Mar 05 2007",
			"ext": [
				"Edited by _N. J. A. Sloane_ at the suggestion of _Philippe Deléham_, Jun 11 2007",
				"More terms from _Philippe Deléham_, Apr 21 2009"
			],
			"references": 10,
			"revision": 39,
			"time": "2021-04-14T04:41:00-04:00",
			"created": "2005-04-09T03:00:00-04:00"
		}
	]
}