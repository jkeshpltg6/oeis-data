{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A094664",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 94664,
			"data": "1,1,2,7,38,286,2756,32299,444998,7038898,125620652,2495811814,54618201884,1305184303996,33812846036552,943878836768947,28242424937855558,901709392642750186,30597227032818965276,1099566630423067201234,41718229482624755005748",
			"name": "Row sums of triangle A094344.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A094664/b094664.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Sum_{k = 0..n} A094344(n, k).",
				"From _Gary W. Adamson_, Jul 26 2011: (Start)",
				"a(n) = upper left term in M^n, a(n+1) =  sum of top row terms in M^n; M = the following infinite square production matrix:",
				"  1, 1, 0, 0, 0, ...",
				"  1, 1, 3, 0, 0, ...",
				"  1, 1, 1, 5, 0, ...",
				"  1, 1, 1, 1, 7, ...",
				"  ... (End)",
				"G.f.: 1/(1 - x/(1 - x/(1 - 3*x/(1 - 3*x/(1 - 5*x/(1 - 5*x/(1 - 7*x/(1 - 7*x/(1-...))))))))) (continued fraction). - _Paul D. Hanna_, Sep 17 2011",
				"G.f.: 1/U(0) where U(k) = 1 - x*(2*k+1)/(1 - x*(2*k+1)/U(k+1)); (continued fraction, 2-step). - _Sergei N. Gladkovskii_, Oct 15 2012",
				"G.f. A(x) satisfies A(x) = 1 + x*(2*A(x)-A(x)^2) + 2*x^2*A'(x). - _Paul D. Hanna_, Mar 09 2013",
				"G.f.: 2 - 1/Q(0) where Q(k) = 1 - x*(2*k-1)/(1 - x*(2*k+3)/Q(k+1) ); (continued fraction). - _Sergei N. Gladkovskii_, Mar 19 2013",
				"G.f.: Q(0)/x - 1/x, where Q(k) = 1 - x*(2*k-1)/(1 - x*(2*k+1)/Q(k+1)); (continued fraction). - _Sergei N. Gladkovskii_, May 21 2013",
				"G.f.: 2/G(0), where G(k) = 1 + 1/(1 - x*(4*k+2)/(x*(4*k+2) - 1 + x*(4*k+2)/G(k+1))); (continued fraction). - _Sergei N. Gladkovskii_, May 31 2013",
				"G.f.: G(0)/2/x - 1/x + 2, where G(k) = 1 + 1/(1 - 2*x*(2*k+1)/(2*x*(2*k+1) - 1 + 2*x*(2*k-1)/G(k+1))); (continued fraction). - _Sergei N. Gladkovskii_, May 31 2013",
				"G.f.: G(0), where G(k) = 1 - x*(2*k+1)/(x*(2*k+1) - 1/(1 - x*(2*k+1)/(x*(2*k+1) - 1/G(k+1) ))); (continued fraction). - _Sergei N. Gladkovskii_, Aug 07 2013",
				"G.f.: 2 - 1/x - G(0)/x, where G(k) = 2*x - 2*x*k - 1 - x*(2*k-1)/G(k+1); (continued fraction). - _Sergei N. Gladkovskii_, Aug 14 2013",
				"a(n) ~ 2^n * (n-1)! / Pi. - _Vaclav Kotesovec_, Sep 05 2017"
			],
			"example": [
				"a(3) = 7, a(4) = 38, since top row of M^3 = (7, 7, 9, 15) with 38 = (7 + 7 + 9 + 15)."
			],
			"mathematica": [
				"nmax = 20; CoefficientList[Series[1/Fold[(1 - #2/#1) \u0026, 1, Reverse[(2*Range[nmax + 1] - 2*Floor[Range[nmax + 1]/2] - 1)*x]], {x, 0, nmax}], x] (* _Vaclav Kotesovec_, Sep 05 2017 *)"
			],
			"program": [
				"(PARI) {a(n)=local(CF=1+x*O(x^n)); for(k=0, n, CF=1/(1-(2*n-2*k+1)*x/(1-(2*n-2*k+1)*x*CF))); polcoeff(CF, n, x)} /* _Paul D. Hanna_ */",
				"(PARI) {a(n)=local(A=1+x);for(i=1,n,A=1+x*(2*A-A^2)+2*x^2*A'+x*O(x^n));polcoeff(A,n)} \\\\ _Paul D. Hanna_, Mar 09 2013"
			],
			"keyword": "easy,nonn",
			"offset": "0,3",
			"author": "_Philippe Deléham_, Jun 06 2004",
			"references": 5,
			"revision": 45,
			"time": "2021-06-05T16:41:19-04:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}