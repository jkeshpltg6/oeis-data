{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A220466",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 220466,
			"data": "1,3,2,10,3,7,4,36,5,11,6,26,7,15,8,136,9,19,10,42,11,23,12,100,13,27,14,58,15,31,16,528,17,35,18,74,19,39,20,164,21,43,22,90,23,47,24,392,25,51,26,106,27,55,28,228,29,59,30,122,31,63,32,2080,33,67,34,138,35",
			"name": "a((2*n-1)*2^p) = 4^p*(n-1) + 2^(p-1)*(1+2^p), p \u003e= 0 and n \u003e= 1.",
			"comment": [
				"The a(n) appeared in the analysis of A220002, a sequence related to the Catalan numbers.",
				"The first Maple program makes use of a program by Peter Luschny for the calculation of the a(n) values. The second Maple program shows that this sequence has a beautiful internal structure, see the first formula, while the third Maple program makes optimal use of this internal structure for the fast calculation of a(n) values for large n.",
				"The cross references lead to sequences that have the same internal structure as this sequence."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A220466/b220466.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Ralf Stephan, \u003ca href=\"https://oeis.org/somedcgf.html\"\u003eSome divide-and-conquer sequences with simple ordinary generating functions\u003c/a\u003e, The OEIS, Jan 01 2004."
			],
			"formula": [
				"a((2*n-1)*2^p) = 4^p*(n-1) + 2^(p-1)*(1+2^p), p \u003e= 0 and n \u003e= 1. Observe that a(2^p) = A007582(p).",
				"a(n) = ((n+1)/2)*(A060818(n)/A060818(n-1))",
				"a(n) = (-1/64)*(q(n+1)/q(n))/(2*n+1) with q(n) = (-1)^(n+1)*2^(4*n-5)*(2*n)!*A060818(n-1) or q(n) = (1/8)*A220002(n-1)*1/(A098597(2*n-1)/A046161(2*n))*1/(A008991(n-1)/A008992(n-1))",
				"Recurrence: a(2n) = 4a(n) - 2^A007814(n), a(2n+1) = n+1. - _Ralf Stephan_, Dec 17 2013"
			],
			"maple": [
				"# First Maple program",
				"a := n -\u003e 2^padic[ordp](n, 2)*(n+1)/2 : seq(a(n), n=1..69); # _Peter Luschny_, Dec 24 2012",
				"# Second Maple program",
				"nmax:=69: for p from 0 to ceil(simplify(log[2](nmax))) do for n from 1 to ceil(nmax/(p+2)) do a((2*n-1)*2^p) := 4^p*(n-1)  + 2^(p-1)*(1+2^p) od: od: seq(a(n), n=1..nmax);",
				"# Third Maple program",
				"nmax:=69: for p from 0 to ceil(simplify(log[2](nmax))) do n:=2^p: n1:=1: while n \u003c= nmax do a(n) := 4^p*(n1-1)+2^(p-1)*(1+2^p): n:=n+2^(p+1): n1:= n1+1: od: od:  seq(a(n), n=1..nmax);"
			],
			"mathematica": [
				"A220466 = Module[{n, p}, p = IntegerExponent[#, 2]; n = (#/2^p + 1)/2; 4^p*(n - 1) + 2^(p - 1)*(1 + 2^p)] \u0026; Array[A220466, 50] (* _JungHwan Min_, Aug 22 2016 *)"
			],
			"program": [
				"(PARI) a(n)=if(n%2,n\\2+1,4*a(n/2)-2^valuation(n/2,2)) \\\\ _Ralf Stephan_, Dec 17 2013",
				"(Haskell) -- Following Ralf Stephan's recurrence:",
				"import Data.List (transpose)",
				"a220466 n = a006519_list !! (n-1)",
				"a220466_list = 1 : concat",
				"   (transpose [zipWith (-) (map (* 4) a220466_list) a006519_list, [2..]])",
				"-- _Reinhard Zumkeller_, Aug 31 2014"
			],
			"xref": [
				"Cf. A000027 (the natural numbers), A000120 (1's-counting sequence), A000265 (remove 2's from n), A001316 (Gould's sequence), A001511 (the ruler function), A003484 (Hurwitz-Radon numbers), A003602 (a fractal sequence), A006519 (highest power of 2 dividing n), A007814 (binary carry sequence), A010060 (Thue-Morse sequence), A014577 (dragon curve), A014707 (dragon curve), A025480 (nim-values), A026741, A035263 (first Feigenbaum symbolic sequence), A037227, A038712, A048460, A048896, A051176, A053381 (smooth nowhere-zero vector fields), A055975 (Gray code related), A059134, A060789, A060819, A065916, A082392, A085296, A086799, A088837, A089265, A090739, A091512, A091519, A096268, A100892, A103391, A105321 (a fractal sequence), A109168 (a continued fraction), A117973, A129760, A151930, A153733, A160467, A162728, A181988, A182241, A191488 (a companion to Gould's sequence), A193365, A220466 (this sequence)."
			],
			"keyword": "nonn,easy,look",
			"offset": "1,2",
			"author": "_Johannes W. Meijer_, Dec 24 2012",
			"references": 40,
			"revision": 32,
			"time": "2020-04-29T07:42:46-04:00",
			"created": "2012-12-29T13:22:35-05:00"
		}
	]
}