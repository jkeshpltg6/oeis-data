{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A185296",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 185296,
			"data": "1,0,2,0,2,4,0,0,12,8,0,0,12,48,16,0,0,0,120,160,32,0,0,0,120,720,480,64,0,0,0,0,1680,3360,1344,128,0,0,0,0,1680,13440,13440,3584,256,0,0,0,0,0,30240,80640,48384,9216,512",
			"name": "Triangle of connection constants between the falling factorials (x)_(n) and (2*x)_(n).",
			"comment": [
				"The falling factorial polynomials (x)_n := x*(x-1)*...*(x-n+1), n = 0,1,2,..., form a basis for the space of polynomials. Hence the polynomial (2*x)_n may be expressed as a linear combination of x_0, x_1,...,x_n; the coefficients in the expansion form the n-th row of the table. Some examples are given below.",
				"This triangle is connected to two families of orthogonal polynomials, the Hermite polynomials H(n,x) A060821, and the Bessel polynomials y(n,x)A001498. The first few Hermite polynomials are",
				"... H(0,x) = 1",
				"... H(1,x) = 2*x",
				"... H(2,x) = -2+4*x^2",
				"... H(3,x) = -12*x+8*x^3",
				"... H(4,x) = 12-48*x^2+16*x^4.",
				"The unsigned coefficients of H(n,x) give the nonzero entries of the n-th row of the triangle.",
				"The Bessel polynomials y(n,x) begin",
				"... y(0,x) = 1",
				"... y(1,x) = 1+x",
				"... y(2,x) = 1+3*x+3*x^2",
				"... y(3,x) = 1+6*x+15*x^2+15*x^3.",
				"The entries in the n-th column of this triangle are the coefficients of the scaled Bessel polynomials 2^n*y(n,x).",
				"Also the Bell transform of g(n) = 2 if n\u003c2 else 0. For the definition of the Bell transform see A264428. - _Peter Luschny_, Jan 19 2016"
			],
			"reference": [
				"L. Comtet, Advanced Combinatorics, Reidel, 1974, page 158, exercise 7."
			],
			"formula": [
				"Defining relation: 2*x*(2*x-1)*...*(2*x-n+1) = sum {k=0..n} T(n, k)*x*(x-1)*...*(x-k+1)",
				"Explicit formula: T(n,k) = (n!/k!)*binomial(k,n-k)*2^(2*k-n). [As defined by Comtet (see reference).]",
				"Recurrence relation: T(n,k) = (2*k-n+1)*T(n-1,k)+2*T(n-1,k-1).",
				"E.g.f.: exp(x*(t^2+2*t)) = 1 + (2*x)*t + (2*x+4*x^2)*t^2/2! + (12*x^2+8*x^3)*t^3/3! + ...",
				"O.g.f. for m-th diagonal (starting at main diagonal m = 0): (2*m)!/m!*x^m/(1-2*x)^(2*m+1).",
				"The triangle is the matrix product [2^k*s(n,k)]n,k\u003e=0 * ([s(n,k)]n,k\u003e=0)^(-1),",
				"where s(n,k) denotes the signed Stirling number of the first kind.",
				"Row sums are [1,2,6,20,76,...] = A000898.",
				"Column sums are [1,4,28,296,...] = [2^n*A001515(n)] n\u003e=0."
			],
			"example": [
				"Triangle begins",
				"n\\k|...0.....1.....2.....3.....4.....5.....6",
				"============================================",
				"0..|...1",
				"1..|...0.....2",
				"2..|...0.....2.....4",
				"3..|...0.....0....12.....8",
				"4..|...0.....0....12....48....16",
				"5..|...0.....0.....0...120...160....32",
				"6..|...0.....0.....0...120...720...480....64",
				"..",
				"Row 3:",
				"(2*x)_3 = (2*x)*(2*x-1)*(2*x-2) = 8*x*(x-1)*(x-2) + 12*x*(x-1).",
				"Row 4:",
				"(2*x)_4 = (2*x)*(2*x-1)*(2*x-2)*(2*x-3) = 16*x*(x-1)*(x-2)*(x-3) +",
				"48*x*(x-1)*(x-2)+ 12*x*(x-1).",
				"Examples of recurrence relation",
				"T(4,4) = 5*T(3,4) + 2*T(3,3) = 5*0 + 2*8 = 16;",
				"T(5,4) = 4*T(4,4) + 2*T(4,3) = 4*16 + 2*48 = 160;",
				"T(6,4) = 3*T(5,4) + 2*T(5,3) = 3*160 + 2*120 = 720;",
				"T(7,4) = 2*T(6,4) + 2*T(6,3) = 2*720 + 2*120 = 1680."
			],
			"maple": [
				"T := (n,k) -\u003e (n!/k!)*binomial(k,n-k)*2^(2*k-n):",
				"seq(seq(T(n, k), k=0..n), n=0..9);"
			],
			"program": [
				"(Sage) # uses[bell_matrix from A264428]",
				"bell_matrix(lambda n: 2 if n\u003c2 else 0, 12) # _Peter Luschny_, Jan 19 2016"
			],
			"xref": [
				"Cf. A000898 (row sums), A001498, A001515, A059343, A060821."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "0,3",
			"author": "_Peter Bala_, Feb 20 2011",
			"references": 0,
			"revision": 34,
			"time": "2020-03-28T14:02:58-04:00",
			"created": "2011-01-25T22:43:09-05:00"
		}
	]
}