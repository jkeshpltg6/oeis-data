{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A329331",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 329331,
			"data": "0,0,0,0,1,0,0,2,2,0,0,3,8,3,0,0,4,10,10,4,0,0,5,16,9,16,5,0,0,6,18,20,20,18,6,0,0,7,24,23,32,23,24,7,0,0,8,26,30,36,36,30,26,8,0,0,9,64,29,48,33,48,29,64,9,0,0,10,66,72,52,54,54,52,72,66,10,0,0,11,72,75,128,51,40,51,128,75,72,11,0",
			"name": "Binary operation over the nonnegative integers, distributive over A003987(.,.), such that A(2^i, 2^j) = 2^A054237(i,j). Square array A(n,k), n \u003e= 0, k \u003e= 0, read by descending antidiagonals.",
			"comment": [
				"This sequence defines a multiplication operation that goes with bitwise exclusive-or (A003987) as addition operation to form a ring over the nonnegative integers. It is isomorphic to the polynomial ring GF(2)[x,y], as is the ring defined in A329329.",
				"The ring defined by A329329 is unusual in that it has A059897(.,.) as its addition operation, given that A059897 has more similarities to integer multiplication. A003987, which is isomorphic to A059897 as a binary operation, seems a more standard choice for an addition operator.",
				"However, as explained in A329329, A059897 has a natural choice for mapping a generating set to the 2-dimensions (x and y) of the generating set for the additive group of GF(2)[x,y]. Instead, A003987 needs a pairing function to map its most natural generating set {2^k: k \u003e= 0} onto {x^i * y^j: i \u003e= 0, j \u003e= 0}.",
				"The choice made here was to map 2^k onto the 2 dimensions of x^i * y^j, by proceeding through x and y dimensions as when reading an array by antidiagonals. 2^0 = 1 is mapped to (x^0 * y*0) = 1, 2^1 = 2 is mapped to (x^1 * y^0) = x, 2^2 = 4 to (x^0 * y^1) = y, 8 to (x^2 * y^0) = x^2, and so on, 16 mapped to xy, 32 to y^2, 64 to x^3, etc. With this mapping, it can be shown that the result of the multiplying the polynomial images of 2^i and 2^j is the image of 2^A054237(i,j)."
			],
			"link": [
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Distributive.html\"\u003eDistributive\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PairingFunction.html\"\u003ePairing Function\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Ring.html\"\u003eRing\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Generating_set_of_a_group\"\u003eGenerating set of a group\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Polynomial_ring\"\u003ePolynomial ring\u003c/a\u003e"
			],
			"formula": [
				"A(2^i, 2^j) = 2^A054237(i,j).",
				"A(A003987(n,m), k) = A003987(A(n,k), A(m,k)).",
				"A(n, A003987(m,k)) = A003987(A(n,m), A(n,k)).",
				"Derived formulas:(Start)",
				"A(n,k) = A(k,n).",
				"A(n,0) = A(0,k) = 0.",
				"A(n,1) = A(1,n) = n.",
				"A(n, A(m,k)) = A(A(n,m), k).",
				"(End)"
			],
			"example": [
				"Square array A(n,k) begins:",
				"  n\\k |   0     1     2     3     4     5     6     7     8     9    10",
				"  ----+----------------------------------------------------------------",
				"    0 |   0     0     0     0     0     0     0     0     0     0     0",
				"    1 |   0     1     2     3     4     5     6     7     8     9    10",
				"    2 |   0     2     8    10    16    18    24    26    64    66    72",
				"    3 |   0     3    10     9    20    23    30    29    72    75    66",
				"    4 |   0     4    16    20    32    36    48    52   128   132   144",
				"    5 |   0     5    18    23    36    33    54    51   136   141   154",
				"    6 |   0     6    24    30    48    54    40    46   192   198   216",
				"    7 |   0     7    26    29    52    51    46    41   200   207   210",
				"    8 |   0     8    64    72   128   136   192   200  1024  1032  1088",
				"    9 |   0     9    66    75   132   141   198   207  1032  1025  1098",
				"   10 |   0    10    72    66   144   154   216   210  1088  1098  1032"
			],
			"xref": [
				"Cf. A003987, A054237, A059897, A329329."
			],
			"keyword": "nonn,tabl",
			"offset": "0,8",
			"author": "_Peter Munn_, Nov 10 2019",
			"references": 1,
			"revision": 22,
			"time": "2020-03-25T14:14:36-04:00",
			"created": "2020-03-25T14:14:36-04:00"
		}
	]
}