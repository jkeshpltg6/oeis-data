{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A023001",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 23001,
			"data": "0,1,9,73,585,4681,37449,299593,2396745,19173961,153391689,1227133513,9817068105,78536544841,628292358729,5026338869833,40210710958665,321685687669321,2573485501354569,20587884010836553,164703072086692425",
			"name": "a(n) = (8^n - 1)/7.",
			"comment": [
				"Gives the (zero-based) positions of odd terms in A007556 (numbers n such that A007556(a(n)) mod 2 = 1). - _Farideh Firoozbakht_, Jun 13 2003",
				"{1, 9, 73, 585, 4681, ...} is the binomial transform of A003950. - _Philippe Deléham_, Jul 22 2005",
				"Let A be the Hessenberg matrix of order n, defined by: A[1,j]=1, A[i,i]:=8, (i\u003e1), A[i,i-1]=-1, and A[i,j]=0 otherwise. Then, for n \u003e= 1, a(n) = det(A). - _Milan Janjic_, Feb 21 2010",
				"Let A be the Hessenberg matrix of order n, defined by: A[1,j]=1, A[i,i]:=9, (i\u003e1), A[i,i-1]=-1, and A[i,j]=0 otherwise. Then, for n \u003e= 1, a(n-1) = (-1)^n*charpoly(A,1). - _Milan Janjic_, Feb 21 2010",
				"This is the sequence A(0,1;7,8;2) = A(0,1;8,0;1) of the family of sequences [a,b:c,d:k] considered by G. Detlefs, and treated as A(a,b;c,d;k) in the W. Lang link given below. - _Wolfdieter Lang_, Oct 18 2010",
				"a(n) is the total number of squares the carpetmaker has removed after the n-th step of a Sierpiński carpet production. - _Ivan N. Ianakiev_, Oct 22 2013",
				"For n \u003e= 1, a(n) is the total number of holes in a box fractal (start with 8 boxes, 1 hole) after n iterations. See illustration in link. - _Kival Ngaokrajang_, Jan 27 2015",
				"From _Bernard Schott_, May 01 2017: (Start)",
				"Except for 0, 1 and 73, all the terms are composite because a(n) = ((2^n - 1) * (4^n + 2^n + 1))/7.",
				"For n \u003e= 3, all terms are Brazilian repunits numbers in base 8, and so belong to A125134.",
				"a(3) = 73 is the only Brazilian prime in base 8, and so it belongs to A085104 and A285017. (End)"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A023001/b023001.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"A. Abdurrahman, \u003ca href=\"https://arxiv.org/abs/1909.10889\"\u003eCM Method and Expansion of Numbers\u003c/a\u003e, arXiv:1909.10889 [math.NT], 2019.",
				"Roger B. Eggleton, \u003ca href=\"http://dx.doi.org/10.1155/2015/216475\"\u003eMaximal Midpoint-Free Subsets of Integers\u003c/a\u003e, International Journal of Combinatorics Volume 2015, Article ID 216475, 14 pages.",
				"Wolfdieter Lang, \u003ca href=\"/A023001/a023001.pdf\"\u003eNotes on certain inhomogeneous three term recurrences.\u003c/a\u003e - _Wolfdieter Lang_, Oct 18 2010",
				"Kival Ngaokrajang, \u003ca href=\"/A023001/a023001_1.pdf\"\u003eIllustration of initial terms\u003c/a\u003e",
				"Quynh Nguyen, Jean Pedersen, and Hien T. Vu, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL19/Pedersen/pedersen2.html\"\u003eNew Integer Sequences Arising From 3-Period Folding Numbers\u003c/a\u003e, Vol. 19 (2016), Article 16.3.1. See Table 1.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Repunit.html\"\u003eRepunit.\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (9,-8)."
			],
			"formula": [
				"Also sum of cubes of divisors of 2^(n-1): a(n) = A001158(A000079(n-1)). - _Labos Elemer_, Apr 10 2003 and _Farideh Firoozbakht_, Jun 13 2003",
				"a(n) = A033138(3n-2). - _Alexandre Wajnberg_, May 31 2005",
				"From _Philippe Deléham_, Oct 12 2006: (Start)",
				"a(0) = 0, a(n) = 8*a(n-1) + 1 for n\u003e0.",
				"G.f.: x/((1-8x)*(1-x)). (End)",
				"From _Wolfdieter Lang_, Oct 18 2010: (Start)",
				"a(n) = 7*a(n-1) + 8*a(n-2) + 2, a(0)=0, a(1)=1.",
				"a(n) = 8*a(n-1) + a(n-2) - 8*a(n-3) = 9*a(n-1) - 8*a(n-2), a(0)=0, a(1)=1, a(2)=9. Observation by _Gary Detlefs_. See the W. Lang comment and link. (End)",
				"a(n) = Sum_{k=0..n-1} 8^k. - _Doug Bell_, May 26 2017"
			],
			"example": [
				"From _Zerinvary Lajos_, Jan 14 2007: (Start)",
				"Octal.............decimal",
				"0....................0",
				"1....................1",
				"11...................9",
				"111.................73",
				"1111...............585",
				"11111.............4681",
				"111111...........37449",
				"1111111.........299593",
				"11111111.......2396745",
				"111111111.....19173961",
				"1111111111...153391689",
				"etc. ...............etc. (End)",
				"a(4) = (8^4 - 1)/7 = 585 = 1111_8 = {(2^4 - 1) * (4^4 + 2^4 + 1) /7 = 15 * 273/7 = 15 * 39. - _Bernard Schott_, May 01 2017"
			],
			"maple": [
				"a:=n-\u003esum(8^(n-j),j=1..n): seq(a(n), n=0..20); # _Zerinvary Lajos_, Jan 04 2007"
			],
			"mathematica": [
				"Table[(8^n-1)/7, {n, 0, m}]",
				"LinearRecurrence[{9,-8},{0,1},30] (* _Harvey P. Dale_, Feb 12 2015 *)"
			],
			"program": [
				"(Sage) [lucas_number1(n,9,8) for n in range(0, 21)] # _Zerinvary Lajos_, Apr 23 2009",
				"(Sage) [gaussian_binomial(n,1,8) for n in range(0,21)] # _Zerinvary Lajos_, May 28 2009",
				"(MAGMA) [(8^n-1)/7: n in [0..20]]; // _Vincenzo Librandi_, Sep 17 2011",
				"(Maxima) A023001(n):=floor((8^n-1)/7)$",
				"makelist(A023001(n),n,0,30); /* _Martin Ettl_, Nov 05 2012 */",
				"(PARI) a(n)=(8^n-1)/7 \\\\ _Charles R Greathouse IV_, Mar 22 2016",
				"(GAP)",
				"A023001:=List([0..10^2],n-\u003e(8^n-1)/7); # _Muniru A Asiru_, Oct 03 2017"
			],
			"xref": [
				"Cf. A007556, A003950, A001158, A033138.",
				"Cf. A125134, A085104, A285017, A220571, A053696."
			],
			"keyword": "easy,nonn",
			"offset": "0,3",
			"author": "_David W. Wilson_",
			"references": 77,
			"revision": 123,
			"time": "2019-12-07T12:18:21-05:00",
			"created": "1998-06-14T03:00:00-04:00"
		}
	]
}