{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A340064",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 340064,
			"data": "3,2,4,6,8,10,12,14,16,18,20,9,22,24,26,28,19,21,30,32,34,36,38,40,42,43,44,46,48,50,52,54,63,73,56,58,60,62,64,66,68,70,72,101,74,76,78,80,82,84,86,88,90,92,94,96,98,100,102,131,104,106,108,110,112,114,116,118,120,122,124,141,126,128,153",
			"name": "Every odd term k of the sequence is the cumulative sum of the prime digits used so far (the digits of k are included in the sum).",
			"comment": [
				"This is the lexicographically earliest sequence of distinct positive terms with this property. The prime digits are 2, 3, 5 and 7.",
				"The sequence is first extended with the smallest odd term not leading to a contradiction; if no such term exists, the sequence is extended with the smallest even term not yet present."
			],
			"example": [
				"Not a(1) = 1 as this 1, being odd, should be the sum of the prime digits so far -- which is wrong (there are none);",
				"not a(1) = 2 as a(1) = 3 is odd and possible here;",
				"a(12) = 9 as 9 is odd and the sum of the prime digits 3 + 2 + 2 + 2;",
				"a(13) = 22 as 22 is the smallest even term available;",
				"a(17) = 19 as 19 = 3 + 2 + 2 + 2 + 2 + 2 + 2 + 2 + 2;",
				"a(18) = 21 as 21 is the sum of 19 + 2 (the first digit of 21 itself); etc."
			],
			"program": [
				"(Python)",
				"def pds(k): return sum(int(d) for d in str(k) if d in \"2357\")",
				"def aupto(nn):",
				"  aset, alst, primesum, nexteven = set(), [], 0, 2",
				"  for n in range(1, nn):",
				"    k = 1",
				"    found = False",
				"    while not found:",
				"      while k in aset: k += 2",
				"      if k == primesum + pds(k): found = True; break",
				"      if k \u003e primesum + 7 * len(str(k)): break",
				"      k += 2",
				"    if found: ak = k",
				"    else: ak = nexteven; nexteven += 2",
				"    aset.add(ak); alst.append(ak); primesum += pds(ak)",
				"  return alst",
				"print(aupto(76)) # _Michael S. Branicky_, Dec 29 2020"
			],
			"xref": [
				"CF. A338924"
			],
			"keyword": "base,nonn",
			"offset": "1,1",
			"author": "_Eric Angelini_ and _Carole Dubois_, Dec 28 2020",
			"references": 0,
			"revision": 12,
			"time": "2021-01-01T12:16:59-05:00",
			"created": "2021-01-01T12:16:59-05:00"
		}
	]
}