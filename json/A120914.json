{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A120914",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 120914,
			"data": "1,4,4,4,20,36,32,16,20,116,256,288,212,144,80,116,720,1776,2388,2144,1504,1012,784,464,720,4656,12372,18800,19632,15604,10848,7648,5712,4736,2880,4656,30996,86912,144320,169332,151792,113456,79696,58176",
			"name": "Cascadence of (1+2x)^2; a triangle, read by rows of 2n+1 terms, that retains its original form upon convolving each row with [1,4,4] and then letting excess terms spill over from each row into the initial positions of the next row such that only 2n+1 terms remain in row n for n\u003e=0.",
			"comment": [
				"More generally, the cascadence of polynomial F(x) of degree d, F(0)=1, is a triangle with d*n+1 terms in row n where the g.f. of the triangle, A(x,y), is given by: A(x,y) = ( x*H(x) - y*H(x*y^d) )/( x*F(y) - y ), where H(x) satisfies: H(x) = G*H(x*G^d)/x and G satisfies: G = x*F(G) so that G = series_reversion(x/F(x)); also, H(x) is the g.f. of column 0."
			],
			"formula": [
				"G.f.: A(x,y) = ( x*H(x) - y*H(x*y^2) )/( x*(1+2y)^2 - y ), where H(x) satisfies: H(x) = G*H(x*G^2)/x and G satisfies: G = x*(1 + 2G)^2 ; also, H(x) is the g.f. of column 0."
			],
			"example": [
				"Triangle begins:",
				"1;",
				"4, 4, 4;",
				"20, 36, 32, 16, 20;",
				"116, 256, 288, 212, 144, 80, 116;",
				"720, 1776, 2388, 2144, 1504, 1012, 784, 464, 720;",
				"4656, 12372, 18800, 19632, 15604, 10848, 7648, 5712, 4736, 2880, 4656;",
				"Convolution of [1,4,4] with each row produces:",
				"[1,4,4]*[1] = [1,4,4];",
				"[1,4,4]*[4,4,4] = [4,20,36,32,16];",
				"[1,4,4]*[20,36,32,16,20] = [20,116,256,288,212,144,80];",
				"[1,4,4]*[116,256,288,212,144,80,116] =",
				"[116,720,1776,2388,2144,1504,1012,784,464];",
				"These convoluted rows, when concatenated, yield the sequence:",
				"1,4,4, 4,20,36,32,16, 20,116,256,288,212,144,80, 116,720,1776,2388,...",
				"which equals the concatenated rows of this original triangle:",
				"1, 4,4,4, 20,36,32,16,20, 116,256,288,212,144,80,116, 720,1776,2388,..."
			],
			"program": [
				"(PARI) /* Generate Triangle by the Recurrence: */",
				"{T(n,k)=if(2*n\u003ck || k\u003c0,0,if(n==0 \u0026\u0026 k==0,1,if(k==0,T(n-1,1)+4*T(n-1,0), if(k==2*n,T(n,0),T(n-1,k+1)+4*T(n-1,k)+4*T(n-1,k-1)))))}",
				"for(n=0,10,for(k=0,n,print1(T(n,k),\", \"));print(\"\"))",
				"(PARI) /* Generate Triangle by the G.F.: */",
				"{T(n,k)=local(A,F=1+4*x+4*x^2,d=2,G=x,H=1+x,S=ceil(log(n+1)/log(d+1))); for(i=0,n,G=x*subst(F,x,G+x*O(x^n)));for(i=0,S,H=subst(H,x,x*G^d+x*O(x^n))*G/x); A=(x*H-y*subst(H,x,x*y^d +x*O(x^n)))/(x*subst(F,x,y)-y); polcoeff(polcoeff(A,n,x),k,y)}",
				"for(n=0,10,for(k=0,n,print1(T(n,k),\", \"));print(\"\"))"
			],
			"xref": [
				"Cf. A120915 (column 0), A120917 (central terms), A120918 (row sums), A000108 (Catalan); variants: A092683, A092686, A120894, A120898, A120919."
			],
			"keyword": "nonn,tabf",
			"offset": "0,2",
			"author": "_Paul D. Hanna_, Jul 17 2006",
			"references": 4,
			"revision": 11,
			"time": "2017-06-13T23:48:28-04:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}