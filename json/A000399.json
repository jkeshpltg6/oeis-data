{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000399",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 399,
			"id": "M4218 N1762",
			"data": "1,6,35,225,1624,13132,118124,1172700,12753576,150917976,1931559552,26596717056,392156797824,6165817614720,102992244837120,1821602444624640,34012249593822720,668609730341153280,13803759753640704000",
			"name": "Unsigned Stirling numbers of first kind s(n,3).",
			"comment": [
				"Number of permutations of n elements with exactly 3 cycles.",
				"The asymptotic expansion of the higher order exponential integral E(x,m=3,n=1) ~ exp(-x)/x^3*(1 - 6/x + 35/x^2 - 225/x^3 + 1624/x^4 - 13132/x^5 + ...) leads to the sequence given above. See A163931 and A163932 for more information. - _Johannes W. Meijer_, Oct 20 2009"
			],
			"reference": [
				"M. Abramowitz and I. A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, 1964 (and various reprintings), p. 833.",
				"L. Comtet, Advanced Combinatorics, Reidel, 1974, p. 217.",
				"F. N. David, M. G. Kendall and D. E. Barton, Symmetric Function and Allied Tables, Cambridge, 1966, p. 226.",
				"Shanzhen Gao, Permutations with Restricted Structure (in preparation). - _Shanzhen Gao_, Sep 14 2010",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe and Robert Israel, \u003ca href=\"/A000399/b000399.txt\"\u003eTable of n, a(n) for n = 3..412\u003c/a\u003e (3..100 from T. D. Noe)",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=32\"\u003eEncyclopedia of Combinatorial Structures 32\u003c/a\u003e.",
				"Sergey Kitaev and Jeffrey Remmel, \u003ca href=\"http://arxiv.org/abs/1201.1323\"\u003eSimple marked mesh patterns\u003c/a\u003e, arXiv:1201.1323 [math.CO], 2012.",
				"Sergey Kitaev and Jeffrey Remmel, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL15/Kitaev/kitaev5.html\"\u003eQuadrant Marked Mesh Patterns\u003c/a\u003e, J. Int. Seq. 15 (2012), #12.4.7.",
				"M. O'Keeffe, \u003ca href=\"/A008527/a008527.pdf\"\u003eCoordination sequences for lattices\u003c/a\u003e, Zeit. f. Krist., 210 (1995), 905-908. [Annotated scanned copy]"
			],
			"formula": [
				"Let P(n-1,X) = (X+1)(X+2)(X+3)...(X+n-1); then a(n) is the coefficient of X^2; or a(n) = P''(n-1,0)/2!. - _Benoit Cloitre_, May 09 2002 [Edited by _Petros Hadjicostas_, Jun 29 2020 to agree with the offset 3]",
				"E.g.f.: -log(1-x)^3/3!.",
				"a(n) is the coefficient of x^(n+3) in (-log(1-x))^3, multiplied by (n+3)!/6.",
				"a(n) = ((Sum_{i=1..n-1} 1/i)^2 - Sum_{i=1..n-1} 1/i^2)*(n-1)!/2 for n \u003e= 3. - Klaus Strassburger (strass(AT)ddfi.uni-duesseldorf.de), Jan 18 2000",
				"a(n) = det(|S(i+3,j+2)|, 1 \u003c= i,j \u003c= n-3), where S(n,k) are Stirling numbers of the second kind. - _Mircea Merca_, Apr 06 2013",
				"a(n) = Gamma(n)*(HarmonicNumber(n-1)^2 + Zeta(2,n) - Zeta(2))/2. - _Gerry Martens_, Jul 05 2015",
				"From _Petros Hadjicostas_, Jun 28 2020: (Start)",
				"a(n) = (n-3)! + (2*n-3)*a(n-1) - (n-2)^2*a(n-2) for n \u003e= 5.",
				"a(n) = 3*(n-2)*a(n-1) - (3*n^2-15*n+19)*a(n-2) + (n-3)^3*a(n-3) for n \u003e= 6. (End)"
			],
			"example": [
				"(-log(1-x))^3 = x^3 + 3/2*x^4 + 7/4*x^5 + 15/8*x^6 + ..."
			],
			"maple": [
				"seq(abs(Stirling1(n,3)),n=3..30); # _Robert Israel_, Jul 05 2015"
			],
			"mathematica": [
				"a=Log[1/(1-x)];Range[0,20]! CoefficientList[Series[a^3/3!,{x,0,20}],x]",
				"f[n_] := Abs@ StirlingS1[n, 3]; Array[f, 19, 3]",
				"Abs[StirlingS1[Range[3,30],3]] (* _Harvey P. Dale_, Jun 23 2014 *)",
				"f[n_] := Gamma[n]*(HarmonicNumber[n - 1]^2 + Zeta[2, n] - Zeta[2])/2; Array[f, 19, 3] (* _Robert G. Wilson v_, Jul 05 2015 *)"
			],
			"program": [
				"(MuPAD) f := proc(n) option remember; begin n^3*f(n-3)-(3*n^2+3*n+1)*f(n-2)+3*(n+1)*f(n-1) end_proc: f(0) := 1: f(1) := 6: f(2) := 35:",
				"(PARI) for(n=2,50,print1(polcoeff(prod(i=1,n,x+i),2,x),\",\"))",
				"(Sage) [stirling_number1(i+2,3) for i in range(1,22)] # _Zerinvary Lajos_, Jun 27 2008",
				"(MAGMA) A000399:=func\u003c n | Abs(StirlingFirst(n, 3)) \u003e; [ A000399(n): n in [3..25] ]; // _Klaus Brockhaus_, Jan 14 2011"
			],
			"xref": [
				"Cf. A000254, A000454, A000482, A001233, A001234, A008275, A243569, A243570."
			],
			"keyword": "nonn,easy,nice",
			"offset": "3,2",
			"author": "_N. J. A. Sloane_",
			"references": 29,
			"revision": 94,
			"time": "2020-06-29T22:18:01-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}