{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A224823",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 224823,
			"data": "1,2,1,3,4,1,5,4,0,6,6,3,5,6,2,6,8,0,7,8,4,9,6,1,11,10,0,8,6,5,9,12,3,7,14,0,11,8,5,13,10,4,8,8,0,14,16,5,11,12,1,16,10,0,14,14,7,9,12,5,14,14,0,7,16,7,18,14,4,19,10,0,12,16,9,13,20,0",
			"name": "Number of solutions to n = x + y + 3*z where x, y, z are triangular numbers.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"a(A224829(n)) = 0. - _Reinhard Zumkeller_, Jul 21 2013"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A224823/b224823.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of psi(x)^2 * psi(x^3) in powers of x where psi() is a Ramanujan theta function.",
				"Expansion of q^(-5/8) * eta(q^2)^4 * eta(q^6)^2 / (eta(q)^2 * eta(q^3)) in powers of q.",
				"Euler transform of period 6 sequence [ 2, -2, 3, -2, 2, -3, ...].",
				"G.f.: (Sum_{k\u003e0} x^((k^2-k)/2))^2 * (Sum_{k\u003e0} x^(3 * (k^2-k)/2)).",
				"-2 * a(n) = A227595(3*n + 1)."
			],
			"example": [
				"G.f. = 1 + 2*x + x^2 + 3*x^3 + 4*x^4 + x^5 + 5*x^6 + 4*x^7 + 6*x^9 + 6*x^10 + ...",
				"G.f. = q^5 + 2*q^13 + q^21 + 3*q^29 + 4*q^37 + q^45 + 5*q^53 + 4*q^61 + 6*q^77 + ...",
				"a(3) = 3 since 3 = 0 + 0 + 3*1 = 0 + 3 + 3*0 = 3 + 0 + 3*0 are the 3 solutions of 3 = x + y + 3*z in triangular numbers.",
				"a(4) = 4 since 4 = 1 + 0 + 3*1 = 0 + 1 + 3*1 = 3 + 1 + 3*0 = 1 + 3 + 3*0 are the 4 solutions of 4 = x + y + 3*z in triangular numbers."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 2, 0, x^(1/2)]^2 EllipticTheta[ 2, 0, x^(3/2)] / (8 x^(5/8)), {x, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = local(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A)^4 * eta(x^6 + A)^2 / (eta(x + A)^2 * eta(x^3 + A)), n))};",
				"(Haskell)",
				"a224823 n = length [() | let ts = takeWhile (\u003c= n) a000217_list,",
				"            x \u003c- ts, y \u003c- ts, z \u003c- takeWhile (\u003c= div (n - x - y) 3) ts,",
				"            x + y + 3 * z == n]",
				"-- _Reinhard Zumkeller_, Jul 21 2013"
			],
			"xref": [
				"Cf. A227595.",
				"Cf. A000217."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Michael Somos_, Jul 20 2013",
			"references": 5,
			"revision": 17,
			"time": "2021-03-12T22:24:46-05:00",
			"created": "2013-07-21T02:21:34-04:00"
		}
	]
}