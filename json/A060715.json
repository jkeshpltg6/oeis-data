{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A060715",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 60715,
			"data": "0,1,1,2,1,2,2,2,3,4,3,4,3,3,4,5,4,4,4,4,5,6,5,6,6,6,7,7,6,7,7,7,7,8,8,9,9,9,9,10,9,10,9,9,10,10,9,9,10,10,11,12,11,12,13,13,14,14,13,13,12,12,12,13,13,14,13,13,14,15,14,14,13,13,14,15,15",
			"name": "Number of primes between n and 2n exclusive.",
			"comment": [
				"See the additional references and links mentioned in A143227. - _Jonathan Sondow_, Aug 03 2008",
				"a(A060756(n)) = n and a(m) \u003c\u003e n for m \u003c A060756(n). - _Reinhard Zumkeller_, Jan 08 2012",
				"For prime n conjecturally a(n) = A226859(n). - _Vladimir Shevelev_, Jun 27 2013",
				"The number of partitions of 2n+2 into exactly two parts where the first part is a prime strictly less than 2n+1. - _Wesley Ivan Hurt_, Aug 21 2013"
			],
			"reference": [
				"M. Aigner and C. M. Ziegler, Proofs from The Book, Chapter 2, Springer NY 2001."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A060715/b060715.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"C. K. Caldwell, The Prime Glossary, \u003ca href=\"http://primes.utm.edu/glossary/page.php/Bertrandspostulate.html\"\u003eBertrand's postulate\u003c/a\u003e",
				"R. Chapman, \u003ca href=\"http://www.maths.ex.ac.uk/~rjc/etc/bertrand.pdf\"\u003eBertrand postulate\u003c/a\u003e  [Broken link]",
				"Math Olympiads, \u003ca href=\"http://matholymp.com/TUTORIALS/Bertrand.pdf\"\u003eBertrand's Postulate\u003c/a\u003e  [Broken link]",
				"S. Ramanujan, \u003ca href=\"http://ramanujan.sirinudi.org/Volumes/published/ram24.html\"\u003eA proof of Bertrand's postulate\u003c/a\u003e, J. Indian Math. Soc., 11 (1919), 181-182.",
				"V. Shevelev, \u003ca href=\"http://arxiv.org/abs/0909.0715\"\u003eRamanujan and Labos Primes, Their Generalizations, and Classifications of Primes\u003c/a\u003e, arXiv:0909.0715v13 [math.NT]",
				"V. Shevelev, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL15/Shevelev/shevelev19.html\"\u003eRamanujan and Labos Primes, Their Generalizations, and Classifications of Primes\u003c/a\u003e, Journal of Integer Sequences, Vol. 15 (2012), Article 12.5.4",
				"M. Slone, PlanetMath.org, \u003ca href=\"http://planetmath.org/encyclopedia/ProofOfBertrandsConjecture.html\"\u003eProof of Bertrand's conjecture\u003c/a\u003e",
				"J. Sondow and Eric Weisstein, \u003ca href=\"http://mathworld.wolfram.com/BertrandsPostulate.html\"\u003eBertrand's Postulate\u003c/a\u003e, World of Mathematics",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Proof_of_Bertrand%27s_postulate\"\u003eProof of Bertrand's postulate\u003c/a\u003e",
				"Dr. Wilkinson, The Math Forum, \u003ca href=\"http://mathforum.org/library/drmath/view/51527.html\"\u003eErdos' Proof\u003c/a\u003e",
				"Wolfram Research, \u003ca href=\"http://functions.wolfram.com/NumberTheoryFunctions/Prime/31/03/ShowAll.html\"\u003eBertrand hypothesis\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Sum_{k=1..n-1} A010051(n+k). - _Reinhard Zumkeller_, Dec 03 2009",
				"a(n) = pi(2n-1) - pi(n). - _Wesley Ivan Hurt_, Aug 21 2013",
				"a(n) = Sum_{k=(n^2-n+2)/2..(n^2+n-2)/2} A010051(A128076(k)). - _Wesley Ivan Hurt_, Jan 08 2022"
			],
			"example": [
				"a(35)=8 since eight consecutive primes (37,41,43,47,53,59,61,67) are located between 35 and 70."
			],
			"maple": [
				"a := proc(n) local counter, i; counter := 0; from i from n+1 to 2*n-1 do if isprime(i) then counter := counter +1; fi; od; return counter; end:",
				"with(numtheory); seq(pi(2*k-1)-pi(k),k=1..100); # _Wesley Ivan Hurt_, Aug 21 2013"
			],
			"mathematica": [
				"a[n_]:=PrimePi[2n-1]-PrimePi[n]; Table[a[n],{n,1,84}] (* _Jean-François Alcover_, Mar 20 2011 *)"
			],
			"program": [
				"(PARI) { for (n=1, 1000, write(\"b060715.txt\", n, \" \", primepi(2*n - 1) - primepi(n)); ) } \\\\ _Harry J. Smith_, Jul 10 2009",
				"(Haskell)",
				"a060715 n = sum $ map a010051 [n+1..2*n-1]  -- _Reinhard Zumkeller_, Jan 08 2012",
				"(MAGMA) [0] cat [#PrimesInInterval(n+1, 2*n-1): n in [2..80]]; // _Bruno Berselli_, Sep 05 2012"
			],
			"xref": [
				"Cf. A060756, A070046, A006992, A051501, A035250, A101909.",
				"Cf. A000720, A014085, A104272, A143223..A143227, A128076."
			],
			"keyword": "nonn,easy,changed",
			"offset": "1,4",
			"author": "_Lekraj Beedassy_, Apr 25 2001",
			"ext": [
				"Corrected by Dug Eichelberger (dug(AT)mit.edu), Jun 04 2001",
				"More terms from Larry Reeves (larryr(AT)acm.org), Jun 05 2001"
			],
			"references": 44,
			"revision": 71,
			"time": "2022-01-08T22:43:59-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}