{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A048724",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 48724,
			"data": "0,3,6,5,12,15,10,9,24,27,30,29,20,23,18,17,48,51,54,53,60,63,58,57,40,43,46,45,36,39,34,33,96,99,102,101,108,111,106,105,120,123,126,125,116,119,114,113,80,83,86,85,92,95,90,89,72,75,78,77,68,71,66,65,192",
			"name": "Write n and 2n in binary and add them mod 2.",
			"comment": [
				"Reversing binary representation of -n. Converting sum of powers of 2 in binary representation of a(n) to alternating sum gives -n. Note that the alternation is applied only to the nonzero bits and does not depend on the exponent of two. All integers have a unique reversing binary representation (see cited exercise for proof). Complement of A065621. - _Marc LeBrun_, Nov 07 2001",
				"A permutation of the \"evil\" numbers A001969. - _Marc LeBrun_, Nov 07 2001",
				"A048725(n) = a(a(n)). - _Reinhard Zumkeller_, Nov 12 2004"
			],
			"reference": [
				"D. E. Knuth, The Art of Computer Programming. Addison-Wesley, Reading, MA, 1969, Vol. 2, p. 178, (exercise 4.1. Nr. 27)"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A048724/b048724.txt\"\u003eTable of n, a(n) for n = 0..1023\u003c/a\u003e",
				"H. D. Nguyen, \u003ca href=\"http://www.rowan.edu/colleges/csm/departments/math/facultystaff/nguyen/papers/mixing-ptm-rademacher.pdf\"\u003eA mixing of Prouhet-Thue-Morse sequences and Rademacher functions\u003c/a\u003e, 2014. See Example 20. - _N. J. A. Sloane_, May 24 2014",
				"R. Stephan, \u003ca href=\"/somedcgf.html\"\u003eSome divide-and-conquer sequences ...\u003c/a\u003e",
				"R. Stephan, \u003ca href=\"/A079944/a079944.ps\"\u003eTable of generating functions\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Xmult(n, 3) (or n XOR (n\u003c\u003c1)).",
				"a(n) = A065621(-n).",
				"a(2n) = 2a(n), a(2n+1) = 2a(n) + 2(-1)^n + 1.",
				"G.f. 1/(1-x) * sum(k\u003e=0, 2^k*(3t-t^3)/(1+t)/(1+t^2), t=x^2^k). - _Ralf Stephan_, Sep 08 2003",
				"a(n) = sum(k=0, n, (1-(-1)^round(+n/2^k))/2*2^k). - _Benoit Cloitre_, Apr 27 2005",
				"a(n) = A001969(A003188(n)). - _Philippe Deléham_, Apr 29 2005",
				"a(n) = A106409(2*n) for n\u003e0. - _Reinhard Zumkeller_, May 02 2005",
				"a(n) = A142149(2*n). - _Reinhard Zumkeller_, Jul 15 2008"
			],
			"example": [
				"12 = 1100 in binary, 24=11000 and their sum is 10100=20, so a(12)=20.",
				"a(4) = 12 = + 8 + 4 -\u003e - 8 + 4 = -4."
			],
			"maple": [
				"a:= n-\u003e Bits[Xor](n, n+n):",
				"seq(a(n), n=0..100);  # _Alois P. Heinz_, Apr 06 2016"
			],
			"mathematica": [
				"Table[ BitXor[2n, n], {n, 0, 65}] (* _Robert G. Wilson v_, Jul 06 2006 *)"
			],
			"program": [
				"(PARI) a(n)=bitxor(n,2*n) \\\\ _Charles R Greathouse IV_, Jan 04 2013",
				"(Haskell)",
				"import Data.Bits (xor, shiftL)",
				"a048724 n = n `xor` shiftL n 1 :: Integer",
				"-- _Reinhard Zumkeller_, Mar 06 2013",
				"(Python)",
				"def A048724(n): return n^(n\u003c\u003c1) # _Chai Wah Wu_, Apr 05 2021"
			],
			"xref": [
				"Cf. A048720, A048725, A048726, A048728.",
				"Bisection of A003188.",
				"See also A065620, A065621.",
				"Cf. A242399."
			],
			"keyword": "nonn,nice,look,easy,base",
			"offset": "0,2",
			"author": "_Antti Karttunen_, Apr 26 1999",
			"references": 60,
			"revision": 52,
			"time": "2021-05-12T03:52:45-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}