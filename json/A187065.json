{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A187065",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 187065,
			"data": "0,0,1,0,0,1,2,1,1,3,5,4,5,9,14,14,19,28,42,47,66,89,131,155,221,286,417,507,728,924,1341,1652,2380,2993,4334,5373,7753,9707,14041,17460,25213,31501,45542,56714,81927,102256,147798,184183,266110,331981,479779",
			"name": "Let i be in {1,2,3} and let r \u003e= 0 be an integer. Let p = {p_1, p_2, p_3} = {-2,0,1}, n=2*r+p_i, and define a(-2)=1. Then, a(n)=a(2*r+p_i) gives the quantity of H_(7,1,0) tiles in a subdivided H_(7,i,r) tile after linear scaling by the factor x^r, where x=sqrt(2*cos(Pi/7)).",
			"comment": [
				"(Start) See A187067 for supporting theory. Define the matrix",
				"U_1=",
				"(0 1 0)",
				"(1 0 1)",
				"(0 1 1).",
				"Let r\u003e=0 and M=(m_(i,j))=(U_1)^r, i,j=1,2,3. Let A_r be the r-th \"block\" defined by A_r={a(2*r-2),a(2*r),a(2*r+1)} with a(-2)=1. Note that A_r-A_(r-1)-2*A_(r-2)+A_(r-3)={0,0,0}, with A_0={a(-2),a(0),a(1)}={1,0,0}. Let p={p_1,p_2,p_3}=(-2,0,1) and n=2*r+p_i. Then a(n)=a(2*r+p_i)=m_(i,1), where M=(m_(i,j))=(U_1)^r was defined above. Hence the block A_r corresponds component-wise to the first column of M, and a(n)=m_(i,1) gives the quantity of H_(7,1,0) tiles that should appear in a subdivided H_(7,i,r) tile. (End)",
				"Combining blocks A_r, B_r and C_r, from this sequence, A187066 and A187067, respectively, as matrix columns [A_r,B_r,C_r] generates the matrix (U_1)^r, and a negative index (-1)*r yields the corresponding inverse [A_(-r),B_(-r),C_(-r)]=(U_1)^(-r) of (U_1)^r. Therefore, the three sequences need not be causal.",
				"Since a(2*r-2)=a(2*(r-1)) for all r, this sequence arises by concatenation of first-column entries m_(2,1) and m_(3,1) from successive matrices M=(U_1)^r.",
				"a(n+2)=A187067(n), a(2*n)=A096976(n+1), a(2*n+1)=A006053(n)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A187065/b187065.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"L. E. Jeffery, \u003ca href=\"/wiki/User:L._Edson_Jeffery/Unit-Primitive_Matrices\"\u003eUnit-primitive matrices\u003c/a\u003e",
				"Roman Witula, Damian Slota and Adam Warzynski, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL9/Slota/slota57.html\"\u003eQuasi-Fibonacci Numbers of the Seventh Order\u003c/a\u003e, J. Integer Seq., 9 (2006), Article 06.4.3.",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0, 1, 0, 2, 0, -1)."
			],
			"formula": [
				"Recurrence: a(n) = a(n-2) + 2*a(n-4) - a(n-6).",
				"G.f.: x^2*(1-x^2+x^3)/(1-x^2-2*x^4+x^6).",
				"Closed-form: a(n) = -(1/14)*((X_1 + Y_1*(-1)^(n-1))*((w_2)^2 - (w_3)^2)*(w_1)^(n-1) + (X_2 + Y_2*(-1)^(n-1))*((w_3)^2 - (w_1)^2)*(w_2)^(n-1) + (X_3 + Y_3*(-1)^(n-1))*((w_1)^2 - (w_2)^2)*(w_3)^(n-1)), where w_k = sqrt(2*(-1)^(k-1)*cos(k*Pi/7)), X_k = (w_k)^3 - w_k + 1 and Y_k = -(w_k)^3 + w_k + 1, k=1,2,3."
			],
			"example": [
				"Suppose r=3. Then",
				"A_r = A_3 = {a(2*r-2,a(2*r),a(2*r+1)} = {a(4),a(6),a(7)} = {0,2,1},",
				"corresponding to the entries in the first column of M = (U_2)^3 =",
				"(0 2 1)",
				"(2 1 3)",
				"(1 3 3).",
				"Choose i=2 and set n=2*r+p_i. Then a(n) = a(2*r+p_i) = a(6+0) = a(6) = 2, which equals the entry in row 2 and column 1 of M. Hence a subdivided H_(7,2,3) tile should contain a(6) = m_(2,1) = 2 H_(7,1,0) tiles."
			],
			"mathematica": [
				"LinearRecurrence[{0,1,0,2,0,-1},{0,0,1,0,0,1},50] (* _Harvey P. Dale_, Aug 15 2012 *)",
				"CoefficientList[Series[x^2 (1 - x^2 + x^3)/(1 - x^2 - 2 x^4 + x^6), {x, 0, 50}], x] (* _Vincenzo Librandi_, Sep 18 2015 *)"
			],
			"program": [
				"(MAGMA) I:=[0,0,1,0,0,1]; [n le 6 select I[n] else Self(n-2)+2*Self(n-4)-Self(n-6): n in [1..60]]; // _Vincenzo Librandi_, Sep 18 2015",
				"(PARI) x='x+O('x^50); concat([0,0], Vec(x^2*(1-x^2+x^3)/(1-x^2-2*x^4 +x^6))) \\\\ _G. C. Greubel_, Jan 29 2018"
			],
			"xref": [
				"Cf. A187066, A187067, A187068, A187069, A187070."
			],
			"keyword": "nonn,easy",
			"offset": "0,7",
			"author": "_L. Edson Jeffery_, Mar 09 2011",
			"ext": [
				"More terms from _Vincenzo Librandi_, Sep 18 2015"
			],
			"references": 5,
			"revision": 35,
			"time": "2019-02-03T16:53:29-05:00",
			"created": "2011-03-03T06:10:11-05:00"
		}
	]
}