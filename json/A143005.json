{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A143005",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 143005,
			"data": "0,1,81,12491,3176120,1235165464,697648230720,550023729068736,586201214122536960,822460381655068717056,1485544574481829982208000,3389058487000919282503680000,9606157364646714324010401792000",
			"name": "a(0) = 0, a(1) = 1, a(n+1) = (2*n+1)*(n^2+n+25)*a(n) - n^6*a(n-1).",
			"comment": [
				"This is the case m = 3 of the general recurrence a(0) = 0, a(1) = 1, a(n+1) = (2*n+1)*(n^2+n+2*m^2+2*m+1 )*a(n) - n^6*a(n-1) (we suppress the dependence of a(n) on m), which arises when accelerating the convergence of the series sum {k = 1..inf} 1/k^3 for the constant zeta(3). For remarks on the general theory see A143003 (m=1). For other cases see A066989 (m=0), A143004 (m=2) and A143006 (m=4)."
			],
			"reference": [
				"Bruce C. Berndt, Ramanujan's Notebooks Part II, Springer-Verlag."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A143005/b143005.txt\"\u003eTable of n, a(n) for n = 0..180\u003c/a\u003e"
			],
			"formula": [
				"a(n) = n!^3*p(n)*sum {k = 1..n} 1/(k^3*p(k-1)*p(k)), where p(n) = (10*n^6 +30*n^5 +85*n^4 +120*n^3 +121*n^2 +66*n +18)/18. Recurrence: a(0) = 0, a(1) = 1, a(n+1) = (2*n+1)*(n^2+n+25)*a(n) - n^6*a(n-1). The sequence b(n):= n!^3*p(n) satisfies the same recurrence with the initial conditions b(0) = 1, b(1) = 25. Hence we obtain the finite continued fraction expansion a(n)/b(n) = 1/(25- 1^6/(81- 2^6/(155- 3^6/(259-...- (n-1)^6/((2*n-1)*(n^2-n+25)))))), for n \u003e=2. The behavior of a(n) for large n is given by lim n -\u003e infinity a(n)/b(n) = sum {k = 1..inf} 1/(k^3*p(k-1)*p(k)) = 1/(25- 1^6/(81- 2^6/(155- 3^6/(259-...- n^6/((2*n+1)*(n^2+n+25)-...))))) = zeta(3) - (1 + 1/2^3 + 1/3^3), where the final equality follows from a result of Ramanujan; see [Berndt, Chapter 12, Entry 32(iii) at x = 3]."
			],
			"maple": [
				"p := n -\u003e (10*n^6+30*n^5+85*n^4+120*n^3+121*n^2+66*n+18)/18: a := n -\u003e n!^3*p(n)*sum (1/(k^3*p(k-1)*p(k)), k = 1..n): seq(a(n), n = 0..13);"
			],
			"mathematica": [
				"RecurrenceTable[{a[0]==0,a[1]==1,a[n+1]==(2n+1)(n^2+n+25)a[n]-n^6 a[n-1]}, a, {n,15}] (* _Harvey P. Dale_, Dec 04 2011 *)"
			],
			"xref": [
				"Cf. A066989, A143003, A143004, A143006, A143007."
			],
			"keyword": "easy,nonn",
			"offset": "0,3",
			"author": "_Peter Bala_, Jul 19 2008",
			"references": 6,
			"revision": 11,
			"time": "2017-08-23T13:07:28-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}