{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A194938",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 194938,
			"data": "1,0,1,0,3,1,0,8,9,1,0,42,59,18,1,0,264,450,215,30,1,0,2160,4114,2475,565,45,1,0,20880,43512,30814,9345,1225,63,1,0,236880,528492,420756,154609,27720,2338,84,1,0,3064320,7235568,6316316,2673972,594489,69552",
			"name": "Triangle read by rows: coefficients of polynomials p(x,n) defined by 1/(1-t-t^2)^x = Sum_{n=1..oo} p(x,n)*t^n/n!.",
			"comment": [
				"A039692 is a similar triangle but without the leading column.",
				"1/(1-t-t^2) is the g.f. for the Fibonacci numbers (A000045).",
				"Row sums: A005442(n-1).",
				"Also the Bell transform of n!*(F(n)+F(n+2)), F(n) the Fibonacci numbers. For the definition of the Bell transform see A264428. - _Peter Luschny_, Jan 21 2016"
			],
			"reference": [
				"Steve Roman, The Umbral Calculus, Dover Publications, New York (1984), pp. 149-150"
			],
			"example": [
				"Triangle begins",
				"1;",
				"0, 1;",
				"0, 3, 1;",
				"0, 8, 9, 1;",
				"0, 42, 59, 18, 1;",
				"0, 264, 450, 215, 30, 1;",
				"0, 2160, 4114, 2475, 565, 45, 1;",
				"0, 20880, 43512, 30814, 9345, 1225, 63, 1;",
				"0, 236880, 528492, 420756, 154609, 27720, 2338, 84, 1;",
				"0, 3064320, 7235568, 6316316, 2673972, 594489, 69552, 4074, 108, 1;",
				"0, 44634240, 110499696, 103889700, 49087520, 12803175, 1887753, 154350,6630,135,1;"
			],
			"maple": [
				"# The function BellMatrix is defined in A264428.",
				"with(combinat): g := n -\u003e factorial(n)*(fibonacci(n)+fibonacci(n+2)):",
				"BellMatrix(g, 10); # _Peter Luschny_, Jan 21 2016"
			],
			"mathematica": [
				"p[t_] = 1/(1 - t - t^2)^x; Table[ ExpandAll[n!SeriesCoefficient[ Series[p[t], {t, 0, 30}], n]], {n, 0, 10}]; a = Table[n!* CoefficientList[SeriesCoefficient[ Series[p[t], {t, 0, 30}], n], x], {n, 0, 10}]; Flatten[a]",
				"(* Second program *)",
				"BellMatrix[f_, len_] := With[{t = Array[f, len, 0]}, Table[BellY[n, k, t], {n, 0, len - 1}, {k, 0, len - 1}]];",
				"B = BellMatrix[Function[n, n!*(Fibonacci[n] + Fibonacci[n+2])], rows = 12];",
				"Table[B[[n, k]], {n, 1, rows}, {k, 1, n}] // Flatten (* _Jean-François Alcover_, Jun 28 2018, after _Peter Luschny_ *)"
			],
			"program": [
				"(Sage) # uses[bell_matrix from A264428]",
				"bell_matrix(lambda n: factorial(n)*(fibonacci(n)+fibonacci(n+2)), 8) # _Peter Luschny_, Jan 21 2016"
			],
			"xref": [
				"Cf. A000045, A005442, A039692."
			],
			"keyword": "nonn,tabl",
			"offset": "1,5",
			"author": "_Roger L. Bagula_, Apr 17 2008",
			"ext": [
				"Edited by _N. J. A. Sloane_, Aug 28 2011"
			],
			"references": 1,
			"revision": 23,
			"time": "2020-03-25T03:09:54-04:00",
			"created": "2011-09-05T09:53:30-04:00"
		}
	]
}