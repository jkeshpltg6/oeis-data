{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A321126",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 321126,
			"data": "1,2,2,3,2,3,4,3,3,4,5,4,3,4,5,6,5,4,4,5,6,7,6,5,5,5,6,7,8,7,6,6,6,6,7,8,9,8,7,7,7,7,7,8,9,10,9,8,8,8,8,8,8,9,10,11,10,9,9,9,9,9,9,9,10,11,12,11,10,10,10,10,10,10,10,10,11,12,13,12,11,11,11,11,11,11,11,11,11,12,13",
			"name": "T(n,k) = max(n + k - 1, n + 1, k + 1), square array read by antidiagonals (n \u003e= 0, k \u003e= 0).",
			"comment": [
				"T(n,k) - 1 is the maximum degree of d in the three-variable bracket polynomial \u003cK\u003e(A,B,d) for the two-bridge knot with Conway's notation C(n,k). Hence, T(n,k) is the maximum number of Jordan curves that are obtained by splitting the crossings of such knot diagram."
			],
			"reference": [
				"Louis H. Kauffman, Formal Knot Theory, Princeton University Press, 1983."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A321126/b321126.txt\"\u003eTable of n, a(n) for n = 0..11475\u003c/a\u003e (rows 0 \u003c= n \u003c= 150, flattened).",
				"Louis H. Kauffman, \u003ca href=\"https://doi.org/10.1016/0040-9383(87)90009-7\"\u003eState models and the Jones polynomial\u003c/a\u003e, Topology Vol. 26 (1987), 395-407.",
				"Kelsey Lafferty, \u003ca href=\"https://scholar.rose-hulman.edu/rhumj/vol14/iss2/7/\"\u003eThe three-variable bracket polynomial for reduced, alternating links\u003c/a\u003e, Rose-Hulman Undergraduate Mathematics Journal Vol. 14 (2013), 98-113.",
				"Matthew Overduin, \u003ca href=\"https://www.math.csusb.edu/reu/OverduinPaper.pdf\"\u003eThe three-variable bracket polynomial for two-bridge knots\u003c/a\u003e, California State University REU, 2013.",
				"Franck Maminirina Ramaharo, \u003ca href=\"/A321125/a321125.pdf\"\u003eIllustration of T(2,2)\u003c/a\u003e",
				"Franck Maminirina Ramaharo, \u003ca href=\"/A321125/a321125.txt\"\u003eNote on sequence A321125 and related ones\u003c/a\u003e",
				"Franck Ramaharo, \u003ca href=\"https://arxiv.org/abs/1902.08989\"\u003eA generating polynomial for the two-bridge knot with Conway's notation C(n,r)\u003c/a\u003e, arXiv:1902.08989 [math.CO], 2019.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/BracketPolynomial.html\"\u003eBracket Polynomial\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/2-bridge_knot\"\u003e2-bridge knot\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Bracket_polynomial\"\u003eBracket polynomial\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = T(k,n).",
				"T(n,k) = A051125(n+1,k+1) for 0 \u003c= k \u003c= 2, n \u003e= 0, and T(n,k) =  A051125(n+1,k+1) + A003983(n-2,k-2) for k \u003e= 3, n \u003e= 3.",
				"T(n,n) = A004280(n+1).",
				"G.f.: (1 - (2*x - x^2)*y + (x - 2*x^2 + x^3)*y^2 + (x^2 - x^3)*y^3)/(((1 - x)*(1 - y))^2)."
			],
			"example": [
				"Square array begins:",
				"    1,  2,  3,  4,  5,  6,  7,  8,  9, 10, ...",
				"    2,  2,  3,  4,  5,  6,  7,  8,  9, 10, ...",
				"    3,  3,  3,  4,  5,  6,  7,  8,  9, 10, ...",
				"    4,  4,  4,  5,  6,  7,  8,  9, 10, 11, ...",
				"    5,  5,  5,  6,  7,  8,  9, 10, 11, 12, ...",
				"    6,  6,  6,  7,  8,  9, 10, 11, 12, 13, ...",
				"    7,  7,  7,  8,  9, 10, 11, 12, 13, 14, ...",
				"    8,  8,  8,  9, 10, 11, 12, 13, 14, 15, ...",
				"    9,  9,  9, 10, 11, 12, 13, 14, 15, 16, ...",
				"   10, 10, 10, 11, 12, 13, 14, 15, 16, 17, ...",
				"  ..."
			],
			"mathematica": [
				"Table[Max[k + 1, n - 1, n - k + 1], {n, 0, 10}, {k, 0, n}] // Flatten"
			],
			"program": [
				"(Maxima) create_list(max(k + 1, n - 1, n - k + 1), n, 0, 10, k, 0, n);"
			],
			"xref": [
				"T(n,1) = degree of the (n+1)-th row polynomial in A300453.",
				"T(n,k) = degree of the n-th row polynomials in A300454 and A321127, k = 2,n, respectively.",
				"Cf. A321125, A316989.",
				"Cf. A003056, A003983, A051125."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "0,2",
			"author": "_Franck Maminirina Ramaharo_, Nov 21 2018",
			"references": 2,
			"revision": 16,
			"time": "2019-04-10T21:55:10-04:00",
			"created": "2018-12-20T23:47:31-05:00"
		}
	]
}