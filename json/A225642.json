{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A225642",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 225642,
			"data": "1,2,3,6,4,12,5,30,60,6,30,60,7,84,420,8,120,840,9,180,1260,2520,10,210,840,2520,11,330,4620,13860,27720,12,420,4620,13860,27720,13,780,8580,60060,180180,360360,14,630,8190,90090,360360,15,840,10920,120120,360360",
			"name": "Irregular table read by rows: n-th row gives distinct values of successively iterated Landau-like functions for n, starting with the initial value n.",
			"comment": [
				"The leftmost column of table (the initial term of each row, T(n,1)) is n, corresponding to lcm(n) computed from the singular {n} partition of n, after which, on the same row, each further term T(n,i) is computed by finding such a partition {p_1 + p_2 + ... + p_k} of n so that value of lcm(T(n, i-1), p_1, p_2, ..., p_k) is maximized, until finally A003418(n) is reached, which will be listed as the last term of row n (as the result would not change after that, if we continued the same process).",
				"Of possible interest: which numbers occur only once in this table, and which occur multiple times? And how many times, if each number occurs only a finite number of times?",
				"Each number occurs a finite number of times: rows are increasing, first column is increasing, so n will occur last in row n, leftmost column. Primes (and other numbers too) occur once. - _Alois P. Heinz_, May 25 2013"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A225642/b225642.txt\"\u003eRows n = 1..150, flattened\u003c/a\u003e"
			],
			"example": [
				"The first fifteen rows of table are:",
				"   1;",
				"   2;",
				"   3,   6;",
				"   4,  12;",
				"   5,  30,    60;",
				"   6,  30,    60;",
				"   7,  84,   420;",
				"   8, 120,   840;",
				"   9, 180,  1260,   2520;",
				"  10, 210,   840,   2520;",
				"  11, 330,  4620,  13860,  27720;",
				"  12, 420,  4620,  13860,  27720;",
				"  13, 780,  8580,  60060, 180180, 360360;",
				"  14, 630,  8190,  90090, 360360;",
				"  15, 840, 10920, 120120, 360360;"
			],
			"mathematica": [
				"b[n_, i_] := b[n, i] = If[n == 0, {1}, If[i \u003c 1, {}, Table[Map[Function[{x}, LCM[x, If[j == 0, 1, i]]], b[n - i * j, i - 1]], {j, 0, n/i}]]]; T[n_] := T[n] = Module[{d, h, t, lis}, t = b[n, n]; lis = {}; d = n; h = 0; While[d != h, AppendTo[lis, d]; h = d; d = Max[Table[LCM[h, i], {i, t}]]]; lis]; Table[T[n], {n, 1, 20}] // Flatten (* _Jean-François Alcover_, Mar 02 2016, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(Scheme with _Antti Karttunen_'s IntSeq-library):",
				"(definec (A225642 n) (A225640bi (Aux_for_225642 n) (- n (A225645 (Aux_for_225642 n))))) ;; Scheme-definition of A225640bi given in A225640.",
				"(define Aux_for_225642 (COMPOSE -1+ (LEAST-GTE-I 1 1 A225645) 1+)) ;; Auxiliary function not submitted separately, which gives the row-number for the n-th term.",
				";; It starts as 1, 2, 3, 3, 4, 4, 5, 5, 5, 6, 6, 6, 7, 7, 7, 8, 8, 8, 9, 9, 9, 9, 10, 10, 10, 10, 11, 11, 11, 11, 11, ..."
			],
			"xref": [
				"Cf. A225644 (length of n-th row), A225646 (for n \u003e= 3, second term of n-th row).",
				"Cf. A003418 (largest and rightmost term of n-th row).",
				"Cf. A225640, A225641, A225645.",
				"Cf. A225632 (each row starts with 1 instead of n).",
				"Cf. A226055 (the first common term with A225632 on the n-th row).",
				"Cf. A225639 (distance to that first common term).",
				"Cf. A226056 (number of trailing common terms with A225632 on the n-th row)."
			],
			"keyword": "nonn,tabf",
			"offset": "1,2",
			"author": "_Antti Karttunen_, May 15 2013",
			"references": 13,
			"revision": 38,
			"time": "2018-03-13T04:09:10-04:00",
			"created": "2013-06-05T21:25:53-04:00"
		}
	]
}