{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A188528",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 188528,
			"data": "9,6,2,5,1,4,9,3,8,5,1,9,6,2,8,10,4,9,6,2,5,1,4,6,2,8,5,1,9,6,2,5,1,4,9,3,8,5,1,9,6,2,8,10,4,9,6,2,5,1,4,6,2,8,5,1,9,6,2,5,1,4,9,3,8,5,1,9,6,2,8,10,4,9,6,2,5,1,4,6,2,8,5,1,9",
			"name": "First month in year n with a \"Friday the 13th\", starting from 1901.",
			"comment": [
				"A101312(n) \u003e 0, therefore a(n) is defined for all n."
			],
			"reference": [
				"Chr. Zeller, Kalender-Formeln, Acta mathematica, 9 (1886), 131-136."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A188528/b188528.txt\"\u003eTable of n, a(n) for n = 1901..3000\u003c/a\u003e",
				"J. R. Stockton, \u003ca href=\"http://www.merlyn.demon.co.uk/zel-86px.htm\"\u003eRektor Chr. Zeller's 1886 Paper \"Kalender-Formeln\"\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Triskaidekaphobia.html\"\u003eTriskaidekaphobia\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Triskaidekaphobia\"\u003eTriskaidekaphobia\u003c/a\u003e",
				"\u003ca href=\"/index/Ca#calendar\"\u003eIndex entries for sequences related to calendars\u003c/a\u003e"
			],
			"example": [
				"Number of times all months occur on \"Friday the 13th\" in the past century and the current one:",
				"          | Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec",
				"----------+------------------------------------------------",
				"1991-2000 |  14  14   4  10  14  14   0  11  15   4   0   0",
				"2000-2100 |  14  15   3  11  14  15   0  11  14   3   0   0",
				"The table doesn't say that there are no \"Friday the 13ths\" in July, November, and December: just first occurrences are considered, e.g. Nov 13 2009 is on a Friday, but a(2009) = 2; Jul 13 2012 is on a Friday, but a(2012) = 1; and Dec 13 2024 is on a Friday, but a(2024) = 9."
			],
			"mathematica": [
				"{* First execute *) \u003c\u003c Calendar`; (* then execute *) Table[Select[ Table[ {yr,n,13},{n,12}],DayOfWeek[#]==Friday\u0026,1][[1,2]],{yr,1901,2011}] (* _Harvey P. Dale_, Oct 26 2011 *)"
			],
			"program": [
				"(Haskell)",
				"import Data.List (findIndex)",
				"import Data.Maybe (fromJust)",
				"a188528 n = succ $ fromJust $",
				"  findIndex (\\m -\u003e h n m 13 == 6) [1..12] where",
				"    h year month day  -- cf. Zeller reference.",
				"      | month \u003c= 2 = h  (year - 1)  (month + 12)  day",
				"      | otherwise  = (day + 26 * (month + 1) `div` 10 + y + y `div` 4",
				"                     + century `div` 4 - 2 * century) `mod` 7",
				"        where (century, y) = divMod year 100",
				"b188528 = bFileFun \"A188528\" a188528 1991 3000",
				"-- For statistics (see example) ...",
				"ff13_perMonth ys m = length $ filter (== m) (map a188528 ys)",
				"century20 = map (ff13_perMonth [1901..2000]) [1..12]",
				"century21 = map (ff13_perMonth [2001..2100]) [1..12]",
				"(Python)",
				"from datetime import date",
				"def a(n):",
				"    for month in range(1, 13):",
				"        if date.isoweekday(date(n, month, 13)) == 5: return month",
				"print([a(n) for n in range(1901, 1986)]) # _Michael S. Branicky_, Sep 06 2021"
			],
			"xref": [
				"Cf. A101312, A157962."
			],
			"keyword": "nonn",
			"offset": "1901,1",
			"author": "_Reinhard Zumkeller_, May 16 2011",
			"references": 3,
			"revision": 25,
			"time": "2021-09-06T11:53:51-04:00",
			"created": "2011-05-17T00:04:32-04:00"
		}
	]
}