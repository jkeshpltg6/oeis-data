{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003818",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3818,
			"data": "1,1,2,9,365,5403014,432130991537958813,14935169284101525874491673463268414536523593057",
			"name": "a(1)=a(2)=1, a(n+1) = (a(n)^3 +1)/a(n-1).",
			"comment": [
				"The term a(9) has 121 digits. - _Harvey P. Dale_, Nov 23 2013",
				"The recursion has the Laurent property. If a(1), a(2) are variables, then a(n) is a Laurent polynomial (a rational function with a monomial denominator). - _Michael Somos_, Feb 25 2019",
				"This sequence was the subject of the 3rd problem of the 14th British Mathematical Olympiad in 1978 where this sequence was defined by: u(1) = 1, u(1) \u003c u(2) and u(n)^3 + 1 = u(n-1) * u(n+1), for n \u003e 1 (see link B. M. O. and reference). - _Bernard Schott_, Apr 01 2021"
			],
			"reference": [
				"A. Gardiner, The Mathematical Olympiad Handbook: An Introduction to Problem Solving, Oxford University Press, 1997, reprinted 2011, Pb 3 pp. 68 and 204-205 (1978)."
			],
			"link": [
				"British Mathematical Olympiad, \u003ca href=\"https://bmos.ukmt.org.uk/home/bmo-1978.pdf\"\u003e1978 - Problem 3\u003c/a\u003e.",
				"math110, \u003ca href=\"http://math.stackexchange.com/questions/1932492/\"\u003eA similar Somos sequence problem prove A_2nB_{n+3} is integer sequence\u003c/a\u003e.",
				"\u003ca href=\"/index/O#Olympiads\"\u003eIndex to sequences related to Olympiads\u003c/a\u003e."
			],
			"formula": [
				"a(n) is asymptotic to c^F(2n) where F(n) is the n-th Fibonacci's number A000045(n) and c=1.1137378757136... - _Benoit Cloitre_, May 31 2005",
				"May be extended to negative arguments by setting a(n) = a(3-n) for all n in Z. - _Michael Somos_, Apr 11 2017"
			],
			"maple": [
				"A003818 := proc(n) option remember; if n \u003c= 2 then 1 else (A003818(n-1)^3+1)/A003818(n-2); fi; end;"
			],
			"mathematica": [
				"RecurrenceTable[{a[1]==a[2]==1,a[n]==(a[n-1]^3+1)/a[n-2]},a,{n,10}] (* _Harvey P. Dale_, Nov 23 2013 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, n=3-n); if( n\u003c3, 1, (1 + a(n-1)^3) / a(n-2))}; /* _Michael Somos_, Apr 11 2017 */"
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "Waldemar Pompe (pompe(AT)zodiac1.mimuw.edu.pl)",
			"ext": [
				"More terms from _Benoit Cloitre_, May 31 2005"
			],
			"references": 1,
			"revision": 27,
			"time": "2021-04-01T12:18:10-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}