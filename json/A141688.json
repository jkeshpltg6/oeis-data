{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A141688",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 141688,
			"data": "1,1,1,1,6,1,1,26,26,1,1,99,416,99,1,1,352,5407,5407,352,1,1,1200,62616,227094,62616,1200,1,1,3977,673728,8212854,8212854,673728,3977,1,1,12918,6889153,269486766,903413940,269486766,6889153,12918,1,1,41338,67863290,8256432767,88493861004,88493861004,8256432767,67863290,41338,1",
			"name": "Triangle T(n, k) = Fibonacci(2*k)*T(n-1, k) + Fibonacci(2*(n-k+1))*T(n-1, k-1), with T(n, 1) = T(n, n) = 1, read by rows.",
			"comment": [
				"Row sums are: {1, 2, 8, 54, 616, 11520, 354728, 17781120, 1456191616, 193636396800, ...}."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A141688/b141688.txt\"\u003eRows n = 1..50 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"Let A088305(n) be defined by b(n) = Sum_{j=1..n} j*b(n-j), with b(0)=1, then T(n, k) = b(n-k+1)*T(n-1, k-1) + b(k)*T(n-1, k) with T(n,1) = T(n,n) = 1.",
				"From _G. C. Greubel_, Mar 29 2021: (Start)",
				"T(n, k) = Fibonacci(2*k)*T(n-1, k) + Fibonacci(2*(n-k+1))*T(n-1, k-1), with T(n, 1) = T(n, n) = 1.",
				"T(n, 2) = A186314(n+1). (End)"
			],
			"example": [
				"Triangle begins as:",
				"  1;",
				"  1,     1;",
				"  1,     6,       1;",
				"  1,    26,      26,         1;",
				"  1,    99,     416,        99,         1;",
				"  1,   352,    5407,      5407,       352,        1;",
				"  1,  1200,   62616,    227094,     62616,     1200,       1;",
				"  1,  3977,  673728,   8212854,   8212854,   673728,    3977,     1;",
				"  1, 12918, 6889153, 269486766, 903413940,269486766, 6889153, 12918, 1;"
			],
			"mathematica": [
				"(* First program *)",
				"b[n_]:= b[n]= If[n==0, 1, Sum[k*b[n-k], {k,n}]];",
				"T[n_, k_]:= If[k==1 || k==n, 1, b[n-k+1]*T[n-1, k-1] + b[k]*T[n-1, k]];",
				"Table[T[n, k], {n,12}, {k,n}]//Flatten (* modified by _G. C. Greubel_, Mar 29 2021 *)",
				"(* Second program *)",
				"T[n_, k_]:= T[n, k]= If[k==1 || k==n, 1, Fibonacci[2*(n-k+1)]*T[n-1, k-1] + Fibonacci[2*k]*T[n-1, k]];",
				"Table[T[n, k], {n, 12}, {k, n}]//Flatten (* _G. C. Greubel_, Mar 29 2021 *)"
			],
			"program": [
				"(Magma)",
				"function T(n,k)",
				"  if k eq 1 or k eq n then return 1;",
				"  else return Fibonacci(2*(n-k+1))*T(n-1, k-1) + Fibonacci(2*k)*T(n-1, k);",
				"  end if; return T;",
				"end function;",
				"[T(n,k): k in [1..n], n in [1..12]]; // _G. C. Greubel_, Mar 29 2021",
				"(Sage)",
				"@CachedFunction",
				"def T(n,k): return 1 if (k==1 or k==n) else fibonacci(2*(n-k+1))*T(n-1, k-1) + fibonacci(2*k)*T(n-1, k)",
				"flatten([[T(n,k) for k in (1..n)] for n in (1..12)]) # _G. C. Greubel_, Mar 29 2021"
			],
			"xref": [
				"Cf. A088305, A186314."
			],
			"keyword": "nonn,tabl",
			"offset": "1,5",
			"author": "_Roger L. Bagula_, Sep 09 2008",
			"ext": [
				"Edited by _G. C. Greubel_, Mar 29 2021"
			],
			"references": 1,
			"revision": 6,
			"time": "2021-03-30T01:51:02-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}