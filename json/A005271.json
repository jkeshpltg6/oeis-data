{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005271",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5271,
			"id": "M1955",
			"data": "1,2,9,272,589185,16332454526976,391689748492473664721077609089",
			"name": "Number of perfect matchings in n-cube.",
			"comment": [
				"The matchings contain 2^n / 2 = 2^(n-1) edges.",
				"a(6) was first found by D. H. Wiedemann, unpublished (see Clark et al., Skupien).",
				"Also the number of minimum edge covers and minimum clique coverings in the n-hypercube graph. - _Eric W. Weisstein_, Dec 24 2017"
			],
			"reference": [
				"L. H. Clark, J. C. George and T. D. Porter, On the number of 1-factors in the n-cube, Congress. Numer., 127 (1997), 67-69.",
				"J. Propp, Enumeration of matchings: problems and progress, pp. 255-291 in L. J. Billera et al., eds, New Perspectives in Algebraic Combinatorics, Cambridge, 1999 (see Problem 18).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"M. K. Chari and M. Joswig, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2004.07.027\"\u003eComplexes of discrete Morse functions\u003c/a\u003e, Disc. Math. 302 (2005), 39-51.",
				"D. Deford, \u003ca href=\"http://www.math.dartmouth.edu/~ddeford/graphs.pdf\"\u003eSeating rearrangements on arbitrary graphs\u003c/a\u003e, 2013; See Table 3.",
				"N. Graham and F. Harary, \u003ca href=\"https://doi.org/10.1016/0893-9659(88)90173-5\"\u003eThe number of perfect matchings in a hypercube\u003c/a\u003e, Appl. Math. Lett., 1 (1988), 45-48.",
				"N. Graham \u0026 F. Harary, \u003ca href=\"/A005271/a005271.pdf\"\u003eThe number of perfect matchings in a hypercube\u003c/a\u003e, Appl. Math. Lett. 1.1 (1988), 45-48. (Annotated scanned copy)",
				"Per Hakan Lundow, \u003ca href=\"http://www.theophys.kth.se/~phl/Text/1factors.pdf\"\u003eComputation of matching polynomials and the number of 1-factors in polygraphs\u003c/a\u003e, Research report, No 12, 1996, Department of Math., Umea University, Sweden.",
				"Patric R. J. Östergård and V. H. Pettersson, \u003ca href=\"https://doi.org/10.1007/s11083-012-9279-8\"\u003eEnumerating Perfect Matchings in n-Cubes\u003c/a\u003e, Order, November 2013, Volume 30, Issue 3, pp 821-835.",
				"Ville Pettersson, \u003ca href=\"https://aaltodoc.aalto.fi/bitstream/handle/123456789/17688/isbn9789526063652.pdf?sequence=1\"\u003eGraph Algorithms for Constructing and Enumerating Cycles and Related Structures\u003c/a\u003e, Preprint 2015.",
				"J. Propp, \u003ca href=\"https://arxiv.org/abs/math/9801061\"\u003eTwenty open problems in enumeration of matchings\u003c/a\u003e, arXiv:math/9801061 [math.CO], 1998-1999.",
				"J. Propp, \u003ca href=\"http://faculty.uml.edu/jpropp/update.pdf\"\u003eUpdated article\u003c/a\u003e",
				"J. Propp, Enumeration of matchings: problems and progress, in L. J. Billera et al. (eds.), \u003ca href=\"http://www.msri.org/publications/books/Book38/contents.html\"\u003eNew Perspectives in Algebraic Combinatorics\u003c/a\u003e",
				"H. Sachs, B. Alspach, \u003ca href=\"https://doi.org/10.1016/S0012-365X(98)90041-3\"\u003eProblem 298: How many perfect matchings does the graph of the n-cube have?, Discrete Math., 191 (1998), 251-252. [From _N. J. A. Sloane_, Feb 18 2012]",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/HypercubeGraph.html\"\u003eHypercube Graph\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Matching.html\"\u003eMatching\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/MaximumIndependentEdgeSet.html\"\u003eMaximum Independent Edge Set\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/MinimumCliqueCovering.html\"\u003eMinimum Clique Covering\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/MinimumEdgeCover.html\"\u003eMinimum Edge Cover\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PerfectMatching.html\"\u003ePerfect Matching\u003c/a\u003e"
			],
			"example": [
				"G.f. = x + 2*x^2 + 9*x^3 + 272*x^4 + 589185*x^5 + 16332454526976*x^6 + ..."
			],
			"xref": [
				"Cf. A220904, A112311.",
				"For all matchings see A045310."
			],
			"keyword": "nonn,hard,more,nice",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"a(6) from _Per H. Lundow_, Jul 15 1996",
				"a(7) from _N. J. A. Sloane_, Jan 01 2013"
			],
			"references": 5,
			"revision": 63,
			"time": "2020-02-02T20:39:54-05:00",
			"created": "1991-05-20T03:00:00-04:00"
		}
	]
}