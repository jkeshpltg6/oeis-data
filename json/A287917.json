{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A287917",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 287917,
			"data": "1,1,2,1,1,1,1,2,1,2,3,1,2,1,3,1,2,2,4,1,3,1,3,1,2,4,1,2,3,4,2,1,3,1,2,3,5,4,1,3,5,2,1,4,1,6,4,2,3,1,2,5,1,4,3,2,6,1,3,5,2,4,2,5,1,6,3,1,6,1,2,3,4,5,7,3,1,4,2,1,6,1,3,2,7,5,4",
			"name": "Number of distinct primorials A002110(k) \u003e A285784(n) such that the primorials are coprime to A285784(n).",
			"comment": [
				"Terms of A285784 that have a(n) = 1 appear in A287390.",
				"Terms of A285784 that have a(n) \u003e 1 appear in A287391.",
				"From _Michael De Vlieger_, Jun 09 2017: (Start)",
				"Let primorial p_n# = A002110(n) and let m be a nonzero positive number called a totative such that gcd(t, p_n#) = 1. This sequence concerns nonprime m. A285784 is the sequence that lists unique nonprime totatives m of primorials p_n#.",
				"For A285784(1), a(n) = infinity, since 1 is the empty product and a totative of (i.e., coprime to) all numbers. Hence the offset of a(n) is 2 and for this reason hereinafter we only consider composite totatives m.",
				"Consider the composite totative m in A285784. For a given composite term in A285784, there is a least primorial p_a# to which m is coprime. Such m \u003c p_a# are products of prime totatives q \u003e p_a, the gpf of p_a#. Therefore m \"appears\" when there are prime totatives q \u003c sqrt(p_a#). The smallest a for which we have this condition is a = 4, as q = 11 is less than sqrt(210). For the same reason the first composite term of A285784 is 11^2 = 121.",
				"For n \u003e= 2, m is coprime to a finite range of primorials p_a# .. p_b#. If m is coprime to p_b#, then it must be coprime to all primorials p_a# .. p_b# by the definition of primorial. m is no longer coprime to p_(b+1)# since at least one of its prime divisors p_(b+1) also divides p_(b+1)#. This sequence gives the range b - a + 1.",
				"To generate data that includes all the terms of A285784 less than a limit x, we can write a while statement that operates so long as there is at least 1 totative m \u003c x of p_n#. Since primorial p_n# is the product of the smallest n primes, fewer numbers less than x are coprime to p_n# as n increases, until exhaustion. Thus we can produce a list of unique m \u003c x (i.e., terms of A285784 less than x) for relatively large primorials p_n#. Then we can count the instances of terms of A285784 for a list of lists of totatives m \u003c x for primorials p_1# .. p_n# and obtain certainty about the number of instances of terms of A285784.",
				"First position of values of a(n): {2, 4, 12, 20, 38, 47, 76, 96, 111, 139, 228, 241, 300, 339, 363, 434, 482, 566, 689, 752, 790, 862, 902, 973, 1264, 1361, 1506, 1562, 1816, ...}",
				"Terms of A285784 that set records in a(n): {121, 169, 361, 529, 841, 961, 1369, 1681, 1849, 2209, 3481, 3721, 4489, 5041, 5329, 6241, 6889, 7921, 9409, 10201, 10609, 11449, 11881, 12769, ...}",
				"(End)"
			],
			"link": [
				"Jamie Morken, \u003ca href=\"/A287917/b287917.txt\"\u003eTable of n, a(n) for n = 2..10000\u003c/a\u003e"
			],
			"example": [
				"The sequence starts:",
				"   n  A285784(n)  a(n)",
				"   2      121     1",
				"   3      143     1",
				"   4      169     2",
				"   5      187     1",
				"   6      209     1",
				"   7      221     1",
				"   8      247     1",
				"   9      289     2",
				"  10      299     1",
				"  11      323     2",
				"  12      361     3",
				"  13      377     1",
				"  14      391     2",
				"  15      403     1",
				"  16      437     3",
				"  17      481     1",
				"  18      493     2   ..."
			],
			"mathematica": [
				"Block[{nn = 1600, k = 1, P = 2, a}, a = Most@ Reap[While[Nand[k \u003e 3, Length@ Sow@ Rest@ Select[Range[Min[P, nn]], And[! PrimeQ@ #, CoprimeQ[#, P]] \u0026] == 0], k++; P *= Prime@ k]][[-1, 1]]; Function[b, Map[Count[b, #] \u0026, Union@ b]]@ Flatten@ a] (* _Michael De Vlieger_, Jun 09 2017 *)"
			],
			"xref": [
				"Cf. A002110, A285784, A287390, A287391."
			],
			"keyword": "nonn",
			"offset": "2,3",
			"author": "_Jamie Morken_ and _Michael De Vlieger_, Jun 08 2017",
			"ext": [
				"Edited by _Michael De Vlieger_, Jun 09 2017"
			],
			"references": 2,
			"revision": 27,
			"time": "2020-01-12T10:18:33-05:00",
			"created": "2017-08-02T13:03:42-04:00"
		}
	]
}