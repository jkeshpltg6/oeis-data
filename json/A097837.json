{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A097837",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 97837,
			"data": "1,52,2651,135149,6889948,351252199,17906972201,912904330052,46540213860451,2372638002552949,120957997916339948,6166485255730784399,314369790044353664401,16026692807006306100052,817046963367277257438251",
			"name": "Chebyshev polynomials S(n,51) + S(n-1,51) with Diophantine property.",
			"comment": [
				"(7*a(n))^2 - 53*b(n)^2 = -4 with b(n)=A097838(n) give all positive solutions of this Pell equation."
			],
			"link": [
				"Indranil Ghosh, \u003ca href=\"/A097837/b097837.txt\"\u003eTable of n, a(n) for n = 0..584\u003c/a\u003e",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"Giovanni Lucca, \u003ca href=\"http://forumgeom.fau.edu/FG2019volume19/FG201902index.html\"\u003eInteger Sequences and Circle Chains Inside a Hyperbola\u003c/a\u003e, Forum Geometricorum (2019) Vol. 19, 11-16.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (51,-1).",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e"
			],
			"formula": [
				"a(n) = S(n, 51) + S(n-1, 51) = S(2*n, sqrt(53)), with S(n, x) = U(n, x/2) Chebyshev's polynomials of the second kind, A049310. S(-1, x) = 0 = U(-1, x). S(n, 51)=A097836(n).",
				"a(n) = (-2/7)*i*((-1)^n)*T(2*n+1, 7*i/2) with the imaginary unit i and Chebyshev's polynomials of the first kind. See the T-triangle A053120.",
				"G.f.: (1+x)/(1-51*x+x^2).",
				"a(n) = 51*a(n-1) - a(n-2), a(0)=1, a(1)=52. - _Philippe Deléham_, Nov 18 2008"
			],
			"example": [
				"All positive solutions of Pell equation x^2 - 53*y^2 = -4 are (7=7*1,1), (364=7*52,50), (18557=7*2651,2549), (946043=7*135149,129949), ..."
			],
			"mathematica": [
				"LinearRecurrence[{51,-1}, {1,52}, 30] (* _G. C. Greubel_, Jan 12 2019 *)"
			],
			"program": [
				"(PARI) my(x='x+O('x^30)); Vec((1+x)/(1-51*x+x^2)) \\\\ _G. C. Greubel_, Jan 12 2019",
				"(MAGMA) m:=30; R\u003cx\u003e:=PowerSeriesRing(Integers(), m); Coefficients(R!( (1+x)/(1-51*x+x^2) )); // _G. C. Greubel_, Jan 12 2019",
				"(Sage) ((1+x)/(1-51*x+x^2)).series(x, 30).coefficients(x, sparse=False) # _G. C. Greubel_, Jan 12 2019",
				"(GAP) a:=[1,52];; for n in [3..30] do a[n]:=51*a[n-1]-a[n-2]; od; a; # _G. C. Greubel_, Jan 12 2019"
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Sep 10 2004",
			"references": 4,
			"revision": 34,
			"time": "2020-01-23T03:45:38-05:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}