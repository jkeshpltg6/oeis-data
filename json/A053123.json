{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A053123",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 53123,
			"data": "1,1,-2,1,-4,3,1,-6,10,-4,1,-8,21,-20,5,1,-10,36,-56,35,-6,1,-12,55,-120,126,-56,7,1,-14,78,-220,330,-252,84,-8,1,-16,105,-364,715,-792,462,-120,9,1,-18,136,-560,1365,-2002,1716,-792,165,-10,1,-20,171,-816,2380,-4368,5005,-3432,1287,-220,11,1",
			"name": "Triangle of coefficients of shifted Chebyshev's S(n,x-2)= U(n,x/2-1) polynomials (exponents of x in decreasing order).",
			"comment": [
				"T(n,m)= A053122(n,n-m).",
				"G.f. for row polynomials and row sums same as in A053122.",
				"Unsigned column sequences are A000012, A005843, A014105, A002492 for m=0..3, resp. and A053126-A053131 for m=4..9.",
				"This is also the coefficient triangle for Chebyshev's U(2*n+1,x) polynomials expanded in decreasing odd powers of (2*x): U(2*n+1,x) = Sum_{m=0..n} (T(n,m)*(2*x)^(2*(n-m)+1). See the W. Lang link given in A053125.",
				"Unsigned version is mirror image of A078812. - _Philippe Deléham_, Dec 02 2008"
			],
			"reference": [
				"M. Abramowitz and I. A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, 1964 (and various reprintings), p. 795",
				"Theodore J. Rivlin, Chebyshev polynomials: from approximation theory to algebra and number theory, 2. ed., Wiley, New York, 1990.",
				"Stephen Barnett, \"Matrices: Methods and Applications\", Oxford University Press, 1990, p. 132, 343."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A053123/b053123.txt\"\u003eRows n=0..50 of triangle, flattened\u003c/a\u003e",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e"
			],
			"formula": [
				"T(n, m) = 0 if n\u003cm else (-1)^m*binomial(2*n+1-m, m);",
				"T(n, m) = -2*T(n-1, m-1) + T(n-1, m) - T(n-2, m-2), T(n, -2) = 0, T(-2, m) = 0, T(n, -1) = 0 = T(-1, m), T(0, 0)=1, T(n, m)= 0 if n\u003cm.",
				"G.f. for m-th column (signed triangle): ((-1)^m)*x^m*Po(m+1, x)/(1-x)^(m+1), with Po(k, x) := sum('binomial(k, 2*j+1)*x^j', 'j'=0..floor(k/2)).",
				"The n-th degree polynomial is the characteristic equation for an n X n tridiagonal matrix with (diagonal = all 2's, sub and superdiagonals all -1's and the rest 0's), exemplified by the 4X4 matrix M = [2 -1 0 0 / -1 2 -1 0 / 0 -1 2 -1 / 0 0 -1 2]. - _Gary W. Adamson_, Jan 05 2005",
				"sum(m=0..n, a(n,m)*(c(n))^(2*n-2*m) ) = 1/c(n), where c(n) = 2*cos(Pi/(2*n+3)). - _L. Edson Jeffery_, Sep 13 2013"
			],
			"example": [
				"Triangle begins:",
				"  1;",
				"  1,  -2;",
				"  1,  -4,  3;",
				"  1,  -6, 10,   -4;",
				"  1,  -8, 21,  -20,   5;",
				"  1, -10, 36,  -56,  35,  -6;",
				"  1, -12, 55, -120, 126, -56, 7; ...",
				"E.g. fourth row (n=3) {1,-6,10,-4} corresponds to polynomial S(3,x-2)= x^3-6*x^2+10*x-4."
			],
			"maple": [
				"A053123 := proc(n,m)",
				"    (-1)^m*binomial(2*n+1-m,m) ;",
				"end proc: # _R. J. Mathar_, Sep 08 2013"
			],
			"mathematica": [
				"T[n_, m_]:= (-1)^m*Binomial[2*n+1-m, m]; Table[T[n, m], {n, 0, 11}, {m, 0, n}]//Flatten (* _Jean-François Alcover_, Mar 05 2014, after _R. J. Mathar_ *)"
			],
			"program": [
				"(PARI) for(n=0,10, for(k=0,n, print1((-1)^k*binomial(2*n-k+1,k), \", \"))) \\\\ _G. C. Greubel_, Jul 23 2019",
				"(MAGMA) [(-1)^k*Binomial(2*n-k+1,k): k in [0..n], n in [0..10]]; // _G. C. Greubel_, Jul 23 2019",
				"(Sage) [[(-1)^k*binomial(2*n-k+1,k) for k in (0..n)] for n in (0..10)] # _G. C. Greubel_, Jul 23 2019",
				"(GAP) Flat(List([0..10], n-\u003e List([0..n], k-\u003e (-1)^k*Binomial(2*n-k+1,k) ))); # _G. C. Greubel_, Jul 23 2019"
			],
			"xref": [
				"Cf. A053124, A053122, A172431."
			],
			"keyword": "easy,sign,tabl",
			"offset": "0,3",
			"author": "_Wolfdieter Lang_",
			"references": 14,
			"revision": 37,
			"time": "2019-07-23T04:39:11-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}