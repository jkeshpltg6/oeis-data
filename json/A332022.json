{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A332022",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 332022,
			"data": "0,2,1,5,7,3,8,4,6,13,14,15,18,9,10,11,21,23,12,24,22,16,20,17,19,34,35,36,37,38,39,40,41,47,25,26,27,28,29,30,31,32,55,57,56,60,62,33,58,59,61,63,64,65,66,42,44,43,48,49,45,50,46,51,52,53,54,89",
			"name": "Lexicographically earliest sequence of distinct nonnegative integers such that for any n \u003e= 0, n and a(n) have no common term in their Zeckendorf representations.",
			"comment": [
				"This sequence is a self-inverse permutation of the nonnegative integers.",
				"Apparently, {a(0), ..., a(k)} = {0, ..., k} for infinitely many integers k."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A332022/b332022.txt\"\u003eTable of n, a(n) for n = 0..8360\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A332022/a332022.png\"\u003eScatterplot of (x, y) such that x and y have no common term in their Zeckendorf representations and 0 \u003c= x, y \u003c= 1218\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A332022/a332022.gp.txt\"\u003ePARI program for A332022\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"A003714(n) AND A003714(a(n)) = 0 for any n \u003e= 0 (where AND denotes the bitwise AND operator)."
			],
			"example": [
				"The first terms, alongside the Zeckendorf representation in binary of n and of a(n), are:",
				"  n   a(n)  z(n)   z(a(n))",
				"  --  ----  -----  -------",
				"   0     0      0        0",
				"   1     2      1       10",
				"   2     1     10        1",
				"   3     5    100     1000",
				"   4     7    101     1010",
				"   5     3   1000      100",
				"   6     8   1001    10000",
				"   7     4   1010      101",
				"   8     6  10000     1001",
				"   9    13  10001   100000",
				"  10    14  10010   100001"
			],
			"program": [
				"(PARI) See Links section."
			],
			"xref": [
				"Cf. A003714, A238757 (binary analog), A332565."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Rémy Sigrist_, Apr 23 2020",
			"references": 3,
			"revision": 24,
			"time": "2020-04-27T08:16:30-04:00",
			"created": "2020-04-25T08:44:54-04:00"
		}
	]
}