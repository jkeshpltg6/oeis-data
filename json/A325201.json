{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A325201",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 325201,
			"data": "0,0,0,0,1,0,0,1,2,0,0,1,2,6,0,0,1,2,9,24,0,0,1,2,9,60,120,0,0,1,2,9,64,540,720,0,0,1,2,9,64,620,6120,5040,0,0,1,2,9,64,625,7620,83790,40320,0,0,1,2,9,64,625,7770,113610,1345680,362880,0,0,1,2,9,64,625,7776,117390,1992480,24811920,3628800,0",
			"name": "Square array whose entry A(n,k) is the number of labeled rooted trees on a set of size n where each node has at most k neighbors that are further away from the root than the node itself, for n \u003e= 0, k \u003e= 0, read by descending antidiagonals.",
			"comment": [
				"A preimage constraint on a function is a set of nonnegative integers such that the size of the inverse image of any element is one of the values in that set. View a labeled rooted tree as an endofunction on the set {1,2,...,n} by sending every non-root node to its neighbor that is closer to the root and sending the root to itself.",
				"Thus, A(n,k) is the number of endofunctions on a set of size n with exactly one cyclic point and such that each preimage has at most k entries."
			],
			"link": [
				"B. Otto, \u003ca href=\"https://arxiv.org/abs/1903.00542\"\u003eCoalescence under Preimage Constraints\u003c/a\u003e, arXiv:1903.00542 [math.CO], 2019, Corollaries 5.3 and 7.8."
			],
			"formula": [
				"A(n,k) = (n-1)! * [x^(n-1)] e_k(x)^n, where e_k(x) is the truncated exponential 1 + x + x^2/2! + ... + x^k/k!. When k\u003e1, the link above yields explicit constants c_k, r_k so that the columns are asymptotically c_k * n^(-3/2) * r_k^-n. Stirling's approximation gives column k=1, and column k=0 is 0."
			],
			"example": [
				"Array begins:",
				"           0           0           0           0           0 ...",
				"           0           1           1           1           1 ...",
				"           0           2           2           2           2 ...",
				"           0           6           9           9           9 ...",
				"           0          24          60          64          64 ...",
				"           0         120         540         620         625 ...",
				"           0         720        6120        7620        7770 ...",
				"           0        5040       83790      113610      117390 ...",
				"           0       40320     1345680     1992480     2088520 ...",
				"           0      362880    24811920    40194000    42771960 ...",
				"           0     3628800   516650400   916927200   991090800 ...",
				"           0    39916800 11992503600 23341071600 25635767850 ...",
				"         ..."
			],
			"mathematica": [
				"e[k_][x_] := Sum[x^j/j!, {j, 0, k}];",
				"A[0, _] = A[_, 0] = 0; A[n_, k_] := (n-1)! Coefficient[e[k][x]^n, x, n-1];",
				"Table[A[n-k, k], {n, 0, 11}, {k, n, 0, -1}] (* _Jean-François Alcover_, Jul 06 2019 *)"
			],
			"program": [
				"(Python)",
				"# print first num_entries entries in column k",
				"import math, sympy; x=sympy.symbols('x')",
				"k=5; num_entries = 64",
				"P=range(k+1); eP=sum([x**d/math.factorial(d) for d in P]); r = [0,1]; curr_pow = eP",
				"for term in range(1, num_entries-1):",
				"...curr_pow=(curr_pow*eP).expand()",
				"...r.append(curr_pow.coeff(x**term)*math.factorial(term))",
				"print(r)"
			],
			"xref": [
				"Column 0: A000004.",
				"Column 1 is A000142, except at n=0 term.",
				"Columns 2-9: A036774, A036775, A036776, A036777, A325205, A325206, A325207, A325208.",
				"A(n,n) gives A152917.",
				"Similar array for arbitrary endofunctions (without limitation on the number of cyclic points) with the same preimage condition {i\u003e=0 | i\u003c=k}: A306800."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "1,9",
			"author": "_Benjamin Otto_, Apr 08 2019",
			"references": 6,
			"revision": 19,
			"time": "2019-07-13T01:20:48-04:00",
			"created": "2019-06-23T11:50:48-04:00"
		}
	]
}