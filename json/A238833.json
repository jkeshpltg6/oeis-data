{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A238833",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 238833,
			"data": "0,1,0,2,2,7,16,40,101,246,615,1504,3724,9147,22567,55541,136884,337128,830628,2046145,5040932,12418320,30593281,75367352,185670647,457405836,1126836394,2776001211,6838779857,16847579205,41504619640,102248123906,251891939366,620544865783,1528734638988,3766092860744",
			"name": "a(n) = n-1 for n \u003c= 2; thereafter a(n) = A238824(n-2) + A238832(n-1).",
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A238833/b238833.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"V. M. Zhuravlev, \u003ca href=\"http://www.mccme.ru/free-books/matpros/mph.pdf\"\u003eHorizontally-convex polyiamonds and their generating functions\u003c/a\u003e, Mat. Pros. 17 (2013), 107-129 (in Russian). See the sequence j(n).",
				"\u003ca href=\"/index/Rec\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,5,-1,-7,-1,6,6,1,-1)."
			],
			"formula": [
				"G.f.: -x^2*(x^8+2*x^7+x^6-2*x^5-2*x^4-x^3+3*x^2+x-1) / ((x+1)^2*(x^7-3*x^6-x^5-x^4+4*x^3-3*x+1)). - _Colin Barker_, Mar 20 2014"
			],
			"maple": [
				"g:=proc(n) option remember; local t1; t1:=[2,3,6,14,34,84,208,515];",
				"if n \u003c= 7 then t1[n] else",
				"3*g(n-1)-4*g(n-3)+g(n-4)+g(n-5)+3*g(n-6)-g(n-7); fi; end proc;",
				"[seq(g(n),n=1..32)]; # A238823",
				"d:=proc(n) option remember; global g; local t1; t1:=[0,1];",
				"if n \u003c= 2 then t1[n] else",
				"g(n-1)-2*d(n-1)-d(n-2); fi; end proc;",
				"[seq(d(n),n=1..32)]; # A238824",
				"p:=proc(n) option remember; global d; local t1; t1:=[0,0,0,1];",
				"if n \u003c= 4 then t1[n] else",
				"p(n-2)+p(n-3)+2*(d(n-3)+d(n-4)); fi; end proc;",
				"[seq(p(n),n=1..32)]; # A238825",
				"h:=n-\u003ep(n+3)-p(n+1); [seq(h(n),n=1..32)]; #A238826",
				"r:=proc(n) option remember; global p; local t1; t1:=[0,0,0,0];",
				"if n \u003c= 4 then t1[n] else",
				"r(n-2)+p(n-3); fi; end proc;",
				"[seq(r(n),n=1..32)]; # A238827",
				"b:=n-\u003e if n=1 then 0 else d(n-1)+p(n); fi; [seq(b(n),n=1..32)]; #A238828",
				"a:=n-\u003eg(n)-h(n); [seq(a(n),n=1..32)]; #A238829",
				"i:=proc(n) option remember; global b,r; local t1; t1:=[0,0];",
				"if n \u003c= 2 then t1[n] else",
				"i(n-2)+b(n-1)+r(n); fi; end proc;",
				"[seq(i(n),n=1..32)]; # A238830",
				"q:=n-\u003e if n\u003c=2 then 0 else r(n)+i(n-2); fi;",
				"[seq(q(n),n=1..45)]; # A238831",
				"e:=n-\u003e if n\u003c=1 then 0 else d(n-1)+i(n-1); fi;",
				"[seq(e(n),n=1..45)]; # A238832",
				"j:=n-\u003e if n\u003c=2 then n-1 else d(n-2)+e(n-1); fi;",
				"[seq(j(n),n=1..45)]; # A238833"
			],
			"mathematica": [
				"CoefficientList[Series[- x (x^8 + 2 x^7 + x^6 - 2 x^5 - 2 x^4 - x^3 + 3 x^2 + x - 1)/((x + 1)^2 (x^7 - 3 x^6 - x^5 - x^4 + 4 x^3 - 3 x + 1)), {x, 0, 40}], x] (* _Vincenzo Librandi_, Mar 21 2014 *)",
				"LinearRecurrence[{1,5,-1,-7,-1,6,6,1,-1},{0,1,0,2,2,7,16,40,101,246},40] (* _Harvey P. Dale_, Jul 23 2021 *)"
			],
			"program": [
				"(PARI) concat(0, Vec(-x^2*(x^8+2*x^7+x^6-2*x^5-2*x^4-x^3+3*x^2+x-1)/((x+1)^2*(x^7-3*x^6-x^5-x^4+4*x^3-3*x+1)) + O(x^100))) \\\\ _Colin Barker_, Mar 20 2014",
				"(MAGMA) m:=40; R\u003cx\u003e:=LaurentSeriesRing(RationalField(), m); [0] cat Coefficients(R! -x^2*(x^8+2*x^7+x^6-2*x^5-2*x^4-x^3+3*x^2+x-1) / ((x+1)^2*(x^7-3*x^6-x^5-x^4+4*x^3-3*x+1))); // _Vincenzo Librandi_, Mar 21 2014"
			],
			"xref": [
				"Cf. A238823-A238832."
			],
			"keyword": "nonn,easy",
			"offset": "1,4",
			"author": "_N. J. A. Sloane_, Mar 08 2014",
			"references": 2,
			"revision": 16,
			"time": "2021-07-23T11:18:12-04:00",
			"created": "2014-03-08T10:50:02-05:00"
		}
	]
}