{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A223732",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 223732,
			"data": "3,6,9,11,14,17,18,19,21,22,26,27,29,30,34,35,42,43,45,46,49,50,53,61,65,67,70,73,75,78,82,91,93,97,106,109,115,133,142,145,147,157,163,169,190,193,202,205,235,253,265,277,298,397,403,427,442,445,505,793",
			"name": "Positive numbers that are the sum of three nonzero squares with no common factor \u003e 1 in exactly one way.",
			"comment": [
				"These are the increasingly ordered numbers a(n) for which A233730(a(n)) = 1. See also A233731. These are the numbers n with exactly one representation as a primitive sum of three nonzero squares (not taking into account the order of the three terms, and the number to be squared for each term is taken positive).",
				"Conjecture: 793 = 6^2 + 9^2 + 26^2 is the largest element of this sequence. - _Alois P. Heinz_, Apr 06 2013"
			],
			"link": [
				"Eugen J. Ionascu, \u003ca href=\"http://arxiv.org/abs/1508.03643\"\u003eEhrhart polynomial for lattice squares, cubes and hypercubes\u003c/a\u003e, arXiv:1508.03643 [math.NT], 2015."
			],
			"formula": [
				"This sequence lists the increasingly ordered members of the set S1 := {m positive integer | m = a^2 + b^2 + c^2, 0 \u003c a \u003c= b \u003c= c, gcd(a,b,c) = 1, with only one such solution for this m}."
			],
			"example": [
				"a(1) = 3 because there is no solution for m = 1 and 2 as a primitive sum of three nonzero squares, and m = 3 = 1^2 + 1^2 + 1^2 is the only solution with [a,b,c] = [1,1,1].",
				"a(5) = 14 because 14 is the fifth largest member of the set S1, and [a,b,c] = [1,2,3] denotes this unique representation for m = 14."
			],
			"mathematica": [
				"threeSquaresCount[n_] := Length[ Select[ PowersRepresentations[n, 3, 2], Times @@ #1 != 0 \u0026\u0026 GCD @@ #1 == 1 \u0026 ]]; Select[ Range[800], threeSquaresCount[#] == 1 \u0026] (* _Jean-François Alcover_, Jun 21 2013 *)"
			],
			"xref": [
				"Cf. A233730, A233731, A233733, A233734."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Wolfdieter Lang_, Apr 05 2013",
			"references": 5,
			"revision": 18,
			"time": "2015-08-18T04:42:35-04:00",
			"created": "2013-04-06T03:57:22-04:00"
		}
	]
}