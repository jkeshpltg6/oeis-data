{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A166242",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 166242,
			"data": "1,2,4,2,4,8,4,2,4,8,16,8,4,8,4,2,4,8,16,8,16,32,16,8,4,8,16,8,4,8,4,2,4,8,16,8,16,32,16,8,16,32,64,32,16,32,16,8,4,8,16,8,16,32,16,8,4,8,16,8,4,8,4,2,4,8,16,8,16,32,16,8,16,32,64,32,16,32,16,8,16,32,64,32,64,128,64,32,16,32",
			"name": "Sequence generated from A014577, the dragon curve.",
			"comment": [
				"Rows of A164281 tend to A166242. Subsets of the first 2^n terms can be parsed into a binomial frequency of powers of 2; for example, the first 16 terms has as frequency of (1, 4, 6, 4, 1): (one 1, four 2's, six 4's, four 8's, and one 16.)."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A166242/b166242.txt\"\u003eTable of n, a(n) for n = -1..8191\u003c/a\u003e",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e"
			],
			"formula": [
				"Let a(-1) = 1, then a(n+1) = 2*a(n) if A014577(n+1) = 1. If A014577(n+1) = 0, then a(n+1) = (1/2)*a(n).",
				"As a recursive string in subsets of 2^n terms, the next subset = twice each term of current string, reversed, and appended.",
				"Apparently, a(n) = 2^A000120(A003188(n+1)). - _Rémy Sigrist_, Feb 21 2021"
			],
			"example": [
				"From the Dragon curve, A014577:",
				"...1...1...0...1...1...0...0...1... generates A166242:",
				"1..2...4...2...4...8...4...2...4... given A166242(-1)=1.",
				"By recursion, given the first four terms: (1, 2, 4, 2); reverse, double, and append to (1, 2, 4, 2) getting (1, 2, 4, 2, 4, 8, 4, 2,...)."
			],
			"mathematica": [
				"FoldList[If[EvenQ[((#2 + 1)/2^IntegerExponent[#2 + 1, 2] - 1)/2], 2 #1, #1/2] \u0026, 1, Range[0, 89]] (* _Michael De Vlieger_, Jul 29 2017, after _Jean-François Alcover_ at A014577 *)"
			],
			"program": [
				"(Scheme, with memoization-macro definec)",
				";; Because definec does not work well with offset -1, we define an offset-0 based version of recurrence:",
				"(definec (A166242off0 n) (if (zero? n) 1 (* (expt 2 (- (* 2 (A014577 (- n 1))) 1)) (A166242off0 (- n 1)))))",
				";; which the offset -1 version will invoke:",
				"(define (A166242 n) (A166242off0 (+ 1 n)))",
				"(define (A014577 n) (- 1 (A000035 (/ (- (A000265 (+ 1 n)) 1) 2))))",
				";; Scheme-code for A000035 and A000265 given under respective entries.",
				";; _Antti Karttunen_, Jul 27 2017"
			],
			"xref": [
				"Cf. A000120, A003188, A014577, A164281."
			],
			"keyword": "nonn",
			"offset": "-1,2",
			"author": "_Gary W. Adamson_, Oct 10 2009",
			"ext": [
				"More terms from _Antti Karttunen_, Jul 27 2017"
			],
			"references": 4,
			"revision": 19,
			"time": "2021-02-21T13:56:08-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}