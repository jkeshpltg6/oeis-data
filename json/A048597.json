{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A048597",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 48597,
			"data": "1,2,3,4,6,8,12,18,24,30",
			"name": "Very round numbers: reduced residue system consists of only primes and 1.",
			"comment": [
				"According to Ribenboim, Schatunowsky and Wolfskehl independently showed that 30 is the largest element in the sequence. This gives a lower bound for the maximum of the smallest prime in a, a+d, a+2d, ... taken over all a with 1 \u003c a \u003c d and gcd(a,d) = 1 for d \u003e 30 [see Ribenboim].",
				"It appears that 2, 4, 6, 10, 12 are all the numbers n with the property that every number m in the range n \u003c m \u003c 2n that is coprime to n is also prime. - _Ely Golden_, Dec 05 2016",
				"Golden's guess is true. See a proof in the links section. - _FUNG Cheok Yin_, Jun 19 2021"
			],
			"reference": [
				"A. H. Beiler, Recreations in the Theory of Numbers, page 91.",
				"H. Rademacher and O. Toeplitz, Von Zahlen und Figuren, Springer Verlag, Berlin, 1933, Zweite Auflage, see last chapter.",
				"H. Rademacher \u0026 O. Toeplitz, The Enjoyment of Mathematics, pp. 187-192 Dover Publications, NY 1990.",
				"P. Ribenboim, The little book of big primes, Chapter on primes in arithmetic progression.",
				"J. E. Roberts, Lure of Integers, pp. 179-180 MAA 1992.",
				"J. V. Uspensky and M. A. Heaslet, Elementary Number Theory, McGraw-Hill, NY, 1939, p. 89."
			],
			"link": [
				"H. Bonse, \u003ca href=\"https://archive.org/stream/archivdermathem31unkngoog#page/n307/mode/2up\"\u003eÜber eine bekannte Eigenshaft der Zahl 30 und ihre Verallgemeinerung\u003c/a\u003e, Archiv d. Math. u. Physik (3) vol. 12 (1907) 292-295.",
				"Ross Honsberger, \u003ca href=\"https://www.jstor.org/stable/3026742\"\u003eMathematical Gems\u003c/a\u003e, The Two-Year College Mathematics Journal, Vol. 10, No. 3 (Jun., 1979), pp. 195-197 (3 pages).",
				"Ross Honsberger, \u003ca href=\"https://archive.org/details/MathematicalDiamonds/page/n87/mode/2up\"\u003eTwo distinguished integers\u003c/a\u003e, in Mathematical Diamonds, MAA, 2003, see p. 79. [Added by _N. J. A. Sloane_, Jul 05 2009]",
				"Bill Taylor, \u003ca href=\"http://mathforum.org/epigone/sci.math/chaxclixsnerm\"\u003ePosting to sci.math, Sep 13 1999\u003c/a\u003e [Broken link]",
				"Fung Cheok Yin, \u003ca href=\"http://oeis.org/wiki/User:FUNG_Cheok_Yin/proof(i)_A048597\"\u003eA property of the set \"2, 4, 6, 10, 12\"\u003c/a\u003e, Dec 24 2020."
			],
			"example": [
				"The reduced residue systems of these numbers are as follows: {{1, {1}}, {2, {1}}, {3, {1, 2}}, {4, {1, 3}}, {6, {1, 5}}, {8, {1, 3, 5, 7}}, {12, {1, 5, 7, 11}}, {18, {1, 5, 7, 11, 13, 17}}, {24, {1, 5, 7, 11, 13, 17, 19, 23}}, {30, {1, 7, 11, 13, 17, 19, 23, 29}}}."
			],
			"mathematica": [
				"Select[Range[10^3], Function[n, Times @@ Boole@ Map[Or[# == 1, PrimeQ@ #] \u0026, Select[Range@ n, CoprimeQ[#, n] \u0026]] == 1]] (* _Michael De Vlieger_, Dec 13 2016 *)"
			],
			"program": [
				"(PARI) is(n)=forcomposite(k=2,n-1,if(gcd(n,k)==1, return(0))); 1 \\\\ _Charles R Greathouse IV_, Apr 28 2015"
			],
			"xref": [
				"The sequences consists of the n with A036997(n)=0."
			],
			"keyword": "fini,full,nonn",
			"offset": "1,2",
			"author": "_Labos Elemer_",
			"ext": [
				"Additional comments from Ulrich Schimke (ulrschimke(AT)aol.com), May 29 2001"
			],
			"references": 30,
			"revision": 59,
			"time": "2021-07-03T18:49:28-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}