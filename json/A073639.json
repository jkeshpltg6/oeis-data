{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A073639",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 73639,
			"data": "2,3,4,6,7,15,22,60,63,127,153,471,532,865,900,1366",
			"name": "Numbers k such that x^k + x + 1 is a primitive polynomial modulo 2.",
			"comment": [
				"Subsequence of A002475, which gives k for which the polynomial x^k + x + 1 is irreducible modulo 2. Term m of A002475 belongs to this sequence iff A046932(m) = 2^m - 1.",
				"Note that a(16) = 1366 = A002475(23). For k = A002475(24) and A002475(25), polynomial x^k + x + 1 is not primitive modulo 2, so a(17) \u003e= A002475(26) = 4495.",
				"The following large terms of A002475 do not belong here: 53484, 62481, 83406, 103468. - _Max Alekseyev_, Aug 18 2015"
			],
			"link": [
				"Joerg Arndt, \u003ca href=\"http://www.jjj.de/fxt/#fxtbook\"\u003eMatters Computational (The Fxtbook)\u003c/a\u003e, section 40.9.3 \"Irreducible trinomials of the form 1 + x^k + x^d\", p.850",
				"I. F. Blake, S. Gao and R. J. Lambert, \u003ca href=\"http://dx.doi.org/10.1007/3-540-57936-2_27\"\u003eConstructive problems for irreducible polynomials over finite fields\u003c/a\u003e, in Information Theory and Applications, LNCS 793, Springer-Verlag, Berlin, 1994, 1-23, See Table 2.",
				"R. P. Brent, \u003ca href=\"http://wwwmaths.anu.edu.au/~brent/trinom-old.html\"\u003eSearching for primitive trinomials (mod 2)\u003c/a\u003e",
				"R. P. Brent, S. Larvala and P. Zimmermann, \u003ca href=\"http://wwwmaths.anu.edu.au/~brent/pd/rpb199.pdf\"\u003eA fast algorithm for testing reducibility of trinomials ...\u003c/a\u003e, Math. Comp. 72 (2003), 1443-1452.",
				"N. Zierler, \u003ca href=\"http://dx.doi.org/10.1016/S0019-9958(69)90631-7\"\u003ePrimitive trinomials whose degree is a Mersenne exponent\u003c/a\u003e, Information and Control 15 1969 67-69.",
				"N. Zierler, \u003ca href=\"http://dx.doi.org/10.1016/S0019-9958(70)90264-0\"\u003eOn x^n+x+1 over GF(2)\u003c/a\u003e, Information and Control 16 1970 502-505.",
				"N. Zierler and J. Brillhart, \u003ca href=\"http://dx.doi.org/10.1016/S0019-9958(68)90973-X\"\u003eOn primitive trinomials (mod 2)\u003c/a\u003e, Information and Control 13 1968 541-554.",
				"N. Zierler and J. Brillhart, \u003ca href=\"http://dx.doi.org/10.1016/S0019-9958(69)90356-8\"\u003eOn primitive trinomials (mod 2), II\u003c/a\u003e, Information and Control 14 1969 566-569.",
				"\u003ca href=\"/index/Tri#trinomial\"\u003eIndex entries for sequences related to trinomials over GF(2)\u003c/a\u003e"
			],
			"mathematica": [
				"Select[Range[2, 1000], PrimitivePolynomialQ[x^# + x + 1, 2] \u0026] (* _Robert Price_, Sep 19 2018 *)"
			],
			"xref": [
				"Cf. A002475, A073571, A057486."
			],
			"keyword": "nonn,nice,hard,more",
			"offset": "1,1",
			"author": "_Richard P. Brent_ and _Paul Zimmermann_, Sep 05 2002",
			"references": 6,
			"revision": 38,
			"time": "2021-07-16T13:17:54-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}