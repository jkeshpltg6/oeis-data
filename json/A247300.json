{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A247300",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 247300,
			"data": "0,1,3,7,17,40,94,222,526,1252,2994,7191,17343,41989,102023,248712,608168,1491349,3666685,9037003,22323243,55259206,137058248,340567477,847711177,2113455657,5277115687,13195311961,33038994039,82829585094,207905352180",
			"name": "Number of h- and H-steps at level 0 in all lattice paths in B(n).",
			"comment": [
				"B(n) is the set of lattice paths of weight n that start in (0,0), end on the horizontal axis and never go below this axis, whose steps are of the following four kinds: h = (1,0) of weight 1, H = (1,0) of weight 2, u = (1,1) of weight 2, and d = (1,-1) of weight 1. The weight of a path is the sum of the weights of its steps.",
				"a(n) = Sum(k*A247299(n,k), 0\u003c=k\u003c=n)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A247300/b247300.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"M. Bona and A. Knopfmacher, \u003ca href=\"http://dx.doi.org/10.1007/s00026-010-0060-7\"\u003eOn the probability that certain compositions have the same number of parts\u003c/a\u003e, Ann. Comb., 14 (2010), 291-306."
			],
			"formula": [
				"G.f.:  4*z*(1 + z)/(1 - z - z^2 +sqrt((1 + z + z^2)*(1 - 3*z + z^2)))^2.",
				"a(n) ~ sqrt(525 + 235*sqrt(5)) * ((3 + sqrt(5))/2)^n / (sqrt(2*Pi)*n^(3/2)). - _Vaclav Kotesovec_, Mar 06 2016",
				"Equivalently, a(n) ~ 5^(3/4) * phi^(2*n + 4) / (sqrt(Pi) * n^(3/2)), where phi = A001622 is the golden ratio. - _Vaclav Kotesovec_, Dec 06 2021"
			],
			"example": [
				"a(3)=7 because in B(3) = {ud, hH, Hh, hhh} all h- and H-steps are at level 0."
			],
			"maple": [
				"G := 4*z*(1+z)/(1-z-z^2+sqrt((1+z+z^2)*(1-3*z+z^2)))^2: Gser := series(G, z = 0, 33): seq(coeff(Gser, z, n), n = 0 .. 30);",
				"# second Maple program:",
				"b:= proc(n, y) option remember; `if`(y\u003c0 or y\u003en or n\u003c0, 0,",
				"      `if`(n=0, [1, 0], (p-\u003e p+`if`(y=0, [0, p[1]], 0))",
				"      (b(n-1, y) +b(n-2, y)) +b(n-2, y+1) +b(n-1, y-1)))",
				"    end:",
				"a:= n-\u003e b(n, 0)[2]:",
				"seq(a(n), n=0..50);  # _Alois P. Heinz_, Sep 17 2014"
			],
			"mathematica": [
				"b[n_, y_] := b[n, y] = If[y\u003c0 || y\u003en || n\u003c0, 0, If[n == 0, {1, 0}, Function[{p}, p + If[y == 0, {0, p[[1]]}, 0]][b[n-1, y] + b[n-2, y]] + b[n-2, y+1] + b[n-1, y-1]]] ; a[n_] := b[n, 0][[2]]; Table[a[n], {n, 0, 50}] (* _Jean-François Alcover_, May 27 2015, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A247299."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Emeric Deutsch_, Sep 17 2014",
			"references": 2,
			"revision": 14,
			"time": "2021-12-06T08:25:49-05:00",
			"created": "2014-09-17T19:47:10-04:00"
		}
	]
}