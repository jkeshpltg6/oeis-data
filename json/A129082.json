{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A129082",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 129082,
			"data": "1,3,11,25,123,53,275,581,5898,6337,81839,52193,794409,929481,611743,1609819,24076913,6686545,176364550,32690593,9049485,10684919,281305624,439838742,20192641459,17176118816,107883019372,142161870055,2874353551691,3214687921599",
			"name": "a(n) = numerator of b(n): b(n) = the maximum possible value for a continued fraction whose terms are a permutation of the terms of the simple continued fraction for H(n) = sum{k=1 to n} 1/k, the n-th harmonic number.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A129082/b129082.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e"
			],
			"example": [
				"The continued fraction for H(5) = 137/60 is [2;3,1,1,8]. The maximum value a continued fraction can have with these same terms in some order is [8;1,3,1,2] = 123/14."
			],
			"maple": [
				"H := proc(n) add(1/k,k=1..n) ; end: Ltoc := proc(L) numtheory[nthconver](L,nops(L)-1) ; end: r := proc(n) option remember ; local m,rL,rp,L ; if n = 1 then 1; else rL := numtheory[cfrac](H(n),'quotients') ; rp := combinat[permute](rL) ; m := Ltoc(rL) ; for L in rp do m := max(m, Ltoc(L)) ; od: m ; fi; end: A129082 := proc(n) numer(r(n)) ; end: for n from 1 do printf(\"%d,\\n\", A129082(n)) ; od: # _R. J. Mathar_, Jul 30 2009",
				"# second Maple program:",
				"with(numtheory):",
				"H:= proc(n) option remember; `if`(n=1, 1, H(n-1)+1/n) end:",
				"r:= proc(l) local j;",
				"      infinity; for j from nops(l) to 1 by -1 do l[j]+1/% od",
				"    end:",
				"sh:= proc(l) local ll, h, s, m; ll:= []; h:= nops(l); s:= 1; m:= h; while s\u003c=h do ll:= [ll[], l[m]]; if m=h then h:= h-1; m:= s else s:= s+1; m:= h fi od; ll end:",
				"a:= n-\u003e numer(r(sh(sort(cfrac(H(n), 'quotients'))))):",
				"seq(a(n), n=1..40);  # _Alois P. Heinz_, Aug 04 2009"
			],
			"mathematica": [
				"r[l_] := Module[{lj, j}, For[lj = Infinity; j = Length[l], j \u003e= 1, j--, lj = l[[j]] + 1/lj]; lj];",
				"sh[l_] := Module[{ll, h, s, m}, ll = {}; h = Length[l]; s = 1; m = h; While[s \u003c= h, ll = Append[ll, l[[m]]]; If[m == h, h--; m = s, s++; m = h ]]; ll];",
				"a[n_] := Numerator[ r[ sh[ Sort[ ContinuedFraction[ HarmonicNumber[n]]]]]];",
				"Table[a[n], {n, 1, 40}] (* _Jean-François Alcover_, Mar 20 2017, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(MAGMA) Q:=Rationals(); [ Numerator(Max([ r: r in R ])) where R:=[ c[1,1]/c[2,1]: c in C ] where C:=[ Convergents(s): s in S ] where S:=Seqset([ [m(p[i]):i in [1..#x] ]: p in P ]) where m:=map\u003c x-\u003ey | [\u003cx[i],y[i]\u003e:i in [1..#x] ] \u003e where P:=Permutations(Seqset(x)) where x:=[1..#y]: y in [ ContinuedFraction(h): h in [ \u0026+[ 1/k: k in [1..n] ]: n in [1..8] ] ] ]; // _Klaus Brockhaus_, Jul 31 2009"
			],
			"xref": [
				"Cf. A129083, A129084, A129085."
			],
			"keyword": "frac,nonn",
			"offset": "1,2",
			"author": "_Leroy Quet_, Mar 28 2007",
			"ext": [
				"6 more terms from _R. J. Mathar_, Jul 30 2009",
				"Extended beyond a(12) _Alois P. Heinz_, Aug 04 2009"
			],
			"references": 4,
			"revision": 25,
			"time": "2017-03-21T04:10:10-04:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}