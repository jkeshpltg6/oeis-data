{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A095076",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 95076,
			"data": "0,1,1,1,0,1,0,0,1,0,0,0,1,1,0,0,0,1,0,1,1,1,0,0,0,1,0,1,1,0,1,1,1,0,1,0,0,0,1,0,1,1,0,1,1,1,0,0,1,1,1,0,1,0,0,1,0,0,0,1,0,1,1,0,1,1,1,0,0,1,1,1,0,1,0,0,0,1,1,1,0,1,0,0,1,0,0,0,1,1,0,0,0,1,0,1,1,0,1,1,1,0",
			"name": "Parity of 1-fibits in Zeckendorf expansion A014417(n).",
			"comment": [
				"Let u = A000201 = (lower Wythoff sequence) and v = A001950 = (upper Wythoff sequence.  Conjecture: This sequence is the sequence p given by p(1) = 0 and p(u(k)) = p(k); p(v(k)) = 1-p(k). [_Clark Kimberling_, Apr 15 2011]",
				"[base 2] 0.111010010001100... = 0.9105334708635617... [_Joerg Arndt_, May 13 2011]",
				"From _Michel Dekking_, Nov 28 2019: (Start)",
				"This sequence is a morphic sequence.",
				"Let the morphism sigma be given by",
				"      sigma(1) = 12, sigma(2) = 4, sigma(3) = 1, sigma(4) = 43,",
				"and let the letter-to-letter map lambda be given by",
				"      lambda(1) = 0, lambda(2) = 1, lambda(3) = 0, lambda(4) = 1.",
				"Then a(n) = lambda(x(n)), where x(0)x(1)... = 1244343... is the fixed point of sigma starting with 1.",
				"See footnote number 4 in the paper by Emmanuel Ferrand. (End)",
				"From _Michel Dekking_, Nov 29 2019: (Start)",
				"Proof of Kimberling's conjecture, by virtue of the four symbol morphism sigma in the previous comment.",
				"We first show that the fixed point x = 1244343143112... of sigma has the remarkable property that the letters 1 and 4 exclusively occur at positions u(k), k=1,2,..., and the letters 2 and 3 exclusively occur at positions v(k) k=1,2,....",
				"To see this, let alpha be the Fibonacci morphism on the alphabet {a,b}:",
				"      alpha(a) = ab, alpha(b) = a.",
				"It is well known that the lower Wythoff sequence u gives the positions of a in the fixed point abaababaab... of alpha, and that the upper Wythoff sequence v gives the positions of b in this infinite Fibonacci word.",
				"Let pi be the projection from {1,2,3,4} to {a,b} given by",
				"      pi(1) = pi(4) = a,    pi(2) = pi(3) = b.",
				"One easily checks that  (composition of morphisms)",
				"      pi sigma = alpha pi.",
				"This implies the remarkable property stated above.",
				"What remains to be proved is that lambda(x(u(k)) = a(k) and lambda(x(v(k)) = a(k), where lambda is the letter-to-letter map in the previous comment. We tackle this problem by an extensive analysis of the return words of the letter 1 in the sequence x= 1244343143112...",
				"These are the words occurring in x which start with 1 and have no other occurrences of 1 in them.",
				"One easily finds that they are given by",
				"      A:=1, B:=12, C:=124, D:=143, E:=1243, F:=12443, G:=1244343.",
				"These words partition x. Moreover, sigma induces a morphism rho on the alphabet {A,B,C,D,E,F,G} given by",
				"      rho(A) = B, rho(B) =C, rho(C) = F, rho(D) = EA,",
				"      rho(E) = FA, rho(F) = GA, rho(G) = GDA.",
				"CLAIM 1: Let mu be the letter-to-letter map given by",
				"      mu(A)=mu(B) = 1, mu(C)=mu(D)=mu(E) = 2, mu(F) = 3,  mu(G) = 4,",
				"then mu(R) = (s(n+1)-s(n)), where R = GDAEABFAB... is the unique fixed point of rho and s = 1,5,7,8,10,... is the sequence of positions of 1 in (x(u(k)).",
				"Proof of CLAIM 1:  The 1's in x occur exclusively at positions u(k), so if we want the differences s(n+1)-s(n), we can strip the 2's and 3's from the return words of 1, and record how long it takes to the next 1. This is the length of the stripped words A~=1, B~=1, C~=14, ... which is given by mu.",
				"CLAIM 2: Let delta be the 'decoration' morphism given by",
				"    delta(A) = 1,  delta(B) = 2, delta(C) = 3,  delta(D) = 21,",
				"      delta(E) = 31, delta(F) = 41,  delta(G) = 421,",
				"then delta(R) = (t(n+1-t(n)), where rho(R) = R, and t is the sequence of positions of 1 or 3 in the sequence x = 1244343143112....",
				"Proof of CLAIM 2:  We have to record the differences in the occurrences of 1 and 3 in the return words of 1. These are given by delta. For example: F = 12443, where 1 and 3 occur at position 1 and 5; and the next 1 will occur at the beginning of any of the seven words A,...,G.",
				"If we combine CLAIM 1 and CLAIM 2 with lambda(1) = lambda(3) = 0, we obtain the first half of Kimberling's conjecture, simply because delta = mu rho, and rho(R) = R.",
				"The second half of the conjecture is obtained in a similar way. (End)",
				"From _Michel Dekking_, Mar 10 2020: (Start)",
				"The fact that this sequence is a morphic sequence can be easily deduced from the morphism tau given in A007895:",
				"      tau((j,0)) = (j,0) (j+1,1),",
				"      tau((j,1)) = (j,0).",
				"The sum of digits of a number n in Zeckendorf expansion is given by projecting the n-th letter in the fixed point of tau starting with (0,0) on its first coordinate: (j,i)-\u003ej for i=0,1.",
				"If we consider all the j's modulo 2, we obtain a morphism sigma' on 4 letters:",
				"      sigma'((0,0)) = (0,0) (1,1),",
				"      sigma'((0,1)) = (0,0),",
				"      sigma'((1,0)) = (1,0)(0,1),",
				"      sigma'((1,1)) = (1,0).",
				"The change of alphabet (0,0)-\u003e1, (0,1)-\u003e3, (1,0)-\u003e4, (1,1)-\u003e2, turns sigma' into sigma given in my Nov 28, 2019 comment. The projection map turns into the map 1-\u003e0, 2-\u003e1, 3-\u003e0, 4-\u003e1, as in my Nov 28 2019 comment.",
				"(End)",
				"Another proof that this sequence is a morphic sequence has been given in the monograph by Allouche and Shallit. They demonstrate on page 239 that (a(n)) is a letter to letter projection of a fixed point of a morphism on an alphabet of six letters. However, it can been seen easily that two pairs of letters can be merged, yielding a morphism on an alphabet of four letters, which after a change of alphabet equals the morphism from my previous comments. - _Michel Dekking_, Jun 25 2020"
			],
			"reference": [
				"J.-P. Allouche and J. Shallit, Automatic Sequences, Cambridge Univ. Press, 2003, Examples 7.8.2 and 7.8.4."
			],
			"link": [
				"Joerg Arndt, \u003ca href=\"http://www.jjj.de/fxt/#fxtbook\"\u003eMatters Computational (The Fxtbook)\u003c/a\u003e, section 38.11.1, pp. 754-756",
				"E. Ferrand, \u003ca href=\"https://doi.org/10.37236/948\"\u003eAn analogue of the Thue-Morse sequence\u003c/a\u003e, The Electronic Journal of Combinatorics, Volume 14 (2007), R30.",
				"Leonard Rozendaal, \u003ca href=\"https://hal.archives-ouvertes.fr/hal-01552281\"\u003ePisano word, tesselation, plane-filling fractal\u003c/a\u003e, Preprint, hal-01552281, 2017.",
				"Jeffrey Shallit, \u003ca href=\"https://arxiv.org/abs/2010.10956\"\u003eSubword complexity of the Fibonacci-Thue-Morse sequence: the proof of Dekking's conjecture\u003c/a\u003e, arXiv:2010.10956 [cs.DM], 2020.",
				"\u003ca href=\"/index/Ch#char_fns\"\u003eIndex entries for characteristic functions\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A007895(n) mod 2. - _Michel Dekking_, Mar 10 2020"
			],
			"maple": [
				"A095076 := proc(n)",
				"    modp(A007895(n),2) ;",
				"end proc:",
				"seq(A095076(n),n=0..40) ; # _R. J. Mathar_, Sep 22 2020"
			],
			"mathematica": [
				"r=(1+5^(1/2))/2; u[n_] := Floor[n*r];  (* A000201 *)",
				"a[1] = 0; h = 128;",
				"c = (u[#1] \u0026) /@ Range[2h];",
				"d = (Complement[Range[Max[#1]], #1] \u0026)[c]; (* A001950 *)",
				"Table[a[d[[n]]] = 1 - a[n], {n, 1, h - 1}];",
				"Table[a[c[[n]]] = a[n], {n, 1, h}] (* A095076 conjectured *)",
				"Flatten[Position[%, 0]]  (* A189034 *)",
				"Flatten[Position[%%, 1]] (* A189035 *)"
			],
			"program": [
				"(Python)",
				"def ok(n): return True if n==0 else n*(2*n \u0026 n == 0)",
				"print([bin(n)[2:].count(\"1\")%2 for n in range(1001) if ok(n)]) # _Indranil Ghosh_, Jun 08 2017"
			],
			"xref": [
				"a(n) = A010060(A003714(n)). a(n) = 1 - A095111(n). Characteristic function of A020899. Run counts are given by A095276.",
				"Cf. A007895, A189034, A189035 (positions of 0 and 1 if the conjecture is valid)."
			],
			"keyword": "nonn",
			"offset": "0,1",
			"author": "_Antti Karttunen_, Jun 01 2004",
			"references": 10,
			"revision": 71,
			"time": "2021-03-23T07:19:25-04:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}