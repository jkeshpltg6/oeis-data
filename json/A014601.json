{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A014601",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 14601,
			"data": "0,3,4,7,8,11,12,15,16,19,20,23,24,27,28,31,32,35,36,39,40,43,44,47,48,51,52,55,56,59,60,63,64,67,68,71,72,75,76,79,80,83,84,87,88,91,92,95,96,99,100,103,104,107,108,111,112,115,116,119,120,123,124",
			"name": "Numbers congruent to 0 or 3 mod 4.",
			"comment": [
				"Discriminants of orders in imaginary quadratic fields (negated). [Comment corrected by _Christopher E. Thompson_, Dec 11 2016]",
				"n such that Langford-Skolem problem has a solution - see A014552.",
				"Complement of A042963. - _Reinhard Zumkeller_, Oct 04 2004",
				"Also called skew amenable numbers; a number n is skew amenable if there exist a set {a(i)} of integers satisfying the relations n = Sum_{i=1..n} a(i) = -Product_{i=1..n} a(i). Thus we have 8 = 1 + 1 + 1 + 1 + 1 + 1 - 2 + 4 = -(1*1*1*1*1*1*(-2)*4). - _Lekraj Beedassy_, Jan 07 2005",
				"Possible nonpositive discriminants of quadratic equation a*x^2 + b*x + c or discriminants of binary quadratic forms a*x^2 + b*x*y + c*y^2. - _Artur Jasinski_, Apr 28 2008",
				"Also, disregarding the 0 term, positive integers m such that, equivalently,",
				"  (i) +-1 +-2 +-... +-m is even for all choices of signs,",
				"  (ii) +-1 +-2 +-... +-m = 0 for some choices of signs,",
				"  (iii) for all -m \u003c= k \u003c= m, k = +-1 +-2 +-... +-(k-1) +-(k+1) +-(k+2) +-... +-m for at least one choice of signs. - _Rick L. Shepherd_, Oct 29 2008",
				"A145768(a(n)) is even. - _Reinhard Zumkeller_, Jun 05 2012",
				"Multiples of 4 interleaved with 1 less than multiples of 4. - _Wesley Ivan Hurt_, Nov 08 2013",
				"((2*k+0) + (2*k+1) + ... + (2*k+m-1) + (2*k+m)) is even if and only if m = a(n) for some n where k is any nonnegative integer. - _Gionata Neri_, Jul 24 2015",
				"Numbers whose binary reflected Gray code (A014550) ends with 0. - _Amiram Eldar_, May 17 2021"
			],
			"reference": [
				"H. Cohen, Course in Computational Alg. No. Theory, Springer, 1993, pp. 514-5.",
				"A. Scholz and B. Schoeneberg, Einführung in die Zahlentheorie, 5. Aufl., de Gruyter, Berlin, New York, 1973, p. 108."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A014601/b014601.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"S. F. Barger, \u003ca href=\"http://www.jstor.org/stable/2589724\"\u003eSolution to problem 10454: Amenable Numbers\u003c/a\u003e, Amer. Math. Monthly, Vol. 105, No. 4 (April 1998), p. 368.",
				"Steven R. Finch, \u003ca href=\"/A000924/a000924.pdf\"\u003eClass number theory\u003c/a\u003e [Cached copy, with permission of the author]",
				"Heiko Harborth, \u003ca href=\"http://dx.doi.org/10.1016/0097-3165(72)90039-8\"\u003eSolution of Steinhaus's problem with plus and minus signs\u003c/a\u003e, Journal of Combinatorial Theory, Series A, Volume 12, Issue 2 (March 1972), Pages 253-259.",
				"Rick L. Shepherd, \u003ca href=\"http://libres.uncg.edu/ir/uncg/f/Shepherd_uncg_0154M_11099.pdf\"\u003eBinary quadratic forms and genus theory\u003c/a\u003e, Master of Arts Thesis, University of North Carolina at Greensboro, 2013.",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,1,-1)."
			],
			"formula": [
				"a(n) = (n + 1)*2 + 1 - n mod 2. - _Reinhard Zumkeller_, Apr 21 2003",
				"A014494(n) = A000217(a(n)). - _Reinhard Zumkeller_, Oct 04 2004",
				"a(n) = Sum_{k=1..n} (2 - (-1)^k). - _William A. Tedeschi_, Mar 20 2008",
				"A139131(a(n)) = A078636(a(n)). - _Reinhard Zumkeller_, Apr 10 2008",
				"a(n) = a(n-1) + a(n-2) - a(n-3) for n \u003e 2. G.f.: x*(3+x)/((1+x)*(x-1)^2). - _R. J. Mathar_, Sep 25 2009",
				"a(n) = 2*n + (n mod 2). - Paolo Valzasina (p.valzasina(AT)gmail.com), Nov 24 2009",
				"a(n) = (4*n - (-1)^n + 1)/2. - _Bruno Berselli_, Oct 06 2010",
				"a(n) = 4*n - a(n-1) - 1 (with a(0) = 0). - _Vincenzo Librandi_, Dec 24 2010",
				"a(n) = -A042948(-n) for all n in Z. - _Michael Somos_, Dec 27 2010",
				"G.f.: 2*x / (1 - x)^2 + (1 / (1 - x) + 1 / (1 + x)) * x/2. - _Michael Somos_, Dec 27 2010",
				"a(n) = Sum_{k\u003e=0} A030308(n,k)*b(k) with b(0) = 3 and b(k) = 2^(k+1) for k \u003e 0. - _Philippe Deléham_, Oct 17 2011",
				"a(n) = ceiling((4/3)*ceiling(3*n/2)). - _Clark Kimberling_, Jul 04 2012",
				"a(n) = 3n - 2*floor(n/2). - _Wesley Ivan Hurt_, Nov 08 2013",
				"a(n) = A042948(n+1) - 1 for all n in Z. - _Michael Somos_, Jul 24 2015",
				"a(n) + a(n+1) = A004767(n) for all n in Z. - _Michael Somos_, Jul 24 2015",
				"Sum_{n\u003e=1} (-1)^(n+1)/a(n) = 3*log(2)/4 - Pi/8. - _Amiram Eldar_, Dec 05 2021"
			],
			"example": [
				"G.f. = 3*x + 4*x^2 + 7*x^3 + 8*x^4 + 11*x^5 + 12*x^6 + 15*x^7 + 16*x^8 + ..."
			],
			"maple": [
				"A014601:=n-\u003e3*n-2*floor(n/2); seq(A014601(k), k=0..100); # _Wesley Ivan Hurt_, Nov 08 2013"
			],
			"mathematica": [
				"aa = {}; Do[Do[Do[d = b^2 - 4 a c; If[d \u003c= 0, AppendTo[aa, -d]], {a, 0, 50}], {b, 0, 50}], {c, 0, 50}]; Union[aa] (* _Artur Jasinski_, Apr 28 2008 *)",
				"Select[Range[0, 124], Or[Mod[#, 4] == 0, Mod[#, 4] == 3] \u0026] (* _Ant King_, Nov 18 2010 *)",
				"CoefficientList[Series[2 x/(1 - x)^2 + (1/(1 - x) + 1/(1 + x)) x/2, {x, 0, 100}], x] (* _Vincenzo Librandi_, May 18 2014 *)",
				"a[ n_] := 2 n + Mod[n, 2]; (* _Michael Somos_, Jul 24 2015 *)"
			],
			"program": [
				"(MAGMA)[n: n in [0..200]|n mod 4 in {0,3}]; // _Vincenzo Librandi_, Dec 24 2010",
				"(PARI) {a(n) = 2*n + n%2}; /* _Michael Somos_, Dec 27 2010 */",
				"(Haskell)",
				"a014601 n = a014601_list !! n",
				"a014601_list = [x | x \u003c- [0..], mod x 4 `elem` [0, 3]]",
				"-- _Reinhard Zumkeller_, Jun 05 2012"
			],
			"xref": [
				"Cf. A004676, A014550, A042948, A079896.",
				"Cf. A274406. [_Bruno Berselli_, Jun 26 2016]"
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "Eric Rains (rains(AT)caltech.edu)",
			"references": 57,
			"revision": 124,
			"time": "2021-12-05T03:27:48-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}