{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A288161",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 288161,
			"data": "2,18,6,1350,270,23814,17010,65063250,7229250,9762090030,4437313650,8267713725521250,635977978886250,81188783595533250,297692206516955250,22510683177794610356250,1564913803803903393750,40011216302189267004656036250,10529267447944543948593693750",
			"name": "Denominator of half moments of Rvachëv function.",
			"comment": [
				"a(n) is equal to the denominator of the integral over (0,1) of n*t^(n-1)*up(t).",
				"These numbers are the half moments of the Rvachëv function. The Rvachëv function is related to the Fabius function, up(x)=F(x+1) for |x|\u003c1 and  up(x)=0 for |x|\u003e=1.",
				"The sequence of numerators is not in the OEIS because it appears t coincide with A272755: Numerators of Fabius function F(1/2^n). In fact d(n) = n! 2^binomial(n,2)F(1/2^n). The coincidence depends on the fact that n! 2^binomial(n,2) divides the denominator of F(1/2^n). It is true that 2^binomial(n,2) divides this denominator, but I do not see any reason for n! to divide this denominator."
			],
			"link": [
				"J. Arias de Reyna, \u003ca href=\"https://arxiv.org/abs/1702.05442\"\u003eAn infinitely differentiable function with compact support: Definition and properties\u003c/a\u003e, arXiv:1702.05442 [math.CA], 2017.",
				"J. Arias de Reyna, \u003ca href=\"https://arxiv.org/abs/1702.06487\"\u003eArithmetic of the Fabius function\u003c/a\u003e, arXiv:1702.06487 [math.NT], 2017."
			],
			"formula": [
				"Recurrence d(0)=1; d(n)=Sum_{k=0..n-1}(binomial(n+1,k)d(k))/((n+1)*(2^n-1)) with a(n) are the denominators of d(n).",
				"It may also be defined to be the only sequence d(n) with d(0)=1 and such that the function f(x)=Sum_{n\u003e=0} d(n) x^n/n! satisfies x*f(2x)=(e^x-1)*f(x)."
			],
			"example": [
				"The rationals d(n) are  1/2, 5/18, 1/6, 143/1350, 19/270,  ..."
			],
			"mathematica": [
				"d[0] = 1;",
				"d[n_] := d[n] =",
				"  Sum[Binomial[n + 1, k] d[k], {k, 0, n - 1}]/((n + 1)*(2^n - 1));",
				"Table[Denominator[d[n]], {n, 1, 20}]"
			],
			"xref": [
				"Cf. A287936, A287937, A287938."
			],
			"keyword": "nonn,frac",
			"offset": "1,1",
			"author": "_Juan Arias-de-Reyna_, Jun 06 2017",
			"references": 2,
			"revision": 17,
			"time": "2017-06-26T01:15:02-04:00",
			"created": "2017-06-07T19:17:48-04:00"
		}
	]
}