{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A317131",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 317131,
			"data": "1,0,1,1,5,19,80,520,2898,22486,171460,1509534,14446457,147241144,1650934446,19494460567,248182635904,3340565727176,47659710452780,718389090777485,11381176852445592,189580213656445309,3305258537062221020,60273557241570401742",
			"name": "Number of permutations of [n] whose lengths of increasing runs are prime numbers.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A317131/b317131.txt\"\u003eTable of n, a(n) for n = 0..400\u003c/a\u003e"
			],
			"example": [
				"a(2) = 1: 12.",
				"a(3) = 1: 123.",
				"a(4) = 5: 1324, 1423, 2314, 2413, 3412.",
				"a(5) = 19: 12345, 12435, 12534, 13245, 13425, 13524, 14235, 14523, 15234, 23145, 23415, 23514, 24135, 24513, 25134, 34125, 34512, 35124, 45123."
			],
			"maple": [
				"g:= n-\u003e `if`(n=0 or isprime(n), 1, 0):",
				"b:= proc(u, o, t) option remember; `if`(u+o=0, g(t),",
				"      `if`(g(t)=1, add(b(u-j, o+j-1, 1), j=1..u), 0)+",
				"       add(b(u+j-1, o-j, t+1), j=1..o))",
				"    end:",
				"a:= n-\u003e b(n, 0$2):",
				"seq(a(n), n=0..27);"
			],
			"mathematica": [
				"g[n_] := If[n == 0 || PrimeQ[n], 1, 0];",
				"b[u_, o_, t_] := b[u, o, t] = If[u + o == 0, g[t],",
				"     If[g[t] == 1, Sum[b[u - j, o + j - 1, 1], {j, 1, u}], 0] +",
				"     Sum[b[u + j - 1, o - j, t + 1], {j, 1, o}]];",
				"a[n_] := b[n, 0, 0];",
				"a /@ Range[0, 27] (* _Jean-François Alcover_, Mar 29 2021, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(Python)",
				"from functools import lru_cache",
				"from sympy import isprime",
				"def g(n): return int(n == 0 or isprime(n))",
				"@lru_cache(maxsize=None)",
				"def b(u, o, t):",
				"  if u + o == 0: return g(t)",
				"  return (sum(b(u-j,  o+j-1,  1) for j in range(1, u+1)) if g(t) else 0) +\\",
				"          sum(b(u+j-1, o-j, t+1) for j in range(1, o+1))",
				"def a(n): return b(n, 0, 0)",
				"print([a(n) for n in range(28)]) # _Michael S. Branicky_, Mar 29 2021 after _Alois P. Heinz_"
			],
			"xref": [
				"Cf. A000040, A097597, A218002, A317111, A317128, A317129, A317130, A317132, A317447."
			],
			"keyword": "nonn",
			"offset": "0,5",
			"author": "_Alois P. Heinz_, Jul 21 2018",
			"references": 7,
			"revision": 16,
			"time": "2021-03-29T08:00:41-04:00",
			"created": "2018-07-22T11:25:15-04:00"
		}
	]
}