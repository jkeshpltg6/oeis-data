{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A240137",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 240137,
			"data": "0,1,35,216,748,1925,4131,7840,13616,22113,34075,50336,71820,99541,134603,178200,231616,296225,373491,464968,572300,697221,841555,1007216,1196208,1410625,1652651,1924560,2228716,2567573,2943675,3359656,3818240,4322241,4874563",
			"name": "Sum of n consecutive cubes starting from n^3.",
			"comment": [
				"Sum_{i\u003e=1} 1/a(i) = 1.0356568858420883122567711052556541...",
				"Consider the partitions of 2n into two parts (p,q) where p \u003c= q. Then a(n) is the total volume of the family of cubes with side length q. - _Wesley Ivan Hurt_, Apr 15 2018"
			],
			"link": [
				"Bruno Berselli, \u003ca href=\"/A240137/b240137.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Bruno Berselli, \u003ca href=\"/A240137/a240137_1.jpg\"\u003eFormula for the constant 1.035656885842088312...\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (5,-10,10,-5,1)."
			],
			"formula": [
				"G.f.: x*(1 + 30*x + 51*x^2 + 8*x^3)/(1 - x)^5.",
				"a(n) = n^2*(3*n - 1)*(5*n - 3)/4 = A000326(n)*A000566(n).",
				"a(n) = A116149(-n), with A116149(0)=0."
			],
			"example": [
				"a(3) = 216 because 216 = 3^3 + 4^3 + 5^3."
			],
			"maple": [
				"A240137:=n-\u003en^2*(3*n-1)*(5*n-3)/4; seq(A240137(n), n=0..40); # _Wesley Ivan Hurt_, May 09 2014"
			],
			"mathematica": [
				"Table[n^2 (3 n - 1) (5 n - 3)/4, {n, 0, 40}]",
				"CoefficientList[Series[x (1 + 30 x + 51 x^2 + 8 x^3)/(1 - x)^5, {x, 0, 40}], x] (* _Vincenzo Librandi_, May 09 2014 *)"
			],
			"program": [
				"(Sage) [n^2*(3*n-1)*(5*n-3)/4 for n in [0..40]]",
				"(MAGMA) [n^2*(3*n-1)*(5*n-3)/4: n in [0..40]];",
				"(PARI) a(n)=n^2*(3*n-1)*(5*n-3)/4 \\\\ _Charles R Greathouse IV_, Oct 07 2015"
			],
			"xref": [
				"Subsequence of A217843.",
				"Cf. A116149: sum of n consecutive cubes after n^3.",
				"Cf. A050410: sum of n consecutive squares starting from n^2.",
				"Cf. A000326 (pentagonal numbers): sum of n consecutive integers starting from n.",
				"Cf. A126274: n-th triangular number (A000217) * n-th pentagonal number (A000326)."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Bruno Berselli_, Apr 02 2014",
			"references": 7,
			"revision": 44,
			"time": "2018-04-16T18:58:52-04:00",
			"created": "2014-04-04T13:38:28-04:00"
		}
	]
}