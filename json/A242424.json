{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A242424",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 242424,
			"data": "1,2,4,3,6,6,10,5,12,9,14,10,22,15,18,7,26,20,34,15,30,21,38,14,27,33,40,25,46,30,58,11,42,39,45,28,62,51,66,21,74,50,82,35,60,57,86,22,75,45,78,55,94,56,63,35,102,69,106,42,118,87,100,13,99,70,122,65",
			"name": "Bulgarian solitaire operation on partition list A112798: a(1) = 1, a(n) = A000040(A001222(n)) * A064989(n).",
			"comment": [
				"In \"Bulgarian solitaire\" a deck of cards or another finite set of objects is divided into one or more piles, and the \"Bulgarian operation\" is performed by taking one card from each pile, and making a new pile of them, which is added to the remaining set of piles. Essentially, this operation is a function whose domain and range are unordered integer partitions (cf. A000041) and which preserves the total size of a partition (the sum of its parts). This sequence is induced when the operation is implemented on the partitions as ordered by the list A112798.",
				"Please compare to the definition of A122111, which conjugates the partitions encoded with the same system.",
				"a(n) is even if and only if n is either a prime or a multiple of three.",
				"Conversely, a(n) is odd if and only if n is a nonprime not divisible by three."
			],
			"reference": [
				"Martin Gardner, Colossal Book of Mathematics, Chapter 34, Bulgarian Solitaire and Other Seemingly Endless Tasks, pp. 455-467, W. W. Norton \u0026 Company, 2001."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A242424/b242424.txt\"\u003eTable of n, a(n) for n = 1..8192\u003c/a\u003e",
				"Ethan Akin and Morton Davis, \u003ca href=\"http://www.jstor.org/stable/2323643\"\u003e\"Bulgarian solitaire\"\u003c/a\u003e, American Mathematical Monthly 92 (4): 237-250. (1985).",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Bulgarian_solitaire\"\u003eBulgarian solitaire\u003c/a\u003e",
				"\u003ca href=\"/index/Pri#prime_indices\"\u003eIndex entries for sequences computed from indices in prime factorization\u003c/a\u003e"
			],
			"formula": [
				"a(1) = 1, a(n) = A000040(A001222(n)) * A064989(n) = A105560(n) * A064989(n).",
				"a(n) = A241909(A243051(A241909(n))).",
				"a(n) = A243353(A226062(A243354(n))).",
				"a(A000079(n)) = A000040(n) for all n.",
				"A056239(a(n)) = A056239(n) for all n."
			],
			"program": [
				"(Scheme) (define (A242424 n) (if (\u003c= n 1) n (* (A000040 (A001222 n)) (A064989 n))))"
			],
			"xref": [
				"Row 1 of A243070 (table which gives successive \"recursive iterates\" of this sequence and converges towards A122111).",
				"Fixed points: A002110 (primorial numbers).",
				"Cf. A000041, A243051, A243072, A243073, A226062, A242422, A242423, A122111, A105560, A000040, A001222, A064989, A056239."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Antti Karttunen_, May 13 2014",
			"references": 17,
			"revision": 22,
			"time": "2017-03-07T13:16:25-05:00",
			"created": "2014-06-18T22:29:42-04:00"
		}
	]
}