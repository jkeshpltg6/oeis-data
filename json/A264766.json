{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A264766",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 264766,
			"data": "1,2,3,2,3,9,13,9,3,4,18,40,51,40,18,4,5,30,90,165,201,165,90,30,5,6,45,170,405,666,783,666,405,170,45,6,7,63,287,840,1736,2646,3039,2646,1736,840,287,63,7,8,84,448,1554,3864,7224,10424,11763,10424,7224,3864,1554,448,84,8,9,108,660,2646,7686,17010,29520,40851,45481,40851,29520,17010,7686,2646,660,108,9,10",
			"name": "Irregular symmetric triangle of coefficients T(n,k) of the polynomials p(n,x) = Sum_{k=0..n} binomial(n+1,k)*(1+x)^(2*k)*(-x)^(n-k) for 0 \u003c= k \u003c= 2*n.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A264766/b264766.txt\"\u003eTable of n, a(n) for the first 50 rows, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = Sum_{j=0..n-d} (-1)^j*binomial(n+1,j+1)*binomial(2*n-2*j,k-j) if d = 0 or better d = abs(k-n), and 0 \u003c= k \u003c= 2*n.",
				"Recurrence: T(n,0) = n+1, and T(n,k) = 0 for k \u003c 0 or k \u003e 2*n, and T(n+1,k) = T(n,k-2) + T(n,k-1) + T(n,k) + binomial(2*n+2,k) for k \u003e 0 and n \u003e= 0.",
				"T(n,k) = T(n,2*n-k) for 0 \u003c= k \u003c= 2*n.",
				"p(n,x) = Sum_{k=0..2*n} T(n,k)*x^k = Sum_{k=0..n} (1+x)^(2*k)*(1+x+x^2)^(n-k) = Sum_{k=0..n} binomial(n+1,k)*(1+x+x^2)^k*x^(n-k) for n \u003e= 0.",
				"Recurrence: p(0,x) = 1, and p(n+1,x) = (1+x+x^2)*p(n,x)+(1+x)^(2*n+2), n \u003e= 0.",
				"T(n,n) = Sum_{j=0..n} (-1)^(n-j)*binomial(n+1,j)*binomial(2*j,j) = A000984(n+1)-A002426(n+1) for n \u003e= 0 (see also A163774).",
				"Sum_{n\u003e=0} T(n,n)*x^(n+1) = 1/sqrt(1-4*x) - 1/sqrt(1-2*x-3*x^2) for abs(x) \u003c 1/4.",
				"T(n,n-1) = binomial(2*n+2,n) - A027907(n+1,n) for n \u003e 0.",
				"T(n+1,n)/(n+2) = A000108(n+2) - A001006(n+1) for n \u003e= 0 (see also A058987).",
				"Row sums: p(n,1) = A005061(n+1) for n \u003e= 0.",
				"Alternating row sums: p(n,-1) = 1 for n \u003e= 0.",
				"p(n,-2) = Sum_{k=0..2*n} T(n,k)*(-2)^k = A003462(n+1) for n \u003e= 0.",
				"T(n,k) = Sum_{j=0..k} (-1)^j*A260056(n,j)*binomial(2*n-j,k-j) for 0 \u003c= k \u003c= 2*n.",
				"A260056(n,k) = Sum_{j=0..k} (-1)^j*T(n,j)*binomial(2*n-j,k-j) for 0 \u003c= k \u003c= 2*n.",
				"p(n,-1-x) = Sum{k=0..2*n} A260056(n,k)*x^(2*n-k) for n \u003e= 0.",
				"p(n,-x/(1+x))*(1+x)^(2*n) = Sum_{k=0..2*n} A260056(n,k)*x^k for n \u003e= 0.",
				"Sum_{n\u003e=0) p(n,x)*t^n = 1/((1-t*(1+x)^2)*(1-t*(1+x+x^2))).",
				"p(n,x)*x = (1+x)^(2*n+2) - (1+x+x^2)^(n+1), n \u003e= 0.",
				"T(n,k) = binomial(2*n+2,k+1) - A027907(n+1,k+1) for 0 \u003c= k \u003c= 2*n."
			],
			"example": [
				"The irregular triangle T(n,k) begins:",
				"n\\k:  0   1    2     3     4     5      6      7      8     9    10  11  12",
				"  0:  1",
				"  1:  2   3    2",
				"  2:  3   9   13     9     3",
				"  3:  4  18   40    51    40    18      4",
				"  4:  5  30   90   165   201   165     90     30      5",
				"  5:  6  45  170   405   666   783    666    405    170    45     6",
				"  6:  7  63  287   840  1736  2646   3039   2646   1736   840   287  63   7",
				"  etc.",
				"The polynomial corresponding to row 2 is p(2,x) = 3 + 9*x + 13*x^2 + 9*x^3 + 3*x^4."
			],
			"mathematica": [
				"T[n_, k_] := Sum[(-1)^j*Binomial[n + 1, j + 1]*Binomial[2*n - 2*j, k - j], {j, 0, n - Abs[k - n]}]; Table[T[n, k], {n,0,10}, {k,0,2*n}] // Flatten (* _G. C. Greubel_, Aug 12 2017 *)"
			],
			"program": [
				"(PARI) T(n,k) = sum(j=0, n-abs(k-n), (-1)^j*binomial(n+1,j+1)*binomial(2*n-2*j,k-j));",
				"tabf(nn) = for (n=0, nn, for (k=0, 2*n, print1(T(n, k), \", \");); print();); \\\\ _Michel Marcus_, Nov 24 2015"
			],
			"xref": [
				"Cf. A000108, A000984, A001006, A002426, A003462, A005061, A027907, A058987, A163774, A260056."
			],
			"keyword": "nonn,easy,tabf",
			"offset": "0,2",
			"author": "_Werner Schulte_, Nov 23 2015",
			"references": 1,
			"revision": 16,
			"time": "2017-08-12T12:58:12-04:00",
			"created": "2015-11-25T02:52:25-05:00"
		}
	]
}