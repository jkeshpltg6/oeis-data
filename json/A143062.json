{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A143062",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 143062,
			"data": "1,-1,1,0,0,-1,0,1,0,0,0,0,-1,0,0,1,0,0,0,0,0,0,-1,0,0,0,1,0,0,0,0,0,0,0,0,-1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,-1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,-1,0,0,0,0,0,0,1,0,0,0,0,0,0,0",
			"name": "Expansion of false theta series variation of Euler's pentagonal number series in powers of x.",
			"comment": [
				"a(n) = sum over all partitions of n into distinct parts of number of partitions with even largest part minus number with odd largest part.",
				"In the Berndt reference replace {a -\u003e 1, q -\u003e x} in equation (3.1) to get g.f. Replace {a -\u003e x, q -\u003e x} to get f(x). G.f. is 1 - f(x) * x / (1 + x)."
			],
			"reference": [
				"G. E. Andrews and B. C. Berndt, Ramanujan's lost notebook, Part I, Springer, New York, 2005, MR2135178 (2005m:11001) See Section 9.4, pp. 232-236.",
				"Srinivasa Ramanujan, The Lost Notebook and Other Unpublished Papers, Narosa Publishing House, New Delhi, 1988, see p. 41, 10th equation numerator."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A143062/b143062.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"B. C. Berndt, B. Kim, and A. J. Yee, \u003ca href=\"http://dx.doi.org/10.1016/j.jcta.2009.07.005\"\u003eRamanujan's lost notebook: Combinatorial proofs of identities associated with Heine's transformation or partial theta functions\u003c/a\u003e, J. Comb. Thy. Ser. A, 117 (2010), 957-973.",
				"I. Pak, \u003ca href=\"http://www.math.ucla.edu/~pak/papers/finefull.pdf\"\u003eOn Fine's partition theorems, Dyson, Andrews and missed opportunities\u003c/a\u003e, Math. Intelligencer, 25 (No. 1, 2003), 10-16."
			],
			"formula": [
				"a(n) = b(24*n + 1) where b() is multiplicative with b(p^(2*e)) = (-1)^e if p = 5 (mod 6), b(p^(2*e)) = +1 if p = 1 (mod 6) and b(p^(2*e-1)) = b(2^e) = b(3^e) = 0 if e\u003e0.",
				"G.f.: Sum_{k\u003e=0} x^((3*k^2 + k) / 2) * (1 - x^(2*k + 1)) = 1 - Sum_{k\u003e0} x^((3*k^2 - k) / 2) * (1 - x^k).",
				"G.f.: 1 - x / (1 + x) + x^3 / ((1 + x) * (1 + x^2)) - x^6 / ((1 + x) * (1 + x^2) * (1 + x^3)) + ...",
				"G.f.: 1 - x / (1 + x^2) + x^2 / ((1 + x^2) * (1 + x^4)) - x^3 / ((1 + x^2 ) * (1 + x^4) * (1 + x^6)) + ...",
				"|a(n)| = |A010815(n)| = |A080995(n)| = |A199918(n)| = |A121373(n)|.",
				"a(n) = A026838(n) - A026837(n) (Fine's theorem), see the Pak reference. [_Joerg Arndt_, Jun 24 2013]",
				"a(n)=1 if n = k(3k+1)/2, a(n)=-1 if n = k(3k-1)/2, a(n)=0 otherwise. [_Joerg Arndt_, Jun 24 2013]",
				"G.f.: sum(n\u003e=0, (-q)^n * prod(k=1..n-1, 1+q^k) ). [_Joerg Arndt_, Jun 24 2013]",
				"a(n) = - A203568(n) unless n=0. a(0) = 1. - _Michael Somos_, Jul 12 2015",
				"From _Peter Bala_, Feb 04 202: (Start)",
				"A pair of conjectural g.f.s: 1 + Sum_{n \u003e= 0} (-1)^n*x^(2*n-1)/Product_{k = 1..n} 1 + x^(2*k-1);",
				"1 - Sum_{n \u003e= 1} x^(n*(2*n-1))/Product_{k = 1..2*n} 1 + x^k. (End)"
			],
			"example": [
				"a(5) = -1 +1 -1 = -1 since 5 = 4 + 1 = 3 + 2. a(7) = -1 +1 -1 +1 +1 = 1 since 7 = 6 + 1 = 5 + 2 = 4 + 3 = 4 + 2 + 1.",
				"G.f. = 1 - x + x^2 - x^5 + x^7 - x^12 + x^15 - x^22 + x^26 - x^35 + x^40 + ...",
				"G.f. = q - q^25 + q^49 - q^121 + q^169 - q^289 + q^361 - q^529 + q^625 - q^841 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ SquaresR[ 1, 24 n + 1] == 2, (-1)^Quotient[ Sqrt[24 n + 1], 3], 0];",
				"a[ n_] := With[ {m = Sqrt[24 n + 1]}, If[ IntegerQ@m, (-1)^Quotient[ m, 3], 0]]; (* _Michael Somos_, Nov 18 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = if( issquare( 24*n + 1, \u0026n), (-1)^(n \\ 3) )};"
			],
			"xref": [
				"Cf. A010815, A026837, A026838, A080995, A121373, A199918, A203568."
			],
			"keyword": "sign",
			"offset": "0,1",
			"author": "_Michael Somos_, Jul 21 2008",
			"references": 8,
			"revision": 32,
			"time": "2021-02-09T11:04:55-05:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}