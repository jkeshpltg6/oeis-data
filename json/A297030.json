{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A297030",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 297030,
			"data": "0,1,1,2,2,2,1,2,3,3,3,3,3,2,1,2,3,4,4,4,4,4,3,3,4,4,4,3,3,2,1,2,3,4,4,5,5,5,4,4,5,5,5,5,5,4,3,3,4,5,5,5,5,5,4,3,4,4,4,3,3,2,1,2,3,4,4,5,5,5,4,5,6,6,6,6,6,5,4,4,5,6,6,6,6,6",
			"name": "Number of pieces in the list d(m),d(m-1),...,d(0) of base-2 digits of n; see Comments",
			"comment": [
				"The definition of \"piece\" starts with the base-b digits d(m), d(m-1), ..., d(0) of n.  First, an *ascent* is a list (d(i),d(i-1),...,d(i-h)) such that d(i)\u003cd(i-1)\u003c...\u003cd(i-h), where d(i+1)\u003e=d(i) if i\u003cm, and d(i-h-1)\u003e=d(i-h) if i\u003eh.  A *descent* is a list (d(i),d(i-1),...,d(i-h)) such that d(i)\u003ed(i-1)\u003e...\u003ed(i-h), where d(i+1)\u003c=d(i) if i\u003cm, and d(i-h-1)\u003c=d(i-h) if i\u003eh.  A *flat* is a list (d(i),d(i-1),...,d(i-h)), where h\u003e0, such that d(i)=d(i-1)=...=d(i-h), where d(i+1)!=d(i) if i\u003cm, and d(i-h-1)!=d(i-h) if i\u003eh. A *piece* is an ascent, a descent, or a flat.  Example:  235621103 has five pieces:  (2,3,5,6), (6,2,1), (1,1), (1,0), and (0,3); that's 2 ascents, 2 descents, and 1 flat. For every b, the \"piece sequence\" includes every positive integer infinitely many times.",
				"Guide to related sequences:",
				"***",
				"Base   # pieces for n\u003e=1",
				"2         A297030",
				"3         A297031",
				"4         A297032",
				"5         A297033",
				"6         A297034",
				"7         A297035",
				"8         A297036",
				"9         A297037",
				"10        A297038",
				"11        A297039",
				"12        A297040",
				"13        A297041",
				"14        A297042",
				"15        A297043",
				"16        A297044",
				"20        A297045",
				"60        A297046"
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A297030/b297030.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"Base-2 digits for 100:  1, 1, 0, 0, 1, 0, 0, so that a(100) = 6."
			],
			"mathematica": [
				"a[n_, b_] := Length[Map[Length, Split[Sign[Differences[IntegerDigits[n, b]]]]]];",
				"b = 2; Table[a[n, b], {n, 1, 120}]"
			],
			"xref": [
				"Cf. A297038, A296712 (rises and falls), A296882 (pits and peaks)."
			],
			"keyword": "nonn,easy,base",
			"offset": "1,4",
			"author": "_Clark Kimberling_, Jan 13 2018",
			"references": 17,
			"revision": 8,
			"time": "2018-01-14T03:20:46-05:00",
			"created": "2018-01-13T09:29:25-05:00"
		}
	]
}