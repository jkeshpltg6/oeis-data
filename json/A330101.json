{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A330101",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 330101,
			"data": "0,1,1,3,4,5,5,7,1,3,3,11,33,19,19,15,4,5,33,19,20,21,37,23,5,7,19,15,37,23,51,31,4,33,5,19,20,37,21,23,5,19,7,15,37,51,23,31,20,37,37,51,52,53,53,55,21,23,23,31,53,55,55,63,64,65,65,67,68,69,69",
			"name": "BII-number of the brute-force normalization of the set-system with BII-number n.",
			"comment": [
				"First differs from A330102 at a(148) = 545, A330102(148) = 274, with corresponding set-systems 545: {{1},{2,3},{2,4}} and 274: {{2},{1,3},{1,4}}.",
				"A set-system is a finite set of finite nonempty sets of positive integers.",
				"We define the brute-force normalization of a set-system to be obtained by first normalizing so that the vertices cover an initial interval of positive integers, then applying all permutations to the vertex set, and finally taking the least representative, where the ordering of sets is first by length and then lexicographically.",
				"For example, 156 is the BII-number of {{3},{4},{1,2},{1,3}}, which has the following normalizations, together with their BII-numbers:",
				"  Brute-force:    2067: {{1},{2},{1,3},{3,4}}",
				"  Lexicographic:   165: {{1},{4},{1,2},{2,3}}",
				"  VDD:             525: {{1},{3},{1,2},{2,4}}",
				"  MM:              270: {{2},{3},{1,2},{1,4}}",
				"  BII:             150: {{2},{4},{1,2},{1,3}}",
				"A binary index of n is any position of a 1 in its reversed binary expansion. The binary indices of n are row n of A048793. We define the set-system with BII-number n to be obtained by taking the binary indices of each binary index of n. Every set-system (finite set of finite nonempty sets of positive integers) has a different BII-number. For example, 18 has reversed binary expansion (0,1,0,0,1), and since the binary indices of 2 and 5 are {2} and {1,3} respectively, the BII-number of {{2},{1,3}} is 18. Elements of a set-system are sometimes called edges."
			],
			"link": [
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Idempotent\"\u003eIdempotence\u003c/a\u003e"
			],
			"mathematica": [
				"bpe[n_]:=Join@@Position[Reverse[IntegerDigits[n,2]],1];",
				"fbi[q_]:=If[q=={},0,Total[2^q]/2];",
				"brute[m_]:=If[Union@@m!={}\u0026\u0026Union@@m!=Range[Max@@Flatten[m]],brute[m/.Rule@@@Table[{(Union@@m)[[i]],i},{i,Length[Union@@m]}]],First[Sort[brute[m,1]]]];",
				"brute[m_,1]:=Table[Sort[Sort/@(m/.Rule@@@Table[{i,p[[i]]},{i,Length[p]}])],{p,Permutations[Union@@m]}];",
				"Table[fbi[fbi/@brute[bpe/@bpe[n]]],{n,0,100}]"
			],
			"xref": [
				"This sequence is idempotent and its image and fixed points are A330099.",
				"Non-isomorphic multiset partitions are A007716.",
				"Unlabeled spanning set-systems by vertices are A055621.",
				"Unlabeled set-systems by weight are A283877.",
				"Cf. A000612, A300913, A321405, A330061, A330102, A330105.",
				"Other fixed points:",
				"- Brute-force: A330104 (multisets of multisets), A330107 (multiset partitions), A330099 (set-systems).",
				"- Lexicographic: A330120 (multisets of multisets), A330121 (multiset partitions), A330110 (set-systems).",
				"- VDD: A330060 (multisets of multisets), A330097 (multiset partitions), A330100 (set-systems).",
				"- MM: A330108 (multisets of multisets), A330122 (multiset partitions), A330123 (set-systems).",
				"- BII: A330109 (set-systems)."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_Gus Wiseman_, Dec 02 2019",
			"references": 12,
			"revision": 11,
			"time": "2019-12-13T10:32:03-05:00",
			"created": "2019-12-03T07:09:07-05:00"
		}
	]
}