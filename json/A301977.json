{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A301977",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 301977,
			"data": "1,2,2,3,4,4,3,4,6,7,6,6,7,6,4,5,8,10,9,10,12,11,8,8,11,12,10,9,10,8,5,6,10,13,12,14,17,16,12,13,18,20,17,16,18,15,10,10,15,18,16,17,20,18,13,12,16,17,14,12,13,10,6,7,12,16,15,18,22,21,16,18",
			"name": "a(n) is the number of distinct positive numbers whose binary digits appear in order but not necessarily as consecutive digits in the binary representation of n.",
			"comment": [
				"This sequence has similarities with A078822; there we consider consecutive digits, here not."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A301977/b301977.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e"
			],
			"formula": [
				"a(2^n) = n + 1 for any n \u003e= 0.",
				"a(2^n - 1) = n for any n \u003e 0.",
				"a(2^n + k) = a(2^(n+1)-1 - k) for any n \u003e= 0 and k=0..2^n-1.",
				"a(n) \u003e= A070939(n) for any n \u003e 0.",
				"a(n) = Sum_{k=1..n} (Stirling2(n+1,k) mod 2) (conjecture). - _Ilya Gutkovskiy_, Jul 04 2019"
			],
			"example": [
				"The first terms, alongside the binary representations of n and of the numbers k whose binary digits appear in order in the binary representation of k, are:",
				"  n  a(n)  bin(n)    bin(k)",
				"  -- ----  ------    ------",
				"   1    1       1    1",
				"   2    2      10    1, 10",
				"   3    2      11    1, 11",
				"   4    3     100    1, 10, 100",
				"   5    4     101    1, 10, 11, 101",
				"   6    4     110    1, 10, 11, 110",
				"   7    3     111    1, 11, 111",
				"   8    4    1000    1, 10, 100, 1000",
				"   9    6    1001    1, 10, 11, 100, 101, 1001",
				"  10    7    1010    1, 10, 11, 100, 101, 110, 1010",
				"  11    6    1011    1, 10, 11, 101, 111, 1011",
				"  12    6    1100    1, 10, 11, 100, 110, 1100",
				"  13    7    1101    1, 10, 11, 101, 110, 111, 1101",
				"  14    6    1110    1, 10, 11, 110, 111, 1110",
				"  15    4    1111    1, 11, 111, 1111",
				"  16    5   10000    1, 10, 100, 1000, 10000",
				"  17    8   10001    1, 10, 11, 100, 101, 1000, 1001, 10001",
				"  18   10   10010    1, 10, 11, 100, 101, 110, 1000, 1001, 1010, 10010",
				"  19    9   10011    1, 10, 11, 100, 101, 111, 1001, 1011, 10011",
				"  20   10   10100    1, 10, 11, 100, 101, 110, 1000, 1010, 1100, 10100"
			],
			"program": [
				"(PARI) a(n) = my (b=binary(n), s=Set(1)); for (i=2, #b, s = setunion(s, Set(apply(v -\u003e 2*v+b[i], s)))); return (#s)"
			],
			"xref": [
				"Cf. A070939, A078822."
			],
			"keyword": "nonn,base,look",
			"offset": "1,2",
			"author": "_Rémy Sigrist_, Mar 30 2018",
			"references": 3,
			"revision": 17,
			"time": "2019-07-04T15:22:46-04:00",
			"created": "2018-04-01T10:50:06-04:00"
		}
	]
}