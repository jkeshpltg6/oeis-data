{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A309104",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 309104,
			"data": "0,1,3,9,25,72,199,545,1487,4048,11007,29930,81371,221199,601295,1634499,4443044,12077466,32829974,89241138,242582585,659407853,1792456409,4872401708,13244561050,36002449653,97864804699,266024120286,723128532126,1965667148555",
			"name": "a(n) = Sum_{k \u003e= 0} floor(n^(2*k+1) / (2*k+1)!).",
			"comment": [
				"This sequence is inspired by the Maclaurin series for the hyperbolic sine function."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A309104/b309104.txt\"\u003eTable of n, a(n) for n = 0..2300\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Taylor_series#Hyperbolic_functions\"\u003eTaylor series: Hyperbolic functions\u003c/a\u003e"
			],
			"formula": [
				"a(n) ~ sinh(n) as n tends to infinity.",
				"a(n) \u003c= A000471(n)."
			],
			"example": [
				"For n = 5:",
				"- we have:",
				"  k  5^(2*k+1)/(2*k+1)!",
				"  -  ------------------",
				"  0                   5",
				"  1                  20",
				"  2                  26",
				"  3                  15",
				"  4                   5",
				"  5                   1",
				"  \u003e=6                 0",
				"- hence a(5) = 5 + 20 + 26 + 15 + 5 + 1 = 72."
			],
			"maple": [
				"f:= proc(n) local t,k,v;",
				"  v:= n; t:= n;",
				"  for k from 1 do",
				"    v:= v*n^2/(2*k*(2*k+1));",
				"    if v \u003c 1 then return t fi;",
				"    t:= t + floor(v);",
				"  od",
				"end proc:",
				"map(f, [$0..30]); # _Robert Israel_, Mar 18 2020"
			],
			"program": [
				"(PARI) a(n) = { my (v=0, d=n); forstep (k=2, oo, 2, if (d\u003c1, return (v), v += floor(d); d *= n^2/(k*(k+1)))) }"
			],
			"xref": [
				"See A309087 for similar sequences.",
				"Cf. A000471."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Rémy Sigrist_, Jul 12 2019",
			"ext": [
				"Definition corrected by _Robert Israel_, Mar 18 2020"
			],
			"references": 2,
			"revision": 10,
			"time": "2020-03-18T23:02:04-04:00",
			"created": "2019-07-14T06:26:34-04:00"
		}
	]
}