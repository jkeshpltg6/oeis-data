{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A244554",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 244554,
			"data": "1,1,-2,1,4,-2,0,1,-1,4,-2,-2,4,0,0,1,2,-1,-2,4,0,-2,0,-2,5,4,-4,0,4,0,0,1,-4,2,0,-1,4,-2,0,4,2,0,-2,-2,4,0,0,-2,1,5,-4,4,4,-4,0,0,-4,4,-2,0,4,0,0,1,8,-4,-2,2,0,0,0,-1,2,4,-2,-2,0,0",
			"name": "Expansion of phi(q) * (phi(q) - phi(q^2)) / 2 in powers of q where phi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A244554/b244554.txt\"\u003eTable of n, a(n) for n = 1..2500\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q * f(-q, -q^7)^2 * phi(q) / psi(-q) = q * f(-q, -q^7)^2 * chi(q)^3 in powers of q where phi(), psi(), f() are Ramanujan theta functions.",
				"Euler transform of period 8 sequence [1, -3, 3, 0, 3, -3, 1, -2, ...].",
				"Moebius transform is period 8 sequence [1, 0, -3, 0, 3, 0, -1, 0, ...].",
				"Convolution product of A244560 and A107635. Convolution product of A000122 and A143259.",
				"a(n) = (A004018(n) - A033715(n)) / 2 = A243747(2*n).",
				"a(2*n) = a(n). a(8*n + 3) = -2 * A033761(n). a(8*n + 5) = 4 * A053692(n). a(8*n + 7) = 0."
			],
			"example": [
				"G.f. = q + q^2 - 2*q^3 + q^4 + 4*q^5 - 2*q^6 + q^8 - q^9 + 4*q^10 - 2*q^11 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 1, 0, Sum[ {1, 0, -3, 0, 3, 0, -1, 0}[[ Mod[ d, 8, 1] ]], {d, Divisors @ n}]];",
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 3, 0, q] (EllipticTheta[ 3, 0, q] - EllipticTheta[ 3, 0, q^2]) / 2, {q, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, 0, sumdiv(n, d, [0, 1, 0, -3, 0, 3, 0, -1][d%8 + 1]))};",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = sum(k=1, sqrtint(n), 2 * x^k^2, 1 + x * O(x^n)); polcoeff( A * (A - subst(A, x, x^2)) / 2, n))};",
				"(Sage) A = ModularForms( Gamma1(8), 1, prec=33) . basis(); A[1] + A[2];",
				"(MAGMA) A := Basis( ModularForms( Gamma1(8), 1), 33); A[2] + A[3];"
			],
			"xref": [
				"Cf. A000122, A004018, A033715, A033761, A053692, A107635, A143259, A243747, A244560."
			],
			"keyword": "sign",
			"offset": "1,3",
			"author": "_Michael Somos_, Jun 30 2014",
			"references": 2,
			"revision": 14,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2014-06-30T01:56:53-04:00"
		}
	]
}