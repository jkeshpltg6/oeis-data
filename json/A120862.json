{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A120862",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 120862,
			"data": "1,2,10,3,20,109,4,30,218,1189,5,43,327,2378,12970,6,53,469,3567,25940,141481,7,63,578,5116,38910,282962,1543321,8,76,687,6305,55807,424443,3086642,16835050,9,86,829,7494,68777,608761,4629963,33670100,183642229",
			"name": "Fixed-j dispersion for Q = 13: array D(g,h) (g, h \u003e= 1), read by ascending antidiagonals.",
			"comment": [
				"For each positive integer n, there exists a unique pair (j,k) of positive integers such that (j+k+1)^2 - 4*k = 13*n^2; in fact, j(n) = A120869(n) and k(n) = A120870(n).",
				"Suppose g \u003e= 1 and let j = j(g). The numbers in row g of array D are among those n for which (j+k+1)^2 - 4*k = 13*n^2 for some k; that is, j stays fixed and k and n vary - hence the name \"fixed-j dispersion\". (The fixed-k dispersion for Q=13 is A120863.)",
				"Every positive integer occurs exactly once in array D and every pair of rows are mutually interspersed. That is, beginning at the first term of any row having greater initial term than that of another row, all the following terms individually separate the individual terms of the other row."
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL10/Kimberling2/kimberling45.html\"\u003eThe equation (j+k+1)^2 - 4*k = Q*n^2 and related dispersions\u003c/a\u003e, Journal of Integer Sequences, 10 (2007), Article #07.2.7.",
				"N. J. A. Sloane, \u003ca href=\"/classic.html#WYTH\"\u003eClassic Sequences\u003c/a\u003e."
			],
			"formula": [
				"Define f(n) = floor(r*n) - floor(3*F(n)), where r = (11 + 3*sqrt(13))/2 and F(n) is the fractional part of (1 + sqrt(13))*n/2. Let D(g,h) be the term in row g and column h of the array to be defined:",
				"D(1,1) = 1; D(1,2) = f(1); and D(1,h) = 11*D(1,h-1) - D(1,h-2) for h \u003e= 3.",
				"For arbitrary g \u003e= 1, once row g is defined, define D(g+1,1) = least positive integer not in rows 1,2,...,g; D(g+1,2) = f(D(g+1,1)); and D(g+1,h) = 11*D(g+1,h-1) - D(g+1,h-2) for h \u003e= 3. All rows after row 1 are thus inductively defined. [Corrected using Conjecture 1 in Kimberling (2007) by _Petros Hadjicostas_, Jul 07 2020]"
			],
			"example": [
				"Northwest corner:",
				"  1, 10, 109, 1189, ...",
				"  2, 20, 218, 2378, ...",
				"  3, 30, 327, 3567, ...",
				"  4, 43, 469, 5116, ...",
				"  5, 53, 578, 6305, ...",
				"  6, 63, 687, 7494, ...",
				"  ..."
			],
			"program": [
				"(PARI) f(n) = floor((11 + 3*sqrt(13))/2*n) - floor(3*frac((1 + sqrt(13))*n/2));",
				"unused(listus) = {my(v=vecsort(Vec(listus))); for (i=1, vecmax(v), if (!vecsearch(v, i), return (i)); ); };",
				"D(nb) = {my(m = matrix(nb, nb), t); my(listus = List); for (g=1, nb, if (g==1, t = 1, t = unused(listus)); m[g, 1]=t; listput(listus, t); t = f(t); m[g, 2]=t; listput(listus, t); for (h=3, nb, t = 11*m[g, h-1] - m[g, h-2]; m[g, h] = t; listput(listus, t); ); ); m; };",
				"lista(nb) = {my(m=D(nb)); for (n=1, nb, for (j=1, n, print1(m[n-j+1, j], \", \");););} \\\\ _Michel Marcus_, Jul 09 2020"
			],
			"xref": [
				"Cf. A120858, A120859, A120860, A120861, A120863, A120869, A120870."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Jul 09 2006",
			"ext": [
				"Name edited by _Petros Hadjicostas_, Jul 07 2020",
				"More terms from _Michel Marcus_, Jul 09 2020"
			],
			"references": 7,
			"revision": 26,
			"time": "2020-07-11T03:06:31-04:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}