{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A216624",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 216624,
			"data": "1,2,2,2,5,2,3,4,4,3,2,8,6,8,2,4,4,6,6,4,4,2,10,4,15,4,10,2,4,4,12,6,6,12,4,4,3,11,4,16,8,16,4,11,3,4,6,8,6,8,8,6,8,6,4,2,10,10,22,4,30,4,22,10,10,2,6,4,8,9,8,8,8,8,9,8,4,6",
			"name": "Square array read by antidiagonals, T(n,k) = sum_{c|n,d|k} gcd(c,d) for n\u003e=1, k\u003e=1.",
			"comment": [
				"T(n,k) = number of subgroups of C_n X C_k. [Hampjes et al.] - _N. J. A. Sloane_, Feb 02 2013"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A216624/b216624.txt\"\u003eAntidiagonals n = 1..141, flattened\u003c/a\u003e",
				"M. Hampejs, N. Holighaus, L. Toth and C. Wiesmeyr, \u003ca href=\"http://www.univie.ac.at/nuhag-php/bibtex/open_files/13485_121031-Toth-finalsubmission.pdf\"\u003eOn the subgroups of the group Z_m X Z_n\u003c/a\u003e, 2012. - From _N. J. A. Sloane_, Feb 02 2013"
			],
			"formula": [
				"T(n,n) = A060724(n) = sum_{d|n} d*tau((n/d)^2).",
				"T(n,1) = T(1,n) = A000005(n) = tau(n).",
				"T(n,2) = T(2,n) = A060710(n) = sum_{d|n} (3-[d is odd]) (Iverson bracket).",
				"T(n+1,n) = A092517(n) = tau(n+1)*tau(n).",
				"T(prime(n),1) = A007395(n) = 2.",
				"T(prime(n),prime(n)) = A113935(n) = prime(n)+3."
			],
			"example": [
				"[----1---2---3---4---5---6---7---8---9--10--11--12]",
				"[ 1] 1,  2,  2,  3,  2,  4,  2,  4,  3,  4,  2,  6",
				"[ 2] 2,  5,  4,  8,  4, 10,  4, 11,  6, 10,  4, 16",
				"[ 3] 2,  4,  6,  6,  4, 12,  4,  8, 10,  8,  4, 18",
				"[ 4] 3,  8,  6, 15,  6, 16,  6, 22,  9, 16,  6, 30",
				"[ 5] 2,  4,  4,  6,  8,  8,  4,  8,  6, 16,  4, 12",
				"[ 6] 4, 10, 12, 16,  8, 30,  8, 22, 20, 20,  8, 48",
				"[ 7] 2,  4,  4,  6,  4,  8, 10,  8,  6,  8,  4, 12",
				"[ 8] 4, 11,  8, 22,  8, 22,  8, 37, 12, 22,  8, 44",
				"[ 9] 3,  6, 10,  9,  6, 20,  6, 12, 23, 12,  6, 30",
				"[10] 4, 10,  8, 16, 16, 20,  8, 22, 12, 40,  8, 32",
				"[11] 2,  4,  4,  6,  4,  8,  4,  8,  6,  8, 14, 12",
				"[12] 6, 16, 18, 30, 12, 48, 12, 44, 30, 32, 12, 90",
				".",
				"Displayed as a triangular array:",
				"1,",
				"2,  2,",
				"2,  5,  2,",
				"3,  4,  4,  3,",
				"2,  8,  6,  8, 2,",
				"4,  4,  6,  6, 4,  4,",
				"2, 10,  4, 15, 4, 10, 2,",
				"4,  4, 12,  6, 6, 12, 4,  4,",
				"3, 11,  4, 16, 8, 16, 4, 11, 3,"
			],
			"maple": [
				"with(numtheory):",
				"T:= (n, k)-\u003e add(add(igcd(c,d), c=divisors(n)), d=divisors(k)):",
				"seq(seq(T(n, 1+d-n), n=1..d), d=1..14); # _Alois P. Heinz_, Sep 12 2012",
				"T:=proc(m,n) local d; add( d*tau(m*n/d^2), d in divisors(gcd(m,n))); end; # _N. J. A. Sloane_, Feb 02 2013"
			],
			"mathematica": [
				"t[n_, k_] := Sum[Sum[GCD[c, d], {c, Divisors[n]}], {d, Divisors[k]}]; Table[t[n-k+1, k], {n, 1, 12}, {k, 1, n}] // Flatten (* _Jean-François Alcover_, May 21 2013 *)"
			],
			"program": [
				"(Sage)",
				"def A216624(n, k) :",
				"    cp = cartesian_product([divisors(n), divisors(k)])",
				"    return reduce(lambda x,y: x+y, map(gcd, cp))",
				"for n in (1..12): [A216624(n,k) for k in (1..12)]"
			],
			"xref": [
				"Cf. A216620, A216621, A216622, A216623, A216625, A216626, A216627.",
				"Main diagonal is A060724.",
				"Rows give A000005, A060710, A054584, A221951, A221852."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Peter Luschny_, Sep 12 2012",
			"references": 13,
			"revision": 23,
			"time": "2020-03-07T14:58:35-05:00",
			"created": "2012-09-12T21:26:29-04:00"
		}
	]
}