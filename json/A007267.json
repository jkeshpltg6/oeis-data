{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007267",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7267,
			"id": "M5369",
			"data": "1,104,4372,96256,1240002,10698752,74428120,431529984,2206741887,10117578752,42616961892,166564106240,611800208702,2125795885056,7040425608760,22327393665024,68134255043715,200740384538624",
			"name": "Expansion of 16 * (1 + k^2)^4 /(k * k'^2)^2 in powers of q where k is the Jacobian elliptic modulus, k' the complementary modulus and q is the nome.",
			"comment": [
				"McKay-Thompson series of class 2A for the Monster group with a(0) = 104."
			],
			"reference": [
				"J. M. Borwein and P. B. Borwein, Pi and the AGM, Wiley, 1987, p. 195.",
				"R. Fricke, Die elliptischen Funktionen und ihre Anwendungen, Teubner, 1922, Vol. 2, see p. 517.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A007267/b007267.txt\"\u003eTable of n, a(n) for n = -1..10000\u003c/a\u003e (terms -1..1000 from T. D. Noe)",
				"J. H. Conway and S. P. Norton, \u003ca href=\"http://blms.oxfordjournals.org/content/11/3/308.extract\"\u003eMonstrous Moonshine\u003c/a\u003e, Bull. Lond. Math. Soc. 11 (1979) 308-339.",
				"D. Ford, J. McKay and S. P. Norton, \u003ca href=\"http://dx.doi.org/10.1080/00927879408825127\"\u003eMore on replicable functions\u003c/a\u003e, Commun. Algebra 22, No. 13, 5175-5193 (1994).",
				"R. Fricke, \u003ca href=\"http://www.archive.org/details/elliptischenfun02ricrich\"\u003eDie elliptischen Funktionen und ihre Anwendungen, Vol. 2\u003c/a\u003e.",
				"Masao Koike, \u003ca href=\"https://oeis.org/A004016/a004016.pdf\"\u003eModular forms on non-compact arithmetic triangle groups\u003c/a\u003e, Unpublished manuscript [Extensively annotated with OEIS A-numbers by N. J. A. Sloane, Feb 14 2021. I wrote 2005 on the first page but the internal evidence suggests 1997.]",
				"J. McKay and H. Strauss, \u003ca href=\"http://dx.doi.org/10.1080/00927879008823911\"\u003eThe q-series of monstrous moonshine and the decomposition of the head characters\u003c/a\u003e, Comm. Algebra 18 (1990), no. 1, 253-278.",
				"Titus Piezas III, \u003ca href=\"http://www.oocities.org/titus_piezas/Ramanujan_a.pdf\"\u003eRamanujan's Constant exp(Pi sqrt(163)) And Its Cousins\u003c/a\u003e.",
				"Michael Somos, \u003ca href=\"/A007191/a007191.pdf\"\u003eEmails to N. J. A. Sloane, 1993\u003c/a\u003e",
				"\u003ca href=\"/index/Mat#McKay_Thompson\"\u003eIndex entries for McKay-Thompson series for Monster simple group\u003c/a\u003e"
			],
			"formula": [
				"Expansion of 16 * (1 + k'^2)^4 /(k' * k^2)^2 in powers of q^2. - _Michael Somos_, Nov 11 2006",
				"a(n) ~ exp(2*Pi*sqrt(2*n)) / (2^(3/4)*n^(3/4)). - _Vaclav Kotesovec_, Apr 01 2017"
			],
			"example": [
				"G.f. = 1/q + 104 + 4372*q + 96256*q^2 + 1240002*q^3 + 10698752*q^4 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c -1, 0, With[ {m = InverseEllipticNomeQ[ q]}, SeriesCoefficient[ 16 (1 + m)^4 /(m (1 - m)^2), {q, 0, n}]]]; (* _Michael Somos_, Jun 29 2011 *)",
				"a[ n_] := If[ n \u003c -1, 0, With[ {m = ModularLambda[ Log[q]/(Pi I)]}, SeriesCoefficient[ 16 (1 + m)^4 /(m (1 - m)^2), {q, 0, n}]]]; (* _Michael Somos_, Jun 30 2011 *)",
				"QP = QPochhammer; A = (QP[q]/QP[q^2])^12; s = (A + 64*(q/A))^2 + O[q]^30; CoefficientList[s, q] (* _Jean-François Alcover_, Nov 16 2015, adapted from PARI *)",
				"nmax = 20; CoefficientList[Series[128*x + Product[1/(1 + x^k)^24, {k, 1, nmax}] + 4096*x^2*Product[(1 + x^k)^24, {k, 1, nmax}], {x, 0, nmax}], x] (* _Vaclav Kotesovec_, Jun 03 2018 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c-1, 0, A = prod(k=1, n\\2 + 1, 1 - x^(2*k - 1),1 + x^2 * O(x^n))^12; polcoeff( (64 * x / A + A)^2, n+1))};",
				"(PARI) {a(n) = my(A); if( n\u003c-1, 0, n++; A = x * O(x^n); A = (eta(x + A) / eta(x^2 + A))^12; polcoeff( (A + 64 * x / A)^2, n))}; /* _Michael Somos_, Nov 11 2006 */"
			],
			"xref": [
				"Cf. A007241, A045478. Convolution square of A007247.",
				"A045478, A007241, A106207, A007267, and A101558 are all essentially the same sequence."
			],
			"keyword": "nonn,nice",
			"offset": "-1,2",
			"author": "_N. J. A. Sloane_, Apr 28 1994",
			"references": 199,
			"revision": 54,
			"time": "2021-03-12T22:24:41-05:00",
			"created": "1994-04-28T03:00:00-04:00"
		}
	]
}