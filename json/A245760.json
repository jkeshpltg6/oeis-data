{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A245760",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 245760,
			"data": "0,1,1,1,1,1,1,2,1,2,2,1,2,2,2,2,2,2,2,2,2,2,3,2,2,3,3,3,3,2,3,2,3,3,3,2,3,2,3,3,3,3,3,3,3,3,3,2,3,3,3,4,3,3,3,3,3,3,3,3,4,3,3,3,3,3,3,4,4,3,3,3,3,3,4,3,4,3,3,3,4,3,4,3,3,4,3",
			"name": "Maximal multiplicative persistence of n in any base.",
			"comment": [
				"It has been conjectured that there is a maximum multiplicative persistence in a given base, but it is not known if this sequence is bounded."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A245760/b245760.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"a(23)=3 since the persistence of 23 in base 6 is 3 (23 in base 6 is 35 / 3x5=15 / 15 in base 6 is 23 / 2x3=6 / 6 in base 6 is 10 / 1x0=0 which is a single digit). In any other base the persistence of 23 is 3 or less, therefore a(23)=3.",
				"a(12)=1 since 12 does not have a multiplicative persistence greater than 1 in any base."
			],
			"maple": [
				"persistence:= proc(n,b) local i,m;",
				"  m:= n;",
				"  for i from 1 do",
				"       m:= convert(convert(m,base,b),`*`);",
				"     if m \u003c b then return i fi",
				"  od:",
				"end proc:",
				"A:= n -\u003e max(seq(persistence(n,b),b=2..n-1)):",
				"0, 1, seq(A(n),n=3..100); # _Robert Israel_, Jul 31 2014"
			],
			"mathematica": [
				"persistence[n_, b_] := Module[{i, m}, m = n; For[i = 1, True, i++, m = Times @@ IntegerDigits[m, b]; If[m \u003c b, Return [i]]]];",
				"A[n_] := Max[Table[persistence[n, b], {b, 2, n-1}]];",
				"Join[{0, 1}, Table[A[n], {n, 3, 100}]] (* _Jean-François Alcover_, Apr 30 2019, after _Robert Israel_ *)"
			],
			"xref": [
				"Cf. A003001, A031346, A064867, A064868, A046510."
			],
			"keyword": "nonn",
			"offset": "1,8",
			"author": "_Sergio Pimentel_, Jul 31 2014",
			"references": 2,
			"revision": 16,
			"time": "2021-11-13T04:45:37-05:00",
			"created": "2014-08-01T07:26:16-04:00"
		}
	]
}