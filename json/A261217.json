{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A261217",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 261217,
			"data": "0,1,1,2,0,2,3,3,5,3,4,2,0,4,4,5,5,4,1,3,5,6,4,3,5,5,2,6,7,7,1,2,1,4,7,7,8,6,8,0,0,0,14,6,8,9,9,11,9,2,1,15,15,11,9,10,8,6,10,10,3,22,14,12,10,10,11,11,10,7,9,11,23,23,16,13,9,11,12,10,9,11,11,8,0,22,21,17,17,8,12,13,13,7,8,7,10,1,1,19,20,13,16,19,13,14,12,14,6,6,6,12,0,2,18,18,12,8,18,14",
			"name": "A(i,j) = rank (in A060118) of the composition of the i-th and the j-th permutation in table A060118, which lists all finite permutations.",
			"comment": [
				"The square array A(row\u003e=0, col\u003e=0) is read by downwards antidiagonals as: A(0,0), A(0,1), A(1,0), A(0,2), A(1,1), A(2,0), A(0,3), A(1,2), A(2,1), A(3,0), ...",
				"A(i,j) gives the rank (in ordering used by table A060118) of the permutation which is obtained by composing permutations p and q listed as the i-th and the j-th permutation in irregular table A060118 (note that the identity permutation is the 0th). Here the convention is that \"permutations act of the left\", thus, if p1 and p2 are permutations, then the product of p1 and p2 (p1 * p2) is defined such that (p1 * p2)(i) = p1(p2(i)) for i=1...",
				"Equally, A(i,j) gives the rank in A060117 of the composition of the i-th and the j-th permutation in A060117, when convention is that \"permutations act on the right\".",
				"Each row and column is a permutation of A001477, because this is the Cayley table (\"multiplication table\") of an infinite enumerable group, namely, that subgroup of the infinite symmetric group (S_inf) which consists of permutations moving only finite number of elements."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A261217/b261217.txt\"\u003eTable of n, a(n) for n = 0..7259; the first 120 antidiagonals of array\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Cayley_table\"\u003eCayley table\u003c/a\u003e"
			],
			"formula": [
				"By conjugating with related permutations and arrays:",
				"A(i,j) = A060125(A261216(A060125(i),A060125(j))).",
				"A(i,j) = A060127(A261096(A060120(i),A060120(j))).",
				"A(i,j) = A060126(A261097(A060119(i),A060119(j)))."
			],
			"example": [
				"The top left corner of the array:",
				"   0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, ...",
				"   1,  0,  3,  2,  5,  4,  7,  6,  9,  8, 11, 10, 13, ...",
				"   2,  5,  0,  4,  3,  1,  8, 11,  6, 10,  9,  7, 14, ...",
				"   3,  4,  1,  5,  2,  0,  9, 10,  7, 11,  8,  6, 15, ...",
				"   4,  3,  5,  1,  0,  2, 10,  9, 11,  7,  6,  8, 16, ...",
				"   5,  2,  4,  0,  1,  3, 11,  8, 10,  6,  7,  9, 17, ...",
				"   6,  7, 14, 15, 22, 23,  0,  1, 12, 13, 18, 19,  8, ...",
				"   7,  6, 15, 14, 23, 22,  1,  0, 13, 12, 19, 18,  9, ...",
				"   8, 11, 12, 16, 21, 19,  2,  5, 14, 17, 20, 23,  6, ...",
				"   9, 10, 13, 17, 20, 18,  3,  4, 15, 16, 21, 22,  7, ...",
				"  10,  9, 17, 13, 18, 20,  4,  3, 16, 15, 22, 21, 11, ...",
				"  11,  8, 16, 12, 19, 21,  5,  2, 17, 14, 23, 20, 10, ...",
				"  12, 19,  8, 21, 16, 11, 14, 23,  2, 20, 17,  5,  0, ...",
				"  ...",
				"For A(1,2) (row=1, column=2, both starting from zero), we take as permutation p the permutation which has rank=1 in the ordering used by A060118, which is a simple transposition (1 2), which we can extend with fixed terms as far as we wish (e.g., like {2,1,3,4,5,...}), and as permutation q we take the permutation which has rank=2 (in the same list), which is {1,3,2}. We compose these from the left, so that the latter one, q, acts first, thus c(i) = p(q(i)), and the result is permutation {2,3,1}, which is listed as the 3rd one in A060118, thus A(1,2) = 3.",
				"For A(2,1) we compose those two permutations in opposite order, as d(i) = q(p(i)), which gives permutation {3,1,2} which is listed as the 5th one in A060118, thus A(2,1) = 5."
			],
			"xref": [
				"Transpose: A261216.",
				"Row 0 \u0026 Column 0: A001477 (identity permutation)",
				"Row 1: A004442.",
				"Column 1: A261218.",
				"Main diagonal: A261219.",
				"Cf. also A060117, A060118, A261096, A261097.",
				"Cf. also A089839.",
				"Permutations used in conjugation-formulas: A060119, A060120, A060125, A060126, A060127."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_Antti Karttunen_, Aug 26 2015",
			"references": 6,
			"revision": 22,
			"time": "2015-09-24T01:40:58-04:00",
			"created": "2015-09-24T01:40:58-04:00"
		}
	]
}