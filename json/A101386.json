{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A101386",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 101386,
			"data": "5,27,157,915,5333,31083,181165,1055907,6154277,35869755,209064253,1218515763,7102030325,41393666187,241259966797,1406166134595,8195736840773,47768254910043,278413792619485,1622714500806867,9457873212221717,55124524772523435",
			"name": "Expansion of g.f.: (5 - 3*x)/(1 - 6*x + x^2).",
			"comment": [
				"A floretion-generated sequence relating to NSW numbers and numbers n such that (n^2 - 8)/2 is a square. It is also possible to label this sequence as the \"tesfor-transform of the zero-sequence\" under the floretion given in the program code, below. This is because the sequence \"vesseq\" would normally have been A046184 (indices of octagonal numbers which are also a square) using the floretion given. This floretion, however, was purposely \"altered\" in such a way that the sequence \"vesseq\" would turn into A000004. As (a(n)) would not have occurred under \"natural\" circumstances, one could speak of it as the transform of A000004.",
				"From _Wolfdieter Lang_, Feb 05 2015: (Start)",
				"All positive solutions x = a(n) of the (generalized) Pell equation x^2 - 2*y^2 = +7 based on the  fundamental solution (x2,y2) = (5,3) of the second class of (proper) solutions. The corresponding y solutions are given by y(n) = A253811(n).",
				"All other positive solutions come from the first class of (proper) solutions based on the fundamental solution (x1,y1) = (3,1). These are given in A038762 and A038761.",
				"All solutions of this Pell equation are found in A077443(n+1) and A077442(n), for n \u003e= 0. See, e.g., the Nagell reference on how to find all solutions.",
				"(End)"
			],
			"reference": [
				"T. Nagell, Introduction to Number Theory, Chelsea Publishing Company, 1964, Theorem 109, pp. 207-208 with Theorem 104, pp. 197-198."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A101386/b101386.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"M. A. Gruber, Artemas Martin, A. H. Bell, J. H. Drummond, A. H. Holmes and H. C. Wilkes, \u003ca href=\"http://www.jstor.org/stable/2968551\"\u003eProblem 47\u003c/a\u003e, Amer. Math. Monthly, 4 (1897), 25-28.",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"Morris Newman, Daniel Shanks, H. C. Williams, \u003ca href=\"http://matwbn.icm.edu.pl/ksiazki/aa/aa38/aa3826.pdf\"\u003eSimple groups of square order and an interesting sequence of primes\u003c/a\u003e, Acta Arith., 38 (1980/1981) 129-140. MR82b:20022.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/NSWNumber.html\"\u003eNSW Number.\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (6,-1)."
			],
			"formula": [
				"a(n) = A002315(n) + A077445(n+1). Note: the offset of A077445 is 1.",
				"a(n+1) - a(n) = 2*A054490(n+1).",
				"a(n) = 6*a(n-1) - a(n-2), a(0)=5, a(1)=27. - _Philippe Deléham_, Nov 17 2008",
				"From Al Hakanson (hawkuu(AT)gmail.com), Aug 17 2009: (Start)",
				"a(n) = ((5+sqrt(18))*(3 + sqrt(8))^n + (5-sqrt(18))*(3 - sqrt(8))^n)/2.",
				"Third binomial transform of A164737. (End)",
				"a(n) = rational part of z(n, with z(n) = (5+3*sqrt(2))*(3+2*sqrt(2))^n), n \u003e= 0, the general positive solutions of the second class of proper solutions. See the preceding formula. - _Wolfdieter Lang_, Feb 05 2015",
				"a(n) = 5*ChebyshevU(n, 3) - 3*ChebyshevU(n-1, 3). - _G. C. Greubel_, Mar 17 2020",
				"a(n) = Pell(2*n+2) + 3*Pell(2*n+1), where Pell(n) = A000129(n). - _G. C. Greubel_, Apr 17 2020"
			],
			"maple": [
				"A101386:= (n) -\u003e simplify(5*ChebyshevU(n, 3) - 3*ChebyshevU(n-1, 3)); seq( A101386(n), n = 0..30); # _G. C. Greubel_, Mar 17 2020"
			],
			"mathematica": [
				"CoefficientList[ Series[(5-3x)/(1-6x+x^2), {x,0,30}], x] (* _Robert G. Wilson v_, Jan 29 2005 *)",
				"LinearRecurrence[{6,-1},{5,27},30] (* _Harvey P. Dale_, Apr 23 2016 *)"
			],
			"program": [
				"Floretion Algebra Multiplication Program FAMP code: - tesforseq[ + 3'i - 2'j + 'k + 3i' - 2j' + k' - 4'ii' - 3'jj' + 4'kk' - 'ij' - 'ji' + 3'jk' + 3'kj' + 4e], Note: vesforseq = A000004, lesforseq = A002315, jesforseq = A077445",
				"(PARI) Vec((5-3*x)/(1-6*x+x^2) + O(x^30)) \\\\ _Colin Barker_, Feb 05 2015",
				"(MAGMA) R\u003cx\u003e:=PowerSeriesRing(Integers(), 30); Coefficients(R!((5 - 3*x)/(1-6*x+x^2))); // _G. C. Greubel_, Jul 26 2018",
				"(Sage) [5*chebyshev_U(n,3) -3*chebyshev_U(n-1,3) for n in (0..30)] # _G. C. Greubel_, Mar 17 2020"
			],
			"xref": [
				"Cf. A000004, A000129, A002315, A038761, A038762, A054490, A164737.",
				"Cf. A077442, A077443(n+1), A077445, A253811."
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "_Creighton Dement_, Jan 23 2005",
			"ext": [
				"More terms from _Robert G. Wilson v_, Jan 29 2005"
			],
			"references": 14,
			"revision": 45,
			"time": "2020-04-18T22:04:22-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}