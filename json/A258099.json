{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A258099",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 258099,
			"data": "1,-2,5,-12,26,-50,92,-168,295,-496,818,-1332,2126,-3324,5126,-7824,11793,-17548,25857,-37788,54734,-78578,111968,-158496,222842,-311224,432095,-596676,819504,-1119624,1522282,-2060448,2776514,-3725294,4978142,-6626988",
			"name": "Expansion of ( psi(x^3) * phi(-x^3) / (psi(x) * f(-x^2)) )^2 in powers of x where phi(), psi(), f() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A258099/b258099.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(-1/3) * (eta(q) * eta(q^3) * eta(q^6) / eta(q^2)^3)^2 in powers of q.",
				"Euler transform of period 6 sequence [ -2, 4, -4, 4, -2, 0, ...].",
				"G.f.: Product_{k\u003e0} (1 - x^k + x^(2*k))^2 * (1 + x^k + x^(2*k))^4 / (1 + x^k)^4.",
				"a(n) = A258100(3*n + 1).",
				"a(n) ~ (-1)^n * exp(2*Pi*sqrt(n/3)) / (2 * 3^(9/4) * n^(3/4)). - _Vaclav Kotesovec_, Jun 06 2018"
			],
			"example": [
				"G.f. = 1 - 2*x + 5*x^2 - 12*x^3 + 26*x^4 - 50*x^5 + 92*x^6 - 168*x^7 + ...",
				"G.f. = q - 2*q^4 + 5*q^7 - 12*q^10 + 26*q^13 - 50*q^16 + 92*q^19 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ (QPochhammer[ x] * QPochhammer[ x^3] *  QPochhammer[ x^6] / QPochhammer[ x^2]^3)^2, {x, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( (eta(x + A) * eta(x^3 + A) * eta(x^6 + A) / eta(x^2 + A)^3)^2, n))};"
			],
			"xref": [
				"Cf. A132977, A258100."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, May 20 2015",
			"references": 3,
			"revision": 15,
			"time": "2021-03-12T22:24:48-05:00",
			"created": "2015-05-20T01:09:12-04:00"
		}
	]
}