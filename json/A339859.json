{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A339859",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 339859,
			"data": "9,16,25,36,49,64,49,64,81,100,121,81,121,100,121,169,196,121,169,144,169,196,225,256,289,169,289,361,196,225,256,289,324,361,400,441,225,289,361,256,289,361,484,529,289,361,441,529,625,324,361,400,441,484,529,576",
			"name": "Largest side of integer-sided primitive triangles whose sides a \u003c b \u003c c form a geometric progression.",
			"comment": [
				"The triples of sides (a, b, c) with a \u003c b \u003c c are in increasing lexicographic order. This sequence lists the c's.",
				"For the corresponding primitive triples and miscellaneous properties and references, see A339856.",
				"The terms are all squares \u003e= 9 but they are not in increasing order. For example, a(6) = 64 for triple (25, 40, 64) while a(7) = 49 for triple (36, 42, 49)."
			],
			"link": [
				"Project Euler, \u003ca href=\"https://projecteuler.net/problem=370\"\u003eProblem 370: Geometric triangles\u003c/a\u003e."
			],
			"formula": [
				"a(n) = A339856(n, 3)."
			],
			"example": [
				"a(1) = 9 for only the smallest such triangle (4, 6, 9) with 6^2 = 4*9, this one corresponds to an obtuse triangle because sqrt(phi) \u003c q = 3/2 \u003c phi, hence C \u003e Pi/2.",
				"a(3) = 25 for only the triple (16, 20, 25) with 16 * 25 = 20^2, this one corresponds to an acute triangle because 1 \u003c q = 5/4 \u003c sqrt(phi), hence C \u003c Pi/2."
			],
			"maple": [
				"for a from 1 to 300 do",
				"for b from a+1 to floor((1+sqrt(5))/2 *a) do",
				"for c from b+1 to floor((1+sqrt(5))/2 *b) do k:=a*c;",
				"if k=b^2 and igcd(a, b, c)=1 then print(c); end if;",
				"end do;",
				"end do;",
				"end do;"
			],
			"program": [
				"(PARI) lista(nn) = {my(phi = (1+sqrt(5))/2); for (a=1, nn, for (b=a+1, floor(a*phi), for (c=b+1, floor(b*phi), if ((a*c == b^2) \u0026\u0026 (gcd([a, b, c])==1), print1(c, \", \"); ); ); ); ); } \\\\ _Michel Marcus_, Jan 07 2021"
			],
			"xref": [
				"Cf. A339856 (triples), A339857 (smallest side), A339858 (middle side), this sequence (largest side), A339860 (perimeter).",
				"Cf. A336753 (similar for sides in arithmetic progression).",
				"Cf. A335896 (similar for angles in arithmetic progression)."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Bernard Schott_, Jan 05 2021",
			"references": 4,
			"revision": 22,
			"time": "2021-02-21T13:53:16-05:00",
			"created": "2021-01-08T04:12:09-05:00"
		}
	]
}