{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A097736",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 97736,
			"data": "1,257,66305,17106433,4413393409,1138638393089,293764292023553,75790048703683585,19553538801258341377,5044737220675948391681,1301522649395593426712321,335787798806842428143387137,86631950569515950867567169025,22350707459136308481404186221313",
			"name": "Pell equation solutions (8*b(n))^2 - 65*a(n)^2 = -1 with b(n):=A097735(n), n \u003e= 0.",
			"link": [
				"Indranil Ghosh, \u003ca href=\"/A097736/b097736.txt\"\u003eTable of n, a(n) for n = 0..413\u003c/a\u003e",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"Giovanni Lucca, \u003ca href=\"http://forumgeom.fau.edu/FG2019volume19/FG201902index.html\"\u003eInteger Sequences and Circle Chains Inside a Hyperbola\u003c/a\u003e, Forum Geometricorum (2019) Vol. 19, 11-16.",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (258,-1)."
			],
			"formula": [
				"a(n) = S(n, 2*129) - S(n-1, 2*129) = T(2*n+1, sqrt(65))/sqrt(65), with Chebyshev polynomials of the 2nd and first kind. See A049310 for the triangle of S(n, x)= U(n, x/2) coefficients. S(-1, x) := 0 =: U(-1, x); and A053120 for the T-triangle.",
				"a(n) = ((-1)^n)*S(2*n, 16*i) with the imaginary unit i and Chebyshev polynomials S(n, x) with coefficients shown in A049310.",
				"G.f.: (1-x)/(1-258*x+x^2).",
				"a(n) = 258*a(n-1) - a(n-2), n \u003e 1; a(0)=1, a(1)=257. - _Philippe Deléham_, Nov 18 2008"
			],
			"example": [
				"(x,y) = (8,1), (2072,257), (534568,66305), ... give the positive integer solutions to x^2 - 65*y^2 =-1."
			],
			"mathematica": [
				"LinearRecurrence[{258, -1},{1, 257},20] (* _Ray Chandler_, Aug 12 2015 *)"
			],
			"program": [
				"(PARI) my(x='x+O('x^20)); Vec((1-x)/(1-258*x+x^2)) \\\\ _G. C. Greubel_, Aug 01 2019",
				"(MAGMA) I:=[1,257]; [n le 2 select I[n] else 258*Self(n-1) - Self(n-2): n in [1..20]]; // _G. C. Greubel_, Aug 01 2019",
				"(Sage) ((1-x)/(1-258*x+x^2)).series(x, 20).coefficients(x, sparse=False) # _G. C. Greubel_, Aug 01 2019",
				"(GAP) a:=[1,257];; for n in [3..20] do a[n]:=258*a[n-1]-a[n-2]; od; a; # _G. C. Greubel_, Aug 01 2019"
			],
			"xref": [
				"Cf. A097734 for S(n, 258).",
				"Row 8 of array A188647."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Aug 31 2004",
			"references": 5,
			"revision": 32,
			"time": "2020-01-23T03:51:17-05:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}