{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A278349",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 278349,
			"data": "10,15,146,237,597,1603,14545,13863,16053,55805,393565,219509,153263,35981,1789339,4686907,11047438,7487726,1589662,39291566,12581421,6974465,93519854,140140538,5835191,756869018,251306317,285074689,1102062347,327023206,4282236806",
			"name": "a(n) is the smallest semiprime followed by gap (to the next semiprime) equal to n-th semiprime.",
			"comment": [
				"It appears that a(n) exists for any n.",
				"a(41) = 42264532659, a(42) = 34279038379, a(45) = 58898255693, a(47) = 70265467489, and a(50) = 49167925231. - _Charles R Greathouse IV_, Dec 07 2016"
			],
			"link": [
				"Dana Jacobsen, \u003ca href=\"/A278349/b278349.txt\"\u003eTable of n, a(n) for n = 1..55\u003c/a\u003e (first 39 terms from Charles R Greathouse IV)"
			],
			"example": [
				"a(1)=10 because p=10 and q=14 are two consecutive semiprimes and q-p=4 (first semiprime A001358(1)),",
				"a(2)=15 because p=15 and q=21 are two consecutive semiprimes and q-p=6 (2nd semiprime A001358(2)),",
				"a(3)=146 because p=146 and q=155 are two consecutive semiprimes and q-p=6=A001358(3))."
			],
			"program": [
				"(PARI) issemi(n)=bigomega(n)==2",
				"nextsemi(n)=while(!issemi(n), n++); n",
				"listsemi(a,b)=my(v=List()); if(a\u003c4, a=4); forprime(p=sqrtint(a-1)+1,b\\2, forprime(q=(a-1)\\p+1,min(b\\p,p), listput(v,p*q))); Set(v)",
				"list(lim)=",
				"{",
				"  lim=nextsemi(ceil(lim));",
				"  my(foundsemi=[0], small=listsemi(4,max(sqrtint(lim),4)), v=small, lastsmall=small[#small], needed=4);",
				"  while(v[1]\u003clim,",
				"    for(i=2,#v,",
				"      my(g=v[i]-v[i-1], idx);",
				"      if(g\u003cneeded, next);",
				"      if(g\u003elastsmall,",
				"        small=listsemi(4,max(2*lastsmall,g));",
				"        lastsmall=small[#small]",
				"      );",
				"      idx=setsearch(small,g);",
				"      if(!idx, next);",
				"      if(idx\u003e#foundsemi,",
				"        foundsemi=concat(foundsemi,vector(idx-#foundsemi));",
				"      );",
				"      if(foundsemi[idx]==0,",
				"        foundsemi[idx]=v[i-1];",
				"        if(needed==g,",
				"          for(j=idx,#foundsemi,",
				"            if(foundsemi[j]==0, break);",
				"            needed=small[j+1];",
				"          )",
				"        );",
				"      )",
				"    );",
				"    v=listsemi(v[#v], min(nextsemi(v[#v]+10^7), lim));",
				"  );",
				"  for(i=1,#foundsemi,",
				"    if(foundsemi[i]==0, return(foundsemi[1..i-1]))",
				"  );",
				"  foundsemi;",
				"} \\\\ _Charles R Greathouse IV_, Nov 19 2016",
				"(Perl) use ntheory \":all\";",
				"  my($l,$nxti,$nxt,@G)=(4,1,4);",
				"  forsemiprimes {",
				"    if (!exists $G[$_-$l]) {",
				"      $G[$_-$l]=$l;",
				"      while (exists $G[$nxt]) {",
				"        print \"$nxti $G[$nxt]\\n\";",
				"        $nxt=nth_semiprime(++$nxti);",
				"      }",
				"    }",
				"    $l=$_;",
				"} 5,1e8; # _Dana Jacobsen_, Oct 16 2018"
			],
			"xref": [
				"Cf. A001358."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Zak Seidov_, Nov 19 2016",
			"ext": [
				"a(14) and a(19) corrected by _Charles R Greathouse IV_, Nov 19 2016",
				"a(27)-a(35) from _Charles R Greathouse IV_, Nov 19 2016",
				"a(36)-a(38) from _Charles R Greathouse IV_, Nov 22 2016"
			],
			"references": 1,
			"revision": 26,
			"time": "2018-10-17T02:59:31-04:00",
			"created": "2016-11-19T17:06:45-05:00"
		}
	]
}