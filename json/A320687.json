{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A320687",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 320687,
			"data": "3,6,8,16,12,28,19,34,31,72,42,58,63,70,116,122,79,90,112,134,169,170,108,212,200,196,246,226,240,244,292,318,394,276,336,418,283,528,445,582,429,392,530,416,565,506,581,634,548,554,655,866,616,676,641,714,965,710,922,968,827",
			"name": "Sum of differences of the larger square and primes between two squares.",
			"comment": [
				"Consider the primes p1,...,pK between two squares n^2 and (n+1)^2, and take the sum of the differences (listed as A106044): ((n+1)^2 - p1) + ... + ((n+1)^2 - pK)."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A320687/b320687.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A014085(n)*A000290(n+1) - A108314(n), where A000290(n) = n^2."
			],
			"example": [
				"a(1) = 3 = 2 + 1, where {2, 1} = 4 - {2, 3: primes between 1^2 = 1 and 2^2 = 4}.",
				"a(2) = 6 = 4 + 2, with {4, 2} = 9 - {5, 7: primes between 2^2 = 4 and 3^2 = 9}.",
				"a(3) = 8 = sum of {5, 3} = 16 - {11, 13: primes between 3^2 = 9 and 4^2 = 16}.",
				"a(4) = 16 = sum of {8, 6, 2} = 25 - {17, 19, 23: primes between 4^2 and 5^2 = 25}.",
				"a(5) = 12 = sum of {7, 5} = 36 - {29, 31: primes between 5^2 = 25 and 6^2 = 36}."
			],
			"maple": [
				"N:= 100: # to get a(1)..a(N)",
				"V:= Vector(N):",
				"p:= 1;",
				"do",
				"   p:= nextprime(p);",
				"   n:= floor(sqrt(p));",
				"   if n \u003e N then break fi;",
				"   V[n]:= V[n]+(n+1)^2-p;",
				"od:",
				"convert(V,list); # _Robert Israel_, Jun 17 2019"
			],
			"program": [
				"(PARI) a(n,s=0)={forprime(p=n^2,(n+=1)^2,s+=n^2-p);s}"
			],
			"xref": [
				"Equals A014085 * A000290(.+1) - A108314.",
				"Row sums of A106044 read as a table."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_M. F. Hasler_, Oct 19 2018",
			"references": 1,
			"revision": 11,
			"time": "2019-06-17T18:02:10-04:00",
			"created": "2018-10-19T17:01:09-04:00"
		}
	]
}