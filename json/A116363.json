{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A116363",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 116363,
			"data": "1,2,7,30,141,698,3571,18686,99385,535122,2908863,15932766,87809541,486421770,2706138987,15110359038,84637982961,475381503266,2676447372535,15100548901790,85357620588541,483304834607322",
			"name": "a(n) = dot product of row n in Catalan triangle A033184 with row n in Pascal's triangle.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A116363/b116363.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"D. Drake, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL13/Drake/drake.html\"\u003eBijections from Weighted Dyck Paths to Schröder Paths\u003c/a\u003e, J. Int. Seq. 13 (2010) # 10.9.2."
			],
			"formula": [
				"a(n) = Sum_{k=0..n} C(n,k)*C(2*n-k+1,n-k)*(k+1)/(2*n-k+1).",
				"G.f. A(x) satisfies: d/dx[log(1 - 4*x*A(x))] = -4*(1-5*x)/(1-13*x+43*x^2-7*x^3).",
				"O.g.f.: 2*(R+x)/(R*(R+x+1)), where R = sqrt(x^2+6*x+1). [_Dan Drake_, May 19 2010]",
				"Conjecture: +(2*n+5)*(n+1)*a(n) +4*(-3*n^2-9*n+5)*a(n-1) +(2*n+7)*(n-1)*a(n-2)=0. - _R. J. Mathar_, Jun 22 2016"
			],
			"example": [
				"The dot product of Catalan row 4 and Pascal row 4 equals",
				"a(4) = [14,14,9,4,1]*[1,4,6,4,1] = 141",
				"which is equivalent to obtaining the final term",
				"in these repeated partial sums of Pascal row 4:",
				"1,4, 6, 4, 1",
				".5,11,15,16",
				"..16,31,47",
				"...47,94",
				"....141"
			],
			"mathematica": [
				"Table[Sum[Binomial[n, j]*Binomial[2*n-j+1, n-j]*(j+1)/(2*n-j+1), {j,0,n} ], {n,0,30}] (* _G. C. Greubel_, May 12 2019 *)"
			],
			"program": [
				"(PARI) a(n)=sum(k=0,n,binomial(n,k)*binomial(2*n-k+1,n-k)*(k+1)/(2*n-k+1))",
				"for(n=0,30,print1(a(n),\", \"))",
				"(MAGMA) [(\u0026+[Binomial(n,j)*Binomial(2*n-j+1, n-j)*(j+1)/(2*n-j+1): j in [0..n]]): n in [0..30]]; // _G. C. Greubel_, May 12 2019",
				"(Sage) [sum(binomial(n,j)*binomial(2*n-j+1, n-j)*(j+1)/(2*n-j+1) for j in (0..n)) for n in (0..30)] # _G. C. Greubel_, May 12 2019",
				"(GAP) List([0..30], n-\u003e Sum([0..n], j-\u003e Binomial(n,j)*Binomial(2*n-j+1, n-j)*(j+1)/(2*n-j+1))) # _G. C. Greubel_, May 12 2019"
			],
			"xref": [
				"Cf. A033184."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Paul D. Hanna_, Feb 04 2006",
			"references": 2,
			"revision": 17,
			"time": "2019-05-12T03:07:49-04:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}