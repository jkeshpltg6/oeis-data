{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A171798",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 171798,
			"data": "101,211,202,321,312,312,303,431,422,422,413,422,413,413,404,541,532,532,523,532,523,523,514,532,523,523,514,523,514,514,505,651,642,642,633,642,633,633,624,642,633,633,624,633,624,624,615,642,633,633,624",
			"name": "a(n) = base-10 concatenation XYZ, where X = number of bits in binary expansion of n, Y = number of 0's, Z = number of 1's.",
			"comment": [
				"Start with n, repeatedly apply the map i -\u003e a(i). Then every n converges to one of 1019, 1147, 1165 or 14311 (cf. A171813). Proof: this is true by direct calculation for n=1..2^14. For larger n, a(n) \u003c n."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A171798/b171798.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"14 = 1110 in base 2, so X=4, Y=1, Z=3, a(14)=413."
			],
			"maple": [
				"# Maple code for trajectories of numbers from 1 to M:",
				"F:=proc(n) local s,t1,t2; t1:=convert(n,base,2); t2:=nops(t1); s:=add(t1[i],i=1..t2);",
				"parse(cat(t2,t2-s,s)); end;",
				"M:=16384;",
				"for n from 1 to M do t3:=F(n); sw:=-1;",
				"for i from 1 to 10 do",
				"if (t3 = 1147) or (t3 = 1165) or (t3 = 1019) or (t3 = 14311) then sw:=1; break; fi;",
				"t3:=F(t3);",
				"od;",
				"if sw \u003c 0 then lprint(n); fi;",
				"od:",
				"Contribution from _R. J. Mathar_, Oct 15 2010: (Start)",
				"read(\"transforms\") ; cat2 := proc(a,b) dgsb := max(1,ilog10(b)+1) ; a*10^dgsb+b ; end proc:",
				"catL := proc(L) local a; a := op(1,L) ; for i from 2 to nops(L) do a := cat2(a,op(i,L)) ; end do; a; end proc:",
				"A070939 := proc(n) max(1,ilog2(n)+1) ; end proc:",
				"A171798 := proc(n) local n1,n3 ; n1 := A070939(n) ; n3 := wt(n) ; catL([n1,n1-n3,n3]) ; end proc:",
				"seq(A171798(n),n=1..80) ; (End)"
			],
			"mathematica": [
				"ans[n_]:=Module[{idn2=IntegerDigits[n,2]},FromDigits[{Length[idn2],Count[idn2,0],Count[idn2,1]}]]; Table[ans[i], {i, 50}] [From _Harvey P. Dale_, Nov 06 2010]"
			],
			"program": [
				"(Haskell)",
				"a171798 n = read $ concatMap (show . ($ n))",
				"                   [a070939, a023416, a000120] :: Integer",
				"-- _Reinhard Zumkeller_, Feb 22 2012"
			],
			"xref": [
				"Cf. A171797, A171813, A171823, A171825.",
				"Cf. A070939, A023416, A000120."
			],
			"keyword": "nonn,base,easy",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_, Oct 15 2010, Oct 16 2010",
			"ext": [
				"More terms from _R. J. Mathar_, Oct 15 2010"
			],
			"references": 6,
			"revision": 11,
			"time": "2013-07-13T12:03:41-04:00",
			"created": "2010-10-20T03:00:00-04:00"
		}
	]
}