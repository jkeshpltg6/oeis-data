{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A034198",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 34198,
			"data": "0,1,3,6,10,16,23,32,43,56,71,89,109,132,158,187,219,255,294,337,384,435,490,550,614,683,757,836,920,1010,1105,1206,1313,1426,1545,1671,1803,1942,2088,2241,2401,2569,2744,2927,3118,3317,3524,3740",
			"name": "Number of binary codes (not necessarily linear) of length n with 3 words.",
			"comment": [
				"Number of distinct triangles on vertices of n-dimensional cube.",
				"Also, a(n) is the number of orbits of C_2^2 subgroups of C_2^n under automorphisms of C_2^n.",
				"Also, a(n) is the number of faithful representations of C_2^2 of dimension n up to equivalence by automorphisms of (C_2^2).",
				"Also, a([n/2]) is equal to the number of partitions mu such that there exists a C_2^2 subgroup G of S_n such that the i^th largest (nontrivial) product of 2-cycles in G consists of mu_i 2-cycles (see below example). - _John M. Campbell_, Jan 22 2016"
			],
			"link": [
				"Campbell, John, \u003ca href=\"https://dmtcs.episciences.org/3210/pdf\"\u003eA class of symmetric difference-closed sets related to commuting involutions\u003c/a\u003e, Discrete Mathematics \u0026 Theoretical Computer Science, Vol 19 no. 1, 2017.",
				"J. Brandts and C. Cihangir, \u003ca href=\"http://www.math.cas.cz/~am2013/proceedings/contributions/brandts.pdf\"\u003eCounting triangles that share their vertices with the unit n-cube\u003c/a\u003e, in Conference Applications of Mathematics 2013 in honor of the 70th birthday of Karel Segeth. Jan Brandts, Sergey Korotov, et al., eds., Institute of Mathematics AS CR, Prague 2013.",
				"Jan Brandts, A. Cihangir, \u003ca href=\"http://arxiv.org/abs/1512.03044\"\u003eEnumeration and investigation of acute 0/1-simplices modulo the action of the hyperoctahedral group\u003c/a\u003e, arXiv preprint arXiv:1512.03044 [math.CO], 2015.",
				"H. Fripertinger, \u003ca href=\"http://www.mathe2.uni-bayreuth.de/frib/codes/tables.html\"\u003eIsometry Classes of Codes\u003c/a\u003e",
				"H. Fripertinger, \u003ca href=\"http://dx.doi.org/10.1023/A:1008248618779\"\u003eEnumeration, construction and random generation of block codes\u003c/a\u003e, Designs, Codes, Crypt., 14 (1998), 213-219.",
				"Petr Lisonek, \u003ca href=\"http://dx.doi.org/10.1016/j.jcta.2006.06.013\"\u003eCombinatorial families enumerated by quasi-polynomials\u003c/a\u003e, Journal of Combinatorial Theory, Series A, Volume 114, Issue 4, May 2007, Pages 619-630.",
				"Thomas Wieder, \u003ca href=\"http://www.math.nthu.edu.tw/~amen/2008/070301.pdf\"\u003eThe number of certain k-combinations of an n-set\u003c/a\u003e, Applied Mathematics Electronic Notes, vol. 8 (2008).",
				"\u003ca href=\"/index/Rec#order_07\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,0,-1,-1,0,2,-1)."
			],
			"formula": [
				"a(n) = floor(n*(2*n^2 + 21*n - 6)/72).",
				"G.f.: (-x^5 + x^3 + x^2)/((1 - x)^2*(1 - x^2)*(1 - x^3)) = 1/((1 - x)^2*(1 - x^2)*(1 - x^3)) - 1/(1 - x)^2.",
				"a(1) = 0, a(2) = 1, a(3) = 3, a(4) = 6, a(5) = 10, a(6) = 16, a(7) = 23, and a(n) = 2*a(n-1) - a(n-3) - a(n-4) + 2*a(n-6) - a(n-7) for n \u003e= 8. [_Harvey P. Dale_, Dec 25 2011]"
			],
			"example": [
				"Let t denote the trivial representation and u_1,u_2,u_3 the three nontrivial irreducible representations of C_2^2 (so the u_i are all equivalent up to automorphisms of C_2^2). Then the a(4) = 6 faithful representations of dimension 4 are:",
				"2t+u_1+u_2",
				"t+2u_1+u_2",
				"t+u_1+u_2+u_3",
				"3u_1+u_2",
				"2u_1+2u_2",
				"2u_1+u_2+u_3",
				"From _John M. Campbell_, Jan 22 2016: (Start)",
				"Letting n=8, there are a([n/2])=a(4)=6 partitions mu such that there exists a Klein four-subgroup G of S_n such that the i^th largest (nontrivial) product of 2-cycles in G consists of mu_i 2-cycles, as indicated below:",
				"{2, 1, 1} \u003c-\u003e {(12)(34), (12), (34), id}",
				"{3, 2, 1} \u003c-\u003e {(12)(34)(56), (34)(56), (12), id}",
				"{2, 2, 2} \u003c-\u003e {(12)(34), (34)(56), (56)(12), id}",
				"{4, 3, 1} \u003c-\u003e {(12)(34)(56)(78), (34)(56)(78), (12), id}",
				"{4, 2, 2} \u003c-\u003e {(12)(34)(56)(78), (56)(78), (12)(34), id}",
				"{3, 3, 2} \u003c-\u003e {(12)(34)(56), (34)(56)(78), (12)(78), id}",
				"(End)"
			],
			"maple": [
				"A034198:=n-\u003efloor(n*(2*n^2+21*n-6)/72); seq(A034198(k), k=1..100); # _Wesley Ivan Hurt_, Oct 29 2013"
			],
			"mathematica": [
				"Table[Floor[n (2n^2+21*n-6)/72],{n,50}] (* _Harvey P. Dale_, Dec 25 2011 *)",
				"LinearRecurrence[ {2,0,-1,-1,0,2,-1},{0,1,3,6,10,16,23},50] (* _Harvey P. Dale_, Dec 25 2011 *)"
			],
			"program": [
				"(MAGMA) [Floor(n*(2*n^2+21*n-6)/72): n in [1..50]]; // _Vincenzo Librandi_, Sep 18 2016"
			],
			"xref": [
				"Cf. A034188.",
				"Column k=2 of both A034356 and A076831 (which are the same except for column k=0)."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_.",
			"ext": [
				"Additional comments from _Max Alekseyev_, Jul 09 2006",
				"Additional comments from _Andrew Rupinski_, Jan 20 2010"
			],
			"references": 7,
			"revision": 57,
			"time": "2019-10-07T20:02:12-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}