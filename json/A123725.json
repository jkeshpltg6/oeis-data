{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A123725",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 123725,
			"data": "1,2,-3,-2,-4,2,3,-2,-5,2,-3,-2,4,2,3,-2,-6,2,-3,-2,-4,2,3,-2,5,2,-3,-2,4,2,3,-2,-7,2,-3,-2,-4,2,3,-2,-5,2,-3,-2,4,2,3,-2,6,2,-3,-2,-4,2,3,-2,5,2,-3,-2,4,2,3,-2,-8,2,-3,-2,-4,2,3,-2,-5,2,-3,-2,4,2,3,-2,-6,2,-3,-2,-4,2,3,-2,5,2,-3,-2,4,2,3,-2,7,2,-3,-2,-4,2",
			"name": "Numerators of fractional partial quotients appearing in a continued fraction for the power series Sum_{n\u003e=0} x^(2^n - 1)/(n+1)^s.",
			"link": [
				"Antti Karttunen, \u003ca href=\"/A123725/b123725.txt\"\u003eTable of n, a(n) for n = 0..65537\u003c/a\u003e"
			],
			"formula": [
				"a(n) = (A007814(n) + 2) * (-1)^A073089(n+1), n\u003e=1, with a(0)=1.",
				"a(n) = A089080(n+1) * (-1)^A073089(n+1) for n\u003e=0."
			],
			"example": [
				"Surprisingly, the following analog of the Riemann zeta function:",
				"Z(x,s) = Sum_{n\u003e=0} x^(2^n-1)/(n+1)^s = 1 + x/2^s + x^3/3^s +x^7/4^s+..",
				"may be expressed by the continued fraction:",
				"Z(x,s) = [1; f(1)^s/x, -f(2)^s/x, -f(3)^s/x,...,f(n)^s*(-1)^e(n)/x,...]",
				"such that the (2^n-1)-th convergent = Sum_{k=0..n} x^(2^k-1)/(k+1)^s,",
				"where f(n) = (b(n)+2)/(b(n)+1)^2 and e(n) = A073089(n+1) for n\u003e=1,",
				"and b(n) = A007814(n) the exponent of highest power of 2 dividing n.",
				"Thus a(n) = (A007814(n) + 2)*(-1)^A073089(n+1) are numerators and",
				"A123726(n) = (A007814(n) + 1)^2 are denominators of partial quotients.",
				"Case s=1.",
				"Sum_{n\u003e=0} x^(2^n-1)/(n+1) = [1; 2/x, -(3/4)/x, -2/x, -(4/9)/x, 2/x,",
				"(3/4)/x, -2/x, -(5/16)/x, 2/x, -(3/4)/x, -2/x, (4/9)/x, 2/x, (3/4)/x,",
				"-2/x, -(6/25)/x, 2/x, -(3/4)/x, -2/x, -(4/9)/x, 2/x, (3/4)/x, -2/x,...].",
				"Note that (2^n-1)-th convergents exactly equal n-th partial sums:",
				"[1;2/x] = 1 + x/2;",
				"[1;2/x,-(3/4)/x,-2/x] = 1 + x/2 + x^3/3;",
				"[1;2/x,-(3/4)/x,-2/x,-(4/9)/x,2/x,(3/4)/x,-2/x] = 1 +x/2 +x^3/3 +x^7/4.",
				"Case s=2.",
				"Sum_{n\u003e=0} x^(2^n-1)/(n+1)^2 = [1; 4/x, -(9/16)/x, -4/x, -(16/81)/x,",
				"4/x, (9/16)/x, -4/x, -(25/256)/x, 4/x, -(9/16)/x, -4/x, (16/81)/x, 4/x,",
				"(9/16)/x, -4/x, -(36/625)/x, 4/x, -(9/16)/x, -4/x, (16/81)/x, 4/x ...].",
				"Note that (2^n-1)-th convergents exactly equal n-th partial sums:",
				"[1;4/x] = 1 + x/4;",
				"[1;4/x,-(9/16)/x,-4/x] = 1 + x/4 + x^3/9;",
				"[1;4/x,-(9/16)/x,-4/x,-(16/81)/x,4/x,(9/16)/x,-4/x]=1+x/4+x^3/9+x^7/16.",
				"Case s=3.",
				"Sum_{n\u003e=0} x^(2^n-1)/(n+1)^3 = [1; 8/x,-(27/64)/x,-8/x,-(64/729)/x,8/x,",
				"(27/64)/x,-8/x,-(125/4096)/x, 8/x,-(27/64)/x,-8/x, (64/729)/x, 8/x,",
				"(27/64)/x,-8/x,-(216/15625)/x, 8/x, -(27/64)/x, -8/x, (64/729)/x ...].",
				"Likewise, the (2^n-1)-th convergents exactly equal n-th partial sums.",
				"It is conjectured that these patterns continue for all s."
			],
			"program": [
				"(PARI) {a(n)=numerator(subst(contfrac(sum(m=0,#binary(n),1/x^(2^m-1)/(m+1)),n+3)[n+1],x,1))}",
				"(PARI)",
				"A007814(n) = valuation(n,2);",
				"A073089(n) = { if(n\u003c=1, return(0)); n-=1; my(v=2^valuation(n, 2)); return((0==bitand(n, v\u003c\u003c1)) != (v%2)); }; \\\\ From A073089",
				"A123725(n) = if(!n,1,(A007814(n) + 2) * (-1)^A073089(n+1)); \\\\ _Antti Karttunen_, Nov 01 2018"
			],
			"xref": [
				"Cf. A123726 (denominators); A007814, A073089, A089080 (unsigned)."
			],
			"keyword": "cofr,frac,sign",
			"offset": "0,2",
			"author": "_Paul D. Hanna_, Oct 12 2006",
			"references": 3,
			"revision": 9,
			"time": "2018-11-01T18:20:53-04:00",
			"created": "2006-12-06T03:00:00-05:00"
		}
	]
}