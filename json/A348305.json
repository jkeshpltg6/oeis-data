{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A348305",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 348305,
			"data": "1,2,4,10,16,39,43,96,142",
			"name": "Number of fusion rings of multiplicity one and rank n.",
			"comment": [
				"The notion of fusion ring was introduced by G. Lusztig (1987). See the modern definition on page 60 of the book \"Tensor Categories\" (2015), in reference below.",
				"It can be seen as a generalization of both finite group (based ring) and its representation (based) ring.",
				"Combinatorially, it is just given by a finite set {1,...,n}, a bijection i-\u003ei^* (called dual map) and fusion coefficients N_{i,j}^k which are nonnegative integers satisfying the following axioms:",
				"- Associativity: Sum_s N_{i,j}^s N_{s,k}^t = Sum_s N_{j,k}^s N_{i,s}^t,",
				"- Neutral: N_{1,i}^j = N_{i,1}^j = delta_{i,j},",
				"- Dual: N_{i^*,k}^{1} = N_{k,i^*}^{1} = delta_{i,k},",
				"- Frobenius reciprocity: N_{i,j}^k = N_{i^*,k}^j = N_{k,j^*}^i.",
				"The rank is just n. The multiplicity is the max of (N_{i,j}^k).",
				"The fusion rings are considered up to equivalence.",
				"There is a distinct fusion ring of multiplicity one and rank n for each finite group of order n (just take the group as finite set, the inverse as dual map and N_{g,h}^k = delta_{gh,k}), so a(n)\u003e=A000001(n). The inequality is strict for n\u003e1.",
				"The above terms of the sequence were computed by J. Slingerland and G. Vercleyen (see link below, slide n°11).",
				"The six first terms were also computed independently by Z. Liu, S. Palcoux and Y. Ren (see reference below, page 1).",
				"The (optimized) code computing these terms may be too long to be put here."
			],
			"reference": [
				"G. Lusztig, Leading coefficients of character values of Hecke algebras, Proc. Symp. in Pure Math., 47, pp. 235-262 (1987)."
			],
			"link": [
				"AnyonWiki, \u003ca href=\"http://www.thphys.nuim.ie/AnyonWiki/index.php/Fusion_ring\"\u003eFusion ring\u003c/a\u003e.",
				"P. Etingof, S. Gelaki, D. Nikshych and V. Ostrik, \u003ca href=\"http://www-math.mit.edu/~etingof/egnobookfinal.pdf\"\u003eTensor Categories\u003c/a\u003e, Mathematical Surveys and Monographs Volume 205 (2015).",
				"Z. Liu, S. Palcoux and Y. Ren, \u003ca href=\"https://arxiv.org/abs/2010.10264\"\u003eClassification of Grothendieck rings of complex fusion categories of multiplicity one up to rank six\u003c/a\u003e, arXiv:2010.10264  [math.CT], 2020-2021.",
				"nLab, \u003ca href=\"https://ncatlab.org/nlab/show/fusion+ring\"\u003efusion ring\u003c/a\u003e.",
				"J. Slingerland and G. Vercleyen \u003ca href=\"https://mathpicture.fas.harvard.edu/files/mathpicture/files/harvard_picture_language_10-2020.pdf\"\u003eExploring small fusion rings and tensor categories\u003c/a\u003e, Harvard Picture Language Seminar, 20th October 2020."
			],
			"example": [
				"For n=1, there is only the trivial fusion ring, so a(1)=1.",
				"For n=2, there are only the fusion ring of the cyclic group C2 and the Yang-Lee fusion ring, so a(2)=2."
			],
			"xref": [
				"Cf. A000001."
			],
			"keyword": "nonn,hard,more",
			"offset": "1,2",
			"author": "_Sébastien Palcoux_, Oct 10 2021",
			"references": 0,
			"revision": 31,
			"time": "2021-10-12T08:02:54-04:00",
			"created": "2021-10-11T18:51:43-04:00"
		}
	]
}