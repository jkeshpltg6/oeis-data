{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A004086",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 4086,
			"data": "0,1,2,3,4,5,6,7,8,9,1,11,21,31,41,51,61,71,81,91,2,12,22,32,42,52,62,72,82,92,3,13,23,33,43,53,63,73,83,93,4,14,24,34,44,54,64,74,84,94,5,15,25,35,45,55,65,75,85,95,6,16,26,36,46,56,66,76,86,96,7,17,27,37,47",
			"name": "Read n backwards (referred to as R(n) in many sequences).",
			"comment": [
				"Also called digit reversal of n.",
				"Leading zeros (after the reversal has taken place) are omitted. - _N. J. A. Sloane_, Jan 23 2017",
				"For n\u003e0: a(a(n)) = n iff n mod 10 != 0. - _Reinhard Zumkeller_, Mar 10 2002"
			],
			"link": [
				"Franklin T. Adams-Watters and Indranil Ghosh, \u003ca href=\"/A004086/b004086.txt\"\u003eTable of n, a(n) for n = 0..50000\u003c/a\u003e (first 1001 terms from Franklin T. Adams-Watters)",
				"Dana G. Korssjoen, Biyao Li, Stefan Steinerberger, Raghavendra Tripathi, and Ruimin Zhang, \u003ca href=\"https://arxiv.org/abs/2012.04625\"\u003eFinding structure in sequences of real numbers via graph theory: a problem list\u003c/a\u003e, arXiv:2012.04625 [math.CO], Dec 08 2020."
			],
			"formula": [
				"a(n) = d(n,0) with d(n,r) = if n=0 then r, otherwise d(floor(n/10), r*10+(n mod 10)). - _Reinhard Zumkeller_, Mar 04 2010",
				"a(10*n+x) = x*10^m + a(n) if 10^(m-1) \u003c= n \u003c 10^m and 0 \u003c= x \u003c= 9. - _Robert Israel_, Jun 11 2015"
			],
			"maple": [
				"read transforms; A004086 := digrev; #cf \"Transforms\" link at bottom of page",
				"A004086:=proc(n) local s,t; if n\u003c10 then n else s:=irem(n,10,'t'); while t\u003e9 do s:=s*10+irem(t,10,'t') od: s*10+t fi end; # _M. F. Hasler_, Jan 29 2012"
			],
			"mathematica": [
				"Table[FromDigits[Reverse[IntegerDigits[n]]], {n, 0, 75}]",
				"IntegerReverse[Range[0,80]](* Requires Mathematica version 10 or later *) (* _Harvey P. Dale_, May 13 2018 *)"
			],
			"program": [
				"(PARI) dig(n) = {local(m=n,r=[]); while(m\u003e0,r=concat(m%10,r);m=floor(m/10));r}",
				"A004086(n) = {local(b,m,r);r=0;b=1;m=dig(n);for(i=1,matsize(m)[2],r=r+b*m[i];b=b*10);r} \\\\ _Michael B. Porter_, Oct 16 2009",
				"(PARI) A004086(n)=fromdigits(Vecrev(digits(n))) \\\\ _M. F. Hasler_, Nov 11 2010, updated May 11 2015, Sep 13 2019",
				"(Haskell) a004086 = read . reverse . show  -- _Reinhard Zumkeller_, Apr 11 2011",
				"(Python)",
				"def A004086(n):",
				"    return int(str(n)[::-1]) # _Chai Wah Wu_, Aug 30 2014",
				"(J) |.\u0026.\": i.@- 1e5 NB. _Stephen Makdisi_, May 14 2018"
			],
			"xref": [
				"Cf. A004185, A004186, A009996, A073138, A188649, A221714.",
				"Cf. A030101, A030102, A030103, A030104, A030105, A030107, A030108."
			],
			"keyword": "nonn,base,nice,look",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Extended by _Ray Chandler_, Dec 30 2004"
			],
			"references": 484,
			"revision": 131,
			"time": "2021-08-03T11:25:45-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}