{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A262181",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 262181,
			"data": "1,2,1,11,1,42,64,202,1,1557,1,5539,32298,30666,1,405200,1,1035642",
			"name": "a(n) = total number of convex equilateral n-gons with corner angles of m*Pi/n (0 \u003c m \u003c= n).",
			"comment": [
				"An n-gon is a polygon with n corners and n sides, each of which is a straight line segment joining two corners. An n-gon or polygon P is said to be a simple polygon (or a Jordan polygon) if the only points of the plane belonging to two polygon edges of P are the polygon vertices of P. Such a polygon has a well-defined interior and exterior. Simple polygons are topologically equivalent to a disk, hence zero angles are not allowed; allowable angles are m*Pi/n (where m and n are integers and 0 \u003c m \u003c= n). An n-gon is convex if it contains all the diagonal segments connecting any pair of its points. A convex polygon is sometimes strictly defined as a polygon with all its interior angles less than Pi. We use the less strict definition where every internal or interior angle is less than or equal to Pi, that is, straight angles are permitted.",
				"Conjecture: There is only one convex equilateral n-gon for prime n."
			],
			"link": [
				"Stuart E Anderson, \u003ca href=\"http://www.squaring.net/polygons/n-gons-cvx.cpp\"\u003eC++ Program\u003c/a\u003e, produces postscript images of convex polygons for n, along with an unsorted list of interior angle multiples m for each polygon. One rotationally invariant representative polygon is produced in postscript.",
				"Stuart E Anderson, \u003ca href=\"http://www.squaring.net/polygons/cvx_3-1_1_1_.pdf\"\u003e for n=3, 1 solution, the equilateral triangle\u003c/a\u003e",
				"Stuart E Anderson, \u003ca href=\"http://www.squaring.net/polygons/o4cvx.pdf\"\u003e for n=4, 2 solutions\u003c/a\u003e",
				"Stuart E Anderson, \u003ca href=\"http://www.squaring.net/polygons/cvx_5-3_3_3_3_.pdf\"\u003e for n=5, 1 solution\u003c/a\u003e",
				"Stuart E Anderson, \u003ca href=\"http://www.squaring.net/polygons/o6cvx.pdf\"\u003e for n=6, 11 solutions\u003c/a\u003e",
				"Stuart E Anderson, \u003ca href=\"http://www.squaring.net/polygons/o7cvx.pdf\"\u003e for n=7, 1 solution\u003c/a\u003e",
				"Stuart E Anderson, \u003ca href=\"http://www.squaring.net/polygons/o8convex.pdf\"\u003e for n=8, 42 solutions\u003c/a\u003e",
				"Gilbert Labelle and Annie Lacasse, \u003ca href=\"https://www.dmtcs.org/dmtcs-ojs/index.php/proceedings/article/viewArticle/dmAO0153.html\"\u003eClosed paths whose steps are roots of unity\u003c/a\u003e, in FPSAC 2011, Reykjavik, Iceland DMTCS proc. AO, 2011, 599-610."
			],
			"formula": [
				"a(n) = A292355(n) for n prime or twice prime. - _Andrew Howroyd_, Sep 14 2017",
				"a(n) = -(1+(-1)^n)/2 + (1/(2*n))*(A321415(n) - binomial(3*n-1, n) + Sum_{d|n} phi(n/d) * binomial(3*d-1, d)). - _Andrew Howroyd_, Nov 09 2018"
			],
			"example": [
				"For n = 3 there is one convex n-gon, the equilateral triangle, with m angle factors (3 3 3); so a(3) = 1.",
				"For n = 4 there are two convex n-gons, the square and a rhombus, with respective m angle factors (2 2 2 2) and (1 3 1 3); so a(4) = 2.",
				"For n = 5, there is the regular pentagon, m factors (3 3 3 3 3); so a(5) = 1.",
				"For n = 6 there are 11 convex n-gons; here are the m factors:(1 5 6 1 5 6), (1 6 5 1 6 5), (2 4 6 2 4 6), (2 5 5 2 5 5), (2 6 2 6 2 6), (2 6 4 2 6 4), (3 3 6 3 3 6), (3 4 5 3 4 5), (3 5 3 5 3 5), (3 5 4 3 5 4), (4 4 4 4 4 4); so a(6) = 11."
			],
			"xref": [
				"A262244 for concave polygons with corner angles of m*Pi/n (0 \u003c m \u003c 2n), where m and n are integers.",
				"Cf. A103314, A292355, A321415."
			],
			"keyword": "nonn,hard,more",
			"offset": "3,2",
			"author": "_Stuart E Anderson_, Sep 14 2015",
			"ext": [
				"a(10) corrected and a(12)-a(17) from _Andrew Howroyd_, Sep 14 2017",
				"a(18)-a(20) from _Andrew Howroyd_, Nov 09 2018"
			],
			"references": 4,
			"revision": 75,
			"time": "2018-11-11T00:42:51-05:00",
			"created": "2015-11-28T13:58:40-05:00"
		}
	]
}