{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A260825",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 260825,
			"data": "2,2,8,8,5,20,9,16,2,98,18,18,10,90,32,32,20,80,36,64,50,50,8,392,25,144,17,272,72,72,52,117,2,3362,45,180,98,98,81,144,20,605,64,225,40,360,18,882,128,128,26,650,80,320,162,162,49,576,5,7220,144,256,200,200,37,1332,32,1568,13,4212,98,578",
			"name": "List of pairs of positive integers [b,c], in increasing order of b*c, whose sum and product are both squares.",
			"comment": [
				"Pairs have been put in increasing order of b*c, thus [9, 16] comes after [2, 98] (since 9*16 \u003c 2*98).",
				"From _Robert Israel_, Mar 16 2018: (Start)",
				"b and c are listed with b \u003c= c.",
				"First b*c that has more than one pair is 120^2 corresponding to [b,c] = [64, 225] and [40, 360].",
				"For a given b*c, pairs are placed in increasing order of c. (End)",
				"From _Robert G. Wilson v_, Aug 03 2015: (Start)",
				"The squares (b*c) are 4, 64, 100, 144, 196, 324, 900, 1024, 1600, 2304, 2500, 3136, 3600, 4624, 5184, ..., .",
				"The square roots of (b*c) are 2, 8, 10, 12, 14, 18, 30, 32, 40, 48, 50, 56, 60, 68, 72, 78, 82, 90, 98, ..., .",
				"The sums (b+c) are 4, 16, 25, 25, 100, 36, 100, 64, 100, 100, 100, 400, 169, 289, 144, 169, 3364, 225, ..., . (End)"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A260825/b260825.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"[5,20] is such a pair, since 5 + 20 = 25 = 5^2 and 5*20 = 100 = 10^2."
			],
			"maple": [
				"count:= 0: Res:= NULL;",
				"for t from 1 while count \u003c 200 do",
				"  tres:= NULL;",
				"  Q:= select(q -\u003e q^2 \u003c= 4*t^2, numtheory:-divisors(4*t^2));",
				"  nt:= 0:",
				"  for q in Q do",
				"    r:= 4*t^2/q;",
				"    if (r-q) mod 2 \u003c\u003e 0 then next fi;",
				"    s2:= (q+r)/2;",
				"    if not issqr(s2) then next fi;",
				"    s:= sqrt(s2);",
				"    d:= (r-q)/2;",
				"    b:= (s2+d)/2;",
				"    c:= (s2-d)/2;",
				"    count:= count+2;",
				"    tres:= tres,[c,b];",
				"    nt:= nt+1;",
				"  od;",
				"  if nt =1 then tres:= [tres]",
				"  else  tres:= sort([tres],(a,b) -\u003e evalb(a[2] \u003c b[2]));",
				"  fi;",
				"  Res:= Res, op(map(op,tres));",
				"od:",
				"Res; # _Robert Israel_, Mar 16 2018"
			],
			"mathematica": [
				"r = Flatten[ Union@ Table[ If[ IntegerQ[ Sqrt[b + c]] \u0026\u0026 IntegerQ[ Sqrt[b*c]], {b, c}, Sequence @@ {}], {b, 10000}, {c, b, 10000}], 1]; Take[ Sort[r, #1[[1]] #1[[2]] \u003c #2[[1]] #2[[2]] \u0026], 36] // Flatten (* _Robert G. Wilson v_, Aug 03 2015 *)"
			],
			"program": [
				"(PARI) lista(nn) = {for (n=1, nn, sq = n^2; fordiv(sq, d, if ((d \u003c= n) \u0026\u0026 issquare(d + sq/d), print1(d, \", \", sq/d, \", \"));););} \\\\ _Michel Marcus_, Aug 03 2015"
			],
			"xref": [
				"Cf. A001105 (when b=c)."
			],
			"keyword": "nonn,tabf",
			"offset": "1,1",
			"author": "_Marco Ripà_, Jul 31 2015",
			"ext": [
				"More terms from _Robert G. Wilson v_, Aug 03 2015"
			],
			"references": 1,
			"revision": 45,
			"time": "2019-01-04T18:28:27-05:00",
			"created": "2015-08-06T07:30:20-04:00"
		}
	]
}