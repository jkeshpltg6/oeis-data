{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A063882",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 63882,
			"data": "1,1,1,1,2,3,4,5,5,6,6,7,8,8,9,9,10,11,11,11,12,12,13,14,14,15,15,16,17,17,17,18,18,19,20,20,21,21,22,22,22,23,23,24,25,25,26,26,27,27,28,29,29,29,30,30,31,32,32,33,33,34,34,34,35,35,36,37,37,38,38,39,39,40",
			"name": "a(n) = a(n - a(n - 1)) + a(n - a(n - 4)), with a(1) = ... = a(4) = 1.",
			"comment": [
				"A captivating recursive function. A meta-Fibonacci recursion.",
				"This has been completely analyzed by Balamohan et al. They prove that the sequence a(n) is monotonic, with successive terms increasing by 0 or 1, so the sequence hits every positive integer.",
				"They demonstrate certain special structural properties and periodicities of the associated frequency sequence (the number of times a(n) hits each positive integer) that make possible an iterative computation of a(n) for any value of n.",
				"Further, they derive a natural partition of the a-sequence into blocks of consecutive terms (\"generations\") with the property that terms in one block determine the terms in the next.",
				"a(A202014(n)) = n and a(m) \u003c n for m \u003c A202014(n). [_Reinhard Zumkeller_, Dec 08 2011]"
			],
			"link": [
				"T. D. Noe and N. J. A. Sloane, \u003ca href=\"/A063882/b063882.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Altug Alkan, \u003ca href=\"https://doi.org//10.1155/2018/8517125\"\u003eOn a Generalization of Hofstadter's Q-Sequence: A Family of Chaotic Generational Structures\u003c/a\u003e, Complexity (2018) Article ID 8517125.",
				"B. Balamohan, A. Kuznetsov and S. Tanny, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL10/Tanny/tanny3.html\"\u003eOn the behavior of a variant of Hofstadter's Q-sequence\u003c/a\u003e, J. Integer Sequences, Vol. 10 (2007), #07.7.1.",
				"A. Isgur, R. Lech, S. Moore, S. Tanny, Y. Verberne, and Y. Zhang, \u003ca href=\"http://dx.doi.org/10.1137/15M1040505\"\u003eConstructing New Families of Nested Recursions with Slow Solutions\u003c/a\u003e, SIAM J. Discrete Math., 30(2), 2016, 1128-1147. (20 pages); DOI:10.1137/15M1040505",
				"Kellie O'Connor Gutman, \u003ca href=\"http://dx.doi.org/10.1007/BF03026855\"\u003eV(n) = V(n - V(n - 1)) + V(n - V(n - 4))\u003c/a\u003e, The Mathematical Intelligencer, Volume 23, Number 3, Summer 2001, page 50.",
				"\u003ca href=\"/index/Ho#Hofstadter\"\u003eIndex entries for Hofstadter-type sequences\u003c/a\u003e"
			],
			"formula": [
				"n/2 \u003c a(n) \u003c= n/2 - log_2 (n) - 1 for all n \u003e 6 [Balamohan et al.]"
			],
			"maple": [
				"a := proc(n) option remember; if n\u003c=4 then 1 else if n \u003e a(n-1) and n \u003e a(n-4) then RETURN(a(n-a(n-1))+a(n-a(n-4))); else ERROR(\" died at n= \", n); fi; fi; end;"
			],
			"mathematica": [
				"a[1] = a[2] = a[3] = a[4] = 1; a[n_] := a[n] = a[n-a[n-1]] + a[n-a[n-4]]"
			],
			"program": [
				"(Haskell)",
				"a063882 n = a063882_list !! (n-1)",
				"a063882_list = 1 : 1 : 1 : 1 : zipWith (+)",
				"   (map a063882 $ zipWith (-) [5..] a063882_list)",
				"   (map a063882 $ zipWith (-) [5..] $ drop 3 a063882_list)",
				"-- _Reinhard Zumkeller_, Dec 08 2011"
			],
			"xref": [
				"Cf. A132157. For partial sums see A129632.",
				"A136036(n) = a(n+1) - a(n).",
				"Cf. A063892, A087777.",
				"Cf. A132174, A132175, A132176, A132177.",
				"Cf. A202016 (occur only once)."
			],
			"keyword": "nice,nonn",
			"offset": "1,5",
			"author": "Theodor Schlickmann (Theodor.Schlickmann(AT)cec.eu.int), Aug 28 2001",
			"ext": [
				"Edited by _N. J. A. Sloane_, Nov 06 2007"
			],
			"references": 31,
			"revision": 32,
			"time": "2018-09-26T22:18:15-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}