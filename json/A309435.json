{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A309435",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 309435,
			"data": "1,2,3,4,5,2,3,7,8,9,5,2,11,3,4,13,14,15,16,17,18,19,4,5,3,7,2,11,23,24,25,26,27,28,29,30,31,32,11,3,34,35,36,37,38,39,40,41,42,43,44,9,5,46,47,48,49,50,51,4,13,53,54,55,7,8,57,58,59,60",
			"name": "Iteratively replace the product of two sequentially chosen consecutive integers with those integers.",
			"formula": [
				"To generate the sequence, start with the integers, A_0={1,2,3,4,5,...}. To generate A_{n+1} calculate x = A_n(n) * A_n(n+1). Replace the next instance of x in A_n (after A_n(n+1)) with A_n(n), A_n(n+1). The limit of this process gives the sequence."
			],
			"example": [
				"A_0 = {1,2,3,4,5,...}",
				"A_0(0) * A_0(1) = 1 * 2 = 2, which is not found after A_0(1), so A_1 = A_0.",
				"A_1(1) * A_1(2) = 2 * 3 = 6, which *is* found after A_1(2), so A_2 = {1,2,3,4,5,2,3,7,8,...}",
				"A_2(2) * A_2(3) = 3 * 4 = 12, A_3 = {1,2,3,4,5,2,3,7,8,9,10,11,3,4,13,...}",
				"A_3(3) * A_3(4) = 4 * 5 = 20, A_4 = {1,2,3,4,5,2,3,7,8,9,10,11,3,4,13,...,19,4,5,21,...}",
				"A_4(4) * A_4(5) = 5 * 2 = 10, A_5 = {1,2,3,4,5,2,3,7,8,9,5,2,11,3,4,13,...,19,4,5,21,...}"
			],
			"mathematica": [
				"T = Range[100]; Do[p = T[[i]] T[[i + 1]]; Do[If[T[[j]] == p, T = Join[ T[[;; j-1]], {T[[i]], T[[i+1]]}, T[[j+1 ;;]]]; Break[]], {j, i+2, Length@ T}], {i, Length@T}]; T (* _Giovanni Resta_, Sep 20 2019 *)"
			],
			"program": [
				"(Kotlin)",
				"fun generate(len: Int): List\u003cInt\u003e {",
				"    fun gen_inner(len: Int, level: Int): List\u003cInt\u003e {",
				"        if (level \u003c 1) return (1..len).toList()",
				"        val prev = gen_inner(len, level - 1)",
				"        if (level == len) return prev.take(len)",
				"        val (a,b) = prev[level - 1] to prev[level]",
				"        return if (prev.drop(level + 1).contains(a*b)) {",
				"            prev.indexOfFirst { it == a*b }.let { idx -\u003e",
				"                prev.take(idx) + a + b + prev.drop(idx + 1)",
				"            }",
				"        } else prev",
				"    }",
				"    return gen_inner(len,len)",
				"}",
				"(PARI) a = vector(92, k, k); for (n=1, #a, print1 (a[n] \", \"); s=a[n]*a[n+1]; for (k=n+2, #a, if (a[k]==s, a=concat([a[1..k-1], a[n..n+1], a[k+1..#a]]); break))) \\\\ _Rémy Sigrist_, Aug 03 2019"
			],
			"xref": [
				"This sequence is similar to A309503 except it uses multiplication instead of addition."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Matthew Malone_, Aug 02 2019",
			"references": 1,
			"revision": 18,
			"time": "2019-09-20T03:27:25-04:00",
			"created": "2019-09-20T03:27:25-04:00"
		}
	]
}