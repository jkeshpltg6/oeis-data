{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A110259",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 110259,
			"data": "3,28,704,768,311296,1507328,3145728,130023424,7516192768,12884901888,2954937499648,12919261626368,52776558133248,774056185954304,66428094503714816,31525197391593472,308982963234634989568",
			"name": "Numerators in the coefficients that form the even-indexed partial quotients of the continued fraction representation of the inverse tangent of 1/x.",
			"comment": [
				"Lim_{n-\u003einfinity} a(n)/A110260(n) = lim_{n-\u003einfinity} A110255(2*n)/A110256(2*n) = Pi.",
				"The continued fraction expansion arctan(z) = z/(1 + z^2/(3 + 4*z^2/(5 + 9*z^2/(7 + ...)))) is due to Lambert - see Roegel, Section 1.2.1. - _Peter Bala_, Dec 02 2021"
			],
			"link": [
				"Paul D. Hanna, \u003ca href=\"/A110259/b110259.txt\"\u003eTable of n, a(n) for n = 1..200\u003c/a\u003e",
				"Denis Roegel, \u003ca href=\"https://hal.archives-ouvertes.fr/hal-02984214/document\"\u003eLambert’s proof of the irrationality of Pi: Context and translation\u003c/a\u003e, [Research Report] LORIA, 2020, hal-02984214."
			],
			"formula": [
				"a(n) = A110255(2*n)."
			],
			"example": [
				"arctan(1/x) = 1/x - 1/(3*x^3) + 1/(5*x^5) - 1/(7*x^7) +- ...",
				"arctan(1/x) = [0; x, 3*x, (5/4)*x, (28/9)*x, (81/64)*x, (704/225)*x, (325/256)*x, (768/245)*x, (20825/16384)*x, (311296/99225)*x, (83349/65536)*x, (1507328/480249)*x, (1334025/1048576)*x, (3145728/1002001)*x, ...]",
				"arctan(1/x) = 1/(x + 1/(3*x + 1/(5/4*x + 1/(28/9*x + 1/(81/64*x + ...))))).",
				"The coefficients of x in the even-indexed partial quotients converge to Pi: {3, 28/9, 704/225, 768/245, 311296/99225, ...}.",
				"The coefficients of x in the odd-indexed partial quotients converge to 4/Pi: {1, 5/4, 81/64, 325/256, 20825/16384, ...}.",
				"From _Peter Bala_, Dec 02 2021: (Start)",
				"Making use of the expansion 2*arcsin(sqrt(x)/2)^2 = Sum_{n \u003e= 1} x^n/ (n^2*binomial(2*n,n)) we calculate",
				"3 + Pi = Sum_{n \u003e= 1} (2^n)*n/binomial(2*n,n);",
				"28 + 9*Pi = Sum_{n \u003e= 3} (2^n)*n*(n-1)*(n-2)/binomial(2*n,n);",
				"704 + 225*Pi = Sum_{n \u003e= 5} (2^n)*n*(n-1)*...*(n-4)/binomial(2*n,n);",
				"45*(768 + 245*Pi) = Sum_{n \u003e= 7} (2^n)*n*(n-1)*...*(n-6)/binomial(2*n,n);",
				"9*(311296 + 99225*Pi) = Sum_{n \u003e= 9} (2^n)*n*(n-1)*...*(n-8)/ binomial(2*n,n).",
				"It appears that Sum_{n \u003e= 2*k+1} (2^n)*n*(n-1)*...*(n-2*k)/binomial(2*n,n) = N(2*k) + D(2*k)*Pi, where the ratios N(2*k)/D(2*k) are equal to the even-indexed partial quotients of Lambert's continued fraction representation of the inverse tangent of 1/x. (End)"
			],
			"program": [
				"(PARI) {a(n)=numerator(subst((contfrac( sum(k=0,2*n+2,(-1)^k/x^(2*k+1)/(2*k+1)),2*n+2))[2*n+1],x,1))}"
			],
			"xref": [
				"Cf. A110260 (denominators), A110255/A110256 (continued fraction), A110257/A110258."
			],
			"keyword": "nonn,frac",
			"offset": "1,1",
			"author": "_Paul D. Hanna_, Jul 18 2005",
			"references": 7,
			"revision": 28,
			"time": "2021-12-12T20:36:40-05:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}