{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A345978",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 345978,
			"data": "0,-1,-1,0,1,1,0,-1,-2,-2,-2,-1,0,1,2,2,2,1,0,-1,-2,-3,-3,-3,-3,-2,-1,0,1,2,3,3,3,3,2,1,0,-1,-2,-3,-4,-4,-4,-4,-4,-3,-2,-1,0,1,2,3,4,4,4,4,4,3,2,1,0,-1,-2,-3,-4,-5,-5,-5,-5,-5,-5,-4,-3,-2,-1,0,1,2",
			"name": "Third coordinate of the points of a counterclockwise spiral on an hexagonal grid in a symmetric redundant hexagonal coordinate system.",
			"comment": [
				"This is a negated version of A307013 with the advantage of symmetry, i.e., A307011(n) + A307012(n) + a(n) = 0. The mutual angles of the 3 coordinate axes then are 120 or 240 degrees.",
				"From _Peter Munn_, Jul 18 2021: (Start)",
				"The coordinate system can be described using 3 axes that pass through spiral point 0 and one of points 1, 2 or 3. Along each axis, one of the coordinates is 0. a(n) is the signed distance from spiral point n to the axis that passes through point 3. The distance is measured along either of the lines through point n that are parallel to one of the other 2 axes and the sign is such that point 1 has negative distance.",
				"The coordinates may be used in 2 ways. Firstly, any 2 of the 3 coordinates can be paired as oblique coordinates, which entails mapping each coordinate to a vector that is parallel to the line along which the other coordinate is 0 (described further in A307012). Alternatively, each of the 3 coordinates is mapped to a vector perpendicular to the line along which the coordinate is 0, then the sum of the vectors is divided by the square root of 3.",
				"This coordinate system has been used for more than half a century. See the extract from Moffatt, Pearsall and Wulff included in the linked Princeton MAE page (which refers to a 4th coordinate, making it a 3D system). \"Cube coordinates\" appears to be a currently popular term for the system in some information technology communities. This refers to the useful isometric view of the cubic cells from a 3 dimensional lattice that are indexed by 3 coordinates that sum to zero.",
				"(End)"
			],
			"reference": [
				"William G Moffatt, George W Pearsall and John Wulff, The Structure and Properties of Materials Volume I: Structure, Wiley, 1964."
			],
			"link": [
				"Margherita Barile, \u003ca href=\"https://mathworld.wolfram.com/ObliqueCoordinates.html\"\u003eOblique Coordinates\u003c/a\u003e, entry in Eric Weisstein's World of Mathematics.",
				"HandWiki, \u003ca href=\"https://handwiki.org/wiki/Hexagonal_lattice\"\u003eHexagonal Lattice\u003c/a\u003e.",
				"Princeton University Mechanical \u0026 Aerospace Engineering, The Structure of Solids, \u003ca href=\"https://www.princeton.edu/~maelabs/mae324/03/03mae_21.htm\"\u003eThe Hexagonal Lattice\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.m.wikipedia.org/wiki/Signed_distance_function\"\u003eSigned distance function\u003c/a\u003e."
			],
			"formula": [
				"a(n) = -A307013(n) = -(A307011(n) + A307012(n))."
			],
			"xref": [
				"Cf. A307011, A307012, A307013."
			],
			"keyword": "sign",
			"offset": "0,9",
			"author": "_Hugo Pfoertner_, Jul 15 2021",
			"ext": [
				"Name revised by _Peter Munn_, Jul 22 2021"
			],
			"references": 3,
			"revision": 14,
			"time": "2021-07-25T02:15:49-04:00",
			"created": "2021-07-16T02:03:57-04:00"
		}
	]
}