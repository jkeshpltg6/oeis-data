{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A257474",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 257474,
			"data": "1,1,2,1,2,2,2,2,2,2,2,2,2,2,3,2,3,2,3,3,3,3,5,3,3,3,5,3,3,3,5,3,4,3,3,3,6,5,1,2,5,4,2,1,2,3,4,3,4,5,3,3,3,3,3,2,2,2,4,3,3,2,3,3,1,3,4,4,5,4,4,3,4,3,3,1,5,3,5,3,2,1,3,3,3,1,2,2,4,2,4,4,5,3,4,4,5,3,3,2",
			"name": "Number of unordered ways to write n = a + b, where a and b are distinct elements of the set {floor(x/3): 3*x-1 and 3*x+1 are twin prime}, one of a and b is even, and one of a and b has the form p-1 or p-2 with p prime.",
			"comment": [
				"Conjecture: a(n) \u003e 0 for all n \u003e 0, and a(n) = 1 only for n = 1, 2, 4, 39, 44, 65, 76, 82, 86, 108, 110, 123, 154, 175, 178, 196, 205, 221, 284, 308, 621, 735, 4655.",
				"This is much stronger than the Twin Prime Conjecture. Note that a(n) \u003c= A257317(n) \u003c= A256707(n). We have verified that a(n) \u003e 0 for all n = 1..10^7."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A257474/b257474.txt\"\u003eTable of n, a(n) for n = 1..20000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1504.01608\"\u003eNatural numbers represented by floor(x^2/a)+floor(y^2/b)+floor(z^2/c)\u003c/a\u003e, arXiv:1504.01608 [math.NT], 2015."
			],
			"example": [
				"a(205) = 1 since 205 = 25 + 180 = floor(76/3) + floor(540/3) with 180 even and 180 + 1 prime, and {3*76-1,3*76+1} = {227,229} and {3*540-1,3*540+1} = {1619,1621} twin prime pairs.",
				"a(308) = 1 since 308 = 128 + 180 = floor(384/3) + floor(540/3) with 180 + 1 prime, and {3*128-1,3*128+1} = {1151,1153} and {3*540-1,3*540+1} = {1619,1621} twin prime pairs.",
				"a(621) = 1 since 621 = 310 + 311 = floor(930/3) + floor(934/3) with 310 even and 310 + 1 prime, {3*930-1,3*930+1} ={2789,2791} and {3*934-1,3*934+1} = {2801,2803} twin prime pairs.",
				"a(735) = 1 since 735 = 311 + 424 = floor(934/3) + floor(1274/3) with 424 even, 311 + 2 = 313 prime, and {3*934-1,3*934+1} = {2801,2803} and {3*1274-1,3*1274+1} = {3821,3823} twin prime pairs.",
				"a(4655) = 1 since 4655 = 15 + 4640 = floor(46/3) + floor(13920/3) with 4640 even, 15 + 2 prime, and {3*46-1,3*46+1} = {137,139} and {3*13920-1,3*13920+1} = {41759,41761} twin prime pairs."
			],
			"mathematica": [
				"TQ[n_]:=PrimeQ[3n-1]\u0026\u0026PrimeQ[3n+1]",
				"PQ[n_]:=TQ[3*n]||TQ[3*n+1]||TQ[3n+2]",
				"RQ[n_]:=PrimeQ[n+1]||PrimeQ[n+2]",
				"Do[r=0;Do[If[Mod[x(n-x),2]==0\u0026\u0026(RQ[x]||RQ[n-x])\u0026\u0026PQ[x]\u0026\u0026PQ[n-x],r=r+1],{x,0,(n-1)/2}];",
				"Print[n,\" \",r];Continue,{n,1,100}]"
			],
			"xref": [
				"Cf. A000040, A014574, A256707, A257121, A257317."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Zhi-Wei Sun_, Apr 25 2015",
			"references": 2,
			"revision": 13,
			"time": "2015-04-26T00:18:06-04:00",
			"created": "2015-04-26T00:18:06-04:00"
		}
	]
}