{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A185004",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 185004,
			"data": "7,31,43,67,97,103,151,163,181,223,229,271,331,337,367,373,409,433,487,499,571,577,601,607,631,643,709,727,751,769,823,853,883,937,991,1009,1021,1033,1051,1063,1087,1117,1123,1231,1291,1297,1303",
			"name": "Ramanujan modulo primes R_(3,1)(n): a(n) is the smallest number such that if x \u003e= a(n), then pi_(3,1)(x) - pi_(3,1)(x/2) \u003e= n, where pi_(3,1)(x) is the number of primes==1 (mod 3) \u003c= x.",
			"comment": [
				"All terms are primes==1 (mod 3).",
				"A modular generalization of Ramanujan numbers, see Section 6 of the Shevelev-Greathouse-Moses paper.",
				"We conjecture that for all n \u003e= 1 a(n) \u003c= A104272(3*n). This conjecture is based on observation that, if interval (x/2, x] contains \u003e= 3*n primes, then at least n of them are of the form 3*k+1.",
				"The function pi_(3,1)(n) starts 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3,... with records occurring as specified in A123365/A002476. - _R. J. Mathar_, Jan 10 2013"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A185004/b185004.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Vladimir Shevelev, Charles R. Greathouse IV, Peter J. C. Moses, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL16/Moses/moses1.html\"\u003eOn intervals (kn, (k+1)n) containing a prime for all n\u003e1\u003c/a\u003e, Journal of Integer Sequences, Vol. 16 (2013), Article 13.7.3. \u003ca href=\"http://arxiv.org/abs/1212.2785\"\u003earXiv:1212.2785\u003c/a\u003e"
			],
			"formula": [
				"lim(a(n)/prime(4*n)) = 1 as n tends to infinity."
			],
			"maple": [
				"pimod := proc(m,n,x)",
				"    option remember;",
				"    a := 0 ;",
				"    for k from n to x by m do",
				"        if isprime(k) then",
				"            a := a+1 ;",
				"        end if;",
				"    end do:",
				"    a ;",
				"end proc:",
				"a := [seq(0,n=1..100)] ;",
				"for x from 1 do",
				"    pdiff := pimod(3,1,x)-pimod(3,1,x/2) ;",
				"    if pdiff+1 \u003c= nops(a) then",
				"        v := x+1 ;",
				"        n := pdiff+1 ;",
				"        if n\u003cv then",
				"            a := subsop(n=v,a) ;",
				"            print(a) ;",
				"        end if;",
				"    end if;",
				"end do: # _R. J. Mathar_, Jan 10 2013"
			],
			"mathematica": [
				"max = 100; pimod[m_, n_, x_] := pimod[m, n, x] = Module[{a = 0}, For[k = n, k \u003c= x, k = k + m, If[PrimeQ[k], a = a + 1]]; a]; a[_] = 0; For[x = 1, x \u003c= max^2, x++, pdiff = pimod[3, 1, x] - pimod[3, 1, x/2]; If[ pdiff + 1 \u003c= max, v = x + 1; n = pdiff + 1; If[ n \u003c v , a[n] = v ] ] ]; Table[a[n], {n, 1, max}] (* _Jean-François Alcover_, Jan 28 2013, translated and adapted from _R. J. Mathar_'s Maple program *)"
			],
			"xref": [
				"Cf. A104272, A185005, A185006, A185007."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Vladimir Shevelev_ and _Peter J. C. Moses_, Dec 18 2012",
			"references": 5,
			"revision": 44,
			"time": "2017-06-06T23:28:32-04:00",
			"created": "2013-01-15T19:13:56-05:00"
		}
	]
}