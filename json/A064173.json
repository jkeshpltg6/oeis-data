{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A064173",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 64173,
			"data": "0,1,1,2,3,5,6,10,13,19,25,35,45,62,80,106,136,178,225,291,366,466,583,735,912,1140,1407,1743,2140,2634,3214,3932,4776,5807,7022,8495,10225,12313,14762,17696,21136,25236,30030,35722,42367,50216,59368,70138,82665",
			"name": "Number of partitions of n with positive rank.",
			"comment": [
				"The rank of a partition is the largest summand minus the number of summands.",
				"Also number of partitions of n with negative rank. - _Omar E. Pol_, Mar 05 2012",
				"Column 1 of A208478. - _Omar E. Pol_, Mar 11 2012",
				"Number of partitions p of n such that max(max(p), number of parts of p) is not a part of p. - _Clark Kimberling_, Feb 28 2014",
				"The sequence enumerates the semigroup of partitions of positive rank for each number n. The semigroup is a subsemigroup of the monoid of partitions of nonnegative rank under the binary operation \"*\": Let A be the positive rank partition (a1,...,ak) where ak \u003e k, and let B=(b1,...bj) with bj \u003e j. Then let A*B be the partition (a1b1,...,a1bj,...,akb1,...,akbj), which has akbj \u003e kj, thus having positive rank. For example, the partition (2,3,4) of 9 has rank 1, and its product with itself is (4,6,6,8,8,9,12,12,16) of 81, which has rank 7. A similar situation holds for partitions of negative rank--they are a subsemigroup of the monoid of nonpositive rank partitions. - _Richard Locke Peterson_, Jul 15 2018"
			],
			"reference": [
				"F. J. Dyson, Some guesses in the theory of partitions, Eureka (Cambridge) 8 (1944), 10-15."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A064173/b064173.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"FindStat, \u003ca href=\"http://www.findstat.org/StatisticsDatabase/St000145\"\u003eSt000145: The Dyson rank of a partition\u003c/a\u003e",
				"Mircea Merca, \u003ca href=\"https://arxiv.org/abs/2006.07705\"\u003eRank partition functions and truncated theta identities\u003c/a\u003e, arXiv:2006.07705 [math.CO], 2020."
			],
			"formula": [
				"a(n) = (A000041(n) - A047993(n))/2.",
				"a(n) = p(n-2) - p(n-7) + p(n-15) - ... - (-1)^k*p(n-(3*k^2+k)/2) + ..., where p() is A000041(). - _Vladeta Jovovic_, Aug 04 2004",
				"G.f.: Product_{k\u003e=1} (1/(1-q^k)) * Sum_{k\u003e=1} ( (-1)^k * (-q^(3*k^2/2+k/2))) (conjectured). - _Thomas Baruchel_, May 12 2018"
			],
			"example": [
				"a(20) = p(18) - p(13) + p(5) = 385 - 101 + 7 = 291.",
				"From _Gus Wiseman_, Feb 09 2021: (Start)",
				"The a(2) = 1 through a(9) = 13 partitions of positive rank:",
				"  (2)  (3)  (4)   (5)   (6)    (7)    (8)     (9)",
				"            (31)  (32)  (33)   (43)   (44)    (54)",
				"                  (41)  (42)   (52)   (53)    (63)",
				"                        (51)   (61)   (62)    (72)",
				"                        (411)  (421)  (71)    (81)",
				"                               (511)  (422)   (432)",
				"                                      (431)   (441)",
				"                                      (521)   (522)",
				"                                      (611)   (531)",
				"                                      (5111)  (621)",
				"                                              (711)",
				"                                              (5211)",
				"                                              (6111)",
				"(End)"
			],
			"maple": [
				"with(combinat): for n from 1 to 30 do P:=partition(n): c:=0: for j from 1 to nops(P) do if P[j][nops(P[j])]\u003enops(P[j]) then c:=c+1 else c:=c fi od: a[n]:=c: od: seq(a[n],n=1..30); # _Emeric Deutsch_, Dec 11 2004"
			],
			"mathematica": [
				"Table[Count[IntegerPartitions[n], q_ /; First[q] \u003e Length[q]], {n, 24}] (* _Clark Kimberling_, Feb 12 2014 *)",
				"Table[Count[IntegerPartitions[n], p_ /; ! MemberQ[p, Max[Max[p], Length[p]]]], {n, 20}] (* _Clark Kimberling_, Feb 28 2014 *)",
				"P = PartitionsP;",
				"a[n_] := (P[n] - Sum[-(-1)^k (P[n - (3k^2 - k)/2] - P[n - (3k^2 + k)/2]), {k, 1, Floor[(1 + Sqrt[1 + 24n])/6]}])/2;",
				"a /@ Range[48] (* _Jean-François Alcover_, Jan 11 2020, after _Wouter Meeussen_ in A047993 *)"
			],
			"xref": [
				"Note: A-numbers of ranking sequences are in parentheses below.",
				"The negative-rank version is also A064173 (A340788).",
				"The case of odd positive rank is A101707 (A340604).",
				"The case of even positive rank is A101708 (A340605).",
				"These partitions are ranked by (A340787).",
				"A063995/A105806 count partitions by rank.",
				"A072233 counts partitions by sum and length.",
				"A168659 counts partitions whose length is a multiple of the greatest part.",
				"A200750 counts partitions whose length and greatest part are coprime.",
				"- Rank -",
				"A064174 counts partitions of nonnegative/nonpositive rank (A324562/A324521).",
				"A101198 counts partitions of rank 1 (A325233).",
				"A257541 gives the rank of the partition with Heinz number n.",
				"A340601 counts partitions of even rank (A340602).",
				"A340692 counts partitions of odd rank (A340603).",
				"- Balance -",
				"A047993 counts balanced partitions (A106529).",
				"A340599 counts alt-balanced factorizations.",
				"A340653 counts balanced factorizations.",
				"Cf. A003114, A006141, A039900, A096401, A117193, A117409, A143773, A324516, A324518, A324520."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Vladeta Jovovic_, Sep 19 2001",
			"references": 28,
			"revision": 72,
			"time": "2021-02-11T22:59:13-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}