{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A297440",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 297440,
			"data": "1,2,3,4,5,6,8,7,10,11,13,9,16,15,20,26,12,18,19,34,33,40,14,21,29,46,47,60,80,17,23,30,56,61,100,101,121,22,24,32,57,87,142,141,182,242,25,28,35,59,92,168,181,304,303,364,27,31,38,62,96,173,263",
			"name": "Rectangular array R by antidiagonals:  row n shows the positive integers whose base-3 digits have total variation n, for n\u003e=0.  See Comments.",
			"comment": [
				"Suppose that a number n has base-b digits b(m), b(m-1), ..., b(0).  The base-b down-variation of n is the sum DV(n,b) of all d(i)-d(i-1) for which d(i) \u003e d(i-1); the base-b up-variation of n is the sum UV(n,b) of all d(k-1)-d(k) for which d(k) \u003c d(k-1).  The total base-b variation of n is the sum TV(n,b) = DV(n,b) + UV(n,b). See A297330 for a guide to related sequences and partitions of the natural numbers.",
				"Every positive integer occurs exactly once in the array, so that as a sequence this is a permutation of the positive integers.",
				"Conjecture:  each column, after some number of initial terms, satisfies the linear recurrence relation c(n) = c(n-1) + 2*c(n-2) - 2*c(n-3) + 3*c(n-4) - 3*c(n-5)."
			],
			"example": [
				"Northwest corner:",
				"   1      2      4      8     13     26     40     80",
				"   3      5      7      9     12     14     17     22",
				"   6     10     16     18     21     23     24     28",
				"  11     15     19     29     30     32     35     38",
				"  20     34     46     56     57     59     62     65",
				"  33     47     61     87     92     96     99    102"
			],
			"mathematica": [
				"a[n_, b_] := Differences[IntegerDigits[n, b]];",
				"b = 3; z = 250000; t = Table[a[n, b], {n, 1, z}];",
				"u = Map[Total, Map[Abs, t]]; p[n_] := Position[u, n];",
				"TableForm[Table[Take[Flatten[p[n]], 15], {n, 0, 9}]]",
				"v[n_, k_] := p[k - 1][[n]]",
				"Table[v[k, n - k + 1], {n, 12}, {k, n, 1, -1}] // Flatten"
			],
			"xref": [
				"Cf. A007089, A297330 (guide), A297443 (conjectured 1st column), A297441, A297442."
			],
			"keyword": "nonn,tabl,base,easy",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Jan 20 2018",
			"references": 3,
			"revision": 12,
			"time": "2021-07-03T11:21:33-04:00",
			"created": "2018-01-21T04:07:44-05:00"
		}
	]
}