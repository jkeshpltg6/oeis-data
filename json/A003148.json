{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003148",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3148,
			"id": "M4389",
			"data": "1,1,7,27,321,2265,37575,390915,8281665,114610545,2946939975,51083368875,1542234996225,32192256321225,1114841223671175,27254953356505875,1064057291370698625,29845288035840902625,1296073464766972266375,41049997128507054562875",
			"name": "a(n+1) = a(n) + 2n*(2n+1)*a(n-1), with a(0) = a(1) = 1.",
			"comment": [
				"Numerators of sequence of fractions with e.g.f. 1/((1-x)*(1+x)^(1/2)). The denominators are successive powers of 2.",
				"a(n) is the coefficient of x^n in arctan(sqrt(2*x/(1-x)))/sqrt(2*x*(1-x)) multiplied by (2*n+1)!!.",
				"This sequence is the linking pin between the a(n) formulas of the ED1, ED2, ED3 and ED4 array rows, see A167552, A167565, A167580 and A167591. - _Johannes W. Meijer_, Nov 23 2009"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A003148/b003148.txt\"\u003eTable of n, a(n) for n=0..100\u003c/a\u003e",
				"P. S. Bruckman, \u003ca href=\"http://www.fq.math.ca/Scanned/10-2/bruckman.pdf\"\u003eAn interesting sequence of numbers derived from various generating functions\u003c/a\u003e, Fib. Quart., 10 (1972), 169-181.",
				"R. J. Mathar, \u003ca href=\"https://arxiv.org/abs/math/0306184\"\u003eNumerical Representation of the Incomplete Gamma Function of Complex Argument\u003c/a\u003e, arXiv:math/0306184 [math.NA], 2003-2004; cf. Eq. 22."
			],
			"formula": [
				"a(n) = (-1)^n*(2n-1)!! + 2n*a(n-1) with (2n-1)!! = 1*3*5*..*(2n-1) the double factorial. - _R. J. Mathar_, Jun 12 2003",
				"a(n) = [(2n+1)!!/4] Int ([cos(phi)]^n cos(phi/2), phi=-Pi..Pi). - _R. J. Mathar_, Jun 30 2003",
				"a(n) = (2n+1)!! 2F1(-n, 1/2;3/2;2). - _R. J. Mathar_, Jun 30 2003",
				"In terms of the (terminating) Gauss hypergeometric function/series 2F1(., .; ., .) a(n) is a special case of the family of integer sequences defined by a(m, n) = [(2n+2m+1)!!/(2m+1)] 2F1(-n, m+1/2; m+3/2; 2), m=0, 1, 2, ..., n=0, 1, 2, ...; a(n) = a(0, n); a(m, n) = [(2n+2m+1)!!/4] Int ([sin(phi/2)]^(2m) [cos(phi)]^n cos(phi/2), phi=-Pi. .Pi); 4(n+1)a(m, n) = (2m-1) a(m-1, n+1)+(-1)^n (2n+2m+1)!!. a(0, n) = this sequence, a(1, n) = A077568, a(2, n) = A084543. - _R. J. Mathar_, Jun 30 2003",
				"E.g.f.: 1/(sqrt(1+2*x)*(1-2*x)). - _Vladeta Jovovic_, Oct 12 2003",
				"a(n) = (2^n)*n!*A123746(n)/A046161(n) = (2^n)*n!*sum(binomial(2*k,k)*(-1/4)^k,k=0..n). From the e.g.f. - _Wolfdieter Lang_, Oct 06 2008.",
				"a(n) = A091520(n) * n! / 2^n. - _Michael Somos_, Mar 17 2011"
			],
			"example": [
				"arctan(sqrt(2*x/(1-x)))/sqrt(2*x*(1-x)) = 1 + 1/3*x + 7/15*x^2 + 9/35*x^3 + ..."
			],
			"maple": [
				"# double factorial of odd \"l\" df := proc(l) local n; n := iquo(l,2); RETURN( factorial(l)/2^n/factorial(n)); end: x := 1; for n from 1 to 15 do if n mod 2 = 0 then x := 2*n*x+df(2*n-1); else x := 2*n*x-df(2*n-1); fi; print(x); od; quit"
			],
			"mathematica": [
				"a[n_] := a[n] = (-1)^n*(2n - 1)!! + 2n*a[n - 1]; a[0] = 1; Table[ a[n], {n, 0, 14}] (* _Jean-François Alcover_, Dec 01 2011, after _R. J. Mathar_ *)",
				"a[ n_] := If[ n \u003c 0, 0, (2 n + 1)!! Hypergeometric2F1[ -n, 1/2, 3/2, 2]]; (* _Michael Somos_, Apr 20 2018 *)",
				"a[ n_] := If[ n \u003c 0, 0, n! SeriesCoefficient[ 1 / ((1 - 2 x) Sqrt[1 + 2 x]), {x, 0, n}]]; (* _Michael Somos_, Apr 20 2018 *)",
				"RecurrenceTable[{a[0]==a[1]==1,a[n+1]==a[n]+2n(2n+1)a[n-1]},a,{n,20}] (* _Harvey P. Dale_, Jul 27 2019 *)"
			],
			"program": [
				"(Haskell)",
				"a003148 n = a003148_list !! n",
				"a003148_list = 1 : 1 : zipWith (+) (tail a003148_list)",
				"                          (zipWith (*) (tail a002943_list) a003148_list)",
				"-- _Reinhard Zumkeller_, Nov 22 2011",
				"(PARI) Vec(serlaplace(1/(sqrt(1+2*x + O(x^20))*(1-2*x)))) \\\\ _Andrew Howroyd_, Feb 05 2018"
			],
			"xref": [
				"Contribution from _Johannes W. Meijer_, Nov 23 2009: (Start)",
				"Appears in A167552, A167565, A167580 and A167591.",
				"Equals A049606*A123746.",
				"(End)",
				"Cf. A002943."
			],
			"keyword": "nonn,nice,easy",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"a(16)-a(20) from _Andrew Howroyd_, Feb 05 2018"
			],
			"references": 13,
			"revision": 45,
			"time": "2019-07-27T12:48:02-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}