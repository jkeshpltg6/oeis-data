{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A184160",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 184160,
			"data": "1,2,3,4,4,6,5,8,9,8,5,12,7,10,12,16,6,18,9,16,15,10,10,24,16,14,27,20,9,24,6,32,15,12,20,36,13,18,21,32,8,30,11,20,36,20,13,48,25,32,18,28,17,54,20,40,27,18,7,48,19,12,45,64,28,30,10,24,30,40,17,72,16,26,48,36,25,42,11,64,81,16,11,60,24",
			"name": "Number of root subtrees in the rooted tree with Matula-Goebel number n.",
			"comment": [
				"A root subtree of a rooted tree T is a subtree of T containing the root.",
				"Also number of antichains in the rooted tree with Matula-Goebel number n. (The vertices of a rooted tree can be regarded as a partially ordered set, where u\u003c=v holds for two vertices u and v if and only if u lies on the unique path between v and the root; an antichain is a nonempty set of mutually incomparable vertices). Example: a(5)=4 because the rooted tree with Matula-Goebel number 5 is a path ABCD and the only antichains are the 1-element subsets of {A,B,C,D}.",
				"There is a simple bijection between the root subtrees and the antichains of a rooted tree: the leaves of a root subtree form an antichain (supplied by Stephan Wagner).",
				"The Matula-Goebel number of a rooted tree can be defined in the following recursive manner: to the one-vertex tree there corresponds the number 1; to a tree T with root degree 1 there corresponds the t-th prime number, where t is the Matula-Goebel number of the tree obtained from T by deleting the edge emanating from the root; to a tree T with root degree m\u003e=2 there corresponds the product of the Matula-Goebel numbers of the m branches of T."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A184160/b184160.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"É. Czabarka, L. Székely, and S. Wagner, \u003ca href=\"http://dx.doi.org/10.1016/j.dam.2009.07.004\"\u003eThe inverse problem for certain tree parameters\u003c/a\u003e, Discrete Appl. Math., 157, 2009, 3314-3319.",
				"E. Deutsch, \u003ca href=\"https://arxiv.org/abs/1111.4288\"\u003eTree statistics from Matula numbers\u003c/a\u003e, arXiv preprint arXiv:1111.4288 [math.CO], 2011.",
				"F. Goebel, \u003ca href=\"http://dx.doi.org/10.1016/0095-8956(80)90049-0\"\u003eOn a 1-1-correspondence between rooted trees and natural numbers\u003c/a\u003e, J. Combin. Theory, B 29 (1980), 141-143.",
				"I. Gutman and A. Ivic, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(95)00182-V\"\u003eOn Matula numbers\u003c/a\u003e, Discrete Math., 150, 1996, 131-142.",
				"I. Gutman and Yeong-Nan Yeh, \u003ca href=\"http://www.emis.de/journals/PIMB/067/3.html\"\u003eDeducing properties of trees from their Matula numbers\u003c/a\u003e, Publ. Inst. Math., 53 (67), 1993, 17-22.",
				"D. W. Matula, \u003ca href=\"http://www.jstor.org/stable/2027327\"\u003eA natural rooted tree enumeration by prime factorization\u003c/a\u003e, SIAM Rev. 10 (1968) 273.",
				"\u003ca href=\"/index/Mat#matula\"\u003eIndex entries for sequences related to Matula-Goebel numbers\u003c/a\u003e"
			],
			"formula": [
				"a(1)=1; if n=p(t) (=the t-th prime), then a(n)=1+a(t); if n=rs (r,s,\u003e=2), then a(n)=a(r)a(s). The Maple program is based on this recursive formula.",
				"Completely multiplicative with a(prime(t)) = 1 + a(t). - _Andrew Howroyd_, Aug 01 2018"
			],
			"example": [
				"a(2^m) = 2^m because the rooted tree with Matula-Goebel number 2^m is a star with m edges (each edge can be included or not in the subtree)."
			],
			"maple": [
				"with(numtheory): a := proc (n) local r, s: r := proc (n) options operator, arrow: op(1, factorset(n)) end proc: s := proc (n) options operator, arrow: n/r(n) end proc: if n = 1 then 1 elif bigomega(n) = 1 then 1+a(pi(n)) else a(r(n))*a(s(n)) end if end proc: seq(a(n), n = 1 .. 85);"
			],
			"mathematica": [
				"a[1] = 1; a[p_?PrimeQ] := a[p] = 1+a[PrimePi[p]]; a[n_] := a[n] = With[{f = FactorInteger[n]}, Times @@ ((a /@ f[[All, 1]])^f[[All, 2]])]; Array[a, 100] (* _Jean-François Alcover_, May 03 2017 *)"
			],
			"program": [
				"(PARI) a(n)={my(f=factor(n)); prod(i=1, #f~, my([p,e]=f[i,]); (1 + a(primepi(p)))^e)} \\\\ _Andrew Howroyd_, Aug 01 2018"
			],
			"xref": [
				"Cf. A184161."
			],
			"keyword": "nonn,mult",
			"offset": "1,2",
			"author": "_Emeric Deutsch_, Oct 19 2011",
			"references": 6,
			"revision": 30,
			"time": "2018-08-01T18:14:04-04:00",
			"created": "2011-10-19T11:57:37-04:00"
		}
	]
}