{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A303217",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 303217,
			"data": "3,8,4,15,9,5,20,16,10,6,30,24,18,12,7,40,36,27,21,14,11,70,48,42,28,33,19,13,60,81,54,44,32,35,22,17,80,72,104,56,45,52,37,25,23,90,84,110,105,64,50,55,38,26,29,140,126,88,112,136,78,57,74,39,31,43",
			"name": "A(n,k) is the n-th index of a Fibonacci number with exactly k distinct prime factors; square array A(n,k), n\u003e=1, k\u003e=1, read by antidiagonals.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A303217/b303217.txt\"\u003eAntidiagonals n = 1..18, flattened\u003c/a\u003e"
			],
			"formula": [
				"A000045(A(n,k)) = A303218(n,k).",
				"A001221(A000045(A(n,k))) = k."
			],
			"example": [
				"Square array A(n,k) begins:",
				"   3,  8, 15, 20, 30,  40,  70,  60,  80,  90, ...",
				"   4,  9, 16, 24, 36,  48,  81,  72,  84, 126, ...",
				"   5, 10, 18, 27, 42,  54, 104, 110,  88, 165, ...",
				"   6, 12, 21, 28, 44,  56, 105, 112,  96, 256, ...",
				"   7, 14, 33, 32, 45,  64, 136, 114, 100, 258, ...",
				"  11, 19, 35, 52, 50,  78, 148, 128, 108, 266, ...",
				"  13, 22, 37, 55, 57,  92, 152, 130, 132, 296, ...",
				"  17, 25, 38, 74, 63,  95, 164, 135, 138, 304, ...",
				"  23, 26, 39, 77, 66,  99, 182, 147, 156, 322, ...",
				"  29, 31, 46, 85, 68, 102, 186, 154, 184, 369, ..."
			],
			"maple": [
				"F:= combinat[fibonacci]: with(numtheory):",
				"A:= proc() local h, p, q; p, q:= proc() [] end, 2;",
				"      proc(n, k)",
				"        while nops(p(k))\u003cn do q:= q+1;",
				"          h:= nops(factorset(F(q)));",
				"          p(h):= [p(h)[], (q)]",
				"        od; p(k)[n]",
				"      end",
				"    end():",
				"seq(seq(A(n, 1+d-n), n=1..d), d=1..12);"
			],
			"mathematica": [
				"nmax = 12; maxIndex = 200;",
				"nu[n_] := nu[n] = PrimeNu[Fibonacci[n]];",
				"col[k_] := Select[Range[maxIndex], nu[#] == k\u0026];",
				"T = Array[col, nmax];",
				"A[n_, k_] := T[[k, n]];",
				"Table[A[n-k+1, k], {n, 1, nmax}, {k, n, 1, -1}] // Flatten (* _Jean-François Alcover_, Jan 04 2020 *)"
			],
			"xref": [
				"Columns k=2-16 give: A114842, A114841, A114843, A114840, A114839, A114838, A114837, A114836, A114826, A114825, A114824, A114823, A117529, A117551, A117550.",
				"Row n=1 gives: A060320.",
				"Cf. A000045, A001221, A022307, A303215, A303218."
			],
			"keyword": "nonn,tabl",
			"offset": "1,1",
			"author": "_Alois P. Heinz_, Apr 19 2018",
			"references": 19,
			"revision": 23,
			"time": "2020-01-04T14:47:02-05:00",
			"created": "2018-04-20T17:52:02-04:00"
		}
	]
}