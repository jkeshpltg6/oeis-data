{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A331933",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 331933,
			"data": "1,1,1,2,4,6,12,18,33,52,90,142,242,384,639,1028,1688,2716,4445,7161,11665,18839,30595,49434,80199,129637,210079,339750,550228,889978,1440909,2330887,3772845,6103823,9878357,15982196,25863454,41845650,67713550,109559443",
			"name": "Number of semi-lone-child-avoiding rooted trees with at most one distinct non-leaf branch directly under any vertex.",
			"comment": [
				"A rooted tree is semi-lone-child-avoiding if there are no vertices with exactly one child unless the child is an endpoint/leaf."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A331933/b331933.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"David Callan, \u003ca href=\"http://arxiv.org/abs/1406.7784\"\u003eA sign-reversing involution to count labeled lone-child-avoiding trees\u003c/a\u003e, arXiv:1406.7784 [math.CO], (30-June-2014).",
				"Gus Wiseman, \u003ca href=\"https://docs.google.com/document/d/e/2PACX-1vS1zCO9fgAIe5rGiAhTtlrOTuqsmuPos2zkeFPYB80gNzLb44ufqIqksTB4uM9SIpwlvo-oOHhepywy/pub\"\u003eSequences counting series-reduced and lone-child-avoiding trees by number of vertices.\u003c/a\u003e",
				"Gus Wiseman, \u003ca href=\"/A331933/a331933.png\"\u003eThe a(11) = 90 semi-lone-child-avoiding rooted trees with at most one distinct non-leaf branch directly under any vertex.\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 1 + Sum_{i=2..n-2} floor((n-1)/i)*a(i). - _Andrew Howroyd_, Feb 09 2020"
			],
			"example": [
				"The a(1) = 1 through a(8) = 18 trees:",
				"  o  (o)  (oo)  (ooo)   (oooo)    (ooooo)    (oooooo)",
				"                (o(o))  (o(oo))   (o(ooo))   (o(oooo))",
				"                        (oo(o))   (oo(oo))   (oo(ooo))",
				"                        ((o)(o))  (ooo(o))   (ooo(oo))",
				"                                  (o(o)(o))  (oooo(o))",
				"                                  (o(o(o)))  ((oo)(oo))",
				"                                             (o(o(oo)))",
				"                                             (o(oo(o)))",
				"                                             (oo(o)(o))",
				"                                             (oo(o(o)))",
				"                                             ((o)(o)(o))",
				"                                             (o((o)(o)))"
			],
			"mathematica": [
				"sseo[n_]:=Switch[n,1,{{}},2,{{{}}},_,Join@@Function[c,Select[Union[Sort/@Tuples[sseo/@c]],Length[Union[DeleteCases[#,{}]]]\u003c=1\u0026]]/@Rest[IntegerPartitions[n-1]]];",
				"Table[Length[sseo[n]],{n,10}]"
			],
			"program": [
				"(PARI) seq(n)={my(v=vector(n)); for(n=1, n, v[n] = 1 + sum(i=2, n-2, ((n-1)\\i)*v[i])); v} \\\\ _Andrew Howroyd_, Feb 09 2020"
			],
			"xref": [
				"Not requiring lone-child-avoidance gives A320222.",
				"The non-semi version is A320268.",
				"Matula-Goebel numbers of these trees are A331936.",
				"Achiral trees are A003238.",
				"Semi-identity trees are A306200.",
				"Numbers S with at most one distinct prime index in S are A331912.",
				"Semi-lone-child-avoiding rooted trees are A331934.",
				"Cf. A000081, A001678, A004111, A050381, A214577, A291636, A320230, A320269, A331784, A331872, A331914, A331935, A331966."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Gus Wiseman_, Feb 03 2020",
			"ext": [
				"Terms a(31) and beyond from _Andrew Howroyd_, Feb 09 2020"
			],
			"references": 10,
			"revision": 9,
			"time": "2020-02-09T18:58:00-05:00",
			"created": "2020-02-03T22:18:22-05:00"
		}
	]
}