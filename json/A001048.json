{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001048",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1048,
			"id": "M0890 N0337",
			"data": "2,3,8,30,144,840,5760,45360,403200,3991680,43545600,518918400,6706022400,93405312000,1394852659200,22230464256000,376610217984000,6758061133824000,128047474114560000,2554547108585472000,53523844179886080000,1175091669949317120000",
			"name": "a(n) = n! + (n-1)!.",
			"comment": [
				"Number of {12, 12*, 1*2, 21, 21*}-avoiding signed permutations in the hyperoctahedral group.",
				"a(n) is the hook product of the shape (n, 1). - _Emeric Deutsch_, May 13 2004",
				"From _Jaume Oliver Lafont_, Dec 01 2009: (Start)",
				"(1+(x-1)*exp(x))/x = Sum_{k \u003e= 1} x^k/a(k).",
				"Setting x = 1 yields Sum_{k \u003e= 1} 1/a(k) = 1. [Jolley eq 302] (End)",
				"For n \u003e= 2, a(n) is the size of the largest conjugacy class of the symmetric group on n + 1 letters. Equivalently, the maximum entry in each row of A036039. - _Geoffrey Critzer_, May 19 2013",
				"In factorial base representation (A007623) the terms are written as: 10, 11, 110, 1100, 11000, 110000, ... From a(2) = 3 = \"11\" onward each term begins always with two 1's, followed by n-2 zeros. - _Antti Karttunen_, Sep 24 2016",
				"e is approximately a(n)/A000255(n-1) for large n. - _Dale Gerdemann_, Jul 26 2019",
				"a(n) is the number of permutations of [n+1] in which all the elements of [n] are cycle-mates, that is, 1,..,n are all in the same cycle. This result is readily shown after noting that the elements of [n] can be members of a n-cycle or an (n+1)-cycle. Hence a(n)=(n-1)!+n!. See an example below. - _Dennis P. Walsh_, May 24 2020"
			],
			"reference": [
				"L. B. W. Jolley, Summation of Series, Dover, 1961.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A001048/b001048.txt\"\u003eTable of n, a(n) for n=1..100\u003c/a\u003e",
				"Barry Balof and Helen Jenne, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Balof/balof22.html\"\u003eTilings, Continued Fractions, Derangements, Scramblings, and e\u003c/a\u003e, Journal of Integer Sequences, Vol. 17 (2014), #14.2.7.",
				"E. Biondi, L. Divieti, and G. Guardabassi, \u003ca href=\"http://dx.doi.org/10.4153/CJM-1970-003-9\"\u003eCounting paths, circuits, chains and cycles in graphs: A unified approach\u003c/a\u003e, Canad. J. Math., Vol. 22, No. 1 (1970), pp. 22-35.",
				"Richard K. Guy, \u003ca href=\"/A002186/a002186.pdf\"\u003eLetters to N. J. A. Sloane, June-August 1968\u003c/a\u003e.",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=97\"\u003eEncyclopedia of Combinatorial Structures 97\u003c/a\u003e.",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=641\"\u003eEncyclopedia of Combinatorial Structures 641\u003c/a\u003e.",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=101\"\u003eEncyclopedia of Combinatorial Structures 101\u003c/a\u003e.",
				"Helen K. Jenne, \u003ca href=\"https://www.whitman.edu/Documents/Academics/Mathematics/Jenne.pdf\"\u003eProofs you can count on\u003c/a\u003e, Honors Thesis, Math. Dept., Whitman College, 2013.",
				"B. D. Josephson and J. M. Boardman, \u003ca href=\"https://archive.org/details/eureka-24/page/19/mode/2up\"\u003eProblems Drive 1961\u003c/a\u003e, Eureka, The Journal of the Archimedeans, Vol. 24 (1961), p. 20; \u003ca href=\"https://www.archim.org.uk/eureka/archive/Eureka-24.pdf\"\u003eentire volume\u003c/a\u003e.",
				"T. Mansour and J. West, \u003ca href=\"http://arxiv.org/abs/math/0207204\"\u003eAvoiding 2-letter signed patterns\u003c/a\u003e, arXiv:math/0207204 [math.CO], 2002.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/UniformSumDistribution.html\"\u003eUniform Sum Distribution\u003c/a\u003e.",
				"\u003ca href=\"/index/Fa#facbase\"\u003eIndex entries for sequences related to factorial base representation\u003c/a\u003e",
				"\u003ca href=\"/index/Fa#factorial\"\u003eIndex entries for sequences related to factorial numbers\u003c/a\u003e"
			],
			"formula": [
				"a(n) = (n+1)*(n-1)!.",
				"E.g.f.: x/(1-x) - log(1-x). - _Ralf Stephan_, Apr 11 2004",
				"The sequence 1, 3, 8, ... has g.f. (1+x-x^2)/(1-x)^2 and a(n) = n!(n + 2 - 0^n) = n!A065475(n) (offset 0). - _Paul Barry_, May 14 2004",
				"a(n) = (n+1)!/n. - Claude Lenormand (claude.lenormand(AT)free.fr), Aug 24 2003",
				"Factorial expansion of 1: 1 = sum_{n \u003e 0} 1/a(n) [Jolley eq 302]. - Claude Lenormand (claude.lenormand(AT)free.fr), Aug 24 2003",
				"a(1) = 2, a(2) = 3, a(n) = (n^2 - n - 2)*a(n-2) for n \u003e= 3. - _Jaume Oliver Lafont_, Dec 01 2009",
				"a(n) = ((n+2)A052649(n) - A052649(n+1))/2. - _Gary Detlefs_, Dec 16 2009",
				"G.f.: U(0) where U(k) = 1 + (k+1)/(1 - x/(x + 1/U(k+1))) ; (continued fraction, 3-step). - _Sergei N. Gladkovskii_, Sep 25 2012",
				"G.f.: 2*(1+x)/x/G(0) - 1/x, where G(k)= 1 + 1/(1 - x*(2*k+2)/(x*(2*k+2) - 1 + x*(2*k+2)/G(k+1))); (continued fraction). - _Sergei N. Gladkovskii_, May 31 2013",
				"a(n) = (n-1)*a(n-1) + (n-1)!. - _Bruno Berselli_, Feb 22 2017",
				"a(1)=2, a(2)=3, a(n) = (n-1)*a(n-1) + (n-2)*a(n-2). - _Dale Gerdemann_, Jul 26 2019",
				"a(n) = 2*A000255(n-1) + A096654(n-2). - _Dale Gerdemann_, Jul 26 2019",
				"Sum_{n\u003e=1} (-1)^(n+1)/a(n) = 1 - 2/e (A334397). - _Amiram Eldar_, Jan 13 2021"
			],
			"example": [
				"For n=3, a(3) counts the 8 permutations of [4] with 1,2, and 3 all in the same cycle, namely, (1 2 3)(4), (1 3 2)(4), (1 2 3 4), (1 2 4 3), (1 3 2 4), (1 2 4 3), (1 4 2 3), and (1 4 3 2). - _Dennis P. Walsh_, May 24 2020"
			],
			"maple": [
				"seq(n!+(n-1)!,n=1..25);"
			],
			"mathematica": [
				"Table[n! + (n + 1)!, {n, 0, 30}] (* _Vladimir Joseph Stephan Orlovsky_, Apr 14 2011 *)",
				"Total/@Partition[Range[0, 20]!, 2, 1] (* _Harvey P. Dale_, Nov 29 2013 *)"
			],
			"program": [
				"(MAGMA) [Factorial(n)+Factorial(n+1): n in [0..30]]; // _Vincenzo Librandi_, Aug 08 2014",
				"(PARI) a(n)=denominator(polcoeff((x-1)*exp(x+x*O(x^(n+1))),n+1)); \\\\ _Gerry Martens_, Aug 12 2015",
				"(PARI) vector(30, n, (n+1)*(n-1)!) \\\\ _Michel Marcus_, Aug 12 2015"
			],
			"xref": [
				"Apart from initial terms, same as A059171.",
				"Equals the square root of the first right hand column of A162990. - _Johannes W. Meijer_, Jul 21 2009",
				"From a(2)=3 onward the second topmost row of arrays A276588 and A276955.",
				"Cf. sequences with formula (n + k)*n! listed in A282466, A334397."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_, _R. K. Guy_",
			"ext": [
				"More terms from _James A. Sellers_, Sep 19 2000"
			],
			"references": 41,
			"revision": 133,
			"time": "2021-01-13T07:15:30-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}