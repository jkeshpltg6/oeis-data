{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A290004",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 290004,
			"data": "3,72,1419,26580,487839,8867088,160391235,2894149932,52158948999,939440707560,16915155908523,304519845578052,5481780715831215,98675865000853056,1776199882077971859,31971906699808312284,575497100061532320855,10358972816581956751128",
			"name": "Wiener index of the n-Hanoi graph.",
			"comment": [
				"Sequence gives 1/2 of the total number of moves summed over all starting and finishing positions in the tower of Hanoi puzzle with n disks. For just the total number of moves from all starting positions to the standard finish position see A060589."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A290004/b290004.txt\"\u003eTable of n, a(n) for n = 1..100\u003c/a\u003e",
				"T. Chan, \u003ca href=\"http://dx.doi.org/10.1080/00207168908803728\"\u003eA statistical analysis of the towers of Hanoi problem\u003c/a\u003e, Internat. J. Comput. Math. 28: 57-65.",
				"A. Hinz, \u003ca href=\"https://doi.org/10.5169/seals-57378\"\u003eThe Tower of Hanoi\u003c/a\u003e, L'Enseignement Mathématique, 35: 289-321.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/HanoiGraph.html\"\u003eHanoi Graph\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/WienerIndex.html\"\u003eWiener Index\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Tower_of_Hanoi\"\u003eTower of Hanoi\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (35, -395, 1761, -2916, 972).",
				"\u003ca href=\"/index/To#Hanoi\"\u003eIndex entries for sequences related to Towers of Hanoi\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 35*a(n-1) - 395*a(n-2) + 1761*a(n-3) - 2916*a(n-4) + 972*a(n-5).",
				"G.f.: 3*x*(1 - 11*x + 28*x^2 + 24*x^3)/((1 - 3*x)*(1 - 9*x)*(1 - 18*x)*(1 - 5*x + 2*x^2)). [Corrected by _Georg Fischer_, May 19 2019]"
			],
			"mathematica": [
				"(* Start from _Eric W. Weisstein_, Sep 07 2017 *)",
				"Table[3/1003 2^-n ((34 - 3 Sqrt[17]) (5 - Sqrt[17])^n + (5 + Sqrt[17])^n (34 + 3 Sqrt[17])) - 3^(n + 1)/10 + 9^n (233/885 2^n - 1/6), {n, 10}]",
				"LinearRecurrence[{35, -395, 1761, -2916, 972}, {3, 72, 1419, 26580, 487839}, 20]",
				"CoefficientList[Series[(3 (1 - 11 x + 28 x^2 + 24 x^3))/((1 - 3 x) (1 - 9 x) (1 - 18 x) (1 - 5 x + 2 x^2)), {x, 0, 20}], x]",
				"(* End *)"
			],
			"program": [
				"(PARI) Vec(3*(1 - 11*x + 28*x^2 + 24*x^3)/((1 - 3*x)*(1 - 9*x)*(1 - 18*x)*(1 - 5*x + 2*x^2)) + O(x^20))"
			],
			"xref": [
				"Cf. A060589, A290129."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_Andrew Howroyd_, Sep 03 2017",
			"references": 2,
			"revision": 26,
			"time": "2021-08-29T02:03:55-04:00",
			"created": "2017-09-04T09:25:25-04:00"
		}
	]
}