{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A209172",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 209172,
			"data": "1,1,1,1,3,1,1,4,7,1,1,6,11,15,1,1,7,23,26,31,1,1,9,30,72,57,63,1,1,10,48,102,201,120,127,1,1,12,58,198,303,522,247,255,1,1,13,82,256,699,825,1291,502,511,1,1,15,95,420,955,2223,2116,3084,1013,1023,1",
			"name": "Triangle of coefficients of polynomials u(n,x) jointly generated with A209413; see the Formula section.",
			"comment": [
				"For n \u003e 1, n-th alternating row sum = ((-1)^n)*F(2n-4), where F=A000045 (Fibonacci numbers).  For a discussion and guide to related arrays, see A208510.",
				"Subtriangle of the triangle given by (1, 0, 1, -2, 0, 0, 0, 0, 0, 0, 0, ...) DELTA (0, 1, 0, 2, 0, 0, 0, 0, 0, 0, 0, ...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, Mar 11 2012"
			],
			"formula": [
				"u(n,x) = x*u(n-1,x) + v(n-1,x),",
				"v(n,x) = u(n-1,x) + 2x*v(n-1,x),",
				"where u(1,x)=1, v(1,x)=1.",
				"From _Philippe Deléham_, Mar 11 2012: (Start)",
				"As DELTA-triangle T(n,k) with 0 \u003c= k \u003c= n:",
				"T(n,k) = 3*T(n-1,k-1) + T(n-2,k) - 2*T(n-2,k-2) with T(0,0) = T(1,0) = T(2,0) = T(2,1) = 1, T(1,1) = T(2,2) = 0 and T(n,k) = 0 if k \u003c 0 or if k \u003e n.",
				"G.f.: (1+x-3*y*x-2*y*x^2+2*y^2*x^2)/(1-3*y*x-x^2+2*y^2*x^2). (End)"
			],
			"example": [
				"First five rows:",
				"  1;",
				"  1,  1;",
				"  1,  3,  1;",
				"  1,  4,  7,  1;",
				"  1,  6, 11, 15,  1;",
				"First three polynomials v(n,x):",
				"  1",
				"  1 + x",
				"  1 + 3x + x^2.",
				"From _Philippe Deléham_, Mar 11 2012: (Start)",
				"(1, 0, 1, -2, 0, 0, 0,...) DELTA (0, 1, 0, 2, 0, 0, ...) begins:",
				"  1;",
				"  1,  0;",
				"  1,  1,  0;",
				"  1,  3,  1,  0;",
				"  1,  4,  7,  1,  0;",
				"  1,  6, 11, 15,  1,  0;",
				"  1,  7, 23, 26, 31,  1,  0;",
				"  1,  9, 30, 72, 57, 63,  1,  0; (End)"
			],
			"mathematica": [
				"u[1, x_] := 1; v[1, x_] := 1; z = 16;",
				"u[n_, x_] := x*u[n - 1, x] + v[n - 1, x];",
				"v[n_, x_] := u[n - 1, x] + 2 x*v[n - 1, x];",
				"Table[Expand[u[n, x]], {n, 1, z/2}]",
				"Table[Expand[v[n, x]], {n, 1, z/2}]",
				"cu = Table[CoefficientList[u[n, x], x], {n, 1, z}];",
				"TableForm[cu]",
				"Flatten[%]    (* A209172 *)",
				"Table[Expand[v[n, x]], {n, 1, z}]",
				"cv = Table[CoefficientList[v[n, x], x], {n, 1, z}];",
				"TableForm[cv]",
				"Flatten[%]    (* A209413 *)"
			],
			"xref": [
				"Cf. A209413, A208510."
			],
			"keyword": "nonn,tabl",
			"offset": "1,5",
			"author": "_Clark Kimberling_, Mar 08 2012",
			"references": 3,
			"revision": 15,
			"time": "2020-01-24T03:29:52-05:00",
			"created": "2012-03-08T19:42:09-05:00"
		}
	]
}