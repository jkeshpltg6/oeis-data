{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A218864",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 218864,
			"data": "0,1,17,20,52,57,105,112,176,185,265,276,372,385,497,512,640,657,801,820,980,1001,1177,1200,1392,1417,1625,1652,1876,1905,2145,2176,2432,2465,2737,2772,3060,3097,3401,3440,3760,3801,4137,4180,4532,4577,4945,4992",
			"name": "Numbers of the form 9*k^2 + 8*k, k an integer.",
			"comment": [
				"Numbers m such that 9*m + 16 is a square. - _Vincenzo Librandi_, Apr 07 2013",
				"Equivalently, integers of the form h*(h + 8)/9 (nonnegative values of h are listed in A090570). - _Bruno Berselli_, Jul 15 2016",
				"Generalized 20-gonal (or icosagonal) numbers: r*(9*r - 8) with r = 0, +1, -1, +2, -2, +3, -3, ... - _Omar E. Pol_, Jun 06 2018",
				"Partial sums of A317316. - _Omar E. Pol_, Jul 28 2018",
				"Exponents in expansion of Product_{n \u003e= 1} (1 + x^(18*n-17))*(1 + x^(18*n-1))*(1 - x^(18*n)) = 1 + x + x^17 + x^20 + x^52 + .... - _Peter Bala_, Dec 10 2020"
			],
			"link": [
				"Jason Kimberley, \u003ca href=\"/A218864/b218864.txt\"\u003eTable of n, a(n) for n = 1..2000\u003c/a\u003e",
				"S. Cooper and M. D. Hirschhorn, \u003ca href=\"http://dx.doi.org/10.1016/S0012-365X(03)00079-7\"\u003eResults of Hurwitz type for three squares.\u003c/a\u003e Discrete Math. 274 (2004), no. 1-3, 9-24. See C(q).",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,2,-2,-1,1)."
			],
			"formula": [
				"a(n) = (18*n*(n - 1) - 7*(-1)^n*(2*n - 1) - 7)/8. - _Bruno Berselli_, Nov 13 2012",
				"G.f.: x*(1 + 16*x + x^2)/((1 + x)^2*(1 - x)^3). - _Bruno Berselli_, Nov 14 2012"
			],
			"mathematica": [
				"Array[(18 # (# - 1) - 7 (-1)^#*(2 # - 1) - 7)/8 \u0026, 48] (* or *)",
				"CoefficientList[Series[x (1 + 16 x + x^2)/((1 + x)^2*(1 - x)^3), {x, 0, 47}], x] (* _Michael De Vlieger_, Jun 06 2018 *)"
			],
			"program": [
				"(MAGMA) a:=func\u003cn | 9*n^2+8*n\u003e; [0]cat[a(n*m): m in [-1,1], n in [1..20]];"
			],
			"xref": [
				"Characteristic function is A205987.",
				"Numbers of the form 9*m^2+k*m, for integer n: A016766 (k=0), A132355 (k=2), A185039 (k=4), A057780 (k=6), this sequence (k=8).",
				"Cf. A074377: numbers m such that 16*m+9 is a square.",
				"Cf. A317316.",
				"For similar sequences of numbers m such that 9*m+i is a square, see list in A266956.",
				"Cf. sequences of the form m*(m+i)/(i+1) listed in A274978. [_Bruno Berselli_, Jul 25 2016]",
				"Sequences of generalized k-gonal numbers: A001318 (k=5), A000217 (k=6), A085787 (k=7), A001082 (k=8), A118277 (k=9), A074377 (k=10), A195160 (k=11), A195162 (k=12), A195313 (k=13), A195818 (k=14), A277082 (k=15), A274978 (k=16), A303305 (k=17), A274979 (k=18), A303813 (k=19), this sequence (k=20), A303298 (k=21), A303299 (k=22), A303303 (k=23), A303814 (k=24), A303304 (k=25), A316724 (k=26), A316725 (k=27), A303812 (k=28), A303815 (k=29), A316729 (k=30)."
			],
			"keyword": "nonn,easy",
			"offset": "1,3",
			"author": "_Jason Kimberley_, Nov 08 2012",
			"references": 37,
			"revision": 78,
			"time": "2020-12-10T07:18:51-05:00",
			"created": "2012-11-13T17:25:40-05:00"
		}
	]
}