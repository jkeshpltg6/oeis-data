{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A285321",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 285321,
			"data": "2,3,4,6,9,8,5,12,27,16,10,25,18,81,32,15,20,125,24,243,64,30,45,40,625,36,729,128,7,60,75,50,3125,48,2187,256,14,49,90,135,80,15625,54,6561,512,21,28,343,120,225,100,78125,72,19683,1024",
			"name": "Square array A(1,k) = A019565(k), A(n,k) = A065642(A(n-1,k)), read by descending antidiagonals.",
			"comment": [
				"A permutation of the natural numbers \u003e 1.",
				"Otherwise like array A284311, but the columns come in different order."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A285321/b285321.txt\"\u003eTable of n, a(n) for n = 1..120; the first 15 antidiagonals of array\u003c/a\u003e"
			],
			"formula": [
				"A(1,k) = A019565(k), A(n,k) = A065642(A(n-1,k)).",
				"For all n \u003e= 2: A(A008479(n), A087207(n)) = n."
			],
			"example": [
				"The top left 12x6 corner of the array:",
				"   2,   3,  6,     5,  10,  15,  30,      7,  14,  21,  42,   35",
				"   4,   9, 12,    25,  20,  45,  60,     49,  28,  63,  84,  175",
				"   8,  27, 18,   125,  40,  75,  90,    343,  56, 147, 126,  245",
				"  16,  81, 24,   625,  50, 135, 120,   2401,  98, 189, 168,  875",
				"  32, 243, 36,  3125,  80, 225, 150,  16807, 112, 441, 252, 1225",
				"  64, 729, 48, 15625, 100, 375, 180, 117649, 196, 567, 294, 1715"
			],
			"mathematica": [
				"a065642[n_] := Module[{k}, If[n == 1, Return[1], k = n + 1; While[ EulerPhi[k]/k != EulerPhi[n]/n, k++]]; k];",
				"A[1, k_] := Times @@ Prime[Flatten[Position[#, 1]]]\u0026[Reverse[ IntegerDigits[k, 2]]];",
				"A[n_ /; n \u003e 1, k_] := A[n, k] = a065642[A[n - 1, k]];",
				"Table[A[n - k + 1, k], {n, 1, 10}, {k, n, 1, -1}] // Flatten (* _Jean-François Alcover_, Nov 17 2019 *)"
			],
			"program": [
				"(Scheme)",
				"(define (A285321 n) (A285321bi (A002260 n) (A004736 n)))",
				"(define (A285321bi row col) (if (= 1 row) (A019565 col) (A065642 (A285321bi (- row 1) col))))",
				"(Python)",
				"from operator import mul",
				"from sympy import prime, primefactors",
				"def a019565(n): return reduce(mul, (prime(i+1) for i, v in enumerate(bin(n)[:1:-1]) if v == '1')) if n \u003e 0 else 1 # This function from _Chai Wah Wu_",
				"def a007947(n): return 1 if n\u003c2 else reduce(mul, primefactors(n))",
				"def a065642(n):",
				"    if n==1: return 1",
				"    r=a007947(n)",
				"    n = n + r",
				"    while a007947(n)!=r:",
				"        n+=r",
				"    return n",
				"def A(n, k): return a019565(k) if n==1 else a065642(A(n - 1, k))",
				"for n in range(1, 11): print([A(k, n - k + 1) for k in range(1, n + 1)]) # _Indranil Ghosh_, Apr 18 2017"
			],
			"xref": [
				"Transpose: A285322.",
				"Cf. A019565, A065642.",
				"Cf. A008479 (index of the row where n is located), A087207 (of the column).",
				"Cf. arrays A284311, A285325, also A285332."
			],
			"keyword": "nonn,tabl",
			"offset": "1,1",
			"author": "_Antti Karttunen_, Apr 17 2017",
			"references": 10,
			"revision": 24,
			"time": "2021-03-27T06:17:18-04:00",
			"created": "2017-04-17T22:43:29-04:00"
		}
	]
}