{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A131108",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 131108,
			"data": "1,1,1,2,3,1,2,6,5,1,2,8,12,7,1,2,10,20,20,9,1,2,12,30,40,30,11,1,2,14,42,70,70,42,13,1,2,16,56,112,140,112,56,15,1,2,18,72,168,252,252,168,72,17,1,2,20,90,240,420,504,420,240,90,19,1",
			"name": "T(n,k) = 2*A007318(n,k) - A097806(n,k).",
			"comment": [
				"Row sums give A095121.",
				"Triangle T(n,k), 0 \u003c= k \u003c= n, read by rows given by [1, 1, -2, 1, 0, 0, 0, 0, 0, 0, 0, ...] DELTA [1,0,0,1,0,0,0,0,0,0,0,...] where DELTA is the operator defined in A084938. - _Philippe Deléham_, Dec 18 2007"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A131108/b131108.txt\"\u003eRows n = 0..100 of triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"Twice Pascal's triangle minus A097806, the pairwise operator.",
				"G.f.: (1-x*y+x^2+x^2*y)/((-1+x+x*y)*(x*y-1)). - _R. J. Mathar_, Aug 11 2015"
			],
			"example": [
				"First few rows of the triangle are:",
				"  1;",
				"  1,  1;",
				"  2,  3,  1;",
				"  2,  6,  5,  1;",
				"  2,  8, 12,  7, 1;",
				"  2, 10, 20, 20, 9, 1;",
				"..."
			],
			"maple": [
				"seq(seq( `if`(k=n-1, 2*n-1, `if`(k=n, 1, 2*binomial(n,k))), k=0..n), n=0..12); # _G. C. Greubel_, Nov 18 2019"
			],
			"mathematica": [
				"Table[If[k==n-1, 2*n-1, If[k==n, 1, 2*Binomial[n, k]]], {n,0,12}, {k,0, n}]//Flatten (* _G. C. Greubel_, Nov 18 2019 *)"
			],
			"program": [
				"(PARI) T(n,k) = if(k==n-1, 2*n-1, if(k==n, 1, 2*binomial(n,k))); \\\\ _G. C. Greubel_, Nov 18 2019",
				"(MAGMA)",
				"function T(n,k)",
				"  if k eq n-1 then return 2*n-1;",
				"  elif k eq n then return 1;",
				"  else return 2*Binomial(n,k);",
				"  end if;",
				"  return T;",
				"end function;",
				"[T(n,k): k in [0..n], n in [0..12]]; // _G. C. Greubel_, Nov 18 2019",
				"(Sage)",
				"@CachedFunction",
				"def T(n, k):",
				"    if (k==n-1): return 2*n-1",
				"    elif (k==n): return 1",
				"    else: return 2*binomial(n,k)",
				"[[T(n, k) for k in (0..n)] for n in (0..12)] # _G. C. Greubel_, Nov 18 2019"
			],
			"xref": [
				"Cf. A007318, A095121, A097806."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_Gary W. Adamson_, Jun 15 2007",
			"ext": [
				"Corrected by _Philippe Deléham_, Dec 17 2007",
				"More terms added and data corrected by _G. C. Greubel_, Nov 18 2019"
			],
			"references": 4,
			"revision": 19,
			"time": "2019-11-19T07:27:03-05:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}