{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A228649",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 228649,
			"data": "2,6,14,22,30,34,38,42,58,66,70,78,86,94,102,106,110,114,130,138,142,158,166,178,182,186,194,202,210,214,218,222,230,238,254,258,266,282,286,302,310,318,322,330,346,354,358,366,382,390,394,398,402,410,418,430,434,438,446,454,462,466,470,482,498",
			"name": "Numbers n such that n-1, n and n+1 are all squarefree.",
			"comment": [
				"Equivalently, a positive integer n is comfortably squarefree if and only if n^3 - n is squarefree. The 'if' direction is obvious from the factorization n(n-1)(n+1), and the converse follows from the coprimality of n, n - 1 and n + 1.",
				"The asymptotic density of comfortably squarefree numbers is the product over all primes of 1 - 3/p^2, which is A206256 = 0.125486980905....",
				"See also comments in A007675."
			],
			"link": [
				"Harvey P. Dale, \u003ca href=\"/A228649/b228649.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"A. P. Goucher, \u003ca href=\"http://cp4space.wordpress.com/2013/08/29/comfortably-squarefree-numbers/\"\u003eComfortably squarefree numbers\u003c/a\u003e, Complex Projective 4-Space.",
				"Ewan Delanoy, \u003ca href=\"http://mathoverflow.net/questions/59741/are-there-infinitely-many-triples-of-consecutive-square-free-integers\"\u003eAre there infinitely many triples of consecutive square-free integers?\u003c/a\u003e, Math Overflow."
			],
			"formula": [
				"a(n) = A007675(n) + 1. - _Giovanni Resta_, Aug 29 2013"
			],
			"maple": [
				"with(numtheory):",
				"a := n -\u003e `if`(issqrfree(n-1) and issqrfree(n) and issqrfree(n+1), n, NULL);",
				"seq(a(n), n = 1..500); # _Peter Luschny_, Jan 18 2014"
			],
			"mathematica": [
				"Select[Range[500], (SquareFreeQ[# - 1] \u0026\u0026 SquareFreeQ[#] \u0026\u0026 SquareFreeQ[# + 1]) \u0026] (* _Adam P. Goucher_ *)",
				"Select[Range[2, 500, 2], (MoebiusMu[# - 1] MoebiusMu[#] MoebiusMu[# + 1]) != 0 \u0026] (* _Alonso del Arte_, Jan 16 2014 *)",
				"Flatten[Position[Partition[Boole[SquareFreeQ/@Range[500]],3,1],{1,1,1}]]+1 (* _Harvey P. Dale_, Jan 14 2015 *)",
				"SequencePosition[Table[If[SquareFreeQ[n],1,0],{n,500}],{1,1,1}][[All,1]]+1 (* Requires Mathematica version 10 or later *) (* _Harvey P. Dale_, Dec 02 2018 *)"
			],
			"program": [
				"(PARI) is(n)=issquarefree(n-1)\u0026\u0026issquarefree(n)\u0026\u0026issquarefree(n+1) \\\\ _Charles R Greathouse IV_, Aug 29 2013"
			],
			"keyword": "easy,nonn",
			"offset": "1,1",
			"author": "_Adam P. Goucher_, Aug 29 2013",
			"references": 4,
			"revision": 32,
			"time": "2018-12-02T13:19:30-05:00",
			"created": "2013-08-31T01:58:08-04:00"
		}
	]
}