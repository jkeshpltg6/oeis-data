{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A331020",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 331020,
			"data": "1,2,3,4,5,6,7,8,9,11,14,15,18,21,27,28,29,30,46,61,91,121,180,184,185,186,188,189,214,216,217,257,258,775,832,1217,1225,1227,1269,1270,1846,1847,2682,2683,2684,2685,2686,2688",
			"name": "Values k for successive maximal records of the function A defined as A(prime(k)) = log(prime(k)) - prime(k)/Pi(prime(k)) where Pi(prime(k)) is number of primes \u003c= prime(k).",
			"comment": [
				"This sequence is finite and complete.",
				"Chebyshev 1852, goes on to conclude that if we put Pi(x) = x/(log(x) - A(x)) has a limit as x -\u003e +infinity, then a limit must be 1, not 1.08366 (A228211), as Legendre incorrectly conjectured in 1808.",
				"R. Farhadian \u0026 R. Jakimczuk 2018 prove again that the function A tends to 1 when n tends to infinity.",
				"A(prime(2688)) = A(24137) = -24137/2688 + log(24137) = 1.11196252139...",
				"A(n) \u003c= -(24137/2688) + log(24137) for all positive integers n."
			],
			"link": [
				"P. L. Chebyshev, \u003ca href=\"http://sites.mathdoc.fr/JMPA/PDF/JMPA_1852_1_17_A18_0.pdf\"\u003eSur la totalité des nombres premiers inférieurs à une limite donnée\u003c/a\u003e, J. math. pures appl. 17, 1852 (in French).",
				"R. Farhadian \u0026 R. Jakimczuk, \u003ca href=\"https://doi.org/10.7546/nntdm.2018.24.3.84-91\"\u003eOne more disproof for the Legendre’s conjecture regarding the prime counting function Pi[x)\u003c/a\u003e, Notes on Number Theory and Discrete Mathematics, Vol. 24, 2018, No. 3, 84-91.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/LegendresConstant.html\"\u003eLegendre's Constant\u003c/a\u003e"
			],
			"example": [
				"   n | a(n) | A(prime(a(n)))",
				"  ---+------+---------------",
				"   1 |    1 | -1.306852819",
				"   2 |    2 | -0.401387711",
				"   3 |    3 | -0.057228754",
				"   4 |    4 |  0.195910149",
				"   5 |    5 |  0.197895272",
				"   6 |    6 |  0.398282690",
				"   7 |    7 |  0.404641915",
				"   8 |    8 |  0.569438979",
				"   9 |    9 |  0.579938660",
				"  10 |   11 |  0.615805386"
			],
			"mathematica": [
				"max = -2; aa = {}; Do[kk = Log[Prime[n]] - Prime[n]/PrimePi[Prime[n]];",
				"If[kk \u003e max, max = kk; AppendTo[aa, n]], {n, 1, 2700}]; aa"
			],
			"xref": [
				"Cf. A000720, A228211."
			],
			"keyword": "nonn,fini,full",
			"offset": "1,2",
			"author": "_Artur Jasinski_, Jan 07 2020",
			"references": 0,
			"revision": 32,
			"time": "2020-02-04T15:27:19-05:00",
			"created": "2020-02-04T15:27:19-05:00"
		}
	]
}