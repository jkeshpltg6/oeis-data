{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A122856",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 122856,
			"data": "1,2,1,0,0,2,2,0,2,2,1,0,0,2,0,0,3,2,0,0,0,4,2,0,2,0,2,0,0,2,0,0,1,2,2,0,0,2,2,0,2,4,1,0,0,2,0,0,2,2,0,0,0,0,2,0,4,2,0,0,0,4,0,0,2,2,3,0,0,0,2,0,2,4,0,0,0,2,0,0,1,2,0,0,0,2,4,0,0,2,2,0,0,2,0,0,4,2,2,0,0,4,0,0,2",
			"name": "Expansion of f(x, x^5)^2 in powers of x where f(, ) is Ramanujan's general theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A122856/b122856.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of (chi(x) * psi(-x^3))^2 in powers of x where chi(), psi() are Ramanujan theta functions.",
				"Expansion of q^(-2/3) * (eta(q^2)^2 * eta(q^3) * eta(q^12) / (eta(q) * eta(q^4) * eta(q^6)))^2 in powers of q.",
				"Euler transform of period 12 sequence [2, -2, 0, 0, 2, -2, 2, 0, 0, -2, 2, -2, ...].",
				"a(4*n + 3) = a(8*n + 4) = 0. a(n) = A002654(3*n + 2) = A035154(3*n + 2) = A113446(3*n + 2). a(2*n) = A122865(n). a(4*n + 1) = 2 * A121444(n). a(4*n + 2) = A122856(n).",
				"a(n) = (-1)^n * A258278(n). Convolution square of A089801."
			],
			"example": [
				"G.f. = 1 + 2*x + x^2 + 2*x^5 + 2*x^6 + 2*x^8 + 2*x^9 + x^10 + 2*x^13 + ...",
				"G.f. = q^2 + 2*q^5 + q^8 + 2*q^17 + 2*q^20 + 2*q^26 + 2*q^29 + q^32 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 0, 0, With[ {m = 3 n + 2}, Sum[ KroneckerSymbol[ -4, d], {d, Divisors@m}]]]; (* _Michael Somos_, Nov 14 2011 *)",
				"QP = QPochhammer; s = (QP[q^2]^2*QP[q^3]*(QP[q^12]/(QP[q]*QP[q^4]*QP[q^6]) ))^2 + O[q]^105; CoefficientList[s, q] (* _Jean-François Alcover_, Nov 30 2015, adapted from PARI *)",
				"a[ n_] := SeriesCoefficient[ (EllipticTheta[ 3, 0, x^(1/3)] - EllipticTheta[ 3, 0, x^3])^2 / (4 x^(2/3)), {x, 0, n}]; (* _Michael Somos_, Jan 19 2017 *)",
				"a[ n_] := SeriesCoefficient[ (QPochhammer[ -x, x^2] EllipticTheta[ 2, Pi/4, x^(3/2)])^2 / (2 x^(3/4)), {x, 0, n}]; (* _Michael Somos_, Jan 19 2017 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, 0, n = 3*n+2; sumdiv(n, d, (d%4==1) - (d%4==3)))};",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( (eta(x^2 + A)^2 * eta(x^3 + A) * eta(x^12 + A) / (eta(x + A) * eta(x^4 + A) * eta(x^6 + A)))^2, n))};"
			],
			"xref": [
				"Cf. A002654, A035154, A089801, A113446, A121444, A122865, A258278."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Michael Somos_, Sep 14 2006",
			"references": 33,
			"revision": 20,
			"time": "2021-03-12T22:24:44-05:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}