{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A345330",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 345330,
			"data": "9,21,25,27,33,45,49,57,63,65,69,77,81,93,99,105,117,121,125,129,133,141,145,147,161,165,169,171,177,185,189,201,207,209,213,217,225,231,237,243,245,249,253,261,265,273,279,285,289,297,301,305,309,321,325",
			"name": "Composite numbers such that m^(2^v(k-1)+1) == -m (mod k) has only one solution m == 0 (mod k), where v(k) = A007814(k) is the 2-adic valuation of k.",
			"comment": [
				"For primes p, m^(2^v(p-1)+1) == -m (mod p) has only one solution m == 0 (mod p). This sequence gives that composite numbers that satisfy this condition.",
				"All terms are odd since for even k, m == -1 (mod k) is a solution.",
				"Odd composite k is a term if and only if v(p-1) \u003c= v(k-1) for all prime factors p of k. Proof: Let k = (p_1)^(e_1)*(p_2)^(e_2)*...*(p_r)^(e_r) be an odd number. m^(2^v(k-1)+1) == -m (mod k) has only one solution if and only if m^(2^v(k-1)+1) == -m (mod (p_i)^(e_i)) has only one solution for 1 \u003c= i \u003c= r, or equivalently, m^(2^v(k-1)) == -1 (mod (p_i)^(e_i)) has no solution for 1 \u003c= i \u003c= r, or v(p_i-1) \u003c= v(k-1) for 1 \u003c= i \u003c= r.",
				"All prime powers of the form p^e for odd prime p and e \u003e= 2 are terms. All Carmichael numbers (A002997) are also terms: if k is a Carmichael number, then p-1 | k-1 for all prime factors p."
			],
			"link": [
				"Jianing Song, \u003ca href=\"/A345330/b345330.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"225 = 3^2 * 5^2 is a term since v(3-1) = 1 \u003c= v(225-1) = 7, v(5-1) = 2 \u003c= v(225-1) = 7. Also, the equation m^(2^v(225-1)+1) == -m (mod 225) has a unique solution m == 0 (mod 225).",
				"1885 = 5 * 13 * 29 is a term since v(5-1) = v(13-1) = v(29-1) = 2 \u003c= v(1885-1) = 2. Also, the equation m^(2^v(1885-1)+1) == -m (mod 1885) has a unique solution m == 0 (mod 1885)."
			],
			"program": [
				"(PARI) isA345330(n) = if(!isprime(n) \u0026\u0026 n\u003e1 \u0026\u0026 n%2, my(f=factor(n), w=omega(n)); for(i=1, w, if(valuation(f[i,1]-1,2) \u003e valuation(n-1,2), return(0))); 1, 0)"
			],
			"xref": [
				"Cf. A007814, A002997.",
				"Complement of A345331 with respect to A071904."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Jianing Song_, Jun 14 2021",
			"references": 2,
			"revision": 16,
			"time": "2021-06-14T14:21:08-04:00",
			"created": "2021-06-14T10:01:01-04:00"
		}
	]
}