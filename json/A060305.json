{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A060305",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 60305,
			"data": "3,8,20,16,10,28,36,18,48,14,30,76,40,88,32,108,58,60,136,70,148,78,168,44,196,50,208,72,108,76,256,130,276,46,148,50,316,328,336,348,178,90,190,388,396,22,42,448,456,114,52,238,240,250,516,176,268,270,556",
			"name": "Pisano periods for primes: period of Fibonacci numbers mod prime(n).",
			"comment": [
				"Assuming Wall's conjecture (which is still open) allows one to calculate A001175(m) when m is a prime power since for any k \u003e= 1: A001175(prime(n)^k) = a(n)*prime(n)^(k-1). For example: A001175(2^k) = 3*2^(k-1) = A007283(k-1)."
			],
			"link": [
				"T. D. Noe and Alois P. Heinz, \u003ca href=\"/A060305/b060305.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (first 1000 terms from T. D. Noe)",
				"B. Avila and T. Khovanova, \u003ca href=\"http://arxiv.org/abs/1403.4614\"\u003eFree Fibonacci Sequences\u003c/a\u003e, arXiv preprint arXiv:1403.4614 [math.NT], 2014 and \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Avila/avila4.html\"\u003eJ. Int. Seq. 17 (2014), # 14.8.5\u003c/a\u003e.",
				"A. Elsenhans, J. Jahnel, \u003ca href=\"http://www.uni-math.gwdg.de/tschinkel/gauss/Fibon.pdf\"\u003eThe Fibonacci sequence modulo p^2 -- An investigation by computer for p \u003c 10^14\u003c/a\u003e, (2004).",
				"D. D. Wall, \u003ca href=\"http://www.jstor.org/stable/2309169\"\u003eFibonacci series modulo m\u003c/a\u003e, Amer. Math. Monthly, 67 (1960), 525-532.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Pisano_period\"\u003ePisano period\u003c/a\u003e."
			],
			"formula": [
				"a(n) = A001175(prime(n)). - _Jonathan Sondow_, Dec 09 2017",
				"a(n) = (3 - L(p))/2 * (p - L(p)) / A296240(n) for n \u003e= 4, where p = prime(n) and L(p) = Legendre(p|5); so a(n) \u003c= p-1 if p == +- 1 mod 5, and a(n) \u003c= 2*p+2 if p == +- 2 mod 5. See Wall's Theorems 6 and 7. - _Jonathan Sondow_, Dec 10 2017"
			],
			"maple": [
				"a:= proc(n) option remember; local F, k, p;",
				"      F:=[1,1]; p:=ithprime(n);",
				"      for k while F\u003c\u003e[0,1] do",
				"        F:=[F[2], irem(F[1]+F[2],p)]",
				"      od: k",
				"    end:",
				"seq(a(n), n=1..70);  # _Alois P. Heinz_, Oct 16 2015"
			],
			"mathematica": [
				"Table[p=Prime[n]; a={1,0}; a0=a; k=0; While[k++; s=Mod[Plus@@a,p];a=RotateLeft[a]; a[[2]]=s; a!=a0]; k, {n,100}] (* _T. D. Noe_, Jun 12 2006 *)"
			],
			"program": [
				"(PARI) for(n=1,100,s=1; while(sum(i=n,n+s,abs(fibonacci(i)%prime(n)-fibonacci(i+s)%prime(n)))+sum(i=n+1,n+1+s,abs(fibonacci(i)%prime(n)-fibonacci(i+s)%prime(n)))\u003e0,s++); print1(s,\",\"))"
			],
			"xref": [
				"Cf. A001175, A000961, A071774, A003147, A296240."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "Louis Mello (mellols(AT)aol.com), Mar 26 2001",
			"ext": [
				"Corrected by _Benoit Cloitre_, Jun 04 2002",
				"Name clarified by _Jonathan Sondow_, Dec 09 2017"
			],
			"references": 13,
			"revision": 41,
			"time": "2019-05-20T14:17:03-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}