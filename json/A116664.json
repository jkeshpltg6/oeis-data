{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A116664",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 116664,
			"data": "1,0,1,1,0,1,1,1,0,1,1,2,0,2,1,1,1,4,0,2,2,2,3,4,0,1,3,4,3,0,3,7,1,1,5,4,6,0,4,10,2,2,6,7,9,0,7,12,5,3,7,13,11,0,1,8,18,7,5,0,11,15,18,1,1,10,25,11,8,0,13,23,24,2,2,15,32,16,13,0,16,33,32,5,3,18,43,24,19,0,23,40",
			"name": "Triangle read by rows: T(n,k) is the number of partitions of n into odd parts and having exactly k parts that appear exactly once (n\u003e=0, k\u003e=0).",
			"comment": [
				"Row n contains 1+floor(sqrt(n)) terms (at the end of certain rows there is an extra 0). Row sums yield A000009. T(n,0)=A097242(n). Sum(k*T(n,k), k\u003e=0)=A116665(n)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A116664/b116664.txt\"\u003eTable of n, a(n) for n = 0..600, flattened\u003c/a\u003e"
			],
			"formula": [
				"G.f.: product(1+tx^(2j-1)+x^(4j-2)/(1-x^(2j-1)), j=1..infinity)."
			],
			"example": [
				"T(10,2) = 3 because the only partitions of 10 into odd parts and having exactly 2 parts that appear only once are [9,1],[7,3] and [5,3,1,1].",
				"Triangle starts:",
				"1;",
				"0, 1;",
				"1, 0;",
				"1, 1;",
				"1, 0, 1;",
				"1, 2, 0;",
				"2, 1, 1;",
				"1, 4, 0;"
			],
			"maple": [
				"g:=product(1+t*x^(2*j-1)+x^(2*(2*j-1))/(1-x^(2*j-1)),j=1..30): gser:=simplify(series(g,x=0,30)): P[0]:=1: for n from 1 to 25 do P[n]:=coeff(gser,x^n) od: for n from 0 to 25 do seq(coeff(P[n],t,j),j=0..floor(sqrt(n))) od; # yields sequence in triangular form, with one extra 0 in some rows",
				"# second Maple program:",
				"b:= proc(n, i) option remember; `if`(n=0, 1, `if`(i\u003c1, 0,",
				"       expand(add(b(n-i*j, i-2)*`if`(j=1, x, 1), j=0..n/i))))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, k), k=0..floor(sqrt(n))))",
				"        (b(n, n-irem(n+1, 2))):",
				"seq(T(n), n=0..25);  # _Alois P. Heinz_, Mar 16 2014"
			],
			"mathematica": [
				"b[n_, i_] :=  b[n, i] = If[n == 0, 1, If[i\u003c1, 0, Expand[Sum[b[n-i*j, i-2]*If[j == 1, x, 1], {j, 0, n/i}]]]]; T[n_] := Function[{p}, Table[Coefficient[p, x, k], {k, 0, Floor[Sqrt[n]]}]][b[n, n-Mod[n+1, 2]]]; Table[T[n], {n, 0, 25}] // Flatten (* _Jean-François Alcover_, May 13 2015, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A000009, A097242, A116665."
			],
			"keyword": "nonn,tabf",
			"offset": "0,12",
			"author": "_Emeric Deutsch_, Feb 22 2006",
			"references": 2,
			"revision": 9,
			"time": "2015-05-13T05:10:18-04:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}