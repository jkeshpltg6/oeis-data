{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A227113",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 227113,
			"data": "1,2,4,3,6,5,10,7,14,8,12,9,15,11,22,13,26,16,18,17,34,19,38,20,24,21,27,23,46,25,30,28,32,29,58,31,62,33,36,35,40,37,74,39,42,41,82,43,86,44,48,45,50,47,94,49,56,51,54,52,60,53,106,55,65,57,63",
			"name": "Lexicographically earliest permutation of the natural numbers such that all pairs of even- and odd-indexed terms have a common divisor \u003e 1.",
			"comment": [
				"a(2*n) = smallest number not occurring earlier;",
				"a(2*n+1) = smallest number having with a(2*n) a common divisor greater than 1 and not occurring earlier;",
				"A227288(n) = gcd(a(n), a(n+1))."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A227113/b227113.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"example": [
				".   n | a(2n) a(2n+1) | GCD |         not occurring after step n",
				".  ---+---------------+-----+-------------------------------------------",
				".   0 |    _      1   |   _ |  {2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,..}",
				".   1 |    2      4   |   2 |  {3,5,6,7,8,9,10,11,12,13,14,15,16,17,..}",
				".   2 |    3      6   |   3 |  {5,7,8,9,10,11,12,13,14,15,16,17,18,19,..}",
				".   3 |    5     10   |   5 |  {7,8,9,11,12,13,14,15,16,17,18,19,20,..}",
				".   4 |    7     14   |   7 |  {8,9,11,12,13,15,16,17,18,19,20,21,22,..}",
				".   5 |    8     12   |   4 |  {9,11,13,15,16,17,18,19,20,21,22,23,24..}",
				".   6 |    9     15   |   3 |  {11,13,16,17,18,19,20,21,22,23,24,25,..}",
				".   7 |   11     22   |  11 |  {13,16,17,18,19,20,21,23,24,25,26,27,..}",
				".   8 |   13     26   |  11 |  {16,17,18,19,20,21,23,24,25,27,28,29,..}",
				".   9 |   16     18   |   2 |  {17,19,20,21,23,24,25,27,28,29,30,31,..} ."
			],
			"program": [
				"(Haskell)",
				"import Data.List (delete)",
				"a227113 n = a227113_list !! (n-1)",
				"a227113_list = 1 : f [2..] where",
				"   f (x:xs) = x : y : f (delete y xs)",
				"     where y : _ = filter ((\u003e 1) . (gcd x)) xs"
			],
			"xref": [
				"Cf. A227114 (inverse).",
				"Cf. A101369, A184992."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Reinhard Zumkeller_, Jul 01 2013",
			"ext": [
				"Thanks to _Zak Seidov_ (who suggested more elaboration) from _Reinhard Zumkeller_, Jul 05 2013"
			],
			"references": 4,
			"revision": 11,
			"time": "2017-08-20T23:19:31-04:00",
			"created": "2013-07-01T16:07:59-04:00"
		}
	]
}