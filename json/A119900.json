{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A119900",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 119900,
			"data": "1,0,2,0,1,3,0,0,4,4,0,0,1,10,5,0,0,0,6,20,6,0,0,0,1,21,35,7,0,0,0,0,8,56,56,8,0,0,0,0,1,36,126,84,9,0,0,0,0,0,10,120,252,120,10,0,0,0,0,0,1,55,330,462,165,11,0,0,0,0,0,0,12,220,792,792,220,12,0,0,0,0,0,0,1,78",
			"name": "Triangle read by rows: T(n,k) is the number of binary words of length n with k strictly increasing runs, for 0\u003c=k\u003c=n.",
			"comment": [
				"Sum of entries in row n is 2^n (A000079). Sum of entries in column k is A001906(k+1) (the even indexed Fibonacci numbers). Row n contains 1+floor(n/2) nonzero terms. Sum_{k=0..n} k*T(n,k) = (3n+1)*2^(n-2) = A066373(n+1) for n\u003e=1.",
				"Triangle T(n,k), 0\u003c=k\u003c=n, read by rows, given by [0,1/2,-1/2,0,0,0,0,0, 0,...] DELTA [2,-1/2,1/2,0,0,0,0,0,0,...] where DELTA is the operator defined in A084938. - _Philippe Deléham_, Dec 02 2008",
				"From R. Bagula's comment in A053122 (cf. Damianou link), the columns of this array give the coefficients (mod sign) of the characteristic polynomials for the Cartan matrix of the root system A_n. - _Tom Copeland_, Oct 11 2014",
				"Odd rows contain the Pascal triangle numbers A091042. See A034867 and A034839 for some relations to tan(x). - _Tom Copeland_, Oct 15 2014"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A119900/b119900.txt\"\u003eTable of n, a(n) for the first 50 rows, flattened\u003c/a\u003e",
				"A. Collins et al., \u003ca href=\"https://www.fq.math.ca/Papers1/51-2/CollinsDedricksonWang.pdf\"\u003eBinary words, n-color compositions and bisection of the Fibonacci numbers\u003c/a\u003e, Fib. Quarterly, 51 (2013), 130-136.",
				"P. Damianou, \u003ca href=\"http://arxiv.org/abs/1110.6620\"\u003eOn the characteristic polynomials of Cartan matrices and Chebyshev polynomials\u003c/a\u003e, arXiv:1110.6620 [math.RT], 2014.",
				"R. Zielinski, \u003ca href=\"https://arxiv.org/abs/1608.04006\"\u003eInduction and Analogy in a Problem of Finite Sums\u003c/a\u003e arXiv:1608.04006 [math.GM], 2016."
			],
			"formula": [
				"T(n,k) = binomial(n+1,2k-n).",
				"G.f.: 1/(1 - 2*t*z - t*(1-t)*z^2).",
				"T(n,k) = A034867(n,n-k)",
				"From _Tom Copeland_, Sep 30 2011: (Start)",
				"With K(x,t) = 1/{d/dx{x/[t-1+1/(1-x)]}} = [t-1+1/(1-x)]^2/{t-[x/(1-x)]^2}, the g.f. of A119900 = K(x*t,t)-t+1.",
				"From formulas in A134264: K(x,t)d/dx is a generator for A001263. A refinement of A119900 to partition polynomials is given by umbralizing",
				"  K(x,t) roughly as K(h.x,h_0) and precisely as in A134264 as",
				"  W(x)= 1/{d/dx[f(x)]}=1/{d/dx[x/h(x)]}. (End)",
				"T(n,k) = 2*T(n-1,k-1) + T(n-2,k-1) - T(n-2,k-2). - _Philippe Deléham_, Oct 02 2011",
				"From _Tom Copeland_, Dec 07 2015: (Start)",
				"An alternate o.g.f. is (1/(x*t)) {-1 + 1 / [1 - (1/t)[x*t/(1-x*t)]^2]} = Sum {n\u003e0} x^(2(n-1)+1) t^(n-1) / (1-t*x)^(2n) = x + 2t x^2 + (t+3t^2) x^3 + ... .",
				"The n-th diagonal has elements binomial(2n+1+k,k), starting with k=0 for the first non-vanishing element, with o.g.f. (1-x)^(-2(n+1)). The first few subdiagonals are shifted versions of A000292, A000389, and A000580. Cf. A049310.",
				"See A034867 for the matrix representation for the infinitesimal generator K(x,t) d/dx for the Narayana polynomials (End)",
				"From _Peter Bala_, Aug 17 2016: (Start)",
				"Let S(k,n) = Sum_{i = 1..n} i^k. Calculations in Zielinski 2016 suggest the following identity holds involving the p-th row elements of this triangle:",
				"Sum_{k = 0..p} T(p,k)*S(2*k + 1,n) = (n*(n + 1)/2)^(p+1).",
				"For example, for row 6 we find S(7,n) + 21*S(9,n) + 35*S(11,n) + 7*S(13,n) = (n*(n + 1)/2)^7.",
				"There appears to be a similar result for the even power sums S(2*k,n) involving A207543. (End)"
			],
			"example": [
				"The binary word 1/0/01/01/1/1/01 has 7 strictly increasing runs.",
				"T(5,3)=6 because we have 0/01/01, 01/0/01, 01/01/0, 01/1/01, 01/01/1 and 1/01/01 (the runs are separated by /).",
				"Triangle starts:",
				"1;",
				"0,2;",
				"0,1,3;",
				"0,0,4,4;",
				"0,0,1,10,5;",
				"0,0,0,6,20,6;"
			],
			"maple": [
				"T:=(n,k)-\u003ebinomial(n+1,2*k-n): for n from 0 to 12 do seq(T(n,k),k=0..n) od; # yields sequence in triangular form"
			],
			"mathematica": [
				"Table[Binomial[n + 1, 2 k - n], {n, 0, 12}, {k, 0, n}] // Flatten (* _Michael De Vlieger_, Aug 21 2016 *)"
			],
			"program": [
				"(PARI) for(n=0,10, for(k=0,n, print1(binomial(n+1, 2*k-n), \", \"))) \\\\ _G. C. Greubel_, Oct 22 2017",
				"(MAGMA) /* triangle */ [[Binomial(n+1, 2*k-n): k in [0..n]]: n in [0..10]]; // _G. C. Greubel_, Oct 22 2017"
			],
			"xref": [
				"Cf. A000079, A001906, A066373, A034867.",
				"Cf. A098158. - _Philippe Deléham_, Dec 02 2008",
				"Cf. A000292, A000389, A000580, A001263, A049310, A053122, A134264, A207543."
			],
			"keyword": "nonn,tabl",
			"offset": "0,3",
			"author": "_Emeric Deutsch_, May 27 2006",
			"ext": [
				"Keyword tabl added by _Philippe Deléham_, Jan 26 2010"
			],
			"references": 15,
			"revision": 67,
			"time": "2019-09-30T06:24:36-04:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}