{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000358",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 358,
			"data": "1,2,2,3,3,5,5,8,10,15,19,31,41,64,94,143,211,329,493,766,1170,1811,2787,4341,6713,10462,16274,25415,39651,62075,97109,152288,238838,375167,589527,927555,1459961,2300348,3626242,5721045,9030451,14264309,22542397,35646312,56393862,89264835,141358275",
			"name": "Number of binary necklaces of length n with no subsequence 00, excluding the necklace \"0\".",
			"comment": [
				"a(n) is also the number of inequivalent compositions of n into parts 1 and 2 where two compositions are considered to be equivalent if one is a cyclic rotation of the other.  a(6)=5 because we have: 2+2+2, 2+2+1+1, 2+1+2+1, 2+1+1+1+1, 1+1+1+1+1+1. - _Geoffrey Critzer_, Feb 01 2014",
				"Moebius transform is A006206. - _Michael Somos_, Jun 02 2019"
			],
			"reference": [
				"Miklos Bona, editor, Handbook of Enumerative Combinatorics, CRC Press, 2015, page 499.",
				"T. Helleseth and A. Kholosha, Bent functions and their connections to combinatorics, in Surveys in Combinatorics 2013, edited by Simon R. Blackburn, Stefanie Gerke, Mark Wildon, Camb. Univ. Press, 2013."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A000358/b000358.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Ricardo Gómez Aíza, \u003ca href=\"https://arxiv.org/abs/2009.02669\"\u003eSymbolic dynamical scales: modes, orbitals, and transversals\u003c/a\u003e, arXiv:2009.02669 [math.DS], 2020.",
				"A. R. Ashrafi, J. Azarija, K. Fathalikhani, S. Klavzar, and Marko Petkovsek, \u003ca href=\"http://www.fmf.uni-lj.si/~klavzar/preprints/Fib-Luc-orbits-August-11-2014.pdf\"\u003eOrbits of Fibonacci and Lucas cubes, dihedral transformations, and asymmetric strings\u003c/a\u003e, 2014.",
				"A. R. Ashrafi, J. Azarija, K. Fathalikhani, S. Klavzar, and Marko Petkovsek, \u003ca href=\"https://doi.org/10.1007/s00026-016-0318-9\"\u003eVertex and Edge Orbits of Fibonacci and Lucas Cubes\u003c/a\u003e, Ann. Comb. 20 (2016), 209-229.",
				"M. Assis, J. L. Jacobsen, I. Jensen, J.-M. Maillard, and B. M. McCoy, \u003ca href=\"http://arxiv.org/abs/1406.5566\"\u003eIntegrability vs non-integrability: Hard hexagons and hard squares compared\u003c/a\u003e, arXiv preprint 1406.5566 [math-ph], 2014.",
				"M. Assis, J. L. Jacobsen, I. Jensen, J.-M. Maillard, and B. M. McCoy, \u003ca href=\"https://doi.org/10.1088/1751-8113/47/44/445001\"\u003eIntegrability versus non-integrability: Hard hexagons and hard squares compared\u003c/a\u003e, J. Phys. A: Math. Theor. 47 (2014) 445001 (53pp.).",
				"Daryl DeFord, \u003ca href=\"https://www.fq.math.ca/Papers1/52-5/DeFord.pdf\"\u003eEnumerating distinct chessboard tilings\u003c/a\u003e, Fibonacci Quart. 52 (2014), 102-116; see formula (5.3) in Theorem 5.2, p. 111.",
				"P. Flajolet and M. Soria, \u003ca href=\"http://algo.inria.fr/flajolet/Publications/cycle2.ps.gz\"\u003eThe Cycle Construction\u003c/a\u003e, SIAM J. Discr. Math., vol. 4 (1), 1991, pp. 58-60.",
				"P. Flajolet and M. Soria, \u003ca href=\"http://dx.doi.org/10.1137/0404006\"\u003eThe Cycle Construction\u003c/a\u003e, SIAM J. Discr. Math., vol. 4 (1), 1991, pp. 58-60.",
				"Benjamin Hackl and Helmut Prodinger, \u003ca href=\"https://arxiv.org/abs/1801.09934\"\u003eThe Necklace Process: A Generating Function Approach\u003c/a\u003e, arXiv:1801.09934 [math.PR], 2018. [The paper mentions this sequence, but the authors mean sequence A032190(n) = a(n) - 1.]",
				"Benjamin Hackl and Helmut Prodinger, \u003ca href=\"https://doi.org/10.1016/j.spl.2018.06.010\"\u003eThe Necklace Process: A Generating Function Approach\u003c/a\u003e, Statistics and Probability Letters 142 (2018), 57-61. [The paper mentions this sequence, but the authors mean sequence A032190(n) = a(n) - 1.]",
				"P. Hadjicostas, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL19/Hadjicostas/hadji2.html\"\u003eCyclic compositions of a positive integer with parts avoiding an arithmetic sequence\u003c/a\u003e, J. Integer Sequences 19 (2016), #16.8.2.",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=119\"\u003eEncyclopedia of Combinatorial Structures 119\u003c/a\u003e",
				"Tom Roby, \u003ca href=\"http://www.math.uwaterloo.ca/~opecheni/alcoveRobyDACtalk.pdf\"\u003eDynamical algebraic combinatorics and homomesy: An action-packed introduction\u003c/a\u003e, AlCoVE: an Algebraic Combinatorics Virtual Expedition (2020).",
				"F. Ruskey, \u003ca href=\"http://combos.org/necklace\"\u003eNecklaces, Lyndon words, De Bruijn sequences, etc.\u003c/a\u003e",
				"F. Ruskey, \u003ca href=\"/A000011/a000011.pdf\"\u003eNecklaces, Lyndon words, De Bruijn sequences, etc.\u003c/a\u003e [Cached copy, with permission, pdf format only]",
				"C. J. Turner, A. A. Michailidis, D. A. Abanin, M. Serbyn, and Z. Papić, \u003ca href=\"https://arxiv.org/abs/1806.10933\"\u003eQuantum scarred eigenstates in a Rydberg atom chain: entanglement, breakdown of thermalization, and stability to perturbations\u003c/a\u003e, arXiv:1806.10933 [cond-mat.quant-gas], 2018.",
				"L. Zhang and P. Hadjicostas, \u003ca href=\"http://appliedprobability.org/data/files/TMS%20articles/40_2_3.pdf\"\u003eOn sequences of independent Bernoulli trials avoiding the pattern '11..1'\u003c/a\u003e, Math. Scientist, 40 (2015), 89-96.",
				"\u003ca href=\"/index/Ne#necklaces\"\u003eIndex entries for sequences related to necklaces\u003c/a\u003e"
			],
			"formula": [
				"a(n) = (1/n) * Sum_{ d divides n } totient(n/d) [ Fib(d-1)+Fib(d+1) ].",
				"G.f.: Sum_{k\u003e=1} phi(k)/k * log( 1/(1-B(x^k)) ) where B(x)=x*(1+x). - _Joerg Arndt_, Aug 06 2012",
				"a(n) ~ ((1+sqrt(5))/2)^n / n. - _Vaclav Kotesovec_, Sep 12 2014",
				"a(n) = Sum_{0 \u003c= i \u003c= ceiling((n-1)/2)} [ (1/(n - i)) * Sum_{d|gcd(i, n-i)} phi(d) * binomial((n - i)/d, i/d) ]. (This is DeFord's formula for the number of distinct Lucas tilings of a 1 X n bracelet up to symmetry, even though in the paper he refers to sequence A032192(n) = a(n) - 1.) - _Petros Hadjicostas_, Jun 07 2019"
			],
			"example": [
				"G.f. = x + 2*x^2 + 2*x^3 + 3*x^4 + 3*x^5 + 5*x^6 + 5*x^7 + 8*x^8 + 10*x^9 + ... - _Michael Somos_, Jun 02 2019",
				"Binary necklaces are: 1; 01, 11; 011, 111; 0101, 0111, 1111; 01010, 01011, 01111. - _Michael Somos_, Jun 02 2019"
			],
			"maple": [
				"A000358 := proc(n) local sum; sum := 0; for d in divisors(n) do sum := sum + phi(n/d)*(fibonacci(d+1)+fibonacci(d-1)) od; RETURN(sum/n); end;",
				"with(combstruct); spec := {A=Union(zero,Cycle(one),Cycle(Prod(zero,Sequence(one,card\u003e0)))),one=Atom,zero=Atom}; seq(count([A,spec,unlabeled],size=i),i=1..30);"
			],
			"mathematica": [
				"nn=48;Drop[Map[Total,Transpose[Map[PadRight[#,nn]\u0026,Table[ CoefficientList[ Series[CycleIndex[CyclicGroup[n],s]/.Table[s[i]-\u003ex^i+x^(2i),{i,1,n}],{x,0,nn}],x],{n,0,nn}]]]],1] (* _Geoffrey Critzer_, Feb 01 2014 *)",
				"max = 50; B[x_] := x*(1+x); A = Sum[EulerPhi[k]/k*Log[1/(1-B[x^k])], {k, 1, max}]/x + O[x]^max; CoefficientList[A, x] (* _Jean-François Alcover_, Feb 08 2016, after _Joerg Arndt_ *)",
				"Table[1/n * Sum[EulerPhi[n/d] Total@ Map[Fibonacci, d + # \u0026 /@ {-1, 1}], {d, Divisors@ n}], {n, 47}] (* _Michael De Vlieger_, Dec 28 2016 *)",
				"a[ n_] := If[ n \u003c 1, 0, DivisorSum[n, EulerPhi[n/#] LucasL[#] \u0026]/n]; (* _Michael Somos_, Jun 02 2019 *)"
			],
			"program": [
				"(PARI)",
				"N=66;  x='x+O('x^N);",
				"B(x)=x*(1+x);",
				"A=sum(k=1, N, eulerphi(k)/k*log(1/(1-B(x^k))));",
				"Vec(A)",
				"/* _Joerg Arndt_, Aug 06 2012 */",
				"(PARI)  {a(n) = if( n\u003c1, 0, sumdiv(n, d, eulerphi(n/d) * (fibonacci(d+1) + fibonacci(d-1)))/n)}; /* _Michael Somos_, Jun 02 2019 */"
			],
			"xref": [
				"Cf. A006206, A032190, A093305, A280218, A280218.",
				"Column k=0 of A320341."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Frank Ruskey_",
			"references": 20,
			"revision": 139,
			"time": "2020-12-09T19:55:19-05:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}