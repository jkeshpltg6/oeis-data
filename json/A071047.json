{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A071047",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 71047,
			"data": "1,3,3,6,5,8,9,10,10,14,13,15,16,18,18,21,20,23,24,25,25,29,28,30,31,33,33,36,35,38,39,40,40,44,43,45,46,48,48,51,50,53,54,55,55,59,58,60,61,63,63,66,65,68,69,70,70,74,73,75,76,78,78,81,80,83,84,85",
			"name": "Number of 1's in n-th row of triangle in A071031, cellular automaton \"rule 62\".",
			"comment": [
				"Number of ON cells at generation n of 1-D CA defined by Rule 62. - _N. J. A. Sloane_, Aug 09 2014"
			],
			"reference": [
				"S. Wolfram, A New Kind of Science, Wolfram Media, 2002; Chapter 3."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A071047/b071047.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/A071047/a071047.png\"\u003eIllustration of first 20 generations of Rule 62\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"http://arxiv.org/abs/1503.01168\"\u003eOn the Number of ON Cells in Cellular Automata\u003c/a\u003e, arXiv:1503.01168, 2015",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_07\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,0,1,1,0,0,-1)."
			],
			"formula": [
				"Comments from _N. J. A. Sloane_, Aug 11 2014. (Start) As one can see from the illustration, there is a pattern that repeats every three steps on the left and every 12 steps on the right.",
				"More precisely, let L(n) denote the number of N cells in the part of the diagram to the left of the central line. Then L(3t+1)=2t+1, L(3t+2)=L(3t+3)=2t+2, which is 0, 1, 2, 2, 3, 4, 4, 5, 6, 6, 7, 8, 8, ... (essentially A004523). This has g.f. (x+x^2)/((1-x)(1-x^3)).",
				"Let R(n) denote the number of ON cells on the central axis and to the right of this axis. Then R(0) through R(5) = [1, 2, 1, 4, 2, 4] and thereafter R(12k+6) through R(12k+17) = 7k + [5, 5, 4, 8, 6, 7, 8, 9, 8, 11, 9, 11] for k = 0,1,2,... The g.f. for R(n) is (1+2*x+x^2+3*x^3-x^4+x^5)/(1-x^3)(1-x^4)).",
				"Combining these, we find that a(n) = L(n) + R(n) has the generating function that is given in the next line. (End)",
				"G.f.: (2*x^5 + x^4 + 5*x^3 + 3*x^2 + 3*x + 1)/((1-x^3)*(1-x^4)). - _Hans Havermann_, May 26 2002",
				"a(n+7) = a(n+4)+a(n+3)-a(n) with initial terms 1, 3, 3, 6, 5, 8, 9. - _N. J. A. Sloane_, Jan 31 2015"
			],
			"mathematica": [
				"CoefficientList[Series[(2 x^5 + x^4 + 5 x^3 + 3 x^2 + 3 x + 1)/((1 - x^3) (1 - x^4)), {x, 0, 80}], x] (* _Vincenzo Librandi_, Aug 10 2014 *)"
			],
			"xref": [
				"Cf. A071031, A004523, A071046 (count 0's)."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Hans Havermann_, May 26 2002",
			"references": 5,
			"revision": 44,
			"time": "2020-09-12T02:27:51-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}