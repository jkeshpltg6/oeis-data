{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A238874",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 238874,
			"data": "1,0,1,1,1,2,3,4,5,7,10,14,19,25,33,44,59,79,105,138,180,234,304,395,513,665,859,1105,1416,1809,2306,2935,3731,4737,6005,7598,9593,12085,15192,19061,23875,29861,37299,46532,57978,72145,89650,111243,137837,170545,210725,260034,320492,394557,485213,596074,731508",
			"name": "Strictly superdiagonal compositions: compositions (p1, p2, p3, ...) of n such that pi \u003e i.",
			"link": [
				"Joerg Arndt and Alois P. Heinz, \u003ca href=\"/A238874/b238874.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e"
			],
			"formula": [
				"G.f.: Sum_{n\u003e=0} q^(n*(n+3)/2) / (1-q)^n. - _Joerg Arndt_, Mar 30 2014"
			],
			"example": [
				"The a(13) = 25 such composition of 13 are:",
				"01:  [ 2 3 8 ]",
				"02:  [ 2 4 7 ]",
				"03:  [ 2 5 6 ]",
				"04:  [ 2 6 5 ]",
				"05:  [ 2 7 4 ]",
				"06:  [ 2 11 ]",
				"07:  [ 3 3 7 ]",
				"08:  [ 3 4 6 ]",
				"09:  [ 3 5 5 ]",
				"10:  [ 3 6 4 ]",
				"11:  [ 3 10 ]",
				"12:  [ 4 3 6 ]",
				"13:  [ 4 4 5 ]",
				"14:  [ 4 5 4 ]",
				"15:  [ 4 9 ]",
				"16:  [ 5 3 5 ]",
				"17:  [ 5 4 4 ]",
				"18:  [ 5 8 ]",
				"19:  [ 6 3 4 ]",
				"20:  [ 6 7 ]",
				"21:  [ 7 6 ]",
				"22:  [ 8 5 ]",
				"23:  [ 9 4 ]",
				"24:  [ 10 3 ]",
				"25:  [ 13 ]"
			],
			"maple": [
				"b:= proc(n, i) option remember; `if`(n=0, 1,",
				"      add(b(n-j, i+1), j=i..n))",
				"    end:",
				"a:= n-\u003e b(n, 2):",
				"seq(a(n), n=0..60);  # _Alois P. Heinz_, Mar 24 2014"
			],
			"mathematica": [
				"b[n_, i_] := b[n, i] = If[n == 0, 1, Sum[b[n-j, i+1], {j, i, n}]]; a[n_] := b[n, 2]; Table[a[n], {n, 0, 60}] (* _Jean-François Alcover_, Mar 23 2015, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(PARI) N=66; q='q+O('q^N);",
				"gf=sum(n=0,N, q^(n*(n+3)/2) / (1-q)^n );",
				"v=Vec(gf) \\\\ _Joerg Arndt_, Mar 30 2014"
			],
			"xref": [
				"Cf. A219282 (superdiagonal compositions), A238873 (superdiagonal partitions), A238394 (strictly superdiagonal partitions), A025147 (strictly superdiagonal partitions into distinct parts).",
				"Cf. A238875 (subdiagonal partitions), A008930 (subdiagonal compositions), A010054 (subdiagonal partitions into distinct parts).",
				"Cf. A238859 (compositions of n with subdiagonal growth), A238876 (partitions with subdiagonal growth), A001227 (partitions into distinct parts with subdiagonal growth).",
				"Cf. A238860 (partitions with superdiagonal growth), A238861 (compositions with superdiagonal growth), A000009 (partitions into distinct parts have superdiagonal growth by definition)."
			],
			"keyword": "nonn",
			"offset": "0,6",
			"author": "_Joerg Arndt_, Mar 23 2014",
			"references": 7,
			"revision": 26,
			"time": "2021-10-05T13:00:13-04:00",
			"created": "2014-03-24T09:30:26-04:00"
		}
	]
}