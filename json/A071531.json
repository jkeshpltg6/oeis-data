{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A071531",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 71531,
			"data": "10,10,5,8,9,4,4,5,1,5,4,6,7,4,3,7,4,4,1,5,3,6,6,4,6,5,5,4,1,6,2,2,3,4,5,3,4,5,1,5,3,3,4,2,5,2,2,2,1,2,2,2,4,2,5,4,6,3,1,5,6,3,2,4,6,3,9,3,1,2,6,3,3,4,8,4,2,3,1,4,5,5,2,4,3,3,6,3,1,5,5,3,3,2,7,2,2,2,1,1,1,1,1,1",
			"name": "Smallest exponent r such that n^r contains at least one zero digit (in base 10).",
			"comment": [
				"For all n, a(n) is at most 40000, as shown below. Is 10 an upper bound?",
				"If n has d digits, the numbers n, n^2, ..., n^k have a total of about N = k*(k+1)*d/2, and if these were chosen randomly the probability of having no zeros would be (9/10)^N. The expected number of d-digit numbers n with f(n)\u003ek would be 9*10^(d-1)*(9/10)^N. If k \u003e= 7, (9/10)^(k*(k+1)/2)*10 \u003c 1 so we would expect heuristically that there should be only finitely many n with f(n) \u003e 7. - _Robert Israel_, Jan 15 2015",
				"The similar definition using \"...exactly one digit 0...\" would be ill-defined for all multiples of 100 and others (1001, ...). - _M. F. Hasler_, Jun 25 2018",
				"When r=40000, one of the last five digits of n^r is always 0. Working modulo 10^5, we have 2^r=9736 and 5^r=90625, and both of these are idempotent; also, if gcd(n,10)=1, then n^r=1, and if 10|n, then n^r=0. Therefore the last five digits of n^r are always either 00000, 00001, 09736, or 90625. In particular, a(n) \u003c= 40000. - _Mikhail Lavrov_, Nov 18 2021"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A071531/b071531.txt\"\u003eTable of n, a(n) for n = 2..10000\u003c/a\u003e",
				"OEIS Wiki, \u003ca href=\"/wiki/Zeroless powers\"\u003eZeroless powers\u003c/a\u003e (2014)."
			],
			"formula": [
				"a(n) \u003e= 1 with equality iff n is in A011540 \\ {0} = {10, 20, ..., 100, 101, ...}. - _M. F. Hasler_, Jun 23 2018"
			],
			"example": [
				"a(4)=5 because 4^1=4, 4^2=16, 4^3=64, 4^4=256, 4^5=1024 (has zero digit)."
			],
			"maple": [
				"f:= proc(n) local j;",
				"for j from 1 do if has(convert(n^j,base,10),0) then return j fi od:",
				"end proc:",
				"seq(f(n),n=2..100); # _Robert Israel_, Jan 15 2015"
			],
			"mathematica": [
				"zd[n_]:=Module[{r=1},While[DigitCount[n^r,10,0]==0,r++];r]; Array[zd,110,2] (* _Harvey P. Dale_, Apr 15 2012 *)"
			],
			"program": [
				"(Python)",
				"def a(n):",
				"    r, p = 1, n",
				"    while 1:",
				"        if \"0\" in str(p):",
				"            return r",
				"        r += 1",
				"        p *= n",
				"[a(n) for n in range(2, 100)] # Tim Peters, May 19 2005",
				"(PARI) A071531(n)=for(k=1, oo, vecmin(digits(n^k))||return(k)) \\\\ _M. F. Hasler_, Jun 23 2018"
			],
			"xref": [
				"Cf. A305941 for the actual powers n^k.",
				"Cf. A007377, A030700, A030701, A008839, A030702, A030703, A030704, A030705, A030706, A195944: decimal expansion of k^n contains no zeros, k = 2, 3, 4, ...",
				"Cf. A305932, A305933, A305924, ..., A305929: row n = {k: x^k has n 0's}, x = 2, 3, ..., 9.",
				"Cf. A305942, ..., A305947, A305938, A305939: #{k: x^k has n 0's}, x = 2, 3, ..., 9.",
				"Cf. A306112, ..., A306119: largest k: x^k has n 0's; x = 2, 3, ..., 9."
			],
			"keyword": "base,nonn",
			"offset": "2,1",
			"author": "Paul Stoeber (paul.stoeber(AT)stud.tu-ilmenau.de), Jun 02 2002",
			"references": 10,
			"revision": 39,
			"time": "2021-12-15T02:36:44-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}