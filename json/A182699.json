{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A182699",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 182699,
			"data": "0,0,0,0,1,1,4,4,10,12,22,27,47,56,89,112,164,205,294,364,505,630,845,1052,1393,1719,2235,2762,3533,4343,5506,6730,8443,10296,12786,15531,19161,23161,28374,34201,41621,49975,60513,72385,87200,103999,124670,148209",
			"name": "Number of emergent parts in all partitions of n.",
			"comment": [
				"Here the \"emergent parts\" of the partitions of n are defined to be the parts (with multiplicity) of all the partitions that do not contain \"1\" as a part, removed by one copy of the smallest part of every partition. Note that these parts are located in the head of the last section of the set of partitions of n.",
				"Also, here the \"filler parts\" of the partitions of n are defined to be the parts of the last section of the set of partitions of n that are not the emergent parts.",
				"For n \u003e= 4, length of row n of A183152. - _Omar E. Pol_, Aug 08 2011",
				"Also total number of parts of the regions that do not contain 1 as a part in the last section of the set of partitions of n (cf. A083751, A187219). - _Omar E. Pol_, Mar 04 2012"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A182699/b182699.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Omar E. Pol, \u003ca href=\"http://www.polprimos.com/imagenespub/polpanxt.jpg\"\u003eIllustration: How to build the last section of the set of partitions (copy, paste and fill)\u003c/a\u003e",
				"Omar E. Pol, \u003ca href=\"http://www.polprimos.com/imagenespub/polpa2dt.jpg\"\u003eIllustration of the shell model of partitions (2D view)\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A138135(n) - A002865(n), n \u003e= 1.",
				"From _Omar E. Pol_, Oct 21 2011: (Start)",
				"a(n) = A006128(n) - A006128(n-1) - A000041(n), n \u003e= 1.",
				"a(n) = A138137(n) - A000041(n), n \u003e= 1. (End)",
				"a(n) = A076276(n) - A006128(n-1), n \u003e= 1. - _Omar E. Pol_, Oct 30 2011"
			],
			"example": [
				"For n = 6 the partitions of 6 contain four \"emergent\" parts: (3), (4), (2), (2), so a(6) = 4. See below the location of the emergent parts.",
				"6",
				"(3) + 3",
				"(4) + 2",
				"(2) + (2) + 2",
				"5 + 1",
				"3 + 2 + 1",
				"4 + 1 + 1",
				"2 + 2 + 1 + 1",
				"3 + 1 + 1 + 1",
				"2 + 1 + 1 + 1 + 1",
				"1 + 1 + 1 + 1 + 1 + 1",
				"For a(10) = 22 see the link for the location of the 22 \"emergent parts\" (colored yellow and green) and the location of the 42 \"filler parts\" (colored blue) in the last section of the set of partitions of 10."
			],
			"maple": [
				"b:= proc(n, i) option remember; local t, h;",
				"      if n\u003c0 then [0, 0, 0]",
				"    elif n=0 then [0, 1, 0]",
				"    elif i\u003c2 then [0, 0, 0]",
				"    else t:= b(n, i-1); h:= b(n-i, i);",
				"         [t[1]+h[1]+h[2], t[2], t[3]+h[3]+h[1]]",
				"      fi",
				"    end:",
				"a:= n-\u003e b(n, n)[3]:",
				"seq (a(n), n=0..50);  # _Alois P. Heinz_, Oct 21 2011"
			],
			"mathematica": [
				"b[n_, i_] := b[n, i] = Module[{t, h}, Which[n\u003c0, {0, 0, 0}, n == 0, {0, 1, 0}, i\u003c2 , {0, 0, 0}, True, t = b[n, i-1]; h = b[n-i, i]; Join [t[[1]] + h[[1]] + h[[2]], t[[2]], t[[3]] + h[[3]] + h[[1]] ]]]; a[n_] := b[n, n][[3]]; Table[a[n], {n, 0, 50}] (* _Jean-François Alcover_, Jun 18 2015, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A000041, A002865, A135010, A138121, A138135, A182700, A182709, A182740."
			],
			"keyword": "nonn,easy",
			"offset": "0,7",
			"author": "_Omar E. Pol_, Nov 29 2010",
			"references": 26,
			"revision": 45,
			"time": "2015-06-18T08:51:53-04:00",
			"created": "2010-11-27T17:24:30-05:00"
		}
	]
}