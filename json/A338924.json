{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A338924",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 338924,
			"data": "1,2,4,6,8,9,10,12,14,15,16,18,20,11,21,22,24,19,25,26,27,28,30,32,33,34,35,36,38,39,40,42,44,45,46,48,49,50,51,52,54,55,56,57,58,60,62,63,64,65,66,68,69,70,72,74,75,76,77,78,80,81,82,84,85,86,87,88,90,91,92,93,94,95,96,98,99,100",
			"name": "Every prime term k of the sequence is the cumulative sum of the prime digits used so far (the digits of k are included in the sum).",
			"comment": [
				"This is the lexicographically earliest sequence of distinct positive terms with this property. The prime digits are 2, 3, 5 and 7."
			],
			"link": [
				"Carole Dubois, \u003ca href=\"/A338924/b338924.txt\"\u003eTable of n, a(n) for n = 1..9999\u003c/a\u003e"
			],
			"example": [
				"a(1) = 1 as 1 (a nonprime term) is the smallest term not yet present in the sequence that doesn't lead to a contradiction;",
				"a(2) = 2 as 2 (a prime term) is the sum of all prime digits used so far;",
				"a(3) = 4 (a nonprime term) as a(3) = 3 (a prime) would be a contradiction and a(3) = 4 doesn't lead to a contradiction;",
				"...",
				"a(14) = 11 (a prime term) as 11 is the sum of all prime digits used so far (2 + 2 + 5 + 2);",
				"a(15) = 21 (a nonprime term) as 21 is the smallest term not yet present in the sequence that doesn't lead to a contradiction;",
				"...",
				"a(18) = 19 (a prime term) as 19 is the sum of all prime digits used so far (2 + 2 + 5 + 2 + 2 + 2 + 2 + 2); etc."
			],
			"program": [
				"(PARI) v=[1]; w=[]; n=1; p=2; while(n\u003c100, for(q=vecsum(w), p,if(isprime(q), m=[]; m=select(isprime,digits(q)); c=0; if(vecsum(w)+vecsum(m)==q\u0026\u0026!vecsearch(vecsort(v), q), v=concat(v, q); w=concat(w, m); c++; break))); if(c==0, while(isprime(p), p++); w=concat(w, select(isprime,digits(p))); v=concat(v, p); p++); n++); v \\\\ _Derek Orr_, Nov 17 2020"
			],
			"xref": [
				"Cf. A338922, A338923 and A338925 (variants on the same idea)."
			],
			"keyword": "base,nonn",
			"offset": "1,2",
			"author": "_Eric Angelini_ and _Carole Dubois_, Nov 15 2020",
			"references": 6,
			"revision": 11,
			"time": "2020-11-23T06:02:36-05:00",
			"created": "2020-11-17T14:20:26-05:00"
		}
	]
}