{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A027960",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 27960,
			"data": "1,1,3,1,1,3,4,4,1,1,3,4,7,8,5,1,1,3,4,7,11,15,13,6,1,1,3,4,7,11,18,26,28,19,7,1,1,3,4,7,11,18,29,44,54,47,26,8,1,1,3,4,7,11,18,29,47,73,98,101,73,34,9,1,1,3,4,7,11,18,29,47,76,120,171,199,174,107,43,10,1",
			"name": "'Lucas array': triangular array T read by rows.",
			"comment": [
				"The k-th row contains 2k+1 numbers.",
				"Columns in the right half consist of convolutions of the Lucas numbers with the natural numbers.",
				"T(n,k) = number of strings s(0),...,s(n) such that s(n)=n-k. s(0) in {0,1,2}, s(1)=1 if s(0) in {1,2}, s(1) in {0,1,2} if s(0)=0 and for 1 \u003c= i \u003c= n, s(i) = s(i-1)+d, with d in {0,2} if s(i)=2i, in {0,1,2} if s(i)=2i-1, in {0,1} if 0 \u003c= s(i) \u003c= 2i-2."
			],
			"link": [
				"Nathaniel Johnston, \u003ca href=\"/A027960/b027960.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = Lucas(k+1) for k \u003c= n, otherwise the (2n-k)th coefficient of the power series for (1+2*x)/{(1-x-x^2)*(1-x)^(k-n)}.",
				"Recurrence: T(n, 0)=T(n, 2n)=1 for n \u003e= 0; T(n, 1)=3 for n \u003e= 1; and for n \u003e= 2, T(n, k) = T(n-1, k-2) + T(n-1, k-1) for 2 \u003c= k \u003c= 2*n-1."
			],
			"example": [
				"                           1",
				"                       1,  3,  1",
				"                   1,  3,  4,  4,  1",
				"               1,  3,  4,  7,  8,  5,   1",
				"           1,  3,  4,  7, 11, 15, 13,   6,  1",
				"        1, 3,  4,  7, 11, 18, 26, 28,  19,  7,  1",
				"     1, 3, 4,  7, 11, 18, 29, 44, 54,  47, 26,  8, 1",
				"  1, 3, 4, 7, 11, 18, 29, 47, 73, 98, 101, 73, 34, 9, 1"
			],
			"maple": [
				"T:=proc(n,k)option remember:if(k=0 or k=2*n)then return 1:elif(k=1)then return 3:else return T(n-1,k-2) + T(n-1,k-1):fi:end:",
				"for n from 0 to 6 do for k from 0 to 2*n do print(T(n,k));od:od: # _Nathaniel Johnston_, Apr 18 2011"
			],
			"mathematica": [
				"t[_, 0] = 1; t[_, 1] = 3; t[n_, k_] /; (k == 2*n) = 1; t[n_, k_] := t[n, k] = t[n-1, k-2] + t[n-1, k-1]; Table[t[n, k], {n, 0, 8}, {k, 0, 2*n}] // Flatten (* _Jean-François Alcover_, Dec 27 2013 *)"
			],
			"program": [
				"(PARI) T(r,n)=if(r\u003c0||n\u003e2*r,return(0)); if(n==0||n==2*r,return(1)); if(n==1,3,T(r-1,n-1)+T(r-1,n-2)) /* _Ralf Stephan_, May 04 2005 */",
				"(Sage)",
				"def T(n, k):",
				"    if (k\u003c0 or k\u003e2*n): return 0",
				"    elif (k==0 or k==2*n): return 1",
				"    elif (k==1): return 3",
				"    else: return T(n-1, k-2) + T(n-1, k-1)",
				"[[T(n, k) for k in (0..2*n)] for n in (0..12)] # _G. C. Greubel_, Jun 01 2019"
			],
			"xref": [
				"Central column is the Lucas numbers without initial 2, cf. A000204. Row sums are A036563. Columns in the right half include A027961, A027962, A027963, A027964, A053298. Bisection triangles are in A026998 and A027011."
			],
			"keyword": "nonn,easy,tabf",
			"offset": "0,3",
			"author": "_Clark Kimberling_",
			"ext": [
				"Edited by _Ralf Stephan_, May 04 2005"
			],
			"references": 33,
			"revision": 26,
			"time": "2019-06-02T00:50:54-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}