{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A347056",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 347056,
			"data": "1,3,4,6,16,10,10,40,50,20,15,80,150,120,35,21,140,350,420,245,56,28,224,700,1120,980,448,84,36,336,1260,2520,2940,2016,756,120,45,480,2100,5040,7350,6720,3780,1200,165,55,660,3300,9240,16170,18480,13860,6600,1815,220",
			"name": "Triangle read by rows: T(n,k) = (n+1)*(n+2)*(k+3)*binomial(n,k)/6, 0 \u003c= k \u003c= n.",
			"comment": [
				"This triangle is T[3] in the sequence (T[p]) of triangles defined by: T[p](n,k) = (k+p)*(n+p-1)!/(k!*(n-k)!*p!) and T[0](0,0)=1.",
				"Riordan triangle (1/(1-x)^3, x/(1-x)) with column k scaled with A000292(k+1) = binomial(k+3, 3), for k \u003e= 0. - _Wolfdieter Lang_, Sep 30 2021"
			],
			"link": [
				"Luc Rousseau, \u003ca href=\"/A347056/a347056.svg\"\u003eIllustration: the A347056 triangle in a sequence of contiguous triangles.\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = (n+1)*(n+2)*(k+3)*binomial(n,k)/6.",
				"G.f. column k: x^k*binomial(k+3, 3)/(1 - x)^(k+3), for k \u003e= 0. - _Wolfdieter Lang_, Sep 30 2021"
			],
			"example": [
				"T(6,2) = (6+1)*(6+2)*(2+3)*binomial(6,2)/6 = 7*8*5*15/6 = 700.",
				"The triangle T begins:",
				"n \\ k  0   1    2     3     4     5     6     7     8    9  10 ...",
				"0:     1",
				"1:     3   4",
				"2:     6  16   10",
				"3:    10  40   50    20",
				"4:    15  80  150   120    35",
				"5:    21 140  350   420   245    56",
				"6:    28 224  700  1120   980   448    84",
				"7:    36 336 1260  2520  2940  2016   756   120",
				"8:    45 480 2100  5040  7350  6720  3780  1200   165",
				"9:    55 660 3300  9240 16170 18480 13860  6600  1815  220",
				"10:   66 880 4950 15840 32340 44352 41580 26400 10890 2640 286",
				"... - _Wolfdieter Lang_, Sep 30 2021"
			],
			"program": [
				"(PARI)",
				"T(p,n,k)=if(n==0\u0026\u0026p==0,1,((k+p)*(n+p-1)!)/(k!*(n-k)!*p!))",
				"for(n=0,9,for(k=0,n,print1(T(3,n,k),\", \")))"
			],
			"xref": [
				"Cf. A097805 (p=0), A103406 (p=1), A124932 (essentially p=2).",
				"From _Wolfdieter Lang_, Sep 30 2021: (Start)",
				"Columns (with leading zeros): A000217(n+1), 4*A000294, 10*A000332(n+2), 20*A000389(n+2), 35*A000579(n+2), 56*A00580(n+2), 84*A000581(n+2), 120*A000582(n+2), ...",
				"Diagonals: A000292(k+1), A004320(k+1), 2*A006411(k+1), 10*A040977, ... (End)"
			],
			"keyword": "nonn,tabl,easy",
			"offset": "0,2",
			"author": "_Luc Rousseau_, Aug 14 2021",
			"references": 1,
			"revision": 10,
			"time": "2021-10-19T15:05:27-04:00",
			"created": "2021-10-19T15:05:27-04:00"
		}
	]
}