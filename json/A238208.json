{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A238208",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 238208,
			"data": "0,1,0,0,0,0,1,1,2,2,3,3,4,4,5,6,7,8,10,12,14,17,20,24,28,33,38,45,52,60,69,80,91,105,120,137,156,178,202,230,261,295,334,378,426,481,542,609,685,769,862,966,1082,1209,1351,1508,1681,1873,2086,2319,2578",
			"name": "The total number of 1's in all partitions of n into an odd number of distinct parts.",
			"comment": [
				"The g.f. for \"number of k's\" is (1/2)*(x^k/(1+x^k))*(Product_{n\u003e=1} 1 + x^n) + (1/2)*(x^k/(1-x^k))*(Product_{n\u003e=1} 1 - x^n).",
				"Or: the number of partitions of n-1 into an even number of distinct parts \u003e=2. - _R. J. Mathar_, May 11 2016"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A238208/b238208.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e (first 1001 terms from Andrew Howroyd)"
			],
			"formula": [
				"a(n) = Sum_{j=1..round(n/2)} A067661(n-(2*j-1)) - Sum_{j=1..floor(n/2)} A067659(n-2*j).",
				"G.f.: (1/2)*(x/(1+x))*(Product_{n\u003e=1} 1 + x^n) + (1/2)*(x/(1-x))*(Product_{n\u003e=1} 1 - x^n).",
				"a(n) ~ exp(Pi*sqrt(n/3)) / (16 * 3^(1/4) * n^(3/4)). - _Vaclav Kotesovec_, May 17 2020",
				"From _Peter Bala_, Feb 02 2021: (Start)",
				"a(n+1) = d(n) - ( d(n-1) + d(n-3) ) + ( d(n-4) + d(n-6) + d(n-8) ) - ( d(n-9) + d(n-11) + d(n-13) + d(n-15) ) + ( d(n-16) + d(n-18) + d(n-20) + d(n-22) + d(n-24) ) - ( d(n-25) + d(n-27) + d(n-29) + d(n-31) + d(n-33) + d(n-35) ) + ..., where d(n) = A000009(n) is the number of partitions of n into distinct parts, with the convention that d(n) = 0 for n \u003c 0.",
				"G.f.: x/(1 - x^2)*Sum_{n \u003e= 0} (-1)^n*x^((n^2+n+1-(-1)^n)/2)/Product_{k = 1..n} 1 - x^k.",
				"Alternative g.f.: ( Product_{k \u003e= 1} 1 + x^k ) * x*Sum_{n \u003e= 0} (-1)^n*x^(n^2)*(1 - x^(2*n+2))/(1 - x^2).",
				"Faster converging g.f. (conjecture): Sum_{n \u003e= 0} x^((n+1)*(2*n+1))/ Product_{k = 1..2*n} 1 - x^k. (End)"
			],
			"example": [
				"a(10) = 3 because the partitions in question are: 7+2+1, 6+3+1, 5+4+1."
			],
			"maple": [
				"A238208 := proc(n)",
				"    local a,L,Lset;",
				"    a := 0 ;",
				"    L := combinat[firstpart](n) ;",
				"    while true do",
				"        # check that parts are distinct",
				"        Lset := convert(L,set) ;",
				"        if nops(L) = nops(Lset) then",
				"            # check that number is odd",
				"            if type(nops(L),'odd') then",
				"                if 1 in Lset then",
				"                    a := a+1 ;",
				"                end if;",
				"            end if;",
				"        end if;",
				"        L := combinat[nextpart](L) ;",
				"        if L = FAIL then",
				"            return a;",
				"        end if;",
				"    end do:",
				"    a ;",
				"end proc: # _R. J. Mathar_, May 11 2016",
				"# second Maple program:",
				"b:= proc(n, i, t) option remember; `if`(n=0, t,",
				"     `if`(i\u003en, 0, b(n, i+1, t)+b(n-i, i+1, 1-t)))",
				"    end:",
				"a:= n-\u003e b(n-1, 2, 1):",
				"seq(a(n), n=0..100);  # _Alois P. Heinz_, May 01 2020"
			],
			"mathematica": [
				"b[n_, i_, t_] := b[n, i, t] = If[n == 0, t, If[i \u003e n, 0, b[n, i+1, t] + b[n-i, i+1, 1-t]]];",
				"a[n_] := b[n-1, 2, 1];",
				"a /@ Range[0, 100] (* _Jean-François Alcover_, May 17 2020, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(PARI) seq(n)={my(A=O(x^n)); Vec(x*(eta(x^2 + A)/(eta(x + A)*(1+x)) + eta(x + A)/(1-x))/2, -(n+1))} \\\\ _Andrew Howroyd_, May 01 2020"
			],
			"xref": [
				"Column k=1 of A238450.",
				"Cf. A067659, A067661, A133280."
			],
			"keyword": "nonn,easy",
			"offset": "0,9",
			"author": "_Mircea Merca_, Feb 20 2014",
			"ext": [
				"a(51)-a(60) from _R. J. Mathar_, May 11 2016"
			],
			"references": 2,
			"revision": 36,
			"time": "2021-03-19T08:48:30-04:00",
			"created": "2014-02-20T12:56:52-05:00"
		}
	]
}