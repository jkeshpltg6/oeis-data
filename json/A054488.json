{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A054488",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 54488,
			"data": "1,8,47,274,1597,9308,54251,316198,1842937,10741424,62605607,364892218,2126747701,12395593988,72246816227,421085303374,2454265004017,14304504720728,83372763320351,485932075201378,2832219687887917",
			"name": "Expansion of (1+2*x)/(1-6*x+x^2).",
			"comment": [
				"Bisection (even part) of Chebyshev sequence with Diophantine property.",
				"b(n)^2 - 8*a(n)^2 = 17, with the companion sequence b(n)= A077240(n).",
				"The odd part is A077413(n) with Diophantine companion A077239(n)."
			],
			"reference": [
				"A. H. Beiler, Recreations in the Theory of Numbers, Dover, N. Y., 1964, pp. 122-125, 194-196."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A054488/b054488.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"I. Adler, \u003ca href=\"http://www.fq.math.ca/Scanned/7-2/adler.pdf\"\u003eThree Diophantine equations - Part II\u003c/a\u003e, Fib. Quart., 7 (1969), pp. 181-193.",
				"E. I. Emerson, \u003ca href=\"http://www.fq.math.ca/Scanned/7-3/emerson.pdf\"\u003eRecurrent Sequences in the Equation DQ^2=R^2+N\u003c/a\u003e, Fib. Quart., 7 (1969), pp. 231-242.",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (6,-1)."
			],
			"formula": [
				"a(n) = 6*a(n-1) - a(n-2), a(0)=1, a(1)=8.",
				"a(n) = ((3 + 2*sqrt(2))^(n+1) - (3 - 2*sqrt(2))^(n+1) + 2*((3 + 2*sqrt(2))^n - (3 - 2*sqrt(2))^n))/(4*sqrt(2)).",
				"a(n) = S(n, 6) + 2*S(n-1, 6), with S(n, x) Chebyshev's polynomials of the second kind, A049310. S(n, 6) = A001109(n+1).",
				"a(n) = (-1)^n*Sum_{k = 0..n} A238731(n,k)*(-9)^k. - _Philippe Deléham_, Mar 05 2014",
				"a(n) = (Pell(2*n+2) + 2*Pell(2*n))/2 = (Pell-Lucas(2*n+1) + Pell(2*n))/2. - _G. C. Greubel_, Jan 19 2020",
				"E.g.f.: (1/4)*exp(3*x)*(4*cosh(2*sqrt(2)*x) + 5*sqrt(2)*sinh(2*sqrt(2)*x)). - _Stefano Spezia_, Jan 27 2020"
			],
			"example": [
				"8 = a(1) = sqrt((A077240(1)^2 - 17)/8) = sqrt((23^2 - 17)/8)= sqrt(64) = 8."
			],
			"maple": [
				"a[0]:=1: a[1]:=8: for n from 2 to 26 do a[n]:=6*a[n-1]-a[n-2] od: seq(a[n], n=0..20); # _Zerinvary Lajos_, Jul 26 2006"
			],
			"mathematica": [
				"LinearRecurrence[{6,-1},{1,8},30] (* _Harvey P. Dale_, Oct 09 2017 *)",
				"Table[(LucasL[2*n+1, 2] + Fibonacci[2*n, 2])/2, {n,0,30}] (* _G. C. Greubel_, Jan 19 2020 *)"
			],
			"program": [
				"(PARI) my(x='x+O('x^30)); Vec((1+2*x)/(1-6*x+x^2)) \\\\ _G. C. Greubel_, Jan 19 2020",
				"(PARI) apply( {A054488(n)=[1,8]*([0,-1;1,6]^n)[,1]}, [0..30]) \\\\ _M. F. Hasler_, Feb 27 2020",
				"(MAGMA) I:=[1,8]; [n le 2 select I[n] else 6*Self(n-1) - Self(n-2): n in [1..30]]; // _G. C. Greubel_, Jan 19 2020",
				"(MAGMA) R\u003cx\u003e:=PowerSeriesRing(Integers(), 21); Coefficients(R!( (1+2*x)/(1-6*x+x^2))); // _Marius A. Burtea_, Jan 20 2020",
				"(Sage) [(lucas_number2(2*n+1,2,-1) + lucas_number1(2*n,2,-1))/2 for n in (0..30)] # _G. C. Greubel_, Jan 19 2020",
				"(GAP) a:=[1,8];; for n in [3..30] do a[n]:=6*a[n-1]-a[n-2]; od; a; # _G. C. Greubel_, Jan 19 2020"
			],
			"xref": [
				"Cf. A000129, A002203, A002315, A038761, A077239, A077240, A077413.",
				"Cf. A077241 (even and odd parts)."
			],
			"keyword": "easy,nonn",
			"offset": "0,2",
			"author": "_Barry E. Williams_, May 04 2000",
			"ext": [
				"More terms from _James A. Sellers_, May 05 2000",
				"Chebyshev comments from _Wolfdieter Lang_, Nov 08 2002"
			],
			"references": 10,
			"revision": 43,
			"time": "2020-02-29T20:32:41-05:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}