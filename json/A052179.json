{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A052179",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 52179,
			"data": "1,4,1,17,8,1,76,50,12,1,354,288,99,16,1,1704,1605,700,164,20,1,8421,8824,4569,1376,245,24,1,42508,48286,28476,10318,2380,342,28,1,218318,264128,172508,72128,20180,3776,455,32,1,1137400,1447338",
			"name": "Triangle of numbers arising in enumeration of walks on cubic lattice.",
			"comment": [
				"Triangle T(n,k), 0 \u003c= k \u003c= n, read by rows given by: T(0,0)=1, T(n,k)=0 if k \u003c 0 or if k \u003e n, T(n,0) = 4*T(n-1,0) + T(n-1,1), T(n,k) = T(n-1,k-1) + 4*T(n-1,k) + T(n-1,k+1) for k \u003e= 1. - _Philippe Deléham_, Mar 27 2007",
				"Triangle read by rows: T(n,k) = number of lattice paths from (0,0) to (n,k) that do not go below the line y=0 and consist of steps U=(1,1), D=(1,-1) and four types of steps H=(1,0); example: T(3,1)=50 because we have UDU, UUD, 16 HHU paths, 16 HUH paths and 16 UHH paths. - _Philippe Deléham_, Sep 25 2007",
				"This triangle belongs to the family of triangles defined by: T(0,0)=1, T(n,k)=0 if k \u003c 0 or if k \u003e n, T(n,0) = x*T(n-1,0) + T(n-1,1), T(n,k) = T(n-1,k-1) + y*T(n-1,k) + T(n-1,k+1) for k \u003e= 1. Other triangles arise by choosing different values for (x,y): (0,0) -\u003e A053121; (0,1) -\u003e A089942; (0,2) -\u003e A126093; (0,3) -\u003e A126970; (1,0)-\u003e A061554; (1,1) -\u003e A064189; (1,2) -\u003e A039599; (1,3) -\u003e A110877; (1,4) -\u003e A124576; (2,0) -\u003e A126075; (2,1) -\u003e A038622; (2,2) -\u003e A039598; (2,3) -\u003e A124733; (2,4) -\u003e A124575; (3,0) -\u003e A126953; (3,1) -\u003e A126954; (3,2) -\u003e A111418; (3,3) -\u003e A091965; (3,4) -\u003e A124574; (4,3) -\u003e A126791; (4,4) -\u003e A052179; (4,5) -\u003e A126331; (5,5) -\u003e A125906. - _Philippe Deléham_, Sep 25 2007",
				"Riordan array ((1-4x-sqrt(1-8x+12x^2))/(2x^2), (1-4x-sqrt(1-8x+12x^2))/(2x)). Inverse of A159764. - _Paul Barry_, Apr 21 2009",
				"6^n = (n-th row terms) dot (first n+1 terms in (1,2,3,...)). Example: 6^3 =  216 = (76, 50, 12, 1) dot (1, 2, 3, 4) = (76 + 100 + 36 + 4) = 216. - _Gary W. Adamson_, Jun 15 2011",
				"A subset of the \"family of triangles\" (Deléham comment of Sep 25 2007) is the succession of binomial transforms beginning with triangle A053121, (0,0); giving -\u003e A064189, (1,1); -\u003e A039598, (2,2); -\u003e A091965, (3,3); -\u003e A052179, (4,4); -\u003e A125906, (5,5) -\u003e, etc.; generally the binomial transform of the triangle generated from (n,n) = that generated from ((n+1),(n+1)). - _Gary W. Adamson_, Aug 03 2011"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A052179/b052179.txt\"\u003eTable of n, a(n) for the first 100 rows, flattened\u003c/a\u003e",
				"Rigoberto Flórez, Leandro Junes, José L. Ramírez, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL21/Florez/florez4.html\"\u003eFurther Results on Paths in an n-Dimensional Cubic Lattice\u003c/a\u003e, Journal of Integer Sequences, Vol. 21 (2018), Article 18.1.2.",
				"R. K. Guy, Catwalks, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL3/GUY/catwalks.html\"\u003eSandsteps and Pascal Pyramids\u003c/a\u003e, J. Integer Seqs., Vol. 3 (2000), #00.1.6."
			],
			"formula": [
				"Sum_{k, k\u003e=0} T(m, k)*T(n, k) = T(m+n, 0) = A005572(m+n). - _Philippe Deléham_, Sep 15 2005",
				"n-th row = M^n * V, where M = the infinite tridiagonal matrix with all 1's in the super and subdiagonals and (4,4,4,...) in the main diagonal. E.g., Row 3 = (76, 50, 12, 1) since M^3 * V = [76, 50, 12, 1, 0, 0, 0, ...]. - _Gary W. Adamson_, Nov 04 2006",
				"Sum_{k=0..n} T(n,k) = A005573(n). - _Philippe Deléham_, Feb 04 2007",
				"Sum_{k=0..n} T(n,k)*(k+1) = 6^n. - _Philippe Deléham_, Mar 27 2007",
				"Sum_{k=0..n} T(n,k)*x^k = A033543(n), A064613(n), A005572(n), A005573(n) for x = -2, -1, 0, 1 respectively. - _Philippe Deléham_, Nov 28 2009",
				"As an infinite lower triangular matrix = the binomial transform of A091965 and 4th binomial transform of A053121. - _Gary W. Adamson_, Aug 03 2011"
			],
			"example": [
				"Triangle begins:",
				"    1;",
				"    4,   1;",
				"   17,   8,   1;",
				"   76,  50,  12,   1;",
				"  354, 288,  99,  16,   1;",
				"  ...",
				"Production matrix begins:",
				"  4, 1;",
				"  1, 4, 1;",
				"  0, 1, 4, 1;",
				"  0, 0, 1, 4, 1;",
				"  0, 0, 0, 1, 4, 1;",
				"  0, 0, 0, 0, 1, 4, 1;",
				"  0, 0, 0, 0, 0, 1, 4, 1;",
				"- _Philippe Deléham_, Nov 04 2011"
			],
			"maple": [
				"T:= proc(n, k) option remember; `if`(min(n, k)\u003c0, 0,",
				"     `if`(max(n, k)=0, 1, T(n-1, k-1)+4*T(n-1, k)+T(n-1, k+1)))",
				"    end:",
				"seq(seq(T(n,k), k=0..n), n=0..10);  # _Alois P. Heinz_, Oct 28 2021"
			],
			"mathematica": [
				"t[0, 0] = 1; t[n_, k_] /; k \u003c 0 || k \u003e n = 0; t[n_, 0] := t[n, 0] = 4*t[n-1, 0] + t[n-1, 1]; t[n_, k_] := t[n, k] = t[n-1, k-1] + 4*t[n-1, k] + t[n-1, k+1]; Flatten[ Table[t[n, k], {n, 0, 9}, {k, 0, n}]] (* _Jean-François Alcover_, Oct 10 2011, after _Philippe Deleham_ *)"
			],
			"xref": [
				"Cf. A039598, A053121, A064189, A091965."
			],
			"keyword": "nonn,walk,tabl,easy,nice",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Jan 26 2000",
			"references": 34,
			"revision": 57,
			"time": "2021-10-28T20:28:23-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}