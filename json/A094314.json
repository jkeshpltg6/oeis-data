{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A094314",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 94314,
			"data": "1,0,1,0,0,2,1,0,3,2,2,8,4,8,2,13,30,40,20,15,2,80,192,210,152,60,24,2,579,1344,1477,994,469,140,35,2,4738,10800,11672,7888,3660,1232,280,48,2,43387,97434,104256,70152,32958,11268,2856,504,63,2,439792,976000,1036050,695760,328920,115056,30300,6000,840,80,2",
			"name": "Triangle read by rows: T(n,k) = number of ways of seating n couples around a circular table so that exactly k married couples are adjacent (0 \u003c= k \u003c= n).",
			"comment": [
				"The men and women alternate."
			],
			"reference": [
				"I. Kaplansky and J. Riordan, The problème des ménages, Scripta Math. 12, (1946), 113-124. See Table 1.",
				"Tolman, L. Kirk, \"Extensions of derangements\",  Proceedings of the West Coast Conference on Combinatorics, Graph Theory and Computing, Humboldt State University, Arcata, California, September 5-7, 1979. Vol. 26. Utilitas Mathematica Pub., 1980. See Table I."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A094314/b094314.txt\"\u003eRows n = 0..140, flattened\u003c/a\u003e",
				"I. Kaplansky and J. Riordan, \u003ca href=\"/A000166/a000166_1.pdf\"\u003eThe problème des ménages\u003c/a\u003e, Scripta Math. 12, (1946), 113-124. [Scan of annotated copy]",
				"Anthony C. Robin, \u003ca href=\"http://www.jstor.org/stable/40378205\"\u003e90.72 Circular Wife Swapping\u003c/a\u003e, The Mathematical Gazette, Vol. 90, No. 519 (Nov., 2006), pp. 471-478.",
				"L. Takacs, \u003ca href=\"http://dx.doi.org/10.1016/S0012-365X(81)80024-6\"\u003eOn the probleme des menages\u003c/a\u003e, Discr. Math. 36 (3) (1981) 289-297, Table 1."
			],
			"formula": [
				"Sum_{k=0..n} T(n,k) = n!.",
				"T(n, k) = Sum_{j=0..n-k} (-1)^j*(2*n*(n-k-j)!/(2*n-k-j))*binomial(k+j, k) * binomial(2*n-k-j, k+j) for n \u003e 1, T(0, 0) = T(1, 1) = 1, and T(1, 0) = 0. - _G. C. Greubel_, May 15 2021"
			],
			"example": [
				"Triangle begins:",
				"     1;",
				"     0,     1;",
				"     0,     0,     2;",
				"     1,     0,     3,    2;",
				"     2,     8,     4,    8,    2;",
				"    13,    30,    40,   20,   15,    2;",
				"    80,   192,   210,  152,   60,   24,   2;",
				"   579,  1344,  1477,  994,  469,  140,  35,  2;",
				"  4738, 10800, 11672, 7888, 3660, 1232, 280, 48, 2;",
				"  ..."
			],
			"mathematica": [
				"T[n_, k_]:= If[n\u003c2, (1+(-1)^(n-k))/2, Sum[(-1)^j*(2*n*(n-k-j)!/(2*n-k-j))* Binomial[k+j, k]*Binomial[2*n-k-j, k+j], {j, 0, n-k}]];",
				"Table[T[n, k], {n,0,12}, {k,0,n}]//Flatten (* _G. C. Greubel_, May 15 2021 *)"
			],
			"program": [
				"(Sage)",
				"def A094314(n,k): return (1+(-1)^(n+k))/2 if (n\u003c2) else sum( (-1)^j*(2*n*factorial(n-k-j)/(2*n-k-j))*binomial(k+j, k)*binomial(2*n-k-j, k+j) for j in (0..n-k) )",
				"flatten([[A094314(n,k) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, May 15 2021"
			],
			"xref": [
				"Essentially a mirror image of A058087, which has much more information.",
				"Diagonals give A000179, A000425, A000033, A000159, A000181, etc."
			],
			"keyword": "nonn,tabl",
			"offset": "0,6",
			"author": "_N. J. A. Sloane_, based on a suggestion from _Anthony C Robin_, Jun 02 2004",
			"references": 3,
			"revision": 32,
			"time": "2021-05-17T04:33:27-04:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}