{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A092276",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 92276,
			"data": "1,2,1,7,4,1,30,18,6,1,143,88,33,8,1,728,455,182,52,10,1,3876,2448,1020,320,75,12,1,21318,13566,5814,1938,510,102,14,1,120175,76912,33649,11704,3325,760,133,16,1,690690,444015,197340,70840,21252,5313,1078,168,18,1",
			"name": "Triangle read by rows: T(n,k) is the number of noncrossing trees with root degree equal to k.",
			"comment": [
				"With offset 0, Riordan array (f(x), x*f(x)) where f(x) is the g.f. of A006013. - _Philippe Deléham_, Jan 23 2010"
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A092276/b092276.txt\"\u003eTable of n, a(n) for n = 1..1275\u003c/a\u003e",
				"Paul Barry, \u003ca href=\"https://arxiv.org/abs/2001.08799\"\u003eCharacterizations of the Borel triangle and Borel polynomials\u003c/a\u003e, arXiv:2001.08799 [math.CO], 2020.",
				"Paul Barry, \u003ca href=\"https://arxiv.org/abs/2011.13985\"\u003eThe second production matrix of a Riordan array\u003c/a\u003e, arXiv:2011.13985 [math.CO], 2020.",
				"P. Flajolet and M. Noy, \u003ca href=\"http://dx.doi.org/10.1016/S0012-365X(98)00372-0\"\u003eAnalytic combinatorics of non-crossing configurations\u003c/a\u003e, Discrete Math., 204, 203-229, 1999.",
				"M. Noy, \u003ca href=\"http://dx.doi.org/10.1016/S0012-365X(97)00121-0\"\u003eEnumeration of noncrossing trees on a circle\u003c/a\u003e, Discrete Math., 180, 301-313, 1998."
			],
			"formula": [
				"T(n, k) = 2*k*binomial(3n-k, n-k)/(3n-k).",
				"G.f. = 1/(1-t*z*g^2), where g := 2*sin(arcsin(3*sqrt(3*z)/2)/3)/sqrt(3*z) is the g.f. of the sequence A001764.",
				"T(n, k) = Sum_{j, j\u003e=1} j*T(n-1, k-2+j). - _Philippe Deléham_, Sep 14 2005",
				"With offset 0, T(n,k)= ((n+1)/(k+1))*binomial(3n-k+1, n-k). - _Philippe Deléham_, Jan 23 2010",
				"Let M = the production matrix",
				"2, 1",
				"3, 2, 1",
				"4, 3, 2, 1",
				"5, 4, 3, 2, 1",
				"...",
				"Top row of M^(n-1) generates n-th row terms of triangle A092276. Leftmost terms of each row = A006013 starting (1, 2, 7, 30, 143,...). - _Gary W. Adamson_, Jul 07 2011"
			],
			"example": [
				"Triangle begins:",
				"     1;",
				"     2,    1;",
				"     7,    4,    1;",
				"    30,   18,    6,   1;",
				"   143,   88,   33,   8,  1;",
				"   728,  455,  182,  52, 10,  1;",
				"  3876, 2448, 1020, 320, 75, 12, 1;",
				"  ...",
				"Top row of M^3 = (30, 18, 6, 1)"
			],
			"maple": [
				"T := proc(n,k) if k=n then 1 else 2*k*binomial(3*n-k,n-k)/(3*n-k) fi end: seq(seq(T(n,k),k=1..n),n=1..11);"
			],
			"mathematica": [
				"t[n_, n_] = 1; t[n_, k_] := 2*k*Binomial[3*n-k, n-k]/(3*n-k); Table[t[n, k], {n, 1, 10}, {k, 1, n}] // Flatten (* _Jean-François Alcover_, Nov 22 2012, after Maple *)"
			],
			"program": [
				"(PARI) T(n, k) = 2*k*binomial(3*n-k, n-k)/(3*n-k); \\\\ _Andrew Howroyd_, Nov 06 2017"
			],
			"xref": [
				"Row sums give sequence A001764.",
				"Columns 1..5 are A006013, A006629, A006630, A006631, A233657."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Emeric Deutsch_, Feb 24 2004",
			"references": 9,
			"revision": 29,
			"time": "2021-03-17T15:30:52-04:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}