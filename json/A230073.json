{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A230073",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 230073,
			"data": "-1,1,1,-6,1,1,-14,1,1,-28,70,-28,1,1,-44,166,-44,1,1,-60,134,-60,1,1,-90,911,-2092,911,-90,1,1,-120,1820,-8008,12870,-8008,1820,-120,1,1,-138,975,-1868,975,-138,1,1,-184,3740,-16136,25414,-16136,3740,-184,1,1,-230,7085,-67528,252242,-394404,252242,-67528,7085,-230,1,1,-248,3612,-16072,25670,-16072,3612,-248,1",
			"name": "Coefficients of the minimal polynomials of the algebraic numbers sqLhat(2*l) from A230072, l \u003e= 1, related to the square of all length in a regular (2*l)-gon inscribed in a circle of radius 1 length unit.",
			"comment": [
				"The length of row No. l of this table is delta(2*l) + 1 = A055034(2*l) + 1.",
				"sqLhat(2*l), l \u003e= 1, from A230072, an algebraic number (in fact integer) of degree delta(2*l) over the rationals, gives the square of the sum of the lengths ratios of all lines/R (also called chords/R) divided by (2*l)^2 in a regular (2*l)-gon inscribed in a circle of radius R. This number lives in the algebraic number field Q(rho(2*l)), with rho(2*l) = 2*cos(Pi/(2*l)) (see A187360 and the W. Lang link below for this number field).",
				"The minimal polynomial for sqLhat(2*l) = sum(A230072(l,m)* rho(2*l)^m, m=0..delta(2*l)), called here psqLhat(l, x), is computed from the conjugates rho(2*l)^{(j)}, j=0 ,..., delta(2*l)-1, with rho(2*l)^{(0)} = rho(2*l), by calculating the conjugates sqLhat(2*l)^{(j)}, j = 0, ..., delta(2*l)-1, which are polynomials in rho(2*l) =: z, with the usual rules for conjugation. All results have to be taken modulo the minimal polynomial C(2*l, z) of rho(2*l) (see A187360 table 2 and section 3 for C(n,x)), in order to obtain finally elements written in the power basis of the field Q(rho(2*l)). The conjugates rho(2*l)^{(j)} are just the delta(2*l) roots of C(2*l, z). Therefore, psqLhat(l, x) = product(x - substitute(rho(2*l) = z, sqLhat(2*l)^{(j)}), j = 0.. delta(2*l)-1) (mod C(2*l, z)).",
				"Thanks go to Seppo Mustonen who asked me to look into this matter. I thank him for sending the below given link to his work about the square of the sum of all lengths in an n-gon, called there L(n)^2. Here n is even (n=2*l) and sqLhat(2*l) =(L(n)^2)/n^2. The odd n case is obtained from A228780 as L(2*l+1)^2 = (2*l+1)^2*S2(2*l+1) (observing that all distinct line length come precisely n times in the regular n-gon if n is odd). His polynomials given in his eq. (6) (here for the n even case) are in general not monic, and not irreducible. Instead one should consider the minimal (monic, irreducible and integer) polynomials  PsqL(l, x) := (2*l)^(2*delta(2*l))* psqLhat(l, x/(2*l)^2), l \u003e= 1 (for n = 2*l).",
				"Mustonen's polynomials from his  eq. (6) for the even n case coincide with PsqL(l, x) precisely for l = 2^k, k\u003e=1, and for l = 1 (k=0) one has to take the negative. In all other cases the degrees do not fit (Mustonen's polynomials become reducible over the integers). His conjecture for the coefficients can then be rewritten as a conjecture for the present polynomials  psqLhat(2^k, x),  k \u003e= 0 (see the formula section).",
				"S. Mustonen also conjectured about the other zeros of his polynomials."
			],
			"link": [
				"Wolfdieter Lang, \u003ca href=\"http://arxiv.org/abs/1210.1018\"\u003eThe field Q(2cos(pi/n)), its Galois group and length ratios in the regular n-gon\u003c/a\u003e.",
				"Seppo Mustonen, \u003ca href=\"http://www.survo.fi/papers/Roots2013.pdf\"\u003e Lengths of edges and diagonals and sums of them in regular polygons as roots of algebraic equations\u003c/a\u003e."
			],
			"formula": [
				"a(l,m) = [x^m](psqLhat(l, x)), l \u003e=1, m = 0, ..., delta(2*l), with delta(2*l) = A055034(2*l), and the formula for psqLhat(2*l, x) is given in a comment above.",
				"Mustonen's conjecture (adapted, see a comment above) is: a(2^k,m) = ((-1)^m)*binomial(2*2^k,2*m), k \u003e= 1, and for k=0 a(1,0) = -1 and  a(1,1) = 1 is trivial."
			],
			"example": [
				"The table a(l,m) (n = 2*l) starts: (row length A055034(2*l))",
				"l,    n\\m 0    1     2      3      4      5     6     7  8",
				"1,   2:  -1    1",
				"2,   4:   1   -6     1",
				"3,   6:   1  -14     1",
				"4,   8:   1  -28    70    -28      1",
				"5,  10:   1  -44   166    -44      1",
				"6,  12:   1  -60   134    -60      1",
				"7,  14:   1  -90   911  -2092    911    -90     1",
				"8,  16:   1 -120  1820  -8008  12870  -8008  1820  -120  1",
				"9,  18:   1 -138   975  -1868    975   -138     1",
				"10, 20:   1 -184  3740 -16136  25414 -16136  3740  -184  1",
				"...",
				"11, 22:  1 -230 7085 -67528 252242 -394404 252242 -67528 7085  -230   1",
				"12, 24: 1 -248 3612 -16072  25670 -16072 3612 -248 1",
				"13, 26: 1 -324 14626 -215604 1346671 -3965064 5692636 -3965064 1346671 -215604 14626 -324 1",
				"14, 28:  1 -372 18242 -266916 1488367 -3925992 5377436 -3925992 1488367 -266916 18242 -372 1",
				"15, 30:  1 -376  4380  -15944  24134  -15944  4380 -376 1",
				"l = 3, n=6: (hexagon) psqLhat(3, x) = 1 - 14*x + x^2. The two roots are positive: 7 + 4*sqrt(3) = sqLhat(3) and 7 - 4*sqrt(3). For the square of the sum of all length ratios one has PsqL(3, x) = 1296 - 504*x + x^2, with the previous two roots scaled by a factor 36.",
				"l = 5, n=10: (decagon)  =  psqLhat(5, x) = 1  - 44*x + 166*x^2 - 44*x^3 + x^4 with thefour  positive roots  sqLhat(10) = 7 + 8*phi + 4*sqrt(7+11*phi), 15 - 8*phi + 4*sqrt(18 - 11*phi), 15 - 8*phi - 4*sqrt(18 - 11*phi),   7 + 8*phi - 4*sqrt(7 + 11*phi), approximately 39.86345819, 3.851840015, 0.259616169, 0.02508563, respectively, where phi = rho(5) = (1+sqrt(5))/2 (the golden section). PsqL(5, x) =100000000 - 44000000*x + 1660000*x^2 - 4400*x^3 + x^4, with the previous four roots scaled by a factor 100.",
				"l=6, n = 12: (dodecagon) psqLhat(6, x)= 1 - 60*x + 134*x^2 - 60*x^3 + x^4, with the four positive roots sqLhat(12) = 15 + 6*sqrt(6) + 80*sqrt(3*(49-20*sqrt(6))) +",
				"  98*sqrt(2*(49-20*sqrt(6))), 15 + 6*sqrt(6) - 80*sqrt(3*(49-20*sqrt(6))) - 98*sqrt(2*(49-20*sqrt(6))), 15 - 6*sqrt(6) + 2*sqrt(2*(49-20*sqrt(6))), 15 - 6*sqrt(6) - 2*sqrt(2*(49-20*sqrt(6))), approximately 57.69548054, 1.69839638, 0.58879070, 0.01733238, respectively.",
				"Mustonen's conjecture for rows No. l = 2^k, k \u003e= 1 (see a comment above):  l = 8 (k=3): ((-1)^m)*binomial(16,2*m), m = 0..8: [1, -120, 1820, -8008, 12870, -8008, 1820, -120, 1], with obvious symmetry."
			],
			"xref": [
				"Cf. A055034, A187360, A228780, A230072 (sqLhat(2*l))."
			],
			"keyword": "sign,tabf",
			"offset": "1,4",
			"author": "_Wolfdieter Lang_, Oct 09 2013",
			"references": 1,
			"revision": 8,
			"time": "2013-10-19T02:03:08-04:00",
			"created": "2013-10-10T03:54:32-04:00"
		}
	]
}