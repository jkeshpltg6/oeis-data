{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A145341",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 145341,
			"data": "1,3,5,7,9,13,11,15,17,25,21,29,19,27,23,31,33,49,41,57,37,53,45,61,35,51,43,59,39,55,47,63,65,97,81,113,73,105,89,121,69,101,85,117,77,109,93,125,67,99,83,115,75,107,91,123,71,103,87,119,79,111,95,127,129,193",
			"name": "Convert 2n-1 to binary. Reverse its digits. Convert back to decimal to get a(n).",
			"comment": [
				"This sequence is a permutation of the odd positive integers.",
				"From _Yosu Yurramendi_, Feb 05 2019: (Start)",
				"If the terms (n \u003e 0) are written as an array (left-aligned fashion) with rows of length 2^m, m = 0,1,2,3,...",
				"   1,",
				"   3,  5,",
				"   7,  9, 13, 11,",
				"  15, 17, 25, 21,  29, 19,  27, 23,",
				"  31, 33, 49, 41,  57, 37,  53, 45,  61, 35,  51, 43,  59, 39,  55, 47,",
				"  63, 65, 97, 81, 113, 73, 105, 89, 121, 69, 101, 85, 117, 77, 109, 93, 125, ...",
				"for m \u003e 0,  a(2^(m+1)) = 2*a(2^m) + 1; a(2^m + 1) = a(2^m) + 2; a(2^(m+1) + 2^m) = 2*a(2^(m+1)) - 1,",
				"for m \u003e 0, 0 \u003c k \u003c 2^m, a(2^(m+1) + k) = 2*a(2^m + k) - 1, a(2^(m+1) + 2^m + k) = a(2^(m+1) + k) + 2.",
				"This relationship is enough to reproduce the sequence.",
				"If the terms (n \u003e 0) are written as an array (right-aligned fashion):",
				"                                                                              1,",
				"                                                                          3,  5,",
				"                                                                 7,  9,  13, 11,",
				"                                              15, 17,  25, 21,  29, 19,  27, 23,",
				"           31, 33, 49, 41,  57, 37,  53, 45,  61, 35,  51, 43,  59, 39,  55, 47,",
				"  ... 93, 125, 67, 99, 83, 115, 75, 107, 91, 123, 71, 103, 87, 119, 79, 111, 95,",
				"...",
				"for m \u003e= 0, a(2^(m+1)+2^m) = 4*a(2^m) + 1.",
				"for m \u003e= 0, 0 \u003c= k \u003c 2^m-1, a(2^(m+2)-1-k) = 2*a(2^(m+1)-1-k) + 1.",
				"(End)"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A145341/b145341.txt\"\u003eTable of n, a(n) for n = 1..16384\u003c/a\u003e",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A030101(2n-1).",
				"a(n) = A145342(n)*2 - 1."
			],
			"mathematica": [
				"Table[FromDigits[Reverse[IntegerDigits[2*n - 1, 2]], 2], {n, 1, 100}] (* _Stefan Steinerberger_, Oct 11 2008 *)"
			],
			"program": [
				"(R)",
				"  nmax \u003c- 10^3 # by choice",
				"  a \u003c- vector()",
				"  for (o in seq(1,nmax,2)){",
				"    w \u003c- which(as.numeric(intToBits(o))==1)",
				"    a \u003c- c(a, sum(2^(max(w)-w)))",
				"}",
				"a[1:66]",
				"# _Yosu Yurramendi_, Feb 04 2019",
				"(PARI) a(n) = fromdigits(Vecrev(binary(2*n-1)), 2); \\\\ _Michel Marcus_, Feb 04 2019"
			],
			"xref": [
				"Cf. A030101, A145342."
			],
			"keyword": "base,nonn",
			"offset": "1,2",
			"author": "_Leroy Quet_, Oct 08 2008",
			"ext": [
				"More terms from _R. J. Mathar_, _Ray Chandler_ and _Stefan Steinerberger_, Oct 10 2008"
			],
			"references": 4,
			"revision": 30,
			"time": "2019-05-14T11:18:18-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}