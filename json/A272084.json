{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A272084",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 272084,
			"data": "1,1,1,1,2,1,1,1,2,3,3,1,2,1,2,1,3,3,2,2,1,2,3,1,3,4,4,1,4,1,2,1,3,3,3,2,2,1,2,3,5,4,2,3,3,3,2,1,2,6,6,2,3,2,2,1,3,4,4,2,3,1,6,1,5,3,4,3,4,1,4,3,4,8,4,2,1,3,2,2,5,4,4,1,6,3,6,2,5,6,7,3,2,2,2,1,3,5,9,3",
			"name": "Number of ordered ways to write n as x^2 + y^2 + z^2 + w^2 with 4*x^2 + 5*y^2 + 20*z*w a square, where x,y,z,w are nonnegative integers with z \u003c w.",
			"comment": [
				"Conjecture: (i) a(n) \u003e 0 for all n \u003e 0, and a(n) = 1 only for n = 2^k*q (k = 0,1,2,... and q = 1, 3, 7), 4^k*m (k = 0,1,2,... and m = 21, 30, 38, 62, 70, 77, 142, 217, 237, 302, 382, 406, 453, 670).",
				"(ii) For each triple (a,b,c) = (1,8,8), (7,9,-12), (9,40,-24), (9,40,-60), any positive integer can be written as x^2 + y^2 + z^2 + w^2 with a*x^2 + b*y^2 + c*z*w a square, where w is a positive integer and x,y,z are nonnegative integers.",
				"(iii) Any natural number can be written as x^2 + y^2 + z^2 + w^2 with (3*x+5*y)^2 -24*z*w a square, where x,y,z,w are nonnegative integers.  Also, for each ordered pair (a,b) = (1,4), (1,8), (1,12), (1,24), (1,32), (1,48), (25,24), (1,-4), (9,-4), (121,-20), every natural number can be written as x^2 + y^2 + z^2 + w^2 with a*x^2 + b*y*z a square, where x,y,z,w are nonnegative integers.",
				"(iv) Any natural number can be written as x^2 + y^2 + z^2 + w^2 with (x^2-y^2)*(w^2-2*z^2) (or (x^2-y^2)*(2*w^2-z^2) or (x^2-y^2)*(w^2-5*z^2)) a square, where w,x,y,z are integers.",
				"See also A262357, A268507, A269400, A271510, A271513, A271518, A271665, A271714, A271721, A271724, A271775, A271778 and A271824 for other conjectures refining Lagrange's four-square theorem."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A272084/b272084.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1604.06723\"\u003eRefining Lagrange's four-square theorem\u003c/a\u003e, arXiv:1604.06723, 2016."
			],
			"example": [
				"a(1) = 1 since 1 = 0^2 + 0^2 + 0^2 + 1^2 with 0 \u003c 1 and 4*0^2 + 5*0^2 + 20*0*1 = 0^2.",
				"a(2) = 1 since 2 = 1^2 + 0^2 + 0^2 + 1^2 with 0 \u003c 1 and 4*1^2 + 5*0^2 + 20*0*1 = 2^2.",
				"a(3) = 1 since 3 = 1^2 + 1^2 + 0^2 + 1^2 with 0 \u003c 1 and 4*1^2 + 5*1^2 + 20*0*1 = 3^2.",
				"a(6) = 1 since 6 = 1^2 + 1^2 + 0^2 + 2^2 with 0 \u003c 2 and 4*1^2 + 5*1^2 + 20*0*2 = 3^2.",
				"a(14) = 1 since 14 = 1^2 + 3^2 + 0^2 + 2^2 with 0 \u003c 2 and 4*1^2 + 5*3^2 + 20*0*2 = 7^2.",
				"a(21) = 1 since 21 = 0^2 + 2^2 + 1^2 + 4^2 with 1 \u003c 4 and 4*0^2 + 5*2^2 + 20*1*4 = 10^2.",
				"a(30) = 1 since 30 = 4^2 + 2^2 + 1^2 + 3^2 with 1 \u003c 3 and 4*4^2 + 5*2^2 + 20*1*3 = 12^2.",
				"a(38) = 1 since 38 = 1^2 + 1^2 + 0^2 + 6^2 with 0 \u003c 6 and 4*1^2 + 5*1^2 + 20*0*6 = 3^2.",
				"a(62) = 1 since 62 = 1^2 + 3^2 + 4^2 + 6^2 with 4 \u003c 6 and 4*1^2 + 5*3^2 + 20*4*6 = 23^2.",
				"a(70) = 1 since 70 = 7^2 + 1^2 + 2^2 + 4^2 with 2 \u003c 4 and 4*7^2 + 5*1^2 + 20*2*4 = 19^2.",
				"a(77) = 1 since 77 = 4^2 + 6^2 + 3^2 + 4^2 with 3 \u003c 4 and 4*4^2 + 5*6^2 + 20*3*4 = 22^2.",
				"a(142) = 1 since 142 = 4^2 + 6^2 + 3^2 + 9^2 with 3 \u003c 9 and 4*4^2 + 5*6^2 + 20*3*9 = 28^2.",
				"a(217) = 1 since 217 = 6^2 + 6^2 + 8^2 + 9^2 with 8 \u003c 9 and 4*6^2 + 5*6^2 + 20*8*9 = 42^2.",
				"a(237) = 1 since 237 = 5^2 + 8^2 + 2^2 + 12^2 with 2 \u003c 12 and 4*5^2 + 5*8^2 + 20*2*12 = 30^2.",
				"a(302) = 1 since 302 = 11^2 + 9^2 + 6^2 + 8^2 with 6 \u003c 8 and 4*11^2 + 5*9^2 + 20*6*8 = 43^2.",
				"a(382) = 1 since 382 = 11^2 + 7^2 + 4^2 + 14^2 with 4 \u003c 14 and 4*11^2 + 5*4^2 + 20*4*14 = 43^2.",
				"a(406) = 1 since 406 = 8^2 + 6^2 + 9^2 + 15^2 with 9 \u003c 15 and 4*8^2 + 5*6^2 + 20*9*15 = 56^2.",
				"a(453) = 1 since 453 = 8^2 + 14^2 + 7^2 + 12^2 with 7 \u003c 12 and 4*8^2 + 5*14^2 + 20*7*12 = 54^2.",
				"a(670) = 1 since 670 = 17^2 + 11^2 + 2^2 + 16^2 with 2 \u003c 16 and 4*17^2 + 5*11^2 + 20*2*16 = 49^2."
			],
			"mathematica": [
				"SQ[n_]:=SQ[n]=IntegerQ[Sqrt[n]]",
				"Do[r=0;Do[If[SQ[n-x^2-y^2-z^2]\u0026\u0026SQ[4x^2+5y^2+20*z*Sqrt[n-x^2-y^2-z^2]],r=r+1],{x,0,Sqrt[n-1]},{y,0,Sqrt[n-1-x^2]},{z,0,Sqrt[(n-1-x^2-y^2)/2]}];Print[n,\" \",r];Continue,{n,1,100}]"
			],
			"xref": [
				"Cf. A000118, A000290, A262357, A268507, A269400, A271510, A271513, A271518, A271608, A271665, A271714, A271721, A271724, A271775, A271778, A271824."
			],
			"keyword": "nonn",
			"offset": "1,5",
			"author": "_Zhi-Wei Sun_, Apr 19 2016",
			"references": 28,
			"revision": 11,
			"time": "2016-05-01T13:23:25-04:00",
			"created": "2016-04-20T00:50:22-04:00"
		}
	]
}