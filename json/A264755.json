{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A264755",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 264755,
			"data": "1,4,2,42,66,12,780,2652,1608,21552,132240,183168,25920,803760,7984320,20815440,10313280",
			"name": "Triangle T(n,g) read by rows: Partition of the set of (2n-1)! circular permutations on 2n elements according to the minimal genus g of the surface in which one can immerse the non-simple closed curves with n crossings determined by those permutations.",
			"comment": [
				"Each line of the triangle adds up to an odd factorial (2n-1)!. Example (line n=5): 21552 + 132240 + 183168 + 25920 = 362880 = 9!.",
				"The lengths of the rows of the triangle do not strictly increase with n, the first lengths are (1,2,3,3,4,4,...)."
			],
			"link": [
				"R. Coquereaux, J.-B. Zuber, \u003ca href=\"http://arxiv.org/abs/1507.03163\"\u003eMaps, immersions and permutations\u003c/a\u003e, arXiv preprint arXiv:1507.03163, 2015. Also J. Knot Theory Ramifications 25, 1650047 (2016), DOI: http://dx.doi.org/10.1142/S0218216516500474"
			],
			"example": [
				"Taking n = 5 crossings and genus g=0, one obtains a subset of 21552 circular permutations of Sym(10) which correspond, in the OO case (the circle is oriented, the sphere is oriented), to the union 179 orbits of length 120=5!/1 and 3 orbits of length 24=5!/5 with respective centralizers of order 1 and 5 under the action of the symmetric group Sym(5) acting on this subset: 179*120 + 3*24 = 21552. The total number of orbits 179 + 3 = 182 is the number of immersed spherical curves (g=0) with 5 crossings, in the OO case. See A008986, also A260285, A260296. The next entry, 132240, gives the number of circular permutations that describe immersed closed curves in a torus (g=1), with n=5 crossings, up to stable geotopy; the number of such closed curves in the OO case is 1102 (cf A260285).",
				"Triangle begins:",
				"1",
				"4 2",
				"42 66 12",
				"780 2652 1608",
				"21552 132240 183168 25920",
				"803760 7984320 20815440 10313280",
				"..."
			],
			"program": [
				"(MAGMA) /* Example: line n=5 of the triangle *)",
				"n:=5;",
				"G:=Sym(2*n);",
				"CG := Classes(G);",
				"pos:= [j: j in [1..#CG]  | CycleStructure(CG[j][3]) eq [\u003c2*n,1\u003e]][1];",
				"circularpermutations:=Class(G,CG[pos][3]); //circularpermutations",
				"doubleG := Sym(4*n);",
				"psifct := function(per);",
				"perinv:=per^(-1);",
				"res:= [IsOdd(j) select (j+1)^per  else j-1 + 2*n : j in [1..2*n] ];",
				"resbis := [IsOdd((j-2*n)^perinv) select  (j-2*n)^perinv +1 +2*n   else ((j-2*n)^perinv -1)^per : j in [2*n+1..4*n] ];",
				"res cat:= resbis;",
				"return doubleG!res;",
				"end function;",
				"numberofcycles := function(per);   ess :=   CycleStructure(per); return \u0026+[ess[i,2]: i in [1..#ess]]; end function;",
				"supernumberofcycles := function(per); return  numberofcycles(psifct(per)) ; end function;",
				"{* supernumberofcycles(x) : x in circularpermutations  *};",
				"quit;"
			],
			"xref": [
				"Cf. A008986, A260296.",
				"Cf. A260285, A260848, A260885, A260914."
			],
			"keyword": "nonn,tabl,more",
			"offset": "1,2",
			"author": "_Robert Coquereaux_, Nov 23 2015",
			"references": 0,
			"revision": 20,
			"time": "2016-08-02T08:52:10-04:00",
			"created": "2015-11-24T01:36:39-05:00"
		}
	]
}