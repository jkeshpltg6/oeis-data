{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A254525",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 254525,
			"data": "1,-1,-1,-1,0,3,4,-1,-6,-5,1,10,11,-4,-19,-17,4,31,31,-9,-50,-46,11,79,77,-21,-122,-112,28,183,173,-46,-273,-249,62,396,370,-98,-573,-521,130,815,751,-193,-1149,-1041,261,1599,1461,-373,-2214,-1998,498,3031",
			"name": "Expansion of f(-x^2)^2 * f(-x, x^2) / f(x^3)^3 in powers of x where f(,) is Ramanujan's general theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A254525/b254525.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of (chi(x) / chi(x^3)^3) * (psi(-x) / psi(-x^3))^2 in powers of x where chi(), psi() are Ramanujan theta functions.",
				"Expansion of q^(1/6) * eta(q) * eta(q^3) * eta(q^4) * eta(q^12) / eta(q^6)^4 in powers of q.",
				"Euler transform of period 12 sequence [ -1, -1, -2, -2, -1, 2, -1, -2, -2, -1, -1, 0, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (144 t)) = 9^(1/2) g(t) where q = exp(2 Pi i t) and g() is the g.f. for A254346.",
				"Convolution of A062243 and A128111.",
				"a(n) = (-1)^n * A132179(n).",
				"a(2*n) = A230256(n). a(2*n + 1) = - A233037(n)."
			],
			"example": [
				"G.f. = 1 - x - x^2 - x^3 + 3*x^5 + 4*x^6 - x^7 - 6*x^8 - 5*x^9 + x^10 + ...",
				"G.f. = 1/q - q^5 - q^11 - q^17 + 3*q^29 + 4*q^35 - q^41 - 6*q^47 - 5*q^53 + ..."
			],
			"mathematica": [
				"eta[q_] := q^(1/24)*QPochhammer[q]; A254525[n_] := SeriesCoefficient[",
				"   q^(1/6)*eta[q]*eta[q^3]*eta[q^4]*eta[q^12]/eta[q^6]^4, {q, 0, n}]; Table[A254525[n], {n,0,50}] (* _G. C. Greubel_, Aug 10 2017 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A) * eta(x^3 + A) * eta(x^4 + A) * eta(x^12 + A) / eta(x^6 + A)^4, n))};"
			],
			"xref": [
				"Cf. A062243, A128111, A132179, A230256, A233037, A254346."
			],
			"keyword": "sign",
			"offset": "0,6",
			"author": "_Michael Somos_, Jan 31 2015",
			"references": 2,
			"revision": 18,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2015-01-31T20:59:05-05:00"
		}
	]
}