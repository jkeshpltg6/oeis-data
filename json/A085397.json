{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A085397",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 85397,
			"data": "2,3,6,7,10,11,12,14,15,18,19,22,23,24,26,28,30,31,34,35,38,39,40,42,43,44,46,47,48,50,51,54,55,56,58,59,60,62,63,66,67,70,71,72,74,75,76,78,79,82,83,86,87,88,90,91,92,94,95,96,98,99,102,103,104,106,107,108",
			"name": "Numbers that are not perfect powers and whose squarefree part is not congruent to 1 (mod 4).",
			"comment": [
				"Contains A016825. - _Robert Israel_, Mar 20 2016",
				"The asymptotic density of this sequence is 2/3. - _Amiram Eldar_, Mar 09 2021"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A085397/b085397.txt\"\u003eTable of n, a(n) for n=1..1000\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ArtinsConstant.html\"\u003eArtin's Constant\u003c/a\u003e."
			],
			"maple": [
				"f:= proc(n) local F,x;",
				"  F:= ifactors(n)[2];",
				"  if igcd(seq(f[2],f=F)) \u003e 1 then return false fi;",
				"  x:= mul(f[1], f = select(t -\u003e t[2]::odd, F));",
				"  x mod 4 \u003c\u003e 1;",
				"end proc:",
				"select(f, [$1..200]); # _Robert Israel_, Mar 20 2016"
			],
			"mathematica": [
				"fi[n_] := fi[n] = FactorInteger[n]; perfectPowerQ[n_] := Length[uf = Union[ fi[n][[All, 2]]]] == 1 \u0026\u0026 uf[[1]] \u003e= 2; SquareFreePart[n_] := Times @@ Apply[Power, ({#[[1]], Mod[#[[2]], 2]} \u0026 ) /@ fi[n], {1}]; ok[n_] := ! perfectPowerQ[n] \u0026\u0026 Mod[ SquareFreePart[n], 4] != 1; Select[ Range[110], ok] (* _Jean-François Alcover_, Jan 20 2012 *)"
			],
			"program": [
				"(PARI) isok(n) = !ispower(n) \u0026\u0026 ((core(n) % 4) != 1); \\\\ _Michel Marcus_, Mar 19 2016"
			],
			"xref": [
				"Subsequence of A007916.",
				"Cf. A016825."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_Eric W. Weisstein_, Jun 27 2003",
			"references": 2,
			"revision": 25,
			"time": "2021-03-09T03:34:27-05:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}