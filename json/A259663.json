{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A259663",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 259663,
			"data": "1,13,11,5,19,7,53,3,55,47,21,35,87,79,31,213,99,23,143,223,191,85,483,407,15,95,319,127,853,739,663,271,351,63,895,767,341,1251,1175,1807,863,1599,1407,1279,511",
			"name": "Square array T(n,k) read by antidiagonals upwards: \"Dropping Times\" in reduced Collatz sequences. (See \"Comments\" for definitions and explanation.)",
			"comment": [
				"Array starts T(2,1).",
				"Coefficients are least residues in congruence classes T(n,k) mod 2^(n+k). Let T\"(n,k) be all members of that class.",
				"Let reduced Collatz sequences (R) be Collatz sequences (C) showing only odd terms; and let S be the initial term in R and C, denoted as R(S) and C(S), respectively.",
				"Define \"Dropping Time\" (D(S)) as the first term in R(S) \u003c its preimage (P(S)). For example, S=159: R(159) starts [159, 239, 359, 539, 809, 607, ...]; therefore, D(159) = 607 and P(159) = 809.",
				"When S is in T\"(n,k): k is the number of terms in R(S) from S to D(S), and n is the number of halving steps in C(S) from P(S) to D(S). So for S=159: since 159 == 31 mod 128 and T(2,5) = 31, there are 5 steps from S=159 to D(159) = 607 (239, 359, 539, 809, 607) and 2 halving steps from P(159) = 809 to D(159), i.e., 809*3+1 = 2428; 2428/2 = 1214, 1214/2 = 607.",
				"Generally, when any term in R is in T\"(n,k) k\u003e=2, trajectories follow T\"(n,k-i) {i=1..k-1}. At T\"(n,1), the next term's congruence class is not clearly predictable. So for example S=159: 159 == 31 mod 128 (in T\"(2,5)), 239 == 47 mod 64 (in T\"(2,4)), 359 == 7 mod 32 (in T\"(2,3)), 539 == 11 mod 16 (in T\"(2,2)) and 809 == 1 mod 8 (in T\"(2,1)). Since 607 == 95 mod 512 (in T\"(4,5)), we know the subsequent term will be in T\"(4,4), followed by terms in T\"(4,3), T\"(4,2) and T\"(4,1).",
				"Let T\"(k) be members of all T\"(n,k) holding k constant. Then T\"(k) == (2^k-1) mod 2^(k+1). However, since S is the only possible term in R(S) that could be congruent to 0 mod 3, it makes sense to consider only terms congruent to {1,2} mod 3 when evaluating T\"(k). Therefore, after the initial term in R(S), all subsequent terms in T\"(k) are congruent to either:",
				"i. {(2^k - 1), (6*4^((k-1)/2) - 1)} mod 3*2^(k+1) when k is odd; or",
				"ii. {(3*4^(k/2) - 1), (5*4^(k/2) - 1)} mod 3*2^(k+1) when k is even.",
				"The array yields a wide variety of interesting patterns and sub-patterns associated with the residues and quotients of the congruence classes. Perhaps analysis of these patterns could shed light on the nature of Collatz sequences, including the Collatz conjecture (i.e., all Collatz sequences terminate at 1).",
				"From _Bob Selcoe_, Sep 30 2019: (Start)",
				"From equations i and ii above, terms in T\"(k) can be described as follows:",
				"ia. for odd k: {T(2,k), T(m+3,k)} mod 3*2^(k+1) when k == 2^m - 1 mod 2^(m+1), m \u003e= 1; or",
				"iia. for even k: {T(2,k), T(3,k)} mod 3*2^(k+1).",
				"(End)"
			],
			"formula": [
				"From _Bob Selcoe_, Jul 15 2017: (Start)",
				"The array is constructed by the following:",
				"T(2,k) = 2^k-1 when k is odd, T(2,k) = 3*2^k-1 when k is even; i.e., A083420((k-1)/2) and A198693(k/2) interleaved.",
				"T(3,k) = 7*2^k-1 when k is odd; T(3,k) = 5*2^k-1 when k is even; i.e., A206372((k-1)/2) and A156760(k/2) interleaved.",
				"For n \u003e= 4: T(n,j) = 2^j-1, j == 2^(n-3) (mod 2^(n-2)); T(n,j-i) = least residue of 2^(j-i)*3^i - 1 mod 2^(n+j-i), 1 \u003c= i \u003c j. (See Example.)",
				"(End)"
			],
			"example": [
				"Array starts T(2,1):",
				"n\\k   1    2     3     4      5      6      7      8      9 ...",
				"2:    1    11    7     47     31     191    127    767    511",
				"3:    13   19    55    79     223    319    895    1279   3583",
				"4:    5    3     87    143    95     63     1407   2303   1535",
				"5:    53   35    23    15     351    1599   2431   4351   13823",
				"6:    21   99    407   271    863    575    383    255    22015",
				"7:    213  483   663   1807   3935   2623   12671  8447   5631",
				"8:    85   739   1175  783    5983   14911  20863  57599  38399",
				"9:    853  1251  2199  6927   10079  6719   4479   90367  235007",
				"10:   341  227   151   11023  18271  55871  37247  24831  366079",
				"For n \u003e= 4: e.g., n=4, so j == 2 (mod 4). Select j=6, i=2 to find T(4,4). T(4,6) = 2^6 - 1 = 63. 2^(6-2)*3^2 - 1 mod 2^(4+6-2) = 143 mod 256 = T(4,4) = 143. Now instead select j=10, i=6 to find T(4,4). T(4,10) = 2^10 - 1 = 1023. 2^(10-6)*3^6 - 1 mod 2^(4+10-6) = 11663 mod 256 = 143. - _Bob Selcoe_, Jul 15 2017"
			],
			"program": [
				"(PARI) T(n, k) = if (n==2, if (k%2, 2^k-1, 3*2^k-1), if (n==3, if (k%2, 7*2^k-1, 5*2^k-1), mj = 2^(n-3) % 2^(n-2); mk = k % 2^(n-2); (2^k*3^(mj-mk) - 1) % 2^(n+k)));",
				"tabl(nn) = matrix(nn, nn, n, k, T(n+1,k)); \\\\ _Michel Marcus_, Jul 10 2018"
			],
			"xref": [
				"Cf. A083420, A156760, A198693, A206372."
			],
			"keyword": "nonn,tabl",
			"offset": "2,2",
			"author": "_Bob Selcoe_, Jul 02 2015",
			"references": 5,
			"revision": 58,
			"time": "2019-10-07T14:26:44-04:00",
			"created": "2015-08-22T00:58:04-04:00"
		}
	]
}