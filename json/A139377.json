{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A139377",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 139377,
			"data": "1,1,1,3,2,1,5,6,3,1,11,15,10,4,1,21,41,30,15,5,1,43,113,92,51,21,6,1,85,327,284,171,79,28,7,1,171,982,897,570,286,115,36,8,1,341,3066,2895,1913,1016,446,160,45,9,1",
			"name": "A Jacobsthal-Catalan triangle.",
			"comment": [
				"First column is A001045(n+1). Second column is A139379. Row sums are A139379(n+1).",
				"Diagonal sums are A135582. Inverse of the Riordan array (1-x-x^2+4x^3-2x^4,x(1-x))."
			],
			"link": [
				"P. Bala, \u003ca href=\"/A264772/a264772_1.pdf\"\u003eA 4-parameter family of embedded Riordan arrays\u003c/a\u003e"
			],
			"formula": [
				"Riordan array (1/(1-x-2x^2), xc(x)) where c(x) is the g.f. of A000108",
				"From _Peter Bala_, Feb 20 2018: (Start)",
				"Define a(n) = floor(2^(n+2)/3) - floor(2^(n+1)/3) = A001045(n+1). Then T(n,0) = a(n) and T(n,k) = Sum_{j = 0..n-k} a(j)*k/(2*n-k-2*j)*binomial(2*n-k-2*j,n-k-j) for 1 \u003c= k \u003c= n.",
				"Define b(n) = (2/3)*(1+i)^(n-1) + (2/3)*(1-i)^(n-1) - (4/3)*(1+i)^(n-2) - (4/3)*(1-i)^(n-2) + (1/3)*(-1)^n*Fibonacci(n+1) + (2/3)*(-1)^n*Fibonacci(n). Then T(n,k) = Sum_{j = 0..n-k} b(j)*binomial(2*n-k-j,n) for 0 \u003c= k \u003c= n.",
				"The n-th row polynomial in descending powers of x is the n-th Taylor polynomial of the rational function (1 - 2*x)/((1 - x)*(1 + x - x^2)*(1 - 2*x + 2*x^2)) * 1/(1 - x)^n about 0. For example, for n = 4, (1 - 2*x)/((1 - x)*(1 + x - x^2)*(1 - 2*x + 2*x^2)) * 1/(1 - x)^4 = (11*x^4  + 15*x^3 + 10*x^2 + 4*x + 1) + O(x^5). (End)"
			],
			"example": [
				"Triangle begins",
				"    1;",
				"    1,   1;",
				"    3,   2,   1;",
				"    5,   6,   3,   1;",
				"   11,  15,  10,   4,   1;",
				"   21,  41,  30,  15,   5,   1;",
				"   43, 113,  92,  51,  21,   6,   1;",
				"   85, 327, 284, 171,  79,  28,   7,   1;",
				"  171, 982, 897, 570, 286, 115,  36,   8,   1;",
				"The production matrix for this array is",
				"   1, 1,",
				"   2, 1, 1,",
				"  -2, 1, 1, 1,",
				"   0, 1, 1, 1, 1,",
				"   0, 1, 1, 1, 1, 1,",
				"   0, 1, 1, 1, 1, 1, 1,",
				"   0, 1, 1, 1, 1, 1, 1"
			],
			"maple": [
				"#define auxiliary sequence",
				"with(combinat):",
				"b := proc (n)",
				"  (2/3)*(1+I)^(n-1) + (2/3)*(1-I)^(n-1) - (4/3)*(1+I)^(n-2)-(4/3)*(1-I)^(n-2) + (1/3)*(-1)^n*fibonacci(n+1) + (2/3)*(-1)^n*fibonacci(n);",
				"end proc:",
				"A139377 := proc (n, k)",
				"  add(b(j)*binomial(2*n-k-j, n), j = 0..n-k);",
				"end proc:",
				"#display sequence as a triangle",
				"for n from 0 to 10 do",
				"  seq(A139377(n, k), k = 0..n);",
				"end do; # _Peter Bala_, Feb 20 2018"
			],
			"xref": [
				"Cf. A001045 (first column), A139379 (second column and row sums), A135582 (sums along shallow diagonals)."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "0,4",
			"author": "_Paul Barry_, Apr 15 2008",
			"references": 3,
			"revision": 10,
			"time": "2018-03-05T04:55:55-05:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}