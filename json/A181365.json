{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A181365",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 181365,
			"data": "2,6,1,22,2,78,3,1,272,6,2,940,13,2,1,3232,28,2,2,11080,58,3,2,1,37920,118,6,2,2,129648,239,12,2,2,1,443008,484,22,2,2,2,1513248,979,37,3,2,2,1,5168000,1976,60,6,2,2,2,17647552,3980,97,12,2,2,2,1,60258304,8004",
			"name": "Triangle read by rows: T(n,k) is the number of 2-compositions of n having least entry equal to k (n \u003e= 1; 0 \u003c= k \u003c= floor(n/2)). A 2-composition of n is a nonnegative matrix with two rows, such that each column has at least one nonzero entry and whose entries sum up to n.",
			"comment": [
				"Row n contains 1 + floor(n/2) entries.",
				"The sum of entries in row n is A003480(n).",
				"T(n,1) = A181367(n).",
				"Sum_{k \u003e= 0} k*T(n,k) = A181366."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A181365/b181365.txt\"\u003eRows n = 1..200, flattened\u003c/a\u003e",
				"G. Castiglione, A. Frosini, E. Munarini, A. Restivo and S. Rinaldi, \u003ca href=\"http://dx.doi.org/10.1016/j.ejc.2006.06.020\"\u003eCombinatorial aspects of L-convex polyominoes\u003c/a\u003e, European J. Combin. 28 (2007), no. 6, 1724-1741."
			],
			"formula": [
				"G.f. for 2-compositions with all entries \u003e= k is h(k,z)=(1-z)^2/(1-2z+z^2-z^{2k}) if k\u003e0 and h(0,z)=(1-z)^2/(1-4z+2z^2) if k=0.",
				"G.f. for 2-compositions with least entry k is f(k,z)=h(k,z)-h(k+1,z) (these are the column g.f.'s).",
				"G.f.: G(t,z) = f(0,z) + Sum_{k\u003e=1} f(k,z)*t^k."
			],
			"example": [
				"T(4,1) = 3 because we have (1/3), (3/1), and (1,1/1,1) (the 2-compositions are written as (top row / bottom row).",
				"Triangle starts:",
				"    2;",
				"    6,  1;",
				"   22,  2;",
				"   78,  3, 1;",
				"  272,  6, 2;",
				"  940, 13, 2, 1;"
			],
			"maple": [
				"h := proc (k) if k = 0 then (1-z)^2/(1-4*z+2*z^2) else (1-z)^2/(1-2*z+z^2-z^(2*k)) end if end proc: f := proc (k) options operator, arrow: h(k)-h(k+1) end proc; G := f(0)+sum(f(k)*t^k, k = 1 .. 30): Gser := simplify(series(G, z = 0, 20)): for n to 15 do P[n] := sort(coeff(Gser, z, n)) end do: for n to 15 do seq(coeff(P[n], t, k), k = 0 .. floor((1/2)*n)) end do; # yields sequence in triangular form",
				"# second Maple program:",
				"A:= proc(n, k) option remember; `if`(n=0, 1, add(add(",
				"     `if`(i=0 and j=0, 0, A(n-i-j, k)), i=k..n-j), j=k..n))",
				"    end:",
				"T:= (n, k)-\u003e A(n, k) -A(n, k+1):",
				"seq(seq(T(n, k), k=0..n/2), n=1..15); # _Alois P. Heinz_, Mar 16 2014"
			],
			"mathematica": [
				"A[n_, k_] := A[n, k] = If[n == 0, 1, Sum[Sum[If[i == 0 \u0026\u0026 j == 0, 0, A[n-i-j, k]], {i, k, n-j}], {j, k, n}]]; T[n_, k_] := A[n, k] - A[n, k+1]; Table[Table[T[n, k], {k, 0, n/2}], {n, 1, 15}] // Flatten (* _Jean-François Alcover_, May 28 2015, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A003480, A181366, A181367."
			],
			"keyword": "nonn,tabf",
			"offset": "1,1",
			"author": "_Emeric Deutsch_, Oct 15 2010",
			"references": 3,
			"revision": 15,
			"time": "2018-08-04T14:31:07-04:00",
			"created": "2010-10-20T03:00:00-04:00"
		}
	]
}