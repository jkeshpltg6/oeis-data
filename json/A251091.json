{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A251091",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 251091,
			"data": "0,1,1,9,8,25,9,49,32,81,25,121,72,169,49,225,128,289,81,361,200,441,121,529,288,625,169,729,392,841,225,961,512,1089,289,1225,648,1369,361,1521,800,1681,441,1849,968,2025,529,2209,1152,2401,625,2601,1352",
			"name": "a(n) = n^2 / gcd(n+2, 4).",
			"comment": [
				"A061038(n), which appears in 4*a(n) formula, is a permutation of n^2.",
				"Origin. In December 2010, I wrote in my 192-page Exercise Book no. 5, page 41, the array (difference table of the first row):",
				"1      0,   1/3,     1,   9/5,    8/3,   25/7,    9/2,   49/9, ...",
				"-1,  1/3,   2/3,   4/5, 13/15,  19/21,  13/14,  17/18,  43/45, ...",
				"Numerators are listed in A176126, denominators are in A064038, and denominator - numerator = 2, 2, 1, 1,... (A014695).",
				"4/3, 1/3,  2/15,  1/15, 4/105,   1/42,   1/63,   1/90,  4/495, ...",
				"-1, -1/5, -1/15, -1/35, -1/70, -1/126, -1/210, -1/330, -1/495, ...",
				"where the denominators of the second row are listed in A000332. Also for those of the inverse binomial transform",
				"1, -1, 4/3, -1, 4/5, -2/3, 4/7, -1/2, 4/9, -2/5, 4/11, -1/3, ... ?",
				"a(n) is the (n+1)-th term of the numerators of the first row."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A251091/b251091.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_12\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,0,0,3,0,0,0,-3,0,0,0,1)."
			],
			"formula": [
				"a(n) = n^2/(period 4: repeat 2, 1, 4, 1).",
				"a(4n) = 8*n^2, a(2n+1) = a(4n+2) = (2*n+1)^2.",
				"a(n+4) = a(n) + 8*A060819(n).",
				"a(n) = 3*a(n-4) - 3*a(n-8) + a(n-12), n\u003e11.",
				"4*a(n) = (period 4: repeat 2, 1, 4, 1) * A061038(n).",
				"G.f.: -x*(x^10+x^9+9*x^8+8*x^7+22*x^6+6*x^5+22*x^4+8*x^3+9*x^2+x+1) / ((x-1)^3*(x+1)^3*(x^2+1)^3). - _Colin Barker_, May 14 2015",
				"a(2n) = A181900(n), a(2n+1) = A016754(n). [_Bruno Berselli_, May 14 2015]",
				"a(n) = ( 1 - (1/16)*(1+(-1)^n)*(5-(-1)^(n/2)) )*n^2. [_Bruno Berselli_, May 14 2015]"
			],
			"example": [
				"a(0) = 0/2, a(1) =1/1, a(2) = 4/4, a(3) = 9/1."
			],
			"maple": [
				"seq(seq((4*i+j-1)^2/[2,1,4,1][j],j=1..4),i=0..30); # _Robert Israel_, May 14 2015"
			],
			"mathematica": [
				"f[n_] := Switch[ Mod[n, 4], 0, n^2/2, 1, n^2, 2, n^2/4, 3, n^2]; Array[f, 50, 0] (* or *) Table[(4 i + j - 1)^2/{2, 1, 4, 1}[[j]], {i, 0, 12}, {j, 4}] // Flatten (* after _Robert Israel_; or: *) LinearRecurrence[{0, 0, 0, 3, 0, 0, 0, -3, 0, 0, 0, 1}, {0, 1, 1, 9, 8, 25, 9, 49, 32, 81, 25, 121}, 53] (* or *) CoefficientList[ Series[-((x (1 + x (1 + x (9 + x (8 + x (22 + x (6 + x (22 + x (8 + x (9 + x + x^2))))))))))/(-1 + x^4)^3), {x, 0, 52}], x] (* _Robert G. Wilson v_, May 19 2015 *)"
			],
			"program": [
				"(PARI) concat(0, Vec(-x*(x^10 + x^9 + 9*x^8 + 8*x^7 + 22*x^6 + 6*x^5 + 22*x^4 + 8*x^3 + 9*x^2 + x + 1) / ((x-1)^3*(x+1)^3*(x^2+1)^3) + O(x^100))) \\\\ _Colin Barker_, May 14 2015",
				"(MAGMA) [(1-(1/16)*(1+(-1)^n)*(5-(-1)^(n div 2)) )*n^2: n in [0..60]]; // _Vincenzo Librandi_, Jun 12 2015"
			],
			"xref": [
				"Cf. A000290, A000332, A016754, A026741, A060819, A061038, A109008, A139098, A168077, A176895, A181900."
			],
			"keyword": "nonn,easy,mult",
			"offset": "0,4",
			"author": "_Paul Curtz_, May 08 2015",
			"ext": [
				"Missing term (1521) inserted in the sequence by _Colin Barker_, May 14 2015",
				"Definition uses a formula by _Jean-François Alcover_, Jul 01 2015",
				"Keyword:mult added by _Andrew Howroyd_, Aug 06 2018"
			],
			"references": 1,
			"revision": 64,
			"time": "2018-08-06T17:29:19-04:00",
			"created": "2015-07-01T11:47:38-04:00"
		}
	]
}