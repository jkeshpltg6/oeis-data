{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A047218",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 47218,
			"data": "0,3,5,8,10,13,15,18,20,23,25,28,30,33,35,38,40,43,45,48,50,53,55,58,60,63,65,68,70,73,75,78,80,83,85,88,90,93,95,98,100,103,105,108,110,113,115,118,120,123,125,128,130,133,135,138,140,143,145,148",
			"name": "Numbers that are congruent to {0, 3} mod 5.",
			"comment": [
				"Multiples of 5 interleaved with 2 less than multiples of 5. - _Wesley Ivan Hurt_, Oct 19 2013",
				"Numbers k such that k^2/5 + k*(k + 1)/10 = k*(3*k + 1)/10 is a nonnegative integer. - _Bruno Berselli_, Feb 14 2017"
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/A047218/b047218.txt\"\u003eTable of n, a(n) for n = 1..10001\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,1,-1)."
			],
			"formula": [
				"a(n) = 2*n - 5 + ceiling(n/2). - Jesus De Loera (deloera(AT)math.ucdavis.edu)",
				"a(n) = 5*n - a(n-1) - 7 for n\u003e1, a(1)=0. - _Vincenzo Librandi_, Aug 05 2010",
				"From _Bruno Berselli_, Jun 28 2011: (Start)",
				"G.f.: (2*x + 3)*x^2/((x + 1)*(x - 1)^2).",
				"a(n) = (10*n + (-1)^n - 9)/4.",
				"a(n) = a(n-1) + a(n-2) - a(n-3).  (End)",
				"a(n+1) = Sum_{k\u003e=0} A030308(n,k)*b(k) with b(0)=3 and b(k)=A020714(k-1)=5*2^(k-1) for k\u003e0. - _Philippe Deléham_, Oct 17 2011",
				"a(n) = n + ceiling(3*(n-1)/2) - 1. - _Arkadiusz Wesolowski_, Sep 18 2012",
				"a(n) = floor(5*n/2)-2 = 3*n - 3 - floor((n-1)/2). - _Wesley Ivan Hurt_, Oct 14 2013",
				"a(n+1) = n + (n + (n + (n mod 2))/2). - _Wesley Ivan Hurt_, Oct 19 2013",
				"Sum_{n\u003e=2} (-1)^n/a(n) = log(5)/4 - sqrt(5)*log(phi)/10 - sqrt(1-2/sqrt(5))*Pi/10, where phi is the golden ratio (A001622). - _Amiram Eldar_, Dec 07 2021"
			],
			"maple": [
				"seq(floor(5*k/2)-2, k=1..100); # _Wesley Ivan Hurt_, Sep 27 2013"
			],
			"mathematica": [
				"Select[Range[0, 200], MemberQ[{0, 3}, Mod[#, 5]] \u0026] (* _Vladimir Joseph Stephan Orlovsky_, Feb 12 2012 *)",
				"Table[Floor[5 n/2] - 2, {n,100}] (* _Wesley Ivan Hurt_, Sep 27 2013 *)",
				"With[{c5=5*Range[0,30]},Riffle[c5,c5+3]] (* or *) LinearRecurrence[{1,1,-1},{0,3,5},60] (* _Harvey P. Dale_, Apr 02 2017 *)"
			],
			"program": [
				"(PARI) forstep(n=0,200,[3,2],print1(n\", \")) \\\\ _Charles R Greathouse IV_, Oct 17 2011"
			],
			"xref": [
				"Cf. A001622, A047211, A047212, A047216."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"references": 36,
			"revision": 67,
			"time": "2021-12-07T05:22:23-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}