{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A285022",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 285022,
			"data": "820,1276,1926,2080,2640,3160,3186,3250,4446,4720,4930,5370,6006,6546,7386,7450,7476,9066,9276,10626,10836,13146,13300,15640,15666,16056,16060,16446,17020,17466,17550,17766,18040,18910,19176,19230,19416,20736,21000,21246",
			"name": "Numbers n such that A002088(n) \u003c 3n^2/Pi^2.",
			"comment": [
				"James Joseph Sylvester conjectured in 1883 that A002088(n) \u003e 3n^2/Pi^2 for all n.",
				"M. L. N. Sarma found the first counterexample, 820, in 1936.",
				"Paul Erdős and Harold N. Shapiro proved in 1951 that A002088(n)- 3n^2/Pi^2 changes signs at infinitely many values of n, thus this sequence is infinite.",
				"R. A. MacLeod proved in 1987 that A002088(n)/n^2 - 3/Pi^2 has a minimum at the second term, 1276."
			],
			"reference": [
				"Sukumar Das Adhikari, The Average Behaviour of the Number of Solutions of a Diophantine Equation and an Averaging Technique, Number Theory: Diophantine, Computational, and Algebraic Aspects: Proceedings of the International Conference Held in Eger, Hungary, July 29-August 2, 1996. Walter de Gruyter, 1998.",
				"Władysław Narkiewicz, Rational Number Theory in the 20th Century, Springer London, 2012, p. 215.",
				"M. L. N. Sarma, On the Error Term in a Certain Sum, Proceedings of the Indian Academy of Sciences, Section A, Vol. 3, No. 1 (1936), pp. 338-338."
			],
			"link": [
				"Amiram Eldar and Giovanni Resta, \u003ca href=\"/A285022/b285022.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (first 97 terms from Amiram Eldar)",
				"Paul Erdős and Harold N. Shapiro, \u003ca href=\"http://dx.doi.org/10.4153/CJM-1951-043-3\"\u003eOn the Changes of Sign of a Certain Error Function\u003c/a\u003e, Canadian Journal of Mathematics, Vol. 3 (1951), pp. 375-385.",
				"R. A. MacLeod, \u003ca href=\"https://doi.org/10.1112/jlms/s1-42.1.652\"\u003eThe Minimum of Phi(x)/x^2\u003c/a\u003e, Journal of the London Mathematical Society, Vol. 1, No. 1 (1967), pp. 652-660.",
				"James Joseph Sylvester, \u003ca href=\"http://gallica.bnf.fr/ark:/12148/bpt6k3052k/f463.item\"\u003eNote sur le théoreme de Legendre citée dans une note insérée dans les Comptes rendus\u003c/a\u003e, Comptes rendus hebdomadaires des seances de l'Academie des sciences, Vol. 46 (1883), pp. 463-465.",
				"James Joseph Sylvester, \u003ca href=\"http://dx.doi.org/10.1080/14786448308627346\"\u003eOn the Number of Fractions Contained in any \"Farey series\" of which the Limiting Number is Given\u003c/a\u003e, Philosophical Magazine, Series 5, Vol. 15, No. 94 (1883), pp. 251-257."
			],
			"example": [
				"A002088(820) = 204376, 3*820^2/(Pi^2) = 204385.091643... \u003e 204376, thus 820 is in this sequence."
			],
			"maple": [
				"F:= ListTools:-PartialSums(map(numtheory:-phi, [$1..30000])):",
				"select(t -\u003e is(F[t] \u003c 3*t^2/Pi^2), [$1..30000]); # _Robert Israel_, Apr 21 2017"
			],
			"mathematica": [
				"s = 0; k = 1; lst = {}; While[k \u003c 50001, s = s + EulerPhi@k; If[s*Pi^2 \u003c 3 k^2, AppendTo[lst, k]]; k++]; lst"
			],
			"xref": [
				"Cf. A000010, A002088."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Amiram Eldar_, Apr 08 2017",
			"references": 3,
			"revision": 31,
			"time": "2020-02-27T17:54:17-05:00",
			"created": "2017-04-30T22:50:13-04:00"
		}
	]
}