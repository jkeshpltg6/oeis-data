{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A054242",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 54242,
			"data": "1,1,1,1,2,1,2,3,3,2,2,5,5,5,2,3,7,9,9,7,3,4,10,14,17,14,10,4,5,14,21,27,27,21,14,5,6,19,31,42,46,42,31,19,6,8,25,44,64,74,74,64,44,25,8,10,33,61,93,116,123,116,93,61,33,10",
			"name": "Triangle read by rows: row n (n\u003e=0) gives the number of partitions of (n,0), (n-1,1), (n-2,2), ..., (0,n) respectively into sums of distinct pairs.",
			"comment": [
				"By analogy with ordinary partitions into distinct parts (A000009). The empty partition gives T(0,0)=1 by definition. A054225 and A201376 give pair partitions with repeats allowed.",
				"Also number of partitions into pairs which are not both even.",
				"In the paper by S. M. Luthra: \"Partitions of bipartite numbers when the summands are unequal\", the square table on page 370 contains an errors. In the formula (6, p. 372) for fixed m there should be factor 1/m!. The correct asymptotic formula is q(m, n) ~ (sqrt(12*n)/Pi)^m * exp(Pi*sqrt(n/3)) / (4*3^(1/4)*m!*n^(3/4)). The same error is also in article by F. C. Auluck (see A054225). - _Vaclav Kotesovec_, Feb 02 2016"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A054242/b054242.txt\"\u003eRows n = 0..75, flattened\u003c/a\u003e",
				"S. M. Luthra, \u003ca href=\"http://www.insa.nic.in/writereaddata/UpLoadedFiles/PINSA/Vol23A_1957_5_Art04.pdf\"\u003ePartitions of bipartite numbers when the summands are unequal\u003c/a\u003e, Proceedings of the Indian National Science Academy, vol.23, 1957, issue 5A, p. 370-376. [broken link]",
				"Reinhard Zumkeller, \u003ca href=\"/A054225/a054225_1.lhs.txt\"\u003eHaskell programs for A054225, A054242, A201376, A201377\u003c/a\u003e"
			],
			"formula": [
				"G.f.: (1/2)*Product(1+x^i*y^j), i, j\u003e=0."
			],
			"example": [
				"The second row (n=1) is 1,1 since (1,0) and (0,1) each have a single partition.",
				"The third row (n=2) is 1, 2, 1 from (2,0), (1,1) or (1,0)+(0,1), (0,2).",
				"In the fourth row, T(1,3)=5 from (1,3), (0,3)+(1,0), (0,2)+(1,1), (0,2)+(0,1)+(1,0), (0,1)+(1,2).",
				"The triangle begins:",
				"  1;",
				"  1,  1;",
				"  1,  2,  1;",
				"  2,  3,  3,  2;",
				"  2,  5,  5,  5,  2;",
				"  3,  7,  9,  9,  7,  3;",
				"  4, 10, 14, 17, 14, 10,  4;",
				"  5, 14, 21, 27, 27, 21, 14,  5;",
				"  6, 19, 31, 42, 46, 42, 31, 19,  6;",
				"  8, 25, 44, 64, 74, 74, 64, 44, 25, 8;",
				"  ..."
			],
			"mathematica": [
				"max = 10; f[x_, y_] := Product[1 + x^n*y^k, {n, 0, max}, {k, 0, max}]/2; se = Series[f[x, y], {x, 0, max}, {y, 0, max}] ; coes = CoefficientList[ se, {x, y}]; t[n_, k_] := coes[[n-k+1, k+1]]; Flatten[ Table[ t[n, k], {n, 0, max}, {k, 0, n}]] (* _Jean-François Alcover_, Dec 06 2011 *)"
			],
			"program": [
				"(Haskell) see Zumkeller link."
			],
			"xref": [
				"See A201377 for the same triangle formatted in a different way.",
				"The outer diagonals are T(n,0) = T(n,n) = A000009(n).",
				"Cf. A054225.",
				"T(2*n,n) = A219554(n). Row sums give A219555. - _Alois P. Heinz_, Nov 22 2012",
				"Columns 0-5: A000009, A036469, A268345, A268346, A268347, A268348."
			],
			"keyword": "easy,nonn,tabl,nice",
			"offset": "0,5",
			"author": "_Marc LeBrun_, Feb 08 2000 and Jul 01 2003",
			"ext": [
				"Entry revised by _N. J. A. Sloane_, Nov 30 2011, to incorporate corrections provided by _Reinhard Zumkeller_, who also contributed the alternative version A201377."
			],
			"references": 13,
			"revision": 92,
			"time": "2020-08-17T01:59:37-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}