{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A276422",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 276422,
			"data": "1,0,1,2,0,0,1,1,0,1,4,0,0,0,1,2,2,0,2,0,1,8,0,0,1,1,0,1,4,4,0,4,0,2,0,1,14,0,0,2,2,1,1,0,2,9,6,0,7,0,4,0,2,0,2,24,1,0,4,3,2,2,1,3,0,2,16,10,0,12,0,8,0,4,1,3,0,2,41,1,0,7,5,4,4,2,6,1,3,0,3,28,16,0,20,0,14,0,8,2,6,1,3,0,3",
			"name": "Triangle read by rows: T(n,k) is the number of partitions of n for which the sum of its odd singletons is k (0\u003c=k\u003c=n). A singleton in a partition is a part that occurs exactly once.",
			"comment": [
				"T(n,0) = A265256(n).",
				"T(n,n) = A000700(n).",
				"Sum(k*T(n,k), k\u003e=0) = A276423(n).",
				"Sum(T(n,k), k\u003e=0) = A000041(n)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A276422/b276422.txt\"\u003eRows n = 0..200, flattened\u003c/a\u003e"
			],
			"formula": [
				"G.f.:  G(t,x) = Product(((1-x^{2j-1})(1+t^{2j-1}x^{2j-1}) + x^{4j-2})/(1-x^j), j=1..infinity)."
			],
			"example": [
				"Row 4 is 4, 0, 0, 0, 1 because in the partitions [1,1,1,1], [1,1,2], [2,2], [1,3], [4] the sums of the odd singletons are 0, 0, 0, 4, 0, respectively.",
				"Row 5 is 2, 2, 0, 2, 0, 1 because in the partitions [1,1,1,1,1], [1,1,1,2], [1,2,2], [1,1,3], [2,3], [1,4], [5] the sums of the odd singletons are 0, 0, 1, 3, 3, 1, 5, respectively.",
				"Triangle starts:",
				"1;",
				"0,1;",
				"2,0,0;",
				"1,1,0,1;",
				"4,0,0,0,1;",
				"2,2,0,2,0,1."
			],
			"maple": [
				"g := Product(((1-x^(2*j-1))*(1+t^(2*j-1)*x^(2*j-1))+x^(4*j-2))/(1-x^j), j = 1 .. 100): gser := simplify(series(g, x = 0, 23)): for n from 0 to 20 do P[n] := sort(coeff(gser, x, n)) end do: for n from 0 to 20 do seq(coeff(P[n], t, i), i = 0 .. n) end do; # yields sequence in triangular form",
				"# second Maple program:",
				"b:= proc(n, i) option remember; expand(",
				"      `if`(n=0, 1, `if`(i\u003c1, 0, add(b(n-i*j, i-1)*",
				"      `if`(j=1 and i::odd, x^i, 1), j=0..n/i))))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=0..n))(b(n$2)):",
				"seq(T(n), n=0..14);  # _Alois P. Heinz_, Sep 14 2016"
			],
			"mathematica": [
				"b[n_, i_] := b[n, i] = Expand[If[n == 0, 1, If[i \u003c 1, 0, Sum[b[n - i*j, i - 1]*If[j == 1 \u0026\u0026 OddQ[i], x^i, 1], {j, 0, n/i}]]]]; T[n_] := Function[p, Table[Coefficient[p, x, i], {i, 0, n}]][b[n, n]]; Table[T[n], {n, 0, 14}] // Flatten (* _Jean-François Alcover_, Oct 04 2016, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A000041, A000700, A265256, A276423, A276424, A276425."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_Emeric Deutsch_, Sep 14 2016",
			"references": 4,
			"revision": 18,
			"time": "2016-10-04T06:00:27-04:00",
			"created": "2016-09-14T16:47:09-04:00"
		}
	]
}