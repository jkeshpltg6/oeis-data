{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A274131",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 274131,
			"data": "6,48,96,960,160,1536,5760,30720,725760,1935360,34560,165888,23224320,1161216,4644864,92897280,4644864,5806080,663552,21233664,464486400,3715891200,232243200,619315200,11354112,81749606400,185794560,2123366400,26542080,70778880",
			"name": "Irregular triangle T(n,m), denominators of coefficients in a power/Fourier series expansion of the plane pendulum's exact time dependence.",
			"comment": [
				"Irregular triangle read by rows (see example). The row length sequence is 2*n = A005843(n), n \u003e= 1.",
				"  The numerator triangle is A274130.",
				"Comments of A274130 give a definition of the fraction triangle, which determines to arbitrary precision the time dependence for the time-independent solution (cf. A273506, A273507) of the plane pendulum's equations of motion. For more details see \"Plane Pendulum and Beyond by Phase Space Geometry\" (Klee, 2016)."
			],
			"link": [
				"Bradley Klee, \u003ca href=\"http://arxiv.org/abs/1605.09102\"\u003ePlane Pendulum and Beyond by Phase Space Geometry\u003c/a\u003e, arXiv:1605.09102 [physics.class-ph], 2016."
			],
			"example": [
				"n\\m  1      2      3          4       5       6",
				"------------------------------------------------------",
				"1  | 6     48",
				"2  | 96    960    160      1536",
				"3  | 5760  30720  725760   1935360  34560   165888",
				"------------------------------------------------------",
				"row 4: 23224320, 1161216, 4644864, 92897280, 4644864, 5806080, 663552, 21233664,",
				"row 5: 464486400, 3715891200, 232243200, 619315200, 11354112, 81749606400, 185794560, 2123366400, 26542080, 70778880."
			],
			"mathematica": [
				"R[n_] := Sqrt[4 k] Plus[1, Total[k^# R[#, Q] \u0026 /@ Range[n]]]",
				"Vq[n_] :=  Total[(-1)^(# - 1) (r Cos[Q] )^(2 #)/((2 #)!) \u0026 /@ Range[2, n]]",
				"RRules[n_] :=  With[{H = ReplaceAll[1/2 r^2 + (Vq[n + 1]), {r -\u003e R[n]}]},",
				"Function[{rules}, Nest[Rule[#[[1]], ReplaceAll[#[[2]], rules]] \u0026 /@ # \u0026, rules, n]][",
				"   Flatten[R[#, Q] -\u003e  Expand[(-1/4) ReplaceAll[ Coefficient[H, k^(# + 1)], {R[#, Q] -\u003e 0}]] \u0026 /@ Range[n]]]]",
				"dt[n_] := With[{rules = RRules[n]}, Expand[Subtract[ Times[Expand[D[R[n] /. rules, Q]], Normal@Series[1/R[n], {k, 0, n}] /. rules, Cot[Q] ], 1]]]",
				"t[n_] := Expand[ReplaceAll[Q TrigReduce[dt[n]], Cos[x_ Q] :\u003e (1/x/Q) Sin[x Q]]]",
				"tCoefficients[n_] := With[{tn = t[n]},Function[{a}, Coefficient[Coefficient[tn, k^a], Sin[2 # Q] ] \u0026 /@ Range[2 a]] /@ Range[n]]",
				"Flatten[Denominator[-tCoefficients[10]]]"
			],
			"xref": [
				"Numerators: A274130. Phase Space Trajectory: A273506, A273507. Time Dependence: A274076, A274078. Elliptic K: A038534, A056982. Cf. A000984, A001790, A038533, A046161, A273496."
			],
			"keyword": "nonn,tabf",
			"offset": "1,1",
			"author": "_Bradley Klee_, Jun 10 2016",
			"references": 10,
			"revision": 15,
			"time": "2018-12-29T03:27:36-05:00",
			"created": "2016-06-14T13:26:35-04:00"
		}
	]
}