{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A098292",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 98292,
			"data": "1,730,533629,390082069,285149458810,208443864308041,152372179659719161,111383854887390398650,81421445550502721693989,59518965313562602167907309,43508282222768711682018548890",
			"name": "First differences of Chebyshev polynomials S(n,731)=A098263(n) with Diophantine property.",
			"comment": [
				"(27*b(n))^2 - 733*a(n)^2 = -4 with b(n)=A098291(n) give all positive solutions of this Pell equation."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A098292/b098292.txt\"\u003eTable of n, a(n) for n = 0..340\u003c/a\u003e",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"Giovanni Lucca, \u003ca href=\"http://forumgeom.fau.edu/FG2019volume19/FG201902index.html\"\u003eInteger Sequences and Circle Chains Inside a Hyperbola\u003c/a\u003e, Forum Geometricorum (2019) Vol. 19, 11-16.",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (731,-1)."
			],
			"formula": [
				"a(n) = ((-1)^n)*S(2*n, 27*i) with the imaginary unit i and the S(n, x) = U(n, x/2) Chebyshev polynomials.",
				"G.f.: (1-x)/(1-731*x+x^2).",
				"a(n) = S(n, 731) - S(n-1, 731) = T(2*n+1, sqrt(733)/2)/(sqrt(733)/2), with S(n, x) = U(n, x/2) Chebyshev's polynomials of the second kind, A049310. S(-1, x)= 0 = U(-1, x) and T(n, x) Chebyshev's polynomials of the first kind, A053120.",
				"a(n) = 731*a(n-1) - a(n-2), n \u003e 1; a(0)=1, a(1)=730. - _Philippe Deléham_, Nov 18 2008"
			],
			"example": [
				"All positive solutions of Pell equation x^2 - 733*y^2 = -4 are (27=27*1,1), (19764=27*732,730), (14447457=27*535091,533629), (10561071303=27*391150789,390082069), ..."
			],
			"mathematica": [
				"LinearRecurrence[{731,-1},{1,730},20] (* _Harvey P. Dale_, Nov 15 2013 *)"
			],
			"program": [
				"(PARI) my(x='x+O('x^20)); Vec((1-x)/(1-731*x+x^2)) \\\\ _G. C. Greubel_, Aug 01 2019",
				"(MAGMA) I:=[1,730]; [n le 2 select I[n] else 731*Self(n-1) - Self(n-2): n in [1..20]]; // _G. C. Greubel_, Aug 01 2019",
				"(Sage) ((1-x)/(1-731*x+x^2)).series(x, 20).coefficients(x, sparse=False) # _G. C. Greubel_, Aug 01 2019",
				"(GAP) a:=[1,730];; for n in [3..20] do a[n]:=731*a[n-1]-a[n-2]; od; a; # _G. C. Greubel_, Aug 01 2019"
			],
			"xref": [
				"Cf. A098291."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Sep 10 2004",
			"references": 4,
			"revision": 30,
			"time": "2020-01-23T03:46:14-05:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}