{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A104984",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 104984,
			"data": "1,-1,1,-1,-2,1,-3,-1,-3,1,-13,-3,-1,-4,1,-71,-13,-3,-1,-5,1,-461,-71,-13,-3,-1,-6,1,-3447,-461,-71,-13,-3,-1,-7,1,-29093,-3447,-461,-71,-13,-3,-1,-8,1,-273343,-29093,-3447,-461,-71,-13,-3,-1,-9,1,-2829325,-273343,-29093,-3447,-461,-71,-13,-3,-1,-10,1",
			"name": "Matrix inverse of triangle A104980.",
			"comment": [
				"Inverse matrix A104980 satisfies: SHIFT_LEFT(column 0 of A104980^p) = p*(column p+1 of A104980) for p\u003e=0."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A104984/b104984.txt\"\u003eRows n = 0..50 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, n) = 1, T(n+1, n) = -(n+1) for n \u003e= 0; otherwise T(n, k) = T(n-k, 0) = -A003319(n-k-1) for n \u003e k+1 and k \u003e= 0.",
				"Sum_{k=0..n} T(n, k) = A104985(n). - _G. C. Greubel_, Jun 07 2021"
			],
			"example": [
				"Triangle begins:",
				"       1;",
				"      -1,     1;",
				"      -1,    -2,    1;",
				"      -3,    -1,   -3,   1;",
				"     -13,    -3,   -1,  -4,   1;",
				"     -71,   -13,   -3,  -1,  -5,  1;",
				"    -461,   -71,  -13,  -3,  -1, -6,  1;",
				"   -3447,  -461,  -71, -13,  -3, -1, -7,  1;",
				"  -29093, -3447, -461, -71, -13, -3, -1, -8, 1; ..."
			],
			"mathematica": [
				"A003319[n_]:= A003319[n]= If[n==0, 0, n! - Sum[j!*A003319[n-j], {j,n-1}]];",
				"T[n_, k_]:= If[k==n, 1, If[k==n-1, -n, -A003319[n-k]]];",
				"Table[T[n, k], {n,0,12}, {k,0,n}]//Flatten (* _G. C. Greubel_, Jun 07 2021 *)"
			],
			"program": [
				"(PARI) T(n,k)=if(n==k,1,if(n==k+1,-n,-(n-k)!-sum(i=1,n-k-1,i!*T(n-k-i,0))));",
				"(Sage)",
				"@CachedFunction",
				"def T(n,k):",
				"    if (k==n): return 1",
				"    elif (k==n-1): return -n",
				"    else: return -factorial(n-k) - sum( factorial(j)*T(n-k-j, 0) for j in (1..n-k-1) )",
				"[[T(n,k) for k in (0..n)] for n in (0..12)] # _G. C. Greubel_, Jun 07 2021"
			],
			"xref": [
				"Cf. A104980, A104985 (row sums)."
			],
			"keyword": "sign,tabl",
			"offset": "0,5",
			"author": "_Paul D. Hanna_, Apr 10 2005",
			"references": 4,
			"revision": 10,
			"time": "2021-06-08T14:58:01-04:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}