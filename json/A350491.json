{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A350491",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 350491,
			"data": "1,0,1,0,0,1,1,0,0,0,1,4,4,1,0,0,0,0,1,9,25,32,22,8,1,0,0,0,0,0,1,17,92,259,441,496,379,195,66,13,1,0,0,0,0,0,0,1,28,259,1286,4026,8754,13930,16686,15289,10785,5842,2397,722,151,19,1",
			"name": "Triangle read by rows: T(n,k) is the number of acyclic digraphs on n unlabeled nodes with k arcs and a global source and sink, n \u003e= 1, k = 0..n*(n-1)/2.",
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A350491/b350491.txt\"\u003eTable of n, a(n) for n = 1..1350\u003c/a\u003e (rows 1..20)"
			],
			"example": [
				"Triangle begins:",
				"  [1] 1;",
				"  [2] 0, 1;",
				"  [3] 0, 0, 1, 1;",
				"  [4] 0, 0, 0, 1, 4, 4,  1;",
				"  [5] 0, 0, 0, 0, 1, 9, 25, 32,  22,   8,   1;",
				"  [6] 0, 0, 0, 0, 0, 1, 17, 92, 259, 441, 496, 379, 195, 66, 13, 1;",
				"  ..."
			],
			"program": [
				"(PARI) \\\\ See PARI link in A122078 for program code.",
				"{ my(A=A350491rows(7)); for(i=1, #A, print(A[i])) }"
			],
			"xref": [
				"Row sums are A345258.",
				"Column sums are A350492.",
				"Cf. A122078, A350447, A350449, A350488."
			],
			"keyword": "nonn,tabf,new",
			"offset": "1,12",
			"author": "_Andrew Howroyd_, Jan 08 2022",
			"references": 4,
			"revision": 10,
			"time": "2022-01-09T22:22:00-05:00",
			"created": "2022-01-08T19:26:03-05:00"
		}
	]
}