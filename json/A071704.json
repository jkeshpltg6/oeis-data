{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A071704",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 71704,
			"data": "0,0,0,2,5,7,10,14,16,24,29,31,42,40,43,52,62,70,75,87,82,96,102,112,127,137,136,142,154,154,186,199,204,215,233,248,250,262,272,284,309,324,344,334,348,358,406,414,430,446,441,489,486,511,508",
			"name": "Number of ways to represent the n-th prime as arithmetic mean of three other odd primes.",
			"link": [
				"Robert Israel, \u003ca href=\"/A071704/b071704.txt\"\u003eTable of n, a(n) for n = 1..2000\u003c/a\u003e"
			],
			"example": [
				"a(5)=5 as A000040(5)=11 and there are no more representations not containing 11 than 11 = (3+7+23)/3 = (3+13+17)/3 = (5+5+23)/3 = (7+7+19)/3 = (7+13+13)/3."
			],
			"maple": [
				"N:= 300: # to get the first A000720(N) terms",
				"P:= select(isprime, [seq(i,i=3..3*N,2)]):",
				"nP:= nops(P):",
				"V:= Vector(N):",
				"for i from 1 to nP do",
				"  for j from i to nP do",
				"    for k from j to nP while P[i]+P[j]+P[k] \u003c= 3*N do",
				"      r:= (P[i]+P[j]+P[k])/3;",
				"      if r::integer and isprime(r) and r \u003c\u003e P[j] and r \u003c= N then V[r]:= V[r]+1 fi",
				"od od od:",
				"seq(V[ithprime(i)],i=1..numtheory:-pi(N)); # _Robert Israel_, Aug 09 2018"
			],
			"mathematica": [
				"M = 300; (* to get the first A000720(M) *)",
				"P = Select[Range[3, 3*M, 2], PrimeQ]; nP = Length[P]; V = Table[0, {M}];",
				"For[i = 1, i \u003c= nP, i++,",
				"For[j = i, j \u003c= nP, j++,",
				"For[k = j, k \u003c= nP \u0026\u0026 P[[i]] + P[[j]] + P[[k]] \u003c= 3*M , k++, r = (P[[i]] + P[[j]] + P[[k]])/3; If[IntegerQ[r] \u0026\u0026 PrimeQ[r] \u0026\u0026 r != P[[j]] \u0026\u0026 r \u003c= M, V[[r]] = V[[r]]+1]",
				"]]];",
				"Table[V[[Prime[i]]], {i, 1, PrimePi[M]}] (* _Jean-François Alcover_, Mar 09 2019, after _Robert Israel_ *)"
			],
			"program": [
				"(Haskell)",
				"a071704 n = z (us ++ vs) 0 (3 * q)  where",
				"   z _ 3 m = fromEnum (m == 0)",
				"   z ps'@(p:ps) i m = if m \u003c p then 0 else z ps' (i+1) (m - p) + z ps i m",
				"   (us, _:vs) = span (\u003c q) a065091_list; q = a000040 n",
				"-- _Reinhard Zumkeller_, May 24 2015",
				"(PARI) a(n, p=prime(n))=my(s=0); forprime(q=p+2, 3*p-4, my(t=3*p-q); forprime(r=max(t-q, 3), (3*p-q)\\2, if(t!=p+r \u0026\u0026 isprime(t-r), s++))); s \\\\ _Charles R Greathouse IV_, Jun 04 2015"
			],
			"xref": [
				"Cf. A071681, A071703, A000040, A065091, A258233."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Reinhard Zumkeller_, Jun 03 2002",
			"ext": [
				"Definition corrected by _Zak Seidov_, May 24 2015"
			],
			"references": 4,
			"revision": 20,
			"time": "2019-03-09T07:22:55-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}