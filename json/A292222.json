{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A292222",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 292222,
			"data": "1,1,2,1,3,6,1,10,12,24,1,15,50,60,120,1,41,180,300,360,720,1,63,497,1260,2100,2520,5040,1,162,1484,6496,10080,16800,20160,40320,1,255,5154,20916,58464,90720,151200,181440,362880,1,637,13680,95640,322560,584640,907200,1512000,1814400,3628800",
			"name": "Triangle corresponding to the partition array of the M_1 multinomials (A036038).",
			"comment": [
				"Abramowitz-Stegun (A-St) M_1 multinomials as partition array (partitions in A-St order) are given in A036038. See this for details.",
				"This is the sub-triangle of A226874(n,k) for n \u003e= k \u003e= 1 (here k=m).",
				"The M_1 multinomials for a partition written in exponent form P = [1^e[1], 2^e[2], ... n^e[n]] with nonnegative e[j], for j =1, ..., n, is M_1(P) = n!/Product_{j=1..n} j!^e[j]. See the A-St link."
			],
			"link": [
				"Milton Abramowitz and Irene A. Stegun, editors, \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP?Res=150\u0026amp;Page=831\u0026amp;Submit=Go\"\u003eMultinomials: M_1, M_2 and M_3\u003c/a\u003e, Handbook of Mathematical Functions, December 1972, pp. 831-2."
			],
			"formula": [
				"T(n, m) = sum over the A036038 entries in row n with parts number m, for m \u003e= n \u003e= 1."
			],
			"example": [
				"The triangle T(n, m) begins:",
				"n\\m  1   2     3     4      5      6      7       8       9      10 ...",
				"1:   1",
				"2:   1   2",
				"3:   1   3     6",
				"4:   1  10    12    24",
				"5:   1  15    50    60    120",
				"6:   1  41   180   300    360    720",
				"7:   1  63   497  1260   2100   2520   5040",
				"8:   1 162  1484  6496  10080  16800  20160   40320",
				"9:   1 255  5154 20916  58464  90720 151200  181440  362880",
				"10:  1 637 13680 95640 322560 584640 907200 1512000 1814400 3628800",
				"...",
				"T(5, 3) =50 because the partitions are [1^2, 3^1] and [1^1, 2^2] with M_1 numbers 20 = A036038(5, 4) and 30 = A036038(5, 5), respectively, adding to 50."
			],
			"mathematica": [
				"b[n_, i_, t_] := b[n, i, t] = If[t == 1, 1/n!, Sum[b[n - j, j, t - 1]/j!, {j, i, n/t}]];",
				"t[n_, k_] := If[n*k == 0, If[n == k, 1, 0], n!*b[n, 1, k]];",
				"Table[Table[t[n, k], {k, 1, n}], {n, 1, 10}] // Flatten (* _Jean-François Alcover_, Sep 29 2017, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A036038, A130534 (M_2 triangle = |Stirling1|), A008277 (M_3 triangle = Stirling2), A226874 (M_1 triangle including empty partition)."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "1,3",
			"author": "_Wolfdieter Lang_, Sep 29 2017",
			"references": 1,
			"revision": 19,
			"time": "2018-12-07T19:01:47-05:00",
			"created": "2017-10-01T00:07:34-04:00"
		}
	]
}