{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A156692",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 156692,
			"data": "1,1,1,1,-7,1,1,77,77,1,1,-1155,12705,-1155,1,1,21945,3620925,3620925,21945,1,1,-504735,1582344225,-23735163375,1582344225,-504735,1,1,13627845,982635763725,280051192661625,280051192661625,982635763725,13627845,1",
			"name": "Triangle T(n, k, m) = t(n,m)/( t(k,m) * t(n-k,m) ) with T(n, 0, m) = T(n, n, m) = 1, where t(n, m) = Product_{j=1..n} Product_{i=1..j-1} ( 1 - (m+1)*(i+1) ) and m = 3, read by rows.",
			"comment": [
				"Row sums are: {1, 2, -5, 156, 10397, 7285742, -20571484393, 562067684106392, 91653158600215578137, ...}."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A156692/b156692.txt\"\u003eRows n = 0..30 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k, m) = t(n,m)/( t(k,m) * t(n-k,m) ) with T(n, 0, m) = T(n, n, m) = 1, where t(n, m) = Product_{j=1..n} Product_{i=1..j-1} ( 1 - (m+1)*(i+1) ) and m = 3.",
				"T(n, k, m, p, q) = (-p*(m+1))^(k*(n-k)) * (f(n,m,p,q)/(f(k,m,p,q)*f(n-k,m,p,q))) where Product_{j=1..n} Pochhammer( (q*(m+1) -1)/(p*(m+1)), j) for (m, p, q) = (3, 1, 1). - _G. C. Greubel_, Feb 25 2021"
			],
			"example": [
				"Triangle begins as:",
				"  1;",
				"  1,       1;",
				"  1,      -7,          1;",
				"  1,      77,         77,            1;",
				"  1,   -1155,      12705,        -1155,          1;",
				"  1,   21945,    3620925,      3620925,      21945,       1;",
				"  1, -504735, 1582344225, -23735163375, 1582344225, -504735, 1;"
			],
			"mathematica": [
				"(* First program *)",
				"t[n_, k_]:= If[k==0, n!, Product[1 -(i+1)*(k+1), {j,n}, {i,0,j-1}] ];",
				"T[n_, k_, m_]:= If[n==0, 1, t[n, m]/(t[k, m]*t[n-k, m])];",
				"Table[T[n,k,3], {n,0,12}, {k,0,n}]//Flatten (* modified by _G. C. Greubel_, Feb 25 2021 *)",
				"(* Second program *)",
				"f[n_, m_, p_, q_]:= Product[Pochhammer[(q*(m+1) -1)/(p*(m+1)), j], {j,n}];",
				"T[n_, k_, m_, p_, q_]:= (-p*(m+1))^(k*(n-k))*(f[n,m,p,q]/(f[k,m,p,q]*f[n-k,m,p,q]));",
				"Table[T[n,k,3,1,1], {n,0,12}, {k,0,n}]//Flatten (* _G. C. Greubel_, Feb 25 2021 *)"
			],
			"program": [
				"(Sage)",
				"@CachedFunction",
				"def f(n, m, p, q): return product( rising_factorial( (q*(m+1)-1)/(p*(m+1)), j) for j in (1..n))",
				"def T(n, k, m, p, q): return (-p*(m+1))^(k*(n-k))*(f(n,m,p,q)/(f(k,m,p,q)*f(n-k,m,p,q)))",
				"flatten([[T(n,k,3,1,1) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, Feb 25 2021",
				"(Magma)",
				"f:= func\u003c n,m,p,q | n eq 0 select 1 else m eq 0 select Factorial(n) else (\u0026*[ 1 -(p*i+q)*(m+1): i in [0..j], j in [0..n-1]]) \u003e;",
				"T:= func\u003c n,k,m,p,q | f(n,m,p,q)/(f(k,m,p,q)*f(n-k,m,p,q)) \u003e;",
				"[T(n,k,3,1,1): k in [0..n], n in [0..12]]; // _G. C. Greubel_, Feb 25 2021"
			],
			"xref": [
				"Cf. A007318 (m=0), A156690 (m=1), A156691 (m=2), this sequence (m=3).",
				"Cf. A156693, A156698, A156727."
			],
			"keyword": "sign,tabl",
			"offset": "0,5",
			"author": "_Roger L. Bagula_, Feb 13 2009",
			"ext": [
				"Edited by _G. C. Greubel_, Feb 25 2021"
			],
			"references": 8,
			"revision": 9,
			"time": "2021-02-28T03:16:27-05:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}