{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A051797",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 51797,
			"data": "1,12,50,140,315,616,1092,1800,2805,4180,6006,8372,11375,15120,19720,25296,31977,39900,49210,60060,72611,87032,103500,122200,143325,167076,193662,223300,256215,292640,332816,376992,425425,478380,536130",
			"name": "Partial sums of A007585.",
			"comment": [
				"a(n-1) is the n-th antidiagonal sum of the convolution array A213835. - _Clark Kimberling_, Jul 04 2012",
				"Convolution of A000027 with A001107 (excluding 0). - _Bruno Berselli_, Dec 07 2012"
			],
			"reference": [
				"A. H. Beiler, Recreations in the Theory of Numbers, Dover, N.Y., 1964, pp. 194-196.",
				"Murray R. Spiegel, Calculus of Finite Differences and Difference Equations, \"Schaum's Outline Series\", McGraw-Hill, 1971, pp. 10-20, 79-94.",
				"Herbert John Ryser, Combinatorial Mathematics, \"The Carus Mathematical Monographs\", No. 14, John Wiley and Sons, 1963, pp. 1-8."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A051797/b051797.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Ps#pyramidal_numbers\"\u003eIndex to sequences related to pyramidal numbers\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (5,-10,10,-5,1)."
			],
			"formula": [
				"a(n) = binomial(n+3,3)*(2*n+1) = (n+1)*(n+2)*(n+3)*(2*n+1)/6.",
				"G.f.: (1+7*x)/(1-x)^5.",
				"a(n) = A080851(8,n). - _R. J. Mathar_, Jul 28 2016",
				"E.g.f.: (6 + 66*x + 81*x^2 + 25*x^3 + 2*x^4)*exp(x)/6. - _G. C. Greubel_, Aug 30 2019"
			],
			"maple": [
				"seq((2*n+1)*binomial(n+3,3), n=0..40); # _G. C. Greubel_, Aug 30 2019"
			],
			"mathematica": [
				"Table[(2*n+1)*Binomial[n+3,3], {n,0,40}] (*  _Vladimir Joseph Stephan Orlovsky_, Apr 19 2011, modified by _G. C. Greubel_, Aug 30 2019 *)"
			],
			"program": [
				"(MAGMA) /* A000027 convolved with A001107 (excluding 0): */",
				"A001107:=func\u003cn | n*(4*n-3)\u003e; [\u0026+[(n-i+1)*A001107(i): i in [1..n]]: n in [1..35]]; // _Bruno Berselli_, Dec 07 2012",
				"(MAGMA) [(2*n+1)*Binomial(n+3,3): n in [0..40]]; // _G. C. Greubel_, Aug 30 2019",
				"(PARI) vector(40, n, (2*n-1)*binomial(n+2,3)) \\\\ _G. C. Greubel_, Aug 30 2019",
				"(Sage) [(2*n+1)*binomial(n+3,3) for n in (0..40)] # _G. C. Greubel_, Aug 30 2019",
				"(GAP) List([0..40], n-\u003e (2*n+1)*Binomial(n+3,3)) # _G. C. Greubel_, Aug 30 2019"
			],
			"xref": [
				"Cf. A001107, A007585.",
				"Cf. A093565 ((8, 1) Pascal, column m=4).",
				"Cf. A220212 for a list of sequences produced by the convolution of the natural numbers with the k-gonal numbers."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Barry E. Williams_, Dec 11 1999",
			"references": 8,
			"revision": 40,
			"time": "2019-08-30T15:20:08-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}