{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A326676",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 326676,
			"data": "1,0,1,0,1,1,0,0,2,1,0,0,1,3,1,0,0,1,3,4,1,0,0,1,3,6,5,1,0,0,0,4,7,10,6,1,0,0,0,3,10,14,15,7,1,0,0,0,2,11,21,25,21,8,1,0,0,0,1,10,28,40,41,28,9,1,0,0,0,1,9,31,60,71,63,36,10,1",
			"name": "Triangular array: T(n,k) equals the number of n triangle stacks of large Schröder type with k down-triangles in the bottom row of the stack.",
			"comment": [
				"We define two types of plane triangles of unit area - up-triangles with vertices at the lattice points (x, y), (x+1, y+1) and (x+2, y) and down-triangles with vertices at the lattice points (x, y), (x-1, y+1) and (x+1, y+1).",
				"To construct a triangle stack of large Schröder type we start with a horizontal row of k contiguous down-triangles forming the base row of the stack. Subsequent rows of the stack are formed by placing up-triangles on some, all or none of the down-triangles of the previous row. In the spaces between pairs of adjacent up-triangles further down-triangles may be placed. For an example, see the illustration in the Links section. There is an obvious bijective correspondence between triangle stacks of large Schröder type with a base of k down-triangles and large Schröder paths of semilength k. For another version of this array see A129179.",
				"For triangle stacks of small Schröder type, where the base row consists of contiguous up-triangles, see A224704."
			],
			"link": [
				"P. Bala, \u003ca href=\"/A326676/a326676.png\"\u003eExample of triangle stack of large Schröder type\u003c/a\u003e",
				"P. Bala, \u003ca href=\"/A326676/a326676.pdf\"\u003eTriangle Stacks of large Schröder type\u003c/a\u003e"
			],
			"formula": [
				"O.g.f. as a continued fraction: (q marks the area of the stack and b marks down-triangles in the base of the stack)",
				"A(q,b) = 1/(1 - q*b - q^2*b/(1 - q^3*b - q^4*b/(1 - q^5*b - q^6*b/( (...) )))) = 1 + b*q + (b + b^2)*q^2 + (2*b^2 + b^3)*q^3 + (b^2 + 3*b^3 + b^4)*q^4 + ....",
				"A(q,b) = 1/(1 - (q + q^2)*b/(1 - q^4*b/(1 - (q^3 + q^6)*b/(1 - q^8*b/(1 - (q^5 + q^10)*b/(1 - q^12*b/( (...) ))))))).",
				"O.g.f. as a ratio of q-series: N(q,b)/D(q,b), where N(q,b) = Sum_{n \u003e= 0} (-1)^n*q^(2*n^2+2*n)*b^n/( (Product_{k = 1..n} 1 - q^(2*k)) * (Product_{k = 1..n+1} 1 - q^(2*k-1)*b) ) and D(q,b) = Sum_{n \u003e= 0} (-1)^n*q^(2*n^2)*b^n/( (Product_{k = 1..n} 1 - q^(2*k)) * (Product_{k = 1..n} 1 - q^(2*k-1)*b) )."
			],
			"example": [
				"Triangle begins",
				"   n\\k  0  1   2   3   4   5   6   7   8   9  10",
				"   - - - - - - - - - - - - - - - - - - - - - - -",
				"   0 |  1",
				"   1 |  0  1",
				"   2 |  0  1   1",
				"   3 |  0  0   2   1",
				"   4 |  0  0   1   3   1",
				"   5 |  0  0   1   3   4   1",
				"   6 |  0  0   1   3   6   5   1",
				"   7 |  0  0   0   4   7  10   6   1",
				"   8 |  0  0   0   3  10  14  15   7   1",
				"   9 |  0  0   0   2  11  21  25  21   8  1",
				"  10 |  0  0   0   1  10  28  40  41  28  9  1",
				"..."
			],
			"xref": [
				"Row sums A088352. Column sums A006318. Cf. A047998, A129179, A224704."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "0,9",
			"author": "_Peter Bala_, Jul 17 2019",
			"references": 3,
			"revision": 17,
			"time": "2019-08-02T08:48:44-04:00",
			"created": "2019-07-29T12:00:53-04:00"
		}
	]
}