{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A188725",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 188725,
			"data": "6,4,3,8,5,0,0,9,6,3,0,6,5,4,0,8,3,9,7,2,2,3,2,3,2,5,6,3,5,9,4,6,9,1,7,2,9,2,6,2,1,6,6,5,4,0,8,1,3,2,6,1,5,2,5,6,1,0,6,5,1,7,3,2,5,8,9,5,9,2,1,2,6,3,3,4,3,7,5,1,1,6,9,3,8,6,9,6,6,9,2,7,7,2,1,5,3,0,9,8,5,0,0,3,9,3,0,2,8,1,2,1,5,8,5,8,7,0,2,3,1,6,7,6,5,3,0,9,1,5",
			"name": "Decimal expansion of shape of a (2*Pi)-extension rectangle; shape = Pi + sqrt(1 + Pi^2).",
			"comment": [
				"See A188640 for definitions of shape and r-extension rectangle. Briefly, an r-extension rectangle is composed of two rectangles of shape r.",
				"A 2*Pi-extension rectangle matches the continued fraction [6,2,3,1,1,3,2,1,16,47,...] of the shape L/W = Pi + sqrt(1 + Pi^2). This is analogous to the matching of a golden rectangle to the continued fraction [1,1,1,1,1,1,1,...]. Specifically, for the (2*Pi)-extension rectangle, 6 squares are removed first, then 2 squares, then 3 squares, then 1 square, then 1 square, ..., so that the original rectangle is partitioned into an infinite collection of squares."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A188725/b188725.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"6.4385009630654083972232325635946917292621665408132615256106..."
			],
			"maple": [
				"evalf(Pi+sqrt(1+Pi^2),140); # _Muniru A Asiru_, Nov 01 2018"
			],
			"mathematica": [
				"r = 2*Pi; t = (r + (4 + r^2)^(1/2))/2; FullSimplify[t]",
				"N[t, 130]",
				"RealDigits[N[t, 130]][[1]] (* A188725 *)",
				"ContinuedFraction[t, 120] (* A188726 *)",
				"RealDigits[Pi + Sqrt[1 + Pi^2], 10, 100][[1]] (* _G. C. Greubel_, Oct 31 2018 *)"
			],
			"program": [
				"(PARI) default(realprecision, 100); Pi + sqrt(1 + Pi^2) \\\\ _G. C. Greubel_, Oct 31 2018",
				"(MAGMA) SetDefaultRealField(RealField(100)); R:= RealField(); Pi(R) + Sqrt(1 + Pi(R)^2); // _G. C. Greubel_, Oct 31 2018"
			],
			"xref": [
				"Cf. A188640, A188726."
			],
			"keyword": "nonn,cons",
			"offset": "1,1",
			"author": "_Clark Kimberling_, Apr 09 2011",
			"references": 3,
			"revision": 16,
			"time": "2021-04-10T23:56:23-04:00",
			"created": "2011-04-09T22:48:51-04:00"
		}
	]
}