{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A263292",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 263292,
			"data": "1,1,1,2,4,8,13,26,44,76,119,238,324,648,1008,1492,2116,4232,5680,11360,15272,21872,33536,67072,83168,121376,185496,249072,328416,656832,790656,1581312,1980192,2758624,4193040,5555616,6532896,13065792,19845216",
			"name": "Number of distinct values of |product(A) - product(B)| where A and B are a partition of {1,2,...,n}.",
			"comment": [
				"The problem of showing that no number k is equal to |product(A)-product(B)| for infinitely many different values of n appears in a Hungarian journal for high school students in math and physics (see KöMaL link).",
				"Compare to A038667, which provided the smallest value of |product(A) - product(B)|.",
				"Also the number of distinct values \u003c= sqrt(n!) of element products of subsets of [n]. - _Alois P. Heinz_, Oct 17 2015"
			],
			"link": [
				"KöMaL-Mathematical and Physical Journal for Secondary Schools, \u003ca href=\"http://www.komal.hu/verseny/feladat.cgi?a=honap\u0026amp;h=201509\u0026amp;t=mat\u0026amp;l=en\"\u003eProblems in Mathematics\u003c/a\u003e, September 2015."
			],
			"example": [
				"For n = 4, the four possible values of |product(A) - product(B)| are 2, 5, 10, and 23."
			],
			"maple": [
				"b:= proc(n) option remember; local f, g, h;",
				"      if n\u003c2 then {1}",
				"    else f, g, h:= n!, y-\u003e `if`(y^2\u003c=f, y, NULL), (n-1)!;",
				"         map(x-\u003e {x, g(x*n), g(h/x)}[], b(n-1))",
				"      fi",
				"    end:",
				"a:= n-\u003e nops(b(n)):",
				"seq(a(n), n=0..25);  # _Alois P. Heinz_, Oct 17 2015"
			],
			"mathematica": [
				"a[n_] := Block[{v = Times @@@ Subsets[ Range[2, n], Floor[n/2]]}, Length@ Union@ Abs[v - n!/v]]; Array[a, 20] (* _Giovanni Resta_, Oct 17 2015 *)"
			],
			"xref": [
				"Cf. A038667."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_Jerrold Grossman_, Oct 13 2015",
			"ext": [
				"a(21)-a(27) from _Giovanni Resta_, Oct 17 2015",
				"a(28)-a(38) from _Alois P. Heinz_, Oct 17 2015"
			],
			"references": 2,
			"revision": 32,
			"time": "2015-10-17T14:03:35-04:00",
			"created": "2015-10-17T12:56:28-04:00"
		}
	]
}