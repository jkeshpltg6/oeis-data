{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A030052",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 30052,
			"data": "3,5,6,15,12,25,40,84,47,63,68,81,102,95,104,162,123",
			"name": "Smallest number whose n-th power is a sum of distinct smaller positive n-th powers.",
			"comment": [
				"Sprague has shown that for any n, all sufficiently large integers are the sum of distinct n-th powers. Sequence A001661 lists the largest number not of this form, so we know that a(n) is less than or equal to the next larger n-th power. - _M. F. Hasler_, May 25 2020",
				"a(18) \u003c= 200, a(19) \u003c= 234, a(20) \u003c= 242; for more upper bounds see the Al Zimmermann's Programming Contests link: The \"Final Report\" gives exact solutions for n = 16 through 30; those for n = 16 and 17 have been confirmed to be minimal by Jeremy Sawicki. - _M. F. Hasler_, Jul 20 2020"
			],
			"link": [
				"R. Sprague, \u003ca href=\"https://doi.org/10.1007/BF01185779\"\u003eÜber Zerlegungen in n-te Potenzen mit lauter verschiedenen Grundzahlen\u003c/a\u003e, Math. Z. 51 (1948) 466-468.",
				"Various authors, \u003ca href=\"/A030052/a030052.txt\"\u003eHow each power is decomposed\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/DiophantineEquation.html\"\u003eDiophantine Equation.\u003c/a\u003e",
				"Al Zimmermann, \u003ca href=\"http://azspcs.com/Contest/SumsOfPowers/FinalReport\"\u003eSum Of Powers: Final Report\u003c/a\u003e, Al Zimmermann's Programming Contests, April-July 2020"
			],
			"formula": [
				"a(n) \u003c= A001661(n)^(1/n) + 1. - _M. F. Hasler_, May 25 2020",
				"a(n) \u003e= A332101(n) = A078607(n)+2 (conjectured). - _M. F. Hasler_, May 25 2020"
			],
			"example": [
				"3^1 = 2^1 + 1^1, and there is no smaller solution given that the r.h.s. is the smallest possible sum of distinct positive powers.",
				"For n = 2, one sees immediately that 3 is not a solution (3^2 \u003e 2^2 + 1^2) and one can check that 4^2 isn't equal to Sum_{x in A} x^2 for any subset A of {1, 2, 3}. Therefore, the well known hypotenuse number 5 (cf. A009003) with 5^2 = 4^2 + 3^2 provides the smallest possible solution.",
				"a(17) = 123 since 123^17 = Sum {3, 5, 7, 8, 9, 11, 13, 16, 17, 19, 30, 33, 34, 35, 38, 40, 41, 43, 51, 52, 54, 55, 58, 59, 60, 63, 66, 69, 70, 71, 72, 73, 75, 76, 81, 86, 87, 88, 89, 90, 92, 95, 98, 106, 107, 108, 120}^17, with obvious notation. [Solution found by Jeremy Sawicki on July 3, 2020, see Al Zimmermann's Programming Contests link.] - _M. F. Hasler_, Jul 18 2020",
				"For more examples, see the link."
			],
			"program": [
				"(PARI) A030052(n, m=n\\/log(2)+1, s=0)={if(!s, until(A030052(n, m, (m+=1)^n),), s \u003c 2^n || s \u003e (m+n+1)*m^n\\(n+1), m=s\u003c2, m=min(sqrtnint(s, n), m); s==m^n || until( A030052(n, m-1, s-m^n) || (s\u003e=(m+n)*(m-=1)^n\\(n+1) \u0026\u0026 !m=0), )); m} \\\\ Does exhaustive search to find the least solution m. Use optional 2nd arg to specify a starting value for m. Calls itself with nonzero 3rd (optional) argument: in this case, returns nonzero iff s is the sum of powers \u003c= m^n. - For illustration only: takes very long already for n = 8 and n \u003e= 10. - _M. F. Hasler_, May 25 2020"
			],
			"xref": [
				"Cf. A001661, A078607, A332065, A332097, A332101."
			],
			"keyword": "nonn,nice,more",
			"offset": "1,1",
			"author": "Richard C. Schroeppel",
			"ext": [
				"n = 8 through 10 terms found by _David W. Wilson_",
				"a(11) from _Al Zimmermann_, Apr 07 2004",
				"a(12) from _Al Zimmermann_, Apr 13 2004",
				"a(13) from Manol Iliev, Jan 04 2010",
				"a(14) and a(15) from Manol Iliev, Apr 28 2011",
				"a(16) and a(17) due to Jeremy Sawicki, added by _M. F. Hasler_, Jul 20 2020"
			],
			"references": 10,
			"revision": 63,
			"time": "2020-07-23T09:33:19-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}