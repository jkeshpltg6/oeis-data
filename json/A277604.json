{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A277604",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 277604,
			"data": "1,1,1,1,1,1,1,1,3,1,1,1,5,5,1,1,1,7,9,13,1,1,1,9,13,37,25,1,1,1,11,17,73,81,61,1,1,1,13,21,121,169,301,125,1,1,1,15,25,181,289,841,729,295,1,1,1,17,29,253,441,1801,2197,2549,625,1,1,1,19,33,337,625,3301,4913,10123,6561,1447,1",
			"name": "Array of coefficients T(k,n) of the formal power series A(k,x) read by upwards antidiagonals, where A(k,x) = sqrt(1 + 2*x*A(k,x) + (4*k+1)*x^2*(A(k,x))^2), k \u003e= 0.",
			"comment": [
				"For k = 0 see A000012, for k = 1 see A098615, and for k = 2 see A200376.",
				"It will be interesting using the formulae for k \u003c 0 (attention: signed terms!). Especially for k = -1 see A157674.",
				"If G is the g.f. of central binomial coefficients (see A000984) and B(k,x) = G(k*x^2), then B(k,x) = A(k,x)/(1+x*A(k,x)) and A(k,x) = B(k,x) / (1-x*B(k,x)) for k \u003e= 0. - _Werner Schulte_, Aug 07 2017"
			],
			"formula": [
				"A(k,x) = (x + sqrt(1 - 4*k*x^2))/(1 - (4*k+1)*x^2) for k \u003e= 0.",
				"T(k,0) = 1 and T(k,2*n+2) = (4*k+1)^(n+1)-2*(Sum_{i=0..n} A000108(i)*k^(i+1)* (4*k+1)^(n-i)), and T(k,2*n+1) = (4*k+1)^n for k \u003e= 0 and n \u003e= 0.",
				"A(k,x) = 1/(1 - x - 2*k*x^2*C(k*x^2)), k \u003e= 0, where C is the g.f. of A000108.",
				"Conjecture: If B(k,n) satisfy B(k,0) = B(k,1) = 1 and B(k,n+2) = B(k,n+1) + k*B(k,n) for k \u003e= 0 and n \u003e= 0 (generalized Fibonacci numbers, see A015441) and G(k,x) = Sum_{n\u003e=0} A000108(n)*B(k,n)*x^n for k \u003e= 0, then you will have (1): A(k,x*G(k,x)) = G(k,x) and (2): G(k,x/A(k,x)) = A(k,x) for k \u003e= 0. Especially for k = 1 see A098615 and for k = 2 see A200376.",
				"Conjecture: T(k,2*n) = Sum_{i=0..n} A046521(n,i)*k^(n-i) for k, n \u003e= 0. - _Werner Schulte_, Aug 02 2017",
				"Recurrence: T(k,2*n+2) = (4*k+1)*T(k,2*n)-2*k^(n+1)*A000108(n) with initial value T(k,0) = 1 for k \u003e= 0 and n \u003e= 0. - _Werner Schulte_, Aug 09 2017",
				"T(k,n) = Sum_{i=0..n} A111959(n,i)*k^((n-i)/2) for k \u003e= 0 and n \u003e= 0. - _Werner Schulte_, Aug 09 2017"
			],
			"example": [
				"The terms define the array T(k,n) for k \u003e= 0 and n \u003e= 0, i.e.,",
				"k\\n  0  1   2   3    4     5      6      7       8        9  . . .",
				"0:   1  1   1   1    1     1      1      1       1        1  . . .",
				"1:   1  1   3   5   13    25     61    125     295      625  . . .",
				"2:   1  1   5   9   37    81    301    729    2549     6561  . . .",
				"3:   1  1   7  13   73   169    841   2197   10123    28561  . . .",
				"4:   1  1   9  17  121   289   1801   4913   28057    83521  . . .",
				"5:   1  1  11  21  181   441   3301   9261   63071   194481  . . .",
				"6:   1  1  13  25  253   625   5461  15625  123565   390625  . . .",
				"7:   1  1  15  29  337   841   8401  24389  219619   707281  . . .",
				"8:   1  1  17  33  433  1089  12241  35937  362993  1185921  . . .",
				"9:   1  1  19  37  541  1369  17101  50653  567127  1874161  . . .",
				"etc."
			],
			"xref": [
				"Cf. A000012, A000045, A000108, A000984, A015441, A046521, A098615, A111959, A200376."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "0,9",
			"author": "_Werner Schulte_, Oct 29 2016",
			"references": 0,
			"revision": 54,
			"time": "2017-08-10T03:59:57-04:00",
			"created": "2016-11-03T15:24:46-04:00"
		}
	]
}