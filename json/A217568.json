{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A217568",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 217568,
			"data": "2,7,6,9,5,1,4,3,8,2,9,4,7,5,3,6,1,8,4,3,8,9,5,1,2,7,6,4,9,2,3,5,7,8,1,6,6,1,8,7,5,3,2,9,4,6,7,2,1,5,9,8,3,4,8,1,6,3,5,7,4,9,2,8,3,4,1,5,9,6,7,2",
			"name": "Rows of the 8 magic squares of order 3 and magic sum 15, lexicographically sorted.",
			"reference": [
				"See A320871, A320872 and A320873 for the list of all 3 X 3 magic squares of distinct integers, primes, resp. consecutive primes. In all these, only the lexicographically smallest of the eight \"equivalent\" squares are listed. Note that the terms are not always in the order that corresponds to the terms of this sequence. For example, in row 3 of A320871 and row 11 of A320873, the second term is smaller than the third term. However, when this is not the case, then row n of the present sequence is the list of indices which gives the n-th variant of the square from the (ordered) set of 9 elements: e.g., (2, 7, 6, ...) means that the 2nd, 7th and 6th of the set of 9 numbers yield the first row of the square. For example, A320873(n) = A073519(a(n)), 1 \u003c= n \u003c= 9. - _M. F. Hasler_, Nov 04 2018"
			],
			"link": [
				"Eric Weisstein, \u003ca href=\"http://mathworld.wolfram.com/MagicSquare.html\"\u003eMathWorld: Magic Square\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Magic_square\"\u003eMagic Square\u003c/a\u003e",
				"\u003ca href=\"/index/Mag#magic\"\u003eIndex entries for sequences related to magic squares\u003c/a\u003e"
			],
			"example": [
				"The first such magic square is",
				"2, 7, 6",
				"9, 5, 1",
				"4, 3, 8",
				"From _M. F. Hasler_, Sep 23 2018: (Start)",
				"The complete table reads:",
				"[2, 7, 6, 9, 5, 1, 4, 3, 8]",
				"[2, 9, 4, 7, 5, 3, 6, 1, 8]",
				"[4, 3, 8, 9, 5, 1, 2, 7, 6]",
				"[4, 9, 2, 3, 5, 7, 8, 1, 6]",
				"[6, 1, 8, 7, 5, 3, 2, 9, 4]",
				"[6, 7, 2, 1, 5, 9, 8, 3, 4]",
				"[8, 1, 6, 3, 5, 7, 4, 9, 2]",
				"[8, 3, 4, 1, 5, 9, 6, 7, 2] (End)"
			],
			"mathematica": [
				"squares = {}; a=5; Do[m = {{a + b, a - b - c, a + c}, {a - b + c, a, a + b - c}, {a - c, a + b + c, a - b}}; If[ Unequal @@ Flatten[m] \u0026\u0026 And @@ (1 \u003c= #1 \u003c= 9 \u0026 ) /@ Flatten[m], AppendTo[ squares, m]], {b, -(a - 1), a - 1}, {c, -(a - 1), a - 1}]; Sort[ squares, FromDigits[ Flatten[#1] ] \u003c FromDigits[ Flatten[#2] ] \u0026 ] // Flatten"
			],
			"program": [
				"(PARI) A217568=select(S-\u003eSet(S)==[1..9],concat(vector(9,a,vector(9,b,[a,b,15-a-b,20-2*a-b,5,2*a+b-10,a+b-5,10-b,10-a])))) \\\\ Could use that a = 2k, k = 1..4, and b is odd, within max(1,7-a)..min(9,13-a). - _M. F. Hasler_, Sep 23 2018"
			],
			"xref": [
				"Cf. A320871, A320872, A320873: inequivalent 3 X 3 magic squares of distinct integers, primes, consecutive primes."
			],
			"keyword": "easy,fini,nonn,full,tabf",
			"offset": "1,1",
			"author": "_Jean-François Alcover_, Oct 08 2012",
			"references": 2,
			"revision": 49,
			"time": "2018-12-26T03:41:07-05:00",
			"created": "2012-10-09T03:42:33-04:00"
		}
	]
}