{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A138180",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 138180,
			"data": "1,1,2,3,8,1,2,3,4,5,8,9,15,24,80,1,2,3,4,5,6,7,8,9,14,15,20,24,27,35,48,49,63,80,125,224,2400,4374,1,2,3,4,5,6,7,8,9,10,11,14,15,20,21,24,27,32,35,44,48,49,54,55,63,80,98,99,120,125,175,224,242,384,440,539",
			"name": "Irregular triangle read by rows: row n consists of all numbers x such that x and x+1 have no prime factor larger than prime(n).",
			"comment": [
				"A number x is p-smooth if all prime factors of x are \u003c= p. The length of row n is A002071(n). Row n begins with 1 and ends with A002072(n). Each term of row n-1 is in row n.",
				"The n-th row is the union of the rows 1 to n of A145605. - _M. F. Hasler_, Jan 18 2015"
			],
			"reference": [
				"See A002071."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A138180/b138180.txt\"\u003eRows n=1..10 of triangle, flattened\u003c/a\u003e"
			],
			"example": [
				"The table reads:",
				"1,",
				"1, 2, 3, 8,",
				"1, 2, 3, 4, 5, 8, 9, 15, 24, 80,  (= A085152)",
				"1, 2, 3, 4, 5, 6, 7, 8, 9, 14, 15, 20, 24, 27, 35, 48, 49, 63, 80, 125, 224, 2400, 4374, (= A085153)",
				"1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 14, 15, 20, 21, 24, 27, 32, 35, 44, 48, 49, 54, 55, 63, 80, 98, 99, 120, 125, 175, 224, 242, 384, 440, 539, 2400, 3024, 4374, 9800 (= A252494),",
				"..."
			],
			"mathematica": [
				"(* This program needs x maxima taken from A002072. *) xMaxima = A002072; smoothNumbers[p_, max_] := Module[{a, aa, k, pp, iter}, k = PrimePi[p]; aa = Array[a, k]; pp = Prime[Range[k]]; iter = Table[{a[j], 0, PowerExpand @ Log[pp[[j]], max/Times @@ (Take[pp, j-1]^Take[aa, j-1])]}, {j, 1, k}]; Table[Times @@ (pp^aa), Sequence @@ iter // Evaluate] // Flatten // Sort]; row[n_] := Module[{sn}, sn = smoothNumbers[Prime[n], xMaxima[[n]]+1]; Reap[Do[If[sn[[i]]+1 == sn[[i+1]], Sow[sn[[i]]]], {i, 1, Length[sn]-1}]][[2, 1]]]; Table[Print[n]; row[n], {n, 1, 10}] // Flatten (* _Jean-François Alcover_, Jan 16 2015, updated Nov 10 2016 *)"
			],
			"program": [
				"(PARI) A138180_row=[]; A138180(n,k)={if(k, A138180(n)[k], #A138180_row\u003cn \u0026\u0026 A138180_row=concat(A138180_row,vector(n)); if(#A138180_row[n], A138180_row[n], k=0; p=prime(n); A138180_row[n]=vector(A002071(n),i, until( vecmax(factor(k++)[, 1])\u003c=p \u0026\u0026 vecmax(factor(k--+(k\u003c2))[, 1])\u003c=p,k++); k)))} \\\\ A138180(n) (w/o 2nd arg. k) returns the whole row. - _M. F. Hasler_, Jan 16 2015"
			],
			"xref": [
				"Cf. A145605; A085152, A085153, A252494, A252493, A252492."
			],
			"keyword": "nonn,tabf",
			"offset": "1,3",
			"author": "_T. D. Noe_, Mar 04 2008",
			"references": 10,
			"revision": 22,
			"time": "2016-11-13T12:36:09-05:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}