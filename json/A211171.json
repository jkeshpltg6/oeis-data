{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A211171",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 211171,
			"data": "1,6,84,420,26040,78120,9921240,168661080,24624517680,270869694480,554470264600560,7208113439807280,59041657185461430480,2538791258974841510640,383357480105201068106640,98522872387036674503406480,25826982813282567927671981480160",
			"name": "Exponent of general linear group GL(n,2).",
			"comment": [
				"a(n) is the smallest integer for which x^a(n) = 1 for any x in GL(n,2)."
			],
			"link": [
				"Alexander Gruber, \u003ca href=\"/A211171/b211171.txt\"\u003eTable of n, a(n) for n = 1..100\u003c/a\u003e",
				"Eugene Karolinsky and Dmytro Seliutin, \u003ca href=\"https://arxiv.org/abs/2001.10315\"\u003eCarmichael numbers for GL(m)\u003c/a\u003e, arXiv:2001.10315 [math.NT], 2020; where a(n) is noted as K2(n), see page 1.",
				"MathStackExchange, \u003ca href=\"http://math.stackexchange.com/a/294524/12952\"\u003eExponent of GL(n,q)\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 2^ceiling(log_2(n)) * Product_{k=1..n} (k-th cyclotomic polynomial evaluated at 2)."
			],
			"example": [
				"n = 2: GL(2,2) is isomorphic to S3 which has exponent 6 (see: A003418).",
				"n = 3: The set of element orders of GL(3,2) is {1,2,3,4,7} so the exponent is 84.",
				"n = 5: The set of element orders of GL(5,2) is {1,2,3,4,5, 6,7,8,12,14, 15,21,31} so the exponent is 26040 (see: A053651)."
			],
			"maple": [
				"with(numtheory):",
				"a:= proc(n) local t; t:= 2^ilog2(n);",
				"      `if`(t\u003cn, 2, 1)*t*mul(cyclotomic(k, 2), k=1..n)",
				"    end:",
				"seq(a(n), n=1..20);  # _Alois P. Heinz_, Feb 04 2013"
			],
			"mathematica": [
				"f[q_, n_] := With[{p = Sort[Divisors[q]][[2]]},",
				"  p^Ceiling[Log[p, n]] Product[Cyclotomic[k, q], {k, n}]]; f[2,#]\u0026/@Range[100]"
			],
			"program": [
				"(MAGMA)",
				"for n in [1..18] do",
				"Exponent(GL(n,2));",
				"end for;",
				"(PARI) a(n) = 2^ceil(log(n)/log(2))*prod(k=1, n, polcyclo(k, 2)); \\\\ _Michel Marcus_, Jan 29 2020"
			],
			"xref": [
				"Cf. A003418, A053651.",
				"Cf. A006951 (number of conjugacy classes in GL(n,2))."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Alexander Gruber_, Jan 31 2013",
			"references": 1,
			"revision": 34,
			"time": "2020-01-31T07:13:43-05:00",
			"created": "2013-02-08T17:40:24-05:00"
		}
	]
}