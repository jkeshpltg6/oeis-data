{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A247792",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 247792,
			"data": "1,10,37,82,145,226,325,442,577,730,901,1090,1297,1522,1765,2026,2305,2602,2917,3250,3601,3970,4357,4762,5185,5626,6085,6562,7057,7570,8101,8650,9217,9802,10405,11026,11665,12322,12997,13690,14401,15130,15877,16642,17425,18226,19045,19882",
			"name": "a(n) = 9*n^2 + 1.",
			"comment": [
				"The odd numbers of the form 9n^2 + 1 are listed in A158591 (36n^2 + 1).",
				"The even numbers of the form 9n^2 + 1 are given by 36x^2 - 36x + 10, x \u003e 0.",
				"Every integer n\u003e0 give three perfect squares and consecutives from 2^2. The formulas for each value of n are: a(n)-6n, a(n)-1 and a(n)+6n. - _Miquel Cerda_, Sep 19 2016",
				"These squares are, for n\u003e0, A000290(3*n-1), 3*n and (3n+1) and the sum of them is 3*a(n) - 1. - _Miquel Cerda_, Sep 26 2016"
			],
			"link": [
				"Karl V. Keller, Jr., \u003ca href=\"/A247792/b247792.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-3,1)."
			],
			"formula": [
				"a(n) = (3n)^2 + 1 = 9n^2 + 1 = A016766(n) + 1.",
				"G.f.: (1+7*x+10*x^2)/(1-x)^3. - _Vincenzo Librandi_, Sep 27 2014",
				"a(n) = ((3n-1)^2 + (3n+1)^2)/2 = (A016790(n-1) + A016778(n))/2. - _Miquel Cerda_, Jun 25 2016",
				"From _Ilya Gutkovskiy_, Jun 25 2016: (Start)",
				"E.g.f.: (1 + 9*x + 9*x^2)*exp(x).",
				"Dirichlet g.f.: 9*zeta(s-2) + zeta(s).",
				"Sum_{n\u003e=0} 1/a(n) = (3 + Pi*coth(Pi/3))/6. (End)",
				"a(n) = 3*a(n-1) - 3*a(n-2) + a(n-3) for n\u003e2. - _Wesley Ivan Hurt_, Jun 25 2016",
				"Sum_{n\u003e=0} (-1)^n/a(n) = (1 + (Pi/3)*csch(Pi/3))/2. - _Amiram Eldar_, Jul 15 2020",
				"From _Amiram Eldar_, Feb 05 2021: (Start)",
				"Product_{n\u003e=0} (1 + 1/a(n)) = sqrt(2)*csch(Pi/3)*sinh(sqrt(2)*Pi/3).",
				"Product_{n\u003e=1} (1 - 1/a(n)) = (Pi/3)*csch(Pi/3). (End)"
			],
			"example": [
				"a(1) = (2^2 + 4^2)/2 = 3^2 + 1 = 10, a(2) = (5^2 + 7^2)/2 = 6^2 + 1 = 37, a(3) = (8^2 + 10^2)/2 = 9^2 + 1 = 82. - _Miquel Cerda_, Jun 25 2016"
			],
			"maple": [
				"A247792:=n-\u003e9*n^2 + 1: seq(A247792(n), n=0..80); # _Wesley Ivan Hurt_, Jun 25 2016"
			],
			"mathematica": [
				"(3Range[0, 49])^2 + 1 (* _Alonso del Arte_, Sep 24 2014 *)",
				"CoefficientList[Series[(1 + 7 x + 10 x^2)/(1 - x)^3, {x, 0, 50}], x] (* _Vincenzo Librandi_, Sep 27 2014 *)"
			],
			"program": [
				"(Python) for n in range (0,100): print (9*n**2+1)",
				"(PARI) a(n)=9*n^2+1 \\\\ _Charles R Greathouse IV_, Sep 26 2014",
				"(MAGMA) [9*n^2+1: n in [0..60]]; // _Vincenzo Librandi_, Sep 27 2014"
			],
			"xref": [
				"Cf. A016766, A158591 (36n^2 + 1), A156226 (primes of the form 9n^2 + 1).",
				"Cf. also A000290."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Karl V. Keller, Jr._, Sep 23 2014",
			"references": 3,
			"revision": 66,
			"time": "2021-02-05T11:27:25-05:00",
			"created": "2014-09-26T16:12:15-04:00"
		}
	]
}