{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A098103",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 98103,
			"data": "23,5,7,11,13,17,19,2,3,293,137,41,43,47,53,59,61,67,71,73,79,83,89,97,101,103,107,109,113,127,131,1371391491511,571,631,67173179181191,193,197,199,211,223,227,229,233,239,241,251,257,263,269,271,277,281,283",
			"name": "Consider the succession of single digits of the primes (A000040): 2 3 5 7 1 1 1 3 1 7 1 9 2 3 2 9 3 1 ... (A033308). This sequence is the lexicographically earliest derangement of A000040 that produces the same succession of digits.",
			"comment": [
				"Derangement here means a(n) != A000040(n) for all n.",
				"Original name: \"Write each prime number \u003e0 on a single label. Put the labels in numerical order to form an infinite sequence L. Consider the succession of single digits of L: 2 3 5 7 1 1 1 3 1 7 1 9 2 3 2 9 3 1 3 7 4 1 4 3 4 7 5 3 5 9 6 1 6 7 7 1 7 3 7 9... (see A033308). The sequence S gives a rearrangement of the labels that reproduces the same succession of digits, subject to the constraints that a label of L cannot represent itself, and the smallest label must be used that does not lead to a contradiction.\"",
				"This could be roughly rephrased like this: \"Rewrite in the most economical way the 'prime numbers pattern' using only prime numbers, but rearranged. Do not use any prime more than once.\"",
				"a(180) has over 1000 digits. - _Danny Rorabaugh_, Nov 29 2015"
			],
			"link": [
				"Danny Rorabaugh, \u003ca href=\"/A098103/b098103.txt\"\u003eTable of n, a(n) for n = 1..179\u003c/a\u003e",
				"Eric Angelini, \u003ca href=\"http://www.cetteadressecomportecinquantesignes.com/Sloane01.htm\"\u003eSequence re-writing\u003c/a\u003e",
				"Eric Angelini, \u003ca href=\"/A098103/a098103.htm\"\u003eSequence re-writing\u003c/a\u003e [Cached copy, with permission]"
			],
			"example": [
				"We must begin with \"2,3,5,7,11,...\" and we cannot have the first term be 2, the first prime, so the smallest available prime is 23."
			],
			"mathematica": [
				"f[lst_List, k_] := Block[{L = lst, g, a = {}, m = 0}, g[] := {Set[m, First@ FromDigits@ Append[IntegerDigits@ m, First@ #]], Set[L, Last@ #]} \u0026@ TakeDrop[L, 1]; Do[g[]; While[Or[m == Prime[Length@ a + 1], ! PrimeQ@ m, MemberQ[a, m]], g[]]; AppendTo[a, m]; m = 0, {k}]; a]; f[Flatten@ Map[IntegerDigits, Prime@ Range@ 120], 53] (* _Michael De Vlieger_, Nov 29 2015, Version 10.2 *)"
			],
			"program": [
				"(Sage)",
				"def A098103(n):",
				"  Pr, p, s, A, i = Primes(), 2, \"\", [], 1",
				"  while len(A)\u003cn:",
				"    while len(s)\u003c=i: s, p = s+str(p), next_prime(p)",
				"    q = int(s[:i])",
				"    if s[i]!=\"0\" and is_prime(q) and Pr.unrank(len(A))!=q and (q not in A):",
				"      A.append(q)",
				"      s, i = s[i:], 1",
				"    else: i += 1",
				"  return A",
				"A098103(179) # _Danny Rorabaugh_, Nov 29 2015"
			],
			"xref": [
				"For other sequences of this type, cf. A098067."
			],
			"keyword": "base,nice,nonn",
			"offset": "1,1",
			"author": "_Eric Angelini_, Sep 22 2004",
			"ext": [
				"Name, Comments, and Example edited by _Danny Rorabaugh_, Nov 28 2015",
				"Corrected and extended by _Danny Rorabaugh_, Nov 29 2015"
			],
			"references": 2,
			"revision": 30,
			"time": "2016-06-09T05:29:38-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}