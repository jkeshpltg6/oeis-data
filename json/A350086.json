{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A350086",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 350086,
			"data": "22,22,2,2,22,2,10,10,2,6,106,2,22,46,2,2,2,6,2,10,2,2,6,2,78,2,18,2,6,2,2,2,2,46,58,2,2,2,58,2,6,2,2,2,10,10,2,46,2,2,2,82,2,30,2,6,2,10,2,10,46,2,2,2,2,2,6,78,2,10,2,10,46,10,2,46,2",
			"name": "a(n) is the smallest totient number k \u003e 1 such that A005277(n)*k is a nontotient number, or 0 if no such number exists.",
			"comment": [
				"Subsequence of A350085.",
				"Conjecture: a(n) != 0 for all n.",
				"Records: 22 (A005277(n) = 14), 106 (A005277(n) = 90), 2010 (A005277(n) = 450), ...",
				"By definition, a totient number N \u003e 1 is a term if and only if there exists an even nontotient r such that: (i) k*r is a totient for totient numbers 2 \u003c= k \u003c N; (ii) N*r is a nontotient. No term can be of the form m*m', where m \u003e 1 is a totient and m' \u003e 1 is in A301587 (otherwise m*r is a totient implies m*m'*r is a totient).",
				"Conjecture: every totient number \u003e 1 which is not of the form m*m', where m \u003e 1 is a totient and m' \u003e 1 is in A301587, appears in this sequence. For example, the numbers 2, 6, 10, 18, 22, 28, 30 first appears when A007617(n) = 7, 15, 5, 33, 11, 902, 3."
			],
			"example": [
				"A005277(11) = 90. N = 106 is a totient number \u003e 1 such that 90*k is a totient for totient numbers 2 \u003c= k \u003c N, and 90*N is a nontotient, so a(11) = 106.",
				"A005277(83) = 450. N = 2010 is a totient number \u003e 1 such that 450*k is a totient for totient numbers 2 \u003c= k \u003c N, and 450*N is a nontotient, so a(83) = 2010.",
				"A005277(187) = 902. N = 28 is a totient number \u003e 1 such that 902*k is a totient for totient numbers 2 \u003c= k \u003c N, and 902*N is a nontotient, so a(187) = 28.",
				"A005277(73991) = 241010. N = 100 is a totient number \u003e 1 such that 241010*k is a totient for totient numbers 2 \u003c= k \u003c N, and 241010*N is a nontotient, so a(73991) = 100. Note that although 100 = 10*10 is a product of 2 totient number \u003e 1, neither factor is in A301587, so nothing prevents that 100 is a term of this sequence."
			],
			"program": [
				"(PARI) b(n) = if(!istotient(n), for(k=2, oo, if(istotient(k) \u0026\u0026 !istotient(n*k), return(k))))",
				"list(lim) = my(v=[]); forstep(n=2, lim, 2, if(!istotient(n), v=concat(v,b(n)))); v \\\\ gives a(n) for A005277(n) \u003c= lim"
			],
			"xref": [
				"Cf. A005277, A350085, A301587."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Jianing Song_, Dec 12 2021",
			"references": 1,
			"revision": 12,
			"time": "2021-12-17T20:58:51-05:00",
			"created": "2021-12-17T20:58:51-05:00"
		}
	]
}