{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A206498",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 206498,
			"data": "0,2,1,1,2,2,1,1,2,2,2,2,2,2,2,1,2,3,1,2,2,2,3,2,2,3,3,2,2,3,2,1,2,2,2,3,2,2,3,2,3,3,2,2,3,3,3,2,2,3,2,3,1,4,2,2,2,3,2,3,3,2,3,1,3,3,2,2,3,3,2,3,3,3,3,2,2,4,2,2,4,3,3,3,2,3,3,2,2,4,3,3,2,3,2,2,3,3,3,3,3,3,4,3,3,2,2,4,3,3,3,2,3,3,3,3,4,2,2,3",
			"name": "Number of quasipendant vertices in the rooted tree with Matula-Goebel number n.",
			"comment": [
				"A pendant vertex in a tree is a vertex having degree 1. A vertex is called quasipendant if it is adjacent to a pendant vertex (see the Bapat reference, p. 106).",
				"The Matula-Goebel number of a rooted tree can be defined in the following recursive manner: to the one-vertex tree there corresponds the number 1; to a tree T with root degree 1 there corresponds the t-th prime number, where t is the Matula-Goebel number of the tree obtained from T by deleting the edge emanating from the root; to a tree T with root degree m\u003e=2 there corresponds the product of the Matula-Goebel numbers of the m branches of T."
			],
			"reference": [
				"R. B. Bapat, Graphs and Matrices, Springer, London, 2010.",
				"F. Goebel, On a 1-1 correspondence between rooted trees and natural numbers, J. Combin. Theory, B 29 (1980), 141-143.",
				"I. Gutman and A. Ivic, On Matula numbers, Discrete Math., 150, 1996, 131-142.",
				"I. Gutman and Y-N. Yeh, Deducing properties of trees from their Matula numbers, Publ. Inst. Math., 53 (67), 1993, 17-22.",
				"D. W. Matula, A natural rooted tree enumeration by prime factorization, SIAM Review, 10, 1968, 273."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A206498/b206498.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"E. Deutsch, \u003ca href=\"http://arxiv.org/abs/1111.4288\"\u003e Rooted tree statistics from Matula numbers\u003c/a\u003e, arXiv:1111.4288.",
				"\u003ca href=\"/index/Mat#matula\"\u003eIndex entries for sequences related to Matula-Goebel numbers\u003c/a\u003e"
			],
			"formula": [
				"a(1)=0; a(2)=2; if n=p(t) (=the t-th prime) and t is even then a(n)=Lp(t); if n=p(t) (=the t-th prime) and t\u003e=3 is odd, then a(n) = 1+Lp(t); if n is composite, then a(n)=Lp(n); here Lp stands for \"number of leaf parents\" (see A196062)."
			],
			"example": [
				"a(5)=2 because the rooted tree with Matula-Goebel number 5 is the path tree A - B - C - D; the pendant vertices are A and D while the quasipendant ones are B and C."
			],
			"maple": [
				"with(numtheory): Lp := proc (n) local r, s: r := proc (n) options operator, arrow: op(1, factorset(n)) end proc: s := proc (n) options operator, arrow: n/r(n) end proc: if n = 1 then 0 elif n = 2 then 1 elif bigomega(n) = 1 then Lp(pi(n)) elif `mod`(r(n), 2) = 0 and `mod`(s(n), 2) = 0 then Lp(r(n))+Lp(s(n))-1 else Lp(r(n))+Lp(s(n)) end if end proc: a := proc (n) if n = 1 then 0 elif n = 2 then 2 elif bigomega(n) = 1 and 0 \u003c `mod`(pi(n), 2) then 1+Lp(pi(n)) elif bigomega(n) = 1 then Lp(pi(n)) else Lp(n) end if end proc: seq(a(n), n = 1 .. 120);"
			],
			"program": [
				"(Haskell)",
				"a206498 1 = 0",
				"a206498 2 = 2",
				"a206498 x = if t \u003e 0 then a196062 t + t `mod` 2 else a196062 x",
				"            where t = a049084 x",
				"-- _Reinhard Zumkeller_, Sep 03 2013"
			],
			"xref": [
				"Cf. A196062.",
				"Cf. A049084."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Emeric Deutsch_, May 22 2012",
			"references": 1,
			"revision": 16,
			"time": "2017-03-07T06:16:53-05:00",
			"created": "2012-05-23T17:17:52-04:00"
		}
	]
}