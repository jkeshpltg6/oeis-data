{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A299327",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 299327,
			"data": "1,2,3,4,7,6,5,8,14,13,10,9,16,28,27,11,12,19,34,56,55,22,15,26,39,70,112,111,23,17,29,54,79,142,224,223,45,18,30,57,110,159,286,448,447,48,20,33,58,113,222,319,574,896,895,92,21,38,69,114,225,446",
			"name": "Rectangular array by antidiagonals: row n gives the ranks of {2,3}-power towers in which the maximal runlength is n; see Comments.",
			"comment": [
				"Suppose that S is a set of real numbers.  An S-power-tower, t, is a number t = x(1)^x(2)^...^x(k), where k \u003e= 1 and x(i) is in S for i = 1..k.  We represent t by (x(1),x(2),...x(k), which for k \u003e 1 is defined as (x(1),((x(2),...,x(k-1)); (2,3,2) means 2^9.  The number k is the *height* of t.  If every element of S exceeds 1 and all the power towers are ranked in increasing order, the position of each in the resulting sequence is its *rank*.  See A299229 for a guide to related sequences."
			],
			"example": [
				"Northwest corner:",
				"   1   2   4   5   10   11   23   23   45   48",
				"   3   7   8   9   12   15   17   18   20   21",
				"   6  14  16  19   26   29   30   33   38   40",
				"  13  28  34  39   54   57   58   69   78   80",
				"  27  56  70  79  110  113  114  141  158  160"
			],
			"mathematica": [
				"t[1] = {2}; t[2] = {3}; t[3] = {2, 2}; t[4] = {2, 3}; t[5] = {3, 2};",
				"t[6] = {2, 2, 2}; t[7] = {3, 3};",
				"t[8] = {3, 2, 2}; t[9] = {2, 2, 3}; t[10] = {2, 3, 2};",
				"t[11] = {3, 2, 3}; t[12] = {3, 3, 2};",
				"z = 500; g[k_] := If[EvenQ[k], {2}, {3}];",
				"f = 6; While[f \u003c 13, n = f; While[n \u003c z, p = 1;",
				"While[p \u003c 15, m = 2 n + 1; v = t[n]; k = 0;",
				"While[k \u003c 2^p, t[m + k] = Join[g[k], t[n + Floor[k/2]]]; k = k + 1];",
				"p = p + 1; n = m]]; f = f + 1]",
				"r[n_] := Select[Range[30000], Max[Map[Length, Split[t[#]]]] == n \u0026 , 12];",
				"TableForm[Table[r[n], {n, 1, 12}]]  (* A299327, array *)",
				"w[n_, k_] := r[n][[k]];",
				"Table[w[n - k + 1, k], {n, 12}, {k, n, 1, -1}] // Flatten (* A299327, sequence *)"
			],
			"xref": [
				"Cf. A299229."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Feb 08 2018",
			"references": 2,
			"revision": 4,
			"time": "2018-02-08T21:49:40-05:00",
			"created": "2018-02-08T21:49:40-05:00"
		}
	]
}