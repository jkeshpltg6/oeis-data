{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A083058",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 83058,
			"data": "1,0,1,1,2,3,4,4,5,6,7,8,9,10,11,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,57,58,59,60,61,62,63,64,65,66",
			"name": "Number of eigenvalues equal to 1 of n X n matrix A(i,j)=1 if j=1 or i divides j.",
			"comment": [
				"All numbers occur at least once, but the A000295: Eulerian numbers \u003e 0 appear twice. - _Robert G. Wilson v_, Apr 19 2006",
				"It appears that a(n)=sum{k=0..n-1, (1+(-1)^A000108(k))/2} (n\u003e1). - _Paul Barry_, Mar 31 2008",
				"Barry's observation above is true because A000108 obtains odd values only at points (2^j)-1 (A000225) and here the repeated values (A000295) occur precisely at positions given by A000225 and A000079. - _Antti Karttunen_, Aug 17 2013",
				"a(n)+1 gives a lower bound for nonzero terms of A228086 and A228087. - _Antti Karttunen_, Aug 17 2013"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A083058/b083058.txt\"\u003eTable of n, a(n) for n = 1..8192\u003c/a\u003e",
				"J. B. Conrey, \u003ca href=\"http://www.ams.org/notices/200303/fea-conrey-web.pdf\"\u003eThe Riemann Hypothesis\u003c/a\u003e, Notices Amer. Math. Soc., 50 (No. 3, March 2003), 341-353. See p. 347.",
				"Will Dana, \u003ca href=\"https://sites.math.washington.edu/~morrow/336_15/papers/will.pdf\"\u003eEigenvalues of the Redheffer Matrix and their relation to the Mertens function\u003c/a\u003e (2015), Theorem 5.",
				"R. Stephan, \u003ca href=\"/somedcgf.html\"\u003eSome divide-and-conquer sequences ...\u003c/a\u003e",
				"R. Stephan, \u003ca href=\"/A079944/a079944.ps\"\u003eTable of generating functions\u003c/a\u003e"
			],
			"formula": [
				"a(n) = n - A070939(n), n\u003e1.",
				"a(1)=1, else a(n)=b(n) with b(0)=0, b(2n)=b(n)+n-1, b(2n+1)=b(n)+n. - _Ralf Stephan_, Oct 11 2003",
				"Except for a(1), a(n) = n - 1 - floor(log(2,n)). - _Robert G. Wilson v_, Apr 19 2006",
				"It seems that a(n) = A182220(n+1)-1 for all n\u003e1. - _Antti Karttunen_, Aug 17 2013"
			],
			"maple": [
				"A083058 := proc(n)",
				"    if n = 1 then",
				"        1;",
				"    else",
				"        n-floor(log[2](n))-1 ;",
				"    end if;",
				"end proc:",
				"seq(A083058(n),n=1..40) ; # _R. J. Mathar_, Jul 23 2017"
			],
			"mathematica": [
				"a[1] = 1; a[n_] := n - Floor[Log[2, n]] - 1;",
				"Array[a, 100] (* _Jean-François Alcover_, Feb 27 2019 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c2,n\u003e0,n-floor(log(n)/log(2))-1)",
				"(PARI) a(n)= if(n\u003c1, 0, valuation( subst( charpoly( matrix(n, n, i, j, (j==1) || (0==j%i))), x, x+1), x))",
				"(Scheme) (define (A083058 n) (if (\u003c n 2) n (- n (A070939 n)))) ;; _Antti Karttunen_, Aug 17 2013"
			],
			"xref": [
				"Cf. A002321, A070939, A143104."
			],
			"keyword": "nonn",
			"offset": "1,5",
			"author": "_Michael Somos_, Apr 18 2003",
			"references": 7,
			"revision": 36,
			"time": "2019-02-27T03:50:23-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}