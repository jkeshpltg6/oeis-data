{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A099999",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 99999,
			"data": "0,0,0,0,0,0,0,0,3,9,31,229",
			"name": "Number of geometrical configurations of type (n_3).",
			"comment": [
				"A geometrical configuration of type (n_3) consists of a set of n points in the Euclidean or extended Euclidean plane together with a set of n lines, such that each point belongs to 3 lines and each line contains 3 points.",
				"Branko Grünbaum comments that it would be nice to settle the question as to whether all combinatorial configurations (13_3) are (as he hopes) geometrically realizable."
			],
			"reference": [
				"Many of the following references refer to combinatorial configurations (A001403) rather than geometrical configurations, but are included here in case they are helpful.",
				"A. Betten and D. Betten, Regular linear spaces, Beitraege zur Algebra und Geometrie, 38 (1997), 111-124.",
				"Bokowski and Sturmfels, Comput. Synthetic Geom., Lect Notes Math. 1355, p. 41.",
				"CRC Handbook of Combinatorial Designs, 1996, p. 255.",
				"D. Hilbert and S. Cohn-Vossen, Geometry and the Imagination Chelsea, NY, 1952, Ch. 3.",
				"F. Levi, Geometrische Konfigurationen, Hirzel, Leipzig, 1929.",
				"Pisanski, T. and Randic, M., Bridges between Geometry and Graph Theory, in Geometry at Work: Papers in Applied Geometry (Ed. C. A. Gorini), M.A.A., Washington, DC, pp. 174-194, 2000.",
				"B. Polster, A Geometrical Picture Book, Springer, 1998, p. 28.",
				"Sturmfels and White, Rational realizations..., in H. Crapo et al. editors, Symbolic Computation in Geometry, IMA preprint, Univ Minn., 1988."
			],
			"link": [
				"A. Betten and D. Betten, \u003ca href=\"https://doi.org/10.1007/BF01225670\"\u003eTactical decompositions and some configurations v_4\u003c/a\u003e, J. Geom. 66 (1999), 27-41.",
				"A. Betten, G. Brinkmann and T. Pisanski, \u003ca href=\"https://doi.org/10.1016/S0166-218X(99)00143-2\"\u003eCounting symmetric configurations v_3\u003c/a\u003e, Discrete Appl. Math., 99 (2000), 331-338.",
				"H. Gropp, \u003ca href=\"http://dx.doi.org/10.1016/S0012-365X(96)00327-5\"\u003eConfigurations and their realization\u003c/a\u003e, Discr. Math. 174 (1997), 137-151.",
				"Jim Loy, \u003ca href=\"https://web.archive.org/web/20140108030301/http://www.jimloy.com:80/geometry/desargue.htm\"\u003eDesargues's Theorem\u003c/a\u003e",
				"Jim Loy, \u003ca href=\"/A099999/a099999.gif\"\u003eThe configuration (10_3) arising from Desargues's theorem\u003c/a\u003e",
				"Tomo Pisanski, \u003ca href=\"http://www.ijp.si/Configurations2004/papers.html\"\u003ePapers on configurations\u003c/a\u003e",
				"T. Pisanski, M. Boben, D. Marušic, A. Orbanic and A. Graovac, \u003ca href=\"https://doi.org/10.1016/S0012-365X(03)00110-9\"\u003eThe 10-cages and derived configurations\u003c/a\u003e, Discrete Math. 275 (2004), 265-276.",
				"B. Sturmfels and N. White, \u003ca href=\"http://resolver.sub.uni-goettingen.de/purl?GDZPPN002038161\"\u003eAll 11_3 and 12_3 configurations are rational\u003c/a\u003e, Aeq. Math., 39 1990 254-260.",
				"Von Sterneck, \u003ca href=\"https://doi.org/10.1007/BF01691614\"\u003eDie Config. 11_3\u003c/a\u003e, Monat. f. Math. Phys., 5 325-330 1894.",
				"Von Sterneck, \u003ca href=\"https://doi.org/10.1007/BF01696586\"\u003eDie Config. 12_3\u003c/a\u003e, Monat. f. Math. Phys., 6 223-255 1895.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Configuration.html\"\u003eConfiguration.\u003c/a\u003e"
			],
			"example": [
				"The smallest examples occur for n = 9, where there are three configurations, one of which is the configuration arising from Pappus's Theorem (see the World of Mathematics \"Configuration\" link for drawings of all three).",
				"The configuration arising from Desargues's theorem (see link above to an illustration) is one of the nine configurations for n = 10."
			],
			"xref": [
				"Cf. A001403 (abstract or combinatorial configurations (n_3)), A023994, A100001, A098702, A098804, A098822, A098841, A098851, A098852, A098854."
			],
			"keyword": "nonn,nice,hard,more",
			"offset": "1,9",
			"author": "_N. J. A. Sloane_, following correspondence from Branko Grünbaum and Tomaz Pisanski, Nov 12 2004.",
			"references": 3,
			"revision": 33,
			"time": "2021-10-24T02:02:04-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}