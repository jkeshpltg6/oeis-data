{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A186358",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 186358,
			"data": "1,0,1,0,1,1,1,1,3,1,4,6,7,6,1,19,35,30,25,10,1,114,210,190,125,65,15,1,799,1468,1351,840,420,140,21,1,6392,11760,10820,6692,3185,1176,266,28,1,57527,105905,97458,60058,28098,10479,2856,462,36,1,575270,1059306,975140,599640,278500,103593,30345,6210,750,45,1",
			"name": "Triangle read by rows: T(n,k) is the number of permutations of {1,2,...,n} having k up-down cycles (0\u003c=k\u003c=n). A cycle (b(1), b(2), ...) is said to be up-down if, when written with its smallest element in the first position, it satisfies b(1)\u003cb(2)\u003eb(3)\u003c... .",
			"comment": [
				"Sum of entries in row n is n!.",
				"T(n,0) = A186359(n).",
				"Sum_{k=0..n} k * T(n,k) = A186360(n)."
			],
			"link": [
				"Emeric Deutsch and Sergi Elizalde, \u003ca href=\"http://arxiv.org/abs/0909.5199\"\u003eCycle up-down permutations\u003c/a\u003e, arXiv:0909.5199 [math.CO], 2009; and \u003ca href=\"https://ajc.maths.uq.edu.au/pdf/50/ajc_v50_p187.pdf\"\u003ealso\u003c/a\u003e, Australas. J. Combin. 50 (2011), 187-199."
			],
			"formula": [
				"E.g.f.: (1-sin z)^(1-t)/(1-z).",
				"The trivariate e.g.f. H(t,s,z) of the permutations of {1,2,...,n} with respect to size (marked by z), number of up-down cycles (marked by t), and number of cycles that are not up-down (marked by s) is given by H(t,s,z) = (1-sin z)^(s-t)/(1-z)^s."
			],
			"example": [
				"T(3,0)=1 because we have (123).",
				"T(4,2)=7 because we have (1)(243), (142)(3), (132)(4), (13)(24), (12)(34), (143)(2), and (14)(23).",
				"Triangle starts:",
				"1;",
				"0,1;",
				"0,1,1;",
				"1,1,3,1;",
				"4,6,7,6,1;",
				"19,35,30,25,10,1;"
			],
			"maple": [
				"G := (1-sin(z))^(1-t)/(1-z): Gser := simplify(series(G, z = 0, 16)): for n from 0 to 10 do P[n] := sort(expand(factorial(n)*coeff(Gser, z, n))) end do: for n from 0 to 10 do seq(coeff(P[n], t, j), j = 0 .. n) end do; # yields sequence in triangular form"
			],
			"mathematica": [
				"m = maxExponent = 11;",
				"(CoefficientList[# + O[t]^m, t] Range[0, m-1]!\u0026) /@ CoefficientList[(1 - Sin[z])^{1-t}/(1-z) + O[z]^m, z] // Flatten (* _Jean-François Alcover_, Aug 07 2018 *)"
			],
			"xref": [
				"Cf. A186359, A168360."
			],
			"keyword": "nonn,tabl",
			"offset": "0,9",
			"author": "_Emeric Deutsch_, Feb 20 2011",
			"references": 4,
			"revision": 17,
			"time": "2020-11-07T06:00:51-05:00",
			"created": "2011-02-18T22:43:57-05:00"
		}
	]
}