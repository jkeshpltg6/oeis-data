{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A214577",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 214577,
			"data": "2,3,4,5,7,8,9,11,16,17,19,23,25,27,31,32,49,53,59,64,67,81,83,97,103,121,125,127,128,131,227,241,243,256,277,289,311,331,343,361,419,431,509,512,529,563,625,661,691,709,719,729,739,961,1024,1331,1433,1523,1543,1619,1787,1879,2048,2063,2187,2221,2309,2401,2437,2809,2897",
			"name": "The Matula-Goebel numbers of the generalized Bethe trees. A generalized Bethe tree is a rooted tree in which vertices at the same level have the same degree.",
			"comment": [
				"The Matula-Goebel number of a rooted tree can be defined in the following recursive manner: to the one-vertex tree there corresponds the number 1; to a tree T with root degree 1 there corresponds the t-th prime number, where t is the Matula-Goebel number of the tree obtained from T by deleting the edge emanating from the root; to a tree T with root degree m\u003e=2 there corresponds the product of the Matula-Goebel numbers of the m branches of T.",
				"Generalized Bethe trees are called uniform trees in the Goldberg - Livshits reference.",
				"There is a simple bijection between generalized Bethe trees with n edges and partitions of n in which each part is divisible by the next (the parts are given by the number of edges at the successive levels). We have the correspondences: number of edges --- sum of parts; root degree --- last part; number of leaves --- first part; height --- number of parts."
			],
			"link": [
				"E. Deutsch, \u003ca href=\"http://arxiv.org/abs/1111.4288\"\u003e Rooted tree statistics from Matula numbers\u003c/a\u003e, arXiv:1111.4288 [math.CO], 2011.",
				"E. Deutsch, \u003ca href=\"http://dx.doi.org/10.1016/j.dam.2012.05.012\"\u003eRooted tree statistics from Matula numbers\u003c/a\u003e, Discrete Appl. Math., 160, 2012, 2314-2322.",
				"F. Goebel, \u003ca href=\"http://dx.doi.org/10.1016/0095-8956(80)90049-0\"\u003eOn a 1-1-correspondence between rooted trees and natural numbers\u003c/a\u003e, J. Combin. Theory, B 29 (1980), 141-143.",
				"M. K. Goldberg and E. M. Livshits, \u003ca href=\"http://mi.mathnet.ru/eng/mz9458\"\u003eOn minimal universal trees\u003c/a\u003e, Mathematical Notes of the Acad. of Sciences of the USSR, 4, 1968, 713-717 (translation from the Russian Mat. Zametki 4 1968 371-379).",
				"I. Gutman and A. Ivic, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(95)00182-V\"\u003eOn Matula numbers\u003c/a\u003e, Discrete Math., 150, 1996, 131-142.",
				"I. Gutman and Yeong-Nan Yeh, \u003ca href=\"http://www.emis.de/journals/PIMB/067/3.html\"\u003eDeducing properties of trees from their Matula numbers\u003c/a\u003e, Publ. Inst. Math., 53 (67), 1993, 17-22.",
				"D. W. Matula, \u003ca href=\"http://www.jstor.org/stable/2027327\"\u003eA natural rooted tree enumeration by prime factorization\u003c/a\u003e, SIAM Rev. 10 (1968) 273.",
				"O. Rojo, \u003ca href=\"http://dx.doi.org/10.1016/j.laa.2008.01.026\"\u003eSpectra of weighted generalized Bethe trees joined at the root\u003c/a\u003e, Linear Algebra and its Appl., 428, 2008, 2961-2979.",
				"\u003ca href=\"/index/Mat#matula\"\u003eIndex entries for sequences related to Matula-Goebel numbers\u003c/a\u003e"
			],
			"formula": [
				"In A214578 one has defined Q(n)=0 if n is the Matula-Goebel number of a rooted tree that is not a generalized Bethe tree and Q(n) to be a certain polynomial if n corresponds to a generalized Bethe tree. The Maple  program makes use of this to find the Matula-Goebel numbers corresponding to the generalized Bethe trees."
			],
			"example": [
				"7 is in the sequence because the corresponding rooted tree is Y, a generalized Bethe tree."
			],
			"maple": [
				"with(numtheory): Q := proc (n) local r, s: r := proc (n) options operator, arrow; op(1, factorset(n)) end proc: s := proc (n) options operator, arrow: n/r(n) end proc: if n = 1 then 0 elif n = 2 then 1 elif bigomega(n) = 1 and Q(pi(n)) = 0 then 0 elif bigomega(n) = 1 then sort(expand(1+x*Q(pi(n)))) elif Q(r(n)) \u003c\u003e 0 and Q(s(n)) \u003c\u003e 0 and type(simplify(Q(r(n))/Q(s(n))), constant) = true then sort(Q(r(n))+Q(s(n))) else 0 end if end proc: A := {}; for n to 3000 do if Q(n) = 0 then  else A := `union`(A, {n}) end if end do: A;"
			],
			"xref": [
				"Cf. A214578.",
				"Differs from A243497 for the first time at n=31."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Emeric Deutsch_, Aug 18 2012",
			"references": 59,
			"revision": 12,
			"time": "2017-09-09T09:28:46-04:00",
			"created": "2012-08-18T14:08:59-04:00"
		}
	]
}