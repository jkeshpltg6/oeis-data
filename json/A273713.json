{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A273713",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 273713,
			"data": "1,1,1,2,2,1,4,5,3,1,8,13,9,4,1,17,32,28,14,5,1,37,80,81,50,20,6,1,82,201,231,165,80,27,7,1,185,505,653,526,295,119,35,8,1,423,1273,1824,1644,1036,483,168,44,9,1,978,3217,5058,5034,3535,1848,742,228,54,10,1",
			"name": "Triangle read by rows: T(n,k) is the number of bargraphs of semiperimeter n having k doublerises (n\u003e=2, k\u003e=0). A doublerise in a bargraph is any pair of adjacent up steps.",
			"comment": [
				"Number of entries in row n is n-1.",
				"Sum of entries in row n = A082582(n).",
				"T(n,0) = A004148(n-1) (the 2ndary structure numbers).",
				"T(n,1) = A110320(n-2).",
				"Sum(k*T(n,k), k\u003e=0) = A273714(n)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A273713/b273713.txt\"\u003eRows n = 2..150, flattened\u003c/a\u003e",
				"M. Bousquet-Mélou and A. Rechnitzer, \u003ca href=\"http://www.labri.fr/Perso/~bousquet/Articles/convexes.html\"\u003eThe site-perimeter of bargraphs\u003c/a\u003e, Adv. in Appl. Math. 31 (2003), 86-112.",
				"Emeric Deutsch, S Elizalde, \u003ca href=\"http://arxiv.org/abs/1609.00088\"\u003eStatistics on bargraphs viewed as cornerless Motzkin paths\u003c/a\u003e, arXiv preprint arXiv:1609.00088, 2016"
			],
			"formula": [
				"G.f.: G = G(t,z) satisfies zG^2 - (1 - z - tz - z^2)G + z^2 = 0."
			],
			"example": [
				"Row 4 is 2,2,1 because the 5 (=A082582(4)) bargraphs of semiperimeter 4 correspond to the compositions [1,1,1], [1,2], [2,1], [2,2], [3] and the corresponding drawings show that they have 0, 0, 1, 1, 2 doublerises.",
				"Triangle starts",
				"1;",
				"1,1;",
				"2,2,1;",
				"4,5,3,1;",
				"8,13,9,4,1"
			],
			"maple": [
				"eq := z*G^2-(1-z-t*z-z^2)*G+z^2 = 0: G := RootOf(eq, G): Gser := simplify(series(G, z = 0, 22)): for n from 2 to 20 do P[n] := sort(coeff(Gser, z, n)) end do: for n from 2 to 20 do seq(coeff(P[n], t, j), j = 0 .. n-2) end do; # yields sequence in triangular form",
				"# second Maple program:",
				"b:= proc(n, y, t) option remember; expand(`if`(n=0, (1-t),",
				"      `if`(t\u003c0, 0, b(n-1, y+1, 1)*`if`(t=1, z, 1))+",
				"      `if`(t\u003e0 or y\u003c2, 0, b(n, y-1, -1))+",
				"      `if`(y\u003c1, 0, b(n-1, y, 0))))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, z, i), i=0..n-2))(b(n, 0$2)):",
				"seq(T(n), n=2..16);  # _Alois P. Heinz_, Jun 06 2016"
			],
			"mathematica": [
				"b[n_, y_, t_] := b[n, y, t] = Expand[If[n == 0, 1 - t, If[t \u003c 0, 0, b[n - 1, y + 1, 1]*If[t == 1, z, 1]] + If[t \u003e 0 || y \u003c 2, 0, b[n, y - 1, -1]] + If[y \u003c 1, 0, b[n - 1, y, 0]]]];",
				"T[n_] := Function [p, Table[Coefficient[p, z, i], {i, 0, n - 2}]][b[n, 0, 0]];",
				"Table[T[n], {n, 2, 16}] // Flatten (* _Jean-François Alcover_, Jul 29 2016, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A004148, A082582, A110320, A273714."
			],
			"keyword": "nonn,tabl",
			"offset": "2,4",
			"author": "_Emeric Deutsch_, May 28 2016",
			"references": 2,
			"revision": 19,
			"time": "2017-08-19T23:07:45-04:00",
			"created": "2016-05-28T15:44:21-04:00"
		}
	]
}