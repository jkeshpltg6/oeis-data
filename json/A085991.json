{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A085991",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 85991,
			"data": "1,4,8,4,3,3,6,5,6,4,6,7,0,0,7,8,2,8,2,2,5,8,6,5,0,7,7,4,9,0,7,1,1,3,7,1,8,8,7,5,5,5,8,4,1,7,4,4,8,0,6,8,8,9,4,4,2,5,0,7,5,0,8,0,5,5,2,9,8,2,0,0,3,1,9,7,6,8,2,2,9,3,0,6,4,3,0,9,8,6,8,5,0,6,7,2,4,6,9,0,9,3,5,0,7",
			"name": "Decimal expansion of the prime zeta modulo function at 2 for primes of the form 4k+3.",
			"link": [
				"Jean-François Alcover, \u003ca href=\"/A085991/b085991.txt\"\u003eTable of n, a(n) for n = 0..999\u003c/a\u003e",
				"P. Flajolet and I. Vardi, \u003ca href=\"http://algo.inria.fr/flajolet/Publications/landau.ps\"\u003eZeta Function Expansions of Classical Constants\u003c/a\u003e, Unpublished manuscript. 1996.",
				"X. Gourdon and P. Sebah, \u003ca href=\"http://numbers.computation.free.fr/Constants/Miscellaneous/constantsNumTheory.html\"\u003eSome Constants from Number theory\u003c/a\u003e.",
				"R. J. Mathar, \u003ca href=\"http://arxiv.org/abs/1008.2547\"\u003eTable of Dirichlet L-series and prime zeta modulo functions for small moduli\u003c/a\u003e, arXiv:1008.2547 [math.NT], 2010-2015, section 3.2 constant P(m=4,n=3,s=2)."
			],
			"formula": [
				"Zeta_R(2) = Sum_{primes p == 3 (mod 4)} 1/p^2",
				"  = (1/2)*Sum_{n\u003e=0} mobius(2*n+1)*log(b((2*n+1)*2))/(2*n+1),",
				"  where b(x)=(1-2^(-x))*zeta(x)/L(x) and L(x) is the Dirichlet Beta function."
			],
			"example": [
				"0.14843365646700782822586507749... = 1/3^2 + 1/7^2 + 1/11^2 + 1/19^2 + 1/23^2 + ..."
			],
			"mathematica": [
				"digits = 1000; nmax0 = 500; dnmax = 10;",
				"Clear[PrimeZeta43];",
				"PrimeZeta43[s_, nmax_] := PrimeZeta43[s, nmax] = (1/2) Sum[(MoebiusMu[2n + 1] ((4n + 2) Log[2] + Log[((-1 + 2^(4n + 2)) Zeta[4n + 2])/(Zeta[4 n + 2, 1/4] - Zeta[4n + 2, 3/4])]))/(2n + 1), {n, 0, nmax}] // N[#, digits+5]\u0026;",
				"PrimeZeta43[2, nmax = nmax0];",
				"PrimeZeta43[2, nmax += dnmax];",
				"While[Abs[PrimeZeta43[2, nmax] - PrimeZeta43[2, nmax - dnmax]] \u003e 10^-(digits+5), Print[\"nmax = \", nmax]; nmax += dnmax];",
				"PrimeZeta43[2] = PrimeZeta43[2, nmax];",
				"RealDigits[PrimeZeta43[2], 10, digits][[1]] (* _Jean-François Alcover_, Jun 21 2011, updated May 06 2021 *)"
			],
			"program": [
				"(PARI) From _M. F. Hasler_, Apr 25 2021: (Start)",
				"PrimeZeta43(s)={suminf(n=0, my(t=s+s*n*2); moebius(n*2+1)*log(zeta(t)/(zetahurwitz(t, 1/4)-zetahurwitz(t, 3/4))*(4^t-2^t))/(n*2+1))/2}",
				"A085991_upto(N=100)={localprec(N+3); digits((PrimeZeta43(2)+1)\\.1^N)[^1]} \\\\ (End)"
			],
			"xref": [
				"Cf. A086032 (analog for primes 4k+1), A085548 (PrimeZeta(2)), A002145.",
				"Cf. A085992 .. A085998 (Zeta_R(3..9))."
			],
			"keyword": "cons,nonn",
			"offset": "0,2",
			"author": "Antonio G. Astudillo (afg_astudillo(AT)lycos.com), Jul 06 2003",
			"references": 13,
			"revision": 32,
			"time": "2021-05-07T02:13:49-04:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}