{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A122840",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 122840,
			"data": "0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0",
			"name": "a(n) is the number of 0's at the end of n when n is written in base 10.",
			"comment": [
				"Greatest k such that 10^k divides n.",
				"a(n) = the number of digits in n - A160093(n).",
				"a(A005117(n)) \u003c= 1. - _Reinhard Zumkeller_, Mar 30 2010",
				"See A054899 for the partial sums. - _Hieronymus Fischer_, Jun 08 2012",
				"From _Amiram Eldar_, Mar 10 2021: (Start)",
				"The asymptotic density of the occurrences of k is 9/10^(k+1).",
				"The asymptotic mean of this sequence is 1/9. (End)"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A122840/b122840.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"S. Ikeda and K. Matsuoka, \u003ca href=\"http://siauliaims.su.lt/pdfai/2013/Iked-Mats-2013.pdf\"\u003eOn transcendental numbers generated by certain integer sequences\u003c/a\u003e, Siauliai Math. Semin., 8 (16) 2013, 63-69."
			],
			"formula": [
				"a(n) = A160094(n) - 1.",
				"From _Hieronymus Fischer_, Jun 08 2012: (Start)",
				"With m = floor(log_10(n)), frac(x) = x-floor(x):",
				"a(n) = Sum_{j=1..m} (1 - ceiling(frac(n/10^j))).",
				"a(n) = m + Sum_{j=1..m} (floor(-frac(n/10^j))).",
				"a(n) = A054899(n) - A054899(n-1).",
				"G.f.: g(x) = Sum_{j\u003e0} x^10^j/(1-x^10^j). (End)"
			],
			"example": [
				"a(160) = 1 because there is 1 zero at the end of 160 when 160 is written in base 10."
			],
			"mathematica": [
				"a[n_] := IntegerExponent[n, 10]; Array[a, 100] (* _Amiram Eldar_, Mar 10 2021 *)"
			],
			"program": [
				"(Haskell)",
				"a122840 n = if n \u003c 10 then 0 ^ n else 0 ^ d * (a122840 n' + 1)",
				"            where (n', d) = divMod n 10",
				"-- _Reinhard Zumkeller_, Mar 09 2013",
				"(PARI) a(n)=valuation(n,10) \\\\ _Charles R Greathouse IV_, Feb 26 2014",
				"(Python)",
				"def a(n): return len(str(n)) - len(str(int(str(n)[::-1]))) # _Indranil Ghosh_, Jun 09 2017"
			],
			"xref": [
				"A007814 is the base 2 equivalent of this sequence.",
				"Cf. A160094, A160093, A001511, A070940, A122841, A027868, A054899, A196563, A196564, A004151."
			],
			"keyword": "nonn,base,easy",
			"offset": "1,100",
			"author": "_Reinhard Zumkeller_, Sep 13 2006",
			"references": 41,
			"revision": 45,
			"time": "2021-03-10T03:19:06-05:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}