{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005145",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5145,
			"data": "2,3,3,5,5,5,7,7,7,7,11,11,11,11,11,13,13,13,13,13,13,17,17,17,17,17,17,17,19,19,19,19,19,19,19,19,23,23,23,23,23,23,23,23,23,29,29,29,29,29,29,29,29,29,29,31,31,31,31,31,31,31,31,31,31,31",
			"name": "n copies of n-th prime.",
			"comment": [
				"Seen as a triangle read by rows: T(n,k) = A000040(n), 1\u003c=k\u003c=n; row sums = A033286; central terms = A031368. - _Reinhard Zumkeller_, Aug 05 2009",
				"Seen as a square array read by antidiagonals, a subtable of the binary operation multiplication tables A297845, A306697 and A329329. - _Peter Munn_, Jan 15 2020"
			],
			"reference": [
				"Douglas Hofstadter, \"Fluid Concepts and Creative Analogies: Computer Models of the Fundamental Mechanisms of Thought\", Basic Books, 1995."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A005145/b005145.txt\"\u003eRows n = 1..125 of triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"From Joseph Biberstine (jrbibers(AT)indiana.edu), Aug 14 2006: (Start)",
				"a(n) = prime(floor(1/2 + sqrt(2*n))).",
				"a(n) = A000040(A002024(n)). (End)",
				"From _Peter Munn_, Jan 15 2020: (Start)",
				"When viewed as a square array A(n,k), the following hold for n \u003e= 1, k \u003e= 1:",
				"A(n,k) = prime(n+k-1).",
				"A(n,1) = A(1,n) = prime(n), where prime(n) = A000040(n).",
				"A(n+1,k) = A(n,k+1) = A003961(A(n,k)).",
				"A(n,k) = A297845(A(n,1), A(1,k)) = A306697(A(n,1), A(1,k)) = A329329(A(n,1), A(1,k)).",
				"(End)"
			],
			"example": [
				"Triangle begins:",
				"  2;",
				"  3,3;",
				"  5,5,5;",
				"  7,7,7,7;",
				"  ..."
			],
			"mathematica": [
				"Table[Prime[Floor[1/2 + Sqrt[2*n]]], {n, 1, 80}] (* Joseph Biberstine (jrbibers(AT)indiana.edu), Aug 14 2006 *)",
				"Flatten[Table[Table[Prime[n], {n}], {n, 12}]] (* _Alonso del Arte_, Jan 18 2012 *)"
			],
			"program": [
				"(Haskell)",
				"a005145 n k = a005145_tabl !! (n-1) !! (k-1)",
				"a005145_row n = a005145_tabl !! (n-1)",
				"a005145_tabl = zipWith ($) (map replicate [1..]) a000040_list",
				"a005145_list = concat a005145_tabl",
				"-- _Reinhard Zumkeller_, Jul 12 2014, Mar 18 2011, Oct 17 2010",
				"(PARI) a(n) = prime(round(sqrt(2*n))) \\\\ _Charles R Greathouse IV_, Oct 23 2015",
				"(MAGMA) [NthPrime(Round(Sqrt(2*n))): n in [1..60]]; // _Vincenzo Librandi_, Jan 18 2020"
			],
			"xref": [
				"Sequences with similar definitions: A002024, A175944.",
				"Cf. A000040 (range of values), A003961, A031368 (main diagonal), A033286 (row sums).",
				"Subtable of A297845, A306697, A329329."
			],
			"keyword": "nonn,nice,tabl",
			"offset": "1,1",
			"author": "_Russ Cox_",
			"references": 8,
			"revision": 57,
			"time": "2021-09-23T05:17:07-04:00",
			"created": "1991-05-16T03:00:00-04:00"
		}
	]
}