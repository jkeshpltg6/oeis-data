{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A243993",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 243993,
			"data": "0,-1,2684,505,22,-1,109,5000005,-1,-1,-1,-1,101,-1,-1,50005,-1,-1,-1,-1,20000,-1,-1,-1,100012,-1,-1,-1,4999449,-1,-1,5000000000000000000000000000005,-1",
			"name": "Consider a decimal number of k \u003e= 2 digits m = d_(k)*10^(k-1) + d_(k-1)*10^(k-2) + ... + d_(2)*10 + d_(1). a(n) is the least number m such that the n-th iteration of the transform T(m) -\u003e (d_(k) + d_(k-1) mod 10)*10^(k-1) + (d_(k-1) + d_(k-2) mod 10)*10^(k-2) + ... + (d_(2) + d_(1) mod 10)*10 + (d_(1) + d(k) mod 10) returns m, or -1 if no such number exists.",
			"comment": [
				"a(0)=0, meaning the smallest m such that if the transformation is never applied (or is applied zero times) what you get is m itself.",
				"For any pair of consecutive digits 'L' and 'R', the digit on the left is replaced by (L+R) mod 10. The least significant digit is replaced by the sum mod 10 of itself and the most significant digit. If we change the rule (digit on the right replaced by (L+R) mod 10 and most significant digit replaced by sum mod 10 of itself and the least significant digit) we get a similar sequence where only a(2)=2486 is different.",
				"If through the transform leading zeros are created they are kept for the next calculation (see example for n = 3, 6, 12 and 15).",
				"a(n) is the smallest k whose first return to itself occurs on the n-th iteration. For instance, if the iterations were continued, 22 would return to itself on iterations 4, 8, 12, etc., but 101 (not 22) is a(12).",
				"In the file \"Other possible entries\" are listed some possible candidates. Other intermediate steps and numbers could be missing and the listed numbers may not be the minima."
			],
			"link": [
				"Paolo P. Lava, \u003ca href=\"/A243993/a243993_2.txt\"\u003eOther possible entries\u003c/a\u003e"
			],
			"example": [
				"n=2: 2684 -\u003e 8426 -\u003e 2684.",
				"n=3: 505 -\u003e 550 -\u003e 055 -\u003e 505.",
				"n=4: 22 -\u003e 44 -\u003e 88 -\u003e 66 -\u003e 22.",
				"n=6: 109 -\u003e 190 -\u003e 091 -\u003e 901 -\u003e 910 -\u003e 019 -\u003e 109.",
				"n=12: 101 -\u003e 112 -\u003e 233 -\u003e 565 -\u003e 110 -\u003e 211 -\u003e 323 -\u003e 556 -\u003e 011 -\u003e 121 -\u003e 332 -\u003e 655 -\u003e 101.",
				"n=15: 50005 -\u003e 50050 -\u003e 50555 -\u003e 55000 -\u003e 05005 -\u003e 55055 -\u003e 05500 -\u003e 50500 -\u003e 55505 -\u003e 00550 -\u003e 05050 -\u003e 55550 -\u003e 00055 -\u003e 00505 -\u003e 05555 -\u003e 50005.",
				"Etc."
			],
			"maple": [
				"# Maple program lists {step,least number} when found (not in order).",
				"P:=proc(q) local a,b,c,d,j,k,n,ok,v;",
				"v:=array(1..5000); for k from 1 to 5000 do v[k]:=0; od;",
				"for n from 10 to q do a:=[]; b:=n; while b\u003e0 do a:=[b mod 10,op(a)];",
				"b:=trunc(b/10); od; b:=0; c:=a; ok:=0;",
				"for k from 1 to 10^nops(a) do b:=b+1; d:=(c[nops(c)]+c[1]) mod 10;",
				"for j from 1 to nops(c)-1 do c[j]:=(c[j]+c[j+1]) mod 10; od; c[nops(c)]:=d;",
				"if a=c then ok:=1; break; fi; od; if ok=1 then if v[b]=0 then v[b]:=n;",
				"print(n); fi; fi; od; end: P(10^10);"
			],
			"mathematica": [
				"g[n_] := Block[{e = 1 + Floor@ Log10@ n, id = Reverse@ IntegerDigits@ n}, PrependTo[id, id[[-1]]]; Sum[10^(k - 1)*Mod[id[[k]] + id[[k + 1]], 10], {k, e}]]; f[n_] := Block[{lst = NestWhileList[ g@# \u0026, n, UnsameQ || ## != 0, All]}, If[ lst[[-1]] == n, Length@ lst - 1, 0]];"
			],
			"xref": [
				"Cf. A243994."
			],
			"keyword": "sign,base,more",
			"offset": "0,3",
			"author": "_Paolo P. Lava_ and _Robert G. Wilson v_, Jun 17 2014",
			"references": 8,
			"revision": 34,
			"time": "2021-02-06T21:51:21-05:00",
			"created": "2014-07-12T17:49:51-04:00"
		}
	]
}