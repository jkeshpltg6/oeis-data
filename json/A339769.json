{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A339769",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 339769,
			"data": "1,2,4,4,5,6,10,12,9,7,4,9,13,11,7,6,8,10,13,14,10,15,14,21,8,7,13,21",
			"name": "Number of unique heights that are achieved by only one starting number in the Collatz (or '3x+1') problem when starting from numbers in the range [2^n, 2^(n+1)).",
			"comment": [
				"The number of unique heights in each interval [2^n, 2^(n+1)) appears to be linear in n - with large oscillations about the best fit line - while the values of attained maximum heights appear to be quadratic in n.",
				"Neither this sequence nor the corresponding sequence of maximum heights in each interval is monotone, e.g.:  a(23..27) = (21, 8, 7, 13, 21) and the corresponding maximum heights are (704, 702, 949, 947, 956)."
			],
			"example": [
				"a(5)=6 since the 6 heights 5, 26, 34, 109, 29, 104 are uniquely attained from the starting numbers 32, 33, 39, 41, 43, 47, respectively. The largest of the distinct heights (A280341) in the interval [32,64) however is 112.",
				"a(11)=9 with largest unique height 237 for starting value 3711 in interval [2^11, 2^12) also is the largest height for all starting values in the interval."
			],
			"mathematica": [
				"collatz[n_] := If[EvenQ[n], n/2, 3n+1]",
				"height[n_] := Length[NestWhileList[collatz, n, #!=1\u0026]] - 1",
				"a339769[n_] := Module[{heightL={}, countL={}, s, h, p}, For[s=2^n, s\u003c2^(n+1), s++, h=height[s]; If[!MemberQ[heightL, h], AppendTo[heightL, h]; AppendTo[countL, 1], {{p}}=Position[heightL, h]; countL[[p]]+=1]]; Length[Select[Transpose[{heightL, countL}], #[[2]]==1\u0026]]]",
				"(* sequence data; long computation times for n \u003e= 22 *)",
				"Map[a339769, Range[0, 27]]"
			],
			"xref": [
				"Cf. A006577, A277109, A280341."
			],
			"keyword": "nonn,more",
			"offset": "0,2",
			"author": "_Hartmut F. W. Hoft_, Dec 16 2020",
			"references": 2,
			"revision": 11,
			"time": "2021-02-01T13:46:45-05:00",
			"created": "2020-12-16T19:34:12-05:00"
		}
	]
}