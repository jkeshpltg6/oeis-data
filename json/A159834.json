{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A159834",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 159834,
			"data": "1,-1,1,0,-2,1,2,0,-3,1,-2,8,0,-4,1,-6,-10,20,0,-5,1,16,-36,-30,40,0,-6,1,20,112,-126,-70,70,0,-7,1,-132,160,448,-336,-140,112,0,-8,1,-28,-1188,720,1344,-756,-252,168,0,-9,1",
			"name": "Coefficient array of Hermite_H(n, (x-1)/sqrt(2))/(sqrt(2))^n.",
			"comment": [
				"Exponential Riordan array [exp(-x-x^2/2), x]."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A159834/b159834.txt\"\u003eRows n=0..100 of triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"G.f.: 1/(1-xy+x+x^2/(1-xy+x+2x^2/(1-xy+x+3x^2/(1-xy+x+4x^2/(1-... (continued fraction).",
				"From _Tom Copeland_, Jun 26 2018: (Start)",
				"E.g.f.: exp[t*p.(x)] = exp[-(t + t^2/2)] e^(x*t).",
				"T(n,k) = binomial(n,k) * A001464(n-k).",
				"These polynomials (p.(x))^n = p_n(x) are an Appell sequence with the lowering and raising operators L = D and R = x - 1 - D, with D = d/dx, such that L p_n(x) = n * p_(n-1)(x) and R p_n(x) = p_(n+1)(x), so the formalism of A133314 applies here, giving recursion relations.",
				"The transpose of the production matrix gives a matrix representation of the raising operator R, with left multiplication of the rows of this entry treated as column vectors.",
				"exp(-(D + D^2/2)) x^n= e^(-D^2/2) (x - 1)^n = He_n(x-1) = p_n(x) = (a. + x)^n, with (a.)^n = a_n = A001464(n) and He_n(x), the unitary or normalized Hermite polynomials of A066325.",
				"A111062 with the e.g.f. exp[t + t^2/2] e^(x*t) gives the matrix inverse for this entry with the umbral inverse polynomials q_n(x), an Appell sequence with the raising operator  x + 1 + D, such that umbrally composed q_n(p.(x)) = x^n = p_n(q.(x)). (End)"
			],
			"example": [
				"Triangle begins:",
				"     1,",
				"    -1,    1,",
				"     0,   -2,    1,",
				"     2,    0,   -3,    1,",
				"    -2,    8,    0,   -4,    1,",
				"    -6,  -10,   20,    0,   -5,    1,",
				"    16,  -36,  -30,   40,    0,   -6,    1,",
				"    20,  112, -126,  -70,   70,    0,   -7,    1,",
				"  -132,  160,  448, -336, -140,  112,    0,   -8,    1",
				"Production matrix is:",
				"  -1,  1,",
				"  -1, -1,  1,",
				"   0, -2, -1,  1,",
				"   0,  0, -3, -1,  1,",
				"   0,  0,  0, -4, -1,  1,",
				"   0,  0,  0,  0, -5, -1,  1,",
				"   0,  0,  0,  0,  0, -6, -1,  1,",
				"   0,  0,  0,  0,  0,  0, -7, -1,  1"
			],
			"maple": [
				"Trow := proc(n) local b, f; b := proc(n, m) option remember; if n \u003c m or m \u003c 0 then",
				"0 elif n = 0 and m = 0 then 1 else b(n-1, m) + b(n-1, m-1) fi end:",
				"f := proc(n) option remember; if n = 0 then 1 elif n = 1 then -1",
				"else f(n-2) - f(n-1) - f(n-2)*n fi end; seq(b(n, k)*f(n-k), k=0..n) end:",
				"seq(Trow(n), n=0..20); # _Peter Luschny_, Aug 19 2018"
			],
			"mathematica": [
				"T[n_] := CoefficientList[Series[HermiteH[n, (x-1)/Sqrt[2]], {x, 0, 50}], x]/ (Sqrt[2])^n; Table[T[n], {n, 0, 20}] // Flatten (* _G. C. Greubel_, May 19 2018 *)"
			],
			"program": [
				"(PARI) row(n) = apply(x-\u003eround(x), Vecrev(polhermite(n, (x-1)/sqrt(2))/ (sqrt(2))^n));",
				"tabl(nn) = for (n=0, nn, print(row(n))); \\\\ _Michel Marcus_, Aug 11 2018"
			],
			"xref": [
				"Inverse of A111062.",
				"Equal to A066325*(A007318)^{-1}.",
				"First column is A001464.",
				"Row sums are (-1)^n*A001147(n) aerated.",
				"Cf. A133314."
			],
			"keyword": "easy,sign,tabl",
			"offset": "0,5",
			"author": "_Paul Barry_, Apr 23 2009",
			"references": 3,
			"revision": 21,
			"time": "2018-08-19T13:43:51-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}