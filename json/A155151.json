{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A155151",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 155151,
			"data": "10,16,26,22,36,50,28,46,64,82,34,56,78,100,122,40,66,92,118,144,170,46,76,106,136,166,196,226,52,86,120,154,188,222,256,290,58,96,134,172,210,248,286,324,362,64,106,148,190,232,274,316,358,400,442,70,116,162",
			"name": "Triangle T(n, k) = 4*n*k + 2*n + 2*k + 2, read by rows.",
			"comment": [
				"First column: A016957, second column: A017341, third column: 2*A017029, fourth column: A082286. - _Vincenzo Librandi_, Nov 21 2012",
				"Conjecture: Let p = prime number. If 2^p belongs to the sequence, then 2^p-1 is not a Mersenne prime. - _Vincenzo Librandi_, Dec 12 2012",
				"Conjecture is true because if T(n, k) = 2^p with p prime, then 2^p-1 = 4*n*k + 2*n + 2*k + 1 = (2*n+1)*(2*k+1) hence 2^p-1 is not prime. - _Michel Marcus_, May 31 2015",
				"It appears that T(m,p) = 2^p for Lucasian primes (A002515) greater than 3. For instance: T(44, 11) = 2^11, T(89240, 23) = 2^23. - _Michel Marcus_, May 28 2015",
				"For n \u003e 1, ascending numbers along the diagonal are also terms of the even principal diagonal of a 2n X 2n spiral (A137928). - _Avi Friedlich_, May 21 2015"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A155151/b155151.txt\"\u003eRows n = 1..100, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = 2*A144650(n, k).",
				"Sum_{k=1..n} T(n,k) = n*(2*n^2 + 5*n + 3) = n*A014105(n+2) ="
			],
			"example": [
				"Triangle begins",
				"  10;",
				"  16,  26;",
				"  22,  36,  50;",
				"  28,  46,  64,  82;",
				"  34,  56,  78, 100, 122;",
				"  40,  66,  92, 118, 144, 170;",
				"  46,  76, 106, 136, 166, 196, 226;",
				"  52,  86, 120, 154, 188, 222, 256, 290;",
				"  58,  96, 134, 172, 210, 248, 286, 324, 362;",
				"  64, 106, 148, 190, 232, 274, 316, 358, 400, 442;"
			],
			"maple": [
				"seq(seq( 2*(2*n*k+n+k+1), k=1..n), n=1..15) # _G. C. Greubel_, Mar 21 2021"
			],
			"mathematica": [
				"T[n_,k_]:=4*n*k + 2*n + 2*k + 2; Table[T[n, k], {n, 11}, {k, n}]//Flatten (* _Vincenzo Librandi_, Nov 21 2012 *)"
			],
			"program": [
				"(MAGMA) [4*n*k + 2*n + 2*k + 2: k in [1..n], n in [1..11]]; // _Vincenzo Librandi_, Nov 21 2012",
				"(Sage) flatten([[2*(2*n*k+n+k+1) for k in (1..n)] for n in (1..15)]) # _G. C. Greubel_, Mar 21 2021"
			],
			"xref": [
				"Cf. A000043, A000668, A016957, A017029, A017341, A054723, A082286, A144650."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "1,1",
			"author": "_Vincenzo Librandi_, Jan 21 2009",
			"ext": [
				"Edited by _Robert Hochberg_, Jun 21 2010"
			],
			"references": 4,
			"revision": 67,
			"time": "2021-03-21T13:10:13-04:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}