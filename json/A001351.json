{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001351",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1351,
			"id": "M2217 N0879",
			"data": "0,1,3,1,3,11,9,8,27,37,33,67,117,131,192,341,459,613,999,1483,2013,3032,4623,6533,9477,14311,20829,30007,44544,65657,95139,139625,206091,300763,439521,646888,948051,1385429,2033193,2983787,4366197,6397723,9387072",
			"name": "Associated Mersenne numbers.",
			"comment": [
				"From _Peter Bala_, Sep 15 2019: (Start)",
				"This is a linear divisibility sequence of order 6 (Haselgrove, p. 21). It is a particular case of a family of divisibility sequences studied by Roettger et al. The o.g.f. has the form x*d/dx(f(x)/(x^3*f(1/x))) where f(x) = x^3 - x^2 - 1.",
				"More generally, if f(x) = 1 + P*x + Q*x^2 + x^3 or f(x) = -1 + P*x + Q*x^2 + x^3, where P and Q are integers, then the rational function x*d/dx(f(x)/(x^3*f(1/x))) is the generating function for a linear divisibility sequence of order 6. Cf. A001945. There are corresponding results when f(x) is a monic quartic polynomial with constant term 1. (End)"
			],
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Danny Rorabaugh, \u003ca href=\"/A001351/b001351.txt\"\u003eTable of n, a(n) for n = 0..6000\u003c/a\u003e",
				"P. Bala, \u003ca href=\"/A001351/a001351_1.pdf\"\u003eSome linear divisibility sequences of order 6\u003c/a\u003e",
				"C. B. Haselgrove, \u003ca href=\"/A001350/a001350.pdf\"\u003eAssociated Mersenne numbers\u003c/a\u003e, Eureka, 11 (1949), 19-22. [Annotated and scanned copy]",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"E. L. Roettger, H. C. Williams, R. K. Guy, \u003ca href=\"https://carma.newcastle.edu.au/meetings/alfcon/pdfs/Hugh_Williams-alfcon.pdf\"\u003eSome extensions of the Lucas functions\u003c/a\u003e, Number Theory and Related Fields: In Memory of Alf van der Poorten, Series: Springer Proceedings in Mathematics \u0026 Statistics, Vol. 43, J. Borwein, I. Shparlinski, W. Zudilin (Eds.) 2013.",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,-1,3,-1,1,-1)."
			],
			"formula": [
				"a(n) = a(n-1) - a(n-2) + 3*a(n-3) - a(n-4) + a(n-5) - a(n-6) for n \u003e= 6. - _Sean A. Irvine_, Sep 23 2015",
				"a(n) = (alpha^n - 1)*(beta^n - 1)*(gamma^n - 1) where alpha, beta and gamma are the zeros of x^3 - x^2 - 1. - _Peter Bala_, Sep 15 2019"
			],
			"maple": [
				"A001351:=z*(z^2-z+1)*(z^2+3*z+1)/(z^3+z-1)/(z^3-z^2-1); # conjectured by _Simon Plouffe_ in his 1992 dissertation"
			],
			"mathematica": [
				"LinearRecurrence[{1, -1, 3, -1, 1, -1}, {0, 1, 3, 1, 3, 11}, 50] (* _Vincenzo Librandi_, Sep 23 2015 *)"
			],
			"program": [
				"(MAGMA) I:=[0,1,3,1,3,11]; [n le 6 select I[n] else Self(n-1) - Self(n-2) + 3*Self(n-3) - Self(n-4) + Self(n-5) - Self(n-6): n in [1..50]]; // _Vincenzo Librandi_, Sel 23 2015"
			],
			"xref": [
				"Cf. A001350, A001945."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_, _R. K. Guy_",
			"ext": [
				"More terms from _Vincenzo Librandi_, Sep 23 2015"
			],
			"references": 4,
			"revision": 54,
			"time": "2021-03-12T22:32:34-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}