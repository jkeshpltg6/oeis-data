{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A321768",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 321768,
			"data": "3,5,21,15,7,55,45,39,119,77,33,65,35,9,105,91,105,297,187,95,207,117,57,377,299,217,697,459,175,319,165,51,275,209,115,403,273,85,133,63,11,171,153,203,555,345,189,429,247,155,987,777,539,1755,1161,429",
			"name": "Consider the ternary tree of triples P(n, k) with n \u003e 0 and 0 \u003c k \u003c= 3^(n-1), such that P(1, 1) = [3; 4; 5] and each triple t on some row branches to the triples A*t, B*t, C*t on the next row (with A = [1, -2, 2; 2, -1, 2; 2, -2, 3], B = [1, 2, 2; 2, 1, 2; 2, 2, 3] and C = [-1, 2, 2; -2, 1, 2; -2, 2, 3]); T(n, k) is the first component of P(n, k).",
			"comment": [
				"The tree P runs uniquely through every primitive Pythagorean triple.",
				"The ternary tree is built as follows:",
				"- for any n and k such that n \u003e 0 and 0 \u003c k \u003c= 3^(n-1):",
				"- P(n, k) is a column vector,",
				"- P(n+1, 3*k-2) = A * P(n, k),",
				"- P(n+1, 3*k-1) = B * P(n, k),",
				"- P(n+1, 3*k)   = C * P(n, k).",
				"All terms are odd.",
				"Every primitive Pythagorean triple (a, b, c) can be characterized by a pair of parameters (i, j) such that:",
				"- i \u003e j \u003e 0 and gcd(i, j) = 1 and i and j are of opposite parity,",
				"- a = i^2 - j^2,",
				"- b = 2 * i * j,",
				"- c = i^2 + j^2,",
				"- A321782(n, k) and A321783(n, k) respectively give the value of i and of j pertaining to (A321768(n, k), A321769(n, k), A321770(n, k)).",
				"Every primitive Pythagorean triple (a, b, c) can also be characterized by a pair of parameters (u, v) such that:",
				"- u \u003e v \u003e 0 and gcd(u, v) = 1 and m and n are odd,",
				"- a = u * v,",
				"- b = (u^2 - v^2) / 2,",
				"- c = (u^2 + v^2) / 2,",
				"- A321784(n, k) and A321785(n, k) respectively give the value of u and of v pertaining to (A321768(n, k), A321769(n, k), A321770(n, k))."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A321768/b321768.txt\"\u003eRows n = 1..9, flattened\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Tree_of_primitive_Pythagorean_triples\"\u003eTree of primitive Pythagorean triples\u003c/a\u003e",
				"\u003ca href=\"/index/Ps#PyTrip\"\u003eIndex entries related to Pythagorean Triples\u003c/a\u003e"
			],
			"formula": [
				"Empirically:",
				"- T(n, 1) = 2*n + 1,",
				"- T(n, (3^(n-1) + 1)/2) = A046727(n),",
				"- T(n, 3^(n-1)) = 4*n^2 - 1."
			],
			"example": [
				"The first rows are:",
				"   3",
				"   5, 21, 15",
				"   7, 55, 45, 39, 119, 77, 33, 65, 35"
			],
			"program": [
				"(PARI) M = [[1, -2, 2; 2, -1, 2; 2, -2, 3], [1, 2, 2; 2, 1, 2; 2, 2, 3], [-1, 2, 2; -2, 1, 2; -2, 2, 3]];",
				"T(n,k) = my (t=[3;4;5], d=digits(3^(n-1)+k-1, 3)); for (i=2, #d, t = M[d[i]+1] * t); return (t[1,1])"
			],
			"xref": [
				"See A321769 and A321770 for the other components.",
				"See A322170 for the corresponding areas.",
				"See A322181 for the corresponding perimeters.",
				"Cf. A321782, A321783, A321784, A321785.",
				"Cf. A046727."
			],
			"keyword": "nonn,tabf",
			"offset": "1,1",
			"author": "_Rémy Sigrist_, Nov 18 2018",
			"references": 9,
			"revision": 22,
			"time": "2018-12-01T08:27:13-05:00",
			"created": "2018-11-29T16:02:44-05:00"
		}
	]
}