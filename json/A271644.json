{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A271644",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 271644,
			"data": "1,3,1,1,4,4,1,3,4,4,2,2,5,2,1,1,8,8,2,5,7,3,2,4,8,7,3,2,6,4,4,3,7,6,2,4,6,4,3,4,9,4,3,4,8,4,1,2,5,7,4,7,10,11,3,2,5,5,2,2,7,4,2,1,8,9,2,8,14,9,1,8,8,6,5,4,8,2,3,5",
			"name": "Number of ordered ways to write n as w^2 + x^2 + y^2 + z^2 such that w*x + 2*x*y + 2*y*z is a square, where w is a positive integer and x,y,z are nonnegative integers.",
			"comment": [
				"Conjecture: (i) a(n) \u003e 0 for all n \u003e 0, and a(n) = 1 only for n = 3, 7, 15, 47, 71, 379, 4^k (k = 0,1,2,...).",
				"(ii) If a, b and c are positive integers with a \u003c= b \u003c= c, gcd(a,b,c) squarefree, and the triple (a,b,c) not equal to (1,2,2), then not all natural numbers can be written as w^2 + x^2 + y^2 + z^2 with w,x,y,z nonnegative integers and a*w*x + b*x*y + c*y*z a square.",
				"（iii) Let a,b,c be positive integers with gcd(a,b,c) squarefree. Then every n = 0,1,2,... can be written as w^2 + x^2 + y^2 + z^2 with w,x,y,z nonnegative integers such that a*x*y + b*y*z + c*z*x is a square, if and only if {a,b,c} is among {1,2,3}, {1,3,8}, {1,8,13}, {2,4,45}, {4,5,7}, {4,7,23}, {5,8,9}, {11,16,31}.",
				"Clearly, part (i) of this conjecture is stronger than Lagrange's four-square theorem.",
				"See also A271510, A271513, A271518 and A271608 for related conjectures."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A271644/b271644.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1604.06723\"\u003eRefining Lagrange's four-square theorem\u003c/a\u003e, arXiv:1604.06723 [math.GM], 2016."
			],
			"example": [
				"a(1) = 1 since 1 = 1^2 + 0^2 + 0^2 + 0^2 with 1*0 + 2*0*0 + 2*0*0 = 0^2.",
				"a(3) = 1 since 3 = 1^2 + 1^2 + 0^2 + 1^2 with 1*1 + 2*1*0 + 2*0*1 = 1^2.",
				"a(7) = 1 since 7 = 1^2 + 1^2 + 2^2 + 1^2 with 1*1 + 2*1*2 + 2*2*1 = 3^2.",
				"a(11) = 2 since 11 = 1^2 + 1^2 + 0^2 + 3^2 with 1*1 + 2*1*0 + 2*0*3 = 1^2, and 11 = 1^2 + 3^2 + 1^2 + 0^2 with 1*3 + 2*3*1 + 2*1*0 = 3^2.",
				"a(12) = 2 since 12 = 1^2 + 1^2 + 1^2 + 3^2 with 1*1 + 2*1*1 + 2*1*3 = 3^2, and 12 = 2^2 + 2^2 + 0^2 + 2^2  with 2*2 + 2*2*0 + 2*0*2 = 2^2.",
				"a(15) = 1 since 15 = 3^2 + 1^2 + 1^2 + 2^2 with 3*1 + 2*1*1 + 2*1*2 = 3^2.",
				"a(47) = 1 since 47 = 1^2 + 1^2 + 6^2 + 3^2 with 1*1 + 2*1*6 + 2*6*3 = 7^2.",
				"a(71) = 1 since 71 = 3^2 + 3^2 + 2^2 + 7^2 with 3*3 + 2*3*2 + 2*2*7 = 7^2.",
				"a(379) = 1 since 379 = 3^2 + 3^2 + 0^2 + 19^2 with 3*3 + 2*3*0 + 2*0*19 = 3^2."
			],
			"mathematica": [
				"SQ[n_]:=SQ[n]=IntegerQ[Sqrt[n]]",
				"Do[r=0;Do[If[SQ[n-w^2-x^2-y^2]\u0026\u0026SQ[w*x+2*x*y+2*y*Sqrt[n-w^2-x^2-y^2]],r=r+1],{w,1,Sqrt[n]},{x,0,Sqrt[n-w^2]},{y,0,Sqrt[n-w^2-x^2]}];Print[n,\" \",r];Continue,{n,1,80}]"
			],
			"xref": [
				"Cf. A000118, A000290, A271510, A271513, A271518, A271608."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Zhi-Wei Sun_, Apr 11 2016",
			"references": 8,
			"revision": 17,
			"time": "2016-11-20T04:00:04-05:00",
			"created": "2016-04-11T21:16:57-04:00"
		}
	]
}