{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A209877",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 209877,
			"data": "1,4,2,6,3,16,15,25,23,17,11,5,38,49,50,22,14,40,81,56,7,61,72,32,8,41,30,114,69,144,57,74,68,21,52,137,167,10,133,196,127,191,174,24,104,143,26,59,43,12,258,238,289,97,77,252,53,29,13,283,48,190,335,361,31,228,291,159,263,123,260,325,363,247,162",
			"name": "a(n) = A209874(n)/2: Least m \u003e 0 such that 4*m^2 = -1 modulo the Pythagorean prime A002144(n).",
			"comment": [
				"Also: Square root of -1/4 in Z/pZ, for Pythagorean primes p=A002144(n).",
				"Also: Least m\u003e0 such that the Pythagorean prime p=A002144(n) divides 4(kp +/- m)^2+1 for all k\u003e=0.",
				"In practice these can also be determined by searching the least N^2+1 whose least prime factor is p=A002144(n): For given p, all of these N will have a(n) or p-a(n) as remainder mod 2p."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A209877/b209877.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"a(1)=1 since A002144(1)=5 and 4*1^2+1 is divisible by 5; as a consequence 4*(5k+/-1)^2+1 = 100k^2 +/- 40k + 5 is divisible by 5 for all k.",
				"a(2)=4 since A002144(2)=13 and 4*4^2+1 = 65 is divisible by 13, while 4*1^1+1=5, 4*2^2+1=17 and 4*3^2+1=37 are not. As a consequence, 4*(13k+/-4)^2+1 = 13(...)+4*4^1+1 is divisible by 13 for all k."
			],
			"maple": [
				"f:= proc(p) local m;",
				"   if not isprime(p) then return NULL fi;",
				"   m:= numtheory:-msqrt(-1/4, p);",
				"   min(m,p-m);",
				"end proc:",
				"map(f, [seq(i,i=5..1000,4)]); # _Robert Israel_, Mar 13 2018"
			],
			"mathematica": [
				"f[p_] := Module[{r}, r /. Solve[4 r^2 == -1, r, Modulus -\u003e p] // Min];",
				"f /@ Select[4 Range[300] + 1, PrimeQ] (* _Jean-François Alcover_, Jul 27 2020 *)"
			],
			"program": [
				"(PARI) apply(p-\u003elift(sqrt(Mod(-1,p)/4)), A002144)"
			],
			"xref": [
				"Cf. A209874.",
				"Cf. A002496, A014442, A085722, A144255, A089120, A193432.",
				"Cf. A002314, A177979."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_M. F. Hasler_, Mar 14 2012",
			"references": 2,
			"revision": 21,
			"time": "2020-07-27T15:16:24-04:00",
			"created": "2012-03-15T13:45:51-04:00"
		}
	]
}