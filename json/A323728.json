{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A323728",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 323728,
			"data": "2,5,10,8,26,13,50,20,18,29,122,25,170,53,34,32,290,45,362,41,58,125,530,52,50,173,90,65,842,61,962,80,130,293,74,72,1370,365,178,89,1682,85,1850,137,106,533,2210,100,98,125,298,185,2810,117,146,113,370",
			"name": "a(n) is the smallest number k such that both k-2*n and k+2*n are squares.",
			"comment": [
				"When n is a prime number, a(n) is greater than all the previous terms.",
				"If n = 4*x*y, then a(n) is the smallest integer solution of the form 4*(x^2 + y^2), with rational values x and y."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A323728/b323728.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Difference_of_two_squares\"\u003eDifference of two squares\u003c/a\u003e"
			],
			"formula": [
				"a(n^2) = 2 * n^2.",
				"a(p) = p^2 + 1, for p prime.",
				"a(n) = A063655(n)^2 - 2*n.",
				"a(n) = A056737(n)^2 + 2*n.",
				"a(n!) = A061057(n)^2 + 2*n!.",
				"a(n) = A033676(n)^2 + A033677(n)^2. - _Robert Israel_, Feb 17 2019"
			],
			"example": [
				"For n = 3, a(3) = 10, which is the smallest integer k such that k+2*n and k-2*n are both squares: 10+2*3 = 4^2 and 10-2*3 = 2^2.",
				"For n=1..10, the following {a(n)-2*n, a(n)+2*n} pairs of squares are produced: {0, 4}, {1, 9}, {4, 16}, {0, 16}, {16, 36}, {1, 25}, {36, 64}, {4, 36}, {0, 36}, {9, 49}."
			],
			"maple": [
				"f:= proc(n) local d;",
				"d:= max(select(t -\u003e t^2 \u003c= n, numtheory:-divisors(n)));",
				"d^2 + (n/d)^2",
				"end proc:",
				"map(f, [$1..100]); # _Robert Israel_, Feb 17 2019"
			],
			"mathematica": [
				"Array[Block[{k = 1}, While[Nand @@ Map[IntegerQ, Sqrt[k + 2 {-#, #}]], k++]; k] \u0026, 57] (* _Michael De Vlieger_, Feb 17 2019 *)"
			],
			"program": [
				"(PARI) a(n) = for(k=2*n, oo, if(issquare(k+2*n) \u0026\u0026 issquare(k-2*n), return(k)));",
				"(PARI) a(n) = my(d=divisors(n)); vecmin(vector(#d, k, 4*((d[k]/2)^2 + (n/d[k]/2)^2)));"
			],
			"xref": [
				"Cf. A000290, A029744, A033676. A033677, A056737, A061057, A063655, A087711."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Daniel Suteu_, Jan 25 2019",
			"references": 1,
			"revision": 19,
			"time": "2019-02-18T02:15:15-05:00",
			"created": "2019-02-17T20:51:15-05:00"
		}
	]
}