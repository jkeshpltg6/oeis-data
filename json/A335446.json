{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A335446",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 335446,
			"data": "0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,2,0,0,0,1,0,0,0,0,0,0,0,3,0,0,0,2,0,0,0,1,1,0,0,3,0,0,0,1,0,0,0,2,0,0,0,6,0,0,1,0,0,0,0,1,0,0,0,7,0,0,0,1,0,0,0,3,0,0,0,6,0,0,0",
			"name": "Number of (1,2,1)-matching permutations of the prime indices of n.",
			"comment": [
				"Depends only on unsorted prime signature (A124010), but not only on sorted prime signature (A118914).",
				"A prime index of n is a number m such that prime(m) divides n. The multiset of prime indices of n is row n of A112798.",
				"We define a pattern to be a finite sequence covering an initial interval of positive integers. Patterns are counted by A000670. A sequence S is said to match a pattern P if there is a not necessarily contiguous subsequence of S whose parts have the same relative order as P. For example, (3,1,1,3) matches (1,1,2), (2,1,1), and (2,1,2), but avoids (1,2,1), (1,2,2), and (2,2,1)."
			],
			"link": [
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Permutation_pattern\"\u003ePermutation pattern\u003c/a\u003e",
				"Gus Wiseman, \u003ca href=\"/A102726/a102726.txt\"\u003eSequences counting and ranking compositions by the patterns they match or avoid.\u003c/a\u003e"
			],
			"example": [
				"The a(n) permutations for n = 12, 24, 36, 60, 72, 90, 120, 144:",
				"  (121)  (1121)  (1212)  (1213)  (11212)  (1232)  (11213)  (111212)",
				"         (1211)  (1221)  (1231)  (11221)  (2132)  (11231)  (111221)",
				"                 (2121)  (1312)  (12112)  (2312)  (11312)  (112112)",
				"                         (1321)  (12121)  (2321)  (11321)  (112121)",
				"                         (2131)  (12211)          (12113)  (112211)",
				"                         (3121)  (21121)          (12131)  (121112)",
				"                                 (21211)          (12311)  (121121)",
				"                                                  (13112)  (121211)",
				"                                                  (13121)  (122111)",
				"                                                  (13211)  (211121)",
				"                                                  (21131)  (211211)",
				"                                                  (21311)  (212111)",
				"                                                  (31121)",
				"                                                  (31211)"
			],
			"mathematica": [
				"primeMS[n_]:=If[n==1,{},Flatten[Cases[FactorInteger[n],{p_,k_}:\u003eTable[PrimePi[p],{k}]]]];",
				"Table[Length[Select[Permutations[primeMS[n]],MatchQ[#,{___,x_,___,y_,___,x_,___}/;x\u003cy]\u0026]],{n,100}]"
			],
			"xref": [
				"Positions of zeros are A065200.",
				"The avoiding version is A335449.",
				"Patterns are counted by A000670.",
				"Permutations of prime indices are counted by A008480.",
				"Unimodal permutations of prime indices are counted by A332288.",
				"(1,2,1) and (2,1,2)-avoiding permutations of prime indices are A333175.",
				"STC-numbers of permutations of prime indices are A333221.",
				"Patterns matched by standard compositions are counted by A335454.",
				"(1,2,1) or (2,1,2)-matching permutations of prime indices are A335460.",
				"(1,2,1) and (2,1,2)-matching permutations of prime indices are A335462.",
				"Dimensions of downsets of standard compositions are A335465.",
				"(1,2,1)-matching compositions are ranked by A335466.",
				"(1,2,1)-matching compositions are counted by A335470.",
				"(1,2,1)-matching patterns are counted by A335509.",
				"Cf. A056239, A056986, A112798, A158005, A158009, A181796, A335452, A335463."
			],
			"keyword": "nonn",
			"offset": "1,24",
			"author": "_Gus Wiseman_, Jun 13 2020",
			"references": 15,
			"revision": 16,
			"time": "2020-06-29T22:20:46-04:00",
			"created": "2020-06-14T22:46:15-04:00"
		}
	]
}