{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A217581",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 217581,
			"data": "1,1,1,2,1,2,1,2,3,2,1,3,1,2,3,2,1,3,1,2,3,2,1,3,5,2,3,2,1,5,1,2,3,2,5,3,1,2,3,5,1,3,1,2,5,2,1,3,7,5,3,2,1,3,5,7,3,2,1,5,1,2,7,2,5,3,1,2,3,7,1,3,1,2,5,2,7,3,1,5,3,2,1,7,5,2,3",
			"name": "Largest prime divisor of n \u003c= sqrt(n), 1 if n is prime or 1.",
			"comment": [
				"If we define a divisor d|n to be inferior if d \u003c= n/d, then inferior divisors are counted by A038548 and listed by A161906. This sequence selects the greatest inferior prime divisor of n. - _Gus Wiseman_, Apr 06 2021"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A217581/b217581.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"From _Gus Wiseman_, Apr 06 2021: (Start)",
				"The sequence selects the greatest element (or 1 if empty) of each of the following sets of strictly superior divisors:",
				"   1:{}     16:{2}      31:{}     46:{2}",
				"   2:{}     17:{}       32:{2}    47:{}",
				"   3:{}     18:{2,3}    33:{3}    48:{2,3}",
				"   4:{2}    19:{}       34:{2}    49:{7}",
				"   5:{}     20:{2}      35:{5}    50:{2,5}",
				"   6:{2}    21:{3}      36:{2,3}  51:{3}",
				"   7:{}     22:{2}      37:{}     52:{2}",
				"   8:{2}    23:{}       38:{2}    53:{}",
				"   9:{3}    24:{2,3}    39:{3}    54:{2,3}",
				"  10:{2}    25:{5}      40:{2,5}  55:{5}",
				"  11:{}     26:{2}      41:{}     56:{2,7}",
				"  12:{2,3}  27:{3}      42:{2,3}  57:{3}",
				"  13:{}     28:{2}      43:{}     58:{2}",
				"  14:{2}    29:{}       44:{2}    59:{}",
				"  15:{3}    30:{2,3,5}  45:{3,5}  60:{2,3,5}"
			],
			"maple": [
				"A217581 := n -\u003e `if`(isprime(n) or n=1, 1, max(op(select(i-\u003ei^2\u003c=n, numtheory[factorset](n)))));"
			],
			"mathematica": [
				"Table[If[n == 1 || PrimeQ[n], 1, Select[Transpose[FactorInteger[n]][[1]], # \u003c= Sqrt[n] \u0026][[-1]]], {n, 100}] (* _T. D. Noe_, Mar 25 2013 *)"
			],
			"xref": [
				"Cf. A033676.",
				"Positions of first appearances are 1 and A001248.",
				"These divisors are counted by A063962.",
				"These divisors add up to A097974.",
				"The smallest prime factor of the same type is A107286.",
				"A strictly superior version is A341643.",
				"A superior version is A341676.",
				"A038548 counts superior (or inferior) divisors.",
				"A048098 lists numbers without a strictly superior prime divisor.",
				"A056924 counts strictly superior (or strictly inferior) divisors.",
				"A063538/A063539 have/lack a superior prime divisor.",
				"A140271 selects the smallest strictly superior divisor.",
				"A161906 lists inferior divisors.",
				"A207375 lists central divisors.",
				"A341591 counts superior prime divisors.",
				"A341642 counts strictly superior prime divisors.",
				"A341673 lists strictly superior divisors.",
				"- Inferior: A066839, A069288, A333749, A333750.",
				"- Superior: A033677, A051283, A059172, A070038, A116882, A116883, A161908, A341592, A341593, A341675.",
				"- Strictly Inferior: A060775, A333805, A333806, A341596, A341674.",
				"- Strictly Superior: A238535, A341594, A341595, A341644, A341645, A341646.",
				"Cf. A000005, A001055, A001221, ~A001222, A001248, A001414, A006530, A020639, A064052."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Peter Luschny_, Mar 21 2013",
			"references": 30,
			"revision": 53,
			"time": "2021-04-10T08:08:17-04:00",
			"created": "2013-03-25T12:26:36-04:00"
		}
	]
}