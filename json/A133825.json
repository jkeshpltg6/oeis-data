{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A133825",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 133825,
			"data": "1,1,3,1,1,3,6,3,1,1,3,6,10,6,3,1,1,3,6,10,15,10,6,3,1,1,3,6,10,15,21,15,10,6,3,1,1,3,6,10,15,21,28,21,15,10,6,3,1,1,3,6,10,15,21,28,36,28,21,15,10,6,3,1,1,3,6,10,15,21,28,36,45,36,28,21,15,10,6,3,1,1,3,6,10",
			"name": "Triangle whose rows are sequences of increasing and decreasing triangular numbers: 1; 1,3,1; 1,3,6,3,1; ... .",
			"comment": [
				"Reading the triangle by rows produces the sequence 1,1,3,1,1,3,6,3,1,..., analogous to A004737.",
				"T(n,k) =  min(n*(n+1)/2,k*(k+1)/2), n, k \u003e0.  The order of the list T(n,k) is by sides of squares from T(1,n) to T(n,n), then from T(n,n) to T(n,1). - _Boris Putievskiy_, Jan 13 2013"
			],
			"link": [
				"Harvey P. Dale, \u003ca href=\"/A133825/b133825.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Boris Putievskiy, \u003ca href=\"http://arxiv.org/abs/1212.2732\"\u003eTransformations [of] Integer Sequences And Pairing Functions\u003c/a\u003e arXiv:1212.2732 [math.CO]"
			],
			"formula": [
				"O.g.f.: (1+qx)/((1-x)(1-qx)^2(1-q^2x)) = 1 + x(1 + 3q + q^2) + x^2(1 + 3q + 6q^2 + 3q^3 + q^4) + ... .",
				"From _Boris Putievskiy_, Jan 13 2013: (Start)",
				"a(n) = A004737(n)*(A004737(n)+1)/2.",
				"a(n) = z*(z+1)/2, where z = floor(sqrt(n-1)) - |n- floor(sqrt(n-1))^2- floor(sqrt(n-1))-1| +1. (End)"
			],
			"example": [
				"Triangle starts",
				"1;",
				"1, 3, 1;",
				"1, 3, 6, 3, 1;",
				"1, 3, 6, 10, 6, 3, 1;",
				"From _Boris Putievskiy_, Jan 13 2013: (Start)",
				"The start of the sequence as table:",
				"1...1...1...1...1...1...",
				"1...3...3...3...3...3...",
				"1...3...6...6...6...6...",
				"1...3...6..10..10..10...",
				"1...3...6..10..15..15...",
				"1...3...6..10..15..21...",
				"1...3...6..10..15..21...",
				". . .",
				"The start of the sequence as triangle array read by rows:",
				"1,",
				"1, 3, 1,",
				"1, 3, 6, 3, 1,",
				"1, 3, 6, 10, 6, 3, 1,",
				"1, 3, 6, 10, 15, 10, 6, 3, 1,",
				"1, 3, 6, 10, 15, 21, 15, 10, 6, 3, 1,",
				"1, 3, 6, 10, 15, 21, 28, 21, 15, 10, 6, 3, 1,",
				". . .",
				"Row number k contains 2*k-1 numbers 1,3,...,k*(k-1)/2,k*(k+1)/2,k*(k-1)/2,...,3,1. (End)"
			],
			"mathematica": [
				"Module[{nn=10,ac},ac=Accumulate[Range[nn]];Table[Join[Take[ ac,n],Reverse[ Take[ac,n-1]]],{n,nn}]]//Flatten (* _Harvey P. Dale_, Apr 18 2019 *)"
			],
			"xref": [
				"Cf. A000330 (row sums), A004737, A124258, A133826, A106255."
			],
			"keyword": "easy,nonn,tabf",
			"offset": "0,3",
			"author": "_Peter Bala_, Sep 25 2007",
			"references": 3,
			"revision": 23,
			"time": "2019-04-18T10:31:07-04:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}