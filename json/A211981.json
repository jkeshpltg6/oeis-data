{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A211981",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 211981,
			"data": "1,2,3,4,5,8,10,16,21,32,42,64,75,85,113,128,151,170,227,256,341,512,682,1024,1365,2048,2730,4096,5461,7281,8192,10922,14563,16384,21845,32768,43690,65536,87381,131072,174762,262144,349525,466033,524288,699050,932067",
			"name": "Numbers n such that floor(2^A006666(n)/3^A006667(n)) = n.",
			"comment": [
				"A006666 and A006667 give the number of halving and tripling steps to reach 1 in 3x+1 problem.",
				"Properties of this sequence:",
				"A006667(a(n)) \u003c= 3, and if a(n) is even then a(n)/2 is in the sequence.",
				"The sequence A000079(n) (power of 2) is included in this sequence.",
				"{a(n)} = E1 union E2 where E1 = {A000079(n)} union {5, 10, 21, 85, 170, 227, 341, 682, 1365, 2730, 5461, ...} and E2 = {75, 113, 151, 7281, ...}. If an element k of E1 generates the Collatz sequence of iterates k -\u003e T_1(k) -\u003e T_2(k) -\u003e T_3(k) -\u003e ... then any T_i(k) is an element of  E1 of the form [2^a /3^b] where a = A006666(n), or A006666(n)-1, or ... and b = A006667(n), or A006667(n)-1, or ... But if k is an element of E2, there exists at least an element T_i(k) that is not in the sequence a(n). For example 75 -\u003e 226 -\u003e113 -\u003e 340 -\u003e ... and 226 is not in the sequence because, if [x] = [2^a /3^b] = [ x. x0 x1 x2 ...], the rational number 0.x0 x1 x2 ... \u003e 0.666666.... =\u003e [2^a /3^(b-1)] of the form [(3x+2).y0 y1 y2 ...], and this integer is different from T_(i+1)(k) = [(3x+1).y0 y1 y2 ...] = 3x+1.",
				"Example: T_2(75) = floor(2^10 /3^2) = 113 =\u003e floor(2^10/3^1) = 341 instead T_3(75) = 340."
			],
			"example": [
				"227 is in the sequence because A006666(227) = 11, A006667(227) = 2 =\u003e floor(2^11/3^2) = 227.",
				"The Collatz trajectory of 227 is 227 -\u003e 682 -\u003e 341 -\u003e 1024 -\u003e 512 -\u003e ... -\u003e 2 -\u003e1, and 227 is in the subset E1 implies the following Collatz iterates:",
				"227 = floor(2^11/3^2);",
				"682 = floor(2^11/3^1);",
				"341 = floor(2^10/3^1);",
				"1024 = floor(2^10/3^0);",
				"512 = floor(2^9)/3^0);",
				"256 = floor(2^8/3^0);",
				"128 = floor(2^7/3^0);",
				"...",
				"2 = floor(2^1/3^0);",
				"1 = floor(2^0/3^0);",
				"With the numbers of E1, we obtain another formulation of the Collatz problem."
			],
			"maple": [
				"A:= proc(n) if type(n, 'even') then n/2; else 3*n+1 ; end if; end proc:",
				"B:= proc(n) a := 0 ; x := n ; while x \u003e 1 do x := A(x) ; a := a+1 ; end do; a ; end proc:",
				"C:= proc(n) a := 0 ; x := n ; while x \u003e 1 do if type(x, 'even') then x := x/2 ; else x := 3*x+1 ; a := a+1 ; end if; end do; a ; end proc:",
				"D:= proc(n) C(n) ; end proc:",
				"A006666:= proc(n) B(n)- C(n) ; end:",
				"A006667:= proc(n) C(n)- D(n) ; end:",
				"G:= proc(n) floor(2^ A006666 (n)/3^ A006667 (n)) ; end:",
				"for i from 1 to 1000000 do: if G(i) =i then printf(`%d, `,i):else fi:od:"
			],
			"mathematica": [
				"Collatz[n_] := NestWhileList[If[EvenQ[#], #/2, 3 # + 1] \u0026, n, # \u003e 1 \u0026]; nn = 30; t = {}; n = 0; While[Length[t] \u003c nn, n++; c = Collatz[n]; ev = Length[Select[c, EvenQ]]; od = Length[c] - ev - 1; If[Floor[2^ev/3^od] == n, AppendTo[t, n]]]; t (* _T. D. Noe_, Feb 13 2013 *)"
			],
			"xref": [
				"Cf. A006666, A006667."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Michel Lagneau_, Feb 13 2013",
			"references": 3,
			"revision": 15,
			"time": "2017-06-13T03:50:11-04:00",
			"created": "2013-02-13T13:36:13-05:00"
		}
	]
}