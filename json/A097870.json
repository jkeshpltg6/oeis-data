{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A097870",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 97870,
			"data": "1,2,4,10,17,27,45,66,92,130,173,223,289,362,444,546,657,779,925,1082,1252,1450,1661,1887,2145,2418,2708,3034,3377,3739,4141,4562,5004,5490,5997,6527,7105,7706,8332,9010,9713,10443,11229,12042,12884,13786,14717,15679",
			"name": "Molien series for group of order 4608 acting on joint weight enumerators of a pair of binary doubly-even self-dual codes.",
			"comment": [
				"This is the Molien series for the group of order 128 discussed in A097869 extended by the extra generator diag{1,1,i,i}. This group was not considered in the reference cited.",
				"The first g.f. inserts zeros between each pair of terms; the second g.f. does not. - _Colin Barker_, Feb 12 2015"
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A097870/b097870.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"F. J. MacWilliams, C. L. Mallows and N. J. A. Sloane, \u003ca href=\"http://neilsloane.com/doc/gleason2.html\"\u003eGeneralizations of Gleason's theorem on weight enumerators of self-dual codes\u003c/a\u003e, IEEE Trans. Inform. Theory, 18 (1972), 794-805.",
				"\u003ca href=\"/index/Mo#Molien\"\u003eIndex entries for Molien series\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_08\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,-1,2,-4,2,-1,2,-1)."
			],
			"formula": [
				"G.f.: (1 + x^2 + 2*x^3 + x^4 + x^5 + x^6 + x^7)/(1 - 2*x + x^2 - 2*x^3 +",
				"   4*x^4 - 2*x^5 + x^6 - 2*x^7 + x^8).",
				"G.f.: (1+x)*(1-x+x^2)*(1+x^2+x^3+x^4) / ((1-x)^4*(1+x+x^2)^2). - _Colin Barker_, Feb 12 2015"
			],
			"maple": [
				"m:=50; S:=series((1+x^3)*(1+x^2+x^3+x^4)/((1-x)*(1-x^3))^2, x, m+1): seq(coeff(S, x, j), j=0..m); # _G. C. Greubel_, Feb 05 2020"
			],
			"mathematica": [
				"CoefficientList[Series[(1+x^3)*(1+x^2+x^3+x^4)/((1-x)*(1-x^3))^2, {x,0,50}], x] (* _G. C. Greubel_, Feb 05 2020 *)"
			],
			"program": [
				"(PARI) Vec((x+1)*(x^2-x+1)*(x^4+x^3+x^2+1)/((x-1)^4*(x^2+x+1)^2) + O(x^100)) \\\\ _Colin Barker_, Feb 12 2015",
				"(MAGMA) R\u003cx\u003e:=PowerSeriesRing(Integers(), 50); Coefficients(R!( (1+x^3)*(1+x^2+x^3+x^4)/((1-x)*(1-x^3))^2 )); // _G. C. Greubel_, Feb 05 2020",
				"(Sage)",
				"def A097870_list(prec):",
				"    P.\u003cx\u003e = PowerSeriesRing(ZZ, prec)",
				"    return P( (1+x^3)*(1+x^2+x^3+x^4)/((1-x)*(1-x^3))^2 ).list()",
				"A097870_list(50) # _G. C. Greubel_, Feb 05 2020"
			],
			"xref": [
				"Cf. A097869."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Sep 02 2004",
			"references": 2,
			"revision": 19,
			"time": "2020-02-05T14:04:29-05:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}