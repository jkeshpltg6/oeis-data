{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A122458",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 122458,
			"data": "0,2,1,4,1,3,1,4,1,2,1,3,1,37,1,35,1,2,1,5,1,3,1,34,1,2,1,3,1,4,1,34,1,2,1,32,1,3,1,5,1,2,1,3,1,28,1,5,1,2,1,26,1,3,1,19,1,2,1,3,1,5,1,9,1,2,1,4,1,3,1,4,1,2,1,3,1,25,1,13,1,2,1,18,1,3,1,5,1,2,1,3,1,4,1,8,1,2,1,5",
			"name": "\"Dropping time\" of the reduced Collatz iteration starting with 2n+1.",
			"comment": [
				"We count only the 3x+1 steps of the usual Collatz iteration. We stop counting when the iteration produces a number less than the initial 2n+1. For a fixed dropping time k, let N(k)=A100982(k) and P(k)=2^(A020914(k)-1). There are exactly N(k) odd numbers less than P(k) with dropping time k. Moreover, the sequence is periodic: if d is one of the N(k) odd numbers, then k=a(d)=a(d+i*P(k)) for all i\u003e=0. This periodicity makes it easy to compute the average dropping time of the reduced Collatz iteration: Sum_{k\u003e0} k*N(k)/P(k) = 3.492651852186... (A122791)."
			],
			"reference": [
				"Victor Klee and Stan Wagon, Old and New Unsolved Problems in Plane Geometry and Number Theory, Mathematical Association of America (1991) pp. 225-229, 308-309. [called on p. 225 stopping time for 2n+1 and the function C(2*n+1) = A075677(n+1), n \u003e= 0. - _Wolfdieter Lang_, Feb 20 2019]"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A122458/b122458.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) is the least k for which fr^[k](n) \u003c 2*n + 1, for n \u003e= 1 and k \u003e= 1, where fr(n) = A075677(n+1) = A000265(3*n+2). No k satisfies this for n = 0: a(0) := 0 by convention. The dropping time a(n) is finite, for n \u003e= 1, if the Collatz conjecture is true. - _Wolfdieter Lang_, Feb 20 2019",
				"a(1+i*8) = 2, for i\u003e=0, because A100982(2) = 1 is odd, and A020914(2) = 4 gives P(2) = 2^(4-1) = 8. - _Ruud H.G. van Tol_, Dec 19 2021"
			],
			"example": [
				"a(3)=4 because, starting with 7, the iteration produces 11,17,13,5 and the last term is less than 7.",
				"n = 13: the fr trajectory for 2*13+1 = 27 is 41, 31, 47, 71, 107, 161, 121, 91, 137, 103, 155, 233, 175, 263, 395, 593, 445, 167, 251, 377, 283, 425, 319, 479, 719, 1079, 1619, 2429, 911, 1367, 2051, 3077, 577, 433, 325, 61, 23, 35, 53, 5, 1 with 41 terms (without 27), hence fr^[37] = 23 \u003c 27  and  a(13) = 37. - _Wolfdieter Lang_, Feb 20 2019"
			],
			"mathematica": [
				"nextOddK[n_]:=Module[{m=3n+1}, While[EvenQ[m], m=m/2]; m]; dt[n_]:=Module[{m=n, cnt=0}, If[n\u003e1, While[m=nextOddK[m]; cnt++; m\u003en]]; cnt]; Table[dt[n],{n,1,301,2}]"
			],
			"xref": [
				"Cf. A000265, A060445, A075677 (one step of the reduced Collatz iteration), A075680.",
				"Cf. A087113 (indices of 1's), A017077 (indices of 2's), A122791 (limit mean)."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_T. D. Noe_, Sep 08 2006",
			"references": 13,
			"revision": 35,
			"time": "2021-12-22T02:19:44-05:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}