{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A176731",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 176731,
			"data": "1,12,504,45360,7076160,1698278400,580811212800,268334780313600,161000868188160000,121716656350248960000,113196490405731532800000,127006462235230779801600000,169172607697327398695731200000,263909268007830741965340672000000,476620138022142319989405253632000000",
			"name": "Denominators of coefficients of a series, called g, related to Airy functions.",
			"comment": [
				"The numerators are always 1.",
				"f(z) := Sum_{n\u003e=0} (1/b(n)) * z^(3*n) with b(n) := A176730(n) and g(z) := Sum_{n\u003e=0} (1/a(n)) * z^(3*n+1) build the two independent Airy functions Ai(z) = c(1)*f(z) - c(2)*g(z) and Bi(z) = sqrt(3) * (c(1)*f(z) + c(2)*g(z)) with c(1) := 1/(3^(2/3) * GAMMA(2/3)), approximately 0.35502805388781723926, and c(2) := 1/(3^(1/3) * GAMMA(1/3)), approximately 0.25881940379280679840.",
				"If y := Sum_{n \u003e= 0} x^(3*n+1)/a(n), then y'' = x*y. - _Michael Somos_, Jul 12 2019"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A176731/b176731.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 , 10.4.2 - 5. [alternative scanned copy].",
				"Wolfdieter Lang, \u003ca href=\"/A176730/a176730.txt\"\u003eThe first 20 terms of the f(z) and g(z) functions\u003c/a\u003e.",
				"NIST's Digital Library of Mathematical Functions, \u003ca href=\"http://dlmf.nist.gov/9.4\"\u003eAiry and Related Functions (Maclaurin Series)\u003c/a\u003e by Frank W. J. Olver."
			],
			"formula": [
				"a(n) = denominator((3^n) * risefac(2/3, n)/(3*n + 1)!) with the rising factorials risefac(k, n) := Product_{j=0..(n-1)} (k+j) and risefac(k, 0) = 1.",
				"From _Peter Bala_, Dec 17 2021: (Start)",
				"a(n) = 3*n*(3*n + 1)*a(n-1) with a(0) = 1.",
				"a(n) = (3*n + 2)!/(n!*3^n)*Sum_{k = 0..n} (-1)^k*binomial(n,k)/(3*k + 2).",
				"a(n) = (1/2)*(3*n + 2)!/(n!*3^n)*hypergeom([-n, 2/3], [5/3], 1).",
				"a(n) = (2*Pi*sqrt(3))/9 *(1/3^(n+1))*Gamma(3*n+4)/( (n+1)*Gamma(1/3)* Gamma(n + 5/3) ). (End)",
				"a(n) = (9^n*n!*(n + 1/3)!)/(1/3)!. - _Peter Luschny_, Dec 20 2021"
			],
			"example": [
				"Rational g-coefficients: [1, 1/12, 1/504, 1/45360, 1/7076160, 1/1698278400, 1/580811212800, 1/268334780313600, ...]."
			],
			"maple": [
				"a := proc (n) option remember; if n = 0 then 1 else 3*n*(3*n+1)*a(n-1) end if; end proc: seq(a(n), n = 0..20); # - _Peter Bala_, Dec 17 2021"
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 0, 0, -1 / (3^(1/3) Gamma[ 1/3] SeriesCoefficient[ AiryAi[ x], {x, 0, 3 n + 1}])]; (* _Michael Somos_, Oct 14 2011 *)",
				"a[ n_] := If[ n \u003c 0, 0, (3 n + 1)! / Product[ k, {k, 2, 3 n + 1, 3}]]; (* _Michael Somos_, Oct 14 2011 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, 0, (3*n + 1)! / prod( k=0, n-1, 3*k + 2))}; /* _Michael Somos_, Oct 14 2011 */"
			],
			"xref": [
				"Cf. A176730."
			],
			"keyword": "nonn,frac,easy",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Jul 14 2010",
			"references": 3,
			"revision": 45,
			"time": "2021-12-20T09:24:12-05:00",
			"created": "2010-07-31T03:00:00-04:00"
		}
	]
}