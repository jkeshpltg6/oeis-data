{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A326190",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 326190,
			"data": "0,1,1,2,1,2,1,3,2,2,1,3,1,2,2,4,1,3,1,3,2,2,1,4,2,2,2,3,1,3,1,5,2,2,2,4,1,2,2,4,1,3,1,3,3,2,1,5,2,3,2,3,1,3,2,4,2,2,1,4,1,2,2,6,2,3,1,3,2,3,1,5,1,2,2,3,2,3,1,5,3,2,1,4,2,2,2,4,1,4,2,3,2,2,2,6,1,3,2,4,1,3,1,4,3",
			"name": "Length of the shortest path to 1 when starting from x=n and on each iteration step one may always choose either transition x -\u003e A032742(x) or x -\u003e A302042(x).",
			"link": [
				"Antti Karttunen, \u003ca href=\"/A326190/b326190.txt\"\u003eTable of n, a(n) for n = 1..65537\u003c/a\u003e"
			],
			"formula": [
				"a(1) = 0; for n \u003e 1, a(n) = 1 + min(a(A032742(n)), a(A302042(n))).",
				"a(n) \u003c= min(A001222(n),A253557(n)) \u003c= max(A001222(n),A253557(n)) \u003c= A326191(n) \u003c= A326189(n)."
			],
			"example": [
				"The directed acyclic graph whose unique root is 153 (illustrated below), spans the following seven numbers [1, 5, 17, 25, 51, 75, 153], as A032742(153) = 51, A302042(153) = 75, A032742(51) = 17, A302042(51) = 25, A032742(75) = 25, A302042(75) = 15, A032742(25) = A302042(25) = 5, and A032742(17) = A302042(17) = A032742(5) = A302042(5) = 1. The length of shortest path(s) from 153 to 1 is 3 (there are actually two shortest paths: 153 -\u003e 51 -\u003e 17 -\u003e 1 and 153 -\u003e 75 -\u003e 17 -\u003e 1), thus a(153) = 3.",
				".",
				"        153",
				"       /  \\",
				"      /    \\",
				"     51    75",
				"     / \\  /  \\",
				"    /   17    \\",
				"    \\    |    /",
				"     \\   1   /",
				"      \\     /",
				"       \\   /",
				"        25",
				"         |",
				"         5",
				"         |",
				"         1"
			],
			"program": [
				"(PARI)",
				"up_to = 65537;",
				"ordinal_transform(invec) = { my(om = Map(), outvec = vector(length(invec)), pt); for(i=1, length(invec), if(mapisdefined(om,invec[i]), pt = mapget(om, invec[i]), pt = 0); outvec[i] = (1+pt); mapput(om,invec[i],(1+pt))); outvec; };",
				"rgs_transform(invec) = { my(om = Map(), outvec = vector(length(invec)), u=1); for(i=1, length(invec), if(mapisdefined(om,invec[i]), my(pp = mapget(om, invec[i])); outvec[i] = outvec[pp] , mapput(om,invec[i],i); outvec[i] = u; u++ )); outvec; };",
				"A020639(n) = if(n\u003e1, if(n\u003en=factor(n, 0)[1, 1], n, factor(n)[1, 1]), 1); \\\\ From A020639",
				"A032742(n) = (n/A020639(n));",
				"v078898 = ordinal_transform(vector(up_to,n,A020639(n)));",
				"A078898(n) = v078898[n];",
				"A302042(n) = if((1==n)||isprime(n),1,my(c = A078898(n), p = prime(-1+primepi(A020639(n))+primepi(A020639(c))), d = A078898(c), k=0); while(d, k++; if((1==k)||(A020639(k)\u003e=p),d -= 1)); (k*p));",
				"A326190(n) = if(1==n,0,1+min(A326190(A032742(n)), A326190(A302042(n))));",
				"\\\\ Somewhat faster version:",
				"memo302042 = Map();",
				"A302042(n) = if((1==n)||isprime(n),1,my(v); if(mapisdefined(memo302042, n, \u0026v), v, my(c = A078898(n), p = prime(-1+primepi(A020639(n))+primepi(A020639(c))), d = A078898(c), k=0); while(d, k++; if((1==k)||(A020639(k)\u003e=p),d -= 1)); v=(k*p); mapput(memo302042,n,v); (v)));",
				"A326190list(up_to) = { my(v=vector(up_to)); v[1] = 0; for(n=2,up_to, v[n] = 1+min(v[A032742(n)], v[A302042(n)])); (v); };",
				"v326190 = A326190list(up_to);",
				"A326190(n) = v326190[n];"
			],
			"xref": [
				"Cf. A032742, A253557, A302042, A323888, A326075, A326139, A326089, A326191."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Antti Karttunen_, Aug 23 2019",
			"references": 5,
			"revision": 14,
			"time": "2019-08-24T11:57:26-04:00",
			"created": "2019-08-24T11:57:26-04:00"
		}
	]
}