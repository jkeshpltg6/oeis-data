{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A301287",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 301287,
			"data": "1,3,6,7,8,15,18,17,20,25,28,29,30,35,40,39,40,47,50,49,52,57,60,61,62,67,72,71,72,79,82,81,84,89,92,93,94,99,104,103,104,111,114,113,116,121,124,125,126,131,136,135,136,143,146,145,148,153,156,157,158",
			"name": "Coordination sequence for node of type 3.12.12 in \"cph\" 2-D tiling (or net).",
			"reference": [
				"Branko Grünbaum and G. C. Shephard, Tilings and Patterns. W. H. Freeman, New York, 1987. See Table 2.2.1, page 66, bottom row, first tiling."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A301287/b301287.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Brian Galebach, \u003ca href=\"http://probabilitysports.com/tilings.html\"\u003eCollection of n-Uniform Tilings\u003c/a\u003e. See Number 2 from the list of  20 2-uniform tilings.",
				"Brian Galebach, \u003ca href=\"/A301287/a301287.pdf\"\u003eEnlarged illustration of tiling, suitable for coloring\u003c/a\u003e (taken from the web site in the previous link)",
				"Brian Galebach, \u003ca href=\"/A250120/a250120.html\"\u003ek-uniform tilings (k \u003c= 6) and their A-numbers\u003c/a\u003e",
				"Chaim Goodman-Strauss and N. J. A. Sloane, \u003ca href=\"https://doi.org/10.1107/S2053273318014481\"\u003eA Coloring Book Approach to Finding Coordination Sequences\u003c/a\u003e, Acta Cryst. A75 (2019), 121-134, also \u003ca href=\"http://NeilSloane.com/doc/Cairo_final.pdf\"\u003eon NJAS's home page\u003c/a\u003e. Also \u003ca href=\"http://arxiv.org/abs/1803.08530\"\u003earXiv:1803.08530\u003c/a\u003e.",
				"Reticular Chemistry Structure Resource (RCSR), \u003ca href=\"http://rcsr.net/layers/cph\"\u003eThe cph tiling (or net)\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A301287/a301287.png\"\u003eIllustration of initial terms\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A301287/a301287.gp.txt\"\u003ePARI program for A301287\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/A301287/a301287_1.pdf\"\u003eTrunks and branches for determining coordination sequence, central view. Blue = trunks, red = branches, green = twigs.\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/A301287/a301287_2.pdf\"\u003eTrunks and branches, a different scan, truncated on right but otherwise shows quadrants I and IV in detail\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/A301287/a301287_2.png\"\u003eTrunks and branches in first quadrant, in full.\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/A301287/a301287_3.png\"\u003eTrunks and branches in fourth quadrant, in full.\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/A301287/a301287_4.png\"\u003eDetails of proof (page 1)\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/A301287/a301287_5.png\"\u003eDetails of proof (page 2)\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,-1,2,-1,1,-1)."
			],
			"formula": [
				"Conjecture: G.f. = -(2*x^8-2*x^7-x^6-4*x^5-2*x^4-2*x^3-4*x^2-2*x-1) / ((x^2+1)*(x^2+x+1)*(x-1)^2). _N. J. A. Sloane_, Mar 28 2018 (This is now a theorem. - _N. J. A. Sloane_, Apr 05 2018)",
				"Equivalent conjecture: 3*a(n) = 8*n+2*A057078(n+1)+3*A228826(n+2). - _R. J. Mathar_, Mar 31 2018 (This is now a theorem. - _N. J. A. Sloane_, Apr 05 2018)",
				"Theorem: G.f. = (1+2*x+4*x^2+2*x^3+2*x^4+4*x^5+1*x^6+2*x^7-2*x^8) / ((1-x)*(1+x^2)*(1-x^3)).",
				"Proof. This follows by applying the coloring book method described in the Goodman-Strauss \u0026 Sloane article. The trunks and branches structure is shown in the links, and the details of the proof (by calculating the generating function) are on the next two scanned pages. - _N. J. A. Sloane_, Apr 05 2018"
			],
			"mathematica": [
				"Join[{1, 3, 6}, LinearRecurrence[{1, -1, 2, -1, 1, -1}, {7, 8, 15, 18, 17, 20}, 100]] (* _Jean-François Alcover_, Aug 05 2018 *)"
			],
			"program": [
				"(PARI) See Links section."
			],
			"xref": [
				"Cf. A301289.",
				"Coordination sequences for the 20 2-uniform tilings in the order in which they appear in the Galebach catalog, together with their names in the RCSR database (two sequences per tiling): #1 krt A265035, A265036; #2 cph A301287, A301289; #3 krm A301291, A301293; #4 krl A301298, A298024; #5 krq A301299, A301301; #6 krs A301674, A301676; #7 krr A301670, A301672; #8 krk A301291, A301293; #9 krn A301678, A301680; #10 krg A301682, A301684; #11 bew A008574, A296910; #12 krh A301686, A301688; #13 krf A301690, A301692; #14 krd A301694, A219529; #15 krc A301708, A301710; #16 usm A301712, A301714; #17 krj A219529, A301697; #18 kre A301716, A301718; #19 krb A301720, A301722; #20 kra A301724, A301726."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Mar 23 2018",
			"ext": [
				"More terms from _Rémy Sigrist_, Mar 27 2018"
			],
			"references": 38,
			"revision": 69,
			"time": "2020-09-17T20:09:40-04:00",
			"created": "2018-03-23T01:36:42-04:00"
		}
	]
}