{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A067538",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 67538,
			"data": "1,2,2,4,2,8,2,11,9,14,2,46,2,24,51,66,2,126,2,202,144,69,2,632,194,116,381,756,2,1707,2,1417,956,316,2043,5295,2,511,2293,9151,2,10278,2,8409,14671,1280,2,36901,8035,21524,11614,25639,2,53138,39810,85004",
			"name": "Number of partitions of n in which the number of parts divides n.",
			"comment": [
				"Also sum of p(n,d) over the divisors d of n, where p(n,m) is the count of partitions of n in exactly m parts. - _Wouter Meeussen_, Jun 07 2009",
				"From _Gus Wiseman_, Sep 24 2019: (Start)",
				"Also the number of integer partitions of n whose maximum part divides n. The Heinz numbers of these partitions are given by A326836. For example, the a(1) = 1 through a(8) = 11 partitions are:",
				"  (1)  (2)   (3)    (4)     (5)      (6)       (7)        (8)",
				"       (11)  (111)  (22)    (11111)  (33)      (1111111)  (44)",
				"                    (211)            (222)                (422)",
				"                    (1111)           (321)                (431)",
				"                                     (2211)               (2222)",
				"                                     (3111)               (4211)",
				"                                     (21111)              (22211)",
				"                                     (111111)             (41111)",
				"                                                          (221111)",
				"                                                          (2111111)",
				"                                                          (11111111)",
				"(End)"
			],
			"link": [
				"David A. Corneth, \u003ca href=\"/A067538/b067538.txt\"\u003eTable of n, a(n) for n = 1..5000\u003c/a\u003e (first 500 terms from Wouter Meeussen, n = 501..1000 from Alois P. Heinz)",
				"Eric W. Weisstein, \u003ca href=\"http://mathworld.wolfram.com/PartitionFunctionP.html\"\u003ePartition Function P\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://www.wikipedia.org/wiki/integer_partition\"\u003eInteger Partition\u003c/a\u003e"
			],
			"formula": [
				"a(p) = 2 for all primes p."
			],
			"example": [
				"a(3)=2 because 3 is a prime; a(4)=4 because the five partitions of 4 are {4}, {3, 1}, {2, 2}, {2, 1, 1}, {1, 1, 1, 1}, and the number of parts in each of them divides 4 except for {2, 1, 1}.",
				"From _Gus Wiseman_, Sep 24 2019: (Start)",
				"The a(1) = 1 through a(8) = 11 partitions whose length divides their sum are the following. The Heinz numbers of these partitions are given by A316413.",
				"  (1)  (2)   (3)    (4)     (5)      (6)       (7)        (8)",
				"       (11)  (111)  (22)    (11111)  (33)      (1111111)  (44)",
				"                    (31)             (42)                 (53)",
				"                    (1111)           (51)                 (62)",
				"                                     (222)                (71)",
				"                                     (321)                (2222)",
				"                                     (411)                (3221)",
				"                                     (111111)             (3311)",
				"                                                          (4211)",
				"                                                          (5111)",
				"                                                          (11111111)",
				"(End)"
			],
			"mathematica": [
				"Do[p = IntegerPartitions[n]; l = Length[p]; c = 0; k = 1; While[k \u003c l + 1, If[ IntegerQ[ n/Length[ p[[k]] ]], c++ ]; k++ ]; Print[c], {n, 1, 57}, All]",
				"p[n_,k_]:=p[n,k]=p[n-1,k-1]+p[n-k,k];p[n_,k_]:=0/;k\u003en;p[n_,n_]:=1;p[n_,0]:=0",
				"Table[Plus @@ (p[n,# ]\u0026/ @ Divisors[n]),{n,36}] (* _Wouter Meeussen_, Jun 07 2009 *)",
				"Table[Count[IntegerPartitions[n], q_ /; IntegerQ[Mean[q]]], {n, 50}]  (*_Clark Kimberling_, Apr 23 2019 *)"
			],
			"program": [
				"(PARI) a(n) = {my(nb = 0); forpart(p=n, if ((vecsum(Vec(p)) % #p) == 0, nb++);); nb;} \\\\ _Michel Marcus_, Jul 03 2018"
			],
			"xref": [
				"Cf. A000005, A000041, A143773, A298422, A298423, A298426.",
				"The strict case is A102627.",
				"Partitions with integer geometric mean are A067539.",
				"Cf. A018818, A102627, A316413, A326622, A326836, A326843, A326850."
			],
			"keyword": "easy,nonn",
			"offset": "1,2",
			"author": "_Naohiro Nomoto_, Jan 27 2002",
			"ext": [
				"Extended by _Robert G. Wilson v_, Oct 16 2002"
			],
			"references": 92,
			"revision": 47,
			"time": "2019-09-26T16:14:55-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}