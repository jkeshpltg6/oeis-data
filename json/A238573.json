{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A238573",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 238573,
			"data": "0,1,1,0,3,0,4,1,2,3,1,3,4,4,4,4,1,3,6,5,3,3,4,6,3,8,5,6,3,4,2,10,6,5,7,8,6,8,7,5,7,5,11,7,7,8,8,11,6,5,7,11,11,7,4,9,7,3,5,7,7,11,8,13,9,8,7,7,12,10,8,11,8,15,8,9,9,15,13,4",
			"name": "a(n) = |{0 \u003c k \u003c= n: prime(k*n) + 2 is prime}|.",
			"comment": [
				"Conjecture: (i) a(n) \u003e 0 for all n \u003e 6, and a(n) = 1 only for n = 2, 3, 8, 11, 17. Moreover, for any n \u003e 0 there exists a positive integer k \u003c 3*sqrt(n) + 6 such that prime(k*n) + 2 is prime.",
				"(ii) For any integer n \u003e 6, prime(k^2*n) + 2 is prime for some k = 1, ..., n.",
				"(iii) If n \u003e 5, then prime(k^2*(n-k)) + 2 is prime for some 0 \u003c k \u003c n.",
				"Clearly, each of the three parts implies the twin prime conjecture.",
				"We have verified part (i) of the conjecture for n up to 2*10^6."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A238573/b238573.txt\"\u003eTable of n, a(n) for n = 1..5000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1402.6641\"\u003eProblems on combinatorial properties of primes\u003c/a\u003e, arXiv:1402.6641, 2014."
			],
			"example": [
				"a(2) = 1 since prime(1*2) + 2 = 3 + 2 = 5 is prime.",
				"a(3) = 1 since prime(1*3) + 2 = 5 + 2 = 7 is prime.",
				"a(8) = 1 since prime(8*8) + 2 = 311 + 2 = 313 is prime.",
				"a(11) = 1 since prime(3*11) + 2 = 137 + 2 = 139 is prime.",
				"a(17) = 1 since prime(1*17) + 2 = 59 + 2 = 61 is prime."
			],
			"mathematica": [
				"p[k_,n_]:=PrimeQ[Prime[k*n]+2]",
				"a[n_]:=Sum[If[p[k,n],1,0],{k,1,n}]",
				"Table[a[n],{n,1,80}]"
			],
			"xref": [
				"Cf. A000040, A001359, A006512, A218829, A236531, A237578, A238576."
			],
			"keyword": "nonn",
			"offset": "1,5",
			"author": "_Zhi-Wei Sun_, Mar 01 2014",
			"references": 7,
			"revision": 13,
			"time": "2014-03-01T09:52:43-05:00",
			"created": "2014-03-01T09:52:43-05:00"
		}
	]
}