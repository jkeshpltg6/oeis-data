{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A212493",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 212493,
			"data": "0,5,3,3,3,17,13,23,19,19,37,31,31,47,43,59,53,67,61,0,79,73,73,73,73,0,107,103,127,131,109,113,113,151,113,139,163,157,157,179,173,0,223,197,193,233,193,191,191,193,199,0,0,257,251,251,0,277,271,271",
			"name": "Let p_n=prime(n), n\u003e=1. Then a(n) is the least prime p which differs from p_n, for which the intervals (p/2,p_n/2), (p,p_n], if p\u003cp_n, or the intervals (p_n/2,p/2), (p_n,p], if p\u003ep_n, contain the same number of primes, and a(n)=0, if no such prime p exists.",
			"comment": [
				"a(n)=0 if and only if p_n is a peculiar prime, i.e., simultaneously Ramanujan (A104272) and Labos (A080359) prime (see sequence A164554).",
				"a(n)\u003ep_n if and only if p_n is Labos prime but not Ramanujan prime."
			],
			"link": [
				"V. Shevelev, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL15/Shevelev/shevelev19.html\"\u003eRamanujan and Labos primes, their generalizations, and classifications of primes\u003c/a\u003e, J. Integer Seq. 15 (2012) Article 12.5.4.",
				"J. Sondow, J. W. Nicholson, and T. D. Noe, \u003ca href=\"http://arxiv.org/abs/1105.2249\"\u003e Ramanujan Primes: Bounds, Runs, Twins, and Gaps\u003c/a\u003e, arXiv:1105.2249 [math.NT], 2011; J. Integer Seq. 14 (2011) Article 11.6.2."
			],
			"formula": [
				"If p_n is not a Labos prime, then a(n) = A080359(n-pi(p_n/2))."
			],
			"example": [
				"Let n=5, p_5=11; p=2 is not suitable, since in (1,5.5) we have 3 primes, while in (2,11] there are 4 primes. Consider p=3. Now in intervals (1.5,5.5) and (3,11] we have the same number (3) of primes. Therefore, a(5)=3. The same value we can obtain by the formula. Since 11 is not a labos prime, then a(5)=A080359(5-pi(5.5))=A080359(2)=3."
			],
			"mathematica": [
				"terms = 60; nn = Prime[terms];",
				"R = Table[0, {nn}]; s = 0; Do[If[PrimeQ[k], s++]; If[PrimeQ[k/2], s--]; If[s \u003c nn, R[[s + 1]] = k], {k, Prime[3 nn]}];",
				"A104272 = R + 1;",
				"t = Table[0, {nn + 1}]; s = 0; Do[If[PrimeQ[k], s++]; If[PrimeQ[k/2], s--]; If[s \u003c= nn \u0026\u0026 t[[s + 1]] == 0, t[[s + 1]] = k], {k, Prime[3 nn]}];",
				"A080359 = Rest[t];",
				"a[n_] := Module[{}, pn = Prime[n]; If[MemberQ[A104272, pn] \u0026\u0026 MemberQ[ A080359, pn], Return[0]]; For[p = 2, True, p = NextPrime[p], Which[p\u003cpn, If[PrimePi[pn/2] - PrimePi[p/2] == PrimePi[pn] - PrimePi[p], Return[p]], p\u003epn, If[PrimePi[p/2] - PrimePi[pn/2] == PrimePi[p] - PrimePi[pn], Return[p]]]]];",
				"Array[a, terms] (* _Jean-François Alcover_, Dec 04 2018, after _T. D. Noe_ in A104272 *)"
			],
			"xref": [
				"Cf. A104272, A080359, A164554."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Vladimir Shevelev_ and _Peter J. C. Moses_, May 18 2012",
			"references": 5,
			"revision": 29,
			"time": "2018-12-04T04:09:52-05:00",
			"created": "2012-05-22T12:31:40-04:00"
		}
	]
}