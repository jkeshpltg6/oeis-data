{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A074903",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 74903,
			"data": "1,3,5,1,1,3,1,5,7,4,4,9,1,6,5,9,0,0,1,7,9,3,8,6,8,0,0,5,2,5,6,5,2,1,0,6,8,3,6,0,6,5,1,5,0,8,7,4,2,7,0,1,6,8,7,3,4,5,1,4,7,2,1,1,0,1,3,7,4,2,2,7,7,1,1,9,5,5,0,1,7,1,2,8,6,9,1,3,0,7,5,1,5,9,7,8,0,2,3,9",
			"name": "Decimal expansion of the mean number of iterations in comparing two numbers via their continued fractions.",
			"comment": [
				"Another description: Decimal expansion of the mean number of comparisons (moment sum of index 2) in the basic continued fraction sign algorithm (\"BCF-sign\").",
				"Still another description: Decimal expansion of expected number of iterations of Gaussian reduction of a 2-dimensional lattice."
			],
			"reference": [
				"Steven R. Finch, Mathematical Constants, Cambridge University Press, 2003, page 161.",
				"Philippe Flajolet and Brigitte Vallée, Continued fraction algorithms and constants, in \"Constructive, Experimental, and Nonlinear Analysis\", Michel Théra Editor, CMS Conference Proceedings, Canadian Mathematical Society Volume 27 (2000), p. 67."
			],
			"link": [
				"H. Daude, P. Flajolet and B. Vallee, \u003ca href=\"http://algo.inria.fr/flajolet/Publications/RR2798.ps.gz\"\u003eAn average-case analysis of the Gaussian algorithm for lattice reduction\u003c/a\u003e, INRIA, 1996. [\u003ca href=\"https://www.semanticscholar.org/paper/An-Average-Case-Analysis-of-the-Gaussian-Algorithm-Daud%C3%A9-Flajolet/5783d4bfc398819b106c216bfea14347dd58550b\"\u003ealternative link\u003c/a\u003e]",
				"Philippe Flajolet, \u003ca href=\"http://algo.inria.fr/seminars/sem99-00/flajolet.pdf\"\u003eContinued Fractions, Comparison Algorithms and Fine Structure Constants.\u003c/a\u003e",
				"Eric Weisstein's MathWorld, \u003ca href=\"http://mathworld.wolfram.com/Polylogarithm.html\"\u003e Polylogarithm\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ValleeConstant.html\"\u003eVallée Constant\u003c/a\u003e"
			],
			"formula": [
				"(-60/Pi^4)*(24*Li_4(1/2) - Pi^2*log(2)^2 + 21*zeta(3)*log(2) + log(2)^4) + 17, with Li_4 the tetralogarithm function. - _Jean-François Alcover_, Apr 23 2015"
			],
			"example": [
				"1.351131574491659001793868005256521068360651508742701687345147211...",
				"(Only the first 31 digits are the same as those given by Flajolet \u0026 Vallée. - _Jean-François Alcover_, Apr 23 2015)"
			],
			"mathematica": [
				"17 - 60/Pi^4 (24*PolyLog[4, 1/2] - Pi^2*Log[2]^2 + 21*Zeta[3]*Log[2] + Log[2]^4) // RealDigits[#, 10, 100]\u0026 // First (* _Jean-François Alcover_, Mar 19 2013, after _Steven Finch_ *)"
			],
			"program": [
				"(PARI) 17 - 60*(24*polylog(4, 1/2) - Pi^2*log(2)^2 + 21*zeta(3)*log(2) + log(2)^4)/Pi^4 \\\\ _Charles R Greathouse IV_, Aug 27 2014"
			],
			"xref": [
				"Cf. A099218."
			],
			"keyword": "nonn,cons",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, Sep 15 2002",
			"ext": [
				"Corrected and extended by _Jean-François Alcover_, Mar 19 2013",
				"Entry revised by _N. J. A. Sloane_, Apr 24 2015 to include information from two other entries (submitted respectively by _Eric W. Weisstein_, Aug 05 2008 and _Jean-François Alcover_, Apr 23 2015) that formerly described this same constant."
			],
			"references": 3,
			"revision": 39,
			"time": "2020-08-23T06:33:06-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}