{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A062745",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 62745,
			"data": "1,1,1,1,1,2,3,3,3,1,3,6,9,12,12,12,1,4,10,19,31,43,55,55,55,1,5,15,34,65,108,163,218,273,273,273,1,6,21,55,120,228,391,609,882,1155,1428,1428,1428,1,7,28,83,203,431,822,1431,2313,3468,4896,6324,7752,7752",
			"name": "Generalized Catalan array FS(3; n,r).",
			"comment": [
				"In the Frey-Sellers reference this array appears in Tab. 2, p. 143 and is called {n over r}_{m-1}, with m=3.",
				"The step width sequence of this staircase array is [1,2,2,2,....], i.e., the degree of the row polynomials is [0,2,4,6,...] = A005843.",
				"The columns r=0..5 give A000012 (powers of 1), A000027 (natural), A000217 (triangular), A062748, A005718, A062749.",
				"Number of lattice paths from (0,0) to (r,n) using steps h=(1,0), v=(0,1) and staying on or above the line y = x/2. Example: a(3,2)=6 because from (0,0) to (2,3) we have the following valid paths: vvvhh, vvhvh, vvhhv, vhvvh, vhvhvh and vhvvh (see the Niederhausen reference). - _Emeric Deutsch_, Jun 24 2005"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A062745/b062745.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"D. D. Frey and J. A. Sellers, \u003ca href=\"http://www.fq.math.ca/Scanned/39-2/frey.pdf\"\u003eGeneralizing Bailey's generalization of the Catalan numbers\u003c/a\u003e, The Fibonacci Quarterly, 39 (2001) 142-148.",
				"W. Lang, \u003ca href=\"/A062745/a062745.txt\"\u003eFirst 10 rows.\u003c/a\u003e",
				"D. Merlini, R. Sprugnoli and M. C. Verri, \u003ca href=\"http://dx.doi.org/10.1006/jcta.2002.3273\"\u003eThe tennis ball problem\u003c/a\u003e, J. Combin. Theory, A 99 (2002), 307-344 (Table 2).",
				"Heinrich Niederhausen, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v9i1r33\"\u003eCatalan Traffic at the Beach\u003c/a\u003e, Electronic Journal of Combinatorics, Volume 9 (2002), #R33."
			],
			"formula": [
				"a(0,0)=1, a(n,-1)=0, n \u003e= 1; a(n,r) = a(n, r-1) + a(n-1, r) if r \u003c= 2n, 0 otherwise.",
				"G.f. for column r = 2*k+j, k \u003e= 0, j=1, 2: (x^(k+1))*N(3; k, x)/ (1-x)^(2*k+1+j), with row polynomials N(3; k, x) of array A062746; for column r=0: 1/(1-x).",
				"a(n,r) = binomial(n+r, r) - (-1)^(r-1)*Sum_{i=0..floor((r-1)/2)} binomial(3i, i)*binomial(i-n-1, r-1-2i)/(2i+1), 0 \u003c= r \u003c= 2n (see the Niederhausen reference, eq. (17)). - _Emeric Deutsch_, Jun 24 2005"
			],
			"example": [
				"{1}; {1,1,1}; {1,2,3,3,3}; {1,3,6,9,12,12,12}; ...; N(3; 1,x)=3-3*x+x^2."
			],
			"maple": [
				"a:=proc(n,r) if r\u003c=2*n then binomial(n+r,r)-(-1)^(r-1)*sum(binomial(3*i,i)*binomial(i-n-1,r-1-2*i)/(2*i+1),i=0..floor((r-1)/2)) else 0 fi end: for n from 0 to 8 do seq(a(n,r),r=0..2*n) od; # yields sequence in triangular form # _Emeric Deutsch_, Jun 24 2005"
			],
			"mathematica": [
				"a[0, 0] = 1; a[_, -1] = 0; a[n_, r_] /; r \u003e 2*n = 0; a[n_, r_] := a[n, r] = a[n, r-1] + a[n-1, r]; Table[a[n, r], {n, 0, 7}, {r, 0, 2*n}] // Flatten (* _Jean-François Alcover_, Jun 21 2013 *)"
			],
			"xref": [
				"Cf. A009766."
			],
			"keyword": "nonn,easy,tabf",
			"offset": "0,6",
			"author": "_Wolfdieter Lang_, Jul 12 2001",
			"ext": [
				"More terms from Antonio G. Astudillo (afg_astudillo(AT)lycos.com), Mar 29 2003"
			],
			"references": 9,
			"revision": 27,
			"time": "2019-08-28T16:54:05-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}