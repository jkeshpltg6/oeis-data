{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A027349",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 27349,
			"data": "1,0,0,1,0,1,0,1,1,1,1,1,2,1,2,2,3,2,3,3,4,4,4,5,6,6,6,8,8,9,9,11,12,13,13,16,17,18,19,22,24,25,27,30,33,35,37,41,46,47,51,56,61,64,69,75,82,86,92,100,109,114,122,133,143,151,161,174,187,198",
			"name": "Number of partitions of n into distinct odd parts, the least being 1.",
			"comment": [
				"Column 1 of A116860. - _Emeric Deutsch_, Feb 27 2006",
				"Also number of partitions of n such that the largest part occurs exactly once and each number smaller than the largest part occurs an even nonzero number of times. Example: a(17)=3 because we have [3,2,2,2,2,2,2,1,1],[3,2,2,2,2,1,1,1,1,1,1] and [3,2,2,1,1,1,1,1,1,1,1,1,1]. - _Emeric Deutsch_, Mar 13 2006",
				"a(n) is the number of symmetric stack polyominoes of area n with square core. The core of a stack is the set of all maximal columns. The core is a square when the number of columns is equal to their height. Equivalently, a(n) is the number of symmetric unimodal compositions of n, where the number of the parts of maximum value equal the maximum value itself. For instance, for n = 20, we have the following stacks: (2,4,4,4,4,2), (1,1,4,4,4,4,1,1), (1,1,1,1,1,1,1,1,2,2,1,1,1,1,1,1,1,1). - _Emanuele Munarini_, Apr 08 2011"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A027349/b027349.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Jason Fulman, \u003ca href=\"https://doi.org/10.1090/S0273-0979-01-00920-X\"\u003eRandom matrix theory over finite fields\u003c/a\u003e, Bull. Amer. Math. Soc. (N.S.) 39 (2002), no. 1, 51--85. MR1864086 (2002i:60012). See top of page 70, Eq. 3, with k=1. - _N. J. A. Sloane_, Aug 31 2014",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/q-PochhammerSymbol.html\"\u003eq-Pochhammer Symbol\u003c/a\u003e"
			],
			"formula": [
				"G.f.: x*Product_{i\u003e=2} 1+x^(2*i-1). - _Emeric Deutsch_, Feb 27 2006",
				"G.f.: (Sum_{k\u003e=1} x^(k^2))/Product_{j=1..k-1} 1-x^(2*j). - _Emeric Deutsch_, Mar 13 2006",
				"a(n) ~ exp(Pi*sqrt(n/6)) / (2^(11/4)*3^(1/4)*n^(3/4)). - _Vaclav Kotesovec_, Oct 30 2015",
				"G.f.: 2/((1 + x)*(-1; -x)_inf), where (a; q)_inf is the q-Pochhammer symbol. - _Vladimir Reshetnikov_, Nov 22 2016",
				"If n \u003e 1, a(n) = A000700(n - 1) - a(n - 1). - _Álvar Ibeas_, Aug 03 2020",
				"G.f.: x*Sum_{n \u003e= 0} x^(n*(n+2))/Product_{k = 1..n} (1 - x^(2*k)) = x*(1 + x^3) * Sum_{n \u003e= 0} x^(n*(n+4))/Product_{k = 1..n} (1 - x^(2*k)) = x*(1 + x^3)*(1 + x^5) * Sum_{n \u003e= 0} x^(n*(n+6))/ Product_{k = 1..n} (1 - x^(2*k)) = .... - _Peter Bala_, Jan 15 2021"
			],
			"example": [
				"a(17) = 3 because we have [13,3,1], [11,5,1] and [9,7,1].",
				"G.f. = x + x^4 + x^6 + x^8 + x^9 + x^10 + x^11 + x^12 + 2*x^13 + x^14 + 2*x^15 + ..."
			],
			"maple": [
				"N := 100; t1 := series(mul(1+x^(2*k+1),k=1..N),x,N); A027349 := proc(n) coeff(t1,x,n); end;"
			],
			"mathematica": [
				"a[n_]:=CoefficientList[Series[1+Sum[x^((k+1)^2)/Product[(1-x^(2i)),{i,1,k}],{k,0,n}],{x,0,n}],x] (* _Emanuele Munarini_, Apr 08 2011 *)",
				"a[ n_] := SeriesCoefficient[ x QHypergeometricPFQ[ {}, {}, x^2, -x^3], {x, 0, n}]; (* _Michael Somos_, Feb 02 2015 *)",
				"nmax = 100; Rest[CoefficientList[Series[x/(1+x) * Product[1+x^(2*k-1), {k, 1, nmax}], {x, 0, nmax}], x]] (* _Vaclav Kotesovec_, Oct 30 2015 *)",
				"(2/((1 + x) QPochhammer[-1, -x]) + O[x]^70)[[3]] (* _Vladimir Reshetnikov_, Nov 22 2016 *)"
			],
			"xref": [
				"Cf. A000700, A116860, A001523, A096441, A188674."
			],
			"keyword": "nonn,easy",
			"offset": "1,13",
			"author": "_Clark Kimberling_",
			"references": 12,
			"revision": 51,
			"time": "2021-01-17T14:31:18-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}