{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A158188",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 158188,
			"data": "1,1,-1,1,-2,1,1,-5,5,-1,1,-6,10,-6,1,1,-9,25,-25,9,-1,1,-12,48,-78,48,-12,1,1,-19,102,-228,228,-102,19,-1,1,-20,121,-330,456,-330,121,-20,1,1,-23,176,-628,1167,-1167,628,-176,23,-1,1,-26,239,-1062,2532,-3368",
			"name": "Characteristic polynomials of a binomial modulo two Hadamard transpose general matrix: t(n,m,d) = If[ m \u003c= n, binomial(n, m) mod 2], 0]; M(d)=t(n,m,d).Transpose[t(n,m,d)].",
			"comment": [
				"Row sums are 1, 0, 0, 0, 0, 0, -4, 0, 0, 0, 0, ...",
				"Example matrix:",
				"  M(3) = {{1, 0, 1},",
				"          {0, 1, 1},",
				"          {1, 1, 3}}",
				"The traditional Hadamard self-similar matrix construction is on symbols {1,-1}.",
				"When instead the symbols {0,1} are use you get:",
				"  H(2*n) = {{H(n), H(n)},",
				"            {H(n),  0  }}",
				"which turns out to be a rotated Sierpinski-Pascal modulo two as an n X n matrix.",
				"Here the Hadamard transpose product of that construction gives a new set of symmetrical polynomials."
			],
			"reference": [
				"F. J. MacWilliams and N. J. A. Sloane, The Theory of Error-Correcting Codes, Elsevier/North Holland, 1978, pp. 44-48."
			],
			"formula": [
				"t(n,m,d) = If[ m \u003c= n, binomial(n, m) mod 2, 0];",
				"M(d) = t(n,m,d).Transpose[t(n,m,d)];",
				"a(n,m) = coefficients(characteristicpolynomial(M(n),x),x)."
			],
			"example": [
				"{1},",
				"{1,  -1},",
				"{1,  -2,   1},",
				"{1,  -5,   5,    -1},",
				"{1,  -6,  10,    -6,    1},",
				"{1,  -9,  25,   -25,    9,    -1},",
				"{1, -12,  48,   -78,   48,   -12,    1},",
				"{1, -19, 102,  -228,  228,  -102,   19,    -1},",
				"{1, -20, 121,  -330,  456,  -330,  121,   -20,   1},",
				"{1, -23, 176,  -628, 1167, -1167,  628,  -176,  23,  -1},",
				"{1, -26, 239, -1062, 2532, -3368, 2532, -1062, 239, -26, 1}"
			],
			"mathematica": [
				"Clear[M, T, d, a, x, a0];",
				"T[n_, m_, d_] := If[ m \u003c= n, Mod[Binomial[n, m], 2], 0];",
				"M[d_] := Table[T[n, m, d], {n, 1, d}, {m, 1, d}].Transpose[Table[T[n, m, d], {n, 1, d}, {m, 1, d}]];",
				"a0 = Table[M[d], {d, 1, 10}];",
				"Table[Det[M[d]], {d, 1, 10}];",
				"Table[CharacteristicPolynomial[M[d], x], {d, 1, 10}];",
				"a = Join[{{1}}, Table[CoefficientList[Expand[ CharacteristicPolynomial[M[n], x]], x], {n, 1, 10}]];",
				"Flatten[a]",
				"Join[{1}, Table[Apply[Plus, CoefficientList[Expand[CharacteristicPolynomial[M[n], x]], x]], {n, 1, 10}]];"
			],
			"keyword": "sign,tabl,uned",
			"offset": "0,5",
			"author": "_Roger L. Bagula_ and _Gary W. Adamson_, Mar 13 2009",
			"references": 0,
			"revision": 8,
			"time": "2019-05-04T07:09:37-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}