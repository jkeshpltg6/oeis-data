{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002866",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2866,
			"id": "M3604 N1463",
			"data": "1,1,4,24,192,1920,23040,322560,5160960,92897280,1857945600,40874803200,980995276800,25505877196800,714164561510400,21424936845312000,685597979049984000,23310331287699456000,839171926357180416000,31888533201572855808000,1275541328062914232320000",
			"name": "a(0) = 1; for n \u003e 0, a(n) = 2^(n-1)*n!.",
			"comment": [
				"Consider the set of n-1 odd numbers from 3 to 2n-1, i.e., {3, 5, ..., 2n-1}. There are 2^(n-1) subsets from {} to {3, 5, 7, ..., 2n-1}; a(n) = the sum of the products of terms of all the subsets. (Product for empty set = 1.) a(4) = 1 + 3 + 5 + 7 + 3*5 + 3*7 + 5*7 + 3*5*7 = 192. - _Amarnath Murthy_, Sep 06 2002",
				"Also, a(n-1) gives the ways to lace a shoe that has n pairs of eyelets such that there is a straight (horizontal) connection between all adjacent eyelet pairs. - _Hugo Pfoertner_, Jan 27 2003",
				"This is also the denominator of the integral of ((1-x^2)^(n-1/2))/(Pi/4) where x ranges from 0 to 1. The numerator is (2*x)!/(x!*2^x). In both cases n starts at 1. E.g., the denominator when n=3 is 24 and the numerator is 15. - Al Hakanson (hawkuu(AT)excite.com), Oct 17 2003",
				"Number of ways to use the elements of {1,...,n} once each to form a sequence of nonempty lists. - Bob Proctor, Apr 18 2005",
				"Row sums of A131222. - _Paul Barry_, Jun 18 2007",
				"Number of rotational symmetries of an n-cube. The number of all symmetries of an n-cube is A000165. See Egan for signed cycle notation, other notes, tables and animation. - _Jonathan Vos Post_, Nov 28 2007",
				"1, 4, 24, 192, 1920, ... is the exponential (or binomial) convolution of 1, 1, 3, 15, 105, ... and 1, 3, 15, 105, 945 (A001147). - _David Callan_, Jul 25 2008",
				"The n-th term of this sequence is the number of regions into which n-dimensional space is partitioned by the 2n hyperplanes of the form x_i=x_j and x_i=-x_j (for 1 \u003c= i \u003c j \u003c= n). - Edward Scheinerman (ers(AT)jhu.edu), May 04 2008",
				"a(n) is the number of ways to seat n churchgoers into pews and then linearly order the nonempty pews. - _Geoffrey Critzer_, Mar 16 2009",
				"Equals the row sums of A156992. - _Geoffrey Critzer_, Mar 05 2010",
				"From _Gary W. Adamson_, May 17 2010: (Start)",
				"Next term in the series = (1, 3, 5, 7, ...) dot (1, 1, 4, 24, ...);",
				"e.g., a(5) = 1920 = (1, 3, 5, 7, 9) dot (1, 1, 4, 24, 192) = (1 + 3 + 20 + 168 + 1728). (End)",
				"a(n) is the number of ways to represent the permutations of {1,2,...,n} in cycle notation, taking into account that we can permute the order of all cycles and also have k ways to write a length-k cycle.",
				"For positive n, a(n) equals the permanent of the n X n matrix with consecutive integers 1 to n along the main diagonal, consecutive integers 2 to n along the subdiagonal, and 1's everywhere else. - _John M. Campbell_, Jul 10 2011",
				"From _Dennis P. Walsh_, Nov 26 2011: (Start)",
				"Number of ways to arrange n books on consecutive bookshelves.",
				"To derive a(n) = n!2^(n-1), we note that there are n! ways to arrange the books in a row. Then there are 2^(n-1) ways to place the arranged books on consecutive shelves since there are 2^(n-1) ordered partitions of n. Hence a(n) = n!2^(n-1).",
				"Also, a(n) is the number of ways to stack n different alphabet blocks in contiguous stacks.",
				"Furthermore, a(n) is the number of labeled, rooted forests that have (i) each root labeled larger than any nonroot, (ii) each root having exactly one child node, (iii) n non-root nodes, and (iv) each node in the forest with at most one child node.",
				"Example: a(3)=24 since there are 24 arrangements of books b1, b2, and b3 on consecutive shelves, namely, |b1 b2 b3|, |b1 b3 b2|, |b2 b1 b3|, |b2 b3 b1|, |b3 b1 b2|, |b3 b2 b1|, |b1 b2||b3|, |b2 b1| |b3|, |b1 b3||b2|, |b3 b1||b2|, |b2 b3||b1|, |b3 b2||b1|, |b1||b2 b3|,|b1||b3 b2|, |b2||b1 b3|, |b2||b3 b1|, |b3||b1 b2|, |b3||b2 b1|, |b1||b2||b3|, |b1||b3||b2|, |b2||b1||b3|, |b2||b3||b1|, |b3||b1||b2|, and |b3||b2||b1|.",
				"(End)",
				"For n \u003e 3, a(n) is the order of the Coxeter group (also called Weyl group) of type D_n. - _Tom Edgar_, Nov 05 2013"
			],
			"reference": [
				"N. Bourbaki, Groupes et alg. de Lie, Chap. 4, 5, 6, p. 257.",
				"A. P. Prudnikov, Yu. A. Brychkov and O.I. Marichev, \"Integrals and Series\", Volume 1: \"Elementary Functions\", Chapter 4: \"Finite Sums\", New York, Gordon and Breach Science Publishers, 1986-1992, Eq. (4.2.2.26)",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A002866/b002866.txt\"\u003eTable of n, a(n) for n = 0..100\u003c/a\u003e",
				"Ofek Lauber Bonomo and Shlomi Reuveni, \u003ca href=\"https://arxiv.org/abs/1905.02170\"\u003eOccupancy correlations in the asymmetric simple inclusion process\u003c/a\u003e, arXiv:1905.02170 [cond-mat.stat-mech], 2019.",
				"J.-P. Bultel and A. Chouria, J.-G. Luque and O. Mallet, \u003ca href=\"https://arxiv.org/abs/1302.5815\"\u003eWord symmetric functions and the Redfield-Polya theorem\u003c/a\u003e, Accepted to FPSAC 2013, 2013; arXiv:1302.5815 [math.CO], 2013.",
				"P. J. Cameron, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL3/groups.html\"\u003eSequences realized by oligomorphic permutation groups\u003c/a\u003e, J. Integ. Seqs. 3 (2000), #00.1.5.",
				"Greg Egan, \u003ca href=\"http://gregegan.customer.netspace.net.au/APPLETS/29/HypercubeNotes.html\"\u003eHypercube Mathematical Details\u003c/a\u003e, revised Apr 22 2007.",
				"Joël Gay and Vincent Pilaud, \u003ca href=\"https://arxiv.org/abs/1804.06572\"\u003eThe weak order on Weyl posets\u003c/a\u003e, arXiv:1804.06572 [math.CO], 2018.",
				"W. S. Gray and M. Thitsa, \u003ca href=\"http://dx.doi.org/10.1109/SSST.2013.6524939\"\u003eSystem Interconnections and Combinatorial Integer Sequences\u003c/a\u003e, in: 2013 45th Southeastern Symposium on System Theory (SSST).",
				"Guo-Niu Han, \u003ca href=\"/A196265/a196265.pdf\"\u003eEnumeration of Standard Puzzles\u003c/a\u003e, 2011. [Cached copy]",
				"Guo-Niu Han, \u003ca href=\"https://arxiv.org/abs/2006.14070\"\u003eEnumeration of Standard Puzzles\u003c/a\u003e, arXiv:2006.14070 [math.CO], 2020.",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=121\"\u003eEncyclopedia of Combinatorial Structures 121\u003c/a\u003e.",
				"Victor Meally, \u003ca href=\"/A002868/a002868.pdf\"\u003eComparison of several sequences given in Motzkin's paper \"Sorting numbers for cylinders...\", letter to N. J. A. Sloane, N. D.\u003c/a\u003e",
				"T. S. Motzkin, \u003ca href=\"/A000262/a000262.pdf\"\u003eSorting numbers for cylinders and other classification numbers\u003c/a\u003e, in Combinatorics, Proc. Symp. Pure Math. 19, AMS, 1971, pp. 167-176. [Annotated, scanned copy]",
				"Norihiro Nakashima and Shuhei Tsujie, \u003ca href=\"https://arxiv.org/abs/1904.09748\"\u003eEnumeration of Flats of the Extended Catalan and Shi Arrangements with Species\u003c/a\u003e, arXiv:1904.09748 [math.CO], 2019.",
				"J.-C. Novelli and J.-Y. Thibon, \u003ca href=\"http://arxiv.org/abs/1403.5962\"\u003eHopf Algebras of m-permutations,(m+1)-ary trees, and m-parking functions\u003c/a\u003e, arXiv:1403.5962 [math.CO], 2014.",
				"OEIS Wiki, \u003ca href=\"http://oeis.org/wiki/Sorting_numbers\"\u003eSorting numbers\u003c/a\u003e.",
				"Hugo Pfoertner, \u003ca href=\"http://www.randomwalk.de/shoelace/strlace.txt\"\u003eCounting straight shoe lacings. FORTRAN program and results\u003c/a\u003e.",
				"Robert A. Proctor, \u003ca href=\"http://arxiv.org/abs/math/0606404\"\u003eLet's Expand Rota's Twelvefold Way For Counting Partitions!\u003c/a\u003e, arXiv:math/0606404 [math.CO], 2007.",
				"N. J. A. Sloane and Thomas Wieder, \u003ca href=\"https://arxiv.org/abs/math/0307064\"\u003eThe Number of Hierarchical Orderings\u003c/a\u003e, arXiv:math/0307064 [math.CO], 2003.",
				"N. J. A. Sloane and Thomas Wieder, \u003ca href=\"https://doi.org/10.1007/s11083-004-9460-9\"\u003eThe Number of Hierarchical Orderings\u003c/a\u003e, Order 21 (2004), 83-89.",
				"\u003ca href=\"/index/Di#divseq\"\u003eIndex to divisibility sequences\u003c/a\u003e",
				"\u003ca href=\"/index/La#lacings\"\u003eIndex entries for sequences related to shoe lacings\u003c/a\u003e",
				"\u003ca href=\"/index/Par#partN\"\u003eIndex entries for related partition-counting sequences\u003c/a\u003e"
			],
			"formula": [
				"E.g.f.: (1 - x)/(1 - 2*x). - _Paul Barry_, May 26 2003, corrected Jun 18 2007",
				"a(n) = n! * A011782(n).",
				"For n \u003e= 1, a(n) = Sum_{i=0..m/2} (-1)^i * binomial(n, i) * (n-2*i)^n. - Yong Kong (ykong(AT)curagen.com), Dec 28 2000",
				"a(n) ~ 2^(1/2) * Pi^(1/2) * n^(3/2) * 2^n * e^(-n) * n^n*{1 + 13/12*n^(-1) + ...}. - Joe Keane (jgk(AT)jgk.org), Nov 23 2001",
				"E.g.f. is B(A(x)), where B(x) = 1/(1 - x) and A(x) = x/(1 - x). - _Geoffrey Critzer_, Mar 16 2009",
				"a(n) = Sum_{k=1..n} A156992(n,k). - _Dennis P. Walsh_, Nov 26 2011",
				"a(n+1) = Sum_{k=0..n} A132393(n,k)*2^(n+k), n\u003e0. - _Philippe Deléham_, Nov 28 2011",
				"G.f.: 1 + x/(1 - 4*x/(1 - 2*x/(1 - 6*x/(1 - 4*x/(1 - 8*x/(1 - 6*x/(1 - 10*x/(1 - ... (continued fraction). - _Philippe Deléham_, Nov 29 2011",
				"a(n) = 2*n*a(n-1) for n \u003e= 2. - _Dennis P. Walsh_, Nov 29 2011",
				"G.f.: (1 + 1/G(0))/2,  where G(k)=  1 + 2*x*k - 2*x*(k + 1)/G(k+1); (continued fraction, Euler's 1st kind, 1-step). - _Sergei N. Gladkovskii_, Aug 02 2012",
				"G.f.: 1 + x/Q(0), m=4, where Q(k) = 1 - m*x*(2*k + 1) - m*x^2*(2*k + 1)*(2*k + 2)/(1 - m*x*(2*k + 2) - m*x^2*(2*k + 2)*(2*k + 3)/Q(k+1)) ; (continued fraction). - _Sergei N. Gladkovskii_, Sep 23 2013",
				"G.f.: 1 + x/(G(0) - x), where G(k) = 1 + x*(k+1) - 4*x*(k + 1)/(1 - x*(k + 2)/G(k+1)); (continued fraction). - _Sergei N. Gladkovskii_, Dec 24 2013",
				"a(n) = Sum_{k=0..n} L(n,k)*k!; L(n,k) are the unsigned Lah numbers. - _Peter Luschny_, Oct 18 2014",
				"a(n) = round(Sum_{k \u003e= 1} log(k)^n/k^(3/2))/4,  for n \u003e= 1, which is related to the n-th derivative of zeta(x) evaluated at x = 3/2. - _Richard R. Forberg_, Jan 02 2015",
				"a(n) = n!*hypergeom([-n+1], [], -1)) for n\u003e=1. - _Peter Luschny_, Apr 08 2015",
				"From _Amiram Eldar_, Aug 04 2020: (Start)",
				"Sum_{n \u003e= 0} 1/a(n) = 2*sqrt(e) - 1.",
				"Sum_{n \u003e= 0} (-1)^n/a(n) = 2/sqrt(e) - 1. (End)"
			],
			"example": [
				"For the shoe lacing: with the notation introduced in A078602 the a(3-1) = 4 \"straight\" lacings for 3 pairs of eyelets are: 125346, 125436, 134526, 143526. Their mirror images 134256, 143256, 152346, 152436 are not counted.",
				"a(3) = 24 because the 24 rotations of a three-dimensional cube fall into four distinct classes:",
				"(i) the identity, which leaves everything fixed;",
				"(ii) 9 rotations which leave the centers of two faces fixed, comprising rotations of 90, 180 and 270 degrees for each of 3 pairs of faces;",
				"(iii) 6 rotations which leave the centers of two edges fixed, comprising rotations of 180 degrees for each of 6 pairs of edges;",
				"(iv) 8 rotations which leave two vertices fixed, comprising rotations of 120 and 240 degrees for each of 4 pairs of vertices. For an n-cube, rotations can be more complex. For example, in 4 dimensions a rotation can either act in a single plane, such as the x-y plane, while leaving any vectors orthogonal to that plane unchanged, or it can act in two orthogonal planes, performing rotations in both and leaving no vectors fixed. In higher dimensions, there will be room for more planes and more choices as to the number of planes in which a given rotation acts."
			],
			"maple": [
				"A002866 := n-\u003e `if`(n=0,1,2^(n-1)*n!):",
				"with(combstruct); SeqSeqL := [S, {S=Sequence(U,card \u003e= 1), U=Sequence(Z,card \u003e=1)},labeled];",
				"seq(ceil(count(Subset(n))*count(Permutation(n))/2),n=0..17); # _Zerinvary Lajos_, Oct 16 2006",
				"G(x):=(1-x)/(1-2*x): f[0]:=G(x): for n from 1 to 26 do f[n]:=diff(f[n-1],x) od:x:=0:seq(f[n],n=0..17); # _Zerinvary Lajos_, Apr 04 2009"
			],
			"mathematica": [
				"Join[{1},Table[2^(n-1) n!,{n,25}]] (* _Harvey P. Dale_, Sep 27 2013 *)"
			],
			"program": [
				"(FORTRAN) See Pfoertner link.",
				"(PARI) a(n)=if(n,n!\u003c\u003c(n-1),1) \\\\ _Charles R Greathouse IV_, Jan 13 2012",
				"(PARI) a(n) = if(n == 0, 1, 2^(n-1)*n!);",
				"vector(25, n, a(n-1)) \\\\ _Altug Alkan_, Oct 18 2015",
				"(MAGMA) [1] cat [2^(n-1)*Factorial(n): n in [1..25]]; // _G. C. Greubel_, Jun 13 2019",
				"(Sage) [1] + [2^(n-1)*factorial(n) for n in (1..25)] # _G. C. Greubel_, Jun 13 2019"
			],
			"xref": [
				"Cf. A028371, A078602, A078698, A078702, A156992.",
				"Bisections give A002671 and A274304.",
				"Appears in A167584 (n \u003e= 1); equals the row sums of A167594 (n \u003e= 1). - _Johannes W. Meijer_, Nov 12 2009"
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"references": 69,
			"revision": 196,
			"time": "2020-08-04T19:21:09-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}