{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A090210",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 90210,
			"data": "1,1,1,2,1,1,5,7,1,1,15,87,34,1,1,52,1657,2971,209,1,1,203,43833,513559,163121,1546,1,1,877,1515903,149670844,326922081,12962661,13327,1,1,4140,65766991,66653198353,1346634725665,363303011071,1395857215,130922,1,1",
			"name": "Triangle of certain generalized Bell numbers.",
			"comment": [
				"Let B_{n}(x) = sum_{j\u003e=0}(exp(j!/(j-n)!*x-1)/j!) and",
				"S(n,k) = k! [x^k] taylor(B_{n}(x)), where [x^k] denotes the",
				"coefficient of x^k in the Taylor series for B_{n}(x).",
				"Then S(n,k) (n\u003e0, k\u003e=0) is the square array representation of the triangle.",
				"To illustrate the cross-references of T(n,k) when written as a square array.",
				"n\\k         A000012,A000012,A002720,A069948,A182925,A182924,...",
				"0: A000012: 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...",
				"1: A000110: 1, 1, 2, 5, 15, 52, 203, 877, 4140, ...",
				"2: A020556: 1, 1, 7, 87, 1657, 43833, 1515903, ...",
				"3: A069223: 1, 1, 34, 2971, 513559, 149670844, ...",
				"4: A071379: 1, 1, 209, 163121, 326922081, ...",
				"5: A090209: 1, 1, 1546, 12962661, 363303011071,...",
				"6:  ...     1, 1, 13327, 1395857215, 637056434385865,...",
				"Note that the sequence T(0,k) is not included in the data.",
				"- _Peter Luschny_, Mar 27 2011"
			],
			"reference": [
				"P. Blasiak, K. A. Penson and A. I. Solomon, The general boson normal ordering problem, Phys. Lett. A 309 (2003) 198-205.",
				"M. Schork, On the combinatorics of normal ordering bosonic operators and deforming it, J. Phys. A 36 (2003) 4651-4665."
			],
			"link": [
				"P. Blasiak, K. A. Penson and A. I. Solomon, \u003ca href=\"http://www.arXiv.org/abs/quant-ph/0402027\"\u003eThe general boson normal ordering problem.\u003c/a\u003e",
				"W. Lang, \u003ca href=\"/A090210/a090210.txt\"\u003eFirst 8 rows\u003c/a\u003e.",
				"K. A. Penson, P. Blasiak, A. Horzela, A. I. Solomon and G. H. E. Duchamp,\u003ca href=\"http://arxiv.org/abs/0904.0369\"\u003eLaguerre-type derivatives: Dobinski relations and combinatorial identities\u003c/a\u003e, J. Math. Phys. 50, 083512 (2009)."
			],
			"formula": [
				"a(n, m)= Bell(m;n-(m-1)), n\u003e= m-1 \u003e=0, with Bell(m;k) := sum(S2(m;k, p), p=m..m*k), where S2(m;k, p) := (((-1)^p)/p!)*sum(((-1)^r)*binomial(p, r)*fallfac(p, r)^k, r=m..p); with fallfac(n, m) := A008279(n, m) (falling factorials) and m\u003c=p\u003c=k*m, k\u003e=1, m=1, 2, ..., else 0. From eqs.(6) with r=s-\u003em and eq.(19) with S_{r, r}(n, k)-\u003e S2(r;n, k) of the Blasiak et al. reference.",
				"a(n, m)= (sum(fallfac(k, m)^(n-(m-1)), k=m..infinity))/exp(1), n\u003e= m-1 \u003e=0, else 0. From eq.(26) with r-\u003em of the Schork reference which is rewritten eq.(11) of the original Blasiak et al. reference.",
				"E.g.f. m-th column (no leading zeros): (sum((exp(fallfac(k, m)*x))/k!, k=m..infinity) + A000522(m)/m!)/exp(1). Rewritten from the top of p. 4656 of the Schork reference."
			],
			"example": [
				"Triangle begins:",
				"1;",
				"1, 1;",
				"2, 1, 1;",
				"5, 7, 1, 1;",
				"15, 87, 34, 1, 1;",
				"52, 1657, 2971, 209, 1, 1;",
				"203, 43833, 513559, 163121, 1546, 1, 1;"
			],
			"maple": [
				"A090210_AsSquareArray := proc(n,k) local r,s,i;",
				"if k=0 then 1 else r := [seq(n+1,i=1..k-1)]; s := [seq(1,i=1..k-1)];",
				"exp(-x)*n!^(k-1)*hypergeom(r,s,x); round(evalf(subs(x=1,%),99)) fi end:",
				"seq(lprint(seq(A090210_AsSquareArray(n,k),k=0..6)),n=0..6);",
				"# _Peter Luschny_, Mar 30 2011"
			],
			"mathematica": [
				"t[n_, k_] := t[n, k] = Sum[(n+j)!^(k-1)/(j!^k*E), {j, 0, Infinity}]; t[_, 0] = 1;",
				"Flatten[ Table[ t[n-k+1, k], {n, 0, 8}, {k, n, 0, -1}]][[1 ;; 43]] (* _Jean-François Alcover_, Jun 17 2011 *)"
			],
			"xref": [
				"Cf. A000110, A020556, A069223, A071379, A090209, A002720, A069948, A182924, A182925, A182933.",
				"T(n,n) gives A070227."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "1,4",
			"author": "_Wolfdieter Lang_, Dec 01 2003",
			"references": 11,
			"revision": 32,
			"time": "2019-08-28T16:36:53-04:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}