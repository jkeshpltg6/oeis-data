{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A123518",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 123518,
			"data": "1,8,38,166,671,2602,9792,36068,130697,467556,1655406,5811290,20255279,70172502,241839184,829685064,2835099649,9653650752,32768012102,110913651342,374469646511,1261386990850,4240037471152,14225209349036",
			"name": "Number of dumbbells in all possible arrangements of dumbbells on a 2 X n rectangular array of compartments.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A123518/b123518.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"R. C. Grimson, \u003ca href=\"/A002889/a002889.pdf\"\u003eExact formulas for 2 x n arrays of dumbbells\u003c/a\u003e, J. Math. Phys., 15.2 (1974), 214-216. (Annotated scanned copy)",
				"R. C. Grimson, \u003ca href=\"http://dx.doi.org/10.1063/1.1666624\"\u003eExact formulas for 2 x n arrays of dumbbells\u003c/a\u003e, J. Math. Phys., 15 (1974), 214-216.",
				"R. B. McQuistan and S. J. Lichtman, \u003ca href=\"http://dx.doi.org/10.1063/1.1665098\"\u003eExact recursion relation for 2 x N arrays of dumbbells\u003c/a\u003e, J. Math. Phys., 11 (1970), 3095-3099.",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (6,-7,-8,5,2,-1)."
			],
			"formula": [
				"a(n) = Sum_{k=0..n} k*A046741(n,k).",
				"G.f.: x*(1 + 2*x - 3*x^2 + 2*x^3)/(1 - 3*x - x^2 + x^3)^2."
			],
			"example": [
				"a(2)=8 because in a 2 X 2 array of compartments, numbered clockwise starting from the NW one, we have 7 (=A030186(2)) possible arrangements of dumbbells: [ ], [14], [23], [12], [34], [14,23] and [12,34] (ij indicates a dumbbell placed in the compartments i and j); these contain altogether 8 dumbbells."
			],
			"maple": [
				"G:=z*(1+2*z-3*z^2+2*z^3)/(1-3*z-z^2+z^3)^2: Gser:=series(G,z=0,30): seq(coeff(Gser,z,n),n=1..27);"
			],
			"mathematica": [
				"LinearRecurrence[{6,-7,-8,5,2,-1}, {1,8,38,166,671,2602}, 30] (* _G. C. Greubel_, Oct 28 2019 *)"
			],
			"program": [
				"(PARI) my(x='x+O('x^30)); Vec(x*(1+2*x-3*x^2+2*x^3)/(1-3*x-x^2+x^3)^2) \\\\ _G. C. Greubel_, Oct 28 2019",
				"(MAGMA) R\u003cx\u003e:=PowerSeriesRing(Integers(), 30); Coefficients(R!( x*(1+2*x-3*x^2+2*x^3)/(1-3*x-x^2+x^3)^2 )); // _G. C. Greubel_, Oct 28 2019",
				"(Sage)",
				"def A123518_list(prec):",
				"    P.\u003cx\u003e = PowerSeriesRing(ZZ, prec)",
				"    return P( x*(1+2*x-3*x^2+2*x^3)/(1-3*x-x^2+x^3)^2 ).list()",
				"a=A123518_list(30); a[1:] # _G. C. Greubel_, Oct 28 2019",
				"(GAP) a:=[1,8,38,166,671,2602];; for n in [7..30] do a[n]:=6*a[n-1] -7*a[n-2]-8*a[n-3]+5*a[n-4]+2*a[n-5]-a[n-6]; od; a; # _G. C. Greubel_, Oct 28 2019"
			],
			"xref": [
				"Cf. A030186, A046741."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Emeric Deutsch_, Oct 16 2006",
			"references": 1,
			"revision": 14,
			"time": "2019-10-29T09:05:50-04:00",
			"created": "2006-12-06T03:00:00-05:00"
		}
	]
}