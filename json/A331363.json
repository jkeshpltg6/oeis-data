{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A331363",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 331363,
			"data": "0,0,1,0,0,1,-2,-1,3,-1,0,2,-4,-2,5,-2,0,3,-6,-3,7,-3,0,4,-8,-4,9,-4,0,5,-10,-5,11,-5,0,6,-12,-6,13,-6,0,7,-14,-7,15,-7,0,8,-16,-8,17,-8,0,9,-18,-9,19,-9,0,10,-20,-10,21,-10,0,11",
			"name": "Pairs of coordinates of the corners in a counterclockwise triangular spiral.",
			"comment": [
				"Odd n yields the x- and even n the y-coordinates (i.e., x- and y-coordinates alternate in the sequence)."
			],
			"link": [
				"\u003ca href=\"/index/Rec#order_12\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,0,0,0,0,2,0,0,0,0,0,-1)."
			],
			"formula": [
				"a(n) = ceiling(1/18*n*((2 - n) mod 6 + 4*n mod (-3) + 1)), for n \u003e= 1.",
				"x(n) = ceiling(n - 2/3*(n^2 + 1) mod 3), for n \u003e= 1 (x-coordinates).",
				"y(n) = floor(2*n/3)*((2 - n) mod (-3) + 1), for n \u003e= 1 (y-coordinates).",
				"From _Colin Barker_, May 03 2020: (Start)",
				"G.f.: x^3*(1 + x^3 - 2*x^4 - x^5 + x^6 - x^7) / ((1 - x)^2*(1 + x)^2*(1 - x + x^2)^2*(1 + x + x^2)^2).",
				"a(n) = 2*a(n-6) - a(n-12) for n\u003e12.",
				"(End)",
				"From _Wolfdieter Lang_, Jul 13 2020: (Start)",
				"Bisection: x(k) = a(2*k-1). x(3+3*l) = 0, x(1+3*l) = -2*l, x(2+3*l) = 1+2*l, for l \u003e= 0.",
				"x(k) = (2*(k-1)*modp((k-4)^2,3) - (2*k-1)*modp((k-2)^2,3) + 1)/3, for k \u003e= 1.",
				"y(k) = a(2*k). y(3+3*k) = 1+l, y(1+3*k) = -l =  y(2+3*k), for l \u003e= 0.",
				"y(k) = ((k-1)*modp((k-1)^2,3) + (k-2)*modp((k+1)^2,3) - k*modp(k^2,3) -(k-3))/3, k \u003e= 1.",
				"G.f.s: Gx(t) = t^2*(1 - 2*t^2 + t^3)/(1 - t^3)^2, and Gy(t) = t^3*(1 - t - t^2) / (1 - t^3)^2.",
				"This produces the g.f. G(x) = Gy(x^2) + Gx(x^2)/x given by _Colin Barker_.",
				"(End)"
			],
			"example": [
				"X- and y-coordinates of the corners alternate in the sequence: 0, 0, 1, 0, 0, 1, -2,-1, 3, -1, ...",
				"                      (0,4)",
				"                     .     \\",
				"                    .       \\",
				"                   .         \\",
				"                      (0,3)   \\",
				"                     /     \\   \\",
				"                    /       \\   \\",
				"                   /         \\   \\",
				"                  /   (0,2)   \\   \\",
				"                 /   /     \\   \\   \\",
				"                /   /       \\   \\   \\",
				"               /   /         \\   \\   \\",
				"              /   /   (0,1)   \\   \\   \\",
				"             /   /   /     \\   \\   \\   \\",
				"            /   /   /       \\   \\   \\   \\",
				"           /   /   /         \\   \\   \\   \\",
				"          /   /   /   (0,0)-\u003e(1,0)\\   \\   \\",
				"         /   /   /                 \\   \\   \\",
				"        /   /   /                   \\   \\   \\",
				"       /   /  (-2,-1)-------------\u003e(3,-1)\\   \\",
				"      /   /                               \\   \\",
				"     /   /                                 \\   \\",
				"    /  (-4,-2)---------------------------\u003e(5,-2)\\",
				"   /                                             \\",
				"  /                                               \\",
				"(-6,-3)------------------------------------------\u003e(7,-3)"
			],
			"mathematica": [
				"a[n_] := Ceiling[1/18*n*(Mod[2 - n, 6] + 4*Mod[n, -3] + 1)]; Table[ a[n], {n, 66}] (* or *)",
				"LinearRecurrence[{0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, -1}, {0, 0, 1, 0, 0, 1, -2, -1, 3, -1, 0, 2}, 66]"
			],
			"xref": [
				"Cf. A329116, A329972."
			],
			"keyword": "sign,look",
			"offset": "1,7",
			"author": "_Mikk Heidemaa_, May 03 2020",
			"references": 0,
			"revision": 45,
			"time": "2020-09-13T17:02:07-04:00",
			"created": "2020-09-13T17:02:07-04:00"
		}
	]
}