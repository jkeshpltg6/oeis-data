{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A192616",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 192616,
			"data": "1,0,1,1,2,6,10,29,57,142,309,720,1625,3714,8457,19259,43902,99970,227830,518943,1182401,2693624,6136837,13980960,31851853,72565704,165320833,376638417,858066430,1954869262,4453630790,10146374277,23115721705",
			"name": "Constant term in the reduction of the n-th Fibonacci polynomial by x^3-\u003ex^2+x+1.  See Comments.",
			"comment": [
				"For discussions of polynomial reduction, see A192232 and A192744."
			],
			"link": [
				"\u003ca href=\"/index/Rec\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,4,-1,-4,1,1)."
			],
			"formula": [
				"a(n) = a(n-1)+4*a(n-2)-a(n-3)-4a(n-4)+a(n-5)+a(n-6).",
				"G.f.: -x*(x^4+x^3-3*x^2-x+1)/(x^6+x^5-4*x^4-x^3+4*x^2+x-1). [_Colin Barker_, Jul 27 2012]"
			],
			"example": [
				"The first five polynomials p(n,x) and their reductions:",
				"F1(x)=1 -\u003e 1",
				"F2(x)=x -\u003e x",
				"F3(x)=x^2+1 -\u003e x^2+1",
				"F4(x)=x^3+2x -\u003e x^2+3x+1",
				"F5(x)=x^4+3x^2+1 -\u003e 4x^2+2x+2, so that",
				"A192616=(1,0,1,1,2,...), A192617=(0,1,0,3,2,...), A192651=(0,0,1,1,5,...)"
			],
			"mathematica": [
				"q = x^3; s = x^2 + x + 1; z = 40;",
				"p[n_, x_] := Fibonacci[n, x];",
				"Table[Expand[p[n, x]], {n, 1, 7}]",
				"reduce[{p1_, q_, s_, x_}] :=",
				"FixedPoint[(s PolynomialQuotient @@ #1 +",
				"       PolynomialRemainder @@ #1 \u0026)[{#1, q, x}] \u0026, p1]",
				"t = Table[reduce[{p[n, x], q, s, x}], {n, 1, z}];",
				"u1 = Table[Coefficient[Part[t, n], x, 0], {n, 1, z}]",
				"  (* A192616 *)",
				"u2 = Table[Coefficient[Part[t, n], x, 1], {n, 1, z}]",
				"  (* A192617 *)",
				"u3 = Table[Coefficient[Part[t, n], x, 2], {n, 1, z}]",
				"  (* A192651 *)"
			],
			"xref": [
				"Cf. A192744, A192232, A192617, A192651."
			],
			"keyword": "nonn,easy",
			"offset": "1,5",
			"author": "_Clark Kimberling_, Jul 09 2011",
			"references": 14,
			"revision": 11,
			"time": "2015-06-13T00:53:53-04:00",
			"created": "2011-07-10T00:59:41-04:00"
		}
	]
}