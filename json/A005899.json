{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005899",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5899,
			"id": "M4115",
			"data": "1,6,18,38,66,102,146,198,258,326,402,486,578,678,786,902,1026,1158,1298,1446,1602,1766,1938,2118,2306,2502,2706,2918,3138,3366,3602,3846,4098,4358,4626,4902,5186,5478,5778,6086,6402,6726,7058,7398,7746,8102,8466",
			"name": "Number of points on surface of octahedron; also coordination sequence for cubic lattice: a(0) = 1; for n \u003e 0, a(n) = 4n^2 + 2,",
			"comment": [
				"Also, the number of regions the plane can be cut into by two overlapping concave (2n)-gons. - _Joshua Zucker_, Nov 05 2002",
				"If X is an n-set and Y_i (i=1,2,3) are mutually disjoint 2-subsets of X then a(n-5) is equal to the number of 5-subsets of X intersecting each Y_i (i=1,2,3). - _Milan Janjic_, Aug 26 2007",
				"Binomial transform of a(n) is A055580(n). - _Wesley Ivan Hurt_, Apr 15 2014",
				"The identity (4*n^2+2)^2 - (n^2+1)*(4*n)^2 = 4 can be written as a(n)^2 - A002522(n)*A008586(n)^2 = 4. - _Vincenzo Librandi_, Jun 15 2014",
				"Also the least number of unit cubes required, at the n-th iteration, to surround a 3D solid built from unit cubes, in order to hide all its visible faces, starting with a unit cube. - _R. J. Cano_, Sep 29 2015",
				"Also, coordination sequence for \"tfs\" 3D uniform tiling. - _N. J. A. Sloane_, Feb 10 2018",
				"Also, the number of n-th order specular reflections arriving at a receiver point from an emitter point inside a cuboid with reflective faces. - _Michael Schutte_, Sep 18 2018"
			],
			"reference": [
				"H. S. M. Coxeter, \"Polyhedral numbers,\" in R. S. Cohen et al., editors, For Dirk Struik. Reidel, Dordrecht, 1974, pp. 25-35.",
				"Gmelin Handbook of Inorg. and Organomet. Chem., 8th Ed., 1994, TYPIX search code (225) cF8",
				"B. Grünbaum, Uniform tilings of 3-space, Geombinatorics, 4 (1994), 49-56. See tilings #16 and #22.",
				"R. W. Marks and R. B. Fuller, The Dymaxion World of Buckminster Fuller. Anchor, NY, 1973, p. 46.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A005899/b005899.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Barry Balof, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL15/Balof/balof19.html\"\u003eRestricted tilings and bijections\u003c/a\u003e, J. Integer Seq. 15 (2012), no. 2, Article 12.2.3, 17 pp.",
				"J. H. Conway and N. J. A. Sloane, Low-Dimensional Lattices VII: Coordination Sequences, Proc. Royal Soc. London, A453 (1997), 2369-2389 (\u003ca href=\"http://neilsloane.com/doc/Me220.pdf\"\u003epdf\u003c/a\u003e).",
				"Pierre de la Harpe, \u003ca href=\"https://arxiv.org/abs/2106.02499\"\u003eOn the prehistory of growth of groups\u003c/a\u003e, arXiv:2106.02499 [math.GR], 2021.",
				"R. W. Grosse-Kunstleve, \u003ca href=\"/A005897/a005897.html\"\u003eCoordination Sequences and Encyclopedia of Integer Sequences\u003c/a\u003e",
				"R. W. Grosse-Kunstleve, G. O. Brunner and N. J. A. Sloane, \u003ca href=\"http://neilsloane.com/doc/ac96cs/\"\u003eAlgebraic Description of Coordination Sequences and Exact Topological Densities for Zeolites\u003c/a\u003e, Acta Cryst., A52 (1996), pp. \u003ca href=\"http://dx.doi.org/10.1107/S0108767396007519\"\u003e879-889\u003c/a\u003e.",
				"Milan Janjic, \u003ca href=\"http://www.pmfbl.org/janjic/\"\u003eTwo Enumerative Functions\u003c/a\u003e",
				"Milan Janjić, \u003ca href=\"https://arxiv.org/abs/1905.04465\"\u003eOn Restricted Ternary Words and Insets\u003c/a\u003e, arXiv:1905.04465 [math.CO], 2019.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"Reticular Chemistry Structure Resource (RCSR), \u003ca href=\"http://rcsr.net/nets/pcu\"\u003eThe pcu tiling (or net)\u003c/a\u003e",
				"Reticular Chemistry Structure Resource (RCSR), \u003ca href=\"http://rcsr.net/nets/tfs\"\u003eThe tfs tiling (or net)\u003c/a\u003e",
				"B. K. Teo and N. J. A. Sloane, \u003ca href=\"http://neilsloane.com/doc/magic1/magic1.html\"\u003eMagic numbers in polygonal and polyhedral clusters\u003c/a\u003e, Inorgan. Chem. 24 (1985),4545-4558.",
				"N. J. A. Sloane, \u003ca href=\"/A005899/a005899.png\"\u003eIllustration of a(0)=1, a(1)=6, a(2)=18\u003c/a\u003e (from Teo-Sloane 1985)",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3, -3, 1)."
			],
			"formula": [
				"G.f.: ((1+x)/(1-x))^3. - _Simon Plouffe_ in his 1992 dissertation",
				"Binomial transform of [1, 5, 7, 1, -1, 1, -1, 1, ...]. - _Gary W. Adamson_, Nov 02 2007",
				"a(n) = 3*a(n-1) - 3*a(n-2) + a(n-3), with a(0)=1, a(1)=6, a(2)=18, a(3)=38. - _Harvey P. Dale_, Nov 08 2011",
				"Recurrence: n*a(n) = (n-2)*a(n-2) + 6*a(n-1), a(0)=1, a(1)=6. - _Fung Lam_, Apr 15 2014",
				"For n \u003e 0, a(n) = A001844(n-1) + A001844(n) = (n-1)^2 + 2n^2 + (n+1)^2. - _Doug Bell_, Aug 18 2015",
				"For n \u003e 0, a(n) = A010014(n) - A195322(n). - _R. J. Cano_, Sep 29 2015",
				"For n \u003e 0, a(n) = A000384(n+1) + A014105(n-1). - _Bruce J. Nicholson_, Oct 08 2017",
				"a(n) = A008574(n) + A008574(n-1) + a(n-1). - _Bruce J. Nicholson_, Dec 18 2017"
			],
			"maple": [
				"A005899:=n-\u003e4*n^2 + 2; seq(A005899(n), n=0..50); # _Wesley Ivan Hurt_, Apr 15 2014"
			],
			"mathematica": [
				"Join[{1},4Range[40]^2+2] (* or *) Join[{1},LinearRecurrence[{3,-3,1},{6,18,38},40]] (* _Harvey P. Dale_, Nov 08 2011 *)"
			],
			"program": [
				"(PARI) Vec(((1+x)/(1-x))^3 + O(x^100)) \\\\ _Altug Alkan_, Oct 26 2015",
				"(MAGMA) [4*n^2 + 2 : n in [0..50]]; // _Wesley Ivan Hurt_, Oct 26 2015"
			],
			"xref": [
				"Partial sums give A001845.",
				"Column 2 * 2 of array A188645.",
				"Cf. A001844, A002522, A008586, A010014, A055580, A195322, A206399.",
				"Cf. A000384, A014105, A000217, A008574, A008412.",
				"The 28 uniform 3D tilings: cab: A299266, A299267; crs: A299268, A299269; fcu: A005901, A005902; fee: A299259, A299265; flu-e: A299272, A299273; fst: A299258, A299264; hal: A299274, A299275; hcp: A007899, A007202; hex: A005897, A005898; kag: A299256, A299262; lta: A008137, A299276; pcu: A005899, A001845; pcu-i: A299277, A299278; reo: A299279, A299280; reo-e:  A299281, A299282; rho: A008137, A299276; sod: A005893, A005894; sve: A299255, A299261; svh: A299283, A299284; svj: A299254, A299260; svk: A010001, A063489; tca: A299285, A299286; tcd: A299287, A299288; tfs: A005899, A001845; tsi: A299289, A299290; ttw: A299257, A299263; ubt: A299291, A299292; bnn: A007899, A007202. See the Proserpio link in A299266 for overview."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 70,
			"revision": 170,
			"time": "2021-09-22T18:50:22-04:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}