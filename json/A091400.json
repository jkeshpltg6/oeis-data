{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A091400",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 91400,
			"data": "1,1,0,1,2,0,0,1,0,2,0,0,2,0,0,1,2,0,0,2,0,0,0,0,2,2,0,0,2,0,0,1,0,2,0,0,2,0,0,2,2,0,0,0,0,0,0,0,0,2,0,2,2,0,0,0,0,2,0,0,2,0,0,1,4,0,0,2,0,0,0,0,2,2,0,0,0,0,0,2,0,2,0,0,4,0,0,0,2,0,0,0,0,0,0,0,2,0,0,2,2,0,0,2,0",
			"name": "a(n) = Product_{ odd primes p | n } (1 + Legendre(-1,p) ).",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"reference": [
				"G. Shimura, Introduction to the Arithmetic Theory of Automorphic Functions, Princeton, 1971, see p. 25, Eq. (2) (but without the restriction that a(4k) = 0)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A091400/b091400.txt\"\u003eTable of n, a(n) for n = 1..5000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Here we use the definition that Legendre(-1, 2) = 0, Legendre(-1, p) = 1 if p == 1 mod 4, = -1 if p == 3 mod 4. This is Shimura's definition, which is different from Maple's.",
				"Moebius transform is period 36 sequence [1, 0, -1, 0, 1, 0, -1, 0, 0, 0, -1, 0, 1, 0, -1, 0, 1, 0, -1, 0, 1, 0, -1, 0, 1, 0, 0, 0, 1, 0, -1, 0, 1, 0, -1, 0, ...]. - _Michael Somos_, Apr 19 2007",
				"Expansion of (phi(q)^2 - phi(q^9)^2) / 4 in powers of q where phi() is a Ramanujan theta function.",
				"a(n) is multiplicative with:",
				"   a(2^e) = 1 for e \u003e= 0,",
				"   a(p^e) = 0 if p == 3 (mod 4) for e \u003e 0,",
				"   a(p^e) = 2 if p == 1 (mod 4) for e \u003e 0.",
				"(corrected by _Werner Schulte_, Dec 12 2020.",
				"a(2*n) = a(n). a(3*n) = a(4*n + 3) = 0.",
				"a(3*n + 1) = A122865(n). a(3*n + 2) = A122856(n).",
				"a(n) = Sum_{d|n} b(d)*(-1)^bigomega(d)*moebius(d) where b(2n)=0 and b(2n+1)=(-1)^n. - _Benoit Cloitre_, Apr 17 2016",
				"G.f.: ((Sum_{k in Z} x^k^2)^2 - (Sum_{k in Z} x^(9*k^2))^2) / 4. - _Michael Somos_, Jan 26 2017"
			],
			"example": [
				"G.f. = x + x^2 + x^4 + 2*x^5 + x^8 + 2*x^10 + 2*x^13 + x^16 + 2*x^17 + 2*x^20 + ..."
			],
			"maple": [
				"with(numtheory): A091400 := proc(n) local i,t1,t2; t1 := ifactors(n)[2]; t2 := 1; for i from 1 to nops(t1) do if t1[i][1] \u003e 2 then t2 := t2*(1+legendre(-1,t1[i][1])); fi; od: t2; end;",
				"with(numtheory): seq(mul(1+legendre(-1,p),p in select(isprime, divisors(n) minus {2})),n=1..105); # _Peter Luschny_, Apr 20 2016"
			],
			"mathematica": [
				"Legendre[-1, p_] := Which[p==2, 0, Mod[p, 4]==1, 1, True, -1]; a[1] = 1; a[n_] := Times @@ (Legendre[-1, #] + 1\u0026) /@ FactorInteger[n][[All, 1]]; Array[a, 105] (* _Jean-François Alcover_, Dec 01 2015 *)",
				"Join[{1},Table[Product[1+JacobiSymbol[-1,p],{p,Complement[FactorInteger[n][[All, 1]], {2}]}], {n,2,105}]] (* _Peter Luschny_, Apr 20 2016 *)",
				"a[ n_] := SeriesCoefficient[ (EllipticTheta[ 3, 0, q]^2 - EllipticTheta[ 3, 0, q^9]^2) / 4, {q, 0, n}]; (* _Michael Somos_, Jan 26 2017 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, 0, sumdiv(n, d, kronecker(-9, d) * kronecker(36, n/d)))}; /* _Michael Somos_, Jan 26 2017 */",
				"(PARI) {a(n)=if(n\u003c1,0,sumdiv(n,d,(-1)^bigomega(d)*moebius(d)*if(d%2,(-1)^(d\\2),0))} /* _Benoit Cloitre_, Apr 17 2016 */"
			],
			"xref": [
				"Cf. A091379, A122856, A122865, A129448."
			],
			"keyword": "nonn,mult",
			"offset": "1,5",
			"author": "_N. J. A. Sloane_, Mar 02 2004",
			"ext": [
				"Definition clarified by _Peter Luschny_, Apr 20 2016"
			],
			"references": 5,
			"revision": 39,
			"time": "2020-12-12T16:16:09-05:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}