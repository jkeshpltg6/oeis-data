{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A050232",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 50232,
			"data": "0,0,0,1,3,8,20,48,111,251,558,1224,2656,5713,12199,25888,54648,114832,240335,501239,1042126,2160676,4468664,9221281,18989899,39034824,80103276,164126496,335808927,686182387,1400438814",
			"name": "a(n) is the number of n-tosses having a run of 4 or more heads for a fair coin (i.e., probability is a(n)/2^n).",
			"comment": [
				"a(n-1) is the number of compositions of n with at least one part \u003e= 5. - _Joerg Arndt_, Aug 06 2012"
			],
			"reference": [
				"W. Feller, An Introduction to Probability Theory and Its Applications, Vol. 1, 2nd ed. New York: Wiley, p. 300, 1968."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A050232/b050232.txt\"\u003eTable of n, a(n) for n = 1..300\u003c/a\u003e",
				"Simon Cowell, \u003ca href=\"http://arxiv.org/abs/1506.03580\"\u003eA Formula for the Reliability of a d-dimensional Consecutive-k-out-of-n:F System\u003c/a\u003e, arXiv preprint arXiv:1506.03580 [math.CO], 2015.",
				"T. Langley, J. Liese, and J. Remmel, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL14/Langley/langley2.html\"\u003eGenerating Functions for Wilf Equivalence Under Generalized Factor Order \u003c/a\u003e, J. Int. Seq. 14 (2011) # 11.4.2.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Run.html\"\u003eRun\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-1,-1,-1,-2)."
			],
			"formula": [
				"a(n) = 2^n - tetranacci(n+4), see A000078. - _Vladeta Jovovic_, Feb 23 2003",
				"G.f.: x^4/((1-2*x)*(1-x-x^2-x^3-x^4)). - _Geoffrey Critzer_, Jan 29 2009",
				"a(n) = 3*a(n-1)-a(n-2)-a(n-3)-a(n-4)-2*a(n-5). - _Wesley Ivan Hurt_, Apr 23 2021"
			],
			"mathematica": [
				"Flatten[With[{tetrnos=LinearRecurrence[{1,1,1,1},{0,1,1,2},50]},Table[ 2^n- Take[tetrnos,{n+3}],{n,40}]]] (* _Harvey P. Dale_, Dec 02 2011 *)",
				"LinearRecurrence[{3, -1, -1, -1, -2},{0, 0, 0, 1, 3},31] (* _Ray Chandler_, Aug 03 2015 *)"
			],
			"program": [
				"(Python)",
				"def a(n, adict={0:0, 1:0, 2:0, 3:1, 4:3}):",
				"    if n in adict:",
				"        return adict[n]",
				"    adict[n]=3*a(n-1) - a(n-2) - a(n-3) - a(n-4) - 2*a(n-5)",
				"    return adict[n] # _David Nacin_, Mar 07 2012",
				"(PARI) a(n)=([0,1,0,0,0; 0,0,1,0,0; 0,0,0,1,0; 0,0,0,0,1; -2,-1,-1,-1,3]^(n-1)*[0;0;0;1;3])[1,1] \\\\ _Charles R Greathouse IV_, Feb 09 2017"
			],
			"xref": [
				"Cf. A000078, A008466, A050231, A050233."
			],
			"keyword": "nonn,nice,easy",
			"offset": "1,5",
			"author": "_Eric W. Weisstein_",
			"references": 8,
			"revision": 58,
			"time": "2021-05-14T16:14:58-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}