{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A221073",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 221073,
			"data": "2,4,1,8,1,32,1,56,1,196,1,336,1,1152,1,1968,1,6724,1,11480,1,39200,1,66920,1,228484,1,390048,1,1331712,1,2273376,1,7761796,1,13250216,1,45239072,1,77227928,1,263672644,1,450117360,1,1536796800,1,2623476240,1",
			"name": "Simple continued fraction expansion of an infinite product.",
			"comment": [
				"Simple continued fraction expansion of product {n \u003e= 0} {1 - sqrt(m)*[sqrt(m) - sqrt(m-1)]^(4*n+3)}/{1 - sqrt(m)*[sqrt(m) - sqrt(m-1)]^(4*n+1)} at m = 2. For other cases see A221074 (m = 3), A221075 (m = 4) and A221076 (m = 5).",
				"If we denote the present sequence by [2; 4, 1, c(3), 1, c(4), 1, ...] then for k \u003e= 1 the sequence [1; c(2*k+1), 1, c(2*(2*k+1)), 1, c(3*(2*k+1)), 1, ...] gives the simple continued fraction expansion of product {n \u003e= 0} [1-sqrt(2)*{(sqrt(2)-1)^(2*k+1)}^(4*n+3)]/[1 - sqrt(2)*{(sqrt(2)-1)^(2*k+1)}^(4*n+1)]. An example is given below."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A221073/b221073.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"P. Bala, \u003ca href=\"/A174500/a174500_2.pdf\"\u003eSome simple continued fraction expansions for an infinite product, Part 1\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_10\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,1,0,6,0,-6,0,-1,0,1)."
			],
			"formula": [
				"a(2*n) = 1 for n \u003e= 1. For n \u003e= 1 we have",
				"a(4*n - 3) = (sqrt(2) + 1)^(2*n) + (sqrt(2) - 1)^(2*n) - 2;",
				"a(4*n - 1) = 1/sqrt(2)*{(sqrt(2) + 1)^(2*n + 1) + (sqrt(2) - 1)^(2*n + 1)} - 2.",
				"a(4*n - 3) = 4*A001108(n); a(4*n - 1) = 4*A053141(n).",
				"O.g.f.: 2 + x^2/(1 - x^2) + 4*x*(1 + x^2)^2/(1 - 7*x^4 + 7*x^8 - x^12) = 2 + 4*x + x^2 + 8*x^3 + x^4 + 32*x^5 + ....",
				"O.g.f.: (x^10-2*x^8-6*x^6+12*x^4-4*x^3+x^2-4*x-2) / ((x-1)*(x+1)*(x^4-2*x^2-1)*(x^4+2*x^2-1)). - _Colin Barker_, Jan 10 2014"
			],
			"example": [
				"Product {n \u003e= 0} {1 - sqrt(2)*(sqrt(2) - 1)^(4*n+3)}/{1 - sqrt(2)*(sqrt(2) - 1)^(4*n+1)} = 2.20409 39255 78752 05766 ...",
				"= 2 + 1/(4 + 1/(1 + 1/(8 + 1/(1 + 1/(32 + 1/(1 + 1/(56 + ...))))))).",
				"We have (sqrt(2) - 1)^3 = 5*sqrt(2) - 7 so product {n \u003e= 0} {1 - sqrt(2)*(5*sqrt(2) - 7)^(4*n+3)}/{1 - sqrt(2)*(5*sqrt(2) - 7)^(4*n+1)} = 1.11117 34981 94843 98511 ... = 1 + 1/(8 + 1/(1 + 1/(196 + 1/(1 + 1/(1968 + 1/(1 + 1/(39200 + ...)))))))."
			],
			"mathematica": [
				"NProduct[( Sqrt[2]*(Sqrt[2] - 1)^(4*n + 3) - 1)/( Sqrt[2]*(Sqrt[2] - 1)^(4*n + 1) - 1), {n, 0, Infinity}, WorkingPrecision -\u003e 200] // ContinuedFraction[#, 37] \u0026 (* _Jean-François Alcover_, Mar 06 2013 *)",
				"Join[{2},LinearRecurrence[{0,1,0,6,0,-6,0,-1,0,1},{4,1,8,1,32,1,56,1,196,1},60]] (* _Harvey P. Dale_, Feb 16 2014 *)"
			],
			"program": [
				"(PARI) x='x+O('x^30); Vec((x^10-2*x^8-6*x^6+12*x^4-4*x^3+x^2-4*x-2)/((x-1)*(x+1)*(x^4-2*x^2-1)*(x^4+2*x^2-1))) \\\\ _G. C. Greubel_, Jul 15 2018",
				"(MAGMA) m:=25; R\u003cx\u003e:=PowerSeriesRing(Integers(), m); Coefficients(R!((x^10-2*x^8-6*x^6+12*x^4-4*x^3+x^2-4*x-2)/((x-1)*(x+1)*(x^4-2*x^2-1)*(x^4+2*x^2-1)))); // _G. C. Greubel_, Jul 15 2018"
			],
			"xref": [
				"Cf. A001108, A053141, A174500, A221074 (m = 3), A221075 (m = 4), A221076 (m = 5)."
			],
			"keyword": "nonn,easy,cofr",
			"offset": "0,1",
			"author": "_Peter Bala_, Jan 06 2013",
			"ext": [
				"More terms from _Harvey P. Dale_, Feb 16 2014"
			],
			"references": 4,
			"revision": 29,
			"time": "2018-07-15T22:09:02-04:00",
			"created": "2013-01-06T14:28:11-05:00"
		}
	]
}