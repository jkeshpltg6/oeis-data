{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A098473",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 98473,
			"data": "1,1,2,1,4,6,1,6,18,20,1,8,36,80,70,1,10,60,200,350,252,1,12,90,400,1050,1512,924,1,14,126,700,2450,5292,6468,3432,1,16,168,1120,4900,14112,25872,27456,12870,1,18,216,1680,8820,31752,77616,123552,115830",
			"name": "Triangle T(n,k) read by rows, T(n, k) = binomial(2*k, k)*binomial(n, k), 0\u003c=k\u003c=n.",
			"comment": [
				"This sequence gives the coefficients of the Jensen polynomials (increasing powers of x) of degree n and shift 0 for the central binomial sequence A000984. For a definition of Jensen polynomials see a comment in A094436. - _Wolfdieter Lang_, Jun 25 2019"
			],
			"link": [
				"Indranil Ghosh, \u003ca href=\"/A098473/b098473.txt\"\u003eRows 0..125, flattened\u003c/a\u003e",
				"O. T. Dasbach, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v15i1n5\"\u003eA natural series for the natural logarithm\u003c/a\u003e, Electronic Journal of Combinatorics, (15) 2008 #N5."
			],
			"formula": [
				"T(n, k) = binomial(2*k, k)*binomial(n, k).",
				"Sum_{k=0..n} T(n,k)*x^(n-k) = A126869(n), A002426(n), A000984(n), A026375(n), A081671(n), A098409(n), A098410(n) for x = -2, -1, 0, 1, 2, 3, 4 respectively. - _Philippe Deléham_, Sep 28 2007",
				"From _Peter Bala_, Jun 06 2011: (Start)",
				"O.g.f.: 1/sqrt(1-t)*1/sqrt(1-t*(1+4*x)) = 1+(2*x+1)*t+(1+4*x+6*x^2)* t^2+...",
				"Let R_n(x) denote the row generating polynomials of this triangle, which begin",
				"R_1(x) = 1+2*x,",
				"R_2(x) = 1+4*x+6*x^2,",
				"R_3(x) = 1+6*x+18*x^2+20*x^3.",
				"[Dasbach] gives the following slowly converging series for the logarithm function:",
				"log(x)  = Sum_{n\u003e=1} 1/n*R_n(-1/x), valid for x \u003e= 4.",
				"The polynomials (1-x)^n*R_n(x/(1-x)) appear to be the row polynomials of A135091 (see also A117128). (End)"
			],
			"example": [
				"Rows begin",
				"1;",
				"1,  2;",
				"1,  4,  6;",
				"1,  6, 18,  20;",
				"1,  8, 36,  80,  70;",
				"1, 10, 60, 200, 350, 252;"
			],
			"maple": [
				"A098473 := proc(n,k) binomial(2*k,k)*binomial(n,k) ; end proc:"
			],
			"mathematica": [
				"Table[Binomial[2k,k]Binomial[n,k],{n,0,10},{k,0,n}]//Flatten (* _Harvey P. Dale_, Aug 15 2020 *)"
			],
			"program": [
				"(PARI): T(n,k)=binomial(2*k, k)*binomial(n, k);",
				"for(n=0,10,for(k=0,n,print1(T(n,k),\", \"));print()); /* as triangle */"
			],
			"xref": [
				"Row sums are A026375.",
				"Antidiagonal sums are A026569.",
				"Principal diagonal is A000984.",
				"Cf. A002426, A081671, A098409, A098410, A094436, A126869."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "0,3",
			"author": "_Paul Barry_, Sep 09 2004",
			"references": 5,
			"revision": 30,
			"time": "2020-08-15T17:31:57-04:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}