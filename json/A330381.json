{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A330381",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 330381,
			"data": "1,1,2,1,5,3,1,9,13,4,1,14,35,26,5,1,20,75,96,45,6,1,27,140,267,216,71,7,1,35,238,623,750,427,105,8,1,44,378,1288,2123,1800,770,148,9,1,54,570,2436,5211,6046,3858,1296,201,10,1,65,825,4302,11505,17303",
			"name": "Triangle read by rows: T(n,k) is the number of ternary strings of length n with k indispensable digits, with 0 \u003c= k \u003c= n.",
			"comment": [
				"A digit in a string is called indispensable, if it is greater than the following digit or equal to the following digits which are eventually greater than the following digit.  We also assume that there is an invisible digit 0 at the end of any string.  For example, in 7233355548, the digits 7, 5, 5, 5, and 8 are indispensable.",
				"T(n, k) is also the number of integers m where the length of the ternary representation of m is less than or equal to n and the digit sum of the ternary representation of 2m is 2k."
			],
			"link": [
				"J. Y. Choi, \u003ca href=\"https://doi.org/10.7546/nntdm.2019.25.2.40-48\"\u003eIndispensable digits for digit sums\u003c/a\u003e, Notes Number Theory Discrete Math 25 (2019), pp. 40-48.",
				"J. Y. Choi, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL22/Choi/choi15.html\"\u003eDigit sums generalizing binomial coefficients\u003c/a\u003e, J. Integer Seq. 22 (2019), Article 19.8.3."
			],
			"formula": [
				"T(n, k) = A027907(n, 2k-1) + A027907(n, 2k)."
			],
			"example": [
				"Triangle begins",
				"  1;",
				"  1,  2;",
				"  1,  5,  3;",
				"  1,  9, 13,  4;",
				"  1, 14, 35, 26,  5;",
				"  1, 20, 75, 96, 45, 6;",
				"  ...",
				"There is 1 string (00) of length 2 with 0 indispensable digits.",
				"There are 5 strings (01, 02, 10, 20, 12) of length 2 with 1 indispensable digit.",
				"There are 3 strings (11, 21, 22) of length 2 with 2 indispensable digits.",
				"Hence T(2, 0) = 1, T(2, 1) = 5, T(2, 2) = 3."
			],
			"mathematica": [
				"Table[Total@ Map[Sum[Binomial[n, i] Binomial[n - i, # - 2 i], {i, 0, n}] \u0026, 2 k + {-1, 0}], {n, 0, 10}, {k, 0, n}] // Flatten (* _Michael De Vlieger_, Dec 23 2019, after _Adi Dani_ at A027907 *)"
			],
			"program": [
				"(PARI) A027907(n, k) = if(n\u003c0, 0, polcoeff((1 + x + x^2)^n, k));",
				"T(n, k) = A027907(n, 2*k-1) + A027907(n, 2*k); \\\\ _Jinyuan Wang_, Dec 14 2019"
			],
			"xref": [
				"Cf. A027907, A330509."
			],
			"keyword": "nonn,base,tabl",
			"offset": "0,3",
			"author": "_Ji Young Choi_, Dec 12 2019",
			"ext": [
				"More terms from _Jinyuan Wang_, Dec 14 2019"
			],
			"references": 3,
			"revision": 40,
			"time": "2020-01-28T01:01:59-05:00",
			"created": "2020-01-28T01:01:59-05:00"
		}
	]
}