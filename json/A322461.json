{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A322461",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 322461,
			"data": "3,-8,54,-389,2834,-20673,150825,-1100401,8028410,-58574450,427353149,-3117924532,22748056061,-165967472679,1210881576595,-8834467193304,64455362190778,-470259679983109,3430966161717678,-25031975531635101,182630713764509309",
			"name": "Sum of n-th powers of the roots of x^3 + 8*x^2 + 5*x - 1.",
			"comment": [
				"Let A = cos(2*Pi/7), B = cos(4*Pi/7), C = cos(8*Pi/7).",
				"For integers h, k let",
				"   X = 2*sqrt(7)*A^(h+k+1)/(B^h*C^k),",
				"   Y = 2*sqrt(7)*B^(h+k+1)/(C^h*A^k),",
				"   Z = 2*sqrt(7)*C^(h+k+1)/(A^h*B^k).",
				"then X, Y, Z are the roots of a monic equation",
				"    t^3 + a*t^2 + b*t + c = 0",
				"where a, b, c are integers and c = 1 or -1.",
				"Then X^n + Y^n + Z^n, n = 0, 1, 2, ... is an integer sequence.",
				"This sequence has (h,k) = (0,1)."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A322461/b322461.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (-8,-5,1)."
			],
			"formula": [
				"a(n) = (2*sqrt(7)*A^2/C)^n + (2*sqrt(7)*B^2/A)^n + (2*sqrt(7)*C^2/B)^n, where A = cos(2*Pi/7), B = cos(4*Pi/7), C = cos(8*Pi/7).",
				"a(n) = -8*a(n-2) - 5*a(n-2) + a(n-3) for n \u003e 2.",
				"G.f.: (3 + x)*(1 + 5*x) / (1 + 8*x + 5*x^2 - x^3). - _Colin Barker_, Dec 09 2018"
			],
			"mathematica": [
				"LinearRecurrence[{-8, -5, 1}, {3, -8, 54}, 50] (* _Amiram Eldar_, Dec 09 2018 *)"
			],
			"program": [
				"(PARI) Vec((3 + x)*(1 + 5*x) / (1 + 8*x + 5*x^2 - x^3) + O(x^25)) \\\\ _Colin Barker_, Dec 09 2018",
				"(PARI) polsym(x^3 + 8*x^2 + 5*x - 1, 25) \\\\ _Joerg Arndt_, Dec 17 2018"
			],
			"xref": [
				"Similar sequences with (h,k) values: A094648 (0,0), A274075 (1,0)."
			],
			"keyword": "sign,easy",
			"offset": "0,1",
			"author": "_Kai Wang_, Dec 09 2018",
			"references": 1,
			"revision": 32,
			"time": "2019-01-13T07:46:33-05:00",
			"created": "2019-01-10T03:12:19-05:00"
		}
	]
}