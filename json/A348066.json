{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A348066",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 348066,
			"data": "1,2,3,2,5,6,3,2,3,10,11,6,13,6,15,2,5,6,3,10,3,22,5,6,5,26,3,6,29,30,31,2,33,10,15,6,13,6,39,10,41,6,13,22,15,10,47,6,3,10,15,26,3,6,55,6,3,58,11,30,13,62,3,2,65,66,5,10,15,30,29,6,5,26",
			"name": "Matula-Goebel tree number of tree n with all duplicate sibling subtrees reduced to one copy of each.",
			"comment": [
				"Here a subtree means a vertex and all its descendants (children, grandchildren, etc).",
				"Any subtree which is a duplicate of a sibling subtree is deleted and this is repeated until no duplicates exist.  The order of deletes does not affect the result.",
				"Or equivalently, duplicates among sibling subtrees are considered after each subtree has itself been reduced.",
				"The resulting tree a(n) has no duplicates, which means asymmetric so that a(n) is a term of A276625.  Reduction is no-change a(n) = n if and only if n is in A276625.",
				"This sequence is a divisibility sequence since the prime factors of a(m*n) are the union of the prime factors of a(m) and a(n), so that a(m*n) is divisible by a(n)."
			],
			"link": [
				"Kevin Ryde, \u003ca href=\"/A348066/b348066.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"\u003ca href=\"/index/Di#divseq\"\u003eIndex to divisibility sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Mat#matula\"\u003eIndex entries for sequences related to Matula-Goebel numbers\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Product of distinct prime(a(primepi(p))), over p the prime factors of n."
			],
			"example": [
				"In the following tree n=42, E and F are (singleton) subtrees of B and are the same so reduce to E alone.  With that change, subtrees A,D and B,E are the same, so reduce to A,D alone.",
				"  tree n=42    tree a(42) = 6",
				"      R___         R         root R",
				"     / \\  \\        | \\",
				"    A   B  C       A  C",
				"    |   |\\         |",
				"    D   E F        D"
			],
			"program": [
				"(PARI) a(n) = vecprod(Set([prime(self()(primepi(p))) | p\u003c-factor(n)[,1]]));"
			],
			"xref": [
				"Cf. A276625 (asymmetric trees), A007947."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Kevin Ryde_, Oct 01 2021",
			"references": 2,
			"revision": 11,
			"time": "2021-10-13T17:13:21-04:00",
			"created": "2021-10-07T14:14:24-04:00"
		}
	]
}