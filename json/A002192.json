{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002192",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2192,
			"id": "M0604 N0218",
			"data": "1,2,3,5,4,7,6,9,13,8,10,19,14,12,29,16,21,22,37,18,27,20,43,33,34,28,49,24,61,32,67,30,73,45,57,44,40,36,50,42,52,101,63,85,109,91,74,54",
			"name": "Least integer with A000203(a(n)) = A002191(n), where A002191 = range of the sum-of-divisors function A000203.",
			"comment": [
				"This is the least integer with the increasing sigma value A002191(n). For integers sorted on the ordered sigma values A007609(n), see A085790. - _Lekraj Beedassy_, Oct 08 2004",
				"The sigma function (A000203) can't have a left nor a right inverse since it is neither injective nor surjective. The first column of the table A085790 (undefined when the row length A054973(n) = 0 \u003c=\u003e no x has sigma(x) = n) or A051444 (which has zeros filled in for these undefined values) are right-inverse of sigma on A002191 = range of sigma: one has A000203(A051444(n)) = A000203(A085790(n,1)) = n for all n in A002191 \u003c=\u003e A054973(n) \u003e 0 \u003c=\u003e row A085790(n,.) nonempty \u003c=\u003e there is x with sigma(x) = n. Since sigma(6) = sigma(11) = 12, a hypothetical left inverse g must satisfy g(12) = 6 and g(12) = 11 which is impossible. Restricted to this list A002192 of smallest indices for the possible values of sigma, there exists a left inverse g such that g(sigma(x)) = x for all x in A002192. This equation defines the function g, i.e., g(A002191(n)) := a(n). A different left inverse exists on the set of largest pre-images for the possible values of sigma, {A085790(n,A054973(n)); n in A002191} = {1, 2, 3, 5, 4, 7, 11, 9, 13, 8, 17, 19, 23, 12, 29, 25, 31, 22, 37, 18, 27, 41, 43, ...}. - _M. F. Hasler_, Nov 21 2019"
			],
			"reference": [
				"J. W. L. Glaisher, Number-Divisor Tables. British Assoc. Math. Tables, Vol. 8, Camb. Univ. Press, 1940, p. 85.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A002192/b002192.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e"
			],
			"mathematica": [
				"m = 1000; Clear[f]; f[k_] := f[k] = Split[{DivisorSigma[1, #], #}\u0026 /@ Range[3k] // Sort, #1[[1]] == #2[[1]]\u0026][[1 ;; m, 1]][[All, 2]]; f[k = m]; f[k = k+m]; While[f[k] != f[k, m], k = k+m]; A002192 = f[k] (* _Jean-François Alcover_, Oct 15 2015 *)"
			],
			"xref": [
				"A051444 is a better version of this sequence. See also A000203, A007626."
			],
			"keyword": "nonn,nice,easy",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"references": 8,
			"revision": 30,
			"time": "2019-11-23T00:53:00-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}