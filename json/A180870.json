{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A180870",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 180870,
			"data": "1,2,1,4,2,-1,8,4,-4,-1,16,8,-12,-4,1,32,16,-32,-12,6,1,64,32,-80,-32,24,6,-1,128,64,-192,-80,80,24,-8,-1,256,128,-448,-192,240,80,-40,-8,1,512,256,-1024,-448,672,240,-160,-40,10,1",
			"name": "D(n,x) is the Dirichlet kernel = sin((n+1/2)x)/sin(x/2). This triangle gives in row n, n\u003e=0, the coefficients of descending powers of x of the  polynomial D(n, arccos(x)).",
			"comment": [
				"D(n,arccos(x)) = U(n,x) + U(n-1,x) where U(n,x) are the Chebyshev polynomials of the second kind. These polynomials arise naturally in the investigation of the integer triples (p, q, (pq+1)/(p+q)).",
				"Chebyshev polynomials of the fourth kind, usually denoted by W(n,x) (see, for example, Mason and Handscomb, Chapter 1, Definition 1.3). See A228565 for Chebyshev polynomials of the third kind. Cf. A155751. - _Peter Bala_, Jan 17 2014"
			],
			"reference": [
				"J. C. Mason and D. C. Handscomb, Chebyshev polynomials, Chapman and Hall/CRC 2002"
			],
			"link": [
				"Paul Barry, \u003ca href=\"https://arxiv.org/abs/1606.05077\"\u003eOn the Group of Almost-Riordan Arrays\u003c/a\u003e, arXiv preprint arXiv:1606.05077 [math.CO], 2016.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Dirichlet_kernel\"\u003eDirichlet kernel\u003c/a\u003e."
			],
			"formula": [
				"From _Peter Bala_, Jan 17 2014: (Start)",
				"O.g.f. (1 + t)/(1 - 2*x*t + t^2) = 1 + (2*x + 1)*t + (4*x^2 + 2*x - 1)*t^2 + ....",
				"Recurrence equation: W(0,x) = 1, W(1,x) = 2*x + 1 and W(n,x) = 2*x*W(n-1,x) - W(n-2,x) for n \u003e= 2.",
				"In terms of U(n,x), the Chebyshev polynomials of the second kind, we have W(n,x) = U(2*n,u) with u = sqrt((1 + x)/2). Also binomial(2*n,n)*W(n,x) = 2^(2*n)*Jacobi_P(n,1/2,-1/2,x). (End)",
				"Row sums : 2*n+1. - _Michel Marcus_, Jul 16 2014",
				"T(n,m) = [x^(n-m)](U(n, x) + U(n-1, x)) = [x^(n-m)] S(2*n, sqrt(2*(1+x))), n \u003e= m \u003e= 0, with U(n, x) = S(n, 2*x). The coefficient triangle of the Chebyshev S-polynomials is given in A049310. See the Peter Bala comments above. - _Wolfdieter Lang_, Jul 26 2014",
				"From _Wolfdieter Lang_, Jul 30 2014: (Start)",
				"O.g.f. for the row polynomials R(n,x) = sum(T(n,m)*x^m, m=0..n), obtained from the one given by Peter Bala above by row reversion: (1 + x*t)/(1 - 2*t + (x*t)^2).",
				"In analogy to A157751 one can derive a recurrence for the row polynomials R(n, x) = x^n*Dir(n,1/x) with Dir(n,x) = U(n,x) + U(n-1,x) using also negative arguments but only one recursive step: R(n,x) = (1+x)*R(n-1,-x) + R(n-1,x), n \u003e= 1, R(0,x) = 1 (R(-1,x) = -1/x). Proof: derive the o.g.f. and compare it with the known one.",
				"This entails the triangle recurrence T(n,m) = (1 + (-1)^m)* T(n-1,m) - (-1)^m*T(n-1,m-1), for n \u003e= m \u003e= 1 with T(n,m) = 0 if n \u003c m and T(n,0)= 2^n.",
				"(End)"
			],
			"example": [
				"The triangle T(n,m) begins:",
				"n\\m    0   1     2     3    4   5    6    7   8  9  10 ...",
				"0:     1",
				"1:     2   1",
				"2:     4   2    -1",
				"3:     8   4    -4    -1",
				"4:    16   8   -12    -4    1",
				"5:    32  16   -32   -12    6   1",
				"6:    64  32   -80   -32   24   6   -1",
				"7:   128  64  -192   -80   80  24   -8   -1",
				"8:   256 128  -448  -192  240  80  -40   -8   1",
				"9:   512 256 -1024  -448  672 240 -160  -40  10  1",
				"10: 1024 512 -2304 -1024 1792 672 -560 -160  60 10  -1",
				"... reformatted - _Wolfdieter Lang_, Jul 26 2014",
				"Recurrence: T(4,2) = (1 + 1)*T(3,2) - T(3,1) = 2*(-4) - 4 = -12. T(4,3) = 0*T(3,3) - (-1)*T(3,2) = T(3,2) = -4. - _Wolfdieter Lang_, Jul 30 2014"
			],
			"program": [
				"(PARI) row(n) = {if (n==0, return([1])); f = 2*x+1; for (k = 2, n, for (i = 1, (k-1)\\2 + 1, f += (-1)^(i+1)*(binomial(k-i, i-1)*(2*x)^(k-2*i+2) - 2*binomial(k-1-i, i-1)*(2*x)^(k-2*i)););); Vec(f);} \\\\ _Michel Marcus_, Jul 18 2014"
			],
			"xref": [
				"Cf. A008312, A028297, A157751, A228565, A049310, A244419 (row reversed triangle)."
			],
			"keyword": "easy,sign,tabl",
			"offset": "0,2",
			"author": "_Jonny Griffiths_, Sep 21 2010",
			"ext": [
				"Missing term in sequence corrected by _Paul Curtz_, Dec 31 2011",
				"Edited. Name reformulated, Wikipedia link added. - _Wolfdieter Lang_, Jul 26 2014"
			],
			"references": 5,
			"revision": 64,
			"time": "2017-05-15T16:05:21-04:00",
			"created": "2010-10-02T03:00:00-04:00"
		}
	]
}