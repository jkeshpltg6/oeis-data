{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A215272",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 215272,
			"data": "1,9,9,81,729,59049,43046721,2541865828329,109418989131512359209,278128389443693511257285776231761,30432527221704537086371993251530170531786747066637049",
			"name": "a(n) = a(n-1)*a(n-2) with a(0)=1, a(1)=9.",
			"comment": [
				"From _Peter Bala_, Nov 01 2013: (Start)",
				"Let phi = 1/2*(1 + sqrt(5)) denote the golden ratio A001622. This sequence is the simple continued fraction expansion of the constant c := 8*sum {n = 1..inf} 1/9^floor(n*phi) (= 64*sum {n = 1..inf} floor(n/phi)/9^n) = 0.90109 74122 99938 29901 ... = 1/(1 + 1/(9 + 1/(9 + 1/(81 + 1/(729 + 1/(59049 + 1/(43046721 + ...))))))). The constant c is known to be transcendental (see Adams and Davison 1977). Cf. A014565.",
				"Furthermore, for k = 0,1,2,... if we define the real number X(k) = sum {n \u003e= 1} 1/9^(n*Fibonacci(k) + Fibonacci(k+1)*floor(n*phi)) then the real number X(k+1)/X(k) has the simple continued fraction expansion [0; a(k+1), a(k+2), a(k+3), ...] (apply Bowman 1988, Corollary 1). (End)"
			],
			"link": [
				"Bruno Berselli, \u003ca href=\"/A215272/b215272.txt\"\u003eTable of n, a(n) for n = 0..15\u003c/a\u003e",
				"W. W. Adams and J. L. Davison, \u003ca href=\"http://www.jstor.org/stable/2041889\"\u003eA remarkable class of continued fractions\u003c/a\u003e, Proc. Amer. Math. Soc. 65 (1977), 194-198.",
				"P. G. Anderson, T. C. Brown, P. J.-S. Shiue, \u003ca href=\"http://people.math.sfu.ca/~vjungic/tbrown/tom-28.pdf\"\u003eA simple proof of a remarkable continued fraction identity\u003c/a\u003e, Proc. Amer. Math. Soc. 123 (1995), 2005-2009.",
				"D. Bowman, \u003ca href=\"http://www.fq.math.ca/Scanned/26-1/bowman.pdf\"\u003eA new generalization of Davison's theorem\u003c/a\u003e, Fib. Quart. Volume 26 (1988), 40-45"
			],
			"formula": [
				"a(n) = 9^Fibonacci(n)."
			],
			"maple": [
				"a:= n-\u003e 9^(\u003c\u003c1|1\u003e, \u003c1|0\u003e\u003e^n)[1, 2]:",
				"seq(a(n), n=0..12);  # _Alois P. Heinz_, Jun 17 2014"
			],
			"mathematica": [
				"RecurrenceTable[{a[0] == 1, a[1] == 9, a[n] == a[n - 1] a[n - 2]}, a[n], {n, 0, 15}]"
			],
			"program": [
				"(MAGMA) [9^Fibonacci(n): n in [0..10]];",
				"(PARI) a(n) = 9^fibonacci(n); \\\\ _Jinyuan Wang_, Apr 06 2019"
			],
			"xref": [
				"Cf. A000045, A000301, A010098-A010100, A214706, A214887, A215270, A215271, A014565.",
				"Column k=9 of A244003."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Bruno Berselli_, Aug 07 2012",
			"references": 9,
			"revision": 40,
			"time": "2019-04-06T10:56:35-04:00",
			"created": "2012-08-07T22:09:02-04:00"
		}
	]
}