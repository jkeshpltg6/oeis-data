{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A320503",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 320503,
			"data": "1,2,3,4,6,8,9,5,7,10,12,14,15,16,18,20,21,22,24,26,27,25,28,30,32,33,34,36,38,39,35,40,42,44,45,46,48,50,51,52,54,56,57,55,49,11,13,17,19,23,29,31,37,41,43,47,53,58,60,62,63,64,66,68,69,65,70",
			"name": "Lexicographically earliest sequence of distinct positive terms such that a(1) = 1, a(2) = 2, and for any n \u003e 2, the least prime factor of a(n) does not exceed the prime next to the least prime factor of a(n-1).",
			"comment": [
				"More formally, for any n \u003e 0, A055396(a(n+1)) \u003c= A055396(a(n)) + 1.",
				"This sequence is a variant of A320454.",
				"This sequence is a permutation of the natural numbers, with inverse A320504; the reasoning to prove it is similar to that of A320454.",
				"The prime numbers appear in ascending order as clusters in the sequence; the first prime clusters are:",
				"- 2 terms: a(2) = 2, a(3) = 3,",
				"- 2 terms: a(8) = 5, a(9) = 7,",
				"- 12 terms: a(46) = 11, ..., a(57) = 53,",
				"- 400 terms: a(2638) = 59, ..., a(3037) = 2861,",
				"- 552398 terms: a(9149634) = 2879, ..., a(9702031) = 8207707.",
				"The next prime cluster is expected to appear after the occurrence of the term 8207707^2.",
				"The first known fixed points are: 1, 2, 3, 4, 10, 58, 84367, 2934331."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A320503/b320503.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A320503/a320503.png\"\u003eScatterplot of the first 10000000 terms\u003c/a\u003e (with prime terms highlighted)",
				"Rémy Sigrist, \u003ca href=\"/A320503/a320503.txt\"\u003eC program for A320503\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"example": [
				"The first terms, alongside the least prime factor of a(n) and A055396(a(n)), are:",
				"  n   a(n)  lpf(a(n))  A055396(a(n))",
				"  --  ----  ---------  -------------",
				"   1     1        N/A              0",
				"   2     2          2              1",
				"   3     3          3              2",
				"   4     4          2              1",
				"   5     6          2              1",
				"   6     8          2              1",
				"   7     9          3              2",
				"   8     5          5              3",
				"   9     7          7              4",
				"  10    10          2              1",
				"  11    12          2              1",
				"  12    14          2              1",
				"  13    15          3              2",
				"  14    16          2              1",
				"  15    18          2              1"
			],
			"mathematica": [
				"Nest[Append[#, Block[{k = 3, p}, While[Nand[Set[p, FactorInteger[k][[1, 1]]] \u003c= NextPrime[#[[-1, -1]] ], FreeQ[#[[All, 1]], k ]], k++]; {k, p}]] \u0026, {{1, 1}, {2, 2}}, 65][[All, 1]] (* _Michael De Vlieger_, Oct 17 2018 *)"
			],
			"program": [
				"(C) See Links section."
			],
			"xref": [
				"Cf. A055396, A320454, A320504 (inverse)."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Rémy Sigrist_, Oct 13 2018",
			"references": 3,
			"revision": 17,
			"time": "2018-10-25T13:01:59-04:00",
			"created": "2018-10-17T21:47:04-04:00"
		}
	]
}