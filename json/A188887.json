{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A188887",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 188887,
			"data": "1,9,3,1,8,5,1,6,5,2,5,7,8,1,3,6,5,7,3,4,9,9,4,8,6,3,9,9,4,5,7,7,9,4,7,3,5,2,6,7,8,0,9,6,7,8,0,1,6,8,0,9,1,0,0,8,0,4,6,8,6,1,5,2,6,2,0,8,4,6,4,2,7,9,5,9,7,1,1,0,3,2,6,9,5,1,2,3,4,8,3,7,1,6,1,4,0,9,0,3,7,7,6,8,0,4,2,2,3,7,2,8,7,6,3,2,4,3,0,7,4,8,9,1,8,5,0,7,5,7",
			"name": "Decimal expansion of sqrt(2 + sqrt(3)).",
			"comment": [
				"Decimal expansion of the length/width ratio of a sqrt(2)-extension rectangle.  See A188640 for definitions of shape and r-extension rectangle.",
				"A sqrt(2)-extension rectangle matches the continued fraction [1,1,13,1,2,15,10,1,18,1,1,21,,...] for the shape L/W = sqrt(2 + sqrt(3)). This is analogous to the matching of a golden rectangle to the continued fraction [1,1,1,1,1,1,1,1,...].  Specifically, for the sqrt(2)-extension rectangle, 1 square is removed first, then 1 square, then 13 squares, then 1 square, ..., so that the original rectangle of shape sqrt(2 + sqrt(3)) is partitioned into an infinite collection of squares.",
				"sqrt(2 + sqrt(3)) is also the shape of the greater sqrt(6)-contraction rectangle; see A188738.",
				"This constant is also the length of the Steiner span of three vertices of a unit square. - _Jean-François Alcover_, May 22 2014",
				"It is also the larger positive coordinate of (symmetrical) intersection points created by x^2 + y^2 = 4 circle and y = 1/x hyperbola. The smaller coordinate is A101263. - _Leszek Lezniak_, Sep 18 2018",
				"Length of the shortest diagonal in a regular 12-gon with unit side. - _Mohammed Yaseen_, Nov 12 2020"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A188887/b188887.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Burkard Polster, \u003ca href=\"https://www.youtube.com/watch?v=D6AFxJdJYW4\"\u003eIrrational roots\u003c/a\u003e, Mathologer video (2018)."
			],
			"formula": [
				"Equals (sqrt(6) + sqrt(2))/2.",
				"Equals exp(asinh(cos(Pi/4))). - _Geoffrey Caveney_, Apr 23 2014",
				"Equals cos(Pi/4) + sqrt(1 + cos(Pi/4)^2). - _Geoffrey Caveney_, Apr 23 2014"
			],
			"example": [
				"1.9318516525781365734994863994577947352678..."
			],
			"mathematica": [
				"r = 2^(1/2); t = (r + (4 + r^2)^(1/2))/2; FullSimplify[t]",
				"N[t, 130]",
				"RealDigits[N[t, 130]][[1]]",
				"ContinuedFraction[t, 120]",
				"RealDigits[Sqrt[2 + Sqrt[3]], 10, 100][[1]] (* _G. C. Greubel_, Apr 10 2018 *)"
			],
			"program": [
				"(PARI) sqrt(2 + sqrt(3)) \\\\ _G. C. Greubel_, Apr 10 2018",
				"(MAGMA) Sqrt(2 + Sqrt(3)); // _G. C. Greubel_, Apr 10 2018"
			],
			"xref": [
				"Cf. A188640, A188888, A101263."
			],
			"keyword": "nonn,cons",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Apr 12 2011",
			"references": 6,
			"revision": 42,
			"time": "2020-11-17T11:06:33-05:00",
			"created": "2011-04-13T17:42:05-04:00"
		}
	]
}