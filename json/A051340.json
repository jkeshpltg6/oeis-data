{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A051340",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 51340,
			"data": "1,1,2,1,1,3,1,1,1,4,1,1,1,1,5,1,1,1,1,1,6,1,1,1,1,1,1,7,1,1,1,1,1,1,1,8,1,1,1,1,1,1,1,1,9,1,1,1,1,1,1,1,1,1,10,1,1,1,1,1,1,1,1,1,1,11,1,1,1,1,1,1,1,1,1,1,1,12,1,1,1,1,1,1,1,1,1,1,1,1,13,1,1,1,1,1,1",
			"name": "A simple 2-dimensional array, read by antidiagonals: T[i,j] = 1 for j\u003e0, T[i,0] = i+1; i,j = 0,1,2,3,...",
			"comment": [
				"Warning: contributions from Kimberling refer to an alternate version indexed by 1 instead of 0. Other contributors (Adamson in A125026/A130301/A130295) refer to this considering the upper right triangle set to zero, T[i,j]=0 for j\u003ei. - _M. F. Hasler_, Aug 15 2015",
				"From _Clark Kimberling_, Feb 05 2011: (Start)",
				"A member of the accumulation chain:",
				"... \u003c A051340 \u003c A141419 \u003c A185874 \u003c A185875 \u003c A185876 \u003c ...",
				"(See A144112 for the definition of accumulation array.)",
				"In the m-th accumulation array of A051340,",
				"row_1 = C(m,1) and column_1 = C(1,m+1), for m\u003e=0. (End)"
			],
			"link": [
				"Johann Cigler, \u003ca href=\"https://arxiv.org/abs/1611.05252\"\u003eSome elementary observations on Narayana polynomials and related topics\u003c/a\u003e, arXiv:1611.05252 [math.CO], 2016. See p. 24.",
				"A. V. Mikhalev and A. A. Nechaev, \u003ca href=\"http://dx.doi.org/10.1007/BF00047168\"\u003eLinear recurring sequences over modules\u003c/a\u003e, Acta Applic. Math., 42 (1996), 161-202."
			],
			"formula": [
				"For n\u003e0, a(n(n+3)/2)=n+1, and if k is not of the form n*(n+3)/2, then a(k)=1. - _Benoit Cloitre_, Oct 31 2002, corrected by _M. F. Hasler_, Aug 15 2015",
				"T(n,1)=n and T(n,k)=1 if k\u003e1, for n\u003e=1. - _Clark Kimberling_, Feb 05 2011"
			],
			"example": [
				"Northwest corner:",
				"1...1...1...1...1...1...1",
				"2...1...1...1...1...1...1",
				"3...1...1...1...1...1...1",
				"4...1...1...1...1...1...1",
				"5...1...1...1...1...1...1",
				"6...1...1...1...1...1...1",
				"The Mathematica code shows that the weight array of A051340 (i.e., the array of which A051340 is the accumulation array), has northwest corner",
				"1....0...0...0...0...0...0",
				"1...-1...0...0...0...0...0",
				"1...-1...0...0...0...0...0",
				"1...-1...0...0...0...0...0",
				"1...-1...0...0...0...0...0. - _Clark Kimberling_, Feb 05 2011"
			],
			"maple": [
				"A051340 := proc(n, k) if k=0 then n+1; else 1; end if; end proc: # _R. J. Mathar_, Jul 16 2015"
			],
			"mathematica": [
				"(* This program generates A051340, then its accumulation array A141419, then its weight array described under Example. *)",
				"f[n_,0]:=0; f[0,k_]:=0;  (* needed for the weight array *)",
				"f[n_,1]:=n; f[n_,k_]:=1/;k\u003e1;",
				"TableForm[Table[f[n,k],{n,1,10},{k,1,15}]] (* A051340 *)",
				"Table[f[n-k+1,k],{n,14},{k,n,1,-1}]//Flatten",
				"s[n_,k_]:=Sum[f[i,j],{i,1,n},{j,1,k}]; (* accumulation array of {f(n,k)} *)",
				"TableForm[Table[s[n,k],{n,1,10},{k,1,15}]] (* A141419 *)",
				"Table[s[n-k+1,k],{n,14},{k,n,1,-1}]//Flatten",
				"w[m_,n_]:=f[m,n]+f[m-1,n-1]-f[m,n-1]-f[m-1,n]/;Or[m\u003e0,n\u003e0];",
				"TableForm[Table[w[n,k],{n,1,10},{k,1,15}]] (* weight array *)",
				"Table[w[n-k+1,k],{n,14},{k,n,1,-1}]//Flatten (* _Clark Kimberling_, Feb 05 2011 *)",
				"f[n_] := Join[ Table[1, {n - 1}], {n}]; Array[ f, 14] // Flatten (* _Robert G. Wilson v_, Mar 04 2012 *)"
			],
			"xref": [
				"Cf. A144112, A141419, A185874, A185875, A185876."
			],
			"keyword": "easy,nice,nonn,tabl",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Edited by _M. F. Hasler_, Aug 15 2015"
			],
			"references": 29,
			"revision": 40,
			"time": "2017-06-23T22:56:20-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}