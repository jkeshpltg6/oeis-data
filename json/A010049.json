{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A010049",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 10049,
			"data": "0,1,1,3,5,10,18,33,59,105,185,324,564,977,1685,2895,4957,8462,14406,24465,41455,70101,118321,199368,335400,563425,945193,1583643,2650229,4430290,7398330,12342849,20573219,34262337,57013865,94800780,157517532,261545777",
			"name": "Second-order Fibonacci numbers.",
			"comment": [
				"Number of parts in all compositions of n+1 with no 1's. E.g. a(5)=10 because in the compositions of 6 with no part equal to 1, namely 6,4+2,3+3,2+4,2+2+2, the total number of parts is 10. - _Emeric Deutsch_, Dec 10 2003"
			],
			"reference": [
				"D. E. Knuth, The Art of Computer Programming. Addison-Wesley, Reading, MA, Vol. 1, p. 83.",
				"Cornelius Gerrit Lekkerkerker, Voorstelling van natuurlyke getallen door een som van getallen van Fibonacci, Simon Stevin, Vol. 29 (1952), pp. 190-195."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A010049/b010049.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Carlos A. Rico A., Ana Paula Chaves, \u003ca href=\"https://arxiv.org/abs/1903.07490\"\u003eDouble-Recurrence Fibonacci Numbers and Generalizations\u003c/a\u003e, arXiv:1903.07490 [math.NT], 2019.",
				"T. Amdeberhan, M. B. Can, M. Jensen, \u003ca href=\"http://arxiv.org/abs/1406.0432\"\u003eDivisors and specializations of Lucas polynomials\u003c/a\u003e, arXiv preprint arXiv:1406.0432 [math.CO], 2014.",
				"Mengmeng Liu, Andrew Yezhou Wang, \u003ca href=\"https://www.emis.de/journals/JIS/VOL23/Wang/wang61.html\"\u003eThe Number of Designated Parts in Compositions with Restricted Parts\u003c/a\u003e, J. Int. Seq., Vol. 23 (2020), Article 20.1.8.",
				"Jia Huang, \u003ca href=\"https://arxiv.org/abs/1812.11010\"\u003eCompositions with restricted parts\u003c/a\u003e, arXiv:1812.11010 [math.CO], 2018.",
				"L. Turban, \u003ca href=\"http://arxiv.org/abs/cond-mat/0011038\"\u003eLattice animals on a staircase and Fibonacci numbers\u003c/a\u003e, arXiv:cond-mat/0011038 [cond-mat.stat-mech], 2000; J.Phys. A 33 (2000) 2587-2595.",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,1,-2,-1)"
			],
			"formula": [
				"First differences of A001629.",
				"a(n) = ((2*n+3)*F(n) - n*F(n-1))/5, F(n)=A000045(n) (Fibonacci numbers) (Turban reference eq.(2.12)). - _Wolfdieter Lang_, May 03 2000",
				"G.f.: x*(1-x)/(1-x-x^2)^2 (Turban reference eq.(2.10)). - _Wolfdieter Lang_, May 03 2000",
				"Recurrence: a(0)=0, a(1)=1, a(2)=1, a(n+2) = a(n+1) + a(n) + F(n). - _Benoit Cloitre_, Sep 02 2002",
				"Set A(n) = a(n+1) + a(n-1), B(n) = a(n+1) - a(n-1). Then A(n+2) = A(n+1) + A(n) + Lucas(n) and B(n+2) = B(n+1) + B(n) + Fibonacci(n). The polynomials F_2(n,-x) = Sum_{k=0..n} C(n,k)*a(n-k)*(-x)^k appear to satisfy a Riemann hypothesis; their zeros appear to lie on the vertical line Re x = 1/2 in the complex plane. Compare with the polynomials F(n,-x) defined in A094440. For a similar conjecture for polynomials involving the second-order Lucas numbers see A134410. - _Peter Bala_, Oct 24 2007",
				"a(n) = -A001629(n+2) + 2*A001629(n+1) + A000045(n+1). - _R. J. Mathar_, Nov 16 2007",
				"Starting (1, 1, 3, 5, 10, ...), = row sums of triangle A135830. - _Gary W. Adamson_, Nov 30 2007",
				"a(n) = F(n) + Sum_{k=0..n-1} F(k)*F(n-1-k), where F = A000045. - _Reinhard Zumkeller_, Nov 01 2013",
				"a(n) = Sum_{k=0..n-1} (k+1)*binomial(n-k-1, k). - _Peter Luschny_, Nov 20 2013",
				"a(n) = Sum_{i=0..n-1} Sum_{j=0..i} F(j-1)*F(i-j-1), where F = A000045. - _Carlos A. Rico A._, Jul 14 2016",
				"a(n) = Sum_{k = F(n+1)..F(n+2)-1} A007895(k), where F(n) is the n-th Fibonacci number (Lekkerkerker, 1952). - _Amiram Eldar_, Jan 11 2020"
			],
			"maple": [
				"with(combinat): A010049 := proc(n) options remember; if n \u003c= 1 then n else A010049(n-1)+A010049(n-2)+fibonacci(n-2); fi; end;"
			],
			"mathematica": [
				"CoefficientList[Series[(z - z^2)/(z^2 + z - 1)^2, {z, 0, 100}], z] (* _Vladimir Joseph Stephan Orlovsky_, Jul 01 2011 *)",
				"CoefficientList[Series[x (1 - x) / (1 - x - x^2)^2, {x, 0, 60}], x] (* _Vincenzo Librandi_, Jun 11 2013 *)",
				"LinearRecurrence[{2, 1, -2, -1}, {0, 1, 1, 3}, 38] (* _Amiram Eldar_, Jan 11 2020 *)"
			],
			"program": [
				"(Haskell)",
				"a010049 n = a010049_list !! n",
				"a010049_list = uncurry c $ splitAt 1 a000045_list where",
				"   c us (v:vs) = (sum $ zipWith (*) us (1 : reverse us)) : c (v:us) vs",
				"-- _Reinhard Zumkeller_, Nov 01 2013",
				"(Sage)",
				"def A010049():",
				"    a, b, c, d = 0, 1, 1, 3",
				"    while True:",
				"        yield a",
				"        a, b, c, d = b, c, d, 2*(d-b)+c-a",
				"a = A010049(); [next(a) for i in range(38)]  # _Peter Luschny_, Nov 20 2013",
				"(PARI) a(n)=([0,1,0,0; 0,0,1,0; 0,0,0,1; -1,-2,1,2]^n*[0;1;1;3])[1,1] \\\\ _Charles R Greathouse IV_, Jul 20 2016",
				"(MAGMA) [((2*n+3)*Fibonacci(n)-n*Fibonacci(n-1))/5: n in [0..40]]; // _Vincenzo Librandi_, Dec 31 2018",
				"(GAP) a:=List([0..40],n-\u003eSum([0..n-1],k-\u003e(k+1)*Binomial(n-k-1,k)));; Print(a); # _Muniru A Asiru_, Dec 31 2018"
			],
			"xref": [
				"Partial sums of A006367.",
				"Cf. A000045, A001629, A007895, A023610, A029907, A094440, A134410, A135830."
			],
			"keyword": "nonn,easy",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Emeric Deutsch_, Dec 10 2003"
			],
			"references": 20,
			"revision": 98,
			"time": "2020-03-23T21:22:53-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}