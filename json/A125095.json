{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A125095",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 125095,
			"data": "1,-2,0,0,3,-2,0,0,2,-2,0,0,1,-4,0,0,4,0,0,0,2,-2,0,0,1,-4,0,0,4,-2,0,0,0,-2,0,0,2,-2,0,0,5,-2,0,0,2,0,0,0,2,-6,0,0,0,-2,0,0,2,0,0,0,3,-4,0,0,4,-2,0,0,2,-2,0,0,0,-2,0,0,6,0,0,0,0",
			"name": "Expansion of phi(-x) * psi(x^4) in powers of x where psi(), phi() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Number 45 of the 74 eta-quotients listed in Table I of Martin (1996). - _Michael Somos_, Mar 14 2012"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A125095/b125095.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Y. Martin, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9947-96-01743-6\"\u003eMultiplicative eta-quotients\u003c/a\u003e, Trans. Amer. Math. Soc. 348 (1996), no. 12, 4825-4856, see page 4852 Table I.",
				"Michael Somos, \u003ca href=\"/A030203/a030203.txt\"\u003eIndex to Yves Martin's list of 74 multiplicative eta-quotients and their A-numbers\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(-1/2) * (eta(q)^2 * eta(q^8)^2) / (eta(q^2) * eta(q^4)) in powers of q.",
				"Given g.f. A(x), then B(q) = q * A(q^2) satisfies 0 = f(B(q), B(q^2), B(q^3), B(q^6)) where f(u1, u2, u3, u6) = u1^2*u6 * (u1 + 3*u3) + 2 * u2^2*u3 * (u2 + 3*u6) - 3 * u3^2*u2 * (u1 + u3) - 6 * u6^2*u1 * (u2 + u6).",
				"a(n) = b(2*n + 1) where b() is multiplicative with b(2^e) = 0^e, b(p^e) = (e+1) * (-1)^e if p == 1, 3 (mod 8), b(p^e) = (1 + (-1)^e) / 2 if p == 5, 7 (mod 8).",
				"Euler transform of period 8 sequence [ -2, -1, -2, 0, -2, -1, -2, -2, ...].",
				"G.f.: (Sum_{k in Z} (-1)^k * x^k^2) * (Sum_{k\u003e=0} x^(2*k^2 + 2*k)).",
				"a(4*n + 2) = a(4*n + 3) = 0. a(n) = (-1)^n * A113411(n). a(4*n) = A112603(n). a(4*n + 1) = -2 * A033761(n)."
			],
			"example": [
				"G.f. = 1 - 2*x + 3*x^4 - 2*x^5 + 2*x^8 - 2*x^9 + x^12 - 4*x^13 + 4*x^16 + ...",
				"G.f. = q - 2*q^3 + 3*q^9 - 2*q^11 + 2*q^17 - 2*q^19 + q^25 - 4*q^27 + 4*q^33 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 0, 0, (-1)^n DivisorSum[ 2 n + 1, If[ Mod[#, 8] \u003e 3, -1, 1] \u0026]]; (* _Michael Somos_, Jul 09 2015 *)",
				"a[ n_] := SeriesCoefficient[ (QPochhammer[ x] QPochhammer[ x^8])^2 / (QPochhammer[ x^2] QPochhammer[ x^4]), {x, 0, n}]; (* _Michael Somos_, Jul 09 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, 0, (-1)^n * sumdiv( 2*n + 1, d, (-1)^(d%8\u003e3)))};",
				"(PARI) {a(n) = if( n\u003c0, 0, n = 2*n + 1; qfrep( [1, 0; 0, 8], n)[n] - qfrep( [3, 1; 1, 3], n)[n])};",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A)^2 * eta(x^8 + A)^2 / (eta(x^2 + A) * eta(x^4 + A)), n))}",
				"(PARI) {a(n) = if( n\u003c0, 0, n = 2*n + 1; sumdiv( n, d, kronecker(2, d) * kronecker( -4, n/d)))};"
			],
			"xref": [
				"Cf. A033761, A113411, A112603."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Nov 20 2006",
			"references": 3,
			"revision": 24,
			"time": "2021-03-12T22:24:44-05:00",
			"created": "2006-12-06T03:00:00-05:00"
		}
	]
}