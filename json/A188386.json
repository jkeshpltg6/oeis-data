{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A188386",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 188386,
			"data": "11,13,47,37,107,73,191,121,299,181,431,253,587,337,767,433,971,541,1199,661,1451,793,1727,937,2027,1093,2351,1261,2699,1441,3071,1633,3467,1837,3887,2053,4331,2281,4799,2521",
			"name": "Numerator(h(n+2)-h(n-1)), where h(n) is the n-th harmonic number sum(1/k, k=1..n).",
			"comment": [
				"Denominator is listed in A033931. A027446 appears to be divisible by a(n).",
				"The sequence lists also the largest odd divisors of 3*m^2-1 (A080663) for m\u003e1. In fact, for m even, the largest odd divisor is 3*m^2-1 itself; for m odd, the largest odd divisor is (3*m^2-1)/2. From this follows the second formula given in Formula field. [_Bruno Berselli_, Aug 27 2013]"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A188386/b188386.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,3,0,-3,0,1)"
			],
			"formula": [
				"a(n) = numerator ((3*n^2+6*n+2)/(n*(n+1)*(n+2))).",
				"a(n) = (3-(-1)^n)*(3*n^2+6*n+2)/4.",
				"a(2n+1) = A158463(n+1), a(2n) = A003154(n+1).",
				"G.f.: -x*(11+13*x+14*x^2-2*x^3-x^4+x^5) / ( (x-1)^3*(1+x)^3 ). - _R. J. Mathar_, Apr 09 2011",
				"a(n) = numerator of coefficient of x^3 in the Maclaurin expansion of sin(x)*exp((n+1)*x). [_Francesco Daddi_, Aug 04 2011]",
				"h(n+3) = 3/2+2*f(n)/((n+2)*(n+3)), where f(n)= sum((-1)^k*binomial(-3,k)/(n+1-k),k=0..n). [_Gary Detlefs_, Jul 17 2011]",
				"a(n) = A213998(n+2,2). - _Reinhard Zumkeller_, Jul 03 2012"
			],
			"maple": [
				"seq((3-(-1)^n)*(3*n^2+6*n+2)/4, n=1..100);"
			],
			"mathematica": [
				"Table[(3 - (-1)^n)*(3*n^2 + 6*n + 2)/4, {n, 40}] (* _Wesley Ivan Hurt_, Jan 29 2017 *)",
				"Numerator[#[[4]]-#[[1]]]\u0026/@Partition[HarmonicNumber[Range[0,50]],4,1] (* or *) LinearRecurrence[{0,3,0,-3,0,1},{11,13,47,37,107,73},50] (* _Harvey P. Dale_, Dec 31 2017 *)"
			],
			"program": [
				"(MAGMA) [Numerator((3*n^2+6*n+2)/((n*(n+1)*(n+2)))): n in [1..50]]; // _Vincenzo Librandi_, Mar 30 2011",
				"(Haskell)",
				"import Data.Ratio ((%), numerator)",
				"a188386 n = a188386_list !! (n-1)",
				"a188386_list = map numerator $ zipWith (-) (drop 3 hs) hs",
				"   where hs = 0 : scanl1 (+) (map (1 %) [1..])",
				"-- _Reinhard Zumkeller_, Jul 03 2012"
			],
			"xref": [
				"Cf. A033931, A027446, A003154, A158436, A001711."
			],
			"keyword": "nonn,easy,look",
			"offset": "1,1",
			"author": "_Gary Detlefs_, Mar 29 2011",
			"references": 7,
			"revision": 45,
			"time": "2017-12-31T16:59:46-05:00",
			"created": "2011-04-01T14:10:08-04:00"
		}
	]
}