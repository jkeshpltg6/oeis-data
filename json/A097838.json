{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A097838",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 97838,
			"data": "1,50,2549,129949,6624850,337737401,17217982601,877779375250,44749530155149,2281348258537349,116304011655249650,5929223246159194801,302274081542463685201,15410048935419488750450,785610221624851462587749",
			"name": "First differences of Chebyshev polynomials S(n,51) = A097836(n) with Diophantine property.",
			"comment": [
				"(7*b(n))^2 - 53*a(n)^2 = -4 with b(n)=A097837(n) give all positive solutions of this Pell equation."
			],
			"link": [
				"Indranil Ghosh, \u003ca href=\"/A097838/b097838.txt\"\u003eTable of n, a(n) for n = 0..584\u003c/a\u003e",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"Giovanni Lucca, \u003ca href=\"http://forumgeom.fau.edu/FG2019volume19/FG201902index.html\"\u003eInteger Sequences and Circle Chains Inside a Hyperbola\u003c/a\u003e, Forum Geometricorum (2019) Vol. 19, 11-16.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (51, -1).",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e"
			],
			"formula": [
				"a(n) = ((-1)^n)*S(2*n, 7*i) with the imaginary unit i and the S(n, x) = U(n, x/2) Chebyshev polynomials.",
				"G.f.: (1-x)/(1 - 51*x + x^2).",
				"a(n) = S(n, 51) - S(n-1, 51) = T(2*n+1, sqrt(53)/2)/(sqrt(53)/2), with S(n, x) = U(n, x/2) Chebyshev's polynomials of the second kind, A049310. S(-1, x)= 0 = U(-1, x) and T(n, x) Chebyshev's polynomials of the first kind, A053120.",
				"a(n) = 51*a(n-1) - a(n-2), a(0)=1, a(1)=50. - _Philippe Deléham_, Nov 18 2008"
			],
			"example": [
				"All positive solutions of Pell equation x^2 - 53*y^2 = -4 are (7=7*1,1), (364=7*52,50), (18557=7*2651,2549), (946043=7*135149,129949), ..."
			],
			"mathematica": [
				"LinearRecurrence[{51,-1}, {1,50}, 20] (* _G. C. Greubel_, Jan 13 2019 *)"
			],
			"program": [
				"(PARI) my(x='x+O('x^20)); Vec((1-x)/(1-51*x+x^2)) \\\\ _G. C. Greubel_, Jan 13 2019",
				"(MAGMA) m:=20; R\u003cx\u003e:=PowerSeriesRing(Integers(), m); Coefficients(R!( (1-x)/(1-51*x+x^2) )); // _G. C. Greubel_, Jan 13 2019",
				"(Sage) ((1-x)/(1-51*x+x^2)).series(x, 20).coefficients(x, sparse=False) # _G. C. Greubel_, Jan 13 2019",
				"(GAP) a:=[1,50];; for n in [3..20] do a[n]:=51*a[n-1]-a[n-2]; od; a; # _G. C. Greubel_, Jan 13 2019"
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Sep 10 2004",
			"references": 5,
			"revision": 31,
			"time": "2019-04-19T13:40:33-04:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}