{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A027380",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 27380,
			"data": "1,8,28,168,1008,6552,43596,299592,2096640,14913024,107370900,780903144,5726600880,42288908760,314146029564,2345624803704,17592184995840,132458812569720,1000799909722368,7585009898729256",
			"name": "Number of irreducible polynomials of degree n over GF(8); dimensions of free Lie algebras.",
			"comment": [
				"Number of Lyndon words with 8 letters. - _Joerg Arndt_, Jul 29 2014",
				"Number of aperiodic necklaces with n beads of 8 colors. - _Herbert Kociemba_, Nov 25 2016"
			],
			"reference": [
				"E. R. Berlekamp, Algebraic Coding Theory, McGraw-Hill, NY, 1968, p. 84.",
				"M. Lothaire, Combinatorics on Words. Addison-Wesley, Reading, MA, 1983, p. 79."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A027380/b027380.txt\"\u003eTable of n, a(n) for n = 0..1110\u003c/a\u003e (terms 0..200 from T. D. Noe)",
				"Y. Puri and T. Ward, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL4/WARD/short.html\"\u003eArithmetic and growth of periodic orbits\u003c/a\u003e, J. Integer Seqs., Vol. 4 (2001), #01.2.1.",
				"G. Viennot, \u003ca href=\"http://dx.doi.org/10.1007/BFb0067950\"\u003eAlgèbres de Lie Libres et Monoïdes Libres\u003c/a\u003e, Lecture Notes in Mathematics 691, Springer Verlag 1978.",
				"\u003ca href=\"/index/Lu#Lyndon\"\u003eIndex entries for sequences related to Lyndon words\u003c/a\u003e"
			],
			"formula": [
				"G.f.: k=8, 1 - Sum_{i\u003e=1} mu(i)*log(1 - k*x^i)/i. - _Herbert Kociemba_, Nov 25 2016",
				"a(n) = Sum_{d|n} mu(d)*8^(n/d)/n for n \u003e 0. - _Andrew Howroyd_, Oct 13 2017"
			],
			"example": [
				"G.f. = 1 + 8*x + 28*x^2 + 168*x^3 + 1008*x^4 + 6552*x^5 + 43596*x^6 + ..."
			],
			"maple": [
				"A027380 := proc(n)",
				"    local d;",
				"    if n = 0 then",
				"        1;",
				"    else",
				"        add( 8^(n/d)*numtheory[mobius](d),d=numtheory[divisors](n)) ;",
				"        %/n ;",
				"    end if;",
				"end proc: # _R. J. Mathar_, Jun 09 2016"
			],
			"mathematica": [
				"f[n_] := (1/n)*Sum[MoebiusMu[d]*8^(n/d), {d, Divisors[n]}]; f[0] = 1; Array[f, 20, 0] (* _Robert G. Wilson v_, Jul 28 2014 *)",
				"mx=40;f[x_,k_]:=1-Sum[MoebiusMu[i] Log[1-k*x^i]/i,{i,1,mx}];CoefficientList[Series[f[x,8],{x,0,mx}],x] (* _Herbert Kociemba_, Nov 25 2016 *)"
			],
			"program": [
				"(PARI) a(n) = if(n, sumdiv(n, d, moebius(d)*8^(n/d))/n, 1) \\\\ _Altug Alkan_, Dec 01 2015"
			],
			"xref": [
				"Column 8 of A074650.",
				"Cf. A001037, A001693."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 4,
			"revision": 46,
			"time": "2017-11-22T16:42:33-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}