{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A060312",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 60312,
			"data": "1,1,2,4,5,9,12,21,30,51,76,127,195,322,504,826,1309,2135,3410,5545,8900,14445,23256,37701,60813,98514,159094,257608,416325,673933,1089648,1763581,2852242,4615823,7466468,12082291,19546175,31628466",
			"name": "Number of distinct ways to tile a 2 X n rectangle with dominoes (solutions are identified if they are rotations or reflections of each other).",
			"comment": [
				"Same as A001224 except that there a(2)=2 not 1. - _N. J. A. Sloane_, Mar 30 2015"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A060312/b060312.txt\"\u003eTable of n, a(n) for n = 1..1001\u003c/a\u003e",
				"A. R. Ashrafi, J. Azarija, K. Fathalikhani, S. Klavzar, et al., \u003ca href=\"http://www.fmf.uni-lj.si/~klavzar/preprints/Fib-Luc-orbits-August-11-2014.pdf\"\u003eOrbits of Fibonacci and Lucas cubes, dihedral transformations, and asymmetric strings\u003c/a\u003e, 2014.",
				"R. J. Mathar, \u003ca href=\"http://arxiv.org/abs/1311.6135\"\u003ePaving rectangular regions with rectangular tiles, ...\u003c/a\u003e, arXiv:1311.6135 [math.CO], Table 9.",
				"W. E. Patten (proposer) and S. W. Golomb (solver), \u003ca href=\"http://www.jstor.org/stable/2312751\"\u003eProblem E1470\u003c/a\u003e, \"Covering a 2Xn rectangle with dominoes\", Amer. Math. Monthly, 69 (1962), 61-62.",
				"N. J. A. Sloane, \u003ca href=\"/A001224/a001224.png\"\u003eAnnotated scan of Monthly problem E1470 with illustration of a(4)=4 (Page 1)\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/A001224/a001224_1.png\"\u003eAnnotated scan of Monthly problem E1470 with illustration of a(4)=4 (Page 2)\u003c/a\u003e",
				"\u003ca href=\"/index/Do#domino\"\u003eIndex entries for sequences related to dominoes\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,2,-1,0,-1,-1)."
			],
			"formula": [
				"If F(n) is the n-th Fibonacci number, then a(2n) = (F(2n) + F(n+1))/2 and a(2n+1) = (F(2n+1) + F(n))/2 for n \u003e 1.",
				"G.f.: -x*(x^7 + x^6 + x^5 + 2*x^4 - x^3 + x^2 - 1) / ((x^2 + x - 1)*(x^4 + x^2 - 1)). - _Colin Barker_, Dec 15 2012"
			],
			"example": [
				"a(3)=2 because of the configurations |= and |||."
			],
			"maple": [
				"# Maple code for A060312 and A001224 from _N. J. A. Sloane_, Mar 30 2015",
				"with(combinat); F:=fibonacci;",
				"f:=proc(n) option remember;",
				"if n=2 then 1 # change this to 2 to get A001224",
				"elif (n mod 2) = 0 then (F(n+1)+F(n/2+2))/2;",
				"else (F(n+1)+F((n+1)/2))/2; fi; end;",
				"[seq(f(n),n=1..50)];"
			],
			"mathematica": [
				"CoefficientList[Series[-(x^7 + x^6 + x^5 + 2 x^4 - x^3 + x^2 - 1) / ((x^2 + x - 1) (x^4 + x^2 - 1)), {x, 0, 40}], x] (* _Vincenzo Librandi_, Nov 22 2014 *)"
			],
			"program": [
				"(MAGMA) [n eq 1 select 1 else (1/2)*(Fibonacci(n+2)+Fibonacci(Floor((n-(-1)^n)/2)+2)): n in [0..40]]; // _Vincenzo Librandi_, Nov 22 2014"
			],
			"xref": [
				"Essentially the same as A001224, which is the main entry for this sequence. Other versions of the sequence can be found in A068928 and A102526."
			],
			"keyword": "easy,nonn",
			"offset": "1,3",
			"author": "_Thomas Ward_, Mar 27 2001",
			"ext": [
				"Edited by _N. J. A. Sloane_, Mar 30 2015"
			],
			"references": 6,
			"revision": 30,
			"time": "2021-02-19T20:10:00-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}