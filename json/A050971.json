{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A050971",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 50971,
			"data": "1,2,8,24,384,240,46080,40320,2064384,725760,3715891200,159667200,392398110720,12454041600,1428329123020800,20922789888000,274239191619993600,711374856192000,1678343852714360832000",
			"name": "4*Denominator of S(n)/Pi^n, where S(n) = Sum_{k=-inf..+inf} ((4k+1)^(-n)).",
			"comment": [
				"Reduced denominators of the Favard constants."
			],
			"link": [
				"Theo Niessink, \u003ca href=\"/A050971/b050971.txt\"\u003eTable of n, a(n) for n = 1..200\u003c/a\u003e (uploaded again by _Georg Fischer_, Feb 20 2019)",
				"N. D. Elkies, \u003ca href=\"https://arxiv.org/abs/math/0101168\"\u003eOn the sums Sum((4k+1)^(-n),k,-inf,+inf)\u003c/a\u003e, arXiv:math/0101168 [math.CA], 2001-2003.",
				"N. D. Elkies, \u003ca href=\"http://www.jstor.org/stable/3647742\"\u003eOn the sums Sum_{k = -infinity .. infinity} (4k+1)^(-n)\u003c/a\u003e, Amer. Math. Monthly, 110 (No. 7, 2003), 561-573.",
				"Maths StackExchange, \u003ca href=\"https://math.stackexchange.com/questions/4112014/can-this-equation-be-written-in-terms-of-x\"\u003eCan this equation be written in terms of x?\u003c/a\u003e, Apr 22 2021.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/FavardConstants.html\"\u003eFavard Constants.\u003c/a\u003e"
			],
			"formula": [
				"There is a simple formula in terms of Euler and Bernoulli numbers."
			],
			"example": [
				"The first few values of S(n)/Pi^n are 1/4, 1/8, 1/32, 1/96, 5/1536, 1/960, ..."
			],
			"maple": [
				"S := proc(n, k) option remember; if k = 0 then `if`(n = 0, 1, 0) else",
				"S(n, k - 1) + S(n - 1, n - k) fi end: EZ := n -\u003e S(n, n)/(2^n*n!):",
				"A050971 := n -\u003e denom(EZ(n-1)): seq(A050971(n), n=1..19); # _Peter Luschny_, Aug 02 2017"
			],
			"mathematica": [
				"s[n_] := Sum[(4*k + 1)^(-n), {k, -Infinity, Infinity}]; a[n_] := 4*s[n]/Pi^n ; a[1] = 1; Table[a[n], {n, 1, 19}] // Denominator (* _Jean-François Alcover_, Nov 05 2012 *)",
				"a[n_] := 4*Sum[((-1)^k/(2*k+1))^n, {k, 0, Infinity}] /. Pi -\u003e 1 // Denominator; Table[a[n], {n, 1, 19}] (* _Jean-François Alcover_, Jun 20 2014 *)",
				"Table[4/(2 Pi)^n LerchPhi[(-1)^n, n, 1/2], {n, 21}] // Denominator (* _Eric W. Weisstein_, Aug 02 2017 *)",
				"Table[4/Pi^n If[Mod[n, 2] == 0, DirichletLambda, DirichletBeta][n], {n, 21}] // Denominator (* _Eric W. Weisstein_, Aug 02 2017 *)"
			],
			"xref": [
				"Cf. A068205, A050970 (numerators)."
			],
			"keyword": "nonn,frac",
			"offset": "1,2",
			"author": "_Eric W. Weisstein_",
			"ext": [
				"Entry revised by _N. J. A. Sloane_, Mar 24 2002"
			],
			"references": 12,
			"revision": 44,
			"time": "2021-05-05T13:51:22-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}