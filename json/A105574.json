{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A105574",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 105574,
			"data": "2,3,5,3,11,3,17,3,5,3,31,3,41,3,5,3,59,3,67,3,5,3,83,3,11,3,5,3,109,3,127,3,5,3,11,3,157,3,5,3,179,3,191,3,5,3,211,3,17,3,5,3,241,3,11,3,5,3,277,3,283,3,5,3,11,3,331,3,5,3",
			"name": "a(1) = 2; for n \u003e 1, a(n) is the prime whose index is the least prime factor of n.",
			"comment": [
				"Previous name: a(n) is the m-th prime number, where m is the smallest prime factor of n, a(1) = 2.",
				"Given that the smallest prime factor of 6k + {-4, -3, -2, -1, 0, 1} is (2, 3, 2, p, 2, q) where p, q \u003e= 5, p \u003c\u003e q, the sequence has from a(2) on the repeating pattern (3, 5, 3, prime(p), 3, prime(q)) of length 6, with prime(p), prime(q) \u003e= prime(5) = 11 and prime(p) \u003c\u003e prime(q). - _Bernard Schott_, Dec 09 2018, edited by _M. F. Hasler_, Dec 10 2018",
				"If n is in standard form and p is the smallest prime factor of n, then a(n) = prime(p) = A000040(p). - _Muniru A Asiru_, Jan 29 2019"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A105574/b105574.txt\"\u003eTable of n, a(n) for n = 1..16384\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A000040(A020639(n)). - _Antti Karttunen_, Nov 10 2018",
				"a(2*n) = 3. - _Muniru A Asiru_, Nov 10 2018"
			],
			"example": [
				"The smallest prime factor of 5 is 5. Hence a(5) is the 5th prime, which is 11.",
				"The smallest prime factor of 6 is 2. Therefore a(6) = 3."
			],
			"mathematica": [
				"Table[Prime[FactorInteger[n][[1, 1]]], {n, 2, 70}]"
			],
			"program": [
				"(PARI) g(n) = for(x=2,n,print1(prime(sdiv(x))\", \"))",
				"sdiv(n) = { local(x); x=ifactor(n); return(x[1]) } \\\\ The smallest prime divisor of n",
				"ifactor(n,m=0) = { local(f,j,k,flist); flist=[]; f=Vec(factor(n,m)); for(j=1,length(f[1]), for(k = 1,f[2][j],flist = concat(flist,f[1][j]) ); ); return(flist) } \\\\ The vector of the integer factors of n with multiplicity.",
				"(PARI)",
				"A020639(n) = if(1==n, n, factor(n)[1, 1]);",
				"A105574(n) = prime(A020639(n)); \\\\ _Antti Karttunen_, Nov 10 2018",
				"(GAP) P:=Filtered([1..350],IsPrime);; a:=List(List([1..Length(P)],Factors),i-\u003eP[i[1]]); # _Muniru A Asiru_, Nov 10 2018",
				"(MAGMA) [2] cat [NthPrime(Min(PrimeFactors(n))):n in[2..70]]; // _Vincenzo Librandi_, Dec 09 2018"
			],
			"xref": [
				"Cf. A000040, A020639, A105562."
			],
			"keyword": "nonn,less",
			"offset": "1,1",
			"author": "_Cino Hilliard_, May 03 2005",
			"ext": [
				"Edited by _Stefan Steinerberger_, Jul 25 2007",
				"Term a(1) = 2 prepended and offset corrected by _Antti Karttunen_, Nov 10 2018",
				"New name from _Michel Marcus_, Dec 09 2018"
			],
			"references": 2,
			"revision": 42,
			"time": "2019-01-29T02:08:37-05:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}