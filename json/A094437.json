{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A094437",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 94437,
			"data": "1,1,2,1,4,3,1,6,9,5,1,8,18,20,8,1,10,30,50,40,13,1,12,45,100,120,78,21,1,14,63,175,280,273,147,34,1,16,84,280,560,728,588,272,55,1,18,108,420,1008,1638,1764,1224,495,89,1,20,135,600,1680,3276,4410,4080,2475,890",
			"name": "Triangular array T(n,k) = Fibonacci(k+2)*C(n,k), k=0..n, n\u003e=0.",
			"comment": [
				"Let F(n) denote the n-th Fibonacci number (A000045). Then n-th row sum of T is F(2n+2) and n-th alternating row sum is -F(n-2).",
				"A094437 is jointly generated with A094436 as a triangular array of coefficients of polynomials v(n,x): initially, u(1,x)=v(1,x)=1; for n\u003e1, u(n,x)=u(n-1,x)+x*v(n-1)x and v(n,x)=x*u(n-1,x)+(x+1)*v(n-1,x).  See the Mathematica section. [_Clark Kimberling_, Feb 26 2012]",
				"Subtriangle of the triangle given by (1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, ...) DELTA (0, 2, -1/2, -1/2, 0, 0, 0, 0, 0, 0, 0, ...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, Apr 28 2012"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A094437/b094437.txt\"\u003eRows n = 0..100 of triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"From _Philippe Deléham_, Apr 28 2012: (Start)",
				"As DELTA-triangle T(n,k):",
				"G.f.: (1-x-y*x+2*y*x^2-y^2*x^2)/(1-2*x-y*x+x^2+y*x^2-y^2*x^2).",
				"T(n,k) = 2*T(n-1,k) + T(n-1,k-1) - T(n-2,k) - T(n-2,k-1) + T(n-2,k-2), T(0,0) = T(1,0) = T(2,0) = 1, T(2,1) = 2, T(1,1) = T(2,2) = 0 and T(n,k) = 0 if k\u003c0 or if k\u003en. (End)",
				"From _G. C. Greubel_, Oct 30 2019: (Start)",
				"T(n, k) = binomial(n, k)*Fibonacci(k+2).",
				"Sum_{k=0..n} T(n,k) = Fibonacci(2*n+2).",
				"Sum_{k=0..n} (-1)^(k+1) * T(n,k) = Fibonacci(n-2). (End)"
			],
			"example": [
				"First four rows:",
				"  1;",
				"  1 2;",
				"  1 4 3;",
				"  1 6 9 5;",
				"sum = 1+6+9+5=21=F(8); alt.sum = 1-6+9-5=-1=-F(1).",
				"T(3,2)=F(4)*C(3,2)=3*3=9.",
				"From _Philippe Deléham_, Apr 28 2012: (Start)",
				"(1, 0, 0, 1, 0, 0, ...) DELTA (0, 2, -1/2, -1/2, 0, 0, ...) begins :",
				"  1;",
				"  1, 0;",
				"  1, 2,  0;",
				"  1, 4,  3,  0;",
				"  1, 6,  9,  5, 0;",
				"  1, 8, 18, 20, 8, 0; . (End)"
			],
			"maple": [
				"with(combinat); seq(seq(fibonacci(k+2)*binomial(n,k), k=0..n), n=0..12); # _G. C. Greubel_, Oct 30 2019"
			],
			"mathematica": [
				"(* First program *)",
				"u[1, x_] := 1; v[1, x_] := 1; z = 13;",
				"u[n_, x_] := u[n - 1, x] + x*v[n - 1, x];",
				"v[n_, x_] := x*u[n - 1, x] + (x + 1)*v[n - 1, x];",
				"Table[Expand[u[n, x]], {n, 1, z/2}]",
				"Table[Expand[v[n, x]], {n, 1, z/2}]",
				"cu = Table[CoefficientList[u[n, x], x], {n, 1, z}];",
				"TableForm[cu]",
				"Flatten[%]  (* A094436 *)",
				"Table[Expand[v[n, x]], {n, 1, z}]",
				"cv = Table[CoefficientList[v[n, x], x], {n, 1, z}];",
				"TableForm[cv]",
				"Flatten[%]  (* A094437 *)",
				"(* Second program *)",
				"Table[Fibonacci[k+2]*Binomial[n, k], {n,0,12}, {k,0,n}]//Flatten (* _G. C. Greubel_, Oct 30 2019 *)"
			],
			"program": [
				"(PARI) T(n,k) = binomial(n,k)*fibonacci(k+2);",
				"for(n=0,12, for(k=0,n, print1(T(n,k), \", \"))) \\\\ _G. C. Greubel_, Oct 30 2019",
				"(MAGMA) [Binomial(n,k)*Fibonacci(k+2): k in [0..n], n in [0..12]]; // _G. C. Greubel_, Oct 30 2019",
				"(Sage) [[binomial(n,k)*fibonacci(k+2) for k in (0..n)] for n in (0..12)] # _G. C. Greubel_, Oct 30 2019",
				"(GAP) Flat(List([0..12], n-\u003e List([0..n], k-\u003e Binomial(n,k)*Fibonacci(k+2) ))); # _G. C. Greubel_, Oct 30 2019"
			],
			"xref": [
				"Cf. A000045.",
				"Cf. A094435, A094436, A094438, A094439, A094440, A094441, A094442, A094443, A094444."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "0,3",
			"author": "_Clark Kimberling_, May 03 2004",
			"references": 11,
			"revision": 24,
			"time": "2019-10-30T20:54:40-04:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}