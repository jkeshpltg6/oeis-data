{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A300356",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 300356,
			"data": "0,1,2,1,1,1,2,1,2,1,2,3,1,1,3,1,3,4,2,2,4,1,2,1,1,2,4,3,1,1,2,1,6,2,2,2,5,1,4,1,2,6,3,3,3,1,2,3,4,3,3,2,4,2,2,1,7,3,1,4,1,2,8,1,3,7,3,4,6,3,4,4,6,5,3,2,4,3,1,2",
			"name": "Number of ways to write n^2 as x^2 + y^2 + z^2 + w^2 with x \u003e= y \u003e= 0 \u003c= z \u003c= w such that x + 63*y = 2^(2k+1) for some nonnegative integer k.",
			"comment": [
				"Conjecture: (i) a(n) \u003e 0 for all n \u003e 1, and a(n) = 1 only for n = 5, 13, 25, 29, 59, 61, 91, 95, 101, 103, 211, 247, 2^k (k = 1,2,...), 4^k*79 (k = 0,1,2,...), 2^(2k+1)*m (k = 0,1,2,... and m = 3, 5, 7, 11, 15, 19, 23).",
				"(ii) Let r be 0 or 1, and let n \u003e r. Then n^2 can be written as x^2 + y^2 + z^2 + w^2 with x,y,z,w nonnegative integers such that x + 15*y = 2^(2k+r) for some k = 0,1,2,.... Also, we can write n^2 as x^2 + y^2 + z^2 + w^2 with x,y,z,w nonnegative integers such that 16*x - 15*y = 2^(2k+r) for some k = 0,1,2,....",
				"We have verified that a(n) \u003e 0 for all n = 2..10^7.",
				"See also A299924 and A300219 for similar conjectures."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A300356/b300356.txt\"\u003eTable of n, a(n) for n = 1..6000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2016.11.008\"\u003eRefining Lagrange's four-square theorem\u003c/a\u003e, J. Number Theory 175(2017), 167-190.",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1701.05868\"\u003eRestricted sums of four squares\u003c/a\u003e, arXiv:1701.05868 [math.NT], 2017-2018."
			],
			"example": [
				"a(5) = 1 sine 5^2 = 2^2 + 2^2 + 1^2 + 4^2 with 2 + 63*2 = 2^7.",
				"a(6) = 1 since 6^2 = 2^2 + 0^2 + 4^2 + 4^2 with 2 + 63*0 = 2^1.",
				"a(10) = 1 since 10^2 = 8^2 + 0^2 + 0^2 + 6^2 with 8 + 63*0 = 2^3.",
				"a(13) = 1 since 13^2 = 8^2 + 8^2 + 4^2 + 5^2 with 8 + 63*8 = 2^9.",
				"a(59) = 1 since 59^2 = 32^2 + 32^2 + 8^2 + 37^2 with 32 + 63*32 = 2^11.",
				"a(85) = 2 since 85^2 = 32^2 + 0^2 + 24^2 + 75^2 = 32^2 + 0^2 + 51^2 + 60^2 with 32 + 63*0 = 2^5.",
				"a(86) = 3 since 86^2 = 65^2 + 1^2 + 19^2 + 53^2 = 65^2 + 1^2 + 31^2 + 47^2 = 71^2 + 7^2 + 25^2 + 41^2 with 65 + 63*1 = 2^7 and 71 + 63*7 = 2^9.",
				"a(247) = 1 since 247^2 = 2^2 + 2^2 + 76^2 + 235^2 with 2 + 63*2 = 2^7."
			],
			"mathematica": [
				"SQ[n_]:=SQ[n]=IntegerQ[Sqrt[n]]",
				"Pow[n_]:=Pow[n]=IntegerQ[Log[4,n]]",
				"tab={};Do[r=0;Do[If[SQ[n^2-x^2-y^2-z^2]\u0026\u0026Pow[(x+63y)/2],r=r+1],{x,0,n},{y,0,Min[x,Sqrt[n^2-x^2]]},{z,0,Sqrt[(n^2-x^2-y^2)/2]}];tab=Append[tab,r],{n,1,80}];Print[tab]"
			],
			"xref": [
				"Cf. A000290, A000118, A271518, A279612, A281976, A299924, A300219."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Zhi-Wei Sun_, Mar 03 2018",
			"references": 4,
			"revision": 14,
			"time": "2018-03-03T12:43:06-05:00",
			"created": "2018-03-03T12:43:06-05:00"
		}
	]
}