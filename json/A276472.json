{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A276472",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 276472,
			"data": "1,1,2,4,3,5,11,7,8,13,29,18,15,21,34,76,47,33,36,55,89,199,123,80,69,91,144,233,521,322,203,149,160,235,377,610,1364,843,525,352,309,395,612,987,1597,3571,2207,1368,877,661,704,1007,1599,2584,4181",
			"name": "Modified Pascal's triangle read by rows: T(n,k) = T(n-1,k) + T(n-1,k-1), 1\u003ck\u003cn. T(n,1) = T(n-1,1) + T(n,2), n\u003e2. T(n,n) = T(n,n-1) + T(n-1,n-1), n\u003e1. T(1,1) = 1, T(2,1) = 1. n\u003e=1.",
			"comment": [
				"The recurrence relations for the border terms are the only way in which this differs from Pascal's triangle.",
				"Column T(2n,n+1) appears to be divisible by 4 for n\u003e=2; T(2n-1,n) divisible by 3 for n\u003e=2; T(2n,n-2) divisible by 2 for n\u003e=3.",
				"The symmetry of T(n,k) can be observed in a hexagonal arrangement (see the links).",
				"Consider T(n,k) mod 3 = q. Terms with q = 0 show reflection symmetry with respect to the central column T(2n-1,n), while q = 1 and q = 2 are mirror images of each other (see the link)."
			],
			"link": [
				"Yuriy Sibirmovsky, \u003ca href=\"/A276472/b276472.txt\"\u003eT(n,k), read by rows as a linear sequence a(j) for j = 1..5050\u003c/a\u003e",
				"Yuriy Sibirmovsky, \u003ca href=\"/A276472/a276472.jpg\"\u003eSymmetrical hexagonal arrangement for initial terms of T(n,k)\u003c/a\u003e",
				"Yuriy Sibirmovsky, \u003ca href=\"/A276472/a276472.png\"\u003eT(n,k) compared with Pascal's triangle\u003c/a\u003e",
				"Yuriy Sibirmovsky, \u003ca href=\"/A276472/a276472_1.png\"\u003eIllustration for T(n,k) mod 3\u003c/a\u003e"
			],
			"formula": [
				"Conjectures:",
				"Relations with other sequences:",
				"T(n+1,1) = A002878(n-1), n\u003e=1.",
				"T(n,n) = A001519(n) = A122367(n-1), n\u003e=1.",
				"T(n+1,2) = A005248(n-1), n\u003e=1.",
				"T(n+1,n) = A001906(n) = A088305(n), n\u003e=1.",
				"T(2n-1,n) = 3*A054441(n-1), n\u003e=2. [the central column].",
				"Sum_{k=1..n} T(n,k) = 3*A105693(n-1), n\u003e=2. [row sums].",
				"Sum_{k=1..n} T(n,k)-T(n,1)-T(n,n) = 3*A258109(n), n\u003e=2.",
				"T(2n,n+1) - T(2n,n) = A026671(n), n\u003e=1.",
				"T(2n,n-1) - T(2n,n) = 2*A026726(n-1), n\u003e=2.",
				"T(n,ceiling(n/2)) - T(n-1,floor(n/2)) = 2*A026732(n-3), n\u003e=3.",
				"T(2n+1,2n) = 3*A004187(n), n\u003e=1.",
				"T(2n+1,2) = 3*A049685(n-1), n\u003e=1.",
				"T(2n+1,2n) + T(2n+1,2) = 3*A033891(n-1), n\u003e=1.",
				"T(2n+1,3) = 5*A206351(n), n\u003e=1.",
				"T(2n+1,2n)/3 - T(2n+1,3)/5 = 4*A092521(n-1), n\u003e=2.",
				"T(2n,1) = 1 + 5*A081018(n-1), n\u003e=1.",
				"T(2n,2) = 2 + 5*A049684(n-1), n\u003e=1.",
				"T(2n+1,2) = 3 + 5*A058038(n-1), n\u003e=1.",
				"T(2n,3) = 3 + 5*A081016(n-2), n\u003e=2.",
				"T(2n+1,1) = 4 + 5*A003482(n-1), n\u003e=1.",
				"T(3n,1) = 4*A049629(n-1), n\u003e=1.",
				"T(3n,1) = 4 + 8*A119032(n), n\u003e=1.",
				"T(3n+1,3) = 8*A133273(n), n\u003e=1.",
				"T(3n+2,3n+2) = 2 + 32*A049664(n), n\u003e=1.",
				"T(3n,3n-2) = 4 + 32*A049664(n-1), n\u003e=1.",
				"T(3n+2,2) = 2 + 16*A049683(n), n\u003e=1.",
				"T(3n+2,2) = 2*A023039(n), n\u003e=1.",
				"T(2n-1,2n-1) = A033889(n-1), n\u003e=1.",
				"T(3n-1,3n-1) = 2*A007805(n-1), n\u003e=1.",
				"T(5n-1,1) = 11*A097842(n-1), n\u003e=1.",
				"T(4n+5,3) - T(4n+1,3) = 15*A000045(8n+1), n\u003e=1.",
				"T(5n+4,3) - T(5n-1,3) = 11*A000204(10n-2), n\u003e=1.",
				"Relations between left and right sides:",
				"T(n,1) = T(n,n) - T(n-2,n-2), n\u003e=3.",
				"T(n,2) = T(n,n-1) - T(n-2,n-3), n\u003e=4.",
				"T(n,1) + T(n,n) = 3*T(n,n-1), n\u003e=2."
			],
			"example": [
				"Triangle T(n,k) begins:",
				"n\\k 1    2    3    4   5    6    7    8    9",
				"1   1",
				"2   1    2",
				"3   4    3    5",
				"4   11   7    8    13",
				"5   29   18   15   21   34",
				"6   76   47   33   36   55   89",
				"7   199  123  80   69   91   144 233",
				"8   521  322  203  149  160  235 377  610",
				"9   1364 843  525  352  309  395 612  987  1597",
				"...",
				"In another format:",
				"__________________1__________________",
				"_______________1_____2_______________",
				"____________4_____3_____5____________",
				"________11_____7_____8_____13________",
				"____29_____18_____15____21_____34____",
				"_76_____47____33_____36____55_____89_"
			],
			"mathematica": [
				"Nm=12;",
				"T=Table[0,{n,1,Nm},{k,1,n}];",
				"T[[1,1]]=1;",
				"T[[2,1]]=1;",
				"T[[2,2]]=2;",
				"Do[T[[n,1]]=T[[n-1,1]]+T[[n,2]];",
				"T[[n,n]]=T[[n-1,n-1]]+T[[n,n-1]];",
				"If[k!=1\u0026\u0026k!=n,T[[n,k]]=T[[n-1,k]]+T[[n-1,k-1]]],{n,3,Nm},{k,1,n}];",
				"{Row[#,\"\\t\"]}\u0026/@T//Grid"
			],
			"program": [
				"(PARI) T(n,k) = if (k==1, if (n==1, 1, if (n==2, 1, T(n-1,1) + T(n,2))), if (k\u003cn,  T(n-1,k) + T(n-1,k-1), if (k==n,  T(n,n-1) + T(n-1,n-1), 0)));",
				"tabl(nn) = for (n=1, nn, for (k=1, n, print1(T(n, k), \", \");); print()); \\\\ _Michel Marcus_, Sep 14 2016"
			],
			"xref": [
				"Cf. A000045, A000204, A001519, A001906, A002878, A005248, A054441, A088305, A122367, A258109, A026671, A026726, A026732, A004187, A049685, A033891, A206351, A092521, A081018, A049684, A058038, A081016, A003482, A049629, A133273, A049664, A049683, A119032, A023039, A007805, A033889."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Yuriy Sibirmovsky_, Sep 12 2016",
			"references": 2,
			"revision": 58,
			"time": "2016-09-17T12:16:30-04:00",
			"created": "2016-09-17T12:16:30-04:00"
		}
	]
}