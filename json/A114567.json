{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A114567",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 114567,
			"data": "1,3,1,5,1,5,1,7,1,3,1,7,1,7,1,9,1,3,1,7,1,7,1,9,1,3,1,9,1,9,1,11,1,3,1,5,1,5,1,9,1,3,1,9,1,9,1,11,1,3,1,9,1,9,1,11,1,3,1,11,1,11,1,13,1,3,1,5,1,5,1,9,1,3,1,9,1,9,1,11,1,3,1,9,1,9,1,11,1,3,1,11,1,11,1,13,1,3,1,5,1",
			"name": "Numbers k such that the binary expansion of n mod 2^k is the postorder traversal of a binary tree, where 1 indicates a node and 0 indicates there are no children on that side.",
			"comment": [
				"Postorder traversals of a binary tree form an instantaneous code; any integer has a unique decomposition into codewords. To get the first codeword, find a(n). Then set n' = floor(n/2^(a(n))), find a(n'), and so on."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A114567/b114567.txt\"\u003eTable of n, a(n) for n = 0..65537\u003c/a\u003e",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 1, if n is even, and a(n) = 1 + a(floor(n/2)) + a(floor(n/2^{a(floor(n/2)) + 1})), if n is odd."
			],
			"example": [
				"a(37) = 1 + a(floor(37/2)) + a(floor(37/2^{a(floor(37/2)) + 1}))",
				"= 1 + a(18) + a(floor(37/2^{a(18) + 1}))",
				"= 1 + 1 + a(floor(37/2^{1 + 1}))",
				"= 2 + a(9)",
				"= 2 + 1 + a(floor(9/2)) + a(floor(9/2^{a(floor(9/2)) + 1}))",
				"= 3 + a(4) + a(floor[9/2^{a(4) + 1}))",
				"= 3 + 1 + a(floor(9/4))",
				"= 4 + a(2)",
				"= 5.",
				"37 mod 2^5 = 5 = 00101, which is the postorder traversal of the binary tree with a root node and a single left child."
			],
			"maple": [
				"a := proc(n) option remember; if 0 = n mod 2 then 1; else 1 + a(floor(n/2)) + a(floor(n/2^(a(floor(n/2)) + 1))); end if; end proc; # _Petros Hadjicostas_, Nov 20 2019"
			],
			"mathematica": [
				"a[n_] := a[n] = If[EvenQ[n], 1, 1 + a[Floor[n/2]] + a[ Floor[ n/2^( a[Floor[n/2]] + 1)]]]; a /@ Range[0, 100] (* _Giovanni Resta_, Nov 21 2019 *)"
			],
			"program": [
				"(PARI) A114567(n) = if(!(n%2),1,1+A114567(n\\2) + A114567(n\u003e\u003e(1+A114567(n\\2)))); \\\\ _Antti Karttunen_, Mar 30 2021, after the Maple-program"
			],
			"keyword": "nonn,base,easy,look",
			"offset": "0,2",
			"author": "_Mike Stay_, Feb 15 2006",
			"references": 1,
			"revision": 21,
			"time": "2021-03-30T18:43:30-04:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}