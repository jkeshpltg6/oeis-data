{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A303913",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 303913,
			"data": "1,1,1,1,1,0,1,1,0,0,1,1,0,1,0,1,1,0,3,2,0,1,1,0,6,10,8,0,1,1,0,10,28,54,18,0,1,1,0,15,60,193,222,61,0,1,1,0,21,110,505,1140,1107,170,0,1,1,0,28,182,1095,3876,7688,5346,538,0,1,1,0,36,280,2093,10326,33125,52364,27399,1654,0",
			"name": "Array read by antidiagonals: T(n,k) is the number of (planar) unlabeled asymmetric k-ary cacti having n polygons.",
			"comment": [
				"A k-ary cactus is a planar k-gonal cactus with vertices on each polygon numbered 1..k counterclockwise with shared vertices having the same number. In total there are always exactly k ways to number a given cactus since all polygons are connected. See the reference for a precise definition. - _Andrew Howroyd_, Feb 18 2020"
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A303913/b303913.txt\"\u003eTable of n, a(n) for n = 0..1274\u003c/a\u003e",
				"Miklos Bona, Michel Bousquet, Gilbert Labelle, Pierre Leroux, \u003ca href=\"https://arxiv.org/abs/math/9804119\"\u003eEnumeration of m-ary cacti\u003c/a\u003e, arXiv:math/9804119 [math.CO], 1998-1999.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Cactus_graph\"\u003eCactus graph\u003c/a\u003e",
				"\u003ca href=\"/index/Ca#cacti\"\u003eIndex entries for sequences related to cacti\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = (Sum_{d|n} mu(n/d)*binomial(k*d, d))/n - (k-1)*binomial(k*n, n)/((k-1)*n+1)) for n \u003e 0."
			],
			"example": [
				"Array begins:",
				"===============================================================",
				"n\\k| 1   2     3      4       5        6        7         8",
				"---+-----------------------------------------------------------",
				"0  | 1   1     1      1       1        1        1         1 ...",
				"1  | 1   1     1      1       1        1        1         1 ...",
				"2  | 0   0     0      0       0        0        0         0 ...",
				"3  | 0   1     3      6      10       15       21        28 ...",
				"4  | 0   2    10     28      60      110      182       280 ...",
				"5  | 0   8    54    193     505     1095     2093      3654 ...",
				"6  | 0  18   222   1140    3876    10326    23394     47208 ...",
				"7  | 0  61  1107   7688   33125   107056   285383    662620 ...",
				"8  | 0 170  5346  52364  290700  1149126  3621150   9702008 ...",
				"9  | 0 538 27399 373560 2661100 12845166 47813367 147765409 ...",
				"..."
			],
			"mathematica": [
				"T[0, _] = 1;",
				"T[n_, k_] := DivisorSum[n, MoebiusMu[n/#] Binomial[k #, #] \u0026]/n - (k-1) Binomial[n k, n]/((k-1) n + 1);",
				"Table[T[n-k, k], {n, 0, 12}, {k, n, 1, -1}] // Flatten (* _Jean-François Alcover_, May 22 2018 *)"
			],
			"program": [
				"(PARI) T(n,k)={if(n==0, 1, sumdiv(n, d, moebius(n/d)*binomial(k*d, d))/n - (k-1)*binomial(k*n, n)/((k-1)*n+1))}"
			],
			"xref": [
				"Columns k=2..7 are A054358, A054422, A052395, A054364, A054367, A054370.",
				"Cf. A303694, A303912."
			],
			"keyword": "nonn,tabl",
			"offset": "0,19",
			"author": "_Andrew Howroyd_, May 02 2018",
			"references": 8,
			"revision": 16,
			"time": "2020-02-18T19:22:59-05:00",
			"created": "2018-05-02T15:20:32-04:00"
		}
	]
}