{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003823",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3823,
			"data": "1,1,0,-1,0,1,1,-1,-2,0,2,2,-1,-3,-1,3,3,-2,-5,-1,6,5,-3,-8,-2,8,7,-5,-12,-2,13,12,-7,-18,-4,18,16,-11,-26,-5,27,24,-14,-37,-8,37,33,-21,-52,-10,53,47,-29,-72,-15,71,63,-40,-98,-19,99,88,-53,-133,-27,131,115,-73,-178,-35,177,156,-95,-236,-48,232,204,-127,-311",
			"name": "Power series expansion of the Rogers-Ramanujan continued fraction 1+x/(1+x^2/(1+x^3/(1+x^4/(1+...)))).",
			"comment": [
				"This is the q-expansion of the Gamma(5)-modular function (or automorphic function) Lambda given, for example, in Erdelyi et al., Higher Transcendental Functions eq. 44 volume 3 page 24 sec. 14.6.3 - Warren Smith.",
				"Number 14 of the 15 generalized eta-quotients listed in Table I of Yang 2004. - Michael Somos, Aug 07 2014",
				"A generator (Hauptmodul) of the function field associated with congruence subgroup Gamma(5). [Yang 2004] - _Michael Somos_, Aug 07 2014"
			],
			"reference": [
				"G. E. Andrews, Ramanujan's \"lost\" notebook, III, the Rogers-Ramanujan continued fraction, Adv. Math. 41 (1981), 186-208.",
				"J. M. Borwein and P. B. Borwein, Pi and the AGM, Wiley, 1987, p. 81.",
				"A. Erdelyi, Higher Transcendental Functions, McGraw-Hill, 1955, Vol. 3, p. 24.",
				"H. S. Wall, Analytic Theory of Continued Fractions, Chelsea 1973, p. 404."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A003823/b003823.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e (terms 0..1000 from T. D. Noe)",
				"S.-D. Chen and S.-S. Huang, \u003ca href=\"http://dx.doi.org/10.1142/S1793042105000030\"\u003eOn the series expansion of the Göllnitz-Gordon continued fraction\u003c/a\u003e, Internat. J. Number Theory, 1 (2005), 53-63.",
				"W. Duke, \u003ca href=\"http://dx.doi.org/10.1090/S0273-0979-05-01047-5\"\u003eContinued fractions and modular functions\u003c/a\u003e, Bull. Amer. Math. Soc. 42 (2005), 137-162; see Eq. (6.5).",
				"J. Malenfant, \u003ca href=\"http://arxiv.org/abs/1109.5957\"\u003eGeneralizing Ramanujan's J Functions\u003c/a\u003e, arXiv preprint arXiv:1109.5957 [math.NT], 2011.",
				"Y. Yang, \u003ca href=\"http://dx.doi.org/10.1112/S0024609304003510\"\u003eTransformation formulas for generalized Dedekind eta functions\u003c/a\u003e, Bull. London Math. Soc. 36 (2004), no. 5, 671-682. See p. 679, Table 1."
			],
			"formula": [
				"G.f.: Prod_{k\u003e0} (1-x^{5k-2})(1-x^{5k-3})/((1-x^{5k-1})(1-x^{5k-4})).",
				"G.f.: (Sum_{k in Z} (-1)^k * x^((5*k + 1) * k/2)) / (Sum_{k in Z} (-1)^k * x^((5*k + 3) * k/2)). - _Michael Somos_, Dec 13 2002",
				"Euler transform of period 5 sequence [1, -1, -1, 1, 0, ...]. - _Michael Somos_, Dec 13 2002",
				"G.f. is reciprocal of that for the Rogers-Ramanujan continued fraction r(tau) - see A007325.",
				"Expansion of f(-x^2, -x^3) / f(-x, -x^4) in powers of x where f(,) is Ramanujan's two-variable theta function. - _Michael Somos_, Aug 07 2014",
				"a(0) = 1, a(n) = (1/n)*Sum_{k=1..n} A109091(k)*a(n-k) for n \u003e 0. - _Seiichi Manyama_, Apr 01 2017"
			],
			"example": [
				"G.f. = 1 + x - x^3 + x^5 + x^6 - x^7 - 2*x^8 + 2*x^10 + 2*x^11 - x^12 - ...",
				"G.f. = 1/q + q^4 - q^14 + q^24 + q^29 - q^34 - 2*q^39 + 2*q^49 + 2*q^54 - q^59 + ..."
			],
			"maple": [
				"M := 100: a[ M ] := 1+z; for n from M-1 by -1 to 1 do a[ n ] := series( 1 + z^n/a[ n+1 ], z, M+1); od: a[ 1 ];",
				"M:=100; qf:=(a,q)-\u003emul(1-a*q^j,j=0..M); t1:=qf(q^2,q^5)*qf(q^3,q^5)/(qf(q,q^5)*qf(q^4,q^5)); series(%,q,M); seriestolist(%);"
			],
			"mathematica": [
				"kmax = 16; f[x_] := Product[(1-x^(5k-2))*(1-x^(5k-3))/((1-x^(5k-1))*(1-x^(5k-4))), {k, 1, kmax}]; CoefficientList[ Series[f[x], {x, 0, 5*kmax}], x] (* _Jean-François Alcover_, Nov 02 2011, after g.f. *)",
				"a[ n_] := SeriesCoefficient[ QPochhammer[ x^2, x^5] QPochhammer[ x^3, x^5] / (QPochhammer[ x, x^5] QPochhammer[ x^4, x^5]), {x, 0, n}]; (* _Michael Somos_, Jul 09 2014 *)",
				"a[ n_] := If[n \u003c 0, 0, SeriesCoefficient[ 1 / ContinuedFractionK[ x^k, 1, {k, 0, n}], {x, 0, n}]]; (* _Michael Somos_, Jul 09 2014 *)"
			],
			"program": [
				"(PARI) {a(n) = local(k); if( n\u003c0, 0, k = (3 + sqrtint(9 + 40*n)) \\ 10; polcoeff( sum( i=-k, k, (-1)^i * x^((5*i^2 + i)/2), x * O(x^n)) / sum( i=-k, k, (-1)^i * x^((5*i^2 + 3*i)/2), x * O(x^n)), n))}; /* _Michael Somos_, Dec 13 2002 */",
				"(PARI) {a(n) = if( n\u003c0, 0, polcoeff( prod( k=1, n, if( k%5, (1 - x^k)^( -(-1)^binomial( k%5, 2)), 1), 1 + x * O(x^n)), n))}; /* _Michael Somos_, Dec 13 2002 */",
				"(PARI) {a(n) = local(cf); if( n\u003c0, 0, cf = contfracpnqn( matrix(2, (sqrtint(8*n + 1) + 1)\\2, i, j, if( i==1, x^(j-1), 1))); polcoeff( cf[1, 1] / cf[2, 1] + x * O(x^n), n))}; /* _Michael Somos_, Dec 13 2002 */"
			],
			"xref": [
				"Cf. A007325."
			],
			"keyword": "sign,easy,nice",
			"offset": "0,9",
			"author": "_N. J. A. Sloane_",
			"references": 24,
			"revision": 54,
			"time": "2017-04-01T11:30:41-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}