{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A151945",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 151945,
			"data": "1,1,3,5,7,10,14,19,25,32,42,53,66,82,101,124,150,181,216,257,306,361,424,495,577,671,776,895,1029,1180,1350,1540,1752,1988,2252,2547,2872,3231,3630,4071,4558,5093,5683,6330,7040,7822,8674,9606,10625,11738",
			"name": "Denomination sequence. Start with the 0th and first coins of value 1 cent: a(0)=a(1)=1. Thereafter a(n), the value of the n-th coin (n\u003e=2), is the number of ways to make change for n cents in earlier coins. The two one-cent coins are considered distinct.",
			"comment": [
				"a(n) is the number of nonnegative solutions to the Diophantine equation 1*x_0 + 1*x_1 + ... + a(n-1)*x_(n-1) = n. - _Melvin Peralta_, Jan 03 2016"
			],
			"link": [
				"Robert G. Wilson v, \u003ca href=\"/A151945/b151945.txt\"\u003eTable of n, a(n) for n = 0..3500\u003c/a\u003e"
			],
			"formula": [
				"G.f: g(x) = Product_{n \u003e= 0} 1/(1-x^a(n)) - x."
			],
			"example": [
				"Call the two one-cent coins c and d.",
				"Then we can make change for 2 cents in three ways: cc,cd,dd, so a(2) = 3.",
				"Then we can make change for 3 cents in five ways: ccc,ccd,cdd,ddd,3, so a(3) = 5."
			],
			"maple": [
				"b:= proc(n,i) option remember;",
				"      if n\u003c0 then 0",
				"    elif n=0 then 1",
				"    elif i\u003c0 then 0",
				"    elif i=1 then n+1",
				"    else b(n,i-1) +b(n-a(i),i)",
				"      fi",
				"    end:",
				"a:= n-\u003e b(n, n-1):",
				"seq(a(n), n=0..100);  # _Alois P. Heinz_, Aug 14 2009"
			],
			"mathematica": [
				"b[n_, i_] := b[n, i] = If[n \u003c 0, 0, If[n == 0, 1, If[i \u003c 0, 0, If[i == 1, n + 1, b[n, i - 1] + b[n - a[i], i]] ]]]; a[0] = a[1] = 1; a[n_] := a[n] = b[n, n - 1]; Table[ a@n, {n, 0, 50}] (* _Robert G. Wilson v_, Aug 17 2009 *)",
				"Nest[Join[#,{Length[FrobeniusSolve[#,Length[#]]]}]\u0026,{1,1},50] (* _Harvey P. Dale_, Jul 29 2018 *)"
			],
			"program": [
				"(Haskell)",
				"a151945 n = a151945_list !! n",
				"a151945_list = 1 : 1 : f [2..] where",
				"   f (x:xs) = p (take x a151945_list) x : f xs",
				"   p _ 0 = 1; p [] _ = 0",
				"   p ds'@(d:ds) m = if m \u003c d then 0 else p ds' (m - d) + p ds m",
				"-- _Reinhard Zumkeller_, Jan 21 2014"
			],
			"keyword": "nonn,nice",
			"offset": "0,3",
			"author": "_David W. Wilson_, Aug 14 2009",
			"ext": [
				"More terms from _Alois P. Heinz_, Aug 14 2009"
			],
			"references": 7,
			"revision": 33,
			"time": "2018-07-29T19:08:24-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}