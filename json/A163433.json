{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A163433",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 163433,
			"data": "0,4,22,52,94,148,214,292,382,484,598,724,862,1012,1174,1348,1534,1732,1942,2164,2398,2644,2902,3172,3454,3748,4054,4372,4702,5044,5398,5764,6142,6532,6934,7348,7774,8212,8662,9124,9598,10084,10582,11092,11614",
			"name": "Number of different fixed (possibly) disconnected trominoes bounded tightly by an n X n square.",
			"comment": [
				"Except for the first term of 0, a(n) is the set of all integers k such that 6k+12 is a perfect square. - _Gary Detlefs_, Mar 01 2010",
				"For n \u003e 2, the surface area of a rectangular prism with sides n-2, n-1, and n. - _J. M. Bergot_, Sep 12 2011",
				"Also the number of 4-cycles in the (n+2) X (n+2) knight graph. - _Eric W. Weisstein_, May 05 2017"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A163433/b163433.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/GraphCycle.html\"\u003eGraph Cycle\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/KnightGraph.html\"\u003eKnight Graph\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-3,1)."
			],
			"formula": [
				"a(n) = 6*n^2 - 12*n + 4, n \u003e 1.",
				"From _Colin Barker_, Sep 06 2013: (Start)",
				"a(n) = 3*a(n-1) - 3*a(n-2) + a(n-3) for n \u003e 4.",
				"G.f.: 2*x^2*(x^2-5*x-2) / (x-1)^3. (End)",
				"a(n+1) = (n*i-1)^3 - (n*i+1)^3, where n \u003e 0, i=sqrt(-1). - _Bruno Berselli_, Jan 23 2014",
				"E.g.f.: 2*((3*x^2 - 3*x + 2)*exp(x) + x - 2). - _G. C. Greubel_, Dec 23 2016"
			],
			"example": [
				"a(2)=4: the four rotations of the (connected) L tromino."
			],
			"maple": [
				"A163433:=n-\u003e6*n^2 - 12*n + 4: 0,seq(A163433(n), n=2..100); # _Wesley Ivan Hurt_, May 05 2017"
			],
			"mathematica": [
				"CoefficientList[Series[(2*z*(z^3 - 5*z^2 - 2*z))/(z - 1)^3, {z, 0, 100}], z] (* _Vladimir Joseph Stephan Orlovsky_, Jul 17 2011 *)",
				"Join[{0}, Table[6*n^2 - 12*n + 4, {n, 2, 50}]] (* _G. C. Greubel_, Dec 23 2016 *)",
				"Join[{0}, LinearRecurrence[{3, -3, 1}, {4, 22, 52}, 50]] (* _G. C. Greubel_, Dec 23 2016 *)",
				"Length /@ Table[FindCycle[KnightTourGraph[n + 2, n + 2], {4}, All], {n, 20}] (* _Eric W. Weisstein_, May 05 2017 *)"
			],
			"program": [
				"(PARI) concat([0], Vec(2*x^2*(x^2-5*x-2) / (x-1)^3 + O(x^50))) \\\\ _G. C. Greubel_, Dec 23 2016"
			],
			"xref": [
				"Cf. A162673, A163434, A163437.",
				"Cf. A289181 (6-cycles in the n X n knight graph)."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_David Bevan_, Jul 28 2009",
			"references": 7,
			"revision": 39,
			"time": "2021-02-06T15:24:01-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}