{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A241922",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 241922,
			"data": "2,2,2,0,1,0,1,4,0,0,1,2,4,0,0,1,2,4,4,16,0,0,1,9,0,0,1,2,4,4,9,2,0,0,0,1,4,0,0,1,16,4,4,9,36,0,1,9,0,1,0,1,4,16,0,1,0,0,1,9,4,0,1,9,0,1,9,64,0,1,9,2,4,0,1,25,0,1,64,25,4,0,1,49,0,0,0,1,4,4,0,1,0,0,0,1,4,4,4,9,16",
			"name": "Smallest k^2\u003e=0 such that n-k^2 is semiprime, or a(n)=2 if there is no such k^2.",
			"comment": [
				"If n = m^2, m\u003e=2, then the condition {a(n) differs from 2} is equivalent to the Goldbach binary conjecture. Indeed, if m^2 - k^2 is semiprime, then (m-k)*(m+k) = p*q, where p\u003c=q are primes. Here we consider two possible cases. 1) m-k=1, m+k=p*q and 2) m-k=p, m+k=q. But in the first case k=m-1\u003em-p, i.e., more than k in the second case. In view of the minimality k, we only have to consider case 2). In this case we have m-/+k both are primes p\u003c=q (with equality in case k=0) and thus 2*m = p + q. Conversely, let the Goldbach conjecture be true. Then for a perfect square n\u003e=4, we have 2*sqrt(n)=p+q (p\u003c=q are both primes). Thus n=((p+q)/2)^2 and n-((p-q)/2)^2=p*q is semiprime. Hence a(n) is a square not exceeding ((p-q)/2)^2.",
				"Note that a(n)=2 for 1,2,3,12,17,28,32,72,...",
				"All these numbers are in A100570. Thus the Goldbach binary conjecture is true if and only if A100570 does not contain perfect squares.",
				"The largest term found in the first 2^28 terms is a(106956964) = 369^2 = 136161. This further encourages one to believe that Goldbach's binary conjecture holds true. - _Daniel Mikhail_, Nov 23 2020"
			],
			"link": [
				"Peter J. C. Moses, \u003ca href=\"/A241922/b241922.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Daniel Mikhail, \u003ca href=\"https://raw.githubusercontent.com/mikhaidn/SemiprimeCalculations/main/Summary%20of%202%5E28%20results\"\u003eLists of up to the first 15 integers that are a squared distance, k^2, away from a semiprime for all k's found between [5..2^28]\u003c/a\u003e"
			],
			"formula": [
				"a(A001358(n)) = 0."
			],
			"program": [
				"(PARI) a(n) = {my(lim = if (issquare(n), sqrtint(n)-1, sqrtint(n))); for (k=0, lim, if (bigomega(n-k^2) == 2, return (k^2));); return (2);} \\\\ _Michel Marcus_, Nov 26 2020"
			],
			"xref": [
				"Cf. A000290, A001358, A100570, A152522, A152451, A156537."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Vladimir Shevelev_, May 01 2014",
			"references": 5,
			"revision": 55,
			"time": "2020-12-25T13:18:22-05:00",
			"created": "2014-05-07T00:32:43-04:00"
		}
	]
}