{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A072170",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 72170,
			"data": "1,1,1,1,1,1,1,2,1,1,1,1,2,1,1,1,3,2,2,1,1,1,2,3,2,2,1,1,1,3,3,4,2,2,1,1,1,1,4,3,4,2,2,1,1,1,4,4,5,4,4,2,2,1,1,1,3,5,4,5,4,4,2,2,1,1,1,5,5,8,5,6,4,4,2,2,1,1,1,2,6,6,8,5,6,4,4,2,2,1,1,1,5,6,9,8,9,6,6,4,4,2,2,1,1",
			"name": "Square array T(n,k) (n \u003e= 0, k \u003e= 2) read by antidiagonals, where T(n,k) is the number of ways of writing n as Sum_{i\u003e=0} c_i 2^i, c_i in {0,1,...,k-1}.",
			"comment": [
				"k-th column is k-th binary partition function.",
				"The sequence data corresponds (via the table link) to the transpose of the array shown in example and given by the definition. - _M. F. Hasler_, Feb 14 2019"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A072170/b072170.txt\"\u003eAntidiagonals n = 0..140, flattened\u003c/a\u003e",
				"B. Reznick, \u003ca href=\"https://doi.org/10.1007/978-1-4612-3464-7_29\"\u003eSome binary partition functions\u003c/a\u003e, in \"Analytic number theory\" (Conf. in honor P. T. Bateman, Allerton Park, IL, 1989), 451-477, Progr. Math., 85, Birkhäuser Boston, Boston, MA, 1990."
			],
			"formula": [
				"T(n,k) = T(n,n+1) = T(n,n)+1 = A000123(floor(n/2)) for all k \u003e= n+1. - _M. F. Hasler_, Feb 14 2019"
			],
			"example": [
				"Array begins: (rows n \u003e= 0, columns k \u003e= 2)",
				"1 1 1 1 1 1 1 1 ...",
				"1 1 1 1 1 1 1 1 ...",
				"1 2 2 2 2 2 2 2 ...",
				"1 1 2 2 2 2 2 2 ...",
				"1 3 3 4 4 4 4 4 ...",
				"1 2 3 3 4 4 4 4 ...",
				"1 3 4 5 5 6 6 6 ..."
			],
			"maple": [
				"b:= proc(n, i, k) option remember;",
				"      `if`(n=0, 1, `if`(i\u003c0, 0, add(`if`(n-j*2^i\u003c0, 0,",
				"         b(n-j*2^i, i-1, k)), j=0..k-1)))",
				"    end:",
				"T:= (n, k)-\u003e b(n, ilog2(n), k):",
				"seq(seq(T(d+2-k, k), k=2..d+2), d=0..14); # _Alois P. Heinz_, Jun 21 2012"
			],
			"mathematica": [
				"b[n_, i_, k_] := b[n, i, k] = If[n == 0, 1, If[i \u003c 0, 0, Sum[If[n-j*2^i \u003c 0, 0, b[n-j*2^i, i-1, k]], {j, 0, k-1}]]];",
				"t[n_, k_] := b[n, Length[IntegerDigits[n, 2]] - 1, k];",
				"Table[Table[t[d+2-k, k], {k, 2, d+2}], {d, 0, 14}] // Flatten (* _Jean-François Alcover_, Jan 14 2014, translated from _Alois P. Heinz_'s Maple code *)"
			],
			"program": [
				"(PARI) M72170=[[]]; A072170(n,k,i=logint(n+!n,2),r=1)={if( !i, k\u003en, r\u0026\u0026(k\u003c5||k\u003e=n),if(k\u003e4, A000123(n\\2)-(k==n), k\u003c3, 1, k\u003c4, A002487(n), n\\2+1), M72170[r=setsearch(M72170,[n,k,i,\"\"],1)-1][^-1]==[n,k,i], M72170[r][4], M72170=setunion(M72170,[[n,k,i,r=sum(j=0,min(k-1,n\u003e\u003ei),A072170(n-j*2^i,k,i-1,0))]]);r)} \\\\ Code for k\u003c5 (using A002487 for k=3) and k\u003e=n (using A000123) is optional but makes it about 3x faster. - _M. F. Hasler_, Feb 14 2019"
			],
			"xref": [
				"k=3 column is A002487, k=4 is A008619 (positive integers repeated), k = 5, 6, 7 are A007728, A007729, A007730, limiting (infinity) column is A000123 doubled up."
			],
			"keyword": "nonn,tabl",
			"offset": "0,8",
			"author": "_N. J. A. Sloane_, Jun 29 2002",
			"references": 13,
			"revision": 30,
			"time": "2021-02-20T03:43:08-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}