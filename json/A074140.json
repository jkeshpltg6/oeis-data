{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A074140",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 74140,
			"data": "1,2,10,50,346,3182,38770,609290,11226106,250148582,7057182250,216512001950,7903965900226,321552174623162,13779150603234010,644574260638821590,33968684108427733426,1994885097404292104942,121496572792097514728530,8114030083731371137603190",
			"name": "Sum of least integers of prime signatures over all partitions of n.",
			"comment": [
				"Old name was: Sum of terms in n-th group in A036035.",
				"a(n) is also the sum of terms in n-th row of A063008, A087443 or A227955."
			],
			"link": [
				"Peter Luschny and Alois P. Heinz, \u003ca href=\"/A074140/b074140.txt\"\u003eTable of n, a(n) for n = 0..350\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PrimeSignature.html\"\u003ePrime Signature\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Partition_(number_theory)\"\u003ePartition (number theory)\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Prime_signature\"\u003ePrime signature\u003c/a\u003e",
				"\u003ca href=\"/index/Pri#prime_signature\"\u003eIndex entries for sequences related to prime signature\u003c/a\u003e"
			],
			"example": [
				"a(6) = 64+96+144+216+240+360+900+840+1260+4620+30030 = 38770."
			],
			"maple": [
				"b:= proc(n, i, j) option remember;",
				"      `if`(n=0, 1, `if`(i\u003c1, 0, b(n, i-1, j)+",
				"      `if`(i\u003en, 0, ithprime(j)^i*b(n-i, i, j+1))))",
				"    end:",
				"a:= n-\u003e b(n$2, 1):",
				"seq(a(n), n=0..40);  # _Alois P. Heinz_, Aug 03 2013"
			],
			"mathematica": [
				"b[n_, i_, j_] := b[n, i, j] = If[n == 0, 1, If[i\u003c1, 0, b[n, i-1, j]+If[i\u003en, 0, Prime[j]^i*b[n-i, i, j+1]]]]; a[n_] := b[n, n, 1]; Table[a[n], {n, 0, 20}] (* _Jean-François Alcover_, Feb 25 2014, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(Sage)",
				"def A074140(n):",
				"    L = []",
				"    P = primes_first_n(n)",
				"    for p in Partitions(n):",
				"        m = mul(P[i]^pi for i, pi in enumerate(p))",
				"        L.append(m)",
				"    return add(L)",
				"[A074140(n) for n in (0..20)]  # _Peter Luschny_, Aug 02 2013"
			],
			"xref": [
				"Cf. A036035, A063008, A074139, A074141, A025487, A087443, A227955, A332626."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Amarnath Murthy_, Aug 28 2002",
			"ext": [
				"More terms from _Alford Arnold_, Sep 10 2002",
				"a(10)-a(12) from Thomas A. Rockwell (LlewkcoRAT(AT)aol.com), Sep 30 2004",
				"a(12) corrected by _Peter Luschny_, Aug 03 2013",
				"New name from _Alois P. Heinz_, Aug 03 2013"
			],
			"references": 9,
			"revision": 43,
			"time": "2020-03-15T09:04:29-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}