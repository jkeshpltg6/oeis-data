{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A245120",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 245120,
			"data": "1,0,1,0,1,0,1,1,0,1,1,0,1,3,0,1,4,1,0,1,8,2,0,1,12,4,0,1,22,9,0,1,36,17,2,0,1,63,35,3,0,1,107,67,9,0,1,188,131,20,0,1,327,249,46,1,0,1,578,484,94,4,0,1,1020,922,202,11,0,1,1820,1775,412,28",
			"name": "Number T(n,k) of n-node rooted identity trees with thinning limbs and root outdegree (branching factor) k; triangle T(n,k), n\u003e=1, 0\u003c=k\u003c=max-index-of-row(n), read by rows.",
			"comment": [
				"In a rooted tree with thinning limbs the outdegree of a parent node is larger than or equal to the outdegree of any of its child nodes."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A245120/b245120.txt\"\u003eRows n = 1..140, flattened\u003c/a\u003e"
			],
			"example": [
				"The A124346(7) = 6 7-node rooted identity trees with thinning limbs sorted by root outdegree are:",
				":  o  :   o     o       o      o   :   o   :",
				":  |  :  / \\   / \\     / \\    / \\  :  /|\\  :",
				":  o  : o   o o   o   o   o  o   o : o o o :",
				":  |  : |     |   |  / \\    ( )  | : | |   :",
				":  o  : o     o   o o   o   o o  o : o o   :",
				":  |  : |     |     |       |      : |     :",
				":  o  : o     o     o       o      : o     :",
				":  |  : |     |     |              :       :",
				":  o  : o     o     o              :       :",
				":  |  : |                          :       :",
				":  o  : o                          :       :",
				":  |  :                            :       :",
				":  o  :                            :       :",
				":     :                            :       :",
				": -1- : -------------2------------ : --3-- :",
				"Thus row 7 = [0, 1, 4, 1].",
				"Triangle T(n,k) begins:",
				"1;",
				"0, 1;",
				"0, 1;",
				"0, 1,  1;",
				"0, 1,  1;",
				"0, 1,  3;",
				"0, 1,  4,  1;",
				"0, 1,  8,  2;",
				"0, 1, 12,  4;",
				"0, 1, 22,  9;",
				"0, 1, 36, 17, 2;",
				"0, 1, 63, 35, 3;"
			],
			"maple": [
				"b:= proc(n, i, h, v) option remember; `if`(n=0, `if`(v=0, 1, 0),",
				"      `if`(i\u003c1 or v\u003c1 or n\u003cv, 0, add(binomial(A(i, min(i-1, h)), j)",
				"       *b(n-i*j, i-1, h, v-j), j=0..min(n/i, v))))",
				"    end:",
				"A:= proc(n, k) option remember;",
				"      `if`(n\u003c2, n, add(b(n-1$2, j$2), j=1..min(k, n-1)))",
				"    end:",
				"g:= proc(n) local k; if n=1 then 0 else",
				"       for k while T(n, k)\u003e0 do od; k-1 fi",
				"    end:",
				"T:= (n, k)-\u003e b(n-1$2, k$2):",
				"seq(seq(T(n, k), k=0..g(n)), n=1..25);"
			],
			"mathematica": [
				"b[n_, i_, h_, v_] := b[n, i, h, v] = If[n==0, If[v==0, 1, 0], If[i\u003c1 || v\u003c1 || n\u003cv, 0, Sum[Binomial[A[i, Min[i-1, h]], j]*b[n-i*j, i-1, h, v-j], {j, 0, Min[n/i, v]}]]]; A[n_, k_] := A[n, k] = If[n\u003c2, n, Sum[b[n-1, n-1, j, j], {j, 1, Min[k, n-1]}]]; g[n_] := If[n==1, 0, For[k=1, T[n, k]\u003e0, k++]; k-1]; T[n_, k_] := b[n-1, n-1, k, k]; Table[T[n, k], {n, 1, 25}, {k, 0, g[n]}] // Flatten (* _Jean-François Alcover_, Jan 18 2017, translated from Maple *)"
			],
			"xref": [
				"Column k=0-10 give: A000007(n-1), A000012 (for n\u003e1), A245121, A245122, A245123, A245124, A245125, A245126, A245127, A245128, A245129.",
				"Row sums give A124346.",
				"Cf. A244657."
			],
			"keyword": "nonn,tabf",
			"offset": "1,14",
			"author": "_Alois P. Heinz_, Jul 12 2014",
			"references": 12,
			"revision": 18,
			"time": "2017-01-18T08:58:56-05:00",
			"created": "2014-07-12T04:27:42-04:00"
		}
	]
}