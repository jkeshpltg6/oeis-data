{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A327853",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 327853,
			"data": "0,0,1,0,0,2,0,1,2,3,0,0,0,0,4,0,1,0,0,4,5,0,0,2,0,4,0,6,0,1,2,3,4,5,6,7,0,0,0,0,0,0,0,0,8,0,1,0,0,0,0,0,0,8,9,0,0,2,0,0,0,0,0,8,0,10,0,1,2,3,0,0,0,0,8,9,10,11,0,0,0,0,4,0,0,0,8,0,0,0,12",
			"name": "Triangle read by rows, Sierpinski's gasket, A047999 * (0,1,2,3,4,...) diagonalized.",
			"comment": [
				"This is similar to A166555, the difference being that this is scaled \"linearly\" instead of exponentially.",
				"The scatterplot of the sequence resembles Sierpinski's gasket (triangle), with a square root border (the \"linear\" scaling is not normalized and actually resembles the scale of the function of the positive inverse of triangular numbers: A003056).",
				"If instead of (0,1,2,3,4,...), we use the A000217 (triangular numbers), then the border of the scatterplot will be truly linear."
			],
			"link": [
				"Matej Veselovac, \u003ca href=\"/A327853/b327853.txt\"\u003eTable of n, a(n) for n = 1..100000\u003c/a\u003e",
				"Math StackExchange, \u003ca href=\"https://math.stackexchange.com/q/2585840/318073\"\u003ePattern in Pascal's triangle \u003c/a\u003e.",
				"Matej Veselovac, \u003ca href=\"https://imgur.com/gallery/6hAL81C\"\u003eScatterplot and Logarithmic Scatterplot of triangle read by rows T(n,k) = k * (binomial(n, k) mod 2), n=1...10^5.\u003c/a\u003e."
			],
			"formula": [
				"Triangle read by rows, A047999 * Q. A047999 = Sierpinski's gasket, Q = an infinite lower triangular matrix with (0,1,2,3,...) as the main diagonal and the rest zeros.",
				"The entries of the triangle are given by T(n, k) = k * (binomial(n, k) (mod 2)), then it is read by rows."
			],
			"example": [
				"First 16 rows of the triangle:",
				"  0;",
				"  0, 1;",
				"  0, 0, 2;",
				"  0, 1, 2, 3;",
				"  0, 0, 0, 0, 4;",
				"  0, 1, 0, 0, 4, 5;",
				"  0, 0, 2, 0, 4, 0, 6;",
				"  0, 1, 2, 3, 4, 5, 6, 7;",
				"  0, 0, 0, 0, 0, 0, 0, 0, 8;",
				"  0, 1, 0, 0, 0, 0, 0, 0, 8, 9;",
				"  0, 0, 2, 0, 0, 0, 0, 0, 8, 0, 10;",
				"  0, 1, 2, 3, 0, 0, 0, 0, 8, 9, 10, 11;",
				"  0, 0, 0, 0, 4, 0, 0, 0, 8, 0,  0,  0, 12;",
				"  0, 1, 0, 0, 4, 5, 0, 0, 8, 9,  0,  0, 12, 13;",
				"  0, 0, 2, 0, 4, 0, 6, 0, 8, 0, 10,  0, 12,  0, 14;",
				"  0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15;"
			],
			"mathematica": [
				"r[n0_]:=Flatten[Table[(k)(Mod[Binomial[n,k],2]),{n,0,n0},{k,0,n}]]; r[20] (* _Matej Veselovac_, Sep 28 2019 *)"
			],
			"xref": [
				"Cf. A001317, A007318, A003056, A000217.",
				"Cf. A166555 (2^k is used instead of k).",
				"Cf. A080099 (similar scatterplot visualization).",
				"Cf. A327889 (alternating, normalized (linear) modification of the sequence, transformed by first decimal digit indicator function)."
			],
			"keyword": "nonn,tabl,hear,look",
			"offset": "1,6",
			"author": "_Matej Veselovac_, Sep 28 2019",
			"references": 2,
			"revision": 24,
			"time": "2019-10-27T14:15:46-04:00",
			"created": "2019-10-23T15:47:44-04:00"
		}
	]
}