{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A347563",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 347563,
			"data": "0,1,1,2,1,2,6,2,2,6,12,3,2,3,12,60,12,6,6,12,60,60,10,4,3,4,10,60,420,60,20,12,12,20,60,420,840,105,30,15,12,15,30,105,840,2520,280,70,30,20,20,30,70,280,2520",
			"name": "Binomial complement triangle, T(n,k) = LCM(1,...,n)/binomial(n,k) for 0 \u003c= k \u003c= n, a(0) = T(0,0) = 0, read by rows.",
			"comment": [
				"The one's complement of each carry value, in base prime p, defined in Lucas's Theorem. Also works using Erdős's method (see formula below).",
				"At row n of the triangle, the values are symmetrical with the largest values occurring at T(n,0) = T(n,n) = LCM(1,...,n). The smallest value(s) occur at k = n/2 when n is even, and at k = floor(n/2) and k = floor(n/2)+1 when n is odd. T(n,k) = T(n,n-k).",
				"Conjecture: For all n, T(n,0) mod A213999(n-1,n-1) = 0, and T(n,k+1) mod A213999(n,k) = 0 for 0 \u003c= k \u003c= n-1 (computed and verified for rows = 0..2000)."
			],
			"formula": [
				"T(n,k) = Product_{p\u003c=n} p^u_p, where u_p = i_max - Sum_{i=1..i_max} v_p(i) = Sum_{i=1..i_max} NOT(v_p(i)), with v_p(i) = floor(n/p^i) - floor(k/p^i) - floor((n-k)/p^i) = {0, 1} and i_max = floor(log(n)/log(p)) (using Erdős's method)."
			],
			"example": [
				"T(7,3) = 12. Triangle T(n,k) begins:",
				"     0;",
				"     1,   1;",
				"     2,   1,  2;",
				"     6,   2,  2,  6;",
				"    12,   3,  2,  3, 12;",
				"    60,  12,  6,  6, 12, 60;",
				"    60,  10,  4,  3,  4, 10, 60;",
				"   420,  60, 20, 12, 12, 20, 60, 420;",
				"   840, 105, 30, 15, 12, 15, 30, 105, 840;",
				"  2520, 280, 70, 30, 20, 20, 30,  70, 280, 2520;"
			],
			"mathematica": [
				"Flatten[Table[(LCM@@Range(1,n))/Binomial[n, k], {n, 0, 11}, {k, 0, n}]]"
			],
			"program": [
				"(PARI) row(n) = vector(n+1, k, k--; lcm([1..n])/binomial(n,k)); \\\\ _Michel Marcus_, Sep 13 2021"
			],
			"xref": [
				"Cf. A003418, A007318, A213999."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "0,4",
			"author": "_Gary Waters_, Sep 06 2021",
			"references": 1,
			"revision": 33,
			"time": "2021-10-09T16:07:38-04:00",
			"created": "2021-10-09T16:07:38-04:00"
		}
	]
}