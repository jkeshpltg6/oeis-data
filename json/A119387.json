{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A119387",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 119387,
			"data": "0,0,1,0,2,1,2,0,3,2,3,1,3,2,3,0,4,3,4,2,4,3,4,1,4,3,4,2,4,3,4,0,5,4,5,3,5,4,5,2,5,4,5,3,5,4,5,1,5,4,5,3,5,4,5,2,5,4,5,3,5,4,5,0,6,5,6,4,6,5,6,3,6,5,6,4,6,5,6,2,6,5,6,4,6,5,6,3,6,5,6,4,6,5,6,1,6,5,6,4,6,5,6,3,6",
			"name": "a(n) is the number of binary digits (1's and nonleading 0's) which remain unchanged in their positions when n and (n+1) are written in binary.",
			"comment": [
				"The largest k for which A220645(n,k) \u003e 0 is k = a(n). That is, a(n) is the largest power of 2 that divides binomial(n,i) for 0 \u003c= i \u003c= n. - _T. D. Noe_, Dec 18 2012"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A119387/b119387.txt\"\u003eTable of n, a(n) for n = 0..1023\u003c/a\u003e",
				"Lukas Spiegelhofer and Michael Wallner, \u003ca href=\"https://arxiv.org/abs/1604.07089\"\u003eDivisibility of binomial coefficients by powers of primes\u003c/a\u003e, arXiv:1604.07089 [math.NT], 2016. Mentions this sequence.",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A048881(n) + A086784(n+1). (A048881(n) is the number of 1's which remain unchanged between binary n and (n+1). A086784(n+1) is the number of nonleading 0's which remain unchanged between binary n and (n+1).)",
				"a(A000225(n))=0. - _R. J. Mathar_, Jul 29 2006",
				"a(n) = -valuation(H(n)*n,2) where H(n) is the n-th harmonic number. - _Benoit Cloitre_, Oct 13 2013",
				"a(n) = A000523(n) - A007814(n) = floor(log(n)/log(2)) - valuation(n,2). - _Benoit Cloitre_, Oct 13 2013",
				"Recurrence: a(2n) = floor(log_2(n)) except a(0) = 0, a(2n+1) = a(n). - _Ralf Stephan_, Oct 16 2013, corrected by _Peter J. Taylor_, Mar 01 2020",
				"a(n) = floor(log_2(A000265(n+1))). - _Laura Monroe_, Oct 18 2020"
			],
			"example": [
				"9 in binary is 1001. 10 (decimal) is 1010 in binary. 2 binary digits remain unchanged (the leftmost two digits) between 1001 and 1010. So a(9) = 2."
			],
			"maple": [
				"a:= n-\u003e ilog2(n+1)-padic[ordp](n+1, 2):",
				"seq(a(n), n=0..128);  # _Alois P. Heinz_, Jun 28 2021"
			],
			"mathematica": [
				"a = {0}; Table[b = IntegerDigits[n, 2]; If[Length[a] == Length[b], c = 1; While[a[[c]] == b[[c]], c++]; c--, c = 0]; a = b; c, {n, 101}] (* _T. D. Noe_, Dec 18 2012 *)"
			],
			"program": [
				"(C)",
				"#include \u003cstdio.h\u003e",
				"#define NMAX 200",
				"int sameD(int a, int b) { int resul=0 ; while(a\u003e0 \u0026\u0026 b \u003e0) { if( (a \u00261) == (b \u0026 1)) resul++ ; a \u003e\u003e= 1 ; b \u003e\u003e= 1 ; } return resul ; }",
				"int main(int argc, char*argv[])",
				"{ for(int n=0;n\u003cNMAX;n++) printf(\"%d,\",sameD(n,n+1)) ; return 0 ; }",
				"/* _R. J. Mathar_, Jul 29 2006 */",
				"(Haskell)",
				"a119387 n = length $ takeWhile (\u003c a070940 n) [1..n]",
				"-- _Reinhard Zumkeller_, Apr 22 2013",
				"(PARI) a(n) = n++; local(c); c=0; while(2^(c+1)\u003cn+1, c=c+1); c-valuation(n, 2); /* _Ralf Stephan_, Oct 16 2013; corrected by _Michel Marcus_, Jun 28 2021 */",
				"(PARI) a(n) = my(x=Vecrev(binary(n)), y=Vecrev(binary(n+1))); sum(k=1, min(#x, #y), x[k] == y[k]); \\\\ _Michel Marcus_, Jun 27 2021",
				"(C)",
				"int A119387(int n)",
				"{",
				"    int m=n+1;",
				"    while (!(m\u00261)) m\u003e\u003e=1;",
				"    int m_bits = 0;",
				"    while (m\u003e\u003e=1) m_bits++;",
				"    return m_bits;",
				"}",
				"/* _Laura Monroe_, Oct 18 2020 */"
			],
			"xref": [
				"Cf. A048881, A086784.",
				"Cf. A070940.",
				"Cf. A000265."
			],
			"keyword": "easy,nonn,base",
			"offset": "0,5",
			"author": "_Leroy Quet_, Jul 26 2006",
			"ext": [
				"More terms from _R. J. Mathar_, Jul 29 2006",
				"Edited by _Charles R Greathouse IV_, Aug 04 2010"
			],
			"references": 4,
			"revision": 70,
			"time": "2021-06-28T08:47:46-04:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}