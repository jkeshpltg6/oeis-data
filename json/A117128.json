{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A117128",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 117128,
			"data": "1,3,6,11,4,15,2,19,38,61,32,63,26,67,24,71,18,77,16,83,12,85,164,81,170,73,174,277,384,275,162,35,166,29,168,317,468,311,148,315,142,321,140,331,138,335,136,347,124,351,122,355,116,357,106,363,100,369,98,375,94,377",
			"name": "Recamán transform of primes (another version): a(0)=1; for n\u003e0, a(n) = a(n-1) - prime(n) if that number is positive and not already in the sequence, otherwise a(n) = a(n-1) + prime(n).",
			"comment": [
				"Differs from Cald's sequence A006509 for first time at n=116 (or 117, depending on offset)."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A117128/b117128.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"\u003ca href=\"/index/Rea#Recaman\"\u003eIndex entries for sequences related to Recamán's sequence\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A064365(n) + 1. - _Thomas Ordowski_, Dec 05 2016"
			],
			"maple": [
				"M1:=500000; a:=array(0..M1); have:=array(1..M1); a[0]:=1; for n from 1 to M1 do have[n]:=0; od: have[1]:=1;",
				"M2:=2000; nmax:=M2;",
				"for n from 1 to M2 do p:=ithprime(n); i:=a[n-1]-p; j:=a[n-1]+p;",
				"if i \u003e= 1 and have[i]=0 then a[n]:=i; have[i]:=1;",
				"elif j \u003c= M1 then a[n]:=j; have[j]:=1;",
				"else nmax:=n-1; break; fi; od: [seq(a[n],n=0..M2)];"
			],
			"mathematica": [
				"a = {1}; Do[If[And[#1 \u003e 0, ! MemberQ[a, #1]], AppendTo[a, #1], AppendTo[a, #2]] \u0026 @@ {#1 - #2, #1 + #2} \u0026 @@ {a[[n - 1]], Prime[n - 1]}, {n, 2, 62}]; a (* _Michael De Vlieger_, Dec 05 2016 *)"
			],
			"program": [
				"(Haskell)",
				"import Data.Set (singleton, notMember, insert)",
				"a117128 n = a117128_list !! n",
				"a117128_list = 1 : f 1 a000040_list (singleton 1) where",
				"   f x (p:ps) s | x' \u003e 0 \u0026\u0026 x' `notMember` s = x' : f x' ps (insert x' s)",
				"                | otherwise                  = xp : f xp ps (insert xp s)",
				"                where x' = x - p; xp = x + p",
				"-- _Reinhard Zumkeller_, Apr 26 2012",
				"(Python)",
				"from sympy import primerange, prime",
				"def aupton(terms):",
				"  alst = [1]",
				"  for n, pn in enumerate(primerange(1, prime(terms)+1), start=1):",
				"    x = alst[-1] - pn",
				"    alst += [x if x \u003e 0 and x not in alst else alst[-1] + pn]",
				"  return alst",
				"print(aupton(61)) # _Michael S. Branicky_, May 30 2021"
			],
			"xref": [
				"Cf. A064365, A006509, A112877."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Apr 20 2006",
			"references": 5,
			"revision": 25,
			"time": "2021-05-31T03:26:55-04:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}