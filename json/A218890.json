{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A218890",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 218890,
			"data": "1,2,3,6,5,4,10,9,8,7,11,12,13,14,15,16,17,18,19,20,21,28,27,26,25,24,23,22,36,35,34,33,32,31,30,29,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,66,65,64,63,62,61,60,59,58,57,56,78",
			"name": "T(n,k) = ((n + k - 1)*(n + k - 2) - (-1 + (-1)^floor((n + k)/2))*n + (1 +(-1)^floor((n + k)/2))*k)/2; n , k \u003e 0, read by antidiagonals.",
			"comment": [
				"Self-inverse permutation of the natural numbers.",
				"a(n) is a pairing function: a function that reversibly maps Z^{+} x Z^{+} onto Z^{+}, where Z^{+} is the set of integer positive numbers.",
				"In general, let m be natural number. Natural numbers placed in table T(n,k) by antidiagonals. The order of placement - at the beginning m antidiagonals downwards, next m antidiagonals upwards and so on. T(n,k) read by antidiagonals downwards.",
				"For m = 1 the result is A056011. This sequence is result for m = 2.",
				"A056023 is result for m = 1 and the changed order of placement - at the beginning m antidiagonals upwards, next m antidiagonals downwards and so on."
			],
			"link": [
				"Boris Putievskiy, \u003ca href=\"/A218890/b218890.txt\"\u003eRows n = 1..140 of triangle, flattened\u003c/a\u003e",
				"Boris Putievskiy, \u003ca href=\"http://arxiv.org/abs/1212.2732\"\u003eTransformations [of] Integer Sequences And Pairing Functions\u003c/a\u003e arXiv:1212.2732 [math.CO], 2012.",
				"Eric W. Weisstein, \u003ca href=\"http://mathworld.wolfram.com/PairingFunction.html\"\u003eMathWorld: Pairing functions\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"For the general case.",
				"As a table",
				"T(n,k) = ((n + k - 1)*(n + k - 2) - (-1 + (-1)^floor((n + k + m - 2)/m))*n + (1 +(-1)^floor((n + k + m - 2)/m))*k)/2.",
				"As linear sequence",
				"a(n) = ((z - 1)*(z - 2) - (-1 + (-1)^floor((z + m - 2)/m))*i + (1 + (-1)^floor((z + m - 2)/m))*j)/2, where i = n - t*(t + 1)/2, j = (t*t + 3*t + 4)/2 - n, t = floor((-1 + sqrt(8*n - 7))/2), z = i + j.",
				"If we change the order of placement - m antidiagonals upwards, m antidiagonals downwards and so on.",
				"As a table",
				"T(n,k) = ((n + k - 1)*(n + k - 2) - (-1 + (-1)^(floor((n + k + m - 2)/m) + 1))*n + (1 + (-1)^(floor((n + k + m - 2)/m) + 1))*k)/2.",
				"As linear sequence",
				"a(n) = ((z - 1)*(z - 2) - (-1 + (-1)^(floor((z + m - 2)/m) + 1))*i + (1 + (-1)^(floor((z + m - 2)/m) + 1))*j)/2, where i = n - t*(t + 1)/2, j = (t*t + 3*t + 4)/2 - n, t = floor((-1 + sqrt(8*n - 7))/2), z = i + j.",
				"For this sequence.",
				"As a table",
				"T(n,k) = ((n + k - 1)*(n + k - 2) - (-1 +(-1)^floor((n + k)/2))*n + (1 + (-1)^floor((n + k)/2))*k)/2.",
				"As linear sequence",
				"a(n) = ((z - 1)*(z - 2) - (-1 + (-1)^floor(z/2))*i + (1 + (-1)^floor(z/2))*j)/2, where i = n - t*(t + 1)/2, j = (t*t + 3*t + 4)/2 - n, t = floor((-1 + sqrt(8*n - 7))/2), z = i + j."
			],
			"example": [
				"The start of the sequence as table.  The direction of the placement denotes by \"\u003e\" and  \"v\".",
				"  v...v          v...v",
				"..1...2...6..10..11..16..28..36...",
				"..3...5...9..12..17..27..35..38...",
				"\u003e 4...8..13..18..26..34..39..48...",
				"\u003e 7..14..19..25..33..40..49..63...",
				".15..20..24..32..41..50..62..74...",
				".21..23..31..42..51..61..73..84...",
				"\u003e22..30..43..52..60..72..85..98...",
				"\u003e29..44..53..59..71..86..99.113...",
				"  . . .",
				"The start of the sequence as triangle array read by rows:",
				"   1;",
				"   2,  3;",
				"   6,  5,  4;",
				"  10,  9,  8,  7;",
				"  11, 12, 13, 14, 15;",
				"  16, 17, 18, 19, 20, 21;",
				"  28, 27, 26, 25, 24, 23, 22;",
				"  36, 35, 34, 33, 32, 31, 30, 29;",
				"  ...",
				"Row r consists of r consecutive numbers from r*r/2 - r/2 + 1 to r*r/2 + r.",
				"If r congruent to 1 or 2 mod 4 rows are increasing.",
				"If r congruent to 0 or 3 mod 4 rows are decreasing."
			],
			"maple": [
				"T:=(n,k)-\u003e((n+k-1)*(n+k-2)-(-1+(-1)^floor((n+k)/2))*n+(1+(-1)^floor((n+k)/2))*k)/2: seq(seq(T(k,n-k),k=1..n-1),n=1..13); # _Muniru A Asiru_, Dec 13 2018"
			],
			"mathematica": [
				"T[n_, k_] := ((n+k-1)(n+k-2) - (-1 + (-1)^Floor[(n+k)/2])n + (1 + (-1)^Floor[(n+k)/2]) k)/2;",
				"Table[T[n-k+1, k], {n, 1, 12}, {k, n, 1, -1}] // Flatten (* _Jean-François Alcover_, Dec 06 2018 *)"
			],
			"program": [
				"(Python)",
				"t=int((math.sqrt(8*n-7) - 1)/ 2)",
				"i=n-t*(t+1)/2",
				"j=(t*t+3*t+4)/2-n",
				"z=i+j",
				"result=((z-1)*(z-2)-(-1+(-1)**int(z/2))*i+(1+(-1)**int(z/2))*j)/2",
				"(Maxima) T(n,k) = ((n + k - 1)*(n + k - 2) - (-1 + (-1)^floor((n + k)/2))*n + (1 +(-1)^floor((n + k)/2))*k)/2$",
				"create_list(T(k, n - k), n, 1, 12, k, 1, n - 1); /* _Franck Maminirina Ramaharo_, Dec 13 2018 */"
			],
			"xref": [
				"Cf. A056011, A056023."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Boris Putievskiy_, Feb 19 2013",
			"references": 4,
			"revision": 55,
			"time": "2018-12-13T11:29:19-05:00",
			"created": "2013-02-28T21:33:00-05:00"
		}
	]
}