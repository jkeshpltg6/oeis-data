{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A292746",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 292746,
			"data": "1,0,1,1,1,1,1,2,3,1,2,3,8,6,1,2,5,19,26,10,1,4,7,43,97,66,15,1,4,11,93,334,361,141,21,1,7,15,197,1095,1778,1066,267,28,1,8,22,409,3482,8207,7108,2668,463,36,1,12,30,840,10855,36310,43747,23116,5909,751,45,1",
			"name": "Triangle read by rows: T(n,k) (n\u003e=0, 0\u003c=k\u003c=n) = number of partitions of n with exactly k kinds of 1's which are introduced in ascending order.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A292746/b292746.txt\"\u003eRows n = 0..140, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = A292745(n,k) - A292745(n,k-1) for k\u003e0. T(n,0) = A292745(n,0) = A002865(n).",
				"T(n,k) = Sum_{i=0..k} (-1)^i * A292741(n, k-i) / ((k-i)!*i!)."
			],
			"example": [
				"T(3,0) = 1: 3.",
				"T(3,1) = 2: 21a, 1a1a1a.",
				"T(3,2) = 3: 1a1a1b, 1a1b1a, 1a1b1b. (The two kinds of 1's are labeled 1a and 1b)",
				"T(3,3) = 1: 1a1b1c.",
				"Triangle T(n,k) begins:",
				"  1;",
				"  0,  1;",
				"  1,  1,   1;",
				"  1,  2,   3,    1;",
				"  2,  3,   8,    6,    1;",
				"  2,  5,  19,   26,   10,    1;",
				"  4,  7,  43,   97,   66,   15,    1;",
				"  4, 11,  93,  334,  361,  141,   21,   1;",
				"  7, 15, 197, 1095, 1778, 1066,  267,  28,  1;",
				"  8, 22, 409, 3482, 8207, 7108, 2668, 463, 36, 1;",
				"  ..."
			],
			"maple": [
				"f:= (n, k)-\u003e add(Stirling2(n, j), j=0..k):",
				"b:= proc(n, i, k) option remember; `if`(n=0 or i\u003c2,",
				"      f(n, k), add(b(n-i*j, i-1, k), j=0..n/i))",
				"    end:",
				"T:= (n, k)-\u003e b(n$2, k)-`if`(k=0, 0, b(n$2, k-1)):",
				"seq(seq(T(n, k), k=0..n), n=0..14);",
				"# second Maple program:",
				"b:= proc(n, i, k) option remember; `if`(n=0 or i\u003c2,",
				"      k^n, b(n, i-1, k) +b(n-i, min(i, n-i), k))",
				"    end:",
				"T:= (n, k)-\u003e add((-1)^i*b(n$2, k-i)/((k-i)!*i!), i=0..k):",
				"seq(seq(T(n, k), k=0..n), n=0..14);"
			],
			"mathematica": [
				"f[n_, k_] := Sum[StirlingS2[n, j], {j, 0, k}];",
				"b[n_, i_, k_] := b[n, i, k] = If[n == 0 || i \u003c 2, f[n, k], Sum[b[n - i*j, i - 1, k], {j, 0, n/i}]];",
				"T[n_, k_] := b[n, n, k] - If[k == 0, 0, b[n, n, k - 1]];",
				"Table[T[n, k], {n, 0, 14}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, May 17 2018, translated from Maple *)"
			],
			"xref": [
				"Columns k=0-10 give: A002865, A000041(n-1) for n\u003e0, A259401(n-2) for n\u003e1, A320816, A320817, A320818, A320819, A320820, A320821, A320822, A320823.",
				"Main diagonal and first lower diagonal give: A000012, A000217.",
				"Row sums give A292503.",
				"T(2n,n) gives A292747.",
				"Cf. A292741, A292745."
			],
			"keyword": "nonn,tabl",
			"offset": "0,8",
			"author": "_Alois P. Heinz_, Sep 22 2017",
			"ext": [
				"Definition clarified by _N. J. A. Sloane_, Dec 12 2020"
			],
			"references": 13,
			"revision": 30,
			"time": "2020-12-16T08:48:30-05:00",
			"created": "2017-09-23T15:16:54-04:00"
		}
	]
}