{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A209830",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 209830,
			"data": "1,1,2,1,5,5,1,7,18,13,1,10,35,59,34,1,12,61,147,185,89,1,15,90,302,558,564,233,1,17,129,527,1324,1986,1685,610,1,20,170,854,2653,5350,6761,4957,1597,1,22,222,1278,4811,12066,20383,22277,14406,4181,1",
			"name": "Triangle of coefficients of polynomials u(n,x) jointly generated with A209831; see the Formula section.",
			"comment": [
				"Each row begins with 1 and ends with an odd-indexed Fibonacci number.",
				"Alternating row sums: 1,-1,1,-1,1,-1,1,-1,...",
				"For a discussion and guide to related arrays, see A208510.",
				"Subtriangle of the triangle given by (1, 0, 1/2, -3/2, 0, 0, 0, 0, 0, 0, 0, ...) DELTA (0, 2, 1/2, 1/2, 0, 0, 0, 0, 0, 0, 0, ...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, Mar 16 2012"
			],
			"formula": [
				"u(n,x) = x*u(n-1,x) + (x+1)*v(n-1,x),",
				"v(n,x) = (x+1)*u(n-1,x) + 2x*v(n-1,x),",
				"where u(1,x)=1, v(1,x)=1.",
				"As DELTA-triangle with 0 \u003c= k \u003c= n: G.f.: (1+x-3*y*x-3*y*x^2+y^2*x^2)/(1-3*y*x-x^2-2*y*x^2+y^2*x^2). - _Philippe Deléham_, Mar 16 2012",
				"As DELTA-triangle: T(n,k) = 3*T(n-1,k-1) + T(n-2,k) + 2*T(n-2,k-1) - T(n-2,k-2), T(0,0) = T(1,0) = T(2,0) = 1, T(1,1 = T(2,2) = 0, T(2,1) = 2 and T(n,k) = 0 if k \u003c 0 or if k \u003e n. - _Philippe Deléham_, Mar 16 2012"
			],
			"example": [
				"First five rows:",
				"  1;",
				"  1,  2;",
				"  1,  5,  5;",
				"  1,  7, 18, 13;",
				"  1, 10, 35, 59, 34;",
				"First three polynomials u(n,x):",
				"  1",
				"  1 + 2x",
				"  1 + 5x + 5x^2.",
				"From _Philippe Deléham_, Mar 16 2012: (Start)",
				"(1, 0, 1/2, -3/2, 0, 0, ...) DELTA (0, 2, 1/2, 1/2, 0, 0, ...) begins:",
				"  1;",
				"  1,  0;",
				"  1,  2,  0;",
				"  1,  5,  5,  0;",
				"  1,  7, 18, 13,  0;",
				"  1, 10, 35, 59, 34, 0; (End)"
			],
			"mathematica": [
				"u[1, x_] := 1; v[1, x_] := 1; z = 16;",
				"u[n_, x_] := x*u[n - 1, x] + (x + 1)*v[n - 1, x];",
				"v[n_, x_] := (x + 1)*u[n - 1, x] + 2 x*v[n - 1, x];",
				"Table[Expand[u[n, x]], {n, 1, z/2}]",
				"Table[Expand[v[n, x]], {n, 1, z/2}]",
				"cu = Table[CoefficientList[u[n, x], x], {n, 1, z}];",
				"TableForm[cu]",
				"Flatten[%]    (* A209830 *)",
				"Table[Expand[v[n, x]], {n, 1, z}]",
				"cv = Table[CoefficientList[v[n, x], x], {n, 1, z}];",
				"TableForm[cv]",
				"Flatten[%]    (* A209831 *)"
			],
			"xref": [
				"Cf. A209831, A208510."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Clark Kimberling_, Mar 13 2012",
			"references": 3,
			"revision": 19,
			"time": "2020-01-24T03:30:13-05:00",
			"created": "2012-03-21T16:21:54-04:00"
		}
	]
}