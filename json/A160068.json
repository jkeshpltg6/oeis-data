{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A160068",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 160068,
			"data": "1,48,1054,-69408,-7284084,-2596032,45400915464,2198714182272,-291719729560944,-35989688841645312,1554341893161645024,524479521392325361152,3802815995858998255296,-7684657653083648501025792,-430659327280723849697798016",
			"name": "Numerator of Hermite(n, 24/25).",
			"link": [
				"Robert Israel, \u003ca href=\"/A160068/b160068.txt\"\u003eTable of n, a(n) for n = 0..380\u003c/a\u003e"
			],
			"formula": [
				"From _Robert Israel_, Jan 02 2017 and Jan 24 2017: (Start)",
				"Theorem: E.g.f.: exp(-x*(625*x-48)); a(n+2) = -1250*(n+1)*a(n)+48*a(n+1).",
				"Proof:",
				"The Hermite polynomials H(n,t) have e.g.f. g(x) = exp(-x^2 + 2 x t).",
				"Thus b(n) := H(n, 24/25)*5^(2n) has e.g.f. exp(-x*(625*x -48)) and satisfies the recurrence b(n+2) = -1250*(n+1)*b(n)+48*b(n+1), n\u003e=0.",
				"To show that these are the numerators of H(n,24/25), we need to show that b(n) is never divisible by 5.  But taking the recurrence mod 5 we get b(n+2) == 3*b(n+1) mod 5.",
				"Since b(0) and b(1) are not divisible by 5, induction finishes the proof.",
				"(End)",
				"From _G. C. Greubel_, Jun 02 2018: (Start)",
				"a(n) = 25^n * Hermite(n, 24/25).",
				"a(n) = numerator(Sum_{k=0..floor(n/2)} (-1)^k*n!*(48/25)^(n-2*k)/(k!*(n-2*k)!)). (End)"
			],
			"example": [
				"Numerators of 1, 48/25, 1054/625, -69408/15625, -7284084/390625, ..."
			],
			"maple": [
				"A160068:=n-\u003enumer(orthopoly[H](n,24/25)): seq(A160068(n), n=0..20); # _Wesley Ivan Hurt_, Nov 20 2014"
			],
			"mathematica": [
				"Table[Numerator@ HermiteH[n, 24/25], {n, 0, 30}] (* _Michael De Vlieger_, Jan 02 2017 *)",
				"Table[25^n*HermiteH[n, 24/25], {n,0,30}] (* _G. C. Greubel_, Jul 11 2018 *)"
			],
			"program": [
				"(PARI) a(n)=numerator(polhermite(n,24/25)) \\\\ _Charles R Greathouse IV_, Jan 29 2016",
				"(MAGMA) [Numerator((\u0026+[(-1)^k*Factorial(n)*(48/25)^(n-2*k)/( Factorial(k) *Factorial(n-2*k)): k in [0..Floor(n/2)]])): n in [0..30]]; // _G. C. Greubel_, Jul 11 2018"
			],
			"xref": [
				"Cf. A009969 (denominators)."
			],
			"keyword": "sign,frac",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Nov 12 2009",
			"references": 1,
			"revision": 33,
			"time": "2018-07-11T15:37:39-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}