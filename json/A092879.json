{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A092879",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 92879,
			"data": "1,1,1,1,3,2,1,5,7,2,1,7,16,13,3,1,9,29,40,22,3,1,11,46,91,86,34,4,1,13,67,174,239,166,50,4,1,15,92,297,541,553,296,70,5,1,17,121,468,1068,1461,1163,496,95,5,1,19,154,695,1912,3300,3544,2269,791,125,6,1,21,191",
			"name": "Triangle of coefficients of the product of two consecutive Fibonacci polynomials.",
			"comment": [
				"The Fibonacci polynomials are defined by F(0,x) = 1, F(1,x) = 1 and F(n, x) = F(n-1, x) + x*F(n-2, x).",
				"This is also the reflected triangle of coefficients of the polynomials defined by the recursion: c0=-1; p(x, n) = (2 + c0 - x)*p(x, n - 1) + (-1 - c0*(2 - x))*p(x, n - 2) + c0*p(x, n - 3). - _Roger L. Bagula_, Apr 09 2008"
			],
			"formula": [
				"From _Johannes W. Meijer_, Jul 20 2011: (Start)",
				"T(n, k) = Sum_{i=0..k} (-1)^(i+k)*binomial(i+2*n-2*k+1, i).",
				"T(n, k) = A035317(2*n-k, k) = A158909(n, n-k.) (End)",
				"T(n,k) = T(n-1,k) + T(n-1,k-1) + T(n-2,k-1) + T(n-2,k-2) - T(n-3,k-3), T(0,0) = 1, T(n,k) = 0 if k \u003c 0 or if k \u003e n. - _Philippe Deléham_, Nov 12 2013"
			],
			"example": [
				"Triangle begins;",
				"  1;",
				"  1,1;",
				"  1,3,2;",
				"  1,5,7,2;",
				"  1,7,16,13,3;",
				"  1,9,29,40,22,3;",
				"  ...",
				"F(3,x) = 1 + 2*x and F(4,x) = 1 + 3*x + x^2 so F(3,x)*F(4,x)=(1 + 3*x + x^2)*(1 + 2*x) = 1 + 5*x + 7*x^2 + 2*x^3 leads to T(3,k) = [1,5,7,2]."
			],
			"maple": [
				"T:=proc(n,k): add((-1)^(i+k)*binomial(i+2*n-2*k+1,i), i=0..k) end: seq(seq(T(n,k), k=0..n), n=0..10); # _Johannes W. Meijer_, Jul 20 2011",
				"T:=proc(n,k): coeff(F(n, x)*F(n+1, x), x, k) end: F:=proc(n, x) option remember: if n=0 then 1 elif n=1 then 1 else procname(n-1, x) + x*procname(n-2, x) fi: end: seq(seq(T(n,k), k=0..n), n=0..10); # _Johannes W. Meijer_, Jul 20 2011"
			],
			"mathematica": [
				"c0 = -1; p[x, -1] = 0; p[x, 0] = 1; p[x, 1] = 2 - x + c0; p[x_, n_] :=p[x, n] = (2 + c0 -x)*p[x, n - 1] + (-1 - c0 (2 - x))*p[x, n - 2] + c0*p[x, n - 3]; Table[ExpandAll[p[x, n]], {n, 0, 10}]; a = Table[Reverse[CoefficientList[p[x, n], x]], {n, 0, 10}]; Flatten[a] (* _Roger L. Bagula_, Apr 09 2008 *)"
			],
			"program": [
				"(PARI) T(n,k)=local(m);if(k\u003c0 || k\u003en,0,n++; m=contfracpnqn(matrix(2,n,i,j,x)); polcoeff(m[1,1]*m[2,1]/x^n,n-k))"
			],
			"xref": [
				"Row sums are A001654(n+1).",
				"Cf. A109954, A136674."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Michael Somos_, Mar 10 2004",
			"ext": [
				"Edited and information added by _Johannes W. Meijer_, Jul 20 2011"
			],
			"references": 2,
			"revision": 19,
			"time": "2017-08-15T03:11:48-04:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}