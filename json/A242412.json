{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A242412",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 242412,
			"data": "15,23,39,63,95,135,183,239,303,375,455,543,639,743,855,975,1103,1239,1383,1535,1695,1863,2039,2223,2415,2615,2823,3039,3263,3495,3735,3983,4239,4503,4775,5055,5343,5639,5943,6255,6575,6903,7239,7583,7935,8295,8663,9039,9423,9815",
			"name": "a(n) = (2n-1)^2 + 14.",
			"comment": [
				"The previous definition was \"a(n) = normalized inverse radius of the inscribed circle that is tangent to the left circle of the symmetric arbelos and the n-th and (n-1)-st circles in the Pappus chain\".",
				"See links section for image of these circles, via Wolfram MathWorld (there an asymmetric arbelos is shown).",
				"The Rothman-Fukagawa article has another picture of the circles, based on a Japanese 1788 sangaku problem. - _N. J. A. Sloane_, Jan 02 2020"
			],
			"reference": [
				"Tony Rothman and Hidetoshi Fukagawa, Japanese temple geometry, Scientific American, Vol. 278, No. 5, May 1998, 85-91."
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/A242412/b242412.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Brady Haran and Simon Pampena, \u003ca href=\"https://www.youtube.com/watch?v=sG_6nlMZ8f4\"\u003eEpic Circles\u003c/a\u003e, Numberphile video (2014).",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/images/eps-gif/PappusTangentChain_800.gif\"\u003eImage of inscribed circles (in red)\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PappusChain.html\"\u003ePappus Chain\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Pappus_chain\"\u003ePappus chain\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-3,1)."
			],
			"formula": [
				"a(n) = 4*n^2 - 4*n + 15.",
				"a(n) = 3*a(n-1)-3*a(n-2)+a(n-3). G.f.: -x*(15*x^2-22*x+15) / (x-1)^3. - _Colin Barker_, May 14 2014",
				"From Descartes three circle theorem:",
				"a(n) = 2 +c(n) + c(n-1)  + 2*sqrt(2*(c(n) + c(n-1) + c(n)*c(n-1)), with c(n) =  A059100(n) = n^2 +2, n \u003e= 1, which produces 4*n^2 - 4*n + 15. - _Wolfdieter Lang_, Jul 01 2015"
			],
			"example": [
				"For n = 1, the radius of the outermost circle divided by the radius of a circle drawn tangent to all three of the initial inner circle, the opposite inner circle (the 0th circle in the chain), and the 1st circle in the chain is 15.",
				"For n = 2, the radius of the outermost circle divided by the radius of a circle drawn tangent to all three of the initial inner circle, the 1st circle in the chain, and the 2nd circle in the chain is 23."
			],
			"maple": [
				"A242412:=n-\u003e4*n^2 - 4*n + 15; seq(A242412(n), n=1..50); # _Wesley Ivan Hurt_, May 13 2014"
			],
			"mathematica": [
				"Table[4 n^2 - 4 n + 15, {n, 50}] (* _Wesley Ivan Hurt_, May 13 2014 *)"
			],
			"program": [
				"(MAGMA) [4*n^2 - 4*n + 15: n in [1..50]]; // _Wesley Ivan Hurt_, May 13 2014",
				"(PARI) a(n) = 4*n^2 - 4*n + 15 \\\\ _Charles R Greathouse IV_, May 14 2014"
			],
			"xref": [
				"Cf. A000012, A059100, A114949, A222465, A259555."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_Aaron David Fairbanks_, May 13 2014",
			"ext": [
				"More terms from _Wesley Ivan Hurt_, May 13 2014",
				"More terms and links from _Robert G. Wilson v_, May 13 2014",
				"Edited: Name reformulated (with consent of the author). - _Wolfdieter Lang_, Jul 01 2015",
				"Edited by _N. J. A. Sloane_, Jan 02 2020, simplifying the definition and adding a reference to the fact that this sequence arose in a sangaku problem from 1788 in a temple in Tokyo Prefecture."
			],
			"references": 2,
			"revision": 53,
			"time": "2020-01-02T22:02:23-05:00",
			"created": "2014-05-14T13:52:50-04:00"
		}
	]
}