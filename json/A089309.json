{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A089309",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 89309,
			"data": "0,1,1,2,1,1,2,3,1,1,1,2,2,1,3,4,1,1,1,2,1,1,2,3,2,1,1,2,3,1,4,5,1,1,1,2,1,1,2,3,1,1,1,2,2,1,3,4,2,1,1,2,1,1,2,3,3,1,1,2,4,1,5,6,1,1,1,2,1,1,2,3,1,1,1,2,2,1,3,4,1,1,1,2,1,1,2,3,2,1,1,2,3,1,4,5,2,1,1,2,1,1,2,3,1",
			"name": "Write n in binary; a(n) = length of the rightmost run of 1's.",
			"comment": [
				"Equivalent to: remove trailing zeros, add one, count trailing zeros. - _Ralf Stephan_, Aug 31 2013",
				"a(n) is also the difference between the two largest distinct parts in the integer partition having viabin number n (we assume that 0 is a part). The viabin number of an integer partition is defined in the following way. Consider the southeast border of the Ferrers board of the integer partition and consider the binary number obtained by replacing each east step with 1 and each north step, except the last one, with 0. The corresponding decimal form is, by definition, the viabin number of the given integer partition. \"Viabin\" is coined from \"via binary\". For example, consider the integer partition [2,2,2,1]. The southeast border of its Ferrers board yields 10100, leading to the viabin number 20. Note that a(20) = 1 = the difference between the two largest distinct parts of the partition [2,2,2,1]. - _Emeric Deutsch_, Aug 17 2017"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A089309/b089309.txt\"\u003eTable of n, a(n) for n = 0..16384\u003c/a\u003e",
				"Francis Laclé, \u003ca href=\"https://hal.archives-ouvertes.fr/hal-03201180v2\"\u003e2-adic parity explorations of the 3n+ 1 problem\u003c/a\u003e, hal-03201180v2 [cs.DM], 2021.",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e"
			],
			"formula": [
				"a(2*n) = a(n), a(2*n+1) = A007814(2*n+2) = A001511(n+1). - _Ralf Stephan_, Jan 31 2004",
				"a(0) = 0, a(2*n) = a(n), a(4*n+1) = 1, a(4*n+3) = 1 + a(2*n+1) (the Maple program makes use of these equations). - _Emeric Deutsch_, Aug 17 2017"
			],
			"example": [
				"13 = 1101 so a(13) = 1."
			],
			"maple": [
				"a := proc(n) if n = 0 then 0 elif `mod`(n, 2) = 0 then a((1/2)*n) elif `mod`(n, 4) = 1 then 1 else 1+a((1/2)*n-1/2) end if end proc: seq(a(n), n = 0 .. 104); # _Emeric Deutsch_, Aug 17 2017"
			],
			"mathematica": [
				"Table[If[n == 0, 0, Length@ Last@ Select[Split@ IntegerDigits[n, 2], First@ # == 1 \u0026]], {n, 0, 104}] (* _Michael De Vlieger_, Aug 17 2017 *)"
			],
			"program": [
				"(PARI) a(n) = if (n==0, 0, valuation(n/2^valuation(n, 2)+1, 2)); \\\\ _Ralf Stephan_, Aug 31 2013; _Michel Marcus_, Apr 30 2020"
			],
			"xref": [
				"Cf. A089310, A089311, A089312, A089313."
			],
			"keyword": "nonn,base",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_, Dec 22 2003",
			"ext": [
				"More terms from _Vladeta Jovovic_ and _John W. Layman_, Jan 21 2004"
			],
			"references": 14,
			"revision": 46,
			"time": "2021-07-19T21:18:17-04:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}