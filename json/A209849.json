{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A209849",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 209849,
			"data": "1,1,1,0,3,1,-2,3,6,1,0,-10,15,10,1,16,-30,-15,45,15,1,0,112,-210,35,105,21,1,-272,588,28,-735,280,210,28,1,0,-2448,5292,-2436,-1575,1008,378,36,1,7936,-18960,4140,20160,-14595,-1575,2730,630,45,1",
			"name": "Triangle read by rows: coefficients of polynomials in Sum_{k = 0..t} k^n*binomial(t,k).",
			"comment": [
				"Repeatedly applying the operator x*d/dx to (1 + x)^t (t a nonnegative integer) and evaluating at x = 1 yields Sum_{k = 0..t} k^n*binomial(t,k) = R(n,t)*2^(t-n), where R(n,t) is a polynomial in t for n = 1,2,.... The polynomial sequence {R(n,t)n\u003e=0 is of binomial type. The first few values are given in the example section below.",
				"This triangle lists the coefficients of these polynomials in ascending powers of t (omitting R(0,t) = 1). A closely related triangle is A102573, which lists the coefficients of the polynomials R(n,t) after factors of t and t*(1 + t) have been removed.",
				"This is the case m = 2 of a family of binomial type polynomials satisfying the recurrence R(n+1,t) = t*(m*(R(n,t) - R(n,t-1)) + R(n,t-1)) with R(0,t) = 1. Case m = 0 gives the falling factorials (A008275); Case m = -1 gives a signed version of A079641."
			],
			"formula": [
				"T(n,k) = Sum_{j = 0..n} (-1)^(n+k)*(-2)^(n-j)*Stirling2(n,j)*Stirling1(j,k).",
				"E.g.f.: F(x,t) := (1/2 + 1/2*exp(2*x))^t = (1 + tanh(-x))^(-t) = 1 + t*x + (t+t^2)*x^2/2! + (3*t^2+t^3)*x^3/3! + ... satisfies the delay differential equation d/dx(F(x,t)) = 2*F(x,t) - F(x,t-1).",
				"Recurrence for row polynomials R(n,t): R(n+1,t) = t*(2*R(n,t) - R(n,t-1)) with R(0,t) = 1.",
				"Let D be the backward difference operator D(f(x)) = f(x) - f(x-1). Then (x*D)^n(2^x) = 2^(x-n)*R(n,x). Cf. A079641.",
				"Discrete Dobinski-type relation: R(n,x) = 1/2^x*Sum_{k = 0..inf} (2*k)^n*x*(x - 1)*...*(x - k + 1)/k!, valid for x = 0,1,2,.... and n \u003e= 1.",
				"Other Dobinski-type relations: exp(-x)*Sum_{k = 0..inf} R(n,k)*x^k/k! = n-th row polynomial of A075497.",
				"exp(-x)*Sum_{k = 0..inf} R(n,k+1)*x^k/k! = n-th row polynomial of A154602.",
				"i^(-n)*exp(i*x)*Sum_{k = 0..inf} R(n,-k)*(-i*x)^k/k! = n-th row polynomial of A059419 where i = sqrt(-1).",
				"Writing x^[n] in place of R(n,x) we have the analog of the Bernoulli summation formula for powers of integers: Sum_{k = 1..n-1} k^[p] = 1/(p + 1)*Sum_{k = 0..p} 2^k*binomial(p+1,k)*B_k*n^[p+1-k], where B_k = [1,-1/2,1/6,0,-1/30,...] is the sequence of Bernoulli numbers.",
				"n-th row sum R(n,1) equals 2^(n-1). Alternating row sums R(n,-1) starting [-1,0,2,0,-16,0,272,...] are signed tangent numbers - see A009006 and A155585.",
				"R(n+1,2) = 2^n + 4^n = A063376(n).",
				"Triangle as a product of lower triangular arrays equals A075497*A008275.",
				"The triangle of connection constants between the polynomials (x + 1)^[n] and x^[n] appears to be A119468 = (P^2 + 1)/2, where P denotes Pascal's triangle.",
				"Also the Bell transform of the sequence 2^n*E(n,1), E(n,x) the Euler polynomials (A155585). For the definition of the Bell transform see A264428. - _Peter Luschny_, Jan 21 2016",
				"From _Peter Bala_, Jun 26 2016: (Start)",
				"With row and column numbering starting at 0:",
				"E.g.f. is exp(x)/cosh(x)*((1 + exp(2*x))/2)^t = 1 + (1 + t)*x + (3*t + t^2)*x^2/2! + (-2 + 3*t + 6*t^2 + t^3)*x^3/3! + ....",
				"Exponential Riordan array [d/dx(f(x)), f(x)] belonging to the Derivative subgroup of the Riordan group, where f(x) = log((1 + exp(2*x))/2) and df/dx = exp(x)/cosh(x) is the e.g.f. for A155585. (End)"
			],
			"example": [
				"Repeatedly applying the operator x*d/dx to (1 + x)^n and evaluating the result at x = 1 yields",
				"sum {k = 0..n} k*binomial(n,k) = n*2^(n-1).",
				"sum {k = 0..n} k^2*binomial(n,k) = (n + n^2)*2^(n-2).",
				"sum {k = 0..n} k^3*binomial(n,k) = (3*n^2 + n^3)*2^(n-3).",
				"Triangle begins:",
				".n\\k.|...1....2....3....4....5....6....7....8",
				"= = = = = = = = = = = = = = = = = = = = = = =",
				"..1..|...1",
				"..2..|...1....1",
				"..3..|...0....3....1",
				"..4..|..-2....3....6....1",
				"..5..|...0..-10...15...10....1",
				"..6..|..16..-30..-15...45...15....1",
				"..7..|...0..112..210...35..105...21....1",
				"..8..|-272..588...28.-735..280..210...28....1",
				"..."
			],
			"maple": [
				"# The function BellMatrix is defined in A264428.",
				"g := n -\u003e 2^n*euler(n,1): BellMatrix(g, 9); # _Peter Luschny_, Jan 21 2016"
			],
			"mathematica": [
				"BellMatrix[f_Function, len_] := With[{t = Array[f, len, 0]}, Table[BellY[n, k, t], {n, 0, len - 1}, {k, 0, len - 1}]];",
				"rows = 12;",
				"M = BellMatrix[2^# EulerE[#, 1]\u0026, rows];",
				"Table[M[[n, k]], {n, 2, rows}, {k, 2, n}] // Flatten (* _Jean-François Alcover_, Jun 24 2018, after _Peter Luschny_ *)"
			],
			"program": [
				"(Sage) # uses[bell_matrix from A264428]",
				"g = lambda n: sum((-2)^(n-k)*factorial(k)*stirling_number2(n,k) for k in (0..n))",
				"bell_matrix(g, 9) # _Peter Luschny_, Jan 21 2016"
			],
			"xref": [
				"Cf. A008275, A009006 (alt. row sums), A059419, A063376, A075497, A079641, A102573, A119468, A154602, A155585, A176668, A195204."
			],
			"keyword": "sign,easy,tabl",
			"offset": "1,5",
			"author": "_Peter Bala_, Mar 15 2012",
			"references": 4,
			"revision": 22,
			"time": "2020-03-28T14:04:35-04:00",
			"created": "2012-03-16T06:42:08-04:00"
		}
	]
}