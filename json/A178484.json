{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A178484",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 178484,
			"data": "1,2,3,6,4,5,9,10,12,15,18,20,25,30,36,45,50,60,75,90,100,150,180,225,300,450,900,7,8,14,21,24,27,28,35,40,42,49,54,56,63,70,72,84,98,105,108,120,125,126,135,140,147,168,175,189,196,200,210,216,245,250,252",
			"name": "For n=1,2,... list all numbers not occurring earlier which can be written as a product of the first n primes raised to some nonnegative power less than n.",
			"comment": [
				"A condensed version of sequence A178483.",
				"Every positive integer occurs exactly once in this sequence, but depending on its largest prime factor, it may appear later than much larger numbers. E.g. 7=a(29) appears after a(28)=900, and 11=a(257) appears only after a(256)=9261000.",
				"The first n^n terms are the divisors of n#^(n-1), so any term divisible by the k-th prime must appear later than position (k-1)^(k-1). - _Charlie Neder_, Mar 08 2019"
			],
			"link": [
				"Ivan Neretin, \u003ca href=\"/A178484/b178484.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"n=1 gives a(1) = 1: numbers 2^a with a \u003c 1.",
				"n=2 gives a(2..4) = [2, 3, 6]: numbers 2^a 3^b with a,b \u003c 2.",
				"n=3 gives a(5..28) = [4, 5, 9, 10, 12, 15, 18, 20, 25, 30, 36, 45, 50, 60, 75, 90, 100, 150, 180, 225, 300, 450, 900]: numbers 2^a 3^b 5^c not occurring earlier, with a,b,c \u003c 3."
			],
			"mathematica": [
				"DeleteDuplicates@Flatten@Table[Sort[Times @@ (Prime@Range@n^PadLeft[ IntegerDigits[#, n], n]) \u0026 /@ (Range[n^n] - 1)], {n, 2, 4}] (* _Ivan Neretin_, May 02 2019 *)"
			],
			"program": [
				"(PARI) { s=0; for( L=1,4, a=[]; forvec( v=vector(L,i,[0,L-1]), bittest(s,t=prod( j=1,L,prime(j)^v[L-j+1] )) \u0026 next; s+=1\u003c\u003ct; a=concat(a,t)); print1(vecsort(a)\",\"))}"
			],
			"xref": [
				"Cf. A178480, A178483, A111791."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_M. F. Hasler_, May 31 2010",
			"references": 3,
			"revision": 12,
			"time": "2019-05-04T09:45:37-04:00",
			"created": "2010-07-11T03:00:00-04:00"
		}
	]
}