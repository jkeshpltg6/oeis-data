{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A051903",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 51903,
			"data": "0,1,1,2,1,1,1,3,2,1,1,2,1,1,1,4,1,2,1,2,1,1,1,3,2,1,3,2,1,1,1,5,1,1,1,2,1,1,1,3,1,1,1,2,2,1,1,4,2,2,1,2,1,3,1,3,1,1,1,2,1,1,2,6,1,1,1,2,1,1,1,3,1,1,2,2,1,1,1,4,4,1,1,2,1,1,1,3,1,2,1,2,1,1,1,5,1,2,2,2,1,1,1,3,1",
			"name": "Maximal exponent in prime factorization of n.",
			"comment": [
				"Smallest number of factors of all factorizations of n into squarefree numbers, see also A128651, A001055. - _Reinhard Zumkeller_, Mar 30 2007",
				"Maximum number of invariant factors among abelian groups of order n. - _Álvar Ibeas_, Nov 01 2014",
				"a(n) is the highest of the frequencies of the parts of the partition having Heinz number n. We define the Heinz number of a partition p = [p_1, p_2, ..., p_r] as Product(p_j-th prime, j=1..r) (concept used by _Alois P. Heinz_ in A215366 as an \"encoding\" of a partition). For example, for the partition [1, 1, 2, 4, 10] we get 2*2*3*7*29 = 2436. Example: a(24) = 3; indeed, the partition having Heinz number 24 = 2*2*2*3 is [1,1,1,2], where the distinct parts 1 and 2 have frequencies 3 and 1, respectively. - _Emeric Deutsch_, Jun 04 2015",
				"From _Thomas Ordowski_, Dec 02 2019: (Start)",
				"a(n) is the smallest k such that b^(phi(n)+k) == b^k (mod n) for all b.",
				"The Euler phi function can be replaced by the Carmichael lambda function.",
				"Problems:",
				"(*) Are there composite numbers n \u003e 4 such that n == a(n) (mod phi(n))? By Lehmer's totient conjecture, there are no such squarefree numbers.",
				"(**) Are there odd numbers n such that a(n) \u003e 1 and n == a(n) (mod lambda(n))? These are odd numbers n such that a(n) \u003e 1 and b^n == b^a(n) (mod n) for all b.",
				"(***) Are there odd numbers n such that a(n) \u003e 1 and n == a(n) (mod ord_{n}(2))? These are odd numbers n such that a(n) \u003e 1 and 2^n == 2^a(n) (mod n).",
				"Note: if (***) do not exist, then (**) do not exist. (End)",
				"Niven (1969) proved that the asymptotic mean of this sequence is 1 + Sum_{j\u003e=2} 1 - (1/zeta(j)) (A033150). - _Amiram Eldar_, Jul 10 2020"
			],
			"link": [
				"T. D. Noe and Daniel Forgues, \u003ca href=\"/A051903/b051903.txt\"\u003eTable of n, a(n) for n = 1..100000\u003c/a\u003e (first 10000 terms from T. D. Noe)",
				"Benjamin Merlin Bumpus and Zoltan A. Kocsis, \u003ca href=\"https://arxiv.org/abs/2104.01841\"\u003eSpined categories: generalizing tree-width beyond graphs\u003c/a\u003e, arXiv:2104.01841 [math.CO], 2021.",
				"Cao Hui-Zhong, \u003ca href=\"http://www.math.bas.bg/infres/MathBalk/MB-05/MB-05-105-108.pdf\"\u003eThe Asymptotic Formulas Related to Exponents in Factoring Integers\u003c/a\u003e, Math. Balkanica, Vol. 5 (1991), Fasc. 2.",
				"Ivan Niven, \u003ca href=\"https://doi.org/10.1090/S0002-9939-1969-0241373-5\"\u003eAverages of Exponents in Factoring Integers\u003c/a\u003e, Proc. Amer. Math. Soc., Vol. 22, No. 2 (1969), pp. 356-360.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/NivensConstant.html\"\u003eNiven's Constant\u003c/a\u003e",
				"\u003ca href=\"/index/Eu#epf\"\u003eIndex entries for sequences computed from exponents in factorization of n\u003c/a\u003e"
			],
			"formula": [
				"a(n) = max_{k=1..A001221(n)} A124010(n,k). - _Reinhard Zumkeller_, Aug 27 2011",
				"a(1) = 0; for n \u003e 1, a(n) = max(A067029(n), a(A028234(n)). - _Antti Karttunen_, Aug 08 2016",
				"Conjecture: a(n) = a(A003557(n)) + 1. This relation together with a(1) = 0 defines the sequence. - _Velin Yanev_, Sep 02 2017",
				"Comment from _David J. Seal_, Sep 18 2017: (Start)",
				"This conjecture seems very easily provable to me: if the factorization of n is p1^k1 * p2^k2 * ... * pm^km, then the factorization of the largest squarefree divisor of n is p1 * p2 * ... * pm. So the factorization of A003557(n) is p1^(k1-1) * p2^(k2-1) * ... * pm^(km-1) if exponents of zero are allowed, or with the product terms that have an exponent of zero removed if they're not (if that results in an empty product, consider it to be 1 as usual).",
				"The formula then follows from the fact that provided all ki \u003e= 1, Max(k1, k2, ..., km) = Max(k1-1, k2-1, ..., km-1) + 1, and Max(k1-1, k2-1, ..., km-1) is not altered by removing the ki-1 values that are 0, provided we treat the empty Max() as being 0. That proves the formula and the provisos about empty products and Max() correspond to a(1) = 0.",
				"Also, for any n, applying the formula Max(k1, k2, ..., km) times to n = p1^k1 * p2^k2 * ... * pm^km reduces all the exponents to zero, i.e., to the case a(1) = 0, so that case and the formula generate the sequence. (End)"
			],
			"example": [
				"For n = 72 = 2^3*3^2, a(72) = max(exponents) = max(3,2) = 3."
			],
			"maple": [
				"A051903 := proc(n)",
				"        a := 0 ;",
				"        for f in ifactors(n)[2] do",
				"                a := max(a,op(2,f)) ;",
				"        end do:",
				"        a ;",
				"end proc: # _R. J. Mathar_, Apr 03 2012",
				"# second Maple program:",
				"a:= n-\u003e max(0, seq(i[2], i=ifactors(n)[2])):",
				"seq(a(n), n=1..120);  # _Alois P. Heinz_, May 09 2020"
			],
			"mathematica": [
				"Table[If[n == 1, 0, Max @@ Last /@ FactorInteger[n]], {n, 100}] (* _Ray Chandler_, Jan 24 2006 *)"
			],
			"program": [
				"(Haskell)",
				"a051903 1 = 0",
				"a051903 n = maximum $ a124010_row n -- _Reinhard Zumkeller_, May 27 2012",
				"(PARI) a(n)=if(n\u003e1,vecmax(factor(n)[,2]),0) \\\\ _Charles R Greathouse IV_, Oct 30 2012",
				"(Python)",
				"from sympy import factorint",
				"def A051903(n):",
				"    return max(factorint(n).values()) if n \u003e 1 else 0",
				"# _Chai Wah Wu_, Jan 03 2015",
				"(Scheme, with memoization-macro definec)",
				"(definec (A051903 n) (if (= 1 n) 0 (max (A067029 n) (A051903 (A028234 n))))) ;; _Antti Karttunen_, Aug 08 2016"
			],
			"xref": [
				"Average value is A033150 = 1.7052....",
				"Cf. A002322, A005361, A008479, A028234, A051904, A052409, A067029, A091050, A129132, A327295, A328310, A329885."
			],
			"keyword": "nonn,easy",
			"offset": "1,4",
			"author": "_Labos Elemer_, Dec 16 1999",
			"references": 167,
			"revision": 103,
			"time": "2021-11-30T09:33:23-05:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}