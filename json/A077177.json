{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A077177",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 77177,
			"data": "0,0,1,0,1,2,3,5,8,17,34,59,111,213,396,746,1413,2690,5147,9826,18885,36269,69952,134949,260743,504636,978311,1899832,3692980,7190329,13994206,27279898,53195986",
			"name": "Number of primitive Pythagorean triangles with perimeter equal to A002110(n), the product of the first n primes.",
			"comment": [
				"A Pythagorean triangle is a right triangle whose edge lengths are all integers; such a triangle is 'primitive' if the lengths are relatively prime.",
				"Equivalently, number of divisors of s=A070826(n) in the range (sqrt(s), sqrt(2s)). More generally, for any positive integer s, the number of primitive Pythagorean triangles with perimeter 2's equals the number of odd unitary divisors of s in the range (sqrt(s), sqrt(2s)). (A divisor d of n is 'unitary' if gcd(d, n/d) = 1.)"
			],
			"reference": [
				"A. S. Anema, \"Pythagorean Triangles with Equal Perimeters\", Scripta Mathematica, vol. 15 (1949) p. 89.",
				"Albert H. Beiler, \"Recreations in the Theory of Numbers\", chapter XIV, \"The Eternal Triangle\", pp. 131, 132.",
				"F. L. Miksa, \"Pythagorean Triangles with Equal Perimeters\", Mathematics, vol. 24 (1950), p. 52."
			],
			"link": [
				"Randall L. Rathbun, \u003ca href=\"https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;90df5cf0.0206\"\u003eEqual Perimeter primitive right triangles\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A070109(A002110(n)) = A078926(A070826(n))."
			],
			"example": [
				"a(5) = 1 since there is exactly one primitive Pythagorean triangle with perimeter 2*3*5*7*11; its edge lengths are (132, 1085, 1093). a(7) = 3; the 3 triangles have edge lengths (70941, 214060, 225509), (96460, 195789, 218261) and (142428, 156485, 211597)."
			],
			"mathematica": [
				"a[n_] := Length[Select[Divisors[s=Times@@Prime/@Range[2, n]], s\u003c#^2\u003c2s\u0026]]"
			],
			"program": [
				"(PARI) semi_peri(p)= {local(q,r,ct,tot); ct=0; tot=0; pt=0; fordiv(p,q,r=p/q-q; if(r\u003c=q\u0026\u0026r\u003e0,print(q,\",\",r,\" [\",gcd(q,r),\"] \"); if(gcd(q,r)==1,ct=ct+1; if(q*r%2==0,pt=pt+1; ); ); tot=tot+1); ); print(\"semiperimeter:\"p,\" Total sets:\",tot,\" Coprime:\",ct,\" Primitive:\",pt); } /* Lists all pairs q,r such that the triangle with edge lengths (q^2-r^2, 2qr, q^2+r^2) has semiperimeter p. */"
			],
			"xref": [
				"Cf. A002110, A070109, A070826, A078926."
			],
			"keyword": "more,nonn",
			"offset": "1,6",
			"author": "Kermit Rose and Randall L. Rathbun, Nov 29 2002",
			"ext": [
				"Edited by _Dean Hickerson_, Dec 18 2002"
			],
			"references": 0,
			"revision": 17,
			"time": "2018-09-10T14:54:11-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}