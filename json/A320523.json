{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A320523",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 320523,
			"data": "2,21,21,11,2,6,5,21,11,2,6,21,21,11,2,6,21,5,11,2,6,21,21,3,2,2,21,21,11,2,6,5,21,11,2,6,21,21,11,2,6,21,5,11,2,6,21,21,3,2,2,21,21,11,2,6,5,21,11,2,6,21,21,11,2,6,21,5,11,2,6,21,21,3,2",
			"name": "Smallest m \u003e 1 such that either n^m == n (mod 25) or n^m == 0 (mod 25).",
			"comment": [
				"This is a periodic sequence. In fact, a(n) (mod 25) == a(n + k*25) (mod 25), for any k \u003e= 0. The maximum value of a(n) is 21 = lambda(25) + 1 = 20 + 1, since 20 is the Carmichael's lambda value in 25.",
				"This sequence, omitting a(n = 10*k), predicts the convergence speed of any tetration a^^b, for any b \u003e= a \u003e 2, since A317905(n) = 1 iff a(n) \u003e 5 and A317905(n) \u003e= 2 otherwise (for any 2 \u003c= a(n) \u003c= 5)."
			],
			"reference": [
				"M. Ripà, La strana coda della serie n^n^...^n, Trento, UNI Service, Nov 2011. ISBN 978-88-6178-789-6."
			],
			"link": [
				"J. Jimenez Urroz and J. Luis A. Yebra, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL12/Yebra/yebra4.html\"\u003eOn the equation a^x == x (mod b^n)\u003c/a\u003e, J. Int. Seq. 12 (2009) #09.8.8.",
				"M. Ripà, \u003ca href=\"https://www.researchgate.net/publication/328493277_On_the_Convergence_Speed_of_Tetration\"\u003eOn the Convergence Speed of Tetration\u003c/a\u003e, ResearchGate (2018).",
				"M. Ripà, \u003ca href=\"http://vixra.org/abs/1810.0223\"\u003eOn the Convergence Speed of Tetration\u003c/a\u003e, viXra (2018).",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Carmichael_function\"\u003eCharmichael function\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Tetration\"\u003eTetration\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Euler%27s_totient_function\"\u003eEuler's totient function\u003c/a\u003e"
			],
			"formula": [
				"For any k \u003e= 0,",
				"  a( 1 + k*25) =  2;",
				"  a( 2 + k*25) = 21;",
				"  a( 3 + k*25) = 21;",
				"  a( 4 + k*25) = 11;",
				"  a( 5 + k*25) =  2;",
				"  a( 6 + k*25) =  6;",
				"  a( 7 + k*25) =  5;",
				"  a( 8 + k*25) = 21;",
				"  a( 9 + k*25) = 11;",
				"  a(10 + k*25) =  2;",
				"  a(11 + k*25) =  6;",
				"  a(12 + k*25) = 21;",
				"  a(13 + k*25) = 21;",
				"  a(14 + k*25) = 11;",
				"  a(15 + k*25) =  2;",
				"  a(16 + k*25) =  6;",
				"  a(17 + k*25) = 21;",
				"  a(18 + k*25) =  5;",
				"  a(19 + k*25) = 11;",
				"  a(20 + k*25) =  2;",
				"  a(21 + k*25) =  6;",
				"  a(22 + k*25) = 21;",
				"  a(23 + k*25) = 21;",
				"  a(24 + k*25) =  3;",
				"  a(25*(k + 1))=  2."
			],
			"example": [
				"For n = 41, a(41) = a(16) = 6, since 16^6 mod 25 = 16."
			],
			"mathematica": [
				"With[{k = 25}, Table[If[Mod[n, 5] == 0, 2, SelectFirst[Range[2, CarmichaelLambda@ k + 1], PowerMod[n, #, k] == Mod[n, k] \u0026]], {n, 75}]] (* _Michael De Vlieger_, Oct 15 2018 *)"
			],
			"program": [
				"(PARI) a(n) = {my(m=2); while ((Mod(n, 25)^m != n) \u0026\u0026 (Mod(n, 25)^m != 0), m++); m;} \\\\ _Michel Marcus_, Oct 16 2018"
			],
			"xref": [
				"Cf. A067251, A317905."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_Marco Ripà_, Oct 14 2018",
			"references": 0,
			"revision": 36,
			"time": "2018-12-18T02:27:12-05:00",
			"created": "2018-10-24T22:31:32-04:00"
		}
	]
}