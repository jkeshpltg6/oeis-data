{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A226517",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 226517,
			"data": "0,0,0,0,0,0,1,0,0,0,1,1,1,1,2,1,2,1,3,2,4,3,6,4,8,5,11,7,15,10,21,14,29,19,40,26,55,36,76,50,105,69,145,95,200,131,276,181,381,250,526,345,726,476,1002,657,1383,907,1909,1252,2635,1728,3637,2385,5020,3292,6929,4544,9564,6272,13201,8657,18221",
			"name": "Number of (19,14)-reverse multiples with n digits.",
			"comment": [
				"Comment from _Emeric Deutsch_, Aug 21 2016 (Start):",
				"Given an increasing sequence of positive integers S = {a0, a1, a2, ... }, let",
				"          F(x) = x^{a0} + x^{a1} + x^{a2} + ... .",
				"Then the g. f. for the number of palindromic compositions of n with parts in S is (see Hoggatt and Bicknell, Fibonacci Quarterly, 13(4), 1975, 350 - 356):",
				"      (1 + F(x))/(1 - F(x^2))",
				"Playing with this, I have found easily that",
				"1. number of palindromic compositions of n into {3,4,5,...} = A226916(n+4);",
				"2. number of palindromic compositions of n into {1,4,7,10,13,...} = A226916(n+6);",
				"3. number of palindromic compositions of n into {1,4} = A226517(n+10);",
				"4. number of palindromic compositions of n into {1,5} = A226516(n+11).",
				"(End)"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A226517/b226517.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"http://arxiv.org/abs/1307.0453\"\u003e2178 And All That\u003c/a\u003e, Fib. Quart., 52 (2014), 99-120.",
				"\u003ca href=\"/index/Rec#order_08\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,1,0,0,0,0,0,1)."
			],
			"formula": [
				"G.f.: x^6*(1-x^2+x^4+x^5)/(1-x^2-x^8).",
				"a(n) = a(n-2) + a(n-8) for n\u003e11, with initial values a(0)-a(11) =  0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1. [_Bruno Berselli_, Jun 17 2013]"
			],
			"maple": [
				"f:=proc(n) option remember;",
				"if",
				"n \u003c= 5 then 0",
				"elif n=6 then 1",
				"elif n \u003c= 9 then 0",
				"elif n \u003c= 11 then 1",
				"else f(n-2)+f(n-8)",
				"fi;",
				"end;",
				"[seq(f(n),n=0..120)];"
			],
			"mathematica": [
				"CoefficientList[Series[x^6 (1 - x^2 + x^4 + x^5) / (1 - x^2 - x^8), {x, 0, 80}], x] (* _Vincenzo Librandi_, Jun 18 2013 *)",
				"LinearRecurrence[{0,1,0,0,0,0,0,1},{0,0,0,0,0,0,1,0,0,0,1,1},80] (* _Harvey P. Dale_, Aug 23 2019 *)"
			],
			"xref": [
				"Cf. A214927, A226516, A226916."
			],
			"keyword": "nonn,easy,base",
			"offset": "0,15",
			"author": "_N. J. A. Sloane_, Jun 16 2013",
			"references": 4,
			"revision": 30,
			"time": "2019-08-23T15:20:52-04:00",
			"created": "2013-06-16T15:17:02-04:00"
		}
	]
}