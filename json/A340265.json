{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A340265",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 340265,
			"data": "1,2,2,3,3,5,3,6,5,8,4,10,4,10,10,11,7,14,6,15,12,15,9,19,11,17,14,19,11,25,7,22,19,24,17,27,11,25,21,30,12,33,11,30,27,32,16,38,17,36,30,34,20,41,26,38,31,40,20,50,12,38,37,43,28,52,16,47,40,52,20,54,20,48",
			"name": "a(n) = n + 1 - a(phi(n)).",
			"comment": [
				"Since phi(n) \u003c n, a(n) only depends on earlier values a(k), k \u003c n. This makes the sequences computable using dynamic programming. The motivation for the \"+ 1\" is that without it the sequence becomes half integer as a(1) would equal 1/2.",
				"1 \u003c= a(n) \u003c= n. Proof: Suppose 1 \u003c= a(k) \u003c= k for all k \u003c n. Then a(k + 1) = k + 1 + 1 - a(phi(k + 1)). As 1 \u003c= a(phi(k)) \u003c= k we have 2 \u003c= k + 1 + 1 - k \u003c= k + 1 + 1 - a(phi(k + 1)) = a(k + 1) \u003c= k + 1."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A340265/b340265.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"maple": [
				"f:= proc(n) option remember; n+ 1 - procname(numtheory:-phi(n)); end proc:",
				"f(1):= 1:",
				"map(f, [$1..100]); # _Robert Israel_, Jan 06 2021"
			],
			"mathematica": [
				"Function[, If[#1 == 1, 1, # + 1 - #0[EulerPhi@#]], Listable]@Range[70]"
			],
			"program": [
				"(Python)",
				"from sympy import totient as phi",
				"N = 100",
				"# a(n) = n + 1 - a(phi(n)), n \u003e 0, a(0) = 0",
				"a = [ 0 ] * N",
				"# a(1) = 1 + 1 - a(phi(1)) = 2 - a(1) =\u003e 2 a(1) = 2 =\u003e a(1) = 1",
				"# a(n), n \u003e 1 computed using dynamic programming",
				"a[1] = 1",
				"for n in range(2, N):",
				"  a[n] = n + 1 - a[phi(n)]",
				"print(a[1:])",
				"(Python)",
				"from sympy import totient as phi",
				"def a(n): return 1 if n==1 else n + 1 - a(phi(n))",
				"print([a(n) for n in range(1, 100)]) # _Michael S. Branicky_, Jan 03 2021",
				"(PARI) first(n) = {my(res = vector(n)); res[1] = 1; for(i = 2, n, res[i] = i + 1 - res[eulerphi(i)]); res} \\\\ _David A. Corneth_, Jan 03 2021"
			],
			"xref": [
				"Cf. A000010, A053478."
			],
			"keyword": "nonn,look",
			"offset": "1,2",
			"author": "_Emanuel Landeholm_, Jan 03 2021",
			"references": 1,
			"revision": 57,
			"time": "2021-01-07T15:16:10-05:00",
			"created": "2021-01-04T16:14:27-05:00"
		}
	]
}