{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A318259",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 318259,
			"data": "1,-1,1,5,-11,6,-61,211,-240,90,1385,-6551,11466,-8820,2520,-50521,303271,-719580,844830,-491400,113400,2702765,-19665491,58998126,-93511440,82661040,-38669400,7484400,-199360981,1704396331,-6187282920,12372329970,-14727913200,10443232800,-4086482400,681080400",
			"name": "Generalized Worpitzky numbers W_{m}(n,k) for m = 2, n \u003e= 0 and 0 \u003c= k \u003c= n, triangle read by rows.",
			"comment": [
				"The triangle can be seen as a member of a family of generalized Worpitzky numbers A028246. See the cross-references for some other members.",
				"The unsigned numbers have row sums A210657 which points to an interpretation of the unsigned numbers as a refinement of marked Schröder paths (see Josuat-Vergès and Kim)."
			],
			"link": [
				"Matthieu Josuat-Vergès and Jang Soo Kim, \u003ca href=\"http://arxiv.org/abs/1101.5608\"\u003eTouchard-Riordan formulas, T-fractions, and Jacobi's triple product identity\u003c/a\u003e, arXiv:1101.5608 [math.CO], 2011."
			],
			"formula": [
				"Let S(n, k) denote Joffe's central differences of zero (A241171) extended to the case n = 0 and k = 0 by prepending a column 1, 0, 0, 0,... to the triangle, then:",
				"T(n,k) = Sum_{j=0..k}((-1)^(k-j)*C(n-j,n-k)*Sum_{i=0..n}((-1)^i*S(n,i)*C(n-i,j)))."
			],
			"example": [
				"[0] [      1]",
				"[1] [     -1,         1]",
				"[2] [      5,       -11,        6]",
				"[3] [    -61,       211,     -240,        90]",
				"[4] [   1385,     -6551,    11466,     -8820,     2520]",
				"[5] [ -50521,    303271,  -719580,    844830,  -491400,    113400]",
				"[6] [2702765, -19665491, 58998126, -93511440, 82661040, -38669400, 7484400]"
			],
			"maple": [
				"Joffe := proc(n, k) option remember; if k \u003e n then 0 elif k = 0 then k^n else",
				"k*(2*k-1)*Joffe(n-1, k-1)+k^2*Joffe(n-1, k) fi end:",
				"T := (n, k) -\u003e add((-1)^(k-j)*binomial(n-j, n-k)*add((-1)^i*Joffe(n,i)*",
				"binomial(n-i, j), i=0..n), j=0..k):",
				"seq(seq(T(n, k), k=0..n), n=0..6);"
			],
			"mathematica": [
				"Joffe[0, 0] = 1; Joffe[n_, k_] := Joffe[n, k] = If[k\u003en, 0, If[k == 0,k^n, k*(2*k-1)*Joffe[n-1, k-1] + k^2*Joffe[n-1, k]]];",
				"T[n_, k_] := Sum[(-1)^(k-j)*Binomial[n-j, n-k]*Sum[(-1)^i*Joffe[n, i]* Binomial[n-i, j], {i, 0, n}], {j, 0, k}];",
				"Table[T[n, k], {n, 0, 7}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Feb 18 2019, from Maple *)"
			],
			"program": [
				"(Sage)",
				"def EW(m, n):",
				"    @cached_function",
				"    def S(m, n):",
				"        R.\u003cx\u003e = ZZ[]",
				"        if n == 0: return R(1)",
				"        return R(sum(binomial(m*n, m*k)*S(m, n-k)*x for k in (1..n)))",
				"    s = S(m, n).list()",
				"    c = lambda k: sum((-1)^(k-j)*binomial(n-j,n-k)*",
				"        sum((-1)^i*s[i]*binomial(n-i,j) for i in (0..n)) for j in (0..k))",
				"    return [c(k) for k in (0..n)]",
				"def A318259row(n): return EW(2, n)",
				"flatten([A318259row(n) for n in (0..6)])"
			],
			"xref": [
				"Row sums are A000007, alternating row sums are A210657.",
				"Cf. T(n,n) = A000680, T(n, 0) = A028296(n) (Gudermannian), A000364 (Euler secant), A241171 (Joffe's differences), A028246 (Worpitzky).",
				"Cf. A167374 (m=0), A028246 \u0026 A163626 (m=1), this seq (m=2), A318260 (m=3)."
			],
			"keyword": "sign,tabl",
			"offset": "0,4",
			"author": "_Peter Luschny_, Sep 06 2018",
			"references": 1,
			"revision": 18,
			"time": "2019-02-18T06:20:58-05:00",
			"created": "2018-10-06T02:55:25-04:00"
		}
	]
}