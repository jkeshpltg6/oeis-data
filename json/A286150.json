{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A286150",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 286150,
			"data": "0,2,2,5,1,5,9,13,13,9,14,8,3,8,14,20,26,7,7,26,20,27,19,42,6,42,19,27,35,43,52,62,62,52,43,35,44,34,25,51,10,51,25,34,44,54,64,33,41,16,16,41,33,64,54,65,53,88,32,23,15,23,32,88,53,65,77,89,102,116,31,39,39,31,116,102,89,77,90,76,63,101,148,30,21,30,148,101,63,76,90",
			"name": "Square array read by antidiagonals: A(n,k) = T(n XOR k, min(n,k)), where T(n,k) is sequence A001477 considered as a two-dimensional table, and XOR is bitwise-xor (A003987).",
			"comment": [
				"The array is read by descending antidiagonals as A(0,0), A(0,1), A(1,0), A(0,2), A(1,1), A(2,0), ..."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A286150/b286150.txt\"\u003eTable of n, a(n) for n = 0..10584; the first 145 antidiagonals of array\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"https://mathworld.wolfram.com/PairingFunction.html\"\u003ePairing Function\u003c/a\u003e"
			],
			"formula": [
				"A(n,k) = T(A003987(n,k), min(n,k)), where T(n,k) is sequence A001477 considered as a two-dimensional table, that is, as a pairing function from [0, 1, 2, 3, ...] x [0, 1, 2, 3, ...] to [0, 1, 2, 3, ...]."
			],
			"example": [
				"The top left 0 .. 12 x 0 .. 12 corner of the array:",
				"   0,   2,   5,   9,  14,  20,  27,  35,  44,  54,  65,  77,  90",
				"   2,   1,  13,   8,  26,  19,  43,  34,  64,  53,  89,  76, 118",
				"   5,  13,   3,   7,  42,  52,  25,  33,  88, 102,  63,  75, 150",
				"   9,   8,   7,   6,  62,  51,  41,  32, 116, 101,  87,  74, 186",
				"  14,  26,  42,  62,  10,  16,  23,  31, 148, 166, 185, 205,  86",
				"  20,  19,  52,  51,  16,  15,  39,  30, 184, 165, 225, 204, 114",
				"  27,  43,  25,  41,  23,  39,  21,  29, 224, 246, 183, 203, 146",
				"  35,  34,  33,  32,  31,  30,  29,  28, 268, 245, 223, 202, 182",
				"  44,  64,  88, 116, 148, 184, 224, 268,  36,  46,  57,  69,  82",
				"  54,  53, 102, 101, 166, 165, 246, 245,  46,  45,  81,  68, 110",
				"  65,  89,  63,  87, 185, 225, 183, 223,  57,  81,  55,  67, 142",
				"  77,  76,  75,  74, 205, 204, 203, 202,  69,  68,  67,  66, 178",
				"  90, 118, 150, 186,  86, 114, 146, 182,  82, 110, 142, 178,  78"
			],
			"mathematica": [
				"T[a_, b_]:=((a + b)^2 + 3a + b)/2; A[n_, k_]:=T[BitXor[n, k],Min[n,  k]]; Table[A[k, n - k], {n, 0, 20}, {k, 0, n}] // Flatten (* _Indranil Ghosh_, May 21 2017 *)"
			],
			"program": [
				"(Scheme)",
				"(define (A286150 n) (A286150bi (A002262 n) (A025581 n)))",
				"(define (A286150bi row col) (let ((a (A003987bi row col)) (b (min col row))) (/ (+ (expt (+ a b) 2) (* 3 a) b) 2))) ;; Where A003987bi implements bitwise-xor (A003987).",
				"(Python)",
				"def T(a, b): return ((a + b)**2 + 3*a + b)//2",
				"def A(n, k): return T(n^k, min(n, k))",
				"for n in range(21): print([A(k, n - k) for k in range(n + 1)]) # _Indranil Ghosh_, May 21 2017"
			],
			"xref": [
				"Cf. A000096 (row 0 \u0026 column 0), A000217 (main diagonal).",
				"Cf. A003987, A001477, A286108, A286109, A286145, A286147, A286151."
			],
			"keyword": "nonn,tabl",
			"offset": "0,2",
			"author": "_Antti Karttunen_, May 03 2017",
			"references": 6,
			"revision": 21,
			"time": "2021-04-30T17:14:18-04:00",
			"created": "2017-05-03T21:56:03-04:00"
		}
	]
}