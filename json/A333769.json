{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A333769",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 333769,
			"data": "1,1,2,1,1,1,1,1,3,1,1,1,2,1,2,1,1,1,1,1,2,1,4,1,1,1,1,1,1,2,1,1,2,1,1,1,1,1,3,1,1,1,1,1,1,2,1,1,2,2,1,2,1,1,3,1,5,1,1,1,1,1,1,2,2,1,1,1,1,1,1,1,3,1,1,1,1,1,3,2,2,1,1,1,1,1,1",
			"name": "Irregular triangle read by rows where row k is the sequence of run-lengths of the k-th composition in standard order.",
			"comment": [
				"A composition of n is a finite sequence of positive integers summing to n. The k-th composition in standard order (graded reverse-lexicographic, A066099) is obtained by taking the set of positions of 1's in the reversed binary expansion of k, prepending 0, taking first differences, and reversing again. This gives a bijective correspondence between nonnegative integers and integer compositions."
			],
			"example": [
				"The standard compositions and their run-lengths:",
				"   0:        () -\u003e ()",
				"   1:       (1) -\u003e (1)",
				"   2:       (2) -\u003e (1)",
				"   3:     (1,1) -\u003e (2)",
				"   4:       (3) -\u003e (1)",
				"   5:     (2,1) -\u003e (1,1)",
				"   6:     (1,2) -\u003e (1,1)",
				"   7:   (1,1,1) -\u003e (3)",
				"   8:       (4) -\u003e (1)",
				"   9:     (3,1) -\u003e (1,1)",
				"  10:     (2,2) -\u003e (2)",
				"  11:   (2,1,1) -\u003e (1,2)",
				"  12:     (1,3) -\u003e (1,1)",
				"  13:   (1,2,1) -\u003e (1,1,1)",
				"  14:   (1,1,2) -\u003e (2,1)",
				"  15: (1,1,1,1) -\u003e (4)",
				"  16:       (5) -\u003e (1)",
				"  17:     (4,1) -\u003e (1,1)",
				"  18:     (3,2) -\u003e (1,1)",
				"  19:   (3,1,1) -\u003e (1,2)",
				"For example, the 119th composition is (1,1,2,1,1,1), so row 119 is (2,1,3)."
			],
			"mathematica": [
				"stc[n_]:=Differences[Prepend[Join@@Position[Reverse[IntegerDigits[n,2]],1],0]]//Reverse;",
				"Table[Length/@Split[stc[n]],{n,0,30}]"
			],
			"xref": [
				"Row sums are A000120.",
				"Row lengths are A124767.",
				"Row k is the A333627(k)-th standard composition.",
				"A triangle counting compositions by runs-resistance is A329744.",
				"All of the following pertain to compositions in standard order (A066099):",
				"- Partial sums from the right are A048793.",
				"- Sum is A070939.",
				"- Adjacent equal pairs are counted by A124762.",
				"- Strict compositions are A233564.",
				"- Partial sums from the left are A272020.",
				"- Constant compositions are A272919.",
				"- Normal compositions are A333217.",
				"- Heinz number is A333219.",
				"- Runs-resistance is A333628.",
				"- First appearances of run-resistances are A333629.",
				"- Combinatory separations are A334030.",
				"Cf. A029931, A098504, A114994, A181819, A182850, A225620, A228351, A238279, A242882, A318928, A329747, A333489, A333630."
			],
			"keyword": "nonn,tabf",
			"offset": "0,3",
			"author": "_Gus Wiseman_, Apr 10 2020",
			"references": 9,
			"revision": 8,
			"time": "2020-05-28T05:01:19-04:00",
			"created": "2020-04-12T08:45:46-04:00"
		}
	]
}