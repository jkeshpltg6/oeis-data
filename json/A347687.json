{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A347687",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 347687,
			"data": "0,1,0,1,1,0,1,0,1,0,1,2,2,1,0,1,0,0,0,1,0,1,3,2,2,3,1,0,1,0,3,0,3,0,1,0,1,4,0,2,2,0,4,1,0,1,0,3,0,0,0,3,0,1,0,1,5,4,3,2,2,3,4,5,1,0,1,0,0,0,5,0,5,0,0,0,1,0,1,6,4,3,5,2,2,5,3,4,6,1,0",
			"name": "Triangle read by rows: T(n,k) (1\u003c=k\u003c=n) = (r(n+k)-r(k))/(2*n), where {r(i), i\u003e=1} is the n-th row of array A347684.",
			"comment": [
				"Stated another way, T(n,k) = (1/(2*n)) * (A347684(n,k+n) - A347684(n,k)) for 1 \u003c= k \u003c= n.",
				"Conjecture: Row n is not just the rescaled differences between the first two blocks of n terms of row n of A347684, but is in fact the rescaled differences between any two successive blocks of n terms that start at positions == 1 mod n. See also the comments in A347684.",
				"It would be nice to have an independent characterization of this triangle. There is a lot of structure - see the link for the first 40 rows.",
				"Except for the final 0 in each row, the rows begin with 1 and are palindromic. T(n,k) = 0 iff gcd(n,k) \u003e 1."
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/A347687/b347687.txt\"\u003eTable of n, a(n) for n = 1..5050\u003c/a\u003e [Rows 1 through 100, flattened]",
				"N. J. A. Sloane, \u003ca href=\"/A347687/a347687.txt\"\u003eRows 1 through 40\u003c/a\u003e"
			],
			"example": [
				"The initial rows are:",
				"1,  [0],",
				"2,  [1, 0],",
				"3,  [1, 1, 0],",
				"4,  [1, 0, 1, 0],",
				"5,  [1, 2, 2, 1, 0],",
				"6,  [1, 0, 0, 0, 1, 0],",
				"7,  [1, 3, 2, 2, 3, 1, 0],",
				"8,  [1, 0, 3, 0, 3, 0, 1, 0],",
				"9,  [1, 4, 0, 2, 2, 0, 4, 1, 0],",
				"10, [1, 0, 3, 0, 0, 0, 3, 0, 1, 0],",
				"11, [1, 5, 4, 3, 2, 2, 3, 4, 5, 1, 0],",
				"12, [1, 0, 0, 0, 5, 0, 5, 0, 0, 0, 1, 0],",
				"...",
				"Row 5 of A347684 starts 1, 9, 11, 9, 0, 11, 29, 31, 19, 0, 21, 49, 51, 29, 0, 31,... Subtracting the first block of five terms from the second block of five terms we get [11, 29, 31, 19, 0] - [1, 9, 11, 9, 0] = [10, 20, 20, 10, 0], and after dividing by 10 we get [1, 2, 2, 1, 0], which is row 5 of the present triangle."
			],
			"maple": [
				"myfun1 := proc(A,B) local Ar,Br;",
				"if igcd(A,B) \u003e 1 then return(0); fi;",
				"  Ar:=(A)^(-1) mod B;",
				"   if 2*Ar \u003e B then Ar:=B-Ar; fi;",
				"  Br:=(B)^(-1) mod A;",
				"   if 2*Br \u003e A then Br:=A-Br; fi;",
				"A*Ar+B*Br;",
				"end;",
				"T87:=(n,k) -\u003e (myfun1(n,n+k)-myfun1(n,k))/(2*n);",
				"for n from 1 to 20 do lprint([seq(T87(n,k),k=1..n)]); od:"
			],
			"xref": [
				"Cf. A347681, A347682, A347683, A347684."
			],
			"keyword": "nonn,tabl",
			"offset": "1,12",
			"author": "_N. J. A. Sloane_, Sep 19 2021",
			"references": 2,
			"revision": 31,
			"time": "2021-09-24T10:18:27-04:00",
			"created": "2021-09-19T23:49:41-04:00"
		}
	]
}