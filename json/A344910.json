{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A344910",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 344910,
			"data": "1,1,1,4,1,1,1,6,1,3,32,1,6,1,3,1,80,1,3,1,15,128,1,720,1,18,1,45,1,2240,1,360,1,45,1,315,2048,1,6720,1,720,1,45,1,315,1,322560,1,90720,1,1080,1,945,1,2835,8192,1,1612800,1,181440,1,5400,1,1890,1,14175",
			"name": "T(n, k) = denominator([x^k] [z^n] ((1 - i*z)/(1 + i*z))^(i*x)*(1 + z^2)^(-3/4)). Denominators of the coefficients of the symmetric Meixner-Pollaczek polynomials P^(3/4)_{n}(x, Pi/2). Triangle read by rows, T(n, k) for 0 \u003c= k \u003c= n.",
			"link": [
				"R. Koekoek, P. A. Lesky, and R. F. Swarttouw, \u003ca href=\"https://doi.org/10.1007/978-3-642-05014-5\"\u003eHypergeometric Orthogonal Polynomials and Their q-Analogues.\u003c/a\u003e Springer, 2010. (p. 213-216.)"
			],
			"formula": [
				"T(n, k) = denominator([x^k] P(n, x), where P(n, x) = i^n*Sum_{k=0..n} (-1)^k* binomial(-3/4 + i*x, k)*binomial(-3/4 - i*x, n - k). The polynomials have the recurrence P(n, x) = (1/n)*(2*x*P(n - 1, x) - (n - 1/2)*P(n - 2, x))), starting with P(0, x) = 1 and P(1, x) = 2*x."
			],
			"example": [
				"Triangle starts:",
				"[0] 1;",
				"[1] 1, 1;",
				"[2] 4, 1, 1;",
				"[3] 1, 6, 1, 3;",
				"[4] 32, 1, 6, 1, 3;",
				"[5] 1, 80, 1, 3, 1, 15;",
				"[6] 128, 1, 720, 1, 18, 1, 45;",
				"[7] 1, 2240, 1, 360, 1, 45, 1, 315;",
				"[8] 2048, 1, 6720, 1, 720, 1, 45, 1, 315;",
				"[9] 1, 322560, 1, 90720, 1, 1080, 1, 945, 1, 2835."
			],
			"maple": [
				"gf := ((1 - I*z)/(1 + I*z))^(I*x)*(1 + z^2)^(-3/4):",
				"serz := series(gf, z, 22): coeffz := n -\u003e coeff(serz, z, n):",
				"row := n -\u003e seq(denom(coeff(coeffz(n), x, k)), k = 0..n):",
				"seq(row(n), n = 0..10);",
				"# Alternative:",
				"CoeffList := p -\u003e denom(PolynomialTools:-CoefficientList(p, x)):",
				"P := proc(n) option remember; if n = 0 then 1 elif n = 1 then 2*x else",
				"expand((1/n)*(2*x*P(n - 1, x) - (n - 1/2)*P(n - 2, x))) fi end:",
				"ListTools:-Flatten([seq(CoeffList(P(n)), n = 0..10)]);"
			],
			"mathematica": [
				"ForceSimpl[a_] := Collect[Expand[Simplify[FunctionExpand[a]]], x]",
				"f[n_] := I^n Sum[(-1)^k Binomial[-3/4 + I*x, k] Binomial[-3/4 - I*x, n-k], {k, 0, n}] // ForceSimpl;",
				"row[n_] := CoefficientList[f[n], x] // Denominator;",
				"Table[row[n], {n, 0, 10}] // Flatten"
			],
			"xref": [
				"Cf. A344909 (numerators).",
				"Cf. A088802 and A123854 (denominator(binomial(1/4, n)) for column 0.",
				"Cf. A049606 (numerator(n!/2^n)) for column n."
			],
			"keyword": "nonn,tabl,frac",
			"offset": "0,4",
			"author": "_Peter Luschny_, Jul 08 2021",
			"references": 1,
			"revision": 16,
			"time": "2021-07-08T14:16:26-04:00",
			"created": "2021-07-08T12:26:10-04:00"
		}
	]
}