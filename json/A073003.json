{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A073003",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 73003,
			"data": "5,9,6,3,4,7,3,6,2,3,2,3,1,9,4,0,7,4,3,4,1,0,7,8,4,9,9,3,6,9,2,7,9,3,7,6,0,7,4,1,7,7,8,6,0,1,5,2,5,4,8,7,8,1,5,7,3,4,8,4,9,1,0,4,8,2,3,2,7,2,1,9,1,1,4,8,7,4,4,1,7,4,7,0,4,3,0,4,9,7,0,9,3,6,1,2,7,6,0,3,4,4,2,3,7",
			"name": "Decimal expansion of -exp(1)*Ei(-1), also called Gompertz's constant, or the Euler-Gompertz constant.",
			"comment": [
				"0! - 1! + 2! - 3! + 4! - 5! + ... = (Borel) Sum_{n\u003e=0} (-y)^n n! = KummerU(1,1,1/y)/y.",
				"Decimal expansion of phi(1) where phi(x) = Integral_{t\u003e=0} e^-t/(x+t) dt. - _Benoit Cloitre_, Apr 11 2003",
				"The divergent series g(x=1,m) = 1^m*1! - 2^m*2! + 3^m*3! - 4^m*4! + ..., m =\u003e -1, is intimately related to Gompertz's constant. We discovered that g(x=1,m) = (-1)^m * (A040027(m) - A000110(m+1) * A073003) with A000110 the Bell numbers and A040027 a sequence that was published by Gould, see for more information A163940. - _Johannes W. Meijer_, Oct 16 2009",
				"Named by Le Lionnais (1983) after the English self-educated mathematician and actuary Benjamin Gompertz (1779 - 1865). It was named the Euler-Gompertz constant by Finch (2003). Lagarias (2013) noted that he has not located this constant in Gompertz's writings. - _Amiram Eldar_, Aug 15 2020"
			],
			"reference": [
				"Bruce C. Berndt, Ramanujan's notebooks Part II, Springer, p. 171",
				"Bruce C. Berndt, Ramanujan's notebooks Part I, Springer, p. 144-145.",
				"S. R. Finch, Mathematical Constants, Cambridge, 2003, pp. 424-425.",
				"Francois Le Lionnais, Les nombres remarquables, Paris: Hermann, 1983. See p. 29.",
				"H. S. Wall, Analytic Theory of Continued Fractions, Van Nostrand, New York, 1948, p. 356."
			],
			"link": [
				"Robert Price, \u003ca href=\"/A073003/b073003.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"A. I. Aptekarev, \u003ca href=\"http://arxiv.org/abs/0902.1768\"\u003eOn linear forms containing the Euler constant\u003c/a\u003e, arXiv:0902.1768 [math.NT], 2009. [From _R. J. Mathar_, Feb 14 2009]",
				"Richard P. Brent, M. L. Glasser, and Anthony J. Guttmann, \u003ca href=\"https://arxiv.org/abs/1812.00316\"\u003eA Conjectured Integer Sequence Arising From the Exponential Integral\u003c/a\u003e, arXiv:1812.00316 [math.NT], 2018.",
				"G. H. Hardy, \u003ca href=\"http://www.archive.org/texts/flipbook/flippy.php?id=divergentseries033523mbp\"\u003eDivergent Series \u003c/a\u003e, Oxford University Press, 1949. p. 29. - _Johannes W. Meijer_, Oct 16 2009",
				"Jeffrey C. Lagarias, \u003ca href=\"https://doi.org/10.1090/S0273-0979-2013-01423-X\"\u003eEuler's constant: Euler's work and modern developments\u003c/a\u003e, Bull. Amer. Math. Soc., Vol. 50, No. 4 (2013), pp. 527-628, \u003ca href=\"http://arxiv.org/abs/1303.1856\"\u003epreprint\u003c/a\u003e, arXiv:1303.1856 [math.NT], 2013.",
				"István Mezo, \u003ca href=\"http://www.naturalspublishing.com/files/published/j18jp677r69ri8.pdf\"\u003eGompertz constant, Gregory coefficients and a series of the logarithm function\u003c/a\u003e, Journal of Analysis \u0026 Number Theory, Vol. 2, No. 2 (2014), pp. 33-36.",
				"Simon Plouffe, \u003ca href=\"http://www.worldwideschool.org/library/books/sci/math/MiscellaneousMathematicalConstants/chap20.html\"\u003e-exp(1)*Ei(-1)\u003c/a\u003e",
				"Tanguy Rivoal, \u003ca href=\"http://doi.org/10.1307/mmj/1339011525\"\u003eOn the arithmetic nature of the values of the gamma function, Euler's constant, and Gompertz's constant\u003c/a\u003e, Michigan Math. J., Vol. 61, No. 2 (2012), pp. 239-254.",
				"Ed Sandifer, \u003ca href=\"https://www.maa.org/sites/default/files/pdf/editorial/euler/How%20Euler%20Did%20It%2032%20divergent%20series.pdf\"\u003eDivergent Series\u003c/a\u003e, How Euler Did It, MAA Online, June 2006. - _Johannes W. Meijer_, Oct 16 2009",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/GompertzConstant.html\"\u003eGompertz Constant\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ExponentialIntegral.html\"\u003eExponential Integral\u003c/a\u003e"
			],
			"formula": [
				"phi(1) = e*(Sum_{k\u003e=1} (-1)^(k-1)/(k*k!) - Gamma) = 0.596347362323194... where Gamma is the Euler constant.",
				"G = 0.596347... = 1/(1+1/(1+1/(1+2/(1+2/(1+3/(1+3/(1+4/(1+4/(1+5/(1+5/(1+6/(... - _Philippe Deléham_, Aug 14 2005",
				"Equals A001113*A099285. - _Johannes W. Meijer_, Oct 16 2009",
				"From _Peter Bala_, Oct 11 2012: (Start)",
				"Stieltjes found the continued fraction representation G = 1/(2 - 1^2/(4 - 2^2/(6 - 3^2/(8 - ...)))). See [Wall, Chapter 18, (92.7) with a = 1]. The sequence of convergents to the continued fraction begins [1/2, 4/7, 20/34, 124/209, ...]. The numerators are in A002793 and the denominators in A002720.",
				"Also, 1 - G has the continued fraction representation 1/(3 - 2/(5 - 6/(7 - ... -n*(n+1)/((2*n+3) - ...)))) with convergents beginning [1/3, 5/13, 29/73, 201/501, ...]. The numerators are in A201203 (unsigned) and the denominators are in A000262.",
				"(End)",
				"G = f(1) with f solution to the o.d.e. x^2*f'(x) + (x+1)*f(x)=1 such that f(0)=1. - _Jean-François Alcover_, May 28 2013",
				"From _Amiram Eldar_, Aug 15 2020: (Start)",
				"Equals Integral_{x=0..1} 1/(1-log(x)) dx.",
				"Equals Integral_{x=1..oo} exp(1-x)/x dx.",
				"Equals Integral_{x=0..oo} exp(-x)*log(x+1) dx.",
				"Equals Integral_{x=0..oo} exp(-x)/(x+1) dx. (End)",
				"From _Gleb Koloskov_, May 01 2021: (Start)",
				"Equals Integral_{x=0..1} LambertW(e/x)-1 dx.",
				"Equals Integral_{x=0..1} 1+1/LambertW(-1,-x/e) dx. (End)",
				"Equals lim_{n-\u003einfinity} A040027(n)/A000110(n+1). - _Vaclav Kotesovec_, Feb 22 2021"
			],
			"example": [
				"0.59634736232319407434107849936927937607417786015254878157348491..."
			],
			"mathematica": [
				"RealDigits[N[-Exp[1]*ExpIntegralEi[-1], 105]][[1]]",
				"G = 1/Fold[Function[2*#2 - #2^2/#1], 2, Reverse[Range[10^4]]] // N[#, 105]\u0026; RealDigits[G] // First (* _Jean-François Alcover_, Sep 19 2014 *)"
			],
			"program": [
				"(PARI) eint1(1)*exp(1) \\\\ _Charles R Greathouse IV_, Apr 23 2013",
				"(MAGMA) SetDefaultRealField(RealField(100)); ExponentialIntegralE1(1)*Exp(1); // _G. C. Greubel_, Dec 04 2018",
				"(Sage) numerical_approx(exp_integral_e(1,1)*exp(1), digits=100) # _G. C. Greubel_, Dec 04 2018"
			],
			"xref": [
				"Cf. A000522 (arrangements), A001620, A000262, A002720, A002793, A058006 (alternating factorial sums), A153229, A201203, A283743 (Ei(1)/e)."
			],
			"keyword": "cons,nonn",
			"offset": "0,1",
			"author": "_Robert G. Wilson v_, Aug 03 2002",
			"ext": [
				"Additional references from _Gerald McGarvey_, Oct 10 2005",
				"Link corrected by _Johannes W. Meijer_, Aug 01 2009"
			],
			"references": 21,
			"revision": 93,
			"time": "2021-05-28T10:37:40-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}