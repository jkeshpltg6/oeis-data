{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A101337",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 101337,
			"data": "1,2,3,4,5,6,7,8,9,1,2,5,10,17,26,37,50,65,82,4,5,8,13,20,29,40,53,68,85,9,10,13,18,25,34,45,58,73,90,16,17,20,25,32,41,52,65,80,97,25,26,29,34,41,50,61,74,89,106,36,37,40,45,52,61,72,85,100,117,49,50,53,58,65",
			"name": "Sum of (each digit of n raised to the power (number of digits in n)).",
			"comment": [
				"Sometimes referred to as \"narcissistic function\" (in base 10). Fixed points are the narcissistic (or Armstrong, or plus perfect) numbers A005188. - _M. F. Hasler_, Nov 17 2019"
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A101337/b101337.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Narcissistic_number\"\u003eNarcissistic number\u003c/a\u003e, as of Nov 18 2019."
			],
			"formula": [
				"a(n) \u003c= A055642(n)*9^A055642(n) with equality for all n = 10^k - 1. Write n = 10^x to get a(n) \u003c n when 1+log_10(x+1) \u003c (x+1)(1-log_10(9)) \u003c=\u003e x \u003e 59.85. It appears that a(n) \u003c n already for all n \u003e 1.02*10^59. - _M. F. Hasler_, Nov 17 2019"
			],
			"example": [
				"a(75) = 7^2 + 5^2 = 74 and a(705) = 7^3 + 0^3 + 5^3 = 468.",
				"a(1.02e59 - 1) = 102429587095122578993551250282047487264694110769657513064859 ~ 1.024e59 is an example of n close to the limit beyond which a(n) \u003c n for all n. - _M. F. Hasler_, Nov 17 2019"
			],
			"mathematica": [
				"Array[Total[IntegerDigits[#]^IntegerLength[#]]\u0026,80] (* _Harvey P. Dale_, Aug 27 2011 *)"
			],
			"program": [
				"(PARI) a(n)=my(d=digits(n)); sum(i=1,#d, d[i]^#d) \\\\ _Charles R Greathouse IV_, Aug 10 2017",
				"(PARI) apply( A101337(n)=vecsum([d^#n|d\u003c-n=digits(n)]), [0..99]) \\\\ _M. F. Hasler_, Nov 17 2019",
				"(Python)",
				"def A101337(n):",
				"    s = str(n)",
				"    l = len(s)",
				"    return sum(int(d)**l for d in s) # _Chai Wah Wu_, Feb 26 2019",
				"(MAGMA) f:=func\u003cn|\u0026+[Intseq(n)[i]^#Intseq(n):i in [1..#Intseq(n)]]\u003e; [f(n):n in [1..75]]; // _Marius A. Burtea_, Nov 18 2019"
			],
			"xref": [
				"Cf. A179239, A306360."
			],
			"keyword": "base,easy,nonn",
			"offset": "1,2",
			"author": "_Gordon Hamilton_, Dec 24 2004",
			"ext": [
				"Name changed by _Axel Harvey_, Dec 26 2011; edited by _M. F. Hasler_, Nov 17 2019"
			],
			"references": 18,
			"revision": 53,
			"time": "2019-11-18T22:14:14-05:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}