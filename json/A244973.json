{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A244973",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 244973,
			"data": "1,-1,-1,17,-65,49,881,-5489,12223,42785,-479951,1746271,440881,-39651457,212039855,-326783183,-2817155137,23175692033,-68726927071,-128775914225,2285692892785,-10156877725985,6169206210815,196882990135745,-1274770281690575",
			"name": "a(n) = Sum_{k=0..n} C(n,k)^2*C(2k,k)(-1)^k, where C(n,k) denotes the binomial coefficient n!/(k!(n-k)!).",
			"comment": [
				"Zhi-Wei Sun introduced this sequence in arXiv:1407.0967. For any prime p \u003e 5, he proved that Sum_{k=1..p-1} a(k)/k^2 == 0 (mod p) and Sum_{k=1..p-1} a(k)/k == 0 (mod p^2). This is quite similar to Wolstenholme's congruences Sum_{k=1..p-1} 1/k^2 == 0 (mod p) and Sum_{k=1..p-1} 1/k == 0 (mod p^2) for any prime p \u003e 3.",
				"Conjecture: For any prime p \u003e 5 and positive integer n, the number (a(p*n)-a(n))/(p*n)^3 is always a p-adic integer.",
				"The author proved a weaker version of this in arXiv:1610.03384. - _Zhi-Wei Sun_, Nov 12 2016"
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A244973/b244973.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"V. J. Guo, G.-S. Mao and H. Pan, \u003ca href=\"http://dx.doi.org/10.1080/10236198.2016.1188088\"\u003eProof of a conjecture involving Sun polynomials\u003c/a\u003e, J. Difference Equ. Appl., 22(2016), no. 8, 1184-1197; also \u003ca href=\"https://arxiv.org/abs/1511.04005\"\u003earXiv:1511.04005 [math.NT]\u003c/a\u003e, 2015.",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1407.0967\"\u003eCongruences involving g_n(x) = sum_{k=0}^n C(n,k)^2*C(2k,k)*x^k\u003c/a\u003e, arXiv:1407.0967 [math.NT], 2014-2016.",
				"Zhi-Wei Sun, \u003ca href=\"http://dx.doi.org/10.1007/s11139-015-9727-3\"\u003eCongruences involving  g_n(x) = sum_{k=0}^n C(n,k)^2*C(2k,k)*x^k\u003c/a\u003e, Ramanujan J. 40 (2016), no. 3, 511-533.",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1610.03384\"\u003eSupercongruences involving Lucas sequences, arXiv:1016.03384 [math.NT], 2016."
			],
			"formula": [
				"Recurrence (obtained via the Zeilberger algorithm):",
				"(n+3)^2*(4n+5)*a(n+3) + (20n^3+125n^2+254n+165)*a(n+2) + (76n^3+399n^2+678n+375)*a(n+1) - 25*(n+1)^2*(4n+9)*a(n) = 0.",
				"Lim sup n-\u003einfinity |a(n)|^(1/n) = 5. - _Vaclav Kotesovec_, Jul 13 2014",
				"a(n) = Sum_{k=0..n}C(n,2k)^2*C(2k,k)*(-1)^(n-k) = Sum_{k=0..n}C(n,k)*C(n,2k)*C(n-k,k)*(-1)^(n-k). - _Zhi-Wei Sun_, Nov 12 2016"
			],
			"example": [
				"a(3) = 17 since C(3,0)^2*C(2*0,0) - C(3,1)^2*C(2,1) + C(3,2)^2*C(4,2) - C(3,3)^2*C(6,3) = 1 - 18 + 54 - 20 = 17."
			],
			"mathematica": [
				"s[n_]:=Sum[Binomial[n,k]^2*Binomial[2k,k](-1)^k,{k,0,n}]",
				"Table[s[n],{n,0,20}]"
			],
			"program": [
				"(PARI) a(n) = sum(k=0, n, (-1)^k*binomial(n,k)^2*binomial(2*k,k)); \\\\ _Michel Marcus_, Nov 13 2016"
			],
			"xref": [
				"Cf. A000172, A002893, A275027, A277640."
			],
			"keyword": "sign",
			"offset": "0,4",
			"author": "_Zhi-Wei Sun_, Jul 08 2014",
			"references": 7,
			"revision": 33,
			"time": "2016-11-14T03:54:52-05:00",
			"created": "2014-07-09T00:22:42-04:00"
		}
	]
}