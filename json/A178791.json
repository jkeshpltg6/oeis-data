{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A178791",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 178791,
			"data": "1,-7,117,-2441,57449,-1453635,38609845,-1061792695,29973352185,-863536596143,25288254409373,-750531594051981,22525211241191881,-682459907754004723,20845409947239778533,-641211780685502724425",
			"name": "The arithmetic mean of (2k+1)(-1)^k*A_k (k=0,...,n-1), where A_0, A_1,... are Apéry numbers given by A005259.",
			"comment": [
				"On Jun 14 2010, _Zhi-Wei Sun_ conjectured that the number a(n) = n^{-1}*Sum_{k=0..n-1} (2*k+1)*(-1)^k*A_k is always an integer and that a(p) = p(p/3) (mod p^3) for any prime p\u003e3. He also formulated the following conjecture: If p=1 (mod 3) is a prime and p = x^2 + 3y^2 with x,y integers then Sum_{k=0..p-1} (-1)^k*A_k = 4*x^2 - 2*p (mod p^2); if p=2 (mod 3) is a prime then Sum_{k=0..p-1 }(-1)^k*A_k=0 (mod p^2)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A178791/b178791.txt\"\u003eTable of n, a(n) for n = 1..500\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1006.2776\"\u003eArithmetic properties of Apery numbers and central Delannoy numbers\u003c/a\u003e, arXiv:1006.2776 [math.NT], 2010-2011."
			],
			"formula": [
				"G.f.: apart from the minus signs (just replace x by -x) the generating function is 2*x*G/(x+1) + Int((x-1)*G/(x+1)^2, x) where G is the generating function of A005259. - _Mark van Hoeij_, May 07 2013",
				"a(n) ~ -(-1)^n * 2^(3/4) * (1 + sqrt(2))^(4*n) / (24 * (Pi*n)^(3/2)). - _Vaclav Kotesovec_, Jan 24 2019"
			],
			"example": [
				"For n=3 we have a(3) = (A_0 - 3A_1 + 5A_2)/3 = (1 - 3*5 + 5*73)/3 = 117."
			],
			"maple": [
				"G := (-1/2)*(3*x-3+(x^2-34*x+1)^(1/2))*(x+1)^(-2)*hypergeom([1/3, 2/3], [1], (-1/2)*(x^2-7*x+1)*(x+1)^(-3)*(x^2-34*x+1)^(1/2)+(1/2)*(x^3+30*x^2 -24*x+1)*(x+1)^(-3))^2;",
				"ogf := 2*x*G/(x+1)+Int((x-1)*G/(x+1)^2,x);",
				"series(ogf, x=0, 25);",
				"series(-subs(x=-x,%), x=0, 25); # _Mark van Hoeij_, May 07 2013"
			],
			"mathematica": [
				"Apery[n_]:= Sum[Binomial[n+k,k]^2Binomial[n,k]^2,{k,0,n}]; AA[n_]:= Sum[(2k+1)(-1)^k*Apery[k],{k,0,n-1}]/n; Table[AA[n],{n,25}]"
			],
			"program": [
				"(PARI) A(n) = sum(k=0, n, (binomial(n, k)*binomial(n+k, k))^2); \\\\ A005259",
				"a(n) = sum(k=0, n-1, (2*k+1)*(-1)^k*A(k))/n; \\\\ _Michel Marcus_, Jan 24 2019"
			],
			"xref": [
				"Cf. A178790, A005259, A173774."
			],
			"keyword": "sign",
			"offset": "1,2",
			"author": "_Zhi-Wei Sun_, Jun 14 2010",
			"references": 7,
			"revision": 24,
			"time": "2019-01-24T08:14:47-05:00",
			"created": "2010-07-11T03:00:00-04:00"
		}
	]
}