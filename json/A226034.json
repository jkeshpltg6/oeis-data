{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A226034",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 226034,
			"data": "1,11,73,368,1552,5755,19337,60054,174801,481760,1266992,3198963,7791921,18382187,42139440,94126547,205343040,438390320,917501570,1885269635,3808353889,7571955531,14833349529,28657374307,54646711136,102932171227,191644299945",
			"name": "Expansion of f(-x)^6 / (chi(x) * phi(-x)^6) in powers of x where phi(), chi(), f() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A226034/b226034.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Vaclav Kotesovec, \u003ca href=\"http://arxiv.org/abs/1509.08708\"\u003eA method of finding the asymptotics of q-series based on the convolution of generating functions\u003c/a\u003e, arXiv:1509.08708 [math.CO], Sep 30 2015",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(-19/24) * eta(q^2)^4 * eta(q^3)^6 * eta(q^4) / eta(q)^11 in powers of q.",
				"a(n) = 1/12 * A001935(9*n + 7).",
				"a(n) ~ exp(3*Pi*sqrt(n/2)) / (2^(19/4) * 3^(5/2) * n^(3/4)). - _Vaclav Kotesovec_, Oct 14 2015"
			],
			"example": [
				"1 + 11*x + 73*x^2 + 368*x^3 + 1552*x^4 + 5755*x^5 + 19337*x^6 + 60054*x^7 + ...",
				"q^19 + 11*q^43 + 73*q^67 + 368*q^91 + 1552*q^115 + 5755*q^139 + 19337*q^163 + ..."
			],
			"mathematica": [
				"nmax=60; CoefficientList[Series[Product[(1+x^k)^4 * (1-x^(3*k))^6 * (1-x^(4*k)) / (1-x^k)^7,{k,1,nmax}],{x,0,nmax}],x] (* _Vaclav Kotesovec_, Oct 14 2015 *)",
				"eta[q_]:= q^(1/24)*QPochhammer[q]; a[n_]:= SeriesCoefficient[q^(-19/24)* eta[q^2]^4*eta[q^3]^6*eta[q^4]/eta[q]^11, {q, 0, n}]; Table[a[n], {n, 0, 50}] (* _G. C. Greubel_, Mar 15 2018 *)"
			],
			"program": [
				"(PARI) {a(n) = local(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A)^4 * eta(x^3 + A)^6 * eta(x^4 + A) / eta(x + A)^11, n))}"
			],
			"xref": [
				"Cf. A001935."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Michael Somos_, May 28 2013",
			"references": 2,
			"revision": 22,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2013-05-30T04:45:28-04:00"
		}
	]
}