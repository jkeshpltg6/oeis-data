{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A288173",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 288173,
			"data": "0,0,1,0,0,0,1,0,0,1,0,0,0,0,1,0,0,1,0,0,0,1,0,0,1,0,0,0,1,0,0,0,1,0,0,1,0,0,0,1,0,0,1,0,0,0,0,1,0,0,1,0,0,0,1,0,0,1,0,0,0,0,1,0,0,1,0,0,0,0,1,0,0,1,0,0,0,1,0,0,1,0,0,0,0,1",
			"name": "Fixed point of the mapping 00-\u003e0010, 1-\u003e001, starting with 00.",
			"comment": [
				"From _Michel Dekking_, Feb 21 2020: (Start)",
				"This sequence is a morphic sequence, i.e., the letter-to-letter image of a fixed point of a morphism mu. In fact, one can take the alphabet {1,2,...,8}, with the morphism",
				"      mu:  1-\u003e123, 2-\u003e14, 3-\u003e56, 4-\u003e1, 5-\u003e27, 6-\u003e83, 7-\u003e56, 8-\u003e14,",
				"and the letter-to-letter map lambda defined by",
				"      lambda:  1-\u003e0, 2-\u003e0, 3-\u003e1, 4-\u003e0, 5-\u003e0, 6-\u003e1, 7-\u003e0, 8-\u003e0.",
				"Then (a(n)) = lambda(x), where x = 12314561231278... is the unique fixed point of the morphism mu.",
				"How does one see this? We note first that the mapping  SR: 00-\u003e0010, 1-\u003e010, is an algorithmic procedure given by StringReplace in Mathematica. This makes it hard in general to describe iterates of it.",
				"To circumvent this, we do the following: define the three words",
				"      A:=001, B:=0001, C:=00001.",
				"Then (a(n)) is a concatenation of these three words, and moreover the StringReplace algorithm SR acts context free on concatenations of these three words:",
				"      SR(001) = 0010001, SR(0001) = 00100001, SR(00001) = 00100010001.",
				"This induces a morphism alpha on the alphabet {A,B,C} given by",
				"      alpha(A) = AB, alpha(B) = AC, alpha(C) = ABB.",
				"It is clear that a(n) = delta(t(n)),  where delta is given by:",
				"      delta(A) = 001, delta(B) = 0001, delta(C) = 00001,",
				"and t is the unique fixed point t = ABACABABB.... of the morphism tau.",
				"In my paper  \"Morphic words, Beatty sequences and integer images of the Fibonacci language\" such a map delta is called a decoration. It is well-known that decorated fixed points are morphic sequences, and the 'natural' algorithm to achieve this yields a morphism on an alphabet of 3+4+5 = 12 symbols. The algorithm gives a large freedom in defining the morphism. This permits to reduce the size of the alphabet, and to obtain the morphism mu and the letter-to-letter map lambda given above.",
				"See A288176 for a proof of Kimberling's conjecture on the number of letters (0's and 1's) in the n-th iteration of the mapping SR. (End)"
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A288173/b288173.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Michel Dekking, \u003ca href=\"https://doi.org/10.1016/j.tcs.2019.12.036\"\u003eMorphic words, Beatty sequences and integer images of the Fibonacci language\u003c/a\u003e, Theoretical Computer Science 809, 407-417 (2020)."
			],
			"example": [
				"Iterates, starting with 00:",
				"00",
				"0010",
				"00100010",
				"0010001001000010",
				"0010001001000010010001001000100010"
			],
			"mathematica": [
				"s = {0, 0}; w[0] = StringJoin[Map[ToString, s]];",
				"w[n_] := StringReplace[w[n - 1], {\"00\" -\u003e \"0010\", \"1\" -\u003e \"001\"}]",
				"Table[w[n], {n, 0, 8}]",
				"st = ToCharacterCode[w[11]] - 48   (* A288173 *)",
				"Flatten[Position[st, 0]]  (* A288174 *)",
				"Flatten[Position[st, 1]]  (* A288175 *)",
				"Table[StringLength[w[n]], {n, 1, 35}] (* A288176 conjectured *)"
			],
			"xref": [
				"Cf. A288174, A288175, A288176."
			],
			"keyword": "nonn,easy",
			"offset": "1",
			"author": "_Clark Kimberling_, Jun 07 2017",
			"references": 4,
			"revision": 12,
			"time": "2020-04-06T20:20:44-04:00",
			"created": "2017-06-07T19:15:30-04:00"
		}
	]
}