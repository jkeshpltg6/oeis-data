{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A077607",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 77607,
			"data": "1,-2,-2,-8,-44,-296,-2312,-20384,-199376,-2138336,-24936416,-314142848,-4252773824,-61594847360,-950757812864,-15586971531776,-270569513970944,-4959071121374720,-95721139472072192,-1941212789888952320,-41271304403571227648",
			"name": "Convolutory inverse of the factorial sequence.",
			"comment": [
				"|a(n)| is the number of permutations on [n] for which no proper initial interval of [n] is mapped to an interval. - _David Callan_, Nov 11 2003"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A077607/b077607.txt\"\u003eTable of n, a(n) for n = 1..449\u003c/a\u003e",
				"Jean-Christophe Aval, Jean-Christophe Novelli, Jean-Yves Thibon, \u003ca href=\"https://dmtcs.episciences.org/2892\"\u003eThe # product in combinatorial Hopf algebras\u003c/a\u003e, dmtcs:2892 - Discrete Mathematics \u0026 Theoretical Computer Science, January 1, 2011, DMTCS Proceedings vol. AO, 23rd International Conference on Formal Power Series and Algebraic Combinatorics (FPSAC 2011).",
				"Richard J. Martin, and Michael J. Kearney, \u003ca href=\"http://dx.doi.org/10.1007/s00493-014-3183-3\"\u003eIntegral representation of certain combinatorial recurrences\u003c/a\u003e, Combinatorica: 35:3 (2015), 309-315.",
				"Ioannis Michos, Christina Savvidou, \u003ca href=\"https://arxiv.org/abs/1803.08818\"\u003eEnumeration of super-strong Wilf equivalence classes of permutations\u003c/a\u003e, arXiv:1803.08818 [math.CO], 2018.",
				"Vincent Pilaud, V. Pons, \u003ca href=\"http://arxiv.org/abs/1606.09643\"\u003ePermutrees\u003c/a\u003e, arXiv preprint arXiv:1606.09643 [math.CO], 2016 (Unsigned version)."
			],
			"formula": [
				"a(n) = -n!*a(1)-(n-1)!*a(2)-...-2!*a(n-1), with a(1)=1.",
				"G.f.: 1/Sum_{k\u003e=0} (k+1)!*x^k. - _Vladeta Jovovic_, May 04 2003",
				"From _Sergei N. Gladkovskii_, Aug 15 2012 - Nov 07 2013: (Start)",
				"Continued fractions:",
				"G.f.: U(0) - x where U(k) = 1-x*(k+1)/(1-x*(k+2)/U(k+1)).",
				"G.f.: A(x) = G(0) - x where G(k) = 1 + (k+1)*x - x*(k+2)/G(k+1).",
				"G.f.: G(0) where G(k) = 1 - x*(k+2)/(1 - x*(k+1)/G(k+1)).",
				"G.f.: (x-x^(2/3))/(Q(0)-1), where Q(k) = 1-(k+1)*x^(2/3)/(1-x^(1/3)/(x^(1/3) - 1/Q(k+1))).",
				"G.f.: 1 - x - x/Q(0), where Q(k)= 1 + k*x - x*(k+2)/Q(k+1).",
				"G.f.: 2/G(0) where G(k)= 1 + 1/(1 - x*(k+2)/(x*(k+2) + 1/G(k+1))).",
				"G.f.: 1/W(0) where W(k) = 1-x*(k+2)/(x*(k+2)-1/(1 - x*(k+1)/(x*(k+1) - 1/W(k+1)))).",
				"G.f.: x/(1- Q(0)) - x, where Q(k) = 1  - (k+1)*x/(1  - (k+1)*x/Q(k+1)).",
				"G.f.: 1-x-x*T(0), where T(k) = 1-x*(k+2)/(x*(k+2)-(1+k*x)*(1+x+k*x)/T(k+1)). (End)",
				"a(n) ~ -n! * (1 - 4/n - 8/n^3 - 76/n^4 - 752/n^5 - 8460/n^6 - 107520/n^7 - 1522124/n^8 - 23717424/n^9 - 402941324/n^10), for coefficients see A260491. - _Vaclav Kotesovec_, Jul 27 2015",
				"a(n) = -2*A111529(n-2), for n\u003e=2. - _Vaclav Kotesovec_, Jul 29 2015"
			],
			"example": [
				"a(4)= -8 = -24*1-6*(-2)-2*(-2). (a(1),a(2),...,a(n))(*)(1,2,3!,...,n!)=(1,0,0,...,0), where (*) denotes convolution."
			],
			"maple": [
				"a:= proc(n) option remember; `if`(n=1, 1,",
				"      -add((n-i+1)!*a(i), i=1..n-1))",
				"    end:",
				"seq(a(n), n=1..25);  # _Alois P. Heinz_, Dec 20 2017"
			],
			"mathematica": [
				"Clear[a]; a[1]=1; a[n_]:=a[n]=Sum[-(n-j+1)!*a[j],{j,1,n-1}]; Table[a[n],{n,1,20}] (* _Vaclav Kotesovec_, Jul 27 2015 *)",
				"terms=21; 1/Sum[(k+1)!*x^k, {k, 0, terms}]+O[x]^terms // CoefficientList[#, x]\u0026 (* _Jean-François Alcover_, Dec 20 2017, after _Vladeta Jovovic_ *)"
			],
			"program": [
				"(Sage)",
				"def A077607_list(len):",
				"    R, C = [1], [1]+[0]*(len-1)",
				"    for n in (1..len-1):",
				"        for k in range(n, 0, -1):",
				"            C[k] = C[k-1] * (k+1)",
				"        C[0] = -sum(C[k] for k in (1..n))",
				"        R.append(C[0])",
				"    return R",
				"print(A077607_list(21)) # _Peter Luschny_, Feb 28 2016"
			],
			"xref": [
				"Cf. A000142, A003319, A111529, A260491."
			],
			"keyword": "sign",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Nov 11 2002",
			"ext": [
				"More terms from _Vaclav Kotesovec_, Jul 29 2015"
			],
			"references": 9,
			"revision": 85,
			"time": "2020-02-29T18:11:08-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}