{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A244579",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 244579,
			"data": "1,3,5,7,9,11,13,17,19,21,23,25,27,29,31,33,37,39,41,43,47,49,51,53,55,57,59,61,65,67,69,71,73,79,81,83,85,87,89,93,95,97,101,103,107,109,111,113,115,119,121,123,125,127,129,131,133,137,139,141,145",
			"name": "Numbers n with the property that the number of parts in the symmetric representation of sigma(n) equals the number of divisors of n.",
			"comment": [
				"Numbers n such that A243982(n) = 0.",
				"First differs from A151991 at a(25).",
				"Let n = 2^m * q with m \u003e= 0 and q odd. Let c_n denote the count of regions in the symmetric representation of sigma(n), which is determined by the positions of 1's in the n-th row of A237048. The maximum of c_n occurs when odd and even positions of 1's alternate implying that all regions have width 1, denoted by w_n = 1. When m \u003e 0 then sigma_0(n) \u003e sigma_0(q) and c_n = sigma_0(n) is impossible. Therefore, exactly those odd n with w_n = 1 are in this sequence. Furthermore, since the 1's in A237048 represent the odd divisors of n, their odd-even alternation expresses the property 2*f \u003c g for any two adjacent divisors f \u003c g of odd number n; in other words, this sequence is also the complement of A090196 relative to the odd numbers (see also A244969). This last property permits computations of elements in this sequence faster than with function a244579, which is based on Dyck paths. - _Hartmut F. W. Hoft_, Oct 11 2015",
				"From _Hartmut F. W. Hoft_, Dec 06 2016: (Start)",
				"Also, integers n such that for any pair a \u003c b of divisors of n the inequality 2*a \u003c b holds (hence n is odd).",
				"Let 1 = d_1 \u003c ... \u003c d_k = n be all (odd) divisors of n. The property 2*d_i \u003c d_(i+1), for 1 \u003c= i \u003c k, is equivalent for the 1's in the n-th row of A249223 to be in positions 1 = d_1 \u003c 2 \u003c d_2 \u003c 2*d_2 \u003c ... \u003c d_i \u003c2*d_i \u003c d_(i+1) \u003c ... where 2*d_i represents the odd divisor e_i with d_i * e_i = n. In other words, the odd divisors are the number of parts in the symmetric representation of sigma(n). The rightmost 1 in the n-th row occurs in an odd (even) position when k is odd (even).",
				"As a consequence this sequence is also the complement of A090196 in the set of odd numbers. (End)",
				"Since A244969 also is a complement of this sequence in the set of odd numbers this shows that A244969 = A090196. - _Hartmut F. W. Hoft_, Dec 10 2016"
			],
			"link": [
				"Hartmut F. W. Hoft, \u003ca href=\"/A244579/a244579.pdf\"\u003eIllustration of the symmetric representations of sigma for sequence data\u003c/a\u003e"
			],
			"formula": [
				"A237271(a(k)) = A000005(a(k))."
			],
			"example": [
				"9 is in the sequence because the parts of the symmetric representation of sigma(9) are [5, 3, 5] and the divisors of 9 are [1, 3, 9] and in both cases there is the same number of elements: A237271(9) = A000005(9) = 3.",
				"See the link for a diagram of the symmetric representations of sigma for sequence data listed above. The symmetric representations of sigma(a(35)) = sigma(81) = sigma(3^4) consists of 5 regions whose areas are [41, 15, 9, 15, 41] and computed as 41 = (3^4+3^0)/2, 15 = (3^3+3^1)/2, and 9 = 3^2 for the central area. Observe also that the 81st row in triangle A237048 is [ 1 1 1 0 0 1 0 0 1 0 0 0 ] with the 1's in positions 1, 2, 3, 6, and 9. This is the largest count for the symmetric regions of sigma shown in the diagram. - _Hartmut F. W. Hoft_, Oct 11 2015"
			],
			"mathematica": [
				"(* Function a237270[] is defined in A237270 *)",
				"a244579[m_, n_] := Select[Range[m,n], Length[a237270[#]] == Length[Divisors[#]]\u0026]",
				"a244579[1, 104]] (* data *)",
				"(* _Hartmut F. W. Hoft_, Sep 19 2014 *)",
				"(* alternative function using the divisor property *)",
				"divisorPairsQ[n_] := Module[{d=Divisors[n]}, Select[2*Most[d] - Rest[d], # \u003e= 0\u0026] == {}]",
				"a244579Alt[m_?OddQ, n_] := Select[Range[m, n, 2], divisorPairsQ]",
				"a244579Alt[1, 145] (* data *)",
				"(* _Hartmut F. W. Hoft_, Oct 11 2015 *)"
			],
			"xref": [
				"Cf. A000005, A001227, A005279, A071561, A071562, A000203, A196020, A236104, A237048, A237270, A237271, A237593, A238443, A238524, A239657, A239929, A239663, A240062, A241558, A241559, A243982, A245092.",
				"Cf. A090196. - _Hartmut F. W. Hoft_, Nov 29 2016"
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Omar E. Pol_, Jul 02 2014",
			"references": 3,
			"revision": 60,
			"time": "2018-06-03T03:39:17-04:00",
			"created": "2014-07-26T21:02:48-04:00"
		}
	]
}