{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A136124",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 136124,
			"data": "1,2,1,6,5,1,24,26,9,1,120,154,71,14,1,720,1044,580,155,20,1,5040,8028,5104,1665,295,27,1,40320,69264,48860,18424,4025,511,35,1,362880,663696,509004,214676,54649,8624,826,44,1,3628800,6999840,5753736,2655764",
			"name": "Triangle read by rows: T(n,k) = (-1)^(n+k)*Sum_{j=1..k} s(n,j), where s(n,j) are the signed Stirling numbers of the first kind (n \u003e= 2; 1 \u003c= k \u003c= n-1; s(n,j) = A008275(n,j)).",
			"comment": [
				"Sum of entries in row n = n!/2 = A001710(n). T(n,1) = (n-1)! = A000142(n-1). Columns 2,3,4 and 5 yield A001705,A001706,A001707 and A001708, respectively.",
				"See A143491 for the interpretation of these numbers as restricted Stirling numbers of the first kind. See A049444 for a signed version of this array. - _Peter Bala_, Aug 25 2008",
				"With offset n=0, k=0: triangle T(n,k), read by rows, given by [2,1,3,2,4,3,5,4,6,5,...] DELTA [1,0,1,0,1,0,1,0,1,0,1,0,...] where DELTA is the operator defined in A084938. - _Philippe Deléham_, Sep 29 2011",
				"With offset n=0, k=0: T(n,k) is the number of ways to seat n people at any number of round tables and serve exactly k of the tables water, some number of the remaining tables red wine, and the rest of the tables white wine. - _Geoffrey Critzer_, Mar 13 2015"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A136124/b136124.txt\"\u003eTable of n, a(n) for the first 50 rows, flattened\u003c/a\u003e",
				"Olivier Bodini, Antoine Genitrini, Mehdi Naima, \u003ca href=\"https://arxiv.org/abs/1808.08376\"\u003eRanked Schröder Trees\u003c/a\u003e, arXiv:1808.08376 [cs.DS], 2018."
			],
			"formula": [
				"E.g.f.: Sum[(1/n!)T(n,k)x^n*t^k, k=1..n-1, n\u003e=2]=1/[(1+t)(1-x)^t]-(1+tx)/(1+t). Generating polynomial of row n = t*Product(j+t, j=2..n-1). T(n,k) is the sum of all products of n-k-1 different integers taken from {2,3,...,n-1}. For example, T(6,3) = 2*3 + 2*4 + 2*5 + 3*4 + 3*5 + 4*5 = 71."
			],
			"example": [
				"T(6,3)=71 because (-1)^9*[s(6,1)+s(6,2)+s(6,3)]=-(-120+274-225)=71.",
				"Triangle starts:",
				"    1;",
				"    2,   1;",
				"    6,   5,   1;",
				"   24,  26,   9,   1;",
				"  120, 154,  71,  14,   1;"
			],
			"maple": [
				"A136124_row := proc(n) local k,j; `if`(n=0,1,seq((-1)^(n+1-k)*add(stirling1(n+1,j), j=1..k),k=1..n)) end: seq(print(A136124_row(r)),r=1..6); # _Peter Luschny_, Sep 29 2011",
				"with(combinat): T:=proc(n, k) options operator, arrow: (-1)^(n+k)*(sum(stirling1(n,j),j=1..k)) end proc: for n from 2 to 11 do seq(T(n,k),k=1..n-1) end do; # yields sequence in triangular form"
			],
			"mathematica": [
				"nn = 10; Map[Select[#, # \u003e 0 \u0026] \u0026,Range[0,nn]!CoefficientList[Series[Exp[(2 + y) Log[1/(1 - x)]], {x, 0, nn}], {x,y}]] // Flatten (* _Geoffrey Critzer_, Mar 13 2015 *)"
			],
			"xref": [
				"Cf. A000142, A008275, A001705, A001706, A001707, A001708, A001710.",
				"Cf. A049444, A143491. [_Peter Bala_, Aug 25 2008]"
			],
			"keyword": "nonn,tabl",
			"offset": "2,2",
			"author": "_Emeric Deutsch_, Dec 23 2007",
			"references": 4,
			"revision": 30,
			"time": "2018-11-28T15:26:03-05:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}