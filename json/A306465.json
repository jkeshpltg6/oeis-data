{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A306465",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 306465,
			"data": "1,2,3,10,4,11,5,100,6,101,7,110,8,111,9,1000,12,13,20,14,21,22,30,23,102,24,112,31,32,103,33,120,40,121,41,200,34,201,42,202,43,1001,15,1010,16,1011,17,1100,18,1101,25,1110,26,1111,27,10000,19,10001,28",
			"name": "Lexicographically earliest sequence of distinct positive terms such that the product of any two consecutive terms can be computed without carry by long multiplication in base 10.",
			"comment": [
				"This sequence is the variant of A266195 in base 10.",
				"This sequence is a permutation of the natural numbers, with inverse A306466. Proof:",
				"- we can always extend the sequence with a power of ten not yet in the sequence, hence the sequence is well defined and infinite,",
				"- for any k \u003e 0, 10^(k-1) is the first k-digit number appearing in the sequence,",
				"- all powers of ten appear in the sequence, in increasing order,",
				"- a power of ten is always followed by the least number unused so far,",
				"hence every number eventually appears. QED"
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A306465/b306465.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A306465/a306465.gp.txt\"\u003ePARI program for A306465\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A306465/a306465.png\"\u003eColored logarithmic scatterplot of the sequence for n = 1..200000\u003c/a\u003e (where the color is function of A054055(a(n)))",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"A007953(a(n) * a(n+1)) = A007953(a(n)) * A007953(a(n+1)).",
				"A054055(a(n)) * A054055(a(n+1)) \u003c= 9."
			],
			"example": [
				"The first terms, alongside their digital sum and the digital sum of the product with the next term, are:",
				"  n   a(n)  ds(a(n))  ds(a(n)*a(n+1))",
				"  --  ----  --------  ---------------",
				"   1     1         1                2",
				"   2     2         2                6",
				"   3     3         3                3",
				"   4    10         1                4",
				"   5     4         4                8",
				"   6    11         2               10",
				"   7     5         5                5",
				"   8   100         1                6",
				"   9     6         6               12",
				"  10   101         2               14",
				"  11     7         7               14",
				"  12   110         2               16",
				"  13     8         8               24",
				"  14   111         3               27",
				"  15     9         9                9",
				"  16  1000         1                3",
				"  17    12         3               12"
			],
			"program": [
				"(PARI) See Links section."
			],
			"xref": [
				"Cf. A007953, A054055, A266195, A306466 (inverse)."
			],
			"keyword": "nonn,base",
			"offset": "1,2",
			"author": "_Rémy Sigrist_, Feb 17 2019",
			"references": 3,
			"revision": 17,
			"time": "2019-02-19T10:34:04-05:00",
			"created": "2019-02-18T14:42:50-05:00"
		}
	]
}