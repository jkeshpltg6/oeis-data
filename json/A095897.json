{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A095897",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 95897,
			"data": "1,8,48,320,1888,11648,69504,419840,2515456,15116288,90667008,544194560,3264913408,19591036928,117544157184,705277460480,4231648116736,25389989101568,152339800915968,914039609753600,5484236586876928",
			"name": "Sequence generated from the Z/4Z addition table considered as a matrix.",
			"comment": [
				"Garrity, p. 218, states \"Since the integers form an Abelian group, every subgroup including nZ, is normal and thus Z/nZ will form a group. It is common to represent each coset in Z/nZ by an integer between 0 and (n-1): Z/nZ = {0,1,2,...,(n-1}.\" Performing the analogous operation with the Z/3Z addition table (as a matrix), generates A007070: 1, 4, 14, 48, 164, 560... The recursion multipliers 4, 20, -32, -96 are present with changed signs in the characteristic polynomial of M: x^4 - 4x^3 - 20x^2 + 32x + 96."
			],
			"reference": [
				"Thomas A. Garrity, \"All the Mathematics You Missed But Need to Know for Graduate School\", Cambridge University Press, 2002."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A095897/b095897.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,20,-32,-96)."
			],
			"formula": [
				"Let M = the Z/4Z = {0, 1, 2, 3} addition table considered as a matrix = [0 1 2 3 / 1 2 3 0 / 2 3 0 1 / 3 0 1 2]. Then a(n) = 2nd term from left in M^n * [1 0 0 0].",
				"The recursion operation (n\u003e4) is a(n+4) = 4*a(n+3) + 20*a(n+2) - 32*a(n+1) - 96*a(n).",
				"From _Bruno Berselli_, Aug 04 2011:  (Start)",
				"  G.f.:  x*(1+4*x-4*x^2)/((1+2*x)*(1-6*x)*(1-8*x^2)).",
				"  a(n) = 2^(n-2)*(3^n-1+((-1)^n-1)*(sqrt(2)^(n-1)-1)).",
				"  a(2k+1) = 2^(2k-1)*(3*9^k-2*2^k+1), a(2k) = 4^(k-1)*(9^k-1). (End)"
			],
			"example": [
				"a(3) = 48 since M^3 * [1 0 0 0] = [44 48 60 64] (a(3) = 2nd term from left.",
				"a(8) = 419840 = 4*69504 + 20*11648 - 32*1888 - 96*320."
			],
			"mathematica": [
				"a[n_] := (MatrixPower[{{0, 1, 2, 3}, {1, 2, 3, 0}, {2, 3, 0, 1}, {3, 0, 1, 2}}, n].{{1}, {0}, {0}, {0}})[[2, 1]]; Table[ a[n], {n, 22}] (* _Robert G. Wilson v_, Jun 16 2004 *)",
				"LinearRecurrence[{4,20,-32,-96},{1,8,48,320},30] (* _Harvey P. Dale_, Jun 11 2011 *)"
			],
			"program": [
				"(MAGMA) [Floor(2^(n-2)*(3^n-1+((-1)^n-1)*(Sqrt(2)^(n-1)-1))): n in [1..30]]; // _Vincenzo Librandi_, Aug 05 2011"
			],
			"xref": [
				"Cf. A007070."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Gary W. Adamson_, Jun 11 2004",
			"ext": [
				"Edited, corrected and extended by _Robert G. Wilson v_, Jun 16 2004"
			],
			"references": 6,
			"revision": 21,
			"time": "2015-06-13T00:51:22-04:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}