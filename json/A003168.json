{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003168",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3168,
			"id": "M3574",
			"data": "1,1,4,21,126,818,5594,39693,289510,2157150,16348960,125642146,976789620,7668465964,60708178054,484093913917,3884724864390,31348290348086,254225828706248,2070856216759478,16936016649259364",
			"name": "Number of blobs with 2n+1 edges.",
			"comment": [
				"a(n) is the number of ways to dissect a convex (2n+2)-gon with non-crossing diagonals so that no (2m+1)-gons (m\u003e0) appear. - _Len Smiley_",
				"a(n) is the number of plane trees with 2n+1 leaves and all non-leaves having an odd number \u003e 1 of children. - _Jordan Tirrell_, Jun 09 2017"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A003168/b003168.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e (terms 0..100 from T. D. Noe)",
				"Thomas H. Bertschinger, Joseph Slote, Olivia Claire Spencer, and Samuel Vinitsky, \u003ca href=\"https://joeslote.com/documents/origami_undergrad_thesis.pdf\"\u003eThe Mathematics of Origami\u003c/a\u003e, Undergrad Thesis, Carleton College (2016).",
				"D. Birmajer, J. B. Gil, and M. D. Weiner, \u003ca href=\"http://arxiv.org/abs/1503.05242\"\u003eColored partitions of a convex polygon by noncrossing diagonals\u003c/a\u003e, arXiv preprint arXiv:1503.05242 [math.CO], 2015.",
				"L. Carlitz, \u003ca href=\"http://www.fq.math.ca/Scanned/11-2/carlitz.pdf\"\u003eEnumeration of two-line arrays\u003c/a\u003e, Fib. Quart., Vol. 11 Number 2 (1973), 113-130.",
				"Frédéric Chapoton and Philippe Nadeau, \u003ca href=\"http://www.mat.univie.ac.at/~slc/wpapers/FPSAC2017/37%20Chapoton%20Nadeau.pdf\"\u003eCombinatorics of the categories of noncrossing partitions\u003c/a\u003e, Séminaire Lotharingien de Combinatoire 78B (2017), Article #37.",
				"Michael Drmota, Anna de Mier, and Marc Noy, \u003ca href=\"http://www.dmg.tuwien.ac.at/drmota/noncrossingFinal4.pdf\"\u003eExtremal statistics on non-crossing configurations\u003c/a\u003e, Discrete Math. 327 (2014), 103--117. MR3192420. See p. 116, B_b(z). - N. J. A. Sloane, May 18 2014",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=415\"\u003eEncyclopedia of Combinatorial Structures 415\u003c/a\u003e",
				"Elżbieta Liszewska and Wojciech Młotkowski, \u003ca href=\"https://arxiv.org/abs/1907.10725\"\u003eSome relatives of the Catalan sequence\u003c/a\u003e, arXiv:1907.10725 [math.CO], 2019.",
				"Jean-Christophe Novelli and Jean-Yves Thibon, \u003ca href=\"http://arxiv.org/abs/1403.5962\"\u003eHopf Algebras of m-permutations,(m+1)-ary trees, and m-parking functions\u003c/a\u003e, arXiv preprint arXiv:1403.5962 [math.CO], 2014.",
				"Jean-Christophe Novelli and Jean-Yves Thibon, \u003ca href=\"https://arxiv.org/abs/2106.08257\"\u003eNoncommutative Symmetric Functions and Lagrange Inversion II: Noncrossing partititions and the Farahat-Higman algebra\u003c/a\u003e, arXiv:2106.08257 [math.CO], 2021.",
				"Ronald C. Read, \u003ca href=\"https://doi.org/10.1007/BF02188172\"\u003eOn the enumeration of a class of plane multigraphs\u003c/a\u003e, Aequat. Math. 31 (1986) No. 1, 47-63.",
				"L. Smiley, \u003ca href=\"http://www.math.uaa.alaska.edu/~smiley/vsd2.html\"\u003eEven-gon reference\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Sum_{k=1..n} binomial(n, k)*binomial(2*n+k, k-1)/n.",
				"G.f.: A(x) = Sum_{n\u003e=0} a(n)*x^(2*n+1) satisfies (A-2*A^3)/(1-A^2)=x. - _Len Smiley_.",
				"D-finite with recurrence 4*n*(2*n + 1)*(17*n - 22)*a(n) = (1207*n^3 - 2769*n^2 + 1850*n - 360)*a(n - 1) - 2*(17*n - 5)*(n - 2)*(2*n - 3)*a(n - 2). - _Vladeta Jovovic_, Jul 16 2004",
				"G.f.: A(x) = 1/(1-G003169(x)) where G003169(x) is the g.f. of A003169. - _Paul D. Hanna_, Nov 17 2004",
				"a(n) = JacobiP(n-1,1,n+1,3)/n for n \u003e 0. - _Mark van Hoeij_, Jun 02 2010",
				"a(n) = 1/(2*n+1)*sum((-1)^j*2^(n-j)*binomial(2*n+1,j)*binomial(3*n-j,2*n),j=0..n). - _Vladimir Kruchinin_, Dec 24 2010",
				"a(n) = upper left term in M^n, M = the production matrix:",
				"1, 1",
				"3, 3, 1",
				"5, 5, 3, 1",
				"7, 7, 5, 3, 1",
				"9, 9, 7, 5, 3, 1",
				"... . - _Gary W. Adamson_, Jul 08 2011",
				"a(n) ~ sqrt(14+66/sqrt(17)) * (71+17*sqrt(17))^n / (sqrt(Pi) * n^(3/2) * 2^(4*n+4)). - _Vaclav Kotesovec_, Jul 01 2015",
				"From _Peter Bala_, Oct 05 2015: (Start)",
				"a(n) = 1/n * Sum_{i = 0..n} 2^(n-i-1)*binomial(2*n,i)* binomial(n,i+1).",
				"O.g.f. = 1 + series reversion( x/((1 + 2*x)*(1 + x)^2) ).",
				"Logarithmically differentiating the modified g.f. 1 + 4*x + 21*x^2 + 126*x^3 + 818*x^4 + ... gives the o.g.f. for A114496, apart from the initial term. (End)",
				"G.f.: A(x) satisfies A = 1 + x*A^3/(1-x*A^2). - _Jordan Tirrell_, Jun 09 2017",
				"a(n) = A100327(n)/2 for n\u003e=1. - _Peter Luschny_, Jun 10 2017"
			],
			"example": [
				"a(2)=4 because we may place exactly one diagonal in 3 ways (forming 2 quadrilaterals), or not place any (leaving 1 hexagon)."
			],
			"maple": [
				"Order := 40; solve(series((A-2*A^3)/(1-A^2),A)=x,A);",
				"A003168 := n -\u003e `if`(n=0,1,A100327(n)/2): seq(A003168(n),n=0..20); # _Peter Luschny_, Jun 10 2017"
			],
			"mathematica": [
				"a[0] = 1; a[n_] = (2^(-n-1)*(3n)!* Hypergeometric2F1[-1-2n, -2n, -3n, -1])/((2n+1)* n!*(2n)!); Table[a[n], {n, 0, 20}] (* _Jean-François Alcover_, Jul 25 2011, after _Vladimir Kruchinin_ *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c0,0,polcoeff(serreverse((x-2*x^3)/(1-x^2)+O(x^(2*n+2))),2*n+1))",
				"(PARI) {a(n)=local(A=1+x+x*O(x^n));for(i=1,n,A=(1+x*A)/(1-x*A)^2); sum(k=0,n,polcoeff(A^(n-k),k))} \\\\ _Paul D. Hanna_, Nov 17 2004",
				"(Haskell)",
				"import Data.List (transpose)",
				"a003168 0 = 1",
				"a003168 n = sum (zipWith (*)",
				"   (tail $ a007318_tabl !! n)",
				"   ((transpose $ take (3*n+1) a007318_tabl) !! (2*n+1)))",
				"   `div` fromIntegral n",
				"-- _Reinhard Zumkeller_, Oct 27 2013"
			],
			"xref": [
				"Cf. A049124 (no 2m-gons).",
				"Cf. A003169, A100327, A114496, A007318.",
				"Row sums of A102537, A243662."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"references": 20,
			"revision": 102,
			"time": "2021-10-05T22:12:53-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}