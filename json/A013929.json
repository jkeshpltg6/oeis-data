{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A013929",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 13929,
			"data": "4,8,9,12,16,18,20,24,25,27,28,32,36,40,44,45,48,49,50,52,54,56,60,63,64,68,72,75,76,80,81,84,88,90,92,96,98,99,100,104,108,112,116,117,120,121,124,125,126,128,132,135,136,140,144,147,148,150,152,153,156,160",
			"name": "Numbers that are not squarefree. Numbers that are divisible by a square greater than 1. The complement of A005117.",
			"comment": [
				"Sometimes misnamed squareful numbers, but officially those are given by A001694.",
				"This is different from the sequence of numbers n such that A007913(n) \u003c phi(n). The two sequences differ at the values: 420, 660, 780, 840, 1320, 1560, 4620, 5460, 7140, ..., which is essentially A070237. - _Ant King_, Dec 16 2005",
				"Numbers n such that Sum_{d|n} d/phi(d)*mu(n/d) = 0. - _Benoit Cloitre_, Apr 28 2002",
				"Also, n with at least one x \u003c n such that A007913(x) = A007913(n). - _Benoit Cloitre_, Apr 28 2002",
				"Numbers for which there exists a partition into two parts p and q such that p + q = n and pq is a multiple of n. - _Amarnath Murthy_, May 30 2003",
				"Numbers n such there is a solution 0 \u003c x \u003c n to x^2 == 0 mod(n). - _Franz Vrabec_, Aug 13 2005",
				"Numbers n such that moebius(n) = 0.",
				"a(n) = k such that phi(k)/k = phi(m)/m for some m \u003c k. - _Artur Jasinski_, Nov 05 2008",
				"Appears to be numbers such that when a column with index equal to a(n) in A051731 is deleted, there is no impact on the result in the first column of A054525. - _Mats Granvik_, Feb 06 2009",
				"Number of prime divisors of (n+1) is less than the number of nonprime divisors of (n+1). - _Juri-Stepan Gerasimov_, Nov 10 2009",
				"Orders for which at least one non-cyclic finite abelian group exists: A000688(a(n)) \u003e 1. This follows from the fact that not all exponents in the prime number factorization of a(n) are 1 (moebius(a(n)) = 0). The number of such groups of order a(n) is A192005(n) = A000688(a(n)) - 1. - _Wolfdieter Lang_, Jul 29 2011",
				"Subsequence of A193166; A192280(a(n)) = 0. - _Reinhard Zumkeller_, Aug 26 2011",
				"It appears that a(n) = n such that Product_{k=1..n} (prime(k) mod n) \u003c\u003e 0. See Maple code. - _Gary Detlefs_, Dec 07 2011",
				"A008477(a(n)) \u003e 1. - _Reinhard Zumkeller_, Feb 17 2012",
				"A057918(a(n)) \u003e 0. - _Reinhard Zumkeller_, Mar 27 2012",
				"Numbers for which gcd(n, n') \u003e 1, where n' is the arithmetic derivative of n. - _Paolo P. Lava_, Apr 24 2012",
				"Sum_{n\u003e=1} 1/a(n)^s = (zeta(s)*(zeta(2*s)-1))/zeta(2*s). - _Enrique Pérez Herrero_, Jul 07 2012",
				"A056170(a(n)) \u003e 0. - _Reinhard Zumkeller_, Dec 29 2012",
				"a(n) = n such that A001221(n) != A001222(n). - _Felix Fröhlich_, Aug 13 2014",
				"Numbers whose sum of divisors is greater than the sum of unitary divisors: A000203(a(n)) \u003e A034448(a(n)). - _Paolo P. Lava_, Oct 08 2014",
				"Numbers n such that A001222(n) \u003e A001221(n), since in this case at least one prime factor of n occurs more than once, which implies that n is divisible by at least one perfect square \u003e 1. - _Carlos Eduardo Olivieri_, Aug 02 2015",
				"Lexicographically least sequence such that each entry has a positive even number of proper divisors not occurring in the sequence, cf. the sieve characterization of A005117. - _Glen Whitney_, Aug 30 2015",
				"There are arbitrarily long runs of consecutive terms. Record runs start at 4, 8, 48, 242, ... (A045882). - _Ivan Neretin_, Nov 07 2015",
				"A number k is a term if 0 \u003c min(A000010(k) + A023900(k), A000010(k) - A023900(k)). - _Torlach Rush_, Feb 22 2018",
				"Every squareful number \u003e 1 is nonsquarefree, but the converse is false and the nonsquarefree numbers that are not squareful (see first comment) are in A332785. - _Bernard Schott_, Apr 11 2021",
				"Integers m where at least one k \u003c m exists such that m divides k^m. - _Richard R. Forberg_, Jul 31 2021"
			],
			"link": [
				"David A. Corneth, \u003ca href=\"/A013929/b013929.txt\"\u003eTable of n, a(n) for n = 1..100000\u003c/a\u003e (first 1000 terms form T.D. Noe)",
				"H. Gent, \u003ca href=\"/A005117/a005117.pdf\"\u003eLetter to N. J. A. Sloane\u003c/a\u003e, Nov 27 1975.",
				"Louis Marmet, \u003ca href=\"http://arxiv.org/abs/1210.3829\"\u003eFirst occurrences of square-free gaps and an algorithm for their computation\u003c/a\u003e, arXiv:1210.3829 [math.NT], 2012.",
				"Srinivasa Ramanujan, \u003ca href=\"http://www.imsc.res.in/~rao/ramanujan/CamUnivCpapers/Cpaper4/page1.htm\"\u003eIrregular numbers\u003c/a\u003e, J. Indian Math. Soc. 5 (1913) 105-106.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/SmarandacheNear-to-PrimorialFunction.html\"\u003eSmarandache Near-to-Primorial Function\u003c/a\u003e, \u003ca href=\"http://mathworld.wolfram.com/Squarefree.html\"\u003eSquarefree\u003c/a\u003e, \u003ca href=\"http://mathworld.wolfram.com/Squareful.html\"\u003eSquareful\u003c/a\u003e, \u003ca href=\"http://mathworld.wolfram.com/MoebiusFunction.html\"\u003eMoebius Function\u003c/a\u003e."
			],
			"formula": [
				"A008966(a(n)) = 0. - _Reinhard Zumkeller_, Apr 22 2012",
				"a(n) ~ n/k, where k = 1 - 1/zeta(2) = 1 - 6/Pi^2 = A229099. - _Charles R Greathouse IV_, Sep 13 2013",
				"A001222(a(n)) \u003e A001221(a(n)). - _Carlos Eduardo Olivieri_, Aug 02 2015",
				"phi(a(n)) \u003e A003958(a(n)). - _Juri-Stepan Gerasimov_, Apr 09 2019"
			],
			"example": [
				"For the terms up to 20, we compute the squares of primes up to floor(sqrt(20)) = 4. Those squares are 4 and 9. For every such square s, put the terms s*k^2 for k = 1 to floor(20 / s). This gives after sorting and removing duplicates the list 4, 8, 9, 12, 16, 18, 20. - _David A. Corneth_, Oct 25 2017"
			],
			"maple": [
				"a := n -\u003e `if`(numtheory[mobius](n)=0,n,NULL); seq(a(i),i=1..160); # _Peter Luschny_, May 04 2009",
				"t:= n-\u003e product(ithprime(k),k=1..n): for n from 1 to 160 do (if t(n) mod n \u003c\u003e0) then print(n) fi od; # _Gary Detlefs_, Dec 07 2011"
			],
			"mathematica": [
				"Union[ Flatten[ Table[ n i^2, {i, 2, 20}, {n, 1, 400/i^2} ] ] ]",
				"Select[ Range[2, 160], (Union[Last /@ FactorInteger[ # ]][[ -1]] \u003e 1) == True \u0026] (* _Robert G. Wilson v_, Oct 11 2005 *)",
				"Cases[Range[160], n_ /; !SquareFreeQ[n]] (* _Jean-François Alcover_, Mar 21 2011 *)",
				"Select[Range@160, ! SquareFreeQ[#] \u0026] (* _Robert G. Wilson v_, Jul 21 2012 *)",
				"Select[Range@160, PrimeOmega[#] \u003e PrimeNu[#] \u0026] (* _Carlos Eduardo Olivieri_, Aug 02 2015 *)",
				"Select[Range[200], MoebiusMu[#] == 0 \u0026] (* _Alonso del Arte_, Nov 07 2015 *)"
			],
			"program": [
				"(PARI) {a(n)= local(m,c); if(n\u003c=1,4*(n==1), c=1; m=4; while( c\u003cn, m++; if(!issquarefree(m), c++)); m)} /* _Michael Somos_, Apr 29 2005 */",
				"(PARI) for(n=1, 1e3, if(omega(n)!=bigomega(n), print1(n, \", \"))) \\\\ _Felix Fröhlich_, Aug 13 2014",
				"(PARI) upto(n) = {my(res = List()); forprime(p = 2, sqrtint(n), for(k = 1, n \\ p^2, listput(res, k * p^2))); listsort(res, 1); res} \\\\ _David A. Corneth_, Oct 25 2017",
				"(MAGMA) [ n : n in [1..1000] | not IsSquarefree(n) ];",
				"(Haskell)",
				"a013929 n = a013929_list !! (n-1)",
				"a013929_list = filter ((== 0) . a008966) [1..]",
				"-- _Reinhard Zumkeller_, Apr 22 2012",
				"(Python)",
				"from sympy.ntheory.factor_ import core",
				"def ok(n): return core(n, 2) != n",
				"print(list(filter(ok, range(1, 161)))) # _Michael S. Branicky_, Apr 08 2021"
			],
			"xref": [
				"Complement of A005117.",
				"Cf. A001694, A008683, A038109, A332785.",
				"Cf. A130897 (subsequence).",
				"Cf. A190641 (subsequence).",
				"Cf. A332785 (subsequence).",
				"Partitions into: A114374, A256012."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_Henri Lifchitz_",
			"ext": [
				"More terms from _Erich Friedman_",
				"More terms from _Franz Vrabec_, Aug 13 2005"
			],
			"references": 335,
			"revision": 177,
			"time": "2021-08-20T05:50:29-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}