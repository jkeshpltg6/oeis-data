{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A034694",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 34694,
			"data": "2,3,7,5,11,7,29,17,19,11,23,13,53,29,31,17,103,19,191,41,43,23,47,73,101,53,109,29,59,31,311,97,67,103,71,37,149,191,79,41,83,43,173,89,181,47,283,97,197,101,103,53,107,109,331,113,229,59,709,61,367,311",
			"name": "Smallest prime == 1 (mod n).",
			"comment": [
				"Thangadurai and Vatwani prove that a(n) \u003c= 2^(phi(n)+1)-1. - _T. D. Noe_, Oct 12 2011",
				"Conjecture: a(n) \u003c n^2 for n \u003e 1. - _Thomas Ordowski_, Dec 19 2016",
				"Eric Bach and Jonathan Sorenson show that, assuming GRH, a(n) \u003c= (1 + o(1))*(phi(n)*log(n))^2 for n \u003e 1. See the abstract of their paper in the Links section. - _Jianing Song_, Nov 10 2019",
				"a(n) is the smallest prime p such that the multiplicative group modulo p has a subgroup of order n. - _Joerg Arndt_, Oct 18 2020"
			],
			"reference": [
				"Steven R. Finch, Mathematical Constants, Cambridge, 2003, section 2.12, pp. 127-130.",
				"P. Ribenboim, The Book of Prime Number Records. Chapter 4,IV.B.: The Smallest Prime In Arithmetic Progressions, 1989, pp. 217-223."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A034694/b034694.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Eric Bach and Jonathan Sorenson, \u003ca href=\"https://doi.org/10.1090/S0025-5718-96-00763-6\"\u003eExplicit bounds for primes in residue classes\u003c/a\u003e, Mathematics of Computation, 65(216) (1996), 1717-1735.",
				"Steven R. Finch, \u003ca href=\"http://web.archive.org/web/20010207193039/http://www.mathsoft.com/asolve/constant/linnik/linnik.html\"\u003eLinnik's Constant\u003c/a\u003e",
				"S. Graham, \u003ca href=\"http://matwbn.icm.edu.pl/ksiazki/aa/aa39/aa3926.pdf\"\u003eOn Linnik's Constant\u003c/a\u003e, Acta Arithm. 39, 1981, pp. 163-179.",
				"I. Niven and B. Powell, \u003ca href=\"http://www.jstor.org/stable/2318341\"\u003ePrimes in Certain Arithmetic Progressions\u003c/a\u003e, Amer. Math. Monthly 83(6) (1976), 467-469.",
				"R. Thangadurai and A. Vatwani, \u003ca href=\"http://www.jstor.org/stable/10.4169/amer.math.monthly.118.08.737\"\u003eThe least prime congruent to one modulo n\u003c/a\u003e, Amer. Math. Monthly 118(8) (2011), 737-742."
			],
			"formula": [
				"a(n) = min{m: m = k*n + 1 with k \u003e 0 and A010051(m) = 1}. - _Reinhard Zumkeller_, Dec 17 2013",
				"a(n) = n * A034693(n) + 1. - _Joerg Arndt_, Oct 18 2020"
			],
			"example": [
				"If n = 7, the smallest prime in the sequence 8, 15, 22, 29, ... is 29, so a(7) = 29."
			],
			"mathematica": [
				"a[n_] := Block[{k = 1}, If[n == 1, 2, While[Mod[Prime@k, n] != 1, k++ ]; Prime@k]]; Array[a, 64] (* _Robert G. Wilson v_, Jul 08 2006 *)",
				"With[{prs=Prime[Range[200]]},Flatten[Table[Select[prs,Mod[#-1,n]==0\u0026,1],{n,70}]]] (* _Harvey P. Dale_, Sep 22 2021 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c0,0,s=1; while((prime(s)-1)%n\u003e0,s++); prime(s))",
				"(Haskell)",
				"a034694 n = until ((== 1) . a010051) (+ n) (n + 1)",
				"-- _Reinhard Zumkeller_, Dec 17 2013"
			],
			"xref": [
				"Cf. A034693, A034780, A034782, A034783, A034784, A034785, A034846, A034847, A034848, A034849, A038700, A085420.",
				"Records: A120856, A120857."
			],
			"keyword": "nonn,nice,easy",
			"offset": "1,1",
			"author": "_Labos Elemer_, _David W. Wilson_, Spring 1998",
			"references": 59,
			"revision": 68,
			"time": "2021-09-22T17:01:35-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}