{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A124289",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 124289,
			"data": "78,79,218,219,234,235,299,300,370,371,500,501",
			"name": "Unstable twins = pairs of consecutive numbers in A124288 (indices of unstable zeros of the Riemann zeta function).",
			"comment": [
				"Assuming the Riemann Hypothesis, the nonreal zeros of zeta(s,1) = zeta(s) lie on the critical line Re(s) = 1/2 and the nonreal zeros of zeta(s,1/2) = (2^s - 1)*zeta(s) lie on the critical line and on the imaginary axis Re(s) = 0."
			],
			"reference": [
				"M. Trott, Zeros of the Generalized Riemann Zeta Function zeta(s,a) as a Function of a, background image in graphics gallery, in S. Wolfram, The Mathematica Book, 4th ed. Cambridge, England: Cambridge University Press, 1999, p. 982.",
				"M. Trott, The Mathematica GuideBook for Symbolics, Springer-Verlag, 2006, see \"Zeros of the Hurwitz Zeta Function\"."
			],
			"link": [
				"A. Fujii, \u003ca href=\"https://doi.org/10.3792/pjaa.65.139\"\u003eZeta zeros, Hurwitz zeta functions and L(1,Chi)\u003c/a\u003e, Proc. Japan Acad. 65 (1989), 139-142.",
				"R. Garunkstis and J. Steuding, \u003ca href=\"https://doi.org/10.1090/S0025-5718-06-01882-5\"\u003eOn the distribution of zeros of the Hurwitz zeta-function\u003c/a\u003e, Math. Comput. 76 (2007), 323-337.",
				"R. Garunkstis and J. Steuding, \u003ca href=\"https://klevas.mif.vu.lt/~garunkstis/preprintai/classzerosMMA.pdf\"\u003eQuestions around the Nontrivial Zeros of the Riemann Zeta-Function. Computations and Classifications\u003c/a\u003e, Math. Model. Anal. 16 (2011), 72-81.",
				"J. Sondow and Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/HurwitzZetaFunction.html\"\u003eHurwitz Zeta Function\u003c/a\u003e",
				"M. Trott, \u003ca href=\"https://web.archive.org/web/20071210074226/http://documents.wolfram.com/v4/MainBook/G.2.22.html\"\u003eZeros of the Generalized Riemann Zeta Function zeta(s,a) as a Function of a\u003c/a\u003e"
			],
			"formula": [
				"Solve the differential equation ds(a)/da = -(dzeta(s,a)/da)/(dzeta(s,a)/ds) = s*zeta(s+1,a)/(dzeta(s,a)/ds) where s = s0(a) and zeta(s0(a),a) = 0. For initial conditions use the zeros of zeta(s,1)."
			],
			"example": [
				"The consecutive zeros rho78 and rho79 of zeta(s,1) on the line Re(s) = 1/2 connect by paths of zeros of zeta(s,a) to zeros of zeta(s,1/2) on the line Re(s) = 0, so rho78 and rho79 are \"unstable twins,\" and 78 and 79 are members."
			],
			"xref": [
				"Cf. A002410, A124288."
			],
			"keyword": "hard,nonn,more",
			"offset": "1,1",
			"author": "_Jonathan Sondow_, Oct 24 2006",
			"ext": [
				"Corrected by _Jonathan Sondow_, Nov 10 2006, using more accurate calculations by R. Garunkstis and J. Steuding."
			],
			"references": 2,
			"revision": 22,
			"time": "2019-03-18T10:10:18-04:00",
			"created": "2006-12-06T03:00:00-05:00"
		}
	]
}