{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A178743",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 178743,
			"data": "1,1,2,3,5,7,1,5,2,0,2,6,7,1,5,6,1,7,5,0,7,2,2,5,5,8,6,0,8,5,4,2,9,3,0,3,7,7,5,5,8,3,4,1,5,4,8,4,3,5,6,3,9,1,5,6,3,4,0,0,7,5,6,9,0,8,0,9,5,5,8,5,3,9,0,4,1,3,4,0,6,7,5,9,0,7,2,3,9,5,3,9,7,7,0,9,4,0,6,5,2,6,9,0,5",
			"name": "a(n) = A000041(n) mod 10.",
			"comment": [
				"From _Johannes W. Meijer_, Jul 08 2011: (Start)",
				"We observe for the last digit a(n) of the partition function p(n) = A000041(n) that the probabilities of p(d = 0) = 0.18 and p(d = 5) = 0.18 while for the other digits p(d = 1, 2, 3, 4, 6, 7, 8, 9) = 0.08, see the examples. Ramanujan, who had access to the first two hundred p(n) thanks to MacMahon, observed this anomaly and subsequently proved that p(5*n+4) mod 5 = 0, see the references and links.",
				"The first digit of the partition function p(n) follows Benford’s Law. This law states that the probability of having first digit d, 1 \u003c= d \u003c= 9, is p(d) = log_10(1+1/d), see the crossrefs. (End)"
			],
			"reference": [
				"Robert Kanigel, The man who knew infinity: A life of the genius Ramanujan (1991) pp. 246-254 and pp. 299-307."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A178743/b178743.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Scott Ahlgren and Ken Ono, \u003ca href=\"http://www.ams.org/notices/200109/fea-ahlgren.pdf\"\u003eAddition and Counting: The Arithmetic of Partitions\u003c/a\u003e, Notices of the AMS, 48 (2001) pp. 978-984.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PartitionFunctionPCongruences.html\"\u003ePartition Function P Congruences\u003c/a\u003e",
				"\u003ca href=\"/index/Fi#final\"\u003eIndex entries for sequences related to final digits of numbers\u003c/a\u003e",
				"\u003ca href=\"/index/Be#Benford\"\u003eIndex entries for sequences related to Benford's law\u003c/a\u003e"
			],
			"formula": [
				"a(n) = p(n) mod 10 with p(n) = A000041(n) the partition function."
			],
			"example": [
				"From _Johannes W. Meijer_, Jul 08 2011: (Start)",
				"d     p(N=200) p(N=2000) p(N=4000) p(N=6000)",
				"0     0.16000   0.17750   0.17600   0.18067",
				"1     0.08500   0.08150   0.08125   0.07833",
				"2     0.08000   0.08400   0.08075   0.08033",
				"3     0.10000   0.08350   0.08150   0.07917",
				"4     0.05500   0.08050   0.07950   0.08233",
				"5     0.18500   0.16900   0.17625   0.17817",
				"6     0.08500   0.07500   0.07725   0.07867",
				"7     0.09000   0.08600   0.08700   0.08283",
				"8     0.06500   0.07650   0.07450   0.07517",
				"9     0.09500   0.08650   0.08600   0.08433",
				"Total 1.00000   1.00000   1.00000   1.00000 (End)"
			],
			"mathematica": [
				"Table[ Mod[ PartitionsP@n, 10], {n, 0, 111}]"
			],
			"program": [
				"(PARI) a(n) = numbpart(n) % 10; \\\\ _Michel Marcus_, Apr 21 2019"
			],
			"xref": [
				"Cf. A000041, A040051.",
				"Cf. A141053 (F(5*n+3) and Benford’s Law). - _Johannes W. Meijer_, Jul 08 2011"
			],
			"keyword": "nonn,base",
			"offset": "0,3",
			"author": "_Robert G. Wilson v_, Jun 08 2010",
			"ext": [
				"Edited by _N. J. A. Sloane_, Jun 08 2010"
			],
			"references": 2,
			"revision": 20,
			"time": "2019-04-21T07:46:52-04:00",
			"created": "2010-07-11T03:00:00-04:00"
		}
	]
}