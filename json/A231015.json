{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A231015",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 231015,
			"data": "7,1,4,2,3,2,3,6,7,6,4,6,3,5,3,5,7,6,7,6,4,5,4,5,7,9,7,5,4,5,4,6,7,6,7,5,7,5,7,6,7,6,7,9,8,5,8,5,7,6,7,6,11,5,8,5,7,6,7,6,7,10,7,6,7,6,7,9,7,10,7,6,7,6,8,9,8,9,8,9,7,6,7,6,11,9",
			"name": "Least k such that n = +- 1^2 +- 2^2 +- 3^2 +- 4^2 +- ... +- k^2 for some choice of +- signs.",
			"comment": [
				"Erdős and Surányi proved that for each n there are infinitely many k satisfying the equation.",
				"A158092(k) is the number of solutions to 0 = +-1^2 +- 2^2 +- ... +- k^2. The first nonzero value is A158092(7) = 2, so a(0) = 7.",
				"a(n) is also defined for n \u003c 0, and clearly a(-n) = a(n).",
				"See A158092 and the Andrica-Ionascu links for more comments.",
				"The integral formula (3.6) in Andrica-Vacaretu (see Theorem 3 of the INTEGERS 2013 slides which has a typo) gives in this case the number of representations of  n as +- 1^2 +- 2^2 +- ... +- k^2 for some choice of +- signs. This integral formula is (2^n/2*Pi)*int _0^{2*Pi} cos(n*t) * prod_{j=1..k} cos(j^2*t) dt. Clearly the number of such representations of n is the coefficient of z^n in the expansion (z^(1^2)+z^(-1^2))*(z^(2^2)+z^(-2^2))*..*(z^(k^2)+z^(-k^2)). Andrica-Vacaretu used this generating function to prove the integral formula. Section 4 of Andrica-Vacaretu gives a table of the number of such representations of n for k=1,..,9. - Dorin Andrica, Nov 12 2013"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A231015/b231015.txt\"\u003eTable of n, a(n) for n = 0..20000\u003c/a\u003e",
				"D. Andrica and E. J. Ionascu, \u003ca href=\"http://www.westga.edu/~math/IntegersConference2013/aaAbstracts.pdf?usp=sharing\"\u003eVariations on a result of Erdős and Surányi\u003c/a\u003e, Integers Conference 2013 Abstract.",
				"D. Andrica and E. J. Ionascu, \u003ca href=\"http://www.dorinandrica.ro/files/presentation-INTEGERS-2013.pdf\"\u003eVariations on a result of Erdős and Surányi\u003c/a\u003e, INTEGERS 2013 slides.",
				"D. Andrica and D. Vacaretu, \u003ca href=\"http://www.cs.ubbcluj.ro/~studia-m/2006-4/andrica.pdf\"\u003eRepresentation theorems and almost unimodal sequences\u003c/a\u003e, Studia Univ. Babes-Bolyai, Mathematica, Vol. LI, 4 (2006), 23-33.",
				"P. Erdős and J. Surányi, \u003ca href=\"http://www.renyi.hu/~p_erdos/1959-05.pdf\"\u003eEgy additív számelméleti probléma\u003c/a\u003e (in Hungarian; Russian and German summaries), Mat. Lapok 10 (1959), pp. 284-290."
			],
			"formula": [
				"a(n(n+1)(2n+1)/6) = a(A000330(n)) = n for n \u003e 0.",
				"a((n(n+1)(2n+1)/6)-2) = a(A000330(n)-2) = n for n \u003e 0."
			],
			"example": [
				"0 = 1^2 + 2^2 - 3^2 + 4^2 - 5^2 - 6^2 + 7^2.",
				"1 = 1^2.",
				"2 = - 1^2 - 2^2 - 3^2 + 4^2.",
				"3 = - 1^2 + 2^2.",
				"4 = - 1^2 - 2^2 + 3^2."
			],
			"maple": [
				"b:= proc(n, i) option remember; local m; m:=i*(i+1)*(2*i+1)/6;",
				"      n\u003c=m and (n=m or b(n+i^2, i-1) or b(abs(n-i^2), i-1))",
				"    end:",
				"a:= proc(n) local k; for k while not b(n, k) do od; k end:",
				"seq(a(n), n=0..100);  # _Alois P. Heinz_, Nov 03 2013"
			],
			"mathematica": [
				"b[n_, i_] := b[n, i] = Module[{m}, m = i*(i+1)*(2*i+1)/6; n \u003c= m \u0026\u0026 (n == m || b[n+i^2, i-1] || b[Abs[n-i^2], i-1])]; a[n_] := Module[{k}, For[k = 1, !b[n, k] , k++]; k]; Table[a[n], {n, 0, 100}] (* _Jean-François Alcover_, Jan 28 2014, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A000330, A158092, A231071, A231272."
			],
			"keyword": "nonn,look",
			"offset": "0,1",
			"author": "_Jonathan Sondow_, Nov 02 2013",
			"ext": [
				"a(4) corrected and a(5)-a(85) from _Donovan Johnson_, Nov 03 2013"
			],
			"references": 6,
			"revision": 52,
			"time": "2017-06-26T09:48:19-04:00",
			"created": "2013-11-03T06:40:52-05:00"
		}
	]
}