{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A329450",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 329450,
			"data": "0,1,8,7,2,13,12,3,6,9,15,5,10,4,11,14,16,18,17,21,19,23,25,26,20,22,24,27,28,29,34,31,32,33,30,35,39,37,38,40,36,41,44,43,42,45,46,47,48,51,54,57,58,53,52,59,56,49,50,55,60,61,62,63,66,67,68,65,64,69,71,72,70,73,74,79,76",
			"name": "Lexicographically earliest sequence of distinct nonnegative integers such that neither a(n) + a(n+1) nor a(n) + a(n+2) is prime for any n.",
			"comment": [
				"Equivalently: For any three consecutive terms, there is no prime among any of the pairwise sums. Or: For any n and 0 \u003c= i \u003c j \u003c= 2, a(n+i) + a(n+j) is never prime.",
				"For any n, a term a(n) which meets the requirements always exists: For any a(n-2), a(n-1), at least one in five consecutive values of k is such that one among {a(n-2) + k, a(n-1) + k} is divisible by 2 and the other one by 3.",
				"Conjectured to be a permutation of the nonnegative integers. The restriction to positive indices is then a permutation of the positive integers with the same property, but not the lexicographically earliest given in A329405.",
				"See the wiki page for additional considerations and other variants. - _M. F. Hasler_, Nov 24 2019"
			],
			"link": [
				"Eric Angelini, \u003ca href=\"http://cinquantesignes.blogspot.com/2019/11/prime-sums-from-neighbouring-terms.html\"\u003ePrime sums from neighbouring terms\u003c/a\u003e, personal blog \"Cinquante signes\" (and post to the SeqFan list), Nov. 11, 2019.",
				"M. F. Hasler, \u003ca href=\"/wiki/User:M._F._Hasler/Prime_sums_from_neighboring_terms\"\u003ePrime sums from neighboring terms\u003c/a\u003e, OEIS wiki, Nov. 23, 2019"
			],
			"example": [
				"After the smallest possible initial terms, a(0) = 0, a(1) = 1, the next term must be neither a prime nor a prime - 1. The smallest possibility is a(2) = 8.",
				"The next term must not be a prime - 1 nor a prime - 8, which excludes 2, 4, 6 on one hand, and 3 and 5 on the other hand. The smallest possibility is a(3) = 7."
			],
			"mathematica": [
				"Nest[Block[{k = 2}, While[Nand[FreeQ[#, k], ! PrimeQ[#[[-1]] + k], ! PrimeQ[#[[-2]] + k]], k++]; Append[#, k]] \u0026, {0, 1}, 89] (* _Michael De Vlieger_, Nov 15 2019 *)"
			],
			"program": [
				"(PARI) A329450(n,show=0,o=0,p=o,U=[])={for(n=o,n-1, show\u0026\u0026print1(p\",\"); U=setunion(U,[p]); while(#U\u003e1\u0026\u0026U[1]==U[2]-1, U=U[^1]); for(k=U[1]+1,oo, setsearch(U,k) || isprime(o+k) || isprime(p+k) || [o=p, p=k, break]));p} \\\\ Optional args: show=1: print a(o..n-1); o=1: start with a(1) = 1 (A329405). See the wiki page for more general code returning a vector: S(n,0,3) = A329450(0..n-1)."
			],
			"xref": [
				"Cf. A329333 (always one odd prime among a(n+i)+a(n+j), 0 \u003c= i \u003c j \u003c= 2).",
				"Cf. A329405 (analog for positive integers)."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_M. F. Hasler_, based on an idea of _Eric Angelini_, Nov 13 2019",
			"ext": [
				"Edited by _N. J. A. Sloane_, Nov 14 2019",
				"New definition corrected by _M. F. Hasler_, Nov 15 2019"
			],
			"references": 29,
			"revision": 38,
			"time": "2019-11-24T11:05:54-05:00",
			"created": "2019-11-13T15:36:14-05:00"
		}
	]
}