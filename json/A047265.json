{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A047265",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 47265,
			"data": "1,-1,1,0,-2,1,0,1,-3,1,-1,0,3,-4,1,0,-2,-1,6,-5,1,-1,2,-3,-4,10,-6,1,0,-2,6,-3,-10,15,-7,1,0,2,-6,12,0,-20,21,-8,1,0,1,6,-16,19,9,-35,28,-9,1,0,0,0,16,-35,24,28,-56,36,-10,1,-1,2,-3,-6,40,-65,21,62,-84,45,-11,1",
			"name": "Triangle a(n,k) (n \u003e= 1, 1\u003c=k\u003c=n) giving coefficient of x^n in expansion of (Product_{j\u003e=1} (1-(-x)^j) - 1 )^k.",
			"comment": [
				"This is an ordinary convolution triangle. If a column k=0 starting at n=0 is added, then this is the Riordan triangle R(1, f(x)), with",
				"  f(x) = Product_{j\u003e=1} (1 -(-x)^j) - 1, generating {0, {A121373(n)}_{n\u003e=1}}. - _Wolfdieter Lang_, Feb 16 2021"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A047265/b047265.txt\"\u003eRows n = 1..200, flattened\u003c/a\u003e",
				"H. Gupta, \u003ca href=\"https://doi.org/10.1112/jlms/s1-39.1.433\"\u003eOn the coefficients of the powers of Dedekind's modular form\u003c/a\u003e, J. London Math. Soc., 39 (1964), 433-440.",
				"H. Gupta, \u003ca href=\"/A001482/a001482.pdf\"\u003eOn the coefficients of the powers of Dedekind's modular form\u003c/a\u003e (annotated and scanned copy)"
			],
			"formula": [
				"G.f. column k: (Product_{j\u003e=1} (1 - (-x)^j) - 1)^k, for k \u003e= 1. See the name and a Riordan triangle comment above. - _Wolfdieter Lang_, Feb 16 2021"
			],
			"example": [
				"Triangle starts:",
				"   1,",
				"  -1,   1,",
				"   0,  -2,   1,",
				"   0,   1,  -3,   1,",
				"  -1,   0,   3,  -4,   1,",
				"   0,  -2,  -1,   6,  -5,   1,",
				"  -1,   2,  -3,  -4,  10,  -6,   1,",
				"   0,  -2,   6,  -3, -10,  15,  -7,   1,",
				"   0,   2,  -6,  12,   0, -20,  21,  -8,   1,",
				"   0,   1,   6, -16,  19,   9, -35,  28,  -9,   1,",
				"   0,   0,   0,  16, -35,  24,  28, -56,  36, -10,   1,",
				"  -1,   2,  -3,  -6,  40, ..."
			],
			"maple": [
				"g:= proc(n) option remember; `if`(n=0, 1, add(add([-d, d, -2*d, d]",
				"      [1+irem(d, 4)], d=numtheory[divisors](j))*g(n-j), j=1..n)/n)",
				"    end:",
				"T:= proc(n, k) option remember;",
				"     `if`(k=0, `if`(n=0, 1, 0), `if`(k=1, `if`(n=0, 0, g(n)),",
				"         (q-\u003e add(T(j, q)*T(n-j, k-q), j=0..n))(iquo(k, 2))))",
				"    end:",
				"seq(seq(T(n, k), k=1..n), n=1..12);  # _Alois P. Heinz_, Feb 07 2021"
			],
			"mathematica": [
				"a[n_, k_] := SeriesCoefficient[(-1)^n*(Product[(1 - x^j), {j, 1, n}] - 1)^k, {x, 0, n}]; Table[a[n, k], {n, 1, 12}, {k, 1, n}] // Flatten (* _Jean-François Alcover_, Dec 05 2013 *)"
			],
			"program": [
				"(PARI) a(n,k)=polcoeff((-1)^n*(Ser(prod(i=1,n,1-x^i)-1)^k),n) \\\\ _Ralf Stephan_, Dec 08 2013"
			],
			"xref": [
				"Columns give A010815, A047654, A047655, A001482-A001488, A047649, A001490, A047938-A047648, A006665.",
				"Cf. A341418 (differently signed)."
			],
			"keyword": "sign,easy,nice,tabl",
			"offset": "1,5",
			"author": "_N. J. A. Sloane_",
			"references": 3,
			"revision": 31,
			"time": "2021-03-07T17:32:17-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}