{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A154914",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 154914,
			"data": "4,5,5,13,-24,13,35,-30,-30,35,97,-936,1584,-936,97,275,2940,-2700,-2700,2940,275,793,-78570,168012,-194400,168012,-78570,793,2315,1153350,-2002140,960120,960120,-2002140,1153350,2315,6817,-24113544,46757880,-42378336,35090496,-42378336,46757880,-24113544,6817",
			"name": "Triangle T(n,m,p,q) = (p^(n-k)*q^k + p^k*q^(n-k))*(StirlingS1(n, k) + StirlingS1(n, n-k)) with p=2 and q=3, read by rows.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A154914/b154914.txt\"\u003eRows n = 0..50 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n,m,p,q) = (p^(n-k)*q^k + p^k*q^(n-k))*(StirlingS1(n, k) + StirlingS1(n, n-k)) with p=2 and q=3.",
				"Sum_{k=0..n} T(n,k,p,q) = 2*(-p)^n*Pochhammer(-q/p, n) + p^(n+1)*[n \u003c 2], where p=2 and q=3. - _G. C. Greubel_, Mar 02 2021"
			],
			"example": [
				"Triangle begins as:",
				"     4;",
				"     5,       5;",
				"    13,     -24,       13;",
				"    35,     -30,      -30,      35;",
				"    97,    -936,     1584,    -936,     97;",
				"   275,    2940,    -2700,   -2700,   2940,      275;",
				"   793,  -78570,   168012, -194400, 168012,   -78570,     793;",
				"  2315, 1153350, -2002140,  960120, 960120, -2002140, 1153350, 2315;"
			],
			"maple": [
				"A154914:= (n,k,p,q) -\u003e (p^(n-k)*q^k + p^k*q^(n-k))*(combinat[stirling1](n, k) + combinat[stirling1](n, n-k));",
				"seq(seq(A154914(n,k,2,3), k=0..n), n=0..12); # _G. C. Greubel_, Mar 02 2021"
			],
			"mathematica": [
				"T[n_, k_, p_, q_]:= (p^(n-k)*q^k + p^k*q^(n-k))*(StirlingS1[n, k] + StirlingS1[n, n-k]);",
				"Table[T[n, k, 2, 3], {n,0,12}, {k,0,n}]//Flatten (* modified by _G. C. Greubel_, Mar 02 2021 *)"
			],
			"program": [
				"(Sage)",
				"def A154914(n,k,p,q): return (p^(n-k)*q^k + p^k*q^(n-k))*(stirling_number1(n, k) + stirling_number1(n, n-k))",
				"flatten([[A154914(n,k,2,3) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, Mar 02 2021",
				"(Magma)",
				"A154914:= func\u003c n,k,p,q | (p^(n-k)*q^k + p^k*q^(n-k))*(StirlingFirst(n, k) + StirlingFirst(n, n-k)) \u003e;",
				"[A154914(n,k,2,3): k in [0..n], n in [0..12]]; // _G. C. Greubel_, Mar 02 2021"
			],
			"xref": [
				"Cf. A154913 (q=1), this sequence (q=3).",
				"Cf. A048994, A154915, A154916, A154922."
			],
			"keyword": "tabl,sign,easy,less",
			"offset": "0,1",
			"author": "_Roger L. Bagula_, Jan 17 2009",
			"ext": [
				"Edited by _G. C. Greubel_, Mar 02 2021"
			],
			"references": 5,
			"revision": 13,
			"time": "2021-03-02T02:07:06-05:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}