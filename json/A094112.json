{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A094112",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 94112,
			"data": "1,0,2,0,3,3,0,12,8,4,0,60,40,15,5,0,360,240,90,24,6,0,2520,1680,630,168,35,7,0,20160,13440,5040,1344,280,48,8,0,181440,120960,45360,12096,2520,432,63,9,0,1814400,1209600,453600,120960,25200,4320,630,80,10,0",
			"name": "Triangle read by rows: T(n,k) is the number of permutations p of [n] in which the length of the longest initial segment avoiding the 123-, the 132- and the 231-pattern is equal to k.",
			"comment": [
				"Row sums are the factorial numbers (A000142)."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A094112/b094112.txt\"\u003eTable of n, a(n) for n = 1..11325\u003c/a\u003e (rows 1 \u003c= n \u003c= 150, flattened)",
				"Olivier Bodini, Antoine Genitrini, Mehdi Naima, \u003ca href=\"https://arxiv.org/abs/1808.08376\"\u003eRanked Schröder Trees\u003c/a\u003e, arXiv:1808.08376 [cs.DS], 2018.",
				"Olivier Bodini, Antoine Genitrini, Cécile Mailler, Mehdi Naima, \u003ca href=\"https://hal.archives-ouvertes.fr/hal-02865198\"\u003eStrict monotonic trees arising from evolutionary processes: combinatorial and probabilistic study\u003c/a\u003e, hal-02865198 [math.CO] / [math.PR] / [cs.DS] / [cs.DM], 2020.",
				"E. Deutsch and W. P. Johnson, \u003ca href=\"http://www.jstor.org/stable/3219101\"\u003eCreate your own permutation statistics\u003c/a\u003e, Math. Mag., 77, 130-134, 2004.",
				"R. Simion and F. W. Schmidt, \u003ca href=\"https://doi.org/10.1016/S0195-6698(85)80052-4\"\u003eRestricted permutations\u003c/a\u003e, European J. Combin., 6, 383-406, 1985."
			],
			"formula": [
				"T(n, k) = n!/[(k-2)!k] for 2\u003c=k\u003c=n-1; T(n, n)=n; T(n, 1)=0 for n\u003e=2; T(n, k)=0 for k\u003en.",
				"G.f.: sum(T(n, k)t^k z^n/n!, n, k\u003e=1) = z[(t-1)exp(tz)+1]/(1-z)."
			],
			"example": [
				"T(4,3)=8 because the permutations 2134, 2143, 3124, 3142, 3241, 4123, 4132 and 4231 do not avoid all three patterns 123, 132 and 231, but their initial segments of length three, namely 213, 214, 312, 314, 324, 412, 413 and 423, do.",
				"Triangle begins:",
				"1;",
				"0,2;",
				"0,3,3;",
				"0,12,8,4;",
				"0,60,40,15,5;",
				"0,360,240,90,24,6;",
				"..."
			],
			"maple": [
				"T:=proc(n,k) if n=1 and k=1 then 1 elif n=1 then 0 elif k=1 then 0 elif k=n then n elif k\u003e1 and k\u003cn then n!/(k-2)!/k else 0 fi end: seq(seq(T(n,k),k=1..n),n=1..11);"
			],
			"mathematica": [
				"T[n_, k_] := Which[n == 1 \u0026\u0026 k == 1, 1, n == 1, 0, k == 1, 0, k == n, n, k \u003e 1 \u0026\u0026 k \u003c n, n!/(k-2)!/k, True, 0]; Table[T[n, k], {n, 1, 11}, {k, 1, n}] // Flatten (* _Jean-François Alcover_, Jan 22 2019, from PARI *)"
			],
			"xref": [
				"Cf. A000142."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Emeric Deutsch_, May 31 2004",
			"references": 1,
			"revision": 20,
			"time": "2020-09-09T18:14:36-04:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}