{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A058183",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 58183,
			"data": "1,2,3,4,5,6,7,8,9,11,13,15,17,19,21,23,25,27,29,31,33,35,37,39,41,43,45,47,49,51,53,55,57,59,61,63,65,67,69,71,73,75,77,79,81,83,85,87,89,91,93,95,97,99,101,103,105,107,109,111,113,115,117,119,121,123,125",
			"name": "Number of digits in concatenation of first n positive integers.",
			"comment": [
				"Or, total number of digits in numbers from 1 through n."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A058183/b058183.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"GeeksforGeeks,  \u003ca href=\"https://www.geeksforgeeks.org/count-total-number-digits-1-n/\"\u003eCount total number of digits from 1 to n\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/SmarandacheNumber.html\"\u003eSmarandache Number\u003c/a\u003e"
			],
			"formula": [
				"a(n) = (n+1)*floor(log_10(10*n)) - (10^floor(log_10(10*n))-1)/(10-1).",
				"a(n) = a(n-1) + floor(log_10(10*n)).",
				"a(n) = A055642(A007908(n)).",
				"a(n) = A055642(A053064(n)). - _Reinhard Zumkeller_, Oct 10 2008",
				"a(n) ~ n log_10 n + O(n). In particular lim inf (n log_10 n - a(n))/n = (1+log(10/9)+log(log(10)))/log(10) and the corresponding lim sup is 10/9. - _Charles R Greathouse IV_, Sep 19 2012",
				"G.f.: (1-x)^(-2)*Sum_{j\u003e=0} x^(10^j). - _Robert Israel_, Nov 04 2015",
				"a(n) = b(n)*(n + 1) - (10^b(n) - 19)/9 - 2, where b(n) = A055642(n). - _Lorenzo Sauras Altuzarra_, May 09 2020",
				"a(n) = A055642(A000422(n)). - _Michel Marcus_, Sep 11 2021"
			],
			"example": [
				"a(12) = 15 since 123456789101112 has 15 digits."
			],
			"maple": [
				"a:= proc(n) a(n):= `if`(n=0, 0, a(n-1) +length(n)) end:",
				"seq(a(n), n=1..100);  # _Alois P. Heinz_, Nov 26 2013",
				"a := proc(n) local d; d:=floor(log10(n))+1; (n+1)*d - (10^d-1)/9; end; # _N. J. A. Sloane_, Feb 20 2020"
			],
			"mathematica": [
				"Length/@ Flatten/@ IntegerDigits/@ Flatten/@ Rest[FoldList[List, {}, Range[70]]] (* _Eric W. Weisstein_, Nov 04 2015 *)",
				"Table[With[{d = IntegerLength[n]}, (n+1) d - (10^d -1)/9], {n, 70}] (* _Eric W. Weisstein_, Nov 06 2015 *)",
				"IntegerLength/@ FoldList[#2 + #1 10^IntegerLength[#2] \u0026, Range[70]] (* _Eric W. Weisstein_, Nov 06 2015 *)",
				"Accumulate[ IntegerLength@ # \u0026 /@ Range @ 70] (* _Robert G. Wilson v_, Jul 31 2018 *)"
			],
			"program": [
				"(PARI) a(n)=my(t=log(10*n+.5)\\log(10));n*t+t-10^t\\9 \\\\ _Charles R Greathouse IV_, Sep 19 2012",
				"(PARI) a(n) = sum(k=1, n, #digits(k)); \\\\ _Michel Marcus_, Jan 01 2017"
			],
			"xref": [
				"Cf. A000422, A007908, A053064, A055642."
			],
			"keyword": "base,easy,nonn",
			"offset": "1,2",
			"author": "_Henry Bottomley_, Nov 17 2000",
			"references": 20,
			"revision": 57,
			"time": "2021-09-11T10:48:25-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}