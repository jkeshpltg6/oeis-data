{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A324319",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 324319,
			"data": "231,561,3655,5565,8911,10585,13695,23653,32131,45451,59685,74305,108345,115921,157641,243253,248865,302253,314821,334153,371091,392055,417241,458403,505515,546535,688551,702705,795691,821121,915981,932295,1004653,1145341,1181953",
			"name": "Terms of A324315 (squarefree integers m \u003e 1 such that if prime p divides m, then the sum of the base p digits of m is at least p) that are also hexagonal numbers (A000384) with index equal to their largest prime factor.",
			"comment": [
				"561, 8911, and 10585 are also Carmichael numbers (A002997).",
				"The smallest primary Carmichael number (A324316) in the sequence is 8801128801 = 181 * 733 * 66337 = A000384(66337).",
				"See the section on polygonal numbers in Kellner and Sondow 2019.",
				"Subsequence of the special polygonal numbers A324973. - _Jonathan Sondow_, Mar 27 2019"
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A324319/b324319.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Bernd C. Kellner and Jonathan Sondow, Power-Sum Denominators, Amer. Math. Monthly, 124 (2017), 695-709. doi:\u003ca href=\"https://doi.org/10.4169/amer.math.monthly.124.8.695\"\u003e10.4169/amer.math.monthly.124.8.695\u003c/a\u003e, arXiv:\u003ca href=\"https://arxiv.org/abs/1705.03857\"\u003e1705.03857\u003c/a\u003e",
				"Bernd C. Kellner and Jonathan Sondow, \u003ca href=\"https://arxiv.org/abs/1902.10672\"\u003eOn Carmichael and polygonal numbers, Bernoulli polynomials, and sums of base-p digits\u003c/a\u003e, arXiv:1902.10672 [math.NT], 2019."
			],
			"example": [
				"A324315(1) = 231 = 3 * 7 * 11 = 11 * (2 * 11 - 1) = A000384(11), so 231 is a member."
			],
			"mathematica": [
				"SD[n_, p_] := If[n \u003c 1 || p \u003c 2, 0, Plus @@ IntegerDigits[n, p]];",
				"LP[n_] := Transpose[FactorInteger[n]][[1]];",
				"HN[n_] := n(2n - 1);",
				"TestS[n_] := (n \u003e 1) \u0026\u0026 SquareFreeQ[n] \u0026\u0026 VectorQ[LP[n], SD[n, #] \u003e= # \u0026];",
				"Select[HN@ Prime[Range[100]], TestS[#] \u0026]"
			],
			"xref": [
				"Cf. A000384, A002997, A195441, A324315, A324316, A324317, A324318, A324320, A324369, A324370, A324371, A324404, A324405, A324973."
			],
			"keyword": "nonn,base",
			"offset": "1,1",
			"author": "_Bernd C. Kellner_ and _Jonathan Sondow_, Feb 23 2019",
			"ext": [
				"More terms from _Amiram Eldar_, Dec 05 2020"
			],
			"references": 12,
			"revision": 29,
			"time": "2020-12-05T04:53:04-05:00",
			"created": "2019-03-01T04:39:06-05:00"
		}
	]
}