{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007307",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7307,
			"data": "0,1,2,1,3,3,4,6,7,10,13,17,23,30,40,53,70,93,123,163,216,286,379,502,665,881,1167,1546,2048,2713,3594,4761,6307,8355,11068,14662,19423,25730,34085,45153,59815,79238,104968,139053,184206,244021,323259,428227",
			"name": "a(n) = a(n-2) + a(n-3).",
			"comment": [
				"Also the number of maximal matchings in the (n-2)-pan graph. - _Eric W. Weisstein_, Dec 30 2017"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A007307/b007307.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Matching.html\"\u003eMatching\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/MaximalIndependentEdgeSet.html\"\u003eMaximal Independent Edge Set\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PanGraph.html\"\u003ePan Graph\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0, 1, 1)."
			],
			"formula": [
				"a(n) = p(n-1) + 2*p(n-2) = p(n+1) + p(n-2), with p(n):=A000931(n+3). O.g.f: x*(1+2*x)/(1-x^2-x^3). - _Wolfdieter Lang_, Jun 15 2010"
			],
			"maple": [
				"G(x):=(-1-x^3)/(-1+x^2+x^3): f[0]:=G(x): for n from 1 to 58 do f[n]:=diff(f[n-1],x) od: x:=0: seq(f[n]/n!,n=1..43); # _Zerinvary Lajos_, Mar 27 2009",
				"# second Maple program:",
				"a:= n-\u003e (\u003c\u003c0|1|0\u003e, \u003c0|0|1\u003e, \u003c1|1|0\u003e\u003e^n.\u003c\u003c($0..2)\u003e\u003e)[1$2]:",
				"seq(a(n), n=0..60);  # _Alois P. Heinz_, Nov 06 2016"
			],
			"mathematica": [
				"Join[{a=0,b=1,c=2},Table[d=a+b;a=b;b=c;c=d,{n,100}]] (* _Vladimir Joseph Stephan Orlovsky_, Feb 26 2011 *)",
				"Table[- RootSum[-1 - # + #^3 \u0026, -16 #^n - 13 #^(n + 1) + #^(n + 2) \u0026]/23, {n, 20}] (* _Eric W. Weisstein_, Dec 30 2017 *)",
				"LinearRecurrence[{0, 1, 1}, {1, 3, 3}, 20] (* _Eric W. Weisstein_, Dec 30 2017 *)",
				"CoefficientList[Series[x (-1 - 3 x - 2 x^2)/(-1 + x^2 + x^3), {x, 0, 20}], x] (* _Eric W. Weisstein_, Dec 30 2017 *)"
			],
			"program": [
				"(MAGMA) I:=[0,1,2]; [n le 3 select I[n] else Self(n-2)+Self(n-3): n in [1..50]]; // _Vincenzo Librandi_, Jun 09 2013"
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_.",
			"references": 5,
			"revision": 26,
			"time": "2017-12-30T17:27:24-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}