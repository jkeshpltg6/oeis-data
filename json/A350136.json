{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A350136",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 350136,
			"data": "550675372,321746104,509549854,792520205,764842161,1014906390,5320116676,47057538,753751947,359455043,126726823,551117432,374101871,1178486506",
			"name": "Pulsar periods from the Pioneer/Voyager plaques (1972).",
			"comment": [
				"The Pioneer 10 \u0026 11 and Voyager 1 \u0026 2 spacecraft each carry a plaque engraved with a circular \"Pulsar Map\" containing information on 14 selected pulsars, which can theoretically be used to locate the Earth from within the Milky Way galaxy. The period (cycle time) of each pulsar's oscillation (as measured in 1972) is expressed as a constant integer multiple of the hyperfine transition period of the hydrogen atom (A294666). These 14 constants are engraved on the plaque as binary integers, each having 26 to 33 bits, which were calculated in decimal as",
				"     a(n) = round((period of n-th pulsar in seconds) * 1420405752 Hz).",
				"The 14 pulsars are arranged counterclockwise around the circular Pulsar Map, in order of increasing galactic longitude, starting at the 3 o'clock position (right side) of the circle. The terms listed here follow the same order, but are expressed as decimal integers. In a paper by the plaque's designers (Sagan et al., 1972) the 14 pulsars are listed in \"Table 1\" in order of right ascension, which differs somewhat from the plaque order, but the Table 1 constants (in decimal) exactly match those on the plaque (in binary). The value of the hyperfine transition period used as multiplier (1420405752) is also given in that paper.",
				"An informative and detailed retrospective of the Pulsar Map, posted 35 years later by William Johnston (2007), compares the plaque's 14 constants with subsequently improved Pulsar measurements, and concludes that only one of them (a(12)) is too inaccurate to calculate the plaque's design date within 1 year.",
				"The hyperfine transition period is now known to an accuracy of 13 digits, but that value, had it been used at the time, would not have changed any of the original a(n) values.",
				"Note that terms are the original plaque values (1972), not any of the later improved values; also, terms are not in ascending order. These 14 integers are the first high-accuracy numerical data to leave our solar system in the form of \"hard copy\"."
			],
			"link": [
				"ATNF (Australia Telescope National Facility), \u003ca href=\"https://www.atnf.csiro.au/people/pulsar/psrcat/\"\u003ePulsar Catalog\u003c/a\u003e, updated at least once each year during (2003-2021).",
				"W. R. Johnston, \u003ca href=\"http://www.johnstonsarchive.net/astro/pulsarmap.html\"\u003eReading the Pioneer/Voyager Pulsar Map\u003c/a\u003e, 2007-Oct-30. All six of Johnston's tables list the pulsars in order of decreasing galactic longitude, i.e., reading clockwise around the circular Pulsar Map, starting at 3 o'clock. In his Table 5, the \"derived dates\" of psr #3 and psr #14 (a(12) and a(1)) are erroneously swapped.",
				"C. Sagan, L. S. Sagan, F. D. Drake, \u003ca href=\"https://jstor.org/stable/173364\"\u003eA Message from Earth\u003c/a\u003e, Science, 175, #4024, 881-884, 1972. Stable web link, must register to read full article.",
				"C. Sagan, L. S. Sagan, F. D. Drake, \u003ca href=\"https://astro.swarthmore.edu/astro61_spring2014/papers/sagan_science_1972.pdf\"\u003eA Message from Earth\u003c/a\u003e. Less stable web link, has full article."
			],
			"xref": [
				"Cf. A294666."
			],
			"keyword": "nonn,easy,fini,full",
			"offset": "1,1",
			"author": "_Robert B Fowler_, Dec 15 2021",
			"references": 0,
			"revision": 26,
			"time": "2021-12-23T11:58:33-05:00",
			"created": "2021-12-23T11:58:33-05:00"
		}
	]
}