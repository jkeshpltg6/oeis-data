{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A328644",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 328644,
			"data": "1,1,6,7,9,27,13,84,54,108,11,39,126,54,81,133,990,1755,3780,1215,1458,463,2793,10395,12285,19845,5103,5103,1261,11112,33516,83160,73710,95256,20412,17496,4039,34047,150012,301644,561330,398034,428652,78732,59049",
			"name": "Triangular array read by rows: row n shows the coefficients of the polynomial p(x,n) constructed as in Comments; these polynomials form a strong divisibility sequence.",
			"comment": [
				"Suppose q is a rational number such that the number r = sqrt(q) is irrational. The function (r x + r)^n - (r x - 1/r)^n of x can be represented as k*p(x,n), where k is a constant and p(x,n) is a product of nonconstant polynomials having GCD = 1; the sequence p(x,n) is a strong divisibility sequence of polynomials; i.e., gcd(p(x,h),p(x,k)) = p(x,gcd(h,k)).  For A327320, r = sqrt(3/2).  If x is an integer, then p(x,n) is a strong divisibility sequence of integers."
			],
			"example": [
				"We have p(x,3) = (1/k)((5 (7 + 9 x + 27 x^2))/(6 sqrt(6))), where k = 5/(6 sqrt(6)).",
				"First six rows:",
				"  1;",
				"  1, 6;",
				"  7, 9, 27;",
				"  13, 84, 54, 108;",
				"  11, 39, 126, 54, 81;",
				"  133, 990, 1755, 3780, 1215, 1458;",
				"The first six polynomials, not factored:",
				"1, 1 + 6 x, 7 + 9 x + 27 x^2, 13 + 84 x + 54 x^2 + 108 x^3, 11 + 39 x + 126 x^2 + 54 x^3 + 81 x^4, 133 + 990 x + 1755 x^2 + 3780 x^3 + 1215 x^4 + 1458 x^5.",
				"The first six polynomials, factored:",
				"1, 1 + 6 x, 7 + 9 x + 27 x^2, (1 + 6 x) (13 + 6 x + 18 x^2), 11 + 39 x + 126 x^2 + 54 x^3 + 81 x^4, (1 + 6 x) (19 + 3 x + 9 x^2) (7 + 9 x + 27 x^2)."
			],
			"mathematica": [
				"c[poly_] := If[Head[poly] === Times, Times @@ DeleteCases[(#1 (Boole[",
				"MemberQ[#1, x] || MemberQ[#1, y] || MemberQ[#1, z]] \u0026) /@",
				"Variables /@ #1 \u0026)[List @@ poly], 0], poly];",
				"r = Sqrt[3/2]; f[x_, n_] := c[Factor[Expand[(r x + r)^n - (r x - 1/r)^n]]];",
				"Table[f[x, n], {n, 1, 6}]",
				"Flatten[Table[CoefficientList[f[x, n], x], {n, 1, 12}]]  (* A328644 *)",
				"(* _Peter J. C. Moses_, Nov 01 2019 *)"
			],
			"xref": [
				"Cf. A327320, A327321, A329017, A329018, A329019."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Clark Kimberling_, Nov 03 2019",
			"references": 4,
			"revision": 6,
			"time": "2019-11-06T19:15:50-05:00",
			"created": "2019-11-06T19:15:50-05:00"
		}
	]
}