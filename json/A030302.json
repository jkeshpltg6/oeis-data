{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A030302",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 30302,
			"data": "1,1,0,1,1,1,0,0,1,0,1,1,1,0,1,1,1,1,0,0,0,1,0,0,1,1,0,1,0,1,0,1,1,1,1,0,0,1,1,0,1,1,1,1,0,1,1,1,1,1,0,0,0,0,1,0,0,0,1,1,0,0,1,0,1,0,0,1,1,1,0,1,0,0,1,0,1,0,1,1,0,1,1,0,1,0,1,1,1,1",
			"name": "Write n in base 2 and juxtapose; irregular table in which row n lists the binary expansion of n.",
			"comment": [
				"The binary Champernowne constant: it is normal in base 2. - _Jason Kimberley_, Dec 07 2012",
				"A word that is recurrent, but neither morphic nor uniformly recurrent. - _N. J. A. Sloane_, Jul 14 2018",
				"See A030303 for the indices of 1's (so this is the characteristic function of A030303), with first differences (i.e., run lengths of 0's, increased by 1, with two consecutive 1's delimiting a run of zero 0's) given by A066099. - _M. F. Hasler_, Oct 12 2020"
			],
			"reference": [
				"Michel Rigo, Formal Languages, Automata and Numeration Systems, 2 vols., Wiley, 2014. Mentions this sequence - see \"List of Sequences\" in Vol. 2."
			],
			"link": [
				"Jean-Paul Allouche, Julien Cassaigne, Jeffrey Shallit and Luca Q. Zamboni, \u003ca href=\"https://arxiv.org/abs/1711.10807\"\u003eA Taxonomy of Morphic Sequences\u003c/a\u003e, arXiv preprint arXiv:1711.10807 [cs.FL], Nov 29 2017."
			],
			"formula": [
				"a(n) = (floor(2^(((n + 2^i - 2) mod i) - i + 1) * ceiling((n + 2^i - 1)/i - 1))) mod 2 where i = ceiling( W(log(2)/2 (n - 1))/log(2) + 1 ) and W denotes the principal branch of the Lambert W function. See also Mathematica code. - David W. Cantrell (DWCantrell(AT)sigmaxi.net), Feb 19 2007"
			],
			"maple": [
				"A030302 := proc(n) local i,t1,t2; t1:=convert(n,base,2); t2:=nops(t1); [seq(t1[t2+1-i],i=1..t2)]; end; # _N. J. A. Sloane_, Apr 08 2021"
			],
			"mathematica": [
				"i[n_] := Ceiling[FullSimplify[ProductLog[Log[2]/2 (n - 1)]/Log[2] + 1]]; a[n_] := Mod[Floor[2^(Mod[n + 2^i[n] - 2, i[n]] - i[n] + 1) Ceiling[(n + 2^i[n] - 1)/i[n] - 1]], 2]; (* David W. Cantrell (DWCantrell(AT)sigmaxi.net), Feb 19 2007 *)",
				"Join @@ Table[ IntegerDigits[i, 2], {i, 1, 40}] (* _Olivier Gérard_, Mar 28 2011 *)",
				"Flatten@ IntegerDigits[ Range@ 25, 2] (* or *)",
				"almostNatural[n_, b_] := Block[{m = 0, d = n, i = 1, l, p}, While[m \u003c= d, l = m; m = (b - 1) i*b^(i - 1) + l; i++]; i--; p = Mod[d - l, i]; q = Floor[(d - l)/i] + b^(i - 1); If[p != 0, IntegerDigits[q, b][[p]], Mod[q - 1, b]]]; Array[ almostNatural[#, 2] \u0026, 105] (* _Robert G. Wilson v_, Jun 29 2014 *)"
			],
			"program": [
				"(MAGMA) \u0026cat[Reverse(IntegerToSequence(n,2)): n in [1..31]]; // _Jason Kimberley_, Mar 02 2012"
			],
			"xref": [
				"Essentially the same as A007088 and A030190. Cf. A030303, A007088.",
				"Tables in which the n-th row lists the base b digits of n: A030190 and this sequence (b=2), A003137 and A054635 (b=3), A030373 (b=4), A031219 (b=5), A030548 (b=6), A030998 (b=7), A031035 and A054634 (b=8), A031076 (b=9), A007376 and A033307 (b=10). [_Jason Kimberley_, Dec 06 2012]",
				"Sequences mentioned in the Allouche et al. \"Taxonomy\" paper, listed by example number: 1: A003849, 2: A010060, 3: A010056, 4: A020985 and A020987, 5: A191818, 6: A316340 and A273129, 18: A316341, 19: A030302, 20: A063438, 21: A316342, 22: A316343, 23: A003849 minus its first term, 24: A316344, 25: A316345 and A316824, 26: A020985 and A020987, 27: A316825, 28: A159689, 29: A049320, 30: A003849, 31: A316826, 32: A316827, 33: A316828, 34: A316344, 35: A043529, 36: A316829, 37: A010060."
			],
			"keyword": "nonn,base,cons,easy,tabf,changed",
			"offset": "1,1",
			"author": "_Clark Kimberling_",
			"references": 54,
			"revision": 67,
			"time": "2022-01-09T22:54:43-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}