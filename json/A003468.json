{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003468",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3468,
			"id": "M5125",
			"data": "1,22,305,3410,33621,305382,2619625,21554170,171870941,1337764142,10216988145,76862115330,571247591461,4203844925302,30687029023865,222518183370890,1604626924403181,11518132293452862",
			"name": "Number of minimal 3-covers of a labeled n-set.",
			"comment": [
				"This is also the fourth column of the Sheffer triangle A143496 (4-restricted Stirling2 numbers). See the e.g.f. given below. See also the Sheffer comments in A193685. - _Wolfdieter Lang_, Oct 08 2011"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A003468/b003468.txt\"\u003eTable of n, a(n) for n = 3..1000\u003c/a\u003e",
				"T. Hearne and C. G. Wagner, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(73)90141-6\"\u003eMinimal covers of finite sets\u003c/a\u003e, Discr. Math. 5 (1973), 247-251.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/MinimalCover.html\"\u003eMinimal cover.\u003c/a\u003e"
			],
			"formula": [
				"G.f.: x^3/((1 - 4*x)*(1 - 5*x)*(1 - 6*x)*(1 - 7*x)). - _N. J. A. Sloane_, May 12 1994, corrected by _Vaclav Kotesovec_, Nov 19 2012",
				"E.g.f.: (exp(4*x)*(exp(x) - 1)^3)/6. More generally, e.g.f. for number of minimal m-covers of a labeled n-set is (exp((2^m - m - 1)*x)*(exp(x) - 1)^m)/m!. - _Vladeta Jovovic_, May 09 2004",
				"If we define f(m, j, x) = sum(binomial(m, k)*stirling2(k, j)*x^(m - k),k = j .. m) then a(n) = f(n, 3, 4), (n \u003e= 3). - _Milan Janjic_, Apr 26 2009",
				"a(n) = 7^n/6 - 6^n/2 + 5^n/2 - 4^n/6. - _Vaclav Kotesovec_, Nov 19 2012"
			],
			"maple": [
				"A003468:=1/(6*z-1)/(4*z-1)/(7*z-1)/(5*z-1); # conjectured by _Simon Plouffe_ in his 1992 dissertation"
			],
			"mathematica": [
				"Table[7^n/6 - 6^n/2 + 5^n/2 - 4^n/6, {n, 3, 20}] (* _Vaclav Kotesovec_, Nov 19 2012 *)"
			],
			"program": [
				"(MAGMA) [7^n/6 - 6^n/2 + 5^n/2 - 4^n/6: n in [3..30]]; // _Vincenzo Librandi_, May 03 2013"
			],
			"xref": [
				"Cf. A000392, A003468, A016111, A046166-A046169, A057668, A005783-A005786, A055066.",
				"Cf. A143496, A000302, A005060, A016103."
			],
			"keyword": "nonn",
			"offset": "3,2",
			"author": "_N. J. A. Sloane_",
			"references": 6,
			"revision": 60,
			"time": "2021-03-12T22:32:36-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}