{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A249223",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 249223,
			"data": "1,1,1,0,1,1,1,0,1,1,2,1,0,0,1,1,1,1,0,1,1,1,1,0,1,0,0,0,1,1,2,2,1,0,0,0,1,1,1,0,1,0,1,1,2,1,1,1,1,1,1,0,0,0,0,1,1,2,1,1,1,0,0,0,0,1,1,1,1,2,1,0,1,1,1,0,1,1,1,0,0,0,1,0,0,0,0,0,1,1,2,2,2,2",
			"name": "Triangle read by rows: row n gives partial alternating sums of row n of A237048.",
			"comment": [
				"All entries in the triangle are nonnegative since the number of 1's in odd-numbered columns of A237048 prior to column j, 1 \u003c= j \u003c= row(n), is at least as large as the number of 1's in even-numbered columns through column j. As a consequence:",
				"(a)  The two adjacent symmetric Dyck paths whose legs are defined by adjacent rows of triangle A237593 never cross each other (see also A235791 and A237591) and the rows in this triangle describe the widths between the legs.",
				"(b)  Let legs(n) denote the n-th row of triangle A237591, widths(n) the n-th row of this triangle, and c(n) the rightmost entry in the n-th row of this triangle (center of the Dyck path). Then area(n) = 2 * legs(n) . width(n) - c(n), where \".\" is the inner product, is the area between the two adjacent symmetric Dyck paths.",
				"(c)  For certain sequences of integers, it is known that area(n) = sigma(n); see A238443, A245685, A246955, A246956 and A247687.",
				"Right border gives A067742. - _Omar E. Pol_, Jan 21 2017"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A249223/b249223.txt\"\u003eTable of n, a(n) for the first 150 rows, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = Sum_{j=1..k} (-1)^(j+1)*A237048(n, j), for n\u003e=1 and 1 \u003c= k \u003c= floor((sqrt(8*n + 1) - 1)/2). - corrected by _Hartmut F. W. Hoft_, Jan 25 2018"
			],
			"example": [
				"The sequence written as a triangle:",
				"  n\\k  1    2    3    4    5    6",
				"   1   1",
				"   2   1",
				"   3   1    0",
				"   4   1    1",
				"   5   1    0",
				"   6   1    1    2",
				"   7   1    0    0",
				"   8   1    1    1",
				"   9   1    0    1",
				"  10   1    1    1    0",
				"  11   1    0    0    0",
				"  12   1    1    2    2",
				"  13   1    0    0    0",
				"  14   1    1    1    0",
				"  15   1    0    1    1    2",
				"  16   1    1    1    1    1",
				"  17   1    0    0    0    0",
				"  18   1    1    2    1    1",
				"  19   1    0    0    0    0",
				"  20   1    1    1    1    2",
				"  21   1    0    1    1    1    0",
				"  22   1    1    1    0    0    0",
				"  23   1    0    0    0    0    0",
				"  24   1    1    2    2    2    2",
				"The triangle shows that area(n) has width 1 for powers of 2 and that area(p) for primes p consists of only 1 horizontal leg of width 1 (and its symmetric vertical leg in the mirror symmetric duplicate of this triangle)."
			],
			"maple": [
				"r := proc(n) floor((sqrt(1+8*n)-1)/2) ; end proc: # R. J. Mathar 2015 A003056",
				"A237048:=proc(n,k) local i; global r;",
				"if n\u003c(k-1)*k/2 or k\u003er(n) then return(0); fi;",
				"if (k mod 2)=1 and (n mod k)=0 then return(1); fi;",
				"if (k mod 2)=0 and ((n-k/2) mod k) = 0 then return(1); fi;",
				"return(0);",
				"end;",
				"A249223:=proc(n,k) local i; global r,A237048;",
				"if n\u003c(k-1)*k/2 or k\u003er(n) then return(0); fi;",
				"add( (-1)^(i+1)*A237048(n,i),i=1..k);",
				"end;",
				"for n from 1 to 12 do lprint([seq(A249223(n,k),k=1..r(n))]); od; # _N. J. A. Sloane_, Jan 15 2021"
			],
			"mathematica": [
				"cd[n_, k_] := If[Divisible[n, k], 1, 0]; row[n_] := Floor[(Sqrt[8 n + 1] - 1)/2]; a237048[n_, k_] := If[OddQ[k], cd[n, k], cd[n - k/2, k]];",
				"a1[n_, k_] := Sum[(-1)^(j + 1)*a237048[n, j], {j, 1, k}];",
				"a2[n_] := Drop[FoldList[Plus, 0, Map[(-1)^(# + 1) \u0026, Range[row[n]]] a237048[n]], 1]; Flatten[Map[a2, Range[24]]] (* data *) (* Corrected by _G. C. Greubel_, Apr 16 2017 *)"
			],
			"program": [
				"(PARI) t237048(n,k) = if (k % 2, (n % k) == 0, ((n - k/2) % k) == 0);",
				"kmax(n) = (sqrt(1+8*n)-1)/2;",
				"t(n,k) = sum(j=1, k, (-1)^(j+1)*t237048(n,j));",
				"tabf(nn) = {for (n=1, nn, for (k=1, kmax(n), print1(t(n,k), \", \");); print(););} \\\\ _Michel Marcus_, Sep 20 2015"
			],
			"xref": [
				"Cf. A000203, A237048, A237270, A237271, A237593, A238443, A245685, A246955, A246956, A247687."
			],
			"keyword": "nonn,tabf",
			"offset": "1,11",
			"author": "_Hartmut F. W. Hoft_, Oct 23 2014",
			"references": 37,
			"revision": 40,
			"time": "2021-01-15T18:16:08-05:00",
			"created": "2014-10-26T16:20:52-04:00"
		}
	]
}