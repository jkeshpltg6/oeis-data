{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A058655",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 58655,
			"data": "2,8,0,7,7,7,0,2,4,2,0,2,8,5,1,9,3,6,5,2,2,1,5,0,1,1,8,6,5,5,7,7,7,2,9,3,2,3,0,8,0,8,5,9,2,0,9,3,0,1,9,8,2,9,1,2,2,0,0,5,4,8,0,9,5,9,7,1,0,0,8,8,9,1,2,1,9,0,1,6,6,5,5,1,0,1,8,5,3,0,8,1,6,8,1,9,6,6,3,8,1,4,1,8,7",
			"name": "Decimal expansion of area under the curve 1/Gamma(x) from zero to infinity.",
			"comment": [
				"Referred to as the Fransén-Robinson constant.",
				"Named Fransén-Robinson constant after _Herman P. Robinson_ who calculated its value to 36 decimal digits (Fransén, 1979) and Arne Fransén who calculated its value to 80 decimal digits (1981). - _Amiram Eldar_, Aug 13 2020"
			],
			"reference": [
				"Steven R. Finch, Mathematical Constants, Cambridge University Press, 2003. See pp. 262-264."
			],
			"link": [
				"Harry J. Smith, \u003ca href=\"/A058655/b058655.txt\"\u003eTable of n, a(n) for n = 1..1001\u003c/a\u003e",
				"Arne Fransén, \u003ca href=\"https://doi.org/10.1007/BF01931232\"\u003eAccurate determination of the inverse gamma integral\u003c/a\u003e, BIT Numerical Mathematics, Vol. 19, No. 1 (1979), pp. 137-138.",
				"Arne Fransén, \u003ca href=\"https://doi.org/10.1090/S0025-5718-1981-0616377-4\"\u003eAddendum and Corrigendum to \"High-Precision Values of the Gamma Function and of Some Related Coefficients\"'\u003c/a\u003e, Mathematics of Computation, Vol. 37, No. 155 (1981), pp. 233-235.",
				"Arne Fransén and Staffan Wrigge, \u003ca href=\"https://doi.org/10.1090/S0025-5718-1980-0559204-5\"\u003eHigh-precision values of the gamma function and of some related coefficients\u003c/a\u003e, Mathematics of Computation, Vol. 34, No. 150 (1980), pp. 553-566.",
				"F. Johansson, \u003ca href=\"http://www.fredrikj.net/math/fransen_robinson.txt\"\u003eValue to 1000 decimal places\u003c/a\u003e.",
				"Simon Plouffe, \u003ca href=\"http://www.plouffe.fr/simon/constants/fransen.txt\"\u003eFransen-Robibson constant\u003c/a\u003e",
				"Simon Plouffe, \u003ca href=\"http://www.worldwideschool.org/library/books/sci/math/MiscellaneousMathematicalConstants/chap34.html\"\u003eFransen-Robinson constant\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Fransen-RobinsonConstant.html\"\u003eFransén-Robinson Constant\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Frans%C3%A9n%E2%80%93Robinson_constant\"\u003eFransén-Robinson constant\u003c/a\u003e."
			],
			"formula": [
				"Equals e + Integral_{x=0..oo} exp(-x)/(Pi^2 + log(x)^2) dx. - _Amiram Eldar_, Aug 13 2020"
			],
			"example": [
				"2.807770242028519365221501186557772932308085920930198291220054809597100..."
			],
			"mathematica": [
				"RealDigits[ NIntegrate[ 1 / Gamma[ x ], {x, 0, Infinity}, AccuracyGoal -\u003e 72, WorkingPrecision -\u003e 90 ] ][ [ 1 ] ]"
			],
			"program": [
				"(PARI) intnum(x=0,[[1],1],1/gamma(x)) \\\\ Bill Allombert, May 18 2015"
			],
			"xref": [
				"Cf. A046943 (continued fraction)."
			],
			"keyword": "nonn,cons",
			"offset": "1,1",
			"author": "_Robert G. Wilson v_, Jan 05 2001",
			"ext": [
				"More terms from Philip Sung (philip_sung(AT)hotmail.com), Jan 22 2002"
			],
			"references": 7,
			"revision": 30,
			"time": "2020-08-13T08:56:15-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}