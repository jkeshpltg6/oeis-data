{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A272977",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 272977,
			"data": "2,4,1,3,8,1,1,4,3,11,3,1,9,5,3,3,10,7,6,9,3,6,1,1,11,15,4,2,13,2,2,4,4,16,5,4,13,5,2,10,12,6,5,1,12,6,1,3,7,19,2,10,10,6,3,1,2,12,7,3,15,7,4,3,16,8,6,9,5,6,1,7,12,19,3,3,7,2,4,9",
			"name": "Number of ordered ways to write n as x^2 + y^2 + z^2 + w^2 with 3*x^2*y + z^2*w a square, where w is a nonzero integer and x,y,z are nonnegative integers with x \u003e= z.",
			"comment": [
				"Conjecture: (i) a(n) \u003e 0 for all n \u003e 0, and a(n) = 1 only for n = 3, 7, 23, 47, 71, 147, 199, 263, 439, 16^k*m (k = 0,1,2,... and m = 6, 12, 24, 44, 56, 140, 156, 174, 204, 284, 4652).",
				"(ii) For each ordered pair (a,b) = (7,1), (8,1), (9,2), any positive integer can be written as x^2 + y^2 + z^2 + w^2 with a*x^2*y + b*z^2*w a square, where x,y,z are nonnegative integers and w is a nonzero integer.",
				"Compare this conjecture with the one in A270073.",
				"See arXiv:1604.06723 for more refinements of Lagrange's four-square theorem."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A272977/b272977.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1604.06723\"\u003eRefining Lagrange's four-square theorem\u003c/a\u003e, arXiv:1604.06723 [math.GM], 2016.",
				"Zhi-Wei Sun, \u003ca href=\"http://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;852b9c4a.1604\"\u003eRefine Lagrange's four-square theorem\u003c/a\u003e, a message to Number Theory List, April 26, 2016."
			],
			"example": [
				"a(1) = 2 since 1 = 1^2 + 0^2 + 0^2 + 1^2 with 1 \u003e 0 and 3*1^2*0 + 0^2*1 = 0^2, and also 1 = 1^2 + 0^2 + 0^2 + (-1)^2 with 1 \u003e 0 and 3*1^2*0 + 0^2*(-1) = 0^2.",
				"a(3) = 1 since 3 = 1^2 + 0^2 + 1^2 + 1^2 with 1 = 1 and 3*1^2*0 + 1^2*1 = 1^2.",
				"a(6) = 1 since 6 = 2^2 + 0^2 + 1^2 + 1^2 with 2 \u003e 1 and 3*2^2*0 + 1^2*1 = 1^2.",
				"a(7) = 1 since 7 = 1^2 + 1^2 + 1^2 + (-2)^2 with 1 = 1 and 3*1^2*1 + 1^2*(-2) = 1^2.",
				"a(12) = 1 since 12 = 1^2 + 1^2 + 1^2 + (-3)^2 with 1 = 1 and 3*1^2*1 + 1*(-3) = 0^2.",
				"a(23) = 1 since 23 = 3^2 + 1^2 + 3^2 + (-2)^2 with 3 = 3 and 3*3^2*1 + 3^2*(-2) = 3^2.",
				"a(24) = 1 since 24 = 2^2 + 0^2 + 2^2 + 4^2 with 2 = 2 and 3*2^2*0 + 2^2*4 = 4^2.",
				"a(44) = 1 since 44 = 3^2 + 5^2 + 3^2 + 1^2 with 3 = 3 and 3*3^2*5 + 3^2*1 = 12^2.",
				"a(47) = 1 since 47 = 3^2 + 2^2 + 3^2 + (-5)^2 with 3 = 3 and 3*3^2*2 + 3^2*(-5) = 3^2.",
				"a(56) = 1 since 56 = 6^2 + 0^2 + 2^2 + 4^2 with 6 \u003e 2 and 3*6^2*0 + 2^2*4 = 4^2.",
				"a(71) = 1 since 71 = 5^2 + 6^2 + 3^2 + (-1)^2 with 5 \u003e 3 and 3*5^2*6 + 3^2*(-1) = 21^2.",
				"a(140) = 1 since 140 = 5^2 + 3^2 + 5^2 + (-9)^2 with 5 = 5 and 3*5^2*3 + 5^2*(-9) = 0^2.",
				"a(147) = 1 since 147 = 11^2 + 0^2 + 5^2 + 1^2 with 11 \u003e 5 and 3*11^2*0 + 5^2*1 = 5^2.",
				"a(156) = 1 since 156 = 7^2 + 3^2 + 7^2 + 7^2 with 7 = 7 and 3*7^2*3 + 7^2*7 = 28^2.",
				"a(174) = 1 since 174 = 13^2 + 0^2 + 2^2 + 1^2 with 13 \u003e 2 and 3*13^2*0 + 2^2*1 = 2^2.",
				"a(199) = 1 since 199 = 9^2 + 1^2 + 9^2 + 6^2 with 9 = 9 and 3*9^2*1 + 9^2*6 = 27^2.",
				"a(204) = 1 since 204 = 1^2 + 9^2 + 1^2 + (-11)^2 with 1 = 1 and 3*1^2*9 + 1^2*(-11) = 4^2.",
				"a(263) = 1 since 263 = 3^2 + 14^2 + 3^2 + 7^2 with 3 = 3 and",
				"3*3^2*14 + 3^2*7 = 21^2.",
				"a(284) = 1 since 284 = 13^2 + 3^2 + 5^2 + (-9)^2 with 13 \u003e 5 and 3*13^2*3 + 5^2*(-9) = 36^2.",
				"a(439) = 1 since 439 = 13^2 + 5^2 + 7^2 + (-14)^2 with 13 \u003e 7 and 3*13^2*5 + 7^2*(-14) = 43^2.",
				"a(4652) = 1 since 4652 = 11^2 + 21^2 + 11^2 + (-63)^2 with 11 = 11 and 3*11^2*21 + 11^2*(-63) = 0^2."
			],
			"mathematica": [
				"SQ[n_]:=SQ[n]=IntegerQ[Sqrt[n]]",
				"Do[r=0; Do[If[SQ[n-x^2-y^2-z^2]\u0026\u0026SQ[3x^2*y+z^2*(-1)^k*Sqrt[n-x^2-y^2-z^2]],r=r+1], {z,0,Sqrt[(n-1)/2]},{x,z,Sqrt[n-1-z^2]},{y,0,Sqrt[n-1-x^2-z^2]},{k,0,1}];Print[n, \" \",r]; Continue, {n,1,80}]"
			],
			"xref": [
				"Cf. A000118, A000290, A260625, A261876, A262357, A267121, A268197, A268507, A269400, A270073, A271510, A271513, A271518, A271608, A271665, A271714, A271721, A271724, A271775, A271778, A271824, A272084, A272332, A272351, A272620, A272888."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Zhi-Wei Sun_, May 11 2016",
			"references": 17,
			"revision": 9,
			"time": "2016-05-12T02:55:59-04:00",
			"created": "2016-05-12T02:55:59-04:00"
		}
	]
}