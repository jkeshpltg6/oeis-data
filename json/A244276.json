{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A244276",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 244276,
			"data": "1,-8,25,-40,48,-80,121,-120,144,-200,192,-248,337,-280,336,-440,384,-480,528,-480,673,-720,624,-720,816,-760,864,-1080,864,-1000,1321,-1008,1200,-1360,1152,-1440,1536,-1400,1488,-1720,1536,-1760,2185,-1560,1872",
			"name": "Expansion of q^(-1/4) * eta(q)^8 * eta(q^4)^2 / eta(q^2)^5 in powers of q.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A244276/b244276.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of phi(-x)^4 * psi(x^2) = phi(-x)^3 * psi(-x)^2 = f(-x)^6 / phi(x) = psi(-x)^8 / psi(x^2)^3 in powers of x where phi(), psi(), f() are Ramanujan theta functions.",
				"Euler transform of period 4 sequence [ -8, -3, -8, -5, ...].",
				"G.f.: Product_{k\u003e0} (1 - x^k)^5 * (1 + x^(2*k))^2 / (1 + x^k)^3.",
				"Convolution inverse of A134415."
			],
			"example": [
				"G.f. = 1 - 8*x + 25*x^2 - 40*x^3 + 48*x^4 - 80*x^5 + 121*x^6 - 120*x^7 + ...",
				"G.f. = q - 8*q^5 + 25*q^9 - 40*q^13 + 48*q^17 - 80*q^21 + 121*q^25 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[QPochhammer[ x]^6/EllipticTheta[ 3, 0, x], {x, 0, n}];",
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 4, 0, x]^4 EllipticTheta[ 2, 0, x]/(2 x^(1/4)), {x, 0, n}];",
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 4, 0, x]^3 EllipticTheta[ 2, Pi/4, x^(1/2)]^2/(2 x^(1/4)), {x, 0, n}];",
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 2, Pi/4, x^(1/2)]^8 / (2 x^(1/4) EllipticTheta[ 2, 0, x]^3 ), {x, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = local(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A)^8 * eta(x^ 4 + A)^2 / eta(x^2 + A)^5, n))};",
				"(MAGMA) A := Basis( ModularForms( Gamma0(16), 5/2), 180); A[2] - 8*A[6];"
			],
			"xref": [
				"Cf. A134415."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Sep 01 2014",
			"references": 3,
			"revision": 82,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2014-09-01T16:51:03-04:00"
		}
	]
}