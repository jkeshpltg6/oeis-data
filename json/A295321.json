{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A295321",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 295321,
			"data": "0,0,2,0,4,8,18,12,26,40,64,52,82,112,156,136,188,240,310,280,360,440,542,500,614,728,868,812,966,1120,1304,1232,1432,1632,1866,1776,2028,2280,2570,2460,2770,3080,3432,3300,3674,4048,4468,4312,4756,5200,5694",
			"name": "Sum of the products of the smaller and larger parts of the partitions of n into two distinct parts with the larger part even.",
			"comment": [
				"Sum of the areas of the distinct rectangles with even length and integer width such that L + W = n, W \u003c L. For example, a(12) = 52; the rectangles are 2 X 10 and 4 X 8 (6 X 6 is not included since we have W \u003c L), so 2*10 + 4*8 = 52.",
				"Sum of the ordinates from the ordered pairs (n-k,n*k-k^2) corresponding to integer points along the right side of the parabola b_k = n*k-k^2 where n-k is an even integer such that 0 \u003c k \u003c floor(n/2).",
				"Sum of the areas of the trapezoids with bases n and n-2i and height i for even values of n-i where i is in 0 \u003c= i \u003c= floor((n-1)/2). For a(n) the area formula for a trapezoid becomes (n+n-2i)*i/2 = (2n-2i)*i/2 = i*(n-i). For n=10, n-i is even when i=0,2,4 so a(10) = 0*(10-0) + 2*(10-2) + 4*(10-4) = 0 + 16 + 24 = 40. - _Wesley Ivan Hurt_, Mar 22 2018",
				"Sum of the areas of the symmetric L-shaped polygons with long side n/2 and width i such that n-i is even for i in 0 \u003c= i \u003c= floor((n-1)/2). The area of each polygon is given by i^2+2i(n/2-i) = i^2+ni-2i^2 = i(n-i). For n=9, 9-i is even for i=1,3 so 1(9-1) + 3(9-3) = 8 + 18 = 26. - _Wesley Ivan Hurt_, Mar 26 2018"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A295321/b295321.txt\"\u003eTable of n, a(n) for n = 1..5000\u003c/a\u003e",
				"\u003ca href=\"/index/Par#part\"\u003eIndex entries for sequences related to partitions\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Sum_{i=1..floor((n-1)/2)} i * (n-i) * ((n-i+1) mod 2).",
				"Conjectures from _Colin Barker_, Nov 20 2017: (Start)",
				"G.f.: 2*x^3*(1 - x + 2*x^2 + 2*x^3 + 2*x^4 + x^6 + x^7) / ((1 - x)^4*(1 + x)^3*(1 + x^2)^3).",
				"a(n) = a(n-1) + 3*a(n-4) - 3*a(n-5) - 3*a(n-8) + 3*a(n-9) + a(n-12) - a(n-13) for n\u003e13.",
				"(End)"
			],
			"example": [
				"a(16) = 136; the partitions of 16 into two distinct parts are (15,1), (14,2), (13,3), (12,4), (11,5), (10,6), (9,7). There are 3 partitions with the larger part even, and the sum of the products of the smaller and larger parts is then 14*2 + 12*4 + 10*6 = 136."
			],
			"maple": [
				"A295321:=n-\u003eadd(i*(n-i)*((n-i+1) mod 2), i=1..floor((n-1)/2)): seq(A295321(n), n=1..100);"
			],
			"mathematica": [
				"Table[Sum[i (n - i) Mod[n - i + 1, 2], {i, Floor[(n - 1)/2]}], {n, 80}]"
			],
			"program": [
				"(PARI) a(n) = sum(i=1, (n-1)\\2, i*(n-i)*((n-i+1) % 2)); \\\\ _Michel Marcus_, Mar 26 2018"
			],
			"xref": [
				"Cf. A295320."
			],
			"keyword": "nonn,easy",
			"offset": "1,3",
			"author": "_Wesley Ivan Hurt_, Nov 19 2017",
			"references": 2,
			"revision": 31,
			"time": "2018-04-16T19:01:09-04:00",
			"created": "2017-12-07T03:49:44-05:00"
		}
	]
}