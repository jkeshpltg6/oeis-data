{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A157297",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 157297,
			"data": "185,233,317,793,1165,1717,4573,6757,9985,26645,39377,58193,155297,229505,339173,905137,1337653,1976845,5275525,7796413,11521897,30748013,45440825,67154537,179212553,264848537,391405325,1044527305,1543650397",
			"name": "Positive numbers y such that y^2 is of the form x^2+(x+233)^2 with integer x.",
			"comment": [
				"(-57, a(1)) and (A129625(n), a(n+1)) are solutions (x, y) to the Diophantine equation x^2+(x+233)^2 = y^2.",
				"lim_{n -\u003e infinity} a(n)/a(n-3) = 3+2*sqrt(2).",
				"lim_{n -\u003e infinity} a(n)/a(n-1) = (251+66*sqrt(2))/233 for n mod 3 = {0, 2}.",
				"lim_{n -\u003e infinity} a(n)/a(n-1) = (82611+44030*sqrt(2))/233^2 for n mod 3 = 1."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A157297/b157297.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,0,6,0,0,-1)."
			],
			"formula": [
				"a(n) = 6*a(n-3) - a(n-6) for n \u003e 6; a(1)=185, a(2)=233, a(3)=317, a(4)=793, a(5)=1165, a(6)=1717.",
				"G.f.: (1-x)*(185 +418*x +735*x^2 +418*x^3 +185*x^4)/(1-6*x^3+x^6).",
				"a(3*k-1) = 233*A001653(k) for k \u003e= 1."
			],
			"example": [
				"(-57, a(1)) = (-57, 185) is a solution: (-57)^2+(-57+233)^2 = 3249+30976 = 34225 = 185^2.",
				"(A129625(1), a(2)) = (0, 233) is a solution: 0^2+(0+233)^2 = 54289 = 233^2.",
				"(A129625(3), a(4)) = (432, 793) is a solution: 432^2+(432+233)^2 = 186624+442225 = 628849 = 793^2."
			],
			"mathematica": [
				"LinearRecurrence[{0,0,6,0,0,-1}, {185,233,317,793,1165,1717}, 50] (* _G. C. Greubel_, Mar 29 2018 *)"
			],
			"program": [
				"(PARI) {forstep(n=-60, 1100000000, [3,1], if(issquare(2*n^2+466*n+54289, \u0026k),print1(k, \",\")))};",
				"(MAGMA) I:=[185,233,317,793,1165,1717]; [n le 6 select I[n] else 6*Self(n-3) - Self(n-6): n in [1..30]]; // _G. C. Greubel_, Mar 29 2018"
			],
			"xref": [
				"Cf. A129625, A001653, A156035 (decimal expansion of 3+2*sqrt(2)), A157298 (decimal expansion of (251+66*sqrt(2))/233), A157299 (decimal expansion of (82611+44030*sqrt(2))/233^2)."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_Klaus Brockhaus_, Apr 11 2009",
			"references": 4,
			"revision": 9,
			"time": "2018-03-30T08:13:21-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}