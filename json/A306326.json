{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A306326",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 306326,
			"data": "1,1,1,1,2,1,1,4,4,1,1,8,10,8,1,1,16,22,22,16,1,1,32,46,50,46,32,1,1,64,94,106,106,94,64,1,1,128,190,218,226,218,190,128,1,1,256,382,442,466,466,442,382,256,1,1,512,766,890,946,962,946,890,766,512,1",
			"name": "The q-analogs T(q; n,k) of the rascal-triangle, here q = 2.",
			"comment": [
				"The formulas are given for the general case depending on some fixed integer q. The terms are valid if q = 2. For the special case q = 1 see A077028, for q = 3 see A306344. For q \u003c 1 the terms might be negative."
			],
			"formula": [
				"T(q; n,k) = 1 + ((q^k-1)/(q-1))*(q^(n-k)-1)/(q-1)) for 0 \u003c= k \u003c= n.",
				"T(q; n,k) = T(q; n,n-k) for 0 \u003c= k \u003c= n.",
				"T(q; n,0) = T(q; n,n) = 1 for n \u003e= 0.",
				"T(q; n,1) = 1 + (q^(n-1)-1)/(q-1) for n \u003e 0.",
				"T(q; i,j) = 0 if i \u003c j or j \u003c 0.",
				"The T(q; n,k) satisfy several recurrence equations:",
				"  (1) T(q; n,k) = q*T(q; n-1,k) + (q^k-1)/(q-1)-(q-1) for 0 \u003c= k \u003c n;",
				"  (2) T(q; n,k) = (T(q; n-1,k)*T(q; n-1,k-1) + q^(n-2))/T(q; n-2,k-1),",
				"  (3) T(q; n,k) = T(q; n,k-1) + T(q; n-1,k) + q^(n-k-1) - T(q; n-1,k-1),",
				"  (4) T(q; n,k) = T(q; n,k-1) + q*T(q; n-2,k-1) - q*T(q; n-2,k-2) for 0 \u003c k \u003c n;",
				"  (5) T(q; n,k) = T(q; n,k-2) + T(q; n-1,k) + (1+q)*q^(n-k-1) - T(q; n-1,k-2)",
				"  for 1 \u003c k \u003c n  with initial values given above.",
				"G.f. of column k \u003e= 0: Sum_{n\u003e=0} T(q; n+k,k)*t^n = (1+((q^k-1)/(q-1)-q)*t) / ((1-t)*(1-q*t)). Take account of lim_{q-\u003e1} (q^k-1)/(q-1) = k.",
				"G.f.: Sum_{n\u003e=0, k=0..n} T(q; n,k)*x^k*t^n = (1-q*t-q*x*t+(1+q^2)*x*t^2) / ((1-t)*(1-q*t)*(1-x*t)*(1-q*x*t)).",
				"The row polynomials p(q; n,x) = Sum_{k=0..n} T(q; n,k)*x^k satisfy the recurrence equation p(q; n,x) = q*p(q; n-1,x) + x^n + Sum_{k=0..n-1} ((q^k-1)/(q-1)-(q-1))*x^k for n \u003e 0 with initial value p(q; 0,x) = 1."
			],
			"example": [
				"If q = 2 the triangle T(2; n,k) starts:",
				"n\\k:  0     1     2     3     4     5     6     7     8     9",
				"=============================================================",
				"  0:  1",
				"  1:  1     1",
				"  2:  1     2     1",
				"  3:  1     4     4     1",
				"  4:  1     8    10     8     1",
				"  5:  1    16    22    22    16     1",
				"  6:  1    32    46    50    46    32     1",
				"  7:  1    64    94   106   106    94    64     1",
				"  8:  1   128   190   218   226   218   190   128     1",
				"  9:  1   256   382   442   466   466   442   382   256     1",
				"etc."
			],
			"xref": [
				"Cf. A077028, A306344."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Werner Schulte_, Feb 07 2019",
			"references": 1,
			"revision": 18,
			"time": "2019-02-11T13:28:46-05:00",
			"created": "2019-02-11T13:28:46-05:00"
		}
	]
}