{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A193514",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 193514,
			"data": "1,-4,4,2,-4,0,4,-8,4,2,0,0,2,-8,8,0,-4,0,4,-8,0,4,0,0,4,-4,8,2,-8,0,0,-8,4,0,0,0,2,-8,8,4,0,0,8,-8,0,0,0,0,2,-12,4,0,-8,0,4,0,8,4,0,0,0,-8,8,4,-4,0,0,-8,0,0,0,0,4,-8,8,2,-8,0,8,-8,0,2,0,0,4,0,8,0,0,0,0,-16,0,4,0,0,4,-8",
			"name": "Expansion of phi(-q)^2 * phi(-q^9) / phi(-q^3) in powers of q where phi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A193514/b193514.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of (-2 * a(q) +2 * a(q^2) +3 * a(q^3)) / 3 = b(q) * (b(q) + 2 * b(q^2)) / (3 * b(q^2)) in powers of q where a(), b() are cubic AGM functions.",
				"Expansion of eta(q)^4 * eta(q^6) * eta(q^9)^2 / (eta(q^2)^2 * eta(q^3)^2 * eta(q^18)) in powers of q.",
				"Euler transform of period 18 sequence [ -4, -2, -2, -2, -4, -1, -4, -2, -4, -2, -4, -1, -4, -2, -2, -2, -4, -2, ...].",
				"Moebius transform is period 18 sequence [ -4, 8, 6, -8, 4, -6, -4, 8, 0, -8, 4, 6, -4, 8, -6, -8, 4, 0, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (18 t)) = 432^(1/2) (t / i) g(t) where q = exp(2 Pi i t) and g() is g.f. for A193426.",
				"a(3*n) = A123330(n). a(3*n + 1) = -4 * A033687(n). a(6*n + 1) = -4 * A097195(n). a(6*n + 2) = 4 * A033687(n). a(6*n + 3) = 2 * A033762(n). a(6*n + 4) = 4 * A033687(n). a(8*n + 2) = 4 * A112604(n). a(8*n + 6) = 4 * A112605(n). a(6*n + 5) = 0. a(4*n) = a(n)."
			],
			"example": [
				"G.f. = 1 - 4*q + 4*q^2 + 2*q^3 - 4*q^4 + 4*q^6 - 8*q^7 + 4*q^8 + 2*q^9 + 2*q^12 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 4, 0, q]^2 EllipticTheta[ 4, 0, q^9] / EllipticTheta[ 4, 0, q^3], {q, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, n==0, 2 * if( n%3==1, -2, 1) * sumdiv( n, d, -(-1)^d * kronecker( -3, d)))};",
				"(PARI) {a(n) = local(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A)^4 * eta(x^6 + A) * eta(x^9 + A)^2 / (eta(x^2 + A)^2 * eta(x^3 + A)^2 * eta(x^18 + A)), n))};"
			],
			"xref": [
				"Cf. A033687, A033762, A097195, A112604, A112605."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Jul 29 2011",
			"references": 1,
			"revision": 13,
			"time": "2021-03-12T22:24:46-05:00",
			"created": "2011-07-29T23:51:23-04:00"
		}
	]
}