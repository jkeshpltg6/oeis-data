{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A213812",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 213812,
			"data": "1,3,4,2,2,3,1,1,2,7,24,4,4,7,47,80,9,1,23,2,46,15,24,21,24,1,1,76,8,21,16,14,6,2,150,16,8,16,3,156,36,232,2,13,10,788,40,25,2,4,123,12,44,16,8,207,226,462,92,6",
			"name": "a(n) = smallest m for which the n-th Carmichael number A002997(n) can be written as p^2*(m+1) - p*m.",
			"comment": [
				"The corresponding values of p are (we write the Carmichael number in brackets): 17(561), 17(1105), 19(1729), 29(2465), 31(2821), 41(6601), 67(8911), 73(10585), 73(15841), 61(29341), 41(41041), 97(46657), 103(52633), 89(62745), 37(63973), 31(75361), 101(101101), 241(115921), 73(126217), 233(162401), 61(172081), 109(188461), 101(252601), 113(278545), 109(294409), 397(314821), 409(334153), 67(340561), 211(399001), 137(410041), 163(449065), 181(488881), 271(512461), 421(530881), 61(552721), 197(656601), 271(658801), 199(670033), 433(748657), 73(825265), 151(838201), 61(852841), 577(997633), 271(1024651), 307(1033669), 37(1050985), 163(1082809), 211(1152271), 631(1193221), 541(1461241), 113(1569457), 353(1615681), 199(1773289), 331(1857241), 461(1909001), 101(2100901), 97(2113921), 73(2433601), 163(2455921), 599(2508013).",
				"Any Carmichael number C can be written as C = p^2*(n+1) - p*n, where p is any prime divisor of C (it can be seen that the smallest n is obtained for the biggest prime divisor).",
				"The formula C = p^2*(n+1) - p*n is equivalent to C = p^2*m - p*(m-1) = p^2*m - p*m + p, equivalent to p^2 - p divides C - p, which is a direct consequence of Korselt’s criterion.",
				"It can be shown from p - 1 divides C - 1 not that just p^2 - p divides C - p but even that p^2 - p divides C - p^k (if C \u003e p^k) or p^k - C (if p^k \u003e C) which leads to the generic formula for a Carmichael number: C = p^k + n*p^2 - n*p (if C \u003e p^k) or C = p^k - n*p^2 + n*p (if p^k \u003e C) for any p prime divisor of C and any k natural number.",
				"The formulas generated giving values of k seems to be very useful in the study of Fermat pseudoprimes; also, the composite numbers C for which the equation C = p^k - n*p^2 + n*p gives, over the integers, as solutions, all their prime divisors, for a certain k, deserve further study."
			],
			"link": [
				"Charles R Greathouse IV, \u003ca href=\"/A213812/b213812.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CarmichaelNumber.html\"\u003eCarmichael Number\u003c/a\u003e"
			],
			"program": [
				"(PARI) Car=[561, 1105, 1729, 2465, 2821, 6601, 8911, 10585, 15841, 29341, 41041, 46657, 52633, 62745, 63973, 75361, 101101, 115921, 126217]; \\\\ use more terms of A002997 as desired",
				"apply(C-\u003emy(f=factor(C)[,1],p=f[#f],p2=p^2); (C-p2)/(p2-p), Car) \\\\ _Charles R Greathouse IV_, Jul 05 2017"
			],
			"xref": [
				"Cf. A002997."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Marius Coman_, Jun 20 2012",
			"references": 5,
			"revision": 7,
			"time": "2017-07-05T10:14:12-04:00",
			"created": "2012-06-21T03:11:08-04:00"
		}
	]
}