{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A153277",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 153277,
			"data": "1,1,2,1,3,5,1,4,12,15,1,5,22,60,52,1,6,35,154,358,203,1,7,51,315,1304,2471,877,1,8,70,561,3455,12915,19302,4140,1,9,92,910,7556,44590,146115,167894,21147,1,10,117,1380,14532,120196,660665,1855570,1606137,115975",
			"name": "Array read by antidiagonals of higher order Bell numbers.",
			"comment": [
				"Mezo's abstract: The powers of matrices with Stirling number-coefficients are investigated. It is revealed that the elements of these matrices have a number of properties of the ordinary Stirling numbers. Moreover, \"higher order\" Bell, Fubini and Eulerian numbers can be defined. Hence we give a new interpretation for E. T. Bell's iterated exponential integers. In addition, it is worth to note that these numbers appear in combinatorial physics, in the problem of the normal ordering of quantum field theoretical operators."
			],
			"link": [
				"E. T. Bell, \u003ca href=\"http://www.jstor.org/stable/1968633\"\u003eThe iterated exponential integers\u003c/a\u003e, Ann. Math. 39(3) (1938), 539-557.",
				"J. Ginsburg, \u003ca href=\"http://www.jstor.org/stable/4145040\"\u003eIterated exponentials\u003c/a\u003e, Scripta Math., 11 (1945), 340-353.",
				"Istvan Mezo, \u003ca href=\"http://arxiv.org/abs/0812.4047\"\u003eOn powers of Stirling matrices\u003c/a\u003e, arXiv:0812.4047.",
				"K. A. Penson, P. Blasiak, G. Duchamp, A. Horzela, A. I. Solomon, \u003ca href=\"http://dx.doi.org/10.1088/0305-4470/37/10/011\"\u003eHierarchical Dobinski-type relations via substitution and the moment problem\u003c/a\u003e, J.Phys. A: Math.Gen. 37 3475-3487 (2004)."
			],
			"example": [
				"The table on p.4 of Mezo begins:",
				"===========================================================",
				"B_p,n|n=1|n=2|n=3.|.n=4.|..n=5.|....n=6.|.....n=7.|comment",
				"===========================================================",
				"p=1..|.1.|.2.|..5.|..15.|...52.|....203.|.....877.|.A000110",
				"p=2..|.1.|.3.|.12.|..60.|..358.|...2471.|...19302.|.A000258",
				"p=3..|.1.|.4.|.22.|.154.|.1304.|..12915.|..146115.|.A000307",
				"p=4..|.1.|.5.|.35.|.315.|.3455.|..44590.|..660665.|.A000357",
				"p=5..|.1.|.6.|.51.|.561.|.7556.|.120196.|.2201856.|.A000405",
				"==========================================================="
			],
			"maple": [
				"g:= proc(a) local b; b:=proc(n) option remember; if n=0 then 1 else (n-1)! *add (a(k)* b(n-k)/ (k-1)!/ (n-k)!, k=1..n) fi end end: B:= (p,n)-\u003e (g@@p)(1)(n):",
				"seq(seq(B(d-n, n), n=1..d-1), d=1..12); # _Alois P. Heinz_, Feb 02 2009"
			],
			"mathematica": [
				"g[k_] := g[k] = Nest[Function[x, E^x-1], x, k]; a[n_, k_] := SeriesCoefficient[ 1+g[k+1], {x, 0, n}]*n!; Table[a[n, k-n+1], {k, 1, 12}, {n, 1, k}] // Flatten (* _Jean-François Alcover_, Jan 28 2015 *)"
			],
			"xref": [
				"Cf. A000110, A000258, A000307, A000357, A000405, A111672.",
				"From _Alois P. Heinz_, Feb 02 2009: (Start)",
				"Truncated and reflected version of A144150.",
				"Cf. A001669, A081624, A081629, A081697, A081740, A000326, A005945. (End)"
			],
			"keyword": "easy,nonn,tabl",
			"offset": "1,3",
			"author": "_Jonathan Vos Post_, Dec 22 2008",
			"ext": [
				"More terms from _Alois P. Heinz_, Feb 02 2009"
			],
			"references": 3,
			"revision": 16,
			"time": "2015-01-28T04:54:11-05:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}