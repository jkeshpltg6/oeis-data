{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A308566",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 308566,
			"data": "1,1,1,2,3,2,4,3,1,3,4,2,2,3,2,4,5,2,3,5,4,6,4,2,6,8,4,4,6,3,6,8,3,4,6,6,5,5,2,6,8,3,6,4,3,6,9,2,4,7,4,6,4,4,4,8,3,4,6,4,7,8,3,4,6,5,7,5,3,7,11,3,6,6,4,8,8,2,2,10,7,9,5,5,9,10,3,6,7,3,6,11,5,5,10,7,7,8,4,6",
			"name": "Number of ways to write n as w^2 + x*(x+1) + 4^y*5^z with w,x,y,z nonnegative integers.",
			"comment": [
				"Recall an observation of Euler: {w^2 + x*(x+1): w,x = 0,1,2,...} = {a*(a+1)/2 + b*(b+1)/2: a,b = 0,1,...}.",
				"Conjecture: a(n) \u003e 0 for all n \u003e 0. Equivalently, each n = 1,2,3,... can be written as a*(a+1)/2 + b*(b+1)/2 + 4^c*5^d with a,b,c,d nonnegative integers.",
				"See also A308584 for a similar conjecture.",
				"We have verified a(n) \u003e 0 for all n = 1..5*10^8.",
				"a(n) \u003e 0 for 0 \u003c n \u003c 10^10. - _Giovanni Resta_, Jun 08 2019"
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A308566/b308566.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://dx.doi.org/10.4064/aa127-2-1\"\u003eMixed sums of squares and triangular numbers\u003c/a\u003e, Acta Arith. 127(2007), 103-113."
			],
			"example": [
				"a(1) = 1 with 1 = 0^2 + 0*1 + 4^0*5^0.",
				"a(2) = 1 with 2 = 1^2 + 0*1 + 4^0*5^0.",
				"a(3) = 1 with 3 = 0^2 + 1*2 + 4^0*5^0.",
				"a(9) = 1 with 9 = 2^2 + 0*1 + 4^0*5^1.",
				"a(303) = 1 with 303 = 16^2 + 6*7 + 4^0*5^1.",
				"a(585) = 1 with 585 = 5^2 + 15*16 + 4^3*5^1.",
				"a(37863) = 2 with 37863 = 166^2 + 101*102 + 4^0*5^1 = 179^2 + 26*27 + 4^5*5^1."
			],
			"mathematica": [
				"SQ[n_]:=SQ[n]=IntegerQ[Sqrt[n]];",
				"tab={};Do[r=0;Do[If[SQ[n-4^k*5^m-x(x+1)],r=r+1],{k,0,Log[4,n]},{m,0,Log[5,n/4^k]},{x,0,(Sqrt[4(n-4^k*5^m)+1]-1)/2}];tab=Append[tab,r],{n,1,100}];Print[tab]"
			],
			"xref": [
				"Cf. A000217, A000290, A000302, A000351, A002378, A303656, A303637, A308411, A308547, A308584."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Zhi-Wei Sun_, Jun 07 2019",
			"references": 12,
			"revision": 21,
			"time": "2019-06-10T10:21:14-04:00",
			"created": "2019-06-10T10:21:14-04:00"
		}
	]
}