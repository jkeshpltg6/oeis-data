{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A103327",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 103327,
			"data": "1,3,1,5,10,1,7,35,21,1,9,84,126,36,1,11,165,462,330,55,1,13,286,1287,1716,715,78,1,15,455,3003,6435,5005,1365,105,1,17,680,6188,19448,24310,12376,2380,136,1,19,969,11628,50388,92378,75582,27132,3876,171,1",
			"name": "Triangle read by rows: T(n,k) = binomial(2n+1, 2k+1).",
			"comment": [
				"A subset of Pascal's triangle A007318.",
				"Elements have the same parity as those of Pascal's triangle.",
				"Matrix inverse is A104033. - _Paul D. Hanna_, Feb 28 2005",
				"Row reverse of A091042. - _Peter Bala_, Jul 29 2013",
				"Let E(y) = cosh(sqrt(y)) = 1 + 3*y/3! + 5*y^2/5! + 7*y^3/7! + .... Then this triangle is the generalized Riordan array (E(y), y) with respect to the sequence (2*n+1)! as defined in Wang and Wang. Cf. A086645. - _Peter Bala_, Aug 06 2013",
				"The row polynomial P(d, x) = Sum_{k=0..d} T(d, k)*x^k, multiplied by (2*d)!/d! = A001813(d), gives the numerator polynomial of the o.g.f. of the sequence of the diagonal d, for d \u003e= 0, of the Sheffer triangle Lah[4,3] given in A292219. - _Wolfdieter Lang_, Oct 12 2017"
			],
			"reference": [
				"A. T. Benjamin and J. J. Quinn, Proofs that really count: the art of combinatorial proof, M.A.A. 2003, id. 224."
			],
			"link": [
				"Indranil Ghosh, \u003ca href=\"/A103327/b103327.txt\"\u003eRows 0..120 of triangle, flattened\u003c/a\u003e",
				"W. Wang and T. Wang, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2007.12.037\"\u003eGeneralized Riordan array\u003c/a\u003e, Discrete Mathematics, Vol. 308, No. 24, 6466-6500."
			],
			"formula": [
				"G.f. for column k: Sum_{j=0..k+1} C(2*(k+1), 2*j)x^j)/(1-x)^(2*(k+1)). - _Paul Barry_, Feb 24 2005",
				"G.f.: A(x, y) = (1 + x*(1-y))/( (1 + x*(1-y))^2 - 4*x ). - _Paul D. Hanna_, Feb 28 2005",
				"Sum_{k=0..n} T(n, k)*A000364(n-k) = A002084(n). - _Philippe Deléham_, Aug 27 2005",
				"E.g.f.: 1/sqrt(x)*sinh(sqrt(x)*t)*cosh(t) = t + (3 + x)*t^3/3! + (5 + 10*x + x^2)*t^5/5! + .... - _Peter Bala_, Jul 29 2013",
				"T(n+2,k+2) = 2*T(n+1,k+2) + 2*T(n+1,k+1) - T(n,k+2) + 2*T(n,k+1) - T(n,k). - _Emanuele Munarini_, Jul 05 2017"
			],
			"example": [
				"The triangle T(n, k) begins:",
				"n\\k   0    1     2      3      4      5      6     7    8   9  10 ...",
				"0:    1",
				"1:    3    1",
				"2:    5   10     1",
				"3:    7   35    21      1",
				"4:    9   84   126     36      1",
				"5:   11  165   462    330     55      1",
				"6:   13  286  1287   1716    715     78      1",
				"7:   15  455  3003   6435   5005   1365    105     1",
				"8:   17  680  6188  19448  24310  12376   2380   136    1",
				"9:   19  969 11628  50388  92378  75582  27132  3876  171   1",
				"10:  21 1330 20349 116280 293930 352716 203490 54264 5985 210   1",
				"... reformatted and extended. - _Wolfdieter Lang_, Oct 12 2017",
				"From _Peter Bala_, Aug 06 2013: (Start)",
				"Viewed as the generalized Riordan array (cosh(sqrt(y)), y) with respect to the sequence (2*n+1)! the column generating functions begin",
				"1st col: cosh(sqrt(y)) = 1 + 3*y/3! + 5*y^2/5! + 7*y^3/7! + 9*y^4/9! + ....",
				"2nd col: 1/3!*y*cosh(sqrt(y)) = y/3! + 10*y^2/5! + 35*y^3/7! + 84*y^4/9! + ....",
				"3rd col: 1/5!*y^2*cosh(sqrt(y)) = y^2/5! + 21*y^3/7!! + 126*y^4/9! + 462*y^5/11! + .... (End)"
			],
			"mathematica": [
				"Flatten[Table[Binomial[2n+1,2k+1],{n,0,10},{k,0,n}]] (* _Harvey P. Dale_, Jun 19 2014 *)"
			],
			"program": [
				"(PARI) {T(n,k)=local(X=x+x*O(x^n),Y=y+y*O(y^k)); polcoeff(polcoeff((1+X*(1-Y))/((1+X*(1-Y))^2-4*X),n,x),k,y)} \\\\ _Paul D. Hanna_, Feb 28 2005",
				"(PARI) T(n,k) = binomial(2*n+1, 2*k+1);",
				"for(n=0, 12, for(k=0,n, print1(T(n,k), \", \"))) \\\\ _G. C. Greubel_, Aug 01 2019",
				"(Maxima) create_list(binomial(2*n+1,2*k+1),n,0,12,k,0,n); /* _Emanuele Munarini_, Mar 11 2011 */",
				"(MAGMA) [Binomial(2*n+1, 2*k+1): k in [0..n], n in [0..12]]; // _G. C. Greubel_, Aug 01 2019",
				"(Sage) [[binomial(2*n+1, 2*k+1) for k in (0..n)] for n in (0..12)] # _G. C. Greubel_, Aug 01 2019",
				"(GAP) Flat(List([0..12], n-\u003e List([0..n], k-\u003e Binomial(2*n+1, 2*k+1) ))); # _G. C. Greubel_, Aug 01 2019"
			],
			"xref": [
				"Reflected version of A091042. Cf. A086645, A103328.",
				"Cf. A104033, A086645, A292219."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "0,2",
			"author": "_Ralf Stephan_, Feb 06 2005",
			"references": 12,
			"revision": 50,
			"time": "2020-01-21T10:07:56-05:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}