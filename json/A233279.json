{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A233279",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 233279,
			"data": "0,1,2,3,4,5,7,6,8,9,11,10,15,14,12,13,16,17,19,18,23,22,20,21,31,30,28,29,24,25,27,26,32,33,35,34,39,38,36,37,47,46,44,45,40,41,43,42,63,62,60,61,56,57,59,58,48,49,51,50,55,54,52,53,64,65,67,66",
			"name": "Permutation of nonnegative integers: a(n) = A054429(A006068(n)).",
			"comment": [
				"This permutation transforms the enumeration system of positive irreducible fractions A007305/A047679 (Stern-Brocot) into the enumeration system A071766/A229742 (HCS), and the enumeration system A162909/A162910 (Bird) into A245325/A245326. - _Yosu Yurramendi_, Jun 09 2015"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A233279/b233279.txt\"\u003eTable of n, a(n) for n = 0..8191\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A054429(A006068(n)).",
				"a(n) = A006068(A063946(n)).",
				"a(n) = A154435(A054429(n)).",
				"a(n) = A180200(A258746(n)) = A117120(A180200(n)), n \u003e 0. - _Yosu Yurramendi_, Apr 10 2017"
			],
			"mathematica": [
				"Module[{nn = 6, s}, s = Flatten[Table[Range[2^(n + 1) - 1, 2^n, -1], {n, 0, nn}]]; Map[If[# == 0, 0, s[[#]]] \u0026, Table[Fold[BitXor, n, Quotient[n, 2^Range[BitLength[n] - 1]]], {n, 0, 2^nn}]]] (* _Michael De Vlieger_, Apr 06 2017, after _Harvey P. Dale_ at A054429 and _Jan Mangaldan_ at A006068 *)"
			],
			"program": [
				"(Scheme) (define (A233279 n) (A054429 (A006068 n)))",
				"(R)",
				"maxrow \u003c- 8 # by choice",
				"a \u003c- 1:3",
				"for(m in 0:maxrow) for(k in 0:(2^m-1)){",
				"a[2^(m+2)+            k] \u003c- a[2^(m+1)+    k] + 2^(m+1)",
				"a[2^(m+2)+        2^m+k] \u003c- a[2^(m+1)+2^m+k] + 2^(m+1)",
				"a[2^(m+2)+2^(m+1)+    k] \u003c- a[2^(m+1)+2^m+k] + 2^(m+2)",
				"a[2^(m+2)+2^(m+1)+2^m+k] \u003c- a[2^(m+1)+   +k] + 2^(m+2)",
				"}",
				"(a \u003c- c(0,a))",
				"# _Yosu Yurramendi_, Apr 05 2017",
				"(R)",
				"# Given n, compute a(n) by taking into account the binary representation of n",
				"maxblock \u003c- 7 # by choice",
				"a \u003c- 1",
				"for(n in 2:2^maxblock){",
				"  ones \u003c- which(as.integer(intToBits(n)) == 1)",
				"  nbit \u003c- as.integer(intToBits(n))[1:tail(ones, n = 1)]",
				"  anbit \u003c- nbit",
				"  for(k in 2^(0:floor(log2(length(nbit))))  )",
				"    anbit \u003c- bitwXor(anbit, c(anbit[-(1:k)], rep(0,k))) # ?bitwXor",
				"  anbit[0:(length(anbit) - 1)] \u003c- 1 - anbit[0:(length(anbit)-1)]",
				"  a \u003c- c(a, sum(anbit*2^(0:(length(anbit) - 1))))",
				"}",
				"(a \u003c- c(0,a))",
				"# _Yosu Yurramendi_, May 29 2021",
				"(Python)",
				"from sympy import floor",
				"def a006068(n):",
				"    s=1",
				"    while True:",
				"        ns=n\u003e\u003es",
				"        if ns==0: break",
				"        n=n^ns",
				"        s\u003c\u003c=1",
				"    return n",
				"def a054429(n): return 1 if n==1 else 2*a054429(floor(n/2)) + 1 - n%2",
				"def a(n): return 0 if n==0 else a054429(a006068(n)) # _Indranil Ghosh_, Jun 11 2017"
			],
			"xref": [
				"Inverse permutation: A233280.",
				"Cf. A006068, A054429, A063946, A154435."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Antti Karttunen_, Dec 18 2013",
			"references": 15,
			"revision": 38,
			"time": "2021-06-20T21:52:35-04:00",
			"created": "2014-01-02T02:38:00-05:00"
		}
	]
}