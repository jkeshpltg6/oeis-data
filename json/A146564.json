{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A146564",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 146564,
			"data": "1,4,4,7,4,13,4,10,7,13,4,22,4,13,13,13,4,22,4,22,13,13,4,31,7,13,10,22,4,40,4,16,13,13,13,37,4,13,13,31,4,40,4,22,22,13,4,40,7,22,13,22,4,31,13,31,13,13,4,67,4,13,22,19,13,40,4,22,13,40,4,52",
			"name": "a(n) is the number of solutions of the equation k*n/(k-n) = c. k,c integers.",
			"comment": [
				"In general, if n is a prime p then a(p)=4, and k is from {p-1, p+1, 2*p, p^2+p}.",
				"In general, if n is a squared prime p^2 then a(p^2)=7, and k is from {p^2-p, p^2-1, p^2+1, p^2+p, p^3-p^2, p^3+p^2, p^4+p^2}.",
				"The sequence counts solutions with k\u003e0 and any sign of c, or, alternatively, solutions with c\u003e0 and any sign of k. If solutions were constrained to k\u003e0 and c\u003e0, A048691 would result. - _R. J. Mathar_, Nov 21 2008"
			],
			"link": [
				"Nathaniel Johnston, \u003ca href=\"/A146564/b146564.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Umberto Cerruti, \u003ca href=\"/A146564/a146564.pdf\"\u003ePercorsi tra i numeri\u003c/a\u003e (in Italian), pages 2-4."
			],
			"formula": [
				"Conjecture: a(n) = A048691(n)+A063647(n). - _R. J. Mathar_, Nov 21 2008 (See Corollary 4 in Cerruti's paper.)",
				"a(n) = Sum(d|n, psi(2^omega(d)), where psi is A001615 and omega is A001221. - _Enrique Pérez Herrero_, Apr 13 2012"
			],
			"example": [
				"For n=7 we search the number of integer solutions of the equation 7*k/(k-7). This holds for k from {6,8,14,56}. Then a(7)=4. For n=10 we search the number of integer solutions of the equation 10*k/(k-10). This holds for k from {5,6,8,9,11,12,14,15,20,30,35,60,110}. Then a(10)=13."
			],
			"maple": [
				"A146564 := proc(n) local b,d,k,c ; b := numtheory[divisors](n^2) ; kbag := {} ; for d in b do k := d+n ; if k \u003e 0 then kbag := kbag union {k} ; fi ; k := -d+n ; if k \u003e 0 then kbag := kbag union {k} ; fi; end do; RETURN(nops(kbag)) ; end: for n from 1 to 800 do printf(\"%d,\",A146564(n)) ; od: # _R. J. Mathar_, Nov 21 2008"
			],
			"mathematica": [
				"psi[n_] := Module[{pp, ee}, {pp, ee} = Transpose[FactorInteger[n]]; If[Max[pp] == 3, n, Times@@(pp+1) * Times@@(pp^(ee-1))]];",
				"a[n_] := Sum[psi[2^PrimeNu[d]], {d, Divisors[n]}]-1;",
				"a /@ Range[72] (* _Jean-François Alcover_, Jan 18 2020 *)"
			],
			"program": [
				"(PARI)",
				"jordantot(n,k)=sumdiv(n,d,d^k*moebius(n/d));",
				"dedekindpsi(n)=jordantot(n,2)/eulerphi(n);",
				"A146564(n)=sumdiv(n, d, dedekindpsi(2^omega(d)));",
				"for(n=1, 200, print(n\" \"A146564(n))) \\\\ _Enrique Pérez Herrero_, Apr 14 2012",
				"(MAGMA) [# [k:k in {1..n^2+n} diff {n}| IsIntegral(k*n/(k-n))]:n in [1..75]]; // _Marius A. Burtea_, Oct 18 2019"
			],
			"xref": [
				"Cf. A191973."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Ctibor O. Zizka_, Nov 01 2008",
			"ext": [
				"Extended beyond a(11) by _R. J. Mathar_, Nov 21 2008"
			],
			"references": 8,
			"revision": 32,
			"time": "2020-01-18T11:09:05-05:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}