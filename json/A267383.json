{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A267383",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 267383,
			"data": "1,1,1,1,1,1,1,1,2,1,1,1,2,4,1,1,1,2,6,14,1,1,1,2,6,18,46,1,1,1,2,6,24,78,230,1,1,1,2,6,24,96,426,1066,1,1,1,2,6,24,120,504,2286,6902,1,1,1,2,6,24,120,600,3216,15402,41506,1",
			"name": "Number A(n,k) of acyclic orientations of the Turán graph T(n,k); square array A(n,k), n\u003e=0, k\u003e=1, read by antidiagonals.",
			"comment": [
				"An acyclic orientation is an assignment of a direction to each edge such that no cycle in the graph is consistently oriented. Stanley showed that the number of acyclic orientations of a graph G is equal to the absolute value of the chromatic polynomial X_G(q) evaluated at q=-1.",
				"Conjecture: In general, column k \u003e 1 is asymptotic to n! / ((k-1) * (1 - log(k/(k-1)))^((k-1)/2) * k^n * (log(k/(k-1)))^(n+1)). - _Vaclav Kotesovec_, Feb 18 2017"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A267383/b267383.txt\"\u003eAntidiagonals n = 0..140, flattened\u003c/a\u003e",
				"Richard P. Stanley, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(73)90108-8\"\u003eAcyclic Orientations of Graphs\u003c/a\u003e, Discrete Mathematics, 5 (1973), pages 171-178, doi:10.1016/0012-365X(73)90108-8",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Tur%C3%A1n_graph\"\u003eTurán graph\u003c/a\u003e"
			],
			"example": [
				"Square array A(n,k) begins:",
				"  1,    1,    1,    1,    1,    1,    1, ...",
				"  1,    1,    1,    1,    1,    1,    1, ...",
				"  1,    2,    2,    2,    2,    2,    2, ...",
				"  1,    4,    6,    6,    6,    6,    6, ...",
				"  1,   14,   18,   24,   24,   24,   24, ...",
				"  1,   46,   78,   96,  120,  120,  120, ...",
				"  1,  230,  426,  504,  600,  720,  720, ...",
				"  1, 1066, 2286, 3216, 3720, 4320, 5040, ..."
			],
			"maple": [
				"A:= proc(n, k) option remember; local b, l, q; q:=-1;",
				"       l:= [floor(n/k)$(k-irem(n,k)), ceil(n/k)$irem(n,k)];",
				"       b:= proc(n, j) option remember; `if`(j=1, (q-n)^l[1]*",
				"             mul(q-i, i=0..n-1), add(b(n+m, j-1)*",
				"             Stirling2(l[j], m), m=0..l[j]))",
				"           end; forget(b);",
				"       abs(b(0, k))",
				"    end:",
				"seq(seq(A(n, 1+d-n), n=0..d), d=0..14);"
			],
			"mathematica": [
				"A[n_, k_] := A[n, k] = Module[{ b, l, q}, q = -1; l = Join[Array[Floor[n/k] \u0026, k - Mod[n, k]], Array[ Ceiling[n/k] \u0026, Mod[n, k]]]; b[nn_, j_] := b[nn, j] = If[j == 1, (q - nn)^l[[1]]*Product[q - i, {i, 0, nn - 1}], Sum[b[nn + m, j - 1]*StirlingS2[l[[j]], m], {m, 0, l[[j]]}]]; Abs[b[0, k]]]; Table[Table[A[n, 1 + d - n], {n, 0, d}], {d, 0, 14}] // Flatten (* _Jean-François Alcover_, Feb 22 2016, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Columns k=1-10 give: A000012, A266695, A266858, A267384, A267385, A267386, A267387, A267388, A267389, A267390.",
				"Main diagonal gives A000142.",
				"A(2n,n) gives A033815.",
				"A(n,ceiling(n/2)) gives A161132."
			],
			"keyword": "nonn,tabl",
			"offset": "0,9",
			"author": "_Alois P. Heinz_, Jan 13 2016",
			"references": 12,
			"revision": 19,
			"time": "2018-10-18T16:24:07-04:00",
			"created": "2016-01-13T16:31:18-05:00"
		}
	]
}