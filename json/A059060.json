{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A059060",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 59060,
			"data": "1,0,0,0,0,1,1,0,16,0,36,0,16,0,1,346,1824,4536,7136,7947,6336,3936,1728,684,128,48,0,1,748521,3662976,8607744,12880512,13731616,11042688,6928704,3458432,1395126,453888,122016,25344,4824,512,96,0,1,3993445276",
			"name": "Card-matching numbers (Dinner-Diner matching numbers).",
			"comment": [
				"This is a triangle of card matching numbers. A deck has n kinds of cards, 4 of each kind. The deck is shuffled and dealt in to n hands with 4 cards each. A match occurs for every card in the j-th hand of kind j. Triangle T(n,k) is the number of ways of achieving exactly k matches (k=0..4n). The probability of exactly k matches is T(n,k)/((4n)!/(4!)^n).",
				"Rows have lengths 1,5,9,13,...",
				"Analogous to A008290 - _Zerinvary Lajos_, Jun 22 2005"
			],
			"reference": [
				"F. N. David and D. E. Barton, Combinatorial Chance, Hafner, NY, 1962, Ch. 7 and Ch. 12.",
				"J. Riordan, An Introduction to Combinatorial Analysis, Wiley, 1958, pp. 174-178.",
				"R. P. Stanley, Enumerative Combinatorics Volume I, Cambridge University Press, 1997, p. 71."
			],
			"link": [
				"F. F. Knudsen and I. Skau, \u003ca href=\"http://www.jstor.org/stable/2691467\"\u003eOn the Asymptotic Solution of a Card-Matching Problem\u003c/a\u003e, Mathematics Magazine 69 (1996), 190-197.",
				"Barbara H. Margolius, \u003ca href=\"http://academic.csuohio.edu/bmargolius/homepage/dinner/dinner/cardentry.htm\"\u003eDinner-Diner Matching Probabilities\u003c/a\u003e",
				"B. H. Margolius, \u003ca href=\"http://www.jstor.org/stable/3219303\"\u003eThe Dinner-Diner Matching Problem\u003c/a\u003e, Mathematics Magazine, 76 (2003), 107-118.",
				"S. G. Penrice, \u003ca href=\"http://www.jstor.org/stable/2324927\"\u003eDerangements, permanents and Christmas presents\u003c/a\u003e, The American Mathematical Monthly 98(1991), 617-620.",
				"\u003ca href=\"/index/Ca#cardmatch\"\u003eIndex entries for sequences related to card matching\u003c/a\u003e"
			],
			"formula": [
				"G.f.: sum(coeff(R(x, n, k), x, j)*(t-1)^j*(n*k-j)!, j=0..n*k) where n is the number of kinds of cards, k is the number of cards of each kind (here k is 4) and R(x, n, k) is the rook polynomial given by R(x, n, k)=(k!^2*sum(x^j/((k-j)!^2*j!))^n (see Stanley or Riordan). coeff(R(x, n, k), x, j) indicates the j-th coefficient on x of the rook polynomial."
			],
			"example": [
				"There are 16 ways of matching exactly 2 cards when there are 2 different kinds of cards, 4 of each so T(2,2)=16.",
				"From _Joerg Arndt_, Nov 08 2020: (Start)",
				"The first few rows are",
				"1",
				"0, 0, 0, 0, 1",
				"1, 0, 16, 0, 36, 0, 16, 0, 1",
				"346, 1824, 4536, 7136, 7947, 6336, 3936, 1728, 684, 128, 48, 0, 1",
				"748521, 3662976, 8607744, 12880512, 13731616, 11042688, 6928704, 3458432, 1395126, 453888, 122016, 25344, 4824, 512, 96, 0, 1",
				"3993445276, 18743463360, 42506546320, 61907282240, 64917874125, 52087325696, 33176621920, 17181584640, 7352761180, 2628808000, 790912656, 201062080, 43284010, 7873920, 1216000, 154496, 17640, 1280, 160, 0, 1 (End)"
			],
			"maple": [
				"p := (x,k)-\u003ek!^2*sum(x^j/((k-j)!^2*j!),j=0..k); R := (x,n,k)-\u003ep(x,k)^n; f := (t,n,k)-\u003esum(coeff(R(x,n,k),x,j)*(t-1)^j*(n*k-j)!,j=0..n*k);",
				"for n from 0 to 5 do seq(coeff(f(t,n,4),t,m)/4!^n,m=0..4*n); od;"
			],
			"mathematica": [
				"p[x_, k_] := k!^2*Sum[ x^j/((k-j)!^2*j!), {j, 0, k}]; r[x_, n_, k_] := p[x, k]^n; f[t_, n_, k_] := Sum[ Coefficient[r[x, n, k], x, j]*(t-1)^j*(n*k-j)!, {j, 0, n*k}]; Table[ Coefficient[f[t, n, 4], t, m]/4!^n, {n, 0, 5}, {m, 0, 4*n}] // Flatten (* _Jean-François Alcover_, Feb 22 2013, translated from Maple *)"
			],
			"xref": [
				"Cf. A008290, A059056-A059071."
			],
			"keyword": "nonn,tabf,nice",
			"offset": "0,9",
			"author": "Barbara Haas Margolius (margolius(AT)math.csuohio.edu)",
			"references": 4,
			"revision": 26,
			"time": "2020-11-08T12:32:49-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}