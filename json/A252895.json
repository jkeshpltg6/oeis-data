{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A252895",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 252895,
			"data": "1,2,3,5,6,7,10,11,13,14,15,16,17,19,21,22,23,26,29,30,31,32,33,34,35,37,38,39,41,42,43,46,47,48,51,53,55,57,58,59,61,62,65,66,67,69,70,71,73,74,77,78,79,80,81,82,83,85,86,87,89,91,93,94,95,96,97",
			"name": "Numbers with an odd number of square divisors.",
			"comment": [
				"Open lockers in the locker problem where the student numbers are the set of perfect squares.",
				"The locker problem is a classic mathematical problem. Imagine a row containing an infinite number of lockers numbered from one to infinity. Also imagine an infinite number of students numbered from one to infinity. All of the lockers begin closed. The first student opens every locker that is a multiple of one, which is every locker. The second student closes every locker that is a multiple of two, so all of the even-numbered lockers are closed. The third student opens or closes every locker that is a multiple of three. This process continues for all of the students.",
				"A variant on the locker problem is when not all student numbers are considered; in the case of this sequence, only the square-numbered students open and close lockers. The sequence here is a list of the open lockers after all of the students have gone.",
				"n is in the sequence if and only if it is the product of a squarefree number (A005117) and a fourth power (A000583). - _Robert Israel_, Apr 07 2015",
				"Let D be the multiset containing d0(k), the divisor counting function, for each divisor k of n. n is in the sequence if and only if D admits a partition into two parts A and B such that the sum of the elements of A is exactly one more or less than the sum of the elements of B. For example, if n = 80, we have D = {1, 2, 2, 3, 4, 4, 5, 6, 8, 10}, and A = {1, 2, 3, 4, 4, 8} and B = {2, 5, 6, 10}. The sum of A is 22, and the sum of B is 23. - _Griffin N. Macris_, Oct 10 2016",
				"From _Amiram Eldar_, Jul 07 2020: (Start)",
				"Numbers k such that the largest square dividing k (A008833) is a fourth power.",
				"The asymptotic density of this sequence is Pi^2/15 = A182448 = 0.657973... (Cesàro, 1885). (End)",
				"Closed under the binary operation A059897(.,.), forming a subgroup of the positive integers under A059897. - _Peter Munn_, Aug 01 2020"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A252895/b252895.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Ernest Cesàro, \u003ca href=\"https://doi.org/10.1007/BF02420801\"\u003eLe plus grand diviseur carré\u003c/a\u003e, Annali di Matematica Pura ed Applicata, Vol. 13, No. 1 (1885), pp. 251-268, \u003ca href=\"https://iris.univ-lille.fr/handle/1908/1932\"\u003eentire volume\u003c/a\u003e.",
				"K. A. P. Dagal, \u003ca href=\"http://arxiv.org/abs/1307.6455\"\u003eGeneralized Locker Problem\u003c/a\u003e, arXiv:1307.6455 [math.NT], 2013.",
				"B. Torrence and S. Wagon, \u003ca href=\"https://cms.math.ca/crux/v33/n4/page232-236.pdf\"\u003eThe Locker Problem\u003c/a\u003e, Crux Mathematicorum, 2007, 33(4), 232-236."
			],
			"example": [
				"The set of divisors of 6 is {1,2,3,6}, which contains only one perfect square: 1; therefore 6 is a term.",
				"The set of divisors of 16 is {1,2,4,8,16}, which contains three perfect squares: 1, 4, and 16; therefore 16 is a term.",
				"The set of divisors of 4 is {1,2,4}, which contains two perfect squares: 1 and 4; therefore 4 is not a term."
			],
			"maple": [
				"N:= 1000: # to get all terms \u003c= N",
				"S:= select(numtheory:-issqrfree, {$1..N}):",
				"map(s -\u003e seq(s*i^4, i = 1 .. floor((N/s)^(1/4))), S);",
				"# if using Maple 11 or earlier, uncomment the next line",
				"# sort(convert(%,list)); # _Robert Israel_, Apr 07 2015"
			],
			"mathematica": [
				"Position[Length@ Select[Divisors@ #, IntegerQ@ Sqrt@ # \u0026] \u0026 /@ Range@ 70, _Integer?OddQ] // Flatten (* _Michael De Vlieger_, Mar 23 2015 *)",
				"a[n_] := DivisorSigma[0, Total[EulerPhi/@Select[Sqrt[Divisors[n]], IntegerQ]]]; Flatten[Position[a/@Range@100,_?OddQ]] (* _Ivan N. Ianakiev_, Apr 07 2015 *)",
				"Select[Range@ 100, OddQ@ Length@ DeleteCases[Divisors@ #, k_ /; ! IntegerQ@ Sqrt@ k] \u0026] (* _Michael De Vlieger_, Oct 10 2016 *)"
			],
			"program": [
				"(C++)",
				"#include \u003ciostream\u003e",
				"using namespace std;",
				"int main()",
				"{",
				"const int one_k = 1000;",
				"//all numbers in sequence up to one_k are given",
				"int lockers [one_k] = {};",
				"int A = 0;",
				"while (A \u003c one_k) {",
				"  lockers [A] = A+1;",
				"  A = A + 1;",
				"}",
				"int B = 1;",
				"while ( ((B) * (B)) \u003c= one_k) {",
				"  int C = ((B) * (B));",
				"  int D = one_k/C;",
				"  int E = 1;",
				"  while (E \u003c= D) {",
				"    lockers [(C*E)-1] = -1 * lockers [(C*E)-1];",
				"    E = E + 1;",
				"  }",
				"  B = B + 1;",
				"}",
				"int F = 0;",
				"while (F \u003c one_k) {",
				"  if (lockers [F] \u003c 0) {",
				"    cout \u003c\u003c (-1 * lockers [F]) \u003c\u003c endl;",
				"  }",
				"F = F + 1;",
				"}",
				"return 0;",
				"} // _Walker Dewey Anderson_, Mar 22 2015",
				"(PARI) isok(n) = sumdiv(n, d, issquare(d)) % 2; \\\\ _Michel Marcus_, Mar 22 2015",
				"(Sage) [n for n in [1..200] if len([x for x in divisors(n) if is_square(x)])%2==1] # _Tom Edgar_, Mar 22 2015",
				"(Haskell)",
				"a252895 n = a252895_list !! (n-1)",
				"a252895_list = filter (odd . a046951) [1..]",
				"-- _Reinhard Zumkeller_, Apr 06 2015"
			],
			"xref": [
				"Cf. A000290, A000583, A005117, A008833, A046951, A182448, A252849.",
				"Positions of ones in A335324."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Walker Dewey Anderson_, Mar 22 2015",
			"references": 4,
			"revision": 81,
			"time": "2020-08-24T23:21:57-04:00",
			"created": "2015-04-06T16:55:05-04:00"
		}
	]
}