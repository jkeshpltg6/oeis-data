{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A305501",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 305501,
			"data": "0,1,1,1,1,2,1,1,1,1,1,2,1,2,2,1,1,2,1,1,2,1,1,2,1,2,1,2,1,2,1,1,1,1,2,2,1,2,2,1,1,3,1,1,2,1,1,2,1,1,2,2,1,2,1,2,1,2,1,2,1,1,2,1,2,1,1,1,2,2,1,2,1,2,2,2,2,3,1,1,1,1,1,3,1,2,2,1,1,2,2,1,1,1,2,2,1,2,1,1,1,2,1,2,3",
			"name": "Number of connected components of the integer partition y + 1 where y is the integer partition with Heinz number n.",
			"comment": [
				"The Heinz number of an integer partition (y_1,...,y_k) is prime(y_1)*...*prime(y_k).",
				"Given a finite set S of positive integers greater than one, let G(S) be the simple labeled graph with vertex set S and edges between any two vertices with a common divisor greater than 1. For example, G({6,14,15,35}) is a 4-cycle. A partition y is said to be connected if G(U(y + 1)) is a connected graph, where U(y + 1) is the set of distinct successors of the parts of y.",
				"This is intended to be a cleaner form of A305079, where the treatment of empty multisets is arbitrary."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A305501/b305501.txt\"\u003eTable of n, a(n) for n = 1..16384\u003c/a\u003e",
				"Antti Karttunen, \u003ca href=\"/A305501/a305501.txt\"\u003eData supplement: n, a(n) computed for n =  1..100000\u003c/a\u003e",
				"\u003ca href=\"/index/Pri#prime_indices\"\u003eIndex entries for sequences computed from indices in prime factorization\u003c/a\u003e",
				"\u003ca href=\"/index/He#Heinz\"\u003eIndex entries for sequences related to Heinz numbers\u003c/a\u003e"
			],
			"example": [
				"The \"prime index plus 1\" multiset of 7410 is {2,3,4,7,9}, with connected components {{2,4},{3,9},{7}}, so a(7410) = 3."
			],
			"mathematica": [
				"primeMS[n_]:=If[n===1,{},Flatten[Cases[FactorInteger[n],{p_,k_}:\u003eTable[PrimePi[p],{k}]]]];",
				"zsm[s_]:=With[{c=Select[Tuples[Range[Length[s]],2],And[Less@@#,GCD@@s[[#]]]\u003e1\u0026]},If[c=={},s,zsm[Union[Append[Delete[s,List/@c[[1]]],LCM@@s[[c[[1]]]]]]]]];",
				"Table[Length[zsm[primeMS[n]+1]],{n,100}]"
			],
			"program": [
				"(PARI)",
				"zero_first_elem_and_connected_elems(ys) = { my(cs = List([ys[1]]), i=1); ys[1] = 0; while(i\u003c=#cs, for(j=2,#ys,if(ys[j]\u0026\u0026(1!=gcd(cs[i],ys[j])), listput(cs,ys[j]); ys[j] = 0)); i++); (ys); };",
				"A305501(n) = { my(cs = apply(p -\u003e 1+primepi(p),factor(n)[,1]~), s=0); while(#cs, cs = select(c -\u003e c, zero_first_elem_and_connected_elems(cs)); s++); (s); }; \\\\ _Antti Karttunen_, Nov 09 2018"
			],
			"xref": [
				"Cf. A001221, A048143, A056239, A112798, A275024, A286518, A302242, A303837, A304118, A304714, A304716, A305078, A305079, A305504."
			],
			"keyword": "nonn",
			"offset": "1,6",
			"author": "_Gus Wiseman_, Jun 03 2018",
			"ext": [
				"More terms from _Antti Karttunen_, Nov 09 2018"
			],
			"references": 3,
			"revision": 25,
			"time": "2018-11-09T22:02:23-05:00",
			"created": "2018-06-04T09:22:13-04:00"
		}
	]
}