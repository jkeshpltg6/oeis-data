{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A338993",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 338993,
			"data": "1,2,2,3,6,2,4,12,4,2,5,20,8,4,2,6,30,12,6,4,2,7,42,18,10,6,4,2,8,56,24,14,8,6,4,2,9,72,32,18,12,8,6,4,2,10,90,40,24,16,10,8,6,4,2,11,110,50,30,20,14,10,8,6,4,2,12,132,60,36,24,18,12,10,8,6,4,2",
			"name": "Triangle read by rows: T(n,k) is the number of k-permutations of {1,...,n} that form a nontrivial arithmetic progression, 1 \u003c= k \u003c= n.",
			"comment": [
				"The step size ranges from 1 to floor((n-1)/(k-1)) and for each r, there are 2*(n-(k-1)*r) possible ways to form a progression.",
				"Proof can be found in Lemma 1 of Goh and Zhao (2020)."
			],
			"link": [
				"M. K. Goh and R. Y. Zhao, \u003ca href=\"https://arxiv.org/abs/2012.12339\"\u003eArithmetic subsequences in a random ordering of an additive set\u003c/a\u003e, arXiv:2012.12339 [math.CO], 2020."
			],
			"formula": [
				"T(n,k) = n, if k=1; Sum_{r=1..floor((n-1)/(k-1))} 2*(n-(k-1)*r), if 2 \u003c= k \u003c= n.",
				"T(n,k) = 2*n*f - (k-1)*(f^2 + f), where f = floor((n-1)/(k-1)), for 2 \u003c= k \u003c= n."
			],
			"example": [
				"Triangle T(n,k) begins:",
				"  n/k  1   2   3   4   5   6   7   8   9  10  11  12 ...",
				"   1   1",
				"   2   2   2",
				"   3   3   6   2",
				"   4   4  12   4   2",
				"   5   5  20   8   4   2",
				"   6   6  30  12   6   4   2",
				"   7   7  42  18  10   6   4   2",
				"   8   8  56  24  14   8   6   4   2",
				"   9   9  72  32  18  12   8   6   4   2",
				"  10  10  90  40  24  16  10   8   6   4   2",
				"  11  11 111  50  30  20  14  10   8   6   4   2",
				"  12  12 132  60  36  24  18  12  10   8   6   4   2",
				"  ...",
				"For n=4 and k=3 the T(4,3)=4 permutations are 123, 234, 321, and 432."
			],
			"mathematica": [
				"T[n_,k_]:=If[k==1, n,Sum[2(n-(k-1)r),{r,Floor[(n-1)/(k-1)]}]]; Flatten[Table[T[n,k],{n,12},{k,n}]] (* _Stefano Spezia_, Nov 17 2020 *)"
			],
			"program": [
				"(PARI) T(n,k) = if (k==1, n, sum(r=1, (n-1)\\(k-1), 2*(n-(k-1)*r))); \\\\ _Michel Marcus_, Sep 08 2021"
			],
			"xref": [
				"Cf. A008279."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Marcel K. Goh_, Nov 17 2020",
			"references": 2,
			"revision": 37,
			"time": "2021-09-08T06:34:22-04:00",
			"created": "2020-12-23T08:46:41-05:00"
		}
	]
}