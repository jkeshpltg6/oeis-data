{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A139621",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 139621,
			"data": "1,1,1,1,4,3,1,8,15,8,1,16,57,66,27,1,25,163,353,295,91,1,40,419,1504,2203,1407,350,1,56,932,5302,12382,13372,6790,1376,1,80,1940,16549,58237,96456,80736,33628,5743,1,105,3743,46566,237904,573963,717114,482730,168645,24635",
			"name": "Triangle read by rows: T(n,k) is the number of connected directed multigraphs with loops and no vertex of degree 0, with n arcs and k vertices.",
			"comment": [
				"Length of the n-th row: n+1."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A139621/b139621.txt\"\u003eTable of n, a(n) for n = 0..1325\u003c/a\u003e",
				"R. J. Mathar, \u003ca href=\"http://arxiv.org/abs/1709.09000\"\u003eStatistics on Small Graphs\u003c/a\u003e, arXiv:1709.09000 (2017) Table 71."
			],
			"formula": [
				"T(n,1) = 1.",
				"T(n,2) = A136564(n,2) - floor(n/2)."
			],
			"example": [
				"Triangle begins:",
				"     1",
				"     1     1",
				"     1     4     3",
				"     1     8    15     8",
				"     1    16    57    66    27",
				"     1    25   163   353   295    91",
				"     1    40   419  1504  2203  1407   350",
				"     1    56   932  5302 12382 13372  6790  1376",
				"T(2 arcs, 2 vertices) = 4: one graph 1-\u003e1, 2-\u003e1; one graph with 1-\u003e1, 1-\u003e2; one graph with 2-\u003e1, 2-\u003e1, one graph with 1-\u003e2, 2-\u003e1.",
				"T(2 arcs, 3 vertices) = 3: one graph 2-\u003e1, 3-\u003e1; one graph 2-\u003e1, 3-\u003e2; one graph 2-\u003e1, 2-\u003e3."
			],
			"program": [
				"(PARI)",
				"InvEulerMT(u)={my(n=#u, p=log(1+x*Ser(u)), vars=variables(p)); Vec(sum(i=1, n, moebius(i)*substvec(p + O(x*x^(n\\i)), vars, apply(v-\u003ev^i, vars))/i) )}",
				"permcount(v) = {my(m=1,s=0,k=0,t); for(i=1,#v,t=v[i]; k=if(i\u003e1\u0026\u0026t==v[i-1],k+1,1); m*=t*k;s+=t); s!/m}",
				"edges(v,t) = {prod(i=2, #v, prod(j=1, i-1, my(g=gcd(v[i],v[j])); t(v[i]*v[j]/g)^(2*g))) * prod(i=1, #v, t(v[i])^v[i])}",
				"G(n, x)={my(s=0); forpart(p=n, s+=permcount(p)/edges(p,i-\u003e1-x^i)); s/n!}",
				"T(n)={Mat([Col(p+O(y^n), -n) | p\u003c-InvEulerMT(vector(n, k, G(k, y + O(y^n))))])}",
				"{my(A=T(10)); for(n=1, #A, print(A[n,1..n]))} \\\\ _Andrew Howroyd_, Oct 22 2019"
			],
			"xref": [
				"Cf. A129620, A136564, A139622, A137975 (row sums), A000238 (diagonal)."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Benoit Jubin_, May 01 2008",
			"ext": [
				"Prepended a(0)=1 to have a regular triangle, _Joerg Arndt_, Apr 14 2013",
				"More terms from _R. J. Mathar_, Jul 31 2017",
				"Terms a(34) and beyond from _Andrew Howroyd_, Oct 22 2019"
			],
			"references": 5,
			"revision": 41,
			"time": "2019-10-22T19:24:21-04:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}