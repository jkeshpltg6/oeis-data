{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A253065",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 253065,
			"data": "1,5,5,17,5,25,17,65,5,25,25,85,17,85,65,229,5,25,25,85,25,125,85,325,17,85,85,289,65,325,229,813,5,25,25,85,25,125,85,325,25,125,125,425,85,425,325,1145,17,85,85,289,85,425,289,1105,65,325,325,1105,229,1145,813,2945,5,25,25,85",
			"name": "Number of odd terms in f^n, where f = 1+x+x^2+x^2*y+x^2/y.",
			"comment": [
				"This is the number of ON cells in a certain 2-D CA in which the neighborhood of a cell is defined by f, and in which a cell is ON iff there was an odd number of ON cells in the neighborhood at the previous generation.",
				"This is the odd-rule cellular automaton defined by OddRule 171 (see Ekhad-Sloane-Zeilberger \"Odd-Rule Cellular Automata on the Square Grid\" link)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A253065/b253065.txt\"\u003eTable of n, a(n) for n = 0..8191\u003c/a\u003e",
				"Shalosh B. Ekhad, N. J. A. Sloane, and  Doron Zeilberger, \u003ca href=\"http://arxiv.org/abs/1503.01796\"\u003eA Meta-Algorithm for Creating Fast Algorithms for Counting ON Cells in Odd-Rule Cellular Automata\u003c/a\u003e, arXiv:1503.01796 [math.CO], 2015; see also the \u003ca href=\"http://www.math.rutgers.edu/~zeilberg/mamarim/mamarimhtml/CAcount.html\"\u003eAccompanying Maple Package\u003c/a\u003e.",
				"Shalosh B. Ekhad, N. J. A. Sloane, and  Doron Zeilberger, \u003ca href=\"http://arxiv.org/abs/1503.04249\"\u003eOdd-Rule Cellular Automata on the Square Grid\u003c/a\u003e, arXiv:1503.04249 [math.CO], 2015.",
				"N. J. A. Sloane, On the No. of ON Cells in Cellular Automata, Video of talk in Doron Zeilberger's Experimental Math Seminar at Rutgers University, Feb. 05 2015: \u003ca href=\"https://vimeo.com/119073818\"\u003ePart 1\u003c/a\u003e, \u003ca href=\"https://vimeo.com/119073819\"\u003ePart 2\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"http://arxiv.org/abs/1503.01168\"\u003eOn the Number of ON Cells in Cellular Automata\u003c/a\u003e, arXiv:1503.01168 [math.CO], 2015.",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e"
			],
			"formula": [
				"This is the Run Length Transform of A253067."
			],
			"example": [
				"Here is the neighborhood f:",
				"[0, 0, X]",
				"[X, X, X]",
				"[0, 0, X]",
				"which contains a(1) = 5 ON cells."
			],
			"maple": [
				"C:=f-\u003esubs({x=1, y=1}, f);",
				"# Find number of ON cells in CA for generations 0 thru M defined by rule",
				"# that cell is ON iff number of ON cells in nbd at time n-1 was odd",
				"# where nbd is defined by a polynomial or Laurent series f(x, y).",
				"OddCA:=proc(f, M) global C; local n, a, i, f2, p;",
				"f2:=simplify(expand(f)) mod 2;",
				"a:=[]; p:=1;",
				"for n from 0 to M do a:=[op(a), C(p)]; p:=expand(p*f2) mod 2; od:",
				"lprint([seq(a[i], i=1..nops(a))]);",
				"end;",
				"f:=1+x+x^2+x^2*y+x^2/y;",
				"OddCA(f, 130);"
			],
			"mathematica": [
				"(* f = A253067 *) f[0]=1; f[1]=5; f[2]=17; f[3]=65; f[4]=229; f[5]=813; f[n_] := f[n] = 8 f[n-5] + 6 f[n-4] + 13 f[n-3] + 5 f[n-2] + f[n-1]; Table[Times @@ (f[Length[#]]\u0026) /@ Select[s = Split[IntegerDigits[n, 2]], #[[1]] == 1\u0026], {n, 0, 67}] (* _Jean-François Alcover_, Jul 12 2017 *)"
			],
			"xref": [
				"Other CA's that use the same rule but with different cell neighborhoods: A160239, A102376, A071053, A072272, A001316, A246034, A246035, A253064, A253066.",
				"Cf. A253067."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Jan 26 2015",
			"references": 5,
			"revision": 26,
			"time": "2017-07-12T03:20:11-04:00",
			"created": "2015-01-26T23:41:15-05:00"
		}
	]
}