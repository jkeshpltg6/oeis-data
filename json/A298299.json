{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A298299",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 298299,
			"data": "2,2,6,2,10,6,14,2,36,20,66,18,26,28,120,2,170,6,570,140,2184,88,184,42,110,312,1440,42,116,9060",
			"name": "a(n) is the smallest b \u003e 0 such that b^(2n) + 1 has all prime divisors p == 1 (mod 2n).",
			"comment": [
				"All the terms are even.",
				"The number a(n)^(2n) + 1 has all divisors d == 1 (mod 2n).",
				"Conjecture: a(n) exists for every n. This is implied by the generalized Bunyakovsky conjecture (Schinzel's hypothesis H).",
				"Theorem: a(n) = 2 if and only if n is a power of 2.",
				"Note: rad(2n) divides rad(a(n)), where rad(m) = A007947(m).",
				"Even numbers 2n such that a(n) = rad(2n) are powers of two and 6, 10, 12, 14, 26, 36, ... Are there infinitely many such numbers?",
				"We have a(n) = 2n = 2, 6, 10, 14, 20, 26, 28, ...",
				"Problem: are there infinitely many even numbers m \u003c\u003e 2^k such that the number m^m + 1 has all divisors d == 1 (mod m)?"
			],
			"formula": [
				"a(n) = min{b\u003e1: for all prime p, if p|(b^(2n)+1) then p==1(mod 2n)}."
			],
			"example": [
				"a(5) = 10, because 10^10 + 1 = 10000000001 = 101*3541*27961 and all the prime factors p == 1 (mod 2*5), so all divisors d == 1 (mod 10).",
				"a(30) = 9060."
			],
			"program": [
				"(PARI)",
				"find_a_ORDOWSKI2(n=2, a=1, B_START=2, LIM=10^11,DEBUG=1)={",
				"  my(B,FF,LL);",
				"  my(fn=\"_THOMAS_ORDOWSKI_b_a_n.txt\");",
				"  LL=R2('b,a,n);   \\\\ R(b,a,n)=(b^n+a)",
				"  FF=factor(LL);",
				"  if(DEBUG==1,",
				"    print(FF);",
				"    print(LL);",
				"  );",
				"  if(Mod(n,2)==0,  \\\\ n-EVEN",
				"    B=FIND_BASE(n,BSTART=B_START,LIM,STEP=2,FF);",
				"  );",
				"  if(B\u003e0,",
				"     return([n,B,[subst(FF,'b,B)]]);",
				"  );",
				"  return(0);",
				"}"
			],
			"xref": [
				"Cf. A007947, A298076 (see PARI subroutines used for a(48)), A298398."
			],
			"keyword": "nonn,hard,more",
			"offset": "1,1",
			"author": "_Krzysztof Ziemak_ and _Thomas Ordowski_, Jan 16 2018",
			"references": 1,
			"revision": 37,
			"time": "2018-01-19T02:16:05-05:00",
			"created": "2018-01-19T02:16:05-05:00"
		}
	]
}