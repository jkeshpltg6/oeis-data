{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A291250",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 291250,
			"data": "1,3,4,13,17,52,69,203,272,781,1053,2976,4029,11267,15296,42469,57765,159596,217361,598499,815860,2241165,3057025,8383872,11440897,31340691,42781588,117100285,159881873,437378260,597260133,1633244795,2230504928,6097779229",
			"name": "p-INVERT of (0,1,0,1,0,1,...), where p(S) = 1 - S - 2 S^2 + 2 S^3.",
			"comment": [
				"Suppose s = (c(0), c(1), c(2), ...) is a sequence and p(S) is a polynomial. Let S(x) = c(0)*x + c(1)*x^2 + c(2)*x^3 + ... and T(x) = (-p(0) + 1/p(S(x)))/x. The p-INVERT of s is the sequence t(s) of coefficients in the Maclaurin series for T(x). Taking p(S) = 1 - S gives the \"INVERT\" transform of s, so that p-INVERT is a generalization of the \"INVERT\" transform (e.g., A033453).",
				"See A291219 for a guide to related sequences."
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A291250/b291250.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1, 5, -4, -5, 1, 1)"
			],
			"formula": [
				"G.f.: (-1 - 2 x + 4 x^2 + 2 x^3 - x^4)/(-1 + x + 5 x^2 - 4 x^3 - 5 x^4 + x^5 + x^6).",
				"a(n) = a(n-1) + 5*a(n-2) - 4*a(n-3) - 5*a(n-4) + a(n-5) + a(n-6) for n \u003e= 7."
			],
			"mathematica": [
				"z = 60; s = x/(1 - x^2); p = 1 - s - 2 s^2 + 2 s^3;",
				"Drop[CoefficientList[Series[s, {x, 0, z}], x], 1]  (* A000035 *)",
				"Drop[CoefficientList[Series[1/p, {x, 0, z}], x], 1]  (* A291250 *)",
				"LinearRecurrence[{1,5,-4,-5,1,1},{1,3,4,13,17,52},40] (* _Harvey P. Dale_, May 13 2019 *)"
			],
			"xref": [
				"Cf. A000035, A291219."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Clark Kimberling_, Aug 29 2017",
			"references": 2,
			"revision": 10,
			"time": "2019-05-13T13:51:48-04:00",
			"created": "2017-09-03T21:41:52-04:00"
		}
	]
}