{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A226132",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 226132,
			"data": "1,-1,3,-1,6,-3,8,-1,9,-6,12,-3,14,-8,18,-1,18,-9,20,-6,24,-12,24,-3,31,-14,27,-8,30,-18,32,-1,36,-18,48,-9,38,-20,42,-6,42,-24,44,-12,54,-24,48,-3,57,-31,54,-14,54,-27,72,-8,60,-30,60,-18,62,-32,72",
			"name": "Expansion of - c(-q) * c(q^2) / 9 in powers of q where c() is a cubic AGM theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Number 91 of the 126 eta-quotients listed in Table 1 of Williams 2012."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A226132/b226132.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e",
				"K. S. Williams, \u003ca href=\"http://people.math.carleton.ca/~williams/papers/pdf/342.pdf\"\u003eFourier series of a class of eta quotients\u003c/a\u003e, Int. J. Number Theory 8 (2012), no. 4, 993-1004."
			],
			"formula": [
				"Expansion of (a(q) - a(q^2)) * (a(q^2) + 2 * a(q^4)) / 18 = c(q^2)^4 / (9 * c(q) * c(q^4)) = (b(-q) * b(q^2) - a(-q) * a(q^2)) / 9 in powers of q where a(), b(), c(q) are cubic AGM theta functions.",
				"Expansion of q * (phi(q^3)^3 / phi(q)) * (ps(-q^3)^3 / psi(-q)) in powers of q where phi(), psi() are Ramanujan theta functions.",
				"Expansion of eta(q) * eta(q^4) * eta(q^6)^12 / (eta(q^2)^4 * eta(q^3)^3 * eta(q^12)^3) in powers of q.",
				"Euler transform of period 12 sequence [ -1, 3, 2, 2, -1, -6, -1, 2, 2, 3, -1, -4, ...].",
				"Multiplicative with a(2^e) = -1 if e\u003e0, a(3^e) = 3^e, a(p^e) = (p^(e+1) - 1) / (p - 1) if p\u003e3.",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (12 t)) = 4/3 (t/i)^2 g(t) where q = exp(2 Pi i t) and g() is the g.f. for A226139.",
				"G.f.: Sum_{k\u003e0 not 3|k} x^k / (1 - (-x)^k)^2 = Sum_{k\u003e0 not 2|k} k * x^k * (1 - x^k) / (1 + x^(3*k)).",
				"G.f.: x * Product_{k\u003e0} (1 - x^k) * (1 - x^(4*k)) * (1 - x^(3*k))^6 * (1 + x^(3*k))^9 / ((1 - x^(2*k))^4 * (1 + x^(6*k))^3).",
				"a(2*n) = - A121443(n). a(2*n + 1) = A185717(n).",
				"a(n) = -(-1)^n * A121443(n). Convolution of A113447 and A113973."
			],
			"example": [
				"G.f. = q - q^2 + 3*q^3 - q^4 + 6*q^5 - 3*q^6 + 8*q^7 - q^8 + 9*q^9 - 6*q^10 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 1, 0, Sum[ If[ OddQ[d] \u0026\u0026 ! Divisible[ n/d, 3], -d (-1)^(n/d), 0], {d, Divisors[ n]}]];",
				"a[ n_] := If[ n \u003c 2, Boole[n == 1], Times @@ (Which[ # == 2, -1, # == 3, #^#2, True, (#^(#2 + 1) - 1) / (# - 1)] \u0026 @@@ FactorInteger[n])];",
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 3, 0, q^3]^3 / EllipticTheta[ 3, 0, q] EllipticTheta[ 2, 0, I q^(3/2)]^3 / EllipticTheta[ 2, 0, I q^(1/2)] / (4 (-1)^(1/4)), {q, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, 0, sumdiv( n, d, if(d%2 \u0026\u0026 (n/d)%3, -d * (-1)^(n/d))))};",
				"(PARI) {a(n) = my(A, p, e); if( n\u003c1, 0, A = factor(n); prod( k= 1, matsize(A)[1], if( p=A[k, 1], e=A[k, 2]; if( p==2, -1, if( p==3, p^e, (p^(e+1) - 1) / (p - 1))))))};",
				"(PARI) {a(n) = my(A); if( n\u003c1, 0, n--; A = x * O(x^n); polcoeff( eta(x + A) * eta(x^4 + A) * eta(x^6 + A)^12 / (eta(x^2 + A)^4 * eta(x^3 + A)^3 * eta(x^12 + A)^3), n))};"
			],
			"xref": [
				"Cf. A113447, A113973, A121443, A185717, A226139."
			],
			"keyword": "sign,mult",
			"offset": "1,3",
			"author": "_Michael Somos_, May 27 2013",
			"references": 2,
			"revision": 19,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2013-05-30T10:58:47-04:00"
		}
	]
}