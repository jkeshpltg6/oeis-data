{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A332439",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 332439,
			"data": "0,1,6,9,10,1,4,5,10,13,0,5,8,9,0,3,4,9,12,13,4,7,8,13,2,3,8,11,12,3,6,7,12,1,2,7,10,11,2,5,6,11",
			"name": "Primitive period of the partial sums of the periodic unsigned Schick sequence for N = 7 (A130794), taken modulo 14, and the related Euler tour using all regular 14-gon vertices.",
			"comment": [
				"The unsigned Schick sequences SBBseq(N, q0) (BB for Brändli and Beyne) are defined by the recurrence q_j(N) = |N - 2*q_{j-1}(N)|, for odd N \u003e= 3, and j \u003e= 1, with certain initial odd values q0 with gcd(q0, N) = 1. They are periodic with primitive period length ppl(n) = A003558((N-1)/2) (pes = pes(N) in Schick's book). One starts with initial value q0 = 1. If not all odd elements of the smallest positive reduced residue system modulo N (RRSodd(N)) are present then one takes the smallest missing odd number as next initial value q0, etc., until all elements of RRSodd(N) have been reached. The number of necessary distinct initial values q0 is A135303((N-1)/2) (called B = B(N) by Schick).",
				"The present sequence is the instance N = 7. SBBseq(7, 1) = repeat(1,5,3,) = A130794, ppl(7) = 3, RRSodd(7)= {1, 3, 5}, B(7) = 1. The start has been taken as a(0) = 0 with offset 0 (not a(42) = 0, with offset 1). The primitive period length of SBBseq(7, 1) is (2*7)*3 = 42.",
				"The primitive periods of the B(N) sequences, each of length ppl(N), define the set SBB(N). The entries of these periods can be interpreted as odd vertex labels in a regular 2*N-gon, with start vertex V^{(2*N)})_{0} (in Cartesian coordinates (r, 0), with r the radius of the circumscribing circle). The other vertices V^{(2*N)}_k, for k = 1..2*N-1, are taken in the positive (counterclockwise) sense.",
				"In the present case N = 7 there are three directed diagonals (arrows) d_1 = s = arrow(V14_0, V14_1) of length r*0.4450418..., d_3 = arrow(V14_0, V14_3) of length r*1.2469796..., and d_5 = arrow(V14_0, V14_5), of length r*1.8019377... (V14 is shorthand for a 14-gon vertex, and s is the side).",
				"A trail with these three arrows (considered as free vectors) in the order d1, d5, and d3, with tails following the present sequence, leads to an Euler tour, involving all vertices of the 14-gon, each visited thrice. Therefore this is a simple regular digraph with 14 vertices each of degree 6 (out-degree = in-degree = 3), and 42 arrows each used once in the tour from V14_0 to V14_0 (or starting from any other vertex). See the figure in the link.",
				"The vertex-vertex 14 X 14 incidence matrix of this directed Euler graph is cyclic with first row [0,1,0,1,0,1,0_8] (0_k for k zeros) shifted by one step to the right, with last row [1, 0, 1, 0, 1, 0_9].",
				"The 14 triples of positions of the vertex labels k, for k from 0 to 13, in the present sequence are given in A332440.",
				"For N = 3, SBBseq(3, 1) = A000012 (sequence of 1's). The primitive period modulo 6 is [0, 1, 2, 3, 4, 5], and the Euler tour, using as diagonal only the side, is the digraph C_6 (circle graph, trail in the positive sense).",
				"For N=5, SBBseq(5, 1) = repeat(1,3,) = A010684. The primitive period modulo 10 is [0, 1, 4, 5, 8, 9, 2, 3, 6, 7]. The simple digraph, using alternatively the two diagonals d1 (side) and d3 in the regular 10-gon, is regular with degree 2."
			],
			"reference": [
				"Carl Schick, Trigonometrie und unterhaltsame Zahlentheorie, Bokos Druck, Zürich, 2003 (ISBN 3-9522917-0-6). Tables 3.1 to 3.10, for odd p = 3..113 (with gaps), pp. 158-166."
			],
			"link": [
				"Gerold Brändli and Tim Beyne, \u003ca href=\"https://arxiv.org/abs/1504.02757\"\u003eModified Congruence Modulo n with Half the Amount of Residues\u003c/a\u003e, arXiv:1504.02757 [math.NT], 2015-2016.",
				"Wolfdieter Lang, \u003ca href=\"/A332439/a332439.pdf\"\u003eFigure: A directed Euler tour on the regular 14-gon with length 42\u003c/a\u003e",
				"Wolfdieter Lang, \u003ca href=\"https://arxiv.org/abs/2008.04300\"\u003eOn the Equivalence of Three Complete Cyclic Systems of Integers\u003c/a\u003e, arXiv:2008.04300 [math.NT], 2020."
			],
			"formula": [
				"a(n) = (Sum_{j=0..n} A130794(j)) mod 14, for n \u003e= 1 with the periodic sequence SBBseq(7, 1) = repeat(1,5,3,) = A130794, with offset 0, and a(0) = 0 (= a(42))."
			],
			"program": [
				"(PARI) get(v, j) = my(x=lift(Mod(j, #v))); if (x==0, x = #v); v[x];",
				"vector(42, k, k--; sum(j=1, k, get([1,5,3], j)) % 14) \\\\ _Michel Marcus_, Jun 11 2020"
			],
			"xref": [
				"Cf. A000012, A003558, A010684, A135303, A332440, A332441."
			],
			"keyword": "nonn,fini,full,walk",
			"offset": "0,3",
			"author": "_Wolfdieter Lang_, Apr 04 2020",
			"references": 6,
			"revision": 32,
			"time": "2021-02-11T09:23:41-05:00",
			"created": "2020-05-09T12:55:06-04:00"
		}
	]
}