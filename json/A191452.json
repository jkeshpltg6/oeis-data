{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A191452",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 191452,
			"data": "1,4,2,16,8,3,64,32,12,5,256,128,48,20,6,1024,512,192,80,24,7,4096,2048,768,320,96,28,9,16384,8192,3072,1280,384,112,36,10,65536,32768,12288,5120,1536,448,144,40,11,262144,131072,49152,20480,6144,1792,576",
			"name": "Dispersion of (4,8,12,16,...), by antidiagonals.",
			"comment": [
				"Background discussion:  Suppose that s is an increasing sequence of positive integers, that the complement t of s is infinite, and that t(1)=1.  The dispersion of s is the array D whose n-th row is (t(n), s(t(n)), s(s(t(n)), s(s(s(t(n)))), ...).  Every positive integer occurs exactly once in D, so that, as a sequence, D is a permutation of the positive integers.  The sequence u given by u(n)=(number of the row of D that contains n) is a fractal sequence.  Examples:",
				"(1) s=A000040 (the primes), D=A114537, u=A114538.",
				"(2) s=A022343 (without initial 0), D=A035513 (Wythoff array), u=A003603.",
				"(3) s=A007067, D=A035506 (Stolarsky array), u=A133299.",
				"More recent examples of dispersions: A191426-A191455."
			],
			"link": [
				"Ivan Neretin, \u003ca href=\"/A191452/b191452.txt\"\u003eTable of n, a(n) for n = 1..5050\u003c/a\u003e (first 100 antidiagonals, flattened)"
			],
			"example": [
				"Northwest corner:",
				"1...4....16....64...256",
				"2...8....32...128...512",
				"3...12...48...192...768",
				"5...20...80...320...1280",
				"6...24...96...384...1536"
			],
			"mathematica": [
				"(* Program generates the dispersion array T of increasing sequence f[n] *)",
				"r=40; r1=12; c=40; c1=12;",
				"f[n_] :=4n  (* complement of column 1 *)",
				"mex[list_] := NestWhile[#1 + 1 \u0026, 1, Union[list][[#1]] \u003c= #1 \u0026, 1, Length[Union[list]]]",
				"rows = {NestList[f, 1, c]};",
				"Do[rows = Append[rows, NestList[f, mex[Flatten[rows]], r]], {r}];",
				"t[i_, j_] := rows[[i, j]];",
				"TableForm[Table[t[i, j], {i, 1, 10}, {j, 1, 10}]]",
				"(* A191452 array *)",
				"Flatten[Table[t[k, n - k + 1], {n, 1, c1}, {k, 1, n}]] (* A191452 sequence *)",
				"(* Program by _Peter J. C. Moses_, Jun 01 2011 *)"
			],
			"xref": [
				"Cf. A114537, A035513, A035506, A191449."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Jun 05 2011",
			"references": 9,
			"revision": 13,
			"time": "2017-10-17T20:49:51-04:00",
			"created": "2011-06-06T12:51:44-04:00"
		}
	]
}