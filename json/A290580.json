{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A290580",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 290580,
			"data": "1,1,3,0,16,1,125,20,0,1296,364,1,16807,7028,112,0,262144,148752,5868,1,4782969,3471192,250128,576,0,100000000,89097664,10020912,82408,1,2357947691,2503362488,399379728,7354688,2816,0,61917364224,76575071488,16255733440,533661360,1066552,1,1792160394037,2536513162508,684615750832,35063521792,194025728,13312,0,56693912375296,90532686154752,30031767680256,2200207121408,24852054816,13053492,1,1946195068359375,3465845396598540,1376568893633760,135791393602560,2630843800320,4759188480,61440,0",
			"name": "E.g.f. W = W(x,m) satisfies: W = E(x*W,m) where E(x,m) = cn(i*x,m) - i*sn(i*x,m), with sn(x,m) and cn(x,m) being Jacobi elliptic functions, read as an irregular triangle of coefficients T(n,k) of x^n*m^k for n\u003e=0 and k=0..[n/2].",
			"comment": [
				"An elliptic analog of the function W = LambertW(-x)/(-x) where W = exp(x*W)."
			],
			"formula": [
				"E.g.f.: W(x,m) = (1/x) * Series_Reversion( x*cn(x,1-m)/(1 + sn(x,1-m)) ).",
				"Define E(x,m) = (1 + sn(x,1-m)) / cn(x,1-m), then",
				"(1) W(x,m) = (1/x) Series_Reversion( x/E(x,m) ).",
				"Further, the n-th row polynomial in m, R(n,m), is given by",
				"(2) R(n,m) = [x^n/n!] E(x,m)^(n+1) / (n+1) for n\u003e=0, where",
				"W(x,m) = Sum_{n\u003e=0} R(n,m) * x^n/n!."
			],
			"example": [
				"E.g.f. W(x,m) = 1 + (1)*x + (3)*x^2/2! + (16 + m)*x^3/3! +",
				"(125 + 20*m)*x^4/4! + (1296 + 364*m + m^2)*x^5/5! +",
				"(16807 + 7028*m + 112*m^2)*x^6/6! +",
				"(262144 + 148752*m + 5868*m^2 + m^3)*x^7/7! +",
				"(4782969 + 3471192*m + 250128*m^2 + 576*m^3)*x^8/8! +",
				"(100000000 + 89097664*m + 10020912*m^2 + 82408*m^3 + m^4)*x^9/9! +",
				"(2357947691 + 2503362488*m + 399379728*m^2 + 7354688*m^3 + 2816*m^4)*x^10/10! +...",
				"such that W = W(x,m) satisfies:",
				"W = E(x*W,m)",
				"where E(x,m) is an elliptic analog to the exponential function, defined by",
				"E(x,m) = cn(i*x,m) - i*sn(i*x,m).",
				"By Jacobi's imaginary transformation, we have",
				"E(x,m) = (1 + sn(x,1-m)) / cn(x,1-m),",
				"where",
				"E(x,m) = 1 + x + x^2/2! + (m + 1)*x^3/3! + (4*m + 1)*x^4/4! + (m^2 + 14*m + 1)*x^5/5! + (16*m^2 + 44*m + 1)*x^6/6! + (m^3 + 135*m^2 + 135*m + 1)*x^7/7! + (64*m^3 + 912*m^2 + 408*m + 1)*x^8/8! + (m^4 + 1228*m^3 + 5478*m^2 + 1228*m + 1)*x^9/9! + (256*m^4 + 15808*m^3 + 30768*m^2 + 3688*m + 1)*x^10/10! +...",
				"Explicitly,",
				"W(x,m) = (1/x) Series_Reversion( x/E(x,m) ).",
				"As a series of row polynomial coefficients of powers of x,",
				"W(x,m) = Sum_{n\u003e=0} x^n/n! * { [x^n/n!] E(x,m)^(n+1) / (n+1) }.",
				"IRREGULAR TRIANGLE.",
				"This triangle of coefficients in e.g.f. W(x,m) begins:",
				"1 ;",
				"1 ;",
				"3, 0 ;",
				"16, 1 ;",
				"125, 20, 0 ;",
				"1296, 364, 1 ;",
				"16807, 7028, 112, 0 ;",
				"262144, 148752, 5868, 1 ;",
				"4782969, 3471192, 250128, 576, 0 ;",
				"100000000, 89097664, 10020912, 82408, 1 ;",
				"2357947691, 2503362488, 399379728, 7354688, 2816, 0 ;",
				"61917364224, 76575071488, 16255733440, 533661360, 1066552, 1 ;",
				"1792160394037, 2536513162508, 684615750832, 35063521792, 194025728, 13312, 0 ; ..."
			],
			"program": [
				"(PARI) /* By definition: */",
				"{ T(n,k) = my(W=1,E=1, S=x,C=1,D=1); for(i=0,n,",
				"S = intformal(C*D +x*O(x^n)) ;",
				"C = 1 - intformal(S*D) ; D = 1 - m*intformal(S*C) ;",
				"E = subst(C - I*S,x,I*x) ) ;",
				"for(i=0,n, W = subst(E,x,x*W));",
				"n!*polcoeff(polcoeff(W, n,x), k,m) }",
				"for(n=0,10, for(k=0,n\\2, print1( T(n,k), \", \")); print(\"\"))",
				"(PARI) /* Using Jacobi's imaginary transformation: */",
				"{ T(n,k) = my(W=1,E=1, S=x,C=1,D=1); for(i=0,n,",
				"S = intformal(C*D +x*O(x^n)) ;",
				"C = 1 - intformal(S*D) ; D = 1 - m*intformal(S*C) ;",
				"E = subst( (1 + S)/C,m,1-m) ) ;",
				"for(i=0,n, W = subst(E,x,x*W));",
				"n!*polcoeff(polcoeff(W, n,x), k,m) }",
				"for(n=0,10, for(k=0,n\\2, print1( T(n,k), \", \")); print(\"\"))"
			],
			"xref": [
				"Cf. A290579 (row sums), A000272 (column 0), A290581 (column 1), A291214 (column 2)."
			],
			"keyword": "nonn,tabf",
			"offset": "0,3",
			"author": "_Paul D. Hanna_, Aug 07 2017",
			"references": 3,
			"revision": 26,
			"time": "2017-08-21T17:24:19-04:00",
			"created": "2017-08-07T00:28:10-04:00"
		}
	]
}