{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A158286",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 158286,
			"data": "1,-1,-1,9,10,1,-256,-288,-33,-1,15625,17500,1950,76,1,-1679616,-1866240,-194400,-7920,-145,-1,282475249,311299254,30000495,1200500,24255,246,1,-68719476736,-75161927680,-6694109184,-256901120,-5304320,-61824,-385,-1",
			"name": "Triangle T(n, k) = coefficients of p(n, x), where p(n, x) = (-1)^n*(1+x)*((n+1)^2 +x)^(n-1), p(0, x) = 1, and p(1, x) = -1-x, read by rows.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A158286/b158286.txt\"\u003eRows n = 0..50 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = coefficients of the characteristic polynomials from the matrix defined by M = M_{0} * M_{1} where M_{0} = (m0_{i,j}), m0_{j,j} = n, otherwise -1 and M_{1} = (m1_{i,j}), m1_{j,j} = -n, otherwise 1.",
				"T(n, k) = coefficients of p(n, x), where p(n, x) = (-1)^n*(1+x)*((n+1)^2 +x)^(n-1), p(0, x) = 1, and p(1, x) = -1-x. - _G. C. Greubel_, May 14 2021"
			],
			"example": [
				"Triangle begins as:",
				"             1;",
				"            -1,           -1;",
				"             9,           10,           1;",
				"          -256,         -288,         -33,         -1;",
				"         15625,        17500,        1950,         76,        1;",
				"      -1679616,     -1866240,     -194400,      -7920,     -145,     -1;",
				"     282475249,    311299254,    30000495,    1200500,    24255,    246,    1;",
				"  -68719476736, -75161927680, -6694109184, -256901120, -5304320, -61824, -385, -1;"
			],
			"mathematica": [
				"(* First program *)",
				"M0[n_]:= Table[If[m==k, n, -1], {k,1,n}, {m,1,n}];",
				"M1[n_]:= Table[If[m==k, -n, 1], {k,1,n}, {m,1,n}];",
				"M[n_]:= M0[n].M1[n];",
				"Join[{{1}}, Table[CoefficientList[CharacteristicPolynomial[M[n], x], x], {n,10}]]//Flatten (* modified by _G. C. Greubel_, May 14 2021 *)",
				"(* Second program *)",
				"f[n_]:= If[n\u003c2, (-1)^n*(1+n*x), (-1)^n*(1+x)*((n+1)^2 +x)^(n-1)];",
				"T[n_, k_]:= SeriesCoefficient[f[n], {x,0,k}];",
				"Table[T[n, k], {n,0,10}, {k,0,n}]//Flatten (* _G. C. Greubel_, May 14 2021 *)"
			],
			"program": [
				"(Sage)",
				"def p(n,x): return (-1)^n*(1 + n*x) if (n\u003c2) else (-1)^n*(1+x)*((n+1)^2 +x)^(n-1)",
				"def T(n): return ( p(n,x) ).full_simplify().coefficients(sparse=False)",
				"flatten([T(n) for n in (0..10)]) # _G. C. Greubel_, May 14 2021"
			],
			"xref": [
				"Cf. A158285."
			],
			"keyword": "sign,tabl,less",
			"offset": "0,4",
			"author": "_Roger L. Bagula_, Mar 15 2009",
			"ext": [
				"Edited by _G. C. Greubel_, May 14 2021"
			],
			"references": 2,
			"revision": 5,
			"time": "2021-05-14T18:47:44-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}