{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A187306",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 187306,
			"data": "1,0,2,2,7,14,37,90,233,602,1586,4212,11299,30536,83098,227474,625993,1730786,4805596,13393688,37458331,105089228,295673995,834086420,2358641377,6684761124,18985057352,54022715450,154000562759,439742222070,1257643249141",
			"name": "Alternating sum of Motzkin numbers A001006.",
			"comment": [
				"Diagonal sums of A089942.",
				"Hankel transform is A187307.",
				"Also gives the number of simple permutations of each length that avoid the pattern 321 (i.e. are the union of two increasing sequences, and in one line notation contain no nontrivial block of values which form an interval). There are 2 such permutations of length 4, 2 of length 5, etc. - _Michael Albert_, Jun 20 2012",
				"Convolution of A005043 with itself. - _Philippe Deléham_, Jan 28 2014"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A187306/b187306.txt\"\u003eTable of n, a(n) for n = 0..300\u003c/a\u003e",
				"M. H. Albert and V. Vatter, \u003ca href=\"http://arxiv.org/abs/1301.3122\"\u003eGenerating and enumerating 321-avoiding and skew-merged simple permutations\u003c/a\u003e, arXiv preprint arXiv:1301.3122 [math.CO], 2013. - _N. J. A. Sloane_, Feb 11 2013",
				"Paul Barry, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL19/Barry/barry321.html\"\u003eJacobsthal Decompositions of Pascal's Triangle, Ternary Trees, and Alternating Sign Matrices\u003c/a\u003e, Journal of Integer Sequences, 19, 2016, #16.3.5.",
				"Cheyne Homberger, \u003ca href=\"http://arxiv.org/abs/1410.2657\"\u003ePatterns in Permutations and Involutions: A Structural and Enumerative Approach\u003c/a\u003e, arXiv preprint 1410.2657 [math.CO], 2014."
			],
			"formula": [
				"G.f.: (1-x-sqrt(1-2*x-3*x^2))/(2*x^2*(1+x)).",
				"a(n) = sum(k=0..n, A001006(k)*(-1)^(n-k)).",
				"D-finite with recurrence -(n+2)*a(n) +(n-1)*a(n-1) +(5*n-2)*a(n-2) +3*(n-1)a(n-3)=0. - _R. J. Mathar_, Nov 17 2011",
				"a(n) = (2*sum(j=0..n, C(2*j+1,j+1)*(-1)^(n-j)*C(n+2,j+2)))/(n+2). - _Vladimir Kruchinin_, Feb 06 2013",
				"a(n) ~ 3^(n+5/2)/(8*sqrt(Pi)*n^(3/2)). - _Vaclav Kotesovec_, Aug 15 2013",
				"a(n) = (-1)^n*(1-hypergeom([1/2,-n-1],[2],4)). - _Peter Luschny_, Sep 25 2014",
				"a(n) = A005043(n+1) + (-1)^n. - _Peter Luschny_, Sep 25 2014",
				"G.f.: (1/(1 - x^2/(1 - x - x^2/(1 - x - x^2/(1 - x - x^2/(1 - ...))))))^2, a continued fraction. - _Ilya Gutkovskiy_, Sep 23 2017"
			],
			"maple": [
				"a := n -\u003e (-1)^n*(1-hypergeom([1/2,-n-1],[2],4));",
				"seq(round(evalf(a(n),99)),n=0..30); # _Peter Luschny_, Sep 25 2014"
			],
			"mathematica": [
				"CoefficientList[Series[(1-x-Sqrt[1-2x-3x^2])/(2x^2(1+x)), {x,0,30}], x] (* _Harvey P. Dale_, Jun 14 2011 *)"
			],
			"program": [
				"(PARI) x='x+O('x^66); Vec((1-x-sqrt(1-2*x-3*x^2))/(2*x^2*(1+x))) /* _Joerg Arndt_, Mar 07 2013 */",
				"(PARI) Vec(serreverse(x*(1-x)/(1-x+x^2) + O(x^30))^2) \\\\ _Andrew Howroyd_, Apr 28 2018",
				"(Sage)",
				"def A187306():",
				"    a, b, n = 1, 0, 1",
				"    yield a",
				"    while True:",
				"        n += 1",
				"        a, b = b, (2*b+3*a)*(n-1)/(n+1)",
				"        yield b - (-1)^n",
				"A187306_list = A187306()",
				"[next(A187306_list) for i in range(20)] # _Peter Luschny_, Sep 25 2014"
			],
			"xref": [
				"Cf. A001006, A005043, A089942."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Paul Barry_, Mar 08 2011",
			"references": 5,
			"revision": 73,
			"time": "2021-10-12T07:50:41-04:00",
			"created": "2011-03-08T10:23:51-05:00"
		}
	]
}