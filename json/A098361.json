{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A098361",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 98361,
			"data": "1,1,1,2,1,2,6,2,2,6,24,6,4,6,24,120,24,12,12,24,120,720,120,48,36,48,120,720,5040,720,240,144,144,240,720,5040,40320,5040,1440,720,576,720,1440,5040,40320,362880,40320,10080,4320,2880,2880,4320,10080,40320,362880",
			"name": "Multiplication table of the factorial numbers read by antidiagonals.",
			"comment": [
				"This sequence gives the variance of the 2-dimensional Polynomial Chaoses (see the Stochastic Finite Elements reference). - _Stephen Crowley_, Mar 28 2007",
				"Antidiagonal sums of the array A are A003149 (row sums of the triangle T). - _Roger L. Bagula_, Oct 29 2008",
				"The triangle T(n, k) = k!*(n-k)! appears as denominators in the coefficients of the Niven polynomials x^n*(1 - x^n)/n! = Sum_{k=0..n} (-1)^k * x^(n+k)/((n-k)!*k!). These polynomials are used in a proof that Pi^2 (hence Pi) is irrational. See the Niven and Havil references. - _Wolfdieter Lang_, May 07 2018"
			],
			"reference": [
				"R. Ghanem and P. Spanos, Stochastic Finite Elements: A Spectral Approach (Revised Edition), 2003, Ch 2.4 Table 2-2.",
				"Julian Havil, The Irrationals, Princeton University Press, Princeton and Oxford, 2012, pp. 116-125.",
				"Ivan Niven, Irrational Numbers, Math. Assoc. Am., John Wiley and Sons, New York, 2nd printing 1963, pp. 19-21."
			],
			"link": [
				"Stefano Spezia, \u003ca href=\"/A098361/b098361.txt\"\u003eFirst 101 antidiagonals of the array, flattened\u003c/a\u003e",
				"Luis Manuel Rivera, \u003ca href=\"http://arxiv.org/abs/1406.3081\"\u003eInteger sequences and k-commuting permutations\u003c/a\u003e, arXiv preprint arXiv:1406.3081 [math.CO], 2014-2015."
			],
			"formula": [
				"T(n, k) = k!*(n-k)! =  n!/C(n,k), (0\u003c=k\u003c=n). - _Peter Luschny_, Aug 23 2010",
				"Array A(n, k) = n!*k! = (k+n)!/binomial(k+n,n). - _R. J. Mathar_, Dec 10 2010",
				"E.g.f. as array: 1/((1 - x)*(1 - y)). - _Stefano Spezia_, Jul 10 2020"
			],
			"example": [
				"The array A(n, k) starts in row n=0 with columns k \u003e= 0 as:",
				"       1,      1,      2,       6,      24,      120, ...",
				"       1,      1,      2,       6,      24,      120, ...",
				"       2,      2,      4,      12,      48,      240, ...",
				"       6,      6,     12,      36,     144,      720, ...",
				"      24,     24,     48,     144,     576,     2880, ...",
				"     120,    120,    240,     720,    2880,    14400, ...",
				"     720,    720,   1440,    4320,   17280,    86400, ...",
				"    5040,   5040,  10080,   30240,  120960,   604800, ...",
				"   40320,  40320,  80640,  241920,  967680,  4838400, ...",
				"  362880, 362880, 725760, 2177280, 8709120, 43545600, ...",
				"The triangle T(n, k) begins:",
				"n\\k       0      1     2     3     4     5     6     7     8      9      10...",
				"0:        1",
				"1:        1      1",
				"2:        2      1     2",
				"3:        6      2     2     6",
				"4:       24      6     4     6    24",
				"5:      120     24    12    12    24   120",
				"6:      720    120    48    36    48   120   720",
				"7:     5040    720   240   144   144   240   720  5040",
				"8:    40320   5040  1440   720   576   720  1440  5040 40320",
				"9:   362880  40320 10080  4320  2880  2880  4320 10080 40320 362880",
				"10: 3628800 362880 80640 30240 17280 14400 17280 30240 80640 362880 3628800",
				"... - _Wolfdieter Lang_, May 07 2018."
			],
			"maple": [
				"seq(print(seq(k!*(n-k)!,k=0..n)),n=0..6); # _Peter Luschny_, Aug 23 2010"
			],
			"mathematica": [
				"Table[Table[(n + 1)!*Beta[n - m + 1, m + 1], {m, 0, n}], {n, 0, 10}] Flatten[%] (* _Roger L. Bagula_, Oct 29 2008 *)"
			],
			"xref": [
				"Row sums A003149.",
				"Cf. A003991, A098358, A098359, A098360."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "Douglas Stones (dssto1(AT)student.monash.edu.au), Sep 04 2004",
			"references": 7,
			"revision": 38,
			"time": "2020-07-10T10:17:39-04:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}