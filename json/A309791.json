{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A309791",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 309791,
			"data": "1,9,3,15,6,78,24,132,51,699,213,1185,456,6288,1914,10662,4101,56589,17223,95955,36906,509298,155004,863592,332151,4583679,1395033,7772325,2989356,41253108,12555294,69950922,26904201,371277969,112997643,629558295,242137806,3341501718,1016978784,5666024652",
			"name": "Expansion of (1 + 8*x - 6*x^2 + 12*x^3 - 18*x^4)/(1 - x - 9*x^4 + 9*x^5).",
			"comment": [
				"This sequence and its companion A309792 describe the additive constants which occur in an infinite series of maps from the row indices in the table defined by A307048 to the arithmetic progression contained in a specific column of that table. Only rows with indices of the form 6*j - 2 are concerned, and j is mapped to the unique term in that row (cf. example).",
				"Conjecture: Any finite subset of these maps can build chains of finite length only."
			],
			"link": [
				"\u003ca href=\"https://oeis.org/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,0,0,9,-9)."
			],
			"formula": [
				"a(n) = (1/48)*(-32+2^(n/2)*(42*(1+(-1)^n)-2*(-i)^n+105*sqrt(2)*(1-(-1)^n)+11*i*(-i)^n*sqrt(2)-i^(n+1)*(-2*i+11*sqrt(2)))), where i=sqrt(-1). - _Stefano Spezia_, Aug 19 2019"
			],
			"example": [
				"The maps for k \u003e= 0 start with:",
				"   3*k +  1 -\u003e  8*k +  2 ( 4-\u003e10,   7-\u003e18,  10-\u003e26, ...)",
				"   9*k +  9 -\u003e  8*k +  8 ( 9-\u003e 8,  18-\u003e16,  27-\u003e24, ...)",
				"   9*k +  3 -\u003e 16*k +  5 ( 3-\u003e 5,  12-\u003e21,  21-\u003e37, ...)",
				"  27*k + 15 -\u003e 16*k +  9 (15-\u003e 9,  42-\u003e25,  69-\u003e41, ...)",
				"  27*k +  6 -\u003e 32*k +  7 ( 6-\u003e 7,  33-\u003e39,  60-\u003e71, ...)",
				"  81*k + 78 -\u003e 32*k + 31 (78-\u003e31, 159-\u003e63, 240-\u003e95, ...)",
				"          ^            ^",
				"          |            |",
				"          A309791      A309792",
				"Chains:",
				"   33 -\u003e  39 -\u003e  69 -\u003e  41",
				"  114 -\u003e 135 -\u003e 120 -\u003e 213 -\u003e 75 -\u003e 133"
			],
			"mathematica": [
				"LinearRecurrence[{1, 0, 0, 9, -9}, {1, 9, 3, 15, 6}, 32] (* or *) CoefficientList[Series[(1 + 8*x - 6*x^2 + 12*x^3 - 18*x^4)/(1 - x - 9*x^4 + 9*x^5), {x, 0, 40}], x]"
			],
			"xref": [
				"Cf. A307048, A309792."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Georg Fischer_, Aug 17 2019",
			"references": 2,
			"revision": 18,
			"time": "2020-10-26T16:11:29-04:00",
			"created": "2019-08-20T02:26:17-04:00"
		}
	]
}