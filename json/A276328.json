{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A276328",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 276328,
			"data": "0,1,2,3,1,2,3,4,2,3,4,5,3,4,5,6,4,5,1,2,3,4,2,3,4,5,3,4,5,6,4,5,6,7,5,6,2,3,4,5,3,4,5,6,4,5,6,7,5,6,7,8,6,7,3,4,5,6,4,5,6,7,5,6,7,8,6,7,8,9,7,8,4,5,6,7,5,6,7,8,6,7,8,9,7,8,9,10,8,9,5,6,7,8,6,7,1",
			"name": "Digit sum when n is expressed in greedy A001563-base (A276326).",
			"comment": [
				"a(n) is the number of terms of A001563 needed to sum to n using the greedy algorithm.",
				"This seems to give also the minimal number of terms of A001563 that sum to n (checked empirically up to n=3265920), but it would be nice to know for sure whether this holds for all n."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A276328/b276328.txt\"\u003eTable of n, a(n) for n = 0..4320\u003c/a\u003e"
			],
			"formula": [
				"a(0) = 0; for n \u003e= 1, a(n) = 1 + a(n-A258199(n)).",
				"a(0) = 0; for n \u003e= 1, a(n) = A276333(n) + a(A276335(n)).",
				"Other identities and observations. For all n \u003e= 0:",
				"a(A276091(n)) = A000120(n).",
				"a(n) \u003e= A276337(n).",
				"It also seems that a(n) \u003c= A276332(n) for all n."
			],
			"example": [
				"For n=1, the largest term of A001563 \u003c= 1 is A001563(1) = 1, thus a(1) = 1.",
				"For n=2, the largest term of A001563 \u003c= 2 is A001563(1) = 1, thus a(2) = 1 + a(2-1) = 2.",
				"For n=18, the largest term of A001563 \u003c= 18 is A001563(3) = 18, thus a(18) = 1.",
				"For n=20, the largest term of A001563 \u003c= 20 is A001563(3) = 18, thus a(20) = 1 + a(20-18) = 3.",
				"For n=36, the largest term of A001563 \u003c= 36 is A001563(3) = 18, thus a(36) = 1 + a(18) = 2."
			],
			"mathematica": [
				"f[n_] := Block[{a = {{0, n}}}, Do[AppendTo[a, {First@ #, Last@ #} \u0026@ QuotientRemainder[a[[-1, -1]], (# #!) \u0026[# - i]]], {i, 0, # - 1}] \u0026@NestWhile[# + 1 \u0026, 0, (# #!) \u0026[# + 1] \u003c= n \u0026]; Rest[a][[All, 1]]]; {0}~Join~Table[Total@ f@ n, {n, 120}] (* _Michael De Vlieger_, Aug 31 2016 *)"
			],
			"program": [
				"(Scheme, with memoization-macro definec, two versions)",
				"(definec (A276328 n) (if (zero? n) n (+ 1 (A276328 (- n (A258199 n))))))",
				"(definec (A276328 n) (if (zero? n) n (+ (A276333 n) (A276328 (A276335 n)))))",
				";; Third version which explicitly searches a minimal representation of n as a sum of terms of A001563 (but please see the comment):",
				"(definec (A276328_by_minimization n) (if (zero? n) n (let loop ((i (A258198 n)) (m #f)) (cond ((zero? i) m) ((not m) (loop (- i 1) (+ 1 (A276328_by_minimization (- n (A001563 i)))))) (else (loop (- i 1) (min m (+ 1 (A276328_by_minimization (- n (A001563 i)))))))))))"
			],
			"xref": [
				"Cf. A001563, A258199.",
				"Cf. A000120, A276329, A276330, A276332, A276333, A276335.",
				"Cf. A276091 (gives all n for which a(n) = A276337(n)).",
				"Cf. also A007895, A034968, A265744, A265745 for similar sequences."
			],
			"keyword": "nonn,base",
			"offset": "0,3",
			"author": "_Antti Karttunen_, Aug 30 2016",
			"references": 7,
			"revision": 29,
			"time": "2021-05-20T10:15:26-04:00",
			"created": "2016-09-01T10:54:38-04:00"
		}
	]
}