{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A252040",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 252040,
			"data": "2,2,4,2,3418801,0,64,2,4,0,21611482313284249,0,38580382095460899155325996786287338133521,0,0,2,2908327322588596409721563830760333292497745890881,0,366742604046618684582110328334808145525757868881,0,0,0,264638735999480827156185738343112840094453729311824181089",
			"name": "Least number k such that the sum of the n-th powers of divisors of k is prime, or 0 if there is no such k.",
			"comment": [
				"a(n) = 2 if and only if 2^n + 1 is in A019434.",
				"From _Jinyuan Wang_, Jan 30 2021: (Start)",
				"a(n) = 0 if n \u003e 1 is not a prime power. Proof: note that sigma_n(k) = Product_{i=1..m} (1 + p_i^n + ... + p_i^(n*e_i)), where k = Product_{i=1..m} p_i^e_i. We only need to prove when n \u003e 1 is not a prime power and e \u003e 1, s = Sum_{i=0..e-1} p^(n*i) = (p^(n*e) - 1)/(p^n - 1) is composite. If e is prime, then s is divisible by (p^(e^(t+1)) - 1)/(p^(e^t) - 1), where t is the e-adic valuation of n. If e is composite, then s is divisible by (p^(n*q) - 1)/(p^n - 1), where q is a prime factor of e.",
				"Corollary: k must be of the form p^(e - 1) when n = e^t, where p and e are primes. Therefore, a(2^t) = 0 if 2^2^t + 1 is composite. (End)"
			],
			"link": [
				"Jinyuan Wang, \u003ca href=\"/A252040/b252040.txt\"\u003eTable of n, a(n) for n = 1..50\u003c/a\u003e"
			],
			"example": [
				"2 has two divisors, 2 and 1. 2^3 + 1^3 = 9 is not prime.",
				"3 has two divisors, 3 and 1. 3^3 + 1^3 = 28 is not prime.",
				"4 has three divisors, 4, 2, and 1. 4^3 + 2^3 + 1^3 = 73 is prime. So, a(3) = 4."
			],
			"mathematica": [
				"a252040[n_Integer] := If[PrimePowerQ[n] \u0026\u0026 (p=First@ First@ FactorInteger[n])\u003e2, q=2; While[!PrimeQ[DivisorSigma[n, q^(p-1)]], q=NextPrime[q]]; q^(p-1), 2*Boole[PrimeQ[2^n+1]]]; a252040 /@ Range[10] (* _Michael De Vlieger_, Dec 13 2014 *) (* modified by _Jinyuan Wang_, Jan 30 2021 *)"
			],
			"program": [
				"(PARI) a(n) = if(isprimepower(n, \u0026p) \u0026\u0026 p\u003e2, my(q=2); while(!ispseudoprime(sigma(q^(p-1), n)), q=nextprime(q+1)); q^(p-1), 2*isprime(2^n+1)); \\\\ Modified by _Jinyuan Wang_, Jan 25 2021"
			],
			"xref": [
				"Cf. A000203, A001157, A001158, A001159, A001160, A019434.",
				"Cf. A023194 (sigma(n) is prime), A063783 (sigma_3(n) is prime)."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Derek Orr_, Dec 12 2014",
			"ext": [
				"Name edited by and more terms from _Jinyuan Wang_, Jan 30 2021"
			],
			"references": 2,
			"revision": 38,
			"time": "2021-02-11T02:41:36-05:00",
			"created": "2015-01-15T10:33:07-05:00"
		}
	]
}