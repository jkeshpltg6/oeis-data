{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A321201",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 321201,
			"data": "1,0,0,1,2,0,1,1,0,2,3,0,2,1,1,2,4,0,0,3,3,1,2,2,5,0,1,3,4,1,0,4,3,2,6,0,2,3,5,1,1,4,4,2,7,0,0,5,3,3,6,1,2,4,5,2,8,0,1,5,4,3,7,1,0,6,3,4,6,2,9,0,2,5,5,3,8,1,1,6,4,4,7,2,10,0",
			"name": "Irregular triangle T with the nontrivial solutions of 2*e2 + 3*e3 = n, for n \u003e= 2, with nonnegative e2 and e3, ordered as pairs with increasing e2 values.",
			"comment": [
				"The length of row n is 2*A(n), with A(n) = A008615(n+2) for n \u003e= 2: 2*[1, 1, 1, 1, 2, 1, 2, 2, 2, 2, 3, 2, 3, 3, 3, 3, 4, 3, 4, ...].",
				"The trivial solution for n = 0 is [0, 0]. There is no solution for n = 1.",
				"The row sums are given in A321202.",
				"If a partition of n with parts 2 or 3 (with inclusive or) is written as 2^{e2} 3^{e3}, where e2 and e3 are nonnegative numbers, then in row n, all pairs [e2, e3] are given, for n \u003e= 2, ordered with increasing values of e2.",
				"The corresponding irregular triangle with the multinomial numbers n!/((n - (e2 + e3)!*e2!*e3!) is given in A321203. It gives the coefficients of x^n = x^{2*{e2} + 3*{e3}} of  (1 + x^2 + x^3)^n, for n \u003e= 2."
			],
			"formula": [
				"T(n, k) gives all pairs [e2, e3] solving 2*e2 + 3*e3 = n, ordered with increasing value of e2, for n \u003e= 2. The trivial solution [0, 0] for n = 0 is not recorded. There is no solution for n = 1."
			],
			"example": [
				"The triangle T(n, k) begins (pairs are separated by commas):",
				"n\\k  0  1   2  3   4  5   6  7 ...",
				"2:   1  0",
				"3:   0  1",
				"4:   2  0",
				"5:   1  1",
				"6:   0  2,  3  0",
				"7:   2  1",
				"8:   1  2,  4  0",
				"9:   0  3,  3  1",
				"10:  2  2,  5  0",
				"11:  1  3,  4  1",
				"12:  0  4,  3  2,  6  0",
				"13:  2  3,  5  1,",
				"14:  1  4,  4  2,  7  0",
				"15:  0  5,  3  3,  6  1",
				"16:  2  4,  5  2,  8  0",
				"17:  1  5,  4  3,  7  1",
				"18:  0  6,  3  4,  6  2,  9  0",
				"19:  2  5,  5  3,  8  1",
				"20:  1  6,  4  4,  7  2, 10  0",
				"...",
				"n=8: the two solutions of 2*e2 + 3*e3 = 8 are [e2, e3] = [1, 2] and = [4, 0], and 1 \u003c 4, therefore row 8 is 1  2  4  0, with a comma after the first pair."
			],
			"mathematica": [
				"row[n_] := Reap[Do[If[2 e2 + 3 e3 == n, Sow[{e2, e3}]], {e2, 0, n/2}, {e3, 0, n/3}]][[2, 1]];",
				"Table[row[n], {n, 2, 20}] // Flatten (* _Jean-François Alcover_, Nov 23 2018 *)"
			],
			"xref": [
				"Cf. A008615, A321202, A321203."
			],
			"keyword": "nonn,tabf",
			"offset": "2,5",
			"author": "_Wolfdieter Lang_, Nov 05 2018",
			"ext": [
				"Missing row 2 inserted by _Jean-François Alcover_, Nov 23 2018"
			],
			"references": 7,
			"revision": 9,
			"time": "2018-11-23T03:18:33-05:00",
			"created": "2018-11-09T21:52:57-05:00"
		}
	]
}