{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A111184",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 111184,
			"data": "1,0,1,0,2,1,0,6,6,1,0,24,34,12,1,0,120,210,110,20,1,0,720,1452,974,270,30,1,0,5040,11256,8946,3248,560,42,1,0,40320,97296,87504,38338,8792,1036,56,1",
			"name": "Triangle T(n,k), 0\u003c=k\u003c=n, read by rows, given by [0, 2, 1, 3, 2, 4, 3, 5, 4, 6, 5, 7, 6, ...] DELTA [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, ...] where DELTA is the operator defined in A084938.",
			"link": [
				"Paul Barry, \u003ca href=\"https://arxiv.org/abs/1804.06801\"\u003eA note on number triangles that are almost their own production matrix\u003c/a\u003e, arXiv:1804.06801 [math.CO], 2018.",
				"R. Cori, \u003ca href=\"http://dx.doi.org/10.1016/j.jcta.2009.02.008\"\u003eIndecomposable permutations, hypermaps and labeled Dyck paths\u003c/a\u003e, J. Comb. Theory A 116 (2009) 1326-1343, end of Section 1.2.2."
			],
			"formula": [
				"O.g.f. satisfies: A(x,y) = (1 + x^2*A'(x,y)) / (1+x - x*y - x*A(x,y)), where A'(x,y) = d/dx A(x,y). [_Paul D. Hanna_, Jul 31 2011]",
				"O.g.f. satisfies: A(x,y) = 1 - x * d/dx log(1+x - x*y - x*A(x,y)). [_Paul D. Hanna_, Jul 30 2011]",
				"Sum_{k, 0\u003c=k\u003c=n} T(n, k) = A003319(n+1).",
				"Sum_{k, 0\u003c=k\u003c=n} T(n, k)*2^(n-k) = A004208(n)."
			],
			"example": [
				"Rows begin:",
				"1;",
				"0, 1;",
				"0, 2, 1;",
				"0, 6, 6, 1;",
				"0, 24, 34, 12, 1;",
				"0, 120, 210, 110, 20, 1;",
				"0, 720, 1452, 974, 270, 30, 1;",
				"0, 5040, 11256, 8946, 3248, 560, 42, 1;",
				"0, 40320, 97296, 87504, 38338, 8792, 1036, 56, 1."
			],
			"mathematica": [
				"DELTA[r_, s_, m_] := Module[{p, q, t, x, y}, q[k_] := x r[[k+1]] + y s[[k+1]]; p[0, _] = 1; p[_, -1] = 0; p[n_ /; n \u003e= 1, k_ /; k \u003e= 0] := p[n, k] = p[n, k-1] + q[k] p[n-1, k+1] // Expand; t[n_, k_] := Coefficient[p[n, 0], x^(n-k) y^k]; t[0, 0] = p[0, 0]; Table[t[n, k], {n, 0, m}, {k, 0, n}]];",
				"DELTA[LinearRecurrence[{1, 1, -1}, {0, 2, 1}, 10], Mod[Range[10], 2], 10] // Flatten (* _Jean-François Alcover_, Jul 27 2018 *)"
			],
			"program": [
				"(PARI) {T(n,k)=local(A=1+x*y);for(i=1,n,A=1-x*deriv(log(1+x-x*y-x*A +x*O(x^n))));polcoeff(polcoeff(A,n,x),k,y)} /* _Paul D. Hanna_ */",
				"(PARI) {T(n, k)=local(A=1+x*y); for(i=1, n, A=(1 + x^2*A')/(1 + x - x*y - x*A +x*O(x^n))); polcoeff(polcoeff(A, n, x), k, y)} /* _Paul D. Hanna_ */",
				"/* Print 10 Rows of the triangle: */",
				"for(n=0,10,for(k=0,n,print1(T(n,k),\",\"));print(\"\"))"
			],
			"xref": [
				"Cf. A003319, A004208."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Philippe Deléham_ and _Paul D. Hanna_, Oct 16 2005",
			"references": 2,
			"revision": 35,
			"time": "2018-08-07T12:15:43-04:00",
			"created": "2006-01-24T03:00:00-05:00"
		}
	]
}