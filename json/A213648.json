{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A213648",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 213648,
			"data": "2,3,5,4,11,7,5,11,14,9,11,6,23,19,11,8,11,17,29,7,29,23,11,24,20,35,23,13,59,29,23,19,8,39,11,18,17,27,29,19,23,43,29,59,23,15,11,55,74,35,41,26,35,9,23,35,41,57,59,14,29,23,47,34,59,67",
			"name": "The minimum number of 1's in the relation n*[n,1,1,...,1,n] = [x,...,x] between simple continued fractions.",
			"comment": [
				"Multiplying n by a simple continued fraction with an increasing number of 1's sandwiched between n generates fractions that have a leading term x in their continued fraction, where x is obviously \u003e n^2. We increase the number of 1's until the first and the last term in the simple terminating continued fraction of n*[n,1,...,1,n] =[x,...,x] is the same, x, and set a(n) to the count of these 1's.",
				"Conjecture: the fixed points of this sequence are in A000057.",
				"We have [n,1,1,...,1,n] = n + (n*Fib(m)+Fib(m-1))/(n*Fib(m+1)+Fib(m)) and n*[n,1,1,...,1,n] = n^2 + 1 + (n^2-n-1)*Fib(m)/(n*Fib(m+1)+Fib(m)), where m is the number of 1's. - _Max Alekseyev_, Aug 09 2012",
				"The analog sequence with 11 instead of 1, A213900, seems to have the same fixed points, while other variants (A262212 - A262220, A262211) have other fixed points (A213891 - A213899,  A261311). - _M. F. Hasler_, Sep 15 2015"
			],
			"reference": [
				"A. Hurwitz, Über die Kettenbrüche, deren Teilnenner arithmetische Reihen bilden, Vierteljahrsschrift der Naturforschenden Gesellschaft in Zürich, Jahrg XLI, 1896, Jubelband II, S. 34-64."
			],
			"link": [
				"Bill Gosper, \u003ca href=\"http://perl.plover.com/yak/cftalk/INFO/gosper.txt\"\u003eAppendix 2 Continued Fraction Arithmetic\u003c/a\u003e"
			],
			"formula": [
				"Conjecture: a(n)=A001177(n)-1."
			],
			"example": [
				"3* [3,1,1,1,3] = [10,1,10],so a(3)=3",
				"4* [4,1,1,1,1,1,4] = [18,2,18],so a(4)=5",
				"5* [5,1,1,1,1,5] = [28,28],so a(5)=4",
				"6* [6,1,1,1,1,1,1,1,1,1,1,1,6] = [39,1,2,2,2,1,39], so a(6)=11",
				"7* [7,1,1,1,1,1,1,1,7] = [53,3,53], so a(7)=7"
			],
			"maple": [
				"A213648 := proc(n)",
				"        local h,ins,c ;",
				"        for ins from 1 do",
				"                c := [n,seq(1,i=1..ins),n] ;",
				"                h := numtheory[cfrac](n*simpcf(c),quotients) ;",
				"                if op(1,h) = op(-1,h) then",
				"                        return ins;",
				"                end if;",
				"        end do:",
				"end proc: # _R. J. Mathar_, Jul 06 2012"
			],
			"mathematica": [
				"f[m_, n_] := Block[{c, k = 1}, c[x_, y_] := ContinuedFraction[x FromContinuedFraction[Join[{x}, Table[m, {y}], {x}]]]; While[First@ c[n, k] != Last@ c[n, k], k++]; k]; f[1, #] \u0026 /@ Range[2, 67] (* _Michael De Vlieger_, Sep 16 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = local(t, m=1); if( n\u003c2, 0, while( t = contfracpnqn( concat( [n, vector(m, i, 1 ), n])), t = contfrac( n * t[1, 1] / t[2, 1]); if( t[1] \u003c n^2 || t[#t] \u003c n^2, m++, break)); m)} /* _Michael Somos_, Jun 17 2012 */",
				"(PARI) {a(n) = local(t, m=0); if( n\u003c2, 0, until(t[1]==t[#t], m++; t = contfrac(n^2 + 1 + (n^2-n-1)*fibonacci(m)/(n*fibonacci(m+1)+fibonacci(m))); ); m )} /* _Max Alekseyev_, Aug 09 2012 */"
			],
			"xref": [
				"Cf. A000057, A262212 - A262220."
			],
			"keyword": "nonn",
			"offset": "2,1",
			"author": "_Art DuPre_, Jun 17 2012",
			"references": 24,
			"revision": 46,
			"time": "2015-09-17T04:40:04-04:00",
			"created": "2012-07-07T08:51:30-04:00"
		}
	]
}