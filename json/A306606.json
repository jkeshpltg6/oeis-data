{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A306606",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 306606,
			"data": "1,2,2,3,4,3,3,3,3,6,5,4,7,5,2,5,5,6,6,3,4,5,1,2,5,6,5,5,6,3,3,2,2,6,5,3,9,7,3,6,4,5,8,3,6,7,2,4,7,6,8,9,9,6,6,5,3,9,9,6,11,7,4,10,5,6,13,5,5,9,2,5,7,7,7,10,7,3,5,3,3,6,6,5,11,6,6,9,4,6,11,3,5,9,2,5,4,4,7,11",
			"name": "Number of ways to write n as w^2 + T(x)^2 + Pen(y)^2 + 2*Pen(z)^2, where w,x,y,z are integers with w \u003e 0 and x \u003e= 0, and T(m) = m*(m+1)/2 and Pen(m) = m*(3m-1)/2.",
			"comment": [
				"Conjecture: a(n) \u003e 0 for all n \u003e 0. In other words, any positive integer n can be written as s^2 + t^2 + u^2 + 2*v^2, where s is a positive integer, t is a triangular number, u and v are generalized pentagonal numbers.",
				"I have verified a(n) \u003e 0 for all n = 1..5*10^6.",
				"It is well known that any positive odd integer can be written as x^2 + y^2 + 2*z^2 with x,y,z integers.",
				"See also A306614 for similar conjectures."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A306606/b306606.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1701.05868\"\u003eRestricted sums of four squares\u003c/a\u003e, arXiv:1701.05868 [math.NT], 2017."
			],
			"example": [
				"a(1) = 1 with 1 = 1^2 + T(0)^2 + Pen(0)^2 + 2*Pen(0)^2.",
				"a(23) = 1 with 23 = 4^2 + T(1)^2 + Pen(-1)^2 + 2*Pen(1)^2.",
				"a(335) = 1 with 335 = 18^2 + T(2)^2 + Pen(0)^2 + 2*Pen(1)^2.",
				"a(3695) = 1 with 3695 = 53^2 + T(7)^2 + Pen(-1)^2 + 2*Pen(-2)^2."
			],
			"mathematica": [
				"t[x_]:=t[x]=x(x+1)/2; p[x_]:=p[x]=x(3x-1)/2;",
				"SQ[n_]:=SQ[n]=IntegerQ[Sqrt[n]];",
				"tab={};Do[r=0;Do[If[SQ[n-t[x]^2-p[y]^2-2*p[z]^2],r=r+1],{x,0,(Sqrt[8*Sqrt[n-1]+1]-1)/2},{y,-Floor[(Sqrt[24*Sqrt[n-1-t[x]^2]+1]-1)/6],(Sqrt[24*Sqrt[n-1-t[x]^2]+1]+1)/6},",
				"{z,-Floor[(Sqrt[24*Sqrt[(n-1-t[x]^2-p[y]^2)/2]+1]-1)/6],(Sqrt[24*Sqrt[(n-1-t[x]^2-p[y]^2)/2]+1]+1)/6}];tab=Append[tab,r],{n,1,100}];Print[tab]"
			],
			"xref": [
				"Cf. A000217, A000290, A001318, A275344, A306614."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Zhi-Wei Sun_, Feb 28 2019",
			"references": 2,
			"revision": 14,
			"time": "2019-03-11T17:21:12-04:00",
			"created": "2019-03-11T17:21:12-04:00"
		}
	]
}