{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A325000",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 325000,
			"data": "1,3,1,6,4,1,10,10,5,1,15,20,15,6,1,21,35,35,21,7,1,28,56,70,56,28,8,1,36,84,126,126,84,36,9,1,45,120,210,252,210,120,45,10,1,55,165,330,462,462,330,165,55,11,1,66,220,495,792,924,792,495,220,66,12,1",
			"name": "Array read by descending antidiagonals: T(n,k) is the number of unoriented colorings of the facets (or vertices) of a regular n-dimensional simplex using up to k colors.",
			"comment": [
				"For n=1, the figure is a line segment with two vertices. For n=2, the figure is a triangle with three edges. For n=3, the figure is a tetrahedron with four triangular faces. The Schläfli symbol, {3,...,3}, of the regular n-dimensional simplex consists of n-1 threes. Each of its n+1 facets is a regular (n-1)-dimensional simplex. Two unoriented colorings are the same if congruent; chiral pairs are counted as one.",
				"Note that antidiagonals are part of rows of the Pascal triangle.",
				"T(n,k-n) is the number of chiral pairs of colorings of the facets (or vertices) of a regular n-dimensional simplex using k or fewer colors. - _Robert A. Russell_, Sep 28 2020"
			],
			"link": [
				"Robert A. Russell, \u003ca href=\"/A325000/b325000.txt\"\u003eTable of n, a(n) for n = 1..1275\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = binomial(n+k,n+1) = A007318(n+k,n+1).",
				"T(n,k) = Sum_{j=1..n+1} A007318(n,j-1) * binomial(k,j).",
				"T(n,k) = A324999(n,k) + T(n,k-n) = (A324999(n,k) - A325001(n,k)) / 2 = T(n,k-n) + A325001(n,k). - _Robert A. Russell_, Sep 28 2020",
				"G.f. for row n: x / (1-x)^(n+2).",
				"Linear recurrence for row n: T(n,k) = Sum_{j=1..n+2} -binomial(j-n-3,j) * T(n,k-j).",
				"G.f. for column k: (1 - (1-x)^k) / (x * (1-x)^k) - k.",
				"T(n,k-n) = A324999(n,k) - T(n,k) = (A324999(n,k) - A325001(n,k)) / 2 = T(n,k) - A325001(n,k). - _Robert A. Russell_, Oct 10 2020"
			],
			"example": [
				"The array begins with T(1,1):",
				"  1  3  6  10  15   21   28    36    45    55    66     78     91    105 ...",
				"  1  4 10  20  35   56   84   120   165   220   286    364    455    560 ...",
				"  1  5 15  35  70  126  210   330   495   715  1001   1365   1820   2380 ...",
				"  1  6 21  56 126  252  462   792  1287  2002  3003   4368   6188   8568 ...",
				"  1  7 28  84 210  462  924  1716  3003  5005  8008  12376  18564  27132 ...",
				"  1  8 36 120 330  792 1716  3432  6435 11440 19448  31824  50388  77520 ...",
				"  1  9 45 165 495 1287 3003  6435 12870 24310 43758  75582 125970 203490 ...",
				"  1 10 55 220 715 2002 5005 11440 24310 48620 92378 167960 293930 497420 ...",
				"  ...",
				"For T(1,2) = 3, the two achiral colorings use just one of the two colors for both vertices; the chiral pair uses two colors. For T(2,2)=4, the triangle may have 0, 1, 2, or 3 edges of one color."
			],
			"mathematica": [
				"Table[Binomial[d+1,n+1], {d,1,15}, {n,1,d}] // Flatten"
			],
			"xref": [
				"Cf. A324999 (oriented), A325001 (achiral).",
				"Unoriented: A007318(n,k-1) (exactly k colors), A327084 (edges, ridges), A337884 (faces, peaks), A325005 (orthotope facets, orthoplex vertices), A325013 (orthoplex facets, orthotope vertices).",
				"Chiral: A327085 (edges, ridges), A337885 (faces, peaks), A325006 (orthotope facets, orthoplex vertices), A325014 (orthoplex facets, orthotope vertices).",
				"Cf. A104712 (same sequence for a triangle; same sequence apart from offset).",
				"Rows 1-4 are A000217, A000292, A000332(n+3), A000389(n+4). - _Robert A. Russell_, Sep 28 2020"
			],
			"keyword": "nonn,tabl,easy",
			"offset": "1,2",
			"author": "_Robert A. Russell_, Mar 23 2019",
			"references": 12,
			"revision": 26,
			"time": "2020-10-21T03:48:50-04:00",
			"created": "2019-05-25T11:43:05-04:00"
		}
	]
}