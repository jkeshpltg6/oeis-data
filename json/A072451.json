{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A072451",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 72451,
			"data": "1,1,2,3,3,5,6,4,8,9,6,11,10,9,14,15,10,12,18,12,20,21,12,23,21,16,26,20,18,29,30,18,24,33,22,35,36,20,30,39,27,41,32,28,44,36,30,36,48,30,50,51,24,53,54,36,56,44,36,48,55,40,50,63,42,65,54,36,68,69,46,60,56",
			"name": "Number of odd terms in the reduced residue system of 2*n-1.",
			"comment": [
				"Given the quasi-order and coach theorems of Hilton and Pedersen (Cf. A003558): phi(b) = 2 * k * c with b odd. Dividing through by 2 we obtain phi(b)/2 = k * c which is the same as a(n) = A003558(n-1) * A135303(n-1). Here k refers to the \"least exponent\" (Cf. A003558), while c is the number of coaches for odd b (Cf. A135303). - _Gary W. Adamson_, Sep 25 2012",
				"After the initial term, this is the totient function phi(2n-1)/2 (A037225(n-1)/2). Also a(n) is the number of partitions of the odd number (2n+1) into two ordered relatively prime parts. If p and q are such parts where p \u003e q and p+q = 2n+1 then they can generate primitive Pythagorean triples of the form (p^2-q^2, 2pq, p^2+q^2). - _Frank M Jackson_, Oct 30 2012"
			],
			"reference": [
				"P. Hilton and J. Pedersen, A Mathematical Tapestry, Demonstrating the Beautiful Unity of Mathematics, Cambridge University Press, 2010, p. 200."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A072451/b072451.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"a(1) = 1, and for n\u003e1 a(n) = phi(2n-1)/2. - _Benoit Cloitre_, Oct 11 2002",
				"It would appear that a(n) = Sum_{k=0..n} abs(Jacobi(k, 2n-2k+1)). - _Paul Barry_, Jul 20 2005",
				"a(n) = A055034(2*n-1), n \u003e= 1. - _Wolfdieter Lang_, Feb 07 2020"
			],
			"example": [
				"n=105: phi(105)=48 with 24 odd, 24 even; for even n=2k reduced residue system consists only of odd terms, so number of odd terms equals phi(n).",
				"With odd integer 33, A072451(17) = 10 = A003558(16) * A135303(16); or 10 = 5 * 2."
			],
			"maple": [
				"A072451 := n -\u003e ceil(numtheory:-phi(2*n-1)/2):",
				"seq(A072451(n), n=1..73); # _Peter Luschny_, Feb 24 2020"
			],
			"mathematica": [
				"gw[x_] := Table[GCD[x, w], {w, 1, x}] rrs[x_] := Flatten[Position[gw[x], 1]] Table[Count[OddQ[rrs[2*w-1]], True], {w, 1, 128}]",
				"(* Additional programs: *)",
				"Table[Count[Range[1, #, 2], k_ /; CoprimeQ[k, #]] \u0026[2 n - 1], {n, 73}] (* or *) Array[If[# == 1, #, EulerPhi[2 # - 1]/2] \u0026, 73] (* _Michael De Vlieger_, Jul 24 2017 *)"
			],
			"program": [
				"(PARI) A072451(n) = if(1==n,n,eulerphi(n+n-1)/2); \\\\ (After _Benoit Cloitre_'s formula) - _Antti Karttunen_, Jul 24 2017"
			],
			"xref": [
				"Cf. A000010, A037225, A003558, A135303, A055034."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Labos Elemer_, Jun 19 2002",
			"references": 9,
			"revision": 35,
			"time": "2020-02-24T09:21:34-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}