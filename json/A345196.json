{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A345196",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 345196,
			"data": "1,1,0,1,1,1,1,3,4,4,4,8,11,11,11,20,27,29,31,48,65,70,74,109,145,160,172,238,314,345,372,500,649,721,782,1019,1307,1451,1577,2015,2552,2841,3098,3885,4867,5418,5914,7318,9071,10109,11050",
			"name": "Number of integer partitions of n with reverse-alternating sum equal to the reverse-alternating sum of their conjugate.",
			"comment": [
				"The reverse-alternating sum of a partition (y_1,...,y_k) is Sum_i (-1)^(k-i) y_i. This is equal to (-1)^(m-1) times the number of odd parts in the conjugate partition, where m is the number of parts. By conjugation, this is also (-1)^(r-1) times the number of odd parts, where r is the greatest part. So a(n) is the number of integer partitions of n of even rank with the same number of odd parts as their conjugate."
			],
			"example": [
				"The a(5) = 1 through a(12) = 11 partitions:",
				"  (311)  (321)  (43)    (44)    (333)    (541)    (65)      (66)",
				"                (2221)  (332)   (531)    (4321)   (4322)    (552)",
				"                (4111)  (2222)  (32211)  (32221)  (4331)    (4332)",
				"                        (4211)  (51111)  (52111)  (4421)    (4422)",
				"                                                  (6311)    (4431)",
				"                                                  (222221)  (6411)",
				"                                                  (422111)  (33222)",
				"                                                  (611111)  (53211)",
				"                                                            (222222)",
				"                                                            (422211)",
				"                                                            (621111)"
			],
			"mathematica": [
				"sats[y_]:=Sum[(-1)^(i-Length[y])*y[[i]],{i,Length[y]}];",
				"conj[y_]:=If[Length[y]==0,y,Table[Length[Select[y,#\u003e=k\u0026]],{k,1,Max[y]}]];",
				"Table[Length[Select[IntegerPartitions[n],sats[#]==sats[conj[#]]\u0026]],{n,0,15}]"
			],
			"xref": [
				"The non-reverse version is A277103.",
				"Comparing even parts to odd conjugate parts gives A277579.",
				"Comparing signs only gives A340601.",
				"A000041 counts partitions of 2n with alternating sum 0, ranked by A000290.",
				"A103919 counts partitions by sum and alternating sum (reverse: A344612).",
				"A120452 counts partitions of 2n with rev-alt sum 2 (negative: A344741).",
				"A124754 gives alternating sums of standard compositions (reverse: A344618).",
				"A316524 is the alternating sum of the prime indices of n (reverse: A344616).",
				"A325534 counts separable partitions, ranked by A335433.",
				"A325535 counts inseparable partitions, ranked by A335448.",
				"A344610 counts partitions by sum and positive reverse-alternating sum.",
				"A344611 counts partitions of 2n with reverse-alternating sum \u003e= 0.",
				"Cf. A000070, A000097, A006330, A027187, A027193, A236559, A239829, A257991, A344607, A344608, A344651, A344654."
			],
			"keyword": "nonn",
			"offset": "0,8",
			"author": "_Gus Wiseman_, Jun 26 2021",
			"references": 5,
			"revision": 8,
			"time": "2021-06-27T07:52:21-04:00",
			"created": "2021-06-27T07:52:21-04:00"
		}
	]
}