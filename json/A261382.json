{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A261382",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 261382,
			"data": "2,2510,15,30,5,510,730,440,195,6230,2040,2760,20,1010,12570,31340,1625,1650,725,2480,2160,520,1055,60,5,20,1260,25800,6185,6240,10,1180,12600,7500,5330,390,325,2880,11655,32670,5850,43110,3230,1470,7680,4950,255,202650,10530,450,2445,11670,8745,103350,80,6890,135,18930,80,245040",
			"name": "Least positive integer k such that (k-1)^2+(k*n)^2, k^2+(k*n-1)^2, (k+1)^2+(k*n)^2 and k^2+(k*n+1)^2 are all prime.",
			"comment": [
				"Conjecture: a(n) exists for any n \u003e 0. In general, any positive rational number r can be written as m/n, where m and n are positive integers with (m-1)^2+n^2, m^2+(n-1)^2, (m+1)^2+n^2 and m^2+(n+1)^2 all prime.",
				"It is easy to prove that if m and n are positive integers with (m-1)^2+n^2, m^2+(n-1)^2, (m+1)^2+n^2 and m^2+(n+1)^2 all prime, then either m = n = 2 or m == n == 0 (mod 5)."
			],
			"reference": [
				"Zhi-Wei Sun, Problems on combinatorial properties of primes, in: M. Kaneko, S. Kanemitsu and J. Liu (eds.), Number Theory: Plowing and Starring through High Wave Forms, Proc. 7th China-Japan Seminar (Fukuoka, Oct. 28 - Nov. 1, 2013), Ser. Number Theory Appl., Vol. 11, World Sci., Singapore, 2015, pp. 169-187."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A261382/b261382.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"/A261382/a261382.txt\"\u003eChecking the conjecture for r = a/b with a,b = 1..60\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1402.6641\"\u003eProblems on combinatorial properties of primes\u003c/a\u003e, arXiv:1402.6641 [math.NT], 2014."
			],
			"example": [
				"a(2) = 2510 since (2510-1)^2+(2510*2)^2 = 31495481, 2510^2+(2510*2-1)^2 = 31490461, (2510+1)^2+(2510*2)^2 = 31505521 and 2510^2+(2510*2+1)^2 = 31510541 are all prime."
			],
			"mathematica": [
				"PQ[p_]:=PrimeQ[p]",
				"q[m_,n_]:=PQ[(m-1)^2+n^2]\u0026\u0026PQ[m^2+(n-1)^2]\u0026\u0026PQ[(m+1)^2+n^2]\u0026\u0026PQ[m^2+(n+1)^2]",
				"Do[k=0;Label[bb];k=k+1;If[q[k,k*n],Goto[aa],Goto[bb]];Label[aa];Print[n,\" \", k];Continue,{n,1,60}]"
			],
			"program": [
				"(PARI) is_ok(k,n)=isprime((k-1)^2+(k*n)^2)\u0026\u0026isprime(k^2+(k*n-1)^2)\u0026\u0026isprime((k+1)^2+(k*n)^2)\u0026\u0026isprime(k^2+(k*n+1)^2)",
				"first(m)=my(v=vector(m),k=1);for(i=1,m,while(!is_ok(k,i),k++);v[i]=k;k++;);v; \\\\ _Anders Hellström_, Aug 17 2015"
			],
			"xref": [
				"Cf. A000040, A000290, A236193, A259492, A261339."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Zhi-Wei Sun_, Aug 17 2015",
			"references": 1,
			"revision": 11,
			"time": "2015-08-17T14:29:38-04:00",
			"created": "2015-08-17T14:29:38-04:00"
		}
	]
}