{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002494",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2494,
			"id": "M1762 N0699",
			"data": "1,0,1,2,7,23,122,888,11302,262322,11730500,1006992696,164072174728,50336940195360,29003653625867536,31397431814147073280,63969589218557753586160,245871863137828405125824848,1787331789281458167615194471072,24636021675399858912682459613241920",
			"name": "Number of n-node graphs without isolated nodes.",
			"comment": [
				"Number of unlabeled simple graphs covering n vertices. - _Gus Wiseman_, Aug 02 2018"
			],
			"reference": [
				"F. Harary, Graph Theory. Addison-Wesley, Reading, MA, 1969, p. 214.",
				"W. L. Kocay, Some new methods in reconstruction theory, Combinatorial Mathematics IX, 952 (1982) 89--114. [From _Benoit Jubin_, Sep 06 2008]",
				"W. L. Kocay, On reconstructing spanning subgraphs, Ars Combinatoria, 11 (1981) 301--313. [From _Benoit Jubin_, Sep 06 2008]",
				"J. H. Redfield, The theory of group-reduced distributions, Amer. J. Math., 49 (1927), 433-435; reprinted in P. A. MacMahon, Coll. Papers I, pp. 805-827.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A002494/b002494.txt\"\u003eTable of n, a(n) for n=0..75\u003c/a\u003e (using A000088)",
				"P. C. Fishburn and W. V. Gehrlein, \u003ca href=\"https://doi.org/10.1002/jgt.3190160204\"\u003eNiche numbers\u003c/a\u003e, J. Graph Theory, 16 (1992), 131-139.",
				"J. H. Redfield, \u003ca href=\"/A002494/a002494.pdf\"\u003eThe theory of group-reduced distributions\u003c/a\u003e [Annotated scan of pages 452 and 453 only]",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/IsolatedPoint.html\"\u003eIsolated Point.\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/GraphicalPartition.html\"\u003eGraphical Partition\u003c/a\u003e",
				"Gus Wiseman, \u003ca href=\"/A048143/a048143_4.txt\"\u003eSequences enumerating clutters, antichains, hypertrees, and hyperforests, organized by labeling, spanning, and allowance of singletons\u003c/a\u003e."
			],
			"formula": [
				"O.g.f.: (1-x)*G(x) where G(x) is o.g.f. for A000088. - _Geoffrey Critzer_, Apr 14 2012"
			],
			"example": [
				"From _Gus Wiseman_, Aug 02 2018: (Start)",
				"Non-isomorphic representatives of the a(4) = 7 graphs:",
				"  (12)(34)",
				"  (12)(13)(14)",
				"  (12)(13)(24)",
				"  (12)(13)(14)(23)",
				"  (12)(13)(24)(34)",
				"  (12)(13)(14)(23)(24)",
				"  (12)(13)(14)(23)(24)(34)",
				"(End)"
			],
			"maple": [
				"b:= proc(n, i, l) `if`(n=0 or i=1, 1/n!*2^((p-\u003e add(ceil((p[j]-1)/2)",
				"      +add(igcd(p[k], p[j]), k=1..j-1), j=1..nops(p)))([l[], 1$n])),",
				"       add(b(n-i*j, i-1, [l[], i$j])/j!/i^j, j=0..n/i))",
				"    end:",
				"a:= n-\u003e b(n$2, [])-`if`(n\u003e0, b(n-1$2, []), 0):",
				"seq(a(n), n=0..20);  # _Alois P. Heinz_, Aug 14 2019"
			],
			"mathematica": [
				"\u003c\u003c MathWorld`Graphs`",
				"Length /@ (gp = Select[ #, GraphicalPartitionQ] \u0026 /@",
				"Graphs /@ Range[9])",
				"nn = 20; g = Sum[NumberOfGraphs[n] x^n, {n, 0, nn}]; CoefficientList[Series[ g (1 - x), {x, 0, nn}], x]  (*Geoffrey Critzer, Apr 14 2012*)",
				"sysnorm[m_]:=If[Union@@m!=Range[Max@@Flatten[m]],sysnorm[m/.Rule@@@Table[{(Union@@m)[[i]],i},{i,Length[Union@@m]}]],First[Sort[sysnorm[m,1]]]];",
				"sysnorm[m_,aft_]:=If[Length[Union@@m]\u003c=aft,{m},With[{mx=Table[Count[m,i,{2}],{i,Select[Union@@m,#\u003e=aft\u0026]}]},Union@@(sysnorm[#,aft+1]\u0026/@Union[Table[Map[Sort,m/.{par+aft-1-\u003eaft,aft-\u003epar+aft-1},{0,1}],{par,First/@Position[mx,Max[mx]]}]])]];",
				"Table[Length[Union[sysnorm/@Select[Subsets[Select[Subsets[Range[n]],Length[#]==2\u0026]],Union@@#==Range[n]\u0026]]],{n,6}] (* _Gus Wiseman_, Aug 02 2018 *)",
				"b[n_, i_, l_] := If[n==0 || i==1, 1/n!*2^(Function[p, Sum[Ceiling[(p[[j]]-1)/2] + Sum[GCD[p[[k]], p[[j]]], {k, 1, j-1}], {j, 1, Length[p]}]][Join[l, Table[1, {n}]]]), Sum[b[n-i*j, i-1, Join[l, Table[i, {j}]]]/j!/i^j, {j, 0, n/i}]];",
				"a[n_] := b[n, n, {}] - If[n \u003e 0, b[n-1, n-1, {}], 0];",
				"a /@ Range[0, 20] (* _Jean-François Alcover_, Dec 03 2019, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Equals first differences of A000088. Cf. A006129.",
				"Cf. also A006647, A006648, A006649, A006650, A006651.",
				"Cf. A000612, A001187, A055621, A304998."
			],
			"keyword": "nonn,nice",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_.",
			"ext": [
				"More terms from _Vladeta Jovovic_, Apr 10 2000",
				"a(0) added from _David W. Wilson_, Aug 24 2008"
			],
			"references": 28,
			"revision": 51,
			"time": "2019-12-03T10:37:43-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}