{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A128764",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 128764,
			"data": "1,1,0,1,1,1,1,1,2,2,2,2,3,2,2,4,4,4,4,5,6,6,6,7,9,9,10,12,12,13,14,16,18,19,20,23,26,26,28,30,33,37,38,42,46,49,52,56,62,65,70,76,84,89,92,101,110,117,123,133,145,153,162,174,188,197,208,227",
			"name": "Expansion of chi(q) / chi(q^13) in powers of q where chi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A128764/b128764.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(-1/2) * eta(q^2)^2 * eta(q^13) * eta(q^52) / (eta(q) * eta(q^4) * eta(q^26)^2) in powers of q.",
				"Given g.f. A(x), then B(q) = q * A(q^2) satisfies 0 = f(B(q), B(q^3)) where f(u, v) = (u - v^3) * (u^3 - v) - 3*u*v * (u^2 + v^2 - u*v).",
				"Euler transform of period 52 sequence [ 1, -1, 1, 0, 1, -1, 1, 0, 1, -1, 1, 0, 0, -1, 1, 0, 1, -1, 1, 0, 1, -1, 1, 0, 1, 0, 1, 0, 1, -1, 1, 0, 1, -1, 1, 0, 1, -1, 0, 0, 1, -1, 1, 0, 1, -1, 1, 0, 1, -1, 1, 0, ...].",
				"G.f.: Product_{k\u003e0} (1 + x^k) * (1 + x^(26*k)) / ( (1 + x^(2*k)) * (1 + x^(13 k)) ).",
				"a(n) ~ exp(Pi*sqrt(2*n/13)) / (2^(5/4) * 13^(1/4) * n^(3/4)). - _Vaclav Kotesovec_, Sep 08 2015"
			],
			"example": [
				"G.f. = 1 + x + x^3 + x^4 + x^5 + x^6 + x^7 + 2*x^8 + 2*x^9 + 2*x^10 + 2*x^11 + ...",
				"G.f. = q + q^3 + q^7 + q^9 + q^11 + q^13 + q^15 + 2*q^17 + 2*q^19 + 2*q^21 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ (QPochhammer[ x^13, -x^13] / QPochhammer[ x, -x]), {x, 0, n}]; (* _Michael Somos_, Apr 26 2015 *)",
				"nmax = 40; CoefficientList[Series[Product[(1 + x^k) * (1 + x^(26*k)) / ( (1 + x^(2*k)) * (1 + x^(13*k)) ), {k, 1, nmax}], {x, 0, nmax}], x] (* _Vaclav Kotesovec_, Sep 08 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A)^2 * eta(x^13 + A) * eta(x^52 + A) / (eta(x + A) * eta(x^4 + A) * eta(x^26 + A)^2), n))};"
			],
			"keyword": "nonn",
			"offset": "0,9",
			"author": "_Michael Somos_, Mar 25 2007",
			"references": 1,
			"revision": 19,
			"time": "2021-03-12T22:24:44-05:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}