{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A050385",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 50385,
			"data": "1,1,3,10,39,160,691,3081,14095,65757,311695,1496833,7266979,35608419,175875537,874698246,4376646808,22016578909,111282845162,564886771380,2878498888625,14719219809915,75505990358779,388451973679785",
			"name": "Reversion of Moebius function A008683.",
			"comment": [
				"From _David W. Wilson_, May 17 2017: (Start)",
				"Appears to equal the number of ways to partition Z into n residue classes. For example, a(4) = 10 since we can partition Z into 4 residue classes in 10 ways:",
				"Z = ∪ {0(mod 2), 1(mod 4), 3(mod 8), 7(mod 8)}",
				"Z = ∪ {0(mod 2), 3(mod 4), 1(mod 8), 5(mod 8)}",
				"Z = ∪ {0(mod 2), 1(mod 6), 3(mod 6), 5(mod 6)}",
				"Z = ∪ {1(mod 2), 0(mod 4), 2(mod 8), 6(mod 8)}",
				"Z = ∪ {1(mod 2), 2(mod 4), 0(mod 8), 4(mod 8)}",
				"Z = ∪ {1(mod 2), 0(mod 6), 2(mod 6), 4(mod 6)}",
				"Z = ∪ {0(mod 3), 1(mod 3), 2(mod 6), 5(mod 6)}",
				"Z = ∪ {0(mod 3), 2(mod 3), 1(mod 6), 4(mod 6)}",
				"Z = ∪ {1(mod 3), 2(mod 3), 0(mod 6), 3(mod 6)}",
				"Z = ∪ {0(mod 4), 1(mod 4), 2(mod 4), 3(mod 4)}",
				"(End)",
				"Unfortunately this conjecture is not correct; it fails at a(13). - _Jeffrey Shallit_, Nov 19 2017",
				"The correct statement is that a(n) is the number of \"natural exact covering systems\" of cardinality n. These are covering systems (like the ones in _David W. Wilson_'s comment) that are obtained by starting with the size-1 system x == 0 (mod 1) and successively choosing a congruence and \"splitting\" it into r \u003e= 2 new congruences. - _Jeffrey Shallit_, Dec 07 2017"
			],
			"link": [
				"David W. Wilson, \u003ca href=\"/A050385/b050385.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"I. P. Goulden, L. B. Richmond, and J. Shallit, \u003ca href=\"https://arxiv.org/abs/1711.04109\"\u003eNatural exact covering systems and the reversion of the Möbius series\u003c/a\u003e, arXiv:1711.04109 [math.NT], 2017-2018.",
				"N. J. A. Sloane, \u003ca href=\"/transforms.txt\"\u003eTransforms\u003c/a\u003e",
				"\u003ca href=\"/index/Res#revert\"\u003eIndex entries for reversions of series\u003c/a\u003e"
			],
			"formula": [
				"G.f. A(x) satisfies: A(x) = x - Sum_{k\u003e=2} mu(k) * A(x)^k. - _Ilya Gutkovskiy_, Apr 22 2020"
			],
			"mathematica": [
				"InverseSeries[Sum[MoebiusMu[n] x^n, {n, 0, 25}] + O[x]^25] // CoefficientList[#, x]\u0026 // Rest (* _Jean-François Alcover_, Sep 29 2018 *)"
			],
			"xref": [
				"Cf. A008683, A050386, A294672, A295162."
			],
			"keyword": "nonn,nice",
			"offset": "1,3",
			"author": "_Christian G. Bower_, Nov 15 1999",
			"references": 8,
			"revision": 71,
			"time": "2020-04-22T07:59:01-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}