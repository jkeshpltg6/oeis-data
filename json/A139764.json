{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A139764",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 139764,
			"data": "1,2,3,1,5,1,2,8,1,2,3,1,13,1,2,3,1,5,1,2,21,1,2,3,1,5,1,2,8,1,2,3,1,34,1,2,3,1,5,1,2,8,1,2,3,1,13,1,2,3,1,5,1,2,55,1,2,3,1,5,1,2,8,1,2,3,1,13,1,2,3,1,5,1,2,21,1,2,3,1,5,1,2,8,1,2,3,1,89",
			"name": "Smallest term in Zeckendorf representation of n.",
			"comment": [
				"Also called a \"Fibonacci fractal\".",
				"Appears to be the same as the \"ruler of Fibonaccis\" mentioned by Knuth. - _N. J. A. Sloane_, Aug 03 2012",
				"a(n) is also the number of matches to take away to win in a certain match game (see Rocher et al.).",
				"The frequencies of occurrences of the values in this sequence and A035614 are related by the golden ratio."
			],
			"reference": [
				"D. E. Knuth, The Art of Computer Programming, Vol. 4A, Section 7.1.3, p. 82, solution to Problem 179. - From _N. J. A. Sloane_, Aug 03 2012"
			],
			"link": [
				"Steve Witham, \u003ca href=\"/A139764/b139764.txt\"\u003eTable of n, a(n) for n = 1..9999\u003c/a\u003e",
				"Alex Bogomolny, \u003ca href=\"http://www.cut-the-knot.org/Curriculum/Games/TakeAway.shtml#theory\"\u003eTheory of Take-Away Games\u003c/a\u003e",
				"Sylvain Rocher, Elodie Privat, Laurent Orban, Alexandre Mothe and Laurent Thouy, \u003ca href=\"http://mathematiques.ac-bordeaux.fr/elv/clubs/mej/mej2005/allumettes.pdf\"\u003eLa stratégie des allumettes\u003c/a\u003e",
				"A. J. Schwenk, \u003ca href=\"http://www.fq.math.ca/Scanned/8-3/schwenk-a.pdf\"\u003eTake-Away Games\u003c/a\u003e, The Fibonacci Quarterly, v 8, no 3 (1970), 225-234.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/WythoffArray.html\"\u003eWythoff Array\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Zeckendorf\u0026#39;s_theorem\"\u003eZeckendorf's theorem\u003c/a\u003e"
			],
			"formula": [
				"a(n) = n if n is a Fibonacci number, else a( n - (largest Fibonacci number \u003c n) ).",
				"a(n) = the value of the (exactly one) digit that turns on between the Fibonacci-base representations of n-1 and n. E.g., from 6 (1001) to 7 (1010), the two's digit turns on.",
				"a(n) = top element of the column of the Wythoff array that contains n.",
				"a(n) = A000045(A035614(n-1) + 2). [Offsets made precise by _Peter Munn_, Apr 13 2021]",
				"a(n) = A035517(n,0). - _Reinhard Zumkeller_, Mar 10 2013"
			],
			"example": [
				"The Zeckendorf representation of 7 = 5 + 2, so a(7) = 2."
			],
			"maple": [
				"A000045 := proc(n) combinat[fibonacci](n) ; end:",
				"A087172 := proc(n)",
				"local a,i ;",
				"a := 0 ;",
				"for i from 0 do",
				"if A000045(i) \u003c= n then",
				"a := A000045(i) ;",
				"else",
				"RETURN(a) ;",
				"fi ;",
				"od:",
				"end:",
				"A139764 := proc(n)",
				"local nResid,prevF ;",
				"nResid := n ;",
				"while true do",
				"prevF := A087172(nResid) ;",
				"if prevF = nResid then",
				"RETURN(prevF) ;",
				"else",
				"nResid := nResid-prevF ;",
				"fi ;",
				"od:",
				"end:",
				"seq(A139764(n),n=1..120) ;",
				"# _R. J. Mathar_, May 22 2008"
			],
			"mathematica": [
				"f[n_] := (k = 1; ff = {}; While[(fi = Fibonacci[k]) \u003c= n, AppendTo[ff, fi]; k++]; Drop[ff, 1]); a[n_] := First[ If[n == 0, 0, r = n; s = {}; fr = f[n]; While[r \u003e 0, lf = Last[fr]; If[lf \u003c= r, r = r - lf; PrependTo[s, lf]]; fr = Drop[fr, -1]]; s]]; Table[a[n], {n, 1, 89}] (* _Jean-François Alcover_, Nov 02 2011 *)"
			],
			"program": [
				"(PARI) a(n)=my(f);forstep(k=log(n*sqrt(5))\\log(1.61803)+2, 2, -1, f=fibonacci(k);if(f\u003c=n,n-=f;if(!n,return(f));k--)) \\\\ _Charles R Greathouse IV_, Nov 02 2011",
				"(Haskell)",
				"a139764 = head . a035517_row  -- _Reinhard Zumkeller_, Mar 10 2013"
			],
			"xref": [
				"Cf. A000045, A035614, A107017, A014417, A006519.",
				"Cf. A087172."
			],
			"keyword": "nonn,nice",
			"offset": "1,2",
			"author": "Steve Witham (sw(AT)tiac.net), May 15 2008",
			"ext": [
				"More terms from _T. D. Noe_ and _R. J. Mathar_, May 22 2008"
			],
			"references": 8,
			"revision": 47,
			"time": "2021-05-01T17:52:00-04:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}