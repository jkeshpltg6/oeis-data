{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A220852",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 220852,
			"data": "7,-37,19899,-235225,268989175,-4985687133,1052143756587,-25075299330081,71491170131441775,-1979286926244381325,319756423353994489291,-9700423363591011143001,5919065321069316557189503,-189993537046726536185033125",
			"name": "Numerators of the fraction (30*n+7) * binomial(2*n,n)^2 * 2F1([1/2 - n/2, -n/2], [1], 64)/(-256)^n, where 2F1 is the hypergeometric function.",
			"comment": [
				"The Gaussian hypergeometric function 2F1() is a polynomial in n because at least one of the \"numerators\" is a negative integer. 2F1( [(1-n)/2,-n/2], [1], 64) = A098441(n). - _R. J. Mathar_, Jan 09 2013"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A220852/b220852.txt\"\u003eTable of n, a(n) for n = 0..460\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"https://arxiv.org/abs/1102.5649\"\u003eList of conjectural series for powers of Pi and other constants\u003c/a\u003e, arXiv:1102.5649 [math.CA], 2011-2014; Conjecture I1 page 24.",
				"Zhi-Wei Sun, \u003ca href=\"https://arxiv.org/abs/1101.0600\"\u003eOn sums related to central binomial and trinomial coefficients\u003c/a\u003e, arXiv:1101.0600 [math.NT], 2011-2014."
			],
			"formula": [
				"Sum_{n\u003e=0} a(n)/A220853(n) = 24/Pi.",
				"More directly, Sum_{k\u003e=0} (30*k+7) * binomial(2k,k)^2 * (Hypergeometric2F1[1/2 - k/2, -k/2, 1,64])/(-256)^k = 24/Pi.",
				"Another version of this identity is Sum_{k\u003e=0} (30*k+7) * binomial(2k,k)^2 * (Sum_{m=0..k/2} binomial(k-m,m) * binomial(k,m) * 16^m)/(-256)^k."
			],
			"maple": [
				"A220852 := proc(n)",
				"    hypergeom([1/2-n/2,-n/2],[1],64) ;",
				"    simplify(%) ;",
				"    (30*n+7)*binomial(2*n,n)^2*%/(-256)^n ;",
				"    numer(%) ;",
				"end proc: # _R. J. Mathar_, Jan 09 2013"
			],
			"mathematica": [
				"Numerator[Table[(30*n + 7)*Binomial[2*n, n]^2* Hypergeometric2F1[(1 - n)/2, -n/2, 1, 64]/(-256)^n, {n, 0, 50}]] (* _G. C. Greubel_, Feb 20 2017 *)"
			],
			"xref": [
				"Cf. A098441, A132714, A220853."
			],
			"keyword": "sign,frac",
			"offset": "0,1",
			"author": "_Alexander R. Povolotsky_, Dec 23 2012",
			"ext": [
				"_R. J. Mathar_'s comment and data corrected by _G. C. Greubel_, Feb 20 2017"
			],
			"references": 2,
			"revision": 60,
			"time": "2021-06-27T07:59:24-04:00",
			"created": "2013-01-25T12:39:18-05:00"
		}
	]
}