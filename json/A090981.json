{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A090981",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 90981,
			"data": "1,1,1,1,4,1,1,11,9,1,1,26,46,16,1,1,57,180,130,25,1,1,120,603,750,295,36,1,1,247,1827,3507,2345,581,49,1,1,502,5164,14224,14518,6076,1036,64,1,1,1013,13878,52068,75558,48006,13776,1716,81,1,1,2036,35905,176430",
			"name": "Triangle read by rows: T(n,k) = number of Schroeder paths of length 2n and having k ascents.",
			"comment": [
				"A Schroeder path is a lattice path in the first quadrant, from the origin to a point on the x-axis and consisting of steps U=(1,1), D=(1,-1) and H=(2,0) of length 2n and having k ascents (i.e., maximal strings of (1,1) steps).",
				"Row sums give A006318 (the large Schroeder numbers). Column 1 gives A000295 (the Eulerian numbers).",
				"Another version of the triangle T(n,k), 0\u003c=k\u003c=n, read by rows; given by [1, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, ...] DELTA [0, 1, 0, 1, 0, 1, 0, 1, 0, 1, ...] = 1; 1, 0; 1, 1, 0; 1, 4, 1, 0; 1, 11, 9, 1, 0; ..., where DELTA is the operator defined in A084938. - _Philippe Deléham_, Jun 14 2004",
				"The expected number of ascents in a Schroeder n-path is asymptotically (sqrt(2)-1)*n for large n. (Previously formulated as conjecture by _David Callan_, Jul 25 2008, now proven.) - _Valerie Roitner_, Aug 06 2020"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A090981/b090981.txt\"\u003eRows n=0..100 of triangle, flattened\u003c/a\u003e",
				"L. Ferrari, E. Munarini, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Ferrari/ferrari.html\"\u003eEnumeration of Edges in Some Lattices of Paths\u003c/a\u003e, J. Int. Seq. 17 (2014) #14.1.5.",
				"Zhicong Lin, Dongsu Kim, \u003ca href=\"https://arxiv.org/abs/1612.02964\"\u003eA sextuple equidistribution arising in Pattern Avoidance\u003c/a\u003e, arXiv:1612.02964 [math.CO], 2016.",
				"Valerie Roitner, \u003ca href=\"https://arxiv.org/abs/2008.02240\"\u003eThe vectorial kernel method for walks wíth longer steps\u003c/a\u003e, arXiv:2008.02240 [math.CO], 2020."
			],
			"formula": [
				"T(n, k) = binomial(n+1, k)*Sum_{j=0..n-k} (binomial(n+1, j)*binomial(n-j-1, k-1)/(n+1).",
				"G.f.: G=G(t, z) satisfies z*(1-z+t*z)G^2 - (1-t*z)G + 1 = 0."
			],
			"example": [
				"T(2,1)=4 because we have the following four Schroeder paths of length 4 with one ascent: (U)HD, (UU)DD, H(U)D and (U)DH (ascents shown between parentheses).",
				"Triangle starts:",
				"  1;",
				"  1,   1;",
				"  1,   4,   1;",
				"  1,  11,   9,   1;",
				"  1,  26,  46,  16,   1;",
				"  1,  57, 180, 130,  25,  1;",
				"  ..."
			],
			"maple": [
				"T := (n,k)-\u003ebinomial(n+1,k)*add(binomial(n+1,j)*binomial(n-j-1,k-1),j=0..n-k)/(n+1): seq(seq(T(n,k),k=0..n),n=0..12);"
			],
			"mathematica": [
				"m = 11(*rows*); G = 0; Do[G = Series[(1+G^2 z(1+(t-1)z))/(1-t z), {t, 0, m-1}, {z, 0, m-1}] // Normal // Expand, {m}]; CoefficientList[#, t]\u0026 /@ CoefficientList[G, z]//Flatten (* _Jean-François Alcover_, Jan 22 2019 *)",
				"Table[Binomial[n+1,k]*Sum[Binomial[n+1,j]*Binomial[n-j-1,k-1], {j,0,n-k}]/(n+1), {n,0,12}, {k,0,n}]//Flatten (* _G. C. Greubel_, Feb 02 2019 *)"
			],
			"program": [
				"(PARI) {T(n,k) = if(k==0, 1, binomial(n+1,k)*sum(j=0,n-k, binomial(n+1,j) *binomial(n- j-1, k-1))/(n+1))};",
				"for(n=0,12, for(k=0,n, print1(T(n,k), \", \"))) \\\\ _G. C. Greubel_, Feb 02 2019",
				"(MAGMA) [[k le 0 select 1 else Binomial(n+1,k)*(\u0026+[Binomial(n+1,j)* Binomial(n-j-1,k-1): j in [0..n-k]])/(n+1): k in [0..n]]: n in [0..12]]; // _G. C. Greubel_, Feb 02 2019",
				"(Sage) [[1] + [binomial(n+1,k)*sum(binomial(n+1,j)*binomial(n-j-1,k-1) for j in (0..n-k))/(n+1) for k in (1..n)] for n in (0..12)] # _G. C. Greubel_, Feb 02 2019"
			],
			"xref": [
				"Cf. A000295, A006318."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Emeric Deutsch_, Feb 29 2004",
			"references": 2,
			"revision": 35,
			"time": "2020-08-30T19:31:41-04:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}