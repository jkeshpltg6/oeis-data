{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A321400",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 321400,
			"data": "1,0,1,0,1,1,0,1,1,1,0,2,2,1,1,0,5,8,2,1,1,0,16,40,10,2,1,1,0,61,256,70,10,2,1,1,0,272,1952,656,75,10,2,1,1,0,1385,17408,7442,816,75,10,2,1,1,0,7936,177280,99280,11407,832,75,10,2,1,1",
			"name": "A family of sequences converging to the exponential limit of sec + tan (A320956). Square array A(n, k) for n \u003e= 0 and k \u003e= 0, read by descending antidiagonals.",
			"comment": [
				"See the comments and definitions in A320956. Note also the corresponding construction for the exp function in A320955."
			],
			"example": [
				"Array starts:",
				"n\\k   0  1  2   3   4    5      6       7        8  ...",
				"-------------------------------------------------------",
				"  [0] 1, 0, 0,  0,  0,   0,     0,      0,       0, ... A000007",
				"  [1] 1, 1, 1,  2,  5,  16,    61,    272,    1385, ... A000111",
				"  [2] 1, 1, 2,  8, 40, 256,  1952,  17408,  177280, ... A000828",
				"  [3] 1, 1, 2, 10, 70, 656,  7442,  99280, 1515190, ... A320957",
				"  [4] 1, 1, 2, 10, 75, 816, 11407, 194480, 3871075, ... A321394",
				"  [5] 1, 1, 2, 10, 75, 832, 12322, 232560, 5325325, ...",
				"  [6] 1, 1, 2, 10, 75, 832, 12383, 238272, 5693735, ...",
				"  [7] 1, 1, 2, 10, 75, 832, 12383, 238544, 5732515, ...",
				"  [8] 1, 1, 2, 10, 75, 832, 12383, 238544, 5733900, ...",
				"-------------------------------------------------------",
				"Seen as a triangle given by descending antidiagonals:",
				"  [0] 1",
				"  [1] 0,  1",
				"  [2] 0,  1,   1",
				"  [3] 0,  1,   1,  1",
				"  [4] 0,  2,   2,  1,  1",
				"  [5] 0,  5,   8,  2,  1, 1",
				"  [6] 0, 16,  40, 10,  2, 1, 1",
				"  [7] 0, 61, 256, 70, 10, 2, 1, 1"
			],
			"maple": [
				"sf := proc(n) option remember; `if`(n \u003c= 1, 1-n, (n-1)*(sf(n-1) + sf(n-2))) end:",
				"kernel := proc(n, k) option remember; binomial(n, k)*sf(k) end:",
				"egf := n -\u003e add(kernel(n, k)*((tan + sec)(x*(n - k))), k=0..n):",
				"A321400Row := proc(n, len) series(egf(n), x, len + 2):",
				"seq(coeff(%, x, k)*k!/n!, k=0..len) end:",
				"seq(lprint(A321400Row(n, 9)), n=0..9);"
			],
			"xref": [
				"Cf. A000111 (n=1), A000828 (n=2), A320957 (n=3), A321394 (n=4), A320956 (limit).",
				"Antidiagonal sums (and row sums of the triangle): A321399."
			],
			"keyword": "nonn,tabl",
			"offset": "0,12",
			"author": "_Peter Luschny_, Nov 08 2018",
			"references": 1,
			"revision": 7,
			"time": "2018-11-08T18:06:31-05:00",
			"created": "2018-11-08T18:06:31-05:00"
		}
	]
}