{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A125127",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 125127,
			"data": "1,1,1,1,3,1,1,3,4,1,1,3,7,7,1,1,3,7,11,11,1,1,3,7,15,21,18,1,1,3,7,15,26,39,29,1,1,3,7,15,31,51,71,47,1,1,3,7,15,31,57,99,131,76,1,1,3,7,15,31,63,113,191,241,123,1",
			"name": "Array L(k,n) read by antidiagonals: k-step Lucas numbers.",
			"link": [
				"Freddy Barrera, \u003ca href=\"/A125127/b125127.txt\"\u003eTable of n, a(n) for n = 1..5050\u003c/a\u003e",
				"C. A. Charalambides, \u003ca href=\"http://www.fq.math.ca/Scanned/29-4/charalambides.pdf\"\u003eLucas numbers and polynomials of order k and the length of the longest circular success run\u003c/a\u003e, The Fibonacci Quarterly, 29 (1991), 290-297.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Lucasn-StepNumber.html\"\u003eLucas n-Step Number\u003c/a\u003e"
			],
			"formula": [
				"L(k,n) = L(k,n-1) + L(k,n-2) + ... + L(k,n-k); L(k,n) = -1 for n \u003c 0, and L(k,0) = k.",
				"G.f. for row k: x*(dB(k,x)/dx)/(1-B(k,x)), where B(k,x) = x + x^2 + ... + x^k. - _Petros Hadjicostas_, Jan 24 2019"
			],
			"example": [
				"Table begins:",
				"1 | 1  1  1   1   1   1    1    1    1    1",
				"2 | 1  3  4   7  11  18   29   47   76  123",
				"3 | 1  3  7  11  21  39   71  131  241  443",
				"4 | 1  3  7  15  26  51   99  191  367  708",
				"5 | 1  3  7  15  31  57  113  223  439  863",
				"6 | 1  3  7  15  31  63  120  239  475  943",
				"7 | 1  3  7  15  31  63  127  247  493  983",
				"8 | 1  3  7  15  31  63  127  255  502 1003",
				"9 | 1  3  7  15  31  63  127  255  511 1013"
			],
			"program": [
				"(Sage)",
				"def L(k, n):",
				"    if n \u003c 0:",
				"        return -1",
				"    a = [-1]*(k-1) + [k] # [-1, -1, ..., -1, k]",
				"    for i in range(1, n+1):",
				"        a[:] = a[1:] + [sum(a)]",
				"    return a[-1]",
				"[L(k, n) for d in (1..12) for k, n in zip((d..1, step=-1), (1..d))] # _Freddy Barrera_, Jan 10 2019"
			],
			"xref": [
				"n-step Lucas number analog of A092921 Array F(k, n) read by antidiagonals: k-generalized Fibonacci numbers (and see related A048887, A048888). L(1, n) = \"1-step Lucas numbers\" = A000012. L(2, n) = 2-step Lucas numbers = A000204. L(3, n) = 3-step Lucas numbers = A001644. L(4, n) = 4-step Lucas numbers = A001648 Tetranacci numbers A073817 without the leading term 4. L(5, n) = 5-step Lucas numbers = A074048 Pentanacci numbers with initial conditions a(0)=5, a(1)=1, a(2)=3, a(3)=7, a(4)=15. L(6, n) = 6-step Lucas numbers = A074584 Esanacci (\"6-anacci\") numbers. L(7, n) = 7-step Lucas numbers = A104621 Heptanacci-Lucas numbers. L(8, n) = 8-step Lucas numbers = A105754. L(9, n) = 9-step Lucas numbers = A105755. See A000295, A125129 for comments on partial sums of diagonals.",
				"Cf. A000012, A000032, A000204, A001644, A001648, A048887, A048888, A074048, A074584, A092921, A104621, A105754, A105755, A125129."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "1,5",
			"author": "_Jonathan Vos Post_, Nov 21 2006",
			"ext": [
				"Corrected by _Freddy Barrera_, Jan 10 2019"
			],
			"references": 7,
			"revision": 28,
			"time": "2019-01-25T08:30:51-05:00",
			"created": "2006-12-06T03:00:00-05:00"
		}
	]
}