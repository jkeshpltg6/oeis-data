{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A008300",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 8300,
			"data": "1,1,1,1,2,1,1,6,6,1,1,24,90,24,1,1,120,2040,2040,120,1,1,720,67950,297200,67950,720,1,1,5040,3110940,68938800,68938800,3110940,5040,1,1,40320,187530840,24046189440,116963796250,24046189440,187530840,40320,1,1,362880,14398171200,12025780892160,315031400802720,315031400802720,12025780892160,14398171200,362880,1",
			"name": "Triangle read by rows: T(n,k) (n \u003e= 0, 0 \u003c= k \u003c= n) gives number of {0,1} n X n matrices with all row and column sums equal to k.",
			"comment": [
				"Or, triangle of multipermutation numbers T(n,k), n \u003e= 0, 0 \u003c= k \u003c= n: number of relations on an n-set such that all vertical sections and all horizontal sections have k elements."
			],
			"reference": [
				"L. Comtet, Advanced Combinatorics, Reidel, 1974, p. 236, P(n,k)."
			],
			"link": [
				"Brendan D. McKay, \u003ca href=\"/A008300/b008300.txt\"\u003eRows n = 0..30, flattened\u003c/a\u003e",
				"C. J. Everett and P. R. Stein, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(71)90007-0\"\u003eThe asymptotic number of integer stochastic matrices\u003c/a\u003e, Disc. Math. 1 (1971), 55-72.",
				"Richard J. Mathar, \u003ca href=\"https://arxiv.org/abs/1903.12477\"\u003e2-regular Digraphs of the Lovelock Lagrangian\u003c/a\u003e, arXiv:1903.12477 [math.GM], 2019.",
				"B. D. McKay, \u003ca href=\"http://users.cecs.anu.edu.au/~bdm/papers/LabelledEnumeration.pdf\"\u003eApplications of a technique for labeled enumeration\u003c/a\u003e, Congress. Numerantium, 40 (1983), 207-221.",
				"Brendan D. McKay, \u003ca href=\"http://users.cecs.anu.edu.au/~bdm/data/Bvals.txt\"\u003efirst 30 rows : entries named Bv[n,k,n,k]\u003c/a\u003e",
				"Wouter Meeussen, \u003ca href=\"/A008300/a008300.txt\"\u003erelevant entries from B. D. McKay reference\u003c/a\u003e"
			],
			"formula": [
				"Comtet quotes Everett and Stein as showing that T(n,k) ~ (kn)!(k!)^(-2n) exp( -(k-1)^2/2 ) for fixed k as n -\u003e oo.",
				"T(n,k) = T(n,n-k)."
			],
			"example": [
				"Triangle begins:",
				"1;",
				"1,    1;",
				"1,    2,       1;",
				"1,    6,       6,        1;",
				"1,   24,      90,       24,        1;",
				"1,  120,    2040,     2040,      120,       1;",
				"1,  720,   67950,   297200,    67950,     720,    1;",
				"1, 5040, 3110940, 68938800, 68938800, 3110940, 5040, 1;"
			],
			"program": [
				"(PARI)",
				"T(n, k)={",
				"  local(M=Map(Mat([n, 1])));",
				"  my(acc(p, v)=my(z); mapput(M, p, if(mapisdefined(M, p, \u0026z), z+v, v)));",
				"  my(recurse(i, p, v, e) = if(i\u003c0, if(!e, acc(p, v)), my(t=polcoef(p,i)); for(j=0, min(t, e), self()(i-1, p+j*(x-1)*x^i, binomial(t, j)*v, e-j))));",
				"  for(r=1, n, my(src=Mat(M)); M=Map(); for(i=1, matsize(src)[1], recurse(k-1, src[i, 1], src[i, 2], k))); vecsum(Mat(M)[,2]);",
				"} \\\\ _Andrew Howroyd_, Apr 03 2020"
			],
			"xref": [
				"Row sums give A067209.",
				"Central coefficients are A058527.",
				"Cf. A000142 (column 1), A001499 (column 2), A001501 (column 3), A058528 (column 4), A075754 (column 5), A172544 (column 6), A172541 (column 7).",
				"Cf. A133687, A333157."
			],
			"keyword": "tabl,nonn,nice",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Greg Kuperberg_, Feb 08 2001"
			],
			"references": 24,
			"revision": 53,
			"time": "2020-04-03T21:04:31-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}