{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A253240",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 253240,
			"data": "1,1,-1,1,0,1,1,1,2,1,1,2,3,3,1,1,3,4,7,2,1,1,4,5,13,5,5,1,1,5,6,21,10,31,1,1,1,6,7,31,17,121,3,7,1,1,7,8,43,26,341,7,127,2,1,1,8,9,57,37,781,13,1093,17,3,1,1,9,10,73,50,1555,21,5461,82,73,1,1,1,10,11,91,65,2801,31,19531,257,757,11,11,1,1,11,12,111,82,4681,43,55987,626,4161,61,2047,1,1",
			"name": "Square array read by antidiagonals: T(m, n) = Phi_m(n), the m-th cyclotomic polynomial at x=n.",
			"comment": [
				"Outside of rows 0, 1, 2 and columns 0, 1, only terms of A206942 occur.",
				"Conjecture: There are infinitely many primes in every row (except row 0) and every column (except column 0), the indices of the first prime in n-th row and n-th column are listed in A117544 and A117545. (See A206864 for all the primes apart from row 0, 1, 2 and column 0, 1.)",
				"Another conjecture: Except row 0, 1, 2 and column 0, 1, the only perfect powers in this table are 121 (=Phi_5(3)) and 343 (=Phi_3(18)=Phi_6(19))."
			],
			"link": [
				"Eric Chen, \u003ca href=\"/A253240/b253240.txt\"\u003eTable of n, a(n) for n = 0..5049 (first 100 antidiagonals)\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CyclotomicPolynomial.html\"\u003eCyclotomic polynomial\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Cyclotomic_polynomial\"\u003eCyclotomic polynomial\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/index/Cy#CyclotomicPolynomialsValuesAtX\"\u003eIndex entries for Cyclotomic polynomials, values at X\u003c/a\u003e"
			],
			"formula": [
				"T(m, n) = Phi_m(n)"
			],
			"example": [
				"Read by antidiagonals:",
				"m\\n  0   1   2   3   4   5   6   7   8   9  10  11  12",
				"------------------------------------------------------",
				"0    1   1   1   1   1   1   1   1   1   1   1   1   1",
				"1   -1   0   1   2   3   4   5   6   7   8   9  10  11",
				"2    1   2   3   4   5   6   7   8   9  10  11  12  13",
				"3    1   3   7  13  21  31  43  57  73  91 111 133 157",
				"4    1   2   5  10  17  26  37  50  65  82 101 122 145",
				"5    1   5  31 121 341 781 ... ... ... ... ... ... ...",
				"6    1   1   3   7  13  21  31  43  57  73  91 111 133",
				"etc.",
				"The cyclotomic polynomials are:",
				"n        n-th cyclotomic polynomial",
				"0        1",
				"1        x-1",
				"2        x+1",
				"3        x^2+x+1",
				"4        x^2+1",
				"5        x^4+x^3+x^2+x+1",
				"6        x^2-x+1",
				"..."
			],
			"mathematica": [
				"Table[Cyclotomic[m, k-m], {k, 0, 49}, {m, 0, k}]"
			],
			"program": [
				"(PARI) t1(n)=n-binomial(floor(1/2+sqrt(2+2*n)), 2)",
				"t2(n)=binomial(floor(3/2+sqrt(2+2*n)), 2)-(n+1)",
				"T(m, n) = if(m==0, 1, polcyclo(m, n))",
				"a(n) = T(t1(n), t2(n))"
			],
			"xref": [
				"Rows 0-16 are A000012, A023443, A000027, A002061, A002522, A053699, A002061, A053716, A002523, A060883, A060884, A060885, A060886, A060887, A060888, A060889, A060890.",
				"Columns 0-13 are A158388, A020500, A019320, A019321, A019322, A019323, A019324, A019325, A019326, A019327, A019328, A019329, A019330, A019331.",
				"Main diagonal is A070518.",
				"Indices of primes in n-th row for n = 1-20 are A008864, A006093, A002384, A005574, A049409, A055494, A100330, A000068, A153439, A246392, A162862, A246397, A217070, A250174, A250175, A006314, A217071, A164989, A217072, A250176.",
				"Indices of primes in n-th column for n = 1-10 are A246655, A072226, A138933, A138934, A138935, A138936, A138937, A138938, A138939, A138940.",
				"Indices of primes in main diagonal is A070519.",
				"Cf. A117544 (indices of first prime in n-th row), A085398 (indices of first prime in n-th row apart from column 1), A117545 (indices of first prime in n-th column).",
				"Cf. A206942 (all terms (sorted) for rows\u003e2 and columns\u003e1).",
				"Cf. A206864 (all primes (sorted) for rows\u003e2 and columns\u003e1)."
			],
			"keyword": "sign,easy,tabl,nice",
			"offset": "0,9",
			"author": "_Eric Chen_, Apr 22 2015",
			"references": 3,
			"revision": 131,
			"time": "2021-09-20T22:14:07-04:00",
			"created": "2015-06-05T04:03:50-04:00"
		}
	]
}