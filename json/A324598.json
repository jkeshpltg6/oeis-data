{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A324598",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 324598,
			"data": "0,2,3,7,4,14,5,23,12,18,6,34,7,47,25,33,17,43,8,62,29,49,9,79,42,52,22,78,10,98,36,84,11,119,63,75,52,93,40,108,27,123,12,142,74,104,13,167,88,102,61,137,47,157,14,194,80,128,32,178",
			"name": "Irregular triangle with the representative solutions of the Diophantine equation x^2 + x - 1 congruent to  0 modulo N(n), with N(n) = A089270(n), for n \u003e= 1.",
			"comment": [
				"The length of row n is 1 for n = 1 and n = 2, and for n \u003e= 3 it is 2^{r1 + r4} with the number r1 and r4 of distinct primes congruent to 1 and 4 modulo 5, respectively, in the prime number factorization of N(n). E.g., n = 29, N = 209 = 11*19, has r1 = 1 and r4 = 1, with four solutions. The next rows with four solutions are n = 41, 43, 59,..., with N = 319, 341, 451, ... ;  for n = 643, 688, 896, ..., with N  = 6061, 6479, 8569, ..., there are eight solutions.",
				"For N(1) = 1 every integer solves this Diophantine equation, and the representative solution is 0.",
				"For N(2) = 5 there is only one representative solution, namely 2.",
				"For n \u003e= 3 the representative solutions come in nonnegtive power of 2 pairs (x1, x2) with x2 = N - 1 - x1.",
				"See the link in A089270 to the W. Lang paper, section 3, and Table 6."
			],
			"example": [
				"The irregular triangle T(n, k) begins (pairs (x, N - 1 - x) in brackets):",
				"n,    N \\ k   1   2     3   4  ...",
				"----------------------------------",
				"1,    1:      0",
				"2,    5:      2",
				"3,   11:     (3   7)",
				"4,   19:     (4  14)",
				"5,   29:     (5  23)",
				"6,   31:    (12  18)",
				"7,   41:     (6  34)",
				"8,   55:     (7  47)",
				"9,   59:    (25  33)",
				"10,  61:    (17  43)",
				"11,  71:     (8  62)",
				"12,  79:    (29  49)",
				"13,  89:     (9  79)",
				"14,  95:    (42  52)",
				"15, 101:    (22  78)",
				"16, 109:    (10  98)",
				"17, 121:    (36  84)",
				"18, 131:    (11 119)",
				"19, 139:    (63  75)",
				"20, 145:    (52  93)",
				"....",
				"29, 209:    (14 194)  (80 128)",
				"...",
				"41, 319:   (139 179) (150 168)",
				"...",
				"43, 341:    (18 322)  (80 260)",
				"...",
				"59, 451:    (47 403) (157 293)",
				"..."
			],
			"xref": [
				"Cf. A089270, A324599 (x^2 - 5 == 0 (mod N))."
			],
			"keyword": "nonn,tabf",
			"offset": "1,2",
			"author": "_Wolfdieter Lang_, Jul 08 2019",
			"references": 1,
			"revision": 10,
			"time": "2019-07-13T14:13:06-04:00",
			"created": "2019-07-12T19:15:42-04:00"
		}
	]
}