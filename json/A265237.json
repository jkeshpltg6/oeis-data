{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A265237",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 265237,
			"data": "1105,2465,10585,29341,46657,115921,162401,252601,278545,294409,314821,410041,488881,530881,552721,1461241,1909001,2433601,3224065,3581761,4335241,5148001,5310721,5444489,5632705,6054985,6189121,7207201,7519441,8134561,8355841",
			"name": "Carmichael numbers (A002997) that are the sum of two squares.",
			"comment": [
				"Carmichael numbers that are the sum of two distinct nonzero squares.",
				"29341 is the first term for which neither of the squares can be the square of a prime.",
				"Carmichael numbers that are not the sum of two squares start 561, 1729, 2821, 6601, 8911, 15841, ...",
				"A Carmichael number m is a sum of two squares if and only if p == 1 (mod m) for every prime p|m. Observation, numerically checked by _Amiram Eldar_: the first 13 terms of this sequence are odd composites m such that m | EulerNumber(m-1) (A122045). - _Thomas Ordowski_, Mar 01 2020"
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A265237/b265237.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"G. Tarry, I. Franel, A. Korselt, and G. Vacca. \u003ca href=\"https://oeis.org/wiki/File:Probl%C3%A8me_chinois.pdf\"\u003eProblème chinois\u003c/a\u003e. L'intermédiaire des mathématiciens 6 (1899), pp. 142-144.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CarmichaelNumber.html\"\u003eCarmichael Number\u003c/a\u003e",
				"\u003ca href=\"/index/Ca#Carmichael\"\u003eIndex entries for sequences related to Carmichael numbers\u003c/a\u003e"
			],
			"example": [
				"1105 is a term because 1105 = 23^2 + 24^2.",
				"2465 is a term because 2465 = 41^2 + 28^2.",
				"10585 is a term because 10585 = 37^2 + 96^2."
			],
			"mathematica": [
				"t = Cases[Range[1, 10^7, 2], n_ /; Mod[n, CarmichaelLambda@ n] == 1 \u0026\u0026 ! PrimeQ@ n]; Select[t, SquaresR[2, #] \u003e 0 \u0026] (* _Michael De Vlieger_, Dec 06 2015, after _Artur Jasinski_ at A002997 *)"
			],
			"program": [
				"(PARI) is(n)=if(n\u003c5, return(0)); my(f=factor(n)%4); if(vecmin(f[, 1])\u003e1, return(0)); for(i=1, #f[, 1], if(f[i, 1]==3 \u0026\u0026 f[i, 2]%2, return(0))); 1",
				"is_c(n)={my(f); bittest(n, 0) \u0026\u0026 !for(i=1, #f=factor(n)~, (f[2, i]==1 \u0026\u0026 n%(f[1, i]-1)==1)||return) \u0026\u0026 #f\u003e1}",
				"for(n=1, 1e7, if(is(n)\u0026\u0026is_c(n), print1(n, \", \")))"
			],
			"xref": [
				"Cf. A002997, A004431, A122045."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Altug Alkan_, Dec 06 2015",
			"references": 4,
			"revision": 44,
			"time": "2020-03-27T19:53:51-04:00",
			"created": "2015-12-20T14:00:24-05:00"
		}
	]
}