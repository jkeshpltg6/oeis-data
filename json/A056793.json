{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A056793",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 56793,
			"data": "1,2,4,6,12,12,24,32,48,48,96,96,192,192,192,240,480,480,960,960,960,960,1920,1920,2880,2880,3840,3840,7680,7680,15360,18432,18432,18432,18432,18432,36864,36864,36864,36864,73728,73728,147456,147456,147456,147456",
			"name": "Number of divisors of lcm(1,...,n).",
			"comment": [
				"The ratio a(n)/a(n-1) equals 1 if n is a member of A024619, equals 2 if n is prime, and is a noninteger value if n is in A025475. The noninteger ratio never seems to exceed 3/2, but appears to equal 3/2 if n is a member of A001248. The noninteger ratio conforms to the formula 1 / ( 1 - 1/n), which has 1 for limit and only 2 as single integer solution. In terms of coordinates (x,y), the lower values are (1/(1-1/n), 2^(n-1)) for n \u003e 2. - _Eric Desbiaux_, Jul 28 2013"
			],
			"link": [
				"Nathaniel Johnston, \u003ca href=\"/A056793/b056793.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = d(lcm(1, .., n)) = A000005(A003418(n)).",
				"a(n) = product(floor(log(n)/log(p))+1) where p runs through all prime numbers less than or equal to n. - _Wei Zhou_, Jun 25 2011"
			],
			"example": [
				"n = 20: lcm(1,..,20) = 2*2*2*2*3*3*5*7*11*13*17*19 = 232792560 and d(232792560) = 5*3*64 = 960."
			],
			"maple": [
				"A056793 := proc(n)",
				"    numtheory[tau](lcm($1..n)) ;",
				"end proc; # _Nathaniel Johnston_, Jun 26 2011"
			],
			"mathematica": [
				"Table[DivisorSigma[0, LCM @@ Range[n]], {n, 50}]",
				"Table[Product[Floor[Log[Prime[i], n]] + 1, {i, PrimePi[n]}], {n, 100}] (* _Wei Zhou_, Jun 25 2011 *)"
			],
			"program": [
				"(PARI) a(n)=n+=.5;prod(e=1,log(n)\\log(2),(1+1/e)^primepi(n^(1/e))) \\\\ _Charles R Greathouse IV_, Jun 06 2013"
			],
			"xref": [
				"Cf. A000005, A003418."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Labos Elemer_, Aug 28 2000",
			"references": 2,
			"revision": 50,
			"time": "2018-06-28T15:31:19-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}