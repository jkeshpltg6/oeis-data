{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A279945",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 279945,
			"data": "1,1,1,1,2,1,3,1,1,3,3,1,6,4,1,4,10,1,6,14,1,1,8,17,4,1,8,27,6,1,6,36,13,1,13,42,21,1,7,58,35,1,10,72,52,1,15,75,84,1,1,12,106,107,5,1,9,119,159,9,1,19,142,204,19,1,10,164,283,32,1,16,199",
			"name": "Irregular triangular array: t(n,k) = number of partitions of n having lexicographic difference set of size k; see Comments.",
			"comment": [
				"A partition P = [p(1), p(2), ..., p(k)] with p(1) \u003e= p(2) \u003e= ... \u003e= p(k) has lexicographic difference set {0} union {|p(i) - p(i-1)|: 2 \u003c= i \u003c= k}. Column 2 is A049990, and the n-th row sum is A000041(n)."
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A279945/b279945.txt\"\u003eTable of n, a(n) for n = 1..100\u003c/a\u003e"
			],
			"example": [
				"First 20 rows of array:",
				"1",
				"1    1",
				"1    2",
				"1    3    1",
				"1    3    3",
				"1    6    4",
				"1    4    10",
				"1    6    14    1",
				"1    8    17    4",
				"1    8    27    6",
				"1    6    36    13",
				"1    13   42    21",
				"1    7    58    35",
				"1    10   72    52",
				"1    15   75    84    1",
				"1    12   106   107   5",
				"1    9    119   159   9",
				"1    19   142   204   19",
				"1    10   164   283   32",
				"1    16   199   360   51",
				"Row 5: the 7 partitions of 5 are shown here with difference sets:",
				"partition  difference set     size",
				"[5]          null              0",
				"[4,1]        {3}               1",
				"[3,2]        {1}               1",
				"[3,1,1]      {0,2}             2",
				"[2,2,1]      {0,1}             2",
				"[2,1,1,1]    {0,1}             2",
				"[1,1,1,1]    {0}               1",
				"Row 5 of the array is 1 3 3, these being the number of 0's, 1's, 2's in the \"size\" column."
			],
			"mathematica": [
				"p[n_] := IntegerPartitions[n]; z = 20;",
				"t[n_, k_] := Length[DeleteDuplicates[Abs[Differences[p[n][[k]]]]]];",
				"u[n_] := Table[t[n, k], {k, 1, PartitionsP[n]}];",
				"v = Table[Count[u[n], h], {n, 1, z}, {h, 0, Max[u[n]]}]",
				"TableForm[v] (* A279945 array *)",
				"Flatten[v]   (* A279945 sequence *)"
			],
			"xref": [
				"Cf. A000041, A049990."
			],
			"keyword": "nonn,easy",
			"offset": "1,5",
			"author": "_Clark Kimberling_, Dec 26 2016",
			"references": 13,
			"revision": 16,
			"time": "2016-12-27T23:25:47-05:00",
			"created": "2016-12-27T23:25:47-05:00"
		}
	]
}