{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A054785",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 54785,
			"data": "2,4,8,8,12,16,16,16,26,24,24,32,28,32,48,32,36,52,40,48,64,48,48,64,62,56,80,64,60,96,64,64,96,72,96,104,76,80,112,96,84,128,88,96,156,96,96,128,114,124,144,112,108,160,144,128,160,120,120,192,124,128,208",
			"name": "a(n) = sigma(2n) - sigma(n), where sigma is the sum of divisors of n, A000203.",
			"comment": [
				"Sum of divisors of 2*n that do not divide n. - _Franklin T. Adams-Watters_, Oct 04 2018",
				"a(n) = 2*n iff n = 2^k, k \u003e= 0 (A000079). - _Bernard Schott_, Mar 24 2020"
			],
			"link": [
				"Paul Tek, \u003ca href=\"/A054785/b054785.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A000203(2n) - A000203(n).",
				"a(n) = 2*A002131(n).",
				"a(2*n) = A000203(n) + A000593(2*n). - _Reinhard Zumkeller_, Apr 23 2008",
				"L.g.f.: -log(EllipticTheta(4,0,x)) = Sum_{ n\u003e0 } (a(n)/n)*x^n. - _Benedict W. J. Irwin_, Jul 05 2016",
				"G.f.: Sum_{k\u003e=1} 2*k*x^k/(1 - x^(2*k)). - _Ilya Gutkovskiy_, Oct 23 2018"
			],
			"example": [
				"n=9: sigma(18)=18+9+6+3+2+1=39, sigma(9)=9+3+1=13, a(9)=39-13=26."
			],
			"maple": [
				"a:= proc(n) local e;",
				"  e:= 2^padic:-ordp(n,2);",
				"  2*e*numtheory:-sigma(n/e)",
				"end proc:",
				"map(a, [$1..100]); # _Robert Israel_, Jul 05 2016"
			],
			"mathematica": [
				"Table[DivisorSigma[1,2n]-DivisorSigma[1,n],{n,70}] (* _Harvey P. Dale_, May 11 2014 *)",
				"Table[CoefficientList[Series[-Log[EllipticTheta[4, 0, x]], {x, 0, 80}],x][[n + 1]] n, {n, 1, 80}] (* _Benedict W. J. Irwin_, Jul 05 2016 *)"
			],
			"program": [
				"(PARI) a(n)=sigma(2*n)-sigma(n) \\\\ _Charles R Greathouse IV_, Feb 13 2013",
				"(MAGMA) [DivisorSigma(1, 2*n) - DivisorSigma(1, n): n in [1..70]]; _Vincenzo Librandi_, Oct 05 2018"
			],
			"xref": [
				"Cf. A000203, A000593, A002131, A320059."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_Labos Elemer_, May 22 2000",
			"references": 23,
			"revision": 41,
			"time": "2020-03-25T13:46:11-04:00",
			"created": "2000-06-15T03:00:00-04:00"
		}
	]
}