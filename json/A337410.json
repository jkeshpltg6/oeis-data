{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A337410",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 337410,
			"data": "1,2,1,3,6,1,4,18,70,1,5,40,1407,93024,1,6,75,12480,294157089,47823602694208,1,7,126,69050,91983927296,67514530382043163023924,443077371786837979607993095063601152,1",
			"name": "Array read by descending antidiagonals: T(n,k) is the number of achiral colorings of the edges of a regular n-dimensional orthotope (hypercube) using k or fewer colors.",
			"comment": [
				"An achiral arrangement is identical to its reflection. For n=1, the figure is a line segment with one edge. For n=2, the figure is a square with 4 edges. For n=3, the figure is a cube with 12 edges. The number of edges is n*2^(n-1).",
				"Also the number of achiral colorings of the regular (n-2)-dimensional simplexes in a regular n-dimensional orthoplex."
			],
			"link": [
				"K. Balasubramanian, \u003ca href=\"https://doi.org/10.33187/jmsm.471940\"\u003eComputational enumeration of colorings of hyperplanes of hypercubes for all irreducible representations and applications\u003c/a\u003e, J. Math. Sci. \u0026 Mod. 1 (2018), 158-180."
			],
			"formula": [
				"The algorithm used in the Mathematica program below assigns each permutation of the axes to a partition of n and then considers separate conjugacy classes for axis reversals. It uses the formulas in Balasubramanian's paper. If the value of m is increased, one can enumerate colorings of higher-dimensional elements beginning with T(m,1).",
				"T(n,k) = 2*A337408(n,k) - A337407(n,k) = A337407(n,k) - 2*A337409(n,k) = A337408(n,k) - A337409(n,k)."
			],
			"example": [
				"Table begins with T(1,1):",
				"1  2    3     4     5      6      7       8       9       10 ...",
				"1  6   18    40    75    126    196     288     405      550 ...",
				"1 70 1407 12480 69050 281946 931490 2632512 6598935 15041950 ...",
				"For T(2,2)=6, the arrangements are AAAA, AAAB, AABB, ABAB, ABBB, and BBBB."
			],
			"mathematica": [
				"m=1; (* dimension of color element, here an edge *)",
				"Fi1[p1_] := Module[{g, h}, Coefficient[Product[g = GCD[k1, p1]; h = GCD[2 k1, p1]; (1 + 2 x^(k1/g))^(r1[[k1]] g) If[Divisible[k1, h], 1, (1+2x^(2 k1/h))^(r2[[k1]] h/2)], {k1, Flatten[Position[cs, n1_ /; n1 \u003e 0]]}], x, n - m]];",
				"FiSum[] := (Do[Fi2[k2] = Fi1[k2], {k2, Divisors[per]}];DivisorSum[per, DivisorSum[d1 = #, MoebiusMu[d1/#] Fi2[#] \u0026]/# \u0026]);",
				"CCPol[r_List] := (r1 = r; r2 = cs - r1; If[EvenQ[Sum[If[EvenQ[j3], r1[[j3]], r2[[j3]]], {j3,n}]],0,(per = LCM @@ Table[If[cs[[j2]] == r1[[j2]], If[0 == cs[[j2]],1,j2], 2j2], {j2,n}]; Times @@ Binomial[cs, r1] 2^(n-Total[cs]) b^FiSum[])]);",
				"PartPol[p_List] := (cs = Count[p, #]\u0026/@ Range[n]; Total[CCPol[#]\u0026/@ Tuples[Range[0,cs]]]);",
				"pc[p_List] := Module[{ci, mb}, mb = DeleteDuplicates[p]; ci = Count[p, #]\u0026/@ mb; n!/(Times@@(ci!) Times@@(mb^ci))] (*partition count*)",
				"row[n_Integer] := row[n] = Factor[(Total[(PartPol[#] pc[#])\u0026/@ IntegerPartitions[n]])/(n! 2^(n-1))]",
				"array[n_, k_] := row[n] /. b -\u003e k",
				"Table[array[n,d+m-n], {d,7}, {n,m,d+m-1}] // Flatten"
			],
			"xref": [
				"Cf. A337407 (oriented), A337408 (unoriented), A337409 (chiral).",
				"Rows 1-4 are A000027, A002411, A331351, A331361.",
				"Cf. A327086 (simplex edges), A337414 (orthoplex edges), A325015 (orthotope vertices)."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Robert A. Russell_, Aug 26 2020",
			"references": 8,
			"revision": 12,
			"time": "2020-08-28T06:05:32-04:00",
			"created": "2020-08-28T06:05:32-04:00"
		}
	]
}