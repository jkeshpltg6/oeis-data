{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A293975",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 293975,
			"data": "0,3,1,8,2,12,3,18,4,20,5,24,6,30,7,32,8,36,9,42,10,44,11,52,12,54,13,56,14,60,15,68,16,70,17,72,18,78,19,80,20,84,21,90,22,92,23,100,24,102,25,104,26,112,27,114,28,116,29,120,30,128,31,130,32,132,33,138,34",
			"name": "If n is even, divide by 2; otherwise, add the next larger prime.",
			"comment": [
				"Inspired by the \"PrimeLatz\" map A174221 (where the next three primes are added).",
				"The trajectory under iterations of this map seems to end in the cycle 1 -\u003e 3 -\u003e 8 -\u003e 4 -\u003e 2 -\u003e 1, for any starting value n. Can this be proved?",
				"In order to develop a proof, one can consider the \"condensed\" version of the map which is: h(x) = odd_part(x+nextprime(x)); i.e., add the next prime, then remove all factors of 2. It is easy to see that this map verifies, for all x \u003e 2, h(x) \u003c= x + g(x)/2 where g(x) is the gap between the x and the next larger prime. Often, h(x) will be close to x/2 or even to x/4 or smaller. Nonetheless, for any power (iteration) of h, there are numbers for which h^m is increasing, e.g., h(h(h(x))) \u003e x for x = 1, 525, 891, 1071, 1135, ..., and h^4(x) \u003e x for x = 2, 1329, 5591, 8469, 9555, ...",
				"From _Robert Israel_, Nov 08 2017: (Start)",
				"It suffices to prove that if n \u003e 1 is odd, the trajectory {x(i)} starting at x(0)=n contains some number \u003c n.  Let p = nextprime(n).  As long as x(2k) is odd we have x(2k+1) = x(2k)+p and x(2k+2)=(x(2k)+p)/2 with",
				"n \u003c= x(2k) \u003c x(2k+2) \u003c p.  But this can only continue finitely many times: eventually x(2k) must be even, and then x(2k+1) \u003c p/2 \u003c n (by Bertrand's postulate). (End)"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A293975/b293975.txt\"\u003eTable of n, a(n) for n = 0..10001\u003c/a\u003e"
			],
			"maple": [
				"seq(op([k,2*k+1+nextprime(2*k+1)]),k=0..100); # _Robert Israel_, Nov 08 2017"
			],
			"mathematica": [
				"Array[If[EvenQ@ #, #/2, NextPrime@ # + # \u0026@ #] \u0026, 69, 0] (* _Michael De Vlieger_, Nov 08 2017 *)"
			],
			"program": [
				"(PARI) A293975(n)=if(bittest(n,0),n+nextprime(n+1),n\\2)"
			],
			"xref": [
				"Cf. A174221 (the \"PrimeLatz\" map), A006370 (the \"3x+1\" map)."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_M. F. Hasler_, Nov 04 2017",
			"references": 4,
			"revision": 20,
			"time": "2017-11-08T22:34:09-05:00",
			"created": "2017-11-08T12:26:13-05:00"
		}
	]
}