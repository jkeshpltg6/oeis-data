{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A038580",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 38580,
			"data": "5,11,31,59,127,179,277,331,431,599,709,919,1063,1153,1297,1523,1787,1847,2221,2381,2477,2749,3001,3259,3637,3943,4091,4273,4397,4549,5381,5623,5869,6113,6661,6823,7193,7607,7841,8221,8527,8719,9319,9461,9739",
			"name": "Primes with indices that are primes with prime indices.",
			"link": [
				"T. D. Noe, \u003ca href=\"/A038580/b038580.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"R. G. Batchko, \u003ca href=\"http://arxiv.org/abs/1405.2900\"\u003eA prime fractal and global quasi-self-similar structure in the distribution of prime-indexed primes\u003c/a\u003e, arXiv preprint arXiv:1405.2900 [math.GM], 2014.",
				"Robert E. Dressler and S. Thomas Parker, \u003ca href=\"http://dx.doi.org/10.1145/321892.321900\"\u003ePrimes with a prime subscript\u003c/a\u003e, J. ACM, Volume 22 Issue 3, July 1975, 380-381.",
				"N. Fernandez, \u003ca href=\"http://www.borve.org/primeness/FOP.html\"\u003eAn order of primeness, F(p)\u003c/a\u003e",
				"N. Fernandez, \u003ca href=\"/A006450/a006450.html\"\u003eAn order of primeness\u003c/a\u003e [cached copy, included with permission of the author]",
				"N. Fernandez, \u003ca href=\"http://www.borve.org/primeness/moreterms.html\"\u003eMore terms of this and other sequences related to A049076.\u003c/a\u003e",
				"Michael P. May, \u003ca href=\"https://doi.org/10.35834/2020/3202158\"\u003eProperties of Higher-Order Prime Number Sequences\u003c/a\u003e, Missouri J. Math. Sci. (2020) Vol. 32, No. 2, 158-170; and \u003ca href=\"https://arxiv.org/abs/2108.04662\"\u003earXiv version\u003c/a\u003e, arXiv:2108.04662 [math.NT], 2021."
			],
			"formula": [
				"a(n) = prime(prime(prime(n))).",
				"a(n) ~ n*log(n)^3. - _Ilya Gutkovskiy_, Jul 17 2016"
			],
			"maple": [
				"a:= ithprime@@3;",
				"seq(a(n), n=1..50);  # _Alois P. Heinz_, Jun 14 2015",
				"# For Maple code for the prime/nonprime compound sequences (listed in cross-references) see A003622. - _N. J. A. Sloane_, Mar 30 2016"
			],
			"mathematica": [
				"Table[ Prime[ Prime[ Prime[ n ] ] ], {n, 1, 60} ]",
				"Nest[Prime, Range[45], 3] (* _Robert G. Wilson v_, Mar 15 2004 *)"
			],
			"program": [
				"(PARI) a(n) = prime(prime(prime(n))) \\\\ _Charles R Greathouse IV_, Apr 28 2015",
				"(PARI) list(lim)=my(v=List(),q,r); forprime(p=2,lim, if(isprime(q++) \u0026\u0026 isprime(r++), listput(v,p))); Set(v) \\\\ _Charles R Greathouse IV_, Feb 14 2017",
				"(MAGMA) [NthPrime(NthPrime(NthPrime(n))): n in [1..50]]; // _Vincenzo Librandi_, Jul 17 2016"
			],
			"xref": [
				"Primes p for which A049076(p) \u003e 3.",
				"Cf. A049090, A049202, A049203, A057847, A057849, A057850, A057851, A058332, A093047.",
				"Second differences give A245175.",
				"Let A = primes A000040, B = nonprimes A018252. The 2-level compounds are AA = A006450, AB = A007821, BA = A078782, BB = A102615. The 3-level compounds AAA, AAB, ..., BBB are A038580, A049078, A270792, A102617, A270794, A270795, A270796, A102616."
			],
			"keyword": "easy,nice,nonn",
			"offset": "1,1",
			"author": "_Brian Galebach_",
			"references": 40,
			"revision": 85,
			"time": "2021-08-11T08:08:22-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}