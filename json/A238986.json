{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A238986",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 238986,
			"data": "0,1,2,3,4,5,6,7,8,9,2,2,4,6,8,2,4,8,4,4,4,4,4,6,8,2,4,8,4,4,6,6,6,6,8,2,4,8,4,4,8,8,8,8,8,2,4,8,4,4,2,2,2,2,2,2,4,8,4,4",
			"name": "Ground Pyramidalized Numbers: Write the decimal digits of 'n' (a nonnegative integer) and take successive absolute differences (\"pyramidalization\"), then sum all digits of each level of the pyramid. If total is greater than 9, repeat the process until result is between 0 and 9, which is 'a(n)' (0 \u003c= a(n) \u003c= 9).",
			"comment": [
				"A given nonnegative integer 'n' is decomposed to its digits and the absolute differences between the digits are taken, then the differences between differences between digits (and so on, until the top of the \"gap-pyramid\" is reached - we could call this process \"pyramidalization\"). If the sum 's(n)' of the resulting digits is 0 \u003c= s(n) \u003c= 9, it's 'a(n)'; if greater than 9, the same process is applied to the result, and to the subsequent result if necessary, and so on, until the result is smaller than 10."
			],
			"link": [
				"Giovanni Resta, \u003ca href=\"/A238986/b238986.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n)=n, if 0\u003c=n\u003c=9;",
				"b'(n)=n-9*floor(n/10)+|-n+11*floor(n/10)|, if 10\u003c=n\u003c=99;",
				"  b'(n)=a(n), if 0\u003c=b'(n)\u003c=9;",
				"  else, b''(n)=b'(n)-9*floor(b'(n)/10)+|-b'(n)+11*floor(b'(n)/10)|;",
				"  b''(n)=a(n), if 0\u003c=b''(n)\u003c=9;",
				"  else, b'''(n)=...",
				"c'(n)=n-9*floor(n/10)-9*floor(n/100)+|-floor(n/10)+11*floor(n/100)|+|-n+11*floor(n/10)-10*floor(n/100)|+||-floor(n/10)+11*floor(n/100)|-|-n+11*floor(n/10)-10*floor(n/100)||, if 100\u003c=n\u003c=999.",
				"  c'(n)=a(n), if 0\u003c=c'(n)\u003c=9;",
				"  else, if 10\u003c=c'(n)\u003c=99, c''(n)=c'(n)-9*floor(c'(n)/10)+|-c'(n)+11*floor(c'(n)/10)|;",
				"                          c''(n)=a(n), if 0\u003c=c''(n)\u003c=9",
				"                          else, ..."
			],
			"example": [
				"If n=364, a(364)=4, for...",
				".",
				"____1",
				"__3_:_2__ --\u003eb'(364)=3+6+4+|3-6|+|6-4|+||3-6|-|6-4||=3+6+4+3+2+1=19\u003e9",
				"3_:_6_:_4",
				".",
				"__8",
				"1_:_9  --\u003e b''(364)=1+9|1-9|=1+9+8=18\u003e9",
				".",
				"__7",
				"1_:_8 --\u003e b'''(364)=1+8+|1-8|=1+8+7=16\u003e9",
				".",
				"__5",
				"1_:_6 --\u003e b''''(364)=1+6+|1-6|=1+6+5=12\u003e9",
				".",
				"__1",
				"1_:_2 --\u003e b'''''(364)=1+2+|1-2|=1+2+1=4=a(364)"
			],
			"mathematica": [
				"a[n_] := If[n \u003c 10, n, Block[{d = IntegerDigits@ n, s}, s = Total@ d; While[Length@ d \u003e 1, d = Abs@ Differences@ d; s += Total@d]; If[s \u003c 10, s, a@s]]]; a /@ Range[0, 99] (* _Giovanni Resta_, Mar 16 2014 *)"
			],
			"xref": [
				"Cf. A227876. The pyramidalization process is applied and reapplied to each term until the result reaches its \"ground limit\".",
				"Cf. A007318. The pyramidalization process relates to Pascal's Triangle because it is done in the opposite direction (towards the top instead of the base), using the contrary operation (absolute difference instead of sum of the prior terms)."
			],
			"keyword": "nonn,base,uned",
			"offset": "0,3",
			"author": "_Filipi R. de Oliveira_, Mar 07 2014",
			"references": 1,
			"revision": 13,
			"time": "2015-12-21T04:15:42-05:00",
			"created": "2014-04-01T10:33:15-04:00"
		}
	]
}