{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A008936",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 8936,
			"data": "1,2,4,8,15,28,52,96,176,320,576,1024,1792,3072,5120,8192,12288,16384,16384,0,-65536,-262144,-786432,-2097152,-5242880,-12582912,-29360128,-67108864,-150994944,-335544320,-738197504,-1610612736,-3489660928,-7516192768,-16106127360,-34359738368",
			"name": "Expansion of (1 - 2*x -x^4)/(1 - 2*x)^2 in powers of x.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A008936/b008936.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-4)."
			],
			"formula": [
				"From _Michael Somos_, Aug 19 2014: (Start)",
				"a(n) = 2^n for all n\u003c4.",
				"a(n) = 2^n - (n-3) * 2^(n-4) for all n\u003e=4.",
				"a(n) = 4*(a(n-1) - a(n-2)) for all n in Z except n=4.",
				"a(n) = 2*a(n-1) - 2^(n-4).",
				"0 = a(n)*(-8*a(n+1) + 8*a(n+2) - 2*a(n+3)) + a(n+1)*(+4*a(n+1) - 4*a(n+2) + a(n+3)) for all n in Z. (End)",
				"E.g.f.: ( -3 -4*x -2*x^2 + (19 - 2*x)*exp(2*x) )/16. - _G. C. Greubel_, Sep 13 2019"
			],
			"example": [
				"G.f. = 1 + 2*x + 4*x^2 + 8*x^3 + 15*x^4 + 28*x^5 + 52*x^6 + 96*x^7 + 176*x^8 + ..."
			],
			"maple": [
				"A008936 := proc(n) option remember; if n \u003c= 3 then 2^n else 2*A008936(n-1)-2^(n-4); fi; end;"
			],
			"mathematica": [
				"a[ n_]:= 2^n - 2^(n-4) Max[0, n-3]; (* _Michael Somos_, Aug 19 2014 *)",
				"Table[If[n \u003c 4, 2^n, 2^(n-4)*(19 - n)], {n,0,40}] (* _G. C. Greubel_, Sep 13 2019 *)"
			],
			"program": [
				"(PARI) {a(n) = 2^n - 2^(n-4) * max(n-3, 0)}; /* _Michael Somos_, 12 Jan 2000 */",
				"(PARI) Vec((1-2*x-x^4)/(1-2*x)^2 +O(x^40)) \\\\ _Charles R Greathouse IV_, Sep 26 2012",
				"(MAGMA) [n lt 4 select 2^n else 2^(n-4)*(19-n): n in [0..40]]; // _G. C. Greubel_, Sep 13 2019",
				"(Sage) [1,2,4,8]+[2^(n-4)*(19 - n) for n in (4..40)] # _G. C. Greubel_, Sep 13 2019",
				"(GAP) a:=[1,2];; for n in [3..40] do a[n]:=4*(a[n-1]-a[n-2]); od; a; # _G. C. Greubel_, Sep 13 2019"
			],
			"keyword": "sign,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Alejandro Teruel (teruel(AT)usb.ve)",
			"ext": [
				"Better description from _Michael Somos_, Jan 12 2000.",
				"More terms added by _G. C. Greubel_, Sep 13 2019"
			],
			"references": 1,
			"revision": 22,
			"time": "2019-09-14T06:41:46-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}