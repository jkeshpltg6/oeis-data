{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A341990",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 341990,
			"data": "0,1,4,12,40,128,402,1278,4040,12776,40417,127803,404136,1277995,4041401,12779996,40413886,127799963",
			"name": "a(n) is the number of primitive solutions to the inverse Pythagorean equation 1/x^2 + 1/y^2 = 1/z^2 such that x \u003c= y and x + y + z \u003c= 10^n.",
			"link": [
				"Math Stack Exchange, \u003ca href=\"https://math.stackexchange.com/questions/2688808/diophantine-equation-of-three-variables\"\u003eDiophantine equation of three variables\u003c/a\u003e"
			],
			"formula": [
				"It can be shown that all the primitive solutions are generated from the following parametric form: x = 2*a*b*(a^2+b^2), y = a^4-b^4, z = 2*a*b*(a^2-b^2), where gcd(a, b) = 1 and a + b is odd.",
				"Lim_{n -\u003e oo} a(n)/10^(n/2) = (4/Pi^2)*Integral_{x=sqrt(2)-1..1} 1/sqrt(1+4*x-x^4)) ~ 0.1277999513464289283641211182341081978805..."
			],
			"example": [
				"For n = 3, the solutions are (20, 15, 12), (156, 65, 60), (136, 255, 120), (600, 175, 168). So a(3) = 4."
			],
			"mathematica": [
				"a[n_] := Module[{m, l, a, b, s, a2, b2, x, y, z, cnt}, m = 10^n; s = 0; l = 3 * Floor[m^0.25]; cnt = 0; For[a = 1, a \u003c= l, a++, a2 = a * a; For[b = 1, b \u003c a, b++, If[GCD[a, b] == 1 \u0026\u0026 Mod[a + b, 2] == 1, b2 = b * b; x = 2 * a * b * (a2 + b2); y = a2 * a2 - b2 * b2; z = 2 * a * b * (a2 - b2); If[x + y + z \u003e m, Continue[]]; cnt += 1];]]; cnt];"
			],
			"program": [
				"(Python)",
				"from math import gcd",
				"def fourth_root(n):",
				"    u, s = n, n + 1",
				"    while u \u003c s:",
				"        s = u",
				"        t = 3 * s + n // (s ** 3)",
				"        u = t // 4",
				"    return s",
				"def a(n):",
				"    N = 10 ** n",
				"    L = fourth_root(N) * 3",
				"    cnt = 0",
				"    for a in range(1, L + 1):",
				"       a2 = a * a",
				"       for b in range(1, a):",
				"           if (a + b) % 2 == 1 and gcd(a, b) == 1:",
				"               b2 = b * b",
				"               v = (4 * a * b + a2) * a2 - b2 * b2",
				"               if v \u003e N:",
				"                   continue",
				"               cnt += 1",
				"    return cnt",
				"(PARI) a(n) = {my(lim = 3*sqrtnint(10^n, 4), nb = 0); for (x=1, lim, for (y=1, x, if (((x+y) % 2) \u0026\u0026 (gcd(x,y) == 1), if (2*x*y*(x^2 + y^2) + x^4 - y^4 + 2*x*y*(x^2 - y^2) \u003c= 10^n, nb++);););); nb;} \\\\ _Michel Marcus_, Mar 25 2021"
			],
			"keyword": "nonn,more",
			"offset": "1,3",
			"author": "_Asif Ahmed_, Feb 25 2021",
			"references": 0,
			"revision": 33,
			"time": "2021-04-01T15:36:57-04:00",
			"created": "2021-04-01T15:36:57-04:00"
		}
	]
}