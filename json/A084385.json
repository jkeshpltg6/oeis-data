{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A084385",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 84385,
			"data": "1,2,4,3,7,5,9,6,8,11,13,10,12,15,17,14,16,19,21,18,20,23,25,22,24,27,29,26,28,31,33,30,32,35,37,34,36,39,41,38,40,43,45,42,44,47,49,46,48,51,53,50,52,55,57,54,56,59,61,58,60,63,65,62,64,67,69,66,68,71,73,70",
			"name": "a(1) = 1; a(n+1) is the smallest number not occurring earlier and coprime to Sum_{j=1..n} a(j).",
			"comment": [
				"Rearrangement of the positive integers.",
				"Any sequence defined in this manner (that is, a(1) is any positive integer and a(n+1) is the smallest integer not occurring earlier and coprime to Sum_{j=1..n} a(j)) is a rearrangement of all positive integers.  This property is used by problem 4 of Chinese High School Mathematical Olympiad in 2018. - _Shu Shang_, Sep 29 2021"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A084385/b084385.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"For n \u003e 6: a(n) = n-2 for n mod 4 = 0, a(n) = n-1 for n mod 4 = 1, a(n) = n+1 for n mod 4 = 2, a(n) = n+2 for n mod 4 = 3. - _Klaus Brockhaus_, Nov 30 2003"
			],
			"example": [
				"1+2+4 = 7, 3 is the smallest number not occurring earlier and coprime to 7, hence a(4) = 3."
			],
			"program": [
				"(PARI) used(k,v)=b=0; j=1; while(b\u003c1\u0026\u0026j\u003c=length(v),if(v[j]==k,b=1,j++)); b",
				"{print1(s=1,\",\"); v=[s]; for(n=1,72,j=1; k=2; while(used(k,v)||gcd(k,s)\u003e1,k++); v=concat(v,k); s=s+k; print1(k,\",\"))}",
				"(PARI) {print1(1,\",\",2,\",\",4,\",\",3,\",\",7,\",\",5,\",\");for(n=7,73,m=n%4;d=(if(m==0,-2,if(m==1,-1,if(m==2,1,2)))); print1(n+d,\",\"))}",
				"(Haskell)",
				"import Data.List (delete)",
				"a084385 n = a084385_list !! (n-1)",
				"a084385_list = 1 : f [2..] 1 where",
				"   f xs s = g xs where",
				"     g (y:ys) = if gcd s y == 1 then y : f (delete y xs) (s + y) else g ys",
				"-- _Reinhard Zumkeller_, Aug 15 2015"
			],
			"xref": [
				"Partial sums are in A111244. Cf. A064413.",
				"Cf. A261351 (inverse)."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Amarnath Murthy_, May 29 2003",
			"ext": [
				"Edited, corrected and extended by _Klaus Brockhaus_, May 29 2003"
			],
			"references": 3,
			"revision": 25,
			"time": "2021-10-01T19:05:01-04:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}