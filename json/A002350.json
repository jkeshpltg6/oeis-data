{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002350",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2350,
			"id": "M2240 N0890",
			"data": "1,3,2,1,9,5,8,3,1,19,10,7,649,15,4,1,33,17,170,9,55,197,24,5,1,51,26,127,9801,11,1520,17,23,35,6,1,73,37,25,19,2049,13,3482,199,161,24335,48,7,1,99,50,649,66249,485,89,15,151,19603,530,31,1766319049,63,8,1",
			"name": "Take solution to Pellian equation x^2 - n*y^2 = 1 with smallest positive y and x \u003e= 0; sequence gives a(n) = x, or 1 if n is a square. A002349 gives values of y.",
			"comment": [
				"From _A.H.M. Smeets_, Nov 20 2017: (Start)",
				"a(p*q^2) = b(p,q/gcd(A002349(p),q)) where",
				"b(p,0) = 1, b(p,1) = a(p), b(p,i) = 2*a(p)*b(p,i-1)- b(p,i-2) for i\u003e1. (End)"
			],
			"reference": [
				"A. Cayley, Report of a committee appointed for the purpose of carrying on the tables connected with the Pellian equation ..., Collected Mathematical Papers. Vols. 1-13, Cambridge Univ. Press, London, 1889-1897, Vol. 13, pp. 430-443.",
				"C. F. Degen, Canon Pellianus. Hafniae, Copenhagen, 1817.",
				"D. H. Lehmer, Guide to Tables in the Theory of Numbers. Bulletin No. 105, National Research Council, Washington, DC, 1941, p. 55.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe and Ray Chandler, \u003ca href=\"/A002350/b002350.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (first 1000 terms from T. D. Noe)",
				"A. Cayley, \u003ca href=\"/A002349/a002349.pdf\"\u003eReport of a committee appointed for the purpose of carrying on the tables connected with the Pellian equation ...\u003c/a\u003e, Collected Mathematical Papers. Vols. 1-13, Cambridge Univ. Press, London, 1889-1897, Vol. 13, pp. 430-443. (Annotated scanned copy)",
				"L. Beeckmans, \u003ca href=\"http://www.jstor.org/stable/2974904\"\u003eSquares expressible as sum of consecutive squares\u003c/a\u003e, Amer. Math. Monthly, 101 (1994), 437-442.",
				"L. Euler, \u003ca href=\"http://math.dartmouth.edu/~euler/pages/E029.html\"\u003eDe solutione problematum diophanteorum per numeros integros\u003c/a\u003e (English and Latin), par. 17.",
				"N. J. A. Sloane et al., \u003ca href=\"https://oeis.org/wiki/Binary_Quadratic_Forms_and_OEIS\"\u003eBinary Quadratic Forms and OEIS\u003c/a\u003e (Index to related sequences, programs, references)"
			],
			"example": [
				"For n = 1, 2, 3, 4, 5 solutions are (x,y) = (1, 0), (3, 2), (2, 1), (1, 0), (9, 4)."
			],
			"mathematica": [
				"PellSolve[(m_Integer)?Positive] := Module[{cf, n, s}, cf = ContinuedFraction[ Sqrt[m]]; n = Length[ Last[cf]]; If[ OddQ[n], n = 2*n]; s = FromContinuedFraction[ ContinuedFraction[ Sqrt[m], n]]; {Numerator[s], Denominator[s]}]; f[n_] := If[ !IntegerQ[ Sqrt[n]], PellSolve[n][[1]], 1]; Table[ f[n], {n, 0, 65}]",
				"Table[If[! IntegerQ[Sqrt[k]], {k, FindInstance[x^2 - k*y^2 == 1 \u0026\u0026 x \u003e 0 \u0026\u0026 y \u003e 0, {x, y}, Integers]}, Nothing], {k, 2, 80}][[All, 2, 1, 1, 2]] (* _Horst H. Manninger_, Mar 23 2021 *)"
			],
			"xref": [
				"Cf. A002349, A006702, A006703, A006704, A006705. See A033316, A033315, A033319 for records."
			],
			"keyword": "nonn,nice,easy",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_.",
			"references": 26,
			"revision": 66,
			"time": "2021-05-03T16:34:35-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}