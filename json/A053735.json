{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A053735",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 53735,
			"data": "0,1,2,1,2,3,2,3,4,1,2,3,2,3,4,3,4,5,2,3,4,3,4,5,4,5,6,1,2,3,2,3,4,3,4,5,2,3,4,3,4,5,4,5,6,3,4,5,4,5,6,5,6,7,2,3,4,3,4,5,4,5,6,3,4,5,4,5,6,5,6,7,4,5,6,5,6,7,6,7,8,1,2,3,2,3,4,3,4,5,2,3,4,3,4,5,4,5,6,3,4,5,4,5,6",
			"name": "Sum of digits of (n written in base 3).",
			"comment": [
				"Also the fixed point of the morphism 0-\u003e{0,1,2}, 1-\u003e{1,2,3}, 2-\u003e{2,3,4}, etc. - _Robert G. Wilson v_, Jul 27 2006"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A053735/b053735.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"F. T. Adams-Watters and F. Ruskey, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL12/Ruskey2/ruskey14.html\"\u003eGenerating Functions for the Digital Sum and Other Digit Counting Sequences\u003c/a\u003e, JIS, Vol. 12 (2009), Article 09.5.6.",
				"Michael Gilleland, \u003ca href=\"/selfsimilar.html\"\u003eSome Self-Similar Integer Sequences\u003c/a\u003e.",
				"Jan-Christoph Puchta and Jürgen Spilker, \u003ca href=\"http://dx.doi.org/10.1007/s00591-002-0048-4\"\u003eAltes und Neues zur Quersumme\u003c/a\u003e, Math. Semesterber, Vol. 49 (2002), pp. 209-226; \u003ca href=\"http://www.math.uni-rostock.de/~schlage-puchta/papers/Quersumme.pdf\"\u003epreprint\u003c/a\u003e.",
				"Jeffrey O. Shallit, \u003ca href=\"http://www.jstor.org/stable/2322179\"\u003eProblem 6450\u003c/a\u003e, Advanced Problems, The American Mathematical Monthly, Vol. 91, No. 1 (1984), pp. 59-60; \u003ca href=\"http://www.jstor.org/stable/2322523\"\u003eTwo series, solution to Problem 6450\u003c/a\u003e, ibid., Vol. 92, No. 7 (1985), pp. 513-514.",
				"Vladimir Shevelev, \u003ca href=\"http://dx.doi.org/10.4064/aa126-3-1\"\u003eCompact integers and factorials\u003c/a\u003e, Acta Arith., Vol. 126, No. 3 (2007), pp. 195-236 (cf. p.205).",
				"Robert Walker, \u003ca href=\"http://robertinventor.com/ftswiki/Self_Similar_Sloth_Canon_Number_Sequences\"\u003eSelf Similar Sloth Canon Number Sequences\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/DigitSum.html\"\u003eDigit Sum\u003c/a\u003e."
			],
			"formula": [
				"From _Benoit Cloitre_, Dec 19 2002: (Start)",
				"a(0) = 0, a(3n) = a(n), a(3n + 1) = a(n) + 1, a(3n + 2) = a(n) + 2.",
				"a(n) = n - 2*Sum_{k\u003e0} floor(n/3^k) = n - 2*A054861(n). (End)",
				"a(n) = A062756(n) + 2*A081603(n). - _Reinhard Zumkeller_, Mar 23 2003",
				"G.f.: (Sum_{k \u003e= 0} (x^(3^k) + 2*x^(2*3^k))/(1 + x^(3^k) + x^(2*3^k)))/(1 - x). - _Michael Somos_, Mar 06 2004, corrected by _Franklin T. Adams-Watters_, Nov 03 2005",
				"In general, the sum of digits of (n written in base b) has generating function (Sum_{k\u003e=0} (Sum_{0 \u003c= i \u003c b} i*x^(i*b^k))/(Sum_{i=0..b-1} x^(i*b^k)))/(1-x). - _Franklin T. Adams-Watters_, Nov 03 2005",
				"First differences of A094345. - _Vladeta Jovovic_, Nov 08 2005",
				"a(A062318(n)) = n and a(m) \u003c n for m \u003c A062318(n). - _Reinhard Zumkeller_, Feb 26 2008",
				"a(n) = A138530(n,3) for n \u003e 2. - _Reinhard Zumkeller_, Mar 26 2008",
				"a(n) \u003c= 2*log_3(n+1). - _Vladimir Shevelev_, Jun 01 2011",
				"a(n) = Sum_{k\u003e=0} A030341(n, k). - _Philippe Deléham_, Oct 21 2011",
				"G.f. satisfies G(x) = (x+2*x^2)/(1-x^3) + (1+x+x^2)*G(x^3), and has a natural boundary at |x|=1. - _Robert Israel_, Jul 02 2015",
				"a(n) = A056239(A006047(n)). - _Antti Karttunen_, Jun 03 2017",
				"a(n) = A000120(A289813(n)) + 2*A000120(A289814(n)). - _Antti Karttunen_, Jul 20 2017",
				"a(0) = 0; a(n) = a(n - 3^floor(log_3(n))) + 1. - _Ilya Gutkovskiy_, Aug 23 2019",
				"Sum_{n\u003e=1} a(n)/(n*(n+1)) = 3*log(3)/2 (Shallit, 1984). - _Amiram Eldar_, Jun 03 2021"
			],
			"example": [
				"a(20) = 2 + 0 + 2 = 4 because 20 is written as 202 base 3.",
				"From _Omar E. Pol_, Feb 20 2010: (Start)",
				"This can be written as a triangle with row lengths A025192 (see the example in the entry A000120):",
				"0,",
				"1,2,",
				"1,2,3,2,3,4,",
				"1,2,3,2,3,4,3,4,5,2,3,4,3,4,5,4,5,6,",
				"1,2,3,2,3,4,3,4,5,2,3,4,3,4,5,4,5,6,3,4,5,4,5,6,5,6,7,2,3,4,3,4,5,4,5,6,3,...",
				"where the k-th row contains a(3^k+i) for 0\u003c=i\u003c2*3^k and converges to A173523 as k-\u003einfinity. (End) [Changed conjectures to statements in this entry. - _Franklin T. Adams-Watters_, Jul 02 2015]",
				"G.f. = x + 2*x^2 + x^3 + 2*x^4 + 3*x^5 + 2*x^6 + 3*x^7 + 4*x^8 + x^9 + 2*x^10 + ..."
			],
			"maple": [
				"seq(convert(convert(n,base,3),`+`),n=0..100); # _Robert Israel_, Jul 02 2015"
			],
			"mathematica": [
				"Table[Plus @@ IntegerDigits[n, 3], {n, 0, 100}] (* or *)",
				"Nest[Join[#, # + 1, # + 2] \u0026, {0}, 6] (* _Robert G. Wilson v_, Jul 27 2006 and modified Jul 27 2014 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, 0, a(n\\3) + n%3)}; /* _Michael Somos_, Mar 06 2004 */",
				"(PARI) A053735(n)=sumdigits(n,3) \\\\ Requires version \u003e= 2.7. Use sum(i=1,#n=digits(n,3),n[i]) in older versions. - _M. F. Hasler_, Mar 15 2016",
				"(Haskell)",
				"a053735 = sum . a030341_row",
				"-- _Reinhard Zumkeller_, Feb 21 2013, Feb 19 2012",
				"(Scheme) (define (A053735 n) (let loop ((n n) (s 0)) (if (zero? n) s (let ((d (mod n 3))) (loop (/ (- n d) 3) (+ s d)))))) ;; For R6RS standard. Use modulo instead of mod in older Schemes like MIT/GNU Scheme. - _Antti Karttunen_, Jun 03 2017",
				"(MAGMA) [\u0026+Intseq(n,3):n in [0..104]]; // _Marius A. Burtea_, Jan 17 2019",
				"(MATLAB) m=1; for u=0:104; sol(m)=sum(dec2base(u,3)-'0'); m=m+1;end",
				"sol; % _Marius A. Burtea_, Jan 17 2019"
			],
			"xref": [
				"Cf. A065363, A007089, A173523. See A134451 for iterations.",
				"Cf. A003137, A138530.",
				"Sum of digits of n written in bases 2-16: A000120, this sequence, A053737, A053824, A053827, A053828, A053829, A053830, A007953, A053831, A053832, A053833, A053834, A053835, A053836.",
				"Related base-3 sequences: A006047, A230641, A230642, A230643, A230853, A230854, A230855, A230856, A230639, A230640, A010063 (trajectory of 1), A286585, A286632, A289813, A289814."
			],
			"keyword": "base,nonn,easy",
			"offset": "0,3",
			"author": "_Henry Bottomley_, Mar 28 2000",
			"references": 100,
			"revision": 121,
			"time": "2022-01-01T09:52:39-05:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}