{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A290812",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 290812,
			"data": "91,247,325,343,485,703,871,901,931,949,1099,1111,1157,1247,1261,1271,1387,1445,1525,1649,1765,1807,1891,1975,2047,2059,2071,2117,2501,2701,2863,2871,3277,3281,3365,3589,3845,4069,4141,4187,4291,4371,4411,4525",
			"name": "Odd composite numbers n such that k^(n - 1) == 1 (mod n) and gcd(k^((n - 1)/2) - 1, n) = 1 for some integer k in the interval [2, sqrt(n) + 1].",
			"comment": [
				"If the condition \"odd composite numbers\" in the definition is replaced by \"odd numbers\", then every odd prime number is in the sequence.",
				"This is not a subsequence of A290543 (for example, 65683 is missing in A290543)."
			],
			"link": [
				"Arkadiusz Wesolowski and Giovanni Resta, \u003ca href=\"/A290812/b290812.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (first 1000 terms from A. Wesolowski)",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Pocklington_primality_test\"\u003ePocklington primality test\u003c/a\u003e",
				"\u003ca href=\"/index/Ps#pseudoprimes\"\u003eIndex entries for sequences related to pseudoprimes\u003c/a\u003e"
			],
			"example": [
				"91 is in the sequence because:",
				"1) it is an odd composite number.",
				"2) k^90 == 1 (mod 91) and gcd(k^45 - 1, 91) = 1 with k = 10 \u003c sqrt(91) + 1."
			],
			"mathematica": [
				"Select[Range[3, 4525, 2], Function[n, And[CompositeQ@ n, AnyTrue[Range[2, Sqrt[n] + 1], And[PowerMod[#, n - 1, n] == 1, CoprimeQ[#^((n - 1)/2) - 1, n]] \u0026]]]] (* _Michael De Vlieger_, Aug 16 2017 *)"
			],
			"program": [
				"(MAGMA) lst:=[]; for n in [3..4525 by 2] do if not IsPrime(n) then for a in [2..Floor(Sqrt(n)+1)] do if Modexp(a, n-1, n) eq 1 and GCD(a^Truncate((n-1)/2)-1, n) eq 1 then Append(~lst, n); break; end if; end for; end if; end for; lst;",
				"(PARI) is(n) = if(n \u003e 1 \u0026\u0026 n%2==1 \u0026\u0026 !ispseudoprime(n), for(x=2, sqrt(n)+1, if(Mod(x, n)^(n-1)==1 \u0026\u0026 gcd(x^((n-1)/2)-1, n)==1, return(1)))); 0 \\\\ _Felix Fröhlich_, Aug 18 2017"
			],
			"xref": [
				"Cf. A130569, A290543."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Arkadiusz Wesolowski_, Aug 11 2017",
			"references": 1,
			"revision": 18,
			"time": "2017-08-20T23:26:30-04:00",
			"created": "2017-08-20T23:26:30-04:00"
		}
	]
}