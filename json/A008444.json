{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A008444",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 8444,
			"data": "1,20,30,60,60,120,40,180,150,140,130,240,180,360,120,260,220,480,210,400,360,240,360,660,200,620,240,600,540,600,240,640,630,720,320,780,420,1080,600,480,650,840,360,1260,720,840,440,1380,660,860,630,640,1080,1560,400",
			"name": "Theta series of A_4 lattice.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"reference": [
				"J. H. Conway and N. J. A. Sloane, \"Sphere Packings, Lattices and Groups\", Springer-Verlag, p. 110."
			],
			"link": [
				"John Cannon, \u003ca href=\"/A008444/b008444.txt\"\u003eTable of n, a(n) for n = 0..5000\u003c/a\u003e",
				"G. Nebe and N. J. A. Sloane, \u003ca href=\"http://www.math.rwth-aachen.de/~Gabriele.Nebe/LATTICES/A4.html\"\u003eHome page for this lattice\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of f(-x)^5 / f(-x^5) + 25 * x * f(-x^5)^5 / f(-x) in powers of x where f() is a Ramanujan theta function. - _Michael Somos_, Feb 06 2011",
				"Expansion of (1 / Pi) integral_{0 .. Pi/2} theta_3(z, q)^5 + theta_4(z, q)^5 dz in powers of q^2. - _Michael Somos_, Jan 01 2012",
				"Coefficient of x^0 in the expansion f(x * q, q / x)^5 in powers of q^2 where f() is a Ramanujan theta function. - _Michael Somos_, Jan 01 2012",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (5 t)) = 5^(3/2) (t/i)^2 g(t) where q = exp(2 Pi i t) and g() is the g.f. for A023916. - _Michael Somos_, Feb 06 2011",
				"A023916(5*n) = a(n) for all n in Z."
			],
			"example": [
				"G.f. = 1 + 20*x + 30*x^2 + 60*x^3 + 60*x^4 + 120*x^5 + 40*x^6 + 180*x^7 + ...",
				"G.f. = 1 + 20*q^2 + 30*q^4 + 60*q^6 + 60*q^8 + 120*q^10 + 40*q^12 + 180*q^14 + 150*q^16 + 140*q^18 + 130*q^20 + 240*q^22 + 180*q^24 + 360*q^26 + 120*q^28 + 260*q^30 + 220*q^32 + 480*q^34 + 210*q^36 + 400*q^38 + 360*q^40 + 240*q^42 + 360*q^44 + 660*q^46 + 200*q^48 + 620*q^50 + ..."
			],
			"mathematica": [
				"a[ n_] := With[ {u1 = QPochhammer[ x], u5 = QPochhammer[ x^5]}, SeriesCoefficient[ u1^5/u5 + 25 x u5^5/u1, {x, 0, n}]]; (* _Michael Somos_, Nov 13 2014 *)",
				"terms = 55; f[q_] = LatticeData[\"A4\", \"ThetaSeriesFunction\"][-I Log[q]/Pi]; s = Series[f[q], {q, 0, 2 terms}]; CoefficientList[s, q^2][[1 ;; terms]] (* _Jean-François Alcover_, Jul 04 2017 *)"
			],
			"program": [
				"(MAGMA) L := Lattice(\"A\", 4); A\u003cq\u003e := ThetaSeries(L, 120); A;",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A)^5 / eta(x^5 + A) + 25 * x * eta(x^5 + A)^5 / eta(x + A), n))}; /* _Michael Somos_, Feb 06 2011 */",
				"(MAGMA) A := Basis( ModularForms( Gamma1(5), 2), 55) ; A[1] + 20*A[2] + 30*A[3]; /* _Michael Somos_, Nov 13 2014 */"
			],
			"xref": [
				"Cf. A000007, A000122, A004016, A004015, A008445, A008446, A008447, A008448, A008449 (Theta series of lattices A_0, A_1, A_2, A_3, A_5, ...)."
			],
			"keyword": "nonn,nice",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 5,
			"revision": 41,
			"time": "2021-12-16T22:36:00-05:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}