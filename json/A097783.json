{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A097783",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 97783,
			"data": "1,12,131,1429,15588,170039,1854841,20233212,220710491,2407582189,26262693588,286482047279,3125039826481,34088956044012,371853476657651,4056299287190149,44247438682433988,482665526219583719,5265073349732986921,57433141320843272412",
			"name": "Chebyshev polynomials S(n,11) + S(n-1,11) with Diophantine property.",
			"comment": [
				"All positive integer solutions of Pell equation (3*a(n))^2 - 13*b(n)^2 = -4 together with b(n)=A078922(n+1), n\u003e=0.",
				"a(n) = L(n,-11)*(-1)^n, where L is defined as in A108299; see also A078922 for L(n,+11). - _Reinhard Zumkeller_, Jun 01 2005"
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A097783/b097783.txt\"\u003eTable of n, a(n) for n = 0..963\u003c/a\u003e",
				"Andersen, K., Carbone, L. and Penta, D., \u003ca href=\"https://pdfs.semanticscholar.org/8f0c/c3e68d388185129a56ed73b5d21224659300.pdf\"\u003eKac-Moody Fibonacci sequences, hyperbolic golden ratios, and real quadratic fields\u003c/a\u003e, Journal of Number Theory and Combinatorics, Vol 2, No. 3 pp 245-278, 2011. See Section 9.",
				"S. Falcon, \u003ca href=\"http://dx.doi.org/10.4236/am.2014.515216\"\u003eRelationships between Some k-Fibonacci Sequences, Applied Mathematics\u003c/a\u003e, 2014, 5, 2226-2234.",
				"Alex Fink, Richard K. Guy, and Mark Krusemeyer, \u003ca href=\"https://cdm.ucalgary.ca/article/view/61940\"\u003ePartitions with parts occurring at most thrice\u003c/a\u003e, Contributions to Discrete Mathematics, Vol 3, No 2 (2008), pp. 76-114. See Section 13.",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"Giovanni Lucca, \u003ca href=\"http://forumgeom.fau.edu/FG2019volume19/FG201902index.html\"\u003eInteger Sequences and Circle Chains Inside a Hyperbola\u003c/a\u003e, Forum Geometricorum (2019) Vol. 19, 11-16.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/FibonacciPolynomial.html\"\u003eFibonacci Polynomial\u003c/a\u003e",
				"H. C. Williams and R. K. Guy, \u003ca href=\"http://dx.doi.org/10.1142/S1793042111004587\"\u003eSome fourth-order linear divisibility sequences\u003c/a\u003e, Intl. J. Number Theory 7 (5) (2011) 1255-1277.",
				"H. C. Williams and R. K. Guy, \u003ca href=\"http://www.emis.de/journals/INTEGERS/papers/a17self/a17self.Abstract.html\"\u003eSome Monoapparitic Fourth Order Linear Divisibility Sequences\u003c/a\u003e Integers, Volume 12A (2012) The John Selfridge Memorial Volume.",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (11,-1)."
			],
			"formula": [
				"a(n) = S(n, 11) + S(n-1, 11) = S(2*n, sqrt(13)), with S(n, x)=U(n, x/2) Chebyshev's polynomials of the 2nd kind, A049310. S(-1, x) = 0 = U(-1, x).",
				"a(n) = (-2/3)*i*((-1)^n)*T(2*n+1, 3*i/2) with the imaginary unit i and Chebyshev's polynomials of the first kind. See the T-triangle A053120.",
				"G.f.: (1+x)/(1-11*x+x^2).",
				"a(n) = 11*a(n-1) - a(n-2) with a(0)=1 and a(1)=12. - _Philippe Deléham_, Nov 17 2008",
				"From _Peter Bala_, Mar 22 2015: (Start)",
				"The aerated sequence (b(n))n\u003e=1 = [1, 0, 12, 0, 131, 0, 1429, 0, ...] is a fourth-order linear divisibility sequence; that is, if n | m then b(n) | b(m). It is the case P1 = 0, P2 = -9, Q = -1 of the 3-parameter family of divisibility sequences found by Williams and Guy. See A100047 for the connection with Chebyshev polynomials.",
				"b(n) = 1/2*( (-1)^n - 1 )*F(n,3) + 1/3*( 1 + (-1)^(n+1) )*F(n+1,3), where F(n,x) is the n-th Fibonacci polynomial. The o.g.f. is x*(1 + x^2)/(1 - 11*x^2 + x^4).",
				"Exp( Sum_{n \u003e= 1} 6*b(n)*x^n/n ) = 1 + Sum_{n \u003e= 1} 6*A006190(n)*x^n.",
				"Exp( Sum_{n \u003e= 1} (-6)*b(n)*x^n/n ) = 1 + Sum_{n \u003e= 1} 6*A006190(n)*(-x)^n. Cf. A002315, A004146, A113224 and A192425. (End)"
			],
			"example": [
				"All positive solutions to the Pell equation x^2 - 13*y^2 = -4 are (3=3*1,1), (36=3*12,10), (393=3*131,109), (4287=3*1429,1189 ), ..."
			],
			"mathematica": [
				"CoefficientList[Series[(1 + x) / (1 - 11 x + x^2), {x, 0, 33}], x] (* _Vincenzo Librandi_, Mar 22 2015 *)"
			],
			"program": [
				"(Sage) [(lucas_number2(n,11,1)-lucas_number2(n-1,11,1))/9 for n in range(1, 19)] # _Zerinvary Lajos_, Nov 10 2009",
				"(PARI) Vec((1+x)/(1-11*x+x^2) + O(x^30)) \\\\ _Michel Marcus_, Mar 22 2015",
				"(MAGMA) I:=[1,12]; [n le 2 select I[n] else 11*Self(n-1)-Self(n-2): n in [1..30]]; // _Vincenzo Librandi_, Mar 22 2015"
			],
			"xref": [
				"Cf. S(n, 11) = A004190(n).",
				"Cf. A000045, A002315, A004146, A006190, A100047, A113224, A192425."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Aug 31 2004",
			"references": 9,
			"revision": 49,
			"time": "2020-01-23T03:46:23-05:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}