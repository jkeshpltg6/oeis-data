{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A098279",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 98279,
			"data": "1,2,10,98,1594,38834,1323658,60134210,3511695322,256306614866,22861774551466,2446866564603362,309483997093321210,45666236465616727538,7774748058886412485834",
			"name": "a(n) = D(n,1)/2^n, where D(n,x) is triangle A098277.",
			"link": [
				"Vaclav Kotesovec, \u003ca href=\"/A098279/b098279.txt\"\u003eTable of n, a(n) for n = 0..250\u003c/a\u003e",
				"Ange Bigeni, \u003ca href=\"https://arxiv.org/abs/1705.03804\"\u003eEnumerating the symplectic Dellac configurations\u003c/a\u003e, arXiv:1705.03804 [math.CO], 2017.",
				"Ange Bigeni, Evgeny Feigin, \u003ca href=\"https://arxiv.org/abs/1804.10804\"\u003ePoincaré polynomials of the degenerate flag varieties of type C\u003c/a\u003e, arXiv:1804.10804 [math.CO], 2018.",
				"Ange Bigeni, Evgeny Feigin, \u003ca href=\"https://arxiv.org/abs/1808.04275\"\u003eSymmetric Dellac configurations\u003c/a\u003e, arXiv:1808.04275 [math.CO], 2018.",
				"Xin Fang and Ghislain Fourier, \u003ca href=\"http://arxiv.org/abs/1504.03980\"\u003eTorus fixed points in Schubert varieties and Genocchi numbers\u003c/a\u003e, arXiv:1504.03980 [math.RT], 2015.",
				"A. Randrianarivony and J. Zeng, \u003ca href=\"http://dx.doi.org/10.1006/aama.1996.0001\"\u003eUne famille de polynomes qui interpole plusieurs suites classiques de nombres\u003c/a\u003e, Adv. Appl. Math. 17 (1996), 1-26."
			],
			"formula": [
				"G.f.: Sum_{n\u003e=0} a(n)*x^n = 1/(1-1*2*x/(1-1*3*x/(1-2*4*x/(1-2*5*x/...)))).",
				"G.f.: Sum_{n\u003e=0} n!*(n+1)! * x^n / Product_{k=1..n} (1 + k*(k+1)/2*x). - _Paul D. Hanna_, Sep 05 2012",
				"G.f.: 2*G(0) - 1 where G(k) =  1 + x*(2*k+1)*(4*k+1)/( 1 + x + 6*x*k + 8*x*k^2 - 2*x*(k+1)*(4*k+3)*(1 + x + 6*x*k + 8*x*k^2)/(2*x*(k+1)*(4*k+3) + (1 + 6*x + 14*x*k + 8*x*k^2)/G(k+1) )); (recursively defined continued fraction). - _Sergei N. Gladkovskii_, Feb 11 2013",
				"a(n) ~ 2^(3*n+11/2) * n^(2*n+2) / (exp(2*n) * Pi^(2*n+3/2)). - _Vaclav Kotesovec_, Apr 23 2015"
			],
			"example": [
				"G.f.: A(x) = 1 + 2*x + 10*x^2 + 98*x^3 + 1594*x^4 + 38834*x^5 +...",
				"where",
				"A(x) = 1 + 2!*x/(1+x) + 2!*3!*x^2/((1+x)*(1+3*x)) + 3!*4!*x^3/((1+x)*(1+3*x)*(1+6*x)) + 4!*5!*x^4/((1+x)*(1+3*x)*(1+6*x)*(1+10*x)) + ...  - Paul D. Hanna, Sep 05 2012"
			],
			"mathematica": [
				"d[0, _] = 1;",
				"d[n_, x_] := d[n, x] = (x+1)(x+2) d[n-1, x+2] - x(x+1) d[n-1, x];",
				"a[n_] := d[n, 1]/2^n;",
				"Table[a[n], {n, 0, 15}] (* _Jean-François Alcover_, Jul 27 2018 *)"
			],
			"program": [
				"(PARI) {a(n)=polcoeff(sum(m=0, n, m!*(m+1)!*x^m/prod(k=1, m, 1+k*(k+1)/2*x +x*O(x^n))), n)} \\\\ _Paul D. Hanna_, Sep 05 2012"
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Ralf Stephan_, Sep 07 2004",
			"references": 2,
			"revision": 36,
			"time": "2018-08-14T04:31:54-04:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}