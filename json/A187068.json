{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A187068",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 187068,
			"data": "1,0,0,0,1,1,1,2,3,5,6,11,14,25,31,56,70,126,157,283,353,636,793,1429,1782,3211,4004,7215,8997,16212,20216,36428,45425,81853,102069,183922,229347,413269,515338,928607,1157954,2086561,2601899",
			"name": "Let i be in {1,2,3}, let r \u003e= 0 be an integer and n=2*r+i-1. Then a(n)=a(2*r+i-1) gives the quantity of H_(7,1,0) tiles in a subdivided H_(7,i,r) tile after linear scaling by the factor x^r, where x=sqrt((2*cos(Pi/7))^2-1).",
			"comment": [
				"(Start) See A187070 for supporting theory. Define the matrix",
				"U_2=",
				"(0 0 1)",
				"(0 1 1)",
				"(1 1 1).",
				"Let r\u003e=0, and let A_r be the r-th \"block\" defined by A_r={a(2*r),a(2*r+1),a(2*r+2)}. Note that A_r-2*A_(r-1)-A_(r-2)+A_(r-3)={0,0,0}. Let n=2*r+i-1 and M=(m_(i,j))=(U_2)^r. Then A_r corresponds component-wise to the first column of M, and a(n)=a(2*r+i-1)=m_(i,1) gives the quantity of H_(7,1,0) tiles that should appear in a subdivided H_(7,i,r) tile.  (End)",
				"Since a(2*r+2)=a(2*(r+1)) for all r, this sequence arises by concatenation of first-column entries m_(1,1) and m_(2,1) from successive matrices M=(U_2)^r.",
				"This sequence is a nontrivial extension of both A038196 and A187070."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A187068/b187068.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"L. E. Jeffery, \u003ca href=\"/wiki/User:L._Edson_Jeffery/Unit-Primitive_Matrices\"\u003eUnit-primitive matrices\u003c/a\u003e",
				"Roman Witula, Damian Slota and Adam Warzynski, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL9/Slota/slota57.html\"\u003eQuasi-Fibonacci Numbers of the Seventh Order\u003c/a\u003e, J. Integer Seq., 9 (2006), Article 06.4.3.",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,2,0,1,0,-1)."
			],
			"formula": [
				"{a(n+2)} = A187070.",
				"a(n) = 2*a(n-2) + a(n-4) - a(n-6).",
				"G.f.: (1-2*x^2+x^5)/(1-2*x^2-x^4+x^6).",
				"Closed-form: a(n) = (1/14)*[[X_1+Y_1*(-1)^(n-1)]*[(w_2)^2-(w_3)^2]*(w_1)^(n-1)+[X_2+Y_2*(-1)^(n-1)]*[(w_3)^2-(w_1)^2]*(w_2)^(n-1)+[X_3+Y_3*(-1)^(n-1)]*[(w_1)^2-(w_2)^2]*(w_3)^(n-1)], where w_k = sqrt[(2cos(k*Pi/7))^2-1], X_k = (w_k)^5-2*(w_k)^3+1 and Y_k = -(w_k)^5+2*(w_k)^3+1, k=1,2,3."
			],
			"example": [
				"(Start) Suppose r=3. Then",
				"A_r = A_3 = {a(2*r,a(2*r+1),a(2*r+2)} = {a(6),a(7),a(8)} = {1,2,3},",
				"corresponding to the entries in the first column of",
				"M = m_(i,j) = (U_2)^3 =",
				"(1 2 3)",
				"(2 4 5)",
				"(3 5 6).",
				"Suppose i=2. Setting n=2*r+i-1, then a(n) = a(2*r+i-1) = a(6+2-1) = a(7) = m_(2,1) = 2. Hence a subdivided H_(7,2,3) tile should contain a(7) = m_(2,1) = 2 H_(7,1,0) tiles. (End)"
			],
			"mathematica": [
				"a[0] = 1; a[1] = a[2] = a[3] = 0; a[4] = a[5] = 1; a[_?Negative] = 0; a[n_] := a[n] = 2*a[n-2] + a[n-4] - a[n-6]; Table[a[n], {n, 0, 42}] (* _Jean-François Alcover_, Jan 02 2013 *)",
				"CoefficientList[Series[(1 - 2*x^2 + x^5)/(1 - 2*x^2 - x^4 + x^6), {x, 0, 50}], x] (* _G. C. Greubel_, Jul 06 2017 *)"
			],
			"program": [
				"(PARI) x='x+O('x^50); Vec((1-2*x^2+x^5)/(1-2*x^2-x^4+x^6)) \\\\ _G. C. Greubel_, Jul 06 2017"
			],
			"xref": [
				"Cf. A038196, A187069, A187070."
			],
			"keyword": "nonn,easy",
			"offset": "0,8",
			"author": "_L. Edson Jeffery_, Mar 06 2011",
			"references": 9,
			"revision": 74,
			"time": "2019-02-03T16:51:33-05:00",
			"created": "2011-03-03T06:12:07-05:00"
		}
	]
}