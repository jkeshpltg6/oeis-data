{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A235998",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 235998,
			"data": "1,2,2,2,3,5,2,14,4,22,6,2,44,18,4,68,56,3,107,146,4,172,312,24,2,261,677,84,6,396,1358,288,2,606,2666,822,4,950,5012,2226,4,1414,9542,5304,120,5,2238,17531,12514,480,2,3418,32412,27904,1800,6,5411,58995,61080,5580",
			"name": "Triangle read by rows: T(n,k) is the number of compositions of n having k distinct parts (n\u003e=1, 1\u003c=k\u003c=floor((sqrt(1+8*n)-1)/2)).",
			"comment": [
				"Row n has length A003056(n) hence the first element of column k is in row A000217(k).",
				"The equivalent sequence for partitions is A116608.",
				"For the number of compositions of n see A011782.",
				"For the connection to overcompositions see A235999.",
				"Row sums give A011782(n), n \u003e= 1.",
				"First column is A000005, second column is A131661.",
				"T(k*(k+1)/2,k) = T(A000217(k),k) = A000142(k) = k!. - _Alois P. Heinz_, Jan 20 2014"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A235998/b235998.txt\"\u003eRows n = 1..500, flattened\u003c/a\u003e"
			],
			"example": [
				"Triangle begins:",
				"  1;",
				"  2;",
				"  2,    2;",
				"  3,    5;",
				"  2,   14;",
				"  4,   22,     6;",
				"  2,   44,    18;",
				"  4,   68,    56;",
				"  3,  107,   146;",
				"  4,  172,   312,    24;",
				"  2,  261,   677,    84;",
				"  6,  396,  1358,   288;",
				"  2,  606,  2666,   822;",
				"  4,  950,  5012,  2226;",
				"  4, 1414,  9542,  5304,  120;",
				"  5, 2238, 17531, 12514,  480;",
				"  2, 3418, 32412, 27904, 1800;",
				"  6, 5411, 58995, 61080, 5580;",
				"  ..."
			],
			"maple": [
				"b:= proc(n, i, p) option remember; `if`(n=0, p!, `if`(i\u003c1, 0,",
				"      expand(add(b(n-i*j, i-1, p+j)/j!*`if`(j=0, 1, x), j=0..n/i))))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=1..degree(p)))(b(n$2, 0)):",
				"seq(T(n), n=1..25); # _Alois P. Heinz_, Jan 20 2014, revised May 25 2014"
			],
			"mathematica": [
				"b[n_, i_, p_] := b[n, i, p] = If[n==0, p!, If[i\u003c1, 0, Sum[b[n-i*j, i-1, p+ j]/j!*If[j==0, 1, x], {j, 0, n/i}]]]; T[n_] := Function[p, Table[ Coefficient[p, x, i], {i, 1, Exponent[p, x]}]][b[n, n, 0]]; Table[T[n], {n, 1, 25}] // Flatten (* _Jean-François Alcover_, Dec 10 2015, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A003056, A116608, A235790, A235999, A236002."
			],
			"keyword": "nonn,tabf,nice,look",
			"offset": "1,2",
			"author": "_Omar E. Pol_, Jan 19 2014",
			"ext": [
				"More terms from _Alois P. Heinz_, Jan 19 2014"
			],
			"references": 10,
			"revision": 54,
			"time": "2021-10-18T08:18:36-04:00",
			"created": "2014-01-20T18:10:35-05:00"
		}
	]
}