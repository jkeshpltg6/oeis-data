{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A279561",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 279561,
			"data": "1,1,2,6,21,77,287,1079,4082,15522,59280,227240,873886,3370030,13027730,50469890,195892565,761615285,2965576715,11563073315,45141073925,176423482325,690215089745,2702831489825,10593202603775,41550902139551,163099562175851",
			"name": "Number of length n inversion sequences avoiding the patterns 101, 102, 201, and 210.",
			"comment": [
				"A length n inversion sequence e_1e_2...e_n is a sequence of integers where 0 \u003c= e_i \u003c= i-1. The term a(n) counts those length n inversion sequences with no entries e_i, e_j, e_k (where i\u003cj\u003ck) such that e_i \u003e e_j \u003c\u003e e_k. This is the same as the set of length n inversion sequences avoiding 101, 102, 201, and 210.",
				"It is conjectured that a_n also counts those length n inversion sequences with no entries e_i, e_j, e_k (where i\u003cj\u003ck) such that e_i \u003c e_j \u003e e_k and e_i \u003c\u003e e_k. This is the same as the set of length n inversion sequences avoiding 021 and 120."
			],
			"link": [
				"Shane Chern, \u003ca href=\"https://arxiv.org/abs/2006.04318\"\u003eOn 0012-avoiding inversion sequences and a Conjecture of Lin and Ma\u003c/a\u003e, arXiv:2006.04318 [math.CO], 2020.",
				"Megan A. Martinez, Carla D. Savage, \u003ca href=\"https://arxiv.org/abs/1609.08106\"\u003ePatterns in Inversion Sequences II: Inversion Sequences Avoiding Triples of Relations\u003c/a\u003e, arXiv:1609.08106 [math.CO], 2016.",
				"Chunyan Yan, Zhicong Lin, \u003ca href=\"https://arxiv.org/abs/1912.03674\"\u003eInversion sequences avoiding pairs of patterns\u003c/a\u003e, arXiv:1912.03674 [math.CO], 2019."
			],
			"formula": [
				"a(n) = 1 + Sum_{i=1..n-1} binomial(2i, i-1).",
				"a(n) = 1 + A057552(n-2).",
				"G.f.: (1-4*x+sqrt(-16*x^3+20*x^2-8*x+1))/(2*(x-1)*(4*x-1)).",
				"D-finite with recurrence: n*a(n) +(-7*n+6)*a(n-1) +2*(7*n-13)*a(n-2) +4*(-2*n+5)*a(n-3)=0. - _R. J. Mathar_, Feb 21 2020"
			],
			"example": [
				"The length 4 inversion sequences avoiding (101, 102, 201, 210) are 0000, 0001, 0002, 0003, 0010, 0011, 0012, 0013, 0020, 0021, 0022, 0023, 0100, 0110, 0111, 0112, 0113, 0120, 0121, 0122, 0123.",
				"The length 4 inversion sequences avoiding (021, 120) are 0000, 0001, 0002, 0003, 0010, 0011, 0012, 0013, 0020, 0022, 0023, 0100, 0101, 0102, 0103, 0110, 0111, 0112, 0113, 0122, 0123."
			],
			"maple": [
				"a:= proc(n) option remember; `if`(n\u003c3, 1+n*(n-1)/2,",
				"      ((5*n^2-12*n+6)*a(n-1)-(4*n^2-10*n+6)*a(n-2))/((n-2)*n))",
				"    end:",
				"seq(a(n), n=0..30);  # _Alois P. Heinz_, Jan 18 2017"
			],
			"mathematica": [
				"a[n_] := 1 + Sum[Binomial[2i, i-1], {i, 0, n-1}];",
				"Table[a[n], {n, 0, 30}] (* _Jean-François Alcover_, Mar 28 2017 *)"
			],
			"xref": [
				"Cf. A000108, A057552, A263777, A263778, A263779, A263780, A279551, A279552, A279553, A279554, A279555, A279556, A279557, A279558, A279559, A279560, A279562, A279563, A279564, A279565, A279566, A279567, A279568, A279569, A279570, A279571, A279572, A279573."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Megan A. Martinez_, Jan 17 2017",
			"references": 23,
			"revision": 27,
			"time": "2020-06-09T07:22:53-04:00",
			"created": "2017-01-18T12:15:40-05:00"
		}
	]
}