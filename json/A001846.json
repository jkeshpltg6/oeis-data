{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001846",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1846,
			"id": "M4622 N1974",
			"data": "1,9,41,129,321,681,1289,2241,3649,5641,8361,11969,16641,22569,29961,39041,50049,63241,78889,97281,118721,143529,172041,204609,241601,283401,330409,383041,441729,506921,579081,658689,746241,842249,947241,1061761,1186369",
			"name": "Centered 4-dimensional orthoplex numbers (crystal ball sequence for 4-dimensional cubic lattice).",
			"comment": [
				"a(n) is the number of points in the Z^4 lattice that are at distance at most n from the origin in the adjacency graph. - _N. J. A. Sloane_, Feb 19 2013",
				"Number of nodes of degree 8 in virtual, optimal, chordal graphs of diameter d(G)=n. - S. Bujnowski \u0026 B. Dubalski (slawb(AT)atr.bydgoszcz.pl), Mar 07 2002",
				"If Y_i (i=1,2,3,4) are 2-blocks of an (n+4)-set X then a(n-4) is the number of 8-subsets of X intersecting each Y_i (i=1,2,3,4). - _Milan Janjic_, Oct 28 2007",
				"Equals binomial transform of [1, 8, 24, 32, 16, 0, 0, 0, ...] where (1, 8, 24, 32, 16) = row 4 of the Chebyshev triangle A013609. - _Gary W. Adamson_, Jul 19 2008",
				"Comment from Ben Thurston, Feb 18 2013: In the plane, if you make a picture by taking one unit step in each of the basic 8 directions from a central dot, then from each of those going one unit step in each of the eight directions, ... (see illustration), it appears that the number of dots in the picture after n steps is equal to a(n). Response from _N. J. A. Sloane_, Feb 19 2013: This is correct, and follows from the fact that the Z-module Z[1,i,(+-1+i)/sqrt(2)] is essentially a copy of the Z^4 lattice.",
				"a(n) = D(4,n) where D are the Delannoy numbers (A008288). As such, a(n) gives the number of grid paths from (0,0) to (4,n) using steps that move one unit north, east, or northeast. - _Jack W Grahl_, Feb 15 2021"
			],
			"reference": [
				"L. Comtet, Advanced Combinatorics, Reidel, 1974, p. 81.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A001846/b001846.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"D. Bump, K. Choi, P. Kurlberg, and J. Vaaler, \u003ca href=\"http://www.cecm.sfu.ca/~choi/paper/lrh.pdf\"\u003eA local Riemann hypothesis, I\u003c/a\u003e pages 16 and 17",
				"J. H. Conway and N. J. A. Sloane, Low-Dimensional Lattices VII: Coordination Sequences, Proc. Royal Soc. London, A453 (1997), 2369-2389 (\u003ca href=\"http://neilsloane.com/doc/Me220.pdf\"\u003epdf\u003c/a\u003e).",
				"Milan Janjic, \u003ca href=\"http://www.pmfbl.org/janjic/\"\u003eTwo Enumerative Functions\u003c/a\u003e",
				"G. Kreweras, \u003ca href=\"http://www.numdam.org/item?id=BURO_1973__20__3_0\"\u003eSur les hiérarchies de segments\u003c/a\u003e, Cahiers du Bureau Universitaire de Recherche Opérationnelle, Institut de Statistique, Université de Paris, #20 (1973).",
				"G. Kreweras, \u003ca href=\"/A001844/a001844.pdf\"\u003eSur les hiérarchies de segments\u003c/a\u003e, Cahiers du Bureau Universitaire de Recherche Opérationnelle, Institut de Statistique, Université de Paris, #20 (1973). (Annotated scanned copy)",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"R. G. Stanton and D. D. Cowan, \u003ca href=\"http://dx.doi.org/10.1137/1012049\"\u003eNote on a \"square\" functional equation\u003c/a\u003e, SIAM Rev., 12 (1970), 277-279.",
				"Ben Thurston, \u003ca href=\"/A001846/a001846.png\"\u003eIllustration of the first four clusters of points in two dimensions\u003c/a\u003e",
				"\u003ca href=\"/index/Cor#crystal_ball\"\u003eIndex entries for crystal ball sequences\u003c/a\u003e"
			],
			"formula": [
				"G.f.: (1+x)^4 /(1-x)^5.",
				"a(n) = (2*n^4+4*n^3+10*n^2+8*n+3)/3. - S. Bujnowski \u0026 B. Dubalski (slawb(AT)atr.bydgoszcz.pl), Mar 07 2002",
				"a(n) = SUM[i=0..n] A008412(i); a(n) = SUM[i=0..n] (8*i)*(i^2+2)/3; a(n) = SUM[i=0..n] (8*i)*(A059100(i))/3. - _Jonathan Vos Post_, Mar 15 2006",
				"a(n) = sum(k=0..min(4,n), 2^k * binomial(4,k)* binomial(n,k) ). See Bump et al. - _Tom Copeland_, Sep 05 2014"
			],
			"example": [
				"a(6)=1289: (2*6^4 + 4*6^3 + 10*6^2 + 8*6 + 3) / 3 = (2592 + 864 + 360 + 48 + 3) / 3 = 3867 / 3 = 1289."
			],
			"maple": [
				"for n from 1 to k do eval((2*n^4+4*n^3+10*n^2+8*n+3)/3) od;",
				"A001846:=-(z+1)**4/(z-1)**5; # conjectured (correctly) by _Simon Plouffe_ in his 1992 dissertation"
			],
			"mathematica": [
				"CoefficientList[Series[(-z^4-4 z^3-6 z^2-4 z-1)/(z-1)^5, {z, 0, 200}], z] (* _Vladimir Joseph Stephan Orlovsky_, Jun 19 2011 *)"
			],
			"xref": [
				"Cf. A005408, A001844, A001845, A001847, A059100, A013609.",
				"First differences are A008412.",
				"Cf. A240876."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 10,
			"revision": 86,
			"time": "2021-03-12T22:32:35-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}