{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A169793",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 169793,
			"data": "1,6,27,104,363,1182,3653,10836,31092,86784,236640,632448,1661056,4296192,10961664,27630592,68889600,170065920,416071680,1009582080,2431254528,5814222848,13815054336,32629850112,76640681984,179080003584,416412598272,963876225024",
			"name": "Expansion of ((1-x)/(1-2*x))^6.",
			"comment": [
				"a(n) is the number of weak compositions of n with exactly 5 parts equal to 0. - _Milan Janjic_, Jun 27 2010",
				"Except for an initial 1, this is the p-INVERT of (1,1,1,1,1,...) for p(S) = (1 - S)^6; see A291000.  - _Clark Kimberling_, Aug 24 2017"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A169793/b169793.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Robert Davis, Greg Simay, \u003ca href=\"https://arxiv.org/abs/2001.11089\"\u003eFurther Combinatorics and Applications of Two-Toned Tilings\u003c/a\u003e, arXiv:2001.11089 [math.CO], 2020.",
				"Nickolas Hein, Jia Huang, \u003ca href=\"https://arxiv.org/abs/1807.04623\"\u003eVariations of the Catalan numbers from some nonassociative binary operations\u003c/a\u003e, arXiv:1807.04623 [math.CO], 2018.",
				"M. Janjic and B. Petkovic, \u003ca href=\"http://arxiv.org/abs/1301.4550\"\u003eA Counting Function\u003c/a\u003e, arXiv 1301.4550 [math.CO], 2013.",
				"M. Janjic, B. Petkovic, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Janjic/janjic45.html\"\u003eA Counting Function Generalizing Binomial Coefficients and Some Other Classes of Integers\u003c/a\u003e, J. Int. Seq. 17 (2014) # 14.3.5.",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (12,-60,160,-240,192,-64)."
			],
			"formula": [
				"G.f.: ((1-x)/(1-2*x))^6.",
				"For n \u003e 0, a(n) = 2^(n-9)*(n+7)*(n^4 + 38*n^3 + 419*n^2 + 1342*n + 1080)/15. - _Bruno Berselli_, Aug 07 2011"
			],
			"maple": [
				"seq(coeff(series(((1-x)/(1-2*x))^6,x,n+1), x, n), n = 0 .. 30); # _Muniru A Asiru_, Oct 16 2018"
			],
			"mathematica": [
				"CoefficientList[Series[((1 - x)/(1 - 2 x))^6, {x, 0, 27}], x] (* _Michael De Vlieger_, Oct 15 2018 *)"
			],
			"program": [
				"(MAGMA) m:=30; R\u003cx\u003e:=PowerSeriesRing(Integers(), m); Coefficients(R!(((1-x)/(1-2*x))^6)); // _G. C. Greubel_, Oct 16 2018",
				"(PARI) x='x+O('x^30); Vec(((1-x)/(1-2*x))^6) \\\\ _G. C. Greubel_, Oct 16 2018"
			],
			"xref": [
				"Cf. for ((1-x)/(1-2x))^k: A011782, A045623, A058396, A062109, A169792-A169797; a row of A160232."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, May 15 2010",
			"references": 4,
			"revision": 51,
			"time": "2020-04-24T21:01:11-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}