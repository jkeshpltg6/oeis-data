{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A038155",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 38155,
			"data": "0,0,1,6,30,160,975,6846,54796,493200,4932045,54252550,651030666,8463398736,118487582395,1777313736030,28437019776600,483429336202336,8701728051642201,165332832981201990,3306656659624039990,69439789852104840000",
			"name": "a(n) = (n!/2) * Sum_{k=0..n-2} 1/k!.",
			"comment": [
				"For n\u003e=2, a(n) gives the operation count to create all permutations of n distinct elements using Algorithm L (lexicographic permutation generation) from Knuth's The Art of Computer Programming, Vol. 4, chapter 7.2.1.2. Sequence gives the number of comparisons required to find the first interchangeable element in step L3 (see answer to exercise 5). - _Hugo Pfoertner_, Jan 27 2003",
				"a(n) mod 5 = A011658(n+1). - _G. C. Greubel_, Apr 13 2016",
				"a(450) has 1001 decimal digits. - _Michael De Vlieger_, Apr 13 2016",
				"Also the number of (undirected) paths in the complete graph K_n. - _Eric W. Weisstein_, Jun 04 2017"
			],
			"reference": [
				"D. E. Knuth: The Art of Computer Programming, Volume 4, Combinatorial Algorithms, Volume 4A, Enumeration and Backtracking. Pre-fascicle 2B, A draft of section 7.2.1.2: Generating all permutations. Available online; see link."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A038155/b038155.txt\"\u003eTable of n, a(n) for n = 0..449\u003c/a\u003e",
				"D. E. Knuth, \u003ca href=\"http://www-cs-faculty.stanford.edu/~knuth/fasc2b.ps.gz\"\u003eTAOCP Vol. 4, Pre-fascicle 2b (generating all permutations)\u003c/a\u003e.",
				"Hugo Pfoertner, \u003ca href=\"http://www.randomwalk.de/sequences/lpure.txt\"\u003eFORTRAN implementation of Knuth's Algorithm L for lexicographic permutation generation\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CompleteGraph.html\"\u003eComplete Graph\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/GraphPath.html\"\u003eGraph Path\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 1/2*floor(n!*exp(1)-n-1), n\u003e0. - _Vladeta Jovovic_, Aug 18 2002",
				"E.g.f.: x^2/2*exp(x)/(1-x). - _Vladeta Jovovic_, Aug 25 2002",
				"a(n) = Sum_{k=0..n-1} a(n-1) + k, a(0)=0. - _Ilya Gutkovskiy_, Apr 13 2016",
				"a(n) = A038154(n)/2. - _Alois P. Heinz_, Jan 26 2017"
			],
			"maple": [
				"A038155:=n-\u003e(n!/2)*add(1/k!, k=0..n-2): seq(A038155(n), n=0..30); # _Wesley Ivan Hurt_, Apr 16 2016"
			],
			"mathematica": [
				"RecurrenceTable[{a[0] == 0, a[n] == Sum[a[n - 1] + k, {k, 0, n - 1}]}, a, {n, 21}] (* _Ilya Gutkovskiy_, Apr 13 2016 *)",
				"Table[(n!/2) Sum[1/k!, {k, 0, n - 2}], {n, 0, 21}] (* _Michael De Vlieger_, Apr 13 2016 *)",
				"Table[1/2 E (n - 1) n Gamma[n - 1, 1], {n, 0, 20}] (* _Eric W. Weisstein_, Jun 04 2017 *)",
				"Table[If[n == 0, 0, Floor[n! E - n - 1]/2], {n, 0, 20}] (* _Eric W. Weisstein_, Jun 04 2017 *)"
			],
			"xref": [
				"Cf. A011658, A038154, A038156, A056542, A079752, A079884, A080047, A080048, A080049."
			],
			"keyword": "nonn,easy",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_",
			"references": 12,
			"revision": 32,
			"time": "2017-06-04T08:01:16-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}