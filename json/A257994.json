{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A257994",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 257994,
			"data": "0,0,1,0,1,1,0,0,2,1,1,1,0,0,2,0,1,2,0,1,1,1,0,1,2,0,3,0,0,2,1,0,2,1,1,2,0,0,1,1,1,1,0,1,3,0,0,1,0,2,2,0,0,3,2,0,1,0,1,2,0,1,2,0,1,2,1,1,1,1,0,2,0,0,3,0,1,1,0,1,4,1,1,1,2,0,1,1,0,3",
			"name": "Number of prime parts in the partition having Heinz number n.",
			"comment": [
				"We define the Heinz number of a partition p = [p_1, p_2, ..., p_r] as Product(p_j-th prime, j=1...r) (concept used by _Alois P. Heinz_ in A215366 as an \"encoding\" of a partition). For example, for the partition [1, 1, 2, 4, 10] we get 2*2*3*7*29 = 2436.",
				"In the Maple program the subprogram B yields the partition with Heinz number n.",
				"The number of nonprime parts is given by A330944, so A001222(n) = a(n) + A330944(n). - _Gus Wiseman_, Jan 17 2020"
			],
			"reference": [
				"G. E. Andrews, K. Eriksson, Integer Partitions, Cambridge Univ. Press, 2004, Cambridge."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A257994/b257994.txt\"\u003eTable of n, a(n) for n = 1..20000\u003c/a\u003e"
			],
			"example": [
				"a(30) = 2 because the partition with Heinz number 30 = 2*3*5 is [1,2,3], having 2 prime parts."
			],
			"maple": [
				"with(numtheory): a := proc (n) local B, ct, s: B := proc (n) local nn, j, m: nn := op(2, ifactors(n)): for j to nops(nn) do m[j] := op(j, nn) end do: [seq(seq(pi(op(1, m[i])), q = 1 .. op(2, m[i])), i = 1 .. nops(nn))] end proc: ct := 0: for s to nops(B(n)) do if isprime(B(n)[s]) = true then ct := ct+1 else end if end do: ct end proc: seq(a(n), n = 1 .. 130);"
			],
			"mathematica": [
				"B[n_] := Module[{nn, j, m}, nn = FactorInteger[n]; For[j = 1, j \u003c= Length[nn], j++, m[j] = nn[[j]]]; Flatten[Table[Table[PrimePi[  m[i][[1]]], {q, 1, m[i][[2]]}], {i, 1, Length[nn]}]]];",
				"a[n_] := Module[{ct, s}, ct = 0; For[s = 1, s \u003c= Length[B[n]], s++, If[ PrimeQ[B[n][[s]]], ct++]]; ct];",
				"Table[a[n], {n, 1, 130}] (* _Jean-François Alcover_, Apr 25 2017, translated from Maple *)",
				"Table[Total[Cases[FactorInteger[n],{p_,k_}/;PrimeQ[PrimePi[p]]:\u003ek]],{n,30}] (* _Gus Wiseman_, Jan 17 2020 *)"
			],
			"xref": [
				"Positions of positive terms are A331386.",
				"Primes of prime index are A006450.",
				"Products of primes of prime index are A076610.",
				"The number of nonprime prime indices is A330944.",
				"Cf. A000040, A000720, A001222, A007821, A018252, A056239, A112798, A215366, A302242, A320628, A330945."
			],
			"keyword": "nonn",
			"offset": "1,9",
			"author": "_Emeric Deutsch_, May 20 2015",
			"references": 20,
			"revision": 18,
			"time": "2020-02-14T16:33:50-05:00",
			"created": "2015-05-20T16:08:29-04:00"
		}
	]
}