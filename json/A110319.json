{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A110319",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 110319,
			"data": "1,0,1,0,1,1,0,0,3,1,0,0,1,6,1,0,0,0,6,10,1,0,0,0,1,20,15,1,0,0,0,0,10,50,21,1,0,0,0,0,1,50,105,28,1,0,0,0,0,0,15,175,196,36,1,0,0,0,0,0,1,105,490,336,45,1,0,0,0,0,0,0,21,490,1176,540,55,1,0,0,0,0,0,0,1,196",
			"name": "Triangle read by rows: T(n,k) (1 \u003c= k \u003c= n) is number of RNA secondary structures of size n (i.e., with n nodes) having k blocks (an RNA secondary structure can be viewed as a restricted noncrossing partition).",
			"comment": [
				"Row sums yield the RNA secondary structure numbers (A004148).",
				"Column sums yield the Catalan numbers (A000108).",
				"A rearrangement of the Narayana numbers triangle (A001263)."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A110319/b110319.txt\"\u003eTable of n, a(n) for n = 1..1275\u003c/a\u003e",
				"W. R. Schmitt and M. S. Waterman, \u003ca href=\"http://dx.doi.org/10.1016/0166-218X(92)00038-N\"\u003eLinear trees and RNA secondary structure\u003c/a\u003e, Discrete Appl. Math., 51, 317-323, 1994.",
				"P. R. Stein and M. S. Waterman, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(79)90033-5\"\u003eOn some new sequences generalizing the Catalan and Motzkin numbers\u003c/a\u003e, Discrete Math., 26 (1978), 261-272.",
				"M. Vauchassade de Chaumont and G. Viennot, \u003ca href=\"http://www.emis.de/journals/SLC/opapers/s08viennot.html\"\u003ePolynômes orthogonaux et problèmes d'énumeration en biologie moléculaire\u003c/a\u003e, Publ. I.R.M.A. Strasbourg, 1984, 229/S-08, Actes 8e Sem. Lotharingien, pp. 79-86."
			],
			"formula": [
				"Sum_{k=1..n} k*T(n,k) = A110320(n).",
				"T(n,k) = (1/k)*binomial(k, n-k)*binomial(k, n-k+1).",
				"G.f.: (1 - tz - tz^2 - sqrt(1 - 2tz - 2tz^2 + t^2*z^2 - 2t^2*z^3 + t^2*z^4))/(2tz^2)."
			],
			"example": [
				"Triangle begins:",
				"  1;",
				"  0, 1;",
				"  0, 1, 1;",
				"  0, 0, 3, 1;",
				"  0, 0, 1, 6,  1;",
				"  0, 0, 0, 6, 10,  1;",
				"  0, 0, 0, 1, 20, 15,   1;",
				"  0, 0, 0, 0, 10, 50,  21,   1;",
				"  0, 0, 0, 0,  1, 50, 105,  28,  1;",
				"  0, 0, 0, 0,  0, 15, 175, 196, 36, 1;",
				"  ...",
				"T(5,4)=6 because we have 13/2/4/5, 14/2/3/5. 15/2/3/4, 1/24/3/5, 1/25/3/4 and 1/2/35/4."
			],
			"maple": [
				"T:=(n,k)-\u003e(1/k)*binomial(k,n-k)*binomial(k,n-k+1): for n from 1 to 14 do seq(T(n,k),k=1..n) od; # yields sequence in triangular form"
			],
			"mathematica": [
				"T[n_, k_] := (1/k)*Binomial[k, n - k]*Binomial[k, n - k + 1];",
				"Table[T[n, k], {n, 1, 14}, {k, 1, n}] // Flatten (* _Jean-François Alcover_, Jul 06 2018, from Maple *)"
			],
			"program": [
				"(PARI) T(n,k) = (1/k)*binomial(k, n-k)*binomial(k, n-k+1); \\\\ _Andrew Howroyd_, Feb 27 2018"
			],
			"xref": [
				"Cf. A000108, A001263, A004148, A089732, A110320."
			],
			"keyword": "nonn,tabl",
			"offset": "1,9",
			"author": "_Emeric Deutsch_, Jul 19 2005",
			"references": 3,
			"revision": 17,
			"time": "2018-07-06T09:47:12-04:00",
			"created": "2005-09-21T03:00:00-04:00"
		}
	]
}