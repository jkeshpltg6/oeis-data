{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A342640",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 342640,
			"data": "0,3,2,15,0,11,6,63,0,3,10,47,8,27,14,255,0,3,2,15,0,43,22,191,0,35,10,111,24,59,30,1023,0,3,2,15,0,11,38,63,0,3,42,175,8,91,46,767,0,3,2,143,32,43,54,447,32,99,42,239,56,123,62,4095,0,3,2,15,0",
			"name": "a(n) = A342639(n, n).",
			"comment": [
				"For any n \u003e= 0:",
				"- let s(n) be the unique finite set of nonnegative integers such that n = Sum_{e in s(n)} 2^e,",
				"- then s(a(n)) corresponds to the set of nonnegative integers that are not the sum of two nonnegative integers not in s(n)."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A342640/b342640.txt\"\u003eTable of n, a(n) for n = 0..8192\u003c/a\u003e"
			],
			"formula": [
				"a(2^n-1) = 4^n-1."
			],
			"example": [
				"The first terms, alongside the corresponding sets, are:",
				"  n   a(n)  s(n)          s(a(n))",
				"  --  ----  ------------  ------------------------",
				"   0     0  {}            {}",
				"   1     3  {0}           {0, 1}",
				"   2     2  {1}           {1}",
				"   3    15  {0, 1}        {0, 1, 2, 3}",
				"   4     0  {2}           {}",
				"   5    11  {0, 2}        {0, 1, 3}",
				"   6     6  {1, 2}        {1, 2}",
				"   7    63  {0, 1, 2}     {0, 1, 2, 3, 4, 5}",
				"   8     0  {3}           {}",
				"   9     3  {0, 3}        {0, 1}",
				"  10    10  {1, 3}        {1, 3}",
				"  11    47  {0, 1, 3}     {0, 1, 2, 3, 5}",
				"  12     8  {2, 3}        {3}",
				"  13    27  {0, 2, 3}     {0, 1, 3, 4}",
				"  14    14  {1, 2, 3}     {1, 2, 3}",
				"  15   255  {0, 1, 2, 3}  {0, 1, 2, 3, 4, 5, 6, 7}"
			],
			"program": [
				"(PARI) a(n) = { my (v=0); for (x=0, 2*#binary(n), my (f=0); for (y=0, x, if (!bittest(n,y) \u0026\u0026 !bittest(n,x-y), f=1; break)); if (!f, v+=2^x)); return (v) }"
			],
			"xref": [
				"Cf. A133457, A342639, A342641, A342642."
			],
			"keyword": "nonn,base",
			"offset": "0,2",
			"author": "_Rémy Sigrist_, Mar 17 2021",
			"references": 4,
			"revision": 9,
			"time": "2021-03-20T14:37:30-04:00",
			"created": "2021-03-20T10:58:57-04:00"
		}
	]
}