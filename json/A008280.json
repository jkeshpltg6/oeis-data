{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A008280",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 8280,
			"data": "1,0,1,1,1,0,0,1,2,2,5,5,4,2,0,0,5,10,14,16,16,61,61,56,46,32,16,0,0,61,122,178,224,256,272,272,1385,1385,1324,1202,1024,800,544,272,0,0,1385,2770,4094,5296,6320,7120,7664,7936,7936",
			"name": "Boustrophedon version of triangle of Euler-Bernoulli or Entringer numbers read by rows.",
			"comment": [
				"The earliest known reference for this triangle is Seidel (1877). - _Don Knuth_, Jul 13 2007",
				"Sum of row n = A000111(n+1). - _Reinhard Zumkeller_, Nov 01 2013"
			],
			"reference": [
				"M. D. Atkinson: Partial orders and comparison problems, Sixteenth Southeastern Conference on Combinatorics, Graph Theory and Computing, (Boca Raton, Feb 1985), Congressus Numerantium 47, 77-88.",
				"J. H. Conway and R. K. Guy, The Book of Numbers, Copernicus Press, NY, 1996, p. 110.",
				"A. J. Kempner, On the shape of polynomial curves, Tohoku Math. J., 37 (1933), 347-362.",
				"A. A. Kirillov, Variations on the triangular theme, Amer. Math. Soc. Transl., (2), Vol. 169, 1995, pp. 43-73, see p. 53.",
				"R. P. Stanley, Enumerative Combinatorics, volume 1, second edition, chapter 1, exercise 141, Cambridge University Press (2012), p. 128, 174, 175."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A008280/b008280.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"V. I. Arnold, \u003ca href=\"http://dx.doi.org/10.1215/S0012-7094-91-06323-4\"\u003eBernoulli-Euler updown numbers associated with function singularities, their combinatorics and arithmetics\u003c/a\u003e, Duke Math. J. 63 (1991), 537-555.",
				"V. I. Arnold, \u003ca href=\"http://mi.mathnet.ru/eng/umn4470\"\u003eThe calculus of snakes and the combinatorics of Bernoulli, Euler and Springer numbers of Coxeter groups\u003c/a\u003e, Uspekhi Mat. nauk., 47 (#1, 1992), 3-45 = Russian Math. Surveys, Vol. 47 (1992), 1-51.",
				"M. D. Atkinson, \u003ca href=\"http://dx.doi.org/10.1016/0020-0190(85)90057-2\"\u003eZigzag permutations and comparisons of adjacent elements\u003c/a\u003e, Information Processing Letters 21 (1985), 187-189.",
				"Dominique Foata and Guo-Niu Han, \u003ca href=\"http://www-irma.u-strasbg.fr/~foata/paper/pub123Seidel.pdf\"\u003eSeidel Triangle Sequences and Bi-Entringer Numbers\u003c/a\u003e, November 20, 2013.",
				"B. Gourevitch, \u003ca href=\"http://www.pi314.net\"\u003eL'univers de Pi\u003c/a\u003e",
				"Peter Luschny, \u003ca href=\"http://oeis.org/wiki/User:Peter_Luschny/SeidelTransform\"\u003eAn old operation on sequences: the Seidel transform\u003c/a\u003e",
				"J. Millar, N. J. A. Sloane and N. E. Young, A new operation on sequences: the Boustrophedon transform, J. Combin. Theory, 17A 44-54 1996 (\u003ca href=\"http://neilsloane.com/doc/bous.txt\"\u003eAbstract\u003c/a\u003e, \u003ca href=\"http://neilsloane.com/doc/bous.pdf\"\u003epdf\u003c/a\u003e, \u003ca href=\"http://neilsloane.com/doc/bous.ps\"\u003eps\u003c/a\u003e).",
				"C. Poupard, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(82)90293-X\"\u003eDe nouvelles significations énumératives des nombres d'Entringer\u003c/a\u003e, Discrete Math., 38 (1982), 265-271.",
				"Sanjay Ramassamy, \u003ca href=\"https://arxiv.org/abs/1712.08666\"\u003eModular periodicity of the Euler numbers and a sequence by Arnold\u003c/a\u003e, arXiv:1712.08666 [math.CO], 2017.",
				"L. Seidel, \u003ca href=\"http://publikationen.badw.de/de/003384831/pdf/CC%20BY\"\u003eÜber eine einfache Entstehungsweise der Bernoulli'schen Zahlen und einiger verwandten Reihen\u003c/a\u003e, Sitzungsberichte der mathematisch-physikalischen Classe der königlich bayerischen Akademie der Wissenschaften zu München, volume 7 (1877), 157-187; see Beilage 5, pp. 183-184.",
				"R. Street, \u003ca href=\"https://arxiv.org/abs/math/0303267\"\u003eTrees, permutations and the tangent function\u003c/a\u003e, arXiv:math/0303267 [math.HO], 2003.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Boustrophedon_transform\"\u003eBoustrophedon transform\u003c/a\u003e",
				"\u003ca href=\"/index/Bo#boustrophedon\"\u003e Index entries for sequences related to boustrophedon transform\u003c/a\u003e"
			],
			"formula": [
				"T(n,m) = abs( Sum_{k=0..n} C(m,k)*Euler(n-m+k) ). - _Vladimir Kruchinin_, Apr 06 2015"
			],
			"example": [
				"This version of the triangle begins:",
				"             1",
				"           0   1",
				"         1   1   0",
				"       0   1   2   2",
				"     5   5   4   2   0",
				"   0   5  10  14  16  16",
				"See A008281 and A108040 for other versions."
			],
			"mathematica": [
				"max = 9; t[0, 0] = 1; t[n_, m_] /; n \u003c m || m \u003c 0 = 0; t[n_, m_] := t[n, m] = Sum[t[n-1, n-k], {k, m}]; tri = Table[t[n, m], {n, 0, max}, {m, 0, n}]; Flatten[ {Reverse[#[[1]]], #[[2]]} \u0026 /@ Partition[tri, 2]] (* _Jean-François Alcover_, Oct 24 2011 *)"
			],
			"program": [
				"(Sage) # Algorithm of L. Seidel (1877)",
				"# Prints the first n rows of the triangle.",
				"def A008280_triangle(n) :",
				"    A = {-1:0, 0:1}",
				"    k = 0; e = 1",
				"    for i in range(n) :",
				"        Am = 0",
				"        A[k + e] = 0",
				"        e = -e",
				"        for j in (0..i) :",
				"            Am += A[k]",
				"            A[k] = Am",
				"            k += e",
				"        print([A[z] for z in (-i//2..i//2)])",
				"A008280_triangle(10) # _Peter Luschny_, Jun 02 2012",
				"(Haskell)",
				"a008280 n k = a008280_tabl !! n !! k",
				"a008280_row n = a008280_tabl !! n",
				"a008280_tabl = ox True a008281_tabl where",
				"  ox turn (xs:xss) = (if turn then reverse xs else xs) : ox (not turn) xss",
				"-- _Reinhard Zumkeller_, Nov 01 2013",
				"(Python) # Python 3.2 or higher required.",
				"from itertools import accumulate",
				"A008280_list = blist = [1]",
				"for n in range(10):",
				"    blist = list(reversed(list(accumulate(reversed(blist))))) + [0] if n % 2 else [0]+list(accumulate(blist))",
				"    A008280_list.extend(blist)",
				"print(A008280_list) # Chai Wah Wu, Sep 20 2014",
				"(Maxima)",
				"T(n, m):=abs(sum(binomial(m, k)*euler(n-m+k), k, 0, m)); /* _Vladimir Kruchinin_, Apr 06 2015 */"
			],
			"xref": [
				"Cf. A008281, A108040, A058257.",
				"Cf. A000657 (central terms); A227862."
			],
			"keyword": "nonn,tabl,nice",
			"offset": "0,9",
			"author": "_N. J. A. Sloane_",
			"references": 14,
			"revision": 78,
			"time": "2021-02-21T02:13:28-05:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}