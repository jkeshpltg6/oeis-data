{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002068",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2068,
			"id": "M3728 N1524",
			"data": "1,1,0,5,1,0,5,2,8,18,19,7,16,13,6,34,27,56,12,69,11,73,20,70,70,72,57,1,30,95,71,119,56,67,94,86,151,108,21,106,48,72,159,35,147,118,173,180,113,131,169,107,196,214,177,73,121,170,25,277,164,231,271,259,288,110",
			"name": "Wilson remainders: a(n) = ((p-1)!+1)/p mod p, where p = prime(n).",
			"comment": [
				"If this is zero, p is a Wilson prime (see A007540).",
				"Costa, Gerbicz, \u0026 Harvey give an efficient algorithm for computing terms of this sequence. - _Charles R Greathouse IV_, Nov 09 2012"
			],
			"reference": [
				"R. Crandall and C. Pomerance, Prime Numbers: A Computational Perspective, Springer, NY, 2001; see p. 29.",
				"J. Roberts, Lure of the Integers, Math. Assoc. America, 1992, p. 244.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A002068/b002068.txt\"\u003eTable of n, a(n) for n = 1..2000\u003c/a\u003e",
				"Edgar Costa, Robert Gerbicz, and David Harvey, \u003ca href=\"http://arxiv.org/abs/1209.3436\"\u003eA search for Wilson primes\u003c/a\u003e, arXiv:1209.3436 [math.NT], 2012.",
				"C.-E. Froberg, \u003ca href=\"https://doi.org/10.1007/BF02591598\"\u003eInvestigation of the Wilson remainders in the interval 3\u003c=p\u003c=50,000\u003c/a\u003e, Arkiv f. Matematik, 4 (1961), 479-481.",
				"J. W. L. Glaisher, \u003ca href=\"http://gradelle.educanet2.ch/christian.aebi/.ws_gen/14/Glaisher_1900b.pdf\"\u003eOn the residues of the sums of products of the first p-1 numbers, and their powers, to modulus p^2 or p^3\u003c/a\u003e, Quart. J. Math. Oxford 31 (1900), 321-353.",
				"K. Goldberg, \u003ca href=\"https://doi.org/10.1112/jlms/s1-28.2.252\"\u003eA table of Wilson quotients and the third Wilson prime\u003c/a\u003e, J. London Math. Soc., 28 (1953), 252-256.",
				"J. Sondow, \u003ca href=\"https://doi.org/10.1007/978-1-4939-1601-6_17\"\u003eLerch quotients, Lerch primes, Fermat-Wilson quotients, and the Wieferich-non-Wilson primes 2, 3, 14771\u003c/a\u003e, In: Nathanson M. (eds) Combinatorial and Additive Number Theory. Springer Proceedings in Mathematics \u0026 Statistics, Vol. 101, Springer, New York, NY, 2014, pp. 243-255, \u003ca href=\"https://arxiv.org/abs/1110.3113\"\u003epreprint\u003c/a\u003e, arXiv:1110.3113 [math.NT], 2011-2012."
			],
			"formula": [
				"a(n) = A007619(n) mod A000040(n).",
				"a(n) + A197631(n) = A275741(n) for n \u003e 1. - _Jonathan Sondow_, Jul 08 2019",
				"a(n) = ( A027641(p-1)/A027642(p-1) + 1/p - 1 ) mod p, where p = prime(n), proved by Glashier (1900). - _Max Alekseyev_, Jun 20 2020"
			],
			"maple": [
				"f:= p -\u003e ((p-1)!+1 mod p^2)/p;",
				"seq(f(ithprime(i)),i=1..1000); # _Robert Israel_, Jun 15 2014"
			],
			"mathematica": [
				"Table[p=Prime[n]; Mod[((p-1)!+1)/p, p], {n,100}] (* _T. D. Noe_, Mar 21 2006 *)",
				"Mod[((#-1)!+1)/#,#]\u0026/@Prime[Range[70]] (* _Harvey P. Dale_, Feb 21 2020 *)"
			],
			"program": [
				"(PARI) forprime(n=2, 10^2, m=(((n-1)!+1)/n)%n; print1(m, \", \")) \\\\ _Felix Fröhlich_, Jun 14 2014"
			],
			"xref": [
				"Cf. A007540, A007619, A275741."
			],
			"keyword": "nonn,nice,easy",
			"offset": "1,4",
			"author": "_N. J. A. Sloane_",
			"references": 8,
			"revision": 51,
			"time": "2020-06-20T11:46:48-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}