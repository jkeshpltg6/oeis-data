{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001822",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1822,
			"data": "0,1,0,1,1,1,0,2,0,2,1,1,0,2,1,2,1,1,0,3,0,2,1,2,1,2,0,2,1,2,0,3,1,2,2,1,0,2,0,4,1,2,0,3,1,2,1,2,0,3,1,2,1,1,2,4,0,2,1,3,0,2,0,3,2,2,0,3,1,4,1,2,0,2,1,2,2,2,0,5,0,2,1,2,2,2,1,4,1,2,0,3,0,2,2,3,0,3,1,4,1,2,0,4,2",
			"name": "Expansion of Sum x^(3n+2)/(1-x^(3n+2)), n=0..inf.",
			"comment": [
				"a(n) is the number of positive divisors of n of the form 3k+2. If r(n) denotes the number of representations of n by the quadratic form j^2+i*j+i^2, then r(n)= 6 *(A001817(n)-a(n)). - _Benoit Cloitre_, Jun 24 2002",
				"a(n) = (A035191(n) - A002324(n)) / 2. - _Reinhard Zumkeller_, Nov 26 2011"
			],
			"reference": [
				"B. C. Berndt,\"On a certain theta-function in a letter of Ramanujan from Fitzroy House\", Ganita 43 (1992),33-43."
			],
			"link": [
				"Nick Hobson, \u003ca href=\"/A001822/b001822.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"P. G. Dirichlet, \u003ca href=\"http://dx.doi.org/10.1515/crll.1840.21.1\"\u003eRecherches sur diverses applications de l'analyse infinitesimale a la theorie des nombres\u003c/a\u003e, J. Reine Angew. Math. 21 (1840), 1-12.",
				"Michael Gilleland, \u003ca href=\"/selfsimilar.html\"\u003eSome Self-Similar Integer Sequences\u003c/a\u003e"
			],
			"formula": [
				"Moebius transform is period 3 sequence [0, 1, 0, ...]. - _Michael Somos_, Sep 20 2005",
				"G.f.: Sum_{k\u003e0} x^(3k-1)/(1-x^(3k-1)) = Sum_{k\u003e0} x^(2k)/(1-x^(3k)). - _Michael Somos_, Sep 20 2005",
				"a(n) + A001817(n) + A000005(n/3) = A000005(n), where A000005(.)=0 if the argument is not an integer. - _R. J. Mathar_, Sep 25 2017"
			],
			"maple": [
				"A001822 := proc(n)",
				"    local a,d ;",
				"    a := 0 ;",
				"    for d in numtheory[divisors](n) do",
				"        if modp(d,3) = 2 then",
				"            a := a+1 ;",
				"        end if ;",
				"    end do:",
				"    a ;",
				"end proc:",
				"seq(A001822(n),n=1..100) ; # _R. J. Mathar_, Sep 25 2017"
			],
			"mathematica": [
				"a[n_] := DivisorSum[n, Boole[Mod[#, 3] == 2]\u0026]; Array[a, 100] (* _Jean-François Alcover_, Dec 01 2015 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c1, 0, sumdiv(n,d, d%3==2))",
				"(Haskell)",
				"a001822 n = length [d | d \u003c- [2,5..n], mod n d == 0]",
				"-- _Reinhard Zumkeller_, Nov 26 2011"
			],
			"xref": [
				"Cf. A001817."
			],
			"keyword": "nonn",
			"offset": "1,8",
			"author": "_N. J. A. Sloane_",
			"references": 11,
			"revision": 30,
			"time": "2017-09-25T05:33:16-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}