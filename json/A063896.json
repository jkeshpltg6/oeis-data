{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A063896",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 63896,
			"data": "0,1,1,3,7,31,255,8191,2097151,17179869183,36028797018963967,618970019642690137449562111,22300745198530623141535718272648361505980415",
			"name": "a(n) = 2^Fibonacci(n) - 1.",
			"comment": [
				"The recurrence can also be written a(n)+1 = (a(n-1)+1)*(a(n-2)+1) or log_p(a(n)+1) = log_p(a(n-1)+1) + log_p(a(n-2)+1), respectively. Setting a(1)=p-1 for any natural p\u003e1, it follows that log_p(a(n)+1)=Fibonacci(n). Hence any other sequence p^Fibonacci(n)-1 could also serve as a valid solution to that recurrence, depending only on the value of the term a(1). - _Hieronymus Fischer_, Jun 27 2007",
				"Written in binary, a(n) contains Fibonacci(n) 1's. Thus the sequence converted to base-2 is A007088(a(n)) = 0, 1, 1, 11, 111, 11111, 11111111, ... . - _Hieronymus Fischer_, Jun 27 2007",
				"In general, if b(n) is defined recursively by b(0) = p, b(1) = q, b(n) = b(n-1)*b(n-2) + b(n-1) + b(n-2) for n \u003e= 2 then b(n) = p^Fibonacci(n-1) * q^Fibonacci(n) - 1. - _Rahul Goswami_, Apr 15 2020",
				"a(n) is also the numerator of the continued fraction [2^F(0), 2^F(1), 2^F(2), 2^F(3), ..., 2^F(n-2)] for n\u003e0. For the denominator, see A005203. - _Chinmay Dandekar_ and _Greg Dresden_, Sep 19 2020"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A063896/b063896.txt\"\u003eTable of n, a(n) for n = 0..18\u003c/a\u003e"
			],
			"formula": [
				"The solution to the recurrence a(0) = 0; a(1) = 1; a(n) = a(n-1)*a(n-2) + a(n-1) + a(n-2).",
				"a(n) = A000301(n) - 1. - _R. J. Mathar_, Apr 26 2007",
				"a(n) = a(n-2)*2^ceiling(log_2(a(n-1))) + a(n-1) for n\u003e1. - _Hieronymus Fischer_, Jun 27 2007",
				"a(n) = A000225(A000045(n)). - _Alois P. Heinz_, Mar 19 2020"
			],
			"maple": [
				"a:= n-\u003e 2^(\u003c\u003c0|1\u003e, \u003c1|1\u003e\u003e^n)[1,2]-1:",
				"seq(a(n), n=0..15);  # _Alois P. Heinz_, Aug 12 2017"
			],
			"mathematica": [
				"2^Fibonacci[Range[0,15]]-1 (* _Harvey P. Dale_, May 20 2014 *)",
				"RecurrenceTable[{a[0] == 0, a[1] == 1, a[n] == (a[n - 1] + 1)*(a[n - 2] + 1) - 1}, a[n], {n, 0, 12}] (* _Ray Chandler_, Jul 30 2015 *)"
			],
			"program": [
				"(PARI) a(n) = 2^fibonacci(n) - 1 \\\\ _Charles R Greathouse IV_, Oct 03 2016"
			],
			"xref": [
				"Cf. A000045 (Fibonacci), A000225, A000301, A005203, A061107.",
				"See A131293 for a base-10 analog with Fib(n) 1's."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_Robert G. Wilson v_, Aug 29 2001",
			"references": 18,
			"revision": 55,
			"time": "2021-05-14T18:50:36-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}