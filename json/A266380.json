{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A266380",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 266380,
			"data": "1,3,0,127,0,2047,0,32767,0,524287,0,8388607,0,134217727,0,2147483647,0,34359738367,0,549755813887,0,8796093022207,0,140737488355327,0,2251799813685247,0,36028797018963967,0,576460752303423487,0,9223372036854775807,0",
			"name": "Decimal representation of the n-th iteration of the \"Rule 21\" elementary cellular automaton starting with a single ON (black) cell.",
			"reference": [
				"S. Wolfram, A New Kind of Science, Wolfram Media, 2002; p. 55."
			],
			"link": [
				"Robert Price, \u003ca href=\"/A266380/b266380.txt\"\u003eTable of n, a(n) for n = 0..500\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ElementaryCellularAutomaton.html\"\u003eElementary Cellular Automaton\u003c/a\u003e",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_Elementary_Cellular_Automata\"\u003eIndex to Elementary Cellular Automata\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,17,0,-16)."
			],
			"formula": [
				"From _Colin Barker_, Dec 29 2015 and Apr 15 2019: (Start)",
				"a(n) = 17*a(n-2) - 16*a(n-4) for n\u003e5.",
				"G.f.: (1 + 3*x - 17*x^2 + 76*x^3 + 16*x^4 - 64*x^5) / ((1-x)*(1+x)*(1-4*x)*(1+4*x)). (End)",
				"a(n) = (1 - (-1)^n)*(4*16^floor(n/2) - 1/2) for n\u003e1. - _Bruno Berselli_, Dec 29 2015",
				"a(n) = (2*4^n - 1)*(n mod 2) + 0^n - 4*0^abs(n-1). - _Karl V. Keller, Jr._, Sep 03 2021"
			],
			"mathematica": [
				"rule=21; rows=20; ca=CellularAutomaton[rule,{{1},0},rows-1,{All,All}]; (* Start with single black cell *) catri=Table[Take[ca[[k]],{rows-k+1,rows+k-1}],{k,1,rows}]; (* Truncated list of each row *) Table[FromDigits[catri[[k]],2],{k,1,rows}] (* Decimal Representation of Rows *)"
			],
			"program": [
				"(MAGMA) [n le 1 select 3^n else (1-(-1)^n)*(4*16^Floor(n/2)-1/2): n in [0..40]]; // _Bruno Berselli_, Dec 29 2015",
				"(Python) print([(2*4**n - 1)*(n%2) + 0**n - 4*0**abs(n-1) for n in range(50)]) # _Karl V. Keller, Jr._, Sep 03 2021"
			],
			"xref": [
				"Cf. A266377, A266379.",
				"Cf. A241955: a(2*n+1) for n\u003e0."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Robert Price_, Dec 28 2015",
			"references": 4,
			"revision": 28,
			"time": "2021-09-03T21:12:12-04:00",
			"created": "2015-12-28T16:51:11-05:00"
		}
	]
}