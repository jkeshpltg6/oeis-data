{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A118194",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 118194,
			"data": "0,1,-3,53,-4871,2262505,-5269940619,61424345593757,-3580474937256484367,1043606492389898678125009,-1520932783784930699920673828115,11082945991224258678496051788222656261,-403804307486446123171767495567989349951171863",
			"name": "Column 0 of the matrix log of triangle A118190, after term in row n is multiplied by n: a(n) = n*[log(A118190)](n,0), where A118190(n,k) = 5^(k*(n-k)).",
			"comment": [
				"The entire matrix log of triangle A118190 is determined by column 0 (this sequence): [log(A118190)](n,k) = a(n-k)5^(k*(n-k))/(n-k) for n\u003ek\u003e=0."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A118194/b118194.txt\"\u003eTable of n, a(n) for n = 0..50\u003c/a\u003e"
			],
			"formula": [
				"G.f.: x/(1-x)^2 = Sum_{n\u003e=0} a(n)*x^n/(1-5^n*x). By using the inverse transformation: a(n) = Sum_{k=0..n} k*A118193(n-k)*5^(k*(n-k)) for n\u003e=0."
			],
			"example": [
				"Column 0 of log(A118190) = [0, 1, -3/2, 53/3, -4871/4, ...].",
				"The g.f. is illustrated by:",
				"x/(1-x)^2 = x + 2*x^2 + 3*x^3 + 4*x^4 + 5*x^5 + 6*x^6 + 7*x^7 + ...",
				"  = x/(1-5*x) -3*x^2/(1-25*x) +53*x^3/(1-125*x) -4871*x^4/(1-625*x) + 2262505*x^5/(1-3125*x) - 5269940619*x^6/(1-15625*x) + ..."
			],
			"mathematica": [
				"A118193[n_]:= A118193[n]= If[n\u003c2, (-1)^n, -Sum[5^(j*(n-j))*A118193[j], {j, 0, n-1}]];",
				"a[n_]:= a[n]= -Sum[5^(j*(n-j))*j*A118193[j], {j, 0, n}];",
				"Table[a[n], {n, 0, 30}] (* _G. C. Greubel_, Jun 29 2021 *)"
			],
			"program": [
				"(PARI) {a(n)=local(T=matrix(n+1,n+1,r,c,if(r\u003e=c,(5^(c-1))^(r-c))), L=sum(m=1,#T,-(T^0-T)^m/m));return(n*L[n+1,1])}",
				"(Sage)",
				"@CachedFunction",
				"def A118193(n): return (-1)^n if (n\u003c2) else -sum(5^(j*(n-j))*A118193(j) for j in (0..n-1))",
				"def a(n): return (-1)*sum(5^(j*(n-j))*j*A118193(j) for j in (0..n))",
				"[a(n) for n in (0..30)] # _G. C. Greubel_, Jun 29 2021"
			],
			"xref": [
				"Cf. A118190."
			],
			"keyword": "sign",
			"offset": "0,3",
			"author": "_Paul D. Hanna_, Apr 15 2006",
			"references": 2,
			"revision": 9,
			"time": "2021-06-30T01:49:36-04:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}