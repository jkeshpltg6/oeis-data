{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A018893",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 18893,
			"data": "1,1,11,375,27897,3817137,865874115,303083960103,155172279680289,111431990979621729,108511603921116483579,139360142400556127213655,230624017175131841824732233,482197541715276031774659298833",
			"name": "Blasius sequence: from coefficients in expansion of solution to Blasius's equation in boundary layer theory.",
			"comment": [
				"Number of increasing trilabeled unordered trees. - _Markus Kuba_, Nov 18 2014"
			],
			"reference": [
				"H. T. Davis: Introduction to Nonlinear Differential and Integral Equations (Dover 1962), page 403."
			],
			"link": [
				"Vaclav Kotesovec, \u003ca href=\"/A018893/b018893.txt\"\u003eTable of n, a(n) for n = 0..180\u003c/a\u003e",
				"Heinrich Blasius, \u003ca href=\"https://play.google.com/store/books/details?id=1lFIAQAAMAAJ\u0026amp;rdid=book-1lFIAQAAMAAJ\u0026amp;rdot=1\"\u003eGrenzschichten in Flüssigkeiten mit kleiner Reibung\u003c/a\u003e, Inaugural Dissertation, Georg-August-Universität Göttingen, Leipzig, 1907; see p. 8 (a(6) = c_6 and a(7) = c_7 are wrong in the dissertation) [USA access only].",
				"Heinrich Blasius, \u003ca href=\"https://iris.univ-lille.fr/handle/1908/2024\"\u003eGrenzschichten in Flüssigkeiten mit kleiner Reibung\u003c/a\u003e, Z. Math. u. Physik 56 (1908), 1-37; see p. 8 (a(6) = c_6 has been corrected, while a(7) = c_7 was re-calculated incorrectly!).",
				"Heinrich Blasius, \u003ca href=\"http://naca.central.cranfield.ac.uk/reports/1950/naca-tm-1256.pdf\"\u003eGrenzschichten in Flüssigkeiten mit kleiner Reibung\u003c/a\u003e, Z. Math. u. Physik 56 (1908), 1-37 [English translation by J. Vanier on behalf of the National Advisory Committee for Aeronautics (NACA), 1950]; see p. 8 (a(6) = c_6 has been corrected, while a(7) = c_7 was re-calculated incorrectly!).",
				"Steven R. Finch, \u003ca href=\"/A018893/a018893.pdf\"\u003ePrandtl-Blasius Flow\u003c/a\u003e. [Cached copy, with permission of the author]",
				"W. H. Hager, \u003ca href=\"https://doi.org/10.1007/s00348-002-0582-9\"\u003eBlasius: A life in research and education\u003c/a\u003e, Exp. Fluids 34(5) (2003), 566-571.",
				"Markus Kuba and Alois Panholzer, \u003ca href=\"http://arxiv.org/abs/1411.4587\"\u003eCombinatorial families of multilabelled increasing trees and hook-length formulas\u003c/a\u003e, arXiv:1411.4587 [math.CO], 2014.",
				"Markus Kuba and Alois Panholzer, \u003ca href=\"https://doi.org/10.1016/j.disc.2015.08.010\"\u003eCombinatorial families of multilabelled increasing trees and hook-length formulas\u003c/a\u003e, Discrete Mathematics 339(1) (2016), 227-254.",
				"Ronald Orozco López, \u003ca href=\"https://www.researchgate.net/publication/350397609_Solution_of_the_Differential_Equation_ykeay_Special_Values_of_Bell_Polynomials_and_ka-Autonomous_Coefficients\"\u003eSolution of the Differential Equation y^(k)= e^(a*y), Special Values of Bell Polynomials and (k,a)-Autonomous Coefficients\u003c/a\u003e, Universidad de los Andes (Colombia 2021).",
				"Hans Salié, \u003ca href=\"https://doi.org/10.1002/mana.19550140405\"\u003e Über die Koeffizienten der Blasiusschen Reihen\u003c/a\u003e, Math. Nachr. 14 (1955), 241-248 (1956). [He generalizes the Blasius numbers.]",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Paul_Richard_Heinrich_Blasius\"\u003ePaul Richard Heinrich Blasius\u003c/a\u003e."
			],
			"formula": [
				"E.g.f. A(x) satisfies (d^3/dx^3)log(A(x)) = A(x). - _Vladeta Jovovic_, Oct 24 2003",
				"Lim_{n-\u003einfinity} (a(n)/(3*n+2)!)^(1/n) = 0.03269425181024... . - _Vaclav Kotesovec_, Oct 28 2014",
				"T(z) = log(A(z)) satisfies T'''(z)=exp(T(z)), such that F(z)=T'(z) satisfies a Blasius type equation: F'''(z)-F(z)*F''(z)=0. - _Markus Kuba_, Nov 18 2014",
				"a(n) = Sum_{v = 0..n-1} binomial(3*n-1, 3*v) * a(v) * a(n-1-v) for n \u003e= 1 with a(0) = 1 (Blasius' recurrence). - _Petros Hadjicostas_, Aug 01 2019"
			],
			"example": [
				"A(x) = 1 + 1/6*x^3 + 11/720*x^6 + 25/24192*x^9 + 9299/159667200*x^12 + ...",
				"G.f. = 1 + x + 11*x^3 + 375*x^4 + 27897*x^5 + 3817137*x^6 + ..."
			],
			"mathematica": [
				"a[0] = 1; a[k_] := a[k] = Sum[Binomial[3*k-1, 3*j]*a[j]*a[k-j-1], {j, 0, k-1}]; Table[a[k], {k, 0, 13}] (* _Jean-François Alcover_, Oct 28 2014 *)"
			],
			"xref": [
				"Cf. A002105, A256522."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "Stan Richardson (stan(AT)maths.ed.ac.uk)",
			"ext": [
				"Corrected and extended by _Vladeta Jovovic_, Oct 24 2003"
			],
			"references": 4,
			"revision": 86,
			"time": "2021-07-16T08:25:38-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}