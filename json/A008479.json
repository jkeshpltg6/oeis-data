{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A008479",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 8479,
			"data": "1,1,1,2,1,1,1,3,2,1,1,2,1,1,1,4,1,3,1,2,1,1,1,4,2,1,3,2,1,1,1,5,1,1,1,5,1,1,1,3,1,1,1,2,2,1,1,6,2,4,1,2,1,7,1,3,1,1,1,2,1,1,2,6,1,1,1,2,1,1,1,8,1,1,3,2,1,1,1,5,4,1,1,2,1,1,1,3,1,3,1,2,1,1,1,9",
			"name": "Number of numbers \u003c= n with same prime factors as n.",
			"comment": [
				"For n \u003e 1, a(n) gives the (one-based) index of the row where n is located in arrays A284311 and A285321 or respectively, index of the column where n is in A284457. A285329 gives the other index. - _Antti Karttunen_, Apr 17 2017"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A008479/b008479.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"P. Erdős, T. Motzkin, \u003ca href=\"http://www.jstor.org/stable/2316593\"\u003eProblem 5735\u003c/a\u003e, Amer. Math. Monthly, 78 (1971), 680-681. (Incorrect solution!)",
				"H. N. Shapiro, \u003ca href=\"http://www.jstor.org/stable/2324350\"\u003eProblem 5735\u003c/a\u003e, Amer. Math. Monthly, 97 (1990), 937."
			],
			"formula": [
				"a(n) = Sum_{k=1..n} (floor(n^k/k)-floor((n^k-1)/k))*(floor(k^n/n)-floor((k^n-1)/n)). - _Anthony Browne_, May 20 2016",
				"If A008683(n) \u003c\u003e 0 [when n is squarefree, A005117], a(n) = 1, otherwise a(n) = 1+a(A285328(n)). - _Antti Karttunen_, Apr 17 2017"
			],
			"maple": [
				"N:= 100: # to get a(1)..a(N)",
				"V:= Vector(N):",
				"V[1]:= 1:",
				"for n from 2 to N do",
				"  if V[n] = 0 then",
				"   S:= {n};",
				"   for p in numtheory:-factorset(n) do",
				"     S := S union {seq(seq(s*p^k,k=1..floor(log[p](N/s))),s=S)};",
				"   od:",
				"   S:= sort(convert(S,list));",
				"   for k from 1 to nops(S) do V[S[k]]:= k od:",
				"fi",
				"od:",
				"convert(V,list); # _Robert Israel_, May 20 2016"
			],
			"mathematica": [
				"PkTbl=Prepend[ Array[ Times @@ First[ Transpose[ FactorInteger[ # ] ] ]\u0026, 100, 2 ], 1 ];1+Array[ Count[ Take[ PkTbl, #-1 ], PkTbl[ [ # ] ] ]\u0026, Length[ PkTbl ] ]",
				"Count[#, k_ /; k == Last@ #] \u0026 /@ Function[s, Take[s, #] \u0026 /@ Range@ Length@ s]@ Array[Map[First, FactorInteger@ #] \u0026, 120] (* or *)",
				"Table[Sum[(Floor[n^k/k] - Floor[(n^k - 1)/k]) (Floor[k^n/n] - Floor[(k^n - 1)/n]), {k, n}], {n, 120}] (* _Michael De Vlieger_, May 20 2016 *)"
			],
			"program": [
				"(Scheme) (define (A008479 n) (if (not (zero? (A008683 n))) 1 (+ 1 (A008479 (A285328 n))))) ;; _Antti Karttunen_, Apr 17 2017",
				"(PARI) a(n)=my(f=factor(n)[,1], s); forvec(v=vector(#f, i, [1, logint(n, f[i])]), if(prod(i=1, #f, f[i]^v[i])\u003c=n, s++)); s \\\\ _Charles R Greathouse IV_, Oct 19 2017"
			],
			"xref": [
				"Cf. A005117 (positions of ones), A005361, A007947, A008683, A284311, A284457, A285321, A285328, A285329."
			],
			"keyword": "nonn,easy",
			"offset": "1,4",
			"author": "_Jeffrey Shallit_, _Olivier Gérard_",
			"references": 17,
			"revision": 30,
			"time": "2017-10-19T10:53:38-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}