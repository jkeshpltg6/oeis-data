{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A143946",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 143946,
			"data": "1,1,0,1,2,0,2,1,0,1,6,0,6,3,2,3,2,1,0,1,24,0,24,12,8,18,8,10,3,6,3,2,1,0,1,120,0,120,60,40,90,64,50,39,42,23,28,13,10,8,6,3,2,1,0,1,720,0,720,360,240,540,384,420,234,372,198,208,168,124,98,75,60,35,34,13,16,8,6,3",
			"name": "Triangle read by rows: T(n,k) is the number of permutations of [n] for which the sum of the positions of the left-to-right maxima is k (1 \u003c= k \u003c= n(n+1)/2).",
			"comment": [
				"Row n contains n(n+1)/2 entries.",
				"Sum of entries in row n = n! = A000142(n)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A143946/b143946.txt\"\u003eRows n = 1..50, flattened\u003c/a\u003e",
				"I. Kortchemski, \u003ca href=\"http://arxiv.org/abs/0804.0446\"\u003eAsymptotic behavior of permutation records\u003c/a\u003e, arXiv: 0804.0446 [math.CO], 2008-2009."
			],
			"formula": [
				"T(n,1) = T(n,3) = (n-1)! for n\u003e=2.",
				"Sum_{k=1..n(n+1)/2} k * T(n,k) = n! * n = A001563(n).",
				"Generating polynomial of row n is t(t^2+1)(t^3+2)...(t^n+n-1)."
			],
			"example": [
				"T(4,6)=3 because we have 1243, 1342 and 2341 with left-to-right maxima at positions 1,2,3.",
				"Triangle starts:",
				"   1;",
				"   1,  0,  1;",
				"   2,  0,  2,  1,  0,  1;",
				"   6,  0,  6,  3,  2,  3,  2,  1,  0,  1;",
				"  24,  0, 24, 12,  8, 18,  8, 10,  3,  6,  3,  2,  1,  0,  1;"
			],
			"maple": [
				"P:=proc(n) options operator, arrow: sort(expand(product(t^j+j-1,j=1..n))) end proc: for n to 7 do seq(coeff(P(n),t,i),i=1..(1/2)*n*(n+1)) end do; # yields sequence in triangular form",
				"# second Maple program:",
				"b:= proc(n) option remember; `if`(n=0, 1,",
				"      expand(b(n-1)*(x^n+n-1)))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=1..degree(p)))(b(n)):",
				"seq(T(n), n=1..7);  # _Alois P. Heinz_, Aug 05 2020"
			],
			"mathematica": [
				"row[n_] := CoefficientList[Product[t^k + k - 1, {k, 1, n}], t] // Rest;",
				"Array[row, 7] // Flatten (* _Jean-François Alcover_, Nov 28 2017 *)"
			],
			"xref": [
				"Cf. A000142, A001563, A143947."
			],
			"keyword": "nonn,tabf",
			"offset": "1,5",
			"author": "_Emeric Deutsch_, Sep 21 2008",
			"references": 3,
			"revision": 26,
			"time": "2020-08-05T19:18:47-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}