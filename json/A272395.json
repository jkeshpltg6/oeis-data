{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A272395",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 272395,
			"data": "1,0,1,1,11,76,671,5916,54131,504316,4779291,45898975,445798221,4371237794,43213522209,430241859971,4310236148075,43417944574136,439495074016427,4468208369691396,45605656313488271,467140985042718910",
			"name": "Degeneracies of entanglement witness eigenstates for n spin 5 irreducible representations.",
			"comment": [
				"The Mathematica formula for a(n) as the difference of two generalized binomial coefficients is adapted from the Appendix of the Mendonça link. - _Thomas Curtright_, Jul 27 2016"
			],
			"link": [
				"Gheorghe Coserea, \u003ca href=\"/A272395/b272395.txt\"\u003eTable of n, a(n) for n = 0..401\u003c/a\u003e",
				"Eliahu Cohen, Tobias Hansen, Nissan Itzhaki, \u003ca href=\"http://arxiv.org/abs/1511.06623\"\u003eFrom Entanglement Witness to Generalized Catalan Numbers\u003c/a\u003e, arXiv:1511.06623 [quant-ph], 2015.",
				"T. L. Curtright, T. S. Van Kortryk, and C. K. Zachos, \u003ca href=\"https://hal.archives-ouvertes.fr/hal-01345527\"\u003eSpin Multiplicities\u003c/a\u003e, hal-01345527, 2016.",
				"Vaclav Kotesovec, \u003ca href=\"/A272395/a272395.txt\"\u003eRecurrence (of order 6)\u003c/a\u003e",
				"J. R. G. Mendonça, \u003ca href=\"http://arxiv.org/abs/1207.4106\"\u003eExact eigenspectrum of the symmetric simple exclusion process on the complete, complete bipartite and related graphs\u003c/a\u003e, Journal of Physics A: Mathematical and Theoretical 46:29 (2013) 295001. arXiv:1207.4106 [cond-mat.stat-mech], 2012-2013."
			],
			"formula": [
				"a(n)=(1/Pi)*int((sin(11x)/sin(x))^n*(sin(x))^2,x,0,2Pi). - _Thomas Curtright_, Jun 24 2016",
				"a(n) ~ (1/20)^(3/2)*11^n/(sqrt(Pi)*n^(3/2))(1-63/(80n)+O(1/n^2)). - _Thomas Curtright_, Jul 26 2016"
			],
			"mathematica": [
				"a[n_]:= 2/Pi*Integrate[Sqrt[(1-t)/t]*(1024t^5-2304t^4+1792t^3-560t^2 +60t-1)^n, {t, 0, 1}] (* _Thomas Curtright_, Jun 24 2016 *)",
				"a[n_]:= c[0, n, 5]-c[1, n, 5]",
				"c[j_, n_, s_]:= Sum[(-1)^k*Binomial[n, k]*Binomial[j - (2*s + 1)*k + n + n*s - 1, j - (2*s + 1)*k + n*s],{k, 0, Floor[(j + n*s)/(2*s + 1)]}] (* _Thomas Curtright_, Jul 26 2016 *)",
				"a[n_]:= mult[0, n, 5]",
				"mult[j_,n_,s_]:=Sum[(-1)^(k+1)*Binomial[n,k]*Binomial[n*s+j-(2*s+1)*k+n- 1,n*s+j-(2*s+1)*k+1],{k,0,Floor[(n*s+j+1)/(2*s+1)]}] (* _Thomas Curtright_, Jun 14 2017 *)"
			],
			"program": [
				"(PARI)",
				"N = 22; S = 5;",
				"M = matrix(N+1, N*numerator(S)+1);",
				"Mget(n, j) =  { M[1 + n, 1 + j*denominator(S)] };",
				"Mset(n, j, v) = { M[1 + n, 1 + j*denominator(S)] = v };",
				"Minit() = {",
				"  my(step = 1/denominator(S));",
				"  Mset(0, 0, 1);",
				"  for (n = 1, N, forstep (j = 0, n*S, step,",
				"     my(acc = 0);",
				"     for (k = abs(j-S), min(j+S, (n-1)*S), acc += Mget(n-1, k));",
				"     Mset(n, j, acc)));",
				"};",
				"Minit();",
				"vector(1 + N\\denominator(S), n, Mget((n-1)*denominator(S),0))",
				"(PARI) c(j, n) = sum(k=0, min((j + 5*n)\\11, n), (-1)^k*binomial(n, k)*binomial(j - 11*k + n + 5*n - 1, j - 11*k + n*5))",
				"a(n)=c(0, n)-c(1, n) \\\\ _Charles R Greathouse IV_, Jul 28 2016; adapted from Curtright's Mathematica code"
			],
			"xref": [
				"For spin S = 1/2, 1, 3/2, 2, 5/2, 3, 7/2, 4, 9/2, 5 we get A000108, A005043, A264607, A007043, A272391, A264608, A272392, A272393, A272394, this sequence."
			],
			"keyword": "nonn",
			"offset": "0,5",
			"author": "_Gheorghe Coserea_, Apr 28 2016",
			"references": 7,
			"revision": 70,
			"time": "2017-06-17T14:27:57-04:00",
			"created": "2016-04-28T21:50:51-04:00"
		}
	]
}