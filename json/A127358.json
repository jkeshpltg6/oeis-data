{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A127358",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 127358,
			"data": "1,3,8,21,54,138,350,885,2230,5610,14088,35346,88596,221952,555738,1391061,3480870,8708610,21783680,54483510,136254964,340729788,852000828,2130354786,5326563004",
			"name": "a(n) = Sum_{k=0..n} binomial(n, floor(k/2))*2^(n-k).",
			"comment": [
				"Hankel transform is (-1)^n. In general, given r \u003e= 0, the sequence given by Sum_{k=0..n} binomial(n, floor(k/2))*r^(n-k)} has Hankel transform (1-r)^n. The sequence is the image of the sequence with g.f. (1+x)/(1-2x) under the Chebyshev mapping g(x) -\u003e (1/sqrt(1-4x^2))g(xc(x^2)), where c(x) is the g.f. of the Catalan numbers A000108."
			],
			"link": [
				"Harvey P. Dale, \u003ca href=\"/A127358/b127358.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Isaac DeJager, Madeleine Naquin, Frank Seidl, \u003ca href=\"https://www.valpo.edu/mathematics-statistics/files/2019/08/Drube2019.pdf\"\u003eColored Motzkin Paths of Higher Order\u003c/a\u003e, VERUM 2019."
			],
			"formula": [
				"G.f.: (1/sqrt(1 - 4x^2))(1 + x*c(x^2))/(1 - 2*x*c(x^2)).",
				"a(n) = 2*a(n-1) + A054341(n-1). a(n) = Sum_{k=0..n} A126075(n,k). - _Philippe Deléham_, Mar 03 2007",
				"a(n) = Sum_{k=0..n} A061554(n,k)*2^k. - _Philippe Deléham_, Dec 04 2009",
				"a(n) is the sum of top row terms of M^n, M = an infinite square production matrix as follows:",
				"2, 1, 0, 0, 0,...",
				"1, 0, 1, 0, 0,...",
				"0, 1, 0, 1, 0,...",
				"0, 0, 1, 0, 1,...",
				"0, 0, 0, 1, 0,...",
				"... - _Gary W. Adamson_, Sep 07 2011",
				"Conjecture: 2*n*a(n) + (-5*n-4)*a(n-1) + 2*(-4*n+13)*a(n-2) + 20*(n-2)*a(n-3) = 0. - _R. J. Mathar_, Nov 30 2012",
				"a(n) ~ 3 * 5^n / 2^(n+1). - _Vaclav Kotesovec_, Feb 13 2014"
			],
			"example": [
				"a(3) = 21 = (12 + 6 + 2 + 1), where the top row of M^3 = (12, 6, 2, 1)."
			],
			"mathematica": [
				"Table[Sum[Binomial[n,Floor[k/2]]2^(n-k),{k,0,n}],{n,0,30}] (* _Harvey P. Dale_, Jun 03 2012 *)",
				"CoefficientList[Series[(1 + 2*x - Sqrt[1 - 4*x^2])/(2*Sqrt[1 - 4*x^2]*(x - 1 + Sqrt[1 - 4*x^2])), {x, 0, 50}], x] (* _G. C. Greubel_, May 22 2017 *)"
			],
			"program": [
				"(PARI) x='x+O('x^50); Vec((1 + 2*x - sqrt(1 - 4*x^2))/(2*sqrt(1 - 4*x^2)*(x - 1 + sqrt(1 - 4*x^2)))) \\\\ _G. C. Greubel_, May 22 2017"
			],
			"xref": [
				"Cf. A107430. - _Philippe Deléham_, Sep 16 2009"
			],
			"keyword": "easy,nonn",
			"offset": "0,2",
			"author": "_Paul Barry_, Jan 11 2007",
			"references": 8,
			"revision": 31,
			"time": "2019-12-15T22:02:25-05:00",
			"created": "2007-01-12T03:00:00-05:00"
		}
	]
}