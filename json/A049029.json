{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A049029",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 49029,
			"data": "1,5,1,45,15,1,585,255,30,1,9945,5175,825,50,1,208845,123795,24150,2025,75,1,5221125,3427515,775845,80850,4200,105,1,151412625,108046575,27478710,3363045,219450,7770,140,1,4996616625,3824996175,1069801425",
			"name": "Triangle read by rows, the Bell transform of the quartic factorial numbers A007696(n+1) without column 0.",
			"comment": [
				"Previous name was: Triangle of numbers related to triangle A048882; generalization of Stirling numbers of second kind A008277, Lah-numbers A008297, ...",
				"a(n,m) enumerates unordered n-vertex m-forests composed of m plane increasing quintic (5-ary) trees. Proof based on the a(n,m) recurrence. See also the F. Bergeron et al. reference, especially Table 1, first row and Example 1 for the e.g.f. for m=1. - _Wolfdieter Lang_, Sep 14 2007",
				"Also the Bell transform of A007696(n+1). For the definition of the Bell transform see A264428. - _Peter Luschny_, Jan 28 2016"
			],
			"link": [
				"F. Bergeron, Ph. Flajolet and B. Salvy, \u003ca href=\"http://dx.doi.org/10.1007/3-540-55251-0_2\"\u003eVarieties of Increasing Trees\u003c/a\u003e, in Lecture Notes in Computer Science vol. 581, ed. J.-C. Raoult, Springer 1992, pp. 24-48.",
				"P. Blasiak, K. A. Penson and A. I. Solomon, \u003ca href=\"http://www.arXiv.org/abs/quant-ph/0402027\"\u003eThe general boson normal ordering problem\u003c/a\u003e, arXiv:quant-ph/0402027, 2004.",
				"T. Copeland, \u003ca href=\"https://tcjpn.wordpress.com/2008/06/12/mathemagical-forests/\"\u003eMathemagical Forests\u003c/a\u003e",
				"T. Copeland, \u003ca href=\"https://tcjpn.wordpress.com/2010/12/28/14/\"\u003eAddendum to Mathemagical Forests\u003c/a\u003e",
				"T. Copeland, \u003ca href=\"http://tcjpn.wordpress.com/2015/08/23/a-class-of-differential-operators-and-the-stirling-numbers/\"\u003eA Class of Differential Operators and the Stirling Numbers\u003c/a\u003e",
				"M. Janjic, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL12/Janjic/janjic22.html\"\u003eSome classes of numbers and derivatives\u003c/a\u003e, JIS 12 (2009) #09.8.3.",
				"W. Lang, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL3/LANG/lang.html\"\u003eOn generalizations of Stirling number triangles\u003c/a\u003e, J. Integer Seqs., Vol. 3 (2000), #00.2.4.",
				"W. Lang, \u003ca href=\"/A049029/a049029.txt\"\u003eFirst 10 rows\u003c/a\u003e.",
				"Shi-Mei Ma, \u003ca href=\"http://arxiv.org/abs/1208.3104\"\u003eSome combinatorial sequences associated with context-free grammars\u003c/a\u003e, arXiv:1208.3104v2 [math.CO]. - From N. J. A. Sloane, Aug 21 2012",
				"E. Neuwirth, \u003ca href=\"http://dx.doi.org/10.1016/S0012-365X(00)00373-3\"\u003eRecursively defined combinatorial Functions: Extending Galton's board\u003c/a\u003e, Discr. Maths. 239 (2001) 33-51.",
				"Mathias Pétréolle, Alan D. Sokal, \u003ca href=\"https://arxiv.org/abs/1907.02645\"\u003eLattice paths and branched continued fractions. II. Multivariate Lah polynomials and Lah symmetric functions\u003c/a\u003e, arXiv:1907.02645 [math.CO], 2019."
			],
			"formula": [
				"a(n, m) = n!*A048882(n, m)/(m!*4^(n-m)); a(n+1, m) = (4*n+m)*a(n, m)+ a(n, m-1), n \u003e= m \u003e= 1; a(n, m) := 0, n\u003cm; a(n, 0) := 0, a(1, 1)=1; E.g.f. of m-th column: ((-1+(1-4*x)^(-1/4))^m)/m!.",
				"a(n, m) = sum(|A051142(n, j)|*S2(j, m), j=m..n) (matrix product), with S2(j, m) := A008277(j, m) (Stirling2 triangle). Priv. comm. to W. Lang by E. Neuwirth, Feb 15 2001; see also the 2001 Neuwirth reference. See the general comment on products of Jabotinsky matrices given under A035342.",
				"From _Peter Bala_, Nov 25 2011: (Start)",
				"E.g.f.: G(x,t) = exp(t*A(x)) = 1+t*x+(5*t+t^2)*x^2/2!+(45*t+15*t^2+t^3)*x^3/3!+..., where A(x) = -1+(1-4*x)^(-1/4) satisfies the autonomous differential equation A'(x) = (1+A(x))^5.",
				"The generating function G(x,t) satisfies the partial differential equation t*(dG/dt+G) = (1-4*x)*dG/dx, from which follows the recurrence given above.",
				"The row polynomials are given by D^n(exp(x*t)) evaluated at x = 0, where D is the operator (1+x)^5*d/dx. Cf. A008277 (D = (1+x)*d/dx), A105278 (D = (1+x)^2*d/dx), A035342 (D = (1+x)^3*d/dx) and A035469 (D = (1+x)^4*d/dx).",
				"(End)"
			],
			"example": [
				"Triangle starts:",
				"{1};",
				"{5,1};",
				"{45,15,1};",
				"{585,255,30,1};",
				"{9945,5175,825,50,1};",
				"..."
			],
			"maple": [
				"# The function BellMatrix is defined in A264428.",
				"# Adds (1,0,0,0, ..) as column 0.",
				"BellMatrix(n -\u003e mul(4*k+1, k=0..n), 9); # _Peter Luschny_, Jan 28 2016"
			],
			"mathematica": [
				"a[n_, m_] /; n \u003e= m \u003e= 1 := a[n, m] = (4(n-1) + m)*a[n-1, m] + a[n-1, m-1]; a[n_, m_] /; n \u003c m = 0; a[_, 0] = 0; a[1, 1] = 1; Flatten[Table[a[n, m], {n, 1, 9}, {m, 1, n}]] (* _Jean-François Alcover_, Jul 22 2011 *)",
				"rows = 9;",
				"a[n_, m_] := BellY[n, m, Table[Product[4k+1, {k, 0, j}], {j, 0, rows}]];",
				"Table[a[n, m], {n, 1, rows}, {m, 1, n}] // Flatten (* _Jean-François Alcover_, Jun 22 2018 *)"
			],
			"xref": [
				"a(n, m) := S2(5, n, m) is the fifth triangle of numbers in the sequence S2(1, n, m) := A008277(n, m) (Stirling 2nd kind), S2(2, n, m) := A008297(n, m) (Lah), S2(3, n, m) := A035342(n, m), S2(4, n, m) := A035469(n, m). a(n, 1)= A007696(n). A007559(n).",
				"Cf. A048882, A007696. Row sums: A049120(n), n \u003e= 1.",
				"Cf. A094638"
			],
			"keyword": "easy,nonn,tabl",
			"offset": "1,2",
			"author": "_Wolfdieter Lang_",
			"ext": [
				"New name from _Peter Luschny_, Jan 30 2016"
			],
			"references": 42,
			"revision": 73,
			"time": "2019-09-04T17:16:52-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}