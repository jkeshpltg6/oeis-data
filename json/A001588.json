{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001588",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1588,
			"id": "M2279 N0901",
			"data": "1,3,3,5,7,11,17,27,43,69,111,179,289,467,755,1221,1975,3195,5169,8363,13531,21893,35423,57315,92737,150051,242787,392837,635623,1028459,1664081,2692539,4356619,7049157,11405775,18454931,29860705,48315635",
			"name": "a(n) = a(n-1) + a(n-2) - 1.",
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A001588/b001588.txt\"\u003eTable of n, a(n) for n = 0..500\u003c/a\u003e",
				"Massimiliano Fasi, Gian Maria Negri Porzio, \u003ca href=\"http://eprints.maths.manchester.ac.uk/id/eprint/2768\"\u003eDeterminants of Normalized Bohemian Upper Hessemberg Matrices\u003c/a\u003e, University of Manchester (England, 2019).",
				"Martin Griffiths, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL18/Griffiths/gr48.html\"\u003eOn a Matrix Arising from a Family of Iterated Self-Compositions\u003c/a\u003e, Journal of Integer Sequences, 18 (2015), #15.11.8.",
				"J. A. H. Hunter and F. D. Parker, \u003ca href=\"http://www.fq.math.ca/Scanned/5-3/elementary5-3.pdf\"\u003eProblem B-100\u003c/a\u003e, Fib. Quart., 5 (1967), p. 288.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,0,-1)."
			],
			"formula": [
				"From _Henry Bottomley_, Feb 20 2001: (Start)",
				"a(n) = 2*Fibonacci(n) + 1 = A000045(n) + A001611(n).",
				"G.f.: (1+x-3x^2)/(1-2*x+x^3). (End)",
				"If n\u003e=4, a(n) = floor(Phi*a(n-1)); Phi = (1 + sqrt(5))/2. - _Philippe Deléham_, Aug 08 2003",
				"a(n) = F(n) + F(n+3) + 1, n \u003e= -2 (where F(n) is the n-th Fibonacci number). - _Zerinvary Lajos_, Feb 01 2008",
				"a(n) = 1 + (2/5)*((1/2) + (1/2)*sqrt(5))^n*sqrt(5) - (2/5)*sqrt(5)*((1/2) - (1/2)*sqrt(5))^n, with n \u003e= 0. - _Paolo P. Lava_, Nov 21 2008"
			],
			"maple": [
				"A001588:=-(-1-z+3*z**2)/(z-1)/(z**2+z-1); # conjectured by _Simon Plouffe_ in his 1992 dissertation",
				"with(combinat): seq(fibonacci(n)+fibonacci(n+3)+1, n=-2..35); # _Zerinvary Lajos_, Feb 01 2008"
			],
			"mathematica": [
				"Fibonacci[Range[0,100]]*2+1 (* _Vladimir Joseph Stephan Orlovsky_, Mar 19 2010 *)"
			],
			"program": [
				"(PARI) a(n)=2*fibonacci(n)+1 \\\\ _Charles R Greathouse IV_, Apr 06 2016"
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 4,
			"revision": 60,
			"time": "2021-03-12T22:32:34-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}