{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A281487",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 281487,
			"data": "1,-1,0,-1,1,-2,2,-3,4,-5,4,-5,8,-9,7,-9,13,-14,12,-13,18,-21,17,-18,29,-31,23,-28,36,-37,36,-37,50,-55,42,-46,64,-65,53,-62,83,-84,75,-76,94,-107,90,-91,129,-132,107,-121,145,-146,135,-141,180,-193,157",
			"name": "a(n+1) = -Sum_{d|n} a(d), a(1) = 1.",
			"comment": [
				"a(1) = 1, any other choice simply adds a factor to all terms.",
				"Observations: sign of a(n) is -(-1)^n, the subsequences |a(n)| with n = 1, 2 mod 4 and |a(n)| with n = 3, 0 mod 4 both grow at n\u003e5. Both these subsequences seem to share the asymptotics with A003238 (and hence A000123): log(|a(n)|) is approximately proportional to (log(n/log(n)))^2; however, the factor is much less than log(4).",
				"There is a family of sequences with the formula a(n) = s*Sum_{d|(n-k), 1\u003c=d\u003cn} a(d). For s=+1 and k = 0,1,2, these are A002033, A003238, A007439. For s=-1 and k = 0,1,2, these are the Möbius function A008683, this sequence, and A281488."
			],
			"link": [
				"Andrey Zabolotskiy, \u003ca href=\"/A281487/b281487.txt\"\u003eTable of n, a(n) for n = 1..20000\u003c/a\u003e"
			],
			"formula": [
				"a(1) = 1.",
				"a(n+1) = -Sum_{d|n} a(d) for n\u003e=1.",
				"a(n+1) = Sum_{d|n} |a(d)|*(-1)^(d+n) for n\u003e=1.",
				"From _Ilya Gutkovskiy_, Apr 29 2019: (Start)",
				"G.f.: x * (1 - Sum_{n\u003e=1} a(n)*x^n/(1 - x^n)).",
				"L.g.f.: log(Product_{n\u003e=1} (1 - x^n)^(a(n)/n)) = Sum_{n\u003e=1} a(n+1)*x^n/n. (End)"
			],
			"example": [
				"a(9) = -(a(1)+a(2)+a(4)+a(8)) = -(1-1-1-3) = 4."
			],
			"program": [
				"(Python)",
				"a = [1]",
				"for n in range(1, 100):",
				"   a.append(-sum(a[d-1] for d in range(1, n+1) if n%d == 0))",
				"print(a)",
				"(PARI) lista(nn) = {my(va = vector(nn)); va[1] = 1; for (n=2, nn, va[n] = -sumdiv(n-1, d, va[d]);); va;} \\\\ _Michel Marcus_, Apr 29 2019"
			],
			"xref": [
				"Cf. A003238, A281488, A000123."
			],
			"keyword": "sign,easy",
			"offset": "1,6",
			"author": "_Andrey Zabolotskiy_, Jan 22 2017",
			"references": 11,
			"revision": 17,
			"time": "2019-04-29T17:23:03-04:00",
			"created": "2017-01-22T21:41:56-05:00"
		}
	]
}