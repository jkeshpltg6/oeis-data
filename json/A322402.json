{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A322402",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 322402,
			"data": "1,0,1,0,1,2,0,4,6,5,0,27,36,28,14,0,248,310,225,120,42,0,2830,3396,2332,1210,495,132,0,38232,44604,29302,14560,6006,2002,429,0,593859,678696,430200,204540,81900,28392,8008,1430,0,10401712,11701926,7204821,3289296,1263780,431256,129948,31824,4862",
			"name": "Triangle read by rows: The number of chord diagrams with n chords and k topologically connected components, 0 \u003c= k \u003c= n.",
			"comment": [
				"If all subsets are allowed instead of just pairs (chords), we get A324173. The rightmost column is A000108 (see Riordan). - _Gus Wiseman_, Feb 27 2019"
			],
			"link": [
				"P. Flajolet and M. Noy, \u003ca href=\"http://algo.inria.fr/flajolet/Publications/FlNo00.pdf\"\u003eAnalytic Combinatorics of Chord Diagrams\u003c/a\u003e, in: Formal power series and algebraic combinatorics (FPSAC '00) Moscow, 2000, \u003ca href=\"https://doi.org/10.1007/978-3-662-04166-6\"\u003ep 191-201\u003c/a\u003e, eq (2)",
				"J. Riordan, \u003ca href=\"https://doi.org/10.1090/S0025-5718-1975-0366686-9\"\u003eThe distribution of crossings of chords joining pairs of 2n points on a circle\u003c/a\u003e, Math. Comp., 29 (1975), 215-222.",
				"J. Riordan, \u003ca href=\"/A003480/a003480.pdf\"\u003eThe distribution of crossings of chords joining pairs of 2n points on a circle\u003c/a\u003e, Math. Comp., 29 (1975), 215-222. [Annotated scanned copy]",
				"Gus Wiseman, \u003ca href=\"/A322402/a322402.png\"\u003eChords diagrams with 3 chords, organized by number of components\u003c/a\u003e."
			],
			"formula": [
				"The g.f. satisfies g(z,w) = 1+w*A000699(w*g^2), where A000699(z) is the g.f. of A000699."
			],
			"example": [
				"From _Gus Wiseman_, Feb 27 2019: (Start)",
				"Triangle begins:",
				"  1",
				"  0      1",
				"  0      1      2",
				"  0      4      6      5",
				"  0     27     36     28     14",
				"  0    248    310    225    120     42",
				"  0   2830   3396   2332   1210    495    132",
				"  0  38232  44604  29302  14560   6006   2002    429",
				"  0 593859 678696 430200 204540  81900  28392   8008   1430",
				"Row n = 3 counts the following chord diagrams (see link for pictures):",
				"  {{1,3},{2,5},{4,6}}  {{1,2},{3,5},{4,6}}  {{1,2},{3,4},{5,6}}",
				"  {{1,4},{2,5},{3,6}}  {{1,3},{2,4},{5,6}}  {{1,2},{3,6},{4,5}}",
				"  {{1,4},{2,6},{3,5}}  {{1,3},{2,6},{4,5}}  {{1,4},{2,3},{5,6}}",
				"  {{1,5},{2,4},{3,6}}  {{1,5},{2,3},{4,6}}  {{1,6},{2,3},{4,5}}",
				"                       {{1,5},{2,6},{3,4}}  {{1,6},{2,5},{3,4}}",
				"                       {{1,6},{2,4},{3,5}}",
				"(End)"
			],
			"xref": [
				"Cf. A000699 (k = 1 column), A001147 (row sums), A000108 (diagonal), A002694 (subdiagonal k = n - 1).",
				"Cf. A000096, A003436, A016098, A099947, A136653, A278990, A293157, A324173, A324323, A324327, A324328."
			],
			"keyword": "nonn,tabl",
			"offset": "0,6",
			"author": "_R. J. Mathar_, Dec 06 2018",
			"ext": [
				"Offset changed to 0 by _Gus Wiseman_, Feb 27 2019"
			],
			"references": 5,
			"revision": 15,
			"time": "2019-03-01T08:05:57-05:00",
			"created": "2018-12-06T12:09:23-05:00"
		}
	]
}