{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A191754",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 191754,
			"data": "0,1,1,1,1,1,1,1,1,-1,-1,41,41,-589,-589,8317,8317,-869807,-869807,43056421,43056421,-250158593,-250158593,67632514765,67632514765,-1581439548217,-1581439548217",
			"name": "Numerators of a companion to the Bernoulli numbers.",
			"comment": [
				"The companion to the Bernoulli numbers BC(0, m) = A191754(m)/A192366(m) is, just like the Bernoulli numbers T(0, m) = A164555(m)/A027642(m), see A190339 for the T(n, m), an autosequence of the second kind, i.e., its inverse binomial transform is the sequence signed.",
				"In order to construct the companion array BC(n, m) we use the following rules: the main diagonal BC(n, n) = 0, the first upper diagonal BC(n, n+1) = T(n, n+1) and recurrence relation BC(n, m) = BC(n-1, m+1) - BC(n-1, m). The companion to the Bernoulli numbers appears in the first row of the BC(n, m) array, i.e., BC(0, m) = A191754(m)/A192366(m).",
				"For the denominators of the companion to the Bernoulli numbers see A192366."
			],
			"formula": [
				"a(2*n+2)/a(2*n+1) = A000012(n)",
				"BC(n, n) = 0, BC(n, n+1) = T(n, n+1) = T(n, n)/2 and BC(n, m) = BC(n-1, m+1) - BC(n-1, m); for the T(n, n+1) see A190339.",
				"BC(0, m) = A191754(m)/A192366(m), i.e., the companion to the Bernoulli numbers.",
				"Sum_{k=0..n} (-1)^(n-k)*binomial(n,k)*A191754(k)/A192366(k). = (-1)^(n+1)*A191754(n)/ A192366(n).",
				"Sum_{k=0..n} (-1)^(n-k)*binomial(n,k)*A164555(k)/A027642(k). = (-1)^n*A164555(n)/A027642(n).",
				"b(n) = A191754(n)/A192366(n) + A164555(n)/A027642(n) = [1, 1, 2/3, 1/3, 2/15, 1/15, 2/35, 1/35, -2/105, -1/105, ...] leads to b(2*n)/b(2*n+1) = 2 for n\u003e1."
			],
			"example": [
				"The first few rows of the BC(n,m) matrix are:",
				"0,        1/2,   1/2,    1/3,    1/6,    1/15,    1/30,",
				"1/2,        0,  -1/6,   -1/6,  -1/10,   -1/30,  -1/210,",
				"-1/2,    -1/6,     0,   1/15,   1/15,    1/35,  -1/105,",
				"1/3,      1/6,  1/15,      0, -4/105,  -4/105,       0,",
				"-1/6,   -1/10, -1/15, -4/105,      0,   4/105,   4/105,",
				"1/15,    1/30,  1/35,  4/105,  4/105,       0, -16/231,",
				"-1/30, -1/210, 1/105,      0, -4/105, -16/231,       0,"
			],
			"maple": [
				"nmax:=26: mmax:=nmax: A164555:=proc(n): if n=1 then 1 else numer(bernoulli(n)) fi: end: A027642:=proc(n): if n=1 then 2 else denom(bernoulli(n)) fi: end: for m from 0 to 2*mmax do T(0,m):=A164555(m)/A027642(m) od: for n from 1 to nmax do for m from 0 to 2*mmax do T(n,m):= T(n-1,m+1)-T(n-1,m) od: od: for n from 0 to nmax do BC(n,n):=0: BC(n,n+1) := T(n,n+1) od: for m from 2 to 2*mmax do for n from 0 to m-2 do BC(n,m):=BC(n,m-1) + BC(n+1,m-1) od: od: for n from 0 to 2*nmax do BC(n,0):=(-1)^(n+1)*BC(0,n) od: for m from 1 to mmax do for n from 2 to 2*nmax do BC(n,m) := BC(n,m-1) + BC(n+1,m-1) od: od: for n from 0 to nmax do seq(BC(n,m),m=0..mmax) od: seq(BC(0,n),n=0..nmax): seq(numer(BC(0,n)),n=0..nmax); # _Johannes W. Meijer_, Jul 02 2011"
			],
			"mathematica": [
				"max = 26; b[n_] := BernoulliB[n]; b[1]=1/2; bb = Table[b[n], {n, 0, max}]; diff = Table[ Differences[bb, n], {n, 1, Ceiling[max/2]}]; dd = Diagonal[diff]; bc[n_, n_] = 0; bc[n_, m_] /; m \u003c n := bc[n, m] = bc[n-1, m+1] - bc[n-1, m]; bc[n_, m_] /; m == n+1 := bc[n, m] = -dd[[n+1]]; bc[n_, m_] /; m \u003e n+1 := bc[n, m] = bc[n, m-1] + bc[n+1, m-1]; Table[bc[0, m], {m, 0, max}] // Numerator (* _Jean-François Alcover_, Aug 08 2012 *)"
			],
			"xref": [
				"Cf. A000012, A027642, A164555, A190339, A191754, A192366."
			],
			"keyword": "sign,frac",
			"offset": "0,12",
			"author": "_Paul Curtz_, Jun 15 2011",
			"ext": [
				"Edited by _Johannes W. Meijer_, Jul 02 2011"
			],
			"references": 5,
			"revision": 19,
			"time": "2015-12-30T16:14:47-05:00",
			"created": "2011-07-13T17:31:43-04:00"
		}
	]
}