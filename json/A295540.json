{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A295540",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 295540,
			"data": "1,1,1,2,1,2,3,1,4,2,3,5,1,5,5,2,7,3,5,8,1,9,5,5,10,2,9,9,3,12,5,8,13,1,13,10,5,15,5,11,15,2,17,9,9,18,3,16,15,5,20,8,13,21,1,22,13,10,23,5,19,20,5,25,11,15,26,2,25,19,9,28,9,20,27,3,30,16,15,31,5,27,25,8,33,13,21,34,1,34,23,13,36,10,27,33,5,38,19,20,39,5,35,30,11,41,15,27,41,2,43,25,19,44,9,36,37,9,46,20,27",
			"name": "Number of ways of writing n as the sum of a lower Wythoff number (A000201) and an upper Wythoff number (A001950), when zero is included in both sequences.",
			"comment": [
				"Note that floor(n*phi) and floor(n*phi^2), for n\u003e=1, form Beatty sequences."
			],
			"link": [
				"Paul D. Hanna, \u003ca href=\"/A295540/b295540.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e"
			],
			"formula": [
				"G.f.: [ Sum_{n\u003e=0} x^floor(n*phi) ] * [ Sum_{n\u003e=0} x^floor(n*phi^2) ], where phi = (1+sqrt(5))/2.",
				"G.f.: [1 + Sum_{n\u003e=1} x^A000201(n) ] * [1 + Sum_{n\u003e=1} x^A001950(n) ], where A000201 and A001950 are the lower and upper Wythoff sequences, respectively.",
				"a(Fibonacci(n+1)-1) = 1 for n\u003e=1.",
				"a(Fibonacci(n+2)-2) = Fibonacci(n) for n\u003e=1."
			],
			"example": [
				"G.f.: A(x) = 1 + x + x^2 + 2*x^3 + x^4 + 2*x^5 + 3*x^6 + x^7 + 4*x^8 + 2*x^9 + 3*x^10 + 5*x^11 + x^12 + 5*x^13 + 5*x^14 + 2*x^15 + 7*x^16 + 3*x^17 + 5*x^18 + 8*x^19 + x^20 + 9*x^21 + 5*x^22 + 5*x^23 + 10*x^24 + 2*x^25 + 9*x^26 + 9*x^27 + 3*x^28 + 12*x^29 + 5*x^30 + 8*x^31 + 13*x^32 + x^33 + 13*x^34 + 10*x^35 + 5*x^36 + 15*x^37 + 5*x^38 + 11*x^39 + 15*x^40 + 2*x^41 + 17*x^42 + 9*x^43 + 9*x^44 + 18*x^45 + 3*x^46 + 16*x^47 + 15*x^48 + 5*x^49 + 20*x^50 +...+ a(n)*x^n +...",
				"such that A(x) = WL(x) * WU(x) where",
				"WL(x) = 1 + x + x^3 + x^4 + x^6 + x^8 + x^9 + x^11 + x^12 + x^14 + x^16 + x^17 + x^19 + x^21 + x^22 + x^24 + x^25 + x^27 + x^29 + x^30 +...+ x^A000201(n) +...",
				"WU(x) = 1 + x^2 + x^5 + x^7 + x^10 + x^13 + x^15 + x^18 + x^20 + x^23 + x^26 + x^28 + x^31 + x^34 + x^36 + x^39 + x^41 + x^44 + x^47 + x^49 +...+ x^A001950(n) +...",
				"Terms equal 1 only at positions:",
				"[0, 1, 2, 4, 7, 12, 20, 33, 54, 88, 143, 232, 376, ..., Fibonacci(n+1)-1, ...]."
			],
			"program": [
				"(PARI) {a(n) = my(phi = (1 + sqrt(5))/2, WL=1, WU=1);",
				"WL = sum(m=0,floor(n/phi)+1, x^floor(m*phi) +x*O(x^n));",
				"WU = sum(m=0,floor(n/phi^2)+1, x^floor(m*phi^2) +x*O(x^n));",
				"polcoeff(WL*WU,n)}",
				"for(n=0,120, print1(a(n),\", \"))"
			],
			"xref": [
				"Cf. A259598, A000201, A001950."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_Paul D. Hanna_, Nov 30 2017",
			"references": 2,
			"revision": 18,
			"time": "2017-12-02T07:44:39-05:00",
			"created": "2017-11-30T18:05:53-05:00"
		}
	]
}