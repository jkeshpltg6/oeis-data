{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A268190",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 268190,
			"data": "1,2,2,1,3,1,1,2,3,1,1,4,3,2,1,1,2,6,3,2,1,1,4,7,5,2,2,1,1,3,11,5,5,2,2,1,1,4,13,10,5,4,2,2,1,1,2,20,11,8,5,4,2,2,1,1,6,23,16,10,8,4,4,2,2,1,1,2,33,20,15,9,8,4,4",
			"name": "Triangle read by rows: T(n,k) (n, k\u003e=1) is the number of partitions of n such that the difference between the two largest distinct parts is k; T(n,0) is the number of partitions of n in which all parts are equal.",
			"comment": [
				"Sum of entries in row n is A000041(n) (the partition numbers).",
				"T(n,0) = A000005(n) = number of divisors of n.",
				"T(n,1) = A083751(n+1) for n\u003e=3.",
				"Sum(k*T(n,k),k\u003e=1) = A268191(n).",
				"T(2n,n) = A002865(n) for n\u003e=2. - _Alois P. Heinz_, Feb 11 2016"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A268190/b268190.txt\"\u003eRows n = 1..200, flattened\u003c/a\u003e"
			],
			"formula": [
				"G.f.: G(t,x) = Sum_{k\u003e0} (x^k/(1-x^k)) + Sum_{k\u003e1} (Sum_{j=1..i-1} t^{i-j}*x^{i+j}/((1 - x^i)*Product_{k=1..j} (1 - x^k)))."
			],
			"example": [
				"T(5,0)=2 because we have [5] and [1,1,1,1,1]; T(5,1)=3 because we have [3,2], [2,2,1], and [2,1,1,1]; T(5,2)=1 because we have [3,1,1]; T(5,3)=1 because we have [4,1].",
				"Triangle starts:",
				"1;",
				"2;",
				"2,1;",
				"3,1,1;",
				"2,3,1,1;",
				"4,3,2,1,1;"
			],
			"maple": [
				"G := add(x^k/(1-x^k), k = 1 .. 80)+ add(add(t^(i-j)*x^(i+j)/((1-x^i)*mul(1-x^k,k = 1 .. j)), j = 1 .. i-1), i = 2 .. 80): Gser := simplify(series(G, x = 0, 35)): for n from 0 to 30 do P[n] := sort(coeff(Gser, x, n)) end do: 1; for n from 2 to 25 do seq(coeff(P[n], t, j), j = 0 .. n-2) end do; # yields sequence in triangular form",
				"# second Maple program:",
				"b:= proc(n, l, i) option remember; `if`(irem(n, i)=0, x^",
				"      `if`(l=0, 0, i-l), 0) +`if`(i\u003en, 0, add(b(n-i*j,",
				"      `if`(j=0, l, i), i+1), j=0..(n-1)/i))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=0..degree(p)))(b(n, 0, 1)):",
				"seq(T(n), n=1..30);  # _Alois P. Heinz_, Feb 11 2016"
			],
			"mathematica": [
				"b[n_, l_, i_] := b[n, l, i] = If[Mod[n, i] == 0, x^If[l == 0, 0, i-l], 0] + If[i\u003en, 0, Sum[b[n-i*j, If[j == 0, l, i], i+1], {j, 0, (n-1)/i}]]; T[n_] := Function[p, Table[Coefficient[p, x, i], {i, 0, Exponent[p, x]}]][b[n, 0, 1]]; Table[T[n], {n, 1, 30}] // Flatten (* _Jean-François Alcover_, Dec 21 2016, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A000005, A000041, A002865, A083751, A268191."
			],
			"keyword": "nonn,tabf",
			"offset": "1,2",
			"author": "_Emeric Deutsch_, Feb 10 2016",
			"references": 2,
			"revision": 14,
			"time": "2016-12-21T08:17:24-05:00",
			"created": "2016-02-11T09:57:03-05:00"
		}
	]
}