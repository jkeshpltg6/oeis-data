{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A278071",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 278071,
			"data": "1,1,-1,6,-4,1,60,-36,9,-1,840,-480,120,-16,1,15120,-8400,2100,-300,25,-1,332640,-181440,45360,-6720,630,-36,1,8648640,-4656960,1164240,-176400,17640,-1176,49,-1,259459200,-138378240,34594560,-5322240,554400,-40320,2016,-64,1",
			"name": "Triangle read by rows, coefficients of the polynomials P(n,x) = (-1)^n*hypergeom( [n,-n], [], x), powers in descending order.",
			"link": [
				"H. L. Krall and O. Fink, \u003ca href=\"https://doi.org/10.1090/S0002-9947-1949-0028473-1\"\u003eA New Class of Orthogonal Polynomials: The Bessel Polynomials\u003c/a\u003e, Trans. Amer. Math. Soc. 65, 100-115, 1949.",
				"Herbert E. Salzer, \u003ca href=\"https://doi.org/10.1090/S0025-5718-1955-0078498-1\"\u003eOrthogonal Polynomials Arising in the Numerical Evaluation of Inverse Laplace Transforms\u003c/a\u003e, Mathematical Tables and Other Aids to Computation, Vol. 9, No. 52 (Oct., 1955), pp. 164-177, (see p.174 and footnote 7)."
			],
			"formula": [
				"The P(n,x) are orthogonal polynomials. They satisfy the recurrence",
				"P(n,x) = ((((4*n-2)*(2*n-3)*x+2)*P(n-1,x)+(2*n-1)*P(n-2,x))/(2*n-3)) for n\u003e=2.",
				"In terms of generalized Laguerre polynomials (see the Krall and Fink link):",
				"P(n,x) = n!*(-x)^n*LaguerreL(n,-2*n,-1/x)."
			],
			"example": [
				"Triangle starts:",
				".       1,",
				".       1,      -1,",
				".       6,      -4,     1,",
				".      60,     -36,     9,    -1,",
				".     840,    -480,   120,   -16,   1,",
				".   15120,   -8400,  2100,  -300,  25,  -1,",
				".  332640, -181440, 45360, -6720, 630, -36, 1,",
				"..."
			],
			"maple": [
				"p := n -\u003e (-1)^n*hypergeom([n, -n], [], x):",
				"ListTools:-Flatten([seq(PolynomialTools:-CoefficientList(simplify(p(n)), x, termorder=reverse), n=0..8)]);",
				"# Alternatively the polynomials by recurrence:",
				"P := proc(n,x) if n=0 then return 1 fi; if n=1 then return x-1 fi;",
				"((((4*n-2)*(2*n-3)*x+2)*P(n-1,x)+(2*n-1)*P(n-2,x))/(2*n-3));",
				"sort(expand(%)) end: for n from 0 to 6 do lprint(P(n,x)) od;",
				"# Or by generalized Laguerre polynomials:",
				"P := (n,x) -\u003e n!*(-x)^n*LaguerreL(n,-2*n,-1/x):",
				"for n from 0 to 6 do simplify(P(n,x)) od;"
			],
			"mathematica": [
				"row[n_] := CoefficientList[(-1)^n HypergeometricPFQ[{n, -n}, {}, x], x] // Reverse;",
				"Table[row[n], {n, 0, 8}] // Flatten (* _Jean-François Alcover_, Jul 12 2019 *)"
			],
			"xref": [
				"Cf. A278069 (x=1, row sums up to sign), A278070 (x=-1).",
				"T(n,0) = Pochhammer(n, n) (cf. A000407).",
				"T(n,1) = -(n+1)*(2n)!/n! (cf. A002690).",
				"T(n,2) = (n+2)*(2n+1)*(2n-1)!/(n-1)! (cf. A002691).",
				"T(n,n-1) = (-1)^(n+1)*n^2 for n\u003e=1 (cf. A000290).",
				"T(n,n-2) = n^2*(n^2-1)/2 for n\u003e=2 (cf. A083374)."
			],
			"keyword": "sign,tabl",
			"offset": "0,4",
			"author": "_Peter Luschny_, Nov 10 2016",
			"references": 2,
			"revision": 24,
			"time": "2019-07-12T16:14:37-04:00",
			"created": "2016-11-11T08:13:30-05:00"
		}
	]
}