{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001692",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1692,
			"id": "M3804 N1554",
			"data": "1,5,10,40,150,624,2580,11160,48750,217000,976248,4438920,20343700,93900240,435959820,2034504992,9536718750,44878791360,211927516500,1003867701480,4768371093720,22706531339280",
			"name": "Number of irreducible polynomials of degree n over GF(5); dimensions of free Lie algebras.",
			"comment": [
				"Exponents in expansion of Hardy-Littlewood constant C_5 = 0.409874885.. = A269843 as a product_{n\u003e=2} zeta(n)^(-a(n)).",
				"Number of aperiodic necklaces with n beads of 5 colors. - _Herbert Kociemba_, Nov 25 2016"
			],
			"reference": [
				"E. R. Berlekamp, Algebraic Coding Theory, McGraw-Hill, NY, 1968, p. 84.",
				"Steven R. Finch, Mathematical Constants, Cambridge University Press, 2003",
				"M. Lothaire, Combinatorics on Words. Addison-Wesley, Reading, MA, 1983, p. 79.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A001692/b001692.txt\"\u003eTable of n, a(n) for n = 0..1435\u003c/a\u003e (terms 0..200 from T. D. Noe)",
				"Alin Bostan, Alexander Marynych, and Kilian Raschel, \u003ca href=\"https://arxiv.org/abs/1901.03002\"\u003eOn the least common multiple of several random~integers\u003c/a\u003e, arXiv:1901.03002 [math.PR], 2019.",
				"Jeremie Detrey, P. J. Spaenlehauer, and P. Zimmermann, \u003ca href=\"https://members.loria.fr/PZimmermann/papers/rho.pdf\"\u003eComputing the rho constant\u003c/a\u003e, Preprint 2016.",
				"E. N. Gilbert and J. Riordan, \u003ca href=\"http://projecteuclid.org/euclid.ijm/1255631587\"\u003eSymmetry types of periodic sequences\u003c/a\u003e, Illinois J. Math., 5 (1961), 657-665.",
				"Gareth A. Jones and Alexander K. Zvonkin, \u003ca href=\"https://www.labri.fr/perso/zvonkin/Research/ProjPrimesShort.pdf\"\u003eGroups of prime degree and the Bateman-Horn conjecture\u003c/a\u003e, 2021.",
				"G. Niklasch, \u003ca href=\"/A001692/a001692.html\"\u003eSome number theoretical constants: 1000-digit values\u003c/a\u003e [Cached copy]",
				"Y. Puri and T. Ward, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL4/WARD/short.html\"\u003eArithmetic and growth of periodic orbits\u003c/a\u003e, J. Integer Seqs., Vol. 4 (2001), #01.2.1.",
				"Dionisel Y. Regalado and Rodel Azura, \u003ca href=\"https://rmrj.usjr.edu.ph/index.php/RMRJ/article/view/532\"\u003eAn Analytic Approximation to the Density of Twin Primes\u003c/a\u003e, Recoletos Multidisciplinary Research Journal (2019) Vol. 6, No. 2.",
				"G. J. Simmons, \u003ca href=\"http://www.jstor.org/stable/2316211\"\u003eThe number of irreducible polynomials of degree n over GF(p)\u003c/a\u003e, Amer. Math. Monthly, 77 (1970), 743-745.",
				"G. Viennot, \u003ca href=\"http://dx.doi.org/10.1007/BFb0067950\"\u003eAlgèbres de Lie Libres et Monoïdes Libres\u003c/a\u003e, Lecture Notes in Mathematics 691, Springer Verlag 1978.",
				"\u003ca href=\"/index/Lu#Lyndon\"\u003eIndex entries for sequences related to Lyndon words\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Sum_{d|n} mu(d)*5^(n/d)/n, for n\u003e0.",
				"G.f.: k=5, 1 - Sum_{i\u003e=1} mu(i)*log(1 - k*x^i)/i. - _Herbert Kociemba_, Nov 25 2016"
			],
			"mathematica": [
				"a[0] = 1; a[n_] := Sum[MoebiusMu[d]*5^(n/d)/n, {d, Divisors[n]}]; Table[a[n], {n, 0, 21}] (* _Jean-François Alcover_, Mar 11 2014 *)",
				"mx=40;f[x_,k_]:=1-Sum[MoebiusMu[i] Log[1-k*x^i]/i,{i,1,mx}];CoefficientList[Series[f[x,5],{x,0,mx}],x] (* _Herbert Kociemba_, Nov 25 2016 *)"
			],
			"program": [
				"(PARI) a(n)=if(n,sumdiv(n,d,moebius(d)*5^(n/d))/n,1) \\\\ _Charles R Greathouse IV_, Jun 15 2011",
				"(Haskell)",
				"a001692 n = flip div n $ sum $",
				"            zipWith (*) (map a008683 divs) (map a000351 $ reverse divs)",
				"            where divs = a027750_row n",
				"-- _Reinhard Zumkeller_, Oct 07 2015"
			],
			"xref": [
				"Cf. A001037, A054720, A002105.",
				"5th column of A074650. - _Alois P. Heinz_, Aug 08 2008",
				"Cf. A008683, A000351, A027750."
			],
			"keyword": "nonn,nice,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 69,
			"revision": 77,
			"time": "2021-11-17T07:18:03-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}