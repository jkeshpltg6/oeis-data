{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A332713",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 332713,
			"data": "1,2,3,4,5,6,7,7,8,10,11,12,13,14,15,13,17,16,19,20,21,22,23,21,22,26,22,28,29,30,31,24,33,34,35,32,37,38,39,35,41,42,43,44,40,46,47,39,44,44,51,52,53,44,55,49,57,58,59,60,61,62,56,46,65,66,67,68,69,70",
			"name": "a(n) = Sum_{d|n} phi(d/gcd(d, n/d)).",
			"link": [
				"Vaclav Kotesovec, \u003ca href=\"/A332713/a332713.jpg\"\u003eGraph - the asymptotic ratio (1000000 terms)\u003c/a\u003e"
			],
			"formula": [
				"Dirichlet g.f.: zeta(s) * zeta(2*s) * zeta(s - 1) * Product_{p prime} (1 - p^(-s) + p^(-2*s) - p^(1 - 2*s)).",
				"a(n) = Sum_{d|n} phi(lcm(d, n/d)/d).",
				"a(n) = Sum_{d|n} A010052(n/d) * A055653(d).",
				"Sum_{k=1..n} a(k) ~ c * Pi^6 * n^2 / 1080, where c = A330523 = Product_{primes p} (1 - 1/p^2 - 1/p^3 + 1/p^4) = 0.5358961538283379998085... - _Vaclav Kotesovec_, Feb 22 2020",
				"From _Richard L. Ollerton_, May 10 2021: (Start)",
				"a(n) = Sum_{k=1..n} phi(gcd(n,k)/gcd(gcd(n,k),n/gcd(n,k)))/phi(n/gcd(n,k)).",
				"a(n) = Sum_{k=1..n} phi(n/gcd(n,k)/gcd(gcd(n,k),n/gcd(n,k)))/phi(n/gcd(n,k)).",
				"a(n) = Sum_{k=1..n} phi(lcm(gcd(n,k),n/gcd(n,k))/gcd(n,k))/phi(n/gcd(n,k)).",
				"a(n) = Sum_{k=1..n} phi(lcm(gcd(n,k),n/gcd(n,k))*gcd(n,k)/n)/phi(n/gcd(n,k)).",
				"a(n) = Sum_{k=1..n} A010052(gcd(n,k))*A055653(n/gcd(n,k))/phi(n/gcd(n,k)).",
				"a(n) = Sum_{k=1..n} A010052(n/gcd(n,k))*A055653(gcd(n,k))/phi(n/gcd(n,k)). (End)"
			],
			"mathematica": [
				"Table[Sum[EulerPhi[d/GCD[d, n/d]], {d, Divisors[n]}], {n, 1, 70}]",
				"A055653[n_] := Sum[Boole[GCD[d, n/d] == 1] EulerPhi[d], {d, Divisors[n]}]; a[n_] := Sum[Boole[IntegerQ[(n/d)^(1/2)]] A055653[d], {d, Divisors[n]}]; Table[a[n], {n, 1, 70}]"
			],
			"program": [
				"(PARI) a(n) = sumdiv(n, d, eulerphi(d/gcd(d, n/d))); \\\\ _Michel Marcus_, Feb 20 2020"
			],
			"xref": [
				"Cf. A000010, A001616, A010052, A046790 (numbers n such that a(n) \u003c n), A055653, A061884, A078779 (fixed points), A332619, A332686, A332712."
			],
			"keyword": "nonn,mult",
			"offset": "1,2",
			"author": "_Ilya Gutkovskiy_, Feb 20 2020",
			"references": 4,
			"revision": 16,
			"time": "2021-05-20T10:56:28-04:00",
			"created": "2020-02-22T08:31:33-05:00"
		}
	]
}