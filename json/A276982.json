{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A276982",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 276982,
			"data": "4,6,8,10,10,10,7,19,18,16,19,17,16,11,20,19,21,22,21,19,30,21,22,23,30,22,30,30,30,7,24,27,23,28,24,29,45,25,29,20,53,28,50,45,50,30,24,25,48,25,45,40,45,26,53,48,53,45,50,45,10,27,26,32,24,26",
			"name": "a(n) = number of primes p whose balanced ternary representation is compatible with the binary representation of A276194(n).",
			"comment": [
				"Let B = binary representation of A276194(n), and let C = C(p) = balanced ternary (bt) representation of a prime p (see A117966). Thus C is a string of 0's, 1's, and -1's. We will write T instead of -1.",
				"We say that C is compatible with B if (i) length(C) = length(B); (ii) C has a 1 or T wherever B has a 1; and (iii) there is exactly one 1 or T in C in the positions where B is 0, and otherwise C has a 0 whenever B has a 0.",
				"Then a(n) is the number of primes p for which C(p) is compatible with B.",
				"It is conjectured that all a(n) \u003e 0.  This has been checked for n \u003c= 100000. But it is possible that there is a counterexample for very large n."
			],
			"link": [
				"Lei Zhou, \u003ca href=\"/A276982/b276982.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"n=1, A276194(1) = 5, or 101 in binary form. Using this as mask to generate positive balanced ternary numbers that allow 1 or T on all 1 digits, but only one digits 1 or T falls on a 0 digits, the following balanced ternary numbers can be generated: 1TT=5, 1T1=7, 11T=11, 111=13. All the four numbers are primes.  So a(1)=4.",
				"n=2, A276194(2) = 9, or 1001 in binary form. Using this as mask to generate positive balanced ternary numbers that allow 1 or T on all 1 digits, but only one digits 1 or T falls on a 0 digits, the following balanced ternary numbers can be generated: 1T0T=17, 1T01=19, 10TT=23, 10T1=25, 101T=29, 1011=31, 110T=35, 1101=37.  Among the 8 numbers, 6 of them (17, 19, 23, 29, 31, and 37) are primes.  So a(2)=6."
			],
			"mathematica": [
				"BNDigits[m_Integer] := Module[{n = m, d, t = {}},",
				"   While[n \u003e 0, d = Mod[n, 2]; PrependTo[t, d]; n = (n - d)/2]; t];",
				"c = 1;",
				"Table[ While[c = c + 2; d = BNDigits[c]; ld = Length[d];",
				"   c1 = Total[d]; !(EvenQ[c1] \u0026\u0026 (c1 \u003c ld))];",
				"  l = Length[d]; flps = Flatten[Position[Reverse[d], 1]] - 1;",
				"  flps = Delete[flps, Length[flps]];",
				"  sfts = Flatten[Position[Reverse[d], 0]] - 1; lf = Length[flps]; ls = Length[sfts]; ct = 0;",
				"  Do[Do[cp10 = 3^(l - 1) + 3^(sfts[[i]]);",
				"    cp20 = 3^(l - 1) - 3^(sfts[[i]]); di = BNDigits[j];",
				"    While[Length[di] \u003c lf, PrependTo[di, 0]]; Do[",
				"     If[di[[k]] == 0, cp10 = cp10 - 3^flps[[k]];",
				"      cp20 = cp20 - 3^flps[[k]], cp10 = cp10 + 3^flps[[k]];",
				"      cp20 = cp20 + 3^flps[[k]]], {k, 1, lf}];",
				"    If[PrimeQ[cp10], ct++]; If[PrimeQ[cp20], ct++], {j, 0, 2^lf - 1}], {i, 1, ls}]; ct, {n, 1, 66}]"
			],
			"xref": [
				"Cf. A276194."
			],
			"keyword": "nonn,base",
			"offset": "1,1",
			"author": "_Lei Zhou_, Oct 20 2016",
			"ext": [
				"Edited by _N. J. A. Sloane_, Nov 05 2016"
			],
			"references": 1,
			"revision": 45,
			"time": "2020-02-27T20:52:22-05:00",
			"created": "2016-11-05T13:13:12-04:00"
		}
	]
}