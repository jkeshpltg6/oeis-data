{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006519",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6519,
			"id": "M0162",
			"data": "1,2,1,4,1,2,1,8,1,2,1,4,1,2,1,16,1,2,1,4,1,2,1,8,1,2,1,4,1,2,1,32,1,2,1,4,1,2,1,8,1,2,1,4,1,2,1,16,1,2,1,4,1,2,1,8,1,2,1,4,1,2,1,64,1,2,1,4,1,2,1,8,1,2,1,4,1,2,1,16,1,2,1,4,1,2,1,8,1,2,1,4,1,2,1,32,1,2,1,4,1,2",
			"name": "Highest power of 2 dividing n.",
			"comment": [
				"Least positive k such that m^k + 1 divides m^n + 1 (with fixed base m). - _Vladimir Baltic_, Mar 25 2002",
				"To construct the sequence: start with 1, concatenate 1, 1 and double last term gives 1, 2. Concatenate those 2 terms, 1, 2, 1, 2 and double last term 1, 2, 1, 2 -\u003e 1, 2, 1, 4. Concatenate those 4 terms: 1, 2, 1, 4, 1, 2, 1, 4 and double last term -\u003e 1, 2, 1, 4, 1, 2, 1, 8, etc. - _Benoit Cloitre_, Dec 17 2002",
				"a(n) = gcd(seq(binomial(2*n, 2*m+1)/2, m = 0 .. n - 1)) (odd numbered entries of even numbered rows of Pascal's triangle A007318 divided by 2), where gcd() denotes the greatest common divisor of a set of numbers. Due to the symmetry of the rows it suffices to consider m = 0 .. floor((n-1)/2). - _Wolfdieter Lang_, Jan 23 2004",
				"Equals the continued fraction expansion of a constant x (cf. A100338) such that the continued fraction expansion of 2*x interleaves this sequence with 2's: contfrac(2*x) = [2; 1, 2, 2, 2, 1, 2, 4, 2, 1, 2, 2, 2, 1, 2, 8, 2, ...].",
				"_Simon Plouffe_ observes that this sequence and A003484 (Radon function) are very similar, the difference being all zeros except for every 16th term (see A101119 for nonzero differences). Dec 02 2004",
				"This sequence arises when calculating the next odd number in a Collatz sequence: Next(x) = (3*x + 1) / A006519, or simply (3*x + 1) / BitAnd(3*x + 1, -3*x - 1). - Jim Caprioli, Feb 04 2005",
				"a(n) = n if and only if n = 2^k. This sequence can be obtained by taking a(2^n) = 2^n in place of a(2^n) = n and using the same sequence building approach as in A001511. - _Amarnath Murthy_, Jul 08 2005",
				"Also smallest m such that m + n - 1 = m XOR (n - 1); A086799(n) = a(n) + n - 1. - _Reinhard Zumkeller_, Feb 02 2007",
				"Number of 1's between successive 0's in A159689. - _Philippe Deléham_, Apr 22 2009",
				"Least number k such that all coefficients of k*E(n, x), the n-th Euler polynomial, are integers (Cf. A144845). - _Peter Luschny_, Nov 13 2009",
				"In the binary expansion of n, delete everything left of the rightmost 1 bit. - _Ralf Stephan_, Aug 22 2013",
				"The equivalent sequence for partitions is A194446. - _Omar E. Pol_, Aug 22 2013",
				"Also the 2-adic value of 1/n, n \u003e= 1. See the Mahler reference, definition on p. 7. This is a non-archimedean valuation. See Mahler, p. 10. Sometimes called 2-adic absolute value of 1/n. - _Wolfdieter Lang_, Jun 28 2014",
				"First 2^(k-1) - 1 terms are also the heights of the successive rectangles and squares of width 2 that are adjacent to any of the four sides of the toothpick structure of A139250 after 2^k stages, with k \u003e= 2. For example: if k = 5 the heights after 32 stages are [1, 2, 1, 4, 1, 2, 1, 8, 1, 2, 1, 4, 1, 2, 1] respectively, the same as the first 15 terms of this sequence. - _Omar E. Pol_, Dec 29 2020"
			],
			"reference": [
				"K. Mahler, p-adic numbers and their functions, second ed., Cambridge University Press, 1981.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A006519/b006519.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Dzmitry Badziahin and Jeffrey Shallit, \u003ca href=\"http://arxiv.org/abs/1505.00667\"\u003eAn Unusual Continued Fraction\u003c/a\u003e, arXiv:1505.00667 [math.NT], 2015.",
				"Tyler Ball, Tom Edgar, and Daniel Juda, \u003ca href=\"http://dx.doi.org/10.4169/math.mag.87.2.135\"\u003eDominance Orders, Generalized Binomial Coefficients, and Kummer's Theorem\u003c/a\u003e, Mathematics Magazine, Vol. 87, No. 2, April 2014, pp. 135-143.",
				"M. Beeler, R. W. Gosper and R. Schroeppel, \u003ca href=\"http://www.inwap.com/pdp10/hbaker/hakmem/hacks.html#item175\"\u003eItem 175\u003c/a\u003e, in Beeler, M., Gosper, R. W. and Schroeppel, R. HAKMEM. MIT AI Memo 239, Feb 29 1972.",
				"Ron Brown and Jonathan L. Merzel, \u003ca href=\"http://www.fq.math.ca/Papers1/45-2/brown.pdf\"\u003eThe number of Ducci sequences with a given period\u003c/a\u003e, Fib. Quart., 45 (2007), 115-121.",
				"Daniel Bruns, Wojciech Mostowski, and Mattias Ulbrich, \u003ca href=\"http://dx.doi.org/10.1007/s10009-013-0293-y\"\u003eImplementation-level verification of algorithms with KeY\u003c/a\u003e, International Journal on Software Tools for Technology Transfer, November 2013.",
				"Victor Meally, \u003ca href=\"/A006516/a006516.pdf\"\u003eLetter to N. J. A. Sloane, May 1975\u003c/a\u003e",
				"Laurent Orseau, Levi H. S. Lelis, and Tor Lattimore, \u003ca href=\"https://arxiv.org/abs/1906.03242\"\u003eZooming Cautiously: Linear-Memory Heuristic Search With Node Expansion Guarantees\u003c/a\u003e, arXiv:1906.03242 [cs.AI], 2019.",
				"Laurent Orseau, Levi H. S. Lelis, Tor Lattimore, and Théophane Weber, \u003ca href=\"https://arxiv.org/abs/1811.10928\"\u003eSingle-Agent Policy Tree Search With Guarantees\u003c/a\u003e, arXiv:1811.10928 [cs.AI], 2018, also in Advances in Neural Information Processing Systems, 32nd Conference on Neural Information Processing Systems (NIPS 2018), Montréal, Canada.",
				"Ralf Stephan, \u003ca href=\"/somedcgf.html\"\u003eSome divide-and-conquer sequences ...\u003c/a\u003e",
				"Ralf Stephan, \u003ca href=\"/A079944/a079944.ps\"\u003eTable of generating functions\u003c/a\u003e",
				"Ralf Stephan, \u003ca href=\"https://arxiv.org/abs/math/0307027\"\u003eDivide-and-conquer generating functions. I. Elementary sequences\u003c/a\u003e, arXiv:math/0307027 [math.CO], 2003.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/EvenPart.html\"\u003eEven Part\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Converse_nonimplication\"\u003eConverse nonimplication\u003c/a\u003e.",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e"
			],
			"formula": [
				"a(n) = n AND -n (where \"AND\" is bitwise, and negative numbers are represented in two's complement in a suitable bit width). - _Marc LeBrun_, Sep 25 2000, clarified by _Alonso del Arte_, Mar 16 2020",
				"Also: a(n) = gcd(2^n, n). - _Labos Elemer_, Apr 22 2003",
				"Multiplicative with a(p^e) = p^e if p = 2; 1 if p \u003e 2. - _David W. Wilson_, Aug 01 2001",
				"G.f.: Sum_{k\u003e=0} 2^k*x^2^k/(1 - x^2^(k+1)). - _Ralf Stephan_, May 06 2003",
				"Dirichlet g.f.: zeta(s)*(2^s - 1)/(2^s - 2) = zeta(s)*(1-2^(-s)/(1 - 2*2^(-s)). - _Ralf Stephan_, Jun 17 2007",
				"a(n) = 2^floor(A002487(n - 1) / A002487(n)). - _Reikku Kulon_, Oct 05 2008",
				"a(n) = 2^A007814(n). - _R. J. Mathar_, Oct 25 2010",
				"a((2*k - 1)*2^e) = 2^e, k \u003e= 1, e \u003e= 0. - _Johannes W. Meijer_, Jun 07 2011",
				"a(n) = denominator of Euler(n-1, 1). - _Arkadiusz Wesolowski_, Jul 12 2012",
				"a(n) = A011782(A001511(n)). - _Omar E. Pol_, Sep 13 2013",
				"a(n) = (n XOR floor(n/2)) XOR (n-1 XOR floor((n-1)/2)) = n - (n AND n-1) (where \"AND\" is bitwise). - _Gary Detlefs_, Jun 12 2014",
				"a(n) = ((n XOR n-1)+1)/2. - _Gary Detlefs_, Jul 02 2014",
				"a(n) = A171977(n)/2. - _Peter Kern_, Jan 04 2017",
				"a(n) = 2^(A001511(n)-1). - _Doug Bell_, Jun 02 2017",
				"a(n) = abs(A003188(n-1) - A003188(n)). - _Doug Bell_, Jun 02 2017",
				"Conjecture: a(n) = (1/(A000203(2*n)/A000203(n)-2)+1)/2. - _Velin Yanev_, Jun 30 2017",
				"a(n) = (n-1) o n where 'o' is the bitwise converse nonimplication. 'o' is not commutative. n o (n+1) = A135481(n). - _Peter Luschny_, Oct 10 2019",
				"From _Peter Munn_, Dec 13 2019: (Start)",
				"a(A225546(n)) = A225546(A007913(n)).",
				"a(A059897(n,k)) = A059897(a(n), a(k)).",
				"(End)"
			],
			"example": [
				"2^3 divides 24, but 2^4 does not divide 24, so a(24) = 8.",
				"2^0 divides 25, but 2^1 does not divide 25, so a(25) = 1.",
				"2^1 divides 26, but 2^2 does not divide 26, so a(26) = 2.",
				"Per _Marc LeBrun_'s 2000 comment, a(n) can also be determined with bitwise operations in two's complement. For example, given n = 48, we see that n in binary in an 8-bit byte is 00110000 while -n is 11010000. Then 00110000 AND 11010000 = 00010000, which is 16 in decimal, and therefore a(48) = 16.",
				"G.f. = x + 2*x^2 + x^3 + 4*x^4 + x^5 + 2*x^6 + x^7 + 8*x^8 + x^9 + ..."
			],
			"maple": [
				"with(numtheory): for n from 1 to 200 do if n mod 2 = 1 then printf(`%d,`,1) else printf(`%d,`,2^ifactors(n)[2][1][2]) fi; od:",
				"A006519 := proc(n) if type(n,'odd') then 1 ; else for f in ifactors(n)[2] do if op(1,f) = 2 then return 2^op(2,f) ; end if; end do: end if; end proc: # _R. J. Mathar_, Oct 25 2010",
				"A006519 := n -\u003e 2^padic[ordp](n,2): # _Peter Luschny_, Nov 26 2010"
			],
			"mathematica": [
				"lowestOneBit[n_] := Block[{k = 0}, While[Mod[n, 2^k] == 0, k++]; 2^(k - 1)]; Table[lowestOneBit[n], {n, 102}] (* _Robert G. Wilson v_ Nov 17 2004 *)",
				"Table[2^IntegerExponent[n, 2], {n, 128}] (* _Jean-François Alcover_, Feb 10 2012 *)",
				"Table[BitAnd[BitNot[i - 1], i], {i, 1, 102}] (* _Peter Luschny_, Oct 10 2019 *)"
			],
			"program": [
				"(PARI) {a(n) = 2^valuation(n, 2)};",
				"(PARI) a(n)=1\u003c\u003cvaluation(n,2); \\\\ _Joerg Arndt_, Jun 10 2011",
				"(PARI) a(n)=bitand(n,-n); \\\\ _Joerg Arndt_, Jun 10 2011",
				"(PARI) a(n)=direuler(p=2,n,if(p==2,1/(1-2*X),1/(1-X)))[n] \\\\ _Ralf Stephan_, Mar 27 2015",
				"(Haskell)",
				"import Data.Bits ((.\u0026.))",
				"a006519 n = n .\u0026. (-n) :: Integer",
				"-- _Reinhard Zumkeller_, Mar 11 2012, Dec 29 2011",
				"(MAGMA) [2^Valuation(n, 2): n in [1..100]]; // _Vincenzo Librandi_, Mar 27 2015",
				"(Scala) (1 to 128).map(Integer.lowestOneBit(_)) // _Alonso del Arte_, Mar 04 2020",
				"(Julia)",
				"using IntegerSequences",
				"[EvenPart(n) for n in 1:102] |\u003e println  # _Peter Luschny_, Sep 25 2021"
			],
			"xref": [
				"Partial sums are in A006520, second partial sums in A022560.",
				"Sequences used in definitions of this sequence: A000079, A001511, A004198, A007814.",
				"Cf. A100338, A003484, A101119, A002487, A139250.",
				"Sequences with related definitions: A038712, A171977, A135481 (GS(1, 6)).",
				"This is Guy Steele's sequence GS(5, 2) (see A135416).",
				"Related to A007913 via A225546.",
				"A059897 is used to express relationship between sequence terms."
			],
			"keyword": "nonn,easy,nice,mult,hear",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, _Simon Plouffe_",
			"ext": [
				"More terms from _James A. Sellers_, Jun 20 2000"
			],
			"references": 277,
			"revision": 242,
			"time": "2021-09-25T23:57:15-04:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}