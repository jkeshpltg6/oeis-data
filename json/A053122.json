{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A053122",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 53122,
			"data": "1,-2,1,3,-4,1,-4,10,-6,1,5,-20,21,-8,1,-6,35,-56,36,-10,1,7,-56,126,-120,55,-12,1,-8,84,-252,330,-220,78,-14,1,9,-120,462,-792,715,-364,105,-16,1,-10,165,-792,1716,-2002,1365,-560,136,-18,1,11,-220,1287,-3432,5005,-4368,2380,-816,171,-20",
			"name": "Triangle of coefficients of Chebyshev's S(n,x-2) = U(n,x/2-1) polynomials (exponents of x in increasing order).",
			"comment": [
				"Apart from signs, identical to A078812.",
				"Another version with row-leading 0's and differing signs is given by A285072.",
				"G.f. for row polynomials S(n,x-2) (signed triangle): 1/(1+(2-x)*z+z^2). Unsigned triangle |a(n,m)| has g.f. 1/(1-(2+x)*z+z^2) for row polynomials.",
				"Row sums (signed triangle) A049347(n) (periodic(1,-1,0)). Row sums (unsigned triangle) A001906(n+1)=F(2*(n+1)) (even indexed Fibonacci).",
				"In the language of Shapiro et al. (see A053121 for the reference) such a lower triangular (ordinary) convolution array, considered as a matrix, belongs to the Bell-subgroup of the Riordan-group.",
				"The (unsigned) column sequences are A000027, A000292, A000389, A000580, A000582, A001288 for m=0..5, resp. For m=6..23 they are A010966..(+2)..A011000 and for m=24..49 they are A017713..(+2)..A017763.",
				"Riordan array (1/(1+x)^2,x/(1+x)^2). Inverse array is A039598. Diagonal sums have g.f. 1/(1+x^2). - _Paul Barry_, Mar 17 2005. Corrected by _Wolfdieter_ Lang, Nov 13 2012.",
				"Unsigned version is in A078812. - _Philippe Deléham_, Nov 05 2006",
				"Also row n gives (except for an overall sign) coefficients of characteristic polynomial of the Cartan matrix for the root system A_n. - _Roger L. Bagula_, May 23 2007",
				"From _Wolfdieter Lang_, Nov 13 2012: (Start)",
				"The A-sequence for this Riordan triangle is A115141, and the Z-sequence is A115141(n+1), n\u003e=0. For A- and Z-sequences for Riordan matrices see the W. Lang link under A006232 with details and references.",
				"S(n,x^2-2) = sum(r(j,x^2),j=0..n) with Chebyshev's S-polynomials and r(j,x^2) := R(2*j+1,x)/x, where R(n,x) are the monic integer Chebyshv T-polynomials with coefficients given in A127672. Proof from comparing the o.g.f. of the partial sum of the r(j,x^2) polynomials (see a comment on the signed Riordan triangle A111125) with the present Riordan type o.g.f. for the row polynomials with x -\u003e x^2.  (End)",
				"S(n,x^2-2) = S(2*n+1,x)/x, n \u003e= 0, from the odd part of the bisection of the o.g.f. - _Wolfdieter Lang_, Dec 17 2012",
				"For a relation to a generator for the Narayana numbers A001263, see A119900, whose columns are unsigned shifted rows (or antidiagonals) of this array, referring to the tables in the example sections. - _Tom Copeland_, Oct 29 2014",
				"The unsigned rows of this array are alternating rows of a mirrored A011973 and alternating shifted rows of A030528 for the Fibonacci polynomials. - _Tom Copeland_, Nov 04 2014",
				"Boas-Buck type recurrence for column k \u003e= 0 (see Aug 10 2017 comment in A046521 with references): a(n, m) =  (2*(m + 1)/(n - m))*Sum_{k = m..n-1} (-1)^(n-k)*a(k, m), with input a(n, n) = 1, and a(n,k) = 0 for n \u003c k. - _Wolfdieter Lang_, Jun 03 2020"
			],
			"reference": [
				"M. Abramowitz and I. A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, 1964 (and various reprintings), p. 795.",
				"Theodore J. Rivlin, Chebyshev polynomials: from approximation theory to algebra and number theory, 2. ed., Wiley, New York, 1990.",
				"R. N. Cahn, Semi-Simple Lie Algebras and Their Representations, Dover, NY, 2006, ISBN 0-486-44999-8, p. 62.",
				"Sigurdur Helgasson, Differential Geometry, Lie Groups and Symmetric Spaces, Graduate Studies in Mathematics, volume 34. A. M. S.: ISBN 0-8218-2848-7, 1978, p. 463."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A053122/b053122.txt\"\u003eRows n=0..50 of triangle, flattened\u003c/a\u003e",
				"Wolfdieter Lang, \u003ca href=\"/A053122/a053122.pdf\"\u003eFirst rows of the triangle.\u003c/a\u003e",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"P. Barry, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL12/Barry4/barry64.html\"\u003eSymmetric Third-Order Recurring Sequences, Chebyshev Polynomials, and Riordan Arrays\u003c/a\u003e, JIS 12 (2009) 09.8.6.",
				"Naiomi T. Cameron and Asamoah Nkwanta, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL8/Cameron/cameron46.html\"\u003eOn some (pseudo) involutions in the Riordan group\u003c/a\u003e, J. of Integer Sequences, 8(2005), 1-16.",
				"P. Damianou, \u003ca href=\"http://arxiv.org/abs/1110.6620\"\u003eOn the characteristic polynomials of Cartan matrices and Chebyshev polynomials\u003c/a\u003e, arXiv preprint arXiv:1110.6620 [math.RT], 2014 (p. 10). - _Tom Copeland_, Oct 11 2014",
				"Pentti Haukkanen, Jorma Merikoski, Seppo Mustonen, \u003ca href=\"http://www.acta.sapientia.ro/acta-math/C6-2/math62-5.pdf\"\u003eSome polynomials associated with regular polygons\u003c/a\u003e, Acta Univ. Sapientiae, Mathematica, 6, 2 (2014) 178-193.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CartanMatrix.html\"\u003eCartan Matrix\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/DynkinDiagram.html\"\u003eDynkin Diagram\u003c/a\u003e",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e"
			],
			"formula": [
				"a(n, m) := 0 if n\u003cm else ((-1)^(n-m))*binomial(n+m+1, 2*m+1);",
				"a(n, m) = -2*a(n-1, m) + a(n-1, m-1) - a(n-2, m), a(n, -1) := 0 =: a(-1, m), a(0, 0)=1, a(n, m) := 0 if n\u003cm;",
				"O.g.f. for m-th column (signed triangle): ((x/(1+x)^2)^m)/(1+x)^2."
			],
			"example": [
				"The triangle a(n,m) begins:",
				"n\\m   0    1    2     3     4     5     6    7    8  9",
				"0:    1",
				"1:   -2    1",
				"2:    3   -4    1",
				"3:   -4   10   -6     1",
				"4:    5  -20   21    -8     1",
				"5:   -6   35  -56    36   -10     1",
				"6:    7  -56  126  -120    55   -12     1",
				"7:   -8   84 -252   330  -220    78   -14    1",
				"8:    9 -120  462  -792   715  -364   105  -16    1",
				"9:  -10  165 -792  1716 -2002  1365  -560  136  -18  1",
				"... Reformatted and extended by _Wolfdieter Lang_, Nov 13 2012",
				"E.g., fourth row (n=3) {-4,10,-6,1} corresponds to the polynomial S(3,x-2) = -4+10*x-6*x^2+x^3.",
				"From _Wolfdieter Lang_, Nov 13 2012: (Start)",
				"Recurrence: a(5,1) = 35 = 1*5 + (-2)*(-20) -1*(10).",
				"Recurrence from Z-sequence [-2,-1,-2,-5,...]: a(5,0) = -6 = (-2)*5 + (-1)*(-20) + (-2)*21 + (-5)*(-8) + (-14)*1.",
				"Recurrence from A-sequence [1,-2,-1,-2,-5,...]: a(5,1) = 35 = 1*5  + (-2)*(-20) + (-1)*21 + (-2)*(-8) + (-5)*1.",
				"(End)",
				"E.g., the fourth row (n=3) {-4,10,-6,1} corresponds also to the polynomial S(7,x)/x = -4 + 10*x^2 - 6*x^4 + x^6. - _Wolfdieter Lang_, Dec 17 2012",
				"Boas-Buck type recurrence: -56 = a(5, 2) = 2*(-1*1 + 1*(-6) - 1*21) = -2*28 = -56. - _Wolfdieter Lang_, Jun 03 2020"
			],
			"maple": [
				"seq(seq((-1)^(n+m)*binomial(n+m+1,2*m+1),m=0..n),n=0..10); # _Robert Israel_, Oct 15 2014"
			],
			"mathematica": [
				"T[n_, m_, d_] := If[ n == m, 2, If[n == m - 1 || n == m + 1, -1, 0]]; M[d_] := Table[T[n, m, d], {n, 1, d}, {m, 1, d}]; a = Join[M[1], Table[CoefficientList[Det[M[d] - x*IdentityMatrix[d]], x], {d, 1, 10}]]; Flatten[a] (* _Roger L. Bagula_, May 23 2007 *)",
				"(* Alternative code for the matrices from MathWorld: *)",
				"sln[n_] := 2IdentityMatrix[n] - PadLeft[PadRight[IdentityMatrix[n - 1], {n, n - 1}], {n, n}] - PadLeft[PadRight[IdentityMatrix[n - 1], {n - 1, n}], {n, n}] (* _Roger L. Bagula_, May 23 2007 *)"
			],
			"program": [
				"(Sage)",
				"@CachedFunction",
				"def A053122(n,k):",
				"    if n\u003c 0: return 0",
				"    if n==0: return 1 if k == 0 else 0",
				"    return A053122(n-1,k-1)-A053122(n-2,k)-2*A053122(n-1,k)",
				"for n in (0..9): [A053122(n,k) for k in (0..n)] # _Peter Luschny_, Nov 20 2012"
			],
			"xref": [
				"Cf. A005248, A127677, A053123, A049310.",
				"Cf. A011973, A030528, A034867, A119900.",
				"Cf. A285072 (version with row-leading 0's and differing signs). - _Eric W. Weisstein_, Apr 09 2017"
			],
			"keyword": "easy,nice,sign,tabl",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_",
			"references": 36,
			"revision": 91,
			"time": "2020-06-04T03:31:05-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}