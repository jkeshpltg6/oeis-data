{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A113428",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 113428,
			"data": "1,0,-1,-1,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,-1,0,0,-1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,-1,0,0,0,0,-1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0",
			"name": "Expansion of f(-x^2, -x^3) in powers of x where f(, ) is Ramanujan's general theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"See the Hardy-Wright reference for the identity given as g.f. formula below. - _Wolfdieter Lang_, Oct 28 2016"
			],
			"reference": [
				"G. H. Hardy, Ramanujan, AMS Chelsea Publ., Providence, RI, 2002, p. 93.",
				"G. H. Hardy and E. M. Wright, An Introduction to the Theory of Numbers, Fifth ed., Clarendon Press, Oxford, 2003, Theorem 356, p. 284."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A113428/b113428.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"https://mathworld.wolfram.com/Rogers-Ramanujan Identities.html\"\u003eRogers-Ramanujan Identities\u003c/a\u003e"
			],
			"formula": [
				"Expansion of G(x) * f(-x) in powers of x where G() is the g.f. of A003114.",
				"Euler transform of period 5 sequence [ 0, -1, -1, 0, -1, ...].",
				"|a(n)| is the characteristic function of the numbers in A057569.",
				"The exponents in the q-series q * A(q^40) are the square of the numbers in A090771.",
				"G.f.: Sum_{k in Z} (-1)^k * x^((5*k^2 + k)/2) = Prod_{k\u003e0} (1 - x^(5*k)) * (1 - x^(5*k - 2)) * (1 - x^(5*k - 3)).",
				"Convolution of A003114 and A010815.",
				"From _Wolfdieter Lang_, Oct 30 2016: (Start)",
				"a(n) = (-1)^k if n = b(2*k+1) for k \u003e= 0, a(n) = (-1)^k if n = b(2*k), for k \u003e= 1, and a(n) = 0 otherwise, where b(n) = A057569(n). See the third formula.",
				"G.f.: Sum_{n\u003e=0} (-1)^n*x^(n*(5*n+1)/2)*(1-x^(2*(2*n+1))). See the Hardy reference, p. 93, eq. (6.11.1) with k=2, a=x and C_n = 1.",
				"(End)",
				"G.f.: Sum_{n \u003e= 0} x^(n^2)*Product_{k \u003e= n+1} 1 - x^k. Cf. A113429. - _Peter Bala_, Feb 12 2021"
			],
			"example": [
				"G.f. = 1 - x^2 - x^3 + x^9 + x^11 - x^21 - x^24 + x^38 + x^42 - x^60 - x^65 + ...",
				"G.f. = q - q^81 - q^121 + q^361 + q^441 - q^841 - q^961 + q^1521 + q^1681 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ x^2, x^5] QPochhammer[ x^3, x^5] QPochhammer[ x^5], {x, 0, n}]; (* _Michael Somos_, Jan 06 2016 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, 0, polcoeff( prod(k=1, n, 1 - x^k * [1, 0, 1, 1, 0][k%5 + 1], 1 + x * O(x^n)), n))};"
			],
			"xref": [
				"Cf. A003114, A057569, A090771, A010815, A113429."
			],
			"keyword": "sign,easy",
			"offset": "0,1",
			"author": "_Michael Somos_, Oct 31 2005",
			"references": 7,
			"revision": 35,
			"time": "2021-02-18T05:42:45-05:00",
			"created": "2006-01-24T03:00:00-05:00"
		}
	]
}