{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A109466",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 109466,
			"data": "1,0,1,0,-1,1,0,0,-2,1,0,0,1,-3,1,0,0,0,3,-4,1,0,0,0,-1,6,-5,1,0,0,0,0,-4,10,-6,1,0,0,0,0,1,-10,15,-7,1,0,0,0,0,0,5,-20,21,-8,1,0,0,0,0,0,-1,15,-35,28,-9,1,0,0,0,0,0,0,-6,35,-56,36,-10,1,0,0,0,0,0,0,1,-21,70,-84,45,-11,1,0,0,0,0",
			"name": "Riordan array (1, x(1-x)).",
			"comment": [
				"Inverse is Riordan array (1, xc(x)) (A106566).",
				"Triangle T(n,k), 0 \u003c= k \u003c= n, read by rows, given by [0, -1, 1, 0, 0, 0, 0, 0, 0, ...] DELTA [1, 0, 0, 0, 0, 0, 0, 0, ...] where DELTA is the operator defined in A084938.",
				"Modulo 2, this sequence gives A106344. - _Philippe Deléham_, Dec 18 2008",
				"Coefficient array of the polynomials Chebyshev_U(n, sqrt(x)/2)*(sqrt(x))^n. - _Paul Barry_, Sep 28 2009"
			],
			"link": [
				"P. Barry, \u003ca href=\"http://arxiv.org/abs/1312.0583\"\u003eEmbedding structures associated with Riordan arrays and moment matrices\u003c/a\u003e, arXiv preprint arXiv:1312.0583 [math.CO], 2013.",
				"T. Copeland, \u003ca href=\"http://tcjpn.wordpress.com/2015/10/12/the-elliptic-lie-triad-kdv-and-ricattt-equations-infinigens-and-elliptic-genera/\"\u003eAddendum to Elliptic Lie Triad\u003c/a\u003e"
			],
			"formula": [
				"Number triangle T(n, k) = (-1)^(n-k)*binomial(k, n-k).",
				"T(n, k)*2^(n-k) = A110509(n, k); T(n, k)*3^(n-k) = A110517(n, k).",
				"Sum_{k=0..n} T(n,k)*A000108(k)=1. - _Philippe Deléham_, Jun 11 2007",
				"Sum_{k=0..n} T(n,k)*A144706(k) = A082505(n+1). - _Philippe Deléham_, Oct 30 2008",
				"Sum_{k=0..n} T(n,k)*A002450(k) = A100335(n). - _Philippe Deléham_, Oct 30 2008",
				"Sum_{k=0..n} T(n,k)*A001906(k) = A100334(n). - _Philippe Deléham_, Oct 30 2008",
				"Sum_{k=0..n} T(n,k)*A015565(k) = A099322(n). - _Philippe Deléham_, Oct 30 2008",
				"Sum_{k=0..n} T(n,k)*A003462(k) = A106233(n). - _Philippe Deléham_, Oct 30 2008",
				"Sum_{k=0..n} T(n,k)*x^(n-k) = A053404(n), A015447(n), A015446(n), A015445(n), A015443(n), A015442(n), A015441(n), A015440(n), A006131(n), A006130(n), A001045(n+1), A000045(n+1), A000012(n), A010892(n), A107920(n+1), A106852(n), A106853(n), A106854(n), A145934(n), A145976(n), A145978(n), A146078(n), A146080(n), A146083(n), A146084(n) for x = -12,-11,-10,-9,-8,-7,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8,9,10,11,12 respectively. - _Philippe Deléham_, Oct 27 2008",
				"Sum_{k=0..n} T(n,k)*x^k = A000007(n), A010892(n), A099087(n), A057083(n), A001787(n+1), A030191(n), A030192(n), A030240(n), A057084(n), A057085(n+1), A057086(n) for x = 0,1,2,3,4,5,6,7,8,9,10 respectively. - _Philippe Deléham_, Oct 28 2008",
				"G.f.: 1/(1-y*x+y*x^2). - _Philippe Deléham_, Dec 15 2011",
				"T(n,k) = T(n-1,k-1) - T(n-2,k-1), T(n,0) = 0^n. - _Philippe Deléham_, Feb 15 2012",
				"Sum_{k=0..n} T(n,k)*x^(n-k) = F(n+1,-x) where F(n,x)is the n-th Fibonacci polynomial in x defined in A011973. - _Philippe Deléham_, Feb 22 2013",
				"Sum_{k=0..n} T(n,k)^2 = A051286(n). - _Philippe Deléham_, Feb 26 2013",
				"Sum_{k=0..n} T(n,k)*T(n+1,k) = -A110320(n). - _Philippe Deléham_, Feb 26 2013",
				"For T(0,0) = 0, the signed triangle below has the o.g.f. G(x,t) = [t*x(1-x)]/[1-t*x(1-x)] = L[t*Cinv(x)] where L(x) = x/(1-x) and Cinv(x)=x(1-x) with the inverses Linv(x) = x/(1+x) and C(x)= [1-sqrt(1-4*x)]/2, an o.g.f. for the shifted Catalan numbers A000108, so the inverse o.g.f. is Ginv(x,t) = C[Linv(x)/t] = [1-sqrt[1-4*x/(t(1+x))]]/2 (cf. A124644 and A030528). - _Tom Copeland_, Jan 19 2016"
			],
			"example": [
				"Rows begin:",
				"  1;",
				"  0,  1;",
				"  0, -1,  1;",
				"  0,  0, -2,  1;",
				"  0,  0,  1, -3,  1;",
				"  0,  0,  0,  3, -4,   1;",
				"  0,  0,  0, -1,  6,  -5,   1;",
				"  0,  0,  0,  0, -4,  10,  -6,   1;",
				"  0,  0,  0,  0,  1, -10,  15,  -7,  1;",
				"  0,  0,  0,  0,  0,   5, -20,  21, -8,  1;",
				"  0,  0,  0,  0,  0,  -1,  15, -35, 28, -9, 1;",
				"From _Paul Barry_, Sep 28 2009: (Start)",
				"Production array is",
				"  0,    1,",
				"  0,   -1,    1,",
				"  0,   -1,   -1,   1,",
				"  0,   -2,   -1,  -1,   1,",
				"  0,   -5,   -2,  -1,  -1,  1,",
				"  0,  -14,   -5,  -2,  -1, -1,  1,",
				"  0,  -42,  -14,  -5,  -2, -1, -1,  1,",
				"  0, -132,  -42, -14,  -5, -2, -1, -1,  1,",
				"  0, -429, -132, -42, -14, -5, -2, -1, -1, 1 (End)"
			],
			"mathematica": [
				"(* The function RiordanArray is defined in A256893. *)",
				"RiordanArray[1\u0026, #(1-#)\u0026, 13] // Flatten (* _Jean-François Alcover_, Jul 16 2019 *)"
			],
			"program": [
				"(MAGMA) /* As triangle */ [[(-1)^(n-k)*Binomial(k, n-k): k in [0..n]]: n in [0.. 15]]; // _Vincenzo Librandi_, Jan 14 2016"
			],
			"xref": [
				"Cf. A026729 (unsigned version), A000108, A030528, A124644."
			],
			"keyword": "easy,sign,tabl",
			"offset": "0,9",
			"author": "_Philippe Deléham_, Aug 28 2005",
			"references": 53,
			"revision": 57,
			"time": "2020-01-20T17:19:33-05:00",
			"created": "2005-09-21T03:00:00-04:00"
		}
	]
}