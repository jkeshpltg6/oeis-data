{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A330776",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 330776,
			"data": "1,1,1,2,6,4,6,37,63,32,20,262,870,1064,436,90,2217,12633,27824,26330,9012,468,21882,201654,710712,1163320,895608,262760,2910,249852,3578610,18924846,47608000,61786254,40042128,10270696,20644,3245520,70539124,538018360,1950556400,3792461176,4070160416,2275829088,518277560",
			"name": "Triangle read by rows: T(n,k) is the number of balanced reduced multisystems of weight n with atoms colored using exactly k colors.",
			"comment": [
				"See A330655 for the definition of a balanced reduced multisystem.",
				"A balanced reduced multisystem of weight n with atoms of k colors corresponds with a rooted tree with n leaves of k colors with all leaves at the same depth and at least one node at each level of the tree having more than one child. The final condition is needed to ensure that the number of such trees is finite."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A330776/b330776.txt\"\u003eTable of n, a(n) for n = 1..1275\u003c/a\u003e (first 50 rows)"
			],
			"example": [
				"Triangle begins:",
				"    1;",
				"    1,     1;",
				"    2,     6,      4;",
				"    6,    37,     63,     32;",
				"   20,   262,    870,   1064,     436;",
				"   90,  2217,  12633,  27824,   26330,   9012;",
				"  468, 21882, 201654, 710712, 1163320, 895608, 262760;",
				"  ...",
				"The T(3,2) = 6 balanced reduced multisystems are: {1,1,2}, {1,2,2}, {{1},{1,2}}, {{1},{2,2}}, {{2},{1,1}}, {{2},{1,2}}."
			],
			"program": [
				"(PARI)",
				"EulerT(v)={Vec(exp(x*Ser(dirmul(v, vector(#v, n, 1/n))))-1, -#v)}",
				"R(n,k)={my(v=vector(n), u=vector(n)); v[1]=k; for(n=1, #v, u += v*sum(j=n, #v, (-1)^(j-n)*binomial(j-1,n-1)); v=EulerT(v)); u}",
				"M(n)={my(v=vector(n, k, R(n, k)~)); Mat(vector(n, k, sum(i=1, k, (-1)^(k-i)*binomial(k, i)*v[i])))}",
				"{my(T=M(10)); for(n=1, #T~, print(T[n, 1..n]))}"
			],
			"xref": [
				"Column 1 is A318813.",
				"Main diagonal is A005121.",
				"Row sums are A330655."
			],
			"keyword": "nonn,tabl",
			"offset": "1,4",
			"author": "_Andrew Howroyd_, Dec 30 2019",
			"references": 3,
			"revision": 9,
			"time": "2020-01-09T19:41:27-05:00",
			"created": "2019-12-31T06:50:23-05:00"
		}
	]
}