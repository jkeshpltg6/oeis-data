{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A173200",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 173200,
			"data": "0,11,70,225,524,1015,1746,2765,4120,5859,8030,10681,13860,17615,21994,27045,32816,39355,46710,54929,64060,74151,85250,97405,110664,125075,140686,157545,175700,195199,216090,238421,262240,287595,314534,343105",
			"name": "Solutions y of the Mordell equation y^2 = x^3 - 3a^2 - 1 for a = 0,1,2, ... (solutions x are given by A053755).",
			"comment": [
				"For many values of k for the equation y^2 = x^3 + k, all the solutions are known. For example, we have solutions for k=-2: (x,y) = (3,-5) and (3,5). A complete resolution for all integers k is unknown. Theorem: Let k be \u003c -1, free of square factors, with k == 2 or 3 (mod 4). Suppose that the number of classes h(Q(sqrt(k))) is not divisible by 3. Then the equation y^2 = x^3 + k admits integer solutions if and only if k = 1 - 3a^2 or 1 - 3a^2 where a is integer. In this case, the solutions are x = a^2 - k, y = a(a^2 + 3k) or -a(a^2 + 3k) (the first reference gives the proof of this theorem). With k = -1 - 3a^2, we obtain the solutions x = 4a^2 + 1, y = a(8a^2 + 3) or -a(8a^2 + 3). For the case k = 1 - 3a^2, we obtain the solution x = 4a^2 - 1 given by the sequence A000466."
			],
			"reference": [
				"T. Apostol, Introduction to Analytic Number Theory, Springer, 1976",
				"D. Duverney, Théorie des nombres (2e edition), Dunod, 2007, p.151"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A173200/b173200.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"W. J. Ellison, F. Ellison, J. Pesek, C. E. Stall \u0026 D. S. Stall, \u003ca href=\"https://doi.org/10.1016/0022-314X(72)90058-3\"\u003eThe diophantine equation y^2 + k = x^3\u003c/a\u003e, J. Number Theory 4 (1972), 107-117.",
				"John J. O'Connor and Edmund F. Robertson, \u003ca href=\"http://www-history.mcs.st-and.ac.uk/Biographies/Mordell.html\"\u003eLouis Joel Mordell\u003c/a\u003e",
				"Helmut Richter, \u003ca href=\"http://hr.userweb.mwn.de/numb/mordell.html\"\u003eSolutions of Mordell's equation y^2 = x^3 + k\u003c/a\u003e (solutions for 0\u003ck\u003c1008)",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/MordellCurve.html\"\u003eMordell Curve\u003c/a\u003e",
				"David J. Wright, \u003ca href=\"http://cauchy.math.okstate.edu/~wrightd/4713/nt_essay/node5.html\"\u003eMordell's Equation\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-6,4,-1)."
			],
			"formula": [
				"y = a*(8*a^2 + 3).",
				"From  _Colin Barker_, Apr 26 2012: (Start)",
				"a(n) = 8*n^3 - 24*n^2 + 27*n - 11.",
				"G.f.: x^2*(11 + 26*x + 11*x^2)/(1 - x)^4. (End)",
				"a(n) = 4*a(n-1) - 6*a(n-2) + 4*a(n-3) - a(n-4). - _Vincenzo Librandi_, Jul 02 2012"
			],
			"example": [
				"With a=3, x =37 and y = 225, and then 225^2 = 37^2 - 28."
			],
			"maple": [
				"for a from 0 to 150 do : z := evalf(a*(8*a^2 + 3)) : print (z) :od :"
			],
			"mathematica": [
				"CoefficientList[Series[x*(11+26*x+11*x^2)/(1-x)^4,{x,0,40}],x] (* _Vincenzo Librandi_, Jul 02 2012 *)",
				"LinearRecurrence[{4,-6,4,-1},{0,11,70,225},40] (* _Harvey P. Dale_, Dec 21 2016 *)"
			],
			"program": [
				"(MAGMA)  I:=[0, 11, 70, 225]; [n le 4 select I[n] else 4*Self(n-1)-6*Self(n-2)+4*Self(n-3)-Self(n-4): n in [1..40]]; // _Vincenzo Librandi_, Jul 02 2012",
				"(Python) for n in range(1,20): print(8*n**3 - 24*n**2 + 27*n - 11, end=', ') # _Stefano Spezia_, Dec 05 2018"
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Michel Lagneau_, Feb 12 2010",
			"references": 2,
			"revision": 33,
			"time": "2018-12-05T12:11:20-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}