{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A285722",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 285722,
			"data": "0,1,1,2,0,3,4,3,2,6,7,5,0,5,10,11,8,6,4,9,15,16,12,9,0,8,14,21,22,17,13,10,7,13,20,28,29,23,18,14,0,12,19,27,36,37,30,24,19,15,11,18,26,35,45,46,38,31,25,20,0,17,25,34,44,55,56,47,39,32,26,21,16,24,33,43,54,66,67,57,48,40,33,27,0,23,32,42,53,65,78,79,68,58,49,41,34,28,22,31,41,52,64,77,91",
			"name": "Square array A(n,k) read by antidiagonals, A(n,n) = 0, otherwise, if n \u003e k, A(n,k) = T(n-k,k), else A(n,k) = T(n,k-n), where T(n,k) is sequence A000027 considered as a two-dimensional table.",
			"comment": [
				"The array is read by descending antidiagonals as A(1,1), A(1,2), A(2,1), A(1,3), A(2,2), A(3,1), etc."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A285722/b285722.txt\"\u003eTable of n, a(n) for n = 1..10585\u003c/a\u003e",
				"MathWorld, \u003ca href=\"http://mathworld.wolfram.com/PairingFunction.html\"\u003ePairing Function\u003c/a\u003e"
			],
			"formula": [
				"If n = k, A(n,k) = 0, if n \u003e k, A(n,k) = T(n-k,k), otherwise [when n \u003c k], A(n,k) = T(n,k-n), where T(n,k) is sequence A000027 considered as a two-dimensional table, that is, as a pairing function from N X N to N."
			],
			"example": [
				"The top left 14 X 14 corner of the array:",
				"   0,  1,  2,  4,  7, 11, 16, 22, 29, 37, 46, 56, 67, 79",
				"   1,  0,  3,  5,  8, 12, 17, 23, 30, 38, 47, 57, 68, 80",
				"   3,  2,  0,  6,  9, 13, 18, 24, 31, 39, 48, 58, 69, 81",
				"   6,  5,  4,  0, 10, 14, 19, 25, 32, 40, 49, 59, 70, 82",
				"  10,  9,  8,  7,  0, 15, 20, 26, 33, 41, 50, 60, 71, 83",
				"  15, 14, 13, 12, 11,  0, 21, 27, 34, 42, 51, 61, 72, 84",
				"  21, 20, 19, 18, 17, 16,  0, 28, 35, 43, 52, 62, 73, 85",
				"  28, 27, 26, 25, 24, 23, 22,  0, 36, 44, 53, 63, 74, 86",
				"  36, 35, 34, 33, 32, 31, 30, 29,  0, 45, 54, 64, 75, 87",
				"  45, 44, 43, 42, 41, 40, 39, 38, 37,  0, 55, 65, 76, 88",
				"  55, 54, 53, 52, 51, 50, 49, 48, 47, 46,  0, 66, 77, 89",
				"  66, 65, 64, 63, 62, 61, 60, 59, 58, 57, 56,  0, 78, 90",
				"  78, 77, 76, 75, 74, 73, 72, 71, 70, 69, 68, 67,  0, 91",
				"  91, 90, 89, 88, 87, 86, 85, 84, 83, 82, 81, 80, 79,  0"
			],
			"mathematica": [
				"A[n_, n_] = 0;",
				"A[n_, k_] /; k == n-1 := (k^2 - k + 2)/2;",
				"A[1, k_] := (k^2 - 3k + 4)/2;",
				"A[n_, k_] /; 1 \u003c= k \u003c= n-2 := A[n, k] = A[n, k+1] + 1;",
				"A[n_, k_] /; k \u003e n := A[n, k] = A[n-1, k] + 1;",
				"Table[A[n-k+1, k], {n, 1, 14}, {k, n, 1, -1}] // Flatten (* _Jean-François Alcover_, Nov 19 2019 *)"
			],
			"program": [
				"(Scheme)",
				"(define (A285722 n) (A285722bi (A002260 n) (A004736 n)))",
				"(define (A285722bi row col) (cond ((= row col) 0) ((\u003e row col) (A000027bi (- row col) col)) (else (A000027bi row (- col row)))))",
				"(define (A000027bi row col) (* (/ 1 2) (+ (expt (+ row col) 2) (- row) (- (* 3 col)) 2)))",
				"(Python)",
				"def T(n, m): return ((n + m)**2 - n - 3*m + 2)//2",
				"def A(n, k): return 0 if n == k else T(n - k, k) if n\u003ek else T(n, k - n)",
				"for n in range(1, 21): print([A(k, n - k + 1) for k in range(1, n + 1)]) # _Indranil Ghosh_, May 03 2017"
			],
			"xref": [
				"Transpose: A285723.",
				"Cf. A000124 (row 1, from 1 onward), A000217 (column 1).",
				"Cf. also A000027, A003989, A072030, A285721, A285732."
			],
			"keyword": "nonn,tabl",
			"offset": "1,4",
			"author": "_Antti Karttunen_, May 03 2017",
			"references": 9,
			"revision": 24,
			"time": "2021-03-28T19:34:55-04:00",
			"created": "2017-05-03T08:40:24-04:00"
		}
	]
}