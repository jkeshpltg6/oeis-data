{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A333337",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 333337,
			"data": "0,1,2,4,6,8,9,10,12,15,16,18,20,21,25,27,24,28,33,35,30,39,44,45,49,51,55,63,57,65,69,75,77,81,85,60,76,87,91,95,99,105,111,115,117,119,121,123,125,135,143,145,147,153,155,161,169,159,165,171,175,177",
			"name": "Indices of rows of n consecutive smallest primes in A333238, or -1 if n consecutive smallest primes do not appear in A333238.",
			"comment": [
				"Consider the irregular table where row m lists the distinct smallest primes p of prime partitions of m. Row n of this sequence contains all m that have n consecutive primes starting with 2.",
				"Alternatively, positions of k-repunits in A333259.",
				"A330507(n) = First terms in row n.",
				"Null rows occur at n = {90, 151, 349, 352, 444, ...} and are thus filled with the term -1."
			],
			"example": [
				"Table begins:",
				"       0:    0     1",
				"       1:    2     4",
				"       2:    6     8     9",
				"       3:   10    12    15    16",
				"       4:   18    20    21    25    27",
				"       5:   24    28    33    35",
				"       6:   30    39    44    45    49",
				"       7:   51    55    63",
				"       8:   57    65",
				"       9:   60    76    87    91    95",
				"      10:   69    75    77    81    85",
				"      11:   99   105",
				"      12:  111   115   117   119   121",
				"      13:  123   125   135",
				"      14:  143   145",
				"      15:  147   153   155   161   169",
				"      16:  159   165   171   175",
				"      17:  177   185   187",
				"Consider the table plotting prime p in row m of A333238 at pi(p) place; intervening primes missing from row m are shown by \".\" as a place holder:",
				"    m      Primes in row m of A333238",
				"    ---------------------------------",
				"    2:     2",
				"    3:     .   3",
				"    4:     2",
				"    5:     2   .   5",
				"    6:     2   3",
				"    7:     2   .   .   7",
				"    8:     2   3",
				"    9:     2   3",
				"    10:    2   3   5",
				"    11:    2   3   .   .  11",
				"    12:    2   3   5",
				"    13:    2   3   .   .   .  13",
				"    14:    2   3   .   7",
				"    15:    2   3   5",
				"    16:    2   3   5",
				"    17:    2   3   5   .   .   .  17",
				"    ...",
				"There are no primes in rows 0 or 1 of A333238, thus row 0 of this sequence contains {0, 1}.",
				"The smallest prime, 2, appears alone in rows 2 and 4 of A333238, thus row 1 of this sequence contains {2, 4}.",
				"We have the primes {2, 3} and no other primes in rows {6, 8, 9} in A333238, thus row 2 of this sequence contains {6, 8, 9}.",
				"We have the primes {2, 3, 5} and no other primes in rows {10, 12, 15, 16} in A333238, thus row 3 of this sequence contains {10, 12, 15, 16}, etc."
			],
			"mathematica": [
				"Block[{m = 120, s, a}, a = ConstantArray[{}, m]; s = {Prime@ PrimePi@ m}; Do[If[# \u003c= m, If[FreeQ[a[[#]], Last@ s], a = ReplacePart[a, # -\u003e Union@ Append[a[[#]], Last@ s]], Nothing]; AppendTo[s, Last@ s], If[Last@ s == 2, s = DeleteCases[s, 2]; If[Length@s == 0, Break[], s = MapAt[Prime[PrimePi[#] - 1] \u0026, s, -1]], s = MapAt[Prime[PrimePi[#] - 1] \u0026, s, -1]]] \u0026@ Total[s], {i, Infinity}]; s = {0}~Join~Map[Which[Length@ # == 0, 0, And[Length@ # == 1, First@ # == 2], 1, True, If[Union@ # == {1}, Length@ # + 1, -1] \u0026[Differences@ PrimePi@ #, {} -\u003e {2}]] \u0026, a]; Array[-1 + Position[s, #][[All, 1]] /. k_ /; MissingQ@ k -\u003e {-1} \u0026, Max@ s + 1, 0]]"
			],
			"xref": [
				"Cf. A330507, A333238, A333259."
			],
			"keyword": "tabf,sign",
			"offset": "0,3",
			"author": "_Michael De Vlieger_, _David James Sycamore_, May 25 2020",
			"references": 0,
			"revision": 10,
			"time": "2021-08-29T01:56:33-04:00",
			"created": "2020-06-24T07:20:28-04:00"
		}
	]
}