{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A064429",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 64429,
			"data": "0,2,1,3,5,4,6,8,7,9,11,10,12,14,13,15,17,16,18,20,19,21,23,22,24,26,25,27,29,28,30,32,31,33,35,34,36,38,37,39,41,40,42,44,43,45,47,46,48,50,49,51,53,52,54,56,55,57,59,58,60,62,61,63,65,64,66,68,67,69,71,70",
			"name": "a(n) = floor(n / 3) * 3 + sign(n mod 3) * (3 - n mod 3).",
			"comment": [
				"a(a(n)) = n (a self-inverse permutation).",
				"Take natural numbers, exchange trisections starting with 1 and 2.",
				"Lodumo_3 of A080425. - _Philippe Deléham_, Apr 26 2009",
				"From _Franck Maminirina Ramaharo_, Jul 27 2018: (Start)",
				"The sequence is A008585 interleaved with A016789 and A016777.",
				"a(n) is also obtained as follows: write n in base 3; if the rightmost digit is '1', then replace it with '2' and vice versa; convert back to decimal. For example a(14) = a('11'2') = '11'1' = 13 and a(10) = a('10'1') = '10'2' = 11.",
				"(End)",
				"A permutation of the nonnegative integers partitioned into triples [3*k-3, 3*k-1, 3*k-2] for k \u003e 0. - _Guenther Schrack_, Feb 05 2020"
			],
			"link": [
				"Muniru A Asiru, \u003ca href=\"/A064429/b064429.txt\"\u003eTable of n, a(n) for n = 0..3000\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/AlternatingPermutation.html\"\u003eAlternating Permutations\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,0,1,-1)."
			],
			"formula": [
				"a(n) = A080782(n+1) - 1.",
				"a(n) = n - 2*sin(4*Pi*n/3)/sqrt(3). - _Jaume Oliver Lafont_, Dec 05 2008",
				"a(n) = A001477(n) + A102283(n). - _Jaume Oliver Lafont_, Dec 05 2008",
				"a(n) = lod_3(A080425(n)). - _Philippe Deléham_, Apr 26 2009",
				"G.f.: x*(2 - x + 2*x^2)/((1 + x + x^2)*(1 - x)^2 ). - _R. J. Mathar_, Feb 20 2011",
				"a(n) = 2*n - 3 - 3*floor((n-2)/3). - _Wesley Ivan Hurt_, Nov 30 2013",
				"a(n) = a(n-1) + a(n-3) - a(n-4) for n \u003e 3. - _Wesley Ivan Hurt_, Oct 06 2017",
				"E.g.f.: x*exp(x) + (2*sin((sqrt(3)*x)/2))/(exp(x/2)*sqrt(3)). - _Franck Maminirina Ramaharo_, Jul 27 2018",
				"From _Guenther Schrack_, Feb 05 2020: (Start)",
				"a(n) = a(n-3) + 3 with a(0)=0, a(1)=2, a(2)=1 for n \u003e 2;",
				"a(n) = n + (w^(2*n) - w^n)*(1 + 2*w)/3 where w = (-1 + sqrt(-3))/2. (End)"
			],
			"example": [
				"From _Franck Maminirina Ramaharo_, Jul 27 2018: (Start)",
				"Interleave 3 sequences:",
				"A008585: 0.....3.....6.....9.......12.......15........",
				"A016789: ..2.....5.....8.....11.......14.......17.....",
				"A016777: ....1.....4.....7......10.......13.......16..",
				"(End)"
			],
			"maple": [
				"A064429:=n-\u003e2*n-3-3*floor((n-2)/3): seq(A064429(n), n=0..100); # _Wesley Ivan Hurt_, Nov 30 2013"
			],
			"mathematica": [
				"Table[2 n - 3 - 3 Floor[(n - 2)/3], {n, 0, 100}] (* _Wesley Ivan Hurt_, Nov 30 2013 *)",
				"{#+1,#-1,#}[[Mod[#,3,1]]]\u0026/@Range[0, 100] (* _Federico Provvedi_, May 11 2021 *)"
			],
			"program": [
				"(PARI) a(n) = 2*n-3-3*((n-2)\\3); \\\\ _Altug Alkan_, Oct 06 2017",
				"(GAP) a:=[0,2,1,3];; for n in [5..100] do a[n]:=a[n-1]+a[n-3]-a[n-4]; od; a; # _Muniru A Asiru_, Jul 27 2018",
				"(MAGMA) [2*n - 3 - 3*((n-2) div 3): n in [0..80]]; // _Vincenzo Librandi_, Aug 05 2018"
			],
			"xref": [
				"Cf. A001477, A004442, A074066, A080425, A080782, A102283, A143097."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Reinhard Zumkeller_, Oct 15 2001",
			"references": 10,
			"revision": 72,
			"time": "2021-06-09T05:35:46-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}