{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A330694",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 330694,
			"data": "1,2,1,3,3,1,8,4,2,1,15,15,0,0,1,24,12,18,3,5,1,49,49,0,14,14,0,1,128,64,32,16,8,4,2,1,189,189,63,63,0,0,3,3,1,480,240,240,0,30,15,15,0,2,1,1023,1023,0,0,0,0,0,0,0,0,1,1536,768,768,384,384,96,96,24,26,7,5,1,4095,4095,0,0,0,0,0,0,0,0,0,0,1",
			"name": "Triangular array read by rows. T(n,k) is the number of k-normal elements in GF(2^n), n \u003e= 1, 0 \u003c= k \u003c= n-1.",
			"comment": [
				"Let g(x) = Sum_{i=0..n-1} g_i x^i in GF(q)[x]. Define an action on the algebraic closure of GF(q) by g*a = Sum_{i=0..n-1} g_i a^(q^i). The annihilator of a is an ideal and is generated by a polynomial g of minimum degree. If deg(g) = n-k then a is k-normal.",
				"An element a in GF(q^n) is 0-normal if {a, a^q, a^(q^2), ..., a^(q^(n-1))} is a basis for GF(q^n) over GF(q)."
			],
			"link": [
				"Zülfükar Saygi, Ernist Tilenbaev, Çetin Ürtiş, \u003ca href=\"https://doi.org/10.3906/mat-1805-113\"\u003eOn the number of k-normal elements over finite fields\u003c/a\u003e, Turk J Math., (2019) 43.795.812.",
				"David Thompson, \u003ca href=\"https://pdfs.semanticscholar.org/d0b2/ce1a4b89d3198cc0444e54c238d459a16a22.pdf\"\u003eSomething about normal bases over finite fields\u003c/a\u003e, Existence and properties of k-normal elements over finite fields, Slides, (2013)."
			],
			"formula": [
				"T(n, k) = Sum_{h | x^n-1, deg(h) = n-k} phi_2(h) where phi_2(h) is the generalized Euler phi function and the polynomial division is in GF(2)[x]."
			],
			"example": [
				"Triangle begins",
				"     1;",
				"     2,    1;",
				"     3,    3,   1;",
				"     8,    4,   2,   1;",
				"    15,   15,   0,   0,   1;",
				"    24,   12,  18,   3,   5,  1;",
				"    49,   49,   0,  14,  14,  0,  1;",
				"   128,   64,  32,  16,   8,  4,  2,  1;",
				"   189,  189,  63,  63,   0,  0,  3,  3,  1;",
				"   480,  240, 240,   0,  30, 15, 15,  0,  2, 1;",
				"  1023, 1023,   0,   0,   0,  0,  0,  0,  0, 0, 1;",
				"  1536,  768, 768, 384, 384, 96, 96, 24, 26, 7, 5, 1;",
				"  4095, 4095,   0,   0,   0,  0,  0,  0,  0, 0, 0, 0, 1;"
			],
			"mathematica": [
				"Needs[\"FiniteFields`\"];Table[b = Map[GF[2^n][#] \u0026, Tuples[{0, 1}, n]];",
				"  Table[Count[Table[MatrixRank[Table[b[[i]]^(2^k), {k, 0, n - 1}][[All, 1]],",
				"       Modulus -\u003e 2], {i, 2, 2^n}], k], {k, 1, n}] // Reverse, {n, 1, 8}] // Grid"
			],
			"xref": [
				"Column k=0 gives A003473."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Geoffrey Critzer_, Dec 25 2019",
			"references": 0,
			"revision": 31,
			"time": "2019-12-26T17:21:50-05:00",
			"created": "2019-12-26T17:21:50-05:00"
		}
	]
}