{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A303864",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 303864,
			"data": "1,1,1,1,1,1,1,1,1,1,1,3,6,2,1,1,4,36,38,3,1,1,10,210,960,384,6,1,1,16,1176,18680,35956,4425,14,1,1,36,6328,313664,2280910,1588192,57976,34,1,1,64,32896,4683168,111925464,323840016,77381016,807318,95,1",
			"name": "Array read by antidiagonals: T(n,k) = number of noncrossing path sets on k*n nodes up to rotation with each path having exactly k nodes.",
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A303864/b303864.txt\"\u003eTable of n, a(n) for n = 0..1274\u003c/a\u003e"
			],
			"example": [
				"Array begins:",
				"=======================================================",
				"n\\k| 1  2     3        4           5              6",
				"---+---------------------------------------------------",
				"0  | 1  1     1        1           1              1 ...",
				"1  | 1  1     1        3           4             10 ...",
				"2  | 1  1     6       36         210           1176 ...",
				"3  | 1  2    38      960       18680         313664 ...",
				"4  | 1  3   384    35956     2280910      111925464 ...",
				"5  | 1  6  4425  1588192   323840016    46552781760 ...",
				"6  | 1 14 57976 77381016 50668922540 21346459738384 ...",
				"..."
			],
			"mathematica": [
				"nmax = 10; seq[n_, k_] := Module[{p, q, h}, p = 1 + InverseSeries[ x/(k*2^If[k == 1, 0, k - 3]*(1 + x)^k) + O[x]^n, x ]; h = p /. x -\u003e x^2 + O[x]^n; q = x*D[p, x]/p; Integrate[((p - 1)/k + Sum[EulerPhi[d]*(q /. x -\u003e x^d + O[x]^n), {d, 2, n}])/x, x] + If[OddQ[k], 0, 2^(k/2 - 2)*x*h^(k/2)] + 1];",
				"Clear[col]; col[k_] := col[k] = CoefficientList[seq[nmax, k], x];",
				"T[n_, k_] := col[k][[n + 1]];",
				"Table[T[n - k, k], {n, 0, nmax}, {k, n, 1, -1}] // Flatten (* _Jean-François Alcover_, Jul 04 2018, after _Andrew Howroyd_ *)"
			],
			"program": [
				"(PARI)",
				"seq(n,k)={ \\\\ gives gf of k'th column",
				"my(p=1 + serreverse( x/(k*2^if(k==1, 0, k-3)*(1 + x)^k) + O(x*x^n) ));",
				"my(h=subst(p,x,x^2+O(x*x^n)), q=x*deriv(p)/p);",
				"intformal( ((p-1)/k + sum(d=2,n,eulerphi(d)*subst(q,x,x^d+O(x*x^n))))/x) + if(k%2, 0, 2^(k/2-2)*x*h^(k/2)) + 1;",
				"}",
				"Mat(vector(6, k, Col(seq(7, k))))"
			],
			"xref": [
				"Columns 2..4 are A002995(n+1), A303865, A303866.",
				"Row n=1 is A051437(k-3).",
				"Cf. A302828, A303844, A303869.",
				"Cf. A295224 (polygon dissections), A303694 (sets of cycles instead of paths)."
			],
			"keyword": "nonn,tabl",
			"offset": "0,12",
			"author": "_Andrew Howroyd_, May 01 2018",
			"references": 7,
			"revision": 13,
			"time": "2018-07-04T02:40:35-04:00",
			"created": "2018-05-01T17:22:29-04:00"
		}
	]
}