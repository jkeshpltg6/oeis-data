{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A288211",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 288211,
			"data": "1,6,-5,36,-45,10,216,-360,80,75,-10,1296,-2700,600,1125,-250,-75,5,7776,-19440,4320,12150,-3600,-1125,-540,225,200,36,-1,46656,-136080,30240,113400,-37800,-23625,2800,5250,-3780,3150,-350,252,-105,-7",
			"name": "Irregular triangle read by rows of normalized Girard-Waring formula (cf. A210258), for m=6 data values.",
			"comment": [
				"Let SM_k = Sum( d_(t_1, t_2, ..., t_6)* eM_1^t_1 * eM_2^t_2 * ... * eM_6^t_6) summed over all length 6 integer partitions of k, i.e., 1*t_1 + 2*t_2 + 3*t_3 + ... + 6*t_6 = k, where SM_k are the averaged k-th power sum symmetric polynomials in 6 data (i.e., SM_k = S_k/6 where S_k are the k-th power sum symmetric polynomials, and where eM_k are the averaged k-th elementary symmetric polynomials, eM_k = e_k/binomial(6,k) with e_k being the k-th elementary symmetric polynomials.  The data d_(t_1, t_2, t_3, ..., t_6) form a triangle, with one row for each k value starting with k=1; the number of terms in successive rows is nondecreasing.",
				"Row sums of positive entries give 1,6,46,371,3026,24707,201748. Row sums of negative entries are always 1 less than corresponding row sums of positive entries."
			],
			"link": [
				"Gregory Gerard Wojnar, \u003ca href=\"/A288211/a288211.java.txt\"\u003eJava program\u003c/a\u003e",
				"G. G. Wojnar, D. Sz. Wojnar, and L. Q. Brin, \u003ca href=\"http://arxiv.org/abs/1706.08381\"\u003eUniversal Peculiar Linear Mean Relationships in All Polynomials\u003c/a\u003e, Table GW.n=6, p.24, arXiv:1706.08381 [math.GM], 2017."
			],
			"example": [
				"Triangle begins:",
				"1;",
				"6,-5;",
				"36,-45,10;",
				"216,-360,80,75,-10;",
				"1296,-2700,600,1125,-250,-75,5;",
				"7776,-19440,4320,12150,-3600,-1125,-540,225,200,36,-1;",
				"...",
				"Above represents:",
				"SM_1 = eM_1;",
				"SM_2 = 6*(eM_1)^2 - 5*eM_2;",
				"SM_3 = 36*(eM_1)^3 - 45*eM_1*eM_2 + 10*eM_3;",
				"SM_4 = 216*(eM_1)^4 - 360*(eM_1)^2*eM_2 + 80*eM_1*eM_3 + 75*(eM_2)^2 - 10*eM_4;",
				"SM_5 = 1296*(eM_1)^5 - 2700*(eM_1)^3*eM_2 + 600*(eM_1)^2*eM_3 + 1125*eM_1*(eM_2)^2 - 250*eM_2*eM_3 - 75*eM_1*eM_4 + 5*eM_5;",
				"..."
			],
			"program": [
				"See Java program link."
			],
			"xref": [
				"Cf. A028297 (m=2), A287768 (m=3), A288199 (m=4), A288207 (m=5), A288245 (m=7), A288188 (m=8). Also see A210258 Girard-Waring.",
				"First column of triangle is powers of m=6, A000400."
			],
			"keyword": "sign,tabf",
			"offset": "1,2",
			"author": "_Gregory Gerard Wojnar_, Jun 06 2017",
			"references": 7,
			"revision": 40,
			"time": "2017-07-04T09:30:51-04:00",
			"created": "2017-06-15T17:10:11-04:00"
		}
	]
}