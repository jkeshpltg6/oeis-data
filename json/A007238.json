{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007238",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7238,
			"id": "M0945",
			"data": "0,1,2,4,5,6,7,10,11,12,13,15,16,17,18,22,23,24,25,27,28,29,30,33,34,35,36,38,39,40,41,46,47,48,49,51,52,53,54,57,58,59,60,62,63,64,65,69,70,71,72,74,75,76,77,80,81,82,83,85,86,87,88,94,95,96,97,99,100,101",
			"name": "Length of longest chain of subgroups in S_n.",
			"comment": [
				"Starting at a(2), this is column 2 of Table 1 of the Donald M. Davis paper, p.32. - _Jonathan Vos Post_, Jul 17 2008"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A007238/b007238.txt\"\u003eTable of n, a(n) for n=1..1000\u003c/a\u003e",
				"J.-P. Allouche and J. Shallit, \u003ca href=\"http://dx.doi.org/10.1016/0304-3975(92)90001-V\"\u003eThe ring of k-regular sequences\u003c/a\u003e, Theoretical Computer Sci., 98 (1992), 163-197.",
				"J.-P. Allouche and J. Shallit, \u003ca href=\"http://www.math.jussieu.fr/~allouche/kreg2.ps\"\u003eThe Ring of k-regular Sequences, II\u003c/a\u003e",
				"L. Babai, \u003ca href=\"http://dx.doi.org/10.1080/00927878608823393\"\u003eOn the length of subgroup chains in the symmetric group\u003c/a\u003e, Commun. Algebra, 14 (1986), 1729-1736.",
				"P. J. Cameron, M. Gadouleau, J. D. Mitchell, Y. Peresse, \u003ca href=\"http://arxiv.org/abs/1501.06394\"\u003eChains of subsemigroups\u003c/a\u003e, arXiv preprint arXiv:1501.06394 [math.GR], 2015.",
				"Peter J. Cameron; Ron Solomon; Alexandre Turull, \u003ca href=\"http://dx.doi.org/10.1016/0021-8693(89)90256-1\"\u003eChains of subgroups in symmetric groups\u003c/a\u003e, J. Algebra 127 (1989), no. 2, 340-352.",
				"Donald M. Davis, \u003ca href=\"http://arxiv.org/abs/0807.2629\"\u003eDivisibility by 2 and 3 of certain Stirling numbers\u003c/a\u003e, arXiv:0807.2629 [math.NT], Jul 16, 2008."
			],
			"formula": [
				"a(n) = ceiling(3n/2) - b(n) - 1, where b(n) = # 1's in binary expansion of n (A000120).",
				"G.f.: 1/(1-x) * (-1/(1-x^2) + Sum(k\u003e=0, x^2^k/(1-x^2^k))). - _Ralf Stephan_, Apr 13 2002"
			],
			"maple": [
				"A000120 := proc(n)",
				"    convert(n,base,2) ;",
				"    add(i,i=%) ;",
				"end proc:",
				"A007238 := proc(n)",
				"    floor((3*n-1)/2)-A000120(n) ;",
				"end proc:",
				"seq(A007238(n),n=1..20) ;"
			],
			"mathematica": [
				"a[n_] := Ceiling[ 3n/2 ] - Count[ IntegerDigits[n, 2], 1] - 1; Table[ a[n], {n, 1, 70}] (* _Jean-François Alcover_, Jan 19 2012, after formula *)",
				"Table[Ceiling[(3n)/2]-DigitCount[n,2,1]-1,{n,70}] (* _Harvey P. Dale_, Nov 20 2021 *)"
			],
			"program": [
				"(PARI) vector(70, n, ceil(3*n/2) - hammingweight(n) - 1) \\\\ _Joerg Arndt_, May 16 2016"
			],
			"keyword": "nonn,nice",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_, _Simon Plouffe_",
			"references": 3,
			"revision": 45,
			"time": "2021-11-20T14:16:21-05:00",
			"created": "1994-04-28T03:00:00-04:00"
		}
	]
}