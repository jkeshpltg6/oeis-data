{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A098916",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 98916,
			"data": "0,4,36,288,2400,21600,211680,2257920,26127360,326592000,4390848000,63228211200,971415244800,15866448998400,274611617280000,5021469573120000,96746980442112000,1959126353952768000",
			"name": "Permanent of the n X n (0,1)-matrices with ij-th entry equal to zero iff (i=1,j=1),(i=1,j=n),(i=n,j=1) and (i=n,j=n).",
			"comment": [
				"The number of all possible ways to permute n distinct aligned balls, one is blue, 2 are red and the remaining are green, such that no red ball occurs by the side of the blue ball. It may generalized to r red balls: a(n,r) = (n-r-1)(n-r)(n-2)!. - Alessandro Nicolosi (xxalenicxx(AT)hotmail.com), Jul 12 2006",
				"A formula for the permanents of these n X n matrices(A) can be easily derived by minor expansion along the first row: a(n)=per(A)=(n-2)*per(B), where B is the n-1 X n-1 (0,1)-matrix with bij=0 iff (i=n,j=1) and (i=n,j=n). A new minor expansion along the last row of B yields: per(B)=(n-3)*per(C)=(n-3)*(n-2)! since C is the n-2 X n-2 1-matrix. Hence: a(n)=(n-2)*(n-3)*(n-2)!. - Herman Jamke (hermanjamke(AT)fastmail.fm), May 13 2007",
				"Number of permutations of n-1 having exactly 4 points P on the boundary of their bounding square. (A bounding square for a permutation of n is the square with sides parallel to the coordinate axis containing (1,1) and (n,n), and the set of points P of a permutation p is the set {(k,p(k)) for 0\u003ck\u003cn+1}). - _David Nacin_, Feb 27 2012",
				"a(n) is also the number of permutations of n symbols that 4-commute with a transposition (see A233440 for definition): a permutation p of {1,...,n} has exactly four points on the boundary of their bounding square if and only if p 4-commutes with transposition (1, n). - _Luis Manuel Rivera Martínez_, Feb 27 2014"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A098916/b098916.txt\"\u003eTable of n, a(n) for n = 3..200\u003c/a\u003e",
				"E. Deutsch, \u003ca href=\"http://webbox.lafayette.edu/~gordong/MathMagProblems.pdf\"\u003ePermutations and their bounding squares\u003c/a\u003e, Math Magazine, 85(1) (2012), p. 63.",
				"Luis Manuel Rivera, \u003ca href=\"http://arxiv.org/abs/1406.3081\"\u003eInteger sequences and k-commuting permutations\u003c/a\u003e, arXiv preprint arXiv:1406.3081 [math.CO], 2014-2015."
			],
			"formula": [
				"a(n) = (n-2)*(n-3)*(n-2)!. - Herman Jamke (hermanjamke(AT)fastmail.fm), May 13 2007"
			],
			"example": [
				"a(3) = 0 because no configuration is allowed, the 2 red balls always occurs by the side of the blue ball. a(4) = 4 because we can have 4 possible permutations: b,g1,r1,r2 b,g1,r2,r1 r1,r2,g1,b r2,r1,g1,b."
			],
			"maple": [
				"a:= n-\u003e(n-2)*(n-3)*(n-2)!: seq(a(n), n=3..20); # _Zerinvary Lajos_, Jul 01 2007"
			],
			"mathematica": [
				"a[n_,r_] := (n-r-1)(n-r)(n-2)! (* Alessandro Nicolosi (xxalenicxx(AT)hotmail.com), Jul 12 2006 *)",
				"Table[(n-2)*(n-3)*(n-2)!,{n,3,30}] (* _Vincenzo Librandi_, Feb 27 2012 *)"
			],
			"program": [
				"(PARI) permRWNb(a)=n=matsize(a)[1]; if(n==1,return(a[1,1])); sg=1; in=vectorv(n); x=in; x=a[,n]-sum(j=1,n,a[,j])/2; p=prod(i=1,n,x[i]); for(k=1,2^(n-1)-1,sg=-sg; j=valuation(k,2)+1; z=1-2*in[j]; in[j]+=z; x+=z*a[,j]; p+=prod(i=1,n,x[i],sg)); return(2*(2*(n%2)-1)*p)",
				"for(n=3,24,a=matrix(n,n,i,j,1); a[1,1]=0; a[1,n]=0; a[n,1]=0; a[n,n]=0; print1(permRWNb(a)\", \")) \\\\ Herman Jamke (hermanjamke(AT)fastmail.fm), May 13 2007",
				"(PARI) for(n=3,24,print1((n-2)*(n-3)*(n-2)!\", \")) \\\\ Herman Jamke (hermanjamke(AT)fastmail.fm), May 13 2007",
				"(Python)",
				"import math",
				"def a(n):",
				"    return (n-2)*(n-3)*math.factorial(n-2) # _David Nacin_, Feb 27 2012"
			],
			"xref": [
				"Cf. A208528, A208529."
			],
			"keyword": "nonn",
			"offset": "3,2",
			"author": "_Simone Severini_, Oct 17 2004",
			"ext": [
				"More terms from Herman Jamke (hermanjamke(AT)fastmail.fm), May 13 2007"
			],
			"references": 5,
			"revision": 38,
			"time": "2020-04-26T09:06:53-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}