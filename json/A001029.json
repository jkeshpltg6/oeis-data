{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001029",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1029,
			"id": "M5079 N2198",
			"data": "1,19,361,6859,130321,2476099,47045881,893871739,16983563041,322687697779,6131066257801,116490258898219,2213314919066161,42052983462257059,799006685782884121,15181127029874798299,288441413567621167681,5480386857784802185939,104127350297911241532841,1978419655660313589123979,37589973457545958193355601",
			"name": "Powers of 19.",
			"comment": [
				"The compositions of n in which each natural number is colored by one of p different colors are called p-colored compositions of n. For n\u003e=1, a(n) equals the number of 19-colored compositions of n such that no adjacent parts have the same color. - _Milan Janjic_, Nov 17 2011"
			],
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A001029/b001029.txt\"\u003eTable of n, a(n) for n = 0..100\u003c/a\u003e",
				"P. J. Cameron, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL3/groups.html\"\u003eSequences realized by oligomorphic permutation groups\u003c/a\u003e, J. Integ. Seqs. Vol. 3 (2000), #00.1.5.",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=283\"\u003eEncyclopedia of Combinatorial Structures 283\u003c/a\u003e",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"Y. Puri and T. Ward, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/index.html\"\u003eArithmetic and growth of periodic orbits\u003c/a\u003e, J. Integer Seqs., Vol. 4 (2001), #01.2.1.",
				"\u003ca href=\"/index/Rec#order_01\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (19)."
			],
			"formula": [
				"G.f.: 1/(1-19x), e.g.f.: exp(19x)",
				"a(n) = 19^n; a(n) = 19*a(n-1) with a(0)=1. - _Vincenzo Librandi_, Nov 21 2010"
			],
			"maple": [
				"A001029:=-1/(-1+19*z); # [Conjectured by _Simon Plouffe_ in his 1992 dissertation.]"
			],
			"mathematica": [
				"Table[19^n,{n,0,40}] (* _Vladimir Joseph Stephan Orlovsky_, Feb 15 2011 *)"
			],
			"program": [
				"(Sage) [lucas_number1(n,19,0) for n in range(1, 17)]# _Zerinvary Lajos_, Apr 29 2009",
				"(MAGMA) [ 19^n: n in [0..20] ]; // _Vincenzo Librandi_, Nov 21 2010",
				"(MAGMA) [ n eq 1 select 1 else 19*Self(n-1): n in [1..21] ];",
				"(PARI) a(n)=19^n \\\\ _Charles R Greathouse IV_, Sep 24 2015"
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_.",
			"references": 36,
			"revision": 61,
			"time": "2021-03-12T22:32:34-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}