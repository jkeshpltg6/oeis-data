{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A058730",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 58730,
			"data": "1,1,1,1,2,1,1,4,3,1,1,9,11,4,1,1,23,49,22,5,1,1,68,617,217,40,6,1,1,383,185981,188936,1092,66,7,1,1,5249,4884573865",
			"name": "Triangle T(n,k) giving number of nonisomorphic simple matroids of rank k on n labeled points (n \u003e= 2, 2 \u003c= k \u003c= n).",
			"comment": [
				"To make this sequence a triangular array, we assume n \u003e= 2 and 2 \u003c= k \u003c= n. According to the references, however, we have T(0,0) = T(1, 1) = 1, and 0 in all other cases. - _Petros Hadjicostas_, Oct 09 2019"
			],
			"link": [
				"Henry H. Crapo and Gian-Carlo Rota, \u003ca href=\"/A002773/a002773.pdf\"\u003eOn the foundations of combinatorial theory. II. Combinatorial geometries\u003c/a\u003e, Studies in Appl. Math. 49 (1970), 109-133. [Annotated scanned copy of pages 126 and 127 only]",
				"Henry H. Crapo and Gian-Carlo Rota, \u003ca href=\"https://doi.org/10.1002/sapm1970492109\"\u003eOn the foundations of combinatorial theory. II. Combinatorial geometries\u003c/a\u003e, Studies in Appl. Math. 49 (1970), 109-133.",
				"W. M. B. Dukes, \u003ca href=\"http://www.stp.dias.ie/~dukes/matroid.html\"\u003eTables of matroids\u003c/a\u003e.",
				"W. M. B. Dukes, \u003ca href=\"https://web.archive.org/web/20030208144026/http://www.stp.dias.ie/~dukes/phd.html\"\u003eCounting and Probability in Matroid Theory\u003c/a\u003e, Ph.D. Thesis, Trinity College, Dublin, 2000.",
				"W. M. B. Dukes, \u003ca href=\"https://arxiv.org/abs/math/0411557\"\u003eThe number of matroids on a finite set\u003c/a\u003e, arXiv:math/0411557 [math.CO], 2004.",
				"W. M. B. Dukes, \u003ca href=\"http://emis.impa.br/EMIS/journals/SLC/wpapers/s51dukes.html\"\u003eOn the number of matroids on a finite set\u003c/a\u003e, Séminaire Lotharingien de Combinatoire 51 (2004), Article B51g.",
				"Dillon Mayhew and Gordon F. Royle, \u003ca href=\"https://arxiv.org/abs/math/0702316\"\u003eMatroids with nine elements\u003c/a\u003e, arXiv:math/0702316 [math.CO], 2007. [See Table 2, p. 9.]",
				"Dillon Mayhew and Gordon F. Royle, \u003ca href=\"https://doi.org/10.1016/j.jctb.2007.07.005\"\u003eMatroids with nine elements\u003c/a\u003e, J. Combin. Theory Ser. B 98(2) (2008), 415-431. [See Table 2, p. 420.]",
				"Y. Matsumoto, S. Moriyama, H. Imai, and D. Bremmer, \u003ca href=\"https://doi.org/10.1007/s00454-011-9388-y\"\u003eMatroid enumeration for incidence geometry\u003c/a\u003e, Discrete Comput. Geom. 47 (2012), 17-43.",
				"Gordon Royle and Dillon Mayhew, \u003ca href=\"https://web.archive.org/web/20080828102733/http://people.csse.uwa.edu.au/gordon/matroid-integer-sequences.html\"\u003e9-element matroids\u003c/a\u003e.",
				"\u003ca href=\"/index/Mat#matroid\"\u003eIndex entries for sequences related to matroids\u003c/a\u003e"
			],
			"formula": [
				"From _Petros Hadjicostas_, Oct 09 2019: (Start)",
				"T(n, n-1) = n-2 for n \u003e= 2. [Dukes (2004), Lemma 2.2(ii).]",
				"T(n, n-2) = 6 - 4*n + Sum_{k = 1..n} A000041(k) for n \u003e= 3. [Dukes (2004), Lemma 2.2(iv).]",
				"(End)"
			],
			"example": [
				"Triangle T(n,k) (with rows n \u003e= 2 and columns k \u003e= 2) begins as follows:",
				"  1;",
				"  1,   1;",
				"  1,   2,      1;",
				"  1,   4,      3,      1;",
				"  1,   9,     11,      4,    1;",
				"  1,  23,     49,     22,    5,  1;",
				"  1,  68,    617,    217,   40,  6, 1;",
				"  1, 383, 185981, 188936, 1092, 66, 7, 1;",
				"  ...",
				"From _Petros Hadjicostas_, Oct 09 2019: (Start)",
				"Matsumoto et al. (2012, p. 36) gave an incomplete row n = 10 (starting at k = 2):",
				"  1, 5249, 4884573865, *, 4886374072, 9742, 104, 8, 1;",
				"They also gave incomplete rows for n = 11 and n = 12.",
				"(End)"
			],
			"xref": [
				"Cf. A058720. Row sums give A002773.",
				"Columns include (truncations of) A000012 (k=2), A058731 (k=3), A058733 (k=4)."
			],
			"keyword": "nonn,tabl,nice,more",
			"offset": "2,5",
			"author": "_N. J. A. Sloane_, Dec 31 2000",
			"ext": [
				"Row n=9 from _Petros Hadjicostas_, Oct 09 2019 using the papers by Mayhew and Royle"
			],
			"references": 4,
			"revision": 38,
			"time": "2019-10-10T04:26:16-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}