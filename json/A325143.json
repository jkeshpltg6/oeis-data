{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A325143",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 325143,
			"data": "3,5,7,11,13,17,19,29,31,37,41,43,53,61,67,73,79,89,97,101,103,109,113,127,137,139,149,151,157,163,173,181,193,197,199,211,223,229,233,241,257,269,271,277,281,283,293,307,313,317,331,337,349,353,367,373",
			"name": "Primes represented by cyclotomic binary forms.",
			"comment": [
				"A cyclotomic binary form over Z is a homogeneous polynomial in two variables which has the form f(x, y) = y^EulerPhi(k)*CyclotomicPolynomial(k, x/y) where k is some integer \u003e= 3. An integer n is represented by f if f(x, y) = n has an integer solution."
			],
			"link": [
				"Peter Luschny, \u003ca href=\"/A325143/b325143.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Étienne Fouvry, Claude Levesque, Michel Waldschmidt, \u003ca href=\"http://dx.doi.org/10.4064/aa171012-24-12\"\u003eRepresentation of integers by cyclotomic binary forms\u003c/a\u003e, Acta Arithmetica 184 (2018), 67-86; \u003ca href=\"https://arxiv.org/abs/1712.09019\"\u003earXiv:1712.09019\u003c/a\u003e, arXiv:1712.09019 [math.NT], 2017."
			],
			"program": [
				"(Julia) using Nemo",
				"function isA325143(n)",
				"    (n \u003c 3 || !isprime(ZZ(n))) \u0026\u0026 return false",
				"    R, x = PolynomialRing(ZZ, \"x\")",
				"    K = floor(Int, 5.383*log(n)^1.161) # Bounds from",
				"    M = floor(Int, 2*sqrt(n/3)) # Fouvry \u0026 Levesque \u0026 Waldschmidt",
				"    N = QQ(n)",
				"    for k in 3:K",
				"        e = Int(eulerphi(ZZ(k)))",
				"        c = cyclotomic(k, x)",
				"        for m in 1:M, j in 0:M if max(j, m) \u003e 1",
				"            N == m^e*subst(c, QQ(j,m)) \u0026\u0026 return true",
				"    end end end",
				"    return false",
				"end",
				"[n for n in 1:373 if isA325143(n)] |\u003e println"
			],
			"xref": [
				"Subsequence of A296095. Complement A325145. Number of A325141.",
				"Cf. A293654, A299214, A299498, A299733, A299928, A299930, A299956, A299964."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Peter Luschny_, May 16 2019",
			"references": 4,
			"revision": 18,
			"time": "2019-05-20T06:34:25-04:00",
			"created": "2019-05-16T17:34:44-04:00"
		}
	]
}