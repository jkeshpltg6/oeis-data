{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A131113",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 131113,
			"data": "1,5,1,5,10,1,5,15,15,1,5,20,30,20,1,5,25,50,50,25,1,5,30,75,100,75,30,1,5,35,105,175,175,105,35,1,5,40,140,280,350,280,140,40,1,5,45,180,420,630,630,420,180,45,1",
			"name": "T(n,k) = 5*binomial(n,k) - 4*I(n,k), where I is the identity matrix; triangle T read by rows (n \u003e= 0 and 0 \u003c= k \u003c= n).",
			"comment": [
				"Row sums = A048487: (1, 6, 16, 36, 76, 156, ...)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A131113/b131113.txt\"\u003eRows n = 0..100 of triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = 5*A007318(n,k) - 4*I(n,k), where A007318 = Pascal's triangle and I = Identity matrix.",
				"Bivariate o.g.f.: Sum_{n,k\u003e=0} T(n,k)*x^n*y^k = (1 + 4*x - x*y)/((1 - x*y)*(1 - x - x*y)). - _Petros Hadjicostas_, Feb 20 2021"
			],
			"example": [
				"Triangle T(n,k) (with rows n \u003e= 0 and columns k = 0..n) begins:",
				"  1;",
				"  5,  1;",
				"  5, 10,  1;",
				"  5, 15, 15,  1;",
				"  5, 20, 30,  20,  1;",
				"  5, 25, 50,  50, 25,  1;",
				"  5, 30, 75, 100, 75, 30, 1;",
				"  ..."
			],
			"maple": [
				"seq(seq(`if`(k=n, 1, 5*binomial(n,k)), k=0..n), n=0..10); # _G. C. Greubel_, Nov 18 2019"
			],
			"mathematica": [
				"Table[If[k==n, 1, 5*Binomial[n, k]], {n,0,10}, {k,0,n}]//Flatten (* _G. C. Greubel_, Nov 18 2019 *)"
			],
			"program": [
				"(PARI) T(n,k) = if(k==n, 1, 5*binomial(n,k)); \\\\ _G. C. Greubel_, Nov 18 2019",
				"(MAGMA) [k eq n select 1 else 5*Binomial(n,k): k in [0..n], n in [0..10]]; // _G. C. Greubel_, Nov 18 2019",
				"(Sage)",
				"def T(n, k):",
				"    if k == n: return 1",
				"    else: return 5*binomial(n, k)",
				"[[T(n, k) for k in (0..n)] for n in (0..10)]",
				"# _G. C. Greubel_, Nov 18 2019",
				"(GAP)",
				"T:= function(n,k)",
				"    if k=n then return 1;",
				"    else return 5*Binomial(n,k);",
				"    fi;  end;",
				"Flat(List([0..10], n-\u003e List([0..n], k-\u003e T(n,k) ))); # _G. C. Greubel_, Nov 18 2019"
			],
			"xref": [
				"Cf. A007318, A048487, A131110, A131112, A131114, A131115."
			],
			"keyword": "nonn,tabl,easy,less",
			"offset": "0,2",
			"author": "_Gary W. Adamson_, Jun 15 2007",
			"references": 6,
			"revision": 24,
			"time": "2021-02-21T04:06:15-05:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}