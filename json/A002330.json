{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002330",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2330,
			"id": "M0462 N0169",
			"data": "1,2,3,4,5,6,5,7,6,8,8,9,10,10,8,11,10,11,13,10,12,14,15,13,15,16,13,14,16,17,13,14,16,18,17,18,17,19,20,20,15,17,20,21,19,22,20,21,19,20,24,23,24,18,19,25,22,25,23,26,26,22,27,26,20,25,22,26,28,25",
			"name": "Value of y in the solution to p = x^2 + y^2, x \u003c= y, with prime p = A002313(n).",
			"comment": [
				"Equals A096029(n) + A096030(n) + 1, for entries after the first. - _Lekraj Beedassy_, Jul 21 2004",
				"a(n+1) = MAX(A002972(n), 2*A002973(n)). [From _Reinhard Zumkeller_, Feb 16 2010]"
			],
			"reference": [
				"A. J. C. Cunningham, Quadratic Partitions. Hodgson, London, 1904, p. 1.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe and Charles R Greathouse IV, \u003ca href=\"/A002330/b002330.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (first 1000 terms from Noe)",
				"John Brillhart, \u003ca href=\"https://doi.org/10.1090/S0025-5718-1972-0314745-6\"\u003eNote on representing a prime as a sum of two squares\u003c/a\u003e, Math. Comp. 26 (1972), pp. 1011-1013.",
				"A. J. C. Cunningham, \u003ca href=\"/A002330/a002330.pdf\"\u003eQuadratic Partitions\u003c/a\u003e, Hodgson, London, 1904 [Annotated scans of selected pages]",
				"K. Matthews, \u003ca href=\"http://www.numbertheory.org/php/serret.html\"\u003eSerret's algorithm Server\u003c/a\u003e",
				"J. Todd, \u003ca href=\"https://www.jstor.org/stable/2305526\"\u003eA problem on arc tangent relations\u003c/a\u003e, Amer. Math. Monthly, 56 (1949), 517-528.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Fermats4nPlus1Theorem.html\"\u003eFermat's 4n Plus 1 Theorem\u003c/a\u003e"
			],
			"example": [
				"The following table shows the relationship between several closely related sequences:",
				"Here p = A002144 = primes == 1 mod 4, p = a^2+b^2 with a \u003c b;",
				"a = A002331, b = A002330, t_1 = ab/2 = A070151;",
				"p^2 = c^2+d^2 with c \u003c d; c = A002366, d = A002365,",
				"t_2 = 2ab = A145046, t_3 = b^2-a^2 = A070079,",
				"with {c,d} = {t_2, t_3}, t_4 = cd/2 = ab(b^2-a^2).",
				"---------------------------------",
				".p..a..b..t_1..c...d.t_2.t_3..t_4",
				"---------------------------------",
				".5..1..2...1...3...4...4...3....6",
				"13..2..3...3...5..12..12...5...30",
				"17..1..4...2...8..15...8..15...60",
				"29..2..5...5..20..21..20..21..210",
				"37..1..6...3..12..35..12..35..210",
				"41..4..5..10...9..40..40...9..180",
				"53..2..7...7..28..45..28..45..630",
				"................................."
			],
			"maple": [
				"a := []; for x from 0 to 50 do for y from x to 50 do p := x^2+y^2; if isprime(p) then a := [op(a),[p,x,y]]; fi; od: od: writeto(trans); for i from 1 to 158 do lprint(a[i]); od: # then sort the triples in \"trans\""
			],
			"mathematica": [
				"Flatten[#, 1]\u0026[Table[PowersRepresentations[Prime[k], 2, 2], {k, 1, 142}]][[All, 2]] (* _Jean-François Alcover_, Jul 05 2011 *)"
			],
			"program": [
				"(PARI) f(p)=my(s=lift(sqrt(Mod(-1,p))),x=p,t);if(s\u003ep/2,s=p-s); while(s^2\u003ep, t=s;s=x%s;x=t);s",
				"forprime(p=2,1e3,if(p%4-3,print1(f(p)\", \"))) \\\\ _Charles R Greathouse IV_, Apr 24 2012",
				"(PARI) do(p)=qfbsolve(Qfb(1,0,1),p)[1]",
				"forprime(p=2,1e3,if(p%4-3,print1(do(p)\", \"))) \\\\ _Charles R Greathouse IV_, Sep 26 2013",
				"(PARI) print1(1); forprimestep(p=5,1e3,4, print1(\", \"qfbcornacchia(1,p)[1])) \\\\ _Charles R Greathouse IV_, Sep 15 2021"
			],
			"xref": [
				"Cf. A002331, A002313, A002144."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_.",
			"references": 39,
			"revision": 47,
			"time": "2021-09-15T09:42:08-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}