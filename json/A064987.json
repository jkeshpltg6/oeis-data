{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A064987",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 64987,
			"data": "1,6,12,28,30,72,56,120,117,180,132,336,182,336,360,496,306,702,380,840,672,792,552,1440,775,1092,1080,1568,870,2160,992,2016,1584,1836,1680,3276,1406,2280,2184,3600,1722,4032,1892,3696,3510,3312,2256,5952",
			"name": "a(n) = n*sigma(n).",
			"comment": [
				"Dirichlet convolution of sigma_2(n)=A001157(n) with phi(n)=A000010(n). - _Vladeta Jovovic_, Oct 27 2002",
				"Equals row sums of triangle A143311 and of triangle A143308. - _Gary W. Adamson_, Aug 06 2008",
				"a(n) is also the sum of all n's present in A244580, or in other words, a(n) is also the volume (or number of cubes) below the terraces of the n-th level of the staircase described in A244580 (see also A237593). - _Omar E. Pol_, Oct 11 2018",
				"If n is a superperfect number then sigma(n) is a Mersenne prime and a(n) is a perfect number, a(A019279(k)) = A000396(k), k \u003e= 1, assuming there are no odd perfect numbers. - _Omar E. Pol_, Apr 15 2020"
			],
			"reference": [
				"B. C. Berndt, Ramanujan's theory of theta-functions, Theta functions: from the classical to the modern, Amer. Math. Soc., Providence, RI, 1993, pp. 1-63. MR 94m:11054. see page 43.",
				"G. H. Hardy, Ramanujan: twelve lectures on subjects suggested by his life and work, AMS Chelsea Publishing, Providence, Rhode Island, 2002, pp. 166-167."
			],
			"link": [
				"Harry J. Smith, \u003ca href=\"/A064987/b064987.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Joerg Arndt, \u003ca href=\"http://arxiv.org/abs/1202.6525\"\u003eOn computing the generalized Lambert series\u003c/a\u003e, arXiv:1202.6525v3 [math.CA], (2012).",
				"M. Planat, \u003ca href=\"http://arxiv.org/abs/1201.5455\"\u003eTwelve-dimensional Pauli group contextuality with eleven rays\u003c/a\u003e, arXiv:1201.5455 [quant-ph], 2012."
			],
			"formula": [
				"Multiplicative with a(p^e) = p^e * (p^(e+1) - 1) / (p - 1).",
				"G.f.: Sum_{n\u003e0} n^2*x^n/(1-x^n)^2. - _Vladeta Jovovic_, Oct 27 2002",
				"G.f. is phi_{2, 1}(x) where phi_{r, s}(x) = Sum_{n, m\u003e0} m^r * n^s * x^{m*n}. - _Michael Somos_, Apr 02 2003",
				"G.f. is also (Q - P^2) / 288 where P, Q are Ramanujan Lambert series. - _Michael Somos_, Apr 02 2003. See the Hardy reference, p. 136, eq. (10.5.4) (with a proof). For Q and P, (10.5.6) and (10.5.5), see E_4 A004009 and E_2 A006352, respectively. - _Wolfdieter Lang_, Jan 30 2017",
				"Convolution of A000118 and A186690. Dirichlet convolution of A000027 and A000290. - _Michael Somos_, Mar 25 2012",
				"Dirichlet g.f. zeta(s-1)*zeta(s-2). - _R. J. Mathar_, Feb 16 2011",
				"a(n) = A009194(n)*A009242(n). - _Michel Marcus_, Oct 23 2013",
				"a(n) (mod 5) = A126832(n) = A000594(n) (mod 5). See A126832 for references. - _Wolfdieter Lang_, Feb 03 2017",
				"L.g.f.: Sum_{k\u003e=1} k*x^k/(1 - x^k) = Sum_{n\u003e=1} a(n)*x^n/n. - _Ilya Gutkovskiy_, May 13 2017",
				"Sum_{k\u003e=1} 1/a(k) = 1.4383899259334187832765458631783591251241657856627653748389234270650138768... - _Vaclav Kotesovec_, Sep 20 2020",
				"From _Peter Bala_, Jan 21 2021: (Start)",
				"G.f.: Sum_{n \u003e= 1} n*q^n*(1 + q^n)/(1 - q^n)^3 (use the expansion x*(1 + x)/(1 - x)^3 = x + 2^2*x^2 + 3^2*x^3 + 4^2*x^4 + ...).",
				"A faster converging g.f.: Sum_{n \u003e= 1} q^(n^2)*( n^3*q^(3*n) - (n^3 + 3*n^2 - n)*q^(2*n) - (n^3 - 3*n^2 - n)*q^n + n^3 )/(1 - q^n)^3 - differentiate equation 5 in Arndt w.r.t. both x and q and then set x = 1. (End)",
				"From _Richard L. Ollerton_, May 07 2021: (Start)",
				"a(n) = Sum_{k=1..n} sigma_2(gcd(n,k)).",
				"a(n) = Sum_{k=1..n} sigma_2(n/gcd(n,k))*phi(gcd(n,k))/phi(n/gcd(n,k)). (End)"
			],
			"maple": [
				"with(numtheory): [n*sigma(n)$n=1..50]; # _Muniru A Asiru_, Jan 01 2019"
			],
			"mathematica": [
				"# DivisorSigma[1,#]\u0026/@Range[80]  (* _Harvey P. Dale_, Mar 12 2011 *)"
			],
			"program": [
				"(PARI) {a(n) = if ( n==0, 0, n * sigma(n))}",
				"(PARI) { for (n=1, 1000, write(\"b064987.txt\", n, \" \", n*sigma(n)) ) } \\\\ _Harry J. Smith_, Oct 02 2009",
				"(MuPAD) numlib::sigma(n)*n$ n=1..81 // _Zerinvary Lajos_, May 13 2008",
				"(Haskell)",
				"a064987 n = a000203 n * n  -- _Reinhard Zumkeller_, Jan 21 2014",
				"(MAGMA) [n*SumOfDivisors(n): n in [1..70]]; // _Vincenzo Librandi_, Jan 01 2019",
				"(GAP) a:=List([1..50],n-\u003en*Sigma(n));; Print(a); # _Muniru A Asiru_, Jan 01 2019"
			],
			"xref": [
				"Main diagonal of A319073.",
				"Cf. A000203, A038040, A002618, A000010, A001157, A143308, A143311, A004009, A006352, A000594, A126832, A069097 (Mobius transform), A001001 (inverse Mobius transform), A237593, A244580."
			],
			"keyword": "mult,nonn,easy",
			"offset": "1,2",
			"author": "_Vladeta Jovovic_, Oct 30 2001",
			"references": 58,
			"revision": 90,
			"time": "2021-10-15T11:58:38-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}