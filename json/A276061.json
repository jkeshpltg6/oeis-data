{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A276061",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 276061,
			"data": "0,0,0,0,0,2,2,4,6,10,18,28,46,74,114,184,286,448,700,1080,1676,2582,3970,6104,9338,14288,21808,33224,50580,76844,116640,176832,267740,405058,612110,924204,1394266,2101558,3165406,4764184,7165530,10770386,16178378",
			"name": "Sum of the asymmetry degrees of all compositions of n into parts congruent to 1 mod 3.",
			"comment": [
				"The asymmetry degree of a finite sequence of numbers is defined to be the number of pairs of symmetrically positioned distinct entries. Example: the asymmetry degree of (2,7,6,4,5,7,3) is 2, counting the pairs (2,3) and (6,5).",
				"A sequence is palindromic if and only if its asymmetry degree is 0."
			],
			"reference": [
				"S. Heubach and T. Mansour, Combinatorics of Compositions and Words, CRC Press, 2010."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A276061/b276061.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Krithnaswami Alladi and V. E. Hoggatt, Jr. \u003ca href=\"http://www.fq.math.ca/Scanned/13-3/alladi1.pdf\"\u003eCompositions with Ones and Twos\u003c/a\u003e, Fibonacci Quarterly, 13 (1975), 233-239.",
				"V. E. Hoggatt, Jr., and Marjorie Bicknell, \u003ca href=\"http://www.fq.math.ca/Scanned/13-4/hoggatt1.pdf\"\u003ePalindromic compositions\u003c/a\u003e, Fibonacci Quart., Vol. 13(4), 1975, pp. 350-356.",
				"\u003ca href=\"/index/Rec#order_12\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,1,1,-1,0,-1,-1,-1,-2,1,0,1)."
			],
			"formula": [
				"G.f. g(z) = 2*z^5*(1-z^3)/((1+z)(1-z+z^2)(1+z-z^3)(1-z-z^3)^2). In the more general situation of compositions into a[1]\u003ca[2]\u003ca[3]\u003c..., denoting F(z) = Sum(z^{a[j]},j\u003e=1}, we have  g(z) = (F(z)^2 - F(z^2))/((1+F(z))(1-F(z))^2).",
				"a(n) = Sum(k*A276060(n,k), k\u003e=0)."
			],
			"example": [
				"a(7) = 4 because the compositions of 7 with parts in {1,4,7,10,... } are  7, 4111, 1411, 1141, 1114, and 1111111, and the sum of their asymmetry degrees is 0+1+1+1+1+0."
			],
			"maple": [
				"g := 2*z^5*(1-z^3)/((1+z)*(1-z+z^2)*(1+z-z^3)*(1-z-z^3)^2): gser := series(g, z = 0, 45): seq(coeff(gser, z, n), n = 0 .. 40);"
			],
			"mathematica": [
				"Table[Total@ Map[Total, Map[Map[Boole[# \u003e= 1] \u0026, BitXor[Take[# - 1, Ceiling[Length[#]/2]], Reverse@ Take[# - 1, -Ceiling[Length[#]/2]]]] \u0026, Flatten[Map[Permutations, DeleteCases[IntegerPartitions@ n, {___, a_, ___} /; Mod[a, 3] != 1]], 1]]], {n, 0, 36}] // Flatten (* _Michael De Vlieger_, Aug 22 2016 *)"
			],
			"program": [
				"(PARI) concat(vector(5), Vec(2*x^5*(1-x^3)/((1+x)*(1-x+x^2)*(1+x-x^3)*(1-x-x^3)^2) + O(x^50))) \\\\ _Colin Barker_, Aug 28 2016"
			],
			"xref": [
				"Cf. A276060."
			],
			"keyword": "nonn,easy",
			"offset": "0,6",
			"author": "_Emeric Deutsch_, Aug 22 2016",
			"references": 2,
			"revision": 13,
			"time": "2016-08-28T11:09:16-04:00",
			"created": "2016-08-23T04:46:08-04:00"
		}
	]
}