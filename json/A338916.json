{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A338916",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 338916,
			"data": "1,0,1,1,2,3,5,6,8,12,16,21,28,37,49,64,80,104,135,169,216,268,341,420,527,654,809,991,1218,1488,1828,2213,2687,3262,3934,4754,5702,6849,8200,9819,11693",
			"name": "Number of integer partitions of n that can be partitioned into distinct pairs of (possibly equal) parts.",
			"comment": [
				"The multiplicities of such a partition form a loop-graphical partition (A339656, A339658)."
			],
			"link": [
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/GraphicalPartition.html\"\u003eGraphical partition.\u003c/a\u003e"
			],
			"formula": [
				"A027187(n) = a(n) + A338915(n)."
			],
			"example": [
				"The a(2) = 1 through a(10) = 16 partitions:",
				"  (11)  (21)  (22)  (32)    (33)    (43)    (44)    (54)      (55)",
				"              (31)  (41)    (42)    (52)    (53)    (63)      (64)",
				"                    (2111)  (51)    (61)    (62)    (72)      (73)",
				"                            (2211)  (2221)  (71)    (81)      (82)",
				"                            (3111)  (3211)  (3221)  (3222)    (91)",
				"                                    (4111)  (3311)  (3321)    (3322)",
				"                                            (4211)  (4221)    (3331)",
				"                                            (5111)  (4311)    (4222)",
				"                                                    (5211)    (4321)",
				"                                                    (6111)    (4411)",
				"                                                    (222111)  (5221)",
				"                                                    (321111)  (5311)",
				"                                                              (6211)",
				"                                                              (7111)",
				"                                                              (322111)",
				"                                                              (421111)",
				"For example, the partition (4,2,1,1,1,1) can be partitioned into {{1,1},{1,2},{1,4}}  so is counted under a(10)."
			],
			"mathematica": [
				"stfs[n_]:=If[n\u003c=1,{{}},Join@@Table[Map[Prepend[#,d]\u0026,Select[stfs[n/d],Min@@#\u003ed\u0026]],{d,Select[Rest[Divisors[n]],PrimeOmega[#]==2\u0026]}]];",
				"Table[Length[Select[IntegerPartitions[n],stfs[Times@@Prime/@#]!={}\u0026]],{n,0,20}]"
			],
			"xref": [
				"A320912 gives the Heinz numbers of these partitions.",
				"A338915 counts the complement in even-length partitions.",
				"A339563 counts factorizations of the same type.",
				"A000070 counts non-multigraphical partitions of 2n, ranked by A339620.",
				"A000569 counts graphical partitions, ranked by A320922.",
				"A001358 lists semiprimes, with squarefree case A006881.",
				"A058696 counts partitions of even numbers, ranked by A300061.",
				"A209816 counts multigraphical partitions, ranked by A320924.",
				"A320655 counts factorizations into semiprimes.",
				"A322353 counts factorizations into distinct semiprimes.",
				"A339617 counts non-graphical partitions of 2n, ranked by A339618.",
				"A339655 counts non-loop-graphical partitions of 2n, ranked by A339657.",
				"A339656 counts loop-graphical partitions, ranked by A339658.",
				"The following count partitions of even length and give their Heinz numbers:",
				"- A027187 has no additional conditions (A028260).",
				"- A096373 cannot be partitioned into strict pairs (A320891).",
				"- A338914 can be partitioned into strict pairs (A320911).",
				"- A338915 cannot be partitioned into distinct pairs (A320892).",
				"- A339559 cannot be partitioned into distinct strict pairs (A320894).",
				"- A339560 can be partitioned into distinct strict pairs (A339561).",
				"Cf. A001055, A007717, A025065, A320656, A320732, A320893, A320921, A338898, A338902, A339564."
			],
			"keyword": "nonn,more",
			"offset": "0,5",
			"author": "_Gus Wiseman_, Dec 10 2020",
			"references": 20,
			"revision": 11,
			"time": "2020-12-18T07:58:40-05:00",
			"created": "2020-12-18T07:58:40-05:00"
		}
	]
}