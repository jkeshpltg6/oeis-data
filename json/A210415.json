{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A210415",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 210415,
			"data": "1,3,10,6,11,7,21,13,15,17,19,101,24,100,29,102,34,103,39,104,44,105,49,106,54,107,59,108,64,109,69,110,70,76,111,77,78,85,112,86,91,94,113,95,211,1111,11111,1110,115,116,118,119,121,122,124,125,127,129",
			"name": "List the positions of all digits 1 in the concatenation of all terms, not necessarily in order. This is the lexicographically earliest such sequence.",
			"comment": [
				"Original name: \"A self-describing sequence: The a(n) say the positions of the digits 1 inside the sequence when it is read as a string of digits.\"",
				"For each n, the digit in position a(n) is equal to 1. At each step, choose the minimum integer not yet present in the sequence and not leading to a contradiction.",
				"This sequence had been mentioned by Wasserman in 2008, cf. A098645, a variant of this sequence with additional restriction a(n+1) \u003e a(n). - _M. F. Hasler_, Oct 08 2013"
			],
			"link": [
				"Danny Rorabaugh, \u003ca href=\"/A210415/b210415.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Se#self-referencing_sequences\"\u003eIndex to the OEIS: Entries related to self-referencing sequences\u003c/a\u003e."
			],
			"example": [
				"The sequence starts with 1: the first digit is equal to 1. In the second position we cannot write 2 because the second digit would not be 1 but 2. Then we write 3. The third digit must be 1 and the minimum number starting with 1 is 10. And so on."
			],
			"program": [
				"(Sage) #Returns the first n terms of this sequence for digit d",
				"def dig_loc(d,n):",
				"  L, S = [], \"\"",
				"  while len(L)\u003cn:",
				"    ext, new, lenS = 0, 0, len(S)",
				"    while new==0:",
				"      ext += 1",
				"      while d==0 and (lenS+ext+1 in L): ext += 1",
				"      d_s = [i for i in range(ext) if (lenS+i+1 in L)]",
				"      need = ext - len(d_s)",
				"      for i in range(10^(need-1)*(0 not in d_s), 10^(need)):",
				"        a = [str(0)]*(need - len(str(i))) + list(str(i))",
				"        for j in d_s: a.insert(j,d)",
				"        new = sum([int(a[ext-1-j])*10^j for j in range(ext)])",
				"        if ( new not in L ) and ( new\u003elenS or S[new-1]==str(d) ) and ( (new-lenS-1 not in range(ext)) or a[new-lenS-1]==str(d) ) and ( d!=0 or lenS+ext+1!=new ):",
				"          L.append(new)",
				"          S += str(new)",
				"          break",
				"        else: new = 0",
				"  return L",
				"dig_loc(1,58) # _Danny Rorabaugh_, Nov 27 2015"
			],
			"xref": [
				"This construction for other decimal digits is A210414, A210416, A210417, A210418, A210419, A210420, A210421, A210422, A210423.",
				"Variants of this sequence include A098645, A098670, A114134, A167519."
			],
			"keyword": "nonn,base,nice",
			"offset": "1,2",
			"author": "_Paolo P. Lava_, Mar 26 2012",
			"ext": [
				"Edited by _M. F. Hasler_, Oct 10 2013"
			],
			"references": 13,
			"revision": 40,
			"time": "2021-04-18T02:15:15-04:00",
			"created": "2012-03-26T12:39:39-04:00"
		}
	]
}