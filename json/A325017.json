{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A325017",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 325017,
			"data": "1,1,1,4,6,3,1,20,204,1056,2850,4080,2940,840,1,400,130899,11230666,347919225,5158324560,43174480650,225086553300,775894225050,1831178115900,3008073915000,3439243962000,2685727044000,1366701336000,408648240000,54486432000",
			"name": "Triangle read by rows: T(n,k) is the number of unoriented colorings of the facets of a regular n-dimensional orthoplex using exactly k colors. Row n has 2^n columns.",
			"comment": [
				"Also called cross polytope and hyperoctahedron. For n=1, the figure is a line segment with two vertices. For n=2 the figure is a square with four edges. For n=3 the figure is an octahedron with eight triangular faces. For n=4, the figure is a 16-cell with sixteen tetrahedral facets. The Schläfli symbol, {3,...,3,4}, of the regular n-dimensional orthoplex (n\u003e1) consists of n-2 threes followed by a four. Each of its 2^n facets is an (n-1)-dimensional simplex. Two unoriented colorings are the same if congruent; chiral pairs are counted as one.",
				"Also the number of unoriented colorings of the vertices of a regular n-dimensional orthotope (cube) using exactly k colors."
			],
			"link": [
				"Robert A. Russell, \u003ca href=\"/A325017/b325017.txt\"\u003eTable of n, a(n) for n = 1..510\u003c/a\u003e, rows 1..8, flattened.",
				"E. M. Palmer and R. W. Robinson, \u003ca href=\"https://projecteuclid.org/euclid.acta/1485889789\"\u003eEnumeration under two representations of the wreath product\u003c/a\u003e, Acta Math., 131 (1973), 123-143."
			],
			"formula": [
				"A325013(n,k) = Sum_{j=1..2^n} T(n,j) * binomial(k,j).",
				"T(n,k) = A325016(n,k) - A325018(n,k) = (A325016(n,k) + A325019(n,k)) / 2 = A325018(n,k) + A325019(n,k)."
			],
			"example": [
				"Triangle begins with T(1,1):",
				"1  1",
				"1  4   6    3",
				"1 20 204 1056 2850 4080 2940 840",
				"For T(2,2)=4, two squares have three edges the same color, one has opposite edges the same color, and one has opposite edges different colors."
			],
			"mathematica": [
				"a48[n_] := a48[n] = DivisorSum[NestWhile[#/2\u0026,n,EvenQ],MoebiusMu[#]2^(n/#)\u0026]/(2n); (* A000048 *)",
				"a37[n_] := a37[n] = DivisorSum[n, MoebiusMu[n/#]2^#\u0026]/n; (* A001037 *)",
				"CI0[{n_Integer}] := CI0[{n}] = CI[Transpose[If[EvenQ[n], p2 = IntegerExponent[n, 2]; sub = Divisors[n/2^p2]; {2^(p2+1) sub, a48 /@ (2^p2 sub) }, sub = Divisors[n]; {sub, a37 /@ sub}]]] 2^(n-1); (* even perm. *)",
				"CI1[{n_Integer}] := CI1[{n}] = CI[sub = Divisors[n]; Transpose[If[EvenQ[n], {sub, a37 /@ sub}, {2 sub, (a37 /@ sub)/2}]]] 2^(n-1); (* odd perm. *)",
				"compress[x : {{_, _} ...}] := (s = Sort[x]; For[i = Length[s], i \u003e 1, i -= 1, If[s[[i, 1]]==s[[i-1, 1]], s[[i-1, 2]] += s[[i, 2]]; s = Delete[s, i], Null]]; s)",
				"cix[{a_, b_}, {c_, d_}] := {LCM[a, c], (a b c d)/LCM[a, c]};",
				"Unprotect[Times]; Times[CI[a_List], CI[b_List]] :=  (* combine *) CI[compress[Flatten[Outer[cix, a, b, 1], 1]]]; Protect[Times];",
				"CI0[p_List] := CI0[p] = Expand[CI0[Drop[p, -1]] CI0[{Last[p]}] + CI1[Drop[p, -1]] CI1[{Last[p]}]]",
				"CI1[p_List] := CI1[p] = Expand[CI0[Drop[p, -1]] CI1[{Last[p]}] + CI1[Drop[p, -1]] CI0[{Last[p]}]]",
				"pc[p_List] := Module[{ci,mb},mb = DeleteDuplicates[p]; ci = Count[p, #] \u0026 /@ mb; n!/(Times @@ (ci!) Times @@ (mb^ci))] (* partition count *)",
				"row[n_Integer] := row[n] = Factor[(Total[((CI0[#] + CI1[#]) pc[#]) \u0026 /@ IntegerPartitions[n]])/(n! 2^n)] /. CI[l_List] :\u003e j^(Total[l][[2]])",
				"array[n_, k_] := row[n] /. j -\u003e k (* A325013 *)",
				"Table[LinearSolve[Table[Binomial[i,j],{i,1,2^n},{j,1,2^n}],Table[array[n,k],{k,1,2^n}]],{n,1,6}] // Flatten"
			],
			"xref": [
				"Cf. A325016 (oriented), A325018 (chiral), A325019 (achiral), A325013 (up to k colors).",
				"Other n-dimensional polytopes: A007318(n,k-1) (simplex), A325009 (orthotope).",
				"Cf. A000048, A001037."
			],
			"keyword": "nonn,tabf",
			"offset": "1,4",
			"author": "_Robert A. Russell_, Jun 09 2019",
			"references": 9,
			"revision": 5,
			"time": "2019-06-11T06:09:32-04:00",
			"created": "2019-06-11T06:09:32-04:00"
		}
	]
}