{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A341838",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 341838,
			"data": "1,2,6,16,100,72,1764,768,39852,14400,1222100,277632,47179392,8754144,2319055200",
			"name": "Number of permutations of degree n with highest Shannon entropy.",
			"comment": [
				"Starting from a list of n ordered numbers, the sequence gives the number of permutations of said list with the highest Shannon entropy.",
				"For 0 \u003c n \u003c 4, the Shannon entropy is 0 for every permutation, while for n \u003e= 4 I found the maximum value of the entropy to be log(n) - log(10 - 6(-1)^n)/n. This holds for at least n=12, and I conjecture that it holds for n \u003e 12.",
				"Confirmed for n \u003c= 15. - _Hugo Pfoertner_, Feb 27 2021",
				"For n \u003e= 4, in the permutations with maximum entropy, the buckets always display one 2 and n-2 1's for even n, and two 2's and n-4 1's for odd n.",
				"n^2 divides a(n) for n \u003e= 4."
			],
			"link": [
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Entropy_(information_theory)\"\u003eShannon Entropy\u003c/a\u003e"
			],
			"example": [
				"Suppose we have an ordered list of n elements [1,2,3,4], whose entropy is 0, since all the differences are the same.",
				"If we consider a permutation, such as [3,4,2,1] the first step is to calculate the differences F(j+1) - F(j), where F(j) are the elements of the list. As for the final difference, we calculate F(1) - F(n). If any of the differences is negative, we add n to make it positive.",
				"The list of differences then becomes [1,2,3,2].",
				"The second step is to count the times each number appears in the list of differences, so 0 appears zero times, 1 appears one time, 2 appears two times, 3 appears one time and 4 zero times, so the grouped list becomes [1,2,1], since the zeros are omitted.",
				"The third and final step to calculate the entropy is to divide each of the numbers in the grouped list by n, thus obtaining p(1),p(2),...p(k) values, which sum to 1, and the entropy is given by E = -Sum_{j=1..k} (p(j)*log(p(j))).",
				"In this example we get a value of E = 1.0397207... for the permutation [3,4,2,1]."
			],
			"program": [
				"(PARI) histo(n, p) = my(d = vector(n, k, my(x = if (k\u003cn, p[k+1] - p[k], p[1] - p[n])); if (x\u003c0, x+n, x)), vd = Set(d)); vecsort(vector(#vd, k, #select(x-\u003e(x==vd[k]), d) / n));",
				"entr(v) = - sum(k=1, #v, v[k]*log(v[k]));",
				"a(n) = {if (n==1, return (1)); my(v = vector(n!), map = Map(), list = List()); for(i=1, n!, my(val = histo(n, numtoperm(n, i)), nb = 0); if (mapisdefined(map, val), nb = mapget(map, val), listput(list, val)); nb++; mapput(map, val, nb);); my(vlist = apply(entr, list), ind = 0, m = -oo); for (i=1, #vlist, if (vlist[i] \u003e m, m = vlist[i]; ind = i);); mapget(map, list[ind]);} \\\\ _Michel Marcus_, Feb 27 2021"
			],
			"xref": [
				"Cf. A002618 (with least instead of highest)."
			],
			"keyword": "nonn,hard,more",
			"offset": "1,2",
			"author": "_Andrea G. Amato_, Feb 24 2021",
			"ext": [
				"a(13)-a(14) from _Hugo Pfoertner_, Feb 27 2021",
				"a(15) from _Hugo Pfoertner_, Mar 01 2021"
			],
			"references": 1,
			"revision": 72,
			"time": "2021-03-01T16:08:49-05:00",
			"created": "2021-02-27T11:24:39-05:00"
		}
	]
}