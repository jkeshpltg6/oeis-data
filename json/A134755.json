{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A134755",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 134755,
			"data": "23,39,55,64,68,73,80,84,91,96,100,105,109,113,114,118,122,123,127,131,132,136,140,140,144,145,145,149,149,153,154,156,158,160,163,164,167,168,168,172,172,176,176,176,180,180,181,181,185,185,185,189,189,190",
			"name": "Minimal number such that all greater numbers can be written as sums of squares of primes in more than n ways.",
			"comment": [
				"The sequence is well-defined, in that a(n) exists for all n\u003e=0. Proof by induction: a(0) exists. We set b(j):=number of ways to write j as sum of squares of primes (=A090677). If a(n) exists, then b(j)\u003en for all j\u003ea(n). Setting m:=a(n)+1, we find that there are n+1 sum of squares of primes B(0,i), 1\u003c=i\u003c=n+1, with m=B(0,i).",
				"Further there are n+1 such sum expressions B(1,i), B(2,i) and B(3,i), 1\u003c=i\u003c=n+1, representing m+1, m+2 and m+3, respectively. For all j\u003ea(n) we have j=m+4*floor((j-m)/4)+(j-m) mod 4. Thus j=m+r+s*2^2, where r=0,1,2 or 3. Hence n can be written B(r,i)+s*2^2 and there are n+1 such representations.",
				"Let q be the maximal prime number (to be squared) occurring as a term within those sum expressions B(r,i), 0\u003c=r\u003c=3,1\u003c=i\u003c=n+1. We select a prime number p\u003eq and we set c:=a(n)+p^2. For j\u003ec, we have the n+1 representations B(r(j),i)+s(j)*2^2. Additionally, for j-p^2 (which is \u003ea(n)) there are also n+1 representations B(r_p,i)+s_p*2^2, where r_p:=r(j-p^2), s_p:=s(j-p^2).",
				"Thus j can be written B(r(j),i)+s(j)*2^2, 1\u003c=i\u003c=n+1 and B(r_p,i)+s_p*2^2+p^2, 1\u003c=i\u003c=n+1. By choice of p all these sum representations of j are different, which implies, that there are 2n+2 such representations. It follows b(j)\u003e2n+2\u003en+1 for all j\u003ec, which implies, that a(n+1) exists."
			],
			"formula": [
				"a(n)=min( m | A090677(j)\u003en for all j\u003em)."
			],
			"example": [
				"a(0)=23, since numbers \u003e23 can be written as sum of squares of primes.",
				"a(1)=39, since there are at least two ways, to write a number \u003e39 as a sum of squares of primes."
			],
			"xref": [
				"Cf. A078134, A078135, A078136, A078139, A090677, A078137, A134754."
			],
			"keyword": "nonn",
			"offset": "0,1",
			"author": "_Hieronymus Fischer_, Nov 11 2007",
			"references": 7,
			"revision": 5,
			"time": "2012-03-31T13:21:04-04:00",
			"created": "2007-12-09T03:00:00-05:00"
		}
	]
}