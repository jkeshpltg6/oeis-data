{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A279185",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 279185,
			"data": "1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,1,1,1,1,1,1,1,1,1,1,1,2,1,2,2,1,2,1,1,1,1,1,1,1,1,1,1,1,1,1,4,4,4,4,4,4,4,4,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,1,2,2,1,2,2,2,1",
			"name": "Triangle read by rows: T(n,k) (n\u003e=1, 0 \u003c=k\u003c=n-1) is the length of the period of the sequence obtained by starting with k and repeatedly squaring mod n.",
			"comment": [
				"Fix n. Start with k (0 \u003c= k \u003c= n-1) and repeatedly square and reduce mod n until this repeats; T(n,k) is the length of the cycle that is reached.",
				"A279186 gives maximal entry in each row.",
				"A037178 gives maximal entry in row p, p = n-th prime.",
				"A279187 gives maximal entry in row c, c = n-th composite number.",
				"A279188 gives maximal entry in row c, c = prime(n)^2.",
				"A256608 gives LCM of entries in row n.",
				"A256607 gives T(2,n)."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A279185/b279185.txt\"\u003eTable of n, a(n) for n = 1..10011\u003c/a\u003e (rows 1 to 141, flattened)",
				"Haifeng Xu, \u003ca href=\"http://arxiv.org/abs/1601.06509\"\u003eThe largest cycles consist by the quadratic residues and Fermat primes\u003c/a\u003e, arXiv:1601.06509 [math.NT], 2016."
			],
			"example": [
				"The triangle begins:",
				"1,",
				"1,1,",
				"1,1,1,",
				"1,1,1,1,",
				"1,1,1,1,1,",
				"1,1,1,1,1,1,",
				"1,1,2,2,2,2,1,",
				"1,1,1,1,1,1,1,1,",
				"1,1,2,1,2,2,1,2,1,",
				"1,1,1,1,1,1,1,1,1,1,",
				"1,1,4,4,4,4,4,4,4,4,1,",
				"...",
				"For example, if n=11 and k=2, repeatedly squaring mod 11 gives the sequence 2, 4, 5, 3, 9, 4, 5, 3, 9, 4, 5, 3, ..., which has period T(11,2) = 4."
			],
			"maple": [
				"A279185 := proc(k,n) local g,y,r;",
				"  if k = 0 then return 1 fi;",
				"  y:= n;",
				"  g:= igcd(k,y);",
				"  while g \u003e 1 do",
				"     y:= y/g;",
				"     g:= igcd(k,y);",
				"  od;",
				"  if y = 1 then return 1 fi;",
				"  r:= numtheory:-order(k,y);",
				"  r:= r/2^padic:-ordp(r,2);",
				"  if r = 1 then return 1 fi;",
				"  numtheory:-order(2,r)",
				"end proc:",
				"seq(seq(A279185(k,n),k=0..n-1),n=1..20); # _Robert Israel_, Dec 14 2016"
			],
			"mathematica": [
				"T[n_, k_] := Module[{g, y, r}, If[k == 0, Return[1]]; y = n; g = GCD[k, y]; While[g \u003e 1, y = y/g; g = GCD[k, y]]; If[y == 1, Return[1]]; r = MultiplicativeOrder[k, y]; r = r/2^IntegerExponent[r, 2]; If[r == 1,  Return[1]]; MultiplicativeOrder[2, r]];",
				"Table[T[n, k], {n, 1, 13}, {k, 0, n-1}] // Flatten (* _Jean-François Alcover_, Nov 27 2017, after _Robert Israel_ *)"
			],
			"xref": [
				"Cf. A037178, A256607, A256608, A279186, A279187, A279188.",
				"See also A141305, A279189, A279190, A279191, A279192."
			],
			"keyword": "nonn,tabl",
			"offset": "1,24",
			"author": "_N. J. A. Sloane_, Dec 14 2016",
			"references": 11,
			"revision": 34,
			"time": "2017-11-27T08:17:32-05:00",
			"created": "2016-12-14T12:14:17-05:00"
		}
	]
}