{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002846",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2846,
			"id": "M1251 N0478",
			"data": "1,1,1,2,4,11,33,116,435,1832,8167,39700,201785,1099449,6237505,37406458,232176847,1513796040,10162373172,71158660160,511957012509,3819416719742,29195604706757,230713267586731,1861978821637735,15484368121967620,131388840051760458",
			"name": "Number of ways of transforming a set of n indistinguishable objects into n singletons via a sequence of n-1 refinements.",
			"comment": [
				"Construct the ranked poset L(n) whose nodes are the A000041(n) partitions of n, with all the partitions into the same number of parts having the same rank. A partition into k parts is joined to a partition into k+1 parts if the latter is a refinement of the former.",
				"The partition n^1 is at the left and the partition 1^n at the right. The illustration by _Olivier Gérard_ shows the posets L(2) through L(8).",
				"Then a(n) is the number of paths of length n-1 in L(n) that join n^1 to 1^n.",
				"Stated another way, a(n) is the number of maximal chains in the ranked poset L(n). (This poset is not a lattice for n \u003e 4.) - Comments corrected by _Gus Wiseman_, May 01 2016"
			],
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A002846/b002846.txt\"\u003eTable of n, a(n) for n = 1..80\u003c/a\u003e",
				"P. Erdős, R. K. Guy and J. W. Moon, \u003ca href=\"http://jlms.oxfordjournals.org/content/s2-9/4/565.extract\"\u003eOn refining partitions\u003c/a\u003e, J. London Math. Soc., 9 (1975), 565-570.",
				"R. K. Guy, Letter to N. J. A. Sloane, June 24 1971: \u003ca href=\"/A002572/a002572.jpg\"\u003efront\u003c/a\u003e, \u003ca href=\"/A002572/a002572_1.jpg\"\u003eback\u003c/a\u003e [Annotated scanned copy, with permission]",
				"Olivier Gérard, \u003ca href=\"/A002846/a002846.png\"\u003eThe ranked posets L(2),...,L(8)\u003c/a\u003e",
				"Gus Wiseman, \u003ca href=\"http://imgur.com/a/UYxDJ\"\u003eHasse Diagrams of Partition Refinement Posets n=1..9\u003c/a\u003e",
				"Gus Wiseman, \u003ca href=\"/A002846/a002846_1.png\"\u003eHasse Diagrams of Partition Refinement Posets n=1..9, Version 1\u003c/a\u003e, [Cached copy, with permission]",
				"Gus Wiseman, \u003ca href=\"/A002846/a002846_2.png\"\u003eHasse Diagrams of Partition Refinement Posets n=1..9, Version 2\u003c/a\u003e, [Cached copy, with permission]"
			],
			"example": [
				"a(5) = 4 because there are 4 paths from top to bottom in this lattice:",
				"  .",
				"       ooooo",
				"     /      \\",
				"  o.oooo   oo.ooo",
				"    |    X    |",
				"  o.o.ooo  o.oo.oo",
				"     \\       /",
				"      o.o.o.oo",
				"          |",
				"      o.o.o.o.o",
				"  .",
				"(This is the ranked poset L(5), but drawn vertically rather than horizontally.)"
			],
			"maple": [
				"v:= l-\u003e [seq(`if`(i=1 or l[i]\u003el[i-1], seq(subs(1=[][], sort(subsop(",
				"         i=[j, l[i]-j][], l))), j=1..l[i]/2), [][]), i=1..nops(l))]:",
				"b:= proc(l) option remember; `if`(max(l)\u003c2, 1, add(b(h), h=v(l))) end:",
				"a:= n-\u003e b([n]):",
				"seq(a(n), n=1..30);  # _Alois P. Heinz_, Sep 22 2019"
			],
			"mathematica": [
				"\u003c\u003cposets.m Table[Build[NumP[n], np]; Last@MaximalChainsDown@np, {n, 1, 25}] (* _Mitch Harris_, Jan 19 2006 *)"
			],
			"program": [
				"(Sage) def A002846(n): return Posets.IntegerPartitions(n).chain_polynomial().leading_coefficient()  # _Max Alekseyev_, Dec 23 2015"
			],
			"xref": [
				"See A213242, A213385, A213427 for related sequences, A327643."
			],
			"keyword": "nonn,nice",
			"offset": "1,4",
			"author": "_N. J. A. Sloane_. Entry revised by _N. J. A. Sloane_, Jun 11 2012",
			"ext": [
				"a(17)-a(25) from _Mitch Harris_, Jan 19 2006"
			],
			"references": 43,
			"revision": 72,
			"time": "2019-09-23T10:10:52-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}