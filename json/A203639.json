{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A203639",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 203639,
			"data": "1,1,1,4,1,1,1,12,6,1,1,4,1,1,1,32,1,6,1,4,1,1,1,12,10,1,27,4,1,1,1,80,1,1,1,24,1,1,1,12,1,1,1,4,6,1,1,32,14,10,1,4,1,27,1,12,1,1,1,4,1,1,6,192,1,1,1,4,1,1,1,72,1,1,10,4,1,1,1,32,108,1,1,4,1,1,1,12,1,6,1,4,1,1,1,80,1,14,6,40",
			"name": "Multiplicative with a(p^e) = e*p^(e-1).",
			"comment": [
				"a(n)=1 for all squarefree n."
			],
			"reference": [
				"Krassimir Atanassov, New integer functions, related to φ and σ functions, Bull. Number Theory Related Topics, Vol. 11, No. 1 (1987), pp. 3-26."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A203639/b203639.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"G. L. Cohen, D. E. Iannucci, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL6/Cohen/cohen6.html\"\u003eDerived Sequences\u003c/a\u003e, J. Int. Seq. 6 (2003) #03.1.1",
				"Vaclav Kotesovec, \u003ca href=\"/A203639/a203639.jpg\"\u003eGraph - the asymptotic ratio (500000000 terms)\u003c/a\u003e.",
				"J. Sandor, B. Crstici, \u003ca href=\"http://cdsweb.cern.ch/record/829127\"\u003eHandbook of Number Theory II\u003c/a\u003e, Kluwer, 2004, page 337."
			],
			"formula": [
				"a(n) = n*A005361(n)/A007947(n).",
				"Dirichlet g.f.: zeta^2(s-1)*product_{primes p} (1-2*p^(1-s)+p^(2-2s)+p^(-s)). - _R. J. Mathar_, Jan 19 2012",
				"a(n) = A005361(n)*A003557(n). - _Vaclav Kotesovec_, Jun 20 2020",
				"Conjecture: Sum_{k=1..n} a(k) = O(n^(5/4) * log(n)). - _Vaclav Kotesovec_, Jun 22 2020"
			],
			"maple": [
				"A203639 := proc(n) local a,f,e ; a :=1; for f in ifactors(n)[2] do e := op(2,f) ; p := op(1,f) ; a := a*e*p^(e-1) ; end do; a; end proc; # _R. J. Mathar_, Jan 11 2012"
			],
			"mathematica": [
				"Table[n*Times @@ Transpose[FactorInteger[n]][[2]] / Last[Select[Divisors[n], SquareFreeQ]], {n, 1, 100}] (* _Vaclav Kotesovec_, Dec 18 2019 *)",
				"f[p_, e_] := e*p^(e-1); a[1] = 1; a[n_] := Times @@ f @@@ FactorInteger[n]; Array[a, 100] (* _Amiram Eldar_, Sep 21 2020 *)"
			],
			"program": [
				"(PARI) a(n)=my(f=factor(n)); n*prod(i=1,#f~, f[i,2]/f[i,1]) \\\\ _Charles R Greathouse IV_, Dec 09 2016",
				"(PARI) for(n=1, 100, print1(direuler(p=2, n, (1 + X/(1 - p*X)^2))[n], \", \")) \\\\ _Vaclav Kotesovec_, Jun 14 2020",
				"(Scheme, with memoization-macro definec)",
				"(definec (A203639 n) (if (= 1 n) n (* (A067029 n) (expt (A020639 n) (+ -1 (A067029 n))) (A203639 (A028234 n)))))",
				";; _Antti Karttunen_, Sep 13 2017"
			],
			"xref": [
				"Cf. A005361, A007947, A203640 (cycles)."
			],
			"keyword": "nonn,mult,easy",
			"offset": "1,4",
			"author": "_R. J. Mathar_, Jan 04 2012",
			"ext": [
				"Terms a(1)-a(24) confirmed and terms a(25)-a(100) added by _John W. Layman_, Jan 04 2012"
			],
			"references": 2,
			"revision": 41,
			"time": "2020-09-21T03:45:58-04:00",
			"created": "2012-01-05T15:55:07-05:00"
		}
	]
}