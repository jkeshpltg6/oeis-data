{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A272148",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 272148,
			"data": "3,5,27,-11,63,-23,115,-87,179,-151,299,-227,371,-303,535,-487,715,-655,843,-783,1079,-951,1251,-1127,1435,-1387,1635,-1479,1903,-1623,1931,-1819,2435,-2239,2579,-2507,3159,-3099,3611,-3415,4059,-3827,4159,-4015",
			"name": "First differences of number of active (ON,black) cells in n-th stage of growth of two-dimensional cellular automaton defined by \"Rule 433\", based on the 5-celled von Neumann neighborhood.",
			"comment": [
				"Initialized with a single black (ON) cell at stage zero."
			],
			"reference": [
				"S. Wolfram, A New Kind of Science, Wolfram Media, 2002; p. 170."
			],
			"link": [
				"Robert Price, \u003ca href=\"/A272148/b272148.txt\"\u003eTable of n, a(n) for n = 0..127\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"http://arxiv.org/abs/1503.01168\"\u003eOn the Number of ON Cells in Cellular Automata\u003c/a\u003e, arXiv:1503.01168 [math.CO], 2015",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ElementaryCellularAutomaton.html\"\u003eElementary Cellular Automaton\u003c/a\u003e",
				"S. Wolfram, \u003ca href=\"http://wolframscience.com/\"\u003eA New Kind of Science\u003c/a\u003e",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_2D_5-Neighbor_Cellular_Automata\"\u003eIndex to 2D 5-Neighbor Cellular Automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_Elementary_Cellular_Automata\"\u003eIndex to Elementary Cellular Automata\u003c/a\u003e"
			],
			"mathematica": [
				"CAStep[rule_,a_]:=Map[rule[[10-#]]\u0026,ListConvolve[{{0,2,0},{2,1,2},{0,2,0}},a,2],{2}];",
				"code=433; stages=128;",
				"rule=IntegerDigits[code,2,10];",
				"g=2*stages+1; (* Maximum size of grid *)",
				"a=PadLeft[{{1}},{g,g},0,Floor[{g,g}/2]]; (* Initial ON cell on grid *)",
				"ca=a;",
				"ca=Table[ca=CAStep[rule,ca],{n,1,stages+1}];",
				"PrependTo[ca,a];",
				"(* Trim full grid to reflect growth by one cell at each stage *)",
				"k=(Length[ca[[1]]]+1)/2;",
				"ca=Table[Table[Part[ca[[n]][[j]],Range[k+1-n,k-1+n]],{j,k+1-n,k-1+n}],{n,1,k}];",
				"on=Map[Function[Apply[Plus,Flatten[#1]]],ca] (* Count ON cells at each stage *)",
				"Table[on[[i+1]]-on[[i]],{i,1,Length[on]-1}] (* Difference at each stage *)"
			],
			"xref": [
				"Cf. A272145."
			],
			"keyword": "sign,easy",
			"offset": "0,1",
			"author": "_Robert Price_, Apr 21 2016",
			"references": 1,
			"revision": 8,
			"time": "2016-04-22T11:39:18-04:00",
			"created": "2016-04-22T02:37:00-04:00"
		}
	]
}