{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A252464",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 252464,
			"data": "0,1,2,2,3,3,4,3,3,4,5,4,6,5,4,4,7,4,8,5,5,6,9,5,4,7,4,6,10,5,11,5,6,8,5,5,12,9,7,6,13,6,14,7,5,10,15,6,5,5,8,8,16,5,6,7,9,11,17,6,18,12,6,6,7,7,19,9,10,6,20,6,21,13,5,10,6,8,22,7,5,14,23,7,8,15,11,8,24,6,7,11,12,16,9,7,25,6,7,6,26,9,27",
			"name": "a(1) = 0, a(2n) = 1 + a(n), a(2n+1) = 1 + a(A064989(2n+1)); also binary width of terms of A156552 and A243071.",
			"comment": [
				"a(n) tells how many iterations of A252463 are needed before 1 is reached, i.e., the distance of n from 1 in binary trees like A005940 and A163511.",
				"Similarly for A253553 in trees A253563 and A253565. - _Antti Karttunen_, Apr 14 2019"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A252464/b252464.txt\"\u003eTable of n, a(n) for n = 1..8192\u003c/a\u003e"
			],
			"formula": [
				"a(1) = 0; for n \u003e 1: a(n) = 1 + a(A252463(n)).",
				"a(n) = A029837(1+A243071(n)). [a(n) = binary width of terms of A243071.]",
				"a(n) = A029837(A005941(n)) = A029837(1+A156552(n)). [Also binary width of terms of A156552.]",
				"Other identities. For all n \u003e= 1:",
				"a(A000040(n)) = n.",
				"a(A001248(n)) = n+1.",
				"a(A030078(n)) = n+2.",
				"And in general, a(prime(n)^k) = n+k-1.",
				"a(A000079(n)) = n. [I.e., a(2^n) = n.]",
				"For all n \u003e= 2:",
				"a(n) = A001222(n) + A061395(n) - 1 = A001222(n) + A252735(n) = A061395(n) + A252736(n) = 1 + A252735(n) + A252736(n).",
				"a(n) = A325134(n) - 1. - _Gus Wiseman_, Apr 02 2019",
				"From _Antti Karttunen_, Apr 14 2019: (Start)",
				"a(1) = 0; for n \u003e 1: a(n) = 1 + a(A253553(n)).",
				"a(n) = A001221(n) + A297167(n) = A297113(n) + A297155(n).",
				"(End)."
			],
			"example": [
				"From _Gus Wiseman_, Apr 02 2019: (Start)",
				"The Heinz number of an integer partition (y_1,...,y_k) is prime(y_1)*...*prime(y_k), so a(n) is the size of the inner lining of the integer partition with Heinz number n, which is also the size of the largest hook of the same partition. For example, the partition with Heinz number 715 is (6,5,3), with diagram",
				"  o o o o o o",
				"  o o o o o",
				"  o o o",
				"which has inner lining",
				"          o o",
				"      o o o",
				"  o o o",
				"and largest hook",
				"  o o o o o o",
				"  o",
				"  o",
				"both of which have size 8, so a(715) = 8.",
				"(End)"
			],
			"mathematica": [
				"Table[If[n==1,1,PrimeOmega[n]+PrimePi[FactorInteger[n][[-1,1]]]]-1,{n,100}] (* _Gus Wiseman_, Apr 02 2019 *)"
			],
			"program": [
				"(Scheme, two different versions)",
				";; Memoization-macro definec can be found from _Antti Karttunen_'s IntSeq-library",
				"(definec (A252464 n) (if (\u003c= n 1) 0 (+ 1 (A252464 (A252463 n)))))",
				"(define (A252464 n) (A029837 (+ 1 (A243071 n))))",
				"(define (A252464 n) (A029837 (A005941 n)))",
				"(PARI)",
				"A061395(n) = if(n\u003e1, primepi(vecmax(factor(n)[, 1])), 0);",
				"A252464(n) = (bigomega(n) + A061395(n) - 1); \\\\ _Antti Karttunen_, Apr 14 2019"
			],
			"xref": [
				"Cf. A000040, A000079, A001221, A001222, A005940, A029837, A061395, A156552 (A005941), A163511, A243071, A252461, A252463, A252735, A252736, A252759, A253553, A253563, A253565, A297113, A297155, A297167, A324870, A324872.",
				"Right edge of irregular triangle A265146.",
				"Cf. also A246348.",
				"Cf. A052126, A056239, A093641, A112798, A257990, A325133, A325134, A325135."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Antti Karttunen_, Dec 20 2014",
			"references": 49,
			"revision": 40,
			"time": "2019-04-24T10:10:54-04:00",
			"created": "2014-12-23T00:37:04-05:00"
		}
	]
}