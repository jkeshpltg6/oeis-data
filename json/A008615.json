{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A008615",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 8615,
			"data": "0,0,1,0,1,1,1,1,2,1,2,2,2,2,3,2,3,3,3,3,4,3,4,4,4,4,5,4,5,5,5,5,6,5,6,6,6,6,7,6,7,7,7,7,8,7,8,8,8,8,9,8,9,9,9,9,10,9,10,10,10,10,11,10,11,11,11,11,12,11,12,12,12,12,13,12,13,13,13,13,14,13,14,14,14,14,15,14",
			"name": "a(n) = floor(n/2) - floor(n/3).",
			"comment": [
				"If the two leading 0's are dropped, this becomes the essentially identical sequence A103221, with g.f. 1/((1-x^2)*(1-x^3)), which arises in many contexts. For example, 1/((1-x^4)*(1-x^6)) is the Poincaré series [or Poincare series] for modular forms of weight w for the full modular group. As generators one may take the Eisenstein series E_4 (A004009) and E_6 (A013973).",
				"Dimension of the space of weight 2n+8 cusp forms for Gamma_0( 1 ).",
				"Apart from initial term(s), dimension of the space of weight 2n cuspidal newforms for Gamma_0( 5 ).",
				"a(n) is the number of ways n can be written as the sum of a positive even number and a nonnegative multiple of 3 and so the number of ways (n-2) can be written as the sum of a nonnegative even number and a nonnegative multiple of 3 and also the number of ways (n+3) can be written as the sum of a positive even number and a positive multiple of 3.",
				"a(A016933(n)) = a(A016957(n)) = a(A016969(n)) = n+1. - _Reinhard Zumkeller_, Feb 27 2008",
				"a(A008588(n)) = a(A016921(n)) = a(A016945(n)) = n. - _Reinhard Zumkeller_, Feb 27 2008",
				"It appears that this is also the number of partitions of 2n+6 that are 4-term arithmetic progressions. - _John W. Layman_, May 01 2009 [verified by _Wesley Ivan Hurt_, Jan 17 2021]",
				"a(n) is the number of (n+3)-digit fixed points under the base-3 Kaprekar map A164993 (see A164997 for the list of fixed points). - _Joseph Myers_, Sep 04 2009",
				"Starting from n=10 also the number of balls in new consecutive hexagonal edges, if an (infinite) chain of balls is winded spirally around the first ball at the center, such that each six steps make an entire winding. - _K. G. Stier_, Dec 21 2012",
				"In any three consecutive terms exactly two of them are equal to each other. - _Michael Somos_, Mar 01 2014",
				"Number of partitions of (n-2) into parts 2 and 3. - _David Neil McGrath_, Sep 05 2014",
				"a(n), n \u003e= 0, is also the dimension of S_{2*(n+4)}, the complex vector space of modular cusp forms of weight 2*(n+4) and level 1 (full modular group). The dimension of S_0, S_2, S_4 and S_6 is 0. See, e.g., Ash and Gross, p. 178. Table 13.1. - _Wolfdieter Lang_, Sep 16 2016",
				"From _Wolfdieter Lang_, May 08 2017: (Start)",
				"a(n-2) = floor((n-2)/2) - floor((n-2)/3) = floor(n/2) - floor((n+1)/3) is for n \u003e=0 the number of integers k in the interval (n+1)/3 \u003c k \u003c= floor(n/2). This problem appears in the computation of the number of zeros of Chebyshev S(n, x) polynomials (coefficients in A049310) in the open interval (-1, +1). See a comment there. This computation was motivated by a conjecture given in A008611 by _Michel Lagneau_, Mar 31 2017.",
				"a(n) is also the number of integers k in the closed interval (n+1)/3 \u003c= k \u003c= floor(n/2), which is floor(n/2) - (ceiling((n+1)/3) - 1) for n \u003e= 0 (proof trivial for n+1 == 0 (mod 3) and otherwise). From the preceding statement this a(n) is also a(n-2) + [n == 2 (mod 3)] for n \u003e= 0 (with [statement] = 1 if the statement is true and zero otherwise). This proves the recurrence given by _Michael Somos_ in the formula section. (End)",
				"Assuming the Collatz conjecture to be true, for n \u003e 1, a(n+7) is the row length of the n-th row of A340985. That is, the number of weakly connected components of the Collatz digraph of order n. - _Sebastian Karlsson_, Feb 23 2021"
			],
			"reference": [
				"Avner Ash and Robert Gross, Summing it up, Princeton  University Press, 2016, p. 178.",
				"D. J. Benson, Polynomial Invariants of Finite Groups, Cambridge, 1993, p. 100.",
				"E. Freitag, Siegelsche Modulfunktionen, Springer-Verlag, Berlin, 1983; p. 141, Th. 1.1.",
				"R. C. Gunning, Lectures on Modular Forms. Princeton Univ. Press, Princeton, NJ, 1962.",
				"J.-M. Kantor, Où en sont les mathématiques, La formule de Molien-Weyl, SMF, Vuibert, p. 79"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A008615/b008615.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"David Broadhurst, \u003ca href=\"http://arxiv.org/abs/1604.03057\"\u003eFeynman integrals, L-series and Kloosterman moments\u003c/a\u003e, arXiv:1604.03057 [physics.gen-ph], 2016. See Cor. 1.",
				"J. Igusa, \u003ca href=\"http://www.jstor.org/stable/2373172\"\u003eOn Siegel modular forms of genus 2 (II)\u003c/a\u003e, Amer. J. Math., 86 (1964), 392-412, esp. p. 402.",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=212\"\u003eEncyclopedia of Combinatorial Structures 212\u003c/a\u003e",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=448\"\u003eEncyclopedia of Combinatorial Structures 448\u003c/a\u003e",
				"Clark Kimberling, \u003ca href=\"https://www.emis.de/journals/JIS/VOL22/Kimberling/kimb9.html\"\u003eA Combinatorial Classification of Triangle Centers on the Line at Infinity\u003c/a\u003e, J. Int. Seq., Vol. 22 (2019), Article 19.5.4.",
				"T. Shioda, \u003ca href=\"http://www.jstor.org/stable/2373415\"\u003eOn the graded ring of invariants of binary octavics\u003c/a\u003e, Amer. J. Math. 89, 1022-1046, 1967.",
				"William A. Stein, \u003ca href=\"http://wstein.org/Tables/\"\u003eThe modular forms database\u003c/a\u003e",
				"James Tanton, \u003ca href=\"http://www.jamestanton.com/?p=1413\"\u003eInteger Triangles\u003c/a\u003e, Chapter 11 in \"Mathematics Galore!\" (MAA, 2012).",
				"James Tanton, \u003ca href=\"http://www.maa.org/sites/default/files/pdf/pubs/mayjun02web.pdf\"\u003eYoung students approach integer triangles\u003c/a\u003e, FOCUS 22 no. 5 (2002), 4 - 6.",
				"James Tanton et al., \u003ca href=\"http://www.themathcircle.org/focusarticle.php\"\u003eYoung students approach integer triangles\u003c/a\u003e, FOCUS 22 no. 5 (2002), 4 - 6.",
				"\u003ca href=\"/index/Mo#Molien\"\u003eIndex entries for Molien series\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,1,1,0,-1)."
			],
			"formula": [
				"a(n) = a(n-6) + 1 = a(n-2) + a(n-3) - a(n-5). - _Henry Bottomley_, Sep 02 2000",
				"G.f.: x^2 / ((1-x^2) * (1-x^3)).",
				"a(6*k) = k, k \u003e= 0. - _Zak Seidov_, Sep 09 2012",
				"a(n) = A005044(n+1) - A005044(n-3). - _Johannes W. Meijer_, Oct 18 2013",
				"a(n) = floor((n+4)/6) - floor((n+3)/6) + floor((n+2)/6). - _Mircea Merca_, Nov 27 2013",
				"Euler transform of length 3 sequence [0, 1, 1]. - _Michael Somos_, Mar 01 2014",
				"a(n+2) = a(n) + 1 if n == 0 (mod 3), a(n+2) = a(n) otherwise. - _Michael Somos_, Mar 01 2014. See the May 08 2017 comment above. - _Wolfdieter Lang_, May 08 2017",
				"a(n) = -a(-1 - n) for all n in Z. - _Michael Somos_, Mar 01 2014.",
				"a(n) = A004526(n) - A002264(n). - _Reinhard Zumkeller_, Apr 28 2014",
				"a(n) = Sum_{i=0..n-2} (floor(i/6)-floor((i-3)/6))*(-1)^i. - _Wesley Ivan Hurt_, Sep 08 2015",
				"a(n) = a(n+6) - 1 = A103221(n+4) - 1, n \u003e= 0. - _Wolfdieter Lang_, Sep 16 2016",
				"12*a(n) = 2*n +1 +3*(-1)^n -4*A057078(n). - _R. J. Mathar_, Jun 19 2019",
				"a(n) = Sum_{k=1..floor((n+3)/2)} Sum_{j=k..floor((2*n+6-k)/3)} Sum_{i=j..floor((2*n+6-j-k)/2)} ([j-k = i-j = 2*n+6-2*i-j-k] - [k = j = i = 2*n+6-i-j-k]), where [ ] is the (generalized) Iverson bracket. - _Wesley Ivan Hurt_, Jan 17 2021"
			],
			"example": [
				"G.f. = x^2 + x^4 + x^5 + x^6 + x^7 + 2*x^8 + x^9 + 2*x^10 + 2*x^11 + 2*x^12 + ..."
			],
			"maple": [
				"a := n-\u003e floor(n/2) - floor(n/3): seq(a(n), n = 0 .. 87);"
			],
			"mathematica": [
				"a[n_]:=Floor[n/2]-Floor[n/3]; Array[a,90,0] (* _Vladimir Joseph Stephan Orlovsky_, Dec 05 2008; corrected by _Harvey P. Dale_, Nov 30 2011 *)",
				"LinearRecurrence[{0, 1, 1, 0, -1}, {0, 0, 1, 0, 1}, 100]; // _Vincenzo Librandi_, Sep 09 2015"
			],
			"program": [
				"(PARI) {a(n) = (n\\2) - (n\\3)}; /* _Michael Somos_, Feb 06 2003 */",
				"(MAGMA) [Floor(n/2)-Floor(n/3): n in [0..10]]; // Sergei Haller (sergei(AT)sergei-haller.de), Dec 21 2006",
				"(MAGMA) a := func\u003c n | n lt 2 select 0 else n eq 2 select 1 else Dimension( ModularForms( PSL2( Integers()), 2*n-4))\u003e; /* _Michael Somos_, Dec 11 2018 */",
				"(Haskell)",
				"a008615 n = n `div` 2 - n `div` 3  -- _Reinhard Zumkeller_, Apr 28 2014"
			],
			"xref": [
				"Essentially the same as A103221.",
				"First differences of A069905 (and A001399)."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,9",
			"author": "_N. J. A. Sloane_, _Simon Plouffe_",
			"references": 52,
			"revision": 136,
			"time": "2021-03-19T18:17:07-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}