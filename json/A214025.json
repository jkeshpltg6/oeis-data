{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A214025",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 214025,
			"data": "13,10,8,77,51,38,68,36,20,330,266,248,300,145,96,1580,1381,1365,1414,813,652,1402,596,432,7678,6630,6357,6630,3968,3192,6357,3192,2828,35971,30070,27638,30709,18037,13744,27591,14507,13851,26574,15318,17846",
			"name": "Irregular array T(n,k) of the numbers of non-extendable (complete) non-self-adjacent simple paths starting at each of a minimal subset of nodes within a square lattice bounded by rectangles with nodal dimensions n and 6, n \u003e= 2.",
			"comment": [
				"The subset of nodes is contained in the top left-hand quarter of the rectangle and has nodal dimensions floor((n+1)/2) and 3 to capture all geometrically distinct counts.",
				"The quarter-rectangle is read by rows.",
				"The irregular array of numbers is:",
				"....k......1.....2.....3.....4.....5.....6.....7.....8.....9....10....11....12",
				"..n",
				"..2.......13....10.....8",
				"..3.......77....51....38....68....36....20",
				"..4......330...266...248...300...145....96",
				"..5.....1580..1381..1365..1414...813...652..1402...596...432",
				"..6.....7678..6630..6357..6630..3968..3192..6357..3192..2828",
				"..7....35971.30070.27638.30709.18037.13744.27591.14507.13851.26574.15318.17846",
				"where k indicates the position of the start node in the quarter-rectangle.",
				"For each n, the maximum value of k is 3*floor((n+1)/2).",
				"Reading this array by rows gives the sequence."
			],
			"link": [
				"C. H. Gribble, \u003ca href=\"https://oeis.org/wiki/Complete_non-self-adjacent_paths:Results_for_Square_Lattice\"\u003eComputed characteristics of complete non-self-adjacent paths in a square lattice bounded by various sizes of rectangle.\u003c/a\u003e",
				"C. H. Gribble, \u003ca href=\"https://oeis.org/wiki/Complete non-self-adjacent paths:Program\"\u003eComputes characteristics of complete non-self-adjacent paths in square and cubic lattices bounded by various sizes of rectangle and rectangular cuboid respectively.\u003c/a\u003e"
			],
			"example": [
				"When n = 2, the number of times (NT) each node in the rectangle is the start node (SN) of a complete non-self-adjacent simple path is",
				"SN  0  1  2  3  4  5",
				"    6  7  8  9 10 11",
				"NT 13 10  8  8 10 13",
				"   13 10  8  8 10 13",
				"To limit duplication, only the top left-hand corner 13 and the 10 and 8 to its right are stored in the sequence, i.e. T(2,1) = 13, T(2,2) = 10 and T(2,3) = 8."
			],
			"xref": [
				"Cf. A213106, A213249, A213375, A213478, A213954, A214022, A214023"
			],
			"keyword": "nonn,tabf",
			"offset": "2,1",
			"author": "_Christopher Hunt Gribble_, Jul 01 2012",
			"references": 5,
			"revision": 12,
			"time": "2012-07-03T15:55:31-04:00",
			"created": "2012-07-03T15:55:31-04:00"
		}
	]
}