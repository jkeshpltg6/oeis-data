{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A262323",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 262323,
			"data": "1,10,11,12,2,20,22,21,13,3,23,30,33,31,14,4,24,32,25,5,15,41,16,6,26,42,27,7,17,51,18,8,28,52,29,9,19,61,36,43,34,40,44,45,50,35,53,37,63,38,73,39,83,48,54,46,60,56,55,57,65,58,75,47,64,49,74",
			"name": "Lexicographically earliest sequence of distinct terms such that the decimal representations of two consecutive terms overlap.",
			"comment": [
				"Two terms are said to overlap:",
				"- if the decimal representation of one term is contained in the decimal representation of the other term (for example, 12 and 2 overlap),",
				"- or if, for some k\u003e0, the first k decimal digits (without leading zero) of one term correspond to the k last decimal digits of the other term (for example, 1017 and 1101 overlap).",
				"This sequence is a permutation of the positive integers, with inverse A262255.",
				"The first overlap involving 1 digit occurs between a(1)=1 and a(2)=10.",
				"The first overlap involving 2 digits occurs between a(108)=100 and a(109)=110.",
				"The first overlap involving 3 digits occurs between a(1039)=1017 and a(1040)=1101.",
				"The first overlap involving 4 digits occurs between a(10584)=10212 and a(10585)=11021."
			],
			"link": [
				"Paul Tek, \u003ca href=\"/A262323/b262323.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Paul Tek, \u003ca href=\"/A262323/a262323.pl.txt\"\u003ePERL program for this sequence\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"example": [
				"The first terms of the sequence are:",
				"+----+---------+",
				"| n  | a(n)    |",
				"+----+---------+",
				"|  1 |  1      |",
				"|  2 |  10     |",
				"|  3 | 11      |",
				"|  4 |  12     |",
				"|  5 |   2     |",
				"|  6 |   20    |",
				"|  7 |  22     |",
				"|  8 |   21    |",
				"|  9 |    13   |",
				"| 10 |     3   |",
				"| 11 |    23   |",
				"| 12 |     30  |",
				"| 13 |    33   |",
				"| 14 |     31  |",
				"| 15 |      14 |",
				"| 16 |       4 |",
				"| 17 |      24 |",
				"| 18 |     32  |",
				"| 19 |      25 |",
				"| 20 |       5 |",
				"+----+---------+"
			],
			"program": [
				"(Perl) See Links section.",
				"(Haskell)",
				"import Data.List (inits, tails, intersect, delete)",
				"a262323 n = a262323_list !! (n-1)",
				"a262323_list = 1 : f \"1\" (map show [2..]) where",
				"   f xs zss = g zss where",
				"     g (ys:yss) | null (intersect its $ tail $ inits ys) \u0026\u0026",
				"                  null (intersect tis $ init $ tails ys) = g yss",
				"                | otherwise = (read ys :: Int) : f ys (delete ys zss)",
				"     its = init $ tails xs; tis = tail $ inits xs",
				"-- _Reinhard Zumkeller_, Sep 21 2015",
				"(Python)",
				"def overlaps(a, b):",
				"  s, t = sorted([str(a), str(b)], key = lambda x: len(x))",
				"  if any(t.startswith(s[i:]) for i in range(len(s))): return True",
				"  return any(t.endswith(s[:i]) for i in range(1, len(s)+1))",
				"def aupto(nn):",
				"  alst, aset = [1], {1}",
				"  for n in range(2, nn+1):",
				"    an = 1",
				"    while True:",
				"      while an in aset: an += 1",
				"      if overlaps(an, alst[-1]): alst.append(an); aset.add(an); break",
				"      an += 1",
				"  return alst",
				"print(aupto(67)) # _Michael S. Branicky_, Jan 10 2021"
			],
			"xref": [
				"Cf. A076654, A262255, A262283.",
				"Cf. A262367 (fixed points), A262411 (ternary version), A262460 (hexadecimal version)."
			],
			"keyword": "nonn,look,base,nice",
			"offset": "1,2",
			"author": "_Paul Tek_, Sep 19 2015",
			"references": 9,
			"revision": 68,
			"time": "2021-01-11T02:57:09-05:00",
			"created": "2015-09-20T03:00:38-04:00"
		}
	]
}