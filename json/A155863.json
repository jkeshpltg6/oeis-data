{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A155863",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 155863,
			"data": "1,1,1,1,6,1,1,24,24,1,1,60,120,60,1,1,120,360,360,120,1,1,210,840,1260,840,210,1,1,336,1680,3360,3360,1680,336,1,1,504,3024,7560,10080,7560,3024,504,1,1,720,5040,15120,25200,25200,15120,5040,720,1,1,990,7920,27720,55440,69300,55440,27720,7920,990,1",
			"name": "Triangle T(n,k) = n*(n^2 - 1)*binomial(n-2, k-1) for 1 \u003c= k \u003c= n-1, n \u003e= 2, and T(n,0) = T(n,n) = 1 for n \u003e= 0, read by rows.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A155863/b155863.txt\"\u003eRows n = 0..50 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = coefficients of p(n, x), where p(n, x) = 1 + x^n + x*((d/dx)^3 (x+1)^(n+1)) and T(0, 0) = 1.",
				"From _Franck Maminirina Ramaharo_, Dec 03 2018: (Start)",
				"T(n, k) = (n-1)*n*(n+1)*binomial(n-2, k-1) with T(n, 0) = T(n, n) = 1.",
				"n-th row polynomial is x^n + n*(n^2 - 1)*x*(x+1)^(n-2) + (1 + (-1)^(2^n))/2.",
				"G.f.: 1/(1 - y) + 1/(1 - x*y) + (6*x*y^2)/(1 - y - x*y)^4 - 1.",
				"E.g.f.: exp(y) + exp(x*y) + (3*x*y^2 + (x + x^2)*y^3)*exp((1 + x)*y) - 1. (End)",
				"Sum_{k=0..n} T(n, k) = 2 - [n=0] + 6*A001789(n+1) = 2 - [n=0] + A052771(n+1). - _G. C. Greubel_, Jun 04 2021"
			],
			"example": [
				"Triangle begins:",
				"  1;",
				"  1,   1;",
				"  1,   6,    1;",
				"  1,  24,   24,     1;",
				"  1,  60,  120,    60,     1;",
				"  1, 120,  360,   360,   120,     1;",
				"  1, 210,  840,  1260,   840,   210,     1;",
				"  1, 336, 1680,  3360,  3360,  1680,   336,     1;",
				"  1, 504, 3024,  7560, 10080,  7560,  3024,   504,    1,",
				"  1, 720, 5040, 15120, 25200, 25200, 15120,  5040,  720,   1;",
				"  1, 990, 7920, 27720, 55440, 69300, 55440, 27720, 7920, 990, 1;",
				"  ..."
			],
			"mathematica": [
				"(* First program *)",
				"p[n_, x_]:= p[n, x]= If[n==0, 1, 1 + x^n + x*D[(x+1)^(n+1), {x, 3}]];",
				"Flatten[Table[CoefficientList[p[n,x], x], {n,0,12}]]",
				"(* Second program *)",
				"T[n_, k_]:= If[k==0 || k==n, 1, 6*Binomial[n+1, 3]*Binomial[n-2, k-1]];",
				"Table[T[n, k], {n,0,12}, {k,0,n}]//Flatten (* _G. C. Greubel_, Jun 04 2021 *)"
			],
			"program": [
				"(Maxima) T(n, k):= ratcoef(expand(x^n + n*(n^2 -1)*x*(x+1)^(n-2) + (1 + (-1)^(2^n))/2), x, k)$",
				"create_list(T(n, k), n, 0, 12, k, 0, n); /* _Franck Maminirina Ramaharo_, Dec 03 2018 */",
				"(MAGMA)",
				"A155863:= func\u003c n,k | k eq 0 or k eq n select 1 else 6*Binomial(n+1, 3)*Binomial(n-2, k-1) \u003e;",
				"[A155863(n,k): k in [0..n], n in [0..12]]; // _G. C. Greubel_, Jun 04 2021",
				"(Sage)",
				"def A155863(n,k): return 1 if (k==0 or k==n) else 6*binomial(n+1, 3)*binomial(n-2, k-1)",
				"flatten([[A155863(n,k) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, Jun 04 2021"
			],
			"xref": [
				"Cf. A001789, A052771, A155864, A155865."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "0,5",
			"author": "_Roger L. Bagula_, Jan 29 2009",
			"ext": [
				"Edited and name clarified by _Franck Maminirina Ramaharo_, Dec 03 2018"
			],
			"references": 5,
			"revision": 10,
			"time": "2021-06-04T22:07:02-04:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}