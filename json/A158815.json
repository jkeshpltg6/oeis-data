{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A158815",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 158815,
			"data": "1,1,1,4,1,1,13,5,1,1,46,16,6,1,1,166,58,19,7,1,1,610,211,71,22,8,1,1,2269,781,261,85,25,9,1,1,8518,2920,976,316,100,28,10,1,1,32206,11006,3676,1196,376,116,31,11,1,1",
			"name": "Triangle T(n,k) read by rows, matrix product of A046899(row-reversed) * A130595.",
			"comment": [
				"The left factor of the matrix product is the triangle which starts",
				"   1;",
				"   2,  1;",
				"   6,  3, 1;",
				"  20, 10, 4, 1;",
				"a row-reversed version of A046899, equivalent to the triangular view of the array A092392. The right factor is the inverse of the matrix A007318, which is A130595.",
				"Swapping the two factors, A007318^(-1) * A046899(row-reversed) would generate A158793.",
				"Riordan array (f(x), g(x)) where f(x) is the g.f. of A026641 and where g(x) is the g.f. of A000957. - _Philippe Deléham_, Dec 05 2009",
				"T(n,k) is the number of nonnegative paths consisting of upsteps U=(1,1) and downsteps D=(1,-1) of length 2n with k low peaks. (A low peak has its peak vertex at height 1.) Example: T(3,1)=5 counts UDUUUU, UDUUUD, UDUUDU, UDUUDD, UUDDUD. - _David Callan_, Nov 21 2011",
				"Matrix product P^2 * Q * P^(-2), where P denotes Pascal's triangle A007318 and Q denotes A061554 (formed from P by sorting the rows into descending order). Cf. A158793 and A171243. - _Peter Bala_, Jul 13 2021"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A158815/b158815.txt\"\u003eRows n = 0..50 of the triangle, flattened\u003c/a\u003e",
				"Filippo Disanto, Andrea Frosini and Simone Rinaldi, Renzo Pinzani, \u003ca href=\"http://www.seams-bull-math.ynu.edu.cn/downloadfile.jsp?filemenu=_200805\u0026amp;filename=The%20Combinatorics%20of%20Convex%20Permutominoes.pdf\"\u003eThe Combinatorics of Convex Permutominoes\u003c/a\u003e, Southeast Asian Bulletin of Mathematics (2008) 32: 883-912."
			],
			"formula": [
				"Sum_{k=0..n} T(n,k) = A046899(n).",
				"T(n,0) = A026641(n).",
				"Sum_{k=0..n} T(n,k)*x^k = A026641(n), A000984(n), A001700(n), A000302(n) for x = 0, 1, 2, 3 respectively. - _Philippe Deléham_, Dec 03 2009",
				"T(n, k) = Sum_{j=0..n} binomial(j, k)*binomial(2*n-j, n). - _Peter Bala_, Jul 13 2021"
			],
			"example": [
				"The triangle starts",
				"       1;",
				"       1,     1;",
				"       4,     1,     1;",
				"      13,     5,     1,    1;",
				"      46,    16,     6,    1,    1;",
				"     166,    58,    19,    7,    1,   1;",
				"     610,   211,    71,   22,    8,   1,   1;",
				"    2269,   781,   261,   85,   25,   9,   1,  1;",
				"    8518,  2620,   976,  316,  100,  28,  10,  1,  1;",
				"   32206, 11006,  3676, 1196,  376, 116,  31, 11,  1, 1;",
				"  122464, 41746, 13938, 4544, 1442, 441, 133, 34, 12, 1, 1;",
				"  ..."
			],
			"maple": [
				"A158815 := proc (n, k)",
				"  add((-1)^(j+k)*binomial(2*n-j, n)*binomial(j, k), j = 0..n);",
				"end proc:",
				"seq(seq(A158815(n, k), k = 0..n), n = 0..10); # _Peter Bala_, Jul 13 2021"
			],
			"mathematica": [
				"T[n_,k_]:= T[n,k]= Sum[(-1)^(j+k)*Binomial[j,k]*Binomial[2*n-j,n], {j,0,n}];",
				"Table[T[n,k], {n,0,12}, {k,0,n}]//Flatten (* _G. C. Greubel_, Dec 22 2021 *)"
			],
			"program": [
				"(Sage)",
				"def A158815(n,k): return sum( (-1)^(j+k)*binomial(2*n-j, n)*binomial(j, k) for j in (0..n) )",
				"flatten([[A158815(n,k) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, Dec 22 2021"
			],
			"xref": [
				"Cf. A000984, A026641, A046899, A158793, A171243."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_Gary W. Adamson_ and _Roger L. Bagula_, Mar 27 2009",
			"references": 5,
			"revision": 26,
			"time": "2021-12-23T05:52:33-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}