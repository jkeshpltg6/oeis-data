{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A235863",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 235863,
			"data": "1,2,4,4,4,4,8,4,12,4,12,4,12,8,4,4,16,12,20,4,8,12,24,4,20,12,36,8,28,4,32,8,12,16,8,12,36,20,12,4,40,8,44,12,12,24,48,4,56,20,16,12,52,36,12,8,20,28,60,4,60,32,24,16,12,12,68,16,24,8,72",
			"name": "Exponent of the multiplicative group G_n:={x+iy: x^2+y^2==1 (mod n); 0 \u003c= x,y \u003c n} where i=sqrt(-1).",
			"comment": [
				"From _Jianing Song_, Nov 05 2019: (Start)",
				"Exponent of the group G is the least e \u003e 0 such that x^e = 1 for every x in G, where 1 is the identity element.",
				"Also the exponent of O(2,Z_n) or SO(2,Z_n). O(2,Z_n) is the group of 2 X 2 matrices A over Z_n such that A*A^T = E = [1,0;0,1]; SO(2,Z_n) is the group of 2 X 2 matrices A over Z_n such that A*A^T = E = [1,0;0,1] and det(A) = 1. Note that G_n is isomorphic to SO(2,Z_n) by the mapping x+yi \u003c-\u003e [x,y;-y,x]. See A060698 for the group structure of SO(2,Z_n) and A182039 for the group structure of O(2,Z_n). (End)"
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A235863/b235863.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"José María Grau, A. M. Oller-Marcén, Manuel Rodriguez and Daniel Sadornil, \u003ca href=\"http://arxiv.org/abs/1401.4708\"\u003eFermat test with Gaussian base and Gaussian pseudoprimes\u003c/a\u003e, arXiv:1401.4708 [math.NT], 2014.",
				"José María Grau, A. M. Oller-Marcén, Manuel Rodriguez and Daniel Sadornil, \u003ca href=\"http://dx.doi.org/10.1007/s10587-015-0221-2\"\u003eFermat test with Gaussian base and Gaussian pseudoprimes\u003c/a\u003e, Czechoslovak Mathematical Journal 65(140), (2015) pp. 969-982."
			],
			"formula": [
				"a(@) = 2, a(4) = a(8) = a(16) = 4, a(2^e) = 2^(e-2) for e \u003e= 5; a(p^e) = (p-1)*p^(e-1) if e p == 1 (mod 4) and (p+1)*p^(e-1) if e p == 1 (mod 4). - _Jianing Song_, Nov 05 2019",
				"If gcd(n,m)=1 then a(nm) = lcm(a(n), a(m))."
			],
			"mathematica": [
				"fa=FactorInteger; lam[1]=1;lam[p_, s_] := Which[Mod[p, 4] == 3, p ^ (s - 1 ) (p + 1) , Mod[p, 4] == 1, p ^ (s - 1 ) (p - 1)  , s ≥ 5, 2 ^ (s - 2 ), s \u003e 1, 4, s == 1, 2];lam[n_] := {aux = 1; Do[aux = LCM[aux, lam[fa[n][[i, 1]], fa[n][[i, 2]]]], {i, 1, Length[fa[n]]}]; aux}[[1]] ; Array[lam, 100]"
			],
			"program": [
				"(PARI) a(n)={my(f=factor(n)); lcm(vector(#f~, i, my([p,e]=f[i,]); if(p==2, 2^max(e-2, min(e,2)), p^(e-1)*if(p%4==1, p-1, p+1))))} \\\\ _Andrew Howroyd_, Aug 06 2018"
			],
			"xref": [
				"                                   (Z/nZ)*    ------    G_n",
				"Order:                             A000010    ------  A060968.",
				"Exponent:                          A002322    ------  this sequence.",
				"                                     n-1      ------  A201629.",
				"Carmichael/G-Carmichael numbers:   A002997    ------  A235865.",
				"Lehmer /G-Lehmer numbers:          unknown    ------  A235864.",
				"Cyclic/G-cyclic numbers:           A003277    ------  A235866.",
				"n such that the group is cyclic:   A033948    ------  A235868.",
				"Cf. A060698, A182039."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_José María Grau Ribas_, Jan 16 2014",
			"references": 7,
			"revision": 70,
			"time": "2019-11-13T09:45:07-05:00",
			"created": "2014-02-22T19:11:25-05:00"
		}
	]
}