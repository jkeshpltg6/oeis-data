{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A239397",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 239397,
			"data": "1,1,2,1,1,2,3,0,0,3,3,2,2,3,4,1,1,4,5,2,2,5,6,1,1,6,5,4,4,5,7,0,0,7,7,2,2,7,6,5,5,6,8,3,3,8,8,5,5,8,9,4,4,9,10,1,1,10,10,3,3,10,8,7,7,8,11,0,0,11,11,4,4,11,10,7,7,10,11,6,6,11,13,2",
			"name": "Prime Gaussian integers x + y*i sorted by norm and increasing y, with x and y nonnegative.",
			"comment": [
				"After the number 1 + i, there are exactly two Gaussian primes here for each norm in A055025; if x + y*i is here, then y + x*i is also. - _T. D. Noe_, Mar 26 2014",
				"Sequence A239621 provides a more condensed version, without y + x*i following each x + y*i. The real parts and imaginary parts are listed in A300587 and A300588. - _M. F. Hasler_, Mar 09 2018"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A239397/b239397.txt\"\u003eTable of n, a(n) for n = 1..10002\u003c/a\u003e (5001 complex numbers)",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/GaussianPrime.html\"\u003eGaussian prime\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Complex_number\"\u003eComplex Number\u003c/a\u003e"
			],
			"formula": [
				"a(4n + 1) = a(4n) = A239621(2n) = A300588(n), a(4n + 2) = a(4n-1) = A239621(2n-1) = A300587(n). - _M. F. Hasler_, Mar 09 2018"
			],
			"example": [
				"The sequence of Gaussian primes (with nonnegative real and imaginary part) begins 1+i, 2+i, 1+2i, 3, 3i,..."
			],
			"mathematica": [
				"mx = 20; lst = Flatten[Table[{a, b}, {a, 0, mx}, {b, 0, mx}], 1]; qq = Select[lst, Norm[#] \u003c= mx \u0026\u0026 PrimeQ[#[[1]] + I*#[[2]], GaussianIntegers -\u003e True] \u0026]; Sort[qq, Norm[#1] \u003c Norm[#2] \u0026]"
			],
			"program": [
				"(PARI) is_GP(x,y=0)={(x=factor(if(imag(x+I*y),x+I*y,I*x+y)))\u0026\u0026vecsum(x[,2])==1+(abs(x[1,1])==1)} \\\\ Returns 1 iff x + iy (y may be omitted) is a Gaussian prime. -  _M. F. Hasler_, Mar 10 2018",
				"(PARI) for(N=2,499, if(isprime(N) \u0026\u0026 N%4\u003c3, z=factor(I*N); for(i=0,N\u003e2, print1(real(z[i+1,1])\",\"imag(z[i+1,1])\",\")), issquare(N,\u0026z) \u0026\u0026 isprime(z) \u0026\u0026 z%4==3 \u0026\u0026 print1(z\",0,0,\"z\",\")) \\\\ _M. F. Hasler_, Mar 10 2018"
			],
			"xref": [
				"Cf. A055025 (norms of Gaussian primes), A239621, A300587, A300588."
			],
			"keyword": "nonn,easy",
			"offset": "1,3",
			"author": "_T. D. Noe_, Mar 22 2014",
			"references": 5,
			"revision": 31,
			"time": "2018-03-15T05:05:15-04:00",
			"created": "2014-03-22T21:11:39-04:00"
		}
	]
}