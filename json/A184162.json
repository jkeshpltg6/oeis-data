{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A184162",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 184162,
			"data": "1,3,7,5,15,9,11,7,13,17,31,11,19,13,21,9,23,15,15,19,17,33,27,13,29,21,19,15,35,23,63,11,37,25,25,17,23,17,25,21,39,19,27,35,27,29,43,15,21,31,29,23,19,21,45,17,21,37,47,25,31,65,23,13,33,39,31,27,33,27,39,19,35,25,35,19,41,27,67,23",
			"name": "Number of chains in the rooted tree with Matula-Goebel number n.",
			"comment": [
				"The vertices of a rooted tree can be regarded as a partially ordered set, where u\u003c=v holds for two vertices u and v if and only if u lies on the unique path between v and the root. A chain is a nonempty set of pairwise comparable vertices.",
				"The Matula-Goebel number of a rooted tree can be defined in the following recursive manner: to the one-vertex tree there corresponds the number 1; to a tree T with root degree 1 there corresponds the t-th prime number, where t is the Matula-Goebel number of the tree obtained from T by deleting the edge emanating from the root; to a tree T with root degree m\u003e=2 there corresponds the product of the Matula-Goebel numbers of the m branches of T."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A184162/b184162.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"É. Czabarka, L. Székely, and S. Wagner, \u003ca href=\"http://dx.doi.org/10.1016/j.dam.2009.07.004\"\u003eThe inverse problem for certain tree parameters\u003c/a\u003e, Discrete Appl. Math., 157, 2009, 3314-3319.",
				"F. Goebel, \u003ca href=\"http://dx.doi.org/10.1016/0095-8956(80)90049-0\"\u003eOn a 1-1-correspondence between rooted trees and natural numbers\u003c/a\u003e, J. Combin. Theory, B 29 (1980), 141-143.",
				"I. Gutman and A. Ivic, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(95)00182-V\"\u003eOn Matula numbers\u003c/a\u003e, Discrete Math., 150, 1996, 131-142.",
				"I. Gutman and Yeong-Nan Yeh, \u003ca href=\"http://www.emis.de/journals/PIMB/067/3.html\"\u003eDeducing properties of trees from their Matula numbers\u003c/a\u003e, Publ. Inst. Math., 53 (67), 1993, 17-22.",
				"D. W. Matula, \u003ca href=\"http://www.jstor.org/stable/2027327\"\u003eA natural rooted tree enumeration by prime factorization\u003c/a\u003e, SIAM Rev. 10 (1968) 273.",
				"\u003ca href=\"/index/Mat#matula\"\u003eIndex entries for sequences related to Matula-Goebel numbers\u003c/a\u003e"
			],
			"formula": [
				"a(1)=1; if n=p(t) (=the t-th prime), then a(n)=1+2a(t); if n=rs (r,s,\u003e=2), then a(n)=a(r)+a(s)-1. The Maple program is based on this recursive formula.",
				"a(n) = 1 + Sum_{k=1..A109082(n)} A196056(n,k)*2^k. - _Kevin Ryde_, Aug 25 2021"
			],
			"example": [
				"a(5) = 15 because the rooted tree with Matula-Goebel number 5 is a path ABCD on 4 vertices and any nonempty subset of {A,B,C,D} is a chain."
			],
			"maple": [
				"with(numtheory): a := proc (n) local r, s: r := proc (n) options operator, arrow: op(1, factorset(n)) end proc: s := proc (n) options operator, arrow: n/r(n) end proc: if n = 1 then 1 elif bigomega(n) = 1 then 1+2*a(pi(n)) else a(r(n))+a(s(n))-1 end if end proc: seq(a(n), n = 1 .. 80);"
			],
			"program": [
				"(Haskell)",
				"import Data.List (genericIndex)",
				"a184162 n = genericIndex a184162_list (n - 1)",
				"a184162_list = 1 : g 2 where",
				"   g x = y : g (x + 1) where",
				"     y = if t \u003e 0 then 2 * a184162 t + 1 else a184162 r + a184162 s - 1",
				"         where t = a049084 x; r = a020639 x; s = x `div` r",
				"-- _Reinhard Zumkeller_, Sep 03 2013",
				"(PARI) a(n) = my(f=factor(n)); [self()(primepi(p)) |p\u003c-f[,1]] * f[,2]*2 + 1; \\\\ _Kevin Ryde_, Aug 25 2021"
			],
			"xref": [
				"Cf. A109082 (height), A196056 (vertices at levels).",
				"Cf. A184160 (antichains).",
				"Cf. A049084, A020639."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Emeric Deutsch_, Oct 19 2011",
			"references": 1,
			"revision": 26,
			"time": "2021-10-04T08:38:53-04:00",
			"created": "2011-10-19T11:57:21-04:00"
		}
	]
}