{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A223853",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 223853,
			"data": "1,1,2,1,2,2,2,1,3,4,1,7,1,13,10,4,25,-5,49,17,38,82,103,-55,245,290,105,621,-107,1219,1196,-274,1749,5329,2881,2451,6836,2910,15905,28044,-10652,55758,18068,129994,-95925,52787,443983,253331,151395,740898,-352415",
			"name": "a(n) = ceiling(li(2*2^n) - li(2^n)) - (pi(2*2^n) - pi(2^n)) with li(x) the logarithmic integral and pi(x) the prime counting function.",
			"comment": [
				"This is the difference between the estimate for the number of primes in power of two intervals determined by the li approximation, and the actual number of primes in the power of two interval. The MAGMA program gives the ceiling of the difference between the li estimate at the end points of the interval and the actual number of primes in the interval (A036378).",
				"H. J. J. te Riele (1987) using methods developed by Lehman (1966) showed that between 6.62*10^370 and 6.69*10^370 there are more than 10^180 consecutive integers where pi(x) \u003e li(x). It is worth noting that this falls entirely within the power of two interval starting at 2^1231, and while the condition \"li underestimates the number of primes in an interval\" is not sufficient to imply that pi(x) \u003e li(x), for example in (2^18, 2^19) li(x) underestimates by 5 but li(x) \u003e pi(x) at every point in the interval, it does seem to be necessary for this to occur, assuming runs of consecutive values where pi(x) \u003e li(x) do not cross a power of two."
			],
			"link": [
				"Brad Clardy, \u003ca href=\"/A223853/b223853.txt\"\u003eTable of n, a(n) for n = 1..74\u003c/a\u003e",
				"R. S. Lehman, \u003ca href=\"http://matwbn.icm.edu.pl/ksiazki/aa/aa11/aa11132.pdf\"\u003eOn the difference pi(x) - li(x)\u003c/a\u003e, Acta Arithmetica XI (1966), p. 397-410",
				"H. J. J. te Riele, \u003ca href=\"https://doi.org/10.1090/S0025-5718-1987-0866118-6\"\u003eOn the sign of the difference pi(x) - li(x)\u003c/a\u003e, Math. Comp. 48 (1987), p.323-328"
			],
			"formula": [
				"a(n) = A223900(n) - A036378(n)."
			],
			"mathematica": [
				"pi = Table[PrimePi[2^n], {n, 1, 30}];",
				"li = Table[LogIntegral[2^n], {n, 1, 30}];",
				"Ceiling[Rest@li - Most@li] - (Rest@pi - Most@pi) (* _Peter Luschny_, Oct 14 2017 *)"
			],
			"program": [
				"(MAGMA)",
				"1;",
				"for i := 2 to 29 do",
				"    x := 2^i;",
				"    y := 2^(i+1);",
				"    delta_li := Ceiling(LogIntegral(y) - LogIntegral(x));",
				"    delta_pi := #PrimesInInterval(x, y);",
				"    delta_li - delta_pi;",
				"end for;"
			],
			"xref": [
				"Cf. A000720, A036378, A052435, A223900."
			],
			"keyword": "sign",
			"offset": "1,3",
			"author": "_Brad Clardy_, Mar 28 2013",
			"references": 2,
			"revision": 40,
			"time": "2017-10-14T10:05:24-04:00",
			"created": "2013-04-02T13:39:37-04:00"
		}
	]
}