{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A058710",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 58710,
			"data": "1,0,1,0,1,1,0,1,4,1,0,1,14,11,1,0,1,51,106,26,1,0,1,202,1232,642,57,1,0,1,876,22172,28367,3592,120,1,0,1,4139,803583,8274374,991829,19903,247,1",
			"name": "Triangle T(n,k) giving number of loopless matroids of rank k on n labeled points (n \u003e= 0, 0 \u003c= k \u003c= n).",
			"comment": [
				"From _Petros Hadjicostas_, Oct 10 2019: (Start)",
				"The old references have some typos, some of which were corrected in the recent references (in 2004). Few additional typos were corrected here from the recent references. Here are some of the changes: T(5,2) = 31 --\u003e 51 (see the comment by _Ralf Stephan_ below); T(5,4) = 21 --\u003e 26; sum of row n=5 is 185 (not 160 or 165); T(8,3) = 686515 --\u003e 803583; T(8, 6) = 19904 --\u003e 19903, and some others.",
				"This triangular array is the same as A058711 except that the current one has row n = 0 and column k = 0.",
				"(End)"
			],
			"link": [
				"W. M. B. Dukes, \u003ca href=\"http://www.stp.dias.ie/~dukes/matroid.html\"\u003eTables of matroids\u003c/a\u003e.",
				"W. M. B. Dukes, \u003ca href=\"https://web.archive.org/web/20030208144026/http://www.stp.dias.ie/~dukes/phd.html\"\u003eCounting and Probability in Matroid Theory\u003c/a\u003e, Ph.D. Thesis, Trinity College, Dublin, 2000.",
				"W. M. B. Dukes, \u003ca href=\"https://arxiv.org/abs/math/0411557\"\u003eThe number of matroids on a finite set\u003c/a\u003e, arXiv:math/0411557 [math.CO], 2004.",
				"W. M. B. Dukes, \u003ca href=\"http://emis.impa.br/EMIS/journals/SLC/wpapers/s51dukes.html\"\u003eOn the number of matroids on a finite set\u003c/a\u003e, Séminaire Lotharingien de Combinatoire 51 (2004), Article B51g."
			],
			"formula": [
				"From _Petros Hadjicostas_, Oct 10 2019: (Start)",
				"T(n,0) = 0^n for n \u003e= 0.",
				"T(n,1) = 1 for n \u003e= 1.",
				"T(n,2) = Bell(n) - 1 = A000110(n) - 1 = A058692(n) for n \u003e= 2.",
				"T(n,3) = Sum_{i = 3..n} Stirling2(n,i) * (A056642(i) - 1) = Sum_{i = 3..n} A008277(n,i) * A058720(i,3) for n \u003e= 3.",
				"T(n,k) = Sum_{i = k..n} Stirling2(n,i) * A058720(i,k) for n \u003e= k. [Dukes (2004), p. 3; see the equation with the Stirling numbers of the second kind.]",
				"(End)"
			],
			"example": [
				"Triangle T(n,k) (with rows n \u003e= 0 and columns k \u003e= 0) begins as follows:",
				"  1;",
				"  0, 1;",
				"  0, 1,    1;",
				"  0, 1,    4,      1;",
				"  0, 1,   14,     11,       1;",
				"  0, 1,   51,    106,      26,      1;",
				"  0, 1,  202,   1232,     642,     57,     1;",
				"  0, 1,  876,  22172,   28367,   3592,   120,   1;",
				"  0, 1, 4139, 803583, 8274374, 991829, 19903, 247, 1;",
				"  ..."
			],
			"xref": [
				"Cf. Same as A058711 (except for row n=0 and column k=0).",
				"Row sums give A058712.",
				"Columns include (truncated versions of) A000007 (k=0), A000012 (k=1), A058692 (k=2), A058715 (k=3)."
			],
			"keyword": "nonn,tabl,nice",
			"offset": "0,9",
			"author": "_N. J. A. Sloane_, Dec 31 2000",
			"ext": [
				"T(5,2) corrected from 31 to 51 by _Ralf Stephan_, Nov 29 2004"
			],
			"references": 8,
			"revision": 38,
			"time": "2019-10-10T15:09:36-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}