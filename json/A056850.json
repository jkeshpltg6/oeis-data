{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A056850",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 56850,
			"data": "0,1,1,5,17,13,217,139,1631,3299,6487,46075,7153,502829,588665,2428309,9492289,5077565,118985033,88519643,808182895,1870418611,2978678759,25423702091,7551629537,252223018333,342842572777,1170495537221,5284606410545,1738366812781",
			"name": "Minimal absolute difference of 3^n and 2^k.",
			"comment": [
				"Except for 3^0 - 2^0, 3^1 - 2^1 and 3^2 - 2^3, there are no cases where the differences are less than 4.",
				"It is known that a(n) tends to infinity as n tends to infinity. Indeed, Tijdeman showed that there exists an effectively computable constant c \u003e 0 such that |2^x - 3^y| \u003e 2^x/x^c. - _Tomohiro Yamada_, Sep 29 2017",
				"Empirical observation: For at least values a(0) through a(6308), k-2 \u003c n*log_2(3) \u003c k+2. - _Matthew Schuster_, Mar 28 2021",
				"For all n \u003e= 0, the lower and upper limits on n*log_2(3) - k are log_2(3/4) = -0.4150374... and log_2(3/2) = 0.5849625..., respectively; i.e., 0 \u003c= n*log_2(3) - k - log_2(3/4) \u003c 1. - _Jon E. Schoenfield_, Apr 21 2021"
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A056850/b056850.txt\"\u003eTable of n, a(n) for n = 0..2097\u003c/a\u003e",
				"R. Tijdeman, \u003ca href=\"http://www.numdam.org/item?id=CM_1973__26_3_319_0\"\u003eOn integers with many small prime factors\u003c/a\u003e, Compos. Math. 26 (1973), 319--330."
			],
			"example": [
				"For n = 4, the closest power of 2 to 3^n = 81 is 2^6 = 64, so a(4) = |3^4 - 2^6| = |81 - 64| = 17. - _Jon E. Schoenfield_, Sep 30 2017"
			],
			"mathematica": [
				"Table[Min[# - 2^Floor@ Log2@ # \u0026[3^n], 2^Ceiling@ Log2@ # - # \u0026[3^n]], {n, 0, 27}]]"
			],
			"xref": [
				"Cf. A056577 (smallest 3^n-2^k), A063003 (smallest 2^k-3^n)."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_Robert G. Wilson v_, Aug 30 2000",
			"ext": [
				"a(28)-a(29) from _Jon E. Schoenfield_, Mar 31 2021"
			],
			"references": 5,
			"revision": 32,
			"time": "2021-04-22T22:18:28-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}