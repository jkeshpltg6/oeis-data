{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A176283",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 176283,
			"data": "1,1,1,1,8,1,1,27,27,1,1,64,83,64,1,1,125,181,181,125,1,1,216,333,370,333,216,1,1,343,551,649,649,551,343,1,1,512,847,1036,1097,1036,847,512,1,1,729,1233,1549,1701,1701,1549,1233,729,1,1,1000,1721,2206,2485,2576,2485,2206,1721,1000,1",
			"name": "Triangle T(n,k) = 1 + A000537(n) - A000537(k) - A000537(n-k), read by rows.",
			"comment": [
				"Like A176282 but build on sums of cubes (A000537) instead of sums of squares.",
				"Row sums are {1, 2, 10, 56, 213, 614, 1470, 3088, 5889, 10426, 17402, ...} = (n+1)*(9*n^4 + 6*n^3 - 11*n^2 - 4*n + 60)/60."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A176283/b176283.txt\"\u003eRows n = 0..100 of triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = T(n,n-k).",
				"T(n, k) = (4 + n^2*(n+1)^2 - k^2*(k+1)^2 - (n-k)^2*(n-k+1)^2)/4. - _G. C. Greubel_, Nov 25 2019"
			],
			"example": [
				"Triangle begins as:",
				"  1;",
				"  1,    1;",
				"  1,    8,    1;",
				"  1,   27,   27,    1;",
				"  1,   64,   83,   64,    1;",
				"  1,  125,  181,  181,  125,    1;",
				"  1,  216,  333,  370,  333,  216,    1;",
				"  1,  343,  551,  649,  649,  551,  343,    1;",
				"  1,  512,  847, 1036, 1097, 1036,  847,  512,    1;",
				"  1,  729, 1233, 1549, 1701, 1701, 1549, 1233,  729,    1;",
				"  1, 1000, 1721, 2206, 2485, 2576, 2485, 2206, 1721, 1000, 1;"
			],
			"maple": [
				"seq(seq(, k=0..n), n=0..12); # _G. C. Greubel_, Nov 25 2019"
			],
			"mathematica": [
				"(* sequences with q=1..10 *)",
				"f[n_, k_, q_]:= f[n, k, q] = 1 + Sum[i^q, {i,0,n}] - Sum[i^q, {i,0,k}] + Sum[i^q, {i,0,n-k}])); Table[Flatten[Table[f[n, k, q], {n,0,10}, {k,0,n}]], {q,1,10}]",
				"(* Second program *)",
				"Table[(4 +n^2*(n+1)^2 -k^2*(k+1)^2 -(n-k)^2*(n-k+1)^2)/4, {n,0,12}, {k,0, n} ]//Flatten (* _G. C. Greubel_, Nov 25 2019 *)"
			],
			"program": [
				"(PARI) T(n,k) = 1 + (n^2*(n+1)^2 - k^2*(k+1)^2 - (n-k)^2*(n-k+1)^2)/4; \\\\ _G. C. Greubel_, Nov 25 2019",
				"(MAGMA) [(4 +n^2*(n+1)^2 -k^2*(k+1)^2 -(n-k)^2*(n-k+1)^2)/4: k in [0..n], n in [0..12]]; // _G. C. Greubel_, Nov 25 2019",
				"(Sage) [[(4 +n^2*(n+1)^2 -k^2*(k+1)^2 -(n-k)^2*(n-k+1)^2)/4 for k in (0..n)] for n in (0..12)] # _G. C. Greubel_, Nov 25 2019",
				"(GAP) Flat(List([0..12], n-\u003e List([0..n], k-\u003e (4 +n^2*(n+1)^2 -k^2*(k+1)^2 - (n-k)^2*(n-k+1)^2)/4  ))); # _G. C. Greubel_, Nov 25 2019"
			],
			"keyword": "nonn,tabl,easy",
			"offset": "0,5",
			"author": "_Roger L. Bagula_, Apr 14 2010",
			"ext": [
				"Edited by _R. J. Mathar_, May 03 2013"
			],
			"references": 1,
			"revision": 11,
			"time": "2019-11-25T01:03:07-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}