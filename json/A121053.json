{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A121053",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 121053,
			"data": "2,3,5,1,7,8,11,13,10,17,19,14,23,29,16,31,37,20,41,43,22,47,53,25,59,27,61,30,67,71,73,33,79,35,83,38,89,97,40,101",
			"name": "A sequence S describing the position of its prime terms.",
			"comment": [
				"S reads like this:",
				"\"At position 2, there is a prime in S\" [indeed, this is 3]",
				"\"At position 3, there is a prime in S\" [indeed, this is 5]",
				"\"At position 5, there is a prime in S\" [indeed, this is 7]",
				"\"At position 1, there is a prime in S\" [indeed, this is 2]",
				"\"At position 7, there is a prime in S\" [indeed, this is 11]",
				"\"At position 8, there is a prime in S\" [indeed, this is 13]",
				"\"At position 11, there is a prime in S\" [indeed, this is 19]",
				"\"At position 13, there is a prime in S\" [indeed, this is 23]",
				"\"At position 10, there is a prime in S\" [indeed, this is 17], etc.",
				"S is built with this rule: when you are about to write a term of S, always use the smallest integer not yet present in S and not leading to a contradiction.",
				"Thus one cannot start with 1; this would read: \"At position 1, there is a prime number in S\" [no, 1 is not a prime]",
				"So start S with 2 and the rest follows smoothly.",
				"S contains all the primes and they appear in their natural order.",
				"Does the ratio primes/composites in S tend to a limit? Answer (from _Dean Hickerson_): Yes, to 1/2.",
				"Comments from _Dean Hickerson_, Aug 11 2006: (Start)",
				"In the limit, exactly half of the terms are primes. Here's a formula, found empirically, for a(n) for n \u003e= 5:",
				"Let pi(n) be the number of primes \u003c= n and p(n) be the n-th prime. Then:",
				"- if n is prime or (n is composite and n+pi(n) is even) then a(n) = p(floor((n+pi(n))/2));",
				"- if n is composite and n+pi(n) is odd and n+1 is composite then a(n) = n+1;",
				"- if n is composite and n+pi(n) is odd and n+1 is prime then a(n) = n+2.",
				"Also, for n \u003e= 5, n is in the sequence iff either n is prime or n+pi(n) is even.",
				"(This could all be proved by induction on n.)",
				"It follows from this that, for n \u003e= 4, the number of primes among a(1), ..., a(n) is exactly floor((n+pi(n))/2. Since pi(n)/n -\u003e 0 as n -\u003e infinity, this is asymptotic to n/2. (End)"
			],
			"link": [
				"Kerry Mitchell, \u003ca href=\"/A121053/b121053.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Eric Angelini, \u003ca href=\"http://www.cetteadressecomportecinquantesignes.com/PrimePos.htm\"\u003eAbout this sequence\u003c/a\u003e",
				"E. Angelini, \u003ca href=\"/A121053/a121053.png\"\u003eA sequence describing the position of its prime terms\u003c/a\u003e [Cached, with permission]"
			],
			"mathematica": [
				"a[1]=2; a[2]=3; a[3]=5; a[4]=1; a[n_ /; PrimeQ[n] || !PrimeQ[n] \u0026\u0026 EvenQ[n+PrimePi[n]]] := Prime[Floor[(n+PrimePi[n])/2]]; a[n_ /; !PrimeQ[n] \u0026\u0026 OddQ[n+PrimePi[n]]] := If[!PrimeQ[n+1],n+1,n+2]; Table[a[n],{n,1,40}] (* _Jean-François Alcover_, Mar 21 2011, after _Dean Hickerson_'s empirical formula *)"
			],
			"xref": [
				"Cf. A105753."
			],
			"keyword": "nonn,nice",
			"offset": "1,1",
			"author": "_Eric Angelini_, Aug 10 2006",
			"references": 6,
			"revision": 22,
			"time": "2017-10-08T23:47:41-04:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}