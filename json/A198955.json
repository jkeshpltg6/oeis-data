{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A198955",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 198955,
			"data": "1,15,54,-76,-243,1188,-1384,-2916,11934,-11580,-21870,79704,-71022,-123444,421308,-352544,-581013,1885572,-1510236,-2388204,7469928,-5777672,-8852004,26869968,-20218587,-30177684,89408826,-65743304,-96033357,278737632,-201031888,-288281592,822239532,-583185916",
			"name": "q-expansion of modular form t_{3B}.",
			"comment": [
				"McKay-Thompson series of class 3B for the Monster group with a(0) = 15.",
				"Cubic AGM theta functions: a(q) (see A004016), b(q) (A005928), c(q) (A005882)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A198955/b198955.txt\"\u003eTable of n, a(n) for n = -1..1000\u003c/a\u003e",
				"Masao Koike, \u003ca href=\"https://oeis.org/A004016/a004016.pdf\"\u003eModular forms on non-compact arithmetic triangle groups\u003c/a\u003e, Unpublished manuscript [Extensively annotated with OEIS A-numbers by N. J. A. Sloane, Feb 14 2021. I wrote 2005 on the first page but the internal evidence suggests 1997.]"
			],
			"formula": [
				"Expansion of 27 + (eta(q) / eta(q^3))^12 in powers of q. - _Michael Somos_, Nov 08 2011",
				"Expansion of 27 * (a(q) / c(q))^3 in powers of q where a(q), c(q) are cubic AGM theta functions. - _Michael Somos_, Nov 08 2011",
				"Convolution cube of A058091. Note psi_0 = a(q), psi_1 = c(q) where q = exp(2 Pi i tau). - _Michael Somos_, Nov 08 2011",
				"Let psi_0 = theta_2(2*tau)*theta_2(6*tau) + theta_3(2*tau)*theta_3(6*tau),",
				"psi_1 = (psi_0(tau/3) - psi_0(tau))/2;",
				"then t_{3B} = 27(psi_0/psi_1)^3."
			],
			"example": [
				"1/q + 15 + 54*q - 76*q^2 - 243*q^3 + 1188*q^4 - 1384*q^5 - 2916*q^6 + 11934*q^7 - 11580*q^8 - 21870*q^9 + 79704*q^10 - 71022*q^11 - 123444*q^12 + 421308*q^13 - 352544*q^14 - 581013*q^15 + O(q^16)"
			],
			"mathematica": [
				"a[ n_] :=  With[{m = n + 1}, SeriesCoefficient[ 27 q + (Product[ 1 - q^k, {k, m}] / Product[ 1 - q^k, {k, 3, m, 3}])^12, {q, 0, m}]]; (* _Michael Somos_, Nov 08 2011 *)",
				"QP = QPochhammer; s = 27q+(QP[q]/QP[q^3])^12+O[q]^40; CoefficientList[s, q] (* _Jean-François Alcover_, Nov 16 2015, adapted from PARI *)"
			],
			"program": [
				"(PARI) {a(n) = local(A); if( n\u003c-1, 0, n++; A = x * O(x^n); polcoeff( 27*x + (eta(x + A) / eta(x^3 + A))^12, n))}; /* _Michael Somos_, Nov 08 2011 */"
			],
			"xref": [
				"Cf. A007244, A030182, A045481."
			],
			"keyword": "sign",
			"offset": "-1,2",
			"author": "_N. J. A. Sloane_, Oct 31 2011",
			"references": 4,
			"revision": 32,
			"time": "2021-02-14T01:15:33-05:00",
			"created": "2011-10-31T23:47:52-04:00"
		}
	]
}