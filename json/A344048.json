{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A344048",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 344048,
			"data": "1,1,2,2,7,14,6,34,86,168,24,209,648,1473,2840,120,1546,5752,14988,32344,61870,720,13327,58576,173007,414160,866695,1649232,5040,130922,671568,2228544,5876336,13373190,27422352,51988748",
			"name": "T(n, k) = n! * [x^n] exp(k * x/(1 - x))/(1 - x). Triangle read by rows, T(n, k) for 0 \u003c= k \u003c= n.",
			"formula": [
				"T(n, k) = (-1)^n*U(-n, 1, -k), where U is the Kummer U function.",
				"T(n, k) = n! * L(n, -k), where L is the Laguerre polynomial function.",
				"T(n, k) = n! * Sum_{j=0..n} binomial(n, j) * k^j / j!."
			],
			"example": [
				"Triangle starts:",
				"[0]    1;",
				"[1]    1,      2;",
				"[2]    2,      7,     14;",
				"[3]    6,     34,     86,     168;",
				"[4]   24,    209,    648,    1473,    2840;",
				"[5]  120,   1546,   5752,   14988,   32344,    61870;",
				"[6]  720,  13327,  58576,  173007,  414160,   866695,  1649232;",
				"[7] 5040, 130922, 671568, 2228544, 5876336, 13373190, 27422352, 51988748;",
				".",
				"Array whose upward read antidiagonals are the rows of the triangle.",
				"n\\k   0       1        2          3           4              5",
				"--------------------------------------------------------------------",
				"[0]   1,      2,      14,       168,        2840,         61870, ...",
				"[1]   1,      7,      86,      1473,       32344,        866695, ...",
				"[2]   2,     34,     648,     14988,      414160,      13373190, ...",
				"[3]   6,    209,    5752,    173007,     5876336,     224995745, ...",
				"[4]  24,   1546,   58576,   2228544,    91356544,    4094022230, ...",
				"[5] 120,  13327,  671568,  31636449,  1542401920,   80031878175, ...",
				"[6] 720, 130922, 8546432, 490102164, 28075364096, 1671426609550, ..."
			],
			"maple": [
				"# Rows of the array:",
				"A := (n, k) -\u003e (n + k)!*LaguerreL(n + k, -k):",
				"seq(print(seq(simplify(A(n, k)), k = 0..6)), n = 0..6);",
				"# Columns of the array:",
				"egf := n -\u003e exp(n*x/(1-x))/(1-x): ser := n -\u003e series(egf(n), x, 16):",
				"C := (k, n) -\u003e (n + k)!*coeff(ser(k), x, n + k):",
				"seq(print(seq(C(k, n), n = 0..6)), k=0..6);"
			],
			"mathematica": [
				"T[n_, k_] := (-1)^(n) HypergeometricU[-n, 1,  -k];",
				"Table[T[n, k], {n, 0, 7}, {k, 0, n}]  // Flatten",
				"(* Alternative: *)",
				"T[n_, k_] := n ! LaguerreL[n , -k];",
				"Table[T[n, k], {n, 0, 7}, {k, 0, n}] // Flatten"
			],
			"program": [
				"(SageMath) # Columns of the array:",
				"def column(k, len):",
				"    R.\u003cx\u003e = PowerSeriesRing(QQ, default_prec=len+k)",
				"    f = exp(k * x / (1 - x)) / (1 - x)",
				"    return f.egf_to_ogf().list()[k:]",
				"for col in (0..6): print(column(col, 8))",
				"# Alternative:",
				"@cached_function",
				"def L(n, x):",
				"    if n == 0: return 1",
				"    if n == 1: return 1 - x",
				"    return (L(n-1, x) * (2*n - 1 - x) - L(n-2, x)*(n - 1)) / n",
				"A344048 = lambda n, k: factorial(n)*L(n, -k)",
				"print(flatten([[A344048(n, k) for k in (0..n)] for n in (0..7)]))",
				"(PARI)",
				"T(n, k) = n! * sum(j=0, n, binomial(n, j) * k^j / j!)",
				"for(n=0, 9, for(k=0, n, print(T(n, k))))"
			],
			"xref": [
				"T(n, n) = A277373(n). T(2*n, n) = A344049(n). Row sums are A343849.",
				"Cf. A343847."
			],
			"keyword": "nonn,tabl",
			"offset": "0,3",
			"author": "_Peter Luschny_, May 08 2021",
			"references": 3,
			"revision": 18,
			"time": "2021-05-09T08:04:25-04:00",
			"created": "2021-05-09T08:04:25-04:00"
		}
	]
}