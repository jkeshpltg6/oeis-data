{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003464",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3464,
			"id": "M4425",
			"data": "0,1,7,43,259,1555,9331,55987,335923,2015539,12093235,72559411,435356467,2612138803,15672832819,94036996915,564221981491,3385331888947,20311991333683,121871948002099,731231688012595,4387390128075571",
			"name": "a(n) = (6^n - 1)/5.",
			"comment": [
				"a(n) = A125118(n, 5) for n\u003e4. - _Reinhard Zumkeller_, Nov 21 2006",
				"Let A be the Hessenberg matrix of order n, defined by: A[1,j]=1, A[i,i]:=6, (i\u003e1), A[i,i-1]=-1, and A[i,j]=0 otherwise. Then, for n\u003e=1, a(n)=det(A). - _Milan Janjic_, Feb 21 2010",
				"Let A be the Hessenberg matrix of order n, defined by: A[1,j]=1, A[i,i]:=7, (i\u003e1), A[i,i-1]=-1, and A[i,j]=0 otherwise. Then, for n\u003e1, a(n-1)=(-1)^n*charpoly(A,1). - _Milan Janjic_, Feb 21 2010",
				"Repunits to base 6. A repunit consisting of zero 1's (empty string) gives the empty sum, i.e., 0 (only case where leading zero is shown, for convenience). - _Daniel Forgues_, Jul 08 2011",
				"3*a(n) is the total number of holes in a certain triangle fractal (start with 6 triangles, 3 holes) after n iterations. See illustration in links. - _Kival Ngaokrajang_, Feb 21 2015"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A003464/b003464.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"C. Banderier and D. Merlini, \u003ca href=\"http://algo.inria.fr/banderier/Papers/infjumps.ps\"\u003eLattice paths with an infinite set of jumps\u003c/a\u003e, FPSAC02, Melbourne, 2002.",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=375\"\u003eEncyclopedia of Combinatorial Structures 375\u003c/a\u003e",
				"Kival Ngaokrajang, \u003ca href=\"/A003464/a003464.pdf\"\u003eIllustration of initial terms\u003c/a\u003e",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Repunit.html\"\u003eRepunit.\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (7,-6)."
			],
			"formula": [
				"Binomial transform of A003948. If preceded by 0, then binomial transform of powers of 5, A000351 (preceded by 0). - _Paul Barry_, Mar 28 2003",
				"a(n) = Sum_{k=1..n} C(n, k)*5^(k-1).",
				"E.g.f.: (exp(6*x) - exp(x))/5. - _Paul Barry_, Mar 28 2003",
				"G.f.: x/((1-x)*(1-6*x)). - Lambert Klasen (lambert.klasen(AT)gmx.net), Feb 06 2005",
				"a(n) = 6*a(n-1) + 1 with a(1)=1. - _Vincenzo Librandi_, Nov 17 2010",
				"a(n) = 7*a(n-1) - 6*a(n-2). - _Vincenzo Librandi_, Nov 08 2012"
			],
			"example": [
				"a(n) in base 6.................... a(n) in base 10:",
				"0..................................0",
				"1..................................1",
				"11.................................7",
				"111................................43",
				"1111...............................259",
				"11111..............................1555",
				"111111.............................9331",
				"1111111............................55987, etc. - _Philippe Deléham_, Mar 12 2014"
			],
			"maple": [
				"a:=n-\u003esum(6^(n-j),j=1..n): seq(a(n), n=1..21); # _Zerinvary Lajos_, Jan 04 2007",
				"A003464:=1/(6*z-1)/(z-1); # conjectured by _Simon Plouffe_ in his 1992 dissertation",
				"a[0]:=0:a[1]:=1:for n from 2 to 50 do a[n]:=5*a[n-1]+6*a[n-2]+2 od: seq(a[n], n=1..33); # _Zerinvary Lajos_, Dec 14 2008"
			],
			"mathematica": [
				"(6^Range[20]-1)/5 (* _Harvey P. Dale_, Dec. 14, 2010 *)",
				"LinearRecurrence[{7, -6}, {0, 1}, 30] (* _Vincenzo Librandi_, Nov 08 2012 *)"
			],
			"program": [
				"(PARI) for(n=1,10,print1((6^n-1)/5,\",\"));",
				"(Sage) [lucas_number1(n,7,6) for n in range(1, 22)] # _Zerinvary Lajos_, Apr 23 2009",
				"(Sage) [gaussian_binomial(n,1,6) for n in range(1,22)] # _Zerinvary Lajos_, May 28 2009",
				"(Maxima) A003464(n):=floor((6^n-1)/5)$  makelist(A003464(n),n,0,30); /* _Martin Ettl_, Nov 05 2012 */",
				"(MAGMA) [n le 2 select n-1 else 7*Self(n-1) - 6*Self(n-2): n in [1..30]]; // _Vincenzo Librandi_, Nov 08 2012"
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Reinhard Zumkeller_, Nov 21 2006",
				"G.f. corrected by _Philippe Deléham_, Mar 11 2014"
			],
			"references": 66,
			"revision": 97,
			"time": "2021-03-12T22:32:36-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}