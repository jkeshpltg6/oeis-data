{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A340407",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 340407,
			"data": "2,5,1,4,5,1,4,2,1,2,3,1,5,5,1,6,2,1,2,5,1,3,3,1,3,2,1,2,4,1,6,10,1,5,2,1,2,3,1,5,8,1,4,2,1,2,4,1,3,3,1,3,2,1,2,18,1,5,4,1,6,2,1,2,3,1,4,4,1,5,2,1,2,7,1,3,3,1,3,2,1,2,7,1,4,9,1,4,2,1",
			"name": "a(n) gives the number of side-branches that will be passed, if A342369 is used to trace the Collatz tree backward starting at 6*n-2 with n \u003e 0.",
			"comment": [
				"Recursion into A342369 means tracing the Collatz tree backward, starting at k = A342369(6*n-2), then k = A342369(k) until k is divisible by 3. At each A342369(k) = 3*m - 1, a new side-branch is connected which would start at 6*m-2. If A342369(k) reached a value divisible by three no further side-branches will be found.",
				"This sequence is a rearrangement of A087088 such that all values at positions divisible by 3 are unchanged."
			],
			"link": [
				"Thomas Scheuerle, \u003ca href=\"/A340407/b340407.txt\"\u003eTable of n, a(n) for n = 1..5000\u003c/a\u003e",
				"\u003ca href=\"/index/3#3x1\"\u003eIndex entries for sequences related to 3x+1 (or Collatz) problem\u003c/a\u003e"
			],
			"formula": [
				"a(n) \u003e 0.",
				"a(3*n) = 1.",
				"a(9*n - b) = 2, b = {1, 8} row 2 of A342261. ( a(A056020(n)) = 2 ).",
				"a(27*n - b) = 3, b = {2, 4, 5, 16} row 3 of A342261.",
				"a(81*n - b) = 4, b = {13, 14, 22, 34, 38, 52, 74, 77} row 4 of A342261.",
				"a(3^k*n - b) = k, b = row k of A342261.",
				"( Sum_{k=1..j} a(k) )/j  lim_{j-\u003einfinity} = 3 = Sum_{k=1..infinity} k*2^(k-1)/3^k."
			],
			"example": [
				"n = 2:",
				"6*n-2 = 10.",
				"A342369(10) = 20. -\u003e 7*3 - 1 -\u003e A side-branch is connected.",
				"A342369(20) = 13.",
				"A342369(13) = 26. -\u003e 9*3 - 1 -\u003e A side-branch is connected.",
				"A342369(26) = 17. -\u003e 6*3 - 1 -\u003e A side-branch is connected.",
				"A342369(17) = 11. -\u003e 4*3 - 1 -\u003e A side-branch is connected.",
				"A342369(11) = 7.",
				"A342369(7) = 14. -\u003e 5*3 - 1 -\u003e A side-branch is connected.",
				"A342369(14) = 9. -\u003e divisible by 3 we stop here.",
				"-\u003e We found 5 connected side-branches, a(2) = 5."
			],
			"program": [
				"(MATLAB)",
				"function a = A340407( max_n )",
				"    for n = 1:max_n",
				"        c = 0;",
				"        s = 6*n -2;",
				"        while mod(s,3) ~= 0",
				"            s = A342369( s );",
				"            if mod(s,3) == 2",
				"                c = c+1;",
				"            end",
				"        end",
				"        a(n) = c;",
				"    end",
				"end",
				"function b = A342369( n )",
				"    if mod(n,3) == 2",
				"        b = (2*n - 1)/3;",
				"    else",
				"        b = 2*n;",
				"    end",
				"end"
			],
			"xref": [
				"Cf. A014682, A342369, A342261, A087088."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Thomas Scheuerle_, Mar 24 2021",
			"references": 3,
			"revision": 76,
			"time": "2021-05-07T00:49:19-04:00",
			"created": "2021-03-24T12:33:00-04:00"
		}
	]
}