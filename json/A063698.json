{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A063698",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 63698,
			"data": "0,1,0,0,0,0,2,0,0,0,10,0,4,0,42,146,0,0,8,0,68,2322,682,0,16,0,2730,0,1092,0,56,0,0,599186,43690,8726850,64,0,174762,9585810,4112,0,792,0,279620,2101256,2796202,0,256,0,32800,2454267026,4473924,0,512",
			"name": "Positions of negative coefficients in cyclotomic polynomial Phi_n(x), converted from binary to decimal. (The constant term in the least significant bit (bit-0), the term x in the next bit (bit-1) and so on).",
			"comment": [
				"Maple procedures Phi_pos_terms and Phi_neg_terms are modeled after the formula given in Lam and Leung paper and they compute correct results for all integers x \u003e 1 and for all n with at most two distinct odd prime factors (that is, up to n=104). Other procedures as in A063696 and A063694."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A063698/b063698.txt\"\u003eTable of n, a(n) for n = 0..3003\u003c/a\u003e",
				"D. M. Bloom, \u003ca href=\"http://www.jstor.org/stable/2313417\"\u003eOn the Coefficients of the Cyclotomic Polynomials\u003c/a\u003e, Amer.Math.Monthly 75, 372-377, 1968.",
				"T.Y. Lam and K. H. Leung, \u003ca href=\"http://www.jstor.org/stable/2974668\"\u003eOn the Cyclotomic Polynomial Phi_pq(X)\u003c/a\u003e, Amer.Math.Monthly 103, 562-564, August-September 1996.",
				"H. Lenstra, \u003ca href=\"http://hdl.handle.net/1887/3800\"\u003eVanishing sums of roots of unity\u003c/a\u003e, in Proc. Bicentennial Congress Wiskundig Genootschap (Vrije Univ. Amsterdam, 1978), Part II, pp. 249-268.",
				"\u003ca href=\"/index/Cy#CyclotomicPolynomialsCoefficients\"\u003eIndex entries for cyclotomic polynomials, positions of coefficients\u003c/a\u003e"
			],
			"maple": [
				"with(numtheory); [seq(Phi_neg_terms(j,2),j=0..104)];",
				"Phi_neg_terms := proc(n,x) local a,m,p,q,e,f,r,s; if(n \u003c 2) then RETURN(n); fi; a := op(2, ifactors(n)); m := nops(a); p := a[1][1]; e := a[1][2]; if(1 = m) then RETURN(0); fi; if(2 = m) then q := a[2][1]; f := a[2][2]; r := inv_p_mod_q(p,q)-1; s := inv_p_mod_q(q,p)-1;",
				"RETURN( x^((s+1)*(q^f)*(p^(e-1))) * x^((r+1)*(p^e)*(q^(f-1))) * x^(-((p^e) * (q^f))) * (`if`((p-2)=s,1,(((x^((p-s-1)*((q^f)*(p^(e-1)))))-1)/((x^((q^f)*(p^(e-1))))-1)))) * (`if`((q-2)=r,1,(((x^((q-r-1)*((p^e)*(q^(f-1)))))-1)/((x^((p^e)*(q^(f-1))))-1)))) ); fi;",
				"if((3 = m) and (2 = p)) then if(1 = e) then RETURN(every_other_pos(Phi_neg_terms(n/2,x),x,0)+every_other_pos(Phi_pos_terms(n/2,x),x,1)); else RETURN(dilate(Phi_neg_terms((n/(2^(e-1))),x),x,2^(e-1))); fi; else printf(`Cannot handle argument %a with \u003e=3 distinct odd prime factors!\\n`,n); RETURN(0); fi; end;"
			],
			"mathematica": [
				"a[n_] := 2^(Flatten[Position[CoefficientList[Cyclotomic[n, x], x], _?Negative]] - 1) // Total; a[0] = 0; Table[a[n], {n, 0, 60}] (* _Jean-François Alcover_, Mar 05 2016 *)"
			],
			"program": [
				"(PARI) a(n)=my(p); if(n\u003c1, 0, p=polcyclo(n); sum(i=0, n, 2^i*(polcoeff(p, i)\u003c0))) \\\\ _Michel Marcus_, Mar 05 2016"
			],
			"xref": [
				"A013594, A063696 gives the positions of the positive and A063697 the nonzero terms. This sequence in binary: A063699. A019320[n] = A063696[n]-A063698[n] for up to n=104"
			],
			"keyword": "nonn",
			"offset": "0,7",
			"author": "_Antti Karttunen_, Aug 03 2001",
			"references": 6,
			"revision": 20,
			"time": "2018-08-06T22:26:20-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}