{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A305273",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 305273,
			"data": "-11,-7,-3,5,13,17,21,29,33,37,41,57,73",
			"name": "Numbers k in A048981 for which the ring Z[sqrt(k)] is not a UFD.",
			"comment": [
				"A factorial ring (or UFD = unique factorization domain) is an integral domain in which one can find a system of irreducible elements P such that every nonzero element admits a unique representation. We consider the ring A = Z[A048981(n)] such that A is not a UFD for some n. In the general case, it is well known that Z[sqrt(d)] is not factorial if one of the following conditions is satisfied:",
				"a) d \u003c= -3,",
				"b) d == 1 (mod 4),",
				"c) d has a square divisor different of 1,",
				"d) the number 2 is irreducible in Z[sqrt(d)]. Consequently, the equation x^2 - dy^2 = -2 or +2 has no solution.",
				"So the ring Z[A048981(n)] is factorial for the following values of A048981: -2, -1, 2, 3, 6, 7, 11 and 19."
			],
			"reference": [
				"R. Dedekind, Sur la théorie des nombres entiers algébriques, Gauthier-Villars, 1877. English translation with an introduction by J. Stillwell: Theory of Algebraic Integers, Cambridge Univ. Press, 1996.",
				"H. M. Stark, An Introduction to Number Theory. Markham, Chicago, 1970, p. 294."
			],
			"link": [
				"Encyclopedia of Mathematics, \u003ca href=\"https://www.encyclopediaofmath.org/index.php/Factorial_ring\"\u003eFactorial ring\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Unique_factorization_domain\"\u003eUnique factorization domain\u003c/a\u003e"
			],
			"example": [
				"5 = A048981(8) is in the sequence because the squarefree number 5 == 1 (mod 4) implies that Z[sqrt(5)] is not UFD.",
				"3 = A048981(7) is not in the sequence because the squarefree number 3 is not congruent to 1 (mod 4), but the solutions of the equation x^2 - 3y^2 = -2 or +2 are x = 1 (or -1), y = 1 (or -1). The ring Z[sqrt(3)] is factorial."
			],
			"maple": [
				"with(numtheory):T:=array(1..18):",
				"A048981:=[-11, -7, -3, -2, -1, 2, 3, 5, 6, 7, 11, 13, 17, 19, 21, 29, 33, 37, 41, 57, 73 ]:",
				"for n from 1 to 21 do:",
				"if A048981[n]\u003c=-3",
				"   or issqrfree(A048981[n])=false",
				"   or irem(A048981[n],4)=1",
				"   or nops(factorEQ(2, A048981[n]))=1",
				"   then",
				"   printf(`%d, `,A048981[n]):",
				"   else",
				"fi:",
				"od:"
			],
			"xref": [
				"Cf. A003174, A048981, A173298."
			],
			"keyword": "sign,fini,full",
			"offset": "1,1",
			"author": "_Michel Lagneau_, Dec 17 2018",
			"references": 0,
			"revision": 40,
			"time": "2019-12-11T08:55:20-05:00",
			"created": "2019-02-08T12:49:41-05:00"
		}
	]
}