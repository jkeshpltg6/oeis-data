{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A047874",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 47874,
			"data": "1,1,1,1,4,1,1,13,9,1,1,41,61,16,1,1,131,381,181,25,1,1,428,2332,1821,421,36,1,1,1429,14337,17557,6105,841,49,1,1,4861,89497,167449,83029,16465,1513,64,1,1,16795,569794,1604098,1100902,296326,38281,2521,81,1",
			"name": "Triangle of numbers T(n,k) = number of permutations of (1,2,...,n) with longest increasing subsequence of length k (1\u003c=k\u003c=n).",
			"comment": [
				"Mirror image of triangle in A126065.",
				"T(n,m) is also the sum of squares of n!/(product of hook lengths), summed over the partitions of n in exactly m parts (Robinson-Schensted correspondence). - _Wouter Meeussen_, Sep 16 2010",
				"Table I \"Distribution of L_n\" on p. 98 of the Pilpel reference. - _Joerg Arndt_, Apr 13 2013",
				"In general, for column k is a(n) ~ product(j!, j=0..k-1) * k^(2*n+k^2/2) / (2^((k-1)*(k+2)/2) * Pi^((k-1)/2) * n^((k^2-1)/2)) (result due to Regev) . - _Vaclav Kotesovec_, Mar 18 2014"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A047874/b047874.txt\"\u003eRows n = 1..60, flattened\u003c/a\u003e",
				"P. Diaconis, \u003ca href=\"http://projecteuclid.org/euclid.lnms/1215467407\"\u003eGroup Representations in Probability and Statistics\u003c/a\u003e, IMS, 1988; see p. 112.",
				"FindStat - Combinatorial Statistic Finder, \u003ca href=\"http://www.findstat.org/StatisticsDatabase/St000062\"\u003eThe length of the longest increasing subsequence of the permutation.\u003c/a\u003e",
				"Gessel, Ira M., \u003ca href=\"http://dx.doi.org/10.1016/0097-3165(90)90060-A\"\u003eSymmetric functions and P-recursiveness\u003c/a\u003e, J. Combin. Theory Ser. A 53 (1990), no. 2, 257-285.",
				"J. M. Hammersley, \u003ca href=\"http://projecteuclid.org/euclid.bsmsp/1200514101\"\u003eA few seedings of research\u003c/a\u003e, in Proc. Sixth Berkeley Sympos. Math. Stat. and Prob., ed. L. M. le Cam et al., Univ. Calif. Press, 1972, Vol. I, pp. 345-394.",
				"Guo-Niu Han, \u003ca href=\"http://www.emis.de/journals/SLC/wpapers/s61vortrag/han.pdf\"\u003eA promenade in the garden of hook length formulas\u003c/a\u003e, Slides, 61st SLC Curia, Portugal - September 22, 2008. [From _Wouter Meeussen_, Sep 16 2010]",
				"Hunt, J. and Szymanski, T., \u003ca href=\"http://dx.doi.org/10.1145/359581.359603\"\u003eA fast algorithm for computing longest common subsequences\u003c/a\u003e, Commun. ACM, 20 (1977), 350-353.",
				"E. Irurozki, B. Calvo, J. A. Lozano, \u003ca href=\"http://hdl.handle.net/10810/11241\"\u003eSampling and learning the Mallows model under the Ulam distance\u003c/a\u003e, 2014",
				"S. Pilpel, \u003ca href=\"http://dx.doi.org/10.1016/0097-3165(90)90022-O\"\u003eDescending subsequences of random permutations\u003c/a\u003e, J. Combin. Theory, A 53 (1990), 96-116.",
				"A. Regev, \u003ca href=\"http://dx.doi.org/10.1016/0001-8708(81)90012-8\"\u003eAsymptotic values for degrees associated with strips of Young diagrams\u003c/a\u003e, Adv. in Math. 41 (1981), 115-136.",
				"C. Schensted, \u003ca href=\"http://dx.doi.org/10.4153/CJM-1961-015-3\"\u003eLongest increasing and decreasing subsequences\u003c/a\u003e. Canadian J. Math. 13 (1961), 179-191.",
				"Richard P. Stanley, \u003ca href=\"http://arxiv.org/abs/math/0512035\"\u003eIncreasing and Decreasing Subsequences of Permutations and Their Variants\u003c/a\u003e, arXiv:math/0512035 [math.CO], 2005.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Longest_increasing_subsequence_problem\"\u003eLongest increasing subsequence problem\u003c/a\u003e"
			],
			"formula": [
				"Sum_{k=1..n} k * T(n,k) = A003316(n). - _Alois P. Heinz_, Nov 04 2018"
			],
			"example": [
				"T(3,2) = 4 because 132, 213, 231, 312 have longest increasing subsequences of length 2.",
				"Triangle T(n,k) begins:",
				"  1;",
				"  1,   1;",
				"  1,   4,    1;",
				"  1,  13,    9,    1;",
				"  1,  41,   61,   16,   1;",
				"  1, 131,  381,  181,  25,  1;",
				"  1, 428, 2332, 1821, 421, 36,  1;"
			],
			"maple": [
				"h:= proc(l) local n; n:= nops(l); add(i, i=l)! /mul(mul(1+l[i]-j",
				"      +add(`if`(l[k]\u003e=j, 1, 0), k=i+1..n), j=1..l[i]), i=1..n) end:",
				"g:= (n, i, l)-\u003e `if`(n=0 or i=1, h([l[], 1$n])^2, `if`(i\u003c1, 0,",
				"                add(g(n-i*j, i-1, [l[], i$j]), j=0..n/i))):",
				"T:= n-\u003e seq(g(n-k, min(n-k, k), [k]), k=1..n):",
				"seq(T(n), n=1..12);  # _Alois P. Heinz_, Jul 05 2012"
			],
			"mathematica": [
				"Table[Total[NumberOfTableaux[#]^2\u0026/@ IntegerPartitions[n,{k}]],{n,7},{k,n}] (* _Wouter Meeussen_, Sep 16 2010, revised Nov 19 2013 *)",
				"h[l_List] := Module[{n = Length[l]}, Total[l]!/Product[Product[1+l[[i]]-j+Sum[If[l[[k]] \u003e= j, 1, 0], {k, i+1, n}], {j, 1, l[[i]]}], {i, 1, n}]]; g[n_, i_, l_List] := If[n == 0 || i == 1, h[Join[l, Array[1\u0026, n]]]^2, If[i\u003c1, 0, Sum[g[n-i*j, i-1, Join[l, Array[i\u0026, j]]], {j, 0, n/i}]]]; T[n_] := Table[g[n-k, Min[n-k, k], {k}], {k, 1, n}]; Table[T[n], {n, 1, 12}] // Flatten (* _Jean-François Alcover_, Mar 06 2014, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A047887 and A047888.",
				"Columns k=1-10 give: A000012, A001453, A001454, A001455, A001456, A001457, A001458, A239432, A245665, A245666.",
				"Row sums give A000142.",
				"Cf. A047884. - _Wouter Meeussen_, Sep 16 2010",
				"Cf. A224652 (Table II \"Distribution of F_n\" on p. 99 of the Pilpel reference).",
				"Cf. A245667.",
				"T(2n,n) gives A267433.",
				"Cf. A003316."
			],
			"keyword": "nonn,easy,nice,tabl",
			"offset": "1,5",
			"author": "Eric Rains (rains(AT)caltech.edu)",
			"references": 24,
			"revision": 72,
			"time": "2020-02-06T20:36:17-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}