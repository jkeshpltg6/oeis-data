{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A164270",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 164270,
			"data": "1,-7,32,-114,350,-967,2468,-5916,13471,-29384,61784,-125838,249230,-481506,909788,-1684824,3063657,-5478698,9648360,-16752522,28708214,-48599047,81338660,-134687856,220802690,-358574468,577143752,-921144678,1458485460",
			"name": "Expansion of f(x^3)^3 * phi(x^3) / (f(x) * phi(x)^3) in powers of x where f(), phi() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A164270/b164270.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(-1/3) * eta(q)^7 * eta(q^4)^7 * eta(q^6)^14 / (eta(q^2)^18 * eta(q^3)^5 * eta(q^12)^5) in powers of q.",
				"Euler transform of period 12 sequence [ -7, 11, -2, 4, -7, 2, -7, 4, -2, 11, -7, 0, ...].",
				"a(n) = A164269(3*n + 1)."
			],
			"example": [
				"G.f. = 1 - 7*x + 32*x^2 - 114*x^3 + 350*x^4 - 967*x^5 + 2468*x^6 + ...",
				"G.f. = q - 7*q^4 + 32*q^7 - 114*q^10 + 350*q^13 - 967*q^16 + 2468*q^19 + ..."
			],
			"mathematica": [
				"f[x_, y_] := QPochhammer[-x, x*y]*QPochhammer[-y, x*y]*QPochhammer[x*y, x*y]; A164270[n_] := SeriesCoefficient[(f[q^3, -q^6]^3*f[q^3, q^3])/( (f[q, -q^2])*f[q, q]^3), {q, 0, n}]; Table[A164270[n], {n,0,50}] (* _G. C. Greubel_, Sep 16 2017 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A)^7 * eta(x^4 + A)^7 * eta(x^6 + A)^14 / (eta(x^2 + A)^18 * eta(x^3 + A)^5 * eta(x^12 + A)^5), n))};"
			],
			"xref": [
				"Cf. A164269."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Aug 11 2009",
			"references": 3,
			"revision": 11,
			"time": "2021-03-12T22:24:46-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}