{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A023038",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 23038,
			"data": "1,6,71,846,10081,120126,1431431,17057046,203253121,2421980406,28860511751,343904160606,4097989415521,48831968825646,581885636492231,6933795669081126,82623662392481281,984550153040694246,11731978174095849671,139799187936109501806",
			"name": "a(n) = 12*a(n-1) - a(n-2).",
			"comment": [
				"From _Wolfdieter Lang_, Nov 08 2002: (Start)",
				"Chebyshev's polynomials T(n,x) evaluated at x=6.",
				"a(n+1) give all (nontrivial, integer) solutions of Pell equation a(n+1)^2 - 35*b(n)^2 = +1 with b(n)=A004191(n), n\u003e=0. (End)",
				"a(35+70k)-1 and a(35+70k)+1 are consecutive odd powerful numbers. The first pair is 23101441813552306872262673994181386126 +- 1. See A076445. - _T. D. Noe_, May 04 2006",
				"Numbers n such that 35*(n^2-1) is a square. - _Vincenzo Librandi_, Nov 19 2010",
				"Except for the first term, positive values of x (or y) satisfying x^2 - 12xy + y^2 + 35 = 0. - _Colin Barker_, Feb 09 2014"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A023038/b023038.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (12,-1)."
			],
			"formula": [
				"a(n) = T(n, 6) = (S(n, 12)-S(n-2, 12))/2 with S(n, x) := U(n, x/2) and T(n), resp. U(n, x), are Chebyshev's polynomials of the first, resp. second, kind. See A053120 and A049310. S(-2, x) := -1, S(-1, x) := 0, S(n, 12)=A004191(n).",
				"a(n) = ((6+sqrt(35))^n + (6-sqrt(35))^n)/2.",
				"G.f.: (1-6*x)/(1-12*x+x^2).",
				"a(n)*a(n+3) - a(n+1)*a(n+2) = 420. - _Ralf Stephan_, Jun 06 2005"
			],
			"maple": [
				"A023038:=n-\u003eround(((6+sqrt(35))^n + (6-sqrt(35))^n)/2); seq(A023038(n), n=0..30); # _Wesley Ivan Hurt_, Feb 03 2014"
			],
			"mathematica": [
				"Table[Round[((6 + Sqrt[35])^n + (6 - Sqrt[35])^n)/2], {n, 0, 30}] (* _Wesley Ivan Hurt_, Feb 03 2014 *)",
				"nn = 20; CoefficientList[Series[(1 - 6*x)/(1 - 12*x + x^2), {x, 0, nn}], x] (* _T. D. Noe_, Feb 05 2014 *)"
			],
			"program": [
				"(PARI) x='x+O('x^30); Vec((1-6*x)/(1-12*x+x^2)) \\\\ _G. C. Greubel_, Dec 19 2017",
				"(MAGMA) I:=[1, 6]; [n le 2 select I[n] else 12*Self(n-1) - Self(n-2): n in [1..30]]; // _G. C. Greubel_, Dec 19 2017"
			],
			"xref": [
				"Cf. A087800."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_David W. Wilson_",
			"references": 12,
			"revision": 46,
			"time": "2018-01-13T19:17:57-05:00",
			"created": "1998-06-14T03:00:00-04:00"
		}
	]
}