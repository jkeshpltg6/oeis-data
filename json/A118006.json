{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A118006",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 118006,
			"data": "0,1,0,1,1,0,0,1,0,1,1,0,0,1,1,0,1,0,0,1,0,1,1,0,0,1,0,1,1,0,0,1,1,0,1,0,0,1,0,1,1,0,0,1,1,0,1,0,0,1,1,0,1,0,0,1,0,1,1,0,0,1,0,1,1,0,0,1,1,0,1,0,0,1,0,1,1,0,0,1,0,1,1,0,0,1,1,0,1,0,0,1,0,1,1,0,0,1,1,0,1,0,0,1,1",
			"name": "Define a sequence of binary words by w(1) = 01 and w(n+1) = w(n)w(n)Reverse[w(n)]. Sequence gives the limiting word w(infinity).",
			"comment": [
				"The article by W. Hebisch and M. Rubey gives a conjectured functional equation for the g.f. for this sequence. - _N. J. A. Sloane_, Sep 08 2010",
				"The formula in Hebisch and Rubey has an extra left parenthesis before f(x). - _Michael Somos_, Jan 03 2011",
				"From _Michel Dekking_, Sep 10 2020: (Start)",
				"This sequence is an automatic sequence, i.e., the letter-to-letter image of the fixed point of a uniform morphism mu.",
				"In fact, one can take the alphabet {1,2,3,4} with the morphism",
				"      mu:  1-\u003e121,  2-\u003e234, 3-\u003e123, 4-\u003e434,",
				"and the letter-to-letter map g defined by",
				"      g:  1-\u003e0, 2-\u003e1, 3-\u003e1, 4-\u003e0.",
				"Then (a(n)) = g(x), where x = 121234121... is the fixed point of the morphism mu starting with 1.",
				"This is obtained by translating the recursion relation for w to the morphism a-\u003eaab, b-\u003eabb, and then decorating the fixed point aabaababb.... of this morphism with a-\u003e01, b-\u003e10, since the recursion starts at w(1) = 01.",
				"It is well-known that decorated fixed points of morphisms are morphic sequences, and the 'natural' algorithm to achieve this (see my paper \"Morphic words, Beatty sequences and integer images of the Fibonacci language\") yields a morphism on an alphabet of 2+2 = 4 symbols. In general there are several choices for mu. Here we have chosen mu such that it has constant length, i.e., the morphism is uniform.",
				"(End)",
				"Morphism a-\u003eaab, b-\u003eabb is Stewart's choral sequence A116178 (ternary lowest non-1 digit, halved).  Decoration 01 and 10 is then an interleaving of that sequence and its complement so a(2n+1) = A116178(n) and a(2n+2) = 1-A116178(n) = A136442(n). - _Kevin Ryde_, Sep 29 2020"
			],
			"link": [
				"M. Dekking, \u003ca href=\"https://doi.org/10.1016/j.tcs.2019.12.036\"\u003eMorphic words, Beatty sequences and integer images of the Fibonacci language\u003c/a\u003e, Theoretical Computer Science  809,  407-417 (2020).",
				"W. Hebisch and M. Rubey, \u003ca href=\"http://arxiv.org/abs/math/0702086\"\u003eExtended Rate, more GFUN\u003c/a\u003e, arXiv:math/0702086 [math.CO], 2007-2010.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ReverendBacksAbbeyFloor.html\"\u003eReverend Back's Abbey Floor\u003c/a\u003e"
			],
			"formula": [
				"G.f. f(x) satisfies: (1+x+x^2) * f(x) - x^2 * f(x^3) = x * (1-x^4)^2 / ((1-x) * (1-x^2) * (1-x^6)) = A096285(x)/x^2. - _Michael Somos_, Jan 03 2011"
			],
			"example": [
				"01, 010110, 010110010110011010, ..."
			],
			"mathematica": [
				"m = maxExponent = 105;",
				"f[_] = 0;",
				"Do[f[x_] = (x^2 f[x^3] + ((1-x^4)^2 x)/((1-x)(1-x^2)(1-x^6)))/(1+x+x^2) + O[x]^m // Normal, {m}];",
				"CoefficientList[f[x], x] (* _Jean-François Alcover_, Aug 17 2018, after _Michael Somos_ *)"
			],
			"program": [
				"(PARI) a(n) = my(b=n%2,d); n=(n-1)\u003e\u003e1; while([n,d]=divrem(n,3);d==1,); d==2*b; \\\\ _Kevin Ryde_, Sep 29 2020"
			],
			"xref": [
				"Cf. A116178 (odd bisection), A136442 (even bisection)."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Eric W. Weisstein_, Apr 09 2006",
			"ext": [
				"Edited by _N. J. A. Sloane_, Feb 13 2008"
			],
			"references": 1,
			"revision": 31,
			"time": "2020-10-04T11:04:19-04:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}