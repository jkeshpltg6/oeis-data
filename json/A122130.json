{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A122130",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 122130,
			"data": "1,1,1,2,2,3,4,5,7,9,11,14,18,22,27,34,41,50,61,73,88,106,126,150,179,211,249,294,345,404,473,551,642,747,865,1002,1159,1336,1539,1771,2033,2331,2670,3052,3485,3976,4527,5150,5854,6642,7530,8529,9647,10902",
			"name": "Expansion of f(-x^4, -x^16) / psi(-x) in powers of x where psi() is a Ramanujan theta function and f(, ) is Ramanujan's general theta function.",
			"comment": [
				"Generating function arises naturally in Rodney Baxter's solution of the Hard Hexagon Model according to George Andrews.",
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"reference": [
				"G. E. Andrews, R. Askey and R. Roy, Special Functions, Cambridge University Press, 1999; Exercise 6(b), p. 591.",
				"G. E. Andrews, q-series, CBMS Regional Conference Series in Mathematics, 66, Amer. Math. Soc. 1986, see p. 8, Eq. (1.8). MR0858826 (88b:11063)"
			],
			"link": [
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of f(x, x^9) / f(-x^2, -x^3) in powers of x where f(, ) is Ramanujan's general theta function. - _Michael Somos_, Nov 12 2016",
				"Expansion of f(-x^2) * f(-x^20) / (f(-x) * f(-x^8, -x^12)) in powers of x where f(-x) : = f(-x, -x^2) and f(, ) is Ramanujan's general theta function.",
				"Euler transform of period 20 sequence [ 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, ...].",
				"G.f.: Sum_{k\u003e0} x^(k^2 - 1) / ((1 - x) * (1 - x^2) * ... * (1 - x^(2k-1))).",
				"G.f.: 1/(Product_{k\u003e0} (1-x^(2k-1))(1-x^(20k-8))(1-x^(20k-12))).",
				"a(n) ~ (3-sqrt(5))^(1/4) * exp(Pi*sqrt(2*n/5)) / (4*sqrt(5)*n^(3/4)). - _Vaclav Kotesovec_, Aug 30 2015"
			],
			"example": [
				"G.f. = 1 + x + x^2 + 2*x^3 + 2*x^4 + 3*x^5 + 4*x^6 + 5*x^7 + 7*x^8 + 9*x^9 + ...",
				"G.f. = q^31 + q^71 + q^111 + 2*q^151 + 2*q^191 + 3*q^231 + 4*q^271 + 5*q^311 + ..."
			],
			"mathematica": [
				"nmax = 100; CoefficientList[Series[Product[1/((1-x^(2*k-1))*(1-x^(20*k-8))*(1-x^(20*k-12))), {k, 1, nmax}], {x, 0, nmax}], x] (* _Vaclav Kotesovec_, Aug 30 2015 *)",
				"a[ n_] := SeriesCoefficient[ 1 / (QPochhammer[x, x^2] QPochhammer[x^8, x^20] QPochhammer[x^12, x^20]), {x, 0, n}]; (* _Michael Somos_, Nov 12 2016 *)",
				"a[ n_] := SeriesCoefficient[ Sqrt[2] x^(1/8) QPochhammer[ x^4, x^20] QPochhammer[ x^16, x^20] QPochhammer[x^20] / EllipticTheta[ 2, Pi/4, x^(1/2)], {x, 0, n}] // Simplify; (* _Michael Somos_, Nov 12 2016 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, n==0, polcoeff( sum(k=1, sqrtint(n+1), x^(k^2-1) / prod(i=1, 2*k-1, 1 - x^i, 1 + x * O(x^(n-k^2+1)))), n))};"
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_Michael Somos_, Aug 21 2006, corrected Aug 21 2006",
			"references": 1,
			"revision": 16,
			"time": "2021-03-12T22:24:44-05:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}