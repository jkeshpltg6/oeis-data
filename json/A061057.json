{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A061057",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 61057,
			"data": "0,1,1,2,2,6,2,18,54,30,36,576,127,840,928,3712,20160,93696,420480,800640,1305696,7983360,55056804,65318400,326592000,2286926400,2610934480,13680979200,18906930876,674165366496,326850970500,16753029012720,16880461678080",
			"name": "Factorial splitting: write n! = x*y with x \u003c= y and x maximal; sequence gives value of y-x.",
			"comment": [
				"Difference between central divisors of n!. - _Jaume Oliver Lafont_, Mar 13 2009",
				"For n \u003e 1, n! will never be a square, because of primes in the last half of the factors. Therefore the divisors of n! come in pairs (x,y) with x*y = n! and x \u003c y. The sequence gives the difference y-x between the pair nearest to the square root of n!. - _Alois P. Heinz_, Jul 06 2009",
				"a(n) = 2 iff n belongs to A146968. - _Max Alekseyev_, Feb 06 2010"
			],
			"link": [
				"Robert G. Wilson v, \u003ca href=\"/A061057/b061057.txt\"\u003eTable of n, a(n) for n = 1..41\u003c/a\u003e (first 40 terms from Max Alekseyev)"
			],
			"example": [
				"2! = 1*2, with difference of 1.",
				"3! = 2*3, with difference of 1.",
				"4! = 4*6, with difference of 2.",
				"5! = 10*12, with difference of 2.",
				"6! = 24*30, with difference of 6.",
				"7! = 70*72 with difference of 2.",
				"The corresponding central divisors are two units apart (equivalently, n!+1=A038507(n) is a square) for n = 4, 5, 7 (see A146968)."
			],
			"maple": [
				"A060777 := proc(n) local d,nd ; d := sort(convert(numtheory[divisors](n!),list)) ; nd := nops(d) ; op(floor(1+nd/2),d) ; end:",
				"A060776 := proc(n) local d,nd ; d := sort(convert(numtheory[divisors](n!),list)) ; nd := nops(d) ; op(floor(nd/2),d) ; end:",
				"A061057 := proc(n) A060777(n)-A060776(n) ; end:",
				"seq(A061057(n),n=2..27) ; # _R. J. Mathar_, Mar 14 2009"
			],
			"mathematica": [
				"Do[ With[ {k = Floor[ Sqrt[ x! ] ] - Do[ If[ Mod[ x!, Floor[ Sqrt[ x! ] ] - n ] == 0, Return[ n ] ], {n, 0, 10000000} ]}, Print[ {x, \"! =\", k, x!/k, x!/k - k} ] ], {x, 3, 22} ]",
				"f[n_] := Block[{k = Floor@ Sqrt[n! ]}, While[ Mod[n!, k] != 0, k-- ]; n!/k - k]; Table[f@n, {n, 2, 32}] (* _Robert G. Wilson v_, Jul 11 2009 *)",
				"Table[d=Divisors[n!]; len=Length[d]; If[OddQ[len], 0, d[[1 + len/2]] - d[[len/2]]], {n, 34}] (* _Vincenzo Librandi_, Jan 02 2016 *)"
			],
			"program": [
				"(PARI) for(k=2,25,d=divisors(k!);print(d[#d/2+1]-d[#d/2])) \\\\ _Jaume Oliver Lafont_, Mar 13 2009"
			],
			"xref": [
				"Cf. A061055, A061056, A061058, A061059, A061060.",
				"Cf. A061030, A061031, A061032, A061033.",
				"Cf. A005563, A038507.",
				"Cf. A038667. - _Robert G. Wilson v_, Jul 12 2009"
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Ed Pegg Jr_, May 28 2001",
			"ext": [
				"More terms from _Dean Hickerson_, Jun 13 2001",
				"Edited by _N. J. A. Sloane_ Jul 07 2009 at the suggestion of _R. J. Mathar_ and _Alois P. Heinz_"
			],
			"references": 6,
			"revision": 52,
			"time": "2019-10-13T02:56:02-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}