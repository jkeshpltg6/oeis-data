{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A304126",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 304126,
			"data": "1,12,308,8976,276276,8767512,283728368,9307523160,308397041460,10297160887440,345907798472808,11677249143964768,395812039652176368,13462467839121604380,459228293024549285160,15704603616059963888976,538244114669755948787508,18482918763808824442733616",
			"name": "a(n) = (6*n)!*(4*n)!/((2*n)!*(3*n)!*(5*n)!).",
			"comment": [
				"From Bober's reference, s. Theorem 1.2: Eq.(9) for a=3, b=2 gives a(n)."
			],
			"link": [
				"J. W. Bober, \u003ca href=\"https://arxiv.org/abs/0709.1977\"\u003eFactorial ratios, hypergeometric series, and a family of step functions\u003c/a\u003e, arXiv:0709.1977 [math.NT], 2007; J. London Math. Soc. (2) 79 (2009), 422-444."
			],
			"formula": [
				"O.g.f.: hypergeom([1/6,1/4,1/2,3/4,5/6],[1/5,2/5,3/5,4/5],110592*z/3125).",
				"a(n) ~ sqrt(10)*sqrt(1/n)*(32248627200000*n^4 - 1657221120000*n^3 + 42581376000*n^2 + 12688032960*n - 680136331)*3125^(-n)*110592^n/(161243136000000*n^4*sqrt(Pi)).",
				"Integral representation as the n-th moment of the positive function V(x) on x = (0, 110592/3125), i.e. in Maple notation: a(n) = int(x^n*V(x),x=0..110592/3125), where V(x) = 2^(1/3)*hypergeom([1/6, 11/30, 17/30, 23/30, 29/30], [1/3, 5/12, 2/3, 11/12], 3125*x*(1/110592))/(12*Pi*x^(5/6))+sqrt(2)*hypergeom([1/4, 9/20, 13/20, 17/20, 21/20], [5/12, 1/2, 3/4, 13/12], 3125*x*(1/110592))/(16*Pi*x^(3/4)) +cos((1/5)*Pi)*cos(2*Pi*(1/5))*hypergeom([1/2, 7/10, 9/10, 11/10, 13/10], [2/3, 3/4, 5/4, 4/3], 3125*x*(1/110592))/(16*Pi*sqrt(x))+11*sqrt(2)*hypergeom([3/4, 19/20, 23/20, 27/20, 31/20], [11/12, 5/4, 3/2, 19/12], 3125*x*(1/110592))/ (2048*Pi*x^(1/4))+247*2^(2/3)*hypergeom([5/6, 31/30, 37/30, 43/30, 49/30], [13/12, 4/3, 19/12, 5/3], 3125*x*(1/110592))/(110592*Pi*x^(1/6)). The function V(x) is singular at both edges of its support and is U-shaped. The function V(x) is unique as it is the solution of the Hausdorff moment problem.",
				"a(n) = (2^(10*n)*Gamma(1/2 + 2*n)*Gamma(1/2 + 3*n))/(Pi*Gamma(1 + 5*n)). - _Peter Luschny_, May 07 2018"
			],
			"maple": [
				"seq((6*n)!*(4*n)!/((2*n)!*(3*n)!*(5*n)!),n=0..17);",
				"# Alternative (to illustrate the constant C):",
				"k := [10,15,30,45,50]/60: j := [12,24,36,48,60]/60: C := 110592/3125:",
				"a := n -\u003e C^n*mul(pochhammer(k[i],n)/pochhammer(j[i],n), i=1..5): # _Peter Luschny_, May 07 2018"
			],
			"mathematica": [
				"a[n_] := (2^(10 n) Gamma[1/2 + 2 n] Gamma[1/2 + 3 n])/(Pi Gamma[1 + 5 n]);",
				"Table[a[n], {n, 0, 17}] (* _Peter Luschny_, May 07 2018 *)",
				"Table[((6n)!(4n)!)/((2n)!(3n)!(5n)!),{n,0,20}] (* _Harvey P. Dale_, Jul 26 2019 *)"
			],
			"xref": [
				"Cf. A295431."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Karol A. Penson_, May 07 2018",
			"references": 1,
			"revision": 34,
			"time": "2019-07-26T16:45:07-04:00",
			"created": "2018-05-07T12:04:05-04:00"
		}
	]
}