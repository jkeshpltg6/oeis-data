{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A138504",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 138504,
			"data": "1,4,-4,-32,-4,104,32,-192,-4,292,-104,-480,32,680,192,-832,-4,1160,-292,-1440,-104,1536,480,-2112,32,2604,-680,-2624,192,3368,832,-3840,-4,3840,-1160,-4992,-292,5480,1440,-5440,-104,6728,-1536,-7392,480,7592,2112,-8832,32,9412,-2604,-9280",
			"name": "Expansion of (eta(q^2)^9 / (eta(q)^2 * eta(q^4)^4))^2 in powers of q.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A138504/b138504.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of (phi(q) * phi(-q^2)^2)^2 in powers of q where phi() is a Ramanujan theta function.",
				"Euler transform of period 4 sequence [ 4, -14, 4, -6, ...].",
				"a(n) = 4 * b(n) where a(0) = 1, b(n) is multiplicative with b(2^e) = -1 if e\u003e0, b(p^e) = ((p^2)^(e+1) - 1) / (p^2 - 1) if p == 1 (mod 4), b(p^e) = ((-p^2)^(e+1) - 1) / ( -p^2 - 1) if p == 3 (mod 4).",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (4 t)) = 32 (t/i)^3 g(t) where q = exp(2 Pi i t) and g() is the g.f. for A122854.",
				"G.f.: 1 + 4 * Sum_{k\u003e0} -(-1)^k * (2*k-1)^2 * x^(2*k-1) / (1 + x^(2*k-1)).",
				"a(n) = (-1)^n * A120030(n). a(n) = 4 * A138505(n) unless n=0."
			],
			"example": [
				"G.f. = 1 + 4*q - 4*q^2 - 32*q^3 - 4*q^4 + 104*q^5 + 32*q^6 - 192*q^7 - 4*q^8 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ (QPochhammer[ q^2]^9 / (QPochhammer[ q]^2 QPochhammer[ q^4]^4))^2, {q, 0, n}]; (* _Michael Somos_, May 24 2015 *)",
				"a[ n_] := If[ n \u003c 1, Boole[n == 0], -4 DivisorSum[ n, #^2 KroneckerSymbol[ -4, #] (-1)^(n/#) \u0026]]; (* _Michael Somos_, May 24 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, n==0, -4 * sumdiv(n, d, d^2 * kronecker(-4, d) * (-1)^(n/d)))};",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( (eta(x^2 + A)^9 / (eta(x + A)^2 * eta(x^4 + A)^4))^2, n))};"
			],
			"xref": [
				"Cf. A120030, A138505."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Mar 21 2008",
			"references": 2,
			"revision": 13,
			"time": "2021-03-12T22:24:45-05:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}