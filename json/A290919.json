{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A290919",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 290919,
			"data": "4,18,72,271,976,3398,11516,38179,124272,398248,1259240,3935420,12173440,37314700,113452128,342426657,1026711724,3059968146,9069834488,26748151221,78518859336,229505772002,668173273988,1938126895864,5602502738380,16143099833606",
			"name": "p-INVERT of the positive integers, where p(S) = (1 - S)^4.",
			"comment": [
				"Suppose s = (c(0), c(1), c(2), ...) is a sequence and p(S) is a polynomial. Let S(x) = c(0)*x + c(1)*x^2 + c(2)*x^3 + ... and T(x) = (-p(0) + 1/p(S(x)))/x. The p-INVERT of s is the sequence t(s) of coefficients in the Maclaurin series for T(x). Taking p(S) = 1 - S gives the \"INVERT\" transform of s, so that p-INVERT is a generalization of the \"INVERT\" transform (e.g., A033453).",
				"See A290890 for a guide to related sequences."
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A290919/b290919.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_08\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (12, -58, 144, -195, 144, -58, 12, -1)"
			],
			"formula": [
				"G.f.: (4 - 30 x + 88 x^2 - 125 x^3 + 88 x^4 - 30 x^5 + 4 x^6)/(1 - 3 x + x^2)^4.",
				"a(n) = 12*a(n-1) - 58*a(n-2) + 144*a(n-3) - 195*a(n-4) + 144*a(n-5) - 58*a(n-6) + 12*a(n-7) - a(n-8).",
				"(a(n)) is the p-INVERT of (1,1,1,1,1...) using p(S) = (1 - S - S^2)^4."
			],
			"mathematica": [
				"z = 60; s = x/(1 - x)^2; p = (1 - s)^4;",
				"Drop[CoefficientList[Series[s, {x, 0, z}], x], 1] (* A000027 *)",
				"Drop[CoefficientList[Series[1/p, {x, 0, z}], x], 1]  (* A290919 *)"
			],
			"xref": [
				"Cf. A000027, A290890."
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "_Clark Kimberling_, Aug 18 2017",
			"references": 2,
			"revision": 11,
			"time": "2017-08-24T12:58:00-04:00",
			"created": "2017-08-18T21:38:53-04:00"
		}
	]
}