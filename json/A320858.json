{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A320858",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 320858,
			"data": "0,-1,0,1,0,1,0,-1,0,1,2,3,2,1,2,3,2,3,2,3,2,3,2,1,0,1,2,1,2,1,2,1,0,-1,0,1,2,1,2,3,2,3,4,3,4,5,4,5,4,5,4,5,4,3,2,3,4,5,6,5,4,5,4,5,4,5,4,3,2,3,2,3,4,5,4,5,6,7,6,5,4,5,6,5,6,5,4",
			"name": "a(n) = A320857(prime(n)).",
			"comment": [
				"Among the first 10000 terms there are only 100 negative ones.",
				"In general, assuming the strong form of RH, if 0 \u003c a, b \u003c k, gcd(a, k) = gcd(b, k) = 1, a is a quadratic residue and b is a quadratic nonresidue mod n, then Pi(k,b)(n) \u003e Pi(k,a)(n) occurs more often than not. Pi(a,b)(x) denotes the number of primes in the arithmetic progression a*k + b less than or equal to x. This phenomenon is called \"Chebyshev's bias\". Here, although 3 is not a quadratic residue modulo 8, for most n we have Pi(8,5)(n) + Pi(8,7)(n) \u003e Pi(8,1)(n) - Pi(8,3)(n), Pi(8,3)(n) + Pi(8,7)(n) \u003e Pi(8,1)(n) + Pi(8,5)(n) and Pi(8,5)(n) + Pi(8,7)(n) \u003e Pi(8,1)(n) + Pi(8,7)(n)."
			],
			"link": [
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Chebyshev%27s_bias\"\u003eChebyshev's bias\u003c/a\u003e"
			],
			"formula": [
				"a(n) = -Sum_{i=1..n} Kronecker(prime(i),2) = -Sum_{primes p\u003c=n} Kronecker(2,prime(i)) = -Sum_{i=1..n} A091337(prime(i))."
			],
			"example": [
				"prime(46) = 199, Pi(8,1)(199) = 8, Pi(8,5)(199) = 13, Pi(8,3)(199) = Pi(8,7)(199) = 12, so a(46) = 13 + 12 - 8 - 12 = 5."
			],
			"mathematica": [
				"a[n_] := -Sum[KroneckerSymbol[-2, Prime[i]], {i, 1, n}];",
				"Array[a, 100] (* _Jean-François Alcover_, Dec 28 2018, from PARI *)"
			],
			"program": [
				"(PARI) a(n) = -sum(i=1, n, kronecker(-2, prime(i)))"
			],
			"xref": [
				"Cf. A188510.",
				"Let d be a fundamental discriminant.",
				"Sequences of the form \"a(n) = -Sum_{primes p\u003c=n} Kronecker(d,p)\" with |d| \u003c= 12: A321860 (d=-11), A320857 (d=-8), A321859 (d=-7), A066520 (d=-4), A321856 (d=-3), A321857 (d=5), A071838 (d=8), A321858 (d=12).",
				"Sequences of the form \"a(n) = -Sum_{i=1..n} Kronecker(d,prime(i))\" with |d| \u003c= 12: A321865 (d=-11), this sequence (d=-8), A321864 (d=-7), A038698 (d=-4), A112632 (d=-3), A321862 (d=5), A321861 (d=8), A321863 (d=12)."
			],
			"keyword": "sign",
			"offset": "1,11",
			"author": "_Jianing Song_, Nov 24 2018",
			"references": 14,
			"revision": 16,
			"time": "2018-12-28T03:55:50-05:00",
			"created": "2018-12-22T16:48:52-05:00"
		}
	]
}