{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A117427",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 117427,
			"data": "1,2,1,4,4,1,9,14,6,1,23,50,28,8,1,66,191,126,48,10,1,209,780,572,264,70,12,1,724,3415,2692,1440,438,98,14,1,2722,15924,13203,7990,2646,698,130,16,1,11054,78595,67542,45566,15920,4702,1029,168,18,1,48221,408337,360117,268072,96848,31030,7536,1472,206,20,1",
			"name": "Triangle T, read by rows, equal to the matrix square of triangle A117418; also equals a column bisection of triangle A117418: column 2k+1 of T^(1/2) equals column k of T.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A117427/b117427.txt\"\u003eRows n = 0..50 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = A117418(n+k+1, 2*k+1). - _G. C. Greubel_, May 31 2021"
			],
			"example": [
				"Triangle T begins:",
				"      1;",
				"      2,     1;",
				"      4,     4,     1;",
				"      9,    14,     6,     1;",
				"     23,    50,    28,     8,     1;",
				"     66,   191,   126,    48,    10,    1;",
				"    209,   780,   572,   264,    70,   12,    1;",
				"    724,  3415,  2692,  1440,   438,   98,   14,   1;",
				"   2722, 15924, 13203,  7990,  2646,  698,  130,  16,  1;",
				"  11054, 78595, 67542, 45566, 15920, 4702, 1029, 168, 18, 1;",
				"The matrix square-root T^(1/2) = A117418, which begins:",
				"  1;",
				"  1,   1;",
				"  1,   2,   1;",
				"  1,   4,   3,   1;",
				"  1,   9,   8,   4,  1;",
				"  1,  23,  22,  14,  5,  1;",
				"  1,  66,  65,  50, 20,  6, 1;",
				"  1, 209, 208, 191, 79, 28, 7, 1;",
				"where column 2k+1 of T^(1/2) equals column k of T for k\u003e=0."
			],
			"mathematica": [
				"A117418[n_, k_]:= A117418[n, k]= If[k\u003c0 || k\u003en, 0, If[k==0 || k==n, 1, If[k==n-1, n, Sum[A117418[n-Floor[(k+1)/2], Floor[k/2] +j]*A117418[Floor[(k-1)/2] +j, Floor[(k-1)/2]], {j, 0, n-k}] ]]];",
				"A117427[n_, k_]:= A117418[n+k+1, 2*k+1];",
				"Table[A117427[n, k], {n,0,12}, {k,0,n}]//Flatten (* _G. C. Greubel_, May 31 2021 *)"
			],
			"program": [
				"(Sage)",
				"@CachedFunction",
				"def A117418(n, k):",
				"    if (k\u003c0 or k\u003en): return 0",
				"    elif (k==0 or k==n): return 1",
				"    elif (k==n-1): return n",
				"    else: return sum( A117418(n -(k+1)//2, k//2 +j)*A117418((k-1)//2 +j, (k-1)//2) for j in (0..n-k))",
				"def A117427(n,k): return A117418(n+k+1,2*k+1)",
				"flatten([[A117427(n, k) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, May 31 2021"
			],
			"xref": [
				"Cf. A117418 (T^(1/2)), A117425 (dual), A117428 (row sums)."
			],
			"keyword": "nonn,tabl",
			"offset": "0,2",
			"author": "_Paul D. Hanna_, Mar 14 2006",
			"references": 4,
			"revision": 8,
			"time": "2021-05-31T03:56:23-04:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}