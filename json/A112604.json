{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A112604",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 112604,
			"data": "1,0,1,2,0,2,1,0,0,2,0,0,3,0,2,2,0,0,2,0,1,0,0,2,2,0,0,2,0,2,1,0,2,4,0,0,0,0,0,2,0,0,3,0,0,2,0,2,2,0,2,0,0,0,4,0,1,2,0,2,2,0,0,0,0,0,0,0,4,2,0,0,1,0,0,4,0,2,2,0,0,2,0,2,2,0,0,2,0,0,3,0,0,2,0,2,0,0,0,2,0,0,2,0,2",
			"name": "Number of representations of n as a sum of three times a square and two times a triangular number.",
			"comment": [
				"Ramanujan theta functions: f(q) := Prod_{k\u003e=1} (1-(-q)^k) (see A121373), phi(q) := theta_3(q) := Sum_{k=-oo..oo} q^(k^2) (A000122), psi(q) := Sum_{k=0..oo} q^(k*(k+1)/2) (A010054), chi(q) := Prod_{k\u003e=0} (1+q^(2k+1)) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A112604/b112604.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"M. D. Hirschhorn, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2004.08.045\"\u003eThe number of representations of a number by various forms\u003c/a\u003e, Discrete Mathematics 298 (2005), 205-211.",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A002324(4n+1) = A033762(2n) = d_{1, 3}(4n+1) - d_{2, 3}(4n+1) where d_{a, m}(n) equals the number of divisors of n which are congruent to a mod m.",
				"Euler transform of period 12 sequence [0,1,2,-1,0,-2,0,-1,2,1,0,-2,...]. - _Michael Somos_, Feb 14 2006",
				"Number of representations of 2n as a sum of three times a triangular number and a triangular number.",
				"Expansion of (psi(q)psi(q^3)+psi(-q)psi(-q^3))/2 in powers of q^2 where psi() is a Ramanujan theta function. - _Michael Somos_, Feb 14 2006",
				"G.f.: (Sum_{k} x^k^2)^3*(Sum_{k\u003e0} x^((k^2-k)/2))^2 = Product_{k\u003e0} (1-x^(4k))(1-x^(6k))(1+x^(2k))(1+x^(3k))^2/(1+x^(6k))^2. - _Michael Somos_, Feb 14 2006",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (48 t)) = 3^(1/2) (t/i) g(t) where q = exp(2 Pi i t) and g() is g.f. for A164272. - _Michael Somos_, Aug 11 2009",
				"a(3*n + 1) = 0. - _Michael Somos_, Aug 11 2009"
			],
			"example": [
				"a(12) = 3 since we can write 12 = 3(2)^2 + 0 = 3(-2)^2 + 0 = 0 + 2*6.",
				"2*12=24=3*1+21=3*3+15=3*6+6 so a(12)=3.",
				"1 + x^2 + 2*x^3 + 2*x^5 + x^6 + 2*x^9 + 3*x^12 + 2*x^14 + 2*x^15 + ... - _Michael Somos_, Aug 11 2009",
				"q + q^9 + 2*q^13 + 2*q^21 + q^25 + 2*q^37 + 3*q^49 + 2*q^57 + 2*q^61 + ... - _Michael Somos_, Aug 11 2009"
			],
			"mathematica": [
				"a[n_] := DivisorSum[4n+1, Switch[Mod[#, 3], 1, 1, 2, -1, 0, 0]\u0026]; Table[ a[n], {n, 0, 104}] (* _Jean-François Alcover_, Dec 04 2015, adapted from PARI *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c0, 0, n=4*n+1; sumdiv(n,d,(d%3==1)-(d%3==2)))",
				"(PARI) {a(n)=local(A); if(n\u003c0, 0, A=x*O(x^n); polcoeff( eta(x^6+A)^5/eta(x^2+A)*(eta(x^4+A)/eta(x^3+A)/eta(x^12+A))^2, n))} /* _Michael Somos_, Feb 14 2006 */"
			],
			"xref": [
				"A112606(n) = a(2*n). 2 * A112607(n) = a(2*n + 1). A123884(n) = a(3*n). A112605(n) = a(3*n + 2). A131961(n) = a(6*n). A112608(n) =a(6*n + 2). 2 * A131963(n) = a(6*n + 3). 2 * A112609(n) = a(6*n + 5). - _Michael Somos_, Aug 11 2009"
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_James A. Sellers_, Dec 21 2005",
			"references": 27,
			"revision": 20,
			"time": "2021-03-12T22:24:43-05:00",
			"created": "2006-01-24T03:00:00-05:00"
		}
	]
}