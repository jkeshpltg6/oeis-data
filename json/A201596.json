{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A201596",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 201596,
			"data": "6,24,30,90,150,156,210,240,306,366,384,444,810,834,1086,1200,1326,2316,3876,4230,4350,8244,8880,9450,10686,10950,11784,12816,13554,15504,15576,16254,16506,16596,19446,19944,21516,38340,39990,41556,45786,47190,48246,59856",
			"name": "Record (maximal) gaps between prime triples (p, p+4, p+6).",
			"comment": [
				"Prime triples (p, p+4, p+6) are one of the two types of densest permissible constellations of 3 primes (A022004 and A022005). By the Hardy-Littlewood k-tuple conjecture, average gaps between prime k-tuples are O(log^k(p)), with k=3 for triples. If a gap is larger than any preceding gap, we call it a maximal gap, or a record gap. Maximal gaps may be significantly larger than average gaps; this sequence suggests that maximal gaps between triples are O(log^4(p)).",
				"A201597 lists initial primes p in triples (p, p+4, p+6) preceding the maximal gaps. A233435 lists the corresponding primes p at the end of the maximal gaps."
			],
			"link": [
				"Alexei Kourbatov, \u003ca href=\"/A201596/b201596.txt\"\u003eTable of n, a(n) for n = 1..79\u003c/a\u003e",
				"T. Forbes, \u003ca href=\"http://anthony.d.forbes.googlepages.com/ktuplets.htm\"\u003ePrime k-tuplets\u003c/a\u003e",
				"G. H. Hardy and J. E. Littlewood, \u003ca href=\"https://dx.doi.org/10.1007/BF02403921\"\u003eSome problems of 'Partitio numerorum'; III: on the expression of a number as a sum of primes\u003c/a\u003e, Acta Mathematica, Vol. 44, pp. 1-70, 1923.",
				"Alexei Kourbatov, \u003ca href=\"http://www.javascripter.net/math/primes/maximalgapsbetweenktuples.htm\"\u003eMaximal gaps between prime k-tuples\u003c/a\u003e",
				"A. Kourbatov, \u003ca href=\"http://arxiv.org/abs/1301.2242\"\u003eMaximal gaps between prime k-tuples: a statistical approach\u003c/a\u003e, arXiv preprint arXiv:1301.2242 [math.NT], 2013 and \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL16/Kourbatov/kourbatov3.html\"\u003eJ. Int. Seq. 16 (2013) #13.5.2\u003c/a\u003e",
				"Alexei Kourbatov, \u003ca href=\"http://arxiv.org/abs/1309.4053\"\u003eTables of record gaps between prime constellations\u003c/a\u003e, arXiv preprint arXiv:1309.4053 [math.NT], 2013.",
				"Alexei Kourbatov, \u003ca href=\"http://arxiv.org/abs/1401.6959\"\u003eThe distribution of maximal prime gaps in Cramer's probabilistic model of primes\u003c/a\u003e, arXiv preprint arXiv:1401.6959 [math.NT], 2014.",
				"Alexei Kourbatov and Marek Wolf, \u003ca href=\"http://arxiv.org/abs/1901.03785\"\u003ePredicting maximal gaps in sets of primes\u003c/a\u003e, arXiv preprint arXiv:1901.03785 [math.NT], 2019.",
				"Eric W. Weisstein, \u003ca href=\"http://mathworld.wolfram.com/k-TupleConjecture.html\"\u003ek-Tuple Conjecture\u003c/a\u003e"
			],
			"formula": [
				"Gaps between prime triples (p, p+4, p+6) are smaller than 0.35*(log p)^4, where p is the prime at the end of the gap. There is no rigorous proof of this formula. The O(log^4(p)) growth rate is suggested by numerical data and heuristics based on probability considerations."
			],
			"example": [
				"The gap of 6 between triples starting at p=7 and p=13 is the very first gap, so a(1)=6. The gap of 24 between triples starting at p=13 and p=37 is a maximal gap - larger than any preceding gap; therefore a(2)=24. The gap of 30 between triples at p=37 and p=67 is again a maximal gap, so a(3)=30. The next gap is smaller, so it does not contribute to the sequence."
			],
			"xref": [
				"Cf. A022005 (prime triples p, p+4, p+6), A113274, A113404, A200503, A201598, A201062, A201073, A201051, A201251, A202281, A202361, A201597, A233435."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Alexei Kourbatov_, Dec 03 2011",
			"references": 11,
			"revision": 49,
			"time": "2019-01-19T04:14:58-05:00",
			"created": "2011-12-03T12:11:50-05:00"
		}
	]
}