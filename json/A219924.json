{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A219924",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 219924,
			"data": "1,1,1,1,1,1,1,1,1,1,1,1,2,1,1,1,1,3,3,1,1,1,1,5,6,5,1,1,1,1,8,13,13,8,1,1,1,1,13,28,40,28,13,1,1,1,1,21,60,117,117,60,21,1,1,1,1,34,129,348,472,348,129,34,1,1,1,1,55,277,1029,1916,1916,1029,277,55,1,1",
			"name": "Number A(n,k) of tilings of a k X n rectangle using integer-sided square tiles; square array A(n,k), n\u003e=0, k\u003e=0, read by antidiagonals.",
			"comment": [
				"For drawings of A(1,1), A(2,2), ..., A(5,5) see A224239."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A219924/b219924.txt\"\u003eAntidiagonals n = 0..30, flattened\u003c/a\u003e",
				"Steve Butler, Jason Ekstrand, Steven Osborne, \u003ca href=\"https://doi.org/10.1007/978-3-030-37853-0_5\"\u003eCounting Tilings by Taking Walks in a Graph\u003c/a\u003e, A Project-Based Guide to Undergraduate Research in Mathematics, Birkhäuser, Cham (2020), see page 169."
			],
			"example": [
				"A(3,3) = 6, because there are 6 tilings of a 3 X 3 rectangle using integer-sided squares:",
				"  ._____.  ._____.  ._____.  ._____.  ._____.  ._____.",
				"  |     |  |   |_|  |_|   |  |_|_|_|  |_|_|_|  |_|_|_|",
				"  |     |  |___|_|  |_|___|  |_|   |  |   |_|  |_|_|_|",
				"  |_____|  |_|_|_|  |_|_|_|  |_|___|  |___|_|  |_|_|_|",
				"Square array A(n,k) begins:",
				"  1,  1,  1,   1,    1,    1,     1,      1, ...",
				"  1,  1,  1,   1,    1,    1,     1,      1, ...",
				"  1,  1,  2,   3,    5,    8,    13,     21, ...",
				"  1,  1,  3,   6,   13,   28,    60,    129, ...",
				"  1,  1,  5,  13,   40,  117,   348,   1029, ...",
				"  1,  1,  8,  28,  117,  472,  1916,   7765, ...",
				"  1,  1, 13,  60,  348, 1916, 10668,  59257, ...",
				"  1,  1, 21, 129, 1029, 7765, 59257, 450924, ..."
			],
			"maple": [
				"b:= proc(n, l) option remember; local i, k, s, t;",
				"      if max(l[])\u003en then 0 elif n=0 or l=[] then 1",
				"    elif min(l[])\u003e0 then t:=min(l[]); b(n-t, map(h-\u003eh-t, l))",
				"    else for k do if l[k]=0 then break fi od; s:=0;",
				"         for i from k to nops(l) while l[i]=0 do s:=s+",
				"           b(n, [l[j]$j=1..k-1, 1+i-k$j=k..i, l[j]$j=i+1..nops(l)])",
				"         od; s",
				"      fi",
				"    end:",
				"A:= (n, k)-\u003e `if`(n\u003e=k, b(n, [0$k]), b(k, [0$n])):",
				"seq(seq(A(n, d-n), n=0..d), d=0..14);",
				"# The following is a second version of the program that lists the actual dissections. It produces a list of pairs for each dissection:",
				"b:= proc(n, l, ll) local i, k, s, t;",
				"      if max(l[])\u003en then 0 elif n=0 or l=[] then lprint(ll); 1",
				"    elif min(l[])\u003e0 then t:=min(l[]); b(n-t, map(h-\u003eh-t, l), ll)",
				"    else for k do if l[k]=0 then break fi od; s:=0;",
				"         for i from k to nops(l) while l[i]=0 do s:=s+",
				"           b(n, [l[j]$j=1..k-1, 1+i-k$j=k..i, l[j]$j=i+1..nops(l)],",
				"            [ll[],[k,1+i-k]])",
				"         od; s",
				"      fi",
				"    end:",
				"A:= (n, k)-\u003e b(k, [0$n], []):",
				"A(5,5);",
				"# In each list [a,b] means put a square with side length b at",
				"leftmost possible position with upper corner in row a.  For example",
				"[[1,3], [4,2], [4,2], [1,2], [3,1], [3,1], [4,1], [5,1]], gives:",
				"._____.___.",
				"|     |   |",
				"|     |___|",
				"|_____|_|_|",
				"|   |   |_|",
				"|___|___|_|"
			],
			"mathematica": [
				"b[n_, l_List] := b[n, l] = Module[{i, k, s, t}, Which[Max[l] \u003e n, 0, n == 0 || l == {}, 1, Min[l] \u003e 0, t = Min[l]; b[n-t, l-t], True, k = Position[l, 0, 1][[1, 1]]; s = 0; For[i = k, i \u003c= Length[l] \u0026\u0026 l[[i]] == 0, i++, s = s + b[n, Join[l[[1;; k-1]], Table[1+i-k, {j, k, i}], l[[i+1;; -1]] ] ] ]; s]]; a[n_, k_] := If[n \u003e= k, b[n, Array[0\u0026, k]], b[k, Array[0\u0026, n]]]; Table[Table[a[n, d-n], {n, 0, d}], {d, 0, 14}] // Flatten (* _Jean-François Alcover_, Dec 13 2013, translated from 1st Maple program *)"
			],
			"xref": [
				"Columns (or rows) k=0+1, 2-10 give: A000012, A000045(n+1), A002478, A054856, A054857, A219925, A219926, A219927, A219928, A219929.",
				"Main diagonal gives A045846.",
				"Cf. A113881, A226545."
			],
			"keyword": "nonn,tabl",
			"offset": "0,13",
			"author": "_Alois P. Heinz_, Dec 01 2012",
			"references": 24,
			"revision": 40,
			"time": "2021-09-05T18:20:49-04:00",
			"created": "2012-12-01T16:44:20-05:00"
		}
	]
}