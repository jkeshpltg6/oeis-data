{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A244339",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 244339,
			"data": "1,-4,6,-4,0,0,6,-8,6,-4,0,0,0,-8,12,0,0,0,6,-8,0,-8,0,0,6,-4,12,-4,0,0,0,-8,6,0,0,0,0,-8,12,-8,0,0,12,-8,0,0,0,0,0,-12,6,0,0,0,6,0,12,-8,0,0,0,-8,12,-8,0,0,0,-8,0,0,0,0,6,-8,12,-4,0",
			"name": "Expansion of (-2 * a(q) + 3*a(q^2) + 2*a(q^4)) / 3 in powers of q where a() is a cubic AGM theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Cubic AGM theta functions: a(q) (see A004016), b(q) (A005928), c(q) (A005882)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A244339/b244339.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of b(q) * (b(q) + 2*b(q^4)) / (3 * b(q^2)) in powers of q where b() is a cubic AGM theta function.",
				"Expansion of psi(-q) * chi(-q)^3 * phi(q^3) * chi(q^3)^3 in powers of q where phi(), psi(), chi() are Ramanujan theta functions.",
				"Expansion of eta(q)^4 * eta(q^4) * eta(q^6)^8 / (eta(q^2)^4 * eta(q^3)^4 * eta(q^12)^3) in powers of q.",
				"Euler transform of period 12 sequence [ -4, 0, 0, -1, -4, -4, -4, -1, 0, 0, -4, -2, ...].",
				"Moebius transform is period 12 sequence [ -4, 10, 0, -6, 4, 0, -4, 6, 0, -10, 4, 0, ...].",
				"a(n) = -4 * b(n) where b(n) is multiplicative with b(2^e) = (1 - (-1)^e) * -3/4 if e\u003e0, b(3^e) = 1, b(p^e) = e+1 if p == 1 (mod 6), b(p^e) = (1 + (-1)^e) / 2 if p == 5 (mod 6) with a(0) = 1.",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (12 t)) = 48^(1/2) (t/i) g(t) where q = exp(2 Pi i t) and g() is the g.f. for A244375.",
				"a(2*n) = A004016(n). a(2*n + 1) = -4 * A033762(n). a(3*n) = a(n). a(6*n + 1) = -4 * A097195(n). a(6*n + 5) = 0."
			],
			"example": [
				"G.f. = 1 - 4*q + 6*q^2 - 4*q^3 + 6*q^6 - 8*q^7 + 6*q^8 - 4*q^9 - 8*q^13 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 1, Boole[n == 0], 2 Sum[ (-1)^(n/d) {2, -1, 0, 1, -2, 0}[[ Mod[ d, 6, 1] ]], {d, Divisors @ n}]];",
				"a[ n_] := SeriesCoefficient[ QPochhammer[ q]^4 QPochhammer[ q^4] QPochhammer[ q^6]^8 / (QPochhammer[ q^2]^4 QPochhammer[ q^3]^4 QPochhammer[ q^12]^3), {q, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, n==0, 2 * sumdiv(n, d, (-1)^(n/d) * [0, 2, -1, 0, 1, -2][d%6 + 1]))};",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A)^4 * eta(x^4 + A) * eta(x^6 + A)^8 / (eta(x^2 + A)^4 * eta(x^3 + A)^4 * eta(x^12 + A)^3), n))};",
				"(PARI) {a(n) = if( n\u003c0, 0, polcoeff( 1 + 2 * sum(k=1, n, x^k / (1 + x^k) * [0, -2, 1, 0, -1, 2][k%6 + 1], x * O(x^n)), n))};",
				"(PARI) {a(n) = if( n\u003c0, 0, polcoeff( 1 + 2 * sum(k=1, n, x^k / (1 + x^k + x^(2*k)) * [3, -2, 1, -2][k%4 + 1], x * O(x^n)), n))};",
				"(PARI) {a(n) = my(A);  if( n\u003c1, n==0, A = factor(n); -4 * prod( j=1, matsize(A)[1], if( p = A[j,1], e = A[j,2]; if( p==2, (1 - (-1)^e) * -3/4, if( p==3, 1, if( p%6 == 1, e+1, (1 + (-1)^e) / 2))))))};"
			],
			"xref": [
				"Cf. A004016, A033762, A097195, A244375."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Jun 26 2014",
			"references": 3,
			"revision": 13,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2014-06-26T03:26:06-04:00"
		}
	]
}