{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A053646",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 53646,
			"data": "0,0,1,0,1,2,1,0,1,2,3,4,3,2,1,0,1,2,3,4,5,6,7,8,7,6,5,4,3,2,1,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25",
			"name": "Distance to nearest power of 2.",
			"comment": [
				"Sum_{j=1..2^(k+1)} a(j) = A002450(k) = (4^k - 1)/3. - _Klaus Brockhaus_, Mar 17 2003"
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A053646/b053646.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Klaus Brockhaus, \u003ca href=\"/A053646/a053646.gif\"\u003eIllustration for A053646, A081252, A081253 and A081254\u003c/a\u003e",
				"Hsien-Kuei Hwang, S. Janson, T.-H. Tsai, \u003ca href=\"http://140.109.74.92/hk/wp-content/files/2016/12/aat-hhrr-1.pdf\"\u003eExact and asymptotic solutions of the recurrence f(n) = f(floor(n/2)) + f(ceiling(n/2)) + g(n): theory and applications\u003c/a\u003e, Preprint 2016.",
				"Hsien-Kuei Hwang, S. Janson, T.-H. Tsai, \u003ca href=\"https://doi.org/10.1145/3127585\"\u003eExact and Asymptotic Solutions of a Divide-and-Conquer Recurrence Dividing at Half: Theory and Applications\u003c/a\u003e, ACM Transactions on Algorithms, 13:4 (2017), #47; DOI: 10.1145/3127585.",
				"\u003ca href=\"/index/Di#distance_to_the_nearest\"\u003eIndex entries for sequences related to distance to nearest element of some set\u003c/a\u003e"
			],
			"formula": [
				"a(2^k+i) = i for 1 \u003c= i \u003c= 2^(k-1); a(3*2^k+i) = 2^k-i for 1 \u003c= i \u003c= 2^k; (Sum_{k=1..n} a(k))/n^2 is bounded. - _Benoit Cloitre_, Aug 17 2002",
				"a(n) = min(n-2^floor(log(n)/log(2)), 2*2^floor(log(n)/log(2))-n). - _Klaus Brockhaus_, Mar 08 2003"
			],
			"example": [
				"a(10)=2 since 8 is closest power of 2 to 10 and |8-10| = 2."
			],
			"maple": [
				"a:= n-\u003e (h-\u003e min(n-h, 2*h-n))(2^ilog2(n)):",
				"seq(a(n), n=1..100);  # _Alois P. Heinz_, Mar 28 2021"
			],
			"mathematica": [
				"np2[n_]:=Module[{min=Floor[Log[2,n]],max},max=min+1;If[2^max-n\u003cn-2^min, 2^max-n, n-2^min]]; np2/@Range[90] (* _Harvey P. Dale_, Feb 21 2012 *)"
			],
			"program": [
				"(PARI) a(n)=vecmin(vector(n,i,abs(n-2^(i-1))))",
				"(PARI) for(n=1,89,p=2^floor(0.1^25+log(n)/log(2)); print1(min(n-p,2*p-n),\",\"))",
				"(PARI) a(n) = my (p=#binary(n)); return (min(n-2^(p-1), 2^p-n)) \\\\ _Rémy Sigrist_, Mar 24 2018"
			],
			"xref": [
				"Cf. A053188, A060973, A081134, A002450, A081252, A081253, A081254."
			],
			"keyword": "easy,nonn",
			"offset": "1,6",
			"author": "_Henry Bottomley_, Mar 22 2000",
			"references": 15,
			"revision": 34,
			"time": "2021-03-28T09:54:17-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}