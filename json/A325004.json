{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A325004",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 325004,
			"data": "1,4,1,9,6,1,16,24,10,1,25,70,57,15,1,36,165,240,126,21,1,49,336,800,730,252,28,1,64,616,2226,3270,2008,462,36,1,81,1044,5390,11991,11880,5006,792,45,1,100,1665,11712,37450,56133,38970,11440,1287,55,1",
			"name": "Array read by descending antidiagonals: A(n,k) is the number of oriented colorings of the facets of a regular n-dimensional orthotope using up to k colors.",
			"comment": [
				"Also called hypercube, n-dimensional cube, and measure polytope. For n=1, the figure is a line segment with two vertices. For n=2 the figure is a square with four edges. For n=3 the figure is a cube with six square faces. For n=4, the figure is a tesseract with eight cubic facets. The Schläfli symbol, {4,3,...,3}, of the regular n-dimensional orthotope (n\u003e1) consists of a four followed by n-2 threes. Each of its 2n facets is an (n-1)-dimensional orthotope. Two oriented colorings are the same if one is a rotation of the other; chiral pairs are counted as two.",
				"Also the number of oriented colorings of the vertices of a regular n-dimensional orthoplex using up to k colors."
			],
			"link": [
				"Robert A. Russell, \u003ca href=\"/A325004/b325004.txt\"\u003eTable of n, a(n) for n = 1..325\u003c/a\u003e",
				"Robin Chapman, answer to \u003ca href=\"https://math.stackexchange.com/q/5732/\"\u003eColoring the faces of a hypercube\u003c/a\u003e, Math StackExchange, September 30, 2010."
			],
			"formula": [
				"A(n,k) = binomial(binomial(k+1,2) + n-1, n) + binomial(binomial(k,2),n).",
				"A(n,k) = Sum_{j=1..2n} A325008(n,j) * binomial(k,j).",
				"A(n,k) = A325005(n,k) + A325006(n,k) = 2*A325005(n,k) - A325007(n,k) = 2*A325006(n,k) + A325007(n,k).",
				"G.f. for row n: Sum{j=1..2n} A325008(n,j) * x^j / (1-x)^(j+1).",
				"Linear recurrence for row n: T(n,k) = Sum_{j=0..2n} binomial(-2-j,2n-j) * T(n,k-1-j).",
				"G.f. for column k: 1/(1-x)^binomial(k+1,2) + (1+x)^binomial(k,2) - 2."
			],
			"example": [
				"Array begins with A(1,1):",
				"1  4    9    16     25      36       49        64        81        100 ...",
				"1  6   24    70    165     336      616      1044      1665       2530 ...",
				"1 10   57   240    800    2226     5390     11712     23355      43450 ...",
				"1 15  126   730   3270   11991    37450    102726    253485     573265 ...",
				"1 21  252  2008  11880   56133   221725    756288   2283876    6228145 ...",
				"1 28  462  5006  38970  235235  1161832   4873128  17838492   58208920 ...",
				"1 36  792 11440 116400  894465  5495896  28162368 124122780  481650400 ...",
				"1 45 1287 24310 319815 3114540 23739310 148116618 782798490 3596651740 ...",
				"For A(1,2) = 4, the two achiral colorings use just one of the two colors for both vertices; the chiral pair uses two colors."
			],
			"mathematica": [
				"Table[Binomial[Binomial[d-n+2,2]+n-1,n]+Binomial[Binomial[d-n+1,2],n],{d,1,11},{n,1,d}] // Flatten"
			],
			"xref": [
				"Cf. A325005 (unoriented), A325006 (chiral), A325007 (achiral), A325008 (exactly k colors)",
				"Other n-dimensional polytopes: A324999 (simplex), A325012 (orthoplex)",
				"Rows 1-3 are A000290, A006528, A047780."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "1,2",
			"author": "_Robert A. Russell_, Mar 23 2019",
			"references": 11,
			"revision": 12,
			"time": "2019-05-25T11:44:14-04:00",
			"created": "2019-05-25T11:44:14-04:00"
		}
	]
}