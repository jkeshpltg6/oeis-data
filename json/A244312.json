{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A244312",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 244312,
			"data": "1,0,1,0,2,0,0,2,4,0,0,4,16,4,0,0,4,48,60,8,0,0,8,160,384,160,8,0,0,8,368,1952,2176,520,16,0,0,16,1152,9648,18688,9648,1152,16,0,0,16,2432,37008,132640,141680,45504,3568,32,0",
			"name": "Triangle read by rows: T(n,k) is the number of single loop solutions formed by n proper arches (connecting an odd and even vertice from 1 to 2n) above the x axis, k arches above the x axis connecting an odd vertice to a higher even vertice and a rainbow of n arches below the x axis.",
			"comment": [
				"Sum of row n = (n-1)!."
			],
			"link": [
				"Hsien-Kuei Hwang, Hua-Huai Chern, Guan-Huei Duh, \u003ca href=\"https://arxiv.org/abs/1807.01412\"\u003eAn asymptotic distribution theory for Eulerian recurrences with applications\u003c/a\u003e, arXiv:1807.01412 [math.CO], 2018."
			],
			"formula": [
				"T(n,k)= (k+ floor((-1)^(n-1)/2))*T(n-1,k) + (n-k- floor((-1)^(n-1)/2))*T(n-1,k-1), n=\u003e2, 1\u003c=k\u003c=n, T(1,1)=1, T(n,0)=0, T(n,n+1)=0."
			],
			"example": [
				"Triangle T(n,k) begins:",
				"n\\k  1     2     3     4     5     6     7     8",
				"1    1",
				"2    0     1",
				"3    0     2     0",
				"4    0     2     4     0",
				"5    0     4    16     4     0",
				"6    0     4    48    60     8     0",
				"7    0     8   160   384   160     8    0",
				"8    0     8   368  1952  2176   520   16    0",
				"T(4,3)=4 [top 14,23,56,78; bottom 18,27,36,45] [top 16,25,34,78; bottom 18,27,36,45] [top 12,34,58,67; bottom 18,27,36,45] [top 12,38,47,56; bottom 18,27,36,45]"
			],
			"mathematica": [
				"T[1,1]:= 1; T[n_,0]:= 0; T[n_, n_+1] := 0; T[n_,k_]:= If[k == n+1, 0, (k + Floor[(-1)^(n-1)/2])*T[n-1, k] + (n-k -Floor[(-1)^(n-1)/2]) T[n-1, k - 1]]; Table[T[n, k], {n, 1, 15}, {k, 1, n}]//Flatten (* _G. C. Greubel_, Oct 10 2018 *)"
			],
			"program": [
				"(PARI) T(n,k)=if(n==1 \u0026\u0026 k==1, 1, if(k==0, 0, if( k==n+1, 0, (k+ floor((-1)^(n-1)/2))*T(n-1,k) + (n-k- floor((-1)^(n-1)/2))*T(n-1,k-1))));",
				"for(n=1, 15, for(k=1,n, print1(T(n,k), \", \"))) \\\\ _G. C. Greubel_, Oct 10 2018"
			],
			"keyword": "nonn,tabl",
			"offset": "1,5",
			"author": "_Roger Ford_, Jul 02 2014",
			"references": 1,
			"revision": 26,
			"time": "2018-10-10T10:49:32-04:00",
			"created": "2014-07-03T16:22:44-04:00"
		}
	]
}