{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A042972",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 42972,
			"data": "4,8,1,0,4,7,7,3,8,0,9,6,5,3,5,1,6,5,5,4,7,3,0,3,5,6,6,6,7,0,3,8,3,3,1,2,6,3,9,0,1,7,0,8,7,4,6,6,4,5,3,4,9,4,0,0,2,0,8,1,5,4,8,9,2,4,2,5,5,1,9,0,4,8,9,1,5,8,2,1,3,6,7,4,8,7,0,4,7,6,6,5,8,3,8,8,3,3,5,4",
			"name": "Decimal expansion of i^(-i), where i = sqrt(-1).",
			"comment": [
				"Square root of Gelfond's constant (A039661). Since Gelfond's constant e^Pi is transcendental, e^(Pi/2) is transcendental. - _Daniel Forgues_, Apr 15 2011",
				"The complex sequence (...((((i)^i)^i)^i)^...) (n pairs of brackets) is periodic with period 4 and the first four entries are i, e^(-Pi/2), -i, e^(+Pi/2). See A049006 for e^(-Pi/2). - _Wolfdieter Lang_, Apr 27 2013",
				"A solution of x^i + x^(-i) = 0. In fact, x = Exp(Pi/2 + k*Pi), where k is any integer. - _Robert G. Wilson v_, Feb 04 2014"
			],
			"link": [
				"Nathaniel Johnston, \u003ca href=\"/A042972/b042972.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"H. S. Uhler, \u003ca href=\"http://www.jstor.org/stable/2972387\"\u003eOn the numerical value of i^i\u003c/a\u003e, Amer. Math. Monthly, 28 (1921), 114-116.",
				"\u003ca href=\"/index/Tra#transcendental\"\u003eIndex entries for transcendental numbers\u003c/a\u003e"
			],
			"formula": [
				"Equals i^(-i) = i^(1/i) = e^(Pi/2).",
				"Also (((i)^i)^i)^i. See a comment above on such powers. - _Wolfdieter Lang_, Apr 27 2013"
			],
			"example": [
				"4.81047738096535165547303566670383312639017087466453494002..."
			],
			"maple": [
				"evalf[110](I^(-I)); # _Muniru A Asiru_, Feb 17 2019"
			],
			"mathematica": [
				"RealDigits[Re[I^(1/I)], 10, 100][[1]] (* _Alonso del Arte_, Oct 31 2011 *)",
				"RealDigits[ Exp[Pi/2], 10, 111][[1]] (* _Robert G. Wilson v_, Apr 08 2014 *)"
			],
			"program": [
				"(PARI) default(realprecision, 110); exp(Pi/2) \\\\ _Nathaniel Johnston_, Apr 15 2011 (modified by _G. C. Greubel_, Feb 17 2019)",
				"(MAGMA) SetDefaultRealField(RealField(110)); R:= RealField(); Exp(Pi(R)/2); // _G. C. Greubel_, Feb 17 2019",
				"(Sage) numerical_approx(exp(pi/2), digits=110) # _G. C. Greubel_, Feb 17 2019"
			],
			"xref": [
				"Cf. A049006."
			],
			"keyword": "cons,nonn",
			"offset": "1,1",
			"author": "_Marvin Ray Burns_",
			"ext": [
				"a(100) corrected by _Nathaniel Johnston_, Apr 15 2011"
			],
			"references": 13,
			"revision": 50,
			"time": "2019-05-14T21:13:37-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}