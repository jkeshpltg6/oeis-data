{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A068695",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 68695,
			"data": "1,3,1,1,3,1,1,3,7,1,3,7,1,9,1,3,3,1,1,11,1,3,3,1,1,3,1,1,3,7,1,17,1,7,3,7,3,3,7,1,9,1,1,3,7,1,9,7,1,3,13,1,23,1,7,3,1,7,3,1,3,11,1,1,3,1,3,3,1,1,9,7,3,3,1,1,3,7,7,9,1,1,9,19,3,3,7,1,23,7,1,9,7,1,3,7,1,3,1,9,3,1",
			"name": "Smallest number (not beginning with 0) that yields a prime when placed on the right of n.",
			"comment": [
				"Max Alekseyev (see link) shows that a(n) always exists. Note that although his argument makes use of some potentially large constants (see the comments in A060199), the proof shows that a(n) exists for all n. - _N. J. A. Sloane_, Nov 13 2020",
				"Many numbers become prime by appending a one-digit odd number. Some numbers (such as 20, 32, 51, etc.) require a 2-digit odd number (A032352 has these). In the first 100000 values of n there are only 22 that require a 3-digit odd number (A091089). There probably are some values that require odd numbers of 4 or more digits, but these are likely to be very large. - Chuck Seggelin (barkeep(AT)plastereddragon.com), Dec 18 2003"
			],
			"link": [
				"David W. Wilson, \u003ca href=\"/A068695/b068695.txt\"\u003eTable of n, a(n) for n=1..10000\u003c/a\u003e",
				"Max Alekseyev, \u003ca href=\"/A068695/a068695_2.txt\"\u003eGiven n, there is a k such that the concatenation n||k is a prime\u003c/a\u003e, Nov 09 2020",
				"\u003ca href=\"/index/Pri#piden\"\u003eIndex entries for primes involving decimal expansion of n\u003c/a\u003e"
			],
			"example": [
				"a(20)=11 because 11 is the minimum odd number which when appended to 20 forms a prime (201, 203, 205, 207, 209 are all nonprime, 2011 is prime)."
			],
			"maple": [
				"T:=proc(t) local x,y; x:=t; y:=0; while x\u003e0 do x:=trunc(x/10); y:=y+1; od; end:",
				"P:=proc(q) local a,k,n; for n from 1 to q do a:=10*n+1; k:=1;",
				"while not isprime(a) do k:=k+1; a:=n*10^T(k)+k; od;",
				"print(k); od; end: P(10^5); # _Paolo P. Lava_, Apr 10 2014"
			],
			"mathematica": [
				"d[n_]:=IntegerDigits[n]; t={}; Do[k=1; While[!PrimeQ[FromDigits[Join[d[n],d[k]]]],k++]; AppendTo[t,k],{n,102}]; t (* _Jayanta Basu_, May 21 2013 *)",
				"mon[n_]:=Module[{k=1},While[!PrimeQ[n*10^IntegerLength[k]+k],k+=2];k]; Array[mon,110] (* _Harvey P. Dale_, Aug 13 2018 *)"
			],
			"program": [
				"(PARI) A068695=n-\u003efor(i=1,9e9,ispseudoprime(eval(Str(n,i)))\u0026\u0026return(i)) \\\\ _M. F. Hasler_, Oct 29 2013"
			],
			"xref": [
				"Cf. A032352 (a(n) requires at least a 2 digit odd number), A091089 (a(n) requires at least a 3 digit odd number).",
				"Cf. also A060199, A228325, A336893."
			],
			"keyword": "base,easy,nonn",
			"offset": "1,2",
			"author": "_Amarnath Murthy_, Mar 03 2002",
			"ext": [
				"More terms from Chuck Seggelin (barkeep(AT)plastereddragon.com), Dec 18 2003",
				"Entry revised by _N. J. A. Sloane_, Feb 20 2006",
				"More terms from _David Wasserman_, Feb 14 2006"
			],
			"references": 11,
			"revision": 46,
			"time": "2020-11-14T12:55:07-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}