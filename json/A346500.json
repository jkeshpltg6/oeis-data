{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A346500",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 346500,
			"data": "1,1,1,2,2,2,5,4,4,5,15,11,9,11,15,52,36,26,26,36,52,203,135,92,66,92,135,203,877,566,371,249,249,371,566,877,4140,2610,1663,1075,712,1075,1663,2610,4140,21147,13082,8155,5133,3274,3274,5133,8155,13082,21147",
			"name": "Number A(n,k) of partitions of the (n+k)-multiset {1,2,...,n,1,2,...,k}; square array A(n,k), n\u003e=0, k\u003e=0, read by antidiagonals.",
			"comment": [
				"Also number A(n,k) of factorizations of Product_{i=1..n} prime(i) * Product_{i=1..k} prime(i); A(2,2) = 9: 2*2*3*3, 3*3*4, 6*6, 2*3*6, 4*9, 2*2*9, 3*12, 2*18, 36."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A346500/b346500.txt\"\u003eAntidiagonals n = 0..140, flattened\u003c/a\u003e"
			],
			"formula": [
				"A(n,k) = A001055(A002110(n)*A002110(k)).",
				"A(n,k) = A(k,n).",
				"A(n,k) = A322765(abs(n-k),min(n,k))."
			],
			"example": [
				"A(2,2) = 9: 1122, 11|22, 12|12, 1|122, 112|2, 11|2|2, 1|1|22, 1|12|2, 1|1|2|2.",
				"Square array A(n,k) begins:",
				"    1,    1,    2,     5,    15,     52,     203,     877, ...",
				"    1,    2,    4,    11,    36,    135,     566,    2610, ...",
				"    2,    4,    9,    26,    92,    371,    1663,    8155, ...",
				"    5,   11,   26,    66,   249,   1075,    5133,   26683, ...",
				"   15,   36,   92,   249,   712,   3274,   16601,   91226, ...",
				"   52,  135,  371,  1075,  3274,  10457,   56135,  325269, ...",
				"  203,  566, 1663,  5133, 16601,  56135,  198091, 1207433, ...",
				"  877, 2610, 8155, 26683, 91226, 325269, 1207433, 4659138, ...",
				"  ..."
			],
			"maple": [
				"g:= proc(n, k) option remember; uses numtheory; `if`(n\u003ek, 0, 1)+",
				"     `if`(isprime(n), 0, add(`if`(d\u003ek or max(factorset(n/d))\u003ed, 0,",
				"        g(n/d, d)), d=divisors(n) minus {1, n}))",
				"    end:",
				"p:= proc(n) option remember; `if`(n=0, 1, p(n-1)*ithprime(n)) end:",
				"A:= (n, k)-\u003e g(p(n)*p(k)$2):",
				"seq(seq(A(n, d-n), n=0..d), d=0..10);",
				"# second Maple program:",
				"b:= proc(n) option remember; `if`(n=0, 1,",
				"      add(b(n-j)*binomial(n-1, j-1), j=1..n))",
				"    end:",
				"A:= proc(n, k) option remember; `if`(n\u003ck, A(k, n),",
				"     `if`(k=0, b(n), (A(n+1, k-1)+add(A(n-k+j, j)",
				"      *binomial(k-1, j), j=0..k-1)+A(n, k-1))/2))",
				"    end:",
				"seq(seq(A(n, d-n), n=0..d), d=0..10);"
			],
			"mathematica": [
				"b[n_] := b[n] = If[n == 0, 1, Sum[b[n-j] Binomial[n-1, j-1], {j, 1, n}]];",
				"A[n_, k_] := A[n, k] = If[n \u003c k, A[k, n],",
				"     If[k == 0, b[n], (A[n + 1, k - 1] + Sum[A[n - k + j, j]*",
				"     Binomial[k - 1, j], {j, 0, k - 1}] + A[n, k - 1])/2]];",
				"Table[Table[A[n, d - n], {n, 0, d}], {d, 0, 10}] // Flatten (* _Jean-François Alcover_, Aug 18 2021, after _Alois P. Heinz_'s second program *)"
			],
			"xref": [
				"Columns (or rows) k=0-10 give: A000110, A035098, A322764, A322768, A346881, A346882, A346883, A346884, A346885, A346886, A346887.",
				"Main diagonal gives A020555.",
				"First upper (or lower) diagonal gives A322766.",
				"Second upper (or lower) diagonal gives A322767.",
				"Antidiagonal sums give A346490.",
				"A(2n,n) gives A322769.",
				"Cf. A001055, A002110, A322765, A346426, A346517."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_Alois P. Heinz_, Jul 20 2021",
			"references": 15,
			"revision": 35,
			"time": "2021-08-18T15:28:28-04:00",
			"created": "2021-07-21T14:12:45-04:00"
		}
	]
}