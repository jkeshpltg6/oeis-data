{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A123202",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 123202,
			"data": "1,1,-2,2,-8,7,6,-36,63,-34,24,-192,504,-544,209,120,-1200,4200,-6800,5225,-1546,720,-8640,37800,-81600,94050,-55656,13327,5040,-70560,370440,-999600,1536150,-1363572,653023,-130922,40320,-645120,3951360,-12794880",
			"name": "Triangle of coefficients of n!*(1 - x)^n*L_n(x/(1 - x)), where L_n(x) is the Laguerre polynomial.",
			"comment": [
				"The n-th row consists of the coefficients in the expansion of Sum_{j=0..n} A021009(n,j)*x^j*(1 - x)^(n - j)."
			],
			"reference": [
				"Milton Abramowitz and Irene A. Stegun, eds., Handbook of Mathematical Functions with Formulas, Graphs and Mathematical Tables, 9th printing. New York: Dover, 1972, p. 782.",
				"Gengzhe Chang and Thomas W. Sederberg, Over and Over Again, The Mathematical Association of America, 1997, p. 164, figure 26.1."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A123202/b123202.txt\"\u003eTable of n, a(n) for n = 0..5150\u003c/a\u003e (Rows n=0..100 of triangle, flattened; offset corrected by _Georg Fischer_, Jan 31 2019)",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/LaguerrePolynomial.html\"\u003eLaguerre Polynomial\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = [x^k] (n!*L_n(x)*(1 - x)^n) with L_n(x) the Laguerre polynomial after substituting x by x/(1 - x). - _Peter Luschny_, Jan 05 2015",
				"From _Franck Maminirina Ramaharo_, Oct 13 2018: (Start)",
				"G.f.: exp(-x*y/(1 - (1 - x)*y))/(1 - (1 - x)*y).",
				"T(n,1) = A000142(n).",
				"T(n,2) = -A052582(n).",
				"T(n,n) = A002720(n). (End)"
			],
			"example": [
				"Triangle begins:",
				"       1;",
				"       1,    -2;",
				"       2,    -8,     7;",
				"       6,   -36,    63,    -34;",
				"      24,  -192,   504,   -544,   209;",
				"     120, -1200,  4200,  -6800,  5225,  -1546;",
				"     720, -8640, 37800, -81600, 94050, -55656, 13327;",
				"      ... reformatted. - _Franck Maminirina Ramaharo_, Oct 13 2018"
			],
			"maple": [
				"M := (n,x) -\u003e n!*subs(x=(x/(1-x)),orthopoly[L](n,x))*(1-x)^n:",
				"seq(print(seq(coeff(simplify(M(n,x)),x,k),k=0..n)),n=0..6); # _Peter Luschny_, Jan 05 2015"
			],
			"mathematica": [
				"w = Table[n!*CoefficientList[LaguerreL[n, x], x], {n, 0, 10}];",
				"v = Table[CoefficientList[Sum[w[[n + 1]][[m + 1]]*x^ m*(1 - x)^(n - m), {m, 0, n}], x], {n, 0, 10}]; Flatten[v]"
			],
			"program": [
				"(Maxima) create_list(ratcoef(n!*(1 - x)^n*laguerre(n, x/(1 - x)), x, k), n, 0, 10, k, 0, n); /* _Franck Maminirina Ramaharo_, Oct 13 2018 */",
				"(PARI) row(n) = Vecrev(n!*(1-x)^n*pollaguerre(n, 0, x/(1 - x))); \\\\ _Michel Marcus_, Feb 06 2021"
			],
			"xref": [
				"Cf. A122753, A123018, A123019, A123021, A123027, A123199, A123202, A123217, A123221."
			],
			"keyword": "sign,tabl",
			"offset": "0,3",
			"author": "_Roger L. Bagula_, Oct 04 2006",
			"ext": [
				"Edited by _N. J. A. Sloane_, Jun 12 2007",
				"Edited, new name, and offset corrected by _Franck Maminirina Ramaharo_, Oct 13 2018"
			],
			"references": 13,
			"revision": 42,
			"time": "2021-02-06T08:07:52-05:00",
			"created": "2006-10-09T03:00:00-04:00"
		}
	]
}