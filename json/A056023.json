{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A056023",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 56023,
			"data": "1,2,3,6,5,4,7,8,9,10,15,14,13,12,11,16,17,18,19,20,21,28,27,26,25,24,23,22,29,30,31,32,33,34,35,36,45,44,43,42,41,40,39,38,37,46,47,48,49,50,51,52,53,54,55,66,65,64,63,62,61,60,59,58,57,56",
			"name": "Unique triangle such that (1) every positive integer occurs exactly once; (2) row n consists of n consecutive numbers; (3) odd-numbered rows are decreasing; (4) even-numbered rows are increasing; and (5) column 1 is increasing.",
			"comment": [
				"Self-inverse permutation of the natural numbers.",
				"T(2*n-1,1) = A000217(2*n-1) = T(2*n,1) - 1; T(2*n,4*n) = A000217(2*n) = T(2*n+1,4*n+1) - 1. - _Reinhard Zumkeller_, Apr 25 2004",
				"Mirror image of triangle in A056011. - _Philippe Deléham_, Apr 04 2009",
				"From _Clark Kimberling_, Feb 03 2011: (Start)",
				"When formatted as a rectangle R, for m \u003e 1, the numbers n-1 and n+1 are neighbors (row, column, or diagonal) of R.",
				"R(n,k) = n + (k+n-2)(k+n-1)/2 if n+k is odd;",
				"R(n,k) = k + (n+k-2)(n+k-1)/2 if n+k is even.",
				"Northwest corner:",
				"   1,  2,  6,  7, 15, 16, 28",
				"   3,  5,  8, 14, 17, 27, 30",
				"   4,  9, 13, 18, 26, 31, 43",
				"  10, 12, 19, 25, 32, 42, 49",
				"  11, 20, 24, 33, 41, 50, 62",
				"  (End)",
				"a(n) is a pairing function: a function that reversibly maps Z^{+} x Z^{+} onto Z^{+}, where Z^{+} is the set of integer positive numbers. - _Boris Putievskiy_, Dec 24 2012",
				"For generalizations see A218890, A213927. - _Boris Putievskiy_, Mar 10 2013"
			],
			"link": [
				"Ivan Neretin, \u003ca href=\"/A056023/b056023.txt\"\u003eTable of n, a(n) for n = 1..5050\u003c/a\u003e",
				"Boris Putievskiy, \u003ca href=\"http://arxiv.org/abs/1212.2732\"\u003eTransformations Integer Sequences And Pairing Functions\u003c/a\u003e arXiv:1212.2732 [math.CO], 2012.",
				"Eric Weisstein's MathWorld, \u003ca href=\"http://mathworld.wolfram.com/PairingFunction.html\"\u003ePairing functions\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = (n^2 - (n - 2*k)*(-1)^(n mod 2))/2 + n mod 2. - _Reinhard Zumkeller_, Apr 25 2004",
				"a(n) = ((i + j - 1)*(i + j - 2) + ((-1)^t + 1)*j - ((-1)^t - 1)*i)/2, where i=n-t*(t+1)/2, j=(t*t+3*t+4)/2-n and t=floor((-1+sqrt(8*n-7))/2). - _Boris Putievskiy_, Dec 24 2012"
			],
			"example": [
				"From _Philippe Deléham_, Apr 04 2009 (Start)",
				"Triangle begins:",
				"  1;",
				"  2,   3;",
				"  6,   5,  4;",
				"  7,   8,  9, 10;",
				"  15, 14, 13, 12, 11;",
				"  ...",
				"(End)",
				"Enumeration by boustrophedonic (\"ox-plowing\") diagonal method. - _Boris Putievskiy_, Dec 24 2012"
			],
			"mathematica": [
				"(* As a rectangle: *)",
				"r[n_, k_] := n + (k + n - 2) (k + n - 1)/2/; OddQ[n + k];",
				"r[n_, k_] := k + (n + k - 2) (n + k - 1)/2/; EvenQ[n+k];",
				"TableForm[Table[f[n, k], {n, 1, 10}, {k, 1, 15}]]",
				"Table[r[n - k + 1, k], {n, 14}, {k, n, 1, -1}]//Flatten",
				"(* _Clark Kimberling_, Feb 03 2011 *)"
			],
			"xref": [
				"Cf. A056011, A218890, A213927."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Aug 01 2000",
			"references": 9,
			"revision": 52,
			"time": "2020-01-22T04:26:55-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}