{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A308821",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 308821,
			"data": "14,95,527,851,1247,3551,4307,8051,14351,26969,30227,37769,64769,87953,152051,163769,199553,202451,256793,275369,341969,455369,1070969,1095953,1159673,1232051,1625369,1702769,2005007,2081993",
			"name": "Semiprimes where the sum of the digits equals the difference between the prime factors.",
			"comment": [
				"14 is the only even number in the sequence, since 2 is the only even prime and p-2 grows much faster than the digit sum of 2p."
			],
			"link": [
				"James Beyer, \u003ca href=\"/A308821/b308821.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"James Beyer, \u003ca href=\"https://jebeyer.github.io/nlfourteen.html\"\u003eNumbers Like Fourteen\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Digit_sum\"\u003eDigit sum\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Semiprime\"\u003eSemiprime\u003c/a\u003e"
			],
			"example": [
				"14=2*7 and 1+4=7-2.",
				"95=5*19 and 9+5=19-5.",
				"527=17*31 and 5+2+7=31-17."
			],
			"mathematica": [
				"Take[Sort@ Reap[ Do[ If[PrimeQ[q + g] \u0026\u0026 g == Total@ IntegerDigits[n = q (q + g)], Sow@n], {g, 9*9}, {q, Prime@ Range@ 2000}]][[2, 1]], 100] (* _Giovanni Resta_, Jul 25 2019 *)",
				"spdpfQ[n_]:=Module[{f=FactorInteger[n][[All,1]]},PrimeOmega[n]== 2 \u0026\u0026 Total[ IntegerDigits[n]]==f[[2]]-f[[1]]]; Select[Range[ 21*10^5],spdpfQ]// Quiet (* or *) Times@@@Select[Subsets[Prime[ Range[ 300]],{2}],#[[2]]-#[[1]]==Total[IntegerDigits[#[[1]]#[[2]]]]\u0026] (* _Harvey P. Dale_, Oct 14 2021 *)"
			],
			"program": [
				"(PARI) isok(n) = (bigomega(n) == 2) \u0026\u0026 (f=factor(n)) \u0026\u0026 (#f~ == 2) \u0026\u0026 (sumdigits(n) == f[2,1] - f[1,1]); \\\\ _Michel Marcus_, Jun 29 2019",
				"(MAGMA) [n:n in [2..2100000]|IsSquarefree(n) and #PrimeDivisors(n) eq 2 and PrimeDivisors(n)[2]-PrimeDivisors(n)[1] eq \u0026+Intseq(n)]; // _Marius A. Burtea_, Jul 27 2019"
			],
			"xref": [
				"Cf. A001358, A006753, A006881."
			],
			"keyword": "nonn,base",
			"offset": "1,1",
			"author": "_James Beyer_, Jun 26 2019",
			"references": 1,
			"revision": 24,
			"time": "2021-10-14T19:35:50-04:00",
			"created": "2019-08-03T05:19:25-04:00"
		}
	]
}