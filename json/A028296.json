{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A028296",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 28296,
			"data": "1,-1,5,-61,1385,-50521,2702765,-199360981,19391512145,-2404879675441,370371188237525,-69348874393137901,15514534163557086905,-4087072509293123892361,1252259641403629865468285,-441543893249023104553682821,177519391579539289436664789665",
			"name": "Expansion of e.g.f. Gudermannian(x) = 2*arctan(exp(x)) - Pi/2.",
			"comment": [
				"The Euler numbers A000364 with alternating signs.",
				"The first column of the inverse to the matrix with entries C(2*i,2*j), i,j \u003e=0. The full matrix is lower triangular with the i-th subdiagonal having entries a(i)*C(2*j,2*i) j\u003e=i. - Nolan Wallach (nwallach(AT)ucsd.edu), Dec 26 2005",
				"This sequence is also EulerE(2*n). - Paul Abbott (paul(AT)physics.uwa.edu.au), Apr 14 2006",
				"Consider the sequence defined by a(0)=1; thereafter a(n) = c*Sum_{k=1..n} binomial(2n,2k)*a(n-k). For c = -3, -2, -1, 1, 2, 3, 4 this is A210676, A210657, A028296, A094088, A210672, A210674, A249939.",
				"To avoid possible confusion: these are the odd e.g.f. coefficients of Gudermannian(x) with the offset shifted by -1 (even coefficients are zero). They are identical to the even e.g.f. coefficients for 1/cosh(x) = -Gudermannian'(x) (see the Example). Since the complex root of cosh(z) with the smallest absolute value is z0 = i*Pi/2, the radius of convergence for the Taylor series of all these functions is Pi/2 = A019669. - _Stanislav Sykora_, Oct 07 2016"
			],
			"reference": [
				"Gradshteyn and Ryzhik, Tables, 5th ed., Section 1.490, pp. 51-52."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A028296/b028296.txt\"\u003eTable of n, a(n) for n = 0..100\u003c/a\u003e",
				"Beáta Bényi and Toshiki Matsusaka, \u003ca href=\"https://arxiv.org/abs/2106.05585\"\u003eExtensions of the combinatorics of poly-Bernoulli numbers\u003c/a\u003e, arXiv:2106.05585 [math.CO], 2021.",
				"F. Callegaro and G. Gaiffi, \u003ca href=\"http://arxiv.org/abs/1406.1304\"\u003eOn models of the braid arrangement and their hidden symmetries\u003c/a\u003e, arXiv preprint arXiv:1406.1304 [math.AT], 2014.",
				"K. Dilcher and C. Vignat, \u003ca href=\"http://www.jstor.org/stable/10.4169/amer.math.monthly.123.5.486\"\u003eEuler and the Strong Law of Small Numbers\u003c/a\u003e, Amer. Math. Mnthly, 123 (May 2016), 486-490.",
				"A. L. Edmonds and S. Klee, \u003ca href=\"http://arxiv.org/abs/1210.7396\"\u003eThe combinatorics of hyperbolized manifolds\u003c/a\u003e, arXiv preprint arXiv:1210.7396 [math.CO], 2012. - From _N. J. A. Sloane_, Jan 02 2013",
				"Guodong Liu, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2008.04.003\"\u003eOn congruences of Euler numbers modulo powers of two\u003c/a\u003e, Journal of Number Theory, Volume 128, Issue 12, December 2008, Pages 3063-3071.",
				"N. E. Nørlund, \u003ca href=\"http://www-gdz.sub.uni-goettingen.de/cgi-bin/digbib.cgi?PPN373206070\"\u003eVorlesungen über Differenzenrechnung\u003c/a\u003e, Springer 1924, p. 25.",
				"Zhi-Hong Sun, \u003ca href=\"http://arxiv.org/abs/1203.5977\"\u003eOn the further properties of {U_n}\u003c/a\u003e, arXiv:1203.5977v1 [math.NT], Mar 27 2012."
			],
			"formula": [
				"E.g.f.: sech(x) = 1/cosh(x) (even terms), or Gudermannian(x) (odd terms).",
				"Recurrence: a(n) = -Sum_{i=0..n-1} a(i)*binomial(2*n, 2*i). - _Ralf Stephan_, Feb 24 2005",
				"a(n) = Sum_{k=1,3,5,..,2n+1} ((-1)^((k-1)/2) /(2^k*k)) * Sum_{i=0..k} (-1)^i*(k-2*i)^(2n+1) * binomial(k,i). - _Vladimir Kruchinin_, Apr 20 2011",
				"a(n) = 2^(4*n+1)*(zeta(-2*n,1/4) - zeta(-2*n,3/4)). - _Gerry Martens_, May 27 2011",
				"From _Sergei N. Gladkovskii_, Dec 15 2011 - Oct 09 2013: (Start)",
				"Continued fractions:",
				"G.f.: A(x) = 1 - x/(S(0)+x), S(k) = euler(2*k) + x*euler(2*k+2) - x*euler(2*k)* euler(2*k+4)/S(k+1).",
				"E.g.f.: E(x) = 1 - x/(S(0)+x); S(k) = (k+1)*euler(2*k) + x*euler(2*k+2) - x*(k+1)* euler(2*k)*euler(2*k+4)/S(k+1).",
				"2*arctan(exp(z)) - Pi/2 = z*(1 - z^2/(G(0) + z^2)), G(k) = 2*(k+1)*(2*k+3)*euler(2*k) + z^2*euler(2*k+2) - 2*z^2*(k+1)*(2*k+3)*euler(2*k)*euler(2*k+4)/G(k+1).",
				"G.f.: A(x) = 1/S(0) where S(k) = 1 + x*(k+1)^2/S(k+1).",
				"G.f.: 1/Q(0) where Q(k) = 1 - x*k*(3*k-1) - x*(k+1)*(2*k+1)*(x*k^2-1)/Q(k+1).",
				"E.g.f.:(2 - x^4/( (x^2+2)*Q(0) + 2))/(2+x^2) where Q(k) = 4*k + 4 + 1/( 1 - x^2/( 2 + x^2 + (2*k+3)*(2*k+4)/Q(k+1))).",
				"E.g.f.: 1/cosh(x) = 8*(1-x^2)/(8 - 4*x^2 - x^4*U(0)) where U(k) = 1 + 4*(k+1)*(k+2)/(2*k+3 - x^2*(2*k+3)/(x^2 + 8*(k+1)*(k+2)*(k+3)/U(k+1)));",
				"G.f.: 1/U(0) where U(k) = 1 - x + x*(2*k+1)*(2*k+2)/(1 + x*(2*k+1)*(2*k+2)/U(k+1));",
				"G.f.: 1 - x/G(0) where G(k) = 1 - x + x*(2*k+2)*(2*k+3)/(1 + x*(2*k+2)*(2*k+3)/G(k+1));",
				"G.f.: 1/Q(0), where Q(k) = 1 - sqrt(x) + sqrt(x)*(k+1)/(1-sqrt(x)*(k+1)/Q(k+1));",
				"G.f.: (1/Q(0) + 1)/(1-sqrt(x)), where Q(k) = 1 - 1/sqrt(x) + (k+1)*(k+2)/Q(k+1);",
				"G.f.: Q(0), where Q(k) = 1 - x*(k+1)^2/( x*(k+1)^2 + 1/Q(k+1)).",
				"(END)",
				"n) ~ (-1)^n * (2*n)! * 2^(2*n+2) / Pi^(2*n+1). - _Vaclav Kotesovec_, Aug 04 2014",
				"a(n) = 2*Im(Li_{-2n}(i)), where Li_n(x) is polylogarithm, i=sqrt(-1). - _Vladimir Reshetnikov_, Oct 22 2015"
			],
			"example": [
				"Gudermannian(x) = x - (1/6)*x^3 + (1/24)*x^5 - (61/5040)*x^7 + (277/72576)*x^9 + ....",
				"Gudermannian'(x) = 1/cosh(x) = (1/1!)*x^0 - (1/2!)*x^2 + (5/4!)*x^4 - (61/6!)*x^6 + (1385/8!)*x^8 + .... - _Stanislav Sykora_, Oct 07 2016"
			],
			"maple": [
				"A028296 := proc(n) a :=0 ; for k from 1 to 2*n+1 by 2 do a := a+(-1)^((k-1)/2)/2^k/k *add( (-1)^i *(k-2*i)^(2*n+1) *binomial(k,i), i=0..k) ; end do: a ; end proc:",
				"seq(A028296(n),n=0..10) ; # _R. J. Mathar_, Apr 20 2011"
			],
			"mathematica": [
				"Table[EulerE[2*n], {n, 0, 30}] (* Paul Abbott, Apr 14 2006 *)",
				"Table[(CoefficientList[Series[1/Cosh[x],{x,0,40}],x]*Range[0,40]!)[[2*n+1]],{n,0,20}] (* _Vaclav Kotesovec_, Aug 04 2014*)",
				"With[{nn=40},Take[CoefficientList[Series[Gudermannian[x],{x,0,nn}],x] Range[ 0,nn-1]!,{2,-1,2}]] (* _Harvey P. Dale_, Feb 24 2018 *)",
				"{1, Table[2*(-I)*PolyLog[-2*n, I], {n, 1, 12}]} // Flatten (* _Peter Luschny_, Aug 12 2021 *)"
			],
			"program": [
				"(Maxima)",
				"a(n):=sum((-1+(-1)^(k))*(-1)^((k+1)/2)/(2^(k+1)*k)*sum((-1)^i*(k-2*i)^n*binomial(k,i),i,0,k),k,1,n); /* with interpolated zeros, _Vladimir Kruchinin_, Apr 20 2011 */",
				"(Sage)",
				"def A028296_list(len):",
				"    f = lambda k: x*(k+1)^2",
				"    g = 1",
				"    for k in range(len-2,-1,-1):",
				"        g = (1-f(k)/(f(k)+1/g)).simplify_rational()",
				"    return taylor(g, x, 0, len-1).list()",
				"print(A028296_list(17))",
				"(Sage)",
				"def A028296(n):",
				"    shapes = ([x*2 for x in p] for p in Partitions(n))",
				"    return sum((-1)^len(s)*factorial(len(s))*SetPartitions(sum(s), s).cardinality() for s in shapes)",
				"print([A028296(n) for n in (0..16)]) # _Peter Luschny_, Aug 10 2015",
				"(PARI) a(n) = 2*imag(polylog(-2*n, I)); \\\\ _Michel Marcus_, May 30 2018"
			],
			"xref": [
				"Absolute values are the Euler numbers A000364.",
				"Cf. A019669."
			],
			"keyword": "sign,easy,nice",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"references": 34,
			"revision": 145,
			"time": "2021-09-25T06:46:48-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}