{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005574",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5574,
			"id": "M1010",
			"data": "1,2,4,6,10,14,16,20,24,26,36,40,54,56,66,74,84,90,94,110,116,120,124,126,130,134,146,150,156,160,170,176,180,184,204,206,210,224,230,236,240,250,256,260,264,270,280,284,300,306,314,326,340,350,384,386,396",
			"name": "Numbers k such that k^2 + 1 is prime.",
			"comment": [
				"Hardy and Littlewood conjectured that the asymptotic number of elements in this sequence not exceeding n is approximately c*sqrt(n)/log(n) for some constant c. - _Stefan Steinerberger_, Apr 06 2006",
				"Also, nonnegative integers such that a(n)+i is a Gaussian prime. - _Maciej Ireneusz Wilczynski_, May 30 2011",
				"Apparently Goldbach conjectured that any a\u003e1 from this sequence can be written as a=b+c where b and c are in this sequence (Lemmermeyer link below). - _Jeppe Stig Nielsen_, Oct 14 2015",
				"No term \u003e 2 can be both in this sequence and in A001105 because of the Aurifeuillean factorization (2*k^2)^2 + 1 = (2*k^2 - 2*k + 1) * (2*k^2 + 2*k + 1). - _Jeppe Stig Nielsen_, Aug 04 2019"
			],
			"reference": [
				"Dubner, Harvey. \"Generalized Fermat primes.\" J. Recreational Math., 18 (1985): 279-280.",
				"R. K. Guy, \"Unsolved Problems in Number Theory\", 3rd edition, A2",
				"G. H. Hardy and E. M. Wright, An Introduction to the Theory of Numbers, 5th ed., Oxford Univ. Press, 1979, p. 15, Thm. 17.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A005574/b005574.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"H. Dubner, \u003ca href=\"/A006093/a006093_1.pdf\"\u003eGeneralized Fermat primes\u003c/a\u003e, J. Recreational Math. 18.4 (1985-1986), 279. (Annotated scanned copy)",
				"F. Ellermann, \u003ca href=\"/A002496/a002496.txt\"\u003ePrimes of the form (m^2)+1 up to 10^6\u003c/a\u003e.",
				"L. Euler, \u003ca href=\"http://eulerarchive.maa.org/pages/E283.html\"\u003eDe numeris primis valde magnis\u003c/a\u003e, Novi Commentarii academiae scientiarum Petropolitanae 9 (1764), pp. 99-153. See pp. 123-125.",
				"R. K. Guy, \u003ca href=\"/A000081/a000081.pdf\"\u003eLetter to N. J. A. Sloane, 1988-04-12\u003c/a\u003e (annotated scanned copy).",
				"F. Lemmermeyer, \u003ca href=\"http://mathoverflow.net/questions/14690/\"\u003ePrimes of the form a^2+1\u003c/a\u003e, Math Overflow question (2010).",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/LandausProblems.html\"\u003eLandau's Problems\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Power.html\"\u003ePower\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Near-SquarePrime.html\"\u003eNear-Square Prime\u003c/a\u003e.",
				"Marek Wolf, \u003ca href=\"http://arXiv.org/abs/0803.1456\"\u003eSearch for primes of the form m^2+1\u003c/a\u003e, arXiv:0803.1456 [math.NT], 2008-2010."
			],
			"formula": [
				"a(n) = A090693(n) - 1.",
				"a(n) = 2*A001912(n-1) for n\u003e1. - _Jeppe Stig Nielsen_, Aug 04 2019"
			],
			"mathematica": [
				"Select[Range[350], PrimeQ[ #^2 + 1] \u0026] (* _Stefan Steinerberger_, Apr 06 2006 *)",
				"Join[{1},2Flatten[Position[PrimeQ[Table[x^2+1,{x,2,1000,2}]],True]]]  (* _Fred Patrick Doty_, Aug 18 2017 *)"
			],
			"program": [
				"(PARI) isA005574(n) = isprime(n^2+1) \\\\ _Michael B. Porter_, Mar 20 2010",
				"(PARI) for(n=1, 1e3, if(isprime(n^2 + 1), print1(n, \", \"))) \\\\ _Altug Alkan_, Oct 14 2015",
				"(MAGMA) [n: n in [0..400] | IsPrime(n^2+1)]; // _Vincenzo Librandi_, Nov 18 2010",
				"(Haskell)",
				"a005574 n = a005574_list !! (n-1)",
				"a005574_list = filter ((== 1) . a010051' . (+ 1) . (^ 2)) [0..]",
				"-- _Reinhard Zumkeller_, Jul 03 2015"
			],
			"xref": [
				"Cf. A002522, A001912, A002496, A062325, A090693, A000068, A006314, A006313, A006315, A006316, A056994, A056995, A057465, A057002, A088361, A088362, A226528, A226529, A226530, A251597, A253854, A244150, A243959, A321323.",
				"Other sequences of the type \"Numbers k such that k^2 + i is prime\": this sequence (i=1), A067201 (i=2), A049422 (i=3), A007591 (i=4), A078402 (i=5), A114269 (i=6), A114270 (i=7), A114271 (i=8), A114272 (i=9), A114273 (i=10), A114274 (i=11), A114275 (i=12).",
				"Cf. A010051, A259645."
			],
			"keyword": "nonn,easy,nice",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"references": 165,
			"revision": 119,
			"time": "2021-11-09T06:03:56-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}