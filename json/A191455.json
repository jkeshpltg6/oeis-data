{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A191455",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 191455,
			"data": "1,2,3,5,8,4,13,21,10,6,35,57,27,16,7,95,154,73,43,19,9,258,418,198,116,51,24,11,701,1136,538,315,138,65,29,12,1905,3087,1462,856,375,176,78,32,14,5178,8391,3974,2326,1019,478,212,86,38,15,14075,22809",
			"name": "Dispersion of (floor(n*e)), by antidiagonals.",
			"comment": [
				"Background discussion:  Suppose that s is an increasing sequence of positive integers, that the complement t of s is infinite, and that t(1)=1.  The dispersion of s is the array D whose n-th row is (t(n), s(t(n)), s(s(t(n)), s(s(s(t(n)))), ...).  Every positive integer occurs exactly once in D, so that, as a sequence, D is a permutation of the positive integers.  The sequence u given by u(n)=(number of the row of D that contains n) is a fractal sequence.  Examples:",
				"(1) s=A000040 (the primes), D=A114537, u=A114538.",
				"(2) s=A022343 (without initial 0), D=A035513 (Wythoff array), u=A003603.",
				"(3) s=A007067, D=A035506 (Stolarsky array), u=A133299.",
				"More recent examples of dispersions: A191426-A191455."
			],
			"example": [
				"Northwest corner:",
				"1...2....5....13...35",
				"3...8....21...57...154",
				"4...10...27...73...198",
				"6...16...43...116..315",
				"7...19...51...138..375"
			],
			"maple": [
				"A191455 := proc(r, c)",
				"    option remember;",
				"    if c = 1 then",
				"        A054385(r) ;",
				"    else",
				"        A022843(procname(r, c-1)) ;",
				"    end if;",
				"end proc: # _R. J. Mathar_, Jan 25 2015"
			],
			"mathematica": [
				"(* Program generates the dispersion array T of increasing sequence f[n] *)",
				"r=40; r1=12; c=40; c1=12;",
				"f[n_] :=Floor[n*E]   (* complement of column 1 *)",
				"mex[list_] := NestWhile[#1 + 1 \u0026, 1, Union[list][[#1]] \u003c= #1 \u0026, 1, Length[Union[list]]]",
				"rows = {NestList[f, 1, c]};",
				"Do[rows = Append[rows, NestList[f, mex[Flatten[rows]], r]], {r}];",
				"t[i_, j_] := rows[[i, j]];",
				"TableForm[Table[t[i, j], {i, 1, 10}, {j, 1, 10}]]",
				"(* A191455 array *)",
				"Flatten[Table[t[k, n - k + 1], {n, 1, c1}, {k, 1, n}]] (* A191455 sequence *)",
				"(* Program by _Peter J. C. Moses_, Jun 01 2011 *)"
			],
			"xref": [
				"Cf. A114537, A035513, A035506."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Jun 05 2011",
			"references": 41,
			"revision": 11,
			"time": "2015-01-25T13:09:28-05:00",
			"created": "2011-06-06T12:53:26-04:00"
		}
	]
}