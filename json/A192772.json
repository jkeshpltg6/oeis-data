{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A192772",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 192772,
			"data": "1,0,1,1,2,7,12,41,86,247,585,1548,3849,9896,25001,63724,161721,411257,1044878,2655719,6748972,17151849,43589578,110777391,281529169,715471992,1818293377,4620978640,11743694657,29845241080,75848270001",
			"name": "Constant term in the reduction of the n-th Fibonacci polynomial by x^3-\u003ex^2+2x+1.",
			"comment": [
				"For discussions of polynomial reduction, see A192232 and A192744."
			],
			"link": [
				"\u003ca href=\"/index/Rec\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,5,-1,-5,1,1)."
			],
			"formula": [
				"a(n)=a(n-1)+5*a(n-2)-a(n-3)-5*a(n-4)+a(n-5)+a(n-6).",
				"G.f.: -x*(x^2-x-1)*(x^2+2*x-1) / (x^6+x^5-5*x^4-x^3+5*x^2+x-1). [_Colin Barker_, Jan 17 2013]"
			],
			"example": [
				"The first five polynomials p(n,x) and their reductions are as follows:",
				"F1(x)=1 -\u003e 1",
				"F2(x)=x -\u003e x",
				"F3(x)=x^2+1 -\u003e x^2+1",
				"F4(x)=x^3+2x -\u003e x^2+4x+1",
				"F5(x)=x^4+3x^2+1 -\u003e 6x^2+3x+2, so that",
				"A192772=(1,0,1,1,2,...), A192773=(0,1,0,4,3,...), A192774=(0,0,1,1,6,...)"
			],
			"mathematica": [
				"q = x^3; s = x^2 + 2 x + 1; z = 40;",
				"p[n_, x_] := Fibonacci[n, x];",
				"Table[Expand[p[n, x]], {n, 1, 7}]",
				"reduce[{p1_, q_, s_, x_}] :=",
				"FixedPoint[(s PolynomialQuotient @@ #1 +",
				"       PolynomialRemainder @@ #1 \u0026)[{#1, q, x}] \u0026, p1]",
				"t = Table[reduce[{p[n, x], q, s, x}], {n, 1, z}];",
				"u1 = Table[Coefficient[Part[t, n], x, 0], {n, 1, z}]",
				"  (* A192772 *)",
				"u2 = Table[Coefficient[Part[t, n], x, 1], {n, 1, z}]",
				"  (* A192773 *)",
				"u3 = Table[Coefficient[Part[t, n], x, 2], {n, 1, z}]",
				"  (* A192774 *)"
			],
			"xref": [
				"Cf. A192744, A192232, A192616, A192773, A192774."
			],
			"keyword": "nonn,easy",
			"offset": "1,5",
			"author": "_Clark Kimberling_, Jul 09 2011",
			"references": 6,
			"revision": 10,
			"time": "2015-06-13T00:53:53-04:00",
			"created": "2011-07-10T00:59:56-04:00"
		}
	]
}