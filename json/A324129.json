{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A324129",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 324129,
			"data": "1,1,3,6,13,25,49,91,169,306,551,979,1729,3029,5279,9150,15793,27149,46513,79439,135301,229866,389643,659111,1112833,1875625,3156219,5303286,8898709,14912641,24961201,41734339,69705889,116311074,193898159,322961275",
			"name": "a(n) = n*Fibonacci(n) + ((-1)^n + 1)/2.",
			"comment": [
				"Equals A324128(n)/2.",
				"This sequence is distantly related to (one-half) the number of losing strings using a binary alphabet in the \"same game\" described by Burns and Purcell (2007) and Kurz (2001). - _Petros Hadjicostas_, Sep 01 2019"
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A324129/b324129.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Chris Burns and Benjamin Purcell, \u003ca href=\"https://www.fq.math.ca/Papers1/45-3/burns.pdf\"\u003eCounting the number of winning strings in the 1-dimensional same game\u003c/a\u003e, Fibonacci Quarterly, 45(3) (2007), 233-238.",
				"Sascha Kurz, \u003ca href=\"/A035617/a035617.pdf\"\u003ePolynomials in \"same game\"\u003c/a\u003e, 2001.",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,2,-4,-2,2,1)."
			],
			"formula": [
				"From _Chai Wah Wu_, Feb 20 2019: (Start)",
				"a(n) = 2*a(n-1) + 2*a(n-2) - 4*a(n-3) - 2*a(n-4) + 2*a(n-5) + a(n-6) for n \u003e 5.",
				"G.f.: (x^5 - x^4 - 2*x^3 + x^2 + x - 1)/((x - 1)*(x + 1)*(x^2 + x - 1)^2). (End)",
				"a(n) = A309874(n)/2 + A099920(n-1) = 2^(n-1) - A035615(n)/2 + A099920(n-1) = A323812(n) + A099920(n-1) for n \u003e= 2. [Sequence A309874 counts the losing strings while A035615 counts the winning strings using a binary alphabet in the \"same game\". See Burns and Purcell (2007) and Kurz (2001).] - _Petros Hadjicostas_, Sep 01 2019"
			],
			"program": [
				"(PARI) Vec((1 - x - x^2 + 2*x^3 + x^4 - x^5) / ((1 - x)*(1 + x)*(1 - x - x^2)^2) + O(x^40)) \\\\ _Colin Barker_, Mar 03 2019",
				"(MAGMA) [n*Fibonacci(n)+((-1)^n+1)/2:n in [0..35]]; // _Marius A. Burtea_, Aug 29 2019"
			],
			"xref": [
				"Cf. A000045, A035615, A099920, A323812, A324128."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_, Feb 20 2019",
			"references": 2,
			"revision": 36,
			"time": "2019-09-02T08:07:47-04:00",
			"created": "2019-02-20T16:07:18-05:00"
		}
	]
}