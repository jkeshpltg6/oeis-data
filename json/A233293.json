{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A233293",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 233293,
			"data": "3,1,0,40,0,0,16,0,88,592,0,628,52,160,304,1672,808,2248,3616,11176,10096,8728,4192,23056,13912,65428,40804,5812,9448,12148,8584,82132,27700,10528,91672,53188,58804,20896,96064,2752,32776,25972,14560,183688,8080",
			"name": "Smallest number that is the largest value in the Collatz (3x + 1) trajectories of exactly n initial values. (a(n)=0 if no such number exists.)",
			"comment": [
				"Smallest number that appears exactly n times in A025586.",
				"Numbers that are not the largest value in the 3x + 1 trajectory of any initial value (that is, numbers that do not appear at all in A025586) are in A213199; the smallest such number is a(0) = 3.",
				"Numbers that are the largest value in the 3x + 1 trajectory of exactly one initial value (that is, numbers that appear exactly once in A025586) are in A222562; the smallest such number is a(1) = 1.",
				"Numbers that are the largest value in the 3x + 1 trajectories of exactly three initial values (that is, numbers that appear exactly three times in A025586) are in A232870; the smallest such number is a(3) = 40.",
				"No number that is the largest value in the 3x + 1 trajectories of exactly 2, 4, 5, 7, or 10 initial values exists, so a(n) = 0 at n = 2, 4, 5, 7, and 10; for all other values of n up to 3000, a(n) \u003e 0. Conjecture: a(n) \u003e 0 for all n \u003e 10. - _Jon E. Schoenfield_, Dec 14 2013"
			],
			"link": [
				"Jon E. Schoenfield, \u003ca href=\"/A233293/b233293.txt\"\u003eTable of n, a(n) for n = 0..3000\u003c/a\u003e"
			],
			"example": [
				"a(0) = 3 because no 3x + 1 trajectories have 3 as their largest value, and 3 is the smallest number for which this is the case.",
				"a(1) = 1 because exactly one 3x + 1 trajectory (namely, the one whose initial value is 1) has 1 as its largest value (and 1 is the smallest number for which this is the case).",
				"a(3) = 40 because exactly three 3x + 1 trajectories (the ones whose initial values are 13, 26, and 40) have 40 as their largest value, and 40 is the smallest number for which this is the case.",
				"a(2) = 0 because there exists no number that is the largest value in exactly two 3x + 1 trajectories."
			],
			"mathematica": [
				"CollatzSeq[n_] := NestWhileList[If[EvenQ[#], #/2, 3 # + 1] \u0026, n, # \u003e 1 \u0026]; nn = 250000; c = Table[Max[CollatzSeq[n]], {n, nn}]; srt = Tally[Sort[c]]; times = Transpose[srt][[2]]; u = Union[times]; d = Differences[u]; mx = Position[d, _?(# \u003e 1 \u0026), 1, 1][[1, 1]] - 1; u2 = Transpose[Take[srt, 10]][[1]]; t0 = Complement[Range[u2[[-1]]], u2][[1]]; t = Table[k = srt[[Position[times, n, 1, 1][[1, 1]]]]; If[k[[1]] \u003e nn, 0, k[[1]]], {n, mx}]; t = Join[{t0}, t] (* _T. D. Noe_, Dec 07 2013 *)"
			],
			"xref": [
				"Cf. A025586, A213199, A222562, A232870."
			],
			"keyword": "nonn",
			"offset": "0,1",
			"author": "_Jon E. Schoenfield_, Dec 06 2013",
			"references": 5,
			"revision": 28,
			"time": "2014-01-14T10:09:38-05:00",
			"created": "2013-12-07T14:57:17-05:00"
		}
	]
}