{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A160412",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 160412,
			"data": "0,3,12,21,48,57,84,111,192,201,228,255,336,363,444,525,768,777,804,831,912,939,1020,1101,1344,1371,1452,1533,1776,1857,2100,2343,3072,3081,3108,3135,3216,3243,3324,3405,3648,3675,3756,3837,4080,4161,4404,4647",
			"name": "Number of \"ON\" cells at n-th stage in simple 2-dimensional cellular automaton (see Comments for precise definition).",
			"comment": [
				"From _Omar E. Pol_, Nov 10 2009: (Start)",
				"On the infinite square grid, consider the outside corner of an infinite square.",
				"We start at round 0 with all cells in the OFF state.",
				"The rule: A cell in turned ON iff exactly one of its four vertices is a corner vertex of the set of ON cells. So in each generation every exposed vertex turns on three new cells.",
				"At round 1, we turn ON three cells around the corner of the infinite square, forming a concave-convex hexagon with three exposed vertices.",
				"At round 2, we turn ON nine cells around the hexagon.",
				"At round 3, we turn ON nine other cells. Three cells around of every corner of the hexagon.",
				"And so on.",
				"Shows a fractal-like behavior similar to the toothpick sequence A153006.",
				"For the first differences see the entry A162349.",
				"For more information see A160410, which is the main entry for this sequence.",
				"(End)"
			],
			"link": [
				"David Applegate, Omar E. Pol and N. J. A. Sloane, \u003ca href=\"/A000695/a000695_1.pdf\"\u003eThe Toothpick Sequence and Other Sequences from Cellular Automata\u003c/a\u003e, Congressus Numerantium, Vol. 206 (2010), 157-191. [There is a typo in Theorem 6: (13) should read u(n) = 4.3^(wt(n-1)-1) for n \u003e= 2.]",
				"Omar E. Pol, \u003ca href=\"http://www.polprimos.com/imagenespub/polca027.jpg\"\u003eIllustration of initial terms\u003c/a\u003e [From _Omar E. Pol_, Nov 10 2009]",
				"N. J. A. Sloane, \u003ca href=\"/wiki/Catalog_of_Toothpick_and_CA_Sequences_in_OEIS\"\u003eCatalog of Toothpick and Cellular Automata Sequences in the OEIS\u003c/a\u003e",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e - _Omar E. Pol_, Nov 10 2009"
			],
			"formula": [
				"From _Omar E. Pol_, Nov 10 2009: (Start)",
				"a(n) = A160410(n)*3/4.",
				"a(0) = 0, a(n) = A130665(n-1)*3, for n\u003e0.",
				"(End)"
			],
			"example": [
				"If we label the generations of cells turned ON by consecutive numbers we get the cell pattern shown below:",
				"...77..77..77..77",
				"...766667..766667",
				"....6556....6556.",
				"....654444444456.",
				"...76643344334667",
				"...77.43222234.77",
				"......44211244...",
				"00000000001244...",
				"00000000002234.77",
				"00000000004334667",
				"0000000000444456.",
				"0000000000..6556.",
				"0000000000.766667",
				"0000000000.77..77",
				"0000000000.......",
				"0000000000.......",
				"0000000000......."
			],
			"xref": [
				"Cf. A139250, A139251, A153006, A152980, A160410, A160414.",
				"Cf. A130665, A162349. - _Omar E. Pol_, Nov 10 2009"
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Omar E. Pol_, May 20 2009, Jun 01 2009",
			"ext": [
				"More terms from _Omar E. Pol_, Nov 10 2009",
				"Edited by _Omar E. Pol_, Nov 11 2009",
				"More terms from _Nathaniel Johnston_, Nov 06 2010",
				"More terms from _Colin Barker_, Apr 19 2015"
			],
			"references": 7,
			"revision": 16,
			"time": "2021-02-24T02:48:18-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}