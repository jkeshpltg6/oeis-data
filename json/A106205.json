{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A106205",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 106205,
			"data": "1,31,-2848,413823,-68767135,12310047967,-2309368876639,447436508910495,-88755684988520798,17924937024841839390,-3671642907594608226078,760722183234128461061246,-159105706560247952472114973",
			"name": "Expansion of (q*j(q))^(1/24) where j(q) is the elliptic modular invariant (A000521).",
			"comment": [
				"From _Vaclav Kotesovec_, Jun 10 2018: (Start)",
				"For k \u003e 0, if mod(k,8) \u003c\u003e 0 then (q*j(q))^(k/24) is asymptotic to -(-1)^n * sin(k*Pi/8) * k * 3^(k/8) * Gamma(1/3)^(3*k/4) * Gamma(k/8) * exp(Pi*sqrt(3)*n) / (Pi^(k/2 + 1) * 2^(k/8 + 3) * exp(k*Pi/(8*sqrt(3))) * n^(k/8 + 1)). Equivalently, is asymptotic to -(-1)^n * k * 3^(k/8) * Gamma(1/3)^(3*k/4) * exp(Pi*sqrt(3)*(n - k/24)) / (Pi^(k/2) * 2^(k/8 + 3) * Gamma(1 - k/8) * n^(k/8 + 1)).",
				"For k \u003e 0, if mod(k,8) = 0 then (q*j(q))^(k/24) is asymptotic to exp(Pi*sqrt(2*k*n/3)) * k^(1/4) / (2^(5/4) * 3^(1/4) * n^(3/4)).",
				"(End)"
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A106205/b106205.txt\"\u003eTable of n, a(n) for n = 0..424\u003c/a\u003e"
			],
			"formula": [
				"This is essentially the eighth root of the theta series of E_8 (A108091), divided by the Dedekind eta function. - _N. J. A. Sloane_, Aug 08 2005",
				"G.f.: Product_{n\u003e=1} (1-q^n)^(A192731(n)/24). - _Seiichi Manyama_, Jul 02 2017",
				"a(n) ~ (-1)^(n+1) * c * exp(Pi*sqrt(3)*n) / n^(9/8), where c = 0.11364889078525240958152388212499254894082832445224690827436413842337... = 3^(1/8) * sqrt(2 - sqrt(2)) * Gamma(1/8) * Gamma(1/3)^(3/4) / (2^(33/8) * exp(Pi/(8 * sqrt(3))) * Pi^(3/2)). - _Vaclav Kotesovec_, Jul 02 2017, updated Mar 06 2018",
				"a(n) * A289397(n) ~ c * exp(2*Pi*sqrt(3)*n) / n^2, where c = -sqrt(2-sqrt(2)) / (16*Pi). - _Vaclav Kotesovec_, Mar 06 2018"
			],
			"example": [
				"1 + 31*q - 2848*q^2 + 413823*q^3 - 68767135*q^4 + 12310047967*q^5 - 2309368876639*q^6 + ..."
			],
			"mathematica": [
				"CoefficientList[Series[(65536 + x*QPochhammer[-1, x]^24)^(1/8) / (2*QPochhammer[-1, x]), {x, 0, 20}], x] (* _Vaclav Kotesovec_, Sep 23 2017 *)",
				"(q*1728*KleinInvariantJ[-Log[q]*I/(2*Pi)])^(1/24) + O[q]^13 // CoefficientList[#, q]\u0026 (* _Jean-François Alcover_, Nov 02 2017 *)"
			],
			"program": [
				"(PARI) {a(n)=if(n\u003c0,0, polcoeff( (ellj(x+x^2*O(x^n))*x)^(1/24),n))}"
			],
			"xref": [
				"(q*j(q))^(k/24): this sequence (k=1), A289297 (k=2), A289298 (k=3), A289299 (k=4), A289300 (k=5), A289301 (k=6), A289302 (k=7), A007245 (k=8), A289303 (k=9), A289304 (k=10), A289305 (k=11), A161361 (k=12).",
				"Cf. A000521, A192731."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Apr 25 2005",
			"references": 22,
			"revision": 34,
			"time": "2018-06-10T08:22:23-04:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}