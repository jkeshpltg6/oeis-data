{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A079317",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 79317,
			"data": "1,5,5,17,9,29,21,65,25,77,37,113,49,149,85,257,89,269,101,305,113,341,149,449,161,485,197,593,233,701,341,1025,345,1037,357,1073,369,1109,405,1217,417,1253,453,1361,489,1469,597,1793,609,1829,645,1937,681",
			"name": "Number of ON cells after n generations of cellular automaton on square grid in which cells which share exactly one edge with an ON cell change their state.",
			"comment": [
				"We work on the square grid in which each cell has four neighbors.",
				"Start with cell (0,0) ON and all other cells OFF; at each succeeding stage the cells that share exactly one edge with an ON cell change their state.",
				"This is not the CA discussed by Singmaster in the reference given in A079314. That was an error based on my misreading of the paper. - _N. J. A. Sloane_, Aug 05 2009",
				"If cells never turn OFF we get the CA of A147562.",
				"The number of active (ON,black) cells in n-th stage of growth of two-dimensional cellular automaton defined by \"Rule 678\", based on the 5-celled von Neumann neighborhood. - _Robert Price_, May 21 2016"
			],
			"reference": [
				"D. Singmaster, On the cellular automaton of Ulam and Warburton, M500 Magazine of the Open University, #195 (December 2003), pp. 2-7.",
				"S. Wolfram, A New Kind of Science, Wolfram Media, 2002; p. 170."
			],
			"link": [
				"Robert Price, \u003ca href=\"/A079317/b079317.txt\"\u003eTable of n, a(n) for n = 0..128\u003c/a\u003e",
				"David Applegate, Omar E. Pol and N. J. A. Sloane, \u003ca href=\"/A000695/a000695_1.pdf\"\u003eThe Toothpick Sequence and Other Sequences from Cellular Automata\u003c/a\u003e, Congressus Numerantium, Vol. 206 (2010), 157-191. [There is a typo in Theorem 6: (13) should read u(n) = 4.3^(wt(n-1)-1) for n \u003e= 2.]",
				"Robert Price, \u003ca href=\"/A079317/a079317.tmp.txt\"\u003eDiagrams of the first 20 stages\u003c/a\u003e",
				"D. Singmaster, \u003ca href=\"/A079314/a079314.pdf\"\u003eOn the cellular automaton of Ulam and Warburton\u003c/a\u003e, 2003 [Cached copy, included with permission]",
				"N. J. A. Sloane, \u003ca href=\"/wiki/Catalog_of_Toothpick_and_CA_Sequences_in_OEIS\"\u003eCatalog of Toothpick and Cellular Automata Sequences in the OEIS\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"http://arxiv.org/abs/1503.01168\"\u003eOn the Number of ON Cells in Cellular Automata\u003c/a\u003e, arXiv:1503.01168 [math.CO], 2015",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ElementaryCellularAutomaton.html\"\u003eElementary Cellular Automaton\u003c/a\u003e",
				"S. Wolfram, \u003ca href=\"http://wolframscience.com/\"\u003eA New Kind of Science\u003c/a\u003e",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_2D_5-Neighbor_Cellular_Automata\"\u003eIndex to 2D 5-Neighbor Cellular Automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_Elementary_Cellular_Automata\"\u003eIndex to Elementary Cellular Automata\u003c/a\u003e"
			],
			"formula": [
				"a(n) = a(n-1) + A151921(n) (and we have an explicit formula for A151921)."
			],
			"example": [
				"Generations 1 through 4 (X = ON):",
				"..................X",
				"..........X......XXX",
				"....X...........X...X",
				"X..XXX..X.X.X..XX.X.XX",
				"....X...........X...X",
				"..........X......XXX",
				"..................X",
				"...........Sizes of first 20 generations:.........",
				".........n...OFF-\u003eON...ON-\u003eOFF..Net gain..Total ON",
				".........n...A079315.(A147582)...A151921...A079317",
				"--------------------------------------------------",
				".........0.........0.........0.........0.........0",
				".........1.........1.........0.........1.........1",
				".........2.........4.........0.........4.........5",
				".........3.........4.........4.........0.........5",
				".........4........12.........0........12........17",
				".........5.........4........12........-8.........9",
				".........6........20.........0........20........29",
				".........7........12........20........-8........21",
				".........8........44.........0........44........65",
				".........9.........4........44.......-40........25",
				"........10........52.........0........52........77",
				"........11........12........52.......-40........37",
				"........12........76.........0........76.......113",
				"........13........12........76.......-64........49",
				"........14.......100.........0.......100.......149",
				"........15........36.......100.......-64........85",
				"........16.......172.........0.......172.......257",
				"........17.........4.......172......-168........89",
				"........18.......180.........0.......180.......269",
				"........19........12.......180......-168.......101",
				"........20.......204.........0.......204.......305"
			],
			"xref": [
				"Cf. A079315 gives number which change from OFF to ON at generation n, A151921 gives net gain in number of ON cells."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Feb 12 2003",
			"ext": [
				"More terms from _John W. Layman_, Oct 29 2003",
				"Edited by _N. J. A. Sloane_, Aug 05 2009"
			],
			"references": 8,
			"revision": 25,
			"time": "2021-02-24T02:48:18-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}