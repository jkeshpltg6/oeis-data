{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A286663",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 286663,
			"data": "0,2,0,4,0,4,6,37,17,21,75,27,3,20,96,21,13,90,37,26,22,20,204,12,148,23,46,24,0,71,22,3,36,41,4,101,228,31,155,304,309,392,146,85,222,346,134,277,43,7,67,484,230,152,10,135,40,256,28,97,129,90,458",
			"name": "a(n) is the least value of k such that for the n-th prime p, p^2 divides Bell(p+k)-Bell(k+1)-Bell(k).",
			"comment": [
				"Jacques Touchard proved in 1933 that for the Bell numbers (A000110), Bell(p+k) == Bell(k+1) + Bell(k) (mod p) for all primes p and k \u003e= 0."
			],
			"reference": [
				"J. Touchard, \"Propriétés arithmétiques de certains nombres récurrents\", Ann. Soc. Sci. Bruxelles A 53 (1933), pp. 21-31."
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A286663/b286663.txt\"\u003eTable of n, a(n) for n = 1..100\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/TouchardsCongruence.html\"\u003eTouchard's Congruence\u003c/a\u003e"
			],
			"example": [
				"The 7th prime is p(7) = 17, and the least k such that Bell(k+17)-Bell(k)-Bell(k+1) is divisible by 17^2 is k = 6: Bell(23)-Bell(6)-Bell(7) = 44152005855083266 = 17^2*152775106764994, thus a(7) = 6."
			],
			"mathematica": [
				"a={};np = 100; p = Prime[Range[np]]; For[i = 0, i \u003c np, i++; p1 = p[[i]];",
				"n = 0; While[!Divisible[BellB[p1 + n] - BellB[n] - BellB[n + 1], p1^2], n++]; a=AppendTo[a,n]]; a"
			],
			"program": [
				"(PARI) bell(n) = polcoeff(sum(k=0, n, prod(i=1, k, x/(1-i*x)), x^n * O(x)), n);",
				"a(n) = {my(k = 0, p = prime(n)); while ((bell(p+k)-bell(k+1)-bell(k)) % p^2, k++); k;} \\\\ _Michel Marcus_, May 20 2017"
			],
			"xref": [
				"Cf. A000110, A283300, A286664."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Amiram Eldar_, May 12 2017",
			"references": 2,
			"revision": 14,
			"time": "2017-05-20T22:46:17-04:00",
			"created": "2017-05-17T15:56:02-04:00"
		}
	]
}