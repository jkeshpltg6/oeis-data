{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A103365",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 103365,
			"data": "1,-1,2,-7,39,-321,3681,-56197,1102571,-27036487,810263398,-29139230033,1238451463261,-61408179368043,3513348386222286,-229724924077987509,17023649385410772579,-1419220037471837658603,132236541042728184852942,-13690229149108218523467549",
			"name": "First column of triangle A103364, which equals the matrix inverse of the Narayana triangle (A001263).",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A103365/b103365.txt\"\u003eTable of n, a(n) for n = 1..250\u003c/a\u003e"
			],
			"formula": [
				"From _Paul D. Hanna_, Jan 31 2009: (Start)",
				"G.f.: A(x) = 1/B(x) where A(x) = Sum_{n\u003e=0} (-1)^n*a(n)*x^n/[n!*(n+1)!/2^n] and B(x) = Sum_{n\u003e=0} x^n/[n!*(n+1)!/2^n].",
				"G.f. satisfies: A(x) = 1/F(x*A(x)) and F(x) = 1/A(x*F(x)) where F(x) = Sum_{n\u003e=0} A155926(n)*x^n/[n!*(n+1)!/2^n].",
				"G.f. satisfies: A(x) = 1/G(x/A(x)) and G(x) = 1/A(x/G(x)) where G(x) = Sum_{n\u003e=0} A155927(n)*x^n/[n!*(n+1)!/2^n]. (End)",
				"a(n) ~ (-1)^(n+1) * c * n! * (n-1)! * d^n, where d = 4/BesselJZero[1, 1]^2 = 0.2724429913055159309179376055957891881897555639652..., and c = 9.11336321311226744479181866135367355200240221549667284076... = BesselJZero[1, 1]^2 / (4*BesselJ[2, BesselJZero[1, 1]]). - _Vaclav Kotesovec_, Mar 01 2014, updated Apr 01 2018"
			],
			"example": [
				"From _Paul D. Hanna_, Jan 31 2009: (Start)",
				"G.f.: A(x) = 1 - x + 2*x^2/3 - 7*x^3/18 + 39*x^4/180 - 321*x^5/2700 +...",
				"G.f.: A(x) = 1/B(x) where:",
				"B(x) = 1 + x + x^2/3 + x^3/18 + x^4/180 + x^5/2700 +...+ x^n/[n!*(n+1)!/2^n] +... (End)"
			],
			"mathematica": [
				"Table[(-1)^((n-1)/2) * (CoefficientList[Series[x/BesselJ[1,2*x],{x,0,40}],x])[[n]] * ((n+1)/2)! * ((n-1)/2)!,{n,1,41,2}] (* _Vaclav Kotesovec_, Mar 01 2014 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c1,0,(matrix(n,n,m,j,binomial(m-1,j-1)*binomial(m,j-1)/j)^-1)[n,1])",
				"(PARI) {a(n)=local(B=sum(k=0,n,x^k/(k!*(k+1)!/2^k))+x*O(x^n));polcoeff(1/B,n)*n!*(n+1)!/2^n} \\\\ _Paul D. Hanna_, Jan 31 2009"
			],
			"xref": [
				"Cf. A001263, A103364, A103366, A103367.",
				"Cf. A155926, A155927. [_Paul D. Hanna_, Jan 31 2009]",
				"Cf. A238390, A180874, A115369."
			],
			"keyword": "sign",
			"offset": "1,3",
			"author": "_Paul D. Hanna_, Feb 02 2005",
			"references": 10,
			"revision": 18,
			"time": "2018-04-01T07:53:55-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}