{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A278900",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 278900,
			"data": "1,1,1,15,4,55,0,247,20,983,20,4087,84,16215,84,65495,340,261463,340,1048407,1364,4191575,1364,16776535,5460,67097943,5460,268432727,21844,1073698135,21844,4294956375,87380,17179694423,87380,68719433047,349524,274877207895",
			"name": "Decimal representation of the x-axis, from the left edge to the origin, of the n-th stage of growth of the two-dimensional cellular automaton defined by \"Rule 107\", based on the 5-celled von Neumann neighborhood.",
			"comment": [
				"Initialized with a single black (ON) cell at stage zero."
			],
			"reference": [
				"S. Wolfram, A New Kind of Science, Wolfram Media, 2002; p. 170."
			],
			"link": [
				"Robert Price, \u003ca href=\"/A278900/b278900.txt\"\u003eTable of n, a(n) for n = 0..126\u003c/a\u003e",
				"Robert Price, \u003ca href=\"/A278900/a278900.tmp.txt\"\u003eDiagrams of first 20 stages\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"http://arxiv.org/abs/1503.01168\"\u003eOn the Number of ON Cells in Cellular Automata\u003c/a\u003e, arXiv:1503.01168 [math.CO], 2015",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ElementaryCellularAutomaton.html\"\u003eElementary Cellular Automaton\u003c/a\u003e",
				"S. Wolfram, \u003ca href=\"http://wolframscience.com/\"\u003eA New Kind of Science\u003c/a\u003e",
				"Wolfram Research, \u003ca href=\"http://atlas.wolfram.com\"\u003eWolfram Atlas of Simple Programs\u003c/a\u003e.",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_2D_5-Neighbor_Cellular_Automata\"\u003eIndex to 2D 5-Neighbor Cellular Automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_Elementary_Cellular_Automata\"\u003eIndex to Elementary Cellular Automata\u003c/a\u003e"
			],
			"formula": [
				"Conjectures from _Colin Barker_, Nov 30 2016: (Start)",
				"a(n) = 5*a(n-2) - 20*a(n-6) + 16*a(n-8) for n\u003e15.",
				"G.f.: (1 +x -4*x^2 +10*x^3 -x^4 -20*x^5 -8*x^7 +24*x^8 +32*x^9 -16*x^10 +32*x^11 -80*x^12 -160*x^13 +64*x^14 +128*x^15) / ((1 -x)*(1 +x)*(1 -2*x)*(1 +2*x)*(1 -2*x^2)*(1 +2*x^2)).",
				"(End)"
			],
			"mathematica": [
				"CAStep[rule_, a_] := Map[rule[[10 - #]] \u0026, ListConvolve[{{0, 2, 0}, {2, 1, 2},{0, 2, 0}}, a, 2], {2}];",
				"code = 107; stages = 128;",
				"rule = IntegerDigits[code, 2, 10];",
				"g = 2 * stages + 1; (* Maximum size of grid *)",
				"a = PadLeft[{{1}}, {g, g}, 0, Floor[{g, g}/2]]; (* Initial ON cell on grid *)",
				"ca = a;",
				"ca = Table[ca = CAStep[rule, ca], {n, 1, stages + 1}];",
				"PrependTo[ca, a];",
				"(* Trim full grid to reflect growth by one cell at each stage *)",
				"k = (Length[ca[[1]]] + 1)/2;",
				"ca = Table[Table[Part[ca[[n]][[j]], Range[k + 1 - n, k - 1 + n]], {j, k + 1 - n, k - 1 + n}], {n, k}];",
				"Table[FromDigits[Part[ca[[i]][[i]], Range[1, i]], 2], {i, 1, stages - 1}]"
			],
			"xref": [
				"Cf. A278898, A278899, A278901."
			],
			"keyword": "nonn,easy",
			"offset": "0,4",
			"author": "_Robert Price_, Nov 30 2016",
			"references": 4,
			"revision": 15,
			"time": "2016-11-30T20:41:48-05:00",
			"created": "2016-11-30T13:00:50-05:00"
		}
	]
}