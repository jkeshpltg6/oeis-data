{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A073107",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 73107,
			"data": "1,2,1,5,4,1,16,15,6,1,65,64,30,8,1,326,325,160,50,10,1,1957,1956,975,320,75,12,1,13700,13699,6846,2275,560,105,14,1,109601,109600,54796,18256,4550,896,140,16,1,986410,986409,493200,164388,41076,8190,1344",
			"name": "Triangle T(n,k) read by rows, where e.g.f. for T(n,k) is exp((1+y)*x)/(1-x) (with n \u003e= 0 and 0 \u003c= k \u003c= n).",
			"comment": [
				"Triangle is second binomial transform of A008290. - _Paul Barry_, May 25 2006",
				"-exp(-x)*Sum(k=0,n,T[n,k]*x^k) = Integral (x+1)^n*exp(-x) dx = -exp(1)*Gamma(n+1,x+1). - _Gerald McGarvey_, Mar 15 2009",
				"Ignoring signs, n-th row is the coefficient list of the permanental polynomial of the n X n matrix with 2's along the main diagonal and 1's everywhere else (see Mathematica code below). - _John M. Campbell_, Jul 02 2012"
			],
			"link": [
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Sheffer_sequence\"\u003eSheffer sequence\u003c/a\u003e."
			],
			"formula": [
				"O.g.f. for k-th column is (1/k!)*Sum_{i \u003e= k} i!*x^i/(1-x)^(i+1).",
				"For n \u003e 0, T(n, 0) = floor(n!*exp(1)) = A000522(n), T(n, 1) = floor(n!*exp(1) - 1) = A007526(n), T(n, 2) = 1/2!*floor(n!*exp(1) - 1 - n) = A038155(n), T(n, 3) = 1/3!*floor(n!*exp(1) - 1 - n - n*(n - 1)), T(n, 4) = 1/4!*floor(n!*exp(1) - 1 - n - n*(n - 1) - n*(n - 1)*(n - 2)), ... .",
				"Row sums give A010842.",
				"E.g.f. for k-th column is (x^k/k!)*exp(x)/(1 - x).",
				"O.g.f. for k-th row is n!*Sum_{k = 0..n} (1 + x)^k/k!.",
				"T(n,k) = Sum_{j = 0..n} binomial(j,k)*n!/j!. - _Paul Barry_, May 25 2006",
				"From _Peter Bala_, Sep 20 2012: (Start)",
				"Exponential Riordan array [exp(x)/(1-x),x] belonging to the Appell subgroup, which factorizes in the Appell group as [1/1-x,x]*[exp(x),x] = A094587*A007318.",
				"The n-th row polynomial R(n,x) of the triangle satisfies d/dx(R(n,x)) = n*R(n-1,x), as well as R(n,x + y) = Sum {k = 0..n} binomial(n,k)*R(k,x)*y^(n-k). The row polynomials are a Sheffer sequence of Appell type.",
				"Matrix inverse of triangle is a signed version of A093375.",
				"(End)",
				"From _Tom Copeland_, Oct 20 2015: (Start)",
				"The raising operator, with D = d/dx, for the row polynomials is RP = x + d{log[e^D/(1-D)]}/dD = x + 1 + 1/(1-D) =  x + 2 + D + D^2 + ..., i.e., RP R(n,x) = R(n+1,x).",
				"This operator is the limit as t tends to 1 of the raising operator of the polynomials p(n,x;t) described in A046802, implying R(n,x) = p(n,x;1). Compare with the raising operator of A094587, x + 1/(1-D), and that of signed A093375, x - 1 - 1/(1-D).",
				"From the Appell formalism, the row polynomials RI(n,x) of signed A093375 are the umbral inverse of this entry's row polynomials; that is, R(n,RI(.,x)) = x^n = RI(n,R(.,x)) under umbral composition.",
				"(End)",
				"From _Werner Schulte_, Sep 07 2020: (Start)",
				"T(n,k) = (n! / k!) * (Sum_{i=k..n} 1 / (n-i)!) for 0 \u003c= k \u003c= n.",
				"T(n,k) = n * T(n-1,k) + binomial(n,k) for 0 \u003c= k \u003c= n with initial values T(0,0) = 1 and T(i,j) = 0 if j \u003c 0 or j \u003e i.",
				"T(n,k) = A000522(n-k) * binomial(n,k) for 0 \u003c= k \u003c= n. (End)"
			],
			"example": [
				"exp((1 + y)*x)/(1 - x) =",
				"  1 +",
				"  1/! * (2 + y) * x +",
				"  1/2! * (5 + 4*y + y^2) * x^2 +",
				"  1/3! * (16 + 15*y + 6*y^2 + y^3) * x^3 +",
				"  1/4! * (65 + 64*y + 30*y^2 + 8*y^3 + y^4) * x^4 +",
				"  1/5! * (326 + 325*y + 160*y^2 + 50*y^3 + 10*y^4 + y^5) * x^5 + ..."
			],
			"mathematica": [
				"Permanent[m_List] := With[{v=Array[x,Length[m]]},Coefficient[Times@@(m.v),Times@@v]] ;",
				"A[q_] := Array[KroneckerDelta[#1,#2] + 1\u0026,{q,q}] ;",
				"n = 1 ; While[n \u003c 10, Print[Abs[CoefficientList[Permanent[A[n] - IdentityMatrix[n] * k], k]]]; n++] (* _John M. Campbell_, Jul 02 2012 *)"
			],
			"xref": [
				"Cf. A008290, A008291, A046802, A093375 (unsigned inverse), A094587.",
				"Column 0 is A000522."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "0,2",
			"author": "_Vladeta Jovovic_, Aug 19 2002",
			"ext": [
				"More terms from _Emeric Deutsch_, Feb 23 2004"
			],
			"references": 5,
			"revision": 48,
			"time": "2020-10-05T12:25:01-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}