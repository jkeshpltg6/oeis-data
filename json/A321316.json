{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A321316",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 321316,
			"data": "1,1,0,1,1,0,4,0,1,1,0,9,4,9,0,1,1,0,16,25,36,25,16,0,1,1,0,25,81,125,256,125,81,25,0,1,1,0,36,196,421,1225,1282,1225,421,196,36,0,1,1,0,49,400,1225,4292,9261,9864,9261,4292,1225,400,49,0,1",
			"name": "Number T(n,k) of permutations of [n] whose difference between the length of the longest increasing subsequence and the length of the longest decreasing subsequence equals k; triangle T(n,k), n \u003e= 1, 1-n \u003c= k \u003c= n-1, read by rows.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A321316/b321316.txt\"\u003eRows n = 1..80, flattened\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Longest_increasing_subsequence\"\u003eLongest increasing subsequence\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = T(n,-k).",
				"Sum_{k=1..n-1} T(n,k) = A321314(n).",
				"Sum_{k=0..n-1} T(n,k) = A321315(n).",
				"(1/2) * Sum_{k=1-n..n-1} abs(k) * T(n,k) = A321277(n).",
				"(1/2) * Sum_{k=1-n..n-1}   k^2  * T(n,k) = A321278(n)."
			],
			"example": [
				":                                1                             ;",
				":                          1,    0,    1                       ;",
				":                    1,    0,    4,    0,   1                  ;",
				":               1,   0,    9,    4,    9,   0,   1             ;",
				":          1,   0,  16,   25,   36,   25,  16,   0,  1         ;",
				":      1,  0,  25,  81,  125,  256,  125,  81,  25,  0, 1      ;",
				":   1, 0, 36, 196, 421, 1225, 1282, 1225, 421, 196, 36, 0, 1   ;"
			],
			"maple": [
				"h:= l-\u003e (n-\u003e add(i, i=l)!/mul(mul(1+l[i]-j+add(`if`(j\u003e",
				"    l[k], 0, 1), k=i+1..n), j=1..l[i]), i=1..n))(nops(l)):",
				"f:= l-\u003e h(l)^2*x^(l[1]-nops(l)) :",
				"g:= (n, i, l)-\u003e `if`(n=0 or i=1, f([l[], 1$n]),",
				"     g(n, i-1, l) +g(n-i, min(i, n-i), [l[], i])):",
				"b:= proc(n) option remember; g(n$2, []) end:",
				"T:= (n, k)-\u003e coeff(b(n), x, k):",
				"seq(seq(T(n, k), k=1-n..n-1), n=1..10);"
			],
			"mathematica": [
				"h[l_] := With[{n = Length[l]}, Total[l]!/Product[Product[1 + l[[i]] - j + Sum[If[j \u003e l[[k]], 0, 1], {k, i + 1, n}], {j, 1, l[[i]]}], {i, 1, n}]];",
				"f[l_] := h[l]^2*x^(l[[1]] - Length[l]);",
				"g[n_, i_, l_] := If[n == 0 || i == 1, f[Join[l, Table[1, {n}]]], g[n, i - 1, l] + g[n - i, Min[i, n - i], Append[l, i]]];",
				"b[n_] := b[n] = g[n, n, {}];",
				"T[n_, k_] := Coefficient[b[n], x, k];",
				"Table[Table[T[n, k], {k, 1 - n, n - 1}], {n, 1, 10}] // Flatten (* _Jean-François Alcover_, Feb 27 2021, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Column k=0 gives A321313.",
				"Row sums give A000142.",
				"T(n+1,n-2) gives A000290.",
				"Cf. A303697, A321277, A321278, A321314, A321315."
			],
			"keyword": "nonn,tabf",
			"offset": "1,7",
			"author": "_Alois P. Heinz_, Nov 03 2018",
			"references": 7,
			"revision": 28,
			"time": "2021-02-27T11:18:31-05:00",
			"created": "2018-11-04T10:52:45-05:00"
		}
	]
}