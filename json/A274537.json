{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A274537",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 274537,
			"data": "1,0,1,0,0,1,0,0,1,1,0,0,1,1,1,0,0,1,3,2,1,0,0,1,3,7,2,1,0,0,1,7,14,13,3,1,0,0,1,7,35,26,22,3,1,0,0,1,15,70,113,66,34,4,1,0,0,1,15,155,226,311,102,50,4,1,0,0,1,31,310,833,933,719,200,70,5,1",
			"name": "Number T(n,k) of set partitions of [n] into k blocks such that each element is contained in a block whose index parity coincides with the parity of the element; triangle T(n,k), n\u003e=0, 0\u003c=k\u003c=n, read by rows.",
			"comment": [
				"All odd elements are in blocks with an odd index and all even elements are in blocks with an even index."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A274537/b274537.txt\"\u003eRows n = 0..140, flattened\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Partition_of_a_set\"\u003ePartition of a set\u003c/a\u003e"
			],
			"example": [
				"T(6,2) = 1: 135|246.",
				"T(6,3) = 3: 13|246|5, 15|246|3, 1|246|35.",
				"T(6,4) = 7: 13|24|5|6, 15|24|3|6, 1|24|35|6, 15|26|3|4, 15|2|3|46, 1|26|35|4, 1|2|35|46.",
				"T(6,5) = 2: 1|26|3|4|5, 1|2|3|46|5.",
				"T(6,6) = 1: 1|2|3|4|5|6.",
				"Triangle T(n,k) begins:",
				"  1;",
				"  0, 1;",
				"  0, 0, 1;",
				"  0, 0, 1,  1;",
				"  0, 0, 1,  1,   1;",
				"  0, 0, 1,  3,   2,   1;",
				"  0, 0, 1,  3,   7,   2,   1;",
				"  0, 0, 1,  7,  14,  13,   3,   1;",
				"  0, 0, 1,  7,  35,  26,  22,   3,  1;",
				"  0, 0, 1, 15,  70, 113,  66,  34,  4, 1;",
				"  0, 0, 1, 15, 155, 226, 311, 102, 50, 4, 1;"
			],
			"maple": [
				"b:= proc(n, m, t) option remember; `if`(n=0, x^m, add(",
				"     `if`(irem(j, 2)=t, b(n-1, max(m, j), 1-t), 0), j=1..m+1))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=0..n))(b(n, 0, 1)):",
				"seq(T(n), n=0..12);"
			],
			"mathematica": [
				"b[n_, m_, t_] := b[n, m, t] = If[n==0, x^m, Sum[If[Mod[j, 2]==t, b[n-1, Max[m, j], 1-t], 0], {j, 1, m+1}]]; T[n_] := Function [p, Table[Coefficient[p, x, i], {i, 0, n}]][b[n, 0, 1]]; Table[T[n], {n, 0, 12}] // Flatten (* _Jean-François Alcover_, Dec 18 2016, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Row sums give A274538.",
				"Columns k=0-10 give: A000007, A000007(n-1), A000012(n-2), A052551(n-3), A274868, A274869, A274870, A274871, A274872, A274873, A274874.",
				"T(2n,n) gives A274875.",
				"Main diagonal and lower diagonals give: A000012, A004526, A002623(n-2) or A173196."
			],
			"keyword": "nonn,tabl",
			"offset": "0,19",
			"author": "_Alois P. Heinz_, Jun 27 2016",
			"references": 10,
			"revision": 19,
			"time": "2018-10-18T16:54:10-04:00",
			"created": "2016-06-27T10:44:38-04:00"
		}
	]
}