{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A321943",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 321943,
			"data": "3,6,9,6,6,9,2,9,9,2,4,6,0,9,3,6,8,8,5,2,2,9,2,6,3,0,8,6,3,5,5,8,3,5,7,5,6,5,9,6,8,2,1,9,4,3,3,2,1,7,8,3,8,6,5,8,5,7,3,2,0,7,6,9,5,9,6,6,8,1,6,7,4,6,1,5,7,1,9,3,7,7,7,3,7,3,0",
			"name": "Decimal expansion of Ni_1 = (1/2)*(gamma - log(2*Pi)) + 1, where gamma is Euler's constant (or the Euler-Mascheroni constant).",
			"comment": [
				"This constant links Euler's constant and Pi to the values of the Riemann zeta function at positive integers (see formulas)."
			],
			"reference": [
				"D. Suryanarayana, Sums of Riemann zeta function, Math. Student, 42 (1974), 141-143."
			],
			"link": [
				"Stefano Spezia, \u003ca href=\"/A321943/b321943.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"B. Candelpergher, \u003ca href=\"https://hal.archives-ouvertes.fr/hal-01150208\"\u003eRamanujan summation of divergent series\u003c/a\u003e, HAL Id : hal-01150208; Lecture Notes in Math. Series (Springer), 2185, (2017), 93.",
				"Marc-Antoine Coppo, \u003ca href=\"https://hal.univ-cotedazur.fr/hal-01735381\"\u003eOn certain alternating series involving zeta and multiple zeta values\u003c/a\u003e. 2018. \u003chal-01735381v4\u003e.",
				"R. J. Singh, V. P. Verma, \u003ca href=\"https://ynu.repo.nii.ac.jp/?action=repository_action_common_download\u0026amp;item_id=6701\u0026amp;item_no=1\u0026amp;attribute_id=20\u0026amp;file_no=1\"\u003eSome series involving Riemann zeta function\u003c/a\u003e, Yokohama Math. J. 31 (1983), 1-4.",
				"H. M. Srivastava, \u003ca href=\"https://doi.org/10.1016/0022-247X(88)90013-3\"\u003eSums of certain series of the Riemann zeta function\u003c/a\u003e, J. Math. Anal. App. 134 (1988), 129-140."
			],
			"formula": [
				"Ni_1 = Sum_{k\u003e=2} (-1)^k*zeta(k)/(k+1).",
				"Ni_1 = Sum_{n\u003e0} (Integral_{x=0..1} x^2*(1-x)_{n-1} dx)/(n*n!), where (z)_n = z*(z+1)*(z+2)*...*(z+n-1) is the Pochhammer symbol.",
				"Ni_1 = Sum_{n\u003e=0} A193546(n)/(A000290(n + 1)*A194506(n))."
			],
			"example": [
				"0.369669299246093688522926308635583575659682194332178386585..."
			],
			"maple": [
				"Digits := 100; evalf((1/2)*(gamma-ln(2*Pi))+1);"
			],
			"mathematica": [
				"First[RealDigits[N[(1/2)*(EulerGamma-Log[2*Pi])+1, 100], 10]]"
			],
			"program": [
				"(PARI) (1/2)*(Euler-log(2*Pi))+1",
				"(Python)",
				"from mpmath import *",
				"mp.dps = 100; mp.pretty = True",
				"+(1/2)*(euler-log(2*pi))+1"
			],
			"xref": [
				"Cf. A001620 (Euler's constant), A000796 (Pi).",
				"Cf. A193546, A000290, A194506, A131688."
			],
			"keyword": "nonn,cons",
			"offset": "0,1",
			"author": "_Stefano Spezia_, Dec 12 2018",
			"references": 1,
			"revision": 27,
			"time": "2019-02-11T20:57:49-05:00",
			"created": "2018-12-14T04:34:51-05:00"
		}
	]
}