{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A320561",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 320561,
			"data": "1,1,3,1,3,5,7,1,11,5,7,9,3,13,15,1,27,21,23,9,19,29,15,17,11,5,7,25,3,13,31,1,27,53,55,9,19,61,47,17,11,5,39,25,3,13,31,33,59,21,23,41,51,29,15,49,43,37,7,57,35,45,63",
			"name": "Irregular table read by rows: T(n,k) = (2*k+1)^(2*k+1) mod 2^n, 0 \u003c= k \u003c= 2^(n-1) - 1.",
			"comment": [
				"The sequence {k^k mod 2^n} has period 2^n. The n-th row contains 2^(n-1) numbers, and is a permutation of the odd numbers below 2^n.",
				"Note that the first 5 rows are the same as those in A320562, but after that they differ.",
				"For all n, k we have v(T(n,k)-1, 2) = v(k, 2) + 1 and v(T(n,k)+1, 2) = v(k+1, 2) + 1, where v(k, 2) = A007814(k) is the 2-adic valuation of k. [Revised by _Jianing Song_, Nov 24 2018]",
				"For n \u003e= 3, T(n,k) = 2*k + 1 iff k is divisible by 2^floor((n-1)/2) or k = 2^(n-2) - 1 or k = 2^(n-1) - 1.",
				"T(n,k) is the multiplicative inverse of A321901(n,k) modulo 2^n. - _Jianing Song_, Nov 24 2018"
			],
			"link": [
				"Muniru A Asiru, \u003ca href=\"/A320561/b320561.txt\"\u003eTable of n, a(n) for n = 1..8191\u003c/a\u003e (Rows n=1..13)"
			],
			"formula": [
				"T(n,k) = A320602(2^n, 2*k+1).",
				"T(n,k) = 2^n - A321901(n,2^(n-1)-1-k). - _Jianing Song_, Nov 24 2018"
			],
			"example": [
				"Table starts",
				"1,",
				"1, 3,",
				"1, 3, 5, 7,",
				"1, 11, 5, 7, 9, 3, 13, 15,",
				"1, 27, 21, 23, 9, 19, 29, 15, 17, 11, 5, 7, 25, 3, 13, 31,",
				"1, 27, 53, 55, 9, 19, 61, 47, 17, 11, 5, 39, 25, 3, 13, 31, 33, 59, 21, 23, 41, 51, 29, 15, 49, 43, 37, 7, 57, 35, 45, 63,",
				"..."
			],
			"mathematica": [
				"Table[PowerMod[(2 k + 1), (2 k + 1), 2^n], {n, 6}, {k, 0, 2^(n - 1) - 1}] // Flatten (* _Michael De Vlieger_, Oct 22 2018 *)"
			],
			"program": [
				"(PARI) T(n,k) = lift(Mod(2*k+1, 2^n)^(2*k+1))",
				"tabf(nn) = for(n=1, nn, for(k=0, 2^(n-1)-1, print1(T(n, k), \", \")); print);",
				"(GAP) T:= Flat(List([1..6],n-\u003eList([0..2^(n-1)-1],k-\u003ePowerMod(2*k+1,2*k+1,2^n)))); # _Muniru A Asiru_, Oct 23 2018"
			],
			"xref": [
				"Cf. A007814.",
				"{x^x} and its inverse: this sequence \u0026 A320562.",
				"{x^(-x)} and its inverse: A321901 \u0026 A321904.",
				"{x^(1/x)} and its inverse: A321902 \u0026 A321905.",
				"{x^(-1/x)} and its inverse: A321903 \u0026 A321906."
			],
			"keyword": "nonn,tabf",
			"offset": "1,3",
			"author": "_Jianing Song_, Oct 15 2018",
			"references": 8,
			"revision": 26,
			"time": "2018-12-22T16:48:11-05:00",
			"created": "2018-10-22T22:41:07-04:00"
		}
	]
}