{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A193401",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 193401,
			"data": "0,1,0,-2,1,0,3,-4,1,0,3,-4,1,0,-4,10,-6,1,0,-4,10,-6,1,0,-4,9,-6,1,0,-4,9,-6,1,0,5,-20,21,-8,1,0,5,-20,21,-8,1,0,5,-20,21,-8,1,0,5,-18,20,-8,1,0,5,-18,20,-8,1,0,5,-18,20,-8,1,0,-6,35,-56,36,-10,1,0,5,-16,18,-8,1,0,5,-18,20,-8,1",
			"name": "Triangle read by rows: row n contains the coefficients (of the increasing powers of the variable) of the characteristic polynomial of the Laplacian matrix of the rooted tree having Matula-Göbel number n.",
			"comment": [
				"Row n contains 1+A061775(n) entries (= 1+ number of vertices of the rooted tree).",
				"The Matula-Göbel number of a rooted tree can be defined in the following recursive manner: to the one-vertex tree there corresponds the number 1; to a tree T with root degree 1 there corresponds the t-th prime number, where t is the Matula-Göbel number of the tree obtained from T by deleting the edge emanating from the root; to a tree T with root degree m\u003e=2 there corresponds the product of the Matula-Göbel numbers of the m branches of T."
			],
			"link": [
				"E. Deutsch, \u003ca href=\"http://arxiv.org/abs/1111.4288\"\u003e Rooted tree statistics from Matula numbers\u003c/a\u003e, arXiv:1111.4288 [math.CO], 2011.",
				"F. Göbel, \u003ca href=\"http://dx.doi.org/10.1016/0095-8956(80)90049-0\"\u003eOn a 1-1-correspondence between rooted trees and natural numbers\u003c/a\u003e, J. Combin. Theory, B 29 (1980), 141-143.",
				"I. Gutman and A. Ivic, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(95)00182-V\"\u003eOn Matula numbers\u003c/a\u003e, Discrete Math., 150, 1996, 131-142.",
				"I. Gutman and Yeong-Nan Yeh, \u003ca href=\"http://www.emis.de/journals/PIMB/067/3.html\"\u003eDeducing properties of trees from their Matula numbers\u003c/a\u003e, Publ. Inst. Math., 53 (67), 1993, 17-22.",
				"D. W. Matula, \u003ca href=\"http://www.jstor.org/stable/2027327\"\u003eA natural rooted tree enumeration by prime factorization\u003c/a\u003e, SIAM Rev. 10 (1968) 273.",
				"\u003ca href=\"/index/Mat#matula\"\u003eIndex entries for sequences related to Matula-Goebel numbers\u003c/a\u003e"
			],
			"formula": [
				"Let T be a rooted tree with root b. If b has degree 1, then let A be the rooted tree with root c, obtained from T by deleting the edge bc emanating from the root. If b has degree \u003e=2, then A is obtained (not necessarily in a unique way) by joining at b two trees B and C, rooted at b. It is straightforward to express the distance matrix of T in terms of the entries of the distance matrix of A (resp. of B and C). Making use of this, the Maple program (improvable!) finds recursively the distance matrices of the rooted trees with Matula-Göbel numbers 1..1000 (upper limit can be altered), then switches (easily) to the Laplacian matrices and finds the coefficients of their characteristic polynomials."
			],
			"example": [
				"Row 4 is 0, 3, -4, 1 because the rooted tree having Matula-Goebel number 4 is V; the Laplacian matrix is [2,-1,-1; -1,1,0; -1,0,1], having characteristic polynomial x^3 - 4x^2 +3x",
				"Triangle starts:",
				"0, 1;",
				"0, -2, 1;",
				"0, 3, -4, 1;",
				"0, 3, -4, 1;",
				"0, -4, 10, -6, 1;",
				"0, -4, 10, -6, 1;",
				"0, -4, 9, -6, 1;",
				"0, -4, 9, -6, 1;"
			],
			"maple": [
				"with(numtheory): with(linalg): with(LinearAlgebra): DA := proc (d) local aa: aa := proc (i, j) if d[i, j] = 1 then 1 else 0 end if end proc: Matrix(RowDimension(d), RowDimension(d), aa) end proc: AL := proc (a) local ll: ll := proc (i, j) if i = j then add(a[i, k], k = 1 .. RowDimension(a)) else -a[i, j] end if end proc: Matrix(RowDimension(a), RowDimension(a), ll) end proc: V := proc (n) local r, s: r := proc (n) options operator, arrow: op(1, factorset(n)) end proc: s := proc (n) options operator, arrow: n/r(n) end proc: if n = 1 then 1 elif bigomega(n) = 1 then 1+V(pi(n)) else V(r(n))+V(s(n))-1 end if end proc: d := proc (n) local r, s, C, a: r := proc (n) options operator, arrow: op(1, factorset(n)) end proc: s := proc (n) options operator, arrow: n/r(n) end proc: C := proc (A, B) local c: c := proc (i, j) options operator, arrow: A[1, i]+B[1, j+1] end proc: Matrix(RowDimension(A), RowDimension(B)-1, c) end proc: a := proc (i, j) if i = 1 and j = 1 then 0 elif 2 \u003c= i and 2 \u003c= j then dd[pi(n)][i-1, j-1] elif i = 1 then 1+dd[pi(n)][1, j-1] elif j = 1 then 1+dd[pi(n)][i-1, 1] else  end if end proc: if n = 1 then Matrix(1, 1, [0]) elif bigomega(n) = 1 then Matrix(V(n), V(n), a) else Matrix(blockmatrix(2, 2, [dd[r(n)], C(dd[r(n)], dd[s(n)]), Transpose(C(dd[r(n)], dd[s(n)])), SubMatrix(dd[s(n)], 2 .. RowDimension(dd[s(n)]), 2 .. RowDimension(dd[s(n)]))])) end if end proc: for n to 1000 do dd[n] := d(n) end do: for n from 1 to 18 do seq(coeff(CharacteristicPolynomial(AL(DA(d(n))), x), x, k), k = 0 .. V(n)) end do; # yields triangle in triangular form"
			],
			"xref": [
				"Cf. A061775, A184187, A193403."
			],
			"keyword": "sign,tabf",
			"offset": "1,4",
			"author": "_Emeric Deutsch_, Feb 09 2012",
			"references": 2,
			"revision": 18,
			"time": "2017-03-07T06:50:41-05:00",
			"created": "2012-02-09T21:14:19-05:00"
		}
	]
}