{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A061549",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 61549,
			"data": "1,8,128,1024,32768,262144,4194304,33554432,2147483648,17179869184,274877906944,2199023255552,70368744177664,562949953421312,9007199254740992,72057594037927936,9223372036854775808,73786976294838206464,1180591620717411303424,9444732965739290427392",
			"name": "Denominator of probability that there is no error when average of n numbers is computed, assuming errors of +1, -1 are possible and they each occur with p=1/4.",
			"comment": [
				"We observe that b(n) = log(a(n))/log(2) = A120738(n). Furthermore c(n+1) = b(n+1)-b(n) = A090739(n+1) and c(n+1)-3 = A007814(n+1) for n\u003e=0. - _Johannes W. Meijer_, Jul 06 2009"
			],
			"link": [
				"Indranil Ghosh, \u003ca href=\"/A061549/b061549.txt\"\u003eTable of n, a(n) for n = 0..500\u003c/a\u003e",
				"Robert M. Kozelka, \u003ca href=\"http://www.jstor.org/stable/2322033\"\u003eGrade Point Averages and the Central Limit Theorem\u003c/a\u003e, American Mathematical Monthly. Nov. 1979 (86:9) pp. 773-7.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CircleLinePicking.html\"\u003eCircle Line Picking\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/GammaFunction.html\"\u003eGamma Function\u003c/a\u003e"
			],
			"formula": [
				"a(n) = denominator of binomial(2*n-1/2, -1/2).",
				"a(n) are denominators of coefficients of 1/(sqrt(1+x)-sqrt(1-x)) power series. - _Benoit Cloitre_, Mar 12 2002",
				"a(n) = 16^n/A001316(n). - _Paul Barry_, Jun 29 2006",
				"a(n) = denom((4*n)!/(2^(4*n)*(2*n)!^2)). - _Johannes W. Meijer_, Jul 06 2009",
				"a(n) = abs(A067624(n)/A117972(n)). - _Johannes W. Meijer_, Jul 06 2009"
			],
			"example": [
				"For n=1, the binomial(2*n-1/2, -1/2) yields the term 3/8. The denominator of this term is 8, which is the second term of the sequence."
			],
			"maple": [
				"seq(denom(binomial(2*n-1/2, -1/2)), n=0..20);"
			],
			"mathematica": [
				"Table[Denominator[(4*n)!/(2^(4*n)*(2*n)!^2) ], {n, 0, 19}] (* _Indranil Ghosh_, Mar 11 2017 *)"
			],
			"program": [
				"(Sage) # uses[A000120]",
				"def a(n): return 1 \u003c\u003c (4*n - A000120(n))",
				"[a(n) for n in (0..19)]  # _Peter Luschny_, Dec 02 2012",
				"(PARI) for(n=0, 19, print1(denominator((4*n)!/(2^(4*n)*(2*n)!^2)),\", \")) \\\\ _Indranil Ghosh_, Mar 11 2017",
				"(Python)",
				"import math",
				"from fractions import gcd",
				"f = math.factorial",
				"def A061549(n): return (2**(4*n)*f(2*n)**2)/ gcd(f(4*n), (2**(4*n)*f(2*n)**2)) # _Indranil Ghosh_, Mar 11 2017"
			],
			"xref": [
				"Cf. A061548. Bisection of A046161. Appears in A162448."
			],
			"keyword": "nonn,frac,easy",
			"offset": "0,2",
			"author": "Leah Schmelzer (leah2002(AT)mit.edu), May 16 2001",
			"ext": [
				"More terms from Asher Auel (asher.auel(AT)reed.edu), May 20 2001"
			],
			"references": 14,
			"revision": 34,
			"time": "2020-03-23T12:10:41-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}