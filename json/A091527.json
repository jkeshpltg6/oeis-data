{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A091527",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 91527,
			"data": "1,4,30,256,2310,21504,204204,1966080,19122246,187432960,1848483780,18320719872,182327718300,1820797698048,18236779032600,183120225632256,1842826521244230,18581317012684800,187679234340049620,1898554215471513600,19232182592635611060",
			"name": "a(n) = ((3*n)!/n!^2)*(Gamma(1+n/2)/Gamma(1+3n/2)).",
			"comment": [
				"Sequence terms are given by [x^n] ( (1 + x)^(k+2)/(1 - x)^k )^n for k = 1. See the crossreferences for related sequences obtained from other values of k. - _Peter Bala_, Sep 29 2015",
				"Let a \u003e b be nonnegative integers. Then the ratio of factorials ((2*a + 1)*n)!*((b + 1/2)*n)!/(((a + 1/2)*n)!*((2*b + 1)*n)!*((a - b)*n)!) is an integer for n \u003e= 0. This is the case a = 1, b = 0. - _Peter Bala_, Aug 28 2016"
			],
			"reference": [
				"R. P. Stanley, Enumerative Combinatorics Volume 2, Cambridge Univ. Press, 1999, Theorem 6.33, p. 197."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A091527/b091527.txt\"\u003eTable of n, a(n) for n = 0..985\u003c/a\u003e",
				"P. Bala, \u003ca href=\"/A276098/a276098.pdf\"\u003eSome integer ratios of factorials\u003c/a\u003e",
				"P. Barry, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL14/Barry3/barry132.html\"\u003eOn the Central Coefficients of Bell Matrices\u003c/a\u003e, J. Int. Seq. 14 (2011) # 11.4.3.",
				"Karl Dilcher, Armin Straub, and Christophe Vignat, \u003ca href=\"https://arxiv.org/abs/1903.11759\"\u003eIdentities for Bernoulli polynomials related to multiple Tornheim zeta functions\u003c/a\u003e, arXiv:1903.11759 [math.NT], 2019. See p. 11.",
				"I. M. Gessel, \u003ca href=\"http://arxiv.org/abs/1403.7656\"\u003eA short proof of the Deutsch-Sagan congruence for connected non crossing graphs\u003c/a\u003e, arXiv preprint arXiv:1403.7656 [math.CO], 2014.",
				"Bernhard Heim and Markus Neuhauser, \u003ca href=\"https://arxiv.org/abs/2107.05013\"\u003eAsymptotic Distribution of the Zeros of recursively defined Non-Orthogonal Polynomials\u003c/a\u003e, arXiv:2107.05013 [math.CA], 2021.",
				"W. Mlotkowski and K. A. Penson, \u003ca href=\"http://arxiv.org/abs/1309.0595\"\u003eProbability distributions with binomial moments\u003c/a\u003e, arXiv preprint arXiv:1309.0595 [math.PR], 2013."
			],
			"formula": [
				"D-finite with recurrence n*(n - 1)*a(n) = 12*(3*n - 1)*(3*n - 5)*a(n-2).",
				"From _Peter Bala_, Sep 29 2015: (Start)",
				"a(n) = Sum_{i = 0..n} binomial(3*n,i)*binomial(2*n-i-1,n-i).",
				"a(n) = [x^n] ( (1 + x)^3/(1 - x) )^n.",
				"exp( Sum_{n \u003e= 1} a(n)*x^n/n ) = 1 + 4*x + 23*x^2 + 156*x^3 + 1162*x^4 + 9192*x^5 + ... is the o.g.f. for A007297 (but with an offset of 0). (End)",
				"a(n) = (n+1)*A078531(n). [Barry, JIS (2011)]",
				"G.f.: x*B'(x)/B(x), where x*B(x)+1 is g.f. of A007297. - _Vladimir Kruchinin_, Oct 02 2015",
				"From _Peter Bala_, Aug 22 2016: (Start)",
				"a(n) = Sum_{k = 0..floor(n/2)} binomial(4*n,n - 2*k)*binomial(n+k-1,k).",
				"O.g.f.: A(x) = Hypergeom([5/6, 1/6], [1/2], 108*x^2) + 4*x*Hypergeom([4/3, 2/3], [3/2], 108*x^2).",
				"The o.g.f. is the diagonal of the bivariate rational function 1/(1 - t*(1 + x)^3/(1 - x)) and hence is algebraic by Stanley 1999, Theorem 6.33, p. 197. (End)",
				"a(n) ~ 2^n*3^(3*n/2)/sqrt(2*Pi*n). - _Ilya Gutkovskiy_, Aug 22 2016",
				"a(n) = 4^n*2*(n+1)*binomial((3*n-1)/2, n+1)/(n-1) for n \u003e= 2. - _Peter Luschny_, Feb 03 2020"
			],
			"maple": [
				"a := n -\u003e 4^n * `if`(n\u003c2, 1, (2*(n+1)*binomial((3*n-1)/2, n + 1))/(n-1)):",
				"seq(a(n), n=0..18); # _Peter Luschny_, Feb 03 2020"
			],
			"mathematica": [
				"Table[((3 n)!/n!^2) Gamma[1 + n/2]/Gamma[1 + 3 n/2], {n, 0, 18}] (* _Michael De Vlieger_, Oct 02 2015 *)",
				"Table[4^n Sum[Binomial[k - 1 + (n - 1)/2, k], {k, 0, n}], {n, 0, 18}] (* _Michael De Vlieger_, Aug 28 2016 *)"
			],
			"program": [
				"(PARI) a(n)=4^n*sum(i=0,n,binomial(i-1+(n-1)/2,i))",
				"(Maxima)",
				"B(x):=(-1/3+(2/3)*sqrt(1+9*x)*sin((1/3)*asin((2+27*x+54*x^2)/2/(1+9*x)^(3/2))))/x-1;",
				"taylor(x*diff(B(x),x)/B(x),x,0,10); /* _Vladimir Kruchinin_, Oct 02 2015 */",
				"(PARI) vector(30, n, sum(k=0, n, binomial(3*n-3, k)*binomial(2*n-k-3, n-k-1))) \\\\ _Altug Alkan_, Oct 04 2015"
			],
			"xref": [
				"Cf. A061162(n) = a(2n), A007297, A000984 (k = 0), A001448 (k = 2), A262732 (k = 3), A211419 (k = 4), A262733 (k = 5), A211421 (k = 6), A276098, A276099."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Michael Somos_, Jan 18 2004",
			"references": 14,
			"revision": 55,
			"time": "2021-11-11T18:59:58-05:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}