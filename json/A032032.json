{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A032032",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 32032,
			"data": "1,0,1,1,7,21,141,743,5699,42241,382153,3586155,38075247,428102117,5257446533,68571316063,959218642651,14208251423433,223310418094785,3699854395380371,64579372322979335,1182959813115161773,22708472725269799933,455643187943171348103",
			"name": "Number of ways to partition n labeled elements into sets of sizes of at least 2 and order the sets.",
			"comment": [
				"With m = floor(n/2), a(n) is the number of ways to distribute n different toys to m numbered children such that each child receiving a toy gets at least two toys and, if child k gets no toys, then each child numbered higher than k also gets no toys. Furthermore, a(n)= row sums of triangle A200091 for n\u003e=2. - _Dennis P. Walsh_, Apr 15 2013",
				"Row sums of triangle A200091. - _Dennis P. Walsh_, Apr 15 2013"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A032032/b032032.txt\"\u003eTable of n, a(n) for n = 0..400\u003c/a\u003e",
				"C. G. Bower, \u003ca href=\"/transforms2.html\"\u003eTransforms (2)\u003c/a\u003e",
				"P. Flajolet and R. Sedgewick, \u003ca href=\"http://algo.inria.fr/flajolet/Publications/books.html\"\u003eAnalytic Combinatorics\u003c/a\u003e, 2009; see page 245",
				"I. Mezo, \u003ca href=\"http://arxiv.org/abs/1308.1637\"\u003ePeriodicity of the last digits of some combinatorial sequences\u003c/a\u003e, arXiv preprint arXiv:1308.1637 [math.CO], 2013 and \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Mezo/mezo19.html\"\u003eJ. Int. Seq. 17 (2014) #14.1.1\u003c/a\u003e.",
				"Robert A. Proctor, \u003ca href=\"https://arxiv.org/abs/math/0606404\"\u003eLet's Expand Rota's Twelvefold Way For Counting Partitions!\u003c/a\u003e, arXiv:math/0606404 [math.CO], 2006-2007.",
				"\u003ca href=\"/index/Par#partN\"\u003eIndex entries for related partition-counting sequences\u003c/a\u003e"
			],
			"formula": [
				"\"AIJ\" (ordered, indistinct, labeled) transform of 0, 1, 1, 1...",
				"E.g.f.: 1/(2+x-exp(x)).",
				"a(n) = n! * sum(k=1..n, sum(j=0..k, C(k,j) *stirling2(n-k+j,j) *j!/(n-k+j)! *(-1)^(k-j))); a(0)=1. - _Vladimir Kruchinin_, Feb 01 2011",
				"a(n) ~ n! / ((r-1)*(r-2)^(n+1)), where r = -LambertW(-1,-exp(-2)) = 3.14619322062... - _Vaclav Kotesovec_, Oct 08 2013",
				"a(0) = 1; a(n) = Sum_{k=2..n} binomial(n,k) * a(n-k). - _Ilya Gutkovskiy_, Feb 09 2020",
				"a(n) = Sum_{s in S_n^0} Product_{i=1..n} binomial(i,s(i)-1), where s ranges over the set S_n^0 of derangements of [n], i.e., the permutations of [n] without fixed points. - _Jose A. Rodriguez_, Feb 02 2021"
			],
			"example": [
				"For n=5, a(5)=21 since there are 21 toy distributions satisfying the conditions above. Denoting a distribution by |kid_1 toys|kid_2 toys|, we have the distributions",
				"  |t1,t2,t3,t4,t5|_|, |t1,t2,t3|t4,t5|, |t1,t2,t4|t3,t5|, |t1,t2,t5|t3,t4|, |t1,t3,t4|t2,t5|, |t1,t3,t5|t2,t4|, |t1,t4,t5|t2,t3|, |t2,t3,t4|t1,t5|, |t2,t3,t5|t1,t4|, |t2,t4,t5|t1,t3|, |t3,t4,t5|t1,t2|, |t1,t2|t3,t4,t5|, |t1,t3|t2,t4,t5|, |t1,t4|t2,t3,t5|, |t1,t5|t2,t3,t4|, |t2,t3|t1,t4,t5|, |t2,t4|t1,t3,t5|, |t2,t5|t1,t3,t4|, |t3,t4|t1,t2,t5|, |t3,t5|t1,t2,t4|, and |t4,t5|,t1,t2,t3|. - _Dennis P. Walsh_, Apr 15 2013"
			],
			"maple": [
				"spec := [ B, {B=Sequence(Set(Z,card\u003e1))}, labeled ]; [seq(combstruct[count](spec, size=n), n=0..30)];",
				"# second Maple program:",
				"b:= proc(n) b(n):= `if`(n=0, 1, add(b(n-j)/j!, j=2..n)) end:",
				"a:= n-\u003e n!*b(n):",
				"seq(a(n), n=0..25);  # _Alois P. Heinz_, Jul 29 2014"
			],
			"mathematica": [
				"a[n_] := n! * Sum[ Binomial[k, j] * StirlingS2[n-k+j, j]*j! / (n-k+j)! * (-1)^(k-j), {k, 1, n}, {j, 0, k}]; a[0] = 1; Table[a[n], {n, 0, 22}] (* _Jean-François Alcover_, Sep 05 2012, from given formula *)"
			],
			"program": [
				"(PARI) x='x+O('x^66); Vec(serlaplace( 1/(2+x-exp(x)) ) ) \\\\ _Joerg Arndt_, Apr 16 2013"
			],
			"xref": [
				"Cf. A102233, A232475.",
				"Cf. column k=2 of A245732."
			],
			"keyword": "nonn",
			"offset": "0,5",
			"author": "_Christian G. Bower_",
			"references": 28,
			"revision": 69,
			"time": "2021-02-02T19:46:45-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}