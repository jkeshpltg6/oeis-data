{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A196050",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 196050,
			"data": "0,1,2,2,3,3,3,3,4,4,4,4,4,4,5,4,4,5,4,5,5,5,5,5,6,5,6,5,5,6,5,5,6,5,6,6,5,5,6,6,5,6,5,6,7,6,6,6,6,7,6,6,5,7,7,6,6,6,5,7,6,6,7,6,7,7,5,6,7,7,6,7,6,6,8,6,7,7,6,7,8,6,6,7,7,6,7,7,6,8,7,7,7,7,7,7,7,7,8,8,6,7,7,7,8,6,6,8,6,8",
			"name": "Number of edges in the rooted tree with Matula-Goebel number n.",
			"comment": [
				"The Matula-Goebel number of a rooted tree is defined in the following recursive manner: to the one-vertex tree there corresponds the number 1; to a tree T with root degree 1 there corresponds the t-th prime number, where t is the Matula-Goebel number of the tree obtained from T by deleting the edge emanating from the root; to a tree T with root degree m\u003e=2 there corresponds the product of the Matula-Goebel numbers of the m branches of T.",
				"a(n) is, for n \u003e= 2, the number of prime function prime(.) = A000040(.) operations in the complete reduction of n. See the W. Lang link with a list of the reductions for n = 2..100, where a curly bracket notation {.} is used for prime(.). - _Wolfdieter Lang_, Apr 03 2018",
				"From _Gus Wiseman_, Mar 23 2019: (Start)",
				"Every positive integer has a unique factorization (encoded by A324924) into factors q(i) = prime(i)/i, i \u003e 0. For example:",
				"   11 = q(1) q(2) q(3) q(5)",
				"   50 = q(1)^3 q(2)^2 q(3)^2",
				"  360 = q(1)^6 q(2)^3 q(3)",
				"In this factorization, a(n) is the number of factors counted with multiplicity. For example, a(11) = 4, a(50) = 7, a(360) = 10.",
				"(End)"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A196050/b196050.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"E. Deutsch, \u003ca href=\"http://arxiv.org/abs/1111.4288\"\u003eTree statistics from Matula numbers\u003c/a\u003e, arXiv preprint arXiv:1111.4288 [math.CO], 2011.",
				"F. Göbel, \u003ca href=\"http://dx.doi.org/10.1016/0095-8956(80)90049-0\"\u003eOn a 1-1-correspondence between rooted trees and natural numbers\u003c/a\u003e, J. Combin. Theory, B 29 (1980), 141-143.",
				"I. Gutman and A. Ivic, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(95)00182-V\"\u003eOn Matula numbers\u003c/a\u003e, Discrete Math., 150, 1996, 131-142.",
				"I. Gutman and Yeong-Nan Yeh, \u003ca href=\"http://www.emis.de/journals/PIMB/067/3.html\"\u003eDeducing properties of trees from their Matula numbers\u003c/a\u003e, Publ. Inst. Math., 53 (67), 1993, 17-22.",
				"Wolfdieter Lang, \u003ca href=\"/A196050/a196050.pdf\"\u003eComplete prime function reduction for n = 2..100.\u003c/a\u003e",
				"D. W. Matula, \u003ca href=\"http://www.jstor.org/stable/2027327\"\u003eA natural rooted tree enumeration by prime factorization\u003c/a\u003e, SIAM Rev. 10 (1968) 273.",
				"\u003ca href=\"/index/Mat#matula\"\u003eIndex entries for sequences related to Matula-Goebel numbers\u003c/a\u003e"
			],
			"formula": [
				"a(1)=0; if n = p(t) (the t-th prime), then a(n)=1 + a(t); if n = rs (r,s\u003e=2), then a(n)=a(r)+a(s). The Maple program is based on this recursive formula.",
				"a(n) = A061775(n) - 1."
			],
			"example": [
				"a(7) = 3 because the rooted tree with Matula-Goebel number 7 is the rooted tree Y.",
				"a(2^m) = m because the rooted tree with Matula-Goebel number 2^m is the star tree with m edges."
			],
			"maple": [
				"with(numtheory): a := proc (n) local r, s: r := proc (n) options operator, arrow: op(1, factorset(n)) end proc: s := proc (n) options operator, arrow: n/r(n) end proc: if n = 1 then 0 elif bigomega(n) = 1 then 1+a(pi(n)) else a(r(n))+a(s(n)) end if end proc: seq(a(n), n = 1 .. 110);"
			],
			"mathematica": [
				"a[1] = 0; a[n_?PrimeQ] := a[n] = 1 + a[PrimePi[n]]; a[n_] := Total[#[[2]] * a[#[[1]] ]\u0026 /@ FactorInteger[n]];",
				"Array[a, 110] (* _Jean-François Alcover_, Nov 16 2017 *)",
				"difac[n_]:=If[n==1,{},With[{i=PrimePi[FactorInteger[n][[1,1]]]},Sort[Prepend[difac[n*i/Prime[i]],i]]]];",
				"Table[Length[difac[n]],{n,100}] (* _Gus Wiseman_, Mar 23 2019 *)"
			],
			"program": [
				"(Haskell)",
				"import Data.List (genericIndex)",
				"a196050 n = genericIndex a196050_list (n - 1)",
				"a196050_list = 0 : g 2 where",
				"   g x = y : g (x + 1) where",
				"     y = if t \u003e 0 then a196050 t + 1 else a196050 r + a196050 s",
				"         where t = a049084 x; r = a020639 x; s = x `div` r",
				"-- _Reinhard Zumkeller_, Sep 03 2013",
				"(PARI) a(n) = my(f=factor(n)); [self()(primepi(p))+1 |p\u003c-f[,1]]*f[,2]; \\\\ _Kevin Ryde_, May 28 2021"
			],
			"xref": [
				"One less than A061775.",
				"Cf. A000040, A000081, A000720, A003963, A007097, A020639, A049084, A109082, A109129, A317713.",
				"Cf. A324850, A324922, A324923, A324924, A324925, A324931, A324935."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Emeric Deutsch_, Sep 27 2011",
			"references": 56,
			"revision": 42,
			"time": "2021-05-28T17:18:11-04:00",
			"created": "2011-09-27T13:40:09-04:00"
		}
	]
}