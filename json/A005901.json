{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005901",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5901,
			"id": "M4834",
			"data": "1,12,42,92,162,252,362,492,642,812,1002,1212,1442,1692,1962,2252,2562,2892,3242,3612,4002,4412,4842,5292,5762,6252,6762,7292,7842,8412,9002,9612,10242,10892,11562,12252,12962,13692,14442,15212,16002",
			"name": "Number of points on surface of cuboctahedron (or icosahedron): a(0) = 1; for n \u003e 0, a(n) = 10n^2 + 2. Also coordination sequence for f.c.c. or A_3 or D_3 lattice.",
			"comment": [
				"Sequence found by reading the segment (1, 12) together with the line from 12, in the direction 12, 42, ..., in the square spiral whose vertices are the generalized heptagonal numbers A085787. - _Omar E. Pol_, Jul 18 2012"
			],
			"reference": [
				"H. S. M. Coxeter, \"Polyhedral numbers,\" in R. S. Cohen et al., editors, For Dirk Struik. Reidel, Dordrecht, 1974, pp. 25-35.",
				"Gmelin Handbook of Inorg. and Organomet. Chem., 8th Ed., 1994, TYPIX search code (225) cF4",
				"B. Grünbaum, Uniform tilings of 3-space, Geombinatorics, 4 (1994), 49-56. See tiling #1.",
				"R. W. Marks and R. B. Fuller, The Dymaxion World of Buckminster Fuller. Anchor, NY, 1973, p. 46.",
				"S. Rosen, Wizard of the Dome: R. Buckminster Fuller; Designer for the Future. Little, Brown, Boston, 1969, p. 109.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A005901/b005901.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"M. Baake and U. Grimm, \u003ca href=\"https://arxiv.org/abs/cond-mat/9706122\"\u003eCoordination sequences for root lattices and related graphs\u003c/a\u003e, arXiv:cond-mat/9706122, Zeit. f. Kristallographie, 212 (1997), 253-256",
				"R. Bacher, P. de la Harpe and B. Venkov, \u003ca href=\"https://doi.org/10.1016/S0764-4442(97)83542-2\"\u003eSéries de croissance et séries d'Ehrhart associées aux réseaux de racines\u003c/a\u003e, C. R. Acad. Sci. Paris, 325 (Series 1) (1997), 1137-1142.",
				"J. H. Conway and N. J. A. Sloane, Low-Dimensional Lattices VII: Coordination Sequences, Proc. Royal Soc. London, A453 (1997), 2369-2389 (\u003ca href=\"http://neilsloane.com/doc/Me220.pdf\"\u003epdf\u003c/a\u003e).",
				"R. W. Grosse-Kunstleve, \u003ca href=\"/A005897/a005897.html\"\u003eCoordination Sequences and Encyclopedia of Integer Sequences\u003c/a\u003e",
				"R. W. Grosse-Kunstleve, G. O. Brunner and N. J. A. Sloane, \u003ca href=\"http://neilsloane.com/doc/ac96cs/\"\u003eAlgebraic Description of Coordination Sequences and Exact Topological Densities for Zeolites\u003c/a\u003e, Acta Cryst., A52 (1996), pp. \u003ca href=\"http://dx.doi.org/10.1107/S0108767396007519\"\u003e879-889\u003c/a\u003e.",
				"G. Nebe and N. J. A. Sloane, \u003ca href=\"http://www.math.rwth-aachen.de/~Gabriele.Nebe/LATTICES/D3.html\"\u003eHome page for this lattice\u003c/a\u003e",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"Reticular Chemistry Structure Resource (RCSR), \u003ca href=\"http://rcsr.net/nets/fcu\"\u003eThe fcu tiling (or net)\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/A000292/a000292a.jpg\"\u003eA portion of the f.c.c. lattice packing.\u003c/a\u003e",
				"B. K. Teo and N. J. A. Sloane, \u003ca href=\"http://dx.doi.org/10.1021/ic00220a025\"\u003eMagic numbers in polygonal and polyhedral clusters\u003c/a\u003e, Inorgan. Chem. 24 (1985), 4545-4558.",
				"K. Urner, \u003ca href=\"http://www.grunch.net/synergetics/virus.html\"\u003eMicroarchitecture of the Virus\u003c/a\u003e",
				"R. Vaughan \u0026 N. J. A. Sloane, \u003ca href=\"/A005901/a005901.pdf\"\u003eCorrespondence, 1975\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Cuboctahedron\"\u003eCuboctahedron\u003c/a\u003e",
				"\u003ca href=\"/index/Fa#fcc\"\u003eIndex entries for sequences related to f.c.c. lattice\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3, -3, 1)."
			],
			"formula": [
				"G.f.: (1+x)(1+8*x+x^2)/(1-x)^3. - _Simon Plouffe_ in his 1992 dissertation",
				"G.f. for coordination sequence for A_n lattice is Sum(binomial(n, i)^2*z^i, i=0..n)/(1-z)^n. [Bacher et al.]",
				"a(n+1) = A027599(n+2) + A092277(n+1) - _Creighton Dement_, Feb 11 2005",
				"a(n) = 2 + A033583(n), n \u003e= 1. - _Omar E. Pol_, Jul 18 2012",
				"a(n) = 12 + 24*(n-1) + 8*A000217(n-2) + 6*A000290(n-1). The properties of the cuboctahedron, namely, its number of vertices (12), edges (24), and faces as well as face-type (8 triangles and 6 squares), are involved in this formula. - _Peter M. Chema_, Mar 26 2017",
				"a(n) = A062786(n)+A062786(n+1). - _R. J. Mathar_, Feb 28 2018"
			],
			"mathematica": [
				"Join[{1},10*Range[40]^2+2] (* or *) Join[{1},LinearRecurrence[{3,-3,1},{12,42,92},40]] (* _Harvey P. Dale_, May 28 2014 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c0,0,10*n^2+1+(n\u003e0))"
			],
			"xref": [
				"Cf. A004015, A206399.",
				"Partial sums give A005902.",
				"The 28 uniform 3D tilings: cab: A299266, A299267; crs: A299268, A299269; fcu: A005901, A005902; fee: A299259, A299265; flu-e: A299272, A299273; fst: A299258, A299264; hal: A299274, A299275; hcp: A007899, A007202; hex: A005897, A005898; kag: A299256, A299262; lta: A008137, A299276; pcu: A005899, A001845; pcu-i: A299277, A299278; reo: A299279, A299280; reo-e:  A299281, A299282; rho: A008137, A299276; sod: A005893, A005894; sve: A299255, A299261; svh: A299283, A299284; svj: A299254, A299260; svk: A010001, A063489; tca: A299285, A299286; tcd: A299287, A299288; tfs: A005899, A001845; tsi: A299289, A299290; ttw: A299257, A299263; ubt: A299291, A299292; bnn: A007899, A007202. See the Proserpio link in A299266 for overview."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, R. Vaughan",
			"references": 64,
			"revision": 102,
			"time": "2021-03-12T22:32:36-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}