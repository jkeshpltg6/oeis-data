{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A181821",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 181821,
			"data": "1,2,4,6,8,12,16,30,36,24,32,60,64,48,72,210,128,180,256,120,144,96,512,420,216,192,900,240,1024,360,2048,2310,288,384,432,1260,4096,768,576,840,8192,720,16384,480,1800,1536,32768,4620,1296,1080,1152,960,65536",
			"name": "a(n) = smallest integer with factorization as Product p(i)^e(i) such that Product p(e(i)) = n.",
			"comment": [
				"A permutation of A025487. a(n) is the member m of A025487 such that A181819(m) = n. a(n) is also the member of A025487 whose prime signature is conjugate to the prime signature of A108951(n).",
				"If n = Product_i prime(e(i)) with the e(i) weakly decreasing, then a(n) = Product_i prime(i)^e(i). For example, 90 = prime(3) * prime(2) * prime(2) * prime(1), so a(90) = prime(1)^3 * prime(2)^2 * prime(3)^2 * prime(4)^1 = 12600. - _Gus Wiseman_, Jan 02 2019"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A181821/b181821.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ConjugatePartition.html\"\u003eConjugate Partition \u003c/a\u003e"
			],
			"formula": [
				"If A108951(n) = Product p(i)^e(i), then a(n) = Product A002110(e(i)). I.e., a(n) = A108951(A181819(A108951(n))).",
				"a(A181819(n)) = A046523(n)). - _Antti Karttunen_, Dec 10 2018"
			],
			"example": [
				"The canonical factorization of 24 is 2^3*3^1.  Therefore, p(e(i)) = prime(3)*prime(1)(i.e., A000040(3)*A000040(1)), which equals 5*2 = 10.  Since 24 is the smallest integer for which p(e(i)) = 10, a(10) = 24."
			],
			"maple": [
				"a:= n-\u003e (l-\u003e mul(ithprime(i)^l[i], i=1..nops(l)))(sort(map(i-\u003e",
				"             numtheory[pi](i[1])$i[2], ifactors(n)[2]), `\u003e`)):",
				"seq(a(n), n=1..70);  # _Alois P. Heinz_, Sep 05 2018"
			],
			"mathematica": [
				"With[{s = Array[If[# == 1, 1, Times @@ Map[Prime@ Last@ # \u0026, FactorInteger@ #]] \u0026, 2^16]}, Array[First@ FirstPosition[s, #] \u0026, LengthWhile[Differences@ Union@ s, # == 1 \u0026]]] (* _Michael De Vlieger_, Dec 17 2018 *)",
				"Table[Times@@MapIndexed[Prime[#2[[1]]]^#1\u0026,Reverse[Flatten[Cases[FactorInteger[n],{p_,k_}:\u003eTable[PrimePi[p],{k}]]]]],{n,30}] (* _Gus Wiseman_, Jan 02 2019 *)"
			],
			"program": [
				"(PARI) A181821(n) = { my(f=factor(n),p=0,m=1); forstep(i=#f~,1,-1,while(f[i,2], f[i,2]--; m *= (p=nextprime(p+1))^primepi(f[i,1]))); (m); }; \\\\ _Antti Karttunen_, Dec 10 2018"
			],
			"xref": [
				"Other rearrangements of A025487 include A036035, A059901, A063008, A077569, A085988, A086141, A087443, A108951, A181822.",
				"Cf. A046523, A181819, A181820.",
				"Cf. A001221, A001222, A056239, A071625, A112798, A118914, A182850, A305936."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Matthew Vandermast_, Dec 07 2010",
			"ext": [
				"Definition corrected by _Gus Wiseman_, Jan 02 2019"
			],
			"references": 87,
			"revision": 16,
			"time": "2019-01-02T21:46:06-05:00",
			"created": "2010-11-13T14:07:27-05:00"
		}
	]
}