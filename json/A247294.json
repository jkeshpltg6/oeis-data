{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A247294",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 247294,
			"data": "1,1,2,4,7,1,14,3,30,7,64,18,141,43,1,316,102,5,713,249,16,1626,608,49,3740,1489,143,1,8659,3669,400,7,20176,9058,1109,29,47274,22407,3046,105,111302,55560,8282,357,1,263201,138004,22385,1149,9",
			"name": "Triangle read by rows: T(n,k) is the number of weighted lattice paths B(n) having a total of k uhd and uHd strings.",
			"comment": [
				"B(n) is the set of lattice paths of weight n that start in (0,0), end on the horizontal axis and never go below this axis, whose steps are of the following four kinds: h = (1,0) of weight 1, H = (1,0) of weight 2, u = (1,1) of weight 2, and d = (1,-1) of weight 1. The weight of a path is the sum of the weights of its steps.",
				"Row n contains 1 + floor(n/4) entries.",
				"Sum of entries in row n is A004148(n+1) (the 2ndary structure numbers).",
				"T(n,0) = A247295(n).",
				"Sum(k*T(n,k), k=0..n) = A247296(n)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A247294/b247294.txt\"\u003eRows n = 0..300, flattened\u003c/a\u003e",
				"M. Bona and A. Knopfmacher, \u003ca href=\"http://dx.doi.org/10.1007/s00026-010-0060-7\"\u003eOn the probability that certain compositions have the same number of parts\u003c/a\u003e, Ann. Comb., 14 (2010), 291-306."
			],
			"formula": [
				"G.f. G = G(t,z) satisfies G = 1 + z*G + z^2*G + z^3*G*(G - z - z^2 + t*z + t*z^2)."
			],
			"example": [
				"T(6,1)=7 because we have uhdhh, huhdh, hhuhd, Huhd, uhdH, uHdh, and huHd.",
				"Triangle starts:",
				"1;",
				"1;",
				"2;",
				"4;",
				"7,1;",
				"14,3;",
				"30,7;"
			],
			"maple": [
				"eq := G = 1+z*G+z^2*G+z^3*(G-z-z^2+t*z+t*z^2)*G: G := RootOf(eq, G): Gser := simplify(series(G, z = 0, 25)): for n from 0 to 22 do P[n] := sort(coeff(Gser, z, n)) end do: for n from 0 to 22 do seq(coeff(P[n], t, k), k = 0 .. floor((1/4)*n)) end do; # yields sequence in triangular form",
				"# second Maple program:",
				"b:= proc(n, y, t) option remember; `if`(y\u003c0 or y\u003en, 0, `if`(n=0, 1,",
				"      expand(b(n-1, y-1, 0)*`if`(t=2, x, 1)+b(n-1, y, `if`(t=1, 2, 0))",
				"      +`if`(n\u003e1, b(n-2, y, `if`(t=1, 2, 0))+b(n-2, y+1, 1), 0))))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=0..degree(p)))(b(n, 0$2)):",
				"seq(T(n), n=0..20);  # _Alois P. Heinz_, Sep 16 2014"
			],
			"mathematica": [
				"b[n_, y_, t_] := b[n, y, t] = If[y\u003c0 || y\u003en, 0, If[n == 0, 1, Expand[b[n-1, y-1, 0]*If[t == 2, x, 1] + b[n-1, y, If[t == 1, 2, 0]] + If[n\u003e1, b[n-2, y, If[t == 1, 2, 0]] + b[n-2, y+1, 1], 0]]]]; T[n_] := Function[{p}, Table[Coefficient[p, x, i], {i, 0, Exponent[p, x]}]][b[n, 0, 0]]; Table[T[n], {n, 0, 20}] // Flatten (* _Jean-François Alcover_, May 27 2015, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A004148, A247290, A247292, A247295, A247296."
			],
			"keyword": "nonn,tabf",
			"offset": "0,3",
			"author": "_Emeric Deutsch_, Sep 16 2014",
			"references": 5,
			"revision": 11,
			"time": "2015-05-27T05:17:42-04:00",
			"created": "2014-09-16T20:23:40-04:00"
		}
	]
}