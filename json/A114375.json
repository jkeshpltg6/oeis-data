{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A114375",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 114375,
			"data": "0,0,1,2,4,7,4,4,1,6,8,15,8,8,1,10,12,7,12,12,1,14,16,31,16,16,1,18,20,7,20,20,1,22,24,15,24,24,1,26,28,7,28,28,1,30,32,63,32,32,1,34,36,7,36,36,1,38,40,15,40,40,1,42,44,7,44,44,1,46,48,31,48,48,1,50,52,7,52,52,1",
			"name": "a(n) = (a(n-1) XOR a(n-2)) + 1, a(0) = a(1) = 0.",
			"comment": [
				"The function moving to the next overlapping pair in the sequence is f:(i,j) = (j, (i XOR j) + 1) is one-to one. This means that the only possible trajectories for the sequence are loops, \"lines\", and \"rays\". The inverse is f^{-1}: (i,j) = (i XOR (j-1), i) is defined except when j = 0. Thus the only infinite non-repeating trajectories are those starting with (i,0) for some i. If we define the size of a pair (i,j) to be the largest power of two \u003c= max(i,j). It is trivial to see that the size of f(i,j) is always \u003e= the size of (i,j). Coupled with the fact there are only finitely many pairs with a given size, this means that \"line\" trajectories are not possible. Any trajectory that goes to a larger size must be part of a ray, so that tracing back will eventually reach zero. - _Franklin T. Adams-Watters_, Mar 03 2014"
			],
			"link": [
				"Harvey P. Dale, \u003ca href=\"/A114375/b114375.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e"
			],
			"formula": [
				"a(3n)=2n. a(3n+1)=4*floor((n+1)/2). a(6n+2)=1. a(6n+5)=2^(A001511(n+1)+2)-1.",
				"a(3*n + 1) = A168273(n+1). a(3*n - 1) = A074723(n) - 1.- _Michael Somos_, Mar 03 2014",
				"a(-n) = -a(n) if n == 0 (mod 3), a(-1-n) = -a(n) if n == 1 (mod 3), a(-2-n) = a(n) if n == 2 (mod 3). - _Michael Somos_, Mar 03 2014"
			],
			"example": [
				"G.f. = x^2 + 2*x^3 + 4*x^4 + 7*x^5 + 4*x^6 + 4*x^7 + x^8 + 6*x^9 + 8*x^10 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 0, BitXor[ a[n + 1], a[n + 2] - 1], If[n \u003c 2, 0, 1 + BitXor[ a[n - 1], a[n - 2]]]]; (* _Michael Somos_, Mar 03 2014 *)",
				"a[ n_] := If[ Mod[n, 3] == 0, 2 n/3, If[ Mod[n, 3] == 1, 4 Quotient[n + 3, 6], If[ n == -1, -1, 2^IntegerExponent[ Fibonacci[n + 1], 2] - 1]]]; (* _Michael Somos_, Mar 03 2014 *)",
				"nxt[{a_,b_}]:={b,BitXor[a,b]+1}; NestList[nxt,{0,0},80][[All,1]] (* _Harvey P. Dale_, Feb 26 2020 *)"
			],
			"xref": [
				"Cf. A003987, A001511, A074723, A168273."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_Franklin T. Adams-Watters_, Feb 09 2006",
			"references": 6,
			"revision": 23,
			"time": "2020-02-26T16:22:03-05:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}