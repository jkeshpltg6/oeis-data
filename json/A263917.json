{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A263917",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 263917,
			"data": "1,3,1,15,4,1,85,22,5,1,519,132,30,6,1,3330,837,190,39,7,1,22135,5516,1250,260,49,8,1,151089,37404,8461,1773,343,60,9,1,1052805,259280,58550,12324,2422,440,72,10,1,7458236,1829018,412375,87045,17283,3214,552,85,11,1",
			"name": "Riordan array (f(x)^3, f(x)), where 1 + x*f^3(x)/(1 - x*f(x)) = f(x).",
			"comment": [
				"Riordan arrays of the form (f(x)^(m+1), f(x)), where f(x) satisfies 1 + x*f^(m+1)(x)/(1 - x*f(x)) = f(x) include (modulo differences of offset) the Motzkin triangle A091836 (m = -1), the Catalan triangle A033184 (m = 0) and the Schroder triangle A091370 (m = 1). This is the case m = 2. See A263918 for the case m = 3.",
				"The coefficients of the power series solution of the equation 1 + x*f^(m+1)(x)/(1 - x*f(x)) = f(x) appear to be given by [x^0] f(x) = 1 and [x^n] f(x) = 1/n * Sum_{k = 1..n} binomial(n,k)*binomial(n + m*k, k - 1) for n \u003e= 1.",
				"This triangle appears in Novelli et al., Figure 8, p. 24, where a combinatorial interpretation is given in terms of trees."
			],
			"link": [
				"J.-C. Novelli and J.-Y. Thibon, \u003ca href=\"https://arxiv.org/abs/math/0512570\"\u003eNoncommutative Symmetric Functions and Lagrange Inversion\u003c/a\u003e, arXiv:math/0512570 [math.CO], 2005-2006."
			],
			"formula": [
				"O.g.f. f^3(x)/(1 - x*t*f(x)), where f(x)  = 1 + x + 4*x^2 + 20*x^3 + 113*x^4 + ... satisfies 1 + x*f^3(x)/(1 - x*f(x)) = f(x);",
				"f(x) is the o.g.f. for A108447.",
				"First column o.g.f f(x)^3 is the o.g.f. for A118342.",
				"f(x) - 1 is the g.f. for the row sums of the array."
			],
			"example": [
				"Triangle begins:",
				"       1",
				"       3     1",
				"      15     4     1",
				"      85    22     5    1",
				"     519   132    30    6   1",
				"    3330   837   190   39   7  1",
				"   22135  5516  1250  260  49  8 1",
				"  151089 37404  8461 1773 343 60 9 1"
			],
			"maple": [
				"# For the function TreesByArityOfTheRoot_Row(m, n) see A263918.",
				"A263917_row := n -\u003e TreesByArityOfTheRoot_Row(2,n):",
				"seq(A263917_row(n), n=0..9); # _Peter Luschny_, Oct 31 2015"
			],
			"mathematica": [
				"rows = 9;",
				"f[_] = 1; Do[f[x_] = 1 + x*f[x]*(f[x]^2 + f[x] - 1) + O[x]^(rows+1) // Normal, {rows+1}];",
				"coes = CoefficientList[f[x]^3/(1 - x*t*f[x]) + O[x]^(rows+1), x];",
				"row[n_] := CoefficientList[coes[[n+1]], t];",
				"Table[row[n], {n, 0, rows}] // Flatten (* _Jean-François Alcover_, Jul 19 2018 *)"
			],
			"xref": [
				"Cf. A108447 (row sums), A118342 (column 0).",
				"Cf. A033184, A091370, A091836, A263918."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "0,2",
			"author": "_Peter Bala_, Oct 29 2015",
			"references": 1,
			"revision": 15,
			"time": "2018-07-19T07:59:07-04:00",
			"created": "2015-11-04T08:17:46-05:00"
		}
	]
}