{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A334230",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 334230,
			"data": "1,1,2,1,2,3,1,2,2,4,1,2,2,4,5,1,2,3,4,4,6,1,2,3,4,4,6,7,1,2,2,4,4,4,4,8,1,2,3,4,4,6,6,4,9,1,2,2,4,5,4,4,8,4,10,1,2,2,4,5,4,4,8,4,10,11,1,2,3,4,4,6,6,8,6,8,8,12,1,2,3,4,4,6,6,8",
			"name": "Triangle read by rows: T(n,k) gives the meet of n and k in the graded lattice of the positive integers defined by covering relations \"n covers (n - n/p)\" for all divisors p of n.",
			"comment": [
				"Any row with prime index p is a copy of row p-1 followed by that prime p."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A334230/b334230.txt\"\u003eTable of n, a(n) for n = 1..10440; The first 144 rows, flattened\u003c/a\u003e",
				"Math Stack Exchange, \u003ca href=\"https://math.stackexchange.com/a/3640072/121988\"\u003eDoes a graded poset on the positive integers generated from subtracting factors define a lattice?\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Semilattice\"\u003eSemilattice\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = m*T(n/m, k/m) for m = gcd(n, k)."
			],
			"example": [
				"The interval [1,15] illustrates that, for example, T(12, 10) = 8, T(12, 4) = T(5, 6) = 4, T(8, 3) = 2, etc.",
				"      15",
				"     _/ \\_",
				"    /     \\",
				"  10       12",
				"  | \\_   _/ |",
				"  |   \\ /   |",
				"  5    8    6",
				"   \\_  |  _/|",
				"     \\_|_/  |",
				"       4    3",
				"       |  _/",
				"       |_/",
				"       2",
				"       |",
				"       |",
				"       1",
				"Triangle begins:",
				"  n\\k| 1 2 3 4 5 6 7 8 9 10 11 12 13 14",
				"  ---+---------------------------------",
				"   1 | 1",
				"   2 | 1 2",
				"   3 | 1 2 3",
				"   4 | 1 2 2 4",
				"   5 | 1 2 2 4 5",
				"   6 | 1 2 3 4 4 6",
				"   7 | 1 2 3 4 4 6 7",
				"   8 | 1 2 2 4 4 4 4 8",
				"   9 | 1 2 3 4 4 6 6 4 9",
				"  10 | 1 2 2 4 5 4 4 8 4 10",
				"  11 | 1 2 2 4 5 4 4 8 4 10 11",
				"  12 | 1 2 3 4 4 6 6 8 6  8  8 12",
				"  13 | 1 2 3 4 4 6 6 8 6  8  8 12 13",
				"  14 | 1 2 3 4 4 6 7 8 6  8  8 12 12 14"
			],
			"program": [
				"(PARI)",
				"\\\\ This just returns the largest (in a normal sense) number x from the intersection of the set of descendants of n and k:",
				"up_to = 105;",
				"buildWdescsets(up_to) = { my(v=vector(up_to)); v[1] = Set([1]); for(n=2,up_to, my(f=factor(n)[, 1]~, s=Set([n])); for(i=1,#f,s = setunion(s,v[n-(n/f[i])])); v[n] = s); (v); }",
				"vdescsets = buildWdescsets(up_to);",
				"A334230tr(n,k) = vecmax(setintersect(vdescsets[n],vdescsets[k]));",
				"A334230list(up_to) = { my(v = vector(up_to), i=0); for(n=1,oo, for(k=1,n, i++; if(i \u003e up_to, return(v)); v[i] = A334230tr(n,k))); (v); };",
				"v334230 = A334230list(up_to);",
				"A334230(n) = v334230[n]; \\\\ _Antti Karttunen_, Apr 19 2020"
			],
			"xref": [
				"Cf. A332809, A333123, A334184, A334231."
			],
			"keyword": "nonn,tabl,look",
			"offset": "1,3",
			"author": "_Peter Kagey_, _Antti Karttunen_, and _Michael De Vlieger_, Apr 19 2020",
			"references": 4,
			"revision": 30,
			"time": "2020-04-29T18:49:16-04:00",
			"created": "2020-04-28T00:44:51-04:00"
		}
	]
}