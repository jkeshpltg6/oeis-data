{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A004185",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 4185,
			"data": "0,1,2,3,4,5,6,7,8,9,1,11,12,13,14,15,16,17,18,19,2,12,22,23,24,25,26,27,28,29,3,13,23,33,34,35,36,37,38,39,4,14,24,34,44,45,46,47,48,49,5,15,25,35,45,55,56,57,58,59,6,16,26,36,46,56,66,67,68,69,7,17,27,37,47",
			"name": "Arrange digits of n in increasing order, then (for n\u003e0) omit the zeros.",
			"comment": [
				"Record values: A009994. - _Reinhard Zumkeller_, Dec 05 2009",
				"If we define \"sortable primes\" as prime numbers that remain prime when their digits are sorted in increasing order, then all absolute primes (A003459) are sortable primes but not all sortable primes are absolute primes. For example, 311 is both sortable and absolute, and 271 is sortable but not absolute, since its digits can be permuted to 217 = 7 * 31 or 712 = 2^3 * 89, etc. - _Alonso del Arte_, Oct 05 2013",
				"The above mentioned \"sortable primes\" are listed in A211654, the nontrivial ones (with digits not in nondecreasing order) in A086042. - _M. F. Hasler_, Jul 30 2019"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A004185/b004185.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e"
			],
			"example": [
				"a(19) = 19 because the digits are already in increasing order.",
				"a(20) = 2 because the digits of 20 are 2 and 0, which in increasing order are 0 and 2, but since zero-padding is not allowed on the left, the zero digit is dropped and we are left with 2.",
				"a(21) = 12 because the digits of 21 are 2 and 1, which in increasing order are 1 and 2."
			],
			"maple": [
				"A004185 := proc(n)",
				"    local dgs;",
				"    convert(n,base,10) ;",
				"    dgs := sort(%,`\u003e`) ;",
				"    add( op(i,dgs)*10^(i-1),i=1..nops(dgs)) ;",
				"end proc:",
				"seq(A004185(n),n=0..20) ; # _R. J. Mathar_, Jul 26 2015"
			],
			"mathematica": [
				"FromDigits[Sort[DeleteCases[IntegerDigits[#], 0]]]\u0026/@Range[0, 60] (* _Harvey P. Dale_, Nov 29 2011 *)"
			],
			"program": [
				"(Haskell)",
				"import Data.List (sort)",
				"a004185 n = read $ sort $ show n :: Integer",
				"-- _Reinhard Zumkeller_, Aug 10 2011",
				"(MAGMA) A004185:=func\u003cn | Seqint(Reverse(Sort(Intseq(n))))\u003e; [n eq 0 select 0 else A004185(n): n in [0..57]]; // _Bruno Berselli_, Apr 03 2012",
				"(Python)",
				"def A004185(n):",
				"    return int(''.join(sorted(str(n))).replace('0','')) if n \u003e 0 else 0 # _Chai Wah Wu_, Nov 10 2015",
				"(PARI) a(n)=fromdigits(vecsort(digits(n))) \\\\ _Charles R Greathouse IV_, Feb 06 2017"
			],
			"xref": [
				"Cf. A004719, A004151, A004086, A004186, A009996, A221714, A073138, A193581, A193582, A065641.",
				"Cf. A211654 (sortable primes) and subsequence A086042 (nontrivial solutions)."
			],
			"keyword": "nonn,base,nice,easy,look",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_.",
			"references": 40,
			"revision": 56,
			"time": "2021-06-28T11:47:44-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}