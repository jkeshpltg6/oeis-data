{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A191537",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 191537,
			"data": "1,3,2,8,6,4,21,16,11,5,55,42,29,13,7,143,109,75,34,19,9,370,282,194,88,50,24,10,957,730,502,228,130,63,26,12,2475,1888,1299,590,337,163,68,32,14,6400,4882,3359,1526,872,422,176,83,37,15,16550,12624",
			"name": "Dispersion of (4n-floor(n*sqrt(2))), by antidiagonals.",
			"comment": [
				"Background discussion: Suppose that s is an increasing sequence of positive integers, that the complement t of s is infinite, and that t(1)=1. The dispersion of s is the array D whose n-th row is (t(n), s(t(n)), s(s(t(n)), s(s(s(t(n)))), ...). Every positive integer occurs exactly once in D, so that, as a sequence, D is a permutation of the positive integers. The sequence u given by u(n)=(number of the row of D that contains n) is a fractal sequence. Examples:",
				"(1) s=A000040 (the primes), D=A114537, u=A114538.",
				"(2) s=A022343 (without initial 0), D=A035513 (Wythoff array), u=A003603.",
				"(3) s=A007067, D=A035506 (Stolarsky array), u=A133299.",
				"More recent examples of dispersions: A191426-A191455 and A191536-A191545."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A191537/b191537.txt\"\u003eTable of n, a(n) for the first 50 rows, flattened\u003c/a\u003e"
			],
			"example": [
				"Northwest corner:",
				"  1,  3,  8,  21,  55, ...",
				"  2,  6, 16,  42, 109, ...",
				"  4, 11, 29,  75, 194, ...",
				"  5, 13, 34,  88, 228, ...",
				"  7, 19, 50, 130, 337, ..."
			],
			"mathematica": [
				"(* Program generates the dispersion array T of the increasing sequence f[n] *)",
				"r=40; r1=12; c=40; c1=12; f[n_] :=4n-Floor[n*Sqrt[2]]   (* complement of column 1 *)",
				"mex[list_] := NestWhile[#1 + 1 \u0026, 1, Union[list][[#1]] \u003c= #1 \u0026, 1, Length[Union[list]]]",
				"rows = {NestList[f, 1, c]};",
				"Do[rows = Append[rows, NestList[f, mex[Flatten[rows]], r]], {r}];",
				"t[i_, j_] := rows[[i, j]];",
				"TableForm[Table[t[i, j], {i, 1, r1}, {j, 1, c1}]]  (* A191537 array *)",
				"Flatten[Table[t[k, n - k + 1], {n, 1, c1}, {k, 1, n}]] (* A191537 sequence *)",
				"(* _Clark Kimberling_, Jun 06 2011 *)"
			],
			"xref": [
				"Cf. A114537, A035513, A035506."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Jun 06 2011",
			"references": 1,
			"revision": 17,
			"time": "2017-12-09T20:45:07-05:00",
			"created": "2011-06-07T12:04:34-04:00"
		}
	]
}