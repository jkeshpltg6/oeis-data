{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A180442",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 180442,
			"data": "1,3,7,9,11,13,15,17,18,20,21,22,25,27,28,30,32,38,44,50,52,55,58,60,64,65,67,73,74,76,83,87,91,103,104,106,112,115,117,119,121,124,128,129,131,132,137,140,142,146,158,168,170,172,175,178,181,183,192,193,197,199,200,204",
			"name": "Numbers n such that a sum of two or more consecutive squares beginning with n^2 is a square.",
			"comment": [
				"That is, numbers n such that Sum_{i=n..k} i^2 is a square for some k \u003e n.",
				"The paper by Bremner, Stroeker, and Tzanakis describes how they found all n \u003c= 100 by solving elliptic curves. Their solutions are the same as the terms in this sequence. They also show that there are only a finite number of sums of squares beginning with n^2 that sum to a square. For example, starting with 3^2, there are only 3 ways to sum consecutive squares to produce a square: 3^2 + 4^2, 3^2 + ... + 580^2, and 3^2 + ... + 963^2. See A184762, A184763, A184885, and A184886 for more results from their paper.",
				"This sequence is more difficult than A001032, which has the possible lengths of the sequences of consecutive squares that sum to a square. Be careful adding terms to this sequence; a simple search may miss some terms. An elliptic curve needs to be solved for each number.",
				"It is conjectured that the sequence continues 103, 104, 106, 112, 115, 117, 119, 121, 124, 128, 129, 131, 132, 137, 140, 142, 146, 158, 168, 170, 172, 175, 178, 181, 183, 192, 193, 197, 199, 200. - _Jean-François Alcover_, Sep 17 2013. Conjecture confirmed (see the Schoenfield link below). - _Jon E. Schoenfield_, Nov 22 2013"
			],
			"link": [
				"Jon E. Schoenfield, \u003ca href=\"/A180442/b180442.txt\"\u003eTable of n, a(n) for n = 1..123 (includes a derivation of the elliptic curves and Magma code used to find the terms)\u003c/a\u003e",
				"A. Bremner, R. J. Stroeker, and N. Tzanakis, \u003ca href=\"http://www.stroeker.nl/JNT97.pdf\"\u003eOn Sums of Consecutive Squares\u003c/a\u003e, J. Number Theory 62 (1997), 39-70.",
				"K. S. Brown, \u003ca href=\"http://www.mathpages.com/home/kmath147.htm\"\u003eSum of Consecutive Nth Powers Equals an Nth Power\u003c/a\u003e",
				"Masoto Kuwata, Jaap Top, \u003ca href=\"http://www.math.rug.nl/~top/toku2.pdf\"\u003eAn elliptic surface related to sums of consecutive squares\u003c/a\u003e, Exposition. Math. 12 (1994) 181-192"
			],
			"formula": [
				"Numbers n such that A075404(n) \u003e 0."
			],
			"example": [
				"30 is in the sequence because 30^2 + 31^2 + 32^2 + ... + 197^2 + 198^2 = 1612^2."
			],
			"mathematica": [
				"jmax[11] = jmax[74] = 10^5; jmax[n_ /; n \u003e 91] = 10^6; jmax[_] = 10^4; Reap[For[n = 1, n \u003c= 200, n++, s = n^2; For[j = n+1, j \u003c= jmax[n], j++, s += j^2; If[IntegerQ[Sqrt[s]], Sow[n]; Print[n, \"(\", j, \", \", Sqrt[s], \")\"]; Break[]]]]][[2, 1]] (* _Jean-François Alcover_, Sep 17 2013, translated and adapted from Pari *)"
			],
			"program": [
				"(PARI)for(n=1, 100,s=n^2;for(j=n+1,999999,s+=j^2; if(issquare(s), print1(n, \"(\",j,\",\",sqrtint(s),\")\");break())))"
			],
			"xref": [
				"Cf. A180259, A180273, A180274, A180465.",
				"Cf. A075404, A075405, A075406."
			],
			"keyword": "nonn,nice",
			"offset": "1,2",
			"author": "_Zhining Yang_, Jan 19 2011",
			"ext": [
				"Example simplified by _Jon E. Schoenfield_, Sep 18 2013",
				"More terms from _Jon E. Schoenfield_, Nov 22 2013"
			],
			"references": 10,
			"revision": 47,
			"time": "2019-07-06T15:31:44-04:00",
			"created": "2010-11-12T14:29:46-05:00"
		}
	]
}