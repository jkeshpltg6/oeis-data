{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A286011",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 286011,
			"data": "1,0,1,2,0,1,3,4,0,0,0,2,1,2,5,0,0,1,0,1,0,0,0,6,0,0,0,3,0,1,1,2,0,0,0,1,0,1,2,1,0,2,0,1,0,0,0,1,0,0,0,0,0,1,0,4,1,0,0,7,0,1,3,0,0,0,0,1,0,0,0,2,0,1,0,0,0,1,0,2,0,0,0,2,0,0,0,0,0,2",
			"name": "a(1)=1, and for n\u003e1, a(n) is the maximum number of iterations of sigma resulting in n, starting at some integer k; or 0 if n cannot be reached from any k.",
			"comment": [
				"a(n)=0 for n in A007369 and a(n)\u003e0 for n in A002191.",
				"Records are found at indices given by A007497.",
				"The above would be correct for a(1) = 0 (in a weak sense) or rather a(1) = -1 (for infinity), but as the sequence is defined, 2 \u0026 3 do not produce a record, so the indices of records are 1, (3), 4, 7, ... = {1} U A007497 \\ {2, (3)}. - _M. F. Hasler_, Nov 20 2019"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A286011/b286011.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"M. Alekseyev, \u003ca href=\"https://home.gwu.edu/~maxal/gpscripts/invphi.gp\"\u003ePARI/GP Scripts for Miscellaneous Math Problems: invsigma.gp\u003c/a\u003e, Oct. 2005"
			],
			"example": [
				"a(4)=2 because 4=sigma(3), but also sigma(sigma(2)) with 2 iterations.",
				"a(7)=3 because 7=sigma(4), but also sigma(sigma(3)), and sigma(sigma(sigma(2))), with 3 iterations."
			],
			"maple": [
				"N:= 100: # to get a(1)..a(N)",
				"V:= Vector(N):",
				"for n from 1 to N do",
				"  s:= numtheory:-sigma(n);",
				"  if s \u003c= N then V[s]:= max(V[s],V[n]+1) fi",
				"od:",
				"convert(V,list); # _Robert Israel_, May 01 2017"
			],
			"program": [
				"(PARI) a(n) = {if (n==1, return(1)); vn = vector(n-1, k, k+1); nb = 0; knb = 0; ok = 1; while(ok, nb++; vn = vector(#vn, k, sigma(vn[k])); svn = Set(vn); if (#select(x-\u003ex==n, svn), knb = nb); if (!#select(x-\u003ex\u003c=n, svn), ok = 0);); knb;}",
				"(PARI) apply( A286011(n)=if(n\u003c3,2-n, n=invsigma(n), vecmax(apply(self,n))+1), [1..99]) \\\\ See Alekseyev-link for invsigma(). - _M. F. Hasler_, Nov 20 2019"
			],
			"xref": [
				"Cf. A000203, A002191, A007369, A007497, A257670."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Michel Marcus_, Apr 30 2017",
			"references": 1,
			"revision": 24,
			"time": "2019-12-01T06:01:42-05:00",
			"created": "2017-04-30T11:28:25-04:00"
		}
	]
}