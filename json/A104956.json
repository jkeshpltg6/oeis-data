{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A104956",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 104956,
			"data": "2,5,9,8,0,7,6,2,1,1,3,5,3,3,1,5,9,4,0,2,9,1,1,6,9,5,1,2,2,5,8,8,0,8,5,5,0,4,1,4,2,0,7,8,8,0,7,1,5,5,7,0,9,4,2,0,8,3,7,1,0,4,6,9,1,7,7,8,9,9,5,2,5,3,6,3,2,0,0,0,5,5,6,2,1,7,1,9,2,8,0,1,3,5,8,7,2,8,6,3,5,1,3,4,3",
			"name": "Decimal expansion of the area of the regular hexagon with circumradius 1.",
			"comment": [
				"Equivalently, the area in the complex plane of the smallest convex set containing all order-6 roots of unity.",
				"Subtracting 2.5 (i.e., dropping the first two digits) we obtain 0.09807.... which is a limiting mean cluster density for a bond percolation model at probability 1/2 [Finch]. - _R. J. Mathar_, Jul 26 2007",
				"This constant is also the minimum radius of curvature of the exponential curve (occurring at x = -log(2)/2 = -0.34657359...). - _Jean-François Alcover_, Dec 19 2016"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A104956/b104956.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"S. R. Finch, \u003ca href=\"http://dx.doi.org/10.1007/BF01608791\"\u003eSeveral Constants Arising in Statistical Mechanics\u003c/a\u003e, Annals Combinat. vol 3 (1999) issue (2-4) pp. 323-335.",
				"Michael Penn, \u003ca href=\"https://www.youtube.com/watch?v=UNMYya7yYGg\"\u003enot as bad as it seems...\u003c/a\u003e, YouTube video, 2021.",
				"Eric Weisstein et al., \u003ca href=\"http://mathworld.wolfram.com/RootofUnity.html\"\u003eRoot of Unity\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/deMoivreNumber.html\"\u003ede Moivre Number\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Twenty-VertexEntropyConstant.html\"\u003eTwenty-Vertex Entropy Constant\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Hexagon\"\u003eHexagon\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Regular_polygon\"\u003eRegular polygon\u003c/a\u003e"
			],
			"formula": [
				"Equals (3*sqrt(3))/2, that is, 2*A104954."
			],
			"example": [
				"2.59807621135331594029116951225880855041420788071557094208371046917789952536320..."
			],
			"mathematica": [
				"Floor[n/2]*Sin[(2*Pi)/n] - Sin[(4*Pi*Floor[n/2])/n]/2 /. n -\u003e 6",
				"RealDigits[(3*Sqrt[3])/2, 10, 50][[1]] (* _G. C. Greubel_, Jul 03 2017 *)"
			],
			"program": [
				"(PARI) 3*sqrt(3)/2 \\\\ _G. C. Greubel_, Jul 03 2017"
			],
			"xref": [
				"Cf. A002194, A104954, A104955, A104957.",
				"Cf. Areas of other regular polygons: A120011, A102771, A178817, A090488, A256853, A178816, A256854, A178809."
			],
			"keyword": "nonn,cons",
			"offset": "1,1",
			"author": "Joseph Biberstine (jrbibers(AT)indiana.edu), Mar 30 2005",
			"references": 20,
			"revision": 35,
			"time": "2021-05-19T16:01:12-04:00",
			"created": "2005-04-09T03:00:00-04:00"
		}
	]
}