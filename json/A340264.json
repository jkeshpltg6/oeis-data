{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A340264",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 340264,
			"data": "1,0,2,0,1,4,0,1,6,8,0,1,11,24,16,0,1,20,70,80,32,0,1,37,195,340,240,64,0,1,70,539,1330,1400,672,128,0,1,135,1498,5033,7280,5152,1792,256,0,1,264,4204,18816,35826,34272,17472,4608,512",
			"name": "T(n, k) = Sum_{j=0..k} binomial(n, k - j)*Stirling2(n - k + j, j). Triangle read by rows, 0 \u003c= k \u003c= n.",
			"comment": [
				"A006905(n) = Sum_{k=0..n} A001035(k) * T(n, k). - _Michael Somos_, Jul 18 2021"
			],
			"link": [
				"Eric W. Weisstein, \u003ca href=\"https://mathworld.wolfram.com/BellPolynomial.html\"\u003eBell Polynomial\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = (-1)^n * n! * [t^k] [x^n] exp(t*(exp(-x) - x - 1)).",
				"n-th row polynomial R(n,x) = exp(-x)*Sum_{k \u003e= 0} (x + k)^n * x^k/k! = Sum_{k = 0..n} binomial(n,k)*Bell(k,x)*x^(n-k), where Bell(n,x) denotes the n-th Bell polynomial. - _Peter Bala_, Jan 13 2022"
			],
			"example": [
				"                            [0] 1;",
				"                           [1] 0, 2;",
				"                          [2] 0, 1, 4;",
				"                        [3] 0, 1, 6, 8;",
				"                     [4] 0, 1, 11, 24, 16;",
				"                   [5] 0, 1, 20, 70, 80, 32;",
				"                [6] 0, 1, 37, 195, 340, 240, 64;",
				"            [7] 0, 1, 70, 539, 1330, 1400, 672, 128;",
				"       [8] 0, 1, 135, 1498, 5033, 7280, 5152, 1792, 256;",
				"  [9] 0, 1, 264, 4204, 18816, 35826, 34272, 17472, 4608, 512;"
			],
			"maple": [
				"egf := exp(t*(exp(-x) - x - 1));",
				"ser := series(egf, x, 22):",
				"p := n -\u003e coeff(ser, x, n);",
				"seq(seq((-1)^n*n!*coeff(p(n), t, k), k=0..n), n = 0..10);",
				"# Alternative:",
				"T := (n, k) -\u003e add(binomial(n, k - j)*Stirling2(n - k + j, j), j=0..k):",
				"seq(seq(T(n, k), k = 0..n), n=0..9); # _Peter Luschny_, Feb 09 2021"
			],
			"mathematica": [
				"T[ n_, k_] := Sum[ Binomial[n, k-j] StirlingS2[n-k+j, j], {j, 0 ,k}]; (* _Michael Somos_, Jul 18 2021 *)"
			],
			"program": [
				"(PARI) T(n, k) = sum(j=0, k, binomial(n, j)*stirling(n-j, k-j, 2)); /* _Michael Somos_, Jul 18 2021 */"
			],
			"xref": [
				"Sum of row(n) is A000110(n+1).",
				"Sum of row(n) - 2^n is A058681(n).",
				"Alternating sum of row(n) is A109747(n).",
				"Cf. A001035, A006905."
			],
			"keyword": "nonn,tabl,changed",
			"offset": "0,3",
			"author": "_Peter Luschny_, Jan 08 2021",
			"ext": [
				"New name from _Peter Luschny_, Feb 09 2021"
			],
			"references": 2,
			"revision": 17,
			"time": "2022-01-14T07:41:28-05:00",
			"created": "2021-01-08T12:59:09-05:00"
		}
	]
}