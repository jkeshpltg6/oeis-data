{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007729",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7729,
			"data": "1,2,4,5,8,10,13,14,18,21,26,28,33,36,40,41,46,50,57,60,68,73,80,82,89,94,102,105,112,116,121,122,128,133,142,146,157,164,174,177,188,196,209,214,226,233,242,244,253,260,272,277,290,298,309,312,322,329,340,344",
			"name": "6th binary partition function.",
			"comment": [
				"From _Gary W. Adamson_, Aug 31 2016: (Start)",
				"The sequence is the left-shifted vector of the production matrix M, with lim_{k-\u003einf} M^k. M =",
				"  1, 0, 0, 0, 0, ...",
				"  2, 0, 0, 0, 0, ...",
				"  2, 1, 0, 0, 0, ...",
				"  1, 2, 0, 0, 0, ...",
				"  0, 2, 1, 0, 0, ...",
				"  0, 1, 2, 0, 0, ...",
				"  0, 0, 2, 1, 0, ...",
				"  0, 0, 1, 2, 0, ...",
				"  ...",
				"The sequence is equal to the product of its aerated variant by (1,2,2,1): (1, 2, 2, 1) * (1, 0, 2, 0, 4, 0, 5, 0, 8, ...) = (1, 2, 4, 5, 8, 10, ...).",
				"Term a((2^n) - 1) = A007051: (1, 2, 5, 14, 41, 122, ...). (End)",
				"a(n) is the number of ways to represent 2n (or 2n+1) as a sum e_0 + 2*e_1 + ... + (2^k)*e_k with each e_i in {0,1,2,3,4,5}. - _Michael J. Collins_, Dec 25 2018"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A007729/b007729.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Michael J. Collins, David Wilson, \u003ca href=\"https://arxiv.org/abs/1812.11174\"\u003eEquivalence of OEIS A007729 and A174868\u003c/a\u003e, arXiv:1812.11174 [math.CO], 2018.",
				"B. Reznick, \u003ca href=\"http://dx.doi.org/10.1007/978-1-4612-3464-7_29\"\u003eSome binary partition functions\u003c/a\u003e, in \"Analytic number theory\" (Conf. in honor P. T. Bateman, Allerton Park, IL, 1989), 451-477, Progr. Math., 85, Birkhäuser Boston, Boston, MA, 1990."
			],
			"formula": [
				"G.f.: (r(x) * r(x^2) * r(x^4) * r(x^8) * ...) where r(x) = (1 + 2x + 2x^2 + x^3 + 0 + 0 + 0 + ...). - _Gary W. Adamson_, Sep 01 2016",
				"a(2k) = 2*a(k-1) + a(k); a(2k+1) = 2*a(k) + a(k-1). - _Michael J. Collins_, Dec 25 2018"
			],
			"maple": [
				"b:= proc(n) option remember;",
				"      `if`(n\u003c2, n, `if`(irem(n, 2)=0, b(n/2), b((n-1)/2) +b((n+1)/2)))",
				"    end:",
				"a:= proc(n) option remember;",
				"      b(n+1) +`if`(n\u003e0, a(n-1), 0)",
				"    end:",
				"seq(a(n), n=0..70);  # _Alois P. Heinz_, Jun 21 2012"
			],
			"mathematica": [
				"b[n_] := b[n] = If[n\u003c2, n, If[Mod[n, 2] == 0, b[n/2], b[(n-1)/2]+b[(n+1)/2]]]; a[n_] := a[n] = b[n+1] + If[n\u003e0, a[n-1], 0]; Table[a[n], {n, 0, 70}] (* _Jean-François Alcover_, Mar 17 2014, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"A column of A072170.",
				"Cf. A002487, A007051.",
				"Apart from an initial zero, coincides with A174868."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Vladeta Jovovic_, May 06 2004"
			],
			"references": 4,
			"revision": 47,
			"time": "2019-01-01T13:10:18-05:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}