{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A336504",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 336504,
			"data": "1,2,3,4,6,8,9,12,15,16,18,20,24,26,27,30,32,36,39,40,42,44,45,48,52,54,56,60,63,64,66,72,78,80,81,84,88,90,96,99,100,104,105,108,112,117,120,126,128,130,132,135,140,144,150,156,160,162,165,168,176,180",
			"name": "3-practical numbers: numbers m such that the polynomial x^m - 1 has a divisor of every degree \u003c= m in the prime field F_3[x].",
			"comment": [
				"For a rational prime number p, a \"p-practical number\" is a number m such that the polynomial x^m - 1 has a divisor of every degree \u003c= m in F_p[x], the prime field of order p.",
				"A number m is 3-practical if and only if every number 1 \u003c= k \u003c= m can be written as Sum_{d|m} A007734(d) * n_d, where A007734(d) is the multiplicative order of 3 modulo the largest divisor of d not divisible by 3, and 0 \u003c= n_d \u003c= phi(d)/A007734(d).",
				"The number of terms not exceeding 10^k for k = 1, 2, ... are 7, 41, 258, 1881, 15069, 127350, 1080749, ..."
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A336504/b336504.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Paul Pollack and Lola Thompson, \u003ca href=\"http://nyjm.albany.edu/j/2013/19-7.html\"\u003eOn the degrees of divisors of T^n-1\u003e\u003c/a\u003e, New York Journal of Mathematics, Vo. 19 (2013), pp. 91-116, \u003ca href=\"https://arxiv.org/abs/1206.2084\"\u003epreprint\u003c/a\u003e, arXiv:1206.2084 [math.NT], 2012.",
				"Lola Thompson, \u003ca href=\"http://www.lolathompson.com/uploads/1/1/0/6/110629329/thesis.pdf\"\u003eProducts of distinct cyclotomic polynomials\u003c/a\u003e, Ph.D. thesis, Dartmouth College, 2012.",
				"Lola Thompson, \u003ca href=\"https://doi.org/10.1142/S1793042112501412\"\u003eOn the divisors of x^n - 1 in F_p[x]\u003c/a\u003e, International Journal of Number Theory, Vol. 9, No. 2 (2013), pp. 421-430.",
				"Lola Thompson, \u003ca href=\"https://doi.org/10.5802/jtnb.866\"\u003eVariations on a question concerning the degrees of divisors of x^n - 1\u003c/a\u003e, Journal de Théorie des Nombres de Bordeaux, Vol. 26, No. 1 (2014), pp. 253-267.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"https://mathworld.wolfram.com/FiniteField.html\"\u003eFinite Field\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Finite_field\"\u003eFinite field\u003c/a\u003e."
			],
			"mathematica": [
				"rep[v_, c_] := Flatten @ Table[ConstantArray[v[[i]], {c[[i]]}], {i, Length[c]}]; mo[n_, p_] := MultiplicativeOrder[p, n/p^IntegerExponent[n, p]]; ppQ[n_, p_] := Module[{d = Divisors[n]}, m = mo[#, p] \u0026 /@ d; ns = EulerPhi[d]/m; r = rep[m, ns]; Min @ Rest @ CoefficientList[Series[Product[1 + x^r[[i]], {i, Length[r]}], {x, 0, n}], x] \u003e  0]; Select[Range[200], ppQ[#, 3] \u0026]"
			],
			"xref": [
				"Cf. A038502, A007734, A007949.",
				"Cf. A000010, A260653, A336503, A336505."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Amiram Eldar_, Jul 23 2020",
			"references": 4,
			"revision": 9,
			"time": "2020-07-26T02:34:21-04:00",
			"created": "2020-07-25T10:40:04-04:00"
		}
	]
}