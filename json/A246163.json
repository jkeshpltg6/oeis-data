{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A246163",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 246163,
			"data": "1,2,7,3,6,9,8,5,10,27,4,24,11,15,30,45,12,40,26,29,17,34,119,20,25,120,46,39,51,102,14,153,60,43,136,114,31,105,85,170,44,18,427,68,125,408,13,150,33,187,255,510,116,54,41,765,204,135,28,680,16,23,442,99,461,257,35,514,156,90,123,1799,118,340,393,36",
			"name": "Permutation of natural numbers: a(1) = 1, a(A014580(n)) = A065621(1+a(n)), a(A091242(n)) = A048724(a(n)), where A065621(n) and A048724(n) give the reversing binary representation of n and -n, respectively, and A014580 resp. A091242 are the binary coded irreducible resp. reducible polynomials over GF(2).",
			"comment": [
				"This is an instance of entanglement permutation, where the two complementary pairs to be entangled with each other are A014580/A091242 (binary codes for irreducible and reducible polynomials over GF(2)) and A065621/A048724, the latter which themselves are permutations of A000069/A001969 (odious and evil numbers), which means that this permutation shares many properties with A246161.",
				"Because 3 is the only evil number in A014580, it implies that, apart from a(3)=7, odious numbers occur in odious positions only (along with many evil numbers that also occur in odious positions).",
				"Furthermore, all terms of A246158 reside in infinite cycles, and apart from 4 and 8, all of them reside in separate cycles. The infinite cycle containing 4 and 8 goes as: ..., 2091, 97, 47, 13, 11, 4, 3, 7, 8, 5, 6, 9, 10, 27, 46, 408, 2535, ... and it is only because a(3) = 7, that it can temporarily switch back from evil terms to odious terms, until right after a(8) = 5 it is finally doomed to the eternal evilness.",
				"Please see also the comments at A246201 and A246161."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A246163/b246163.txt\"\u003eTable of n, a(n) for n = 1..10001\u003c/a\u003e",
				"\u003ca href=\"/index/Ge#GF2X\"\u003eIndex entries for sequences operating on GF(2)[X]-polynomials\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"a(1) = 1, and for n \u003e 1, if n is in A014580, a(n) = A065621(1+a(A091226(n))), otherwise a(n) = A048724(a(A091245(n))).",
				"As a composition of related permutations:",
				"a(n) = A193231(A246201(n)).",
				"a(n) = A234026(A245701(n)).",
				"a(n) = A234612(A246161(n)).",
				"a(n) = A193231(A246203(A193231(n))).",
				"Other identities:",
				"For all n \u003e 1, A010060(a(n)) = A091225(n). [Maps binary representations of irreducible GF(2) polynomials (A014580) to odious numbers and the corresponding representations of reducible polynomials (A091242) to evil numbers, in some order]."
			],
			"program": [
				"(Scheme, with memoization-macro definec)",
				"(definec (A246163 n) (cond ((= 1 n) n) ((= 1 (A091225 n)) (A065621 (+ 1 (A246163 (A091226 n))))) (else (A048724 (A246163 (A091245 n))))))"
			],
			"xref": [
				"Inverse: A246164.",
				"Similar or related permutations: A246205, A193231, A246201, A234026, A245701, A234612, A246161, A246203.",
				"Cf. A010060, A014580, A048724, A065621, A091225, A091226, A091245."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Antti Karttunen_, Aug 19 2014",
			"references": 7,
			"revision": 17,
			"time": "2014-08-20T23:58:45-04:00",
			"created": "2014-08-20T23:58:45-04:00"
		}
	]
}