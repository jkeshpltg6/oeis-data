{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A181819",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 181819,
			"data": "1,2,2,3,2,4,2,5,3,4,2,6,2,4,4,7,2,6,2,6,4,4,2,10,3,4,5,6,2,8,2,11,4,4,4,9,2,4,4,10,2,8,2,6,6,4,2,14,3,6,4,6,2,10,4,10,4,4,2,12,2,4,6,13,4,8,2,6,4,8,2,15,2,4,6,6,4,8,2,14,7,4,2,12,4,4,4,10,2,12,4,6,4,4,4,22,2,6,6,9,2,8,2,10,8",
			"name": "a(1) = 1; for n\u003e1, if n = Product prime(i)^e(i), then a(n) = Product prime(e(i)).",
			"comment": [
				"a(n) depends only on prime signature of n (cf. A025487). a(m) = a(n) iff m and n have the same prime signature."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A181819/b181819.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_OEIS:_Section_Eu#epf\"\u003eIndex entries for sequences computed from exponents in factorization of n\u003c/a\u003e"
			],
			"formula": [
				"From _Antti Karttunen_, Feb 07 2016: (Start)",
				"a(1) = 1; for n \u003e 1, a(n) = A000040(A067029(n)) * a(A028234(n)).",
				"a(1) = 1; for n \u003e 1, a(n) = A008578(A001511(n)) * a(A064989(n)).",
				"Other identities. For all n \u003e= 1:",
				"a(A124859(n)) = A122111(a(n)) = A238745(n). - from _Matthew Vandermast_'s formulas for the latter sequence.",
				"(End)",
				"a(n) = A246029(A156552(n)). - _Antti Karttunen_, Oct 15 2016"
			],
			"example": [
				"20 = 2^2*5 has the exponents (2,1) in its prime factorization. Accordingly, a(20) = prime(2)*prime(1) = A000040(2)*A000040(1) = 3*2 = 6."
			],
			"maple": [
				"A181819 := proc(n)",
				"    local a;",
				"    a := 1;",
				"    for pf in ifactors(n)[2] do",
				"        a := a*ithprime(pf[2]) ;",
				"    end do:",
				"    a ;",
				"end proc:",
				"seq(A181819(n),n=1..80) ; # _R. J. Mathar_, Jan 09 2019"
			],
			"mathematica": [
				"{1}~Join~Table[Times @@ Prime@ Map[Last, FactorInteger@ n], {n, 2, 120}] (* _Michael De Vlieger_, Feb 07 2016 *)"
			],
			"program": [
				"(Haskell)",
				"a181819 = product . map a000040 . a124010_row",
				"-- _Reinhard Zumkeller_, Mar 26 2012",
				"(PARI) a(n) = {my(f=factor(n)); prod(k=1, #f~, prime(f[k,2]));} \\\\ _Michel Marcus_, Nov 16 2015",
				"(Scheme, with memoization-macro definec, two variants)",
				"(definec (A181819 n) (cond ((= 1 n) 1) (else (* (A000040 (A067029 n)) (A181819 (A028234 n))))))",
				"(definec (A181819 n) (cond ((= 1 n) 1) ((even? n) (* (A000040 (A007814 n)) (A181819 (A000265 n)))) (else (A181819 (A064989 n)))))",
				";; _Antti Karttunen_, Feb 05 \u0026 07 2016"
			],
			"xref": [
				"Cf. A000040, A000265, A001511, A007814, A008578, A028234, A064989, A067029, A122111, A124010, A124859, A156552, A181820, A181821, A182850, A115621, A101296, A238690, A238745, A238748, A246029."
			],
			"keyword": "nonn,easy,mult",
			"offset": "1,2",
			"author": "_Matthew Vandermast_, Dec 07 2010",
			"references": 249,
			"revision": 61,
			"time": "2021-04-18T03:14:36-04:00",
			"created": "2010-11-13T14:07:27-05:00"
		}
	]
}