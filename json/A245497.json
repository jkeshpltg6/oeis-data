{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A245497",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 245497,
			"data": "2,2,8,2,18,8,18,8,50,8,72,18,32,32,128,18,162,32,72,50,242,32,200,72,162,72,392,32,450,128,200,128,288,72,648,162,288,128,800,72,882,200,288,242,1058,128,882,200,512,288,1352,162,800,288,648,392,1682",
			"name": "a(n) = phi(n)^2/2, where phi(n) = A000010(n), the Euler totient function.",
			"comment": [
				"Values of a(n) \u003c 3 are non-integers since phi(1) = phi(2) = 1 (odd). Since phi(n) is even for all n \u003e 2, a(n) is a positive integer.",
				"a(n) gives the sum of all the parts in the partitions of phi(n) with exactly two parts (see example).",
				"a(n) is also the area of a square with diagonal phi(n)."
			],
			"link": [
				"Jens Kruse Andersen, \u003ca href=\"/A245497/b245497.txt\"\u003eTable of n, a(n) for n = 3..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = phi(n)^2/2 = A000010(n)^2/2 = A127473(n)/2, n \u003e 2."
			],
			"example": [
				"a(5) = 8; since phi(5)^2/2 = 4^2/2 = 8. The partitions of phi(5) = 4 into exactly two parts are: (3,1) and (2,2). The sum of all the parts in these partitions gives: 3+1+2+2 = 8.",
				"a(7) = 18; since phi(7)^2/2 = 6^2/2 = 18. The partitions of phi(7) = 6 into exactly two parts are: (5,1), (4,2) and (3,3). The sum of all the parts in these partitions gives: 5+1+4+2+3+3 = 18."
			],
			"maple": [
				"with(numtheory): 245497:=n-\u003ephi(n)^2/2: seq(245497(n), n=3..50);"
			],
			"mathematica": [
				"Table[EulerPhi[n]^2/2, {n, 3, 50}]"
			],
			"program": [
				"(PARI) vector(100, n, eulerphi(n+2)^2/2) \\\\ _Derek Orr_, Aug 04 2014"
			],
			"xref": [
				"Cf. A000010, A127473."
			],
			"keyword": "nonn,easy",
			"offset": "3,1",
			"author": "_Wesley Ivan Hurt_, Jul 24 2014",
			"references": 2,
			"revision": 20,
			"time": "2016-12-09T04:08:58-05:00",
			"created": "2014-08-07T16:18:23-04:00"
		}
	]
}