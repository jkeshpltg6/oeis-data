{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A302977",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 302977,
			"data": "1,-1,-1,-43,-223,-60623,-764783,-107351407,-2499928867,-596767688063,-22200786516383,-64470807442488761,-3504534741776035061,-3597207408242668198973,-268918457620309807441853,-185388032403184965693274807,-18241991360742724891839902347",
			"name": "Numerators of the rational factor of Kaplan's series for the Dottie number.",
			"comment": [
				"In Kaplan's original article, where the term \"Dottie\" was coined, he mentioned that while the number was indeed transcendental, it was possible to express it as an infinite sum with the general term r_n*π^(2n+1) where r_n was a sequence of rational numbers."
			],
			"reference": [
				"Bertrand, J., Exercise III in Traité d'algèbre, Vols. 1-2, 4th ed. Paris, France: Librairie de L. Hachette et Cie, p. 285, 1865."
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A302977/b302977.txt\"\u003eTable of n, a(n) for n = 0..100\u003c/a\u003e",
				"Ozaner Hansha, \u003ca href=\"https://ozanerhansha.github.io/dottie-number\"\u003eThe Dottie Number\u003c/a\u003e.",
				"Ozaner Hansha, \u003ca href=\"https://ozanerhansha.github.io/dottie-number/#kaplans-series\"\u003eKaplan's series\u003c/a\u003e",
				"Samuel R. Kaplan, \u003ca href=\"https://www.maa.org/sites/default/files/Kaplan2007-131105.pdf\"\u003eThe Dottie Number\u003c/a\u003e, Math. Magazine, 80 (No. 1, 2007), 73-74.",
				"V. Salov, \u003ca href=\"https://arxiv.org/abs/1212.1027\"\u003eInevitable Dottie Number. Iterals of cosine and sine\u003c/a\u003e, arXiv preprint arXiv:1212.1027 [math.HO], 2012."
			],
			"formula": [
				"These are the numerators of the unique sequence of rational numbers r_n such that d=Sum_{n\u003e=0} (r_n*Pi^(2n+1)) (where d is the Dottie number A003957).",
				"r_0 = 1/4 and for n\u003e0, r_n = b_(2n+1); where b_n = g^(n)(Pi/2)/(2^n*n!)) (and g^(n) is the n-th derivative of the inverse of x - cos x. A proof of this can be found in the second Hansha link."
			],
			"example": [
				"The partial Kaplan series at n=3 is d = Pi/4 - Pi^3/768 - Pi^5/61440 - 43*Pi^7/165150720."
			],
			"mathematica": [
				"f[x_] := x - Cos[x]; g[x_] := InverseFunction[f][x]; s = {Pi/4}; Do[AppendTo[s, Numerator[(-1/2)^n * 1/n! * Derivative[n][g][Pi/2]], {n, 3, 30, 2}]; s (* _Amiram Eldar_, Jan 31 2019 *)"
			],
			"xref": [
				"Cf. A003957, A177413, A182503, A200309, A212112, A212113."
			],
			"keyword": "sign,frac",
			"offset": "0,4",
			"author": "_Ozaner Hansha_, Apr 16 2018",
			"ext": [
				"More terms from _Amiram Eldar_, Jan 31 2019"
			],
			"references": 2,
			"revision": 21,
			"time": "2019-02-01T01:25:02-05:00",
			"created": "2019-01-30T06:05:53-05:00"
		}
	]
}