{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A052882",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 52882,
			"data": "0,1,2,9,52,375,3246,32781,378344,4912515,70872610,1124723193,19471590876,365190378735,7376016877334,159620144556645,3684531055645648,90366129593683035,2346673806524446218",
			"name": "A simple grammar: rooted ordered set partitions.",
			"comment": [
				"Recurrence (see Mathematica line) is similar to that for Genocchi numbers A001469. - _Wouter Meeussen_, Jan 09 2001",
				"Stirling transform of A024167(n) = [ 1, 1, 5, 14, 94, ...] is a(n) = [ 1, 2, 9, 52, 375, ...]. Stirling transform of a(n) = [ 0, 2, 9, 52, 375, ...] is A087301(n+1) = [ 0, 2, 3, 20, ...]. - _Michael Somos_, Mar 04 2004",
				"Starting with offset 1 = the right border of triangle A208744. - _Gary W. Adamson_, Mar 05 2012",
				"a(n) is the number of ordered set partitions of {1,2,...,n} such that the first block is a singleton. - _Geoffrey Critzer_, Jul 22 2013",
				"Ramanujan gives a method of finding a continued fraction of the solution x of an equation 1 = x + a2*x^2 + ... and uses log(2) as the solution of 1 = x + x^2/2 + x^3/6 + ... as an example giving the sequence of simplified convergents as 0/1, 1/1, 2/3, 9/13, 52/75, 375/541, ... of which the sequence of numerators is this sequence while A000670 is the denominators. - _Michael Somos_, Jun 19 2015"
			],
			"reference": [
				"S. Ramanujan, Notebooks, Tata Institute of Fundamental Research, Bombay 1957 Vol. 1, see page 19."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A052882/b052882.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"S. Giraudo, \u003ca href=\"http://arxiv.org/abs/1306.6938\"\u003eCombinatorial operads from monoids\u003c/a\u003e, arXiv preprint arXiv:1306.6938 [math.CO], 2013.",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=855\"\u003eEncyclopedia of Combinatorial Structures 855\u003c/a\u003e",
				"S. Ramanujan, \u003ca href=\"http://www.imsc.res.in/~rao/ramanujan/NoteBooks/NoteBook2/chapterII/page10.htm\"\u003eNotebook entry\u003c/a\u003e"
			],
			"formula": [
				"E.g.f.: x / (2 - exp(x)).",
				"a(n) = n * A000670(n-1) if n\u003e0.",
				"a(n) = (1/2)*sum(k=0, n-1, B_k*A000629(k)*binomial(n, k)) where B_k is the k-th Bernoulli number. - _Benoit Cloitre_, Oct 19 2005",
				"a(n) ~ n!/(2*(log(2))^n). - _Vaclav Kotesovec_, Aug 09 2013",
				"a(0) = 0, a(1) = 1; a(n) = n! * [x^n] exp(x)*Sum_{k=1..n-1} a(k)*x^k/k!. - _Ilya Gutkovskiy_, Oct 17 2017"
			],
			"example": [
				"G.f. = x + 2*x^2 + 9*x^3 + 52*x^4 + 375*x^5 + 3246*x^6 + 32781*x^7 + ..."
			],
			"maple": [
				"spec := [S,{C=Sequence(B),B=Set(Z,1 \u003c= card),S=Prod(Z,C)},labeled]: seq(combstruct[count](spec,size=n), n=0..20);",
				"with(combinat): a:=n-\u003e add(add(add((-1)^(k-i)*binomial(k, i)*i^(n-1), i=0..n-1), k=0..n-1), m=0..n-1): seq(a(n), n=0..20); # _Zerinvary Lajos_, Jun 03 2007"
			],
			"mathematica": [
				"a[1] := 1; a[n_] := a[n]=Sum[ Binomial[n, m] a[ n-m], {m, 1, n-1}]",
				"Range[0, 30]!* CoefficientList[Series[x/(2 - Exp[x]),{x, 0, 30}], x] (* _Vincenzo Librandi_, Dec 06 2012 *)",
				"a[ n_] := If[ n \u003c 2, Boole[n == 1], n PolyLog[ 1 - n, 1/2] / 2]; (* _Michael Somos_, Jun 19 2015 *)",
				"a[ n_] := If[ n \u003c 0, 0, n! SeriesCoefficient[ x / (2 - Exp@x), {x, 0, n}]]; (* _Michael Somos_, Jun 19 2015 *)",
				"Fubini[n_, r_] := Sum[k!*Sum[(-1)^(i+k+r)*(i+r)^(n-r)/(i!*(k-i-r)!), {i, 0, k-r}], {k, r, n}]; Fubini[0, 1] = 1; a[n_] := n*Fubini[n-1, 1]; Table[ a[n], {n, 0, 18}] (* _Jean-François Alcover_, Mar 30 2016 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, 0, n! * polcoeff( subst( x / (1 - y), y, exp(x + x*O(x^n)) - 1), n))};"
			],
			"xref": [
				"Cf. A001469.",
				"Cf. A000629, A000670, A024167, A087301.",
				"Cf. A108744."
			],
			"keyword": "easy,nonn",
			"offset": "0,3",
			"author": "encyclopedia(AT)pommard.inria.fr, Jan 25 2000",
			"references": 16,
			"revision": 52,
			"time": "2017-10-17T22:17:48-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}