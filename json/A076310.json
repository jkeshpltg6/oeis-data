{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A076310",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 76310,
			"data": "0,4,8,12,16,20,24,28,32,36,1,5,9,13,17,21,25,29,33,37,2,6,10,14,18,22,26,30,34,38,3,7,11,15,19,23,27,31,35,39,4,8,12,16,20,24,28,32,36,40,5,9,13,17,21,25,29,33,37,41,6,10,14,18,22,26,30,34,38,42,7,11,15,19,23",
			"name": "a(n) = floor(n/10) + 4*(n mod 10).",
			"comment": [
				"(n==0 modulo 13) iff (a(n)==0 modulo 13); applied recursively, this property provides a divisibility test for numbers given in base 10 notation."
			],
			"reference": [
				"Karl Menninger, Rechenkniffe, Vandenhoeck \u0026 Ruprecht in Goettingen (1961), 79A."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A076310/b076310.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/DivisibilityTests.html\"\u003eDivisibility Tests\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Divisibility_rule\"\u003eDivisibility rule\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_11\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1, 0, 0, 0, 0, 0, 0, 0, 0, 1, -1)."
			],
			"formula": [
				"a(n) = +a(n-1) +a(n-10) -a(n-11). G.f.: -x*(-4-4*x-4*x^2-4*x^3-4*x^4-4*x^5-4*x^6-4*x^7-4*x^8+35*x^9) / ( (1+x) *(x^4+x^3+x^2+x+1) *(x^4-x^3+x^2-x+1) *(x-1)^2 ). - _R. J. Mathar_, Feb 20 2011"
			],
			"example": [
				"435598 is not a multiple of 13, as 435598 -\u003e 43559+4*8=43591 -\u003e 4359+4*1=4363 -\u003e 436+4*3=448 -\u003e 44+4*8=76 -\u003e 7+4*6=29=13*2+3, therefore the answer is NO.",
				"Is 8424 divisible by 13? 8424 -\u003e 842+4*4=858 -\u003e 85+4*8=117 -\u003e 11+4*7=39=13*3, therefore the answer is YES."
			],
			"maple": [
				"A076310:=n-\u003efloor(n/10) + 4*(n mod 10); seq(A076310(n), n=0..100); # _Wesley Ivan Hurt_, Jan 30 2014"
			],
			"mathematica": [
				"Table[Floor[n/10] + 4*Mod[n, 10], {n, 0, 100}] (* _Wesley Ivan Hurt_, Jan 30 2014 *)",
				"LinearRecurrence[{1,0,0,0,0,0,0,0,0,1,-1},{0,4,8,12,16,20,24,28,32,36,1},80] (* _Harvey P. Dale_, Sep 30 2015 *)"
			],
			"program": [
				"(Haskell)",
				"a076310 n =  n' + 4 * m where (n', m) = divMod n 10",
				"-- _Reinhard Zumkeller_, Jun 01 2013",
				"(PARI) a(n) = n\\10 + 4*(n % 10); \\\\ _Michel Marcus_, Jan 31 2014",
				"(MAGMA) [Floor(n/10)+4*(n mod 10): n in [0..75]]; // _Vincenzo Librandi_, Feb 27 2016"
			],
			"xref": [
				"Cf. A008595, A076309, A076311, A076312."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Reinhard Zumkeller_, Oct 06 2002",
			"references": 7,
			"revision": 29,
			"time": "2016-02-27T06:09:50-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}