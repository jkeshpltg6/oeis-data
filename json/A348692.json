{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A348692",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 348692,
			"data": "1,2,0,2,0,0,0,3,4,0,0,0,6,0,8,9,0,8,9,0,7,0,10,0,0,0,12,0,0,0,14,0,0,0,15,16,0,18,0,18,0,0,0,20,0,0,0,22,0,0,0,24,25,0,0,0,26,0,0,0,28,0,0,0,30,0,32,0,32,0,0,0,34,0,0,0,35,36,0,0,0,38,0,0,0,40",
			"name": "Triangle whose n-th row lists the integers m such that A000178(n) / m! is a square, where A000178(n) = n$ = 1!*2!*...*n! is the superfactorial of n; if there is no such m, then n-th row = 0.",
			"comment": [
				"This sequence is the generalization of a problem proposed during the 17th Tournament of Towns (Spring 1996) and also during the first stage of the Moscow Mathematical Olympiad (1995-1996); the problem asked the question for n = 100 (see Andreescu-Gelca reference, Norman Do link, and Examples section).",
				"Exhaustive results coming from Mabry-McCormick's link and adapted for OEIS:",
				"-\u003e n$ (A000178) is never a square if n \u003e 1.",
				"-\u003e There is no solution if n is odd \u003e 1, hence row(2q+1) = 0 when q \u003e 0.",
				"-\u003e When n is even and there is a solution, then m belongs to {n/2 - 2, n/2 - 1, n/2, n/2 + 1, n/2 + 2}.",
				"-\u003e If 4 divides n (A008536), m = n/2 is always a solution because",
				"    (n$) / (n/2)! = ( 2^(n/4) * Product_{j=1..n/2} ((2j-1)!) )^2.",
				"-\u003e For other cases, see Formula section.",
				"-\u003e When n is even, there are 0, 1 or 2 solutions, so, the maximal length of a row is 2.",
				"-\u003e It is not possible to get more than three consecutive 0 terms, and three consecutive 0 terms correspond to three consecutive rows such that (n, n+1, n+2) = (4u+1, 4u+2, 4u+3) for some u \u003e= 1."
			],
			"reference": [
				"Titu Andreescu and Rǎzvan Gelca, Putnam and Beyond, New York, Springer, 2007, problem 725, pp. 253 and 686.",
				"Peter J. Taylor and A. M. Storozhev, Tournament of Towns 1993-1997, Book 4, Tournament 17, Spring 1996, O Level, Senior questions, Australian Mathematics Trust, 1998, problem 3, p. 96."
			],
			"link": [
				"Diophante, \u003ca href=\"http://www.diophante.fr/problemes-par-themes/arithmetique-et-algebre/a1-pot-pourri/1407-a1963-le-vilain-petit-canard\"\u003eA1963 - Le vilain petit canard\u003c/a\u003e (in French).",
				"Norman Do, \u003ca href=\"https://users.monash.edu/~normd/documents/Puzzle-Corner-13.pdf\"\u003eFactorial fun\u003c/a\u003e, Puzzle Corner 13, Gaz. Aust. Math. Soc. 36, 2009, 176-179, page 178.",
				"Rick Mabry and Laura McCormick, \u003ca href=\"https://www.austms.org.au/wp-content/uploads/Gazette/2009/Nov09/TechPaperMabry.pdf\"\u003eSquare products of punctured sequences of factorials\u003c/a\u003e, Gaz. Aust. Math. Soc., 2009, pages 346-352.",
				"Tournament of Towns, \u003ca href=\"https://www.turgor.ru/problems/17/index.php#turnir17otm\"\u003eTournament 17, 1995-1996, Spring 1996\u003c/a\u003e, O Level, Senior questions, question 3 (in Russian).",
				"\u003ca href=\"/index/O#Olympiads\"\u003eIndex to sequences related to Olympiads and other Mathematical competitions\u003c/a\u003e."
			],
			"formula": [
				"When there are two such integers m, then m_1 \u003c m_2.",
				"If n = 8*q^2 (A139098), then m_1 = n/2 - 1 = 4q^2-1 (see example for n=8).",
				"If n = 8q*(q+1) (A035008), then m_2 = n/2 + 1 = (2q+1)^2 (see example for n=16).",
				"if n = 4q^2 - 2 (A060626), then m_1 = n/2 + 1 = 2q^2 (see example for n=14).",
				"If n = 2q^2, q\u003e1 in A001541, then m = n/2 - 2 = q^2-2 (see example for n=18).",
				"If n = 2q^2-4, q\u003e1 in A001541, then m_2 = n/2 + 2 = q^2 (see example for n=14)."
			],
			"example": [
				"For n = 4, 4$ / 3! = 48, 4$ / 4! = 12 but 4$ / 2! = 12^2, hence, m = 2.",
				"For n = 8, 8$ / 2! is not a square, but m_1 = 3 because 8$ / 3! = 29030400^2 and m_2 = 4 because 8$ / 4! = 14515200^2.",
				"For n = 14, m_1 = 8 because 14$ / 8! = 1309248519599593818685440000000^2 and m_2 = 9 because 14$ / 9! = 436416173199864606228480000000^2.",
				"For n = 16, m_1 = 8 because 16$ / 8! = 6848282921689337839624757371207680000000000^2 and m_2 = 9 because 16$ / 9! = 2282760973896445946541585790402560000000000^2.",
				"For n = 18, m = 7 because 18$ / 7! = 29230177671473293820176594405114531928195727360000000000000^2 and there is no other solution.",
				"For n = 100, m = 50, unique solution to the Olympiad problems.",
				"Triangle begins:",
				"    1;",
				"    2;",
				"    0;",
				"    2;",
				"    0;",
				"    0;",
				"    0;",
				"    8,  9;",
				"    0;",
				"    ..."
			],
			"program": [
				"(PARI) sf(n)=prod(k=2, n, k!); \\\\ A000178",
				"row(n) = my(s=sf(n)); Vec(select(issquare, vector(n, k, s/k!), 1));",
				"lista(nn) = {my(list = List()); for (n=1, nn, my(r=row(n)); if (#r, for (k=1, #r, listput(list, r[k])), listput(list, 0));); Vec(list);} \\\\ _Michel Marcus_, Oct 30 2021"
			],
			"xref": [
				"Cf. A000178, A001541, A008536, A035008, A060626, A139098."
			],
			"keyword": "nonn,tabf",
			"offset": "1,2",
			"author": "_Bernard Schott_, Oct 30 2021",
			"references": 9,
			"revision": 60,
			"time": "2021-11-22T12:26:58-05:00",
			"created": "2021-11-19T16:36:59-05:00"
		}
	]
}