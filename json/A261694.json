{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A261694",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 261694,
			"data": "0,1,1,2,3,5,8,13,0,13,13,5,18,2,20,1,0,1,1,2,3,5,8,13,0,13,13,5,18,2,20,1,0,1,1,2,3,5,8,13,0,13,13,5,18,2,20,1,0,1,1,2,3,5,8,13,0,13,13,5,18,2,20,1,0,1,1,2,3,5,8,13,0,13,13,5,18,2,20,1,0,1,1,2,3,5,8,13,0,13",
			"name": "a(n) = Fibonacci(n) mod 21.",
			"comment": [
				"Sequence is periodic with Pisano period 16; Pisano number 21 in the sequence A001175. The only other sequence with Pisano period 16 is that of A105870 which is the Fibonacci sequence mod 7. This is Pisano number 7."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A261694/b261694.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e [a(391) = 13 corrected by _Georg Fischer_, May 24 2019]"
			],
			"formula": [
				"G.f.: x*(1 + x + 2*x^2 + 3*x^3 + 5*x^4 + 8*x^5 + 13*x^6 + 13*x^8 + 13*x^9 + 5*x^10 + 18*x^11 + 2*x^12 + 20*x^13 + x^14)/(1-x^16)."
			],
			"mathematica": [
				"Table[Mod[Fibonacci[n], 21], {n, 0, 100}]",
				"PadRight[{},120,{0,1,1,2,3,5,8,13,0,13,13,5,18,2,20,1}] (* _Harvey P. Dale_, May 16 2020 *)"
			],
			"program": [
				"(MAGMA) [Fibonacci(n) mod 21: n in [0..120]]; // _Vincenzo Librandi_, Nov 19 2015",
				"(PARI) a(n) = fibonacci(n)%21; \\\\ _Altug Alkan_, Nov 19 2015",
				"(Python)",
				"A261694_list, a, b, = [], 0, 1",
				"for _ in range(10**3):",
				"    A261694_list.append(a)",
				"    a, b = b, (a+b) % 21 # _Chai Wah Wu_, Nov 26 2015"
			],
			"xref": [
				"Cf. A000045, A001175, A105870,"
			],
			"keyword": "nonn,easy",
			"offset": "0,4",
			"author": "_G. C. Greubel_, Nov 18 2015",
			"references": 1,
			"revision": 28,
			"time": "2020-05-16T12:09:04-04:00",
			"created": "2015-11-26T01:03:37-05:00"
		}
	]
}