{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A100823",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 100823,
			"data": "1,2,4,7,12,19,30,46,69,101,146,208,293,408,563,768,1040,1397,1864,2470,3254,4261,5550,7192,9277,11911,15229,19391,24597,31085,39150,49142,61489,76702,95401,118324,146362,180573,222226,272826,334173,408394,498022",
			"name": "G.f.: Product_{k\u003e0} (1+x^k)/((1-x^k)*(1+x^(3k))*(1+x^(5k))).",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"Vaclav Kotesovec and Alois P. Heinz, \u003ca href=\"/A100823/b100823.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e (first 2001 terms from Vaclav Kotesovec)",
				"Vaclav Kotesovec, \u003ca href=\"http://arxiv.org/abs/1509.08708\"\u003eA method of finding the asymptotics of q-series based on the convolution of generating functions\u003c/a\u003e, arXiv:1509.08708 [math.CO], Sep 30 2015, p. 17.",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"a(n) ~ exp(Pi*sqrt(37*n/5)/3) * sqrt(37) / (12*sqrt(5)*n). - _Vaclav Kotesovec_, Sep 01 2015",
				"G.f.: (E(2)*E(3)*E(5)) / (E(1)^2*E(6)*E(10)) where E(k) = prod(n\u003e=1, 1-q^k ). - _Joerg Arndt_, Sep 01 2015",
				"Euler transform of period 30 sequence [ 2, 1, 1, 1, 1, 1, 2, 1, 1, 1, 2, 1, 2, 1, 0, 1, 2, 1, 2, 1, 1, 1, 2, 1, 1, 1, 1, 1, 2, 1, ...]. - _Michael Somos_, Mar 07 2016",
				"Expansion of chi(-x^3) * chi(-x^5) / phi(-x) in powers of x where phi(), chi() are Ramanujan theta functions. - _Michael Somos_, Mar 07 2016",
				"a(n) - A035939(2*n + 1) = A122129(2*n + 1). - _Michael Somos_, Mar 07 2016"
			],
			"example": [
				"G.f. = 1 + 2*x + 4*x^2 + 7*x^3 + 12*x^4 + 19*x^5 + 30*x^6 + 46*x^7 + ...",
				"G.f. = q^-1 + 2*q^2 + 4*q^5 + 7*q^8 + 12*q^11 + 19*q^14 + 30*q^17 + 46*q^20 + ..."
			],
			"maple": [
				"series(product((1+x^k)/((1-x^k)*(1+x^(3*k))*(1+x^(5*k))),k=1..100),x=0,100);"
			],
			"mathematica": [
				"CoefficientList[ Series[ Product[(1 + x^k)/((1 - x^k)*(1 + x^(3k))*(1 + x^(5k))), {k, 100}], {x, 0, 45}], x] (* _Robert G. Wilson v_, Jan 12 2005 *)",
				"nmax = 50; CoefficientList[Series[Product[(1+x^(5*k-1))*(1+x^(5*k-2))*(1+x^(5*k-3))*(1+x^(5*k-4)) / ((1-x^(6*k))*(1-x^(3*k-1))*(1-x^(3*k-2))), {k, 1, nmax}], {x, 0, nmax}], x] (* _Vaclav Kotesovec_, Sep 01 2015 *)",
				"a[ n_] := SeriesCoefficient[ QPochhammer[ x^3, x^6] QPochhammer[ x^5, x^10] / EllipticTheta[ 4, 0, x], {x, 0, n}]; (* _Michael Somos_, Mar 07 2016 *)"
			],
			"program": [
				"(PARI) q='q+O('q^33); E(k)=eta(q^k);",
				"Vec( (E(2)*E(3)*E(5)) / (E(1)^2*E(6)*E(10)) ) \\\\ _Joerg Arndt_, Sep 01 2015"
			],
			"xref": [
				"Cf. A035939, A098151, A102346, A122129."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Noureddine Chair_, Jan 06 2005",
			"ext": [
				"More terms from _Robert G. Wilson v_, Jan 12 2005",
				"Offset corrected by _Vaclav Kotesovec_, Sep 01 2015",
				"a(14) = 563 \u003c- 562 corrected by _Vaclav Kotesovec_, Sep 01 2015"
			],
			"references": 2,
			"revision": 31,
			"time": "2021-03-12T22:24:43-05:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}