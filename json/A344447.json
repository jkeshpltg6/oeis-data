{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A344447",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 344447,
			"data": "1,0,0,0,0,1,0,0,1,0,0,0,1,0,1,0,1,1,0,0,0,1,1,0,0,1,0,1,1,1,0,1,1,0,0,1,1,1,0,0,0,1,0,0,2,2,1,0,0,2,1,0,0,2,1,1,1,0,1,1,1,1,0,1,0,3,2,1,0,0,1,2,1,0,0,2,3,2,1,1,0,1,2,2,1,1,0,1,1,2,3,2,1,0,0,1,3,3,1,0,0,2,3,4,2,1,1",
			"name": "Number T(n,k) of partitions of n into k semiprimes; triangle T(n,k), n\u003e=0, read by rows.",
			"comment": [
				"T(n,k) is defined for all n,k \u003e= 0.  The triangle contains in each row n only the terms for k=0 and then up to the last positive T(n,k) (if it exists)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A344447/b344447.txt\"\u003eRows n = 0..500, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = [x^n y^k] 1/Product_{j\u003e=1} (1-y*x^A001358(j)).",
				"Sum_{k\u003e0} k * T(n,k) = A281617(n)."
			],
			"example": [
				"Triangle T(n,k) begins:",
				"  1 ;",
				"  0 ;",
				"  0 ;",
				"  0 ;",
				"  0, 1 ;",
				"  0    ;",
				"  0, 1 ;",
				"  0    ;",
				"  0, 0, 1 ;",
				"  0, 1    ;",
				"  0, 1, 1 ;",
				"  0       ;",
				"  0, 0, 1, 1 ;",
				"  0, 0, 1    ;",
				"  0, 1, 1, 1 ;",
				"  0, 1, 1    ;",
				"  0, 0, 1, 1, 1 ;",
				"  0, 0, 0, 1    ;",
				"  0, 0, 2, 2, 1 ;",
				"  0, 0, 2, 1    ;",
				"  0, 0, 2, 1, 1, 1 ;",
				"  ..."
			],
			"maple": [
				"h:= proc(n) option remember; `if`(n=0, 0,",
				"     `if`(numtheory[bigomega](n)=2, n, h(n-1)))",
				"    end:",
				"b:= proc(n, i) option remember; `if`(n=0, 1, `if`(i\u003c1, 0,",
				"     `if`(i\u003en, 0, expand(x*b(n-i, h(min(n-i, i)))))+b(n, h(i-1))))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=0..max(0, degree(p))))(b(n, h(n))):",
				"seq(T(n), n=0..32);"
			],
			"mathematica": [
				"h[n_] := h[n] = If[n == 0, 0,",
				"     If[PrimeOmega[n] == 2, n, h[n-1]]];",
				"b[n_, i_] := b[n, i] = If[n == 0, 1, If[i \u003c 1, 0,",
				"     If[i \u003e n, 0, Expand[x*b[n-i, h[Min[n-i, i]]]]] + b[n, h[i-1]]]];",
				"T[n_] := Table[Coefficient[#, x, i], {i, 0, Max[0, Exponent[#, x]]}]\u0026[b[n, h[n]]];",
				"Table[T[n], {n, 0, 32}] // Flatten (* _Jean-François Alcover_, Aug 19 2021, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Columns k=0-10 give: A000007, A064911, A072931, A344446, A340756, A344245, A344246, A344254, A344255, A344256, A344257.",
				"Row sums give A101048.",
				"T(4n,n) gives A000012.",
				"Cf. A001358, A117278, A281617."
			],
			"keyword": "nonn,tabf",
			"offset": "0,45",
			"author": "_Alois P. Heinz_, May 19 2021",
			"references": 12,
			"revision": 26,
			"time": "2021-08-19T04:47:02-04:00",
			"created": "2021-05-21T16:42:00-04:00"
		}
	]
}