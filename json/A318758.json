{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A318758",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 318758,
			"data": "1,0,1,0,1,1,0,2,1,1,0,3,4,1,1,0,6,9,3,1,1,0,12,22,9,3,1,1,0,25,54,23,8,3,1,1,0,52,138,60,23,8,3,1,1,0,113,346,164,61,22,8,3,1,1,0,247,889,443,167,61,22,8,3,1,1,0,548,2285,1209,461,168,60,22,8,3,1,1",
			"name": "Number T(n,k) of rooted trees with n nodes such that k equals the maximal number of isomorphic subtrees extending from the same node; triangle T(n,k), n\u003e=1, 0\u003c=k\u003c=n-1, read by rows.",
			"comment": [
				"T(n,k) is defined for n,k \u003e= 0.  The triangle contains only the terms with k \u003c n.  T(n,k) = 0 for k \u003e= n."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A318758/b318758.txt\"\u003eRows n = 1..200, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = A318757(n,k) - A318757(n,k-1) for k \u003e 0, A(n,0) = A063524(n)."
			],
			"example": [
				"Triangle T(n,k) begins:",
				"  1;",
				"  0,   1;",
				"  0,   1,   1;",
				"  0,   2,   1,   1;",
				"  0,   3,   4,   1,  1;",
				"  0,   6,   9,   3,  1,  1;",
				"  0,  12,  22,   9,  3,  1, 1;",
				"  0,  25,  54,  23,  8,  3, 1, 1;",
				"  0,  52, 138,  60, 23,  8, 3, 1, 1;",
				"  0, 113, 346, 164, 61, 22, 8, 3, 1, 1;"
			],
			"maple": [
				"h:= proc(n, m, t, k) option remember; `if`(m=0, binomial(n+t, t),",
				"      `if`(n=0, 0, add(h(n-1, m-j, t+1, k), j=1..min(k, m))))",
				"    end:",
				"b:= proc(n, i, k) option remember; `if`(n=0, 1, `if`(i\u003c1, 0,",
				"      add(b(n-i*j, i-1, k)*h(A(i, k), j, 0, k), j=0..n/i)))",
				"    end:",
				"A:= (n, k)-\u003e `if`(n\u003c2, n, b(n-1$2, k)):",
				"T:= (n, k)-\u003e A(n, k)-`if`(k=0, 0, A(n, k-1)):",
				"seq(seq(T(n, k), k=0..n-1), n=1..14);"
			],
			"mathematica": [
				"h[n_, m_, t_, k_] := h[n, m, t, k] = If[m == 0, Binomial[n + t, t], If[n == 0, 0, Sum[h[n - 1, m - j, t + 1, k], {j, 1, Min[k, m]}]]];",
				"b[n_, i_, k_] := b[n, i, k] = If[n == 0, 1, If[i \u003c 1, 0, Sum[b[n - i*j, i - 1, k]*h[A[i, k], j, 0, k], {j, 0, n/i}]]];",
				"A[n_, k_] := If[n \u003c 2, n, b[n - 1, n - 1, k]];",
				"T[n_, k_] := A[n, k] - If[k == 0, 0, A[n, k - 1]];",
				"Table[T[n, k], {n, 1, 14}, {k, 0, n - 1}] // Flatten (* _Jean-François Alcover_, May 11 2019, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Columns k=0-10 give: A063524, A004111 (for n\u003e1), A318859, A318860, A318861, A318862, A318863, A318864, A318865, A318866, A318867.",
				"Row sums give A000081.",
				"T(2n+2,n+1) gives A255705.",
				"Cf. A318754, A318757."
			],
			"keyword": "nonn,tabl",
			"offset": "1,8",
			"author": "_Alois P. Heinz_, Sep 02 2018",
			"references": 12,
			"revision": 18,
			"time": "2019-05-11T10:11:19-04:00",
			"created": "2018-09-04T18:52:38-04:00"
		}
	]
}