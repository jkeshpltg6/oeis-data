{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A026835",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 26835,
			"data": "1,1,1,2,1,1,2,1,1,1,3,2,1,1,1,4,2,1,1,1,1,5,3,2,1,1,1,1,6,3,2,1,1,1,1,1,8,5,3,2,1,1,1,1,1,10,5,3,2,1,1,1,1,1,1,12,7,4,3,2,1,1,1,1,1,1,15,8,5,3,2,1,1,1,1,1,1,1,18,10,6,4,3,2,1,1,1,1,1,1,1,22,12,7,4,3,2,1,1",
			"name": "Triangular array read by rows: T(n,k) = number of partitions of n into distinct parts in which every part is \u003e=k, for k=1,2,...,n.",
			"comment": [
				"T(n,1)=A000009(n), T(n,2)=A025147(n) for n\u003e1, T(n,3)=A025148(n) for n\u003e2, T(n,4)=A025149(n) for n\u003e3.",
				"A219922(n) = smallest number of row containing n. - _Reinhard Zumkeller_, Dec 01 2012"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A026835/b026835.txt\"\u003eRows n = 1..120 of triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"G.f.: Sum_{k\u003e=1} (y^k*(-1+Product_{i\u003e=k} (1+x^i))). - _Vladeta Jovovic_, Aug 25 2003",
				"T(n, k) = 1 + Sum(T(i, j): i\u003e=j\u003ek and i+j=n+1). - _Reinhard Zumkeller_, Jan 01 2003",
				"T(n, k) \u003e 1 iff 2*k \u003c n. - _Reinhard Zumkeller_, Jan 01 2003"
			],
			"example": [
				"From _Michael De Vlieger_, Aug 03 2020: (Start)",
				"Table begins:",
				"   1",
				"   1   1",
				"   2   1   1",
				"   2   1   1   1",
				"   3   2   1   1   1",
				"   4   2   1   1   1   1",
				"   5   3   2   1   1   1   1",
				"   6   3   2   1   1   1   1   1",
				"   8   5   3   2   1   1   1   1   1",
				"  10   5   3   2   1   1   1   1   1   1",
				"  12   7   4   3   2   1   1   1   1   1   1",
				"  15   8   5   3   2   1   1   1   1   1   1   1",
				"  ... (End)"
			],
			"mathematica": [
				"Nest[Function[{T, n, r}, Append[T, Table[1 + Total[T[[##]] \u0026 @@@ Select[r, #[[-1]] \u003e k + 1 \u0026]], {k, 0, n}]]] @@ {#1, #2, Transpose[1 + {#2 - #3, #3}]} \u0026 @@ {#1, #2, Range[Ceiling[#2/2] - 1]} \u0026 @@ {#, Length@ #} \u0026, {{1}}, 12] // Flatten (* _Michael De Vlieger_, Aug 03 2020 *)"
			],
			"program": [
				"(Haskell)",
				"import Data.List (tails)",
				"a026835 n k = a026835_tabl !! (n-1) !! (k-1)",
				"a026835_row n = a026835_tabl !! (n-1)",
				"a026835_tabl = map",
				"   (\\row -\u003e map (p $ last row) $ init $ tails row) a002260_tabl",
				"   where p 0      _ = 1",
				"         p _     [] = 0",
				"         p m (k:ks) = if m \u003c k then 0 else p (m - k) ks + p m ks",
				"-- _Reinhard Zumkeller_, Dec 01 2012"
			],
			"xref": [
				"Cf. A026807.",
				"Cf. A002260, A060016."
			],
			"keyword": "nonn,tabl",
			"offset": "1,4",
			"author": "_Clark Kimberling_",
			"references": 7,
			"revision": 21,
			"time": "2020-09-06T11:42:47-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}