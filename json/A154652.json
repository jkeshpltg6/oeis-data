{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A154652",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 154652,
			"data": "1,1,1,1,20,1,1,84,84,1,1,455,5005,5005,1,1,816,18564,48620,816,1,1,2024,134596,1307504,1307504,134596,1,1,2925,296010,4686825,17383860,4686825,2925,1,1,5456,1107568,38567100,1037158320,1037158320,38567100,1107568,1",
			"name": "Triangle read by rows, T(n, k) = binomial(3*(prime(n+1) - 1)/2, 3*(prime(k+1) - 1)/2) with T(n,0) = 1.",
			"comment": [
				"Row sums are {1, 2, 22, 170, 10467, 68818, 2886226, 27059372, 2153671434, 905636138220, 7579946523936, ...}."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A154652/b154652.txt\"\u003eRows n = 0..100 of triangle, flattened\u003c/a\u003e",
				"A. Lakhtakia, R. Messier, V. K. Varadan, V. V. Varadan, \u003ca href=\"http://dx.doi.org/10.1103/PhysRevA.34.2501\"\u003eUse of combinatorial algebra for diffusion on fractals\u003c/a\u003e, Physical Review A, volume 34, Number 3 (1986) page 2503 (7b)."
			],
			"formula": [
				"T(n, k) = binomial(3*(prime(n+1) - 1)/2, 3*(prime(k+1) - 1)/2) with T(n,0) = 1."
			],
			"example": [
				"Triangle begins as:",
				"  1;",
				"  1,    1;",
				"  1,   20,      1;",
				"  1,   84,     84,       1;",
				"  1,  455,   5005,    5005,        1;",
				"  1,  816,  18564,   48620,      816,       1;",
				"  1, 2024, 134596, 1307504,  1307504,  134596,    1;",
				"  1, 2925, 296010, 4686825, 17383860, 4686825, 2925, 1;"
			],
			"maple": [
				"seq(seq( `if`(k=0, 1, binomial(3*(ithprime(n+1)-1)/2, 3*(ithprime(k+1)-1)/2) ), k=0..n), n=0..10); # _G. C. Greubel_, Dec 02 2019"
			],
			"mathematica": [
				"T[n_, k_]:= If[k==0, 1, Binomial[3*(Prime[n+1] -1)/2, 3*(Prime[k+1] -1)/2]]; Table[T[n, k], {n,0,10}, {k,0,n}]//Flatten"
			],
			"program": [
				"(PARI) T(n,k) = if(k==0, 1, binomial(3*(prime(n+1)-1)/2, 3*(prime(k+1)-1)/2) ); \\\\ _G. C. Greubel_, Dec 02 2019",
				"(MAGMA) [k eq 0 select 1 else Round( Gamma((3*NthPrime(n+1)-1)/2)/( Gamma((3*NthPrime(k+1)-1)/2)*Gamma((3*NthPrime(n+1)-3*NthPrime(k+1))/2 + 1) ) ): k in [0..n], n in [0..10]]; // _G. C. Greubel_, Dec 02 2019",
				"(Sage)",
				"def T(n, k):",
				"    if (k==0): return 1",
				"    else: return binomial(3*(nth_prime(n+1)-1)/2, 3*(nth_prime(k+1)-1)/2)",
				"[[T(n, k) for k in (0..n)] for n in (0..10)] # _G. C. Greubel_, Dec 02 2019"
			],
			"xref": [
				"Cf. A154653."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Roger L. Bagula_, Jan 13 2009",
			"ext": [
				"Edited by _G. C. Greubel_, Dec 02 2019"
			],
			"references": 2,
			"revision": 17,
			"time": "2019-12-05T04:34:49-05:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}