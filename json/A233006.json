{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A233006",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 233006,
			"data": "1,1,0,1,0,0,2,1,0,1,1,0,3,2,0,3,1,0,5,3,0,5,2,0,8,5,0,8,4,0,12,7,0,12,6,0,19,11,0,19,9,0,27,15,0,28,14,0,39,22,0,41,20,0,55,31,0,58,29,0,77,43,0,82,41,0,106,58,0,113,57,0,145,80,0,156",
			"name": "Expansion of psi(x) / f(-x^6) in powers of x where psi(), f() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A233006/b233006.txt\"\u003eTable of n, a(n) for n = 0..2500\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(-1/8) * eta(q^2)^2 / (eta(q) * eta(q^6)) in powers of q.",
				"Euler transform of period 6 sequence [ 1, -1, 1, -1, 1, 0, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (1152 t)) = (3/2)^(1/2) g(t) where q = exp(2 Pi i t) and g() is the g.f. of A070047.",
				"G.f.: Product_{k\u003e0} (1 + x^k) / (1 + x^(2*k) + x^(4*k)).",
				"a(3*n) = A070047(n). a(3*n + 1) = A097451(n). a(3*n + 2) = 0."
			],
			"example": [
				"G.f. = 1 + x + x^3 + 2*x^6 + x^7 + x^9 + x^10 + 3*x^12 + 2*x^13 + 3*x^15 + ...",
				"G.f. = q + q^9 + q^25 + 2*q^49 + q^57 + q^73 + q^81 + 3*q^97 + 2*q^105 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 2, 0, x^(1/2)] / (2 x^(1/8) QPochhammer[ x^6]), {x, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A)^2 / (eta(x + A) * eta(x^6 + A)), n))};"
			],
			"xref": [
				"Cf. A070047, A097451."
			],
			"keyword": "nonn",
			"offset": "0,7",
			"author": "_Michael Somos_, Dec 03 2013",
			"references": 2,
			"revision": 13,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2013-12-03T15:26:53-05:00"
		}
	]
}