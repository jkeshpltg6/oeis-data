{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A238289",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 238289,
			"data": "2,2,17,4,1,1,59,3,56,1,39,10,9,130,2,18,11,7,80,67,2,19,27,17,92,73,180,65,5,110,282,4,6,8,16,2,23,198,20,3,99,83,217,13,110,28,16,6,5,3,144,31,9,187,176,145,75,11,43,424,4,54,272,8,26,131,123,107,8,4,52,9,127,84,264,33,145,663,16,285",
			"name": "Least positive integer k such that prime(k*n), prime((k+1)*n) and prime((k+2)*n) form an arithmetic progression, or 0 if such a number k does not exist.",
			"comment": [
				"Conjecture: (i) We always have 0 \u003c a(n) \u003c 3*prime(n) + 9.",
				"(ii) For any integer n \u003e 3, there is a positive integer k \u003c 5*n^3 such that prime(k*n), prime((k+1)*n), prime((k+2)*n), and prime((k+3)*n) form a 4-term arithmetic progression.",
				"(iii) In general, for each m = 3, 4, ..., if n is sufficiently large then there is a positive integer k = O(n^(m-1)) such that prime((k+j)*n) (j = 0, ..., m-1) form an arithmetic progression.",
				"The conjecture is a refinement of the Green-Tao theorem."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A238289/b238289.txt\"\u003eTable of n, a(n) for n = 1..8000\u003c/a\u003e",
				"B. Green and T. Tao, \u003ca href=\"http://dx.doi.org/10.4007/annals.2008.167.481\"\u003eThe primes contain arbitrarily long arithmetic progressions\u003c/a\u003e, Annals of Math. 167(2008), 481-547.",
				"Z.-W. Sun, \u003ca href=\"http://arxiv.org/abs/1402.6641\"\u003eProblems on combinatorial properties of primes\u003c/a\u003e, arXiv:1402.6641, 2014"
			],
			"example": [
				"a(2) = 2 since prime(2*2) = 7, prime(3*2) = 13 and prime(4*2) = 19 form a 3-term arithmetic progression, but prime(1*2) = 3,  prime(2*2) = 7 and prime(3*2) = 13 do not form a 3-term arithmetic progression."
			],
			"mathematica": [
				"d[k_,n_]:=Prime[(k+1)*n]-Prime[k*n]",
				"Do[Do[If[d[k,n]==d[k+1,n],Print[n,\" \",k];Goto[aa]],{k,1,3*Prime[n]+8}];",
				"Print[n,\" \",0];Label[aa];Continue,{n,1,80}]"
			],
			"program": [
				"(PARI) okpr(p, q, r) = (q - p) == (r - q);",
				"a(n) = {k = 1; while(! okpr(prime(k*n), prime((k+1)*n), prime((k+2)*n)), k++); k;} \\\\ _Michel Marcus_, Aug 28 2014"
			],
			"xref": [
				"Cf. A000040, A238277, A238278, A238281."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Zhi-Wei Sun_, Feb 22 2014",
			"references": 2,
			"revision": 15,
			"time": "2014-08-28T09:20:55-04:00",
			"created": "2014-02-22T12:00:42-05:00"
		}
	]
}