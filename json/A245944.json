{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A245944",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 245944,
			"data": "5,8,6,6,5,8,6,7,14,9,20,12,8,6,8,6,9,20,12,8,6,10,7,14,9,20,12,8,6,11,26,15,38,21,56,30,17,44,24,14,9,20,12,8,6,12,8,6,13,32,18,11,26,15,38,21,56,30,17,44,24,14,9,20,12,8,6,14,9,20,12,8,6",
			"name": "Irregular triangle read by rows of a variation of the Collatz iteration with signature (2,4).",
			"comment": [
				"It is conjectured that the trajectory of this Collatz-like iteration arrives at 6 in a finite number of steps for any initial value x, (x\u003e4). The iterative step is divide by 2 and add 2 if even, or multiply by 3 and subtract 7 if odd. For any even initial value the number of steps in the trajectory is the same as the number of steps in the Collatz trajectory starting with floor(x-4)/2.",
				"This is one of a subset of Collatz-like variations with parameters a = 2 and b = (any positive or negative even integer). The halting value h for type (a=2, b:even) is given by h = 2 + b. Any even halting value can be chosen by selecting the appropriate value for b. For any sequence starting with an even number x, the halting value is arrived at in the same number of steps as the Collatz trajectory starting with (x-b)/2. The iterative function for variation type (a=2, b:even) is x -\u003e (x/2 + b/2) if x is even or x -\u003e (3*x - 2*b + 1) if x is odd.",
				"Two variations belong to the same subset if their (a) parameters are the same and their (b) parameters have the same parity. It is conjectured that any variations belonging to the same subset have equal row lengths.",
				"The subset is part of a wider class of Collatz variations uniquely identified by two parameters (a,b) where a or b can be any integer. The general formula for the halting value is h = 6^(b mod 2)*a + b + b mod 2; the general formula for the iterative mapping function is x -\u003e (x/2 + ceiling(b/2)) if x is even and x -\u003e (3*x - 2*b + a^(a mod 2)) if x is odd. The minimum starting value is b + 1 + b mod 2 for a = 1 or a = 2. Values of a other than 1 or 2 are not always \"well behaved\"."
			],
			"example": [
				"Some initial rows of the irregular array (r,j):",
				"r: j = (1, 2, 3, ... )",
				"1: (5, 8, 6),",
				"2: (6, 5, 8, 6),",
				"3: (7, 14, 9, 20, 12, 8, 6),",
				"4: (8, 6),",
				"5: (9, 20, 12, 8, 6),",
				"6: (10, 7, 14, 9, 20, 12, 8, 6),",
				"7: (11, 26, 15, 38, 21, 56, 30, 17, 44, 24, 14, 9, 20, 12, 8, 6),",
				"8: (12, 8, 6),",
				"9: (13, 32, 18, 11, 26, 15, 38, 21, 56, 30, 17, 44, 24, 14, 9, 20, 12, 8, 6),",
				"10: (14, 9, 20, 12, 8, 6),",
				"11: (15, 38, 21, 56, 30, 17, 44, 24, 14, 9, 20, 12, 8, 6),",
				"12: (16, 10, 7, 14, 9, 20, 12, 8, 6)"
			],
			"program": [
				"(PARI) {for(n=5, 16, x=n; print1(x,\", \"); until(x==6, if(x%2,x=x*3-7,x=x/2+2);print1(x,\", \")))} \\\\ Prints flattened triangle.",
				"(PARI) variation(a,b) = {if(!(a==1||a==2), print(\"Enter a=1 or a=2\"), h=6^(b%2)*a+b+b%2; c=ceil(b/2); d=2*-b+a^(a%2); for(r=1,12, x=r+b+b%2; print1(r,\": (\",x); until(x==h, if(x%2, x=3*x+d, x=x/2+c); print1(\", \",x)); print(\"),\")))} \\\\ Generalized version.",
				"{variation(2,4)} \\\\ Prints first 12 rows of this irregular array."
			],
			"xref": [
				"Cf. A245942 for variation type (a=2, b:odd).",
				"Cf. A245943 for variation type (a=1, b:even).",
				"Cf. A242030 for variation type (a=1, b:odd)."
			],
			"keyword": "nonn,tabf",
			"offset": "1,1",
			"author": "_K. Spage_, Aug 11 2014",
			"references": 4,
			"revision": 9,
			"time": "2015-10-03T18:36:02-04:00",
			"created": "2014-08-12T12:46:13-04:00"
		}
	]
}