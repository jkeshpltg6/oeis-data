{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A274106",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 274106,
			"data": "1,1,2,1,4,2,1,8,14,4,1,12,38,32,4,1,18,98,184,100,8,1,24,188,576,652,208,8,1,32,356,1704,3532,2816,632,16,1,40,580,3840,12052,16944,9080,1280,16,1,50,940,8480,38932,89256,93800,37600,3856,32,1,60,1390,16000,98292,322848,540080,412800,116656,7744,32",
			"name": "Triangle read by rows: T(n,k) = number of configurations of k non-attacking bishops on the white squares of an n X n chessboard (0 \u003c= k \u003c n).",
			"link": [
				"Irving Kaplansky and John Riordan, \u003ca href=\"http://projecteuclid.org/euclid.dmj/1077473616\"\u003eThe problem of the rooks and its applications\u003c/a\u003e, Duke Mathematical Journal 13.2 (1946): 259-268. See Section 9.",
				"Irving Kaplansky and John Riordan, \u003ca href=\"/A274105/a274105.pdf\"\u003eThe problem of the rooks and its applications\u003c/a\u003e, in Combinatorics, Duke Mathematical Journal, 13.2 (1946): 259-268. See Section 9. [Annotated scanned copy]",
				"J. Perott, \u003ca href=\"https://doi.org/10.24033/bsmf.267\"\u003eSur le problème des fous\u003c/a\u003e, Bulletin de la S. M. F., tome 11 (1883), pp. 173-186.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/WhiteBishopGraph.html\"\u003eWhite Bishop Graph\u003c/a\u003e."
			],
			"example": [
				"Triangle begins:",
				"  1;",
				"  1,  2;",
				"  1,  4,    2;",
				"  1,  8,   14,     4;",
				"  1, 12,   38,    32,     4;",
				"  1, 18,   98,   184,   100,      8;",
				"  1, 24,  188,   576,   652,    208,      8;",
				"  1, 32,  356,  1704,  3532,   2816,    632,     16;",
				"  1, 40,  580,  3840, 12052,  16944,   9080,   1280,     16;",
				"  1, 50,  940,  8480, 38932,  89256,  93800,  37600,   3856,   32;",
				"  1, 60, 1390, 16000, 98292, 322848, 540080, 412800, 116656, 7744, 32;",
				"  ..."
			],
			"maple": [
				"with(combinat): with(gfun):",
				"T := n -\u003e add(stirling2(n+1,n+1-k)*x^k, k=0..n):",
				"# bishops on white squares",
				"bish := proc(n) local m,k,i,j,t1,t2; global T;",
				"    if (n mod 2) = 0 then m:=n/2;",
				"        t1:=add(binomial(m,k)*T(2*m-1-k)*x^k, k=0..m);",
				"    else",
				"        m:=(n-1)/2;",
				"        t1:=add(binomial(m,k)*T(2*m-k)*x^k, k=0..m+1);",
				"    fi;",
				"    seriestolist(series(t1,x,2*n+1));",
				"end:",
				"for n from 1 to 12 do lprint(bish(n)); od:"
			],
			"xref": [
				"Alternate rows give A088960.",
				"Row sums are A216078(n+1).",
				"Cf. A274105 (black squares), A288182, A201862, A002465."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_, Jun 14 2016",
			"references": 3,
			"revision": 25,
			"time": "2019-08-06T06:14:56-04:00",
			"created": "2016-06-14T12:41:51-04:00"
		}
	]
}