{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A324886",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 324886,
			"data": "2,3,5,9,7,25,11,15,35,49,13,625,17,121,117649,225,19,1225,23,2401,1771561,169,29,875,717409,289,55,14641,31,184877,37,21,4826809,361,36226650889,1500625,41,529,24137569,77,43,143,47,28561,1127357,841,53,1715,902613283,514675673281,47045881,83521,59,3025,8254129,214358881,148035889,961,61",
			"name": "a(n) = A276086(A108951(n)).",
			"link": [
				"Antti Karttunen, \u003ca href=\"/A324886/b324886.txt\"\u003eTable of n, a(n) for n = 1..2310\u003c/a\u003e",
				"\u003ca href=\"/index/Pri#prime_indices\"\u003eIndex entries for sequences computed from indices in prime factorization\u003c/a\u003e",
				"\u003ca href=\"/index/Pri#primorialbase\"\u003eIndex entries for sequences related to primorial base\u003c/a\u003e",
				"\u003ca href=\"/index/Pri#primorial_numbers\"\u003eIndex entries for sequences related to primorial numbers\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A276086(A108951(n)).",
				"a(n) = A117366(n) * A324896(n).",
				"A001222(a(n)) = A324888(n).",
				"A020639(a(n)) = A117366(n).",
				"A032742(a(n)) = A324896(n).",
				"a(A000040(n)) = A000040(1+n).",
				"From _Antti Karttunen_, Jul 09 2021: (Start)",
				"For n \u003e 1, a(n) = A003961(A329044(n)).",
				"a(n) = A346091(n) * A344592(n).",
				"a(n) = A346106(n) /  A346107(n).",
				"A003415(a(n)) = A329047(n).",
				"A003557(a(n)) = A344592(n).",
				"A342001(a(n)) = A342920(n) = A329047(n) / A344592(n).",
				"(End)"
			],
			"mathematica": [
				"With[{b = MixedRadix[Reverse@ Prime@ Range@ 120]}, Array[Function[k, Times @@ Power @@@ # \u0026@ Transpose@ {Prime@ Range@ Length@ k, Reverse@ k}]@ IntegerDigits[Apply[Times, Map[#1^#2 \u0026 @@ # \u0026, FactorInteger[#] /. {p_, e_} /; e \u003e 0 :\u003e {Times @@ Prime@ Range@ PrimePi@ p, e}]], b] \u0026, 58]] (* _Michael De Vlieger_, Nov 18 2019 *)",
				"A276086[n0_] := Module[{m = 1, i = 1, n = n0, p}, While[n \u003e 0, p = Prime[i]; m *= p^Mod[n, p]; n = Quotient[n, p]; i++]; m];",
				"(* b is A108951 *)",
				"b[n_] := b[n] = Module[{pe = FactorInteger[n], p, e}, If[Length[pe] \u003e 1, Times @@ b /@ Power @@@ pe, {{p, e}} = pe; Times @@ (Prime[Range[ PrimePi[p]]]^e)]]; b[1] = 1;",
				"a[n_] := A276086[b[n]];",
				"Array[a, 100] (* _Jean-François Alcover_, Dec 01 2021, after _Antti Karttunen in A296086 *)"
			],
			"program": [
				"(PARI)",
				"A034386(n) = prod(i=1, primepi(n), prime(i));",
				"A108951(n) = { my(f=factor(n)); prod(i=1, #f~, A034386(f[i, 1])^f[i, 2]) };  \\\\ From A108951",
				"A276086(n) = { my(i=0,m=1,pr=1,nextpr); while((n\u003e0),i=i+1; nextpr = prime(i)*pr; if((n%nextpr),m*=(prime(i)^((n%nextpr)/pr));n-=(n%nextpr));pr=nextpr); m; };",
				"A324886(n) = A276086(A108951(n));"
			],
			"xref": [
				"Cf. A034386, A108951, A117366, A276086, A324887, A324888, A324896, A329047, A342920, A344592, A346106, A346107.",
				"Permutation of A324576.",
				"Cf. also A346105."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Antti Karttunen_, Mar 30 2019",
			"references": 46,
			"revision": 25,
			"time": "2021-12-01T11:36:58-05:00",
			"created": "2019-03-31T00:17:46-04:00"
		}
	]
}