{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A284861",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 284861,
			"data": "1,1,3,1,15,18,1,63,216,162,1,255,1890,3564,1944,1,1023,14760,52650,68040,29160,1,4095,109458,659340,1516320,1487160,524880,1,16383,790776,7578522,27624240,46539360,36741600,11022480,1,65535,5633730,82902204,450057384,1158993360,1535798880,1014068160,264539520",
			"name": "Triangle read by rows: T(n, k) = S2[3,1](n, k)*k! with the Sheffer triangle S2[3,1] = (exp(x), exp(3*x) -1) given in A282629.",
			"comment": [
				"This is a generalization of triangle A131689(n, k) = Stirling2(n, k)*k!, because S2[3,1] is a generalization of the Stirling2 triangle written as S2[1,0].",
				"This triangle appears in the o.g.f. G(3,1;n,x) of the powers {(1+3*m)^n}_{m\u003e=0} as  G(3,1;n,x) = Sum_{k\u003e=0..n} T(n, k)*x^k / (1-x)^k.",
				"This triangle is also related to the generalized row reversed Euler triangle rEu[3,1] with row polynomial rEu(3,1;n,x) = Sum_{m=0..n} rEu(3,1;n,m)*x^m with rEu(3,1;n,m) = Sum_{j=0..m} (-1)^(m-j)*binomial(n-j, m-j)*T(n, m). This follows from the above given o.g.f. of powers G(3,1;n,x) = rEu(3,1;n,x)/(1-x)^(n+1). The Euler triangle E[3,1] (row reversed rEu[3,1] is given in A225117. See a formula below.",
				"The e.g.f. of the row polynomials R(3,1;n,x) = Sum_{m=0..n} T(n, m)*x^m follows from the e.g.f. of the row polynomials of the Sheffer triangle A282629. See the formula section.",
				"The diagonal sequence is A032031(k) = k!*3^k.",
				"The row sums give unsigned A151919, and the alternating row sums give A122803.",
				"The first column k sequences divided by A032031(k) are A000012, A002450 (with a leading 0), A016223, A021874. For the e.g.f.s and o.g.f.s see below."
			],
			"link": [
				"P. Bala, \u003ca href=\"/A131689/a131689.pdf\"\u003eDeformations of the Hadamard product of power series\u003c/a\u003e",
				"M. Dukes, C. D. White, \u003ca href=\"http://arxiv.org/abs/1603.01589\"\u003eWeb Matrices: Structural Properties and Generating Combinatorial Identities\u003c/a\u003e, arXiv:1603.01589 [math.CO], 2016.",
				"Wolfdieter Lang, \u003ca href=\"https://arxiv.org/abs/1707.04451\"\u003eOn Sums of Powers of Arithmetic Progressions, and Generalized Stirling, Eulerian and Bernoulli numbers\u003c/a\u003e, arXiv:1707.04451 [math.NT], 2017."
			],
			"formula": [
				"E.g.f. of the row polynomials R(n, x) (see a comment above) is exp(z)/(1 - x*(exp(3*z) - 1)). This is the e.g.f. for the triangle.",
				"T(n, k) = Sum_{m=0..k} binomial(k, m)*(-1)^(k-m)*(1+ 3*m)^n, 0 \u003c= k \u003c= n.",
				"T(n, k) = Sum_{m=0..k} binomial(n-m, k-m)*A225117(n,n-m), 0 \u003c= k \u003c= n.",
				"Three term recurrence: T(n, k) = 0 if n \u003c k, T(n,-1) = 0, T(0, 0) = 1, T(n, k) = 3*k*T(n-1, k-1) + (1+3*k)*T(n-1, k) for  n \u003e= 1. See A282629.",
				"The column k sequence has e.g.f. exp(x)*(exp(3*x) - 1)^k (from the Sheffer property of A282629).",
				"  The o.g.f. is A032031(k)*x^k/Product_{j=0..k} (1 - (1+3*j)*x).",
				"From _Peter Bala_, Jan 12 2018: (Start)",
				"n-th row polynomial R(n,x) = (1 + 3*x) o (1 + 3*x) o ... o (1 + 3*x) (n factors), where o denotes the black diamond multiplication operator of Dukes and White. See example E14 in the Bala link. Cf. A145901.",
				"R(n,x) = Sum_{k = 0..n} binomial(n,k)*3^k*F(k,x) where F(k,x) is the Fubini polynomial of order k, the k-th row polynomial of A019538. (End)"
			],
			"example": [
				"The triangle T(n, k) begins",
				"n\\k 0     1      2       3        4        5        6        7 ...",
				"0:  1",
				"1:  1     3",
				"2:  1    15     18",
				"3:  1    63    216     162",
				"4:  1   255   1890    3564     1944",
				"5:  1  1023  14760   52650    68040    29160",
				"6:  1  4095 109458  659340  1516320  1487160   524880",
				"7:  1 16383 790776 7578522 27624240 46539360 36741600 11022480",
				"...",
				"row n=8: 1 65535 5633730 82902204 450057384 1158993360 1535798880 1014068160 264539520,",
				"row n=9: 1 262143 39829320 879725610 6845572440 25294754520 50042059200 54561276000 30951123840 7142567040,",
				"row n=10: 1 1048575 280378098 9155719980 99549149040 507399658920 1406104706160 2251231315200 2083248720000 1035672220800 214277011200.",
				"------------------------------------------------------------------",
				"T(2, 1) =  -1 + 4^2 = 15 = 2*A225117(2,2) + 1*A225117(2,1) = 2*1 + 1*13."
			],
			"mathematica": [
				"Table[Sum[Binomial[k, m] (-1)^(k - m) (1 + 3m)^n, {m, 0, k}], {n, 0, 10}, {k, 0, n}]// Flatten (* _Indranil Ghosh_, Apr 09 2017 *)"
			],
			"program": [
				"(PARI) for(n=0, 10, for(k=0, n, print1(sum(m=0, k, binomial(k, m) * (-1)^(k - m)*(1 + 3*m)^n),\", \"); ); print();) \\\\ _Indranil Ghosh_, Apr 09 2017",
				"(Python) # _Indranil Ghosh_, Apr 09 2017",
				"from sympy import binomial",
				"for n in range(11):",
				"    print([sum([binomial(k, m)*(-1)**(k - m)*(1 + 3*m)**n for m in range(k + 1)]) for k in range(n + 1)])"
			],
			"xref": [
				"Cf. A000012, A002450, A016223, A021874, A032031, A111577, A122803, A131689, |A151919|, A225117, A282629, A019538, A145901."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "0,3",
			"author": "_Wolfdieter Lang_, Apr 09 2017",
			"references": 7,
			"revision": 37,
			"time": "2019-11-30T15:07:00-05:00",
			"created": "2017-04-09T13:38:54-04:00"
		}
	]
}