{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A084703",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 84703,
			"data": "0,4,144,4900,166464,5654884,192099600,6525731524,221682772224,7530688524100,255821727047184,8690408031080164,295218051329678400,10028723337177985444,340681375412721826704,11573138040695364122500",
			"name": "Squares n such that 2*n+1 is also a square.",
			"comment": [
				"With the exception of 0, a subsequence of A075114. - _R. J. Mathar_, Dec 15 2008",
				"Consequently, A014105(k) is a square if and only if k = a(n). - _Bruno Berselli_, Oct 14 2011",
				"From _M. F. Hasler_, Jan 17 2012: (Start)",
				"Bisection of A079291. The squares 2*n+1 are given in A055792.",
				"A204576 is this sequence written in binary. (End)",
				"a(n+1),n \u003e= 0, is the perimeter squared (x(n)+y(n)+z(n))^2 of the ordered primitive Pythagorean triple (x(n), y(n)=x(n)+1, z(n)). The first two terms are (x(0)=0, y(0)=1, z(0)=1), a(1) = 2^2, and (x(1)=3, y(1)=4, z(1)=5), a(2) = 12^2. - _George F. Johnson_, Nov 02 2012"
			],
			"link": [
				"D. W. Wilson, \u003ca href=\"/A084703/a084703.txt\"\u003eTable of n, a(n-1) for n = 1..100\u003c/a\u003e (offset=1)",
				"E. Kilic, Y. T. Ulutas, N. Omur, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL14/Omur/omur6.html\"\u003eA Formula for the Generating Functions of Powers of Horadam's Sequence with Two Additional Parameters\u003c/a\u003e, J. Int. Seq. 14 (2011) #11.5.6, table 3, k=2.",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (35,-35,1)."
			],
			"formula": [
				"a(n) = 4*A001110(n) = A001542(n)^2.",
				"a(n+1) = A001652(n)*A001652(n+1) + A046090(n)*A046090(n+1) = A001542(n+1)^2. - _Charlie Marion_, Jul 01 2003",
				"For k\u003e=n\u003e=0, a(n) = A001653(k+n)*A001653(k-n) - A001653(k)^2; e.g. 144 = 5741*5 - 169^2. - _Charlie Marion_, Jul 16 2003",
				"G.f.: 4*x*(1+x)/((1-x)*(1-34*x+x^2)). - _R. J. Mathar_, Dec 15 2008",
				"a(n) = A079291(2n). - _M. F. Hasler_, Jan 16 2012",
				"From _George F. Johnson_, Nov 02 2012: (Start)",
				"a(n) = ((17+12*sqrt(2))^n + (17-12*sqrt(2))^n - 2)/8.",
				"a(n+1) = 17*a(n) + 4 + 12*sqrt(a(n)*(2*(a(n) + 1)).",
				"a(n-1) = 17*a(n) + 4 - 12*sqrt(a(n)*(2*(a(n) + 1)).",
				"a(n-1)*a(n+1) = (a(n) - 4)^2.",
				"2*a(n) + 1 = (A001541(n))^2.",
				"a(n+1) = 34*a(n) - a(n-1) + 8 for n\u003e1, a(0)=0, a(1)=4.",
				"a(n+1) = 35*a(n) - 35*a(n-1) + a(n-2) for n\u003e0, a(0)=0, a(1)=4, a(2)=144.",
				"a(n)*a(n+1) = (4*A029549(n))^2.",
				"a(n+1) - a(n) = 4*A046176(n).",
				"a(n) + a(n+1) = 4*(6*A029549(n) + 1).",
				"a(n) = (2*A001333(n)*A000129(n))^2.",
				"Lim_{n -\u003e infinity} a(n)/a(n-r) = (17+12*sqrt(2))^r.",
				"(End)",
				"Empirical: for n\u003e0, a(n) = A089928(4*n-2). - _Alex Ratushnyak_, Apr 12 2013"
			],
			"mathematica": [
				"a[0] = 0; a[1] = 1; a[n_] := 34a[n - 1] - a[n - 2] + 2; Table[ 4a[n], {n, 0, 15}]"
			],
			"xref": [
				"Cf. A001110, A055792, A075114, A079291, A084702, A204576.",
				"Cf. similar sequences with closed form ((1 + sqrt(2))^(4*r) + (1 - sqrt(2))^(4*r))/8 + k/4: this sequence (k=-1), A076218 (k=3), A278310 (k=-5)."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Amarnath Murthy_, Jun 08 2003",
			"ext": [
				"Edited and extended by _Robert G. Wilson v_, Jun 15 2003"
			],
			"references": 12,
			"revision": 41,
			"time": "2017-08-21T12:53:21-04:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}