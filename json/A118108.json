{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A118108",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 118108,
			"data": "1,7,17,119,273,1911,4369,30583,69905,489335,1118481,7829367,17895697,125269879,286331153,2004318071,4581298449,32069089143,73300775185,513105426295,1172812402961,8209686820727,18764998447377,131354989131639,300239975158033",
			"name": "Decimal representation of n-th iteration of the Rule 54 elementary cellular automaton starting with a single black cell.",
			"comment": [
				"a(1660) is 1000 digits long. - _Michael De Vlieger_, Oct 07 2015"
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A118108/b118108.txt\"\u003eTable of n, a(n) for n = 0..1660\u003c/a\u003e",
				"A. J. Macfarlane, \u003ca href=\"http://www.damtp.cam.ac.uk/user/ajm/Papers2016/GFsForCAsOfEvenRuleNo.ps\"\u003eGenerating functions for integer sequences defined by the evolution of cellular automata...\u003c/a\u003e, Fig 8.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Rule54.html\"\u003eRule 54\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ElementaryCellularAutomaton.html\"\u003eElementary Cellular Automaton\u003c/a\u003e",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,17,0,-16)."
			],
			"formula": [
				"a(n) = 7*(4^(n+1)-1)/15 for n odd; a(n) = (4^(n+2)-1)/15 for n even.",
				"From _Colin Barker_, Oct 08 2015 and Apr 16 2019: (Start)",
				"a(n) = 17*a(n-2) - 16*a(n-4) for n\u003e3.",
				"G.f.: (7*x+1) / ((x-1)*(x+1)*(4*x-1)*(4*x+1)).",
				"(End)",
				"a(n) = floor((16+12*(n mod 2))*4^n/15). - _Karl V. Keller, Jr._, Aug 04 2021"
			],
			"example": [
				"From _Michael De Vlieger_, Oct 07 2015: (Start)",
				"First 8 rows, representing ON cells as \"1\", OFF cells within the bounds of ON cells as \"0\", interpreted as a binary number at left, the decimal equivalent appearing at right:",
				"                    1 =     1",
				"                  111 =     7",
				"               1 0001 =    17",
				"             111 0111 =   119",
				"          1 0001 0001 =   273",
				"        111 0111 0111 =  1911",
				"     1 0001 0001 0001 =  4369",
				"   111 0111 0111 0111 = 30583",
				"1 0001 0001 0001 0001 = 69905",
				"(End)"
			],
			"mathematica": [
				"clip[lst_] := Block[{p = Flatten@ Position[lst, 1]}, Take[lst, {Min@ p, Max@ p}]]; FromDigits[#, 2] \u0026 /@ Map[clip, CellularAutomaton[54, {{1}, 0}, 27]] (* or *)",
				"Table[If[EvenQ@ n, (4^(n + 2) - 1), 7 (4^(n + 1) - 1)]/15, {n, 0, 27}] (* _Michael De Vlieger_, Oct 07 2015 *)"
			],
			"program": [
				"(Python) print([(16+12*(n%2))*4**n//15 for n in range(30)]) # _Karl V. Keller, Jr._, Aug 04 2021"
			],
			"xref": [
				"See A071030, A118109 for two other versions of this sequence."
			],
			"keyword": "nonn,base,easy",
			"offset": "0,2",
			"author": "_Eric W. Weisstein_, Apr 13 2006",
			"ext": [
				"a(23)-a(24) from _Michael De Vlieger_, Oct 07 2015"
			],
			"references": 6,
			"revision": 44,
			"time": "2021-08-08T09:36:18-04:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}