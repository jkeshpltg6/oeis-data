{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A101035",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 101035,
			"data": "1,-3,-5,1,-9,15,-13,1,4,27,-21,-5,-25,39,45,1,-33,-12,-37,-9,65,63,-45,-5,16,75,4,-13,-57,-135,-61,1,105,99,117,4,-73,111,125,-9,-81,-195,-85,-21,-36,135,-93,-5,36,-48,165,-25,-105,-12,189,-13,185,171,-117,45,-121,183,-52,1,225,-315,-133,-33,225,-351,-141,4",
			"name": "Dirichlet inverse of the gcd-sum function (A018804).",
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A101035/b101035.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"G. P. Michon, \u003ca href=\"http://www.numericana.com/answer/numbers.htm#multiplicative\"\u003eMultiplicative Functions\u003c/a\u003e."
			],
			"formula": [
				"Multiplicative function with a(p)=1-2p and a(p^e)=(p-1)^2 when e\u003e1 [p prime].",
				"Dirichlet g.f.: zeta(s)/zeta^2(s-1). - _R. J. Mathar_, Apr 10 2011",
				"a(n) = Sum{d|n} tau_{-2}(d)*d, where tau_{-2} is A007427. - _Enrique Pérez Herrero_, Jan 19 2013",
				"Conjecture: Logarithmic g.f. Sum_{n\u003e0,k\u003e0} mu(n)*mu(k)*log(1/(1-x^(n*k))). - _Benedict W. J. Irwin_, Jul 26 2017"
			],
			"example": [
				"a(4)=1, a(8)=1, a(16)=1, a(32)=1, etc. because of the multiplicative definition for powers of 2."
			],
			"mathematica": [
				"DirichletInverse[f_][1] = 1/f[1]; DirichletInverse[f_][n_] := DirichletInverse[f][n] = -1/f[1]*Sum[ f[n/d]*DirichletInverse[f][d], {d, Most[ Divisors[n]]}]; GCDSum[n_] := Sum[ GCD[n, k], {k, 1, n}]; Table[ DirichletInverse[ GCDSum][n], {n, 1, 72}](* _Jean-François Alcover_, Dec 12 2011 *)"
			],
			"program": [
				"(Haskell)",
				"a101035 n = product $ zipWith f (a027748_row n) (a124010_row n) where",
				"   f p 1 = 1 - 2 * p",
				"   f p e = (p - 1) ^ 2",
				"-- _Reinhard Zumkeller_, Jul 16 2012",
				"(PARI) seq(n)={dirdiv(vector(n, n, n==1), vector(n, n, sumdiv(n, d, n*eulerphi(d)/d)))} \\\\ _Andrew Howroyd_, Aug 05 2018",
				"(PARI) for(n=1, 100, print1(direuler(p=2, n, (1 - p*X)^2/(1 - X))[n], \", \")) \\\\ _Vaclav Kotesovec_, Aug 22 2021"
			],
			"xref": [
				"Cf. A018804, A055615, A046692, A023900, A007427, A053822, A053825, A053826.",
				"Cf. A008683."
			],
			"keyword": "easy,nice,sign,mult",
			"offset": "1,2",
			"author": "_Gerard P. Michon_, Nov 27 2004",
			"references": 9,
			"revision": 33,
			"time": "2021-08-22T05:30:16-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}