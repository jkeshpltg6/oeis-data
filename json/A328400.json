{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A328400",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 328400,
			"data": "1,2,2,4,2,2,2,8,4,2,2,12,2,2,2,16,2,12,2,12,2,2,2,24,4,2,8,12,2,2,2,32,2,2,2,4,2,2,2,24,2,2,2,12,12,2,2,48,4,12,2,12,2,24,2,24,2,2,2,12,2,2,12,64,2,2,2,12,2,2,2,72,2,2,12,12,2,2,2,48,16,2,2,12,2,2,2,24,2,12,2,12,2,2,2,96,2,12,12,4,2,2,2,24,2",
			"name": "Smallest number with the same set of distinct prime exponents as n.",
			"comment": [
				"A variant of A046523 which gives the smallest number with the same prime signature as n. However, in this sequence, if any prime exponent occurs multiple times in n, the extra occurrences are removed and the signature is that of one of the numbers where only distinct values of prime exponents occur (A130091)."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A328400/b328400.txt\"\u003eTable of n, a(n) for n = 1..16384\u003c/a\u003e",
				"\u003ca href=\"/index/Eu#epf\"\u003eIndex entries for sequences computed from exponents in factorization of n\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A181821(A007947(A181819(n))).",
				"For all n, a(n) = a(A046523(n))."
			],
			"example": [
				"90 = 2^1 * 3^2 * 5^1 has prime signature (1,1,2). The smallest number with prime signature (1,2) is 12 = 2^2 * 3, thus a(90) = 12."
			],
			"mathematica": [
				"Array[Times @@ MapIndexed[Prime[#2[[1]]]^#1 \u0026, Reverse[Flatten[Cases[FactorInteger[#], {p_, k_} :\u003e Table[PrimePi[p], {k}]]]]] \u0026[Times @@ FactorInteger[#][[All, 1]]] \u0026@ If[# == 1, 1, Times @@ Prime@ FactorInteger[#][[All, -1]]] \u0026, 105] (* _Michael De Vlieger_, Oct 17 2019, after _Gus Wiseman_ at A181821 *)"
			],
			"program": [
				"(PARI)",
				"A007947(n) = factorback(factorint(n)[, 1]);",
				"A181819(n) = factorback(apply(e-\u003eprime(e),(factor(n)[,2])));",
				"A181821(n) = { my(f=factor(n),p=0,m=1); forstep(i=#f~,1,-1,while(f[i,2], f[i,2]--; m *= (p=nextprime(p+1))^primepi(f[i,1]))); (m); };",
				"A328400(n) = A181821(A007947(A181819(n)));"
			],
			"xref": [
				"Cf. A007947, A046523, A181819, A181821, A328401 (rgs-transform).",
				"Cf. A005117 (gives indices of terms \u003c= 2), A062503 (after its initial 1, gives indices of 4's in this sequence)."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Antti Karttunen_, Oct 15 2019",
			"references": 7,
			"revision": 18,
			"time": "2019-10-18T11:28:41-04:00",
			"created": "2019-10-18T11:28:41-04:00"
		}
	]
}