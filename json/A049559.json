{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A049559",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 49559,
			"data": "1,1,2,1,4,1,6,1,2,1,10,1,12,1,2,1,16,1,18,1,4,1,22,1,4,1,2,3,28,1,30,1,4,1,2,1,36,1,2,1,40,1,42,1,4,1,46,1,6,1,2,3,52,1,2,1,4,1,58,1,60,1,2,1,16,5,66,1,4,3,70,1,72,1,2,3,4,1,78,1,2,1,82,1,4,1,2,1,88,1,18,1,4",
			"name": "a(n) = gcd(n - 1, phi(n)).",
			"comment": [
				"For prime n, a(n) = n - 1. Question: do nonprimes exist with this property?",
				"Answer: No. If n is composite then a(n) \u003c n - 1. - _Charles R Greathouse IV_, Dec 09 2013",
				"Lehmer's totient problem (1932): are there composite numbers n such that a(n) = phi(n)? - _Thomas Ordowski_, Nov 08 2015",
				"a(n) = 1 for n in A209211. - _Robert Israel_, Nov 09 2015"
			],
			"reference": [
				"Richard K. Guy, Unsolved Problems in Number Theory, B37."
			],
			"link": [
				"Charles R Greathouse IV, \u003ca href=\"/A049559/b049559.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/LehmersTotientProblem.html\"\u003eLehmer's Totient Problem\u003c/a\u003e"
			],
			"formula": [
				"a(p^m) = a(p) = p - 1 for prime p and m \u003e 0. - _Thomas Ordowski_, Dec 10 2013",
				"From _Antti Karttunen_, Sep 09 2018: (Start)",
				"a(n) = A000010(n) / A160595(n) = A063994(n) / A318829(n).",
				"a(n) = n - A318827(n) = A000010(n) - A318830(n).",
				"(End)",
				"a(n) = gcd(A000010(n), A219428(n)) = gcd(A000010(n), A318830(n)). - _Antti Karttunen_, Jan 05 2021"
			],
			"example": [
				"a(9) = 2 because phi(9) = 6 and gcd(8, 6) = 2.",
				"a(10) = 1 because phi(10) = 4 and gcd(9, 4) = 1."
			],
			"maple": [
				"seq(igcd(n-1, numtheory:-phi(n)), n=1..100); # _Robert Israel_, Nov 09 2015"
			],
			"mathematica": [
				"Table[GCD[n - 1, EulerPhi[n]], {n, 93}] (* _Michael De Vlieger_, Nov 09 2015 *)"
			],
			"program": [
				"(PARI) a(n)=gcd(eulerphi(n),n-1) \\\\ _Charles R Greathouse IV_, Dec 09 2013",
				"(Python)",
				"from sympy import totient, gcd",
				"print([gcd(totient(n), n - 1) for n in range(1, 101)]) # _Indranil Ghosh_, Mar 27 2017",
				"(MAGMA) [Gcd(n-1, EulerPhi(n)): n in [1..80]]; // _Vincenzo Librandi_, Oct 13 2018"
			],
			"xref": [
				"Cf. A000010, A002322, A039766, A063994, A160595, A209211, A219428, A264012, A264024, A280262, A283656, A283872, A284089, A284440, A318827, A318829, A318830, A330747 (ordinal transform), A340195.",
				"Cf. also A009195, A058515, A058663, A187730, A258409, A339964, A340071, A340081, A340087 for more or less analogous sequences."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Labos Elemer_, Dec 28 2000",
			"references": 32,
			"revision": 77,
			"time": "2021-05-06T11:05:00-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}