{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A134013",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 134013,
			"data": "1,2,0,0,2,0,0,0,1,4,0,0,2,0,0,0,2,2,0,0,0,0,0,0,3,4,0,0,2,0,0,0,0,4,0,0,2,0,0,0,2,0,0,0,2,0,0,0,1,6,0,0,2,0,0,0,0,4,0,0,2,0,0,0,4,0,0,0,0,0,0,0,2,4,0,0,0,0,0,0,1,4,0,0,4,0,0",
			"name": "Expansion of q * phi(q) * psi(q^8) in powers of q where phi(), psi() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A134013/b134013.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of eta(q^2)^5 * eta(q^16)^2 / ( eta(q)^2 * eta(q^4)^2 * eta(q^8) ) in powers of q.",
				"Euler transform of period 16 sequence [ 2, -3, 2, -1, 2, -3, 2, 0, 2, -3, 2, -1, 2, -3, 2, -2, ...].",
				"Moebius transform is period 16 sequence [ 1, 1, -1, -2, 1, -1, -1, 0, 1, 1, -1, 2, 1, -1, -1, 0, ...].",
				"a(n) is multiplicative with a(2) = 2, a(2^e) = 0 if e\u003e1, a(p^e) = e+1 if p == 1 (mod 4), a(p^e) = (1 + (-1)^e)/2 if p == 3 (mod 4).",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (16 t)) = 2 (t/i) g(t) where q = exp(2 Pi i t) and g() is the g.f. for A134014.",
				"a(4*n) = a(4*n + 3) = a(8*n + 6) = 0. a(8*n + 2) = 2 * a(4*n + 1).",
				"G.f.: Sum_{k\u003e0} Kronecker(-4, k) * x^k * (1 + x^k)^2 / (1 - x^(4*k)).",
				"a(n) = -(-1)^n * A112301(n). a(4*n + 1) = A008441(n). a(8*n + 1) = A113407(n). a(8*n = 5) = 2 * A053692(n)."
			],
			"example": [
				"G.f. = q + 2*q^2 + 2*q^5 + q^9 + 4*q^10 + 2*q^13 + 2*q^17 + 2*q^18 + 3*q^25 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ (1/2) EllipticTheta[ 3, 0, q] EllipticTheta[ 2, 0, q^4], {q, 0, n}]; (* _Michael Somos_, Oct 30 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003e0 \u0026\u0026 (n+1)%4\\2, (n%4) * sumdiv( n/gcd(n,2), d, (-1)^(d\\2)))};",
				"(PARI) {a(n) = my(A); if( n\u003c1, 0, n--; A = x * O(x^n); polcoeff( eta(x^2 + A)^5 * eta(x^16 + A)^2 / (eta(x + A)^2 * eta(x^4 + A)^2 * eta(x^8 + A)), n))};"
			],
			"xref": [
				"Cf. A008441, A053692, A112301, A113407."
			],
			"keyword": "nonn,mult",
			"offset": "1,2",
			"author": "_Michael Somos_, Oct 02 2007",
			"references": 4,
			"revision": 16,
			"time": "2021-03-12T22:24:45-05:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}