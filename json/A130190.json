{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A130190",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 130190,
			"data": "1,2,6,4,15,12,42,24,90,10,33,8,910,105,90,48,255,180,3990,420,6930,330,345,720,13650,273,378,28,145,20,14322,2464,117810,3570,7,24,1919190,1729,2730,840,9471,13860,99330,1540,217350,4830,4935,10080,324870",
			"name": "Denominators of z-sequence for the Sheffer matrix (triangle) A094816 (coefficients of Poisson-Charlier polynomials).",
			"comment": [
				"The numerators are given in A130189.",
				"See A130189 for the W. Lang link on z-sequences for Sheffer matrices.",
				"The prime factors of each a(n) are such that n!/a(n) has the prime, p = n+1, as the denominator of its reduced fraction, and if n+1 is not prime then n!/a(n) is an integer, except at n = 3, which has denominator = 2. Also see alternate formula for a(n) below. - _Richard R. Forberg_, Dec 28 2014",
				"As implied above, at n = p-1 the largest prime factor of a(n) is p. For a(m), where m is an integer within the set given by A089965, the two largest prime factors of a(m) are m+1 and (m+1)/2. Furthermore, it appears, when n+1 is not a prime no prime factor of a(n) is greater than k/2, where k is the next higher value of n where n+1 is prime. Two examples at this upper limit of k/2 are n = 104 and 105, where the highest prime factor of a(n) is 53; it is then at n = k = 106 where n+1 is prime. - _Richard R. Forberg_, Jan 01 2015"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A130190/b130190.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = denominator(z(n)),n\u003e=0, with the e.g.f. for z(n) given in A130189.",
				"Denominator of Sum_{k=0..n} A048993(n,k)/(k+1).  - _Peter Luschny_, Apr 28 2009",
				"Alternate: a(n) = denominator((1/e)*Sum_{k\u003e=0}*(Sum_{j=0..k} j^n/k!)). NOTE: Numerators are different from A130189, and given by A248716. - _Richard R. Forberg_, Dec 28 2014",
				"This more generalized expression ((1/e)*Sum_{k\u003e=0} (Sum_{j=0..k} (j+m)^n/k!)), gives the same denominators for any integer m. - _Richard R. Forberg_, Jan 14 2015"
			],
			"maple": [
				"seq(denom(add(Stirling2(n,k)/(k+1),k=0..n)),n=0..20); # _Peter Luschny_, Apr 28 2009"
			],
			"mathematica": [
				"Denominator[Table[(1/Exp[1])* Sum[Sum[j^n/k!, {j, 0, k}], {k, 0, Infinity}], {n, 0, 100}]] (* _Richard R. Forberg_, Dec 28 2014 *)",
				"Table[Denominator[Sum[StirlingS2[n, k]/(k + 1), {k, 0, n}]], {n, 0, 50}] (* _G. C. Greubel_, Jul 10 2018 *)"
			],
			"program": [
				"(PARI) a(n) = denominator(sum(k=0, n, stirling(n, k, 2)/(k+1))); \\\\ _Michel Marcus_, Jan 15 2015, after Maple"
			],
			"xref": [
				"Cf. A089965, A094816, A248716, A130189."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Jun 01 2007",
			"references": 5,
			"revision": 37,
			"time": "2021-08-06T05:07:14-04:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}