{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A283962",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 283962,
			"data": "1,2,3,4,5,6,7,8,9,11,10,12,13,15,17,14,16,18,20,22,25,19,21,23,26,28,31,34,24,27,29,32,35,38,41,44,30,33,36,39,42,46,49,52,56,37,40,43,47,50,54,58,61,65,69,45,48,51,55,59,63,67,71,75,79,84,53",
			"name": "Interspersion of the signature sequence of sqrt(1/2).",
			"comment": [
				"Every row intersperses all other rows, and every column intersperses all other columns. The array is the dispersion of the complement of (column 1 = A022776).",
				"R(n,m) = position of n*r + m when all the numbers k*r + h, where r = sqrt(2), k \u003e= 1, h \u003e= 0, are jointly ranked. - _Clark Kimberling_, Oct 06 2017"
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A283962/b283962.txt\"\u003eAntidiagonals n = 1..60, flattened \u003c/a\u003e",
				"Clark Kimberling and John E. Brown, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL7/Kimberling/kimber67.html\"\u003ePartial Complements and Transposable Dispersions\u003c/a\u003e, J. Integer Seqs., Vol. 7, 2004."
			],
			"formula": [
				"R(i,j) = R(i,0) + R(0,j) + i*j - 1, for i\u003e=1, j\u003e=1."
			],
			"example": [
				"Northwest corner of R:",
				"   1   2   4   7  10  14  19  24  30",
				"   3   5   8  12  16  21  27  33  40",
				"   6   9  13  18  23  29  36  43  51",
				"  11  15  20  26  32  39  47  44  64",
				"  17  22  28  35  42  50  59  68  78",
				"  25  31  38  46  54  63  73  83  94"
			],
			"mathematica": [
				"r = Sqrt[1/2]; z = 100;",
				"s[0] = 1; s[n_] := s[n] = s[n - 1] + 1 + Floor[n*r];",
				"u = Table[n + 1 + Sum[Floor[(n - k)/r], {k, 0, n}], {n, 0, z}] (* A022775, col 1 of A283962 *)",
				"v = Table[s[n], {n, 0, z}] (* A022776, row 1 of A283962*)",
				"w[i_, j_] := u[[i]] + v[[j]] + (i - 1)*(j - 1) - 1;",
				"Grid[Table[w[i, j], {i, 1, 10}, {j, 1, 10}]] (* A283962, array *)",
				"Flatten[Table[w[k, n - k + 1], {n, 1, 20}, {k, 1, n}]] (* A283962, sequence *)"
			],
			"program": [
				"(PARI)",
				"r = sqrt(1/2);",
				"z = 100;",
				"s(n) = if(n\u003c1, 1, s(n - 1) + 1 + floor(n*r));",
				"p(n) = n + 1 + sum(k=0, n, floor((n - k)/r));",
				"u = v = vector(z + 1);",
				"for(n=1, 101, (v[n] = s(n - 1)));",
				"for(n=1, 101, (u[n] = p(n - 1)));",
				"w(i, j) = u[i] + v[j] + (i - 1) * (j - 1) - 1;",
				"tabl(nn) = {for(n=1, nn, for(k=1, n, print1(w(k, n - k + 1), \", \"); );print(); ); };",
				"tabl(10) \\\\ _Indranil Ghosh_, Mar 21 2017",
				"(Python)",
				"from sympy import sqrt",
				"import math",
				"def s(n): return 1 if n\u003c1 else s(n - 1) + 1 +",
				"int(math.floor(n*sqrt(1/2)))",
				"def p(n): return n + 1 + sum([int(math.floor((n - k)/sqrt(1/2))) for k in",
				"range(0, n+1)])",
				"v=[s(n) for n in range(0, 101)]",
				"u=[p(n) for n in range(0, 101)]",
				"def w(i,j): return u[i - 1] + v[j - 1] + (i - 1) * (j - 1) - 1",
				"for n in range(1, 11):",
				"....print [w(k, n - k + 1) for k in range(1, n + 1)] # _Indranil Ghosh_, Mar 21 2017"
			],
			"xref": [
				"Cf. A010503, A022775, A022776, A283939."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Mar 19 2017",
			"references": 5,
			"revision": 14,
			"time": "2019-12-07T12:33:53-05:00",
			"created": "2017-03-19T19:29:29-04:00"
		}
	]
}