{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A066888",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 66888,
			"data": "0,2,1,1,2,2,1,2,3,2,2,3,3,3,3,2,4,3,3,4,4,4,4,4,4,4,4,5,5,6,4,5,3,6,6,7,5,5,6,4,8,5,6,6,8,6,8,5,7,5,11,4,6,9,7,8,9,8,7,7,9,7,8,7,12,5,9,9,11,9,7,7,12,10,10,9,9,9,6,11,10,11,9,12,11,12,9,10,11,12,10,13,9,11,10,12",
			"name": "Number of primes p between triangular numbers T(n) \u003c p \u003c= T(n+1).",
			"comment": [
				"It is conjectured that for n \u003e 0, a(n) \u003e 0. See also A190661. - _John W. Nicholson_, May 18 2011",
				"If the above conjecture is true, then for any k \u003e 1 there is a prime p \u003e k such that p \u003c= (n+1)(n+2)/2, where n = floor(sqrt(2k)+1/2). Ignoring the floor function we can obtain a looser (but nicer) lower bound of p \u003c= 1 + k + 2*sqrt(2k). - _Dmitry Kamenetsky_, Nov 26 2016"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A066888/b066888.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = pi(n*(n+1)/2) - pi(n*(n-1)/2).",
				"a(n) equals the number of occurrences of n+1 in A057062. - _Esko Ranta_, Jul 29 2011"
			],
			"example": [
				"Write the numbers 1, 2, ... in a triangle with n terms in the n-th row; a(n) = number of primes in n-th row.",
				"Triangle begins",
				"   1              (0 primes)",
				"   2  3           (2 primes)",
				"   4  5  6        (1 prime)",
				"   7  8  9 10     (1 prime)",
				"  11 12 13 14 15  (2 primes)"
			],
			"mathematica": [
				"Table[PrimePi[(n^2 + n)/2] - PrimePi[(n^2 - n)/2], {n, 96}] (* _Alonso del Arte_, Sep 03 2011 *)",
				"PrimePi[#[[2]]]-PrimePi[#[[1]]]\u0026/@Partition[Accumulate[Range[0,100]],2,1] (* _Harvey P. Dale_, Jun 04 2019 *)"
			],
			"program": [
				"(PARI) { tp(m)=local(r, t); r=1; for(n=1,m,t=0; for(k=r,n+r-1,if(isprime(k),t++)); print1(t\",\"); r=n+r; ) }",
				"(PARI) {tpf(m)=local(r, t); r=1; for(n=1,m,t=0; for(k=r,n+r-1,if(isprime(k),t++); print1(k\" \")); print1(\" (\"t\" prime)\"); print(); r=n+r;) }"
			],
			"xref": [
				"Cf. A083382.",
				"Essentially the same as A065382 and A090970.",
				"Cf. A000217, A000040, A014085, A190661."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Jun 06 2003",
			"ext": [
				"More terms from _Vladeta Jovovic_ and _Jason Earls_, Jun 06 2003",
				"Offset corrected by _Daniel Forgues_, Sep 05 2012"
			],
			"references": 12,
			"revision": 49,
			"time": "2021-05-23T02:38:46-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}