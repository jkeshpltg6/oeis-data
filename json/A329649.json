{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A329649",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 329649,
			"data": "1,10,4,16,159,48,52,680,136,48,168,288,4150,448,348,64,792,5196,740,1120,1312,1232,144,192,33565,624,1484,2240,3192,2880,2684,43680,4160,-544,3312,576,6424,5776,3696,192,118071,2016,6120,8096,9256,7360,6696,1152,13192",
			"name": "Let D = A042948(n) be the n-th positive integer congruent to 0 or 1 mod 4, then a(n) = b(D) := Sum_{i=1..D} Kronecker(D,i)*i^2, where Kronecker(D,i) is the Kronecker symbol.",
			"comment": [
				"Note that {Kronecker(D,i)} is a Dirichlet character mod |D| if and only if D == 0, 1 (mod 4).",
				"Conjecture (a): There are no 0's in this sequence.",
				"Conjecture (b): if D is not of the form k^2 or 5*k^2 or 2^t (t odd, t \u003e 1), then b(D) is divisible by 4*D; if D = 5*k^2, then b(D) is divisible by 4*k^2 = 4*(D/5).",
				"Conjecture (c): let D be a fundamental discriminant \u003e 1 (A003658), then Sum_{k\u003e=1} Kronecker(D,k)/k^2 = Pi^2*b(D)/D^(5/2) = (4*Pi^2)/D^(3/2) * (b(D)/(4*D)), which is an integer times (4*Pi^2)/D^(3/2) if D \u003e 8 by the conjecture (b). Here Sum_{k\u003e=1} Kronecker(D,k)/k^2 is the value of the Dirichlet L-series of a non-principal character modulo D at s=2."
			],
			"link": [
				"Jianing Song, \u003ca href=\"/A329649/b329649.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"If D is a square, then b(D) = A053818(D), so b(D) is divisible by D if and only if sqrt(D) is not in A316860.",
				"If D is not a square, D is divisible by p^e, where e \u003e= 4 if p = 2, e \u003e= 3 if p \u003e 2, then it is easy to see that b(D) = p^2*b(D/p^2). So we only need to consider the value of b(D) where D is a cubefree number or 8 times a cubefree odd number. Specially, for odd t, we have b(2^t) = b(8)*(2^(t-3)) = 2^(t+1) for t \u003e 1; b(5^t) = b(5)*(5^(t-1)) = 4*5^(t-1); b(13^t) = b(13)*(13^(t-1)) = 4*13^t and so on."
			],
			"example": [
				"For n = 3, D = 5, b(5) = 1^2 - 2^2 - 3^2 + 4^2 = 4. Here 5 = 5*1^2, we have b(5)/(4*1^2) = 1 is an integer. Also, Sum_{k\u003e=1} Kronecker(5,k)/k^2 = Pi^2/(25*sqrt(5)) = Pi^2*b(5)/5^(5/2).",
				"For n = 4, D = 8, b(8) = 1^2 - 3^2 - 5^2 + 7^2 = 16. We have Sum_{k\u003e=1} Kronecker(8,k)/k^2 = Pi^2/(8*sqrt(2)) = Pi^2*b(8)/8^(5/2).",
				"For n = 6, D = 12, b(12) = 1^2 - 5^2 - 7^2 + 11^2 = 48. Here 12 is not of the form k^2 or 5*k^2 or 2^t (t odd, t \u003e 1), we have b(12)/(4*12) = 1 is an integer. Also, Sum_{k\u003e=1} Kronecker(12,k)/k^2 = Pi^2/(6*sqrt(3)) = Pi^2*b(12)/12^(5/2)."
			],
			"mathematica": [
				"b[n_] = Sum[KroneckerSymbol[n, i]*i^2, {i, 1, n}];",
				"a[n_] = b[2 n - Mod[n, 2]]"
			],
			"program": [
				"(PARI) b(n) = sum(i=1, n, kronecker(n,i)*i^2)",
				"a(n) = b(2*n - (n%2))"
			],
			"xref": [
				"Cf. A042948, A003658, A053818, A316860."
			],
			"keyword": "sign",
			"offset": "1,2",
			"author": "_Jianing Song_, Nov 18 2019",
			"references": 1,
			"revision": 20,
			"time": "2019-11-19T07:29:03-05:00",
			"created": "2019-11-19T06:46:17-05:00"
		}
	]
}