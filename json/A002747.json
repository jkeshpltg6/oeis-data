{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002747",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2747,
			"id": "M1924 N0759",
			"data": "1,-2,9,-28,185,-846,7777,-47384,559953,-4264570,61594841,-562923252,9608795209,-102452031878,2017846993905,-24588487650736,548854382342177,-7524077221125234,187708198761024553,-2859149344027588940,78837443479630312281,-1320926996940746090302",
			"name": "Logarithmic numbers.",
			"comment": [
				"From _Gerald McGarvey_, Jun 06 2004: (Start)",
				"For n odd, lim_{n-\u003einfinity} a(n)/n! = cosh(1).",
				"For n even, lim_{n-\u003einfinity} a(n)/n! = sinh(1).",
				"For n even, lim_{n-\u003einfinity} n*a(n)*a(n-1)/n!^2 = cosh(1)*sinh(1).",
				"For signed values, Sum_{n\u003e=1} a(n)/n!^2 = 0.",
				"For unsigned values, Sum_{n\u003e=1} a(n)/n!^2 = cosh(1)*sinh(1). (End)"
			],
			"reference": [
				"J. M. Gandhi, On logarithmic numbers, Math. Student, 31 (1963), 73-83.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A002747/b002747.txt\"\u003eTable of n, a(n) for n = 1..200\u003c/a\u003e",
				"J. M. Gandhi, \u003ca href=\"/A002741/a002741.pdf\"\u003eOn logarithmic numbers\u003c/a\u003e, Math. Student, 31 (1963), 73-83. [Annotated scanned copy]",
				"Simon Plouffe, \u003ca href=\"http://oldweb.cecm.sfu.ca/cgi-bin/isc/lookup?number=1.8134302039235\u0026amp;lookup_type=simple\"\u003eInverter lookup on 1.8134302039235\u003c/a\u003e [Broken link]",
				"\u003ca href=\"/index/Lo#logarithmic\"\u003eIndex entries for sequences related to logarithmic numbers\u003c/a\u003e"
			],
			"formula": [
				"E.g.f.: x/exp(x)/(1-x^2). - _Vladeta Jovovic_, Feb 09 2003",
				"a(n) = n*((n-1)*a(n-2)-(-1)^n). - _Matthew Vandermast_, Jun 30 2003",
				"For n odd, n! * Sum_{i=0..n-1, i even} 1/i!, for n even, n! * Sum_{i=1..n-1, i odd} 1/i!. - _Gerald McGarvey_, Jun 06 2004",
				"a(n) = (-1)^(n-1)*Sum_{k=0..n} C(n, k)*k!*(1-(-1)^k)/2. - _Paul Barry_, Sep 14 2004",
				"a(n) = (-1)^(n+1)*n*A087208(n-1). - _R. J. Mathar_, Jul 24 2015",
				"a(n) = (exp(-1)*Gamma(1+n,-1) - (-1)^n*exp(1)*Gamma(1+n,1))/2 = (A000166(n) - (-1)^n*A000522(n))/2. - _Peter Luschny_, Dec 18 2017"
			],
			"maple": [
				"a:= proc(n) a(n):= n*`if`(n\u003c2, n, (n-1)*a(n-2)-(-1)^n) end:",
				"seq(a(n), n=1..25);  # _Alois P. Heinz_, Jul 10 2013"
			],
			"mathematica": [
				"egf = x/Exp[x]/(1-x^2); a[n_] := SeriesCoefficient[egf, {x, 0, n}]*n!; Table[a[n], {n, 1, 22}] (* _Jean-François Alcover_, Jan 17 2014, after _Vladeta Jovovic_ *)",
				"a[n_] := (Exp[-1] Gamma[1 + n, -1] - (-1)^n Exp[1] Gamma[1 + n, 1])/2;",
				"Table[a[n], {n, 1, 22}] (* _Peter Luschny_, Dec 18 2017 *)"
			],
			"program": [
				"(PARI) a(n) = (-1)^(n+1)*sum(k=0, n, binomial(n, k)*k!*(1-(-1)^k)/2); \\\\ _Michel Marcus_, Jan 13 2022"
			],
			"xref": [
				"Cf. A000166, A000522, A087208."
			],
			"keyword": "sign,changed",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Jeffrey Shallit_",
				"More terms from _Vladeta Jovovic_, Feb 09 2003"
			],
			"references": 5,
			"revision": 67,
			"time": "2022-01-13T09:53:53-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}