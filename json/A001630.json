{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001630",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1630,
			"id": "M0795 N0301",
			"data": "0,0,1,2,3,6,12,23,44,85,164,316,609,1174,2263,4362,8408,16207,31240,60217,116072,223736,431265,831290,1602363,3088654,5953572,11475879,22120468,42638573,82188492,158423412,305370945,588621422,1134604271,2187020050",
			"name": "Tetranacci numbers: a(n) = a(n-1) + a(n-2) + a(n-3) + a(n-4), with a(0)=a(1)=0, a(2)=1, a(3)=2.",
			"comment": [
				"Also (with a different offset), coordination sequence for (4,infinity,infinity) tiling of hyperbolic plane. - _N. J. A. Sloane_, Dec 29 2015",
				"Apparently for n\u003e=2 the number of 1-D walks of length n-2 using steps +1, +3 and -1, avoiding consecutive -1 steps. - _David Scambler_, Jul 15 2013"
			],
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Indranil Ghosh, \u003ca href=\"/A001630/b001630.txt\"\u003eTable of n, a(n) for n = 0..3503\u003c/a\u003e (terms 0..500 from T. D. Noe)",
				"Martin Burtscher, Igor Szczyrba, Rafał Szczyrba, \u003ca href=\"http://www.emis.de/journals/JIS/VOL18/Szczyrba/sz3.pdf\"\u003eAnalytic Representations of the n-anacci Constants and Generalizations Thereof\u003c/a\u003e, Journal of Integer Sequences, Vol. 18 (2015), Article 15.4.5.",
				"J. W. Cannon, P. Wagreich, \u003ca href=\"http://dx.doi.org/10.1007/BF01444714\"\u003eGrowth functions of surface groups\u003c/a\u003e, Mathematische Annalen, 1992, Volume 293, pp. 239-257. See Prop. 3.1.",
				"W. C. Lynch, \u003ca href=\"http://www.fq.math.ca/Scanned/8-1/lynch.pdf\"\u003eThe t-Fibonacci numbers and polyphase sorting\u003c/a\u003e, Fib. Quart., 8 (1970), pp. 6ff.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"H. Prodinger, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Prodinger2/prod31.html\"\u003eCounting Palindromes According to r-Runs of Ones Using Generating Functions\u003c/a\u003e, J. Int. Seq. 17 (2014) # 14.6.2, even length, r=3.",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,1,1,1)."
			],
			"formula": [
				"G.f.: -x^2*(1+x)/(-1+x+x^2+x^3+x^4). [_Simon Plouffe_ in his 1992 dissertation]",
				"a(n) = A000078(n) + A000078(n+1) = a(n-1) + A000078(n+1) - A000078(n-1). - _Henry Bottomley_",
				"a(n) = 2*a(n-1) - a(n-5) with n\u003e4, a(0)=a(1)=0, a(2)=1, a(3)=2, a(4)=3. [_Vincenzo Librandi_, Dec 21 2010]",
				"G.f.: x^2 + x^3*G(0) where G(k) = 2 + x*(1+x+x^2 + (1+x)*(1+x^2)*G(k+1)). - _Sergei N. Gladkovskii_, Jan 27 2013 [Edited by _Michael Somos_, Nov 12 2013]"
			],
			"example": [
				"G.f. = x^2 + 2*x^3 + 3*x^4 + 6*x^5 + 12*x^6 + 23*x^7 + 44*x^8 + 85*x^9 + ..."
			],
			"maple": [
				"a:= proc(n) option operator; local M; M := Matrix(4, (i,j)-\u003e if (i=j-1) or j=1 then 1 else 0 fi)^n; M[1,4]+M[1,3] end; seq (a(n), n=0..34); # _Alois P. Heinz_, Aug 01 2008"
			],
			"mathematica": [
				"a=0; b=0; c=1; d=2; lst={a, b, c, d}; Do[e=a+b+c+d; AppendTo[lst, e]; a=b; b=c; c=d; d=e, {n, 4!}]; lst (* _Vladimir Joseph Stephan Orlovsky_, Sep 30 2008 *)",
				"RecurrenceTable[{a[0] == a[1] == 0, a[2] == 1, a[3] == 2, a[n] == a[n - 1] + a[n - 2] + a[n - 3] + a[n - 4]}, a, {n, 35}] (* or *) a = {0, 0, 1, 2}; Do[AppendTo[a, a[[-1]] + a[[-2]] + a[[-3]] + a[[-4]]], {35}]; a (* _Bruno Berselli_, Jan 29 2013 *)",
				"CoefficientList[Series[- x^2 * (1 + x)/(- 1 + x + x^2 + x^3 + x^4), {x, 0, 35}], x] (* _Vincenzo Librandi_, Jan 29 2013 *)",
				"LinearRecurrence[{1,1,1,1},{0,0,1,2},40] (* _Harvey P. Dale_, Aug 25 2013 *)"
			],
			"program": [
				"(MAGMA) I:=[0, 0, 1, 2]; [n le 4 select I[n] else Self(n-1)+ Self(n-2) + Self(n-3) + Self(n-4): n in [1..40]]; // _Vincenzo Librandi_, Jan 29 2013",
				"(PARI) concat([0, 0], Vec(-x^2*(1+x)/(-1+x+x^2+x^3+x^4) + O(x^50))) \\\\ _Michel Marcus_, Dec 30 2015"
			],
			"xref": [
				"Coordination sequences for triangular tilings of hyperbolic space: A001630, A007283, A054886, A078042, A096231, A163876, A179070, A265057, A265058, A265059, A265060, A265061, A265062, A265063, A265064, A265065, A265066, A265067, A265068, A265069, A265070, A265071, A265072, A265073, A265074, A265075, A265076, A265077.",
				"Cf. A000032."
			],
			"keyword": "nonn,easy",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_",
			"references": 49,
			"revision": 91,
			"time": "2021-03-12T22:32:35-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}