{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A112591",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 112591,
			"data": "1,6,2,12,6,28,2,4,10,2,58,12,2,4,26,14,6,126,4,14,6,28,10,56,4,2,12,6,28,14,252,10,2,30,2,10,62,4,10,30,6,10,126,4,2,20,12,60,6,12,6,30,10,506,6,10,2,26,12,2,62,22,4,14,4,118,26,10,6,60,6,8,26,14,4,250,8,28,8",
			"name": "a(n) = prime(n) XOR prime(n + 1).",
			"comment": [
				"When represented in binary, this sequence represents the uncommon bits in two consecutive prime numbers.",
				"a(n) = 2 for indices of the lesser twin primes of the form 4k + 1, A071695. - _Michel Marcus_, Apr 17 2020"
			],
			"link": [
				"Harvey P. Dale, \u003ca href=\"/A112591/b112591.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e"
			],
			"example": [
				"a(2) = 6 ; since prime(2) = 3, which is 11 in binary, prime(3) = 5, which is 101 in binary; and 011 XOR 101 = 110, which is 6 in decimal."
			],
			"maple": [
				"A112591 := proc(n) local ndual,n2dual,nxor,i ; ndual := convert(ithprime(n),base,2) ; n2dual := convert(ithprime(n+1),base,2) ; nxor := [] ; i := 1 ; while i \u003c= nops(ndual) do nxor := [op(nxor), abs(op(i,ndual)-op(i,n2dual)) ] ; i := i+1 ; od ; while i \u003c= nops(n2dual) do nxor := [op(nxor), op(i,n2dual) ] ; i := i+1 ; od ; add( op(i,nxor)*2^(i-1),i=1..nops(nxor)) ; end: for n from 1 to 80 do printf(\"%d,\",A112591(n)) ; od ; # _R. J. Mathar_, Mar 07 2007",
				"with(Bits):seq(Xor(ithprime(n),ithprime(n+1)),n=1..50) # _Gary Detlefs_, Aug 03 2013"
			],
			"mathematica": [
				"BitXor@@#\u0026/@Partition[Prime[Range[80]], 2, 1] (* _Harvey P. Dale_, May 04 2018 *)"
			],
			"program": [
				"(PARI) a(n) = bitxor(prime(n),prime(n+1)); \\\\ _Joerg Arndt_, Aug 04 2013",
				"(Scala) val prime: LazyList[Int] = 2 #:: LazyList.from(3).filter(i =\u003e prime.takeWhile { j =\u003e j * j \u003c= i }.forall { k =\u003e i % k != 0 })",
				"(0 to 127).map(n =\u003e prime(n) ^ prime(n + 1)) // _Alonso del Arte_, Apr 18 2020"
			],
			"xref": [
				"Cf. A003987, A071695."
			],
			"keyword": "nonn,base",
			"offset": "1,2",
			"author": "Sandeep Chellappen (sandeep.chellappen(AT)gmail.com), Dec 18 2005",
			"ext": [
				"More terms and better name from Christopher M. Herron (cmh285(AT)psu.edu), Apr 25 2006"
			],
			"references": 5,
			"revision": 38,
			"time": "2021-07-25T01:25:25-04:00",
			"created": "2006-01-24T03:00:00-05:00"
		}
	]
}