{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A271834",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 271834,
			"data": "0,0,0,4,0,42,0,116,162,730,0,2458,0,11494,16890,32628,0,180960,0,554994,931476,2800534,0,11005898,6643750,43946838,44738892,136580910,0,720879712,0,2147450740,3250382916,10923409738,11517062060,45683761528,0,172783692982",
			"name": "a(n) = 2^n - Sum_{m=0..n} binomial(n/gcd(n,m), m/gcd(n,m)) = 2^n - A082906.",
			"comment": [
				"Compared to A082906, this sequence shows better the drop from 2^n upon replacing every binomial(n,m) in the Newton's expansion of (1+1)^n by the 'reduced' binomial(n/gcd(n,m), m/gcd(n,m)). For n \u003e 1, a(n) is zero if and only if n is prime (no reduction, no drop). The ratio r(n) = a(n)/2^n is always smaller than 1 and presents considerable excursions. For composite n up to 5000, the minimum of 0.01471... occurs for n = 4489, and the maximum of 0.80849... occurs for n = 2310. This apparently large relative difference is actually surprisingly small: on log_2 scale it amounts to just about 5.78; a tiny fraction compared to the full scale, given by the values of n for the extrema. This insight suggests the following conjecture: there exists an average ratio r, defined as r = lim_{n-\u003einfinity} Sum_{m=1..n} r(m)/n. Its value appears to be approximately 0.3915+-0.0010, which can be interpreted as the average drop in a binomial value upon the 'reduction' of its arguments."
			],
			"link": [
				"Stanislav Sykora, \u003ca href=\"/A271834/b271834.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Stanislav Sykora, \u003ca href=\"/A271834/a271834.txt\"\u003eRatios A271834(n)/2^n for n=1..5000\u003c/a\u003e"
			],
			"formula": [
				"For prime p, a(p) = 0.",
				"For any n, a(n) \u003c 2^n - n(n+1)/2."
			],
			"example": [
				"Sum_{m=1..2500} r(m)/2500 = 0.391460...",
				"Sum_{m=2501..5000} r(m)/2500 = 0.391975...",
				"Sum_{m=1..5000} r(m)/5000 = 0.391718..."
			],
			"maple": [
				"A271834:=n-\u003e2^n-add(binomial(n/gcd(n,m),m/gcd(n,m)),m=0..n): seq(A271834(n), n=1..50); # _Wesley Ivan Hurt_, Apr 19 2016"
			],
			"mathematica": [
				"Table[2^n - Sum[Binomial[n/GCD[n, m], m/GCD[n, m]], {m, 0, n}], {n, 40}] (* _Wesley Ivan Hurt_, Apr 19 2016 *)"
			],
			"program": [
				"(PARI) bcg(n,m)=binomial(n/gcd(n,m),m/gcd(n,m));",
				"a = vector(1000,n,2^n-vecsum(vector(n+1,m,bcg(n,m-1))))"
			],
			"xref": [
				"Cf. A082905, A082906."
			],
			"keyword": "nonn,easy",
			"offset": "1,4",
			"author": "_Stanislav Sykora_, Apr 19 2016",
			"references": 1,
			"revision": 38,
			"time": "2019-08-01T04:09:52-04:00",
			"created": "2016-04-20T01:01:18-04:00"
		}
	]
}