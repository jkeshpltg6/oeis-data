{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002210",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2210,
			"id": "M1564 N0609",
			"data": "2,6,8,5,4,5,2,0,0,1,0,6,5,3,0,6,4,4,5,3,0,9,7,1,4,8,3,5,4,8,1,7,9,5,6,9,3,8,2,0,3,8,2,2,9,3,9,9,4,4,6,2,9,5,3,0,5,1,1,5,2,3,4,5,5,5,7,2,1,8,8,5,9,5,3,7,1,5,2,0,0,2,8,0,1,1,4,1,1,7,4,9,3,1,8,4,7,6,9,7,9,9,5,1,5",
			"name": "Decimal expansion of Khintchine's constant.",
			"comment": [
				"_Carles Simó_, Oct 11 2016, reports that he has computed 10^6 terms of this sequence (see links). - _N. J. A. Sloane_, Nov 04 2016",
				"Named after the Soviet mathematician Aleksandr Yakovlevich Khintchine (1894 - 1959). - _Amiram Eldar_, Aug 19 2020"
			],
			"reference": [
				"S. R. Finch, Mathematical Constants, Encyclopedia of Mathematics and its Applications, vol. 94, Cambridge University Press, pp. 59-65.",
				"A. Ya. Khintchine, Continued Fractions, Groningen: Noordhoff, 1963.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence).",
				"I. Vardi, Computational Recreations in Mathematica. Addison-Wesley, Redwood City, CA, 1991, p. 164."
			],
			"link": [
				"Harry J. Smith, \u003ca href=\"/A002210/b002210.txt\"\u003eTable of n, a(n) for n = 1..1200\u003c/a\u003e",
				"D. H. Bailey, J. M. Borwein \u0026 R. E. Crandall, \u003ca href=\"http://crd.lbl.gov/~dhbailey/dhbpapers/khinchine.pdf\"\u003eOn the Khintchine Constant\u003c/a\u003e",
				"D. H. Bailey, J. M. Borwein, V. Kapoor and E. Weisstein, \u003ca href=\"http://crd-legacy.lbl.gov/~dhbailey/dhbpapers/tenproblems.pdf\"\u003eTen Problems in Experimental Mathematics\u003c/a\u003e",
				"Ph. Flajolet and I. Vardi, \u003ca href=\"http://algo.inria.fr/flajolet/Publications/publist.html\"\u003eZeta function expansions of some classical constants\u003c/a\u003e",
				"E. Fontich, C. Simó, A. Vieiro, \u003ca href=\"https://doi.org/10.1134/S1560354718060011\"\u003eOn the \"hidden\" harmonics associated to best approximants due to quasiperiodicity in splitting phenomena\u003c/a\u003e, Regular and Chaotic Dynamics (2018), Pleiades Publishing, Vol. 23, Issue 6, 638-653.",
				"Brady Haran and Tony Padilla, \u003ca href=\"http://www.youtube.com/watch?v=VDD6FDhKCYA\"\u003eSix Sequences\u003c/a\u003e, Numberphile video, 2013.",
				"A. Khintchine, \u003ca href=\"http://www.numdam.org/item/?id=CM_1935__1__361_0\"\u003eMetrische kettenbruchprobleme\u003c/a\u003e, Compositio Mathematica, Vol. 1 (1935), pp. 361-382.",
				"A. Khintchine, \u003ca href=\"http://www.numdam.org/item/CM_1936__3__276_0/\"\u003eZur metrischen Kettenbruchtheorie\u003c/a\u003e, Compositio Mathematica, Vol. 3 (1936), pp. 276-285.",
				"Christian Perfect, \u003ca href=\"http://aperiodical.com/2013/07/integer-sequence-reviews-on-numberphile-or-vice-versa/\"\u003eInteger sequence reviews on Numberphile (or vice versa)\u003c/a\u003e, 2013.",
				"Simon Plouffe, \u003ca href=\"http://www.plouffe.fr/simon/constants/khintchine.txt\"\u003e110000 digits of the Khintchine constant\u003c/a\u003e",
				"Simon Plouffe, \u003ca href=\"http://www.worldwideschool.org/library/books/sci/math/MiscellaneousMathematicalConstants/chap50.html\"\u003eKhinchin constant to 1024 digits\u003c/a\u003e",
				"D. Shanks and J. W. Wrench, Jr., \u003ca href=\"http://www.jstor.org/stable/2309633\"\u003eKhintchine's constant\u003c/a\u003e, Amer. Math. Monthly, 66 (1959), 276-279.",
				"Carles Simó, \u003ca href=\"http://www.maia.ub.es/dsg/khinchin\"\u003eComputation of 10^6 digits of Khintchine's constant\u003c/a\u003e",
				"Carles Simó, \u003ca href=\"/A002210/a002210.png\"\u003eComputation of 10^6 digits of Khintchine's constant\u003c/a\u003e [Cached copy, with permission]",
				"Carles Simó, \u003ca href=\"/A002210/a002210.txt\"\u003e10^6 digits of Khintchine's constant\u003c/a\u003e [Cached copy, with permission]",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ContinuedFraction.html\"\u003eContinued Fraction\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/KhinchinsConstant.html\"\u003eKhinchin's Constant\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/KhinchinsConstantDigits.html\"\u003eKhinchin's Constant Digits\u003c/a\u003e",
				"Thomas Wieting, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9939-07-09202-7\"\u003eA Khinchin Sequence\u003c/a\u003e, Proc. Amer. Math. Soc. 136 (2008), 815-824.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Khinchin%27s_constant\"\u003eKhinchin's constant\u003c/a\u003e",
				"J. W. Wrench, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1960-0170455-1\"\u003eFurther evaluation of Khintchine's constant\u003c/a\u003e, Math. Comp., 14 (1960), 370-371."
			],
			"formula": [
				"From _Amiram Eldar_, Aug 19 2020: (Start)",
				"Equal Product_{k\u003e=1} (1 + 1/(k*(k+2)))^log_2(k).",
				"Equals exp(A247038/log(2)). (End)"
			],
			"example": [
				"2.685452001065306445309714835481795693820382293994462953051152345557218..."
			],
			"mathematica": [
				"RealDigits[N[Khinchin, 100]][[1]] (* _Vladimir Joseph Stephan Orlovsky_, Jun 18 2009 *)"
			],
			"program": [
				"(Python)",
				"from mpmath import mp, khinchin",
				"mp.dps = 106",
				"print([int(k) for k in list(str(khinchin).replace('.', ''))[:-1]]) # _Indranil Ghosh_, Jul 08 2017"
			],
			"xref": [
				"Cf. A002211, A247038."
			],
			"keyword": "nonn,cons,nice",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Pari code removed by _D. S. McNeil_, Dec 26 2010"
			],
			"references": 47,
			"revision": 90,
			"time": "2021-06-26T20:33:21-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}