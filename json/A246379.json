{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A246379",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 246379,
			"data": "1,3,9,2,21,6,5,18,4,42,39,12,11,10,36,8,15,84,23,78,24,22,7,20,72,16,30,168,47,46,189,156,48,44,14,40,17,144,32,60,45,336,13,94,92,378,41,312,96,88,28,80,25,34,288,64,120,90,81,672,133,26,188,184,756,82,135,624,192,176,83,56,49",
			"name": "Permutation of natural numbers: a(1) = 1, a(p_n) = A003961(1+a(n)), a(c_n) = 2*a(n), where p_n = n-th prime = A000040(n), c_n = n-th composite number = A002808(n), and A003961(n) shifts the prime factorization of n one step towards larger primes.",
			"comment": [
				"Because 2 is the only even prime, it implies that, apart from a(2)=3, odd numbers occur in odd positions only (along with many even numbers that also occur in odd positions). This in turn implies that each odd composite (A071904) resides in a separate infinite cycle in this permutation, except 9, which is in a finite cycle (2 3 9 4)."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A246379/b246379.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"a(1) = 1, and for n \u003e 1, if A010051(n) = 1 [i.e. when n is a prime], a(n) = A003961(1+a(A000720(n))), otherwise a(n) = 2*a(A065855(n)).",
				"As a composition of related permutations:",
				"a(n) = A246375(A246377(n)).",
				"Other identities. For all n \u003e 1 the following holds:",
				"A000035(a(n)) = A010051(n). [Maps primes to odd numbers \u003e 1, and composites to even numbers, in some order. Permutations A246377 \u0026 A246681 have the same property]."
			],
			"program": [
				"(PARI)",
				"default(primelimit, (2^31)+(2^30));",
				"A003961(n) = my(f = factor(n)); for (i=1, #f~, f[i, 1] = nextprime(f[i, 1]+1)); factorback(f); \\\\ Using code of _Michel Marcus_",
				"A246379(n) = if(1==n, 1, if(isprime(n), A003961(1+A246379(primepi(n))), 2*A246379(n-primepi(n)-1)));",
				"for(n=1, 10000, write(\"b246379.txt\", n, \" \", A246379(n)));",
				"(Scheme, with memoization-macro definec)",
				"(definec (A246379 n) (cond ((\u003c n 2) n) ((= 1 (A010051 n)) (A003961 (+ 1 (A246379 (A000720 n))))) (else (* 2 (A246379 (A065855 n))))))"
			],
			"xref": [
				"Inverse: A246380.",
				"Similar or related permutations: A246375, A246377, A246363, A246364, A246365, A246367, A246681.",
				"Cf. A000720, A003961, A010051, A065855, A071904."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Antti Karttunen_, Aug 29 2014",
			"references": 5,
			"revision": 15,
			"time": "2014-09-01T11:47:19-04:00",
			"created": "2014-09-01T11:47:19-04:00"
		}
	]
}