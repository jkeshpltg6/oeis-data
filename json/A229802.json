{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A229802",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 229802,
			"data": "1,-2,4,-3,1,2,-2,0,3,-2,2,-2,4,-6,4,1,-2,4,0,-3,2,-4,4,0,1,2,0,-4,0,2,2,-2,8,-6,-2,1,-2,0,6,0,2,-4,4,-6,3,2,-2,4,-3,-2,2,-2,4,0,2,0,0,0,0,-2,2,-4,4,-3,4,4,-2,-4,6,-6,2,0,4,-6,4,0,-4",
			"name": "Expansion of q * f(-q, -q^4)^5 / f(-q)^3 in powers of q where f() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A229802/b229802.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q * f(-q)^2 * (f(-q^5) / f(-q^2, -q^3))^5 = q * f(-q, -q^4)^2 * (f(-q^5) / f(-q^2, -q^3))^3 in powers of q where f() is a Ramanujan theta function. - _Michael Somos_, Jun 10 2014",
				"Euler transform of period 5 sequence [ -2, 3, 3, -2, -2, ...].",
				"Moebius transform is period 5 sequence [ 1, -3, 3, -1, 0, ...]. - _Michael Somos_, Jun 10 2014",
				"G.f.: x * (Product_{k\u003e0} (1 - x^k)^2) / (Product_{k\u003e0} (1 - x^(5*k - 2)) * (1 - x^(5*k - 3)))^5.",
				"a(5*n) = a(n)."
			],
			"example": [
				"G.f. = q - 2*q^2 + 4*q^3 - 3*q^4 + q^5 + 2*q^6 - 2*q^7 + 3*q^9 - 2*q^10 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ q QPochhammer[ q]^2 / (QPochhammer[ q^2, q^5] QPochhammer[ q^3, q^5])^5, {q, 0, n}]; (* _Michael Somos_, Jun 10 2014 *)",
				"a[ n_] := If[ n \u003c 1, 0, Sum[ Im[(I - 3) {1, I, -I, -1, 0}[[ Mod[ d, 5, 1] ]] ], {d, Divisors @ n}]]; (* _Michael Somos_, Jun 10 2014 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c1, 0, n--; A = x * O(x^n); polcoeff( prod(k=1, n, (1 - x^k + A)^[ 2, 2, -3, -3, 2][k%5 + 1]), n))};",
				"(PARI) {a(n) = if( n\u003c1, 0, sumdiv(n, d, imag( (I - 3) * [ 0, 1, I, -I, -1][ d%5 + 1])))}; /* _Michael Somos_, Jun 10 2014 */",
				"(Sage) ModularForms( Gamma1(5), 1, prec=70).1;",
				"(MAGMA) Basis( ModularForms( Gamma1(5), 1), 70) [2] ; /* _Michael Somos_, Jun 10 2014 */"
			],
			"xref": [
				"Cf. A227216."
			],
			"keyword": "sign",
			"offset": "1,2",
			"author": "_Michael Somos_, Sep 29 2013",
			"references": 4,
			"revision": 20,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2013-09-29T20:29:52-04:00"
		}
	]
}