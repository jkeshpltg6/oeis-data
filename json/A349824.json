{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A349824",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 349824,
			"data": "0,0,2,3,8,5,10,7,18,12,14,11,21,13,18,16,32,17,24,19,27,20,26,23,36,20,30,27,33,29,30,31,50,28,38,24,40,37,42,32,44,41,36,43,45,33,50,47,55,28,36,40,51,53,44,32,52,44,62,59,48,61,66,39,72,36,48,67,63,52,42,71,60,73",
			"name": "a(0) = 0; for n \u003e= 1, a(n) = (number of primes, counted with repetition) * (sum of primes, counted with repetition).",
			"comment": [
				"More precisely, a(n) = (number of prime factors of n, counted with repetition) * (sum of primes factors of n, counted with repetition): a(n) = A001414(n) * A001222(n).",
				"Suggested by Mike Klein in an email, Dec 31 2021.",
				"Conjecture (Mike Klein): Iterating n -\u003e a(n) eventually leads to one of the fixed points {primes union 0, 27, 30} or the loop (28, 33).",
				"It appears that with the exception of {1,4,16,27,30}, n divides a(n) iff n is prime. - _Gary Detlefs_, Jan 11 2022"
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A349824/b349824.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e"
			],
			"example": [
				"If n = 27 = 3^3, a(n) = 3*(3+3+3) = 27.",
				"If we start with n = 4, iterating this map produces the trajectory 4, 8, 18, 24, 36, 40, 44, 45, 33, 28, 33, 28, 33, 28, ...",
				"If we start with n = 6, iterating this map produces the trajectory 6, 10, 14, 18, 24, 36, 40, 44, 45, 33, 28, 33, 28, 33, 28, ..."
			],
			"mathematica": [
				"{0, 0}~Join~Array[Total[#]*Length[#] \u0026@ Flatten[ConstantArray[#1, #2] \u0026 @@@ FactorInteger[#]] \u0026, 72, 2] (* _Michael De Vlieger_, Jan 02 2022 *)"
			],
			"program": [
				"(PARI) a(n) = { if (n==0, 0, my (f=factor(n)); bigomega(f)*sum(k=1, #f~, f[k,1]*f[k,2])) } \\\\ _Rémy Sigrist_, Jan 01 2022",
				"(Python)",
				"from sympy import factorint",
				"def a(n):",
				"    if n == 0: return 0",
				"    f = factorint(n)",
				"    return sum(f.values()) * sum(p*e for p, e in f.items())",
				"print([a(n) for n in range(74)]) # _Michael S. Branicky_, Jan 02 2022"
			],
			"xref": [
				"Cf. A001222, A001414."
			],
			"keyword": "nonn,new",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_, Jan 01 2022",
			"references": 4,
			"revision": 19,
			"time": "2022-01-11T21:41:54-05:00",
			"created": "2022-01-01T16:23:16-05:00"
		}
	]
}