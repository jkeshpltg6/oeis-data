{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A014445",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 14445,
			"data": "0,2,8,34,144,610,2584,10946,46368,196418,832040,3524578,14930352,63245986,267914296,1134903170,4807526976,20365011074,86267571272,365435296162,1548008755920,6557470319842,27777890035288,117669030460994",
			"name": "Even Fibonacci numbers; or, Fibonacci(3*n).",
			"comment": [
				"a(n) = 3^n*b(n;2/3) = -b(n;-2), but we have 3^n*a(n;2/3) = F(3n+1) = A033887 and a(n;-2) = F(3n-1) = A015448, where a(n;d) and b(n;d), n=0,1,..., d, denote the so-called delta-Fibonacci numbers (the argument \"d\" of a(n;d) and b(n;d) is abbreviation of the symbol \"delta\") defined by the following equivalent relations: (1 + d*((sqrt(5) - 1)/2))^n = a(n;d) + b(n;d)*((sqrt(5) - 1)/2) equiv. a(0;d)=1, b(0;d)=0, a(n+1;d) = a(n;d) + d*b(n;d), b(n+1;d) = d*a(n;d) + (1-d)b(n;d) equiv. a(0;d)=a(1;d)=1, b(0;1)=0, b(1;d)=d, and x(n+2;d) + (d-2)*x(n+1;d) + (1-d-d^2)*x(n;d) = 0 for every n=0,1,...,d, and x=a,b equiv. a(n;d) = Sum_{k=0..n} C(n,k)*F(k-1)*(-d)^k, and b(n;d) = Sum_{k=0..n} C(n,k)*(-1)^(k-1)*F(k)*d^k equiv. a(n;d) = Sum_{k=0..n} C(n,k)*F(k+1)*(1-d)^(n-k)*d^k, and b(n;d) = Sum_{k=1..n} C(n;k)*F(k)*(1-d)^(n-k)*d^k. The sequences a(n;d) and b(n;d) for special values d are connected with many known sequences: A000045, A001519, A001906, A015448, A020699, A033887, A033889, A074872, A081567, A081568, A081569, A081574, A081575, A163073 (see also the papers of Witula et al.). - _Roman Witula_, Jul 12 2012",
				"For any odd k, Fibonacci(k*n) = sqrt(Fibonacci((k-1)*n) * Fibonacci((k+1)*n) + Fibonacci(n)^2). - _Gary Detlefs_, Dec 28 2012",
				"The ratio of consecutive terms approaches the continued fraction 4 + 1/(4 + 1/(4 +...)) = A098317. - _Hal M. Switkay_, Jul 05 2020"
			],
			"reference": [
				"A. T. Benjamin and J. J. Quinn, Proofs that really count: the art of combinatorial proof, M.A.A. 2003, id. 232."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A014445/b014445.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"Mohammad K. Azarian, \u003ca href=\"http://www.m-hikari.com/ijcms/ijcms-2012/37-40-2012/azarianIJCMS37-40-2012.pdf\"\u003eFibonacci Identities as Binomial Sums\u003c/a\u003e, International Journal of Contemporary Mathematical Sciences, Vol. 7, No. 38, 2012, pp. 1871-1876 (See Corollary 1 (v)).",
				"I. M. Gessel and Ji Li, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL16/Gessel/gessel6.html\"\u003eCompositions and Fibonacci identities\u003c/a\u003e, J. Int. Seq. 16 (2013), 13.4.5.",
				"Edyta Hetmaniok, Bozena Piatek, and Roman Wituła, \u003ca href=\"https://doi.org/10.1515/math-2017-0047\"\u003eBinomials Transformation Formulae of Scaled Fibonacci Numbers\u003c/a\u003e, Open Math. 15 (2017), 477-485.",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"Ron Knott, \u003ca href=\"http://www.maths.surrey.ac.uk/hosted-sites/R.Knott/Fibonacci/fibmaths.html\"\u003eMathematics of the Fibonacci Series\u003c/a\u003e",
				"Peter McCalla and Asamoah Nkwanta, \u003ca href=\"https://arxiv.org/abs/1901.07092\"\u003eCatalan and Motzkin Integral Representations\u003c/a\u003e, arXiv:1901.07092 [math.NT], 2019.",
				"Michael Z. Spivey and Laura L. Steil, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL9/Spivey/spivey7.html\"\u003eThe k-Binomial Transforms and the Hankel Transform\u003c/a\u003e, Journal of Integer Sequences, Vol. 9 (2006), Article 06.1.1.",
				"Roman Witula and Damian Slota, \u003ca href=\"http://dx.doi.org/10.2298/AADM0902310W\"\u003edelta-Fibonacci numbers\u003c/a\u003e, Appl. Anal. Discr. Math 3 (2009) 310-329, \u003ca href=\"http://www.ams.org/mathscinet-getitem?mr=2555042\"\u003eMR2555042\u003c/a\u003e",
				"Roman Witula, \u003ca href=\"https://doi.org/10.1515/dema-2013-0436\"\u003eBinomials transformation formulae of scaled Lucas numbers\u003c/a\u003e, Demonstratio Math. 46 (2013), 15-27.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,1)."
			],
			"formula": [
				"a(n) = Sum_{k=0..n} binomial(n, k)*F(k)*2^k. - _Benoit Cloitre_, Oct 25 2003",
				"From _Lekraj Beedassy_, Jun 11 2004: (Start)",
				"a(n) = 4*a(n-1) + a(n-2), with a(-1) = 2, a(0) = 0.",
				"a(n) = 2*A001076(n).",
				"a(n) = (F(n+1))^3 + (F(n))^3 - (F(n-1))^3. (End)",
				"a(n) = Sum_{k=0..floor((n-1)/2)} C(n, 2*k+1)*5^k*2^(n-2*k). - Mario Catalani (mario.catalani(AT)unito.it), Jul 22 2004",
				"a(n) = Sum_{k=0..n} F(n+k)*binomial(n, k). - _Benoit Cloitre_, May 15 2005",
				"O.g.f.: 2*x/(1 - 4*x - x^2). - _R. J. Mathar_, Mar 06 2008",
				"a(n) = second binomial transform of (2,4,10,20,50,100,250). This is 2* (1,2,5,10,25,50,125) or 5^n (offset 0): *2 for the odd numbers or *4 for the even. The sequences are interpolated. Also a(n) = 2*((2+sqrt(5))^n - (2-sqrt(5))^n)/sqrt(20). - Al Hakanson (hawkuu(AT)gmail.com), May 02 2009",
				"a(n) = 3*F(n-1)*F(n)*F(n+1) + 2*F(n)^3, F(n)=A000045(n). - _Gary Detlefs_, Dec 23 2010",
				"a(n) = (-1)^n*3*F(n) + 5*F(n)^3, n \u003e= 0. See the D. Jennings formula given in a comment on A111125, where also the reference is given. - _Wolfdieter Lang_, Aug 31 2012",
				"With L(n) a Lucas number, F(3*n) = F(n)*(L(2*n) + (-1)^n) = (L(3*n+1) + L(3*n-1))/5 starting at n=1. - _J. M. Bergot_, Oct 25 2012",
				"a(n) = sqrt(Fibonacci(2*n)*Fibonacci(4*n) + Fibonacci(n)^2). - _Gary Detlefs_, Dec 28 2012",
				"For n \u003e 0, a(n) = 5*F(n-1)*F(n)*F(n+1) - 2*F(n)*(-1)^n. - _J. M. Bergot_, Dec 10 2015",
				"a(n) = -(-1)^n * a(-n) for all n in Z. - _Michael Somos_, Nov 15 2018"
			],
			"example": [
				"G.f. = 2*x + 8*x^2 + 34*x^3 + 144*x^4 + 610*x^5 + 2584*x^6 + 10946*x^7 + ..."
			],
			"mathematica": [
				"Table[Fibonacci[3n], {n,0,30}] (* _Stefan Steinerberger_, Apr 07 2006 *)",
				"LinearRecurrence[{4,1},{0,2},30] (* _Harvey P. Dale_, Nov 14 2021 *)"
			],
			"program": [
				"(MuPAD) numlib::fibonacci(3*n) $ n = 0..30; // _Zerinvary Lajos_, May 09 2008",
				"(Sage) [fibonacci(3*n) for n in range(0, 30)] # _Zerinvary Lajos_, May 15 2009",
				"(MAGMA) [Fibonacci(3*n): n in [0..30]]; // _Vincenzo Librandi_, Apr 18 2011",
				"(PARI) a(n)=fibonacci(3*n) \\\\ _Charles R Greathouse IV_, Oct 25 2012"
			],
			"xref": [
				"Cf. A000045, A001076.",
				"First differences of A099919. Third column of array A102310."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,2",
			"author": "_Mohammad K. Azarian_",
			"references": 38,
			"revision": 155,
			"time": "2021-11-14T10:29:27-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}