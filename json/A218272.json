{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A218272",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 218272,
			"data": "0,1,0,0,2,0,0,0,3,0,0,0,0,4,0,0,0,0,0,5,0,0,0,0,0,0,6,0,0,0,0,0,0,0,7,0,0,0,0,0,0,0,0,8,0,0,0,0,0,0,0,0,0,9,0,0,0,0,0,0,0,0,0,0,10,0,0,0,0,0,0,0,0,0,0,0,11,0,0,0",
			"name": "Infinitesimal generator for transpose of the Pascal matrix A007318 (as upper triangular matrices).",
			"comment": [
				"T is the transpose of A132440.",
				"Let M(t) = exp(t*T) = limit [1 + t*T/n]^n as n tends to infinity.",
				"Then M(1) = the transpose of the lower triangular Pascal matrix A007318, with inverse M(-1).",
				"Given a polynomial sequence p_n(x) with p_0(x)=1 and the lowering and raising operators L and R defined by L P_n(x) = n * P_(n-1)(x) and",
				"  R P_n(x) = P_(n+1)(x), the matrix T represents the action of L in the p_n(x) basis. For p_n(x) = x^n, L = D = d/dx and R = x. For p_n(x) = x^n/n!, L = DxD and R = D^(-1).",
				"See A132440 as an analog and more general discussion.",
				"Sum_{n\u003e=0} c_n T^n / n! = e^(c.T) gives the Maurer-Cartan form matrix for the one-dimensional Leibniz group defined by multiplication of a Taylor series by the formal Taylor series e^(c.x) (cf. Olver). - _Tom Copeland_, Nov 05 2015",
				"From _Tom Copeland_, Jul 02 2018: (Start)",
				"The transpose Psc^Trn of the lower triangular Pascal matrix Psc = A007318 gives the numerical coefficients of the Maurer-Cartan form matrix M of the Leibniz group Leibniz(n)(1,1) presented on p. 9 of the Olver paper. M = exp[c. * T] with (c.)^n = c_n and T the Lie infinitesimal generator of this entry. The columns e^T are the rows of the Pascal matrix A007318.",
				"M can be obtained by multiplying each n-th column vector of Psc by c_n and then transposing the result; i.e., with the diagonal matrix H = Diag(c_0, c_1, c_2, ...), M = (Psc * H)^Trn = H * Psc^Trn.",
				"M is a matrix representation of the differential operator S = e^{c.*D} with D = d/dx, which acting on x^m gives the Appell polynomial p_m(x) = (c. + x)^m, with (c.)^k = c_k an arbitrary indeterminate except for c_0 = 1. For example, S x^2 = (c. + x)^2 = c_0*x^2 + 2*c_1*x + c_2, and M * (0,0,1,0,0,...)^Trn = (c_2,2*c_1,c_0,0,0,...)^Trn = V, so V^Trn = (0,0,1,0,...) * M^Trn = (0,0,1,0,...) * Psc * H = (c_2,2*c_1,c_0,0,...).",
				"The differential lowering and raising operators for the Appell sequence are given by L = D and R = x + dlog(S)/dD, with L p_n(x = n * p_(n-1)(x) and R p_n(x) = p_(n+1)(x).",
				"(End)"
			],
			"link": [
				"Tom Copeland, \u003ca href=\"http://tcjpn.wordpress.com/2012/11/29/infinigens-the-pascal-pyramid-and-the-witt-and-virasoro-algebras/\"\u003eInfinitesimal Generators, the Pascal Pyramid, and the Witt and Virasoro Algebras\u003c/a\u003e",
				"P. Olver, \u003ca href=\"http://www.math.umn.edu/~olver/di_/contact.pdf\"\u003eThe canonical contact form\u003c/a\u003e."
			],
			"formula": [
				"The matrix operation b = T*a can be characterized in several ways in terms of the coefficients a(n) and b(n), their o.g.f.s A(x) and B(x), or e.g.f.s EA(x) and EB(x):",
				"  1) b(n) = (n+1) * a(n+1),",
				"  2) B(x) = D A(x), or",
				"  3) EB(x) = DxD  EA(x),",
				"  where D is the derivative w.r.t. x.",
				"So the exponentiated operator can be characterized as",
				"  4) exp(t*T) A(x) = exp(t*D) A(x) = A(x+t),",
				"  5) exp(t*T) EA(x) = exp(t*DxD) EA(x) = exp[x*a/(1+t*a)]/(1+t*a),",
				"     = Sum_{n\u003e=0} (1+t*a)^(-n-1) (x*a)^n/n!, where umbrally",
				"     a^n *(1+t*a)^(-n-1) = Sum_{j\u003e0} binomial(n+j,j)a(n+j)t^j,",
				"  6) exp(t*T) EA(x) = Sum_{n\u003e=0} a(n) t^n Lag(n,-x/t),",
				"     where Lag(n,x) are the Laguerre polynomials (A021009), or",
				"  7) [exp(t*T) * a]_n = [M(t) * a]_n",
				"     = Sum_{j\u003e=0} binomial(n+j,j)a(n+j)t^j.",
				"For more on the operator DxD, see A021009 and references in A132440."
			],
			"example": [
				"Matrix T begins",
				"  0,1;",
				"  0,0,2;",
				"  0,0,0,3;",
				"  0,0,0,0,4;",
				"  0,0,0,0,0,5;",
				"  0,0,0,0,0,0,6;",
				"  ..."
			],
			"mathematica": [
				"Table[PadLeft[{n+1}, n+2], {n, 0, 11}] // Flatten (* _Jean-François Alcover_, Apr 30 2014 *)"
			],
			"xref": [
				"Cf. A134402."
			],
			"keyword": "nonn,easy,tabf,changed",
			"offset": "0,5",
			"author": "_Tom Copeland_, Oct 24 2012",
			"references": 10,
			"revision": 42,
			"time": "2022-01-13T02:30:02-05:00",
			"created": "2012-11-01T13:57:27-04:00"
		}
	]
}