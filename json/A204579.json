{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A204579",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 204579,
			"data": "1,-1,1,4,-5,1,-36,49,-14,1,576,-820,273,-30,1,-14400,21076,-7645,1023,-55,1,518400,-773136,296296,-44473,3003,-91,1,-25401600,38402064,-15291640,2475473,-191620,7462,-140,1,1625702400,-2483133696",
			"name": "Triangle read by rows: matrix inverse of A036969.",
			"comment": [
				"This is a signed version of A008955 with rows in reverse order. - _Peter Luschny_, Feb 04 2012"
			],
			"link": [
				"M. W. Coffey, M. C. Lettington, \u003ca href=\"http://arxiv.org/abs/1510.05402\"\u003eOn Fibonacci Polynomial Expressions for Sums of mth Powers, their implications for Faulhaber's Formula and some Theorems of Fermat\u003c/a\u003e, arXiv:1510.05402 [math.NT], 2015."
			],
			"formula": [
				"T(n,k) = (-1)^(n-k)*A008955(n, n-k). - _Peter Luschny_, Feb 05 2012",
				"T(n,k) = Sum_{i=k-n..n-k} (-1)^(n-k+i)*s(n,k+i)*s(n,k-i) = Sum_{i=0..2*k} (-1)^(n+i)*s(n,i)*s(n,2*k-i), where s(n,k) are Stirling numbers of the first kind, A048994. - _Mircea Merca_, Apr 07 2012",
				"From _Peter Bala_, Aug 29 2012: (Start)",
				"Recurrence equation: T(n,k) = T(n-1,k-1) - (n-1)^2*T(n-1,k).",
				"Let E(x) = cosh(sqrt(2*x)) = Sum_{n \u003e= 0} x^n/{(2*n)!/2^n} and",
				"L(x) = 2*{arcsinh(sqrt(x/2))}^2 = Sum_{n \u003e=1} (-1)^n*(n-1)!^2*x^n/{(2*n)!/2^n}.",
				"L(x) is the compositional inverse of E(x) - 1.",
				"A generating function for the triangle is E(t*L(x)) = 1 + t*x + t*(-1 + t)*x^2/6 + t*(4 - 5*t + t^2)*x^3/90 + ..., where the sequence of denominators [1,1,6,90,...] is given by (2*n)!/2^n. Cf. A008275 with generating function exp(t*log(1+x)).",
				"The e.g.f. is E(t*L(x^2/2)) = cosh(2*sqrt(t)*arcsinh(x/2)) = 1 + t*x^2/2! + t*(t-1)*x^4/4! + t*(t-1)*(t-4)*x^6/6! + ....",
				"(End)"
			],
			"example": [
				"Padding A036969 with zeros yields the infinite square matrix",
				"[ 1  0  0  0 ...]",
				"[ 1  1  0  0 ...]",
				"[ 1  5  1  0 ...]",
				"[ 1 21 14  1 ...]",
				"with inverse",
				"[  1  0  0  0 ...]",
				"[ -1  1  0  0 ...]",
				"[  4 -5  1  0 ...]",
				"[-36 49 -14 1 ...]."
			],
			"mathematica": [
				"rows = 10;",
				"t[n_, k_] := 2*Sum[j^(2*n)*(-1)^(k - j)/((k - j)!*(k + j)!), {j, 1, k}];",
				"T = Table[t[n, k], {n, 1, rows}, {k, 1, rows}] // Inverse;",
				"Table[T[[n, k]], {n, 1, rows}, {k, 1, n}] // Flatten (* _Jean-François Alcover_, Jul 14 2018 *)"
			],
			"program": [
				"(PARI) select(concat(Vec(matrix(10,10,n,k,T(n,k)/*from A036969*/)~^-1)), x-\u003ex)",
				"(Sage)",
				"def A204579(n, k): return (-1)^(n-k)*A008955(n, n-k)",
				"for n in (0..7): print([A204579(n, k) for k in (0..n)]) # _Peter Luschny_, Feb 05 2012"
			],
			"xref": [
				"Cf. A008955. A008275, A121408."
			],
			"keyword": "sign,tabl",
			"offset": "1,4",
			"author": "_M. F. Hasler_, Feb 03 2012",
			"ext": [
				"Typo in data corrected by _Peter Luschny_, Feb 05 2012"
			],
			"references": 3,
			"revision": 39,
			"time": "2020-03-16T13:08:00-04:00",
			"created": "2012-02-05T11:52:22-05:00"
		}
	]
}