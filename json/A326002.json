{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A326002",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 326002,
			"data": "1,2,5,4,11,6,22,8,29,22,41,12,89,14,71,76,109,18,214,20,196,190,155,24,573,56,209,388,519,30,877,32,809,694,341,316,2119,38,419,1132,2411,42,2045,44,2531,2986,599,48,6053,106,3011,2500,4759,54,4978,4016,6589,3478,929,60,21468,62,1055,5524,10713,10076,12046,68,13499,6142,18656,72,34474,74,1481,29716,20939,5622,28432,80,57921,10000,1805,84,84155,42926,1979,12268,41449,90,122339,24116,44759,14974,2351,77616,153969",
			"name": "G.f.: Sum_{n\u003e=0} (n+1) * x^n * (1 + x^n)^n.",
			"comment": [
				"More generally, the following sums are equal:",
				"(1) Sum_{n\u003e=0} binomial(n+k-1, n) * r^n * (p + q^n)^n,",
				"(2) Sum_{n\u003e=0} binomial(n+k-1, n) * r^n * q^(n^2) / (1 - p*q^n*r)^(n+k),",
				"for some fixed integer k; here, k = 2 and p = 1, q = x, r = x."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A326002/b326002.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e"
			],
			"formula": [
				"Generating functions.",
				"(1) Sum_{n\u003e=0} (n+1) * x^n * (1 + x^n)^n.",
				"(2) Sum_{n\u003e=0} (n+1) * x^(n*(n+1)) / (1 - x^(n+1))^(n+2)."
			],
			"example": [
				"G.f.: A(x) = 1 + 2*x + 5*x^2 + 4*x^3 + 11*x^4 + 6*x^5 + 22*x^6 + 8*x^7 + 29*x^8 + 22*x^9 + 41*x^10 + 12*x^11 + 89*x^12 + 14*x^13 + 71*x^14 + 76*x^15 + 109*x^16 + 18*x^17 + 214*x^18 + 20*x^19 + 196*x^20 + ...",
				"where we have the following series identity:",
				"A(x) = 1 + 2*x*(1+x) + 3*x^2*(1+x^2)^2 + 4*x^3*(1+x^3)^3 + 5*x^4*(1+x^4)^4 + 6*x^5*(1+x^5)^5  + 7*x^6*(1+x^6)^6 + 8*x^7*(1+x^7)^7 + 9*x^8*(1+x^8)^8 + 10*x^9*(1+x^9)^9 + ...",
				"is equal to",
				"A(x) = 1/(1-x)^2 + 2*x^2/(1-x^2)^3 + 3*x^6/(1-x^3)^4 + 4*x^12/(1-x^4)^5 + 5*x^20/(1-x^5)^6 + 6*x^30/(1-x^6)^7 + 7*x^42/(1-x^7)^8 + 8*x^56/(1-x^8)^9 + ..."
			],
			"maple": [
				"N:= 100: # for a(0)..a(N)",
				"S:= series(add((n+1)*x^n*(1+x^n)^n,n=0..N),x,N+1):",
				"seq(coeff(S,x,n),n=0..N); # _Robert Israel_, Jun 03 2019"
			],
			"program": [
				"(PARI) {a(n) = my(A = sum(m=0,n, (m+1) * x^m * (1 + x^m +x*O(x^n))^m)); polcoeff(A,n)}",
				"for(n=0,120,print1(a(n),\", \"))",
				"(PARI) {a(n) = my(A = sum(m=0,n, (m+1) * x^m * x^(m^2) / (1 - x^(m+1) +x*O(x^n))^(m+2))); polcoeff(A,n)}",
				"for(n=0,120,print1(a(n),\", \"))"
			],
			"xref": [
				"Cf. A217668 (k=1), A326003 (k=3), A326004 (k=4), A326005 (k=5)."
			],
			"keyword": "nonn,look",
			"offset": "0,2",
			"author": "_Paul D. Hanna_, Jun 01 2019",
			"references": 4,
			"revision": 14,
			"time": "2019-07-02T22:34:36-04:00",
			"created": "2019-06-01T17:23:47-04:00"
		}
	]
}