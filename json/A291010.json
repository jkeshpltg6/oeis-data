{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A291010",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 291010,
			"data": "5,24,108,468,1980,8244,33948,138708,563580,2280564,9200988,37040148,148869180,597602484,2396787228,9606280788,38482518780,154102262004,616925608668,2469252116628,9881657512380,39540577187124,158204150161308,632942124883668",
			"name": "p-INVERT of (1,1,1,1,1,...), where p(S) = (1 - 2 S)(1 - 3 S).",
			"comment": [
				"Suppose s = (c(0), c(1), c(2),...) is a sequence and p(S) is a polynomial. Let S(x) = c(0)*x + c(1)*x^2 + c(2)*x^3 + ... and T(x) = (-p(0) + 1/p(S(x)))/x. The p-INVERT of s is the sequence t(s) of coefficients in the Maclaurin series for T(x).  Taking p(S) = 1 - S gives the \"INVERT\" transform of s, so that p-INVERT is a generalization of the \"INVERT\" transform (e.g., A033453).",
				"See A291000 for a guide to related sequences."
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A291010/b291010.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (7, -12)"
			],
			"formula": [
				"G.f.: (5 - 11 x)/(1 - 7 x + 12 x^2).",
				"a(n) = 7*a(n-1) - 12*a(n-2) for n \u003e= 3.",
				"a(n) = 9*4^n - 4*3^n. - _Colin Barker_, Aug 23 2017"
			],
			"mathematica": [
				"z = 60; s = x/(1 - x); p = (1 - 2 s) (1 - 3 s);",
				"Drop[CoefficientList[Series[s, {x, 0, z}], x], 1]  (* A000012 *)",
				"Drop[CoefficientList[Series[1/p, {x, 0, z}], x], 1]  (* A291010 *)"
			],
			"program": [
				"(PARI) Vec((5 - 11*x) / ((1 - 3*x)*(1 - 4*x)) + O(x^30)) \\\\ _Colin Barker_, Aug 23 2017"
			],
			"xref": [
				"Cf. A000012, A289780, A291000."
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "_Clark Kimberling_, Aug 23 2017",
			"references": 2,
			"revision": 8,
			"time": "2017-08-23T16:04:03-04:00",
			"created": "2017-08-23T16:04:03-04:00"
		}
	]
}