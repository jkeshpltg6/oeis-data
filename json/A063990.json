{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A063990",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 63990,
			"data": "220,284,1184,1210,2620,2924,5020,5564,6232,6368,10744,10856,12285,14595,17296,18416,63020,66928,66992,67095,69615,71145,76084,79750,87633,88730,100485,122265,122368,123152,124155,139815,141664,142310",
			"name": "Amicable numbers.",
			"comment": [
				"A pair of numbers x and y is called amicable if the sum of the proper divisors of either one is equal to the other. The smallest pair is x = 220, y = 284.",
				"The sequence lists the amicable numbers in increasing order. Note that the pairs x, y are not adjacent to each other in the list. See also A002025 for the x's, A002046 for the y's.",
				"Theorem: If the three numbers p = 3*(2^(n-1)) - 1, q = 3*(2^n) - 1 and r = 9*(2^(2n-1)) - 1 are all prime where n \u003e= 2, then p*q*(2^n) and r*(2^n) are amicable numbers. This 9th century theorem is due to Thabit ibn Kurrah (see for example, the History of Mathematics by David M. Burton, 6th ed., p. 510). - _Mohammad K. Azarian_, May 19 2008",
				"The first time a pair ordered by its first element is not adjacent is x = 63020, y = 76084 which correspond to a(17) and a(23), respectively. - _Omar E. Pol_, Jun 22 2015",
				"For amicable pairs see A259180 and also A259933. First differs from A259180 (amicable pairs) at a(18). - _Omar E. Pol_, Jun 01 2017",
				"Sierpiński (1964), page 176, mentions Erdős's work on the number of pairs of amicable numbers \u003c= x. - _N. J. A. Sloane_, Dec 27 2017",
				"Kanold (1954) proved that the asymptotic upper density of amicable numbers is \u003c 0.204 and Erdős (1955) proved that it is 0. - _Amiram Eldar_, Feb 13 2021"
			],
			"reference": [
				"Scott T. Cohen, Mathematical Buds, Ed. Harry D. Ruderman, Vol. 1, Chap. VIII, pp. 103-126, Mu Alpha Theta, 1984.",
				"Clifford A. Pickover, The Math Book, Sterling, NY, 2009; see p. 90.",
				"Wacław Sierpiński, Elementary Theory of Numbers, Panst. Wyd. Nauk, Warsaw, 1964.",
				"David Wells, The Penguin Dictionary of Curious and Interesting Numbers, pp. 145-7, Penguin Books, 1987."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A063990/b063990.txt\"\u003eTable of n, a(n) for n = 1..77977\u003c/a\u003e (terms \u003c 10^14 from Pedersen's tables)",
				"Titu Andreescu, \u003ca href=\"http://staff.imsa.edu/math/journal/volume3/articles/NumberTrivia.pdf\"\u003eNumber Theory Trivia: Amicable Numbers\u003c/a\u003e.",
				"Titu Andreescu, \u003ca href=\"http://britton.disted.camosun.bc.ca/amicable.html\"\u003eNumber Theory Trivia: Amicable Numbers\u003c/a\u003e.",
				"Anonymous, \u003ca href=\"http://nautilus.fis.uc.pt/mn/i_amigos/amigos.swf\"\u003eAmicable Pairs Applet Test\u003c/a\u003e.",
				"Anonymous, \u003ca href=\"http://www-maths.swan.ac.uk/pgrads/bb/project/node16.html\"\u003eAmicable and Social Numbers\u003c/a\u003e. [broken link]",
				"Jonathan Bayless and Dominic Klyve, \u003ca href=\"http://math.colgate.edu/~integers/a3.5int2009/a3.5int2009.pdf\"\u003eOn the sum of reciprocals of amicable numbers\u003c/a\u003e, Integers, Vol. 11A (2011), Article 5.",
				"Sergei Chernykh, \u003ca href=\"/A063990/a063990-6M.zip\"\u003eTable of n, a(n) for n = 1..823818, zipped file\u003c/a\u003e (results of an exhaustive search for all amicable pairs with smaller member \u003c 10^17).",
				"Sergei Chernykh, \u003ca href=\"http://sech.me/ap/\"\u003eAmicable pairs list\u003c/a\u003e.",
				"Germano D'Abramo, \u003ca href=\"http://arXiv.org/abs/math.HO/0501402\"\u003eOn Amicable Numbers With Different Parity\u003c/a\u003e, arXiv:math/0501402 [math.HO], 2005-2007.",
				"Paul Erdős, \u003ca href=\"https://users.renyi.hu/~p_erdos/1955-03.pdf\"\u003eOn amicable numbers\u003c/a\u003e, Pub. Math. Debrecen, Vol. 4 (1955), pp. 108-111.",
				"Leonhard Euler, \u003ca href=\"http://arXiv.org/abs/math.HO/0409196\"\u003eOn amicable numbers\u003c/a\u003e, arXiv:math/0409196 [math.HO], 2004-2009.",
				"Steven Finch, \u003ca href=\"/A000396/a000396.pdf\"\u003eAmicable Pairs and Aliquot Sequences\u003c/a\u003e, 2013. [Cached copy, with permission of the author]",
				"Mariano García, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL4/GARCIA/millionc.html\"\u003eA Million New Amicable Pairs\u003c/a\u003e, J. Integer Sequences, Vol. 4 (2001), Article #01.2.6.",
				"Mariano García, Jan Munch Pedersen, Herman te Riele, \u003ca href=\"http://oai.cwi.nl/oai/asset/4143/04143D.pdf\"\u003eAmicable pairs, a survey\u003c/a\u003e, Report MAS-R0307, Centrum Wiskunde \u0026 Informatica.",
				"Hans-Joachim Kanold, \u003ca href=\"https://doi.org/10.1007/BF01181341\"\u003eÜber die Dichten der Mengen der vollkommenen und der befreundeten Zahlen\u003c/a\u003e, Mathematische Zeitschrift, Vol. 61 (1954), pp. 180-185.",
				"Hisanori Mishima, \u003ca href=\"http://www.asahi-net.or.jp/~kc2h-msm/mathland/math09/ami02.htm\"\u003eAmicable Numbers: first 236 pairs (smaller member \u003c 10^8) fully factorized\u003c/a\u003e.",
				"David Moews, \u003ca href=\"http://djm.cc/amicable2.txt\"\u003eA List Of The First 5001 Amicable Pairs\u003c/a\u003e.",
				"David and P. C. Moews, \u003ca href=\"http://djm.cc/amicable.txt\"\u003eA List Of Amicable Pairs Below 2.01*10^11\u003c/a\u003e",
				"Hanh My Nguyen and Carl Pomerance, \u003ca href=\"https://doi.org/10.1090/mcom/3362\"\u003eThe reciprocal sum of the amicable numbers\u003c/a\u003e, Mathematics of Computation, Vol. 88, No. 317 (2019), pp. 1503-1526, \u003ca href=\"https://math.dartmouth.edu/~carlp/mcom3362.pdf\"\u003ealternative link\u003c/a\u003e.",
				"Number Theory List, \u003ca href=\"http://listserv.nodak.edu/cgi-bin/wa.exe?A1=ind9308\u0026amp;L=nmbrthry\"\u003eNMBRTHRY Archives--August 1993\u003c/a\u003e.",
				"J. O. M. Pedersen, \u003ca href=\"http://amicable.homepage.dk/tables.htm\"\u003eTables of Aliquot Cycles\u003c/a\u003e. [Broken link]",
				"J. O. M. Pedersen, \u003ca href=\"http://web.archive.org/web/20140502102524/http://amicable.homepage.dk/tables.htm\"\u003eTables of Aliquot Cycles\u003c/a\u003e. [Via Internet Archive Wayback-Machine]",
				"J. O. M. Pedersen, \u003ca href=\"/A063990/a063990.pdf\"\u003eTables of Aliquot Cycles\u003c/a\u003e [Cached copy, pdf file only]",
				"Ivars Peterson, \u003ca href=\"https://web.archive.org/web/20130628060857/http://www.maa.org/mathland/mathtrek_2_26_01.html\"\u003eAppealing Numbers\u003c/a\u003e, MathTrek, 2001.",
				"Ivars Peterson, \u003ca href=\"https://web.archive.org/web/20130126192043/http://maa.org/mathland/mathtrek_02_02_04.html\"\u003eAmicable Pairs, Divisors and a New Record\u003c/a\u003e, MathTrek, 2004.",
				"Paul Pollack, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL14/Pollack/pollack3.html\"\u003eQuasi-Amicable Numbers are Rare\u003c/a\u003e, J. Int. Seq., Vol. 14 (2011), Article # 11.5.2.",
				"Carl Pomerance, \u003ca href=\"https://doi.org/10.1007/978-3-319-22240-0_19\"\u003eOn amicable numbers\u003c/a\u003e, in: C. Pomerance and M. Rassias M. (eds.), Analytic number theory, Springer, Cham, 2015, pp. 321-327; \u003ca href=\"https://math.dartmouth.edu/~carlp/amicablesv3.pdf\"\u003ealternative link\u003c/a\u003e.",
				"Herman J. J. te Riele, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1984-0725997-0\"\u003eOn generating new amicable pairs from given amicable pairs\u003c/a\u003e, Math. Comp., Vol. 42, No. 165 (1984), pp. 219-223.",
				"Herman J. J. te Riele, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1986-0842142-3\"\u003eComputation of all the amicable pairs below 10^10\u003c/a\u003e, Math. Comp., Vol. 47, No. 175 (1986), pp. 361-368 and Supplement pp. S9-S40.",
				"Herman J. J. te Riele, \u003ca href=\"https://core.ac.uk/download/pdf/301661745.pdf\"\u003eA New Method for Finding Amicable Pairs\u003c/a\u003e, Proceedings of Symposia in Applied Mathematics, Volume 48, 1994.",
				"Ed Sandifer, \u003ca href=\"https://web.archive.org/web/20130126165856/http://maa.org/editorial/euler/How%20Euler%20Did%20It%2025%20amicable%20numbers.pdf\"\u003eAmicable numbers\u003c/a\u003e.",
				"Gérard Villemin's Almanach of Numbers, \u003ca href=\"http://villemin.gerard.free.fr/Wwwgvmm/Decompos/Amiable.htm\"\u003eNombres amiables et sociables\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/AmicablePair.html\"\u003eAmicable Pair\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"http://www.wikipedia.org/wiki/Amicable_number\"\u003eAmicable number\u003c/a\u003e."
			],
			"formula": [
				"Pomerance shows that there are at most x/exp(sqrt(log x log log log x)/(2 + o(1))) terms up to x for sufficiently large x. - _Charles R Greathouse IV_, Jul 21 2015",
				"Sum_{n\u003e=1} 1/a(n) is in the interval (0.0119841556, 227) (Nguyen and Pomerance, 2019; an upper bound 6.56*10^8 was given by Bayless and Klyve, 2011). - _Amiram Eldar_, Oct 15 2020"
			],
			"maple": [
				"F:= proc(t) option remember; numtheory:-sigma(t)-t end proc:",
				"select(t -\u003e F(t) \u003c\u003e t and F(F(t))=t, [$1.. 200000]); # _Robert Israel_, Jun 22 2015"
			],
			"mathematica": [
				"s[n_] := DivisorSigma[1, n] - n; AmicableNumberQ[n_] := If[Nest[s, n, 2] == n \u0026\u0026 ! s[n] == n, True, False]; Select[Range[10^6], AmicableNumberQ[ # ] \u0026] (* _Ant King_, Jan 02 2007 *)"
			],
			"program": [
				"(PARI) aliquot(n)=sigma(n)-n",
				"isA063990(n)={local(a);a=aliquot(n);a\u003c\u003en \u0026\u0026 aliquot(a)==n} \\\\ _Michael B. Porter_, Apr 13 2010",
				"(Python)",
				"from sympy import divisors",
				"A063990 = [n for n in range(1,10**5) if sum(divisors(n))-2*n and not sum(divisors(sum(divisors(n))-n))-sum(divisors(n))] # _Chai Wah Wu_, Aug 14 2014"
			],
			"xref": [
				"Union of A002025 and A002046.",
				"A180164 (gives for each pair (x, y) the value x+y = sigma(x)+sigma(y)).",
				"Cf. A259180."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_, Sep 18 2001.",
			"references": 114,
			"revision": 153,
			"time": "2021-11-19T07:21:27-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}