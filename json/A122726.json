{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A122726",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 122726,
			"data": "12496,14264,14288,14316,14536,15472,17716,19116,19916,22744,22976,31704,45946,47616,48976,83328,97946,122410,152990,177792,243760,274924,275444,285778,294896,295488,358336,366556,376736,381028,418904,589786",
			"name": "Conjectured list of sociable numbers.",
			"comment": [
				"Comments from David Moews, Sep 17 2021 (Start)",
				"It is possible that there are quite small numbers missing from this sequence. There is no proof that 564 (for example) is missing.",
				"Let s(n) = sigma(n)-n denote the sum of the divisors of n, excluding n itself. The aliquot sequence starting at n is the sequence n, s(n), s(s(n)), s(s(s(n))), ...",
				"Starting at 564, the aliquot sequence continues for at least 3486 steps, reaching a 198-digit number after 3486 iterations of s.  In the reverse direction, 563 = 564 - 1 is prime so s(563^2) = 564 and also s(7*316961) = 563^2, s(17*2218709) = 7*316961, etc.; given a strengthened form of the Goldbach conjecture (see Booker, 2018), one can continue iterating s^(-1) indefinitely.",
				"Although it seems unlikely, I don't see any way to be completely certain that the forward aliquot sequence doesn't meet the backwards tree; if it did, 564 would be part of a (very long) aliquot cycle.",
				"Many other numbers below 79750 are in a similar situation (although not 276, because it is not in the image of s).",
				"(Added Sep 18 2021) The smallest uncertain number is 564.  All smaller numbers either have known aliquot sequences (all except 276, 306, 396, and 552), are not in the image of s (276, 306, and 552), or are in the image of s but not the image of s^2 (396).",
				"(End)"
			],
			"reference": [
				"Andrew R. Booker, Finite connected components of the aliquot graph, Math. Comp. 87 (2018), 2891-2902."
			],
			"link": [
				"Steven Finch, \u003ca href=\"/A000396/a000396.pdf\"\u003eAmicable Pairs and Aliquot Sequences\u003c/a\u003e, 2013. [Cached copy, with permission of the author]",
				"David Moews, \u003ca href=\"http://djm.cc/amicable.html\"\u003ePerfect, amicable and sociable numbers\u003c/a\u003e",
				"D. Moews, \u003ca href=\"http://djm.cc/sociable.txt\"\u003eA list of currently known aliquot cycles of length greater than 2\u003c/a\u003e [This list is not known to be complete.]",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/SociableNumbers.html\"\u003eSociable Numbers\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Sociable_number\"\u003eSociable number\u003c/a\u003e"
			],
			"example": [
				"The smallest sociable number cycle is {12496, 14288, 15472, 14536, 14264, 12496}."
			],
			"xref": [
				"Cf. A003416 (smallest member of each cycle), A063990 (amicable numbers), A052470."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Tanya Khovanova_, Sep 23 2006",
			"ext": [
				"Edited (including adding comments from David Moews that this is only conjectural) by _N. J. A. Sloane_, Sep 17 2021"
			],
			"references": 8,
			"revision": 26,
			"time": "2021-09-18T17:56:30-04:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}