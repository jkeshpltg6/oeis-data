{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A198328",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 198328,
			"data": "1,1,2,1,3,2,2,1,4,3,5,2,3,2,6,1,3,4,2,3,4,5,7,2,9,3,8,2,5,6,11,1,10,3,6,4,3,2,6,3,5,4,3,5,12,7,13,2,4,9,6,3,2,8,15,2,4,5,5,6,7,11,8,1,9,10,3,3,14,6,5,4,7,3,18,2,10,6,11,3,16,5",
			"name": "The Matula-Goebel number of the rooted tree obtained from the rooted tree with Matula-Goebel number n after removing the leaves, together with their incident edges.",
			"comment": [
				"This is not the pruning operation mentioned in the Balaban reference (p. 360) and in the Todeschini-Consonni reference (p. 42) since in the case that the root has degree 1, this root and the incident edge are not deleted."
			],
			"reference": [
				"A. T. Balaban, Chemical graphs, Theoret. Chim. Acta (Berl.) 53, 355-375, 1979.",
				"R. Todeschini and V. Consonni, Handbook of Molecular Descriptors, Wiley-VCH, 2000."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A198328/b198328.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"F. Goebel, \u003ca href=\"http://dx.doi.org/10.1016/0095-8956(80)90049-0\"\u003eOn a 1-1-correspondence between rooted trees and natural numbers\u003c/a\u003e, J. Combin. Theory, B 29 (1980), 141-143.",
				"I. Gutman and A. Ivic, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(95)00182-V\"\u003eOn Matula numbers\u003c/a\u003e, Discrete Math., 150, 1996, 131-142.",
				"I. Gutman and Yeong-Nan Yeh, \u003ca href=\"http://www.emis.de/journals/PIMB/067/3.html\"\u003eDeducing properties of trees from their Matula numbers\u003c/a\u003e, Publ. Inst. Math., 53 (67), 1993, 17-22.",
				"D. Matula, \u003ca href=\"http://www.jstor.org/stable/2027327\"\u003eA natural rooted tree enumeration by prime factorization\u003c/a\u003e, SIAM Rev. 10 (1968) 273.",
				"\u003ca href=\"/index/Mat#matula\"\u003eIndex entries for sequences related to Matula-Goebel numbers\u003c/a\u003e"
			],
			"formula": [
				"a(1)=1; a(2)=1; if n=p(t) (the t-th prime, t\u003e1), then a(n)=p(a(t)); if n=rs (r,s,\u003e=2), then a(n)=a(r)a(s). The Maple program is based on this recursive formula.",
				"Completely multiplicative with a(2) = 1, a(prime(t)) = prime(a(t)) for t \u003e 1. - _Andrew Howroyd_, Aug 01 2018"
			],
			"example": [
				"a(7)=2 because the rooted tree with Matula-Goebel number 7 is Y; after deleting the leaves and their incident edges, we obtain the 1-edge tree having Matula-Goebel number 2."
			],
			"maple": [
				"with(numtheory): a := proc (n) local r, s: r := proc (n) options operator, arrow: op(1, factorset(n)) end proc: s := proc (n) options operator, arrow: n/r(n) end proc: if n = 1 then 1 elif n = 2 then 1 elif bigomega(n) = 1 then ithprime(a(pi(n))) else a(r(n))*a(s(n)) end if end proc; seq(a(n), n = 1 .. 120);"
			],
			"mathematica": [
				"a[1] = a[2] = 1; a[n_] := a[n] = If[PrimeQ[n], Prime[a[PrimePi[n]]], Times @@ (a[#[[1]]]^#[[2]]\u0026 /@ FactorInteger[n])];",
				"Array[a, 100] (* _Jean-François Alcover_, Dec 18 2017 *)"
			],
			"program": [
				"(Haskell)",
				"import Data.List (genericIndex)",
				"a198328 n = genericIndex a198328_list (n - 1)",
				"a198328_list = 1 : 1 : g 3 where",
				"   g x = y : g (x + 1) where",
				"     y = if t \u003e 0 then a000040 (a198328 t) else a198328 r * a198328 s",
				"         where t = a049084 x; r = a020639 x; s = x `div` r",
				"-- _Reinhard Zumkeller_, Sep 03 2013"
			],
			"xref": [
				"Cf. A198329.",
				"Cf. A049084, A020639, A000040."
			],
			"keyword": "nonn,mult",
			"offset": "1,3",
			"author": "_Emeric Deutsch_, Nov 24 2011",
			"references": 2,
			"revision": 22,
			"time": "2018-08-01T18:14:36-04:00",
			"created": "2011-11-25T17:10:08-05:00"
		}
	]
}