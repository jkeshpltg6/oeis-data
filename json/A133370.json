{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A133370",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 133370,
			"data": "2,3,7,13,23,29,43,47,53,67,71,79,83,89,101,103,107,109,113,127,131,137,149,157,167,173,199,223,229,239,263,269,277,281,311,313,317,337,349,353,359,373,383,389,397,401,409,421,449,457,461,467,479,487,491",
			"name": "Primes p such that p does not divide any term of the Apery sequence A005259 .",
			"comment": [
				"Malik and Straub give arguments suggesting that this sequence is infinite. - _N. J. A. Sloane_, Aug 06 2017"
			],
			"link": [
				"Robert Price, \u003ca href=\"/A133370/b133370.txt\"\u003eTable of n, a(n) for n = 1..758\u003c/a\u003e",
				"Amita Malik and Armin Straub, \u003ca href=\"https://doi.org/10.1007/s40993-016-0036-8\"\u003eDivisibility properties of sporadic Apéry-like numbers\u003c/a\u003e, Research in Number Theory, 2016, 2:5.",
				"Amita Malik, \u003ca href=\"/A133370/a133370.nb\"\u003eMathematica notebook for generating this sequence and A260793, A291275-A291284\u003c/a\u003e",
				"Amita Malik, \u003ca href=\"/A133370/a133370.pdf\"\u003eList of all primes up to 10000 in this sequence and in A260793, A291275-A291284, together with Mathematica code.\u003c/a\u003e",
				"E. Rowland, R. Yassawi, \u003ca href=\"https://arxiv.org/abs/1310.8635\"\u003eAutomatic congruences for diagonals of rational functions\u003c/a\u003e, arXiv preprint arXiv:1310.8635 [math.NT], 2013."
			],
			"mathematica": [
				"NeverDividesLucasSeqQ[a_, p_] := And @@ Table[Mod[a[n], p]\u003e0, {n, 0, p-1}];",
				"A3[a_, b_, c_, n_ /; n \u003c 0] = 0;",
				"A3[a_, b_, c_, 0] = 1;",
				"A3[a_, b_, c_, n_] := A3[a, b, c, n] = (((2n - 1)(a (n-1)^2 + a (n-1) + b)) A3[a, b, c, n-1] - c (n-1)^3 A3[a, b, c, n-2])/n^3;",
				"A3[a_, b_, c_, d_, n_ /; n \u003c 0] = 0;",
				"Agamma[n_] := A3[17, 5, 1, n];",
				"Select[Range[1000], PrimeQ[#] \u0026\u0026 NeverDividesLucasSeqQ[Agamma, #]\u0026] (* _Jean-François Alcover_, Aug 05 2018, copied from Amita Malik's notebook *)"
			],
			"xref": [
				"For primes that do not divide the terms of the sequences A000172, A005258, A002893, A081085, A006077, A093388, A125143, A229111, A002895, A290575, A290576, A005259 see A260793, A291275-A291284 and A133370 respectively."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Philippe Deléham_, Oct 27 2007",
			"ext": [
				"Terms a(16) onwards computed by Amita Malik - _N. J. A. Sloane_, Aug 21 2017"
			],
			"references": 24,
			"revision": 39,
			"time": "2018-09-18T21:51:21-04:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}