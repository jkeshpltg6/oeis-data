{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A269419",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 269419,
			"data": "1,48,4608,55296,42467328,84934656,21743271936,36691771392,400771988324352,1352605460594688,16620815899787526144,779100745302540288,153177439332441840943104,2393397489569403764736,235280546814630667688607744,57441539749665690353664",
			"name": "a(n) is denominator of y(n), where y(n+1) = (25*n^2-1)/48 * y(n) + (1/2)*Sum_{k=1..n}y(k)*y(n+1-k), with y(0) = -1.",
			"link": [
				"Gheorghe Coserea, \u003ca href=\"/A269419/b269419.txt\"\u003eTable of n, a(n) for n = 0..500\u003c/a\u003e",
				"Edward A. Bender, Zhicheng Gao, L. Bruce Richmond, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v15i1r51\"\u003e The map asymptotics constant tg\u003c/a\u003e, The Electronic Journal of Combinatorics, Volume 15 (2008), Research Paper #R51.",
				"Stavros Garoufalidis, Thang T.Q. Le, Marcos Marino, \u003ca href=\"http://arxiv.org/abs/0809.2572\"\u003eAnalyticity of the Free Energy of a Closed 3-Manifold\u003c/a\u003e, arXiv:0809.2572 [math.GT], 2008."
			],
			"formula": [
				"t(g) = (A269418(g)/A269419(g)) / (2^(g-2) * gamma((5*g-1)/2)), where t(g) is the orientable map asymptotics constant and gamma is the Gamma function."
			],
			"example": [
				"For n=0 we have t(0) = (-1) / (2^(-2)*gamma(-1/2)) = 2/sqrt(Pi).",
				"For n=1 we have t(1) = (1/48) / (2^(-1)*gamma(2))  = 1/24.",
				"n   y(n)                        t(n)",
				"0   -1                          2/sqrt(Pi)",
				"1   1/48                        1/24",
				"2   49/4608                     7/(4320*sqrt(Pi))",
				"3   1225/55296                  245/15925248",
				"4   4412401/42467328            37079/(96074035200*sqrt(Pi))",
				"5   73560025/84934656           38213/14089640214528",
				"6   245229441961/21743271936    5004682489/(92499927372103680000*sqrt(Pi))",
				"7   7759635184525/36691771392   6334396069/20054053184087387013120",
				"..."
			],
			"mathematica": [
				"y[0] = -1;",
				"y[n_] := y[n] = (25(n-1)^2-1)/48 y[n-1] + 1/2 Sum[y[k] y[n-k], {k, 1, n-1}];",
				"Table[y[n] // Denominator, {n, 0, 15}] (* _Jean-François Alcover_, Oct 23 2018 *)"
			],
			"program": [
				"(PARI)",
				"seq(n) = {",
				"  my(y  = vector(n));",
				"  y[1] = 1/48;",
				"  for (g = 1, n-1,",
				"       y[g+1] = (25*g^2-1)/48 * y[g] + 1/2*sum(k = 1, g, y[k]*y[g+1-k]));",
				"  return(concat(-1,y));",
				"}",
				"apply(denominator, seq(14))"
			],
			"xref": [
				"Cf. A266240, A269418 (numerator)."
			],
			"keyword": "nonn,frac",
			"offset": "0,2",
			"author": "_Gheorghe Coserea_, Feb 25 2016",
			"references": 9,
			"revision": 33,
			"time": "2018-10-23T10:33:00-04:00",
			"created": "2016-02-27T15:43:51-05:00"
		}
	]
}