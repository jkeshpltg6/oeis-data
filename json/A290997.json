{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A290997",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 290997,
			"data": "0,0,1,3,6,12,27,63,143,315,684,1479,3195,6903,14932,32361,70266,152775,332397,723330,1573829,3423444,7444722,16185939,35185779,76483890,166253545,361396431,785621808,1707884880,3712912632,8071922817,17548551692,38150905170",
			"name": "p-INVERT of (1,1,1,1,1,...), where p(S) = 1 - S^3 - S^6.",
			"comment": [
				"Suppose s = (c(0), c(1), c(2),...) is a sequence and p(S) is a polynomial. Let S(x) = c(0)*x + c(1)*x^2 + c(2)*x^3 + ... and T(x) = (-p(0) + 1/p(S(x)))/x. The p-INVERT of s is the sequence t(s) of coefficients in the Maclaurin series for T(x).  Taking p(S) = 1 - S gives the \"INVERT\" transform of s, so that p-INVERT is a generalization of the \"INVERT\" transform (e.g., A033453).",
				"See A291000 for a guide to related sequences."
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A290997/b290997.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (6, -15, 21, -18, 9, -1)"
			],
			"formula": [
				"a(n) = 6*a(n-1) - 15*a(n-2) + 21*a(n-3) - 18*a(n-4) + 9*a(n-5) - a(n-6) for n \u003e= 7.",
				"G.f.: x^2*(1 - 3*x + 3*x^2) / (1 - 6*x + 15*x^2 - 21*x^3 + 18*x^4 - 9*x^5 + x^6). - _Colin Barker_, Aug 22 2017"
			],
			"mathematica": [
				"z = 60; s = x/(1 - x); p = 1 - s^3 - s^6;",
				"Drop[CoefficientList[Series[s, {x, 0, z}], x], 1]  (* A000012 *)",
				"Drop[CoefficientList[Series[1/p, {x, 0, z}], x], 1]  (* A290997 *)"
			],
			"program": [
				"(PARI) concat(vector(2), Vec(x^2*(1 - 3*x + 3*x^2) / (1 - 6*x + 15*x^2 - 21*x^3 + 18*x^4 - 9*x^5 + x^6) + O(x^50))) \\\\ _Colin Barker_, Aug 22 2017"
			],
			"xref": [
				"Cf. A000012, A289780, A291000."
			],
			"keyword": "nonn,easy",
			"offset": "0,4",
			"author": "_Clark Kimberling_, Aug 22 2017",
			"references": 2,
			"revision": 6,
			"time": "2017-08-23T09:46:21-04:00",
			"created": "2017-08-23T09:46:21-04:00"
		}
	]
}