{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A328178",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 328178,
			"data": "0,3,2,0,4,1,6,6,0,7,10,4,12,5,6,0,16,5,18,1,4,9,22,2,0,15,10,3,28,3,30,12,8,19,2,0,36,17,14,13,40,1,42,15,12,21,46,8,0,15,18,9,52,15,14,10,16,31,58,9,60,29,14,0,8,13,66,21,20,11,70,1,72",
			"name": "a(n) is the minimal value of the expression d XOR (n/d) where d runs through the divisors of n and XOR denotes the bitwise XOR operator.",
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A328178/b328178.txt\"\u003eTable of n, a(n) for n = 1..16384\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A328178/a328178.png\"\u003eLogarithmic scatterplot of (n, 1+a(n)) for n = 1..2^16\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 0 iff n is a square.",
				"a(p) = p-1 for any odd prime number p."
			],
			"example": [
				"For n = 12:",
				"- we have the following values:",
				"    d   12/d  d XOR (12/d)",
				"    --  ----  ------------",
				"     1    12            13",
				"     2     6             4",
				"     3     4             7",
				"     4     3             7",
				"     6     2             4",
				"    12     1            13",
				"- hence a(12) = min({4, 7, 13}) = 4."
			],
			"maple": [
				"a:= n-\u003e min(seq(Bits[Xor](d, n/d), d=numtheory[divisors](n))):",
				"seq(a(n), n=1..100);  # _Alois P. Heinz_, Oct 09 2019"
			],
			"mathematica": [
				"mvx[n_]:=Min[BitXor[#,n/#]\u0026/@Divisors[n]]; Array[mvx,80] (* _Harvey P. Dale_, Nov 04 2019 *)"
			],
			"program": [
				"(PARI) a(n) = vecmin(apply(d -\u003e bitxor(d, n/d), divisors(n)))"
			],
			"xref": [
				"See A328176 and A328177 for similar sequences.",
				"Cf. A178910."
			],
			"keyword": "nonn,base",
			"offset": "1,2",
			"author": "_Rémy Sigrist_, Oct 06 2019",
			"references": 3,
			"revision": 16,
			"time": "2019-11-04T14:52:34-05:00",
			"created": "2019-10-09T04:11:51-04:00"
		}
	]
}