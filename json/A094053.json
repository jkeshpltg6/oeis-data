{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A094053",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 94053,
			"data": "0,1,0,2,2,0,3,4,3,0,4,6,6,4,0,5,8,9,8,5,0,6,10,12,12,10,6,0,7,12,15,16,15,12,7,0,8,14,18,20,20,18,14,8,0,9,16,21,24,25,24,21,16,9,0,10,18,24,28,30,30,28,24,18,10,0,11,20,27,32,35,36,35,32,27,20,11,0,12",
			"name": "Triangle read by rows: T(n,k) = k*(n-k), 1 \u003c= k \u003c= n.",
			"comment": [
				"T(n,k) = A003991(n-1,k) for 1 \u003c= k \u003c n;",
				"T(n,k) = T(n,n-1-k) for k \u003c n;",
				"T(n,1) = n-1; T(n,n) = 0;          T(n,2) = A005843(n-2) for n \u003e 1;",
				"T(n,3) = A008585(n-3) for n\u003e2;     T(n,4) = A008586(n-4) for n \u003e 3;",
				"T(n,5) = A008587(n-5) for n\u003e4;     T(n,6) = A008588(n-6) for n \u003e 5;",
				"T(n,7) = A008589(n-7) for n\u003e6;     T(n,8) = A008590(n-8) for n \u003e 7;",
				"T(n,9) = A008591(n-9) for n\u003e8;     T(n,10) = A008592(n-10) for n \u003e 9;",
				"T(n,11) = A008593(n-11) for n\u003e10;  T(n,12) = A008594(n-12) for n \u003e 11;",
				"T(n,13) = A008595(n-13) for n\u003e12;  T(n,14) = A008596(n-14) for n \u003e 13;",
				"T(n,15) = A008597(n-15) for n\u003e14;  T(n,16) = A008598(n-16) for n \u003e 15;",
				"T(n,17) = A008599(n-17) for n\u003e16;  T(n,18) = A008600(n-18) for n \u003e 17;",
				"T(n,19) = A008601(n-19) for n\u003e18;  T(n,20) = A008602(n-20) for n \u003e 19;",
				"Row sums give A000292; triangle sums give A000332;",
				"All numbers m \u003e 0 occur A000005(m) times;",
				"A002378(n) = T(A005408(n),n+1) = n*(n+1).",
				"k-th columns are arithmetic progressions with step k, starting with 0. If a zero is prefixed to the sequence, then we get a new table where the columns are again arithmetic progressions with step k, but starting with k, k=0,1,2,...: 1st column = (0,0,0,...), 2nd column = (1,2,3,...), 3rd column = (2,4,6,8,...), etc. - _M. F. Hasler_, Feb 02 2013",
				"Construct the infinite-dimensional matrix representation of angular momentum operators (J_1,J_2,J_3) in the Jordan-Schwinger form (cf. Harter, Klee, Schwinger). The triangle terms T(n,k) = T(2j,j+m) satisfy: (1/2)T(2j,j+m)^(1/2) = \u003cj,m|J_1|j,m+1\u003e = \u003cj,m+1|J_1|j,m\u003e = i \u003cj,m|J_2|j,m+1\u003e = -i \u003cj,m+1|J_2|j,m\u003e. Matrices for J_1 and J_2 are sparse. These equalities determine the only nonzero entries. - _Bradley Klee_, Jan 29 2016",
				"T(n+1,k+1) is the number of degrees of freedom of a k-dimensional affine subspace within an n-dimensional vector space. This is most readily interpreted geometrically: e.g. in 3 dimensions a line (1-dimensional subspace) has T(4,2) = 4 degrees of freedom and a plane has T(4,3) = 3. T(n+1,1) = n indicates that points in n dimensions have n degrees of freedom. T(n,n) = 0 for any n as all n-dimensional spaces in an n-dimensional space are equivalent. - _Daniel Leary_, Apr 29 2020"
			],
			"link": [
				"W. Harter, \u003ca href=\"http://www.uark.edu/ua/modphys/markup/PSDS_Info.html/\"\u003ePrinciples of Symmetry, Dynamics, Spectroscopy\u003c/a\u003e, Wiley, 1993, Ch. 5, page 345-346.",
				"B. Klee, \u003ca href=\"http://demonstrations.wolfram.com/QuantumAngularMomentumMatrices/\"\u003eQuantum Angular Momentum Matrices\u003c/a\u003e, Wolfram Demonstrations Project, 2016.",
				"J. Schwinger, \u003ca href=\"http://www.ifi.unicamp.br/~cabrera/teaching/paper_schwinger.pdf\"\u003e On Angular Momentum \u003c/a\u003e, Cambridge: Harvard University, Nuclear Development Associates, Inc., 1952."
			],
			"example": [
				"From _M. F. Hasler_, Feb 02 2013: (Start)",
				"Triangle begins:",
				"  0;",
				"  1, 0;",
				"  2, 2, 0;",
				"  3, 4, 3, 0;",
				"  4, 6, 6, 4, 0;",
				"  5, 8, 9, 8, 5, 0;",
				"  (...)",
				"If an additional 0 was added at the beginning, this would become:",
				"  0;",
				"  0, 1;",
				"  0, 2, 2;",
				"  0, 3, 4; 3;",
				"  0, 4, 6, 6, 4;",
				"  0, 5, 8, 9, 8, 5;",
				"  ... (End)"
			],
			"mathematica": [
				"Flatten[Table[(j - m) (j + m + 1), {j, 0, 10, 1/2}, {m, -j, j}]] (* _Bradley Klee_, Jan 29 2016 *)"
			],
			"program": [
				"(MAGMA) /* As triangle */ [[k*(n-k): k in [1..n]]: n in [1.. 15]]; // _Vincenzo Librandi_, Jan 30 2016",
				"(PARI) {for(n=1, 13, for(k=1, n, print1(k*(n - k),\" \");); print(););} \\\\ _Indranil Ghosh_, Mar 12 2017"
			],
			"xref": [
				"J_3: A114327; J_1^2, J_2^2: A141387, A268759.",
				"Cf. A000005, A002378, A003991, A005408.",
				"Cf. A000292 (row sums), A000332 (triangle sums).",
				"T(n,k) for values of k:",
				"  A005843 (k=2), A008585 (k=3), A008586 (k=4), A008587 (k=5), A008588 (k=6), A008589 (k=7), A008590 (k=8), A008591 (k=9), A008592 (k=10), A008593 (k=11), A008594 (k=12), A008595 (k=13), A008596 (k=14), A008597 (k=15), A008598 (k=16), A008599 (k=17), A008600 (k=18), A008601 (k=19), A008602 (k=20)."
			],
			"keyword": "nonn,tabl",
			"offset": "1,4",
			"author": "_Reinhard Zumkeller_, May 31 2004",
			"references": 7,
			"revision": 37,
			"time": "2020-06-04T05:58:38-04:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}