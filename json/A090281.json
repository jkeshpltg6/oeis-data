{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A090281",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 90281,
			"data": "1,2,3,4,4,3,2,1,1,2,3,4,4,3,2,1,1,2,3,4,4,3,2,1,1,2,3,4,4,3,2,1,1,2,3,4,4,3,2,1,1,2,3,4,4,3,2,1,1,2,3,4,4,3,2,1,1,2,3,4,4,3,2,1,1,2,3,4,4,3,2,1,1,2,3,4,4,3,2,1,1,2,3,4,4,3,2,1,1,2,3,4,4,3,2,1,1,2,3",
			"name": "\"Plain Bob Minimus\" in bell-ringing is a sequence of permutations p_1=(1,2,3,4), p_2=(2,1,4,3), ... which runs through all permutations of {1,2,3,4} with period 24; sequence gives position of bell 1 (the treble bell) in n-th permutation.",
			"comment": [
				"This is the \"plain hunting\" sequence with 4 bells.",
				"a(n) is also the position of bell 4 (the tenor bell) in the (n+4)-th permutation of the \"Fourth down, Extream between the two farthest Bells from it\" bell-ringing permutation, A143484. - _Alois P. Heinz_, Aug 19 2008",
				"Period 8 sequence: 1, 2, 3, 4, 4, 3, 2, 1, ... - _Wesley Ivan Hurt_, Mar 27 2014"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A090281/b090281.txt\"\u003eTable of n, a(n) for n = 1..8192\u003c/a\u003e",
				"R. Bailey, \u003ca href=\"http://www.ringing.info\"\u003eChange Ringing Resources\u003c/a\u003e",
				"David Joyner, \u003ca href=\"http://www.usna.edu/Users/math/wdj/book/node158.html\"\u003eApplication: Bell Ringing\u003c/a\u003e",
				"M.I.T. Bell-Ringers, \u003ca href=\"http://web.mit.edu/bellringers/www/html/change_instruction.html\"\u003eGeneral Information On Change Ringing\u003c/a\u003e",
				"Richard Duckworth and Fabian Stedman, \u003ca href=\"http://www.gutenberg.org/files/18567/18567-h/18567-h.htm\"\u003eThe Project Gutenberg EBook of Tintinnalogia, or, the Art of Ringing\u003c/a\u003e [From _Alois P. Heinz_, Aug 19 2008]",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,0,0,-1,1).",
				"\u003ca href=\"/index/Be#bell_ringing\"\u003eIndex entries for sequences related to bell ringing\u003c/a\u003e"
			],
			"formula": [
				"a(n) = (floor(-abs(n-(16*ceiling(n/8)-7)/2) + (16*ceiling(n/8)-7)/2)) mod 8. - _Wesley Ivan Hurt_, Mar 26 2014",
				"G.f.: -x*(x^4+x^3+x^2+x+1) / ((x-1)*(x^4+1)). - _Colin Barker_, Mar 26 2014"
			],
			"example": [
				"The full list of the 24 permutations is as follows (the present sequence tells where the 1's are):",
				"1 2 3 4",
				"2 1 4 3",
				"2 4 1 3",
				"4 2 3 1",
				"4 3 2 1",
				"3 4 1 2",
				"3 1 4 2",
				"1 3 2 4",
				"1 3 4 2",
				"3 1 2 4",
				"3 2 1 4",
				"2 3 4 1",
				"2 4 3 1",
				"4 2 1 3",
				"4 1 2 3",
				"1 4 3 2",
				"1 4 2 3",
				"4 1 3 2",
				"4 3 1 2",
				"3 4 2 1",
				"3 2 4 1",
				"2 3 1 4",
				"2 1 3 4",
				"1 2 4 3"
			],
			"maple": [
				"ring:= proc(k) option remember; local l, a, b, c, swap, h; l:= [1,2,3,4]; swap:= proc(i,j) h:=l[i]; l[i]:=l[j]; l[j]:=h end; a:= proc() swap(1,2); swap(3,4); l[k] end; b:= proc() swap(2,3); l[k] end; c:= proc() swap(3,4); l[k] end; [l[k], seq([seq([a(), b()][], j=1..3), a(), c()][], i=1..3)] end: bells:=[seq(ring(k), k=1..4)]: indx:= proc(l, n, k) local i; for i from 1 to 4 do if l[i][n]=k then break fi od; i end: a:= n-\u003e indx(bells, modp(n-1,24)+1, 1): seq(a(n), n=1..99); # _Alois P. Heinz_, Aug 19 2008"
			],
			"mathematica": [
				"Table[Mod[Floor[-Abs[n-(16*Ceiling[n/8]-7)/2] + (16*Ceiling[n/8]-7)/2],8], {n, 100}] (* _Wesley Ivan Hurt_, Mar 26 2014 *)",
				"LinearRecurrence[{1, 0, 0, -1, 1}, {1, 2, 3, 4, 4}, 105] (* _Jean-François Alcover_, Mar 15 2021 *)"
			],
			"program": [
				"(Scheme) (define (A090281 n) (list-ref '(1 2 3 4 4 3 2 1) (modulo (- n 1) 8))) ;; _Antti Karttunen_, Aug 10 2017"
			],
			"xref": [
				"Cf. A090277, A090278, A090279, A090280, A090282, A090283, A090284."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, Jan 24 2004",
			"references": 9,
			"revision": 41,
			"time": "2021-03-15T08:25:28-04:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}