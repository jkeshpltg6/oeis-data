{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A306945",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 306945,
			"data": "2,1,1,2,2,3,4,1,6,8,2,9,16,7,18,30,14,2,30,60,34,4,56,114,72,14,99,220,156,36,1,186,422,320,90,6,335,817,671,207,18,630,1564,1364,484,54,1161,3023,2787,1070,148,3,2182,5818,5624,2362,386,12,4080,11240,11357,5095,947,49",
			"name": "Triangular array T(n,k) read by rows: T(n,k) is the number of degree n monic polynomials in GF(2)[x] with exactly k squarefree factors in its unique factorization into irreducible polynomials.",
			"comment": [
				"T(n,k) is also the number of binary words of length n whose Lyndon factorization is strict, i.e., it contains exactly k factors of distinct Lyndon words."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A306945/b306945.txt\"\u003eRows n = 1..500, flattened\u003c/a\u003e"
			],
			"formula": [
				"G.f.: Product_{k\u003e=1} (1 + y*x)^A001037(k)."
			],
			"example": [
				"Triangular array T(n,k) begins:",
				"   2;",
				"   1,   1;",
				"   2,   2;",
				"   3,   4,   1;",
				"   6,   8,   2;",
				"   9,  16,   7;",
				"  18,  30,  14,  2;",
				"  30,  60,  34,  4;",
				"  56, 114,  72, 14;",
				"  99, 220, 156, 36, 1;",
				"  ..."
			],
			"maple": [
				"with(numtheory):",
				"g:= proc(n) option remember; `if`(n=0, 1,",
				"      add(mobius(n/d)*2^d, d=divisors(n))/n)",
				"    end:",
				"b:= proc(n, i) option remember; expand(`if`(n=0, x^n, `if`(i\u003c1, 0,",
				"      add(binomial(g(i), j)*b(n-i*j, i-1)*x^j, j=0..n/i))))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=1..degree(p)))(b(n$2)):",
				"seq(T(n), n=1..20);  # _Alois P. Heinz_, May 28 2019"
			],
			"mathematica": [
				"nn = 16; a = Table[1/n Sum[2^d MoebiusMu[n/d], {d, Divisors[n]}], {n, 1, nn}]; Map[Select[#, # \u003e 0 \u0026] \u0026, Drop[CoefficientList[",
				"    Series[Product[ (1 + u z^k)^a[[k]], {k, 1, nn}], {z, 0, nn}], {z, u}], 1]] // Grid"
			],
			"xref": [
				"Column k=1 gives A001037.",
				"Row sums give A090129(n+1).",
				"Cf. A269456."
			],
			"keyword": "nonn,look,tabf",
			"offset": "1,1",
			"author": "_Geoffrey Critzer_, Mar 25 2019",
			"references": 3,
			"revision": 52,
			"time": "2019-12-01T17:07:20-05:00",
			"created": "2019-06-03T10:03:34-04:00"
		}
	]
}