{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A049374",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 49374,
			"data": "1,6,1,42,18,1,336,276,36,1,3024,4200,960,60,1,30240,66024,23400,2460,90,1,332640,1086624,557424,87360,5250,126,1,3991680,18805248,13349952,2916144,255360,9912,168,1,51891840,342486144,325854144,95001984",
			"name": "A triangle of numbers related to triangle A030527.",
			"comment": [
				"a(n,1) = A001725(n+4). a(n,m)=: S1p(6; n,m), a member of a sequence of lower triangular Jabotinsky matrices with nonnegative entries, including S1p(1; n,m) = A008275 (unsigned Stirling first kind), S1p(2; n,m) = A008297(n,m) (unsigned Lah numbers). S1p(3; n,m) = A046089(n,m), S1p(4; n,m) = A049352, S1p(5; n,m) = A049353(n,m).",
				"Signed lower triangular matrix (-1)^(n-m)*a(n,m) is inverse to matrix A049385(n,m) =: S2(6; n,m). The monic row polynomials E(n,x) := Sum_{m=1..n} (a(n,m)*x^m), E(0,x) := 1 are exponential convolution polynomials (see A039692 for the definition and a Knuth reference).",
				"a(n,m) enumerates unordered increasing n-vertex m-forests composed of m unary trees (out-degree r from {0,1}) whose vertices of depth (distance from the root) j \u003e= 1 come in j+5 colors. The k roots (j=0) each come in one (or no) color. - _Wolfdieter Lang_, Oct 12 2007"
			],
			"link": [
				"Muniru A Asiru, \u003ca href=\"/A049374/b049374.txt\"\u003eTable of n, a(n) for n = 1..1275\u003c/a\u003e",
				"W. Lang, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL3/LANG/lang.html\"\u003eOn generalizations of Stirling number triangles\u003c/a\u003e, J. Integer Seqs., Vol. 3 (2000), #00.2.4.",
				"W. Lang, \u003ca href=\"/A049374/a049374.txt\"\u003eFirst ten rows. \u003c/a\u003e"
			],
			"formula": [
				"a(n, m) = n!*A030527(n, m)/(m!*5^(n-m)); a(n, m) = (5*m+n-1)*a(n-1, m) + a(n-1, m-1), n \u003e= m \u003e= 1; a(n, m)=0, n \u003c m; a(n, 0) := 0; a(1, 1)=1. E.g.f. for m-th column: ((x*(5 - 10*x + 10*x^2 - 5*x^3 + x^4)/(5*(1-x)^5))^m)/m!.",
				"a(n,k) = n!* Sum_{j=1..k} (-1)^(k-j)*binomial(k,j)*binomial(n+5*j-1,5*j-1) /(5^k*k!). - _Vladimir Kruchinin_, Apr 01 2011"
			],
			"example": [
				"Triangle begins",
				"       1;",
				"       6,       1;",
				"      42,      18,      1;",
				"     336,     276,     36,     1;",
				"    3024,    4200,    960,    60,    1;",
				"   30240,   66024,  23400,  2460,   90,   1;",
				"  332640, 1086624, 557424, 87360, 5250, 126, 1;",
				"E.g., row polynomial E(3,x) = 42*x + 18*x^2 + x^3.",
				"a(4,2) = 276 = 4*(6*7) + 3*(6*6) from the two types of unordered 2-forests of unary increasing trees associated with the two m=2 parts partitions (1,3) and (2^2) of n=4. The first type has 4 increasing labelings, each coming in (1)*(1*6*7)=42 colored versions, e.g., ((1c1),(2c1,3c6,4c3)) with lcp for vertex label l and color p. Here the vertex labeled 3 has depth j=1, hence 6 colors, c1..c6, can be chosen and the vertex labeled 4 with j=2 can come in 7 colors, e.g., c1..c7. Therefore there are 4*((1)*(1*6*7))=168 forests of this (1,3) type. Similarly the (2,2) type yields 3*((1*6)*(1*6))=108 such forests, e.g., ((1c1,3c4)(2c1,4c6)) or ((1c1,3c5)(2c1,4c2)), etc. - _Wolfdieter Lang_, Oct 12 2007"
			],
			"maple": [
				"# The function BellMatrix is defined in A264428.",
				"# Adds (1,0,0,0, ..) as column 0.",
				"BellMatrix(n -\u003e (n+5)!/120, 10); # _Peter Luschny_, Jan 28 2016"
			],
			"mathematica": [
				"a[n_, k_] = n!*Sum[(-1)^(k-j)*Binomial[k, j]*Binomial[n + 5j - 1, 5j - 1]/(5^k*k!), {j, 1, k}] ;",
				"Flatten[Table[a[n, k], {n, 1, 9}, {k, 1, n}] ][[1 ;; 40]]",
				"(* _Jean-François Alcover_, Jun 01 2011, after _Vladimir Kruchinin_ *)",
				"BellMatrix[f_Function, len_] := With[{t = Array[f, len, 0]}, Table[BellY[n, k, t], {n, 0, len-1}, {k, 0, len-1}]];",
				"rows = 10;",
				"M = BellMatrix[(#+5)!/120\u0026, rows];",
				"Table[M[[n, k]], {n, 2, rows}, {k, 2, n}] // Flatten (* _Jean-François Alcover_, Jun 23 2018, after _Peter Luschny_ *)"
			],
			"program": [
				"(Maxima)",
				"a(n,k)=(n!*sum((-1)^(k-j)*binomial(k,j)*binomial(n+5*j-1,5*j-1),j,1,k))/(5^k*k!); /* _Vladimir Kruchinin_, Apr 01 2011 */",
				"(PARI)",
				"a(n,k)=(n!*sum(j=1,k,(-1)^(k-j)*binomial(k,j)*binomial(n+5*j-1,5*j-1)))/(5^k*k!);",
				"for(n=1,12,for(k=1,n,print1(a(n,k),\", \"));print()); /* print triangle */ /* _Joerg Arndt_, Apr 01 2011 */",
				"(GAP) Flat(List([1..10],n-\u003eFactorial(n)*List([1..n],k-\u003eSum([1..k],j-\u003e(-1)^(k-j)*Binomial(k,j)*Binomial(n+5*j-1,5*j-1)/(5^k*Factorial(k)))))); # _Muniru A Asiru_, Jun 23 2018"
			],
			"xref": [
				"Cf. A049402 (row sums), A134140 (alternating row sums)."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "1,2",
			"author": "_Wolfdieter Lang_",
			"references": 9,
			"revision": 39,
			"time": "2019-08-28T16:48:44-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}