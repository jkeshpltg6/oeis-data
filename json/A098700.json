{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A098700",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 98700,
			"data": "2,3,11,17,23,29,35,37,47,53,57,65,67,79,83,89,93,97,107,117,125,127,137,145,149,157,163,173,177,179,189,197,205,207,209,217,219,223,233,237,245,257,261,277,289,303,305,307,317,323,325,337,345,353,367,373",
			"name": "Numbers n such that x' = n has no integer solution, where x' is the arithmetic derivative of x.",
			"comment": [
				"If x' = n has solutions, they occur for x \u003c= (n/2)^2. - _T. D. Noe_, Oct 12 2004",
				"The prime and composite terms are in A189483 and A189554, respectively.",
				"A099302(a(n)) = 0. - _Reinhard Zumkeller_, Mar 18 2014"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A098700/b098700.txt\"\u003eTable of n, a(n) for n = 1..5000\u003c/a\u003e",
				"Victor Ufnarovski and Bo Ahlander, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL6/Ufnarovski/ufnarovski.html\"\u003eHow to Differentiate a Number\u003c/a\u003e, J. Integer Seqs., Vol. 6, 2003. (See p. 7.)"
			],
			"mathematica": [
				"a[1] = 0; a[n_] := Block[{f = Transpose[ FactorInteger[ n]]}, If[ PrimeQ[n], 1, Plus @@ (n*f[[2]]/f[[1]])]]; b = Table[ -1, {500}]; b[[1]] = 1; Do[c = a[n]; If[c \u003c 500 \u0026\u0026 b[[c + 1]] == 0, b[[c + 1]] = n], {n, 10^6}]; Select[ Range[500], b[[ # ]] == 0 \u0026]",
				"dn[0]=0; dn[1]=0; dn[n_]:=Module[{f=Transpose[FactorInteger[n]]}, If[PrimeQ[n], 1, Plus@@(n*f[[2]]/f[[1]])]]; d1=Table[dn[n], {n, 40000}]; Select[Range[400], 0==Count[d1, # ]\u0026]"
			],
			"program": [
				"(Haskell)",
				"a098700 n = a098700_list !! (n-1)",
				"a098700_list = filter",
				"   (\\z -\u003e all (/= z) $ map a003415 [1 .. a002620 z]) [2..]",
				"-- _Reinhard Zumkeller_, Mar 18 2014",
				"(PARI) list(lim)=my(v=List()); lim\\=1; forfactored(n=1, lim^2, my(f=n[2],t); listput(v, n[1]*sum(i=1, #f~, f[i, 2]/f[i, 1]))); setminus([1..lim], Set(v)); \\\\ _Charles R Greathouse IV_, Oct 21 2021"
			],
			"xref": [
				"Cf. A003415 (arithmetic derivative of n), A099302 (number of solutions to x' = n), A099303 (greatest x such that x' = n), A098699 (least x such that x' = n).",
				"Cf. A239433 (complement), A002620."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Robert G. Wilson v_, Sep 21 2004",
			"ext": [
				"Corrected and extended by _T. D. Noe_, Oct 12 2004"
			],
			"references": 12,
			"revision": 35,
			"time": "2021-10-25T04:19:47-04:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}