{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A103997",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 103997,
			"data": "1,1,1,1,3,1,1,11,7,1,1,41,71,18,1,1,153,769,539,47,1,1,571,8449,17753,4271,123,1,1,2131,93127,603126,434657,34276,322,1,1,7953,1027207,20721019,46069729,10894561,276119,843,1,1,29681,11332097,714790675,4974089647,3625549353,275770321,2226851,2207,1",
			"name": "Square array T(M,N) read by antidiagonals: number of dimer tilings of a 2*M X 2*N Moebius strip.",
			"link": [
				"Laura Florescu, Daniela Morar, David Perkinson, Nicholas Salter and Tianyuan Xu, \u003ca href=\"https://doi.org/10.37236/4472\"\u003eSandpiles and Dominos\u003c/a\u003e, El. J. Comb., 22 (2015), P1.66. See Theorem 18.",
				"W. T. Lu and F. Y. Fu, \u003ca href=\"https://arxiv.org/abs/cond-mat/9906154\"\u003eDimer statistics on the Moebius strip and the Klein bottle\u003c/a\u003e, arXiv:cond-mat/9906154 [cond-mat.stat-mech], 1999.",
				"\u003ca href=\"/index/Do#domino\"\u003eIndex entries for sequences related to dominoes\u003c/a\u003e"
			],
			"formula": [
				"T(M, N) = Product_{m=1..M} (Product_{n=1..N} 4*sin(Pi*(4*n-1)/(4*N))^2 + 4*cos(Pi*m/(2*M + 1))^2).",
				"For k \u003e 0, T(n,k) = 2^n * sqrt(Resultant(U_{2*n}(x/2), T_{2*k}(i*x/2))), where T_n(x) is a Chebyshev polynomial of the first kind, U_n(x) is a Chebyshev polynomial of the second kind and i = sqrt(-1). - _Seiichi Manyama_, Apr 15 2020"
			],
			"example": [
				"Array begins:",
				"  1,   1,     1,        1,          1,             1,               1,",
				"  1,   3,     7,       18,         47,           123,             322,",
				"  1,  11,    71,      539,       4271,         34276,          276119,",
				"  1,  41,   769,    17753,     434657,      10894561,       275770321,",
				"  1, 153,  8449,   603126,   46069729,    3625549353,    289625349454,",
				"  1, 571, 93127, 20721019, 4974089647, 1234496016491, 312007855309063,",
				"  ..."
			],
			"mathematica": [
				"T[M_, N_] := Product[4Sin[(4n-1)Pi/(4N)]^2 + 4Cos[m Pi/(2M+1)]^2, {n, 1, N}, {m, 1, M}];",
				"Table[T[M - N, N] // Round, {M, 0, 9}, {N, 0, M}] // Flatten (* _Jean-François Alcover_, Dec 03 2018 *)"
			],
			"xref": [
				"Rows include A005248, A103998.",
				"Columns 1..7 give A001835(n+1), A334135, A334179, A334180, A334181, A334182, A334183.",
				"Main diagonal gives A334124.",
				"Cf. A099390, A103999, A341741, A334178, A256045, A348566."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Ralf Stephan_, Feb 26 2005",
			"references": 15,
			"revision": 45,
			"time": "2021-12-27T10:59:50-05:00",
			"created": "2005-04-09T03:00:00-04:00"
		}
	]
}