{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A341841",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 341841,
			"data": "0,1,0,2,0,0,3,3,0,0,4,3,0,1,0,5,4,0,1,1,0,6,4,7,0,1,0,0,7,7,7,7,0,0,0,0,8,7,7,6,0,0,3,1,0,9,8,7,6,1,0,3,2,1,0,10,8,8,7,1,0,3,3,2,0,0,11,11,8,8,0,0,3,3,3,3,0,0,12,11,8,9,15,0,0,2,3,3,0,1,0",
			"name": "Square array T(n, k), n, k \u003e= 0, read by antidiagonals upwards; for any number m with runs in binary expansion (r_1, ..., r_j), let R(m) = {r_1 + ... + r_j, r_2 + ... + r_j, ..., r_j}; T(n, k) is the unique number t such that R(t) equals R(n) minus R(k).",
			"comment": [
				"For any m \u003e 0, R(m) contains the partial sums of the m-th row of A227736; by convention, R(0) = {}.",
				"This sequence uses set subtraction, and is related to:",
				"- A003987 which uses set difference,",
				"- A341839 which uses set union,",
				"- A341840 which uses set intersection."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A341841/b341841.txt\"\u003eTable of n, a(n) for n = 0..10010\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A341841/a341841.png\"\u003eColored representation of the table for n, k \u003c 2^10\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A341841/a341841.gp.txt\"\u003ePARI program for A341841\u003c/a\u003e",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e"
			],
			"formula": [
				"T(n, n) = 0.",
				"T(n, 0) = n.",
				"T(T(n, k), k) = T(n, k).",
				"A070939(T(n, k)) \u003c= A070939(n).",
				"A003188(T(n, k)) = A003188(n) - (A003188(n) AND A003188(k)) (where AND denotes the bitwise AND operator)."
			],
			"example": [
				"Array T(n, k) begins:",
				"  n\\k|   0   1   2   3   4   5   6   7  8  9  10  11  12  13  14  15",
				"  ---+--------------------------------------------------------------",
				"    0|   0   0   0   0   0   0   0   0  0  0   0   0   0   0   0   0",
				"    1|   1   0   0   1   1   0   0   1  1  0   0   1   1   0   0   1",
				"    2|   2   3   0   1   1   0   3   2  2  3   0   1   1   0   3   2",
				"    3|   3   3   0   0   0   0   3   3  3  3   0   0   0   0   3   3",
				"    4|   4   4   7   7   0   0   3   3  3  3   0   0   7   7   4   4",
				"    5|   5   4   7   6   1   0   3   2  2  3   0   1   6   7   4   5",
				"    6|   6   7   7   6   1   0   0   1  1  0   0   1   6   7   7   6",
				"    7|   7   7   7   7   0   0   0   0  0  0   0   0   7   7   7   7",
				"    8|   8   8   8   8  15  15  15  15  0  0   0   0   7   7   7   7",
				"    9|   9   8   8   9  14  15  15  14  1  0   0   1   6   7   7   6",
				"   10|  10  11   8   9  14  15  12  13  2  3   0   1   6   7   4   5",
				"   11|  11  11   8   8  15  15  12  12  3  3   0   0   7   7   4   4",
				"   12|  12  12  15  15  15  15  12  12  3  3   0   0   0   0   3   3",
				"   13|  13  12  15  14  14  15  12  13  2  3   0   1   1   0   3   2",
				"   14|  14  15  15  14  14  15  15  14  1  0   0   1   1   0   0   1",
				"   15|  15  15  15  15  15  15  15  15  0  0   0   0   0   0   0   0"
			],
			"program": [
				"(PARI) See Links section."
			],
			"xref": [
				"Cf. A003188, A003987, A070939, A227736, A341839, A341840."
			],
			"keyword": "nonn,tabl,base",
			"offset": "0,4",
			"author": "_Rémy Sigrist_, Feb 21 2021",
			"references": 3,
			"revision": 12,
			"time": "2021-02-24T08:20:45-05:00",
			"created": "2021-02-23T12:38:21-05:00"
		}
	]
}