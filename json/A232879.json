{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A232879",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 232879,
			"data": "1,-1,-5,-13,-37,-83,-194,-469,-1111,-2743,-6698,-16379,-40543,-101251,-254053,-640483,-1622840,-4133371,-10578367,-27130829,-69814219",
			"name": "The y-axis intercept of the line y = n*x + b tangent to the curve y = prime(k), k = 1, 2, 3, ....",
			"comment": [
				"This sequence contains the y intercepts for lines with integer slopes, such that all primes fall at or above the line.  Verified for primes less than 2*10^9.",
				"The first 15 tangent lines intercept prime(k) at the following primes: 2, 3, 5, 7, 13, 19, 23, 31, 43, 47, 113, 283, 1129, 2803, 7043, 24137, 59753, 59797, 155893, 445033, 1195247, 3278837."
			],
			"link": [
				"John R Phelan, \u003ca href=\"/A232879/a232879.pdf\"\u003eFirst 5 integer tangents to prime(n)\u003c/a\u003e"
			],
			"formula": [
				"n*k + a(n) \u003c= prime(k), where n is the slope, and a(n) is the y intercept."
			],
			"example": [
				"The 2nd tangent line, a(2)+2*k tangent line intercepts p(k) at 3,5,7.",
				"a(n)+n*k = ...",
				"a(2)+2*2 = -1+2*2 = 3 = p(2).",
				"a(2)+2*3 = -1+2*3 = 5 = p(3).",
				"a(2)+2*4 = -1+2*4 = 7 = p(4).",
				"But other primes fall above the 2nd tangent line.",
				"a(2)+2*1 = -1+2*1 = 1 \u003c 2=p(1).",
				"a(2)+2*5 = -1+2*5 = 9 \u003c 11=p(5).",
				"a(2)+2*6 = -1+2*6 = 11 \u003c 13=p(6).",
				"For the 11th tangent line...",
				"a(11)+11*6041 = -6698+6041*11 = 59753 = p(6041).",
				"a(11)+11*6045 = -6698+6045*11 = 59797 = p(6045).",
				"But other primes fall above the 11th tangent line...",
				"a(11)+11*6040 = -6698+6040*11 = 59742 \u003c 59747 = p(6040)",
				"a(11)+11*6042 = -6698+6042*11 = 59764 \u003c 59771 = p(6042)",
				"a(11)+11*6043 = -6698+6043*11 = 59765 \u003c 59779 = p(6043)",
				"a(11)+11*6044 = -6698+6044*11 = 59776 \u003c 59791 = p(6044)",
				"a(11)+11*6046 = -6698+6046*11 = 59798 \u003c 59809 = p(6046)"
			],
			"mathematica": [
				"nn = 10^6; pt = Table[Prime[k], {k, nn}]; Table[r = n*Range[nn] - pt;",
				"mx = Max[r]; Print[{-mx, Flatten[Prime[Position[r, mx]]]}]; -mx, {n, 16}] (* _T. D. Noe_, Dec 04 2013 *)"
			],
			"program": [
				"(Java) public class Itp {private static long LIMIT = 10000000; private static long[] a = new long[100]; private static long[] p = new long[100]; public static void main(String[] args) {for (int n = 1; n \u003c a.length; n++) {a[n] = Integer.MAX_VALUE;} long k = 1; for (int i = 2; i \u003c LIMIT; i++) {if (isPrime(i)) {for (int n = 1; n \u003c a.length; n++) {long l = i - n * k; if (l \u003c a[n]) {a[n] = l; p[n] = i;}} k++;}} for (int n = 1; p[n] \u003c LIMIT / 2; n++) {System.out.print(a[n] + \",\");} System.out.println(\"\");} private static boolean isPrime(int i) {if (i \u003c 2) {return false;} int m = (int) Math.sqrt(i); for (int j = 2; j \u003c= m; j++) {if (i % j == 0) {return false;}} return true;}}"
			],
			"keyword": "sign,hard",
			"offset": "1,3",
			"author": "_John R Phelan_, Dec 01 2013",
			"ext": [
				"a(16)-a(21) from _T. D. Noe_, Dec 04 2013"
			],
			"references": 1,
			"revision": 12,
			"time": "2013-12-04T16:20:03-05:00",
			"created": "2013-12-04T16:20:03-05:00"
		}
	]
}