{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A215947",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 215947,
			"data": "1,5,4,13,6,20,8,29,13,30,12,52,14,40,24,61,18,65,20,78,32,60,24,116,31,70,40,104,30,120,32,125,48,90,48,169,38,100,56,174,42,160,44,156,78,120,48,244,57,155,72,182,54,200,72,232,80,150,60,312,62,160",
			"name": "Difference between the sum of the even divisors and the sum of the odd divisors of 2n.",
			"comment": [
				"Multiplicative because a(n) = -A002129(2*n), A002129 is multiplicative and a(1) = -A002129(2) = 1. - _Andrew Howroyd_, Jul 31 2018"
			],
			"link": [
				"Michel Lagneau, \u003ca href=\"/A215947/b215947.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Hartosh Singh Bal and Gaurav Bhatnagar, \u003ca href=\"https://arxiv.org/abs/2102.10804\"\u003eTwo curious congruences for the sum of divisors function\u003c/a\u003e, arXiv:2102.10804 [math.NT], 2021. Mentions this sequence.",
				"H. Movasati and Y. Nikdelan, \u003ca href=\"http://arxiv.org/abs/1603.09411\"\u003eGauss-Manin Connection in Disguise: Dwork Family\u003c/a\u003e, arXiv preprint arXiv:1603.09411 [math.AG], 2016-2017."
			],
			"formula": [
				"From _Andrew Howroyd_, Jul 28 2018: (Start)",
				"a(n) = 4*sigma(n) - sigma(2*n).",
				"a(n) = -A002129(2*n). (End)",
				"G.f.: Sum_{k\u003e=1} x^k*(1 + 4*x^k + x^(2*k))/(1 - x^(2*k))^2. - _Ilya Gutkovskiy_, Sep 14 2019",
				"a(p) = p + 1 for p prime \u003e= 3. - _Bernard Schott_, Sep 14 2019",
				"a(n) = A239050(n) - A062731(n) - _Omar E. Pol_, Mar 06 2021 (after _Andrew Howroyd_)"
			],
			"example": [
				"a(6) = 20 because the divisors of 2*6 = 12 are {1, 2, 3, 4, 6, 12} and (12 + 6 + 4 +2) - (3 + 1) = 20."
			],
			"maple": [
				"with(numtheory):for n from 1 to 100 do:x:=divisors(2*n):n1:=nops(x):s0:=0:s1:=0:for m from 1 to n1 do: if irem(x[m],2)=0 then s0:=s0+x[m]:else s1:=s1+x[m]:fi:od:if s0\u003es1  then printf(`%d, `,s0-s1):else fi:od:"
			],
			"mathematica": [
				"a[n_] := DivisorSum[2n, (1 - 2 Mod[#, 2]) #\u0026];",
				"Array[a, 62] (* _Jean-François Alcover_, Sep 13 2018 *)",
				"edod[n_]:=Module[{d=Divisors[2n]},Total[Select[d,EvenQ]]-Total[ Select[ d,OddQ]]]; Array[edod,70] (* _Harvey P. Dale_, Jul 30 2021 *)"
			],
			"program": [
				"(PARI) a(n) = 4*sigma(n) - sigma(2*n); \\\\ _Andrew Howroyd_, Jul 28 2018"
			],
			"xref": [
				"Cf. A000593, A002129, A022998 (Moebius transform), A074400, A195382, A195690.",
				"Cf. A062731, A239050."
			],
			"keyword": "nonn,look,mult",
			"offset": "1,2",
			"author": "_Michel Lagneau_, Aug 28 2012",
			"references": 4,
			"revision": 42,
			"time": "2021-07-30T12:34:43-04:00",
			"created": "2012-08-28T18:52:45-04:00"
		}
	]
}