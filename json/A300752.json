{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A300752",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 300752,
			"data": "1,2,2,1,1,1,1,1,2,3,2,2,2,4,2,1,5,5,2,1,2,2,2,1,4,5,2,1,4,7,1,2,5,3,2,1,3,5,4,2,8,5,1,3,5,6,1,2,4,8,5,4,2,4,4,2,6,5,5,2,1,6,4,1,8,9,6,2,3,4,1,2,6,8,5,4,5,8,2,1",
			"name": "Number of ways to write n as x^2 + y^2 + z^2 + w^2 with x + 3*y + 5*z a positive square, where x,y,z,w are nonnegative integers such that 3*x or y or z is a square.",
			"comment": [
				"Conjecture: a(n) \u003e 0 for all n = 1,2,3,....",
				"This is stronger than the author's 1-3-5 conjecture in A271518. See also A300751 for a similar conjecture stronger than the 1-3-5 conjecture.",
				"In 2020, A. Machiavelo, R. Reis and N. Tsopanidis verified a(n) \u003e 0 for n up to 1.05*10^11. - _Zhi-Wei Sun_, Oct 06 2020"
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A300752/b300752.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"António Machiavelo, Rogério Reis, and Nikolaos Tsopanidis, \u003ca href=\"https://arxiv.org/abs/2005.13526\"\u003eReport on Zhi-Wei Sun's \"1-3-5 conjecture\" and some of its refinements\u003c/a\u003e, arXiv:2005.13526 [math.NT], 2020.",
				"Zhi-Wei Sun, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2016.11.008\"\u003eRefining Lagrange's four-square theorem\u003c/a\u003e, J. Number Theory 175(2017), 167-190.",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1701.05868\"\u003eRestricted sums of four squares\u003c/a\u003e, arXiv:1701.05868 [math.NT], 2017-2018."
			],
			"example": [
				"a(71) = 1 since 71 = 3^2 + 1^2 + 6^2 + 5^2 with 1 = 1^2 and 3 + 3*1 + 5*6 = 6^2.",
				"a(248) = 1 since 248 = 10^2 + 2^2 + 0^2 + 12^2 with 0 = 0^2 and 10 + 3*2 + 5*0 = 4^2.",
				"a(263) = 1 since 263 = 13^2 + 2^2 + 9^2 + 3^2 with 9 = 3^2 and 13 + 3*2 + 5*9 = 8^2.",
				"a(808) = 1 since 808 = 12^2 + 14^2 + 18^2 + 12^2 with 3*12 = 6^2 and 12 + 3*14 + 5*18 = 12^2.",
				"a(1288) = 1 since 1288 = 12^2 + 18^2 + 26^2 + 12^2 with 3*12 = 6^2 and 12 + 3*18 + 5*26 = 14^2.",
				"a(3544) = 1 since 3544 = 14^2 + 34^2 + 16^2 + 44^2 with 16 = 4^2 and 14 + 3*34 + 5*16 = 14^2."
			],
			"mathematica": [
				"SQ[n_]:=SQ[n]=IntegerQ[Sqrt[n]];",
				"tab={};Do[r=0;Do[If[(SQ[3(m^2-3y-5z)]||SQ[y]||SQ[z])\u0026\u0026SQ[n-(m^2-3y-5z)^2-y^2-z^2],r=r+1],{m,1,(35n)^(1/4)},{y,0,Min[m^2/3,Sqrt[n]]},{z,0,Min[(m^2-3y)/5,Sqrt[n-y^2]]}];tab=Append[tab,r],{n,1,80}];Print[tab]"
			],
			"xref": [
				"Cf. A000118, A000290, A271518, A281976, A282542, A300666, A300667, A300708, A300712, A300751."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Zhi-Wei Sun_, Mar 11 2018",
			"references": 11,
			"revision": 15,
			"time": "2020-10-07T05:44:07-04:00",
			"created": "2018-03-12T03:42:03-04:00"
		}
	]
}