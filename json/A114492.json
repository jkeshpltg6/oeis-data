{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A114492",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 114492,
			"data": "1,1,2,5,13,1,35,7,97,34,1,275,143,11,794,558,77,1,2327,2083,436,16,6905,7559,2180,151,1,20705,26913,10051,1095,22,62642,94547,43796,6758,268,1,190987,328943,183130,37402,2409,29,586219,1136218,742253,191408",
			"name": "Triangle read by rows: T(n,k) is the number of Dyck paths of semilength n and having k DDUU's, where U=(1,1), D=(1,-1) (0\u003c=k\u003c=floor(n/2)-1 for n\u003e=2).",
			"comment": [
				"Rows 0 and 1 contain one term each; row n contains floor(n/2) terms (n\u003e=2).",
				"Row sums are the Catalan numbers (A000108). Column 0 yields A086581.",
				"Sum(k*T(n,k),k=0..floor(n/2)-1) = binomial(2n-3,n-4) (A003516)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A114492/b114492.txt\"\u003eRows n = 0..200, flattened\u003c/a\u003e",
				"FindStat - Combinatorial Statistic Finder, \u003ca href=\"http://www.findstat.org/StatisticsDatabase/St000122\"\u003eThe number of occurrences of the contiguous pattern [.,[.,[[.,.],.]]].\u003c/a\u003e",
				"A. Sapounakis, I. Tasoulas and P. Tsikouras, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2007.03.005\"\u003eCounting strings in Dyck paths\u003c/a\u003e, Discrete Math., 307 (2007), 2909-2924."
			],
			"formula": [
				"G.f.: G=G(t, z) satisfies z(t+z-tz)G^2-(1-2(1-t)z+(1-t)z^2)G+1-z+tz=0."
			],
			"example": [
				"T(5,1) = 7 because we have UU(DDUU)DUDD, UU(DDUU)UDDD, UDUU(DDUU)DD, their mirror images and UUU(DDUU)DDD (the DDUU's are shown between parentheses).",
				"Triangle starts:",
				"1;",
				"1;",
				"2;",
				"5;",
				"13,  1;",
				"35,  7;",
				"97, 34, 1;"
			],
			"maple": [
				"G:=1/2/(-t*z-z^2+z^2*t)*(-1+2*z-2*t*z-z^2+z^2*t+sqrt(1+z^4-2*z^4*t+z^4*t^2-4*z+2*z^2-2*z^2*t)): Gser:=simplify(series(G,z=0,17)): P[0]:=1: for n from 1 to 14 do P[n]:=coeff(Gser,z^n) od: 1; 1; for n from 0 to 14 do seq(coeff(t*P[n],t^j),j=1..floor(n/2)) od; # yields sequence in triangular form"
			],
			"mathematica": [
				"m = 15; G[_, _] = 0;",
				"Do[G[t_, z_] = (-1 + z - t z - t z G[t, z]^2 - z^2 G[t, z]^2 + t z^2 G[t, z]^2)/(-1 + 2z - 2t z - z^2 + t z^2) + O[t]^Floor[m/2] + O[z]^m, {m}];",
				"CoefficientList[#, t]\u0026 /@ Take[CoefficientList[G[t, z], z], m] // Flatten (* _Jean-François Alcover_, Oct 05 2019 *)"
			],
			"xref": [
				"Cf. A000108, A086581, A003516."
			],
			"keyword": "nonn,tabf",
			"offset": "0,3",
			"author": "_Emeric Deutsch_, Dec 01 2005",
			"references": 5,
			"revision": 20,
			"time": "2019-10-05T09:14:27-04:00",
			"created": "2006-01-24T03:00:00-05:00"
		}
	]
}