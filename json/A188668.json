{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A188668",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 188668,
			"data": "0,1,0,0,1,-2,4,-1,0,0,0,1,3,-9,9,-2,-21,27,-9,1,0,0,0,0,1,-4,16,-24,16,-3,92,-176,120,-32,3,-232,256,-96,16,-1,0,0,0,0,0,1,5,-25,50,-50,25,-4,-315,775,-750,350,-75,6,2115,-3275,1950,-550,75,-4,-3005,3125,-1250,250,-25,1",
			"name": "Triangle read by rows: row n gives (coefficients * n!) in expansion of pieces k=0..n-1 of the cumulative distribution function for the Irwin-Hall distribution, lowest powers first.",
			"comment": [
				"This is the probability distribution for the sum of n independent, random variables, each uniformly distributed on [0,1)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A188668/b188668.txt\"\u003eRows n = 1..31, flattened\u003c/a\u003e",
				"Philip Hall, \u003ca href=\"http://www.jstor.org/stable/2331961\"\u003eThe Distribution of Means for Samples of Size N Drawn from a Population in which the Variate Takes Values Between 0 and 1, All Such Values Being Equally Probable\u003c/a\u003e, Biometrika, Vol. 19, No. 3/4. (Dec., 1927), pp. 240-245.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Irwin%E2%80%93Hall_distribution\"\u003eIrwin-Hall distribution\u003c/a\u003e"
			],
			"formula": [
				"G.f. for piece k in row n: (1/n!) * Sum_{j=0..k} (-1)^j * C(n,j) * (x-j)^n."
			],
			"example": [
				"For n = 3, k = 2 (three variables, third piece) the distribution is the polynomial: 1/6 * (1*(x-0)^3 - 3*(x-1)^3 + 3*(x-2)^3) = 1/6 * (-21 + 27*x - 9*x^2 + x^3). That gives the subsequence [-21, 27, -9, 1].",
				"Triangle begins:",
				"  [0, 1];",
				"  [0, 0, 1], [-2, 4, -1];",
				"  [0, 0, 0, 1], [3, -9, 9, -2], [-21, 27, -9, 1];",
				"  ..."
			],
			"maple": [
				"f:= proc(n, k) option remember;",
				"       add((-1)^j * binomial(n, j) * (x-j)^n, j=0..k)",
				"    end:",
				"T:= (n, k)-\u003e seq(coeff(f(n, k), x, t), t=0..n):",
				"seq(seq(T(n, k), k=0..n-1), n=1..7);  # _Alois P. Heinz_, Apr 09 2011"
			],
			"mathematica": [
				"f[n_, k_] := f[n, k] = Sum[(-1)^j*Binomial[n, j]*(x-j)^n, {j, 0, k}]; T[n_, k_] := Table[Coefficient[f[n, k], x, t], {t, 0, n}]; Table[T[n, k], {n, 1, 7}, { k, 0, n-1}] // Flatten (* _Jean-François Alcover_, Feb 26 2017, after _Alois P. Heinz_ *)"
			],
			"keyword": "sign,look,tabf",
			"offset": "1,6",
			"author": "_Thomas Dybdahl Ahle_, Apr 07 2011",
			"references": 2,
			"revision": 30,
			"time": "2021-02-19T07:23:56-05:00",
			"created": "2011-04-09T00:15:48-04:00"
		}
	]
}