{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A221150",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 221150,
			"data": "0,0,1,0,0,0,1,0,0,1,0,0,0,1,0,0,0,1,0,0,1,0,0,0,1,0,0,1,0,0,0,1,0,0,0,1,0,0,1,0,0,0,1,0,0,0,1,0,0,1,0,0,0,1,0,0,1,0,0,0,1,0,0,0,1,0,0,1,0,0,0,1,0,0,1,0",
			"name": "The generalized Fibonacci word f^[3].",
			"comment": [
				"(a(n)) is the [0-\u003e01, 1-\u003e0]-transform of the Fibonacci word A005614, or alternatively (see Ramirez et al.) the [0-\u003e0, 1-\u003e01]-transform of the Fibonacci word A003849. (a(n)) is the homogeneous Sturmian word with slope r = (5-sqrt(5))/10. Since the algebraic conjugate (5+sqrt(5)/10 of r is also in (0,1), (a(n)) is NOT a fixed point of a morphism (by Allauzen's criterion). - _Michel Dekking_, Oct 14 2017",
				"From _Michel Dekking_, Oct 04 2018: (Start)",
				"Let psi_3 be the elementary Sturmian morphism given by",
				"      psi_3(0)=0, psi_3(1)=01,",
				"and let x = A003849 be the Fibonacci word. Then, see previous comment, (a(n)) = psi_3(x). We show that (a(n)) is a fixed point of an automorphism sigma of the free group generated by 0 and 1.",
				"To see this, let gamma be the Fibonacci morphism given by gamma(0)=01, gamma(1)=0. Then gamma(x) = x, and so",
				"      psi_3(gamma(x)) = psi_3(x) = a,",
				"implying that a = (a(n)) is fixed by",
				"      sigma := psi_3 gamma psi_3^{-1}.",
				"One easily computes psi_3^{-1}: 0-\u003e0, 1-\u003e0^{-1}1, which gives sigma:",
				"      sigma(0) = 001,   sigma(1) = 1^{-1}0^{-1}.",
				"(End)",
				"From _Michel Dekking_, Sep 17 2020: (Start)",
				"Although not a fixed point of a morphism, this sequence is a morphic sequence, i.e., the letter-to-letter image of the fixed point of a morphism mu. In fact, one can take the alphabet {1,2,3} with the morphism",
				"      mu:  1-\u003e1231, 2-\u003e12, 3-\u003e3,",
				"and the letter-to-letter map g defined by",
				"       g:  1-\u003e0, 2-\u003e0, 3-\u003e1.",
				"Then (a(n)) = g(x), where x = 1231123123... is the fixed point of the morphism mu starting with 1.",
				"This is obtained by noting that (a(n)) is a decoration of the Fibonacci word abaababaab... by a-\u003e0, b-\u003e01, fixed point of a-\u003eaba, b-\u003eab.",
				"It is well-known that decorated fixed points of morphisms are morphic sequences, and the 'natural' algorithm to achieve this (see my paper on morphic words) yields a morphism on an alphabet of 1+2=3 symbols.",
				"(End)"
			],
			"reference": [
				"Dale Gerdemann, Problem 5.1, Problem Proposals, Ed. Clark Kimberling, Sixteenth International Conference on Fibonacci Numbers and Their Applications, Rochester Institute of Technology, Rochester, New York, July 24, 2014. Fibonacci Quarterly, to appear. [Mentions a sequence that appears to match this entry]"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A221150/b221150.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"W. W. Adams and J. L. Davison, \u003ca href=\"https://doi.org/10.1090/S0002-9939-1977-0441879-4\"\u003eA remarkable class of continued fractions\u003c/a\u003e, Proc. Amer. Math. Soc. 65 (1977), 194-198.",
				"P. G. Anderson, T. C. Brown, P. J.-S. Shiue, \u003ca href=\"https://doi.org/10.1090/S0002-9939-1995-1249866-4\"\u003eA simple proof of a remarkable continued fraction identity\u003c/a\u003e Proc. Amer. Math. Soc. 123 (1995), 2005-2009.",
				"Eunice Y. S. Chan, Robert M. Corless, Laureano Gonzalez-Vega, J. Rafael Sendra, Juana Sendra, Steven E. Thornton, \u003ca href=\"https://arxiv.org/abs/1809.10664\"\u003eBohemian Upper Hessenberg Toeplitz Matrices\u003c/a\u003e, arXiv:1809.10664 [cs.SC], 2018.",
				"M.Dekking, \u003ca href=\"https://doi.org/10.1016/j.tcs.2019.12.036\"\u003eMorphic words, Beatty sequences and integer images of the Fibonacci language\u003c/a\u003e, Theoretical Computer Science 809, 407-417 (2020).",
				"José L. Ramírez, Gustavo N. Rubiano, and Rodrigo de Castro, \u003ca href=\"http://arxiv.org/abs/1212.1368\"\u003eA Generalization of the Fibonacci Word Fractal and the Fibonacci Snowflake\u003c/a\u003e, arXiv preprint arXiv:1212.1368 [cs.DM], 2012-2014."
			],
			"formula": [
				"Set S_0=0, S_1=001; thereafter S_n = S_{n-1}S_{n-2}; sequence is S_{oo}.",
				"From _Peter Bala_, Nov 19 2013: (Start)",
				"a(n) = floor((n + 2)/(phi + 2)) - floor((n + 1)/(phi + 2)) where phi = 1/2*(1 + sqrt(5)) denotes the golden ratio.",
				"If we read the present sequence as the digits of a decimal constant c = 0.00100 01001 00010 00100 .... then we have the series representation c = Sum_{n \u003e= 1} 1/10^floor(n*(phi + 2)). An alternative representation is c = 9*Sum_{n \u003e= 1} floor(n*(5 - sqrt(5))/10) /10^n.",
				"The constant 9*c has the simple continued fraction representation [0; 111, 10, 10^3, 10^4, 10^7, ..., 10^Lucas(n), ...] (see Adams and Davison). Compare with A230900.",
				"Using this result we can find the alternating series representation c = 9*Sum_{n \u003e= 1} (-1)^(n+1)*(1 + 10^Lucas(3*n))/( (10^Lucas(3*n - 2) - 1)*(10^Lucas(3*n + 1) - 1) ). The series converges very rapidly: for example, the first 10 terms of the series give a value for c accurate to more than 7.8 million decimal places. Cf. A005614. (End)"
			],
			"example": [
				"(a(n)) can be obtained by iteration of sigma starting with 0.",
				"      sigma(0)   =  001,",
				"      sigma^2(0) =  0010011^{-1}0^{-1} = 0010,",
				"      sigma^3(0) =  0010011^{-1}0^{-1}001 = 0010001.",
				"      sigma^4(0) =  0010011^{-1}0^{-1}0010010011^{-1}0^{-1} = 00100010010."
			],
			"maple": [
				"fibi := proc(n,i)",
				"    option remember;",
				"    local j;",
				"    if n = 0 then",
				"        [0] ;",
				"    elif n = 1 then",
				"        [seq(0,j=1..i-1),1] ;",
				"    else",
				"        [op(procname(n-1,i)),op(procname(n-2,i))] ;",
				"    end if;",
				"end proc:",
				"fibonni := proc(n,i)",
				"    local fn;",
				"    for fn from 0 do",
				"        Fn := fibi(fn, i) ;",
				"        if nops( Fn) \u003e= n+1 and nops(Fn) \u003e i+3 then",
				"            return op(n+1, Fn) ;",
				"        end if;",
				"    end do:",
				"end proc:",
				"A221150 := proc(n)",
				"    fibonni(n,3) ;",
				"end proc: # _R. J. Mathar_, Jul 09 2013"
			],
			"mathematica": [
				"Table[Floor[(n + 2)/(GoldenRatio + 2)] - Floor[(n + 1)/(GoldenRatio + 2)], {n, 0, 120}] (* _Michael De Vlieger_, Apr 03 2016 *)",
				"fibi[n_, i_] := fibi[n, i] = Which[n == 0, {0}, n == 1, Append[Table[0, {j, 1, i - 1}], 1], True, Join[fibi[n - 1, i], fibi[n - 2, i]]];",
				"fibonni[n_, i_] := fibonni[n, i] = Module[{fn, Fn}, For[fn = 0, True, fn++, Fn = fibi[fn, i]; If[Length[ Fn] \u003e= n + 1 \u0026\u0026 Length[Fn] \u003e i + 3, Return[ Fn[[n + 1]]]]]];",
				"a[n_] := fibonni[n, 3]; Table[a[n], {n, 0, 100}] (* _Jean-François Alcover_, Nov 21 2017, after _R. J. Mathar_ *)"
			],
			"program": [
				"(MAGMA) [Floor((n+2)/(1/2*(1+Sqrt(5))+2))-Floor((n+1)/(1/2*(1 +Sqrt(5))+2)): n in [0..100]]; // _Vincenzo Librandi_, Oct 15 2017"
			],
			"xref": [
				"Cf. A003849, A005614. A000204, A221151, A221152, A230900."
			],
			"keyword": "nonn",
			"offset": "0",
			"author": "_N. J. A. Sloane_, Jan 03 2013",
			"references": 10,
			"revision": 60,
			"time": "2020-09-19T23:50:06-04:00",
			"created": "2013-01-03T22:39:23-05:00"
		}
	]
}