{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A008440",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 8440,
			"data": "1,6,15,26,45,66,82,120,156,170,231,276,290,390,435,438,561,630,651,780,861,842,1020,1170,1095,1326,1431,1370,1716,1740,1682,2016,2145,2132,2415,2550,2353,2850,3120,2810,3321,3486,3285,3906,4005,3722,4350",
			"name": "Expansion of Jacobi theta constant theta_2^6 /(64q^(3/2)).",
			"comment": [
				"Number of representations of n as sum of 6 triangular numbers. - _Michel Marcus_, Oct 24 2012. See the Ono et al. link."
			],
			"reference": [
				"B. C. Berndt, Fragments by Ramanujan on Lambert series, in Number Theory and Its Applications, K. Gyory and S. Kanemitsu, eds., Kluwer, Dordrecht, 1999, pp. 35-49, see Entry 6.",
				"J. H. Conway and N. J. A. Sloane, \"Sphere Packings, Lattices and Groups\", Springer-Verlag, p. 102."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A008440/b008440.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e (terms 0..1000 from Vincenzo Librandi)",
				"B. C. Berndt, \u003ca href=\"http://www.math.uiuc.edu/~berndt/lambertseries.ps\"\u003eFragments by Ramanujan on Lambert series\u003c/a\u003e.",
				"K. Ono, S. Robins and P. T. Wahl, \u003ca href=\"http://www.mathcs.emory.edu/~ono/publications-cv/pdfs/006.pdf\"\u003eOn the representation of integers as sums of triangular numbers\u003c/a\u003e, Aequationes mathematicae, August 1995, Volume 50, Issue 1-2, pp 73-94. Theorem 4."
			],
			"formula": [
				"Expansion of Ramanujan phi^6(q) in powers of q.",
				"Expansion of q^(-3/4)(eta(q^2)^2/eta(q))^6 in powers of q.",
				"Euler transform of period 2 sequence [6, -6, ...]. - _Michael Somos_, May 23 2006",
				"G.f.: (Sum_{n\u003e=0} x^((n^2+n)/2))^6.",
				"a(n) = (-1/8)*Sum_{d divides (4n+3)} Chi_2(4;d)*d^2. - _Michel Marcus_, Oct 24 2012. See the Ono et al. link. Theorem 4.",
				"a(n) =(-1/8)*A002173(4*n+3). This is the preceding formula. - _Wolfdieter Lang_, Jan 12 2017",
				"a(0) = 1, a(n) = (6/n)*Sum_{k=1..n} A002129(k)*a(n-k) for n \u003e 0. - _Seiichi Manyama_, May 06 2017",
				"G.f.: exp(Sum_{k\u003e=1} 6*(x^k/k)/(1 + x^k)). - _Ilya Gutkovskiy_, Jul 31 2017"
			],
			"example": [
				"G.f. = 1 + 6*x + 15*x^2 + 26*x^3 + 45*x^4 + 66*x^5 + 82*x^6 + ... - _Michael Somos_, Jun 25 2019",
				"G.f. = q^3 + 6*q^7 + 15*q^11 + 26*q^15 + 45*q^19 + 66*q^23 + 82*q^27 + ..."
			],
			"mathematica": [
				"CoefficientList[(QPochhammer[q^2]^2 / QPochhammer[q])^6 + O[q]^50, q] (* _Jean-François Alcover_, Nov 05 2015 *)",
				"a[ n_] := If[ n \u003c 0, 0, -DivisorSum[ 4 n + 3, Re[I^(# - 1)] #^2 \u0026] / 8]; (* _Michael Somos_, Jun 25 2019 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, 0, polcoeff( sum(k=0, (sqrtint(8*n+1)-1)\\2, x^((k^2+k)/2), x * O(x^n))^6, n))}; /* _Michael Somos_, May 23 2006 */",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( (eta(x^2 + A)^2 / eta(x + A))^6, n))}; /* _Michael Somos_, May 23 2006 */",
				"(PARI) {a(n)= -sumdiv(4*n + 3, d, real(I^(d-1))*d^2)/8}; /* _Michael Somos_, Oct 24 2012 */"
			],
			"xref": [
				"Number of ways of writing n as a sum of k triangular numbers, for k=1,...: A010054, A008441, A008443, A008438, A008439, A008440, A226252, A007331, A226253, A226254, A226255, A014787, A014809, A002173."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 19,
			"revision": 52,
			"time": "2019-06-25T09:48:31-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}