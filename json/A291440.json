{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A291440",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 291440,
			"data": "0,1,0,2,0,2,-1,2,6,9,5,9,3,8,12,18,12,17,8,14,21,28,18,24,33,41,48,56,46,54,41,51,60,70,79,89,75,84,96,107,94,105,87,99,110,123,104,117,132,142,153,168,153,165,178,189,201,218,198,214,195,208,225,240,254,270,248,263,280,293,275,290,264,281,298,316,338,352,327,350",
			"name": "a(n) = pi(n^2) - pi(n)^2, where pi(n) = A000720(n).",
			"comment": [
				"The only zero values are a(1) = a(3) = a(5) = 0. The only negative value is a(7) = -1. In particular, pi(n^2) \u003e pi(n)^2 for n \u003e 7. These can be proved by the PNT with error term for large n and computation for smaller n.",
				"For prime(n)^2 - prime(n^2), see A123914.",
				"For pi(n^3) - pi(n)^3, see A291538.",
				"Mincu and Panaitopol (2008) prove that pi(m*n) \u003e= pi(m)*pi(n) for all positive m and n except for m = 5, n = 7; m = 7, n = 5; and m = n = 7. This implies for m = n that a(n) \u003e= 0 if n \u003c\u003e 7. - _Jonathan Sondow_, Nov 03 2017",
				"Diagonal of the triangular array A294508. - _Jonathan Sondow_ and _Robert G. Wilson v_, Nov 08 2017"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A291440/b291440.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Gabriel Mincu and Laurentiu Panaitopol, \u003ca href=\"https://www.emis.de/journals/JIPAM/article951.html\"\u003eProperties of some functions connected to prime numbers\u003c/a\u003e, J. Inequal. Pure Appl. Math., 9 No. 1 (2008), Art. 12."
			],
			"formula": [
				"a(n) = A000720(n^2) - A000720(n)^2.",
				"a(n) ~ (n^2 / log(n))*(1/2 - 1/log(n)) as n tends to infinity, by the PNT.",
				"From _Jonathan Sondow_ and _Robert G. Wilson v_, Nov 08 2017: (Start)",
				"a(n) = A294508(n*(n+1)/2).",
				"a(n) \u003e= A294509(n). (End)"
			],
			"example": [
				"a(7) = pi(7^2) - pi(7)^2 = 15 - 4^2 = -1."
			],
			"maple": [
				"seq(numtheory:-pi(n^2)-numtheory:-pi(n)^2, n=1..100); # _Robert Israel_, Aug 25 2017"
			],
			"mathematica": [
				"Table[PrimePi[n^2] - PrimePi[n]^2, {n, 80}]"
			],
			"program": [
				"(MAGMA) [#PrimesUpTo(n^2)-#PrimesUpTo(n)^2: n in [1..80]]; // _Vincenzo Librandi_, Aug 26 2017",
				"(PARI) a(n) = primepi(n^2) - primepi(n)^2; \\\\ _Michel Marcus_, Sep 10 2017"
			],
			"xref": [
				"Cf. A000720, A123914, A262199, A291538, A291539, A291540, A291541, A291542."
			],
			"keyword": "sign",
			"offset": "1,4",
			"author": "_Jonathan Sondow_, Aug 23 2017",
			"references": 10,
			"revision": 41,
			"time": "2017-11-08T10:48:17-05:00",
			"created": "2017-08-25T03:24:13-04:00"
		}
	]
}