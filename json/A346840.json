{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A346840",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 346840,
			"data": "1,1,3,19,323,38716,32253681,78267222216,762698791293515,37603168183752885658,9243338412512497704718998,11335247475355582487279558180986,68305298969167998414438069494886302081,1920885596256995709122789811280666218400361901",
			"name": "Number of n-dimensional lattice walks from {1}^n to {0}^n using steps that decrease the Euclidean distance to the origin and that change each coordinate by at most 1.",
			"comment": [
				"Lattice points may have negative coordinates, and different walks may differ in length.  All walks are self-avoiding."
			],
			"link": [
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Lattice_path#Counting_lattice_paths\"\u003eCounting lattice paths\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Self-avoiding_walk\"\u003eSelf-avoiding walk\u003c/a\u003e"
			],
			"example": [
				"a(3) = 19:",
				"  ((1,1,1), (0,0,0)),",
				"  ((1,1,1), (0,0,1), (0,0,0)),",
				"  ((1,1,1), (0,1,0), (0,0,0)),",
				"  ((1,1,1), (0,1,1), (0,0,0)),",
				"  ((1,1,1), (1,0,0), (0,0,0)),",
				"  ((1,1,1), (1,0,1), (0,0,0)),",
				"  ((1,1,1), (1,1,0), (0,0,0)),",
				"  ((1,1,1), (0,1,1), (-1,0,0), (0,0,0)),",
				"  ((1,1,1), (0,1,1), (0,0,1), (0,0,0)),",
				"  ((1,1,1), (0,1,1), (0,1,0), (0,0,0)),",
				"  ((1,1,1), (0,1,1), (1,0,0), (0,0,0)),",
				"  ((1,1,1), (1,0,1), (0,-1,0), (0,0,0)),",
				"  ((1,1,1), (1,0,1), (0,0,1), (0,0,0)),",
				"  ((1,1,1), (1,0,1), (0,1,0), (0,0,0)),",
				"  ((1,1,1), (1,0,1), (1,0,0), (0,0,0)),",
				"  ((1,1,1), (1,1,0), (0,0,-1), (0,0,0)),",
				"  ((1,1,1), (1,1,0), (0,0,1), (0,0,0)),",
				"  ((1,1,1), (1,1,0), (0,1,0), (0,0,0)),",
				"  ((1,1,1), (1,1,0), (1,0,0), (0,0,0))."
			],
			"maple": [
				"s:= proc(n) option remember;",
				"     `if`(n=0, [[]], map(x-\u003e seq([x[], i], i=-1..1), s(n-1)))",
				"    end:",
				"b:= proc(l) option remember; (n-\u003e `if`(l=[0$n], 1, add((h-\u003e `if`(",
				"      add(i^2, i=h)\u003cadd(i^2, i=l), b(sort(h)), 0))(l+x), x=s(n))))(nops(l))",
				"    end:",
				"a:= n-\u003e b([1$n]):",
				"seq(a(n), n=0..10);"
			],
			"mathematica": [
				"s[n_] := s[n] = If[n == 0, {{}}, Sequence @@ Table[Append[#, i], {i, -1, 1}]\u0026 /@ s[n-1]];",
				"b[l_List] := b[l] = With[{n = Length[l]}, If[l == Table[0, {n}], 1, Sum[With[{h = l+x}, If[h.h \u003c l.l, b[Sort[h]], 0]], {x, s[n]}]]];",
				"a[n_] := b[Table[1, {n}]];",
				"Table[Print[n, \" \", a[n]]; a[n], {n, 0, 13}] (* _Jean-François Alcover_, Nov 04 2021, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Row n=1 of A347811."
			],
			"keyword": "nonn,walk",
			"offset": "0,3",
			"author": "_Alois P. Heinz_, Sep 14 2021",
			"references": 1,
			"revision": 31,
			"time": "2021-11-04T05:59:27-04:00",
			"created": "2021-09-15T19:06:15-04:00"
		}
	]
}