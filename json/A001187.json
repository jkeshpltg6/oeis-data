{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001187",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1187,
			"id": "M3671 N1496",
			"data": "1,1,1,4,38,728,26704,1866256,251548592,66296291072,34496488594816,35641657548953344,73354596206766622208,301272202649664088951808,2471648811030443735290891264,40527680937730480234609755344896,1328578958335783201008338986845427712",
			"name": "Number of connected labeled graphs with n nodes.",
			"comment": [
				"\"Based on experimental data obtained using the software LattE [14] and the Online Encyclopedia of Integer Sequences [19], we make the following conjecture: Conjecture 11. For j \u003e= 2, Vol(C_j ) is equal to the number of labeled connected graphs on j - 1 vertices.\" [Beck et al., 2011]",
				"For n \u003e 1, a(n) is log-convex. Furthermore, a(n+1)*a(n-1) ~ 2*a(n)*a(n). - _Ran Pan_, Oct 28 2015",
				"a(n) is also the number of tournaments on {1,...,n} for which 1 is reachable from every vertex. - _Don Knuth_, Aug 06 2020"
			],
			"reference": [
				"Miklos Bona, editor, Handbook of Enumerative Combinatorics, CRC Press, 2015, p. 398-402.",
				"D. G. Cantor, personal communication.",
				"Cowan, D. D.; Mullin, R. C.; Stanton, R. G. Counting algorithms for connected labelled graphs. Proceedings of the Sixth Southeastern Conference on Combinatorics, Graph Theory, and Computing (Florida Atlantic Univ., Boca Raton, Fla., 1975), pp. 225-236. Congressus Numerantium, No. XIV, Utilitas Math., Winnipeg, Man., 1975. MR0414417 (54 #2519).",
				"J. L. Gross and J. Yellen, eds., Handbook of Graph Theory, CRC Press, 2004; p. 518.",
				"F. Harary and E. M. Palmer, Graphical Enumeration, Academic Press, NY, 1973, p. 7.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence).",
				"R. P. Stanley, Enumerative Combinatorics, Cambridge, Vol. 2, 1999; see Example 5.2.1.",
				"H. S. Wilf, Generatingfunctionology, Academic Press, NY, 1990, p. 78."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A001187/b001187.txt\"\u003eTable of n, a(n) for n = 0..50\u003c/a\u003e",
				"Matthias Beck, Benjamin Braun and Nguyen Le, \u003ca href=\"http://arxiv.org/abs/1103.1070\"\u003eMahonian partition identities via polyhedral geometry\u003c/a\u003e, arXiv:1103.1070 [math.NT], 2011.",
				"Huantian Cao, \u003ca href=\"https://getd.libs.uga.edu/pdfs/cao_huantian_200205_ms.pdf\"\u003eAutoGF: An Automated System to Calculate Coefficients of Generating Functions\u003c/a\u003e, MS Thesis, 2002.",
				"Patrick De Causmaecker, Stefan De Wannemacker, \u003ca href=\"http://arxiv.org/abs/1407.4288\"\u003eOn the number of antichains of sets in a finite universe\u003c/a\u003e, arXiv:1407.4288 [math.CO], 2014.",
				"Steven R. Finch, \u003ca href=\"https://doi.org/10.1017/9781316997741\"\u003eMathematical Constants II\u003c/a\u003e, Encyclopedia of Mathematics and Its Applications, Cambridge University Press, Cambridge, 2018.",
				"P. Flajolet and R. Sedgewick, \u003ca href=\"http://algo.inria.fr/flajolet/Publications/books.html\"\u003eAnalytic Combinatorics\u003c/a\u003e, 2009; see page 138.",
				"Philippe Fournier Viger, Ganghuan He, Chun-Wei Jerry Lin, Heitor Murilo Gomes, \u003ca href=\"https://doi.org/10.1007/978-3-030-59065-9_14\"\u003eMining Attribute Evolution Rules in Dynamic Attributed Graphs\u003c/a\u003e, Proceedings of the Big Data Analytics and Knowledge Discovery, 22nd International Conference, (Bratislava, Slovakia, DaWaK 2020).",
				"Adriano M. Garsia, James Haglund, Dun Qiu, Marino Romero, \u003ca href=\"https://arxiv.org/abs/1904.07912\"\u003ee-Positivity Results and Conjectures\u003c/a\u003e, arXiv:1904.07912 [math.CO], 2019.",
				"E. N. Gilbert, \u003ca href=\"http://dx.doi.org/10.4153/CJM-1956-046-2\"\u003eEnumeration of labelled graphs\u003c/a\u003e, Canad. J. Math., 8 (1956), 405-411.",
				"E. N. Gilbert, \u003ca href=\"/A001187/a001187.pdf\"\u003eEnumeration of labelled graphs\u003c/a\u003e (Annotated scanned copy)",
				"M. Konvalinka and I. Pak, \u003ca href=\"https://www.math.ucla.edu/~pak/papers/CayleyComp7.pdf\"\u003eCayley compositions, partitions, polytopes, and geometric bijections\u003c/a\u003e, preprint.",
				"M. Konvalinka and I. Pak, \u003ca href=\"https://doi.org/10.1016/j.jcta.2013.11.008\"\u003eCayley compositions, partitions, polytopes, and geometric bijections\u003c/a\u003e, Journal of Combinatorial Theory, Series A, Volume 123, Issue 1, April 2014, Pages 86-91.",
				"Arun P. Mani, R. J. Stones, \u003ca href=\"https://doi.org/10.1137/15M1024615\"\u003eThe Number of Labeled Connected Graphs Modulo Prime Powers\u003c/a\u003e, SIAM Journal on Discrete Mathematics, Vol. 30, No. 2, pp. 1046-1057.",
				"Alexey A. Melnikov, Leonid E. Fedichkin, Ray-Kuang Lee, Alexander Alodjants, \u003ca href=\"https://arxiv.org/abs/2001.05472\"\u003eMachine learning transfer efficiencies for noisy quantum walks\u003c/a\u003e, arXiv:2001.05472 [quant-ph], 2020. Also in \u003ca href=\"https://doi.org/10.1002/qute.201900115\"\u003eAdvanced Quantum Technologies\u003c/a\u003e (2020) Vol. 3, 1900115.",
				"Albert Nijenhuis and Herbert S. Wilf, \u003ca href=\"http://dx.doi.org/10.1016/0097-3165(79)90023-2\"\u003eThe enumeration of connected graphs and linked diagrams\u003c/a\u003e, J. Combin. Theory Ser. A 27 (1979), no. 3, 356--359. MR0555804 (82b:05074)",
				"J. Novak, \u003ca href=\"http://arxiv.org/abs/1205.2097\"\u003eThree lectures on free probability\u003c/a\u003e, arXiv preprint arXiv:1205.2097 [math.CO], 2012.",
				"Igor Pak, \u003ca href=\"https://arxiv.org/abs/1803.06636\"\u003eComplexity problems in enumerative combinatorics\u003c/a\u003e, arXiv:1803.06636 [math.CO], 2018.",
				"R. W. Robinson, \u003ca href=\"/A001187/a001187_1.pdf\"\u003eFirst 50 terms of A1187 and A1188\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/transforms.txt\"\u003eTransforms\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ConnectedGraph.html\"\u003eConnected Graph\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/LabeledGraph.html\"\u003eLabeled Graph\u003c/a\u003e",
				"H. S. Wilf, \u003ca href=\"http://www.math.upenn.edu/~wilf/DownldGF.html\"\u003eGeneratingfunctionology\u003c/a\u003e, 2nd edn., Academic Press, NY, 1994, p. 87, Eq. 3.10.2."
			],
			"formula": [
				"n * 2^binomial(n, 2) = Sum_{k=1..n} binomial(n, k) * k * a(k) * 2^binomial(n-k, 2).",
				"E.g.f.: 1 + log( sum( 2^binomial(n, 2) * x^n / n!, n=0..infinity) ). - _Michael Somos_, Jun 12 2000"
			],
			"example": [
				"E.g.f.: 1 + x + x^2/2! + 4*x^3/3! + 38*x^4/4! + 728*x^5/5! + 26704*x^6/6! + 1866256*x^7/7! + 251548592*x^8/8! + ..."
			],
			"maple": [
				"t1 := 1+log( add(2^binomial(n,2)*x^n/n!,n=0..30)): t2 := series(t1,x,30): A001187 := n-\u003en!*coeff(t2,x,n);",
				"# second Maple program:",
				"a:= proc(n) option remember; `if`(n=0, 1, 2^(n*(n-1)/2)-",
				"      add(k*binomial(n, k)* 2^((n-k)*(n-k-1)/2)*a(k), k=1..n-1)/n)",
				"    end:",
				"seq(a(n), n=0..20);  # _Alois P. Heinz_, Aug 26 2013"
			],
			"mathematica": [
				"g = Sum[2^Binomial[n, 2] x^n/n!, {n, 0, 20}]; Range[0, 20]! CoefficientList[Series[Log[g] + 1, {x, 0, 20}], x] (* _Geoffrey Critzer_, Nov 12 2011*)",
				"a[n_] := a[n] = If[n == 0, 1, 2^(n*(n-1)/2) - Sum[k*Binomial[n, k]* 2^((n-k)*(n-k-1)/2)*a[k], {k, 1, n-1}]/n]; Table[a[n], {n, 0, 20}] (* _Jean-François Alcover_, Apr 09 2014, after _Alois P. Heinz_ *)",
				"a[ n_] := If[ n \u003c 0, 0, n! SeriesCoefficient[ 1 + Log[ Sum[ 2^(k (k - 1)/2) x^k/k!, {k, 0, n}]], {x, 0, n}]]; (* _Michael Somos_, Jul 11 2019 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n \u003c 0, 0, n! * polcoeff( 1 + log( sum( k=0, n, 2^binomial(k, 2) * x^k / k!, x * O(x^n))), n))}; /* _Michael Somos_, Jun 12 2000 */",
				"(Sage)",
				"@cached_function",
				"def A001187(n):",
				"    if n == 0: return 1",
				"    return 2^(n*(n-1)/2)- sum(k*binomial(n, k)*2^((n-k)*(n-k-1)/2)*A001187(k) for k in (1..n-1))/n",
				"[A001187(n) for n in (0..15)] # _Peter Luschny_, Jan 17 2016"
			],
			"xref": [
				"Logarithmic transform of A006125 (labeled graphs). Cf. A053549.",
				"Row sums of triangle A062734."
			],
			"keyword": "nonn,nice,easy",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_",
			"references": 112,
			"revision": 137,
			"time": "2020-12-16T19:12:30-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}