{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A191448",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 191448,
			"data": "1,3,2,7,5,4,15,11,9,6,31,23,19,13,8,63,47,39,27,17,10,127,95,79,55,35,21,12,255,191,159,111,71,43,25,14,511,383,319,223,143,87,51,29,16,1023,767,639,447,287,175,103,59,33,18,2047,1535,1279,895,575",
			"name": "Dispersion of the odd integers greater than 1, by antidiagonals.",
			"comment": [
				"Background discussion:  Suppose that s is an increasing sequence of positive integers, that the complement t of s is infinite, and that t(1)=1.  The dispersion of s is the array D whose n-th row is (t(n), s(t(n)), s(s(t(n)), s(s(s(t(n)))), ...).  Every positive integer occurs exactly once in D, so that, as a sequence, D is a permutation of the positive integers.  The sequence u given by u(n)=(number of the row of D that contains n) is a fractal sequence.  Examples:",
				"(1) s=A000040 (the primes), D=A114537, u=A114538.",
				"(2) s=A022343 (without initial 0), D=A035513 (Wythoff array), u=A003603.",
				"(3) s=A007067, D=A035506 (Stolarsky array), u=A133299.",
				"More recent examples of dispersions: A191426-A191455."
			],
			"link": [
				"Ivan Neretin, \u003ca href=\"/A191448/b191448.txt\"\u003eTable of n, a(n) for n = 1..5050\u003c/a\u003e (first 100 antidiagonals, flattened)"
			],
			"example": [
				"Northwest corner:",
				"1...3...7...15..31",
				"2...5...11..23..47",
				"4...9...19..39..79",
				"6...13..27..55..111",
				"8...17..35..71..143"
			],
			"mathematica": [
				"(* Program generates the dispersion array T of increasing sequence f[n] *)",
				"r=40; r1=12; c=40; c1=12;",
				"f[n_] :=2n+1 (* complement of column 1 *)",
				"mex[list_] := NestWhile[#1 + 1 \u0026, 1, Union[list][[#1]] \u003c= #1 \u0026, 1, Length[Union[list]]]",
				"rows = {NestList[f, 1, c]};",
				"Do[rows = Append[rows, NestList[f, mex[Flatten[rows]], r]], {r}];",
				"t[i_, j_] := rows[[i, j]];",
				"TableForm[Table[t[i, j], {i, 1, 10}, {j, 1, 10}]]",
				"(* A191448 array *)",
				"Flatten[Table[t[k, n - k + 1], {n, 1, c1}, {k, 1, n}]] (* A191448 sequence *)",
				"(* Program by _Peter J. C. Moses_, Jun 01 2011 *)"
			],
			"xref": [
				"Cf. A114537, A035513, A035506."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Jun 05 2011",
			"references": 6,
			"revision": 12,
			"time": "2017-10-17T19:31:38-04:00",
			"created": "2011-06-06T12:51:17-04:00"
		}
	]
}