{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A275055",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 275055,
			"data": "1,1,2,1,3,1,2,4,1,5,1,2,3,6,1,7,1,2,4,8,1,3,9,1,2,5,10,1,11,1,2,4,3,6,12,1,13,1,2,7,14,1,3,5,15,1,2,4,8,16,1,17,1,2,3,6,9,18,1,19,1,2,4,5,10,20,1,3,7,21,1,2,11,22,1,23,1,2,4,8,3,6,12,24,1,5,25,1,2,13,26,1,3",
			"name": "Irregular triangle read by rows listing divisors d of n in order of appearance in a matrix of products that arranges the powers of prime divisors p of n along independent axes.",
			"comment": [
				"a(p^e) = A027750(p^e) for e \u003e= 1.",
				"The matrix of products that are divisors of n is arranged such that the powers of the prime divisors range across an axis, one axis per prime divisor. Thus a squarefree semiprime has a 2-dimensional matrix, a sphenic number has 3 dimensions, etc.",
				"Generally, the number of dimensions for the matrix of divisors = omega(n) = A001221(n). Because of this, tau(n)*(mod omega(n)) = 0 for n \u003e 1.",
				"This follows from the formula for tau(n).",
				"Prime divisors p of n are considered in numerical order.",
				"Product matrix of tensors T = 1,p,p^2,...,p^e that include the powers 1 \u003c= e of the prime divisor p that divide n."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A275055/b275055.txt\"\u003eTable of n, a(n) for n = 1..11214\u003c/a\u003e (Rows 1 \u003c= n \u003c= 1500)",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Divisor.html\"\u003eDivisor\u003c/a\u003e"
			],
			"example": [
				"Triangle begins:",
				"1;",
				"1, 2;",
				"1, 3;",
				"1, 2, 4;",
				"1, 5;",
				"1, 2, 3, 6;",
				"1, 7;",
				"1, 2, 4, 8;",
				"1, 3, 9;",
				"1, 2, 5, 10;",
				"1, 11;",
				"1, 2, 4, 3, 6, 12;",
				"1, 13;",
				"1, 2, 7, 14;",
				"1, 3, 5, 15;",
				"1  2, 4, 8, 16;",
				"1, 17;",
				"1, 2, 3, 6, 9, 18;",
				"...",
				"2 prime divisors: n = 72",
				"   1  2  4  8",
				"   3  6 12 24",
				"   9 18 36 72",
				"   thus a(72) = {1, 2, 4, 8, 3, 6, 12, 24, 9, 18, 36, 72}",
				"3 prime divisors: n = 60",
				"(the 3 dimensional levels correspond with powers of 5)",
				"  level 5^0:        level 5^1:",
				"   1  2  4    |     5  10  20",
				"   3  6 12    |    15  30  60",
				"   thus a(60) = {1, 2, 4, 3, 6, 12, 5, 10, 20, 15, 30, 60}",
				"4 prime divisors: n = 210",
				"(the 3 dimensional levels correspond with powers of 5,",
				"the 4 dimensional levels correspond with powers of 7)",
				"  level 5^0*7^0:    level 5^1*7^0:",
				"     1   2     |     5  10",
				"     3   6     |    15  30",
				"  level 5^0*7^1:    level 5^1*7^1:",
				"     7  14     |    35  70",
				"    21  42     |   105 210",
				"   thus a(210) = {1,2,3,6,5,10,15,30,7,14,21,42,35,70,105,210}"
			],
			"mathematica": [
				"{{1}}~Join~Table[TensorProduct @@ Reverse@ Apply[PowerRange[1, #1^#2, #1] \u0026, # \u0026@ FactorInteger@ n, 1], {n, 2, 30}] // Flatten"
			],
			"xref": [
				"Cf. A027750, A000005 (row length), A000203 (row sums), A056538."
			],
			"keyword": "nonn,easy,tabf",
			"offset": "1,3",
			"author": "_Michael De Vlieger_, Jul 14 2016",
			"references": 4,
			"revision": 12,
			"time": "2016-07-28T21:47:50-04:00",
			"created": "2016-07-27T21:24:21-04:00"
		}
	]
}