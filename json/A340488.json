{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A340488",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 340488,
			"data": "0,0,1,1,0,2,2,3,3,2,0,3,1,3,0,4,4,5,5,4,6,6,7,7,6,4,7,5,7,4,0,5,6,5,1,2,1,5,0,6,2,6,3,7,3,6,0,7,2,7,1,4,1,7,0,8,8,9,9,8,10,10,11,11,10,8,11,9,11,8,12,12,13,13,12,14,14",
			"name": "a(n+1) = XOR(a(n),y) for n\u003e=1, where y is the number of m \u003c n such that a(m) = a(n); a(1)=0.",
			"comment": [
				"It appears that the graph of the first n terms is similar to the graph of the first n/4 terms for sufficiently large values (n \u003e 100).",
				"It appears that among the first 4^n terms, each integer value less than 2^n appears at least (3/4)*2^n times. This would imply that every positive integer appears an infinite number of times.",
				"Yet another variant of the Van Eck sequence A181391. - _N. J. A. Sloane_, Jan 10 2021",
				"It follows from the definition that when m appears for the first time, it is immediately followed by m, and then if m is even by m+1. A340494 gives a conjectured generating function for when a number appears for the first time. - _Rémy Sigrist_ and _N. J. A. Sloane_, Jan 10 2021",
				"Conjectures from _Rémy Sigrist_, Jan 10 2021 (Start)",
				"It appears that the positions of the 0's are given by A336190, and the \"y\" sequence appears to be A336033.",
				"For a fixed k \u003e 0, let b(n) be the position of the k-th occurrence of n in A340488, Then the sequence { b(n) - A340494(n) } has period 2^m for some m,",
				"- for k = 2 we have period ( 1 ) (the second occurrence appear next to the first).",
				"- for k = 3 we have period ( 4, 10, 4, 4 ),",
				"- for k = 4 we have period (10, 32, 30, 6, 10, 14, 12, 6). (End)",
				"Theorem: Every number appears.",
				"Proof. (i) If there were only finitely many different numbers in the sequence then one number k (say) would appear infinitely often. As the number of k's reaches the next power of 2, 2^m say, we get a term \u003e= 2^m. So the sequence is unbounded. Contradiction. So there are infinitely many different terms.",
				"(ii) To show that every number appears, consider a k-bit number n, n \u003c 2^k. Let a(m) be the first term \u003e= 2^k, where a(m) = a(m-1) XOR y, with a(m-1) \u003c 2^k and y \u003e= 2^k. Let a(m-1) = t. Since this is at least the 2^k-th copy of t, the sequence contains t XOR 0, t XOR 1, t XOR 2, ..., t XOR 2^k-1, and so contains every number from 0 to 2^k-1, and in particular contains n. QED",
				"  - _Rémy Sigrist_ and _N. J. A. Sloane_, Jan 11 2021",
				"Conjecture: Let pi_i(n) denote the number of occurrences of i in the first n terms. Then pi_0(n) \u003e= pi_i(n) for all i, and the first time pi_0(n) = m, pi_i(n) \u003c m for i\u003e0. - _N. J. A. Sloane_, Jan 13 2021"
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A340488/b340488.txt\"\u003eTable of n, a(n) for n = 1..25000\u003c/a\u003e",
				"Ian Hutchinson, \u003ca href=\"/A340488/a340488.png\"\u003eGraph of first 3500 terms\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A340488/a340488.gp.txt\"\u003ePARI program for A340488\u003c/a\u003e"
			],
			"example": [
				"We start with a(1) = 0. 0 has not appeared before, so we set a(2) to bitxor(0,0), or 0. There has now been one previous appearance of 0, so we set a(3) to bitxor(0,1), or 1. There has been no previous appearance of 1, so we set a(4) to 1. There has now been one previous appearance of 1, so we set a(5) to bitxor(1,1), or 0."
			],
			"maple": [
				"b:= proc(n) option remember; `if`(n\u003c1, 0, b(n-1)+x^a(n)) end:",
				"a:= proc(n) option remember; `if`(n=1, 0, (t-\u003e",
				"      Bits[Xor](t, coeff(b(n-2), x, t)))(a(n-1)))",
				"    end:",
				"seq(a(n), n=1..100);  # _Alois P. Heinz_, Apr 14 2021"
			],
			"mathematica": [
				"b[n_] := b[n] = If[n \u003c 1, 0, b[n-1] + x^a[n]];",
				"a[n_] := a[n] = If[n == 1, 0, With[{t = a[n-1]},",
				"     BitXor[t, Coefficient[b[n - 2], x, t]]]];",
				"Array[a, 100] (* _Jean-François Alcover_, Jun 27 2021, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(Python)",
				"a340488 = [0]",
				"repeat = [0] # Running tally for occurrences of each value",
				"for n in range(3414):",
				"    if(a340488[-1] \u003e= len(repeat)):",
				"        repeat.append(0)",
				"    newValue = (a340488[-1] ^ repeat[a340488[-1]])",
				"    repeat[a340488[-1]] += 1",
				"    a340488.append(newValue)",
				"(PARI) See Links section."
			],
			"xref": [
				"Cf. A181391 (Van Eck), A340496 (partial sums), A340499 (first differences), A340500 (running maximum).",
				"See A340494, A340495 for when n first appears.",
				"Cf. also A336033, A336190."
			],
			"keyword": "nonn,nice,look",
			"offset": "1,6",
			"author": "_Ian Hutchinson_, Jan 09 2021",
			"references": 12,
			"revision": 90,
			"time": "2021-06-27T14:01:35-04:00",
			"created": "2021-01-10T11:33:55-05:00"
		}
	]
}