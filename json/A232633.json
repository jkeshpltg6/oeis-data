{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A232633",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 232633,
			"data": "0,1,-4,1,-3,1,-2,1,5,-5,1,-1,1,-7,14,-7,1,2,-4,1,-3,9,-6,1,1,-3,1,-11,55,-77,44,-11,1,1,-4,1,13,-91,182,-156,65,-13,1,-1,6,-5,1,1,-8,14,-7,1,2,-16,20,-8,1,17,-204,714,-1122,935,-442,119,-17,1,-1,9,-6,1,-19,285,-1254,2508,-2717,1729,-665,152,-19,1",
			"name": "Coefficient table for minimal polynomials of s(n)^2 = (2*sin(Pi/n))^2.",
			"comment": [
				"The length of row n of this table is 1 +  A023022(n), n \u003e= 0, that is  2, 2, 2, 2, 3, 2, 4, 3, 4, 3, 6, 3, 7, 4, 5, 5, 9, 4,...",
				"s(n):= 2*sin(Pi/n) is for n \u003e= 2 the length ratio side/R of a regular n-gon inscribed in a circle of radius R (in some units). s(1) = 0.  In general s(n)^2 = 4 - rho(n)^2 with rho(n):= 2*cos(Pi/n), for n\u003e=2 this is the length ratio (smallest diagonal)/s(n) in the regular n-gon. If n is even, say 2*l, l\u003e=1, then s(2*l)^2 = 2 - rho(l) (because rho(2*l)^2 = rho(l) +2). Therefore, if n is even s(n)^2 is an integer in the algebraic number field Q(rho(n/2)), and if n is odd then it is an integer in Q(rho(n)). The coefficient tables for the minimal polynomials of s(n)^2, called MPs2(n, x), for even and odd n have been given in A232631 and A232632, respectively. See these entries for details, and the link to the Q(2 cos(pi/n)) paper, Table 4, in A187360 for the power basis representation of the zeros of the minimal polynomial C(n, x) of rho(n).",
				"The degree deg(n) of MPs2(n, x) is therefore delta(n/2) or delta(n) for n even or odd, respectively, where delta(n) = A055034(n). This means that deg(1) = deg(2) =1 and deg(n) = phi(n)/2 = A023022(n), n \u003e= 3. deg(n) = A023022(n).",
				"Especially MPs2(p, x) =  product(x - 2*(1 + cos(Pi*(2*j+1)/p)), j=0..(p-3)/2), for p an odd prime (A065091).",
				"This computation was motivated by a preprint of S. Mustonen, P. Haukkanen and J. K. Merikoski, called ``Polynomials associated with squared diagonals of regular polygons'', Nov 16 2013."
			],
			"formula": [
				"a(n,m) = [x^m] MPs2(n, x), n \u003e= 0, m = 0, 1, ...., deg(n),with the minimal polynomial MPs2(n, x) of s(n)^2 = (2*sin(Pi/n))^2. The degree is deg(n) = A023022(n).",
				"a(2*l,m) = A232631(l,m), l \u003e=1,  a(2*l+1,m) = A232832(l,m), l \u003e=0."
			],
			"example": [
				"The table a(n,m) begins:",
				"------------------------------------------------------------------",
				"n/m    0     1      2      3       4     5     6    7    8   9 ...",
				"1:     0     1",
				"2:    -4     1",
				"3:    -3     1",
				"4:    -2     1",
				"5:     5    -5      1",
				"6:    -1     1",
				"7:    -7    14     -7      1",
				"8:     2    -4      1",
				"9:    -3     9     -6      1",
				"10:    1    -3      1",
				"11:  -11    55    -77     44     -11     1",
				"12:    1    -4      1",
				"13:   13   -91    182   -156      65   -13     1",
				"14:   -1     6     -5      1",
				"15:    1    -8     14     -7       1",
				"16:    2   -16     20     -8       1",
				"17:   17  -204    714  -1122     935  -442   119  -17    1",
				"18:   -1     9     -6      1",
				"19:  -19   285  -1254   2508   -2717  1729  -665  152  -19   1",
				"20:    1   -12     19     -8       1",
				"...",
				"MPs2(7, x) =  product(x - 2*(1 + cos(Pi*(2*j+1)/7)), j=0..2) = (x - (2 + rho(7))*(x - (2 + (-1 - rho(7) - rho(7)^2))*(x - (2 + (2 - rho(7)^2 )))  = (-8+4*z-2*z^2-5*z^3+z^4+z^5) + (14-z+2*z^2+z^3-z^4)*x -7*x^2 +x^3 , with z = rho(7), and this becomes  due to C(7, z) = z^3 - z^2 - 2*z + 1, finally",
				"  MPs2(7, x) = -7 + 14*x - 7*x^2 + x^3.",
				"MPs2(14, x) = product(x - 2*(1 - cos(Pi*(2*j+1)/7)), j=0..2) = (x - (2 - rho(7))*(x - (2 - (-1 - rho(7) - rho(7)^2))*(x - (2 - (2 - rho(7)^2 ))) =  -1 + 6*x - 5*x^2 + x^3 (using again C(7, z) = 0 with z = rho(7))."
			],
			"mathematica": [
				"Flatten[ CoefficientList[ Table[ MinimalPolynomial[ (2*Sin[Pi/n])^2, x], {n, 1, 20}], x]] (adapted from Jean-François Alcover, A187360) - Wolfdieter Lang, Dec 24 2013"
			],
			"xref": [
				"Cf. A232631 (even n),  A232632 (odn), A023022 (degree), A187360."
			],
			"keyword": "sign,tabf,easy",
			"offset": "1,3",
			"author": "_Wolfdieter Lang_, Dec 19 2013",
			"references": 4,
			"revision": 10,
			"time": "2014-01-13T12:46:28-05:00",
			"created": "2013-12-23T14:44:08-05:00"
		}
	]
}