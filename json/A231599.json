{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A231599",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 231599,
			"data": "1,1,-1,1,-1,-1,1,1,-1,-1,0,1,1,-1,1,-1,-1,0,0,2,0,0,-1,-1,1,1,-1,-1,0,0,1,1,1,-1,-1,-1,0,0,1,1,-1,1,-1,-1,0,0,1,0,2,0,-1,-1,-1,-1,0,2,0,1,0,0,-1,-1,1,1,-1,-1,0,0,1,0,1,1,0,-1,-1,-2,0",
			"name": "T(n,k) is the coefficient of x^k in Product_{i=1..n} (1-x^i); triangle T(n,k), n \u003e= 0, 0 \u003c= k \u003c= A000217(n), read by rows.",
			"comment": [
				"From _Tilman Piesk_, Feb 21 2016: (Start)",
				"The sum of each row is 0. The even rows are symmetric; in the odd rows numbers with the same absolute value and opposed signum are symmetric to each other.",
				"The odd rows where n mod 4 = 3 have the central value 0.",
				"The even rows where n mod 4 = 0 have positive central values. They form the sequence A269298 and are also the rows maximal values.",
				"A086376 contains the maximal values of each row, A160089 the maximal absolute values, and A086394 the absolute parts of the minimal values.",
				"Rows of this triangle can be used to efficiently calculate values of A026807.",
				"(End)"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A231599/b231599.txt\"\u003eRows n = 0..40, flattened\u003c/a\u003e",
				"Dorin Andrica, Ovidiu Bagdasar, \u003ca href=\"http://hdl.handle.net/10545/623501\"\u003eOn some results concerning the polygonal polynomials\u003c/a\u003e, Carpathian Journal of Mathematics (2019) Vol. 35, No. 1, 1-11.",
				"Tilman Piesk, Rows n = 0..40 as \u003ca href=\"http://paste.watchduck.net/1602/A231599_left.html\"\u003eleft-aligned\u003c/a\u003e and \u003ca href=\"http://paste.watchduck.net/1602/A231599_centered.html\"\u003ecentered\u003c/a\u003e table"
			],
			"formula": [
				"T(n,k) = [x^k] Product_{i=1..n} (1-x^i).",
				"T(n,k) = T(n-1, k) + (-1)^n*T(n-1, n*(n+1)/2-k), n \u003e 1. - _Gevorg Hmayakyan_, Feb 09 2017 [corrected by _Giuliano Cabrele_, Mar 02 2018]"
			],
			"example": [
				"For n=2 the corresponding polynomial is (1-x)*(1-x^2) = 1 -x - x^2 + x^3.",
				"Irregular triangle starts:",
				"  k    0   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15",
				"n",
				"0      1",
				"1      1  -1",
				"2      1  -1  -1   1",
				"3      1  -1  -1   0   1   1  -1",
				"4      1  -1  -1   0   0   2   0   0  -1  -1   1",
				"5      1  -1  -1   0   0   1   1   1  -1  -1  -1   0   0   1   1  -1"
			],
			"maple": [
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=0..degree(p)))",
				"        (expand(mul(1-x^i, i=1..n))):",
				"seq(T(n), n=0..10);  # _Alois P. Heinz_, Dec 22 2013"
			],
			"mathematica": [
				"Table[If[k == 0, 1, Coefficient[Product[(1 - x^i), {i, n}], x^k]], {n, 0, 6}, {k, 0, (n^2 + n)/2}] // Flatten (* _Michael De Vlieger_, Mar 04 2018 *)"
			],
			"program": [
				"(PARI) row(n) = pol = prod(i=1, n, 1 - x^i); for (i=0, poldegree(pol), print1(polcoeff(pol, i), \", \")); \\\\ _Michel Marcus_, Dec 21 2013",
				"(Python)",
				"from sympy import poly, symbols",
				"def a231599_row(n):",
				"    if n == 0:",
				"        return [1]",
				"    x = symbols('x')",
				"    p = 1",
				"    for i in range(1, n+1):",
				"        p *= poly(1-x**i)",
				"    p = p.all_coeffs()",
				"    return p[::-1]",
				"# _Tilman Piesk_, Feb 21 2016"
			],
			"xref": [
				"Cf. A000217 (triangular numbers).",
				"Cf. A086376, A160089, A086394 (maxima, etc.).",
				"Cf. A269298 (central nonzero values)."
			],
			"keyword": "sign,look,tabf",
			"offset": "0,20",
			"author": "_Marc Bogaerts_, Nov 11 2013",
			"references": 14,
			"revision": 57,
			"time": "2019-03-29T17:56:30-04:00",
			"created": "2013-12-22T08:48:47-05:00"
		}
	]
}