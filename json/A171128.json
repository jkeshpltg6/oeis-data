{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A171128",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 171128,
			"data": "1,1,1,3,2,1,7,9,3,1,19,28,18,4,1,51,95,70,30,5,1,141,306,285,140,45,6,1,393,987,1071,665,245,63,7,1,1107,3144,3948,2856,1330,392,84,8,1,3139,9963,14148,11844,6426,2394,588,108,9,1",
			"name": "A117852*A130595 as lower triangular matrices.",
			"comment": [
				"Mirror image of triangle in A135091.",
				"Exponential Riordan array [exp(x)*Bessel_I(0,2*x), x] = A007318 * A109187. - _Peter Bala_, Feb 12 2017"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A171128/b171128.txt\"\u003eTable of n, a(n) for the first 100 rows, flattened\u003c/a\u003e"
			],
			"formula": [
				"Sum_{k=0..n} T(n,k)*x^k = A126869(n), A002426(n), A000984(n), A026375(n), A081671(n), A098409(n), A098410(n), A104454(n) for x = -1,0,1,2,3,4,5,6 respectively.",
				"T(n,k) = binomial(n,k)*A002426(n-k). - _Philippe Deléham_, Dec 12 2009",
				"From _Peter Bala_, Feb 12 2017: (Start)",
				"T(n,k) = Sum_{j = 0..floor((n-k)/2)} n!/((n-k-2*j)!*j!^2*k!).",
				"T(n,k) = n/k*T(n-1,k-1) with T(n,0) = A002426(n).",
				"(n - k)^2*T(n,k) = n*(2*n - 2*k - 1)*T(n-1,k) + 3*n*(n - 1)*T(n-2,k).",
				"O.g.f. = 1/sqrt((1 - (1 + t)*z)^2 - 4*z^2) = 1 + (1 + t)*z + (3 + 2*t + t^2)*z^2 + (7 + 9*t + 3*t^2 + t^3 )*z^3 + ....",
				"E.g.f. Bessel_I(0,2*x) * exp((1 + t)*x) = 1 + (1 + t)*z + (3 + 2*t + t^2)*z^2/2! + (7 + 9*t + 3*t^2 + t^3 )*z^3/3! + ....",
				"n-th row polynomial R(n,t) = Sum_{k = 0..floor(n/2)} binomial(n,2*k)*binomial(2*k,k)*(1 + t)^(n-2*k) = coefficient of x^n in the expansion of (1 + (1 + t)*x + x^2)^n.",
				"The polynomials R(n, t - 1) are the row polynomials of A109187.",
				"d/dt(R(n,t)) = n*R(n-1,t).",
				"Moment representation on a finite interval: R(n,t) = 1/Pi * Integral_{x = t-1 .. t+3} x^n/sqrt((t + 3 - x)*(x - t + 1)) dx.",
				"The zeros of the row polynomials appear to lie on the vertical line Re(z) = -1 in the complex plane, and the zeros of R(n,t) and R(n+1,t) appear to interlace along this line.",
				"(End)"
			],
			"example": [
				"Triangle begins:",
				"   1",
				"   1  1",
				"   3  2  1",
				"   7  9  3 1",
				"  19 28 18 4 1",
				"  ...",
				"From _Peter Bala_, Feb 12 2017: (Start)",
				"The infinitesimal generator begins",
				"      0",
				"      1    0",
				"      2    2     0",
				"      0    6     3     0",
				"     -6    0    12     4     0",
				"      0  -30     0    20     5   0",
				"     80    0   -90     0    30   6   0",
				"      0  560     0  -210     0  42   7  0",
				"  -2310    0  2240     0  -420   0  56  8  0",
				"  ....",
				"and equals the generalized exponential Riordan array [x + log(Bessel_I(0,2*x), x], and so has integer entries. (End)"
			],
			"mathematica": [
				"A002426[n_] := Sum[Binomial[n, 2*k]*Binomial[2*k, k], {k, 0, Floor[n/2]}]; Table[ Binomial[n, k]*A002426[n - k], {n, 0, 99}, {k, 0, n} ]//Flatten (* G_. C. Greubel_, Mar 07 2017 *)"
			],
			"xref": [
				"A000984 (row sums), A135091 (row reversed). Cf. A002426, A117852, A130595, A109187."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_Philippe Deléham_, Dec 04 2009",
			"references": 3,
			"revision": 13,
			"time": "2017-03-08T02:27:16-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}