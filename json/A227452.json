{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A227452",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 227452,
			"data": "0,1,5,7,6,18,61,8,11,58,28,25,77,246,66,55,36,237,226,35,46,116,197,115,102,306,985,265,445,200,155,946,905,285,220,145,475,786,925,140,185,465,395,826,460,409,1229,3942,1062,1782,1602,823,612,3789,3622,1142",
			"name": "Irregular table where each row lists the partitions occurring on the main trunk of the Bulgarian Solitaire game tree (from the top to the root) for deck of n(n+1)/2 cards. Nonordered partitions are encoded in the runlengths of binary expansion of each term, in the manner explained in A129594.",
			"comment": [
				"The terms for row n are computed as A227451(n), A226062(A227451(n)), A226062(A226062(A227451(n))), etc. until a term that is a fixed point of A226062 is reached (A037481(n)), which will be the last term of row n.",
				"Row n has A002061(n) = 1,1,3,7,13,21,... terms."
			],
			"reference": [
				"Martin Gardner, Colossal Book of Mathematics, Chapter 34, Bulgarian Solitaire and Other Seemingly Endless Tasks, pp. 455-467, W. W. Norton \u0026 Company, 2001."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A227452/b227452.txt\"\u003eRows 0-31 of table, flattened\u003c/a\u003e"
			],
			"formula": [
				"For n \u003c 2, a(n) = n, and for n\u003e=2, if A226062(a(n-1)) = a(n-1) [in other words, when a(n-1) is one of the terms of A037481] then a(n) = A227451(A227177(n+1)), otherwise a(n) = A226062(a(n-1)).",
				"Alternatively, a(n) = value of the A227179(n)-th iteration of the function A226062, starting from the initial value A227451(A227177(n)). [See the other Scheme-definition in the Program section]"
			],
			"example": [
				"Rows 0 - 5 of the table are:",
				"0",
				"1",
				"5, 7, 6",
				"18, 61, 8, 11, 58, 28, 25",
				"77, 246, 66, 55, 36, 237, 226, 35, 46, 116, 197, 115, 102",
				"306, 985, 265, 445, 200, 155, 946, 905, 285, 220, 145, 475, 786, 925, 140, 185, 465, 395, 826, 460, 409"
			],
			"program": [
				"(Scheme);; with _Antti Karttunen_'s IntSeq-library for memoizing definec-macro",
				";; Compare with the other definition for A218616:",
				"(definec (A227452 n) (cond ((\u003c n 2) n) ((A226062 (A227452 (- n 1))) =\u003e (lambda (next) (if (= next (A227452 (- n 1))) (A227451 (A227177 (+ 1 n))) next)))))",
				";; Alternative implementation using nested cached closures for function iteration:",
				"(define (A227452 n) ((compose-A226062-to-n-th-power (A227179 n)) (A227451 (A227177 n))))",
				"(definec (compose-A226062-to-n-th-power n) (cond ((zero? n) (lambda (x) x)) (else (lambda (x) (A226062 ((compose-A226062-to-n-th-power (- n 1)) x))))))"
			],
			"xref": [
				"Left edge A227451. Right edge: A037481. Cf. A227147 (can be computed from this sequence)."
			],
			"keyword": "nonn,base,tabf",
			"offset": "0,3",
			"author": "_Antti Karttunen_, Jul 12 2013",
			"references": 5,
			"revision": 26,
			"time": "2017-09-09T19:35:19-04:00",
			"created": "2013-07-21T03:03:36-04:00"
		}
	]
}