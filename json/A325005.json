{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A325005",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 325005,
			"data": "1,3,1,6,6,1,10,21,10,1,15,55,56,15,1,21,120,220,126,21,1,28,231,680,715,252,28,1,36,406,1771,3060,2002,462,36,1,45,666,4060,10626,11628,5005,792,45,1,55,1035,8436,31465,53130,38760,11440,1287,55,1",
			"name": "Array read by descending antidiagonals: A(n,k) is the number of unoriented colorings of the facets of a regular n-dimensional orthotope using up to k colors.",
			"comment": [
				"Also called hypercube, n-dimensional cube, and measure polytope. For n=1, the figure is a line segment with two vertices. For n=2 the figure is a square with four edges. For n=3 the figure is a cube with six square faces. For n=4, the figure is a tesseract with eight cubic facets. The Schläfli symbol, {4,3,...,3}, of the regular n-dimensional orthotope (n\u003e1) consists of a four followed by n-2 threes. Each of its 2n facets is an (n-1)-dimensional orthotope. Two unoriented colorings are the same if congruent; chiral pairs are counted as one.",
				"Also the number of unoriented colorings of the vertices of a regular n-dimensional orthoplex using up to k colors."
			],
			"link": [
				"Robert A. Russell, \u003ca href=\"/A325005/b325005.txt\"\u003eTable of n, a(n) for n = 1..325\u003c/a\u003e",
				"Robin Chapman, answer to \u003ca href=\"https://math.stackexchange.com/q/5732/\"\u003eColoring the faces of a hypercube\u003c/a\u003e, Math StackExchange, September 30, 2010."
			],
			"formula": [
				"A(n,k) = binomial(n + binomial(k+1,2) - 1, n).",
				"A(n,k) = Sum_{j=1..2n} A325009(n,j) * binomial(k,j).",
				"A(n,k) = A325004(n,k) - A325006(n,k) = (A325004(n,k) + A325007(n,k)) / 2 = A325006(n,k) + A325007(n,k).",
				"G.f. for row n: Sum_{j=1..2n} A325009(n,j) * x^j / (1-x)^(j+1).",
				"Linear recurrence for row n: T(n,k) = Sum_{j=0..2n} binomial(-2-j,2n-j) * T(n,k-1-j).",
				"G.f. for column k: 1/(1-x)^binomial(k+1,2) - 1."
			],
			"example": [
				"Array begins with A(1,1):",
				"1  3    6    10     15      21       28        36        45         55 ...",
				"1  6   21    55    120     231      406       666      1035       1540 ...",
				"1 10   56   220    680    1771     4060      8436     16215      29260 ...",
				"1 15  126   715   3060   10626    31465     82251    194580     424270 ...",
				"1 21  252  2002  11628   53130   201376    658008   1906884    5006386 ...",
				"1 28  462  5005  38760  230230  1107568   4496388  15890700   50063860 ...",
				"1 36  792 11440 116280  888030  5379616  26978328 115775100  436270780 ...",
				"1 45 1287 24310 319770 3108105 23535820 145008513 752538150 3381098545 ...",
				"For A(1,2) = 3, the two achiral colorings use just one of the two colors for both vertices; the chiral pair uses one color for each vertex."
			],
			"mathematica": [
				"Table[Binomial[Binomial[d-n+2,2]+n-1,n],{d,1,11},{n,1,d}] // Flatten"
			],
			"xref": [
				"Cf. A325004 (oriented), A325006 (chiral), A325007 (achiral), A325009 (exactly k colors).",
				"Other n-dimensional polytopes: A325000 (simplex), A325013 (orthoplex).",
				"Rows 1-3 are A000217, A002817, A198833."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "1,2",
			"author": "_Robert A. Russell_, Mar 23 2019",
			"references": 11,
			"revision": 16,
			"time": "2019-08-06T01:13:11-04:00",
			"created": "2019-05-25T11:43:21-04:00"
		}
	]
}