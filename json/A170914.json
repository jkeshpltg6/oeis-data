{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A170914",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 170914,
			"data": "1,0,-1,1,-19,19,-659,37,-7675,40043,-3578279,1123009,-95259767,7091713,-85215100151,832857559,-4180679675171,63804880881241,-6399968826052559,5697831990097981,-478887035449041839,252737248941887573,-1123931378903214542099,35703551772944759",
			"name": "Write 1 + sin x = Product_{n\u003e=1} (1 + g_n * x^n); a(n) = numerator(g_n).",
			"comment": [
				"From _Petros Hadjicostas_, Oct 06 2019: (Start)",
				"The recurrence about (A(m,n): m,n \u003e= 1) in the Formula section follows from Theorem 3 in Gingold et al. (1988); see also Gingold and Knopfmacher (1995, p. 1222). A(m=1,n) equals the n-th coefficient of the Taylor expansion of 1 + sin(x).",
				"If 1 + sin(x) = 1/Product_{n\u003e=1} (1 + f_n * x^n) (inverse power product expansion), then Gingold and Knopfmacher (1995) and Alkauskas (2008, 2009) proved that f_n = -g_n for n odd, and Sum_{s|n} (-g_{n/s})^s/s = -Sum_{s|n} (-f_{n/s})^s/s. [We caution that different authors may use -g_n for g_n, or -f_n for f_n, or both.] We have A328191(n) = numerator(f_n) and A328186(n) = denominator(f_n).",
				"_Wolfdieter Lang_ (see the link below) examined inverse power product expansions both for ordinary g.f.'s and for exponential g.f.'s.",
				"In all cases, we assume the g.f.'s are unital, i.e., the g.f.'s start with a constant 1.",
				"(End)"
			],
			"link": [
				"Giedrius Alkauskas, \u003ca href=\"http://arxiv.org/abs/0801.0805\"\u003eOne curious proof of Fermat's little theorem\u003c/a\u003e, arXiv:0801.0805 [math.NT], 2008.",
				"Giedrius Alkauskas, \u003ca href=\"https://www.jstor.org/stable/40391097\"\u003eA curious proof of Fermat's little theorem\u003c/a\u003e, Amer. Math. Monthly 116(4) (2009), 362-364.",
				"H. Gingold, H. W. Gould, and Michael E. Mays, \u003ca href=\"https://www.researchgate.net/publication/268023169_Power_product_expansions\"\u003ePower Product Expansions\u003c/a\u003e, Utilitas Mathematica 34 (1988), 143-161.",
				"H. Gingold and A. Knopfmacher, \u003ca href=\"http://dx.doi.org/10.4153/CJM-1995-062-9\"\u003eAnalytic properties of power product expansions\u003c/a\u003e, Canad. J. Math. 47 (1995), 1219-1239.",
				"W. Lang, \u003ca href=\"/A157162/a157162.txt\"\u003eRecurrences for the general problem\u003c/a\u003e, 2009."
			],
			"formula": [
				"From _Petros Hadjicostas_, Oct 07 2019: (Start)",
				"a(2*n+1) = -A328191(2*n+1) for n \u003e= 0.",
				"Define (A(m,n): n,m \u003e= 1) by A(m=1,2*n+1) = (-1)^n/(2*n+1)! for n \u003e= 0, A(m=1,2*n) = 0 for n \u003e= 1, A(m,n) = 0 for m \u003e n \u003e= 1 (upper triangular), and A(m,n) = A(m-1,n) - A(m-1,m-1) * A(m,n-m+1) for n \u003e= m \u003e= 2. Then g_n = A(n,n). (End)"
			],
			"example": [
				"g_n = 1, 0, -1/6, 1/6, -19/120, 19/120, -659/5040, 37/280, -7675/72576, ..."
			],
			"maple": [
				"# Calculates the fractions g_n (choose L much larger than M):",
				"PPE_sin := proc(L, M)",
				"local t1, t0, g, t2, n, t3;",
				"if L \u003c 2.5*M then print(\"Choose larger value for L\");",
				"else",
				"t1 := 1 + sin(x);",
				"t0 := series(t1, x, L);",
				"g := []; t2 := t0;",
				"for n to M do",
				"t3 := coeff(t2, x, n);",
				"t2 := series(t2/(1 + t3*x^n), x, L);",
				"g := [op(g), t3];",
				"end do;",
				"end if;",
				"[seq(g[n], n = 1 .. nops(g))];",
				"end proc;",
				"# Calculates the numerators of g_n:",
				"h1 := map(numer, PPE_sin(100, 40)); # _Petros Hadjicostas_, Oct 06 2019 by modifying _N. J. A. Sloane_'s program from A170912 and A170913."
			],
			"mathematica": [
				"A[m_, n_] :=",
				"  A[m, n] =",
				"   Which[m == 1, (1-(-1)^n)*(-1)^Floor[(n-1)/2]/(2*n!), m \u003e n \u003e= 1, 0, True,",
				"    A[m - 1, n] - A[m - 1, m - 1]*A[m, n - m + 1]];",
				"a[n_] := Numerator[A[n, n]];",
				"a /@ Range[1, 55] (* _Petros Hadjicostas_, Oct 06 2019, courtesy of _Jean-François Alcover_ *)"
			],
			"xref": [
				"Cf. Denominators are in A170915.",
				"Cf. A170910, A170911, A170912, A170913, A328186, A328191."
			],
			"keyword": "sign,frac",
			"offset": "1,5",
			"author": "_N. J. A. Sloane_, Jan 30 2010",
			"references": 6,
			"revision": 56,
			"time": "2019-10-09T02:52:16-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}