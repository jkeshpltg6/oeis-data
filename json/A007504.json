{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007504",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7504,
			"id": "M1370",
			"data": "0,2,5,10,17,28,41,58,77,100,129,160,197,238,281,328,381,440,501,568,639,712,791,874,963,1060,1161,1264,1371,1480,1593,1720,1851,1988,2127,2276,2427,2584,2747,2914,3087,3266,3447,3638,3831,4028,4227,4438,4661,4888",
			"name": "Sum of the first n primes.",
			"comment": [
				"a(A051838(n)) = A002110(A051838(n)) / A116536(n). - _Reinhard Zumkeller_, Oct 03 2011",
				"It appears that a(n)^2 - a(n-1)^2 = A034960(n). - _Gary Detlefs_, Dec 20 2011",
				"This is true. Proof: By definition we have A034960(n) = Sum_{k = (a(n-1)+1)..a(n)} (2*k-1). Since Sum_{k = 1..n} (2*k-1) = n^2, it follows A034960(n) = a(n)^2 - a(n-1)^2, for n \u003e 1. - _Hieronymus Fischer_, Sep 27 2012 [formulas above adjusted to changed offset of A034960 - _Hieronymus Fischer_, Oct 14 2012]",
				"Row sums of the triangle in A037126. - _Reinhard Zumkeller_, Oct 01 2012",
				"Ramanujan noticed the apparent identity between the prime parts partition numbers A000607 and the expansion of Sum_{k \u003e= 0} x^a(k)/((1-x)...(1-x^k)), cf. A046676. See A192541 for the difference between the two. - _M. F. Hasler_, Mar 05 2014",
				"For n \u003e 0: row 1 in A254858. - _Reinhard Zumkeller_, Feb 08 2015",
				"a(n) is the smallest number that can be partitioned into n distinct primes. - _Alonso del Arte_, May 30 2017",
				"For a(n) \u003c m \u003c a(n+1), n \u003e 0, at least one m is a perfect square.",
				"Proof: For n = 1, 2, ..., 6, the proposition is clear. For n \u003e 6, a(n) \u003c ((prime(n) - 1)/2)^2, set (k - 1)^2 \u003c= a(n) \u003c k^2 \u003c ((prime(n) + 1)/2)^2, then k^2 \u003c (k - 1)^2 + prime(n) \u003c= a(n) + prime(n) = a(n+1), so m = k^2 is this perfect square. - _Jinyuan Wang_, Oct 04 2018"
			],
			"reference": [
				"E. Bach and J. Shallit, §2.7 in Algorithmic Number Theory, Vol. 1: Efficient Algorithms, MIT Press, Cambridge, MA, 1996.",
				"H. L. Nelson, \"Prime Sums\", J. Rec. Math., 14 (1981), 205-206.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"R. J. Mathar, \u003ca href=\"/A007504/b007504.txt\"\u003eTable of n, a(n) for n = 0..100000\u003c/a\u003e",
				"C. Axler, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL18/Axler/axler6.html\"\u003eOn a Sequence involving Prime Numbers\u003c/a\u003e, J. Int. Seq. 18 (2015) # 15.7.6",
				"Christian Axler, \u003ca href=\"http://arxiv.org/abs/1606.06874\"\u003eNew bounds for the sum of the first n prime numbers\u003c/a\u003e, arXiv:1606.06874 [math.NT], 2016.",
				"P. Hecht, \u003ca href=\"https://dx.doi.org/10.22161/ijaers.4.6.10\"\u003ePost-Quantum Cryptography: S_381 Cyclic Subgroup of High Order\u003c/a\u003e, International Journal of Advanced Engineering Research and Science (IJAERS, 2017) Vol. 4, Issue 6, 78-86.",
				"R. J. Mathar, \u003ca href=\"/A007504/a007504.txt\"\u003eTable of 100000n, a(100000n) for n = 1..10000\u003c/a\u003e",
				"Romeo Meštrović, \u003ca href=\"https://arxiv.org/abs/1804.04198\"\u003eCurious conjectures on the distribution of primes among the sums of the first 2n primes\u003c/a\u003e, arXiv:1804.04198 [math.NT], 2018.",
				"Vladimir Shevelev, \u003ca href=\"http://list.seqfan.eu/pipermail/seqfan/2013-August/011512.html\"\u003eAsymptotics of sum of the first n primes with a remainder term\u003c/a\u003e",
				"Nilotpal Kanti Sinha, \u003ca href=\"http://arxiv.org/abs/1011.1667\"\u003eOn the asymptotic expansion of the sum of the first n primes\u003c/a\u003e, arXiv:1011.1667 [math.NT], 2010-2015.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PrimeSums.html\"\u003ePrime Sums\u003c/a\u003e",
				"OEIS Wiki, \u003ca href=\"https://oeis.org/wiki/Sums_of_primes_divisibility_sequences\"\u003eSums of powers of primes divisibility sequences\u003c/a\u003e"
			],
			"formula": [
				"a(n) has the asymptotic expression a(n) ~ n^2 * log(n) / 2. - Ahmed Fares (ahmedfares(AT)my-deja.com), Apr 24 2001 (see Bach \u0026 Shallit 1996)",
				"a(n) = A014284(n+1) - 1. - _Jaroslav Krizek_, Aug 19 2009",
				"a(n+1) - a(n) = A000040(n+1). - _Jaroslav Krizek_, Aug 19 2009",
				"a(n) = min(A068873(n), A073619(n)) for n \u003e 1. - _Jonathan Sondow_, Jul 10 2012",
				"a(n) = A033286(n) - A152535(n). - _Omar E. Pol_, Aug 09 2012",
				"For n \u003e= 3, a(n) \u003e= (n-1)^2 * (log(n-1) - 1/2)/2 and a(n) \u003c= n*(n+1)*(log(n) + log(log(n))+ 1)/2. Thus a(n) = n^2 * log(n) / 2 + O(n^2*log(log(n))). It is more precise than in Fares's comment. - _Vladimir Shevelev_, Aug 01 2013",
				"a(n) = (n^2/2)*(log n + log log n - 3/2 + (log log n - 3)/log n + (2 (log log n)^2 - 14 log log n + 27)/(4 log^2 n) + O((log log n/log n)^3)) [Sinha]. - _Charles R Greathouse IV_, Jun 11 2015",
				"G.f: (x*b(x))/(1-x), where b(x) is the g.f. of A000040. - _Mario C. Enriquez_, Dec 10 2016",
				"a(n) = A008472(A002110(n)), for n\u003e0. - _Michel Marcus_, Jul 16 2020"
			],
			"maple": [
				"s1:=[2]; for n from 2 to 1000 do s1:=[op(s1),s1[n-1]+ithprime(n)]; od: s1;",
				"A007504 := proc(n)",
				"    add(ithprime(i), i=1..n) ;",
				"end proc: # _R. J. Mathar_, Sep 20 2015"
			],
			"mathematica": [
				"Accumulate[Prime[Range[100]]] (* _Zak Seidov_, Apr 10 2011 *)",
				"primeRunSum = 0; Table[primeRunSum = primeRunSum + Prime[k], {k, 100}] (* _Zak Seidov_, Apr 16 2011 *)"
			],
			"program": [
				"(PARI) A007504(n) = sum(k=1,n,prime(k)) \\\\ _Michael B. Porter_, Feb 26 2010",
				"(PARI) a(n) = vecsum(primes(n)); \\\\ _Michel Marcus_, Feb 06 2021",
				"(MAGMA) [0] cat [\u0026+[ NthPrime(k): k in [1..n]]: n in [1..50]];  // _Bruno Berselli_, Apr 11 2011 (adapted by _Vincenzo Librandi_, Nov 27 2015 - after Hasler's change on Mar 05 2014)",
				"(Haskell)",
				"a007504 n = a007504_list !! n",
				"a007504_list = scanl (+) 0 a000040_list",
				"-- _Reinhard Zumkeller_, Oct 01 2014, Oct 03 2011",
				"(GAP) P:=Filtered([1..250],IsPrime);;",
				"a:=Concatenation([0],List([1..Length(P)],i-\u003eSum([1..i],k-\u003eP[k]))); # _Muniru A Asiru_, Oct 07 2018"
			],
			"xref": [
				"Cf. A000041, A034386, A111287, A013916, A013918 (primes), A045345, A050247, A050248, A068873, A073619, A034387, A014148, A014150, A178138, A254784, A254858.",
				"See A122989 for the value of Sum_{n \u003e= 1} 1/a(n).",
				"Cf. A008472, A002110."
			],
			"keyword": "nonn,nice",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, _Robert G. Wilson v_",
			"ext": [
				"More terms from _Stefan Steinerberger_, Apr 11 2006",
				"a(0) = 0 prepended by _M. F. Hasler_, Mar 05 2014"
			],
			"references": 448,
			"revision": 185,
			"time": "2021-04-02T16:33:29-04:00",
			"created": "1994-09-19T03:00:00-04:00"
		}
	]
}