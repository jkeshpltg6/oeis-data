{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001693",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1693,
			"id": "M4373 N1838",
			"data": "1,7,21,112,588,3360,19544,117648,720300,4483696,28245840,179756976,1153430600,7453000800,48444446376,316504099520,2077057800300,13684147881600,90467419857752,599941851861744",
			"name": "Number of degree-n irreducible polynomials over GF(7); dimensions of free Lie algebras.",
			"comment": [
				"Number of aperiodic necklaces with n beads of 7 colors. - _Herbert Kociemba_, Nov 25 2016"
			],
			"reference": [
				"E. R. Berlekamp, Algebraic Coding Theory, McGraw-Hill, NY, 1968, p. 84.",
				"M. Lothaire, Combinatorics on Words. Addison-Wesley, Reading, MA, 1983, p. 79.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A001693/b001693.txt\"\u003eTable of n, a(n) for n = 0..1186\u003c/a\u003e (terms 0..200 from T. D. Noe)",
				"Y. Puri and T. Ward, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL4/WARD/short.html\"\u003eArithmetic and growth of periodic orbits\u003c/a\u003e, J. Integer Seqs., Vol. 4 (2001), #01.2.1.",
				"G. J. Simmons, \u003ca href=\"http://www.jstor.org/stable/2316211\"\u003eThe number of irreducible polynomials of degree n over GF(p)\u003c/a\u003e, Amer. Math. Monthly, 77 (1970), 743-745.",
				"G. Viennot, \u003ca href=\"http://dx.doi.org/10.1007/BFb0067950\"\u003eAlgèbres de Lie Libres et Monoïdes Libres\u003c/a\u003e, Lecture Notes in Mathematics 691, Springer Verlag 1978.",
				"\u003ca href=\"/index/Lu#Lyndon\"\u003eIndex entries for sequences related to Lyndon words\u003c/a\u003e"
			],
			"formula": [
				"a(n) = (1/n)*Sum_{d|n} mu(d)*7^(n/d), for n\u003e0.",
				"G.f.: k=7, 1 - Sum_{i\u003e=1} mu(i)*log(1 - k*x^i)/i. - _Herbert Kociemba_, Nov 25 2016"
			],
			"maple": [
				"with(numtheory); A001693 := proc(n) local d,s; if n = 0 then RETURN(1); else s := 0; for d in divisors(n) do s := s+mobius(d)*7^(n/d); od; RETURN(s/n); fi; end;"
			],
			"mathematica": [
				"a[n_]:=(1/n)*Sum[MoebiusMu[d]*7^(n/d), {d, Divisors[n]}]; a[0] = 1; Table[a[n], {n, 0, 19}] (* _Jean-François Alcover_, Aug 31 2011, after formula *)",
				"mx=40;f[x_,k_]:=1-Sum[MoebiusMu[i] Log[1-k*x^i]/i,{i,1,mx}];CoefficientList[Series[f[x,7],{x,0,mx}],x] (* _Herbert Kociemba_, Nov 25 2016 *)"
			],
			"program": [
				"(PARI) a(n) = if(n, sumdiv(n, d, moebius(d)*7^(n/d))/n, 1) \\\\ _Altug Alkan_, Dec 01 2015"
			],
			"xref": [
				"Column 7 of A074650.",
				"Cf. A027376, A000031, A001037, A032164."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Description corrected by _Vladeta Jovovic_, Feb 09 2001"
			],
			"references": 5,
			"revision": 52,
			"time": "2017-11-22T16:41:10-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}