{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A109064",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 109064,
			"data": "1,-5,5,10,-15,-5,-10,30,25,-35,5,-60,30,60,-30,10,-55,80,35,-100,-15,-60,60,110,-50,-5,-60,100,90,-150,-10,-160,105,120,-80,30,-105,180,100,-120,25,-210,60,210,-180,-35,-110,230,110,-215,5,-160,180,260",
			"name": "Expansion of eta(q)^5 / eta(q^5) in powers of q.",
			"comment": [
				"Number 12 of the 74 eta-quotients listed in Table I of Martin (1996)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A109064/b109064.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"W. Duke, \u003ca href=\"http://dx.doi.org/10.1090/S0273-0979-05-01047-5\"\u003eContinued fractions and modular functions\u003c/a\u003e, Bull. Amer. Math. Soc. 42 (2005), 137-162. See page 151.",
				"Y. Martin, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9947-96-01743-6\"\u003eMultiplicative eta-quotients\u003c/a\u003e, Trans. Amer. Math. Soc. 348 (1996), no. 12, 4825-4856, see page 4852 Table I.",
				"Michael Somos, \u003ca href=\"/A030203/a030203.txt\"\u003eIndex to Yves Martin's list of 74 multiplicative eta-quotients and their A-numbers\u003c/a\u003e",
				"G. N. Watson, \u003ca href=\"http://gdz.sub.uni-goettingen.de/dms/load/img/?PID=GDZPPN002174499\u0026amp;physid=PHYS_0110\"\u003eRamanujans Vermutung über Zerfällungsanzahlen\u003c/a\u003e, J. Reine Angew. Math. (Crelle), 179 (1938), 97-128. See the expression B^5/C in the notation of p. 106. [Added by _N. J. A. Sloane_, Nov 13 2009]"
			],
			"formula": [
				"Euler transform of period 5 sequence [ -5, -5, -5, -5, -4, ...].",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2), A(x^4)) where f(u, v, w) = v^3 + 2 * u*v*w + u^2*w - 4 * u*w^2.",
				"a(n) = -5 * b(n) where b() is multiplicative with a(0) = 1, b(p^e) = 1 if p=5,  b(p^e) = b(p) * b(p^(e-1)) - Kronecker(5, p) * p * b(p^(e-2)) otherwise. - _Michael Somos_, May 19 2015",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (5 t)) = 5^(5/2) (t/i)^2 g(t) where q = exp(2 Pi i t) and g() is the g.f. for A053723. - _Michael Somos_, May 19 2015",
				"G.f.: Product_{k\u003e0} (1 - x^k)^5 / (1 - x^(5*k)).",
				"a(n) = -5 * A109091(n), unless n=0. a(n) = (-1)^n * A138506(n). a(5*n) = a(n).",
				"a(0) = 1, a(n) = -(5/n)*Sum_{k=1..n} A285896(k)*a(n-k) for n \u003e 0. - _Seiichi Manyama_, Apr 29 2017"
			],
			"example": [
				"G.f. = 1 - 5*q + 5*q^2 + 10*q^3 - 15*q^4 - 5*q^5 - 10*q^6 + 30*q^7 + 25*q^8 + ..."
			],
			"maple": [
				"with(numtheory):",
				"a:= proc(n) option remember; `if`(n=0, 1, add(add(d*",
				"      `if`(irem(d, 5)=0, -4, -5), d=divisors(j))*a(n-j), j=1..n)/n)",
				"    end:",
				"seq(a(n), n=0..70);  # _Alois P. Heinz_, Jan 07 2017"
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ q]^5 / QPochhammer[ q^5], {q, 0, n}]; (* _Michael Somos_, May 19 2015 *)",
				"a[ n_] := If[ n \u003c 1, Boole[n == 0], -5 DivisorSum[ n, # KroneckerSymbol[ 5, #] \u0026]]; (* _Michael Somos_, May 19 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A)^5 / eta(x^5 + A), n))};",
				"(PARI)  {a(n) = if( n\u003c1, n==0, -5 * sumdiv(n, d, d * kronecker(5, d)))}; /* _Michael Somos_, May 19 2015 */",
				"(MAGMA) A := Basis( ModularForms( Gamma1(5), 2), 54); A[1] - 5*A[2] + 5*A[3]; /* _Michael Somos_, May 19 2015 */"
			],
			"xref": [
				"Cf. A053723, A109091, A138506, A277212, A285896."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Jun 17 2005",
			"references": 12,
			"revision": 37,
			"time": "2018-11-22T21:50:11-05:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}