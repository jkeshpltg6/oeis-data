{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A231664",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 231664,
			"data": "0,1,3,6,7,9,12,16,18,21,25,30,33,37,42,48,49,51,54,58,60,63,67,72,75,79,84,90,94,99,105,112,114,117,121,126,129,133,138,144,148,153,159,166,171,177,184,192,195,199,204,210,214,219,225,232,237,243,250,258,264,271,279,288,289,291,294,298,300,303,307,312,315,319,324,330,334,339,345,352,354,357",
			"name": "a(n) = Sum_{i=0..n} digsum_4(i), where digsum_4(i) = A053737(i).",
			"reference": [
				"Jean-Paul Allouche and Jeffrey Shallit, Automatic sequences, Cambridge University Press, 2003, p. 94."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A231664/b231664.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Jean Coquet, \u003ca href=\"https://doi.org/10.1016/0022-314X(86)90067-3\"\u003ePower sums of digital sums\u003c/a\u003e, J. Number Theory, Vol. 22, No. 2 (1986), pp. 161-176.",
				"P. J. Grabner, P. Kirschenhofer, H. Prodinger and R. F. Tichy, \u003ca href=\"http://math.sun.ac.za/~hproding/abstract/abs_80.htm\"\u003eOn the moments of the sum-of-digits function\u003c/a\u003e, \u003ca href=\"http://math.sun.ac.za/~hproding/pdffiles/st_andrews.pdf\"\u003ePDF\u003c/a\u003e, Applications of Fibonacci numbers, Vol. 5 (St. Andrews, 1992), pp. 263-271, Kluwer Acad. Publ., Dordrecht, 1993.",
				"Hsien-Kuei Hwang, Svante Janson and Tsung-Hsi Tsai, \u003ca href=\"https://doi.org/10.1145/3127585\"\u003eExact and Asymptotic Solutions of a Divide-and-Conquer Recurrence Dividing at Half: Theory and Applications\u003c/a\u003e, ACM Transactions on Algorithms, Vol. 13, No. 4 (2017), Article #47; \u003ca href=\"https://www.researchgate.net/profile/Hsien-Kuei-Hwang/publication/320642171_Exact_and_Asymptotic_Solutions_of_a_Divide-and-Conquer_Recurrence_Dividing_at_Half_Theory_and_Applications/links/59f9a5be0f7e9b553ec0eaad\"\u003eResearchGate link\u003c/a\u003e; \u003ca href=\"http://140.109.74.92/hk/wp-content/files/2016/12/aat-hhrr-1.pdf\"\u003epreprint\u003c/a\u003e, 2016.",
				"J.-L. Mauclaire and Leo Murata, \u003ca href=\"https://dx.doi.org/10.3792/pjaa.59.274\"\u003eOn q-additive functions. I\u003c/a\u003e, Proc. Japan Acad. Ser. A Math. Sci., Vol. 59, No. 6 (1983), pp. 274-276.",
				"J.-L. Mauclaire and Leo Murata, \u003ca href=\"https://dx.doi.org/10.3792/pjaa.59.441\"\u003eOn q-additive functions. II\u003c/a\u003e, Proc. Japan Acad. Ser. A Math. Sci., Vol. 59, No. 9 (1983), pp. 441-444.",
				"J. R. Trollope, \u003ca href=\"http://www.jstor.org/stable/2687954\"\u003eAn explicit expression for binary digital sums\u003c/a\u003e, Math. Mag., Vol. 41, No. 1 (1968), pp. 21-25."
			],
			"formula": [
				"G.f.: g(x) satisfies g(x) = (1+x+x^2+x^3)^2*g(x^4) + (x+2*x^2+3*x^3)/(1-x-x^4+x^5). - _Robert Israel_, Sep 20 2017",
				"a(n) ~ 3*n*log(n)/(4*log(2)). - _Amiram Eldar_, Dec 09 2021"
			],
			"maple": [
				"ListTools:-PartialSums([seq(convert(convert(n,base,4),`+`), n=0..200)]); # _Robert Israel_, Sep 20 2017"
			],
			"mathematica": [
				"Table[Sum[Total[IntegerDigits[j, 4]], {j,0,n}], {n, 0, 100}] (* _G. C. Greubel_, Feb 16 2019 *)"
			],
			"program": [
				"(PARI) a(n) = sum(i=0, n, sumdigits(i, 4)); \\\\ _Michel Marcus_, Sep 20 2017",
				"(MAGMA) [(\u0026+[\u0026+Intseq(j, 4): j in [0..n]]): n in [0..100]]; // _G. C. Greubel_, Feb 16 2019"
			],
			"xref": [
				"Cf. A053737, A231665, A231666, A231667."
			],
			"keyword": "nonn,base",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_, Nov 13 2013",
			"references": 6,
			"revision": 37,
			"time": "2021-12-10T05:56:45-05:00",
			"created": "2013-11-13T13:01:02-05:00"
		}
	]
}