{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A186575",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 186575,
			"data": "1,3,10,15,31,66,127,255,514,1023,2047,4098,8191,16383,32770,65535,131071,262146,524287,1048575,2097154,4194303,8388607,16777218,33554431,67108863,134217730,268435455,536870911,1073741826,2147483647,4294967295",
			"name": "Expansion of (1 + 2*x + 6*x^2)/(1 - x - x^2 - 2*x^3) in powers of x.",
			"comment": [
				"From _Kai Wang_, May 23 2020: (Start)",
				"Let f(t) = t^3 + u*t^2 + v*t + w and {x,y,z} be the simple roots of f(t).",
				"For n\u003e=0, let p(n) =  x^n/((x-y)(x-z)) + y^n/((y-x)(y-z)) + z^n/((z-x)(z-y)) and q(n) =  x^n + y^n + z^n.",
				"Then for n \u003e= 0,  q(n) = 3*p(n+2) +2*u*p(n+1) + v*p(n).",
				"In this case, f(t) = t^3 - t^2 - t - 2. q(n) = 3*p(n+2} - 2*p(n+1) - p(n).",
				"p(n) = {0, 0, 1, 1, 2, 5, 9,...}, q(n) = {3, 1, 3, 10, 15, 31,...}.",
				"a(n) = q(n+1),  A077939(n) = p(n+2). (End)"
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A186575/b186575.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Gamaliel Cerda-Morales, \u003ca href=\"https://arxiv.org/abs/1905.00725\"\u003eA note on Modified Third-order Jacobsthal numbers\u003c/a\u003e, arXiv:1905.00725 [math.CO], 2019. See pp. 3-4.",
				"Vladimir Kruchinin, \u003ca href=\"http://arxiv.org/abs/1009.2565\"\u003eComposition of ordinary generating functions\u003c/a\u003e, arXiv:1009.2565 [math.CO], 2010.",
				"Evren Eyican Polatlı and Yüksel Soykan, \u003ca href=\"https://doi.org/10.9734/ARJOM/2021/v17i230270\"\u003eOn generalized third-order Jacobsthal numbers\u003c/a\u003e, Asian Res. J. of Math. (2021) Vol. 17, No. 2, 1-19, Article No. ARJOM.66022.",
				"Kai Wang, \u003ca href=\"https://www.researchgate.net/publication/341072691_Closed_Forms_and_Generating_Functions_For_Power_Sums_outline\"\u003eClosed Forms and Generating Functions For Power Sums\u003c/a\u003e, 2020.",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,1,2)."
			],
			"formula": [
				"a(n+1) = n*Sum_{k=1..n} Sum_{j=n-3*k..k} 2^(k-j)*binomial(j,n-3*k+2*j)*binomial(k,j)/k.",
				"G.f.: [log(1/(1 - x - x^2 - 2*x^3))]', (x + x^2 + 2*x^3)^k = Sum_{n\u003e=k} Sum_{j=n-3*k..k} 2^(k-j)*binomial(j,n-3*k+2*j)*binomial(k,j)*x^n (see link).",
				"a(n) = 2^(n+1) + A099837(n+1). - _R. J. Mathar_, Mar 18 2011",
				"a(n) = a(n-1) + a(n-2) + 2*a(n-3) for n\u003e2. - _Colin Barker_, May 03 2019",
				"From _Kai Wang_, May 23 2020: (Start)",
				"a(n) = 3*A077947(n+1) - 2*A077947(n) - A077947(n-1).",
				"A077947(n) = (-8*a(n+3) + 27*a(n+2) - a(n+1))/147. (End)"
			],
			"example": [
				"G.f. = 1 + 3*x + 10*x^2 + 15*x^3 + 31*x^4 + 66*x^5 + 127*x^6 + 255*x^7 + ..."
			],
			"mathematica": [
				"CoefficientList[Series[(1+2x+6x^2)/(1-x-x^2-2x^3),{x,0,40}],x]  (* _Harvey P. Dale_, Mar 14 2011 *)"
			],
			"program": [
				"(PARI) Vec((1 + 2*x + 6*x^2) / ((1 - 2*x)*(1 + x + x^2)) + O(x^40)) \\\\ _Colin Barker_, May 03 2019",
				"(PARI) polsym(polrecip(1 - x - x^2 - 2*x^3),44)[^1] \\\\ _Joerg Arndt_, Jun 23 2020",
				"(MAGMA) R\u003cx\u003e:=PowerSeriesRing(Integers(), 35); Coefficients(R!( (1 + 2*x + 6*x^2)/(1 - x - x^2 - 2*x^3))); // _Marius A. Burtea_, Jan 31 2020"
			],
			"xref": [
				"Cf. A099837."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Vladimir Kruchinin_, Feb 23 2011",
			"ext": [
				"More terms from _Harvey P. Dale_, Mar 14 2011"
			],
			"references": 3,
			"revision": 64,
			"time": "2021-06-04T15:50:58-04:00",
			"created": "2011-02-23T22:11:54-05:00"
		}
	]
}