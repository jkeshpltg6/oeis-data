{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A053556",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 53556,
			"data": "1,1,2,3,8,30,144,280,5760,45360,44800,3991680,43545600,172972800,6706022400,93405312000,42268262400,22230464256000,376610217984000,250298560512000,11640679464960000,196503623737344000,17841281393295360000",
			"name": "Denominator of Sum_{k=0..n} (-1)^k/k!.",
			"comment": [
				"Denominator of probability of a derangement of n things (A000166(n)/n!).",
				"Also numerators of successive convergents to e using continued fraction 2 +1/(1 +1/(2 +2/(3 +3/(4 +4/(5 +5/(6 +6/(7 +7/8 +...)))))))."
			],
			"reference": [
				"L. Lorentzen and H. Waadeland, Continued Fractions with Applications, North-Holland 1992, p. 562.",
				"E. Maor, e: The Story of a Number, Princeton Univ. Press 1994, pp. 151 and 157."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A053556/b053556.txt\"\u003eTable of n, a(n) for n = 0..450\u003c/a\u003e (terms 0..100 from T. D. Noe)",
				"Leonhardo Eulero, \u003ca href=\"http://gallica.bnf.fr/ark:/12148/bpt6k69587\"\u003eIntroductio in analysin infinitorum. Tomus primus\u003c/a\u003e, Lausanne, 1748.",
				"L. Euler, Introduction à l'analyse infinitésimale, \u003ca href=\"http://gallica.bnf.fr/ark:/12148/bpt6k3884z\"\u003eTome premier\u003c/a\u003e, \u003ca href=\"http://gallica.bnf.fr/ark:/12148/bpt6k38858\"\u003eTome second\u003c/a\u003e, trad. du latin en français par J. B. Labey, Paris, 1796-1797.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Subfactorial.html\"\u003eSubfactorial\u003c/a\u003e"
			],
			"formula": [
				"Let exp(-x)/(1-x) = Sum_{n\u003e=0) (a_n/b_n) * x^n. Then sequence b_n is A053556 - Aleksandar Petojevic (apetoje(AT)ptt.yu), Apr 14 2004"
			],
			"example": [
				"Sum_{k=0..n} (-1)^k/k! = {1, 0, 1/2, 1/3, 3/8, 11/30, 53/144, 103/280, 2119/5760, ...} for n \u003e= 0."
			],
			"mathematica": [
				"Table[Denominator[Sum[(-1)^k/k!, {k, 0, n}]], {n, 0, 20}] (* _Robert G. Wilson v_, Oct 13 2005 *)",
				"Table[ Denominator[1 - Subfactorial[n]/n!], {n, 0, 22}] (* _Jean-François Alcover_, Feb 11 2014 *)",
				"Denominator[Accumulate[Table[(-1)^k/k!,{k,0,30}]]] (* _Harvey P. Dale_, Aug 22 2016 *)"
			],
			"program": [
				"(PARI) for(n=0,50, print1(denominator(sum(k=0,n,(-1)^k/k!)), \", \")) \\\\ _G. C. Greubel_, Nov 05 2017",
				"(MAGMA) [Denominator( (\u0026+[(-1)^k/Factorial(k): k in [0..n]]) ): n in [0..20]]; // _G. C. Greubel_, May 16 2019",
				"(Sage) [denominator(sum((-1)^k/factorial(k) for k in (0..n))) for n in (0..20)] # _G. C. Greubel_, May 16 2019"
			],
			"xref": [
				"Cf. A053557 (numerators), A053518-A053520. See also A103816.",
				"a(n) = (D(n, n) of A103360), A053557/A053556 = A000166/n! = (N(n, n) of A103361)/(D(n, n) of A103360)."
			],
			"keyword": "nonn,frac,nice,easy",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_, Jan 17 2000",
			"ext": [
				"More terms from _Vladeta Jovovic_, Mar 31 2000"
			],
			"references": 14,
			"revision": 39,
			"time": "2019-05-16T21:19:27-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}