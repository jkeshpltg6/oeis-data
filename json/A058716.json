{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A058716",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 58716,
			"data": "1,0,1,0,1,1,0,1,2,1,0,1,4,3,1,0,1,6,9,4,1,0,1,8,19,16,5,1,0,1,10,33,44,25,6,1,0,1,12,51,96,85,36,7,1,0,1,14,73,180,225,146,49,8,1,0,1,16,99,304,501,456,231,64,9,1",
			"name": "Triangle T(n,k) giving number of nonisomorphic loopless matroids of rank k on n labeled points (n \u003e= 0, 0 \u003c= k \u003c= n).",
			"comment": [
				"A signed version is given by A119328. - _Paul Barry_, May 14 2006"
			],
			"link": [
				"W. M. B. Dukes, \u003ca href=\"http://www.stp.dias.ie/~dukes/matroid.html\"\u003eTables of matroids\u003c/a\u003e.",
				"W. M. B. Dukes, \u003ca href=\"https://web.archive.org/web/20030208144026/http://www.stp.dias.ie/~dukes/phd.html\"\u003eCounting and Probability in Matroid Theory\u003c/a\u003e, Ph.D. Thesis, Trinity College, Dublin, 2000.",
				"W. M. B. Dukes, \u003ca href=\"https://arxiv.org/abs/math/0411557\"\u003eThe number of matroids on a finite set\u003c/a\u003e, arXiv:math/0411557 [math.CO], 2004.",
				"W. M. B. Dukes, \u003ca href=\"http://emis.impa.br/EMIS/journals/SLC/wpapers/s51dukes.html\"\u003eOn the number of matroids on a finite set\u003c/a\u003e, Séminaire Lotharingien de Combinatoire 51 (2004), Article B51g.",
				"\u003ca href=\"/index/Mat#matroid\"\u003eIndex entries for sequences related to matroids\u003c/a\u003e"
			],
			"formula": [
				"From _Paul Barry_, May 14 2006: (Start)",
				"T(n,k) = Sum{i = 0..n} (-1)^(i-k) * C(n,i) * sum{j = 0..i-k} C(k,2j)*C(i-k,2j).",
				"Column k has g.f. (x/(1-x))^k * Sum{j = 0..k} C(k,2j) * x^(2j). (End)"
			],
			"example": [
				"Triangle T(n,k) (with rows n \u003e= 0 and columns k \u003e= 0) begins as follows:",
				"  1;",
				"  0, 1;",
				"  0, 1,  1;",
				"  0, 1,  2,  1;",
				"  0, 1,  4,  3,  1;",
				"  0, 1,  6,  9,  4,  1;",
				"  0, 1,  8, 19, 16,  5, 1;",
				"  0, 1, 10, 33, 44, 25, 6, 1;",
				"  ..."
			],
			"mathematica": [
				"t[n_, k_] := Sum[(-1)^(i-k)*Binomial[n, i]*Sum[Binomial[k, 2*j]*Binomial[i-k, 2*j], {j, 0, i-k}] , {i, 0, n}]; Table[t[n, k], {n, 0, 10}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Oct 21 2013 *)"
			],
			"xref": [
				"Cf. A058717 (same except for border), A058710, A058711. Row sums give A058718. Diagonals give A000065, A058719."
			],
			"keyword": "nonn,tabl,nice",
			"offset": "0,9",
			"author": "_N. J. A. Sloane_, Dec 31 2000",
			"ext": [
				"Corrected and extended by _Jean-François Alcover_, Oct 21 2013"
			],
			"references": 6,
			"revision": 29,
			"time": "2019-10-10T16:51:47-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}