{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006065",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6065,
			"id": "M0290",
			"data": "0,0,0,1,1,1,2,2,3,5,6,7,9,10,12,15,16,18,20,23",
			"name": "Maximal number of 4-tree rows in n-tree orchard problem.",
			"comment": [
				"Maximum number of rows with exactly 4 trees in each row if there are n trees in the orchard.",
				"For further references and links see A003035."
			],
			"reference": [
				"M. Gardner, Time Travel and Other Mathematical Bewilderments. Freeman, NY, 1988, Chap. 22.",
				"F. Levi, Geometrische Konfigurationen, Hirzel, Leipzig, 1929.",
				"Xianzu Lin, A new result about orchard-planting problem, Preprint, 2005. [Shows a(20) \u003e= 23.]",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence).",
				"For further references and links see A003035."
			],
			"link": [
				"P. Berloquin, \u003ca href=\"/A008997/a008997.jpg\"\u003ea(12) \u003e= 7\u003c/a\u003e (from an article in Jeux \u0026 Strategies from 1983 - see Fig. 10).",
				"S. A. Burr, B. Grünbaum and N. J. A. Sloane, \u003ca href=\"https://doi.org/10.1007/BF00147569\"\u003eThe Orchard Problem\u003c/a\u003e, Geometriae Dedicata, 2 (1974), 397-424.",
				"S. A. Burr, B. Grünbaum and N. J. A. Sloane, \u003ca href=\"http://neilsloane.com/doc/ORCHARD/orchard.html\"\u003eThe Orchard Problem\u003c/a\u003e, Geometriae Dedicata, 2 (1974), 397-424.",
				"Zhao Hui Du, \u003ca href=\"http://bbs.emath.ac.cn/viewthread.php?tid=703\u0026amp;page=11#pid9076\"\u003eCode to verify a(13) to a(16) for orchard planting problem\u003c/a\u003e",
				"Zhao Hui Du, \u003ca href=\"https://emathgroup.github.io/blog/orchard-planting-problem#fulllist\"\u003efull list of the optimal results from 13~18 trees\u003c/a\u003e",
				"Zhao Hui Du, \u003ca href=\"http://bbs.emath.ac.cn/thread-703-1-1.html\"\u003eA chinese webpage for the problem\u003c/a\u003e",
				"Noam D. Elkies, \u003ca href=\"http://arxiv.org/abs/math/0612749\"\u003eOn some points-and-lines problems and configurations\u003c/a\u003e, arXiv:math/0612749 [math.MG], 2006.",
				"Erich Friedman, \u003ca href=\"https://erich-friedman.github.io/packing/trees/\"\u003eTable of values and bounds for up to 25 trees\u003c/a\u003e",
				"Branko Grünbaum and J. F. Rigby, \u003ca href=\"http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.118.7092\"\u003eThe real configuration (21_4)\u003c/a\u003e, Journal of the London Mathematical Society 2.2 (1990): 336-346. [Shows a(21) \u003e= 21.]",
				"Xianzu Lin, \u003ca href=\"/A006065/a006065.gif\"\u003eIllustration showing that a(20) \u003e= 23\u003c/a\u003e [The points S and T are at infinity]",
				"Ed Pegg, Jr., \u003ca href=\"http://blog.wolfram.com/2018/02/02/cultivating-new-solutions-for-the-orchard-planting-problem/\"\u003eCultivating New Solutions for theOrchard-Planting Problem\u003c/a\u003e, 2018.",
				"Ed Pegg, Jr., \u003ca href=\"http://www.mathpuzzle.com/\"\u003eMathpuzzxle Blog\u003c/a\u003e, Updated Feb 27 2020. [Gives new construction for n = 22]",
				"Ed Pegg, Jr., \u003ca href=\"/A006065/a006065_1.png\"\u003eMathpuzzxle Blog\u003c/a\u003e, Updated Feb 27 2020. [Gives new construction for n = 22] (extract, local copy)",
				"J. Solymosi and M. Stojakovic, \u003ca href=\"https://arxiv.org/abs/1107.0327\"\u003eMany collinear k-tuples with no k + 1 collinear points\u003c/a\u003e, Discrete \u0026 Computational Geometry, October 2013, Volume 50, Issue 3, pp 811-820; also arXiv 1107.0327 [math.CO], 2011-2013.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Orchard-PlantingProblem.html\"\u003eOrchard-Planting Problem.\u003c/a\u003e",
				"Zhao Hui Du, \u003ca href=\"/A006065/a006065.png\"\u003eIllustration showing that a(22)\u003e=28\u003c/a\u003e [Line ABCV is infinity line]"
			],
			"formula": [
				"a(n) \u003e= A172992(n)."
			],
			"xref": [
				"Cf. A003035, A008997.",
				"Cf. A172992 (the same problem, but with integer-valued tree coordinates)."
			],
			"keyword": "nonn,hard,nice,more",
			"offset": "1,7",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"a(13)-a(15) from _Zhao Hui Du_, Aug 24 2008",
				"a(17) from _Zhao Hui Du_, Nov 11 2008",
				"a(18) from _Zhao Hui Du_, Nov 25 2008",
				"a(19) from _Zhao Hui Du_, Dec 17 2009",
				"a(20) from _Zhao Hui Du_, Feb 01 2010"
			],
			"references": 7,
			"revision": 82,
			"time": "2020-09-03T12:23:21-04:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}