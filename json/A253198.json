{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A253198",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 253198,
			"data": "0,1,2,4,5,10,16,25,42,68,109,178,288,465,754,1220,1973,3194,5168,8361,13530,21892,35421,57314,92736,150049,242786,392836,635621,1028458,1664080,2692537,4356618,7049156,11405773,18454930,29860704,48315633,78176338,126491972,204668309,331160282,535828592",
			"name": "a(n) = a(n-1) + a(n-2) - (-1)^(a(n-1) + a(n-2))) with a(0)=0, a(1)=1.",
			"comment": [
				"This is a minimally modified Fibonacci sequence (A000045) in that it preserves characteristic properties of the original sequence: a(n) is a function of the sum of the preceding two terms, the ratio of two consecutive terms tends to the Golden Mean, and the initial two terms are the same as in the Fibonacci sequence. See A253197 and A255978 for other members of this family."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A253198/b253198.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"W. Puszkarz, \u003ca href=\"http://vixra.org/abs/1503.0113\"\u003eA Note on Minimal Extensions of the Fibonacci Sequence\u003c/a\u003e, viXra:1503.0113, 2015.",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,1,1,-1,-1)."
			],
			"formula": [
				"a(n) = a(n-1) + a(n-2) - (-1)^(a(n-1) + a(n-2))), a(0)=0, a(1)=1.",
				"a(n) = a(n-1) + a(n-2) + a(n-3) - a(n-4) - a(n-5) for n\u003e4. - _Colin Barker_, Mar 28 2015",
				"G.f.: -x*(2*x^3-x^2-x-1) / ((x-1)*(x^2+x-1)*(x^2+x+1)). - _Colin Barker_, Mar 28 2015",
				"a(n) = 2*A000045(n) - A079978(n+2). - _Nicolas Bělohoubek_, Aug 16 2021"
			],
			"example": [
				"For n=2, a(2) = 0 + 1 - (-1)^1 = 0 + 1 + 1 = 2.",
				"For n=3, a(3) = 1 + 2 - (-1)^3 = 1 + 2 + 1 = 4.",
				"For n=4, a(4) = 2 + 4 - (-1)^6 = 2 + 4 - 1 = 5."
			],
			"mathematica": [
				"RecurrenceTable[{a[n]==a[n-1]+a[n-2] -(-1)^(a[n-1]+a[n-2]), a[0]==0, a[1]==1}, a, {n, 0, 50}]",
				"LinearRecurrence[{1,1,1,-1,-1},{0,1,2,4,5},50] (* _Harvey P. Dale_, Mar 17 2019 *)"
			],
			"program": [
				"(MAGMA) [n le 2 select (n-1) else Self(n-1) + Self(n-2) - (-1)^(Self(n-1) + Self(n-2)): n in [1..50] ]; // _Vincenzo Librandi_, Mar 28 2015",
				"(PARI) concat(0, Vec(-x*(2*x^3-x^2-x-1)/((x-1)*(x^2+x-1)*(x^2+x+1)) + O(x^100))) \\\\ _Colin Barker_, Mar 28 2015"
			],
			"xref": [
				"Cf. A000045, A253197, A255978, A079978."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Waldemar Puszkarz_, Mar 24 2015",
			"references": 3,
			"revision": 40,
			"time": "2021-08-17T02:15:34-04:00",
			"created": "2015-04-03T05:18:04-04:00"
		}
	]
}