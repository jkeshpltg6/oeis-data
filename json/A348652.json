{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A348652",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 348652,
			"data": "0,1,1,1,0,-1,-2,-1,-1,-1,0,1,2,3,4,4,4,3,2,1,2,2,2,3,4,5,1,2,2,2,1,0,-1,0,0,0,1,2,3,-1,0,0,0,-1,-2,-3,-2,-2,-2,-1,0,1,-2,-1,-1,-1,-2,-3,-4,-3,-3,-3,-2,-1,0,-5,-4,-4,-4,-5,-6,-7,-6,-6,-6",
			"name": "For any nonnegative number n with base-13 expansion Sum_{k \u003e= 0} d_k*13^k, a(n) is the real part of Sum_{k \u003e= 0} g(d_k)*(3+2*i)^k where g(0) = 0, and g(1+u+3*v) = (1+u*i)*i^v for any u = 0..2 and v = 0..3 (where i denotes the imaginary unit); see A348653 for the imaginary part.",
			"comment": [
				"The function f defines a bijection from the nonnegative integers to the Gaussian integers.",
				"The following diagram depicts g(d) for d = 0..12:",
				"                      |",
				"                      |    +",
				"                      |     3",
				"                      |",
				"            +    +    +    +",
				"             6    5   |4    2",
				"                      |",
				"         --------+----+----+-------",
				"                  7   |0    1",
				"                      |",
				"                 +    +    +    +",
				"                  8   |10   11   12",
				"                      |",
				"                 +    |",
				"                  9   |"
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A348652/b348652.txt\"\u003eTable of n, a(n) for n = 0..2197\u003c/a\u003e",
				"Stephen K. Lucas, \u003ca href=\"https://www.researchgate.net/publication/355680849_Base_2_i_with_digit_set_0_1_i\"\u003eBase 2 + i with digit set {0, +/-1, +/-i}\u003c/a\u003e, ResearchGate (October 2021).",
				"Rémy Sigrist, \u003ca href=\"/A348652/a348652.png\"\u003eColored representation of f for n = 0..13^5-1 in the complex plane\u003c/a\u003e (the hue is function of n)"
			],
			"formula": [
				"a(13^k) = A121622(k)."
			],
			"program": [
				"(PARI) g(d) = { if (d==0, 0, (1+I*((d-1)%3))*I^((d-1)\\3)) }",
				"a(n) = real(subst(Pol([g(d)|d\u003c-digits(n, 13)]), 'x, 3+2*I))"
			],
			"xref": [
				"See A316657 for a similar sequence.",
				"Cf. A121622, A348653."
			],
			"keyword": "sign,base",
			"offset": "0,7",
			"author": "_Rémy Sigrist_, Oct 27 2021",
			"references": 3,
			"revision": 14,
			"time": "2021-10-29T13:31:55-04:00",
			"created": "2021-10-29T12:23:12-04:00"
		}
	]
}