{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A191567",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 191567,
			"data": "0,6,4,30,3,70,24,126,10,198,60,286,21,390,112,510,36,646,180,798,55,966,264,1150,78,1350,364,1566,105,1798,480,2046,136,2310,612,2590,171,2886,760,3198,210,3526,924,3870,253,4230,1104,4606,300,4998,1300,5406,351",
			"name": "Four interlaced 2nd order polynomials: a(4*k) = k*(1+2*k); a(1+2*k) = 2*(1+2*k)*(3+2*k); a(2+4*k) = 4*(1+k)*(1+2*k).",
			"comment": [
				"a(n) = T(0,n) and differences T(n,k) = T(n-1,k+1) - T(n-1,k) define the array",
				"0,   6,  4,  30,    3,  70,   24,  126,   10,  198,   60,  286,   21,  390,  ..",
				"6,  -2, 26, -27,   67, -46,  102, -116,  188, -138,  226, -265,  369, -278, ..",
				"-8, 28 -53,  94, -113, 148, -218,  304, -326,  364, -491,  634, -647,  676, ...",
				"T(3,n) mod 9 is the sequence 1, 1, 1, 4, 4, 4, 7, 7, 7, 4, 4, 4 (and periodically repeated with period 12).",
				"A064680(2+n) divides a(n), where b(n) = a(n)/A064680(2+n) = 0, 1, 2, 3, 1, 5, 6, 7, 2,... for n\u003e=0, obeys b(4*k) = k and has recurrence b(n) = 2*b(n-4) - b(n-8)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A191567/b191567.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_12\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,0,0,3,0,0,0,-3,0,0,0,1)."
			],
			"formula": [
				"a(n) = 3*a(n-4) - 3*a(n-8) + a(n-12).",
				"a(n) = A061037(n+2) + A181318(n). - _Paul Curtz_, Jul 19 2011",
				"a(n) = A060819(n) * A145979(n). - _Paul Curtz_, Sep 06 2011",
				"G.f.: x*(6+4*x+30*x^2+3*x^3+52*x^4+12*x^5+36*x^6+x^7+6*x^8-2*x^10) /( (1-x)^3 *(1+x)^3 *(1+x^2)^3 ). - _R. J. Mathar_, Jun 17 2011",
				"Let BEB(n) = a(n)/A061038(n+2) = A060819(n)/A145979(n). Then (BEB(n))^2 = A181318(n)/A061038(n+2) = BEB(n) - A061037(n+2)/A061038(n+2). - _Paul Curtz_, Jul 19 2011, index corrected by _R. J. Mathar_, Sep 09 2011",
				"From _Luce ETIENNE_, Apr 18 2017: (Start)",
				"a(n) = n*(n + 2)*(37 - 27*(-1)^n - 3*((-1)^((2*n + 1 - (-1)^n)/4) + (-1)^((2*n - 1 + (-1)^n)/4)))/32.",
				"a(n) = n*(n+2)*(37-27*cos(n*Pi) - 6*cos(n*Pi/2))/32.",
				"a(n) = n*(n + 2)*(37 - 27*(-1)^n - 3*(i^n + (-i)^n))/32, where i=sqrt(-1). (End)"
			],
			"mathematica": [
				"Table[Which[OddQ@ n, 2 (1 + 2 #) (3 + 2 #) \u0026[(n - 1)/2], Mod[n, 4] == 0, # (1 + 2 #) \u0026[n/4], True, 4 (1 + #) (1 + 2 #) \u0026[(n - 2)/4]], {n, 0, 60}] (* or *)",
				"CoefficientList[Series[x(6 +4x +30x^2 +3x^3 +52x^4 +12x^5 +36x^6 +x^7 +6x^8 -2x^10)/((1-x)^3*(1+x)^3*(1+x^2)^3), {x, 0, 60}], x] (* _Michael De Vlieger_, Apr 22 2017 *)",
				"LinearRecurrence[{0,0,0,3,0,0,0,-3,0,0,0,1}, {0,6,4,30,3,70,24,126,10,198,60, 286}, 80] (* _Vincenzo Librandi_, Apr 23 2017 *)"
			],
			"program": [
				"(MAGMA) I:=[0,6,4,30,3,70,24,126,10,198,60,286]; [n le 12 select I[n] else 3*Self(n-4)-3*Self(n-8)+Self(n-12): n in [1..60]]; // _Vincenzo Librandi_, Apr 23 2017",
				"(PARI) m=60; v=concat([0,6,4,30,3,70,24,126,10,198,60,286], vector(m-12)); for(n=13, m, v[n]=3*v[n-4]-3*v[n-8]+v[n-12]); v \\\\ _G. C. Greubel_, Feb 26 2019",
				"(Sage) (x*(6+4*x+30*x^2+3*x^3+52*x^4+12*x^5+36*x^6+x^7+6*x^8-2*x^10)/((1-x)^3 *(1+x)^3*(1+x^2)^3 )).series(x, 60).coefficients(x, sparse=False) # _G. C. Greubel_, Feb 26 2019",
				"(GAP) a:=[0,6,4,30,3,70,24,126,10,198,60,286];; for n in [13..60] do a[n]:= 3*a[n-4]-3*a[n-8]+a[n-12]; od; a; # _G. C. Greubel_, Feb 26 2019"
			],
			"xref": [
				"Cf. A014105, A000466, A000384, A177427."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Paul Curtz_, Jun 12 2011",
			"references": 3,
			"revision": 48,
			"time": "2019-02-27T02:54:01-05:00",
			"created": "2011-07-13T17:11:15-04:00"
		}
	]
}