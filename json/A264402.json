{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A264402",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 264402,
			"data": "1,2,2,1,3,2,2,4,1,4,5,2,2,8,4,1,4,9,7,2,3,12,10,4,1,4,14,15,7,2,2,17,20,12,4,1,6,18,27,17,7,2,2,23,33,26,12,4,1,4,24,44,35,19,7,2,4,27,51,49,28,12,4,1,5,30,64,63,41,19,7,2,2",
			"name": "Triangle read by rows: T(n,k) is the number of partitions of n that have k parts larger than the smallest part (n\u003e=1, k\u003e=0).",
			"comment": [
				"T(n,k) = number of partitions of n in which the 2nd largest part is k (0 if all parts are equal). Example: T(7,2) = 4 because we have [3,2,1,1], [3,2,2], [4,2,1], and [5,2].",
				"The fact that the above two statistics (in Name and in 1st Comment) have the same distribution follows at once by conjugation. - _Emeric Deutsch_, Dec 11 2015",
				"Row sums yield the partition numbers (A000041).",
				"T(n,0) = A000005(n) = number of divisors of n.",
				"Sum_{k\u003e=0} k*T(n,k) = A182984(n)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A264402/b264402.txt\"\u003eRows n = 1..350, flattened\u003c/a\u003e"
			],
			"formula": [
				"G.f.: G(t,x) = Sum_{i\u003e=1} (x^i/((1 - x^i)*Product_{j\u003e=i+1}(1-t*x^j)))."
			],
			"example": [
				"T(7,2) = 4 because we have [2,2,1,1,1], [3,2,1,1], [3,3,1], and [4,2,1].",
				"Triangle starts:",
				"1;",
				"2;",
				"2,1;",
				"3,2;",
				"2,4,1;",
				"4,5,2;",
				"2,8,4,1;"
			],
			"maple": [
				"g := sum(x^i/((1-x^i)*(product(1-t*x^j, j = i+1 .. 100))), i = 1 .. 100): gser := simplify(series(g, x = 0, 30)): for n to 27 do P[n] := sort(coeff(gser, x, n)) end do: for n to 27 do seq(coeff(P[n], t, j), j = 0 .. degree(P[n])) end do; # yields sequence in triangular form",
				"# second Maple program:",
				"b:= proc(n, i) option remember; `if`(n=0, [1, 0],",
				"      `if`(i\u003c1, 0, b(n, i-1) +add((p-\u003e[0, p[1]+",
				"       expand(p[2]*x^j)])(b(n-i*j, i-1)) , j=1..n/i)))",
				"    end:",
				"T:= n-\u003e(p-\u003eseq(coeff(p, x, i), i=0..degree(p)))(b(n$2)[2]):",
				"seq(T(n), n=1..20);  # _Alois P. Heinz_, Nov 29 2015"
			],
			"mathematica": [
				"b[n_, i_] := b[n, i] = If[n==0, {1, 0}, If[i\u003c1, {0, 0}, b[n, i-1] + Sum[ Function[p, {0, p[[1]] + Expand[p[[2]]*x^j]}][b[n-i*j, i-1]], {j, 1, n/i} ]]]; T[n_] := Function[p, Table[Coefficient[p, x, i], {i, 0, Exponent[p, x]}]][b[n, n][[2]]]; Table[T[n], {n, 1, 20}] // Flatten (* _Jean-François Alcover_, Jan 15 2016, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A000005, A000041, A116685, A182984, A002541."
			],
			"keyword": "nonn,tabf",
			"offset": "1,2",
			"author": "_Emeric Deutsch_, Nov 21 2015",
			"references": 4,
			"revision": 17,
			"time": "2016-01-15T15:42:54-05:00",
			"created": "2015-11-22T02:20:22-05:00"
		}
	]
}