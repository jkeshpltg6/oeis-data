{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A129862",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 129862,
			"data": "1,2,-1,4,-4,1,4,-10,6,-1,4,-20,21,-8,1,4,-34,56,-36,10,-1,4,-52,125,-120,55,-12,1,4,-74,246,-329,220,-78,14,-1,4,-100,441,-784,714,-364,105,-16,1,4,-130,736,-1680,1992,-1364,560,-136,18,-1,4,-164,1161,-3312,4950,-4356,2379,-816,171,-20,1",
			"name": "Triangle read by rows: T(n,k) is the coefficient [x^k] of (-1)^n times the characteristic polynomial of the Cartan matrix for the root system D_n.",
			"comment": [
				"Row sums of the absolute values are s(n) = 1, 3, 9, 21, 54, 141, 369, 966, 2529, 6621, 17334, ... (i.e., s(n) = 3*|A219233(n-1)| for n \u003e 0). - _R. J. Mathar_, May 31 2014)"
			],
			"reference": [
				"R. N. Cahn, Semi-Simple Lie Algebras and Their Representations, Dover, NY, 2006, ISBN 0-486-44999-8, p. 60.",
				"Sigurdur Helgasson, Differential Geometry, Lie Groups and Symmetric Spaces, Graduate Studies in Mathematics, volume 34. A. M. S. :ISBN 0-8218-2848-7, 1978, p. 464."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A129862/b129862.txt\"\u003eRows n = 0..100 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = coefficients of ( (2-x)*Lucas(2*n-2, i*sqrt(x)) ) with T(0, 0) = 1, T(1, 0) = 2 and T(1, 1) = -1. - _G. C. Greubel_, Jun 21 2021"
			],
			"example": [
				"Triangle begins:",
				"  1;",
				"  2,   -1;",
				"  4,   -4,    1;",
				"  4,  -10,    6,    -1;",
				"  4,  -20,   21,    -8,    1;",
				"  4,  -34,   56,   -36,   10,    -1;",
				"  4,  -52,  125,  -120,   55,   -12,    1;",
				"  4,  -74,  246,  -329,  220,   -78,   14,   -1;",
				"  4, -100,  441,  -784,  714,  -364,  105,  -16,   1;",
				"  4, -130,  736, -1680, 1992, -1364,  560, -136,  18,  -1;",
				"  4, -164, 1161, -3312, 4950, -4356, 2379, -816, 171, -20, 1;"
			],
			"maple": [
				"A129862 := proc(n,k)",
				"    M := Matrix(n,n);",
				"    for r from 1 to n do",
				"    for c from 1 to n do",
				"        if r = c then",
				"            M[r,c] := 2;",
				"        elif abs(r-c)= 1 then",
				"            M[r,c] := -1;",
				"        else",
				"            M[r,c] := 0 ;",
				"        end if;",
				"    end do:",
				"    end do:",
				"    if n-2 \u003e= 1 then",
				"        M[n,n-2] := -1 ;",
				"        M[n-2,n] := -1 ;",
				"    end if;",
				"    if n-1 \u003e= 1 then",
				"        M[n-1,n] := 0 ;",
				"        M[n,n-1] := 0 ;",
				"    end if;",
				"    LinearAlgebra[CharacteristicPolynomial](M,x) ;",
				"    (-1)^n*coeftayl(%,x=0,k) ;",
				"end proc: # _R. J. Mathar_, May 31 2014"
			],
			"mathematica": [
				"(* First program *)",
				"t[n_, m_, d_]:= If[n==m, 2, If[(m==d \u0026\u0026 n==d-2) || (n==d \u0026\u0026 m==d-2), -1, If[(n==m- 1 || n==m+1) \u0026\u0026 n\u003c=d-1 \u0026\u0026 m\u003c=d-1, -1, 0]]];",
				"M[d_]:= Table[t[n,m,d], {n,1,d}, {m,1,d}];",
				"p[n_, x_]:= If[n==0, 1, CharacteristicPolynomial[M[n], x]];",
				"T[n_, k_]:= SeriesCoefficient[p[n, x], {x, 0, k}];",
				"Table[T[n, k], {n,0,12}, {k,0,n}]//Flatten (* modified by _G. C. Greubel_, Jun 21 2021 *)",
				"(* Second program *)",
				"Join[{{1}, {2, -1}}, CoefficientList[Table[(2-x)*LucasL[2(n-1), Sqrt[-x]], {n, 2, 10}], x]]//Flatten (* _Eric W. Weisstein_, Apr 04 2018 *)"
			],
			"program": [
				"(Sage)",
				"def p(n,x): return 2*(2-x)*sum( ((n-1)/(2*n-k-2))*binomial(2*n-k-2, k)*(-x)^(n-k-1) for k in (0..n-1) )",
				"def T(n): return ( p(n,x) ).full_simplify().coefficients(sparse=False)",
				"[1,2,-1]+flatten([T(n) for n in (2..12)]) # _G. C. Greubel_, Jun 21 2021"
			],
			"xref": [
				"Cf. A156608, A156609, A156610, A156612, A219233."
			],
			"keyword": "tabl,sign",
			"offset": "0,2",
			"author": "_Roger L. Bagula_, May 23 2007",
			"references": 6,
			"revision": 26,
			"time": "2021-06-25T03:30:08-04:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}