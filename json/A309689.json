{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A309689",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 309689,
			"data": "0,0,0,0,0,1,2,2,2,3,4,5,6,7,8,9,10,12,14,15,16,18,20,22,24,26,28,30,32,35,38,40,42,45,48,51,54,57,60,63,66,70,74,77,80,84,88,92,96,100,104,108,112,117,122,126,130,135,140,145,150,155,160,165,170",
			"name": "Number of even parts appearing among the second largest parts of the partitions of n into 3 parts.",
			"link": [
				"Colin Barker, \u003ca href=\"/A309689/b309689.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Par#part\"\u003eIndex entries for sequences related to partitions\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_10\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,-2,2,-1,0,1,-2,2,-2,1)."
			],
			"formula": [
				"a(n) = Sum_{j=1..floor(n/3)} Sum_{i=j..floor((n-j)/2)} ((i-1) mod 2).",
				"From _Colin Barker_, Aug 23 2019: (Start)",
				"G.f.: x^5 / ((1 - x)^3*(1 + x)*(1 - x + x^2)*(1 + x^2)*(1 + x + x^2)).",
				"a(n) = 2*a(n-1) - 2*a(n-2) + 2*a(n-3) - a(n-4) + a(n-6) - 2*a(n-7) + 2*a(n-8) - 2*a(n-9) + a(n-10) for n\u003e9.",
				"(End)",
				"a(n) = (6*n^2+48*cos(n*Pi/3)-36*cos(n*Pi/2)+16*cos(2*n*Pi/3)-3*(-1)^n-25)/144. - _Ilya Gutkovskiy_, Oct 29 2021"
			],
			"example": [
				"Figure 1: The partitions of n into 3 parts for n = 3, 4, ...",
				"                                                          1+1+8",
				"                                                   1+1+7  1+2+7",
				"                                                   1+2+6  1+3+6",
				"                                            1+1+6  1+3+5  1+4+5",
				"                                     1+1+5  1+2+5  1+4+4  2+2+6",
				"                              1+1+4  1+2+4  1+3+4  2+2+5  2+3+5",
				"                       1+1+3  1+2+3  1+3+3  2+2+4  2+3+4  2+4+4",
				"         1+1+1  1+1+2  1+2+2  2+2+2  2+2+3  2+3+3  3+3+3  3+3+4    ...",
				"-----------------------------------------------------------------------",
				"  n  |     3      4      5      6      7      8      9     10      ...",
				"-----------------------------------------------------------------------",
				"a(n) |     0      0      1      2      2      2      3      4      ...",
				"-----------------------------------------------------------------------"
			],
			"mathematica": [
				"Table[Sum[Sum[Mod[i - 1, 2], {i, j, Floor[(n - j)/2]}], {j, Floor[n/3]}], {n, 0, 80}]",
				"LinearRecurrence[{2, -2, 2, -1, 0, 1, -2, 2, -2, 1}, {0, 0, 0, 0, 0, 1, 2, 2, 2, 3}, 80]"
			],
			"program": [
				"(PARI) concat([0,0,0,0,0], Vec(x^5 / ((1 - x)^3*(1 + x)*(1 - x + x^2)*(1 + x^2)*(1 + x + x^2)) + O(x^40))) \\\\ _Colin Barker_, Aug 23 2019"
			],
			"xref": [
				"Cf. A026923, A026927, A309683, A309684, A309685, A309686, A309687, A309688, A309690, A309692, A309694."
			],
			"keyword": "nonn,easy",
			"offset": "0,7",
			"author": "_Wesley Ivan Hurt_, Aug 12 2019",
			"references": 12,
			"revision": 24,
			"time": "2021-11-02T07:05:03-04:00",
			"created": "2019-08-12T22:34:46-04:00"
		}
	]
}