{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A335311",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 335311,
			"data": "1,1,2,2,6,12,3,24,72,24,24,4,120,480,180,360,40,120,5,720,3600,1440,4320,360,2160,720,60,240,180,6,5040,30240,12600,50400,3360,30240,20160,630,5040,3780,7560,84,420,840,7",
			"name": "Coefficients of polynomials arising in the series expansion of the multiplicative inverse of an analytic function. Irregular triangle read by rows.",
			"comment": [
				"The coefficients of Bell-type polynomials where the monomials correspond to integer partitions. The monomials are in graded lexicographic order with variables x[0] \u003e x[1] \u003e ... \u003e x[n]. This means that monomials are compared first by their total degree, with ties broken by lexicographic order. (This is the monomial order of Maple after sorting.)"
			],
			"example": [
				"The triangle starts (the refinement is indicated by square brackets):",
				"[0]    1;",
				"[1]    1;",
				"[2]    2,     2;",
				"[3]    6,    12,     3;",
				"[4]   24,    72,    (24,    24),    4;",
				"[5]  120,   480,   (180,   360),   (40,   120),     5;",
				"[6]  720,  3600,  (1440,  4320),  (360,  2160,   720), (60,  240,  180),    6;",
				"[7] 5040, 30240, (12600, 50400), (3360, 30240, 20160), (630, 5040, 3780, 7560), (84, 420, 840), 7;",
				"[8] 40320, 282240, (120960, 604800), (33600, 403200, 403200), (6720, 80640, 60480,",
				"241920, 40320), (1008, 10080, 20160, 20160, 30240), (112, 672, 1680, 1120), 8;",
				"The multivariate polynomials start:",
				"        1",
				"        x[0]",
				"      2*x[0]^2 +          2*x[1]",
				"      6*x[0]^3 +    12*x[0]*x[1] +          3*x[2]",
				"     24*x[0]^4 +  72*x[0]^2*x[1] +    24*x[0]*x[2] +       24*x[1]^2 +       4*x[3]",
				"    120*x[0]^5 + 480*x[0]^3*x[1] + 180*x[0]^2*x[2] + 360*x[0]*x[1]^2 + 40*x[0]*x[3] + 120*x[1]*x[2] + 5*x[4]"
			],
			"maple": [
				"A335311Triangle := proc(numrows) local ser, p, C, B, P;",
				"B(0) := 1; ser := series(1/B(s), s, numrows);",
				"C := [seq(expand(simplify(n!*coeff(ser,s,n))), n=0..numrows-1)]:",
				"P := subs(seq((D@@n)(B)(0)=n*x[n], n=1..numrows), C):",
				"for p in P do print(seq(abs(c), c=coeffs(sort(p)))) od end:",
				"A335311Triangle(8);"
			],
			"xref": [
				"Cf. A199673 (row reversed refinement), A006153 (row sums),  A000041 (length of rows), A182779 (different monomial order)."
			],
			"keyword": "nonn,tabf",
			"offset": "0,3",
			"author": "_Peter Luschny_, May 31 2020",
			"references": 0,
			"revision": 19,
			"time": "2020-06-01T06:48:58-04:00",
			"created": "2020-06-01T06:48:58-04:00"
		}
	]
}