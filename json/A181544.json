{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A181544",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 181544,
			"data": "1,1,4,1,1,20,48,20,1,1,54,405,760,405,54,1,1,112,1828,8464,13840,8464,1828,112,1,1,200,5925,52800,182700,273504,182700,52800,5925,200,1,1,324,15606,233300,1424925,4050864,5703096,4050864,1424925,233300,15606,324,1,1,490,35623,818300,7917371,37215794,91789005,123519792,91789005,37215794,7917371,818300,35623,490,1,1,704,73200,2430400,34657700,246781248,955910032,2116980800,2751843600,2116980800,955910032,246781248,34657700,2430400,73200,704,1",
			"name": "Triangle in which the g.f. for row n is [Sum_{k\u003e=0} C(n+k-1,k)^3*x^k]*(1-x)^(3n+1), read by rows of k=0..2n terms.",
			"link": [
				"Paul D. Hanna, \u003ca href=\"/A181544/b181544.txt\"\u003eTable of n, a(n) for n = 0..1088, as a flattened triangle of rows 0..32\u003c/a\u003e"
			],
			"formula": [
				"Row sums equal A006480(n) = (3n)!/(n!)^3, which is de Bruijn's s(3,n).",
				"From _Yahia Kahloune_, Jan 30 2014: (Start)",
				"Using these coefficients we can obtain formulas for the sums",
				"Sum_{i=1..n} C(e-1+i,e)^3. Let us define b(k,e,3) = sum_{i=0..k-e} (-1)^i*C(3*e+1,i)*C(k-i,e)^3, where k=e+i.",
				"For example:",
				"b(e,e,3) = 1;",
				"b(e+1,e,3) = (e+1)^3-(3*e+1) = e^2*(e+3);",
				"b(e+2,e,3) = C(e+2,2)^3 - (3*e+1)*(e+1)^3 + C(3*e+1,2);",
				"b(e+3,e,3) = C(e+3,e)^3 - (3*e+1)*C(e+2,e)^3 + C(3*e+1,2)*C(e+1,e)^3 - C(3*e+1,3);",
				"b(e+4,e,3) = C(e+4,e)^3 - (3*e+1)*C(e+3,e)^3  + C(3*e+1,2)*C(e+2,e) - C(3*e+1,3)*C(e+1,e)^3 + C(3*e+1,4).",
				"Then we have the formula: Sum_{i=1..n} C(e-1+i,e)^3 = Sum_{i=0..2*e} b(e+i,e,3)*C(n+e+i,3*e+1).",
				"Example: Sum_{i=1..7} C(2+i,3)^3 = C(10,10) + 54*C(11,10) + 405*C(12,10) + 760*C(13,10) + 405*C(14,10) + 54*C(15,10) + C(16,10) = 820260.",
				"(End)"
			],
			"example": [
				"Triangle begins:",
				"1;",
				"1, 4, 1;",
				"1, 20, 48, 20, 1;",
				"1, 54, 405, 760, 405, 54, 1;",
				"1, 112, 1828, 8464, 13840, 8464, 1828, 112, 1;",
				"1, 200, 5925, 52800, 182700, 273504, 182700, 52800, 5925, 200, 1;",
				"1, 324, 15606, 233300, 1424925, 4050864, 5703096, 4050864, 1424925, 233300, 15606, 324, 1; ...",
				"Row g.f.s begin:",
				"(1) = (1-x)*(1 + x + x^2 + x^3 + x^4 +...);",
				"(1 + 4*x + x^2) = (1-x)^4*(1 + 2^3*x + 3^3*x^2 + 4^3*x^3 +...);",
				"(1 + 20*x + 48*x^2 + 20*x^3 + x^4) = (1-x)^7*(1 + 3^3*x + 6^3*x^2 +...);",
				"(1 + 54*x + 405*x^2 + 760*x^3 + 405*x^4 + 54*x^5 + x^6) = (1-x)^10*(1 + 4^3*x + 10^3*x^2 + 20^3*x^3 + 35^3*x^4 +...); ..."
			],
			"mathematica": [
				"t[n_, k_] := SeriesCoefficient[Sum[Binomial[n+j, j]^3*x^j, {j, 0, n+k}]*(1-x)^(3*n+1), {x,0, k}]; Table[t[n, k], {n, 0, 9}, {k, 0, 2*n}] // Flatten (* _Jean-François Alcover_, Feb 04 2014, after PARI *)"
			],
			"program": [
				"(PARI) {T(n,k)=polcoeff(sum(j=0,n+k,binomial(n+j,j)^3*x^j)*(1-x)^(3*n+1),k)}",
				"for(n=0,10,for(k=0,2*n,print1(T(n,k),\", \"));print(\"\"))"
			],
			"xref": [
				"Cf. A181543, A181545, A006480, A002897, A000172.",
				"Cf. A183204 (central terms), A183205."
			],
			"keyword": "nonn,tabf",
			"offset": "0,3",
			"author": "_Paul D. Hanna_, Oct 30 2010",
			"references": 9,
			"revision": 33,
			"time": "2017-01-01T19:47:17-05:00",
			"created": "2010-11-10T03:00:00-05:00"
		}
	]
}