{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A008831",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 8831,
			"data": "0,1,4,2,9,5,11,3,8,10,7,6",
			"name": "Discrete logarithm of n to the base 2 modulo 13.",
			"comment": [
				"This is also a (12,1)-sequence.",
				"Equivalently, a(n) is the multiplicative order of n with respect to base 2 (modulo 13), i.e., a(n) is the base-2 logarithm of the smallest k such that 2^k mod 13 = n."
			],
			"reference": [
				"I. M. Vinogradov, Elements of Number Theory, p. 220."
			],
			"link": [
				"H. Y. Song and S. W. Golomb, \u003ca href=\"http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.109.5973\"\u003eGeneralized Welch-Costas sequences and their application to Vatican arrays\u003c/a\u003e, in Proc. 2nd International Conference on Finite Fields: Theory, Algorithms and Applications (Las Vegas 1993) Contemp. Math. vol. 168 344 1994.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"https://mathworld.wolfram.com/DiscreteLogarithm.html\"\u003eDiscrete Logarithm\u003c/a\u003e."
			],
			"formula": [
				"2^a(n) == n (mod 13). - _Michael S. Branicky_, Aug 22 2021"
			],
			"example": [
				"From _Jon E. Schoenfield_, Aug 21 2021: (Start)",
				"Sequence is a permutation of the 12 integers 0..11:",
				"   k     2^k  2^k mod 13",
				"  --  ------  ----------",
				"   0       1           1  so a(1)  =  0",
				"   1       2           2  so a(2)  =  1",
				"   2       4           4  so a(4)  =  2",
				"   3       8           8  so a(8)  =  3",
				"   4      16           3  so a(3)  =  4",
				"   5      32           6  so a(6)  =  5",
				"   6      64          12  so a(12) =  6",
				"   7     128          11  so a(11) =  7",
				"   8     256           9  so a(9)  =  8",
				"   9     512           5  so a(5)  =  9",
				"  10    1024          10  so a(10) = 10",
				"  11    2048           7  so a(7)  = 11",
				"  12    4096           1",
				"but a(1) = 0, so the sequence is finite with 12 terms.",
				"(End)"
			],
			"maple": [
				"[ seq(numtheory[mlog](n, 2, 13), n=1..12) ];"
			],
			"mathematica": [
				"a[1] = 0; a[n_] := MultiplicativeOrder[2, 13, {n}]; Array[a, 12] (* _Jean-François Alcover_, Feb 09 2018 *)"
			],
			"program": [
				"(Python)",
				"from sympy.ntheory import discrete_log",
				"def a(n): return discrete_log(13, n, 2)",
				"print([a(n) for n in range(1, 13)]) # _Michael S. Branicky_, Aug 22 2021"
			],
			"xref": [
				"A row of A054503."
			],
			"keyword": "nonn,base,fini,full",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_",
			"references": 2,
			"revision": 22,
			"time": "2021-08-25T22:15:55-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}