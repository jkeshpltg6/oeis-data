{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A229037",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 229037,
			"data": "1,1,2,1,1,2,2,4,4,1,1,2,1,1,2,2,4,4,2,4,4,5,5,8,5,5,9,1,1,2,1,1,2,2,4,4,1,1,2,1,1,2,2,4,4,2,4,4,5,5,8,5,5,9,9,4,4,5,5,10,5,5,10,2,10,13,11,10,8,11,13,10,12,10,10,12,10,11,14,20,13",
			"name": "The \"forest fire\": sequence of positive integers where each is chosen to be as small as possible subject to the condition that no three terms a(j), a(j+k), a(j+2k) (for any j and k) form an arithmetic progression.",
			"comment": [
				"Added name \"forest fire\" to make it easier to locate this sequence. - _N. J. A. Sloane_, Sep 03 2019",
				"This sequence and A235383 and A235265 were winners in the best new sequence contest held at the OEIS Foundation booth at the 2014 AMS/MAA Joint Mathematics Meetings. - _T. D. Noe_, Jan 20 2014",
				"See A236246 for indices n such that a(n)=1. - _M. F. Hasler_, Jan 20 2014",
				"See A241673 for indices n such that a(n)=2^k. - _Reinhard Zumkeller_, Apr 26 2014",
				"The graph (for up to n = 10000) has an eerie similarity (why?) to the distribution of rising smoke particles subjected to a lateral wind, and where the particles emanate from randomly distributed burning areas in a fire in a forest or field. - _Daniel Forgues_, Jan 21 2014",
				"The graph (up to n = 100000) appears to have a fractal structure. The dense areas are not random but seem to repeat, approximately doubling in width and height each time. - _Daniel Forgues_, Jan 21 2014",
				"a(A241752(n)) = n and a(m) != n for m \u003c A241752(n). - _Reinhard Zumkeller_, Apr 28 2014"
			],
			"link": [
				"Giovanni Resta, Alois P. Heinz, and Charles R Greathouse IV, \u003ca href=\"/A229037/b229037.txt\"\u003eTable of n, a(n) for n = 1..100000\u003c/a\u003e (1..1000 from Resta, 1001..10000 from Heinz, and 10001..100000 from Greathouse)",
				"Jean Bickerton, \u003ca href=\"/A229037/a229037_4.png\"\u003eColored plot of 200000 terms; color is difference from previous term in sequence\u003c/a\u003e",
				"Xan Gregg, \u003ca href=\"/A229037/a229037_1.png\"\u003eEnhanced scatterplot of 10000 terms\u003c/a\u003e [In this plot, the points have been made translucent to reduce the information lost to overstriking, and the point size varies with n in an attempt to keep the density comparable.]",
				"OEIS, \u003ca href=\"/A229037/a229037.png\"\u003ePin plot of 200 terms and scatterplot of 10000 terms\u003c/a\u003e",
				"Sébastien Palcoux, \u003ca href=\"https://mathoverflow.net/q/338415/34538\"\u003eOn the first sequence without triple in arithmetic progression\u003c/a\u003e (version: 2019-08-21), MathOverflow",
				"Sébastien Palcoux, \u003ca href=\"/A229037/a229037.txt\"\u003eTable of n, a(n) for n = 1..1000000\u003c/a\u003e",
				"Sébastien Palcoux, \u003ca href=\"/A229037/a229037_3.png\"\u003eDensity plot of the first 1000000 terms\u003c/a\u003e",
				"Reddit User \"garnet420\", \u003ca href=\"/A229037/a229037.jpg\"\u003eColored plot of 16 million terms; horizontal divisions are 1000000; vertical divisions are 25000\u003c/a\u003e",
				"Reddit User \"garnet420\", \u003ca href=\"/A229037/a229037_5.png\"\u003eB/W plot of 16 million terms; horizontal divisions are 1000000; vertical divisions are 25000\u003c/a\u003e",
				"N. J. A. Sloane and Brady Haran, \u003ca href=\"https://www.youtube.com/watch?v=o8c4uYnnNnc\"\u003eAmazing Graphs II (including Star Wars)\u003c/a\u003e, Numberphile video (2019)",
				"\u003ca href=\"http://oeis.org/wiki/Index_to_OEIS:_Section_Gra#graphs_plots\"\u003eIndex entries for sequences with interesting graphs or plots\u003c/a\u003e",
				"\u003ca href=\"http://oeis.org/wiki/Index_to_OEIS:_Section_No#non_averaging\"\u003eIndex entries for non-averaging sequences\u003c/a\u003e"
			],
			"formula": [
				"a(n) \u003c= (n+1)/2. - _Charles R Greathouse IV_, Jan 21 2014"
			],
			"mathematica": [
				"a[1] = 1; a[n_] := a[n] = Block[{z = 1}, While[Catch[ Do[If[z == 2*a[n-k] - a[n-2*k], Throw@True], {k, Floor[(n-1)/2]}]; False], z++]; z]; a /@ Range[100] (* _Giovanni Resta_, Jan 01 2014 *)"
			],
			"program": [
				"(PARI) step(v)=my(bad=List(),n=#v+1,t); for(d=1,#v\\2,t=2*v[n-d]-v[n-2*d]; if(t\u003e0, listput(bad,t))); bad=Set(bad); for(i=1,#bad, if(bad[i]!=i, return(i))); #bad+1",
				"first(n)=my(v=List([1])); while(n--, listput(v, step(v))); Vec(v) \\\\ _Charles R Greathouse IV_, Jan 21 2014",
				"(Haskell)",
				"import Data.IntMap (empty, (!), insert)",
				"a229037 n = a229037_list !! (n-1)",
				"a229037_list = f 0 empty  where",
				"   f i m = y : f (i + 1) (insert (i + 1) y m) where",
				"     y = head [z | z \u003c- [1..],",
				"                   all (\\k -\u003e z + m ! (i - k) /= 2 * m ! (i - k `div` 2))",
				"                       [1, 3 .. i - 1]]",
				"-- _Reinhard Zumkeller_, Apr 26 2014",
				"(Python)",
				"A229037_list = []",
				"for n in range(10**6):",
				"....i, j, b = 1, 1, set()",
				"....while n-2*i \u003e= 0:",
				"........b.add(2*A229037_list[n-i]-A229037_list[n-2*i])",
				"........i += 1",
				"........while j in b:",
				"............b.remove(j)",
				"............j += 1",
				"....A229037_list.append(j) # _Chai Wah Wu_, Dec 21 2014"
			],
			"xref": [
				"Cf. A094870.",
				"For a variant see A309890.",
				"A selection of sequences related to \"no three-term arithmetic progression\": A003002, A003003, A003278, A004793, A005047, A005487, A033157, A065825, A092482, A093678, A093679, A093680, A093681, A093682, A094870, A101884, A101886, A101888, A140577, A185256, A208746, A229037."
			],
			"keyword": "nonn,easy,nice,look",
			"offset": "1,3",
			"author": "_Jack W Grahl_, Sep 11 2013",
			"references": 36,
			"revision": 109,
			"time": "2019-09-26T11:03:13-04:00",
			"created": "2014-01-20T10:01:42-05:00"
		}
	]
}