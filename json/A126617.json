{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A126617",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 126617,
			"data": "1,-1,2,-3,7,-10,31,-21,204,307,2811,12100,74053,432211,2768858,18473441,129941283,956187814,7351696139,58897405759,490681196604,4242903803727,38014084430983,352341755256348,3373662303816313,33326335433122711,339232538387804530",
			"name": "a(n) = Sum_{i=0..n} (-2)^(n-i)*B(i)*binomial(n,i) where B(n) = Bell numbers A000110(n).",
			"comment": [
				"a(n) is positive starting at n=8. - _Karol A. Penson_ and _Olivier Gérard_, Oct 22 2007",
				"Hankel transform is A000178. - _Paul Barry_, Apr 23 2009"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A126617/b126617.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e"
			],
			"formula": [
				"E.g.f.: exp(exp(x)-2*x-1). - _Vladeta Jovovic_, Aug 04 2007",
				"a(n) = e^(-1) * Sum_{k\u003e=0} (k-2)^n / k!. This is a Dobinski-type formula. - _Karol A. Penson_ and _Olivier Gérard_, Oct 22 2007",
				"G.f.: 1/(1+x-x^2/(1-2x^2/(1-x-3x^2/(1-2x-4x^2/(1-3x-5x^2/(1-.... (continued fraction). - _Paul Barry_, Apr 23 2009",
				"Let A be the upper Hessenberg matrix of order n defined by: A[i,i-1]=-1, A[i,j]=binomial(j-1,i-1), (i\u003c=j), and A[i,j]=0 otherwise. Then, for n\u003e=1, a(n)=(-1)^(n)charpoly(A,2). - _Milan Janjic_, Jul 08 2010",
				"G.f.: -1/U(0)  where U(k) = x*k - 1 - x - x^2*(k+1)/U(k+1); (continued fraction, 1-step). - _Sergei N. Gladkovskii_, Sep 28 2012",
				"G.f.: 1/G(0) where G(k) = 1 + 2*x/(1 + 1/(1 - 2*x*(k+1)/G(k+1))); (continued fraction, 3-step). - _Sergei N. Gladkovskii_, Nov 23 2012",
				"G.f.: G(0)/(1+3*x) where G(k) = 1 - 2*x*(k+1)/((2*k+1)*(2*x*k-2*x-1) - x*(2*k+1)*(2*k+3)*(2*x*k-2*x-1)/(x*(2*k+3) - 2*(k+1)*(2*x*k-x-1)/G(k+1) )); (recursively defined continued fraction). - _Sergei N. Gladkovskii_, Dec 19 2012",
				"From _Sergei N. Gladkovskii_, Feb 13 2013: (Start)",
				"Conjecture: if the e.g.f. is E(x)= exp( exp(x) -1 + p*x) then",
				"g.f.: (x+1-p*x)/x/(G(0)-x) - 1/x where G(k) = 2*x + 1 - p*x - x*k + x*(x*k - x - 1 + p*x)/G(k+1); (continued fraction).",
				"So, for this sequence (p=-2), g.f.: (3*x+1)/x/( G(0)-x ) - 1/x where G(k) = 4*x + 1 - x*k + x*(x*k - 3*x - 1)/G(k+1);",
				"(End)",
				"G.f.: 1/Q(0), where Q(k) = 1 + 2*x - x/(1 - x*(k+1)/Q(k+1)); (continued fraction). - _Sergei N. Gladkovskii_, Apr 22 2013",
				"a(0) = 1; a(n) = -2 * a(n-1) + Sum_{k=0..n-1} binomial(n-1,k) * a(k). - _Ilya Gutkovskiy_, Jul 30 2021"
			],
			"example": [
				"G.f.: 1 - 1*x + 2*x^2 - 3*x^3 + 7*x^4 - 10*x^5 + 31*x^6 - 21*x^7 + 204*x^8 + 307*x^9 + 2811*x^10 + 12100*x^11 + 74053*x^12 + 432211*x^13 + ..."
			],
			"mathematica": [
				"Table[ Sum[ (-2)^(n - k) Binomial[n, k] BellB[k], {k, 0, n}], {n, 0, 50}] (* _Karol A. Penson_ and _Olivier Gérard_, Oct 22 2007 *)"
			],
			"xref": [
				"Cf. A000110, A000296, A005493, A124311, A126390, A153732."
			],
			"keyword": "sign",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_, Aug 04 2007",
			"ext": [
				"More terms from _Karol A. Penson_ and _Olivier Gérard_, Oct 22 2007"
			],
			"references": 10,
			"revision": 51,
			"time": "2021-07-31T02:33:45-04:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}