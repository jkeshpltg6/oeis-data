{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A067182",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 67182,
			"data": "0,1,2,3,13,5,-1,34,8,144,55",
			"name": "Smallest Fibonacci number with digit sum n, or -1 if no such number exists.",
			"comment": [
				"a(n) = Fibonacci(k) where k is the index of the first occurrence of n in A004090, or -1 if n never appears there. - _N. J. A. Sloane_, Dec 26 2016",
				"Starting at n = 11, the terms a(11), a(12), ... are probably -1, -1, -1, 4181, -1, -1, 89, -1, 2584, 10946, 317811, 1597, 514229, 987, -1, -1, 46368, 28657, 196418, 2178309, 1346269, -1, 701408733, 3524578, 9227465, -1, 5702887, -1, -1, -1, 433494437, -1, 63245986, 39088169, -1, 267914296, -1, ... However, these -1's are only conjectural.",
				"It appears that 0.9*n \u003c A004090(n) \u003c n for all but a few small n: In the range [0..10^5] the slope of A004090 is roughly 0.93. I conjecture that A004090(n) - n has 92 as its maximum, at n = 2619. This would prove that the given -1's are correct. - _M. F. Hasler_, Dec 26 2016",
				"Joseph Myers and _Don Reble_ proved that a(6) = -1 as follows (cf. Links): If the sum of digits of N is less than 9, then it equals the sum of digits of N modulo 10^k-1 for any k \u003e 0. Now A000045 mod 9999 has period 600 (cf. A001175), and has no term equal to 6. - _M. F. Hasler_, Dec 28 2016"
			],
			"link": [
				"Hans Havermann, \u003ca href=\"http://chesswanks.com/seq/b067182.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (Note that this is not what would be called a b-file in the OEIS, since the -1 entries except for the first are conjectural, and a b-file may not contain conjectured values. - _N. J. A. Sloane_, Feb 05 2017)",
				"Hans Havermann, \u003ca href=\"/A067182/a067182.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (the -1's, except for the first, are only conjectural). [Cached copy, with permission]",
				"Joseph Myers and Don Reble, \u003ca href=\"http://list.seqfan.eu/pipermail/seqfan/2016-December/017192.html\"\u003eRe: What are the possible digit-sums for Fibonacci numbers?\u003c/a\u003e (click \"next\" to see the second post), SeqFan list, Dec 27 2016"
			],
			"formula": [
				"a(n) = min { A000045(k) | A004090(k) = n } U { -1 }. - _M. F. Hasler_, Dec 26 2016"
			],
			"example": [
				"a(14) = 4181, as it is the smallest Fibonacci number with a digit sum of 14."
			],
			"mathematica": [
				"Take[#, 48] \u0026@ Function[w, Function[t, {0}~Join~ReplacePart[t, Flatten@ Map[{#2 -\u003e #1} \u0026 @@ # \u0026, w]]]@ ConstantArray[0, w[[-1, -1]]]]@ Map[First, SplitBy[#, Last]] \u0026@ SortBy[#, Last] \u0026@ Table[{#, Total@ IntegerDigits@ #} \u0026@ Fibonacci@ n, {n, 10^4}] (* _Michael De Vlieger_, Dec 28 2016 *)",
				"a = 0; b = c = 1; t[_] = -1; While[a \u003c 10^1000, s = Plus @@ IntegerDigits[a]; If[s \u003c 101 \u0026\u0026 t[s] == -1, t[s] = a]; a = b; b = c; c = a + b]; Array[t, 48, 0] (* _Robert G. Wilson v_, Jan 25 2017 *)"
			],
			"program": [
				"(PARI) A067182(n,a=1,b=-1)=-!for(k=0,n+99,sumdigits(a=b+b=a)==n\u0026\u0026return(a)) \\\\ _M. F. Hasler_, Dec 28 2016"
			],
			"xref": [
				"Cf. A004090, A020995, A007953, A000045, A001175."
			],
			"keyword": "easy,sign,base",
			"offset": "0,3",
			"author": "_Amarnath Murthy_, Jan 09 2002",
			"ext": [
				"More terms from _Frank Ellermann_, Jan 18 2002",
				"More terms from _Jason Earls_, May 27 2002",
				"Edited by _M. F. Hasler_, Dec 26 2016 and Dec 28 2016",
				"Edited (including changing the value of a(n) for when no k exists from 0 to -1) by _N. J. A. Sloane_, Dec 29 2016 and Feb 05 2017"
			],
			"references": 1,
			"revision": 81,
			"time": "2018-07-12T08:55:24-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}