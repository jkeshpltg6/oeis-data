{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002421",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2421,
			"id": "M4058 N1683",
			"data": "1,-6,6,4,6,12,28,72,198,572,1716,5304,16796,54264,178296,594320,2005830,6843420,23571780,81880920,286583220,1009864680,3580429320,12765008880,45741281820,164668614552,595340375688,2160865067312,7871722745208,28772503827312",
			"name": "Expansion of (1-4*x)^(3/2) in powers of x.",
			"comment": [
				"Terms that are not divisible by 12 have indices in A019469. - _Ralf Stephan_, Aug 26 2004",
				"From _Ralf Steiner_, Apr 06 2017: (Start)",
				"By analytic continuation to the entire complex plane there exist regularized values for divergent sums such as:",
				"Sum_{k\u003e=0} a(k)^2/8^k = 2F1(-3/2,-3/2,1,2).",
				"Sum_{k\u003e=0} a(k) / 2^k = -i. (End)"
			],
			"reference": [
				"A. Fletcher, J. C. P. Miller, L. Rosenhead and L. J. Comrie, An Index of Mathematical Tables. Vols. 1 and 2, 2nd ed., Blackwell, Oxford and Addison-Wesley, Reading, MA, 1962, Vol. 1, p. 55.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence).",
				"T. N. Thiele, Interpolationsrechnung. Teubner, Leipzig, 1909, p. 164."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A002421/b002421.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/A000984/a000984.pdf\"\u003eNotes on A984 and A2420-A2424\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Sum_{m=0..n} binomial(n, m)*K_m(4), where K_m(x) = K_m(n, 2, x) is a Krawtchouk polynomial. - Alexander Barg (abarg(AT)research.bell-labs.com)",
				"a(n) ~ (3/4)*Pi^(-1/2)*n^(-5/2)*2^(2*n)*(1 + 15/8*n^-1 + ...). - Joe Keane (jgk(AT)jgk.org), Nov 22 2001",
				"From _Ralf Stephan_, Mar 11 2004: (Start)",
				"a(n) = 12*(2*n-4)! /(n!*(n-2)!), n \u003e 1.",
				"a(n) = 12*Cat(n-2)/n = 2(Cat(n-1) - 4*Cat(n-2)), in terms of Catalan numbers (A000108).",
				"Terms that are not divisible by 12 have indices in A019469. (End)",
				"Let rho(x)=(1/Pi)*(x*(4-x))^(3/2), then for n \u003e= 4, a(n) = Integral_{x=0..4} (x^(n-4) *rho(x)). - Groux Roland, Mar 16 2011",
				"G.f.: (1-4*x)^(3/2) = 1 - 6*x + 12*x^2/(G(0) + 2*x); G(k) = (4*x+1)*k-2*x+2-2*x*(k+2)*(2*k+1)/G(k+1); for -1/4 \u003c= x \u003c 1/4, otherwise G(0)= 2*x; (continued fraction). - _Sergei N. Gladkovskii_, Dec 05 2011",
				"G.f.: 1/G(0) where G(k)= 1 + 4*x*(2*k+1)/(1 - 1/(1 + (2*k+2)/G(k+1))); (continued fraction, 3-step). - _Sergei N. Gladkovskii_, Nov 18 2012",
				"G.f.: G(0)/2, where G(k)= 2 + 2*x*(2*k-3)*G(k+1)/(k+1). - _Sergei N. Gladkovskii_, Jun 06 2013 [Edited by _Michael Somos_, Dec 04 2013]",
				"0 = a(n+2) * (a(n+1) - 14*a(n)) + a(n+1) * (6*a(n+1) + 16*a(n)) for all n in Z. - _Michael Somos_, Dec 04 2013",
				"A232546(n) = 3^n * a(n). - _Michael Somos_, Dec 04 2013",
				"G.f.: hypergeometric1F0(-3/2;;4*x). - _R. J. Mathar_, Aug 09 2015",
				"a(n) = 3*4^(n-1)*Gamma(-3/2+n)/(sqrt(Pi)*Gamma(1+n)). - _Peter Luschny_, Dec 14 2015",
				"From _Ralf Steiner_, Apr 06 2017: (Start)",
				"Sum_{k\u003e=0} a(k)/4^k = 0.",
				"Sum_{k\u003e=0} a(k)^2/16^k = 32/(3*Pi).",
				"Sum_{k\u003e=0} a(k)^2*(k/8)/16^k = 1/Pi.",
				"Sum_{k\u003e=0} a(k)^2*(-k/24+1/8)/16^k = 1/Pi.",
				"Sum_{k\u003e=0} a(k-1)^2*(k-1/4)/16^k = 1/Pi.",
				"Sum_{k\u003e=0} a(k-1)^2*(2k-2)/16^k = 1/Pi.(End)",
				"D-finite with recurrence: n*a(n) +2*(-2*n+5)*a(n-1)=0. - _R. J. Mathar_, Feb 20 2020"
			],
			"example": [
				"G.f. = 1 - 6*x + 6*x^2 + 4*x^3 + 6*x^4 + 12*x^5 + 28*x^6 + 72*x^7 + 198*x^8 + 572*x^9 + ..."
			],
			"maple": [
				"A002421 := n -\u003e 3*4^(n-1)*GAMMA(-3/2+n)/(sqrt(Pi)*GAMMA(1+n)):",
				"seq(A002421(n), n=0..29); # _Peter Luschny_, Dec 14 2015"
			],
			"mathematica": [
				"CoefficientList[Series[(1-4x)^(3/2),{x,0,40}],x] (* _Vincenzo Librandi_, Jun 11 2012",
				"a[n_]:= Binomial[ 3/2, n] (-4)^n; (* _Michael Somos_, Dec 04 2013 *)",
				"a[n_]:= SeriesCoefficient[(1-4x)^(3/2), {x, 0, n}]; (* _Michael Somos_, Dec 04 2013 *)"
			],
			"program": [
				"(MAGMA) [1,-6] cat [12*Catalan(n-2)/n: n in [2..30]]; // _Vincenzo Librandi_, Jun 11 2012",
				"(PARI) {a(n) = binomial( 3/2, n) * (-4)^n}; /* _Michael Somos_, Dec 04 2013 */",
				"(PARI) {a(n) = if( n\u003c0, 0, polcoeff( (1 - 4*x + x * O(x^n))^(3/2), n))}; /* _Michael Somos_, Dec 04 2013 */",
				"(Sage) ((1-4*x)^(3/2)).series(x, 40).coefficients(x, sparse=False) # _G. C. Greubel_, Jul 03 2019",
				"(GAP) Concatenation([1], List([1..40], n-\u003e 12*Factorial(2*n-4) /( Factorial(n)*Factorial(n-2)) )) # _G. C. Greubel_, Jul 03 2019"
			],
			"xref": [
				"Cf. A007054, A004001, A002420, A002422, A002423, A002424.",
				"Cf. A000257, A071721, A071724, A085687."
			],
			"keyword": "sign,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 13,
			"revision": 86,
			"time": "2020-02-20T03:17:28-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}