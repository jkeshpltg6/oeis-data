{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A214681",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 214681,
			"data": "1,1,1,1,5,6,7,1,1,5,11,6,13,7,5,1,17,6,19,5,7,11,23,6,25,13,1,7,29,30,31,1,11,17,35,36,37,19,13,5,41,42,43,11,5,23,47,6,49,25,17,13,53,6,55,7,19,29,59,30,61,31,7,1,65,66,67,17,23,35,71,36",
			"name": "a(n) is obtained from n by removing factors of 2 and 3 that do not contribute to a factor of 6.",
			"comment": [
				"In this sequence, the number 6 exhibits some characteristics of a prime number since we have removed extraneous 2's and 3's from the prime factorizations of numbers."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A214681/b214681.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = n*6^(v_6(n))/(2^(v_2(n))*3^(v_3(n)), where v_k(n) is the k-adic valuation of n, that is v_k(n) gives the largest power of k, a, such that k^a divides n."
			],
			"example": [
				"For n=4, v_2(4)=2, v_3(4)=0, and v_6(4)=0, so a(4) = 4*1/(4*1) = 1.",
				"For n=36, v_2(36)=2, v_3(36)=2, and v_6(36)=2, so a(36) = 36*36/(4*9) = 36.",
				"For n=17, a(17) = 17 since 17 has no factors of 6, 2 or 3."
			],
			"maple": [
				"a:= proc(n) local i, m, r; m:=n;",
				"      for i from 0 while irem(m, 6, 'r')=0 do m:=r od;",
				"      while irem(m, 2, 'r')=0 do m:=r od;",
				"      while irem(m, 3, 'r')=0 do m:=r od;",
				"      m*6^i",
				"    end:",
				"seq(a(n), n=1..100);  # _Alois P. Heinz_, Jul 04 2013"
			],
			"mathematica": [
				"With[{v = IntegerExponent}, a[n_] := n*6^v[n, 6]/2^v[n, 2]/3^v[n, 3]; Array[a, 100]] (* _Amiram Eldar_, Dec 09 2020 *)"
			],
			"program": [
				"(Sage)",
				"n=100 #change n for more terms",
				"C=[]",
				"b=6",
				"P = factor(b)",
				"for i in [1..n]:",
				"    prod = 1",
				"    for j in range(len(P)):",
				"        prod = prod * ((P[j][0])^(Integer(i).valuation(P[j][0])))",
				"    C.append((b^(Integer(i).valuation(b)) * i) /prod)"
			],
			"xref": [
				"Cf. A214682, A214685."
			],
			"keyword": "easy,nonn",
			"offset": "1,5",
			"author": "_Tom Edgar_, Jul 25 2012",
			"references": 3,
			"revision": 35,
			"time": "2020-12-09T01:13:33-05:00",
			"created": "2012-07-26T14:14:46-04:00"
		}
	]
}