{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A192874",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 192874,
			"data": "1,0,4,6,26,72,246,774,2532,8150,26434,85448,276654,895054,2896788,9373678,30334682,98163784,317666758,1027987894,3326644036,10765237670,34837054674,112735054856,364818336766,1180576879422",
			"name": "Constant term in the reduction by (x^2 -\u003e x + 1) of the polynomial p(n,x) given in Comments.",
			"comment": [
				"The polynomial p(n,x) is defined by p(0,x) = 1, p(1,x) = x, and p(n,x) = x*p(n-1,x) + 2*(x^2)*p(n-1,x) + 1.  See A192872."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A192874/b192874.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,6,-5,-6,4)."
			],
			"formula": [
				"a(n) = 2*a(n-1) + 6*a(n-2) - 5*a(n-3) - 6*a(n-4) + 4*a(n-5).",
				"G.f.: (x^2-x+1)*(4*x^2+x-1) / ( (x-1)*(x^2-x-1)*(4*x^2+2*x-1) ). - _R. J. Mathar_, May 06 2014"
			],
			"mathematica": [
				"q = x^2; s = x + 1; z = 26;",
				"p[0, x_] := 1; p[1, x_] := x;",
				"p[n_, x_] := p[n - 1, x]*x + 2*p[n - 2, x]*x^2 + 1;",
				"Table[Expand[p[n, x]], {n, 0, 7}]",
				"reduce[{p1_, q_, s_, x_}] := FixedPoint[(s PolynomialQuotient @@ #1 + PolynomialRemainder @@ #1 \u0026)[{#1, q, x}] \u0026, p1]",
				"t = Table[reduce[{p[n, x], q, s, x}], {n, 0, z}];",
				"u1 = Table[Coefficient[Part[t, n], x, 0], {n, 1, z}] (* A192874 *)",
				"u2 = Table[Coefficient[Part[t, n], x, 1], {n, 1, z}] (* A192875 *)",
				"LinearRecurrence[{2,6,-5,-6,4}, {1,0,4,6,26}, 30] (* _G. C. Greubel_, Jan 08 2019 *)"
			],
			"program": [
				"(PARI) my(x='x+O('x^30)); Vec((x^2-x+1)*(4*x^2+x-1)/((x-1)*(x^2-x-1)*( 4*x^2+2*x-1))) \\\\ _G. C. Greubel_, Jan 08 2019",
				"(MAGMA) I:=[1,0,4,6,26]; [n le 5 select I[n] else 2*Self(n-1)+6*Self(n-2) -5*Self(n-3)-6*Self(n-4)+4*Self(n-5): n in [1..30]]; // _G. C. Greubel_, Jan 08 2019",
				"(Sage) ((x^2-x+1)*(4*x^2+x-1)/((x-1)*(x^2-x-1)*( 4*x^2+2*x-1))).series(x, 30).coefficients(x, sparse=False) # _G. C. Greubel_, Jan 08 2019",
				"(GAP) a:=[1,0,4,6,26];; for n in [6..30] do a[n]:=2*a[n-1]+6*a[n-2] - 5*a[n-3]-6*a[n-4]+4*a[n-5]; od; a; # _G. C. Greubel_, Jan 08 2019"
			],
			"xref": [
				"Cf. A192872, A192875."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Clark Kimberling_, Jul 11 2011",
			"references": 3,
			"revision": 14,
			"time": "2019-01-22T03:11:29-05:00",
			"created": "2011-07-12T12:15:46-04:00"
		}
	]
}