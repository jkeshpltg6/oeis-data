{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A326267",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 326267,
			"data": "1,0,2,9,112,1585,28776,637189,16725136,510567201,17872335280,709140147661,31587858029256,1566848912178433,85946841477148120,5181755738815497885,341518373893520290336,24487048531932288330049,1901736820850206799775456,159346732413596476282342669,14353534016946934243615057240,1385413661318328442225223922081",
			"name": "E.g.f.: Sum_{n\u003e=0} (W(x)^n - 1)^n * x^n / n!, where W(x) = exp(x*W(x)) = LambertW(-x)/(-x).",
			"comment": [
				"More generally, the following sums are equal:",
				"(1) Sum_{n\u003e=0} (p + q^n)^n * r^n/n!,",
				"(2) Sum_{n\u003e=0} q^(n^2) * exp(p*q^n*r) * r^n/n!;",
				"here, q = LambertW(-x)/(-x) with p = -1, r = x."
			],
			"formula": [
				"Let W(x) = LambertW(-x)/(-x), then e.g.f. A(x) equals the following sums.",
				"(1) Sum_{n\u003e=0} (W(x)^n - 1)^n * x^n / n!.",
				"(2) Sum_{n\u003e=0} W(x)^(n^2) * exp( -W(x)^n * x ) / n!."
			],
			"example": [
				"E.g.f.: A(x) = 1 + 2*x^2/2! + 9*x^3/3! + 112*x^4/4! + 1585*x^5/5! + 28776*x^6/6! + 637189*x^7/7! + 16725136*x^8/8! + 510567201*x^9/9! + 17872335280*x^10/10! + ...",
				"such that",
				"A(x) = 1 + (W(x) - 1)*x + (W(x)^2 - 1)^2*x^2/2! + (W(x)^3 - 1)^3*x^3/3! + (W(x)^4 - 1)^4*x^4/4! + (W(x)^5 - 1)^5*x^5/5! + (W(x)^6 - 1)^6*x^6/6! + (W(x)^7 - 1)^7*x^7/7! + (W(x)^8 - 1)^8*x^8/8! + ...",
				"also",
				"A(x) = exp(-x) + W(x)*exp(-W(x)*x)*x + W(x)^4*exp(-W(x)^2*x)*x^2/2! + W(x)^9*exp(-W(x)^3*x)*x^3/3! + W(x)^16*exp(-W(x)^4*x)*x^4/4! + W(x)^25*exp(-W(x)^5*x)*x^5/5! + W(x)^36*exp(-W(x)^6*x)*x^6/6! + ...",
				"where W(x) = exp(x*W(x)) = LambertW(-x)/(-x) begins",
				"W(x) = 1 + x + 3*x^2/2! + 16*x^3/3! + 125*x^4/4! + 1296*x^5/5! + 16807*x^6/6! + 262144*x^7/7! + 4782969*x^8/8! + 100000000*x^9/9! + ... + (n+1)^(n-1)*x^n/n! + ...",
				"RELATED SERIES.",
				"Note that W(x)^n equals",
				"W(x)^n = Sum_{k\u003e=0} n * (n + k)^(k-1) * x^k/k!",
				"and so",
				"W(x)^(n^2) = Sum_{k\u003e=0} n^2 * (n^2 + k)^(k-1) * x^k/k!."
			],
			"program": [
				"(PARI) /* E.g.f.: Sum_{n\u003e=0} (W(x)^n - 1)^n * x^n / n! */",
				"{a(n) = my(W = 1/x*serreverse(x*exp(-x +x*O(x^n))));",
				"n! * polcoeff( sum(m=0, n, (W^m - 1)^m * x^m / m!), n)}",
				"for(n=0, 30, print1(a(n), \", \"))",
				"(PARI) /* E.g.f.: Sum_{n\u003e=0} W(x)^(n^2) * exp( -W(x)^n * x ) / n! */",
				"{a(n) = my(W = 1/x*serreverse(x*exp(-x +x*O(x^n))));",
				"n! * polcoeff( sum(m=0, n, W^(m^2) * exp(-W^m*x +x*O(x^n)) * x^m / m!), n)}",
				"for(n=0, 30, print1(a(n), \", \"))"
			],
			"xref": [
				"Cf. A326266, A326268."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Paul D. Hanna_, Jun 29 2019",
			"references": 2,
			"revision": 8,
			"time": "2019-06-29T12:21:39-04:00",
			"created": "2019-06-29T12:06:49-04:00"
		}
	]
}