{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A343636",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 343636,
			"data": "6,87,0,0,6057,0,91257,526557,12710877,2054787,10234947,6222567,33407547,6589467,44514957,587445747,113139567,816858057,210160347,896654097,1523730387,1048338207,1309977027,2095519077,3453564567,1143089517,735261237,723151317,99411447,77882127",
			"name": "10^n + a(n) is the smallest (n+1)-digit initial member of a prime sextuplet, or a(n) = 0 if no such number exists.",
			"comment": [
				"The smallest (n+1)-digit sextuplet is given as 10^n + a(n) + {0, 4, 6, 10, 12, 16}.",
				"a(0) = 6 and a(1) = 87, i.e., n = 0 and n = 1, are the only cases where larger members of the sextuplet have one digit more than the smallest member of the sextuplet. These terms would be zero if all members of the sextuplet ought to have the same number of digits. We require only the first member to have the given number of digits, in order to include these two nontrivial terms.",
				"Numerical evidence strongly suggests the conjecture that 0 \u003c a(n) \u003c 10^n for all n \u003e 5, but not even the existence of infinitely many prime sextuplets is proved.",
				"Values for n \u003e 300 found by _Norman Luhn_: a(399) = 33756090918084087, a(499) = 464261549124325347 (October 2020), a(599) = 314360191056418137 (June 2021)."
			],
			"link": [
				"M. F. Hasler, \u003ca href=\"/A343636/b343636.txt\"\u003eTable of n, a(n) for n = 0..299\u003c/a\u003e",
				"Norman Luhn, \u003ca href=\"https://mathematikalpha.de/primzahltupel\"\u003ePrimzahltupel, prime k-tuple: Smallest-n-digit-prime-sexuplets\u003c/a\u003e, on mathematikalpha.de, 2020"
			],
			"example": [
				"a(0) = 6 because A022008(1) = 7 = 10^0 + 6 is the start of the smallest prime sextuplet, starting with a 1-digit prime.",
				"a(1) = 87 because A022008(2) = 97 = 10^1 + 87 is the start of the smallest prime sextuplet starting with a 2-digit prime.",
				"a(n) = 0 iff n = 2, 3 or 5, because there is no prime sextuplet with members having 3, 4 or 6 digits.",
				"a(4) = 6057 because A022008(3) = 16057 = 10^4 + 6057 is the start of the smallest prime sextuplet made of 5-digit primes."
			],
			"program": [
				"(PARI) apply( {A343636(n,D=[16,12,10,6,4])=forprime(p=10^n, 10^(n+1), foreach(D, d, ispseudoprime(p+d)||next(2));return(p-10^n))}, [0..11]) \\\\ For illustration; unoptimized code, very fast only for n \u003c 12."
			],
			"xref": [
				"Cf. A022008 (start of prime sextuplets)."
			],
			"keyword": "nonn,base,hard",
			"offset": "0,1",
			"author": "_M. F. Hasler_, Jul 13 2021",
			"references": 3,
			"revision": 18,
			"time": "2021-07-21T09:48:19-04:00",
			"created": "2021-07-13T23:48:43-04:00"
		}
	]
}