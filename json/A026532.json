{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A026532",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 26532,
			"data": "1,3,6,18,36,108,216,648,1296,3888,7776,23328,46656,139968,279936,839808,1679616,5038848,10077696,30233088,60466176,181398528,362797056,1088391168,2176782336,6530347008,13060694016,39182082048",
			"name": "Ratios of successive terms are 3, 2, 3, 2, 3, 2, 3, 2 ...",
			"comment": [
				"Preface the series with a 1: (1, 1, 3, 6, 18, 36,...); then the next term in the series = (1, 1, 3, 6,...) dot (1, 2, 1, 2,...). Example: 36 = (1, 1, 3, 6, 18) dot (1, 2, 1, 2, 1) = (1 + 2 + 3 + 12 + 18). - _Gary W. Adamson_, Apr 18 2009",
				"Partial products of A176059. - _Reinhard Zumkeller_, Apr 04 2012"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A026532/b026532.txt\"\u003eTable of n, a(n) for n = 1..700\u003c/a\u003e",
				"José L. Ramírez, \u003ca href=\"http://ami.ektf.hu/uploads/papers/finalpdf/AMI_42_from83to92.pdf\"\u003eBi-periodic incomplete Fibonacci sequences\u003c/a\u003e, Annales Mathematicae et Informaticae 42 (2013), 83-92. See the 1st column of Table 1 on p. 85.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,6)."
			],
			"formula": [
				"a(n) = T(n, 0) + T(n, 1) + ... + T(n, 2n-2), T given by A026519.",
				"From _Benoit Cloitre_, Nov 14 2003: (Start)",
				"a(n) = (1/2)*(5+(-1)^n)*a(n-1) for n\u003e1, a(1) = 1.",
				"a(n) = (1/4)*(3-(-1)^n)*6^floor(n/2).  (End)",
				"From _Ralf Stephan_, Feb 03 2004: (Start)",
				"G.f.: x*(1+3*x)/(1-6*x^2).",
				"a(n+2) = 6*a(n). (End)",
				"a(n+3) = a(n+2)*a(n+1)/a(n). - _Reinhard Zumkeller_, Mar 04 2011",
				"a(n) = (1/2)*6^((n-2)/2)*(3*(1+(-1)^n) + sqrt(6)*(1-(-1)^n)). - _G. C. Greubel_, Dec 21 2021"
			],
			"mathematica": [
				"FoldList[(2 + Boole[EvenQ@ #2]) #1 \u0026, Range@ 28] (* or *)",
				"CoefficientList[Series[x*(1+3x)/(1-6x^2), {x,0,31}], x] (* _Michael De Vlieger_, Aug 02 2017 *)",
				"LinearRecurrence[{0,6},{1,3},30] (* _Harvey P. Dale_, Jul 11 2018 *)"
			],
			"program": [
				"(MAGMA) [(1/4)*(3-(-1)^n)*6^Floor(n/2) : n in [1..30]]; // _Vincenzo Librandi_, Jun 08 2011",
				"(Haskell)",
				"a026532 n = a026532_list !! (n-1)",
				"a026532_list = scanl (*) 1 $ a176059_list",
				"-- _Reinhard Zumkeller_, Apr 04 2012",
				"(PARI) a(n)=if(n%2,3,1)*6^(n\\2) \\\\ _Charles R Greathouse IV_, Jul 02 2013",
				"(Python)",
				"def a(n): return (3 if n%2 else 1)*6**(n//2)",
				"print([a(n) for n in range(31)]) # _Indranil Ghosh_, Aug 02 2017",
				"(Sage) [(1/2)*6^((n-2)/2)*(3*(1+(-1)^n) + sqrt(6)*(1-(-1)^n)) for n in (1..30)] # _G. C. Greubel_, Dec 21 2021"
			],
			"xref": [
				"Cf. A026519, A026520, A026521, A026522, A026523, A026524, A026525, A026526, A026527, A026528, A026529, A026530, A026531, A026533, A026534, A027262, A027263, A027264, A027265, A027266.",
				"Cf. A026534, A026549, A176059, A208131.",
				"Cf. A038730, A038792, and A134511 for incomplete Fibonacci sequences, and A324242 for incomplete Lucas sequences."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Clark Kimberling_",
			"ext": [
				"New definition from _Ralf Stephan_, Dec 01 2004",
				"Offset changed from 0 to 1 by _Vincenzo Librandi_, Jun 08 2011"
			],
			"references": 21,
			"revision": 63,
			"time": "2021-12-22T07:04:12-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}