{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A335261",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 335261,
			"data": "1,1,1,1,4,2,4,1,4,2,4,1,8,4,8,2,8,4,8,1,8,4,8,2,8,4,8,1,8,4,8,2,8,4,8,1,8,4,8,2,8,4,8,1,8,4,8,2,8,4,8,1,8,4,8,2,8,4,8,1,16,8,16,4,16,8,16,2,16,8,16,4,16,8,16,1,16,8,16,4,16,8,16",
			"name": "Irregular triangle S(n,k) = denominators of k*A002110(n)/A005867(n) for 1 \u003c= k \u003c= A005867(n).",
			"comment": [
				"Alternatively, denominators of k*A060753(n)/A038110(n) for 1 \u003c= k \u003c= A005867(n).",
				"Let m = A038110(n). For row n, the primitive denominators d | m.",
				"The mean of row n is related to the mean of row n of A309497: A060753(n+1)/(A038110(n+1)*2)) = 35/16 = Mean(A309497(n))/A038110(n+1)."
			],
			"formula": [
				"T(n,k) = m/GCD(k (mod m), m) with m = A038110(n).",
				"Row lengths: A005867(n)."
			],
			"example": [
				"Table begins:",
				"    1;",
				"    1;",
				"    1, 1;",
				"    4, 2, 4, 1, 4, 2, 4, 1;",
				"    8, 4, 8, 2, 8, 4, 8, 1, ..., 8, 1;",
				"    ...",
				"Row n = 4 contains the denominators of (35/8)*k for 1 \u003c= k \u003c= A005867(4): 35/8, 35/4, 105/8, 35/2, 175/8, 105/4, 245/8, 35, 315/8, 175/4, 385/8, 105/2, 455/8, 245/4, 525/8, 70, 595/8, 315/4, 665/8, 175/2, 735/8, 385/4, 805/8, 105, 875/8, 455/4, 945/8, 245/2, 1015/8, 525/4, 1085/8, 140, 1155/8, 595/4, 1225/8, 315/2, 1295/8, 665/4, 1365/8, 175, 1435/8, 735/4, 1505/8, 385/2, 1575/8, 805/4, 1645/8, 210.",
				"The mean of row 4: A060753(4+1)/(A038110(4+1)*2))* (A005867(4)+1) = (35/(8*2))*(48+1) = (35/16)*49 = 1715/16."
			],
			"mathematica": [
				"Table[Denominator[P Range[EulerPhi[P]]/EulerPhi[P]], {P, FoldList[Times, Prime@ Range@ 5]}] (* or, more efficiently for larger datasets: *)",
				"Flatten@ Block[{nn = 7, s, t}, s = Array[Numerator@ Product[1 - 1/Prime[k], {k, # - 1}] \u0026, nn]; t = Nest[Append[#, #[[-1]] (Prime[Length@ #] - 1)]\u0026, {1}, nn]; MapIndexed[Function[{m, D, i}, PadRight[{}, t[[i]], ReplacePart[ConstantArray[0, m], Flatten@ Map[Function[d, Map[# -\u003e d \u0026, m/d Select[Range[d], GCD[#, d] == 1 \u0026]]], D]]]] @@ {#1, Divisors@ #1, First[#2]} \u0026, s]]",
				"(* or, to generate a single denominator of T(n,k) *)",
				"f[n_, k_] := #/GCD[#, Mod[k, #]] \u0026@ Numerator@ Product[1 - 1/Prime[i], {i, n - 1}]"
			],
			"xref": [
				"Cf. A000010, A002110, A005867, A038110, A060753, A309497, A335260."
			],
			"keyword": "nonn,frac,easy,tabf",
			"offset": "1,5",
			"author": "_Michael De Vlieger_, _Jamie Morken_, May 30 2020",
			"references": 1,
			"revision": 6,
			"time": "2020-05-31T21:59:47-04:00",
			"created": "2020-05-31T21:59:47-04:00"
		}
	]
}