{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A329348",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 329348,
			"data": "1,1,1,2,1,2,1,1,1,2,1,4,1,2,6,2,1,2,1,4,6,2,1,3,2,2,1,4,1,5,1,1,6,2,8,4,1,2,6,1,1,1,1,4,1,2,1,1,1,4,6,4,1,2,4,8,6,2,1,3,1,2,3,2,13,12,1,4,6,5,1,3,1,2,5,4,2,12,1,2,1,2,1,2,11,2,6,8,1,2,6,4,6,2,7,2,1,2,10,1,1,12,1,8,4",
			"name": "The least significant nonzero digit in the primorial base expansion of primorial inflation of n, A108951(n).",
			"comment": [
				"Number of occurrences of the least primorial present in the greedy sum of primorials adding to A108951(n).",
				"The greedy sum is also the sum with the minimal number of primorials, used for example in the primorial base representation."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A329348/b329348.txt\"\u003eTable of n, a(n) for n = 1..65537\u003c/a\u003e",
				"\u003ca href=\"/index/Pri#prime_indices\"\u003eIndex entries for sequences computed from indices in prime factorization\u003c/a\u003e",
				"\u003ca href=\"/index/Pri#primorialbase\"\u003eIndex entries for sequences related to primorial base\u003c/a\u003e",
				"\u003ca href=\"/index/Pri#primorial_numbers\"\u003eIndex entries for sequences related to primorial numbers\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A067029(A324886(n)) = A276088(A108951(n)).",
				"a(n) \u003c= A324888(n).",
				"From _Antti Karttunen_, Jan 15-17 2020: (Start)",
				"a(n) = A331188(n) mod A117366(n).",
				"a(n) = A001511(A246277(A324886(n))).",
				"(End)"
			],
			"example": [
				"For n = 24 = 2^3 * 3, A108951(24) = A034386(2)^3 * A034386(3) = 2^3 * 6 = 48 = 1*30 + 3*6, and as the factor of the least primorial in the sum is 3, we have a(24) = 3."
			],
			"program": [
				"(PARI)",
				"A034386(n) = prod(i=1, primepi(n), prime(i));",
				"A108951(n) = { my(f=factor(n)); prod(i=1, #f~, A034386(f[i, 1])^f[i, 2]) };  \\\\ From A108951",
				"A276088(n) = { my(e=0, p=2); while(n \u0026\u0026 !(e=(n%p)), n = n/p; p = nextprime(1+p)); (e); };",
				"A329348(n) = A276088(A108951(n));",
				"(PARI)",
				"A276086(n) = { my(m=1, p=2); while(n, m *= (p^(n%p)); n = n\\p; p = nextprime(1+p)); (m); };",
				"A324886(n) = A276086(A108951(n));",
				"A067029(n) = if(1==n, 0, factor(n)[1, 2]); \\\\ From A067029",
				"A329348(n) = A067029(A324886(n));",
				"(PARI)",
				"A002110(n) = prod(i=1, n, prime(i));",
				"A329348(n) = if(1==n, n, my(f=factor(n), p=nextprime(1+vecmax(f[, 1]))); prod(i=1, #f~, A002110(primepi(f[i, 1]))^(f[i, 2]-(#f~==i)))%p); \\\\ _Antti Karttunen_, Jan 15 2020"
			],
			"xref": [
				"Cf. A002110, A034386, A067029, A108951, A117366, A276086, A276088, A324886, A324888, A329040, A329345, A329343, A329344, A329349, A331188, A331289, A331290, A331291.",
				"Cf. also A331292 (the next digit to left of this one), A331293."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Antti Karttunen_, Nov 11 2019",
			"ext": [
				"Name changed by _Antti Karttunen_, Jan 17 2020"
			],
			"references": 14,
			"revision": 32,
			"time": "2020-01-17T23:37:24-05:00",
			"created": "2019-11-11T18:44:01-05:00"
		}
	]
}