{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A020497",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 20497,
			"data": "1,3,7,9,13,17,21,27,31,33,37,43,49,51,57,61,67,71,77,81,85,91,95,101,111,115,121,127,131,137,141,147,153,157,159,163,169,177,183,187,189,197,201,211,213,217,227,237,241,247,253,255,265,271,273,279,283,289,301,305",
			"name": "Conjecturally, this is the minimal y such that n primes occur infinitely often among (x+1, ..., x+y), that is, pi(x+y) - pi(x) \u003e= n for infinitely many x.",
			"comment": [
				"a(n) purportedly gives the least k with A023193(k) = n; that is, this sequence should be the \"least inverse\" of A023193.",
				"My web page extends the sequence to rho(305)=2047 and also gives a super-dense occurrence at rho(592)=4333 when pi(4333)=591 - the first known occurrence. - Thomas J Engelsma (tom(AT)opertech.com), Feb 16 2004",
				"Tomás Oliveira e Silva (see link) has a table extending to n = 1000.",
				"The minimal y such that there are n elements of {1, ..., y} with fewer than p distinct elements mod p for all prime p. - _Charles R Greathouse IV_, Jun 13 2013"
			],
			"reference": [
				"R. K. Guy, Unsolved Problems in Number Theory, (2nd edition, Springer, 1994), Section A9."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A020497/b020497.txt\"\u003eTable of n, a(n) for n = 1..672\u003c/a\u003e (from Engelsma's data)",
				"Thomas J. Engelsma, \u003ca href=\"http://www.opertech.com/primes/k-tuples.html\"\u003ePermissible Patterns\u003c/a\u003e.",
				"T. Forbes, \u003ca href=\"http://anthony.d.forbes.googlepages.com/adf.htm\"\u003ePrime k-tuplets\u003c/a\u003e",
				"Daniel M. Gordon and Gene Rodemich, \u003ca href=\"https://dmgordon.org/papers/ants.pdf\"\u003eDense admissible sets\u003c/a\u003e, Proceedings of ANTS III, LNCS 1423 (1998), pp. 216-225.",
				"D. Hensley and I. Richards, \u003ca href=\"http://matwbn.icm.edu.pl/ksiazki/aa/aa25/aa2548.pdf\"\u003ePrimes in intervals\u003c/a\u003e, Acta Arith. 25 (1974), pp. 375-391.",
				"H. L. Montgomery and R. C. Vaughan, \u003ca href=\"http://dx.doi.org/10.1112/S0025579300004708\"\u003eThe large sieve\u003c/a\u003e, Mathematika 20 (1973), pp. 119-134.",
				"Tomás Oliveira e Silva, \u003ca href=\"http://sweet.ua.pt/tos/apc.html\"\u003eAdmissible prime constellations\u003c/a\u003e",
				"Ian Richards, \u003ca href=\"http://projecteuclid.org/euclid.bams/1183535510\"\u003eOn the incompatibility of two conjectures concerning primes; a discussion of the use of computers in attacking a theoretical problem\u003c/a\u003e, Bulletin of the American Mathematical Society 80:3 (1974), pp. 419-438.",
				"H. Smith, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1957-0094314-8\"\u003eOn a generalization of the prime pair problem\u003c/a\u003e, Math. Comp., 11 (1957) 249-254.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/k-TupleConjecture.html\"\u003ePrime k-Tuples Conjecture\u003c/a\u003e."
			],
			"formula": [
				"Prime(floor((n+1)/2)) \u003c= a(n) \u003c prime(n) for large n. See Hensley \u0026 Richards and Montgomery \u0026 Vaughan. - _Charles R Greathouse IV_, Jun 18 2013"
			],
			"xref": [
				"Equals A008407 + 1. First differences give A047947.",
				"Cf. A023193 (prime k-tuplet conjectures), A066081 (weaker binary conjectures)."
			],
			"keyword": "nonn,nice",
			"offset": "1,2",
			"author": "_Robert G. Wilson v_, _Christopher E. Thompson_",
			"ext": [
				"Corrected and extended by _David W. Wilson_"
			],
			"references": 22,
			"revision": 45,
			"time": "2021-11-01T11:56:24-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}