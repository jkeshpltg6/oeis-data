{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A162630",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 162630,
			"data": "2,4,2,6,2,4,8,4,2,6,10,6,2,4,8,12,8,4,2,6,10,14,10,6,2,4,8,12,16,12,8,4,2,6,10,14,18,14,10,6,2,4,8,12,16,20,16,12,8,4,2,6,10,14,18,22,18,14,10,6,2,4,8,12,16,20,24,20,16,12,8,4,2",
			"name": "Triangle read by rows in which row n lists the number of states of the subshells of the n-th shell of the nuclear shell model ordered by energy level in increasing order.",
			"comment": [
				"The list of the spin-orbit coupling of this version of the nuclear shell model starts: 1s_(1/2), 1p_(3/2), 1p_(1/2), 1d_(5/2), 2s_(1/2), 1d_(3/2), 1f_(7/2), 2p_(3/2), 2p_(1/2), etc. The numerators of the fractions are 1, 3, 1, 5, 1, 3, 7, 3, 1, ... then we add 1 to every numerator, so we have this sequence: 2, 4, 2, 6, 2, 4, 8, 4, 2, ... Other sequences that arise from this sequence are A A130517, A210983, A210984. - _Omar E. Pol_, Sep 02 2012"
			],
			"link": [
				"Boris Putievskiy, \u003ca href=\"http://arxiv.org/abs/1212.2732\"\u003eTransformations [of] Integer Sequences And Pairing Functions\u003c/a\u003e, 2012, arXiv:1212.2732 [math.CO], 2012."
			],
			"formula": [
				"a(n) = 2*A130517(n).",
				"From _Boris Putievskiy_, Jan 16 2013: (Start)",
				"a(n) = 2*(|2*A000027(n) - A003056(n)^2 - 2*A003056(n) - 3| + floor((2*A000027(n) - A003056(n)^2 - A003056(n))/(A003056(n) + 3))).",
				"a(n) = 2*(|2*n - t*t - 2*t - 3| + floor((2*n - t*t - t)/(t+3))) where t = floor((-1 + sqrt(8*n-7))/2). (End)"
			],
			"example": [
				"A geometric shell model of the atomic nucleus:",
				"   +---------------------- i ----------------------+",
				"   |   +------------------ h ------------------+   |",
				"   |   |   +-------------- g --------------+   |   |",
				"   |   |   |   +---------- f ----------+   |   |   |",
				"   |   |   |   |   +------ d ------+   |   |   |   |",
				"   |   |   |   |   |   +-- p --+   |   |   |   |   |",
				"   |   |   |   |   |   |   s   |   |   |   |   |   |",
				"   |   |   |   |   |   |   |   |   |   |   |   |   |",
				"   |   |   |   |   |   |       |   |   |   |   |   |",
				"   |   |   |   |   |       2       |   |   |   |   |",
				"   |   |   |   |       4       2       |   |   |   |",
				"   |   |   |       6       2       4       |   |   |",
				"   |   |       8       4       2       6       |   |",
				"   |      10       6       2       4       8       |",
				"      12       8       4       2       6      10",
				"  14      10       6       2       4       8      12",
				"   |   |   |   |   |   |   |   |   |   |   |   |   |",
				"   |   |   |   |   |   |   +1/2+   |   |   |   |   |",
				"   |   |   |   |   |   +--- 3/2 ---+   |   |   |   |",
				"   |   |   |   |   +------- 5/2 -------+   |   |   |",
				"   |   |   |   +----------- 7/2 -----------+   |   |",
				"   |   |   +--------------- 9/2 ---------------+   |",
				"   |   +------------------ 11/2 -------------------+",
				"   +---------------------- 13/2 -----------------------"
			],
			"mathematica": [
				"t[n_, 1] := n; t[n_, n_] := n-1;",
				"t[n_, k_] := Abs[2k - n - If[2k \u003c= n+1, 2, 1]];",
				"2 Table[t[n, k], {n, 1, 12}, {k, 1, n}] // Flatten (* _Jean-François Alcover_, Nov 17 2018 *)"
			],
			"xref": [
				"Cf. A018226, A130517, A130556, A130598, A130602, A162626.",
				"Other versions are A212012, A212122, A213362, A213372"
			],
			"keyword": "nonn,tabl",
			"offset": "1,1",
			"author": "_Omar E. Pol_, Jul 10 2009",
			"ext": [
				"Corrected by _Omar E. Pol_, Jul 13 2009",
				"More terms from _Omar E. Pol_, Jul 14 2012",
				"New name from _Omar E. Pol_, Sep 02 2012"
			],
			"references": 10,
			"revision": 24,
			"time": "2018-11-18T09:05:34-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}