{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A064709",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 64709,
			"data": "6,14,20,33,54,91,323,141",
			"name": "Initial term of run of (exactly) n consecutive numbers with just 2 distinct prime factors.",
			"comment": [
				"The given terms up to a(8) = 141 are the only terms less than 10^18. To speed the search, note that any string of 6 or more consecutive numbers contains a multiple of 6 and hence must contain a number of the form 2^a * 3^b. Conjecture: 141 is the last term, because numbers with only two different prime factors get pretty rare, so having several in a row near a number of the form 2^a * 3^b is pretty unlikely. - _Joshua Zucker_, May 05 2006",
				"Sequence cannot have any terms for n \u003e 29, since a run of 30 or more consecutive numbers must contain a multiple of 30, divisible by at least 3 primes. - _Franklin T. Adams-Watters_, Oct 23 2006",
				"I searched numbers of the form n=2^a * 3^b through 10^700 and could not find any solution where even 4 numbers (n+2, n-2, n+3, n-3) had omega=2. The last such number through 10^700 is only 169075682574336=2^33 * 3^9. So a full set of 9 numbers seems quite unlikely. - _Fred Schneider_, Jan 05 2008",
				"From _Vim Wenders_, Apr 02 2008: (Start)",
				"The sequence is complete. The argument of Franklin T. Adams-Watters is easily extended: if 2^a*3^b, a,b, \u003e= 1 is a term then omega(2^a*3^b+-6) \u003e 2 (because the exponents of 2 and 3 follow a ruler like sequence). So the last possible term would be a(11).",
				"Also, if 2*p, p prime, is in the run of an initial value to check, then p+2, p+4, ... has to be prime too (for the values 2p+4 = 2(p+2), 2p+8 = 2(p+4) ...), which is impossible for obvious reason.",
				"The two arguments limit the maximum length of a run to 8. (End)",
				"Wenders's argument is incomplete because the consecutive even numbers can have the form 2^a p^b. As stated in the paper by Eggleton and MacDougall, it is still a conjecture that 9 consecutive omega-2 numbers do not exist. - _T. D. Noe_, Oct 13 2008",
				"For a(10) to exist, one of the consecutive terms must be in A033846. Also, the sequence cannot have any terms for n \u003e 14. If a(15) exists, one term has to be of the form A = 2^n*5^m. Then, there also must be two other terms divisible by 5, excluding A. Call them B and C.",
				"       Case 1: If A is the smallest of these terms, then B = 5*(2^n*5^(m-1)+1) and C = 10*(2^(n-1)*5^(m-1)+1). However, for large enough values of m and n, either B/5 or C/10 is divisible by 3 and another prime \u003e 5.",
				"       Case 2: If A is the middle term, then B = 5*(2^n*5^(m-1)-1) and C = 5*(2^n*5^(m-1)+1). For large enough values of m and n, either B/5 or C/5 is a multiple of 3 and another prime \u003e 5.",
				"       Case 3: If A is the highest term, B = 5*(2^n*5^(m-1)-1) and C = 10*(2^(n-1)*5^(m-1)-1). Again, for large enough values of m and n, either B/5 or C/10 is a multiple of 3 and another prime \u003e 5.",
				"       Thus, if there are more terms in the sequence, they can only be a(9), a(10), a(11), a(12), a(13) or a(14).",
				"     Other comments: The only numbers that satisfy a(8) are 141 and 212. The reason a(9) wasn't satisfied in either of these is because one end of the run of numbers was a prime and the other end stopped at a multiple of 10. I believe it is possible to show that a(10) can never exist because there cannot be a multiple of 10 in the run of consecutive numbers, perhaps because there cannot be two multiples of 5. - _Derek Orr_, May 24 2014",
				"Eggleton and MacDougall prove that no terms exist beyond a(9) and conjecture that a(9) does not exist. - _Jason Kimberley_, Jul 08 2017"
			],
			"link": [
				"Roger B. Eggleton and James A. MacDougall, \u003ca href=\"http://www.jstor.org/stable/27643119\"\u003eConsecutive integers with equally many principal divisors\u003c/a\u003e, Math. Mag. 81 (2008), 235-248. [_T. D. Noe_, Oct 13 2008]",
				"Carlos Rivera, \u003ca href=\"http://www.primepuzzles.net/puzzles/puzz_427.htm\"\u003ePrime Puzzle 427\u003c/a\u003e"
			],
			"example": [
				"6 = 2*3;",
				"14 = 2*7 and 15 = 3*5;",
				"20 = 2^2*5, 21 = 3*7 and 22 = 2*11;",
				"33 = 3*11, 34 = 2*17, 35 = 5*7 and 36 = (2*3)^2; etc."
			],
			"mathematica": [
				"Function[s, Function[t, Map[t[[First@ FirstPosition[t[[All, -1]], k_ /; k == #] ]] \u0026, Range[0, Max@ t[[All, -1]] ] ][[All, 1]] ]@ Join[{{First@ s, 0}, {#[[1, 1, 1]], 1}}, Rest@ Map[{#[[1, 1]], Length@ # + 1} \u0026, #, {1}]] \u0026@ SplitBy[Partition[Select[#, Last@ # == 1 \u0026][[All, 1]], 2, 1], Differences] \u0026@ Map[{First@ #, First@ Differences@ #} \u0026, Partition[s, 2, 1]]]@ Select[Range[10^5], PrimeNu[#] == 2 \u0026] (* _Michael De Vlieger_, Jul 17 2017 *)",
				"With[{t=Table[If[PrimeNu[n]==2,1,0],{n,350}]},Flatten[Table[ SequencePosition[ t,Join[{0},PadRight[{},k,1],{0}],1],{k,8}],1]][[All,1]]+1 (* Requires Mathematica version 10 or later *) (* _Harvey P. Dale_, May 16 2020 *)"
			],
			"xref": [
				"Cf. A064708."
			],
			"keyword": "nonn,hard,fini,full",
			"offset": "1,1",
			"author": "_Robert G. Wilson v_, Oct 13 2001",
			"references": 6,
			"revision": 40,
			"time": "2020-05-16T13:40:29-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}