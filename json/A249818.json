{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A249818",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 249818,
			"data": "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,27,22,23,24,25,26,21,28,29,30,31,32,45,34,35,36,37,38,33,40,41,42,43,44,81,46,47,48,49,50,75,52,53,54,125,56,63,58,59,60,61,62,39,64,55,66,67,68,135,70,71,72,73,74,51,76,77,78,79,80,99,82,83,84,175,86,105",
			"name": "Permutation of natural numbers: a(1) = 1, a(n) = A246278(A055396(n),A078898(n)).",
			"comment": [
				"a(n) tells which number in square array A246278 is at the same position where n is in array A083221, the sieve of Eratosthenes. As both arrays have even numbers as their topmost row and primes as their leftmost column, both sequences are among the fixed points of this permutation.",
				"Equally: a(n) tells which number in array A246279 is at the same position where n is in the array A083140, as they are the transposes of above two arrays."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A249818/b249818.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"a(1) = 1, a(n) = A246278(A055396(n), A078898(n)).",
				"a(1) = 1, a(n) = A246278(A055396(n), A249822(A055396(n), A246277(n))).",
				"As a composition of other permutations:",
				"a(1) = 1, and for n \u003e 1, a(n) = 1 + A249816(n-1).",
				"Other identities. For all n \u003e= 1:",
				"a(A005843(n)) = A005843(n) and a(A000040(n)) = A000040(n). [Fixes even numbers and primes, among other numbers. Cf. comments above].",
				"A020639(a(n)) = A020639(n) and A055396(a(n)) = A055396(n). [Preserves the smallest prime factor of n]."
			],
			"mathematica": [
				"lim = 87; a003961[p_?PrimeQ] := a003961[p] = Prime[PrimePi@ p + 1]; a003961[1] = 1; a003961[n_] :=  a003961[n] = Times @@ (a003961[First@ #]^Last@ # \u0026) /@ FactorInteger@ n; a055396[n_] := PrimePi[FactorInteger[n][[1, 1]]]; a078898 = Block[{nn = 90, spfs}, spfs = Table[FactorInteger[n][[1, 1]], {n, nn}]; Table[Count[Take[spfs, i], spfs[[i]]], {i, nn}]]; a246278 = NestList[Map[a003961, #] \u0026, Table[2 k, {k, lim}], lim]; Table[a246278[[a055396@ n, a078898[[n]]]], {n, 2, lim}]",
				"(* _Michael De Vlieger_, Jan 04 2016, after _Harvey P. Dale_ at A055396 and A078898 *)"
			],
			"program": [
				"(define (A249818 n) (if (= 1 n) n (A246278bi (A055396 n) (A078898 n)))) ;; Code for A246278bi given in A246278.",
				";; Alternatively:",
				"(define (A249818 n) (if (= 1 n) n (A246278bi (A055396 n) (A249822bi (A055396 n) (A246277 n))))) ;; Code for A249822bi given in A249822."
			],
			"xref": [
				"Inverse: A249817.",
				"There are three different \"deep\" versions of this permutation, recursing on values of A055396(n) and/or A078898(n), namely: A250246, A250248 and A250250.",
				"Other similar or related permutations: A249816.",
				"Cf. A000040, A005843, A020639, A055396, A078898, A083140, A083221, A246277, A246278, A246279, A249822.",
				"Differs from its inverse A249817 for the first time at n=33, where a(33) = 45, while A249817(33) = 39."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Antti Karttunen_, Nov 06 2014",
			"references": 29,
			"revision": 21,
			"time": "2016-01-09T18:02:40-05:00",
			"created": "2014-11-17T22:02:34-05:00"
		}
	]
}