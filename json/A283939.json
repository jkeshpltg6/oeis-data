{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A283939",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 283939,
			"data": "1,3,2,6,5,4,11,9,8,7,17,15,13,12,10,25,22,20,18,16,14,34,31,28,26,23,21,19,44,41,38,35,32,29,27,24,56,52,49,46,42,39,36,33,30,69,65,61,58,54,50,47,43,40,37,84,79,75,71,67,63,59,55,51,48,45,100",
			"name": "Interspersion of the signature sequence of sqrt(2).",
			"comment": [
				"Row n is the ordered sequence of numbers k such that A007336(k)=n.  As a sequence, A283939 is a permutation of the positive integers. As an array, A283939 is the joint-rank array (defined at A182801) of the numbers {i+j*r}, for i\u003e=1, j\u003e=1, where r = sqrt(2).  This is a transposable interspersion; i.e., every row intersperses all other rows, and every column intersperses all other columns."
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A283939/b283939.txt\"\u003eAntidiagonals n = 1..60, flattened\u003c/a\u003e",
				"Clark Kimberling and John E. Brown, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL7/Kimberling/kimber67.html\"\u003ePartial Complements and Transposable Dispersions\u003c/a\u003e, J. Integer Seqs., Vol. 7, 2004."
			],
			"example": [
				"Northwest corner:",
				"1   3   6   11   17   25   34   44   56",
				"2   5   9   15   22   31   41   52   65",
				"4   8   13  20   28   38   49   61   75",
				"7   12  18  26   35   46   58   71   86",
				"10  16  23  32   42   54   67   81   97",
				"14  21  29  39   50   63   77   91   109"
			],
			"mathematica": [
				"r = Sqrt[2]; z = 100;",
				"s[0] = 1; s[n_] := s[n] = s[n - 1] + 1 + Floor[n*r];",
				"u = Table[n + 1 + Sum[Floor[(n - k)/r], {k, 0, n}], {n, 0, z}] (* A022776, col 1 of A283939 *)",
				"v = Table[s[n], {n, 0, z}] (* A022775, row 1 of A283939*)",
				"w[i_, j_] := u[[i]] + v[[j]] + (i - 1)*(j - 1) - 1;",
				"Grid[Table[w[i, j], {i, 1, 10}, {j, 1, 10}]] (* A283939, array *)",
				"p = Flatten[Table[w[k, n - k + 1], {n, 1, 20}, {k, 1, n}]] (* A283939, sequence *)"
			],
			"program": [
				"(PARI)",
				"r = sqrt(2);",
				"z = 100;",
				"s(n) = if(n\u003c1, 1, s(n - 1) + 1 + floor(n*r));",
				"p(n) = n + 1 + sum(k=0, n, floor((n - k)/r));",
				"u = v = vector(z + 1);",
				"for(n=1, 101, (v[n] = s(n - 1)));",
				"for(n=1, 101, (u[n] = p(n - 1)));",
				"w(i, j) = u[i] + v[j] + (i - 1) * (j - 1) - 1;",
				"tabl(nn) = {for(n=1, nn, for(k=1, n, print1(w(k, n - k + 1), \", \"); );print(); ); };",
				"tabl(10) \\\\ _Indranil Ghosh_, Mar 21 2017",
				"(Python)",
				"from sympy import sqrt",
				"import math",
				"def s(n): return 1 if n\u003c1 else s(n - 1) + 1 +",
				"int(math.floor(n*sqrt(2)))",
				"def p(n): return n + 1 + sum([int(math.floor((n - k)/sqrt(2))) for k in range(0, n+1)])",
				"v=[s(n) for n in range(0, 101)]",
				"u=[p(n) for n in range(0, 101)]",
				"def w(i,j): return u[i - 1] + v[j - 1] + (i - 1) * (j - 1) - 1",
				"for n in range(1, 11):",
				"....print [w(k, n - k + 1) for k in range(1, n + 1)] # _Indranil Ghosh_, Mar 21 2017"
			],
			"xref": [
				"Cf. A007336, A002193, A022776, A283962."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Mar 19 2017",
			"references": 2,
			"revision": 8,
			"time": "2019-12-07T12:18:28-05:00",
			"created": "2017-03-19T19:29:11-04:00"
		}
	]
}