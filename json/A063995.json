{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A063995",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 63995,
			"data": "1,1,0,1,1,0,1,0,1,1,0,1,1,1,0,1,1,0,1,1,1,1,1,0,1,1,0,1,1,2,1,2,1,1,0,1,1,0,1,1,2,1,3,1,2,1,1,0,1,1,0,1,1,2,2,3,2,3,2,2,1,1,0,1,1,0,1,1,2,2,3,3,4,3,3,2,2,1,1,0,1,1,0,1,1,2,2,4,3,5,4,5,3,4,2,2,1,1,0,1,1,0,1,1,2",
			"name": "Irregular triangle read by rows: T(n,k), n \u003e= 1, -(n-1) \u003c= k \u003c= n-1, = number of partitions of n with rank k.",
			"comment": [
				"The rank of a partition is the largest part minus the number of parts.",
				"The rows are symmetric: for every partition of rank r there is its conjugate with rank -r. [_Joerg Arndt_, Oct 07 2012]"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A063995/b063995.txt\"\u003eRows n = 1..72 of triangle, flattened\u003c/a\u003e",
				"G. E. Andrews, \u003ca href=\"http://www.math.psu.edu/vstein/alg/antheory/preprint/andrews/17.pdf\"\u003eThe number of smallest parts in the partitions of n\u003c/a\u003e. [Also Selected Works, p. 603, see N(m,n).] - _N. J. A. Sloane_, Dec 16 2013",
				"A. O. L. Atkin and P. Swinnerton-Dyer, \u003ca href=\"https://doi.org/10.1112/plms/s3-4.1.84\"\u003eSome properties of partitions\u003c/a\u003e, Proc. London Math. Soc. (3) 4, (1954). 84-106. Math. Rev. 15,685d.",
				"Alexander Berkovich and Frank G. Garvan, \u003ca href=\"https://doi.org/10.1006/jcta.2002.3281\"\u003eSome observations on Dyson's new symmetries of partitions\u003c/a\u003e, Journal of Combinatorial Theory, Series A 100.1 (2002): 61-93.",
				"Freeman J. Dyson, \u003ca href=\"https://doi.org/10.1016/S0021-9800(69)80006-2\"\u003eA new symmetry of partitions\u003c/a\u003e, Journal of Combinatorial Theory 7.1 (1969): 56-61. See Table 1.",
				"Freeman J. Dyson, \u003ca href=\"https://doi.org/10.1016/0097-3165(89)90043-5\"\u003eMappings and symmetries of partitions\u003c/a\u003e, J. Combin. Theory Ser. A 51 (1989), 169-180."
			],
			"example": [
				"The partition 5 = 4+1 has largest summand 4 and 2 summands, hence has rank 4-2 = 2.",
				"Triangle begins:",
				"[ 1]                               1,",
				"[ 2]                            1, 0, 1,",
				"[ 3]                         1, 0, 1, 0, 1,",
				"[ 4]                      1, 0, 1, 1, 1, 0, 1,",
				"[ 5]                   1, 0, 1, 1, 1, 1, 1, 0, 1,",
				"[ 6]                1, 0, 1, 1, 2, 1, 2, 1, 1, 0, 1,",
				"[ 7]             1, 0, 1, 1, 2, 1, 3, 1, 2, 1, 1, 0, 1,",
				"[ 8]          1, 0, 1, 1, 2, 2, 3, 2, 3, 2, 2, 1, 1, 0, 1,",
				"[ 9]       1, 0, 1, 1, 2, 2, 3, 3, 4, 3, 3, 2, 2, 1, 1, 0, 1,",
				"[10]    1, 0, 1, 1, 2, 2, 4, 3, 5, 4, 5, 3, 4, 2, 2, 1, 1, 0, 1,",
				"[11] 1, 0, 1, 1, 2, ...",
				"Row 20 is:",
				"T(20, k) = 1, 0, 1, 1, 2, 2, 4, 4, 7, 8, 12, 14, 20, 22, 30, 33, 40, 42, 48, 45, 48, 42, 40, 33, 30, 22, 20, 14, 12, 8, 7, 4, 4, 2, 2, 1, 1, 0, 1; -19 \u003c= k \u003c= 19.",
				"Another view of the table of p(n,m) = number of partitions of n with rank m, taken from Dyson (1969):",
				"n\\m -6 -5  -4  -3  -2  -1   0   1   2   3   4   5   6",
				"-----------------------------------------------------",
				"0   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,",
				"1   0,  0,  0,  0,  0,  0,  1,  0,  0,  0,  0,  0,  0,",
				"2   0,  0,  0,  0,  0,  1,  0,  1,  0,  0,  0,  0,  0,",
				"3   0,  0,  0,  0,  1,  0,  1,  0,  1,  0,  0,  0,  0,",
				"4   0,  0,  0,  1,  0,  1,  1,  1,  0,  1,  0,  0,  0,",
				"5   0,  0,  1,  0,  1,  1,  1,  1,  1,  0,  1,  0,  0,",
				"6   0,  1,  0,  1,  1,  2,  1,  2,  1,  1,  0,  1,  0,",
				"7   1,  0,  1,  l,  2,  1,  3,  1,  2,  1,  1,  0,  1,",
				"...",
				"The central triangle is the present sequence, the right-hand triangle is A105806. - _N. J. A. Sloane_, Jan 23 2020"
			],
			"mathematica": [
				"Table[ Count[ (First[ # ]-Length[ # ]\u0026 /@ IntegerPartitions[ k ]), # ]\u0026 /@ Range[ -k+1, k-1 ], {k, 16} ]"
			],
			"program": [
				"(Haskell)",
				"import Data.List (sort, group)",
				"a063995 n k = a063995_tabf !! (n-1) !! (n-1+k)",
				"a063995_row n = a063995_tabf !! (n-1)",
				"a063995_tabf = [[1], [1, 0, 1]] ++ (map",
				"   (\\rs -\u003e [1, 0] ++ (init $ tail $ rs) ++ [0, 1]) $ drop 2 $ map",
				"   (map length . group . sort . map rank) $ tail pss) where",
				"      rank ps = maximum ps - length ps",
				"      pss = [] : map (\\u -\u003e [u] : [v : ps | v \u003c- [1..u],",
				"                             ps \u003c- pss !! (u - v), v \u003c= head ps]) [1..]",
				"-- _Reinhard Zumkeller_, Jul 24 2013"
			],
			"xref": [
				"For the number of partitions of n with rank 0 (balanced partitions) see A047993.",
				"Cf. A105806 (right half of triangle), A005408 (row lengths), A000041 (row sums), A047993 (central terms)."
			],
			"keyword": "nonn,nice,tabf",
			"offset": "1,30",
			"author": "_N. J. A. Sloane_, Sep 19 2001",
			"ext": [
				"More terms from _Vladeta Jovovic_ and _Wouter Meeussen_, Sep 19 2001"
			],
			"references": 24,
			"revision": 37,
			"time": "2020-01-24T03:23:03-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}