{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007433",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7433,
			"id": "M4089",
			"data": "1,6,11,27,27,66,51,112,102,162,123,297,171,306,297,453,291,612,363,729,561,738,531,1232,678,1026,922,1377,843,1782,963,1818,1353,1746,1377,2754,1371,2178,1881,3024,1683",
			"name": "Inverse Moebius transform applied twice to squares.",
			"comment": [
				"Dirichlet convolution of A001157 and A000012. Dirichlet convolution of A000005 and A000290 (Jovovic formula). - _R. J. Mathar_, Feb 03 2011"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vaclav Kotesovec, \u003ca href=\"/A007433/b007433.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/transforms.txt\"\u003eTransforms\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Sum_{d|n} d^2*tau(n/d). - _Vladeta Jovovic_, Jul 31 2002",
				"Equals A134577 * [1, 2, 3, ...]. - _Gary W. Adamson_, Nov 02 2007",
				"G.f.: Sum_{k\u003e=1} sigma_2(k)*x^k/(1 - x^k), where sigma_2(k) is the sum of squares of divisors of k (A001157). - _Ilya Gutkovskiy_, Jan 16 2017",
				"Dirichlet g.f.: zeta(s-2)*zeta(s)^2. - _Benedict W. J. Irwin_, Jul 14 2018",
				"a(n) is multiplicative with a(p^e) = (p^(2*e + 4) - (e+2) * p^2 + e+1)) / (p^2 - 1)^2. - _Michael Somos_, Jul 15 2018",
				"Sum_{k=1..n} a(k) ~ Zeta(3)^2 * n^3 / 3. - _Vaclav Kotesovec_, Nov 04 2018"
			],
			"example": [
				"G.f. = x + 6*x^2 + 11*x^3 + 27*x^4 + 27*x^5 + 66*x^6 + 51*x^7 + 112*x^8 + 102*x^9 + ... - _Michael Somos_, Jul 15 2018"
			],
			"mathematica": [
				"a[n_] := Plus @@ DivisorSigma[2, Divisors[n]]; Array[a, 41] (* _Robert G. Wilson v_, May 05 2010 *)",
				"a[ n_] := If[ n \u003c 1, 0, Times @@ (If[ # == 1, 1, (#^(2 #2 + 4) - (#2 + 2) #^2 + #2 + 1) / (#^2 - 1)^2] \u0026 @@@ FactorInteger @ n)]; (* _Michael Somos_, Jul 15 2018 *)"
			],
			"program": [
				"(PARI) /* Dirichlet convolution of A001157, A000012 (Mathar): */",
				"a(n)=sumdiv(n, d, sigma(d,2))",
				"(PARI) /* Dirichlet convolution of A000005, A000290 (Mathar): */",
				"a(n)=sumdiv(n, d, d^2*sigma(n/d,0))"
			],
			"xref": [
				"Cf. A134577."
			],
			"keyword": "nonn,mult",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"a(38) corrected by _Ilya Gutkovskiy_, Jan 16 2016"
			],
			"references": 9,
			"revision": 37,
			"time": "2018-11-04T10:50:44-05:00",
			"created": "1994-05-24T03:00:00-04:00"
		}
	]
}