{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A008297",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 8297,
			"data": "-1,2,1,-6,-6,-1,24,36,12,1,-120,-240,-120,-20,-1,720,1800,1200,300,30,1,-5040,-15120,-12600,-4200,-630,-42,-1,40320,141120,141120,58800,11760,1176,56,1,-362880,-1451520,-1693440,-846720,-211680,-28224,-2016,-72,-1,3628800,16329600,21772800,12700800",
			"name": "Triangle of Lah numbers.",
			"comment": [
				"|a(n,k)| = number of partitions of {1..n} into k lists, where a list means an ordered subset.",
				"Let N be a Poisson random variable with parameter (mean) lambda, and Y_1,Y_2,... independent exponential(theta) variables, independent of N, so that their density is given by (1/theta)*exp(-x/theta), x \u003e 0. Set S=Sum_{i=1..N} Y_i. Then E(S^n), i.e., the n-th moment of S, is given by (theta^n) * L_n(lambda), n \u003e= 0, where L_n(y) is the Lah polynomial Sum_{k=0..n} |a(n,k)| * y^k. - Shai Covo (green355(AT)netvision.net.il), Feb 09 2010",
				"For y = lambda \u003e 0, formula 2) for the Lah polynomial L_n(y) dated Feb 02 2010 can be restated as follows: L_n(lambda) is the n-th ascending factorial moment of the Poisson distribution with parameter (mean) lambda. - Shai Covo (green355(AT)netvision.net.il), Feb 10 2010",
				"See A111596 for an expression of the row polynomials in terms of an umbral composition of the Bell polynomials and relation to an inverse Mellin transform and a generalized Dobinski formula. - _Tom Copeland_, Nov 21 2011",
				"Also the Bell transform of the sequence (-1)^(n+1)*(n+1)! without column 0. For the definition of the Bell transform see A264428. - _Peter Luschny_, Jan 28 2016",
				"Named after the Slovenian mathematician and actuary Ivo Lah (1896-1979). - _Amiram Eldar_, Jun 13 2021"
			],
			"reference": [
				"Louis Comtet, Advanced Combinatorics, Reidel, 1974, p. 156.",
				"Shai Covo, The moments of a compound Poisson process with exponential or centered normal jumps, J. Probab. Stat. Sci., Vol. 7, No. 1 (2009), pp. 91-100.",
				"Theodore S. Motzkin, Sorting numbers for cylinders and other classification numbers, in Combinatorics, Proc. Symp. Pure Math. 19, AMS, 1971, pp. 167-176; the sequence called {!}^{n+}. For a link to this paper see A000262.",
				"John Riordan, An Introduction to Combinatorial Analysis, Wiley, 1958, p. 44.",
				"S. Gill Williamson, Combinatorics for Computer Science, Computer Science Press, 1985; see p. 176."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A008297/b008297.txt\"\u003eRows n=1..100 of triangle, flattened\u003c/a\u003e",
				"J. Fernando Barbero G., Jesús Salas, Eduardo J. S. Villaseñor, \u003ca href=\"http://arxiv.org/abs/1307.2010\"\u003eBivariate Generating Functions for a Class of Linear Recurrences. I. General Structure\u003c/a\u003e, arXiv:1307.2010 [math.CO], 2013.",
				"P. Blasiak, K. A. Penson and A. I. Solomon, \u003ca href=\"http://arXiv.org/abs/quant-ph/0212072\"\u003eThe Boson Normal Ordering Problem and Generalized Bell Numbers\u003c/a\u003e, arXiv:quant-ph/0212072, 2002.",
				"P. Blasiak, K. A. Penson and A. I. Solomon, \u003ca href=\"http://www.arXiv.org/abs/quant-ph/0402027\"\u003eThe general boson normal ordering problem\u003c/a\u003e, arXiv:quant-ph/0402027, 2004.",
				"Tom Copeland, \u003ca href=\"http://tcjpn.wordpress.com/2015/12/21/generators-inversion-and-matrix-binomial-and-integral-transforms/\"\u003eGenerators, Inversion, and Matrix, Binomial, and Integral Transforms\u003c/a\u003e, 2015.",
				"Tom Copeland, \u003ca href=\"https://tcjpn.wordpress.com/2011/04/11/lagrange-a-la-lah/\"\u003eLagrange a la Lah\u003c/a\u003e, 2011.",
				"Siad Daboul, Jan Mangaldan, Michael Z. Spivey and Peter J. Taylor, \u003ca href=\"http://math.pugetsound.edu/~mspivey/Exp.pdf\"\u003eThe Lah Numbers and the n-th Derivative of exp(1/x)\u003c/a\u003e, Math. Mag., Vol. 86, No. 1 (2013), pp. 39-47.",
				"Askar Dzhumadil'daev and Damir Yeliussizov, \u003ca href=\"http://arxiv.org/abs/1408.6764v1\"\u003ePath decompositions of digraphs and their applications to Weyl algebra\u003c/a\u003e, arXiv preprint arXiv:1408.6764v1 [math.CO], 2014-2015. [Version 1 contained many references to the OEIS, which were removed in Version 2. - _N. J. A. Sloane_, Mar 28 2015]",
				"Askar Dzhumadil’daev and Damir Yeliussizov, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v22i4p10\"\u003eWalks, partitions, and normal ordering\u003c/a\u003e, Electronic Journal of Combinatorics, Vol. 22, No. 4 (2015), #P4.10.",
				"B. S. El-Desouky, Nenad P.Cakić and Toufik Mansour, \u003ca href=\"https://doi.org/10.1016/j.aml.2009.08.018\"\u003eModified approach to generalized Stirling numbers via differential operators\u003c/a\u003e, Appl. Math. Lett., Vol. 23, No. 1 (2010), pp. 115-120.",
				"Sen-Peng Eu, Tung-Shan Fu, Yu-Chang Liang and Tsai-Lien Wong. \u003ca href=\"https://arxiv.org/abs/1701.00600\"\u003eOn xD-Generalizations of Stirling Numbers and Lah Numbers via Graphs and Rooks\u003c/a\u003e. arXiv:1701.00600 [math.CO], 2017.",
				"Milan Janjic, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL12/Janjic/janjic22.html\"\u003eSome classes of numbers and derivatives\u003c/a\u003e, JIS, Vol. 12 (2009), pp. 09.8.3",
				"Dmitry Karp and Elena Prilepkina, \u003ca href=\"http://arxiv.org/abs/1111.4271\"\u003eGeneralized Stieltjes transforms: basic aspects\u003c/a\u003e, arXiv preprint arXiv:1111.4271 [math.CA], 2011.",
				"Dmitry Karp and Elena Prilepkina, \u003ca href=\"http://files.ele-math.com/articles/jca-01-07.pdf\"\u003eGeneralized Stieltjes functions and their exact order\u003c/a\u003e, Journal of Classical Analysis, Vol. 1, No. 1 (2012), pp. 53-74. - _N. J. A. Sloane_, Dec 25 2012.",
				"Udita N. Katugampola, \u003ca href=\"http://arxiv.org/abs/1106.0965\"\u003eA new Fractional Derivative and its Mellin Transform\u003c/a\u003e, arXiv preprint arXiv:1106.0965 [math.CA], 2011.",
				"Udita N. Katugampola, \u003ca href=\"http://arxiv.org/abs/1112.6031\"\u003eMellin Transforms of the Generalized Fractional Integrals and Derivatives\u003c/a\u003e, arXiv preprint arXiv:1112.6031 [math.CA], 2011.",
				"Donald E. Knuth, \u003ca href=\"http://arxiv.org/abs/math/9207221\"\u003eConvolution polynomials\u003c/a\u003e, Mathematica J. 2.1 (1992), no. 4, 67-78.",
				"Wolfdieter Lang, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL3/LANG/lang.html\"\u003eOn generalizations of Stirling number triangles\u003c/a\u003e, J. Integer Seq., Vol. 3 (2000), Article 00.2.4.",
				"Toufik Mansour and Matthias Schork, \u003ca href=\"http://puma.dimai.unifi.it/23_2/mansour_schork.pdf\"\u003eGeneralized Bell numbers and algebraic differential equations\u003c/a\u003e, Pure Math. Appl.(PU. MA), Vol. 23, No. 2 (2012), pp. 131-142.",
				"Toufik Mansour, Matthias Schork and Mark Shattuck, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL15/Schork/schork2.html\"\u003eThe Generalized Stirling and Bell Numbers Revisited\u003c/a\u003e, Journal of Integer Sequences, Vol. 15 (2012), Article 12.8.3.",
				"Toufik Mansour and Mark Shattuck, \u003ca href=\"https://dx.doi.org/10.1007/s10998-017-0209-9\"\u003eA polynomial generalization of some associated sequences related to set partitions\u003c/a\u003e, Periodica Mathematica Hungarica, Vol. 75, No. 2 (December 2017), pp. 398-412.",
				"Emanuele Munarini, \u003ca href=\"https://doi.org/10.2298/AADM180226017M\"\u003eCombinatorial identities involving the central coefficients of a Sheffer matrix\u003c/a\u003e, Applicable Analysis and Discrete Mathematics, Vol. 13, No. 2 (2019), pp. 495-517.",
				"Mathias Pétréolle and Alan D. Sokal, \u003ca href=\"https://arxiv.org/abs/1907.02645\"\u003eLattice paths and branched continued fractions. II. Multivariate Lah polynomials and Lah symmetric functions\u003c/a\u003e, arXiv:1907.02645 [math.CO], 2019.",
				"Jose L. Ramirez, M Shattuck, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL19/Ramirez2/ramirez12.pdf\"\u003eA (p, q)-Analogue of the r-Whitney-Lah Numbers\u003c/a\u003e, Journal of Integer Sequences, Vol. 19 (2016), Article 16.5.6.",
				"Mark Shattuck, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL20/Shattuck/sha11.html\"\u003eIdentities for Generalized Whitney and Stirling Numbers\u003c/a\u003e, Journal of Integer Sequences, Vol. 20 (2017), Article 17.10.4.",
				"Mark Shattuck, \u003ca href=\"https://doi.org/10.33039/ami.2018.11.001\"\u003eSome formulas for the restricted r-Lah numbers\u003c/a\u003e, Annales Mathematicae et Informaticae, Vol. 49 (2018), Eszterházy Károly University Institute of Mathematics and Informatics, pp. 123-140.",
				"Michael Z. Spivey, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL14/Spivey/spivey31.html\"\u003eOn Solutions to a General Combinatorial Recurrence\u003c/a\u003e, J. Int. Seq., Vol. 14 (2011) Article 11.9.7.",
				"Bao-Xuan Zhu, \u003ca href=\"https://arxiv.org/abs/2006.14485\"\u003eTotal positivity from a generalized cycle index polynomial\u003c/a\u003e, arXiv:2006.14485 [math.CO], 2020."
			],
			"formula": [
				"a(n, m) = (-1)^n*n!*A007318(n-1, m-1)/m!, n \u003e= m \u003e= 1.",
				"a(n+1, m) = (n+m)*a(n, m)+a(n, m-1), a(n, 0) := 0; a(n, m) := 0, n \u003c m; a(1, 1)=1.",
				"a(n, m) = ((-1)^(n-m+1))*L(1, n-1, m-1) where L(1, n, m) is the triangle of coefficients of the generalized Laguerre polynomials n!*L(n, a=1, x). These polynomials appear in the radial l=0 eigen-functions for discrete energy levels of the H-atom.",
				"|a(n, m)| = Sum_{k=m..n} |A008275(n, k)|*A008277(k, m), where A008275 = Stirling numbers of first kind, A008277 = Stirling numbers of second kind. - _Wolfdieter Lang_",
				"If L_n(y) = Sum_{k=0..n} |a(n, k)|*y^k (a Lah polynomial) then the e.g.f. for L_n(y) is exp(x*y/(1-x)). - _Vladeta Jovovic_, Jan 06 2001",
				"E.g.f. for the k-th column (unsigned): x^k/(1-x)^k/k!. - _Vladeta Jovovic_, Dec 03 2002",
				"a(n, k) = (n-k+1)!*N(n, k) where N(n, k) is the Narayana triangle A001263. - _Philippe Deléham_, Jul 20 2003",
				"From Shai Covo (green355(AT)netvision.net.il), Feb 02 2010: (Start)",
				"We have the following expressions for the Lah polynomial L_n(y) = Sum_{k=0..n} |a(n, k)|*y^k -- exact generalizations of results in A000262 for A000262(n) = L_n(1):",
				"1) L_n(y) = y*exp(-y)*n!*M(n+1,2,y), n \u003e= 1, where M (=1F1) is the confluent hypergeometric function of the first kind;",
				"2) L_n(y) = exp(-y)* Sum_{m\u003e=0} y^m*[m]^n/m!, n\u003e=0, where [m]^n = m*(m+1)*...*(m+n-1) is the rising factorial;",
				"3) L_n(y) = (2n-2+y)L_{n-1}(y)-(n-1)(n-2)L_{n-2}(y), n\u003e=2;",
				"4) L_n(y) = y*(n-1)!*Sum_{k=1..n} (L_{n-k}(y) k!)/((n-k)! (k-1)!), n\u003e=1. (End)",
				"The row polynomials are given by D^n(exp(-x*t)) evaluated at x = 0, where D is the operator (1-x)^2*d/dx. Cf. A008277 and A035342. - _Peter Bala_, Nov 25 2011",
				"n!C(-xD,n) = Lah(n,:xD:) where C(m,n) is the binomial coefficient, xD= x d/dx, (:xD:)^k = x^k D^k, and Lah(n,x) are the row polynomials of this entry. E.g., 2!C(-xD,2)= 2 xD + x^2 D^2. - _Tom Copeland_, Nov 03 2012",
				"From _Tom Copeland_, Sep 25 2016: (Start)",
				"The Stirling polynomials of the second kind A048993 (A008277), i.e., the Bell-Touchard-exponential polynomials B_n[x], are umbral compositional inverses of the Stirling polynomials of the first kind signed A008275 (A130534), i.e., the falling factorials, (x)_n = n! binomial(x,n); that is, umbrally B_n[(x).] = x^n = (B.[x])_n.",
				"An operational definition of the Bell polynomials is (xD_x)^n = B_n[:xD:], where, by definition, (:xD_x:)^n = x^n D_x^n, so (B.[:xD_x:])_n = (xD_x)_n = :xD_x:^n = x^n (D_x)^n.",
				"Let y = 1/x, then D_x = -y^2 D_y; xD_x = -yD_y; and P_n(:yD_y:) = (-yD_y)_n = (-1)^n (1/y)^n (y^2 D_y)^n, the row polynomials of this entry in operational form, e.g., P_3(:yD_y:) = (-yD_y)_3 = (-yD_y) (yD_y-1) (yD_y-2) = (-1)^3 (1/y)^3 (y^2 D_y)^3 = -( 6 :yD_y: +  6 :yD_y:^2 + :yD_y:^3 ) = - ( 6 y D_y + 6 y^2 (D_y)^2 + y^3 (D_y)^3).",
				"Therefore, P_n(y) = e^(-y) P_n(:yD_y:) e^y = e^(-y) (-1/y)^n (y^2 D_y)^n e^y = e^(-1/x) x^n (D_x)^n e^(1/x) = P_n(1/x) and P_n(x) =  e^(-1/x) x^n (D_x)^n e^(1/x) = e^(-1/x) (:x D_x:)^n e^(1/x). (Cf. also A094638.) (End)"
			],
			"example": [
				"|a(2,1)| = 2: (12), (21); |a(2,2)| = 1: (1)(2). |a(4,1)| = 24: (1234) (24 ways); |a(4,2)| = 36: (123)(4) (6*4 ways), (12)(34) (3*4 ways); |a(4,3)| = 12: (12)(3)(4) (6*2 ways); |a(4,4)| = 1: (1)(2)(3)(4) (1 way).",
				"Triangle:",
				"    -1;",
				"     2,    1;",
				"    -6,   -6,   -1;",
				"    24,   36,   12,   1;",
				"  -120, -240, -120, -20, -1; ..."
			],
			"maple": [
				"A008297 := (n,m) -\u003e (-1)^n*n!*binomial(n-1,m-1)/m!;"
			],
			"mathematica": [
				"a[n_, m_] := (-1)^n*n!*Binomial[n-1, m-1]/m!; Table[a[n, m], {n, 1, 10}, {m, 1, n}] // Flatten (* _Jean-François Alcover_, Dec 12 2012, after Maple *)"
			],
			"program": [
				"(Sage)",
				"def A008297_triangle(dim): # computes unsigned T(n, k).",
				"    M = matrix(ZZ,dim,dim)",
				"    for n in (0..dim-1): M[n,n] = 1",
				"    for n in (1..dim-1):",
				"        for k in (0..n-1):",
				"            M[n,k] = M[n-1,k-1]+(2+2*k)*M[n-1,k]+((k+1)*(k+2))*M[n-1,k+1]",
				"    return M",
				"A008297_triangle(9) # _Peter Luschny_, Sep 19 2012",
				"(Haskell)",
				"a008297 n k = a008297_tabl !! (n-1) !! (k-1)",
				"a008297_row n = a008297_tabl !! (n-1)",
				"a008297_tabl = [-1] : f [-1] 2 where",
				"   f xs i = ys : f ys (i + 1) where",
				"     ys = map negate $",
				"          zipWith (+) ([0] ++ xs) (zipWith (*) [i, i + 1 ..] (xs ++ [0]))",
				"-- _Reinhard Zumkeller_, Sep 30 2014",
				"(PARI) T(n, m) = (-1)^n*n!*binomial(n-1, m-1)/m!",
				"for(n=1,9, for(m=1,n, print1(T(n,m)\", \"))) \\\\ _Charles R Greathouse IV_, Mar 09 2016",
				"(Perl) use bigint; use ntheory \":all\"; my @L; for my $n (1..9) { push @L, map { stirling($n,$_,3)*(-1)**$n } 1..$n; } say join(\", \",@L); # _Dana Jacobsen_, Mar 16 2017"
			],
			"xref": [
				"Same as A066667 and A105278 except for signs. Other variants: A111596 (differently signed triangle and (0,0)-based), A271703 (unsigned and (0,0)-based), A089231.",
				"A293125 (row sums) and A000262 (row sums of unsigned triangle).",
				"Columns 1-6 (unsigned): A000142, A001286, A001754, A001755, A001777, A001778.",
				"A002868 gives maximal element (in magnitude) in each row.",
				"A248045 (central terms, negated). A130561 is a natural refinement.",
				"Cf. A007318, A048786, A001263, A008275, A008277, A048993, A094638, A130534."
			],
			"keyword": "sign,tabl,nice,easy",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"references": 108,
			"revision": 184,
			"time": "2021-06-13T06:07:10-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}