{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A327117",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 327117,
			"data": "1,0,1,0,1,2,0,1,4,5,0,1,7,18,15,0,1,10,45,84,52,0,1,14,94,298,415,203,0,1,18,174,844,1995,2178,877,0,1,23,300,2081,7440,13638,12131,4140,0,1,28,486,4652,23670,64898,95823,71536,21147,0,1,34,756,9682,67390,259599,566447,694676,445356,115975",
			"name": "Number T(n,k) of colored integer partitions of n using all colors of a k-set such that a color pattern for part i has i distinct colors in increasing order; triangle T(n,k), n\u003e=0, 0\u003c=k\u003c=n, read by rows.",
			"comment": [
				"The sequence of column k satisfies a linear recurrence with constant coefficients of order k*2^(k-1) = A001787(k)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A327117/b327117.txt\"\u003eRows n = 0..140, flattened\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Partition_(number_theory)\"\u003ePartition (number theory)\u003c/a\u003e"
			],
			"formula": [
				"Sum_{k=1..n} k * T(n,k) = A327118(n)."
			],
			"example": [
				"T(3,2) = 4: 2ab1a, 2ab1b, 1a1a1b, 1a1b1b.",
				"Triangle T(n,k) begins:",
				"  1;",
				"  0, 1;",
				"  0, 1,  2;",
				"  0, 1,  4,   5;",
				"  0, 1,  7,  18,   15;",
				"  0, 1, 10,  45,   84,    52;",
				"  0, 1, 14,  94,  298,   415,    203;",
				"  0, 1, 18, 174,  844,  1995,   2178,    877;",
				"  0, 1, 23, 300, 2081,  7440,  13638,  12131,   4140;",
				"  0, 1, 28, 486, 4652, 23670,  64898,  95823,  71536,  21147;",
				"  0, 1, 34, 756, 9682, 67390, 259599, 566447, 694676, 445356, 115975;",
				"  ..."
			],
			"maple": [
				"C:= binomial:",
				"b:= proc(n, i, k) option remember; `if`(n=0, 1, `if`(i\u003c1, 0, add(",
				"      b(n-i*j, min(n-i*j, i-1), k)*C(C(k, i)+j-1, j), j=0..n/i)))",
				"    end:",
				"T:= (n, k)-\u003e add(b(n$2, i)*(-1)^(k-i)*C(k, i), i=0..k):",
				"seq(seq(T(n, k), k=0..n), n=0..12);"
			],
			"mathematica": [
				"b[n_, i_, k_] := b[n, i, k] = If[n == 0, 1, If[i\u003c1, 0, Sum[b[n - i j, Min[n - i j, i - 1], k] Binomial[Binomial[k, i] + j - 1, j], {j, 0, n/i}]]];",
				"T[n_, k_] := Sum[b[n, n, i] (-1)^(k - i) Binomial[k, i], {i, 0, k}];",
				"Table[T[n, k], {n, 0, 12}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Oct 04 2019, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Columns k=0-3 give: A000007, A057427, A014616(n-1) for n\u003e1, A327842.",
				"Main diagonal gives A000110.",
				"Row sums give A116540.",
				"T(2n,n) gives A327843.",
				"Cf. A001787, A255903, A326914, A326962, A327116, A327118."
			],
			"keyword": "nonn,tabl",
			"offset": "0,6",
			"author": "_Alois P. Heinz_, Sep 13 2019",
			"references": 9,
			"revision": 29,
			"time": "2019-10-04T13:10:27-04:00",
			"created": "2019-09-14T20:44:03-04:00"
		}
	]
}