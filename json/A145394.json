{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A145394",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 145394,
			"data": "1,1,2,3,2,4,4,5,5,6,4,10,6,8,8,11,6,13,8,14,12,12,8,20,11,14,14,20,10,24,12,21,16,18,16,31,14,20,20,30,14,32,16,28,26,24,16,42,21,31,24,34,18,40,24,40,28,30,20,56,22,32,36,43,28,48,24,42,32,48,24,65,26,38,42,48,32,56,28,62",
			"name": "Number of inequivalent sublattices of index n in hexagonal lattice, where two sublattices are considered equivalent if one can be rotated by a multiple of Pi/3 to give the other.",
			"comment": [
				"Also, apparently a(n) is the number of nonequivalent (up to lattice-preserving affine transformation) triangles on 2D square lattice of area n/2 [Karpenkov]. - _Andrey Zabolotskiy_, Jul 06 2017",
				"From _Andrey Zabolotskiy_, Jan 18 2018: (Start)",
				"The parent lattice of the sublattices under consideration has Patterson symmetry group p6, and two sublattices are considered equivalent if they are related via a symmetry from that group [Rutherford]. For other 2D Patterson groups, the analogous sequences are A000203 (p2), A069734 (p2mm), A145391 (c2mm), A145392 (p4), A145393 (p4mm), A003051 (p6mm).",
				"If we count sublattices related by parent-lattice-preserving reflection as equivalent, we get A003051 instead of this sequence. If we count sublattices related by rotation of the sublattice only (but not parent lattice; equivalently, sublattices related by rotation by angle which is not a multiple of Pi/3; see illustration in links) as equivalent, we get A054384. If we count sublattices related by any rotation or reflection as equivalent, we get A300651.",
				"Rutherford says at p. 161 that a(n) != A054384(n) only when A002324(n) \u003e 1, but actually these two sequences differ at other terms, too, for example, at n = 14 (see illustration). (End)"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A145394/b145394.txt\"\u003eTable of n, a(n) for n = 1..16384\u003c/a\u003e",
				"Oleg Karpenkov, \u003ca href=\"http://www.mscand.dk/article/viewFile/15058/13053\"\u003eElementary notions of lattice trigonometry\u003c/a\u003e, Mathematica Scandinavica, vol.102, no.2, pp.161-205, (2008) [See page 203].",
				"Oleg Karpenkov, \u003ca href=\"http://pcwww.liv.ac.uk/~karpenk/hab/habilitation.pdf\"\u003eGeometry of Lattice Angles, Polygons, and Cones\u003c/a\u003e, Thesis, Technische Universität Graz, 2009.",
				"Christian Kassel and Christophe Reutenauer, \u003ca href=\"https://arxiv.org/abs/1505.07229v3\"\u003eThe zeta function of the Hilbert scheme of n points on a two-dimensional torus\u003c/a\u003e, arXiv:1505.07229v3 [math.AG], 2015. [Note that a later version of this paper has a different title and different contents, and the number-theoretical part of the paper was moved to the publication which is next in this list.]",
				"Christian Kassel and Christophe Reutenauer, \u003ca href=\"https://arxiv.org/abs/1610.07793\"\u003eComplete determination of the zeta function of the Hilbert scheme of n points on a two-dimensional torus\u003c/a\u003e, arXiv:1610.07793 [math.NT], 2016.",
				"John S. Rutherford, \u003ca href=\"http://dx.doi.org/10.1107/S010876730804333X\"\u003eSublattice enumeration. IV. Equivalence classes of plane sublattices by parent Patterson symmetry and colour lattice group type\u003c/a\u003e, Acta Cryst. (2009). A65, 156-163. [See Table 2.]",
				"Andrey Zabolotskiy, \u003ca href=\"/A145394/a145394.pdf\"\u003eSublattices of the hexagonal lattice\u003c/a\u003e (illustrations for n = 1..7, 14)",
				"Andrey Zabolotskiy, \u003ca href=\"https://arxiv.org/abs/2003.10251\"\u003eCoweight lattice A^*_n and lattice simplices\u003c/a\u003e, arXiv:2003.10251 [math.CO], 2020.",
				"\u003ca href=\"/index/Su#sublatts\"\u003eIndex entries for sequences related to sublattices\u003c/a\u003e",
				"\u003ca href=\"/index/Aa#A2\"\u003eIndex entries for sequences related to A2 = hexagonal = triangular lattice\u003c/a\u003e"
			],
			"formula": [
				"a(n) = (A000203(n) + 2 * A002324(n))/3. [Rutherford] - _N. J. A. Sloane_, Mar 13 2009",
				"a(n) = Sum_{ m: m^2|n } A000086(n/m^2) + A157227(n/m^2) = A002324(n) + Sum_{ m: m^2|n } A157227(n/m^2). [Rutherford] - _Andrey Zabolotskiy_, Apr 23 2018",
				"a(n) = Sum_{ d|n } A008611(d-1). - _Andrey Zabolotskiy_, Aug 29 2019"
			],
			"mathematica": [
				"a[n_] := (DivisorSigma[1, n] + 2 DivisorSum[n, Switch[Mod[#, 3], 1, 1, 2, -1, 0, 0] \u0026])/3; Array[a, 80] (* _Jean-François Alcover_, Dec 03 2015 *)"
			],
			"program": [
				"(PARI)",
				"A002324(n) = if( n\u003c1, 0, sumdiv(n, d, (d%3==1) - (d%3==2)));",
				"A000203(n) = if( n\u003c1, 0, sigma(n));",
				"a(n) = (A000203(n) + 2 * A002324(n)) / 3;",
				"\\\\ _Joerg Arndt_, Oct 13 2013"
			],
			"xref": [
				"Cf. A054384, A000203, A069734, A145391, A145392, A145393, A003051, A002324, A002654, A069735, A145390, A300651, A000086, A157227, A008611."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_, Feb 23 2009",
			"ext": [
				"New name from _Andrey Zabolotskiy_, Dec 14 2017"
			],
			"references": 10,
			"revision": 67,
			"time": "2020-07-03T06:22:04-04:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}