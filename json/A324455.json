{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A324455",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 324455,
			"data": "6,10,12,14,15,18,20,21,22,24,26,28,30,33,34,36,38,39,40,42,44,45,46,48,50,51,52,54,56,57,58,60,62,63,65,66,68,69,70,72,74,75,76,78,80,82,84,85,86,87,88,90,91,92,93,94,95,96,98,99,100,102,104,105",
			"name": "Numbers m \u003e 1 such that there exists a divisor g \u003e 1 of m which satisfies s_g(m) \u003e= g.",
			"comment": [
				"The function s_g(m) gives the sum of the base-g digits of m.",
				"The sequence is infinite, since it contains A324460 and the Carmichael numbers A002997.",
				"A term m must have at least 2 prime factors, and the divisor g satisfies the inequalities 1 \u003c g \u003c m^(1/(ord_g(m)+1)) \u003c= sqrt(m), where ord_g(m) gives the maximum exponent e such that g^e divides m.",
				"See Kellner 2019."
			],
			"link": [
				"Bernd C. Kellner, \u003ca href=\"/A324455/b324455.txt\"\u003eTable of n, a(n) for n = 1..743\u003c/a\u003e",
				"Bernd C. Kellner, \u003ca href=\"https://arxiv.org/abs/1902.11283\"\u003eOn primary Carmichael numbers\u003c/a\u003e, arXiv:1902.11283 [math.NT], 2019."
			],
			"example": [
				"6 is a member, since 2 divides 6 and s_2(6) = 2."
			],
			"mathematica": [
				"s[n_, g_] := If[n \u003c 1 || g \u003c 2, 0, Plus @@ IntegerDigits[n, g]];",
				"f[n_] := AnyTrue[Divisors[n], s[n, #] \u003e= # \u0026];",
				"Select[Range[1000], f[#] \u0026]"
			],
			"xref": [
				"Subsequences are A002997, A324315, A324316, A324456, A324457, A324458, A324459, A324460."
			],
			"keyword": "nonn,base",
			"offset": "1,1",
			"author": "_Bernd C. Kellner_, Feb 28 2019",
			"references": 6,
			"revision": 21,
			"time": "2019-03-11T09:55:08-04:00",
			"created": "2019-03-11T09:55:08-04:00"
		}
	]
}