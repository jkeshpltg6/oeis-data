{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A264774",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 264774,
			"data": "1,5,1,45,6,1,455,55,7,1,4845,560,66,8,1,53130,5985,680,78,9,1,593775,65780,7315,816,91,10,1,6724520,736281,80730,8855,969,105,11,1,76904685,8347680,906192,98280,10626,1140,120,12,1,886163135,95548245,10295472,1107568,118755,12650,1330,136,13,1",
			"name": "Triangle T(n,k) = binomial(5*n - 4*k, 4*n - 3*k), 0 \u003c= k \u003c= n.",
			"comment": [
				"Riordan array (f(x),x*g(x)), where g(x) = 1 + x + 5*x^2 + 35*x^3 + 285*x^4 + ... is the o.g.f. for A002294 and f(x) = g(x)/(5 - 4*g(x)) = 1 + 5*x + 45*x^2 + 455*x^3 + 4845*x^4 + ... is the o.g.f. for A001449.",
				"More generally, if (R(n,k))n,k\u003e=0 is a proper Riordan array and m is a nonnegative integer and a \u003e b are integers then the array with (n,k)-th element R((m + 1)*n - a*k, m*n - b*k) is also a Riordan array (not necessarily proper). Here we take R as Pascal's triangle and m = a = 4 and b = 3. See A092392, A264772, A264773 and A113139 for further examples."
			],
			"link": [
				"P. Bala, \u003ca href=\"/A264772/a264772_1.pdf\"\u003eA 4-parameter family of embedded Riordan arrays\u003c/a\u003e",
				"E. Lebensztayn, \u003ca href=\"http://www.dmtcs.org/dmtcs-ojs/index.php/dmtcs/article/view/1517\"\u003eOn the asymptotic enumeration of accessible automata, Section 2\u003c/a\u003e, Discrete Mathematics and Theoretical Computer Science, Vol. 12, No. 3, 2010, 75-80",
				"R. Sprugnoli, \u003ca href=\"http://www.dsi.unifi.it/~resp/Handbook.pdf\"\u003eAn Introduction to Mathematical Methods in Combinatorics, Section 5.6\u003c/a\u003e CreateSpace Independent Publishing Platform 2006, ISBN-13: 978-1502925244"
			],
			"formula": [
				"T(n,k) = binomial(5*n - 4*k, n - k).",
				"O.g.f.: f(x)/(1 - t*x*g(x)), where f(x) = Sum_{n \u003e= 0} binomial(5*n,n)*x^n and g(x) = Sum_{n \u003e= 0} 1/(4*n + 1)*binomial(5*n,n)*x^n."
			],
			"example": [
				"Triangle begins",
				"  n\\k |       0      1     2    3   4   5   6   7",
				"------+---------------------------------------------",
				"   0  |       1",
				"   1  |       5      1",
				"   2  |      45      6     1",
				"   3  |     455     55     7    1",
				"   4  |    4845    560    66    8   1",
				"   5  |   53130   5985   680   78   9   1",
				"   6  |  593775  65780  7315  816  91  10   1",
				"   7  | 6724520 736281 80730 8855 969 105  11  1",
				"..."
			],
			"maple": [
				"A264774:= proc(n,k) binomial(5*n - 4*k, 4*n - 3*k); end proc:",
				"seq(seq(A264774(n,k), k = 0..n), n = 0..10);"
			],
			"mathematica": [
				"Table[Binomial[5 n - 4 k, 4 n - 3 k], {n, 0, 9}, {k, 0, n}] // Flatten (* _Michael De Vlieger_, Dec 01 2015 *)"
			],
			"program": [
				"(MAGMA) /* As triangle */ [[Binomial(5*n-4*k, 4*n-3*k): k in [0..n]]: n in [0.. 10]]; // _Vincenzo Librandi_, Dec 02 2015"
			],
			"xref": [
				"Cf. A001449 (column 0), A079589(column 1). Cf. A002294, A007318, A092392 (C(2n-k,n), A113139, A119301 (C(3n-k,n-k)), A264772, A264773."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "0,2",
			"author": "_Peter Bala_, Nov 30 2015",
			"references": 3,
			"revision": 23,
			"time": "2018-03-03T02:41:55-05:00",
			"created": "2015-12-12T15:22:24-05:00"
		}
	]
}