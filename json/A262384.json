{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A262384",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 262384,
			"data": "0,-1,5,-469,6515,-131672123,63427,-47800416479,15112153995391,-29632323552377537,4843119962464267,-1882558877249847563479,2432942522372150087,-2768809380553055597986831,334463513629004852735064113,-1125061940756859461946444233539,333807583501528759350875247323",
			"name": "Numerators of a semi-convergent series leading to the second Stieltjes constant gamma_2.",
			"comment": [
				"gamma_2 = - 1/60 + 5/336 - 469/21600 + 6515/133056 - 131672123/825552000 + ..., see formulas (46)-(47) in the reference below."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A262384/b262384.txt\"\u003eTable of n, a(n) for n = 1..237\u003c/a\u003e",
				"Iaroslav V. Blagouchine, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2015.06.012\"\u003eExpansions of generalized Euler's constants into the series of polynomials in 1/pi^2 and into the formal enveloping series with rational coefficients only\u003c/a\u003e, Journal of Number Theory (Elsevier), vol. 158, pp. 365-396, 2016. \u003ca href=\"http://arxiv.org/abs/1501.00740\"\u003earXiv version\u003c/a\u003e, arXiv:1501.00740 [math.NT], 2015."
			],
			"formula": [
				"a(n) = numerator(B_{2n}*(H^2_{2n-1}-H^(2)_{2n-1})/(2n)), where B_n, H_n and H^(k)_n are Bernoulli, harmonic and generalized harmonic numbers respectively.",
				"a(n) = numerator(-Zeta(1 - 2*n)*(Psi(1,2*n) + (Psi(0,2*n) + gamma)^2 - (Pi^2)/6)), where gamma is Euler's gamma and Psi is the digamma function. - _Peter Luschny_, Apr 19 2018"
			],
			"example": [
				"Numerators of 0/1, -1/60, 5/336, -469/21600, 6515/133056, -131672123/825552000, ..."
			],
			"maple": [
				"a := n -\u003e numer(-Zeta(1 - 2*n)*(Psi(1, 2*n) + (Psi(0,2*n) + gamma)^2 - (Pi^2)/6)):",
				"seq(a(n), n=1..17); # _Peter Luschny_, Apr 19 2018"
			],
			"mathematica": [
				"a[n_] := Numerator[BernoulliB[2*n]*(HarmonicNumber[2*n - 1]^2 - HarmonicNumber[2*n - 1, 2])/(2*n)]; Table[a[n], {n, 1, 20}]"
			],
			"program": [
				"(PARI) a(n) = numerator(bernfrac(2*n)*(sum(k=1,2*n-1,1/k)^2 - sum(k=1,2*n-1,1/k^2))/(2*n)); \\\\ _Michel Marcus_, Sep 23 2015"
			],
			"xref": [
				"The sequence of denominators is A262385.",
				"Cf. A001067, A001620, A002206, A006953, A075266, A082633, A086279, A086280, A195189, A262235, A262382, A262383, A262386, A262387."
			],
			"keyword": "frac,sign",
			"offset": "1,3",
			"author": "_Iaroslav V. Blagouchine_, Sep 20 2015",
			"references": 6,
			"revision": 37,
			"time": "2018-08-29T02:50:54-04:00",
			"created": "2015-11-24T18:37:52-05:00"
		}
	]
}