{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A224164",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 224164,
			"data": "2,3,5,7,1117,1171,7331,131983991,179907191,179991179,191199311,191739971,191797919,199199311,199709971,199937971,337353739,373151113,733353337,797389337,919311739,971727179,1193100992213191,1193120192093911,1193123793413719",
			"name": "Prime numbers with m^2 digits that, if arranged in an m X m matrix, form m-digit reversible primes in each row, column, and main diagonal.",
			"comment": [
				"Number of primes with m^2 digits in this sequence are 4, 3, 15, 86, ...",
				"Smallest primes with m^2 digits in this sequence are 2, 1117, 131983991, 1193100992213191, ...",
				"Largest primes with m^2 digits in this sequence are 7, 7331, 971727179, 9931722992931193, ...",
				"Palindromic primes in this sequence are 2, 3, 5, 7, 733353337, 971727179, ...",
				"There are 1303816 terms with 25 digits, from 1119710007309831033317939 to 9979399989793939049937997, while the terms with 36 digits range from 111119100049100049150607134777979313 to 999931999983999983792293733331319919. - _Giovanni Resta_, Apr 05 2013"
			],
			"reference": [
				"Chris K. Caldwell; G. L. Honaker, Jr.: Prime Curios! The Dictionary of Prime Number Trivia. CreateSpace 2009, p. 219, and 229."
			],
			"link": [
				"Martin Renner and Giovanni Resta, \u003ca href=\"/A224164/b224164.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (first 108 terms from Martin Renner)",
				"Chris K. Caldwell and G. L. Honaker, Jr., \u003ca href=\"http://primes.utm.edu/curios/page.php?number_id=621\"\u003ePrime Curios! 733353337\u003c/a\u003e.",
				"Chris K. Caldwell and G. L. Honaker, Jr., \u003ca href=\"http://primes.utm.edu/curios/page.php?number_id=662\"\u003ePrime Curios! 3391382115599173\u003c/a\u003e.",
				"Chris K. Caldwell and G. L. Honaker, Jr., \u003ca href=\"http://primes.utm.edu/curios/page.php?number_id=699\"\u003ePrime Curios! 19973...37991 (25-digits)\u003c/a\u003e."
			],
			"example": [
				"a(5) = 1117 is the smallest 4-digit prime, that if arranged in a 2 X 2 matrix yields to each row, column, and main diagonal being prime in both directions, i. e.",
				"  11",
				"  17",
				"-\u003e 11 (4 times), 17 (3 times), 71 (3 times) are all reversible primes.",
				"a(8) = 131983991 is the smallest 9-digit prime, that if arranged in a 3 X 3 matrix yields to each row, column, and main diagonal being prime in both directions, i. e.",
				"  131",
				"  983",
				"  991",
				"-\u003e 131 (4 times), 181, 199, 389, 983, 991 (each 2 times) are all reversible primes.",
				"a(23) = 1193100992213191 is the smallest 16-digit prime, that if arranged in a 4 X 4 matrix yields to each row, column, and main diagonal being prime in both directions, i. e.",
				"  1193",
				"  1009",
				"  9221",
				"  3191",
				"-\u003e 1009, 1021 (2 times), 1193 (3 times), 1201 (2 times), 1229, 1913, 3191, 3911 (3 times), 9001, 9029, 9209, 9221 are all reversible primes."
			],
			"maple": [
				"# Maple-program generating all 4-digit primes",
				"M:={}: for a in [1,3,7,9] do for b in [1,3,7,9] do if isprime(10*a+b) and isprime(10*b+a) then for c in [1,3,7,9] do for d in [1,3,7,9] do if isprime(10*c+d) and isprime(10*d+c) and isprime(10*a+c) and isprime(10*c+a) and isprime(10*b+d) and isprime(10*d+b) and isprime(10*a+d) and isprime(10*d+a) then S:=[a,b,c,d]: if isprime(add(S[j]*10^(4-j),j=1..4)) then M:={op(M),add(S[j]*10^(4-j),j=1..4)}: fi: fi: od: od: fi: od: od: M;",
				"# Maple-program generating all 9-digit primes",
				"M:={}: for d in [1,3,7,9] do for e from 0 to 9 do for f in [1,3,7,9] do if isprime(100*d+10*e+f) and isprime(100*f+10*e+d) then for a in [1,3,7,9] do for b in [1,3,7,9] do for c in [1,3,7,9] do if isprime(100*a+10*b+c) and isprime(100*c+10*b+a) then for g in [1,3,7,9] do for h in [1,3,7,9] do for i in [1,3,7,9] do if isprime(100*g+10*h+i) and isprime(100*i+10*h+g) and isprime(100*a+10*d+g) and isprime(100*g+10*d+a) and isprime(100*b+10*e+h) and isprime(100*h+10*e+b) and isprime(100*c+10*f+i) and isprime(100*i+10*f+c) and isprime(100*a+10*e+i) and isprime(100*i+10*e+a) then S:=[a,b,c,d,e,f,g,h,i]: if isprime(add(S[j]*10^(9-j),j=1..9)) then M:={op(M),add(S[j]*10^(9-j),j=1..9)}: fi: fi: od: od: od: fi: od: od: od: fi: od: od: od: M;"
			],
			"xref": [
				"Cf. A224398."
			],
			"keyword": "nonn,base",
			"offset": "1,1",
			"author": "_Martin Renner_, Mar 31 2013",
			"references": 2,
			"revision": 19,
			"time": "2014-02-07T05:58:53-05:00",
			"created": "2013-04-09T11:58:01-04:00"
		}
	]
}