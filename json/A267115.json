{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A267115",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 267115,
			"data": "0,1,1,2,1,1,1,3,2,1,1,0,1,1,1,4,1,0,1,0,1,1,1,1,2,1,3,0,1,1,1,5,1,1,1,2,1,1,1,1,1,1,1,0,0,1,1,0,2,0,1,0,1,1,1,1,1,1,1,0,1,1,0,6,1,1,1,0,1,1,1,2,1,1,0,0,1,1,1,0,4,1,1,0,1,1,1,1,1,0,1,0,1,1,1,1,1,0,0,2,1,1,1,1,1,1,1,2,1,1,1,0,1,1,1,0,0,1,1,1",
			"name": "Bitwise-AND of the exponents of primes in the prime factorization of n, a(1) = 0.",
			"link": [
				"Antti Karttunen, \u003ca href=\"/A267115/b267115.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"\u003ca href=\"/index/Eu#epf\"\u003eIndex entries for sequences computed from exponents in factorization of n\u003c/a\u003e"
			],
			"formula": [
				"If A028234(n) = 1 [when n is a power of prime, in A000961], a(n) = A067029(n), otherwise a(n) = A067029(n) AND a(A028234(n)). [Here AND stands for bitwise-and, A004198.]"
			],
			"example": [
				"For n = 24 = 2^3 * 3^1, bitwise-and of 3 and 1 (\"11\" and \"01\" in binary) gives 1, thus a(24) = 1.",
				"For n = 210 = 2^1 * 3^1 * 5^1 * 7^1, bitwise-and of 1, 1, 1 and 1 gives 1, thus a(210) = 1.",
				"For n = 720 = 2^4 * 3^2 * 5^1, bitwise-and of 4, 2 and 1 (\"100\", \"10\" and \"1\" in binary) gives zero, thus a(720) = 0."
			],
			"mathematica": [
				"{0}~Join~Table[BitAnd @@ Map[Last, FactorInteger@ n], {n, 2, 120}] (* _Michael De Vlieger_, Feb 07 2016 *)"
			],
			"program": [
				"(Scheme, two variants)",
				"(define (A267115 n) (let loop ((n (A028234 n)) (z (A067029 n))) (cond ((= 1 n) z) (else (loop (A028234 n) (A004198bi z (A067029 n))))))) ;; A004198bi implements bitwise-and (see A004198).",
				";; A recursive version using memoizing definec-macro:",
				"(definec (A267115 n) (if (= 1 (A028234 n)) (A067029 n) (A004198bi (A067029 n) (A267115 (A028234 n)))))",
				"(PARI) a(n)=my(f = factor(n)[,2]); if (#f == 0, return (0)); my(b = f[1]); for (k=2, #f, b = bitand(b, f[k]);); b; \\\\ _Michel Marcus_, Feb 07 2016",
				"(PARI) a(n)=if(n\u003e1, fold(bitand, factor(n)[,2]), 0) \\\\ _Charles R Greathouse IV_, Aug 04 2016"
			],
			"xref": [
				"Cf. A000961, A004198, A028234, A067029, A267114, A267116, A268387.",
				"Cf. A002035 (indices of odd numbers), A072587 (indices of even numbers that occur after a(1)).",
				"Cf. A267117 (indices of zeros)."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Antti Karttunen_, Feb 03 2016",
			"references": 11,
			"revision": 27,
			"time": "2017-03-07T00:11:19-05:00",
			"created": "2016-02-05T20:42:30-05:00"
		}
	]
}