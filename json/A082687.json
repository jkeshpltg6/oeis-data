{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A082687",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 82687,
			"data": "1,7,37,533,1627,18107,237371,95549,1632341,155685007,156188887,3602044091,18051406831,7751493599,225175759291,13981692518567,14000078506967,98115155543129,3634060848592973,3637485804655193",
			"name": "Numerator of Sum_{k=1..n} 1/(n+k).",
			"comment": [
				"Numerator of Sum_{k=0..n-1} 1/((k+1)(2k+1)) (denominator is A111876). - _Paul Barry_, Aug 19 2005",
				"Numerator of the sum of all matrix elements of n X n Hilbert matrix M(i,j) = 1/(i+j-1) (i,j = 1..n). - _Alexander Adamchuk_, Apr 11 2006",
				"Numerator of the 2n-th alternating harmonic number H'(2n) = Sum ((-1)^(k+1)/k, k=1..2n). H'(2n) = H(2n) - H(n), where H(n) = Sum_{k=1..n} 1/k is the n-th Harmonic Number. - _Alexander Adamchuk_, Apr 11 2006",
				"a(n) almost always equals A117731(n) = numerator(n*Sum_{k=1..n} 1/(n+k)) = numerator(Sum_{j=1..n} Sum_{i=1..n} 1/(i+j-1)) but differs for n = 14, 53, 98, 105, 111, 114, 119, 164. - _Alexander Adamchuk_, Jul 16 2006",
				"Sum_{k=1..n} 1/(n+k) = n!^2 *Sum_{j=1..n} (-1)^(j+1) /((n+j)!(n-j)!j). - _Leroy Quet_, May 20 2007",
				"Seems to be the denominator of the harmonic mean of the first n hexagonal numbers. - _Colin Barker_, Nov 19 2014",
				"Numerator of 2*n*binomial(2*n,n)*Sum_{k = 0..n-1} (-1)^k* binomial(n-1,k)/(n+k+1)^2. Cf. A049281. - _Peter Bala_, Feb 21 2017"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A082687/b082687.txt\"\u003eTable of n, a(n) for n = 1..100\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/HilbertMatrix.html\"\u003eHilbert Matrix\u003c/a\u003e."
			],
			"formula": [
				"limit n--\u003einfinity Sum_{k=1..n} 1/(n+k) = log(2).",
				"Numerator of Psi(2*n+1) - Psi(n+1). - _Vladeta Jovovic_, Aug 24 2003",
				"a(n) = numerator(Sum_{k=1..2*n} 1/k) - Sum_{k=1..n} 1/k). - _Alexander Adamchuk_, Apr 11 2006",
				"a(n) = numerator(Sum_{j=1..n} (Sum_{i=1..n} 1/(i+j-1))). - _Alexander Adamchuk_, Apr 11 2006",
				"The o.g.f for Sum_{k=1..n} 1/(n+k) is f(x) = (sqrt(x)*log((1+sqrt(x))/(1-sqrt(x))) + log(1-x))/(2*x*(1-x))."
			],
			"example": [
				"H'(2n) = H(2n) - H(n) = {1/2, 7/12, 37/60, 533/840, 1627/2520, 18107/27720, 237371/360360, 95549/144144, 1632341/2450448, 155685007/232792560, ...}, where H(n) = A001008/A002805.",
				"n=2: HilbertMatrix(n,n)",
				"1 1/2",
				"1/2 1/3",
				"so a(2) = Numerator(1 + 1/2 + 1/2 + 1/3) = Numerator(7/3) = 7.",
				"The n X n Hilbert matrix begins:",
				"   1   1/2  1/3  1/4  1/5  1/6  1/7  1/8  ...",
				"  1/2  1/3  1/4  1/5  1/6  1/7  1/8  1/9  ...",
				"  1/3  1/4  1/5  1/6  1/7  1/8  1/9  1/10 ...",
				"  1/4  1/5  1/6  1/7  1/8  1/9  1/10 1/11 ...",
				"  1/5  1/6  1/7  1/8  1/9  1/10 1/11 1/12 ...",
				"  1/6  1/7  1/8  1/9  1/10 1/11 1/12 1/13 ..."
			],
			"maple": [
				"a := n -\u003e numer(harmonic(2*n) - harmonic(n)):",
				"seq(a(n), n=1..20); # _Peter Luschny_, Nov 02 2017"
			],
			"mathematica": [
				"Numerator[Sum[1/k,{k,1,2*n}] - Sum[1/k,{k,1,n}]] (* _Alexander Adamchuk_, Apr 11 2006 *)",
				"Table[Numerator[Sum[1/(i + j - 1), {i, n}, {j, n}]], {n, 20}] (* _Alexander Adamchuk_, Apr 11 2006 *)",
				"Table[HarmonicNumber[2 n] - HarmonicNumber[n], {n, 20}] // Numerator (* _Eric W. Weisstein_, Dec 14 2017 *)"
			],
			"program": [
				"(PARI) a(n) = numerator(sum(k=1, n, 1/(n+k))); \\\\ _Michel Marcus_, Dec 14 2017"
			],
			"xref": [
				"Bisection of A058313. A082688 (denominators).",
				"Cf. A001008, A002805, A058312, A098118, A086881, A005249, A001008, A117731."
			],
			"keyword": "frac,nonn,easy",
			"offset": "1,2",
			"author": "_Benoit Cloitre_, Apr 12 2003",
			"references": 11,
			"revision": 51,
			"time": "2017-12-14T05:22:30-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}