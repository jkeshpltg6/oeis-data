{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A085541",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 85541,
			"data": "1,7,4,7,6,2,6,3,9,2,9,9,4,4,3,5,3,6,4,2,3,1,1,3,3,1,4,6,6,5,7,0,6,7,0,0,9,7,5,4,1,2,1,2,1,9,2,6,1,4,9,2,8,9,8,8,8,6,7,2,0,1,6,7,0,1,6,3,1,5,8,9,5,2,8,1,2,9,5,8,7,6,3,5,6,3,4,2,0,0,5,3,6,9,7,2,5,6,0,5,4,6,7,9,1",
			"name": "Decimal expansion of the prime zeta function at 3.",
			"comment": [
				"Mathar's Table 1 (cited below) lists expansions of the prime zeta function at integers s in 10..39. - _Jason Kimberley_, Jan 05 2017"
			],
			"reference": [
				"Henri Cohen, Number Theory, Volume II: Analytic and Modern Tools, GTM Vol. 240, Springer, 2007; see pp. 208-209.",
				"J. W. L. Glaisher, On the Sums of Inverse Powers of the Prime Numbers, Quart. J. Math. 25, 347-362, 1891."
			],
			"link": [
				"Jason Kimberley, \u003ca href=\"/A085541/b085541.txt\"\u003eTable of n, a(n) for n = 0..1497\u003c/a\u003e",
				"Henri Cohen, \u003ca href=\"http://www.math.u-bordeaux.fr/~cohen/hardylw.dvi\"\u003eHigh Precision Computation of Hardy-Littlewood Constants\u003c/a\u003e, Preprint, 1998.",
				"Henri Cohen, \u003ca href=\"/A221712/a221712.pdf\"\u003eHigh-precision computation of Hardy-Littlewood constants\u003c/a\u003e. [pdf copy, with permission]",
				"X. Gourdon and P. Sebah, \u003ca href=\"http://numbers.computation.free.fr/Constants/Miscellaneous/constantsNumTheory.html\"\u003eSome Constants from Number theory\u003c/a\u003e",
				"R. J. Mathar, \u003ca href=\"http://arxiv.org/abs/0803.0900\"\u003eSeries of reciprocal powers of k-almost primes\u003c/a\u003e, arXiv:0803.0900 [math.NT], 2008-2009. Table 1.",
				"Gerhard Niklasch and Pieter Moree, \u003ca href=\"/A001692/a001692.html\"\u003eSome number-theoretical constants\u003c/a\u003e [Cached copy]",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PrimeZetaFunction.html\"\u003ePrime Zeta Function\u003c/a\u003e"
			],
			"formula": [
				"P(3) = Sum_{p prime} 1/p^3 = Sum_{n\u003e=1} mobius(n)*log(zeta(3*n))/n. - Antonio G. Astudillo (afg_astudillo(AT)lycos.com), Jul 06 2003",
				"Equals A086033 + A085992 + 1/8. - _R. J. Mathar_, Jul 22 2010",
				"Equals Sum_{k\u003e=1} 1/A030078(k). - _Amiram Eldar_, Jul 27 2020"
			],
			"example": [
				"0.1747626392994435364231..."
			],
			"maple": [
				"A085541:= proc(i) print(evalf(add(1/ithprime(k)^3,k=1..i),100)); end:",
				"A085541(100000); # _Paolo P. Lava_, May 29 2012"
			],
			"mathematica": [
				"(* If Mathematica version \u003e= 7.0 then RealDigits[PrimeZetaP[3]//N[#,105]\u0026][[1]] else : *) m = 200; $MaxExtraPrecision = 200; PrimeZetaP[s_] := NSum[MoebiusMu[k]*Log[Zeta[k*s]]/k, {k, 1, m}, AccuracyGoal -\u003e m, NSumTerms -\u003e m, PrecisionGoal -\u003e m, WorkingPrecision -\u003e m]; RealDigits[PrimeZetaP[3]][[1]][[1 ;; 105]] (* _Jean-François Alcover_, Jun 24 2011 *)"
			],
			"program": [
				"(PARI) recip3(n) = { v=0; p=1; forprime(y=2,n, v=v+1./y^3; ); print(v) }",
				"(PARI) sumeulerrat(1/p,3) \\\\ _Hugo Pfoertner_, Feb 03 2020",
				"(MAGMA) R := RealField(106);",
				"PrimeZeta := func\u003ck,N|",
				"\u0026+[R|MoebiusMu(n)/n*Log(ZetaFunction(R,k*n)):n in[1..N]]\u003e;",
				"Reverse(IntegerToSequence(Floor(PrimeZeta(3,117)*10^105)));",
				"// _Jason Kimberley_, Dec 30 2016"
			],
			"xref": [
				"Decimal expansion of the prime zeta function: A085548 (at 2), this sequence (at 3), A085964 (at 4) to A085969 (at 9).",
				"Cf. A002117, A030078, A242302."
			],
			"keyword": "easy,nonn,cons",
			"offset": "0,2",
			"author": "_Cino Hilliard_, Jul 02 2003",
			"ext": [
				"More terms from Antonio G. Astudillo (afg_astudillo(AT)lycos.com), Jul 06 2003"
			],
			"references": 34,
			"revision": 56,
			"time": "2020-07-27T07:56:56-04:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}