{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A290731",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 290731,
			"data": "1,1,2,2,3,2,4,4,4,3,6,4,7,4,6,8,9,4,10,6,8,6,12,8,11,7,11,8,15,6,16,16,12,9,12,8,19,10,14,12,21,8,22,12,12,12,24,16,22,11,18,14,27,11,18,16,20,15,30,12,31,16,16,32,21,12,34,18,24,12,36,16,37,19,22,20,24,14,40,24",
			"name": "Number of distinct values of X*(X+1) mod n.",
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A290731/b290731.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Andreas Enge, William Hart, Fredrik Johansson, \u003ca href=\"http://arxiv.org/abs/1608.06810\"\u003eShort addition sequences for theta functions\u003c/a\u003e, arXiv:1608.06810 [math.NT], (24-August-2016). See Table 5."
			],
			"formula": [
				"Multiplicative with a(2^e) = 2^(e-1), a(p^2) = 1 + floor(p^(e+1)/(2*p+2)) for odd prime p. - _Andrew Howroyd_, Aug 01 2018"
			],
			"example": [
				"The values taken by X^2+X mod n for small n are:",
				"1, [0]",
				"2, [0]",
				"3, [0, 2]",
				"4, [0, 2]",
				"5, [0, 1, 2]",
				"6, [0, 2]",
				"7, [0, 2, 5, 6]",
				"8, [0, 2, 4, 6]",
				"9, [0, 2, 3, 6]",
				"10, [0, 2, 6]",
				"11, [0, 1, 2, 6, 8, 9]",
				"12, [0, 2, 6, 8]",
				"..."
			],
			"maple": [
				"a:=[]; M:=80;",
				"for n from 1 to M do",
				"q1:={};",
				"for i from 0 to n-1 do q1:={op(q1), (i^2+i) mod n}; od;",
				"s1:=sort(convert(q1,list));",
				"a:=[op(a),nops(s1)];",
				"od:",
				"a;"
			],
			"mathematica": [
				"a[n_] := Product[{p, e} = pe; If[p==2, 2^(e-1), 1+Quotient[p^(e+1), (2p+2)]], {pe, FactorInteger[n]}];",
				"Array[a, 100] (* _Jean-François Alcover_, Aug 05 2018, after _Andrew Howroyd_ *)"
			],
			"program": [
				"(PARI) a(n)={my(v=vector(n)); for(i=0, n-1, v[i*(i+1)%n + 1]=1); vecsum(v)} \\\\ _Andrew Howroyd_, Aug 01 2018",
				"(PARI) a(n)={my(f=factor(n)); prod(i=1, #f~, my([p,e]=f[i,]); if(p==2, 2^(e-1), 1 + p^(e+1)\\(2*p+2)))} \\\\ _Andrew Howroyd_, Aug 01 2018"
			],
			"xref": [
				"Cf. A000224 (analog for X^2), A290732."
			],
			"keyword": "nonn,mult",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_, Aug 10 2017",
			"references": 7,
			"revision": 18,
			"time": "2018-08-05T11:30:49-04:00",
			"created": "2017-08-10T12:27:44-04:00"
		}
	]
}