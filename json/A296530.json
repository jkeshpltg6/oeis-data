{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A296530",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 296530,
			"data": "1,1,1,1,2,2,5,10,28,24,50,124,283,528,1266,3715,10702,8740,15414,31988,68465,160964,380124,890738,2230219,3990852,8354276,20281732,46056920,131289988,349369117,1054037937,3081527146,2440225484,4201202020,7475926894,13276918426",
			"name": "Number of non-averaging permutations of [n] with first element n.",
			"comment": [
				"A non-averaging permutation avoids any 3-term arithmetic progression.",
				"a(0) = 1 by convention."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A296530/b296530.txt\"\u003eTable of n, a(n) for n = 0..99\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/NonaveragingSequence.html\"\u003eNonaveraging Sequence\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Arithmetic_progression\"\u003eArithmetic progression\u003c/a\u003e",
				"\u003ca href=\"/index/No#non_averaging\"\u003eIndex entries related to non-averaging sequences\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A296529(n,n)."
			],
			"example": [
				"a(4) = 2: 4213, 4231.",
				"a(5) = 2: 51324, 51342.",
				"a(6) = 5: 621453, 624153, 624315, 624351, 624513.",
				"a(7) = 10: 7312564, 7315264, 7315426, 7315462, 7315624, 7351264, 7351426, 7351462, 7351624, 7356124."
			],
			"maple": [
				"b:= proc(s) option remember; local n, r, ok, i, j, k;",
				"      if nops(s) = 1 then 1",
				"    else n, r:= max(s), 0;",
				"         for j in s minus {n} do ok, i, k:= true, j-1, j+1;",
				"           while ok and i\u003e=0 and k\u003cn do ok, i, k:=",
				"             not i in s xor k in s, i-1, k+1 od;",
				"           r:= r+ `if`(ok, b(s minus {j}), 0)",
				"         od; r",
				"      fi",
				"    end:",
				"a:= n-\u003e b({$0..n} minus {n-1}):",
				"seq(a(n), n=0..30);"
			],
			"mathematica": [
				"b[s_] := b[s] = Module[{n = Max[s], r = 0, ok, i, j, k}, If[Length[s] == 1, 1, Do[{ok, i, k} = {True, j - 1, j + 1}; While[ok \u0026\u0026 i \u003e= 0 \u0026\u0026 k \u003c n, {ok, i, k} = {FreeQ[s, i] ~Xor~ MemberQ[s, k], i - 1, k + 1}]; r = r + If[ok, b[s ~Complement~ {j}], 0], {j, s ~Complement~ {n}}]; r]];",
				"a[n_] := b[Complement[Range[0, n], {n - 1}]]",
				"Table[a[n], {n, 0, 30}] (* _Jean-François Alcover_, Jun 02 2018, from Maple *)"
			],
			"xref": [
				"Main diagonal of A296529.",
				"Cf. A003407, A292523."
			],
			"keyword": "nonn",
			"offset": "0,5",
			"author": "_Alois P. Heinz_, Dec 14 2017",
			"references": 2,
			"revision": 19,
			"time": "2018-06-02T10:38:06-04:00",
			"created": "2017-12-15T09:42:08-05:00"
		}
	]
}