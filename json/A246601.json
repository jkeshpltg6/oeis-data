{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A246601",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 246601,
			"data": "1,2,4,4,6,8,8,8,10,12,12,16,14,16,24,16,18,20,20,24,22,24,24,32,26,28,40,32,30,48,32,32,34,36,36,40,38,40,43,48,42,44,44,48,60,48,48,64,50,52,72,56,54,80,61,64,58,60,60,96,62,64,104,64,66,68,68,72,70,72",
			"name": "Sum of divisors d of n with property that the binary representation of d can be obtained from the binary representation of n by changing any number of 1's to 0's.",
			"comment": [
				"Equivalently, the sum of the divisors d of n such that the bitwise OR of d and n is equal to n. - _Chai Wah Wu_, Sep 06 2014"
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/A246601/b246601.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"a(2^i) = 2^i.",
				"a(odd prime p) = p+1."
			],
			"example": [
				"12 = 1100_2; only the divisors 4 = 0100_2 and 12 = 1100_2 satisfy the condition, so(12) = 4+12 = 16.",
				"15 = 1111_2; all divisors 1,3,5,15 satisfy the condition, so a(15)=24."
			],
			"maple": [
				"with(numtheory);",
				"sd:=proc(n) local a,d,s,t,i,sw;",
				"s:=convert(n,base,2);",
				"a:=0;",
				"for d in divisors(n) do",
				"sw:=-1;",
				"t:=convert(d,base,2);",
				"for i from 1 to nops(t) do if t[i]\u003es[i] then sw:=1; fi; od:",
				"if sw\u003c0 then a:=a+d; fi;",
				"od;",
				"a;",
				"end;",
				"[seq(sd(n),n=1..100)];"
			],
			"mathematica": [
				"a[n_] := DivisorSum[n, #*Boole[BitOr[#, n] == n] \u0026]; Array[a, 100] (* _Jean-François Alcover_, Dec 02 2015, adapted from PARI *)"
			],
			"program": [
				"(Python)",
				"from sympy import divisors",
				"def A246601(n):",
				"....return sum(d for d in divisors(n) if n|d == n)",
				"# _Chai Wah Wu_, Sep 06 2014",
				"(PARI) a(n) = sumdiv(n, d, d*(bitor(n,d)==n)); \\\\ _Michel Marcus_, Sep 07 2014"
			],
			"xref": [
				"Cf. A000005, A000203, A246600."
			],
			"keyword": "nonn,base",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, Sep 06 2014",
			"references": 2,
			"revision": 19,
			"time": "2015-12-03T04:31:19-05:00",
			"created": "2014-09-06T14:02:48-04:00"
		}
	]
}