{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A327263",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 327263,
			"data": "3,5,2,9,3,2,13,5,3,2,21,7,4,3,2,25,11,5,4,3,2,33,13,7,5,4,3,2,37,17,9,6,5,4,3,2,45,19,10,7,6,5,4,3,2,57,23,13,9,7,6,5,4,3,2,61,29,15,11,8,7,6,5,4,3,2,73,31,17,12,9,8,7,6,5,4,3,2",
			"name": "Array T(n,k) in which the i-th row consists of numbers \u003e 1 not in the array U(i;n,k) = (i*n*k - (i-2)*A319929(n,k))/2 where i \u003e= 1, n \u003e= 1 and k \u003e= 1, read by antidiagonals.",
			"comment": [
				"All the U(i;n,k) mimic the ordinary multiplication table in that they are commutative, associative, have identity element 1 and have 0. However (except when i=2) they are partially distributive, meaning that distributivity works except if an even number is partitioned into a sum of two odd numbers. Only when i=2, the odd-even-dependent A319929 term disappears and normal distributivity holds.",
				"U(0;n,k) = A319929(n,k);",
				"U(1;n,k) = A322630(n,k);",
				"U(2;n,k) = n*k;",
				"U(3;n,k) = A322744(n,k);",
				"U(4;n,k) = A327259(n,k);",
				"U(i;n,k) = 2i*floor(n/2)*floor(k/2) + A319929(n,k).",
				"Row 1 is 2p-1 where p is a prime number (A076274 without 1).",
				"Row 2 is the prime numbers.",
				"Row 3 is A307002 without 1.",
				"Row 4 is A327261 without 1.",
				"The i-th row of T(n,k) consists of numbers that sieve out of the array U(i;n,k) = (i*n*k - (i-2)*A319929(n,k))/2, in numerical order.",
				"From _David Lovler_, Sep 02 2020: (Start)",
				"Row 1 has no even numbers. Row 2 has one even number. Generally, the even numbers of the i-th row start with i-1 consecutive even numbers (from 2). This is because U(i;2,2) = 2*i gives the first even number not in row i.",
				"Row 3 seems to have even numbers that, after 2, coincide with A112774 which has an infinite number of terms. For i \u003e 3, as i increases, row i has a denser presence of even numbers, thus each row has an infinite number of even terms.",
				"Generalization of the twin prime conjecture: Since row 2 is the prime numbers, we can observe the twin prime conjecture that after the first three odd primes, the sprinkling of pairs of consecutive prime numbers never ends. Concerning just odd terms, a similar conjecture can be stated for rows i \u003e= 3. Row 3 starts with four odd numbers then the sprinkling of three consecutive odd number never ends. Row 4 starts with five odd numbers then the sprinkling of four consecutive odd numbers never ends. The pattern continues as row i starts with i+1 odd numbers then the sprinkling of i consecutive odd numbers never ends. We can take this back to row 1 which starts with two odd numbers then continues with isolated odd numbers.",
				"Studying the even terms, there is an analog to the above generalization of the twin prime conjecture. Row 3 starts with two even numbers then continues with isolated even numbers. Row 4 starts with three even numbers then the sprinkling of pairs of consecutive even numbers never ends. Row 5 starts with four even numbers then the sprinkling of three consecutive even numbers never ends. The pattern continues as row i starts with i-1 even numbers then the sprinkling of i-2 consecutive even numbers never ends.",
				"(End)"
			],
			"formula": [
				"With one exception there are likely no formulas for the rows of T(n,k) since their creation is based on a sieving process like the familiar prime number sieve. The exception is T(1,k) = 2*T(2,k)-1."
			],
			"example": [
				"3  5  9  13  21  25  33  37  45  57  61  73  81  85  93 105 117 121 133 141 145 ...",
				"2  3  5   7  11  13  17  19  23  29  31  37  41  43  47  53  59  61  67  71  73 ...",
				"2  3  4   5   7   9  10  13  15  17  21  22  23  25  29  31  34  37  39  41  45 ...",
				"2  3  4   5   6   7   9  11  12  14  15  17  19  21  22  25  27  28  29  31  35 ...",
				"2  3  4   5   6   7   8   9  11  13  14  16  17  18  19  21  23  25  26  28  29 ...",
				"2  3  4   5   6   7   8   9  10  11  13  15  16  18  19  20  21  22  23  25  27 ...",
				"2  3  4   5   6   7   8   9  10  11  12  13  15  17  18  20  21  22  23  24  25 ...",
				"2  3  4   5   6   7   8   9  10  11  12  13  14  15  17  19  20  22  23  24  25 ...",
				"2  3  4   5   6   7   8   9  10  11  12  13  14  15  16  17  19  21  22  24  25 ...",
				"2  3  4   5   6   7   8   9  10  11  12  13  14  15  16  17  18  19  21  23  24 ...",
				"..."
			],
			"mathematica": [
				"row=12;max=200;U[i_,n_,k_]:=(i*n*k-(i-2)If[OddQ@n,If[OddQ@k,n+k-1,k],If[OddQ@k,n,0]])/2;t=Table[c=Union@Flatten@Table[U[i,n,k],{n,2,max},{k,2,max}];Complement[Range[2,max],c][[;;row]],{i,row}];Flatten@Table[t[[m,k-m+1]],{k,row},{m,k}] (* _Giorgos Kalogeropoulos_, Jun 08 2021 *)"
			],
			"xref": [
				"Cf. A000040, A003991, A076274, A112774.",
				"Cf. A319929, A322630, A322744, A327259, A345474."
			],
			"keyword": "nonn,tabl",
			"offset": "1,1",
			"author": "_David Lovler_, Oct 15 2019",
			"references": 12,
			"revision": 72,
			"time": "2021-10-29T02:24:53-04:00",
			"created": "2020-04-24T16:20:15-04:00"
		}
	]
}