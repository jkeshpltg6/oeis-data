{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A065339",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 65339,
			"data": "0,0,1,0,0,1,1,0,2,0,1,1,0,1,1,0,0,2,1,0,2,1,1,1,0,0,3,1,0,1,1,0,2,0,1,2,0,1,1,0,0,2,1,1,2,1,1,1,2,0,1,0,0,3,1,1,2,0,1,1,0,1,3,0,0,2,1,0,2,1,1,2,0,0,1,1,2,1,1,0,4,0,1,2,0,1,1,1,0,2,1,1,2,1,1,1,0,2,3,0,0,1,1,0,2",
			"name": "Number of primes congruent to 3 modulo 4 dividing n (with multiplicity).",
			"comment": [
				"(2^A007814(n)) * (3^a(n)) = A065338(n)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A065339/b065339.txt\"\u003eTable of n, a(n) for n=1..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A001222(n) - A007814(n) - A083025(n).",
				"From _Antti Karttunen_, Aug 14 2015: (Start)",
				"a(1) = a(2) = 0; thereafter, if n is even, a(n) = a(n/2), otherwise a(n) = ((A020639(n) mod 4)-1)/2 + a(n/A020639(n)). [Where A020639(n) gives the smallest prime factor of n.]",
				"Other identities and observations. For all n \u003e= 1:",
				"a(n) = A007949(A065338(n)).",
				"a(n) = A001222(A097706(n)).",
				"a(n) \u003e= A260728(n). [See A260730 for the positions of differences.]",
				"(End)"
			],
			"maple": [
				"A065339 := proc(n)",
				"    a := 0 ;",
				"    for f in ifactors(n)[2] do",
				"        if op(1,f) mod 4 = 3 then",
				"            a := a+op(2,f) ;",
				"        end if;",
				"    end do:",
				"    a ;",
				"end proc: # _R. J. Mathar_, Dec 16 2011"
			],
			"mathematica": [
				"f[n_]:=Plus@@Last/@Select[If[n==1,{},FactorInteger[n]],Mod[#[[1]],4]==3\u0026]; Table[f[n],{n,100}] (* _Ray Chandler_, Dec 18 2011 *)"
			],
			"program": [
				"(Haskell)",
				"a065339 1 = 0",
				"a065339 n = length [x | x \u003c- a027746_row n, mod x 4 == 3]",
				"-- _Reinhard Zumkeller_, Jan 10 2012",
				"(PARI) A065339(n)=sum(i=1,#n=factor(n)~,if(n[1,i]%4==3,n[2,i]))  \\\\ _M. F. Hasler_, Apr 16 2012",
				"(Scheme, two variants using memoization-macro definec)",
				"(definec (A065339 n) (cond ((\u003c n 3) 0) ((even? n) (A065339 (/ n 2))) (else (+ (/ (- (modulo (A020639 n) 4) 1) 2) (A065339 (A032742 n))))))",
				"(definec (A065339 n) (cond ((\u003c n 3) 0) ((even? n) (A065339 (/ n 2))) ((= 1 (modulo (A020639 n) 4)) (A065339 (A032742 n))) (else (+ (A067029 n) (A065339 (A028234 n))))))",
				";; _Antti Karttunen_, Aug 14 2015"
			],
			"xref": [
				"Cf. A001222, A007814, A065338, A005091, A007949, A083025 (analogous for 4k+1 primes), A097706.",
				"Cf. A020639, A027746, A028234, A032742, A067029, A260728, A260730."
			],
			"keyword": "nonn",
			"offset": "1,9",
			"author": "_Reinhard Zumkeller_, Oct 29 2001",
			"references": 19,
			"revision": 30,
			"time": "2015-08-16T12:14:35-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}