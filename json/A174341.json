{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A174341",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 174341,
			"data": "2,1,1,1,1,1,1,1,7,1,1,1,-37,1,37,1,-211,1,2311,1,-407389,1,37153,1,-1181819909,1,76977929,1,-818946931,1,277930363757,1,-84802531453217,1,90219075042851,1,-711223555487930419,1,12696640293313423,1,-6367871182840222481,1,35351107998094669831,1,-83499808737903072705023,1,12690449182849194963361,1",
			"name": "a(n) = numerator(Bernoulli(n, 1) + 1/(n+1)).",
			"comment": [
				"a(n) is numerator of (A164555(n)/A027642(n) + 1/(n+1)).",
				"1/(n+1) and Bernoulli(n,1) are autosequences in the sense that they remain the same (up to sign) under inverse binomial transform. This feature is kept for their sum, a(n)/A174342(n) = 2, 1, 1/2, 1/4, 1/6, 1/6, 1/6, 1/8, 7/90, 1/10, ...",
				"Similar autosequences are also A000045, A001045, A113405, A000975 preceded by two zeros, and A140096.",
				"Conjecture: the numerator of (A164555(n)/(n+1) + A027642(n)/(n+1)^2) is a(n) and the denominator of this fraction is equal to 1 if and only if n+1 is prime or 1. Cf. A309132. - _Thomas Ordowski_, Jul 09 2019",
				"The \"if\" part of the conjecture is true: see the theorems in A309132 and A326690. The values of the numerator when n+1 is prime are A327033. - _Jonathan Sondow_, Aug 15 2019"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A174341/b174341.txt\"\u003eTable of n, a(n) for n = 0..300\u003c/a\u003e",
				"OEIS Wiki, \u003ca href=\"https://oeis.org/wiki/Autosequence\"\u003eAutosequence\u003c/a\u003e"
			],
			"maple": [
				"B := proc(n) if n \u003c\u003e 1 then bernoulli(n) ; else -bernoulli(n) ; end if; end proc:",
				"A174341 := proc(n) B(n)+1/(n+1) ; numer(%) ;end proc: # _R. J. Mathar_, Nov 19 2010"
			],
			"mathematica": [
				"a[n_] := Numerator[BernoulliB[n, 1] + 1/(n + 1)];",
				"Table[a[n], {n, 0, 47}] (* _Peter Luschny_, Jul 13 2019 *)"
			],
			"program": [
				"(PARI)",
				"B(n)=if(n!=1, bernfrac(n), -bernfrac(n));",
				"a(n)=numerator(B(n) + 1/(n + 1));",
				"for(n=0, 50, print1(a(n),\", \")) \\\\ _Indranil Ghosh_, Jun 19 2017",
				"(Python)",
				"from sympy import bernoulli, Integer",
				"def B(n): return bernoulli(n) if n != 1 else -bernoulli(n)",
				"def a(n): return (B(n) + 1/Integer(n + 1)).numerator() # _Indranil Ghosh_, Jun 19 2017",
				"(MAGMA) [2,1] cat [Numerator(Bernoulli(n)+1/(n+1)): n in [2..40]]; // _Vincenzo Librandi_, Jul 18 2019"
			],
			"xref": [
				"Cf. A164555, A027642, A174342 (denominators), A025529, A003506, A309132, A326690, A327033."
			],
			"keyword": "sign,frac",
			"offset": "0,1",
			"author": "_Paul Curtz_, Mar 16 2010",
			"ext": [
				"Reformulation of the name by _Peter Luschny_, Jul 13 2019"
			],
			"references": 7,
			"revision": 54,
			"time": "2020-05-06T12:30:14-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}