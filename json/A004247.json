{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A004247",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 4247,
			"data": "0,0,0,0,1,0,0,2,2,0,0,3,4,3,0,0,4,6,6,4,0,0,5,8,9,8,5,0,0,6,10,12,12,10,6,0,0,7,12,15,16,15,12,7,0,0,8,14,18,20,20,18,14,8,0,0,9,16,21,24,25,24,21,16,9,0,0,10,18,24,28,30,30,28,24,18,10,0,0,11,20,27,32,35,36,35,32,27,20,11,0,0,12,22,30,36,40,42,42,40,36,30",
			"name": "Multiplication table read by antidiagonals: T(i,j) = i*j (i\u003e=0, j\u003e=0). Alternatively, multiplication triangle read by rows: P(i,j) = j*(i-j) (i\u003e=0, 0\u003c=j\u003c=i).",
			"comment": [
				"Table of x*y, where (x,y) = (0,0),(0,1),(1,0),(0,2),(1,1),(2,0),...",
				"Or, triangle read by rows, in which row n gives the numbers 0, n*1, (n-1)*2, (n-2)*3, ..., 2*(n-1), 1*n, 0.",
				"Letting T(n,k) be the (k+1)st entry in the (n+1)st row (same numbering used for Pascal's triangle), T(n,k) is the dimension of the space of all k-dimensional subspaces of a (fixed) n-dimensional real vector space. - _Paul Boddington_, Oct 21 2003",
				"From _Dennis P. Walsh_, Nov 10 2009: (Start)",
				"Triangle P(n,k), 0\u003c=k\u003c=n, equals n^2 x the variance of a binary data set with k zeros and (n-k) ones. [For the case when n=0, let the variance of the empty set be defined as 0.]",
				"P(n,k) is also the number of ways to form an opposite-sex dance couple from k women and (n-k) men. (End)",
				"P(n,k) is the number of negative products of two numbers from a set of n real numbers, k of which are negative. - _Logan Pipes_, Jul 08 2021"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A004247/b004247.txt\"\u003eRows n = 0..50 of triangle, flattened\u003c/a\u003e",
				"Dennis Walsh, \u003ca href=\"http://www.mtsu.edu/~dwalsh/VBOUND2.pdf\"\u003eVariance bounds on binary data sets\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A002262(n) * A025581(n). - _Antti Karttunen_",
				"From _Ridouane Oudra_, Dec 14 2019: (Start)",
				"a(n) = A004197(n)*A003984(n).",
				"a(n) = (3/4 + n)*t^2 - (1/4)*t^4 - (1/2)*t - n^2 - n, where t = floor(sqrt(2*n+1)+1/2). (End)",
				"P(n,k) = (P(n-1,k-1) + P(n-1,k) + n) / 2. - _Robert FERREOL_, Jan 16 2020",
				"P(n,floor(n/2)) = A002620(n). - _Logan Pipes_, Jul 08 2021"
			],
			"example": [
				"As the triangle P, sequence begins:",
				"  0;",
				"  0,0;",
				"  0,1,0;",
				"  0,2,2,0;",
				"  0,3,4,3,0;",
				"  0,4,6,6,4,0,;",
				"  0,5,8,9,8,5,0;",
				"  ...",
				"From _Dennis P. Walsh_, Nov 10 2009: (Start)",
				"P(5,2)=T(2,3)=6 since the variance of the data set \u003c0,0,1,1,1\u003e equals 6/25.",
				"P(5,2)=6 since, with 2 women, say Alice and Betty, and with 3 men, say Charles, Dennis, and Ed, the dance couple is one of the following: {Alice, Charles}, {Alice, Dennis}, {Alice, Ed}, {Betty, Charles}, {Betty, Dennis} and {Betty, Ed}. (End)"
			],
			"maple": [
				"seq(seq(k*(n-k),k=0..n),n=0..13); # _Dennis P. Walsh_, Nov 10 2009"
			],
			"mathematica": [
				"Table[(x - y) y, {x, 0, 13}, {y, 0, x}] // Flatten (* _Robert G. Wilson v_, Oct 06 2007 *)"
			],
			"program": [
				"(PARI) T(i,j)=i*j \\\\ _Charles R Greathouse IV_, Jun 23 2017"
			],
			"xref": [
				"See A003991 for another version with many more comments.",
				"Cf. A002262, A025581, A003056, A004197, A003984, A048720, A325820, A000292 (row sums of triangle)."
			],
			"keyword": "tabl,nonn,easy,nice",
			"offset": "0,8",
			"author": "_David W. Wilson_",
			"ext": [
				"Edited by _N. J. A. Sloane_, Sep 30 2007"
			],
			"references": 25,
			"revision": 63,
			"time": "2021-09-27T04:10:10-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}