{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A158786",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 158786,
			"data": "1,25,4,1,100,25,11,4,1,275,100,25,29,11,4,1,725,275,100,25,76,29,11,4,1,1900,725,275,100,25,199,76,29,11,4,1,4975,1900,725,275,100,25,521,199,76,29,11,4,1,13025,4975,1900,725,275,100,25,1364,521,199,76,29,11,4,1,34100,13025,4975,1900,725,275,100,25",
			"name": "Irregular triangle T(n, k) = A000032(n-2*k+1) if (n-2*k) mod 2 = 0, otherwise 25*A000032(n-2*k), read by rows.",
			"reference": [
				"H. S. M. Coxeter, Regular Polytopes, 3rd ed., Dover, NY, 1973, pp. 159-162."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A158786/b158786.txt\"\u003eRows n = 0..50 of the irregular triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = 5*e(n, k), where e(n,k) = (e(n-1, k)*e(n, k-1) + 1)/e(n-1, k-1), and e(n, 0) = sqrt(5)*(GoldenRatio^(n) + GoldenRatio^(-n)).",
				"From _G. C. Greubel_, Dec 06 2021: (Star)",
				"T(n, k) = A000032(n-2*k+1) if (n-2*k) mod 2 = 0, otherwise 25*A000032(n-2*k).",
				"Sum_{k=0..floor(n/2)} T(n, k) = A000032(n) - 2 if (n mod 2 = 0), otherwise 25*(A000032(n-1) - 2). (End)"
			],
			"example": [
				"Irregular triangle begins as:",
				"     1;",
				"    25;",
				"     4,    1;",
				"   100,   25;",
				"    11,    4,   1;",
				"   275,  100,  25;",
				"    29,   11,   4,   1;",
				"   725,  275, 100,  25;",
				"    76,   29,  11,   4,  1;",
				"  1900,  725, 275, 100, 25;",
				"   199,   76,  29,  11,  4,   1;",
				"  4975, 1900, 725, 275, 100, 25;"
			],
			"mathematica": [
				"(* First program *)",
				"e[n_, 0]:= Sqrt[5]*(GoldenRatio^(n) + GoldenRatio^(-n));",
				"e[n_, k_]:= If[k\u003en-1, 0, (e[n-1, k]*e[n, k-1] +1)/e[n-1, k-1]];",
				"T[n_,k_]:= 5*Rationalize[N[e[n, k]]];",
				"Table[T[n, k], {n, 2, 16}, {k, Mod[n, 2] +1, n-1,2}]//Flatten",
				"(* Second program *)",
				"f[n_]:= f[n]= If[EvenQ[n], LucasL[n-1], 25*LucasL[n-2]];",
				"T[n_, k_]:= f[n-2*k];",
				"Table[T[n, k], {n, 2, 16}, {k, 0, (n-2)/2}]//Flatten (* _G. C. Greubel_, Dec 06 2021 *)"
			],
			"program": [
				"(Sage)",
				"def A158786(n,k): return lucas_number2(n-2*k-1,1,-1) if ((n-2*k)%2==0) else 25*lucas_number2(n-2*k-2,1,-1)",
				"flatten([[A158786(n,k) for k in (0..((n-2)//2))] for n in (2..16)]) # _G. C. Greubel_, Dec 06 2021"
			],
			"xref": [
				"Cf. A000032, A002878, A004146, A158753."
			],
			"keyword": "nonn,tabf",
			"offset": "2,2",
			"author": "_Roger L. Bagula_ and _Gary W. Adamson_, Mar 26 2009",
			"ext": [
				"Edited by _G. C. Greubel_, Dec 06 2021"
			],
			"references": 2,
			"revision": 9,
			"time": "2021-12-08T07:34:45-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}