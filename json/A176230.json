{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A176230",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 176230,
			"data": "1,1,1,3,6,1,15,45,15,1,105,420,210,28,1,945,4725,3150,630,45,1,10395,62370,51975,13860,1485,66,1,135135,945945,945945,315315,45045,3003,91,1,2027025,16216200,18918900,7567560,1351350,120120,5460,120,1,34459425",
			"name": "Exponential Riordan array [1/sqrt(1-2x), x/(1-2x)].",
			"comment": [
				"Row sums are A066223. Reverse of A119743. Inverse is alternating sign version.",
				"Diagonal sums are essentially A025164.",
				"From _Tom Copeland_, Dec 13 2015: (Start)",
				"See A099174 for relations to the Hermite polynomials and the link for operator relations, including the infinitesimal generator containing A000384.",
				"Row polynomials are 2^n n! Lag(n,-x/2,-1/2), where Lag(n,x,q) is the associated Laguerre polynomial of order q.",
				"The triangles of Bessel numbers entries A122848, A049403, A096713, A104556 contain these polynomials as even or odd rows. Also the aerated version A099174 and A066325. Reversed, these entries are A100861, A144299, A111924.",
				"Divided along the diagonals by the initial element (A001147) of the diagonal, this matrix becomes the even rows of A034839.",
				"(End)",
				"The first few rows appear in expansions related to the Dedekind eta function on pp. 537-538 of the Chan et al. link. - _Tom Copeland_, Dec 14 2016"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A176230/b176230.txt\"\u003eTable of n, a(n) for the first 50 rows, flattened\u003c/a\u003e",
				"P. Bala, \u003ca href=\"/A035342/a035342.txt\"\u003eGeneralized Dobinski formulas\u003c/a\u003e",
				"H. Chan, S. Cooper, and P. Toh, \u003ca href=\"http://dx.doi.org/10.1016/j.aim.2005.12.003\"\u003eThe 26th power of Dedekind's eta function\u003c/a\u003e Advances in Mathematics, 207 (2006) 532-543.",
				"T. Copeland, \u003ca href=\"http://tcjpn.wordpress.com/2012/11/29/infinigens-the-pascal-pyramid-and-the-witt-and-virasoro-algebras/\"\u003eInfinitesimal Generators, the Pascal Pyramid, and the Witt and Virasoro Algebras\u003c/a\u003e, 2012.",
				"T. Copeland, \u003ca href=\"https://tcjpn.wordpress.com/2020/07/15/juggling-zeros-in-the-matrix-example-ii/\"\u003eJuggling Zeros in the Matrix (Example II)\u003c/a\u003e, 2020."
			],
			"formula": [
				"Number triangle T(n,k) = (2n)!/((2k)!(n-k)!2^(n-k)).",
				"T(n,k) = A122848(2n,k+n). - _R. J. Mathar_, Jan 14 2011",
				"[x^(1/2)(1+2D)]^2 p(n,x)= p(n+1,x) and [D/(1+2D)]p(n,x)= n p(n-1,x) for the row polynomials of T, with D=d/dx. - _Tom Copeland_, Dec 26 2012",
				"E.g.f.: exp[t*x/(1-2x)]/(1-2x)^(1/2). - _Tom Copeland_ , Dec 10 2013",
				"The n-th row polynomial R(n,x) is given by the type B Dobinski formula R(n,x) = exp(-x/2)*Sum_{k\u003e=0} (2*k+1)*(2*k+3)*...*(2*k+1+2*(n-1))*(x/2)^k/k!. Cf. A113278. - _Peter Bala_, Jun 23 2014",
				"The raising operator in my 2012 formula expanded is R = [x^(1/2)(1+2D)]^2 = 1 + x + (2 + 4x) D + 4x D^2, which in matrix form acting on an o.g.f. (formal power series) is the transpose of the production array below. The linear term x is the diagonal of ones after transposition. The main diagonal comes from (1 + 4xD) x^n = (1 + 4n) x^n. The last diagonal comes from (2 D + 4 x D^2) x^n = (2 + 4 xD) D x^n = n * (2 + 4(n-1)) x^(n-1). - _Tom Copeland_, Dec 13 2015",
				"T(n, k) = (-2)^(n-k)*[x^k] KummerU(-n, 1/2, x). - _Peter Luschny_, Jan 18 2020"
			],
			"example": [
				"Triangle begins",
				"        1,",
				"        1,        1,",
				"        3,        6,        1,",
				"       15,       45,       15,       1,",
				"      105,      420,      210,      28,       1,",
				"      945,     4725,     3150,     630,      45,      1,",
				"    10395,    62370,    51975,   13860,    1485,     66,    1,",
				"   135135,   945945,   945945,  315315,   45045,   3003,   91,   1,",
				"  2027025, 16216200, 18918900, 7567560, 1351350, 120120, 5460, 120, 1",
				"Production matrix is",
				"  1,  1,",
				"  2,  5,  1,",
				"  0, 12,  9,  1,",
				"  0,  0, 30, 13,  1,",
				"  0,  0,  0, 56, 17,   1,",
				"  0,  0,  0,  0, 90,  21,   1,",
				"  0,  0,  0,  0,  0, 132,  25,   1,",
				"  0,  0,  0,  0,  0,   0, 182,  29,  1,",
				"  0,  0,  0,  0,  0,   0,   0, 240, 33, 1."
			],
			"maple": [
				"ser := n -\u003e series(KummerU(-n, 1/2, x), x, n+1):",
				"seq(seq((-2)^(n-k)*coeff(ser(n), x, k), k=0..n), n=0..8); # _Peter Luschny_, Jan 18 2020"
			],
			"mathematica": [
				"t[n_, k_] := k!*Binomial[n, k]/((2 k - n)!*2^(n - k)); u[n_, k_] := t[2 n, k + n]; Table[ u[n, k], {n, 0, 8}, {k, 0, n}] // Flatten (* _Robert G. Wilson v_, Jan 14 2011 *)"
			],
			"xref": [
				"Cf. A113278.",
				"Cf. A000384, A001147, A034839, A049403, A066325, A096713, A099174, A100861, A104556, A111924, A122848, A144299."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "0,4",
			"author": "_Paul Barry_, Apr 12 2010",
			"references": 6,
			"revision": 60,
			"time": "2020-09-07T12:08:07-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}