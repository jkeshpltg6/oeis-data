{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005439",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5439,
			"id": "M1888",
			"data": "1,2,8,56,608,9440,198272,5410688,186043904,7867739648,401293838336,24290513745920,1721379917619200,141174819474169856,13266093250285568000,1415974941618255921152,170361620874699124637696,22948071824232932086513664,3439933090471867097102680064",
			"name": "Genocchi medians (or Genocchi numbers of second kind).",
			"comment": [
				"a(n) is the number of Boolean functions of n variables whose ROBDD (reduced ordered binary decision diagram) contains exactly n branch nodes, one for each variable. - _Don Knuth_, Jul 11 2007",
				"The earliest known reference for these numbers is Seidel (1877, pages 185 and 186). - _Don Knuth_, Jul 13 2007",
				"Hankel transform of 1,1,2,8,... is A168488. - _Paul Barry_, Nov 27 2009",
				"According to Hetyei [2017], alternation acyclic tournaments \"are counted by the median Genocchi numbers\"; an alternation acyclic tournament \"does not contain a cycle in which descents and ascents alternate.\" - _Danny Rorabaugh_, Apr 25 2017",
				"The n-th Genocchi number of the second kind is also the number of collapsed permutations in (2n) letters. A permutation pi of size 2n is said to be collapsed if 1+floor(k/2) \u003c= pi^{-1}(k) \u003c= n + floor(k/2). There are 2 collapsed permutations of size 4, namely 1234 and 1324. - _Arvind Ayyer_, Oct 23 2020",
				"For any positive integer n, a(n) is (-1)^n times the permanent of the 2n X 2n matrix M with M(j, k) = floor((2*j-k-1)/(2*n)). This former conjecture of Luschny, inspired by a conjecture of _Zhi-Wei Sun_ in A036968, was proven by Fu, Lin and Sun (see link). - _Peter Luschny_, Sep 07 2021 [updated Sep 24 2021]"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Muniru A Asiru, \u003ca href=\"/A005439/b005439.txt\"\u003eTable of n, a(n) for n = 1..270\u003c/a\u003e (terms n = 1..100 from T. D. Noe)",
				"A. Ayyer, D. Hathcock and P. Tetali, \u003ca href=\"https://arxiv.org/abs/2010.11236\"\u003e Toppleable Permutations, Excedances and Acyclic Orientations\u003c/a\u003e, arXiv:2010.11236 [math.CO], 2020.",
				"Paul Barry, \u003ca href=\"https://arxiv.org/abs/2107.14278\"\u003eSeries reversion with Jacobi and Thron continued fractions\u003c/a\u003e, arXiv:2107.14278 [math.NT], 2021.",
				"Ange Bigeni, \u003ca href=\"https://arxiv.org/abs/1712.05475\"\u003eThe universal sl2 weight system and the Kreweras triangle\u003c/a\u003e, arXiv:1712.05475 [math.CO], 2017.",
				"Ange Bigeni, \u003ca href=\"https://arxiv.org/abs/1712.01929\"\u003eCombinatorial interpretations of the Kreweras triangle in terms of subset tuples\u003c/a\u003e, arXiv:1712.01929 [math.CO], 2017.",
				"Ange Bigeni, \u003ca href=\"https://doi.org/10.1016/j.jcta.2018.08.005\"\u003eA generalization of the Kreweras triangle through the universal sl_2 weight system\u003c/a\u003e, Journal of Combinatorial Theory, Series A (2019) Vol. 161, 309-326.",
				"Alexander Burstein, Sergi Elizalde and Toufik Mansour, \u003ca href=\"https://arxiv.org/abs/math/0610234\"\u003eRestricted Dumont permutations, Dyck paths and noncrossing partitions\u003c/a\u003e, arXiv:math/0610234 [math.CO], 2006. [Theorem 3.5]",
				"Kwang-Wu Chen, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL6/Chen/chen50.html\"\u003eAn Interesting Lemma for Regular C-fractions\u003c/a\u003e, J. Integer Seqs., Vol. 6, 2003.",
				"Shane Chern, \u003ca href=\"https://arxiv.org/abs/2112.02074\"\u003eParity considerations for drops in cycles on {1,2,...,n}\u003c/a\u003e, arXiv:2112.02074 [math.CO], 2021.",
				"D. Dumont and J. Zeng, \u003ca href=\"http://math.univ-lyon1.fr/homes-www/zeng/public_html/paper/publication.html\"\u003ePolynomes d'Euler et les fractions continues de Stieltjes-Rogers\u003c/a\u003e, Ramanujan J. 2 (1998) 3, 387-410.",
				"Richard Ehrenborg and Einar Steingrímsson, \u003ca href=\"http://dx.doi.org/10.1006/eujc.1999.0370\"\u003eYet another triangle for the Genocchi numbers\u003c/a\u003e, European J. Combin. 21 (2000), no. 5, 593-600. MR1771988 (2001h:05008).",
				"Sen-Peng Eu, Tung-Shan Fu, Hsin-Hao Lai, and Yuan-Hsun Lo, \u003ca href=\"https://arxiv.org/abs/2103.09130\"\u003eGamma-positivity for a Refinement of Median Genocchi Numbers\u003c/a\u003e, arXiv:2103.09130 [math.CO], 2021.",
				"Shishuo Fu, Zhicong Lin and Zhi-Wei Sun, \u003ca href=\"https://arxiv.org/abs/2109.11506\"\u003eProofs of five conjectures relating permanents to combinatorial sequences\u003c/a\u003e, arXiv:2109.11506 [math.CO], 2021.",
				"I. M. Gessel, \u003ca href=\"https://arxiv.org/abs/math/0108121\"\u003eApplications of the classical umbral calculus\u003c/a\u003e, arXiv:math/0108121 [math.CO], 2001.",
				"G. Han and J. Zeng, \u003ca href=\"http://www.labmath.uqam.ca/~annales/volumes/23-1/PDF/063-072.pdf\"\u003eOn a q-sequence that generalizes the median Genocchi numbers\u003c/a\u003e, Annal Sci. Math. Quebec, 23(1999), no. 1, 63-72.",
				"Gábor Hetyei, \u003ca href=\"https://arxiv.org/abs/1704.07245\"\u003eAlternation acyclic tournaments\u003c/a\u003e, arXiv:math/1704.07245 [math.CO], 2017.",
				"G. Kreweras, \u003ca href=\"http://dx.doi.org/10.1006/eujc.1995.0081\"\u003eSur les permutations comptées par les nombres de Genocchi de 1-ière et 2-ième espèce\u003c/a\u003e, Europ. J. Comb., vol. 18, pp. 49-58, 1997. (See also page 76.)",
				"Alexander Lazar and Michelle L. Wachs, \u003ca href=\"https://arxiv.org/abs/1910.07651\"\u003eThe Homogenized Linial Arrangement and Genocchi Numbers\u003c/a\u003e, arXiv:1910.07651 [math.CO], 2019.",
				"Qiongqiong Pan and Jiang Zeng, \u003ca href=\"https://arxiv.org/abs/2108.03200\"\u003eCycles of even-odd drop permutations and continued fractions of Genocchi numbers\u003c/a\u003e, arXiv:2108.03200 [math.CO], 2021.",
				"A. Randrianarivony and J. Zeng, \u003ca href=\"http://dx.doi.org/10.1006/aama.1996.0001\"\u003eUne famille de polynomes qui interpole plusieurs suites...\u003c/a\u003e, Adv. Appl. Math. 17 (1996), 1-26.",
				"L. Seidel, \u003ca href=\"http://publikationen.badw.de/de/003384831\"\u003eÜber eine einfache Entstehungsweise der Bernoulli'schen Zahlen und einiger verwandten Reihen\u003c/a\u003e, Sitzungsberichte der mathematisch-physikalischen Classe der königlich bayerischen Akademie der Wissenschaften zu München, volume 7 (1877), 157-187.",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/2108.07723\"\u003eArithmetic properties of some permanents\u003c/a\u003e, arXiv:2108.07723 [math.GM], 2021.",
				"G. Viennot, \u003ca href=\"http://www.jstor.org/stable/44165433\"\u003eInterprétations combinatoires des nombres d'Euler et de Genocchi\u003c/a\u003e, Seminar on Number Theory, 1981/1982, Exp. No. 11, 94 pp., Univ. Bordeaux I, Talence, 1982."
			],
			"formula": [
				"a(n) = T(n, 1) where T(1, x) = 1; T(n, x) = (x+1)*((x+1)*T(n-1, x+1)-x*T(n-1, x)); see A058942.",
				"a(n) = A000366(n)*2^(n-1).",
				"a(n) = 2 * (-1)^n * Sum_{k=0..n} binomial(n, k)*(1-2^(n+k+1))*B(n+k+1), with B(n) the Bernoulli numbers. - _Ralf Stephan_, Apr 17 2004",
				"O.g.f.: 1 + x*A(x) = 1/(1-x/(1-x/(1-4*x/(1-4*x/(1-9*x/(1-9*x/(... -[(n+1)/2]^2*x/(1-...)))))))) (continued fraction). - _Paul D. Hanna_, Oct 07 2005",
				"G.f.: (of 1,1,2,8,...) 1/(1-x-x^2/(1-5*x-16*x^2/(1-13*x-81*x^2/(1-25*x-256*x^2/(1-41*x-625*x^2/(1-... (continued fraction). - _Paul Barry_, Nov 27 2009",
				"O.g.f.: Sum_{n\u003e=0} n!*(n+1)! * x^(n+1) / Product_{k=1..n} (1 + k*(k+1)*x). - _Paul D. Hanna_, May 10 2012",
				"From _Sergei N. Gladkovskii_, Dec 14 2011, Dec 27 2012, May 29 2013, Oct 09 2013, Oct 24 2013, Oct 27 2013: (Start)",
				"Continued fractions:",
				"G.f.: A(x) = 1/S(0), S(k) = 1 - x*(k+1)*(k+2)/(1 - x*(k+1)*(k+2)/S(k+1)).",
				"G.f.: A(x) = -1/S(0), S(k) = 2*x*(k+1)^2 - 1 - x^2*(k+1)^2*(k+2)^2/S(k+1).",
				"G.f.: A(x) = (1/(G(0)-1)/x where G(k) = 1 - x*(k+1)^2/(1 - x*(k+1)^2/G(k+1)).",
				"G.f.: 2/G(0), where G(k) = 1 + 1/(1 - 1/(1 - 1/(4*x*(k+1)) + 1/G(k+1))).",
				"G.f.: Q(0)/x - 1/x, where Q(k) = 1 - x*(k+1)^2/( x*(k+1)^2 - 1/(1 - x*(k+1)^2/( x*(k+1)^2 - 1/Q(k+1)))).",
				"G.f.: T(0)/(1-2*x), where T(k) = 1 - x^2*((k + 2)*(k+1))^2/(x^2*((k + 2)*(k+1))^2 - (1 - 2*x*k^2 - 4*x*k - 2*x)*(1 - 2*x*k^2 - 8*x*k - 8*x)/T(k+1)).",
				"G.f.: R(0), where R(k) = 1 - x*(k+1)*(k+2)/( x*(k+1)*(k+2) - 1/(1 - x*(k+1)*(k+2)/( x*(k+1)*(k+2) - 1/R(k+1) ))). (End)",
				"a(n) ~ 2^(2*n+4) * n^(2*n+3/2) / (exp(2*n) * Pi^(2*n+1/2)). - _Vaclav Kotesovec_, Oct 28 2014"
			],
			"maple": [
				"seq(2*(-1)^n*add(binomial(n,k)*(1 - 2^(n+k+1))*bernoulli(n+k+1), k=0..n), n=1..20); # _G. C. Greubel_, Oct 18 2019"
			],
			"mathematica": [
				"a[n_]:= 2*(-1)^(n-2)*Sum[Binomial[n, k]*(1 -2^(n+k+1))*BernoulliB[n+k+1], {k, 0, n}]; Table[a[n], {n,16}] (* _Jean-François Alcover_, Jul 18 2011, after PARI prog. *)"
			],
			"program": [
				"(PARI) a(n)=2*(-1)^n*sum(k=0,n,binomial(n,k)*(1-2^(n+k+1))* bernfrac(n+k+1))",
				"(PARI) a(n)=local(CF=1+x*O(x^(n+2)));if(n\u003c0,return(0), for(k=1,n+1,CF=1/(1-((n-k+1)\\2+1)^2*x*CF));return(Vec(CF)[n+2])) \\\\ _Paul D. Hanna_",
				"(Sage) # Algorithm of L. Seidel (1877)",
				"# n -\u003e [a(1), ..., a(n)] for n \u003e= 1.",
				"def A005439_list(n) :",
				"    D = []; [D.append(0) for i in (0..n+2)]; D[1] = 1",
				"    R = [] ; b = True",
				"    for i in(0..2*n-1) :",
				"        h = i//2 + 1",
				"        if b :",
				"            for k in range(h-1,0,-1) : D[k] += D[k+1]",
				"        else :",
				"            for k in range(1,h+1,1) :  D[k] += D[k-1]",
				"        if b : R.append(D[1])",
				"        b = not b",
				"    return R",
				"A005439_list(18) # _Peter Luschny_, Apr 01 2012",
				"(Sage) [2*(-1)^n*sum(binomial(n,k)*(1-2^(n+k+1))*bernoulli(n+k+1) for k in (0..n)) for n in (1..20)] # _G. C. Greubel_, Oct 18 2019",
				"(MAGMA) [2*(-1)^n*(\u0026+[Binomial(n, k)*(1-2^(n+k+1))*Bernoulli(n+k+1): k in [0..n]]): n in [1..20]]; // _G. C. Greubel_, Nov 28 2018",
				"(GAP) List([1..20],n-\u003e2*(-1)^n*Sum([0..n],k-\u003eBinomial(n,k)*(1-2^(n+k+1))*Bernoulli(n+k+1))); # _Muniru A Asiru_, Nov 29 2018"
			],
			"xref": [
				"Cf. A000366, A036968."
			],
			"keyword": "nonn,nice,easy",
			"offset": "1,2",
			"author": "_Simon Plouffe_",
			"ext": [
				"More terms and additional comments from _David W. Wilson_, Jan 11 2001"
			],
			"references": 21,
			"revision": 173,
			"time": "2021-12-09T00:58:32-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}