{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A290260",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 290260,
			"data": "0,1,0,0,1,1,0,0,0,2,1,0,1,1,0,0,0,1,0,1,2,2,1,0,0,2,1,0,1,1,0,0,0,1,0,0,1,1,0,1,1,3,2,1,2,2,1,0,0,1,0,1,2,2,1,0,0,2,1,0,1,1,0,0,0,1,0,0,1,1,0,0,0,2,1,0,1,1,0,1,1,2,1,2,3,3,2,1,1,3,2,1,2,2,1,0,0,1,0,0,1,1,0,1,1,3,2,1,2,2,1,0,0",
			"name": "a(n) = number of isolated 0's in the binary representation of n.",
			"comment": [
				"a(2n) = number of singletons in the integer partition having viabin number n. The viabin number of an integer partition is defined in the following way. Consider the southeast border of the Ferrers board of the integer partition and consider the binary number obtained by replacing each east step with 1 and each north step, except the last one, with 0. The corresponding decimal form is, by definition, the viabin number of the given integer partition. \"Viabin\" is coined from \"via binary\". For example, consider the integer partition [5,4,4,3,1]. The southeast border of its Ferrers board yields 101101001, leading to the viabin number 361.",
				"The following 5 formulae are used for the Maple program."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A290260/b290260.txt\"\u003eTable of n, a(n) for n = 1..65536\u003c/a\u003e"
			],
			"formula": [
				"a(1) = 0.",
				"If n is odd, then a(2n) = 1 + a(n).",
				"a(2n+1) = a(n).",
				"If n-2 mod 4 = 0, then a(2n) = a(n) - 1.",
				"If n mod 8 = 0 then a(2n) = a(n)."
			],
			"example": [
				"a(722) = 3; indeed, the binary representation of 722 is 1011010010, having 3 isolated 0's."
			],
			"maple": [
				"a := proc (n) if n = 1 then 0 elif `mod`(n, 2) = 0 and `mod`((1/2)*n, 2) = 1 then 1+a((1/2)*n) elif `mod`(n, 2) = 1 then a((1/2)*n-1/2) elif `mod`(n-4, 8) = 0 then a((1/2)*n)-1 else a((1/2)*n) end if end proc: seq(a(n), n = 1 .. 200);",
				"# second Maple program:",
				"b:= n-\u003e `if`(n\u003c6, 0, b(iquo(n, 2)))+`if`(n mod 8=5, 1, 0):",
				"a:= n-\u003e b(2*n+1):",
				"seq(a(n), n=1..200);  # _Alois P. Heinz_, Sep 14 2017"
			],
			"mathematica": [
				"Table[Count[Split@ IntegerDigits[n, 2], {0}], {n, 105}] (* _Michael De Vlieger_, Sep 16 2017 *)"
			],
			"xref": [
				"Cf. A292342."
			],
			"keyword": "nonn",
			"offset": "1,10",
			"author": "_Emeric Deutsch_, Sep 14 2017",
			"references": 2,
			"revision": 15,
			"time": "2018-08-27T23:53:09-04:00",
			"created": "2017-09-16T16:57:36-04:00"
		}
	]
}