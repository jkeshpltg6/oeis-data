{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A249583",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 249583,
			"data": "1,1,1,2,5,9,40,169,477,3194,19241,74601,666160,5216485,25740261,287316122,2769073949,16591655817,222237912664,2543467934449,17929265150637,280180369563194,3712914075133121,30098784753112329,537546603651987424,8094884285992309261",
			"name": "Number of permutations p of [n] such that p(i) \u003e p(i+1) iff i == 2 (mod 3).",
			"comment": [
				"This is the (UDU)* version of 3-alternating permutations of [n], (U=Up, D=Down)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A249583/b249583.txt\"\u003eTable of n, a(n) for n = 0..500\u003c/a\u003e",
				"J. M. Luck, \u003ca href=\"https://arxiv.org/abs/1309.7764\"\u003eOn the frequencies of patterns of rises and falls\u003c/a\u003e, arXiv:1309.7764, 2013",
				"Anthony Mendes and Jeffrey Remmel, Generating functions from symmetric functions, Preliminary version of book, available from \u003ca href=\"http://math.ucsd.edu/~remmel/\"\u003eJeffrey Remmel's home page\u003c/a\u003e",
				"R. P. Stanley, \u003ca href=\"https://arxiv.org/abs/0912.4240\"\u003eA survey of alternating permutations\u003c/a\u003e, arXiv:0912.4240, 2009"
			],
			"example": [
				"a(2) = 1: 12.",
				"a(3) = 2: 132, 231.",
				"a(4) = 5: 1324, 1423, 2314, 2413, 3412.",
				"a(5) = 9: 13245, 14235, 15234, 23145, 24135, 25134, 34125, 35124, 45123.",
				"a(6) = 40: 132465, 132564, 142365, 142563, 143562, 152364, 152463, 153462, 162354, 162453, 163452, 231465, 231564, 241365, 241563, 243561, 251364, 251463, 253461, 261354, 261453, 263451, 341265, 341562, 342561, 351264, 351462, 352461, 361254, 361452, 362451, 451263, 451362, 452361, 461253, 461352, 462351, 561243, 561342, 562341.",
				"a(7) = 169: 1324657, 1324756, 1325647, ..., 6723514, 6724513, 6734512."
			],
			"maple": [
				"b:= proc(u, o, t) option remember; `if`(u+o=0, 1,",
				"     `if`(t=2, add(b(u-j, o+j-1, irem(t+1, 3)), j=1..u),",
				"               add(b(u+j-1, o-j, irem(t+1, 3)), j=1..o)))",
				"    end:",
				"a:= n-\u003e b(0, n, 0):",
				"seq(a(n), n=0..35);"
			],
			"mathematica": [
				"b[u_, o_, t_] := b[u, o, t] = If[u + o == 0, 1, If[t == 2, Sum[b[u - j, o + j - 1, Mod[t+1, 3]], {j, 1, u}], Sum[b[u + j - 1, o - j, Mod[t+1, 3]], {j, 1, o}]]];",
				"a[n_] := b[0, n, 0];",
				"Table[a[n], {n, 0, 35}] (* _Jean-François Alcover_, Nov 06 2017, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A178963 (i=0), A249402 (i=1)."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_Alois P. Heinz_, Nov 01 2014",
			"references": 4,
			"revision": 23,
			"time": "2021-09-03T01:51:22-04:00",
			"created": "2014-11-01T21:39:22-04:00"
		}
	]
}