{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A235117",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 235117,
			"data": "1,1,6,10,6,1,1,12,54,124,162,124,54,12,1,1,18,134,556,1451,2530,3036,2530,1451,556,134,18,1,1,24,250,1516,6042,16892,34430,52352,60122,52352,34430,16892,6042,1516,250,24,1,1,30,402,3220,17393,67676,197588,444584,784702,1098826,1228500,1098826,784702,444584,197588,67676,17393,3220,402,30,1",
			"name": "Irregular triangle read by rows: T(n,k) = number of independent vertex subsets of size k of the graph g_n obtained by attaching two pendant edges to each vertex of the ladder graph L_n (i.e., L_n is the 2 X n grid graph; 0 \u003c= k \u003c= 4n+1).",
			"comment": [
				"Each row is palindromic (see the Stevanovic and Mandrescu references).",
				"Row n (n \u003e= 0) contains 4n+1 entries.",
				"Sum of entries in row n = A235118(n).",
				"In the Maple program, P(n) gives the independence polynomial of the graph g_n."
			],
			"link": [
				"E. Mandrescu, \u003ca href=\"http://ajc.maths.uq.edu.au/pdf/53/ajc_v53_p077.pdf\"\u003eUnimodality of some independence polynomials via their palindromicity\u003c/a\u003e, Australasian J. of Combinatorics, 53, 2012, 77-82.",
				"D. Stevanovic, \u003ca href=\"https://draganstevanovic.files.wordpress.com/2012/09/gtn-341998-31-36-acro6.pdf\"\u003eGraphs with palindromic independence polynomial\u003c/a\u003e, Graph Theory Notes of New York, 34, 1998, 31-36."
			],
			"formula": [
				"Generating polynomial P(n) of row n (i.e., the independence polynomial of the graph g_n) satisfies the recurrence relation P(n) = (1 + x)^2*(1 + 3x + x^2)P(n-1) + x(1 + x)^6 *P(n-2); P(0)=1; P(1)=(1 + 4x + x^2)*(1 + x)^2."
			],
			"example": [
				"Triangle begins:",
				"1;",
				"1,6,10,6,1;",
				"1,12,54,124,162,124,54,12,1;",
				"1,18,134,556,1451,2530,3036,2530,1451,556,134,18,1;"
			],
			"maple": [
				"P := proc (n) option remember: if n = 0 then 1 elif n = 1 then sort(expand((1+x)^2*(1+4*x+x^2))) else sort(expand((1+x)^2*(1+3*x+x^2)*P(n-1) +x*(1+x)^6*P(n-2))) end if end proc: for n from 0 to 5 do seq(coeff(P(n), x, i), i = 0 .. 4*n) end do; # yields sequence in triangular form"
			],
			"xref": [
				"Cf. A235118."
			],
			"keyword": "nonn,tabf",
			"offset": "0,3",
			"author": "_Emeric Deutsch_, Jan 14 2014",
			"references": 1,
			"revision": 16,
			"time": "2020-02-27T22:38:53-05:00",
			"created": "2014-01-14T20:43:11-05:00"
		}
	]
}