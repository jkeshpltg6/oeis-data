{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A328911",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 328911,
			"data": "2,0,3,3,0,4,3,1,0,4,4,0,0,6,7,4,0,0,6,5,3,0,0,5,7,4,2,1,0,8,13,5,1,0,0,9,12,3,1,0,0,6,6,3,0,0,0,8,13,9,3,0,0,0,8,7,1,0,0,0,0,6,15,6,2,1,0,0,12,16,12,3,0,0,0,12,15,11,4,2,1,0,0,6,8,2,2,0,0,0,0",
			"name": "Irregular triangle read by rows: T(n,k) = number of solutions to Erdös's Last Equation x_1*...*x_n = n*(x_1+...+x_n), 0 \u003c x_1 \u003c= ... \u003c= x_n, having k+1 components x_i \u003e 1, 1 \u003c= k \u003c= 2*log_2(n).",
			"comment": [
				"For n = 1 the equation is trivially solved by any integer, therefore we only consider n \u003e= 2.",
				"If any x_k = 0, then all x_i must be zero, so (0, ..., 0) would be the only additional solution in nonnegative integers. This solution is not considered here.",
				"A vector (1, ..., 1, x_n) can never be a solution for n \u003e 1. The number of components different from 1 must be k+1 \u003e= 2 \u003c=\u003e k \u003e= 1.",
				"It can be shown that no solution can have 2^k \u003e n^2, cf. the Shiu paper. Therefore row lengths are floor(2 log_2(n)) = (2, 3, 4, 4, 5, 5, 6, 6, 6, 6, 7, 7, 7, ...) = A329202(n), n \u003e= 2.",
				"Row sums yield the total number of nontrivial solutions A328910(n), see there for more information.",
				"T(n,k) is equal to |C_k(n)| in the Shiu paper, but some values given in the table on top of p. 803 are erroneous (pers. comm. from the author)."
			],
			"link": [
				"David A. Corneth, \u003ca href=\"/A328911/b328911.txt\"\u003eTable of n, a(n) for n = 2..14645\u003c/a\u003e (first 899 rows flattened)",
				"Peter Shiu, \u003ca href=\"https://doi.org/10.1080/00029890.2019.1639466\"\u003eOn Erdös's Last Equation\u003c/a\u003e, Amer. Math. Monthly, 126 (2019), 802-808."
			],
			"example": [
				"The table starts:",
				"   n : T(n,k), 1 \u003c= k \u003c= 2*log_2(n)",
				"   2 :   2   0",
				"   3 :   3   3   0",
				"   4 :   4   3   1   0",
				"   5 :   4   4   0   0",
				"   6 :   6   7   4   0   0",
				"   7 :   6   5   3   0   0",
				"   8 :   5   7   4   2   1   0",
				"   9 :   8  13   5   1   0   0",
				"  10 :   9  12   3   1   0   0",
				"  11 :   6   6   3   0   0   0",
				"  12 :   8  13   9   3   0   0   0",
				"  13 :   8   7   1   0   0   0   0",
				"  14 :   6  15   6   2   1   0   0",
				"  15 :  12  16  12   3   0   0   0",
				"For n = 2 variables, we have the equation x1*x2 = 2*(x1 + x2) with positive integer solutions (3,6) and (4,4): Both have k+1 = 2 components \u003e 1, i.e., k = 1.",
				"For n = 3, we have T(3,1) = 3 solutions with k+1 = 2 components \u003e 1, {(1, 4, 15), (1, 5, 9), (1, 6, 7)}, and T(3,2) = 3 with k+1 = 3 components \u003e 1, {(2, 2, 12), (2, 3, 5), (3,3,3)}.",
				"For n = 4 we have the 8 solutions (1, 1, 5, 28), (1, 1, 6, 16), (1, 1, 7, 12), (1, 1, 8, 10), (1, 2, 3, 12), (1, 2, 4, 7), (1, 3, 4, 4) and (2, 2, 2, 6). Four of them have k+1 = 2 components \u003e 1, i.e., k = 1, whence T(4,1) = 4. Three have k+1 = 3 \u003c=\u003e k = 2, so T(4,2) = 3. One has k+1 = 4, so T(4,3) = 1.",
				"For n = 5, the solutions are, omitting initial components x_i = 1: {(6, 45), (7, 25), (9, 15), (10, 13), (2, 3, 35), (2, 5, 9), (3, 3, 10), (3, 5, 5)}. Therefore T(5,1..4) = (4, 4, 0, 0).",
				"For n = 6, the solutions are (omitting x_i = 1): {(7, 66), (8, 36), (9, 26), (10, 21), (11, 18), (12, 16), (2, 4, 27), (2, 5, 15), (2, 6, 11), (2, 7, 9), (3, 3, 18), (3, 4, 10), (3, 6, 6), (2, 2, 2, 24), (2, 2, 3, 9), (2, 2, 4, 6), (2, 3, 3, 5)}. Therefore T(6,1..5) = (6, 7, 4, 0, 0).",
				"For n = 9, the 27 solutions are (omitting '1's): {(10, 153), (11, 81), (12, 57), (13, 45), (15, 33), (17, 27), (18, 25), (21, 21), (2, 5, 117), (2, 6, 42), (2, 7, 27), (2, 9, 17), (2, 12, 12), (3, 4, 39), (3, 5, 21), (3, 6, 15), (3, 7, 12), (3, 9, 9), (4, 4, 18), (5, 5, 9), (6, 6, 6), (2, 2, 3, 36), (2, 2, 6, 9), (2, 3, 3, 13), (3, 3, 3, 7), (3, 3, 4, 5), (2, 3, 3, 3, 3)}. Therefore T(9,1..6) = (8, 13, 5, 1, 0, 0)."
			],
			"program": [
				"(PARI) A328911(n,k,show=1)={if( k\u003cmin(exponent(n^2)+1, n), my(s=0,t=n*(n-k-1),d); forvec(x=vector(k,i,[2,n\\(sqrt(2)-1)]), (d=vecprod(x)-n)\u003e0 \u0026\u0026 (n*vecsum(x)+t)%d==0 \u0026\u0026 (n*vecsum(x)+t)\\d \u003e= x[k] \u0026\u0026 s++\u0026\u0026 show\u0026\u0026 printf(\"%d,\",concat(x,(n*vecsum(x)+t)\\d)),1);s)}"
			],
			"xref": [
				"Cf. A328910, A329202."
			],
			"keyword": "nonn,tabf",
			"offset": "2,1",
			"author": "_M. F. Hasler_, Nov 07 2019",
			"references": 5,
			"revision": 32,
			"time": "2020-06-05T04:27:00-04:00",
			"created": "2019-11-08T11:44:52-05:00"
		}
	]
}