{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A334572",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 334572,
			"data": "1,1,2,2,1,1,3,3,2,1,2,2,1,1,4,4,2,2,2,2,1,1,3,3,2,3,3,2,1,1,5,5,1,1,2,2,1,1,3,3,1,1,2,2,2,1,4,4,2,2,2,2,3,3,3,3,1,1,2,2,1,2,6,6,1,1,2,2,1,1,3,3,1,2,2,2,1,1,4,4,4,1,2,2,1,1,3,3,2",
			"name": "Let x(n, k) be the exponent of prime(k) in the factorization of n, then a(n) = Max_{k} abs(x(n,k)- x(n-1,k)).",
			"comment": [
				"a(n) = d_infinite(n, n-1) as defined in Kolossváry link."
			],
			"link": [
				"István B. Kolossváry and István T. Kolossváry, \u003ca href=\"https://arxiv.org/abs/2005.02027\"\u003eDistance between natural numbers based on their prime signature\u003c/a\u003e, arXiv:2005.02027 [math.NT], 2020.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Chebyshev_distance\"\u003eChebyshev distance\u003c/a\u003e"
			],
			"formula": [
				"a(n) = max(A051903(n-1), A051903(n)). - _Pontus von Brömssen_, May 07 2020"
			],
			"example": [
				"The \"coordinates\" of the prime factorization are",
				"0,0,0,0, ... for n=1,",
				"1,0,0,0, ... for n=2,",
				"0,1,0,0, ... for n=3,",
				"2,0,0,0, ... for n=4,",
				"0,0,1,0, ... for n=5,",
				"1,1,0,0, ... for n=6;",
				"so the absolute differences are",
				"1,0,0,0, ... so a(2)=1,",
				"1,1,0,0, ... so a(3)=1,",
				"2,1,0,0, ... so a(4)=2,",
				"2,0,1,0, ... so a(5)=2,",
				"1,1,1,0, ... so a(6)=1."
			],
			"maple": [
				"f:= n-\u003e add(i[2]*x^i[1], i=ifactors(n)[2]):",
				"a:= n-\u003e max(map(abs, {coeffs(f(n)-f(n-1))})):",
				"seq(a(n), n=2..120);  # _Alois P. Heinz_, May 06 2020"
			],
			"mathematica": [
				"Block[{f}, f[n_] := If[n == 1, {0}, Function[g, ReplacePart[Table[0, {PrimePi[g[[-1, 1]]]}], #] \u0026@ Map[PrimePi@ First@ # -\u003e Last@ # \u0026, g]]@ FactorInteger@ n]; Array[Function[{a, b, m}, Max@ Abs[Subtract @@ #] \u0026@ Map[PadRight[#, m] \u0026, {a, b}]] @@ {#1, #2, Max@ Map[Length, {#1, #2}]} \u0026 @@ {f[# - 1], f@ #} \u0026, 106, 2]] (* _Michael De Vlieger_, May 06 2020 *)",
				"(* Second program: *)",
				"f[n_] := Sum[{p, e} = pe; e x^p, {pe, FactorInteger[n]}];",
				"a[n_] := CoefficientList[f[n]-f[n-1], x] // Abs // Max;",
				"a /@ Range[2, 90] (* _Jean-François Alcover_, Nov 16 2020, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(PARI) a(n) = {my(f=factor(n/(n-1))[,2]~); vecmax(apply(x-\u003eabs(x), f));}"
			],
			"xref": [
				"Cf. A051903, A067255, A124010, A334573, A334574."
			],
			"keyword": "nonn",
			"offset": "2,3",
			"author": "_Michel Marcus_, May 06 2020",
			"references": 2,
			"revision": 26,
			"time": "2020-11-16T09:35:35-05:00",
			"created": "2020-05-07T02:42:38-04:00"
		}
	]
}