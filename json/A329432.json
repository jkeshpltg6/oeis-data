{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A329432",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 329432,
			"data": "1,1,2,3,8,8,19,96,224,256,128,723,7296,35456,105472,208384,278528,245760,131072,32768,1045459,21100032,209001984,1339772928,6194997248,21845442560,60641837056,134967984128,243130040320,355391766528,419950493696,396881821696",
			"name": "Irregular triangular array, read by rows: row n shows the coefficients of the polynomial p(n,x) defined in Comments.",
			"comment": [
				"Let f(x) = 2 x^2 + 1, u(0,x) = 1, u(n,x) = f(u(n-1),x), and p(n,x) = u(n,sqrt(x)). Then the sequence (p(n,0)) = (1, 1, 3, 19, 723, 1045459, ... ) is a strong divisibility sequence, as implied by Dickson's record of a statement by J. J. Sylvester proved by W. S. Foster in 1889."
			],
			"reference": [
				"L. E. Dickson, History of the Theory of Numbers, vol. 1, Chelsea, New York, 1952, p. 403."
			],
			"example": [
				"Rows 0..4:",
				"  1;",
				"  1, 2;",
				"  3, 8, 8;",
				"  19, 96, 224, 256, 128;",
				"  723, 7296, 35456, 105472, 208384, 278528, 245760, 131072, 32768.",
				"Rows 0..4, the polynomials u(n,x):",
				"  1,",
				"  1 + 2 x^2",
				"  3 + 8 x^2 + 8 x^4",
				"  19 + 96 x^2 + 224 x^4 + 256 x^6 + 128 x^8",
				"  723 + 7296 x^2 + 35456 x^4 + 105472 x^6 + 208384 x^8 + 278528 x^10 + 245760 x^12 + 131072 x^14 + 32768 x^16."
			],
			"mathematica": [
				"f[x_] := 2 x^2 + 1;  u[0, x_] := 1;",
				"u[1, x_] := f[x]; u[n_, x_] := f[u[n - 1, x]]",
				"Column[Table [Expand[u[n, x]], {n, 0, 5}]] (* A329432 polynomials u(n,x) *)",
				"Table[CoefficientList[u[n, Sqrt[x], x], {n, 0, 5}]  (* A329432 array *)"
			],
			"xref": [
				"Cf. A329429, A329430, A329431, A329433."
			],
			"keyword": "nonn,tabf",
			"offset": "0,3",
			"author": "_Clark Kimberling_, Nov 23 2019",
			"references": 8,
			"revision": 4,
			"time": "2019-12-27T16:40:25-05:00",
			"created": "2019-12-27T16:40:25-05:00"
		}
	]
}