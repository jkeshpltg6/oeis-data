{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A291680",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 291680,
			"data": "1,0,1,0,1,1,0,1,3,2,0,1,9,8,4,0,1,25,36,20,10,0,1,71,156,108,58,26,0,1,205,666,586,340,170,74,0,1,607,2860,3098,2014,1078,528,218,0,1,1833,12336,16230,11888,6772,3550,1672,672,0,1,5635,53518,85150,69274,42366,23284,11840,5454,2126",
			"name": "Number T(n,k) of permutations p of [n] such that in 0p the largest up-jump equals k and no down-jump is larger than 2; triangle T(n,k), n\u003e=0, 0\u003c=k\u003c=n, read by rows.",
			"comment": [
				"An up-jump j occurs at position i in p if p_{i} \u003e p_{i-1} and j is the index of p_i in the increasingly sorted list of those elements in {p_{i}, ..., p_{n}} that are larger than p_{i-1}. A down-jump j occurs at position i in p if p_{i} \u003c p_{i-1} and j is the index of p_i in the decreasingly sorted list of those elements in {p_{i}, ..., p_{n}} that are smaller than p_{i-1}. First index in the lists is 1 here."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A291680/b291680.txt\"\u003eRows n = 0..140, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n,n) = A206464(n-1) for n\u003e0.",
				"Sum_{k=0..n} T(n,k) = A264868(n+1)."
			],
			"example": [
				"T(4,1) = 1: 1234.",
				"T(4,2) = 9: 1243, 1324, 1342, 2134, 2143, 2314, 2341, 2413, 2431.",
				"T(4,3) = 8: 1423, 1432, 3124, 3142, 3214, 3241, 3412, 3421.",
				"T(4,4) = 4: 4213, 4231, 4312, 4321.",
				"T(5,5) = 10: 53124, 53142, 53214, 53241, 53412, 53421, 54213, 54231, 54312, 54321.",
				"Triangle T(n,k) begins:",
				"  1;",
				"  0, 1;",
				"  0, 1,   1;",
				"  0, 1,   3,    2;",
				"  0, 1,   9,    8,    4;",
				"  0, 1,  25,   36,   20,   10;",
				"  0, 1,  71,  156,  108,   58,   26;",
				"  0, 1, 205,  666,  586,  340,  170,  74;",
				"  0, 1, 607, 2860, 3098, 2014, 1078, 528, 218;"
			],
			"maple": [
				"b:= proc(u, o, k) option remember; `if`(u+o=0, 1,",
				"      add(b(u-j, o+j-1, k), j=1..min(2, u))+",
				"      add(b(u+j-1, o-j, k), j=1..min(k, o)))",
				"    end:",
				"T:= (n, k)-\u003e b(0, n, k)-`if`(k=0, 0, b(0, n, k-1)):",
				"seq(seq(T(n, k), k=0..n), n=0..12);"
			],
			"mathematica": [
				"b[u_, o_, k_] := b[u, o, k] = If[u+o == 0, 1, Sum[b[u-j, o+j-1, k], {j, 1, Min[2, u]}] + Sum[b[u+j-1, o-j, k], {j, 1, Min[k, o]}]];",
				"T[n_, k_] := b[0, n, k] - If[k == 0, 0, b[0, n, k-1]];",
				"Table[T[n, k], {n, 0, 12}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, May 29 2019, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(Python)",
				"from sympy.core.cache import cacheit",
				"@cacheit",
				"def b(u, o, k): return 1 if u + o==0 else sum([b(u - j, o + j - 1, k) for j in range(1, min(2, u) + 1)]) + sum([b(u + j - 1, o - j, k) for j in range(1, min(k, o) + 1)])",
				"def T(n, k): return b(0, n, k) - (0 if k==0 else b(0, n, k - 1))",
				"for n in range(13): print([T(n, k) for k in range(n + 1)]) # _Indranil Ghosh_, Aug 30 2017"
			],
			"xref": [
				"Columns k=0-10 give: A000007, A057427, A291683, A321110, A321111, A321112, A321113, A321114, A321115, A321116, A321117.",
				"T(2n,n) gives A320290.",
				"Cf. A203717, A206464, A264868."
			],
			"keyword": "nonn,tabl",
			"offset": "0,9",
			"author": "_Alois P. Heinz_, Aug 29 2017",
			"references": 15,
			"revision": 26,
			"time": "2020-03-23T06:22:07-04:00",
			"created": "2017-08-29T17:57:55-04:00"
		}
	]
}