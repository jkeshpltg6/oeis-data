{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A169699",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 169699,
			"data": "1,5,12,25,28,56,56,113,60,120,120,240,120,240,240,481,124,248,248,496,248,496,496,992,248,496,496,992,496,992,992,1985,252,504,504,1008,504,1008,1008,2016,504,1008,1008,2016,1008,2016,2016,4032,504,1008,1008,2016",
			"name": "Total number of ON cells at stage n of two-dimensional 5-neighbor outer totalistic cellular automaton defined by \"Rule 510\".",
			"comment": [
				"We work on the square grid. Each cell has 4 neighbors, N, S, E, W. If none of your 4 neighbors are ON, your state does not change. If all 4 of your neighbors are ON, your state flips. In all other cases you turn ON. We start with one ON cell.",
				"As observed by Packard and Wolfram (see Fig. 2), a slice along the E-W line shows the successive states of the 1-D CA Rule 126 (see A071035, A071051)."
			],
			"reference": [
				"S. Wolfram, A New Kind of Science, Wolfram Media, 2002; p. 170."
			],
			"link": [
				"Robert Price, \u003ca href=\"/A169699/b169699.txt\"\u003eTable of n, a(n) for n = 0..128\u003c/a\u003e",
				"N. H. Packard and S. Wolfram, \u003ca href=\"http://new.math.uiuc.edu/im2008/dakkak/papers/files/wolfram.2dca.pdf\"\u003eTwo-Dimensional Cellular Automata\u003c/a\u003e, Journal of Statistical Physics, 38 (1985), 901-946.",
				"N. J. A. Sloane, \u003ca href=\"/A169699/a169699.pdf\"\u003eIllustration of first 28 generations\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"http://arxiv.org/abs/1503.01168\"\u003eOn the Number of ON Cells in Cellular Automata\u003c/a\u003e, arXiv:1503.01168, 2015",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ElementaryCellularAutomaton.html\"\u003eElementary Cellular Automaton\u003c/a\u003e",
				"S. Wolfram, \u003ca href=\"http://wolframscience.com/\"\u003eA New Kind of Science\u003c/a\u003e",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_2D_5-Neighbor_Cellular_Automata\"\u003eIndex to 2D 5-Neighbor Cellular Automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_Elementary_Cellular_Automata\"\u003eIndex to Elementary Cellular Automata\u003c/a\u003e"
			],
			"formula": [
				"For n\u003e0, it is easy to show that if 2^k \u003c= n \u003c 2^(k+1) then a(n) =",
				"(2^(k+1)-1)*2^(1+wt(n)), where wt is the binary weight A000120, except that if n is a power of 2 we must add 1 to the result."
			],
			"example": [
				"When arranged into blocks of sizes 1,1,2,4,8,16,...:",
				"1,",
				"5,",
				"12, 25,",
				"28, 56, 56, 113,",
				"60, 120, 120, 240, 120, 240, 240, 481,",
				"124, 248, 248, 496, 248, 496, 496, 992, 248, 496, 496, 992, 496, 992, 992, 1985,",
				"252, 504, 504, 1008, 504, 1008, 1008, 2016, 504, 1008, 1008, 2016, 1008, 2016, 2016, 4032, 504, 1008, 1008, 2016, 1008, 2016, 2016, 4032,",
				"..., the initial terms in the rows (after the initial rows) have the form 2^m-4 and the final terms are given by A092440. The row beginning with 2^m-4 is divisible by 2^(m-2)-1 (see formula)."
			],
			"maple": [
				"A000120 := proc(n) add(i,i=convert(n,base,2)) end:",
				"ht:=n-\u003efloor(log[2](n));",
				"f:=proc(n) local a,t1;",
				"if n=0 then 1 else",
				"a:=(2^(ht(n)+1)-1)*2^(1+A000120(n));",
				"if 2^log[2](n)=n then a:=a+1; fi; a; fi; end;",
				"[seq(f(n),n=0..65)]; # A169699"
			],
			"mathematica": [
				"Map[Function[Apply[Plus,Flatten[ #1]]], CellularAutomaton[{ 510, {2,{{0,2,0},{2,1,2},{0,2,0}}},{1,1}},{{{1}},0},100]]",
				"ArrayPlot /@ CellularAutomaton[{510, {2, {{0, 2, 0}, {2, 1, 2}, {0, 2, 0}}}, {1, 1}}, {{{1}}, 0}, 28]"
			],
			"xref": [
				"Cf. A000120, A071035, A071051, A008574, A092440, A169700.",
				"See A253089 for 9-celled neighborhood version."
			],
			"keyword": "nonn,tabf",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Apr 17 2010",
			"ext": [
				"Entry revised with more precise definition, formula and additional information, _N. J. A. Sloane_, Aug 24 2014"
			],
			"references": 17,
			"revision": 28,
			"time": "2016-05-04T19:05:32-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}