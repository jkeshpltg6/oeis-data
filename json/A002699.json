{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002699",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2699,
			"id": "M2090 N0825",
			"data": "0,2,16,96,512,2560,12288,57344,262144,1179648,5242880,23068672,100663296,436207616,1879048192,8053063680,34359738368,146028888064,618475290624,2611340115968,10995116277760,46179488366592,193514046488576",
			"name": "a(n) = n*2^(2*n-1).",
			"comment": [
				"Right side of binomial sum Sum(i * binomial(2*n, i), i=1..n) - Yong Kong (ykong(AT)curagen.com), Dec 26 2000",
				"Coefficients of shifted Chebyshev polynomials.",
				"Starting with offset 1 = 4th binomial transform of [2, 8, 0, 0, 0, ...]. - _Gary W. Adamson_, Jul 21 2009",
				"Let P(A) be the power set of an n-element set A and B be the Cartesian product of P(A) with itself. Then a(n) = the sum of the size of the symmetric difference of x and y for every (x,y) of B. - _Ross La Haye_, Jan 04 2013",
				"It's the relation [27] with T(n) in the document of Ross. Following the last comment of Ross, A002697 is the similar sequence when replacing \"symmetric difference\" by \"intersection\" and A212698 is the similar  sequence when replacing \"symmetric difference\" by union. - _Bernard Schott_, Jan 04 2013",
				"If Delta = Symmetric difference, here, X Delta Y and Y Delta X are considered as two distinct Cartesian products, if we want to consider that X Delta Y = X Delta Y is the same Cartesian product, see A002697. - _Bernard Schott_, Jan 15 2013"
			],
			"reference": [
				"C. Lanczos, Applied Analysis. Prentice-Hall, Englewood Cliffs, NJ, 1956, p. 518.",
				"A. P. Prudnikov, Yu. A. Brychkov and O.I. Marichev, \"Integrals and Series\", Volume 1: \"Elementary Functions\", Chapter 4: \"Finite Sums\", New York, Gordon and Breach Science Publishers, 1986-1992.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A002699/b002699.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Ross La Haye, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL12/LaHaye/lahaye5.html\"\u003eBinary Relations on the Power Set of an n-Element Set\u003c/a\u003e, Journal of Integer Sequences, Vol. 12 (2009), Article 09.2.6.",
				"C. Lanczos, \u003ca href=\"/A002457/a002457.pdf\"\u003eApplied Analysis\u003c/a\u003e (Annotated scans of selected pages)",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (8,-16)."
			],
			"formula": [
				"a(n) = 2 * A002697(n). - _Bernard Schott_, Jan 04 2013",
				"a(n) = A212698(n) - A002697(n)",
				"a(n) = 8*a(n-1)-16*a(n-2) with n\u003e1, a(0)=0, a(1)=2. - _Vincenzo Librandi_, Mar 20 2013",
				"G.f.: (2*x)/(1 - 4*x)^2 (* _Harvey P. Dale_, Jul 28 2021 *)"
			],
			"maple": [
				"A002699 := n-\u003en*2^(2*n-1);",
				"A002699:=2*z/(4*z-1)**2; # conjectured by _Simon Plouffe_ in his 1992 dissertation"
			],
			"mathematica": [
				"Table[(n 2^(2 n - 1)), {n, 0, 30}] (* _Vincenzo Librandi_, Mar 20 2013 *)",
				"LinearRecurrence[{8,-16},{0,2},30] (* _Harvey P. Dale_, Dec 20 2015 *)"
			],
			"program": [
				"(MAGMA) [n*2^(2*n-1): n in [0..30]]; /* or */ I:=[0, 2]; [n le 2 select I[n] else 8*Self(n-1)-16*Self(n-2): n in [1..30]]; // _Vincenzo Librandi_, Mar 20 2013",
				"(PARI) a(n)=n*2^(2*n-1) \\\\ _Charles R Greathouse IV_, Oct 07 2015"
			],
			"xref": [
				"Found in A053125 and A053124.",
				"Cf. A002697, A212698."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 8,
			"revision": 74,
			"time": "2021-07-28T16:47:49-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}