{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002023",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2023,
			"data": "6,24,96,384,1536,6144,24576,98304,393216,1572864,6291456,25165824,100663296,402653184,1610612736,6442450944,25769803776,103079215104,412316860416,1649267441664,6597069766656,26388279066624,105553116266496,422212465065984",
			"name": "a(n) = 6*4^n.",
			"comment": [
				"From _Peter M. Chema_, Mar 02 2017: (Start)",
				"Number of rods (line segments) required to make a Sierpinski tetrahedron of side length 2^n.",
				"Also equals the number of balls (vertices) in a Sierpinski tetrahedron of side length 2^n+1 minus the number of balls in a Sierpinski tetrahedron of side length 2^n (the first difference in the tetrix numbers). See formula. (End)",
				"Equivalently, the number of edges in the (n+1)-Sierpinski tetrahedron graph. - _Eric W. Weisstein_, Aug 17 2017",
				"These numbers a(n) together with the 13 numbers from A337217 give the positive integers m represented uniquely by the ternary form x^2 + y^2 + 2*z^2, with integers 0 \u003c= x \u003c= y and 0 \u003c= z. This is theorem 2.1 of Kaplansky, p. 87 with proof on p. 90. - _Wolfdieter Lang_, Aug 20 2020",
				"a(n) is also the domination number of the (n+3)-Sierpinski tetrahedron graph. - _Eric W. Weisstein_, Sep 13 2021"
			],
			"reference": [
				"Irving Kaplansky, Integers Uniquely Represented by Certain Ternary Forms, in \"The Mathematics of Paul Erdős I\", Ronald. L. Graham and Jaroslav Nešetřil (Eds.), Springer, 1997, pp. 86 - 94."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A002023/b002023.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/DominationNumber.html\"\u003eDomination Number\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/SierpinskiTetrahedronGraph.html\"\u003eSierpinski Tetrahedron Graph\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_01\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4)."
			],
			"formula": [
				"From _Philippe Deléham_, Nov 23 2008: (Start)",
				"a(n) = 4*a(n-1) for n \u003e 0, a(0)=6.",
				"G.f.: 6/(1-4*x). (End)",
				"a(n) = 3*A004171(n). - _R. J. Mathar_, Mar 08 2011",
				"From _Peter M. Chema_, Mar 03 2017: (Start)",
				"a(n) = A283070(n+1) - A283070(n).",
				"a(n) = A004171(n+1) - A004171(n). (End)",
				"E.g.f.: 6*exp(4*x). - _G. C. Greubel_, Aug 17 2017"
			],
			"mathematica": [
				"6*4^Range[0, 100] (* _Vladimir Joseph Stephan Orlovsky_, Jun 09 2011 *)",
				"Table[6 4^n, {n, 0, 20}] (* _Eric W. Weisstein_, Aug 17 2017 *)",
				"LinearRecurrence[{4}, {6}, 20] (* _Eric W. Weisstein_, Aug 17 2017 *)",
				"CoefficientList[Series[6/(1 - 4 x), {x, 0, 20}], x] (* _Eric W. Weisstein_, Aug 17 2017 *)"
			],
			"program": [
				"(MAGMA) [6*4^n: n in [0..30]]; // _Vincenzo Librandi_, May 16 2011",
				"(PARI) a(n)=6\u003c\u003c(2*n) \\\\ _Charles R Greathouse IV_, Apr 17 2012"
			],
			"xref": [
				"Cf. A283070 (vertex count).",
				"Cf. A004171."
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_",
			"references": 18,
			"revision": 78,
			"time": "2021-09-14T03:56:31-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}