{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A057081",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 57081,
			"data": "1,10,89,791,7030,62479,555281,4935050,43860169,389806471,3464398070,30789776159,273643587361,2432002510090,21614379003449,192097408520951,1707262297685110,15173263270645039,134852107138120241",
			"name": "Even indexed Chebyshev U-polynomials evaluated at sqrt(11)/2.",
			"comment": [
				"This is the m=11 member of the m-family of sequences S(n,m-2)+S(n-1,m-2) = S(2*n,sqrt(m)) (for S(n,x) see Formula). The m=4..10 instances are: A005408, A002878, A001834, A030221, A002315, A033890 and A057080, resp. The m=1..3 (signed) sequences are: A057078, A057077 and A057079, resp.",
				"a(n) = L(n,-9)*(-1)^n, where L is defined as in A108299; see also A070998 for L(n,+9). - _Reinhard Zumkeller_, Jun 01 2005",
				"General recurrence is a(n)=(a(1)-1)*a(n-1)-a(n-2), a(1)\u003e=4, lim n-\u003einfinity a(n)= x*(k*x+1)^n, k =(a(1)-3), x=(1+sqrt((a(1)+1)/(a(1)-3)))/2. Examples in OEIS: a(1)=4 gives A002878. a(1)=5 gives A001834. a(1)=6 gives A030221. a(1)=7 gives A002315. a(1)=8 gives A033890. a(1)=9 gives A057080. a(1)=10 gives A057081. [_Ctibor O. Zizka_, Sep 02 2008]",
				"The primes in this sequence are 89, 389806471, 192097408520951, 7477414486269626733119, ... - _Ctibor O. Zizka_, Sep 02 2008",
				"The aerated sequence (b(n))n\u003e=1 = [1, 0, 10, 0, 89, 0, 791, 0, ...] is a fourth-order linear divisibility sequence; that is, if n | m then b(n) | b(m). It is the case P1 = 0, P2 = -7, Q = -1 of the 3-parameter family of divisibility sequences found by Williams and Guy. See A100047. - _Peter Bala_, Mar 22 2015"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A057081/b057081.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Marco Abrate, Stefano Barbero, Umberto Cerruti, Nadir Murru, \u003ca href=\"https://www.emis.de/journals/INTEGERS/papers/p38/p38.Abstract.html\"\u003ePolynomial sequences on quadratic curves\u003c/a\u003e, Integers, Vol. 15, 2015, #A38.",
				"Andersen, K., Carbone, L. and Penta, D., \u003ca href=\"https://pdfs.semanticscholar.org/8f0c/c3e68d388185129a56ed73b5d21224659300.pdf\"\u003eKac-Moody Fibonacci sequences, hyperbolic golden ratios, and real quadratic fields\u003c/a\u003e, Journal of Number Theory and Combinatorics, Vol 2, No. 3 pp 245-278, 2011. See Section 9.",
				"Alex Fink, Richard K. Guy, and Mark Krusemeyer, \u003ca href=\"https://doi.org/10.11575/cdm.v3i2.61940\"\u003ePartitions with parts occurring at most thrice\u003c/a\u003e, Contributions to Discrete Mathematics, Vol 3, No 2 (2008), pp. 76-114. See Section 13.",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"Wolfdieter Lang, \u003ca href=\"http://www.fq.math.ca/Scanned/38-5/lang.pdf\"\u003eOn polynomials related to powers of the generating function of Catalan's numbers\u003c/a\u003e, Fib. Quart. 38 (2000) 408-419. Eq.(44), rhs, m=11.",
				"H. C. Williams and R. K. Guy, \u003ca href=\"http://dx.doi.org/10.1142/S1793042111004587\"\u003eSome fourth-order linear divisibility sequences\u003c/a\u003e, Intl. J. Number Theory 7 (5) (2011) 1255-1277.",
				"H. C. Williams and R. K. Guy, \u003ca href=\"http://www.emis.de/journals/INTEGERS/papers/a17self/a17self.Abstract.html\"\u003eSome Monoapparitic Fourth Order Linear Divisibility Sequences\u003c/a\u003e Integers, Volume 12A (2012) The John Selfridge Memorial Volume.",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (9,-1)."
			],
			"formula": [
				"a(n) = 9*a(n-1)-a(n-2), a(-1)=-1, a(0)=1.",
				"a(n) = S(n, 9)+S(n-1, 9)= S(2*n, sqrt(11)) with S(n, x) := U(n, x/2), Chebyshev polynomials of 2nd kind, A049310. S(n, 9)= A018913(n).",
				"G.f.: (1+x)/(1-9*x+x^2).",
				"Let q(n, x) = sum(i=0, n, x^(n-i)*binomial(2*n-i, i)), a(n) = (-1)^n*q(n, -11). - _Benoit Cloitre_, Nov 10 2002"
			],
			"maple": [
				"A057081 := proc(n)",
				"    option remember;",
				"    if n \u003c= 1 then",
				"        op(n+1,[1,10]);",
				"    else",
				"        9*procname(n-1)-procname(n-2) ;",
				"    end if;",
				"end proc: # _R. J. Mathar_, Apr 30 2017"
			],
			"mathematica": [
				"CoefficientList[Series[(1 + x)/(1 - 9*x + x^2), {x,0,50}], x] (* or *) LinearRecurrence[{9,-1}, {1,10}, 50] (* _G. C. Greubel_, Apr 12 2017 *)"
			],
			"program": [
				"(Sage) [(lucas_number2(n,9,1)-lucas_number2(n-1,9,1))/7 for n in range(1, 20)] # _Zerinvary Lajos_, Nov 10 2009",
				"(PARI) Vec((1+x)/(1-9*x+x^2) + O(x^30)) \\\\ _Michel Marcus_, Mar 22 2015"
			],
			"xref": [
				"Cf. A005408, A002878, A001834, A030221, A002315, A033890, A057080.",
				"Cf. A057078, A057077, A057079.",
				"Cf. A018913, A049310.",
				"Cf. A100047."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_ Aug 04 2000",
			"references": 18,
			"revision": 47,
			"time": "2019-12-07T12:18:22-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}