{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A131128",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 131128,
			"data": "1,2,8,20,44,92,188,380,764,1532,3068,6140,12284,24572,49148,98300,196604,393212,786428,1572860,3145724,6291452,12582908,25165820,50331644,100663292,201326588,402653180,805306364,1610612732,3221225468",
			"name": "Binomial transform of [1, 1, 5, 1, 5, 1, 5, ...].",
			"comment": [
				"Row sums of triangle A131129. - _Emeric Deutsch_, Jun 19 2007",
				"For n \u003e= 4, a(n) is the number of vertices in the dendrimer nanostar NS1[n-3] defined pictorially in the Ashrafi et al. reference (Ns1[3] is shown in Fig. 1) or in the Ahmadi et al. reference (Fig. 1). - _Emeric Deutsch_, May 17 2018"
			],
			"reference": [
				"B. Monjardet, Acyclic domains of linear orders: a survey, in \"The Mathematics of Preference, Choice and Order: Essays in Honor of Peter Fishburn\", edited by Steven Brams, William V. Gehrlein and Fred S. Roberts, Springer, 2009, pp. 139-160. [From _N. J. A. Sloane_, Feb 07 2009]"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A131128/b131128.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"M. B. Ahmadi and M. Sadeghimehr, \u003ca href=\"https://oam-rc.inoe.ro/download.php?idu=1158=52\"\u003eAtom bond connectivity index of an infinite class NS1[n] of dendrimer nanostars\u003c/a\u003e, Optoelectronics and Advanced Materials, 4(7):1040-1042 July 2010.",
				"Ali Reza Ashrafi and Parisa Nikzad, \u003ca href=\"http://www.chalcogen.ro/383_Ashrafi.pdf\"\u003eKekulé index and bounds of energy for nanostar dendrimers\u003c/a\u003e, Digest J. of Nanomaterials and Biostructures, 4, No. 2, 2009, 383-388.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-2)."
			],
			"formula": [
				"a(n) = 3*2^n - 4 for n \u003e= 1; a(0)=1. Formula follows by replacing [1,1,5,1,5,1,...] with [1,3-2,3+2,3-2,3+2,3-2,...]. - _Emeric Deutsch_, Jun 19 2007",
				"G.f.: (1 - x + 4x^2)/((1-x)(1-2x)). - _Emeric Deutsch_, Jul 09 2007",
				"Row sums of triangle A132047. - _Gary W. Adamson_, Aug 08 2007",
				"a(n) = 2*a(n-1) + 4 for n \u003e= 2, a(0)=1, a(1)=2. - _Philippe Deléham_, Sep 23 2009",
				"a(n) = 2*A033484(n-1) for n\u003e0. - _R. J. Mathar_, Feb 27 2019"
			],
			"example": [
				"a(3) = 20 = (1, 3, 3, 1) dot (1, 1, 5, 1) = (1 + 3 + 15 + 1)."
			],
			"maple": [
				"1, seq(3*2^n-4, n = 1 .. 30); # _Emeric Deutsch_, Jun 19 2007"
			],
			"mathematica": [
				"CoefficientList[Series[(1-x+4x^2)/((1-x)(1-2x)),{x,0,40}],x] (* _Vincenzo Librandi_, Apr 11 2012 *)"
			],
			"program": [
				"(GAP) Concatenation([1],List([1..30], n-\u003e3*2^n-4)); # _Muniru A Asiru_, May 17 2018"
			],
			"xref": [
				"Cf. A131129, A095121, A132047."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Gary W. Adamson_, Jun 16 2007",
			"references": 11,
			"revision": 39,
			"time": "2019-02-27T04:53:27-05:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}