{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A089817",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 89817,
			"data": "1,6,30,145,696,3336,15985,76590,366966,1758241,8424240,40362960,193390561,926589846,4439558670,21271203505,101916458856,488311090776,2339638995025,11209883884350,53709780426726,257339018249281",
			"name": "a(n) = 5*a(n-1) - a(n-2) + 1 with a(0)=1, a(1)=6.",
			"comment": [
				"Partial sums of Chebyshev sequence S(n,5) = U(n,5/2) = A004254(n) (Chebyshev's polynomials of the second kind, see A049310). - _Wolfdieter Lang_, Aug 31 2004",
				"In this sequence 4*a(n)*a(n+2)+1 is a square. - _Bruno Berselli_, Jun 19 2012"
			],
			"reference": [
				"R. C. Alperin, A nonlinear recurrence and its relations to Chebyshev polynomials, Fib. Q., 58:2 (2020), 140-142."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A089817/b089817.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Ioana-Claudia Lazăr, \u003ca href=\"https://arxiv.org/abs/1904.06555\"\u003eLucas sequences in t-uniform simplicial complexes\u003c/a\u003e, arXiv:1904.06555 [math.GR], 2019.",
				"F. M. van Lamoen, \u003ca href=\"http://forumgeom.fau.edu/FG2006volume6/FG200637index.html\"\u003eSquare wreaths around hexagons\u003c/a\u003e, Forum Geometricorum, 6 (2006) 311-325.",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (6,-6,1)."
			],
			"formula": [
				"For n \u003e 0, a(n-1) = Sum_{i=1..n} Sum_{j=1..i} b(n) with b(n) as in A004253.",
				"a(n) = (2/3 - sqrt(21)/7)*(5/2 - sqrt(21)/2)^n + (2/3 + sqrt(21)/7)*(5/2 + sqrt(21)/2)^n - 1/3.",
				"G.f.: 1/((1-x)*(1 - 5*x + x^2)) = 1/(1 - 6*x + 6*x^2 - x^3).",
				"a(n) = 6*a(n-1) - 6*a(n-2) + a(n-3) for n \u003e= 2, a(-1):=0, a(0)=1, a(1)=6.",
				"a(n) = (S(n+1, 5) - S(n, 5) - 1)/3 for n \u003e= 0.",
				"a(n)*a(n-2) = a(n-1)*(a(n-1)-1) for n \u003e 1. - _Bruno Berselli_, Nov 29 2016"
			],
			"mathematica": [
				"Join[{a=1,b=6},Table[c=5*b-a+1;a=b;b=c,{n,60}]] (* _Vladimir Joseph Stephan Orlovsky_, Feb 06 2011*)",
				"CoefficientList[Series[1/(1 - 6*x + 6*x^2 - x^3), {x, 0, 50}], x] (* _G. C. Greubel_, Nov 20 2017 *)"
			],
			"program": [
				"(PARI) a(n)=([0,1,0; 0,0,1; 1,-6,6]^n*[1;6;30])[1,1] \\\\ _Charles R Greathouse IV_, Nov 29 2016",
				"(PARI) x='x+O('x^50); Vec(1/(1-6*x+6*x^2-x^3)) \\\\ _G. C. Greubel_, Nov 20 2017",
				"(MAGMA) [Round((2/3 - Sqrt(21)/7)*(5/2 - Sqrt(21)/2)^n + (2/3 + Sqrt(21)/7)*(5/2 + Sqrt(21)/2)^n - 1/3): n in [0..30]]; // _G. C. Greubel_, Nov 20 2017"
			],
			"xref": [
				"Cf. A061278, A053142, A101368.",
				"See. A212336 for more sequences with g.f. of the type 1/(1-k*x+k*x^2-x^3)."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Paul Barry_, Nov 14 2003",
			"references": 13,
			"revision": 45,
			"time": "2020-06-02T22:22:25-04:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}