{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A133399",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 133399,
			"data": "1,1,1,2,1,9,1,28,12,1,75,120,1,186,750,120,1,441,3780,2100,1,1016,16856,21840,1680,1,2295,69552,176400,45360,1,5110,272250,1224720,705600,30240,1,11253,1026300,7692300,8316000,1164240,1,24564,3762132,45018600",
			"name": "Triangle T(n,k)=number of forests of labeled rooted trees with n nodes, containing exactly k trees of height one, all others having height zero (n\u003e=0, 0\u003c=k\u003c=floor(n/2)).",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A133399/b133399.txt\"\u003eRows n = 0..200, flattened\u003c/a\u003e",
				"A. P. Heinz, \u003ca href=\"https://doi.org/10.1007/3-540-53504-7_68\"\u003eFinding Two-Tree-Factor Elements of Tableau-Defined Monoids in Time O(n^3)\u003c/a\u003e, Ed. S. G. Akl, F. Fiala, W. W. Koczkodaj: Advances in Computing and Information, ICCI90 Niagara Falls, LNCS 468, Springer-Verlag (1990), pp. 120-128."
			],
			"formula": [
				"T(n,k) = C(n,k) * k! * stirling2(n-k+1,k+1).",
				"E.g.f.: exp(y*x*(exp(x)-1))*exp(x). - _Geoffrey Critzer_, Feb 09 2013",
				"Sum_{k=1..floor(n/2)} T(n,k) = A235596(n+1). - _Alois P. Heinz_, Jun 21 2019"
			],
			"example": [
				"Triangle begins:",
				"  1;",
				"  1;",
				"  1,     2;",
				"  1,     9;",
				"  1,    28,     12;",
				"  1,    75,    120;",
				"  1,   186,    750,     120;",
				"  1,   441,   3780,    2100;",
				"  1,  1016,  16856,   21840,   1680;",
				"  1,  2295,  69552,  176400,  45360;",
				"  1,  5110, 272250, 1224720, 705600, 30240;",
				"  ..."
			],
			"maple": [
				"T:= (n,k)-\u003e binomial(n,k)*k!*Stirling2(n-k+1,k+1): for n from 0 to 10 do lprint(seq(T(n, k), k=0..floor(n/2))) od;"
			],
			"mathematica": [
				"nn=12;f[list_]:=Select[list,#\u003e0\u0026];Map[f,Range[0,nn]!CoefficientList[ Series[Exp[y x (Exp[x]-1)] Exp[x],{x,0,nn}],{x,y}]]//Grid (* _Geoffrey Critzer_, Feb 09 2013 *)",
				"t[n_, k_] := Binomial[n, k]*k!*StirlingS2[n-k+1, k+1]; Table[t[n, k], {n, 0, 12}, {k, 0, n/2}] // Flatten (* _Jean-François Alcover_, Dec 19 2013 *)"
			],
			"program": [
				"(MAGMA) /* As triangle */ [[Binomial(n,k)*Factorial(k)*StirlingSecond(n-k+1,k+1): k in [0..Floor(n/2)]]: n in [0.. 15]]; // _Vincenzo Librandi_, Jun 06 2019"
			],
			"xref": [
				"Columns k=1,2 give: A058877, A133386.",
				"Row sums give: A000248.",
				"T(2n,n) = A001813(n), T(2n+1,n) = A002691(n).",
				"Reading the table by diagonals gives triangle A198204. - _Peter Bala_, Jul 31 2012",
				"Cf. A235596."
			],
			"keyword": "nonn,tabf",
			"offset": "0,4",
			"author": "_Alois P. Heinz_, Nov 24 2007",
			"references": 5,
			"revision": 46,
			"time": "2019-06-21T17:25:22-04:00",
			"created": "2007-12-09T03:00:00-05:00"
		}
	]
}