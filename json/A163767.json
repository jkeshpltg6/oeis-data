{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A163767",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 163767,
			"data": "1,2,3,10,5,36,7,120,45,100,11,936,13,196,225,3876,17,3078,19,4200,441,484,23,62400,325,676,3654,11368,29,27000,31,376992,1089,1156,1225,443556,37,1444,1521,459200,41,74088,43,43560,46575,2116,47,11995200,1225",
			"name": "a(n) = tau_{n}(n) = number of ordered n-factorizations of n.",
			"comment": [
				"Also the number of length n - 1 chains of divisors of n. - _Gus Wiseman_, May 07 2021"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A163767/b163767.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (first 1000 terms from Enrique Pérez Herrero)"
			],
			"formula": [
				"a(p) = p for prime p.",
				"a(n) = n^k when n is the product of k distinct primes (conjecture).",
				"a(n) = n-th term of the n-th Dirichlet self-convolution of the all 1's sequence."
			],
			"example": [
				"Successive Dirichlet self-convolutions of the all 1's sequence begin:",
				"(1),1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,... (A000012)",
				"1,(2),2,3,2,4,2,4,3,4,2,6,2,4,4,5,... (A000005)",
				"1,3,(3),6,3,9,3,10,6,9,3,18,3,9,9,15,... (A007425)",
				"1,4,4,(10),4,16,4,20,10,16,4,40,4,16,16,35,... (A007426)",
				"1,5,5,15,(5),25,5,35,15,25,5,75,5,25,25,70,... (A061200)",
				"1,6,6,21,6,(36),6,56,21,36,6,126,6,36,36,126,... (A034695)",
				"1,7,7,28,7,49,(7),84,28,49,7,196,7,49,49,210,... (A111217)",
				"1,8,8,36,8,64,8,(120),36,64,8,288,8,64,64,330,... (A111218)",
				"1,9,9,45,9,81,9,165,(45),81,9,405,9,81,81,495,... (A111219)",
				"1,10,10,55,10,100,10,220,55,(100),10,550,10,100,... (A111220)",
				"1,11,11,66,11,121,11,286,66,121,(11),726,11,121,... (A111221)",
				"1,12,12,78,12,144,12,364,78,144,12,(936),12,144,... (A111306)",
				"...",
				"where the main diagonal forms this sequence.",
				"From _Gus Wiseman_, May 07 2021: (Start)",
				"The a(1) = 1 through a(5) = 5 chains of divisors:",
				"  ()  (1)  (1/1)  (1/1/1)  (1/1/1/1)",
				"      (2)  (3/1)  (2/1/1)  (5/1/1/1)",
				"           (3/3)  (2/2/1)  (5/5/1/1)",
				"                  (2/2/2)  (5/5/5/1)",
				"                  (4/1/1)  (5/5/5/5)",
				"                  (4/2/1)",
				"                  (4/2/2)",
				"                  (4/4/1)",
				"                  (4/4/2)",
				"                  (4/4/4)",
				"(End)"
			],
			"mathematica": [
				"Table[Times@@(Binomial[#+n-1,n-1]\u0026/@FactorInteger[n][[All,2]]),{n,1,50}] (* _Enrique Pérez Herrero_, Dec 25 2013 *)"
			],
			"program": [
				"(PARI) {a(n,m=n)=if(n==1,1,if(m==1,1,sumdiv(n,d,a(d,1)*a(n/d,m-1))))}"
			],
			"xref": [
				"Main diagonal of A077592.",
				"Diagonal n = k + 1 of the array A334997.",
				"The version counting all multisets of divisors (not just chains) is A343935.",
				"A000005 counts divisors.",
				"A001055 counts factorizations (strict: A045778, ordered: A074206).",
				"A001221 counts distinct prime factors.",
				"A001222 counts prime factors with multiplicity.",
				"A067824 counts strict chains of divisors starting with n.",
				"A122651 counts strict chains of divisors summing to n.",
				"A146291 counts divisors of n with k prime factors (with multiplicity).",
				"A167865 counts strict chains of divisors \u003e 1 summing to n.",
				"A253249 counts nonempty strict chains of divisors of n.",
				"A251683/A334996 count strict nonempty length-k divisor chains from n to 1.",
				"A337255 counts strict length-k chains of divisors starting with n.",
				"A339564 counts factorizations with a selected factor.",
				"A343662 counts strict length-k chains of divisors (row sums: A337256).",
				"Cf. A002033, A007425, A008480, A018818, A062319, `A066959, A186972, A327527, A337105, A337107, A343658."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Paul D. Hanna_, Aug 04 2009",
			"references": 12,
			"revision": 18,
			"time": "2021-05-08T08:32:24-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}