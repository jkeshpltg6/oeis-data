{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A286326",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 286326,
			"data": "0,1,1,1,2,1,2,2,1,3,2,2,3,1,3,3,2,4,2,3,4,1,4,3,3,5,2,4,4,2,5,3,4,5,1,5,4,3,6,3,5,5,2,6,4,4,6,2,6,5,3,7,4,5,6,1,7,5,4,7,3,6,6,3,8,5,5,7,2,7,6,4,8,4,6,7,2,8,6,5,8,3,7,7,4,9,5",
			"name": "Least possible maximum of the two initial terms of a Fibonacci-like sequence containing n.",
			"comment": [
				"A Fibonacci-like sequence f satisfies f(n+2) = f(n+1) + f(n), and is uniquely identified by its two initial terms f(0) and f(1); here we consider Fibonacci-like sequences with f(0) \u003e= 0 and f(1) \u003e= 0.",
				"This sequence is part of a family of variations of A249783, where we minimize a function g of the initial terms of Fibonacci-like sequences containing n:",
				"- A249783: g(f) = f(0) + f(1),",
				"- A286321: g(f) = f(0) * f(1),",
				"- a:       g(f) = max(f(0), f(1)),",
				"- A286327: g(f) = f(0)^2 + f(1)^2.",
				"For any n\u003e0, a(n) \u003c= n (as the Fibonacci-like sequence with initial terms n and 0 contains n).",
				"For any n\u003e0, a(A000045(n)) = 1.",
				"Apparently the same as A097368 for n \u003e 1. - _Georg Fischer_, Oct 09 2018"
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A286326/b286326.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A286326/a286326.png\"\u003eScatterplot of the first 100000 terms\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A286326/a286326.pdf\"\u003eIllustration of the first terms\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A286326/a286326_1.txt\"\u003eC program for A286326\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A286326/a286326_1.png\"\u003eColored scatterplot of (n, a(n)) for n = 1..100000\u003c/a\u003e (where the color is function of the least k \u003e 0 such that a(n)/n \u003e= A000045(k)/A001654(k))"
			],
			"example": [
				"See illustration of the first terms in Links section."
			],
			"mathematica": [
				"{0}~Join~Table[Module[{a = 0, b = 1, s = {}}, While[a \u003c= n, AppendTo[s, Flatten@ NestWhileList[{#2, #1 + #2} \u0026 @@ # \u0026, {a, b}, Last@ # \u003c n \u0026]]; If[a + b \u003e= n, a++; b = 1, b++]]; Min@ Map[Max@ #[[1 ;; 2]] \u0026, Select[s, MemberQ[#, n] \u0026]]], {n, 86}] (* _Michael De Vlieger_, May 10 2017 *)"
			],
			"xref": [
				"Cf. A000045, A001654, A097368, A249783, A286321, A286327."
			],
			"keyword": "nonn,look",
			"offset": "0,5",
			"author": "_Rémy Sigrist_, May 07 2017",
			"references": 4,
			"revision": 23,
			"time": "2019-03-09T05:11:37-05:00",
			"created": "2017-05-10T23:55:05-04:00"
		}
	]
}