{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A015518",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 15518,
			"data": "0,1,2,7,20,61,182,547,1640,4921,14762,44287,132860,398581,1195742,3587227,10761680,32285041,96855122,290565367,871696100,2615088301,7845264902,23535794707,70607384120,211822152361,635466457082",
			"name": "a(n) = 2*a(n-1) + 3*a(n-2), with a(0)=0, a(1)=1.",
			"comment": [
				"Number of walks of length n between any two distinct vertices of the complete graph K_4. - _Paul Barry_ and _Emeric Deutsch_, Apr 01 2004",
				"For n \u003e= 1, a(n) is the number of integers k, 1 \u003c= k \u003c= 3^(n-1), whose ternary representation ends in an even number of zeros (see A007417). - _Philippe Deléham_, Mar 31 2004",
				"Form the digraph with matrix A=[0,1,1,1;1,0,1,1;1,1,0,1;1,0,1,1]. A015518(n) corresponds to the (1,3) term of A^n. - _Paul Barry_, Oct 02 2004",
				"The same sequence may be obtained by the following process. Starting a priori with the fraction 1/1, the denominators of fractions built according to the rule: add top and bottom to get the new bottom, add top and 4 times the bottom to get the new top. The limit of the sequence of fractions is 2. - _Cino Hilliard_, Sep 25 2005",
				"(A046717(n))^2 + (2*a(n))^2 = A046717(2n). E.g., A046717(3) = 13, 2*a(3) = 14, A046717(6) = 365. 13^2 + 14^2 = 365. - _Gary W. Adamson_, Jun 17 2006",
				"For n \u003e= 2, number of ordered partitions of n-1 into parts of sizes 1 and 2 where there are two types of 1 (singletons) and three types of 2 (twins). For example, the number of possible configurations of families of n-1 male (M) and female (F) offspring considering only single births and twins, where the birth order of M/F/pair-of-twins is considered and there are three types of twins; namely, both F, both M, or one F and one M - where birth order within a pair of twins itself is disregarded. In particular, for a(3)=7, two children could be either: (1) F, then M; (2) M, then F; (3) F,F; (4) M,M; (5) F,F twins; (6) M,M twins; or (7) M,F twins (emphasizing that birth order is irrelevant here when both/all children are the same gender and when two children are within the same pair of twins). - _Rick L. Shepherd_, Sep 18 2004",
				"a(n) is prime for n = {2, 3, 5, 7, 13, 23, ...}, where only a(2) = 2 corresponds to a prime of the form (3^n - 1)/4. All prime a(n), except a(2) = 2, are the primes of the form (3^n + 1)/4. Numbers n such that (3^n + 1)/4 is prime are listed in A007658(n) = {3, 5, 7, 13, 23, 43, 281, 359, 487, 577, 1579, 1663, 1741, 3191, 9209, 11257, 12743, 13093, 17027, 26633, ...}. Note that all prime a(n) have prime indices. Prime a(n) are listed in A111010(n) = {2, 7, 61, 547, 398581, 23535794707, 82064241848634269407, ...}. - _Alexander Adamchuk_, Nov 19 2006",
				"General form: k=3^n-k. Also: A001045, A078008, A097073, A115341. - _Vladimir Joseph Stephan Orlovsky_, Dec 11 2008",
				"Let A be the Hessenberg matrix of order n, defined by: A[1,j]=1, A[i,i]:=-2, A[i,i-1]=-1, and A[i,j]=0 otherwise. Then, for n\u003e=1, a(n)=charpoly(A,1). - _Milan Janjic_, Jan 26 2010",
				"Select an odd size subset S from {1,2,...,n}, then select an even size subset from S. - _Geoffrey Critzer_, Mar 02 2010",
				"a(n) is the number of ternary sequences of length n where the numbers of (0's, 1's) are (even, odd) respectively, and, by symmetry, the number of such sequences where those numbers are (odd, even) respectively. A122983 covers (even, even), and A081251 covers (odd, odd). - _Toby Gottfried_, Apr 18 2010",
				"An elephant sequence, see A175654. For the corner squares just one A[5] vector, with decimal value 341, leads to this sequence (without the leading 0). For the central square this vector leads to the companion sequence A046717 (without the first leading 1). - _Johannes W. Meijer_, Aug 15 2010",
				"Let R be the commutative algebra resulting from adjoining the elements of the Klein four-group to the integers (equivalently, K = Z[x,y,z]/{x*y - z, y*z - x, x*z - y, x^2 - 1, y^2 - 1, z^2 - 1}). Then a(n) is equal to the coefficients of x, y, and z in the expansion of (x + y + z)^n. - Joseph E. Cooper III (easonrevant(AT)gmail.com), Nov 06 2010",
				"Pisano period lengths:  1, 2, 2, 4, 4, 2, 6, 8, 2, 4, 10, 4, 6, 6, 4, 16, 16, 2, 18, 4, ... - _R. J. Mathar_, Aug 10 2012",
				"The ratio a(n+1)/a(n) converges to 3 as n approaches infinity. - _Felix P. Muga II_, Mar 09 2014",
				"This is a divisibility sequence, also the values of Chebyshev polynomials, and also the number of ways of packing a 2 X n-1 rectangle with dominoes and unit squares. - _R. K. Guy_, Dec 16 2016",
				"For n\u003e0, gcd(a(n),a(n+1))=1. - _Kengbo Lu_, Jul 02 2020"
			],
			"reference": [
				"John Derbyshire, Prime Obsession, Joseph Henry Press, April 2004, see p. 16."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A015518/b015518.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"A. Abdurrahman, \u003ca href=\"https://arxiv.org/abs/1909.10889\"\u003eCM Method and Expansion of Numbers\u003c/a\u003e, arXiv:1909.10889 [math.NT], 2019.",
				"Jean-Paul Allouche, Jeffrey Shallit, Zhixiong Wen, Wen Wu, Jiemeng Zhang, \u003ca href=\"https://arxiv.org/abs/1911.01687\"\u003eSum-free sets generated by the period-k-folding sequences and some Sturmian sequences\u003c/a\u003e, arXiv:1911.01687 [math.CO], 2019.",
				"K. Böhmová, C. Dalfó, C. Huemer, \u003ca href=\"http://hdl.handle.net/2117/80848\"\u003eOn cyclic Kautz digraphs\u003c/a\u003e, Preprint 2016.",
				"G. Bowlin and M. G. Brin, \u003ca href=\"http://arxiv.org/abs/1301.3984\"\u003eColoring Planar Graphs via Colored Paths in the Associahedra\u003c/a\u003e, arXiv preprint arXiv:1301.3984 [math.CO], 2013. - From _N. J. A. Sloane_, Feb 12 2013",
				"Ji Young Choi, \u003ca href=\"https://www.emis.de/journals/JIS/VOL21/Choi/choi10.html\"\u003eA Generalization of Collatz Functions and Jacobsthal Numbers\u003c/a\u003e, J. Int. Seq., Vol. 21 (2018), Article 18.5.4.",
				"Sergio Falcón, \u003ca href=\"https://www.rgnpublications.com/journals/index.php/cma/article/viewFile/1221/950\"\u003eBinomial Transform of the Generalized k-Fibonacci Numbers\u003c/a\u003e, Communications in Mathematics and Applications (2019) Vol. 10, No. 3, 643-651.",
				"Dale Gerdemann, \u003ca href=\"https://www.youtube.com/watch?v=i6gDh_ZB3vo\"\u003eFractal generated from (2,3) recursion\u003c/a\u003e, YouTube Video, Dec 05 2014.",
				"R. J. Mathar, \u003ca href=\"/A102518/a102518.pdf\"\u003eCounting Walks on Finite Graphs\u003c/a\u003e, Nov 2020, Section 2.",
				"F. P. Muga II, \u003ca href=\"https://www.researchgate.net/publication/267327689_Extending_the_Golden_Ratio_and_the_Binet-de_Moivre_Formula\"\u003eExtending the Golden Ratio and the Binet-de Moivre Formula\u003c/a\u003e, March 2014.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,3).",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e"
			],
			"formula": [
				"G.f.: x/((1+x)*(1-3*x)).",
				"a(n) = (3^n - (-1)^n)/4 = floor(3^n/4 + 1/2).",
				"a(n) = 3^(n-1) - a(n-1). - _Emeric Deutsch_, Apr 01 2004",
				"E.g.f.: (exp(3*x) - exp(-x))/4. Second inverse binomial transform of (5^n-1)/4, A003463. Inverse binomial transform for powers of 4, A000302 (when preceded by 0). - _Paul Barry_, Mar 28 2003",
				"a(n) = Sum_{k=0..floor(n/2)} C(n, 2k+1)*2^(2k). - _Paul Barry_, May 14 2003",
				"a(n) = Sum_{k=1..n} binomial(n, k)*(-1)^(n+k)*4^(k-1). - _Paul Barry_, Apr 02 2003",
				"a(n+1) = Sum_{k=0..floor(n/2)} binomial(n-k, k)*2^(n-2*k)*3^k. - _Paul Barry_, Jul 13 2004",
				"a(n) = U(n-1, i/sqrt(3))(-i*sqrt(3))^(n-1), i^2=-1. - _Paul Barry_, Nov 17 2003",
				"G.f.: x*(1+x)^2/(1 - 6*x^2 - 8*x^3 - 3*x^4) = x(1+x)^2/characteristic polynomial(x^4*adj(K_4)(1/x)). - _Paul Barry_, Feb 03 2004",
				"a(n) = sum_{k=0..3^(n-1)} A014578(k) = -(-1)^n*A014983(n) = A051068(3^(n-1)), for n \u003e 0. - _Philippe Deléham_, Mar 31 2004",
				"E.g.f.: exp(x)*sinh(2*x)/2. - _Paul Barry_, Oct 02 2004",
				"a(2*n+1) = A054880(n) + 1. - _M. F. Hasler_, Mar 20 2008",
				"2*a(n) + (-1)^n = A046717(n). - _M. F. Hasler_, Mar 20 2008",
				"((1+sqrt(4))^n - (1-sqrt(4))^n)/4 = (3^n - (-1)^n)/4. Offset =1. a(3)=7. - Al Hakanson (hawkuu(AT)gmail.com), Dec 31 2008",
				"a(n) = abs(A014983(n)). - _Zerinvary Lajos_, May 28 2009",
				"a(n) = round(3^n/4). - _Mircea Merca_, Dec 28 2010",
				"a(n) = Sum_{k=1,3,5,...} binomial(n,k)*2^(k-1). - _Geoffrey Critzer_, Mar 02 2010",
				"Starting with \"1\" = triangle A059260 * the powers of 2: [1, 2, 4, 8, ...] as a vector. - _Gary W. Adamson_, Mar 06 2012",
				"From _Sergei N. Gladkovskii_, Jul 19 2012: (Start)",
				"G.f.: G(0)/4 where G(k)= 1 - 1/(9^k - 3*x*81^k/(3*x*9^k - 1/(1 + 1/(3*9^k - 27*x*81^k/(9*x*9^k + 1/G(k+1)))))); (continued fraction, 3rd kind, 6-step).",
				"E.g.f.: G(0)/4 where G(k)= 1 - 1/(9^k - 3*x*81^k/(3*x*9^k - (2*k+1)/(1 + 1/(3*9^k - 27*x*81^k/(9*x*9^k + (2*k+2)/G(k+1)))))); (continued fraction, 3rd kind, 6-step).  (End)",
				"G.f.: G(0)*x/(2*(1-x)), where G(k) = 1 + 1/(1 - x*(4*k-1)/(x*(4*k+3) - 1/G(k+1))); (continued fraction). - _Sergei N. Gladkovskii_, May 26 2013",
				"a(n+1) = Sum_{k = 0..n} A238801(n,k)*2^k. - _Philippe Deléham_, Mar 07 2014",
				"a(n) = (-1)^(n-1)*Sum_{k=0..n-1} A135278(n-1,k)*(-4)^k = (3^n - (-1)^n)/4 = (-1)^(n-1)*Sum_{k=0..n-1} (-3)^k. Equals (-1)^(n-1)*Phi(n,-3), where Phi is the cyclotomic polynomial when n is an odd prime. (For n \u003e 0.) - _Tom Copeland_, Apr 14 2014",
				"a(n) = 2*A006342(n-1) - n mod 2 if n \u003e 0, a(0)=0. - _Yuchun Ji_, Nov 30 2018",
				"a(n) = 2*A033113(n-2) + n mod 2 if n \u003e 0, a(0)=0. - _Yuchun Ji_, Aug 16 2019",
				"a(2*k) = 2*A002452(k), a(2*k+1) = A066443(k). - _Yuchun Ji_, Aug 14 2019",
				"a(n+1) = 2*Sum_{k=0..n} a(k) if n odd, and 1 + 2*Sum_{k=0..n} a(k) if n even. - _Kengbo Lu_, May 30 2020",
				"a(n) = F(n) + Sum_{k=1..(n-1)} a(k)*L(n-k), for F(n) and L(n) the Fibonacci and Lucas numbers. - _Kengbo Lu_ and _Greg Dresden_, Jun 05 2020",
				"From _Kengbo Lu_, Jun 11 2020: (Start)",
				"a(n) = A002605(n) + Sum_{k = 1..n-2} a(k)*A002605(n-k-1).",
				"a(n) = A006130(n-1) + Sum_{k = 1..n-1} a(k)*A006130(n-k-1). (End)",
				"a(2n) = Sum_{i\u003e=0, j\u003e=0} binomial(n-j-1,i)*binomial(n-i-1,j)*2^(2n-2i-2j-1)*3^(i+j). - _Kengbo Lu_, Jul 02 2020"
			],
			"mathematica": [
				"Table[(3^n-(-1)^n)/4,{n,0,30}] (* _Alexander Adamchuk_, Nov 19 2006 *)"
			],
			"program": [
				"(PARI) a(n)=round(3^n/4)",
				"(Sage) [round(3^n/4) for n in range(0,27)]",
				"(MAGMA) [Round(3^n/4): n in [0..30]]; // _Vincenzo Librandi_, Jun 24 2011",
				"(Python) for n in range(0, 20): print(int((3**n-(-1)**n)/4), end=', ') # _Stefano Spezia_, Nov 30 2018"
			],
			"xref": [
				"a(n) = A080926(n-1) + 1 = (1/3)*A054878(n+1) = (1/3)*abs(A084567(n+1)).",
				"First differences of A033113 and A039300.",
				"Partial sums of A046717.",
				"The following sequences (and others) belong to the same family: A000129, A001333, A002532, A002533, A002605, A015518, A015519, A026150, A046717, A063727, A083098, A083099, A083100, A084057.",
				"Cf. A046717.",
				"Cf. A007658, A111010.",
				"Cf. A001045, A078008, A097073, A115341. - _Vladimir Joseph Stephan Orlovsky_, Dec 11 2008",
				"Cf. A059260."
			],
			"keyword": "nonn,walk,easy",
			"offset": "0,3",
			"author": "_Olivier Gérard_",
			"ext": [
				"More terms from _Emeric Deutsch_, Apr 01 2004",
				"Edited by _Ralf Stephan_, Aug 30 2004"
			],
			"references": 89,
			"revision": 221,
			"time": "2020-11-05T04:57:06-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}