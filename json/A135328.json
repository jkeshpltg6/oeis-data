{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A135328",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 135328,
			"data": "1,1,2,4,1,10,4,29,12,1,90,36,6,290,114,24,1,960,376,86,8,3246,1272,303,40,1,11164,4380,1074,168,10,38934,15293,3838,660,60,1,137358,54012,13812,2528,290,12,489341,192612,50013,9584,1265,84,1",
			"name": "Triangle read by rows: T(n,k) is the number of Dyck paths of semilength n having k UDDU's starting at level 1.",
			"comment": [
				"Each of rows 0, 1, 2 has one term; row n (n \u003e= 1) has ceiling(n/2) terms. Row sums are the Catalan numbers (A000108). Column 0 yields A135334. - _Emeric Deutsch_, Dec 14 2007"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A135328/b135328.txt\"\u003eRows n = 0..200, flattened\u003c/a\u003e",
				"A. Sapounakis, I. Tasoulas and P. Tsikouras, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2007.03.005\"\u003eCounting strings in Dyck paths\u003c/a\u003e, Discrete Math., 307 (2007), 2909-2924."
			],
			"formula": [
				"From _Emeric Deutsch_, Dec 14 2007: (Start)",
				"T(n,k) = 2*((k+1)/(n+1))*Sum_{j=k..floor((n-1)/2)} (-1)^(j-k)*binomial(j+1, k+1)*binomial(2n-2j-1, n) (n \u003e= 1).",
				"G.f.: 1 + z*C^2/(1 + (1-t)*z^2*C^2), where C = (1-sqrt(1-4z))/(2z) is the g.f. of the Catalan numbers (A000108). (End)"
			],
			"example": [
				"Triangle begins:",
				"     1;",
				"     1;",
				"     2;",
				"     4    1;",
				"    10    4;",
				"    29   12   1;",
				"    90   36   6;",
				"   290  114  24  1;",
				"   960  376  86  8;",
				"  3246 1272 303 40 1;",
				"  ...",
				"T(4,1)=4 because we have UDU(UDDU)D, U(UDDU)DUD, U(UDDU)UDD and UUD(UDDU)D (the UDDU's starting at level 1 are shown between parentheses)."
			],
			"maple": [
				"T:=proc(n,k) options operator, arrow: (2*k+2)*(sum((-1)^(j-k)*binomial(j+1, k+1)*binomial(2*n-2*j-1,n),j=k..floor((1/2)*n-1/2)))/(n+1) end proc: 1; for n to 13 do seq(T(n,k),k=0..ceil((n-2)*1/2)) end do; # yields sequence in triangular form; _Emeric Deutsch_, Dec 14 2007",
				"G:=1+z*C^2/(1+(1-t)*z^2*C^2): C:=((1-sqrt(1-4*z))*1/2)/z: Gser:=simplify(series(G,z=0,16)): for n from 0 to 13 do P[n]:=sort(coeff(Gser,z,n)) end do: 1; for n to 13 do seq(coeff(P[n],t,j),j=0..floor((n-1)*1/2)) end do; # yields sequence in triangular form; _Emeric Deutsch_, Dec 14 2007",
				"# third Maple program:",
				"b:= proc(x, y, t) option remember; `if`(y\u003c0 or y\u003ex, 0,",
				"      `if`(x=0, 1, expand(b(x-1, y+1, `if`(y=1, 1, 0))*",
				"      `if`(t=3, z, 1))+b(x-1, y-1, `if`(t in [1, 2], t+1, 0))))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, z, i), i=0..degree(p)))(b(2*n, 0$2)):",
				"seq(T(n), n=0..14);  # _Alois P. Heinz_, Nov 16 2019"
			],
			"mathematica": [
				"b[x_, y_, t_] := b[x, y, t] = If[y \u003c 0 || y \u003e x, 0,",
				"     If[x == 0, 1, Expand[b[x - 1, y + 1, If[y == 1, 1, 0]]*",
				"     If[t == 3, z, 1]] + b[x - 1, y - 1, If[1 \u003c= t \u003c= 2, t + 1, 0]]]];",
				"T[n_] := CoefficientList[b[2n, 0, 0], z];",
				"T /@ Range[0, 14] // Flatten (* _Jean-François Alcover_, Feb 14 2021, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A000108, A135334."
			],
			"keyword": "nonn,tabf",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_, Dec 07 2007",
			"ext": [
				"Edited and extended by _Emeric Deutsch_, Dec 14 2007"
			],
			"references": 2,
			"revision": 17,
			"time": "2021-02-14T13:03:32-05:00",
			"created": "2007-12-09T03:00:00-05:00"
		}
	]
}