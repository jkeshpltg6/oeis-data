{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A208328",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 208328,
			"data": "1,1,1,1,1,3,1,1,5,5,1,1,7,9,11,1,1,9,13,25,21,1,1,11,17,43,53,43,1,1,13,21,65,97,125,85,1,1,15,25,91,153,255,273,171,1,1,17,29,121,221,441,597,609,341,1,1,19,33,155,301,691,1089,1443,1325,683,1,1,21",
			"name": "Triangle of coefficients of polynomials u(n,x) jointly generated with A208329; see the Formula section.",
			"comment": [
				"Row sums, u(n,1):  A000129",
				"Row sums, v(n,1):  A001333",
				"Subtriangle of the triangle T(n,k) given by (1, 0, -1, 1, 0, 0, 0, 0, 0, 0, 0, ...) DELTA (0, 1, 2, -2, 0, 0, 0, 0, 0, 0, 0, ...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, Mar 07 2012"
			],
			"formula": [
				"u(n,x) = u(n-1,x) + x*v(n-1,x),",
				"v(n,x) = 2x*u(n-1,x) + x*v(n-1,x),",
				"where u(1,x)=1, v(1,x)=1.",
				"From _Philippe Deléham_, Mar 07 2012: (Start)",
				"As DELTA-triangle T(n,k), 0 \u003c= k \u003c= n:",
				"G.f.: (1-y*x - y*(2*y-1)*x^2)/(1-(1+y)*x-y(2*y-1)*x^2).",
				"T(n,k) = T(n-1,k) + T(n-1,k-1) - T(n-2,k-1) + 2*T(n-2,k-2), T(0,0) = 1, T(1,0) = 1, T(1,1) = 0, T(n,k) = 0 if k \u003c 0 or if k \u003e n.",
				"Sum_{k=0..n, n\u003e0} T(n,k)*x^k = A000012(n), A000129(n), A083858(n) for x = 0, 1, 2 respectively. (End)"
			],
			"example": [
				"First five rows:",
				"  1;",
				"  1, 1;",
				"  1, 1, 3;",
				"  1, 1, 5, 5;",
				"  1, 1, 7, 9, 11;",
				"First five polynomials u(n,x):",
				"  1",
				"  1 + x",
				"  1 + x + 3x^2",
				"  1 + x + 5x^2 + 5x^3",
				"  1 + x + 7x^2 + 9x^3 + 11x^4.",
				"From _Philippe Deléham_, Mar 07 2012: (Start)",
				"(1, 0, -1, 1, 0, 0, 0, ...) DELTA (0, 1, 2, -2, 0, 0, 0, ...) begins:",
				"  1;",
				"  1, 0;",
				"  1, 1,  0;",
				"  1, 1,  3,  0;",
				"  1, 1,  5,  5,  0;",
				"  1, 1,  7,  9, 11,  0;",
				"  1, 1,  9, 13, 25, 21,  0;",
				"  1, 1, 11, 17, 43, 53, 43, 0; (End)"
			],
			"mathematica": [
				"u[1, x_] := 1; v[1, x_] := 1; z = 13;",
				"u[n_, x_] := u[n - 1, x] + x*v[n - 1, x];",
				"v[n_, x_] := 2 x*u[n - 1, x] + x*v[n - 1, x];",
				"Table[Expand[u[n, x]], {n, 1, z/2}]",
				"Table[Expand[v[n, x]], {n, 1, z/2}]",
				"cu = Table[CoefficientList[u[n, x], x], {n, 1, z}];",
				"TableForm[cu]",
				"Flatten[%]  (* A208328 *)",
				"Table[Expand[v[n, x]], {n, 1, z}]",
				"cv = Table[CoefficientList[v[n, x], x], {n, 1, z}];",
				"TableForm[cv]",
				"Flatten[%]  (* A208329 *)"
			],
			"xref": [
				"Cf. A208329."
			],
			"keyword": "nonn,tabl",
			"offset": "1,6",
			"author": "_Clark Kimberling_, Feb 26 2012",
			"references": 3,
			"revision": 17,
			"time": "2020-01-22T20:12:51-05:00",
			"created": "2012-02-26T17:44:40-05:00"
		}
	]
}