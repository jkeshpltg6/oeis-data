{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A210595",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 210595,
			"data": "1,2,1,3,3,2,4,6,7,3,5,10,16,13,5,6,15,30,35,25,8,7,21,50,75,76,46,13,8,28,77,140,181,157,84,21,9,36,112,238,371,413,317,151,34,10,45,156,378,686,924,911,625,269,55,11,55,210,570,1176,1848,2206,1949,1211,475,89",
			"name": "Triangle of coefficients of polynomials v(n,x) jointly generated with A209999; see the Formula section.",
			"comment": [
				"Row n starts with n and ends with F(n), where F=A000045 (Fibonacci numbers).",
				"Row sums: A048739.",
				"Alternating row sums: 1,1,2,2,3,3,4,4,5,5, ...",
				"For a discussion and guide to related arrays, see A208510."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A210595/b210595.txt\"\u003eRows n = 1..30 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"u(n,x) = x*u(n-1,x) + (x+1)*v(n-1,x) + 1,",
				"v(n,x) = x*u(n-1,x) + v(n-1,x) + 1,",
				"where u(1,x) = 1, v(1,x) = 1.",
				"T(n, k) = [x^k]( v(n,x) ), where v(n, x) = (1+x)*v(n-1, x) + x^2*v(n-2, x) + 1, v(1, x) = 1, and v(2, x) = 2 + x. - _G. C. Greubel_, May 24 2021"
			],
			"example": [
				"First few rows are:",
				"  1;",
				"  2,  1;",
				"  3,  3,  2;",
				"  4,  6,  7,  3;",
				"  5, 10, 16, 13,  5;",
				"  6, 15, 30, 35, 25,  8;",
				"  7, 21, 50, 75, 76, 46, 13;",
				"First few polynomials v(n,x) are:",
				"  v(1, x) = 1;",
				"  v(2, x) = 2 +  1*x;",
				"  v(3, x) = 3 +  3*x +  2*x^2;",
				"  v(4, x) = 4 +  6*x +  7*x^2 +  3*x^3;",
				"  v(5, x) = 5 + 10*x + 16*x^2 + 13*x^3 + 5*x^4;"
			],
			"mathematica": [
				"(* First program *)",
				"u[1, x_]:= 1; v[1, x_]:= 1; z = 16;",
				"u[n_, x_]:= x*u[n-1, x] + (1+x)*v[n-1, x] + 1;",
				"v[n_, x_]:= x*u[n-1, x] + v[n-1, x] + 1;",
				"Table[Expand[u[n, x]], {n, 1, z/2}]",
				"Table[Expand[v[n, x]], {n, 1, z/2}]",
				"cu = Table[CoefficientList[u[n, x], x], {n, 1, z}];",
				"TableForm[cu]",
				"Flatten[%]    (* A210565 *)",
				"Table[Expand[v[n, x]], {n, 1, z}]",
				"cv = Table[CoefficientList[v[n, x], x], {n, 1, z}];",
				"TableForm[cv]",
				"Flatten[%]    (* A210595 *)",
				"(* Second program *)",
				"v[n_, x_]:= v[n, x]= If[n\u003c2, n+1 +n*x, (1+x)*v[n-1, x] +x^2*v[n-2, x] +1];",
				"T[n_]:= CoefficientList[Series[v[n, x], {x,0,n}], x];",
				"Table[T[n-1], {n, 12}]//Flatten (* _G. C. Greubel_, May 24 2021 *)"
			],
			"program": [
				"(Sage)",
				"@CachedFunction",
				"def v(n,x): return n+1+n*x if (n\u003c2) else (1+x)*v(n-1,x) +x^2*v(n-2,x) +1",
				"def T(n): return taylor( v(n,x) , x,0,n).coefficients(x, sparse=False)",
				"flatten([T(n-1) for n in (1..12)]) # _G. C. Greubel_, May 24 2021"
			],
			"xref": [
				"Cf. A208510, A210565."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Mar 23 2012",
			"references": 3,
			"revision": 10,
			"time": "2021-05-25T01:37:56-04:00",
			"created": "2012-03-24T18:19:18-04:00"
		}
	]
}