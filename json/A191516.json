{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A191516",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 191516,
			"data": "2,2,2,1,2,1,0,3,0,3,2,2,2,2,2,2,1,2,1,1,2,1,1,2,1,2,3,0,0,4,1,2,1,2,1,2,0,0,4,1,3,1,1,3,1,2,3,2,1,2,1,0,3,1,2,4,2,1,2,3,0,3,0,4,0,1,1,3,1,2,2,2,2,3,0,0,0,5,2,4,1,3,1,1,4,1,2,0,2,2,1,0,3,1,1,0,3,1,2,2,2,1,1,3,1,2,1,2,1,3,1,1",
			"name": "Irregular triangle read by rows: a(n,k) is the number of edges with degree k (k\u003e=1) in the rooted tree with Matula-Goebel number n (n\u003e=3).",
			"comment": [
				"The degree of an edge is the number of edges adjacent to it.",
				"The Matula-Goebel number of a rooted tree can be defined in the following recursive manner: to the one-vertex tree there corresponds the number 1; to a tree T with root degree 1 there corresponds the t-th prime number, where t is the Matula-Goebel number of the tree obtained from T by deleting the edge emanating from the root; to a tree T with root degree m\u003e=2 there corresponds the product of the Matula-Goebel numbers of the m branches of T.",
				"Sum of entries in row n = A196050(n)  (= number of edges).",
				"Sum(k*a(n,k), k\u003e=1) = A198332(n)  (=sum of edge degrees (the Platt index))."
			],
			"reference": [
				"A. T. Balaban, Chemical graphs, Theoret. Chim. Acta (Berl.) 53, 355-375, 1979.",
				"F. Goebel, On a 1-1-correspondence between rooted trees and natural numbers, J. Combin. Theory, B 29 (1980), 141-143.",
				"I. Gutman and A. Ivic, On Matula numbers, Discrete Math., 150, 1996, 131-142.",
				"I. Gutman and Yeong-Nan Yeh, Deducing properties of trees from their Matula numbers, Publ. Inst. Math., 53 (67), 1993, 17-22.",
				"D. W. Matula, A natural rooted tree enumeration by prime factorization, SIAM Review, 10, 1968, 273.",
				"R. Todeschini and V. Consonni, Handbook of Molecular Descriptors, Wiley-VCH, 2000."
			],
			"link": [
				"E. Deutsch, \u003ca href=\"http://arxiv.org/abs/1111.4288\"\u003eRooted tree statistics from Matula numbers\u003c/a\u003e, arXiv:1111.4288",
				"\u003ca href=\"/index/Mat#matula\"\u003eIndex entries for sequences related to Matula-Goebel numbers\u003c/a\u003e"
			],
			"formula": [
				"Let f(n)=F(n,x) be the generating polynomial of the edges of the rooted tree with Matula-Goebel number n, with respect to edge-degree. Write f(n)=g(n)+h(n), where g(n) is over the edges emanating from the root and h(n) is over the remaining edges. We have g(1)=0, h(1)=0; if n = p(t) (=the t-th prime), then g(n)=x^G(t), h(n)=xg(t)+h(t); if n=rs (r,s\u003e=2), then g(n)=x^G(s)*g(r) + x^G(r)*g(s), h(n)=h(r)+h(s). G(m) denotes the number of prime divisors of m counted with multiplicities."
			],
			"example": [
				"Row 5 is 2,1 because the rooted tree with Matula-Goebel number 5 is the path tree ABCD on 4 vertices; AB and CD have degree 1 and BC has degree 2.",
				"Row 7 is 0,3 because the rooted tree with Matula-Goebel number 7 is Y, where no edge has degree 1 and all 3 edges have degree 2.",
				"Triangle starts:",
				"2;",
				"2;",
				"2,1;",
				"2,1;",
				"0,3;",
				"0,3;",
				"2,2;"
			],
			"maple": [
				"with(numtheory): f := proc (n) local r, s,g,h: r := proc (n) options operator, arrow: op(1, factorset(n)) end proc: s := proc (n) options operator, arrow: n/r(n) end proc: g:=proc(n) if n = 1 then 0 elif bigomega(n) = 1 then x^(bigomega(pi(n))) else x^(bigomega(s(n)))*g(r(n))+x^(bigomega(r(n)))*g(s(n)) fi end: h:=proc(n) if n=1 then 0 elif bigomega(n)=1 then x*g(pi(n))+h(pi(n)) else h(r(n))+h(s(n)) fi end: sort(expand(g(n)+h(n))) end: for n from 3 to 42 do seq(coeff(f(n),x,j),j=1..degree(f(n))) od; # yields sequence in triangular form"
			],
			"xref": [
				"Cf. A196050, A198332."
			],
			"keyword": "nonn,tabf",
			"offset": "3,1",
			"author": "_Emeric Deutsch_, Dec 15 2011",
			"references": 1,
			"revision": 14,
			"time": "2021-10-23T21:18:29-04:00",
			"created": "2011-12-15T20:27:59-05:00"
		}
	]
}