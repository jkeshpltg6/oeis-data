{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A309119",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 309119,
			"data": "0,1,0,1,3,3,2,2,0,1,3,3,5,8,9,9,10,9,8,8,6,6,7,6,4,3,0,1,3,3,5,8,9,9,10,9,11,14,15,18,22,24,25,27,27,27,28,27,28,30,30,29,29,27,26,26,24,24,25,24,22,21,18,18,19,18,19,21,21,20,20,18,16,15",
			"name": "a(n) is the number of 1's minus the number of 2's among the ternary representations of the integers in the interval [0..n].",
			"comment": [
				"This sequence has connections with a Takagi (or blancmange) curve.",
				"Let t be the real function defined over [0..1] as follows:",
				"- t(x) = 0 for x in [0..1/3],",
				"- t(x) = x - 1/3 for x in ]1/3..2/3],",
				"- t(x) = 1 - x for x in ]2/3..1].",
				"Let g be the real function defined over [0..1] as follows:",
				"- g(x) = Sum_{k \u003e= 0} t(x * 3^k)/3^k.",
				"The representation of n -\u003e (n/3^k, a(n)/3^k) for n = 0..3^k converges to the representation of g over [0..1] as k tends to infinity."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A309119/b309119.txt\"\u003eTable of n, a(n) for n = 0..6560\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A309119/a309119.png\"\u003eColored pinplot of the sequence for n = 0..3^7-1\u003c/a\u003e (where the color denotes the contribution of the digits according to their position in the ternary expansion)",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Blancmange_curve\"\u003eBlancmange curve\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Sum_{k = 0..n} (A062756(k) - A081603(k)).",
				"a(n) \u003e= 0 with equality iff n = 3^k - 1 for some k \u003e= 0 (A024023).",
				"a(3*k + 2) = 3*a(k) for any k \u003e= 0.",
				"a(3^k + m) = a(m) + m + 1 for any k \u003e= 0 and m = 0..3^k-1.",
				"a(2*3^k + m) = a(m) + 3^k - m - 1 for any k \u003e= 0 and m = 0..3^k-1."
			],
			"example": [
				"The first terms, alongside the ternary expansion of n and the corresponding number of 1's and 2's, are:",
				"  n   a(n)  ter(n)  A062756(n)  A081603(n)",
				"  --  ----  ------  ----------  ----------",
				"   0     0       0           0           0",
				"   1     1       1           1           0",
				"   2     0       2           0           1",
				"   3     1      10           1           0",
				"   4     3      11           2           0",
				"   5     3      12           1           1",
				"   6     2      20           0           1",
				"   7     2      21           1           1",
				"   8     0      22           0           2",
				"   9     1     100           1           0",
				"  10     3     101           2           0"
			],
			"mathematica": [
				"Accumulate[Table[Total[IntegerDigits[n,3]/.(2-\u003e-1)],{n,0,80}]] (* _Harvey P. Dale_, Jun 23 2020 *)"
			],
			"program": [
				"(PARI) s = 0; for (n=0, 73, t = digits(n,3); print1 (s+=sum(i=1, #t, if (t[i]==1, +1, t[i]==2, -1, 0)) \", \"))"
			],
			"xref": [
				"Cf. A024023, A062756, A081603."
			],
			"keyword": "nonn,look,base",
			"offset": "0,5",
			"author": "_Rémy Sigrist_, Jul 13 2019",
			"references": 1,
			"revision": 17,
			"time": "2020-06-23T13:08:50-04:00",
			"created": "2019-07-14T06:26:49-04:00"
		}
	]
}