{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001285",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1285,
			"id": "M0193 N0071",
			"data": "1,2,2,1,2,1,1,2,2,1,1,2,1,2,2,1,2,1,1,2,1,2,2,1,1,2,2,1,2,1,1,2,2,1,1,2,1,2,2,1,1,2,2,1,2,1,1,2,1,2,2,1,2,1,1,2,2,1,1,2,1,2,2,1,2,1,1,2,1,2,2,1,1,2,2,1,2,1,1,2,1,2,2,1,2,1,1,2,2,1,1,2,1,2,2,1,1,2,2,1,2,1",
			"name": "Thue-Morse sequence: let A_k denote the first 2^k terms; then A_0 = 1 and for k \u003e= 0, A_{k+1} = A_k B_k, where B_k is obtained from A_k by interchanging 1's and 2's.",
			"comment": [
				"Or, follow a(0), ..., a(2^k-1) by its complement.",
				"Equals limiting row of A161175. - _Gary W. Adamson_, Jun 05 2009",
				"Parse A010060 into consecutive pairs: (01, 10, 10, 01, 10, 01, ...); then apply the rules: (01 -\u003e 1; 10 -\u003e2), obtaining (1, 2, 2, 1, 2, 1, 1, ...). - _Gary W. Adamson_, Oct 25 2010"
			],
			"reference": [
				"J.-P. Allouche and J. Shallit, Automatic Sequences, Cambridge Univ. Press, 2003, p. 15.",
				"G. Everest, A. van der Poorten, I. Shparlinski and T. Ward, Recurrence Sequences, Amer. Math. Soc., 2003; see esp. p. 255.",
				"W. H. Gottschalk and G. A. Hedlund, Topological Dynamics. American Mathematical Society, Colloquium Publications, Vol. 36, Providence, RI, 1955, p. 105.",
				"M. Lothaire, Combinatorics on Words. Addison-Wesley, Reading, MA, 1983, p. 23.",
				"A. Salomaa, Jewels of Formal Language Theory. Computer Science Press, Rockville, MD, 1981, p. 6.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A001285/b001285.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e (terms 0..1023 from T. D. Noe)",
				"J.-P. Allouche and Jeffrey Shallit, \u003ca href=\"http://www.cs.uwaterloo.ca/~shallit/Papers/ubiq.ps\"\u003eThe Ubiquitous Prouhet-Thue-Morse Sequence\u003c/a\u003e, in C. Ding. T. Helleseth and H. Niederreiter, eds., Sequences and Their Applications: Proceedings of SETA '98, Springer-Verlag, 1999, pp. 1-16.",
				"F. Axel et al., \u003ca href=\"https://hal.archives-ouvertes.fr/jpa-00225729\"\u003eVibrational modes in a one dimensional \"quasi-alloy\": the Morse case\u003c/a\u003e, J. de Physique, Colloq. C3, Supp. to No. 7, Vol. 47 (Jul 1986), pp. C3-181-C3-186; see Eq. (10).",
				"Scott Balchin and Dan Rust, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL20/Rust/rust3.html\"\u003eComputations for Symbolic Substitutions\u003c/a\u003e, Journal of Integer Sequences, Vol. 20 (2017), Article 17.4.1.",
				"J. D. Currie, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Currie/currie7.html\"\u003eThe Least Self-Shuffle of the Thue-Morse Sequence\u003c/a\u003e, J. Int. Seq. 17 (2014) # 14.10.2.",
				"Francoise Dejean, \u003ca href=\"http://dx.doi.org/10.1016/0097-3165(72)90011-8\"\u003eSur un Theoreme de Thue\u003c/a\u003e, J. Combinatorial Theory, vol. 13 A, iss. 1 (1972) 90-99.",
				"F. Michel Dekking, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL19/Dekking/dekk4.html\"\u003eMorphisms, Symbolic Sequences, and Their Standard Forms\u003c/a\u003e, Journal of Integer Sequences, Vol. 19 (2016), Article 16.1.1.",
				"Arturas Dubickas, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2005.07.004\"\u003eOn the distance from a rational power to the nearest integer\u003c/a\u003e, Journal of Number Theory, Volume 117, Issue 1, March 2006, Pages 222-239.",
				"Arturas Dubickas, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2006.08.001\"\u003eOn a sequence related to that of Thue-Morse and its applications\u003c/a\u003e, Discrete Math. 307 (2007), no. 9-10, 1082--1093. MR2292537 (2008b:11086).",
				"Michael Gilleland, \u003ca href=\"/selfsimilar.html\"\u003eSome Self-Similar Integer Sequences\u003c/a\u003e",
				"G. A. Hedlund, \u003ca href=\"https://www.jstor.org/stable/24525014\"\u003eRemarks on the work of Axel Thue on sequences\u003c/a\u003e, Nordisk Mat. Tid., 15 (1967), 148-150.",
				"A. Hof, O. Knill and B. Simon, \u003ca href=\"http://inis.iaea.org/search/search.aspx?orig_q=RN:27016845\"\u003eSingular continuous spectrum for palindromic Schroedinger operators\u003c/a\u003e, Commun. Math. Phys. 174 (1995), 149-159.",
				"Tanya Khovanova, \u003ca href=\"http://arxiv.org/abs/1410.2193\"\u003eThere are no coincidences\u003c/a\u003e, arXiv preprint 1410.2193 [math.CO], 2014.",
				"M. Morse, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9947-1921-1501161-8\"\u003eRecurrent geodesics on a surface of negative curvature\u003c/a\u003e, Trans. Amer. Math. Soc., 22 (1921), 84-100.",
				"G. Siebert, \u003ca href=\"/A001285/a001285_1.pdf\"\u003eLetter to N. J. A. Sloane, Sept. 1977\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/A001285/a001285.txt\"\u003eThe first 1000 terms as a string\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/A001149/a001149.pdf\"\u003eHandwritten notes on Self-Generating Sequences, 1970\u003c/a\u003e (note that A1148 has now become A005282)",
				"N. J. A. Sloane, P. Flor, L. F. Meyers, G. A. Hedlund. M. Gardner, \u003ca href=\"/A001285/a001285.pdf\"\u003eCollection of documents and notes related to A1285, A3270, A3324\u003c/a\u003e",
				"S. Wolfram, \u003ca href=\"http://www.wolframscience.com/nksonline/page-889c-text\"\u003eSource for short Thue-Morse generating code\u003c/a\u003e",
				"\u003ca href=\"/index/Cor#core\"\u003eIndex entries for \"core\" sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Fi#FIXEDPOINTS\"\u003eIndex entries for sequences that are fixed points of mappings\u003c/a\u003e"
			],
			"formula": [
				"a(2n) = a(n), a(2n+1) = 3 - a(n), a(0) = 1. Also, a(k+2^m) = 3 - a(k) if 0 \u003c= k \u003c 2^m.",
				"a(n) = 1 + A010060(n).",
				"a(n) = 2 - A010059(n) = 1/2*(3 - (-1)^A000120(n)). - _Ralf Stephan_, Jun 20 2003",
				"a(n) = (Sum{k=0..n} binomial(n, k) mod 2) mod 3 = A001316(n) mod 3. - _Benoit Cloitre_, May 09 2004",
				"G.f.: (3/(1 - x) - Product_{k\u003e=0} (1 - x^(2^k)))/2. - _Ilya Gutkovskiy_, Apr 03 2019"
			],
			"maple": [
				"A001285 := proc(n) option remember; if n=0 then 1 elif n mod 2 = 0 then A001285(n/2) else 3-A001285((n-1)/2); fi; end;",
				"s := proc(k) local i, ans; ans := [ 1,2 ]; for i from 0 to k do ans := [ op(ans),op(map(n-\u003eif n=1 then 2 else 1 fi, ans)) ] od; RETURN(ans); end; t1 := s(6); A001285 := n-\u003et1[n]; # s(k) gives first 2^(k+2) terms"
			],
			"mathematica": [
				"Nest[ Flatten@ Join[#, # /. {1 -\u003e 2, 2 -\u003e 1}] \u0026, {1}, 7] (* _Robert G. Wilson v_, Feb 26 2005 *)",
				"a[n_] := Mod[Sum[Mod[Binomial[n, k], 2], {k, 0, n}], 3]; Table[a[n], {n, 0, 101}] (* _Jean-François Alcover_, Jul 02 2019 *)",
				"ThueMorse[Range[0,120]]+1 (* _Harvey P. Dale_, May 07 2021 *)"
			],
			"program": [
				"(PARI) a(n)=1+subst(Pol(binary(n)),x,1)%2",
				"(PARI) a(n)=sum(k=0,n,binomial(n,k)%2)%3",
				"(PARI) a(n)=hammingweight(n)%2+1 \\\\ _Charles R Greathouse IV_, Mar 26 2013",
				"(Haskell)",
				"a001285 n = a001285_list !! n",
				"a001285_list = map (+ 1) a010060_list",
				"-- _Reinhard Zumkeller_, Oct 03 2012"
			],
			"xref": [
				"Cf. A010060 for 0, 1 version, which is really the main entry for this sequence; also A003159. A225186 (squares).",
				"A026465 gives run lengths.",
				"Cf. A010059 (1, 0 version).",
				"Cf. A161175. - _Gary W. Adamson_, Jun 05 2009",
				"Cf. A026430 (partial sums).",
				"Boustrophedon transforms: A230958, A029885."
			],
			"keyword": "nonn,easy,core,nice",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 63,
			"revision": 123,
			"time": "2021-05-07T17:50:55-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}