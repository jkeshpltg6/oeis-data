{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A211956",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 211956,
			"data": "1,1,1,1,1,2,1,4,2,1,6,4,1,9,12,4,1,12,20,8,1,16,40,32,8,1,20,60,56,16,1,25,100,140,80,16,1,30,140,224,144,32,1,36,210,448,432,192,32,1,42,280,672,720,352,64,1,49,392,1176,1680,1232,448,64",
			"name": "Coefficients of a sequence of polynomials related to the Morgan-Voyce polynomials.",
			"comment": [
				"The row generating polynomials R(n,x) of A211955 factorize in the ring Z[x] as R(n,x) = P(n,x)*P(n+1,x) for n \u003e= 1: explicitly, P(2*n,x) = 1/2*(b(2*n,2*x) + 1)/b(n,2*x) and P(2*n+1,x) = b(n,2*x), where b(n,x) := sum {k = 0..n} binomial(n+k,2*k)*x^k are the Morgan-Voyce polynomials of A085478. This triangle lists the coefficients in ascending powers of x of the polynomials P(n,x).",
				"The odd numbered rows of the present triangle produce triangle A123519; the even numbered row entries are recorded separately in A211957 and appear to equal the unsigned and row reversed form of A204021. The even numbered rows with a factor of 2^(k-1) removed from the k-th column entries produce triangle A208513."
			],
			"link": [
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Morgan-VoycePolynomials.html\"\u003eMorgan-Voyce polynomials\u003c/a\u003e"
			],
			"formula": [
				"T(n,0) = 1; for k \u003e 0, T(2*n,k) = 2^k*binomial(n+k,2*k) = A123519(n,k); for k \u003e 0, T(2*n-1,k) = n/(n+k)*2^k*binomial(n+k,2*k) = 2^(k-1)*A208513(n,k).",
				"O.g.f.: ((1+t)*(1-t^2)-t^2*x)/((1-t^2)^2-2*t^2*x) = 1 + t + (1+x)*t^2 + (1+2*x)*t^3 + (1+4*x+2*x^2)*t^4 + ....",
				"Row generating polynomials: P(2*n,x) := 1/2*(b(2*n,2*x)+1)/b(n,2*x) and P(2*n+1,x) := b(n,2*x), where b(n,x) := sum {k = 0..n} binomial(n+k,2*k)*x^k are the Morgan-Voyce polynomials of A085478.",
				"The product P(n,x)*P(n+1,x) is the n-th row polynomial of A211955.",
				"In terms of T(n,x), the Chebyshev polynomials of the first kind, we have P(2*n,x) = T(2*n,u) and P(2*n+1,x) = 1/u*T(2*n+1,u), where u = sqrt((x+2)/2).",
				"Other representations for the row polynomials include",
				"P(2*n,x) = 1/2*(1+x+sqrt(x^2+2*x))^n + 1/2*(1+x-sqrt(x^2+2*x))^n;",
				"P(2*n,x) = n*sum {k = 0..n}(-1)^(n-k)/(n+k)*binomial(n+k,2*k) *(2*x+4)^k for n \u003e= 1; and P(2*n+1,x) = (2*n+1)*sum {k=0..n} (-1)^(n-k)/(n+k+1)* binomial(n+k+1,2*k+1)*(2*x+4)^k.",
				"Recurrence equation: P(n+1,x)*P(n-2,x) - P(n,x)*P(n-1,x) = x.",
				"Row sums A005246(n+2)."
			],
			"example": [
				"Triangle begins",
				".n\\k.|..0....1....2....3....4",
				"= = = = = = = = = = = = = = =",
				"..0..|..1",
				"..1..|..1",
				"..2..|..1....1",
				"..3..|..1....2",
				"..4..|..1....4....2",
				"..5..|..1....6....4",
				"..6..|..1....9...12....4",
				"..7..|..1...12...20....8",
				"..8..|..1...16...40...32....8",
				"..9..|..1...20...60...56...16",
				"..."
			],
			"xref": [
				"Cf. A005246 (row sums), A085478, A123519, A204021, A208513, A211955, A211957."
			],
			"keyword": "nonn,easy,tabf",
			"offset": "0,6",
			"author": "_Peter Bala_, Apr 30 2012",
			"references": 5,
			"revision": 7,
			"time": "2012-04-30T14:28:12-04:00",
			"created": "2012-04-30T14:28:12-04:00"
		}
	]
}