{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A317583",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 317583,
			"data": "1,4,8,30,32,342,128,3754,11360,56138,2048,3834670,8192,27528494,577439424,2681075210,131072,238060300946,524288,11045144602614,115488471132032,49840258213638,8388608,152185891301461434,140102945910265344,124260001149229146,85092642310351607968",
			"name": "Number of multiset partitions of normal multisets of size n such that all blocks have the same size.",
			"comment": [
				"A multiset is normal if it spans an initial interval of positive integers.",
				"a(n) is the number of nonnegative integer matrices with total sum n, nonzero rows and each column with the same sum with columns in nonincreasing lexicographic order. - _Andrew Howroyd_, Jan 15 2020"
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A317583/b317583.txt\"\u003eTable of n, a(n) for n = 1..200\u003c/a\u003e",
				"Gus Wiseman, \u003ca href=\"/A038041/a038041.txt\"\u003eSequences counting and ranking multiset partitions whose part lengths, sums, or averages are constant or strict.\u003c/a\u003e"
			],
			"formula": [
				"a(p) = 2^p for prime p. - _Andrew Howroyd_, Sep 15 2018",
				"a(n) = Sum_{d|n} A331315(n/d, d). - _Andrew Howroyd_, Jan 15 2020"
			],
			"example": [
				"The a(3) = 8 multiset partitions:",
				"  {{1,1,1}}",
				"  {{1,1,2}}",
				"  {{1,2,2}}",
				"  {{1,2,3}}",
				"  {{1},{1},{1}}",
				"  {{1},{1},{2}}",
				"  {{1},{2},{2}}",
				"  {{1},{2},{3}}"
			],
			"mathematica": [
				"sps[{}]:={{}};sps[set:{i_,___}]:=Join@@Function[s,Prepend[#,s]\u0026/@sps[Complement[set,s]]]/@Cases[Subsets[set],{i,___}];",
				"mps[set_]:=Union[Sort[Sort/@(#/.x_Integer:\u003eset[[x]])]\u0026/@sps[Range[Length[set]]]];",
				"allnorm[n_]:=Function[s,Array[Count[s,y_/;y\u003c=#]+1\u0026,n]]/@Subsets[Range[n-1]+1];",
				"Table[Length[Select[Join@@mps/@allnorm[n],SameQ@@Length/@#\u0026]],{n,8}]"
			],
			"program": [
				"(PARI) \\\\ here U(n,m) gives number for m blocks of size n.",
				"U(n,m)={sum(k=1, n*m, binomial(binomial(k+n-1, n)+m-1, m)*sum(r=k, n*m, binomial(r, k)*(-1)^(r-k)) )}",
				"a(n)={sumdiv(n, d, U(d, n/d))} \\\\ _Andrew Howroyd_, Sep 15 2018"
			],
			"xref": [
				"Cf. A038041, A255906, A298422, A306017, A306019, A306020, A306021, A320324, A322794, A326517, A326518, A326519, A326520, A326521, A331315."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Gus Wiseman_, Aug 01 2018",
			"ext": [
				"Terms a(9) and beyond from _Andrew Howroyd_, Sep 15 2018"
			],
			"references": 16,
			"revision": 17,
			"time": "2020-01-15T22:47:50-05:00",
			"created": "2018-08-01T09:22:47-04:00"
		}
	]
}