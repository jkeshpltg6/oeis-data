{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A306773",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 306773,
			"data": "1,1,3,1,3,16,1,3,24,6,1,3,13,6,100,1,3,8,252,120,24,1,3,8,42,119,96,576,1,3,60,588,378,36,624,1932,1,3,126,600,144,381,200,936,912,1,3,480,51,9,2760,6220,540,1800,5700,1,3,170,750,2480,14880,10990,300,1440,3660,840,1,3,13800,5880,432,48096,60528,456,17640,8496,10560",
			"name": "Triangle read by rows, 0 \u003c= k \u003c n, n \u003e= 2: T(n,k) is the eventual period of the modified Fibonacci sequence x(j) (or 0 if x(j) never enters a cycle) defined as follows: x(0) = 0, x(1) = 1, and for j \u003e 1 x(j) is obtained from x(j-1) + x(j-2) by deleting all occurrences of the digit k in base n.",
			"comment": [
				"For k = 1 x(j) = 0 for all j \u003e 1, so T(n,1) = 1 for n \u003e= 2.",
				"For k = 2 x(j) is the periodic sequence 0, 1, 1, 0, 1, 1, ... (A011655), so T(n,2) = 3 for n \u003e= 3."
			],
			"link": [
				"Pontus von Brömssen, \u003ca href=\"/A306773/b306773.txt\"\u003eRows n = 2..24, flattened\u003c/a\u003e"
			],
			"example": [
				"Triangle begins:",
				"   n\\k    0     1     2     3     4     5     6     7     8     9    10    11",
				"  ---------------------------------------------------------------------------",
				"   2:     1     1",
				"   3:     3     1     3",
				"   4:    16     1     3    24",
				"   5:     6     1     3    13     6",
				"   6:   100     1     3     8   252   120",
				"   7:    24     1     3     8    42   119    96",
				"   8:   576     1     3    60   588   378    36   624",
				"   9:  1932     1     3   126   600   144   381   200   936",
				"  10:   912     1     3   480    51     9  2760  6220   540  1800",
				"  11:  5700     1     3   170   750  2480 14880 10990   300  1440  3660",
				"  12:   840     1     3 13800  5880   432 48096 60528   456 17640  8496 10560",
				"T(10,0) = 912 because A243063 eventually enters a cycle of length 912."
			],
			"program": [
				"(PARI) isok(v) = {for (n=1, #v-2, if ((v[#v] == v[#v - n]) \u0026\u0026 (v[#v-1] == v[#v - n - 1]), return (n));); 0;}",
				"f(x, y, n, k) = {my(z=x+y, d = digits(z, n)); fromdigits(select(t-\u003e(t!=k), d), n);}",
				"T(n,k) = {my(v = [0, 1], len = 2); while (! (per = isok(v)), v = concat(v, f(v[len-1], v[len], n, k)); len++;); per;} \\\\ _Michel Marcus_, May 01 2019",
				"(Python)",
				"# Note: the function hangs if the sequence never enters a cycle.",
				"import functools,sympy",
				"def drop(x,n,k): return functools.reduce(lambda x,j:n*x+j if j!=k else x,sympy.ntheory.factor_.digits(x,n)[1:],0) # Drop all digits k from x in base n.",
				"def A306773(n,k): return next(sympy.cycle_length(lambda x:(x[1],drop(x[0]+x[1],n,k)),(0,1)))[0]",
				"# _Pontus von Brömssen_, May 09 2019"
			],
			"xref": [
				"Cf. A011655, A243063."
			],
			"keyword": "nonn,base,tabf",
			"offset": "2,3",
			"author": "_Pontus von Brömssen_, Mar 09 2019",
			"references": 7,
			"revision": 16,
			"time": "2020-02-22T20:57:36-05:00",
			"created": "2019-05-05T11:53:33-04:00"
		}
	]
}