{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A045831",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 45831,
			"data": "1,1,2,3,1,3,3,3,4,4,2,2,7,3,5,6,2,4,7,3,4,7,5,8,5,4,4,8,5,6,7,2,9,11,3,8,9,4,6,5,7,5,14,7,4,10,5,10,11,3,9,10,5,8,10,4,6,15,8,9,10,6,8,15,6,10,6,5,15,9,6,8,14,8,6,13,5,16,18,7,8,7,9,6,15,6,12,17,5,8,15,7,12",
			"name": "Number of 4-core partitions of n.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Conjecturally  Sum_n a(n)q^(8n+5) equals theta series of sodalite. - _Fred Lunnon_, Mar 05 2015",
				"Dickson writes that Liouville proved several related theorems about sums of triangular numbers. - _Michael Somos_, Feb 10 2020"
			],
			"reference": [
				"L. E. Dickson, History of the Theory of Numbers. Carnegie Institute Public. 256, Washington, DC, Vol. 1, 1919; Vol. 2, 1920; Vol. 3, 1923, see vol. II, p. 23."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A045831/b045831.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e (terms 0..1000 from T. D. Noe)",
				"M. Hirschhorn, and J. Sellers, \u003ca href=\"http://dx.doi.org/10.1006/jnth.1996.0112\"\u003eSome amazing facts about 4-cores\u003c/a\u003e, J. Num. Thy. 60 (1996), 51-69.",
				"K. Ono, and L. Sze, \u003ca href=\"http://matwbn.icm.edu.pl/ksiazki/aa/aa80/aa8035.pdf\"\u003e4-core partitions and class numbers\u003c/a\u003e, Acta. Arith. 80 (1997), 249-272.",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"eta(32*z)^4/eta(8*z) = Sum_{x, y, z} q^(x^2+2*y^2+2*z^2), x, y, z \u003e= 1 and odd.",
				"From _Michael Somos_, Mar 24 2003: (Start)",
				"Euler transform of period 4 sequence [1, 1, 1, -3, ...].",
				"Expansion of q^(-5/8) * eta(q^4)^4/eta(q) in powers of q.",
				"(End)",
				"Number of solutions to n=t1+2*t2+2*t3 where t1, t2, t3 are triangular numbers. - _Michael Somos_, Jan 02 2006",
				"G.f.: Product_{k\u003e0} (1-q^(4*k))^4/(1-q^k).",
				"Expansion of psi(q) * psi(q^2)^2 in powers of q where psi() is a Ramanujan theta function. - _Michael Somos_, Sep 02 2008"
			],
			"example": [
				"G.f. = 1 + x + 2*x^2 + 3*x^3 + x^4 + 3*x^5 + 3*x^6 + 3*x^7 + 4*x^8 + 4*x^9 + ...",
				"G.f. = q^5 + q^13 + 2*q^21 + 3*q^29 + q^37 + 3*q^45 + 3*q^53 + 3*q^61 + 4*q^69 + ... ,",
				"apparently the theta series of the sodalite net, aka edge-skeleton of space honeycomb by truncated octahedra. - _Fred Lunnon_, Mar 05 2015"
			],
			"mathematica": [
				"QP = QPochhammer; s = QP[q^4]^4/QP[q] + O[q]^100; CoefficientList[s, q] (* _Jean-François Alcover_, Jul 26 2011, updated Nov 29 2015 *)"
			],
			"program": [
				"(PARI)",
				"{a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^4 + A)^4 / eta(x + A), n))}; /* _Michael Somos_, Mar 24 2003 */"
			],
			"xref": [
				"A004024/4, column t=4 of A175595.",
				"Cf. A286953.",
				"Cf. A008443, A045818, A213624."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _James A. Sellers_, Feb 11 2000"
			],
			"references": 14,
			"revision": 59,
			"time": "2021-11-23T05:48:50-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}