{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A208757",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 208757,
			"data": "1,1,2,1,2,6,1,2,8,16,1,2,10,24,44,1,2,12,32,76,120,1,2,14,40,112,232,328,1,2,16,48,152,368,704,896,1,2,18,56,196,528,1200,2112,2448,1,2,20,64,244,712,1824,3840,6288,6688,1,2,22,72,296,920,2584,6144",
			"name": "Triangle of coefficients of polynomials u(n,x) jointly generated with A208758; see the Formula section.",
			"comment": [
				"For a discussion and guide to related arrays, see A208510.",
				"Subtriangle of the triangle (1, 0, -1, 1, 0, 0, 0, 0, 0, 0, 0, ...) DELTA (0, 2, 1, -1, 0, 0, 0, 0, 0, 0, 0, ...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, Mar 18 2012"
			],
			"formula": [
				"u(n,x) = u(n-1,x) + 2x*v(n-1,x),",
				"v(n,x) = x*u(n-1,x) + 2x*v(n-1,x),",
				"where u(1,x)=1, v(1,x)=1.",
				"From _Philippe Deléham_, Mar 18 2012: (Start)",
				"As DELTA-triangle with 0 \u003c= k \u003c= n:",
				"G.f.: (1-2*y*x+2*y*x^2-2*y^2*x^2)/(1-x-2*y*x+2*y*x^2-2*y^2*x^2).",
				"T(n,k) = T(n-1,k) + 2*T(n-1,k-1) -2*T(n-2,k-1) + 2*T(n-2,k-2), T(0,0) = T(1,0) = T(2,0) = 1, T(1,1) = T(2,2) = 0, T(2,1) = 2 and T(n,k) = 0 if k \u003c 0 or if k \u003e n. (End)"
			],
			"example": [
				"First five rows:",
				"  1;",
				"  1, 2;",
				"  1, 2,  6;",
				"  1, 2,  8, 16;",
				"  1, 2, 10, 24, 44;",
				"First five polynomials u(n,x):",
				"  1",
				"  1 + 2x",
				"  1 + 2x +  6x^2",
				"  1 + 2x +  8x^2 + 16x^3",
				"  1 + 2x + 10x^2 + 24x^3 + 44x^4",
				"From _Philippe Deléham_, Mar 18 2012: (Start)",
				"(1, 0, -1, 1, 0, 0, ...) DELTA (0, 2, 1, -1, 0, 0, ...) begins:",
				"  1",
				"  1, 0",
				"  1, 2,  0",
				"  1, 2,  6,  0",
				"  1, 2,  8, 16,   0",
				"  1, 2, 10, 24,  44,   0",
				"  1, 2, 12, 32,  76, 120,   0",
				"  1, 2, 14, 40, 112, 232, 328, 0; (End)"
			],
			"mathematica": [
				"u[1, x_] := 1; v[1, x_] := 1; z = 16;",
				"u[n_, x_] := u[n - 1, x] + 2 x*v[n - 1, x];",
				"v[n_, x_] := x*u[n - 1, x] + 2 x*v[n - 1, x];",
				"Table[Expand[u[n, x]], {n, 1, z/2}]",
				"Table[Expand[v[n, x]], {n, 1, z/2}]",
				"cu = Table[CoefficientList[u[n, x], x], {n, 1, z}];",
				"TableForm[cu]",
				"Flatten[%]  (* A208757 *)",
				"Table[Expand[v[n, x]], {n, 1, z}]",
				"cv = Table[CoefficientList[v[n, x], x], {n, 1, z}];",
				"TableForm[cv]",
				"Flatten[%]  (* A208758 *)"
			],
			"xref": [
				"Cf. A208758, A208510."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Clark Kimberling_, Mar 02 2012",
			"references": 4,
			"revision": 14,
			"time": "2020-01-22T20:13:30-05:00",
			"created": "2012-03-02T14:56:51-05:00"
		}
	]
}