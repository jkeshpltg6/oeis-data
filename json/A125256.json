{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A125256",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 125256,
			"data": "5,5,17,13,37,5,5,41,101,61,5,5,197,113,257,5,5,181,401,13,5,5,577,313,677,5,5,421,17,13,5,5,13,613,1297,5,5,761,1601,29,5,5,13,1013,29,5,5,1201,41,1301,5,5,2917,17,3137,5,5,1741,13,1861,5,5,17,2113,4357,5,5",
			"name": "Smallest odd prime divisor of n^2 + 1.",
			"comment": [
				"Any odd prime divisor of n^2+1 is congruent to 1 modulo 4.",
				"n^2+1 is never a power of 2 for n \u003e 1; hence a prime divisor congruent to 1 modulo 4 always exists.",
				"a(n) = 5 if and only if n is congruent to 2 or -2 modulo 5.",
				"If the map \"x -\u003e smallest odd prime divisor of n^2+1\" is iterated, does it always terminate in the 2-cycle (5 \u003c-\u003e 13)? - _Zoran Sunic_, Oct 25 2017"
			],
			"reference": [
				"D. M. Burton, Elementary Number Theory, McGraw-Hill, Sixth Edition (2007), p. 191."
			],
			"link": [
				"Ray Chandler, \u003ca href=\"/A125256/b125256.txt\"\u003eTable of n, a(n) for n = 2..20001\u003c/a\u003e (first 999 terms from N. Hobson)",
				"N. Hobson, \u003ca href=\"http://www.qbyte.org/puzzles/\"\u003eHome page (listed in lieu of email address)\u003c/a\u003e"
			],
			"example": [
				"The prime divisors of 8^2 + 1 = 65 are 5 and 13, so a(7) = 5."
			],
			"maple": [
				"with(numtheory, factorset);",
				"A125256 := proc(n) local t1,t2;",
				"if n \u003c= 1 then return(-1); fi;",
				"if (n mod 5) = 2 or (n mod 5) = 3 then return(5); fi;",
				"t1 := numtheory[factorset](n^2+1);",
				"t2:=sort(convert(t1,list));",
				"if (n mod 2) = 1 then return(t2[2]); fi;",
				"t2[1];",
				"end;",
				"[seq(A125256(n),n=1..40)]; # _N. J. A. Sloane_, Nov 04 2017"
			],
			"program": [
				"(PARI) vector(68, n, if(n\u003c2, \"-\", factor(n^2+1)[1+(n%2),1]))",
				"(PARI) A125256(n)=factor(n^2+1)[1+bittest(n,0),1] \\\\ _M. F. Hasler_, Nov 06 2017"
			],
			"xref": [
				"Cf. A002522, A002496, A014442, A057207, A031439.",
				"For bisections see A256970, A293958.",
				"See also A125257, A125258, A294656, A294657, A294658."
			],
			"keyword": "easy,nonn",
			"offset": "2,1",
			"author": "Nick Hobson, Nov 26 2006",
			"references": 7,
			"revision": 35,
			"time": "2020-02-22T15:03:14-05:00",
			"created": "2006-12-06T03:00:00-05:00"
		}
	]
}