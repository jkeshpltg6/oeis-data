{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A056993",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 56993,
			"data": "2,2,2,2,2,30,102,120,278,46,824,150,1534,30406,67234,70906,48594,62722,24518,75898,919444",
			"name": "a(n) is the smallest k \u003e= 2 such that k^(2^n)+1 is prime, or -1 if no such k exists.",
			"comment": [
				"Smallest base value yielding generalized Fermat primes. - _Hugo Pfoertner_, Jul 01 2003",
				"The first 5 terms correspond with the known (ordinary) Fermat primes. A probable candidate for the next entry is 62722^131072+1, discovered by Michael Angel in 2003. It has 628808 decimal digits. - _Hugo Pfoertner_, Jul 01 2003",
				"For any n, a(n+1) \u003e= sqrt(a(n)), because k^(2^(n+1))+1 = (k^2)^(2^n)+1. - _Jeppe Stig Nielsen_, Sep 16 2015",
				"Does the sequence contain any perfect squares? If a(n) is a perfect square, then a(n+1) = sqrt(a(n)). - _Jeppe Stig Nielsen_, Sep 16 2015",
				"If for a particular n, a(n) exists, then a(i) exist for all i=0,1,2,...,n. No proof is known that this sequence is infinite. Such a result would clearly imply the infinitude of A002496. - _Jeppe Stig Nielsen_, Sep 18 2015",
				"919444 is a candidate for a(20). See Zimmermann link. -  _Serge Batalov_, Sep 02 2017",
				"Now PrimeGrid has tested and double checked all b^(2^20) + 1 with b \u003c 919444, so we have proof that a(20) = 919444. - _Jeppe Stig Nielsen_, Dec 30 2017"
			],
			"link": [
				"Yves Gallot, \u003ca href=\"http://yves.gallot.pagesperso-orange.fr/primes/gfn.html\"\u003eGeneralized Fermat Prime Search\u003c/a\u003e",
				"Lucile and Yves Gallot, \u003ca href=\"http://yves.gallot.pagesperso-orange.fr/primes/results.html\"\u003eGeneralized Fermat Prime Search\u003c/a\u003e",
				"Michael Goetz, \u003ca href=\"http://primes.utm.edu/primes/page.php?id=103235\"\u003eid=103235 of Top 5000 Primes\u003c/a\u003e",
				"Stephen Scott, \u003ca href=\"http://primes.utm.edu/primes/page.php?id=84401\"\u003eid=84401 of Top 5000 Primes\u003c/a\u003e",
				"Sylvanus A. Zimmerman, \u003ca href=\"http://www.primegrid.com/download/GFN-919444_1048576.pdf\"\u003ePrimeGrid’s Generalized Fermat Prime Search\u003c/a\u003e"
			],
			"example": [
				"The primes are 2^(2^0) + 1 = 3, 2^(2^1) + 1 = 5, 2^(2^2) + 1 = 17, 2^(2^3) + 1 = 257, 2^(2^4) + 1 = 65537, 30^(2^5) + 1, 102^(2^6) + 1, ...."
			],
			"mathematica": [
				"f[n_] := (p = 2^n; k = 2; While[cp = k^p + 1; !PrimeQ@cp, k++ ]; k); Do[ Print[{n, f@n}], {n, 0, 17}] (* _Lei Zhou_, Feb 21 2005 *)"
			],
			"program": [
				"(PARI) a(n)=my(k=2);while(!isprime(k^(2^n)+1),k++);k \\\\ _Anders Hellström_, Sep 16 2015"
			],
			"xref": [
				"Cf. A006093, A005574, A000068, A006314, A006313, A006315, A006316, A056994, A056995, A057465, A057002, A088361, A088362, A226528, A226529, A226530, A251597, A253854, A244150, A243959, A321323.",
				"Cf. A019434 (Fermat primes)."
			],
			"keyword": "hard,more,nonn",
			"offset": "0,1",
			"author": "_Robert G. Wilson v_, Sep 06 2000",
			"ext": [
				"1534 from _Robert G. Wilson v_, Oct 30 2000",
				"62722 from _Jeppe Stig Nielsen_, Aug 07 2005",
				"24518 and 75898 from _Lei Zhou_, Feb 01 2012",
				"919444 from _Jeppe Stig Nielsen_, Dec 30 2017"
			],
			"references": 35,
			"revision": 61,
			"time": "2020-11-16T03:53:42-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}