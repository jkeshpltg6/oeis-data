{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A078442",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 78442,
			"data": "0,1,2,0,3,0,1,0,0,0,4,0,1,0,0,0,2,0,1,0,0,0,1,0,0,0,0,0,1,0,5,0,0,0,0,0,1,0,0,0,2,0,1,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,3,0,1,0,0,0,0,0,2,0,0,0,1,0,1,0,0,0,0,0,1,0,0,0,2,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,1,0,1,0,0",
			"name": "a(p) = a(n) + 1 if p is the n-th prime, prime(n); a(n)=0 if n is not prime.",
			"comment": [
				"Fernandez calls this the order of primeness of n.",
				"a(A007097(n))=n, for any n \u003e= 0. - _Paul Tek_, Nov 12 2013",
				"When a nonoriented rooted tree is encoded as a Matula-Goebel number n, a(n) tells how many edges needs to be climbed up from the root of the tree until the first branching vertex (or the top of the tree, if n is one of the terms of A007097) is encountered. Please see illustrations at A061773. - _Antti Karttunen_, Jan 27 2014"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A078442/b078442.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"N. Fernandez, \u003ca href=\"http://www.borve.org/primeness/FOP.html\"\u003eAn order of primeness, F(p)\u003c/a\u003e",
				"N. Fernandez, \u003ca href=\"/A006450/a006450.html\"\u003eAn order of primeness\u003c/a\u003e [cached copy, included with permission of the author]",
				"\u003ca href=\"/index/Mat#matula\"\u003eIndex entries for sequences related to Matula-Goebel numbers\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A049076(n)-1.",
				"a(n) = if A049084(n) = 0 then 0 else a(A049084(n)) + 1. - _Reinhard Zumkeller_, Jul 14 2013",
				"For all n, a(n) = A007814(A135141(n)) and a(A227413(n)) = A007814(n). Also a(A235489(n)) = a(n). - _Antti Karttunen_, Jan 27 2014"
			],
			"example": [
				"a(1) = 0 since 1 is not prime;",
				"a(2) = a(prime(1)) = a(1) + 1 = 1 + 0 = 1;",
				"a(3) = a(prime(2)) = a(2) + 1 = 1 + 1 = 2;",
				"a(4) = 0 since 4 is not prime;",
				"a(5) = a(prime(3)) = a(3) + 1 = 2 + 1 = 3;",
				"a(6) = 0 since 6 is not prime;",
				"a(7) = a(prime(4)) = a(4) + 1 = 0 + 1 = 1."
			],
			"maple": [
				"A078442 := proc(n)",
				"    if not isprime(n) then",
				"        0 ;",
				"    else",
				"        1+procname(numtheory[pi](n)) ;",
				"    end if;",
				"end proc: # _R. J. Mathar_, Jul 07 2012"
			],
			"mathematica": [
				"a[n_] := a[n] = If[!PrimeQ[n], 0, 1+a[PrimePi[n]]]; Array[a, 105] (* _Jean-François Alcover_, Jan 26 2018 *)"
			],
			"program": [
				"(PARI) A078442(n)=for(i=0,n, isprime(n) | return(i); n=primepi(n)) \\\\ _M. F. Hasler_, Mar 09 2010",
				"(Haskell)",
				"a078442 n = fst $ until ((== 0) . snd)",
				"                        (\\(i, p) -\u003e (i + 1, a049084 p)) (-2, a000040 n)",
				"-- _Reinhard Zumkeller_, Jul 14 2013"
			],
			"xref": [
				"A left inverse of A007097.",
				"One less than A049076.",
				"a(A000040(n)) = A049076(n).",
				"Cf. A018252 (positions of zeros).",
				"Cf. A000720, A049084, A061773.",
				"Cf. permutations A235489, A250247/A250248, A250249/A250250, A245821/A245822 that all preserve a(n).",
				"Cf. also array A114537 (A138947) and permutations A135141/A227413, A246681."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Henry Bottomley_, Dec 31 2002",
			"references": 25,
			"revision": 52,
			"time": "2020-03-15T12:11:38-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}