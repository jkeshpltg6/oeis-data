{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A236067",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 236067,
			"data": "1,0,12,4624,3909511,0,13177388,1033,10,0,0,0,0,0,2758053616,1053202,7413245658,419370838921,52135640,1347536041,833904227332,5117557126,3606012949057,5398293152472,31301,0,15554976231978,405287637330,35751665247,19705624111111",
			"name": "a(n) is the least number m such that m = n^d_1 + n^d_2 + ... + n^d_k where d_k represents the k-th digit in the decimal expansion of m, or 0 if no such number exists.",
			"comment": [
				"The 0's in the sequence are definite. There exists both a maximum and a minimum number that a(n) can be based on n. They are given in the programs below as Max(n) and Min(n), respectively.",
				"It is known that a(22) = 5117557126, a(25) = 31301, a(29) = 35751665247, a(32) = 2112, a(33) = 1224103, a(37) = 111, a(40) = 102531321, a(48) = 25236435456, a(50) = 101, a(66) = 2524232305, a(78) = 453362316342, a(98) = 100, and a(100) = 20102.",
				"There are an infinite number of nonzero entries. First, note if a(n) is nonzero, a(n) \u003e= n. Further, a(9) = 10, a(98) = 100, a(997) = 1000, ..., a(10^k-k) = 10^k for all k \u003e= 0.",
				"For n = 21, 23, and 24, a(n) \u003e 10^10.",
				"For n in {26, 27, 28, 30, 31, 34, 35, 36, 38, 39, 41, 42, 43, 44, 45, 46, 47, 49}, a(n) \u003e 5*10^10.",
				"For n in {51, 52, 53, ..., 64, 65} and {67, 68, 69, ..., 73, 74}, a(n) \u003e 10^11.",
				"For n in {75, 76, 77} and {79, 80, 81, ..., 96, 97, 99}, a(n) \u003e 5*10^11.",
				"A few nonzero terms were added by math4pad.net @PascalCardin",
				"a(1000) = 1000000000000002002017, a(10000) = 0, a(1000000) = 1000002000010, a(10000000) = 200000020000011. It looks like a(10^k) in decimal consists of mostly the digits 0, 1 and 2. - _Chai Wah Wu_, Dec 07 2017"
			],
			"link": [
				"Chai Wah Wu, \u003ca href=\"/A236067/b236067.txt\"\u003eTable of n, a(n) for n = 1..500\u003c/a\u003e (n = 1..100 from Hiroaki Yamanouchi)",
				"John D. Cook, \u003ca href=\"http://www.johndcook.com/blog/2012/02/20/monday-morning-math-puzzle/\"\u003eMonday morning math puzzle\u003c/a\u003e (2012)",
				"Dean Morrow, \u003ca href=\"http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.64.3408\u0026amp;rep=rep1\u0026amp;type=pdf\"\u003eCycles of a family of digit functions\u003c/a\u003e"
			],
			"example": [
				"12 is the smallest number such that 3^1 + 3^2 = 12 so a(3) = 12.",
				"4624 is the smallest number such that 4^4 + 4^6 + 4^2 + 4^4 = 4624 so a(4) = 4624.",
				"1033 is the smallest number such that 8^1 + 8^0 + 8^3 + 8^3 = 1033 so a(8) = 1033."
			],
			"program": [
				"(PARI)",
				"Min(n)=for(k=1,10^3,if(n+k\u003c=10^k,return(10^k)))",
				"Max(n)=for(k=1,10^3,if(k*n^9\u003c=10^k-1,return(10^(k-1))))",
				"side(n,q)=v=digits(q);for(i=1,10,qq=digits((floor(q/10^i)+1)*10^i);st=sum(j=1,#qq,n^qq[j]);if(q+10^i\u003est,return((floor(q/10^i)+1)*10^(i-1))))",
				"a(n)=k=Min(n);while(k\u003c=Max(n),q=10*k;d=digits(q);s=sum(i=1,#d,n^d[i]);if(q\u003cs,k=side(n,q));if(q\u003es,for(j=1,9,dd=digits(q+j);ss=sum(m=1,#dd,n^dd[m]);if(q+j\u003css,k++;break);if(q+j==ss,return(q+j)));if(q+9\u003ess,k++));if(q==s,return(q)));return(0)",
				"n=1;while(n\u003c100,print1(a(n),\", \");n++) \\\\ PARI program more advanced than Python program \\\\ _Derek Orr_, Aug 01 2014",
				"(Python)",
				"def Min(n):",
				"..for k in range(1,10**3):",
				"....if n+k \u003c= 10**k:",
				"......return 10**k",
				"def Max(n):",
				"..for k in range(1,10**3):",
				"....if k*(n**9) \u003c= 10**k-1:",
				"......return 10**(k-1)",
				"def div10(n):",
				"..for j in range(10**3):",
				"....if n%10**j!=0:",
				"......return j",
				"def a(n):",
				"..k = Min(n)",
				"..while k \u003c= Max(n):",
				"....tot = 0",
				"....for i in str(k):",
				"......tot += n**(int(i))",
				"....if tot == k:",
				"......return k",
				"....if tot \u003c k:",
				"......k += 1",
				"....if tot \u003e k-1:",
				"......k = (1+k//10**div10(k))*10**div10(k)",
				"n = 1",
				"while n \u003c 100:",
				"..if a(n):",
				"....print(a(n),end=', ')",
				"..else:",
				"....print(0,end=', ')",
				"..n += 1",
				"# _Derek Orr_, Aug 01 2014"
			],
			"xref": [
				"Cf. A139410 (for 4th term), A003321, A296138, A296139."
			],
			"keyword": "nonn,base",
			"offset": "1,3",
			"author": "_Derek Orr_, Jan 19 2014",
			"ext": [
				"More terms and edited extensively by _Derek Orr_, Aug 26 2014",
				"a(21)-a(30) from _Hiroaki Yamanouchi_, Sep 27 2014"
			],
			"references": 3,
			"revision": 85,
			"time": "2017-12-08T15:50:22-05:00",
			"created": "2014-02-01T15:55:01-05:00"
		}
	]
}