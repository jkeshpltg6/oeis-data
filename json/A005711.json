{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005711",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5711,
			"id": "M0479",
			"data": "1,1,1,1,1,1,1,1,2,3,4,5,6,7,8,9,10,12,15,19,24,30,37,45,54,64,76,91,110,134,164,201,246,300,364,440,531,641,775,939,1140,1386,1686,2050,2490,3021,3662,4437,5376,6516,7902,9588,11638,14128,17149,20811,25248",
			"name": "a(n) = a(n-1) + a(n-9) for n \u003e= 9; a(n) = 1 for n=0..7; a(8) = 2.",
			"comment": [
				"a(n+7) equals the number of binary words of length n having at least 8 zeros between every two successive ones. - _Milan Janjic_, Feb 09 2015",
				"a(n) is the number of compositions of n+1 into parts 1 and 9. - _Joerg Arndt_, May 19 2018"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A005711/b005711.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"I. M. Gessel, Ji Li, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL16/Gessel/gessel6.html\"\u003eCompositions and Fibonacci identities\u003c/a\u003e, J. Int. Seq. 16 (2013) 13.4.5",
				"R. K. Guy, \u003ca href=\"/A004001/a004001_2.pdf\"\u003eLetter to N. J. A. Sloane with attachment, 1988\u003c/a\u003e",
				"D. Kleitman, \u003ca href=\"http://www.jstor.org/stable/2324158\"\u003eSolution to Problem E3274\u003c/a\u003e, Amer. Math. Monthly, 98 (1991), 958-959.",
				"Augustine O. Munagi, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL21/Munagi/munagi10.html\"\u003eInteger Compositions and Higher-Order Conjugation\u003c/a\u003e, J. Int. Seq., Vol. 21 (2018), Article 18.8.5.",
				"D. Newman, \u003ca href=\"http://www.jstor.org/stable/2322766\"\u003eProblem E3274\u003c/a\u003e, Amer. Math. Monthly, 95 (1988), 555.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=382\"\u003eEncyclopedia of Combinatorial Structures 382\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_09\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1, 0, 0, 0, 0, 0, 0, 0, 1)."
			],
			"formula": [
				"G.f.: (1+x^8)/(1-x-x^9).",
				"For positive integers n and k such that k \u003c= n \u003c= 9*k, and 8 divides n-k, define c(n,k) = binomial(k,(n-k)/8), and c(n,k) = 0, otherwise. Then, for n\u003e= 1, a(n-1) = Sum_{k=1..n} c(n,k). - _Milan Janjic_, Dec 09 2011"
			],
			"maple": [
				"A005711:=-(1+z**8)/(-1+z+z**9); # _Simon Plouffe_ in his 1992 dissertation",
				"ZL:=[S, {a = Atom, b = Atom, S = Prod(X,Sequence(Prod(X,b))), X = Sequence(b,card \u003e= 8)}, unlabelled]: seq(combstruct[count](ZL, size=n), n=9..65); # _Zerinvary Lajos_, Mar 26 2008",
				"M:= Matrix(9, (i,j)-\u003e if j=1 and member(i,[1,9]) then 1 elif (i=j-1) then 1 else 0 fi); a:= n-\u003e (M^(n+1))[1,1]; seq(a(n), n=0..60); # _Alois P. Heinz_, Jul 27 2008"
			],
			"mathematica": [
				"CoefficientList[Series[(1+x^8)/(1-x-x^9), {x, 0, 57}], x] (* _Michael De Vlieger_, May 20 2018 *)"
			],
			"program": [
				"(PARI) x='x+O('x^66); Vec((1+x^8)/(1-x-x^9)) /* _Joerg Arndt_, Jun 25 2011 */"
			],
			"xref": [
				"Cf. A005710."
			],
			"keyword": "nonn,easy",
			"offset": "0,9",
			"author": "_N. J. A. Sloane_",
			"references": 11,
			"revision": 90,
			"time": "2021-03-12T22:32:36-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}