{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000171",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 171,
			"id": "M0014 N0780",
			"data": "1,0,0,1,2,0,0,10,36,0,0,720,5600,0,0,703760,11220000,0,0,9168331776,293293716992,0,0,1601371799340544,102484848265030656,0,0,3837878966366932639744,491247277315343649710080,0,0",
			"name": "Number of self-complementary graphs with n nodes.",
			"comment": [
				"a(n) = A007869(n)-A054960(n), where A007869(n) is number of unlabeled graphs with n nodes and an even number of edges and A054960(n) is number of unlabeled graphs with n nodes and an odd number of edges."
			],
			"reference": [
				"F. Harary and E. M. Palmer, Graphical Enumeration, Academic Press, NY, 1973, p. 139, Table 6.1.1.",
				"R. C. Read and R. J. Wilson, An Atlas of Graphs, Oxford, 1998.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A000171/b000171.txt\"\u003eTable of n, a(n) for n = 1..100\u003c/a\u003e",
				"H. Fripertinger, \u003ca href=\"http://www.mathe2.uni-bayreuth.de/frib/html/book/hyl00_46.html\"\u003eSelf-complementary graphs\u003c/a\u003e",
				"Victoria Gatt, Mikhail Klin, Josef Lauri, Valery Liskovets, \u003ca href=\"https://doi.org/10.1007/978-3-030-32808-5_2\"\u003eFrom Schur Rings to Constructive and Analytical Enumeration of Circulant Graphs with Prime-Cubed Number of Vertices\u003c/a\u003e, in Isomorphisms, Symmetry and Computations in Algebraic Graph Theory, (Pilsen, Czechia, WAGT 2016) Vol. 305, Springer, Cham, 37-65.",
				"Richard A. Gibbs, \u003ca href=\"https://doi.org/10.1016/0095-8956(74)90053-7\"\u003eSelf-complementary graphs\u003c/a\u003e J. Combinatorial Theory Ser. B 16 (1974), 106--123. MR0347686 (50 #188). - _N. J. A. Sloane_, Mar 27 2012",
				"Sebastian Jeon, Tanya Khovanova, \u003ca href=\"https://arxiv.org/abs/2003.03870\"\u003e3-Symmetric Graphs\u003c/a\u003e, arXiv:2003.03870 [math.CO], 2020.",
				"B. D. McKay, \u003ca href=\"http://users.cecs.anu.edu.au/~bdm/data/graphs.html\"\u003eSelf-complementary graphs\u003c/a\u003e",
				"R. C. Read, \u003ca href=\"https://doi.org/10.1112/jlms/s1-38.1.99\"\u003eOn the number of self-complementary graphs and digraphs\u003c/a\u003e, J. London Math. Soc., 38 (1963), 99-104.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Self-ComplementaryGraph.html\"\u003eSelf-Complementary Graph\u003c/a\u003e",
				"D. Wille, \u003ca href=\"https://doi.org/10.1016/0095-8956(78)90034-5\"\u003eEnumeration of self-complementary structures\u003c/a\u003e, J. Comb. Theory B 25 (1978) 143-150"
			],
			"formula": [
				"a(4n) = A003086(2n).",
				"a(4*n+1) = A047832(n), a(4*n+2) = a(4*n+3) = 0. - _Andrew Howroyd_, Sep 16 2018"
			],
			"mathematica": [
				"\u003c\u003cCombinatorica`; Table[GraphPolynomial[n, x]/.x -\u003e -1, {n, 1, 20}]  (* _Geoffrey Critzer_, Oct 21 2012 *)",
				"permcount[v_] := Module[{m = 1, s = 0, k = 0, t}, For[i = 1, i \u003c= Length[v], i++, t = v[[i]]; k = If[i \u003e 1 \u0026\u0026 t == v[[i - 1]], k + 1, 1]; m *= t*k; s += t]; s!/m];",
				"edges[v_] := 4 Sum[Sum[GCD[v[[i]], v[[j]]], {j, 1, i - 1}], {i, 2, Length[v]}] + 2 Total[v];",
				"a[n_] := Module[{s = 0}, Switch[Mod[n, 4], 2|3, 0, _, Do[s += permcount[4 p]*2^edges[p]*If[OddQ[n], n*2^Length[p], 1], {p, IntegerPartitions[ Quotient[n, 4]]}]; s/n!]];",
				"Array[a, 40] (* _Jean-François Alcover_, Aug 26 2019, after _Andrew Howroyd_ *)"
			],
			"program": [
				"(PARI)",
				"permcount(v) = {my(m=1,s=0,k=0,t); for(i=1,#v,t=v[i]; k=if(i\u003e1\u0026\u0026t==v[i-1],k+1,1); m*=t*k;s+=t); s!/m}",
				"edges(v) = {4*sum(i=2, #v, sum(j=1, i-1, gcd(v[i],v[j]))) + 2*sum(i=1, #v, v[i])}",
				"a(n) = {my(s=0); if(n%4\u003c2, forpart(p=n\\4, s+=permcount(4*Vec(p)) * 2^edges(p) * if(n%2, n*2^#p, 1))); s/n!} \\\\ _Andrew Howroyd_, Sep 16 2018"
			],
			"xref": [
				"Cf. A047660, A051251, A047832.",
				"Cf. A008406 (triangle of coefficients of the \"graph polynomial\")."
			],
			"keyword": "nonn,nice",
			"offset": "1,5",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from R. C. Read (rcread(AT)math.uwaterloo.ca) and _Vladeta Jovovic_"
			],
			"references": 16,
			"revision": 66,
			"time": "2020-06-19T03:46:46-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}