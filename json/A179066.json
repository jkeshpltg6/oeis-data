{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A179066",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 179066,
			"data": "28,11,12,13,14,15,16,17,18,37,2,3,4,5,6,7,8,9,46,38,30,31,41,33,34,35,36,1,47,21,22,50,24,25,26,27,10,20,48,58,23,51,52,53,63,19,29,39,67,32,42,43,44,72,64,74,66,40,68,78,70,71,45,55,83,57,49,59,87,61,62",
			"name": "a(n) is the smallest positive number having the same digital root as n but no digit in common with n and not occurring earlier.",
			"comment": [
				"The sequence certainly cannot be continued beyond N* = 1124578, because a(N*) could only be written using \"0\", \"3\", \"6\" and \"9\", and then its digital root A010888 is always 3, 6 or 9, while that of N* equals 1.",
				"It appears that a(n)=m \u003c=\u003e a(m)=n, i.e., a is its own inverse, a(a(n))=n, whenever a(m) is defined.",
				"a(261379)=4000555 is the first term for which a(m) is not defined. - _Hans Havermann_, Jan 24 2011",
				"It seems that there is no invariant subset of the form {1,...,N} on which a is defined.",
				"_Lars Blomberg_ has calculated all terms up to a(1124577)=3008889, the first 481185 terms being confirmed by _Hans Havermann_. - _Eric Angelini_, Mar 21 2011"
			],
			"link": [
				"M. F. Hasler, \u003ca href=\"/A179066/b179066.txt\"\u003eTable of n, a(n) for n = 1..13578\u003c/a\u003e",
				"Lars Blomberg, \u003ca href=\"http://chesswanks.com/seq/b179066.txt\"\u003eTable of n, a(n) for n = 1..1124577\u003c/a\u003e (complete sequence)",
				"E. Angelini, M. F. Hasler, B. Jubin, \u003ca href=\"http://list.seqfan.eu/pipermail/seqfan/2011-January/006744.html\"\u003ea(n) and n have the same digitsum but no digit in common\u003c/a\u003e, seqfan mailing list, Jan 04 2011",
				"Hans Havermann, \u003ca href=\"http://chesswanks.com/num/fractalA179066.html\"\u003eA179066 as a fractal\u003c/a\u003e"
			],
			"example": [
				"The digital root (A010888) of n=1 is 1 and the first not-yet-used number with the same digital root (1, 10, 19, 28, ...) not containing a \"1\" is 28. The digital root of n=2 is 2 and the first not-yet-used number with the same digital root (2, 11, 20, ...) not containing a \"2\" is 11. The digital root of n=12345 is 6 and the first not-yet-used number with the same digital root (6, 15, 24, ...) not containing a \"1\" or a \"2\" or a \"3\" or a \"4\" or a \"5\" is 60000."
			],
			"mathematica": [
				"digitalRoot[n_] := Mod[n-1, 9] + 1; a[0] = 1; a[n_] := a[n] = For[d = IntegerDigits[n]; r = digitalRoot[n]; k = 1, True, k++, If[ FreeQ[ Array[a, n-1], k] \u0026\u0026 digitalRoot[k] == r \u0026\u0026 Intersection[d, IntegerDigits[k]] == {}, Return[k]]]; Table[a[n], {n, 1, 100}] (* _Jean-François Alcover_, Aug 13 2013 *)"
			],
			"program": [
				"(PARI) /* using the \"proof by 9\" idea from Benoît Jubin */ {A179066=[]; Nmax=9999 /* number of terms to compute */; S=sum(j=1,#A179066,1\u003c\u003cA179066[j]); L=1 /* least number not yet used */; LIM=1124578 /* search limit */; for(n=#A179066+1,Nmax, dn=Set(Vec(Str(n))); while(bittest(S,L),L++); forstep( a=L+(n-L)%9,LIM,9, bittest(S,a) \u0026 next; setintersect(dn,Set(Vec(Str(a)))) \u0026 next; S+=1\u003c\u003ca; A179066=concat(A179066,a); next(2)); print1(\"A179066(\"n\") not found up to search limit \"LIM); break)}",
				"(MAGMA) DigitalRoot:=func\u003c n | n le 0 select 0 else (n-1) mod 9 + 1 \u003e; NextA179066:=function(n, T); k:=DigitalRoot(n); while k in T or not IsEmpty(Set(Intseq(k)) meet Set(Intseq(n))) do k+:=9; end while; return k; end function; T:=[]; for n in [1..100] do a:=NextA179066(n, T); Append(~T, a); end for; T; // _Klaus Brockhaus_, Jan 26 2011"
			],
			"xref": [
				"Cf. A179105 for record values, A179110 for indices of record values."
			],
			"keyword": "nonn,fini,full,base,nice",
			"offset": "1,1",
			"author": "_Eric Angelini_ and _M. F. Hasler_, Jan 04 2011",
			"references": 3,
			"revision": 50,
			"time": "2018-05-07T03:47:17-04:00",
			"created": "2010-11-12T14:28:32-05:00"
		}
	]
}