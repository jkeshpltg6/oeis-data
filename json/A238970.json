{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A238970",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 238970,
			"data": "1,1,2,2,2,3,4,3,4,5,6,8,3,5,6,8,9,12,16,4,6,8,10,8,12,16,14,18,24,32,4,7,9,12,10,15,20,16,18,24,32,27,36,48,64,5,8,11,14,12,18,24,13,20,23,30,40,24,32,36,48,64,41,54,72,96,128",
			"name": "The number of nodes at even level in divisor lattice in canonical order.",
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A238970/b238970.txt\"\u003eTable of n, a(n) for n = 0..2713\u003c/a\u003e (rows 0..20)",
				"S.-H. Cha, E. G. DuCasse, and L. V. Quintas, \u003ca href=\"http://arxiv.org/abs/1405.5283\"\u003eGraph Invariants Based on the Divides Relation and Ordered by Prime Signatures\u003c/a\u003e, arxiv:1405.5283 [math.NT], 2014."
			],
			"formula": [
				"From _Andrew Howroyd_, Mar 25 2020: (Start)",
				"T(n,k) = A038548(A063008(n,k)).",
				"T(n,k) = A238963(n,k) - A238971(n,k).",
				"T(n,k) = ceiling(A238963(n,k)/2). (End)"
			],
			"example": [
				"Triangle T(n,k) begins:",
				"  1;",
				"  1;",
				"  2, 2;",
				"  2, 3, 4;",
				"  3, 4, 5,  6, 8;",
				"  3, 5, 6,  8, 9, 12, 16;",
				"  4, 6, 8, 10, 8, 12, 16, 14, 18, 24, 32;",
				"  ..."
			],
			"maple": [
				"b:= (n, i)-\u003e `if`(n=0 or i=1, [[1$n]], [map(x-\u003e",
				"    [i, x[]], b(n-i, min(n-i, i)))[], b(n, i-1)[]]):",
				"T:= n-\u003e map(x-\u003e ceil(numtheory[tau](mul(ithprime(i)",
				"        ^x[i], i=1..nops(x)))/2), b(n$2))[]:",
				"seq(T(n), n=0..9);  # _Alois P. Heinz_, Mar 25 2020"
			],
			"program": [
				"(PARI) \\\\ here b(n) is A038548.",
				"b(n)={ceil(numdiv(n)/2)}",
				"N(sig)={prod(k=1, #sig, prime(k)^sig[k])}",
				"Row(n)={apply(s-\u003eb(N(s)), vecsort([Vecrev(p) | p\u003c-partitions(n)], , 4))}",
				"{ for(n=0, 8, print(Row(n))) } \\\\ _Andrew Howroyd_, Mar 25 2020"
			],
			"xref": [
				"Cf. A238957 in canonical order.",
				"Cf. A038548, A063008, A238963, A238971."
			],
			"keyword": "nonn,tabf",
			"offset": "0,3",
			"author": "_Sung-Hyuk Cha_, Mar 07 2014",
			"ext": [
				"Offset changed and terms a(50) and beyond from _Andrew Howroyd_, Mar 25 2020"
			],
			"references": 3,
			"revision": 19,
			"time": "2020-04-24T11:42:45-04:00",
			"created": "2014-09-30T16:04:17-04:00"
		}
	]
}