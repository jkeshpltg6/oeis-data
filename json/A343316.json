{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A343316",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 343316,
			"data": "0,0,0,0,1,0,0,-1,-1,0,0,0,4,0,0,0,1,3,3,1,0,0,-1,2,3,2,-1,0,0,0,-2,3,3,-2,0,0,0,1,-3,-3,4,-3,-3,1,0,0,-1,-4,-3,-4,-4,-3,-4,-1,0,0,0,1,-3,-3,13,-3,-3,1,0,0,0,1,0,0,-2,12,12,-2,0,0,1,0",
			"name": "Array T(n, k), n, k \u003e 0, read by antidiagonals; the balanced ternary representation of T(n, k) is obtained by multiplying componentwise the digits in the balanced ternary representations of n and of k.",
			"comment": [
				"For any k \u003e= 0, n -\u003e T(n, k) is 3^A134021(k)-periodic.",
				"The zeros of the table form a Vicsek fractal (see illustration in Links section)."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A343316/b343316.txt\"\u003eTable of n, a(n) for n = 0..10010\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A343316/a343316.png\"\u003eColored representation of the table for n, k \u003c 3^6\u003c/a\u003e (where the color denotes the sign of T(n, k): red for positive values, blue for negative values, white for zeros)",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Vicsek_fractal\"\u003eViczek fractal\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = T(k, n).",
				"T(m, T(n, k)) = T(T(m, n), k).",
				"T(n, 0) = 0.",
				"T(n, 1) = A102283(n).",
				"T(n, n) = A060374(n)."
			],
			"example": [
				"Array T(n, k) begins:",
				"  n\\k|  0   1   2   3   4   5   6   7   8  9  10  11  12",
				"  ---+--------------------------------------------------",
				"    0|  0   0   0   0   0   0   0   0   0  0   0   0   0",
				"    1|  0   1  -1   0   1  -1   0   1  -1  0   1  -1   0",
				"    2|  0  -1   4   3   2  -2  -3  -4   1  0  -1   4   3",
				"    3|  0   0   3   3   3  -3  -3  -3   0  0   0   3   3",
				"    4|  0   1   2   3   4  -4  -3  -2  -1  0   1   2   3",
				"    5|  0  -1  -2  -3  -4  13  12  11  10  9   8   7   6",
				"    6|  0   0  -3  -3  -3  12  12  12   9  9   9   6   6",
				"    7|  0   1  -4  -3  -2  11  12  13   8  9  10   5   6",
				"    8|  0  -1   1   0  -1  10   9   8  10  9   8  10   9",
				"    9|  0   0   0   0   0   9   9   9   9  9   9   9   9",
				"   10|  0   1  -1   0   1   8   9  10   8  9  10   8   9",
				"   11|  0  -1   4   3   2   7   6   5  10  9   8  13  12",
				"   12|  0   0   3   3   3   6   6   6   9  9   9  12  12",
				"Array T(n, k) begins in balanced ternary notation (with \"T\" instead of digits \"-1\"):",
				"  n\\k|  0  1  1T  10  11  1TT  1T0  1T1  10T  100  101  11T  110",
				"  ---+----------------------------------------------------------",
				"    0|  0  0   0   0   0    0    0    0    0    0    0    0    0",
				"    1|  0  1   T   0   1    T    0    1    T    0    1    T    0",
				"   1T|  0  T  11  10  1T   T1   T0   TT    1    0    T   11   10",
				"   10|  0  0  10  10  10   T0   T0   T0    0    0    0   10   10",
				"   11|  0  1  1T  10  11   TT   T0   T1    T    0    1   1T   10",
				"  1TT|  0  T  T1  T0  TT  111  110  11T  101  100  10T  1T1  1T0",
				"  1T0|  0  0  T0  T0  T0  110  110  110  100  100  100  1T0  1T0",
				"  1T1|  0  1  TT  T0  T1  11T  110  111  10T  100  101  1TT  1T0",
				"  10T|  0  T   1   0   T  101  100  10T  101  100  10T  101  100",
				"  100|  0  0   0   0   0  100  100  100  100  100  100  100  100",
				"  101|  0  1   T   0   1  10T  100  101  10T  100  101  10T  100",
				"  11T|  0  T  11  10  1T  1T1  1T0  1TT  101  100  10T  111  110",
				"  110|  0  0  10  10  10  1T0  1T0  1T0  100  100  100  110  110"
			],
			"program": [
				"(PARI) T(n,k) = { if (n==0 || k==0, return (0), my (d=centerlift(Mod(n,3)), t=centerlift(Mod(k,3))); d*t + 3*T((n-d)\\3, (k-t)\\3)) }"
			],
			"xref": [
				"Cf. A059095, A060374, A102283, A134021, A343317."
			],
			"keyword": "sign,tabl,base",
			"offset": "0,13",
			"author": "_Rémy Sigrist_, Apr 11 2021",
			"references": 3,
			"revision": 12,
			"time": "2021-04-12T01:36:47-04:00",
			"created": "2021-04-11T22:16:32-04:00"
		}
	]
}