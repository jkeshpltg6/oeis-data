{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A020985",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 20985,
			"data": "1,1,1,-1,1,1,-1,1,1,1,1,-1,-1,-1,1,-1,1,1,1,-1,1,1,-1,1,-1,-1,-1,1,1,1,-1,1,1,1,1,-1,1,1,-1,1,1,1,1,-1,-1,-1,1,-1,-1,-1,-1,1,-1,-1,1,-1,1,1,1,-1,-1,-1,1,-1,1,1,1,-1,1,1,-1,1,1,1,1,-1,-1,-1,1,-1,1",
			"name": "The Rudin-Shapiro or Golay-Rudin-Shapiro sequence (coefficients of the Shapiro polynomials).",
			"comment": [
				"Other names are the Rudin-Shapiro or Golay-Rudin-Shapiro infinite word.",
				"The Shapiro polynomials are defined by P_0 = Q_0 = 1; for n\u003e=0, P_{n+1} = P_n + x^(2^n)*Q_n, Q_{n+1} = P_n - x^(2^n)*Q_n. Then P_n = Sum_{m=0..2^n-1} a(m)*x^m, where the a(m) (the present sequence) do not depend on n. - _N. J. A. Sloane_, Aug 12 2016",
				"Related to paper-folding sequences - see the Mendès France and Tenenbaum article.",
				"a(A022155(n)) = -1; a(A203463(n)) = 1. - _Reinhard Zumkeller_, Jan 02 2012",
				"a(n) = 1 if and only if the numbers of 1's and runs of 1's in binary representation of n have the same parity: A010060(n) = A268411(n); otherwise, when A010060(n) = 1 - A268411(n), a(n) = -1. - _Vladimir Shevelev_, Feb 10 2016. Typo corrected and comment edited by _Antti Karttunen_, Jul 11 2017",
				"A word that is uniform primitive morphic, but not pure morphic. - _N. J. A. Sloane_, Jul 14 2018",
				"Named after the Austrian-American mathematician Walter Rudin (1921-2010), the mathematician Harold S. Shapiro (1928-2021) and the Swiss mathematician and physicist Marcel Jules Edouard Golay (1902-1989). - _Amiram Eldar_, Jun 13 2021"
			],
			"reference": [
				"Jean-Paul Allouche and Jeffrey Shallit, Automatic Sequences, Cambridge Univ. Press, 2003, p. 78 and many other pages."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A020985/b020985.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Jean-Paul Allouche, \u003ca href=\"http://ssdnm.mimuw.edu.pl/pliki/wyklady/allouche-uj.pdf\"\u003eLecture notes on automatic sequences\u003c/a\u003e, Krakow October 2013.",
				"Jean-Paul Allouche, Julien Cassaigne, Jeffrey Shallit, and Luca Q. Zamboni, \u003ca href=\"https://arxiv.org/abs/1711.10807\"\u003eA Taxonomy of Morphic Sequences\u003c/a\u003e, arXiv preprint arXiv:1711.10807 [cs.FL], Nov 29 2017",
				"Jean-Paul Allouche and M. Mendes France, \u003ca href=\"https://webusers.imj-prg.fr/~jean-paul.allouche/allmendeshouches.pdf\"\u003eAutomata and Automatic Sequences\u003c/a\u003e, in: Axel F. and Gratias D. (eds), Beyond Quasicrystals. Centre de Physique des Houches, Vol. 3, Springer, Berlin, Heidelberg, pp. 293-367, 1995; DOI https://doi.org/10.1007/978-3-662-03130-8_11.",
				"Jean-Paul Allouche and M. Mendes France, \u003ca href=\"/A003842/a003842.pdf\"\u003eAutomata and Automatic Sequences\u003c/a\u003e, in: Axel F. and Gratias D. (eds), Beyond Quasicrystals. Centre de Physique des Houches, vol 3. Springer, Berlin, Heidelberg, pp. 293-367, 1995; DOI https://doi.org/10.1007/978-3-662-03130-8_11. [Local copy]",
				"Jean-Paul Allouche and Jonathan Sondow, \u003ca href=\"http://arxiv.org/abs/1408.5770\"\u003eSummation of rational series twisted by strongly B-multiplicative coefficients\u003c/a\u003e, arXiv:1408.5770 [math.NT], 2014; Electron. J. Combin., 22 #1 (2015) P1.59; see pp.9-10.",
				"Joerg Arndt, \u003ca href=\"http://www.jjj.de/fxt/#fxtbook\"\u003eMatters Computational (The Fxtbook)\u003c/a\u003e, section 1.16.5 \"The Golay-Rudin-Shapiro sequence\", pp.44-45",
				"Scott Balchin and Dan Rust, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL20/Rust/rust3.html\"\u003eComputations for Symbolic Substitutions\u003c/a\u003e, Journal of Integer Sequences, Vol. 20 (2017), Article 17.4.1.",
				"John Brillhart and L. Carlitz, \u003ca href=\"https://doi.org/10.1090/S0002-9939-1970-0260955-6\"\u003eNote on the Shapiro polynomials\u003c/a\u003e, Proc. Amer. Math. Soc., Vol. 25 (1970), pp. 114-118.",
				"John Brillhart and Patrick Morton, \u003ca href=\"http://projecteuclid.org/euclid.ijm/1256048841\"\u003eÜber Summen von Rudin-Shapiroschen Koeffizienten\u003c/a\u003e, (German) Illinois J. Math., Vol. 22, No. 1 (1978), pp. 126-148. MR0476686 (57 #16245). - From _N. J. A. Sloane_, Jun 06 2012",
				"John Brillhart and Patrick Morton, \u003ca href=\"http://www.maa.org/programs/maa-awards/writing-awards/a-case-study-in-mathematical-research-the-golay-rudin-shapiro-sequence\"\u003eA case study in mathematical research: the Golay-Rudin-Shapiro sequence\u003c/a\u003e, Amer. Math. Monthly, Vol. 103 (1996) pp. 854-869.",
				"James D. Currie, Narad Rampersad, Kalle Saari, and Luca Q. Zamboni, \u003ca href=\"http://arxiv.org/abs/1301.4972\"\u003eExtremal words in morphic subshifts\u003c/a\u003e, arXiv:1301.4972 [math.CO], 2013.",
				"James D. Currie, Narad Rampersad, Kalle Saari, and Luca Q. Zamboni, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2014.01.002\"\u003eExtremal words in morphic subshifts\u003c/a\u003e, Discrete Math., Vol. 322 (2014), pp. 53-60. MR3164037. See Sect. 8.",
				"Michel Dekking, Michel Mendes France and Alf van der Poorten, \u003ca href=\"https://doi.org/10.1007/BF03024244\"\u003eFolds\u003c/a\u003e, The Mathematical Intelligencer, Vol. 4, No. 3 (1982), pp. 130-138.",
				"Michel Dekking, Michel Mendes France and Alf van der Poorten, \u003ca href=\"https://doi.org/10.1007/BF03023552\"\u003eFolds II. Symmetry disturbed\u003c/a\u003e, The Mathematical Intelligencer, Vol. 4, No. 4 (1982), pp. 173-181.",
				"Arturas Dubickas, \u003ca href=\"http://dx.doi.org/10.4064/ap105-2-3\"\u003eHeights of squares of Littlewood polynomials and infinite series\u003c/a\u003e, Ann. Polon. Math., Vol. 105 (2012), pp. 145-163. - From _N. J. A. Sloane_, Dec 16 2012",
				"Albertus Hof, Oliver Knill and Barry Simon, \u003ca href=\"http://inis.iaea.org/search/search.aspx?orig_q=RN:27016845\"\u003eSingular continuous spectrum for palindromic Schroedinger operators\u003c/a\u003e, Commun. Math. Phys., Vol. 174, No. 1 (1995), pp. 149-159.",
				"Philip Lafrance, Narad Rampersad and Randy Yee, \u003ca href=\"http://arxiv.org/abs/1408.2277\"\u003eSome properties of a Rudin-Shapiro-like sequence\u003c/a\u003e, arXiv:1408.2277 [math.CO], 2014.",
				"D. H. Lehmer and Emma Lehmer, \u003ca href=\"http://gdz.sub.uni-goettingen.de/dms/load/img/?PID=GDZPPN002197537\"\u003ePicturesque exponential sums. II\u003c/a\u003e, Journal für die reine und angewandte Mathematik, Vol. 318 (1980), pp. 1-19.",
				"Michel Mendès France and Gérald Tenenbaum, \u003ca href=\"http://www.numdam.org/item?id=BSMF_1981__109__207_0\"\u003eDimension des courbes planes, papiers pliés et suites de Rudin-Shapiro\u003c/a\u003e. (French) Bull. Soc. Math. France, Vol. 109, No. 2 (1981), pp. 207-215. MR0623789 (82k:10073).",
				"Luke Schaeffer and Jeffrey Shallit, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v23i1p25\"\u003eClosed, Palindromic, Rich, Privileged, Trapezoidal, and Balanced Words in Automatic Sequences\u003c/a\u003e, Electronic Journal of Combinatorics, Vol. 23, No. 1 (2016), #P1.25.",
				"Harold S. Shapiro, \u003ca href=\"http://hdl.handle.net/1721.1/12198\"\u003eExtremal problems for polynomials and power series\u003c/a\u003e, Ph.D. Diss. Massachusetts Institute of Technology, 1952.",
				"Vladimir Shevelev, \u003ca href=\"http://arxiv.org/abs/1603.04434\"\u003eTwo analogs of Thue-Morse sequence\u003c/a\u003e, arXiv:1603.04434 [math.NT], 2016.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Rudin-ShapiroSequence.html\"\u003eRudin-Shapiro Sequence\u003c/a\u003e."
			],
			"formula": [
				"a(0) = 1, a(2n) = a(n), a(2n+1) = a(n) * (-1)^n. [Brillhart and Carlitz, in proof of theorem 4]",
				"a(0) = a(1) = 1, a(2n) = a(n), a(2n+1) = a(n)*(1-2*(n AND 1)), where AND is the bitwise AND operator. - _Alex Ratushnyak_, May 13 2012",
				"Brillhart and Morton (1978) list many properties.",
				"a(n) = (-1)^A014081(n)  = (-1)^A020987(n) = 1-2*A020987(n). - _M. F. Hasler_, Jun 06 2012",
				"Sum(n \u003e= 1, a(n-1)(8n^2+4n+1)/(2n(2n+1)(4n+1)) = 1; see Allouche and Sondow, 2015. - _Jean-Paul Allouche_ and _Jonathan Sondow_, Mar 21 2015"
			],
			"maple": [
				"A020985 := proc(n) option remember; if n = 0 then 1 elif n mod 2 = 0 then A020985(n/2) else (-1)^((n-1)/2 )*A020985( (n-1)/2 ); fi; end;"
			],
			"mathematica": [
				"a[0] = 1; a[1] = 1; a[n_?EvenQ] := a[n] = a[n/2]; a[n_?OddQ] := a[n] = (-1)^((n-1)/2)* a[(n-1)/2]; a /@ Range[0, 80] (* _Jean-François Alcover_, Jul 05 2011 *)",
				"a[n_] := 1 - 2 Mod[Length[FixedPointList[BitAnd[#, # - 1] \u0026, BitAnd[n, Quotient[n, 2]]]], 2] (* _Jan Mangaldan_, Jul 23 2015 *)",
				"Array[RudinShapiro, 81, 0] (* _JungHwan Min_, Dec 22 2016 *)"
			],
			"program": [
				"(Haskell)",
				"a020985 n = a020985_list !! n",
				"a020985_list = 1 : 1 : f (tail a020985_list) (-1) where",
				"   f (x:xs) w = x : x*w : f xs (0 - w)",
				"-- _Reinhard Zumkeller_, Jan 02 2012",
				"(PARI) A020985(n)=(-1)^A014081(n)  \\\\ _M. F. Hasler_, Jun 06 2012",
				"(Python)",
				"def a014081(n): return sum([((n\u003e\u003ei)\u00263==3) for i in range(len(bin(n)[2:]) - 1)])",
				"def a(n): return (-1)**a014081(n) # _Indranil Ghosh_, Jun 03 2017"
			],
			"xref": [
				"Cf. A022155, A005943 (factor complexity), A014081.",
				"Cf. A020987 (0-1 version), A020986 (partial sums), A203531 (run lengths), A033999.",
				"Sequences mentioned in the Allouche et al. \"Taxonomy\" paper, listed by example number: 1: A003849, 2: A010060, 3: A010056, 4: A020985 and A020987, 5: A191818, 6: A316340 and A273129, 18: A316341, 19: A030302, 20: A063438, 21: A316342, 22: A316343, 23: A003849 minus its first term, 24: A316344, 25: A316345 and A316824, 26: A020985 and A020987, 27: A316825, 28: A159689, 29: A049320, 30: A003849, 31: A316826, 32: A316827, 33: A316828, 34: A316344, 35: A043529, 36: A316829, 37: A010060."
			],
			"keyword": "sign,nice,easy",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_",
			"references": 47,
			"revision": 160,
			"time": "2021-06-13T07:12:47-04:00",
			"created": "1998-06-14T03:00:00-04:00"
		}
	]
}