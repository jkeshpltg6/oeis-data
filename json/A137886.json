{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A137886",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 137886,
			"data": "12,144,3840,138240,6804000,436504320,35417088000,3546005299200,429451518988800,61883150757120000,10463789706751180800,2051763183437532364800,461802751261297205760000,118254166096501129863168000",
			"name": "Number of (directed) Hamiltonian paths in the n-crown graph.",
			"comment": [
				"The reference to A094047 arises in the formula because that sequence is also the number of directed Hamiltonian cycles in the n-crown graph. (Each cycle can be broken in 2n ways to give a path.) - _Andrew Howroyd_, Feb 21 2016",
				"Also, the number of ways of seating n married couples at 2*n chairs arranged side-by-side in a straight line, men and women in alternate positions, so that no husband is next to his wife. - _Andrew Howroyd_, Sep 19 2017"
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A137886/b137886.txt\"\u003eTable of n, a(n) for n = 3..253\u003c/a\u003e (terms 3..50 from Andrew Howroyd)",
				"Max Alekseyev, \u003ca href=\"http://home.gwu.edu/~maxal/gpscripts/\"\u003ePARI/GP Scripts for Miscellaneous Math Problems\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CrownGraph.html\"\u003eCrown Graph\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/HamiltonianPath.html\"\u003eHamiltonian Path\u003c/a\u003e"
			],
			"formula": [
				"For n\u003e3, a(n) = 2*n*A094047(n) + n*a(n-1) = A059375(n) + n*a(n-1). - _Andrew Howroyd_, Feb 21 2016",
				"a(n) ~ 4*Pi*n^(2*n+1) / exp(2*n+2). - _Vaclav Kotesovec_, Feb 25 2016",
				"a(n) = (n-1)*n*a(n-1) + (n-1)^2*n*a(n-2) + (n-2)*(n-1)*n*a(n-3). - _Vaclav Kotesovec_, Feb 25 2016",
				"a(n) = 2*n! * A000271(n). - _Andrew Howroyd_, Sep 19 2017"
			],
			"mathematica": [
				"Table[2 n! Sum[(-1)^(n - k) k! Binomial[n + k, 2 k], {k, 0, n}], {n, 3, 20}] (* _Eric W. Weisstein_, Sep 20 2017 *)",
				"Table[2 (-1)^n n! HypergeometricPFQ[{1, -n, n + 1}, {1/2}, 1/4], {n, 3, 20}] (* _Eric W. Weisstein_, Sep 20 2017 *)"
			],
			"program": [
				"(PARI) /* needs the routine nhp() from the Alekseyev link */",
				"{ A137886(n) = nhp( matrix(2*n,2*n,i,j, if(min(i,j)\u003c=n \u0026\u0026 max(i,j)\u003en \u0026\u0026 abs(j-i)!=n, 1, 0)) ) }"
			],
			"xref": [
				"Cf. A000271, A059375, A094047, A259212, A270174."
			],
			"keyword": "nonn",
			"offset": "3,1",
			"author": "_Eric W. Weisstein_, Feb 20 2008",
			"ext": [
				"More terms from _Max Alekseyev_, Feb 13 2009",
				"a(14) from _Eric W. Weisstein_, Jan 15 2014",
				"a(15)-a(16) from _Andrew Howroyd_, Feb 21 2016"
			],
			"references": 3,
			"revision": 40,
			"time": "2020-01-18T06:37:09-05:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}