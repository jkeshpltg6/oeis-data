{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A157013",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 157013,
			"data": "1,1,-1,1,-4,1,1,-15,5,-1,1,-58,10,-6,1,1,-229,-66,-26,7,-1,1,-912,-1017,-288,23,-8,1,1,-3643,-8733,-4779,-415,-41,9,-1,1,-14566,-61880,-63606,-17242,-1158,40,-10,1,1,-58257,-396796,-691036,-375118,-60990,-1956,-60,11,-1",
			"name": "Riordan's general Eulerian recursion: T(n, k) = (k+2)*T(n-1, k) + (n-k-1) * T(n-1, k-1) with T(n,1) = 1, T(n,n) = (-1)^(n-1).",
			"comment": [
				"Row sums are: {1, 0, -2, -10, -52, -314, -2200, -17602, -158420, -1584202,...}.",
				"This recursion set doesn't seem to produce the Eulerian 2nd A008517.",
				"The Mathematica code gives ten sequences of which the first few are in the OEIS (see Crossrefs section). - _G. C. Greubel_, Feb 22 2019"
			],
			"reference": [
				"J. Riordan, An Introduction to Combinatorial Analysis, Wiley, 1958, pp. 214-215"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A157013/b157013.txt\"\u003eRows n=1..100 of triangle, flattned\u003c/a\u003e"
			],
			"formula": [
				"e(n,k,m)= (k+m)*e(n-1, k, m) + (n-k+1-m)*e(n-1, k-1, m) with m=3.",
				"T(n, k) = (k+2)*T(n-1, k) + (n-k-1)*T(n-1, k-1) with T(n,1) = 1, T(n,n) = (-1)^(n-1). - _G. C. Greubel_, Feb 22 2019"
			],
			"example": [
				"Triangle begins with:",
				"1.",
				"1,     -1.",
				"1,     -4,       1.",
				"1,    -15,       5,      -1.",
				"1,    -58,      10,      -6,       1.",
				"1,   -229,     -66,     -26,       7,     -1.",
				"1,   -912,   -1017,    -288,      23,     -8,     1.",
				"1,  -3643,   -8733,   -4779,    -415,    -41,     9,   -1.",
				"1, -14566,  -61880,  -63606,  -17242,  -1158,    40,  -10,   1.",
				"1, -58257, -396796, -691036, -375118, -60990, -1956,  -60,  11,  -1."
			],
			"mathematica": [
				"e[n_, 0, m_]:= 1;",
				"e[n_, k_, m_]:= 0 /; k \u003e= n;",
				"e[n_, k_, m_]:= (k+m)*e[n-1, k, m] + (n-k+1-m)*e[n-1, k-1, m];",
				"Table[Flatten[Table[Table[e[n, k, m], {k,0,n-1}], {n,1,10}]], {m,0,10}]",
				"T[n_,1]:=1; T[n_,n_]:=(-1)^(n-1); T[n_,k_]:= T[n,k] = (k+2)*T[n-1,k] + (n-k-1)*T[n-1,k-1]; Table[T[n,k], {n,1,10}, {k,1,n}]//Flatten (* _G. C. Greubel_, Feb 22 2019 *)"
			],
			"program": [
				"(PARI) {T(n, k) = if(k==1, 1, if(k==n, (-1)^(n-1), (k+2)*T(n-1, k) + (n-k-1)* T(n-1, k-1)))};",
				"for(n=1, 10, for(k=1, n, print1(T(n, k), \", \"))) \\\\ _G. C. Greubel_, Feb 22 2019",
				"(Sage)",
				"def T(n, k):",
				"    if (k==1): return 1",
				"    elif (k==n): return (-1)^(n-1)",
				"    else: return (k+2)*T(n-1, k) + (n-k-1)* T(n-1, k-1)",
				"[[T(n, k) for k in (1..n)] for n in (1..10)] # _G. C. Greubel_, Feb 22 2019"
			],
			"xref": [
				"Cf. A008517.",
				"Cf. A157011 (m=0), A008292 (m=1), A157012 (m=2), This Sequence (m=3)."
			],
			"keyword": "sign,tabl",
			"offset": "1,5",
			"author": "_Roger L. Bagula_, Feb 21 2009",
			"references": 4,
			"revision": 6,
			"time": "2019-02-22T20:14:44-05:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}