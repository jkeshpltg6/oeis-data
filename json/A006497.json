{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006497",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6497,
			"id": "M0910",
			"data": "2,3,11,36,119,393,1298,4287,14159,46764,154451,510117,1684802,5564523,18378371,60699636,200477279,662131473,2186871698,7222746567,23855111399,78788080764,260219353691,859446141837,2838557779202",
			"name": "a(n) = 3*a(n-1) + a(n-2) with a(0) = 2, a(1) = 3.",
			"comment": [
				"For more information about this type of recurrence follow the Khovanova link and see A086902 and A054413. - _Johannes W. Meijer_, Jun 12 2010"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A006497/b006497.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"P. Bhadouria, D. Jhala, B. Singh, \u003ca href=\"http://dx.doi.org/10.22436/jmcs.08.01.07\"\u003eBinomial Transforms of the k-Lucas Sequences and its [sic] Properties\u003c/a\u003e, Journal of Mathematics and Computer Science (JMCS), Volume 8, Issue 1, Pages 81-92, Sequence L_{3,n}.",
				"A. F. Horadam, \u003ca href=\"http://www.fq.math.ca/Scanned/15-4/horadam.pdf\"\u003eGenerating identities for generalized Fibonacci and Lucas triples\u003c/a\u003e, Fib. Quart., 15 (1977), 289-292.",
				"Haruo Hosoya, \u003ca href=\"http://www.hyle.org/journal/issues/19-1/hosoya.htm\"\u003eWhat Can Mathematical Chemistry Contribute to the Development of Mathematics?\u003c/a\u003e, HYLE--International Journal for Philosophy of Chemistry, Vol. 19, No.1 (2013), pp. 87-105.",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"Pablo Lam-Estrada, Myriam Rosalía Maldonado-Ramírez, José Luis López-Bonilla, Fausto Jarquín-Zárate, \u003ca href=\"https://arxiv.org/abs/1904.13002\"\u003eThe sequences of Fibonacci and Lucas for each real quadratic fields Q(Sqrt(d))\u003c/a\u003e, arXiv:1904.13002 [math.NT], 2019.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"\u003ca href=\"/index/Rea#recur1\"\u003eIndex entries for recurrences a(n) = k*a(n - 1) +/- a(n - 2)\u003c/a\u003e",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,1)."
			],
			"formula": [
				"G.f.: (2-3*x)/(1-3*x-x^2). - _Simon Plouffe_ in his 1992 dissertation",
				"From _Gary W. Adamson_, Jun 15 2003: (Start)",
				"a(n) = ((3 + sqrt(13))/2)^n + ((3 - sqrt(13))/2)^n.",
				"A006190(n-2) + A006190(n) = a(n-1).",
				"a(n)^2 - 13*A006190(n)^2 = 4(-1)^n. (End)",
				"From _Paul Barry_, Nov 15 2003: (Start)",
				"E.g.f.: 2*exp(3*x/2)*cosh(sqrt(13)*x/2).",
				"a(n) = 2^(1-n)*Sum_{k=0..floor(n/2)} C(n, 2*k)* (13)^k * 3^(n-2*k).",
				"a(n) = 2*T(n, 3i/2)*(-i)^n with T(n, x) Chebyshev's polynomials of the first kind (see A053120) and i^2=-1. (End)",
				"From _Hieronymus Fischer_, Jan 02 2009: (Start)",
				"fract(((3+sqrt(13))/2)^n)) = (1/2)*(1+(-1)^n) - (-1)^n*((3+sqrt(13))/2)^(-n) = (1/2)*(1+(-1)^n) - ((3-sqrt(13))/2)^n.",
				"See A001622 for a general formula concerning the fractional parts of powers of numbers x\u003e1, which satisfy x-x^(-1)=floor(x).",
				"a(n) = round(((3+sqrt(13))/2)^n) for n \u003e 0. (End)",
				"From _Johannes W. Meijer_, Jun 12 2010: (Start)",
				"a(2n+1) = 3*A097783(n), a(2n) = A057076(n).",
				"a(3n+1) = A041018(5n), a(3n+2) = A041018(5n+3) and a(3n+3) = 2*A041018(5n+4).",
				"Limit_{k -\u003e infinity} a(n+k)/a(k) = (a(n) + A006190(n)*sqrt(13))/2.",
				"Limit_{n -\u003e infinity} a(n)/A006190(n) = sqrt(13).",
				"(End)",
				"a(n) = sqrt(13*(A006190(n))^2 + 4*(-1)^n). - _Vladimir Shevelev_, Mar 13 2013",
				"G.f.: G(0), where G(k) = 1 + 1/(1 - (x*(13*k-9))/((x*(13*k+4)) - 6/G(k+1))); (continued fraction). - _Sergei N. Gladkovskii_, Jun 15 2013",
				"a(n) = [x^n] ( (1 + 3*x + sqrt(1 + 6*x + 13*x^2))/2 )^n for n \u003e= 1. - _Peter Bala_, Jun 23 2015",
				"a(n) = Lucas(n,3), Lucas polynomials, L(n,x), evaluated at x=3. - _G. C. Greubel_, Jun 06 2019"
			],
			"maple": [
				"a:= n-\u003e (\u003c\u003c0|1\u003e, \u003c1|3\u003e\u003e^n. \u003c\u003c2, 3\u003e\u003e)[1, 1]:",
				"seq(a(n), n=0..30);  # _Alois P. Heinz_, Jan 26 2018"
			],
			"mathematica": [
				"Table[LucasL[n, 3], {n, 0, 30}] (* _Zerinvary Lajos_, Jul 09 2009 *)",
				"LucasL[Range[0, 30], 3] (* _Eric W. Weisstein_, Apr 17 2018 *)",
				"LinearRecurrence[{3,1},{2,3},30] (* _Harvey P. Dale_, Feb 17 2020 *)"
			],
			"program": [
				"(Sage) [lucas_number2(n,3,-1) for n in range(0, 30)] # _Zerinvary Lajos_, Apr 30 2009",
				"(MAGMA) [ n eq 1 select 2 else n eq 2 select 3 else 3*Self(n-1)+Self(n-2): n in [1..30] ]; // _Vincenzo Librandi_, Aug 20 2011",
				"(Haskell)",
				"a006497 n = a006497_list !! n",
				"a006497_list = 2 : 3 : zipWith (+) (map (* 3) $ tail a006497_list) a006497_list",
				"-- _Reinhard Zumkeller_, Feb 19 2011",
				"(PARI) my(x='x+O('x^30)); Vec((2-3*x)/(1-3*x-x^2)) \\\\ _G. C. Greubel_, Jul 05 2017",
				"(PARI) apply( {A006497(n)=[2,3]*([0,1;1,3]^n)[,1]}, [0..30]) \\\\ _M. F. Hasler_, Mar 06 2020"
			],
			"xref": [
				"Cf. A006190, A100230, A001622, A014176, A080039, A098316."
			],
			"keyword": "nonn,easy,changed",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Definition completed by _M. F. Hasler_, Mar 06 2020"
			],
			"references": 44,
			"revision": 117,
			"time": "2022-01-15T00:00:32-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}