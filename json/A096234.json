{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A096234",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 96234,
			"data": "0,-1,1,-1,2,-1,3,-1,4,-1,5,10,6,11,7,12,8,13,9,14,-1,15,20,16,21,17,22,18,23,19,24,-1,25,30,26,31,27,32,28,33,29,34,-1,35,40,36,41,37,42,38,43,39,44,-1,45,50,46,51,47,52,48,53,49,54,-1,55,60,56,61,57,62",
			"name": "Base 10 \"digit addition generators\": a(n) = smallest m such that m + (sum of digits of m) = n, or -1 if no such m exists.",
			"comment": [
				"The -1 entries in this sequence correspond to A003052, the self numbers.",
				"This sequence has several terms in common with A025804 (as long as we match -1's with 0's), the expansion of 1/((1-x^2)(1-x^4)(1-x^9)). a(25) to a(34) of that sequence match a(10) to a(19) of the present sequence.",
				"There are 102 -1's in the first 1000 terms here and 983 -1's in the first 10000 terms. - _Harvey P. Dale_, Feb 22 2016"
			],
			"link": [
				"Harvey P. Dale, \u003ca href=\"/A096234/b096234.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Max A. Alekseyev, Donovan Johnson and N. J. A. Sloane, \u003ca href=\"http://neilsloane.com/doc/colombian12302021.pdf\"\u003eOn Kaprekar's Junction Numbers\u003c/a\u003e, Preprint, 2021.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/DigitadditionGenerator.html\"\u003eDigit Addition Generator\u003c/a\u003e."
			],
			"example": [
				"a(0) = 0 because 0 + 0 = 0",
				"a(1) = -1 because there is no nonnegative integer that added to its digits-sum results in 1",
				"a(29) = 19 because 19 + (1 + 9) = 29",
				"a(30) = 24 because 24 + (2 + 4) = 30",
				"a(31) = -1 because there is no integer that added to its digits-sum results in 31"
			],
			"mathematica": [
				"msodm[n_]:=Module[{m=n-9*IntegerLength[n]},While[m+Total[ IntegerDigits[ m]] != n\u0026\u0026m\u003cn,m++]; If[m+Total[IntegerDigits[m]]==n,m,-1]]; Array[ msodm,80,0] (* _Harvey P. Dale_, Feb 22 2016, updated Dec 31 2021 *)"
			],
			"program": [
				"/* PARI: Gen(n,b) returns a list of the generators of n in base b. Written by _Max Alekseyev_ (see Alekseyev et al., 2021).",
				"For example, Gen(101,10) returns [91, 101]. - _N. J. A. Sloane_, Jan 02 2022 */",
				"{ Gen(u,b=10) = my(d,m,k);",
				"  if(u\u003c0 || u==1, return([]); );",
				"  if(u==0, return([0]); );",
				"  d = #digits(u,b)-1;",
				"  m = u\\b^d;",
				"  while( sumdigits(m,b) \u003e u - m*b^d,",
				"    m--;",
				"    if(m==0, m=b-1; d--; );",
				"  );",
				"  k = u - m*b^d - sumdigits(m,b);",
				"  vecsort( concat( apply(x-\u003ex+m*b^d,Gen(k,b)),",
				"                   apply(x-\u003em*b^d-1-x,Gen((b-1)*d-k-2,b)) ) );",
				"}"
			],
			"xref": [
				"Cf. A003052, A025804."
			],
			"keyword": "base,sign,changed",
			"offset": "0,5",
			"author": "_Alonso del Arte_, Aug 09 2004",
			"ext": [
				"Typo in definition corrected by _Harvey P. Dale_, Feb 22 2016",
				"Edited. Escape clause value changed from 0 to -1, and a(0) = 0 added. - _N. J. A. Sloane_, Dec 31 2021"
			],
			"references": 1,
			"revision": 29,
			"time": "2022-01-08T14:51:27-05:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}