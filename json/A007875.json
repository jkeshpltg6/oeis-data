{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007875",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7875,
			"data": "1,1,1,1,1,2,1,1,1,2,1,2,1,2,2,1,1,2,1,2,2,2,1,2,1,2,1,2,1,4,1,1,2,2,2,2,1,2,2,2,1,4,1,2,2,2,1,2,1,2,2,2,1,2,2,2,2,2,1,4,1,2,2,1,2,4,1,2,2,4,1,2,1,2,2,2,2,4,1,2,1,2,1,4,2,2,2,2,1,4",
			"name": "Number of ways of writing n as p*q, with p \u003c= q, gcd(p, q) = 1.",
			"comment": [
				"a(n), n \u003e= 2, is the number of divisor products in the numerator as well as denominator of the unique representation of n in terms of divisor products. See the W. Lang link under A007955, where a(n)=l(n) in Table 1. - _Wolfdieter Lang_, Feb 08 2011",
				"Record values are the binary powers, occurring at primorial positions except at 2: a(A002110(0))=A000079(0), a(A002110(n+1))=A000079(n) for n \u003e 0. - _Reinhard Zumkeller_, Aug 24 2011",
				"For n \u003e 1: a(n) = (A000005(n) - A048105(n)) / 2; number of ones in row n of triangle in A225817. - _Reinhard Zumkeller_, Jul 30 2013"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A007875/b007875.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Larry Bates and Peter Gibson, \u003ca href=\"http://arxiv.org/abs/1603.06622\"\u003eA geometry where everything is better than nice\u003c/a\u003e, arXiv:1603.06622 [math.DG], (21-March-2016); see page 2.",
				"Vaclav Kotesovec, \u003ca href=\"/A007875/a007875.jpg\"\u003eGraph - the asymptotic ratio\u003c/a\u003e"
			],
			"formula": [
				"a(n) = (1/2)*Sum_{ d divides n } abs(mu(d)) = 2^(A001221(n)-1) = A034444(n)/2, n \u003e 1. - _Vladeta Jovovic_, Jan 25 2002",
				"a(n) = phi(2^omega(n)) = A000010(2^A001221(n)). - _Enrique Pérez Herrero_, Apr 10 2012",
				"Sum_{k=1..n} a(k) ~ 3*n*((log(n) + (2*gamma - 1))/ Pi^2 - 12*(Zeta'(2)/Pi^4)), where gamma is the Euler-Mascheroni constant A001620. Equivalently, Sum_{k=1..n} a(k) ~ 3*n*(log(n) + 24*log(A) - 1 - 2*log(2*Pi)) / Pi^2, where A is the Glaisher-Kinkelin constant A074962. - _Vaclav Kotesovec_, Jan 30 2019",
				"a(n) = Sum_{d|n} mu(d) * A018892(n/d). - _Daniel Suteu_, Jan 08 2021"
			],
			"maple": [
				"A007875 := proc(n)",
				"    if n = 1 then",
				"        1;",
				"    else",
				"        2^(A001221(n)-1) ;",
				"    end if;",
				"end proc: # _R. J. Mathar_, May 28 2016"
			],
			"mathematica": [
				"a[n_] := With[{r = Reduce[1 \u003c= p \u003c= q \u003c= n \u0026\u0026 n == p*q \u0026\u0026 GCD[p, q] == 1, {p, q}, Integers]}, If[Head[r] === And, 1, Length[r]]]; Table[a[n], {n, 1, 90}] (* _Jean-François Alcover_, Nov 02 2011 *)",
				"a[n_] := EulerPhi[2^PrimeNu[n]]; Array[a, 105] (* _Robert G. Wilson v_, Apr 10 2012 *)",
				"a[n_] := Sum[If[Mod[n, k] == 0, Re[Sqrt[MoebiusMu[k]]], 0], {k, 1, n}] (* _Mats Granvik_, Aug 10 2018 *)"
			],
			"program": [
				"(Haskell)",
				"a007875 = length . filter (\u003e 0) . a225817_row",
				"-- _Reinhard Zumkeller_, Jul 30 2013, Aug 24 2011",
				"(PARI) a(n)=ceil((1\u003c\u003comega(n))/2) \\\\ _Charles R Greathouse IV_, Nov 02 2011"
			],
			"xref": [
				"Cf. A000005, A000079, A001221, A001620, A002110, A007955, A034444, A048105, A074962."
			],
			"keyword": "nonn,nice,easy",
			"offset": "1,6",
			"author": "Victor Ufnarovski",
			"references": 18,
			"revision": 73,
			"time": "2021-01-21T11:36:48-05:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}