{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A291793",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 291793,
			"data": "2,6,6,6,0,10,28,6,10,6,6,6,0,0,6,28,10,6,10,6,6,0,6,6,0,6,6,6,6,6,6,52,6,6,6,6,6,6,6,6,6,6,6,28,6,0,0,28,6,6,6,6,6,0,6,6,6,10,6,6,6,6,0,6,0,6,6,6,6,0,6,6,6,0,6,6,6,0,10,0,10,6,6",
			"name": "Period of orbit of Post's tag system applied to the word (100)^n (version 2), or -1 if the orbit increases without limit.",
			"comment": [
				"Post's tag system maps a word w over {0,1} to w', where if w begins with 0, w' is obtained by appending 00 to w and deleting the first three letters, or if w begins with 1, w' is obtained by appending 1101 to w and deleting the first three letters.",
				"The empty word is included in the count.",
				"Here, following Asveld, a(n)=0 if the orbit ends at the empty word. On the other hand, Shallit defines a(n) to be 1 if that happens, which gives a different sequence, A284121.",
				"From _A.H.M. Smeets_, Jul 16 2020: (Start)",
				"In general a tag as defined by Emil Leon Post, is given by a 4-tuple (Sigma,AF,n,w0), where Sigma is some (nonempty) alphabet, AF is the associated function (sometimes also called set of production rules) AF: Sigma -\u003e Sigma*, n is the deletion number and w0 the initial string.",
				"Here, the period lengths a(n) refer to the tags ({0,1},{(0,00),(1,1101)},3,100^n).",
				"a(n) is an even number. Proof: for each cycle the number of associations (productions) 0 -\u003e 00 must equal the number of associations (productions) 1 -\u003e 1101 applied within a cycle. (End)"
			],
			"link": [
				"Lars Blomberg, \u003ca href=\"/A291793/b291793.txt\"\u003eTable of n, a(n) for n = 1..6075\u003c/a\u003e (corrected for n=165 by _A.H.M. Smeets_)",
				"Peter R. J. Asveld, \u003ca href=\"http://doc.utwente.nl/66184/1/1988m20.pdf\"\u003eOn a Post's System of Tag\u003c/a\u003e. Bulletin of the EATCS 36 (1988), 96-102.",
				"Lars Blomberg, \u003ca href=\"/A291793/a291793.png\"\u003eHistogram over non-zero terms\u003c/a\u003e",
				"Emil L. Post, \u003ca href=\"http://www.lib.ysu.am/articles_art/63062f3ed126193beb426becc0fbbe33.pdf\"\u003eFormal reductions of the general combinatorial decision problem.\u003c/a\u003e, American Journal of Mathematics, Vol. 65, No. 2 (Apr., 1943), pp. 197-215.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/TagSystem.html\"\u003eTag System\u003c/a\u003e"
			],
			"example": [
				"For n = 2 the orbit of (100)^2 = 100100 consists of a preperiod of length 15, followed by a periodic portion of length 6."
			],
			"program": [
				"(Python)",
				"def step(w):",
				"    i = 0",
				"    while w[0] != alfabet[i]:",
				"        i = i+1",
				"    w = w+suffix[i]",
				"    return w[n:len(w)]",
				"alfabet, suffix, n, ws, w0, m = \"01\", [\"00\",\"1101\"], 3, \"100\", \"\", 0",
				"while m \u003c 83:",
				"    w0, m = w0+ws, m+1",
				"    w, ww, i, a = w0, w0, 0, 0",
				"    while w != \"\" and a == 0:",
				"        w, i = step(w), i+1",
				"        if i%1000 == 0:",
				"            ww = w",
				"        else:",
				"            if w == ww or w == \"\":",
				"                if w != \"\":",
				"                    a = i%1000",
				"                print(m,a) # _A.H.M. Smeets_, Jul 16 2020"
			],
			"xref": [
				"Cf. A284116, A284119, A291792, A284121, A336287, A336327."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_, Sep 04 2017, based on _Jeffrey Shallit_'s A284121.",
			"ext": [
				"a(50)-a(83) from _Lars Blomberg_, Sep 08 2017"
			],
			"references": 10,
			"revision": 49,
			"time": "2021-01-08T20:30:11-05:00",
			"created": "2017-09-04T15:01:35-04:00"
		}
	]
}