{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A045945",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 45945,
			"data": "0,12,42,90,156,240,342,462,600,756,930,1122,1332,1560,1806,2070,2352,2652,2970,3306,3660,4032,4422,4830,5256,5700,6162,6642,7140,7656,8190,8742,9312,9900,10506,11130,11772,12432,13110,13806,14520,15252,16002",
			"name": "Hexagonal matchstick numbers: a(n) = 3*n*(3*n+1).",
			"comment": [
				"This may also be construed as the number of line segments illustrating the isometric projection of a cube of side length n. Moreover, a(n) equals the number of rods making a cube of side length n+1 minus the number of rods making a cube of side length n. See the illustration in the links and formula below."
			],
			"link": [
				"Ivan Panchenko, \u003ca href=\"/A045945/b045945.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Peter M. Chema, \u003ca href=\"/A045945/a045945.pdf\"\u003eIllustration of initial terms as the first difference of number of rods required to make a 3-D cube\u003c/a\u003e.",
				"Craig Knecht, \u003ca href=\"/A045945/a045945.png\"\u003eNumber of positions a frame shifted H1 hexagon can occupy in a hexagon of order n\u003c/a\u003e.",
				"Amelia Carolina Sparavigna, \u003ca href=\"https://doi.org/10.5281/zenodo.3471358\"\u003eThe groupoids of Mersenne, Fermat, Cullen, Woodall and other Numbers and their representations by means of integer sequences\u003c/a\u003e, Politecnico di Torino, Italy (2019), [math.NT].",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-3,1)."
			],
			"formula": [
				"a(n) = a(n-1) + 6*(3*n-1) (with a(0)=0). - _Vincenzo Librandi_, Nov 18 2010",
				"G.f.: 6*x*(2+x)/(1-x)^3. - _Colin Barker_, Feb 12 2012",
				"a(n) = 6*A005449(n). - _R. J. Mathar_, Feb 13 2016",
				"a(n) = A059986(n) - A059986(n-1). - _Peter M. Chema_, Feb 26 2017",
				"a(n) = 6*(A000217(n) + A000290(n)). - _Peter M. Chema_, Mar 26 2017",
				"From _Amiram Eldar_, Jan 14 2021: (Start)",
				"Sum_{n\u003e=1} 1/a(n) = 1 - Pi/(6*sqrt(3)) - log(3)/2.",
				"Sum_{n\u003e=1} (-1)^(n+1)/a(n) = -1 + Pi/(3*sqrt(3)) + 2*log(2)/3. (End)"
			],
			"maple": [
				"a:= n-\u003e 3*n*(3*n+1): seq(a(n), n=0..42); # _Zerinvary Lajos_, May 03 2007"
			],
			"mathematica": [
				"f[n_]:=3*n*(3*n+1);f[Range[0,60]] (* _Vladimir Joseph Stephan Orlovsky_, Feb 05 2011 *)"
			],
			"program": [
				"(PARI) a(n) = 3*n*(3*n+1) \\\\ _Charles R Greathouse IV_, Feb 27 2017",
				"(Python) def a(n): return 3*n*(3*n+1) # _Indranil Ghosh_, Mar 26 2017"
			],
			"xref": [
				"Cf. A033580, A045946, A059986.",
				"The hexagon matchstick sequences are: Number of matchsticks: this sequence; size=1 triangles: A033581; larger triangles: A307253; total triangles: A045949.  Analog for triangles: A045943; analog for stars: A045946. - _John King_, Apr 05 2019"
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_R. K. Guy_",
			"references": 11,
			"revision": 79,
			"time": "2021-01-14T04:39:36-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}