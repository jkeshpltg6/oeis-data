{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A118340",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 118340,
			"data": "1,1,0,1,1,0,1,2,1,0,1,3,4,1,0,1,4,9,5,1,0,1,5,15,20,6,1,0,1,6,22,48,28,7,1,0,1,7,30,85,113,37,8,1,0,1,8,39,132,282,169,47,9,1,0,1,9,49,190,519,688,237,58,10,1,0,1,10,60,260,837,1762,1074,318,70,11,1,0",
			"name": "Pendular triangle, read by rows, where row n is formed from row n-1 by the recurrence: if n \u003e 2k, T(n,k) = T(n,n-k) + T(n-1,k), else T(n,k) = T(n,n-1-k) + T(n-1,k), for n\u003e=k\u003e0, with T(n,0) = 1 and T(n,n) = 0^n.",
			"comment": [
				"Definitions. A pendular triangle is a triangle in which row n is generated from the pendular sums of row n-1. Pendular sums of a row are partial sums taken in back-and-forth order, starting with the leftmost term, jumping to the rightmost term, back to the leftmost unused term, then forward to the rightmost unused term, etc.",
				"In each pass, the partial sum is placed in the new row directly under the term most recently used in the sum. Continue in this way until all the terms of the prior row have been used and then complete the new row by appending a zero at the end. Pendular sums are so named because the process resembles a swinging pendulum that slows down on each pass until it eventually comes to rest in the center.",
				"In the simplest case, pendular triangles obey the recurrence: if n \u003e 2k, T(n,k) = T(n,n-k) + T(n-1,k), else T(n,k) = T(n,n-1-k) + p*T(n-1,k), for n\u003e=k\u003e0, with T(n,0)=1 and T(n,n)=0^n, for some fixed number p.",
				"In which case the g.f. G=G(x) of the central terms satisfies: G = 1 - p*x*G + p*x*G^2 + x*G^3.",
				"More generally, a pendular triangle is defined by the recurrence: if n \u003e 2k, T(n,k) = T(n,n-k) + T(n-1,k), else T(n,k) = T(n,n-1-k) + Sum_{j\u003e=1} p(j)*T(n-1,k-1+j), for n\u003e=k\u003e0, with T(n,0)=1 and T(n,n)=0^n.",
				"Remarkably, the g.f. G=G(x) of the central terms satisfies: G = 1 + x*G^3 + Sum_{j\u003e=1} p(j)*x^j*[G^(2*j) - G^(2*j-1)].",
				"Further, the g.f. of the m-th lower semi-diagonal equals G(x)^(m+1) for m\u003e=0, where the m-th semi-diagonal consists of those terms located at m rows below the central terms.",
				"For variants of pendular triangles, the main diagonal may be nonzero, but then the g.f.s of the semi-diagonals are more complex."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A118340/b118340.txt\"\u003eRows n = 0..100 of the triangle, flattened\u003c/a\u003e(Rows n = 0..20 from Paul D. Hanna)"
			],
			"formula": [
				"T(2*n+m,n) = [A108447^(m+1)](n), i.e., the m-th lower semi-diagonal forms the self-convolution (m+1)-power of A108447; compare semi-diagonals to the diagonals of convolution triangle A118343."
			],
			"example": [
				"Row 6 equals the pendular sums of row 5:",
				"  [1,  4,  9,  5,  1,  0], where the sums proceed as follows:",
				"  [1, __, __, __, __, __], T(6,0) = T(5,0) = 1;",
				"  [1, __, __, __, __,  1], T(6,5) = T(6,0) + T(5,5) = 1 + 0 = 1;",
				"  [1,  5, __, __, __,  1], T(6,1) = T(6,5) + T(5,1) = 1 + 4 = 5;",
				"  [1,  5, __, __,  6,  1], T(6,4) = T(6,1) + T(5,4) = 5 + 1 = 6;",
				"  [1,  5, 15, __,  6,  1], T(6,2) = T(6,4) + T(5,2) = 6 + 9 = 15;",
				"  [1,  5, 15, 20,  6,  1], T(6,3) = T(6,2) + T(5,3) = 15 + 5 = 20;",
				"  [1,  5, 15, 20,  6,  1, 0] finally, append a zero to obtain row 6.",
				"Triangle begins:",
				"  1;",
				"  1,  0;",
				"  1,  1,  0;",
				"  1,  2,  1,   0;",
				"  1,  3,  4,   1,    0;",
				"  1,  4,  9,   5,    1,    0;",
				"  1,  5, 15,  20,    6,    1,    0;",
				"  1,  6, 22,  48,   28,    7,    1,    0;",
				"  1,  7, 30,  85,  113,   37,    8,    1,   0;",
				"  1,  8, 39, 132,  282,  169,   47,    9,   1,  0;",
				"  1,  9, 49, 190,  519,  688,  237,   58,  10,  1,  0;",
				"  1, 10, 60, 260,  837, 1762, 1074,  318,  70, 11,  1, 0;",
				"  1, 11, 72, 343, 1250, 3330, 4404, 1568, 413, 83, 12, 1, 0; ...",
				"Central terms are T(2*n,n) = A108447(n);",
				"semi-diagonals form successive self-convolutions of the central terms:",
				"T(2*n+1,n) = A118341(n) = [A108447^2](n),",
				"T(2*n+2,n) = A118342(n) = [A108447^3](n)."
			],
			"mathematica": [
				"T[n_, k_, p_]:= T[n,k,p] = If[n\u003ck || k\u003c0, 0, If[k==0, 1, If[k==n, 0, If[n\u003c=2*k, T[n,n-k-1,p] + p*T[n-1,k,p], T[n,n-k,p] + T[n-1,k,p] ]]]];",
				"Table[T[n,k,1], {n,0,12}, {k,0,n}]//Flatten (* _G. C. Greubel_, Feb 17 2021 *)"
			],
			"program": [
				"(PARI) {T(n,k) = if(n\u003ck || k\u003c0,0, if(k==0,1, if(n==k,0, if(n\u003e2*k, T(n-1,k) + T(n,n-k), T(n-1,k) + T(n,n-1-k)))))}",
				"for(n=0,12, for(k=0,n, print1(T(n,k),\", \"));print(\"\"))",
				"(Sage)",
				"@CachedFunction",
				"def T(n, k, p):",
				"    if (k\u003c0 or n\u003ck): return 0",
				"    elif (k==0): return 1",
				"    elif (k==n): return 0",
				"    elif (n\u003e2*k): return T(n,n-k,p) + T(n-1,k,p)",
				"    else: return T(n, n-k-1, p) + p*T(n-1, k, p)",
				"flatten([[T(n,k,1) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, Feb 17 2021",
				"(Magma)",
				"function T(n,k,p)",
				"  if k lt 0 or n lt k then return 0;",
				"  elif k eq 0 then return 1;",
				"  elif k eq n then return 0;",
				"  elif n gt 2*k then return T(n,n-k,p) + T(n-1,k,p);",
				"  else return T(n,n-k-1,p) + p*T(n-1,k,p);",
				"  end if;",
				"  return T;",
				"end function;",
				"[T(n,k,1): k in [0..n], n in [0..12]]; // _G. C. Greubel_, Feb 17 2021"
			],
			"xref": [
				"Cf. A108447 (central terms), A118341, A118343.",
				"variants: A118344 (Catalan), A118362 (ternary), A118350, A118355.",
				"Cf. A167763 (p=0), this sequence (p=1), A118345 (p=2), A118350 (p=3)."
			],
			"keyword": "nonn,tabl",
			"offset": "0,8",
			"author": "_Paul D. Hanna_, Apr 25 2006",
			"references": 9,
			"revision": 30,
			"time": "2021-02-18T00:28:43-05:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}