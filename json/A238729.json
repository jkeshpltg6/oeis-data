{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A238729",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 238729,
			"data": "2,3,2,5,4,7,2,3,4,11,4,13,4,6,2,17,5,19,4,6,4,23,4,5,4,3,4,29,8,31,2,6,4,10,5,37,4,6,4,41,8,43,4,6,4,47,4,7,6,6,4,53,5,10,4,6,4,59,8,61,4,6,2,10,8,67,4,6,8,71,5,73,4,8,4,14,8,79,4,3",
			"name": "Maximum flow of a number.",
			"comment": [
				"F(n) is the maximal flow in a network whose nodes are the divisors of n, with an edge from a to b if and only if b/a is a prime factor of n, in which case the capacity of the edge is b/a.  The source of the network is 1 and the sink is n."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A238729/b238729.txt\"\u003eTable of n, a(n) for n = 2..10000\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Maximum_flow_problem\"\u003eMaximum flow problem\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Max-flow_min-cut_theorem\"\u003eMax-flow min-cut theorem\u003c/a\u003e"
			],
			"formula": [
				"a(n) = min_{S:P([m])\\{}} Product_{i:[m]\\S} (e_i+1) * Sum_{i:S} p_i, where n = Product_{i=1..m} p_i^e_i and P([m]) is the powerset of {1,...,m}."
			],
			"maple": [
				"with(combinat):",
				"a:= proc(n) local S, s, f, l, m;",
				"      f:= infinity; l:= ifactors(n)[2]; m:= nops(l);",
				"      S:= subsets({$1..m}):",
				"      while not S[finished] do s:= S[nextvalue]();",
				"        if s={} then next fi:",
				"        f:= min(f, mul(1+l[i][2], i=({$1..m} minus s))*add(l[i][1], i=s))",
				"      od; f",
				"    end:",
				"seq(a(n), n=2..100);  # _Alois P. Heinz_, Mar 04 2014"
			],
			"mathematica": [
				"F[n_] := F[n] = Module[{v, e, flowgraph, flow},",
				"   v = Divisors[n];",
				"   e = Apply[DirectedEdge,",
				"     Select[Subsets[v, {2}], PrimeQ[Last[#]/First[#]] \u0026], {1}];",
				"   flowgraph =",
				"    Graph[e, EdgeCapacity -\u003e Map[Rule[#, (Divide @@ Reverse[#])] \u0026, e]];",
				"   flow = FindMaximumFlow[flowgraph, 1, n, \"OptimumFlowData\"];",
				"   flow[\"FlowValue\"]]"
			],
			"keyword": "nonn",
			"offset": "2,1",
			"author": "_Ken Levasseur_, Mar 03 2014",
			"references": 1,
			"revision": 16,
			"time": "2014-03-06T12:44:55-05:00",
			"created": "2014-03-06T10:50:03-05:00"
		}
	]
}