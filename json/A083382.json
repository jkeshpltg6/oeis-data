{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A083382",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 83382,
			"data": "0,1,1,1,1,1,1,1,2,1,1,1,1,1,2,2,1,1,1,1,2,2,2,2,2,3,2,3,3,2,2,3,3,3,3,3,2,2,2,2,3,4,3,3,4,3,3,4,3,3,4,4,4,3,3,4,5,4,3,4,5,4,5,4,5,5,5,5,5,5,5,5,5,5,6,7,6,4,5,6,6,5,6,6,6,6,6,7,7,6,6,6,7,7,7,7,6,6,7,7,7",
			"name": "Write the numbers from 1 to n^2 consecutively in n rows of length n; a(n) = minimal number of primes in a row.",
			"comment": [
				"Conjectured by Schinzel (Hypothesis H2) to be always positive for n \u003e 1.",
				"The conjecture has been verified for n = prime \u003c 790000 by Aguilar.",
				"If this is true, then Legendre's conjecture is true as well. (See A014085). - _Antti Karttunen_, Jan 01 2019"
			],
			"reference": [
				"P. Ribenboim, The New Book of Prime Number Records, Chapter 6.",
				"P. Ribenboim, The Little Book Of Big Primes, Springer-Verlag, NY 1991, page 185."
			],
			"link": [
				"Charles R Greathouse IV, \u003ca href=\"/A083382/b083382.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Carlos Rivera, \u003ca href=\"http://www.primepuzzles.net/conjectures/conj_026.htm\"\u003eThe calendar-like square conjecture\u003c/a\u003e",
				"A. Schinzel and W. Sierpinski, \u003ca href=\"http://matwbn.icm.edu.pl/ksiazki/aa/aa4/aa432.pdf\"\u003eSur certaines hypothèses concernant les nombres premiers\u003c/a\u003e, Acta Arithmetica 4 (1958), 185-208; erratum 5 (1958) p. 259."
			],
			"example": [
				"For n = 3 the array is",
				"1 2 3 (2 primes)",
				"4 5 6 (1 prime)",
				"7 8 9 (1 prime)",
				"so a(3) = 1"
			],
			"maple": [
				"A083382 := proc(n) local t1,t2,at; t1 := n; at := 0; for i from 1 to n do t2 := 0; for j from 1 to n do at := at+1; if isprime(at) then t2 := t2+1; fi; od; if t2 \u003c t1 then t1 := t2; fi; od; t1; end;"
			],
			"mathematica": [
				"Table[minP=n; Do[s=0; Do[If[PrimeQ[c+(r-1)*n], s++ ], {c, n}]; minP=Min[s, minP], {r, n}]; minP, {n, 100}]",
				"Table[Min[Count[#,_?PrimeQ]\u0026/@Partition[Range[n^2],n]],{n,110}] (* _Harvey P. Dale_, May 29 2013 *)"
			],
			"program": [
				"(Haskell)",
				"a083382 n = f n n a010051_list where",
				"   f m 0 _     = m",
				"   f m k chips = f (min m $ sum chin) (k - 1) chips' where",
				"     (chin,chips') = splitAt n chips",
				"-- _Reinhard Zumkeller_, Jun 10 2012",
				"(PARI) A083382(n) = { my(m=-1); for(i=0,n-1,my(s=sum(j=(i*n),((i+1)*n)-1,isprime(1+j))); if((m\u003c0) || (s \u003c m), m = s)); (m); }; \\\\ _Antti Karttunen_, Jan 01 2019"
			],
			"xref": [
				"A084927 generalizes this to three dimensions.",
				"Cf. A083415, A083383, A066888, A092556, A092557. See A083414 for primes in columns.",
				"Cf. A139326.",
				"Cf. A000720, A010051, A014085."
			],
			"keyword": "nonn,nice",
			"offset": "1,9",
			"author": "_James Propp_, Jun 05 2003",
			"ext": [
				"Edited by _Charles R Greathouse IV_, Jul 07 2010"
			],
			"references": 12,
			"revision": 24,
			"time": "2019-01-02T11:54:12-05:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}