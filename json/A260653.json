{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A260653",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 260653,
			"data": "1,2,3,4,6,8,12,15,16,18,20,24,30,32,36,40,42,48,54,56,60,64,72,80,84,90,96,100,105,108,112,120,126,128,132,140,144,150,156,160,162,165,168,176,180,192,195,198,200,208,210,216,220,224,234",
			"name": "Phi-practical numbers: integers n such that x^n - 1 has divisors of every degree up to n.",
			"comment": [
				"n is phi-practical if and only if each natural number up to n is a subsum of the multiset {phi(d) : d | n} (see Pomerance link p. 2).",
				"There are 6 terms up to 10, 28 up to 10^2, 174 up to 10^3, 1198 up to 10^4, 9301 up to 10^5, 74461 up to 10^6, 635528 up to 10^7, 5525973 up to 10^8, and 48386047 up to 10^9. - _Charles R Greathouse IV_, Nov 13 2015",
				"There are 431320394 terms up to 10^10. - _Charles R Greathouse IV_, Sep 19 2017",
				"Let F(x) denote the number of phi-practical numbers up to x. F(x) has order of magnitude x/log(x) (See Thompson 2012).  Moreover, we have F(x) = c*x/log(x) + O(x/(log(x))^2), where 0.945 \u003c c \u003c 0.967 (See Pomerance, Thompson \u0026 Weingartner 2016 and Weingartner 2019).  As a result, a(n) = k*n*log(n*log(n)) + O(n), where k = 1/c and 1.034 \u003c k \u003c 1.059. - _Andreas Weingartner_, Jun 29 2021"
			],
			"link": [
				"Charles R Greathouse IV, \u003ca href=\"/A260653/b260653.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Carl Pomerance, Lola Thompson, and Andreas Weingartner, \u003ca href=\"https://www.impan.pl/en/publishing-house/journals-and-series/acta-arithmetica/all/175/3/91743/on-integers-n-for-which-x-n-1-has-a-divisor-of-every-degree\"\u003eOn integers n for which X^n-1 has a divisor of every degree\u003c/a\u003e, Acta Arithmetica 175 (2016), 225-243; \u003ca href=\"https://arxiv.org/abs/1511.03357\"\u003earXiv preprint\u003c/a\u003e, arXiv:1511.03357 [math.NT], 2015.",
				"Lola Thompson, \u003ca href=\"http://arxiv.org/abs/1111.5401\"\u003ePolynomials with divisors of every degree\u003c/a\u003e, arXiv:1111.5401 [math.NT], 2011.",
				"Lola Thompson, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2011.10.005\"\u003ePolynomials with divisors of every degree\u003c/a\u003e, Journal of Number Theory 132 (2012), pp. 1038-1053.",
				"Lola Thompson, \u003ca href=\"http://arxiv.org/abs/1206.4355\"\u003eVariations on a question concerning the degrees of divisors of x^n-1\u003c/a\u003e, arXiv:1206.4355 [math.NT], 2012.",
				"Andreas Weingartner, \u003ca href=\"https://doi.org/10.1090/mcom/3402\"\u003eOn the constant factor in several related asymptotic estimates\u003c/a\u003e, Math. Comp., Vol. 88, No. 318 (2019), pp. 1883-1902; \u003ca href=\"https://arxiv.org/abs/1705.06349\"\u003earXiv preprint\u003c/a\u003e, arXiv:1705.06349 [math.NT], 2017-2018."
			],
			"formula": [
				"Pomerance, Thompson, \u0026 Weingartner show that a(n) ~ kn log n for some constant k, strengthening an earlier result of Thompson. The former give a heuristic suggesting that k is about 1/0.96. - _Charles R Greathouse IV_, Nov 13 2015",
				"More precisely, a(n) = k*n*log(n*log(n)) + O(n), where 1.034 \u003c k \u003c 1.059 (See comments). - _Andreas Weingartner_, Jun 29 2021"
			],
			"example": [
				"For n = 3, the divisors of x^3 - 1 are 1, x - 1, x^2 + x + 1, x^3 - 1, so 3 is a term.",
				"For n = 5, the divisors of x^5 - 1 are 1, x - 1, x^4 + x^3 + x^2 + x + 1, x^5 - 1, so 5 is not a term."
			],
			"mathematica": [
				"PhiPracticalQ[n_] := If[n\u003c1, False, If[n==1, True, (lst=Sort@EulerPhi[Divisors[n]]; ok=True; Do[If[lst[[m]]\u003eSum[lst[[l]], {l, 1, m-1}]+1, (ok=False; Break[])], {m, 1, Length[lst]}]; ok)]]; Select[Range[1000], PhiPracticalQ] (* _Frank M Jackson_, Jan 21 2016 *)"
			],
			"program": [
				"(PARI) isok(n)=vd = divisors(x^n-1); for (k=1, n, ok = 0; for (j=1, #vd, if (poldegree(vd[j])==k, ok = 1; break);); if (!ok, break);); ok;",
				"(PARI) is(n)=#Set(apply(poldegree, divisors('x^n-1)))==n+1 \\\\ _Charles R Greathouse IV_, Nov 13 2015",
				"(PARI) is(n)=my(u=List(),f=factor(n),t,s=1); forvec(v=vector(#f~,i,[0,f[i,2]]), t=prod(i=1,#v, if(v[i],(f[i,1]-1)*f[i,1]^(v[i]-1),1)); listput(u,t)); listsort(u); for(i=1,#u, if(u[i]\u003es, return(0)); s+=u[i]); 1 \\\\ _Charles R Greathouse IV_, Nov 13 2015"
			],
			"xref": [
				"Subsequence of A171581.",
				"Cf. A005153, A102190."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Michel Marcus_, Nov 13 2015",
			"ext": [
				"a(29)-a(55) from _Charles R Greathouse IV_, Nov 13 2015"
			],
			"references": 10,
			"revision": 42,
			"time": "2021-06-30T02:34:26-04:00",
			"created": "2015-11-13T20:03:22-05:00"
		}
	]
}