{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A188014",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 188014,
			"data": "0,1,0,0,1,0,1,0,0,1,0,1,1,0,1,0,0,1,0,1,0,0,1,0,1,1,0,1,0,0,1,0,1,1,0,1,0,0,1,0,1,0,0,1,0,1,1,0,1,0,0,1,0,1,0,0,1,0,1,1,0,1,0,0,1,0,1,1,0,1,0,0,1,0,1,0,0,1,0,1,1,0,1,0,0,1,0,1,1,0,1,0,0,1,0,1,0,0,1,0,1,1,0,1,0,0,1,0,1,0,0,1,0,1,1,0,1,0,0,1,0,1,1,0",
			"name": "a(n) = [nr]-[nr-kr]-[kr], where r=(1+sqrt(5))/2, k=4, [ ]=floor.",
			"comment": [
				"Suppose r is a positive irrational number and k is a positive integer, so that the sequence given by a(n)=[nr]-[nr-kr]-[kr] consists of zeros and ones. Let b(n)=(position of n-th 0) and c(n)=(position of n-th 1), so that b and c are a complementary pair of sequences.",
				"Examples of a, b, c using r=(1+sqrt(5))/2:",
				"  k=1:  a=A096270, b=A026352, c=A026351",
				"  k=2:  a=A188009, b=A188010, c=A101866",
				"  k=3:  a=A188011, b=A188012, c=A188013",
				"  k=4:  a=A188014, b=A188015, c=A188016",
				"Example using r=sqrt(2):",
				"  k=1:  a=A188037, b=A083088, c=A080754.",
				"Returning to arbitrary positive irrational r, let s(n)=[nr]-[nr-r]-[r], this being a(n) when k=1.  For k\u003e=2, the sequence a(n)=[nr]-[nr-kr]-[kr] is a shifted sum of shifted copies of s:",
				"  a(n)=s(n)+s(n-1)+...+s(n-k+1)-(constant).  This sum matches the sum s(n)+s(n+1)+...+s(n+k-1)-(constant) at A187950.",
				"Ratio of number of zeros to number of ones tends to sqrt(5)/2=1.118. Any (simple) proof? [_Zak Seidov_, Feb 14 2012] According to Charles R Greathouse IV (see formula), a(n)=1 if the fractional part of nr is less than w(1)=4r-6,and 0 otherwise, where r=(1+sqrt(5))/2. It means that probability of ones and zeros is, respectively, w(1) and w(0)=1-w(1). In the long run, number of ones and zeros is, respectively, w(1)*n and w(0)*n and, finally, the ratio of number of zeros to number of ones tends to w(0)/w(1)=sqrt(5)/2, qed. [_Zak Seidov_, Feb 17 2012]",
				"Essentially the same as A187950. - _Michel Dekking_, Sep 17 2016"
			],
			"link": [
				"Ivan Panchenko, \u003ca href=\"/A188014/b188014.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = [nr]-[nr-4r]-[4r], where r=(1+sqrt(5))/2.",
				"a(n) = 1 if the fractional part of nr is less than 4r - 6, and 0 otherwise. [_Charles R Greathouse IV_, Feb 14 2012]",
				"a(n+4) = A187950(n) for n\u003e=1. - _Michel Dekking_, Sep 17 2016"
			],
			"example": [
				"We get a=A188014, b=A188015, c=A188016 when",
				"r=(1+sqrt(5))/2 and k=4:",
				"a......0..1..0..0..1..0..1..0..0..1..0..1..1...",
				"b......1..3..4..6..8..9..11..14.. (positions of 0 in a)",
				"c......2..5..7..10..12..13..15... (positions of 1 in a).",
				"As noted in Comments, a(n)=[nr]-[nr-4r]-[4r] is also obtained in another way:  by adding right shifts of the infinite Fibonacci word s=A096270 (after left-extending it) and then down shifting:",
				"s(n)......0..1..0..1..1..0..1..0..1..1..0..1..1..",
				"s(n-1)....1..0..1..0..1..1..0..1..0..1..1..0..1..",
				"s(n-2)....1..1..0..1..0..1..1..0..1..0..1..1..0..",
				"s(n-3)....0..1..1..0..1..0..1..1..0..1..0..1..1..",
				"sum.......2..3..2..2..3..2..3..2..2..3..2..3..3..",
				"sum-2.....0..1..0..0..1..0..1..0..0..1..0..1..1..",
				"(Right shifts s(n-k) of the infinite Fibonacci word are obtained from s(n)=[nr]-[nr-nk]-[nk] rather than the morphism 0-\u003e01, 1-\u003e011 at A096270.  For an analogous example using left shifts, see A187950.)"
			],
			"mathematica": [
				"r=(1+5^(1/2))/2; k=4;",
				"t=Table[Floor[n*r]-Floor[(n-k)*r]-Floor[k*r], {n, 1, 220}]   (*A188014*)",
				"Flatten[Position[t, 0]]  (*A188015*)",
				"Flatten[Position[t, 1]]  (*A188016*)"
			],
			"program": [
				"(PARI) a(n)=my(r=(1+sqrt(5))/2);n*r-n*r\\1\u003c4*r-6 \\\\ _Charles R Greathouse IV_, Feb 14 2012",
				"(Python)",
				"from __future__ import division",
				"from gmpy2 import isqrt",
				"def A188014(n):",
				"    return int((isqrt(5*n**2)+n)//2 -(isqrt(5*(n-4)**2)+n)//2 - 4) if n \u003e 3 else 1-(n % 2) # _Chai Wah Wu_, Oct 07 2016"
			],
			"xref": [
				"Cf. A188015, A188016, A187950."
			],
			"keyword": "nonn",
			"offset": "1",
			"author": "_Clark Kimberling_, Mar 19 2011",
			"references": 80,
			"revision": 45,
			"time": "2016-10-08T16:58:04-04:00",
			"created": "2011-03-18T18:45:03-04:00"
		}
	]
}