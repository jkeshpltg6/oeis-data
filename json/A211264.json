{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A211264",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 211264,
			"data": "0,1,2,3,4,6,7,9,10,12,13,16,17,19,21,23,24,27,28,31,33,35,36,40,41,43,45,48,49,53,54,57,59,61,63,67,68,70,72,76,77,81,82,85,88,90,91,96,97,100,102,105,106,110,112,116,118,120,121,127,128,130,133,136",
			"name": "Number of integer pairs (x,y) such that 0 \u003c x \u003c y \u003c= n and x*y \u003c= n.",
			"comment": [
				"Partial sums of A056924.",
				"For a guide to related sequences, see A211266."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A211264/b211264.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = (1/2)*Sum_{i=1..n} (1 - A008836(i))*floor(n/i). - _Enrique Pérez Herrero_, Jul 10 2012 [Corrected by _Ridouane Oudra_, Oct 17 2019]",
				"From _Ridouane Oudra_, Oct 17 2019: (Start)",
				"a(n) = Sum_{i=1..n} (A001222(i) mod 2)*floor(n/i)",
				"a(n) = (1/2)*(A006218(n) - A000196(n)). (End)"
			],
			"maple": [
				"with(numtheory): seq(add((bigomega(i) mod 2)*floor(n/i), i=1..n), n=1..60); # _Ridouane Oudra_, Oct 17 2019",
				"# Alternative:",
				"ListTools:-PartialSums(map(t-\u003e floor(numtheory:-tau(t)/2), [$1..100])); # _Robert Israel_, Oct 18 2019"
			],
			"mathematica": [
				"a = 1; b = n; z1 = 120;",
				"t[n_] := t[n] = Flatten[Table[x*y, {x, a, b - 1},",
				"{y, x + 1, b}]]",
				"c[n_, k_] := c[n, k] = Count[t[n], k]",
				"Table[c[n, n], {n, 1, z1}]           (* A056924 *)",
				"Table[c[n, n + 1], {n, 1, z1}]       (* A211159 *)",
				"Table[c[n, 2*n], {n, 1, z1}]         (* A211261 *)",
				"Table[c[n, 3*n], {n, 1, z1}]         (* A211262 *)",
				"Table[c[n, Floor[n/2]], {n, 1, z1}]  (* A211263 *)",
				"Print",
				"c1[n_, m_] := c1[n, m] = Sum[c[n, k], {k, a, m}]",
				"Table[c1[n, n], {n, 1, z1}]          (* A211264 *)",
				"Table[c1[n, n + 1], {n, 1, z1}]      (* A211265 *)",
				"Table[c1[n, 2*n], {n, 1, z1}]        (* A211266 *)",
				"Table[c1[n, 3*n], {n, 1, z1}]        (* A211267 *)",
				"Table[c1[n, Floor[n/2]], {n, 1, z1}] (* A181972 *)"
			],
			"program": [
				"(MAGMA) [0] cat [\u0026+[(\u0026+[p[2]: p in Factorization(i)] mod 2) *Floor(n div i):i in [2..n] ]:n in [2..65]]; // _Marius A. Burtea_, Oct 17 2019",
				"(Python)",
				"from math import isqrt",
				"def A211264(n): return (lambda m: sum(n//k for k in range(1, m+1))-m*(m+1)//2)(isqrt(n)) # _Chai Wah Wu_, Oct 08 2021"
			],
			"xref": [
				"Cf. A211266, A001222, A006218, A000196."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Clark Kimberling_, Apr 06 2012",
			"references": 10,
			"revision": 24,
			"time": "2021-10-08T23:56:54-04:00",
			"created": "2012-04-11T18:49:43-04:00"
		}
	]
}