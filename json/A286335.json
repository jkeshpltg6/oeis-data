{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A286335",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 286335,
			"data": "1,1,0,1,1,0,1,2,1,0,1,3,3,2,0,1,4,6,6,2,0,1,5,10,13,9,3,0,1,6,15,24,24,14,4,0,1,7,21,40,51,42,22,5,0,1,8,28,62,95,100,73,32,6,0,1,9,36,91,162,206,190,120,46,8,0,1,10,45,128,259,384,425,344,192,66,10,0",
			"name": "Square array A(n,k), n\u003e=0, k\u003e=0, read by antidiagonals, where column k is the expansion of Product_{j\u003e=1} (1 + x^j)^k.",
			"comment": [
				"A(n,k) is the number of partitions of n into distinct parts (or odd parts) with k types of each part."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A286335/b286335.txt\"\u003eAntidiagonals n = 0..139, flattened\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/transforms.txt\"\u003eTransforms\u003c/a\u003e"
			],
			"formula": [
				"G.f. of column k: Product_{j\u003e=1} (1 + x^j)^k.",
				"A(n,k) = Sum_{i=0..k} binomial(k,i) * A308680(n,k-i). - _Alois P. Heinz_, Aug 29 2019"
			],
			"example": [
				"A(3,2) = 6 because we have [3], [3'], [2, 1], [2', 1], [2, 1'] and [2', 1'] (partitions of 3 into distinct parts with 2 types of each part).",
				"Also A(3,2) = 6 because we have [3], [3'], [1, 1, 1], [1, 1, 1'], [1, 1', 1'] and [1', 1', 1'] (partitions of 3 into odd parts with 2 types of each part).",
				"Square array begins:",
				"  1,  1,  1,   1,   1,   1,  ...",
				"  0,  1,  2,   3,   4,   5,  ...",
				"  0,  1,  3,   6,  10,  15,  ...",
				"  0,  2,  6,  13,  24,  40,  ...",
				"  0,  2,  9,  24,  51,  95,  ...",
				"  0,  3, 14,  42, 100, 206,  ..."
			],
			"maple": [
				"b:= proc(n, i, k) option remember; `if`(n=0, 1, `if`(i\u003c1, 0, add(",
				"     (t-\u003e b(t, min(t, i-1), k)*binomial(k, j))(n-i*j), j=0..n/i)))",
				"    end:",
				"A:= (n, k)-\u003e b(n$2, k):",
				"seq(seq(A(n, d-n), n=0..d), d=0..12);  # _Alois P. Heinz_, Aug 29 2019"
			],
			"mathematica": [
				"Table[Function[k, SeriesCoefficient[Product[(1 + x^i)^k , {i, Infinity}], {x, 0, n}]][j - n], {j, 0, 11}, {n, 0, j}] // Flatten"
			],
			"xref": [
				"Columns k=0-32 give: A000007, A000009, A022567-A022596.",
				"Rows n=0-2 give: A000012, A001477, A000217.",
				"Main diagonal gives A270913.",
				"Antidiagonal sums give A299106.",
				"Cf. A144064, A286352, A308680."
			],
			"keyword": "nonn,tabl",
			"offset": "0,8",
			"author": "_Ilya Gutkovskiy_, May 07 2017",
			"references": 35,
			"revision": 20,
			"time": "2019-09-03T15:23:46-04:00",
			"created": "2017-05-07T21:37:57-04:00"
		}
	]
}