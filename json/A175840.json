{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A175840",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 175840,
			"data": "1,3,2,9,6,4,27,18,12,8,81,54,36,24,16,243,162,108,72,48,32,729,486,324,216,144,96,64,2187,1458,972,648,432,288,192,128,6561,4374,2916,1944,1296,864,576,384,256,19683,13122,8748,5832,3888,2592,1728,1152,768,512",
			"name": "Mirror image of Nicomachus' table: T(n,k) = 3^(n-k)*2^k for n\u003e=0 and 0\u003c=k\u003c=n.",
			"comment": [
				"Lenstra calls these numbers the harmonic numbers of Philippe de Vitry (1291-1361). De Vitry wanted to find pairs of harmonic numbers that differ by one. Levi ben Gerson, also known as Gersonides, proved in 1342 that there are only four pairs with this property of the form 2^n*3^m. See also Peterson’s story ‘Medieval Harmony’.",
				"This triangle is the mirror image of Nicomachus' table A036561. The triangle sums, see the crossrefs, mirror those of A036561. See A180662 for the definitions of these sums."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A175840/b175840.txt\"\u003eRows n = 0..120 of triangle, flattened\u003c/a\u003e",
				"J. O'Connor and E.F. Robertson, \u003ca href=\"http://www-history.mcs.st-and.ac.uk/history/Biographies/Nicomachus.html\"\u003eNicomachus of Gerasa\u003c/a\u003e, The MacTutor History of Mathematics archive, 2010.",
				"Jay Kappraff, \u003ca href=\"http://www.nexusjournal.com\"\u003eThe Arithmetic of Nicomachus of Gerasa and its Applications to Systems of Proportion\u003c/a\u003e, Nexus Network Journal, vol. 2, no. 4 (October 2000).",
				"Hendrik Lenstra, \u003ca href=\"http://www.nieuwarchief.nl/serie5/pdf/naw5-2001-02-1-023.pdf\"\u003eAeternitatem Cogita\u003c/a\u003e, Nieuw Archief voor Wiskunde, 5/2, maart 2001, pp. 23-28.",
				"Ivars Peterson, \u003ca href=\"https://archive.is/iRXz\"\u003eMedieval Harmony\u003c/a\u003e, Math Trek, Mathematical Association of America, 1998."
			],
			"formula": [
				"T(n,k) = 3^(n-k)*2^k for n\u003e=0 and 0\u003c=k\u003c=n.",
				"T(n,n-k) = T(n,n-k+1) + T(n-1,n-k) for n\u003e=1 and 1\u003c=k\u003c=n with T(n,n) = 2^n for n\u003e=0."
			],
			"example": [
				"1;",
				"3, 2;",
				"9, 6, 4;",
				"27, 18, 12, 8;",
				"81, 54, 36, 24, 16;",
				"243, 162, 108, 72, 48, 32;"
			],
			"maple": [
				"A175840 := proc(n,k): 3^(n-k)*2^k end: seq(seq(A175840(n,k),k=0..n),n=0..9);"
			],
			"mathematica": [
				"Flatten[Table[3^(n-k) 2^k,{n,0,10},{k,0,n}]] (* _Harvey P. Dale_, May 08 2013 *)"
			],
			"program": [
				"(Haskell)",
				"a175840 n k = a175840_tabf !! n !! k",
				"a175840_row n = a175840_tabf !! n",
				"a175840_tabf = iterate (\\xs@(x:_) -\u003e x * 3 : map (* 2) xs) [1]",
				"-- _Reinhard Zumkeller_, Jun 08 2013"
			],
			"xref": [
				"Triangle sums: A001047 (Row1), A015441 (Row2), A016133 (Kn1 \u0026 Kn4), A005061 (Kn2 \u0026 Kn3), A016153 (Fi1\u0026 Fi2), A180844 (Ca1 \u0026 Ca4), A016140 (Ca2, Ca3), A180846 (Gi1 \u0026 Gi4), A180845 (Gi2 \u0026 Gi3), A016185 (Ze1 \u0026 Ze4), A180847 (Ze2 \u0026 Ze3).",
				"Cf. A000079, A000244, A000400, A003586."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "0,2",
			"author": "_Johannes W. Meijer_, Sep 21 2010, Jul 13 2011, Jun 03 2012",
			"references": 4,
			"revision": 22,
			"time": "2015-06-28T18:16:23-04:00",
			"created": "2010-10-02T03:00:00-04:00"
		}
	]
}