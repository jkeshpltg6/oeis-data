{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A101280",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 101280,
			"data": "1,1,1,2,1,8,1,22,16,1,52,136,1,114,720,272,1,240,3072,3968,1,494,11616,34304,7936,1,1004,40776,230144,176896,1,2026,136384,1328336,2265344,353792,1,4072,441568,6949952,21953408,11184128,1,8166,1398000,33981760",
			"name": "Triangle T(n,k) (n \u003e= 1, 0 \u003c= k \u003c= floor((n-1)/2)) read by rows, where T(n,k) = (k+1)T(n-1,k) + (2n-4k)T(n-1,k-1).",
			"comment": [
				"Row n has ceiling(n/2) terms.",
				"The paper by Shapiro et al. explains why T(n,k) is the number of permutations of n elements having k peaks and with the further property that every rise (ascent) is immediately followed by a peak. [That is, the permutation p_1 ... p_n has the further property that (j \u003e 1 and p_{j-1} \u003c p_j) implies (j \u003c n and p_j \u003e p_{j+1}).] For example, the T(4,1)=8 permutations in the case n=4, k=1 are 1423, 2143, 2431, 3142, 3241, 3421, 4231, 4132.",
				"A more elegant way to state this property: T(n,k) is the number of permutations of n objects with k descents such that every descent is a peak. The eight examples for n=4 and k=1 are now 1243, 1324, 1342, 1423, 2314, 2341, 2413, 3412.",
				"The rows of this triangle are the gamma vectors of the n-dimensional (type A) permutohedra (Postnikov et al., p. 31). Cf. A055151 and A089627. - _Peter Bala_, Oct 28 2008"
			],
			"reference": [
				"D. Foata and V. Strehl, \"Euler numbers and variations of permutations\", in Colloquio Internazionale sulle Teorie Combinatorie, Rome, September 1973, (Atti dei Convegni Lincei 17, Rome, 1976), 129.",
				"Guoniu Han, Frédéric Jouhet, Jiang Zeng, Two new triangles of q-integers via q-Eulerian polynomials of type A and B, Ramanujan J (2013) 31:115-127, DOI 10.1007/s11139-012-9389-3",
				"T. K. Petersen, Eulerian Numbers, Birkhauser, 2015, Chapter 4."
			],
			"link": [
				"Paul Barry, \u003ca href=\"https://arxiv.org/abs/1805.02274\"\u003eOn the f-Matrices of Pascal-like Triangles Defined by Riordan Arrays\u003c/a\u003e, arXiv:1805.02274 [math.CO], 2018.",
				"Bergeron, Ph. Flajolet and B. Salvy, \u003ca href=\"http://algo.inria.fr/flajolet/Publications/BeFlSa92.pdf\"\u003eVarieties of Increasing Trees\u003c/a\u003e, Lecture Notes in Computer Science vol. 581, ed. J.-C. Raoult, Springer 1992, pp. 24-48.",
				"L. Carlitz and Richard Scoville, \u003ca href=\"http://dx.doi.org/10.1515/crll.1974.265.110\"\u003eGeneralized Eulerian numbers: combinatorial applications\u003c/a\u003e, Journal für die reine und angewandte Mathematik 265 (1974), 111.",
				"Colin Defant, \u003ca href=\"https://arxiv.org/abs/2004.11367\"\u003eTroupes, Cumulants, and Stack-Sorting\u003c/a\u003e, arXiv:2004.11367 [math.CO], 2020.",
				"D. Dominici, \u003ca href=\"http://arxiv.org/abs/math/0501052\"\u003eNested derivatives: A simple method for computing series expansions of inverse functions\u003c/a\u003e, arXiv:math/0501052 [math.CA], 2005.",
				"D. Foata and M. P. Schützenberger, \u003ca href=\"http://arxiv.org/abs/math/0508232\"\u003eThéorie géometrique des polynômes eulériens\u003c/a\u003e, arXiv:math/0508232 [math.CO], 2005; Lecture Notes in Math. 138 (1970), 81-83.",
				"Shi-Mei Ma, \u003ca href=\"http://arxiv.org/abs/1304.6654\"\u003eOn gamma-vectors and the derivatives of the tangent and secant functions\u003c/a\u003e, arXiv:1304.6654 [math.CO], 2013.",
				"Shi-Mei Ma, Jun Ma, Yeong-Nan Yeh, \u003ca href=\"https://arxiv.org/abs/1802.02861\"\u003eOn certain combinatorial expansions of descent polynomials and the change of grammars\u003c/a\u003e, arXiv:1802.02861 [math.CO], 2018.",
				"Shi-Mei Ma, Yeong-Nan Yeh, \u003ca href=\"https://arxiv.org/abs/1904.11437\"\u003eThe alternating run polynomials of permutations\u003c/a\u003e, arXiv:1904.11437 [math.CO], 2019. See p. 4.",
				"A. Postnikov, V. Reiner, L. Williams, \u003ca href=\"http://arxiv.org/abs/math/0609184\"\u003eFaces of generalized permutohedra\u003c/a\u003e, arXiv:math/0609184 [math.CO], 2006-2007. [_Peter Bala_, Oct 28 2008]",
				"L. W. Shapiro, W.-J. Woan and S. Getu, \u003ca href=\"http://dx.doi.org/10.1137/0604046\"\u003eRuns, slides and moments\u003c/a\u003e, SIAM J. Alg. Discrete Methods, 4 (1983), 459-466."
			],
			"formula": [
				"G.f.: Sum_{n\u003e=1, k\u003e=0} T(n, k) t^k z^n/n! = C(t)(2-C(t))/(exp^(-z sqrt(1-4t)) + 1 - C(t)) - C(t), where the sum on k is actually finite, running up to ceiling(n/2) - 1; here C(t) = (1 - sqrt(1-4t))/2t is the generating function for the Catalan numbers (A000108).",
				"Sum_{k} Eulerian(n, k) x^k = Sum_{k} T(n, k) x^k (1+x)^(n-1-2k). E.g., 1 + 11x + 11x^2 + x^3 = (1+x)^3 + 8x(1+x).",
				"From _Peter Bala_, Jun 26 2012: (Start)",
				"T(n,k) = 2^k*A094503(n,k+1).",
				"Let r(t) = sqrt(1 - 2*t) and w(t) = (1 - r(t))/(1 + r(t)). Define F(t,z) = r(t)*(1 + w(t)*exp(r(t)*z))/(1 - w(t)*exp(r(t)*z)) = 1 + t*z + t*z^2/2! + (t+t^2)*z^3/3! + (t+4*t^2)*z^4/4! + ...; F(t,z) is the e.g.f. for A094503. The e.g.f. for the present table is A(t,z) := (F(2*t,z) - 1)/(2*t) = z + z^2/2! + (1+2*t)*z^3/3! + (1+8*t)*z^4/4! + ....",
				"The e.g.f. A(t,z) satisfies the autonomous partial differential equation dA/dz = 1 + A + t*A^2 with A(t,0) = 0. It follows that the inverse function A(t,z)^(-1) may be expressed as an integral: A(t,z)^(-1) = int {x = 0..z} 1/(1+x+t*x^2) dx.",
				"Applying [Dominici, Theorem 4.1] to invert the integral gives the following method for calculating the row polynomials R(n,t) of the table: let f(t,x) = 1+x+t*x^2 and let D be the operator f(t,x)*d/dx. Then R(n+1,t) = D^n(f(t,x)) evaluated at x = 0.",
				"By Bergeron et al., Theorem 1, the row polynomial R(n,t) is the generating function for rooted plane increasing 0-1-2 trees on n vertices, where the vertices of outdegree 2 have weight t and all other vertices have weight 1. An example is given below.",
				"Row sums A080635.",
				"(End)"
			],
			"example": [
				"Triangle begins:",
				"  1;",
				"  1,",
				"  1,   2;",
				"  1,   8,",
				"  1,  22,  16;",
				"  1,  52, 136,",
				"  1, 114, 720, 272;",
				"  ...",
				"From _Peter Bala_, Jun 2012: (Start)",
				"n = 4: the 9 weighted plane increasing 0-1-2 trees on 4 vertices are",
				"..................................................................",
				"..4...............................................................",
				"..|...............................................................",
				"..3..........4...4...............4...4...............3...3........",
				"..|........./.....\\............./.....\\............./.....\\.......",
				"..2....2...3.......3...2...3...2.......2...3...4...2.......2...4..",
				"..|.....\\./.........\\./.....\\./.........\\./.....\\./.........\\./...",
				"..1...(t)1........(t)1....(t)1........(t)1....(t)1........(t)1....",
				"..................................................................",
				"..3...4...4...3...................................................",
				"...\\./.....\\./....................................................",
				".(t)2....(t)2.....................................................",
				"....|.......|.....................................................",
				"....1.......1.....................................................",
				"Hence R(4,t) = 1 + 8*t.",
				"(End)"
			],
			"maple": [
				"T:=proc(n,k) if k\u003c0 then 0 elif n=1 and k=0 then 1 elif k\u003efloor((n-1)/2) then 0 else (k+1)*T(n-1,k)+(2*n-4*k)*T(n-1,k-1) fi end: for n from 1 to 13 do seq(T(n,k),k=0..floor((n-1)/2)) od; # yields sequence in triangular form # _Emeric Deutsch_, Aug 06 2005"
			],
			"mathematica": [
				"t[_, k_?Negative] = 0; t[1, 0] = 1; t[n_, k_] /; k \u003e (n-1)/2 = 0; t[n_, k_] := t[n, k] = (k+1)*t[n-1, k] + (2*n-4*k)*t[n-1, k-1]; Table[t[n, k], {n, 1, 13}, {k, 0, (n-1)/2}] // Flatten (* _Jean-François Alcover_, Nov 22 2012 *)"
			],
			"xref": [
				"The numbers 2^{n-1-k} T(n, k) form the array shown in A008303.",
				"Cf. A055151, A089627. - _Peter Bala_, Oct 28 2008",
				"Cf. A008292, A094503, A080635 (row sums)."
			],
			"keyword": "nonn,tabf,easy",
			"offset": "1,4",
			"author": "_Don Knuth_, Jan 28 2005",
			"ext": [
				"More terms from _Emeric Deutsch_, Aug 06 2005"
			],
			"references": 4,
			"revision": 44,
			"time": "2020-08-05T19:12:39-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}