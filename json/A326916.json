{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A326916",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 326916,
			"data": "0,11,14,31,28,51,10,13,34,95,190,247,312,385,244,133,242,239,376,301,372,233,370,295,232,173,228,367,230,171,226,223,358,285,220,355,282,217,352,283,218,115,44,73,20,71,40,17,36,15,18,3,12,1,22,75,46,117,48,77,24,79,50,81,118,221,286,225,292,229,296,451,298,235",
			"name": "Trajectory of the knight tour for choice of the square with the lowest digit, then closest to the origin, then first in the spiral.",
			"comment": [
				"A variant of Angelini's \"Kneil's Knumberphile Knight\", inspired by Sloane's \"The Trapped Knight\", cf. A316328 and links:",
				"Consider an infinite chess board with squares numbered along the infinite square spiral starting with 0 at the origin (as in A174344, A274923 and A296030). The squares are filled with successive digits of the integers: 0, 1, 2, ..., 9, 1, 0, 1, 1, ... (= A007376 starting with 0). The knight moves at each step to the yet unvisited square with the lowest digit on it, and in case of a tie, the one closest to the origin, first by Euclidean distance, then by appearance on the spiral (i.e., number of the square). This sequence lists the number of the square visited in the n-th move, if the knight starts at the origin, viz a(0) = 0.",
				"It turns out that following these rules, the knight gets trapped at the 1070th move, when he can't reach any unvisited square.",
				"See A326918 for the sequence of visited digits, given as A007376(a(n)).",
				"Many squares, e.g., 2: (1,1), 4: (-1,1), 5: (-1,0), 6: (-1,-1), 7: (0,-1), 8: (1,-1), 9: (2,-1), ..., will never be visited, even in the infinite extension of the sequence where the knight can move back if it gets trapped, in order to resume with a new unvisited square, as in A323809. - _M. F. Hasler_, Nov 08 2019"
			],
			"link": [
				"M. F. Hasler, \u003ca href=\"/A326916/b326916.txt\"\u003eTable of n, a(n) for n = 0..1069\u003c/a\u003e",
				"Eric Angelini, \u003ca href=\"https://cinquantesignes.blogspot.com/2019/05/kneils-knumberphile-knight.html\"\u003eKneil's Knumberphile Knight\u003c/a\u003e, Cinquante signes, May 04 2019.",
				"M. F. Hasler, \u003ca href=\"/wiki/Knight_tours\"\u003eKnight tours\u003c/a\u003e, OEIS wiki, Nov. 2019.",
				"N. J. A. Sloane and Brady Haran, \u003ca href=\"https://www.youtube.com/watch?v=RGQe8waGJ4w\"\u003eThe Trapped Knight\u003c/a\u003e, Numberphile video (2019)"
			],
			"program": [
				"(PARI) {L326916=List(0) /* list of terms */; U326916=1 /* bitmap of used squares */; local( K=vector(8, i, [(-1)^(i\\2)\u003c\u003c(i\u003e4), (-1)^i\u003c\u003c(i\u003c5)])/* knight moves*/, coords(n, m=sqrtint(n), k=m\\/2)=if(m\u003c=n-=4*k^2, [n-3*k, -k], n\u003e=0, [-k, k-n], n\u003e=-m, [-k-n, k], [k, 3*k+n]), pos(x, y)=if(y\u003e=abs(x), 4*y^2-y-x, -x\u003e=abs(y), 4*x^2-x-y, -y\u003e=abs(x), (4*y-3)*y+x, (4*x-3)*x+y), val(x, p=pos(x[1],x[2]))=if(bittest(U326916, p), oo, [A007376(p), norml2(x), p])); iferr( for(n=1,oo, my(x=coords(L326916[n])); U326916+=1\u003c\u003clistput(L326916, vecsort([val(x+D)|D\u003c-K])[1][3])), E, printf(\"Final square at index %d.\",#L326916-1)); A326916(n)=L326916[n+1]} \\\\ Requires function A007376; defines function A326916."
			],
			"xref": [
				"Cf. A326918; A316328, A316667; A326924, A326922; A328908, A328928; A328909, A328929; A326413, A328698.",
				"Cf. A174344, A274923, A296030; A007376."
			],
			"keyword": "nonn,base,fini,full",
			"offset": "0,2",
			"author": "_M. F. Hasler_, Oct 21 2019",
			"references": 11,
			"revision": 30,
			"time": "2019-11-10T20:04:39-05:00",
			"created": "2019-10-21T12:12:11-04:00"
		}
	]
}