{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A265345",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 265345,
			"data": "1,3,2,7,6,4,5,10,12,8,9,22,20,24,16,21,18,28,40,48,64,13,30,36,56,80,192,32,19,26,60,72,112,160,96,184,25,14,52,120,144,224,640,552,352,11,46,76,208,240,576,448,320,1056,704,15,58,68,136,104,480,288,1720,1600,2112,1408",
			"name": "Square array A(row,col): For row=0, A(0,col) = A265341(col), for row \u003e 0, A(row,col) = A265342(A(row-1,col)).",
			"comment": [
				"Square array A(row,col) is read by downwards antidiagonals as: A(0,0), A(0,1), A(1,0), A(0,2), A(1,1), A(2,0), A(0,3), A(1,2), A(2,1), A(3,0), ...",
				"All the terms in the same column are either all divisible by 3, or none of them are.",
				"Reducing A265342 to its constituent sequences gives A265342(n) = A263273(2*A263273(n)). Iterating this function k times starting from n reduces to (because A263273 is an involution, so pairs of them are canceled) to A263273((2^k)*A263273(n))."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A265345/b265345.txt\"\u003eTable of n, a(n) for n = 1..7381; the antidiagonals 0 .. 120 of the array\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"For row=0, A(0,col) = A265341(col), for row\u003e0, A(row,col) = A265342(A(row-1,col)).",
				"A(row, col) = A263273((2^row) * A263273(A265341(col))). [The above reduces to this.]"
			],
			"example": [
				"The top left corner of the array:",
				"    1,    3,    7,    5,    9,   21,   13,   19,   25,   11,   15,    39, .",
				"    2,    6,   10,   22,   18,   30,   26,   14,   46,   58,   66,    78, .",
				"    4,   12,   20,   28,   36,   60,   52,   76,   68,   44,   84,   156, .",
				"    8,   24,   40,   56,   72,  120,  208,  136,   88,  232,  168,   624, .",
				"   16,   48,   80,  112,  144,  240,  104,  200,  496,  424,  336,   312, .",
				"   64,  192,  160,  224,  576,  480,  520,  256,  344,  608,  672,  1560, .",
				"   32,   96,  640,  448,  288, 1920, 1144,  512, 1984,  736, 1344,  3432, .",
				"  184,  552,  320, 1720, 1656,  960, 2072, 1024, 1376, 4384, 5160,  6216, .",
				"  352, 1056, 1600,  824, 3168, 4800, 3712, 6040, 5344, 2936, 2472, 11136, .",
				"  ..."
			],
			"program": [
				"(Scheme)",
				"(define (A265345 n) (A265345bi (A002262 (+ -1 n)) (A025581 (+ -1 n)))) ;; o=1.",
				"(define (A265345bi row col) (A263273 (* (A000079 row) (A263273 (A265341 col))))) ;; Faster than below.",
				"(define (A265345bi row col) (if (= 0 row) (A265341 col) (A265342 (A265345bi (- row 1) col)))) ;; row\u003e=0, col\u003e=0."
			],
			"xref": [
				"Inverse: A265346.",
				"Transpose: A265347.",
				"Leftmost column: A264980.",
				"Topmost row: A265341.",
				"Row index: A265330 (zero-based), A265331 (one-based).",
				"Column index: A265910 (zero-based), A265911 (one-based).",
				"Cf. also A265342.",
				"Related permutations: A263273, A265895."
			],
			"keyword": "nonn,tabl,base",
			"offset": "1,2",
			"author": "_Antti Karttunen_, Dec 18 2015",
			"references": 10,
			"revision": 22,
			"time": "2016-09-20T13:26:19-04:00",
			"created": "2015-12-29T03:19:38-05:00"
		}
	]
}