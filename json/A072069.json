{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A072069",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 72069,
			"data": "2,4,0,0,6,4,0,0,4,4,0,0,2,8,0,0,12,8,0,0,16,12,0,0,10,16,0,0,12,20,0,0,16,4,0,0,12,12,0,0,14,20,0,0,20,8,0,0,4,20,0,0,8,12,0,0,24,8,0,0,14,8,0,0",
			"name": "Number of integer solutions to the equation 2x^2+y^2+32z^2=m for an odd number m=2n-1.",
			"comment": [
				"Related to primitive congruent numbers A006991.",
				"Assuming the Birch and Swinnerton-Dyer conjecture, the odd number 2n-1 is a congruent number if it is squarefree and 2 a(n) = A072068(n).",
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"reference": [
				"J. B. Tunnell, A classical Diophantine problem and modular forms of weight 3/2, Invent. Math., 72 (1983), 323-334."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A072069/b072069.txt\"\u003eTable of n, a(n) for n=1..10000\u003c/a\u003e",
				"Clay Mathematics Institute, \u003ca href=\"http://www.claymath.org/prizeproblems/birchsd.htm\"\u003eThe Birch and Swinnerton-Dyer Conjecture\u003c/a\u003e",
				"Department of Pure Maths., Univ. Sheffield, \u003ca href=\"http://www.shef.ac.uk/~puremath/theorems/congruent.html\"\u003ePythagorean triples and the congruent number problem\u003c/a\u003e",
				"Karl Rubin, \u003ca href=\"http://math.Stanford.EDU/~rubin/lectures/sumo/\"\u003eElliptic curves and right triangles\u003c/a\u003e"
			],
			"formula": [
				"Expansion of 2 * x * phi(x) * psi(x^4) * phi(x^16) in powers of x where phi(), psi() are Ramanujan theta functions. - _Michael Somos_, Jun 08 2012",
				"Expansion of 2 * q^(1/2) * eta(q^2)^5 * eta(q^8)^2 * eta(q^32)^5 / (eta(q)^2 * eta(q^4)^3 * eta(q^16)^2 * eta(q^64)^2) in powers of q. - _Michael Somos_, Dec 26 2019"
			],
			"example": [
				"a(2) = 4 because (1,1,0), (-1,1,0), (1,-1,0) and (-1,-1,0) are solutions when m=3.",
				"G.f. = 2*x + 4*x^2 + 6*x^5 + 4*x^6 + 4*x^9 + 4*x^10 + 2*x^13 + 8*x^14 + ... - _Michael Somos_, Dec 26 2019",
				"G.f. = 2*q + 4*q^3 + 6*q^9 + 4*q^11 + 4*q^17 + 4*q^19 + 2*q^25 + 8*q^27 + 12*q^33",
				"+ ..."
			],
			"mathematica": [
				"maxN=128; soln2=Table[0, {maxN/2}]; xMax=Ceiling[Sqrt[maxN/2]]; yMax=Ceiling[Sqrt[maxN]]; zMax=Ceiling[Sqrt[maxN/32]]; Do[n=2x^2+y^2+32z^2; If[OddQ[n]\u0026\u0026n\u003cmaxN, s=8; If[x==0, s=s/2]; If[y==0, s=s/2]; If[z==0, s=s/2]; soln2[[(n+1)/2]]+=s], {x, 0, xMax}, {y, 0, yMax}, {z, 0, zMax}]"
			],
			"program": [
				"(PARI) {a(n) = my(A); n--; if( n\u003c0, 0, A = x * O(x^n); polcoeff( 2 * eta(x^2 + A)^5 * eta(x^8 + A)^2 * eta(x^32 + A)^5 / (eta(x + A)^2 * eta(x^4 + A)^3 * eta(x^16 + A)^2 * eta(x^64 + A)^2), n))}; /* _Michael Somos_, Dec 26 2019 */"
			],
			"xref": [
				"Cf. A006991, A003273, A072068, A072070, A072071, A080918."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_T. D. Noe_, Jun 13 2002",
			"references": 9,
			"revision": 16,
			"time": "2019-12-26T13:41:24-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}