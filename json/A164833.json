{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A164833",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 164833,
			"data": "0,4,6,1,2,5,4,9,1,4,1,8,7,5,1,5,0,0,0,9,9,2,1,4,3,6,2,1,8,0,8,4,9,5,7,6,4,8,6,8,9,6,1,0,7,7,4,1,7,6,0,6,0,0,5,6,1,5,2,8,0,6,9,2,9,1,7,8,0,2,3,9,8,0,0,9,2,8,7,6,7,0,2,5,5,7,2,6,8,9,6,6,9,5,5,5,2,8,9,7,2,6,7,6,7,7,7,0,3,0,3,8,7,4,9,4,5,4,6",
			"name": "Decimal expansion of Pi/8 - log(2)/2.",
			"comment": [
				"Digits and formula given at Waldschmidt, p. 4."
			],
			"reference": [
				"Mohammad K. Azarian, Problem 1218, Pi Mu Epsilon Journal, Vol. 13, No. 2, Spring 2010, p. 116.  Solution published in Vol. 13, No. 3, Fall 2010, pp. 183-185.",
				"L. B. W. Jolley, Summation of series, Dover Publications Inc. (New York), 1961, p. 46 (series n. 251).",
				"A. J. Van Der Poorten, Effectively computable bounds for the solutions of certain Diophantine equations, Acta Arith., 33 (1977), pp. 195-207."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A164833/b164833.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Michel Waldschmidt, \u003ca href=\"http://arxiv.org/abs/0908.4031\"\u003ePerfect Powers: Pillai's works and their developments\u003c/a\u003e, Aug 27, 2009."
			],
			"formula": [
				"Equals Sum_{n\u003e=0} Sum_{m\u003e=0} 1/((4*n+3)^(2*m+1)).",
				"Equals Sum_{k\u003e=1} 1/( (4k-2)*(4k-1)*(4k)) ). - _Bruno Berselli_, Mar 17 2014"
			],
			"example": [
				"0.0461254914187515000992143621808495764868961077417606...",
				"1/(2*3*4) + 1/(6*7*8) + 1/(10*11*12) + 1/(14*15*16) + ... [_Bruno Berselli_, Mar 17 2014]"
			],
			"maple": [
				"evalf[130]((Pi - 4*log(2))/8 ); # _G. C. Greubel_, Aug 11 2019"
			],
			"mathematica": [
				"Join[{0},RealDigits[Pi/8-Log[2]/2,10,120][[1]]] (* _Harvey P. Dale_, Nov 13 2012 *)"
			],
			"program": [
				"(PARI) default(realprecision, 130); (Pi - 4*log(2))/8 \\\\ _G. C. Greubel_, Aug 11 2019",
				"(MAGMA) SetDefaultRealField(RealField(130)); R:= RealField(); (Pi(R)-4*Log(2))/8; // _G. C. Greubel_, Aug 11 2019",
				"(Sage) numerical_approx((pi-4*log(2))/8, digits=130) # _G. C. Greubel_, Aug 11 2019"
			],
			"xref": [
				"Cf. A001597, A019675, A016655.",
				"Cf. A195909, A195913, A195697. - _Mohammad K. Azarian_, Oct 11 2011",
				"Cf. A239362: Sum_{k\u003e=1} 1/((3k-2)*(3k-1)*(3k)) )."
			],
			"keyword": "nonn,cons",
			"offset": "0,2",
			"author": "_Jonathan Vos Post_, Aug 27 2009",
			"ext": [
				"Normalized offset and leading zeros - _R. J. Mathar_, Sep 27 2009"
			],
			"references": 5,
			"revision": 25,
			"time": "2019-08-11T14:37:17-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}