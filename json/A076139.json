{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A076139",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 76139,
			"data": "0,1,15,210,2926,40755,567645,7906276,110120220,1533776805,21362755051,297544793910,4144264359690,57722156241751,803965923024825,11197800766105800,155965244802456376,2172315626468283465,30256453525753512135,421418033734080886426",
			"name": "Triangular numbers that are one-third of another triangular number: T(m) such that 3*T(m)=T(k) for some k.",
			"comment": [
				"Both triangular and generalized pentagonal numbers: intersection of A000217 and A001318. - _Vladeta Jovovic_, Aug 29 2004",
				"Partial sums of Chebyshev polynomials S(n,14)."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A076139/b076139.txt\"\u003eTable of n, a(n) for n = 0..874\u003c/a\u003e",
				"Francesca Arici and Jens Kaad, \u003ca href=\"https://arxiv.org/abs/2012.11186\"\u003eGysin sequences and SU(2)-symmetries of C*-algebras\u003c/a\u003e, arXiv:2012.11186 [math.OA], 2020.",
				"Roger B. Nelson, \u003ca href=\"http://www.jstor.org/stable/10.4169/math.mag.89.3.159\"\u003eMulti-Polygonal Numbers\u003c/a\u003e, Mathematics Magazine, Vol. 89, No. 3 (June 2016), pp. 159-164.",
				"Vladimir Pletser, \u003ca href=\"https://arxiv.org/abs/2101.00998\"\u003eRecurrent Relations for Multiple of Triangular Numbers being Triangular Numbers\u003c/a\u003e, arXiv:2101.00998 [math.NT], 2021.",
				"Vladimir Pletser, \u003ca href=\"https://arxiv.org/abs/2102.12392\"\u003eClosed Form Equations for Triangular Numbers Multiple of Other Triangular Numbers\u003c/a\u003e, arXiv:2102.12392 [math.GM], 2021.",
				"Vladimir Pletser, \u003ca href=\"https://arxiv.org/abs/2102.13494\"\u003eTriangular Numbers Multiple of Triangular Numbers and Solutions of Pell Equations\u003c/a\u003e, arXiv:2102.13494 [math.NT], 2021.",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (15,-15,1)."
			],
			"formula": [
				"a(n) = (A061278(n))*(A061278(n)+1)/2.",
				"a(n) = (1/288)*(-24 + (12-6*sqrt(3))*(7-4*sqrt(3))^n + (12+6*sqrt(3))*(7+4*sqrt(3))^n).",
				"a(0)=0, a(1)=1, a(2)=15; a(n) = 15*a(n-1) - 15*a(n-2) + a(n-3) for n \u003e= 3. G.f.: x/(1-15*x+15*x^2-x^3). - Antonio G. Astudillo (afg_astudillo(AT)hotmail.com), Nov 01 2002",
				"a(n+1) = Sum_{k=0..n} S(k, 14), n \u003e= 0, where S(k, 14) = U(k, 7) = A007655(k+2).",
				"a(n+1) = (S(n+1, 14) - S(n, 14) - 1)/12, n \u003e= 0.",
				"a(n) = 14 * a(n-1) - a(n-2) + 1. a(0)=0, a(1)=1.",
				"a(-n) = a(n-1).",
				"G.f.: x / ((1 - x) * (1 - 14*x +x^2)).",
				"a(2*n) = A108281(n + 1). a(2*n + 1) = A014979(n + 2). - _Michael Somos_, Jun 16 2011",
				"a(n) = (1/2)*A217855(n) = (1/3)*A076140(n) = (1/4)*A123480(n) = (1/8)*A045899(n). - _Peter Bala_, Dec 31 2012",
				"a(n) = A001353(n) * A001353(n-1) / 4. - _Richard R. Forberg_, Aug 26 2013",
				"a(n) = ((2+sqrt(3))^(2*n+1) + (2-sqrt(3))^(2*n+1))/48 - 1/12. - _Vladimir Pletser_, Jan 15 2021"
			],
			"example": [
				"G.f. = x + 15*x^2 + 210*x^3 + 2926*x^4 + 40755*x^5 + 567645*x^6 + ...",
				"a(3)=210=T(20) and 3*210=630=T(35)."
			],
			"mathematica": [
				"a[n_] := a[n] = 14*a[n-1] - a[n-2] + 1; a[0] = 0; a[1] = 1; Table[ a[n], {n, 0, 17}] (* _Jean-François Alcover_, Dec 15 2011, after given formula *)"
			],
			"program": [
				"(PARI) {a(n) = polchebyshev( n, 2, 7) / 14 + polchebyshev( n, 1, 7)/ 84 - 1 / 12}; /* _Michael Somos_, Jun 16 2011 */",
				"(PARI) concat(0, Vec(-x/((x-1)*(x^2-14*x+1)) + O(x^100))) \\\\ _Colin Barker_, May 15 2015"
			],
			"xref": [
				"The m values are in A061278, the k values are in A001571.",
				"Cf. A014979, A076140, A108281.",
				"Cf. A045899, A123480, A217855.",
				"Cf. A212336 for more sequences with g.f. of the type 1/(1-k*x+k*x^2-x^3)."
			],
			"keyword": "easy,nonn",
			"offset": "0,3",
			"author": "Bruce Corrigan (scentman(AT)myfamily.com), Oct 31 2002",
			"ext": [
				"More terms from Antonio G. Astudillo (afg_astudillo(AT)hotmail.com), Nov 01 2002",
				"Chebyshev comments from _Wolfdieter Lang_, Aug 31 2004"
			],
			"references": 25,
			"revision": 68,
			"time": "2021-05-12T22:48:26-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}