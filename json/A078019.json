{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A078019",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 78019,
			"data": "1,0,-2,-1,3,3,-4,-7,4,14,-1,-25,-9,40,33,-56,-82,63,171,-37,-316,-71,524,350,-769,-945,943,2064,-767,-3952,-354,6783,3539,-10381,-10676,13625,24596,-13330,-48897,2359,86823,33208,-138079,-117672,191694,288959,-212101,-598325,114836,1099385,271388",
			"name": "Expansion of (1-x)/(1-x+2*x^2-x^3).",
			"comment": [
				"With 1 prepended, and up to sign this is the q-deformation of 12/5. See Leclere and Morier-Genoud. - _Michel Marcus_, Jul 01 2021"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A078019/b078019.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Ludivine Leclere and Sophie Morier-Genoud, \u003ca href=\"https://arxiv.org/abs/2101.02953\"\u003eq-deformations of the modular group and of the real quadratic irrational numbers\u003c/a\u003e, arXiv:2101.02953 [math.NT], 2021. See Example 2.7 p. 6.",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,-2,1)."
			],
			"formula": [
				"a(n) = a(n-1) - 2*a(n-2) + a(n-3). - _Michael Somos_, Sep 18 2012",
				"a(n) = -A000931(-2*n - 1). - _Michael Somos_, Sep 18 2012",
				"G.f.: (1+x)/x^3 - 1/( Q(0) - x )/x^3 where Q(k) = 1 - x^2/(x^2*k - 1 )/Q(k+1) ; (recursively defined continued fraction). - _Sergei N. Gladkovskii_, Feb 23 2013",
				"a(n) = (-1)^(n-1)*(A077979(n) + A077979(n-1)) = A077954(n) - A077954(n-1). - _G. C. Greubel_, Jun 29 2019"
			],
			"example": [
				"G.f. = 1 - 2*x^2 - x^3 + 3*x^4 + 3*x^5 - 4*x^6 - 7*x^7 + 4*x^8 + ..."
			],
			"mathematica": [
				"CoefficientList[Series[(1-x)/(1-x+2x^2-x^3),{x,0,50}],x] (* or *) LinearRecurrence[{1,-2,1},{1,0,-2},51] (* _Harvey P. Dale_, Feb 18 2013 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, polcoeff( (1 - 2*x) / (1 - 2*x + x^2 - x^3) + x * O(x^-n), -n), polcoeff( (1 - x) / (1 - x + 2*x^2 - x^3) + x * O(x^n), n))} /* _Michael Somos_, Sep 18 2012 */",
				"(MAGMA) R\u003cx\u003e:=PowerSeriesRing(Integers(), 50); Coefficients(R!( (1-x)/(1-x+2*x^2-x^3) )); // _G. C. Greubel_, Jun 29 2019",
				"(Sage) ((1-x)/(1-x+2*x^2-x^3)).series(x, 50).coefficients(x, sparse=False) # _G. C. Greubel_, Jun 29 2019",
				"(GAP) a:=[1,0,-2];; for n in [4..50] do a[n]:=a[n-1]-2*a[n-2]+a[n-3]; od; a; # _G. C. Greubel_, Jun 29 2019"
			],
			"xref": [
				"Cf. A000931, A077954, A077979."
			],
			"keyword": "sign,easy",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_, Nov 17 2002",
			"references": 2,
			"revision": 26,
			"time": "2021-07-01T12:10:26-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}