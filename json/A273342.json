{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A273342",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 273342,
			"data": "1,1,1,2,2,1,5,4,3,1,13,10,7,4,1,35,27,18,11,5,1,97,75,50,30,16,6,1,275,213,143,86,47,22,7,1,794,616,416,253,140,70,29,8,1,2327,1808,1227,754,424,218,100,37,9,1,6905,5372,3661,2269,1295,681,327,138,46,10,1,20705,16127,11030,6885,3978,2133,1056,475,185,56,11,1",
			"name": "Triangle read by rows: T(n,k) is the number of bargraphs of semiperimeter n having length of first column k (n\u003e=2, k\u003e=1).",
			"comment": [
				"Sum of entries in row n = A082582(n).",
				"Sum(k*T(n,k), k\u003e=1) = A273343(n)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A273342/b273342.txt\"\u003eRows n = 2..150, flattened\u003c/a\u003e",
				"M. Bousquet-Mélou and A. Rechnitzer, \u003ca href=\"http://dx.doi.org/10.1016/S0196-8858(02)00553-5\"\u003eThe site-perimeter of bargraphs\u003c/a\u003e, Adv. in Appl. Math. 31 (2003), 86-112.",
				"Emeric Deutsch, S Elizalde, \u003ca href=\"http://arxiv.org/abs/1609.00088\"\u003eStatistics on bargraphs viewed as cornerless Motzkin paths\u003c/a\u003e, arXiv preprint arXiv:1609.00088, 2016"
			],
			"formula": [
				"G.f.: G(x,z) satisfies (1 - t - tz^2 + t^2 z)G^2  - t(1 - z)(1- z - tz - tz^2)G + t^2 z^2 (1 - z) = 0 (z marks semiperimeter, x marks length of first column)."
			],
			"example": [
				"Row 4 is 2,2,1 because the 5 (=A082582(4)) bargraphs of semiperimeter 4 correspond to the compositions [1,1,1], [1,2], [2,1], [2,2], [3] which, clearly, have first-column lengths 1, 1, 2, 2, 3."
			],
			"maple": [
				"eq := (1-t-t*z^2+t^2*z)*G^2-t*(1-z)*(1-z-t*z-t*z^2)*G+t^2*z^2*(1-z) = 0: G := RootOf(eq, G): Gser := simplify(series(G, z = 0, 23)): for n from 2 to 20 do P[n] := sort(expand(coeff(Gser, z, n))) end do: for n from 2 to 20 do seq(coeff(P[n], t, j), j = 1 .. n-1) end do; # yields sequence in triangular form",
				"# second Maple program:",
				"b:= proc(n, y, t, h) option remember; expand(",
				"      `if`(n=0, (1-t), `if`(t\u003c0, 0, b(n-1, y+1, 1, h))+",
				"      `if`(t\u003e0 or y\u003c2, 0, b(n, y-1, -1, 0))+",
				"      `if`(y\u003c1, 0, b(n-1, y, 0, 0)*`if`(h=1, z^y, 1))))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, z, i), i=1..degree(p)))(b(n, 0$2, 1)):",
				"seq(T(n), n=2..20);  # _Alois P. Heinz_, Jun 06 2016"
			],
			"mathematica": [
				"b[n_, y_, t_, h_] := b[n, y, t, h] = Expand[If[n == 0, 1 - t, If[t \u003c 0, 0, b[n - 1, y + 1, 1, h]] + If[t \u003e 0 || y \u003c 2, 0, b[n, y - 1, -1, 0]] + If[y \u003c 1, 0, b[n - 1, y, 0, 0]*If[h == 1, z^y, 1]]]];",
				"T[n_] := Function [p, Table[Coefficient[p, z, i], {i, 1, Exponent[p, z]}]][ b[n, 0, 0, 1]];",
				"Table[T[n], {n, 2, 20}] // Flatten (* _Jean-François Alcover_, Jul 29 2016, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A082582, A273343."
			],
			"keyword": "nonn,tabl",
			"offset": "2,4",
			"author": "_Emeric Deutsch_, May 21 2016",
			"references": 2,
			"revision": 17,
			"time": "2017-08-19T23:03:30-04:00",
			"created": "2016-05-21T12:13:48-04:00"
		}
	]
}