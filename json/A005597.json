{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005597",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5597,
			"id": "M4056",
			"data": "6,6,0,1,6,1,8,1,5,8,4,6,8,6,9,5,7,3,9,2,7,8,1,2,1,1,0,0,1,4,5,5,5,7,7,8,4,3,2,6,2,3,3,6,0,2,8,4,7,3,3,4,1,3,3,1,9,4,4,8,4,2,3,3,3,5,4,0,5,6,4,2,3,0,4,4,9,5,2,7,7,1,4,3,7,6,0,0,3,1,4,1,3,8,3,9,8,6,7,9,1,1,7,7,9",
			"name": "Decimal expansion of the twin prime constant C_2 = Product_{ p prime \u003e= 3 } (1-1/(p-1)^2).",
			"comment": [
				"C_2 = Product_{ p prime \u003e 2} (p * (p-2) / (p-1)^2) is the 2-tuple case of the Hardy-Littlewood prime k-tuple constant (part of First H-L Conjecture): C_k = Product_{ p prime \u003e k} (p^(k-1) * (p-k) / (p-1)^k).",
				"Although C_2 is commonly called the twin prime constant, it is actually the prime 2-tuple constant (prime pair constant) which is relevant to prime pairs (p, p+2m), m \u003e= 1.",
				"The Hardy-Littlewood asymptotic conjecture for Pi_2m(n), the number of prime pairs (p, p+2m), m \u003e= 1, with p \u003c= n, claims that Pi_2m(n) ~ C_2(2m) * Li_2(n), where Li_2(n) = Integral_{2, n} (dx/log^2(x)) and C_2(2m) = 2 * C_2 * Product_{p prime \u003e 2, p | m} (p-1)/(p-2), which gives: C_2(2) = 2 * C_2 as the prime pair (p, p+2) constant, C_2(4) = 2 * C_2 as the prime pair (p, p+4) constant, C_2(6) = 2* (2/1) * C_2 as the prime pair (p, p+6) constant, C_2(8) = 2 * C_2 as the prime pair (p, p+8) constant, C_2(10) = 2 * (4/3) * C_2 as the prime pair (p, p+10) constant, C_2(12) = 2 * (2/1) * C_2 as the prime pair (p, p+12) constant, C_2(14) = 2 * (6/5) * C_2 as the prime pair (p, p+14) constant, C_2(16) = 2 * C_2 as the prime pair (p, p+16) constant, ... and, for i \u003e= 1, C_2(2^i) = 2 * C_2 as the prime pair (p, p+2^i) constant.",
				"C_2 also occurs as part of other Hardy-Littlewood conjectures related to prime pairs, e.g., the Hardy-Littlewood conjecture concerning the distribution of the Sophie Germain primes (A156874) on primes p such that 2p+1 is also prime.",
				"Another constant related to the twin primes is Viggo Brun's constant B (sometimes also called the twin primes Viggo Brun's constant B_2) A065421, where B_2 = Sum (1/p + 1/q) as (p,q) runs through the twin primes.",
				"Reciprocal of the Selberg-Delange constant A167864. See A167864 for additional comments and references. - _Jonathan Sondow_, Nov 18 2009",
				"C_2 = Product_{prime p\u003e2} (p-2)p/(p-1)^2 is an analog for primes of Wallis' product 2/Pi = Product_{n=1 to oo} (2n-1)(2n+1)/(2n)^2. - _Jonathan Sondow_, Nov 18 2009",
				"One can compute a cubic variant, product_{primes \u003e2} (1-1/(p-1)^3) = 0.855392... = (2/3) * 0.6601618...* 1.943596... by multiplying this constant with 2/3 and A082695. - _R. J. Mathar_, Apr 03 2011",
				"Cohen (1998, p. 7) referred to this number as the \"twin prime and Goldbach constant\" and noted that, conjecturally, the number of twin prime pairs (p,p+2) with p \u003c= X tends to 2*C_2*X/log(X)^2 as X tends to infinity. - _Artur Jasinski_, Feb 01 2021"
			],
			"reference": [
				"Henri Cohen, Number Theory, Volume II: Analytic and Modern Tools, GTM Vol. 240, Springer, 2007; see pp. 208-209.",
				"Richard Crandall and Carl Pomerance, Prime Numbers: A Computational Perspective, Springer, NY, 2001; see p. 11.",
				"Steven R. Finch, Mathematical Constants, Encyclopedia of Mathematics and its Applications, Vol. 94, Cambridge University Press, 2003, pp. 84-93.",
				"G. H. Hardy and E. M. Wright, An Introduction to the Theory of Numbers, 5th ed., Oxford Univ. Press, 1979, ch. 22.20.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Harry J. Smith, \u003ca href=\"/A005597/b005597.txt\"\u003eTable of n, a(n) for n = 0..1001\u003c/a\u003e",
				"Folkmar Bornemann, \u003ca href=\"http://www.ams.org/notices/200305/fea-smale.pdf\"\u003ePRIMES Is in P: Breakthrough for \"Everyman\"\u003c/a\u003e, Notices Amer. Math. Soc., Vol. 50, No. 5 (May 2003), p. 549.",
				"Paul S. Bruckman, \u003ca href=\"https://fq.math.ca/Scanned/39-4/advanced39-4.pdf\"\u003eProblem H-576\u003c/a\u003e, Advanced Problems and Solutions, The Fibonacci Quarterly, Vol. 39, No. 4 (2001), p. 379; \u003ca href=\"https://www.fq.math.ca/Scanned/40-4/advanced40-4.pdf\"\u003eGeneral IZE\u003c/a\u003e, Solution to Problem H-576 by the proposer, ibid., Vol. 40, No. 4 (2002), pp. 383-384.",
				"C. K. Caldwell, The Prime Glossary, \u003ca href=\"http://primes.utm.edu/glossary/page.php?sort=TwinPrimeConstant\"\u003etwin prime constant\u003c/a\u003e.",
				"Henri Cohen, \u003ca href=\"http://www.math.u-bordeaux.fr/~cohen/hardylw.dvi\"\u003eHigh-precision computation of Hardy-Littlewood constants\u003c/a\u003e, (1998).",
				"Henri Cohen, \u003ca href=\"/A221712/a221712.pdf\"\u003eHigh-precision computation of Hardy-Littlewood constants\u003c/a\u003e. [pdf copy, with permission]",
				"Steven R. Finch, \u003ca href=\"http://arxiv.org/abs/2001.00578\"\u003eMathematical Constants, Errata and Addenda\u003c/a\u003e, arXiv:2001.00578 [math.HO], 2020, Sec. 2.1.",
				"Philippe Flajolet and Ilan Vardi, \u003ca href=\"http://algo.inria.fr/flajolet/Publications/landau.ps\"\u003eZeta Function Expansions of Classical Constants\u003c/a\u003e, Unpublished manuscript. 1996.",
				"Daniel A. Goldston, Timothy Ngotiaoco and Julian Ziegler Hunts, \u003ca href=\"https://doi.org/10.7169/facm/1602\"\u003eThe tail of the singular series for the prime pair and Goldbach problems\u003c/a\u003e, Functiones et Approximatio Commentarii Mathematici, Vol. 56, No. 1 (2017), pp. 117-141; \u003ca href=\"http://arxiv.org/abs/1409.2151\"\u003earXiv preprint\u003c/a\u003e, arXiv:1409.2151 [math.NT], 2014.",
				"R. J. Mathar, \u003ca href=\"http://arxiv.org/abs/0903.2514\"\u003eHardy-Littlewood constants embedded into infinite products over all positive integers\u003c/a\u003e, arXiv:0903.2514 [math.NT], 2009-2011, constant T_1^(2).",
				"G. Niklasch, \u003ca href=\"/A001692/a001692.html\"\u003eSome number theoretical constants: 1000-digit values\u003c/a\u003e. [Cached copy]",
				"G. Niklasch, \u003ca href=\"http://www.gn-50uma.de/alula/essays/Moree/Moree-details.en.shtml#t05-twin\"\u003eTwin primes constant\u003c/a\u003e.",
				"Simon Plouffe, \u003ca href=\"http://www.worldwideschool.org/library/books/sci/math/MiscellaneousMathematicalConstants/chap90.html\"\u003eThe twin primes constant\u003c/a\u003e.",
				"Simon Plouffe, Plouffe's Inverter, \u003ca href=\"http://www.plouffe.fr/simon/constants/twinprime.txt\"\u003eThe twin primes constant\u003c/a\u003e.",
				"Pascal Sebah, \u003ca href=\"http://numbers.computation.free.fr/Constants/Primes/twin.html\"\u003eNumbers, constants and computation\u003c/a\u003e (gives 5000 digits).",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/TwinPrimesConstant.html\"\u003eTwin Primes Constant\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/TwinPrimeConjecture.html\"\u003eTwin Prime Conjecture\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/k-TupleConjecture.html\"\u003ek-Tuple Conjecture\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PrimeConstellation.html\"\u003ePrime Constellation\u003c/a\u003e.",
				"John W. Wrench, Jr., \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1961-0124305-0\"\u003eEvaluation of Artin's constant and the twin-prime constant\u003c/a\u003e, Math. Comp., Vol. 15, No. 76 (1961), pp. 396-398."
			],
			"formula": [
				"Equals Product_{k\u003e=2} (zeta(k)*(1-1/2^k))^(-Sum_{d|k} mu(d)*2^(k/d)/k). - _Benoit Cloitre_, Aug 06 2003",
				"Equals 1/A167864. - _Jonathan Sondow_, Nov 18 2009",
				"Equals Sum_{k\u003e=1} mu(2*k-1)/phi(2*k-1)^2, where mu is the Möbius function (A008683) and phi is the Euler totient function (A000010) (Bruckman, 2001). - _Amiram Eldar_, Jan 14 2022"
			],
			"example": [
				"0.6601618158468695739278121100145557784326233602847334133194484233354056423..."
			],
			"mathematica": [
				"s[n_] := (1/n)*N[ Sum[ MoebiusMu[d]*2^(n/d), {d, Divisors[n]}], 160]; C2 = (175/256)*Product[ (Zeta[n]*(1 - 2^(-n))*(1 - 3^(-n))*(1 - 5^(-n))*(1 - 7^(-n)))^(-s[n]), {n, 2, 160}]; RealDigits[C2][[1]][[1 ;; 105]] (* _Jean-François Alcover_, Oct 15 2012, after PARI *)",
				"digits = 105; f[n_] := -2*(2^n-1)/(n+1); C2 = Exp[NSum[f[n]*(PrimeZetaP[n+1] - 1/2^(n+1)), {n, 1, Infinity}, NSumTerms -\u003e 5 digits, WorkingPrecision -\u003e 5 digits]]; RealDigits[C2, 10, digits][[1]] (* _Jean-François Alcover_, Apr 16 2016, updated Apr 24 2018 *)"
			],
			"program": [
				"(PARI) \\p1000; 175/256*prod(k=2,500,(zeta(k)*(1-1/2^k)*(1-1/3^k)*(1-1/5^k)*(1-1/7^k))^(-sumdiv(k,d,moebius(d)*2^(k/d))/k))",
				"(PARI) prodeulerrat(1-1/(p-1)^2, 1, 3) \\\\ _Amiram Eldar_, Mar 12 2021"
			],
			"xref": [
				"Cf. A065645 (continued fraction), A065646 (denominators of convergents to twin prime constant), A065647 (numerators of convergents to twin prime constant), A062270, A062271, A114907, A065418 (C_3), A167864, A000010, A008683."
			],
			"keyword": "cons,nonn,nice,changed",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Vladeta Jovovic_, Nov 08 2001",
				"Commented and edited by _Daniel Forgues_, Jul 28 2009, Aug 04 2009, Aug 12 2009",
				"PARI code removed by _D. S. McNeil_, Dec 26 2010"
			],
			"references": 37,
			"revision": 106,
			"time": "2022-01-14T07:31:40-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}