{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A309200",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 309200,
			"data": "1,2,5,7,3,4,11,10,13,17,14,19,20,6,9,15,22,12,21,23,26,8,18,29,28,31,34,35,37,16,41,38,30,39,43,46,47,25,49,44,27,24,36,40,42,45,51,50,52,33,53,54,55,48,57,58,59,60,61,56,63,32,65,66,67,68,69",
			"name": "a(n) is the smallest divisor of the Catalan number C(n) = A000108(n) not already in the sequence.",
			"comment": [
				"Conjecture: This is a permutation of the positive integers. [The conjecture is true, see A309364. - _Rémy Sigrist_, Jul 25 2019]",
				"Given any monotonically increasing sequence {b(n): n \u003e= 1} of positive integers we can define a sequence {a(n): n \u003e= 1} by setting a(n) to be smallest divisor of b(n) not already in the {a(n)} sequence. The triangular numbers A000217 produce A111273. A000027 is fixed under this transformation."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A309200/b309200.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A309200/a309200.gp.txt\"\u003ePARI program for A309200\u003c/a\u003e"
			],
			"maple": [
				"with(numtheory);",
				"# the general transformation",
				"f := proc(b) local t1,d,j,dlis,L,hit,i,n,a,n1;",
				"if whattype(b) \u003c\u003e list then RETURN([]); fi;",
				"n1:=nops(b); a:=[]; L:=10000;",
				"hit:=Array(0..L,0);",
				"for n from 1 to n1 do",
				"   t1:=b[n];",
				"dlis:=sort(convert(divisors(t1),list));",
				"for j from 1 to nops(dlis) do d:=dlis[j];",
				"  if d \u003e L then error(\"d too large\",n,t1,d); fi;",
				"   if hit[d]=0 then break; fi; od:",
				"a:=[op(a),d];",
				"hit[d]:=1;          od;",
				"[seq(a[i],i=1..nops(a))];",
				"end;",
				"# the Catalan numbers",
				"C:=[seq(binomial(2*n,n)/(n+1),n=1..40)];",
				"f(C);"
			],
			"program": [
				"(PARI) \\\\ See Links section.",
				"(Sage)",
				"def transform(sup, fun):",
				"    A = []",
				"    for n in (1..sup):",
				"        D = divisors(fun(n))",
				"        A.append(next(d for d in D if d not in A))",
				"    return A",
				"A309200list = lambda lim: transform(lim, catalan_number)",
				"print(A309200list(29)) # _Peter Luschny_, Jul 26 2019"
			],
			"xref": [
				"Cf. A000027, A000108, A000217, A111273, A309364.."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, Jul 25 2019",
			"ext": [
				"More terms from _Rémy Sigrist_, Jul 25 2019"
			],
			"references": 6,
			"revision": 22,
			"time": "2020-04-28T15:10:00-04:00",
			"created": "2019-07-25T13:50:07-04:00"
		}
	]
}