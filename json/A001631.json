{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001631",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1631,
			"id": "M1081 N0410",
			"data": "0,0,1,0,1,2,4,7,14,27,52,100,193,372,717,1382,2664,5135,9898,19079,36776,70888,136641,263384,507689,978602,1886316,3635991,7008598,13509507,26040412,50194508,96753025,186497452,359485397,692930382,1335666256,2574579487",
			"name": "Tetranacci numbers: a(n) = a(n-1) + a(n-2) + a(n-3) + a(n-4), with initial conditions a(0..3) = (0, 0, 1, 0).",
			"comment": [
				"The \"standard\" Tetranacci numbers with initial terms (0,0,0,1) are listed in A000078.",
				"Starting (1, 2, 4, ...) is the INVERT transform of the cyclic sequence (1, 1, 1, 0, (repeat) ...); equivalent to the statement that (1, 2, 4, ...) corresponding to n = (1, 2, 3, ...) represents the numbers of ordered compositions of n using terms in the set \"not multiples of four\". - _Gary W. Adamson_, May 13 2013",
				"a(n+4) equals the number of n-length binary words avoiding runs of zeros of lengths 4i+3, (i=0,1,2,...). - _Milan Janjic_, Feb 26 2015"
			],
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Harvey P. Dale, \u003ca href=\"/A001631/b001631.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Matthias Beck and Neville Robbins, \u003ca href=\"http://arxiv.org/abs/1403.0665\"\u003eVariations on a Generating Function Theme: Enumerating Compositions with Parts Avoiding an Arithmetic Sequence\u003c/a\u003e, arXiv:1403.0665 [math.NT], 2014.",
				"Petros Hadjicostas, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL19/Hadjicostas/hadji2.html\"\u003eCyclic compositions of a positive integer with parts avoiding an arithmetic sequence\u003c/a\u003e, Journal of Integer Sequences, 19 (2016), #16.8.2.",
				"W. C. Lynch, \u003ca href=\"http://www.fq.math.ca/Scanned/8-1/lynch.pdf\"\u003eThe t-Fibonacci numbers and polyphase sorting\u003c/a\u003e, Fib. Quart., 8 (1970), pp. 6-22.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,1,1,1)."
			],
			"formula": [
				"G.f.: ((x-1)*x^2)/(x^4+x^3+x^2+x-1). - _Harvey P. Dale_, Oct 21 2011"
			],
			"maple": [
				"A001631:=(-1+z)/(-1+z+z**2+z**3+z**4); # conjectured by _Simon Plouffe_ in his 1992 dissertation",
				"a:= n-\u003e (Matrix([[0,-1,2,-1]]). Matrix(4, (i,j)-\u003e `if`(i=j-1 or j=1, 1, 0))^n)[1,1]: seq(a(n), n=0..35); # _Alois P. Heinz_, Aug 01 2008"
			],
			"mathematica": [
				"LinearRecurrence[{1, 1, 1, 1}, {0, 0, 1, 0}, 100] (* _Vladimir Joseph Stephan Orlovsky_, Jul 01 2011 *)",
				"CoefficientList[Series[((-1+x) x^2)/(-1+x+x^2+x^3+x^4),{x,0,50}],x] (* _Harvey P. Dale_, Oct 21 2011 *)"
			],
			"program": [
				"(PARI) a(n)=([0,1,0,0; 0,0,1,0; 0,0,0,1; 1,1,1,1]^n)[1,3] \\\\ _Charles R Greathouse IV_, Apr 08 2016, simplified by _M. F. Hasler_, Apr 20 2018",
				"(PARI) x='x+O('x^30); concat([0,0], Vec(((x-1)*x^2)/(x^4+x^3+x^2+x-1))) \\\\ _G. C. Greubel_, Jan 09 2018",
				"(MAGMA) I:=[0,0,1,0]; [n le 4 select I[n] else Self(n-1) + Self(n-2) + Self(n-3) + Self(n-4): n in [1..30]]; // _G. C. Greubel_, Jan 09 2018"
			],
			"xref": [
				"Absolute values of first differences of standard Tetranacci numbers A000078.",
				"Cf. A000288 (variant: starting with 1, 1, 1, 1).",
				"Cf. A000336 (variant: sum replaced by product)."
			],
			"keyword": "nonn,easy",
			"offset": "0,6",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from Larry Reeves (larryr(AT)acm.org), Jul 31 2000",
				"Edited by _M. F. Hasler_, Apr 20 2018"
			],
			"references": 40,
			"revision": 101,
			"time": "2021-03-12T22:32:35-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}