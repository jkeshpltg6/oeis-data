{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A157306",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 157306,
			"data": "1,1,-1,-5,23,151,-1249,-10961,122975,1380703,-18981121,-261461441,4220277887,69016559743,-1278668869633,-24212706029825,507099112447487,10908810091109887,-255195984676134913,-6145413122840501249",
			"name": "G.f. A(x) satisfies the condition that both G(x) = A(x/G(x)) and H(x) = A(x*H(x)) have zeros for every other coefficient after initial terms; g.f. of dual sequence A157303 satisfies the same condition.",
			"formula": [
				"For n\u003e=1, [x^(2n)] A(x)^(2n+1) = 0.",
				"For n\u003e=1, [x^(2n+1)] 1/A(x)^(2n) = 0.",
				"G.f. satisfies: A(x) = G(x*A(x)) where G(x) = A(x/G(x)) = x/Series_Reversion(x*A(x)) = g.f. of A157305.",
				"G.f. satisfies: A(x) = H(x/A(x)) where H(x) = A(x*H(x)) = Series_Reversion(x/A(x))/x = g.f. of A157307."
			],
			"example": [
				"G.f.: A(x) = 1 + x - x^2 - 5*x^3 + 23*x^4 + 151*x^5 - 1249*x^6 -++-...",
				"...",
				"Let G(x) = A(x/G(x)) so that A(x) = G(x*A(x)) then",
				"G(x) = 1 + x - 2*x^2 + 26*x^4 - 1378*x^6 + 141202*x^8 -+...",
				"has alternating zeros in the coefficients (cf. A157305):",
				"[1,1,-2,0,26,0,-1378,0,141202,0,-22716418,0,5218302090,0,...]",
				"...",
				"Let H(x) = A(x*H(x)) so that A(x) = H(x/A(x)) then",
				"H(x) = 1 + x - 7*x^3 + 242*x^5 - 17771*x^7 + 2189294*x^9 -+...",
				"has alternating zeros in the coefficients (cf. A157307):",
				"[1,1,0,-7,0,242,0,-17771,0,2189294,0,-404590470,0,104785114020,0,...]",
				"...",
				"ZERO COEFFICIENTS IN POWERS OF G.F. A(x).",
				"Odd powers A(x)^(2n+1) yield zeros at even positions 2n for n\u003e=1:",
				"A^3: [1,3, 0, -20,39,609,-2806,-41598,302361,4976719,...];",
				"A^5: [1,5,5,-35, 0, 1176,-2530,-80630,359635,9462895,...];",
				"A^7: [1,7,14,-42,-98,1694, 0, -122408,263963,14465941,...];",
				"A^9: [1,9,27,-33,-243,1989,4797,-159939, 0, 19515184,...];",
				"...",
				"Even negative powers 1/A(x)^(2n) yield zeros at odd positions 2n+1 for n\u003e=1:",
				"A^-2: [1,-2,5, 0, -56,-112,2916,12112,-284944,-1831680,...];",
				"A^-4: [1,-4,14,-20,-87, 0, 5720,11440,-586040,-2389920,...];",
				"A^-6: [1,-6,27,-68,-33,186,7865, 0, -865776,-1731552,...];",
				"A^-8: [1,-8,44,-152,182,136,9404,-19400,-1095871, 0, ...]; ..."
			],
			"program": [
				"(PARI) {a(n)=local(A=[1, 1]); for(i=1, n, if(#A%2==0, A=concat(A, t); A[ #A]=-subst(Vec(serreverse(x/Ser(A)))[ #A], t, 0)); if(#A%2==1, A=concat(A, t); A[ #A]=-subst(Vec(x^2/serreverse(x*Ser(A)))[ #A], t, 0))); A[n+1]}"
			],
			"xref": [
				"Cf. A157305, A157307, A157302, A157303 (dual), A157304."
			],
			"keyword": "sign",
			"offset": "0,4",
			"author": "_Paul D. Hanna_, Feb 28 2009",
			"references": 5,
			"revision": 2,
			"time": "2012-03-30T18:37:16-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}