{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A290995",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 290995,
			"data": "0,0,0,0,0,0,0,1,8,36,120,330,792,1716,3432,6436,11456,19584,32640,54264,93024,170544,341088,735472,1653632,3749760,8386560,18289440,38724480,79594560,159189120,311058496,597137408,1133991936,2147450880,4089171840",
			"name": "p-INVERT of (1,1,1,1,1,...), where p(S) = 1 - S^8.",
			"comment": [
				"Suppose s = (c(0), c(1), c(2),...) is a sequence and p(S) is a polynomial. Let S(x) = c(0)*x + c(1)*x^2 + c(2)*x^3 + ... and T(x) = (-p(0) + 1/p(S(x)))/x. The p-INVERT of s is the sequence t(s) of coefficients in the Maclaurin series for T(x).  Taking p(S) = 1 - S gives the \"INVERT\" transform of s, so that p-INVERT is a generalization of the \"INVERT\" transform (e.g., A033453).",
				"See A291000 for a guide to related sequences."
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A290995/b290995.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_07\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (8, -28, 56, -70, 56, -28, 8)"
			],
			"formula": [
				"a(n) = 8*a(n-1) - 28*a(n-2) + 56*a(n-3) - 70*a(n-4) + 56*a(n-5) - 28*a(n-6) + 8*a(n-7) for n \u003e= 9.",
				"G.f.: x^7 / ((1 - 2*x)*(1 - 2*x + 2*x^2)*(1 - 4*x + 6*x^2 - 4*x^3 + 2*x^4)). - _Colin Barker_, Aug 22 2017"
			],
			"mathematica": [
				"z = 60; s = x/(1 - x); p = 1 - s^8;",
				"Drop[CoefficientList[Series[s, {x, 0, z}], x], 1]  (* A000012 *)",
				"Drop[CoefficientList[Series[1/p, {x, 0, z}], x], 1]  (* A290995 *)"
			],
			"program": [
				"(PARI) concat(vector(7), Vec(x^7 / ((1 - 2*x)*(1 - 2*x + 2*x^2)*(1 - 4*x + 6*x^2 - 4*x^3 + 2*x^4)) + O(x^50))) \\\\ _Colin Barker_, Aug 22 2017"
			],
			"xref": [
				"Cf. A000012, A289780, A291000."
			],
			"keyword": "nonn,easy",
			"offset": "0,9",
			"author": "_Clark Kimberling_, Aug 22 2017",
			"references": 3,
			"revision": 6,
			"time": "2017-08-23T09:46:03-04:00",
			"created": "2017-08-23T09:46:03-04:00"
		}
	]
}