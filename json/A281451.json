{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A281451",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 281451,
			"data": "1,3,2,0,2,2,0,0,0,2,2,0,0,0,0,0,2,3,2,0,1,4,0,0,2,2,4,0,0,2,0,0,0,2,0,0,4,2,0,0,0,0,2,0,0,2,0,0,0,2,2,0,1,4,0,0,4,1,2,0,0,4,0,0,2,2,4,0,2,2,0,0,0,2,0,0,0,2,0,0,0,4,4,0,2,0,0",
			"name": "Expansion of x * f(x, x) * f(x, x^17) in powers of x where f(, ) is Ramanujan's general theta function.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A281451/b281451.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"f(x,x^m) = 1 + Sum_{k\u003e=1} x^((m+1)*k*(k-1)/2) (x^k + x^(m*k)). - _N. J. A. Sloane_, Jan 30 2017",
				"Euler transform of a period 36 sequence.",
				"G.f.: x * (Sum_{k in Z} x^k^2) * (Sum_{k in Z} x^(9*k^2 + 8*k)).",
				"G.f.: x * Product_{k\u003e0} (1 + x^(2*k-1))^2 * (1 - x^(2*k)) * (1 + x^(18*k-17)) * (1 + x^(18*k-1)) * (1 - x^(18*k)).",
				"a(4*n) = a(8*n + 7) = a(16*n + 13) = a(32*n + 9) = a(49*n + 7) = a(98*n + 14) = 0.",
				"a(4*n + 1) = A281452(n).  a(8*n + 3) = 2 * A281491(n).  A(16*n + 1) = A281453(n).",
				"a(32*n + 25) = 2 * A281490(n). a(64*n + 49) = a(n). a(128*n + 17) = 2 * A281492(n).",
				"a(n) = A122865(3*n + 2). a(n) = A122856(6*n + 4) = A258278(6*n + 4).",
				"a(n) = b(9*n + 7) where b = A002654, A035154, A113446, A122864, A125061, A129448, A138950, A163746, A256276, A258228, A258256.",
				"2 * a(n) = b(9*n + 7) where b = A105673, A122857, A258034, A259761.  -2 * a(n) = b(9*n + 7) where b = A138949, A256280, A258292.",
				"a(n) = - A256269(9*n + 7). 4 * a(n) = A004018(9*n + 7)."
			],
			"example": [
				"G.f. = x + 3*x^2 + 2*x^3 + 2*x^5 + 2*x^6 + 2*x^10 + 2*x^11 + 2*x^17 + ...",
				"G.f. = q^16 + 3*q^25 + 2*q^34 + 2*q^52 + 2*q^61 + 2*q^97 + 2*q^106 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 0, 0, DivisorSum[ 9 n + 7, KroneckerSymbol[ -4, #] \u0026]];",
				"a[ n_] := If[ n \u003c 0, 0, Times @@ (Which[# \u003c 3, 1, Mod[#, 4] == 1, #2 + 1, True, (1 + (-1)^#2) / 2] \u0026 @@@ FactorInteger[ 9 n + 7])];",
				"a[ n_] := SeriesCoefficient[ x EllipticTheta[ 3, 0, x] QPochhammer[ -x, x^18] QPochhammer[ -x^17, x^18] QPochhammer[ x^18], {x, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, 0, sumdiv(9*n + 7, d, (d%4==1) - (d%4==3)))};",
				"(PARI) {a(n) = if( n\u003c0, 0, my(A, p, e); A = factor(9*n + 7); prod(k=1, matsize(A)[1], [p, e] = A[k, ]; if(p==2, 1, p%4==1, e+1, 1-e%2)))};",
				"(PARI) {a(n) = if( n\u003c0, 0, my(m = 9*n + 7, k, s); forstep(j=0, sqrtint(m), 3, if( issquare(m - j^2, \u0026k) \u0026\u0026 (k%9 == 4 || k%9 == 5), s+=(j\u003e0)+1)); s)};"
			],
			"xref": [
				"Cf. A002654, A004018, A035154, A105673, A113446, A122856, A122857, A122864, A122865.",
				"Cf. A125061, A129448, A138949, A138950, A163746, A256269, A256276, A256280, A258034.",
				"Cf. A258228, A258256, A258278, A258292, A259761.",
				"Cf. A281452, A281453, A281490, A281491, A281492."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Michael Somos_, Jan 23 2017",
			"references": 6,
			"revision": 21,
			"time": "2021-06-04T02:32:51-04:00",
			"created": "2017-01-23T13:22:21-05:00"
		}
	]
}