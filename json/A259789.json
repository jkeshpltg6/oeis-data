{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A259789",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 259789,
			"data": "2,27,8,2,2,9,3,5,96,10,9,2,2,2,28,4,9,11,8,195,3,3,723,28,573,225,2,2,2,35,46,132,4,4,65,14,58,11,8,967,311,10,98,3,3,21,94,20,2,2,28,23,30,16,29,3419,134,4,251,7",
			"name": "Least integer k \u003e 1 such that pi(k)*pi(k*n) is a square, where pi(.) is the prime-counting function given by A000720.",
			"comment": [
				"Conjecture: a(n) exists for any n \u003e 0. In general, every positive rational number r can be written as m/n, where m and n are positive integers with pi(m)*pi(n) a positive square.",
				"For example, 25/32 = 13102500/16771200 with pi(13102500)*pi(16771200) = 855432*1077512 = 921738245184 = 960072^2, and 49/58 = 1076068567/1273713814 with pi(1076068567)*pi(1273713814) = 54511776*63975626 = 3487424993971776 = 59054424^2."
			],
			"reference": [
				"Zhi-Wei Sun, Problems on combinatorial properties of primes, in: M. Kaneko, S. Kanemitsu and J. Liu (eds.), Number Theory: Plowing and Starring through High Wave Forms, Proc. 7th China-Japan Seminar (Fukuoka, Oct. 28 - Nov. 1, 2013), Ser. Number Theory Appl., Vol. 11, World Sci., Singapore, 2015, pp. 169-187."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A259789/b259789.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"/A259789/a259789_2.txt\"\u003eChecking the conjecture for r = a/b with a,b = 1..60\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1402.6641\"\u003eProblems on combinatorial properties of primes\u003c/a\u003e, arXiv:1402.6641 [math.NT], 2014.",
				"Zhi-Wei Sun, \u003ca href=\"http://link.springer.com/article/10.1007/s11139-015-9702-z\"\u003eA new theorem on the prime-counting function\u003c/a\u003e, Ramanujan J. 42(2017), 59-67. (See also  \u003ca href=\"http://arxiv.org/abs/1409.5685\"\u003earXiv:1409.5685 [math.NT]\u003c/a\u003e, 2014.)",
				"Zhi-Wei Sun, \u003ca href=\"http://maths.nju.edu.cn/~zwsun/176r.pdf\"\u003eConjectures on representations involving primes\u003c/a\u003e, in: M. Nathanson (ed.), Combinatorial and Additive Number Theory II: CANT, New York, NY, USA, 2015 and 2016, Springer Proc. in Math. \u0026 Stat., Vol. 220, Springer, New York, 2017, pp. 279-310. (See also \u003ca href=\"http://arxiv.org/abs/1211.1588\"\u003earXiv:1211.1588 [math.NT]\u003c/a\u003e, 2012-2017.)"
			],
			"example": [
				"a(1) = 2 since pi(2)*pi(2*1) = 1^2.",
				"a(2) = 27 since pi(27)*pi(27*2) = 9*16 = 12^2.",
				"a(8) = 5 since pi(5)*pi(5*8) = 3*12 = 6^2.",
				"a(9) = 96 since pi(96)*pi(96*9) = 24*150 = 60^2.",
				"a(675) = 1465650 since pi(1465650)*pi(1465650*675) = 111747*50331648 = 5624410669056 = 2371584^2.",
				"a(946) = 1922745 since pi(1922745)*pi(1922745*946) = 143599*89749375 = 12887920500625 = 3589975^2."
			],
			"mathematica": [
				"SQ[n_]:=IntegerQ[Sqrt[n]]",
				"Do[k=1; Label[bb]; k=k+1; If[SQ[PrimePi[k]*PrimePi[k*n]], Goto[aa], Goto[bb]]; Label[aa]; Print[n, \" \", k]; Continue,{n,1,60}]"
			],
			"program": [
				"(PARI)  main(size) = {v=vector(size);for(t=1,size,v[t]=1;until(issquare(primepi(v[t])*primepi(t*v[t])),v[t]++));return(v);} \\\\ _Anders Hellström_, Jul 05 2015"
			],
			"xref": [
				"Cf. A000040, A000290, A000720, A259712."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Zhi-Wei Sun_, Jul 05 2015",
			"references": 7,
			"revision": 32,
			"time": "2017-12-17T10:07:53-05:00",
			"created": "2015-07-05T16:07:21-04:00"
		}
	]
}