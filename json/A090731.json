{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A090731",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 90731,
			"data": "2,23,527,12098,277727,6375623,146361602,3359941223,77132286527,1770682648898,40648568638127,933146396028023,21421718540006402,491766380024119223,11289205022014735727,259159949126314802498",
			"name": "a(n) = 23a(n-1) - a(n-2), starting with a(0) = 2 and a(1) = 23.",
			"comment": [
				"A Chebyshev T-sequence with Diophantine property.",
				"a(n) gives the general (nonnegative integer) solution of the Pell equation a^2 - 21*(5*b)^2 =+4 with companion sequence b(n)=A097778(n-1), n\u003e=1; b(0):=0."
			],
			"reference": [
				"O. Perron, \"Die Lehre von den Kettenbruechen, Bd.I\", Teubner, 1954, 1957 (Sec. 30, Satz 3.35, p. 109 and table p. 108)."
			],
			"link": [
				"Indranil Ghosh, \u003ca href=\"/A090731/b090731.txt\"\u003eTable of n, a(n) for n = 0..733\u003c/a\u003e",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Rea#recur1\"\u003eIndex entries for recurrences a(n) = k*a(n - 1) +/- a(n - 2)\u003c/a\u003e",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (23,-1)."
			],
			"formula": [
				"a(n) = S(n, 23) - S(n-2, 23) = 2*T(n, 23/2) with S(n, x) := U(n, x/2), S(-1, x) := 0, S(-2, x) := -1. S(n, 23)=A097778(n). U-, resp. T-, are Chebyshev's polynomials of the second, resp. first, case. See A049310 and A053120.",
				"a(n) = ap^n + am^n, with ap := (23+5*sqrt(21))/2 and am := (23-5*sqrt(21))/2.",
				"G.f.: (2-23*x)/(1-23*x+x^2)."
			],
			"example": [
				"(x;y) = (0;2), (23;1), (527;23), (12098;528), ... give the",
				"nonnegative integer solutions to x^2 - 21*(5*y)^2 = 4."
			],
			"mathematica": [
				"a[0] = 2; a[1] = 23; a[n_] := 23a[n - 1] - a[n - 2]; Table[ a[n], {n, 0, 15}] (* _Robert G. Wilson v_, Jan 30 2004 *)",
				"LinearRecurrence[{23,-1},{2,23},30] (* _Harvey P. Dale_, Feb 20 2012 *)"
			],
			"program": [
				"(Sage) [lucas_number2(n,23,1) for n in range(0,20)] # _Zerinvary Lajos_, Jun 26 2008"
			],
			"xref": [
				"Cf. A037088, A051502.",
				"a(n)=sqrt(4 + 21*(5*A097778(n-1))^2), n\u003e=1.",
				"Cf. A077428, A078355 (Pell +4 equations)."
			],
			"keyword": "easy,nonn",
			"offset": "0,1",
			"author": "Nikolay V. Kosinov (kosinov(AT)unitron.com.ua), Jan 18 2004",
			"ext": [
				"Chebyshev and Pell comments from _Wolfdieter Lang_, Sep 10 2004"
			],
			"references": 4,
			"revision": 27,
			"time": "2019-12-07T12:18:24-05:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}