{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A145706",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 145706,
			"data": "1,0,1,0,1,-1,2,-1,2,-1,3,-2,4,-2,5,-4,6,-5,8,-6,11,-8,13,-10,16,-14,20,-17,24,-21,31,-26,37,-32,44,-41,54,-49,64,-59,79,-72,94,-86,111,-106,132,-126,156,-149,187,-178,219,-210,257,-251,302,-295,352",
			"name": "Expansion of chi(-x^5) / chi(-x^2) in powers of x where chi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A145706/b145706.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(1/8) * eta(q^4) * eta(q^5) / (eta(q^2) * eta(q^10)) in powers of q.",
				"Euler transform of period 20 sequence [ 0, 1, 0, 0, -1, 1, 0, 0, 0, 1, 0, 0, 0, 1, -1, 0, 0, 1, 0, 0, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (1280 t)) = f(t) where q = exp(2 Pi i t).",
				"G.f.: Product_{k\u003e0} (1 - x^(10*k - 5)) / (1 - x^(4*k - 2)).",
				"a(n) = (-1)^n * A139631(n) = A145704(2*n) = A145705(2*n)."
			],
			"example": [
				"G.f. = 1 + x^2 + x^4 - x^5 + 2*x^6 - x^7 + 2*x^8 - x^9 + 3*x^10 - 2*x^11 + ...",
				"G.f. = 1/q + q^15 + q^31 - q^39 + 2*q^47 - q^55 + 2*q^63 - q^71 + 3*q^79 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ x^5, x^10] QPochhammer[ -x^2, x^2], {x, 0, n}]; (* _Michael Somos_, Sep 06 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^4 + A) * eta(x^5 + A) / (eta(x^2 + A) * eta(x^10 + A)), n))};"
			],
			"xref": [
				"Cf. A139631, A145704, A145705."
			],
			"keyword": "sign",
			"offset": "0,7",
			"author": "_Michael Somos_, Oct 17 2008, Oct 20 2008",
			"references": 3,
			"revision": 13,
			"time": "2021-03-12T22:24:45-05:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}