{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A118343",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 118343,
			"data": "1,1,0,1,1,0,1,2,4,0,1,3,9,20,0,1,4,15,48,113,0,1,5,22,85,282,688,0,1,6,30,132,519,1762,4404,0,1,7,39,190,837,3330,11488,29219,0,1,8,49,260,1250,5516,22135,77270,199140,0,1,9,60,343,1773,8461,37404,151089,532239,1385904,0",
			"name": "Triangle, read by rows, where diagonals are successive self-convolutions of A108447.",
			"comment": [
				"A108447 equals the central terms of pendular triangle A118340 and the diagonals of this triangle form the semi-diagonals of the triangle A118340. Row sums equal A054727, the number of forests of rooted trees with n nodes on a circle without crossing edges."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A118343/b118343.txt\"\u003eRows n = 0..50 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"Since g.f. G=G(x) of A108447 satisfies: G = 1 - x*G + x*G^2 + x*G^3 then T(n,k) = T(n-1,k) - T(n-1,k-1) + T(n,k-1) + T(n+1,k-1). Also, a recurrence involving antidiagonals is: T(n,k) = T(n-1,k) + Sum_{j=1..k} [2*T(n-1+j,k-j) - T(n-2+j,k-j)] for n\u003ek\u003e=0.",
				"Sum_{k=0..n} T(n,k) = [n=0] + A054727(n) = [n=0] + Sum_{j=1..n} binomial(n, j-1)*binomial(3*n-2*j-1, n-j)/(2*n-j). - _G. C. Greubel_, Mar 17 2021"
			],
			"example": [
				"Show: T(n,k) = T(n-1,k) - T(n-1,k-1) + T(n,k-1) + T(n+1,k-1)",
				"at n=8,k=4: T(8,4) = T(7,4) - T(7,3) + T(8,3) + T(9,3)",
				"or 837 = 519 - 132 + 190 + 260.",
				"Triangle begins:",
				"  1;",
				"  1, 0;",
				"  1, 1,  0;",
				"  1, 2,  4,   0;",
				"  1, 3,  9,  20,    0;",
				"  1, 4, 15,  48,  113,    0;",
				"  1, 5, 22,  85,  282,  688,     0;",
				"  1, 6, 30, 132,  519, 1762,  4404,      0;",
				"  1, 7, 39, 190,  837, 3330, 11488,  29219,      0;",
				"  1, 8, 49, 260, 1250, 5516, 22135,  77270, 199140,       0;",
				"  1, 9, 60, 343, 1773, 8461, 37404, 151089, 532239, 1385904, 0;"
			],
			"maple": [
				"T:= proc(n, k) option remember;",
				"      if k\u003c0 or  k\u003en then 0;",
				"    elif k=0 then 1;",
				"    elif k=n then 0;",
				"    else T(n-1, k) -T(n-1, k-1) +T(n, k-1) +T(n+1, k-1);",
				"      fi; end:",
				"seq(seq(T(n, k), k=0..n), n=0..12); # _G. C. Greubel_, Mar 17 2021"
			],
			"mathematica": [
				"T[n_, k_]:= T[n, k]= If[k\u003c0 || k\u003en, 0, If[k==0, 1, If[k==n, 0, T[n-1, k] -T[n-1, k-1] +T[n, k-1] +T[n+1, k-1] ]]];",
				"Table[T[n, k], {n, 0, 10}, {k, 0, n}]//Flatten (* _G. C. Greubel_, Mar 17 2021 *)"
			],
			"program": [
				"(PARI) {T(n,k)=polcoeff((serreverse(x*(1-x+sqrt((1-x)*(1-5*x)+x*O(x^k)))/2/(1-x))/x)^(n-k),k)}",
				"(Sage)",
				"@CachedFunction",
				"def T(n, k):",
				"    if (k\u003c0 or k\u003en): return 0",
				"    elif (k==0): return 1",
				"    elif (k==n): return 0",
				"    else: return T(n-1, k) -T(n-1, k-1) +T(n, k-1) +T(n+1, k-1)",
				"flatten([[T(n, k) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, Mar 17 2021"
			],
			"xref": [
				"Cf. A054727 (row sums), A108447, A118340."
			],
			"keyword": "nonn,tabl",
			"offset": "0,8",
			"author": "_Paul D. Hanna_, Apr 26 2006",
			"references": 4,
			"revision": 8,
			"time": "2021-03-18T11:28:46-04:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}