{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A267437",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 267437,
			"data": "11,23,67,151,275,487,963,2039,4211,8327,16291,32407,65363,131623,263043,524087,1046579,2095559,4196707,8394199,16778003,33544039,67096899,134226551,268468211,536886023,1073691427,2147403031,4294987475,8590116007,17180010243",
			"name": "A linear recurrence related to the elliptic curves y^2 = x^3 -35*a^2*x - 98*a^3 with a = -1, -5, -6, -17, or -111.",
			"comment": [
				"Abatzoglou, Silverberg, Sutherland, \u0026 Wong give a quasi-quadratic algorithm for finding primes in this sequence, which relies on a correspondence between the Frobenius endomorphism of one of the five elliptic curves given above and complex multiplication in Z[(1 + sqrt(-7))/2]."
			],
			"link": [
				"Charles R Greathouse IV, \u003ca href=\"/A267437/b267437.txt\"\u003eTable of n, a(n) for n = 2..3319\u003c/a\u003e",
				"Alexander Abatzoglou, Alice Silverberg, Andrew V. Sutherland, and Angela Wong, \u003ca href=\"http://dx.doi.org/10.2140/obs.2013.1.1\"\u003eDeterministic elliptic curve primality proving for a special sequence of numbers\u003c/a\u003e, Tenth Algorithmic Number Theory Symposium (ANTS X, 2012), pp. 1-20.",
				"Alexander Abatzoglou, Alice Silverberg, Andrew V. Sutherland, Angela Wong, \u003ca href=\"http://arxiv.org/abs/1202.3695\"\u003eDeterministic elliptic curve primality proving for a special sequence of numbers\u003c/a\u003e, arXiv:1202.3695 [math.NT], 2012.",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-7,8,-4)."
			],
			"formula": [
				"a(n) = 4*a(n-1) - 7*a(n-2) + 8*a(n-3) - 4*a(n-4).",
				"a(n) ~ 4*2^n.",
				"G.f.: x^2*(11 - 21*x + 52*x^2 - 44*x^3)/((1 - x)*(1 - 2*x)*(1 - x + 2*x^2)). - _Bruno Berselli_, Jan 24 2016",
				"a(n) = 1 + 2^(2+n) + 2*(1/2-(i*sqrt(7))/2)^n + 2*(1/2+(i*sqrt(7))/2)^n where i=sqrt(-1). - _Colin Barker_, Jul 02 2017"
			],
			"mathematica": [
				"RecurrenceTable[{a[n] == 4 a[n - 1] - 7 a[n - 2] + 8 a[n - 3] - 4 a[n - 4], a[2] == 11, a[3] == 23, a[4] == 67, a[5] == 151}, a, {n, 2, 30}] (* _Michael De Vlieger_, Jan 24 2016 *)",
				"LinearRecurrence[{4, -7, 8, -4}, {11, 23, 67, 151}, 40] (* _Vincenzo Librandi_, Jan 27 2016 *)"
			],
			"program": [
				"(PARI) a(n)=([0,1,0,0;0,0,1,0;0,0,0,1;-4,8,-7,4]^n*[9;11;11;23])[1,1]",
				"(PARI) first(n)=if(n\u003c5,return(first(5)[1..n-1])); my(v=vector(n-1)); v[1]=11; v[2]=23; v[3]=67; v[4]=151; for(k=5,#v, v[k]=4*v[k-1]-7*v[k-2]+8*v[k-3]-4*v[k-4]); v",
				"(MAGMA) I:=[11,23,67,151]; [n le 4 select I[n] else 4*Self(n-1)-7*Self(n-2)+8*Self(n-3)-4*Self(n-4): n in [1..31]]; // _Vincenzo Librandi_, Jan 27 2016",
				"(PARI) i=I; vector(50, n, n++; round(1 + 2^(2+n) + 2*(1/2-(i*sqrt(7))/2)^n + 2*(1/2+(i*sqrt(7))/2)^n)) \\\\ _Colin Barker_, Jul 02 2017"
			],
			"xref": [
				"Cf. A267438, A267439."
			],
			"keyword": "nonn,easy",
			"offset": "2,1",
			"author": "_Charles R Greathouse IV_, Jan 15 2016",
			"references": 3,
			"revision": 26,
			"time": "2018-03-02T02:14:56-05:00",
			"created": "2016-01-24T16:06:30-05:00"
		}
	]
}