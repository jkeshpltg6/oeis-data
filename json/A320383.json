{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A320383",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 320383,
			"data": "2,6,10,4,16,3,11,7,30,36,40,21,23,13,58,12,33,7,36,26,82,88,8,25,102,106,108,112,126,130,136,69,74,150,156,81,83,86,178,36,95,96,49,66,5,222,226,228,232,119,30,250,256,131,67,270,276,40,141,73,51,155,156,79,11,168,346,348,352,179,366,124",
			"name": "Multiplicative order of 3/2 modulo n-th prime.",
			"comment": [
				"Let p = prime(n). a(n) is the smallest positive k such that p divides 3^k - 2^k. Obviously, a(n) divides p - 1. If a(n) = p - 1, then p is listed in A320384.",
				"If p == 1, 5, 19, 23 (mod 24), then 3/2 is a quadratic residue modulo p, so a(n) divides (p - 1)/2.",
				"By Zsigmondy's theorem, for each k \u003e=2 there is a prime that divides 3^k-2^k but not 3^j-2^j for j \u003c k.  Therefore each integer \u003e= 2 appears in the sequence at least once. - _Robert Israel_, Apr 20 2021"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A320383/b320383.txt\"\u003eTable of n, a(n) for n = 3..10000\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Multiplicative_order\"\u003eMultiplicative order\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Zsigmondy%27s_theorem\"\u003eZsigmondy's theorem\u003c/a\u003e"
			],
			"example": [
				"Let ord(n,p) be the multiplicative order of n modulo p.",
				"3/2 == 4 (mod 5), so a(3) = ord(4,5) = 2.",
				"3/2 == 5 (mod 7), so a(4) = ord(5,7) = 6.",
				"3/2 == 7 (mod 11), so a(5) = ord(7,11) = 10.",
				"3/2 == 8 (mod 13), so a(6) = ord(8,13) = 4."
			],
			"maple": [
				"f:= proc(n) local p; p:= ithprime(n); numtheory:-order(3/2 mod p,p) end proc:",
				"map(f, [$3..100]); # _Robert Israel_, Apr 20 2021"
			],
			"program": [
				"(PARI) forprime(p=5,10^3,print1(znorder(Mod(3/2,p)),\", \")) \\\\ _Joerg Arndt_, Oct 13 2018"
			],
			"xref": [
				"Cf. A001047, A211242, A320384."
			],
			"keyword": "nonn,look",
			"offset": "3,1",
			"author": "_Jianing Song_, Oct 12 2018",
			"references": 3,
			"revision": 15,
			"time": "2021-04-20T22:36:14-04:00",
			"created": "2018-10-13T15:35:51-04:00"
		}
	]
}