{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A055471",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 55471,
			"data": "1,2,3,4,5,6,7,8,9,10,11,12,15,20,24,30,36,40,50,60,70,80,90,100,101,102,104,105,110,111,112,115,120,128,132,135,140,144,150,175,200,208,210,212,216,220,224,240,250,300,306,312,315,360,384,400,432,480,500",
			"name": "Divisible by the product of its nonzero digits.",
			"comment": [
				"If n is the term then 10n also is. - _Zak Seidov_, Jun 09 2013",
				"De Koninck and Luca showed that the number of terms of this sequence below x is at least x^0.495 but at most x^0.901 for sufficiently large x. - _Tomohiro Yamada_, Nov 18 2017",
				"This sequence begins with a run of 12 consecutive terms, from 1 to 12. The maximal length of a run of consecutive integer terms is 13. The smallest example of such a run begins with 1111011111000 and ends with 1111011111012 (Diophante link). - _Bernard Schott_, Apr 26 2019",
				"These numbers are called \"nombres prodigieux\" on the French site Diophante. - _Bernard Schott_, Apr 26 2019"
			],
			"link": [
				"Marius A. Burtea, \u003ca href=\"/A055471/b055471.txt\"\u003eTable of n, a(n) for n = 1..11442\u003c/a\u003e (terms 1..1000 from Zak Seidov)",
				"Jean-Marie De Koninck and Florian Luca, \u003ca href=\"https://doi.org/10.4171/PM/1777\"\u003ePositive integers divisible by the product of their nonzero digits\u003c/a\u003e, Port. Math. 64 (2007) 75-85. (This proof for upper bounds contains an error. See the paper below)",
				"Jean-Marie De Koninck and Florian Luca, \u003ca href=\"https://doi.org/10.4171/PM/1999\"\u003eCorrigendum to \"Positive integers divisible by the product of their nonzero digits\", Portugaliae Math. 64 (2007), 1: 75-85\u003c/a\u003e, Port. Math. 74 (2017), 169-170.",
				"Diophante, \u003ca href=\"http://www.diophante.fr/problemes-par-themes/arithmetique-et-algebre/a3-nombres-remarquables/3578-a365-les-nombres-prodigieux\"\u003e, A365, les nombres prodigieux\u003c/a\u003e, July 2016."
			],
			"mathematica": [
				"Select[Range[5000], IntegerQ[ #/(Times @@ Select[IntegerDigits[ # ], # \u003e 0 \u0026])] \u0026] (* _Alonso del Arte_, Aug 04 2004 *)"
			],
			"program": [
				"(MATLAB) m=1;",
				"for n=1:1000",
				"    v=dec2base(n,10)-'0';",
				"    v = v(v~=0);",
				"    if mod(n,prod(v))==0",
				"        sol(m)=n;",
				"        m=m+1;",
				"    end",
				"end",
				"sol % _Marius A. Burtea_, May 07 2019",
				"(MAGMA) m:=1;sol:=[];",
				"for n in [1..1000] do",
				"      v:=Intseq(n,10);",
				"       while \u0026*v eq 0 do; Exclude(~v, 0); end while;",
				"     if n mod \u0026*(v) eq 0  then ; sol[m]:=n; m:=m+1; end if;",
				"end for;",
				"sol // _Marius A. Burtea_, May 07 2019",
				"(Python)",
				"from math import prod",
				"def ok(n): return n \u003e 0 and n%prod([int(d) for d in str(n) if d!='0']) == 0",
				"print(list(filter(ok, range(501)))) # _Michael S. Branicky_, Jul 27 2021"
			],
			"xref": [
				"Superset of A007602.",
				"Cf. A007088."
			],
			"keyword": "nonn,base",
			"offset": "1,2",
			"author": "_Robert G. Wilson v_, Jul 05 2000",
			"ext": [
				"Corrected by _Patrick De Geest_, Aug 15 2000"
			],
			"references": 9,
			"revision": 57,
			"time": "2021-07-27T14:41:11-04:00",
			"created": "2000-07-22T03:00:00-04:00"
		}
	]
}