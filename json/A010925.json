{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A010925",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 10925,
			"data": "5,21,88,368,1538,6427,26857,112229,468978,1959746,8189306,34221135,143001871,597570335,2497102330,10434788478,43604464772,182212543365,761422279419,3181800093939,13295975323332,55560674643076,232174661258332,970201922073653,4054239874815929,16941690784755705,70795240417122019",
			"name": "Pisot sequence T(5,21), a(n) = floor( a(n-1)^2/a(n-2) ).",
			"comment": [
				"Comments from Pab Ter (pabrlos(AT)yahoo.com), May 23 2004, with updates from _N. J. A. Sloane_, Aug 05 2016: (Start)",
				"Different from A019992. The two sequences differ from n=26 on (A010925(26) = 70795240417122019 != 70795240417122020 = A019992(26)).",
				"From Boyd's paper \"Linear recurrence relations for some generalized Pisot sequences\", T(5,21) satisfies the rational generating function F(x)/(1+x-x*F(x)), with F(x) = 5 + x - x^2 - x^4 - x^26 - x^2048, a 2049th-order recurrence; and not the A019992 generating function: F(x)/(1+x-x*F(x)), with F(x) = 5 + x - x^2 - x^4, which gives the 5th-order recurrence for A019992.",
				"The g.f. F(x)/(1+x-x*F(x)) with F(x) = 5 + x - x^2 - x^4 - x^26 - x^2048 is not in lowest terms, however, and a factor of 1+x can be canceled. The lowest-order recurrence satisfied by this sequence has order 2048.",
				"This and other examples show that it is essential to reject conjectured generating functions for Pisot sequences until a proof or reference is provided. (End)"
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A010925/b010925.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"D. W. Boyd, \u003ca href=\"http://matwbn.icm.edu.pl/ksiazki/aa/aa32/aa32110.pdf\"\u003ePisot sequences which satisfy no linear recurrences\u003c/a\u003e, Acta Arith. 32 (1) (1977) 89-98",
				"D. W. Boyd, \u003ca href=\"http://matwbn.icm.edu.pl/ksiazki/aa/aa34/aa3444.pdf\"\u003eSome integer sequences related to the Pisot sequences\u003c/a\u003e, Acta Arithmetica, 34 (1979), 295-305",
				"D. W. Boyd, \u003ca href=\"http://matwbn.icm.edu.pl/ksiazki/aa/aa47/aa4712.pdf\"\u003eOn linear recurrence relations satisfied by Pisot sequences\u003c/a\u003e, Acta Arithm. 47 (1) (1986) 13-27; 54 (1990), 255-256.",
				"D. W. Boyd, \u003ca href=\"http://matwbn.icm.edu.pl/ksiazki/aa/aa48/aa4825.pdf\"\u003ePisot sequences which satisfy no linear recurrences. II\u003c/a\u003e, Acta Arithm. 48 (1987) 191-195.",
				"D. W. Boyd, \u003ca href=\"https://www.researchgate.net/profile/David_Boyd7/publication/262181133_Linear_recurrence_relations_for_some_generalized_Pisot_sequences_-_annotated_with_corrections_and_additions/links/00b7d536d49781037f000000.pdf\"\u003eLinear recurrence relations for some generalized Pisot sequences\u003c/a\u003e, in Advances in Number Theory (Kingston ON, 1991), pp. 333-340, Oxford Univ. Press, New York, 1993; with updates from 1996 and 1999.",
				"D. G. Cantor, \u003ca href=\"http://www.numdam.org/item?id=ASENS_1976_4_9_2_283_0\"\u003eOn families of Pisot E-sequences\u003c/a\u003e, Ann. Sci. Ecole Nat. Sup. 9 (2) (1976) 283-308",
				"M. J. DeLeon, \u003ca href=\"http://dx.doi.org/10.1515/crll.1971.249.20\"\u003ePisot Sequences\u003c/a\u003e, J. Reine Angew. Mathem. 249 (1971) 20-30",
				"Charles Pisot, \u003ca href=\"http://archive.numdam.org/article/THESE_1938__203__1_0.pdf\"\u003eLa répartition modulo un et les nombres algébriques\u003c/a\u003e, Thesis (1938).",
				"Charles Pisot, \u003ca href=\"http://www.numdam.org/item?id=ASNSP_1938_2_7_3-4_205_0\"\u003eLa répartition modulo 1 et les nombres algébriques\u003c/a\u003e, Ann. Scuola Norm. Sup. Pisa, 7 (1938), 205-248."
			],
			"formula": [
				"G.f.: F(x)/(1+x-x*F(x)), with F(x) = 5 + x - x^2 - x^4 - x^26 - x^2048 (D. W. Boyd). - Pab Ter (pabrlos(AT)yahoo.com), May 23 2004"
			],
			"mathematica": [
				"nxt[{a_,b_}]:={b,Floor[b^2/a]}; NestList[nxt,{5,21},30][[All,1]] (* _Harvey P. Dale_, May 15 2017 *)"
			],
			"program": [
				"(PARI) pisotT(nmax, a1, a2) = {",
				"  a=vector(nmax); a[1]=a1; a[2]=a2;",
				"  for(n=3, nmax, a[n] = floor(a[n-1]^2/a[n-2]));",
				"  a",
				"}",
				"pisotT(50, 5, 21) \\\\ _Colin Barker_, Jul 27 2016"
			],
			"keyword": "nonn",
			"offset": "0,1",
			"author": "_Simon Plouffe_",
			"ext": [
				"More terms from Pab Ter (pabrlos(AT)yahoo.com), May 23 2004"
			],
			"references": 13,
			"revision": 45,
			"time": "2018-05-17T03:09:52-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}