{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A280513",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 280513,
			"data": "1,2,1,5,4,3,2,1,13,12,11,10,9,8,7,6,5,4,3,2,1,34,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74",
			"name": "Index sequence of the reverse block-fractal sequence A001468.",
			"comment": [
				"The sequence is the concatenation of blocks, the n-th of which, for n \u003e= 1, consists of the integers from F(2n+1) down to F(2) = 1, where F = A000045, the Fibonacci numbers. See A280511 for the definition of reverse block-fractal sequence. The index sequence (a(n)) of a reverse block-fractal sequence (s(n)) is defined here by a(n) = least k \u003e 0 such that (s(k), s(k+1), ..., s(k+n)) = (s(n), s(n-1), ..., s(1)).",
				"Let W be the Fibonacci word A096270.  Then a(n) = least k such that the reversal of the first n-block in W occurs in W beginning at the k-th term.  Since (a(n)) is unbounded, the reversal of every block in W occurs infinitely many times in W. - _Clark Kimberling_, Dec 17 2020"
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A280513/b280513.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e"
			],
			"example": [
				"A001468 = (1,2,1,2,2,1,2,1,2,2,1,2,2,...) = (s(1), s(2), ... ).",
				"(init. block #1) = (1); reversal (1) first occurs at s(1), so a(1) = 1;",
				"(init. block #2) = (1,2); rev. (2,1) first occurs at s(2), so a(2) = 2;",
				"(init. block #3) = (1,2,1); rev. (1,2,1) first occurs at s(1), so a(3) = 1;",
				"(init. block #4) = (1,2,1,2); rev. (2,1,2,1) first occurs at s(5), so a(4) = 5."
			],
			"mathematica": [
				"r = GoldenRatio; t = Table[Floor[(n + 1) r] - Floor[n*r], {n, 0, 420}]",
				"u = StringJoin[Map[ToString, t]]; breverse[seq_] :=",
				"Flatten[Last[Reap[NestWhile[# + 1 \u0026, 1, (StringLength[",
				"str = StringTake[seq, Min[StringLength[seq], #]]] == # \u0026\u0026 ! (Sow[",
				"StringPosition[seq, StringReverse[str], 1][[1]][[1]]]) === {}) \u0026]]]];",
				"breverse[u]  (* _Peter J. C. Moses_, Jan 02 2017 *)"
			],
			"xref": [
				"Cf. A000045, A001468, A096270."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Jan 06 2017",
			"references": 3,
			"revision": 23,
			"time": "2020-12-18T04:23:43-05:00",
			"created": "2017-01-07T11:58:31-05:00"
		}
	]
}