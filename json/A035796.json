{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A035796",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 35796,
			"data": "1,1,2,2,3,18,4,48,6,5,36,100,144,6,200,180,600,7,450,900,294,24,300,1800,8,882,7200,448,1200,1470,4410,9,1568,22050,648,7200,3136,1800,9408,10,14700,2592,16200,1960,56448,900,29400,6048,22050,18144",
			"name": "Words over signatures (derived from multisets and multinomials).",
			"comment": [
				"A reordering of A049009(n)=A049009(p(n)): distribution of words by numeric partition where the partition sequence: p(n)=[1],[2],[1,1],[3],[2,1],[1,1,1],[4],[3,1],[2,2],[2,1,1],... (A036036) is encoded by prime factorization ([P1,P2,P3,...] with P1 \u003e= P2 \u003e= P3 \u003e= ... is encoded as 2^P1 * 3^P2 * 5^P3 *...): ep(n)=2,4,6,8,12,30,16,24,36,60, ... (A036035(n)) and then sorted: s(m)=2,4,6,8,12,16,24,30,32,36,48,60,... (A025487(m)). Hence A035796(n) = A049009(s(m))."
			],
			"reference": [
				"M. Abramowitz and I. A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, 1964 (and various reprintings), p. 831."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A035796/b035796.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy]."
			],
			"formula": [
				"a(n) = A049009(p) where p is such that A036035(p) = A025487(n). [Corrected by _Andrew Howroyd_ and _Sean A. Irvine_, Oct 18 2020]"
			],
			"example": [
				"27 = a(5) + a(6) + a(9) since a8(4) = 3, a12(5) = 18, a30(8) = 6; 256 = a(7) + a(8) + a(11) + a(13) + a(22) = 4 + 48 + 36 + 144 + 24",
				"27 = a(5) + a(6) + a(9) = A049009(4) + A049009(5) + A049009(6) = 3 + 18 + 6 since A036035(4) = 8 = A025487(4+1), A036035(5) = 12 = A025487(5+1), A036035(6) = 30 = A025487(8+1);..."
			],
			"program": [
				"(PARI) \\\\ here P is A025487 as vector and C is A049009 by partition.",
				"GenS(lim)={my(L=List(), S=[1]); forprime(p=2, oo, listput(L, S); my(pp=vector(logint(lim, p), i, p^i)); S=concat([k*pp[1..min(if(k\u003e1, my(f=factor(k)[, 2]); f[#f], oo), logint(lim\\k, p))] | k\u003c-S]); if(!#S, return(Set(concat(L)))) )}",
				"P(n)={my(lim=1, v=[1]); while(#v\u003cn, lim*=4; v=GenS(lim)); v[1..n]}",
				"C(sig)={my(S=Set(sig)); (binomial(vecsum(sig), #sig)) * (#sig)! * vecsum(sig)! / (prod(k=1, #S, (#select(t-\u003et==S[k], sig))!) * prod(k=1, #sig, sig[k]!))}",
				"seq(n)={[C(factor(t)[,2]) | t\u003c-P(n)]} \\\\ _Andrew Howroyd_, Oct 18 2020"
			],
			"xref": [
				"Cf. A000312, A025487, A019575, A001700, A005651, A036035, A036036, A025487, A049009."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Alford Arnold_",
			"ext": [
				"More terms and additional comments from Antonio G. Astudillo (afg_astudillo(AT)hotmail.com), Jul 02 2001",
				"a(1)=1 inserted by _Andrew Howroyd_ and _Sean A. Irvine_, Oct 18 2020"
			],
			"references": 3,
			"revision": 20,
			"time": "2020-10-18T22:24:33-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}