{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A262809",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 262809,
			"data": "1,1,1,1,1,1,1,3,1,1,1,13,13,1,1,1,75,409,63,1,1,1,541,23917,16081,321,1,1,1,4683,2244361,10681263,699121,1683,1,1,1,47293,308682013,14638956721,5552351121,32193253,8989,1,1,1,545835,58514835289,35941784497263,117029959485121,3147728203035,1538743249,48639,1,1",
			"name": "Number A(n,k) of lattice paths from {n}^k to {0}^k using steps that decrement one or more components by one; square array A(n,k), n\u003e=0, k\u003e=0, read by antidiagonals.",
			"comment": [
				"Also, A(n,k) is the number of alignments for k sequences of length n each (Slowinski 1998).",
				"Row r \u003e 0 is asymptotic to sqrt(r*Pi) * (r^(r-1)/(r-1)!)^n * n^(r*n+1/2) / (2^(r/2) * exp(r*n) * (log(2))^(r*n+1)), or equivalently to sqrt(r) * (r^(r-1)/(r-1)!)^n * (n!)^r / (2^r * (Pi*n)^((r-1)/2) * (log(2))^(r*n+1)). - _Vaclav Kotesovec_, Mar 23 2016",
				"From _Vaclav Kotesovec_, Mar 23 2016: (Start)",
				"Column k \u003e 0 is asymptotic to sqrt(c(k)) * d(k)^n / (Pi*n)^((k-1)/2), where c(k) and d(k) are roots of polynomial equations of degree k, independent on n.",
				"---------------------------------------------------",
				"k               d(k)",
				"---------------------------------------------------",
				"2              5.8284271247461900976033774484193...",
				"3             56.9476283720414911685286267804411...",
				"4            780.2794068067951456595241495989622...",
				"5          13755.2719024115081712083954421541320...",
				"6         296476.9162644200814909862281498491264...",
				"7        7553550.6198338218721069097516499501996...",
				"8      222082591.6017202421029000117685530884167...",
				"9     7400694480.0494436216324852038000444393262...",
				"10  275651917450.6709238286995776605620357737005...",
				"---------------------------------------------------",
				"d(k) is a root of polynomial:",
				"---------------------------------------------------",
				"k=2, 1 - 6*d + d^2",
				"k=3, -1 + 3*d - 57*d^2 + d^3",
				"k=4, 1 - 12*d - 218*d^2 - 780*d^3 + d^4",
				"k=5, -1 + 5*d - 1260*d^2 - 3740*d^3 - 13755*d^4 + d^5",
				"k=6, 1 - 18*d - 5397*d^2 - 123696*d^3 + 321303*d^4 - 296478*d^5 + d^6",
				"k=7, -1 + 7*d - 24031*d^2 - 374521*d^3 - 24850385*d^4 + 17978709*d^5 - 7553553*d^6 + d^7",
				"k=8, 1 - 24*d - 102692*d^2 - 9298344*d^3 + 536208070*d^4 - 7106080680*d^5 - 1688209700*d^6 - 222082584*d^7 + d^8",
				"(End)",
				"A(n,k) is the number of binary matrices with k columns and any number of nonzero rows with n ones in every column. - _Andrew Howroyd_, Jan 23 2020"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A262809/b262809.txt\"\u003eAntidiagonals n = 0..48, flattened\u003c/a\u003e",
				"J. B. Slowinski, \u003ca href=\"http://www.neurociencias.org.ve/cont-cursos-laboratorio-de-neurociencias-luz/Slowinski1998%20phylogenetics.pdf\"\u003eThe Number of Multiple Alignments\u003c/a\u003e, Molecular Phylogenetics and Evolution 10:2 (1998), 264-266. doi:\u003ca href=\"http://dx.doi.org/10.1006/mpev.1998.0522\"\u003e10.1006/mpev.1998.0522\u003c/a\u003e"
			],
			"formula": [
				"A(n,k) = Sum_{j=0..k*n} Sum_{i=0..j} (-1)^i*C(j,i)*C(j-i,n)^k.",
				"A(n,k) = Sum_{i \u003e= 0} binomial(i,n)^k/2^(i+1). - _Peter Bala_, Jan 30 2018",
				"A(n,k) = Sum_{j=0..n*k} binomial(j,n)^k * Sum_{i=j..n*k} (-1)^(i-j) * binomial(i,j). - _Andrew Howroyd_, Jan 23 2020"
			],
			"example": [
				"A(2,2) = 13: [(2,2),(1,2),(0,2),(0,1),(0,0)], [(2,2),(1,2),(0,1),(0,0)], [(2,2),(1,2),(1,1),(0,1),(0,0)], [(2,2),(1,2),(1,1),(0,0)], [(2,2),(1,2),(1,1),(1,0),(0,0)], [(2,2),(2,1),(1,1),(0,1),(0,0)], [(2,2),(2,1),(1,1),(0,0)], [(2,2),(2,1),(1,1),(1,0),(0,0)], [(2,2),(2,1),(2,0),(0,1),(0,0)], [(2,2),(2,1),(1,0),(0,0)], [(2,2),(1,1),(0,1),(0,0)], [(2,2),(1,1),(0,0)], [(2,2),(1,1),(1,0),(0,0)].",
				"Square array A(n,k) begins:",
				"  1, 1,    1,        1,             1,                   1, ...",
				"  1, 1,    3,       13,            75,                 541, ...",
				"  1, 1,   13,      409,         23917,             2244361, ...",
				"  1, 1,   63,    16081,      10681263,         14638956721, ...",
				"  1, 1,  321,   699121,    5552351121,     117029959485121, ...",
				"  1, 1, 1683, 32193253, 3147728203035, 1050740615666453461, ..."
			],
			"maple": [
				"A:= (n, k)-\u003e add(add((-1)^i*binomial(j, i)*",
				"     binomial(j-i, n)^k, i=0..j), j=0..k*n):",
				"seq(seq(A(n, d-n), n=0..d), d=0..10);"
			],
			"mathematica": [
				"A[_, 0] =  1; A[n_, k_] := Sum[Sum[(-1)^i*Binomial[j, i]*Binomial[j - i, n]^k, {i, 0, j}], {j, 0, k*n}];",
				"Table[Table[A[n, d - n], {n, 0, d}], {d, 0, 10}] // Flatten (* _Jean-François Alcover_, Jul 22 2016, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(PARI) T(n,k) = {my(m=n*k); sum(j=0, m, binomial(j,n)^k*sum(i=j, m, (-1)^(i-j)*binomial(i, j)))} \\\\ _Andrew Howroyd_, Jan 23 2020"
			],
			"xref": [
				"Columns: A000012 (k=0 and k=1), A001850 (k=2), A126086 (k=3), A263064 (k=4), A263065 (k=5), A263066 (k=6), A263067 (k=7), A263068 (k=8), A263069 (k=9), A263070 (k=10).",
				"Rows: A000012 (n=0), A000670 (n=1), A055203 (n=2), A062208 (n=3), A062205 (n=4), A263061 (n=5), A263062 (n=6), A062204 (n=7), A263063 (n=8), A263071 (n=9), A263072 (n=10).",
				"Main diagonal: A262810.",
				"Cf. A210472, A225094, A227578, A227655, A229142, A229345, A263159, A316674.",
				"Cf. A188392, A330942, A331461, A331637."
			],
			"keyword": "nonn,tabl",
			"offset": "0,8",
			"author": "_Alois P. Heinz_, Oct 02 2015",
			"references": 33,
			"revision": 49,
			"time": "2020-01-24T07:51:55-05:00",
			"created": "2015-10-02T17:29:40-04:00"
		}
	]
}