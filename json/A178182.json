{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A178182",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 178182,
			"data": "4,4,12,1,20,12,28,8,36,20,44,6,52,28,60,16,68,36,76,5,84,44,92,24,100,52,108,14,116,60,124,32,132,68,140,9,148,76,156,40,164,84,172,22,180,92,188,48,196,100,204,13,212,108,220,56,228,116,236,30,244,124,252,64,260,132,268,17,276,140,284,72,292,148,300,38,308,156,316,80",
			"name": "Minimal polynomials of sin(2Pi/n) are mapped to those of cos(2Pi/a(n)).",
			"comment": [
				"The minimal polynomials of cos(2*Pi/n) are treated, e.g. in the Lehmer, Niven and Watkins-Zeitlin references. Lehmer and Niven call them psi_n(x) (eq. (1) and Lemma 3.8, p.37, respectively). In the latter reference they are called Psi_n(x), and we call them Psi(n,x). By definition (Niven, p. 28) these are monic, rational polynomials which have as a root cos(2*Pi/n) and are of minimal degree. They are irreducible (Niven p. 37, Lemma 3.8). See also A181875 for more details and a link with Psi(n,x), n=1..30.",
				"The minimal polynomials of sin(2*Pi/n) are treated, e.g. in the Lehmer and Niven references. Lehmer's theorem 2 is, however, incorrect. See A181872 and the link there for a counterexample. In this link one can also find these polynomials, called Pi(n,x), for n=1..30.",
				"The sequence a(n) translates these polynomials: Pi(n,x) = Psi(a(n),x), n \u003e= 1. This translation is based on the trigonometric identity: sin(2*Pi/n) = cos(2*Pi*r(n)), with r(n):=|(4-n)/(4*n)|.",
				"a(n):=denominator(r(n)) (in lowest terms). Note that the degrees agree with those given in the Niven reference, Theorem 3.9, p. 37."
			],
			"reference": [
				"I. Niven, Irrational Numbers, The Math. Assoc. of America, second printing, 1963, distributed by John Wiley and Sons."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A178182/b178182.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"D. H. Lehmer, \u003ca href=\"http://www.jstor.org/stable/2301023\"\u003eA Note on Trigonometric Algebraic Numbers\u003c/a\u003e, Am. Math. Monthly 40,3 (1933) 165-6.",
				"Pinthira Tangsupphathawat, Takao Komatsu, Vichian Laohakosol, \u003ca href=\"https://www.emis.de/journals/JIS/VOL21/Laohakosol/lao8.html\"\u003eMinimal Polynomials of Algebraic Cosine Values, II\u003c/a\u003e, J. Int. Seq., Vol. 21 (2018), Article 18.9.5.",
				"W. Watkins and J. Zeitlin, \u003ca href=\"http://www.jstor.org/stable/2324301\"\u003eThe Minimal Polynomial of cos(2Pi/n)\u003c/a\u003e, Am. Math. Monthly 100,5 (1993) 471-4."
			],
			"formula": [
				"a(n) = denominator(|(n-4)/(4*n)|), n \u003e= 1.",
				"a(n) = 4*n/gcd(n-4,16). a(n) = 4*n if n is odd; if n is even then a(n) = 2*n if n/2 == 1, 3, 5, 7 (mod 8), a(n) = n if n/2 == 0, 4 (mod 8), a(n) = n/2 if n/2 == 6 (mod 8) and a(n) = n/4 if n/2 == 2 (mod 8). - _Wolfdieter Lang_, Dec 01 2013",
				"a(2*n)/(2*n) = 1/4, 1/2, 1, and 2, for n == 2 (mod 8), 6 (mod 8), 0 (mod 4), and 1 (mod 2), for n \u003e= 1. The reciprocal can be used in a formula for the zeros of the minimal polynomials of 2*sin(Pi/2) (A228786). See A327921. - _Wolfdieter Lang_, Nov 02 2019"
			],
			"example": [
				"Pi(5,x) = Psi(20,x) because sin(2*Pi/5) = cos(2*Pi/20)."
			],
			"mathematica": [
				"Array[4 #/GCD[# - 4, 16] \u0026, 80] (* _Michael De Vlieger_, Feb 07 2019 *)"
			],
			"xref": [
				"Cf. A181872, A228786, A327921."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_Wolfdieter Lang_, Jan 11 2011",
			"references": 7,
			"revision": 21,
			"time": "2019-11-03T16:06:21-05:00",
			"created": "2010-11-12T14:26:41-05:00"
		}
	]
}