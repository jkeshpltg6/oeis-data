{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A305929",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 305929,
			"data": "0,1,2,3,4,6,7,12,13,14,17,34,5,8,9,10,25,26,36,11,15,16,18,19,20,21,22,23,24,28,29,30,31,32,48,54,68,41,45,56,33,35,37,44,49,53,58,64,65,38,39,40,43,46,51,52,59,61,67,82,83,106,42,47,62,66,69,72,73,76,84,89,144,27,50",
			"name": "Irregular table: row n \u003e= 0 lists all k \u003e= 0 such that the decimal representation of 9^k has n digits '0' (conjectured).",
			"comment": [
				"The set of (nonempty) rows forms a partition of the nonnegative integers.",
				"Read as a flattened sequence, a permutation of the nonnegative integers.",
				"In the same way, another choice of (basis, digit, base) = (m, d, b) different from (9, 0, 10) will yield a similar partition of the nonnegative integers, trivial if m is a multiple of b.",
				"It remains an open problem to provide a proof that the rows are complete, in the same way as each of the terms of A020665 is unproved.",
				"We can also decide that the rows are to be truncated as soon as no term is found within a sufficiently large search limit. (For all of the displayed rows, there is no additional term up to many orders of magnitude beyond the last term.) That way the rows are well-defined, but it is no longer guaranteed to have a partition of the integers.",
				"The author finds this sequence \"nice\", i.e., appealing (as well as, e.g., the variant A305933 for basis 3) in view of the idea of partitioning the integers in such an elementary yet highly nontrivial way, and the remarkable fact that the rows are just roughly one line long. Will this property remain for large n, or else, how will the row lengths evolve?"
			],
			"link": [
				"M. F. Hasler, \u003ca href=\"https://oeis.org/wiki/Zeroless_powers\"\u003eZeroless powers.\u003c/a\u003e. OEIS Wiki, March 2014"
			],
			"formula": [
				"Row n consists of the integers in (row n of A305933 divided by 2)."
			],
			"example": [
				"The table reads:",
				"n \\ k's",
				"0 : 0, 1, 2, 3, 4, 6, 7, 12, 13, 14, 17, 34 (= A030705)",
				"1 : 5, 8, 9, 10, 25, 26, 36",
				"2 : 11, 15, 16, 18, 19, 20, 21, 22, 23, 24, 28, 29, 30, 31, 32, 48, 54, 68",
				"3 : 41, 45, 56",
				"4 : 33, 35, 37, 44, 49, 53, 58, 64, 65",
				"5 : 38, 39, 40, 43, 46, 51, 52, 59, 61, 67, 82, 83, 106",
				"...",
				"Column 0 is A063626: least k such that 9^k has n digits '0' in base 10.",
				"Row lengths are 12, 7, 18, 3, 9, 13, 11, 11, 6, 9, 17, 15, 12, 9, 11, 6, 9, 9, ... (not yet in the OEIS).",
				"Last element of the rows (largest exponent such that 9^k has exactly n digits 0) are (34, 36, 68, 56, 65, 106, 144, 134, 119, 138, 154, ...), not in OEIS.",
				"Inverse permutation is (0, 1, 2, 3, 4, 12, 5, 6, 13, 14, 15, 19, 7, 8, 9, 20, 21, 10, 22, 23, 24, 25, 26, 27, 28, 16, 17, 73, 29, 30, 31, 32, ...), not in OEIS."
			],
			"mathematica": [
				"mx = 1000; g[n_] := g[n] = DigitCount[9^n, 10, 0]; f[n_] := Select[Range@mx, g@# == n \u0026]; Table[f@n, {n, 0, 4}] // Flatten (* _Robert G. Wilson v_, Jun 20 2018 *)"
			],
			"program": [
				"(PARI) apply( A305929_row(n,M=50*(n+1))=select(k-\u003e#select(d-\u003e!d,digits(9^k))==n,[0..M]), [0..10])",
				"print(apply(t-\u003e#t,%)\"\\n\"apply(vecmax,%)\"\\n\"apply(t-\u003et-1,Vec(vecsort(concat(%),,1)[1..99]))) \\\\ to show row lengths, last terms and the inverse permutation"
			],
			"xref": [
				"Cf. A030705, A063626.",
				"Cf. A305932 (analog for 2^k), A305933 (analog for 3^k), A305924 (analog for 4^k), ..., A305928 (analog for 8^k)."
			],
			"keyword": "nonn,base,tabf",
			"offset": "0,3",
			"author": "_M. F. Hasler_, Jun 19 2018",
			"references": 11,
			"revision": 16,
			"time": "2018-11-21T02:34:51-05:00",
			"created": "2018-06-23T07:23:52-04:00"
		}
	]
}