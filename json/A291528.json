{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A291528",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 291528,
			"data": "2,7,17,19,31,43,53,71,67,79,97,103,109,113,127,137,151,163,181,173,191,197,199,211,229,239,241,251,269,257,271,283,293,317,331,337,349,367,373,419,409,431,433,439,443,463,491,487,499,523,557,547,577,593,607,599,601",
			"name": "First term s_n(1) of equivalence classes of prime sequences {s_n(k)} for k \u003e 0 derived by records of first differences of Rowland-like recurrences with increasing even starting values e(n) \u003e= 4.",
			"comment": [
				"These kinds of equivalence classes {s_n(k)} were defined by Shevelev, see Crossrefs.",
				"Some equivalence classes of prime sequences {s_n(k)} have the same tail for a constant C_n \u003c k, such as {s_2(k)} = {a(2),...} = {7,13,29,59,131,...} and {s_5(k)} = {a(5),...} = {31,61,131,...} with common tail {131,...}. Thus it seems that all terms are leaves of a kind of an inverse prime-tree with branches in A291620 and the root at infinity.",
				"In each equivalence class {s_n(k)} the terms hold: s_n(k+1)-2*s_n(k) \u003e= -1;",
				"(s_n(k+1)+1)/s_n(k) \u003e= 2; lim_{k -\u003e inf} (s_n(k+1)+1)/s_n(k) = 2."
			],
			"formula": [
				"a(n) \u003e= 2*n; a(n) \u003e 10*n - 50; a(n) \u003c 12*n.",
				"a(n) \u003e= e(n) - 1, for n \u003e 1; a(n) \u003c e(n) + n."
			],
			"example": [
				"For n=1 the Rowland recurrence with e(1)=4 is A084662 with first differences A134734 and records {2,3,5,11,...} gives the least new prime a(1)=2 as the first term of a first equivalence class {2,3,5,11,...} of prime sequences.",
				"For n=2 with e(2)=8 and records {2,7,13,29,59,...} gives the least new prime a(2)=7 as the first term of a second equivalence class {7,13,29,59,...} of prime sequences.",
				"For n=3 with e(3)=16, a(3)=17 the third equivalence class is {17,41,83,167,...}."
			],
			"mathematica": [
				"For[i = 2; pl = {}; fp = {}, i \u003c 350, i++,",
				"ps = Union@FoldList[Max, 1, Rest@# - Most@#] \u0026@",
				"   FoldList[#1 + GCD[#2, #1] \u0026, 2 i, Range[2, 10^5]];",
				"p = Select[ps, (i \u003c= #) \u0026\u0026 ! MemberQ[pl, #] \u0026, 1];",
				"If[p != {}, fp = Join[fp, {p}];",
				"  pl = Union[pl,",
				"    Drop[ps, -1 + Position[ps, p[[1]]][[1]][[1]]]]]]; Flatten@fp"
			],
			"xref": [
				"Cf. A291620 (branches), A167168 (equivalence classes), A134734 (first differences of A084662), A134162."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Ralf Steiner_, Aug 25 2017",
			"references": 1,
			"revision": 69,
			"time": "2017-10-29T14:11:07-04:00",
			"created": "2017-10-29T14:11:07-04:00"
		}
	]
}