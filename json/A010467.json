{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A010467",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 10467,
			"data": "3,1,6,2,2,7,7,6,6,0,1,6,8,3,7,9,3,3,1,9,9,8,8,9,3,5,4,4,4,3,2,7,1,8,5,3,3,7,1,9,5,5,5,1,3,9,3,2,5,2,1,6,8,2,6,8,5,7,5,0,4,8,5,2,7,9,2,5,9,4,4,3,8,6,3,9,2,3,8,2,2,1,3,4,4,2,4,8,1,0,8,3,7,9,3,0,0,2,9",
			"name": "Decimal expansion of square root of 10.",
			"comment": [
				"Continued fraction expansion is 3 followed by {6} repeated. - _Harry J. Smith_, Jun 02 2009",
				"In 1594, Joseph Scaliger claimed Pi = sqrt(10), but Ludolph van Ceulen immediately knew this to be wrong. - _Alonso del Arte_, Jan 17 2013"
			],
			"link": [
				"Harry J. Smith, \u003ca href=\"/A010467/b010467.txt\"\u003eTable of n, a(n) for n = 1..20000\u003c/a\u003e",
				"Josep M. Brunat and Joan-Carles Lario, \u003ca href=\"https://arxiv.org/abs/2103.05306\"\u003eA problem on concatenated integers\u003c/a\u003e, arXiv:2103.05306 [math.NT], 2021. For 1/sqrt(10).",
				"Jason Kimberley, \u003ca href=\"/wiki/User:Jason_Kimberley/sqrt_base\"\u003eIndex of expansions of sqrt(d) in base b\u003c/a\u003e",
				"R. J. Nemiroff \u0026 J. Bonnell, \u003ca href=\"http://antwrp.gsfc.nasa.gov/htmltest/gifcity/sqrt10.1mil\"\u003eThe first 1 million digits of square root of 10\u003c/a\u003e",
				"R. J. Nemiroff \u0026 J. Bonnell, Plouffe's Inverter, \u003ca href=\"http://www.plouffe.fr/simon/constants/sqrt10.txt\"\u003eThe first 1 million digits of square root of 10\u003c/a\u003e",
				"J. J. O'Connor and E. F. Robertson, \u003ca href=\"http://www-history.mcs.st-and.ac.uk/Biographies/Van_Ceulen.html\"\u003eLudolph Van Ceulen\u003c/a\u003e"
			],
			"formula": [
				"Sqrt(10) = sqrt(1 + i*sqrt(15)) + sqrt(1 - i*sqrt(15)) = sqrt(1/2 + 2*i*sqrt(5)) + sqrt(1/2 - 2*i*sqrt(5)), where i = sqrt(-1). - _Bruno Berselli_, Nov 20 2012",
				"a(n) = -10*floor(10^(-(3/2) + n)) + floor(10^(-(1/2) + n)) for n \u003e 0. - _Mariusz Iwaniuk_, Apr 26 2017",
				"Equals 1/sqrt(10), with offset 0. - _Michel Marcus_, Mar 10 2021"
			],
			"example": [
				"3.162277660168379331998893544432718533719555139325216826857504852792594..."
			],
			"mathematica": [
				"RealDigits[N[Sqrt[10],200]] (* _Vladimir Joseph Stephan Orlovsky_, May 27 2010 *)"
			],
			"program": [
				"(PARI) { default(realprecision, 20080); x=sqrt(10); for (n=1, 20000, d=floor(x); x=(x-d)*10; write(\"b010467.txt\", n, \" \", d)); } \\\\ _Harry J. Smith_, Jun 02 2009",
				"(MAGMA) SetDefaultRealField(RealField(100)); Sqrt(10); // _Vincenzo Librandi_, Feb 15 2020"
			],
			"xref": [
				"Cf. A040006 (continued fraction)."
			],
			"keyword": "nonn,cons",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_",
			"references": 38,
			"revision": 54,
			"time": "2021-03-10T18:19:51-05:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}