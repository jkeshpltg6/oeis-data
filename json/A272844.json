{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A272844",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 272844,
			"data": "1,9,29,70,130,227,359,532,744,1017,1349,1770,2254,2807,3451,4184,4948,5885,6926,8074,9427,10896,12476,14233,16086,18183,20408,22740,25305,28037,30874,33846,36847,40139,43804,47588,51789,56162,60787,65771,70872,76264",
			"name": "Partial sums of the number of active (ON,black) cells in n-th stage of growth of two-dimensional cellular automaton defined by \"Rule 549\", based on the 5-celled von Neumann neighborhood.",
			"comment": [
				"Initialized with a single black (ON) cell at stage zero."
			],
			"reference": [
				"S. Wolfram, A New Kind of Science, Wolfram Media, 2002; p. 170."
			],
			"link": [
				"Robert Price, \u003ca href=\"/A272844/b272844.txt\"\u003eTable of n, a(n) for n = 0..128\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"http://arxiv.org/abs/1503.01168\"\u003eOn the Number of ON Cells in Cellular Automata\u003c/a\u003e, arXiv:1503.01168 [math.CO], 2015",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ElementaryCellularAutomaton.html\"\u003eElementary Cellular Automaton\u003c/a\u003e",
				"S. Wolfram, \u003ca href=\"http://wolframscience.com/\"\u003eA New Kind of Science\u003c/a\u003e",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_2D_5-Neighbor_Cellular_Automata\"\u003eIndex to 2D 5-Neighbor Cellular Automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_Elementary_Cellular_Automata\"\u003eIndex to Elementary Cellular Automata\u003c/a\u003e"
			],
			"mathematica": [
				"CAStep[rule_,a_]:=Map[rule[[10-#]]\u0026,ListConvolve[{{0,2,0},{2,1,2},{0,2,0}},a,2],{2}];",
				"code=549; stages=128;",
				"rule=IntegerDigits[code,2,10];",
				"g=2*stages+1; (* Maximum size of grid *)",
				"a=PadLeft[{{1}},{g,g},0,Floor[{g,g}/2]]; (* Initial ON cell on grid *)",
				"ca=a;",
				"ca=Table[ca=CAStep[rule,ca],{n,1,stages+1}];",
				"PrependTo[ca,a];",
				"(* Trim full grid to reflect growth by one cell at each stage *)",
				"k=(Length[ca[[1]]]+1)/2;",
				"ca=Table[Table[Part[ca[[n]][[j]],Range[k+1-n,k-1+n]],{j,k+1-n,k-1+n}],{n,1,k}];",
				"on=Map[Function[Apply[Plus,Flatten[#1]]],ca] (* Count ON cells at each stage *)",
				"Table[Total[Part[on,Range[1,i]]],{i,1,Length[on]}] (* Sum at each stage *)"
			],
			"xref": [
				"Cf. A272842."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Robert Price_, May 07 2016",
			"references": 1,
			"revision": 7,
			"time": "2016-05-08T10:04:36-04:00",
			"created": "2016-05-08T00:45:03-04:00"
		}
	]
}