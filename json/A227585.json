{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A227585",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 227585,
			"data": "1,2,3,2,3,6,10,12,15,22,30,36,44,60,78,96,117,150,190,228,276,340,420,504,603,732,885,1052,1245,1488,1770,2088,2454,2902,3420,3996,4666,5460,6378,7400,8583,9972,11566,13344,15378,17752,20448,23472,26904,30876",
			"name": "McKay-Thompson series of class 36A for the Monster group with a(0) = 2.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A227585/b227585.txt\"\u003eTable of n, a(n) for n = -1..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of 3 + psi(-q) / (q * psi(-q^9)) + 3 * q * psi(-q^9) / psi(-q) in powers of q where psi() is a Ramanujan  theta function.",
				"Expansion of (1/q) * (psi(-q^3)^2 / (psi(-q) * psi(-q^9)))^2 in powers of q where psi() is a Ramanujan theta function.",
				"Expansion of -3 * b(-q) * c(-q) * (b(q^9) / (b(q^2) * c(q^2) * b(-q^3)))^2 in powers of q where b(), c() are cubic AGM theta functions.",
				"Euler transform of period 36 sequence [ 2, 0, -2, 2, 2, 0, 2, 2, 0, 0, 2, -2, 2, 0, -2, 2, 2, 0, 2, 2, -2, 0, 2, -2, 2, 0, 0, 2, 2, 0, 2, 2, -2, 0, 2, 0, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (36 t)) = f(t) where q = exp(2 Pi i t).",
				"a(n) = -(-1)^n * A215412(n). a(n) = A058644(n) unless n=0.",
				"Convolution square of A112205.",
				"a(n) ~ exp(2*Pi*sqrt(n)/3) / (2*sqrt(3)*n^(3/4)). - _Vaclav Kotesovec_, Nov 12 2015"
			],
			"example": [
				"1/q + 2 + 3*q + 2*q^2 + 3*q^3 + 6*q^4 + 10*q^5 + 12*q^6 + 15*q^7 + 22*q^8 + ..."
			],
			"mathematica": [
				"nmax = 60; CoefficientList[Series[Product[((1+x^k) * (1-x^(3*k))^2 * (1+x^(6*k))^2 * (1+x^(9*k)) / ((1-x^(4*k)) * (1-x^(36*k))))^2, {k, 1, nmax}], {x, 0, nmax}], x] (* _Vaclav Kotesovec_, Nov 12 2015 *)",
				"a[n_]:= SeriesCoefficient[3 - EllipticTheta[2, 0, I*q^(1/2)]/EllipticTheta[2, 0, I*q^(9/2)] - 3*EllipticTheta[2, 0, I*q^(9/2)]/EllipticTheta[2, 0, q^(1/2)], {q,0,n}]; Table[a[n], {n,-1,50}] (* _G. C. Greubel_, Feb 18 2018 *)"
			],
			"program": [
				"(PARI) {a(n) = local(A); if( n\u003c-1, 0, n++; A = x * O(x^n); polcoeff( (eta(x^2 + A) * eta(x^3 + A)^2 * eta(x^12 + A)^2 * eta(x^18 + A) / (eta(x + A) * eta(x^4 + A) * eta(x^6 + A)^2 * eta(x^9 + A) * eta(x^36 + A)))^2, n))}"
			],
			"xref": [
				"Cf. A058644, A112205, A215412."
			],
			"keyword": "nonn",
			"offset": "-1,2",
			"author": "_Michael Somos_, Jul 16 2013",
			"references": 3,
			"revision": 17,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2013-07-17T12:18:45-04:00"
		}
	]
}