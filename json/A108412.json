{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A108412",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 108412,
			"data": "1,1,5,4,19,15,71,56,265,209,989,780,3691,2911,13775,10864,51409,40545,191861,151316,716035,564719,2672279,2107560,9973081,7865521,37220045,29354524,138907099,109552575,518408351,408855776,1934726305",
			"name": "Expansion of (1 + x + x^2)/(1 - 4x^2 + x^4).",
			"comment": [
				"This is the sequence of Lehmer numbers u_n(sqrt(R),Q) with the parameters R = 6 and Q = 1. It is a strong divisibility sequence, that is, gcd(a(n),a(m)) = a(gcd(n,m)) for all natural numbers n and m. The sequence satisfies a linear recurrence of order four. - _Peter Bala_, Apr 18 2014",
				"The sequence of convergents of the 2-periodic continued fraction [0; 1, -6, 1, -6, ...] = 1/(1 - 1/(6 - 1/(1 - 1/(6 - ...)))) = 3 - sqrt(3) begins [0/1, 1/1, 6/5, 5/4, 24/19, 19/15, 90/71,...]. The present sequence is the sequence of denominators; the sequence of numerators of the continued fraction convergents [1, 6, 5, 24, 19, 90,...] is also a strong divisibility sequence. Cf. A005013 and A203976. - _Peter Bala_, May 19 2014",
				"From _Peter Bala_, Mar 25 2018: (Start)",
				"The following remarks assume an offset of 1.",
				"Define a binary operation o on the real numbers by x o y = x*sqrt(1 + (1/2)*y^2) + y*sqrt(1 + (1/2)*x^2). The operation o is commutative and associative with identity 0. We have a(2*n + 1) = 1 o 1 o ... o 1 (2*n + 1 terms) and sqrt(6)*a(2*n) = (1 o 1 o ... o 1) (2*n terms). Cf. A005013 and A084068. For example, 1 o 1 = sqrt(6) and 1 o 1 o 1 = sqrt(6) o 1 = 5 = a(3).",
				"From the obvious identity ( 1 o 1 o ... o 1 (2*n terms) ) o ( 1 o 1 o ... o 1 (2*m terms) ) = 1 o 1 o ... o 1 (2*n + 2*m terms) we find the relation a(2*n+2*m) = a(2*n)*sqrt(1 + 3*a(2*m)^2) + a(2*m)*sqrt(1 + 3*a(2*n)^2).",
				"Similarly, from a(2*n+1) o a(2*m+1) = sqrt(6)*a(2*n+2*m+2) we find sqrt(6)*a(2*n+2*m+2) = a(2*n+1)*sqrt(1 + (1/2)*a(2*m+1)^2) + a(2*m+1)*sqrt(1 + (1/2)*a(2*n+1)^2). (End)"
			],
			"link": [
				"Harvey P. Dale, \u003ca href=\"/A108412/b108412.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"P. Bala, \u003ca href=\"/A243469/a243469_1.pdf\"\u003eNotes on 2-periodic continued fractions and Lehmer sequences\u003c/a\u003e",
				"Seong Ju Kim, R. Stees, L. Taalman, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL19/Stees/stees4.html\"\u003eSequences of Spiral Knot Determinants\u003c/a\u003e, Journal of Integer Sequences, Vol. 19 (2016), #16.1.4.",
				"Ryan Stees, \u003ca href=\"https://commons.lib.jmu.edu/honors201019/84\"\u003eSequences of Spiral Knot Determinants\u003c/a\u003e, Senior Honors Projects, Paper 84, James Madison Univ., May 2016.",
				"E. W. Weisstein, \u003ca href=\"http://mathworld.wolfram.com/LehmerNumber.html\"\u003eMathWorld: Lehmer Number\u003c/a\u003e",
				"\u003ca href=\"/index/Di#divseq\"\u003eIndex to divisibility sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,4,0,-1)."
			],
			"formula": [
				"a(0)=a(1)=1, a(2)=5, a(n)a(n+3) - a(n+1)a(n+2) = -1.",
				"a(0)=1, a(1)=1, a(2)=5, a(3)=4, a(n) = 4*a(n-2)-a(n-4). - _Harvey P. Dale_, Nov 15 2012",
				"a(n) = (alpha^n - beta^n)/(alpha - beta) for n odd, and a(n) = (alpha^n - beta^n)/(alpha^2 - beta^2) for n even, where alpha = (1/2)*(sqrt(6) + sqrt(2)) (A188887) and beta = (1/2)*(sqrt(6) - sqrt(2)) (A101263). Equivalently, a(n) = U(n-1,sqrt(6)/2) for n odd and a(n) = (1/sqrt(6))*U(n-1,sqrt(6)/2) for n even, where U(n,x) is the Chebyshev polynomial of the second kind. - _Peter Bala_, Apr 18 2014",
				"a(2*n) = A001834(n). a(2*n + 1) = A001353(n+1). - _Michael Somos_, Feb 10 2015",
				"a(n) = -a(-2-n) for all n in Z. - _Michael Somos_, Feb 10 2015"
			],
			"example": [
				"G.f. = 1 + x + 5*x^2 + 4*x^3 + 19*x^4 + 15*x^5 + 71*x^6 + 56*x^7 + ..."
			],
			"maple": [
				"a := proc (n) if `mod`(n, 2) = 1 then 1/sqrt(2)*( ((sqrt(6) + sqrt(2))/2 )^n - ( (sqrt(6) - sqrt(2))/2 )^n) else 1/sqrt(12)*( ((sqrt(6) + sqrt(2))/2 )^n - ( (sqrt(6) - sqrt(2))/2 )^n) end if;",
				"end proc:",
				"seq(simplify(a(n)), n = 1..30); # _Peter Bala_, Mar 25 2018"
			],
			"mathematica": [
				"CoefficientList[Series[(1+x+x^2)/(1-4x^2+x^4),{x,0,40}],x] (* or *) LinearRecurrence[{0,4,0,-1},{1,1,5,4},40] (* _Harvey P. Dale_, Nov 15 2012 *)"
			],
			"program": [
				"(PARI) {a(n) = my( w = quadgen(24)); simplify( polchebyshev( n, 2, w/2) / if( n%2, w, 1))}; /* _Michael Somos_, Feb 10 2015 */"
			],
			"xref": [
				"Cf. A001353, A001834.",
				"Cf. A026741, A005013, A084068."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Ralf Stephan_, Jun 05 2005",
			"references": 5,
			"revision": 51,
			"time": "2020-11-22T12:18:56-05:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}