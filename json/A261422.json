{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A261422",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 261422,
			"data": "1,3,6,10,15,21,28,36,45,55,63,72,79,84,87,88,87,84,79,72,66,55,51,45,40,36,33,31,30,30,30,33,27,34,33,33,33,33,33,33,33,33,36,27,39,36,36,36,36,36,36,36,36,39,27,45,39,39,39,39,39,39,39,39,42,27,52,42,42,42,42,42,42,42,42,45",
			"name": "Number of ordered triples (u,v,w) of palindromes such that u+v+w=n.",
			"comment": [
				"It is known that a(n)\u003e0 for all n."
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/A261422/b261422.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e (See also the extended file with 200000 terms below)",
				"William D. Banks, \u003ca href=\"https://arxiv.org/abs/1508.04721\"\u003eEvery natural number is the sum of forty-nine palindromes\u003c/a\u003e, arXiv:1508.04721 [math.NT], 2015. [Establishes the much weaker result that the coefficients of P(x)^49 are positive (see Formula section below).]",
				"Javier Cilleruelo and Florian Luca, \u003ca href=\"https://arxiv.org/abs/1602.06208\"\u003eEvery positive integer is a sum of three palindromes\u003c/a\u003e, arXiv: 1602.06208 [math.NT], 2016-2017.",
				"Erich Friedman, \u003ca href=\"https://erich-friedman.github.io/mathmagic/0699.html\"\u003eProblem of the Month (June 1999)\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/A261422/a261422.txt\"\u003eTable of n, a(n) for n=0..200000\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/A261422/a261422_1.txt\"\u003eMaple program to produce 200000 terms\u003c/a\u003e"
			],
			"formula": [
				"G.f. = P(x)^3, where P(x) = 1 + x + x^2 + x^3 + x^4 + x^5 + x^6 + x^7 + x^8 + x^9 + x^11 + x^22 + x^33 + x^44 + x^55 + x^66 + x^77 + x^88 + x^99 + x^101 + x^111 + ... = Sum_{palindromes p} x^p."
			],
			"example": [
				"4 can be written as a sum of three palindromes in 15 ways: 4+0+0 (3 ways), 3+1+0 (6 ways), 2+2+0 (3 ways), and 2+1+1 (3 ways), so a(4)=15."
			],
			"mathematica": [
				"(* This program is not suitable to compute a large number of terms. *)",
				"compositions[n_, k_] := Flatten[Permutations[PadLeft[#, k]]\u0026 /@ IntegerPartitions[n, k], 1];",
				"a[n_] := Select[compositions[n, 3], AllTrue[#, PalindromeQ]\u0026] // Length;",
				"Table[a[n], {n, 0, 100}] (* _Jean-François Alcover_, Aug 05 2018 *)"
			],
			"xref": [
				"Cf. A002113. Differs from A261132, which assumes 0 \u003c= u \u003c= v \u003c= w.",
				"For records see A262544, A262545."
			],
			"keyword": "nonn,look,hear,base",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Aug 27 2015",
			"references": 19,
			"revision": 46,
			"time": "2020-08-14T13:28:31-04:00",
			"created": "2015-08-27T23:56:45-04:00"
		}
	]
}