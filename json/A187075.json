{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A187075",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 187075,
			"data": "1,2,3,4,18,15,8,84,180,105,16,360,1500,2100,945,32,1488,10800,27300,28350,10395,64,6048,72240,294000,529200,436590,135135,128,24384,463680,2857680,7938000,11060280,7567560,2027025,256,97920,2904000,26107200,105099120,220041360,249729480,145945800,34459425",
			"name": "A Galton triangle: T(n,k) = 2*k*T(n-1,k) + (2*k-1)*T(n-1,k-1).",
			"comment": [
				"This is a companion triangle to A186695.",
				"Let f(x)=(exp(2*x)+1)^(-1/2) then the n-th derivative of f equals sum(k=1..n,(-1)^k*T(n,k)*(f(x))^(2*k+1)). [Groux Roland, May 17 2011]",
				"Triangle T(n,k), 1\u003c=k\u003c=n, given by (0, 2, 0, 4, 0, 6, 0, 8, 0, 10, 0, ...) DELTA (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, ...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, Oct 20 2013"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A187075/b187075.txt\"\u003eTable of n, a(n) for the first 50 rows\u003c/a\u003e",
				"Shi-Mei Ma, \u003ca href=\"http://arxiv.org/abs/1204.4963\"\u003eA family of two-variable derivative polynomials for tangent and secant\u003c/a\u003e, arXiv:1204.4963v3 [math.CO]",
				"Shi-Mei Ma, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v20i1p11\"\u003eA family of two-variable derivative polynomials for tangent and secant\u003c/a\u003e, El J. Combinat. 20 (1) (2013) P11.",
				"E. Neuwirth, \u003ca href=\"http://homepage.univie.ac.at/erich.neuwirth/papers/TechRep99-05.pdf\"\u003eRecursively defined combinatorial functions: Extending Galton's board,\u003c/a\u003e Discrete Math. 239 (2001) 33-51."
			],
			"formula": [
				"T(n,k) = 2^(n-2*k)*binomial(2k,k)*k!*Stirling2(n,k).",
				"Recurrence relation T(n,k) = 2*k*T(n-1,k) + (2*k-1)*T(n-1,k-1) with boundary conditions T(1,1) = 1, T(1,k) = 0 for k\u003e=2.",
				"G.f.: F(x,t) = 1/sqrt((1+x)-x*exp(2*t)) - 1 = sum {n = 1..inf} R(n,x)*t^n/n! = x*t + (2*x+3*x^2)*t^2/2! + (4*x+18*x^2+15*x^3)*t^3/3! + ....",
				"The g.f. F(x,t) satisfies the partial differential equation dF/dt = 2*(x+x^2)*dF/dx + x*F.",
				"The row polynomials R(n,x) satisfy the recursion R(n+1,x) = 2*(x+x^2)*R'(n,x) + x*R(n,x) where ' indicates differentiation with respect to x.",
				"O.g.f. for column k: (2k-1)!!*x^k/product {m = 1..k} (1-2*m*x) (Compare with A075497). T(n,k) = (2*k-1)!!*A075497(n,k).",
				"The row polynomials R(n,x) = sum {k = 1..n} T(n,k)*x^k satisfy R(n,-x-1) = (-1)^n*(1+x)/x*P(n,x) where P(n,x) is the n-th row polynomial of A186695. We also haveR(n,x/(1-x)) = x/(1-x)^n*Q(n-1,x) where Q(n,x) is the n-th row polynomial of A156919.",
				"T(n,k) = 2^(n-k)*A211608(n,k). - _Philippe Deléham_, Oct 20 2013"
			],
			"example": [
				"Triangle begins",
				"n\\k.|...1.....2......3......4......5......6",
				"===========================================",
				"..1.|...1",
				"..2.|...2.....3",
				"..3.|...4....18.....15",
				"..4.|...8....84....180....105",
				"..5.|..16...360...1500...2100....945",
				"..6.|..32..1488..10800..27300..28350..10395",
				"..",
				"Examples of recurrence relation",
				"T(4,3) = 6*T(3,3)+5*T(3,2) = 6*15+5*18 = 180;",
				"T(6,4) = 8*T(5,4)+7*T(5,3) = 8*2100+7*1500 = 27300."
			],
			"maple": [
				"A187075 := proc(n, k) option remember; if k \u003c 1 or k \u003e n then 0; elif k = 1 then 2^(n-1); else 2*k*procname(n-1, k) + (2*k-1)*procname(n-1, k-1) ; end if; end proc:seq(seq(A187075(n,k),k = 1..n),n = 1..10);"
			],
			"mathematica": [
				"Flatten[Table[2^(n - 2*k)*Binomial[2 k, k]*k!*StirlingS2[n, k], {n, 10}, {k, 1, n}]] (* _G. C. Greubel_, Jun 17 2016 *)"
			],
			"program": [
				"(Sage) # uses[delehamdelta from A084938]",
				"# Adds a first column (1,0,0,0, ...).",
				"def A187075_triangle(n):",
				"    return delehamdelta([(i+1)*int(is_even(i+1)) for i in (0..n)], [i+1 for i in (0..n)])",
				"A187075_triangle(4)  # _Peter Luschny_, Oct 20 2013"
			],
			"xref": [
				"Cf. A075497, A156919, A186695, A211402."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "1,2",
			"author": "_Peter Bala_, Mar 27 2011",
			"references": 5,
			"revision": 36,
			"time": "2020-03-19T11:04:20-04:00",
			"created": "2011-04-03T02:35:37-04:00"
		}
	]
}