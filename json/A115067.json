{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A115067",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 115067,
			"data": "0,4,11,21,34,50,69,91,116,144,175,209,246,286,329,375,424,476,531,589,650,714,781,851,924,1000,1079,1161,1246,1334,1425,1519,1616,1716,1819,1925,2034,2146,2261,2379,2500,2624,2751,2881,3014,3150,3289,3431,3576",
			"name": "a(n) = (3*n^2 - n - 2)/2.",
			"comment": [
				"Number of orbits of Aut(Z^7) as function of the infinity norm n of the representative integer lattice point of the orbit, when the cardinality of the orbit is equal to 6720. - _Philippe A.J.G. Chevalier_, Dec 28 2015",
				"a(n) = the sum of the numerator and denominator of the reduced fraction resulting from the sum A000217(n-2)/A000217(n-1) + A000217(n-1)/A000217(n), n\u003e1. - _J. M. Bergot_, Jun 10 2017",
				"For n \u003e 1, a(n) is also the number of (not necessarily maximum) cliques in the (n-1)-Andrasfai graph. - _Eric W. Weisstein_, Nov 29 2017",
				"a(n+1) is the sum of the lengths of all the segments used to draw a square of side n representing the most classic pattern for walls made of 2 X 1 bricks, known as a 1-over-2 pattern, where each joint between neighboring bricks falls over the center of the brick below. - _Stefano Spezia_, Jun 05 2021"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A115067/b115067.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Alfred Hoehn, \u003ca href=\"/A000326/a000326.pdf\"\u003eIllustration of initial terms of A000326, A005449, A045943, A115067\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/AndrasfaiGraph.html\"\u003eAndrasfai Graph\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Clique.html\"\u003eClique\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-3,1)."
			],
			"formula": [
				"a(n) = (3*n+2)*(n-1)/2.",
				"a(n+1) = n*(3*n + 5)/2. - _Omar E. Pol_, May 21 2008",
				"a(n) = 3*n + a(n-1) - 2 for n\u003e1, a(1)=0. - _Vincenzo Librandi_, Nov 13 2010",
				"a(n) = A095794(-n). - _Bruno Berselli_, Sep 02 2011",
				"G.f.: x^2*(4-x) / (1-x)^3. - _R. J. Mathar_, Sep 02 2011",
				"a(n) = A055998(2*n-2) - A055998(n-1). - _Bruno Berselli_, Sep 23 2016",
				"E.g.f.: exp(x)*x*(8 + 3*x)/2. - _Stefano Spezia_, May 19 2021"
			],
			"example": [
				"Illustrations for n = 2..7 from _Stefano Spezia_, Jun 05 2021:",
				"       _           _ _          _ _ _",
				"      |_|         |_|_|        |_|_ _|",
				"                  |_ _|        |_ _|_|",
				"                               |_|_ _|",
				"   a(2) = 4     a(3) = 11     a(4) = 21",
				"    _ _ _ _     _ _ _ _ _    _ _ _ _ _ _",
				"   |_ _|_ _|   |_ _|_ _|_|  |_ _|_ _|_ _|",
				"   |_|_ _|_|   |_|_ _|_ _|  |_|_ _|_ _|_|",
				"   |_ _|_ _|   |_ _|_ _|_|  |_ _|_ _|_ _|",
				"   |_|_ _|_|   |_|_ _|_ _|  |_|_ _|_ _|_|",
				"               |_ _|_ _|_|  |_ _|_ _|_ _|",
				"                            |_|_ _|_ _|_|",
				"   a(5) = 34    a(6) = 50     a(7) = 69"
			],
			"mathematica": [
				"Table[n (3 n - 1)/2 - 1, {n, 50}] (* _Vincenzo Librandi_, Jun 11 2017 *)",
				"LinearRecurrence[{3, -3, 1}, {0, 4, 11}, 20] (* _Eric W. Weisstein_, Nov 29 2017 *)",
				"CoefficientList[Series[(-4 + x) x/(-1 + x)^3, {x, 0, 20}], x] (* _Eric W. Weisstein_, Nov 29 2017 *)"
			],
			"program": [
				"(PARI) a(n)=n*(3*n-1)/2-1 \\\\ _Charles R Greathouse IV_, Jan 27 2012",
				"(MAGMA) [n*(3*n-1)/2-1: n in [1..50]]; // _Vincenzo Librandi_, Jun 11 2017"
			],
			"xref": [
				"The generalized pentagonal numbers b*n+3*n*(n-1)/2, for b = 1 through 12, form sequences A000326, A005449, A045943, A115067, A140090, A140091, A059845, A140672, A140673, A140674, A140675, A151542.",
				"Orbits of Aut(Z^7) as function of the infinity norm A000579, A154286, A102860, A002412, A045943, A008585, A005843, A001477, A000217.",
				"Cf. A055998, A095794."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Roger L. Bagula_, Mar 01 2006",
			"ext": [
				"Edited by _N. J. A. Sloane_, Mar 05 2006"
			],
			"references": 34,
			"revision": 80,
			"time": "2021-06-05T16:46:55-04:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}