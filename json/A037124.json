{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A037124",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 37124,
			"data": "1,2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90,100,200,300,400,500,600,700,800,900,1000,2000,3000,4000,5000,6000,7000,8000,9000,10000,20000,30000,40000,50000,60000,70000,80000,90000,100000",
			"name": "Numbers that contain only one nonzero digit.",
			"comment": [
				"Starting with 1: next greater number not containing the highest digit (see also A098395). - _Reinhard Zumkeller_, Oct 31 2004",
				"a(n) = A140740(n+9, 9). - _Reinhard Zumkeller_, May 26 2008",
				"A055640(a(n)) = 1. - _Reinhard Zumkeller_, May 03 2011",
				"A193459(a(n)) = A000005(a(n)), subsequence of A193460.",
				"Sum_{n\u003e0} 1/a(n)^s = (10^s)*(Zeta(s) - Zeta(s,10))/(10^s-1), with (s\u003e1). - _Enrique Pérez Herrero_, Feb 05 2013"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A037124/b037124.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Michael Maltenfort, \u003ca href=\"http://www.jstor.org/stable/10.4169/amer.math.monthly.124.2.132\"\u003eCharacterizing Additive Systems\u003c/a\u003e, The American Mathematical Monthly 124.2 (2017): 132-148.",
				"\u003ca href=\"/index/Ar#10-automatic\"\u003eIndex entries for 10-automatic sequences\u003c/a\u003e."
			],
			"formula": [
				"a(n) = ((n mod 9) + 1) * 10^floor(n/9). E.g., a(39) = ((39 mod 9) + 1) * 10^floor(39/9) = (3 + 1) * 10^4 = 40000. - _Carl R. White_, Jan 08 2004",
				"a(n) = A051885(n-1) + 1. - _Reinhard Zumkeller_, Jan 03 2008, Jul 10 2011",
				"A138707(a(n)) = A000005(a(n)); A061116 is a subsequence. - _Reinhard Zumkeller_, Mar 26 2008",
				"a(n+1) = a(n) + a(n - n mod 9). - _Reinhard Zumkeller_, May 26 2008",
				"a(n) = (10^floor((n - 1)/9))*(n - 9*floor((n - 1)/9)). - _José de Jesús Camacho Medina_, Nov 10 2014",
				"From _Chai Wah Wu_, May 28 2016: (Start)",
				"a(n) = 10*a(n-9).",
				"G.f.: x*(9*x^8 + 8*x^7 + 7*x^6 + 6*x^5 + 5*x^4 + 4*x^3 + 3*x^2 + 2*x + 1)/(1 - 10*x^9). (End)",
				"a(n) ≍ 1.2589...^n, where the constant is A011279. (f ≍ g when f \u003c\u003c g and g \u003c\u003c f, that is, there are absolute constants c,C \u003e 0 such that for all large n, |f(n)| \u003c= c|g(n)| and |g(n)| \u003c= C|f(n)|.) - _Charles R Greathouse IV_, Mar 11 2021"
			],
			"mathematica": [
				"Table[(10^Floor[(n - 1)/9])*(n - 9*Floor[(n - 1)/9]), {n, 1, 50}](* _José de Jesús Camacho Medina_, Nov 10 2014 *)"
			],
			"program": [
				"(Haskell)",
				"a037124 n = a037124_list !! (n-1)",
				"a037124_list = f [1..9] where f (x:xs) = x : f (xs ++ [10*x])",
				"-- _Reinhard Zumkeller_, May 03 2011",
				"(MAGMA) [((n mod 9)+1) * 10^Floor(n/9): n in [0..50]]; // _Vincenzo Librandi_, Nov 11 2014",
				"(PARI) is(n)=n\u003e0 \u0026\u0026 n/10^valuation(n,10)\u003c10 \\\\ _Charles R Greathouse IV_, Jan 29 2017"
			],
			"xref": [
				"Cf. A000079, A038754, A133464, A140730."
			],
			"keyword": "nonn,base,easy",
			"offset": "1,2",
			"author": "Vasiliy Danilov (danilovv(AT)usa.net), Jun 15 1998",
			"references": 18,
			"revision": 62,
			"time": "2021-03-11T15:14:13-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}