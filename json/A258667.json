{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A258667",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 258667,
			"data": "0,0,0,0,0,20,116,791,6205,55004,543596,5922929,70518903,910711188,12678337924,189252400363,3015217931281,51067619058668,916176426367084,17355904144230373,346195850528456683,7252654441430368404,159210363452786908116,3654550890657000160319",
			"name": "A total of n married couples, including a mathematician M and his wife, are to be seated at the 2n chairs around a circular table, with no man seated next to his wife. After the ladies are seated at every other chair, M is the first man allowed to choose one of the remaining chairs. The sequence gives the number of ways of seating the other men, with no man seated next to his wife, if M chooses the chair that is 9 seats clockwise from his wife's chair.",
			"comment": [
				"This is a variation of the classic ménage problem (cf. A000179).",
				"It is known [Riordan, ch. 8, ex. 7(b)] that, after the ladies are seated at every other chair, the number U_n of ways of seating the men in the ménage problem has asymptotic expansion U_n ~ e^(-2)*n!*(1 + sum{k\u003e=1}(-1)^k/(k!(n-1)_k)), where (n)_k = n*(n-1)*...*(n-k+1).",
				"Therefore, it is natural to conjecture that a(n) ~ e^(-2)*n!/(n-2)*(1 + sum{k\u003e=1}(-1)^k/(k!(n-1)_k))."
			],
			"reference": [
				"I. Kaplansky and J. Riordan, The problème des ménages, Scripta Math. 12, (1946), 113-124.",
				"J. Riordan, An Introduction to Combinatorial Analysis, Wiley, 1958, chs. 7, 8."
			],
			"link": [
				"I. Kaplansky and J. Riordan, \u003ca href=\"/A000166/a000166_1.pdf\"\u003eThe problème des ménages\u003c/a\u003e, Scripta Math. 12, (1946), 113-124. [Scan of annotated copy]",
				"Peter J. C. Moses, \u003ca href=\"/A258667/a258667_1.pdf\"\u003eSeatings for 6 couples\u003c/a\u003e",
				"E. Lucas, \u003ca href=\"https://archive.org/details/thoriedesnombre00lucagoog/page/n495\"\u003eSur le problème des ménages\u003c/a\u003e, Théorie des nombres, Paris, 1891, 491-496.",
				"Vladimir Shevelev, Peter J. C. Moses, \u003ca href=\"http://arxiv.org/abs/1101.5321\"\u003eThe ménage problem with a known mathematician\u003c/a\u003e, arXiv:1101.5321 [math.CO], 2011-2015.",
				"Vladimir Shevelev and Peter J. C. Moses, \u003ca href=\"http://www.emis.de/journals/INTEGERS/papers/q72/q72.Abstract.html\"\u003eAlice and Bob go to dinner: A variation on menage\u003c/a\u003e, INTEGERS, Vol. 16(2016), #A72.",
				"J. Touchard, \u003ca href=\"http://gallica.bnf.fr/ark:/12148/bpt6k31506/f631.image\"\u003eSur un problème de permutations\u003c/a\u003e, C.R. Acad. Sci. Paris, 198 (1934), 631-633."
			],
			"formula": [
				"For n\u003c=5, a(n)=0; otherwise a(n) = Sum_{0\u003c=k\u003c=n-1}(-1)^k*(n-k-1)! Sum_{max(k-n+5, 0)\u003c=j\u003c=min(k,4)}binomial(8-j, j)*binomial(2*n-k+j-10, k-j)."
			],
			"mathematica": [
				"a[n_] := If[n\u003c6, 0, Sum[(-1)^k (n-k-1)! Sum[Binomial[8-j, j] Binomial[2n-k+j-10, k-j], {j, Max[k-n+5, 0], Min[k, 4]}], {k, 0, n-1}]];",
				"Array[a, 24] (* _Jean-François Alcover_, Sep 19 2018 *)"
			],
			"program": [
				"(PARI) a(n) = if (n\u003c=5, 0, sum(k=0, n-1, (-1)^k*(n-k-1)!*sum(j=max(k-n+5, 0), min(k,4), binomial(8-j, j)*binomial(2*n-k+j-10, k-j)))); \\\\ _Michel Marcus_, Jun 26 2015"
			],
			"xref": [
				"Cf. A000179, A258664, A258665, A258666, A258673, A259212."
			],
			"keyword": "nonn",
			"offset": "1,6",
			"author": "_Vladimir Shevelev_ and _Peter J. C. Moses_, Jun 07 2015",
			"references": 9,
			"revision": 63,
			"time": "2020-07-20T16:21:50-04:00",
			"created": "2015-07-01T02:23:05-04:00"
		}
	]
}