{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A344834",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 344834,
			"data": "0,0,0,0,1,0,0,2,2,0,0,2,2,2,0,0,4,2,2,4,0,0,4,4,3,4,4,0,0,4,4,4,4,4,4,0,0,4,4,4,4,4,4,4,0,0,8,4,6,4,4,6,4,8,0,0,8,8,6,4,5,4,6,8,8,0,0,8,8,8,4,4,4,4,8,8,8,0,0,8,8,8,8,5,6,5,8,8,8,8,0",
			"name": "Square array T(n, k), n, k \u003e= 0, read by antidiagonals; T(n, k) = (n * 2^max(0, w(k)-w(n))) AND (k * 2^max(0, w(n)-w(k))) (where AND denotes the bitwise AND operator and w = A070939).",
			"comment": [
				"In other words, we right pad the binary expansion of the lesser of n and k with zeros (provided it is positive) so that both numbers have the same number of binary digits, and then apply the bitwise AND operator."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A344834/b344834.txt\"\u003eTable of n, a(n) for n = 0..10010\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A344834/a344834.png\"\u003eColored representation of the table for n, k \u003c 2^10\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = T(k, n).",
				"T(m, T(n, k)) = T(T(m, n), k).",
				"T(n, n) = n.",
				"T(n, 0) = n.",
				"T(n, 1) = A053644(n)."
			],
			"example": [
				"Array T(n, k) begins:",
				"  n\\k|  0  1  2   3  4   5   6   7  8  9  10  11  12  13  14  15",
				"  ---+----------------------------------------------------------",
				"    0|  0  0  0   0  0   0   0   0  0  0   0   0   0   0   0   0",
				"    1|  0  1  2   2  4   4   4   4  8  8   8   8   8   8   8   8",
				"    2|  0  2  2   2  4   4   4   4  8  8   8   8   8   8   8   8",
				"    3|  0  2  2   3  4   4   6   6  8  8   8   8  12  12  12  12",
				"    4|  0  4  4   4  4   4   4   4  8  8   8   8   8   8   8   8",
				"    5|  0  4  4   4  4   5   4   5  8  8  10  10   8   8  10  10",
				"    6|  0  4  4   6  4   4   6   6  8  8   8   8  12  12  12  12",
				"    7|  0  4  4   6  4   5   6   7  8  8  10  10  12  12  14  14",
				"    8|  0  8  8   8  8   8   8   8  8  8   8   8   8   8   8   8",
				"    9|  0  8  8   8  8   8   8   8  8  9   8   9   8   9   8   9",
				"   10|  0  8  8   8  8  10   8  10  8  8  10  10   8   8  10  10",
				"   11|  0  8  8   8  8  10   8  10  8  9  10  11   8   9  10  11",
				"   12|  0  8  8  12  8   8  12  12  8  8   8   8  12  12  12  12",
				"   13|  0  8  8  12  8   8  12  12  8  9   8   9  12  13  12  13",
				"   14|  0  8  8  12  8  10  12  14  8  8  10  10  12  12  14  14",
				"   15|  0  8  8  12  8  10  12  14  8  9  10  11  12  13  14  15"
			],
			"program": [
				"(PARI) T(n,k,op=bitand,w=m-\u003e#binary(m)) = { op(n*2^max(0, w(k)-w(n)), k*2^max(0, w(n)-w(k))) }"
			],
			"xref": [
				"Cf. A004198, A053644, A070939.",
				"Cf. A344835 (OR), A344836 (XOR), A344837 (min), A344838 (max), A344839 (absolute difference)."
			],
			"keyword": "nonn,base,tabl",
			"offset": "0,8",
			"author": "_Rémy Sigrist_, May 29 2021",
			"references": 6,
			"revision": 13,
			"time": "2021-05-31T02:11:28-04:00",
			"created": "2021-05-31T00:41:16-04:00"
		}
	]
}