{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A204022",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 204022,
			"data": "1,3,3,5,3,5,7,5,5,7,9,7,5,7,9,11,9,7,7,9,11,13,11,9,7,9,11,13,15,13,11,9,9,11,13,15,17,15,13,11,9,11,13,15,17,19,17,15,13,11,11,13,15,17,19,21,19,17,15,13,11,13,15,17,19,21,23,21,19,17,15,13,13,15,17,19,21,23",
			"name": "Symmetric matrix based on f(i,j) = max(2i-1, 2j-1), by antidiagonals.",
			"comment": [
				"This sequence represents the matrix M given by f(i,j) = max(2i-1, 2j-1) for i \u003e= 1 and j \u003e= 1. See A204023 for characteristic polynomials of principal submatrices of M, with interlacing zeros. See A204016 for a guide to other choices of M."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A204022/b204022.txt\"\u003eRows n = 1..100 of triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"From _Ridouane Oudra_, May 27 2019: (Start)",
				"a(n) = t + |t^2-2n+1|, where t = floor(sqrt(2n-1)+1/2).",
				"a(n) = A209302(2n-1).",
				"a(n) = A002024(n) + |A002024(n)^2-2n+1|.",
				"a(n) = t + |t^2-2n+1|, where t = floor(sqrt(2n)+1/2). (End)"
			],
			"example": [
				"Northwest corner:",
				"  1 3 5 7 9",
				"  3 3 5 7 9",
				"  5 5 5 7 9",
				"  7 7 7 7 9",
				"  9 9 9 9 9"
			],
			"mathematica": [
				"(* First program *)",
				"f[i_, j_] := Max[2 i - 1, 2 j - 1];",
				"m[n_] := Table[f[i, j], {i, n}, {j, n}]",
				"TableForm[m[6]] (* 6 X 6 principal submatrix *)",
				"Flatten[Table[f[i, n + 1 - i],",
				"  {n, 15}, {i, n}]]                (* A204022 *)",
				"p[n_] := CharacteristicPolynomial[m[n], x];",
				"c[n_] := CoefficientList[p[n], x]",
				"TableForm[Flatten[Table[p[n], {n, 10}]]]",
				"Table[c[n], {n, 12}]",
				"Flatten[%]                         (* A204023 *)",
				"TableForm[Table[c[n], {n, 10}]]",
				"(* Second program *)",
				"Table[Max[2*k-1, 2*(n-k)+1], {n, 12}, {k, n}]//Flatten (* _G. C. Greubel_, Jul 23 2019 *)"
			],
			"program": [
				"(PARI) {T(n, k) = max(2*k-1, 2*(n-k)+1)};",
				"for(n=1, 12, for(k=1, n, print1(T(n, k), \", \"))) \\\\ _G. C. Greubel_, Jul 23 2019",
				"(MAGMA) [[Max(2*k-1, 2*(n-k)+1): k in [1..n]]: n in [1..12]]; // _G. C. Greubel_, Jul 23 2019",
				"(Sage) [[max(2*k-1, 2*(n-k)+1) for k in (1..n)] for n in (1..12)] # _G. C. Greubel_, Jul 23 2019",
				"(GAP) Flat(List([1..12], n-\u003e List([1..n], k-\u003e Maximum(2*k-1, 2*(n-k)+1) ))); # _G. C. Greubel_, Jul 23 2019"
			],
			"xref": [
				"Cf. A202453, A204016, A204022, A209302."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Jan 11 2012",
			"references": 4,
			"revision": 26,
			"time": "2019-07-23T15:55:07-04:00",
			"created": "2012-01-11T17:32:20-05:00"
		}
	]
}