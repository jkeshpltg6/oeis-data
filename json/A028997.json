{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A028997",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 28997,
			"data": "1,0,8,8,16,8,24,0,40,16,40,16,72,24,8,32,80,16,88,24,104,8,80,32,152,48,88,48,16,48,160,48,168,64,128,8,224,48,136,64,232,48,24,48,208,104,160,80,328,0,200,112,248,64,272,96,40,112,192,88,416,72,208,16,336,112,320",
			"name": "Theta series of quadratic form with Gram matrix [ 4, 1, 0, 1; 1, 4, 1, 0; 0, 1, 4, -1; 1, 0, -1, 4 ].",
			"comment": [
				"Associated with permutations in Mathieu group M24 of shape (14)(7)(2)(1). - _Michael Somos_, Nov 22 2007"
			],
			"link": [
				"John Cannon, \u003ca href=\"/A028997/b028997.txt\"\u003eTable of n, a(n) for n = 0..5000\u003c/a\u003e",
				"M. Koike, \u003ca href=\"http://projecteuclid.org/euclid.nmj/1118787871\"\u003eMathieu group M24 and modular forms\u003c/a\u003e, Nagoya Math. J., 99 (1985), 147-157. MR0805086 (87e:11060)",
				"G. Nebe and N. J. A. Sloane, \u003ca href=\"http://www.math.rwth-aachen.de/~Gabriele.Nebe/LATTICES/QQF.4.c.html\"\u003eHome page for this lattice\u003c/a\u003e"
			],
			"formula": [
				"G.f. is Fourier series of a weight 2 level 14 modular form. f(-1 / (14 t)) = 14 (t/i)^2 f(t) where q = exp(2 Pi i t). - _Michael Somos_, Nov 22 2007",
				"Convolution of A030187 and A134782. -  _Michael Somos_, Nov 22 2007"
			],
			"example": [
				"G.f. = 1 + 8*x^2 + 8*x^3 + 16*x^4 + 8*x^5 + 24*x^6 + 40*x^8 + 16*x^9 + 40*x^10 + ...",
				"G.f. = 1 + 8*q^4 + 8*q^6 + 16*q^8 + 8*q^10 + 24*q^12 + 40*q^16 + 16*q^18 + 40*q^20 + ..."
			],
			"mathematica": [
				"a[ n_] := With[{e1 = QPochhammer[ x] QPochhammer[ x^7], e2 = QPochhammer[ x^2] QPochhammer[ x^14]}, SeriesCoefficient[ e1^4 / e2^2 + 4 x e1 e2 + 8 x^2 e2^4 / e1^2, {x, 0, n}]]; (* _Michael Somos_, Apr 19 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A, B); if( n\u003c0, 0, A = x * O(x^n); B = eta(x^2 + A) * eta(x^14 + A); A = eta(x + A) * eta(x^7 + A); polcoeff( A^4 / B^2 + 4 * x * A * B + 8 * x^2 * B^4 / A^2, n))}; /* _Michael Somos_, Nov 22 2007 */",
				"(PARI) {a(n) = my(G); if( n\u003c0, 0, G = [ 4, 1, 0, 1; 1, 4, 1, 0; 0, 1, 4, -1; 1, 0, -1, 4 ]; polcoeff( 1 + 2 * x * Ser(qfrep( G, n, 1)), n))}; /* _Michael Somos_, Nov 22 2007 */",
				"(MAGMA) A := Basis( ModularForms( Gamma0(14), 2), 67); A[1] + 8*A[3] + 8*A[4]; /* _Michael Somos_, Apr 19 2015 */"
			],
			"xref": [
				"Cf. A030187, A134782."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_.",
			"references": 3,
			"revision": 23,
			"time": "2015-04-19T15:24:08-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}