{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A274880",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 274880,
			"data": "1,1,2,5,1,4,2,18,11,1,10,8,2,65,57,17,1,28,28,12,2,238,252,116,23,1,84,96,54,16,2,882,1050,615,195,29,1,264,330,220,88,20,2,3300,4257,2915,1210,294,35,1,858,1144,858,416,130,24,2,12441,17017,13013,6461,2093,413,41,1",
			"name": "A statistic on orbital systems over n sectors: the number of orbitals with k restarts.",
			"comment": [
				"The definition of an orbital system is given in A232500 (see also the illustration there). The number of orbitals over n sectors is counted by the swinging factorial A056040.",
				"A 'restart' of an orbital is a raise which starts from the central circle.",
				"A118920 is a subtriangle."
			],
			"link": [
				"Peter Luschny, \u003ca href=\"https://oeis.org/wiki/User:Peter_Luschny/Orbitals\"\u003eOrbitals\u003c/a\u003e"
			],
			"formula": [
				"For even n\u003e0: T(n,k) = 4*(k+1)*binomial(n,n/2-k-1)/n for k=0..n/2-1 (from A118920)."
			],
			"example": [
				"Triangle read by rows, n\u003e=0. The length of row n is floor((n+1)/2) for n\u003e=1.",
				"[n] [k=0,1,2,...]                 [row sum]",
				"[ 0] [1]                              1",
				"[ 1] [1]                              1",
				"[ 2] [2]                              2",
				"[ 3] [5, 1]                           6",
				"[ 4] [4, 2]                           6",
				"[ 5] [18, 11, 1]                     30",
				"[ 6] [10, 8, 2]                      20",
				"[ 7] [65, 57, 17, 1]                140",
				"[ 8] [28, 28, 12, 2]                 70",
				"[ 9] [238, 252, 116, 23, 1]         630",
				"[10] [84, 96, 54, 16, 2]            252",
				"[11] [882, 1050, 615, 195, 29, 1]  2772",
				"T(6, 2) = 2 because there are two orbitals over 6 segments which have 2 ascents:",
				"[-1, 1, 1, -1, 1, -1] and [1, -1, 1, -1, 1, -1]."
			],
			"program": [
				"(Sage) # uses[unit_orbitals from A274709]",
				"from itertools import accumulate",
				"# Brute force counting",
				"def orbital_restart(n):",
				"    if n == 0: return [1]",
				"    S = [0]*((n+1)//2)",
				"    for u in unit_orbitals(n):",
				"        A = list(accumulate(u))",
				"        L = [1 if A[i] == 0 and A[i+1] == 1  else 0 for i in (0..n-2)]",
				"        S[sum(L)] += 1",
				"    return S",
				"for n in (0..12): print(orbital_restart(n))"
			],
			"xref": [
				"Cf. A056040 (row sum), A118920, A232500.",
				"Other orbital statistics: A241477 (first zero crossing), A274706 (absolute integral), A274708 (peaks), A274709 (max. height), A274710 (number of turns), A274878 (span), A274879 (returns), A274881 (ascent)."
			],
			"keyword": "nonn,tabf",
			"offset": "0,3",
			"author": "_Peter Luschny_, Jul 11 2016",
			"references": 10,
			"revision": 22,
			"time": "2020-03-23T17:33:32-04:00",
			"created": "2016-07-18T05:31:54-04:00"
		}
	]
}