{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A319373",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 319373,
			"data": "1,2,-1,-10,-5,20,13,-36,-27,54,43,-78,-65,104,89,-136,-119,170,151,-210,-189,252,229,-300,-275,350,323,-406,-377,464,433,-528,-495,594,559,-666,-629,740,701,-820,-779,902,859,-990,-945,1080,1033,-1176,-1127",
			"name": "a(n) = 1*2 - 3*4 + 5*6 - 7*8 + 9*10 - 11*12 + 13*14 - ... + (up to n).",
			"comment": [
				"In general, for alternating sequences that multiply the first k natural numbers, and subtract/add the products of the next k natural numbers (preserving the order of operations up to n), we have a(n) = (-1)^floor(n/k) * Sum_{i=1..k-1} (1-sign((n-i) mod k)) * (Product_{j=1..i} (n-j+1)) + Sum_{i=1..n} (-1)^(floor(i/k)+1) * (1-sign(i mod k)) * (Product_{j=1..k} (i-j+1)). Here k=2.",
				"An alternating version of A228958."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A319373/b319373.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_07\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,-3,3,-3,3,-1,1)."
			],
			"formula": [
				"a(n) = (cos(n*Pi/2)*(1-n-n^2) + sin(n*Pi/2)*(1+3*n-n^2) - 1)/2.",
				"From _Colin Barker_, Sep 18 2018: (Start)",
				"G.f.: x*(1 + x - 6*x^3 - x^4 + x^5) / ((1 - x)*(1 + x^2)^3).",
				"a(n) = a(n-1) - 3*a(n-2) + 3*a(n-3) - 3*a(n-4) + 3*a(n-5) - a(n-6) + a(n-7) for n\u003e7. (End)",
				"a(n) = (-1 + (-1)^((n-1)*n/2))/2 + (-2 + (-1)^n)*(-1)^(n*(n+1)/2)*n/2 - (-1)^((n-1)*n/2)*n^2/2. - _Bruno Berselli_, Sep 25 2018"
			],
			"example": [
				"a(1) = 1;",
				"a(2) = 1*2 = 2;",
				"a(3) = 1*2 - 3 = -1;",
				"a(4) = 1*2 - 3*4 = -10;",
				"a(5) = 1*2 - 3*4 + 5 = -5;",
				"a(6) = 1*2 - 3*4 + 5*6 = 20;",
				"a(7) = 1*2 - 3*4 + 5*6 - 7 = 13;",
				"a(8) = 1*2 - 3*4 + 5*6 - 7*8 = -36;",
				"a(9) = 1*2 - 3*4 + 5*6 - 7*8 + 9 = -27;",
				"a(10) = 1*2 - 3*4 + 5*6 - 7*8 + 9*10 = 54;",
				"a(11) = 1*2 - 3*4 + 5*6 - 7*8 + 9*10 - 11 = 43;",
				"a(12) = 1*2 - 3*4 + 5*6 - 7*8 + 9*10 - 11*12 = -78;",
				"a(13) = 1*2 - 3*4 + 5*6 - 7*8 + 9*10 - 11*12 + 13 = -65;",
				"a(14) = 1*2 - 3*4 + 5*6 - 7*8 + 9*10 - 11*12 + 13*14 = 104;",
				"a(15) = 1*2 - 3*4 + 5*6 - 7*8 + 9*10 - 11*12 + 13*14 - 15 = 89; etc."
			],
			"mathematica": [
				"Table[(Cos[n Pi/2] (1 - n - n^2) + Sin[n Pi/2] (1 + 3 n - n^2) - 1)/2, {n, 50}]",
				"a[n_] := (-1)^Floor[n/2] Sum[(1 - Sign[Mod[n - i, 2]]) Product[n - j + 1, {j, 1, i}], {i, 1, 1}] + Sum[(-1)^(Floor[i/2] + 1) (1 - Sign[Mod[i, 2]]) Product[i - j + 1, {j, 1, 2}], {i, 1, n}]; Array[a, 30] (* _Stefano Spezia_, Sep 23 2018 *)"
			],
			"program": [
				"(PARI) Vec(x*(1 + x - 6*x^3 - x^4 + x^5) / ((1 - x)*(1 + x^2)^3) + O(x^50)) \\\\ _Colin Barker_, Sep 18 2018"
			],
			"xref": [
				"Cf. A093361, A228958, A305189.",
				"For similar sequences, see: A001057 (k=1), this sequence (k=2), A319543 (k=3), A319544 (k=4), A319545 (k=5), A319546 (k=6), A319547 (k=7), A319549 (k=8), A319550 (k=9), A319551 (k=10)."
			],
			"keyword": "sign,easy",
			"offset": "1,2",
			"author": "_Wesley Ivan Hurt_, Sep 17 2018",
			"references": 9,
			"revision": 23,
			"time": "2018-10-05T08:48:19-04:00",
			"created": "2018-10-05T07:59:23-04:00"
		}
	]
}