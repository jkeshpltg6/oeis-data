{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006358",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6358,
			"id": "M3862",
			"data": "1,5,15,55,190,671,2353,8272,29056,102091,358671,1260143,4427294,15554592,54648506,191998646,674555937,2369942427,8326406594,29253473175,102777312308,361091343583,1268635610806,4457144547354",
			"name": "Number of distributive lattices; also number of paths with n turns when light is reflected from 5 glass plates.",
			"comment": [
				"Let M denotes the 5 X 5 matrix = row by row (1,1,1,1,1)(1,1,1,1,0)(1,1,1,0,0)(1,1,0,0,0)(1,0,0,0,0) and A(n) the vector (x(n),y(n),z(n),t(n),u(n)) = M^n*A where A is the vector (1,1,1,1,1); then a(n)=y(n). - _Benoit Cloitre_, Apr 02 2002"
			],
			"reference": [
				"J. Berman and P. Koehler, Cardinalities of finite distributive lattices, Mitteilungen aus dem Mathematischen Seminar Giessen, 121 (1976), 103-124.",
				"S. J. Cyvin and I. Gutman, Kekulé structures in benzenoid hydrocarbons, Lecture Notes in Chemistry, No. 46, Springer, New York, 1988 (see p. 120).",
				"J. Haubrich, Multinacci Rijen [Multinacci sequences], Euclides (Netherlands), Vol. 74, Issue 4, 1998, pp. 131-133.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A006358/b006358.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"J. Berman and P. Koehler, \u003ca href=\"/A006356/a006356.pdf\"\u003eCardinalities of finite distributive lattices\u003c/a\u003e, Mitteilungen aus dem Mathematischen Seminar Giessen, 121 (1976), 103-124. [Annotated scanned copy]",
				"Emma L. L. Gao, Sergey Kitaev, Philip B. Zhang, \u003ca href=\"https://personal.cis.strath.ac.uk/sergey.kitaev/index_files/Papers/pat-av-alt-words.pdf\"\u003ePattern-avoiding alternating words\u003c/a\u003e, preprint, 2015.",
				"Manfred Goebel, \u003ca href=\"http://dx.doi.org/10.1007/s002000050118\"\u003eRewriting Techniques and Degree Bounds for Higher Order Symmetric Polynomials\u003c/a\u003e, Applicable Algebra in Engineering, Communication and Computing (AAECC), Volume 9, Issue 6 (1999), 559-573.",
				"G. Kreweras, \u003ca href=\"http://www.numdam.org/item?id=MSH_1976__53__5_0\"\u003eLes préordres totaux compatibles avec un ordre partiel\u003c/a\u003e, Math. Sci. Humaines No. 53 (1976), 5-30.",
				"G. Kreweras, \u003ca href=\"/A019538/a019538.pdf\"\u003eLes préordres totaux compatibles avec un ordre partiel\u003c/a\u003e, Math. Sci. Humaines No. 53 (1976), 5-30. (Annotated scanned copy)",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,3,-4,-1,1)."
			],
			"formula": [
				"a(n) = 3*a(n-1) + 3*a(n-2) - 4*a(n-3) - a(n-4) + a(n-5).",
				"a(n) is asymptotic to z(5)*w(5)^n where w(5) = (1/2)/cos(5*Pi/11) and z(5) is the root 1 \u003c x \u003c 2 of P(5, X) = -1 + 55*X + 847*X^2 - 5324*X^3 - 14641*X^4 + 14641*X^5. - _Benoit Cloitre_, Oct 16 2002",
				"G.f.: A(x) = (1 + 2*x - 3*x^2 - x^3 + x^4)/(1 - 3*x - 3*x^2 + 4*x^3 + x^4 - x^5). - _Paul D. Hanna_, Feb 06 2006"
			],
			"maple": [
				"A=seq(a.j,j=0..4):grammar1:=[Q4,{ seq(Q.i=Union(Epsilon,seq(Prod(a.j,Q.j),j=4-i..4)),i=0..4), seq(a.j=Z,j=0..4) }, unlabeled]: seq(count(grammar1,size=j),j=0..23); # _Zerinvary Lajos_, Mar 09 2007",
				"A006358:=-(z-1)*(z**3-3*z-1)/(-1+3*z+3*z**2-4*z**3-z**4+z**5); # conjectured by _Simon Plouffe_ in his 1992 dissertation"
			],
			"mathematica": [
				"m = Table[ If[j \u003c= 6-i, 1, 0], {i, 1, 5}, {j, 1, 5}] ; a[n_] := MatrixPower[m, n].Table[1, {5}]; Table[ a[n], {n, 0, 23}][[All, 1]] (* _Jean-François Alcover_, Dec 08 2011, after _Benoit Cloitre_ *)",
				"LinearRecurrence[{3,3,-4,-1,1},{1,5,15,55,190},30] (* _Harvey P. Dale_, Jun 16 2016 *)"
			],
			"program": [
				"(PARI) k=5; M(k)=matrix(k,k,i,j,if(1-sign(i+j-k),0,1)); v(k)=vector(k,i,1); a(n)=vecmax(v(k)*M(k)^n)",
				"(PARI) {a(n)=local(p=5);polcoeff(sum(k=0,p-1,(-1)^((k+1)\\2)*binomial((p+k-1)\\2,k)* (-x)^k)/sum(k=0,p,(-1)^((k+1)\\2)*binomial((p+k)\\2,k)*x^k+x*O(x^n)),n)}"
			],
			"xref": [
				"Cf. A000217, A000330, A050446, A050447.",
				"See also A006356-A006359, A025030, A030112-A030116.",
				"Cf. A038201 (5-wave sequence)."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Alternative description and formula from Jacques Haubrich (jhaubrich(AT)freeler.nl)",
				"More terms from _James A. Sellers_, Dec 24 1999"
			],
			"references": 11,
			"revision": 68,
			"time": "2021-03-12T22:32:37-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}