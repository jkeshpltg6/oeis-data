{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A145901",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 145901,
			"data": "1,1,2,1,8,8,1,26,72,48,1,80,464,768,384,1,242,2640,8160,9600,3840,1,728,14168,72960,151680,138240,46080,1,2186,73752,595728,1948800,3037440,2257920,645120,1,6560,377504,4612608,22305024,52899840",
			"name": "Triangle of f-vectors of the simplicial complexes dual to the permutohedra of type B_n.",
			"comment": [
				"The Coxeter group of type B_n may be realized as the group of n X n matrices with exactly one nonzero entry in each row and column, that entry being either +1 or -1. The order of the group is 2^n*n!. The orbit of the point (1,2,...,n) (or any sufficiently generic point (x_1,...,x_n)) under the action of this group is a set of 2^n*n! distinct points whose convex hull is defined to be the permutohedron of type B_n. The rows of this table are the f-vectors of the simplicial complexes dual to these type B permutohedra. Some examples are given in the Example section below. See A060187 for the corresponding table of h-vectors of type B permutohedra.",
				"This is the (unsigned) triangle of connection constants between the polynomial sequences (2*x + 1)^n, n \u003e= 0, and binomial(x+k,k), k \u003e= 0. For example, (2*x + 1)^2 = 8*binomial(x+2,2) - 8*binomial(x+1,1) + 1 and (2*x + 1)^3 = 48*binomial(x+3,3) - 72*binomial(x+2,2) + 26*binomial(x+1,1) - 1. Cf. A163626. - _Peter Bala_, Jun 06 2019"
			],
			"link": [
				"Sandrine Dasse-Hartaut and Pawel Hitczenko, \u003ca href=\"http://arxiv.org/abs/1202.3092\"\u003eGreek letters in random staircase tableaux\u003c/a\u003e arXiv:1202.3092v1 [math.CO], 2012.",
				"P. Bala, \u003ca href=\"/A131689/a131689.pdf\"\u003eDeformations of the Hadamard product of power series\u003c/a\u003e",
				"M. Dukes, C. D. White, \u003ca href=\"http://arxiv.org/abs/1603.01589\"\u003eWeb Matrices: Structural Properties and Generating Combinatorial Identities\u003c/a\u003e, arXiv:1603.01589 [math.CO], 2016.",
				"S. Fomin, N. Reading, \u003ca href=\"https://arxiv.org/abs/math/0505518\"\u003eRoot systems and generalized associahedra\u003c/a\u003e, Lecture notes for IAS/Park-City 2004; arXiv:math/0505518 [math.CO], 2005-2008.",
				"Ghislain R. Franssens, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL9/Franssens/franssens13.html\"\u003eOn a Number Pyramid Related to the Binomial, Deleham, Eulerian, MacMahon and Stirling number triangles\u003c/a\u003e, Journal of Integer Sequences, Vol. 9 (2006), Article 06.4.1.",
				"Gábor Hetyei, \u003ca href=\"https://math.uncc.edu/sites/math.uncc.edu/files/fields/preprint_archive/paper/2019_16.pdf\"\u003eThe type B permutohedron and the poset of intervals as a Tchebyshev transform\u003c/a\u003e, University of North Carolina-Charlotte (2019).",
				"Shi-Mei Ma, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v20i1p11\"\u003eA family of two-variable derivative polynomials for tangent and secant\u003c/a\u003e, El J. Combinat. 20 (1) (2013) P11.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Truncated_cuboctahedron\"\u003e Truncated cuboctahedron\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = Sum_{i = 0..k} (-1)^(k-i)*binomial(k,i)*(2*i+1)^n.",
				"Recurrence relation: T(n,k) = (2*k + 1)*T(n-1,k) + 2*k*T(n-1,k-1) with T(0,0) = 1 and T(0,k) = 0 for k \u003e= 1.",
				"Relation with type B Stirling numbers of the second kind: T(n,k) = 2^k*k!*A039755(n,k).",
				"Row sums A080253. The matrix product A060187 * A007318 produces the mirror image of this triangle.",
				"E.g.f.: exp(t)/(1 + x - x* exp(2*t)) = 1 + (1 + 2*x)*t + (1 + 8*x + 8*x^2 )*t^2/2! + ... .",
				"From _Peter Bala_, Oct 13 2011: (Start)",
				"The polynomials in the first column of the array ((1+t)*P^(-1)-t*P)^(-1), P Pascal's triangle and I the identity, are the row polynomials of this table.",
				"The polynomials in the first column of the array ((1+t)*I-t*A062715)^(-1) are, apart from the initial 1, the row polynomials of this table with an extra factor of t. Cf. A060187. (End)",
				"From _Peter Bala_, Jul 18 2013: (Start)",
				"Integrating the above e.g.f. with respect to x from x = 0 to x = 1 gives Sum_{k = 0..n} (-1)^k*T(n,k)/(k + 1) = 2^n*Bernoulli(n,1/2), the n-th cosecant number.",
				"The corresponding Type A result is considered in A028246 as Worpitzky's algorithm.",
				"Also for n \u003e= 0, Sum_{k = 0..2*n} (-1)^k*T(2*n,k)/((k + 1)*(k + 2)) = 1/2*2^(2*n)*Bernoulli(2*n,1/2) and for n \u003e= 1, Sum_{k = 0..2*n-1} (-1)^k*T(2*n - 1,k)/((k + 1)*(k + 2)) = -1/2 * 2^(2*n)* Bernoulli(2*n,1/2).",
				"The nonzero cosecant numbers are given by A001896/A001897. (End)",
				"From _Peter Bala_, Jul 22 2014: (Start)",
				"The row polynomials R(n,x) satisfy the recurrence equation R(n+1,x) = D(R(n,x)) with R(0,x) = 1, where D is the operator 1 + 2*x + 2*x(1 + x)*d/dx.",
				"R(n,x) = 1/(1 + x)* Sum_{k = 0..inf} (2*k + 1)^n*(x/(1 + x))^k, valid for x in the open interval (-1/2, inf). Cf. A019538.",
				"The shifted row polynomial x*R(n,x) = (1 + x)^n*P(n,x/(1 + x)) where P(n,x) denotes the n-th row polynomial of A060187.",
				"The row polynomials R(n,x) have only real zeros.",
				"Symmetry: R(n,x) = (-1)^n*R(n,-1 - x). Consequently the zeros of R(n,x) lie in the open interval (-1, 0). (End)",
				"From _Peter Bala_, May 28 2015: (Start)",
				"Recurrence for row polynomials: R(n,x) = 1 + x*Sum_{k = 0..n-1} binomial(n,k)2^(n-k)*R(k,x) with R(0,x) = 1.",
				"For a fixed integer k, the expansion of the function A(k,z) := exp( Sum_{n \u003e= 1} R(n,k)*z^n/n ) has integer coefficients and satisfies the functional equation A(k,z)^(k + 1) = 1/(1 - z)*( BINOMIAL(BINOMIAL(A(k,z))) )^k, where BINOMIAL(F(z))= 1/(1 - z)*F(z/(1 - z)) denotes the binomial transform of the o.g.f. F(z). A(k,z) = A(-(k + 1),-z). Cf. A019538.",
				"For cases see A258377 (k = 1), A258378(k = 2), A258379 (k = 3), A258380 (k = 4) and A258381 (k = 5). (End)",
				"T(n,k) = A154537(n,k)*k! = A039755(n,k)*(2^k*k!), 0 \u003c= k \u003c= n. - _Wolfdieter Lang_, Apr 19 2017",
				"From _Peter Bala_, Jan 12 2018: (Start)",
				"n-th row polynomial R(n,x) =  (1 + 2*x) o (1 + 2*x) o ... o (1 + 2*x) (n factors), where o denotes the black diamond multiplication operator of Dukes and White. See example E13 in the Bala link.",
				"R(n,x) = Sum_{k = 0..n} binomial(n,k)*2^k*F(k,x) where F(k,x) is the Fubini polynomial of order k, the k-th row polynomial of A019538. (End)"
			],
			"example": [
				"The triangle begins",
				"n\\k|..0.....1.....2.....3.....4.....5",
				"=====================================",
				"0..|..1",
				"1..|..1.....2",
				"2..|..1.....8.....8",
				"3..|..1....26....72....48",
				"4..|..1....80...464...768...384",
				"5..|..1...242..2640..8160..9600..3840",
				"...",
				"Row 2: the permutohedron of type B_2 is an octagon with 8 vertices and 8 edges. Its dual, also an octagon, has f-vector (1,8,8) - row 3 of this triangle.",
				"Row 3: for an appropriate choice of generic point in R_3, the permutohedron of type B_3 is realized as the great rhombicuboctahedron, also known as the truncated cuboctahedron, with 48 vertices, 72 edges and 26 faces (12 squares, 8 regular hexagons and 6 regular octagons). See the Wikipedia entry and also [Fomin and Reading p.22]. Its dual polyhedron is a simplicial polyhedron, the disdyakis dodecahedron, with 26 vertices, 72 edges and 48 triangular faces and so its f-vector is (1,26,72,48) - row 4 of this triangle.",
				"From _Peter Bala_, Jun 06 2019: (Start)",
				"Examples of falling factorials identities for odd numbered rows: Let (x)_n = x*(x - 1)*...*(x - n + 1) with (x)_0 = 1 denote the falling factorial power.",
				"Row 1: 2*(x)_1 + (0 - 2*x)_1 = 0.",
				"Row 3: 48*(x)_3 + 72*(x)_2 * (2 - 2*x)_1 + 26*(x)_1 * (2 - 2*x)_2 + (2 - 2*x)_3 = 0",
				"Row 5: 3840*(x)_5 + 9600*(x)_4 * (4 - 2*x)_(1) + 8160*(x)_3 * (4 - 2*x)_2 + 2640*(x)_2 * (4 - 2*x)_3 + 242*(x)_1 * (4 - 2*x)_4 + (4 - 2*x)_5 = 0. (End)"
			],
			"maple": [
				"with(combinat):",
				"T:= (n,k) -\u003e add((-1)^(k-i)*binomial(k,i)*(2*i+1)^n,i = 0..k):",
				"for n from 0 to 9 do",
				"seq(T(n,k),k = 0..n);",
				"end do;"
			],
			"mathematica": [
				"T[n_, k_] := Sum[(-1)^(k - i)*Binomial[k, i]*(2*i + 1)^n, {i, 0, k}];",
				"Table[T[n, k], {n, 0, 9}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Jun 02 2019 *)"
			],
			"xref": [
				"Cf. A019538 (f-vectors type A permutohedra), A060187 (h-vectors type B permutohedra), A080253 (row sums), A145905, A062715, A028246.",
				"Cf. A258377, A258378, A258379, A258380, A258381.",
				"Cf. A039755, A154537."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "0,3",
			"author": "_Peter Bala_, Oct 26 2008",
			"references": 20,
			"revision": 69,
			"time": "2020-02-11T19:20:39-05:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}