{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A079612",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 79612,
			"data": "2,24,2,240,2,504,2,480,2,264,2,65520,2,24,2,16320,2,28728,2,13200,2,552,2,131040,2,24,2,6960,2,171864,2,32640,2,24,2,138181680,2,24,2,1082400,2,151704,2,5520,2,1128,2,4455360,2,264,2,12720,2,86184,2,13920",
			"name": "Largest number m such that a^n == 1 (mod m) whenever a is coprime to m.",
			"comment": [
				"a(m) divides the Jordan function J_m(n) for all n except when n is a prime dividing a(m) or m=2, n=4; it is the largest number dividing all but finitely many values of J_m(n). For m \u003e 0, a(m) also divides Sum_{k=1}^n J_m(k) for n \u003e= the largest exceptional value. - _Franklin T. Adams-Watters_, Dec 10 2005",
				"The numbers m with this property are the divisors of a(n) that are not divisors of a(r) for r\u003cn."
			],
			"reference": [
				"R. C. Vaughan and T. D. Wooley, Waring's problem: a survey, pp. 285-324 of Surveys in Number Theory (Urbana, May 21, 2000), ed. M. A. Bennett et al., Peters, 2003. (The function K(n), see p. 303.)"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A079612/b079612.txt\"\u003eTable of n, a(n) for n = 1..16384\u003c/a\u003e",
				"P. J. Cameron and D. A. Preece, \u003ca href=\"http://www.maths.qmul.ac.uk/~pjc/csgnotes/lambda.pdf\"\u003eNotes on primitive lambda-roots\u003c/a\u003e, 2009. See lambda*() in theorem 5.2 (b) p. 8.",
				"Joris van der Hoeven and Grégoire Lecerf, \u003ca href=\"https://hal.archives-ouvertes.fr/hal-02382117\"\u003eSparse polynomial interpolation. Exploring fast heuristic algorithms over finite fields\u003c/a\u003e, Simon Fraser University (BC Canada) / Institut Polytechnique de Paris (France, 2019) hal-02382117.",
				"R. C. Vaughan and T. D. Wooley, \u003ca href=\"https://www.people.maths.bris.ac.uk/~matdw/2002%20wps.pdf\"\u003eWaring's problem: a survey\u003c/a\u003e, The function K(n), see p. 19."
			],
			"formula": [
				"a(n) = 2 for n odd; for n even, a(n) = product of 2^(t+2) (where 2^t exactly divides n) and p^(t+1) (where p runs through all odd primes such that p-1 divides n and p^t exactly divides n).",
				"From _Antti Karttunen_, Dec 19 2018: (Start)",
				"a(n) = A185633(n)*(2-A000035(n)).",
				"It also seems that for n \u003e 1, a(n) = 2*A075180(n-1). (End)",
				"We have 2*A075180(2n-1) = A006863(n) by definition, and A006863(n) = a(2n) by the comments in A006863. Hence a(n) = 2*A075180(n-1) for all even n. For all odd n \u003e 1, we have a(n) = 2, which is also equal to 2*A075180(n-1). So the formula above is true. - _Jianing Song_, Apr 05 2021"
			],
			"program": [
				"(PARI) a(n) = {if (n%2, 2, res = 1; forprime(p=2, n+1, if (!(n % (p-1)), t = valuation(n, p); if (p==2, if (t, res *= p^(t+2)), res *= p^(t+1)););); res;);} \\\\ _Michel Marcus_, May 12 2018"
			],
			"xref": [
				"Cf. A006863 (bisection except for initial term); A059379 (Jordan function).",
				"Cf. A075180, A115000, A115001, A115002, A115003.",
				"Cf. A143407, A143408, A185633, A322315."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_, Jan 29 2003",
			"ext": [
				"Edited by _Franklin T. Adams-Watters_, Dec 10 2005",
				"Definition corrected by _T. D. Noe_, Aug 13 2008",
				"Rather arbitrary term a(0) removed by _Max Alekseyev_, May 27 2010"
			],
			"references": 10,
			"revision": 44,
			"time": "2021-04-05T09:11:57-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}