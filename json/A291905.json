{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A291905",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 291905,
			"data": "1,1,0,1,1,0,2,1,1,3,2,3,4,4,6,8,8,11,14,16,21,26,32,39,49,60,75,93,114,142,176,217,268,334,411,510,632,779,967,1196,1477,1832,2266,2801,3470,4291,5310,6572,8129,10061,12449,15401,19058,23581,29178,36102,44668",
			"name": "Row sums of A291904.",
			"comment": [
				"Number of compositions of n where the first part is 1 and the absolute difference between consecutive parts is 1."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A291905/b291905.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e"
			],
			"example": [
				"The a(6)=2 compositions of 6 are:",
				":",
				":  o o|",
				": oooo|",
				":",
				":   o|",
				":  oo|",
				": ooo|",
				":",
				"The a(9)=3 compositions of 9 are:",
				":",
				":   o  |",
				":  ooo |",
				": ooooo|",
				":",
				":  o o o|",
				": oooooo|",
				":",
				":     o|",
				":  o oo|",
				": ooooo|"
			],
			"maple": [
				"b:= proc(n, i) option remember; `if`(n=0, 1, add(",
				"     `if`(j=i, 0, b(n-j, j)), j=max(1, i-1)..min(i+1, n)))",
				"    end:",
				"a:= n-\u003e b(n, 0):",
				"seq(a(n), n=0..60);  # _Alois P. Heinz_, Sep 05 2017"
			],
			"mathematica": [
				"T[0, 0] = 1; T[_, 0] = 0; T[n_?Positive, k_] /; 0 \u003c k \u003c= Floor[(Sqrt[8n+1] - 1)/2] := T[n, k] = T[n-k, k-1] + T[n-k, k+1]; T[_, _] = 0;",
				"a[n_] := Sum[T[n, k], {k, 0, Floor[(Sqrt[8n+1] - 1)/2]}];",
				"Table[a[n], {n, 0, 60}] (* _Jean-François Alcover_, May 29 2019 *)"
			],
			"program": [
				"(Python)",
				"from sympy.core.cache import cacheit",
				"@cacheit",
				"def b(n, i): return 1 if n==0 else sum(b(n - j, j) for j in range(max(1, i - 1), min(i + 1, n) + 1) if j != i)",
				"def a(n): return b(n, 0)",
				"print([a(n) for n in range(61)]) # _Indranil Ghosh_, Sep 06 2017, after Maple program"
			],
			"xref": [
				"Cf. A291896, A291904."
			],
			"keyword": "nonn",
			"offset": "0,7",
			"author": "_Seiichi Manyama_, Sep 05 2017",
			"references": 5,
			"revision": 26,
			"time": "2021-05-15T06:17:55-04:00",
			"created": "2017-09-06T03:42:29-04:00"
		}
	]
}