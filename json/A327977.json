{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A327977",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 327977,
			"data": "7,10,21,25,18,38,46,65,77,217,361,129,205,493,529,98,426,718,170,254,462,982,1501,2077,2257,2105,2933,6953,11513,14393,16469,17813,19769,21653,24053,25769,27413,29993,34553,35369,41273,42233,42869,44969,45113,45173,11917,27757,38881,45937,62317,76897,84781,102637,111457,114481,117217,118477,120781,127117,128881,501,1141",
			"name": "Breadth-first reading of the subtree rooted at 7 of the tree where each parent node is the arithmetic derivative (A003415) of all its children.",
			"comment": [
				"Permutation of A328117.",
				"The branching degree of vertex v is given by A099302(v).",
				"Leaves form a subsequence of A098700.",
				"For any number k at level n (where 7 is at level 2), we have A256750(k) = A327966(k) = n.",
				"Question: Does this subtree contain infinitely long paths? How many? Cf. conjecture number 8 in Ufnarovski and Ahlander paper. As an example of possible beginning of such a sequence they give: 1 ← 7 ← 10 ← 25 ← 46 ← 129 ← 170 ← 501 ← 414 ← 2045."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A327977/b327977.txt\"\u003eTable of n, a(n) for n = 1..204\u003c/a\u003e",
				"Victor Ufnarovski and Bo Ahlander, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL6/Ufnarovski/ufnarovski.html\"\u003eHow to Differentiate a Number\u003c/a\u003e, J. Integer Seqs., Vol. 6, 2003."
			],
			"example": [
				"The subtree is laid out as below. The terms of this sequence are obtained by scanning each successive level of the tree from left to right, from the node 7 onward:",
				"   (0)",
				"    |",
				"   (1)",
				"    |",
				"    7",
				"    |",
				"    10______________________________",
				"    |                               |",
				"    21________                     25",
				"    |         |                     |",
				"    18___    38_____               46_________________________________",
				"    |    |    |     |               |            |      |             |",
				"    65   77  217   361____         129____      205    493_____      529",
				"         |          |     |         |     |             |      |",
				"         98        426   718       170   254           462    982",
				"         |          |     |         |     |             |      |",
				"        [3]       [21]   [15]      [9]   [9]           [28]   [17]",
				"On the last level illustrated above, the numbers in brackets [ ] tell how many children the node has. E.g, there are three for 98: 1501, 2077, 2257, as A003415(1501) = A003415(2077) = A003415(2257) = 98, and nine for 170: 501, 1141, 2041, 2869, 4309, 5461, 6649, 6901, 7081."
			],
			"program": [
				"(PARI)",
				"A002620(n) = ((n^2)\u003e\u003e2);",
				"A003415(n) = {my(fac); if(n\u003c1, 0, fac=factor(n); sum(i=1, matsize(fac)[1], n*fac[i, 2]/fac[i, 1]))}; \\\\ From A003415",
				"A327977list(e) = { my(lista=List([7]), f); for(i=1, e, f = lista[i]; for(k=1,1+A002620(f),if(A003415(k)==f, listput(lista,k)))); Vec(lista); };",
				"(PARI)",
				"\\\\ With precomputed large A328117, use this:",
				"v328117 = readvec(\"a328117.txt\");",
				"A327977list(e) = { my(lista=List([7]), f, i); for(n=1, e, f = lista[n]; print(\"n=\",n,\" #lista=\", #lista, \" A002620(\",f,\")=\",A002620(f)); my(u=1+A002620(f)); if(u\u003e=v328117[#v328117],print(\"Not enough precomputed terms of A328117 as search upper limit \", u, \" \u003e \", v328117[#v328117], \" (the last item in v328117). Number of expansions so far=\", n); return(1/0)); i=1; while(v328117[i]\u003cu, if(A003415(v328117[i])==f, listput(lista,v328117[i])); i++)); Vec(lista); };",
				"v327977 = A327977list(114);",
				"A327977(n) = v327977[n];",
				"for(n=1,#v327977,write(\"b327977.txt\", n, \" \", A327977(n)));",
				"(Sage) # uses[A003415]",
				"def A327977():",
				"  '''Breadth-first reading of irregular subtree rooted at 7, defined by the edge-relation A003415(child) = parent. Starts giving terms from 7 onward, after a(0) = 0 and a(1) = 1.'''",
				"  yield 7",
				"  for x in A327977():",
				"    for k in [1 .. 1+floor((x*x)/2)]:",
				"      if(A003415(k) == x): yield k",
				"def take(n, g):",
				"  '''Returns a list composed of the next n elements returned by generator g.'''",
				"  z = []",
				"  if 0 == n: return(z)",
				"  for x in g:",
				"    z.append(x)",
				"    if n \u003e 1: n = n-1",
				"    else: return(z)",
				"take(52, A327977())"
			],
			"xref": [
				"Cf. A003415, A098699, A098700, A099302, A099303, A099307, A099308, A189760, A256750, A327966, A327968, A328117.",
				"Cf. A327975 for the subtree starting from 5, and also A263267 for another similar tree."
			],
			"keyword": "nonn,tabf",
			"offset": "1,1",
			"author": "_Antti Karttunen_, Oct 02 2019",
			"references": 6,
			"revision": 30,
			"time": "2020-03-24T15:54:15-04:00",
			"created": "2019-10-11T16:55:33-04:00"
		}
	]
}