{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A277130",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 277130,
			"data": "0,1,1,2,1,3,1,6,2,3,1,14,1,3,3,24,1,14,1,14,3,3,1,78,2,3,6,14,1,25,1,112,3,3,3,110,1,3,3,78,1,25,1,14,14,3,1,464,2,14,3,14,1,78,3,78,3,3,1,206,1,3,14,568,3,25,1,14,3,25,1,850,1,3,14,14",
			"name": "Number of planar branching factorizations of n.",
			"comment": [
				"A planar branching factorization of n is either the number n itself or a sequence of at least two planar branching factorizations, one of each factor in an ordered factorization of n. - _Gus Wiseman_, Sep 11 2018"
			],
			"link": [
				"Daniel Mondot, \u003ca href=\"/A277130/b277130.txt\"\u003eTable of n, a(n) for n = 1..9999\u003c/a\u003e",
				"A. Knopfmacher, M. E. Mays, \u003ca href=\"http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.113.7323\"\u003eA survey of factorization counting functions\u003c/a\u003e, International Journal of Number Theory, 1(4):563-581,(2005). See page 15."
			],
			"formula": [
				"a(prime^n) = A118376(n). a(product of n distinct primes) = A319122(n). - _Gus Wiseman_, Sep 11 2018"
			],
			"example": [
				"From _Gus Wiseman_, Sep 11 2018: (Start)",
				"The a(12) = 14 planar branching factorizations:",
				"  12,",
				"  (2*6), (3*4), (4*3), (6*2), (2*2*3), (2*3*2), (3*2*2),",
				"  (2*(2*3)), (2*(3*2)), (3*(2*2)), ((2*2)*3), ((2*3)*2), ((3*2)*2).",
				"(End)"
			],
			"mathematica": [
				"ordfacs[n_]:=If[n\u003c=1,{{}},Join@@Table[(Prepend[#1,d]\u0026)/@ordfacs[n/d],{d,Rest[Divisors[n]]}]]",
				"otfs[n_]:=Prepend[Join@@Table[Tuples[otfs/@f],{f,Select[ordfacs[n],Length[#]\u003e1\u0026]}],n];",
				"Table[Length[otfs[n]],{n,20}] (* _Gus Wiseman_, Sep 11 2018 *)"
			],
			"program": [
				"(C)",
				"#include \u003cstdio.h\u003e",
				"#include \u003cstring.h\u003e",
				"#include \u003cmath.h\u003e",
				"#define MAX 10000",
				"/* Number of planar branching factorizations of n. */",
				"#define lu unsigned long",
				"lu nbr[MAX]; /* number of branching */",
				"lu a, b, d, e; /* temporary variables */",
				"lu n; lu m, p; // factors of n",
				"lu x; // square root of n",
				"void main(unsigned argc, char *argv[])",
				"{",
				"  memset(nbr, 0, MAX*sizeof(lu));",
				"  for (b=0, n=1; n\u003cMAX; ++n)",
				"  {",
				"    d=0;",
				"    x=sqrt(n);",
				"    for (p=2; p\u003c=x;++p)",
				"    {",
				"      if ((n%p)==0)",
				"      {",
				"        m= n/p;",
				"        if (m\u003cp) break;",
				"        a = nbr[p] * nbr[m];",
				"        b += (m==p) ? a : 2*a;",
				"        e = nbr[p] * (nbr[m]-1) + (nbr[p]-1) * nbr[m];",
				"        d += (m==p) ? e : 2*e;",
				"      }",
				"    }",
				"    nbr[n]=b+d/2;",
				"    printf(\"%lu %lu\\n\", n, nbr[n]);",
				"    b = 1;",
				"  }",
				"} /* _Daniel Mondot_, May 19 2017 */"
			],
			"xref": [
				"Cf. A277120.",
				"Cf. A000108, A001055, A074206, A118376, A281113, A319122, A319123.",
				"Cf. A277130, A281118, A281119, A292504, A292505, A295279, A295281, A319136, A319137, A319138."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Michel Marcus_, Oct 01 2016",
			"ext": [
				"Terms a(65) and beyond from _Daniel Mondot_, May 19 2017"
			],
			"references": 6,
			"revision": 25,
			"time": "2019-01-02T11:49:54-05:00",
			"created": "2016-10-01T12:15:50-04:00"
		}
	]
}