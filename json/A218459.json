{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A218459",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 218459,
			"data": "1,2,1,3,2,1,1,2,7,1,3,1,1,2,11,1,2,1,2,7,1,3,2,1,1,1,3,2,1,1,3,2,1,2,1,3,1,2,23,1,2,1,7,1,1,3,2,3,2,1,1,7,1,2,1,7,1,3,1,1,2,1,2,11,1,1,2,1,2,1,1,7,3,1,2,22,1,1,1,1,2,1,7,1,3,2,1,1,1,3,2,19,3,2,2",
			"name": "a(n) is the smallest positive integer d such that prime(n) = x^2 + dy^2 has a solution (x,y) in integers.",
			"comment": [
				"a(n) = smallest positive integer d such that prime(n) is reducible in the ring Z[sqrt(-d)].",
				"If prime(n) == 1 or 2 mod 4, then a(n) = 1. If prime(n) == 3 mod 8, then a(n) = 2. If prime(n) == 7 mod 24 then a(n) = 3.",
				"If prime(n) == 23 mod 24, a(n) \u003e= 7. In particular, the above conditions are if and only if. - _Charles R Greathouse IV_, Oct 31 2012",
				"a(n) = 7 if and only if prime(n) is 11, 15, or 23 mod 28. - _Charles R Greathouse IV_, Nov 09 2012"
			],
			"reference": [
				"Ethan D. Bolker, Elementary Number Theory: An Algebraic Approach. Mineola, New York: Dover Publications (1969, reprinted 2007): p. 68, Theorem 24.5; p. 74, Theorem 25.4.",
				"David A. Cox, \"Primes of the Form x^2 + n y^2\", Wiley, 1989, Section 9, \"Ring class fields and p = x^2 + n y^2.\" - From _N. J. A. Sloane_, Dec 26 2012"
			],
			"link": [
				"Charles R Greathouse IV, \u003ca href=\"/A218459/b218459.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Alonso del Arte, \u003ca href=\"/A218459/a218459_1.png\"\u003eDiagram illustrating first six terms on the complex plane\u003c/a\u003e"
			],
			"formula": [
				"a(n) \u003e= A088192(n). - _Charles R Greathouse IV_, Oct 31 2012"
			],
			"example": [
				"a(1) = 1 because the first prime is 2, which is 1^2 + 1^2.",
				"a(2) = 2 because the second prime is 3, which is 1^2 + 2*1^2, but not of the form x^2 + y^2 for any integers x, y.",
				"a(3) = 1 because the third prime is 5, which is 2^2 + 1*1^2.",
				"a(4) = 3 because the third prime is 7, which is 2^2 + 3*1^2, but not of the form x^2 + y^2 or x^2 + 2y^2 for any integers x, y."
			],
			"mathematica": [
				"r[n_, d_] := Reduce[ Prime[n] == x^2 + d*y^2, {x, y}, Integers]; a[n_] := For[d = 1, True, d++, If[r[n, d] =!= False, Return[d] ] ]; Table[a[n], {n, 1, 95}] (* _Jean-François Alcover_, Apr 04 2013 *)"
			],
			"program": [
				"(PARI) ndv(d, p)=(#bnfisintnorm(bnfinit(y^2+d), p))==0",
				"forprime(p=2, 500, for(d=1, p, if(!ndv(d, p), print1(d, \", \"); break))) \\\\ _Georgi Guninski_, Oct 27 2012",
				"(PARI) check(d,p)={",
				"   if(kronecker(-d,p)\u003c0 || #bnfisintnorm(bnfinit('x^2+d),p)==0, return(0));",
				"   for(y=1,sqrtint(p\\d),if(issquare(p-d*y^2),return(1)));",
				"   0",
				"};",
				"do(p)={",
				"   if(p%24\u003c23,return(if(p%4\u003c3,1,if(p%8==3,2,3))));",
				"   if(kronecker(p,7)\u003e0, return(7));",
				"   if(check(11,p), return(11));",
				"   for(d=19,p,",
				"    if(issquarefree(d) \u0026\u0026 check(d,p), return(d))",
				"   )",
				"};",
				"apply(do, primes(100)) \\\\ _Charles R Greathouse IV_, Oct 31 2012",
				"(PARI) A218459(n)={my(p=prime(n),d);while(d++,for(y=1,sqrtint((p-1)\\d), issquare(p-d*y^2)\u0026\u0026return(d)))} \\\\ _M. F. Hasler_, May 05 2013"
			],
			"xref": [
				"Cf. A088192."
			],
			"keyword": "nonn,nice",
			"offset": "1,2",
			"author": "_Alonso del Arte_, Oct 29 2012",
			"ext": [
				"a(76) corrected by _Charles R Greathouse IV_, Nov 13 2012",
				"Edited by _N. J. A. Sloane_, Dec 07 2012, Dec 26 2012"
			],
			"references": 2,
			"revision": 81,
			"time": "2018-12-25T11:32:08-05:00",
			"created": "2012-10-31T14:22:23-04:00"
		}
	]
}