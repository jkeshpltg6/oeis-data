{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A320047",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 320047,
			"data": "5,6,28,18,81,36,176,60,325,90,540,126,833,168,1216,216,1701,270,2300,330,3025,396,3888,468,4901,546,6076,630,7425,720,8960,816,10693,918,12636,1026,14801,1140,17200,1260,19845,1386,22748,1518,25921,1656",
			"name": "Consider coefficients U(m,l,k) defined by the identity Sum_{k=1..l} Sum_{j=0..m} A302971(m,j)/A304042(m,j) * k^j * (T-k)^j = Sum_{k=0..m} (-1)^(m-k) * U(m,l,k) * T^k that holds for all positive integers l,m,T. This sequence gives 2-column table read by rows, where n-th row lists coefficients U(1,n,k) for k = 0, 1 and n \u003e= 1.",
			"comment": [
				"For l=T, the identity takes the form T^(2m+1) = Sum_{k=0..m} (-1)^(m-k)*U(m,T,k)*T^k, which holds for all positive integers T and m."
			],
			"link": [
				"Max Alekseyev, \u003ca href=\"https://mathoverflow.net/q/309470\"\u003eDerivation of the general formula for U(m,n,k)\u003c/a\u003e, MathOverflow, 2018.",
				"Petro Kolosov, \u003ca href=\"https://arxiv.org/abs/1603.02468\"\u003eAnother Power Identity involving Binomial Theorem and Faulhaber's formula\u003c/a\u003e, arXiv:1603.02468 [math.NT], 2016-2018.",
				"Petro Kolosov, \u003ca href=\"https://kolosovpetro.github.io/pdf/OEIS_Um(n,k)_coefficients.pdf\"\u003eMore details on derivation of present sequence\u003c/a\u003e.",
				"Petro Kolosov, \u003ca href=\"https://kolosovpetro.github.io/arxiv_1603_02468/identity_1_1_r_h_s.txt\"\u003eMathematica program\u003c/a\u003e, verifies the identity T^(2m+1) = Sum_{k=0..m} (-1)^(m-k)*U(m,T,k)*T^k for m=0,1,...,12."
			],
			"formula": [
				"U(m,l,t) = (-1)^m * Sum_{k=1..l} Sum_{j=t..m} binomial(j,t) * R(m,j) * k^{2j-t} * (-1)^j, where m = 1, l \u003e= 1 and R(m,j) = A302971(m,j)/A304042(m,j); after _Max Alekseyev_, see links.",
				"Conjectures from _Colin Barker_, Aug 03 2019: (Start)",
				"G.f.: x*(5 + 6*x + 8*x^2 - 6*x^3 - x^4) / ((1 - x)^4*(1 + x)^4).",
				"a(n) = (4 - 4*(-1)^n - 3*(-5+(-1)^n)*n - 3*(-3+(-1)^n)*n^2 + (1+(-1)^(1+n))*n^3) / 8.",
				"a(n) = 4*a(n-2) - 6*a(n-4) + 4*a(n-6) - a(n-8) for n\u003e8.",
				"(End)"
			],
			"example": [
				"        column  column",
				"   l      k=0     k=1",
				"  ---   ------  ------",
				"   1       5       6",
				"   2      28      18",
				"   3      81      36",
				"   4     176      60",
				"   5     325      90",
				"   6     540     126",
				"   7     833     168",
				"   8    1216     216",
				"   9    1701     270",
				"  10    2300     330",
				"  11    3025     396",
				"  12    3888     468",
				"  ..."
			],
			"mathematica": [
				"(* Define the R[n,k] := A302971(n,k)/A304042(n,k) *)",
				"R[n_, k_] := 0",
				"R[n_, k_] := (2 k + 1)*Binomial[2 k, k]*",
				"   Sum[R[n, j]*Binomial[j, 2 k + 1]*(-1)^(j - 1)/(j - k)*",
				"   BernoulliB[2 j - 2 k], {j, 2 k + 1, n}] /; 2 k + 1 \u003c= n",
				"R[n_, k_] := (2 n + 1)*Binomial[2 n, n] /; k == n;",
				"(* Define the U(m,l,t) coefficients *)",
				"U[m_, l_, t_] := (-1)^m Sum[Sum[Binomial[j, t] R[m,j] k^(2 j - t) (-1)^j, {j, t, m}], {k, 1, l}];",
				"(* Define the value of the variable 'm' to be m = 1 for A320047 *)",
				"m = 1;",
				"(* Print first 10 rows of U(m,l,t) coefficients for 'm' defined above *)",
				"Column[Table[U[m, l, t], {l, 1, 10}, {t, 0, m}]]"
			],
			"xref": [
				"The case m=2 is A316349.",
				"The case m=3 is A316387.",
				"Column k=0 is A275709.",
				"Column k=1 is A028896.",
				"Cf. A302971, A304042, A287326, A300656, A300785."
			],
			"keyword": "nonn,tabf",
			"offset": "1,1",
			"author": "_Kolosov Petro_, Oct 04 2018",
			"references": 5,
			"revision": 41,
			"time": "2019-08-03T08:44:47-04:00",
			"created": "2018-10-18T14:58:32-04:00"
		}
	]
}