{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A212885",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 212885,
			"data": "1,-2,-4,8,6,-8,-8,0,12,-10,-8,24,8,-8,-16,0,6,-16,-12,24,24,-16,-8,0,24,-10,-24,32,0,-24,-16,0,12,-16,-16,48,30,-8,-24,0,24,-32,-16,24,24,-24,-16,0,8,-18,-28,48,24,-24,-32,0,48,-16,-8,72,0,-24,-32",
			"name": "Expansion of phi(q) * phi(-q)^2 in powers of q where phi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A212885/b212885.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of phi(-x) * phi(-x^2)^2 = phi(-x^2)^4 / phi(x) in powers of x where phi() is a Ramanujan theta function.",
				"Expansion of eta(q^2)^3 * eta(q)^2 / eta(q^4)^2 in powers of q.",
				"Euler transform of period 4 sequence [-2, -5, -2, -3, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (8 t)) = 32 (t/i)^(3/2) g(t) where q = exp(2 Pi i t) and g() is the g.f. for A045828.",
				"G.f.: Product_{k\u003e0} (1 - x^(2*k))^3 * (1 - x^k)^2 / (1 - x^(4*k))^2.",
				"a(4*n) = A005875(n). a(4*n + 1) = -2 * A045834(n). a(4*n + 2) = - A005877(n) = -4 * A045828(n).",
				"a(8*n) = A004015(n). a(8*n + 3) = A005878(n) = 8 * A008443(n). a(8*n + 4)= A005887(n). a(8*n + 5) = -2 * A004024(n). a(8*n + 6) = -8 * A213624(n). a(8*n + 7) = 0."
			],
			"example": [
				"G.f. = 1 - 2*q - 4*q^2 + 8*q^3 + 6*q^4 - 8*q^5 - 8*q^6 + 12*q^8 - 10*q^9 + ..."
			],
			"mathematica": [
				"a[n_]:= SeriesCoefficient[EllipticTheta[3, 0, q]* EllipticTheta[3, 0, -q]^2, {q, 0, n}]; Table[a[n], {n, 0, 50}] (* _G. C. Greubel_, Nov 30 2017 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A)^3 * eta(x + A)^2 / eta(x^4 + A)^2, n))};"
			],
			"xref": [
				"Cf. A004015, A004024, A005875, A005877, A005878, A005887, A008443, A045828, A045834, A213624."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, May 29 2012",
			"references": 7,
			"revision": 17,
			"time": "2021-03-12T22:24:46-05:00",
			"created": "2012-05-31T12:19:11-04:00"
		}
	]
}