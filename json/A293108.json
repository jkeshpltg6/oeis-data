{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A293108",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 293108,
			"data": "1,1,0,1,1,0,1,1,2,0,1,1,3,3,0,1,1,3,6,5,0,1,1,3,7,15,7,0,1,1,3,7,19,31,11,0,1,1,3,7,20,48,73,15,0,1,1,3,7,20,53,131,155,22,0,1,1,3,7,20,54,157,348,351,30,0,1,1,3,7,20,54,163,455,954,755,42,0",
			"name": "Number A(n,k) of multisets of nonempty words with a total of n letters over k-ary alphabet such that within each prefix of a word every letter of the alphabet is at least as frequent as the subsequent alphabet letter; square array A(n,k), n\u003e=0, k\u003e=0, read by antidiagonals.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A293108/b293108.txt\"\u003eAntidiagonals n = 0..80, flattened\u003c/a\u003e"
			],
			"formula": [
				"G.f. of column k: Product_{j\u003e=1} 1/(1-x^j)^A182172(j,k).",
				"A(n,k) = Sum_{j=0..k} A293109(n,j).",
				"A(n,n) = A(n,k) for all k \u003e= n."
			],
			"example": [
				"Square array A(n,k) begins:",
				"  1,  1,   1,   1,   1,   1,   1,   1, ...",
				"  0,  1,   1,   1,   1,   1,   1,   1, ...",
				"  0,  2,   3,   3,   3,   3,   3,   3, ...",
				"  0,  3,   6,   7,   7,   7,   7,   7, ...",
				"  0,  5,  15,  19,  20,  20,  20,  20, ...",
				"  0,  7,  31,  48,  53,  54,  54,  54, ...",
				"  0, 11,  73, 131, 157, 163, 164, 164, ...",
				"  0, 15, 155, 348, 455, 492, 499, 500, ..."
			],
			"maple": [
				"h:= l-\u003e (n-\u003e add(i, i=l)!/mul(mul(1+l[i]-j+add(`if`(l[k]",
				"    \u003cj, 0, 1), k=i+1..n), j=1..l[i]), i=1..n))(nops(l)):",
				"g:= proc(n, i, l) option remember;",
				"      `if`(n=0, h(l), `if`(i\u003c1, 0, `if`(i=1, h([l[], 1$n]),",
				"        g(n, i-1, l) +`if`(i\u003en, 0, g(n-i, i, [l[], i])))))",
				"    end:",
				"A:= proc(n, k) option remember; `if`(n=0, 1, add(add(g(d, k, [])",
				"      *d, d=numtheory[divisors](j))*A(n-j, k), j=1..n)/n)",
				"    end:",
				"seq(seq(A(n, d-n), n=0..d), d=0..14);"
			],
			"mathematica": [
				"h[l_] := Function [n, Total[l]!/Product[Product[1 + l[[i]] - j + Sum[If[ l[[k]] \u003c j, 0, 1], {k, i+1, n}], {j, 1, l[[i]]}], {i, n}]][Length[l]];",
				"g[n_, i_, l_] := g[n, i, l] = If[n == 0, h[l], If[i \u003c 1, 0, If[i == 1, h[Join[l, Table[1, n]]], g[n, i - 1, l] + If[i \u003e n, 0, g[n - i, i, Append[l, i]]]]]];",
				"A[n_, k_] := A[n, k] = If[n == 0, 1, Sum[Sum[g[d, k, {}]*d, {d, Divisors[j] }]*A[n - j, k], {j, 1, n}]/n];",
				"Table[A[n, d-n], {d, 0, 14}, {n, 0, d}] // Flatten (* _Jean-François Alcover_, Jun 03 2018, from Maple *)"
			],
			"xref": [
				"Columns k=0-10 give: A000007, A000041, A293732, A293733, A293734, A293735, A293736, A293737, A293738, A293739, A293740.",
				"Main diagonal gives A293110.",
				"Cf. A182172, A293109, A293112."
			],
			"keyword": "nonn,tabl",
			"offset": "0,9",
			"author": "_Alois P. Heinz_, Sep 30 2017",
			"references": 13,
			"revision": 26,
			"time": "2018-09-20T12:12:20-04:00",
			"created": "2017-10-15T09:19:19-04:00"
		}
	]
}