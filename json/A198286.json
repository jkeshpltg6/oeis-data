{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A198286",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 198286,
			"data": "1,5,10,9,26,50,50,25,19,130,122,90,170,250,260,41,290,95,362,234,500,610,530,250,51,850,100,450,842,1300,962,105,1220,1450,1300,171,1370,1810,1700,650,1682,2500,1850,1098,494,2650,2210,410,99,255,2900,1530,2810",
			"name": "a(n) = Sum_{d|n} (A053143(d) or smallest square divisible by d).",
			"comment": [
				"Multiplicative function with a(p^e) = 1+2*(p^(e+2)-p^2)/(p^2-1) if e is even else a(p^e)=(1+p^2)((p^(e+1)-1)/(p^2-1)). Examples: a(9)=a(3^2)=1+2*((81-9)/(9-1))=1+2*9=19; a(8)=a(2^3)=(1+4)((16-1)/(4-1))=5*5=25.",
				"Another definition of a(n): Sum_{d|n} (d*core(d)), where core(d) is the squarefree part of d (A007913), i.e., inverse Mobius transform of A053143."
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A198286/b198286.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"Dirichlet g.f.: zeta(s)*zeta(s-2)*zeta(2s-2)/zeta(2s-4). - _R. J. Mathar_, Mar 12 2012",
				"Sum_{k=1..n} a(k) ~ Pi^2 * Zeta(3) * n^3 / 45. - _Vaclav Kotesovec_, Feb 02 2019"
			],
			"example": [
				"a(18) = 95 because 18=2*3^2, so a(18) = (1+4)(1+9+9) = 5*19 = 95.",
				"a(20) = 234 because 20=2^2*5, so a(20) = (1+4+4)(1+25) = 9*26 = 234."
			],
			"mathematica": [
				"ssq[n_] := For[k=1, True, k++, If[ Divisible[s = k^2, n], Return[s]]]; a[n_] := Sum[ ssq[d], {d, Divisors[n]}]; Table[a[n], {n, 1, 53}] (* _Jean-François Alcover_, Sep 03 2012 *)",
				"f[p_, e_] := If[OddQ[e], (1+p^2)((p^(e+1)-1)/(p^2-1)), 1+2*(p^(e+2)-p^2)/(p^2-1)]; a[1] = 1; a[n_] := Times @@ (f @@@ FactorInteger[n]); Array[a, 53] (* _Amiram Eldar_, Sep 05 2020 *)"
			],
			"program": [
				"(PARI) a(n)=sumdiv(n,d,d*core(d)) \\\\ _Charles R Greathouse IV_, Oct 30 2011"
			],
			"xref": [
				"Similar to A068976 (sum of square part of d) and A069088  (sum of squarefree part of d).",
				"Cf. A007913, A053143."
			],
			"keyword": "nonn,mult",
			"offset": "1,2",
			"author": "_Antonio Roldán_, Oct 23 2011",
			"references": 1,
			"revision": 23,
			"time": "2020-09-05T07:46:34-04:00",
			"created": "2011-10-31T00:34:46-04:00"
		}
	]
}