{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A052530",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 52530,
			"data": "0,2,8,30,112,418,1560,5822,21728,81090,302632,1129438,4215120,15731042,58709048,219105150,817711552,3051741058,11389252680,42505269662,158631825968,592022034210,2209456310872,8245803209278,30773756526240",
			"name": "a(n) = 4*a(n-1) - a(n-2), with a(0) = 0, a(1) = 2.",
			"comment": [
				"a(n-1) and a(n+1) are the solutions for c if b = a(n) in (b^2 + c^2)/(b*c + 1) = 4 and there are no other pairs of solutions apart from consecutive pairs of terms in this sequence. Cf. A061167. - _Henry Bottomley_, Apr 18 2001",
				"a(n)^2 for n \u003e= 1 gives solutions to A007913(3*x+4) = A007913(x). - _Benoit Cloitre_, Apr 07 2002",
				"For all elements n of the sequence, 3*n^2 + 4 is a perfect square. Lim_{n-\u003einf} a(n)/a(n-1) = 2 + sqrt(3). - _Gregory V. Richardson_, Oct 06 2002",
				"a(n) = the number of compositions of the integer 2*n into even parts, where each part 2*i comes in 2*i colors. (Dedrickson, Theorem 3.2.6) An example is given below. Cf. A052529, A095263. - _Peter Bala_, Sep 17 2013",
				"Except for an initial 1, this is the p-INVERT of (1, 1, 1, 1, 1, ...) for p(S) = 1 - 2*S - 2*S^2; see A291000.  - _Clark Kimberling_, Aug 24 2017",
				"a(n+1) is the number of spanning trees of the graph P_n, where P_n is a 2 X n grid with two additional vertices, u and v, where u is adjacent to (1,1) and (2,1), and v is adjacent to (1,n) and (2,n). - _Kevin Long_, May 04 2018"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A052530/b052530.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"Andersen, K., Carbone, L. and Penta, D., \u003ca href=\"https://pdfs.semanticscholar.org/8f0c/c3e68d388185129a56ed73b5d21224659300.pdf\"\u003eKac-Moody Fibonacci sequences, hyperbolic golden ratios, and real quadratic fields\u003c/a\u003e, Journal of Number Theory and Combinatorics, Vol 2, No. 3 pp 245-278, 2011. See Section 9.",
				"Hacène Belbachir, Soumeya Merwa Tebtoub, and László Németh, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL23/Nemeth/nemeth7.html\"\u003eEllipse Chains and Associated Sequences\u003c/a\u003e, J. Int. Seq., Vol. 23 (2020), Article 20.8.5.",
				"Daniel Birmajer, Juan B. Gil, and Michael D. Weiner, \u003ca href=\"https://arxiv.org/abs/1707.07798\"\u003e(an + b)-color compositions\u003c/a\u003e, arXiv:1707.07798 [math.CO], 2017.",
				"Niccolò Castronuovo, \u003ca href=\"https://arxiv.org/abs/2102.02739\"\u003eOn the number of fixed points of the map gamma\u003c/a\u003e, arXiv:2102.02739 [math.NT], 2021. Mentions this sequence.",
				"C. R. Dedrickson III, \u003ca href=\"https://digitalcommons.georgiasouthern.edu/etd/17\"\u003eCompositions, Bijections, and Enumerations\u003c/a\u003e Thesis, 2012, Jack N. Averitt College of Graduate Studies, Georgia Southern University.",
				"J.-P. Ehrmann et al., \u003ca href=\"http://forumgeom.fau.edu/POLYA/ProblemCenter/POLYA002.html\"\u003eProblem POLYA002\u003c/a\u003e, Integer pairs (x,y) for which (x^2+y^2)/(1+pxy) is an integer.",
				"F. Goebel and A. A. Jagers, \u003ca href=\"http://dx.doi.org/10.1016/0095-8956(79)90010-8\"\u003eOn a conjecture of Tutte concerning minimal tree numbers\u003c/a\u003e, J. Combin. Theory Ser. B 26 (1979), no. 3, 346-348. MR0535948 (80m:05064). [From _N. J. A. Sloane_, Feb 20 2012]",
				"A. F. Horadam, \u003ca href=\"http://www.fq.math.ca/Scanned/3-3/horadam.pdf\"\u003eBasic Properties of a Certain Generalized Sequence of Numbers\u003c/a\u003e, Fibonacci Quarterly, Vol. 3, No. 3, 1965, pp. 161-176.",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=460\"\u003eEncyclopedia of Combinatorial Structures 460\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/transforms.txt\"\u003eTransforms\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-1)."
			],
			"formula": [
				"G.f.: 2*x/(1 - 4*x + x^2).",
				"Invert transform of even numbers: a(n) = 2*Sum_{k=1..n} k*a(n-k). - _Vladeta Jovovic_, Apr 27 2001",
				"From _Gregory V. Richardson_, Oct 06 2002: (Start)",
				"a(n) = Sum(-(1/3)*(-1 + 2*alpha)*alpha^(-1 - n), alpha = root of (1 - 4*Z + Z^2),",
				"a(n) = (((2+sqrt(3))^(n+1) -(2-sqrt(3))^(n+1)) -((2+sqrt(3))^(n) -(2 -sqrt(3))^(n)) +((2+sqrt(3))^(n-1) -(2-sqrt(3))^(n-1)))/(3*sqrt(3)). (End)",
				"a(n) = A071954(n) - 2. - _N. J. A. Sloane_, Feb 20 2005",
				"a(n) = (2*sinh(2n*arcsinh(1/sqrt(2))))/sqrt(3). - _Herbert Kociemba_, Apr 24 2008",
				"a(n) = 2*A001353(n). - _R. J. Mathar_, Oct 26 2009",
				"a(n) = ((3 - 2*sqrt(3))/3)*(2 - sqrt(3))^(n - 1) + ((3 + 2*sqrt(3))/3)*(2 + sqrt(3))^(n - 1). - _Vincenzo Librandi_, Nov 20 2010",
				"a(n) = floor((2 + sqrt(3))^n/sqrt(3)). - _Zak Seidov_, Mar 31 2011",
				"a(n) = ((2 + sqrt(3))^n - (2 - sqrt(3))^n)/sqrt(3). (See Horadam for construction.) - _Johannes Boot_, Jan 08 2012",
				"a(n) = A217233(n) + A217233(n-1) with A217233(-1) = -1. - _Bruno Berselli_, Oct 01 2012",
				"a(n) = A001835(n+1) - A001835(n). - _Kevin Long_, May 04 2018",
				"E.g.f.: (exp((2 + sqrt(3))*x) - exp((2 - sqrt(3))*x))/sqrt(3). - _Franck Maminirina Ramaharo_, Nov 12 2018"
			],
			"example": [
				"Colored compositions. a(2) = 8: There are two compositions of 4 into even parts, namely 4 and 2 + 2. Using primes to indicate the coloring of parts, the 8 colored compositions are 4, 4', 4'', 4''', 2 + 2, 2 + 2', 2' + 2 and 2' + 2'. - _Peter Bala_, Sep 17 2013"
			],
			"maple": [
				"spec := [S,{S=Sequence(Prod(Union(Z,Z),Sequence(Z),Sequence(Z)))},unlabeled]: seq(combstruct[count](spec, size=n), n=0..20);",
				"s := sqrt(3): a := n -\u003e ((2-s)^n-(s+2)^n)/(s*(s-2)*(s+2)):",
				"seq(simplify(a(n)), n=0..24); # _Peter Luschny_, Apr 28 2020"
			],
			"mathematica": [
				"p=1; c=2; a[0]=0; a[1]=c; a[n_]:=a[n]=p*c^2*a[n-1]-a[n-2]; Table[a[n], {n, 0, 20}]",
				"NestList[2 # + Sqrt[4 + 3 #^2]\u0026, 0, 200] (* _Zak Seidov_, Mar 31 2011 *)",
				"LinearRecurrence[{4, -1}, {0, 2}, 25] (* _T. D. Noe_, Jan 09 2012 *)"
			],
			"program": [
				"(PARI) { polya002(p,c,m) = local(v,w,j,a); w=0; print1(w,\", \"); v=c; print1(v,\", \"); j=1; while(j\u003c=m,a=p*c^2*v-w; print1(a,\", \"); w=v; v=a; j++) };",
				"polya002(1,2,25)",
				"(PARI) my(x='x+O('x^30)); concat([0], Vec(2*x/(1-4*x+x^2))) \\\\ _G. C. Greubel_, Feb 25 2019",
				"(PARI) first(n) = n = max(n, 2); my(res = vector(n)); res[1] = 0; res[2] = 2; for(i = 3, n, res[i] = 4 * res[i-1] - res[i-2]); res \\\\ _David A. Corneth_, Apr 28 2020",
				"(Haskell)",
				"a052530 n = a052530_list !! n",
				"a052530_list =",
				"   0 : 2 : zipWith (-) (map (* 4) $ tail a052530_list) a052530_list",
				"-- _Reinhard Zumkeller_, Sep 29 2011",
				"(MAGMA) I:=[0,2]; [n le 2 select I[n] else 4*Self(n-1) - Self(n-2): n in [1..30]]; // _G. C. Greubel_, Feb 25 2019",
				"(Sage) (2*x/(1-4*x+x^2)).series(x, 30).coefficients(x, sparse=False) # _G. C. Greubel_, Feb 25 2019"
			],
			"xref": [
				"Cf. A007913, A003699, A217233.",
				"Cf. A052529, A095263."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,2",
			"author": "encyclopedia(AT)pommard.inria.fr, Jan 25 2000",
			"ext": [
				"More terms from _James A. Sellers_, Jun 06 2000",
				"Edited by _N. J. A. Sloane_, Nov 11 2006",
				"a(0) changed to 0 and entry revised accordingly by _Max Alekseyev_, Nov 15 2007",
				"Signs in definition corrected by _John W. Layman_, Nov 20 2007"
			],
			"references": 29,
			"revision": 117,
			"time": "2021-02-05T14:31:31-05:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}