{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A293416",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 293416,
			"data": "2,1,9,2,0,4,7,7,3,3,7,2,5,0,6,0,7,5,8,3,0,3,5,7,9,9,3,1,3,5,3,8,6,6,4,7,9,9,8,5,3,2,7,6,5,4,6,2,4,2,8,4,7,1,7,6,8,4,5,6,0,3,0,7,8,4,7,0,5,9,2,6,2,1,8,7,3,7,9,3,5,0,7,3,2,9,2,3,9,0,5,9,8,8,1,4,8,0,4,5,2,7,0,6,4,2,3,7",
			"name": "Decimal expansion of the minimum ripple factor for a ninth-order, reflectionless, Chebyshev filter.",
			"comment": [
				"This is the smallest ripple factor (a constant) for which the prototype elements of the ninth-order generalized reflectionless filter topology (see Morgan, 2017) needs no negative elements. It is also the ripple factor for which the first two and last two Chebyshev prototype parameters (of the canonical ladder, or Cauer, topology) are equal.",
				"Other related sequences in the OEIS are the decimal and continued fraction expansions of the limiting ripple factors for third, fifth, seventh, and ninth order, as well as for the limiting case where the order diverges to infinity. As these ripple factors do approach a common limit very quickly, the sequences for the fifth- and higher-order constants share the same initial terms, to greater length as the order increases.",
				"There are simple radical expressions for the third- and fifth-order constants (see formulas). Further, the third-order constant is a quadratic irrational, thus having a repeating continued fraction expansion. I do not know if such simple expressions or patterns exist for the higher-order constants or the limiting (infinite-order) constant."
			],
			"reference": [
				"M. Morgan, Reflectionless Filters, Norwood, MA: Artech House, pp. 129-132, January 2017."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A293416/b293416.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e"
			],
			"formula": [
				"Equals sqrt(exp(4*arctanh(exp(-2*9*arcsinh(sqrt(1/2*sin(Pi/9)tan(Pi/9))))))-1)."
			],
			"example": [
				"0.2192047733..."
			],
			"mathematica": [
				"RealDigits[Sqrt[Exp[4 ArcTanh[Exp[-2*9*ArcSinh[Sqrt[1/2*Sin[Pi/9] Tan[Pi/9]]]]]] - 1], 10,100][[1]]"
			],
			"program": [
				"(PARI) sqrt(exp(4*atanh(exp(-2*9*asinh(sqrt(1/2*sin(Pi/9)*tan(Pi/9))))))-1) \\\\ _Michel Marcus_, Oct 16 2017",
				"(MAGMA) R:= RealField(); Sqrt(Exp(4*Argtanh(Exp(-2*9*Argsinh(Sqrt(1/2* Sin(Pi(R)/9)*Tan(Pi(R)/9))))))-1); // _G. C. Greubel_, Feb 16 2018"
			],
			"xref": [
				"Decimal expansions (A020784, A293409, A293415, A293416, A293417) and continued fractions (A040021, A293768, A293769, A293770, A293882) for third-, fifth-, seventh-, ninth-order and the limiting \"infinite-order\" constant, respectively."
			],
			"keyword": "cons,easy,nonn",
			"offset": "0,1",
			"author": "_Matthew A. Morgan_, Oct 15 2017",
			"references": 8,
			"revision": 36,
			"time": "2019-02-22T01:51:30-05:00",
			"created": "2017-10-18T13:43:31-04:00"
		}
	]
}