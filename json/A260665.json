{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A260665",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 260665,
			"data": "1,1,2,5,1,15,7,1,1,52,39,13,12,2,1,1,203,211,112,103,41,24,17,5,2,1,1,877,1168,843,811,492,337,238,122,68,39,28,8,5,2,1,1,4140,6728,6089,6273,4851,3798,2956,1960,1303,859,594,314,204,110,64,43,17,8,5,2,1,1",
			"name": "Number T(n,k) of permutations of [n] with exactly k (possibly overlapping) occurrences of the generalized pattern 12-3; triangle T(n,k), n\u003e=0, 0\u003c=k\u003c=(n-1)*(n-2)/2-[n=0], read by rows.",
			"comment": [
				"Patterns 1-23, 3-21, 32-1 give the same triangle."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A260665/b260665.txt\"\u003eRows n = 0..50, flattened\u003c/a\u003e",
				"A. Claesson and T. Mansour, \u003ca href=\"https://arxiv.org/abs/math/0110036\"\u003eCounting occurrences of a pattern of type (1,2) or (2,1) in permutations\u003c/a\u003e, arXiv:math/0110036 [math.CO], 2001"
			],
			"formula": [
				"Sum_{k\u003e0} k * T(n,k) = A001754(n)."
			],
			"example": [
				"T(4,1) = 7: 1324, 1342, 2134, 2314, 2341, 3124, 4123.",
				"T(4,2) = 1: 1243.",
				"T(4,3) = 1: 1234.",
				"T(5,3) = 12: 12534, 12543, 13245, 13425, 13452, 21345, 23145, 23415, 23451, 31245, 41235, 51234.",
				"T(5,4) = 2: 12435, 12453.",
				"T(5,5) = 1: 12354.",
				"T(5,6) = 1: 12345.",
				"Triangle T(n,k) begins:",
				"0 :   1;",
				"1 :   1;",
				"2 :   2;",
				"3 :   5,    1;",
				"4 :  15,    7,   1,   1;",
				"5 :  52,   39,  13,  12,   2,   1,   1;",
				"6 : 203,  211, 112, 103,  41,  24,  17,   5,  2,  1,  1;",
				"7 : 877, 1168, 843, 811, 492, 337, 238, 122, 68, 39, 28, 8, 5, 2, 1, 1;"
			],
			"maple": [
				"b:= proc(u, o) option remember;",
				"      `if`(u+o=0, 1, add(b(u-j, o+j-1), j=1..u)+",
				"       add(expand(b(u+j-1, o-j)*x^(o-j)), j=1..o))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=0..degree(p)))(b(n, 0)):",
				"seq(T(n), n=0..10);"
			],
			"mathematica": [
				"b[u_, o_] := b[u, o] = If[u + o == 0, 1, Sum[b[u - j, o + j - 1], {j, 1, u}] + Sum[Expand[b[u + j - 1, o - j]*x^(o - j)], {j, 1, o}]]; T[n_] := Function[p, Table[Coefficient[p, x, i], {i, 0, Exponent[p, x]}]][b[n, 0] ]; Table[T[n], {n, 0, 10}] // Flatten (* _Jean-François Alcover_, Jul 10 2017, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Columns k=0-10 give: A000110, A092923, A264451, A264452, A264453, A264454, A264455, A264456, A264457, A264458, A264459.",
				"Row sums give A000142.",
				"Cf. A000217, A001754, A260670, A263776."
			],
			"keyword": "nonn,tabf",
			"offset": "0,3",
			"author": "_Alois P. Heinz_, Nov 14 2015",
			"references": 14,
			"revision": 29,
			"time": "2018-05-09T09:55:26-04:00",
			"created": "2015-11-14T05:31:29-05:00"
		}
	]
}