{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A196052",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 196052,
			"data": "0,1,2,2,2,3,3,3,4,3,2,4,3,4,4,4,2,5,4,4,5,3,3,5,4,4,6,5,3,5,2,5,4,3,5,6,4,5,5,5,2,6,3,4,6,4,3,6,6,5,4,5,5,7,4,6,6,4,2,6,4,3,7,6,5,5,2,4,5,6,4,7,3,5,6,6,5,6,3,6,8,3,2,7,4,4,5,5,5,7,6,5,4,4,6,7,3,7,6,6,3,5,4,6,7,6,4,8,2,5",
			"name": "Sum of the degrees of the nodes at level 1 in the rooted tree with Matula-Goebel number n.",
			"comment": [
				"The Matula-Goebel number of a rooted tree can be defined in the following recursive manner: to the one-vertex tree there corresponds the number 1; to a tree T with root degree 1 there corresponds the t-th prime number, where t is the Matula-Goebel number of the tree obtained from T by deleting the edge emanating from the root; to a tree T with root degree m\u003e=2 there corresponds the product of the Matula-Goebel numbers of the m branches of T."
			],
			"reference": [
				"D. W. Matula, A natural rooted tree enumeration by prime factorization, SIAM Review, 10, 1968, 273."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A196052/b196052.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"E. Deutsch, \u003ca href=\"http://arxiv.org/abs/1111.4288\"\u003eTree statistics from Matula numbers\u003c/a\u003e, arXiv preprint arXiv:1111.4288, 2011",
				"F. Goebel, \u003ca href=\"https://doi.org/10.1016/0095-8956(80)90049-0\"\u003eOn a 1-1-correspondence between rooted trees and natural numbers\u003c/a\u003e, J. Combin. Theory, B 29 (1980), 141-143.",
				"I. Gutman and A. Ivic, \u003ca href=\"https://doi.org/10.1016/0012-365X(95)00182-V\"\u003eOn Matula numbers\u003c/a\u003e, Discrete Math., 150, 1996, 131-142.",
				"I. Gutman and Yeong-Nan Yeh, \u003ca href=\"http://www.emis.de/journals/PIMB/067/3.html\"\u003eDeducing properties of trees from their Matula numbers\u003c/a\u003e, Publ. Inst. Math., 53 (67), 1993, 17-22.",
				"\u003ca href=\"/index/Mat#matula\"\u003eIndex entries for sequences related to Matula-Goebel numbers\u003c/a\u003e"
			],
			"formula": [
				"a(1)=0; if n = p(t) (the t-th prime), then a(n)=1+G(t), where G(t) is the number of prime divisors of t counted with multiplicities;  if n=rs (r,s\u003e=2), then a(n)=a(r)+a(s). The Maple program is based on this recursive formula."
			],
			"example": [
				"a(7)=3 because the rooted tree with Matula-Goebel number 7 is the rooted tree Y.",
				"a(2^m) = m because the rooted tree with Matula-Goebel number 2^m is a star with m edges."
			],
			"maple": [
				"with(numtheory): a := proc (n) local r, s: r := proc (n) options operator, arrow: op(1, factorset(n)) end proc: s := proc (n) options operator, arrow: n/r(n) end proc: if n = 1 then 0 elif bigomega(n) = 1 then 1+bigomega(pi(n)) else a(r(n))+a(s(n)) end if end proc: seq(a(n), n = 1 .. 110);"
			],
			"program": [
				"(Haskell)",
				"import Data.List (genericIndex)",
				"a196052 n = genericIndex a196052_list (n - 1)",
				"a196052_list = 0 : g 2 where",
				"   g x = y : g (x + 1) where",
				"     y = if t \u003e 0 then a001222 t + 1 else a196052 r + a196052 s",
				"         where t = a049084 x; r = a020639 x; s = x `div` r",
				"-- _Reinhard Zumkeller_, Sep 03 2013",
				"a(n) = my(m=factor(n)); [bigomega(primepi(p))+1 | p\u003c-m[,1]] * m[,2]; \\\\ _Kevin Ryde_, Oct 16 2020"
			],
			"xref": [
				"Cf. A049084, A020639, A001222."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Emeric Deutsch_, Sep 27 2011",
			"references": 3,
			"revision": 20,
			"time": "2020-10-16T02:11:03-04:00",
			"created": "2011-09-27T13:40:42-04:00"
		}
	]
}