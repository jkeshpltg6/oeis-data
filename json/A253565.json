{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A253565",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 253565,
			"data": "1,2,3,4,5,9,6,8,7,25,15,27,10,18,12,16,11,49,35,125,21,75,45,81,14,50,30,54,20,36,24,32,13,121,77,343,55,245,175,625,33,147,105,375,63,225,135,243,22,98,70,250,42,150,90,162,28,100,60,108,40,72,48,64,17,169,143,1331,91,847,539,2401,65,605,385,1715,275,1225,875,3125,39",
			"name": "Permutation of natural numbers: a(0) = 1, a(1) = 2; after which, a(2n) = A253550(a(n)), a(2n+1) = A253560(a(n)).",
			"comment": [
				"This sequence can be represented as a binary tree. Each child to the left is obtained by applying A253550 to the parent, and each child to the right is obtained by applying A253560 to the parent:",
				"                                    1",
				"                                    |",
				"                 ...................2...................",
				"                3                                       4",
				"      5......../ \\........9                   6......../ \\........8",
				"     / \\                 / \\                 / \\                 / \\",
				"    /   \\               /   \\               /   \\               /   \\",
				"   /     \\             /     \\             /     \\             /     \\",
				"  7       25         15       27         10       18         12       16",
				"11 49   35  125    21  75   45  81     14  50   30  54     20  36   24  32",
				"etc.",
				"Sequence A253563 is the mirror image of the same tree. Also in binary trees A005940 and A163511 the terms on level of the tree are some permutation of the terms present on the level n of this tree. A252464(n) gives the distance of n from 1 in all these trees. Of these four trees, this is the one where the left child is always smaller than the right child.",
				"Note that the indexing of sequence starts from 0, although its range starts from one."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A253565/b253565.txt\"\u003eTable of n, a(n) for n = 0..8191\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"a(0) = 1, a(1) = 2; after which, a(2n) = A253550(a(n)), a(2n+1) = A253560(a(n)).",
				"As a composition of related permutations:",
				"a(n) = A122111(A163511(n)).",
				"a(n) = A253563(A054429(n)).",
				"Other identities and observations. For all n \u003e= 0:",
				"a(2n+1) - a(2n) \u003e 0. [See the comment above.]"
			],
			"program": [
				"(Scheme, two versions)",
				"(definec (A253565 n) (cond ((\u003c n 2) (+ 1 n)) ((even? n) (A253550 (A253565 (/ n 2)))) (else (A253560 (A253565 (/ (- n 1) 2))))))",
				"(define (A253565 n) (A122111 (A163511 n)))"
			],
			"xref": [
				"Inverse: A253566.",
				"Cf. A252737 (row sums), A252738 (row products).",
				"Cf. A122111, A163511, A253550, A253560, A252464, A253563, A054429."
			],
			"keyword": "nonn,tabf,look",
			"offset": "0,2",
			"author": "_Antti Karttunen_, Jan 03 2015",
			"references": 13,
			"revision": 16,
			"time": "2015-01-23T22:52:24-05:00",
			"created": "2015-01-22T01:31:24-05:00"
		}
	]
}