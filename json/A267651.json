{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A267651",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 267651,
			"data": "6,6,30,6,30,30,54,6,102,30,78,30,78,54,150,6,102,102,126,30,270,78,150,30,150,78,318,54,174,150,198,6,390,102,270,102,222,126,390,30,246,270,270,78,510,150,294,30,390,150,510,78,318,318,390,54,630,174,366,150,366,198,918,6,390,390,414,102,750,270,438,102,438,222,750,126,702,390",
			"name": "Number of ways to write n^2 as a sum of three squares: a(n) = A005875(n^2).",
			"comment": [
				"This sequence is R_3(n), where R_D(n) is the number of vectors with integral components whose length is n in a D-dimensional vector space.",
				"It appears that all entries of this sequence are divisible by 6 and the quotient is odd.",
				"Indeed, for an integer n\u003e0, on the set of ordered integer triples (a,b,c) such that a^2 + b^2 + c^2 = n^2, consider the equivalence relation {|a|,|b|,|c|} = {|d|,|e|,|f|}. We can see that the cardinality of every equivalence class is a multiple of 6. Let the representatives be the integer triples of the form (a,b,c) with 0 \u003c= a \u003c= b \u003c= c. We cannot have a = b = c, else 3a^2 = n^2. If 0 = a = b \u003c c, the equivalence class has 6 triples. If 0 = a \u003c b = c, the class has 12 triples. If 0 = a \u003c b \u003c c, 0 \u003c a = b \u003c c, or 0 \u003c a \u003c b = c, the class has 24 triples. If 0 \u003c a \u003c b \u003c c, the class has 48 triples. - _Danny Rorabaugh_, Mar 17 2016",
				"Same as A016725 for n \u003e 0. - _Georg Fischer_, Oct 22 2018"
			],
			"link": [
				"Christopher Heiling, \u003ca href=\"/A267651/b267651.txt\"\u003eTable of n, a(n) for n = 1..150\u003c/a\u003e"
			],
			"formula": [
				"Sum_{k=1..n} a(k) = A267309(n).",
				"a(n) = A267309(n)-A267309(n-1) if n\u003e1 and a(1) = A267309(1) = 6."
			],
			"example": [
				"For n = 2 the a(n)= 6 integral solutions of x^2 + y^2 + z^2 = 2^2 are: {x,y,z} = {{0,0,2};{0,2,0};{2,0,0};{0,0,-2};{0,-2,0};{-2,0,0}}."
			],
			"mathematica": [
				"Table[SquaresR[3, n^2], {n, 80}] (* _Michael De Vlieger_, Jan 27 2016 *)"
			],
			"xref": [
				"Cf. A005875, A016725, A267309."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Christopher Heiling_, Jan 19 2016",
			"references": 2,
			"revision": 29,
			"time": "2018-10-22T10:34:00-04:00",
			"created": "2016-03-17T08:59:34-04:00"
		}
	]
}