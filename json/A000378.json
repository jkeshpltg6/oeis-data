{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000378",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 378,
			"data": "0,1,2,3,4,5,6,8,9,10,11,12,13,14,16,17,18,19,20,21,22,24,25,26,27,29,30,32,33,34,35,36,37,38,40,41,42,43,44,45,46,48,49,50,51,52,53,54,56,57,58,59,61,62,64,65,66,67,68,69,70,72,73,74,75,76,77,78,80,81,82,83",
			"name": "Sums of three squares: numbers of the form x^2 + y^2 + z^2.",
			"comment": [
				"An equivalent definition: numbers of the form x^2 + y^2 + z^2 with x,y,z \u003e= 0.",
				"Bourgain studies \"the spatial distribution of the representation of a large integer as a sum of three squares, on the small and critical scale as well as their electrostatic energy. The main results announced give strong evidence to the thesis that the solutions behave randomly. This is in sharp contrast to what happens with sums of two or four or more square.\" Sums of two nonzero squares are A000404. - _Jonathan Vos Post_, Apr 03 2012",
				"The multiplicities for a(n) (if 0 \u003c= x \u003c= y \u003c= z) are given as A000164(a(n)), n \u003e= 1. Compare with A005875(a(n)) for integer x, y and z, and order taken into account. - _Wolfdieter Lang_, Apr 08 2013",
				"a(n)^k is a member of this sequence for any k \u003e 1. - _Boris Putievskiy_, May 05 2013"
			],
			"reference": [
				"J. H. Conway and N. J. A. Sloane, \"Sphere Packings, Lattices and Groups\", Springer-Verlag, p. 107.",
				"E. Grosswald, Representations of Integers as Sums of Squares. Springer-Verlag, NY, 1985, p. 37.",
				"G. H. Hardy and E. M. Wright, An Introduction to the Theory of Numbers. 3rd ed., Oxford Univ. Press, 1954, p. 311."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A000378/b000378.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Jean Bourgain, Peter Sarnak and Zeév Rudnick, \u003ca href=\"http://arxiv.org/abs/1204.0134\"\u003eLocal statistics of lattice points on the sphere\u003c/a\u003e, arXiv:1204.0134 [math.NT], 2012-2015.",
				"J. W. Cogdell, \u003ca href=\"http://www.numdam.org/item?id=JTNB_2003__15_1_33_0\"\u003eOn sums of three squares\u003c/a\u003e, Journal de théorie des nombres de Bordeaux, 15 no. 1 (2003), p. 33-44.",
				"L. E. Dickson, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9904-1927-04312-9\"\u003eIntegers represented by positive ternary quadratic forms\u003c/a\u003e, Bull. Amer. Math. Soc. 33 (1927), 63-70. See Theorem I.",
				"Eric T. Mortenson, \u003ca href=\"https://arxiv.org/abs/1702.01627\"\u003eA Kronecker-type identity and the representations of a number as a sum of three squares\u003c/a\u003e, arXiv:1702.01627 [math.NT], 2017.",
				"P. Pollack, \u003ca href=\"http://www.math.dartmouth.edu/~ppollack/notes.pdf\"\u003eAnalytic and Combinatorial Number Theory\u003c/a\u003e Course Notes, p. 91. [?Broken link]",
				"P. Pollack, \u003ca href=\"http://alpha01.dm.unito.it/personalpages/cerruti/ac/notes.pdf\"\u003eAnalytic and Combinatorial Number Theory\u003c/a\u003e Course Notes, p. 91.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/SquareNumber.html\"\u003eSquare Number\u003c/a\u003e",
				"\u003ca href=\"/index/Su#ssq\"\u003eIndex entries for sequences related to sums of squares\u003c/a\u003e"
			],
			"formula": [
				"Legendre: a nonnegative integer is a sum of three squares iff it is not of the form 4^k m with m == 7 (mod 8).",
				"n^(2k+1) is in the sequence iff n is in the sequence. - _Ray Chandler_, Feb 03 2009",
				"Complement of A004215; complement of A000302(i)*A004771(j), i,j\u003e=0. - _Boris Putievskiy_, May 05 2013",
				"a(n) = 6n/5 + O(log n). - _Charles R Greathouse IV_, Mar 14 2014"
			],
			"example": [
				"a(1) = 0 = 0^2 + 0^2 + 0^2. A005875(0) = 1 = A000164(0).",
				"a(9) = 9 = 0^2 + 0^2 + 3^2 =  1^2 +  2^2 + 2^2. A000164(9) = 2. A000164(9) = 30 = 2*3 + 8*3 (counting signs and order). - _Wolfdieter Lang_, Apr 08 2013"
			],
			"maple": [
				"isA000378 := proc(n) # return true or false depending on n being in the list",
				"    local x,y ;",
				"    for x from 0 do",
				"        if 3*x^2 \u003e n then",
				"            return false;",
				"        end if;",
				"        for y from x do",
				"            if x^2+2*y^2 \u003e n then",
				"                break;",
				"            else",
				"                if issqr(n-x^2-y^2) then",
				"                    return true;",
				"                end if;",
				"            end if;",
				"        end do:",
				"    end do:",
				"end proc:",
				"A000378 := proc(n) # generate A000378(n)",
				"    option remember;",
				"    local a;",
				"    if n = 1 then",
				"        0;",
				"    else",
				"        for a from procname(n-1)+1 do",
				"            if isA000378(a) then",
				"                return a;",
				"            end if;",
				"        end do:",
				"    end if;",
				"end proc:",
				"seq(A000378(n),n=1..100) ; # _R. J. Mathar_, Sep 09 2015"
			],
			"mathematica": [
				"okQ[n_] := If[EvenQ[k = IntegerExponent[n, 2]], m = n/2^k; Mod[m, 8] != 7, True]; Select[Range[0, 100], okQ] (* _Jean-François Alcover_, Feb 08 2016, adapted from PARI *)"
			],
			"program": [
				"(PARI) isA000378(n)=my(k=valuation(n, 2)); if(k%2==0, n\u003e\u003e=k; n%8!=7, 1)",
				"(PARI) list(lim)=my(v=List(),k,t); for(x=0,sqrtint(lim\\=1), for(y=0, min(sqrtint(lim-x^2),x), k=x^2+y^2; for(z=0,min(sqrtint(lim-k), y), listput(v,k+z^2)))); Set(v) \\\\ _Charles R Greathouse IV_, Sep 14 2015",
				"(Python)",
				"def valuation(n, b):",
				"    v = 0",
				"    while n \u003e 1 and n%b == 0: n //= b; v += 1",
				"    return v",
				"def ok(n): return n//4**valuation(n, 4)%8 != 7",
				"print(list(filter(ok, range(84)))) # _Michael S. Branicky_, Jul 15 2021"
			],
			"xref": [
				"Union of A000290, A000404 and A000408 (common elements).",
				"Union of A000290, A000415 and A000419 (disjunct sets).",
				"Complement of A004215.",
				"Cf. A005875 (number of representations if x, y and z are integers).",
				"Cf. A047449, A034043, A034044, A034045, A034046, A034047, A065883, A072400, A072401, A071374, A002828, A001481, A125084, A000164."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Ray Chandler_, Sep 05 2004"
			],
			"references": 52,
			"revision": 100,
			"time": "2021-07-16T02:03:37-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}