{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A056941",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 56941,
			"data": "1,1,1,1,6,1,1,21,21,1,1,56,196,56,1,1,126,1176,1176,126,1,1,252,5292,14112,5292,252,1,1,462,19404,116424,116424,19404,462,1,1,792,60984,731808,1646568,731808,60984,792,1,1,1287,169884,3737448,16818516",
			"name": "Number of antichains (or order ideals) in the poset 5*m*n or plane partitions with not more than m rows, n columns and entries \u003c= 5.",
			"comment": [
				"Triangle of generalized binomial coefficients (n,k)_5; cf. A342889. - _N. J. A. Sloane_, Apr 03 2021"
			],
			"reference": [
				"Berman and Koehler, Cardinalities of finite distributive lattices, Mitteilungen aus dem Mathematischen Seminar Giessen, 121 (1976), p. 103-124",
				"P. A. MacMahon, Combinatory Analysis, Section 495, 1916.",
				"R. P. Stanley, Theory and application of plane partitions. II. Studies in Appl. Math. 50 (1971), p. 259-279. Thm. 18.1"
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A056941/b056941.txt\"\u003eRows n = 0..139 of triangle, flattened\u003c/a\u003e",
				"J. Berman and P. Koehler, \u003ca href=\"/A006356/a006356.pdf\"\u003eCardinalities of finite distributive lattices\u003c/a\u003e, Mitteilungen aus dem Mathematischen Seminar Giessen, 121 (1976), 103-124. [Annotated scanned copy]",
				"Johann Cigler, \u003ca href=\"https://arxiv.org/abs/2103.01652\"\u003ePascal triangle, Hoggatt matrices, and analogous constructions\u003c/a\u003e, arXiv:2103.01652 [math.CO], 2021.",
				"Johann Cigler, \u003ca href=\"https://www.researchgate.net/publication/349376205_Some_observations_about_Hoggatt_triangles\"\u003eSome observations about Hoggatt triangles\u003c/a\u003e, Universität Wien (Austria, 2021).",
				"P. A. MacMahon, \u003ca href=\"http://name.umdl.umich.edu/ABU9009.0001.001\"\u003eCombinatory analysis\u003c/a\u003e.",
				"\u003ca href=\"/index/Pos#posets\"\u003eIndex entries for sequences related to posets\u003c/a\u003e"
			],
			"formula": [
				"From _Peter Bala_, Oct 13 2011: (Start)",
				"Product_{k=0..4} C(n+m+k, m+k)/C(n+k, k) gives the array as a square.",
				"T(n-1,k-1)*T(n,k+1)*T(n+1,k) = T(n-1,k)*T(n,k-1)*T(n+1,k+1).",
				"Define f(r,n) = n!*(n+1)!*...*(n+r)!. The triangle whose (n,k)-th entry is f(r,0)*f(r,n)/(f(r,k)*f(r,n-k)) is A007318 (r = 0), A001263 (r = 1), A056939 (r = 2), A056940 (r = 3) and A056941 (r = 4). (End)",
				"From _Peter Bala_, May 10 2012: (Start)",
				"Determinants of 5 X 5 subarrays of Pascal's triangle A007318 (a matrix entry being set to 0 when not present).",
				"Also determinants of 5 X 5 arrays whose entries come from a single row:",
				"  det [C(n,k),C(n,k-1),C(n,k-2),C(n,k-3),C(n,k-4); C(n,k+1),C(n,k),C(n,k-1),C(n,k-2),C(n,k-3); C(n,k+2),C(n,k+1),C(n,k),C(n,k-1),C(n,k-2); C(n,k+3),C(n,k+2),C(n,k+1),C(n,k),C(n,k-1); C(n,k+4),C(n,k+3),C(n,k+2),C(n,k+1),C(n,k)]. (End)"
			],
			"example": [
				"The array starts:",
				"  [1    1      1        1          1           1            1 ...]",
				"  [1    6     21       56        126         252          462 ...]",
				"  [1   21    196     1176       5292       19404        60984 ...]",
				"  [1   56   1176    14112     116424      731808      3737448 ...]",
				"  [1  126   5292   116424    1646568    16818516    133613766 ...]",
				"  [1  252  19404   731808   16818516   267227532   3184461423 ...]",
				"  [1  462  60984  3737448  133613766  3184461423  55197331332 ...]",
				"  [...]",
				"Considered as a triangle, the initial rows are:",
				"  [1],",
				"  [1, 1],",
				"  [1, 6, 1],",
				"  [1, 21, 21, 1],",
				"  [1, 56, 196, 56, 1],",
				"  [1, 126, 1176, 1176, 126, 1],",
				"  [1, 252, 5292, 14112, 5292, 252, 1],",
				"  [1, 462, 19404, 116424, 116424, 19404, 462, 1],",
				"  [1, 792, 60984, 731808, 1646568, 731808, 60984, 792, 1],",
				"  ..."
			],
			"program": [
				"(PARI) A056941(n,m)=prod(k=0,4,binomial(n+m+k,m+k)/binomial(n+k,k) \\\\ _M. F. Hasler_, Sep 26 2018"
			],
			"xref": [
				"Cf. A000372, A056932, A001263, A056939, A056940, A142465, A142467, A142468.",
				"Antidiagonals sum to A005363 (Hoggatt sequence).",
				"Triangles of generalized binomial coefficients (n,k)_m (or generalized Pascal triangles) for m = 1,...,12: A007318 (Pascal), A001263, A056939, A056940, A056941, A142465, A142467, A142468, A174109, A342889, A342890, A342891."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "0,5",
			"author": "_Mitch Harris_",
			"ext": [
				"Edited by _M. F. Hasler_, Sep 26 2018"
			],
			"references": 16,
			"revision": 54,
			"time": "2021-05-15T01:24:18-04:00",
			"created": "2000-07-22T03:00:00-04:00"
		}
	]
}