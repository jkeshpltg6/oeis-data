{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A348829",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 348829,
			"data": "3,1,12,59,521,872492,415603,471263387,100453109125251,249063001217323,1206701295264057,2340564635396243082668,1836709980831869650909,7917057291763619291770993,6790679763108188972468718224386027,497252110757159525928442098399943",
			"name": "Numerator of relativistic sum w(2n) of the velocities 1/p^(2n) over all primes p, in units where the speed of light c = 1.",
			"comment": [
				"Generally, for a complex number s, w(s) = tanh(Sum_{p prime} artanh(1/p^s)), assuming that Re(s) \u003e 1.",
				"Theorem. If Re(s) \u003e 1, then w(s) = (1 - t(s))/(1 + t(s)) with t(s) = zeta(2s)/zeta(s)^2, where zeta(z) is the Riemann zeta function of z.",
				"Proof. Einstein's formula w = (u + v)/(1 + uv) can be expanded as (1-w)/(1+w) = ((1-u)/(1+u))((1-v)/(1+v))... for any number of velocities u, v, ... Hence, by the Euler product, Product_{p prime} (1-1/p^s)/(1+1/p^s) = zeta(2s)/zeta(s)^2, qed. Note that the function f(x) = (1-x)/(1+x) is an involution.",
				"If an integer s \u003e 0 is even, then w(s) is rational (related to the Bernoulli numbers B_{s} and B_{2s}).",
				"Conjecture: if an odd integer s \u003e 1, then w(s) is irrational.",
				"Note: Apery's constant zeta(3) = 1.202... is irrational."
			],
			"link": [
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Euler_product\"\u003eEuler product\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Riemann_zeta_function\"\u003eRiemann zeta function\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Velocity-addition_formula\"\u003eVelocity-addition formula\u003c/a\u003e."
			],
			"formula": [
				"a(n) = Numerator(tanh(Sum_{p prime} artanh(1/p^(2n))).",
				"a(n) = Numerator((zeta(2n)^2 - zeta(4n))/(zeta(2n)^2 + zeta(4n))).",
				"If Re(s) \u003e 1, then w(s) = f(f(w(s))) = (1-t(s))/(1+t(s)) and t(s) = f(f(t(s))) = (1-w(s))/(1+w(s)) = zeta(2s)/zeta(s)^2, where f(x) = (1-x)/(1+x). See my theorem and the note under my proof of this theorem. - _Thomas Ordowski_, Jan 03 2022"
			],
			"example": [
				"w(2) = 3/7, w(4) = 1/13, w(6) = 12/703, ..."
			],
			"mathematica": [
				"r[s_] := Zeta[2*s]/Zeta[s]^2; w[s_] := (1 - r[s])/(1 + r[s]); Table[Numerator[w[2*n]], {n, 1, 15}] (* _Amiram Eldar_, Nov 01 2021 *)"
			],
			"xref": [
				"The denominators are A348830.",
				"See also A348131 / A348132."
			],
			"keyword": "nonn,frac,changed",
			"offset": "1,1",
			"author": "_Thomas Ordowski_, Nov 01 2021",
			"ext": [
				"More terms from _Amiram Eldar_, Nov 01 2021"
			],
			"references": 1,
			"revision": 62,
			"time": "2022-01-04T05:15:21-05:00",
			"created": "2021-12-15T02:27:04-05:00"
		}
	]
}