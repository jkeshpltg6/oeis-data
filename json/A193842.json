{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A193842",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 193842,
			"data": "1,1,4,1,7,13,1,10,34,40,1,13,64,142,121,1,16,103,334,547,364,1,19,151,643,1549,2005,1093,1,22,208,1096,3478,6652,7108,3280,1,25,274,1720,6766,17086,27064,24604,9841,1,28,349,2542,11926,37384,78322,105796",
			"name": "Triangular array: the fission of the polynomial sequence ((x+1)^n: n \u003e= 0) by the polynomial sequence ((x+2)^n: n \u003e= 0). (Fission is defined at Comments.)",
			"comment": [
				"Suppose that p = p(n)*x^n + p(n-1)*x^(n-1) + ... + p(1)*x + p(0) is a polynomial and that Q is a sequence of polynomials:",
				"...",
				"q(k,x) = t(k,0)*x^k + t(k,1)*x^(k-1) + ... + t(k,k-1)*x + t(k,k),",
				"...",
				"for k = 0, 1, 2, ...  The Q-downstep of p is the polynomial given by",
				"...",
				"D(p) = p(n)*q(n-1,x) + p(n-1)*q(n-2,x) + ... + p(1)*q(0,x). (Note that p(0) does not appear. \"Q-downstep\" as just defined differs slightly from \"Q-downstep\" as defined for a different purpose at A193649.)",
				"...",
				"Now suppose that P = (p(n,x): n \u003e= 0) and Q = (q(n,x): n \u003e= 0) are sequences of polynomials, where n indicates degree.  The fission of P by Q, denoted by P^^Q, is introduced here as the sequence W = (w(n,x): n \u003e= 0) of polynomials defined by w(0,x) = 1 and w(n,x) = D(p(n+1,x)).",
				"...",
				"Strictly speaking, ^^ is an operation on sequences of polynomials.  However, if P and Q are regarded as numerical triangles (of coefficients of polynomials), then ^^ can be regarded as an operation on numerical triangles.  In this case, row n of P^^Q, for n \u003e 0, is given by the matrix product P(n+1)*QQ(n), where P(n+1) =(p(n+1,n+1), p(n+1,n), ..., p(n+1,2), p(n+1,1)) and QQ(n) is the (n+1)-by-(n+1) matrix given by",
				"...",
				"q(n,0) .. q(n,1)............. q(n,n-1) .... q(n,n)",
				"0 ....... q(n-1,0)........... q(n-1,n-2)... q(n-1,n-1)",
				"0 ....... 0.................. q(n-2,n-3) .. q(n-2,n-2)",
				"...",
				"0 ....... 0.................. q(1,0) ...... q(1,1)",
				"0 ....... 0 ................. 0 ........... q(0,0).",
				"Here, the polynomial q(k,x) is taken to be",
				"q(k,0)*x^k + q(k,1)x^(k-1) + ... + q(k,k)*x + q(k,k);",
				"i.e., \"q\" is used instead of \"t\".",
				"...",
				"Example:  Let p(n,x) = (x+1)^n and q(n,x) = (x+2)^n.  Then",
				"...",
				"w(0,x) = 1 by the definition of W,",
				"w(1,x) = D(p(2,x)) = 1*(x+2) + 2*1 = x + 4,",
				"w(2,x) = D(p(3,x)) = 1*(x^2+4*x+4) + 3*(x+2) + 3*1 = x^2 + 7*x + 13,",
				"w(3,x) = D(p(4,x)) = 1*(x^3+6*x^2+12*x+8) + 4*(x^2+4x+4) + 6*(x+2) + 4*1 = x^3 + 10*x^2 + 34*x + 40.",
				"...",
				"From these first 4 polynomials in the sequence P^^Q, we can write the first 4 rows of P^^Q when P, Q, and P^^Q are regarded as triangles:",
				"1",
				"1...4",
				"1...7....13",
				"1...10...34...40",
				"...",
				"In the following examples, r(P^^Q) is the mirror of P^^Q, obtained by reversing the rows of P^^Q.  Let u denote the polynomial x^n + x^(n-1) + ... + x + 1.",
				"...",
				"..P........Q...........P^^Q........r(P^^Q)",
				"(x+1)^n....(x+2)^n.....A193842.....A193843",
				"(x+1)^n....(x+1)^n.....A193844.....A193845",
				"(x+2)^n....(x+1)^n.....A193846.....A193847",
				"(2x+1)^n...(x+1)^n.....A193856.....A193857",
				"(x+1)^n....(2x+1)^n....A193858.....A193859",
				"(x+1)^n.......u........A054143.....A104709",
				"..u........(x+1)^n.....A074909.....A074909",
				"..u...........u........A002260.....A004736",
				"(x+2)^n.......u........A193850.....A193851",
				"..u.........(x+2)^n....A193844.....A193845",
				"(2x+1)^n......u........A193860.....A193861",
				"..u.........(2x+1)^n...A115068.....A193862",
				"...",
				"Regarding A193842,",
				"col 1 ...... A000012",
				"col 2 ...... A016777",
				"col 3 ...... A081271",
				"w(n,n) ..... A003462",
				"w(n,n-1) ... A014915"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A193842/b193842.txt\"\u003eRows n = 0..100 of triangle, flattened\u003c/a\u003e",
				"Digital Library of Mathematical Functions, \u003ca href=\"http://dlmf.nist.gov/15.2#ii\"\u003eHypergeometric function, analytic properties\u003c/a\u003e.",
				"Clark Kimberling, \u003ca href=\"https://www.fq.math.ca/Papers1/52-3/Kimberling11132013.pdf\"\u003eFusion, Fission, and Factors\u003c/a\u003e, Fib. Q., 52(3) (2014), 195-202."
			],
			"formula": [
				"From _Peter Bala_, Jul 16 2013: (Start)",
				"T(n,k) = Sum_{i = 0..k} 3^(k-i)*binomial(n-i,k-i).",
				"O.g.f.: 1/((1 - x*t)*(1 - (1 + 3*x)*t)) = 1 + (1 + 4*x)*t + (1 + 7*x + 13*x^2)*t^2 + ....",
				"The n-th row polynomial is R(n,x) = (1/(2*x + 1))*((3*x + 1)^(n+1) - x^(n+1)). (End)",
				"T(n,k) = T(n-1,k) + 4*T(n-1,k-1) - T(n-2,k-1) - 3*T(n-2,k-2), T(0,0) = 1, T(1,0) = 1, T(1,1) = 4, T(n,k) = 0 if k \u003c 0 or if k \u003e n. - _Philippe Deléham_, Jan 17 2014",
				"T(n,k) = 3^k * C(n,k) * hyp2F1(1, -k, -n, 1/3) with or without the additional term -0^(n-k)/2 depending on the exact definition of the hypergeometric function used. Compare formulas 15.2.5 and 15.2.6 in the DLMF reference. - _Peter Luschny_, Jul 23 2014"
			],
			"example": [
				"First six rows, for 0 \u003c= k \u003c= n and 0 \u003c= n \u003c= 5:",
				"  1",
				"  1...4",
				"  1...7....13",
				"  1...10...34....40",
				"  1...13...64....142...121",
				"  1...16...103...334...547...364"
			],
			"maple": [
				"fission := proc(p, q, n) local d, k;",
				"p(n+1,0)*q(n,x)+add(coeff(p(n+1,x),x^k)*q(n-k,x), k=1..n);",
				"seq(coeff(%,x,n-k), k=0..n) end:",
				"A193842_row := n -\u003e fission((n,x) -\u003e (x+1)^n, (n,x) -\u003e (x+2)^n, n);",
				"for n from 0 to 5 do A193842_row(n) od; # _Peter Luschny_, Jul 23 2014",
				"# Alternatively:",
				"p := (n,x) -\u003e add(x^k*(1+3*x)^(n-k),k=0..n): for n from 0 to 7 do [n], PolynomialTools:-CoefficientList(p(n,x), x) od; # _Peter Luschny_, Jun 18 2017"
			],
			"mathematica": [
				"(* First program *)",
				"z = 10;",
				"p[n_, x_] := (x + 1)^n;",
				"q[n_, x_] := (x + 2)^n",
				"p1[n_, k_] := Coefficient[p[n, x], x^k];",
				"p1[n_, 0] := p[n, x] /. x -\u003e 0;",
				"d[n_, x_] := Sum[p1[n, k]*q[n - 1 - k, x], {k, 0, n - 1}]",
				"h[n_] := CoefficientList[d[n, x], {x}]",
				"TableForm[Table[Reverse[h[n]], {n, 0, z}]]",
				"Flatten[Table[Reverse[h[n]], {n, -1, z}]]  (* A193842 *)",
				"TableForm[Table[h[n], {n, 0, z}]]  (* A193843 *)",
				"Flatten[Table[h[n], {n, -1, z}]]",
				"(* Second program *)",
				"Table[SeriesCoefficient[((x+3)^(n+1) -1)/(x+2), {x,0,n-k}], {n,0,10}, {k,0,n}]//Flatten (* _G. C. Greubel_, Feb 18 2020 *)"
			],
			"program": [
				"(Sage)",
				"from mpmath import mp, hyp2f1",
				"mp.dps = 100; mp.pretty = True",
				"def T(n,k):",
				"    return 3^k*binomial(n,k)*hyp2f1(1,-k,-n,1/3)-0^(n-k)//2",
				"for n in range(7):",
				"    print([int(T(n,k)) for k in (0..n)]) # _Peter Luschny_, Jul 23 2014",
				"(Sage) # Second program using the 'fission' operation.",
				"def fission(p, q, n):",
				"    F = p(n+1,0)*q(n,x)+add(expand(p(n+1,x)).coefficient(x,k)*q(n-k,x) for k in (1..n))",
				"    return [expand(F).coefficient(x,n-k) for k in (0..n)]",
				"A193842_row = lambda k: fission(lambda n,x: (x+1)^n, lambda n,x: (x+2)^n, k)",
				"for n in range(7): A193842_row(n) # _Peter Luschny_, Jul 23 2014",
				"(PARI) T(n,k) = sum(j=0,k, 3^(k-j)*binomial(n-j,k-j)); \\\\ _G. C. Greubel_, Feb 18 2020",
				"(MAGMA) [ (\u0026+[3^(k-j)*Binomial(n-j,k-j): j in [0..k]]): k in [0..n], n in [0..10]]; // _G. C. Greubel_, Feb 18 2020"
			],
			"xref": [
				"Cf. A193722 (fusion of P by Q), A193649 (Q-residue), A193843 (mirror of A193842)."
			],
			"keyword": "nonn,tabl",
			"offset": "0,3",
			"author": "_Clark Kimberling_, Aug 07 2011",
			"ext": [
				"Name and Comments edited by _Petros Hadjicostas_, Jun 05 2020"
			],
			"references": 27,
			"revision": 71,
			"time": "2020-06-08T02:36:19-04:00",
			"created": "2011-08-07T16:13:25-04:00"
		}
	]
}