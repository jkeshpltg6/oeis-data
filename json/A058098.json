{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A058098",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 58098,
			"data": "1,0,6,-8,17,-32,54,-80,116,-192,290,-408,585,-832,1192,-1648,2237,-3072,4156,-5576,7414,-9824,12964,-16896,22002,-28544,36794,-47184,60185,-76736,97388,-122864,154615,-194048,242904,-302800,376271,-466720,577176,-711840",
			"name": "McKay-Thompson series of class 10B for the Monster group with a(0) = 0.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A058098/b058098.txt\"\u003eTable of n, a(n) for n = -1..5000\u003c/a\u003e (terms -1..997 from G. A. Edgar)",
				"D. Ford, J. McKay and S. P. Norton, \u003ca href=\"https://dx.doi.org/10.1080/00927879408825127\"\u003eMore on replicable functions\u003c/a\u003e, Commun. Algebra 22, No. 13, 5175-5193 (1994).",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e",
				"\u003ca href=\"/index/Mat#McKay_Thompson\"\u003eIndex entries for McKay-Thompson series for Monster simple group\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^-1 * (chi(-q) * chi(-q^5))^4 + 4 in powers of q where chi() is a Ramanujan theta function.",
				"Expansion of (eta(q) * eta(q^5) / (eta(q^2) * eta(q^10)))^4 + 4 in powers of q.",
				"G.f.: (Product_{k\u003e0} (1 + x^k) * (1 + x^(5*k)))^-4 + 4.",
				"a(n) = A132040(n) unless n = 0. a(n) = -(-1)^n * A112158(n). - _Michael Somos_, Feb 02 2012",
				"a(n) ~ -(-1)^n * exp(2*Pi*sqrt(n/5)) / (2 * 5^(1/4) * n^(3/4)). - _Vaclav Kotesovec_, Sep 08 2017"
			],
			"example": [
				"T10B = 1/q + 6*q - 8*q^2 + 17*q^3 - 32*q^4 + 54*q^5 - 80*q^6 + ..."
			],
			"mathematica": [
				"QP = QPochhammer; s = (QP[q]*(QP[q^5]/QP[q^2]/QP[q^10]))^4 + 4*q + O[q]^40; CoefficientList[s, q] (* _Jean-François Alcover_, Nov 13 2015, adapted from PARI *)"
			],
			"program": [
				"(PARI) {a(n) = local(A); if( n\u003c-1, 0, n++; A = x * O(x^n); polcoeff( (eta(x + A) * eta(x^5 + A) / eta(x^2 + A) / eta(x^10 + A))^4 + 4 * x, n))} /* _Michael Somos_, Feb 02 2012 */"
			],
			"xref": [
				"Cf. A000521, A007240, A007241, A007267, A014708, A045478.",
				"Cf. A112158, A132040."
			],
			"keyword": "sign",
			"offset": "-1,3",
			"author": "_N. J. A. Sloane_, Nov 27 2000",
			"references": 3,
			"revision": 36,
			"time": "2021-03-12T22:24:42-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}