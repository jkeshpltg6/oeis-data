{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A323296",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 323296,
			"data": "1,0,0,1,11,10,25,406,4823,15436,72915,895180,11320441,71777498,519354927,6155284240,82292879425,788821735656,7772567489083,98329764933354,1400924444610675,17424772471470490,216091776292721021,3035845122991962688,46700545575567202903",
			"name": "Number of 3-uniform hypergraphs spanning n labeled vertices where no two edges have exactly one vertex in common.",
			"comment": [
				"The only way to meet the requirements is to cover the vertices with zero or more disconnected 3-uniform hypergraphs with each edge having exactly two vertices in common (A323294). - _Andrew Howroyd_, Aug 18 2019"
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A323296/b323296.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e"
			],
			"formula": [
				"From _Andrew Howroyd_, Aug 18 2019: (Start)",
				"Exponential transform of A323294.",
				"E.g.f.: exp(-x^2/2 - x^3/3 + 5*x^4/24 + x^2*exp(x)/2). (End)"
			],
			"example": [
				"The a(4) = 11:",
				"  {{1,2,3},{1,2,4}}",
				"  {{1,2,3},{1,3,4}}",
				"  {{1,2,3},{2,3,4}}",
				"  {{1,2,4},{1,3,4}}",
				"  {{1,2,4},{2,3,4}}",
				"  {{1,3,4},{2,3,4}}",
				"  {{1,2,3},{1,2,4},{1,3,4}}",
				"  {{1,2,3},{1,2,4},{2,3,4}}",
				"  {{1,2,3},{1,3,4},{2,3,4}}",
				"  {{1,2,4},{1,3,4},{2,3,4}}",
				"  {{1,2,3},{1,2,4},{1,3,4},{2,3,4}}",
				"The following are non-isomorphic representatives of the 3 unlabeled 3-uniform hypergraphs spanning 7 vertices with no two edges having exactly one vertex in common, and their multiplicities in the labeled case, which add up to a(7) = 406.",
				"  210 X {{1,2,3},{4,6,7},{5,6,7}}",
				"  140 X {{1,2,3},{4,5,7},{4,6,7},{5,6,7}}",
				"   21 X {{1,6,7},{2,6,7},{3,6,7},{4,6,7},{5,6,7}}",
				"   35 X {{1,2,3},{4,5,6},{4,5,7},{4,6,7},{5,6,7}}"
			],
			"maple": [
				"b:= n-\u003e `if`(n\u003c5, (n-2)*(2*n^2-6*n+3)/6, n/2)*(n-1):",
				"a:= proc(n) option remember; `if`(n=0, 1, add(",
				"      binomial(n-1, k-1)*b(k)*a(n-k), k=1..n))",
				"    end:",
				"seq(a(n), n=0..25);  # _Alois P. Heinz_, Aug 18 2019"
			],
			"mathematica": [
				"stableSets[u_,Q_]:=If[Length[u]===0,{{}},With[{w=First[u]},Join[stableSets[DeleteCases[u,w],Q],Prepend[#,w]\u0026/@stableSets[DeleteCases[u,r_/;r===w||Q[r,w]||Q[w,r]],Q]]]];",
				"Table[Length[Select[stableSets[Subsets[Range[n],{3}],Length[Intersection[#1,#2]]==1\u0026],Union@@#==Range[n]\u0026]],{n,8}]"
			],
			"program": [
				"(PARI) seq(n)={Vec(serlaplace(exp(-x^2/2 - x^3/3 + 5*x^4/24 + x^2*exp(x + O(x^(n-1)))/2)))} \\\\ _Andrew Howroyd_, Aug 18 2019"
			],
			"xref": [
				"Cf. A025035, A125791, A190865, A289837, A299471, A302374, A302394, A322451, A323293, A323294, A323297."
			],
			"keyword": "nonn",
			"offset": "0,5",
			"author": "_Gus Wiseman_, Jan 11 2019",
			"ext": [
				"a(11) from _Alois P. Heinz_, Aug 12 2019",
				"Terms a(12) and beyond from _Andrew Howroyd_, Aug 18 2019"
			],
			"references": 5,
			"revision": 15,
			"time": "2019-08-18T12:50:21-04:00",
			"created": "2019-01-12T08:05:05-05:00"
		}
	]
}