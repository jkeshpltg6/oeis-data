{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A273580",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 273580,
			"data": "2,5,2,9,5,4,3,3,2,6,2,2,0,3,9,8,4,3,0,3,1,0,3,7,9,1,2,8,8,5,9,7,5,3,3,3,5,1,9,3,5,3,7,1,2,4,4,5,9,3,8,3,4,1,7,8,6,5,7,1,8,7,1,1,3,9,6,7,3,0,9,4,6,5,4,0,4,8,7,4,8,2,5,3,1,0,3,3,5,4,4,6,0,7,2,1,5,0,0,2,3,8,9,3,3",
			"name": "Decimal expansion of the infinite nested radical sqrt(F_0 + sqrt(F_1 + sqrt(F_3 + ...))), where F_k are the Fermat numbers A000215.",
			"comment": [
				"The convergence of this expression follows from Vijayaraghavan's theorem, for which it represents an extreme example.",
				"Two PARI programs to compute this constant are listed below. The first one is a brute-force implementation of the definition and allows the computation of only 13 digits before exceeding current PARI capabilities. The second one implements the following 'trick' inspired by a comment in A094885: Let us try to compute first x = a/sqrt(2). We have x = (1/sqrt(2))sqrt(3+ sqrt(5+ sqrt(17+ ... ))) = sqrt(3/2+ (1/2)sqrt(5+ sqrt(17+ ... ))) = sqrt(3/2+ sqrt(5/4+ (1/4)sqrt(17+ ... ))) = sqrt(3/2+ sqrt(5/4+ sqrt(17/16+ ... ))) = sqrt(c_0+sqrt(c_1+sqrt(c_3+...))), where c_n = (2^(2^n)+1)/2^(2^n) = 1+d_n, with d_n = 2^(-2^n). This nested radical is easy to manage to any precision. However, evaluating it up to N terms, its convergence with increasing N is no better than that of the original algorithm. To speed it up, one must notice that, since the c_n converge rapidly to 1, and since the nested radical sqrt(1+sqrt(1+...)) evaluates to the golden ratio phi (A001622), the latter is the natural best stand-in for the neglected part (terms from N+1 to infinity). With this modification, i.e., 'seeding' the iterations with phi instead of 0, the convergence becomes extremely fast (the number of valid digits more than doubles upon incrementing N by 1)."
			],
			"link": [
				"Stanislav Sykora, \u003ca href=\"/A273580/b273580.txt\"\u003eTable of n, a(n) for n = 1..2000\u003c/a\u003e"
			],
			"formula": [
				"Equals sqrt(2)*sqrt(1+1/2+sqrt(1+1/4+sqrt(1+1/16+sqrt(1+1/256+ ... ))))."
			],
			"example": [
				"2.5295433262203984303103791288597533351935371244593834178657187113967..."
			],
			"program": [
				"(PARI) /* This function crashes PARI beyond N=28: */",
				"s(N)={my(r=0.0);for(k=1,N,r=sqrt(2^(2.0^(N-k))+1+r));return(r)}",
				"/* N is the number of terms to include in the evaluation. It turns out that the starting digits s(28) shares with s(27) are only 13 */",
				"(PARI) /* This alternative can easily generate millions of digits: */",
				"d=vector(30);d[1]=0.5;for(n=2,#d,d[n]=d[n-1]^2);",
				"S(N)={my(r=(1+sqrt(5))/2);for(k=1,N,r=sqrt(1+d[N-k+1]+r));return(r*sqrt(2))}",
				"/* S(12) exceeds 1200 stable digits, S(20) goes over 150000. For the b-file, the first 2000 digits of S(13) were used, computed with the realprecision of 2100 digits */"
			],
			"xref": [
				"Cf. A000215, A001622, A094885."
			],
			"keyword": "nonn,cons",
			"offset": "1,1",
			"author": "_Stanislav Sykora_, May 25 2016",
			"references": 1,
			"revision": 20,
			"time": "2017-03-19T19:33:52-04:00",
			"created": "2016-06-23T00:20:11-04:00"
		}
	]
}