{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A128129",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 128129,
			"data": "1,2,4,7,12,20,32,50,76,114,168,244,350,496,696,967,1332,1820,2468,3324,4448,5916,7824,10292,13471,17548,22756,29384,37788,48408,61784,78578,99600,125838,158496,199036,249230,311224,387608,481506,596676",
			"name": "Expansion of (chi(-q^3)/ chi^3(-q) -1)/3 in powers of q where chi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) := Prod_{k\u003e=1} (1-(-q)^k) (see A121373), phi(q) := theta_3(q) := Sum_{k=-oo..oo} q^(k^2) (A000122), psi(q) := Sum_{k=0..oo} q^(k*(k+1)/2) (A010054), chi(q) := Prod_{k\u003e=0} (1+q^(2k+1)) (A000700)."
			],
			"link": [
				"Vaclav Kotesovec, \u003ca href=\"/A128129/b128129.txt\"\u003eTable of n, a(n) for n = 1..10001\u003c/a\u003e",
				"Kevin Acres, David Broadhurst, \u003ca href=\"https://arxiv.org/abs/1810.07478\"\u003eEta quotients and Rademacher sums\u003c/a\u003e, arXiv:1810.07478 [math.NT], 2018. See Table 1 p. 10.",
				"Andrew Sills, \u003ca href=\"http://home.dimacs.rutgers.edu/~asills/EMDC/SillsEMDC-Rev.pdf\"\u003eTowards an Automation of the Circle Method\u003c/a\u003e, Gems in Experimental Mathematics in Contemporary Mathematics, 2010, formula S76.",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of (eta(q^2)^3* eta(q^3)/ (eta(q)^3* eta(q^6)) -1)/3 in powers of q.",
				"Euler transform of period 18 sequence [ 2, 1, 1, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 1, 1, 2, 0, ...].",
				"G.f. A(x) satisfies 0=f(A(x), A(x^2)) where f(u, v)= u^2 -v -2*v^2 -4*u*v -6*u*v^2.",
				"G.f. A(x) satisfies 0=f(A(x), A(x^3)) where f(u, v)= u^3 -v* (1+3*v+3*v^2)* (1+6*u+12*u^2).",
				"a(n) ~ exp(2*sqrt(2*n)*Pi/3) / (2^(7/4) * 3^(3/2) * n^(3/4)). - _Vaclav Kotesovec_, Jan 12 2017"
			],
			"mathematica": [
				"nmax = 50; CoefficientList[Series[Product[(1 - x^(18*k))*(1 - x^(18*k - 3))*(1 - x^(18*k - 15))/((1 - x^(2*k - 1))*(1 - x^k)), {k, 1, nmax}], {x, 0, nmax}], x] (* _Vaclav Kotesovec_, Jan 11 2017 *)"
			],
			"program": [
				"(PARI) {a(n)=local(A); if(n\u003c1, 0, n--; A=x*O(x^n); polcoeff( eta(x^2+A)* eta(x^3+A)* eta(x^18+A)^2/ (eta(x^6+A)* eta(x^9+A)* eta(x+A)^2), n))}"
			],
			"xref": [
				"A128128(n)=3*a(n) if n\u003e0."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Michael Somos_, Feb 15 2007",
			"references": 8,
			"revision": 15,
			"time": "2021-03-12T22:24:44-05:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}