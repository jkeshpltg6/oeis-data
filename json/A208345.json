{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A208345",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 208345,
			"data": "1,0,3,0,1,7,0,1,3,17,0,1,3,10,41,0,1,3,11,30,99,0,1,3,12,35,87,239,0,1,3,13,40,108,245,577,0,1,3,14,45,130,322,676,1393,0,1,3,15,50,153,406,938,1836,3363,0,1,3,16,55,177,497,1236,2682,4925,8119,0,1",
			"name": "Triangle of coefficients of polynomials v(n,x) jointly generated with A208344; see the Formula section.",
			"comment": [
				"row sums, u(n,1):  (1,2,5,13,...), odd-indexed Fibonacci numbers",
				"row sums, v(n,1):  (1,3,8,21,...), even-indexed Fibonacci numbers",
				"As triangle T(n,k) with 0\u003c=k\u003c=n, it is (0, 1/3, 2/3, 0, 0, 0, 0, 0, 0, 0, ...) DELTA (3, -2/3, -1/3, 0, 0, 0, 0, 0, 0, 0, ...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, Feb 26 2012"
			],
			"formula": [
				"u(n,x)=u(n-1,x)+x*v(n-1,x),",
				"v(n,x)=x*u(n-1,x)+2x*v(n-1,x),",
				"where u(1,x)=1, v(1,x)=1.",
				"(Start)- As triangle T(n,k), 0\u003c=k\u003c=n :",
				"T(n,k) = T(n-1,k) + 2*T(n-1,k-1) + T(n-2,k-2) - 2*T(n-2,k-1) with T(0,0) = 1, T(1,0) = 0, T(1,1) = 3, T(n,k) = 0 if k\u003c0 or if k\u003en.",
				"G.f.: (1+(y-1)*x)/(1-(1+2*y)*x+y*(2-y)*x^2).",
				"Sum_{k, 0\u003c=k\u003c=n} T(n,k)*x^k = A152167(n), A000007(n), A001906(n+1), A003948(n) for x = -1, 0, 1, 2 respectively.",
				"Sum_{k, 0\u003c=k\u003c=n} T(n,k)*x^(n-k) = A078057(n), A001906(n+1), A000244(n), A081567(n), A083878(n), A165310(n) for x = 0, 1, 2, 3, 4, 5 respectively. (END) - _Philippe Deléham_, Feb 26 2012"
			],
			"example": [
				"First five rows:",
				"1",
				"0...3",
				"0...1...7",
				"0...1...3...17",
				"0...1...3...10...41",
				"First five polynomials u(n,x):",
				"1, 3x, x + 7x^2, x + 3x^2 + 17x^3, x + 3x^2 + 10x^3 +",
				"41x^4."
			],
			"mathematica": [
				"u[1, x_] := 1; v[1, x_] := 1; z = 13;",
				"u[n_, x_] := u[n - 1, x] + x*v[n - 1, x];",
				"v[n_, x_] := x*u[n - 1, x] + 2 x*v[n - 1, x];",
				"Table[Expand[u[n, x]], {n, 1, z/2}]",
				"Table[Expand[v[n, x]], {n, 1, z/2}]",
				"cu = Table[CoefficientList[u[n, x], x], {n, 1, z}];",
				"TableForm[cu]",
				"Flatten[%]    (* A208344 *)",
				"Table[Expand[v[n, x]], {n, 1, z}]",
				"cv = Table[CoefficientList[v[n, x], x], {n, 1, z}];",
				"TableForm[cv]",
				"Flatten[%]    (* A208345 *)",
				"Table[u[n, x] /. x -\u003e 1, {n, 1, z}]",
				"Table[v[n, x] /. x -\u003e 1, {n, 1, z}]"
			],
			"xref": [
				"Cf. A208344.",
				"Cf. A084938, A000045, A000244, A001906"
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Clark Kimberling_, Feb 25 2012",
			"references": 5,
			"revision": 10,
			"time": "2013-09-08T19:59:30-04:00",
			"created": "2012-02-25T18:36:02-05:00"
		}
	]
}