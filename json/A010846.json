{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A010846",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 10846,
			"data": "1,2,2,3,2,5,2,4,3,6,2,8,2,6,5,5,2,10,2,8,5,7,2,11,3,7,4,8,2,18,2,6,6,8,5,14,2,8,6,11,2,19,2,9,8,8,2,15,3,12,6,9,2,16,5,11,6,8,2,26,2,8,8,7,5,22,2,10,6,20,2,18,2,9,9,10,5,23,2,14,5,9,2,28,5,9,7,11,2,32,5,10",
			"name": "Number of numbers \u003c= n whose set of prime factors is a subset of the set of prime factors of n.",
			"comment": [
				"This function of n appears in an ABC-conjecture by Andrew Granville. See Goldfeld. - _T. D. Noe_, Jun 30 2009"
			],
			"link": [
				"T. D. Noe and Michael De Vlieger, \u003ca href=\"/A010846/b010846.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (first 5000 terms from T. D. Noe)",
				"Dorian Goldfled, \u003ca href=\"http://www.math.columbia.edu/~goldfeld/ABC-Conjecture.pdf\"\u003eModular forms, elliptic curves, and the ABC conjecture\u003c/a\u003e"
			],
			"formula": [
				"a(n) = |{k\u003c=n, k|n^(tau(k)-1)}|. - _Vladeta Jovovic_, Sep 13 2006",
				"a(n) = Sum_{j = 1..n} Product_{primes p | j} delta(n mod p,0) where delta is the Kronecker delta. - _Robert Israel_, Feb 09 2015",
				"a(n) = Sum_{1\u003c=k\u003c=n,(n,k)=1} mu(k)*floor(n/k). - _Benoit Cloitre_, May 07 2016",
				"a(n) = Sum_{k=1..n} floor(n^k/k)-floor((n^k -1)/k). - _Anthony Browne_, May 28 2016"
			],
			"example": [
				"From _Wolfdieter Lang_, Jun 30 2014: (Start)",
				"a(1) = 1 because the empty set is a subset of any set.",
				"a(6) = 5 from the five numbers: 1 with the empty set, 2 with the set {2}, 3 with {3}, 4 with {2} and 6 with {2,3}, which are all subsets of {2,3}. 5 is out because {5} is not a subset of {2,3}. (End)",
				"From _David A. Corneth_, Feb 10 2015: (Start)",
				"Let p# be the product of primes up to p, A002110. Then,",
				"a(13#) = 1161",
				"a(17#) = 4843",
				"a(19#) = 19985",
				"a(23#) = 83074",
				"a(29#) = 349670",
				"a(31#) = 1456458",
				"a(37#) = 6107257",
				"a(41#) = 25547835",
				"(End)"
			],
			"maple": [
				"A:= proc(n) local F, S, s,j,p;",
				"  F:= numtheory:-factorset(n);",
				"  S:= {1};",
				"  for p in F do",
				"    S:= {seq(seq(s*p^j, j=0..floor(log[p](n/s))),s=S)}",
				"  od;",
				"  nops(S)",
				"end proc;",
				"seq(A(n),n=1..1000); # _Robert Israel_, Jun 27 2014"
			],
			"mathematica": [
				"pf[n_] := If[n==1, {}, Transpose[FactorInteger[n]][[1]]]; SubsetQ[lst1_, lst2_] := Intersection[lst1,lst2]==lst1; Table[pfn=pf[n]; Length[Select[Range[n], SubsetQ[pf[ # ],pfn] \u0026]], {n,100}] (* _T. D. Noe_, Jun 30 2009 *)",
				"Table[Total[MoebiusMu[#] Floor[n/#] \u0026@ Select[Range@ n, CoprimeQ[#, n] \u0026]], {n, 92}] (* _Michael De Vlieger_, May 08 2016 *)"
			],
			"program": [
				"(PARI) a(n,f=factor(n)[,1])=if(#f\u003e1,my(v=f[1..#f-1],p=f[#f],s); while(n\u003e0, s+=a(n,v); n\\=p); s, if(#f\u0026\u0026n\u003e0,log(n+.5)\\log(f[1])+1,n\u003e0)) \\\\ _Charles R Greathouse IV_, Jun 27 2013",
				"(PARI) a(n) = sum(k=1,n,if(gcd(n,k)-1,0,moebius(k)*(n\\k))) \\\\ _Benoit Cloitre_, May 07 2016",
				"(PARI) a(n,f=factor(n)[,1])=if(#f\u003c2, return(if(#f, valuation(n,f[1])+1, 0))); my(v=f[1..#f-1],p=f[#f],s); while(n, s+=a(n,v); n\\=p); s \\\\ _Charles R Greathouse IV_, Nov 03 2021"
			],
			"xref": [
				"Cf. A162306 (numbers for each n)."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Olivier Gérard_",
			"ext": [
				"Definition made more precise at the suggestion of _Wolfdieter Lang_"
			],
			"references": 36,
			"revision": 66,
			"time": "2021-11-03T02:23:13-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}