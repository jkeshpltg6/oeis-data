{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005420",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5420,
			"id": "M2609",
			"data": "3,7,5,31,7,127,17,73,31,89,13,8191,127,151,257,131071,73,524287,41,337,683,178481,241,1801,8191,262657,127,2089,331,2147483647,65537,599479,131071,122921,109,616318177,524287,121369,61681,164511353,5419",
			"name": "Largest prime factor of 2^n - 1.",
			"reference": [
				"J. Brillhart et al., Factorizations of b^n +- 1. Contemporary Mathematics, Vol. 22, Amer. Math. Soc., Providence, RI, 2nd edition, 1985; and later supplements.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, Charles R Greathouse IV and Amiram Eldar, \u003ca href=\"/A005420/b005420.txt\"\u003eTable of n, a(n) for n = 2..1206\u003c/a\u003e (terms up to 500 from T. D. Noe, terms 501..1000 from Charles R Greathouse IV, terms 1001..1206 from Amiram Eldar)",
				"J. Brillhart et al., \u003ca href=\"http://dx.doi.org/10.1090/conm/022\"\u003eFactorizations of b^n +- 1\u003c/a\u003e, Contemporary Mathematics, Vol. 22, Amer. Math. Soc., Providence, RI, 3rd edition, 2002.",
				"R. K. Guy, \u003ca href=\"/A002968/a002968_1.pdf\"\u003eLetter to G. B. Huff \u0026 N. J. A. Sloane, Aug 1974\u003c/a\u003e",
				"S. S. Wagstaff, Jr., \u003ca href=\"http://www.cerias.purdue.edu/homes/ssw/cun/index.html\"\u003eThe Cunningham Project\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/MersenneNumber.html\"\u003eMersenne Number\u003c/a\u003e"
			],
			"formula": [
				"a(n) = a(2n) iff a(n) \u003e A002587(n). a(n) = a(2n) = a(4n) iff n is prime p == +-1 (mod 8) and 2^p-1 is prime. - _Thomas Ordowski_, Jan 07 2014",
				"A002326((a(n)-1)/2) = n iff n is odd or n is even such that a(n/2) != a(n). - _Thomas Ordowski_, Jan 11 2014",
				"a(n) = A006530(A000225(n)). - _Vincenzo Librandi_, Jul 13 2016"
			],
			"example": [
				"2^6 - 1 = 63 = 3*21 = 9*7, so a(6) = 7."
			],
			"mathematica": [
				"a[n_] := a[n] = FactorInteger[2^n-1] // Last // First; Table[Print[{n, a[n]}, If[2^n-1 == a[n], \" Mersenne prime\", \" \"]]; a[n], {n, 2, 127}] (* _Jean-François Alcover_, Dec 11 2012 *)",
				"Table[FactorInteger[2^n - 1][[-1, 1]], {n, 2, 40}] (* _Vincenzo Librandi_, Jul 13 2016 *)"
			],
			"program": [
				"(PARI) for(n=2,44, v=factor(2^n-1)[,1]; print1(v[#v]\", \"));",
				"(MAGMA) [Maximum(PrimeDivisors(2^n-1)): n in [2..45]]; // _Vincenzo Librandi_, Jul 13 2016"
			],
			"xref": [
				"Cf. A000225, A006530.",
				"Cf. similar sequences listed in A274906."
			],
			"keyword": "nonn",
			"offset": "2,1",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Description corrected by _Michael Somos_, Feb 24 2002",
				"More terms from _Rick L. Shepherd_, Aug 22 2002"
			],
			"references": 26,
			"revision": 74,
			"time": "2021-12-26T21:19:06-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}