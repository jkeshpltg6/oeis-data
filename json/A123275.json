{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A123275",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 123275,
			"data": "1,1,2,1,1,3,1,2,3,4,1,1,1,1,5,1,2,3,4,5,6,1,1,3,1,5,3,7,1,2,1,4,5,2,7,8,1,1,3,1,1,3,7,1,9,1,2,3,4,5,6,7,8,9,10,1,1,1,1,5,1,7,1,1,5,11,1,2,3,4,5,6,7,8,9,10,11,12,1,1,3,1,5,3,1,1,9,5,11,3,13,1,2,1,4,1,2,7,8,1,2",
			"name": "Square array A(n,m) = largest divisor of m which is coprime to n, read by upwards antidiagonals.",
			"comment": [
				"Read by upwards antidiagonals as A(1,1), A(2,1), A(1,2), A(3,1), A(2,2), A(1,3), etc."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A123275/b123275.txt\"\u003eTable of n, a(n) for n = 1..10440; the first 144 antidiagonals of array\u003c/a\u003e"
			],
			"example": [
				"The top left 18 x 18 corner of the array:",
				"  1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18",
				"  1, 1, 3, 1, 5, 3, 7, 1, 9,  5, 11,  3, 13,  7, 15,  1, 17,  9",
				"  1, 2, 1, 4, 5, 2, 7, 8, 1, 10, 11,  4, 13, 14,  5, 16, 17,  2",
				"  1, 1, 3, 1, 5, 3, 7, 1, 9,  5, 11,  3, 13,  7, 15,  1, 17,  9",
				"  1, 2, 3, 4, 1, 6, 7, 8, 9,  2, 11, 12, 13, 14,  3, 16, 17, 18",
				"  1, 1, 1, 1, 5, 1, 7, 1, 1,  5, 11,  1, 13,  7,  5,  1, 17,  1",
				"  1, 2, 3, 4, 5, 6, 1, 8, 9, 10, 11, 12, 13,  2, 15, 16, 17, 18",
				"  1, 1, 3, 1, 5, 3, 7, 1, 9,  5, 11,  3, 13,  7, 15,  1, 17,  9",
				"  1, 2, 1, 4, 5, 2, 7, 8, 1, 10, 11,  4, 13, 14,  5, 16, 17,  2",
				"  1, 1, 3, 1, 1, 3, 7, 1, 9,  1, 11,  3, 13,  7,  3,  1, 17,  9",
				"  1, 2, 3, 4, 5, 6, 7, 8, 9, 10,  1, 12, 13, 14, 15, 16, 17, 18",
				"  1, 1, 1, 1, 5, 1, 7, 1, 1,  5, 11,  1, 13,  7,  5,  1, 17,  1",
				"  1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,  1, 14, 15, 16, 17, 18",
				"  1, 1, 3, 1, 5, 3, 1, 1, 9,  5, 11,  3, 13,  1, 15,  1, 17,  9",
				"  1, 2, 1, 4, 1, 2, 7, 8, 1,  2, 11,  4, 13, 14,  1, 16, 17,  2",
				"  1, 1, 3, 1, 5, 3, 7, 1, 9,  5, 11,  3, 13,  7, 15,  1, 17,  9",
				"  1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16,  1, 18",
				"  1, 1, 1, 1, 5, 1, 7, 1, 1,  5, 11,  1, 13,  7,  5,  1, 17,  1",
				"...",
				"A(12,1) = 12 because d=12 is the largest divisor of 12 for which gcd(d,1) = 1.",
				"A(12,2) = 3 because d=3 is the largest divisor of 12 for which gcd(d,2) = 1.",
				"A(12,3) = 4 because d=4 is the largest divisor of 12 for which gcd(d,3) = 1.",
				"A(12,4) = 3 because d=3 is the largest divisor of 12 for which gcd(d,4) = 1.",
				"A(12,6) = 1 because d=1 is the largest divisor of 12 for which gcd(d,6) = 1."
			],
			"mathematica": [
				"t[n_, m_] := Last[Select[Divisors[m], GCD[ #, n] == 1 \u0026]];Flatten[Table[t[i + 1 - j, j], {i, 15}, {j, i}]] (* _Ray Chandler_, Oct 17 2006 *)"
			],
			"program": [
				"(Python)",
				"# Produces the triangle when the array is read by antidiagonals (upwards)",
				"from sympy.ntheory import divisors",
				"from math import gcd",
				"def T(n,m):",
				"    return [i for i in divisors(m) if gcd(i,n)==1][-1]",
				"for i in range(1, 16):",
				"    print([T(i+1-j, j) for j in range(1, i+1)]) # _Indranil Ghosh_, Mar 22 2017",
				"(Scheme)",
				";; A naive implementation of A020639 given under that entry. The result of (A123275bi b a) is a product of all those prime factors of a (possibly occurring multiple times) that are not prime factors of b:",
				"(define (A123275 n) (A123275bi (A004736 n) (A002260 n)))",
				"(define (A123275bi b a) (let loop ((a a) (m 1)) (let ((s (A020639 a))) (cond ((= 1 a) m) ((zero? (modulo b s)) (loop (/ a s) m)) (else (loop (/ a s) (* s m)))))))",
				";; _Antti Karttunen_, Mar 22 2017"
			],
			"xref": [
				"Cf. A003989, A020639, A243103."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Leroy Quet_, Oct 10 2006",
			"ext": [
				"Extended by _Ray Chandler_, Oct 17 2006",
				"Name and Example section edited by _Antti Karttunen_, Mar 22 2017"
			],
			"references": 2,
			"revision": 43,
			"time": "2021-05-09T07:54:38-04:00",
			"created": "2006-12-06T03:00:00-05:00"
		}
	]
}