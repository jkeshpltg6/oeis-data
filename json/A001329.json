{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001329",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1329,
			"id": "M4760 N2035",
			"data": "1,1,10,3330,178981952,2483527537094825,14325590003318891522275680,50976900301814584087291487087214170039,155682086691137947272042502251643461917498835481022016",
			"name": "Number of nonisomorphic groupoids with n elements.",
			"comment": [
				"The number of isomorphism classes of closed binary operations on a set of order n."
			],
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"J. Berman and S. Burris, \u003ca href=\"http://www.math.uic.edu/~berman/1994-Groupoid-Catalog-Preprint.pdf\"\u003eA computer study of 3-element groupoids\u003c/a\u003e Lect. Notes Pure Appl. Math. 180 (1994) 379-429  \u003ca href=\"http://www.ams.org/mathscinet-getitem?mr=1404949\"\u003eMR1404949\u003c/a\u003e",
				"M. A. Harrison, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9939-1966-0200219-9\"\u003eThe number of isomorphism types of finite algebras\u003c/a\u003e, Proc. Amer. Math. Soc., 17 (1966), 731-737.",
				"Eric Postpischil, \u003ca href=\"http://groups.google.com/groups?\u0026amp;hl=en\u0026amp;lr=\u0026amp;ie=UTF-8\u0026amp;selm=11802%40shlump.nac.dec.com\u0026amp;rnum=2\"\u003ePosting to sci.math newsgroup, May 21 1990\u003c/a\u003e",
				"Marko Riedel \u003ca href=\"http://math.stackexchange.com/questions/47792/how-many-non-isomorphic-binary-structures-on-the-set-of-n-elements\"\u003eCounting non-isomorphic binary relations\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/A001329/a001329.jpg\"\u003eOverview of A001329, A001423-A001428, A258719, A258720.\u003c/a\u003e",
				"T. Tamura, \u003ca href=\"/A001329/a001329.pdf\"\u003eSome contributions of computation to semigroups and groupoids\u003c/a\u003e, pp. 229-261 of J. Leech, editor, Computational Problems in Abstract Algebra. Pergamon, Oxford, 1970. (Annotated and scanned copy)",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Groupoid.html\"\u003eGroupoid\u003c/a\u003e",
				"\u003ca href=\"/index/Gre#groupoids\"\u003eIndex entries for sequences related to groupoids\u003c/a\u003e"
			],
			"formula": [
				"a(n) = prod{i, j \u003e= 1}(sum{d|[ i, j ]}(d*n(d))^((i, j)*n(i)*n(j))).",
				"a(n) = sum {1*s_1+2*s_2+...=n} (fixA[s_1, s_2, ...]/(1^s_1*s_1!*2^s_2*s2!*...)) where fixA[s_1, s_2, ...] = prod {i, j\u003e=1} ( (sum {d|lcm(i, j)} (d*s_d))^(gcd(i, j)*s_i*s_j)).",
				"a(n) asymptotic to n^(n^2)/n! = A002489(n)/A000142(n) ~ (e*n^(n-1))^n / sqrt(2*pi*n).",
				"a(n) = A079173(n)+A027851(n) = A079177(n)+A079180(n).",
				"a(n) = A079183(n)+A001425(n) = A079187(n)+A079190(n).",
				"a(n) = A079193(n)+A079196(n)+A079199(n)+A001426(n)."
			],
			"maple": [
				"with(numtheory);",
				"with(group):",
				"with(combinat):",
				"pet_cycleind_symm :=",
				"proc(n)",
				"        local p, s;",
				"        option remember;",
				"        if n=0 then return 1; fi;",
				"        expand(1/n*add(a[l]*pet_cycleind_symm(n-l), l=1..n));",
				"end;",
				"pet_flatten_term :=",
				"proc(varp)",
				"        local terml, d, cf, v;",
				"        terml := [];",
				"        cf := varp;",
				"        for v in indets(varp) do",
				"            d := degree(varp, v);",
				"            terml := [op(terml), seq(v, k=1..d)];",
				"            cf := cf/v^d;",
				"        od;",
				"        [cf, terml];",
				"end;",
				"bs_binop :=",
				"proc(n)",
				"        option remember;",
				"        local dsjc, flat, p, q, len,",
				"              cyc, cyc1, cyc2, l1, l2, res;",
				"        if n=0 then return 1; fi;",
				"        if n=1 then return 1; fi;",
				"        res := 0;",
				"        for dsjc in pet_cycleind_symm(n) do",
				"            flat := pet_flatten_term(dsjc);",
				"            p := 1;",
				"            for cyc1 in flat[2] do",
				"                l1 := op(1, cyc1);",
				"                for cyc2 in flat[2] do",
				"                    l2 := op(1, cyc2);",
				"                    len := lcm(l1,l2); q := 0;",
				"                    for cyc in flat[2] do",
				"                        if len mod op(1, cyc) = 0 then",
				"                           q := q  + op(1, cyc);",
				"                        fi;",
				"                    od;",
				"                    p := p * q^(l1*l2/len);",
				"                od;",
				"            od;",
				"            res := res + p*flat[1];",
				"        od;",
				"        res;",
				"end;"
			],
			"xref": [
				"Cf. A001424, A001425, A002489, A006448, A029850, A030245-A030265, A030271, A038015-A038023."
			],
			"keyword": "nonn,nice",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_.",
			"ext": [
				"Formula and more terms from _Christian G. Bower_, May 08 1998, Dec 03 2003."
			],
			"references": 48,
			"revision": 47,
			"time": "2021-12-19T13:59:38-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}