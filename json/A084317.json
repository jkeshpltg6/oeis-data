{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A084317",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 84317,
			"data": "0,2,3,2,5,23,7,2,3,25,11,23,13,27,35,2,17,23,19,25,37,211,23,23,5,213,3,27,29,235,31,2,311,217,57,23,37,219,313,25,41,237,43,211,35,223,47,23,7,25,317,213,53,23,511,27,319,229,59,235,61,231,37,2,513,2311,67",
			"name": "Concatenation of the prime factors of n, in increasing order.",
			"comment": [
				"Prime factor set of n is concatenated as follows:",
				"1. factorize n;",
				"2. order prime factors without exponents in order of magnitude;",
				"3. concatenate digits to get a(n) as a decimal number.",
				"The choice a(1)=0 is conventional; a(1)=1 would have been another possible choice. - _M. F. Hasler_, Oct 21 2014"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A084317/b084317.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = a(squarefree kernel of n) = a(n^k) for any power k \u003e= 1."
			],
			"example": [
				"a(1) = 0 since 1 has no prime factors to concatenate.",
				"n = 2520 = 2*2*2*3*3*5*7; prime factor set = {2,3,5,7}, so a(2520) = 2357."
			],
			"maple": [
				"with(numtheory):",
				"a:= n-\u003e parse(cat(`if`(n=1, 0, sort([factorset(n)[]])[]))):",
				"seq(a(n), n=1..100);  # _Alois P. Heinz_, Dec 06 2014"
			],
			"mathematica": [
				"ffi[x_] := Flatten[FactorInteger[x]] ba[x_] := Table[Part[ffi[x], 2*w-1], {w, 1, lf[x]}] lf[x_] := Length[FactorInteger[x]] nd[x_, y_] := 10*x+y tn[x_] := Fold[nd, 0, x] conc[x_] := Fold[nd, 0, Flatten[IntegerDigits[ba[x]], 1]] Table[conc[w], {w, 1, 128}]",
				"{0}~Join~Table[FromDigits@ Flatten@ IntegerDigits@ Map[First, FactorInteger@ n], {n, 2, 67}] (* _Michael De Vlieger_, May 02 2016 *)"
			],
			"program": [
				"(PARI) A084317(n)=if(n\u003e1,eval(concat(apply(t-\u003eStr(t),factor(n)[,1]~)))) \\\\ Unfortunately up to PARI version 2.7.1 at least, \"Str\" cannot be applied as a closure (= function), but Str = Str() = \"\". - _M. F. Hasler_, Oct 22 2014"
			],
			"xref": [
				"Cf. A084318, A084319."
			],
			"keyword": "base,nonn,look",
			"offset": "1,2",
			"author": "_Labos Elemer_, Jun 16 2003",
			"ext": [
				"Edited by _M. F. Hasler_, Oct 21 2014"
			],
			"references": 24,
			"revision": 30,
			"time": "2020-03-26T18:01:27-04:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}