{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007406",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7406,
			"id": "M4004",
			"data": "1,5,49,205,5269,5369,266681,1077749,9778141,1968329,239437889,240505109,40799043101,40931552621,205234915681,822968714749,238357395880861,238820721143261,86364397717734821,17299975731542641,353562301485889,354019312583809,187497409728228241",
			"name": "Wolstenholme numbers: numerator of Sum_{k=1..n} 1/k^2.",
			"comment": [
				"By Wolstenholme's theorem, p divides a(p-1) for prime p \u003e 3. - _T. D. Noe_, Sep 05 2002",
				"Also p divides a( (p-1)/2 ) for prime p \u003e 3. - _Alexander Adamchuk_, Jun 07 2006",
				"The rationals a(n)/A007407(n) converge to Zeta(2) = (Pi^2)/6 = 1.6449340668... (see the decimal expansion A013661).",
				"For the rationals a(n)/A007407(n), n \u003e= 1, see the W. Lang link under A103345 (case k=2).",
				"See the Wolfdieter Lang link under A103345 on Zeta(k, n) with the rationals for k=1..10, g.f.s and polygamma formulas. - _Wolfdieter Lang_, Dec 03 2013",
				"Denominator of the harmonic mean of the first n squares. - _Colin Barker_, Nov 13 2014",
				"Conjecture: for n \u003e 3, gcd(n, a(n-1)) = A089026(n). Checked up to n = 10^5. - _Amiram Eldar_ and _Thomas Ordowski_, Jul 28 2019",
				"True if n is prime, by Wolstenholme's theorem. It remains to show that gcd(n, a(n-1)) = 1 if n \u003e 3 is composite. - _Jonathan Sondow_, Jul 29 2019"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A007406/b007406.txt\"\u003eTable of n, a(n) for n = 1..1152\u003c/a\u003e (terms 1..200 from T. D. Noe)",
				"Stephen Crowley, \u003ca href=\"https://arxiv.org/abs/1207.1126\"\u003eTwo New Zeta Constants: Fractal String, Continued Fraction, and Hypergeometric Aspects of the Riemann Zeta Function\u003c/a\u003e, arXiv:1207.1126 [math.NT], 2012.",
				"R. Mestrovic, \u003ca href=\"http://arxiv.org/abs/1111.3057\"\u003eWolstenholme's theorem: Its Generalizations and Extensions in the last hundred and fifty years (1862-2011)\u003c/a\u003e, arXiv:1111.3057 [math.NT], 2011.",
				"Hisanori Mishima, \u003ca href=\"http://www.asahi-net.or.jp/~KC2H-MSM/mathland/matha1/matha103.htm\"\u003eFactorizations of many number sequences\u003c/a\u003e",
				"Hisanori Mishima, \u003ca href=\"http://www.asahi-net.or.jp/~KC2H-MSM/mathland/matha1/matha129.htm\"\u003eFactorizations of many number sequences\u003c/a\u003e",
				"Hisanori Mishima, \u003ca href=\"http://www.asahi-net.or.jp/~KC2H-MSM/mathland/matha1/matha1291.htm\"\u003eFactorizations of many number sequences\u003c/a\u003e",
				"D. Y. Savio, E. A. Lamagna and S.-M. Liu, \u003ca href=\"http://dx.doi.org/10.1007/978-1-4613-9647-5_2\"\u003eSummation of harmonic numbers\u003c/a\u003e, pp. 12-20 of E. Kaltofen and S. M. Watt, editors, Computers and Mathematics, Springer-Verlag, NY, 1989.",
				"M. D. Schmidt, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL13/Schmidt/multifact.html\"\u003eGeneralized j-Factorial Functions, Polynomials, and Applications \u003c/a\u003e, J. Int. Seq. 13 (2010), 10.6.7, Section 4.3.2.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/WolstenholmesTheorem.html\"\u003eWolstenholme's Theorem\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/WolstenholmeNumber.html\"\u003eWolstenholme Number\u003c/a\u003e"
			],
			"formula": [
				"Sum_{k=1..n} 1/k^2 = sqrt(Sum_{j=1..n} Sum_{i=1..n} 1/(i*j)^2). - _Alexander Adamchuk_, Oct 26 2004",
				"G.f. for rationals a(n)/A007407(n), n \u003e= 1: polylog(2,x)/(1-x).",
				"a(n) = Numerator of (Pi^2)/6 - Zeta(2,n). - _Artur Jasinski_, Mar 03 2010"
			],
			"maple": [
				"a:= n-\u003e numer(add(1/i^2, i=1..n)): seq(a(n), n=1..24);  # _Zerinvary Lajos_, Mar 28 2007"
			],
			"mathematica": [
				"a[n_] := If[ n\u003c1, 0, Numerator[HarmonicNumber[n, 2]]]; Table[a[n], {n, 100}]",
				"Numerator[HarmonicNumber[Range[20],2]] (* _Harvey P. Dale_, Jul 06 2014 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, 0, numerator( sum( k=1, n, 1 / k^2 ) ) )} /* _Michael Somos_, Jan 16 2011 */",
				"(Haskell)",
				"import Data.Ratio ((%), numerator)",
				"a007406 n = a007406_list !! (n-1)",
				"a007406_list = map numerator $ scanl1 (+) $ map (1 %) $ tail a000290_list",
				"-- _Reinhard Zumkeller_, Jul 06 2012",
				"(MAGMA) [Numerator(\u0026+[1/k^2:k in [1..n]]):n in [1..23]]; // _Marius A. Burtea_, Aug 02 2019"
			],
			"xref": [
				"Cf. A001008, A007407 (denominators), A000290.",
				"Numbers n such that a(n) is prime are listed in A111354. Primes in {a(n)} are listed in A123751. - _Alexander Adamchuk_, Oct 11 2006"
			],
			"keyword": "nonn,frac,easy,nice",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, _Mira Bernstein_",
			"references": 74,
			"revision": 98,
			"time": "2019-08-05T02:48:44-04:00",
			"created": "1994-09-19T03:00:00-04:00"
		}
	]
}