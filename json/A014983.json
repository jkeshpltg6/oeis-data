{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A014983",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 14983,
			"data": "0,1,-2,7,-20,61,-182,547,-1640,4921,-14762,44287,-132860,398581,-1195742,3587227,-10761680,32285041,-96855122,290565367,-871696100,2615088301,-7845264902,23535794707,-70607384120,211822152361,-635466457082,1906399371247",
			"name": "a(n) = (1 - (-3)^n)/4.",
			"comment": [
				"q-integers for q=-3.",
				"Let A be the Hessenberg matrix of order n, defined by: A[1,j]=1, A[i,i]:=-3, A[i,i-1]=-1, and A[i,j]=0 otherwise. Then, for n\u003e=1, a(n)=(-1)^n*charpoly(A,0). - _Milan Janjic_, Jan 27 2010",
				"Pisano period lengths:  1, 2, 1, 4, 4, 2, 3, 8, 1, 4, 10, 4, 6, 6, 4, 16, 16, 2, 9, 4, ... - _R. J. Mathar_, Aug 10 2012"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A014983/b014983.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=927\"\u003eEncyclopedia of Combinatorial Structures 927\u003c/a\u003e",
				"László Németh, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL21/Nemeth/nemeth6.html\"\u003eThe trinomial transform triangle\u003c/a\u003e, J. Int. Seqs., Vol. 21 (2018), Article 18.7.3. Also \u003ca href=\"https://arxiv.org/abs/1807.07109\"\u003earXiv:1807.07109\u003c/a\u003e [math.NT], 2018.",
				"R. A. Sulanke, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL3/SULANKE/sulanke.html\"\u003eMoments of generalized Motzkin paths\u003c/a\u003e, J. Integer Sequences, Vol. 3 (2000), #00.1.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (-2,3)"
			],
			"formula": [
				"a(n) = a(n-1) + (-3)^(n-1).",
				"G.f.: x/((1-x)*(1+3*x)).",
				"a(n) = -(-1)^n*A015518(n).",
				"a(n) = the (1, 2)-th element of M^n, where M = ((1, 1, 1, -2), (1, 1, -2, 1), (1, -2, 1, 1), (-2, 1, 1, 1)). - _Simone Severini_, Nov 25 2004",
				"a(0)=0, a(1)=1, a(n) = -2*a(n-1) + 3*a(n-2) for n\u003e1. - _Philippe Deléham_, Sep 19 2009",
				"From _Sergei N. Gladkovskii_, Apr 29 2012: (Start)",
				"G.f. A(x) = G(0)/4; G(k) = 1 - 1/(3^(2*k) - 3*x*3^(4*k)/(3*x*3^(2*k) + 1/(1 + 1/(3*3^(2*k) - 3^(3)*x*3^(4*k)/(3^2*x*3^(2*k) - 1/G(k+1)))))); (continued fraction, 3rd kind, 6-step).",
				"E.g.f. E(x) = G(0)/4; G(k) = 1 - 1/(9^k - 3*x*81^k/(3*x*9^k + (2*k+1)/(1 + 1/(3*9^k - 27*x*81^k/(9*x*9^k - (2*k+2)/G(k+1)))))); (continued fraction, 3rd kind, 6-step). (End)",
				"a(n) = A084222(n) - 1. - _Filip Zaludek_, Nov 19 2016",
				"E.g.f.: sinh(x)*cosh(x)*exp(-x). - _Ilya Gutkovskiy_, Nov 20 2016"
			],
			"maple": [
				"a:=n-\u003esum ((-3)^j, j=0..n): seq(a(n), n=-1..25); # _Zerinvary Lajos_, Dec 16 2008"
			],
			"mathematica": [
				"nn = 25; CoefficientList[Series[x/((1 - x)*(1 + 3*x)), {x, 0, nn}], x] (* _T. D. Noe_, Jun 21 2012 *)",
				"Table[(1 - (-3)^n)/4, {n, 0, 27}] (* _Michael De Vlieger_, Nov 23 2016 *)"
			],
			"program": [
				"(PARI) a(n)=(1-(-3)^n)/4",
				"(Sage) [gaussian_binomial(n,1,-3) for n in range(0,27)] # _Zerinvary Lajos_, May 28 2009",
				"(MAGMA) [(1-(-3)^n)/4: n in [0..30]]; // _G. C. Greubel_, May 26 2018"
			],
			"xref": [
				"Cf. A077925, A014985, A014986, A014987, A014989, A014990, A014991, A014992, A014993, A014994. - _Zerinvary Lajos_, Dec 16 2008"
			],
			"keyword": "sign,easy",
			"offset": "0,3",
			"author": "_Olivier Gérard_",
			"references": 28,
			"revision": 62,
			"time": "2019-12-07T12:18:18-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}