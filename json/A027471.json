{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A027471",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 27471,
			"data": "0,1,6,27,108,405,1458,5103,17496,59049,196830,649539,2125764,6908733,22320522,71744535,229582512,731794257,2324522934,7360989291,23245229340,73222472421,230127770466,721764371007,2259436291848",
			"name": "a(n) = (n-1)*3^(n-2), n \u003e 0.",
			"comment": [
				"Arithmetic derivative of 3^n: a(n) = A003415(A000244(n)). - _Reinhard Zumkeller_, Feb 26 2002",
				"Binomial transform of A053220(n+1) is a(n+2). Binomial transform of A001787 is a(n+1). Binomial transform of A045883(n-1). - _Michael Somos_, Jul 10 2003",
				"If X_1,X_2,...,X_n are 3-blocks of a (3n+1)-set X then, for n \u003e= 1, a(n+2) is the number of (n+1)-subsets of X intersecting each X_i, (i=1,2,...,n). \u003e - _Milan Janjic_, Nov 18 2007",
				"Let S be a binary relation on the power set P(A) of a set A having n = |A| elements such that for every element x, y of P(A), xSy if x is a subset of y. Then a(n+1) = the sum of the differences in size (i.e., |y|-|x|) for all (x, y) of S. - _Ross La Haye_, Nov 19 2007"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A027471/b027471.txt\"\u003eTable of n, a(n) for n = 1..700\u003c/a\u003e",
				"Jean-Luc Baril, Sergey Kirgizov, and Vincent Vajnovszki, \u003ca href=\"https://arxiv.org/abs/1803.06706\"\u003eDescent distribution on Catalan words avoiding a pattern of length at most three\u003c/a\u003e, arXiv:1803.06706 [math.CO], 2018.",
				"Samuele Giraudo, \u003ca href=\"http://arxiv.org/abs/1603.01040\"\u003ePluriassociative algebras I: The pluriassociative operad\u003c/a\u003e, arXiv:1603.01040 [math.CO], 2016.",
				"Milan Janjić, \u003ca href=\"http://www.pmfbl.org/janjic/\"\u003eTwo Enumerative Functions\u003c/a\u003e",
				"Milan Janjić and Boris Petković, \u003ca href=\"http://arxiv.org/abs/1301.4550\"\u003eA Counting Function\u003c/a\u003e, arXiv preprint arXiv:1301.4550 [math.CO], 2013. - From _N. J. A. Sloane_, Feb 13 2013",
				"Milan Janjić and Boris Petković, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Janjic/janjic45.html\"\u003eA Counting Function Generalizing Binomial Coefficients and Some Other Classes of Integers\u003c/a\u003e, J. Int. Seq. 17 (2014) # 14.3.5.",
				"Frank Ellermann, \u003ca href=\"/A001792/a001792.txt\"\u003eIllustration of binomial transforms\u003c/a\u003e",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=715\"\u003eEncyclopedia of Combinatorial Structures 715\u003c/a\u003e",
				"Ross La Haye, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL12/LaHaye/lahaye5.html\"\u003eBinary Relations on the Power Set of an n-Element Set\u003c/a\u003e, Journal of Integer Sequences, Vol. 12 (2009), Article 09.2.6.",
				"Aleksandar Petojević, \u003ca href=\"http://dx.doi.org/10.5937/MatMor0801037P\"\u003eA Note about the Pochhammer Symbol\u003c/a\u003e, Mathematica Moravica, Vol. 12-1 (2008), 37-42.",
				"Franck Ramaharo, \u003ca href=\"https://arxiv.org/abs/1802.07701\"\u003eStatistics on some classes of knot shadows\u003c/a\u003e, arXiv:1802.07701 [math.CO], 2018.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (6,-9)."
			],
			"formula": [
				"From _Wolfdieter Lang_: (Start)",
				"G.f.: (x/(1-3*x))^2.",
				"E.g.f.: (1 + (3*x-1)*exp(3*x))/9.",
				"a(n) = 3^(n-2)*(n-1) (convolution of A000244, powers of 3, with itself). (End)",
				"a(n) = 6*a(n-1) - 9*a(n-2), n \u003e 2, a(1)=0, a(2)=1. - _Barry E. Williams_, Jan 13 2000",
				"a(n) = A036290(n)/3. - _Paul Barry_, Feb 06 2004",
				"a(n) = Sum_{k=0..n} 3^(n-k)*binomial(n-k+1, k)*binomial(1, (k+1)/2)*(1-(-1)^k)/2.",
				"From _Paul Barry_, Feb 15 2005: (Start)",
				"a(n) = (1/3)*Sum_{k=0..2n} T(n, k)*k, where T(n, k) is given by A027907.",
				"a(n) = (1/3)*Sum_{k=0..n} Sum_{j=0..n} C(n, j)*C(j, k)*(j+k).",
				"a(n) = Sum_{k=0..n} Sum_{j=0..n} C(n, j)*C(j, k)*(j-k).",
				"a(n+1) = Sum_{k=0..n} Sum_{j=0..n} C(n, j)*C(j, k)*(j+k+1). (End)",
				"Sum_{n\u003e=2} 1/a(n) = 3*log(3/2). - _Jaume Oliver Lafont_, Sep 19 2009",
				"a(n) = 3*a(n-1) + 3^(n-2) (with a(1)=0). - _Vincenzo Librandi_, Dec 30 2010",
				"Sum_{n\u003e=2} (-1)^n/a(n) = 3*log(4/3). - _Amiram Eldar_, Oct 28 2020"
			],
			"maple": [
				"seq((n-1)*3^(n-2), n=1..40); # _Muniru A Asiru_, Jul 15 2018"
			],
			"mathematica": [
				"Table[(n-1)3^(n-2),{n,30}] (* or *)",
				"LinearRecurrence[{6,-9},{0,1},30] (* _Harvey P. Dale_, Apr 14 2016 *)",
				"Range[0, 24]! CoefficientList[ Series[x*Exp[3 x], {x, 0, 24}], x] (* _Robert G. Wilson v_, Aug 03 2018 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c1, 0, (n-1)*3^(n-2));",
				"(MAGMA) [(n-1)*3^(n-2): n in [1..30]]; // _Vincenzo Librandi_, Jun 09 2011",
				"(GAP) List([1..40], n-\u003e (n-1)*3^(n-2)); # _Muniru A Asiru_, Jul 15 2018",
				"(Sage) [3^(n-2)*(n-1) for n in (1..30)] # _G. C. Greubel_, May 20 2021"
			],
			"xref": [
				"Second column of A027465.",
				"Partial sums of A081038.",
				"Cf. A006234."
			],
			"keyword": "nonn,easy",
			"offset": "1,3",
			"author": "_Olivier Gérard_",
			"ext": [
				"Edited by _Michael Somos_, Jul 10 2003"
			],
			"references": 51,
			"revision": 101,
			"time": "2021-11-25T09:40:07-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}