{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A202870",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 202870,
			"data": "1,-1,1,-11,1,1,-46,37,-1,1,-162,299,-99,1,1,-567,1675,-1324,225,-1,1,-1872,8316,-11315,5292,-432,1,1,-5881,40254,-79457,60782,-16458,760,-1,1,-17990,182413,-490520,543130,-260498,45424,-1232",
			"name": "Array:  row n shows the coefficients of the characteristic polynomial of the n-th principal submatrix of the symmetric matrix A202869; by antidiagonals.",
			"comment": [
				"Let p(n)=p(n,x) be the characteristic polynomial of the n-th principal submatrix. The zeros of p(n) are positive, and they interlace the zeros of p(n+1)."
			],
			"link": [
				"S.-G. Hwang, \u003ca href=\"http://matrix.skku.ac.kr/Series-E/Monthly-E.pdf\"\u003eCauchy's interlace theorem for eigenvalues of Hermitian matrices\u003c/a\u003e, American Mathematical Monthly 111 (2004) 157-159.",
				"A. Mercer and P. Mercer, \u003ca href=\"http://dx.doi.org/10.1155/S016117120000257X\"\u003eCauchy's interlace theorem and lower bounds for the spectral radius\u003c/a\u003e, International Journal of Mathematics and Mathematical Sciences 23, no. 8 (2000) 563-566."
			],
			"example": [
				"The 1st principal submatrix (ps) of A202869 is {{1}} (using Mathematica matrix notation), with p(1)=1-x and zero-set {1}.",
				"...",
				"The 2nd ps is {{1,3},{3,10}}, with p(2)=1-11x+x^2 and zero-set {0.091..., 10.908...}.",
				"...",
				"The 3rd ps is {{1,3,4},{3,10,15},{4,15,26}}, with p(3)=1-46x+37x^2-x^3 and zero-set {0.022..., 1.265..., 35.712...}.",
				"...",
				"Top of the array:",
				"1...-1",
				"1...-11....1",
				"1...-46....37....-1",
				"1...-162...299...-99...1"
			],
			"mathematica": [
				"f[k_] := Floor[k*GoldenRatio];",
				"U[n_] := NestList[Most[Prepend[#, 0]] \u0026, #, Length[#] - 1] \u0026[Table[f[k], {k, 1, n}]];",
				"L[n_] := Transpose[U[n]];",
				"F[n_] := CharacteristicPolynomial[L[n].U[n], x];",
				"c[n_] := CoefficientList[F[n], x]",
				"TableForm[Flatten[Table[F[n], {n, 1, 10}]]]",
				"Table[c[n], {n, 1, 12}]",
				"Flatten[%]  (* A202870 as a sequence *)",
				"TableForm[Table[c[n], {n, 1, 10}]]  (* A202870 as a matrix *)"
			],
			"xref": [
				"Cf. A202869, A202605."
			],
			"keyword": "tabl,sign",
			"offset": "1,4",
			"author": "_Clark Kimberling_, Dec 26 2011",
			"references": 3,
			"revision": 12,
			"time": "2017-10-02T09:58:19-04:00",
			"created": "2011-12-31T15:31:54-05:00"
		}
	]
}