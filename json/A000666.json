{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000666",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 666,
			"id": "M1650 N0646",
			"data": "1,2,6,20,90,544,5096,79264,2208612,113743760,10926227136,1956363435360,652335084592096,405402273420996800,470568642161119963904,1023063423471189431054720,4178849203082023236058229792,32168008290073542372004082199424",
			"name": "Number of symmetric relations on n nodes.",
			"comment": [
				"Each node may or may not be related to itself.",
				"Also the number of rooted graphs on n+1 nodes.",
				"The 1-to-1 correspondence is as follows: Given a rooted graph on n+1 nodes, replace each edge joining the root node to another node with a self-loop at that node and erase the root node. The result is an undirected graph on n nodes which is the graph of the symmetric relation.",
				"Also the number of the graphs with n nodes whereby each node is colored or not colored. A loop can be interpreted as a colored node. - _Juergen Will_, Oct 31 2011"
			],
			"reference": [
				"F. Harary and E. M. Palmer, Graphical Enumeration, Academic Press, NY, 1973, pp. 101, 241.",
				"M. D. McIlroy, Calculation of numbers of structures of relations on finite sets, Massachusetts Institute of Technology, Research Laboratory of Electronics, Quarterly Progress Reports, No. 17, Sept. 15, 1955, pp. 14-22.",
				"R. W. Robinson, Numerical implementation of graph counting algorithms, AGRC Grant, Math. Dept., Univ. Newcastle, Australia, 1976.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"David Applegate, \u003ca href=\"/A000666/b000666.txt\"\u003eTable of n, a(n) for n = 0..80\u003c/a\u003e [Shortened file because terms grow rapidly: see Applegate link below for additional terms]",
				"David Applegate, \u003ca href=\"/A000666/a000666.txt\"\u003eTable of n, a(n) for n = 0..100\u003c/a\u003e",
				"P. J. Cameron, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL3/groups.html\"\u003eSequences realized by oligomorphic permutation groups\u003c/a\u003e, J. Integ. Seqs. Vol. 3 (2000), #00.1.5.",
				"R. L. Davis, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9939-1953-0055294-2\"\u003eThe number of structures of finite relations\u003c/a\u003e, Proc. Amer. Math. Soc. 4 (1953), 486-495.",
				"R. L. Davis, \u003ca href=\"/A000568/a000568_4.pdf\"\u003eStructure of dominance relations\u003c/a\u003e, Bull. Math. Biophys., 16 (1954), 131-140. [Annotated scanned copy]",
				"F. Harary, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9947-1955-0068198-2\"\u003eThe number of linear, directed, rooted, and connected graphs\u003c/a\u003e, Trans. Am. Math. Soc. 78 (1955) 445-463, eq. (24).",
				"Frank Harary, Edgar M. Palmer, Robert W. Robinson and Allen J. Schwenk, \u003ca href=\"http://dx.doi.org/10.1002/jgt.3190010405\"\u003eEnumeration of graphs with signed points and lines\u003c/a\u003e, J. Graph Theory 1 (1977), no. 4, 295-308.",
				"Adib Hassan, Po-Chien Chung and Wayne B. Hayes, \u003ca href=\"https://arxiv.org/abs/1708.04341\"\u003eGraphettes: Constant-time determination of graphlet and orbit identity including (possibly disconnected) graphlets up to size 8\u003c/a\u003e, arXiv:1708.04341 [cs.DS], 2017.",
				"M. D. McIlroy, \u003ca href=\"/A000088/a000088.pdf\"\u003eCalculation of numbers of structures of relations on finite sets\u003c/a\u003e, Massachusetts Institute of Technology, Research Laboratory of Electronics, Quarterly Progress Reports, No. 17, Sep. 15, 1955, pp. 14-22. [Annotated scanned copy]",
				"W. Oberschelp, \u003ca href=\"http://gdz.sub.uni-goettingen.de/dms/load/img/?PID=GDZPPN002298732\"\u003eKombinatorische Anzahlbestimmungen in Relationen\u003c/a\u003e, Math. Ann., 174 (1967), 53-78.",
				"W. Oberschelp, \u003ca href=\"/A000666/a000666_1.pdf\"\u003eThe number of non-isomorphic m-graphs\u003c/a\u003e, Presented at Mathematical Institute Oberwolfach, July 3 1967 [Scanned copy of manuscript]",
				"W. Oberschelp, \u003ca href=\"/A000662/a000662.pdf\"\u003e Strukturzahlen in endlichen Relationssystemen\u003c/a\u003e, in Contributions to Mathematical Logic (Proceedings 1966 Hanover Colloquium), pp. 199-213, North-Holland Publ., Amsterdam, 1968. [Annotated scanned copy]",
				"G. Pfeiffer, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL7/Pfeiffer/pfeiffer6.html\"\u003eCounting Transitive Relations\u003c/a\u003e, Journal of Integer Sequences, Vol. 7 (2004), Article 04.3.2.",
				"Marko Riedel, \u003ca href=\"/A000666/a000666_2.maple.txt\"\u003eMaple code for sequence.\u003c/a\u003e",
				"Marko Riedel et al., \u003ca href=\"https://math.stackexchange.com/questions/41386/\"\u003eEnumerating Graphs with Self-Loops\u003c/a\u003e",
				"R. W. Robinson, \u003ca href=\"/A000666/a000666_2.pdf\"\u003eNotes - \"A Present for Neil Sloane\"\u003c/a\u003e",
				"R. W. Robinson, \u003ca href=\"/A004102/a004102_1.pdf\"\u003eNotes - computer printout\u003c/a\u003e",
				"Sage, \u003ca href=\"http://www.sagemath.org/doc/reference/graphs/sage/graphs/graph_generators.html\"\u003eCommon Graphs (Graph Generators)\u003c/a\u003e",
				"StackExchange, \u003ca href=\"http://math.stackexchange.com/questions/41386/enumerating-graphs-with-self-loops\"\u003eEnumerating Graphs with Self-Loops\u003c/a\u003e, Jan 23 2014",
				"J. M. Tangen and N. J. A. Sloane, \u003ca href=\"/A000666/a000666.pdf\"\u003eCorrespondence, 1976-1976\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RootedGraph.html\"\u003eRooted Graph\u003c/a\u003e"
			],
			"formula": [
				"Euler transform of A054921. - _N. J. A. Sloane_, Oct 25 2018",
				"Let G_{n+1,k} be the number of rooted graphs on n+1 nodes with k edges and let G_{n+1}(x) = Sum_{k=0..n(n+1)/2} G_{n+1,k} x^k. Thus a(n) = G_{n+1}(1). Let S_n(x_1, ..., x_n) denote the cycle index for Sym_n. (cf. the link in A000142).",
				"Compute x_1*S_n and regard it as the cycle index of a set of permutations on n+1 points and find the corresponding cycle index for the action on the n(n+1)/2 edges joining those points (the corresponding \"pair group\"). Finally, by replacing each x_i by 1+x^i gives G_{n+1}(x). [Harary]",
				"Example, n=2. S_2 = (1/2)*(x_1^2+x_2), x_1*S_2 = (1/2)*(x_1^3+x_1*x_2). The pair group is (1/2)*(x_1^2+x_1*x_2) and so G_3(x) = (1/2)*((1+x)^3+(1+x)*(1+x^2)) = 1+2*x+2*x^2+x^3; set x=1 to get a(2) = 6.",
				"a(n) ~ 2^(n*(n+1)/2)/n! [McIlroy, 1955]. - _Vaclav Kotesovec_, Dec 19 2016"
			],
			"maple": [
				"# see Riedel link above"
			],
			"mathematica": [
				"Join[{1,2}, Table[CycleIndex[Join[PairGroup[SymmetricGroup[n]], Permutations[Range[n*(n-1)/2+1, n*(n+1)/2]], 2],s] /. Table[s[i]-\u003e2, {i,1,n^2-n}], {n,2,8}]] (* _Geoffrey Critzer_, Nov 04 2011 *)",
				"Table[Module[{eds,pms,leq},",
				"eds=Select[Tuples[Range[n],2],OrderedQ];",
				"pms=Map[Sort,eds/.Table[i-\u003ePart[#,i],{i,n}]]\u0026/@Permutations[Range[n]];",
				"leq=Function[seq,PermutationCycles[Ordering[seq],Length]]/@pms;",
				"Total[Thread[Power[2,leq]]]/n!",
				"],{n,0,8}] (* This is after _Geoffrey Critzer_'s program but does not use the (deprecated) Combinatorica package. - _Gus Wiseman_, Jul 21 2016 *)",
				"permcount[v_] := Module[{m = 1, s = 0, k = 0, t}, For[i = 1, i \u003c= Length[v], i++, t = v[[i]]; k = If[i \u003e 1 \u0026\u0026 t == v[[i - 1]], k + 1, 1]; m *= t*k; s += t]; s!/m];",
				"edges[v_] := Sum[Sum[GCD[v[[i]], v[[j]]], {j, 1, i-1}], {i, 2, Length[v]}] + Sum[Quotient[v[[i]], 2] + 1, {i, 1, Length[v]}];",
				"a[n_] := a[n] = (s = 0; Do[s += permcount[p]*2^edges[p], {p, IntegerPartitions[n]}]; s/n!);",
				"Table[Print[\"a(\", n, \") = \", a[n]]; a[n], {n, 0, 17}] (* _Jean-François Alcover_, Nov 13 2017, after _Andrew Howroyd_ *)"
			],
			"program": [
				"(PARI)",
				"permcount(v) = {my(m=1,s=0,k=0,t); for(i=1,#v,t=v[i]; k=if(i\u003e1\u0026\u0026t==v[i-1],k+1,1); m*=t*k;s+=t); s!/m}",
				"edges(v) = {sum(i=2, #v, sum(j=1, i-1, gcd(v[i],v[j]))) + sum(i=1, #v, v[i]\\2 + 1)}",
				"a(n) = {my(s=0); forpart(p=n, s+=permcount(p)*2^edges(p)); s/n!} \\\\ _Andrew Howroyd_, Oct 22 2017"
			],
			"xref": [
				"Cf. A000595, A001172, A001174, A006905, A000250, A054921 (connected relations)."
			],
			"keyword": "nonn,nice",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Description corrected by _Christian G. Bower_",
				"More terms from _Vladeta Jovovic_, Apr 18 2000",
				"Entry revised by _N. J. A. Sloane_, Mar 06 2007"
			],
			"references": 36,
			"revision": 118,
			"time": "2021-09-01T03:05:55-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}