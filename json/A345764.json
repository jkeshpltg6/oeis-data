{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A345764",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 345764,
			"data": "0,1,2,2,1,2,2,7,8,7,10,11,10,7,8,7,10,11,10,19,20,21,20,19,24,25,25,24,19,20,21,20,19,24,25,25,24,37,38,39,40,39,38,37,44,45,46,45,44,37,38,39,40,39,38,37,44,45,46,45,44,61,62,63,64,65,64,63,62,61",
			"name": "Number the tiles of a regular hexagonal tiling from 0 in a spiral. Consider perpendicular axes, X and Y, through the center of tile 0, one of which passes through the center of tile 1. Define a set of equivalence classes of tiles with respect to reflections about X and Y. a(n) is the smallest number of a tile in the same equivalence class as tile n.",
			"comment": [
				"The sense of the spiral (clockwise/counterclockwise) and its orientation are not significant, but for the purpose of illustration, we depict a counterclockwise spiral with its first step towards the right side of the page.",
				"Equivalence classes contain a maximum of 4 tiles. This happens when tile m's reflection about axis X is a different tile, m_x, and these 2 tiles' reflections about axis Y are 2 further tiles, m_y and m_xy, to give an equivalence class {m, m_x, m_y, m_xy}. Some equivalence classes are smaller, because a tile is its own reflection about an axis, X or Y, that passes through the center of the tile.",
				"The Wichmann reference describes bijections from certain unique factorization domains to the hexagonal tiling. Align the spiral with the mapping so that domain identities 0 and 1 map to tiles 0 and 1 respectively. If two integers from one of the domains map to tiles in the same equivalence class, then they share the same status as units, primes or composites."
			],
			"link": [
				"Eric Weisstein's World of Mathematics, \u003ca href=\"https://mathworld.wolfram.com/HexagonalGrid.html\"\u003eHexagonal Grid\u003c/a\u003e.",
				"Brian Wichmann, \u003ca href=\"http://www.tilingsearch.org/special/ufd.pdf\"\u003eTiling for Unique Factorization Domains\u003c/a\u003e, Jul 22 2019."
			],
			"formula": [
				"a(n) = min({m : |A307012(m)| = |A307012(n)| and |A328818(m)| = |A328818(n)|}).",
				"a(n) = min({m : |A307012(n)| = |A307012(m)| and |2*A307013(n) - A307012(n)| = |2*A307013(m) - A307012(m)|})."
			],
			"example": [
				"Illustration of the relative positions of tiles on the spiral, marking the n-th tile on the spiral by a(n) to denote its equivalence class:",
				".",
				".              24 -- 25 -- 25 -- 24",
				".              /                   \\",
				".             /                     \\",
				".           19    10 -- 11 -- 10    19",
				".           /     /             \\     \\",
				".          /     /               \\     \\",
				".        20     7     2 --- 2     7    20",
				".        /     /     /       \\     \\     \\",
				".       /     /     /         \\     \\     \\",
				".     21     8     1     0 --- 1     8    21",
				".       \\     \\     \\               /     /",
				".        \\     \\     \\             /     /",
				".        20     7     2 --- 2 --- 7    20",
				".          \\     \\                     /",
				".           \\     \\                   /",
				".           19    10 -- 11 -- 10 -- 19",
				".             \\",
				".              \\",
				".              24 -- 25 -- 25 -- 24",
				".",
				"Recall that the underlying tile numbers count steps along the spiral from 0. When we follow the spiral in the illustration above and encounter a number m, which denotes an equivalence class, for the first time, this is also at tile number m.",
				"Tile 1 maps to itself (as does tile 4) when reflected about the axis through the centers of tiles 0 and 1 (horizontal as shown above). Tiles 1 and 4 map to each other when reflected about the perpendicular (vertical) axis. So tiles 1 and 4 form an equivalence class, and the smallest number of a tile in this class is 1. So a(1) = 1 and a(4) = 1."
			],
			"xref": [
				"Cf. A274820, A307012, A307013, A328818."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Peter Munn_, Jun 26 2021",
			"references": 6,
			"revision": 23,
			"time": "2021-08-05T00:15:41-04:00",
			"created": "2021-08-05T00:15:41-04:00"
		}
	]
}