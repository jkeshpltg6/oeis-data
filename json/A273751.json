{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A273751",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 273751,
			"data": "1,2,3,4,5,7,6,8,10,13,9,11,14,17,21,12,15,18,22,26,31,16,19,23,27,32,37,43,20,24,28,33,38,44,50,57,25,29,34,39,45,51,58,65,73,30,35,40,46,52,59,66,74,82,91,36,41",
			"name": "Triangle of the natural numbers written by decreasing antidiagonals.",
			"comment": [
				"A permutation of the natural numbers.",
				"a(n) and A091995(n) are different at the ninth term.",
				"Antidiagonal sums: 1, 2, 7, 11, ... = A235355(n+1). Same idea.",
				"Row sums: 1, 5, 16, 37, 72, 124, 197, 294, ... = 7*n^3/12 -n^2/8 +5*n/12 +1/16 -1/16*(-1)^n with g.f. x*(1+2*x+3*x^2+x^3) / ( (1+x)*(x-1)^4 ). The third difference is of period 2: repeat [3, 4].",
				"Indicates the order in which electrons fill the different atomic orbitals (s,p,d,f,g,h). - _Alexander Goebel_, May 12 2020"
			],
			"link": [
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Atomic_orbital#Orbital_energy\"\u003eAtomic orbital\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"example": [
				"1,",
				"2,   3,",
				"4,   5,  7,",
				"6,   8, 10, 13,",
				"9,  11, 14, 17, 21,",
				"12, 15, 18, 22, 26, 31,",
				"16, 19, 23, 27, 32, 37, 43,",
				"20, etc."
			],
			"maple": [
				"A273751 := proc(n,k)",
				"    option remember;",
				"    if k = n then",
				"        A002061(n) ;",
				"    elif k \u003e n or k \u003c 0 then",
				"        0;",
				"    elif k = n-1 then",
				"        procname(n-1,k)+k ;",
				"    else",
				"        procname(n-1,k+1)+1 ;",
				"    end if;",
				"end proc: # _R. J. Mathar_, Jun 13 2016"
			],
			"mathematica": [
				"T[n_, k_] := T[n, k] = Which[k == n, n(n-1) + 1, k == n-1, (n-1)^2 + 1, k == 1, n + T[n-2, 1], 1 \u003c k \u003c n-1, T[n-1, k+1] + 1,True, 0];",
				"Table[T[n, k], {n, 12}, {k, n}] // Flatten (* _Jean-François Alcover_, Jun 10 2016 *)"
			],
			"xref": [
				"Cf. A002061 (right diagonal), A002620 (first column), A033638, A091995, A234305 (antidiagonals of the triangle)."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Paul Curtz_, May 30 2016",
			"references": 2,
			"revision": 47,
			"time": "2020-07-31T20:20:36-04:00",
			"created": "2016-06-10T10:32:19-04:00"
		}
	]
}