{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A054898",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 54898,
			"data": "0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,3,4,4,4,4,4,4,4,4,4,5,5,5,5,5,5,5,5,5,6,6,6,6,6,6,6,6,6,7,7,7,7,7,7,7,7,7,8,8,8,8,8,8,8,8,8,10,10,10,10,10,10,10,10,10,11,11,11,11,11,11,11",
			"name": "a(n) = Sum_{k\u003e0} floor(n/9^k).",
			"comment": [
				"Different from the highest power of 9 dividing n!, A090618."
			],
			"link": [
				"Hieronymus Fischer, \u003ca href=\"/A054898/b054898.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e"
			],
			"formula": [
				"floor[n/9] + floor[n/81] + floor[n/729] + floor[n/6561] + ....",
				"a(n) = (n-A053830(n))/8.",
				"From _Hieronymus Fischer_, Aug 14 2007 (Start):",
				"Recurrence:",
				"a(n) = floor(n/9) + a(floor(n/9));",
				"a(9*n) = n + a(n);",
				"a(n*9^m) = n*(9^m-1)/8 + a(n).",
				"a(k*9^m) = k*(9^m-1)/8, for 0\u003c=k\u003c9, m\u003e=0.",
				"Asymptotic behavior:",
				"a(n) = n/8 + O(log(n)),",
				"a(n+1) - a(n) = O(log(n)); this follows from the inequalities below.",
				"a(n) \u003c= (n-1)/8; equality holds for powers of 9.",
				"a(n) \u003e= (n-8)/8 - floor(log_9(n)); equality holds for n=9^m-1, m\u003e0.",
				"lim inf (n/8 - a(n)) =1/8, for n--\u003eoo.",
				"lim sup (n/8 - log_9(n) - a(n)) = 0, for n--\u003eoo.",
				"lim sup (a(n+1) - a(n) - log_9(n)) = 0, for n--\u003eoo.",
				"G.f.: g(x) = sum{k\u003e0, x^(9^k)/(1-x^(9^k))}/(1-x). (End)"
			],
			"example": [
				"a(100)=12.",
				"a(10^3)=124.",
				"a(10^4)=1248.",
				"a(10^5)=12498.",
				"a(10^6)=124996.",
				"a(10^7)=1249997.",
				"a(10^8)=12499996.",
				"a(10^9)=124999997."
			],
			"mathematica": [
				"Table[t = 0; p = 9; While[s = Floor[n/p]; t = t + s; s \u003e 0, p *= 9]; t, {n, 0, 100} ]"
			],
			"xref": [
				"Cf. A011371 and A054861 for analogs involving powers of 2 and 3.",
				"Cf. A054899, A067080, A098844, A132033."
			],
			"keyword": "nonn",
			"offset": "0,19",
			"author": "_Henry Bottomley_, May 23 2000",
			"ext": [
				"Examples added by _Hieronymus Fischer_, Jun 06 2012"
			],
			"references": 4,
			"revision": 16,
			"time": "2021-07-08T09:35:41-04:00",
			"created": "2000-06-15T03:00:00-04:00"
		}
	]
}