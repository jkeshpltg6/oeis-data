{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A008407",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 8407,
			"data": "0,2,6,8,12,16,20,26,30,32,36,42,48,50,56,60,66,70,76,80,84,90,94,100,110,114,120,126,130,136,140,146,152,156,158,162,168,176,182,186,188,196,200,210,212,216,226,236,240,246,252,254,264,270,272,278",
			"name": "Minimal difference s(n) between beginning and end of n consecutive large primes (n-tuplet) permitted by divisibility considerations.",
			"comment": [
				"Tony Forbes defines a prime k-tuplet (distinguished from a prime k-tuple) to be a maximally possible dense cluster of primes (a prime constellation) which will necessarily involve consecutive primes whereas a prime k-tuple is a prime cluster which may not necessarily be of maximum possible density (in which case the primes are not necessarily consecutive.)",
				"a(n) \u003e\u003e n log log n; in particular, for any eps \u003e 0, there is an N such that a(n) \u003e (e^gamma - eps) n log log n for all n \u003e N. Probably N can be chosen as 1; the actual rate of growth is larger. Can a larger growth rate be established? Perhaps a(n) ~ n log n. - _Charles R Greathouse IV_, Apr 19 2012",
				"Conjecture: (i) The sequence a(n)^(1/n) (n=3,4,...) is strictly decreasing (to the limit 1). (ii) We have 0 \u003c a(n)/n - H_n \u003c (gamma + 2)/(log n) for all n \u003e 4, where H_n denotes the harmonic number 1+1/2+1/3+...+1/n, and gamma refers to the Euler constant 0.5772... [The second inequality has been verified for n = 5, 6, ..., 5000.] - _Zhi-Wei Sun_, Jun 28 2013.",
				"Conjecture: For any integer n \u003e 2, there is 1 \u003c k \u003c n such that 2*n - a(k)- 1 and 2*n - a(k) + 1 are twin primes. Also, every n = 3, 4, ... can be written as p + a(k)/2 with p a prime and k an integer greater than one. - _Zhi-Wei Sun_, Jun 29-30 2013.",
				"The number of configurations that realize this minimal diameter, is A083409(n). - _Jeppe Stig Nielsen_, Jul 26 2018"
			],
			"reference": [
				"R. K. Guy, \"Unsolved Problems in Number Theory\", lists a number of relevant papers in Section A8.",
				"John Leech, \"Groups of primes having maximum density\", Math. Tables Aids to Comput., 12 (1958) 144-145."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A008407/b008407.txt\"\u003eTable of n, a(n) for n = 1..672\u003c/a\u003e (from Engelsma's data)",
				"Thomas J. Engelsma, \u003ca href=\"http://www.opertech.com/primes/k-tuples.html\"\u003ePermissible Patterns\u003c/a\u003e",
				"Tony Forbes, \u003ca href=\"http://anthony.d.forbes.googlepages.com/ktuplets.htm\"\u003e k-tuplets\u003c/a\u003e",
				"G. H. Hardy and J. E. Littlewood, \u003ca href=\"http://dx.doi.org/10.1007/BF02403921\"\u003eSome problems of 'partitio numerorum'; III: on the expression of a number as a sum of primes\u003c/a\u003e, Acta Mathematica, Vol. 44, pp. 1-70, 1923. See final section.",
				"A. V. Sutherland, \u003ca href=\"http://math.mit.edu/~primegaps\"\u003eNarrow admissible k-tuples: bounds on H(k)\u003c/a\u003e, 2013.",
				"T. Tao, \u003ca href=\"http://michaelnielsen.org/polymath1/index.php?title=Bounded_gaps_between_primes\"\u003eBounded gaps between primes\u003c/a\u003e, PolyMath Wiki Project, 2013.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PrimeConstellation.html\"\u003ePrime Constellation.\u003c/a\u003e"
			],
			"formula": [
				"s(k), k \u003e= 2, is smallest s such that there exist B = {b_1, b_2, ..., b_k} with s = b_k - b_1 and such that for all primes p \u003c= k, not all residues modulo p are represented by B."
			],
			"xref": [
				"Equals A020497 - 1.",
				"Cf. A083409."
			],
			"keyword": "nonn,nice",
			"offset": "1,2",
			"author": "T. Forbes (anthony.d.forbes(AT)googlemail.com)",
			"ext": [
				"Correction from Pat Weidhaas (weidhaas(AT)wotan.llnl.gov), Jun 15 1997",
				"Edited by _Daniel Forgues_, Aug 13 2009",
				"a(1)=0 prepended by _Max Alekseyev_, Aug 14 2015"
			],
			"references": 29,
			"revision": 72,
			"time": "2021-11-01T11:55:17-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}