{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A108561",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 108561,
			"data": "1,1,-1,1,0,1,1,1,1,-1,1,2,2,0,1,1,3,4,2,1,-1,1,4,7,6,3,0,1,1,5,11,13,9,3,1,-1,1,6,16,24,22,12,4,0,1,1,7,22,40,46,34,16,4,1,-1,1,8,29,62,86,80,50,20,5,0,1,1,9,37,91,148,166,130,70,25,5,1,-1,1,10,46,128,239,314,296,200,95,30,6,0,1,1,11,56,174,367",
			"name": "Triangle read by rows: T(n,0)=1, T(n,n)=(-1)^n, T(n+1,k)=T(n,k-1)+T(n,k) for 0 \u003c k \u003c n.",
			"comment": [
				"Sum_{k=0..n} T(n,k) = A078008(n);",
				"Sum_{k=0..n} abs(T(n,k) = A052953(n-1) for n \u003e 0;",
				"T(n,1) = n - 2 for n \u003e 1;",
				"T(n,2) = A000124(n-3) for n \u003e 2;",
				"T(n,3) = A003600(n-4) for n \u003e 4;",
				"T(n,n-6) = A001753(n-6) for n \u003e 6;",
				"T(n,n-5) = A001752(n-5) for n \u003e 5;",
				"T(n,n-4) = A002623(n-4) for n \u003e 4;",
				"T(n,n-3) = A002620(n-1) for n \u003e 3;",
				"T(n,n-2) = A008619(n-2) for n \u003e 2;",
				"T(n,n-1) = n mod 2 for n \u003e 0;",
				"T(2*n,n) = A072547(n+1).",
				"Sum_{k=0..n} T(n,k)*x^k = A232015(n), A078008(n), A000012(n), A040000(n), A001045(n+2), A140725(n+1) for x = 2, 1, 0, -1, -2, -3 respectively. - _Philippe Deléham_, Nov 17 2013, Nov 19 2013",
				"(1,a^n) Pascal triangle with a = -1. - _Philippe Deléham_, Dec 27 2013",
				"T(n,k) = A112465(n,n-k). - _Reinhard Zumkeller_, Jan 03 2014"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A108561/b108561.txt\"\u003eRows n = 0..125 of triangle, flattened\u003c/a\u003e",
				"C. Merino, S. D. Noble, M. Ramirez-Ibanez, R. Villarroel-Flores, \u003ca href=\"https://doi.org/10.1016/j.ejc.2012.04.002\"\u003eOn the structure of the h-vector of a paving matroid\u003c/a\u003e, Eur. J. Comb. 33, No. 8, 1787-1799 (2012).",
				"\u003ca href=\"/index/Pas#Pascal\"\u003eIndex entries for triangles and arrays related to Pascal's triangle\u003c/a\u003e"
			],
			"formula": [
				"G.f.: (1-y*x)/(1-x-(y+y^2)*x). - _Philippe Deléham_, Nov 17 2013",
				"T(n,k) = T(n-1,k) + T(n-2,k-1) + T(n-2,k-2), T(0,0)=T(1,0)=1, T(1,1)=-1, T(n,k)=0 if k \u003c 0 or if k \u003e n. - _Philippe Deléham_, Nov 17 2013",
				"From _Peter Bala_, Feb 18 2018: (Start)",
				"T(n,k) = Sum_{i = 0..k} binomial(n,i)*(-2)^(k-i), 0 \u003c= k \u003c= n.",
				"The n-th row polynomial is the n-th degree Taylor polynomial of the rational function (1 + x)^n/(1 + 2*x) about 0. For example, for n = 4, (1 + x)^4/(1 + 2*x) = 1 + 2*x + 2*x^2 + x^4 + O(x^5). (End)"
			],
			"example": [
				"From _Philippe Deléham_, Nov 17 2013: (Start)",
				"Triangle begins:",
				"  1;",
				"  1, -1;",
				"  1,  0,  1;",
				"  1,  1,  1, -1;",
				"  1,  2,  2,  0,  1;",
				"  1,  3,  4,  2,  1, -1;",
				"  1,  4,  7,  6,  3,  0,  1; (End)"
			],
			"maple": [
				"A108561 := (n, k) -\u003e add(binomial(n, i)*(-2)^(k-i), i = 0..k):",
				"seq(seq(A108561(n,k), k = 0..n), n = 0..12); # _Peter Bala_, Feb 18 2018"
			],
			"mathematica": [
				"Clear[t]; t[n_, 0] = 1; t[n_, n_] := t[n, n] = (-1)^Mod[n, 2]; t[n_, k_] := t[n, k] = t[n-1, k] + t[n-1, k-1]; Table[t[n, k], {n, 0, 13}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Mar 06 2013 *)"
			],
			"program": [
				"(Haskell)",
				"a108561 n k = a108561_tabl !! n !! k",
				"a108561_row n = a108561_tabl !! n",
				"a108561_tabl = map reverse a112465_tabl",
				"-- _Reinhard Zumkeller_, Jan 03 2014",
				"(Sage)",
				"def A108561_row(n):",
				"    @cached_function",
				"    def prec(n, k):",
				"        if k==n: return 1",
				"        if k==0: return 0",
				"        return -prec(n-1,k-1)-sum(prec(n,k+i-1) for i in (2..n-k+1))",
				"    return [(-1)^k*prec(n, k) for k in (1..n-1)]+[(-1)^(n+1)]",
				"for n in (1..12): print(A108561_row(n)) # _Peter Luschny_, Mar 16 2016",
				"(GAP) Flat(List([0..13],n-\u003eList([0..n],k-\u003eSum([0..k],i-\u003eBinomial(n,i)*(-2)^(k-i))))); # _Muniru A Asiru_, Feb 19 2018"
			],
			"xref": [
				"Cf. A007318 (a=1), A008949(a=2), A164844(a=10).",
				"Similar to the triangles A035317, A059259, A080242, A112555.",
				"Cf. A228196",
				"Cf. A072547 (central terms)."
			],
			"keyword": "sign,tabl",
			"offset": "0,12",
			"author": "_Reinhard Zumkeller_, Jun 10 2005",
			"ext": [
				"Definition corrected by _Philippe Deléham_, Dec 26 2013"
			],
			"references": 23,
			"revision": 43,
			"time": "2020-02-26T13:47:53-05:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}