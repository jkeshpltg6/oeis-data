{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A218991",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 218991,
			"data": "6,36,221,1361,8386,51676,318441,1962321,12092366,74516516,459191461,2829665281,17437183146,107452764156,662153768081,4080375372641,25144406003926,154946811396196,954825274381101,5883898457682801",
			"name": "Power floor sequence of 3+sqrt(10).",
			"comment": [
				"See A214992 for a discussion of power floor sequence and the power floor function, p1(x) = limit of a(n,x)/x^n.  The present sequence is a(n,r), where r = 3+sqrt(10), and the limit p1(r) = 5.815421188487681054332319082...",
				"See A218992 for the power floor function, p4.  For comparison with p1, we have limit(p4(r)/p1(r) = (3+sqrt(10))/5 = 1.23245553...."
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A218991/b218991.txt\"\u003eTable of n, a(n) for n = 0..250\u003c/a\u003e",
				"\u003ca href=\"/index/Rec\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (7,-5,-1)."
			],
			"formula": [
				"a(n) = floor(r*a(n-1)), where r=3+sqrt(10), a(0) = floor(r).",
				"a(n) = 7*a(n-1) - 5*a(n-2) - a(n-3).",
				"G.f.:  (6 - 6*x - x^2)/(1 - 7*x + 5*x^2 + x^3).",
				"a(n) = ((5+sqrt(10))*(3-sqrt(10))^(n+2)+(5-sqrt(10))*(3+sqrt(10))^(n+2)+2)/12. [_Bruno Berselli_, Nov 22 2012]"
			],
			"example": [
				"a(0) = floor(r) = 6, where r = 3+sqrt(10);",
				"a(1) = floor(6*r) = 36;",
				"a(2) = floor(36*r) = 221."
			],
			"mathematica": [
				"x = 3 + Sqrt[10]; z = 30; (* z = # terms in sequences *)",
				"f[x_] := Floor[x]; c[x_] := Ceiling[x];",
				"p1[0] = f[x]; p2[0] = f[x]; p3[0] = c[x]; p4[0] = c[x];",
				"p1[n_] := f[x*p1[n - 1]]",
				"p2[n_] := If[Mod[n, 2] == 1, c[x*p2[n - 1]], f[x*p2[n - 1]]]",
				"p3[n_] := If[Mod[n, 2] == 1, f[x*p3[n - 1]], c[x*p3[n - 1]]]",
				"p4[n_] := c[x*p4[n - 1]]",
				"t1 = Table[p1[n], {n, 0, z}]  (* A218991 *)",
				"t2 = Table[p2[n], {n, 0, z}]  (* A005668 *)",
				"t3 = Table[p3[n], {n, 0, z}]  (* A015451 *)",
				"t4 = Table[p4[n], {n, 0, z}]  (* A218992 *)"
			],
			"program": [
				"(MAGMA) [IsZero(n) select Floor(r) else Floor(r*Self(n)) where r is 3+Sqrt(10): n in [0..20]]; // _Bruno Berselli_, Nov 22 2012"
			],
			"xref": [
				"Cf. A214992, A005668, A015451, A218992."
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "_Clark Kimberling_, Nov 12 2012",
			"references": 3,
			"revision": 8,
			"time": "2015-06-13T00:54:35-04:00",
			"created": "2012-11-22T06:19:52-05:00"
		}
	]
}