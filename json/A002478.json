{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002478",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2478,
			"id": "M2572 N1017",
			"data": "1,1,3,6,13,28,60,129,277,595,1278,2745,5896,12664,27201,58425,125491,269542,578949,1243524,2670964,5736961,12322413,26467299,56849086,122106097,262271568,563332848,1209982081,2598919345,5582216355,11990037126,25753389181",
			"name": "Bisection of A000930.",
			"comment": [
				"Number of ways to tile a 3 X n region with 1 X 1, 2 X 2 and 3 X 3 tiles.",
				"Number of ternary words with subwords (0,0), (0,1) and (1,1) not allowed. - _Olivier Gérard_, Aug 28 2012",
				"Diagonal sums of A063967. - _Paul Barry_, Nov 09 2005",
				"Row sums of number triangle A116088. - _Paul Barry_, Feb 04 2006",
				"Sequence is identical to its second differences negated, minus the first 3 terms. - _Paul Curtz_, Feb 10 2008",
				"a(n) = term (3,3) in the 3 X 3 matrix [0,1,0; 0,0,1; 1,2,1]^n. - _Gary W. Adamson_, May 30 2008",
				"a(n)/a(n-1) tends to 2.147899035..., an eigenvalue of the matrix and a root to x^3 - x^2 - 2x - 1 = 0. - _Gary W. Adamson_, May 30 2008",
				"INVERT transform of (1, 2, 1, 0, 0, 0, ...) = (1, 3, 6, 13, 28, ...); such that (1, 2, 1, 0, 0, 0, ...) convolved with (1, 1, 3, 6, 13, 28, 0, 0, 0, ...) shifts to the left. - _Gary W. Adamson_, Apr 18 2010",
				"a(n) is the top left entry in the n-th power of the 3 X 3 matrix [1, 1, 1; 1, 0, 1; 1, 0, 0] or of the 3 X 3 matrix [1, 1, 1; 1, 0, 0; 1, 1, 0]. - _R. J. Mathar_, Feb 03 2014"
			],
			"reference": [
				"Kenneth Edwards, Michael A. Allen, A new combinatorial interpretation of the Fibonacci numbers squared, Part II, Fib. Q., 58:2 (2020), 169-177.",
				"L. Euler, (E388) Vollstaendige Anleitung zur Algebra, Zweiter Theil, reprinted in: Opera Omnia. Teubner, Leipzig, 1911, Series (1), Vol. 1, p. 322.",
				"S. Heubach, Tiling an m X n Area with Squares of Size up to k X k (m\u003c=5), Congressus Numerantium 140 (1999), pp. 43-64.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A002478/b002478.txt\"\u003eTable of n, a(n) for n = 0..300\u003c/a\u003e",
				"Paul Barry, \u003ca href=\"https://arxiv.org/abs/2104.01644\"\u003eCentered polygon numbers, heptagons and nonagons, and the Robbins numbers\u003c/a\u003e, arXiv:2104.01644 [math.CO], 2021.",
				"E. Deutsch, \u003ca href=\"http://www.jstor.org/stable/3647950\"\u003eCounting tilings with L-tiles and squares\u003c/a\u003e, Problem 10877, Amer. Math. Monthly, 110 (March 2003), 245-246.",
				"Kenneth Edwards, Michael A. Allen, \u003ca href=\"https://arxiv.org/abs/1907.06517\"\u003eA new combinatorial interpretation of the Fibonacci numbers squared\u003c/a\u003e, arXiv:1907.06517 [math.CO], 2019.",
				"L. Euler, \u003ca href=\"http://www.mathematik.uni-bielefeld.de/~sieben/euler/euler_2.djvu\"\u003eVollstaendige Anleitung zur Algebra, Zweiter Teil\u003c/a\u003e.",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=412\"\u003eEncyclopedia of Combinatorial Structures 412\u003c/a\u003e",
				"Milan Janjic, \u003ca href=\"https://arxiv.org/abs/1705.02497\"\u003ePascal Triangle and Restricted Words\u003c/a\u003e, arXiv:1705.02497 [math.CO], 2017.",
				"Milan Janjić, \u003ca href=\"https://www.emis.de/journals/JIS/VOL21/Janjic2/janjic103.html\"\u003ePascal Matrices and Restricted Words\u003c/a\u003e, J. Int. Seq., Vol. 21 (2018), Article 18.5.2.",
				"R. J. Mathar, \u003ca href=\"http://arxiv.org/abs/1311.6135\"\u003ePaving Rectangular Regions with Rectangular Tiles: Tatami and Non-Tatami Tilings\u003c/a\u003e, arXiv:1311.6135 [math.CO], 2013, Table 19 (halved...).",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"Murray Tannock, \u003ca href=\"https://skemman.is/bitstream/1946/25589/1/msc-tannock-2016.pdf\"\u003eEquivalence classes of mesh patterns with a dominating pattern\u003c/a\u003e, MSc Thesis, Reykjavik Univ., May 2016. See Appendix B2.",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,2,1)."
			],
			"formula": [
				"G.f.: 1 / (1-x-2*x^2-x^3). [_Simon Plouffe_ in his 1992 dissertation.]",
				"a(n) = a(n-1) + 2*a(n-2) + a(n-3).",
				"a(n) = Sum_{k=0..n} binomial(2*n-2*k, k). - _Paul Barry_, Nov 13 2004",
				"a(n) = Sum_{k=0..floor(n/2)} Sum_{j=0..n-k} C(j, n-k-j)*C(j, k). - _Paul Barry_, Nov 09 2005",
				"a(n) = Sum_{k=0..n} C(2*k,n-k) = Sum_{k=0..n} C(n,k)*C(3*k,n)/C(3*k,k). - _Paul Barry_, Feb 04 2006",
				"a(n) = A000930(n) + 2*Sum_{i=0..n-2} a(i)*A000930(n-2-i). - _Michael Tulskikh_, Jun 07 2020"
			],
			"example": [
				"a(3)=6 as there is one tiling of a 3 X 3 region with only 1 X 1 tiles, 4 tilings with exactly one 2 X 2 tile and 1 tiling consisting of the 3 X 3 tile."
			],
			"mathematica": [
				"f[ A_ ] := Module[ {til = A}, AppendTo[ til, A[ [ -1 ] ] + 2A[ [ -2 ] ] + A[ [ -3 ] ] ] ]; NumOfTilings[ n_ ] := Nest[ f, {1, 1, 3}, n - 2 ]; NumOfTilings[ 30 ]",
				"LinearRecurrence[{1,2,1},{1,1,3},40] (* _Vladimir Joseph Stephan Orlovsky_, Jan 28 2012 *)"
			],
			"program": [
				"(PARI) a(n)=([0,1,0; 0,0,1; 1,2,1]^n*[1;1;3])[1,1] \\\\ _Charles R Greathouse IV_, Apr 08 2016"
			],
			"xref": [
				"Cf. A000930, A054856, A054857, A025234, A078007, A078039, A226546, A077936 (INVERT transform), A008346 (inverse INVERT transform)."
			],
			"keyword": "easy,nonn,nice",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Additional comments from Silvia Heubach (silvi(AT)cine.net), Apr 21 2000"
			],
			"references": 39,
			"revision": 113,
			"time": "2021-06-28T15:39:34-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}