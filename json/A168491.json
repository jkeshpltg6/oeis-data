{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A168491",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 168491,
			"data": "1,-1,2,-5,14,-42,132,-429,1430,-4862,16796,-58786,208012,-742900,2674440,-9694845,35357670,-129644790,477638700,-1767263190,6564120420,-24466267020,91482563640,-343059613650,1289904147324,-4861946401452,18367353072152,-69533550916004",
			"name": "a(n) = (-1)^n*Catalan(n).",
			"comment": [
				"Second inverse binomial transform of A001405. Hankel transform of this sequence gives A000012 = [1,1,1,1,1,1,1,...].",
				"Also the expansion of real root of y+y^2=x, With offset 1, series reversion of x+x^2. - _Robert G. Wilson v_, Mar 07 2011"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A168491/b168491.txt\"\u003eTable of n, a(n) for n = 0..500\u003c/a\u003e",
				"\u003ca href=\"/index/Res#revert\"\u003eIndex to sequences related to reversion of series\u003c/a\u003e"
			],
			"formula": [
				"a(n) = (-1)^n * A000108(n).",
				"G.f.: (sqrt(1+4*x) - 1) / (2*x) = 2 / (sqrt(1+4*x) + 1).",
				"E.g.f.: exp(-2*x)*(BesselI(0, 2*x) + BesselI(1, 2*x)). - _Peter Luschny_, Aug 26 2012",
				"(n+1)*a(n) +2*(2*n - 1)*a(n-1) = 0. - _R. J. Mathar_, Oct 06 2012",
				"G.f.: 1 / (1 + x / (1 + x / (1 + x / ...))). - _Michael Somos_, Jan 03 2013",
				"G.f.: 1/(x*Q(0)) - 1/x, where Q(k)= 1 - (4*k+1)*x/(k+1 - x*(2*k+2)*(4*k+3)/(2*x*(4*k+3) - (2*k+3)/Q(k+1))); (continued fraction). - _Sergei N. Gladkovskii_, May 15 2013",
				"G.f.: G(0)/(2*x) - 1/(2*x), where G(k)= 1 + 4*x*(4*k+1)/( (4*k+2)*(1+4*x) - 2*x*(1+4*x)*(2*k+1)*(4*k+3)/(x*(4*k+3) + (1+4*x)*(k+1)/G(k+1))); (continued fraction). - _Sergei N. Gladkovskii_, Jun 24 2013",
				"G.f.: G(0)/x - 1/x, where G(k)= k+1 - 2*x*(2*k+1) + 2*x*(k+1)*(2*k+3)/G(k+1) ; (continued fraction). - _Sergei N. Gladkovskii_, Jul 14 2013"
			],
			"example": [
				"G.f. = 1 - x + 2*x^2 - 5*x^3 + 14*x^4 - 42*x^5 + 132*x^6 - 429*x^7 + ..."
			],
			"mathematica": [
				"CoefficientList[InverseSeries[Series[y + y^2, {y, 0, 28}], x]/x, x] (* _Robert G. Wilson v_, Mar 07 2011 *)",
				"a[ n_] := If[ n \u003c 0, 0, (-1)^n CatalanNumber[n]]; (* _Michael Somos_, Nov 22 2014 *)",
				"Table[(-1)^n*CatalanNumber[n], {n, 0, 50] (* _G. C. Greubel_, Jul 23 2016 *)"
			],
			"program": [
				"(PARI) a(n)=(-1)^n*binomial(2*n,n)/(n+1); \\\\ _Joerg Arndt_, May 15 2013",
				"(MAGMA) [(-1)^n*Catalan(n): n in [0..40]]; // _Vincenzo Librandi_, Nov 16 2014"
			],
			"keyword": "sign,less",
			"offset": "0,3",
			"author": "_Philippe Deléham_, Nov 27 2009",
			"references": 11,
			"revision": 55,
			"time": "2019-02-27T19:56:28-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}