{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002379",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2379,
			"id": "M0666 N0245",
			"data": "1,1,2,3,5,7,11,17,25,38,57,86,129,194,291,437,656,985,1477,2216,3325,4987,7481,11222,16834,25251,37876,56815,85222,127834,191751,287626,431439,647159,970739,1456109,2184164,3276246,4914369,7371554,11057332",
			"name": "a(n) = floor(3^n / 2^n).",
			"comment": [
				"It is an important unsolved problem related to Waring's problem to show that a(n) = floor((3^n-1)/(2^n-1)) holds for all n \u003e 1. This has been checked for 10000 terms and is true for all sufficiently large n, by a theorem of Mahler. [Lichiardopol]",
				"a(n) = floor((3^n-1)/(2^n-1)) holds true at least for 2 \u003c= n \u003c= 305000. - _Hieronymus Fischer_, Dec 31 2008",
				"a(n) is also the curve length (rounded down) of the Sierpiński arrowhead curve after n iterations, let a(0) = 1. - _Kival Ngaokrajang_, May 21 2014",
				"a(n) is composite infinitely often (Forman and Shapiro). More exactly, a(n) is divisible by at least one of 2, 5, 7 or 11 infinitely often (Dubickas and Novikas). - _Tomohiro Yamada_, Apr 15 2017"
			],
			"reference": [
				"R. K. Guy, Unsolved Problems in Number Theory, E19.",
				"D. H. Lehmer, Guide to Tables in the Theory of Numbers. Bulletin No. 105, National Research Council, Washington, DC, 1941, p. 82.",
				"S. S. Pillai, On Waring's problem, J. Indian Math. Soc., 2 (1936), 16-44.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A002379/b002379.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Arturas Dubickas, Aivaras Novikas, \u003ca href=\"https://doi.org/10.1007/s00209-005-0827-4\"\u003eInteger parts of powers of rational numbers\u003c/a\u003e, Math. Z. 251 (2005), 635--648, available from \u003ca href=\"http://www.mif.vu.lt/~dubickas/files/dvifai/suaivaru.pdf\"\u003ethe first author's page\u003c/a\u003e.",
				"W. Forman and H. N. Shapiro, \u003ca href=\"https://doi.org/10.1002/cpa.3160200305\"\u003eAn arithmetic property of certain rational powers\u003c/a\u003e, Comm. Pure. Appl. Math. 20 (1967), 561-573.",
				"R. K. Guy, \u003ca href=\"http://www.jstor.org/stable/2691503\"\u003eThe Second Strong Law of Small Numbers\u003c/a\u003e, Math. Mag, 63 (1990), no. 1, 3-20.",
				"R. K. Guy, \u003ca href=\"/A005347/a005347.pdf\"\u003eThe Second Strong Law of Small Numbers\u003c/a\u003e, Math. Mag, 63 (1990), no. 1, 3-20. [Annotated scanned copy]",
				"N. Lichiardopol, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2007.07.021\"\u003eProblem 925 (BCC20.19)\u003c/a\u003e, A number-theoretic problem, in Research Problems from the 20th British Combinatorial Conference, Discrete Math., 308 (2008), 621-630.",
				"K. Mahler, \u003ca href=\"http://dx.doi.org/10.1112/S0025579300001170\"\u003eOn the fractional parts of the powers of a rational number, II\u003c/a\u003e, Mathematika 4 (1957), 122-124.",
				"Kival Ngaokrajang, \u003ca href=\"/A002379/a002379_1.pdf\"\u003eIllustration of Sierpinski arrowhead curve for n = 0..5\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PowerFloors.html\"\u003ePower Floors\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Sierpi%C5%84ski_arrowhead_curve\"\u003eSierpiński arrowhead curve\u003c/a\u003e"
			],
			"formula": [
				"a(n) = b(n) - (-2/3)^n where b(n) is defined by the recursion b(0):=2, b(1):=5/6, b(n+1):=(5/6)*b(n) + b(n-1). - _Hieronymus Fischer_, Dec 31 2008",
				"a(n) = (1/2)*(b(n) + sqrt(b(n)^2 - (-4)^n)) (with b(n) as defined above). - _Hieronymus Fischer_, Dec 31 2008",
				"3^n = a(n)*2^n + A002380(n). - _R. J. Mathar_, Oct 26 2012",
				"a(n) = -(1/2) + (3/2)^n + arctan(cot((3/2)^n Pi)) / Pi. - _Fred Daniel Kline_, Apr 14 2018",
				"a(n+1) = round( -(1/2) + (3^n-1)/(2^n-1) ). - _Fred Daniel Kline_, Apr 14 2018"
			],
			"maple": [
				"A002379:=n-\u003efloor(3^n/2^n); seq(A002379(k), k=0..100); # _Wesley Ivan Hurt_, Oct 29 2013"
			],
			"mathematica": [
				"Table[Floor[(3/2)^n], {n, 0, 40}] (* _Robert G. Wilson v_, May 11 2004 *)",
				"x[n_] := -(1/2) + (3/2)^n + ArcTan[Cot[(3/2)^n Pi]]/Pi; Array[x, 40] (* _Fred Daniel Kline_, Dec 21 2017 *)",
				"x[n_]:=Round[-(1/2) + (3^n - 1)/(2^n - 1)]; Array[x, 39, 2] (* offset n+1, _Fred Daniel Kline_, Apr 13 2018 *)"
			],
			"program": [
				"(PARI) a(n)=3^n\u003e\u003en \\\\ _Charles R Greathouse IV_, Jun 10 2011",
				"(MAGMA) [Floor(3^n / 2^n): n in [0..40]]; // _Vincenzo Librandi_, Sep 08 2011",
				"(Maxima) makelist(floor(3^n/2^n), n, 0, 50); /* _Martin Ettl_, Oct 17 2012 */",
				"(Haskell)",
				"a002379 n = 3^n `div` 2^n  -- _Reinhard Zumkeller_, Jul 11 2014"
			],
			"xref": [
				"Cf. A094969-A094500, A000217, A081464, A153662, A153665, A153666.",
				"Cf. A060692, A002380, A000079, A000244.",
				"Cf. A046037, A070758, A070759, A067904 (Composites and Primes).",
				"Cf. A064628 (an analog for 4/3)."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Robert G. Wilson v_, May 11 2004"
			],
			"references": 88,
			"revision": 122,
			"time": "2018-04-21T17:35:22-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}