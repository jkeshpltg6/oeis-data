{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A155537",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 155537,
			"data": "3,5,5,9,27,9,17,102,102,17,33,330,660,330,33,65,975,3250,3250,975,65,129,2709,13545,22575,13545,2709,129,257,7196,50372,125930,125930,50372,7196,257,513,18468,172368,603288,904932,603288,172368,18468,513",
			"name": "Triangle T(n,k,p,q) = (p^n + q^n)*A001263(n, k) with p=2 and q=1, read by rows.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A155537/b155537.txt\"\u003eRows n = 1..50 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"Define T(n,k,p,q) = (p^n + q^n)*binomial(n-1, k-1)*binomial(n, k)/(n-k+1) (A scaled Narayana triangle) for various p and q. When p = 2 and q = 1 this sequence is obtained.",
				"From _G. C. Greubel_, Mar 15 2021: (Start)",
				"T(n,k,p,q) = T(n,k,q,p) = (p^n + q^n)*A001263(n, k).",
				"T(n,k,2,1) = A000051(n) * A001263(n,k).",
				"Sum_{k=1..n} T(n,k,p,q) = (p^n + q^n)*C(n), where C(n) are the Catalan numbers (A000108). (End)"
			],
			"example": [
				"Triangle begins as:",
				"     3;",
				"     5,     5;",
				"     9,    27,      9;",
				"    17,   102,    102,      17;",
				"    33,   330,    660,     330,      33;",
				"    65,   975,   3250,    3250,     975,      65;",
				"   129,  2709,  13545,   22575,   13545,    2709,     129;",
				"   257,  7196,  50372,  125930,  125930,   50372,    7196,    257;",
				"   513, 18468, 172368,  603288,  904932,  603288,  172368,  18468,   513;",
				"  1025, 46125, 553500, 2583000, 5424300, 5424300, 2583000, 553500, 46125, 1025;"
			],
			"maple": [
				"A155537:= (n,k,p,q)-\u003e (p^n + q^n)*binomial(n-1, k-1)*binomial(n, k)/(n-k+1);",
				"seq(seq(A155537(n,k,2,1), k=1..n), n=1..12); # _G. C. Greubel_, Mar 15 2021"
			],
			"mathematica": [
				"T[n_, k_, p_, q_]:= T[n,k,p,q]= (p^n + q^n)*Binomial[n-1, k-1]*Binomial[n, k]/(n-k+1);",
				"Table[T[n,k,2,1], {n, 12}, {k, n}]//Flatten (* modified by _G. C. Greubel_, Mar 15 2021 *)"
			],
			"program": [
				"(Sage)",
				"def T(n,k,p,q): return (p^n + q^n)*binomial(n-1, k-1)*binomial(n, k)/(n-k+1)",
				"flatten([[T(n,k,2,1) for k in (1..n)] for n in (1..12)]) # _G. C. Greubel_, Mar 15 2021",
				"(Magma)",
				"T:= func\u003c n,k,p,q | (p^n + q^n)*Binomial(n-1, k-1)*Binomial(n, k)/(n-k+1) \u003e;",
				"[T(n,k,2,1): k in [1..n], n in [1..12]]; // _G. C. Greubel_, Mar 15 2021"
			],
			"xref": [
				"Cf. A000051, A000108, A001263."
			],
			"keyword": "nonn,tabl",
			"offset": "1,1",
			"author": "_Roger L. Bagula_, Jan 23 2009",
			"ext": [
				"Edited by _G. C. Greubel_, Mar 15 2021"
			],
			"references": 1,
			"revision": 5,
			"time": "2021-03-15T21:30:45-04:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}