{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A324225",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 324225,
			"data": "1,1,2,1,2,4,6,4,2,6,12,18,24,18,12,6,24,48,72,96,120,96,72,48,24,120,240,360,480,600,720,600,480,360,240,120,720,1440,2160,2880,3600,4320,5040,4320,3600,2880,2160,1440,720,5040,10080,15120,20160,25200,30240,35280,40320,35280,30240,25200,20160,15120,10080,5040",
			"name": "Total number T(n,k) of 1's in falling diagonals with index k in all n X n permutation matrices; triangle T(n,k), n\u003e=1, 1-n\u003c=k\u003c=n-1, read by rows.",
			"comment": [
				"T(n,k) is the number of occurrences of k in all (signed) displacement lists [p(i)-i, i=1..n] of permutations p of [n]."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A324225/b324225.txt\"\u003eRows n = 1..100, flattened\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Permutation\"\u003ePermutation\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Permutation_matrix\"\u003ePermutation matrix\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = T(n,-k).",
				"T(n,k) = (n-t)*(n-1)! if t \u003c n with t = |k|, T(n,k) = 0 otherwise.",
				"T(n,k) = |k|! * A324224(n,k).",
				"E.g.f. of column k: x^t/t * hypergeom([2, t], [t+1], x) with t = |k|+1.",
				"|T(n,k)-T(n,k-1)| = (n-1)! for k = 1-n..n."
			],
			"example": [
				"The 6 permutations p of [3]: 123, 132, 213, 231, 312, 321 have (signed) displacement lists [p(i)-i, i=1..3]: [0,0,0], [0,1,-1], [1,-1,0], [1,1,-2], [2,-1,-1], [2,0,-2], representing the indices of falling diagonals of 1's in the permutation matrices",
				"  [1    ]  [1    ]  [  1  ]  [  1  ]  [    1]  [    1]",
				"  [  1  ]  [    1]  [1    ]  [    1]  [1    ]  [  1  ]",
				"  [    1]  [  1  ]  [    1]  [1    ]  [  1  ]  [1    ] , respectively. Indices -2 and 2 occur twice, -1 and 1 occur four times, and 0 occurs six times. So row n=3 is [2, 4, 6, 4, 2].",
				"Triangle T(n,k) begins:",
				"  :                             1                           ;",
				"  :                        1,   2,   1                      ;",
				"  :                   2,   4,   6,   4,   2                 ;",
				"  :              6,  12,  18,  24,  18,  12,   6            ;",
				"  :        24,  48,  72,  96, 120,  96,  72,  48,  24       ;",
				"  :  120, 240, 360, 480, 600, 720, 600, 480, 360, 240, 120  ;"
			],
			"maple": [
				"b:= proc(s, c) option remember; (n-\u003e `if`(n=0, c,",
				"      add(b(s minus {i}, c+x^(n-i)), i=s)))(nops(s))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=1-n..n-1))(b({$1..n}, 0)):",
				"seq(T(n), n=1..8);",
				"# second Maple program:",
				"egf:= k-\u003e (t-\u003e x^t/t*hypergeom([2, t], [t+1], x))(abs(k)+1):",
				"T:= (n, k)-\u003e n! * coeff(series(egf(k), x, n+1), x, n):",
				"seq(seq(T(n, k), k=1-n..n-1), n=1..8);",
				"# third Maple program:",
				"T:= (n, k)-\u003e (t-\u003e `if`(t\u003cn, (n-t)*(n-1)!, 0))(abs(k)):",
				"seq(seq(T(n, k), k=1-n..n-1), n=1..8);"
			],
			"mathematica": [
				"T[n_, k_] := With[{t = Abs[k]}, If[t\u003cn, (n-t)(n-1)!, 0]];",
				"Table[Table[T[n, k], {k, 1-n, n-1}], {n, 1, 8}] // Flatten (* _Jean-François Alcover_, Mar 25 2021, after 3rd Maple program *)"
			],
			"xref": [
				"Columns k=0-6 give (offsets may differ): A000142, A001563, A062119, A052571, A052520, A282822, A052521.",
				"Row sums give A001563.",
				"T(n+1,n) gives A000142.",
				"T(n+1,n-1) gives A052849.",
				"T(n+1,n-2) gives A052560 for n\u003e1.",
				"Cf. A152883 (right half of this triangle without center column), A162608 (left half of this triangle), A306461, A324224."
			],
			"keyword": "nonn,look,tabf",
			"offset": "1,3",
			"author": "_Alois P. Heinz_, Feb 18 2019",
			"references": 3,
			"revision": 39,
			"time": "2021-03-25T04:30:13-04:00",
			"created": "2019-02-19T18:08:12-05:00"
		}
	]
}