{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A298364",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 298364,
			"data": "2,3,1,4,6,7,5,8,10,11,9,12,14,15,13,16,18,19,17,20,22,23,21,24,26,27,25,28,30,31,29,32,34,35,33,36,38,39,37,40,42,43,41,44,46,47,45,48,50,51,49,52,54,55,53,56,58,59,57,60,62,63,61,64,66,67,65",
			"name": "Permutation of the natural numbers partitioned into quadruples [4k-2, 4k-1, 4k-3, 4k] for k \u003e 0.",
			"comment": [
				"Partition the natural number sequence into quadruples starting with (1,2,3,4); swap the first and second elements, then swap the second and third elements; repeat for all quadruples."
			],
			"link": [
				"Guenther Schrack, \u003ca href=\"/A298364/b298364.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,0,0,1,-1)."
			],
			"formula": [
				"O.g.f.: (3*x^3 - 2*x^2 + x + 2)/(x^5 - x^4 - x - 1).",
				"a(1) = 2, a(2) = 3, a(3) = 1, a(4) = 4, a(n) = a(n-4) + 4 for n \u003e 4.",
				"a(n) = n + ((-1)^n + ((-1)^(n*(n-1)/2))*(1 - 2*(-1)^n))/2.",
				"a(n) = n + (cos(n*Pi) - cos(n*Pi/2) + 3*sin(n*Pi/2))/2.",
				"a(n) = 2*floor((n+1)/2) - 4*floor((n+1)/4) + floor(n/2) + 2*floor(n/4).",
				"a(n) = n + (-1)^floor((n-1)^2/4)*A140081(n) for n \u003e 0.",
				"a(n) = A056699(n+1) - 1, n \u003e 0.",
				"a(n+2) = A168269(n+1) - a(n), n \u003e 0.",
				"a(n+2) = a(n) + (-1)^floor((n+1)^2/4)*A132400(n+2) for n \u003e 0.",
				"Linear recurrence: a(n) = a(n-1) + a(n-4) - a(n-5) for n \u003e 5.",
				"First differences: periodic, (1, -2, 3, 2) repeat.",
				"Compositions:",
				"  a(n) = A080412(A116966(n-1)) for n \u003e 0.",
				"  a(n) = A284307(A256008(n)) for n \u003e 0.",
				"  a(A067060(n)) = A133256(n) for n \u003e 0.",
				"  A116966(a(n+1)-1) = A092486(n) for n \u003e= 0.",
				"  A056699(a(n)) = A256008(n) for n \u003e 0."
			],
			"mathematica": [
				"Nest[Append[#, #[[-4]] + 4] \u0026, {2, 3, 1, 4}, 63] (* or *)",
				"Array[# + ((-1)^# + ((-1)^(# (# - 1)/2)) (1 - 2 (-1)^#))/2 \u0026, 67] (* _Michael De Vlieger_, Jan 23 2018 *)",
				"LinearRecurrence[{1,0,0,1,-1},{2,3,1,4,6},70] (* _Harvey P. Dale_, Dec 12 2018 *)"
			],
			"program": [
				"(MATLAB) a = [2 3 1 4];",
				"max = 10000;    % Generation of b-file.",
				"for n := 5:max",
				"   a(n) = a(n-4) + 4;",
				"end;",
				"(PARI) for(n=1, 100, print1(n + ((-1)^n + ((-1)^(n*(n-1)/2))*(1 - 2*(-1)^n))/2, \", \"))"
			],
			"xref": [
				"Inverse: A292576.",
				"Sequence of fixed points: A008586(n) for n \u003e 0.",
				"First differences: (-1)^floor(n^2/4)*A068073(n-1) for n \u003e 0.",
				"Subsequences:",
				"  elements with odd index: A042963(A103889(n)) for n \u003e 0.",
				"  elements with even index A014601(n) for n \u003e 0.",
				"  odd elements: A166519(n-1) for n \u003e 0.",
				"  indices of odd elements: A042964(n) for n \u003e 0.",
				"  even elements: A005843(n) for n \u003e 0.",
				"  indices of even elements: A042948(n) for n \u003e 0.",
				"Other similar permutations: A116966, A284307, A292576."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_Guenther Schrack_, Jan 18 2018",
			"references": 2,
			"revision": 37,
			"time": "2018-12-12T13:45:35-05:00",
			"created": "2018-03-03T13:01:31-05:00"
		}
	]
}