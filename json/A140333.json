{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A140333",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 140333,
			"data": "1,-1,2,-2,0,6,-4,-12,12,24,-4,-60,-60,120,120,24,-120,-720,-240,1080,720,120,840,-2520,-8400,0,10080,5040,-960,6720,20160,-47040,-100800,20160,100800,40320,-12096,-60480,241920,423360",
			"name": "Triangle T(n,k) with the coefficient [x^k] (n+1)!* C(n,x), in row n, column k, where C(.,.) are the Bernoulli twin number polynomials of A129378.",
			"comment": [
				"The terms at x=0 define the Bernoulli twin numbers, C(n,0)=C(n) = A129826(n)/(n+1)! .",
				"Because the C(n,x) are derived from the Bernoulli polynomials B(n,x) via a binomial transformation and because the odd-indexed Bernoulli numbers are (essentially) zero, the following sum rules for the C(n) emerge (partially in Umbral notation):",
				"For odd C(n): C(2n)=(C-1)^(2n-1), n \u003e 1, C(2n) disappears; example: C(4)=C(4)-3C(3)+3C(2)-C(1).",
				"0r for C(2n+1): (C-1)^2n=0, n \u003e0; example: C(1)-4C(2)+6C(3)-4C(4)+C(5)=0.",
				"With positive coefficients, table",
				"1, 2;",
				"2, 2, 3;",
				"3, 2, 3, 6;",
				"4, 2, 3, 6, 30;",
				"5, 2, 3, 6, 30, -30;",
				"6, 2, 3, 6, 30, -30, -42;",
				"gives C(n). Example: 3C(0)+2C(1)+3C(2)+6C(3)=0. See -A051717(n+1), Bernoulli twin numbers denominators, with from 30 opposite twin."
			],
			"example": [
				"1;    C(0,x) = 1",
				"-1, 2;    C(1,x) = -1/2+x",
				"-2, 0, 6;       C(2,x) = -1/3+x^2",
				"-4, -12, 12, 24;      C(3,x) = -1/6 -x/2 +x^2/2 +x^3",
				"-4, -60, -60, 120, 120;"
			],
			"maple": [
				"C := proc(n,x) if n =0 then 1; else add( binomial(n-1,j-1)*bernoulli(j,x),j=1..n) ; expand(%) ; end if; end proc:",
				"A140333 := proc(n,k) (n+1)!*C(n,x) ; coeftayl(%,x=0,k) ; end proc: # _R. J. Mathar_, Jun 27 2011"
			],
			"mathematica": [
				"c[0, _] = 1; c[n_, x_] := Sum[Binomial[n-1, j-1]*BernoulliB[j, x], {j, 1, n}]; t[n_, k_] := (n+1)!*Coefficient[c[n, x], x, k]; Table[t[n, k], {n, 0, 8}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Sep 16 2013 *)"
			],
			"xref": [
				"Cf. A129378."
			],
			"keyword": "sign,less,uned,tabl",
			"offset": "0,3",
			"author": "_Paul Curtz_, May 28 2008",
			"references": 3,
			"revision": 15,
			"time": "2014-02-12T18:09:11-05:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}