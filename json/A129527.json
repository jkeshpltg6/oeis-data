{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A129527",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 129527,
			"data": "0,1,3,3,7,5,9,7,15,9,15,11,21,13,21,15,31,17,27,19,35,21,33,23,45,25,39,27,49,29,45,31,63,33,51,35,63,37,57,39,75,41,63,43,77,45,69,47,93,49,75,51,91,53,81,55,105,57,87,59,105,61,93,63,127,65,99,67",
			"name": "a(2n) = a(n) + 2n, a(2n+1) = 2n + 1.",
			"comment": [
				"Sum of odd part of n and its double, fourfold, eightfold etc. \u003c= n.",
				"Starting with 1 = the ruler function triangle A115361 * [1, 2, 3, ...]. - _Gary W. Adamson_, Nov 27 2009",
				"Starting with 1 and parsed into subsets of 1, 2, 4, 8, ... terms, sum of terms in the subsets = A006516: (1, 6, 28, 120, ...). Example: 120 = (15 + 9 + 15 + 11 + 21 + 13 + 21 + 15). - _Gary W. Adamson_ Mar 18 2011",
				"a(n) = Sum(even divisors of 2(n-1) not including 2(n-1) that are obtained dividing repeatedly by 2) + (greatest odd divisor of 2(n-1), including 1), for the initial case 2(1-1)=0 will be set to 0). E.g., (offset is starting with n=1) a(3) = Sum(even divisors of 2*(3-1)=2*2=4 not including 4 obtained dividing repeatedly by 2) + greatest odd divisor of 4 = (2)+(1)=3; a(4) = Sum(even divisors of 2*(4-1)=6 not including 6 obtained dividing repeatedly by 2) + greatest odd divisor of 6 = (0) + (3) = 3; a(5) = Sum(even divisors of 2*(5-1)=8 not including 8 obtained dividing repeatedly by 2) + greatest odd divisor of 8 = (4+2) + (1) = 7, etc. - _David Morales Marciel_, Dec 21 2015"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A129527/b129527.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e"
			],
			"formula": [
				"G.f.: Sum_{k\u003e=0} x^(2^k)/(1-x^(2^k))^2.",
				"Dirichlet g.f.: zeta(s-1)*2^s/(2^s-1). - _Ralf Stephan_, Jun 17 2007",
				"G.f. satisfies g(x) = g(x^2) + x/(1-x)^2. - _Robert Israel_, Dec 20 2015",
				"n \u003c= a(n) \u003c= 2n - 1 for n \u003e 0. - _Charles R Greathouse IV_, Feb 09 2016",
				"Conjecture: a(n) = 2*n-A000265(n) for n \u003e 0. - _Velin Yanev_, Jun 23 2017. [_Joerg Arndt_, Jun 23 2017: For odd n the conjecture holds, for even n induction should work.",
				"_Andrey Zabolotskiy_, Aug 03 2017: Confirm: induction works, the conjecture holds for all n.]",
				"a(n) for n \u003e 0 is multiplicative with a(2^e) = 2^(e+1)-1 and a(p^e) = p^e for prime p \u003e 2 and e \u003e= 0. - _Werner Schulte_, Jul 02 2018",
				"Inverse Moebius transform of A062570. - _Andrew Howroyd_, Aug 07 2018",
				"Sum_{k=1..n} a(k) ~ 2*n^2/3. - _Vaclav Kotesovec_, Jun 11 2020"
			],
			"maple": [
				"f:= proc(n) option remember;",
				"  if n::odd then n else n + procname(n/2) fi",
				"end proc:",
				"f(0):= 0:",
				"seq(f(n),n=0..100); # _Robert Israel_, Dec 20 2015"
			],
			"mathematica": [
				"a[n_] := a[n] = If[EvenQ@ n, a[n/2] + n, n]; {0}~Join~Array[a, 67] (* _Michael De Vlieger_, Jun 26 2017 *)"
			],
			"program": [
				"(PARI) a(n)=if (n==0, 0, sum(k=0,valuation(n,2),n/2^k)); \\\\ corrected by _Michel Marcus_, Dec 22 2021",
				"(PARI) a(n)=if(n\u003c2,return(n)); my(k=valuation(n,2)); 2*n-n\u003e\u003ek \\\\ _Charles R Greathouse IV_, Feb 09 2016",
				"(PARI) a(n)=sumdiv(n, d, eulerphi(2*d)) \\\\ _Andrew Howroyd_, Aug 07 2018"
			],
			"xref": [
				"Row sums of A129265 and A129559.",
				"Cf. A000265, A006516, A062570, A115361."
			],
			"keyword": "nonn,easy,mult",
			"offset": "0,3",
			"author": "_Ralf Stephan_, May 29 2007",
			"references": 16,
			"revision": 58,
			"time": "2021-12-22T06:16:10-05:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}