{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A096470",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 96470,
			"data": "1,1,1,1,0,1,1,-1,2,1,1,-2,4,-3,1,1,-3,7,-10,11,1,1,-4,11,-21,32,-31,1,1,-5,16,-37,69,-100,101,1,1,-6,22,-59,128,-228,329,-328,1,1,-7,29,-88,216,-444,773,-1101,1102,1,1,-8,37,-125,341,-785,1558,-2659,3761,-3760,1,1,-9,46,-171,512,-1297,2855,-5514,9275,-13035,13036,1",
			"name": "Triangle T(n,k), read by rows, formed by setting all entries in the zeroth column and in the main diagonal ((n,n) entries) to 1 and defining the rest of the entries by the recursion T(n,k) = T(n-1,k) - T(n,k-1).",
			"comment": [
				"If A(x,y) is the bivariate o.g.f. of a triangular array T(n,k) and B(x,y) is the bivariate o.g.f. of its mirror image T(n,n-k), then B(x,y) = A(x*y, y^(-1)) and A(x,y) = B(x*y, y^(-1)). - _Petros Hadjicostas_, Aug 08 2020"
			],
			"formula": [
				"T(n,k) = T(n-1,k) - T(n,k-1) for 1 \u003c= k \u003c= n-1 with T(n,0) = 1 = T(n,n) for n \u003e= 0.",
				"The 2nd column is T(n,2) = A000124(n-2) for n \u003e= 2 (Hogben's central polygonal numbers).",
				"The \"first subdiagonal\" (unsigned) is |T(n,n-1)| = A032357(n-1) for n \u003e= 1 (Convolution of Catalan numbers and powers of -1).",
				"The \"2nd subdiagonal\" (unsigned) is |T(n,n-2)| = A033297(n) = Sum_{i=0..n-2} (-1)^i*C(n-1-i) for n \u003e= 2, where C(n) are the Catalan numbers (A000108).",
				"From _Petros Hadjicostas_, Aug 08 2020: (Start)",
				"|T(n,k)| = |A168377(n,n-k)| for 0 \u003c= k \u003c= n.",
				"Bivariate o.g.f.: (1 + y + x*y*c(-x*y))/((1 - x*y)*(1 - x + y)), where c(x) = 2/(1 + sqrt(1 - 4*x)) = o.g.f. of A000108.",
				"Bivariate o.g.f. of |T(n,k)|: (1 - y - x*y*c(x*y))/((1 + x*y)*(1 - x - y)) + 2*x*y/(1 - x^2*y^2).",
				"Bivariate o.g.f. of mirror image T(n,n-k): (1 + y + x*y*c(-x))/((1 - x)*(1 + y - x*y^2)).",
				"Bivariate o.g.f. of |T(n,n-k)|: (1 - y + x*y*c(x))/((1 + x)*(1 - y + x*y^2)) + 2*x/(1 - x^2). (End)"
			],
			"example": [
				"From _Petros Hadjicostas_, Aug 08 2020: (Start)",
				"Triangle T(n,k) (with rows n \u003e= 0 and columns k = 0..n) begins:",
				"  1;",
				"  1,  1;",
				"  1,  0,  1;",
				"  1, -1,  2,   1;",
				"  1, -2,  4,  -3,   1;",
				"  1, -3,  7, -10,  11,    1;",
				"  1, -4, 11, -21,  32,  -31,   1;",
				"  1, -5, 16, -37,  69, -100, 101,    1;",
				"  1, -6, 22, -59, 128, -228, 329, -328, 1;",
				"  ... (End)"
			],
			"program": [
				"(PARI) T(n, k) = if ((k==0) || (n==k), 1, if ((n\u003c0) || (k\u003c0), 0, if (n\u003ek, T(n-1, k) - T(n, k-1), 0)));",
				"for(n=0, 10, for (k=0, n, print1(T(n, k), \", \")); print); \\\\ _Petros Hadjicostas_, Aug 08 2020"
			],
			"xref": [
				"Cf. A000108, A000124, A032357, A033297, A168377."
			],
			"keyword": "sign,tabl",
			"offset": "0,9",
			"author": "_Gerald McGarvey_, Aug 12 2004",
			"ext": [
				"Offset changed to 0 by _Petros Hadjicostas_, Aug 08 2020"
			],
			"references": 1,
			"revision": 25,
			"time": "2020-08-09T01:07:55-04:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}