{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A187197",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 187197,
			"data": "1,2,-1,0,7,0,-9,0,10,0,-23,0,38,0,-47,0,75,0,-112,0,148,0,-217,0,293,0,-385,0,553,0,-728,0,928,0,-1272,0,1670,0,-2111,0,2765,0,-3566,0,4504,0,-5784,0,7300,0,-9123,0,11592,0,-14458,0,17838,0,-22342,0,27668,0,-33884",
			"name": "McKay-Thompson series of class 12E for the Monster group with a(0) = 2.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A187197/b187197.txt\"\u003eTable of n, a(n) for n = -1..1000\u003c/a\u003e",
				"D. Ford, J. McKay and S. P. Norton, \u003ca href=\"http://dx.doi.org/10.1080/00927879408825127\"\u003eMore on replicable functions\u003c/a\u003e, Comm. Algebra 22, No. 13, 5175-5193 (1994).",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of (1/q) * ((psi(q) * psi(q^3)) / (psi(q^2) * psi(q^6)))^2 in powers of q where psi() is a Ramanujan theta function.",
				"Expansion of ((eta(q^2)^3 * eta(q^6)^3) / (eta(q) * eta(q^3) * eta(q^4)^2 * eta(q^12)^2))^2 in powers of q.",
				"Euler transform of period 12 sequence [ 2, -4, 4, 0, 2, -8, 2, 0, 4, -4, 2, 0, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (12 t)) = 4 * g(t) where q = exp(2 Pi i t) and g() is the g.f. for A123861.",
				"Given g.f. A(q) then 0 = f(A(q), A(q^2)) where f(u, v) = (v - 4)^2 - u*v * (u - 4). - _Michael Somos_, Aug 31 2014",
				"Convolution square of A112165. a(n) = A187196(n) unless n=0. a(2*n) = 0 unless n=0. a(2*n - 1) = A058483(n)."
			],
			"example": [
				"G.f. = 1/q + 2 - q + 7*q^3 - 9*q^5 + 10*q^7 - 23*q^9 + 38*q^11 - 47*q^13 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ (1/q) (QPochhammer[ q^2]^3 QPochhammer[ q^6]^3 / (QPochhammer[ q] QPochhammer[ q^3] QPochhammer[ q^4]^2 QPochhammer[ q^12]^2))^2, {q, 0, n}]; (* _Michael Somos_, Sep 05 2014 *)",
				"a[ n_] := SeriesCoefficient[ (1/q) (QPochhammer[ -q] QPochhammer[ -q^3] / (QPochhammer[ q^4] QPochhammer[ q^12]))^2, {q, 0, n}]; (* _Michael Somos_, Sep 05 2014 *)",
				"a[ n_] := SeriesCoefficient[ 4 EllipticTheta[ 3, 0, q] EllipticTheta[ 3, 0, q^3] / (EllipticTheta[ 2, 0, q] EllipticTheta[ 2, 0, q^3]), {q, 0, n}]; (* _Michael Somos_, Sep 05 2014 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c-1, 0, n++; A = x * O(x^n); polcoeff( ((eta(x^2 + A)^3 * eta(x^6 + A)^3) / (eta(x + A) * eta(x^3 + A) * eta(x^4 + A)^2 * eta(x^12 + A)^2))^2, n))};"
			],
			"xref": [
				"Cf. A058483, A112165, A123861, A187196."
			],
			"keyword": "sign",
			"offset": "-1,2",
			"author": "_Michael Somos_, Mar 06 2011",
			"references": 3,
			"revision": 20,
			"time": "2021-03-12T22:24:46-05:00",
			"created": "2011-03-06T18:01:14-05:00"
		}
	]
}