{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A229339",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 229339,
			"data": "1,1,2,5,1,4,1,15,2,11,1,40,1,29,2,105,1,76,1,275,2,199,1,720,1,521,2,1885,1,1364,1,4935,2,3571,1,12920,1,9349,2,33825,1,24476,1,88555,2,64079,1,231840,1,167761,2,606965,1,439204,1,1589055,2,1149851,1,4160200,1,3010349,2",
			"name": "GCD of all sums of n consecutive Lucas numbers.",
			"comment": [
				"The sum of two consecutive Lucas number is the sum of four consecutive Fibonacci numbers, which is verified easily enough with the identity L(n) = F(n - 1) + F(n + 1). Therefore a(1) = a(2) = A210209(4)."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A229339/b229339.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Dan Guyer and aBa Mbirika, \u003ca href=\"https://arxiv.org/abs/2104.12262\"\u003eGCD of sums of k consecutive Fibonacci, Lucas, and generalized Fibonacci numbers\u003c/a\u003e, arXiv:2104.12262 [math.NT], 2021.",
				"\u003ca href=\"/index/Rec#order_14\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,0,0,3,0,1,0,-1,0,-3,0,0,0,1)."
			],
			"formula": [
				"a(n) = 3*a(n-4) + a(n-6) - a(n-8) - 3*a(n-10) + a(n-14) for n \u003e 14. - _Giovanni Resta_, Oct 04 2013",
				"G.f.: x*(x^12 -x^11 +2*x^10 -5*x^9 -2*x^8 -x^7 -6*x^6 +x^5 -2*x^4 +5*x^3 +2*x^2 +x +1) / (-x^14 +3*x^10 +x^8 -x^6 -3*x^4 +1). - _Colin Barker_, Nov 09 2014"
			],
			"example": [
				"a(3) = 2 because any sum of three consecutive Lucas numbers is an even number.",
				"a(4) = 5 because all sums of four consecutive Lucas numbers are divisible by 5.",
				"a(5) = 1 because some sums of five consecutive Lucas numbers are coprime."
			],
			"mathematica": [
				"a[n_] := a[n] = If[n \u003c= 14, {1, 1, 2, 5, 1, 4, 1, 15, 2, 11, 1, 40, 1, 29}[[n]], 3*a[n - 4] + a[n - 6] - a[n - 8] - 3*a[n - 10] + a[n - 14]]; Array[a, 64] (* _Giovanni Resta_, Oct 04 2013 *)",
				"CoefficientList[Series[(x^12 - x^11 + 2 x^10 - 5 x^9 - 2 x^8 - x^7 - 6 x^6 + x^5 - 2 x^4 + 5 x^3 + 2 x^2 + x + 1) / (-x^14 + 3 x^10 + x^8 - x^6 - 3 x^4 + 1), {x, 0, 40}], x] (* _Vincenzo Librandi_, Nov 09 2014 *)",
				"LinearRecurrence[{0,0,0,3,0,1,0,-1,0,-3,0,0,0,1},{1,1,2,5,1,4,1,15,2,11,1,40,1,29},70] (* _Harvey P. Dale_, Jul 21 2021 *)",
				"Table[GCD[LucasL[n + 1] - 2, LucasL[n] + 1], {n, 0, 50}] (* _Horst H. Manninger_, Dec 25 2021 *)"
			],
			"program": [
				"(PARI) Vec(x*(x^12 -x^11 +2*x^10 -5*x^9 -2*x^8 -x^7 -6*x^6 +x^5 -2*x^4 +5*x^3 +2*x^2 +x +1) / (-x^14 +3*x^10 +x^8 -x^6 -3*x^4 +1) + O(x^100)) \\\\ _Colin Barker_, Nov 09 2014"
			],
			"xref": [
				"Cf. A210209, A022112, A022088, A022098."
			],
			"keyword": "nonn,easy",
			"offset": "1,3",
			"author": "_Alonso del Arte_, Sep 23 2013",
			"references": 3,
			"revision": 50,
			"time": "2021-12-25T11:16:22-05:00",
			"created": "2013-10-09T11:23:36-04:00"
		}
	]
}