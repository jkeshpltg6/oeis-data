{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A096978",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 96978,
			"data": "0,1,4,19,74,305,1208,4863,19398,77709,310612,1242907,4970722,19884713,79535216,318148151,1272578046,5090341317,20361307020,81445344595,325781145370,1303125047521,5212499258024,20849998896239,83399991856694",
			"name": "Sum of the areas of the first n Jacobsthal rectangles.",
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A096978/b096978.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,3,-14,8)."
			],
			"formula": [
				"G.f.: x/((1-x)^2*(1+2*x)*(1-4*x)).",
				"a(n) = 8*4^n/27 - 2*(-2)^n/27 - (n+2)/9;",
				"a(n) = Sum_{k=0..n} (2*4^k/3 + (-2)^k/3)*(n-k).",
				"a(n) = 4*a(n-1) + 3*a(n-2) - 14*a(n-3) + 8*a(n-4).",
				"a(n) = Sum_{k=0..n} A001045(k)*A001045(k+1).",
				"a(n-1) = Sum_{k=0..n} (-1)^(k+1)*A001045(k)*A001045(2*(n-k)). - _Paul Barry_, Aug 11 2009"
			],
			"mathematica": [
				"LinearRecurrence[{4,3,-14,8},{0,1,4,19},30] (* or *) Table[(2^(2n+1)-3n - 3+(-2)^n)/27,{n,30}] (* _Harvey P. Dale_, Aug 08 2011 *)"
			],
			"program": [
				"(MAGMA) [8*4^n/27-2*(-2)^n/27-(n+2)/9: n in [0..30]]; // _Vincenzo Librandi_, May 31 2011"
			],
			"xref": [
				"Cf. A096977, A064831."
			],
			"keyword": "easy,nonn",
			"offset": "0,3",
			"author": "_Paul Barry_, Jul 17 2004",
			"references": 2,
			"revision": 16,
			"time": "2018-09-09T04:13:58-04:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}