{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A110907",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 110907,
			"data": "1,12,50,108,194,300,434,588,770,972,1202,1452,1730,2028,2354,2700,3074,3468,3890,4332,4802,5292,5810,6348,6914,7500,8114,8748,9410,10092,10802,11532,12290,13068,13874,14700,15554,16428,17330,18252,19202",
			"name": "Number of points in the standard root system version of the D_3 (or f.c.c.) lattice having L_infinity norm n.",
			"comment": [
				"This lattice consists of all points (x,y,z) where x,y,z are integers with an even sum.",
				"The L_infinity norm of a vector is the largest component in absolute value.",
				"The sequence for the D_k lattice has the terms ((2*n+1)^k-(2*n-1)^k)/2, if k is even, and the terms ((2n+1)^k-(2*n-1)^k)/2+(-1)^n if k is odd (like here for k=3). The sequence for A_2 is A008458, for A_3 A010006, for A_4 the first differences of A083669. A_5 is 2+2*n^2*(25+44*n^2) if n\u003e0, and 1 if n=0. - _R. J. Mathar_, Feb 09 2010"
			],
			"reference": [
				"J. H. Conway and N. J. A. Sloane, Sphere Packings, Lattices and Groups, Springer-Verlag, Chap. 4."
			],
			"link": [
				"R. J. Mathar, \u003ca href=\"http://arxiv.org/abs/1002.3844\"\u003ePoint counts of D_k and some A_k and E_k integer lattices inside hypercubes\u003c/a\u003e arXiv:1002.3844  [math.GT], 2010.",
				"G. Nebe and N. J. A. Sloane, \u003ca href=\"http://www.math.rwth-aachen.de/~Gabriele.Nebe/LATTICES/D3.html\"\u003eHome page for this lattice\u003c/a\u003e",
				"\u003ca href=\"/index/Fa#fcc\"\u003eIndex entries for sequences related to f.c.c. lattice\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2, 0, -2, 1)."
			],
			"formula": [
				"From _R. J. Mathar_, Feb 03 2010: (Start)",
				"a(n) = 2*a(n-1) - 2*a(n-3) + a(n-4), n\u003e4.",
				"a(n) = 1 + (-1)^n + 12*n^2, n\u003e0.",
				"G.f.: 1 - 2*x*(6 + 13*x + 4*x^2 + x^3)/((1+x)*(x-1)^3). (End)"
			],
			"example": [
				"a(0) = 1: 000",
				"a(1) = 12: +-1 +-1 0, where the 0 can be in any of the three coordinates",
				"a(2) = 50: +-2 0 0 (6), +-2 +-1 +-1 (24), +-2 +-2 0 (12), +-2 +-2 +-2 (8)."
			],
			"maple": [
				"A110907 := proc(n) a :=0 ; for x from -n to n do for y from -n to n do for z from -n to n do if type(x+y+z,'even') then m := max( abs(x),abs(y),abs(z)) ; if m = n then a := a+1 ; end if; end if; end do ; end do ; end do ; a ; end proc: seq(A110907(n),n=0..40) ; # _R. J. Mathar_, Feb 03 2010"
			],
			"mathematica": [
				"a[0] = 1; a[n_] := 1 + (-1)^n + 12*n^2;",
				"Table[a[n], {n, 0, 40}] (* _Jean-François Alcover_, Nov 16 2017, after _R. J. Mathar_ *)"
			],
			"xref": [
				"Cf. A117216, A022144, A010014, A175112 (D_5), A175114 (D_6)."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Apr 15 2008",
			"ext": [
				"I would like to get analogous sequences for A_2, A_4, A_5, ..., D_4 (see A117216), D_5, ..., E_6, E_7, E_8.",
				"Extended by _R. J. Mathar_, Feb 03 2010",
				"Removed the \"conjectured\" attribute from formulas - _R. J. Mathar_, Feb 27 2010"
			],
			"references": 6,
			"revision": 28,
			"time": "2017-11-16T06:22:38-05:00",
			"created": "2006-01-24T03:00:00-05:00"
		}
	]
}