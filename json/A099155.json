{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A099155",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 99155,
			"data": "1,2,4,7,13,26,50,98",
			"name": "Maximum length of a simple path with no chords in the n-dimensional hypercube, also known as snake-in-the-box problem.",
			"comment": [
				"Some confusion seems to exist in the distinction between n-snakes and n-coils. Earlier papers and also A000937 used \"snake\" to mean a closed path, which is called n-coil in newer notation, see Harary et al. a(8) is conjectured to be 97 by Rajan and Shende. [The true value, however, is 98. See Ostergard and Ville, 2014. - _N. J. A. Sloane_, Apr 06 2014]",
				"Longest open achordal path in n-dimensional hypercube.",
				"After 50, lower bounds on the next terms are 97, 186, 358, 680, 1260. - Darren Casella (artdeco42(AT)yahoo.com), Mar 04 2005",
				"For current records see Potter link.",
				"The length of the longest known snake (open path) in dimension 8 (as of December, 2009) is 98. It was found by B. Carlson (confirmed by W. D. Potter) and soon to be reported in the literature. Numerous 97-length snakes are currently published. - W. D. Potter (potter(AT)uga.edu), Feb 24 2009"
			],
			"reference": [
				"B. P. Carlson, D. F. Hougen: Phenotype feedback genetic algorithm operators for heuristic encoding of snakes within hypercubes. In: Proc. 12th Annu. Conf. Genetic and Evolutionary Computation, pp. 791-798 (2010). [Shows a(8) \u003e= 98. - _N. J. A. Sloane_, Apr 06 2014]",
				"D. Casella and W. D. Potter, \"New Lower Bounds for the Snake-in-the-box Problem: Using Evolutionary Techniques to Hunt for Coils\". Submitted to IEEE Conference on Evolutionary Computing, 2005."
			],
			"link": [
				"David Allison, Daniel Paulusma, \u003ca href=\"https://arxiv.org/abs/1603.05119\"\u003eNew Bounds for the Snake-in-the-Box Problem\u003c/a\u003e, arXiv:1603.05119 [math.CO], 16 Jun 2016.",
				"D. A. Casella and W. D. Potter, \u003ca href=\"https://www.researchgate.net/publication/221439264_New_Lower_Bounds_for_the_Snake-in-the-Box_Problem_Using_Evolutionary_Techniques_to_Hunt_for_Snakes\"\u003eNew Lower Bounds for the Snake-in-the-box Problem: Using Evolutionary Techniques to Hunt for Snakes\u003c/a\u003e, 18th International FLAIRS Conference (2005).",
				"F. Harary, J. P. Hayes and H. J. Wu, \u003ca href=\"https://doi.org/10.1016/0898-1221(88)90213-1\"\u003eA survey of the theory of hypercube graphs\u003c/a\u003e, Comput. Math. Applic., 15 (1988) 277-289.",
				"S. Hood, D. Recoskie, J. Sawada, D. Wong, \u003ca href=\"https://doi.org/10.1007/s10878-013-9630-z\"\u003eSnakes, coils, and single-track circuit codes with spread k\u003c/a\u003e, J. Combin. Optim. 30 (1) (2015) 42-62, Table 2 (lower bounds for n\u003c=17)",
				"K. J. Kochut, \u003ca href=\"http://cobweb.cs.uga.edu/~potter/CompIntell/kochut.pdf\"\u003eSnake-In-The-Box Codes for Dimension 7\u003c/a\u003e, Journal of Combinatorial Mathematics and Combinatorial Computing, Vol. 20, pp. 175-185, 1996.",
				"Patric R. J. Östergård, Ville H. Pettersson, \u003ca href=\"https://doi.org/10.1007/s00373-014-1423-3\"\u003eExhaustive Search for Snake-in-the-Box Codes\u003c/a\u003e, Graphs and Combinatorics 31, 1019-1028 (2015), shows a(8)=98.",
				"Ville Pettersson, \u003ca href=\"https://aaltodoc.aalto.fi/handle/123456789/17688\"\u003eGraph Algorithms for Constructing and Enumerating Cycles and Related Structures\u003c/a\u003e, Doctoral Dissertation, 2015.",
				"Potter, W. D., \u003ca href=\"http://ai1.ai.uga.edu/sib/sibwiki/doku.php/records\"\u003eA list of current records for the Snake-in-the-Box problem.\u003c/a\u003e",
				"Potter, W. D., R. W. Robinson, J. A. Miller, K. J. Kochut and D. Z. Redys, \u003ca href=\"https://www.researchgate.net/publication/2577776_Using_The_Genetic_Algorithm_to_Find_Snake-In-The-Box_Codes\"\u003eUsing the Genetic Algorithm to Find Snake In The Box Codes\u003c/a\u003e, Proceedings of the Seventh International Conference on Industrial \u0026 Engineering Applications of Artificial Intelligence and Expert Systems, pp. 421-426, Austin, Texas, 1994.",
				"Dayanand S. Rajan, Anil M. Shende, \u003ca href=\"https://www.researchgate.net/publication/2525975_Maximal_and_Reversible_Snakes_in_Hypercubes\"\u003eMaximal and Reversible Snakes in Hypercubes.\u003c/a\u003e",
				"Gilles Zémor, \u003ca href=\"https://doi.org/10.1007/BF01200911\"\u003eAn upper bound on the size of the snake-in-the-box\u003c/a\u003e, Combinatorica 17.2 (1997): 287-298."
			],
			"example": [
				"a(3)=4: Path of a longest 3-snake starts at 000 and then visits 100 101 111 011.",
				"a(4)=7: Path of a longest 4-snake: 0000 1000 1010 1110 0110 0111 0101 1101.",
				"See figures 1 and 2 in Rajan-Shende."
			],
			"xref": [
				"Cf. A000937 = length of maximum n-coil."
			],
			"keyword": "hard,more,nonn",
			"offset": "1,2",
			"author": "_Hugo Pfoertner_, Oct 11 2004",
			"ext": [
				"a(8) from Patric R. J. Östergård and V. H. Pettersson (2014). - _N. J. A. Sloane_, Apr 06 2014"
			],
			"references": 4,
			"revision": 65,
			"time": "2021-01-14T08:11:43-05:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}