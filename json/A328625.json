{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A328625",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 328625,
			"data": "0,1,2,5,4,3,6,7,14,17,22,21,12,13,26,29,10,9,18,19,8,11,28,27,24,25,20,23,16,15,30,31,32,35,34,33,66,67,74,77,82,81,102,103,116,119,100,99,138,139,128,131,148,147,174,175,170,173,166,165,60,61,62,65,64,63,126,127,134,137,142,141,192,193,206,209,190,189,48,49,38,41,58,57,114",
			"name": "In primorial base representation of n, multiply all other digits except the least significant with 1+{their right hand side neighbor}, and reduce each modulo prime(k) (with k \u003e 1) to get a new digit for the position k (the least significant digit stays as it is), then convert back to decimal.",
			"comment": [
				"In primorial base (see A049345) we keep the least significant digit (0 or 1) intact, and replace each digit d(i) left of that (for i \u003e= 2) with a new digit value computed as d(i)*(1+d(i-1)) mod prime(i). a(n) is then the newly constructed primorial expansion converted back to decimal.",
				"Because for all primes p, Z_p is a field (not just a ring), this sequence is a permutation of nonnegative integers, and roughly speaking, offers a kind of analog of A003188 for primorial base system. Note however that it is the digit neighbor on the right (not left) hand side that affects here what will be the new digit at each position."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A328625/b328625.txt\"\u003eTable of n, a(n) for n = 0..30029\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e",
				"\u003ca href=\"/index/Pri#primorialbase\"\u003eIndex entries for sequences related to primorial base\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A276085(A328624(n)).",
				"For all n, A328620(a(n)) = A328620(n)."
			],
			"example": [
				"In primorial base (A049345) 199 is written as \"6301\" because 6*A002110(3) + 3*A002110(2) + 0*A002110(1) + 1*A002110(0) = 6*30 + 3*6 + 0*2 + 1*1 = 199. Multiplying each digit left of the least significant by 1+{digit one step right}, and reducing modulo the corresponding prime yields 4*6 mod 7, 1*3 mod 5, 2*0 mod 3, (with the least significant 1 staying the same), so we get \"3301\", which is the primorial base expansion of 109, thus a(199) = 109."
			],
			"program": [
				"(PARI)",
				"A002110(n) = prod(i=1,n,prime(i));",
				"A276085(n) = { my(f = factor(n)); sum(k=1, #f~, f[k, 2]*A002110(primepi(f[k, 1])-1)); };",
				"A328624(n) = { my(m=1, p=2, e, g=1); while(n, e = (n%p); m *= (p^((g*e)%p)); g = e+1; n = n\\p; p = nextprime(1+p)); (m); };",
				"A328625(n) = A276085(A328624(n));"
			],
			"xref": [
				"Cf. A002110, A049345, A276085, A328620, A328624, A328626 (inverse permutation), A328628.",
				"Cf. also A003188, A289234, A328622."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Antti Karttunen_, Oct 23 2019",
			"references": 9,
			"revision": 22,
			"time": "2019-10-25T18:09:25-04:00",
			"created": "2019-10-25T18:09:25-04:00"
		}
	]
}