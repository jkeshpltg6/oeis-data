{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002209",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2209,
			"id": "M2015 N0796",
			"data": "1,2,12,8,720,288,60480,17280,3628800,89600,95800320,17418240,2615348736000,402361344000,4483454976000,98402304,32011868528640000,342372925440000,51090942171709440000,5377993912811520000,33720021833328230400000",
			"name": "Denominators of coefficients for numerical integration.",
			"comment": [
				"a(n) is the denominator of the \"reverse\" multiple zeta value zeta_n^R(0,0,...,0) for n \u003e 0. - _Jonathan Sondow_, Nov 29 2006",
				"The numerators are given in A002208."
			],
			"reference": [
				"Charles Jordan, Calculus of Finite Differences, Chelsea 1965, p. 529.",
				"N. E. Nørlund, Vorlesungen über Differenzenrechnung, Springer-Verlag, Berlin, 1924.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A002209/b002209.txt\"\u003eTable of n, a(n) for n = 0..100\u003c/a\u003e",
				"S. Akiyama and Y. Tanigawa, \u003ca href=\"http://math.tsukuba.ac.jp/~akiyama/papers/Mzvnrev.pdf\"\u003eMultiple zeta values at non-positive integers\u003c/a\u003e, Ramanujan J. 5 (2001), 327-351.",
				"Guodong Liu, \u003ca href=\"http://www.fq.math.ca/Papers1/45-2/quartliu02_2007.pdf\"\u003eSome computational formulas for Norlund numbers\u003c/a\u003e, Fib. Quart., 45 (2007), 133-137.",
				"A. N. Lowan and H. Salzer, \u003ca href=\"http://dx.doi.org/10.1002/sapm194322149\"\u003eTable of coefficients in numerical integration formulas\u003c/a\u003e, J. Math. Phys., 22 (1943), 49-50.",
				"A. N. Lowan and H. Salzer, \u003ca href=\"/A002206/a002206.pdf\"\u003eTable of coefficients in numerical integration formulas\u003c/a\u003e, J. Math. Phys. Mass. Inst. Tech. 22 (1943), 49-50.[Annotated scanned copy]"
			],
			"formula": [
				"G.f. of A002208(n)/a(n): -x/((1-x)*log(1-x)).",
				"a(n) = denominator(v(n)), where v(n) = 1 - Sum_{i=0..n-1} v(i)/(n-i+1), v(0)=1. - _Vladimir Kruchinin_, Aug 28 2013",
				"a(n) = denominator(((-1)^n/n!)*Sum_{k=0..n} Stirling1(n+1,k+1)/(k+1)). - _Vladimir Kruchinin_, Oct 12 2016"
			],
			"example": [
				"1, 1/2, 5/12, 3/8, 251/720, 95/288, 19087/60480, 5257/17280, 1070017/3628800, 25713/89600, 26842253/95800320, 4777223/17418240, 703604254357/2615348736000, 106364763817/402361344000, ... = A002208/A002209."
			],
			"mathematica": [
				"a[0] = 1; a[n_] := (-1)^n*Sum[(-1)^(k+1)*BernoulliB[k]*StirlingS1[n, k]/k, {k, 1, n}]/(n-1)!; Table[a[n], {n, 0, 20}] // Denominator (* _Jean-François Alcover_, Sep 27 2012, after Rudi Huysmans's formula for A002208 *)",
				"Denominator[CoefficientList[Series[-x/((1-x)Log[1-x]),{x,0,20}],x]] (* _Harvey P. Dale_, Feb 01 2013 *)"
			],
			"program": [
				"(Maxima)",
				"a(n):=denom(((-1)^(n)*sum(stirling1(n+1,k+1)/(k+1),k,0,n))/(n)!); /* _Vladimir Kruchinin_, Oct 12 2016 */"
			],
			"xref": [
				"Cf. A002208. See also A002657, A002790, A002206, A002207, A006232, A006233."
			],
			"keyword": "nonn,frac,easy,nice",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 21,
			"revision": 63,
			"time": "2018-11-22T15:36:44-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}