{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A107711",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 107711,
			"data": "1,1,1,1,1,1,1,1,1,1,1,1,3,1,1,1,1,2,2,1,1,1,1,5,10,5,1,1,1,1,3,5,5,3,1,1,1,1,7,7,35,7,7,1,1,1,1,4,28,14,14,28,4,1,1,1,1,9,12,42,126,42,12,9,1,1,1,1,5,15,30,42,42,30,15,5,1,1,1,1,11,55,165,66,462,66,165,55,11,1,1",
			"name": "Triangle read by rows: T(0,0)=1, T(n,m) = binomial(n,m) * gcd(n,m)/n.",
			"comment": [
				"T(0,0) is an indeterminate, but 1 seems a logical value to assign it. T(n,0) = T(n,1) = T(n,n-1) = T(n,n) = 1.",
				"T(2n,n) = A001700(n-1) (n\u003e=1). - _Emeric Deutsch_, Jun 13 2005"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A107711/b107711.txt\"\u003eRows n = 1..125 of triangle, flattened\u003c/a\u003e",
				"Wolfdieter Lang, \u003ca href=\"http://arxiv.org/abs/1404.2710\"\u003eOn Collatz' Words, Sequences and Trees\u003c/a\u003e, arXiv:1404.2710 [math.NT], 2014 and \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Lang/lang6.html\"\u003eJ. Int. Seq. 17 (2014) # 14.11.7\u003c/a\u003e."
			],
			"formula": [
				"From _Wolfdieter Lang_, Feb 28 2014 (Start)",
				"T(n, m) = T(n-1,m)*(n-1)*gcd(n,m)/((n-m)*gcd(n-1,m)), n \u003e m \u003e= 1, T(n, 0) = 1, T(n, n) = 1, otherwise 0.",
				"T(n, m) = binomial(n-1,m-1)*gcd(n,m)/m for n \u003e= m \u003e= 1, T(n,0) = 1, otherwise 0 (from iteration of the preceding recurrence).",
				"T(n, m) = T(n-1, m-1)*(n-1)*gcd(n,m)/(m*gcd(n-1,m-1)) for n \u003e= m \u003e= 2, T(n, 0) = 1, T(n, 1) = 0, otherwise 0 (from the preceding formula).",
				"T(2*n, n) = A001700(n-1) (n\u003e=1) (see the _Emeric Deutsch_ comment above), T(2*n, n-1) = A234040(n), T(2*n+1,n) = A000108(n), n \u003e= 0 (Catalan numbers).",
				"Column sequences: T(n+2, 2) = A026741(n+1), T(n+3, 3) = A234041(n), T(n+4, 4) = A208950(n+2), T(n+5, 5) = A234042, n \u003e= 0. (End)"
			],
			"example": [
				"T(6,2)=5 because binomial(6,2)*gcd(6,2)/6 = 15*2/6 = 5.",
				"The triangle T(n,m) begins:",
				"n\\m 0  1  2   3   4    5   6   7  8  9  10...",
				"0:  1",
				"1:  1  1",
				"2:  1  1  1",
				"3:  1  1  1   1",
				"4:  1  1  3   1   1",
				"5:  1  1  2   2   1    1",
				"6:  1  1  5  10   5    1   1",
				"7:  1  1  3   5   5    3   1   1",
				"8:  1  1  7   7  35    7   7   1  1",
				"9:  1  1  4  28  14   14  28   4  1  1",
				"10: 1  1  9  12  42  126  42  12  9  1   1",
				"n\\m 0  1  2   3   4    5   6   7  8  9  10...",
				"... reformatted - _Wolfdieter Lang_, Feb 23 2014"
			],
			"maple": [
				"a:=proc(n,k) if n=0 and k=0 then 1 elif k\u003c=n then binomial(n,k)*gcd(n,k)/n else 0 fi end: for n from 0 to 13 do seq(a(n,k),k=0..n) od; # yields sequence in triangular form. - _Emeric Deutsch_, Jun 13 2005"
			],
			"mathematica": [
				"T[0, 0] = 1; T[n_, m_] := Binomial[n, m] * GCD[n, m]/n;",
				"Table[T[n, m], {n, 1, 13}, {m, 1, n}] // Flatten (* _Jean-François Alcover_, Nov 16 2017 *)"
			],
			"program": [
				"(Haskell)",
				"a107711 n k = a107711_tabl !! n !! k",
				"a107711_row n = a107711_tabl !! n",
				"a107711_tabl = [1] : zipWith (map . flip div) [1..]",
				"               (tail $ zipWith (zipWith (*)) a007318_tabl a109004_tabl)",
				"-- _Reinhard Zumkeller_, Feb 28 2014"
			],
			"xref": [
				"Cf. A007318, A001700, A000108, A234040, A026741, A234042.",
				"Cf. A109004."
			],
			"keyword": "tabl,nonn",
			"offset": "0,13",
			"author": "_Leroy Quet_, Jun 10 2005",
			"ext": [
				"More terms from _Emeric Deutsch_, Jun 13 2005"
			],
			"references": 9,
			"revision": 41,
			"time": "2019-02-21T02:52:49-05:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}