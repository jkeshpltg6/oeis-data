{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A188967",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 188967,
			"data": "0,1,0,1,1,0,0,0,1,1,1,1,1,0,0,0,0,0,0,0,1,0,1,1,1,1,1,1,1,1,1,0,1,1,0,0,1,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,1,1,0,0,1,0,1,1,1,1,1,0,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,0,0,1,1,1,1,0,1,1,0,0,1,0,0,0,0,1,1,0,0,1,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0",
			"name": "Zero-one sequence based on (3n-2): a(A016777(k))=a(k); a(A007494(k))=1-a(k); a(1)=0.",
			"comment": [
				"Compare with the generation of the Thue-Morse sequence T=A010060 from T(2n-1)=T(n), T(2n)=1-T(n), T(1)=0.",
				"Other zero-one sequences generated in this manner:",
				"from triangular numbers: A189011",
				"from squares: A188973",
				"from pentagonal numbers: A189014",
				"from hexagonal numbers: A189212",
				"from Beatty [n*sqrt(2)]: A189078 and A189081",
				"from cubes: A189008",
				"from primes: A189141",
				"from (3n): A189215 and A189222",
				"from (3n-1): A189097",
				"from (3n-2): A188967",
				"from (4n): A189289, A189292, A189275, A189298"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A188967/b188967.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"Let u=A016777 and v=A007494, so that u(n)=3n-2 and v=complement(u) for n\u003e=1.  Then a is a self-generating zero-one sequence with initial value a(1)=0 and a(u(k))=a(k); a(v(k))=1-a(k).",
				"a(2)=a(v(1))=1-a(1)=1",
				"a(3)=a(v(2))=1-a(2)=0",
				"a(4)=a(u(2))=a(2)=1."
			],
			"mathematica": [
				"u[n_] := 3n - 2;  (*A016777*)",
				"a[1] = 0; h = 128;",
				"c = (u[#1] \u0026) /@ Range[h];",
				"d = (Complement[Range[Max[#1]], #1] \u0026)[c]; (*A007494*)",
				"Table[a[d[[n]]] = 1 - a[n], {n, 1, h - 1}];",
				"Table[a[c[[n]]] = a[n], {n, 1, h}]   (*A188967*)",
				"Flatten[Position[%, 0]]  (*A188968*)",
				"Flatten[Position[%%, 1]] (*A188969*)"
			],
			"program": [
				"(Haskell)",
				"import Data.List (transpose)",
				"a188967 n = a188967_list !! (n-1)",
				"a188967_list = 0 : zipWith ($)",
				"                   (cycle [(1 -) . a188967, (1 -) . a188967, a188967])",
				"                   (concat $ transpose [[1, 3 ..], [2, 4 ..], [2 ..]])",
				"-- _Reinhard Zumkeller_, May 18 2015"
			],
			"xref": [
				"Cf. A188968, A188969.",
				"Cf. A257998 (partial sums), A258062 (run lengths)."
			],
			"keyword": "nonn",
			"offset": "1",
			"author": "_Clark Kimberling_, Apr 14 2011",
			"references": 45,
			"revision": 19,
			"time": "2017-06-28T20:29:21-04:00",
			"created": "2011-04-15T11:04:24-04:00"
		}
	]
}