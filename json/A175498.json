{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A175498",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 175498,
			"data": "1,2,4,3,6,10,5,11,7,12,9,16,8,17,15,23,13,24,18,28,14,26,19,32,20,34,21,36,25,41,22,39,30,48,27,46,29,49,31,52,37,59,33,56,40,64,35,60,38,65,42,68,43,71,44,73,45,75,51,82,47,79,112,50,84,53,88,54,90,57,94,55,93,61,100,58,98,62,103,63,105,67",
			"name": "a(1)=1. a(n) = the smallest positive integer not occurring earlier such that a(n)-a(n-1) doesn't equal a(k)-a(k-1) for any k with 2 \u003c= k \u003c= n-1.",
			"comment": [
				"This sequence is a permutation of the positive integers.",
				"a(n+1)-a(n) = A175499(n).",
				"Conjecture: the lexicographically earliest permutation of {1,2,...n} for which differences of adjacent numbers are all distinct (cf. A131529) has, for n--\u003einfinity, this sequence as its prefix. - _Joerg Arndt_, May 27 2012"
			],
			"link": [
				"Joerg Arndt and Reinhard Zumkeller, \u003ca href=\"/A175498/b175498.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e, first 1122 terms from Joerg Arndt",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"mathematica": [
				"a[1] = 1; d[1] = 0; k = 1; z = 10000; zz = 120;",
				"A[k_] := Table[a[i], {i, 1, k}]; diff[k_] := Table[d[i], {i, 1, k}];",
				"c[k_] := Complement[Range[-z, z], diff[k]];",
				"T[k_] := -a[k] + Complement[Range[z], A[k]];",
				"Table[{h = Min[Intersection[c[k], T[k]]], a[k + 1] = a[k] + h, d[k + 1] = h, k = k + 1}, {i, 1, zz}];",
				"u = Table[a[k], {k, 1, zz}]  (* _Clark Kimberling_, May 13 2015 *)"
			],
			"program": [
				"(Python)",
				"A175498_list, l, s, b1, b2 = [1,2], 2, 3, set(), set([1])",
				"for n in range(3, 10**5):",
				"....i = s",
				"....while True:",
				"........if not (i in b1 or i-l in b2):",
				"............A175498_list.append(i)",
				"............b1.add(i)",
				"............b2.add(i-l)",
				"............l = i",
				"............while s in b1:",
				"................b1.remove(s)",
				"................s += 1",
				"............break",
				"........i += 1 # _Chai Wah Wu_, Dec 15 2014",
				"(Haskell)",
				"import Data.List (delete)",
				"a175498 n = a175498_list !! (n-1)",
				"a175498_list = 1 : f 1 [2..] [] where",
				"   f x zs ds = g zs where",
				"     g (y:ys) | diff `elem` ds = g ys",
				"              | otherwise      = y : f y (delete y zs) (diff:ds)",
				"              where diff = y - x",
				"-- _Reinhard Zumkeller_, Apr 25 2015"
			],
			"xref": [
				"Cf. A081145, A175499, A257465 (inverse), A257883, A131388, A131389, A257705."
			],
			"keyword": "nonn,nice",
			"offset": "1,2",
			"author": "_Leroy Quet_, May 31 2010",
			"ext": [
				"More terms from _Sean A. Irvine_, Jan 27 2011"
			],
			"references": 22,
			"revision": 36,
			"time": "2015-05-13T15:55:22-04:00",
			"created": "2010-07-11T03:00:00-04:00"
		}
	]
}