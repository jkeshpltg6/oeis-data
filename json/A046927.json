{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A046927",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 46927,
			"data": "0,0,0,1,2,2,2,2,4,2,3,3,3,4,4,2,5,3,4,4,5,4,6,4,4,7,5,3,7,3,3,7,7,5,7,4,4,8,7,5,8,4,7,8,7,4,11,5,6,9,6,5,12,6,6,10,8,6,11,7,5,11,8,6,10,6,6,13,8,5,13,6,9,12,8,6,14,8,6,11,10,9,16,5,8,13,9,9,14,7,6,14",
			"name": "Number of ways to express 2n+1 as p+2q where p and q are primes.",
			"comment": [
				"This is related to a conjecture of Lemoine (also sometimes called Levy's conjecture, although Levy was anticipated by Lemoine 69 years earlier). - _Zhi-Wei Sun_, Jun 10 2008",
				"The conjecture states that any odd number greater than 5 can be written as p+2q where p and q are primes.",
				"It can be conjectured that 1, 3, 5, 59 and 151 are the only odd integers n such that n + 2p and n + 2q both are composite for all primes p,q with n = p + 2q. (Following an observation from V. Shevelev, cf. link to SeqFan list.) - _M. F. Hasler_, Apr 10 2017"
			],
			"reference": [
				"L. E. Dickson, \"History of the Theory of Numbers\", Vol. I (Amer. Math. Soc., Chelsea Publ., 1999); see p. 424."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A046927/b046927.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"L. Hodges, \u003ca href=\"http://www.jstor.org/pss/2690477\"\u003eA lesser-known Goldbach conjecture\u003c/a\u003e, Math. Mag., 66 (1993), 45-47.",
				"E. Lemoine, L'intermédiaire des math., 1 (1894), \u003ca href=\"http://hdl.handle.net/2027/uc1.$b415067?urlappend=%3Bseq=197\"\u003ep. 179\u003c/a\u003e; 3 (1896), \u003ca href=\"http://hdl.handle.net/2027/uc1.$b415069?urlappend=%3Bseq=157\"\u003ep. 151\u003c/a\u003e.",
				"H. Levy, \u003ca href=\"https://www.jstor.org/stable/3613447\"\u003eOn Goldbach's Conjecture\u003c/a\u003e, Math. Gaz. 47 (1963), 274.",
				"Vladimir Shevelev, \u003ca href=\"https://arxiv.org/abs/0901.3102\"\u003eBinary additive problems: recursions for numbers of representations\u003c/a\u003e, arXiv:0901.3102 [math.NT], 2009-2013.",
				"V. Shevelev, \u003ca href=\"http://list.seqfan.eu/pipermail/seqfan/2017-April/017447.html\"\u003eRe: New sequence\u003c/a\u003e, SeqFan list, April 2017.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/LevysConjecture.html\"\u003eLevy's Conjecture\u003c/a\u003e",
				"\u003ca href=\"/index/Go#Goldbach\"\u003eIndex entries for sequences related to Goldbach conjecture\u003c/a\u003e"
			],
			"formula": [
				"For n \u003e= 1, a(n) = Sum_{3\u003c=p\u003c=n+1, p prime} A((2*n + 1 - p)/2) + Sum_{2\u003c=q\u003c=(n+1)/2, q prime} B(2*n + 1 - 2*q) - A((n+1)/2)*B(n+1) - a(n-1) - ... - a(0), where A(n) = A000720(n), B(n) = A033270(n). - _Vladimir Shevelev_, Jul 12 2013"
			],
			"mathematica": [
				"a[n_] := (ways = 0; Do[p = 2k + 1; q = n-k; If[PrimeQ[p] \u0026\u0026 PrimeQ[q], ways++], {k, 1, n}]; ways); Table[a[n], {n, 0, 91}] (* _Jean-François Alcover_, Dec 05 2012 *)",
				"Table[Count[FrobeniusSolve[{1, 2}, 2 n + 1], {__?PrimeQ}], {n, 0, 91}] (* _Jan Mangaldan_, Apr 08 2013 *)"
			],
			"program": [
				"(PARI) a(n)=my(s);n=2*n+1;forprime(p=2,n\\2,s+=isprime(n-2*p));s \\\\ _Charles R Greathouse IV_, Jul 17 2013"
			],
			"xref": [
				"Cf. A194831 (records), A194830 (positions of records)."
			],
			"keyword": "nonn",
			"offset": "0,5",
			"author": "_David W. Wilson_",
			"ext": [
				"Additional references from _Zhi-Wei Sun_, Jun 10 2008"
			],
			"references": 40,
			"revision": 61,
			"time": "2020-10-04T23:24:09-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}