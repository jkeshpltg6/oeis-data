{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A258337",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 258337,
			"data": "11,2,3,41,5,61,7,83,97,101,113,127,13,149,151,163,17,181,19,2003,211,223,23,241,251,263,271,281,29,307,31,3203,331,347,353,367,37,383,397,401,419,421,43,443,457,461,47,487,491,503,5101,521,53,541,557,563,571,587,59,601,613,6203,631,641,653,661,67,683,691,701",
			"name": "Smallest prime starting with n that does not appear earlier.",
			"comment": [
				"According to a comment in A018800, every term exists. So the sequence is a permutation of the primes. Indeed, every prime p appears in the sequence either as the p-th term or earlier."
			],
			"link": [
				"Peter J. C. Moses, \u003ca href=\"/A258337/b258337.txt\"\u003eTable of n, a(n) for n = 1..5000\u003c/a\u003e",
				"\u003ca href=\"/index/Pri#piden\"\u003eIndex entries for primes involving decimal expansion of n\u003c/a\u003e"
			],
			"formula": [
				"For n\u003e=2, a(n) \u003e= prime(n-1). The equality holds iff prime(n-1) did not already appear as a(k), k\u003cn."
			],
			"maple": [
				"N:= 100: # to get a(1) to a(N)",
				"for n from 1 to N do",
				"  p:= n;",
				"  if n \u003c 10 then",
				"    p1:= 0",
				"  else",
				"    p1:= A[floor(n/10)];",
				"  fi;",
				"  if isprime(p) and p \u003e p1 then",
				"     A[n]:= p",
				"  else",
				"    for d from 1 while not assigned(A[n]) do",
				"      for i from 1 to 10^d-1 do",
				"        p:= 10^d*n+i;",
				"        if p \u003e p1 and isprime(p) then",
				"           A[n]:= p;",
				"           break",
				"        fi;",
				"      od",
				"    od",
				"  fi",
				"od:",
				"seq(A[i],i=1..N); # _Robert Israel_, Jun 29 2015"
			],
			"program": [
				"(PARI) sw(m,n)=d1=digits(n);d2=digits(m);for(i=1,min(#d1,#d2),if(d1[i]!=d2[i],return(0)));1",
				"v=[];n=1;while(n\u003c70,k=1;while(k\u003c10^3,p=prime(k);if(sw(p,n)\u0026\u0026!vecsearch(vecsort(v),p),v=concat(v,p);k=0;n++);k++));v \\\\ _Derek Orr_, Jun 13 2015",
				"(Haskell)",
				"import Data.List (isPrefixOf, delete)",
				"a258337 n = a258337_list !! (n-1)",
				"a258337_list = f 1 $ map show a000040_list where",
				"   f x pss = g pss where",
				"     g (qs:qss) = if show x `isPrefixOf` qs",
				"                     then (read qs :: Int) : f (x + 1) (delete qs pss)",
				"                     else g qss",
				"-- _Reinhard Zumkeller_, Jul 01 2015"
			],
			"xref": [
				"Cf. A000040, A018800, A030665, A062584, A258190, A258339."
			],
			"keyword": "nonn,base",
			"offset": "1,1",
			"author": "_Vladimir Shevelev_, May 27 2015",
			"references": 3,
			"revision": 32,
			"time": "2020-11-11T19:05:36-05:00",
			"created": "2015-07-01T08:26:37-04:00"
		}
	]
}