{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A060216",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 60216,
			"data": "13,78,728,7098,74256,804076,8964072,101962770,1178277464,13785812040,162923672184,1941506688940,23298085122480,281241165925044,3412392867581152,41588538022965570",
			"name": "Number of orbits of length n under the full 13-shift (whose periodic points are counted by A001022).",
			"comment": [
				"Number of monic irreducible polynomials of degree n over GF(13). - _Robert Israel_, Jan 07 2015",
				"Number of Lyndon words (aperiodic necklaces) with n beads of 13 colors. - _Andrew Howroyd_, Dec 10 2017"
			],
			"link": [
				"Indranil Ghosh, \u003ca href=\"/A060216/b060216.txt\"\u003eTable of n, a(n) for n = 1..100\u003c/a\u003e",
				"Yash Puri and Thomas Ward, \u003ca href=\"http://www.fq.math.ca/Scanned/39-5/puri.pdf\"\u003eA dynamical property unique to the Lucas sequence\u003c/a\u003e, Fibonacci Quarterly, Volume 39, Number 5 (November 2001), pp. 398-402.",
				"Y. Puri and T. Ward, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL4/WARD/short.html\"\u003eArithmetic and growth of periodic orbits\u003c/a\u003e, J. Integer Seqs., Vol. 4 (2001), #01.2.1.",
				"T. Ward, \u003ca href=\"http://www.mth.uea.ac.uk/~h720/research/files/integersequences.html\"\u003eExactly realizable sequences\u003c/a\u003e"
			],
			"formula": [
				"a(n) = (1/n)* Sum_{d|n} mu(d) 13^(n/d).",
				"G.f.: Sum_{k\u003e=1} mu(k)*log(1/(1 - 13*x^k))/k. - _Ilya Gutkovskiy_, May 19 2019"
			],
			"example": [
				"a(2)=78 since there are 169 points of period 2 in the full 13-shift and 13 fixed points, so there must be (169-13)/2 = 78 orbits of length 2."
			],
			"maple": [
				"f:= n -\u003e add(numtheory:-mobius(d)*13^(n/d),d=numtheory:-divisors(n))/n;",
				"seq(f(n), n=1..100); # _Robert Israel_, Jan 07 2015"
			],
			"mathematica": [
				"a[n_]:=(1/n) * Sum[MoebiusMu[d] *13^(n/d), {d, Divisors[n]}]; Table[a[n], {n, 20}] (* _Indranil Ghosh_, Mar 26 2017 *)"
			],
			"program": [
				"(PARI) a(n) = sumdiv(n, d, moebius(d)*13^(n/d))/n; \\\\ _Michel Marcus_, Jan 07 2015",
				"(Python)",
				"from sympy import divisors, mobius",
				"print([sum(mobius(d) * 13**(n//d) for d in divisors(n))//n for n in range(1, 21)]) # _Indranil Ghosh_, Mar 26 2017"
			],
			"xref": [
				"Column 13 of A074650.",
				"Cf. A001022."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Thomas Ward_, Mar 21 2001",
			"references": 2,
			"revision": 29,
			"time": "2021-05-08T23:04:58-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}