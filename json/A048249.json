{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A048249",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 48249,
			"data": "1,2,3,4,6,9,11,17,23,30,44,60,80,114,156,212,296,404,556,770,1065,1463,2032,2795,3889,5364,7422,10300,14229,19722,27391,37892,52599,73075,101301,140588,195405,271024,376608,523518,726812,1010576,1405013,1952498",
			"name": "Number of distinct values produced from sums and products of n unity arguments.",
			"comment": [
				"Values listed calculated by exhaustive search algorithm.",
				"For n+1 operands (n operations) there are (2n)!/((n!)((n+1)!)) possible postfix forms over a single operator. For each such form, there are 2^n ways to assign 2 operators (here, sum and product). Calculate results and eliminate duplicates.",
				"Number of distinct positive integers that can be obtained by iteratively adding or multiplying together parts of an integer partition until only one part remains, starting with 1^n. - _Gus Wiseman_, Sep 29 2018"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A048249/b048249.txt\"\u003eTable of n, a(n) for n = 1..75\u003c/a\u003e",
				"\u003ca href=\"/index/Com#complexity\"\u003eIndex to sequences related to the complexity of n\u003c/a\u003e",
				"\u003ca href=\"/index/Fo#4x4\"\u003eIndex entries for similar sequences\u003c/a\u003e"
			],
			"formula": [
				"Equals partial sum of \"number of numbers of complexity n\" (A005421). - _Jonathan Vos Post_, Apr 07 2006"
			],
			"example": [
				"a(3)=3 since (in postfix): 111** = 11*1* = 1, 111*+ = 11*1+ = 111+* = 11+1* = 2 and 111++ = 11+1+ = 3. Note that at n=7, the 11 possible values produced are the set {1,2,3,4,5,6,7,8,9,10,12}. This is the first n for which there are \"skipped\" values in the set."
			],
			"maple": [
				"b:= proc(n) option remember; `if`(n=1, {1}, {seq(seq(seq(",
				"     [f+g, f*g][], g=b(n-i)), f=b(i)), i=1..iquo(n, 2))})",
				"    end:",
				"a:= n-\u003e nops(b(n)):",
				"seq(a(n), n=1..35);  # _Alois P. Heinz_, May 05 2019"
			],
			"mathematica": [
				"ReplaceListRepeated[forms_,rerules_]:=Union[Flatten[FixedPointList[Function[pre,Union[Flatten[ReplaceList[#,rerules]\u0026/@pre,1]]],forms],1]];",
				"Table[Length[Select[ReplaceListRepeated[{Array[1\u0026,n]},{{foe___,x_,mie___,y_,afe___}:\u003eSort[Append[{foe,mie,afe},x+y]],{foe___,x_,mie___,y_,afe___}:\u003eSort[Append[{foe,mie,afe},x*y]]}],Length[#]==1\u0026]],{n,10}] (* _Gus Wiseman_, Sep 29 2018 *)"
			],
			"xref": [
				"Cf. A000792, A005520, A066739, A070960, A201163, A319850, A318949, A319855, A319856, A319909, A319910, A319911."
			],
			"keyword": "nonn,nice",
			"offset": "1,2",
			"author": "_Tony Bartoletti_",
			"ext": [
				"More terms from _David W. Wilson_, Oct 10 2001",
				"a(43)-a(44) from _Alois P. Heinz_, May 05 2019"
			],
			"references": 14,
			"revision": 30,
			"time": "2019-05-05T09:12:25-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}