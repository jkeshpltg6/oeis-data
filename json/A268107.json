{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A268107",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 268107,
			"data": "3,9,9,5,2,4,6,6,7,0,9,6,7,9,9,4,6,5,5,2,5,0,3,3,4,7,4,3,3,2,2,5,8,3,3,2,2,1,7,3,6,9,8,5,4,6,7,5,9,9,6,8,9,7,7,3,6,7,0,0,5,2,8,9,4,8,5,3,0,7,3,7,0,2,7,1,2,5,9,3,4,5,6,6,3,4,9,2,0,9,8,2,0,2,5,7,5,7,4,3,3,3,1,0,0,1,6,6,7,5,1,6,4,6,9,6,4,7,4,5,6,4,4,9,8,4,5",
			"name": "Decimal expansion of 'lambda', a Somos quadratic recurrence constant mentioned by Steven Finch.",
			"comment": [
				"[Quoted from _Steven Finch_] Another Somos constant lambda = 0.3995246670... arises as follows: If k \u003c lambda, then the sequence h_0 = 0, h_1 = k, h_n = h_(n-1)*(1 + h_(n-1) - h_(n-2)) for n\u003e=2 converges to a limit less than 1; if k \u003e lambda, then the sequence diverges to infinity. This is similar to Grossman's constant.",
				"A heuristical evaluation of lambda = 0.39952466709679946552503347433225833221736985467599... was communicated to me by _Jon E. Schoenfield_ in a private email."
			],
			"reference": [
				"Steven R. Finch, Mathematical Constants, Cambridge University Press, 2003, Section 6.10 Quadratic Recurrence Constants, p. 446."
			],
			"link": [
				"Jon E. Schoenfield, \u003ca href=\"/A268107/a268107.txt\"\u003eMagma program\u003c/a\u003e communicated to J.-F. Alcover.",
				"Eric Weisstein's MathWorld, \u003ca href=\"http://mathworld.wolfram.com/SomossQuadraticRecurrenceConstant.html\"\u003eSomos's Quadratic Recurrence Constant\u003c/a\u003e"
			],
			"formula": [
				"Conjecture: lambda is the radius of convergence of the function Sum_{n\u003e=0} A045761(n)*x^n, that is the constant 1/d computed by _Vaclav Kotesovec_ in A045761."
			],
			"example": [
				"0.39952466709679946552503347433225833221736985467599689773670052894853..."
			],
			"mathematica": [
				"n0 (* initial number of terms *) = 2*10^7; iter = 10^5; dn = 10^6; k1 = 0.3; k2 = 0.4; eps = 10^-16; f[k_?NumericQ] := (h0 = 0; h1 = k; h2 = k*(1+k); Do[h0 = h1; h1 = h2; h2 = Min[h1 + (h1-h0), h1*(1+h1-h0)], {iter}]; h2); Clear[g]; g[n0] = k1; g[n = n0+dn] = k2; g[n_] := g[n] = k /. FindRoot[f[k]==1, {k, g[n-dn] }]; While[Print[n, \" \", g[n] // RealDigits]; Abs[g[n] - g[n-dn]] \u003e eps, n = n+dn]; lambda = g[n]; RealDigits[lambda][[1]][[1;;9]]"
			],
			"program": [
				"(MAGMA) See the link to _Jon E. Schoenfield_'s program."
			],
			"xref": [
				"Cf. A045761, A085835, A112302."
			],
			"keyword": "cons,nonn",
			"offset": "0,1",
			"author": "_Jean-François Alcover_, Jan 26 2016",
			"ext": [
				"Extended to 127 digits using _Jon E. Schoenfield_'s evaluation, Aug 27 2016"
			],
			"references": 2,
			"revision": 48,
			"time": "2019-07-06T20:15:36-04:00",
			"created": "2016-01-26T14:29:46-05:00"
		}
	]
}