{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A039754",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 39754,
			"data": "1,1,1,1,1,1,2,1,1,1,1,3,3,6,3,3,1,1,1,1,4,6,19,27,50,56,74,56,50,27,19,6,4,1,1,1,1,5,10,47,131,472,1326,3779,9013,19963,38073,65664,98804,133576,158658,169112,158658,133576,98804,65664,38073,19963,9013,3779,1326,472,131,47,10,5,1,1",
			"name": "Irregular triangle read by rows: T(n,k) = number of binary codes of length n with k words (n \u003e= 0, 0 \u003c= k \u003c= 2^n); also number of 0/1-polytopes with vertices from the unit n-cube; also number of inequivalent Boolean functions of n variables with exactly k nonzero values under action of Jevons group.",
			"comment": [
				"For N=1 through N=5, the first 2^(N-1) terms of row N are also found in triangle A171871, which is related to A005646. This was shown for all N by _Andrew Weimholt_, Dec 30 2009. [_Robert Munafo_, Jan 25 2010]"
			],
			"reference": [
				"F. Harary and E. M. Palmer, Graphical Enumeration, Academic Press, NY, 1973, p. 112.",
				"M. A. Harrison, Introduction to Switching and Automata Theory. McGraw Hill, NY, 1965, p. 150."
			],
			"link": [
				"Jan Brandts, A. Cihangir, \u003ca href=\"http://arxiv.org/abs/1512.03044\"\u003eEnumeration and investigation of acute 0/1-simplices modulo the action of the hyperoctahedral group\u003c/a\u003e, arXiv preprint arXiv:1512.03044 [math.CO], 2015. See Fig. 13.",
				"D. Condon, S. Coskey, L. Serafin, C. Stockdale, \u003ca href=\"http://arxiv.org/abs/1412.4683\"\u003eOn generalizations of separating and splitting families\u003c/a\u003e, arXiv preprint arXiv:1412.4683 [math.CO], 2014-2015.",
				"Jacob Feldman, \u003ca href=\"http://ruccs.rutgers.edu/~jacob/Papers/feldman_catalog.pdf\"\u003eA catalog of Boolean concepts\u003c/a\u003e, Journal of Mathematical Psychology, Volume 47, Issue 1, 2003, 75-89.",
				"H. Fripertinger, \u003ca href=\"http://dx.doi.org/10.1023/A:1008248618779\"\u003eEnumeration, construction and random generation of block codes\u003c/a\u003e, Designs, Codes, Crypt., 14 (1998), 213-219.",
				"H. Fripertinger, \u003ca href=\"http://www.mathe2.uni-bayreuth.de/frib/codes/tables.html\"\u003eIsometry Classes of Codes\u003c/a\u003e",
				"Harald Fripertinger, \u003ca href=\"http://www.mathe2.uni-bayreuth.de/frib/html2/construction/blockcodes_2.html\"\u003eEnumeration of block codes\u003c/a\u003e",
				"Tilman Piesk, \u003ca href=\"https://commons.wikimedia.org/wiki/File:Venn_and_Euler_diagram_of_3-ary_Boolean_relations.svg\"\u003eIllustration of row 3\u003c/a\u003e",
				"\u003ca href=\"/index/Bo#Boolean\"\u003eIndex entries for sequences related to Boolean functions\u003c/a\u003e"
			],
			"formula": [
				"Reference gives g.f.",
				"Fripertinger gives g.f. for the number of classes of (n, m) nonlinear codes over an alphabet of size A."
			],
			"example": [
				"Triangle begins:",
				"  k     0   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16       sums",
				"n",
				"0       1                                                                          1",
				"1       1   1   1                                                                  3",
				"2       1   1   2   1   1                                                          6",
				"3       1   1   3   3   6   3   3   1   1                                         22",
				"4       1   1   4   6  19  27  50  56  74  56  50  27  19   6   4   1   1        402"
			],
			"mathematica": [
				"P = IntegerPartitions;",
				"AC[d_Integer] := Module[{C, M, p},(* from W. Y. C. Chen algorithm *) M[p_List] := Plus @@ p!/(Times @@ p * Times @@ (Length /@ Split[p]!)); C[p_List, q_List] := Module[{r, m, k, x}, r = If[0 == Length[q], 1, 2*2^IntegerExponent[LCM @@ q, 2]]; m = LCM @@ Join[p/GCD[r, p], q/GCD[r, q]]; CoefficientList[Expand[Product[(1 + x^(k *r))^((Plus @@ Map[MoebiusMu[k/#]*2^Plus @@ GCD[#*r, Join[p, q]]\u0026, Divisors[k]])/(k*r)), {k, 1, m}]], x]]; Sum[Binomial[d, p]*Plus @@ Plus @@ Outer[M[#1] M[#2] C[#1, #2]*2^(d - Length[#1] - Length[#2]) \u0026, P[p], P[d - p], 1], {p, 0, d}]/(d! 2^d)]; AC[0]  = {1};",
				"AC /@ Range[0, 5] // Flatten (* _Jean-François Alcover_, Dec 15 2019, after _Robert A. Russell_ in A034189 *)",
				"Table[ CoefficientList[ CycleIndexPolynomial[ GraphData[ {\"Hypercube\", n}, \"AutomorphismGroup\"], Array[Subscript[x, ##] \u0026, 2^n]] /. Table[ Subscript[x, i] -\u003e 1 + x^i, {i, 1, 2^n}], x], {n, 1,8}] // Grid (* _Geoffrey Critzer_, Jan 10 2020 *)"
			],
			"xref": [
				"Row sums give A000616. Cf. A052265.",
				"Rows give A034188, A034189, A034190, etc.",
				"For other versions of this triangle see A171876, A039754, A276777.",
				"Cf. A171871. [_Robert Munafo_, Jan 25 2010]"
			],
			"keyword": "nonn,tabf,nice",
			"offset": "0,7",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Corrected and extended by _Vladeta Jovovic_, Apr 20 2000",
				"Entry revised by _N. J. A. Sloane_, Sep 19 2016"
			],
			"references": 8,
			"revision": 39,
			"time": "2020-01-11T03:10:21-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}