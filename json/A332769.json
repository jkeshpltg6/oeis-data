{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A332769",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 332769,
			"data": "1,3,2,5,4,7,6,13,12,15,14,9,8,11,10,21,20,23,22,17,16,19,18,29,28,31,30,25,24,27,26,53,52,55,54,49,48,51,50,61,60,63,62,57,56,59,58,37,36,39,38,33,32,35,34,45,44",
			"name": "Permutation of the positive integers: a(n) = A258996(A054429(n)) = A054429(A258996(n)).",
			"comment": [
				"Sequence is self-inverse: a(a(n)) = n.",
				"A002487(1+a(n)) = A162911(n) and A002487(a(n)) = A162912(n). So, a(n) generates the enumeration system of positive rationals based on Stern's sequence A002487 called 'drib'.",
				"Given n, one can compute a(n) by taking into account the binary representation of n, and by flipping every second bit starting from the lowest until reaching the highest 1, which is not flipped."
			],
			"link": [
				"Yosu Yurramendi, \u003ca href=\"/A332769/b332769.txt\"\u003eTable of n, a(n) for n = 1..8191\u003c/a\u003e"
			],
			"formula": [
				"a(A054429(n)) = A054429(a(n)) = A258996(n),",
				"a(A258996(n)) = A258996(a(n)) = A054429(n).",
				"a(n) = A284447(A065190(n)) = A065190(A284447(n)),",
				"a(A065190(n)) = A065190(a(n)) = A284447(n),",
				"a(A284447(n)) = A284447(a(n)) = A065190(n).",
				"a(A231551(n)) = A154437(n), a(A154437(n)) = A231551(n).",
				"a(A153154(n)) = A284459(n), a(A284459(n)) = A153154(n).",
				"a(1) = 1, a(2) = 3, a(3) = 2; for n \u003e 3, a(2*n) = 2*a(A054429(n)) + 1, a(2*n+1) = 2*a(A054429(n)).",
				"a(1) = 1; for m \u003e= 0 and 0 \u003c= k \u003c 2^m, a(2^(m+1)+2*k) = 2*a(2^(m+1)-1-k) + 1, a(2^(m+1)+2*k+1) = 2*a(2^(m+1)-1-k)."
			],
			"example": [
				"n = 23 =  10111_2",
				"            x x",
				"          10010_2 = 18 = a(n).",
				"n = 33 = 100001_2",
				"          x x x",
				"         110100_2 = 52 = a(n)."
			],
			"program": [
				"(R)",
				"maxrow \u003c- 6 # by choice",
				"a \u003c- 1",
				"for(m in 0:maxrow) for(k in 0:(2^m-1)){",
				"a[2^(m+1)+2*k  ] \u003c- 2*a[2^(m+1)-1-k] + 1",
				"a[2^(m+1)+2*k+1] \u003c- 2*a[2^(m+1)-1-k]",
				"}",
				"a",
				"(R) # Given n, compute a(n) by taking into account the binary representation of n",
				"maxblock \u003c- 7 # by choice",
				"a \u003c- c(1, 3, 2)",
				"for(n in 4:2^maxblock){",
				"  ones \u003c- which(as.integer(intToBits(n)) == 1)",
				"  nbit \u003c- as.integer(intToBits(n))[1:tail(ones, n = 1)]",
				"  anbit \u003c- nbit",
				"  anbit[seq(1, length(anbit) - 1, 2)] \u003c- 1 - anbit[seq(1, length(anbit) - 1, 2)]",
				"  a \u003c- c(a, sum(anbit*2^(0:(length(anbit) - 1))))",
				"}",
				"a",
				"# _Yosu Yurramendi_, Mar 30 2021",
				"(PARI) a(n) = bitxor(n, 2\u003c\u003cbitor(logint(n,2)-1,1)\\3); \\\\ _Kevin Ryde_, Mar 30 2021"
			],
			"xref": [
				"Similar R-programs: A258996, A284447."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Yosu Yurramendi_, Feb 23 2020",
			"references": 4,
			"revision": 30,
			"time": "2021-04-24T18:51:54-04:00",
			"created": "2020-04-09T22:22:44-04:00"
		}
	]
}