{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A122252",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 122252,
			"data": "1,1,59,29,533,1577,280361,69311,36226519,7178335,64766889203,32128227179,459253205417,325788932161,2311165698322609,287144996287039,1215091897184850539,402833263943353393,476099430416027805187,236881416523193720213,650730651653461090091101",
			"name": "Binet's factorial series. Numerators of the coefficients of a convergent series for the logarithm of the Gamma function.",
			"link": [
				"Robert G. Wilson v, \u003ca href=\"/A122252/b122252.txt\"\u003eTable of n, a(n) for n = 1..100\u003c/a\u003e",
				"J. P. M. Binet, \u003ca href=\"https://gallica.bnf.fr/ark:/12148/bpt6k65355594.texteImage\"\u003eMémoire sur les intégrales définites Eulériennes et sur leur application à la théorie des suites ainsi qu'à l`évaluation des functions des grands nombres\u003c/a\u003e, Journal de l`École Polytechnique, XVI:123-343, July 1839.",
				"Ch. Hermite, \u003ca href=\"http://www.digizeitschriften.de/dms/img/?PID=GDZPPN002163330\"\u003eSur la function log Gamma(a)\u003c/a\u003e Journal für die reine und angewandte Mathematik, 115:201-208, 1895.",
				"G. Nemes, \u003ca href=\"https://doi.org/10.1080/10652469.2012.725168\"\u003eGeneralization of Binet's Gamma function formulas\u003c/a\u003e, Integral Transforms and Special Functions, 24(8):595-606, 2013.",
				"Raphael Schumacher, \u003ca href=\"http://arxiv.org/abs/1602.00336\"\u003eRapidly Convergent Summation Formulas involving Stirling Series\u003c/a\u003e, arXiv:1602.00336 [math.NT], 2016.",
				"P. Van Mieghem, \u003ca href=\"https://arxiv.org/abs/2102.04891\"\u003eBinet's factorial series and extensions to Laplace transforms\u003c/a\u003e, arXiv:2102.04891 [math.FA], 2021.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Stirling%27s_approximation\"\u003eStirling's Approximation\u003c/a\u003e"
			],
			"formula": [
				"a(n) = numerator(c(n)), where c(n) are given by Binet's formulas:",
				"log Gamma z = (z - 1/2) log z - z + log(2*Pi)/2 + Sum_{n \u003e= 1} c(n)/(z+1)^(n), where z^(n) is the rising factorial.",
				"c(n) = (1/n)*Integral_{x=0..1} x^(n)*(x - 1/2).",
				"a(n) = numerator((1/2n)*Sum_{j=1..n} (-1)^(n-j)*Stirling1(n,j)*j/((j+1)*(j+2))). - _Peter Luschny_, Sep 22 2021"
			],
			"example": [
				"Rational sequence starts: 1/12, 1/12, 59/360, 29/60, 533/280, 1577/168, 280361/5040, ...",
				"c(1) = Integral_{x=0..1} x*(x - 1/2) / 1 = Integral_{x=0..1} (x^2 - x/2) = (x^3/3 - x^2/4) | {x, 0, 1} = 1/12."
			],
			"maple": [
				"r := n -\u003e add((-1)^(n-j)*Stirling1(n,j)*j/((j+1)*(j+2)), j=1..n)/(2*n):",
				"a := n -\u003e numer(r(n)); seq(a(n), n=1..21); # _Peter Luschny_, Sep 22 2021"
			],
			"mathematica": [
				"Rising[z_, n_Integer/;n\u003e0] := z Rising[z + 1, n - 1]; Rising[z_, 0] := 1; c[n_Integer/;n\u003e0] := Integrate[Rising[x, n] (x - 1/2), {x, 0, 1}] / n; Numerator@ Array[c, 19] (* updated by _Robert G. Wilson v_, Aug 15 2015 *)"
			],
			"program": [
				"(PARI) a(n) = numerator(sum(j=1, n, (-1)^(n-j)*stirling(n,j,1)*j/((j+1)*(j+2)))/(2*n)); \\\\ _Michel Marcus_, Sep 22 2021"
			],
			"xref": [
				"Cf. A122253 (denominators), A001163, A001164."
			],
			"keyword": "easy,frac,nonn",
			"offset": "1,3",
			"author": "Paul Drees (zemyla(AT)gmail.com), Aug 27 2006",
			"ext": [
				"Edited by _Peter Luschny_, Sep 22 2021"
			],
			"references": 2,
			"revision": 32,
			"time": "2021-09-22T10:29:04-04:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}