{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003324",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3324,
			"id": "M0443",
			"data": "1,2,3,4,1,4,3,2,1,2,3,2,1,4,3,4,1,2,3,4,1,4,3,4,1,2,3,2,1,4,3,2,1,2,3,4,1,4,3,2,1,2,3,2,1,4,3,2,1,2,3,4,1,4,3,4,1,2,3,2,1,4,3,4,1,2,3,4,1",
			"name": "A nonrepetitive sequence.",
			"comment": [
				"Let b(0) be the sequence 1,2,3,4. Proceeding by induction, let b(n) be a sequence of length 2^(n+2). Quarter b(n) into four blocks, A,B,C,D each of length 2^n, so that b(n) = ABCD. Then b(n+1) = ABCDADCB. [After Dean paper.] - _Sean A. Irvine_, Apr 20 2015"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Sean A. Irvine, \u003ca href=\"/A003324/b003324.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Richard A. Dean, \u003ca href=\"http://www.jstor.org/stable/2313498\"\u003eA sequence without repeats on x, x^{-1}, y, y^{-1}\u003c/a\u003e, Amer. Math. Monthly 72, 1965. pp. 383-385. MR 31 #350.",
				"Françoise Dejean, \u003ca href=\"http://dx.doi.org/10.1016/0097-3165(72)90011-8\"\u003eSur un Theoreme de Thue\u003c/a\u003e, J. Combinatorial Theory, vol. 13 A, iss. 1 (1972) 90-99.",
				"N. J. A. Sloane, P. Flor, L. F. Meyers, G. A. Hedlund. M. Gardner, \u003ca href=\"/A001285/a001285.pdf\"\u003eCollection of documents and notes related to A1285, A3270, A3324\u003c/a\u003e",
				"Jianing Song, \u003ca href=\"/A003324/a003324.pdf\"\u003eProof for my formula for A003324\u003c/a\u003e"
			],
			"formula": [
				"a(n) = n mod 4 for odd n; for even n, write n = (2*k+1) * 2^e, then a(n) = 2 if k+e is odd, 4 if k+e is even. - _Jianing Song_, Apr 15 2021",
				"Conjecture: a(2*n) = (A292077(n)+1)*2. Confirmed for first 1000 terms. - _John Keith_, Apr 18 2021 [This conjecture is correct. Write n = (2*k+1) * 2^e. If k+e is even, then we have A292077(n) = 0 and a(2n) = 2; if k+e is odd, then we have A292077(n) = 1 and a(2n) = 4. - _Jianing Song_, Nov 27 2021]"
			],
			"mathematica": [
				"b[0] = Range[4];",
				"b[n_] := b[n] = Module[{aa, bb, cc, dd}, {aa, bb, cc, dd} = Partition[b[n - 1], 2^(n-1)]; Join[aa, bb, cc, dd, aa, dd, cc, bb] // Flatten];",
				"b[5] (* _Jean-François Alcover_, Sep 27 2017 *)",
				"a[n_] := If[OddQ[n], Mod[n, 4], Module[{e = IntegerExponent[n, 2], k}, k = (n/2^e - 1)/2; If[OddQ[k + e], 2, 4]]];",
				"Array[a, 100] (* _Jean-François Alcover_, Apr 19 2021, after _Jianing Song_ *)"
			],
			"program": [
				"(PARI) a(n) = if(n%2, n%4, my(e=valuation(n,2), k=bittest(n, e+1)); if((k+e)%2, 2, 4)) \\\\ _Jianing Song_, Apr 15 2021"
			],
			"xref": [
				"Positions of 1's, 2's, 3's and 4's: A016813, A343500, A004767, A343501.",
				"Cf. A292077."
			],
			"keyword": "nonn,nice,easy",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_.",
			"references": 10,
			"revision": 47,
			"time": "2021-11-27T03:49:38-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}