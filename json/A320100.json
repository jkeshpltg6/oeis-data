{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A320100",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 320100,
			"data": "4,16,44,61,4,16,64,176,244,16,64,201,324,556,44,176,324,736,1004,61,244,556,1004,1561,4,16,64,176,244,16,64,256,704,976,64,256,804,1296,2224,176,704,1296,2944,4016,244,976,2224,4016,6244,16,64,201,324,556",
			"name": "Automata sum similar to A102376 but using mod 5.",
			"comment": [
				"The automata that generates this sequence operates on a grid of cells c(i,j). The cells in the automata have five possible values, [0-4]. The next generation in the CA is calculated by applying the following rule to each cell: c(i,j) = ( c(i+1,j-1) + c(i+1,j+1) + c(i-1,j-1) + c(i-1,j+1) ) mod 5.",
				"Start with a single cell with a value of 1, with all other cells set to 0. For each generation, the term in this sequence c(n) is the aggregate values of all cells in the grid for each discrete generation of the automaton (i.e., not cumulative over multiple generations).",
				"The cellular automaton that generates this sequence has been empirically observed to repeat the number of active cells (4 in this case) if the iteration number N is a power of the modulus + 1. The modulus in this case is 5.",
				"This has been observed to occur with any prime mod and any starting pattern of cells. I'm picking this particular implementation because it's the same as the one used in A102376.",
				"Counting the active (nonzero) cells instead of taking the sum also creates a different but related sequence. This sequence is the sum of each iteration, and cells in this automaton have values 0, 1, 2, 3 or 4. Only for mod 2 are both the sum and active cell counts the same."
			],
			"link": [
				"Georg Fischer, \u003ca href=\"/A320100/b320100.txt\"\u003eTable of n, a(n) for n = 1..100\u003c/a\u003e"
			],
			"program": [
				"(Python)",
				"# requires scipy library (try pip install scipy)",
				"# more info: https://www.scipy.org/scipylib/download.html)",
				"import numpy as np",
				"from scipy import signal",
				"frameSize = 301",
				"filter = [[0, 1, 0], [1, 0, 1], [0, 1, 0]] # this defines the CA neighborhood",
				"frame  = np.zeros((frameSize, frameSize))",
				"frame[int(frameSize/2), int(frameSize/2)] = 1",
				"mod = 5",
				"sequence = []",
				"for j in range(140):",
				"    frame = signal.convolve2d(frame, filter, mode='same')",
				"    frame = np.mod(frame, mod)",
				"    # If you want to visualize the automaton you can use a tool",
				"    # like opencv (pip install opencv-python) to save the frame",
				"    # as an image each  iteration.",
				"    # i:e:(with other imports) import cv2",
				"    #     (inside loop)         cv2.imwrite('automatonFrame%s.png' % j, frame)",
				"    sequence.append(int(np.sum(frame.reshape(1, -1))))",
				"print(sequence)"
			],
			"xref": [
				"Cf. A102376 (mod 2), A320030 (mod 3)."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Nathan M Epstein_, Dec 10 2018",
			"references": 2,
			"revision": 75,
			"time": "2019-02-24T11:48:45-05:00",
			"created": "2019-02-10T17:26:00-05:00"
		}
	]
}