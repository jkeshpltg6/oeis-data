{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A225491",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 225491,
			"data": "0,4,5,6,6,6,7,7,7,7,7,7,7,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8",
			"name": "Maximal frequency depth for multisets over an alphabet of n letters.",
			"comment": [
				"Frequency depth is defined at A225485.  Suppose S is a multiset on an alphabet y(1),..,y(n).  Let f(n) \u003e 0 be the frequency of y(i) in S, so that F(S) (as at A225485) is the multiset {f(1),..,f(m)}, where m is the number of distinct terms in S.  Let {g(1),..,g(k)} be the set of distinct terms of F(S), and let h(i) be the number of occurrences of g(i) in F(S).  Then F(F(S)) is a partition p(m) of m, and D(F(F(S))) = D(p(m)), where D denotes frequency depth.  To maximize D for n\u003e1, put m = n to get a(n) = 2 + A225486(n), for n \u003e 1."
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A225491/b225491.txt\"\u003eTable of n, a(n) for n = 1..40\u003c/a\u003e"
			],
			"formula": [
				"a(1) = 0, a(n) = 2 + A225486(n) for n \u003e 1."
			],
			"example": [
				"For n = 2, let the alphabet be {u,v}.  Then for some p\u003e=0 and q\u003e=0, S consists of p u's and q v's, so that F(S) = {p,q}.  Assume without loss of generality that p\u003c=q.  If 1 \u003c= p \u003c q, then the depth of 4 is the number of arrows when we write S -\u003e pq -\u003e 11 -\u003e 2 -\u003e 1.  The other possibilities (p = 0, or p=q) for p and q lead to depths \u003c 4, so that a(2) = 4."
			],
			"mathematica": [
				"c[s_] := c[s] = Select[Table[Count[s, i], {i, 1, Max[s]}], # \u003e 0 \u0026]",
				"f[s_] := f[s] = Drop[FixedPointList[c, s], -2]",
				"t[s_] := t[s] = Length[f[s]]",
				"u[n_] := u[n] = Table[t[Part[IntegerPartitions[n], k]], {k, 1,",
				"     Length[IntegerPartitions[n]]}];",
				"v = Table[Max[u[n]], {n, 2, 40}]; (* A225491 *)",
				"Prepend[2 + v, 0]"
			],
			"xref": [
				"Cf. A225485, A225486."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Clark Kimberling_, May 09 2013",
			"references": 1,
			"revision": 5,
			"time": "2013-05-09T12:35:37-04:00",
			"created": "2013-05-09T12:35:37-04:00"
		}
	]
}