{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A059576",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 59576,
			"data": "1,1,1,2,3,2,4,8,8,4,8,20,26,20,8,16,48,76,76,48,16,32,112,208,252,208,112,32,64,256,544,768,768,544,256,64,128,576,1376,2208,2568,2208,1376,576,128,256,1280,3392,6080,8016,8016,6080,3392,1280,256",
			"name": "Summatory Pascal triangle T(n,k) (0 \u003c= k \u003c= n) read by rows. Top entry is 1. Each entry is the sum of the parallelogram above it.",
			"comment": [
				"We may also relabel the entries as U(0,0), U(1,0), U(0,1), U(2,0), U(1,1), U(0,2), U(3,0), ... [That is, T(n,k) = U(n-k, k) for 0 \u003c= k \u003c= n and U(m,s) = T(m+s, s) for m,s \u003e= 0.]",
				"From _Petros Hadjicostas_, Jul 16 2020: (Start)",
				"We explain the parallelogram definition of T(n,k).",
				"    T(0,0) *",
				"           |\\",
				"           | \\",
				"           |  * T(k,k)",
				"  T(n-k,0) *  |",
				"            \\ |",
				"             \\|",
				"              * T(n,k)",
				"The definition implies that T(n,k) is the sum of all T(i,j) such that (i,j) has integer coordinates over the set",
				"{(i,j): a(1,0) + b(1,1), 0 \u003c= a \u003c= n-k, 0 \u003c= b \u003c= k} - {(n,k)}.",
				"The parallelogram can sometimes be degenerate; e.g., when k = 0 or n = k. (End)",
				"T(n,k) is the number of 2-compositions of n having sum of the entries of the first row equal to k (0 \u003c= k \u003c= n). A 2-composition of n is a nonnegative matrix with two rows, such that each column has at least one nonzero entry and whose entries sum up to n. - _Emeric Deutsch_, Oct 12 2010",
				"From _Michel Marcus_ and _Petros Hadjicostas_, Jul 16 2020: (Start)",
				"Robeva and Sun (2020) let A(m,n) = U(m-1, n-1) be the number of subdivisions of a 2-row grid with m points on the top and n points at the bottom (and such that the lower left point is the origin).",
				"The authors proved that A(m,n) = 2*(A(m,n-1) + A(m-1,n) - A(m-1,n-1)) for m, n \u003e= 2 (with (m,n) \u003c\u003e (2,2)), which is equivalent to a similar recurrence for U(n,k) given in the Formula section below. (They did not explicitly specify the value of A(1,1) = U(0,0) because they did not care about the number of subdivisions of a degenerate polygon with only one side.)",
				"They also proved that, for (m,n) \u003c\u003e (1,1), A(m,n) = (2^(m-2)/(n-1)!) * Q_n(m) =",
				"= (2^(m-2)/(n-1)!) * Sum_{k=1..n} A336244(n,k) * m^(n-k), where Q_n(m) is a polynomial in m of degree n-1. (End)",
				"With the square array notation of Petros Hadjicostas, Jul 16 2020 below, U(i,j) is the number of lattice paths from (0,0) to (i,j) whose steps move north or east or have positive slope. For example, representing a path by its successive lattice points rather than its steps, U(1,2) = 8 counts {(0,0),(1,2)}, {(0,0),(0,1),(1,2)}, {(0,0),(0,2),(1,2)}, {(0,0),(1,0),(1,2)}, {(0,0),(1,1),(1,2)}, {(0,0),(0,1),(0,2),(1,2)}, {(0,0),(0,1),(1,1),(1,2)}, {(0,0),(1,0),(1,1),(1,2)}. If north (vertical) steps are excluded, the resulting paths are counted by A049600.  - _David Callan_, Nov 25 2021"
			],
			"reference": [
				"Fang, E., Jenkins, J., Lee, Z., Li, D., Lu, E., Miller, S. J., ... \u0026 Siktar, J. (2019). Central Limit Theorems for Compound Paths on the 2-Dimensional Lattice. arXiv preprint arXiv:1906.10645. Also Fib. Q., 58:1 (2020), 208-225."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A059576/b059576.txt\"\u003eRows n = 0..120 of triangle, flattened\u003c/a\u003e",
				"G. Castiglione, A. Frosini, E. Munarini, A. Restivo and S. Rinaldi, \u003ca href=\"http://dx.doi.org/10.1016/j.ejc.2006.06.020\"\u003eCombinatorial aspects of L-convex polyominoes\u003c/a\u003e, European Journal of Combinatorics, 28(6) (2007), 1724-1741; see Fig. 5, p. 1729.",
				"Elina Robeva and Melinda Sun, \u003ca href=\"https://arxiv.org/abs/2007.00877\"\u003eBimonotone Subdivisions of Point Configurations in the Plane\u003c/a\u003e, arXiv:2007.00877 [math.CO], 2020.",
				"\u003ca href=\"/index/Pas#Pascal\"\u003eIndex entries for triangles and arrays related to Pascal's triangle\u003c/a\u003e"
			],
			"formula": [
				"G.f.: U(z, w) = Sum_{n \u003e= 0, k \u003e= 0} U(n, k)*z^n*w^k = Sum{n \u003e= 0, k \u003e= 0} T(n, k)*z^(n-k)*w^k = (1-z)*(1-w)/(1 - 2*w - 2*z + 2*z*w).",
				"Maple code gives another explicit formula for U(n, k).",
				"From Jon Stadler (jstadler(AT)capital.edu), Apr 30 2003: (Start)",
				"U(n,k) is the number of ways of writing the vector (n,k) as an ordered sum of vectors, equivalently, the number of paths from (0,0) to (n,k) in which steps may be taken from (i,j) to (p,q) provided (p,q) is to the right or above (i,j).",
				"2*U(n,k) = Sum_{i \u003c= n, j \u003c= k} U(i,j).",
				"U(n,k) = 2*U(n-1,k) + Sum_{i \u003c k} U(n,i).",
				"U(n,k) = Sum_{j=0..n+k} C(n,j-k+1)*C(k,j-n+1)*2^j. (End)",
				"T(n, k) = 2*(T(n-1, k-1) + T(n-1, k)) - (2 - 0^(n-2))*T(n-2, k-1) for n \u003e 1 and 1 \u003c k \u003c n; T(n, 0) = T(n, n) = 2*T(n-1, 0) for n \u003e 0; and T(0, 0) = 1. - _Reinhard Zumkeller_, Dec 03 2004",
				"From _Emeric Deutsch_, Oct 12 2010: (Start)",
				"Sum_{k=0..n} k*T(n,k) = A181292(n).",
				"T(n,k) = Sum_{j=0..min(k, n-k)} (-1)^j*2^(n-j-1)*binomial(k, j)*binomial(n-j, k) for (n,k) != (0,0).",
				"G.f.: G(t,z) = (1-z)*(1-t*z)/(1 - 2*z - 2*t*z + 2*t*z^2). (End)",
				"U(n,k) = 0 if k \u003c 0; else U(k,n) if k \u003e n; else 1 if n \u003c= 1; else 3 if n = 2 and k = 1; else 2*U(n,k-1) + 2*U(n-1,k) - 2*U(n-1,k-1). - _David W. Wilson_; corrected in the case k \u003e n by _Robert Israel_, Jun 15 2011 [Corrected by _Petros Hadjicostas_, Jul 16 2020]",
				"U(n,k) = binomial(n,k) * 2^(n-1) * hypergeom([-k,-k], [n+1-k], 2) if n \u003e= k \u003e= 0 with (n,k) \u003c\u003e (0,0). - _Robert Israel_, Jun 15 2011 [Corrected by _Petros Hadjicostas_, Jul 16 2020]",
				"U(n,k) = Sum_{0 \u003c= i+j \u003c= n+k-1} (-1)^j*C(i+j+1, j)*C(n+i, n)*C(k+i, k). - _Masato Maruoka_, Dec 10 2019",
				"T(n, k) = 2^(n - 1)*binomial(n, k)*hypergeom([-k, k - n], [-n], 1/2) = A059474(n, k)/2 for n \u003e= 1. - _Peter Luschny_, Nov 26 2021"
			],
			"example": [
				"Triangle T(n,k) (with rows n \u003e= 0 and columns k = 0..n) begins",
				"[0]   1;",
				"[1]   1,   1;",
				"[2]   2,   3,   2;",
				"[3]   4,   8,   8,   4;",
				"[4]   8,  20,  26,  20,   8;",
				"[5]  16,  48,  76,  76,  48,  16;",
				"[6]  32, 112, 208, 252, 208, 112, 32;",
				"  ...",
				"T(5,2) = 76 is the sum of the elements above it in the parallelogram bordered by T(0,0), T(5-2,0) = T(3,0), T(2,2) and T(5,2). We of course exclude T(5,2) from the summation. Thus",
				"T(5,2) = Sum_{a=0..5-2, b=0..2, (a,b) \u003c\u003e (5-2,2)} T(a(1,0) + b(1,1)) =",
				"= (1 + 1 + 2) + (1 + 3 + 8) + (2 + 8 + 26) + (4 + 20) = 76. [Edited by _Petros Hadjicostas_, Jul 16 2020]",
				"From _Petros Hadjicostas_, Jul 16 2020: (Start)",
				"Square array U(n,k) (with rows n \u003e= 0 and columns k \u003e= 0) begins",
				"   1,   1,   2,    4,    8, ...",
				"   1,   3,   8,   20,   48, ...",
				"   2,   8,  26,   76,  208, ...",
				"   4,  20,  76,  252,  768, ...",
				"   8,  48, 208,  768, 2568, ...",
				"  16, 112, 544, 2208, 8016, ...",
				"  ...",
				"Consider the following 2-row grid with n = 3 points at the top and k = 2 points at the bottom:",
				"   A  B  C",
				"   *--*--*",
				"   |    /",
				"   |   /",
				"   *--*",
				"   D  E",
				"The sets of the dividing internal lines of the A(3,2) = U(3-1, 2-1) = 8 subdivisions of the above 2-row grid are as follows: { }, {DC}, {DB}, {EB}, {EA}, {DB, DC}, {DB, EB}, and {EA, EB}. See Robeva and Sun (2020).",
				"These are the 2-compositions of n = 3 with sum of first row entries equal to k = 1:",
				"[1; 2], [0,1; 2,0], [0,1; 1,1], [1,0; 0,2], [1,0; 1,1], [0,0,1; 1,1,0], [0,1,0; 1,0,1], and [1,0,0; 0,1,1]. We have T(3,2) = 8 such matrices. See _Emeric Deutsch_'s contribution above. See also Section 2 in Castiglione et al. (2007). (End)"
			],
			"maple": [
				"A059576 := proc(n,k) local b,t1; t1 := min(n+k-2,n,k); add( (-1)^b * 2^(n+k-b-2) * (n+k-b-2)! * (1/(b! * (n-b)! * (k-b)!)) * (-2 * n-2 * k+2 * k^2+b^2-3 * k * b+2 * n^2+5 * n * k-3 * n * b), b=0..t1); end;",
				"T := proc (n, k) if k \u003c= n then add((-1)^j*2^(n-j-1)*binomial(k, j)*binomial(n-j, k), j = 0 .. min(k, n-k)) fi end proc: 1; for n to 10 do seq(T(n, k), k = 0 .. n) end do; # yields sequence in triangular form # _Emeric Deutsch_, Oct 12 2010",
				"T := (n, k) -\u003e `if`(n=0, 1, 2^(n-1)*binomial(n, k)*hypergeom([-k, k - n], [-n], 1/2)): seq(seq(simplify(T(n, k)), k=0..n), n=0..10); # _Peter Luschny_, Nov 26 2021"
			],
			"mathematica": [
				"t[0, 0] = 1; t[n_, k_] := 2^(n-k-1)*n!*Hypergeometric2F1[ -k, -k, -n, -1 ] / (k!*(n-k)!); Flatten[ Table[ t[n, k], {n, 0, 9}, {k, 0, n}]] (* _Jean-François Alcover_, Feb 01 2012, after _Robert Israel_ *)"
			],
			"program": [
				"(Haskell)",
				"a059576 n k = a059576_tabl !! n !! k",
				"a059576_row n = a059576_tabl !! n",
				"a059576_tabl = [1] : map fst (iterate f ([1,1], [2,3,2])) where",
				"   f (us, vs) = (vs, map (* 2) ws) where",
				"     ws = zipWith (-) (zipWith (+) ([0] ++ vs) (vs ++ [0]))",
				"                      ([0] ++ us ++ [0])",
				"-- _Reinhard Zumkeller_, Dec 03 2012"
			],
			"xref": [
				"First diagonals give A000079, A001792.",
				"T(2*n, n) gives A052141. Row sums give A003480.",
				"Cf. A059474, A059473, A008288, A035002, A059226, A059283, A181292, A336244."
			],
			"keyword": "easy,nonn,tabl,nice",
			"offset": "0,4",
			"author": "_Floor van Lamoen_, Jan 23 2001",
			"references": 10,
			"revision": 94,
			"time": "2021-11-26T16:37:09-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}