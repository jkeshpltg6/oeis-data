{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A174109",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 174109,
			"data": "1,1,1,1,10,1,1,55,55,1,1,220,1210,220,1,1,715,15730,15730,715,1,1,2002,143143,572572,143143,2002,1,1,5005,1002001,13026013,13026013,1002001,5005,1,1,11440,5725720,208416208,677352676,208416208,5725720,11440,1",
			"name": "Triangle read by rows: T(n, k) = c(n, q)/(c(k, q)*c(n-k, q)), where c(n, q) = Product_{j=1..n} (j+q)!/(j-1)! and q = 8.",
			"comment": [
				"Triangle of generalized binomial coefficients (n,k)_9; cf. A342889. - _N. J. A. Sloane_, Apr 03 2021",
				"Row sums are: {1, 2, 12, 112, 1652, 32892, 862864, 28066040, 1105659414, 51177188350, 2734044648194, ...}.",
				"These sequences (q \u003e= 2) are a generalization of A056939."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A174109/b174109.txt\"\u003eRows n = 0..100 of triangle, flattened\u003c/a\u003e",
				"Johann Cigler, \u003ca href=\"https://arxiv.org/abs/2103.01652\"\u003ePascal triangle, Hoggatt matrices, and analogous constructions\u003c/a\u003e, arXiv:2103.01652 [math.CO], 2021.",
				"Johann Cigler, \u003ca href=\"https://www.researchgate.net/publication/349376205_Some_observations_about_Hoggatt_triangles\"\u003eSome observations about Hoggatt triangles\u003c/a\u003e, Universität Wien (Austria, 2021)."
			],
			"formula": [
				"T(n, k, q) = c(n, q)/(c(k, q)*c(n-k, q)) where c(n, q) = Product_{j=1..n} (j+q)!/(j-1)! and q=8.",
				"T(n, k, q) = (G(q+2)*G(k+1)*G(n+q+2)*G(n-k+1))/(G(n+1)*G(k+q+2)*G(n-k+q+ 2)), where G(x) is the Barnes G-function (see A000178). - _G. C. Greubel_, Apr 13 2019"
			],
			"example": [
				"Triangle begins as:",
				"  1.",
				"  1,     1.",
				"  1,    10,       1.",
				"  1,    55,      55,         1.",
				"  1,   220,    1210,       220,         1.",
				"  1,   715,   15730,     15730,       715,         1.",
				"  1,  2002,  143143,    572572,    143143,      2002,       1.",
				"  1,  5005, 1002001,  13026013,  13026013,   1002001,    5005,     1.",
				"  1, 11440, 5725720, 208416208, 677352676, 208416208, 5725720, 11440, 1."
			],
			"mathematica": [
				"c[n_, q_]:= Product[i+j, {j, 0, q}, {i, 1, n}];",
				"T[n_, m_, q_] = c[n, q]/(c[m, q]*c[n - m, q]);",
				"Table[T[n, k, 8], {n, 0, 10}, {k, 0, n}]//Flatten (* modified by _G. C. Greubel_, Apr 13 2019 *)",
				"T[n_, k_, q_]:= (BarnesG[n+q+2]*BarnesG[k+1]*BarnesG[n-k+1]*BarnesG[q+2] )/(BarnesG[n-k+q+2]*BarnesG[k+q+2]*BarnesG[n+1]);",
				"Table[T[n, k, 8], {n, 0, 10}, {k, 0, n}]//Flatten (* _G. C. Greubel_, Apr 13 2019 *)"
			],
			"program": [
				"(PARI) {c(m,q) = prod(j=1,m, (j+q)!/(j-1)!)};",
				"for(n=0,10, for(k=0,n, print1(c(n, 8)/(c(k, 8)*c(n-k, 8)), \", \"))) \\\\ _G. C. Greubel_, Apr 13 2019"
			],
			"xref": [
				"Cf. A056939 (q=2), A056940 (q=3), A056941 (q=4), A142465 (q=5), A142467 (q=6), A142468 (q=7), this sequence (q=8).",
				"Triangles of generalized binomial coefficients (n,k)_m (or generalized Pascal triangles) for m = 1,...,12: A007318 (Pascal), A001263, A056939, A056940, A056941, A142465, A142467, A142468, A174109, A342889, A342890, A342891."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Roger L. Bagula_, Mar 08 2010",
			"ext": [
				"Edited by _G. C. Greubel_, Apr 13 2019"
			],
			"references": 14,
			"revision": 21,
			"time": "2021-05-14T22:40:38-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}