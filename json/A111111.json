{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A111111",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 111111,
			"data": "1,2,0,2,6,46,338,2926,28146,298526,3454434,43286526,583835650,8433987582,129941213186,2127349165822,36889047574274,675548628690430,13030733384956418,264111424634864638,5612437196153963522,124789500579376435198,2897684052921851965442",
			"name": "Number of simple permutations of degree n.",
			"comment": [
				"A permutation is simple if the only intervals that are mapped onto intervals are the singletons and [1..n].",
				"For example, the permutation",
				"1234567",
				"2647513",
				"is not simple since it maps [2..5] onto [4..7].",
				"In other words, a permutation [1 ... n] -\u003e [p_1 p_2 ... p_n] is simple if there is no string of consecutive numbers [i_1 ... i_k] which is mapped onto a string of consecutive numbers [p_i_1 ... p_i_k] except for the strings of length k = 1 or n."
			],
			"reference": [
				"Corteel, Sylvie; Louchard, Guy; and Pemantle, Robin, Common intervals of permutations. in Mathematics and Computer Science. III, 3--14, Trends Math., Birkhuser, Basel, 2004.",
				"S. Kitaev, Patterns in Permutations and Words, Springer-Verlag, 2011. see p. 399 Table A.7",
				"Bridget Eileen Tenner, Interval posets of permutations, arXiv:2007.06142, Aug 2021."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A111111/b111111.txt\"\u003eTable of n, a(n) for n = 1..100\u003c/a\u003e",
				"M. H. Albert and M. D. Atkinson, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2005.06.016\"\u003eSimple permutations and pattern restricted permutations\u003c/a\u003e, Discr. Math., 300 (2005), 1-15.",
				"M. H. Albert, M. D. Atkinson and M. Klazar, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL6/Albert/albert.html\"\u003eThe enumeration of simple permutations\u003c/a\u003e, Journal of Integer Sequences 6 (2003), Article 03.4.4, 18 pages.",
				"Joerg Arndt, \u003ca href=\"/A111111/a111111.txt\"\u003eAll simple permutations for n \u003c= 6\u003c/a\u003e",
				"Michael Borinsky, \u003ca href=\"https://arxiv.org/abs/1603.01236\"\u003eGenerating asymptotics for factorially divergent sequences\u003c/a\u003e, arXiv preprint arXiv:1603.01236 [math.CO], 2016.",
				"M. Bouvel, M. Mishna, C. Nicaud, \u003ca href=\"http://www.liafa.univ-paris-diderot.fr/fpsac13/pdfAbstracts/dmAS0179.pdf\"\u003eSome simple varieties of trees arising in permutation analysis\u003c/a\u003e, FPSAC 2013 Paris, France DMTCS Proc. AS, 2013, 855-866.",
				"Robert Brignall, Sophie Huczynska, Vincent Vatter, \u003ca href=\"http://arxiv.org/abs/math/0608391\"\u003eSimple permutations and algebraic generating functions\u003c/a\u003e, arXiv:math/0608391 [math.CO], (2006).",
				"R. Brignall, S. Huczynska, V. Vatter, \u003ca href=\"http://dx.doi.org/10.1007/s00493-008-2314-0\"\u003eDecomposing simple permutations with enumerative consequences\u003c/a\u003e, Combinatorica, 28 (2008) 384-400.",
				"Robert Brignall, \u003ca href=\"http://arxiv.org/abs/0801.0963\"\u003eA Survey of Simple Permutations\u003c/a\u003e, arXiv:0801.0963 [math.CO], (18-April-2008)",
				"Sylvie Corteel, Guy Louchard, and Robin Pemantle, \u003ca href=\"http://www.dmtcs.org/dmtcs-ojs/index.php/dmtcs/article/view/536\"\u003eCommon intervals in permutations\u003c/a\u003e, Discrete Math. Theor. Comput. Sci. 8 (2006), no. 1, 189-216.",
				"Scott Garrabrant and Igor Pak, \u003ca href=\"http://www.math.ucla.edu/~pak/papers/PatternAvoid10.pdf\"\u003ePattern Avoidance is Not P-Recursive\u003c/a\u003e, preprint, 2015.",
				"V. Jelínek, P. Valtr, \u003ca href=\"http://arxiv.org/abs/1307.0027\"\u003eSplittings and Ramsey Properties of Permutation Classes\u003c/a\u003e, arXiv preprint arXiv:1307.0027 [math.CO], 2013.",
				"D. Oudrar, M. Pouzet, \u003ca href=\"http://arxiv.org/abs/1409.1108\"\u003eProfile and hereditary classes of ordered relational structures\u003c/a\u003e, arXiv preprint arXiv:1409.1108 [math.CO], 2014.",
				"Djamila Oudrar, \u003ca href=\"http://arxiv.org/abs/1604.05839\"\u003eSur l'énumération de structures discrètes, une approche par la théorie des relations\u003c/a\u003e, Thesis (in French), arXiv:1604.05839 [math.CO], 2016."
			],
			"formula": [
				"a(n) = -A059372(n)+2(-1)^(n+1).",
				"a(n) ~ n!*(1-4/n)/e^2. - _Jon E. Schoenfield_, Aug 05 2006",
				"a(n) ~ n!*exp(-2)*(1 - 4/n + 2/(n*(n-1)) - (40/3)/(n*(n-1)*(n-2)) - ...). Coefficients are given by A280780(n)/A280781(n).- _Gheorghe Coserea_, Jan 23 2017"
			],
			"example": [
				"G.f. = x + 2*x^2 + 2*x^4 + 6*x^5 + 46*x^6 + 338*x^7 + 2926*x^8 + ...",
				"The simple permutations of lowest degree are 1, 12, 21, 2413, 3142."
			],
			"mathematica": [
				"nmax = 20; t[n_, k_] := t[n, k] = Sum[(m + 1)!*t[n - m - 1, k - 1], {m, 0, n - k}]; t[n_, 1] = n!; t[n_, n_] = 1; tnk = Table[t[n, k], {n, 1, nmax}, {k, 1, nmax}]; A111111 = -Inverse[tnk][[All, 1]] + 2*(-1)^Range[0, nmax - 1]; A111111[[2]] = 2;",
				"A111111 (* _Jean-François Alcover_, Jul 13 2016 *)"
			],
			"program": [
				"(PARI) simple(v)=for(i=1,#v-1, for(j=i+1,#v, my(u=vecsort(v[i..j]));if(u[#u]-u[1]==#u-1 \u0026\u0026 #u\u003c#v, return(0)))); 1",
				"a(n)=sum(i=0,n!-1, simple(numtoperm(n,i))) \\\\ _Charles R Greathouse IV_, Nov 05 2013",
				"seq(N) = Vec(2 + 2*x^2 - 2/(1+x) - serreverse(x*Ser(vector(N, n, n!))));  \\\\ _Gheorghe Coserea_, Jan 22 2017"
			],
			"xref": [
				"Cf. A059372, A280780."
			],
			"keyword": "nonn,nice,changed",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, Oct 14 2005",
			"ext": [
				"Incorrect statement removed by _Jay Pantone_, Jul 16 2014",
				"Word \"fixed\" removed by _Franklin T. Adams-Watters_, Jul 22 2014"
			],
			"references": 11,
			"revision": 80,
			"time": "2022-01-16T11:08:42-05:00",
			"created": "2006-01-24T03:00:00-05:00"
		}
	]
}