{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003406",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3406,
			"id": "M0206",
			"data": "1,1,-1,2,-2,1,0,1,-2,0,2,0,-1,-2,2,1,0,-2,2,-2,0,0,3,0,-2,-2,1,0,2,0,0,0,-2,0,0,1,0,0,0,2,-1,0,-2,-2,0,4,0,2,-2,0,-2,-1,2,0,-2,2,0,1,0,0,0,0,-2,0,0,0,0,-2,4,2,-1,0,0,-2,-2,-2,2,1,2,0,0,0,0,-2,2,0,0,-2,2,-2,-2,0,3,0,0,2,0,0,0,-2,1,-2,0,-2,0",
			"name": "Expansion of Ramanujan's function R(x) = 1 + Sum_{n \u003e= 1} { x^(n*(n+1)/2) / ((1+x)(1+x^2)(1+x^3)...(1+x^n)) }.",
			"comment": [
				"a(n) = A117192(n) - A117193(n) for n\u003e0 (number of partitions into distinct parts with even rank minus those with odd rank); see also A000025. - _Reinhard Zumkeller_, Mar 03 2006",
				"Ramanujan showed that R(x) = 2*Sum_{n\u003e=0} (S(x) - P(n,x)) - 2*S(x)*D(x), where P(n,x) = Product_{k=1..n} (1+x^k), S(x) = g.f. A000009 = P(oo,x) and D(x) = -1/2 + Sum_{n\u003e=1} x^n/(1-x^n) = -1/2 + g.f. A000005. - Michael Somos"
			],
			"reference": [
				"G. E. Andrews, Ramanujan's \"lost\" notebook V: Euler's partition identity, Adv. in Math. 61 (1986), no. 2, 156-164; Math. Rev. 87i:11137. [ The expansion in (2.8) is incorrect. ]",
				"F. J. Dyson, A walk through Ramanujan's garden, pp. 7-28 of G. E. Andrews et al., editors, Ramanujan Revisited. Academic Press, NY, 1988.",
				"F. J. Dyson, Selected Papers, Am. Math. Soc., 1996, p. 200.",
				"B. Gordon and D. Sinor, Multiplicative properties of eta-products, Number theory, Madras 1987, pp. 173-200, Lecture Notes in Math., 1395, Springer, Berlin, 1989. see page 182. MR1019331 (90k:11050)",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A003406/b003406.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e (terms 0..2000 from T. D. Noe)",
				"G. E. Andrews, \u003ca href=\"http://www.jstor.org/stable/2322284\"\u003eQuestions and conjectures in partition theory\u003c/a\u003e, Amer. Math. Monthly, 93 (1986), 708-711.",
				"G. E. Andrews, \u003ca href=\"http://www.mat.univie.ac.at/~slc/s/s42andrews.html\"\u003eSome debts I owe\u003c/a\u003e, Séminaire Lotharingien de Combinatoire, Paper B42a, Issue 42, 2000.",
				"G. E. Andrews, F. J. Dyson and D. Hickerson, \u003ca href=\"http://dx.doi.org/10.1007/BF01388778\"\u003ePartitions and indefinite quadratic forms\u003c/a\u003e, Invent. Math. 91 (1988) 391-407.",
				"S.-Y. Kang, \u003ca href=\"http://jlms.oxfordjournals.org/content/75/1/18.abstract\"\u003eGeneralizations of Ramanujan's reciprocity theorem and their applications\u003c/a\u003e, J. London Math. Soc., 75 (2007), 18-34.",
				"Alexander E. Patkowski, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2009.10.001\"\u003eA note on the rank parity function\u003c/a\u003e, Discrete Math. 310 (2010), 961-965.",
				"D. Zagier, \u003ca href=\"http://people.mpim-bonn.mpg.de/zagier/files/qmf/fulltext.pdf\"\u003eQuantum modular forms\u003c/a\u003e, Example 1 in Quanta of Maths: Conference in honor of Alain Connes, Clay Mathematics Proceedings 11, AMS and Clay Mathematics Institute 2010, 659-675"
			],
			"formula": [
				"G.f.: 1 - Sum_{n \u003e 0} (-x)^n * (1 - x) * (1 - x^2) * ... * (1  -x^(n-1)).",
				"G.f.: 1 + Sum_{n\u003e=1}(x^(n(n+1)/2)/Product_{j=1..n}(1+x^j)). - _Emeric Deutsch_, Mar 30 2006",
				"Define c(24*k + 1) = A003406(k), c(24*k - 1) = -2*A003475(k), c(n) = 0 otherwise. Then c(n) is multiplicative with c(2^e) = c(3^e) = 0^e, c(p^e) = (-1)^(e/2) * (1+(-1)^e)/2 if p == 7, 17 (mod 24), c(p^e) = (1+(-1)^e)/2 if p == 5, 11, 13, 19 (mod 24), c(p^e) = (e+1)*(-1)^(y*e) where p == 1, 23 (mod 24) and p = x^2 - 72*y^2 . - _Michael Somos_, Aug 17 2006",
				"Also R(x) = -2 + Sum_{n\u003e=0} (n+1)*x^(n(n-1)/2)/(Product_{k=1..n} (1+x^k)). - _Paul D. Hanna_, May 22 2010"
			],
			"example": [
				"1 + x - x^2 + 2*x^3 - 2*x^4 + x^5 + x^7 - 2*x^8 + 2*x^10 - x^12 - 2*x^13 + ...",
				"q + q^25 - q^49 + 2*q^73 - 2*q^97 + q^121 + q^169 - 2*q^193 + 2*q^241 - ..."
			],
			"maple": [
				"g:=1+sum(x^(n*(n+1)/2)/product(1+x^j,j=1..n),n=1..20): gser:=series(g,x=0,110): seq(coeff(gser,x,n),n=0..104); # _Emeric Deutsch_, Mar 30 2006",
				"t1:= add( (-1)^n*q^(n*(3*n+1)/2)*(1-q^(2*n+1))* add( (-1)^j*q^(-j^2),j=-n..n), n=0..20); t2:=series(t1,q,40); # _N. J. A. Sloane_, Jun 27 2011"
			],
			"mathematica": [
				"max = 105; f[x_] := 1 + Sum[ x^(n*(n+1)/2) / Product[ 1+x^j, {j, 1, n}], {n, 1, max}]; CoefficientList[ Series[ f[x], {x, 0, max}], x] (* _Jean-François Alcover_, Dec 02 2011 *)",
				"max = 105; s = 1 + Sum[2*q^(n*(n+1)/2)/QPochhammer[-1, q, n+1], {n, 1, Ceiling[Sqrt[2 max]]}] + O[q]^max; CoefficientList[s, q] (* _Jean-François Alcover_, Nov 25 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = local(t); if( n\u003c0, 0, t = 1 + O(x^n); polcoeff( sum( k=1, n, t *= if( k\u003e1, x^k - x, x) + O(x^(n-k+2)), 1), n))} /* _Michael Somos_, Mar 07 2006 */",
				"(PARI) {a(n) = local(t); if( n\u003c0, 0, t = 1 + O(x^n); polcoeff( sum( k=1, (sqrtint(8*n + 1)-1)\\2, t *= x^k / (1 + x^k) + x * O(x^(n - (k^2-k)/2)), 1), n))} /* _Michael Somos_, Aug 17 2006 */",
				"(PARI) {a(n) = local(A, p, e, x, y); if( n\u003c0, 0, n = 24*n+1; A = factor(n); prod( k=1, matsize(A)[1], if( p=A[k, 1], e=A[k, 2]; if( p\u003c5, 0, if( p%24\u003e1 \u0026\u0026 p%24\u003c23, if(e%2, 0, if( p%24==7 || p%24==17, (-1)^(e/2), 1)), x=y=0; if( p%24==1, forstep(i=1, sqrtint(p), 2, if( issquare( (i^2+p)/2, \u0026y), x=i; break)), for( i=1, sqrtint(p\\2), if( issquare(2*i^2 + p, \u0026x), y=i; break))); (e+1)*(-1)^( (x + if((x-y)%6, y, -y))/6*e))))))} /* _Michael Somos_, Aug 17 2006 */"
			],
			"xref": [
				"Cf. A003475, A005895, A005896, A158690."
			],
			"keyword": "sign,easy,nice",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_",
			"references": 14,
			"revision": 59,
			"time": "2020-06-29T09:50:43-04:00",
			"created": "1998-06-14T03:00:00-04:00"
		}
	]
}