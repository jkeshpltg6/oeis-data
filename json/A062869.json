{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A062869",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 62869,
			"data": "1,1,1,1,2,3,1,3,7,9,4,1,4,12,24,35,24,20,1,5,18,46,93,137,148,136,100,36,1,6,25,76,187,366,591,744,884,832,716,360,252,1,7,33,115,327,765,1523,2553,3696,4852,5708,5892,5452,4212,2844,1764,576,1,8,42,164,524",
			"name": "Triangle read by rows: For n \u003e= 1, k \u003e= 0, T(n,k) is the number of permutations pi of n such that the total distance Sum_i abs(i-pi(i)) = 2k. Equivalently, k = Sum_i max(i-pi(i),0).",
			"comment": [
				"Number of possible values is 1,2,3,5,7,10,13,17,21,... = A033638. Maximum distance divided by 2 is the same minus one, i.e., 0,1,2,4,6,9,12,16,20,... = A002620.",
				"T. Kyle Petersen and Bridget Eileen Tenner proved that T(n,k) is also the number of permutations of n for which the sum of descent differences equals k. - _Susanne Wienand_, Sep 11 2014"
			],
			"reference": [
				"D. E. Knuth, The Art of Computer Programming, vol. 3, (1998), page 22 (exercise 28) and page 597 (solution and comments)."
			],
			"link": [
				"Daniel Graf and Alois P. Heinz, \u003ca href=\"/A062869/b062869.txt\"\u003eRows n = 1..50, flattened\u003c/a\u003e (First 30 rows from Alois P. Heinz)",
				"Max Alekseyev, \u003ca href=\"/A062869/a062869.txt\"\u003eProof that T(n,k) is even for k\u003e=n,\u003c/a\u003e SeqFan Mailing List, Dec 07 2006",
				"A. Bärtschi, B. Geissmann, D. Graf, T. Hruz, P. Penna, T. Tschager \u003ca href=\"https://arxiv.org/abs/1606.05538\"\u003eOn computing the total displacement number via weighted Motzkin paths\u003c/a\u003e, arXiv:1606.05538 [cs.DS], 2016. - _Daniel Graf_, Jun 20 2016",
				"P. Diaconis and R. L. Graham, \u003ca href=\"http://www-stat.stanford.edu/~cgates/PERSI/papers/77_04_spearmans.pdf\"\u003eSpearman's Footrule as a Measure of Disarray\u003c/a\u003e, J. Royal Stat. Soc. B, 39 (1977), 262-268.",
				"FindStat - Combinatorial Statistic Finder, \u003ca href=\"http://www.findstat.org/StatisticsDatabase/St000029\"\u003eThe depth of a permutation.\u003c/a\u003e, \u003ca href=\"http://www.findstat.org/StatisticsDatabase/St000030\"\u003eThe sum of the descent differences of a permutations.\u003c/a\u003e",
				"Mathieu Guay-Paquet and T. Kyle Petersen, \u003ca href=\"http://arxiv.org/abs/1404.4674\"\u003eThe generating function for total displacement\u003c/a\u003e, arXiv:1404.4674 [math.CO], 2014.",
				"M. Guay-Paquet, T. K. Petersen, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v21i3p37\"\u003eThe generating function for total displacement\u003c/a\u003e, The Electronic Journal of Combinatorics, 21(3) (2014), #P3.37.",
				"Dirk Liebhold, G Nebe, A Vazquez-Castro, \u003ca href=\"http://arxiv.org/abs/1612.07177\"\u003eNetwork coding and spherical buildings\u003c/a\u003e, arXiv preprint arXiv:1612.07177 [cs.IT], 2016.",
				"T. Kyle Petersen and Bridget Eileen Tenner, \u003ca href=\"http://arxiv.org/abs/1202.4765\"\u003eThe depth of a permutation\u003c/a\u003e, arXiv:1202.4765 [math.CO], 2012."
			],
			"formula": [
				"From _Mathieu Guay-Paquet_, Apr 30 2014: (Start)",
				"G.f.: 1/(1-z/(1-t*z/(1-2*t*z/(1-2*t^2*z/(1-3*t^2*z/(1-3*t^3*z/(1-4*t^3*z/(1-4*t^4*z/(...",
				"This is a continued fraction where the (2i)th numerator is (i+1)*t^i*z, and the (2i+1)st numerator is (i+1)*t^(i+1)*z for i \u003e= 0. The coefficient of z^n gives row n, n \u003e= 1, and the coefficient of t^k gives column k, k \u003e= 0. (End)"
			],
			"example": [
				"Triangle T(n,k) begins:",
				"1;",
				"1, 1;",
				"1, 2,  3;",
				"1, 3,  7,  9,  4;",
				"1, 4, 12, 24, 35, 24, 20; ...",
				"(4,3,1,2) has distances (3,1,2,2), sum is 8 and there are 4 other permutations of degree 4 with this sum."
			],
			"maple": [
				"# The following program yields the entries of the specified row n",
				"n := 9: with(combinat): P := permute(n): excsum := proc (p) (1/2)*add(abs(p[i]-i), i = 1 .. nops(p)) end proc: f[n] := sort(add(t^excsum(P[j]), j = 1 .. factorial(n))): seq(coeff(f[n], t, j), j = 0 .. floor((1/4)*n^2)); # _Emeric Deutsch_, Apr 02 2010",
				"# Maple program using the g.f. given by Guay-Paquey and Petersen:",
				"g:= proc(h, n) local i, j; j:= irem(h, 2, 'i');",
				"       1-`if`(h=n, 0, (i+1)*z*t^(i+j)/g(h+1, n))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, t, k), k=0..degree(p)))",
				"        (coeff(series(1/g(0, n), z, n+1), z, n)):",
				"seq(T(n), n=1..10);  # _Alois P. Heinz_, May 02 2014"
			],
			"mathematica": [
				"g[h_, n_] := Module[{i, j}, {i, j} = QuotientRemainder[h, 2]; 1 - If[h == n, 0, (i + 1)*z*t^(i + j)/g[h + 1, n]]]; T[n_] := Function[p, Table[ Coefficient[p, t, k], {k, 0, Exponent[p, t]}]][SeriesCoefficient[ 1/g[0, n], {z, 0, n}]]; Table[T[n], {n, 1, 10}] // Flatten (* _Jean-François Alcover_, Jan 07 2016, after _Alois P. Heinz_ *)",
				"f[i_] := If[i == 1, 1, -(i-1)^2 t^(2i - 3) z^2];",
				"g[i_] := 1 - (2i - 1) t^(i-1) z;",
				"cf = ContinuedFractionK[f[i], g[i], {i, 1, 5}];",
				"CoefficientList[#, t]\u0026 /@ CoefficientList[cf + O[z]^10, z] // Rest // Flatten (* _Jean-François Alcover_, Nov 25 2018, after _Mathieu Guay-Paquet_ *)"
			],
			"program": [
				"(Sage)",
				"# The following Sage program",
				"# yields the entries of the first n rows",
				"# as a list of lists",
				"def get_first_rows(n):",
				"    R, t = PolynomialRing(ZZ, 't').objgen()",
				"    S, z = PowerSeriesRing(R, 'z').objgen()",
				"    gf = S(1).add_bigoh(1)",
				"    for i in srange(n, 0, -1):",
				"        a = (i+1) // 2",
				"        b = i // 2",
				"        gf = 1 / (1 - a * t^b * z * gf)",
				"    return [list(row) for row in gf.shift(-1)]",
				"# _Mathieu Guay-Paquet_, Apr 30 2014"
			],
			"xref": [
				"Cf. A062866, A062867, A062870, A072949, A301897."
			],
			"keyword": "nonn,tabf,easy,look",
			"offset": "1,5",
			"author": "_Olivier Gérard_, Jun 26 2001",
			"references": 5,
			"revision": 98,
			"time": "2020-03-06T09:26:33-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}