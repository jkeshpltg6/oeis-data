{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A292696",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 292696,
			"data": "-1,6,4,21,44,126,319,846,2204,5781,15124,39606,103679,271446,710644,1860501,4870844,12752046,33385279,87403806,228826124,599074581,1568397604,4106118246,10749957119,28143753126,73681302244,192900153621,505019158604,1322157322206,3461452807999",
			"name": "a(n) = L(n)^2 - 5*(-1)^n = L(n+1)*L(n-1), where L = A000032.",
			"comment": [
				"This is the case k=1 of the identity L(n)^2 + 5*F(k)^2*(-1)^(n+k) = L(n+k)*L(n-k), where F = A000045. See also the comment in A292612."
			],
			"reference": [
				"Steven Vajda, Fibonacci and Lucas Numbers, and the Golden Section: Theory and Applications, Dover Publications (2008), page 29 (the formula 20b, for h=-k, gives the identity in Comments section)."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A292696/b292696.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Ron Knott, \u003ca href=\"http://www.maths.surrey.ac.uk/hosted-sites/R.Knott/Fibonacci/fibFormulae.html#section5.3\"\u003eFibonacci and Lucas Numbers\u003c/a\u003e (see 31st formula: the identity in the Comments section is the case i=-k).",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/LucasNumber.html\"\u003eLucas Number\u003c/a\u003e (see the formula n. 5).",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,2,-1)."
			],
			"formula": [
				"G.f.: (-1 + 8*x - 6*x^2)/((1 + x)*(1 - 3*x + x^2)).",
				"a(n) = a(-n) = 2*a(n-1) + 2*a(n-2) - a(n-3).",
				"a(n) = -A001654(n+1) + 8*A001654(n) - 6*A001654(n-1) with A001654(-1)=0.",
				"a(n) = L(2*n) - 3*(-1)^n.",
				"Sum_{i=0..n} a(i) = L(2*n+1) - (3*(-1)^n + 1)/2.",
				"a(n) = 2^(-n)*(-3*(-2)^n + (3-sqrt(5))^n + (3+sqrt(5))^n). - _Colin Barker_, Sep 21 2017",
				"From _Peter Bala_, Oct 14 2019: (Start)",
				"Sum_{n \u003e= 1} L(n)/a(n) = 3/2.",
				"Sum_{n \u003e= 1} (-1)^n*L(n)/a(n) = 1/2.  (End)",
				"From _Amiram Eldar_, Oct 06 2020: (Start)",
				"Sum_{n\u003e=1} 1/a(n) = 1/2.",
				"Sum_{n\u003e=1} (-1)^n/a(n) = 7/10 - 2*phi/5, where phi is the golden ratio (A001622). (End)"
			],
			"maple": [
				"A292696:=proc(n) option remember:",
				"if n=0 then -1 elif n=1 then 6 elif n=2 then 4 elif  n\u003e=3 then 2*procname(n-1)+2*procname(n-2)-procname(n-3) fi; end:",
				"seq(A292696(n),n=0..10^2); # _Muniru A Asiru_, Oct 03 2017"
			],
			"mathematica": [
				"Table[LucasL[n]^2 - 5 (-1)^n, {n, 0, 40}]",
				"LinearRecurrence[{2,2,-1},{-1,6,4},40] (* _Harvey P. Dale_, Oct 02 2018 *)"
			],
			"program": [
				"(Sage) [lucas(n)^2-5*(-1)^n for n in range(40)]",
				"(MAGMA) [Lucas(n)^2-5*(-1)^n: n in [0..40]];",
				"(PARI) Vec(-(1 - 8*x + 6*x^2) / ((1 + x)*(1 - 3*x + x^2)) + O(x^30)) \\\\ _Colin Barker_, Sep 21 2017",
				"(GAP)",
				"a:=[-1,6,4];; for n in [4..10^3] do a[n]:= 2*a[n-1]+2*a[n-2]-a[n-3]; od; A292696:=a; # _Muniru A Asiru_, Oct 03 2017"
			],
			"xref": [
				"Cf. A000032, A001622, A001654, A292612.",
				"Cf. A059929: Fibonacci(n+2)*Fibonacci(n)."
			],
			"keyword": "sign,easy",
			"offset": "0,2",
			"author": "_Bruno Berselli_, Sep 21 2017",
			"references": 1,
			"revision": 44,
			"time": "2020-10-06T04:02:28-04:00",
			"created": "2017-09-21T10:14:22-04:00"
		}
	]
}