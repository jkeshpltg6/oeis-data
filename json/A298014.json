{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A298014",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 298014,
			"data": "1,3,9,15,18,27,37,37,44,57,54,61,77,71,78,97,88,95,117,105,112,137,122,129,157,139,146,177,156,163,197,173,180,217,190,197,237,207,214,257,224,231,277,241,248,297,258,265,317,275,282,337,292,299,357,309,316,377,326,333,397,343,350,417,360",
			"name": "Coordination sequence of snub-632 tiling with respect to a trivalent node of type short-short-long.",
			"comment": [
				"The snub-632 tiling in also called the fsz-d net. It is the dual of the 3.3.3.3.6 Archimedean tiling.",
				"This is also called the \"6-fold pentille\" tiling in Conway, Burgiel, Goodman-Strauss, 2008, p. 288. - _Felix Fröhlich_, Jan 13 2018"
			],
			"reference": [
				"J. H. Conway, H. Burgiel and Chaim Goodman-Strauss, The Symmetries of Things, A K Peters, Ltd., 2008, ISBN 978-1-56881-220-5."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A298014/b298014.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Chaim Goodman-Strauss and N. J. A. Sloane, \u003ca href=\"https://doi.org/10.1107/S2053273318014481\"\u003eA Coloring Book Approach to Finding Coordination Sequences\u003c/a\u003e, Acta Cryst. A75 (2019), 121-134, also \u003ca href=\"http://NeilSloane.com/doc/Cairo_final.pdf\"\u003eon NJAS's home page\u003c/a\u003e. Also \u003ca href=\"http://arxiv.org/abs/1803.08530\"\u003earXiv:1803.08530\u003c/a\u003e.",
				"N. J. A. Sloane, \u003ca href=\"/A296368/a296368_2.png\"\u003eOverview of coordination sequences of Laves tilings\u003c/a\u003e [Fig. 2.7.1 of Grünbaum-Shephard 1987 with A-numbers added and in some cases the name in the RCSR database]",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,0,2,0,0,-1)."
			],
			"formula": [
				"For n \u003e= 5, let k=floor(n/3). Then a(3*k) = 20*k-3, a(3*k+1)=17*k+3, a(3*k+2)=17*k+10.",
				"G.f.: -(x+1)*(2*x^9+x^7-5*x^6-3*x^5-6*x^4-6*x^3-7*x^2-2*x-1)/((x-1)^2*(x^2+x+1)^2).",
				"a(n) = 2*a(n-3) - a(n-6) for n\u003e10. - _Colin Barker_, Jan 13 2018"
			],
			"maple": [
				"f:=proc(n) local k,r,L; L:=[1,3,9,15,18];",
				"if n\u003c5 then L[n+1]",
				"else k:=floor(n/3); r:=n-3*k;",
				"  if r=0 then 20*k-3 elif r=1 then 17*k+3 else 17*k+10; fi;",
				"fi; end;",
				"[seq(f(n),n=0..80)];"
			],
			"mathematica": [
				"Join[{1, 3, 9, 15, 18}, LinearRecurrence[{0, 0, 2, 0, 0, -1}, {27, 37, 37, 44, 57, 54}, 60]] (* _Jean-François Alcover_, Apr 28 2018 *)"
			],
			"program": [
				"(PARI) Vec(-(x+1)*(2*x^9+x^7-5*x^6-3*x^5-6*x^4-6*x^3-7*x^2-2*x-1)/((x-1)^2*(x^2+x+1)^2) + O(x^60)) \\\\ _Colin Barker_, Jan 13 2018"
			],
			"xref": [
				"Cf. A298015, A298016.",
				"List of coordination sequences for Laves tilings (or duals of uniform planar nets): [3,3,3,3,3.3] = A008486; [3.3.3.3.6] = A298014, A298015, A298016; [3.3.3.4.4] = A298022, A298024; [3.3.4.3.4] = A008574, A296368; [3.6.3.6] = A298026, A298028; [3.4.6.4] = A298029, A298031, A298033; [3.12.12] = A019557, A298035; [4.4.4.4] = A008574; [4.6.12] = A298036, A298038, A298040; [4.8.8] = A022144, A234275; [6.6.6] = A008458."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "Chaim Goodman-Strauss and _N. J. A. Sloane_, Jan 11 2018",
			"references": 22,
			"revision": 37,
			"time": "2020-01-19T18:13:17-05:00",
			"created": "2018-01-11T13:45:07-05:00"
		}
	]
}