{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A348611",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 348611,
			"data": "1,1,1,1,1,3,1,3,1,3,1,6,1,3,3,4,1,6,1,6,3,3,1,14,1,3,3,6,1,13,1,7,3,3,3,17,1,3,3,14,1,13,1,6,6,3,1,29,1,6,3,6,1,14,3,14,3,3,1,36,1,3,6,14,3,13,1,6,3,13,1,45,1,3,6,6,3,13,1,29,4,3",
			"name": "Number of ordered factorizations of n with no adjacent equal factors.",
			"comment": [
				"First differs from A348610 at a(24) = 14, A348610(24) = 12.",
				"An ordered factorization of n is a finite sequence of positive integers \u003e 1 with product n.",
				"In analogy with Carlitz compositions, these may be called Carlitz ordered factorizations."
			],
			"formula": [
				"a(n) = A074206(n) - A348616(n)."
			],
			"example": [
				"The a(n) ordered factorizations without adjacent equal factors for n = 1, 6, 12, 16, 24, 30, 32, 36 are:",
				"  ()   6     12      16      24      30      32      36",
				"       2*3   2*6     2*8     3*8     5*6     4*8     4*9",
				"       3*2   3*4     8*2     4*6     6*5     8*4     9*4",
				"             4*3     2*4*2   6*4     10*3    16*2    12*3",
				"             6*2             8*3     15*2    2*16    18*2",
				"             2*3*2           12*2    2*15    2*8*2   2*18",
				"                             2*12    3*10    4*2*4   3*12",
				"                             2*3*4   2*3*5           2*3*6",
				"                             2*4*3   2*5*3           2*6*3",
				"                             2*6*2   3*2*5           2*9*2",
				"                             3*2*4   3*5*2           3*2*6",
				"                             3*4*2   5*2*3           3*4*3",
				"                             4*2*3   5*3*2           3*6*2",
				"                             4*3*2                   6*2*3",
				"                                                     6*3*2",
				"                                                     2*3*2*3",
				"                                                     3*2*3*2",
				"Thus, of total A074206(12) = 8 ordered factorizations of 12, only factorizations 2*2*3 and 3*2*2 (see A348616) are not included in this count, therefore a(12) = 6. - _Antti Karttunen_, Nov 12 2021"
			],
			"mathematica": [
				"ordfacs[n_]:=If[n\u003c=1,{{}},Join@@Table[Prepend[#,d]\u0026/@ordfacs[n/d],{d,Rest[Divisors[n]]}]];",
				"antirunQ[y_]:=Length[y]==Length[Split[y]]",
				"Table[Length[Select[ordfacs[n],antirunQ]],{n,100}]"
			],
			"program": [
				"(PARI) A348611(n, e=0) = if(1==n, 1, my(s=0); fordiv(n, d, if((d\u003e1)\u0026\u0026(d!=e), s += A348611(n/d, d))); (s)); \\\\ _Antti Karttunen_, Nov 12 2021"
			],
			"xref": [
				"The additive version (compositions) is A003242, complement A261983.",
				"The additive alternating version is A025047, ranked by A345167.",
				"Factorizations without a permutation of this type are counted by A333487.",
				"As compositions these are ranked by A333489, complement A348612.",
				"Factorizations with a permutation of this type are counted by A335434.",
				"The non-alternating additive version is A345195, ranked by A345169.",
				"The alternating case is A348610, which is dominated at positions A122181.",
				"The complement is counted by A348616.",
				"A001055 counts factorizations, strict A045778, ordered A074206.",
				"A325534 counts separable partitions, ranked by A335433.",
				"A335452 counts anti-run permutations of prime indices, complement A336107.",
				"A339846 counts even-length factorizations.",
				"A339890 counts odd-length factorizations.",
				"A348613 counts non-alternating ordered factorizations.",
				"Cf. A001250, A138364, A336103, A347050, A347438, A347463, A347466, A347706, A348379, A348383."
			],
			"keyword": "nonn",
			"offset": "1,6",
			"author": "_Gus Wiseman_, Nov 07 2021",
			"references": 8,
			"revision": 17,
			"time": "2021-11-12T22:31:21-05:00",
			"created": "2021-11-12T22:31:21-05:00"
		}
	]
}