{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A157285",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 157285,
			"data": "1,2,2,6,12,18,28,84,336,1456,210,840,6300,88200,1874250,2604,13020,156240,4843440,377788320,59010535584,54684,328104,5741820,329197680,63946649340,39774815889480,61856467844122980,1984248,13889736",
			"name": "Triangle T(n, k, m) = (m+1)^n*t(n, m)*t(k, n-m)/(k! * (n-k)!), where T(0, k, m) = 1, t(n, k) = Product_{j=1..n} ( Sum_{i=0..j-1} (m+1)^i ), and t(n, 0) = n!, read by rows.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A157285/b157285.txt\"\u003eRows n = 0..30 of the triangle, flattened\u003c/a\u003e",
				"J. Baik, T. Kriecherbauer, K. D. T-R McLaughlin, P. D. Miller, \u003ca href=\"http://dx.doi.org/10.1155/S1073792803212125\"\u003eUniform asymptotics for polynomials orthogonal with respect to a general class of discrete weights and universality results for associated ensembles: announcement of results\u003c/a\u003e, International Mathematics Research Notices vol 2003, (2003) 821-858.",
				"V. Gorin, \u003ca href=\"http://arxiv.org/abs/0708.2349\"\u003eNon-intersecting paths and Hahn orthogonal polynomial ensemble\u003c/a\u003e, arXiv preprint arXiv:0708.2349 [math.PR], 2007."
			],
			"formula": [
				"T(n, k, m) = (m+1)^n*t(n, m)*t(k, n-m)/(k! * (n-k)!), where T(0, k, m) = 1, t(n, k) = Product_{j=1..n} ( Sum_{i=0..j-1} (m+1)^i ), and t(n, 0) = n!.",
				"T(n, k, m) = (1/n!)*binomial(n, k)*(m+1)^n*t(n, m)*t(k, n-m), with T(1, k, m) = 2, and t(n, k) = (1/m^n)*Product_{j=1..n} ((m+1)^j - 1). - _G. C. Greubel_, Jul 09 2021"
			],
			"example": [
				"Triangle begins as:",
				"     1;",
				"     2,     2;",
				"     6,    12,     18;",
				"    28,    84,    336,    1456;",
				"   210,   840,   6300,   88200,   1874250;",
				"  2604, 13020, 156240, 4843440, 377788320, 59010535584;"
			],
			"mathematica": [
				"(* First program *)",
				"t[n_, k_] = If[k==0, n!, Product[Sum[(k+1)^i, {i, 0, j-1}], {j, n}]];",
				"T[n_, k_, m_] = If[n==0, 1, ((m+1)^n*t[n, m]*t[k, n-m])/(k!*(n-k)!)];",
				"Flatten@Table[T[n, k, 1], {n,0,10}, {k,0,n}]",
				"(* Second program *)",
				"t[n_, m_] = (1/m^n)*Product[(m+1)^j - 1, {j,n}];",
				"T[n_, k_, m_] = If[n==1, 2, Binomial[n, k]*(m+1)^n*t[n, m]*t[k, n-m]/n!];",
				"Table[T[n, k, 1], {n,0,10}, {k,0,n}]//Flatten (* _G. C. Greubel_, Jul 09 2021 *)"
			],
			"program": [
				"(Sage)",
				"def t(n, m): return (1/m^n)*product( (m+1)^j -1 for j in (1..n) )",
				"def T(n,k,m): return 2 if n==1 else binomial(n,k)*(m+1)^n*t(n,m)*t(k,n-m)/factorial(n)",
				"flatten([[T(n,k,1) for k in (0..n)] for n in (0..10)]) # _G. C. Greubel_, Jul 09 2021"
			],
			"keyword": "nonn,tabl,easy,less",
			"offset": "0,2",
			"author": "_Roger L. Bagula_, Feb 26 2009",
			"ext": [
				"Edited by _G. C. Greubel_, Jul 09 2021"
			],
			"references": 2,
			"revision": 19,
			"time": "2021-07-09T22:12:39-04:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}