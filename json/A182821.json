{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A182821",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 182821,
			"data": "1,6,27,98,315,917,2486,6345,15427,35965,80897,176296,373652,772381,1561130,3091476,6008896,11480887,21591830,40016045,73157052,132052382,235535752,415433365,725043875,1252857043,2144601961,3638413830",
			"name": "Expansion of g.f.: exp( Sum_{n\u003e=1} sigma(5*n)*x^n/n ).",
			"comment": [
				"sigma(5*n) = A000203(5*n), the sum of divisors of 5n.",
				"Compare g.f. to P(x), the g.f. of partition numbers (A000041): P(x) = exp( Sum_{n\u003e=1} sigma(n)*x^n/n )."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A182821/b182821.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e"
			],
			"formula": [
				"G.f.: A(x) = E(x^5)/E(x)^6 where E(x) = Product_{k\u003e=1} (1-x^k). - _Joerg Arndt_, Dec 05 2010",
				"a(n) ~ 29^(3/2) * exp(sqrt(58*n/15)*Pi) / (2400*sqrt(3)*n^2). - _Vaclav Kotesovec_, Nov 28 2016",
				"A(x^5) = P(x)*P(a*x)*P(a^2*x)*P(a^3*x)*P(a^4*x), where P(x) = 1/Product_{n\u003e=1} (1 - x^n) is the g.f. for the partition function p(n) = A000041(n), and where a = exp(2*Pi*i/5) is a primitive fifth root of unity. - _Peter Bala_, Jan 24 2017"
			],
			"example": [
				"G.f.: A(x) = 1 + 6*x + 27*x^2 + 98*x^3 + 315*x^4 + 917*x^5 + ...",
				"log(A(x)) = 6*x + 18*x^2/2 + 24*x^3/3 + 42*x^4/4 + 31*x^5/5 + 72*x^6/6 + 48*x^7/7 + 90*x^8/8 + ... + sigma(5n)*x^n/n + ..."
			],
			"mathematica": [
				"nmax = 50; CoefficientList[Series[Product[(1 - x^(5*k))/(1 - x^k)^6, {k, 1, nmax}], {x, 0, nmax}], x] (* _Vaclav Kotesovec_, Nov 28 2016 *)"
			],
			"program": [
				"(PARI) {a(n)=polcoeff(exp(sum(m=1,n,sigma(5*m)*x^m/m)+x*O(x^n)),n)}",
				"(PARI) default(seriesprecision,66); Vec(eta(x^5)/eta(x)^6) \\\\ _Joerg Arndt_, Dec 05 2010",
				"(PARI) m=30; x='x+O('x^m); Vec(prod(j=1,m+2, (1 - x^(5*j))/(1 - x^j)^6)) \\\\ _G. C. Greubel_, Nov 18 2018",
				"(MAGMA) m:=30; R\u003cx\u003e:=PowerSeriesRing(Integers(), m); Coefficients(R!( (\u0026*[(1 - x^(5*j))/(1 - x^j)^6: j in [1..(m+2)]]) )); // _G. C. Greubel_, Nov 18 2018",
				"(Sage)",
				"R = PowerSeriesRing(ZZ, 'x')",
				"x = R.gen().O(30)",
				"s = prod((1 - x^(5*j))/(1 - x^j)^6 for j in (1..32))",
				"list(s) # _G. C. Greubel_, Nov 18 2018"
			],
			"xref": [
				"Cf. A000203, A000041; variants: A182818, A182819, A182820.",
				"Cf. Product_{n\u003e=1} (1 - x^(5*n))/(1 - x^n)^k: A035959 (k=1), A160461 (k=2), A277212 (k=5),",
				"this sequence (k=6)."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Paul D. Hanna_, Dec 05 2010",
			"references": 14,
			"revision": 33,
			"time": "2020-03-15T07:27:00-04:00",
			"created": "2010-12-05T06:23:43-05:00"
		}
	]
}