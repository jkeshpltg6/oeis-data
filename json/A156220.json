{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A156220",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 156220,
			"data": "-2,-2,3,-2,3,-1,-2,3,-1,109,-2,3,-1,325,1555523,-2,3,-1,973,32671835,49621794478165,-2,3,-1,2917,686126051,15630874866123949,27744919164118690798376051,-2,3,-1,8749,14408699579,4923725784550050421,270929135785330782929292449579,2134369240927848351630724472718209102550421",
			"name": "Triangle T(n, k) = (2^k/3)*Q(k, n), with T(0, 0) = -2, where Q(k, n) = (1/2)*( -Q(k-1, n) + 3*p(2, k-1)^n), and p(q, n) = Product_{j=1..n} ( (1-x^k)/(1-x) ), read by rows.",
			"comment": [
				"A triangle sequence based on Carlitz q-Eulerian formulas (see ref)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A156220/b156220.txt\"\u003eRows n = 0..25 of the triangle, flattened\u003c/a\u003e",
				"L. Carlitz, \u003ca href=\"https://projecteuclid.org/journals/duke-mathematical-journal/volume-15/issue-4/q-Bernoulli-numbers-and-polynomials/10.1215/S0012-7094-48-01588-9.short\"\u003eq-Bernoulli numbers and polynomials\u003c/a\u003e, Duke Math. J. Volume 15, Number 4 (1948), 987-1000."
			],
			"formula": [
				"T(n, k) = (2^k/3)*Q(k, n), with T(0, 0) = -2, where Q(k, n) = (1/2)*( -Q(k-1, n) + 3*p(2, k-1)^n), and p(q, n) = Product_{j=1..n} ( (1-q^k)/(1-q) )."
			],
			"example": [
				"Triangle begins as:",
				"  -2;",
				"  -2, 3;",
				"  -2, 3, -1;",
				"  -2, 3, -1,  109;",
				"  -2, 3, -1,  325,   1555523;",
				"  -2, 3, -1,  973,  32671835,    49621794478165;",
				"  -2, 3, -1, 2917, 686126051, 15630874866123949, 27744919164118690798376051;"
			],
			"mathematica": [
				"Q[x_, n_]:= Q[x, n]= If[n==0, 1, If[x==0, -6, (1/2)*(-Q[x-1, n] + 3*((-1)^(k-1)*QPochhammer[2, 2, x-1])^n)]];",
				"T[n_, k_]:= If[n==0, -2, (2^k/3)*Q[k, n]];",
				"Table[T[n, k], {n,0,10}, {k,0,n}]//Flatten (* modified by _G. C. Greubel_, Dec 31 2021 *)"
			],
			"program": [
				"(Sage)",
				"from sage.combinat.q_analogues import q_pochhammer",
				"@CachedFunction",
				"def Q(k,n):",
				"    if (n==0): return 1",
				"    elif (k==0): return -6",
				"    else: return (1/2)*( -Q(k-1,n) + 3*(-1)^(n*(k-1))*(q_pochhammer(k-1,2,2))^n)",
				"def T(n,k): return -2 if (n==0) else (2^k/3)*Q(k,n)",
				"flatten([[T(n,k) for k in (0..n)] for n in (0..10)]) # _G. C. Greubel_, Dec 31 2021"
			],
			"xref": [
				"Cf. A156222."
			],
			"keyword": "sign,tabl",
			"offset": "0,1",
			"author": "_Roger L. Bagula_, Feb 06 2009",
			"ext": [
				"Edited by _G. C. Greubel_, Dec 31 2021"
			],
			"references": 2,
			"revision": 10,
			"time": "2022-01-02T07:26:18-05:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}