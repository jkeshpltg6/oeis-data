{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A258833",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 258833,
			"data": "1,2,4,5,7,8,9,11,12,14,15,16,18,19,21,22,23,25,26,28,29,31,32,33,35,36,38,39,40,42,43,45,46,48,49,50,52,53,55,56,57,59,60,62,63,64,66,67,69,70,72,73,74,76,77,79,80,81,83,84,86,87,89,90,91,93",
			"name": "Nonhomogeneous Beatty sequence: ceiling((n + 1/4)*sqrt(2)).",
			"comment": [
				"Complement of A258834.",
				"Let r = sqrt(2) and s = r/(r-1) = 2 + sqrt(2). Let R be the ordered set {floor[(n + 1/4)*r] : n is an integer} and let S be the ordered set {floor[(n - 1/4)*s : n is an integer}; thus,",
				"R = (..., -8, -7, -5, -4, -2, -1, 1, 2, 3, 5, 6, ...)",
				"S = (..., -13, -10, -6, -3, 0, 4, 7, 11, 14, ...).",
				"By Fraenkel's theorem (Theorem XI in the cited paper); R and S partition the integers.",
				"A184580 = (1,2,3,5,6,...), positive terms of R;",
				"A184581 = (4,7,11,14,...), positive terms of S;",
				"A258833 = (1,2,4,5,6,...), - (negative terms of R);",
				"A258834 = (0,3,6,10,...), - (nonpositive terms of S).",
				"A184580 and A184581 partition the positive integers, and A258833 and A248834 partition the nonnegative integers."
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A258833/b258833.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"A. S. Fraenkel, \u003ca href=\"http://dx.doi.org/10.4153/CJM-1969-002-7\"\u003eThe bracket function and complementary sets of integers\u003c/a\u003e, Canadian J. of Math. 21 (1969) 6-27.",
				"Clark Kimberling, \u003ca href=\"https://www.emis.de/journals/INTEGERS/papers/q15/q15.Abstract.html\"\u003eBeatty sequences and trigonometric functions\u003c/a\u003e, Integers 16 (2016), #A15."
			],
			"formula": [
				"a(n) = ceiling((n + 1/4)*sqrt(2)) = floor((n + 1/4)*sqrt(2) + 1)."
			],
			"mathematica": [
				"r = Sqrt[2]; s = r/(r - 1);",
				"Table[Ceiling[(n + 1/4) r], {n, 0, 100}] (* A258833 *)",
				"Table[Ceiling[(n - 1/4) s], {n, 0, 100}] (* A258834 *)"
			],
			"program": [
				"(MAGMA) [Ceiling((n + 1/4)*Sqrt(2)): n in [0..80]]; // _Vincenzo Librandi_, Jun 13 2015",
				"(PARI) for(n=0,50, print1(ceil((n + 1/4)*sqrt(2)), \", \")) \\\\ _G. C. Greubel_, Feb 08 2018"
			],
			"xref": [
				"Cf. A258834 (complement), A184580, A184581."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Clark Kimberling_, Jun 12 2015",
			"references": 4,
			"revision": 27,
			"time": "2020-09-03T16:07:50-04:00",
			"created": "2015-06-16T13:55:24-04:00"
		}
	]
}