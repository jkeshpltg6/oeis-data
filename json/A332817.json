{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A332817",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 332817,
			"data": "1,2,4,3,8,9,6,5,16,27,18,25,12,15,10,7,32,81,54,125,36,75,50,49,24,45,30,35,20,21,14,13,64,243,162,625,108,375,250,343,72,225,150,245,100,147,98,169,48,135,90,175,60,105,70,91,40,63,42,65,28,39,26,11,128,729,486,3125,324,1875,1250,2401,216,1125,750,1715,500",
			"name": "a(n) = A108548(A163511(n)).",
			"comment": [
				"This irregular table can be represented as a binary tree. Each child to the left is obtained by doubling the parent, and each child to the right is obtained by applying A332818 to the parent:",
				"                                       1",
				"                                       |",
				"                    ...................2...................",
				"                   4                                       3",
				"         8......../ \\........9                   6......../ \\........5",
				"        / \\                 / \\                 / \\                 / \\",
				"       /   \\               /   \\               /   \\               /   \\",
				"      /     \\             /     \\             /     \\             /     \\",
				"    16       27         18       25         12       15         10       7",
				"  32 81    54  125    36  75   50  49     24  45   30  35     20  21   14 13",
				"etc.",
				"This is the mirror image of the tree in A332815."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A332817/b332817.txt\"\u003eTable of n, a(n) for n = 0..16383\u003c/a\u003e",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A108548(A163511(n)).",
				"For n \u003e= 1, a(n) = A332815(A054429(n))."
			],
			"program": [
				"(PARI)",
				"up_to = 26927;",
				"A005940(n) = { my(p=2, t=1); n--; until(!n\\=2, if((n%2), (t*=p), p=nextprime(p+1))); t }; \\\\ From A005940",
				"A054429(n) = ((3\u003c\u003c#binary(n\\2))-n-1); \\\\ From A054429",
				"A163511(n) = if(!n,1,A005940(1+A054429(n)));",
				"A108546list(up_to) = { my(v=vector(up_to), p,q); v[1] = 2; v[2] = 3; v[3] = 5; for(n=4,up_to, p = v[n-2]; q = nextprime(1+p); while(q%4 != p%4, q=nextprime(1+q)); v[n] = q); (v); };",
				"v108546 = A108546list(up_to);",
				"A108546(n) = v108546[n]; \\\\ _Antti Karttunen_, Mar 05 2020",
				"A108548(n) = { my(f=factor(n)); f[,1] = apply(A108546,apply(primepi,f[,1])); factorback(f); };",
				"A332817(n) = A108548(A163511(n));"
			],
			"xref": [
				"Cf. A332811 (inverse permutation).",
				"Cf. A054429, A108548, A163511, A332815 (mirror image).",
				"Cf. A108546 (the right edge of the tree from 2 downward).",
				"Cf. also A332214."
			],
			"keyword": "nonn,tabf",
			"offset": "0,2",
			"author": "_Antti Karttunen_, Mar 05 2020",
			"references": 7,
			"revision": 11,
			"time": "2020-03-10T00:29:04-04:00",
			"created": "2020-03-10T00:29:04-04:00"
		}
	]
}