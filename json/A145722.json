{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A145722",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 145722,
			"data": "1,1,3,4,8,12,21,30,48,68,102,143,207,284,400,542,744,996,1344,1776,2361,3088,4050,5248,6808,8742,11232,14310,18224,23052,29133,36601,45936,57360,71528,88812,110110,135990,167704,206108,252912,309408",
			"name": "Expansion of f(q) * f(q^5) / phi(-q^2)^2 in powers of q where f(), phi() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A145722/b145722.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Vaclav Kotesovec, \u003ca href=\"http://arxiv.org/abs/1509.08708\"\u003eA method of finding the asymptotics of q-series based on the convolution of generating functions\u003c/a\u003e, arXiv:1509.08708 [math.CO], Sep 30 2015",
				"MathOverflow, \u003ca href=\"http://mathoverflow.net/questions/230737/up-to-2000-a145722n-1-equiv-sigma4n-3-pmod5\"\u003eUp to 2000...\u003c/a\u003e, 2016.",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(-1/4) * eta(q^4) * eta(q^10)^3 / (eta(q) * eta(q^2) * eta(q^5) * eta(q^20)) in powers of q.",
				"Euler transform of period 20 sequence [ 1, 2, 1, 1, 2, 2, 1, 1, 1, 0, 1, 1, 1, 2, 2, 1, 1, 2, 1, 0, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (80 t)) = 20^(-1/2) g(t), where q = exp(2 Pi i t) and g() is the g.f. for A145723.",
				"G.f.: Product_{k\u003e0} (1 + x^(2*k)) * (1 - x^(10*k)) * (1 + x^(5*k)) / ((1 - x^k) * (1 + x^(10*k))).",
				"a(n) = A036026(2*n).",
				"a(n) ~ exp(2*Pi*sqrt(n/5)) / (4 * 5^(3/4) * n^(3/4)). - _Vaclav Kotesovec_, Oct 13 2015"
			],
			"example": [
				"G.f. = 1 + x + 3*x^2 + 4*x^3 + 8*x^4 + 12*x^5 + 21*x^6 + 30*x^7 + 48*x^8 + ...",
				"G.f. = q + q^5 + 3*q^9 + 4*q^13 + 8*q^17 + 12*q^21 + 21*q^25 + 30*q^29 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ -x]  QPochhammer[ -x^5] / EllipticTheta[ 4, 0, x^2]^2, {x, 0, n}]; (* _Michael Somos_, Aug 26 2015 *)",
				"nmax=60; CoefficientList[Series[Product[(1+x^(2*k)) * (1-x^(10*k)) * (1+x^(5*k)) / ((1-x^k) * (1 + x^(10*k))),{k,1,nmax}],{x,0,nmax}],x] (* _Vaclav Kotesovec_, Oct 13 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^4 + A) * eta(x^10 + A)^3 / (eta(x + A) * eta(x^2 + A) * eta(x^5 + A) * eta(x^20 + A)), n))};"
			],
			"xref": [
				"Cf. A036026."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Michael Somos_, Oct 23 2008",
			"references": 5,
			"revision": 20,
			"time": "2021-03-12T22:24:45-05:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}