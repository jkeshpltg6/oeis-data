{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A245938",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 245938,
			"data": "0,1,0,0,1,1,0,0,1,0,1,1,0,0,1,1,0,1,0,0,1,0,1,1,0,1,0,0,1,1,0,0,1,0,1,1,0,1,0,0,1,0,1,1,0,0,1,1,0,1,0,0,1,0,1,1,0,1,0,0,1,1,0,0,1,0,1,1,0,0,1,1,0,1,0,0,1,1,0,0,1,0,1,1,0,1",
			"name": "Limit-reverse of the Thue-Morse sequence (A010060), with first term as initial block.",
			"comment": [
				"Suppose S = (s(0),s(1),s(2),...) is an infinite sequence such that every finite block of consecutive terms occurs infinitely many times in S.  (It is assumed that A014675 is such a sequence.)  Let B = B(m,k) = (s(m-k),s(m-k+1),...,s(m)) be such a block, where m \u003e= 0 and k \u003e= 0.  Let m(1) be the least i \u003e m such that (s(i-k),s(i-k+1),...,s(i)) = B(m,k), and put B(m(1),k+1) = (s(m(1)-k-1),s(m(1)-k),...,s(m(1))).  Let m(2) be the least i \u003e m(1) such that (s(i-k-1),s(i-k),...,s(i)) = B(m(1),k+1), and put B(m(2),k+2) = (s(m(2)-k-2),s(m(2)-k-1),...,s(m(2))).  Continuing in this manner gives a sequence of blocks B(m(n),k+n).  Let B'(n) = reverse(B(m(n),k+n)), so that for n \u003e= 1, B'(n) comes from B'(n-1) by suffixing a single term; thus the limit of B'(n) is defined; we call it the \"limit-reverse of S with initial block B(m,k)\", denoted by S*(m,k), or simply S*.",
				"...",
				"The sequence (m(i)), where m(0) = 0, is the \"index sequence for limit-reversing S with initial block B(m,k)\" or simply the index sequence for S*, as in A245939."
			],
			"example": [
				"S = A010060 (re-indexed to start with s(0) = 1, with B = (s(0)); that is, (m,k) = (0,0); S = (0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, ...)",
				"B'(0) = (0)",
				"B'(1) = (0,1)",
				"B'(2) = (0,1,0)",
				"B'(3) = (0,1,0,0)",
				"B'(4) = (0,1,0,0,1)",
				"B'(5) = (0,1,0,0,1,1)",
				"S* = (0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1,...),",
				"with index sequence (0,3,5,12,20,36,60,92,108,132,...)"
			],
			"mathematica": [
				"z = 20; seqPosition2[list_, seqtofind_] := Last[Last[Position[Partition[list, Length[#], 1], Flatten[{___, #, ___}], 1, 2]]] \u0026[seqtofind]; Print[\"Thue-Morse sequence, to be limit-reversed:  0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 1, 0, 0, 1,...\"]",
				"n = 10; s = Nest[Flatten[# /. {0 -\u003e {0, 1}, 1 -\u003e {1, 0}}] \u0026, {0}, n];",
				"ans = Join[{s[[p[0] = pos = seqPosition2[s, #] - 1]]}, #] \u0026[{s[[1]]}]; cfs = Table[s = Drop[s, pos - 1]; ans = Join[{s[[p[n] = pos = seqPosition2[s, #] - 1]]}, #] \u0026[ans], {n, z}];",
				"Print[\"Before reversing:\"]",
				"cfs",
				"Print[\"After reversing:\"]",
				"Column[Map[Reverse, cfs]]",
				"Print[\"Positions where previous block repeats:\"]",
				"q = Accumulate[Join[{1}, Table[p[n], {n, 0, z}]]] (* A245937 *)"
			],
			"xref": [
				"Cf. A010060, A245939, A245920."
			],
			"keyword": "nonn",
			"offset": "0",
			"author": "_Clark Kimberling_ and _Peter J. C. Moses_, Aug 07 2014",
			"references": 1,
			"revision": 14,
			"time": "2014-08-22T10:13:04-04:00",
			"created": "2014-08-22T10:13:04-04:00"
		}
	]
}