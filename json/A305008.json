{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A305008",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 305008,
			"data": "1,1,1,1,1,0,1,2,-1,-2,1,2,-1,-4,-2,1,3,-3,-11,0,6,1,3,-3,-17,-8,20,16,1,4,-6,-32,1,64,20,-20,1,4,-6,-44,-19,140,136,-120,-132,1,5,-10,-70,5,301,152,-396,-280,28,1,5,-10,-90,-35,541,608,-1228,-1752,800,1216,1,6,-15,-130,15,966,643,-2798",
			"name": "Triangle read by rows of coefficients for functions and generating functions for the number of achiral color patterns (set partitions) for a row or loop of varying length using exactly n colors (sets).",
			"comment": [
				"Triangle begins with T(0,0).",
				"Two color patterns are equivalent if we permute the colors. Achiral color patterns must be equivalent if we reverse the order of the pattern.",
				"The generating function for exactly n colors (column n of A304972) is",
				"  x^n * Sum_{k=0..n} (T(n, k) * x^k) / Product_{k=1..n} (1 - k*x^2).",
				"Both the numerator and denominator of this g.f. have factors of (1+x) and (1-(n-2)*x^2) when n \u003e 2.",
				"Letting S2(m,n) be the Stirling subset number A008277(m,n), the function for exactly n colors for a row or loop of length m, A304972(m,n), n even, is",
				"  [m==0 mod 2] * Sum_{k=0..n/2} T(n, 2k) * S2((m+n)/2-k, n) +",
				"  [m==1 mod 2] * Sum_{k=1..n/2} T(n, 2k-1) * S2((m+n+1)/2-k, n).",
				"When n is odd, the function for A304972(m,n) is",
				"  [m==0 mod 2] * Sum_{k=0..(n-1)/2} T(n, 2k+1) * S2((m+n-1)-k, n) +",
				"  [m==1 mod 2] * Sum_{k=0..(n-1)/2} T(n, 2k) * S2((m+n)/2-k, n)."
			],
			"formula": [
				"T(n,k) = [1 \u003c= k \u003c= n] * (T(n-1, k-1) + T(n-2, k) - (n-1) * T(n-2, k-2)) + [k==0 \u0026 n\u003e=0]."
			],
			"example": [
				"Triangle begins:",
				"1;",
				"1, 1;",
				"1, 1,   0;",
				"1, 2,  -1,   -2;",
				"1, 2,  -1,   -4,  -2;",
				"1, 3,  -3,  -11,   0,   6;",
				"1, 3,  -3,  -17,  -8,  20,  16;",
				"1, 4,  -6,  -32,   1,  64,  20,   -20;",
				"1, 4,  -6,  -44, -19, 140, 136,  -120,  -132;",
				"1, 5, -10,  -70,   5, 301, 152,  -396,  -280,   28;",
				"1, 5, -10,  -90, -35, 541, 608, -1228, -1752,  800, 1216;",
				"1, 6, -15, -130,  15, 966, 643, -2798, -3028, 2236, 3600, 936;"
			],
			"mathematica": [
				"Coef[n_, -1] := Coef[n, -1] = 0; Coef[n_, 0] := Coef[n, 0] = Boole[n\u003e=0];",
				"Coef[n_, k_] := Coef[n, k] = If[k \u003e n, 0, Coef[n-1, k-1] + Coef[n-2, k] - (n-1) Coef[n-2, k-2]]",
				"Table[Coef[n, k], {n, 0, 30}, {k, 0, n}] // Flatten"
			],
			"xref": [
				"Coefficients for functions and generating functions of A304973, A304974, A304975, A304976, which are columns 3-6 of A304972."
			],
			"keyword": "sign,tabl,easy",
			"offset": "0,8",
			"author": "_Robert A. Russell_, May 23 2018",
			"references": 5,
			"revision": 22,
			"time": "2018-06-28T03:15:51-04:00",
			"created": "2018-06-28T03:15:51-04:00"
		}
	]
}