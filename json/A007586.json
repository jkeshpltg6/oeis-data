{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007586",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7586,
			"id": "M4835",
			"data": "0,1,12,42,100,195,336,532,792,1125,1540,2046,2652,3367,4200,5160,6256,7497,8892,10450,12180,14091,16192,18492,21000,23725,26676,29862,33292,36975,40920,45136,49632,54417,59500,64890,70596,76627,82992,89700,96760,104181",
			"name": "11-gonal (or hendecagonal) pyramidal numbers: n*(n+1)*(3*n-2)/2.",
			"comment": [
				"Starting with 1 equals binomial transform of [1, 11, 19, 9, 0, 0, 0, ...]. - _Gary W. Adamson_, Nov 02 2007"
			],
			"reference": [
				"A. H. Beiler, Recreations in the Theory of Numbers, Dover, NY, 1964, p. 194.",
				"E. Deza and M. M. Deza, Figurate numbers, World Scientific Publishing (2012), page 93.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A007586/b007586.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-6,4,-1)."
			],
			"formula": [
				"G.f.: x*(1+8*x)/(1-x)^4.",
				"a(0)=0, a(1)=1, a(2)=12, a(3)=42; for n\u003e3, a(n) = 4*a(n-1) - 6*a(n-2) + 4*a(n-3) - a(n-4). - _Harvey P. Dale_, Apr 09 2012",
				"a(n) = Sum_{i=0..n-1}  (n-i)*(9*i+1), with a(0)=0. - _Bruno Berselli_, Feb 10 2014",
				"From _Amiram Eldar_, Jun 28 2020: (Start)",
				"Sum_{n\u003e=1} 1/a(n) = (9*log(3) + sqrt(3)*Pi - 4)/10.",
				"Sum_{n\u003e=1} (-1)^(n+1)/a(n) = (sqrt(3)*Pi + 2 - 4*log(2))/5. (End)"
			],
			"example": [
				"From _Vincenzo Librandi_, Feb 12 2014: (Start)",
				"After 0, the sequence is provided by the row sums of the triangle (see above, third formula):",
				"  1;",
				"  2, 10;",
				"  3, 20, 19;",
				"  4, 30, 38, 28;",
				"  5, 40, 57, 56, 37;",
				"  6, 50, 76, 84, 74, 46; etc. (End)"
			],
			"maple": [
				"seq(n*(n+1)*(3*n-2)/2, n=0..45); # _G. C. Greubel_, Aug 30 2019"
			],
			"mathematica": [
				"Table[n(n+1)(3n-2)/2,{n,0,45}] (* or *) LinearRecurrence[{4,-6,4,-1},{0,1,12,42}, 45] (* _Harvey P. Dale_, Apr 09 2012 *)",
				"CoefficientList[Series[x(1+8x)/(1-x)^4, {x, 0, 45}], x] (* _Vincenzo Librandi_, Feb 12 2014 *)"
			],
			"program": [
				"(MAGMA) I:=[0,1,12,42]; [n le 4 select I[n] else 4*Self(n-1)-6*Self(n-2)+4*Self(n-3)-Self(n-4) : n in [1..50]]; // _Vincenzo Librandi_, Feb 12 2014",
				"(PARI) a(n)=n*(n+1)*(3*n-2)/2 \\\\ _Charles R Greathouse IV_, Oct 07 2015",
				"(Sage) [n*(n+1)*(3*n-2)/2 for n in (0..45)] # _G. C. Greubel_, Aug 30 2019",
				"(GAP) List([0..45], n-\u003e n*(n+1)*(3*n-2)/2); # _G. C. Greubel_, Aug 30 2019"
			],
			"xref": [
				"Cf. A051682.",
				"Cf. A093644 ((9, 1) Pascal, column m=3).",
				"Cf. similar sequences listed in A237616."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_, _R. K. Guy_",
			"ext": [
				"More terms from _Vincenzo Librandi_, Feb 12 2014"
			],
			"references": 11,
			"revision": 52,
			"time": "2020-06-28T02:52:52-04:00",
			"created": "1994-09-19T03:00:00-04:00"
		}
	]
}