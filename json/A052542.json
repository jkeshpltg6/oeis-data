{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A052542",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 52542,
			"data": "1,2,4,10,24,58,140,338,816,1970,4756,11482,27720,66922,161564,390050,941664,2273378,5488420,13250218,31988856,77227930,186444716,450117362,1086679440,2623476242,6333631924,15290740090,36915112104,89120964298,215157040700",
			"name": "a(n) = 2*a(n-1) + a(n-2), with a(0) = 1, a(1) = 2, a(2) = 4.",
			"comment": [
				"Apart from the initial 1, this sequence is simply twice the Pell numbers, A000129. - _Antonio Alberto Olivares_, Dec 31 2003",
				"Image of 1/(1-2x) under the mapping g(x) -\u003e g(x/(1+x^2)). - _Paul Barry_, Jan 16 2005",
				"The intermediate convergents to 2^(1/2) begin with 4/3, 10/7, 24/17, 58/41; essentially, numerators = A052542 and denominators = A001333. - _Clark Kimberling_, Aug 26 2008",
				"a(n) is the number of generalized compositions of n+1 when there are 2*i-2 different types of i, (i=1,2,...). - _Milan Janjic_, Aug 26 2010",
				"Apart from the initial 1, this is the p-INVERT transform of (1,0,1,0,1,0,...) for p(S) = 1 - 2 S. See A291219. - _Clark Kimberling_, Sep 02 2017",
				"Conjecture: Apart from the initial 1, a(n) is the number of compositions of two types of n having no even parts. - _Gregory L. Simay_, Feb 17 2018",
				"For n\u003e0, a(n+1) is the length of tau^n(10) where tau is the morphism: 1 -\u003e 101, 0 -\u003e 1. See Song and Wu. - _Michel Marcus_, Jul 21 2020"
			],
			"link": [
				"Iain Fox, \u003ca href=\"/A052542/b052542.txt\"\u003eTable of n, a(n) for n = 0..2500\u003c/a\u003e (first 1001 terms from Vincenzo Librandi)",
				"C. Banderier and D. Merlini, \u003ca href=\"http://algo.inria.fr/banderier/Papers/infjumps.ps\"\u003eLattice paths with an infinite set of jumps\u003c/a\u003e, FPSAC02, Melbourne, 2002.",
				"C. P. de Andrade, J. P. de Oliveira Santos, E. V. P. da Silva and K. C. P. Silva, \u003ca href=\"http://dx.doi.org/10.4236/ojdm.2013.31006\"\u003ePolynomial Generalizations and Combinatorial Interpretations for Sequences Including the Fibonacci and Pell Numbers\u003c/a\u003e, Open Journal of Discrete Mathematics, 2013, 3, 25-32 doi:10.4236/ojdm.2013.31006. - From _N. J. A. Sloane_, Feb 20 2013",
				"Massimiliano Fasi, Gian Maria Negri Porzio, \u003ca href=\"http://eprints.maths.manchester.ac.uk/2709/\"\u003eDeterminants of Normalized Bohemian Upper Hessemberg Matrices\u003c/a\u003e, University of Manchester (England, 2019).",
				"I. M. Gessel, Ji Li, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL16/Gessel/gessel6.html\"\u003eCompositions and Fibonacci identities\u003c/a\u003e, J. Int. Seq. 16 (2013) 13.4.5.",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=477\"\u003eEncyclopedia of Combinatorial Structures 477\u003c/a\u003e",
				"S. Kitaev and J. Remmel, \u003ca href=\"http://arxiv.org/abs/1305.6970\"\u003eThe 1-box pattern on pattern avoiding permutations\u003c/a\u003e, arXiv:1305.6970 [math.CO], 2013.",
				"Haocong Song and Wen Wu, \u003ca href=\"https://arxiv.org/abs/2007.09940\"\u003eHankel determinants of a Sturmian sequence\u003c/a\u003e, arXiv:2007.09940 [math.CO], 2020. See p.2 and 4.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,1)."
			],
			"formula": [
				"G.f.: (1 - x^2)/(1 - 2*x - x^2).",
				"Recurrence: a(0)=1, a(2)=4, a(1)=2, a(n) + 2*a(n+1) - a(n+2) = 0;",
				"a(n) = Sum_{alpha = RootOf(-1+2*x+x^2)} (1/2)*(1-alpha)*alpha^(-n-1).",
				"a(n) = 2*A001333(n-1) + a(n-1), n \u003e 1. A001333(n)/a(n) converges to sqrt(1/2). - Mario Catalani (mario.catalani(AT)unito.it), Apr 29 2003",
				"Binomial transform of A094024. a(n) = 0^n + ((1 + sqrt(2))^n - (1 - sqrt(2))^n)/sqrt(2). - _Paul Barry_, Apr 22 2004",
				"a(n) = Sum_{k=0..floor(n/2)} binomial(n-k-1, k)2^(n-2k). - _Paul Barry_, Jan 16 2005",
				"If p[i] = 2modp(i,2) and if A is Hessenberg matrix of order n defined by A[i,j] = p[j-i+1], (i \u003c= j), A[i,j] = -1, (i=j+1), and A[i,j] = 0 otherwise. Then, for n \u003e= 1, a(n) = det A. - _Milan Janjic_, May 02 2010",
				"G.f.: 1 + x + x^2/(2*G(0)-x) where G(k) = 1 - (k+1)/(1 - x/(x +(k+1)/G(k+1) )); (continued fraction). - _Sergei N. Gladkovskii_, Dec 07 2012",
				"G.f.: G(0)*(1-x)/(2*x) + 1 - 1/x, where G(k)= 1 + 1/(1 - x*(2*k-1)/(x*(2*k+1) - 1/G(k+1))); (continued fraction). - _Sergei N. Gladkovskii_, May 26 2013",
				"G.f.: 1 + G(0)*x/(1-x), where G(k) = 1 + 1/(1 - x*(2*k-1)/(x*(2*k+1) - 1/G(k+1))); (continued fraction). - _Sergei N. Gladkovskii_, Jun 19 2013",
				"G.f.: 1 + (1+G(0))/(2-2*x), where G(k) = 2*x*(k+2) - 1 - x + x*(2*k-1)/G(k+1); (continued fraction). - _Sergei N. Gladkovskii_, Aug 14 2013",
				"G.f.: Q(0), where Q(k) = 1 + (1+x)*x + (2*k+3)*x - x*(2*k+1 + x+1)/Q(k+1); (continued fraction). - _Sergei N. Gladkovskii_, Oct 04 2013",
				"a(n) = round(sqrt(Pell(2n) + Pell(2n-1))). - _Richard R. Forberg_, Jun 22 2014",
				"a(n) = 2*A000129(n) + A000007(n) - _Iain Fox_, Nov 30 2017",
				"a(n) = A000129(n) - A000129(n-2). - _Gregory L. Simay_, Feb 17 2018"
			],
			"maple": [
				"spec := [S,{S=Sequence(Prod(Union(Z,Z),Sequence(Prod(Z,Z))))},unlabeled]: seq(combstruct[count](spec,size=n), n=0..20);",
				"A052542 := proc(n)",
				"    option remember;",
				"    if n \u003c=2 then",
				"        2^n;",
				"    else",
				"        2*procname(n-1)+procname(n-2) ;",
				"    end if;",
				"end proc: # _R. J. Mathar_, Sep 23 2016"
			],
			"mathematica": [
				"Join[{1}, LinearRecurrence[{2, 1}, {2, 4}, 40]] (* _Vladimir Joseph Stephan Orlovsky_, Feb 22 2012 *)"
			],
			"program": [
				"(PARI) Vec((1-x^2)/(1-2*x-x^2) +O(x^40)) \\\\ _Charles R Greathouse IV_, Nov 20 2011",
				"(Haskell)",
				"a052542 n = a052542_list !! n",
				"a052542_list = 1 : 2 : 4 : tail (zipWith (+)",
				"               (map (* 2) $ tail a052542_list) a052542_list)",
				"-- _Reinhard Zumkeller_, Feb 24 2015",
				"(MAGMA) I:=[2,4]; [n le 2 select I[n] else 2*Self(n-1) +Self(n-2): n in [1..40]]; // _G. C. Greubel_, May 09 2019",
				"(Sage) ((1-x^2)/(1-2*x-x^2)).series(x, 40).coefficients(x, sparse=False) # _G. C. Greubel_, May 09 2019",
				"(GAP) a:=[2,4];; for n in [3..40] do a[n]:=2*a[n-1]+a[n-2]; od; a; # _G. C. Greubel_, May 09 2019"
			],
			"xref": [
				"Cf. A052906."
			],
			"keyword": "easy,nonn",
			"offset": "0,2",
			"author": "encyclopedia(AT)pommard.inria.fr, Jan 25 2000",
			"references": 24,
			"revision": 118,
			"time": "2020-07-21T06:00:50-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}