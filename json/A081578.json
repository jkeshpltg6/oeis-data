{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A081578",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 81578,
			"data": "1,1,1,1,5,1,1,9,9,1,1,13,33,13,1,1,17,73,73,17,1,1,21,129,245,129,21,1,1,25,201,593,593,201,25,1,1,29,289,1181,1921,1181,289,29,1,1,33,393,2073,4881,4881,2073,393,33,1,1,37,513,3333,10497,15525,10497,3333,513,37,1",
			"name": "Pascal-(1,3,1) array.",
			"comment": [
				"One of a family of Pascal-like arrays. A007318 is equivalent to the (1,0,1)-array. A008288 is equivalent to the (1,1,1)-array. Rows include A016813, A081585, A081586. Coefficients of the row polynomials in the Newton basis are given by A013611.",
				"As a number triangle, this is the Riordan array (1/(1-x), x*(1+3*x)/(1-x)). It has row sums A015518(n+1) and diagonal sums A103143. - _Paul Barry_, Jan 24 2005"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A081578/b081578.txt\"\u003eRows n = 0..100, flattened\u003c/a\u003e",
				"Paul Barry, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL9/Barry/barry91.html\"\u003eOn Integer-Sequence-Based Constructions of Generalized Pascal Triangles\u003c/a\u003e, Journal of Integer Sequences, Vol. 9 (2006), Article 06.2.4.",
				"P. Barry, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL11/Barry/barry594.html\"\u003eA note on Krawtchouk Polynomials and Riordan Arrays\u003c/a\u003e, JIS 11 (2008) 08.2.2"
			],
			"formula": [
				"Square array T(n, k) defined by T(n, 0) = T(0, k) = 1, T(n, k) = T(n, k-1) + 3*T(n-1, k-1) + T(n-1, k).",
				"Rows are the expansions of (1+3*x)^k/(1-x)^(k+1).",
				"T(n,k) = Sum_{j=0..n} binomial(k,j-k)*binomial(n+k-j,k)*3^(j-k). - _Paul Barry_, Oct 23 2006",
				"E.g.f. for the n-th subdiagonal of the triangle, n = 0,1,2,..., equals exp(x)*P(n,x), where P(n,x) is the polynomial Sum_{k = 0..n} binomial(n,k)*(4*x)^k/k!. For example, the e.g.f. for the second subdiagonal is exp(x)*(1 + 8*x + 16*x^2/2) = 1 + 9*x + 33*x^2/2! + 73*x^3/3! + 129*x^4/4! + 201*x^5/5! + .... - _Peter Bala_, Mar 05 2017",
				"From _G. C. Greubel_, May 26 2021: (Start)",
				"T(n, k, m) = Hypergeometric2F1([-k, k-n], [1], m+1), for m = 3.",
				"T(n, k, m) = Sum_{j=0..n-k} binomial(k,j)*binomial(n-j,k)*m^j, for m = 3.",
				"Sum_{k=0..n} T(n, k, 3) = A015518(n+1). (End)"
			],
			"example": [
				"Square array begins as:",
				"  1,  1,   1,   1,    1, ... A000012;",
				"  1,  5,   9,  13,   17, ... A016813;",
				"  1,  9,  33,  73,  129, ... A081585;",
				"  1, 13,  73, 245,  593, ... A081586;",
				"  1, 17, 129, 593, 1921, ...",
				"As a triangle this begins:",
				"  1;",
				"  1,  1;",
				"  1,  5,   1;",
				"  1,  9,   9,    1;",
				"  1, 13,  33,   13,     1;",
				"  1, 17,  73,   73,    17,     1;",
				"  1, 21, 129,  245,   129,    21,     1;",
				"  1, 25, 201,  593,   593,   201,    25,    1;",
				"  1, 29, 289, 1181,  1921,  1181,   289,   29,   1;",
				"  1, 33, 393, 2073,  4881,  4881,  2073,  393,  33,  1;",
				"  1, 37, 513, 3333, 10497, 15525, 10497, 3333, 513, 37, 1; - _Philippe Deléham_, Mar 15 2014"
			],
			"mathematica": [
				"Table[Hypergeometric2F1[-k, k-n, 1, 4], {n,0,10}, {k,0,n}]//Flatten (* _Jean-François Alcover_, May 24 2013 *)"
			],
			"program": [
				"(Haskell)",
				"a081578 n k = a081578_tabl !! n !! k",
				"a081578_row n = a081578_tabl !! n",
				"a081578_tabl = map fst $ iterate",
				"   (\\(us, vs) -\u003e (vs, zipWith (+) (map (* 3) ([0] ++ us ++ [0])) $",
				"                      zipWith (+) ([0] ++ vs) (vs ++ [0]))) ([1], [1, 1])",
				"-- _Reinhard Zumkeller_, Mar 16 2014",
				"(MAGMA)",
				"A081578:= func\u003c n,k,q | (\u0026+[Binomial(k, j)*Binomial(n-j, k)*q^j: j in [0..n-k]]) \u003e;",
				"[A081578(n,k,3): k in [0..n], n in [0..12]]; // _G. C. Greubel_, May 26 2021",
				"(Sage) flatten([[hypergeometric([-k, k-n], [1], 4).simplify() for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, May 26 2021"
			],
			"xref": [
				"Cf. Pascal (1,m,1) array: A123562 (m = -3), A098593 (m = -2), A000012 (m = -1), A007318 (m = 0), A008288 (m = 1), A081577 (m = 2), A081579 (m = 4), A081580 (m = 5), A081581 (m = 6), A081582 (m = 7), A143683 (m = 8)."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "0,5",
			"author": "_Paul Barry_, Mar 23 2003",
			"references": 14,
			"revision": 35,
			"time": "2021-05-26T20:56:49-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}