{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A057680",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 57680,
			"data": "1,16470,44899,79873884,711939213,36541622473,45677255610,62644957128,656430109694",
			"name": "Self-locating strings within Pi: numbers n such that the string n is at position n in the decimal digits of Pi, where 1 is the first digit.",
			"comment": [
				"The average number of matches of length \"n\" digits is exactly 0.9.  That is, we expect 0.9 matches with 1 digit, 0.9 matches with 2 digits, etc.  Increasing the number of digits by a factor of 10 means that we expect to find 0.9 new matches.  Increasing the search from 10^11 to 10^12 (which includes 10 times as much work) would thus only expect to find 0.9 new matches. - _Alan Eliasen_, May 01 2013 (corrected by _Michael Beight_, Mar 21 2020)",
				"Consequently, with the second Borel-Cantelli lemma, the expected number of terms in this sequence is infinite with probability 1. (Of course the sequence is not random, but almost all of the sequences corresponding to randomly-chosen real numbers in place of Pi have infinitely many terms.) - _Charles R Greathouse IV_, Apr 29 2013",
				"a(1) \u0026 a(5) are the first occurrences in Pi of their respective strings; a(2) \u0026 a(4) are the second occurrences; a(3) is the fourth occurrence. - _Hans Havermann_, Jul 27 2014",
				"A near-miss '043611' occurs at position 43611. - _S. Alwin Mao_, Feb 18 2020",
				"a(10) \u003e 5 * 10^13. - _Kang Seonghoon_, Nov 02 2020"
			],
			"reference": [
				"Clifford A. Pickover, A Passion for Mathematics, Wiley, 2005; see p. 60."
			],
			"link": [
				"David G. Andersen, \u003ca href=\"http://www.angio.net/pi/piquery\"\u003eThe Pi-Search Page\u003c/a\u003e.",
				"Tom Crawford and Brady Haran, \u003ca href=\"https://www.youtube.com/watch?v=W20aT14t8Pw\"\u003eStrings and Loops within Pi\u003c/a\u003e, Numberphile video (2020).",
				"Google, \u003ca href=\"https://storage.googleapis.com/pi50t/index.html\"\u003e50 trillion digits of pi\u003c/a\u003e (2020)."
			],
			"example": [
				"1 is a term because 1 is the first digit after the decimal point."
			],
			"mathematica": [
				"StringsinPiAfterPoint[m_] := Module[{cc = 10^m + m, sol, aa}, sol = Partition[RealDigits[Pi,10,cc] // First // Rest, m, 1]; Do[aa = FromDigits[sol[[i]]]; If[aa==i, Print[{i, aa}]], {i,Length[sol]}];] (* For example, StringsinPiAfterPoint[5] returns all 5-digit members of the sequence. - _Colin Rose_, Mar 15 2006 *)",
				"Do[If[RealDigits[Pi,10,a=i+IntegerLength@i-1,-1][[1,i;;a]]==IntegerDigits@i,Print@i],{i,50000}] (* _Giorgos Kalogeropoulos_, Feb 21 2020 *)"
			],
			"xref": [
				"Cf. A000796, A057679, A109513, A064810."
			],
			"keyword": "nonn,base,more",
			"offset": "1,2",
			"author": "Mike Keith (domnei(AT)aol.com), Oct 19 2000",
			"ext": [
				"More terms from _Colin Rose_, Mar 15 2006",
				"a(5) from _Nathaniel Johnston_, Nov 12 2010",
				"a(6)-a(8) from _Alan Eliasen_, May 01 2013",
				"a(9) from _Alan Eliasen_, Jun 06 2013",
				"Name clarified by _Kang Seonghoon_, Nov 02 2020"
			],
			"references": 19,
			"revision": 82,
			"time": "2020-12-28T02:51:18-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}