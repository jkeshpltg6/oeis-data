{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A116422",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 116422,
			"data": "1,0,1,0,1,1,0,0,1,1,0,0,2,1,0,1,0,2,0,1,0,1,0,3,0,1,0,2,0,3,0,1,0,3,0,4,0,1,1,0,4,0,0,4,0,1,1,0,5,0,0,5,0,2,1,0,7,0,0,5,0,3,1,0,8,0,0,6,0,5,1,0,10,0,1,0,6,0,6,0,1,0,12,0,1,0,7,0,9,0,1,0,14,0,2,0,7,0,11,0,1,0,16",
			"name": "Triangle read by rows: T(n,k) is the number of self-conjugate partitions of n having Durfee square of size k (n\u003e=1; 1\u003c=k\u003c=floor(sqrt(n))).",
			"comment": [
				"Row n contains floor(sqrt(n)) terms (0's are possible even at the end of the rows). Row sums yield A000700. Sum_{k=1..floor(sqrt(n))} k*T(n,k) = A079499(n).",
				"Also, number of partitions of n into k distinct odd parts. Example: T(13,3)=2 because we have [9,3,1] and [7,5,1]. - _Emeric Deutsch_, Feb 24 2006"
			],
			"reference": [
				"G. E. Andrews, The Theory of Partitions, Addison-Wesley, 1976 (pp. 27-28).",
				"G. E. Andrews and K. Eriksson, Integer Partitions, Cambridge Univ. Press, 2004 (pp. 75-78)."
			],
			"link": [
				"Álvar Ibeas, \u003ca href=\"/A116422/b116422.txt\"\u003eFirst 400 rows, flattened\u003c/a\u003e"
			],
			"formula": [
				"G.f.: Sum_{k=1..infinity} (t^k*x^(k^2))/Product_{i=1..k} 1-x^(2*i).",
				"G.f.: -1 + Product_{j=1..infinity} 1+t*x^(2*j-1). - _Emeric Deutsch_, Feb 24 2006",
				"T(n, k) = T(n-2*k, k) + T(n-2*k+1, k-1). If n+k is even, T(n, k) = A008284((n-k^2)/2 + k, k) = A072233((n-k^2)/2, k); 0 otherwise. - _Álvar Ibeas_, Jul 27 2020"
			],
			"example": [
				"T(13,3)=2 because we have [5,3,3,1,1] and [4,4,3,2] (there is one more self-conjugate partition of 13, namely [7,1,1,1,1,1,1], having Durfee square of size 1).",
				"Triangle starts:",
				"1;",
				"0;",
				"1;",
				"0,1;",
				"1,0;",
				"0,1;",
				"1,0;",
				"0,2;",
				"1,0,1;",
				"0,2,0;"
			],
			"maple": [
				"g:=sum(t^k*q^(k^2)/product(1-q^(2*i),i=1..k),k=1..15): gser:=simplify(series(g,q=0,40)): for n from 1 to 33 do P[n]:=coeff(gser,q^n) od: for n from 1 to 33 do row[n]:=seq(coeff(P[n],t^j),j=1..floor(sqrt(n))) od; # yields sequence in triangular form"
			],
			"mathematica": [
				"rows = 31; jmax = Floor[Sqrt[rows]]; T[n_, k_] := SeriesCoefficient[Sum[ t^j*x^(j^2)/Product[1-x^(2i), {i, 1, j}], {j, 1, jmax}], {x, 0, n}, {t, 0, k}]; Table[T[n, k], {n, 1, rows}, {k, 1, Floor[Sqrt[n]]}] // Flatten (* _Jean-François Alcover_, Jul 16 2017 *)"
			],
			"xref": [
				"Cf. A000700, A079499."
			],
			"keyword": "nonn,tabf",
			"offset": "1,13",
			"author": "_Emeric Deutsch_, Feb 14 2006",
			"references": 2,
			"revision": 12,
			"time": "2020-08-01T11:39:06-04:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}