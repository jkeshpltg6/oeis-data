{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A341445",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 341445,
			"data": "1,0,2,2,0,3,2,6,0,6,8,8,16,0,10,16,32,24,40,0,20,52,84,108,60,90,0,35,134,262,294,310,150,210,0,70,432,816,1008,880,816,336,448,0,126,1248,2544,3192,3208,2460,2100,784,1008,0,252",
			"name": "Triangle read by rows: T(n,k) is the number of Dyck paths of semilength n having degree of symmetry k (n \u003e= 1, 1 \u003c= k \u003c= n).",
			"comment": [
				"The degree of symmetry of a Dyck path is defined as the number of steps in the first half that are mirror images of steps in the second half, with respect to the reflection along the vertical line through the midpoint of the path."
			],
			"link": [
				"Sergi Elizalde, \u003ca href=\"https://arxiv.org/abs/2002.12874\"\u003eThe degree of symmetry of lattice paths\u003c/a\u003e, arXiv:2002.12874 [math.CO], 2020.",
				"Sergi Elizalde, \u003ca href=\"https://www.mat.univie.ac.at/~slc/wpapers/FPSAC2020/26.html\"\u003eMeasuring symmetry in lattice paths and partitions\u003c/a\u003e, Sem. Lothar. Combin. 84B.26, 12 pp (2020)."
			],
			"example": [
				"For n=4 there are 6 Dyck paths with degree of symmetry equal to 2: uuuddudd, uuduuddd, uududdud, uuddudud, uduududd, ududuudd.",
				"Triangle begins:",
				"     1;",
				"     0,    2;",
				"     2,    0,    3;",
				"     2,    6,    0,    6;",
				"     8,    8,   16,    0,   10;",
				"    16,   32,   24,   40,    0,   20;",
				"    52,   84,  108,   60,   90,    0,  35;",
				"   134,  262,  294,  310,  150,  210,   0,   70;",
				"   432,  816, 1008,  880,  816,  336, 448,    0, 126;",
				"  1248, 2544, 3192, 3208, 2460, 2100, 784, 1008,   0, 252;",
				"  ..."
			],
			"maple": [
				"b:= proc(x, y, v) option remember; expand(",
				"      `if`(min(y, v, x-max(y, v))\u003c0, 0, `if`(x=0, 1, (l-\u003e add(add(",
				"      `if`(y=v+(j-i)/2, z, 1)*b(x-1, y+i, v+j), i=l), j=l))([-1, 1]))))",
				"    end:",
				"g:= proc(n) option remember; add(b(n, j$2), j=0..n) end:",
				"T:= (n, k)-\u003e coeff(g(n), z, k):",
				"seq(seq(T(n, k), k=1..n), n=1..10);  # _Alois P. Heinz_, Feb 12 2021"
			],
			"mathematica": [
				"b[x_, y_, v_] := b[x, y, v] = Expand[If[Min[y, v, x - Max[y, v]] \u003c 0, 0, If[x == 0, 1, Function[l, Sum[Sum[If[y == v + (j - i)/2, z, 1]*b[x - 1, y + i, v + j], {i, l}], {j, l}]][{-1, 1}]]]];",
				"g[n_] := g[n] = Sum[b[n, j, j], {j, 0, n}];",
				"T[n_, k_] := Coefficient[g[n], z, k];",
				"Table[Table[T[n, k], {k, 1, n}], {n, 1, 10}] // Flatten (* _Jean-François Alcover_, Feb 13 2021, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Equivalent to A298645 with rows reversed.",
				"Row sums give A000108.",
				"Main diagonal gives A001405.",
				"Column k=1 gives A298647 (for n\u003e2).",
				"Second subdiagonal gives 2*A191522."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Sergi Elizalde_, Feb 12 2021",
			"references": 1,
			"revision": 23,
			"time": "2021-02-13T05:01:18-05:00",
			"created": "2021-02-12T18:45:05-05:00"
		}
	]
}