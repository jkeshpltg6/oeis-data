{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A193862",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 193862,
			"data": "1,2,2,3,6,4,4,12,16,8,5,20,40,40,16,6,30,80,120,96,32,7,42,140,280,336,224,64,8,56,224,560,896,896,512,128,9,72,336,1008,2016,2688,2304,1152,256,10,90,480,1680,4032,6720,7680,5760,2560,512,11,110,660",
			"name": "Mirror of the triangle A115068.",
			"comment": [
				"A193862 is obtained by reversing the rows of the triangle A115068.",
				"Riordan array (1/(1-x)^2, 2*x/(1-x)). - _Philippe Deléham_, Jan 29 2014",
				"Let P(n, x) := Sum_{k=1..n} T(n, k)*x^k. Then P(n, P(m, x)) = P(n*m, x) for all n and m in Z. - _Michael Somos_, Apr 10 2020"
			],
			"formula": [
				"Write w(n,k) for the triangle at A115068.  The triangle at A193862 is then given by w(n,n-k).",
				"T(n, k) = binomial(n, k)/2 * 2^k. - _Michael Somos_, Apr 10 2020"
			],
			"example": [
				"First six rows:",
				"1",
				"2...2",
				"3...6....4",
				"4...12...16...8",
				"5...20...40...40....16",
				"6...30...80...120...96...32",
				"Production matrix begins",
				"2......2",
				"-1/2...1...2",
				"1/4....0...1...2",
				"-1/8...0...0...1...2",
				"1/16...0...0...0...1...2",
				"-1/32..0...0...0...0...1...2",
				"1/64...0...0...0...0...0...1...2",
				"-1/128.0...0...0...0...0...0...1...2",
				"1/256..0...0...0...0...0...0...0...1...2",
				"- _Philippe Deléham_, Jan 29 2014"
			],
			"mathematica": [
				"z = 11;",
				"p[0, x_] := 1; p[n_, x_] := x*p[n - 1, x] + 1;",
				"q[n_, x_] := (2 x + 1)^n;",
				"p1[n_, k_] := Coefficient[p[n, x], x^k];",
				"p1[n_, 0] := p[n, x] /. x -\u003e 0;",
				"d[n_, x_] := Sum[p1[n, k]*q[n - 1 - k, x], {k, 0, n - 1}]",
				"h[n_] := CoefficientList[d[n, x], {x}]",
				"TableForm[Table[Reverse[h[n]], {n, 0, z}]]",
				"Flatten[Table[Reverse[h[n]], {n, -1, z}]]  (* A115068 *)",
				"TableForm[Table[h[n], {n, 0, z}]]",
				"Flatten[Table[h[n], {n, -1, z}]]   (* A193862 *)",
				"T[ n_, k_] := Binomial[n, k]/2 2^k; (* _Michael Somos_, Apr 10 2020 *)"
			],
			"program": [
				"(PARI) {T(n, k) = binomial(n, k)/2 * 2^k}; /* _Michael Somos_, Apr 10 2020 */"
			],
			"xref": [
				"Cf. A115068."
			],
			"keyword": "nonn,tabl",
			"offset": "0,2",
			"author": "_Clark Kimberling_, Aug 07 2011",
			"references": 7,
			"revision": 12,
			"time": "2020-04-10T12:38:07-04:00",
			"created": "2011-08-07T16:11:54-04:00"
		}
	]
}