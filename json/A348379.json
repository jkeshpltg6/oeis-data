{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A348379",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 348379,
			"data": "1,1,1,1,1,2,1,2,1,2,1,4,1,2,2,3,1,4,1,4,2,2,1,6,1,2,2,4,1,5,1,5,2,2,2,8,1,2,2,6,1,5,1,4,4,2,1,10,1,4,2,4,1,6,2,6,2,2,1,11,1,2,4,6,2,5,1,4,2,5,1,15,1,2,4,4,2,5,1,10,3,2,1,11,2",
			"name": "Number of factorizations of n with an alternating permutation.",
			"comment": [
				"First differs from A335434 at a(216) = 27, A335434(216) = 28. Also differs from A335434 at a(270) = 19, A335434(270) = 20.",
				"A factorization of n is a weakly increasing sequence of positive integers \u003e 1 with product n.",
				"All of the counted factorizations are separable (A335434).",
				"A sequence is alternating if it is alternately strictly increasing and strictly decreasing, starting with either. For example, the partition (3,2,2,2,1) has no alternating permutations, even though it does have the anti-run permutations (2,3,2,1,2) and (2,1,2,3,2). Alternating permutations of multisets are a generalization of alternating or up-down permutations of {1..n}."
			],
			"link": [
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Alternating_permutation\"\u003eAlternating permutation\u003c/a\u003e"
			],
			"formula": [
				"a(2^n) = A345170(n)."
			],
			"example": [
				"The a(270) = 19 factorizations:",
				"  (2*3*3*15)  (2*3*45)  (2*135)  (270)",
				"  (2*3*5*9)   (2*5*27)  (3*90)",
				"  (3*3*5*6)   (2*9*15)  (5*54)",
				"              (3*3*30)  (6*45)",
				"              (3*5*18)  (9*30)",
				"              (3*6*15)  (10*27)",
				"              (3*9*10)  (15*18)",
				"              (5*6*9)"
			],
			"mathematica": [
				"facs[n_]:=If[n\u003c=1,{{}},Join@@Table[Map[Prepend[#,d]\u0026,Select[facs[n/d],Min@@#\u003e=d\u0026]],{d,Rest[Divisors[n]]}]];",
				"wigQ[y_]:=Or[Length[y]==0,Length[Split[y]]==Length[y]\u0026\u0026Length[Split[Sign[Differences[y]]]]==Length[y]-1];",
				"Table[Length[Select[facs[n],Select[Permutations[#],wigQ]!={}\u0026]],{n,100}]"
			],
			"xref": [
				"Partitions not of this type are counted by A345165, ranked by A345171.",
				"Partitions of this type are counted by A345170, ranked by A345172.",
				"Twins and partitions of this type are counted by A344740, ranked by A344742.",
				"The case with twins is A347050.",
				"The complement is counted by A348380, without twins A347706.",
				"The ordered version is A348610.",
				"A001055 counts factorizations, strict A045778, ordered A074206.",
				"A001250 counts alternating permutations.",
				"A025047 counts alternating or wiggly compositions, ranked by A345167.",
				"A339846 counts even-length factorizations.",
				"A339890 counts odd-length factorizations.",
				"Cf. A038548, A056986, A325534, A335452, A347437, A347438, A347439, A347442, A347456, A347463, A348381, A348383, A348609."
			],
			"keyword": "nonn",
			"offset": "1,6",
			"author": "_Gus Wiseman_, Oct 28 2021",
			"references": 25,
			"revision": 12,
			"time": "2021-11-03T10:48:45-04:00",
			"created": "2021-10-31T22:58:19-04:00"
		}
	]
}