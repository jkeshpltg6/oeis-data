{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A305493",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 305493,
			"data": "0,1,3,7,15,31,63,127,255,511,2,5,11,23,47,95,191,383,767,1023,6,13,27,55,111,223,447,895,1791,2047,14,29,59,119,239,479,959,1919,3839,4095,30,61,123,247,495,991,1983,3967,7935,8191,62,125,251,503,1007,2015",
			"name": "A binary encoding of the decimal representation of a number: for any number n \u003e= 0, consider its decimal representation and replace each 9 with \"111111111\" and each other digit d with a \"0\" followed by d \"1\"s and interpret the result as a binary string.",
			"comment": [
				"This sequence is a permutation of the nonnegative numbers.",
				"The inverse sequence, say b, satisfies b(n) = A001202(n+1) for n = 0..1022, but b(1023) = 19 whereas A001202(1024) = 10.",
				"The first known fixed points are: 0, 1, 65010; they all belong to A037308.",
				"This encoding can be applied to any base b \u003e 1 (when b = 2, we obtain the identity function) as well as to the factorial base and to the primorial base."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A305493/b305493.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A305493/a305493.png\"\u003eColored logarithmic scatterplot of the first 1000000 terms\u003c/a\u003e (where the color is function of the digital sum minus the number of nonleading digits different from 9)",
				"\u003ca href=\"/index/De#decimal_expansion\"\u003eIndex entries for sequences related to decimal expansion of n\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"A000120(a(n)) = A007953(n).",
				"a(A051885(k)) = 2^k - 1 for any k \u003e= 0.",
				"a(A002275(k)) = A002450(k) for any k \u003e= 0.",
				"a(10 * n) = 2 * a(n)."
			],
			"example": [
				"For n = 1972:",
				"- the digit 1 is replaced by \"01\",",
				"- the digit 9 is replaced by \"111111111\",",
				"- the digit 7 is replaced by \"01111111\",",
				"- the digit 2 is replaced by \"011\",",
				"- hence we obtain the binary string \"0111111111101111111011\",",
				"- and a(1972) = 2096123."
			],
			"mathematica": [
				"tb=Table[n-\u003ePadRight[{0},n+1,1],{n,9}]/.PadRight[{0},10,1]-\u003e PadRight[ {},9,1]; Table[FromDigits[IntegerDigits[n]/.tb//Flatten,2],{n,0,60}] (* _Harvey P. Dale_, Jul 31 2018 *)"
			],
			"program": [
				"(PARI) a(n, base=10) = my (b=[], d=digits(n, base)); for (i=1, #d, if (d[i]!=base-1, b=concat(b, 0)); b=concat(b, vector(d[i], k, 1))); fromdigits(b, 2)",
				"/* inverse */ b(n, base=10) = my (v=0, p=1); while (n, my (d = min(valuation(n+1, 2), base-1)); v += p * d; n \\= 2^min(base-1, 1+d); p *= base); v"
			],
			"xref": [
				"Cf. A000120, A001202, A002275, A002450, A007953, A037308, A051885."
			],
			"keyword": "nonn,base,easy",
			"offset": "0,3",
			"author": "_Rémy Sigrist_, Jun 02 2018",
			"references": 1,
			"revision": 23,
			"time": "2020-02-27T23:24:39-05:00",
			"created": "2018-06-03T07:59:08-04:00"
		}
	]
}