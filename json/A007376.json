{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007376",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7376,
			"id": "M0469",
			"data": "0,1,2,3,4,5,6,7,8,9,1,0,1,1,1,2,1,3,1,4,1,5,1,6,1,7,1,8,1,9,2,0,2,1,2,2,2,3,2,4,2,5,2,6,2,7,2,8,2,9,3,0,3,1,3,2,3,3,3,4,3,5,3,6,3,7,3,8,3,9,4,0,4,1,4,2,4,3,4,4,4,5,4,6,4,7,4,8,4,9,5,0,5,1,5,2,5,3,5,4,5,5,5,6,5,7",
			"name": "The almost-natural numbers: write n in base 10 and juxtapose digits.",
			"comment": [
				"Also called the Barbier infinite word.",
				"This is an example of a non-morphic sequence.",
				"a(n) = A162711(n,1); A136414(n) = 10*a(n) + a(n+1). - _Reinhard Zumkeller_, Jul 11 2009",
				"a(A031287(n)) = 0, a(A031288(n)) = 1, a(A031289(n)) = 2, a(A031290(n)) = 3, a(A031291(n)) = 4, a(A031292(n)) = 5, a(A031293(n)) = 6, a(A031294(n)) = 7, a(A031295(n)) = 8, a(A031296(n)) = 9. - _Reinhard Zumkeller_, Jul 28 2011",
				"May be regarded as an irregular table in which the n-th row lists the digits of n. - _Jason Kimberley_, Dec 07 2012",
				"The digits of the integer n start at index A117804(n). The digit a(n) at index n belongs to the number A100470(n). - _M. F. Hasler_, Oct 23 2019",
				"See also the Copeland-Erdős constant A033308, equivalent using primes instead of all numbers. - _M. F. Hasler_, Oct 24 2019"
			],
			"reference": [
				"J.-P. Allouche and J. Shallit, Automatic Sequences, Cambridge Univ. Press, 2003, pp. 114, 336.",
				"R. Honsberger, Mathematical Chestnuts from Around the World, MAA, 2001; see p. 163.",
				"M. Kraitchik, Mathematical Recreations. Dover, NY, 2nd ed., 1953, p. 49.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Robert G. Wilson v, \u003ca href=\"/A007376/b007376.txt\"\u003eTable of n, a(n) for n = 0..100000\u003c/a\u003e (a(0) = 0 added by _M. F. Hasler_, Oct 23 2019).",
				"Putnam Competition No. 48, \u003ca href=\"http://www.jstor.org/stable/2690046\"\u003eProblem A2\u003c/a\u003e, Math. Mag., 61 (1988), 131-134.",
				"R. G. Wilson v, \u003ca href=\"/A007376/a007376.pdf\"\u003eLetter to N. J. A. Sloane, Oct. 1993\u003c/a\u003e"
			],
			"maple": [
				"c:=proc(x,y) local s: s:=proc(m) nops(convert(m,base,10)) end: if y=0 then 10*x else x*10^s(y)+y: fi end: b:=proc(n) local nn: nn:=convert(n,base,10):[seq(nn[nops(nn)+1-i],i=1..nops(nn))] end: A:=0: for n from 1 to 75 do A:=c(A,n) od: b(A); # c concatenates 2 numbers while b converts a number to the sequence of its digits - _Emeric Deutsch_, Jul 27 2006",
				"#alternative",
				"A007376 := proc(n) option remember ; local aprev, dOld,N ; if n \u003c=9 then RETURN([n,n,1]) ; else aprev := A007376(n-1) ; dOld := op(3,aprev) ; N := op(2,aprev) ; if dOld \u003c A055642(N) then RETURN([op(-dOld-1,convert(N,base,10)),N,dOld+1]) ; else RETURN([op(-1,convert(N+1,base,10)),N+1,1]) ; fi ; fi ; end: # _R. J. Mathar_, Jan 21 2008"
			],
			"mathematica": [
				"Flatten[ IntegerDigits /@ Range@ 57] (* Or *)",
				"almostNatural[n_, b_] := Block[{m = 0, d = n, i = 1, l, p}, While[m \u003c= d, l = m; m = (b - 1) i*b^(i - 1) + l; i++]; i--; p = Mod[d - l, i]; q = Floor[(d - l)/i] + b^(i - 1); If[p != 0, IntegerDigits[q, b][[p]], Mod[q - 1, b]]]; Array[ almostNatural[#, 10] \u0026, 105] (* updated Jun 29 2014 *)",
				"With[{nn=120},RealDigits[N[ChampernowneNumber[],nn],10,nn]][[1]] (* _Harvey P. Dale_, Mar 13 2018 *)"
			],
			"program": [
				"(Haskell)",
				"a007376 n = a007376_list !! (n-1)",
				"a007376_list = concatMap (map (read . return) . show) [0..] :: [Int]",
				"-- _Reinhard Zumkeller_, Nov 11 2013, Dec 17 2011, Mar 28 2011",
				"(PARI) for(n=0,90,v=digits(n);for(i=1,#v,print1(v[i]\", \"))) \\\\ _Charles R Greathouse IV_, Nov 20 2012",
				"(PARI) apply( A007376(n)={for(k=1,n, k*10^k\u003en\u0026\u0026 return(digits(n\\k)[n%k+1]); n+=10^k)}, [0..200]) \\\\ _M. F. Hasler_, Nov 03 2019",
				"(MAGMA) \u0026cat[Reverse(IntegerToSequence(n)):n in[0..31]]; // _Jason Kimberley_, Dec 07 2012",
				"(Python) A007376_list = [int(d) for n in range(10**2) for d in str(n)] # _Chai Wah Wu_, Feb 04 2015"
			],
			"xref": [
				"Considered as a sequence of digits, this is the same as the decimal expansion of the Champernowne constant, A033307. See that entry for a formula for a(n), further references, etc.",
				"Cf. A054632 (partial sums), A023103.",
				"For \"decimations\" see A127050 A127353 A127414 A127508 A127584 A127734 A127794 A127950 A128178 A128211 A128359 A128423 A128475 A128881.",
				"Cf. A193428, A256100, A001477 (the nonnegative integers), A117804, A100470.",
				"Tables in which the n-th row lists the base b digits of n: A030190 and A030302 (b=2), A003137 and A054635 (b=3), A030373 (b=4), A031219 (b=5), A030548 (b=6), A030998 (b=7), A031035 and A054634 (b=8), A031076 (b=9), this sequence and A033307 (b=10). - _Jason Kimberley_, Dec 06 2012",
				"Row lengths in A055642.",
				"For primes here see A071620. See A007908 for a very similar sequence."
			],
			"keyword": "base,easy,nice,nonn,tabf",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_, _Robert G. Wilson v_",
			"ext": [
				"Extended to a(0) = 0 by _M. F. Hasler_, Oct 23 2019"
			],
			"references": 131,
			"revision": 98,
			"time": "2019-11-03T15:03:24-05:00",
			"created": "1994-05-09T03:00:00-04:00"
		}
	]
}