{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A046800",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 46800,
			"data": "0,0,1,1,2,1,2,1,3,2,3,2,4,1,3,3,4,1,4,1,5,3,4,2,6,3,3,3,6,3,6,1,5,4,3,4,8,2,3,4,7,2,6,3,7,6,4,3,9,2,7,5,7,3,6,6,8,4,6,2,11,1,3,6,7,3,8,2,7,4,9,3,12,3,5,7,7,4,7,3,9,6,5,2,12,3,5,6,10,1,11,5,9,3,6,5,12,2,5,8,12,2",
			"name": "Number of distinct prime factors of 2^n-1.",
			"link": [
				"Amiram Eldar, \u003ca href=\"/A046800/b046800.txt\"\u003eTable of n, a(n) for n = 0..1206\u003c/a\u003e (terms 1..500 from T. D. Noe)",
				"J. Brillhart et al., \u003ca href=\"http://dx.doi.org/10.1090/conm/022\"\u003eFactorizations of b^n +- 1\u003c/a\u003e. Contemporary Mathematics, Vol. 22, Amer. Math. Soc., Providence, RI, 3rd edition, 2002.",
				"Abílio Lemos and Ady Cambraia Junior, \u003ca href=\"https://arxiv.org/abs/1606.08690\"\u003eOn the number of prime factors of Mersenne numbers\u003c/a\u003e (2016)"
			],
			"formula": [
				"a(n) = Sum_{d|n} A086251(d), Mobius transform of A086251.",
				"a(n) \u003c 0.7 * n; the constant 0.7 cannot be improved below log 2 using only the size of 2^n-1. - _Charles R Greathouse IV_, Apr 12 2012",
				"a(n) = A001221(2^n-1). - _R. J. Mathar_, Nov 10 2017"
			],
			"example": [
				"a(6) = 2 because 63 = 3*3*7 has 2 distinct prime factors."
			],
			"maple": [
				"A046800 := proc(n)",
				"    if n \u003c= 1 then",
				"        0;",
				"    else",
				"        numtheory[factorset](2^n-1) ;",
				"        nops(%) ;",
				"    end if;",
				"end proc:",
				"seq(A046800(n),n=0..100) ; # _R. J. Mathar_, Nov 10 2017"
			],
			"mathematica": [
				"Table[Length[ FactorInteger [ 2^n -1 ] ], {n, 0, 100}]",
				"Join[{0},PrimeNu/@(2^Range[110]-1)] (* _Harvey P. Dale_, Mar 09 2015 *)"
			],
			"program": [
				"(PARI) a(n)=omega(2^n-1) \\\\ _Charles R Greathouse IV_, Nov 17 2014"
			],
			"xref": [
				"Length of row n of A060443.",
				"Cf. A000225, A046051 (number of prime factors, with repetition, of 2^n-1), A086251."
			],
			"keyword": "nonn",
			"offset": "0,5",
			"author": "_Labos Elemer_",
			"ext": [
				"Edited by _T. D. Noe_, Jul 14 2003"
			],
			"references": 28,
			"revision": 36,
			"time": "2021-06-02T17:28:38-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}