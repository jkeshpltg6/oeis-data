{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A093145",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 93145,
			"data": "0,1,10,80,600,4400,32000,232000,1680000,12160000,88000000,636800000,4608000000,33344000000,241280000000,1745920000000,12633600000000,91417600000000,661504000000000,4786688000000000,34636800000000000",
			"name": "Third binomial transform of Fibonacci(3n)/Fibonacci(3).",
			"comment": [
				"Fifth binomial transform of 1,5,5,25,25,125. - Al Hakanson (hawkuu(AT)gmail.com), Jul 13 2009"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A093145/b093145.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"S. Falcon, \u003ca href=\"http://dx.doi.org/10.9734/BJMCS/2014/11783\"\u003eIterated Binomial Transforms of the k-Fibonacci Sequence\u003c/a\u003e, British Journal of Mathematics \u0026 Computer Science, 4 (22): 2014.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (10,-20)."
			],
			"formula": [
				"G.f.: x/(1 - 10*x + 20*x^2).",
				"a(n) = ((5+sqrt(5))^n - (5-sqrt(5))^n)/(2*sqrt(5)).",
				"a(n) = Sum_{k=0..n} binomial(n, 2*k+ 1)*5^(n-k-1).",
				"a(n) = 10*a(n-1) - 20*a(n-2), n \u003e 1; a(0)=0, a(1)=1. - _Zerinvary Lajos_, Apr 26 2009",
				"G.f.: A(x) = x*G(0)/(1-5*x) where G(k) = 1 + 5*x/(1-5*x - x*(1-5*x)/(x + (1-5*x)/G(k+1) )); (recursively defined continued fraction). - _Sergei N. Gladkovskii_, Dec 30 2012"
			],
			"mathematica": [
				"Join[{a=0,b=1},Table[c=10*b-20*a;a=b;b=c,{n,60}]] (* _Vladimir Joseph Stephan Orlovsky_, Jan 27 2011 *)",
				"f[n_] := Block[{s = Sqrt@ 5}, Simplify[((1 + s)(5 + s)^n + (1 - s)(5 - s)^n)/2]]; Array[f, 21, 0] (* Or *)",
				"a[n_] := 10 a[n - 1] - 20 a[n - 2]; a[0] = 0; a[1] = 1; Array[a, 22, 0] (* Or *)",
				"CoefficientList[Series[x/(1 - 10 x + 20 x^2), {x, 0, 21}], x] (* _Robert G. Wilson v_, Mar 07 2011 *)",
				"LinearRecurrence[{10,-20},{0,1},30] (* _Harvey P. Dale_, Jan 23 2019 *)"
			],
			"program": [
				"(Sage) [lucas_number1(n,10,20) for n in range(0, 21)] # _Zerinvary Lajos_, Apr 26 2009",
				"(MAGMA) [n le 2 select n - 1 else 10*Self(n-1)-20*Self(n-2): n in [1..25]]; // _Vincenzo Librandi_, Dec 30 2012"
			],
			"xref": [
				"Cf. A000045, A091870."
			],
			"keyword": "easy,nonn",
			"offset": "0,3",
			"author": "_Paul Barry_, Mar 26 2004",
			"references": 4,
			"revision": 39,
			"time": "2019-12-07T12:18:24-05:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}