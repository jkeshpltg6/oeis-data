{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A228825",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 228825,
			"data": "2,2,-1,-1,-1,-2,2,-2,1,1,1,2,-2,2,-2,2,-1,-1,-1,-2,2,-2,2,-2,2,-2,1,1,1,2,-2,2,-2,2,-2,2,-2,2,-1,-1,-1,-2,2,-2,2,-2,2,-2,2,-2,2,-2,1,1,1,2,-2,2,-2,2,-2,2,-2,2,-2,2,-2,2,-1,-1,-1,-2,2,-2",
			"name": "Delayed continued fraction of e.",
			"comment": [
				"An algorithm for the (usual) continued fraction of r \u003e 0 follows:  x(0) = r, a(n) = floor(x(n)), x(n+1) = 1/(x(n) - a(n)).",
				"The accelerated continued fraction uses \"round\" instead of \"floor\" (cf. A133593, A133570, A228667), where round(x) is the integer nearest x.",
				"The delayed continued fraction (DCF) uses \"second nearest integer\", so that all the terms are in {-2, -1, 1, 2}.  If s/t and u/v are consecutive convergents of a DCF, then |s*x-u*t| = 1.",
				"Regarding DCF(e), after the initial (2,2), the strings (-1,-1,-1) and (1,1,1) alternate with odd-length strings of the forms (-2,2,...,-2) and (2,-2,...,2).  The string lengths form the sequence 2,3,3,3,5,3,7,3,9,3,11,3,13,3,...",
				"Comparison of convergence rates is indicated by the following approximate values of x-e, where x is the 20th convergent: for delayed CF, x-e = 5.4x10^-7; for classical CF, x-e = 6.1x10^-16; for accelerated CF, x-e = -6.6x10^-27.  The convergents for accelerated CF are a proper subset of those for classical CF, which are a proper subset of those for delayed CF (which are sampled in Example)."
			],
			"example": [
				"Convergents: 2, 5/2, 3, 8/3, 11/4, 30/11, 49/18, 68/25, 19/7, 87/32, 106/39, 299/110, 492/181,..."
			],
			"mathematica": [
				"$MaxExtraPrecision = Infinity; x[0] = E; s[x_] := s[x] = If[FractionalPart[x] \u003c 1/2, Ceiling[x], Floor[x]]; a[n_] := a[n] = s[Abs[x[n]]]*Sign[x[n]]; x[n_] := 1/(x[n - 1] - a[n - 1]); t = Table[a[n], {n, 0, 100}]"
			],
			"xref": [
				"Cf. A133570, A228826"
			],
			"keyword": "cofr,sign,easy",
			"offset": "0,1",
			"author": "_Clark Kimberling_, Sep 04 2013",
			"references": 3,
			"revision": 11,
			"time": "2013-09-09T03:30:45-04:00",
			"created": "2013-09-09T03:30:45-04:00"
		}
	]
}