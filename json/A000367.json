{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000367",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 367,
			"id": "M4039 N1677",
			"data": "1,1,-1,1,-1,5,-691,7,-3617,43867,-174611,854513,-236364091,8553103,-23749461029,8615841276005,-7709321041217,2577687858367,-26315271553053477373,2929993913841559,-261082718496449122051",
			"name": "Numerators of Bernoulli numbers B_2n.",
			"reference": [
				"M. Abramowitz and I. A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, 1964 (and various reprintings), p. 810.",
				"Miklos Bona, editor, Handbook of Enumerative Combinatorics, CRC Press, 2015, page 932.",
				"L. Comtet, Advanced Combinatorics, Reidel, 1974, p. 49.",
				"H. T. Davis, Tables of the Mathematical Functions. Vols. 1 and 2, 2nd ed., 1963, Vol. 3 (with V. J. Fisher), 1962; Principia Press of Trinity Univ., San Antonio, TX, Vol. 2, p. 230.",
				"G. Everest, A. van der Poorten, I. Shparlinski and T. Ward, Recurrence Sequences, Amer. Math. Soc., 2003; see esp. p. 255.",
				"H. H. Goldstine, A History of Numerical Analysis, Springer-Verlag, 1977; Section 2.6.",
				"F. Lemmermeyer, Reciprocity Laws From Euler to Eisenstein, Springer-Verlag, 2000, p. 330.",
				"H. Rademacher, Topics in Analytic Number Theory, Springer, 1973, Chap. 1.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Simon Plouffe, \u003ca href=\"/A000367/b000367.txt\"\u003eTable of n, a(n) for n = 0..249\u003c/a\u003e [taken from link below]",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"J. L. Arregui, \u003ca href=\"http://arXiv.org/abs/math.NT/0109108\"\u003eTangent and Bernoulli numbers related to Motzkin and Catalan numbers by means of numerical triangles\u003c/a\u003e, arXiv:math/0109108 [math.NT], 2001.",
				"Richard P. Brent and David Harvey, \u003ca href=\"http://arxiv.org/abs/1108.0286\"\u003eFast computation of Bernoulli, Tangent and Secant numbers\u003c/a\u003e, arXiv preprint arXiv:1108.0286 [math.CO], 2011.",
				"J. Butcher, \u003ca href=\"http://www.math.auckland.ac.nz/~butcher/miniature/miniature18.pdf\"\u003eSome applications of Bernoulli numbers\u003c/a\u003e",
				"C. K. Caldwell, The Prime Glossary, \u003ca href=\"http://primes.utm.edu/glossary/page.php/BernoulliNumber.html\"\u003eBernoulli number\u003c/a\u003e",
				"F. N. Castro, O. E. González, and L. A. Medina, \u003ca href=\"http://emmy.uprrp.edu/lmedina/papers/eulerian/EulerianFinal.pdf\"\u003eThe p-adic valuation of Eulerian numbers: trees and Bernoulli numbers\u003c/a\u003e, 2014.",
				"R. Jovanovic, \u003ca href=\"http://milan.milanovic.org/math/english/bernoulli/bernoulli.html\"\u003eBernoulli numbers and the Pascal triangle\u003c/a\u003e",
				"M. Kaneko, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL3/KANEKO/AT-kaneko.html\"\u003eThe Akiyama-Tanigawa algorithm for Bernoulli numbers\u003c/a\u003e, J. Integer Sequences, 3 (2000), #00.2.9.",
				"B. C. Kellner, \u003ca href=\"http://arXiv.org/abs/math.NT/0409223\"\u003eOn irregular prime power divisors of the Bernoulli numbers\u003c/a\u003e, arXiv:math/0409223 [math.NT], 2004-2005.",
				"B. C. Kellner, \u003ca href=\"http://arxiv.org/abs/math/0411498\"\u003eThe structure of Bernoulli numbers\u003c/a\u003e, arXiv:math/0411498 [math.NT], 2004.",
				"C. Lin and L. Zhipeng, \u003ca href=\"http://arXiv.org/abs/math.HO/0408082\"\u003eOn Bernoulli numbers and its properties\u003c/a\u003e, arXiv:math/0408082 [math.HO], 2004.",
				"Guo-Dong Liu, H. M. Srivastava, and Hai-Quing Wang, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Srivastava/sriva3.html\"\u003eSome Formulas for a Family of Numbers Analogous to the Higher-Order Bernoulli Numbers\u003c/a\u003e, J. Int. Seq. 17 (2014) # 14.4.6.",
				"H.-M. Liu, S-H. Qi, and S.-Y. Ding, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL13/Liu/liu4.html\"\u003eSome Recurrence Relations for Cauchy Numbers of the First Kind\u003c/a\u003e, JIS 13 (2010) # 10.3.8.",
				"S. O. S. Math, \u003ca href=\"http://www.sosmath.com/tables/bernoulli/bernoulli.html\"\u003eBernoulli and Euler Numbers\u003c/a\u003e",
				"R. Mestrovic, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Mestrovic/mes4.html\"\u003eOn a Congruence Modulo n^3 Involving Two Consecutive Sums of Powers\u003c/a\u003e, Journal of Integer Sequences, Vol. 17 (2014), 14.8.4.",
				"Hisanori Mishima, \u003ca href=\"http://www.asahi-net.or.jp/~KC2H-MSM/mathland/matha1/matha134.htm\"\u003eFactorizations of many number sequences\u003c/a\u003e",
				"Hisanori Mishima, \u003ca href=\"http://www.asahi-net.or.jp/~KC2H-MSM/mathland/matha1/matha1341.htm\"\u003eFactorizations of many number sequences\u003c/a\u003e",
				"Niels Nielsen, \u003ca href=\"http://gallica.bnf.fr/ark:/12148/bpt6k62119c\"\u003eTraite Elementaire des Nombres de Bernoulli\u003c/a\u003e, Gauthier-Villars, 1923, pp. 398.",
				"N. E. Nörlund, \u003ca href=\"/A001896/a001896_1.pdf\"\u003eVorlesungen über Differenzenrechnung\u003c/a\u003e, Springer-Verlag, Berlin, 1924 [Annotated scanned copy of pages 144-151 and 456-463]",
				"Ronald Orozco López, \u003ca href=\"https://www.researchgate.net/publication/350397609_Solution_of_the_Differential_Equation_ykeay_Special_Values_of_Bell_Polynomials_and_ka-Autonomous_Coefficients\"\u003eSolution of the Differential Equation y^(k)= e^(a*y), Special Values of Bell Polynomials and (k,a)-Autonomous Coefficients\u003c/a\u003e, Universidad de los Andes (Colombia 2021).",
				"Simon Plouffe, \u003ca href=\"http://plouffe.fr/simon/constants/ber250000.txt\"\u003eThe 250,000th Bernoulli Number\u003c/a\u003e",
				"Simon Plouffe, \u003ca href=\"http://www.ibiblio.org/gutenberg/etext01/brnll10.txt\"\u003eThe First 498 Bernoulli numbers\u003c/a\u003e [Project Gutenberg Etext]",
				"S. Ramanujan, \u003ca href=\"http://www.imsc.res.in/~rao/ramanujan/collectedpapers/Bernoulli/bernoulli1.html\"\u003eSome Properties of Bernoulli's Numbers\u003c/a\u003e",
				"Zhi-Hong Sun, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2007.03.038\"\u003eCongruences involving Bernoulli polynomials\u003c/a\u003e, Discr. Math., 308 (2007), 71-112.",
				"S. S. Wagstaff, \u003ca href=\"http://www.cerias.purdue.edu/homes/ssw/bernoulli/bnum\"\u003ePrime factors of the absolute values of Bernoulli numerators\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/BernoulliNumber.html\"\u003eBernoulli Number.\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Bernoulli_number\"\u003eBernoulli number\u003c/a\u003e",
				"\u003ca href=\"/index/Be#Bernoulli\"\u003eIndex entries for sequences related to Bernoulli numbers.\u003c/a\u003e"
			],
			"formula": [
				"E.g.f: x/(exp(x) - 1); take numerators of even powers.",
				"B_{2n}/(2n)! = 2*(-1)^(n-1)*(2*Pi)^(-2n) Sum_{k\u003e=1} 1/k^(2n) (gives asymptotics) - Rademacher, p. 16, Eq. (9.1). In particular, B_{2*n} ~ (-1)^(n-1)*2*(2*n)!/(2*Pi)^(2*n).",
				"If n \u003e= 3 is prime, then 12*|a((n+1)/2)| == (-1)^((n-1)/2)*A002445((n+1)/2) (mod n). - _Vladimir Shevelev_, Sep 04 2010",
				"a(n) = numerator(-i*(2*n)!/(Pi*(1-2*n))*Integral_{t=0..1} log(1-1/t)^(1-2*n) dt). - _Gerry Martens_, May 17 2011, corrected by _Vaclav Kotesovec_, Oct 22 2014",
				"a(n) = numerator((-1)^(n+1)*(2*Pi)^(-2*n)*(2*n)!*Li_{2*n}(1)) for n \u003e 0. - _Peter Luschny_, Jun 29 2012",
				"E.g.f.: G(0) where G(k) = 2*k + 1 - x*(2*k+1)/(x + (2*k+2)/(1 + x/G(k+1) )); (continued fraction). - _Sergei N. Gladkovskii_, Feb 13 2013",
				"a(n) = numerator(2*n*Sum_{k=0..2*n} (2*n+k-2)! * Sum_{j=1..k} ((-1)^(j+1) * Stirling1(2*n+j,j)) / ((k-j)!*(2*n+j)!)), n \u003e 0. - _Vladimir Kruchinin_, Mar 15 2013",
				"E.g.f.: E(0) where E(k) = 2*k+1 - x/(2 + x/E(k+1) ); (continued fraction). - _Sergei N. Gladkovskii_, Mar 16 2013",
				"E.g.f.: E(0) - x, where E(k) = x + k + 1 - x*(k+1)/E(k+1); (continued fraction). - _Sergei N. Gladkovskii_, Jul 14 2013",
				"a(n) = numerator((-1)^(n+1)*2*Gamma(2*n + 1)*zeta(2*n)/(2*Pi)^(2*n)). - _Artur Jasinski_, Dec 29 2020",
				"a(n) = numerator(-2*n*zeta(1 - 2*n)) for n \u003e 0. - _Artur Jasinski_, Jan 01 2021"
			],
			"example": [
				"B_{2n} = [ 1, 1/6, -1/30, 1/42, -1/30, 5/66, -691/2730, 7/6, -3617/510, ... ]."
			],
			"maple": [
				"A000367 := n -\u003e numer(bernoulli(2*n)):",
				"# Illustrating an algorithmic approach:",
				"S := proc(n,k) option remember; if k=0 then `if`(n=0,1,0) else S(n, k-1) + S(n-1, n-k) fi end: Bernoulli2n := n -\u003e `if`(n = 0,1,(-1)^n * S(2*n-1,2*n-1)*n/(2^(2*n-1)*(1-4^n))); A000367 := n -\u003e numer(Bernoulli2n(n)); seq(A000367(n),n=0..20); # _Peter Luschny_, Jul 08 2012"
			],
			"mathematica": [
				"Numerator[ BernoulliB[ 2*Range[0, 20]]] (* _Jean-François Alcover_, Oct 16 2012 *)",
				"Table[Numerator[(-1)^(n+1) 2 Gamma[2 n + 1] Zeta[2 n]/(2 Pi)^(2 n)], {n, 0, 20}] (* _Artur Jasinski_, Dec 29 2020 *)"
			],
			"program": [
				"(PARI) a(n)=numerator(bernfrac(2*n))",
				"(Python) # The objective of this implementation is efficiency.",
				"# n -\u003e [a(0), a(1), ..., a(n)] for n \u003e 0.",
				"from fractions import Fraction",
				"def A000367_list(n):  # Bernoulli numerators",
				"    T = [0 for i in range(1, n+2)]",
				"    T[0] = 1; T[1] = 1",
				"    for k in range(2, n+1):",
				"        T[k] = (k-1)*T[k-1]",
				"    for k in range(2, n+1):",
				"        for j in range(k, n+1):",
				"            T[j] = (j-k)*T[j-1]+(j-k+2)*T[j]",
				"    a = 0; b = 6; s = 1",
				"    for k in range(1, n+1):",
				"        T[k] = s*Fraction(T[k]*k, b).numerator",
				"        h = b; b = 20*b - 64*a; a = h; s = -s",
				"    return T",
				"print(A000367_list(100)) # _Peter Luschny_, Aug 09 2011",
				"(Maxima)",
				"B(n):=if n=0 then 1 else 2*n*sum((2*n+k-2)!*sum(((-1)^(j+1)*stirling1(2*n+j,j))/ ((k-j)!*(2*n+j)!),j,1,k),k,0,2*n);",
				"makelist(num(B(n)),n,0,10); /* _Vladimir Kruchinin_, Mar 15 2013, fixed by _Vaclav Kotesovec_, Oct 22 2014 */"
			],
			"xref": [
				"B_n gives A027641/A027642. See A027641 for full list of references, links, formulas, etc.",
				"See A002445 for denominators.",
				"Cf. also A002882, A003245, A127187, A127188."
			],
			"keyword": "sign,frac,nice",
			"offset": "0,6",
			"author": "_N. J. A. Sloane_",
			"references": 136,
			"revision": 186,
			"time": "2021-08-08T01:15:06-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}