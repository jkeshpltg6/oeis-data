{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A326410",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 326410,
			"data": "4,-1,-1,3,-1,3,-1,3,3,2,-1,5,-1,2,2,2,-1,3,-1,3,3,2,-1,2,1,0,2,3,-1,3,-1,3,3,1,2,2,-1,3,3,2,-1,3,-1,1,1,2,-1,2,1,1,1,1,-1,2,3,2,2,2,-1,2,-1,2,2,1,3,3,-1,1,2,3,-1,4,-1,3,2,0,1,2,-1,1,1",
			"name": "Minesweeper sequence of positive integers arranged on a square spiral on a 2D grid.",
			"comment": [
				"Place positive integers on a 2D grid starting with 1 in the center and continue along a spiral.",
				"Replace each prime with -1 and each nonprime with the number of primes in adjacent grid cells around it.",
				"n is replaced by a(n).",
				"This sequence treats prime numbers as \"mines\" and fills gaps according to the rules of the classic Minesweeper game.",
				"a(n) = 5 for n = 12.",
				"Set of n such that a(n) = 4 is unbounded (conjecture)."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A326410/b326410.txt\"\u003eTable of n, a(n) for n = 1..10201\u003c/a\u003e (51 spiral iterations).",
				"Michael De Vlieger, \u003ca href=\"/A326410/a326410.png\"\u003eMinesweeper-style graph\u003c/a\u003e read along original mapping, replacing -1 with a \"mine\", and 0 with blank space.",
				"Michael De Vlieger, \u003ca href=\"/A326410/a326410_1.png\"\u003eSquare plot of 10^3 spiral iterations\u003c/a\u003e read along original mapping, with black indicating a prime and levels of gray commensurate to a(n).",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Minesweeper_(video_game)\"\u003eMinesweeper game\u003c/a\u003e"
			],
			"example": [
				"Consider positive integers distributed onto the plane along the square spiral:",
				".",
				"  37--36--35--34--33--32--31",
				"   |                       |",
				"  38  17--16--15--14--13  30",
				"   |   |               |   |",
				"  39  18   5---4---3  12  29",
				"   |   |   |       |   |   |",
				"  40  19   6   1---2  11  28",
				"   |   |   |           |   |",
				"  41  20   7---8---9--10  27",
				"   |   |                   |",
				"  42  21--22--23--24--25--26",
				"   |",
				"  43--44--45--46--47--48--49--...",
				".",
				"1 is not prime and in adjacent grid cells there are 4 primes: 2, 3, 5 and 7. Therefore a(1) = 4.",
				"2 is prime, therefore a(2) = -1.",
				"8 is not prime and in adjacent grid cells there are 4 primes: 2, 7 and 23. Therefore a(8) = 3.",
				"Replacing n with a(n) in the plane described above, and using \".\" for a(n) = 0 and \"*\" for negative a(n), we produce a graph resembling Minesweeper, where the mines are situated at prime n:",
				"  *---2---2---1---3---3---*",
				"  |                       |",
				"  3   *---2---2---2---*   3",
				"  |   |               |   |",
				"  3   3   *---3---*   5   *",
				"  |   |   |       |   |   |",
				"  2   *   3   4---*   *   3",
				"  |   |   |           |   |",
				"  *   3   *---3---3---2   2",
				"  |   |                   |",
				"  3   3---2---*---2---1---.",
				"  |",
				"  *---1---1---2---*---2---1---...",
				"In order to produce the sequence, the graph is read along the square spiral."
			],
			"xref": [
				"Cf. A136626 - similar sequence: For every number n in Ulam's spiral the sequence gives the number of primes around it (number n excluded).",
				"Cf. A136627 - similar sequence: For every number n in Ulam's spiral the sequence gives the number of primes around it (number n included).",
				"Different arrangements of integers:",
				"Cf. A326405 (antidiagonals), A326406 (triangle maze), A326407 (square mapping), A326408 (square maze), A326409 (Hamiltonian path)."
			],
			"keyword": "sign,tabl",
			"offset": "1,1",
			"author": "_Witold Tatkiewicz_, Oct 07 2019",
			"references": 7,
			"revision": 20,
			"time": "2020-03-20T23:36:57-04:00",
			"created": "2019-10-08T14:27:45-04:00"
		}
	]
}