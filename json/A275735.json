{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A275735",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 275735,
			"data": "1,2,2,4,3,6,2,4,4,8,6,12,3,6,6,12,9,18,5,10,10,20,15,30,2,4,4,8,6,12,4,8,8,16,12,24,6,12,12,24,18,36,10,20,20,40,30,60,3,6,6,12,9,18,6,12,12,24,18,36,9,18,18,36,27,54,15,30,30,60,45,90,5,10,10,20,15,30,10,20,20,40,30,60,15,30,30,60,45,90,25,50,50,100,75",
			"name": "Prime-factorization representations of \"factorial base level polynomials\": a(0) = 1; for n \u003e= 1, a(n) = 2^A257511(n) * A003961(a(A257684(n))).",
			"comment": [
				"These are prime-factorization representations of single-variable polynomials where the coefficient of term x^(k-1) (encoded as the exponent of prime(k) in the factorization of n) is equal to the number of times a nonzero digit k occurs in the factorial base representation of n. See the examples."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A275735/b275735.txt\"\u003eTable of n, a(n) for n = 0..40320\u003c/a\u003e",
				"\u003ca href=\"/index/Fa#facbase\"\u003eIndex entries for sequences related to factorial base representation\u003c/a\u003e"
			],
			"formula": [
				"a(0) = 1; for n \u003e= 1, a(n) = 2^A257511(n) * A003961(a(A257684(n))).",
				"Other identities and observations. For all n \u003e= 0:",
				"a(n) = A275734(A225901(n)).",
				"A001221(a(n)) = A275806(n).",
				"A001222(a(n)) = A060130(n).",
				"A048675(a(n)) = A275729(n).",
				"A051903(a(n)) = A264990(n).",
				"A008683(a(A265349(n))) = -1 or +1 for all n \u003e= 0.",
				"A008683(a(A265350(n))) = 0 for all n \u003e= 1."
			],
			"example": [
				"For n = 0 whose factorial base representation (A007623) is also 0, there are no nonzero digits at all, thus there cannot be any prime present in the encoding, and a(0) = 1.",
				"For n = 1 there is just one 1, thus a(1) = prime(1) = 2.",
				"For n = 2 (\"10\", there is just one 1-digit, thus a(2) = prime(1) = 2.",
				"For n = 3 (\"11\") there are two 1-digits, thus a(3) = prime(1)^2 = 4.",
				"For n = 18 (\"300\") there is just one 3, thus a(18) = prime(3) = 5.",
				"For n = 19 (\"301\") there is one 1 and one 3, thus a(19) = prime(1)*prime(3) = 2*5 = 10.",
				"For n = 141 (\"10311\") there are three 1's and one 3, thus a(141) = prime(1)^3 * prime(3) = 2^3 * 5^1 = 40."
			],
			"program": [
				"(Scheme, with memoization-macro definec)",
				"(definec (A275735 n) (if (zero? n) 1 (* (A000079 (A257511 n)) (A003961 (A275735 (A257684 n))))))",
				"(Python)",
				"from sympy import prime",
				"from operator import mul",
				"import collections",
				"def a007623(n, p=2): return n if n\u003cp else a007623(n//p, p+1)*10 + n%p",
				"def a(n):",
				"    y=collections.Counter(map(int, list(str(a007623(n)).replace(\"0\", \"\")))).most_common()",
				"    return 1 if n==0 else reduce(mul, [prime(y[i][0])**y[i][1] for i in range(len(y))])",
				"print([a(n) for n in range(101)]) # _Indranil Ghosh_, Jun 19 2017"
			],
			"xref": [
				"Cf. A000079, A001221, A001222, A003961, A007623, A008683, A225901, A257511, A257684, A265349, A265350, A264990, A275729, A275806.",
				"Cf. also A275725, A275733, A275734 for other such prime factorization encodings of A060117/A060118-related polynomials.",
				"Differs from A227154 for the first time at n=18, where a(18) = 5, while A227154(18) = 4."
			],
			"keyword": "nonn,base",
			"offset": "0,2",
			"author": "_Antti Karttunen_, Aug 09 2016",
			"references": 19,
			"revision": 25,
			"time": "2021-03-12T16:01:20-05:00",
			"created": "2016-08-11T23:45:45-04:00"
		}
	]
}