{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A276533",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 276533,
			"data": "5,2,19,127,17,67,163,41,89,101,131,313,257,211,227,461,241,401,613,337,433,353,577,467,863,887,617,787,601,569,761,641,823,673,857,1217,881,1091,1289,977,1427,1097,1801,929,1153,953,1321,1049,1747,1409",
			"name": "Least prime p with A271518(p) = n.",
			"comment": [
				"Conjecture: a(n) exists for any positive integer n.",
				"In contrast, it is known that for each prime p the number of ordered integral solutions to the equation x^2 + y^2 + z^2 + w^2 = p is 8*(p+1).",
				"In 1998 J. Friedlander and H. Iwaniec proved that there are infinitely many primes p of the form w^2 + x^4 = w^2 + (x^2)^2 + 0^2 + 0^2 with w and x nonnegative integers. Since x^2 + 3*0 + 5*0 is a square, we see that A271518(p) \u003e 0 for infinitely many primes p."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A276533/b276533.txt\"\u003eTable of n, a(n) for n = 1..500\u003c/a\u003e",
				"J. Friedlander and H. Iwaniec, \u003ca href=\"https://arxiv.org/abs/math/9811185\"\u003eThe polynomial x^2 + y^4 captures its primes\u003c/a\u003e, arXiv:math/9811185 [math.NT], 1998; Ann. of Math. 148 (1998), 945-1040.",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1604.06723\"\u003eRefining Lagrange's four-square theorem\u003c/a\u003e, arXiv:1604.06723 [math.GM], 2016."
			],
			"example": [
				"a(1) = 5 since 5 is the first prime which can be written in a unique way as x^2 + y^2 + z^2 + w^2 with x,y,z,w nonnegative integer and x + 3*y + 5*z a square; in fact, 5 = 1^2 + 0^2 + 0^2 + 2^2 with 1 + 3*0 + 5*0 = 1^2.",
				"a(2) = 2 since 2 = 1^2 + 0^2 + 0^2 + 1^2 with 1 + 3*0 + 5*0 = 1^2, and 2 = 1^2 + 1^2 + 0^2 + 0^2 with 1 + 3*1 + 5*0 = 2^2."
			],
			"mathematica": [
				"SQ[n_]:=SQ[n]=IntegerQ[Sqrt[n]];",
				"Do[m=0;Label[aa];m=m+1;r=0;Do[If[SQ[Prime[m]-x^2-y^2-z^2]\u0026\u0026SQ[x+3y+5z],r=r+1;If[r\u003en,Goto[aa]]],{x,0,Sqrt[Prime[m]]},{y,0,Sqrt[Prime[m]-x^2]},{z,0,Sqrt[Prime[m]-x^2-y^2]}];If[r\u003cn,Goto[aa],Print[n,\" \",Prime[m]]];Continue,{n,1,50}]"
			],
			"xref": [
				"Cf. A000040, A000118, A000290, A028916, A271518, A273294, A273302, A278560."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Zhi-Wei Sun_, Dec 12 2016",
			"references": 2,
			"revision": 21,
			"time": "2021-08-04T03:15:10-04:00",
			"created": "2016-12-12T07:48:55-05:00"
		}
	]
}