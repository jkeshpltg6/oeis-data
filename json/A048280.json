{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A048280",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 48280,
			"data": "2,2,3,3,3,3,5,4,5,4,4,4,5,5,5,3,5,5,6,7,9,6,7,5,9,7,7,6,5,5,7,8,6,5,4,7,6,6,6,6,6,6,7,9,7,6,7,7,7,5,6,7,13,7,6,7,8,7,10,6,9,9,7,11,9,5,8,9,8,6,6,8,9,6,8,8,8,5,7,13,8,7,7,9,10,8,8,9,8,8,11,13,8,8,10,8,9,8,10,7,9,9,10,10,7,9",
			"name": "Length of longest run of consecutive quadratic residues mod prime(n).",
			"comment": [
				"0 and 1 are consecutive quadratic residues for any prime, so a(n) \u003e= 2.",
				"\"Consecutive\" allows wrap-around, so p-1 and 0 are consecutive. - _Robert Israel_, Jul 20 2014",
				"A002307(n) is defined similarly, except that only positive reduced quadratic residues are counted. - _Jonathan Sondow_, Jul 20 2014",
				"For longest runs of quadratic nonresidues, see A002308. - _Jonathan Sondow_, Jul 20 2014"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A048280/b048280.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"P. Pollack and E. Treviño, \u003ca href=\"http://pollack.uga.edu/mullin.pdf\"\u003eThe primes that Euclid forgot\u003c/a\u003e, Amer. Math. Monthly, 121 (2014), 433-437.",
				"Enrique Treviño, \u003ca href=\"http://campus.lakeforest.edu/trevino/CorrigendumConstantCharacter.pdf\"\u003eCorrigendum to “On the maximum number of consecutive integers on which a character is constant”\u003c/a\u003e, Mosc. J. Comb. Number Theory 7 (2017), no. 3, 1-2."
			],
			"formula": [
				"a(n) \u003c 2*sqrt(prime(n)) for n \u003e= 1 (see Pollack and Treviño for n \u003e 1). - _Jonathan Sondow_, Jul 20 2014",
				"a(n) \u003e= A002307(n). - _Jonathan Sondow_, Jul 20 2014",
				"a(n) \u003c 7 prime(n)^(1/4)log(prime(n)) for all n \u003e 1, or a(n) \u003c 3.2 prime(n)^(1/4)log(prime(n)) for n \u003e= 10^13. - _Enrique Treviño_, Apr 16 2020"
			],
			"example": [
				"For n = 7, prime(7) = 17 has consecutive quadratic residues 15,16,0,1,2, and no longer sequence of consecutive quadratic residues, so a(7)=5."
			],
			"maple": [
				"A:= proc(n) local P, res, nonres, nnr;",
				"     P:= ithprime(n);",
				"     res:= {seq(i^2,i=0..floor((P-1)/2)};",
				"     nonres:= {$1..P-1} minus res;",
				"     nnr:= nops(nonres);",
				"     max(seq(nonres[i+1]-nonres[i]-1,i=1..nnr-1),nonres[1]-nonres[-1]+P-1)",
				"end proc;",
				"A(1):= 2:",
				"seq(A(n),n=1..100); # _Robert Israel_, Jul 20 2014"
			],
			"xref": [
				"Cf. A002307, A048281."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_David W. Wilson_",
			"ext": [
				"Offset corrected to 1 and definition clarified by _Jonathan Sondow_ Jul 20 2014"
			],
			"references": 3,
			"revision": 41,
			"time": "2020-05-23T22:45:24-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}