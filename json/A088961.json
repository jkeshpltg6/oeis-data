{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A088961",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 88961,
			"data": "3,5,5,5,10,14,14,7,14,21,21,7,21,35,42,48,27,9,48,69,57,36,27,57,78,84,9,36,84,126,132,165,110,44,11,165,242,209,121,55,110,209,253,220,165,44,121,220,297,330,11,55,165,330,462",
			"name": "Zigzag matrices listed entry by entry.",
			"comment": [
				"For each n \u003e= 1 the n X n matrix Z(n) is constructed as follows. The i-th row of Z(n) is obtained by generating a hexagonal array of numbers with 2*n+1 rows, 2*n numbers in the odd numbered rows and 2*n+1 numbers in the even numbered rows. The first row is all 0's except for two 1's in the i-th and the (2*n+1-i)th positions. The remaining rows are generated using the same rule for generating Pascal's triangle. The i-th row of Z(n) then consists of the first n numbers in the bottom row of our array.",
				"For example the top row of Z(2) is [5,5], found from the array:",
				". 1 0 0 1",
				"1 1 0 1 1",
				". 2 1 1 2",
				"2 3 2 3 2",
				". 5 5 5 5",
				"Zigzag matrices have remarkable properties. Here is a selection:",
				"1) Z(n) is symmetric.",
				"2) det(Z(n)) = A085527(n).",
				"3) tr(Z(n)) = A033876(n-1).",
				"4) If 2*n+1 is a power of a prime p then all entries of Z(n) are multiples of p.",
				"5) If 4*n+1 is a power of a prime p then the dot product of any two distinct rows of Z(n) is a multiple of p.",
				"6) It is always possible to move from the bottom left entry of Z(n) to the top right entry using only rightward and upward moves and visiting only odd numbers.",
				"A001700(n) = last term of last row of Z(n): a(A000330(n-1)) = A001700(n); A230585(n) = first term of first row of Z(n): a(A056520(n-1)) = A230585(n); A051417(n) = greatest common divisor of entries of Z(n). - _Reinhard Zumkeller_, Oct 25 2013"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A088961/b088961.txt\"\u003eMatrices Z(n): n = 1..30, flattened\u003c/a\u003e"
			],
			"formula": [
				"The ij entry of Z(n) is binomial(2*n, n+j-i) - binomial(2*n, n+i+j) + binomial(2*n, 3*n+1-i-j)."
			],
			"example": [
				"The first five values are 3, 5, 5, 5, 10 because the first two zigzag matrices are [[3]] and [[5,5],[5,10]]."
			],
			"mathematica": [
				"Flatten[Table[Binomial[2n,n+j-i]-Binomial[2n,n+i+j]+ Binomial[2n, 3n+1-i-j], {n,5},{i,n},{j,n}]] (* _Harvey P. Dale_, Dec 15 2011 *)"
			],
			"program": [
				"(Haskell)",
				"a088961 n = a088961_list !! (n-1)",
				"a088961_list = concat $ concat $ map f [1..] where",
				"   f x = take x $ g (take x (1 : [0,0..])) where",
				"     g us = (take x $ g' us) : g (0 : init us)",
				"     g' vs = last $ take (2 * x + 1) $",
				"                    map snd $ iterate h (0, vs ++ reverse vs)",
				"   h (p,ws) = (1 - p, drop p $ zipWith (+) ([0] ++ ws) (ws ++ [0]))",
				"-- _Reinhard Zumkeller_, Oct 25 2013"
			],
			"xref": [
				"Cf. A085527, A003876."
			],
			"keyword": "easy,nice,nonn,look",
			"offset": "1,1",
			"author": "_Paul Boddington_, Oct 28 2003",
			"references": 6,
			"revision": 15,
			"time": "2015-08-19T11:59:04-04:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}