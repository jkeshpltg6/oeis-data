{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A330781",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 330781,
			"data": "1,2,12,36,360,27000,75600,378000,1587600,174636000,1944810000,5762988000,42785820000,5244319080000,36710233560000,1431699108840000,65774855015100000,731189187729000000,1710146230392600000,2677277333530800000,2267653901500587600000,115650348976529967600000",
			"name": "Numbers m that have recursively self-conjugate prime signatures.",
			"comment": [
				"Let m be a product of a primorial, listed by A025487.",
				"Consider the standard form prime power decomposition of m = Product(p^e), where prime p | m (listed from smallest to largest p), and e is the largest multiplicity of p such that p^e | m (which we shall hereinafter simply call \"multiplicity\").",
				"Products of primorials have a list L of multiplicities in a strictly decreasing arrangement.",
				"A recursively self-conjugate L has a conjugate L* = L. Further, elimination of the Durfee square and leg (conjugate with the arm) to leave the arm L_1. L_1 likewise has conjugate L_1* = L_1. We continue taking the arm, eliminating the new Durfee square and leg in this manner until the entire list L is processed and all arms are self-conjugate.",
				"a(n) is a subsequence of A181825 (m with self-conjugate prime signatures).",
				"Subsequences of a(n) include A006939 and A181555.",
				"This sequence can be produced by a similar algorithm that pertains to recursively self-conjugate integer partitions at A322156.",
				"From _Michael De Vlieger_, Jan 16 2020: (Start)",
				"2 is the only prime in a(n).",
				"The smallest 2 terms of a(n) are primorials, i.e., in A002110.",
				"The smallest 5 terms of a(n) are highly composite, i.e., in A002182. (End)"
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A330781/b330781.txt\"\u003eTable of n, a(n) for n = 1..2243\u003c/a\u003e",
				"Michael De Vlieger, \u003ca href=\"/A330781/a330781.txt\"\u003eA322156 encoding for a(n) for n = 1..75047\u003c/a\u003e, with the largest term a(75047) = A002110(60)^60 (8019 decimal digits).",
				"Michael De Vlieger, \u003ca href=\"/A330781/a330781_1.txt\"\u003eIndices of terms in a(n) that are also in the Chernoff Sequence (A006939)\u003c/a\u003e",
				"Michael De Vlieger, \u003ca href=\"/A330781/a330781_2.txt\"\u003eIndices of terms m in a(n) that are also in A181555 = A002110(n)^n\u003c/a\u003e (useful for assuring a(n) \u003c= m is complete)."
			],
			"example": [
				"A025487(1) = 1, the empty product, is in the sequence since it is the product of no primes at all; this null sequence is self-conjugate.",
				"A025487(2) = 2 = 2^1 -\u003e {1} is self conjugate.",
				"A025487(6) = 12 = 2^2 * 3 -\u003e {2, 1} is self conjugate.",
				"A025487(32) = 360 = 2^3 * 3^2 * 5 -\u003e {3, 2, 1} is self-conjugate.",
				"Graphing the multiplicities, we have:",
				"3  x           3  x",
				"2  x x   ==\u003e   2  x x",
				"1  x x x       1  x x x",
				"   2 3 5          2 3 5",
				"where the vertical axis represents multiplicity and the horizontal the k-th prime p, and the arrow represents the transposition of the x's in the graph. We see that the transposition does not change the prime signature (thus, m is also in A181825), and additionally, the prime signature is recursively self-conjugate."
			],
			"mathematica": [
				"Block[{n = 6, f, g}, f[n_] := Block[{w = {n}, c}, c[x_] := Apply[Times, Most@ x - Reverse@ Accumulate@ Reverse@ Rest@ x]; Reap[Do[Which[And[Length@ w == 2, SameQ @@ w], Sow[w]; Break[], Length@ w == 1, Sow[w]; AppendTo[w, 1], c[w] \u003e 0, Sow[w]; AppendTo[w, 1], True, Sow[w]; w = MapAt[1 + # \u0026, Drop[w, -1], -1]], {i, Infinity}] ][[-1, 1]] ]; g[w_] := Block[{k}, k = Total@ w; Total@ Map[Apply[Function[{s, t}, s Array[Boole[t \u003c= # \u003c= s + t - 1] \u0026, k] ], #] \u0026, Apply[Join, Prepend[Table[Function[{v, c}, Map[{w[[k]], # + 1} \u0026, Map[Total[v #] \u0026, Tuples[{0, 1}, {Length@ v}]]]] @@ {Most@ #, ConstantArray[1, Length@ # - 1]} \u0026@ Take[w, k], {k, 2, Length@ w}], {{w[[1]], 1}}]]] ]; {1}~Join~Take[#, FirstPosition[#, StringJoin[\"{\", ToString[n], \"}\"]][[1]] ][[All, 1]] \u0026@ Sort[MapIndexed[{Times @@ Flatten@ MapIndexed[Prime[#2]^#1 \u0026, #2], ToString@ #1} \u0026 @@ {#1, g[#1], First@ #2} \u0026, Apply[Join, Array[f[#] \u0026, n] ] ] ] ]",
				"(* Second program: decompress dataset of a(n) for n = 0..75047 *)",
				"{1}~Join~Map[Block[{k, w = ToExpression@ StringSplit[#, \" \"]}, k = Total@ w; Times @@ Flatten@ MapIndexed[Prime[#2]^#1 \u0026, Total@ #] \u0026@ Map[Apply[Function[{s, t}, s Array[Boole[t \u003c= # \u003c= s + t - 1] \u0026, k] ], #] \u0026, Apply[Join, Prepend[Table[Function[{v, c}, Map[{w[[k]], # + 1} \u0026, Map[Total[v #] \u0026, Tuples[{0, 1}, {Length@ v}]]]] @@ {Most@ #, ConstantArray[1, Length@ # - 1]} \u0026@ Take[w, k], {k, 2, Length@ w}], {{w[[1]], 1}}]]] ] \u0026, Import[\"https://oeis.org/A330781/a330781.txt\", \"Data\"] ] (* _Michael De Vlieger_, Jan 16 2020 *)"
			],
			"xref": [
				"Cf. A006939, A025487, A181555, A181822, A181825, A322156."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Michael De Vlieger_, Jan 02 2020",
			"references": 1,
			"revision": 36,
			"time": "2020-01-20T12:56:59-05:00",
			"created": "2020-01-13T20:41:24-05:00"
		}
	]
}