{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A267485",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 267485,
			"data": "1,1,2,-2,-3,1,1,-2,2,17,-9,-32,12,24,-6,-8,1,1,-2,-6,25,71,-80,-218,126,284,-106,-190,48,69,-11,-13,1,1,3,-6,-70,101,506,-453,-1592,980,2658,-1201,-2608,886,1581,-400,-600,108,139,-16,-18,1,1,3,12,-88,-334,779,2774,-3226,-10389,7709,21620,-11608,-27865,11496,23591,-7645,-13512,3427,5276,-1020,-1385,193,234,-21,-23,1,1",
			"name": "Triangle of coefficients of Gaussian polynomials [2n+5,5]_q represented as finite sum of terms (1+q^2)^k*q^(g-k), where k = 0,1,...,g with g=5n.",
			"comment": [
				"The entry a(n,k), n \u003e= 0, k = 0,1,...,g, where g=5n, of this irregular triangle is the coefficient of (1+q^2)^k*q^(g-k) in the representation of the Gaussian polynomial [2n+5,5]_q = Sum_{k=0..g) a(n,k)*(1+q^2)^k*q^(g-k).",
				"Row n is of length 5n+1.",
				"The sequence arises in the formal derivation of the stability polynomial B(x) = Sum_{i=0..N} d_i T(iM,x) of rank N, and degree L, where T(iM,x) denotes the Chebyshev polynomial of the first kind of degree iM. The coefficients d_i are determined by order conditions on the stability polynomial.",
				"Conjecture: More generally, the Gaussian polynomial [2*n+m+1-(m mod 2),m]_q = Sum_{k=0..g(m;n)} a(m;n,k)*(1+q^2)^k*q^(g(m;n)-k), for m \u003e= 0, n \u003e= 0, where g(m;n) = m*n if m is odd and (2*n+1)*m/2 if m is even, and the tabf array entries a(m;n,k) are the coefficients of the g.f. for the row n polynomials G(m;n,x) = (d^m/dt^m)G(m;n,t,x)/m!|_{t=0}, with G(m;n,t,x) = (1+t)*Product_{k=1..n+(m - m (mod 2))/2}(1 + t^2 + 2*t*T(k,x/2) (Chebyshev's T-polynomials). Hence a(m;n,k) = [x^k]G(m;n,x), for k=0..g(m;n). The present entry is the instance m = 2. (Thanks to _Wolfdieter Lang_ for clarifying the text on the general prescription of a(m;n,k).)"
			],
			"link": [
				"Stephen O'Sullivan, \u003ca href=\"/A267485/b267485.txt\"\u003eTable of n, a(n) for n = 0..1070\u003c/a\u003e",
				"S. O'Sullivan, \u003ca href=\"http://dx.doi.org/10.1016/j.jcp.2015.07.050\"\u003eA class of high-order Runge-Kutta-Chebyshev stability polynomials\u003c/a\u003e, Journal of Computational Physics, 300 (2015), 665-678.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Gaussian_binomial_coefficient\"\u003eGaussian binomial coefficients\u003c/a\u003e."
			],
			"formula": [
				"G.f. for row polynomial: G(n,x) = (d^5/dt^5)((1+t)*Product_{i=1..n+1}(1+t^2+2t*T(i,x/2))/5!)|_{t=0}."
			],
			"example": [
				"1;",
				"1,2,-2,-3,1,1;",
				"-2,2,17,-9,-32,12,24,-6,-8,1,1;",
				"-2,-6,25,71,-80,-218,126,284,-106,-190,48,69,-11,-13,1,1;"
			],
			"maple": [
				"A267485 := proc (n, k) local y: y := expand(subs(t = 0, diff((1+t)*product(1+t^2+2*t*ChebyshevT(i, x/2), i = 1 .. n+2),t$5)/5!)): if k = 0 then subs(x = 0, y) else subs(x = 0, diff(y, x$k)/k!) end if: end proc: seq(seq(A267485(n, k), k = 0 .. 5*n), n = 0 .. 5);"
			],
			"mathematica": [
				"row[n_] := 1/5! D[(1+t)*Product[1+t^2+2*t*ChebyshevT[i, x/2], {i, 1, n+1}], {t, 5}] /. t -\u003e 0 // CoefficientList[#, x]\u0026; Table[row[n], {n, 0, 20}] // Flatten (* From A267120 entry by _Jean-François Alcover_ *)"
			],
			"xref": [
				"Cf. A267120, A267482, A267483, A267484, A267486."
			],
			"keyword": "sign,tabf",
			"offset": "0,3",
			"author": "_Stephen O'Sullivan_, Jan 15 2016",
			"ext": [
				"Added row length"
			],
			"references": 6,
			"revision": 21,
			"time": "2017-12-19T01:46:34-05:00",
			"created": "2016-02-20T20:52:42-05:00"
		}
	]
}