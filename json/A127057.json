{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A127057",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 127057,
			"data": "1,3,1,4,1,1,7,3,1,1,6,1,1,1,1,12,6,3,1,1,1,8,1,1,1,1,1,1,15,7,3,3,1,1,1,1,13,4,4,1,1,1,1,1,1,18,8,3,3,3,1,1,1,1,1,12,1,1,1,1,1,1,1,1,1,1,28,16,10,6,3,3,1,1,1,1,1,1,14,1,1,1,1,1,1,1,1,1,1,1,1,24,10,3,3,3,3,3,1,1",
			"name": "Triangle T(n,k), partial row sums of the n-th row of A127013 read right to left.",
			"comment": [
				"Also partial row sums of the n-th row of A126988 read left to right. - _Reinhard Zumkeller_, Jan 21 2014"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A127057/b127057.txt\"\u003eRows n = 1..125 of triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = Sum_{i=1..n-k+1} A127013(n,i), n\u003e=1, 1\u003c=k\u003c=n.",
				"T(n,k) = Sum_{i=k..n} A126988(n,i).",
				"Row sums: Sum_{k=1..n} T(n,k) = A038040(n).",
				"T(n,1) = A000203(n).",
				"T = A126988 * M as infinite lower triangular matrices, M = (1; 1, 1; 1, 1, 1; ...)."
			],
			"example": [
				"The triangle starts",
				"   1;",
				"   3, 1;",
				"   4, 1, 1;",
				"   7, 3, 1, 1;",
				"   6, 1, 1, 1, 1;",
				"  12, 6, 3, 1, 1, 1;",
				"   8, 1, 1, 1, 1, 1, 1;",
				"  15, 7, 3, 3, 1, 1, 1, 1;",
				"  13, 4, 4, 1, 1, 1, 1, 1, 1;",
				"  18, 8, 3, 3, 3, 1, 1, 1, 1, 1; ..."
			],
			"mathematica": [
				"A126988[n_, m_]:= If[Mod[n, m]==0, n/m, 0];",
				"T[n_, m_]:= Sum[A126988[n, j], {j,m,n}];",
				"Table[T[n, m], {n,1,12}, {m,1,n}]//Flatten (* _G. C. Greubel_, Jun 03 2019 *)"
			],
			"program": [
				"(Haskell)",
				"a127057 n k = a127057_tabl !! (n-1) !! (k-1)",
				"a127057_row n = a127057_tabl !! (n-1)",
				"a127057_tabl = map (scanr1 (+)) a126988_tabl",
				"-- _Reinhard Zumkeller_, Jan 21 2014",
				"(PARI)",
				"A126988(n, k) = if(n%k==0, n/k, 0);",
				"T(n,k) = sum(j=k,n, A126988(n,j));",
				"for(n=1, 12, for(k=1,n, print1(T(n,k), \", \"))) \\\\ _G. C. Greubel_, Jun 03 2019",
				"(MAGMA)",
				"A126988:= func\u003c n,k | (n mod k) eq 0 select n/k else 0 \u003e;",
				"T:= func\u003c n,k | (\u0026+[A126988(n, j): j in [k..n]]) \u003e;",
				"[[T(n,k): k in [1..n]]: n in [1..12]]; // _G. C. Greubel_, Jun 03 2019",
				"(Sage)",
				"def A126988(n, k):",
				"    if (n%k==0): return n/k",
				"    else: return 0",
				"def T(n,k): return sum(A126988(n,j) for j in (k..n))",
				"[[T(n, k) for k in (1..n)] for n in (1..12)] # _G. C. Greubel_, Jun 03 2019"
			],
			"xref": [
				"Cf. A126988, A127013, A000203, A038040."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Gary W. Adamson_, Jan 04 2007",
			"ext": [
				"Edited and extended by _R. J. Mathar_, Jul 23 2008"
			],
			"references": 4,
			"revision": 11,
			"time": "2019-06-03T17:58:36-04:00",
			"created": "2007-01-12T03:00:00-05:00"
		}
	]
}