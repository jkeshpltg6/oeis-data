{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A240908",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 240908,
			"data": "0,7,3,4,1,6,2,5",
			"name": "The sequency numbers of the 8 rows of a version of the Hadamard-Walsh matrix of order 8.",
			"comment": [
				"The Hadamard (Hadamard-Walsh) matrix is widely used in telecommunications and signal analysis. It has 3 well-known forms which vary according to the sequency ordering of its rows: \"natural\" ordering, \"dyadic\" or Payley ordering, and sequency ordering.  In a mathematical context the sequency is the number of zero crossings or transitions in a matrix row (although in a physical signal context, it is half the number of zero crossings per time period). The matrix row sequencies are a permutation of the set [0,1,2,...n-1], where n is the order of the matrix. For spectral analysis of signals the sequency-ordered form is needed. Unlike the dyadic ordering (given by A153141), the natural ordering requires a separate list for each matrix order. This sequence is the natural sequency ordering for an order 8 matrix."
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"http://neilsloane.com/hadamard\"\u003eA Library of Hadamard Matrices\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/HadamardMatrix.html\"\u003eHadamard Matrix\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Walsh_matrix\"\u003eWalsh matrix\u003c/a\u003e"
			],
			"formula": [
				"Recursion: H(2)=[1 1; 1 -1]; H(n) = H(n-1)*H(2),  where * is Kronecker matrix product."
			],
			"example": [
				"This is a fixed length sequence of only 8 values, as given."
			],
			"xref": [
				"Cf. A240909 \"natural order\" sequencies for Hadamard-Walsh matrix, order 16.",
				"Cf. A240910 \"natural order\" sequencies for Hadamard-Walsh matrix, order 32.",
				"Cf. A153141 \"dyadic order\" sequencies for Hadamard-Walsh matrix, all orders.",
				"Cf. A000975(n) is sequency of last row of H(n). - _William P. Orrick_, Jun 28 2015"
			],
			"keyword": "nonn,fini,full",
			"offset": "1,2",
			"author": "_Ross Drewe_, Apr 14 2014",
			"ext": [
				"Definition of H(n) corrected by _William P. Orrick_, Jun 28 2015"
			],
			"references": 4,
			"revision": 28,
			"time": "2015-06-29T06:16:54-04:00",
			"created": "2014-09-18T20:49:11-04:00"
		}
	]
}