{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A128692",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 128692,
			"data": "1,-16,128,-704,3072,-11488,38400,-117632,335872,-904784,2320128,-5702208,13504512,-30952544,68901888,-149403264,316342272,-655445792,1331327616,-2655115712,5206288384,-10049485312,19115905536,-35867019904,66437873664",
			"name": "Expansion of (theta_4(q) / theta_3(q))^4 in powers of q.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"reference": [
				"J. H. Conway and N. J. A. Sloane, \"Sphere Packings, Lattices and Groups\", Springer-Verlag, p. 102."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A128692/b128692.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/EllipticLambdaFunction.html\"\u003eElliptic Lambda Function\u003c/a\u003e"
			],
			"formula": [
				"Expansion of 1 - lambda(tau) = lambda( -1 / tau) in powers of q = exp(pi  i tau).",
				"Expansion of (eta(q^4) * eta(q)^2 / eta(q^2)^3)^8 in powers of q.",
				"Expansion of (phi(-q) / phi(q))^4 = (phi(-q) / phi(-q^2))^8 = (phi(-q^2) / phi(q))^8 = (f(-q) / f(q))^8 = (chi(-q) / chi(q))^8 = (psi(-q) / psi(q))^8 in powers of q where phi(), psi(), chi(), f() are Ramanujan theta functions.",
				"Euler transform of period 4 sequence [ -16, 8, -16, 0, ...].",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2)) where f(u, v) = v^2 * (1 - u)^2 - 16 * u * (1 - v).",
				"G.f.: (Product_{k\u003e0} (1 - x^(2*k - 1)) / (1 + x^(2*k - 1)))^8 = exp( -16 * Sum_{k\u003e0} x^(2*k - 1) * sigma(2*k - 1) / (2*k - 1)).",
				"A014972(n) = (-1)^n * a(n). Convolution inverse of A014972.",
				"Empirical : Sum_{n=1..infinity} (exp(-2*Pi)^(n-1)*a(n)) = -16+12*2^(1/2). - _Simon Plouffe_, Feb. 20, 2011.",
				"Empirical : Sum_{n=1..infinity} exp(-Pi*sqrt(3))^(n-1)*(-1)^(n+1)*a(n) = 8 - 4*sqrt(3). - _Simon Plouffe_, Feb. 20, 2011."
			],
			"example": [
				"1 - 16*q + 128*q^2 - 704*q^3 + 3072*q^4 - 11488*q^5 + 38400*q^6 + ..."
			],
			"mathematica": [
				"CoefficientList[(QPochhammer[q]/QPochhammer[-q])^8 + O[q]^30, q] (* _Jean-François Alcover_, Nov 05 2015 *)",
				"eta[q_] := q^(1/24)*QPochhammer[q]; a[n_] := SeriesCoefficient[(eta[q^4]* eta[q]^2/eta[q^2]^3)^8, {q, 0, n}]; Table[a[n], {n, 0, 50}] (* _G. C. Greubel_, Jan 18 2018 *)"
			],
			"program": [
				"(PARI) {a(n) = local(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( (eta(x + A)^2 * eta(x^4 + A) / eta(x^2 + A)^3)^8, n))}",
				"(PARI) {a(n) = if( n\u003c0, 0, polcoeff( exp(-16 * sum(k=1, (n+1)\\2, sigma(2*k-1) / (2*k-1) * x^(2*k-1), x * O(x^n))), n))}"
			],
			"xref": [
				"Cf. A014972."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Mar 20 2007",
			"references": 5,
			"revision": 21,
			"time": "2021-03-12T22:24:44-05:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}