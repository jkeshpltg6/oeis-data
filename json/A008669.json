{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A008669",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 8669,
			"data": "1,1,2,3,4,6,8,10,13,16,20,24,29,34,40,47,54,62,71,80,91,102,114,127,141,156,172,189,207,226,247,268,291,315,340,367,395,424,455,487,521,556,593,631,671,713,756,801,848,896,947,999,1053,1109,1167,1227,1289",
			"name": "Molien series for 4-dimensional complex reflection group of order 7680 (in powers of x^4).",
			"comment": [
				"Number of partitions of n into parts 1, 2, 3 and 5. - _David Neil McGrath_, Sep 15 2014"
			],
			"reference": [
				"L. Comtet, Advanced Combinatorics, Reidel, 1974, p. 120, D(n;1,2,3,5).",
				"L. Smith, Polynomial Invariants of Finite Groups, Peters, 1995, p. 199 (No. 29)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A008669/b008669.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=239\"\u003eEncyclopedia of Combinatorial Structures 239\u003c/a\u003e",
				"Atsuto Seko, Atsushi Togo, Isao Tanaka, \u003ca href=\"https://arxiv.org/abs/1901.02118\"\u003eGroup-theoretical high-order rotational invariants for structural representations: Application to linearized machine learning interatomic potential\u003c/a\u003e, arXiv:1901.02118 [physics.comp-ph], 2019.",
				"\u003ca href=\"/index/Tu#2wis\"\u003eIndex entries for two-way infinite sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Rec\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,1,0,-1,0,0,-1,0,1,1,-1).",
				"\u003ca href=\"/index/Mo#Molien\"\u003eIndex entries for Molien series\u003c/a\u003e"
			],
			"formula": [
				"a(n) = round((n+3)*(2*n+9)*(n+9)/360).",
				"G.f.: 1/((1-x)*(1-x^2)*(1-x^3)*(1-x^5)).",
				"a(n) = -a(-11-n).",
				"a(n) = a(n-1) + a(n-2) - a(n-4) - a(n-7) + a(n-9) + a(n-10) - a(n-11). - _David Neil McGrath_, Sep 15 2014"
			],
			"example": [
				"There are 6 partitions of 5 into parts 1,2,3 and 5. These are (5)(32)(311)(221)(2111)(11111). - _David Neil McGrath_, Sep 15 2014"
			],
			"maple": [
				"1/((1-x)*(1-x^2)*(1-x^3)*(1-x^5)); seq(coeff(series(%, x, n+1), x, n), n = 0..60); # modified by _G. C. Greubel_, Sep 08 2019"
			],
			"mathematica": [
				"LinearRecurrence[{1,1,0,-1,0,0,-1,0,1,1,-1}, {1,1,2,3,4,6,8,10,13,16, 20}, 60] (* _Harvey P. Dale_, Feb 25 2015 *)"
			],
			"program": [
				"(PARI) a(n)=round((n+3)*(2*n+9)*(n+9)/360)",
				"(MAGMA) [Round((n+3)*(2*n+9)*(n+9)/360): n in [0..60]]; // _Vincenzo Librandi_, Jun 23 2011",
				"(Sage) [round((n+3)*(2*n+9)*(n+9)/360) for n in (0..60)] # _G. C. Greubel_, Sep 08 2019"
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"references": 4,
			"revision": 42,
			"time": "2019-09-09T01:46:16-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}