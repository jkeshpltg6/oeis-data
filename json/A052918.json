{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A052918",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 52918,
			"data": "1,5,26,135,701,3640,18901,98145,509626,2646275,13741001,71351280,370497401,1923838285,9989688826,51872282415,269351100901,1398627786920,7262490035501,37711077964425,195817879857626",
			"name": "a(0) = 1, a(1) = 5, a(n+1) = 5*a(n) + a(n-1).",
			"comment": [
				"A087130(n)^2 - 29*a(n-1)^2 = 4*(-1)^n, n \u003e= 1. - _Gary W. Adamson_, Jul 01 2003, corrected Oct 07 2008, corrected by _Jianing Song_, Feb 01 2019",
				"a(p) == 29^((p-1)/2)) (mod p), for odd primes p. - _Gary W. Adamson_, Feb 22 2009",
				"For more information about this type of recurrence, follow the Khovanova link and see A054413, A086902 and A178765. - _Johannes W. Meijer_, Jun 12 2010",
				"Binomial transform of A015523. - _Johannes W. Meijer_, Aug 01 2010",
				"For positive n, a(n) equals the permanent of the n X n tridiagonal matrix with 5's along the main diagonal and 1's along the superdiagonal and the subdiagonal. - _John M. Campbell_, Jul 08 2011",
				"a(n) equals the number of words of length n on alphabet {0,1,...,5} avoiding runs of zeros of odd lengths. - _Milan Janjic_, Jan 28 2015"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A052918/b052918.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"D. Birmajer, J. B. Gil, M. D. Weiner, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL19/Gil/gil6.html\"\u003en the Enumeration of Restricted Words over a Finite Alphabet \u003c/a\u003e, J. Int. Seq. 19 (2016) # 16.1.3, Example 8.",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=901\"\u003eEncyclopedia of Combinatorial Structures 901\u003c/a\u003e",
				"M. Janjic, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL13/Janjic/janjic33.html\"\u003eHessenberg Matrices and Integer Sequences \u003c/a\u003e, J. Int. Seq. 13 (2010) # 10.7.8, section 3.",
				"M. Janjic, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL18/Janjic/janjic63.html\"\u003eOn Linear Recurrence Equations Arising from Compositions of Positive Integers\u003c/a\u003e, Journal of Integer Sequences, Vol. 18 (2015), Article 15.4.7.",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"Kai Wang, \u003ca href=\"https://www.researchgate.net/publication/339487198_On_k-Fibonacci_Sequences_And_Infinite_Series_List_of_Results_and_Examples\"\u003eOn k-Fibonacci Sequences And Infinite Series List of Results and Examples\u003c/a\u003e, 2020.",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (5,1)."
			],
			"formula": [
				"G.f.: 1/(1 - 5*x - x^2).",
				"a(3n) = A041047(5n), a(3n+1) = A041047(5n+3), a(3n+2) = 2*A041047(5n+4). - _Henry Bottomley_, May 10 2000",
				"a(n) = Sum_{alpha=RootOf(-1+5*z+z^2)} (1/29)*(5+2*alpha)*alpha^(-1-n).",
				"a(n-1) = (((5 + sqrt(29))/2)^n - ((5 - sqrt(29))/2)^n)/sqrt(29). - _Gary W. Adamson_, Jul 01 2003",
				"a(n) = U(n, 5*i/2)*(-i)^n with i^2 = -1 and Chebyshev's U(n, x/2) = S(n, x) polynomials. See triangle A049310.",
				"Let M = {{0, 1}, {1, 5}}, then a(n) is the lower-right term of M^n. - _Roger L. Bagula_, May 29 2005",
				"a(n) = F(n, 5), the n-th Fibonacci polynomial evaluated at x = 5. - _T. D. Noe_, Jan 19 2006",
				"a(n) = denominator of n-th convergent to [1, 4, 5, 5, 5,...], for n \u003e 0. Continued fraction [1, 4, 5, 5, 5,...] = .807417596..., the inradius of a right triangle with legs 2 and 5. n-th convergent = A100237(n)/A052918(n), the first few being: 1/1, 4/5, 21/26, 109/135, 566/701,... - _Gary W. Adamson_, Dec 21 2007",
				"From _Johannes W. Meijer_, Jun 12 2010: (Start)",
				"a(2n+1) = 5*A097781(n), a(2n) = A097835(n).",
				"Lim_{k-\u003einfinity} a(n+k)/a(k) = (A087130(n) + a(n-1)*sqrt(29))/2.",
				"Lim_{n-\u003einfinity} A087130(n)/a(n-1) = sqrt(29).",
				"(End)",
				"From _L. Edson Jeffery_, Jan 07 2012: (Start)",
				"Define the 2 X 2 matrix A = {{1, 1}, {5, 4}}. Then:",
				"a(n) is the upper-left term of (1/5)*(A^(n+2) - A^(n+1));",
				"a(n) is the upper-right term of A^(n+1);",
				"a(n) is the lower-left term of (1/5)*A^(n+1);",
				"a(n) is the lower-right term of (Sum_{k=0..n} A^k). (End)",
				"Sum_{n\u003e=0} (-1)^n/(a(n)*a(n+1)) = (sqrt(29) - 5)/2. - _Vladimir Shevelev_, Feb 23 2013"
			],
			"maple": [
				"spec := [S,{S=Sequence(Union(Z,Z,Z,Z,Z,Prod(Z,Z)))},unlabeled]: seq(combstruct[count](spec,size=n), n=0..30);",
				"a[0]:=1: a[1]:=5: for n from 2 to 26 do a[n]:=5*a[n-1]+a[n-2] od: seq(a[n], n=0..30); # _Zerinvary Lajos_, Jul 26 2006",
				"with(combinat):a:=n-\u003efibonacci(n,5):seq(a(n),n=1..30); # _Zerinvary Lajos_, Dec 07 2008"
			],
			"mathematica": [
				"LinearRecurrence[{5, 1}, {1, 5}, 30] (* _Vincenzo Librandi_, Feb 23 2013 *)",
				"Table[Fibonacci[n+1, 5], {n,0,30}] (* _Vladimir Reshetnikov_, May 08 2016 *)"
			],
			"program": [
				"(Sage) [lucas_number1(n,5,-1) for n in range(1, 22)] # _Zerinvary Lajos_, Apr 24 2009",
				"(PARI) Vec(1/(1-5*x-x^2)+O(x^30)) \\\\ _Charles R Greathouse IV_, Nov 20 2011",
				"(MAGMA) I:=[1, 5]; [n le 2 select I[n] else 5*Self(n-1)+Self(n-2): n in [1..30]]; // _Vincenzo Librandi_, Feb 23 2013",
				"(GAP) a:=[1,5];; for n in [3..30] do a[n]:=5*a[n-1]+a[n-2]; od; a; # _G. C. Greubel_, Oct 16 2019",
				"(MAGMA) R\u003cx\u003e:=PowerSeriesRing(Integers(), 22); Coefficients(R!( 1/(1 - 5*x - x^2) )); // _Marius A. Burtea_, Oct 16 2019"
			],
			"xref": [
				"Row 5 of A172236 (with an offset shift).",
				"Cf. A087130, A099365 (squares), A100237, A175184 (Pisano periods), A201005 (prime subsequence).",
				"Sequences with g.f. 1/(1-k*x-x^2) or x/(1-k*x-x^2): A000045 (k=1), A000129 (k=2), A006190 (k=3), A001076 (k=4), this sequence (k=5), A005668 (k=6), A054413 (k=7), A041025 (k=8), A099371 (k=9), A041041 (k=10), A049666 (k=11), A041061 (k=12), A140455 (k=13), A041085 (k=14), A154597 (k=15), A041113 (k=16), A178765 (k=17), A041145 (k=18), A243399 (k=19), A041181 (k=20)."
			],
			"keyword": "easy,nonn",
			"offset": "0,2",
			"author": "encyclopedia(AT)pommard.inria.fr, Jan 25 2000",
			"references": 40,
			"revision": 106,
			"time": "2020-06-15T22:57:55-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}