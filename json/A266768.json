{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A266768",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 266768,
			"data": "1,0,1,0,2,1,3,1,5,2,7,3,10,5,13,7,18,10,23,13,30,18,37,23,47,30,57,37,70,47,84,57,101,70,119,84,141,101,164,119,192,141,221,164,255,192,291,221,333,255,377,291,427,333,480,377,540,427,603,480,674,540,748,603,831,674,918,748,1014,831,1115,918,1226,1014,1342,1115",
			"name": "Molien series for invariants of finite Coxeter group D_5.",
			"comment": [
				"The Molien series for the finite Coxeter group of type D_k (k \u003e= 3) has G.f. = 1/Prod_i (1-x^(1+m_i)) where the m_i are [1,3,5,...,2k-3,k-1]. If k is even only even powers of x appear, and we bisect the sequence."
			],
			"reference": [
				"J. E. Humphreys, Reflection Groups and Coxeter Groups, Cambridge, 1990. See Table 3.1, page 59."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A266768/b266768.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Mo#Molien\"\u003eIndex entries for Molien series\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_25\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,1,0,1,1,0,-1,0,-1,-2,0,0,0,0,2,1,0,1,0,-1,-1,0,-1,0,1)."
			],
			"formula": [
				"G.f.: 1/((1-x^2)*(1-x^4)*(1-x^6)*(1-x^5)*(1-x^8)).",
				"a(n) = a(n-2)+a(n-4)+a(n-5)-a(n-7)-a(n-9)-2*a(n-10)+2*a(n-15)+a(n-16)+a(n-18)-a(n-20)-a(n-21)-a(n-23)+a(n-25). - _Wesley Ivan Hurt_, May 03 2021"
			],
			"maple": [
				"seq(coeff(series(1/((1-x^2)*(1-x^4)*(1-x^6)*(1-x^5)*(1-x^8)), x, n+1), x, n), n = 0..80); # _G. C. Greubel_, Jan 31 2020"
			],
			"mathematica": [
				"CoefficientList[Series[1/((1-x^2)*(1-x^4)*(1-x^6)*(1-x^5)*(1-x^8)), {x,0,80}], x] (* _G. C. Greubel_, Jan 31 2020 *)"
			],
			"program": [
				"(PARI) my(x='x+O('x^80)); Vec(1/((1-x^2)*(1-x^4)*(1-x^6)*(1-x^5)*(1-x^8))) \\\\ _G. C. Greubel_, Jan 31 2020",
				"(MAGMA) R\u003cx\u003e:=PowerSeriesRing(Integers(), 80); Coefficients(R!( 1/((1-x^2)*(1-x^4)*(1-x^6)*(1-x^5)*(1-x^8)) )); // _G. C. Greubel_, Jan 31 2020",
				"(Sage)",
				"def A266768_list(prec):",
				"    P.\u003cx\u003e = PowerSeriesRing(ZZ, prec)",
				"    return P( 1/((1-x^2)*(1-x^4)*(1-x^6)*(1-x^5)*(1-x^8)) ).list()",
				"A266768_list(80) # _G. C. Greubel_, Jan 31 2020"
			],
			"xref": [
				"Molien series for finite Coxeter groups D_3 through D_12 are A266755, A266769, A266768, A003402, and A266770-A266775."
			],
			"keyword": "nonn",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_, Jan 10 2016",
			"references": 10,
			"revision": 13,
			"time": "2021-05-03T13:29:09-04:00",
			"created": "2016-01-10T14:21:58-05:00"
		}
	]
}