{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A348824",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 348824,
			"data": "32,48,72,96,112,126,128,144,160,168,176,192,198,221,224,240,252,256,264,288,294,304,336,342,347,352,360,368,384,392,396,414,416,432,448,456,462,480,496,504,512,528,544,545,552,558,560,576,588,599",
			"name": "Numbers in array A327259 that do not have a unique decomposition into numbers of A327261 (excluding 1).",
			"comment": [
				"While array A327259 has many properties of the multiplication table, one way the numbers that sieve out of the array fail to be prime numbers is that unique factorization does not hold. Some numbers have two or more decompositions.",
				"For i \u003e= 2, A327259(i, a(n)) is in the sequence."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A348824/b348824.txt\"\u003eTable of n, a(n) for n = 1..1256\u003c/a\u003e (all terms m \u003c= 10000)",
				"Michael De Vlieger, \u003ca href=\"/A348824/a348824.txt\"\u003eDecomposition of a(n) into A327259(k)\u003c/a\u003e"
			],
			"formula": [
				"A327259(n,k) = 2*n*k - n - k + 1, if n is odd and k is odd;",
				"A327259(n,k) = 2*n*k - n, if n is even and k is odd;",
				"A327259(n,k) = 2*n*k - k, if n is odd and k is even;",
				"A327259(n,k) = 2*n*k, if n is even and k is even.",
				"Array A327259(n,k) begins:",
				"   1   2   3   4   5   6   7   8   9  10  11  12",
				"   2   8  10  16  18  24  26  32  34  40  42  48",
				"   3  10  13  20  23  30  33  40  43  50  53  60",
				"   4  16  20  32  36  48  52  64  68  80  84  96",
				"   5  18  23  36  41  54  59  72  77  90  95 108",
				"   6  24  30  48  54  72  78  96 102 120 126 144",
				"   7  26  33  52  59  78  85 104 111 130 137 156",
				"   8  32  40  64  72  96 104 128 136 160 168 192",
				"   9  34  43  68  77 102 111 136 145 170 179 204",
				"  10  40  50  80  90 120 130 160 170 200 210 240",
				"  11  42  53  84  95 126 137 168 179 210 221 252",
				"  12  48  60  96 108 144 156 192 204 240 252 288",
				"A327261 is numbers not appearing in array A327259 with its first row and column omitted, starting with 1, 2, 3, 4, 5, 6, 7, 9, 11, 12, 14, 15, 17, 19, 21, 22, 25, 27, 28, 29, 31, 35, 37, 38, 39."
			],
			"example": [
				"48 is in the sequence because 48 = A327259(2,12) = A327259(4,6) and 2, 4, 6 and 12 are in A327261.",
				"72 is in the sequence because 72 = A327259(2,2,5) = A327259(6,6) and 2, 5 and 6 are in A327261. A327259(2,2,5) is well-defined because A327259(n,k) is associative.",
				"221 is in the sequence because 221 = A327259(5,25) = A327259(11,11) and 5, 11 and 25 are in A327261.",
				"462 is in the sequence because 462 = A327259(6,39) = A327259(11,22) = A327259(14,17) and 6, 11, 14, 17, 22 and 39 are in A327261.",
				"The first six terms and their decompositions:",
				"1 32 = A327259(2,2,2) = A327259(4,4)",
				"2 48 = A327259(2,12) = A327259(4,6)",
				"3 72 = A327259(2,2,5) = A327259(6,6)",
				"4 96 = A327259(2,2,6) = A327259(4,12)",
				"5 112 = A327259(2,28) = A327259(4,14)",
				"6 126 = A327259(5,14) = A327259(6,11)",
				"More in a-file."
			],
			"mathematica": [
				"T[n_,k_]:=2n*k-If[Mod[n,2]==1,If[Mod[k,2]==1,n+k-1,k],If[Mod[k,2]==1,n,0]];F[d_]:=If[(q=Union[Sort/@(Position[Table[T[n,k],{n,2,Ceiling[d/3]},{k,2,Ceiling[d/3]}],d]+1)])=={},{{d}},q];FC[x_]:=FixedPoint[Union[Sort/@Flatten[Flatten/@Tuples[#]\u0026/@((F/@#\u0026/@#)\u0026[#]),1]]\u0026,F[x]];list={};Do[If[Length@FC@i\u003e1,AppendTo[list,i]],{i,300}];list (* _Giorgos Kalogeropoulos_, Nov 05 2021 *)"
			],
			"xref": [
				"Cf. A327259, A327261, A340747."
			],
			"keyword": "nonn,changed",
			"offset": "1,1",
			"author": "_David Lovler_, Oct 31 2021",
			"references": 1,
			"revision": 13,
			"time": "2022-01-07T15:29:09-05:00",
			"created": "2021-12-15T11:39:52-05:00"
		}
	]
}