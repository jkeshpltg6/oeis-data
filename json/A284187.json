{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A284187",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 284187,
			"data": "838,904,1970,2176,3134,3562,4226,5038,5580,6612,6706,7096,7384,9266,9530,10742,12384,12592,12682,13098,14846,15284,15520,15578,15632,17314,17602,18310,18604,18808,19076,19216,20380,20696,20752,21182,21510,21746,22444",
			"name": "5-untouchable numbers.",
			"comment": [
				"Let sigma(n) denote the sum of divisors of n, and s(n) := sigma(n) - n, with the convention that s(0)=0. Untouchable numbers are those numbers that do not lie in the image of s(n), and they were studied extensively (see the references). In 2016, Pollack and Pomerance conjectured that the set of untouchable numbers has a natural asymptotic density.",
				"Let sk(n) denote the k-th iterate of s(n). 5-untouchable numbers are the numbers that lie in the image of s4(n), but not in the image of s5(n). Question: does the set of 5-untouchable numbers have a natural asymptotic density?"
			],
			"link": [
				"Anton Mosunov, \u003ca href=\"/A284187/b284187.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Kevin Chum, Richard K. Guy, Michael J. Jacobson Jr. and Anton S. Mosunov, \u003ca href=\"https://arxiv.org/abs/2110.14136\"\u003eNumerical and Statistical Analysis of Aliquot Sequences\u003c/a\u003e, arXiv:2110.14136 [math.NT], 2021.",
				"R. K. Guy, J. L. Selfridge, \u003ca href=\"https://doi.org/10.1090/S0025-5718-1975-0384669-X\"\u003eWhat drives an aliquot sequence?\u003c/a\u003e, Math. Comp. 29 (129), 1975, 101-107.",
				"Paul Pollack, Carl Pomerance, \u003ca href=\"https://doi.org/10.1090/btran/10\"\u003eSome problems of Erdos on the sum-of-divisors function\u003c/a\u003e, Trans. Amer. Math. Soc., Ser. B, 3 (2016), 1-26.",
				"Carl Pomerance, \u003ca href=\"https://math.dartmouth.edu/~carlp/aliquot.pdf\"\u003eThe first function and its iterates\u003c/a\u003e, A Celebration of the Work of R. L. Graham, S. Butler, J. Cooper, and G. Hurlbert, eds., Cambridge U. Press, to appear.",
				"Carl Pomerance, Hee-Sung Yang, \u003ca href=\"https://doi.org/10.1090/S0025-5718-2013-02775-5\"\u003eVariant of a theorem of Erdos on the sum-of-proper-divisors function\u003c/a\u003e, Math. Comp., 83 (2014), 1903-1913."
			],
			"example": [
				"All even numbers less than 838 have a preimage under s5(n), so they are not 5-untouchable.",
				"a(1) = 838, because 838 = s4(2588) but 2588 is untouchable. Therefore 838 is not in the image of s5(n). Note that 2588 is the only preimage of 838 under s4(n).",
				"a(2) = 904, because 904 = s4(4402) = s4(5378) but both 4402 and 5378 is untouchable.",
				"a(3) = 1970, because 1970 = s4(4312) but 4312 is untouchable."
			],
			"xref": [
				"Cf. A005114, A152454, A283152, A284147, A284156."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Anton Mosunov_, Mar 21 2017",
			"references": 4,
			"revision": 12,
			"time": "2021-10-28T02:01:20-04:00",
			"created": "2017-03-22T00:25:12-04:00"
		}
	]
}