{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A291792",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 291792,
			"data": "5,13,14,22,25,46,47,54,63,65,70,74,78,80,91,93,106,110,117,118,128,144,148,160,166,169,190,195,199,209,222,229,234,236,239,240,243,252,254,263,264,265,266,278,281,283,286,302,304,310,324,326,327,336,339",
			"name": "Numbers n such that Post's tag system started at the word (100)^n eventually dies (i.e., reaches the empty string).",
			"comment": [
				"These are the numbers such that A291793(n)=0, or equivalently A284121(n)=1.",
				"Comments from _Lars Blomberg_ on the method used in calculating the terms, Sep 14 2017: (Start)",
				"Here is an overview of the method I have been using.",
				"Build the words in a large byte array. Each iteration just adds 00 or 1101 to the end and removes 3 bytes from the beginning, without moving the whole word, just keeping track of the length of the word and where it starts within the array.",
				"When the word reaches the end of the array it is moved to the beginning.  This allows for very fast iterations, as long as the word is substantially shorter than the array.",
				"The size of the byte array is 10^9, this is the longest word we can handle.",
				"As for cycle detection, the words at iterations A: k*10^5 and B: (k+1)*10^5 are saved.",
				"For iterations above B when the current word has same length as B (a fast test), then check if the current word is equal to the one at B. If so, we have a cycle whose length can be determined simply by continue iterating.  When the current iteration reaches C: (k+2)*10^5, move B-\u003eA, C-\u003eB, and continue.",
				"The cycle has started somewhere between A and B and we know the cycle length. So restart two iterations at A and initially iterate one of them by the cycle length. Then iterate the two in parallel (being a cycle length apart) until they are equal, which gives the start of the cycle. Only the two words being iterated need to be stored.",
				"One drawback with this method is that it cannot detect a cycle longer than 10^5 (or whatever value we choose). In that case the iterations will go on forever.",
				"(End)",
				"The trajectory of the word (100)^n for n=110 dies after 43913328040672 iterations, so 110 is a term in this sequence. The longest word in the trajectory is 31299218, which appeared at iteration 14392308412264. _Lars Blomberg_, Oct 04, 2017"
			],
			"link": [
				"Lars Blomberg, \u003ca href=\"/A291792/b291792.txt\"\u003eTable of n, a(n) for n = 1..1100\u003c/a\u003e",
				"Peter R. J. Asveld, \u003ca href=\"http://doc.utwente.nl/66184/1/1988m20.pdf\"\u003eOn a Post's System of Tag\u003c/a\u003e. Bulletin of the EATCS 36 (1988), 96-102.",
				"N. J. A. Sloane, \u003ca href=\"/A291792/a291792.txt\"\u003eMaple code for A291792, A284119, A291793, A284121), A291794, A291795, A291796, A292089, A292090, A292091, A292092, A292093, A292094.\u003c/a\u003e",
				"N. J. A. Sloane, Three (No, 8) Lovely Problems from the OEIS, Experimental Mathematics Seminar, Rutgers University, Oct 05 2017, \u003ca href=\"https://vimeo.com/237029685\"\u003ePart I\u003c/a\u003e, \u003ca href=\"https://vimeo.com/237030304\"\u003ePart 2\u003c/a\u003e, \u003ca href=\"https://oeis.org/A290447/a290447_slides.pdf\"\u003eSlides.\u003c/a\u003e (Mentions this sequence)",
				"Shigeru Watanabe, \u003ca href=\"/A284116/a284116.pdf\"\u003ePeriodicity of Post's normal process of tag\u003c/a\u003e, in Jerome Fox, ed., Proceedings of Symposium on Mathematical Theory of Automata, New York, April 1962, Polytechnic Press, Polytechnic Institute of Brooklyn, 1963, pp. 83-99. [Annotated scanned copy]"
			],
			"xref": [
				"Cf. A284116, A284119, A284121, A291793.",
				"Asveld's Table 1 gives data about the behavior of Post's 3-shift tag system {00/1101} applied to the word (100)^n. The first column gives n, the nonzero values in column 2 give A291792, and columns 3 through 7 give A284119, 291793 (or A284121), A291794, A291795, A291796. For the corresponding data for Watanabe's 3-shift tag system {00/1011} applied to (100)^n see A292089, A292090, A292091, A292092, A292093, A292094."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_, Sep 04 2017",
			"ext": [
				"a(8)-a(17) from _Lars Blomberg_, Sep 08 2017",
				"a(18)-a(55) from _Lars Blomberg_, Oct 15 2017"
			],
			"references": 12,
			"revision": 75,
			"time": "2018-04-19T13:06:41-04:00",
			"created": "2017-09-04T14:26:49-04:00"
		}
	]
}