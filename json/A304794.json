{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A304794",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 304794,
			"data": "1,2,2,2,2,6,4,6,2,6,4,4,2,6,4,10,10,12,12,8,2,8,12,8,12,10,10,2,6,4,10,10,18,18,18,8,8,2,2,12,8,12,10,6,8,12,8,12,10,4,10,10,14,14,24,14,14,8,14,2,6,4,4,4,24,20,18,14,28,14,28,26,26,4,12",
			"name": "a(n) is the least possible difference between the greatest and the smallest prime in any partition of prime(n) into three primes p+q+r (p \u003c= q \u003c= r); n \u003e= 4.",
			"comment": [
				"From Goldbach's (weak) conjecture, any prime \u003e= 7 can be expressed as the sum of three primes. For n \u003e= 5 at least 2 different partitions of this kind are possible for the same prime. Many primes (\u003e90% in the above data) exhibit the property of the maximum value of the smallest prime p (=A302607(n)) occurring in the same partition as the minimum value of the greatest prime, r (=A302756(n)). The exceptions in these data are 103, 193, 229, 271, 281, ... where the greatest value of the smallest prime and the smallest value of the greatest prime appear in different partitions. In such cases a(n) is chosen from the partition with the smallest difference r-p, see examples."
			],
			"example": [
				"Prime(6) = 13 = 3+5+5 = 3+3+7, so a(6) = 5-3 = 2.",
				"Prime(27) = 103 = 31+31+41 = 29+37+37, A302607(27) = 31, and A302756(27) = 37; a(27) = 37-29 = 8.",
				"Prime(44) = 193 = 61+61+71 = 59+67+67, A302607(44) = 61, A302756(44) = 67; a(44) = 67-59 = 8."
			],
			"mathematica": [
				"Array[Min@ Map[First@ # - Last@ # \u0026, Select[IntegerPartitions[Prime@ #, {3}], AllTrue[#, PrimeQ] \u0026]] \u0026, 75, 4] (* _Michael De Vlieger_, Jul 19 2018 *)"
			],
			"program": [
				"(PARI) a(n) = {my(pn = prime(n), res = oo); forprime(p=2, pn, forprime(q=p, pn, forprime(r=q, pn, if (p+q+r == pn, res = min(res, r-p)); ); ); ); res; } \\\\ _Michel Marcus_, Jul 05 2018"
			],
			"xref": [
				"Cf. A302756, A302607."
			],
			"keyword": "nonn",
			"offset": "4,2",
			"author": "_David James Sycamore_, May 18 2018",
			"references": 0,
			"revision": 20,
			"time": "2018-07-27T16:59:09-04:00",
			"created": "2018-07-27T16:59:09-04:00"
		}
	]
}