{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A307689",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 307689,
			"data": "1,1,1,1,1,2,1,1,1,3,3,4,3,3,1,1,1,4,6,10,16,21,13,11,13,21,16,10,6,4,1,1,1,5,10,20,50,82,73,66,184,411,548,351,455,326,144,67,144,326,455,351,548,411,184,66,73,82,50,20,10,5,1,1",
			"name": "The number of rooted binary trees with minimal Colless tree balance index.",
			"comment": [
				"a(n) is the number of maximally balanced trees with n leaves according to the Colless tree balance index. It is bounded above by sequence A299037."
			],
			"link": [
				"Mareike Fischer, \u003ca href=\"/A307689/b307689.txt\"\u003eTable of n, a(n) for n = 1..512\u003c/a\u003e",
				"Tomás M. Coronado, Francesc Rosselló, \u003ca href=\"https://arxiv.org/abs/1903.11670\"\u003eThe minimum value of the Colless index\u003c/a\u003e arXiv:1903.11670 [q-bio.PE], 2019.",
				"Mareike Fischer, Lina Herbst, Kristina Wicke, \u003ca href=\"https://arxiv.org/abs/1904.09771\"\u003eExtremal properties of the Colless tree balance index for rooted binary trees\u003c/a\u003e, arXiv:1904.09771 [math.CO], 2019."
			],
			"formula": [
				"a(1)=1; a(n) = Sum_{(n_a,n_b): n_a+n_b=n, n_a \u003e n_b, (n_a,n_b) in QB(n)}}  ( a(n_a)* a(n_b)) +f(n), where f(n)=0 if n is odd} and f(n)= binomial(a(n/2)+1,2) if n is even; and where QB(n)={(n_a,n_b): n_a+n_b=n and such that there exists a tree T on n leaves with minimal Colless index and with leaf partitioning (n_a,n_b)} }."
			],
			"example": [
				"There are 13 trees with minimal Colless index and 23 leaves, i.e. a(23)=13."
			],
			"mathematica": [
				"(*QB[n] is just a support function -- the function that actually generates the sequence of the numbers of trees with minimal Colless index and n leaves is given by minCbasedonQB; see below*)",
				"QB[n_] := Module[{k, n0, bin, l, m = {}, i, length, qb = {}, j},",
				"  If[OddQ[n], k = 0,",
				"   k = FactorInteger[n][[1]][[2]];",
				"   ];",
				"  n0 = n/(2^k);",
				"  bin = IntegerDigits[n0, 2];",
				"  length = Length[bin];",
				"  For[i = Length[bin], i \u003e= 1, i--,",
				"   If[bin[[i]] != 0, PrependTo[m, length - i]];",
				"   ];",
				"  l = Length[m];",
				"  If[l == 1, Return[{{n/2, n/2}}],",
				"   AppendTo[",
				"    qb, {2^k*(Sum[2^(m[[i]] - 1), {i, 1, l - 1}] + 1),",
				"     2^k*(Sum[2^(m[[i]] - 1), {i, 1, l - 1}])}];",
				"   For[j = 2, j \u003c= l - 1, j++,",
				"    If[m[[j]] \u003e m[[j + 1]] + 1,",
				"     AppendTo[",
				"      qb, {2^k*(Sum[2^(m[[i]] - 1), {i, 1, j - 1}] + 2^m[[j]]),",
				"       n - 2^k*(Sum[2^(m[[i]] - 1), {i, 1, j - 1}] + 2^m[[j]])}]];",
				"    If[m[[j]] \u003c m[[j - 1]] - 1,",
				"     AppendTo[",
				"      qb, {n - 2^k*Sum[2^(m[[i]] - 1), {i, 1, j - 1}],",
				"       2^k*Sum[2^(m[[i]] - 1), {i, 1, j - 1}]}]];",
				"    ];",
				"   If[k \u003e= 1, AppendTo[qb, {n/2, n/2}]];",
				"   Return[qb];",
				"   ];",
				"  ]",
				"minCbasedonQB[n_] := Module[{qb = QB[n], min = 0, i, na, nb},",
				"  If[n == 1, Return[1],",
				"   For[i = 1, i \u003c= Length[qb], i++,",
				"    na = qb[[i]][[1]]; nb = qb[[i]][[2]];",
				"    If[na != nb, min = min + minCbasedonQB[na]*minCbasedonQB[nb],",
				"     min = min + Binomial[minCbasedonQB[n/2] + 1, 2]];",
				"    ];",
				"   Return[min];",
				"   ]",
				"  ]"
			],
			"xref": [
				"The sequence is bounded above by A299037.",
				"The sequence of the number of trees with minimal Colless index is also linked to the values of the minimal Colless index as given by A296062."
			],
			"keyword": "nonn",
			"offset": "1,6",
			"author": "_Mareike Fischer_, Apr 22 2019",
			"references": 1,
			"revision": 17,
			"time": "2019-05-06T06:15:13-04:00",
			"created": "2019-05-05T11:45:26-04:00"
		}
	]
}