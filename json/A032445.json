{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A032445",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 32445,
			"data": "33,2,7,1,3,5,8,14,12,6,50,95,149,111,2,4,41,96,425,38,54,94,136,17,293,90,7,29,34,187,65,1,16,25,87,10,286,47,18,44,71,3,93,24,60,61,20,120,88,58,32,49,173,9,192,131,211,405,11,5,128,220,21,313,23,8,118,99,606",
			"name": "Number the digits of the decimal expansion of Pi: 3 is the first, 1 is the second, 4 is the third and so on; a(n) gives the starting position of the first occurrence of n.",
			"comment": [
				"See A176341 for a variant counting positions starting with 0, and A232013 for a sequence based on iterations of A176341. - _M. F. Hasler_, Nov 16 2013"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A032445/b032445.txt\"\u003eTable of n, a(n) for n=0..9999\u003c/a\u003e",
				"M. J. Halm, \u003ca href=\"http://michaelhalm.tripod.com/id171.htm\"\u003eMore Sequences\u003c/a\u003e, Mpossibilities 83, April 2003.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ConstantDigitScanning.html\"\u003eConstant Digit Scanning\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PiDigits.html\"\u003ePi Digits\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A176341(n)+1. - _M. F. Hasler_, Nov 16 2013"
			],
			"example": [
				"a(10) = 50 because the first \"10\" in the decimal expansion of Pi occurs at digits 50 and 51: 31415926535897932384626433832795028841971693993751058209749445923..."
			],
			"mathematica": [
				"p = ToString[FromDigits[RealDigits[N[Pi, 10^4]][[1]]]]; Do[Print[StringPosition[p, ToString[n]][[1]][[1]]], {n, 1, 100}]",
				"With[{pi=RealDigits[Pi,10,1000][[1]]},Transpose[Flatten[Table[ SequencePosition[ pi,IntegerDigits[n],1],{n,0,70}],1]][[1]]] (* The program uses the SequencePosition function from Mathematica version 10 *) (* _Harvey P. Dale_, Dec 01 2015 *)"
			],
			"program": [
				"(PARI) A032445(n)=my(L=#Str(n)); n=Mod(n, 10^L); for(k=L-1, 9e9, Pi\\.1^k-n||return(k+2-L)) \\\\ Make sure to use sufficient realprecision, e.g. via \\p999. - _M. F. Hasler_, Nov 16 2013"
			],
			"xref": [
				"Cf. A000796 (decimal expansion of Pi).",
				"Cf. A080597 (terms from the decimal expansion of Pi which include every combination of n digits as consecutive subsequences).",
				"Cf. A032510 (last string seen when scanning the decimal expansion of Pi until all n-digit strings have been seen).",
				"Cf. A064467 (primes in Pi)."
			],
			"keyword": "nonn,base,easy,nice",
			"offset": "0,1",
			"author": "_Jeff Burch_, Paul Simon (paulsimn(AT)microtec.net)",
			"ext": [
				"More terms from _Simon Plouffe_. Corrected by Michael Esposito and Michelle Vella (michael_esposito(AT)oz.sas.com).",
				"More terms from _Robert G. Wilson v_, Oct 04 2001"
			],
			"references": 20,
			"revision": 26,
			"time": "2017-04-17T13:10:59-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}