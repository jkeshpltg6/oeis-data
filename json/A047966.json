{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A047966",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 47966,
			"data": "1,2,3,4,4,8,6,10,11,15,13,25,19,29,33,42,39,62,55,81,84,103,105,153,146,185,203,253,257,344,341,432,463,552,594,747,761,920,1003,1200,1261,1537,1611,1921,2089,2410,2591,3095,3270,3815,4138,4769,5121,5972,6394,7367,7974,9066,9793,11305,12077,13736,14940",
			"name": "a(n) = Sum_{ d divides n } q(d), where q(d) = A000009 = number of partitions of d into distinct parts.",
			"comment": [
				"Number of partitions of n such that every part occurs with the same multiplicity. - _Vladeta Jovovic_, Oct 22 2004",
				"Christopher and Christober call such partitions uniform. - _Gus Wiseman_, Apr 16 2018",
				"Equals inverse Mobius transform (A051731) * A000009, where the latter begins (1, 1, 2, 2, 3, 4, 5, 6, 8, ...). -  _Gary W. Adamson_, Jun 08 2009"
			],
			"link": [
				"T. D. Noe and Alois P. Heinz, \u003ca href=\"/A047966/b047966.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (first 1000 terms from T. D. Noe)",
				"A. David Christopher and M. Davamani Christober, \u003ca href=\"http://emis.impa.br/EMIS/journals/GMN/yahoo_site_admin/assets/docs/1_GMN-2492-V13N2.77213831.pdf\"\u003eRelatively Prime Uniform Partitions\u003c/a\u003e, Gen. Math. Notes, Vol. 13, No. 2, December, 2012, pp.1-12."
			],
			"formula": [
				"G.f.: Sum_{k\u003e0} (-1+Product_{i\u003e0} (1+z^(k*i))). - _Vladeta Jovovic_, Jun 22 2003",
				"G.f.: Sum_{k\u003e=1} q(k)*x^k/(1 - x^k), where q() = A000009. - _Ilya Gutkovskiy_, Jun 20 2018",
				"a(n) ~ exp(Pi*sqrt(n/3)) / (4*3^(1/4)*n^(3/4)). - _Vaclav Kotesovec_, Aug 27 2018"
			],
			"example": [
				"The a(6) = 8 uniform partitions are (6), (51), (42), (33), (321), (222), (2211), (111111). - _Gus Wiseman_, Apr 16 2018"
			],
			"maple": [
				"with(numtheory):",
				"b:= proc(n) option remember; `if`(n=0, 1, add(add(",
				"     `if`(d::odd, d, 0), d=divisors(j))*b(n-j), j=1..n)/n)",
				"    end:",
				"a:= n-\u003e add(b(d), d=divisors(n)):",
				"seq(a(n), n=1..100);  # _Alois P. Heinz_, Jul 11 2016"
			],
			"mathematica": [
				"b[n_] := b[n] = If[n==0, 1, Sum[DivisorSum[j, If[OddQ[#], #, 0]\u0026]*b[n-j], {j, 1, n}]/n]; a[n_] := DivisorSum[n, b]; Table[a[n], {n, 1, 100}] (* _Jean-François Alcover_, Dec 06 2016 after _Alois P. Heinz_ *)",
				"Table[DivisorSum[n,PartitionsQ],{n,20}] (* _Gus Wiseman_, Apr 16 2018 *)"
			],
			"program": [
				"(PARI)",
				"N = 66; q='q+O('q^N);",
				"D(q)=eta(q^2)/eta(q); \\\\ A000009",
				"Vec( sum(e=1,N,D(q^e)-1) ) \\\\ _Joerg Arndt_, Mar 27 2014"
			],
			"xref": [
				"Cf. A000009, A000837, A024994, A047968, A063834, A279788, A289501, A300383, A301462, A302698."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"references": 81,
			"revision": 34,
			"time": "2018-08-27T02:40:06-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}