{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A337321",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 337321,
			"data": "0,1,2,10,3,7,9,10,8,10,4,5,6,8,7,9,8,9,9,9,5,6,7,8,8,10,7,10,9,10,5,6,7,7,8,9,6,9,10,8,7,8,7,9,8,9,8,9,8,10,6,7,8,8,9,10,7,10,9,11,8,9,9,9,10,8,8,10,9,9,8,7,6,10,7,10,9,10,7,9,9,9",
			"name": "a(n) is the least number of steps required to reach 1 starting from n under substring substitutions of the form k \u003c-\u003e prime(k) (where prime(k) denotes the k-th prime number).",
			"comment": [
				"This sequence is a variant of \"Choix de Bruxelles\" (where we consider substring substitutions of the form k \u003c-\u003e 2*k, see A323286):",
				"- we map a positive number n to any number that can be obtained as follows:",
				"- take a nonempty substring s (without leading zero) in the decimal representation of n,",
				"- if the value of s corresponds to a prime number, say the k-th prime number, then replace s by k or by prime(s),",
				"- otherwise replace s by prime(s).",
				"For example, the number 17 can be mapped to any of those values:",
				"- 27 (by replacing the leading 1 by prime(1) = 2),",
				"- 14 (by replacing the trailing 7 = prime(4) by 4),",
				"- 117 (by replacing the trailing 7 by prime(7) = 17),",
				"- 7 (by replacing 17 = prime(7) by 7),",
				"- 59 (by replacing 17 by prime(17) = 59).",
				"This sequence is well defined:",
				"- the sequence is well defined for any number \u003c= 11 by considering the following (minimal) paths:",
				"      1",
				"      2 -\u003e  1",
				"      3 -\u003e  2 -\u003e  1",
				"      4 -\u003e  7 -\u003e 17 -\u003e  27 -\u003e  37 -\u003e 12 -\u003e 11 -\u003e 5 -\u003e 3 -\u003e 2 -\u003e 1",
				"      5 -\u003e  3 -\u003e  2 -\u003e   1",
				"      6 -\u003e 13 -\u003e 12 -\u003e  11 -\u003e   5 -\u003e  3 -\u003e  2 -\u003e 1",
				"      7 -\u003e 17 -\u003e 27 -\u003e  37 -\u003e  12 -\u003e 11 -\u003e  5 -\u003e 3 -\u003e 2 -\u003e 1",
				"      8 -\u003e 19 -\u003e 67 -\u003e 137 -\u003e 127 -\u003e 31 -\u003e 11 -\u003e 5 -\u003e 3 -\u003e 2 -\u003e 1",
				"      9 -\u003e 23 -\u003e 13 -\u003e  12 -\u003e  11 -\u003e  5 -\u003e  3 -\u003e 2 -\u003e 1",
				"     10 -\u003e 20 -\u003e 71 -\u003e  41 -\u003e  13 -\u003e 12 -\u003e 11 -\u003e 5 -\u003e 3 -\u003e 2 -\u003e 1",
				"     11 -\u003e  5 -\u003e  3 -\u003e   2 -\u003e   1",
				"- so for any number n:",
				"     - we can transform any of its nonzero digit \u003e 1 into a digit 1,",
				"     - once we have a number with only 1's and 0's:",
				"         - while this number is \u003e 1, it either starts with \"10\" or with \"11\",",
				"           and we can transform this prefix into a \"1\",",
				"         - and eventually we will reach 1."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A337321/a337321.gp.txt\"\u003ePARI program for A337321\u003c/a\u003e"
			],
			"formula": [
				"a(prime(n)) \u003c= 1 + a(n)."
			],
			"program": [
				"(PARI) See Links section."
			],
			"xref": [
				"Cf. A323286, A323454."
			],
			"keyword": "nonn,base",
			"offset": "1,3",
			"author": "_Rémy Sigrist_, Aug 23 2020",
			"references": 4,
			"revision": 8,
			"time": "2020-08-24T02:03:42-04:00",
			"created": "2020-08-24T02:03:42-04:00"
		}
	]
}