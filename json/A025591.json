{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A025591",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 25591,
			"data": "1,1,1,2,2,3,5,8,14,23,40,70,124,221,397,722,1314,2410,4441,8220,15272,28460,53222,99820,187692,353743,668273,1265204,2399784,4559828,8679280,16547220,31592878,60400688,115633260,221653776,425363952,817175698",
			"name": "Maximal coefficient of Product_{k\u003c=n} (1 + x^k). Number of solutions to +- 1 +- 2 +- 3 +- ... +- n = 0 or 1.",
			"comment": [
				"If k is allowed to approach infinity, this gives the partition numbers A000009.",
				"a(n) is the maximal number of subsets of {1,2,...,n} that share the same sum."
			],
			"link": [
				"T. D. Noe, Alois P. Heinz and Ray Chandler, \u003ca href=\"/A025591/b025591.txt\"\u003eTable of n, a(n) for n = 0..3339\u003c/a\u003e (terms \u003c 10^1000, first 201 terms from T. D. Noe, next 200 terms from Alois P. Heinz)",
				"Dorin Andrica and Ioan Tomescu, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL5/Tomescu/tomescu4.html\"\u003eOn an Integer Sequence Related to a Product of Trigonometric Functions, and Its Combinatorial Relevance \u003c/a\u003e, Journal of Integer Sequences, Vol. 5 (2002), Article 02.2.4.",
				"Vlad-Florin Dragoi, Valeriu Beiu, \u003ca href=\"https://arxiv.org/abs/1911.01153\"\u003eFast Reliability Ranking of Matchstick Minimal Networks\u003c/a\u003e, arXiv:1911.01153 [cs.DM], 2019.",
				"Steven R. Finch, \u003ca href=\"/A000980/a000980.pdf\"\u003eSignum equations and extremal coefficients\u003c/a\u003e, February 7, 2009. [Cached copy, with permission of the author]",
				"E. Friedman and M. Keith, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL3/KEITH/carpet.html\"\u003eMagic Carpets\u003c/a\u003e, J. Int Sequences, 3 (2000), Article 00.2.5.",
				"Marco Mondelli, SH Hassani, R Urbanke, \u003ca href=\"http://arxiv.org/abs/1612.05295\"\u003eConstruction of Polar Codes with Sublinear Complexity\u003c/a\u003e, arXiv preprint arXiv:1612.05295 [cs.IT], 2016-2017. See Sect. I.",
				"Robert A. Proctor, \u003ca href=\"http://www.jstor.org/stable/2975833\"\u003eSolution of two difficult combinatorial problems with linear algebra\u003c/a\u003e, American Mathematical Monthly 89, 721-734.",
				"B. D. Sullivan, \u003ca href=\"http://cs.uwaterloo.ca/journals/JIS/VOL16/Sullivan/sullivan8.html\"\u003eOn a conjecture of Adrica and Tomescu\u003c/a\u003e, J. Int. Sequences 16 (2013), Article 13.3.1."
			],
			"formula": [
				"a(n) = A063865(n) + A063866(n).",
				"a(n) ~ sqrt(6/Pi) * 2^n / n^(3/2) [conjectured by Andrica and Tomescu (2002) and proved by Sullivan (2013)]. - _Vaclav Kotesovec_, Mar 17 2020"
			],
			"maple": [
				"b:= proc(n, i) option remember; `if`(n\u003ei*(i+1)/2, 0,",
				"      `if`(i=0, 1, b(n+i, i-1)+b(abs(n-i), i-1)))",
				"    end:",
				"a:=n-\u003e b(0, n)+b(1, n):",
				"seq(a(n), n=0..40);  # _Alois P. Heinz_, Mar 10 2014"
			],
			"mathematica": [
				"f[n_, s_] := f[n, s]=Which[n==0, If[s==0, 1, 0], Abs[s]\u003e(n*(n+1))/2, 0, True, f[n-1, s-n]+f[n-1, s+n]]; Table[Which[Mod[n, 4]==0||Mod[n, 4]==3, f[n, 0], Mod[n, 4]==1||Mod[n, 4]==2, f[n, 1]], {n, 0, 40}]",
				"{* Second program: *)",
				"p = 1; Flatten[{1, Table[p = Expand[p*(1 + x^n)]; Max[CoefficientList[p, x]], {n, 1, 50}]}] (* _Vaclav Kotesovec_, May 04 2018 *)",
				"b[n_, i_] := b[n, i] = If[n \u003e i(i+1)/2, 0, If[i == 0, 1, b[n+i, i-1] + b[Abs[n-i], i-1]]];",
				"a[n_] := b[0, n] + b[1, n]; a /@ Range[0, 40] (* _Jean-François Alcover_, Feb 17 2020, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c0,0,polcoeff(prod(k=1,n,1+x^k),n*(n+1)\\4))"
			],
			"xref": [
				"Cf. A039828, A063865, A069918, A063866, A063867, A083309, A083527, A086376."
			],
			"keyword": "nonn,nice",
			"offset": "0,4",
			"author": "_David W. Wilson_",
			"references": 29,
			"revision": 62,
			"time": "2021-01-04T06:18:48-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}