{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A192917",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 192917,
			"data": "0,5,6,22,51,140,360,949,2478,6494,16995,44500,116496,304997,798486,2090470,5472915,14328284,37511928,98207509,257110590,673124270,1762262211,4613662372,12078724896,31622512325,82788812070,216743923894,567442959603,1485584954924",
			"name": "Coefficient of x in the reduction by (x^2 -\u003e x+1) of the polynomial C(n)*x^n, where C=A022095.",
			"comment": [
				"See A192872."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A192917/b192917.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,2,-1)."
			],
			"formula": [
				"a(n) = 2*a(n-1) + 2*a(n-2) - a(n-3).",
				"G.f.: x*(5 -4*x)/((1+x)*(1-3*x+x^2)). - _R. J. Mathar_, May 08 2014",
				"a(n) = -3*a(n-1) +a(n-2) = 9*(-1)^(n+1). - _R. J. Mathar_, May 08 2014",
				"a(n) = (2^(-1-n)*(-9*(-1)^n*2^(1+n)-(3-sqrt(5))^n*(-9+sqrt(5))+(3+sqrt(5))^n*(9+sqrt(5))))/5. - _Colin Barker_, Oct 01 2016",
				"a(n) = Fibonacci(2*n+1) + 2*Fibonacci(n)^2 - (-1)^n. - _G. C. Greubel_, Jul 29 2019"
			],
			"mathematica": [
				"(* First program *)",
				"q = x^2; s = x + 1; z = 28;",
				"p[0, x_]:= 1; p[1, x_]:= 5 x;",
				"p[n_, x_]:= p[n-1, x]*x + p[n-2, x]*x^2;",
				"Table[Expand[p[n, x]], {n, 0, 7}]",
				"reduce[{p1_, q_, s_, x_}]:= FixedPoint[(s PolynomialQuotient @@ #1 + PolynomialRemainder @@ #1 \u0026)[{#1, q, x}] \u0026, p1]",
				"t = Table[reduce[{p[n, x], q, s, x}], {n, 0, z}];",
				"u1 = Table[Coefficient[Part[t, n], x, 0], {n, 1, z}] (* A192914 *)",
				"u2 = Table[Coefficient[Part[t, n], x, 1], {n, 1, z}] (* see A192878 *)",
				"(* Second program *)",
				"With[{F=Fibonacci}, Table[F[2*n+1] +2*F[n]^2 -(-1)^n, {n,0,30}]] (* _G. C. Greubel_, Jul 28 2019 *)"
			],
			"program": [
				"(PARI) a(n) = round((2^(-1-n)*(-9*(-1)^n*2^(1+n)-(3-sqrt(5))^n*(-9+sqrt(5))+(3+sqrt(5))^n*(9+sqrt(5))))/5) \\\\ _Colin Barker_, Oct 01 2016",
				"(PARI) concat(0, Vec((-x*(-5+4*x))/((1+x)*(x^2-3*x+1)) + O(x^40))) \\\\ _Colin Barker_, Oct 01 2016",
				"(PARI) vector(30, n, n--; f=fibonacci; f(2*n+1) +2*f(n)^2 -(-1)^n) \\\\ _G. C. Greubel_, Jul 29 2019",
				"(MAGMA) F:=Fibonacci; [F(2*n+1) +2*F(n)^2 -(-1)^n: n in [0..30]]; // _G. C. Greubel_, Jul 29 2019",
				"(Sage) f=fibonacci; [f(2*n+1) +2*f(n)^2 -(-1)^n for n in (0..30)] # _G. C. Greubel_, Jul 29 2019",
				"(GAP) F:=Fibonacci;; List([0..30], n-\u003e F(2*n+1) +2*F(n)^2 -(-1)^n); # _G. C. Greubel_, Jul 29 2019"
			],
			"xref": [
				"Cf. A000045, A192232, A192744, A192872, A192916."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Clark Kimberling_, Jul 12 2011",
			"references": 2,
			"revision": 15,
			"time": "2019-07-29T13:51:36-04:00",
			"created": "2011-07-13T12:20:37-04:00"
		}
	]
}