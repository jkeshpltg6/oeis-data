{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A102587",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 102587,
			"data": "1,-1,1,-1,-2,1,2,0,-3,1,-1,4,2,-4,1,-1,-5,5,5,-5,1,2,0,-12,4,9,-6,1,-1,7,7,-21,0,14,-7,1,-1,-8,12,24,-30,-8,20,-8,1,2,0,-27,9,54,-36,-21,27,-9,1,-1,10,15,-60,-15,98,-35,-40,35,-10,1,-1,-11,22,66,-99,-77,154,-22,-66,44,-11,1,2,0,-48,16,180,-120,-196,216,9",
			"name": "T(n, k) = (-1)^n*2*[x^k] ChebyshevT(n, (1 - x)/2) with T(0,0) = 1, for 0 \u003c= k \u003c= n, triangle read by rows.",
			"comment": [
				"Previous name: Triangular matrix, read by rows, equal to the matrix inverse of triangle A094531, which is the right-hand side of trinomial table A027907.",
				"Riordan array ((1-x^2)/(1+x+x^2),x/(1+x+x^2)). - _Paul Barry_, Jul 14 2005",
				"Inverse of A094531. Rows sums are 1,0,-2,0,2,0,-2,... with g.f. (1-x^2)/(1+x^2). Diagonal sums are (-1)^n*C(1,n) with g.f. 1-x. - _Paul Barry_, Jul 14 2005",
				"Row sums form the period 4 sequence: {1, 0,-2,0,2, 0,-2,0,2, ...}. Absolute row sums form A102588.",
				"Sum_{k=0..n} T(n,k)^2 = 2*A002426(n) for n\u003e0."
			],
			"link": [
				"P. Peart and W.-J. Woan, \u003ca href=\"http://dx.doi.org/10.1016/S0166-218X(99)00166-3\"\u003eA divisibility property for a subgroup of Riordan matrices\u003c/a\u003e, Discrete Applied Mathematics, Vol. 98, Issue 3, Jan 2000, 255-263."
			],
			"formula": [
				"T(n,k) = T(n-1,k-1) - T(n-1,k) - T(n-2,k), T(0,0) = T(1,1) = T(2,2) = 1, T(1,0) = T(2,0) = -1, T(2,1) = -2, T(n,k) = 0 if k\u003c0 or if k\u003en. - _Philippe Deléham_, Jan 22 2014",
				"From _Peter Bala_, Jun 29 2015: (Start)",
				"Riordan array has the form ( x*h'(x)/h(x), h(x) ) with h(x) = x/(1 + x + x^2) and so belongs to the hitting time subgroup H of the Riordan group (see Peart and Woan).",
				"T(n,k) = [x^(n-k)] f(x)^n with f(x) = ( 1 - x + sqrt(1 - 2*x - 3*x^2) )/2. In general the (n,k)th entry of the hitting time array ( x*h'(x)/h(x), h(x) ) has the form [x^(n-k)] f(x)^n, where f(x) = x/( series reversion of h(x) ). (End)"
			],
			"example": [
				"Rows begin:",
				"[1],",
				"[ -1,1],",
				"[ -1,-2,1],",
				"[2,0,-3,1],",
				"[ -1,4,2,-4,1],",
				"[ -1,-5,5,5,-5,1],",
				"[2,0,-12,4,9,-6,1],",
				"[ -1,7,7,-21,0,14,-7,1],",
				"[ -1,-8,12,24,-30,-8,20,-8,1],",
				"[2,0,-27,9,54,-36,-21,27,-9,1],",
				"[ -1,10,15,-60,-15,98,-35,-40,35,-10,1],",
				"[ -1,-11,22,66,-99,-77,154,-22,-66,44,-11,1],..."
			],
			"mathematica": [
				"Table[If[n==0, 1, CoefficientList[(-1)^n 2 ChebyshevT[n, (1-x)/2], x]], {n, 0, 9}] // Flatten (* _Peter Luschny_, Mar 07 2018 *)"
			],
			"program": [
				"(PARI) {T(n,k)=local(A); A=matrix(n+1,n+1,r,c,if(r\u003cc-1,0,polcoeff((1+x+x^2)^(r-1),r+c-2))); return((A^-1)[n+1,k+1])}",
				"(PARI) tabl(nn) = {my(m = matrix(nn, nn, n, k, n--; k--; sum(j=0, n, binomial(n,j)*binomial(j,n-k-j)))^(-1)); for (n=1, nn, for (k=1, n, print1(m[n, k], \", \");); print(););} \\\\ _Michel Marcus_, Jun 30 2015"
			],
			"xref": [
				"Cf. A094531 (matrix inverse), A102588, A002426."
			],
			"keyword": "sign,tabl",
			"offset": "0,5",
			"author": "_Paul D. Hanna_, Jan 22 2005",
			"ext": [
				"New name by _Peter Luschny_, Mar 07 2018"
			],
			"references": 7,
			"revision": 23,
			"time": "2018-03-07T08:19:39-05:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}