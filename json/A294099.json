{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A294099",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 294099,
			"data": "1,1,2,1,3,1,1,4,5,-1,1,5,11,7,-2,1,6,19,29,9,-1,1,7,29,71,76,11,1,1,8,41,139,265,199,13,2,1,9,55,239,666,989,521,15,1,1,10,71,377,1393,3191,3691,1364,17,-1,1,11,89,559,2584,8119,15289,13775,3571,19,-2",
			"name": "Rectangular array read by (upward) antidiagonals: A(n,k) = Sum_{j=0..k} (-1)^floor(j/2)*binomial(k-floor((j+1)/2), floor(j/2))*n^(k-j), n \u003e= 1, k \u003e= 0.",
			"comment": [
				"This array is used to compute A269254: A269254(n) = least k such that A(n,k) is a prime, or -1 if no such k exists.",
				"For detailed theory, see [Hone]. - _L. Edson Jeffery_, Feb 09 2018"
			],
			"link": [
				"Andrew N. W. Hone, et al., \u003ca href=\"https://arxiv.org/abs/1802.01793\"\u003eOn a family of sequences related to Chebyshev polynomials\u003c/a\u003e, arXiv:1802.01793 [math.NT], 2018."
			],
			"formula": [
				"A(n,0) = 1, A(n,1) = n + 1, A(n,k) = n*A(n,k-1) - A(n,k-2), n \u003e= 1, k \u003e= 2.",
				"G.f. for row n: (1 + x)/(1 - n*x + x^2), n \u003e= 1."
			],
			"example": [
				"Array begins:",
				"1   2    1    -1     -2      -1        1         2          1          -1",
				"1   3    5     7      9      11       13        15         17          19",
				"1   4   11    29     76     199      521      1364       3571        9349",
				"1   5   19    71    265     989     3691     13775      51409      191861",
				"1   6   29   139    666    3191    15289     73254     350981     1681651",
				"1   7   41   239   1393    8119    47321    275807    1607521     9369319",
				"1   8   55   377   2584   17711   121393    832040    5702887    39088169",
				"1   9   71   559   4401   34649   272791   2147679   16908641   133121449",
				"1  10   89   791   7030   62479   555281   4935050   43860169   389806471",
				"1  11  109  1079  10681  105731  1046629  10360559  102558961  1015229051"
			],
			"mathematica": [
				"(* Array: *)",
				"Grid[Table[LinearRecurrence[{n, -1}, {1, 1 + n}, 10], {n, 10}]]",
				"(* Array antidiagonals flattened (gives this sequence): *)",
				"A294099[n_, k_] := Sum[(-1)^(Floor[j/2]) Binomial[k - Floor[(j + 1)/2], Floor[j/2]] n^(k - j), {j, 0, k}]; Flatten[Table[A294099[n - k, k], {n, 11}, {k, 0, n - 1}]]"
			],
			"xref": [
				"Cf. A285992, A299107, A299109, A088165, A117522, A299100, A299101, A113501, A269253, A269254, A294099, A298675, A298677, A298878, A299045, A299071.",
				"Rows: A057079, A005408, A002878, A001834, A030221, A002315, A033890, A057080, A057081, A054320, ...",
				"Columns: A000012, A000027, A028387, ..."
			],
			"keyword": "sign,tabl",
			"offset": "1,3",
			"author": "_L. Edson Jeffery_, _Bob Selcoe_ and _Andrew Hone_, Oct 22 2017",
			"references": 16,
			"revision": 32,
			"time": "2018-02-27T02:59:41-05:00",
			"created": "2017-11-02T15:24:46-04:00"
		}
	]
}