{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A213343",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 213343,
			"data": "1,4,12,3,32,24,80,120,10,192,480,120,448,1680,840,35,1024,5376,4480,560,2304,16128,20160,5040,126,5120,46080,80640,33600,2520,11264,126720,295680,184800,27720,462,24576,337920,1013760,887040,221760,11088",
			"name": "1-quantum transitions in systems of N spin 1/2 particles, in columns by combination indices. Triangle read by rows, T(n, k) for n \u003e= 1 and 0 \u003c= k \u003c= floor((n-1)/2).",
			"comment": [
				"[General discussion]: Consider the 2^N numbers with N-digit binary expansion. Let a pair (v,w), here called a \"transition\", be such that there are exactly k+q digits which are '0' in v and '1' in w, and exactly k digits which are '1' in v and '0' in w. Then T(q;N,k) is the number of all such pairs.",
				"For given N and q, the rows of the triangle T(q;N,k) sum up to Sum[k]T(q;N,k) = C(2N,N-q) which is the total number of q-quantum transitions or, equivalently, the number of pairs in which the sum of binary digits of w exceeds that of v by exactly q (see Crossrefs).",
				"The terminology stems from the mapping of the i-th digit onto quantum states of the i-th particle (-1/2 for digit '0', +1/2 for digit '1'), the numbers onto quantum states of the system, and the pairs onto quantum transitions between states. In magnetic resonance (NMR) the most intense transitions are the single-quantum ones (q=1) with k=0, called \"main transitions\", while those with k\u003e0, called \"combination transitions\", tend to be weaker. Zero-, double- and, in general, q-quantum transitions are detectable by special techniques.",
				"[Specific case]: This sequence is for single-quantum transitions (q = 1). It lists the flattened triangle T(1;N,k), with rows N = 1,2,... and columns k = 0..floor((N-1)/2)."
			],
			"reference": [
				"R. R. Ernst, G. Bodenhausen, A. Wokaun, Principles of nuclear magnetic resonance in one and two dimensions, Clarendon Press, 1987, Chapters 2-6.",
				"M. H. Levitt, Spin Dynamics, J.Wiley \u0026 Sons, 2nd Ed.2007, Part3 (Section 6).",
				"J. A. Pople, W. G. Schneider, H. J. Bernstein, High-resolution Nuclear Magnetic Resonance, McGraw-Hill, 1959, Chapter 6."
			],
			"link": [
				"Stanislav Sykora, \u003ca href=\"/A213343/b213343.txt\"\u003eTable of n, a(n) for n = 1..2550\u003c/a\u003e",
				"Stanislav Sykora, \u003ca href=\"/A213343/a213343_1.txt\"\u003eT(1;N,k) with rows N=1,..,100 and columns k=0,..,floor((N-1)/2)\u003c/a\u003e",
				"S. Sykora, \u003ca href=\"http://dx.doi.org/10.3247/sl2math07.005\"\u003ep-Quantum Transitions and a Combinatorial Identity\u003c/a\u003e, Stan's Library, II, Aug 2007.",
				"Stanislav Sýkora, \u003ca href=\"http://www.ebyte.it/stan/blog12to14.html#14Dec31\"\u003eMagnetic Resonance on OEIS\u003c/a\u003e, Stan's NMR Blog (Dec 31, 2014), Retrieved Nov 12, 2019."
			],
			"formula": [
				"Set q = 1 in: T(q;N,k) = 2^(N-q-2*k)*binomial(N,k)*binomial(N-k,q+k).",
				"T(n, k) = n! * [y^(n-2*k-1)] [x^n] exp(2*x*y)*BesselI(1, 2*x). - _Peter Luschny_, May 12 2021"
			],
			"example": [
				"T(1;3,1) = 3 because the only transitions compatible with q=1,k=1 are (001,110),(010,101),(100,011).",
				"Starting rows of the triangle T(1;N,k):",
				"  N | k = 0, 1, ..., floor((N-1)/2)",
				"  1 |  1",
				"  2 |  4",
				"  3 | 12   3",
				"  4 | 32  24",
				"  5 | 80 120 10"
			],
			"maple": [
				"egf := exp(2*x*y) * BesselI(1, 2*x):",
				"ser := series(egf, x, 32): cx := n -\u003e coeff(ser, x, n):",
				"Trow := n -\u003e n!*seq(coeff(cx(n), y, n - 2*k - 1), k = 0..floor((n-1)/2)):",
				"seq(print([n], Trow(n)), n = 1..12); # _Peter Luschny_, May 12 2021"
			],
			"mathematica": [
				"With[{q = 1}, Table[2^(n - q - 2 k)*Binomial[n, k] Binomial[n - k, q + k], {n, 11}, {k, 0, Floor[(n - 1)/2]}]] // Flatten (* _Michael De Vlieger_, Nov 18 2019 *)"
			],
			"program": [
				"(PARI)",
				"TNQK(N, q, k)={binomial(N, k)*binomial(N-k, q+k)*2^((N-k)-(q+k))}",
				"TQ(Nmax, q)={vector(Nmax-q+1, n, vector(1+(n-1)\\2, k, TNQK(n+q-1, q, k-1)))}",
				"{ concat(TQ(13, 1)) } \\\\ simplified by _Andrew Howroyd_, May 12 2021"
			],
			"xref": [
				"Cf. A051288 (q=0), A213344..A213352 (q=2..10).",
				"Cf. A001787 (first column), A001791 (row sums)."
			],
			"keyword": "nonn,tabf",
			"offset": "1,2",
			"author": "_Stanislav Sykora_, Jun 09 2012",
			"references": 10,
			"revision": 51,
			"time": "2021-05-12T10:40:09-04:00",
			"created": "2012-06-14T16:35:37-04:00"
		}
	]
}