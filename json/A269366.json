{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A269366",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 269366,
			"data": "1,2,3,4,6,5,7,8,12,16,15,9,11,10,22,13,24,18,38,40,48,33,46,20,14,32,27,17,19,25,44,29,28,50,75,21,30,72,71,73,70,133,139,113,76,129,91,42,35,36,23,37,54,45,51,26,43,49,39,82,62,128,107,80,56,53,83,114,140,109,214,52,59,34,47,149,150,141,123,221,111,121",
			"name": "a(1) = 1, a(2n) = A269361(1+a(n)), a(2n+1) = A269363(a(n)).",
			"comment": [
				"This sequence can be represented as a binary tree. Each left hand child is produced as A269361(1+n), and each right hand child as A269363(n), when the parent node contains n:",
				"                                    |",
				"                 ...................1...................",
				"                2                                       3",
				"      4......../ \\........6                   5......../ \\........7",
				"     / \\                 / \\                 / \\                 / \\",
				"    /   \\               /   \\               /   \\               /   \\",
				"   /     \\             /     \\             /     \\             /     \\",
				"  8       12         16       15          9       11         10       22",
				"13 24   18  38     40  48   33  46      20 14   32  27     17  19   25  44",
				"etc.",
				"An example of (suspected) \"entanglement permutation\" where the other pair of complementary sequences is generated by a greedy algorithm.",
				"Sequence is not only injective, but also surjective on N (thus a permutation of natural numbers) provided that A269361 is surjective on A091072 and A269363 is surjective on A091067."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A269366/b269366.txt\"\u003eTable of n, a(n) for n = 1..8192\u003c/a\u003e",
				"Antti Karttunen, \u003ca href=\"/A135141/a135141.pdf\"\u003eEntanglement Permutations\u003c/a\u003e, 2016-2017",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"program": [
				"(Scheme, with defineperm1-macro from _Antti Karttunen_'s IntSeq-library)",
				"(defineperm1 (A269366 n) (cond ((= 1 n) n) ((even? n) (A269361 (+ 1 (A269366 (/ n 2))))) (else (A269363 (A269366 (/ (- n 1) 2))))))",
				"(define (A269365 n) (A269366 (- n))) ;; The negative side gives the values for the inverse function (from the cache).",
				";; We consider a \u003e b (i.e. not less than b) also in case a is #f.",
				";; (Because of the stateful caching system used by defineperm1-macro):",
				"(define (not-lte? a b) (cond ((not (number? a)) #t) (else (\u003e a b))))"
			],
			"xref": [
				"Left inverse: A269365 (also right inverse, if this sequence is a permutation of natural numbers).",
				"Cf. A091067, A091072, A269361, A269363, A269367, A266121."
			],
			"keyword": "nonn,look,tabf",
			"offset": "1,2",
			"author": "_Antti Karttunen_, Feb 25 2016",
			"references": 5,
			"revision": 18,
			"time": "2019-12-10T12:10:10-05:00",
			"created": "2016-02-28T07:59:26-05:00"
		}
	]
}