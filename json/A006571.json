{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006571",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6571,
			"id": "M0092",
			"data": "1,-2,-1,2,1,2,-2,0,-2,-2,1,-2,4,4,-1,-4,-2,4,0,2,2,-2,-1,0,-4,-8,5,-4,0,2,7,8,-1,4,-2,-4,3,0,-4,0,-8,-4,-6,2,-2,2,8,4,-3,8,2,8,-6,-10,1,0,0,0,5,-2,12,-14,4,-8,4,2,-7,-4,1,4,-3,0,4,-6,4,0,-2,8,-10,-4,1,16,-6,4,-2,12,0,0,15,4,-8,-2,-7,-16,0,-8,-7,6,-2,-8",
			"name": "Expansion of q*Product_{k\u003e=1} (1-q^k)^2*(1-q^(11*k))^2.",
			"comment": [
				"Number 23 of the 74 eta-quotients listed in Table I of Martin (1996).",
				"Unique cusp form of weight 2 for congruence group Gamma_1(11). - _Michael Somos_, Aug 11 2011",
				"For some elliptic curves with p-defects given by this sequence, and for more references, see A272196. See also the Michael Somos formula from May 23 2008 below. - _Wolfdieter Lang_, Apr 25 2016"
			],
			"reference": [
				"Barry Cipra, What's Happening in the Mathematical Sciences, Vol. 5, Amer. Math. Soc., 2002; see p. 5.",
				"M. du Sautoy, Review of \"Love and Math: The Heart of Hidden Reality\" by Edward Frenkel, Nature, 502 (Oct 03 2013), p. 36.",
				"N. D. Elkies, Elliptic and modular curves..., in AMS/IP Studies in Advanced Math., 7 (1998), 21-76, esp. p. 42.",
				"J. H. Silverman, A Friendly Introduction to Number Theory, 3rd ed., Pearson Education, Inc, 2006, p. 412.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence).",
				"A. Wiles, Modular forms, elliptic curves and Fermat's last theorem, pp. 243-245 of Proc. Intern. Congr. Math. (Zurich), Vol. 1, 1994."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A006571/b006571.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (first 1002 terms from T. D. Noe)",
				"J. Cowles, \u003ca href=\"http://dx.doi.org/10.1016/0022-314X(80)90076-1\"\u003eSome congruence properties of three well-known sequences: Two notes\u003c/a\u003e, J. Num. Theory 12(1) (1980) 84.",
				"H. Darmon, \u003ca href=\"http://www.ams.org/notices/199911/comm-darmon.pdf\"\u003eA proof of the full Shimura-Taniyama-Weil conjecture is announced\u003c/a\u003e, Notices Amer. Math. Soc., Dec. 1999, pp. 1397-1401.",
				"F. Diamond, \u003ca href=\"http://www.pnas.org/content/94/21/11115.full\"\u003eCongruences between modular forms: raising the level and dropping Euler factors\u003c/a\u003e, in Elliptic curves and modular forms (Washington, DC, 1996). Proc. Nat. Acad. Sci. U.S.A. 94 (1997), 11143-11146.",
				"Steven R. Finch, \u003ca href=\"https://doi.org/10.1017/9781316997741\"\u003eMathematical Constants II\u003c/a\u003e, Encyclopedia of Mathematics and Its Applications, Cambridge University Press, Cambridge, 2018.",
				"A. W. Knapp, \u003ca href=\"http://www.ams.org/notices/201409/rnoti-p1056.pdf\"\u003eReview of \"Love and Math: The Heart of Hidden Reality\" by E. Frenkel\u003c/a\u003e, Notices Amer. Math. Soc., 61 (2014), pp. 1056-1060; see p. 1058, but beware typos.",
				"Y. Martin, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9947-96-01743-6\"\u003eMultiplicative eta-quotients\u003c/a\u003e, Trans. Amer. Math. Soc. 348 (1996), no. 12, 4825-4856, see page 4852 Table I.",
				"Shimura, Goro, \u003ca href=\"http://dx.doi.org/10.1515/crll.1966.221.209\"\u003eA reciprocity law in non-solvable extensions\u003c/a\u003e, J. Reine Angew. Math. 221 1966 209-220.",
				"G. Shimura, \u003ca href=\"/A002070/a002070.pdf\"\u003eA reciprocity law in non-solvable extensions\u003c/a\u003e, J. Reine Angew. Math. 221 1966 209-220. [Annotated scan of pages 218, 219 only]",
				"Michael Somos, \u003ca href=\"/A030203/a030203.txt\"\u003eIndex to Yves Martin's list of 74 multiplicative eta-quotients and their A-numbers\u003c/a\u003e"
			],
			"formula": [
				"Expansion of (eta(q) * eta(q^11))^2 in powers of q.",
				"a(n) = A000594(n) (mod 11). [Cowles]. - _R. J. Mathar_, Feb 13 2007",
				"Euler transform of period 11 sequence [ -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -4, ...]. - _Michael Somos_, Feb 12 2006",
				"a(n) is multiplicative with a(11^e) = 1, a(p^e) = a(p) * a(p^(e-1)) - p * a(p^(e-2)) for p != 11. - _Michael Somos_, Feb 12 2006",
				"G.f. A(q) satisfies 0 = f(A(q), A(q^2), A(q^4)) where f(u, v, w) = u*w * (u + 4*v + 4*w) - v^3. - _Michael Somos_, Mar 21 2005",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (11 t)) = 11 (t/i)^2 f(t) where q = exp(2 Pi i t).",
				"Convolution square of A030200.",
				"Coefficients of L-series for elliptic curve \"11a3\": y^2 + y = x^3 - x^2. - _Michael Somos_, May 23 2008",
				"Convolution inverse is A032442. - _Michael Somos_, Apr 21 2015",
				"a(prime(n)) = prime(n) - A272196(n), n \u003e= 3.",
				"  a(2) = -2 is not  2 - A272196(1) = 0. Modularity pattern of some elliptic curves. - _Wolfdieter Lang_, Apr 25 2016"
			],
			"example": [
				"G.f.: q - 2*q^2 - q^3 + 2*q^4 + q^5 + 2*q^6 - 2*q^7 - 2*q^9 - 2*q^10 + q^11 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ q (QPochhammer[ q] QPochhammer[ q^11])^2, {q, 0, n}]; (* _Michael Somos_, Aug 11 2011 *)",
				"a[ n_] := SeriesCoefficient[ q (Product[ (1 - q^k), {k, 11, n, 11}] Product[ 1 - q^k, {k, n}])^2, {q, 0, n}]; (* _Michael Somos_, May 27 2014 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, 0, ellak( ellinit( [0, -1, 1, 0, 0], 1), n))};",
				"(PARI) {a(n) = my(A); if( n\u003c1, 0, n--; A = x * O(x^n); polcoeff( (eta(x + A) * eta(x^11 + A))^2, n))};",
				"(PARI) {a(n) = my(A, p, e, x, y, a0, a1); if( n\u003c1, 0, A = factor(n); prod( k=1, matsize(A)[1], [p, e] = A[k, ]; if( p==11, 1, a0=1; a1 = y = -sum(x=0, p-1, kronecker( 4*x^3 - 4*x^2 + 1, p)); for( i=2, e, x = y*a1 - p*a0; a0=a1; a1=x); a1)))}; /* _Michael Somos_, Aug 13 2006 */",
				"(Sage) CuspForms( Gamma1(11), 2, prec = 101).0 # _Michael Somos_, Aug 11 2011",
				"(MAGMA) [ Coefficient(qEigenform(EllipticCurve([0, -1, 1, 0, 0]), n+1),n) : n in [1..100] ]; /* _Klaus Brockhaus_, Jan 29 2007 */",
				"(MAGMA) [ Coefficient(Basis(ModularForms(Gamma0(11), 2))[2], n) : n in [1..100] ]; /* _Klaus Brockhaus_, Jan 31 2007 */",
				"(MAGMA) Basis( CuspForms( Gamma1(11), 2), 101) [1]; /* _Michael Somos_, Jul 14 2014 */"
			],
			"xref": [
				"Cf. A002070 (terms with prime indices), A032442, A030200."
			],
			"keyword": "sign,easy,nice,mult",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"references": 18,
			"revision": 79,
			"time": "2021-03-12T22:24:41-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}