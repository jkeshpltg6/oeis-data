{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A181327",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 181327,
			"data": "1,2,4,3,12,12,32,41,9,86,140,54,232,451,246,27,624,1416,1008,216,1680,4357,3811,1215,81,4522,13192,13692,5832,810,12172,39455,47380,25254,5400,243,32764,116820,159296,102024,29700,2916,88192,343029,523549",
			"name": "Triangle read by rows: T(n,k) is the number of 2-compositions of n having k columns with an even sum (0\u003c=k\u003c=floor(n/2)).",
			"comment": [
				"A 2-composition of n is a nonnegative matrix with two rows, such that each column has at least one nonzero entry and whose entries sum up to n.",
				"Row n has 1+floor(n/2) entries.",
				"The sum of entries in row n is A003480(n).",
				"T(n,0) = A181329(n).",
				"Sum(k*T(n,k), k\u003e=0) = A181328(n).",
				"For the statistic \"number of column with an odd sum\" see A181308."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A181327/b181327.txt\"\u003eRows n = 0..200, flattened\u003c/a\u003e",
				"G. Castiglione, A. Frosini, E. Munarini, A. Restivo and S. Rinaldi, \u003ca href=\"http://dx.doi.org/10.1016/j.ejc.2006.06.020\"\u003eCombinatorial aspects of L-convex polyominoes\u003c/a\u003e, European J. Combin. 28 (2007), no. 6, 1724-1741."
			],
			"formula": [
				"G.f.: G(t,z) = (1-z)^2*(1+z)^2/(1-2*z-2*z^2+z^4-3*t*z^2+t*z^4).",
				"The g.f. of column k is z^{2k}*(1-z^2)^2*(3-z^2)^k/(1-2z-2z^2+z^4)^{k+1}",
				"The g.f. H(t,s,z), where z marks size and t (s) marks number of columns with an odd (even) sum, is H=(1-z^2)^2/(1-2z^2+z^4-2tz-3sz^2+sz^4)."
			],
			"example": [
				"T(2,1) = 3 because we have (0 / 2), (1 / 1), and (2 / 0) (the 2-compositions are written as (top row / bottom row)).",
				"Triangle starts:",
				"1;",
				"2;",
				"4,    3;",
				"12,  12;",
				"32,  41,  9;",
				"86, 140, 54;"
			],
			"maple": [
				"G := (1-z^2)^2/(1-2*z-2*z^2+z^4-3*t*z^2+t*z^4): Gser := simplify(series(G, z = 0, 15)): for n from 0 to 12 do P[n] := sort(coeff(Gser, z, n)) end do: for n from 0 to 12 do seq(coeff(P[n], t, k), k = 0 .. floor((1/2)*n)) end do; # yields sequence in triangular form",
				"# second Maple program:",
				"b:= proc(n) option remember; `if`(n=0, 1,",
				"       expand(add(add(`if`(i=0 and j=0, 0, b(n-i-j)*",
				"       `if`(irem(i+j,2)=0, x, 1)), i=0..n-j), j=0..n)))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=0..degree(p)))(b(n)):",
				"seq(T(n), n=0..15);  # _Alois P. Heinz_, Mar 16 2014"
			],
			"mathematica": [
				"b[n_] := b[n] = If[n == 0, 1, Expand[Sum[Sum[If[i == 0 \u0026\u0026 j == 0, 0, b[n-i-j] * If[Mod[i+j, 2] == 0, x, 1]], {i, 0, n-j}], {j, 0, n}]]]; T[n_] := Function[{p}, Table[Coefficient[p, x, i], {i, 0, Exponent[p, x]}]][b[n]]; Table[T[n], {n, 0, 15}] // Flatten (* _Jean-François Alcover_, May 27 2015, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A003480, A181308, A181328, A181329."
			],
			"keyword": "nonn,tabf",
			"offset": "0,2",
			"author": "_Emeric Deutsch_, Oct 13 2010",
			"references": 5,
			"revision": 14,
			"time": "2015-05-27T10:34:36-04:00",
			"created": "2010-10-20T03:00:00-04:00"
		}
	]
}