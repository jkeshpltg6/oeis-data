{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A328969",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 328969,
			"data": "1,0,1,3,0,0,0,1,3,1,0,0,0,0,1,7,0,0,0,0,4,0,0,5,0,1,0,0,0,0,0,1,9,1,0,0,0,0,0,0,0,0,1,7,0,0,1,0,0,0,5,1,0,0,0,15,0,0,0,0,0",
			"name": "Irregular table T(n,k), n \u003e= 2, k=1..pi(n). arising in expressing the sequence A006022 as the coefficients depending on the maximal k-th prime factor pk of the formula for A006022(n) of its unique prime factor equation.",
			"comment": [
				"The length of the n-th row is pi(n) (A000720), i.e., 1,2,2,3,... for n\u003e2.",
				"The sum of the rows equals the sequence A006022.",
				"When n is prime the entire row is 0 except at p=n where T(p,p)=1."
			],
			"link": [
				"Jonathan Blanchette and Robert Laganière, \u003ca href=\"https://arxiv.org/abs/1910.11749\"\u003eA Curious Link Between Prime Numbers, the Maundy Cake Problem and Parallel Sorting\u003c/a\u003e, arXiv:1910.11749 [cs.DS], 2019."
			],
			"formula": [
				"Let p_k be the k-th prime, where k is the column index, p_k \u003c= n, and n \u003e= 2, and m_k is the multiplicity of p_k occurring in n:",
				"T(n,p_k) = n * 1/(p_1^m_1*p_2^m_2*...*p_k^m_k) * (p_k^m_k-1)/(p_k-1), if p_k divides n;",
				"T(n,p_k) = 0; if p_k does not divide n.",
				"T(2*n,2) = A129527(n); T(2*n+1,2) = 0."
			],
			"example": [
				"First few rows are:",
				"  1;",
				"  0, 1;",
				"  3, 0;",
				"  0, 0, 1;",
				"  3, 1, 0;",
				"  0, 0, 0, 1;",
				"  7, 0, 0, 0;",
				"  0, 4, 0, 0;",
				"  5, 0, 1, 0;",
				"  0, 0, 0, 0, 1;",
				"  ...",
				"Examples (see the p_k formulas)",
				"T(2^3,1) = (2^3-1) / (2-1) = 7",
				"T(3^2,1) = (3^2-1) / (3-1) = 4",
				"T(3*2,2) = (6/(2*3)) * (3^2-1) / (3-1) = 4",
				"T(12,1) = (12/(2^2)) * (2^2-1) / (2-1) = 9",
				"T(12,2) = (12/(2^2*3)) * (3-1) / (3-1) = 1",
				"T(15,2) = (15/3) * (3-1) / (3-1) = 5",
				"T(15,3) = (15/(2^2*3)) * (3-1) / (3-1) = 1",
				"T(2*3*5^2*7,3) = (2*3*5^2*7/(2*3*5^2)) * (5^2-1) / (5-1) = 42"
			],
			"xref": [
				"The rows sum to A006022. Cf. A129527 (first column)."
			],
			"keyword": "nonn,tabf",
			"offset": "2,4",
			"author": "_Jonathan Blanchette_, Nov 01 2019",
			"references": 1,
			"revision": 42,
			"time": "2020-01-08T00:14:14-05:00",
			"created": "2020-01-08T00:14:14-05:00"
		}
	]
}