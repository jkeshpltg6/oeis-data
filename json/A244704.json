{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A244704",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 244704,
			"data": "1,1,3,6,13,25,55,107,224,454,938,1916,3969,8163,16918,35010,72724,151093,314749,656115,1370348,2864948,5998547,12572884,26385837,55431031,116577538,245415158,517152607,1090771973,2302729115,4865449045,10288826434,21774842539",
			"name": "Number of n-node unlabeled rooted trees with thinning limbs and root outdegree (branching factor) 3.",
			"comment": [
				"In a rooted tree with thinning limbs the outdegree of a parent node is larger than or equal to the outdegree of any of its child nodes."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A244704/b244704.txt\"\u003eTable of n, a(n) for n = 4..1000\u003c/a\u003e"
			],
			"formula": [
				"a(n) ~ c * d^n / n^(3/2), where d = 2.1991393868..., c = 1.0259536... . - _Vaclav Kotesovec_, Aug 27 2014"
			],
			"example": [
				"a(7) = 6:",
				"    o       o        o      o        o       o",
				"   /|\\     /|\\      /|\\    /|\\     / | \\    /|\\",
				"  o o o   o o o    o o o  o o o   o  o  o  o o o",
				"  |      ( )      /|\\     | |    ( ) |     | | |",
				"  o      o o     o o o    o o    o o o     o o o",
				"  |      |                |",
				"  o      o                o",
				"  |",
				"  o"
			],
			"maple": [
				"b:= proc(n, i, h, v) option remember; `if`(n=0,",
				"      `if`(v=0, 1, 0), `if`(i\u003c1 or v\u003c1 or n\u003cv, 0,",
				"      `if`(n=v, 1, add(binomial(A(i, min(i-1, h))+j-1, j)",
				"       *b(n-i*j, i-1, h, v-j), j=0..min(n/i, v)))))",
				"    end:",
				"A:= proc(n, k) option remember;",
				"      `if`(n\u003c2, n, add(b(n-1$2, j$2), j=1..min(k,n-1)))",
				"    end:",
				"a:= n-\u003e b(n-1$2, 3$2):",
				"seq(a(n), n=4..50);"
			],
			"mathematica": [
				"b[n_, i_, h_, v_] := b[n, i, h, v] = If[n == 0, If[v == 0, 1, 0], If[i \u003c 1 || v \u003c 1 || n \u003c v, 0, If[n == v, 1, Sum[Binomial[A[i, Min[i - 1, h]] + j - 1, j]*b[n - i*j, i - 1, h, v - j], {j, 0, Min[n/i, v]}]]]];",
				"A[n_, k_] := A[n, k] = If[n \u003c 2, n, Sum[b[n - 1, n - 1, j, j], {j, 1, Min[k, n - 1]}]];",
				"a[n_] := b[n-1, n-1, 3, 3];",
				"a /@ Range[4, 50] (* _Jean-François Alcover_, Dec 27 2020, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Column k=3 of A244657."
			],
			"keyword": "nonn",
			"offset": "4,3",
			"author": "_Alois P. Heinz_, Jul 04 2014",
			"references": 2,
			"revision": 12,
			"time": "2020-12-27T19:44:23-05:00",
			"created": "2014-07-08T08:58:13-04:00"
		}
	]
}