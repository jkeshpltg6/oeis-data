{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A243094",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 243094,
			"data": "1,2,5,8,19,44,92,201,444,965,2104,4602,10045,21924,47879,104540,228236,498337,1088072,2375657,5186976,11325186,24727205,53988976,117878715,257374492,561947340,1226946953,2678896484,5849059949,12770744632,27883440986,60880261949",
			"name": "Cardinality of the Weyl alternation set corresponding to the zero-weight in the representation of the Lie algebra sp(2n) whose highest weight is the second fundamental weight.",
			"comment": [
				"Number of Weyl group elements contributing nonzero terms to Kostant's weight multiplicity formula when computing the multiplicity of the zero-weight in the defining representation for the Lie algebra of type C and rank n. Here the highest weight would be the second fundamental weight of sp(2n)."
			],
			"link": [
				"P. E. Harris, \u003ca href=\"https://people.uwm.edu/jw/files/2016/04/pamela-e-harris-1zu8lyw.pdf\"\u003eCombinatorial problems related to Kostant's weight multiplicity formula\u003c/a\u003e, PhD Dissertation, University of Wisconsin-Milwaukee, 2012.",
				"P. E. Harris, E. Insko, L. K. Williams, \u003ca href=\"http://arxiv.org/abs/1401.0055\"\u003eThe adjoint representation of a Lie algebra and the support of Kostant's weight multiplicity formula\u003c/a\u003e, arXiv preprint arXiv:1401.0055 [math.RT], 2013.",
				"B. Kostant, \u003ca href=\"http://www.ncbi.nlm.nih.gov/pmc/articles/PMC528626/\"\u003eA Formula for the Multiplicity of a Weight\u003c/a\u003e, Proc Natl Acad Sci U S A. 1958 June; 44(6): 588-589."
			],
			"formula": [
				"a(n) = A232162(n) + A232162(n-1).",
				"a(n) = a(n-1) + a(n-2) + 3*a(n-3) + a(n-4).",
				"G.f.: (x^4 + 2*x^3 - 2*x^2 - x - 1) / (x^4 + 3*x^3 + x^2 + x - 1). - _Joerg Arndt_, Aug 18 2014"
			],
			"maple": [
				"r:=proc(n::nonnegint) option remember",
				"if n=0 then return 0:",
				"elif n=1 then return 0:",
				"elif n=2 then return 2:",
				"elif n=3 then return 3:",
				"else return",
				"r(n-1)+r(n-2)+3*r(n-3)+r(n-4):",
				"end if;",
				"end proc:",
				"a:=proc(n::nonnegint)",
				"if n=0 then return 0:",
				"elif n=1 then return 1:",
				"else return",
				"r(n)+r(n-1):",
				"end if;",
				"end proc:"
			],
			"mathematica": [
				"Join[{1}, LinearRecurrence[{1, 1, 3, 1}, {2, 5, 8, 19}, 32]] (* _Jean-François Alcover_, Dec 05 2017 *)"
			],
			"program": [
				"(PARI) Vec( (x^4+2*x^3-2*x^2-x-1) / (x^4+3*x^3+x^2+x-1) +O(x^66) ) \\\\ _Joerg Arndt_, Aug 18 2014"
			],
			"xref": [
				"Cf. A232162."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Pamela E Harris_, Aug 18 2014",
			"references": 0,
			"revision": 21,
			"time": "2017-12-05T04:01:27-05:00",
			"created": "2014-08-21T00:35:04-04:00"
		}
	]
}