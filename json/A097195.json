{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A097195",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 97195,
			"data": "1,2,2,2,1,2,2,2,3,0,2,2,2,2,0,4,2,2,2,0,1,2,4,2,0,2,2,2,3,2,2,0,2,2,0,2,4,2,2,0,2,4,0,4,0,2,2,2,1,0,4,2,2,0,2,2,2,4,2,0,3,2,2,2,0,0,2,4,2,0,2,4,2,2,0,0,2,2,4,2,4,2,0,2,0,4,0",
			"name": "Expansion of s(12)^3*s(18)^2/(s(6)^2*s(36)), where s(k) := subs(q=q^k, eta(q)) and eta(q) is Dedekind's function, cf. A010815. Then replace q^6 with q.",
			"reference": [
				"N. J. Fine, Basic Hypergeometric Series and Applications, Amer. Math. Soc., 1988; p. 80, Eq. (32.38)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A097195/b097195.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(-1/6) * eta(q^2)^3 * eta(q^3)^2 / (eta(q)^2 * eta(q^6)) in powers of q. - _Michael Somos_, Mar 05 2016",
				"Euler transform of period 6 sequence [ 2, -1, 0, -1, 2, -2, ...]. - _Michael Somos_, Mar 05 2016",
				"Fine gives an explicit formula for a(n) in terms of the divisors of n.",
				"a(n) = b(6*n + 1) where b() is multiplicative with b(2^e) = b(3^e) = 0^e, b(p^e) = e+1 if p == 1 (mod 6), b(p^e) = (1 + (-1)^e)/2 if p == 5 (mod 6).",
				"G.f.: Sum_{k} x^k / (1 - x^(6*k + 1)). - _Michael Somos_, Nov 03 2005",
				"G.f.: Sum_{k\u003e=0} a(k) * x^(6*k + 1) = Sum_{k\u003e0} x^(2*k-1) * (1 - x^(4*k - 2)) * (1 - x^(8*k - 4)) * (1 - x^(20*k - 10)) / (1 - x^(36*k - 18)). - _Michael Somos_, Nov 03 2005",
				"6 * a(n) = A004016(6*n + 1). - _Michael Somos_, Mar 05 2016"
			],
			"example": [
				"G.f. = 1 + 2*x + 2*x^2 + 2*x^3 + x^4 + 2*x^5 + 2*x^6 + 2*x^7 + 3*x^8 + ...",
				"G.f. = q + 2*q^7 + 2*q^13 + 2*q^19 + q^25 + 2*q^31 + 2*q^37 + 2*q^43 + ..."
			],
			"mathematica": [
				"a[n_] := DivisorSum[6n+1, KroneckerSymbol[-3, #]\u0026]; Table[a[n], {n, 0, 100} ] (* _Jean-François Alcover_, Nov 23 2015, after _Michael Somos_ *)",
				"QP = QPochhammer; s = QP[q^2]^3*(QP[q^3]^2/QP[q]^2/QP[q^6]) + O[q]^105; CoefficientList[s, q] (* _Jean-François Alcover_, Nov 27 2015 *)",
				"a[ n_] := If[ n \u003c 1, Boole[n == 0], Times @@ (Which[# \u003c 2, 0^#2, Mod[#, 6] == 5, 1 - Mod[#2, 2], True, #2 + 1] \u0026 @@@ FactorInteger@(6 n + 1))]; (* _Michael Somos_, Mar 05 2016 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, 0, sumdiv(6*n+1, d, kronecker(-3, d)))}; /* _Michael Somos_, Nov 03 2005 */",
				"(PARI) {a(n) = my(A, p, e); if( n\u003c0, 0, n = 6*n+1; A = factor(n); prod(k=1, matsize(A)[1], [p, e] = A[k, ]; if( p\u003e3, if( p%6==1, e+1, !(e%2)))))}; /* _Michael Somos_, Nov 03 2005 */",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A)^3 * eta(x^3 + A)^2 / (eta(x + A)^2 * eta(x^6 + A)), n))}; /* _Michael Somos_, Nov 03 2005 */"
			],
			"xref": [
				"cf. A004016."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Sep 16 2004",
			"references": 34,
			"revision": 23,
			"time": "2017-05-17T08:16:18-04:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}