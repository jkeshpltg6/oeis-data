{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A045500",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 45500,
			"data": "1,1,6,27,125,635,3488,20425,126817,831915,5744784,41618459,315388311,2493721645,20526285716,175529425815,1556577220651,14290644428279,135624265589086,1328702240382589,13420603191219111,139592874355534071",
			"name": "Fifth-from-right diagonal of triangle A121207.",
			"comment": [
				"With leading 0 and offset 4: number of permutations beginning with 54321 and avoiding 1-23. - _Ralf Stephan_, Apr 25 2004",
				"a(n) is the number of set partitions of {1,2,...,n+4} in which the last block has length 4: the blocks are arranged in order of their least element. - _Don Knuth_, Jun 12 2017"
			],
			"reference": [
				"See also references under sequence A040027."
			],
			"link": [
				"S. Kitaev, \u003ca href=\"http://www.mat.univie.ac.at/~slc/wpapers/s48kitaev.html\"\u003eGeneralized pattern avoidance with additional restrictions\u003c/a\u003e, Sem. Lothar. Combinat. B48e (2003).",
				"S. Kitaev and T. Mansour, \u003ca href=\"https://arxiv.org/abs/math/0205182\"\u003eSimultaneous avoidance of generalized patterns\u003c/a\u003e, arXiv:math/0205182 [math.CO], 2014."
			],
			"formula": [
				"a(n+1) = Sum_{k=0..n} binomial(n+4, k+4)*a(k). - _Vladeta Jovovic_, Nov 10 2003",
				"With offset 4, e.g.f.: x^4 + exp(exp(x))/24 * int[0..x, t^4*exp(-exp(t)+t) dt]. - _Ralf Stephan_, Apr 25 2004",
				"O.g.f. satisfies: A(x) = 1 + x*A( x/(1-x) ) / (1-x)^5. [From Paul D. Hanna, Mar 23 2012]"
			],
			"mathematica": [
				"a[0] = a[1] = 1; a[n_] := a[n] = Sum[Binomial[n+3, k+4]*a[k], {k, 0, n-1}];",
				"Table[a[n], {n, 0, 21}] (* _Jean-François Alcover_, Jul 14 2018, after _Vladeta Jovovic_ *)"
			],
			"program": [
				"(PARI) {a(n)=local(A=1+x); for(i=1, n, A=1+x*subst(A, x, x/(1-x+x*O(x^n)))/(1-x)^5); polcoeff(A, n)} /* Paul D. Hanna, Mar 23 2012 */",
				"(Python)",
				"# The function Gould_diag is defined in A121207.",
				"A045500_list = lambda size: Gould_diag(5, size)",
				"print(A045500_list(24)) # _Peter Luschny_, Apr 24 2016"
			],
			"xref": [
				"Cf. A040027, A045499, A045501, A121207.",
				"Column k=4 of A124496."
			],
			"keyword": "easy,nonn",
			"offset": "0,3",
			"author": "_Henry Gould_",
			"ext": [
				"More terms from _Vladeta Jovovic_, Nov 10 2003",
				"Entry revised by _N. J. A. Sloane_, Dec 11 2006"
			],
			"references": 5,
			"revision": 38,
			"time": "2018-07-14T04:21:10-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}