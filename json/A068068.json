{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A068068",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 68068,
			"data": "1,1,2,1,2,2,2,1,2,2,2,2,2,2,4,1,2,2,2,2,4,2,2,2,2,2,2,2,2,4,2,1,4,2,4,2,2,2,4,2,2,4,2,2,4,2,2,2,2,2,4,2,2,2,4,2,4,2,2,4,2,2,4,1,4,4,2,2,4,4,2,2,2,2,4,2,4,4,2,2,2,2,2,4,4,2,4,2,2,4,4,2,4,2,4,2,2,2,4,2,2,4,2,2,8",
			"name": "Number of odd unitary divisors of n. d is a unitary divisor of n if d divides n and gcd(d,n/d)=1.",
			"comment": [
				"Shadow transform of triangular numbers.",
				"a(n) is the number of primitive Pythagorean triangles with inradius n. For the smallest inradius of exactly 2^n primitive Pythagorean triangles see A070826.",
				"Multiplicative with a(2^e) = 1, a(p^e) = 2, p\u003e2. - _Christian G. Bower_ May 18 2005",
				"Number of primitive Pythagorean triangles with leg 4n. For smallest (even) leg of exactly 2^n PPTs, see A088860. - _Lekraj Beedassy_, Jul 12 2006",
				"As shown by Chi and Killgrove, a(n) is the total number of primitive Pythagorean triples satisfying area = n * perimeter, or equivalently 2 raised to the power of the number of distinct, odd primes contained in n. - _Ant King_, Mar 15 2011",
				"This is the case k=0 of the sum over the k-th powers of the odd unitary divisors of n, which is multiplicative with a(2^e)=1 and a(p^e)=1+p^(e*k), p\u003e2, and has Dirichlet g.f. zeta(s)*zeta(s-k)*(1-2^(k-s))/( zeta(2s-k)*(1-2^(k-2*s)) ). - _R. J. Mathar_, Jun 20 2011",
				"Also the number of odd squarefree divisors of n: a(n) = sum ((A077610(n,k) mod 2): k = 1..A034444(k)) = sum ((A206778(n,k) mod 2): k = 1..A034444(k)). - _Reinhard Zumkeller_, Feb 12 2012"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A068068/b068068.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Henjin Chi and Raymond Killgrove, \u003ca href=\"https://cms.math.ca/crux/backfile/Crux_v15n05_May.pdf\"\u003eProblem 1447\u003c/a\u003e, Crux Math 15(5), May 1989.",
				"Henjin Chi and Raymond Killgrove, \u003ca href=\"https://cms.math.ca/crux/backfile/Crux_v16n07_Sep.pdf\"\u003eSolution to Problem 1447\u003c/a\u003e, Crux Math 16(7), September 1990.",
				"L. J. Gerstein, \u003ca href=\"http://www.jstor.org/stable/30044157\"\u003ePythagorean triples and inner products\u003c/a\u003e, Math. Mag. 78 (2005), 205-213.",
				"Lorenz Halbeisen and Norbert Hungerbuehler, Number theoretic aspects of a combinatorial function, Notes on Number Theory and Discrete Mathematics 5 (1999), 138-150. (\u003ca href=\"http://user.math.uzh.ch/halbeisen/publications/psf/seq.ps\"\u003eps\u003c/a\u003e, \u003ca href=\"http://user.math.uzh.ch/halbeisen/publications/pdf/seq.pdf\"\u003epdf\u003c/a\u003e); see Definition 7 for the shadow transform.",
				"Lorenz Halbeisen, \u003ca href=\"http://www.iam.fmph.uniba.sk/amuc/_vol-74/_no_2/_halbeisen/halbeisen.html\"\u003eA number-theoretic conjecture and its implication for set theory\u003c/a\u003e, Acta Math. Univ. Comenianae 74(2) (2005), 243-254.",
				"R. J. Mathar, \u003ca href=\"http://arxiv.org/abs/1106.4038\"\u003eSurvey of Dirichlet series of multiplicative arithmetic functions\u003c/a\u003e, arXiv:1106.4038 [math.NT], 2011-2012.",
				"OEIS Wiki, \u003ca href=\"https://oeis.org/wiki/Shadow_transform\"\u003eShadow transform\u003c/a\u003e.",
				"Neville Robbins, \u003ca href=\"http://www.fq.math.ca/Papers1/44-4/quartRobbins04_2006.pdf\"\u003eOn the number of primitive Pythagorean triangles with a given inradius\u003c/a\u003e, Fibonacci Quart. 44(4) (2006), 368-369.",
				"N. J. A. Sloane, \u003ca href=\"/transforms.txt\"\u003eTransforms\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/UnitaryDivisor.html\"\u003eUnitary Divisor\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Unitary_divisor\"\u003eUnitary divisor\u003c/a\u003e."
			],
			"formula": [
				"a(n) = A034444(2n)/2. If n is even, a(n) = 2^(omega(n)-1); if n is odd, a(n) = 2^omega(n). Here omega(n) = A001221(n) is the number of distinct prime divisors of n.",
				"a(n) = A024361(4n). - _Lekraj Beedassy_, Jul 12 2006",
				"Dirichlet g.f.: zeta^2(s)/ ( zeta(2*s)*(1+2^(-s)) ). Dirichlet convolution of A034444 and A154269. - _R. J. Mathar_, Apr 16 2011",
				"a(n) = Sum_{d|n} A008683(2d)^2. - _Ridouane Oudra_, Aug 11 2019",
				"Sum_{k=1..n} a(k) ~ 4*n*((log(n) + 2*gamma - 1 + log(2)/3) / Pi^2 - 12*zeta'(2) / Pi^4), where gamma is the Euler-Mascheroni constant A001620. - _Vaclav Kotesovec_, Sep 18 2020"
			],
			"maple": [
				"A068068 := proc(n) local a,f; a :=1 ; for f in ifactors(n)[2] do if op(1,f) \u003e 2 then a := a*2 ; end if; end do: a ; end proc: # _R. J. Mathar_, Apr 16 2011"
			],
			"mathematica": [
				"a[n_] := Length[Select[Divisors[n], OddQ[ # ]\u0026\u0026GCD[ #, n/# ]==1\u0026]]",
				"a[n_] := 2^(PrimeNu[n]+Mod[n, 2]-1); Array[a, 105] (* _Jean-François Alcover_, Dec 01 2015 *)",
				"f[p_, e_] := If[p == 2, 1, 2]; a[1] = 1; a[n_] := Times @@ f @@@ FactorInteger[n]; Array[a, 100] (* _Amiram Eldar_, Sep 18 2020 *)"
			],
			"program": [
				"(Haskell)",
				"a068068 = length . filter odd . a077610_row",
				"-- _Reinhard Zumkeller_, Feb 12 2012",
				"(PARI) a(n) = sumdiv(n, d, (d%2)*(gcd(d, n/d)==1)); \\\\ _Michel Marcus_, May 13 2014",
				"(PARI) a(n) = 2^omega(n\u003e\u003evaluation(n,2)) \\\\ _Charles R Greathouse IV_, May 14 2014"
			],
			"xref": [
				"Cf. A056901, A068067, A008683."
			],
			"keyword": "nonn,mult,easy",
			"offset": "1,3",
			"author": "_Robert G. Wilson v_, Feb 19 2002",
			"ext": [
				"Edited by _Dean Hickerson_, Jun 08 2002"
			],
			"references": 15,
			"revision": 85,
			"time": "2020-09-18T05:35:41-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}