{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A228550",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 228550,
			"data": "1,0,1,1,0,1,3,4,0,1,38,15,10,0,1,720,238,45,20,0,1,26614,5145,868,105,35,0,1,1858122,215355,21000,2408,210,56,0,1,250586792,16797942,980371,64260,5628,378,84,0,1,66121926720,2509697144,84370230,3306415,163800,11676,630,120,0,1",
			"name": "Triangular array read by rows: T(n,k) is the number of simple labeled graphs with n vertices and k components such that each vertex has even degree; n \u003e= 1, 1 \u003c= k \u003c= n.",
			"comment": [
				"The Bell transform of A033678(n+1). For the definition of the Bell transform, see A264428. - _Peter Luschny_, Jan 17 2016"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A228550/b228550.txt\"\u003eRows n = 1..45, flattened\u003c/a\u003e"
			],
			"formula": [
				"E.g.f.: (A(x) + 1)^y, where A(x) = Sum_{n\u003e=1} 2^C(n-1,2) * x^n/n!.",
				"Row sums are 2^binomial(n-1,2) = A006125(n-1).",
				"Column 1 is A033678 (because a connected graph has only one component)."
			],
			"example": [
				"T(3,1) = 1 counts the complete graph on 3 labeled vertices.",
				"T(3,3) = 1 counts the empty graph (no edges) on 3 labeled vertices.",
				"Triangular array T(n,k) (with rows n \u003e= 1 and columns k = 1..n) begins:",
				"    1;",
				"    0,    1;",
				"    1,    0,   1;",
				"    3,    4,   0,   1;",
				"   38,   15,  10,   0,  1;",
				"  720,  238,  45,  20,  0, 1;",
				"  ..."
			],
			"mathematica": [
				"nn = 8; e = Sum[2^Binomial[n - 1, 2] x^n/n!, {n, 1, nn}];",
				"  Prepend[Drop[Map[Insert[#, 0, -2] \u0026,",
				"    Map[Select[#, # \u003e 0 \u0026] \u0026,",
				"     Range[0, nn]! CoefficientList[",
				"       Series[(e + 1)^y, {x, 0, nn}], {x, y}]]], 2], {1}] // Grid"
			],
			"program": [
				"(Sage) # uses[bell_matrix from A264428]",
				"# Adds a column 1, 0, 0, 0, ... at the left side of the triangle.",
				"bell_matrix(lambda n: A033678(n+1), 9) # _Peter Luschny_, Jan 17 2016"
			],
			"xref": [
				"Cf. A006125, A033678, A264428."
			],
			"keyword": "nonn,tabl",
			"offset": "1,7",
			"author": "_Geoffrey Critzer_, Aug 27 2013",
			"references": 2,
			"revision": 76,
			"time": "2021-02-20T17:25:29-05:00",
			"created": "2013-08-28T05:37:31-04:00"
		}
	]
}