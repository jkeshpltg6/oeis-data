{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A122960",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 122960,
			"data": "1,0,1,0,1,1,0,0,3,1,0,1,0,6,1,0,0,5,0,10,1,0,1,0,15,0,15,1,0,0,7,0,35,0,21,1,0,1,0,28,0,70,0,28,1,0,0,9,0,84,0,126,0,36,1,0,1,0,45,0,210,0,210,0,45,1",
			"name": "Triangle T(n,k), 0 \u003c= k \u003c= n, read by rows given by [0, 1, -1, -1, 1, 0, 0, 0, 0, 0, 0, ...] DELTA [1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, ...] where DELTA is the operator defined in A084938.",
			"comment": [
				"T(n,k) = binomial (n,n-k+1) if (n-k) is an odd number (see A000217, A000332, A000579, A000581, ...). T(n,k)= 0 if (n-k)=2x with x \u003e 0 (see A000004). T(n,n)=1 (see A000012)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A122960/b122960.txt\"\u003eRows n = 0..100 of triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"Sum_{k=0..n} T(n,k) = A011782(n).",
				"Sum_{k=0..n} 2^k*T(n,k) = A083323(n).",
				"Sum_{k=0..n} 2^(n-k)*T(n,k) = A122983(n).",
				"G.f.: (1 - 2*x*y - x^2 + x^2*y^2 + x^2*y)/(1 - 3*x*y - x^2 + 3*x^2*y^2 + x^3*y - x^3*y^3). - _Philippe Deléham_, Nov 09 2013",
				"T(n,k) = 3*T(n-1,k-1) + T(n-2,k) - 3*T(n-2,k-2) - T(n-3,k-1) + T(n-3,k-3), T(0,0) = T(1,1) = T(2,1) = T(2,2) = 1, T(1,0) = T(2,0) = 0, T(n,k) = 0 if k \u003c 0 or if k \u003e n. - _Philippe Deléham_, Nov 09 2013"
			],
			"example": [
				"Triangle begins:",
				"  1;",
				"  0, 1;",
				"  0, 1, 1;",
				"  0, 0, 3,  1;",
				"  0, 1, 0,  6,  1;",
				"  0, 0, 5,  0, 10,   1;",
				"  0, 1, 0, 15,  0,  15,   1;",
				"  0, 0, 7,  0, 35,   0,  21,   1;",
				"  0, 1, 0, 28,  0,  70,   0,  28,  1;",
				"  0, 0, 9,  0, 84,   0, 126,   0, 36,  1;",
				"  0, 1, 0, 45,  0, 210,   0, 210,  0, 45, 1;"
			],
			"maple": [
				"T:= proc(n, k) option remember;",
				"      if k\u003c0 or k\u003en then 0",
				"    elif k=n then 1",
				"    elif n=2 and k=1 then 1",
				"    elif k=0 then 0",
				"    else 3*T(n-1, k-1) + T(n-2, k) - 3*T(n-2, k-2) - T(n-3, k-1) + T(n-3, k-3)",
				"      fi; end:",
				"seq(seq(T(n, k), k=0..n), n=0..10); # _G. C. Greubel_, Feb 17 2020"
			],
			"mathematica": [
				"With[{m = 10}, CoefficientList[CoefficientList[Series[(1-2*x*y-x^2+x^2*y^2+",
				"x^2*y)/(1-3*x*y-x^2+3*x^2*y^2+x^3*y-x^3*y^3), {x, 0 , m}, {y, 0, m}], x], y]] // Flatten (* _Georg Fischer_, Feb 17 2020 *)"
			],
			"program": [
				"(PARI) T(n, k) = if(k\u003c0 || k\u003en, 0, if(k==n, 1, if(n==2 \u0026\u0026 k==1, 1, if(k==0, 0, 3*T(n-1, k-1) + T(n-2, k) - 3*T(n-2, k-2) - T(n-3, k-1) + T(n-3, k-3) )))); \\\\ _G. C. Greubel_, Feb 17 2020",
				"(Sage)",
				"@CachedFunction",
				"def T(n, k):",
				"    if (k\u003c0 or k\u003en): return 0",
				"    elif (k==n): return 1",
				"    elif (n=2 and k=1): then 1",
				"    elif (k=0): then 0",
				"    else: return 3*T(n-1, k-1) + T(n-2, k) - 3*T(n-2, k-2) - T(n-3, k-1) + T(n-3, k-3)",
				"[[T(n, k) for k in (0..n)] for n in (0..10)] # _G. C. Greubel_, Feb 17 2020"
			],
			"xref": [
				"Cf. A000004, A000012, A000217, A000332, A000579, A000581, A007318."
			],
			"keyword": "nonn,tabl",
			"offset": "0,9",
			"author": "_Philippe Deléham_, Oct 26 2006",
			"ext": [
				"a(12) corrected by _Georg Fischer_, Feb 17 2020"
			],
			"references": 1,
			"revision": 22,
			"time": "2020-02-17T20:51:24-05:00",
			"created": "2006-12-06T03:00:00-05:00"
		}
	]
}