{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A008315",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 8315,
			"data": "1,1,1,1,1,2,1,3,2,1,4,5,1,5,9,5,1,6,14,14,1,7,20,28,14,1,8,27,48,42,1,9,35,75,90,42,1,10,44,110,165,132,1,11,54,154,275,297,132,1,12,65,208,429,572,429,1,13,77,273,637,1001,1001,429,1,14,90,350,910,1638,2002,1430,1,15,104",
			"name": "Catalan triangle read by rows. Also triangle of expansions of powers of x in terms of Chebyshev polynomials U_n(x).",
			"comment": [
				"There are several versions of a Catalan triangle: see A009766, A008315, A028364, A053121.",
				"Number of standard tableaux of shape (n-k,k) (0\u003c=k\u003c=floor(n/2)). Example: T(4,1)=3 because in th top row we can have 124, 134, or 123 (but not 234). - _Emeric Deutsch_, May 23 2004",
				"T(n,k) is the number of n-digit binary words (length n sequences on {0,1}) containing k 1's such that no initial segment of the sequence has more 1's than 0's. - _Geoffrey Critzer_, Jul 31 2009",
				"T(n,k) is the number of dispersed Dyck paths (i.e. Motzkin paths with no (1,0) steps at positive heights) of length n and having k (1,1)-steps. Example: T(5,1)=4 because, denoting U=(1,1), D=(1,-1), H=1,0), we have HHHUD, HHUDH, HUDHH, and UDHHH. - _Emeric Deutsch_, May 30 2011",
				"T(n,k) is the number of length n left factors of Dyck paths having k (1,-1)-steps. Example: T(5,1)=4 because, denoting U=(1,1), D=(1,-1), we have UUUUD, UUUDU, UUDUU, and UDUUU. There is a simple bijection between length n left factors of Dyck paths and dispersed Dyck paths of length n, that takes D steps into D steps. - _Emeric Deutsch_, Jun 19 2011",
				"Triangle, with zeros omitted, given by (1, 0, 0, -1, 1, 0, 0, -1, 1, 0, 0, -1, 1, ...) DELTA (0, 1, -1, 0, 0, 1, -1, 0, 0, 1, -1, 0, 0, 1, ...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, Dec 12 2011",
				"T(n,k) are rational multiples of A055151(n,k). - _Peter Luschny_, Oct 16 2015",
				"T(2*n,n) = Sum_{k\u003e=0} T(n,k)^2 = A000108(n), T(2*n+1,n) = A000108(n+1). - _Michael Somos_, Jun 08 2020"
			],
			"reference": [
				"M. Abramowitz and I. A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, 1964 (and various reprintings), p. 796.",
				"P. J. Larcombe, A question of proof..., Bull. Inst. Math. Applic. (IMA), 30, Nos. 3/4, 1994, 52-54."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A008315/b008315.txt\"\u003eRows n=0..100 of triangle, flattened\u003c/a\u003e",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"Tewodros Amdeberhan, Moa Apagodu, Doron Zeilberger, \u003ca href=\"http://arxiv.org/abs/1507.07660\"\u003eWilf's \"Snake Oil\" Method Proves an Identity in The Motzkin Triangle\u003c/a\u003e, arXiv:1507.07660 [math.CO], 2015.",
				"Suyoung Choi and Hanchul Park, \u003ca href=\"http://arxiv.org/abs/1210.3776\"\u003eA new graph invariant arises in toric topology\u003c/a\u003e, arXiv preprint arXiv:1210.3776 [math.AT], 2012.",
				"C. Kenneth Fan, \u003ca href=\"http://dx.doi.org/10.1090/S0894-0347-97-00222-1\"\u003eStructure of a Hecke algebra quotient\u003c/a\u003e, J. Amer. Math. Soc. 10 (1997), no. 1, 139-167.",
				"R. K. Guy, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL3/GUY/catwalks.html\"\u003eCatwalks, sandsteps and Pascal pyramids\u003c/a\u003e, J. Integer Sequences, Vol. 3 (2000), Article #00.1.6.",
				"L. Jiu, V. H. Moll, C. Vignat, \u003ca href=\"https://arxiv.org/abs/1401.8037\"\u003eIdentities for generalized Euler polynomials\u003c/a\u003e, arXiv:1401.8037 [math.PR], 2014.",
				"N. Lygeros, O. Rozier, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL13/Lygeros/lygeros5.html\"\u003eA new solution to the equation tau(rho) == 0 (mod p)\u003c/a\u003e, J. Int. Seq. 13 (2010) # 10.7.4.",
				"M. A. A. Obaid, S. K. Nauman, W. M. Fakieh, C. M. Ringel, \u003ca href=\"http://www.math.uni-bielefeld.de/~ringel/opus/jeddah.pdf\"\u003eThe numbers of support-tilting modules for a Dynkin algebra\u003c/a\u003e, 2014 and \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL18/Ringel/ringel22.html\"\u003eJ. Int. Seq. 18 (2015) 15.10.6\u003c/a\u003e.",
				"Alon Regev, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL16/Regev/regev4.html\"\u003eThe central component of a triangulation\u003c/a\u003e, Journal of Integer Sequences, Vol. 16 (2013), Article 13.4.1, p. 7.",
				"J. Riordan, \u003ca href=\"http://www.jstor.org/stable/2005477\"\u003eThe distribution of crossings of chords joining pairs of 2n points on a circle\u003c/a\u003e, Math. Comp., 29 (1975), 215-222.",
				"J. Riordan, \u003ca href=\"/A003480/a003480.pdf\"\u003eThe distribution of crossings of chords joining pairs of 2n points on a circle\u003c/a\u003e, Math. Comp., 29 (1975), 215-222. [Annotated scanned copy]",
				"L. W. Shapiro, \u003ca href=\"/A003517/a003517.pdf\"\u003eA Catalan triangle\u003c/a\u003e, Discrete Math. 14 (1976), no. 1, 83-90. [Annotated scanned copy]",
				"Zheng Shi, \u003ca href=\"https://arxiv.org/abs/1602.00068\"\u003eImpurity entropy of junctions of multiple quantum wires\u003c/a\u003e, arXiv preprint arXiv:1602.00068 [cond-mat.str-el], 2016 (See Appendix A).",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e"
			],
			"formula": [
				"T(n, 0) = 1 if n \u003e= 0; T(2*k, k) = T(2*k-1, k-1) if k\u003e0; T(n, k) = T(n-1, k-1) + T(n-1, k) if k=1, 2, ..., floor(n/2). - _Michael Somos_, Aug 17 1999",
				"T(n, k) = binomial(n, k) - binomial(n, k-1). - _Michael Somos_, Aug 17 1999",
				"Rows of Catalan triangle A008313 read backwards. Sum_{k\u003e=0} T(n, k)^2 = A000108(n); A000108 : Catalan numbers. - _Philippe Deléham_, Feb 15 2004",
				"T(n,k) = C(n,k)*(n-2*k+1)/(n-k+1). - _Geoffrey Critzer_, Jul 31 2009",
				"Sum_{k, 0\u003c=k\u003c=n} T(n,k)*x^k = A000012(n), A001405(n), A126087(n), A128386(n), A121724(n), A128387(n), A132373(n), A132374(n), A132375(n), A121725(n) for x = 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 respectively. - _Philippe Deléham_, Dec 12 2011"
			],
			"example": [
				"Triangle begins:",
				"  1;",
				"  1;",
				"  1, 1;",
				"  1, 2;",
				"  1, 3,  2;",
				"  1, 4,  5;",
				"  1, 5,  9,  5;",
				"  1, 6, 14, 14;",
				"  1, 7, 20, 28, 14;",
				"  ...",
				"T(5,2) = 5 because there are 5 such sequences: {0, 0, 0, 1, 1}, {0, 0, 1, 0, 1}, {0, 0, 1, 1, 0}, {0, 1, 0, 0, 1}, {0, 1, 0, 1, 0}. - _Geoffrey Critzer_, Jul 31 2009"
			],
			"mathematica": [
				"Table[Binomial[k, i]*(k - 2 i + 1)/(k - i + 1), {k, 0, 20}, {i, 0, Floor[k/2]}] // Grid (* _Geoffrey Critzer_, Jul 31 2009 *)"
			],
			"program": [
				"(PARI) {T(n, k) = if( k\u003c0 || k\u003en\\2, 0, if( n==0, 1, T(n-1, k-1) + T(n-1, k)))}; /* _Michael Somos_, Aug 17 1999 */",
				"(Haskell)",
				"a008315 n k = a008315_tabf !! n !! k",
				"a008315_row n = a008315_tabf !! n",
				"a008315_tabf = map reverse a008313_tabf",
				"-- _Reinhard Zumkeller_, Nov 14 2013"
			],
			"xref": [
				"T(2n, n) = A000108 (Catalan numbers), row sums = A001405 (central binomial coefficients).",
				"This is also the positive half of the triangle in A008482. - _Michael Somos_",
				"This is another reading (by shallow diagonals) of the triangle A009766, i.e. A008315[n] = A009766[A056536[n]].",
				"Cf. A120730, A055151."
			],
			"keyword": "nonn,tabf,nice,easy",
			"offset": "0,6",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Expanded description from _Clark Kimberling_, Jun 15 1997"
			],
			"references": 26,
			"revision": 94,
			"time": "2020-06-08T08:35:03-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}