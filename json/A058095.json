{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A058095",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 58095,
			"data": "1,-4,2,12,-21,4,36,-68,21,112,-184,44,275,-456,112,644,-1019,240,1370,-2156,514,2828,-4340,992,5498,-8392,1930,10428,-15675,3528,19060,-28472,6399,34072,-50382,11184,59333,-87260,19312,101496,-148148,32480,170130,-247156",
			"name": "McKay-Thompson series of class 9c for the Monster group.",
			"comment": [
				"Cubic AGM theta functions: a(q) (see A004016), b(q) (A005928), c(q) (A005882)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A058095/b058095.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"D. Ford, J. McKay and S. P. Norton, \u003ca href=\"http://dx.doi.org/10.1080/00927879408825127\"\u003eMore on replicable functions\u003c/a\u003e, Commun. Algebra 22, No. 13, 5175-5193 (1994).",
				"\u003ca href=\"/index/Mat#McKay_Thompson\"\u003eIndex entries for McKay-Thompson series for Monster simple group\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(1/3) * 3 * b(q) / c(q) in powers of q where b(), c() are cubic AGM theta functions.",
				"Expansion of q^(1/3) * (eta(q) / eta(q^3))^4 in powers of q. - _Michael Somos_, Mar 24 2007",
				"Given g.f. A(x), then B(q) = A(q^3) / q satisfies 0 = f(B(q), B(q^2)) where f(u, v) = (u+v)^3 - u*v * (u+3) * (v+3) .",
				"Given g.f. A(x), then B(q) = A(q^3) / q satisfies 0 = f(B(q), B(q^2), B(q^4)) where f(u, v, w) = u^2*v^2 + v^2*w^2 - v*u^2*w^2 + u*w*v^2 - 9*u*w * (u+w).",
				"G.f.: (Product_{k\u003e0} (1 + x^k + x^(2*k)))^-4.",
				"Euler transform of period 3 sequence [ -4, -4, 0, ...]. - _Michael Somos_, Mar 24 2007",
				"a(n) = A112146(3*n - 1). Convolution inverse of A128758."
			],
			"example": [
				"G.f. = 1 - 4*x + 2*x^2 + 12*x^3 - 21*x^4 + 4*x^5 + 36*x^6 - 68*x^7 + ...",
				"T9c = 1/q - 4*q^2 + 2*q^5 + 12*q^8 - 21*q^11 + 4*q^14 + 36*q^17 - 68*q^20 + ..."
			],
			"mathematica": [
				"a[0] = 1; a[n_] := Module[{A = x*O[x]^n}, SeriesCoefficient[(QPochhammer[x + A]/QPochhammer[x^3 + A])^4, n]]; Table[a[n], {n, 0, 50}] (* _Jean-François Alcover_, Nov 06 2015, adapted from _Michael Somos_'s PARI script *)"
			],
			"program": [
				"(PARI) {a(n) = local(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( (eta(x + A) / eta(x^3 + A))^4, n))}; /* _Michael Somos_, Mar 24 2007 */"
			],
			"xref": [
				"Cf. A000521, A007240, A014708, A007241, A007267, A045478, etc.",
				"Cf. A112146, A128758."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Nov 27 2000",
			"references": 4,
			"revision": 29,
			"time": "2017-01-02T01:52:34-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}