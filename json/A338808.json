{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A338808",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 338808,
			"data": "8,128,56,8,0,3,450,270,82,20,10,0,2,2592,2376,972,204,168,48,0,0,5,7266,7574,4550,2254,660,336,98,14,14,0,2,0,0,0,0,0,0,2,27216,31088,15632,5360,1904,432,128,0,0,0,0,0,9,68778,84240,61272,33138,15714,5400,1946,720,270,126,72,18,0,0,4,0,0,0,0,0,0,0,0,4",
			"name": "Irregular table read by rows: The number of k-faced polyhedra, where k\u003e=4, created when an n-antiprism, formed from two n-sided regular polygons joined by 2n adjacent alternating triangles, is internally cut by all the planes defined by any three of its vertices.",
			"comment": [
				"See A338806 for further details and images for this sequence.",
				"The author thanks _Zach J. Shannon_ for assistance in producing the images for this sequence."
			],
			"link": [
				"Hyung Taek Ahn and Mikhail Shashkov, \u003ca href=\"https://cnls.lanl.gov/~shashkov/papers/ahn_geometry.pdf\"\u003eGeometric Algorithms for 3D Interface Reconstruction\u003c/a\u003e.",
				"Scott R. Shannon, \u003ca href=\"/A338808/a338808.jpg\"\u003e4-antiprism, showing the 56 5-faced polyhedra\u003c/a\u003e. See A338806 for an image of the full polyhedra.",
				"Scott R. Shannon, \u003ca href=\"/A338808/a338808_1.jpg\"\u003e4-antiprism, showing the 8 6-faced polyhedra\u003c/a\u003e",
				"Scott R. Shannon, \u003ca href=\"/A338808/a338808_2.jpg\"\u003e4-antiprism, showing the 3 8-faced polyhedra\u003c/a\u003e",
				"Scott R. Shannon, \u003ca href=\"/A338808/a338808_3.jpg\"\u003e7-antiprism, showing the 7266 4-faced polyhedra\u003c/a\u003e. See A338806 for an image of the full polyhedra.",
				"Scott R. Shannon, \u003ca href=\"/A338808/a338808_4.jpg\"\u003e7-antiprism, showing the 7574 5-faced polyhedra\u003c/a\u003e",
				"Scott R. Shannon, \u003ca href=\"/A338808/a338808_5.jpg\"\u003e7-antiprism, showing the 4550 6-faced polyhedra\u003c/a\u003e",
				"Scott R. Shannon, \u003ca href=\"/A338808/a338808_6.jpg\"\u003e7-antiprism, showing the 2254 7-faced polyhedra\u003c/a\u003e",
				"Scott R. Shannon, \u003ca href=\"/A338808/a338808_7.jpg\"\u003e7-antiprism, showing the 660 8-faced polyhedra\u003c/a\u003e",
				"Scott R. Shannon, \u003ca href=\"/A338808/a338808_8.jpg\"\u003e7-antiprism, showing the 336 9-faced polyhedra\u003c/a\u003e.",
				"Scott R. Shannon, \u003ca href=\"/A338808/a338808.png\"\u003e7-antiprism, showing the 98 10-faced polyhedra\u003c/a\u003e. None of these are visible on the surface.",
				"Scott R. Shannon, \u003ca href=\"/A338808/a338808_9.jpg\"\u003e7-antiprism, showing the 14 11-faced, 14 12-faced, 2 14-faced, 2 21-faced polyhedra\u003c/a\u003e. These are colored white, black, red, yellow respectively. None of these are visible on the surface.",
				"Scott R. Shannon, \u003ca href=\"/A338808/a338808_1.png\"\u003e10-antiprism, showing the 13 20-faced polyhedra\u003c/a\u003e. See A338806 for an image of the full polyhedra.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"https://mathworld.wolfram.com/Antiprism.html\"\u003eAntiprism\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Antiprism\"\u003eAntiprism\u003c/a\u003e."
			],
			"formula": [
				"Sum of row n = A338806(n)."
			],
			"example": [
				"The 4-antiprism is cut with 16 internal planes defined by all 3-vertex combinations of its 8 vertices. This leads to the creation of 128 4-faced polyhedra, 56 5-faced polyhedra, 8 6-faced polyhedra, and 3 8-faced polyhedra, 195 pieces in all. Note the number of 8-faced polyhedra is not a multiple of 4 - they lie directly along the z-axis so need not be a multiple of the number of edges forming the regular n-gons.",
				"The table begins:",
				"8;",
				"128,56,8,0,3;",
				"450,270,82,20,10,0,2;",
				"2592,2376,972,204,168,48,0,0,5;",
				"7266,7574,4550,2254,660,336,98,14,14,0,2,0,0,0,0,0,0,2;",
				"27216,31088,15632,5360,1904,432,128,0,0,0,0,0,9;",
				"68778,84240,61272,33138,15714,5400,1946,720,270,126,72,18,0,0,4,0,0,0,0,0,0,0,0,4;",
				"194580,235880,153620,68580,25240,7460,2560,660,200,0,0,0,0,0,0,0,13;"
			],
			"xref": [
				"Cf. A338806 (number of polyhedra), A338801 (regular prism), A338622 (Platonic solids), A333543 (n-dimensional cube)."
			],
			"keyword": "nonn,tabf",
			"offset": "3,1",
			"author": "_Scott R. Shannon_, Nov 10 2020",
			"references": 8,
			"revision": 23,
			"time": "2020-12-07T01:45:19-05:00",
			"created": "2020-12-07T01:45:19-05:00"
		}
	]
}