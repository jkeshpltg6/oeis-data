{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A210849",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 210849,
			"data": "1,2,13,37,314,365,73,13369,31226,1432954,1346393,10982633,59784881,986508685,197301737,12342639754,16335212753,165277755905,33055551181,12781804411945,2556360882389,25830314642530",
			"name": "a(n) = (A048899(n)^2 + 1)/5^n, n \u003e= 0.",
			"comment": [
				"a(n) is integer (nonnegative) because b(n):=A048899(n) satisfies b(n)^2 + 1 == 0 (mod 5^n), n\u003e=0. The solution of this congruence for n\u003e=1, which satisfies also b(n) == 3 (mod 5), is b(n) = 3^(5^(n-1)) (mod 5^n), but this is inconvenient for computing b(n) for large n. Instead one can use the b(n) recurrence which follows immediately, and this is given in the formula field below. To prove that the given b(n) formula solves the first congruence one can analyze the binomial expansion of (10 - 1)^(5^(n-1)) + 1 and show that it is 0 (mod 5^n)) term by term. The second congruence reduces to b(n) == 3^(5^(n-1)) (mod 5) which follows for n\u003e=1 by induction. Because b(n) = 5^n - A048898(n) one could also use the result A048898(n) == 2 (mod 5) once this is proved.",
				"See also the comment on A210848 on two relevant theorems."
			],
			"formula": [
				"a(n) = (b(n)^2 + 1)/5^n, n\u003e=0, with b(n) = A048899(n) given by the recurrence b(n) = b(n-1)^5 (mod 5^n), n\u003e=2, b(0):=0, b(1)=3 (this is the analog of the Mathematica Program by _Jean-François Alcover_ for A048898).",
				"a(n) - A210848(n) = A048899(n) - A048898(n) (== 1 mod 5 if n\u003e0). - _Álvar Ibeas_, Feb 21 2017"
			],
			"example": [
				"a(0) = 1/1 = 1.",
				"a(3) = (68^2 + 1)/5^3 = 37  (b(3) = 18^5 (mod 5^3) = 68)."
			],
			"maple": [
				"b:=proc(n) option remember: if n=0 then 0 elif n=1 then 3",
				"else modp(b(n-1)^5,5^n) fi end proc:",
				"[seq((b(n)^2+1)/5^n,n=0..29)];"
			],
			"mathematica": [
				"b[n_] := b[n] = Which[n == 0, 0, n == 1, 3, True, Mod[b[n-1]^5, 5^n]]; Table[(b[n]^2+1)/5^n, {n, 0, 29}] (* _Jean-François Alcover_, Mar 05 2014, after Maple *)"
			],
			"xref": [
				"Cf. A048899, A048898, A210848 (companion sequence)."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Apr 28 2012",
			"references": 7,
			"revision": 24,
			"time": "2017-02-23T23:28:26-05:00",
			"created": "2012-04-29T14:29:00-04:00"
		}
	]
}