{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A085614",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 85614,
			"data": "1,3,16,105,768,6006,49152,415701,3604480,31870410,286261248,2604681690,23957864448,222399744300,2080911654912,19604537460045,185813170126848,1770558814528770,16951376923852800,162984598242674670",
			"name": "Number of elementary arches of size n.",
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A085614/b085614.txt\"\u003eTable of n, a(n) for n = 1..200\u003c/a\u003e",
				"F. Cazals, \u003ca href=\"http://algo.inria.fr/libraries/autocomb/NCC-html/NCC.html\"\u003eCombinatorics of Non-Crossing Configurations\u003c/a\u003e, Studies in Automatic Combinatorics, Volume II (1997).",
				"Karl Dilcher, Armin Straub, Christophe Vignat, \u003ca href=\"https://arxiv.org/abs/1903.11759\"\u003eIdentities for Bernoulli polynomials related to multiple Tornheim zeta functions\u003c/a\u003e, arXiv:1903.11759 [math.NT], 2019. See p. 13.",
				"Loïc Foissy, \u003ca href=\"http://arxiv.org/abs/1504.06056\"\u003eFree quadri-algebras and dual quadri-algebras\u003c/a\u003e, arXiv preprint arXiv:1504.06056 [math.CO], 2015.",
				"I. M. Gessel, \u003ca href=\"http://arxiv.org/abs/1403.7656\"\u003eA short proof of the Deutsch-Sagan congruence for connected non crossing graphs\u003c/a\u003e, arXiv preprint arXiv:1403.7656 [math.CO], 2014.",
				"Elżbieta Liszewska, Wojciech Młotkowski, \u003ca href=\"https://arxiv.org/abs/1907.10725\"\u003eSome relatives of the Catalan sequence\u003c/a\u003e, arXiv:1907.10725 [math.CO], 2019.",
				"Thomas M. Richardson, \u003ca href=\"http://arxiv.org/abs/1609.01193\"\u003eThe three 'R's and Dual Riordan Arrays\u003c/a\u003e, arXiv:1609.01193 [math.CO], 2016.",
				"M. R. Sepanski, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v21i1p32\"\u003eOn Divisibility of Convolutions of Central Binomial Coefficients\u003c/a\u003e, Electronic Journal of Combinatorics, 21 (1) 2014, #P1.32.",
				"Jian Zhou, \u003ca href=\"https://arxiv.org/abs/1810.03883\"\u003eFat and Thin Emergent Geometries of Hermitian One-Matrix Models\u003c/a\u003e, arXiv:1810.03883 [math-ph], 2018."
			],
			"formula": [
				"G.f. is the series reversion of x-3*x^2+2*x^3.",
				"a(n) = 2^n*(3*n)!!/((n+1)!*n!!). - Maxim Krikun (krikun(AT)iecn.u-nancy.fr), May 25 2007",
				"G.f.: 1/6*sqrt(3)*sin(1/3*arcsin(6*sqrt(3)*x))-1/2*cos(1/3*arcsin(6*sqrt(3)*x)). - _Vaclav Kotesovec_, Oct 21 2012",
				"Conjecture: n*(n-1)*a(n) +(n-1)*(n-2)*a(n-1) -12*(3*n-5)*(3*n-7)*a(n-2) -12*(3*n-8)*(3*n-10)*a(n-3) = 0. - _R. J. Mathar_, Oct 18 2013",
				"a(n) ~ 2^(n - 3/2) * 3^(3*n/2 - 1) / (sqrt(Pi) * n^(3/2)). - _Vaclav Kotesovec_, Aug 22 2017",
				"From _Dixon J. Jones_, Apr 15 2021: (Start)",
				"a(n) = A206300(n)/2 = abs(A224884(n))/2 for n\u003e=1.",
				"a(n) = 4^n Gamma((3*n + 2)/2)/(Gamma((n + 2)/2)*(n + 1)!).",
				"a(n) = (4^n*((n + 2)/2)_n)/(n + 1)!, where (x)_k is the Pochhammer symbol. (End)"
			],
			"maple": [
				"with(combstruct); ar := {EA = Union(Sequence(EA, card \u003e= 2), Prod(Z, Sequence(EA), Sequence(EA))), C=Union(Z, Prod(Z,Z,Sequence(EA), Sequence(EA), Sequence(Union(Sequence(EA,card\u003e=1), Prod(Z,Sequence(EA),Sequence(EA))))))}; seq(count([EA,ar], size=i),i=1..20);"
			],
			"mathematica": [
				"Rest[CoefficientList[Series[1/6*Sqrt[3]*Sin[1/3*ArcSin[6*Sqrt[3]*x]] - 1/2*Cos[1/3*ArcSin[6*Sqrt[3]*x]],{x,0,20}],x]] (* _Vaclav Kotesovec_, Oct 21 2012 *)",
				"Rest[CoefficientList[InverseSeries[Series[x - 3*x^2 + 2*x^3, {x, 0, 20}], x],x]] (* _Vaclav Kotesovec_, Aug 22 2017 *)",
				"(* From _Dixon J. Jones_, Apr 15 2021: (Start) *)",
				"Table[4^n Gamma[(3n + 2)/2]/(Gamma[(n + 2)/2](n + 1)!), {n, 0, 20}]",
				"Table[4^n Pochhammer[(n + 2)/2, n]/(n + 1)!, {n, 0, 20}] (* End *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c1,0,polcoeff(serreverse(x-3*x^2+2*x^3+x*O(x^n)),n))"
			],
			"xref": [
				"Cf. A143018, A206300."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, Jul 10 2003",
			"references": 4,
			"revision": 68,
			"time": "2021-05-26T03:16:43-04:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}