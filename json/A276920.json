{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A276920",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 276920,
			"data": "1,8,27,72,125,216,595,704,1539,1000,1331,1944,3133,4760,3375,5632,4913,12312,8911,9000,16065,10648,12167,19008,16125,25064,45927,42840,24389,27000,35371,47104,35937,39304,74375,110808,58645,71288,84591,88000",
			"name": "Number of solutions to x^3 + y^3 + z^3 + t^3 == 0 (mod n) for 1 \u003c= x, y, z, t \u003c= n.",
			"comment": [
				"a(n) = n^3 if n is in A074243. - _Robert Israel_, Oct 13 2016"
			],
			"link": [
				"Chai Wah Wu, \u003ca href=\"/A276920/b276920.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (terms n = 1..242 from Robert Israel)"
			],
			"example": [
				"For n = 3, we see that all nondecreasing solutions {x, y, z, t} are in {{1, 1, 1, 3}, {1, 1, 2, 2}, {1, 2, 3, 3}, {2, 2, 2, 3}, {3, 3, 3, 3}}. The numbers in the sets can be ordered in 4, 6, 12, 4 and 1 ways respectively. Therefore, a(3) = 4 + 6 + 12 + 4 + 1 = 27. - _David A. Corneth_, Oct 11 2016"
			],
			"maple": [
				"CF:= table([[false, false, true] = 12, [true, false, false] = 12, [true, false, true] = 6, [false, false, false] = 24, [true, true, true] = 1, [false, true, true] = 4, [false, true, false] = 12, [true, true, false] = 4]):",
				"f1:= proc(n)",
				"  option remember;",
				"  local count, t, x,y,z,signature;",
				"  if isprime(n) and n mod 3 = 2 then return n^3 fi;",
				"  count:= 0;",
				"  for t from 1 to n do",
				"    for x from 1 to t do",
				"      for y from 1 to x do",
				"        for z from 1 to y do",
				"          if t^3 + x^3 + y^3 + z^3 mod n = 0 then",
				"            signature:= map(evalb,[z=y,y=x,x=t]);",
				"            count:= count + CF[signature];",
				"          fi",
				"  od od od od;",
				"  count",
				"end proc:",
				"f:= proc(n) local t;",
				"    mul(f1(t[1]^t[2]),t=ifactors(n)[2])",
				"end proc:",
				"map(f, [$1..40]); # _Robert Israel_, Oct 13 2016"
			],
			"mathematica": [
				"JJJ[4, n, lam] = Sum[If[Mod[a^3 + b^3 + c^3 + d^3, n] == Mod[lam, n], 1, 0], {d, 0, n - 1}, {a, 0, n - 1}, {b, 0, n - 1}, {c, 0 , n - 1}]; Table[JJJ[4, n, 0], {n, 1, 50}]"
			],
			"program": [
				"(PARI) a(n) = sum(x=1, n, sum(y=1, n, sum(z=1, n, sum(t=1, n, Mod(x,n)^3 + Mod(y,n)^3 + Mod(z,n)^3 + Mod(t,n)^3 == 0)))); \\\\ _Michel Marcus_, Oct 11 2016",
				"(PARI) qperms(v) = {my(r=1,t); v = vecsort(v); for(i=1,#v-1, if(v[i]==v[i+1], t++, r*=binomial(i, t+1);t=0));r*=binomial(#v,t+1)}",
				"a(n) = {my(t=0); forvec(v=vector(4,i,[1,n]), if(sum(i=1,4,Mod(v[i],n)^3)==0, t+=qperms(v)),1);t} \\\\ _David A. Corneth_, Oct 11 2016",
				"(Python)",
				"def A276920(n):",
				"    ndict = {}",
				"    for i in range(n):",
				"        i3 = pow(i,3,n)",
				"        for j in range(i+1):",
				"            j3 = pow(j,3,n)",
				"            m = (i3+j3) % n",
				"            if m in ndict:",
				"                if i == j:",
				"                    ndict[m] += 1",
				"                else:",
				"                    ndict[m] += 2",
				"            else:",
				"                if i == j:",
				"                    ndict[m] = 1",
				"                else:",
				"                    ndict[m] = 2",
				"    count = 0",
				"    for i in ndict:",
				"        j = (-i) % n",
				"        if j in ndict:",
				"            count += ndict[i]*ndict[j]",
				"    return count # _Chai Wah Wu_, Jun 06 2017"
			],
			"xref": [
				"Cf. A000189, A047726, A060839, A063454, A074243, A087412, A087786, A254073, A276919."
			],
			"keyword": "nonn,mult",
			"offset": "1,2",
			"author": "_José María Grau Ribas_, Sep 22 2016",
			"references": 2,
			"revision": 29,
			"time": "2017-06-07T00:35:43-04:00",
			"created": "2016-12-03T13:08:46-05:00"
		}
	]
}