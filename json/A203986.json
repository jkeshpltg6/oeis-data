{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A203986",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 203986,
			"data": "1,1,1,1,1,1,1,1,0,1,1,1,1,0,1,1,1,0,1,0,1,1,1,4,2,1,0,1,1,1,0,32,0,1,0,1,1,1,29,305,392,11,1,0,1,1,1,0,4331,0,6883,0,1,0,1,1,1,263,63261,2097719,3245664,171088,84,1,0,1,1,1,0,1025113,0,2549091482,0,5661874,0,1,0,1",
			"name": "Square array A(n,k), n\u003e=0, k\u003e=0, read by antidiagonals: A(n,k) is the number of partitions of {1,2,...,k*n} into n k-element subsets having the same sum.",
			"comment": [
				"A(n,k) = 0 if n\u003e1 and k\u003e0 and (k=1 or k*(n-1) mod 2 = 1).",
				"The element sum of each subset is k*(k*n+1)/2."
			],
			"link": [
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Partition_of_a_set\"\u003ePartition of a set\u003c/a\u003e"
			],
			"example": [
				"A(0,0) = 1.",
				"A(1,1) = 1: {1}.",
				"A(2,2) = 1: {1,4}, {2,3}.",
				"A(3,3) = 2: {1,5,9}, {2,6,7}, {3,4,8}; {1,6,8}, {2,4,9}, {3,5,7}.",
				"A(4,2) = 1: {1,8}, {2,7}, {3,6}, {4,5}.",
				"A(2,4) = 4: {1,2,7,8}, {3,4,5,6}; {1,3,6,8}, {2,4,5,7}; {1,4,5,8}, {2,3,6,7}; {1,4,6,7}, {2,3,5,8}.",
				"Square array A(n,k) begins:",
				"  1, 1, 1,  1,    1,       1,          1, ...",
				"  1, 1, 1,  1,    1,       1,          1, ...",
				"  1, 0, 1,  0,    4,       0,         29, ...",
				"  1, 0, 1,  2,   32,     305,       4331, ...",
				"  1, 0, 1,  0,  392,       0,    2097719, ...",
				"  1, 0, 1, 11, 6883, 3245664, 2549091482, ..."
			],
			"maple": [
				"b:= proc() option remember; local i, j, t, k, m; m:= args[nargs-1]; k:= args[nargs]; if args[1]=0 then `if`(nargs=3, 1, b(args[t] $t=2..nargs)) elif args[1]\u003c1 then 0 else add(`if`(args[j]\u003cm, 0, b(sort([seq(args[i] -`if`(i=j, m+1/97, 0), i=1..nargs-2)])[], m-1, k)), j=1..nargs-2) fi end:",
				"A:= (n, k)-\u003e `if`(n=0 or k=0, 1, b((k*(n*k+1)/2 +k/97) $n, k*n, k)/n!):",
				"seq(seq(A(n, d-n), n=0..d), d=0..10);"
			],
			"mathematica": [
				"b[args_List] := b[args] = Module[{nargs = Length[args], k = args[[-1]], m = args[[-2]]}, Which[args[[1]] == 0, If[nargs == 3, 1, b[args[[2 ;; nargs]]]], args[[1]] \u003c 1, 0, True, Sum[If[args[[j]] \u003c m, 0, b[Join[Sort[Table[args[[i]] - If[i == j, m + 1/97, 0], {i, 1, nargs - 2}]], {m - 1, k}]]], {j, 1, nargs - 2}] ]]; A[n_, k_] := If[n == 0 || k == 0, 1, b[Join[Array[(k*(n*k + 1)/2 + k/97) \u0026, n], {k*n, k}]]/n!]; Table[Table[A[n, d - n], {n, 0, d}], {d, 0, 10}] // Flatten (* _Jean-François Alcover_, Jan 21 2015, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A168238 (bisection of row n=2), A203017 (row n=3), A104185 (bisection of column k=3), A203435 (column k=4).",
				"Main diagonal gives A321230."
			],
			"keyword": "nonn,tabl",
			"offset": "0,24",
			"author": "_Alois P. Heinz_, Jan 09 2012",
			"references": 5,
			"revision": 25,
			"time": "2018-11-02T09:44:43-04:00",
			"created": "2012-01-11T17:39:51-05:00"
		}
	]
}