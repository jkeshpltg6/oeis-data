{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A131044",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 131044,
			"data": "1,1,1,1,0,1,1,1,2,1,1,0,4,4,1,1,1,3,8,5,1,1,0,6,14,14,6,1,1,1,6,21,32,21,7,1,1,0,7,32,55,54,28,8,1,1,1,8,38,96,116,83,36,9,1,1,0,10,54,142,222,206,120,45,10,1,1,1,9,65,211,386,438,328,165,55,11,1",
			"name": "Triangle T(n,k) read by rows: T(n,k) is the number of compositions of n into k parts such that at least two adjacent parts are equal.",
			"comment": [
				"Condition is void for compositions into 1 part (there is one such composition).",
				"Triangle = Pascal's triangle (A007318) -  A106351, except for first column."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A131044/b131044.txt\"\u003eRows n = 1..141, flattened\u003c/a\u003e"
			],
			"example": [
				"T(5,3) = 4 because among the 6 compositions of 5 into 3 parts there are 4 with one part repeated, marked by (*) between the parts:",
				"[ 3 1*1 ], [ 2*2 1 ], [ 1 3 1 ], [ 2 1 2 ], [ 1 2*2 ], [ 1*1 3 ].",
				"Triangle begins",
				"1;",
				"1, 1;",
				"1, 0, 1;",
				"1, 1, 2,  1;",
				"1, 0, 4,  4,  1;",
				"1, 1, 3,  8,  5,  1;",
				"1, 0, 6, 14, 14,  6, 1;",
				"1, 1, 6, 21, 32, 21, 7, 1;"
			],
			"maple": [
				"b:= proc(n, h, t) option remember;",
				"      if n\u003ct then 0 elif n=0 then `if`(t=0, 1, 0)",
				"    else add(`if`(h=j, 0, b(n-j, j, t-1)), j=1..n) fi",
				"    end:",
				"T:= (n, k)-\u003e `if`(k=1, 1, binomial(n-1, k-1) -b(n, -1, k)):",
				"seq(seq(T(n, k), k=1..n), n=1..12); # _Alois P. Heinz_, Feb 13 2013"
			],
			"mathematica": [
				"b[n_, h_, t_] := b[n, h, t] = Which[n\u003ct, 0, n == 0, If[t == 0, 1, 0], True, Sum[If[h == j, 0, b[n-j, j, t-1]], {j, 1, n}]]; T[n_, k_] := If[k == 1, 1, Binomial[n-1, k-1] - b[n, -1, k]]; Table[Table[T[n, k], {k, 1, n}], {n, 1, 12}] // Flatten (* _Jean-François Alcover_, Feb 18 2015, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(Sage)",
				"def A131044_r(n,k):",
				"    allowed = lambda x: len(x) \u003c= 1 or 0 in differences(x)",
				"    return len([c for c in Compositions(n,length=k) if allowed(c)])",
				"# [_D. S. McNeil_, Jan 06 2011]"
			],
			"xref": [
				"Cf. A106351 (no two adjacent parts are equal)."
			],
			"keyword": "nonn,tabl",
			"offset": "1,9",
			"author": "_Joerg Arndt_, Jan 06 2011",
			"references": 7,
			"revision": 30,
			"time": "2020-02-27T16:38:29-05:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}