{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006064",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6064,
			"id": "M5367",
			"data": "0,101,10000000000001,1000000000000000000000102",
			"name": "Smallest junction number with n generators.",
			"comment": [
				"Strictly speaking, a junction number is a number n with more than one solution to x+digitsum(x) = n. However, it seems best to start this sequence with n=0, for which there is just one solution, x=0. - _N. J. A. Sloane_, Oct 31 2013.",
				"a(3) = 10^13 + 1 was found by Narasinga Rao, who reports that Kaprekar verified that it is the smallest term. No details of Kaprekar's proof were given.",
				"a(4) = 10^24 + 102 was conjectured by Narasinga Rao.",
				"a(5) = 10^1111111111124 + 102. - Conjectured by Narasinga Rao, confirmed by _Max Alekseyev_ and _N. J. A. Sloane_.",
				"a(6) = 10^2222222222224 + 10000000000002. - _Max Alekseyev_",
				"a(7) = 10^( (10^24 + 10^13 + 115) / 9 ) + 10^13 + 2. - _Max Alekseyev_",
				"a(8) = 10^( (2*10^24 + 214)/9 ) + 10^24 + 103. - _Max Alekseyev_"
			],
			"reference": [
				"M. Gardner, Time Travel and Other Mathematical Bewilderments. Freeman, NY, 1988, p. 116.",
				"D. R. Kaprekar, The Mathematics of the New Self Numbers, Privately printed, 311 Devlali Camp, Devlali, India, 1963.",
				"Narasinga Rao, A. On a technique for obtaining numbers with a multiplicity of generators. Math. Student 34 1966 79--84 (1967). MR0229573 (37 #5147)",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Max Alekseyev, \u003ca href=\"/A006064/a006064.txt\"\u003eTable of expressions for a(n), for n=1..100\u003c/a\u003e",
				"Max A. Alekseyev, Donovan Johnson and N. J. A. Sloane, \u003ca href=\"http://neilsloane.com/doc/colombian12302021.pdf\"\u003eOn Kaprekar's Junction Numbers\u003c/a\u003e, Preprint, 2021.",
				"D. R. Kaprekar, \u003ca href=\"/A003052/a003052_2.pdf\"\u003eThe Mathematics of the New Self Numbers\u003c/a\u003e [annotated and scanned]",
				"Terry Trotter, \u003ca href=\"http://trottermath.net/charlene-numbers/\"\u003eCharlene numbers\u003c/a\u003e [Warning: As of March 2018 this site appears to have been hacked. Proceed with great caution. The original content should be retrieved from the Wayback machine and added here. - _N. J. A. Sloane_, Mar 29 2018]",
				"\u003ca href=\"/index/Coi#Colombian\"\u003eIndex entries for Colombian or self numbers and related sequences\u003c/a\u003e"
			],
			"formula": [
				"a(n) = the smallest m such that there are exactly n solutions to A062028(x)=m."
			],
			"example": [
				"a(2) = 101 since 101 is the smallest number with two generators: 101 = A062028(91) = A062028(100).",
				"a(4) = 10^24 + 102 = 1000000000000000000000102 has exactly four inverses w.r.t. A062028, namely 999999999999999999999893, 999999999999999999999902, 1000000000000000000000091 and 1000000000000000000000100."
			],
			"xref": [
				"Cf. A003052, A230093, A230100, A230303, A230857 (highest power of 10).",
				"Smallest number m such that u + (sum of base-b digits of u) = m has exactly n solutions, for bases 2 through 10: A230303, A230640, A230638, A230867, A238840, A238841, A238842, A238843, A006064."
			],
			"keyword": "nonn,base",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_.",
			"ext": [
				"Edited, a(5)-a(6) added by _Max Alekseyev_, Jun 01 2011",
				"a(1) added, a(5) corrected, a(7)-a(8) added by _Max Alekseyev_, Oct 26 2013"
			],
			"references": 17,
			"revision": 74,
			"time": "2021-12-31T17:21:41-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}