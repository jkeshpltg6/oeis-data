{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A128311",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 128311,
			"data": "0,1,0,3,0,1,0,7,3,1,0,7,0,1,3,15,0,13,0,7,3,1,0,7,15,1,12,7,0,1,0,31,3,1,8,31,0,1,3,7,0,31,0,7,30,1,0,31,14,11,3,7,0,13,48,15,3,1,0,7,0,1,3,63,15,31,0,7,3,21,0,31,0,1,33,7,8,31,0,47,39,1,0,31,15,1,3,39,0,31,63",
			"name": "Remainder upon division of 2^(n-1)-1 by n.",
			"comment": [
				"By Fermat's little theorem, if p \u003e 2 is prime, then 2^(p-1) == 1 (mod p), thus a(p)=0. If a(n)=0, then n may be only pseudoprime, as for n = 341 = 11*31 [F. Sarrus, 1820].",
				"See A001567 for the list of all pseudoprimes to base 2, i.e., composite numbers which have a(n) = 0, also called Sarrus or Poulet numbers. Carmichael numbers A002997 are pseudoprimes to all (coprime) bases b \u003e= 2. - _M. F. Hasler_, Mar 13 2020"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A128311/b128311.txt\"\u003eTable of n, a(n) for n = 1..2048\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Fermat\u0026#39;s_little_theorem\"\u003eFermat's little theorem\u003c/a\u003e"
			],
			"formula": [
				"a(n) = M(n-1) - n floor( M(n-1)/n ) = M(n-1) - max{ k in nZ | k \u003c= M(n-1) } where M(k)=2^k-1."
			],
			"example": [
				"a(1)=0 since any integer == 0 (mod 1);",
				"a(2)=1 since 2^1-1 == 1 (mod 2),",
				"a(3)=0 since 3 is a prime \u003e 2,",
				"a(4)=3 since 2^3-1 = 7 == 3 (mod 4);",
				"a(341)=0 since 341=11*31 is a Sarrus number."
			],
			"mathematica": [
				"Table[Mod[2^(n-1)-1,n],{n,100}] (* _Harvey P. Dale_, Dec 22 2012 *)"
			],
			"program": [
				"(PARI) a(n)=(1\u003c\u003c(n-1)-1)%n",
				"(PARI) apply( {A128311(n)=lift(Mod(2,n)^(n-1)-1)}, [1..99]) \\\\ Much more efficient when n becomes very large. - _M. F. Hasler_, Mar 13 2020"
			],
			"xref": [
				"Cf. A001348 (Mersenne numbers), A001567 (Sarrus numbers: pseudoprimes to base 2), A002997 (Carmichael numbers), A084653, A001220 (Wieferich primes)."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_M. F. Hasler_, May 04 2007",
			"references": 2,
			"revision": 18,
			"time": "2020-03-14T03:18:01-04:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}