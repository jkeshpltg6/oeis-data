{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A090132",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 90132,
			"data": "1,0,-2,4,-4,0,8,-16,16,0,-32,64,-64,0,128,-256,256,0,-512,1024,-1024,0,2048,-4096,4096,0,-8192,16384,-16384,0,32768,-65536,65536,0,-131072,262144,-262144,0,524288,-1048576,1048576,0,-2097152,4194304,-4194304,0,8388608",
			"name": "Expansion of (1+2*x)/(1+2*x+2*x^2).",
			"comment": [
				"The expansion of (1-2x)/(1-2x+2x^2) has a(n) = Sum_{k=0..n} C(n,k)(-1)^(-k)(-1)^floor(k/2).",
				"Pisano period lengths: 1, 1, 8, 1, 4, 8, 24, 1, 24, 4, 40, 8, 12, 24, 8, 1, 16, 24, 72, 4, ... - _R. J. Mathar_, Aug 10 2012"
			],
			"link": [
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (-2,-2)."
			],
			"formula": [
				"G.f.: (1+2*x)/(1+2*x+2*x^2).",
				"a(n) = Sum_{k=0..n} C(n, k)*(-1)^(n-k)*(-1)^floor(k/2).",
				"a(n) = sqrt(2)*2^(n/2)*sin(3*Pi*n/4+Pi/4). - _Paul Barry_, Feb 25 2004",
				"a(n) = -a(n-1) + 2*a(n-3). - _Paul Curtz_, Apr 24 2008",
				"Negated real part of (-1+i)^n, imaginary part is A108520. - _Joerg Arndt_, May 13 2011",
				"E.g.f.: A(x)=(cos(x)+sin(x))/exp(x)=Q(0); Q(k)=1-(x^2)/((4*k+1)*(2*k+1)+2*x*(4*k+1)*(2*k+1)/(4*k+3-2*x-x*(4*k+3)/(x-(4*k+4)/Q(k+1)))); (continued fraction). - _Sergei N. Gladkovskii_, Nov 28 2011",
				"a(4*n + 1) = 0. a(2*n) = A120617(n). a(4*n + 3) = (-4)^n. - _Michael Somos_, May 25 2013",
				"a(n) = - 2^(n-1)*a(2-n) for all n in Z. - _Michael Somos_, Jun 26 2017",
				"a(n) = (I + 1)*((-1 - I)^n - I*(-1 + I)^n)/2. - _Taras Goy_, Apr 20 2019"
			],
			"example": [
				"G.f. = 1 - 2*x^2 + 4*x^3 - 4*x^4 + 8*x^6 - 16*x^7 + 16*x^8 - 32*x^10 + 64*x^11 + ..."
			],
			"mathematica": [
				"a[ n_] := Re[ -(I - 1)^(n + 1)]; (* _Michael Somos_, May 25 2013 *)",
				"a[ n_] := If[ n \u003c 0, - 2^(n-1) a[2 - n], SeriesCoefficient[ (1 + 2 x) / (1 + 2 x + 2 x^2), {x, 0, n}]; (* _Michael Somos_, May 25 2013 *)",
				"a[ n_] := If[ n \u003c 0, - 2^(n-1) a[2 - n], n! SeriesCoefficient[ (Cos[x] + Sin[x]) / Exp[x], {x, 0, n}]]; (* _Michael Somos_, May 25 2013 *)",
				"a[ n_] := Simplify[ -2 Sqrt[2]^(n - 1) ChebyshevT[ n + 1, -1 / Sqrt[2]]]; (* _Michael Somos_, May 25 2013 *)",
				"LinearRecurrence[{-2,-2},{1,0},50] (* _Harvey P. Dale_, Oct 23 2017 *)"
			],
			"program": [
				"(PARI) x='x+O('x^66) /* that many terms */",
				"Vec(serlaplace(cos(x)/exp(x))) /* show terms */",
				"/* or */",
				"vector(66, n, -real((-1+I)^n)) /* _Joerg Arndt_, May 13 2011 */",
				"(PARI) {a(n) = real( -(I - 1)^(n + 1) )}; /* _Michael Somos_, May 25 2013 */",
				"(PARI) {a(n) = if( n\u003c0, - 2^(n-1) * a(2 - n), polcoeff( (1 + 2*x) / (1 + 2*x + 2*x^2) + x * O(x^n), n))}; /* _Michael Somos_, May 25 2013 */",
				"(PARI) {a(n) = my(A); if( n\u003c0, - 2^(n-1) * a(2 - n), A = x * O(x^n); n! * polcoeff( (cos(x + A) + sin(x + A)) / exp(x + A), n))}; /* _Michael Somos_, May 25 2013 */",
				"(PARI) {a(n) = simplify( -2 * quadgen(8)^(n - 1) * polchebyshev( n + 1, 1, -1 / quadgen(8)))}; /* _Michael Somos_, May 25 2013 */"
			],
			"xref": [
				"Cf. A009116.",
				"Cf. A135353, A137444, A137426, A137429."
			],
			"keyword": "easy,sign",
			"offset": "0,3",
			"author": "_Paul Barry_, Nov 21 2003",
			"references": 8,
			"revision": 42,
			"time": "2019-04-20T03:36:44-04:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}