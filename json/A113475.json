{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A113475",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 113475,
			"data": "1,3,5,2,4,2,2,4,2,4,3,2,3,4,2,2,1,1,2,1,5,1,7,1,5,4,2,2,3,3,2,11,5,10,4,2,2,6,14,4,6,2,3,9,14,10,3,3,4,2,1,5,4,16,8,9,5,8,14,6,2,2,26,8,30,4,5,1,4,2,22,36,20,2,10,2,15,3,18,6,15",
			"name": "a(1)=1 and a(n) for n\u003e1 has the smallest positive value such that Sum_{i=1..n} a(i)^a(n-i+1) is semiprime (A001358).",
			"comment": [
				"Previous name was: Least integers so ascending descending base exponent transforms all semiprime.",
				"Semiprime analogy to A113320. The sequence is probably infinite, but it is hard to characterize the asymptotic cost of adding an n-th term. The ascending descending base exponent transform of semiprimes is A113173.",
				"The sequence is infinite because a(n) is the minimum k such that a(1)^k + k^a(1) + Sum_{i=2..n-1} a(i)^a(n-i+1) is semiprime, and since a(1)=1 this is equal to 1+k+T where T does not depend on k, thus k is the smallest positive value that makes 1+k+T semiprime, which exists because semiprimes are infinite. - _Giovanni Resta_, Jan 03 2020"
			],
			"formula": [
				"a(1) = 1. For n\u003e1, a(n) = min {k\u003e0: a(1)^k + k^a(1) + Sum_{i=2..n-1} a(i)^a(n-i+1) is in A001358}."
			],
			"example": [
				"a(1) = 1 by definition.",
				"a(2) = 3 because 3 is the min x such that 1^x + x^1 is semiprime, i.e., 1^3 + 3^1 = 4 = 2*2.",
				"a(3) = 5 because 1^5 + 3^3 + 5^1 = 33 = 3 * 11 is semiprime.",
				"a(4) = 2 because 1^2 + 3^5 + 5^3 + 2^1 = 371 = 7 * 53.",
				"a(5) = 4 because 1^4 + 3^2 + 5^5 + 2^3 + 4^1 = 3147 = 3 * 1049.",
				"a(6) = 2 because 1^2 + 3^4 + 5^2 + 2^5 + 4^3 + 2^1 = 205 = 5 * 41.",
				"a(7) = 2 because 1^2 + 3^2 + 5^4 + 2^2 + 4^5 + 2^3 + 2^1 = 1673 = 7 * 239.",
				"a(8) = 4 because 1^4 + 3^2 + 5^2 + 2^4 + 4^2 + 2^5 + 2^3 + 4^1 = 111 = 3 * 37."
			],
			"mathematica": [
				"semipQ[n_] := PrimeOmega[n] == 2; inve[w_] := Total[w^Reverse[w]]; a[1] = 1; a[n_] := a[n] = Block[{k = 0}, While[! semipQ[ inve@ Append[ Array[a, n - 1], ++k]]]; k]; Array[a, 81] (* _Giovanni Resta_, Jun 13 2016 *)"
			],
			"program": [
				"(PARI) lista(n)={my(a=vector(n)); a[1]=1; print1(1, \", \"); for(n=2, #a, my(t=sum(i=2, n-1, a[i]^a[n-i+1])); my(k=1); while(2!=bigomega(t+1+k), k++); a[n]=k; print1(k, \", \"))} \\\\ _Andrew Howroyd_, Jan 03 2020"
			],
			"xref": [
				"Cf. A001358, A005408, A113122, A113153, A113154, A113336, A113320, A113271, A113258, A113257, A113231, A087316, A113208."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Jonathan Vos Post_, Jan 08 2006",
			"ext": [
				"Corrected and extended by _Giovanni Resta_, Jun 13 2016"
			],
			"references": 1,
			"revision": 19,
			"time": "2020-01-05T06:13:08-05:00",
			"created": "2006-01-24T03:00:00-05:00"
		}
	]
}