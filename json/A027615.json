{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A027615",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 27615,
			"data": "0,1,2,3,1,2,3,4,2,3,4,5,3,4,2,3,1,2,3,4,2,3,4,5,3,4,5,6,4,5,3,4,2,3,4,5,3,4,5,6,4,5,6,7,5,6,4,5,3,4,5,6,4,5,3,4,2,3,4,5,3,4,2,3,1,2,3,4,2,3,4,5,3,4,5,6,4,5,3,4,2",
			"name": "Number of 1's when n is written in base -2.",
			"comment": [
				"Base -2 is also called \"negabinary\".",
				"From _Jianing Song_, Oct 18 2018: (Start)",
				"Define f(n) as: f(0) = 0, f(-2*n) = f(n), f(-2*n+1) = f(n) + 1, then a(n) = f(n), n \u003e= 0. See A320642 for the other half of f.",
				"For k \u003e 0, the earliest occurrence of k is n = A305750(k).",
				"Conjecture: a(n) != A053737(n) if and only if there exists even k \u003e= 4 such that n mod 2^k \u003e= (5*2^(k+1) + 2)/3. If this holds, then the probability of a random chosen number n to satisfy a(n) != A053737(n) is 1/6. (End)"
			],
			"reference": [
				"J.-P. Allouche and J. Shallit, Automatic Sequences, Cambridge Univ. Press, 2003, p. 164."
			],
			"link": [
				"Jianing Song, \u003ca href=\"/A027615/b027615.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Negabinary.html\"\u003eNegabinary.\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Negative_base\"\u003eNegative base\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 3*A072894(n+1) - 2*n - 3. Proof by Nikolaus Meyberg, following a conjecture by _Ralf Stephan_. - _R. J. Mathar_, Jan 11 2013",
				"a(n) == n (mod 3). - _Jianing Song_, Oct 18 2018",
				"a(n) = A000120(A005351(n)). - _Michel Marcus_, Oct 23 2018"
			],
			"example": [
				"A039724(7) = 11011 which has four 1's, so a(7) = 4."
			],
			"program": [
				"(PARI) a(n) = if(n==0, 0, a(n\\(-2))+n%2) /* _Jianing Song_, Oct 18 2018 */"
			],
			"xref": [
				"Cf. A000120, A005351, A039724, A053737, A072894, A305750, A320642."
			],
			"keyword": "nonn,base",
			"offset": "0,3",
			"author": "_Pontus von Brömssen_, Nov 14 1997",
			"references": 12,
			"revision": 32,
			"time": "2018-11-05T04:42:44-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}