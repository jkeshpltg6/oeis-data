{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A255974",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 255974,
			"data": "0,1,3,-1,3,6,-3,1,6,-1,6,10,-3,10,-3,1,10,-1,10,15,-6,3,-1,15,-3,15,-3,1,15,-1,15,21,-6,1,21,-6,3,-1,21,-3,21,-3,1,21,-1,21,28,-6,28,-6,1,28,-6,3,-1,28,-3,28,-3,1,28,-1,28,36,-10,3,36,-6,36",
			"name": "R(k), the minimal alternating triangular-number representation of k, concatenated for k = 0, 1, 2,....",
			"comment": [
				"Suppose that b = (b(0), b(1), ... ) is an increasing sequence of positive integers satisfying b(0) = 1 and b(n+1) \u003c= 2*b(n) for n \u003e= 0.  Let B(n) be the least b(m) \u003e= n.  Let R(0) = 1, and for n \u003e 0, let R(n) = B(n) - R(B(n) - n).  The resulting sum of the form R(n) = B(n) - B(m(1)) + B(m(2)) - ... + ((-1)^k)*B(k) is the minimal alternating b-representation of n.  The sum B(n) + B(m(2)) + ... is the positive part of R(n), and the sum B(m(1)) + B(m(3)) + ... , the nonpositive part of R(n).  The number ((-1)^k)*B(k) is the trace of n.  If b(n) = n(n+1)/2, the n-th triangular number, then the sum R(n) is the minimal alternating triangular-number representation of n."
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A255974/b255974.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e"
			],
			"example": [
				"R(0) = 0",
				"R(1) = 1",
				"R(2) = 3 - 1",
				"R(3) = 3",
				"R(4) = 6 - 3 + 1",
				"R(5) = 6 - 1",
				"R(8) = 10 - 3 + 1",
				"R(11) = 15 - 6 + 3 - 1"
			],
			"mathematica": [
				"b[n_] := n (n + 1)/2; bb = Table[b[n], {n, 0, 1000}];",
				"s[n_] := Table[b[n], {k, 1, n}];",
				"h[1] = {1}; h[n_] := Join[h[n - 1], s[n]];",
				"g = h[100]; r[0] = {0};",
				"r[n_] := If[MemberQ[bb, n], {n}, Join[{g[[n]]}, -r[g[[n]] - n]]]",
				"t = Table[r[n], {n, 0, 120}]  (* A255974 actual representations *)",
				"Flatten[t]  (* A255974 sequence *)"
			],
			"xref": [
				"Cf. A000217, A256655 (Fibonacci based), A256696 (binary), A256789 (squares)."
			],
			"keyword": "easy,sign",
			"offset": "0,3",
			"author": "_Clark Kimberling_, Apr 11 2015",
			"references": 10,
			"revision": 4,
			"time": "2015-04-13T09:38:49-04:00",
			"created": "2015-04-13T09:38:49-04:00"
		}
	]
}