{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006874",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6874,
			"id": "M0535",
			"data": "1,1,2,3,4,6,6,9,10,12,10,22,12,18,24,27,16,38,18,44,36,30,22,78,36,36,50,66,28,104,30,81,60,48,72,158,36,54,72,156,40,156,42,110,152,66,46,270,78,140,96,132,52,230,120,234,108,84,58,456,60,90,228,243,144,260",
			"name": "Mu-atoms of period n on continent of Mandelbrot set.",
			"reference": [
				"B. B. Mandelbrot, The Fractal Geometry of Nature, Freeman, NY, 1982, p. 183.",
				"R. Penrose, The Emperor's New Mind, Penguin Books, NY, 1991, p. 138.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A006874/b006874.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"R. P. Munafo, \u003ca href=\"http://www.mrob.com/pub/muency.html\"\u003eMu-Ency - The Encyclopedia of the Mandelbrot Set\u003c/a\u003e",
				"F. V. Weinstein, \u003ca href=\"https://arxiv.org/abs/math/0307150\"\u003eNotes on Fibonacci partitions\u003c/a\u003e, arXiv:math/0307150 [math.NT], 2003-2018."
			],
			"formula": [
				"a(n) = Sum_{ d divides n, d\u003cn} phi(n/d)*a(d), n\u003e1, a(1)=1, where phi is Euler totient function (A000010). - _Vladeta Jovovic_, Feb 09 2002",
				"a(1)=1; for n \u003e 1, a(n) = Sum_{k=1..n-1} a(gcd(n,k)). - _Reinhard Zumkeller_, Sep 25 2009",
				"G.f. A(x) satisfies: A(x) = x + Sum_{k\u003e=2} phi(k) * A(x^k). - _Ilya Gutkovskiy_, Sep 04 2019"
			],
			"example": [
				"a(1)  = 1;",
				"a(2)  = a(1);",
				"a(3)  = 2*a(1);",
				"a(4)  = 2*a(1) + a(2);",
				"a(5)  = 4*a(1);",
				"a(6)  = 2*a(1) + 2*a(2) + a(3);",
				"a(7)  = 6*a(1);",
				"a(8)  = 4*a(1) + 2*a(2) + a(4);",
				"a(9)  = 6*a(1) + 2*a(3);",
				"a(10) = 4*a(1) + 4*a(2) + a(5);",
				"a(11) = 10*a(1);",
				"a(12) = 4*a(1) + 2*a(2) + 2*a(3) + 2*a(4) + a(6); ..."
			],
			"mathematica": [
				"a[1] = 1; a[n_] := a[n] = Block[{d = Most@Divisors@n}, Plus @@ (EulerPhi[n/d]*a /@ d)]; Array[a, 66] (* _Robert G. Wilson v_, Nov 22 2005 *)"
			],
			"program": [
				"(PARI) a(n) = if (n==1, 1, sumdiv(n, d, if (d==1, 0, a(n/d)*eulerphi(d)))); \\\\ _Michel Marcus_, Apr 19 2014",
				"(Python)",
				"from sympy import divisors, totient",
				"l=[0, 1]",
				"for n in range(2, 101):",
				"    l.append(sum([totient(n//d)*l[d] for d in divisors(n)[:-1]]))",
				"print(l[1:]) # _Indranil Ghosh_, Jul 12 2017",
				"(MAGMA) sol:=[1]; for n in [2..66] do Append(~sol,\u0026+[sol[Gcd(n,k)]:k in [1..n-1]]); end for; sol; // _Marius A. Burtea_, Sep 05 2019"
			],
			"xref": [
				"Cf. A006875, A006876."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Robert Munafo_, Apr 28 1994",
			"ext": [
				"More terms from _Vladeta Jovovic_, Feb 09 2002"
			],
			"references": 11,
			"revision": 49,
			"time": "2021-03-09T19:11:23-05:00",
			"created": "1994-04-28T03:00:00-04:00"
		}
	]
}