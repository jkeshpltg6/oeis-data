{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A214314",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 214314,
			"data": "1,1,2,1,2,3,1,2,4,5,1,2,4,6,7,1,2,5,8,10,11,1,2,5,9,12,14,15,1,2,6,11,16,19,21,22,1,2,6,13,19,24,27,29,30,1,2,7,15,24,31,36,39,41,42,1,2,7,17,28,38,45,50,53,55,56,1,2,8,20,35,48,59,66,71,74,76,77",
			"name": "Number triangle with entry T(n,m) giving the position of the first partition of n with m parts in the Abramowitz-Stegun (A-St) partition ordering.",
			"comment": [
				"For the Abramowitz-Stegun ordering of partitions see A036036 for the reference and a C. F. Hindenburg link.",
				"The present triangle is the partial sum triangle of the triangle t(n,k) = 0 if 0 \u003c= n -1 \u003c  k , t(n,0) = 1, n \u003e= 1 and t(n,k) = A008284(n,k) if n-1 \u003e=  k \u003e= 1. This triangle with offset [1,0] for [n,k] is 1; 1,1; 1,1,1; 1,1,2,1; 1,1,2,2,1; 1,1,3,3,2,1;... (erase the diagonal of A008284 and add instead a column k=0 with only 1's). See the example section."
			],
			"formula": [
				"T(n,m) = sum(p(n,k),k=0..m-1) if n \u003e= m \u003e= 1, otherwise 0, with p(n,0) :=1 and p(n,k) = A008284(n,k) for k=1,2,...,n-1."
			],
			"example": [
				"T(n,m) starts with:",
				"n\\m   1  2  3   4   5   6   7   8   9  10  11  12...",
				"1     1",
				"2     1  2",
				"3     1  2  3",
				"4     1  2  4   5",
				"5     1  2  4   6   7",
				"6     1  2  5   8  10  11",
				"7     1  2  5   9  12  14  15",
				"8     1  2  6  11  16  19  21  22",
				"9     1  2  6  13  19  24  27  29  30",
				"10    1  2  7  15  24  31  36  39  41  42",
				"11    1  2  7  17  28  38  45  50  53  55  56",
				"12    1  2  8  20  35  48  59  66  71  74  76  77",
				"...",
				"T(6,4) = 8 because the 11=T(6,6) partitions for n=6 are, in A-St order: [6]; [1,5],[2,4],[3,3]; [1^2,4],[1,2,3],[2^3]; [1^3,3],[1^2,2^2]; [1^4,2]; [1^6] and the first partition with 4 parts, appears at position 8.",
				"This triangle is obtained as partial sum triangle from the triangle t(n,k) (see the comment section) which starts with:",
				"n\\m   0  1  2   3   4   5   6  7  8  9 10 11 ...",
				"1     1",
				"2     1  1",
				"3     1  1  1",
				"4     1  1  2   1",
				"5     1  1  2   2   1",
				"6     1  1  3   3   2   1",
				"7     1  1  3   4   3   2   1",
				"8     1  1  4   5   5   3   2  1",
				"9     1  1  4   7   6   5   3  2  1",
				"10    1  1  5   8   9   7   5  3  2  1",
				"11    1  1  5  10  11  10   7  5  3  2  1",
				"12    1  1  6  12  15  13  11  7  5  3  2  1",
				"..."
			],
			"xref": [
				"Cf. A008284."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Wolfdieter Lang_, Jul 24 2012",
			"references": 6,
			"revision": 13,
			"time": "2017-05-14T12:21:11-04:00",
			"created": "2012-07-24T14:51:14-04:00"
		}
	]
}