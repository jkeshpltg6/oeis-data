{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A153810",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 153810,
			"data": "4,2,2,7,8,4,3,3,5,0,9,8,4,6,7,1,3,9,3,9,3,4,8,7,9,0,9,9,1,7,5,9,7,5,6,8,9,5,7,8,4,0,6,6,4,0,6,0,0,7,6,4,0,1,1,9,4,2,3,2,7,6,5,1,1,5,1,3,2,2,7,3,2,2,2,3,3,5,3,2,9,0,6,3,0,5,2,9,3,6,7,0,8,2,5,3,2,5,0,4,8,5,3,6,8",
			"name": "Decimal expansion of 1 - gamma, where gamma is Euler's constant (or the Euler-Mascheroni constant).",
			"comment": [
				"Average fractional part of a random (large) integer when divided by all numbers up to it. The result remains true if primes or numbers from particular (fixed) congruence classes are used instead. The result is due to Vallée Poussin. - _Charles R Greathouse IV_, Apr 11 2012",
				"Expected value of the fractional part of 1/x where x is chosen uniformly at random from (0, 1]. - _Charles R Greathouse IV_, Apr 11 2012",
				"Value of digamma function psi(x) for x=2. - _Stanislav Sykora_, Apr 30 2012",
				"The asymptotic evaluation of the counting function of A064052 (\"jagged\" numbers) is j(n) ~ log(2)*n - (1-gamma)*n/log(n) + ... - _Jean-François Alcover_, May 16 2014, after _Steven Finch_.",
				"Letting eta denote the Dirichlet eta function, and letting zeta denote the Riemann zeta function, we have that 1-gamma is equal to lim x -\u003e infinity 2^x+(4/3)^x-zeta(2-eta(x)). - _John M. Campbell_, Jan 28 2016"
			],
			"reference": [
				"Steven R. Finch, Mathematical Constants, Cambridge University Press, 2003, chapter 2.21, p. 166."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A153810/b153810.txt\"\u003eTable of n, a(n) for n = 0..2000\u003c/a\u003e",
				"Friedrich Pillichshammer, \u003ca href=\"http://www.dmg.tuwien.ac.at/nfn/gamma.pdf\"\u003eEuler's constant and averages of fractional parts\u003c/a\u003e.",
				"Charles Jean de la Vallée Poussin, \u003ca href=\"https://oeis.org/wiki/File:Sur_les_valeurs_moyennes_de_certaines_fonctions_arithm%C3%A9tiques.pdf\"\u003eSur les valeurs moyennes de certaines fonctions arithmétiques\u003c/a\u003e, Annales de la société scientifique de Bruxelles 22 (1898), pp. 84-90.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Digamma_function\"\u003eDigamma function\u003c/a\u003e."
			],
			"formula": [
				"Equals Integral_{x\u003e=1} {x}dx/x^2 dx, where {x} is the fractional part of x. - _Charles R Greathouse IV_, Apr 11 2012",
				"Equals Integral_{x\u003e=0} x*log(x)*exp(-x) dx. - _Jean-François Alcover_, Jun 17 2013",
				"Equals Sum_{n\u003e=2} (zeta(n)-1)/n. - _Vaclav Kotesovec_, Dec 11 2015",
				"Equals Sum_{k\u003e=1} zeta(2*k+1)/((k+1)*(2*k+1)). - _Amiram Eldar_, May 24 2021"
			],
			"example": [
				"0.422784335..."
			],
			"mathematica": [
				"RealDigits[N[PolyGamma[2], 105]][[1]] (* _Arkadiusz Wesolowski_, Jan 10 2013 *)",
				"RealDigits[1 - EulerGamma, 10, 50][[1]] (* _G. C. Greubel_, Aug 29 2016 *)"
			],
			"program": [
				"(PARI) 1-Euler \\\\ _Charles R Greathouse IV_, Apr 11 2012"
			],
			"xref": [
				"Cf. A001620."
			],
			"keyword": "cons,nonn,nice",
			"offset": "0,1",
			"author": "_Omar E. Pol_, Jan 28 2009",
			"ext": [
				"More digits from _R. J. Mathar_, Feb 06 2009"
			],
			"references": 7,
			"revision": 41,
			"time": "2021-05-24T07:41:23-04:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}