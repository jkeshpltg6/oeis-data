{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A053250",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 53250,
			"data": "1,1,0,-1,1,1,-1,-1,0,2,0,-2,1,1,-1,-2,1,3,-1,-2,1,2,-2,-3,1,4,0,-4,2,3,-2,-4,1,5,-2,-5,3,5,-3,-5,2,7,-2,-7,3,6,-4,-8,3,9,-2,-9,5,9,-5,-10,3,12,-4,-12,5,11,-6,-13,6,16,-6,-15,7,15,-8,-17,7,19,-6,-20,9,19,-10,-22,8,25,-9,-25,12,25,-12,-27,11,31",
			"name": "Coefficients of the '3rd order' mock theta function phi(q).",
			"reference": [
				"N. J. Fine, Basic Hypergeometric Series and Applications, Amer. Math. Soc., 1988; p. 55, Eq. (26.12), p. 58, Eq. (26.56).",
				"Srinivasa Ramanujan, Collected Papers, Chelsea, New York, 1962, pp. 354-355",
				"Srinivasa Ramanujan, The Lost Notebook and Other Unpublished Papers, Narosa Publishing House, New Delhi, 1988, pp. 17, 31"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A053250/b053250.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Leila A. Dragonette, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9947-1952-0049927-8\"\u003eSome asymptotic formulas for the mock theta series of Ramanujan\u003c/a\u003e, Trans. Amer. Math. Soc., 72 (1952) 474-500",
				"John F. R. Duncan, Michael J. Griffin and Ken Ono, \u003ca href=\"http://arxiv.org/abs/1503.01472\"\u003eProof of the Umbral Moonshine Conjecture\u003c/a\u003e, arXiv:1503.01472 [math.RT], 2015.",
				"A. Folsom, K. Ono and R. C. Rhoades, \u003ca href=\"http://math.stanford.edu/~rhoades/FILES/ramanujans_radial.pdf\"\u003eRamanujan's radial limits, 2013\u003c/a\u003e. - From _N. J. A. Sloane_, Feb 09 2013",
				"George N. Watson, \u003ca href=\"http://jlms.oxfordjournals.org/content/s1-11/1/55.extract\"\u003eThe final problem: an account of the mock theta functions\u003c/a\u003e, J. London Math. Soc., 11 (1936) 55-80."
			],
			"formula": [
				"Consider partitions of n into distinct odd parts. a(n) = number of them for which the largest part minus twice the number of parts is == 3 (mod 4) minus the number for which it is == 1 (mod 4).",
				"a(n) = (-1)^n*(A027358(n)-A027357(n)). - _Vladeta Jovovic_, Mar 12 2006",
				"G.f.: 1 + Sum_{k\u003e0} x^k^2 / ((1 + x^2) (1 + x^4) ... (1 + x^(2*k))).",
				"G.f. 1 + Sum_{n \u003e= 0} x^(2*n+1)*Product_{k = 1..n} (x^(2*k-1) - 1) (Folsom et al.). Cf. A207569 and A215066. - _Peter Bala_, May 16 2017"
			],
			"example": [
				"G.f. = 1 + x - x^3 + x^4 + x^5 - x^6 - x^7 + 2*x^9 - 2*x^11 + x^12 + x^13 - x^14 + ..."
			],
			"maple": [
				"f:=n-\u003eq^(n^2)/mul((1+q^(2*i)),i=1..n); add(f(n),n=0..10);"
			],
			"mathematica": [
				"Series[Sum[q^n^2/Product[1+q^(2k), {k, 1, n}], {n, 0, 10}], {q, 0, 100}]",
				"a[ n_] := SeriesCoefficient[ Sum[ x^k^2 / QPochhammer[ -x^2, x^2, k], {k, 0, Sqrt@ n}], {x, 0, n}]; (* _Michael Somos_, Jul 09 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = my(t); if(n\u003c0, 0, t = 1 + O(x^n); polcoeff( sum(k=1, sqrtint(n), t *= x^(2*k - 1) / (1 + x^(2*k)) + O(x^(n - (k-1)^2 + 1)), 1), n))}; /* _Michael Somos_, Jul 16 2007 */"
			],
			"xref": [
				"Other '3rd order' mock theta functions are at A000025, A053251, A053252, A053253, A053254, A053255.",
				"Cf. A207569, A215066."
			],
			"keyword": "sign,easy",
			"offset": "0,10",
			"author": "_Dean Hickerson_, Dec 19 1999",
			"references": 19,
			"revision": 37,
			"time": "2018-01-09T08:41:10-05:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}