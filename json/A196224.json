{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A196224",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 196224,
			"data": "12,15,19,44,51,63,76,83,108,112,115,140,143,147,172,179,204,211,236,240,243,255,268,271,275,300,307,332,339,364,368,371,396,399,403,428,435,448,460,467,492,496,499,524,527,531,556,563,575,588,595,620,624",
			"name": "Numbers n such that n^2 + n is not of the form x^2 + y^2 + z^2.",
			"comment": [
				"Nick Herbert calls these \"Sirag Numbers\" after Saul-Paul Sirag. Initially the idea arose by considering the quantum operators for spin or angular momentum, where J^2 = J[x]^2 + J[y]^2 + J[z]^2 = ħ^2 j(j+1), see link.",
				"32n + 12 and 32n + 19 are members for all nonnegative n. All members are in {0, 12, 15, 16, 19, 31} mod 32. - _Charles R Greathouse IV_, Sep 29 2011",
				"As noted in A004215, n is in the sequence iff n^2+n is of the form 4^i * (8*j+7).",
				"Express J*(J+1) in base 4.  If the last two nonzero digits are either 13 or 33, J is a Sirag number. - _Jack Brennen_, Sep 30 2011",
				"n is in this sequence iff n == 12 or 19 (mod 32), n == 4^j*(8k+7), where j \u003e= 2, or n == 4^j*(8k+1)-1, where j \u003e= 2, k \u003e= 0. - _David W. Wilson_, Oct 21 2011, (clarified by _Mauro Fiorentini_, May 11 2017)",
				"Also n == 4^j - 1, where j \u003e= 2 - _Mauro Fiorentini_, May 11 2017"
			],
			"link": [
				"Charles R Greathouse IV, \u003ca href=\"/A196224/b196224.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Nick Herbert, \u003ca href=\"http://quantumtantra.blogspot.com/2011/08/sirag-numbers.html\"\u003eThe Sirag Numbers\u003c/a\u003e, Aug 27 2011.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Quantum_number#Quantum_numbers_with_spin-orbit_interaction\"\u003eQuantum numbers with spin-orbit interaction\u003c/a\u003e."
			],
			"formula": [
				"16/3 * n \u003c a(n) \u003c 16n. - _Charles R Greathouse IV_, Sep 29 2011",
				"a(n) = 12n + O(log(n)). - _David W. Wilson_, Oct 21 2011"
			],
			"mathematica": [
				"siragQ[n_]:=Module[{b4=IntegerDigits[n(n+1),4]},While[Last[b4]==0, b4= Drop[b4,-1]];MemberQ[{{1,3},{3,3}},Take[b4,-2]]]; Select[Range[650], siragQ] (* _Harvey P. Dale_, relying on _Jack Brennen_'s comment, Oct 01 2011 *)"
			],
			"program": [
				"(PARI) is_A196224(n)={ (n*=n+1)\\4^valuation(n,4)%8==7 } \\\\ _M. F. Hasler_, Sep 29 2011"
			],
			"xref": [
				"Cf. A004215, A002378."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_M. F. Hasler_, Sep 29 2011",
			"references": 2,
			"revision": 55,
			"time": "2017-05-11T13:51:24-04:00",
			"created": "2011-09-29T16:39:04-04:00"
		}
	]
}