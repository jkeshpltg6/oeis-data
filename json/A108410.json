{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A108410",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 108410,
			"data": "1,2,1,5,5,2,14,21,15,5,42,84,84,49,14,132,330,420,336,168,42,429,1287,1980,1980,1350,594,132,1430,5005,9009,10725,9075,5445,2145,429,4862,19448,40040,55055,55055,40898,22022,7865,1430,16796,75582",
			"name": "Triangle T(n,k) read by rows: number of 12312-avoiding matchings on [2n] with exactly k crossings (n \u003e= 1, 0 \u003c= k \u003c= n-1).",
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A108410/b108410.txt\"\u003eTable of n, a(n) for n = 1..1275\u003c/a\u003e",
				"Alexander Burstein, Megan Martinez, \u003ca href=\"https://permutationpatterns.com/files/2020/06/WednesdayA_Burstein.pdf\"\u003ePattern classes equinumerous to the class of ternary forests\u003c/a\u003e, Permutation Patterns Virtual Workshop, Howard University (2020).",
				"W. Y. C. Chen, T. Mansour and S. H. F. Yan, \u003ca href=\"https://arxiv.org/abs/math/0504342\"\u003eMatchings avoiding partial patterns\u003c/a\u003e, arXiv:math/0504342 [math.CO], 2005.",
				"W. Y. C. Chen, T. Mansour and S. H. F. Yan, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v13i1r112\"\u003eMatchings avoiding partial patterns\u003c/a\u003e, The Electronic Journal of Combinatorics 13, 2006, #R112, Theorem 2.2.",
				"D. S. Hough, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v10i1n13\"\u003eDescents in noncrossing trees\u003c/a\u003e, Electronic J. Combinatorics 10 (2003), #N13, Theorem 2.2. [_Ira M. Gessel_, May 10 2010]"
			],
			"formula": [
				"T(n, k) = Sum_{i=n..2*n-1} (-1)^(n+k+i)/i*C(i, n)*C(3*n, i+1+n)*C(i-n, k).",
				"T(n,k) = C(n-1+k,n-1)*C(2*n-k,n+1)/n, (0 \u003c= k \u003c= n-1). [Chen et al.] - _Emeric Deutsch_, Dec 19 2006",
				"O.g.f. equals the series reversion with respect to x of x*(1 + x*(1 - t))/(1 + x)^3. If R(n,t) is the n-th row polynomial of this triangle then R(n, 1+t) is the n-th row polynomial of A089434. - _Peter Bala_, Jul 15 2012"
			],
			"example": [
				"Triangle begins",
				"     1;",
				"     2,     1;",
				"     5,     5,     2;",
				"    14,    21,    15,     5;",
				"    42,    84,    84,    49,    14;",
				"   132,   330,   420,   336,   168,    42;",
				"   429,  1287,  1980,  1980,  1350,   594,   132;",
				"  1430,  5005,  9009, 10725,  9075,  5445,  2145,  429;",
				"  4862, 19448, 40040, 55055, 55055, 40898, 22022, 7865, 1430;"
			],
			"maple": [
				"T:=(n,k)-\u003ebinomial(n-1+k,n-1)*binomial(2*n-k,n+1)/n: for n from 1 to 10 do seq(T(n,k),k=0..n-1) od; # yields sequence in triangular form - _Emeric Deutsch_, Dec 19 2006"
			],
			"mathematica": [
				"T[n_, k_] := Binomial[n + k - 1, n - 1]*Binomial[2*n - k, n + 1]/n;",
				"Table[T[n, k], {n, 1, 10}, {k, 0, n - 1}] // Flatten (* _Jean-François Alcover_, Nov 11 2017, after _Emeric Deutsch_ *)"
			],
			"program": [
				"(PARI) T(n,k) = binomial(n-1+k, n-1)*binomial(2*n-k, n+1)/n; \\\\ _Andrew Howroyd_, Nov 06 2017"
			],
			"xref": [
				"Left-hand columns include A000108 and A002054. Right-hand columns include A000108 and A007851+1. Row sums are A001764. A089434."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Ralf Stephan_, Jun 03 2005",
			"references": 3,
			"revision": 32,
			"time": "2020-09-24T03:33:25-04:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}