{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A298933",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 298933,
			"data": "1,2,3,4,4,6,5,6,6,4,8,6,9,6,6,12,8,12,8,8,9,8,12,6,8,14,12,12,8,12,13,12,18,8,8,12,16,14,12,12,16,12,13,14,6,20,16,18,8,10,18,16,20,12,16,16,15,20,12,18,24,14,18,8,16,18,16,22,12,12,20,24",
			"name": "Expansion of f(x, x^2) * f(x, x^3) * f(x^2, x^4) in powers of x where f(, ) is Ramanujan's general theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A298933/b298933.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of phi(x) * phi(-x^3) * phi(-x^6) / chi(-x^2)^3 in powers of x where phi(), chi() are Ramanujan theta functions.",
				"Expansion of q^(-1/4) * eta(q^2)^2 * eta(q^3)^2 * eta(q^4) * eta(q^6) / (eta(q)^2 * eta(q^12)) in powers of q.",
				"Euler transform of period 12 sequence [2, 0, 0, -1, 2, -3, 2, -1, 0, 0, 2, -3, ...].",
				"a(n) = A298932(2*n)."
			],
			"example": [
				"G.f. = 1 + 2*x + 3*x^2 + 4*x^3 + 4*x^4 + 6*x^5 + 5*x^6 + 6*x^7 + 6*x^8 + ...",
				"G.f. = q + 2*q^5 + 3*q^9 + 4*q^13 + 4*q^17 + 6*q^21 + 5*q^25 + 6*q^29 + ..."
			],
			"maple": [
				"N:= 100:",
				"S:= series(JacobiTheta3(0,x)*JacobiTheta4(0,x^3)*JacobiTheta4(0,x^6)*expand(QDifferenceEquations:-QPochhammer(-x^2,x^2,floor(N/2)))^3, x, N+1):",
				"seq(coeff(S,x,j),j=0..N); # _Robert Israel_, Jan 29 2018"
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 3, 0, x] EllipticTheta[ 4, 0, x^3] EllipticTheta[ 4, 0, x^6] QPochhammer[ -x^2, x^2]^3, {x, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A)^2 * eta(x^3 + A)^2 * eta(x^4 + A) * eta(x^6 + A) / (eta(x + A)^2 * eta(x^12 + A)), n))};"
			],
			"xref": [
				"Cf. A298932."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Michael Somos_, Jan 29 2018",
			"references": 2,
			"revision": 11,
			"time": "2021-03-12T22:24:48-05:00",
			"created": "2018-01-29T15:24:07-05:00"
		}
	]
}