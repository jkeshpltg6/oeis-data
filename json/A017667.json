{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A017667",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 17667,
			"data": "1,5,10,21,26,25,50,85,91,13,122,35,170,125,52,341,290,455,362,273,500,305,530,425,651,425,820,75,842,13,962,1365,1220,725,52,637,1370,905,1700,221,1682,625,1850,1281,2366,1325,2210,1705,2451,651,2900,1785",
			"name": "Numerator of sum of -2nd powers of divisors of n.",
			"comment": [
				"Sum_{d|n} 1/d^k is equal to sigma_k(n)/n^k. So sequences A017665-A017712 also give the numerators and denominators of sigma_k(n)/n^k for k = 1..24. The power sums sigma_k(n) are in sequences A000203 (k=1), A001157-A001160 (k=2,3,4,5), A013954-A013972 for k = 6,7,...,24. - Ahmed Fares (ahmedfares(AT)my-deja.com), Apr 05 2001",
				"C. Defant proves that there are no positive integers n such that sigma_{-2}(n) lies in (Pi^2/8, 5/4). See arxiv link. - _Michel Marcus_, Aug 24 2018"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A017667/b017667.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Colin Defant, \u003ca href=\"https://arxiv.org/abs/1506.05432\"\u003eOn the Density of Ranges of Generalized Divisor Functions\u003c/a\u003e, arXiv:1506.05432 [math.NT], 2015."
			],
			"formula": [
				"Dirichlet g.f.: zeta(s)*zeta(s+2) [for fraction A017667/A017668]. - _Franklin T. Adams-Watters_, Sep 11 2005",
				"Numerators of coefficients in expansion of Sum_{k\u003e=1} x^k/(k^2*(1 - x^k)). - _Ilya Gutkovskiy_, May 24 2018"
			],
			"example": [
				"1, 5/4, 10/9, 21/16, 26/25, 25/18, 50/49, 85/64, 91/81, 13/10, 122/121, 35/24, 170/169, ..."
			],
			"mathematica": [
				"Table[Numerator[DivisorSigma[-2, n]], {n, 50}] (* _Vladimir Joseph Stephan Orlovsky_, Jul 21 2011 *)",
				"Table[Numerator[DivisorSigma[2, n]/n^2], {n, 1, 50}] (* _G. C. Greubel_, Nov 08 2018 *)"
			],
			"program": [
				"(PARI) a(n) = numerator(sigma(n, -2)); \\\\ _Michel Marcus_, Aug 24 2018",
				"(PARI) vector(50, n, numerator(sigma(n, 2)/n^2)) \\\\ _G. C. Greubel_, Nov 08 2018",
				"(MAGMA) [Numerator(DivisorSigma(2,n)/n^2): n in [1..50]]; // _G. C. Greubel_, Nov 08 2018"
			],
			"xref": [
				"Cf. A017668 (denominator), A111003 (Pi^2/8)."
			],
			"keyword": "nonn,frac",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"references": 3,
			"revision": 29,
			"time": "2018-11-09T08:02:07-05:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}