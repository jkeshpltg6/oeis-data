{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A318256",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 318256,
			"data": "1,1,2,1,6,1,6,3,10,1,6,1,210,15,2,3,30,5,210,21,110,15,30,5,546,21,14,1,30,1,462,231,1190,105,6,1,51870,1365,70,21,2310,55,2310,105,322,105,210,35,6630,663,286,33,330,55,798,57,290,15,30,1,930930,15015",
			"name": "a(n) = (denominator of B(n,x)) / (the squarefree kernel of n+1), where B(n,x) is the n-th Bernoulli polynomial.",
			"link": [
				"Peter Luschny, \u003ca href=\"/A318256/b318256.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"András Bazsó and István Mező, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2015.01.019\"\u003eOn the coefficients of power sums of arithmetic progressions\u003c/a\u003e, J. Number Th., 153 (2015), 117-123.",
				"Bernd C. Kellner and Jonathan Sondow, \u003ca href=\"https://arxiv.org/abs/1705.03857\"\u003ePower-Sum Denominators\u003c/a\u003e, arXiv:1705.03857 [math.NT] 2017, Amer. Math. Monthly.",
				"Bernd C. Kellner and Jonathan Sondow, \u003ca href=\"https://arxiv.org/abs/1705.05331\"\u003eThe denominators of power sums of arithmetic progressions\u003c/a\u003e, arXiv:1705.05331 [math.NT], 2017."
			],
			"formula": [
				"Let Q(n) = {p \u003c= floor((n + 2)/(2 + n mod 2)) and p is prime and p does not divide n + 1 and the sum of the digits in base p of n+1 is at least p} then a(n) = Product_{p in Q(n)} p. (See the Kellner \u0026 Sondow links.)"
			],
			"example": [
				"a(59) = 1 because there exist no number which satisfies the definition (and the product of an empty set is 1).",
				"a(60) = 930930 because {2, 3, 5, 7, 11, 13, 31} are the only primes which satisfy the definition.",
				"The denominator of the Bernoulli polynomial B_n(x) equals the squarefree kernel of n+1 if n is in {0, 1, 3, 5, 9, 11, 27, 29, 35, 59}. These might be the only numbers with this property."
			],
			"maple": [
				"a := n -\u003e denom(bernoulli(n, x)) / mul(p, p in numtheory:-factorset(n+1)):",
				"seq(a(n), n=0..61);"
			],
			"mathematica": [
				"sfk[n_] := Times @@ FactorInteger[n][[All, 1]];",
				"a[n_] := (BernoulliB[n, x] // Together // Denominator)/sfk[n+1];",
				"Table[a[n], {n, 0, 61}] (* _Jean-François Alcover_, Feb 14 2019 *)"
			],
			"program": [
				"(Sage)",
				"def A318256(n): return mul([p for p in (2..(n+2)//(2+n%2))",
				"                if is_prime(p)",
				"                and not p.divides(n+1)",
				"                and sum((n+1).digits(base=p)) \u003e= p])",
				"print([A318256(n) for n in (0..61)])"
			],
			"xref": [
				"a(n) = A144845(n) / A007947(n+1).",
				"Cf. A027642, A064538, A195441, A286515, A286516, A286517, A286762, A286763, A319084."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Peter Luschny_, Sep 12 2018",
			"references": 5,
			"revision": 30,
			"time": "2020-03-07T14:57:27-05:00",
			"created": "2018-09-13T07:11:28-04:00"
		}
	]
}