{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A010094",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 10094,
			"data": "1,1,1,2,2,1,5,5,4,2,16,16,14,10,5,61,61,56,46,32,16,272,272,256,224,178,122,61,1385,1385,1324,1202,1024,800,544,272,7936,7936,7664,7120,6320,5296,4094,2770,1385,50521,50521,49136,46366,42272,36976,30656,23536,15872,7936,353792",
			"name": "Triangle of Euler-Bernoulli or Entringer numbers.",
			"comment": [
				"T(n, k) is the number of up-down permutations of n starting with k where 1 \u003c= k \u003c= n. - _Michael Somos_, Jan 20 2020"
			],
			"reference": [
				"R. C. Entringer, A combinatorial interpretation of the Euler and Bernoulli numbers, Nieuw Archief voor Wiskunde, 14 (1966), 241-246."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A010094/b010094.txt\"\u003eRows n = 1..150, flattened\u003c/a\u003e (first 51 rows from Vincenzo Librandi)",
				"B. Bauslaugh and F. Ruskey, \u003ca href=\"https://doi.org/10.1007/BF01932127\"\u003eGenerating alternating permutations lexicographically\u003c/a\u003e, Nordisk Tidskr. Informationsbehandling (BIT) 30 16-26 1990.",
				"D. Foata and G.-N. Han, \u003ca href=\"http://arxiv.org/abs/1304.2485\"\u003eSecant Tree Calculus\u003c/a\u003e, arXiv preprint arXiv:1304.2485 [math.CO], 2013.",
				"Dominique Foata and Guo-Niu Han, \u003ca href=\"http://www-irma.u-strasbg.fr/~foata/paper/pub123Seidel.pdf\"\u003eSeidel Triangle Sequences and Bi-Entringer Numbers\u003c/a\u003e, November 20, 2013.",
				"M. Josuat-Verges, J.-C. Novelli and J.-Y. Thibon, \u003ca href=\"http://arxiv.org/abs/1110.5272\"\u003eThe algebraic combinatorics of snakes\u003c/a\u003e, arXiv preprint arXiv:1110.5272 [math.CO], 2011.",
				"J. Millar, N. J. A. Sloane and N. E. Young, A new operation on sequences: the Boustrophedon transform, J. Combin. Theory, 17A (1996) 44-54 (\u003ca href=\"http://neilsloane.com/doc/bous.txt\"\u003eAbstract\u003c/a\u003e, \u003ca href=\"http://neilsloane.com/doc/bous.pdf\"\u003epdf\u003c/a\u003e, \u003ca href=\"http://neilsloane.com/doc/bous.ps\"\u003eps\u003c/a\u003e).",
				"C. Poupard, \u003ca href=\"https://doi.org/10.1016/0012-365X(82)90293-X\"\u003eDe nouvelles significations énumeratives des nombres d'Entringer\u003c/a\u003e, Discrete Math., 38 (1982), 265-271."
			],
			"formula": [
				"T(1, 1) = 1; T(n, n) = 0 if n \u003e 1; T(n, k) = T(n, k+1) + T(n-1, n-k) if 1 \u003c= k \u003c n. - _Michael Somos_, Jan 20 2020"
			],
			"example": [
				"From _Vincenzo Librandi_, Aug 13 2013: (Start)",
				"Triangle begins:",
				"     1;",
				"     1,    1;",
				"     2,    2,    1;",
				"     5,    5,    4,    2;",
				"    16,   16,   14,   10,    5;",
				"    61,   61,   56,   46,   32,   16;",
				"   272,  272,  256,  224,  178,  122,   61;",
				"  1385, 1385, 1324, 1202, 1024,  800,  544,  272;",
				"  7936, 7936, 7664, 7120, 6320, 5296, 4094, 2770, 1385;",
				"  ... (End)",
				"Up-down permutations for n = 4 are k = 1: 1324, 1423; k = 2: 2314, 2413; k = 3: 3411; k = 4: none. - _Michael Somos_, Jan 20 2020"
			],
			"maple": [
				"b:= proc(u, o) option remember; `if`(u+o=0, 1,",
				"      add(b(o-1+j, u-j), j=1..u))",
				"    end:",
				"T:= (n, k)-\u003e b(n-k+1, k-1):",
				"seq(seq(T(n, k), k=1..n), n=1..12); # _Alois P. Heinz_, Jun 03 2020"
			],
			"mathematica": [
				"e[0, 0] = 1; e[_, 0] = 0; e[n_, k_] := e[n, k] = e[n, k-1] + e[n-1, n-k]; Join[{1}, Table[e[n, k], {n, 0, 11}, {k, n, 1, -1}] // Flatten] (* _Jean-François Alcover_, Aug 13 2013 *)"
			],
			"program": [
				"(PARI) {T(n, k) = if( n \u003c 1 || k \u003e= n, k == 1 \u0026\u0026 n == 1, T(n, k+1) + T(n-1, n-k))}; /* _Michael Somos_, Jan 20 2020 */"
			],
			"xref": [
				"Columns k=1,3-4 give: A000111, A006212, A006213.",
				"Row sums give A000111(n+1).",
				"Cf. A008282."
			],
			"keyword": "nonn,tabl,easy,nice",
			"offset": "1,4",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from Will Root (crosswind(AT)bright.net), Oct 08 2001",
				"Irregular zeroth row deleted by _N. J. A. Sloane_, Jun 04 2020"
			],
			"references": 9,
			"revision": 67,
			"time": "2021-05-21T17:02:53-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}