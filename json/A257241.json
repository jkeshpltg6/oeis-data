{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A257241",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 257241,
			"data": "1,2,3,3,4,6,5,10,10,6,15,20,7,21,35,35,8,28,56,70,9,36,84,126,126,10,45,120,210,252,11,55,165,330,462,462,12,66,220,495,792,924,13,78,286,715,1287,1716,1716",
			"name": "Irregular triangle read by rows: Stifel's version of the arithmetical triangle.",
			"comment": [
				"The row length of this array is A008619(n-1), for n \u003e= 1: 1, 1, 2, 2, ...",
				"This is a truncated version of Pascal's triangle used by Michael Stifel (1487?-1567). It already appeared on the title page (frontispiece) of Peter Apianus's book of 1527 on business arithmetic: \"Eyn Newe Und wolgegründte underweysung aller Kauffmanns Rechnung in dreyen Büchern\". See the Kac reference, p. 394 and Table 12.1 on p. 395. It appeared in Stifel's 1553 edition of Rudolff's Coß: \"Die Coß Christoffs Rudolffs. Die schönen Exemplen der Coß Durch Michael Stifel gebessert und sehr gemehrt.\" See the MacTutor Archive link and the Alten reference.",
				"The row sums give A258143. The alternating row sums give A258144.",
				"T(n,A008619(n-1)) = A001405(n). - _Reinhard Zumkeller_, May 22 2015"
			],
			"reference": [
				"H.-W. Alten et al., 4000 Jahre Algebra, 2. Auflage, Springer, 2014, p. 260.",
				"Victor J. Kac, A History of Mathematics, third edition, Addison-Wesley, 2009.",
				"Reich, Karin; Michael Stifel. In: Folkerts, Menso; Eberhard Knobloch; Karin Reich: Maß, Zahl und Gewicht: Mathematik als Schlüssel zu Weltverständnis und Weltbeherrschung. Wolfenbüttel 1989, S. 73 - 95 und 373."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A257241/b257241.txt\"\u003eRows n = 1..150 of triangle, flattened\u003c/a\u003e",
				"MacTutor History of Mathematics archive, \u003ca href=\"http://www-history.mcs.st-andrews.ac.uk/Biographies/Apianus.html\"\u003ePetrus Apianus\u003c/a\u003e.",
				"MacTutor History of Mathematics archive, \u003ca href=\"http://www-history.mcs.st-andrews.ac.uk/Biographies/Stifel.html\"\u003eMichael Stifel\u003c/a\u003e",
				"Maurer, Bertram, \u003ca href=\"http://www.kk.s.bw.schule.de/mathge/stifel.htm\"\u003eMichael Stifel\u003c/a\u003e, 1999, Kolping-Kolleg Stuttgart.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Petrus_Apianus\"\u003ePetrus Apianus\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Michael_Stifel\"\u003eMichael Stifel\u003c/a\u003e."
			],
			"formula": [
				"T(n, m) = binomial(n, m), n \u003e= 1, m = 1, 2, ..., ceiling(n/2).",
				"O.g.f. row m = 1, 2, ..., 4 (with leading zeros): x/(1-x)^2, x^3*(3-3*x+x^2)/(1-x)^3, x^5*(10-20*x+15*x^2-4*x^3)/(1-x)^4, x^7*(35-105*x+126*x^2-70*x^3+15*x^4)/(1-x)^5."
			],
			"example": [
				"The irregular triangle T(n, m) begins:",
				"  n\\m|  1    2    3    4    5    6    7 ...",
				"  ---+-------------------------------------",
				"   1 |  1",
				"   2 |  2",
				"   3 |  3    3",
				"   4 |  4    6",
				"   5 |  5   10   10",
				"   6 |  6   15   20",
				"   7 |  7   21   35   35",
				"   8 |  8   28   56   70",
				"   9 |  9   36   84  126  126",
				"  10 | 10   45  120  210  252",
				"  11 | 11   55  165  330  462  462",
				"  12 | 12   66  220  495  792  924",
				"  13 | 13   78  286  715 1287 1716 1716",
				"  ..."
			],
			"program": [
				"(Haskell)",
				"a257241 n k = a257241_tabf !! (n-1) !! (k-1)",
				"a257241_row n = a257241_tabf !! (n-1)",
				"a257241_tabf = iterate stifel [1] where",
				"   stifel xs@(x:_) = if odd x then xs' else xs' ++ [last xs']",
				"                     where xs' = zipWith (+) xs (1 : xs)",
				"-- _Reinhard Zumkeller_, May 22 2015"
			],
			"xref": [
				"Cf. A007318, A258143, A258144, A014410 (Scheubel's version).",
				"Cf. A001405 (right edge)."
			],
			"keyword": "nonn,easy,tabf",
			"offset": "1,2",
			"author": "_Wolfdieter Lang_, May 22 2015",
			"references": 4,
			"revision": 23,
			"time": "2018-10-12T22:44:13-04:00",
			"created": "2015-05-22T15:01:26-04:00"
		}
	]
}