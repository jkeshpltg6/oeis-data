{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A191449",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 191449,
			"data": "1,3,2,9,6,4,27,18,12,5,81,54,36,15,7,243,162,108,45,21,8,729,486,324,135,63,24,10,2187,1458,972,405,189,72,30,11,6561,4374,2916,1215,567,216,90,33,13,19683,13122,8748,3645,1701,648,270,99,39,14,59049",
			"name": "Dispersion of (3,6,9,12,15,...), by antidiagonals.",
			"comment": [
				"Transpose of A141396.",
				"Background discussion:  Suppose that s is an increasing sequence of positive integers, that the complement t of s is infinite, and that t(1)=1.  The dispersion of s is the array D whose n-th row is (t(n), s(t(n)), s(s(t(n)), s(s(s(t(n)))), ...).  Every positive integer occurs exactly once in D, so that, as a sequence, D is a permutation of the positive integers.  The sequence u given by u(n)=(number of the row of D that contains n) is a fractal sequence.  Examples:",
				"(1) s=A000040 (the primes), D=A114537, u=A114538.",
				"(2) s=A022343 (without initial 0), D=A035513 (Wythoff array), u=A003603.",
				"(3) s=A007067, D=A035506 (Stolarsky array), u=A133299.",
				"More recent examples of dispersions: A191426-A191455."
			],
			"formula": [
				"T(i,j)=T(i,1)*T(1,j)=floor((3i-1)/2)*3^(j-1)."
			],
			"example": [
				"Northwest corner:",
				"1...3....9....27...81",
				"2...6....18...54...162",
				"4...12...36...108..324",
				"5...15...45...135..405",
				"7...21...63...189..567"
			],
			"mathematica": [
				"(* Program generates the dispersion array T of increasing sequence f[n] *)",
				"r=40; r1=12; c=40; c1=12;",
				"f[n_] :=3n (* complement of column 1 *)",
				"mex[list_] := NestWhile[#1 + 1 \u0026, 1, Union[list][[#1]] \u003c= #1 \u0026, 1, Length[Union[list]]]",
				"rows = {NestList[f, 1, c]};",
				"Do[rows = Append[rows, NestList[f, mex[Flatten[rows]], r]], {r}];",
				"t[i_, j_] := rows[[i, j]];",
				"TableForm[Table[t[i, j], {i, 1, 10}, {j, 1, 10}]]",
				"(* A191449 array *)",
				"Flatten[Table[t[k, n - k + 1], {n, 1, c1}, {k, 1, n}]] (* A191449 sequence *)",
				"(* Program by _Peter J. C. Moses_, Jun 01 2011 *)"
			],
			"xref": [
				"Cf. A114537, A035513, A035506,",
				"A054582: dispersion of (2,4,6,8,...).",
				"A191450: dispersion of (2,5,8,11,...).",
				"A191451: dispersion of (4,7,10,13,...).",
				"A191452: dispersion of (4,8,12,16,...)."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Jun 05 2011",
			"references": 7,
			"revision": 12,
			"time": "2014-02-14T00:32:15-05:00",
			"created": "2011-06-06T12:52:37-04:00"
		}
	]
}