{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A077800",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 77800,
			"data": "3,5,5,7,11,13,17,19,29,31,41,43,59,61,71,73,101,103,107,109,137,139,149,151,179,181,191,193,197,199,227,229,239,241,269,271,281,283,311,313,347,349,419,421,431,433,461,463,521,523,569,571,599,601,617,619",
			"name": "List of twin primes {p, p+2}.",
			"comment": [
				"Union (with repetition) of A001359 and A006512."
			],
			"reference": [
				"M. Abramowitz and I. A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, 1964 (and various reprintings), p. 870."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A077800/b077800.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"Jean-Paul Delahaye, \u003ca href=\"http://www.lifl.fr/~delahaye/SIME/JPD/PLS_Nb_premiers_jumeaux.pdf\"\u003ePremiers jumeaux: frères ennemis?\u003c/a\u003e [Twin primes: Enemy Brothers?], Pour la science, No. 260 (Juin 1999), 102-106.",
				"Jean-Claude Evard, \u003ca href=\"http://web.archive.org/web/20110726012847/http://www.math.utoledo.edu/~jevard/Page012.htm\"\u003eTwin primes and their applications\u003c/a\u003e. [Cached copy on the Wayback Machine]",
				"Jean-Claude Evard, \u003ca href=\"/A077800/a077800.html\"\u003eTwin primes and their applications\u003c/a\u003e. [Local cached copy]",
				"Jean-Claude Evard, \u003ca href=\"/A077800/a077800.pdf\"\u003eTwin primes and their applications\u003c/a\u003e. [Pdf file of cached copy]",
				"Dave Platt and Tim Trudgian, \u003ca href=\"https://doi.org/10.1007/978-3-030-36568-4_25\"\u003eImproved bounds on Brun's constant\u003c/a\u003e, in: David H. Bailey et al. (eds), From Analysis to Visualization, JBCC 2017, Springer Proceedings in Mathematics \u0026 Statistics, Vol 313, Springer, Cham, 2020, \u003ca href=\"https://arxiv.org/abs/1803.01925\"\u003epreprint\u003c/a\u003e, arXiv:1803.01925 [math.NT], 2018.",
				"Hayden Tronnolone, \u003ca href=\"https://www.semanticscholar.org/paper/A-tale-of-two-primes-Tronnolone/2576b80d487c909639c98a1e3cb255658c40d699\"\u003eA tale of two primes\u003c/a\u003e, COLAUMS Space, #3, 2013.",
				"Wikipedia, \u003ca href=\"http://www.wikipedia.org/wiki/Twin_prime\"\u003eTwin prime\u003c/a\u003e.",
				"\u003ca href=\"/index/Pri#gaps\"\u003eIndex entries for primes, gaps between\u003c/a\u003e"
			],
			"formula": [
				"Sum_{n\u003e=1} 1/a(n) is in the interval (1.840503, 2.288490) (Platt and Trudgian, 2020). The conjectured value based on assumptions about the distribution of twin primes is A065421. - _Amiram Eldar_, Oct 15 2020"
			],
			"mathematica": [
				"Sort[ Join[ Select[ Prime[ Range[ 115]], PrimeQ[ # - 2] \u0026], Select[ Prime[ Range[ 115]], PrimeQ[ # + 2] \u0026]]] (* _Robert G. Wilson v_, Jun 09 2005 *)",
				"Select[ Partition[ Prime@ Range@ 115, 2, 1], #[[1]] + 2 == #[[2]] \u0026] // Flatten",
				"Flatten[Select[{#, # + 2} \u0026 /@Prime[Range[1000]], PrimeQ[Last[#]]\u0026]] (* _Vincenzo Librandi_, Nov 01 2012 *)"
			],
			"program": [
				"(Haskell)",
				"a077800 n = a077800_list !! (n-1)",
				"a077800_list = concat $ zipWith (\\p q -\u003e if p == q+2 then [q,p] else [])",
				"                                (tail a000040_list) a000040_list",
				"-- _Reinhard Zumkeller_, Nov 27 2011",
				"(PARI) p=2;forprime(q=3,1e3,if(q-p==2,print1(p\", \"q\", \"));p=q) \\\\ _Charles R Greathouse IV_, Mar 22 2013"
			],
			"xref": [
				"Cf. A065421, A070076, A095958. See A001097 for another version."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_, Dec 03 2002",
			"references": 97,
			"revision": 77,
			"time": "2020-10-15T03:58:49-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}