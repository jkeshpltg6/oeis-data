{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005378",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5378,
			"id": "M0263",
			"data": "1,1,2,2,3,3,4,5,5,6,6,7,8,8,9,9,10,11,11,12,13,13,14,14,15,16,16,17,17,18,19,19,20,21,21,22,22,23,24,24,25,25,26,27,27,28,29,29,30,30,31,32,32,33,34,34,35,35,36,37,37,38,38,39,40,40,41,42,42,43,43,44,45,45",
			"name": "The female of a pair of recurrences.",
			"comment": [
				"F(n) is not equal to M(n) if and only if n+1 is a Fibonacci number (A000045); a(n)=A005379(n)+A192687(n). [_Reinhard Zumkeller_, Jul 12 2011]",
				"Differs from A098294 in indices n=0,17,20,22,25,27,29,30,... - _M. F. Hasler_, Jun 29 2014"
			],
			"reference": [
				"Hofstadter, \"Goedel, Escher, Bach\", p. 137.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A005378/b005378.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"D. R. Hofstadter, \u003ca href=\"/A006336/a006336_1.pdf\"\u003eEta-Lore\u003c/a\u003e [Cached copy, with permission]",
				"D. R. Hofstadter, \u003ca href=\"/A006336/a006336_2.pdf\"\u003ePi-Mu Sequences\u003c/a\u003e [Cached copy, with permission]",
				"D. R. Hofstadter and N. J. A. Sloane, \u003ca href=\"/A006336/a006336.pdf\"\u003eCorrespondence, 1977 and 1991\u003c/a\u003e",
				"Th. Stoll, \u003ca href=\"http://www.fq.math.ca/Papers1/46_47-1/Stoll_11-08.pdf\"\u003eOn Hofstadter's married functions\u003c/a\u003e, Fib. Q., 46/47 (2008/2009), 62-67. - from _N. J. A. Sloane_, May 30 2009",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/HofstadterMale-FemaleSequences.html\"\u003eHofstadter Male-Female Sequences.\u003c/a\u003e",
				"\u003ca href=\"/index/Ho#Hofstadter\"\u003eIndex entries for Hofstadter-type sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Go#GEB\"\u003eIndex entries for sequences from \"Goedel, Escher, Bach\"\u003c/a\u003e"
			],
			"formula": [
				"F(0) = 1; M(0) = 0; F(n) = n-M(F(n-1)); M(n) = n-F(M(n-1))."
			],
			"mathematica": [
				"f[0] = 1; m[0] = 0; f[n_] := f[n] = n - m[f[n-1]]; m[n_] := m[n] = n - f[m[n-1]]; Table[f[n], {n, 0, 73}] (* _Jean-François Alcover_, Jul 27 2011 *)"
			],
			"program": [
				"(Haskell)",
				"a005378 n = a005378_list !! n",
				"a005378_list = 1 : zipWith (-) [1..] (map a005379 a005378_list)",
				"a005379 n = a005379_list !! n",
				"a005379_list = 0 : zipWith (-) [1..] (map a005378 a005379_list)",
				"-- Without memoization the original recursion would be feasible only for small n.",
				"-- _Reinhard Zumkeller_, Jul 12 2011"
			],
			"xref": [
				"Cf. A005379."
			],
			"keyword": "nonn,nice,easy",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_.",
			"ext": [
				"More terms from _James A. Sellers_, Jul 12 2000",
				"Comment corrected by _Jaroslav Krizek_, Dec 25 2011"
			],
			"references": 8,
			"revision": 56,
			"time": "2016-05-03T16:52:51-04:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}