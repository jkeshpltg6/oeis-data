{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A086278",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 86278,
			"data": "9,7,8,0,1,2,4,7,8,1,8,6,6,4,6,2,2,0,2,0,1,8,2,7,9,5,9,9,7,8,6,8,2,6,8,0,9,3,2,5,3,8,6,3,5,3,4,5,9,1,4,1,8,0,9,4,9,5,3,0,4,2,0,8,3,4,5,9,9,4,4,9,2,5,8,0,7,1,0,6,9,7,5,0,0,5,5,6,6,8,9,8,5,2,0,3,9,2,6,5,9,2,4",
			"name": "Decimal expansion of Shapiro's cyclic sum constant mu.",
			"comment": [
				"From _Petros Hadjicostas_, Jun 01 2020: (Start)",
				"Based on the references, it seems that this constant was first defined by Elbert (1973). We have psi(0) = mu. Two auxiliary constants, b = -0.33060494... = -A335809 and c = 0.38755227... = A335810, are needed for the estimation of mu.",
				"Here psi(x) is the convex hull of y = (1 + exp(x))/2 and y = (1 + exp(x))/(1 + exp(x/2)); i.e., psi(x) = (1 + exp(x))/2 for x \u003c= b; psi(x) = (1 + exp(b))/2 + (((1 + exp(c))/(1 + exp(c/2)) - (1 + exp(b))/2)/(c - b)) * (x - b) for b \u003c= x \u003c= c; and psi(x) = (1 + exp(x))/(1 + exp(x/2)) for x \u003e= c. (For b \u003c= x \u003c= c, we have the equation of the line segment tangent to both curves.)",
				"It follows that mu = psi(0) = (1 + exp(b))/2 - b * (((1 + exp(c))/(1 + exp(c/2)) - (1 + exp(b))/2)/(c - b))  (where the y-axis crosses the line segment). Or by using the tangent line at x = b to the curve y = (1 + exp(x))/2, we find mu = psi(0) = (1 + exp(b))/2 - b * exp(b)/2. Or by using the tangent line at x = c to the curve y = (1 + exp(x))/(1 + exp(x/2)), we may get a third formula for mu = psi(0) in terms of c only.",
				"Similar calculations were done by Drinfel'd (1971) for the Shapiro cyclic sum constant lambda = phi(0)/2 = A086277 = A245330/2. In this case, the corresponding curves are y = exp(-x) and y = 2/(exp(x) + exp(x/2)), while the corresponding x-coordinates at the tangent points are -A319568 = -0.20081... and A319569 = 0.15519... Here phi(x) is the convex hull of these two curves (and it becomes a line segment tangent to both curves for -A319568 \u003c= x \u003c= A319569).",
				"Eric W. Weisstein, in the link below, has a summary of the above discussion (with contributions by Steven Finch). (End)"
			],
			"reference": [
				"Steven R. Finch, Mathematical Constants, Cambridge University Press, 2003, Section 3.1, Shapiro-Drinfeld constant, p. 209."
			],
			"link": [
				"V. G. Drinfel'd, \u003ca href=\"https://doi.org/10.1007/BF01316982\"\u003eA cyclic inequality\u003c/a\u003e, Mathematical Notes of the Academy of Sciences of the USSR, 9 (1971), 68-71.",
				"Á. Elbert, \u003ca href=\"https://doi.org/10.1007/BF02276104\"\u003eOn a cyclic inequality\u003c/a\u003e, Periodica Mathematica Hungarica, 4 (1973), 163-168.",
				"Á. Elbert, \u003ca href=\"https://akjournals.com/view/journals/10998/4/2-3/article-p163.xml\"\u003eOn a cyclic inequality\u003c/a\u003e, Periodica Mathematica Hungarica, 4 (1973), 163-168.",
				"Petros Hadjicostas, \u003ca href=\"/A086278/a086278.pdf\"\u003ePlot of the curves y = (1 + exp(x))/2 and y = (1 + exp(x))/(1 + exp(x/2)) and their common tangent\u003c/a\u003e, 2020.",
				"R. A. Rankin, \u003ca href=\"https://doi.org/10.2307/3608356\"\u003e2743. An inequality\u003c/a\u003e, Mathematical Gazette, 42(339) (1958), 39-40.",
				"H. S. Shapiro, \u003ca href=\"https://www.jstor.org/stable/2307617\"\u003eProposed problem for solution 4603\u003c/a\u003e, American Mathematical Monthly, 61(8) (1954), 571.",
				"H. S. Shapiro, \u003ca href=\"https://www.jstor.org/stable/2306671\"\u003eSolution to Problem 4603: An invalid inequality\u003c/a\u003e, American Mathematical Monthly, 63(3) (1956), 191-192; counterexample provided by M. J. Lighthill.",
				"B. A. Troesch, \u003ca href=\"https://doi.org/10.1090/S0025-5718-1989-0983563-0\"\u003eThe validity of Shapiro's cyclic inequality\u003c/a\u003e, Mathematics of Computation, 53 (1989), 657-664.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"https://mathworld.wolfram.com/ShapirosCyclicSumConstant.html\"\u003eShapiro's cyclic sum constant\u003c/a\u003e."
			],
			"formula": [
				"From _Petros Hadjicostas_, Jun 23 2020: (Start)",
				"Solve the following system of equations to find the x-coordinates of the two points where the common tangent touches the two curves:",
				"exp(b) = (-exp(c/2) + 2*exp(c) + exp(3*c/2))/(1 + exp(c/2))^2 and",
				"(exp(b)*(c - b + 1) + 1)*(1 + exp(c/2)) = 2*(1 + exp(c)).",
				"Then the constant equals (1 + exp(b)*(1 - b))/2. (End)",
				"It can be proved that this constant equals 1 plus the Gauchman constant (which is the negation of A243261); i.e., without negations, A086278 = 1 - A243261. - _Petros Hadjicostas_, Jul 04 2020"
			],
			"example": [
				"0.97801247818664622020182795997868268... = 1 - 0.02198752181335377979817204..."
			],
			"mathematica": [
				"eq = E^u + 2*E^(u + v/2) + E^(v/2) + E^(u + v) == 2*E^v + E^(3*v/2) \u0026\u0026 2 + 2*E^(u + v/2) == 2*y + 2*E^v + E^u*(v - 2) \u0026\u0026 E^u*(u - v + 1) + 2*E^(u + v/2) + 1 == 2*E^v; mu = y /. FindRoot[eq , {{y, 1}, {u, -1/3}, {v, 1/3}}, WorkingPrecision -\u003e 105]; RealDigits[mu, 10, 103] // First"
			],
			"program": [
				"(PARI)",
				"default(\"realprecision\",200)",
				"b(c) = log((-exp(c/2) + 2*exp(c) + exp(3*c/2))/(1 + exp(c/2))^2);",
				"a = solve(c=-1, 1, (exp(b(c))*(c - b(c) + 1) + 1)*(1 + exp(c/2)) - 2*(1 + exp(c)));",
				"(1 + exp(b(a))*(1 - b(a)))/2 \\\\ _Petros Hadjicostas_, Jun 23 2020"
			],
			"xref": [
				"Cf. A086277, A243261, A245330, A319568, A319569, A335809, A335810."
			],
			"keyword": "nonn,cons",
			"offset": "0,1",
			"author": "_Eric W. Weisstein_, Jul 14 2003",
			"ext": [
				"More terms from _Jean-François Alcover_, Jun 02 2014"
			],
			"references": 12,
			"revision": 69,
			"time": "2020-07-07T06:22:50-04:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}