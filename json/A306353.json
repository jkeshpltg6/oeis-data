{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A306353",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 306353,
			"data": "1,2,3,1,4,5,6,2,7,8,9,3,10,11,1,12,4,13,14,15,5,16,2,17,18,6,19,20,21,7,22,23,1,24,8,25,26,3,27,9,28,29,30,10,31,4,32,33,11,34,35,36,12,37,2,38,39,13,40,41,5,42,14,43,44,3,45,15,46,6,47,48,16,49,50,51,17,52,53,54,18,55,56,7",
			"name": "Number of composites among the first n composite numbers whose least prime factor p is that of the n-th composite number.",
			"comment": [
				"Composites with least prime factor p are on that row of A083140 which begins with p",
				"Sequence with similar values: A122005.",
				"Sequence written as a jagged array A with new row when a(n) \u003e a(n+1):",
				"  1,  2,  3,",
				"  1,  4,  5,  6,",
				"  2,  7,  8,  9,",
				"  3, 10, 11,",
				"  1, 12,",
				"  4, 13, 14, 15,",
				"  5, 16,",
				"  2, 17, 18,",
				"  6, 19, 20, 21,",
				"  7, 22, 23,",
				"  1, 24,",
				"  8, 25, 26,",
				"  3, 27,",
				"  9, 28, 29, 30.",
				"A153196 is the list B of the first values in successive rows with length 4.",
				"  B is given by the formula for A002808(x)=A256388(n+3), an(x)=A153196(n+2)",
				"For example: A002808(26)=A256388(3+3), an(26)=A153196(3+2).",
				"A243811 is the list of the second values in successive rows with length 4.",
				"A047845 is the list of values in the second column and A104279 is the list of values in the third column of the jagged array starting on the second row.",
				"Sequence written as an irregular triangle C with new row when a(n)=1:",
				"  1,2,3,",
				"  1,4,5,6,2,7,8,9,3,10,11,",
				"  1,12,4,13,14,15,5,16,2,17,18,6,19,20,21,7,22,23,",
				"  1,24,8,25,26,3,27,9,28,29,30,10,31,4,32,33,11,34,35,36,12,37,2,38,39,13,40,41,5,42,14,43,44,3,45,15,46,6,47,48,16,49,50,51,17,52,53,54,18,55,56,7,57,19,58,4,59.",
				"A243887 is the last value in each row of C.",
				"The second value D on the row n \u003e 1 of the irregular triangle C is a(A053683(n)) or equivalently A084921(n). For example for row 3 of the irregular triangle:",
				"  D = a(A053683(3)) = a(16) = 12 or D = A084921(3) = 12. This is the number of composites \u003c A066872(3) with the same least prime factor p as the A053683(3) = 16th composite, A066872(3) = 26.",
				"The number of values in each row of the irregular triangle C begins: 3,11,18,57,39,98,61,141,265,104,351,268,...",
				"The second row of the irregular triangle C is A117385(b) for 3 \u003c b \u003c 15.",
				"The third row of the irregular triangle C has similar values as A117385 in different order."
			],
			"link": [
				"Jamie Morken, \u003ca href=\"/A306353/b306353.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) is approximately equal to A002808(n)*(A038110(x)/A038111(x)), with A000040(x)=A020639(A002808(n)).",
				"For example if n=325, a(325)~= A002808(325)*(A038110(2)/A038111(2)) with A000040(2)=A020639(A002808(325)).",
				"This gives an estimate of 67.499... and the actual value of a(n)=67."
			],
			"example": [
				"First composite 4, least prime factor is 2, first case for 2 so a(1)=1.",
				"Next composite 6, least prime factor is 2, second case for 2 so a(2)=2.",
				"Next composite 8, least prime factor is 2, third case for 2 so a(3)=3.",
				"Next composite 9, least prime factor is 3, first case for 3 so a(4)=1.",
				"Next composite 10, least prime factor is 2, fourth case for 2 so a(5)=4."
			],
			"mathematica": [
				"counts = {}",
				"values = {}",
				"For[i = 2, i \u003c 130, i = i + 1,",
				"If[PrimeQ[i], ,",
				"x = PrimePi[FactorInteger[i][[1, 1]]];",
				"  If[Length[counts] \u003e= x,",
				"   counts[[x]] = counts[[x]] + 1;",
				"   AppendTo[values, counts[[x]]], AppendTo[counts, 1];",
				"   AppendTo[values, 1]]]]",
				"   (* Print[counts] *)",
				"   Print[values]"
			],
			"program": [
				"(PARI) c(n) = for(k=0, primepi(n), isprime(n++)\u0026\u0026k--); n; \\\\ A002808",
				"a(n) = my(c=c(n), lpf = vecmin(factor(c)[,1]), nb=0); for(k=2, c, if (!isprime(k) \u0026\u0026 vecmin(factor(k)[,1])==lpf, nb++)); nb; \\\\ _Michel Marcus_, Feb 10 2019"
			],
			"xref": [
				"Cf. A002808, A256388, A056608, A083140, A122005, A153196, A243811, A047845, A104279, A243887, A117385, A216244, A084921, A066872, A053683, A038110, A038111, A020639."
			],
			"keyword": "nonn,hear",
			"offset": "1,2",
			"author": "_Jamie Morken_ and _Vincenzo Librandi_, Feb 09 2019",
			"references": 1,
			"revision": 99,
			"time": "2019-04-09T05:12:01-04:00",
			"created": "2019-03-23T21:16:42-04:00"
		}
	]
}