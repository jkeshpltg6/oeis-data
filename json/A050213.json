{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A050213",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 50213,
			"data": "24,120,720,5040,40320,362880,72576,3628800,1330560,39916800,20338560,479001600,303937920,6227020800,4643084160,87178291200,73721007360,1743565824,1307674368000,1224694598400,69742632960,20922789888000",
			"name": "Triangle of number of permutations of {1, 2, ..., n} having exactly k cycles, each of which is of length \u003e=r for r=5.",
			"comment": [
				"Generalizes Stirling numbers of the first kind"
			],
			"reference": [
				"L. Comtet, Advanced Combinatorics, Reidel, 1974, p. 257."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A050213/b050213.txt\"\u003eRows n = 5..300, flattened\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PermutationCycle.html\"\u003ePermutation Cycle.\u003c/a\u003e"
			],
			"example": [
				"Triangle begins:",
				"05:       24;",
				"06:      120;",
				"07:      720;",
				"08:     5040;",
				"09:    40320;",
				"10:   362880,    72576;",
				"11:  3628800,  1330560;",
				"12: 39916800, 20338560;"
			],
			"maple": [
				"b:= proc(n) option remember; expand(`if`(n=0, 1, add(",
				"      b(n-i)*x*binomial(n-1, i-1)*(i-1)!, i=5..n)))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=1..degree(p)))(b(n)):",
				"seq(T(n), n=5..20);  # _Alois P. Heinz_, Sep 25 2016"
			],
			"mathematica": [
				"b[n_] := b[n] = Expand[If[n == 0, 1, Sum[b[n - i]*x*Binomial[n - 1, i - 1]* (i - 1)!, {i, 5, n}]]];",
				"T[n_] := Function[p, Table[Coefficient[p, x, i], {i, 1, Exponent[p, x]}]][ b[n]];",
				"T /@ Range[5, 20] // Flatten (* _Jean-François Alcover_, Dec 08 2019, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A008275, A008306, A050211, A050212."
			],
			"keyword": "nonn,tabl",
			"offset": "5,1",
			"author": "_Eric W. Weisstein_",
			"ext": [
				"Offset changed from 1 to 5 by _Alois P. Heinz_, Sep 25 2016"
			],
			"references": 3,
			"revision": 18,
			"time": "2019-12-08T07:40:10-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}