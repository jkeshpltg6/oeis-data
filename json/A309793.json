{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A309793",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 309793,
			"data": "0,0,0,0,1,1,1,1,2,3,5,6,8,9,11,13,17,20,24,27,32,36,42,47,54,60,68,75,85,93,103,112,124,135,149,161,176,189,205,220,239,256,276,294,316,336,360,382,408,432,460,486,517,545,577,607,642,675,713,748",
			"name": "Number of odd parts appearing among the second largest parts of the partitions of n into 4 parts.",
			"link": [
				"Colin Barker, \u003ca href=\"/A309793/b309793.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Par#part\"\u003eIndex entries for sequences related to partitions\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_16\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,-1,0,0,0,1,-2,2,-2,1,0,0,0,-1,2,-1)."
			],
			"formula": [
				"a(n) = Sum_{k=1..floor(n/4)} Sum_{j=k..floor((n-k)/3)} Sum_{i=j..floor((n-j-k)/2)} (i mod 2).",
				"From _Colin Barker_, Aug 18 2019: (Start)",
				"G.f.: x^4*(1 - x + x^4) / ((1 - x)^4*(1 + x)^2*(1 - x + x^2)*(1 + x^2)*(1 + x + x^2)*(1 + x^4)).",
				"a(n) = 2*a(n-1) - a(n-2) + a(n-6) - 2*a(n-7) + 2*a(n-8) - 2*a(n-9) + a(n-10) - a(n-14) + 2*a(n-15) - a(n-16) for n\u003e15.",
				"(End) [Recurrence verified by _Wesley Ivan Hurt_, Aug 24 2019]"
			],
			"example": [
				"Figure 1: The partitions of n into 4 parts for n = 8, 9, ..",
				"                                                         1+1+1+9",
				"                                                         1+1+2+8",
				"                                                         1+1+3+7",
				"                                                         1+1+4+6",
				"                                             1+1+1+8     1+1+5+5",
				"                                             1+1+2+7     1+2+2+7",
				"                                 1+1+1+7     1+1+3+6     1+2+3+6",
				"                                 1+1+2+6     1+1+4+5     1+2+4+5",
				"                                 1+1+3+5     1+2+2+6     1+3+3+5",
				"                     1+1+1+6     1+1+4+4     1+2+3+5     1+3+4+4",
				"         1+1+1+5     1+1+2+5     1+2+2+5     1+2+4+4     2+2+2+6",
				"         1+1+2+4     1+1+3+4     1+2+3+4     1+3+3+4     2+2+3+5",
				"         1+1+3+3     1+2+2+4     1+3+3+3     2+2+2+5     2+2+4+4",
				"         1+2+2+3     1+2+3+3     2+2+2+4     2+2+3+4     2+3+3+4",
				"         2+2+2+2     2+2+2+3     2+2+3+3     2+3+3+3     3+3+3+3",
				"--------------------------------------------------------------------------",
				"  n  |      8           9          10          11          12        ...",
				"--------------------------------------------------------------------------",
				"a(n) |      2           3           5           6           8        ...",
				"--------------------------------------------------------------------------"
			],
			"mathematica": [
				"LinearRecurrence[{2, -1, 0, 0, 0, 1, -2, 2, -2, 1, 0, 0, 0, -1, 2, -1}, {0, 0, 0, 0, 1, 1, 1, 1, 2, 3, 5, 6, 8, 9, 11, 13}, 50]"
			],
			"program": [
				"(PARI) concat([0,0,0,0], Vec(x^4*(1 - x + x^4) / ((1 - x)^4*(1 + x)^2*(1 - x + x^2)*(1 + x^2)*(1 + x + x^2)*(1 + x^4)) + O(x^50))) \\\\ _Colin Barker_, Oct 10 2019"
			],
			"xref": [
				"Cf. A309795, A309797, A026928."
			],
			"keyword": "nonn,easy",
			"offset": "0,9",
			"author": "_Wesley Ivan Hurt_, Aug 17 2019",
			"references": 2,
			"revision": 24,
			"time": "2019-11-07T07:02:01-05:00",
			"created": "2019-08-26T12:48:25-04:00"
		}
	]
}