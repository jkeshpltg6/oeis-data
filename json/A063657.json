{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A063657",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 63657,
			"data": "3,7,8,13,14,15,21,22,23,24,31,32,33,34,35,43,44,45,46,47,48,57,58,59,60,61,62,63,73,74,75,76,77,78,79,80,91,92,93,94,95,96,97,98,99,111,112,113,114,115,116,117,118,119,120,133,134,135,136,137,138,139,140",
			"name": "Numbers with property that truncated square root is unequal to rounded square root.",
			"comment": [
				"Also: skip 1, take 0, skip 2, take 1, skip 3, take 2, ...",
				"Integers for which the periodic part of the continued fraction for the square root of n begins with a 1. - _Robert G. Wilson v_, Nov 01 2001",
				"a(n) belongs to the sequence if and only if a(n) \u003e floor(sqrt(a(n))) * ceiling(sqrt(a(n))), i.e. a(n) in (k*(k+1),k^2), k \u003e= 0. - _Daniel Forgues_, Apr 17 2011",
				"Any integer between (k - 1/2)^2 and k^2 exclusive, for k \u003e 1, is in this sequence. If we take this sequence and remove each term that is one more than the previous term, we obtain the central polygonal numbers (A002061). If instead we remove each term that is one less than the next term, we obtain numbers that are one less than squares (A005563). - _Alonso del Arte_, Dec 28 2013",
				"a(n) = A217575(n) + 1. - _Reinhard Zumkeller_, Jun 20 2015"
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A063657/b063657.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (terms 1..1000 from Harry J. Smith)",
				"R. B. Nelsen, \u003ca href=\"https://www.maa.org/sites/default/files/Knuth50415330.pdf\"\u003eProof without Words: Consecutive sums of consecutive integers\u003c/a\u003e, Mathematical Association of America, p.25."
			],
			"example": [
				"7 is in the sequence because its square root is 2.64575..., which truncates to 2 but rounds to 3.",
				"8 is in the sequence because its square root is 2.828427..., which also truncates to 2 but rounds to 3.",
				"9 is not in the sequence because its square root is 3 exactly, which truncates and rounds the same.",
				"Here is the example per Lamoen's skip n, take n - 1 process: starting at 0, we skip one integer (0) but take zero integers for our sequence. Then we skip two integers (1 and 2) and take one integer (3) for our sequence. Then we skip three integers (4, 5, 6) and take two integers for our sequence (7 and 8, so the sequence now stands as 3, 7, 8). Then we skip four integers (9, 10, 11, 12) and so on and so forth.",
				"From _Seiichi Manyama_, Sep 19 2017: (Start)",
				"See R. B. Nelsen's paper.",
				"   k|            A063656(n)         |            a(n)",
				"   -------------------------------------------------------------------",
				"   0|                             0",
				"   1|                        1 +  2 =  3",
				"   2|                   4 +  5 +  6 =  7 +  8",
				"   3|              9 + 10 + 11 + 12 = 13 + 14 + 15",
				"   4|        16 + 17 + 18 + 19 + 20 = 21 + 22 + 23 + 24",
				"    | ...",
				"(End)"
			],
			"maple": [
				"A063657:=n-\u003e`if`(floor(floor(sqrt(n+1)) * (1+floor(sqrt(n+1)))/(n+1))=1, NULL, n+1); seq(A063657(n), n=1..200); # _Wesley Ivan Hurt_, Dec 28 2013"
			],
			"mathematica": [
				"Select[ Range[200], Floor[ Sqrt[ # ]] != Floor[ Sqrt[ # ] + 1/2] \u0026 ] (* or *) Select[ Range[200], First[ Last[ ContinuedFraction[ Sqrt[ # ]]]] == 1 \u0026 ]"
			],
			"program": [
				"(PARI) { n=0; for (m=0, 10^9, if (sqrt(m)%1 \u003e .5, write(\"b063657.txt\", n++, \" \", m); if (n==1000, break)) ) } \\\\ _Harry J. Smith_, Aug 27 2009",
				"(Haskell)",
				"a063657 n = a063657_list !! n",
				"a063657_list = f 0 [0..] where",
				"   f k (_:xs) = us ++ f (k + 1) (drop (k + 1) vs) where",
				"                        (us, vs) = splitAt k xs",
				"-- _Reinhard Zumkeller_, Jun 20 2015"
			],
			"xref": [
				"Cf. A063656, A004201-A004202, A002620, A189151.",
				"Cf. A217575."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Floor van Lamoen_, Jul 24 2001",
			"references": 9,
			"revision": 47,
			"time": "2017-09-19T09:45:05-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}