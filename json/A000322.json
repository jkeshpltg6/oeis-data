{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000322",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 322,
			"id": "M3786 N1542",
			"data": "1,1,1,1,1,5,9,17,33,65,129,253,497,977,1921,3777,7425,14597,28697,56417,110913,218049,428673,842749,1656801,3257185,6403457,12588865,24749057,48655365,95653929,188050673,369697889,726806913,1428864769",
			"name": "Pentanacci numbers: a(n) = a(n-1) + a(n-2) + a(n-3) + a(n-4) + a(n-5) with a(0) = a(1) = a(2) = a(3) = a(4) = 1.",
			"comment": [
				"For n\u003e=0: a(n+2) is the number of length-n strings with letters {0,1,2,3,4} where the letter x is followed by at least x zeros, see fxtbook link below. - _Joerg Arndt_, Apr 08 2011",
				"Satisfies Benford's law [see A186192] - _N. J. A. Sloane_, Feb 09 2017"
			],
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Indranil Ghosh, \u003ca href=\"/A000322/b000322.txt\"\u003eTable of n, a(n) for n = 0..3402\u003c/a\u003e (terms 0..200 from T. D. Noe)",
				"Joerg Arndt, \u003ca href=\"http://www.jjj.de/fxt/#fxtbook\"\u003eMatters Computational (The Fxtbook)\u003c/a\u003e, pp.311-312.",
				"B. G. Baumgart, Letter to the editor \u003ca href=\"http://www.fq.math.ca/Scanned/2-4/baumgart-a.pdf\"\u003ePart 1\u003c/a\u003e \u003ca href=\"http://www.fq.math.ca/Scanned/2-4/baumgart-b.pdf\"\u003ePart 2\u003c/a\u003e \u003ca href=\"http://www.fq.math.ca/Scanned/2-4/baumgart-c.pdf\"\u003ePart 3\u003c/a\u003e, Fib. Quart. 2 (1964), 260, 302.",
				"D. Birmajer, J. B. Gil, M. D. Weiner, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL19/Gil/gil6.html\"\u003en the Enumeration of Restricted Words over a Finite Alphabet \u003c/a\u003e, J. Int. Seq. 19 (2016) # 16.1.3, Example 7",
				"Martin Burtscher, Igor Szczyrba, Rafał Szczyrba, \u003ca href=\"http://www.emis.de/journals/JIS/VOL18/Szczyrba/sz3.pdf\"\u003eAnalytic Representations of the n-anacci Constants and Generalizations Thereof\u003c/a\u003e, Journal of Integer Sequences, Vol. 18 (2015), Article 15.4.5.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1, 1, 1, 1, 1).",
				"\u003ca href=\"/index/Be#Benford\"\u003eIndex entries for sequences related to Benford's law\u003c/a\u003e"
			],
			"maple": [
				"A000322:=(-1+z**2+2*z**3+3*z**4)/(-1+z**2+z**3+z+z**4+z**5); # _Simon Plouffe_ in his 1992 dissertation.",
				"a:= n-\u003e (Matrix([[1$5]]). Matrix(5, (i,j)-\u003e if (i=j-1) or j=1 then 1 else 0 fi)^n)[1,5]: seq (a(n), n=0..28); # _Alois P. Heinz_, Aug 26 2008"
			],
			"mathematica": [
				"LinearRecurrence[{1,1,1,1,1},{1,1,1,1,1},50]"
			],
			"program": [
				"(MAGMA) [ n le 5 select 1 else Self(n-1)+Self(n-2)+Self(n-3)+Self(n-4)+Self(n-5): n in [1..40] ];",
				"(PARI) Vec((1-x^2-2*x^3-3*x^4)/(1-x-x^2-x^3-x^4-x^5)+O(x^99)) \\\\ _Charles R Greathouse IV_, Jul 01 2013",
				"(J) (see www.jsoftware.com) First construct the generating matrix",
				"   (((+ +/),]),:^:(1=#@$))/\u0026.|.\u003c:/~i.5",
				"1  1  1  1  1",
				"1  2  2  2  2",
				"2  3  4  4  4",
				"4  6  7  8  8",
				"8 12 14 15 16",
				"Given that matrix, one can produce the first 2000 numbers in almost 17 millisecs by",
				"   ,((((+ +/),]),:^:(1=#@$))/\u0026.|.\u003c:/~i.5) (+/ . *)^:(i.400) 1 1 1 1 1x"
			],
			"xref": [
				"Cf. A000045, A000288, A000383, A060455, A186192.",
				"Cf. A001591 (Pentanacci numbers starting 0, 0, 0, 0, 1)."
			],
			"keyword": "nonn,easy",
			"offset": "0,6",
			"author": "_N. J. A. Sloane_.",
			"references": 53,
			"revision": 67,
			"time": "2021-03-12T22:32:33-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}