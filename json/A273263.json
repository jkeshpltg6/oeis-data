{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A273263",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 273263,
			"data": "1,2,2,3,3,3,4,4,5,5,4,5,6,6,7,7,4,6,8,8,7,9,9,4,7,10,10,11,11,4,6,8,10,12,12,13,13,4,9,14,14,11,13,15,15,5,8,12,16,16,17,17,12,11,12,15,18,18,19,19,-3,4,10,15,20,20,13,17,21,21,4,13,22,22,23,23,-4,3,8,12,16,20,24,24,21,25,25",
			"name": "Irregular triangle read by rows: T(n,k) is the sum of the elements of the k-th column of the difference table of the divisors of n.",
			"comment": [
				"If n is prime then row n is [n, n].",
				"It appears that the last two terms of the n-th row are [n, n], n \u003e 1.",
				"First differs from A274533 at a(38)."
			],
			"example": [
				"Triangle begins:",
				"   1;",
				"   2,  2;",
				"   3,  3;",
				"   3,  4,  4;",
				"   5,  5;",
				"   4,  5,  6,  6;",
				"   7,  7;",
				"   4,  6,  8,  8;",
				"   7,  9,  9;",
				"   4,  7, 10, 10;",
				"  11, 11;",
				"   4,  6,  8, 10, 12, 12;",
				"  13, 13;",
				"   4,  9, 14, 14;",
				"  11, 13, 15, 15;",
				"   5,  8, 12, 16, 16;",
				"  17, 17;",
				"  12, 11, 12, 15, 18, 18;",
				"  19, 19;",
				"  -3,  4, 10, 15, 20, 20;",
				"  13, 17, 21, 21;",
				"   4, 13, 22, 22;",
				"  23, 23;",
				"  -4,  3,  8, 12, 16, 20, 24, 24;",
				"  21, 25, 25;",
				"   4, 15, 26, 26;",
				"  ...",
				"For n = 18 the divisors of 18 are 1, 2, 3, 6, 9, 18, and the difference triangle of the divisors is",
				"   1,  2,  3,  6,  9, 18;",
				"   1,  1,  3,  3,  9;",
				"   0,  2,  0,  6;",
				"   2, -2,  6;",
				"  -4,  8;",
				"  12;",
				"The column sums give [12, 11, 12, 15, 18, 18] which is also the 18th row of the irregular triangle."
			],
			"mathematica": [
				"Table[Total /@ Transpose@ Map[Function[w, PadRight[w, Length@ #]], NestWhileList[Differences, #, Length@ # \u003e 1 \u0026]] \u0026@ Divisors@ n, {n, 25}] // Flatten (* _Michael De Vlieger_, Jun 26 2016 *)"
			],
			"program": [
				"(PARI) row(n) = {my(d = divisors(n)); my(nd = #d); my(m = matrix(#d, #d)); for (j=1, nd, m[1,j] = d[j];); for (i=2, nd, for (j=1, nd - i +1, m[i,j] = m[i-1,j+1] - m[i-1,j];);); vector(nd, j, sum(i=1, nd, m[i, j]));}",
				"tabf(nn) = for (n=1, nn, print(row(n)););",
				"lista(nn) = for (n=1, nn, v = row(n); for (j=1, #v, print1(v[j], \", \"));); \\\\ _Michel Marcus_, Jun 25 2016"
			],
			"xref": [
				"Row lengths give A000005. Right border gives A000027. Column 1 is A161857. Row sums give A273103.",
				"Cf. A187202, A273102, A273135, A272210, A273136, A273261, A273262, A274533."
			],
			"keyword": "sign,tabf",
			"offset": "1,2",
			"author": "_Omar E. Pol_, May 22 2016",
			"references": 5,
			"revision": 30,
			"time": "2021-08-31T02:43:03-04:00",
			"created": "2016-06-26T10:21:28-04:00"
		}
	]
}