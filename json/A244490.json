{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A244490",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 244490,
			"data": "1,1,1,1,2,3,1,3,8,18,1,4,15,52,163,1,5,24,110,478,1950,1,6,35,198,1083,5706,28821,1,7,48,322,2110,13482,83824,505876,1,8,63,488,3715,27768,203569,1461944,10270569,1,9,80,702,6078,51894,436656,3618540,29510268,236644092,1,10,99,970,9403,90150,854485,8003950,74058105,676549450,6098971555",
			"name": "Triangle read by rows: T(n,k) (0 \u003c= k \u003c= n) = Sum_{i=0..[k/2]} (-1)^i*binomial(k,2*i)*(2*i-1)!!*n^(k-2*i).",
			"comment": [
				"East and Gray (p. 24) give a combinatorial interpretation of the numbers: A function f: Y -\u003e X with Y \u003c= X (\u003c= inclusion) has a 2-cycle if there exists x, y in Y with x != y, f(x) = y and f(y) = x. Then T(n,k) = card({f : [k] -\u003e [n]: f has no 2-cycles}). For instance T(3,3) = 18 because there are 27 functions [3] -\u003e [3], 9 of which have a 2-cycle. - _Peter Luschny_, Oct 05 2016"
			],
			"link": [
				"J. East, R. D. Gray, \u003ca href=\"http://arxiv.org/abs/1404.2359\"\u003eIdempotent generators in finite partition monoids and related semigroups\u003c/a\u003e, arXiv preprint arXiv:1404.2359 [math.GR], 2014."
			],
			"formula": [
				"From _Peter Luschny_, Oct 05 2016: (Start)",
				"T(n,k) = 2^(-k/2)*HermiteH(k, n/sqrt(2)).",
				"T(n,k) = 2^((k-1)/2)*n*KummerU((1-k)/2, 3/2, n^2/2) for n\u003e=1.",
				"T(n,k) = n^k*hypergeom([-k/2, (1-k)/2], [], -2/n^2) for n\u003e=1. (End)"
			],
			"example": [
				"Triangle begins:",
				"1",
				"1 1",
				"1 2 3",
				"1 3 8 18",
				"1 4 15 52 163",
				"1 5 24 110 478 1950",
				"1 6 35 198 1083 5706 28821",
				"1 7 48 322 2110 13482 83824 505876",
				"1 8 63 488 3715 27768 203569 1461944 10270569",
				"1 9 80 702 6078 51894 436656 3618540 29510268 236644092",
				"..."
			],
			"maple": [
				"T := (n,k) -\u003e add((-1)^i*binomial(k,2*i)*doublefactorial(2*i-1)*n^(k-2*i), i=0..k/2):",
				"seq(seq(T(n,k), k=0..n), n=0..10); # _Peter Luschny_, Oct 05 2016"
			],
			"mathematica": [
				"Table[Simplify[2^(-k/2) HermiteH[k, n/Sqrt[2]]], {n, 0, 10}, {k,0,n}] // Flatten (* _Peter Luschny_, Oct 05 2016 *)"
			],
			"program": [
				"(Sage)",
				"def T(n, k):",
				"    @cached_function",
				"    def h(n, x):",
				"        if n == 0: return 1",
				"        if n == 1: return 2*x",
				"        return 2*(x*h(n-1,x)-(n-1)*h(n-2,x))",
				"    return h(k, n/sqrt(2))/2^(k/2)",
				"for n in range(10):",
				"    print([T(n,k) for k in (0..n)]) # _Peter Luschny_, Oct 05 2016"
			],
			"xref": [
				"As has been noticed by _Tom Copeland_: T(n,0) = A000012(n), T(n,1) = A001477(n) for n\u003e=1, T(n,2) = A067998(n+1) for n\u003e=3, T(n,3) = A121670(n) for n\u003e=3.",
				"Cf. A276999."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_, Jul 05 2014",
			"references": 3,
			"revision": 36,
			"time": "2020-03-07T14:57:40-05:00",
			"created": "2014-07-05T00:01:15-04:00"
		}
	]
}