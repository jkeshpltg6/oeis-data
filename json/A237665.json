{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A237665",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 237665,
			"data": "0,0,0,1,1,3,3,6,6,10,11,16,17,24,27,35,39,50,57,70,79,97,111,132,150,178,204,239,271,316,361,416,472,545,618,706,800,912,1032,1173,1320,1496,1687,1902,2137,2410,2702,3034,3398,3808,4258,4765,5313,5932,6613",
			"name": "Number of partitions of n such that the distinct terms arranged in increasing order form a string of two or more consecutive integers.",
			"comment": [
				"Number of partitions of n with maximal distance between parts = 1; column k=1 of A238353. [_Joerg Arndt_, Mar 23 2014]",
				"Conjecture:  a(n+1) = sum of smallest parts in the distinct partitions of n with an even number of parts. - _George Beck_, May 06 2017"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A237665/b237665.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Shane Chern (Xiaohang Chen), \u003ca href=\"https://sites.psu.edu/shanechern/files/2018/07/On-a-conjecture-of-George-Beck-II-2dpatgk.pdf\"\u003eOn a conjecture of George Beck. II\u003c/a\u003e, 2018."
			],
			"example": [
				"The qualifying partitions of 8 are 332, 3221, 32111, 22211, 221111, 2111111, so that a(8) = 6.  (The strings of distinct parts arranged in increasing order are 23, 123, 123, 12, 12, 12.)"
			],
			"maple": [
				"b:= proc(n, i, t) option remember;",
				"      `if`(n=0 or i=1, `if`(n=0 and t=2 or n\u003e0 and t\u003e0, 1, 0),",
				"      `if`(i\u003en, 0, add(b(n-i*j, i-1, min(t+1, 2)), j=1..n/i)))",
				"    end:",
				"a:= n-\u003e add(b(n, i, 0), i=1..n):",
				"seq(a(n), n=0..60);  # _Alois P. Heinz_, Feb 15 2014"
			],
			"mathematica": [
				"Map[Length[Select[Map[Differences[DeleteDuplicates[#]] \u0026, IntegerPartitions[#]], (Table[-1, {Length[#]}] == # \u0026\u0026 # =!= \\{}) \u0026]] \u0026, Range[55]] (* _Peter J. C. Moses_, Feb 09 2014 *)",
				"b[n_, i_, t_] := b[n, i, t] = If[n==0 || i==1, If[n==0 \u0026\u0026 t==2 || n\u003e0 \u0026\u0026 t \u003e 0, 1, 0], If[i\u003en, 0, Sum[b[n-i*j, i-1, Min[t+1, 2]], {j, 1, n/i}]]]; a[n_] := Sum[b[n, i, 0], {i, 1, n}]; Table[a[n], {n, 0, 60}] (* _Jean-François Alcover_, Nov 17 2015, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A034296, A237666, A092265."
			],
			"keyword": "nonn,easy",
			"offset": "0,6",
			"author": "_Clark Kimberling_, Feb 11 2014",
			"references": 6,
			"revision": 37,
			"time": "2018-11-20T16:33:52-05:00",
			"created": "2014-02-15T13:36:56-05:00"
		}
	]
}