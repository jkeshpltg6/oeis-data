{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A343046",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 343046,
			"data": "0,0,0,0,1,0,0,2,2,0,0,3,6,3,0,0,2,8,8,2,0,0,3,6,9,6,3,0,0,6,8,8,8,8,6,0,0,7,30,9,12,9,30,7,0,0,8,32,36,14,14,36,32,8,0,0,9,36,39,30,15,30,39,36,9,0,0,8,38,38,32,36,36,32,38,38,8,0",
			"name": "Array T(n, k), n, k \u003e= 0, read by antidiagonals; lunar multiplication table for the primorial base.",
			"comment": [
				"To compute T(n, k):",
				"- write the primorial base representations of n and of k on two lines, right aligned,",
				"- to \"multiply\" two digits: take the smallest,",
				"- to \"add\" two digits: take the largest,",
				"- for example, for T(9, 10):",
				"      9   -\u003e   1 1 1",
				"     10   -\u003e x 1 2 0",
				"             -------",
				"               0 0 0",
				"             1 1 1",
				"         + 1 1 1",
				"         -----------",
				"           1 1 1 1 0  -\u003e  248 = T(9, 10)",
				"See A343044 for the corresponding addition table."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A343046/b343046.txt\"\u003eTable of n, a(n) for n = 0..10010\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A343046/a343046.gp.txt\"\u003ePARI program for A343046\u003c/a\u003e",
				"\u003ca href=\"/index/Di#dismal\"\u003eIndex entries for sequences related to dismal (or lunar) arithmetic\u003c/a\u003e",
				"\u003ca href=\"/index/Pri#primorialbase\"\u003eIndex entries for sequences related to primorial base\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = T(k, n).",
				"T(m, T(n, k)) = T(T(m, n), k).",
				"T(n, 0) = 0.",
				"T(n, 1) = A328841(n).",
				"T(n, n) = A343047(n)."
			],
			"example": [
				"Array T(n, k) begins:",
				"  n\\k|  0  1   2   3   4   5    6    7    8    9   10   11   12",
				"  ---+---------------------------------------------------------",
				"    0|  0  0   0   0   0   0    0    0    0    0    0    0    0",
				"    1|  0  1   2   3   2   3    6    7    8    9    8    9    6  -\u003e  A328841",
				"    2|  0  2   6   8   6   8   30   32   36   38   36   38   30",
				"    3|  0  3   8   9   8   9   36   39   38   39   38   39   36",
				"    4|  0  2   6   8  12  14   30   32   36   38   42   44   60",
				"    5|  0  3   8   9  14  15   36   39   38   39   44   45   66",
				"    6|  0  6  30  36  30  36  210  216  240  246  240  246  210",
				"    7|  0  7  32  39  32  39  216  217  248  249  248  249  216",
				"    8|  0  8  36  38  36  38  240  248  246  248  246  248  240",
				"    9|  0  9  38  39  38  39  246  249  248  249  248  249  246",
				"   10|  0  8  36  38  42  44  240  248  246  248  252  254  270",
				"   11|  0  9  38  39  44  45  246  249  248  249  254  255  276",
				"   12|  0  6  30  36  60  66  210  216  240  246  270  276  420"
			],
			"program": [
				"(PARI) See Links section."
			],
			"xref": [
				"Cf. A087062, A235168, A328841, A343042, A343044, A343047."
			],
			"keyword": "nonn,base,tabl",
			"offset": "0,8",
			"author": "_Rémy Sigrist_, Apr 05 2021",
			"references": 3,
			"revision": 13,
			"time": "2021-04-08T10:49:22-04:00",
			"created": "2021-04-07T19:59:04-04:00"
		}
	]
}