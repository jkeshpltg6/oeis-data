{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A322034",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 322034,
			"data": "0,1,1,3,1,2,1,7,4,3,1,5,1,4,2,15,1,13,1,4,8,6,1,11,6,7,13,11,1,7,1,31,4,9,8,31,1,10,14,9,1,29,1,17,7,12,1,23,8,31,6,10,1,20,12,25,20,15,1,17,1,16,29,63,14,15,1,13,8,43,1,67",
			"name": "Let p1 \u003c= p2 \u003c= ... \u003c= pk be the prime factors of n, with repetition; let s = 1/p1 + 1/(p1*p2) + 1/(p1*p2*p3) + ... + 1/(p1*p2*...*pk); a(n) = numerator of s. a(1)=0 by convention.",
			"comment": [
				"Note that s \u003c 1 for all n (compare A322036). This follows easily by induction, since when we increase n by multiplying it by a new (not-smaller) prime, we increase s by less than 1-s."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A322034/b322034.txt\"\u003eTable of n, a(n) for n = 1..16384\u003c/a\u003e",
				"Antti Karttunen, \u003ca href=\"/A322034/a322034.txt\"\u003eData supplement: n, a(n) computed for n = 1..65537\u003c/a\u003e"
			],
			"example": [
				"If n=12 we get the prime factors 2,2,3, and s = 1/2 + 1/4 + 1/12 = 5/6. So a(12) = 5.",
				"The fractions s for n \u003e= 2 are 1/2, 1/3, 3/4, 1/5, 2/3, 1/7, 7/8, 4/9, 3/5, 1/11, 5/6, 1/13, 4/7, 2/5, 15/16, 1/17, 13/18, 1/19, 4/5, 8/21, ..."
			],
			"maple": [
				"# This generates the terms starting at n=2:",
				"P:=proc(n) local FM: FM:=ifactors(n)[2]: seq(seq(FM[j][1], k=1..FM[j][2]), j=1..nops(FM)) end: # A027746",
				"f0:=[]; f1:=[]; f2:=[];",
				"for n from 2 to 120 do",
				"a:=0; b:=1; t1:=[P(n)];",
				"for i from 1 to nops(t1) do b:=b/t1[i]; a:=a+b; od;",
				"f0:=[op(f0),a]; f1:=[op(f1), numer(a)]; f2:=[op(f2),denom(a)]; od:",
				"f0;    # s",
				"f1;    # A322034",
				"f2;    # A322035",
				"f2-f1; # A322036"
			],
			"program": [
				"(PARI) A322034(n) = if(1==n,0,my(f=factor(n),pm=1,s=0); for(i=1,#f~,while(f[i,2],pm *= f[i,1]; f[i,2]--; s += 1/pm)); numerator(s)); \\\\ _Antti Karttunen_, Feb 28 2019"
			],
			"xref": [
				"Cf. A006022, A027746, A322035, A322036.",
				"A017665/A017666 = sum of reciprocals of all divisors of n."
			],
			"keyword": "nonn,frac",
			"offset": "1,4",
			"author": "_N. J. A. Sloane_ and _David James Sycamore_, Nov 28 2018",
			"references": 5,
			"revision": 33,
			"time": "2019-06-07T16:30:31-04:00",
			"created": "2018-11-28T01:41:20-05:00"
		}
	]
}