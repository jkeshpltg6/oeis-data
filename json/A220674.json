{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A220674",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 220674,
			"data": "1,7,2,0,3,1,1,4,2,9,7,3,7,1,7,1,6,6,2,6,1,8,8,1,7,8,1,0,2,8,4,9,4,7,9,7,6,1,6,1,2,0,3,4,6,8,1,1,1,8,9,7,9,1,2,7,4,5,8,4,2,5,3,3,3,2,2,7,4,2,5,3,9,8,5,9,6,0,2,9,0,4,8,3,9,0,6,2,5,2,9,6,1,6,0,8,6,1,2,8",
			"name": "Decimal expansion of the area of Dürer's approximation of a regular pentagon with each side of unit length.",
			"comment": [
				"To be read as dimensionless area F_D/r^2 = 1.720311429... where the length of each side is r, which is the radius of each circle in Dürer's construction. See the link Dürer, Zweites Buch, figure 16. Compare this with the regular pentagon with unit side length, which is given in A102771, and is F_5/r^2 = 1.720477400... The relative error is about -0.96*10^{-4}.",
				"The angles in Dürer's pentagon are approximately twice 108.3661201 degrees, twice 107.0378260 degrees and once 109.1921079 degrees. The sum has to be exactly 3*Pi, or 540 degrees, as for any pentagon. For the analytic values see the W. Lang link.",
				"Alonso del Arte pointed out the Hughes reference where this construction is shown on p.5 and p. 16. See also the historical remarks on p. 17.",
				"In the cut-the-knot link this construction is considered in more detail, and the two interior angles at the bottom of the pentagon are shown to be 108.36612.. degrees.",
				"For more references and links see the W. Lang link. There also the length of the dimensionless diagonals which approximate the golden section are given. Also the angles of the companion of Dürer's pentagon with the same area are there computed. - _Wolfdieter Lang_, Feb 14 2013"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A220674/b220674.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Cut-The-Knot, \u003ca href=\"http://www.cut-the-knot.org/pythagoras/DurerPentagon.shtml\"\u003e Approximate Construction of Regular Pentagon by A. Dürer \u003c/a\u003e",
				"G. Hughes, \u003ca href=\"http://arxiv.org/abs/1205.0080\"\u003eThe Polygons of Albrecht Durer -1525.\u003c/a\u003e",
				"Wolfdieter Lang, \u003ca href=\"/A220674/a220674_3.pdf\"\u003eAlbrecht Dürer's approximation of a regular 5-gon\u003c/a\u003e.",
				"Wikimedia Commons, \u003ca href=\"http://commons.wikimedia.org/wiki/File:Duerer_Underweysung_der_Messung_005.jpg\"\u003eAlbrecht Dürer, Underweysung der messung ..., 1525, title page\u003c/a\u003e.",
				"Wikisource \u003ca href=\"http://de.wikisource.org/wiki/Underweysung_der_Messung,_mit_dem_Zirckel_und_Richtscheyt,_in_Linien,_Ebenen_unnd_gantzen_corporen/Zweites_Buch\"\u003e Albrecht Dürer, Underweysung der messung ..., Zweites Buch, 1525,\u003c/a\u003e (in German)."
			],
			"formula": [
				"The dimensionless area of Duerer's pentagon is",
				"F_D/r^2 = (1 + 2*x)*y1/2 + x*y2, with x = (a + sqrt(a*(a+8)))/4, a := sqrt(3) - 1 , y1 = 1 - sqrt(3)/2 + x, y2 = sqrt(1 - x^2). The approximate values for x, y1 and y2 are 0.8150878978, 0.9490624938, 0.5793373101, respectively. This leads to the approximate value 1.720311430 for F_D/r^2, and the present sequence gives more accurate digits."
			],
			"mathematica": [
				"r = Sqrt[7 - 3*Sqrt[3] + 2*(Sqrt[3]-1)* Cos[a]]; area = (2 + Sqrt[3] + r - 2*Cos[a]*(r+2))/4 /. Cos[a] -\u003e (3 - Sqrt[3] - Sqrt[6*Sqrt[3]-4])/4; RealDigits[area, 10, 100] // First (* _Jean-François Alcover_, Feb 13 2013 *)"
			],
			"xref": [
				"Cf. A102771 (pentagon area)."
			],
			"keyword": "nonn,cons",
			"offset": "1,2",
			"author": "_Wolfdieter Lang_, Jan 30 2013",
			"references": 1,
			"revision": 39,
			"time": "2018-03-07T02:29:50-05:00",
			"created": "2013-02-07T09:43:49-05:00"
		}
	]
}