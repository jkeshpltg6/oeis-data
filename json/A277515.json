{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A277515",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 277515,
			"data": "3,3,3,3,7,3,3,3,3,13,3,3,3,3,3,7,3,3,3,3,11,3,3,3,3,3,19,3,3,3,3,11,3,3,3,3,3,13,3,3,3,3,7,3,3,3,3,3,5,3,3,3,3,7,3,3,3,3,7,3,3,3,3,3,7,3,3,3,3,11,3,3,3,3,3,7,3,3,3,3,13,3,3,3,3,3,7,3,3,3,3,19,3,3,3,3,3,5,3,3",
			"name": "Smallest prime p such that n sqrt(2) \u003c m sqrt(p) \u003c (n+1) sqrt(2) for some integer m.",
			"comment": [
				"In fact, m is both the ceiling of the square root of 2n^2/p and the floor of the square root of 2(n+1)^2 / p.",
				"Eggleton et al. show that a(n)=3 if and only if n is a term in A277644.",
				"First occurrence of the n-th prime \u003e 2: 1, 49, 5, 21, 10, 174, 27, 223, 1656, 3901, 1286, 1847, 5095, 3117, 5678, 1727, 14844, 23678, 10986, 33868, 41241, 42794, 50451, 35301, 39546, 206241, 10561, 89600, 50075, 87273, 75922, 142760, 3493, 236213, 277242, 805287, 619149, 333339, 308517, 186105, 109981, 1385669, 215516, 1389450, 130253, 29797, 368004, 584234, 879460, 1711711, 6061772, 2401437, 1891953, 3664144, 1465847, 3260206, 2908877, 4414026, 1338945, 506017, 5420710, ..., . - _Robert G. Wilson v_, Nov 17 2016"
			],
			"reference": [
				"R. B. Eggleton, J. S. Kimberley and J. A. MacDougall, Square-free rank of integers, submitted."
			],
			"link": [
				"Jason Kimberley, \u003ca href=\"/A277515/b277515.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"a(5)=7 because 3 r(5) \u003c 4 r(3) \u003c 5 r(2) \u003c 3 r(7) \u003c 6 r(2) \u003c 5 r(3) \u003c 4 r(5), where r(x) is the square root of x."
			],
			"mathematica": [
				"f[n_] := Block[{p = 2}, While[ Ceiling[ Sqrt[2 n^2/p]] != Floor[ Sqrt[2 (n + 1)^2/p]], p = NextPrime@ p]; p]; Array[f, 80] (* _Robert G. Wilson v_, Nov 17 2016 *)"
			],
			"program": [
				"(Magma)",
				"function A277515(n)",
				"  p := 2;",
				"  lower := 2*n^2;",
				"  upper := 2*(n+1)^2;",
				"  repeat",
				"    p := NextPrime(p);",
				"    m := Isqrt(upper div p);",
				"  until p*m^2 gt lower;",
				"  return p;",
				"end function;",
				"[A277515(n):n in[1..100]];"
			],
			"xref": [
				"First occurrences of each prime \u003e 2 are listed in A278107.",
				"Cf. A277644 and A277645."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_Jason Kimberley_, Oct 18 2016",
			"references": 4,
			"revision": 17,
			"time": "2016-12-31T01:46:58-05:00",
			"created": "2016-12-31T01:46:58-05:00"
		}
	]
}