{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A174450",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 174450,
			"data": "1,1,1,1,12,1,1,24,24,1,1,40,960,40,1,1,60,2400,2400,60,1,1,84,5040,201600,5040,84,1,1,112,9408,564480,564480,9408,112,1,1,144,16128,1354752,81285120,1354752,16128,144,1,1,180,25920,2903040,243855360,243855360,2903040,25920,180,1",
			"name": "Triangle T(n, k, q) = n!*(n+1)!*q^k/((n-k)!(n-k+1)!) if floor(n/2) \u003e k-1, otherwise n!*(n+1)!*q^(n-k)/(k!*(k+1)!) for q = 2, read by rows.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A174450/b174450.txt\"\u003eRows n = 0..50 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k, q) = n!*(n+1)!*q^k/((n-k)!(n-k+1)!) if floor(n/2) \u003e k-1, otherwise n!*(n+1)!*q^(n-k)/(k!*(k+1)!) for q = 2.",
				"T(n, n-k, q) = T(n, k, q).",
				"From _G. C. Greubel_, Nov 29 2021: (Start)",
				"T(2*n, n, q) = q^n*(2*n+1)!*Catalan(n) for q = 2.",
				"T(n, k, q) = binomial(n, k)*binomial(n+1, k+1) * ( k!*(k+1)!*q^k/(n-k+1) if (floor(n/2) \u003e= k), otherwise ((n-k)!)^2*q^(n-k) ), for q = 2. (End)"
			],
			"example": [
				"Triangle begins as:",
				"  1;",
				"  1,   1;",
				"  1,  12,     1;",
				"  1,  24,    24,       1;",
				"  1,  40,   960,      40,         1;",
				"  1,  60,  2400,    2400,        60,         1;",
				"  1,  84,  5040,  201600,      5040,        84,       1;",
				"  1, 112,  9408,  564480,    564480,      9408,     112,     1;",
				"  1, 144, 16128, 1354752,  81285120,   1354752,   16128,   144,   1;",
				"  1, 180, 25920, 2903040, 243855360, 243855360, 2903040, 25920, 180,  1;"
			],
			"mathematica": [
				"T[n_, k_, q_]:= If[Floor[n/2]\u003ek-1, n!*(n+1)!*q^k/((n-k)!*(n-k+1)!), n!*(n+1)!*q^(n-k)/(k!*(k+1)!)];",
				"Table[T[n, k, 2], {n,0,12}, {k,0,n}]//Flatten"
			],
			"program": [
				"(MAGMA)",
				"F:= Factorial; // T = A174450",
				"T:= func\u003c n,k,q | Floor(n/2) gt k-1 select F(n)*F(n+1)*q^k/(F(n-k)*F(n-k+1)) else F(n)*F(n+1)*q^(n-k)/(F(k)*F(k+1)) \u003e;",
				"[T(n,k,2): k in [0..n], n in [0..12]]; // _G. C. Greubel_, Nov 29 2021",
				"(Sage)",
				"f=factorial",
				"def A174450(n,k,q):",
				"    if ((n//2)\u003ek-1): return f(n)*f(n+1)*q^k/(f(n-k)*f(n-k+1))",
				"    else: return f(n)*f(n+1)*q^(n-k)/(f(k)*f(k+1))",
				"flatten([[A174450(n,k,2) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, Nov 29 2021"
			],
			"xref": [
				"Cf. A174449 (q=1), this sequence (q=2), A174451 (q=3).",
				"Cf. A000108."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "0,5",
			"author": "_Roger L. Bagula_, Mar 20 2010",
			"ext": [
				"Edited by _G. C. Greubel_, Nov 29 2021"
			],
			"references": 3,
			"revision": 10,
			"time": "2021-11-30T05:00:15-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}