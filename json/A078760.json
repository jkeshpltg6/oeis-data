{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A078760",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 78760,
			"data": "1,1,1,2,1,3,6,1,4,6,12,24,1,5,10,20,30,60,120,1,6,15,30,20,60,120,90,180,360,720,1,7,21,42,35,105,210,140,210,420,840,630,1260,2520,5040,1,8,28,56,56,168,336,70,280,420,840,1680,560,1120,1680,3360,6720,2520",
			"name": "Combinations of a partition: number of ways to label a partition (of size n) with numbers 1 to n.",
			"comment": [
				"This is a function of the individual partitions of an integer. The number of values in each line is given by A000041; thus lines 0 to 5 of the sequence are (1), (1), (1,2), (1,3,6), (1,4,6,12,24). The partitions in each line are ordered with the largest part sizes first, so the line 4 indices are [4], [3,1], [2,2], [2,1,1] and [1,1,1,1]. Note that exponents are often used to represent repeated values in a partition, so the last index could instead be written [1^4]. The combination function (sequence A007318) C(n,m) = C([m,n-m]).",
				"This sequence is also the sequence of multinomial coefficients for partitions ordered lexicographically, matching partition sequence A080577. This is different ordering than in sequence A036038 of multinomial coefficients. - _Sergei Viznyuk_, Mar 15 2012"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A078760/b078760.txt\"\u003eRows n=0..25 of triangle, flattened\u003c/a\u003e",
				"S.-H. Cha, E. G. DuCasse, and L. V. Quintas, \u003ca href=\"http://arxiv.org/abs/1405.5283\"\u003eGraph Invariants Based on the Divides Relation and Ordered by Prime Signatures\u003c/a\u003e, arxiv:1405.5283 [math.NT], 2014.",
				"Sergei Viznyuk, \u003ca href=\"http://phystech.com/ftp/s_A209936.c\"\u003eC Program\u003c/a\u003e",
				"\u003ca href=\"/index/Pas#Pascal\"\u003eIndex entries for triangles and arrays related to Pascal's triangle\u003c/a\u003e."
			],
			"formula": [
				"C([\u003ca(i)\u003e]) = (Sum a(i))! / Product a(i) !.",
				"T(n,k) = A008480(A063008(n,k)). - _Andrew Howroyd_, Mar 25 2020"
			],
			"example": [
				"The irregular table starts:",
				"[0] {1},",
				"[1] {1},",
				"[2] {1, 2},",
				"[3] {1, 3,  6},",
				"[4] {1, 4,  6, 12, 24},",
				"[5] {1, 5, 10, 20, 30, 60, 120},",
				"[6] {1, 6, 15, 30, 20, 60, 120, 90, 180, 360, 720}",
				".",
				"C([2,1]) = 3 for the labelings ({1,2},{3}), ({1,3},{2}) and ({2,3},{2})."
			],
			"maple": [
				"g:= n-\u003e (l-\u003e add(i, i=l)!/mul(i!, i=l))(map(i-\u003e i[2], ifactors(n)[2])):",
				"b:= (n, i)-\u003e `if`(n=0 or i=1, [[1$n]], [map(x-\u003e",
				"    [i, x[]], b(n-i, min(n-i, i)))[], b(n, i-1)[]]):",
				"T:= n-\u003e map(x-\u003e g(mul(ithprime(i)^x[i], i=1..nops(x))), b(n$2))[]:",
				"seq(T(n), n=0..9);  # _Alois P. Heinz_, Mar 25 2020"
			],
			"mathematica": [
				"Flatten[Table[Apply[Multinomial, IntegerPartitions[i], {1}], {i,0,25}] (* _T. D. Noe_, Oct 14 2007 *)",
				"Flatten[ Multinomial @@@ IntegerPartitions @ # \u0026 /@ Range[ 0, 8]] (* _Michael Somos_, Feb 05 2011 *)",
				"g[n_] := With[{ee = FactorInteger[n][[All, 2]]}, Total[ee]!/Times@@(ee!)];",
				"b[n_, i_] := b[n, i] = If[n == 0 || i == 1, {Table[1, {n}]}, Join[ Prepend[#, i] \u0026 /@ b[n - i, Min[n - i, i]], b[n, i - 1]]];",
				"row[n_] := Product[Prime[i]^#[[i]], {i, 1, Length[#]}] \u0026 /@ b[n, n];",
				"T[n_] := g /@ row[n];",
				"T /@ Range[0, 9] // Flatten (* _Jean-François Alcover_, Jun 09 2021, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(PARI)",
				"C(sig)={vecsum(sig)!/vecprod(apply(k-\u003ek!, sig))}",
				"Row(n)={apply(C, vecsort([Vecrev(p) | p\u003c-partitions(n)], , 4))}",
				"{ for(n=0, 8, print(Row(n))) }  \\\\ _Andrew Howroyd_, Mar 25 2020"
			],
			"xref": [
				"Different from A036038.",
				"Cf. A000041, A008480, A063008, A080577."
			],
			"keyword": "nice,easy,nonn,tabf,look",
			"offset": "0,4",
			"author": "_Franklin T. Adams-Watters_, Jan 08 2003",
			"references": 6,
			"revision": 52,
			"time": "2021-06-09T05:24:26-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}