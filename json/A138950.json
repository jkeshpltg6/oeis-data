{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A138950",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 138950,
			"data": "1,1,-3,1,2,-3,0,1,1,2,0,-3,2,0,-6,1,2,1,0,2,0,0,0,-3,3,2,-3,0,2,-6,0,1,0,2,0,1,2,0,-6,2,2,0,0,0,2,0,0,-3,1,3,-6,2,2,-3,0,0,0,2,0,-6,2,0,0,1,4,0,0,2,0,0,0,1,2,2,-9,0,0,-6,0,2,1,2",
			"name": "Expansion of (2 - 3 * phi(q^3)^2 + phi(q)^2) / 4 in powers of q where phi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A138950/b138950.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of (1 - eta(q)^2 * eta(q^2) * eta(q^6)^3 / (eta(q^3)^2 * eta(q^4) * eta(q^12))) / 2 in powers of q.",
				"Moebius transform is period 12 sequence [ 1, 0, -4, 0, 1, 0, -1, 0, 4, 0, -1, 0, ...].",
				"a(n) is multiplicative with a(2^e) = 1, a(3^e) = -1 + 2 * (-1)^e, a(p^e) = e+1 if p == 1, 5 (mod 12), a(p^e) = (1 + (-1)^e) / 2 if p == 7, 11 (mod 12).",
				"G.f.: Sum_{k\u003e0} f(3*k - 2) + f(3*k - 1) - 2 * f(3*k) where f(n) := x^n / (1 + x^(2*n)).",
				"a(12*n + 7) = a(12*n + 11) = 0. a(2*n) = a(n). a(2*n + 1) = A116604(n).",
				"-2 * a(n) = A138949(n) unless n=0. a(3*n + 1) = A122865(n). a(3*n + 2) = A122856(n). a(4*n + 1) = A008441(n)."
			],
			"example": [
				"G.f. = q + q^2 - 3*q^3 + q^4 + 2*q^5 - 3*q^6 + q^8 + q^9 + 2*q^10 - 3*q^12 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 1, 0, DivisorSum[ n, KroneckerSymbol[ -4, n/#] {1, 1, -2}[[Mod[#, 3, 1]]] \u0026]]; (* _Michael Somos_, Sep 07 2015 *)",
				"a[ n_] := SeriesCoefficient[ (2 - 3 EllipticTheta[ 3, 0, q^3]^2 + EllipticTheta[ 3, 0, q]^2) / 4, {q, 0, n}]; (* _Michael Somos_, Sep 07 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, 0, - sumdiv(n, d, kronecker(-4, n/d) * [2, -1, -1][d%3 + 1]))};",
				"(PARI) {a(n) = my(A, p, e); if( n\u003c1, 0, A = factor(n); prod( k=1, matsize(A)[1], [p, e] = A[k, ]; if( p==2, 1, p==3, -1 + 2 * (-1)^e, p%12 \u003c 6, e+1, 1-e%2)))};"
			],
			"xref": [
				"Cf. A008441, A116604, A122856, A122865, A138949."
			],
			"keyword": "sign,mult",
			"offset": "1,3",
			"author": "_Michael Somos_, Apr 03 2008",
			"references": 7,
			"revision": 9,
			"time": "2021-03-12T22:24:45-05:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}