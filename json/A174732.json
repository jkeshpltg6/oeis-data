{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A174732",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 174732,
			"data": "1,1,1,1,-51,1,1,-399,-399,1,1,-2177,-4597,-2177,1,1,-10191,-35671,-35671,-10191,1,1,-43719,-227343,-380363,-227343,-43719,1,1,-177119,-1279199,-3207839,-3207839,-1279199,-177119,1,1,-688869,-6593469,-23126349,-34699365,-23126349,-6593469,-688869,1",
			"name": "Triangle T(n, k, q) = (1-q^n)*(1/k)*binomial(n-1, k-1)*binomial(n, k-1) - (1-q^n) + 1, for q = 3, read by rows.",
			"comment": [
				"From _G. C. Greubel_, Feb 09 2021: (Start)",
				"The triangle coefficients are connected to the Narayana numbers by T(n, k, q) = (1-q^n)*(A001263(n, k) - 1) + 1, for varying q values.",
				"The row sums of this class of sequences, for varying q, is given by Sum_{k=1..n} T(n, k, q) = q^n * n + (1 - q^n)*C_{n}, where C_{n} are the Catalan numbers (A000108). (End)"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A174732/b174732.txt\"\u003eRows n = 1..100 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k, q) = (1-q^n)*(1/k)*binomial(n-1, k-1)*binomial(n, k-1) - (1-q^n) + 1, for q = 3.",
				"From _G. C. Greubel_, Feb 09 2021: (Start)",
				"T(n, k, 3) = (1-3^n)*(A001263(n,k) - 1) + 1.",
				"Sum_{k=1..n} T(n, k, 3) = 3^n * n + (1 - 3^n)*A000108(n). (End)"
			],
			"example": [
				"Triangle begins as:",
				"  1;",
				"  1,       1;",
				"  1,     -51,        1;",
				"  1,    -399,     -399,         1;",
				"  1,   -2177,    -4597,     -2177,         1;",
				"  1,  -10191,   -35671,    -35671,    -10191,         1;",
				"  1,  -43719,  -227343,   -380363,   -227343,    -43719,        1;",
				"  1, -177119, -1279199,  -3207839,  -3207839,  -1279199,  -177119,       1;",
				"  1, -688869, -6593469, -23126349, -34699365, -23126349, -6593469, -688869, 1;"
			],
			"mathematica": [
				"T[n_, k_, q_]:= 1 + (1-q^n)*(1/k)*(Binomial[n-1, k-1]*Binomial[n, k-1] - k);",
				"Table[T[n, k, 3], {n, 12}, {k, n}]//Flatten"
			],
			"program": [
				"(Sage)",
				"def T(n,k,q): return 1 + (1-q^n)*(1/k)*(binomial(n-1, k-1)*binomial(n, k-1) - k)",
				"flatten([[T(n,k,3) for k in (1..n)] for n in (1..12)]) # _G. C. Greubel_, Feb 09 2021",
				"(Magma)",
				"T:= func\u003c n,k,q | 1 +(1-q^n)*(1/k)*(Binomial(n-1, k-1)*Binomial(n, k-1) - k) \u003e;",
				"[T(n,k,3): k in [1..n], n in [1..12]]; // _G. C. Greubel_, Feb 09 2021"
			],
			"xref": [
				"Cf. A000108, A001263.",
				"Cf. A000012 (q=1), A174731 (q=2), this sequence (q=3), A174733 (q=4)."
			],
			"keyword": "sign,tabl",
			"offset": "1,5",
			"author": "_Roger L. Bagula_, Mar 28 2010",
			"ext": [
				"Edited by _G. C. Greubel_, Feb 09 2021"
			],
			"references": 3,
			"revision": 9,
			"time": "2021-02-09T21:40:22-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}