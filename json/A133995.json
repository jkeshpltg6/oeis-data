{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A133995",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 133995,
			"data": "0,0,0,0,0,4,0,6,6,4,6,8,0,8,9,10,0,4,6,8,10,12,6,9,10,12,6,10,12,14,0,4,8,10,12,14,15,16,0,6,8,12,14,15,16,18,6,9,12,14,15,18,4,6,8,10,12,14,16,18,20,0,9,10,14,15,16,18,20,21,22,10,15,20,4,6,8,10,12,14,16,18,20,22,24,6,12,15,18,21,24",
			"name": "Irregular array read by rows: n-th row contains (in numerical order) the positive integers \u003c= n which are neither divisors of n nor are coprime to n. A 0 is put into row n if there are no such integers.",
			"comment": [
				"Row n has length A264441(n).",
				"The number of nonzero entries in row n is A045763(n).",
				"Row n has a 0 if every positive integer \u003c= n is coprime to n or divides n.",
				"From _Michael De Vlieger_, Aug 19 2017: (Start)",
				"When row n is not empty (and here represented by 0), the terms of row n are composite, since primes p \u003c n must either divide or be coprime to n and the empty product 1 both divides and is coprime to all numbers. For the following, let prime p divide n and prime q be coprime to n.",
				"Row n is empty for n \u003c 8 except n = 6.",
				"There are two distinct species of term m of row n. The first are nondivisor regular numbers g in A272618(n) that divide some integer power e \u003e 1 of n. In other words, these numbers are products of primes p that also divide n and no primes q that are coprime to n, yet g itself does not divide n. Prime powers n = p^k cannot have numbers g in A272618(n) since they have only one distinct prime divisor p; all regular numbers g = p^e with 0 \u003c= e \u003c= k divide p^k. The smallest n = 6 for which there is a number in A272618. The number 4 is the smallest composite and is equal to n = 4 thus must divide it; 4 is coprime to 5. The number 4 is neither coprime to nor a divisor of 6.",
				"The second are numbers h in A272619(n) that are products of at least one prime p that divides n and one prime q that is coprime to n.",
				"The smallest n = 8 for which there is a number in A272619 is 8; the number 6 is the product of the smallest two distinct primes. 6 divides 6 and is coprime to 7. The number 6 is neither coprime to nor a divisor of the prime power 8; 4 divides 8 and does not appear in a(8).",
				"There can be no other species since primes p \u003c= n divide n and q \u003c n are coprime to n, and products of primes q exclusive of any p are coprime to n.",
				"As a consequence of these two species, rows 1 \u003c= n \u003c= 5 and n = 7 are empty and thus have 0 in row n.",
				"(End)"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A133995/b133995.txt\"\u003eTable of n, a(n) for n = 1..10014\u003c/a\u003e (rows 1 to 237, flattened)"
			],
			"formula": [
				"a(n) = union(A272618(n), A272619(n)). - _Michael De Vlieger_, Aug 19 2017"
			],
			"example": [
				"The divisors of 12 are: 1,2,3,4,6,12. The positive integers which are \u003c= 12 and are coprime to 12 are: 1,5,7,11. So row 12 contains the positive integers \u003c= 12 which are in neither of these two lists: 8,9,10.",
				"The irregular triangle T(n, k) begins:",
				"n\\k 1  2  3  4  5  6  7 ...",
				"1:  0",
				"2:  0",
				"3:  0",
				"4:  0",
				"5:  0",
				"6:  4",
				"7:  0",
				"8:  6",
				"9:  6",
				"10: 4  6  8",
				"11: 0",
				"12: 8  9 10",
				"13: 0",
				"14: 4  6  8 10 12",
				"15: 6  9 10 12",
				"16: 6 10 12 14",
				"17: 0",
				"18: 4  8 10 12 14 15 16",
				"19: 0",
				"20: 6  8 12 14 15 16 18",
				"... formatted by _Wolfdieter Lang_, Jan 16 2016"
			],
			"maple": [
				"row:= proc(n) local r;",
				"   r:= remove(t -\u003e member(igcd(t, n), [1, t]), [$1..n]):",
				"   if r = [] then 0 else op(r) fi",
				"end proc:",
				"A:= [seq](row(n), n=1..30); # _Robert Israel_, Jan 19 2016"
			],
			"mathematica": [
				"Table[Select[Range@ n, Nor[Divisible[n, #], CoprimeQ[n, #]] \u0026] /. {} -\u003e {0}, {n, 27}] // Flatten (* _Michael De Vlieger_, Aug 19 2017 *)"
			],
			"xref": [
				"Cf. A045763, A133994, A264441, A272618, A272619."
			],
			"keyword": "nonn,tabf",
			"offset": "1,6",
			"author": "_Leroy Quet_, Oct 01 2007",
			"ext": [
				"More terms from _Alvin Hoover Belt_, Jan 21 2008",
				"Edited by _Wolfdieter Lang_, Jan 16 2016"
			],
			"references": 14,
			"revision": 34,
			"time": "2021-03-20T11:04:17-04:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}