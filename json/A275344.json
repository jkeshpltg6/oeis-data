{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A275344",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 275344,
			"data": "1,1,2,1,2,1,2,1,2,3,2,2,2,2,1,1,1,3,5,3,4,3,3,2,4,1,4,3,3,4,1,4,3,1,4,3,3,8,3,2,3,2,3,2,3,3,3,4,2,2,9,3,8,7,5,5,4,2,6,4,4,9,4,4,5,4,3,8,6,5,6,5,5,5,4,2,5,5,4,6,4",
			"name": "Number of ordered ways to write n as x^2 + y^2 + z^2 + 2*w^2 with x + 2*y + 3*z a square, where x,y,z,w are nonnegative integers.",
			"comment": [
				"Conjecture: (i) a(n) \u003e 0 for all n = 0,1,2,..., and a(n) = 1 only for n = 0, 1, 3, 5, 7, 14, 15, 16, 25, 30, 33, 84, 169, 225.",
				"(ii) For each ordered pair (a,b) = (1,2), (1,3), (1,12), (1,23), (2,3), (2,4), (2,6), (2,7), (2,15), (2,16), any natural number can be written as x^2 + y^2 + z^2 + 2*w^2 with x,y,z,w nonnegative integers such that a*x + b*y is a square.",
				"This is similar to the conjecture in A271518. It is known that any natural number can be written as x^2 + y^2 + z^2 + 2*w^2 with x,y,z,w nonnegative integers."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A275344/b275344.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1604.06723\"\u003eRefining Lagrange's four-square theorem\u003c/a\u003e, arXiv:1604.06723 [math.GM], 2016."
			],
			"example": [
				"a(0) = 1 since 0 = 0^2 + 0^2 + 0^2 + 2*0^2 with 0 + 2*0 + 3*0 = 0^2.",
				"a(1) = 1 since 1 = 1^2 + 0^2 + 0^2 + 2*0^2 with 1 + 2*0 + 3*0 = 1^2.",
				"a(3) = 1 since 3 = 1^2 + 0^2 + 0^2 + 2*1^2 with 1 + 2*0 + 3*0 = 1^2.",
				"a(5) = 1 since 5 = 2^2 + 1^2 + 0^2 + 2*0^2 with 2 + 2*1 + 3*0 = 2^2.",
				"a(7) = 1 since 7 = 2^2 + 1^2 + 0^2 + 2*1^2 with 2 + 2*1 + 3*0 = 2^2.",
				"a(14) = 1 since 14 = 1^2 + 1^2 + 2^2 + 2*2^2 with 1 + 2*1 + 3*2 = 3^2.",
				"a(15) = 1 since 15 = 3^2 + 0^2 + 2^2 + 2*1^2 with 3 + 2*0 + 3*2 = 3^2.",
				"a(16) = 1 since 16 = 4^2 + 0^2 + 0^2 + 2*0^2 with 4 + 2*0 + 3*0 = 2^2.",
				"a(25) = 1 since 25 = 1^2 + 4^2 + 0^2 + 2*2^2 with 1 + 2*4 + 3*0 = 3^2.",
				"a(30) = 1 since 30 = 3^2 + 2^2 + 3^2 + 2*2^2 with 3 + 2*2 + 3*3 = 4^2.",
				"a(33) = 1 since 33 = 1^2 + 0^2 + 0^2 + 2*4^2 with 1 + 2*0 + 3*0 = 1^2.",
				"a(84) = 1 since 84 = 4^2 + 6^2 + 0^2 + 2*4^2 with 4 + 2*6 + 3*0 = 4^2.",
				"a(169) = 1 since 169 = 10^2 + 6^2 + 1^2 + 2*4^2 with 10 + 2*6 + 3*1 = 5^2.",
				"a(225) = 1 since 225 = 10^2 + 6^2 + 9^2 + 2*2^2 with 10 + 2*6 + 3*9 = 7^2."
			],
			"mathematica": [
				"SQ[n_]:=SQ[n]=IntegerQ[Sqrt[n]]",
				"Do[r=0;Do[If[SQ[n-2*w^2-x^2-y^2]\u0026\u0026SQ[x+2*y+3*Sqrt[n-2w^2-x^2-y^2]],r=r+1],{w,0,Sqrt[n/2]},{x,0,Sqrt[n-2*w^2]},{y,0,Sqrt[n-2*w^2-x^2]}];Print[n,\" \",r];Continue,{n,0,80}]"
			],
			"xref": [
				"Cf. A000290, A271518, A275297, A275301."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Zhi-Wei Sun_, Jul 24 2016",
			"references": 7,
			"revision": 11,
			"time": "2016-07-24T09:21:02-04:00",
			"created": "2016-07-24T09:21:01-04:00"
		}
	]
}