{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A290102",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 290102,
			"data": "1,2,9,2,3,2,52,2,3,2,18,2,3,2,49,2,3,2,9,2,3,2,18,2,3,2,395,2,3,2,1108,2,3,2,9,2,3,2,52,2,3,2,18,2,3,2,360,2,3,2,9,2,3,2,18,2,3,2,57,2,3,2,1116,2,3,2,9,2,3,2,115,2,3,2,18,2,3,2,109,2,3,2,9,2,3,2,18,2,3,2,56,2,3,2,105,2,3,2,9,2,3,2,368,2,3,2,18",
			"name": "Start from the singleton set S = {n}, and generate on each iteration a new set where each odd number k is replaced by 3k+1, and each even number k is replaced by 3k+1 and k/2. a(n) is the size of the set after the first iteration which has produced a number smaller than n. a(1) = 1 by convention.",
			"link": [
				"Antti Karttunen, \u003ca href=\"/A290102/b290102.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"\u003ca href=\"/index/3#3x1\"\u003eIndex entries for sequences related to 3x+1 (or Collatz) problem\u003c/a\u003e"
			],
			"formula": [
				"a(n) \u003c= A290100(n)."
			],
			"example": [
				"For n=3, the initial set is {3}, the next set is 3*3+1 = {10}, from which we get (by applying both x/2 and 3x+1 to 10): {5, 31}, and then on we get -\u003e {16, 94} -\u003e {8, 47, 49, 283} -\u003e {4, 25, 142, 148, 850} -\u003e {2, 13, 71, 74, 76, 425, 427, 445, 2551}, which set already has a member less than 3, and has 9 members in total, thus a(3) = 9."
			],
			"mathematica": [
				"Table[-2 Boole[n == 1] + Length@ Last@ NestWhileList[Union@ Flatten[# /. {k_ /; OddQ@ k :\u003e 3 k + 1, k_ /; EvenQ@ k :\u003e {k/2, 3 k + 1}}] \u0026, {n}, AllTrue[#, # \u003e n \u0026] \u0026, {2, 1}], {n, 107}] (* _Michael De Vlieger_, Aug 20 2017 *)"
			],
			"program": [
				"(PARI) A290102(n) = { if(1==n,return(1)); my(S, k); S=[n]; k=0; while( S[1]\u003e=n, k++; S=vecsort( concat(apply(x-\u003e3*x+1, S), apply(x-\u003ex\\2, select(x-\u003ex%2==0, S) )), , 8);  ); length(S); } \\\\ After _Max Alekseyev_'s code for A127885, see also A290101.",
				"(Python)",
				"from sympy import flatten",
				"def ok(n, L):",
				"    return any(i \u003c n for i in L)",
				"def a(n):",
				"    if n==1: return 1",
				"    L=[n]",
				"    while not ok(n, L):",
				"        L=set(flatten([[3*k + 1, k//2] if k%2==0 else 3*k + 1 for k in L]))",
				"    return len(L)",
				"print([a(n) for n in range(1, 121)]) # _Indranil Ghosh_, Aug 22 2017"
			],
			"xref": [
				"Cf. A127885, A290100, A007395 (even bisection).",
				"Cf. A290101 (number of iterations needed until a set with smaller member than n is produced)."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Antti Karttunen_, Aug 20 2017",
			"references": 3,
			"revision": 22,
			"time": "2021-04-23T01:30:52-04:00",
			"created": "2017-08-21T12:59:34-04:00"
		}
	]
}