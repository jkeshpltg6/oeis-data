{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A316789",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 316789,
			"data": "1,1,1,2,1,1,1,2,2,1,1,1,1,1,1,6,1,1,1,1,1,1,1,1,2,1,2,1,1,1,1,2,1,1,1,2,1,1,1,1,1,1,1,1,1,1,1,1,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,14,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,6,1,1,1,1,1,1",
			"name": "Number of same-tree-factorizations of n.",
			"comment": [
				"A constant factorization of n is a finite nonempty constant multiset of positive integers greater than 1 with product n. Constant factorizations correspond to perfect divisors (A089723). A same-tree-factorization of n is either (case 1) the number n itself or (case 2) a finite sequence of two or more same-tree-factorizations, one of each factor in a constant factorization of n.",
				"a(n) depends only on the prime signature of n. - _Andrew Howroyd_, Nov 18 2018"
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A316789/b316789.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 1 + Sum_{n = x^y, y \u003e 1} a(x)^y.",
				"a(2^n) = A281145(n)."
			],
			"example": [
				"The a(64) = 14 same-tree-factorizations:",
				"  64",
				"  (8*8)",
				"  (4*4*4)",
				"  (8*(2*2*2))",
				"  ((2*2*2)*8)",
				"  (4*4*(2*2))",
				"  (4*(2*2)*4)",
				"  ((2*2)*4*4)",
				"  (2*2*2*2*2*2)",
				"  (4*(2*2)*(2*2))",
				"  ((2*2)*4*(2*2))",
				"  ((2*2)*(2*2)*4)",
				"  ((2*2*2)*(2*2*2))",
				"  ((2*2)*(2*2)*(2*2))"
			],
			"mathematica": [
				"a[n_]:=1+Sum[a[n^(1/d)]^d,{d,Rest[Divisors[GCD@@FactorInteger[n][[All,2]]]]}]",
				"Array[a,100]"
			],
			"program": [
				"(PARI) a(n)={my(z, e=ispower(n,,\u0026z)); 1 + if(e, sumdiv(e, d, if(d\u003e1, a(z^(e/d))^d)))} \\\\ _Andrew Howroyd_, Nov 18 2018"
			],
			"xref": [
				"Cf. A001055, A001597, A001678, A003238, A007916, A052409, A052410, A067824, A089723, A281118, A281145, A294336, A316790."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Gus Wiseman_, Jul 14 2018",
			"references": 2,
			"revision": 8,
			"time": "2018-11-18T14:47:32-05:00",
			"created": "2018-07-14T15:37:56-04:00"
		}
	]
}