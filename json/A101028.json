{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A101028",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 101028,
			"data": "1,11,79,479,5297,69071,69203,471181,8960447,44831407,1031626241,5160071143,15484789693,64166447971,1989542332021,3979714828967,27861681000449,1030996803010973,1031094241305773,42278288849598913",
			"name": "Numerator of partial sums of a certain series. First member (m=2) of a family.",
			"comment": [
				"The denominators are given in A101029.",
				"The limit s=lim(s(n),n-\u003einfty) with s(n) defined below equals 3*sum(Zeta(2*k+1)/2^(2*k),k=1..infty) with Euler's (or Riemann's) Zeta function. This limit is 3*(2*log(2)-1)= 1.158883083...; see the Abramowitz-Stegun reference p. 259, eq. 6.3.15 with z=1/2 together with p. 258, eqs. 6.3.5 and 6.3.3.",
				"This is the first member (m=2) of a family of rational partial sum sequences s(n,m)=(m-1)*m*(m+1)*sum(1/((m*k-1)*(m*k)*(m*k+1)),k=1..n) which have limit s(m)=lim(s(n,m),n-\u003einfty) = -(gamma + Psi(1/m) + m/2 + Pi*cot(Pi*x)/2), with the Euler-Mascheroni constant gamma and the digamma-function Psi. The same limit is reached by (m^2-1)*sum(Zeta(2*k+1)/m^(2*k),k=1..infty)."
			],
			"reference": [
				"M. Abramowitz and I. A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, Tenth Printing, 1972, pp. 258-259.",
				"Mohammad K. Azarian, Problem 1218, Pi Mu Epsilon Journal, Vol. 13, No. 2, Spring 2010, p. 116.  Solution published in Vol. 13, No. 3, Fall 2010, pp. 183-185."
			],
			"link": [
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"W. Lang: \u003ca href=\"/A101028/a101028.txt\"\u003eRationals s(n) and more.\u003c/a\u003e"
			],
			"formula": [
				"a(n)=numerator(s(n)) with s(n)=6*sum(1/((2*k-1)*(2*k)*(2*k+1)), k=1..n)."
			],
			"example": [
				"s(3)= 6*(1/(1*2*3)+ 1/(3*4*5) + 1/(5*6*7)) = 79/70, hence a(3)=79 and A101029(3)=70."
			],
			"xref": [
				"Cf. A101627, A101629, A101631 members m=3, 4, 5."
			],
			"keyword": "nonn,frac,easy",
			"offset": "1,2",
			"author": "_Wolfdieter Lang_, Dec 17 2004",
			"references": 5,
			"revision": 21,
			"time": "2019-08-28T17:52:11-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}