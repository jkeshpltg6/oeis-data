{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A245071",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 245071,
			"data": "10,21,31,41,49,59,67,77,85,91,101,107,115,125,133,139,145,155,161,169,179,185,193,199,203,211,221,229,239,247,245,253,259,269,271,281,287,293,301,307,313,323,325,335,343,353,353,353,361,371,379,385,395,397,403,409,415,425",
			"name": "a(n) = 12n - prime(n).",
			"comment": [
				"Prime(n) \u003e n for n \u003e 0. Let prime(n) = k*n with k as an even integer constant, for example, k = 12; then a(n) = k*n - prime(n) is a sequence of odd integers that are positive as long as k*n \u003e prime(n). This is the case up to a(40072) = 11. If k*n \u003c prime(n) then a(n) \u003c 0, a(40073) = -5 up to a(40083) = -5. From a(40084) = 5 up to a(40121) = 5, a(n) \u003e 0 again, but a(n) \u003c 0 for n \u003e= 40122. For k = 12 the table shows this result compared with floor(prime(n)/n) and (prime(n) mod n) \u003c= (prime(n+1) mod (n+1)) for n \u003e= 1. Observations:",
				"(1) If k \u003e floor(prime(n)/n) then a(n) is positive.",
				"(2) If k \u003c= floor(prime(n)/n) and (prime(n) mod n) \u003c (prime(n+1) mod (n+1)) and n \u003e 1 then a(n) is negative.",
				"(3) If k \u003c= floor(prime(n)/n) and (prime(n) mod n) \u003e (prime(n+1) mod (n+1)) then a(n) is positive.",
				".",
				"n     prime(n) floor(prime(n)/n) (prime(n) mod n)  a(n)",
				"40072 480853        12                 5            11",
				"40073 480881        12                23            -5",
				"40083 481001        11             40079            -5",
				"40084 481003        11             40074             5",
				"40121 481447        12                 5             5",
				"40122 481469        12                13            -5"
			],
			"link": [
				"Freimut Marschner, \u003ca href=\"/A245071/b245071.txt\"\u003eTable of n, a(n) for n = 1..100000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 12*n - prime(n)."
			],
			"example": [
				"a(3) = 12*3 - prime(3) = 36 - 5 = 31."
			],
			"mathematica": [
				"Table[12n - Prime[n], {n, 60}] (* _Alonso del Arte_, Jul 27 2014 *)"
			],
			"program": [
				"(PARI) vector(133, n, 12*n-prime(n) )"
			],
			"xref": [
				"A000040 (prime(n)), A038605 (Floor(n-th prime/n), A004648 (prime(n) mod n), A038606 (Least k such that k-th prime \u003e n * k),  A038607 (the smallest prime number k such that k \u003e n*pi(k)),  A102281 (the largest number m such that m = pi(n*m))."
			],
			"keyword": "sign,easy",
			"offset": "1,1",
			"author": "_Freimut Marschner_, Jul 21 2014",
			"references": 2,
			"revision": 42,
			"time": "2015-12-21T04:15:15-05:00",
			"created": "2014-08-21T23:42:52-04:00"
		}
	]
}