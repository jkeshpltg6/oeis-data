{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A111588",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 111588,
			"data": "1,1,1,2,1,2,1,4,2,2,1,8,1,2,2,10,1,8,1,8,2,2,1,33,2,2,4,8,1,13,1,26,2,2,2,57,1,2,2,33,1,13,1,8,8,2,1,140,2,8,2,8,1,33,2,33,2,2,1,125,1,2,8,71,2,13,1,8,2,13,1,348,1,2,8,8,2,13,1,140,10,2,1,122,2,2,2,33,1,118,2",
			"name": "Crazy Dice: number of ways to design a pair of n-sided dice with positive integers on their faces, so that the sums when they are tossed occur with the same probabilities as if a pair of standard n-sided dice were tossed.",
			"comment": [
				"It is not required that the two dice be identical, it is not required that the entries be bounded by n and we do not ask that the entries be distinct from one another on each cube.",
				"We pretend for the purpose of this sequence that regular n-sided dice exist for all n.",
				"In other words, how many (unordered) pairs of polynomials B(x) = x^b_1 + x^b_2 + ... + x^b_n, C(x) = x^c_1 + x^c_2 + ... + x^c_n, are there with all exponents positive integers, such that B(x)*C(x) = (x+x^2+x^3+...+x^n)^2?",
				"a(n) = 1 means that the only way two n-sided dice can have the same probability distribution as two normal n-sided dice (each side numbered 1 through n) is if they are normal. a(6) = 2 corresponds to normal dice and Sicherman dice (one labeled 1,2,2,3,3,4 and the other 1,3,4,5,6,8). - _Charles R Greathouse IV_, Jan 19 2017",
				"Records are: 1, 2, 4, 8, 10, 33, 57, 140, 348, 583, 956, 2036, 2393, 3050, ... and they seem to occur at positions given by A033833. - _Antti Karttunen_, Aug 28 2017"
			],
			"reference": [
				"M. Gardner, \"Penrose Tiles to Trapdoor Ciphers\", p. 266."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A111588/b111588.txt\"\u003eTable of n, a(n) for n = 1..239\u003c/a\u003e",
				"Donald J. Newman, \u003ca href=\"http://dx.doi.org/10.1007/978-1-4613-8214-0\"\u003eA Problem Seminar\u003c/a\u003e, Springer; see Problem #88."
			],
			"example": [
				"The first nontrivial example is for n=4: {1,2,2,3} and {1,3,3,5} together have the same sum probabilities as a pair of {1,2,3,4}. That is, (x + 2x^2 + x^3)(x + 2x^3 + x^5)=(x + x^2 + x^3 + x^4)^2."
			],
			"program": [
				"(PARI) ok(p,e,n)=my(v=Vec(factorback(p,e))); vecmin(v)\u003e=0 \u0026\u0026 vecsum(v)==n",
				"a(n)=if(n\u003c4, return(1)); my(x='x,f=factor((x^n-1)/(x-1)),p=f[,1],e=2*f[,2]~,u=vector(#e,i,[0,e[i]]),s,t); t=vecmax(e); for(i=1,#e, if(e[i]==t, u[i][2]\\=2; break)); forvec(v=u, t=e-v; if(cmp(v,t)\u003c=0 \u0026\u0026 ok(p,v,n) \u0026\u0026 ok(p,t,n), s++)); s \\\\ _Charles R Greathouse IV_, Jan 19 2017"
			],
			"xref": [
				"Cf. A033833."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_N. J. A. Sloane_, Nov 17 2005",
			"ext": [
				"Edited and extended by _Matthew Conroy_, Jan 16 2006",
				"Correction to some terms, thanks to Adam Chalcraft. - _Matthew Conroy_, Apr 04 2010"
			],
			"references": 3,
			"revision": 17,
			"time": "2017-08-28T09:22:09-04:00",
			"created": "2006-01-24T03:00:00-05:00"
		}
	]
}