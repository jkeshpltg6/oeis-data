{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A226353",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 226353,
			"data": "1,49,169,36,1,1,2601,1089,1,8836,33489,44100,1,149769,128164,96721,1,156816,1225,40804,12321,831744,839056,1149184,1737124,3655744,407044,1890625,2208196,1089,1,1466521,6125625,2235025,2832489,1,3759721,6885376,8844676",
			"name": "Largest integer k in base n whose squared digits sum to sqrt(k).",
			"comment": [
				"Any d-digit number in base n meeting the criterion must also meet the condition d*(n-1)^2 \u003c n^(d/2). Numerically, it can be shown this limits the candidate values to squares \u003c 22*n^4. The larger values are statistically unlikely, and in fact the largest value of k in the first 1000 bases is ~9.96*n^4 in base 775.",
				"a(n)=1 iff A226352(n)=1."
			],
			"link": [
				"Christian N. K. Anderson, \u003ca href=\"/A226353/b226353.txt\"\u003eTable of n, a(n) for n = 2..1000\u003c/a\u003e",
				"Christian N. K. Anderson, \u003ca href=\"/A226353/a226353.txt\"\u003eTable of base, all solutions in base 10, and all solutions in base n\u003c/a\u003e for bases 2 to 1000."
			],
			"example": [
				"In base 8, the four solutions are the values {1,16,256,2601}, which are written as {1,20,400,5051} in base 8 and",
				"sqrt(1)   = 1  = 1^2",
				"sqrt(16)  = 4  = 2^2+0^2",
				"sqrt(256) = 16 = 4^2+0^2+0^2",
				"sqrt(2601)= 51 = 5^2+0^2+5^2+1^2"
			],
			"program": [
				"(R) inbase=function(n,b) { x=c(); while(n\u003e=b) { x=c(n%%b,x); n=floor(n/b) }; c(n,x) }",
				"for(n in 2:50) cat(\"Base\",n,\":\",which(sapply((1:(4.7*n^2))^2,function(x) sum(inbase(x,n)^2)==sqrt(x)))^2,\"\\n\")"
			],
			"xref": [
				"Cf. A226352, A226224.",
				"Cf. digital sums for digits at various powers: A007953, A003132, A055012, A055013, A055014, A055015."
			],
			"keyword": "nonn,base",
			"offset": "2,2",
			"author": "_Christian N. K. Anderson_ and _Kevin L. Schwartz_, Jun 04 2013",
			"references": 2,
			"revision": 10,
			"time": "2013-06-08T15:34:35-04:00",
			"created": "2013-06-08T15:34:35-04:00"
		}
	]
}