{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A286619",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 286619,
			"data": "1,2,3,2,3,4,5,2,3,6,7,4,5,6,5,2,3,6,8,6,7,9,10,4,5,11,10,6,5,6,5,2,3,6,8,6,8,12,13,6,7,14,15,9,10,12,10,4,5,11,13,11,10,14,13,6,5,11,10,6,5,6,5,2,3,6,8,6,8,12,13,6,8,16,17,12,13,16,13,6,7,14,17,14,15,18,19,9,10,20,21,12,10,12,10,4,5,11,13,11,13,20,22,11,10",
			"name": "Restricted growth sequence computed for filter-sequence A278219, related to run-lengths in the binary representation of n.",
			"comment": [
				"When filtering sequences (by equivalence class partitioning), this sequence can be used instead of A278219, because for all i, j it holds that: a(i) = a(j) \u003c=\u003e A278219(i) = A278219(j).",
				"For example, for all i, j: a(i) = a(j) =\u003e A005811(i) = A005811(j). (The same is true for A073334, as it is a sequence computed from A005811)."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A286619/b286619.txt\"\u003eTable of n, a(n) for n = 0..65537\u003c/a\u003e",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e"
			],
			"mathematica": [
				"f[n_, i_, x_] := Which[n == 0, x, EvenQ@ n, f[n/2, i + 1, x], True, f[(n - 1)/2, i, x Prime@ i]]; g[n_] := If[n == 1, 1, Times @@ MapIndexed[Prime[First@ #2]^#1 \u0026, Sort[FactorInteger[n][[All, -1]], Greater]]]; With[{nn = 99}, Function[s, Table[Position[Keys@ s, k_ /; MemberQ[k, n]][[1, 1]], {n, nn}]]@ Map[#1 -\u003e #2 \u0026 @@ # \u0026, Transpose@ {Values@ #, Keys@ #}] \u0026@ PositionIndex@ Table[g@ f[BitXor[n, Floor[n/2]], 1, 1], {n, 0, nn}]] (* _Michael De Vlieger_, May 12 2017, Version 10 *)"
			],
			"program": [
				"(PARI)",
				"rgs_transform(invec) = { my(occurrences = Map(), outvec = vector(length(invec)), u=1); for(i=1, length(invec), if(mapisdefined(occurrences,invec[i]), my(pp = mapget(occurrences, invec[i])); outvec[i] = outvec[pp] , mapput(occurrences,invec[i],i); outvec[i] = u; u++ )); outvec; };",
				"write_to_bfile(start_offset,vec,bfilename) = { for(n=1, length(vec), write(bfilename, (n+start_offset)-1, \" \", vec[n])); }",
				"A005940(n) = { my(p=2, t=1); n--; until(!n\\=2, if((n%2), (t*=p), p=nextprime(p+1))); t }; \\\\ Modified from code of _M. F. Hasler_",
				"A046523(n) = { my(f=vecsort(factor(n)[, 2], , 4), p); prod(i=1, #f, (p=nextprime(p+1))^f[i]); };  \\\\ This function from _Charles R Greathouse IV_, Aug 17 2011",
				"A278222(n) = A046523(A005940(1+n));",
				"A003188(n) = bitxor(n, n\u003e\u003e1);",
				"A278219(n) = A278222(A003188(n));",
				"write_to_bfile(0,rgs_transform(vector(65538,n,A278219(n-1))),\"b286619.txt\");"
			],
			"xref": [
				"Cf. A278219, A005811, A073334.",
				"Cf. also A101296, A286603, A286605, A286610, A286621, A286622, A286626, A286378 for similarly constructed sequences."
			],
			"keyword": "nonn,look",
			"offset": "0,2",
			"author": "_Antti Karttunen_, May 11 2017",
			"references": 18,
			"revision": 15,
			"time": "2017-05-14T00:04:09-04:00",
			"created": "2017-05-12T18:22:24-04:00"
		}
	]
}