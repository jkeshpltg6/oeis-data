{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A163382",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 163382,
			"data": "1,2,3,4,5,5,7,8,9,10,11,9,11,11,15,16,17,18,19,18,21,21,23,17,19,21,23,19,23,23,31,32,33,34,35,36,37,38,39,34,38,42,43,37,45,43,47,33,35,37,39,38,43,45,47,35,39,43,47,39,47,47,63,64,65,66,67,68,69,70,71,68,73",
			"name": "a(n) = the (decimal equivalent of the) smallest integer that can be made by rotating the binary digits of n any number of positions to the left or right, where a(n) in binary must contain the same number of digits (without any leading 0's) as n written in binary.",
			"comment": [
				"By rotating the binary digits of n, it is meant: Write n in binary without any leading 0's. To rotate this string to the right, say, by one position, first remove the rightmost digit and then append it on the left side of the remaining string. (So the least significant digit becomes the most significant digit.) Here, leading 0's are not removed after the first rotation, so that each binary string being rotated has the same number of binary digits as n.",
				"Alternatively, compute n in binary and denote the number of digits by d. Concatenate the binary number to itself. In the new \"number\", find the smallest binary number with length d and a leading 1. - _David A. Corneth_, Sep 28 2017"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A163382/b163382.txt\"\u003eTable of n, a(n) for n = 1..8190\u003c/a\u003e"
			],
			"example": [
				"13 in binary is 1101. Rotating this just once to the right, we get 1110, 14 in decimal. If we rotate twice to the right, we would have had 0111 = 7 in decimal. Rotating 3 times, we end up with 1011, which is 11 in decimal. Rotating 4 times, we end up at the beginning with 1101 = 13. 7 is the smallest of these, but it contains a 0 in the leftmost position of its 4-digit binary representation. 11 (decimal), on the other hand, is the smallest with a 1 in the leftmost position of its 4-digit binary representation. So a(13) = 11.",
				"20 in binary is 10100 and has 5 digits. Concatenating the binary expansion of 20 to itself gives 1010010100. The shortest binary number of length 5 is 10010, which corresponds to 18 in decimal. Therefore, a(20) = 18. - _David A. Corneth_, Sep 28 2017"
			],
			"maple": [
				"a:= proc(n) local i, k, m, s;",
				"      k, m, s:= ilog2(n), n, n;",
				"      for i to k do m:= iquo(m, 2, 'd') +d*2^k;",
				"          if d=1 then s:=s, m fi od;",
				"      min(s)",
				"    end:",
				"seq(a(n), n=1..80);  # _Alois P. Heinz_, May 24 2012"
			],
			"mathematica": [
				"Table[With[{d = IntegerDigits[n, 2]}, Min@ Map[FromDigits[#, 2] \u0026, Select[Map[RotateRight[d, #] \u0026, Range[Length@ d]], First@ # == 1 \u0026]]], {n, 73}] (* _Michael De Vlieger_, Sep 23 2017 *)"
			],
			"program": [
				"(PARI) a(n) = {my(b = binary(n), l = List(), m = #b, v, r = 2^m); b = concat(b, b); for(i=1, m, if(b[i]==1, r = min(r, fromdigits(vector(m, j, b[i + j - 1]), 2)))); r} \\\\ _David A. Corneth_, Sep 28 2017"
			],
			"xref": [
				"Cf. A163380, A163381."
			],
			"keyword": "base,nonn,easy,look",
			"offset": "1,2",
			"author": "_Leroy Quet_, Jul 25 2009",
			"ext": [
				"Corrected and extended by _Sean A. Irvine_, Nov 08 2009"
			],
			"references": 4,
			"revision": 23,
			"time": "2017-10-01T00:47:33-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}