{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A111820",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 111820,
			"data": "1,1,1,1,5,1,1,55,25,1,1,2055,1525,125,1,1,291430,311525,38875,625,1,1,165397680,239305275,40338875,975625,3125,1,1,390075741430,735920617775,157056792000,5077475625,24409375,15625,1",
			"name": "Triangle P, read by rows, that satisfies [P^5](n,k) = P(n+1,k+1) for n\u003e=k\u003e=0, also [P^(5*m)](n,k) = [P^m](n+1,k+1) for all m, where [P^m](n,k) denotes the element at row n, column k, of the matrix power m of P, with P(0,k)=1 and P(k,k)=1 for all k\u003e=0.",
			"comment": [
				"Also P(n,k) = the partitions of (5^n - 5^(n-k)) into powers of 5 \u003c= 5^(n-k)."
			],
			"formula": [
				"Let q=5; the g.f. of column k of P^m (ignoring leading zeros) equals: 1 + Sum_{n\u003e=1} (m*q^k)^n/n! * Product_{j=0..n-1} L(q^j*x) where L(x) satisfies: x/(1-x) = Sum_{n\u003e=1} Product_{j=0..n-1} L(q^j*x)/(j+1) and L(x) equals the g.f. of column 0 of the matrix log of P (A111824)."
			],
			"example": [
				"Let q=5; the g.f. of column k of matrix power P^m is:",
				"1 + (m*q^k)*L(x) + (m*q^k)^2/2!*L(x)*L(q*x) +",
				"(m*q^k)^3/3!*L(x)*L(q*x)*L(q^2*x) +",
				"(m*q^k)^4/4!*L(x)*L(q*x)*L(q^2*x)*L(q^3*x) + ...",
				"where L(x) satisfies:",
				"x/(1-x) = L(x) + L(x)*L(q*x)/2! + L(x)*L(q*x)*L(q^2*x)/3! + ...",
				"and L(x) = x - 3/2!*x^2 + 16/3!*x^3 + 2814/4!*x^4 +... (A111824).",
				"Thus the g.f. of column 0 of matrix power P^m is:",
				"1 + m*L(x) + m^2/2!*L(x)*L(5*x) + m^3/3!*L(x)*L(5*x)*L(5^2*x) +",
				"m^4/4!*L(x)*L(5*x)*L(5^2*x)*L(5^3*x) + ...",
				"Triangle P begins:",
				"1;",
				"1,1;",
				"1,5,1;",
				"1,55,25,1;",
				"1,2055,1525,125,1;",
				"1,291430,311525,38875,625,1;",
				"1,165397680,239305275,40338875,975625,3125,1; ...",
				"where P^5 shifts columns left and up one place:",
				"1;",
				"5,1;",
				"55,25,1;",
				"2055,1525,125,1;",
				"291430,311525,38875,625,1; ..."
			],
			"program": [
				"(PARI) P(n,k,q=5)=local(A=Mat(1),B);if(n\u003ck || k\u003c0,0, for(m=1,n+1,B=matrix(m,m);for(i=1,m, for(j=1,i, if(j==i || j==1,B[i,j]=1,B[i,j]=(A^q)[i-1,j-1]);));A=B); return(A[n+1,k+1]))"
			],
			"xref": [
				"Cf. A111821 (column 1), A111822 (row sums), A111823 (matrix log); triangles: A110503 (q=-1), A078121 (q=2), A078122 (q=3), A078536 (q=4), A111825 (q=6), A111830 (q=7), A111835 (q=8)."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Gottfried Helms_ and _Paul D. Hanna_, Aug 22 2005",
			"references": 7,
			"revision": 7,
			"time": "2017-06-13T22:34:21-04:00",
			"created": "2005-09-21T03:00:00-04:00"
		}
	]
}