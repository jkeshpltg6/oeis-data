{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002505",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2505,
			"id": "M5052 N2185",
			"data": "18,23,28,32,35,39,42,46,49,52,55,58,60,63,66,68,71,74,76,79,81,84,86,88,91,93,95,98,100,102,104,107,109,111,113,115,118,120,122,124,126",
			"name": "Nearest integer to the n-th Gram point.",
			"comment": [
				"Every integer greater than 3295 is in this sequence. - _T. D. Noe_, Aug 03 2007",
				"Nearest integer to points t such that Re(zeta(1/2+i*t)) is not equal to zero and Im(zeta(1/2+i*t))=0. - _Mats Granvik_, May 14 2016"
			],
			"reference": [
				"C. B. Haselgrove and J. C. P. Miller, Tables of the Riemann Zeta Function. Royal Society Mathematical Tables, Vol. 6, Cambridge Univ. Press, 1960, p. 58.",
				"A. Ivić, The Theory of Hardy's Z-Function, Cambridge University Press, 2013, pages 109-112.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A002505/b002505.txt\"\u003eTable of n, a(n) for n = 0..3000\u003c/a\u003e",
				"Guilherme França and André LeClair, \u003ca href=\"http://arxiv.org/abs/1407.4358\"\u003eA theory for the zeros of Riemann Zeta and other L-functions\u003c/a\u003e, arXiv:1407.4358 [math.NT], 2014, formula (163) at page 47.",
				"Mats Granvik \u003ca href=\"https://pastebin.com/cZ0Ue7rN\"\u003eGram points computed by iterative formula\u003c/a\u003e",
				"C. B. Haselgrove and J. C. P. Miller, \u003ca href=\"/A002505/a002505.pdf\"\u003eTables of the Riemann Zeta Function\u003c/a\u003e, annotated scanned copy of page 58.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/GramPoint.html\"\u003eGram Point\u003c/a\u003e"
			],
			"formula": [
				"a(n) ~ 2*Pi*n/log n. - _Charles R Greathouse IV_, Oct 23 2015",
				"From _Mats Granvik_, May 16 2016: (Start)",
				"a(n) = round(2*Pi*exp(1 + LambertW((8*n + 1)/(8*exp(1))))), Eric Weisstein's World of Mathematics.",
				"a(n+1) = round(2*Pi*(n - 7/8)/LambertW((n - 7/8)/exp(1))), after Guilherme França, André LeClair formula (163) page 47.",
				"(End)",
				"For c = 0 the n-th Gram point x is the fixed point solution to the iterative formula:",
				"x = 2*Pi*e^(LambertW (-((c - n + RiemannSiegelTheta (x)/Pi + (x*(-log (x) + 1 + log (2) + log (Pi)))/(2*Pi) + 2)/e)) +  1). - _Mats Granvik_, Jun 17 2017"
			],
			"mathematica": [
				"a[n_] := Round[ g /. FindRoot[ RiemannSiegelTheta[g] == Pi*n, {g, 2*Pi*Exp[1 + ProductLog[(8*n + 1)/(8*E)]]}]]; Table[a[n], {n, 0, 40}] (* _Jean-François Alcover_, Oct 17 2012, after _Eric W. Weisstein_ *)"
			],
			"program": [
				"(Sage)",
				"a = lambda n: round(2*pi*(n - 7/8)/lambert_w((n - 7/8)/exp(1)))",
				"print([a(n) for n in (1..41)]) # _Peter Luschny_, May 19 2016"
			],
			"xref": [
				"Cf. A273061. A114857 = 17.8455995..., A114858 = 23.1702827..."
			],
			"keyword": "nonn",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_.",
			"references": 8,
			"revision": 70,
			"time": "2020-02-22T04:54:40-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}