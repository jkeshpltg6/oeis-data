{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A266552",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 266552,
			"data": "2,3,10,5,16,8,4,2,4,2,5,16,8,4,2,6,3,10,5,16,8,4,2,7,22,11,34,17,52,26,13,40,20,10,5,16,8,4,2,8,4,2,9,3,10,5,16,8,4,2,10,5,16,8,4,2,11,34,17,52,26,13,40,20,10,5,16,8,4,2",
			"name": "Irregular triangle read by rows giving the 3p+1 sequence of n.",
			"comment": [
				"The n-th row of the triangle provides the 3p+1 sequence for n, such that the sequence terminates at the first occurrence of 2. The 3p+1 sequence is a variation of the 3x+1 (Collatz) sequence.",
				"The 3p+1 sequence for n \u003e= 2 is defined as follows: b(0) = n; b(n+1) = 3 * b(n) + 1 if b(n) is prime; otherwise, b(n+1) = b(n) divided by the smallest prime factor of b(n).",
				"It seems that all 3p+1 sequences reach 2. This has been verified for n up to 5*10^8. Once a 3p+1 sequence reaches 2, it repeats the following cycle: 2, 7, 22, 11, 34, 17, 52, 26, 13, 40, 20, 10, 5, 16, 8, 4, 2, ..."
			],
			"link": [
				"Jack Brennen, Felice Russo, \u003ca href=\"http://www.primepuzzles.net/puzzles/puzz_073.htm\"\u003eA Collatz-like sequence.\u003c/a\u003e"
			],
			"example": [
				"The irregular array a(n,k) starts:",
				"n\\k   0   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16  17",
				"2:    2",
				"3:    3  10   5  16   8   4   2",
				"4:    4   2",
				"5:    5  16   8   4   2",
				"6:    6   3  10   5  16   8   4   2",
				"7:    7  22  11  34  17  52  26  13  40  20  10   5  16   8   4   2",
				"8:    8   4   2",
				"9:    9   3  10   5  16   8   4   2",
				"10:   5  16   8   4   2",
				"11:  11  34  17  52  26  13  40  20  10   5  16   8   4   2"
			],
			"program": [
				"(PARI) row(n) = {print1 (n, \", \"); while (n!=2, nn = if (isprime(n), 3*n+1, n/factor(n)[1,1]); print1(nn, \", \"); n=nn);} \\\\ _Michel Marcus_, Jan 02 2016",
				"(Python)",
				"from sympy import isprime, primefactors",
				"def a(n):",
				"    if n==2: return [2]",
				"    l=[n, ]",
				"    while True:",
				"        if isprime(n): n = 3*n + 1",
				"        else: n/=min(primefactors(n))",
				"        l+=[n, ]",
				"        if n==2: break",
				"    return l",
				"for n in range(2, 21): print a(n) # _Indranil Ghosh_, Apr 22 2017"
			],
			"xref": [
				"Cf. A266551 (image of n under the 3p+1 map).",
				"Cf. A175871 (the repeating cycle starting at 2).",
				"Cf. A070165 (irregular triangle read by rows giving trajectory of n in Collatz problem)."
			],
			"keyword": "nonn,tabf",
			"offset": "2,1",
			"author": "_Robert C. Lyons_, Dec 31 2015",
			"references": 0,
			"revision": 19,
			"time": "2019-12-07T12:18:28-05:00",
			"created": "2016-01-04T10:44:20-05:00"
		}
	]
}