{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A346917",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 346917,
			"data": "32,51,70,89,108,149,168,187,206,266,285,304,367,383,386,402,405,424,484,500,503,522,601,620,702,718,721,740,819,838,936,1037,1056,1154,1355,1372,1374,1393,1412,1472,1491,1510,1589,1608,1690,1706,1709,1728,1807,1826",
			"name": "Numbers that are a sum of the cubes of four primes, not necessarily distinct.",
			"comment": [
				"Roth (1951) proved that the number of terms below x is \u003e\u003e x/log(x)^8.",
				"Ren (2001) proved that this sequence has a positive lower density.",
				"The lower density was proven to be larger than 0.003125 (Ren, 2003), 0.005776 (Liu, 2012), and 0.009664 (Elsholtz and Schlage-Puchta, 2019)."
			],
			"link": [
				"Christian Elsholtz and Jan-Christoph Schlage-Puchta, \u003ca href=\"https://arxiv.org/abs/1902.09858\"\u003eThe density of integers representable as the sum of four prime cubes\u003c/a\u003e, arXiv preprint, arXiv:1902.09858 [math.NT], 2019.",
				"Zhixin Liu, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2011.12.003\"\u003eDensity of the sums of four cubes of primes\u003c/a\u003e, Journal of Number Theory, Vol. 132, No. 4 (2012), pp. 735-747.",
				"Xiumin Ren, \u003ca href=\"https://doi.org/10.1142/S025295990100022X\"\u003eDensity of integers that are the sum of four cubes of primes\u003c/a\u003e, Chin. Ann. Math. Ser. B, Vol. 22, No. 2 (2001), pp. 233-242.",
				"Xiumin Ren, \u003ca href=\"https://doi.org/10.1016/S0022-314X(02)00022-7\"\u003eSums of four cubes of primes\u003c/a\u003e, J. Number Theory, Vol. 98, No. 1 (2003), pp. 156-171.",
				"K. F. Roth, \u003ca href=\"https://doi.org/10.1112/plms/s2-53.4.268\"\u003eOn Waring's problem for cubes\u003c/a\u003e, Proc. London Math. Soc. (2), Vol. 53 (1951), pp. 268-279."
			],
			"example": [
				"a(1) = 32 = 2^3 + 2^3 + 2^3 + 2^3.",
				"a(2) = 51 = 2^3 + 2^3 + 2^3 + 3^3.",
				"a(3) = 70 = 2^3 + 2^3 + 3^3 + 3^3."
			],
			"mathematica": [
				"seq[max_] := Module[{s = Select[Range[Floor @ Surd[max, 3]], PrimeQ]}, Select[Union[Plus @@@ (Tuples[s, 4]^3)], # \u003c= max \u0026]]; seq[2000]"
			],
			"program": [
				"(PARI) list(lim)=my(v=List(), P=apply(p-\u003ep^3,primes(sqrtnint(lim\\=1,3)))); foreach(P,p, foreach(P,q, foreach(P,r, my(s=p+q+r,t); for(i=1,#P, t=s+P[i]; if(t\u003elim, break); listput(v,t))))); Set(v) \\\\ _Charles R Greathouse IV_, Aug 09 2021"
			],
			"xref": [
				"Cf. A030078, A086119, A258865."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Amiram Eldar_, Aug 07 2021",
			"references": 0,
			"revision": 7,
			"time": "2021-08-09T09:11:49-04:00",
			"created": "2021-08-07T05:15:23-04:00"
		}
	]
}