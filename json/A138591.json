{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A138591",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 138591,
			"data": "1,3,5,6,7,9,10,11,12,13,14,15,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,65,66,67,68,69,70,71,72,73,74,75,76,77",
			"name": "Sums of two or more consecutive nonnegative integers.",
			"comment": [
				"Closely related to but different from A057716. - _N. J. A. Sloane_, May 16 2008",
				"These are called polite numbers [From Howard Berman (howard_berman(AT)hotmail.com), Oct 29 2008] by those who require nonnegative integers in the definition as opposed to positive integers. With the latter requirement, 1 = 0 + 1 does not count as a polite number. [This difference of definition pointed out by Ant King (Nov 19 2010)] There is no disagreement that 1 belongs in this sequence, but there is disagreement as to whether it counts as a polite number.",
				"Of course sums of two or more consecutive nonpositive integers have the same absolute values (noted while inserting \"nonnegative\" in title). All integers are sums of two or more consecutive integers without such restriction. - _Rick L. Shepherd_, Jun 03 2014",
				"In K-12 education, these are known as \"staircase numbers.\" The \"1\" is often omitted. - _Gordon Hamilton_, Mar 17 2015",
				"Complement of A155559. - _Ray Chandler_, Mar 23 2016"
			],
			"reference": [
				"J. M. Rodriguez Caballero, A characterization of the hypotenuses of primitive Pythagorean triangles ..., Amer. Math. Monthly 126 (2019), 74-77.",
				"A. Wah and H. Picciotto, Algebra: Themes, Tools, Concepts, 1994, page 190."
			],
			"link": [
				"Ray Chandler, \u003ca href=\"/A138591/b138591.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Ron Knott, \u003ca href=\"http://www.maths.surrey.ac.uk/hosted-sites/R.Knott/runsums/index.html\"\u003eAn Introduction to Runsums\u003c/a\u003e",
				"Mathedpage, \u003ca href=\"http://www.mathedpage.org/teachers/staircases.pdf\"\u003eStaircases\u003c/a\u003e",
				"NRICH, \u003ca href=\"http://nrich.maths.org/public/viewer.php?obj_id=2074\"\u003ePolite numbers\u003c/a\u003e",
				"Melfried Olson, \u003ca href=\"http://www.jstor.org/stable/2689781\"\u003eSequentially so\u003c/a\u003e, Mathematics Magazine 52:5, pp. 297-298.",
				"Erzsébet Orosz, \u003ca href=\"http://ami.ektf.hu/uploads/papers/finalpdf/AAPASM_31_from125to129.pdf\"\u003eOn odd-summing numbers\u003c/a\u003e, Acia Academiae Paedagogicae Agriensis, Seciio Maihemaiicae 31 (2004), pp. 125-129.",
				"PlanetMath, \u003ca href=\"http://planetmath.org/?op=getobj\u0026amp;from=objects\u0026amp;id=10725\"\u003ePolite number\u003c/a\u003e",
				"Wai Yan Pong, \u003ca href=\"http://www.maa.org/sites/default/files/Pong-1-0750774.pdf\"\u003eSums of consecutive integers\u003c/a\u003e, The College Mathematics Journal, 38 (2007), 119-123. - _Parthasarathy Nambi_, May 20 2009",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Polite_number\"\u003ePolite number\u003c/a\u003e"
			],
			"formula": [
				"a(n) = n + A000523(n + A000523(n)). - _Charles R Greathouse IV_, Aug 12 2010"
			],
			"example": [
				"0+1=1, 1+2=3, 2+3=5, 1+2+3=6, 3+4=7, 4+5=9, 1+2+3+4=10, ..."
			],
			"mathematica": [
				"1 + # + Floor[Log[2, # + 1 + Log[2, # + 1]]] \u0026/@Range[0, 70] (* _Ant King_, Nov 18 2010 *)"
			],
			"program": [
				"(PARI) a(n)=n+logint(n+logint(n,2),2) \\\\ _Charles R Greathouse IV_, Sep 01 2015",
				"(PARI) is(n)=n\u003e\u003evaluation(n,2)\u003e1 || n==1 \\\\ _Charles R Greathouse IV_, Aug 01 2016"
			],
			"xref": [
				"Cf. A057716, A155559."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Vladimir Joseph Stephan Orlovsky_, May 13 2008",
			"ext": [
				"More terms from _Carl R. White_, Jul 22 2009",
				"Comment regarding polite numbers edited by _Ant King_, Nov 19 2010"
			],
			"references": 33,
			"revision": 56,
			"time": "2021-01-23T23:17:09-05:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}