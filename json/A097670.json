{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A097670",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 97670,
			"data": "3,3,9,2,7,6,4,2,7,8,9,2,7,8,4,9,8,0,7,7,0,4,7,5,5,0,5,6,5,5,4,4,7,1,2,8,3,9,2,7,4,0,1,0,9,2,5,8,6,0,8,4,4,2,2,3,4,7,8,0,8,4,4,1,9,3,5,2,4,6,3,6,1,5,9,8,0,3,4,6,1,3,5,1,7,3,5,0,1,0,5,1,9,3,2,9,7,8,5,7,3,4,6,7,3",
			"name": "Decimal expansion of the constant 5*exp(psi(4/5) + EulerGamma), where EulerGamma is the Euler-Mascheroni constant (A001620) and psi(x) is the digamma function.",
			"comment": [
				"This constant appears in _Benoit Cloitre_'s generalized Euler-Gauss formula for the Gamma function (see Cloitre link) and is involved in the exact determination of asymptotic limits of certain order-5 linear recursions with varying coefficients (see A097680 for example)."
			],
			"reference": [
				"A. M. Odlyzko, Linear recurrences with varying coefficients, in Handbook of Combinatorics, Vol. 2, R. L. Graham, M. Grotschel and L. Lovasz, eds., Elsevier, Amsterdam, 1995, pp. 1135-1138."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A097670/b097670.txt\"\u003eTable of n, a(n) for n = 1..2500\u003c/a\u003e",
				"Benoit Cloitre, \u003ca href=\"/A097679/a097679.pdf\"\u003eOn a generalization of Euler-Gauss formula for the Gamma function\u003c/a\u003e, preprint 2004.",
				"Xavier Gourdon and Pascal Sebah, \u003ca href=\"http://numbers.computation.free.fr/Constants/Miscellaneous/gammaFunction.html\"\u003eIntroduction to the Gamma Function\u003c/a\u003e.",
				"Andrew Odlyzko, \u003ca href=\"http://www.dtc.umn.edu/~odlyzko/doc/asymptotic.enum.pdf\"\u003eAsymptotic enumeration methods\u003c/a\u003e, in Handbook of Combinatorics, vol. 2, 1995, pp. 1063-1229."
			],
			"formula": [
				"c = ((sqrt(5)+1)/2)^(-sqrt(5)/2)/5^(1/4)*exp(Pi/2*sqrt(1+2/sqrt(5)))."
			],
			"example": [
				"c = 3.39276427892784980770475505655447128392740109258608442234780..."
			],
			"mathematica": [
				"RealDigits[ GoldenRatio^(-Sqrt[5]/2)/5^(1/4)*E^(Pi/2Sqrt[1 + 2/Sqrt[5]]), 10, 105][[1]] (* _Robert G. Wilson v_, Aug 27 2004 *)"
			],
			"program": [
				"(PARI) 5*exp(psi(4/5)+Euler)"
			],
			"xref": [
				"Cf. A097663-A097669, A097671-A097676."
			],
			"keyword": "cons,nonn",
			"offset": "1,1",
			"author": "_Paul D. Hanna_, Aug 25 2004",
			"ext": [
				"More terms from _Robert G. Wilson v_, Aug 27 2004"
			],
			"references": 3,
			"revision": 20,
			"time": "2021-02-27T13:20:38-05:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}