{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A254349",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 254349,
			"data": "1,0,7,4,2,5,8,2,5,2,9,5,4,7,8,9,2,2,5,8,9,4,1,1,9,6,7,7,6,2,4,3,6,6,8,3,0,1,6,3,0,4,2,6,1,6,3,6,0,6,7,5,3,7,9,5,1,6,4,5,8,4,3,9,6,8,7,3,7,2,8,3,6,6,9,6,1,0,0,9,2,3,3,8,9,8,9,6,9,5,6,3,1,9,9,6,7,3,8,6,9,6",
			"name": "Decimal expansion of gamma_1(1/6), the first generalized Stieltjes constant at 1/6 (negated).",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A254349/b254349.txt\"\u003eTable of n, a(n) for n = 2..5002\u003c/a\u003e",
				"Iaroslav V. Blagouchine, \u003ca href=\"http://arxiv.org/abs/1401.3724\"\u003eA theorem for the closed-form evaluation of the first generalized Stieltjes constant at rational arguments\u003c/a\u003e, arXiv:1401.3724 [math.NT], 2015.",
				"Iaroslav V. Blagouchine, \u003ca href=\"http://www.researchgate.net/publication/259743013_A_theorem_for_the_closed-form_evaluation_of_the_first_generalized_Stieltjes_constant_at_rational_arguments_and_some_related_summations\"\u003eA theorem ... (same title)\u003c/a\u003e, Journal of Number Theory Volume 148, March 2015, Pages 537-592.",
				"Iaroslav V. Blagouchine, \u003ca href=\"http://dx.doi.org/10.1007/s11139-015-9763-z\"\u003eRediscovery of Malmsten’s integrals, their evaluation by contour integration methods and some related results\u003c/a\u003e, The Ramanujan Journal October 2014, Volume 35, Issue 1, pp 21-110.",
				"Iaroslav V. Blagouchine, \u003ca href=\"http://www.researchgate.net/publication/257381156_Rediscovery_of_Malmsten%27s_integrals_their_evaluation_by_contour_integration_methods_and_some_related_results\"\u003eRediscovery of Malmsten’s integrals: Full PDF text\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/HurwitzZetaFunction.html\"\u003eHurwitz Zeta Function\u003c/a\u003e;",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/StieltjesConstants.html\"\u003eStieltjes Constants\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Stieltjes_constants\"\u003eStieltjes constants\u003c/a\u003e"
			],
			"formula": [
				"Equals integral_[0..infinity] (6*(-2*arctan(6*x) + 6*x*log(1/36 + x^2)))/((-1 + exp(2*Pi*x))*(1 + 36*x^2)) dx - (3 + log(6)/2)*log(6)."
			],
			"example": [
				"-10.742582529547892258941196776243668301630426163606753795..."
			],
			"maple": [
				"evalf(int((6*(-2*arctan(6*x) + 6*x*log(1/36 + x^2)))/((-1 + exp(2*Pi*x))*(1 + 36*x^2)), x = 0..infinity) - (3 + log(6)/2)*log(6), 120); # _Vaclav Kotesovec_, Jan 29 2015"
			],
			"mathematica": [
				"gamma1[1/6] = (1/2)*((-Log[6])*Log[24] - EulerGamma*Log[432] - 2*Log[2]*Log[2*Pi^2] + Log[(2*Pi)/Sqrt[3]]*Log[144*Pi^2] + Log[Pi]*Log[4/Gamma[1/6]^2] - 2*Log[12]*Log[Gamma[1/6]] - 2*Log[12*Pi]*Log[Gamma[5/6]] - Sqrt[3]*Pi*(EulerGamma + Log[(12*2^(2/3)*Pi^(3/2)*Gamma[5/6])/Gamma[1/6]^2]) + 2*StieltjesGamma[1] + Derivative[2, 0][Zeta][0, 1/6] - Derivative[2, 0][Zeta][0, 1/3] - 2*Derivative[2, 0][Zeta][0, 1/2] - Derivative[2, 0][Zeta][0, 2/3] + Derivative[2, 0][Zeta][0, 5/6]) // Re; RealDigits[gamma1[1/6], 10, 102] // First",
				"(* Or, from Mma version 7 up: *) RealDigits[StieltjesGamma[1, 1/6], 10, 102] // First"
			],
			"xref": [
				"Cf. A001620 (gamma), A082633 (gamma_1), A254327 (gamma_1(1/2)), A254331 (gamma_1(1/3)), A254345 (gamma_1(2/3)), A254347 (gamma_1(1/4)), A254348 (gamma_1(3/4)), A254350 (gamma_1(5/6)), A251866 (gamma_1(1/5)), A255188 (gamma_1(1/8)), A255189 (gamma_1(1/12))."
			],
			"keyword": "nonn,cons,easy",
			"offset": "2,3",
			"author": "_Jean-François Alcover_, Jan 29 2015",
			"references": 11,
			"revision": 26,
			"time": "2020-11-30T08:42:23-05:00",
			"created": "2015-01-29T03:58:06-05:00"
		}
	]
}