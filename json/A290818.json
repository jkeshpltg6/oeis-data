{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A290818",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 290818,
			"data": "2,3,3,4,11,4,5,24,24,5,6,47,94,47,6,7,88,272,272,88,7,8,163,774,1185,774,163,8,9,304,2230,4280,4280,2230,304,9,10,575,6542,15781,20106,15781,6542,575,10,11,1104,19452,60604,88512,88512,60604,19452,1104,11",
			"name": "Array read by antidiagonals: T(m,n) = number of irredundant sets in the lattice (rook) graph K_m X K_n.",
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A290818/b290818.txt\"\u003eTable of n, a(n) for n = 1..1275\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/IrredundantSet.html\"\u003eIrredundant Set\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RookGraph.html\"\u003eRook Graph\u003c/a\u003e"
			],
			"formula": [
				"T(m,n) = A290632(m, n) + Sum_{k=0..m-1} Sum_{r=2*k..n-1} binomial(m,k) * binomial(n,r) * k! * A008299(r,k) * c(m-k,n-r) where c(m,n) = Sum_{i=0..m-1} binomial(n,i) * (n^i - n!*stirling2(i, n))."
			],
			"example": [
				"Array begins:",
				"===============================================================",
				"m\\n| 1   2     3      4       5        6        7         8",
				"---+-----------------------------------------------------------",
				"1  | 2   3     4      5       6        7        8         9 ...",
				"2  | 3  11    24     47      88      163      304       575 ...",
				"3  | 4  24    94    272     774     2230     6542     19452 ...",
				"4  | 5  47   272   1185    4280    15781    60604    240073 ...",
				"5  | 6  88   774   4280   20106    88512   400728   1879744 ...",
				"6  | 7 163  2230  15781   88512   453271  2326534  12363513 ...",
				"7  | 8 304  6542  60604  400728  2326534 13169346  76446456 ...",
				"8  | 9 575 19452 240073 1879744 12363513 76446456 476777153 ...",
				"..."
			],
			"mathematica": [
				"s[n_, k_]:=Sum[(-1)^i*Binomial[n, i] StirlingS2[n - i, k - i], {i, 0, Min[n, k]}];",
				"c[m_, n_, x_]:=Sum[Binomial[m, i] (n^i - n!*StirlingS2[i, n])*x^i, {i, 0, m - 1}];",
				"p[m_, n_, x_]:=Sum[Sum[Binomial[m, k] Binomial[n, r]* k!*s[r, k]*x^r*c[m - k, n - r, x], {r, 2k, n - 1}], {k,0, m - 1}];",
				"b[m_, n_, x_]:=m^n*x^n + n^m*x^m - If[n\u003c=m, n!*x^m*StirlingS2[m, n], m!*x^n*StirlingS2[n, m]];",
				"T[m_, n_]:= b[m, n, 1] + p[m, n, 1];",
				"Table[T[n, m -n + 1], {m, 10}, {n, m}]//Flatten",
				"(* _Indranil Ghosh_, Aug 12 2017, after PARI code *)"
			],
			"program": [
				"(PARI) \\\\ See A. Howroyd note in A290586 for explanation.",
				"s(n,k)=sum(i=0, min(n, k), (-1)^i * binomial(n, i) * stirling(n-i, k-i, 2) );",
				"c(m,n,x)=sum(i=0, m-1, binomial(m, i) * (n^i - n!*stirling(i, n, 2))*x^i);",
				"p(m,n,x)={sum(k=0, m-1, sum(r=2*k, n-1, binomial(m, k) * binomial(n, r) * k! * s(r, k) * x^r * c(m-k, n-r, x) ))}",
				"b(m,n,x) = m^n*x^n + n^m*x^m - if(n\u003c=m, n!*x^m*stirling(m, n, 2), m!*x^n*stirling(n, m, 2));",
				"T(m,n) = b(m,n,1) + p(m,n,1);",
				"for(m=1,10,for(n=1,m,print1(T(n,m-n+1),\", \")));"
			],
			"xref": [
				"Row 2 is A290707 for n \u003e 1.",
				"Main diagonal is A290586.",
				"Cf. A287274, A290632."
			],
			"keyword": "nonn,tabl",
			"offset": "1,1",
			"author": "_Andrew Howroyd_, Aug 11 2017",
			"references": 4,
			"revision": 25,
			"time": "2017-10-14T15:43:05-04:00",
			"created": "2017-08-12T13:02:13-04:00"
		}
	]
}