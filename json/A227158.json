{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A227158",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 227158,
			"data": "5,8,1,9,4,8,6,5,9,3,1,7,2,9,0,7,9,7,9,2,8,1,4,9,8,8,4,5,0,2,3,6,7,5,5,9,3,0,4,8,3,2,8,7,3,0,7,1,7,7,2,5,2,1,8,2,3,4,2,1,2,9,9,2,6,5,2,5,1,2,3,1,5,5,5,9,5,0,3,4,6,1,4,3,0,1,2,3,6,1,3,1,4,9,2,4,1,3,4,9,6",
			"name": "Second-order term in the asymptotic expansion of B(x), the count of numbers up to x which are the sum of two squares.",
			"comment": [
				"K = A064533, the Landau-Ramanujan constant, is the first-order term. This constant is c = lim_{x-\u003einfinity} (B(x)*sqrt(log x)/x - 1)*log x."
			],
			"reference": [
				"Steven R. Finch, Mathematical Constants, Cambridge University Press, 2003, Section 2.3 Landau-Ramanujan constants, p. 99."
			],
			"link": [
				"Alexandru Ciolan, Alessandro Languasco and Pieter Moree, \u003ca href=\"https://arxiv.org/abs/2109.03288\"\u003eLandau and Ramanujan approximations for divisor sums and coefficients of cusp forms\u003c/a\u003e, section 10, 31000 digits are obtained, arXiv:2109.03288 [math.NT], 2021.",
				"David Hare, \u003ca href=\"http://web.archive.org/web/20001210182300/http://www.lacim.uqam.ca/piDATA/landau.txt\"\u003eLandau-Ramanujan Constant, second order\u003c/a\u003e obtained about 5000 digits, 1996.",
				"D. Shanks, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1964-0159174-9\"\u003eThe second-order term in the asymptotic expansion of B(x)\u003c/a\u003e, Mathematics of Computation 18 (1964), pp. 75-86.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Landau-RamanujanConstant.html\"\u003eLandau-Ramanujan Constant\u003c/a\u003e"
			],
			"example": [
				"0.58194865931729079777136487517474826173838317235153574360562...."
			],
			"mathematica": [
				"digits = 101; m0 = 5; dm = 5; beta[x_] := 1/4^x*(Zeta[x, 1/4] - Zeta[x, 3/4]); L = Pi^(3/2)/Gamma[3/4]^2*2^(1/2)/2; Clear[f]; f[m_] := f[m] = 1/2*(1 - Log[Pi*E^EulerGamma/(2*L)]) - 1/4*NSum[ Zeta'[2^k]/Zeta[2^k] - beta'[2^k]/beta[2^k] + Log[2]/(2^(2^k) - 1), {k, 1, m}, WorkingPrecision -\u003e digits + 10] ; f[m0]; f[m = m0 + dm]; While[RealDigits[f[m], 10, digits] != RealDigits[f[m - dm], 10, digits], m = m + dm]; RealDigits[f[m], 10, digits] // First (* _Jean-François Alcover_, May 27 2014 *)"
			],
			"program": [
				"(PARI) L(s)=sumalt(k=0,(-1)^k/(2*k+1)^s)",
				"LL(s)=L'(s)/L(s)",
				"ZZ(s)=zeta'(s)/zeta(s)",
				"sm(x)=my(s);forprime(q=2,x,if(q%4==3,s+=log(q)/(q^8-1))); s+1/49/x^7+log(x)/7/x^7",
				"1/2+log(2)/4-Euler/4-LL(1)/4-ZZ(2)/4+LL(2)/4-log(2)/12-ZZ(4)/4+LL(4)/4-log(2)/60+sm(1e5)/2"
			],
			"xref": [
				"Cf. A064533, A001481."
			],
			"keyword": "nonn,cons",
			"offset": "0,1",
			"author": "_Charles R Greathouse IV_, Jul 03 2013",
			"ext": [
				"Corrected and extended by _Jean-François Alcover_, Mar 19 2014 and again May 27 2014"
			],
			"references": 4,
			"revision": 47,
			"time": "2021-10-12T16:17:55-04:00",
			"created": "2013-07-10T02:44:21-04:00"
		}
	]
}