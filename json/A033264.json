{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A033264",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 33264,
			"data": "0,1,0,1,1,1,0,1,1,2,1,1,1,1,0,1,1,2,1,2,2,2,1,1,1,2,1,1,1,1,0,1,1,2,1,2,2,2,1,2,2,3,2,2,2,2,1,1,1,2,1,2,2,2,1,1,1,2,1,1,1,1,0,1,1,2,1,2,2,2,1,2,2,3,2,2,2,2,1,2,2,3,2,3,3,3,2,2,2,3,2,2,2,2,1,1,1,2,1,2,2,2",
			"name": "Number of blocks of {1,0} in the binary expansion of n.",
			"comment": [
				"Number of i such that d(i) \u003c d(i-1), where Sum_{d(i)*2^i: i=0,1,....,m} is base 2 representation of n.",
				"This is the base-2 down-variation sequence; see A297330. - _Clark Kimberling_, Jan 18 2017"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A033264/b033264.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Jean-Paul Allouche and Jeffrey Shallit, \u003ca href=\"https://doi.org/10.1007/BFb0097122\"\u003eSums of digits and the Hurwitz zeta function\u003c/a\u003e, in: K. Nagasaka and E. Fouvry (eds.), Analytic Number Theory, Lecture Notes in Mathematics, Vol. 1434, Springer, Berlin, Heidelberg, 1990, pp. 19-30.",
				"Ralf Stephan, \u003ca href=\"/somedcgf.html\"\u003eSome divide-and-conquer sequences with (relatively) simple ordinary generating functions\u003c/a\u003e, 2004.",
				"Ralf Stephan, \u003ca href=\"/A079944/a079944.ps\"\u003eTable of generating functions\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/DigitBlock.html\"\u003eDigit Block\u003c/a\u003e.",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e"
			],
			"formula": [
				"G.f.: 1/(1-x) * Sum_(k\u003e=0, t^2/(1+t)/(1+t^2), t=x^2^k). - _Ralf Stephan_, Sep 10 2003",
				"a(n) = A069010(n) - (n mod 2). - _Ralf Stephan_, Sep 10 2003",
				"a(4n) = a(4n+1) = a(2n), a(4n+2) = a(n)+1, a(4n+3) = a(n). - _Ralf Stephan_, Aug 20 2003",
				"a(n) = A087116(n) for n \u003e 0, since strings of 0's alternate with strings of 1's, which end in (1,0). - _Jonathan Sondow_, Jan 17 2016",
				"Sum_{n\u003e=1} a(n)/(n*(n+1)) = Pi/4 - log(2)/2 (A196521) (Allouche and Shallit, 1990). - _Amiram Eldar_, Jun 01 2021"
			],
			"maple": [
				"f:= proc(n) option remember; local k;",
				"k:= n mod 4;",
				"if k = 2 then procname((n-2)/4) + 1",
				"elif k = 3 then procname((n-3)/4)",
				"else procname((n-k)/2)",
				"fi",
				"end proc:",
				"f(1):= 0: f(0):= q:",
				"seq(f(i),i=1..100); # _Robert Israel_, Aug 31 2015"
			],
			"mathematica": [
				"Table[Count[Partition[IntegerDigits[n, 2], 2, 1], {1, 0}], {n, 102}] (* _Michael De Vlieger_, Aug 31 2015, after _Robert G. Wilson v_ at A014081 *)",
				"Table[SequenceCount[IntegerDigits[n,2],{1,0}],{n,110}] (* Requires Mathematica version 10 or later *) (* _Harvey P. Dale_, Jan 26 2017 *)"
			],
			"program": [
				"(Haskell)",
				"a033264 = f 0 . a030308_row where",
				"   f c [] = c",
				"   f c (0 : 1 : bs) = f (c + 1) bs",
				"   f c (_ : bs) = f c bs",
				"-- _Reinhard Zumkeller_, Feb 20 2014, Jun 17 2012",
				"(PARI)",
				"a(n) = { hammingweight(bitand(n\u003e\u003e1, bitneg(n))) }; \\\\ _Gheorghe Coserea_, Aug 30 2015"
			],
			"xref": [
				"Cf. A014081, A014082, A037800, A056974, A056975, A056976, A056977, A056978, A056979, A056980, A196521.",
				"a(n) = A005811(n) - ceiling(A005811(n)/2) = A005811(n) - A069010(n).",
				"Equals (A072219(n+1)-1)/2.",
				"Cf. also A175047, A030308.",
				"Essentially the same as A087116."
			],
			"keyword": "nonn,base,easy",
			"offset": "1,10",
			"author": "_Clark Kimberling_",
			"references": 17,
			"revision": 54,
			"time": "2021-06-01T02:14:04-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}