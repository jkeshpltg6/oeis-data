{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A244375",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 244375,
			"data": "1,3,1,-3,0,3,2,3,1,0,0,-3,2,6,0,-3,0,3,2,0,2,0,0,3,1,6,1,-6,0,0,2,3,0,0,0,-3,2,6,2,0,0,6,2,0,0,0,0,-3,3,3,0,-6,0,3,0,6,2,0,0,0,2,6,2,-3,0,0,2,0,0,0,0,3,2,6,1,-6,0,6,2,0,1,0,0",
			"name": "Expansion of (a(q) + 3*a(q^2) - 4*a(q^4)) / 6 in powers of q where a() is a cubic AGM theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Cubic AGM theta functions: a(q) (see A004016), b(q) (A005928), c(q) (A005882)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A244375/b244375.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of (b(q) - b(q^4)) * (b(q) - 2*b(q^4)) / (3* b(q^2)) = b(q^2)^2 * (b(q^4) - b(q)) / (3 * b(q) * b(q^4)) in powers of q where b() is a cubic AGM theta function.",
				"Expansion of q * phi(q)^2 * psi(q^6)^2 / (psi(q) * psi(q^3)) in powers of q where phi(), psi() are Ramanujan theta functions.",
				"Expansion of q * chi(q)^3 * phi(-q^2) * psi(-q^3) / chi(-q^6)^3 in powers of q where phi(), psi(), chi() are Ramanujan theta functions. - _Michael Somos_, Jan 17 2015",
				"Expansion of q * f(-q) * f(q, q^5)^4 / f(-q^3)^3 in powers of q where f() is a Ramanujan theta function. - _Michael Somos_, Jan 17 2015",
				"Expansion of eta(q^2)^8 * eta(q^3) * eta(q^12)^4 / (eta(q)^3 * eta(q^4)^4 * eta(q^6)^4) in powers of q. - _Michael Somos_, Jan 17 2015",
				"a(n) is multiplicative with a(2^e) = 3 * (-1)^(e+1) if e\u003e0, a(3^e) = 1, a(p^e) = e+1 if p == 1 (mod 6), a(p^e) = (1 + (-1)^e) / 2 if p == 5 (mod 6).",
				"Euler transform of period 12 sequence [ 3, -5, 2, -1, 3, -2, 3, -1, 2, -5, 3, -2, ...].",
				"Moebius transform is period 12 sequence [ 1, 2, 0, -6, -1, 0, 1, 6, 0, -2, -1, 0, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (12 t)) = 3^(1/2) (t/i) g(t) where q = exp(2 Pi i t) and g() is the g.f. for A244339.",
				"a(2*n) = 3 * A093829(n). a(2*n + 1) = A033762(n). a(3*n) = a(n). a(3*n + 1) = A122861(n). a(6*n + 2) = 3 * A033687(n). a(6*n + 5) = 0.",
				"a(n) = -(-1)^n * A112298(n). - _Michael Somos_, Jan 17 2015"
			],
			"example": [
				"G.f. = q + 3*q^2 + q^3 - 3*q^4 + 3*q^6 + 2*q^7 + 3*q^8 + q^9 - 3*q^12 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 1, 0, Sum[ Mod[ n/d, 2] {1, 3, 0, -3, -1, 0}[[ Mod[ d, 6, 1] ]], {d, Divisors @ n}]];",
				"a[ n_] := SeriesCoefficient[ QPochhammer[ q^2]^8 QPochhammer[ q^3] QPochhammer[ q^12]^4 / (QPochhammer[ q]^3 QPochhammer[ q^4]^4 QPochhammer[ q^6]^4), {q, 0, n}];",
				"a[ n_] := SeriesCoefficient[ q QPochhammer[ -q, q^2]^3 QPochhammer[ -q^6, q^6]^3 EllipticTheta[ 4, 0, q^2] EllipticTheta[ 2, Pi/4, q^(3/2)] / (2^(1/2) q^(3/8)), {q, 0, n}]; (* _Michael Somos_, Jan 17 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, 0, sumdiv(n, d, (n/d%2) * [0, 1, 3, 0, -3, -1][d%6 + 1]))};",
				"(PARI) {a(n) = my(A); if( n\u003c1, 0, n--; A = x * O(x^n); polcoeff( eta(x^2 + A)^8 * eta(x^3 + A) * eta(x^12 + A)^4 / (eta(x + A)^3 * eta(x^4 + A)^4 * eta(x^6 + A)^4), n))};",
				"(PARI) {a(n) = if( n\u003c0, 0, polcoeff( sum(k=1, n, x^k / (1 + x^k + x^(2*k)) * [0, 1, 4, 1][k%4 + 1], x * O(x^n)), n))};",
				"(PARI) {a(n) = if( n\u003c0, 0, polcoeff( sum(k=1, n, x^k / (1 - x^(2*k)) * [0, 1, 3, 0, -3, -1][k%6 + 1], x * O(x^n)), n))};",
				"(PARI) {a(n) = my(A);  if( n\u003c1, 0, A = factor(n); prod( j=1, matsize(A)[1], if( p = A[j,1], e = A[j,2]; if( p==2, 3 * (-1)^(e+1), if( p==3, 1, if( p%6 == 1, e+1, (1 + (-1)^e) / 2))))))};",
				"(MAGMA) A := Basis( ModularForms( Gamma1(12), 1), 82); A[2] + 3*A[3] + A[4] - 3*A[5]; /* _Michael Somos_, Jan 17 2015 */"
			],
			"xref": [
				"Cf. A033687, A033762, A093829, A112298, A122861, A244339."
			],
			"keyword": "sign,mult",
			"offset": "1,2",
			"author": "_Michael Somos_, Jun 26 2014",
			"references": 4,
			"revision": 16,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2014-06-26T23:31:29-04:00"
		}
	]
}