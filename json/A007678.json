{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007678",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7678,
			"id": "M3411",
			"data": "0,0,1,4,11,24,50,80,154,220,375,444,781,952,1456,1696,2500,2466,4029,4500,6175,6820,9086,9024,12926,13988,17875,19180,24129,21480,31900,33856,41416,43792,52921,52956,66675,69996,82954,86800,102050",
			"name": "Number of regions in regular n-gon with all diagonals drawn.",
			"comment": [
				"This sequence and A006533 are two equivalent ways of presenting the same sequence.",
				"A quasipolynomial of order 2520. - _Charles R Greathouse IV_, Jan 15 2013",
				"Also the circuit rank of the n-polygon diagonal intersection graph. - _Eric W. Weisstein_, Mar 08 2018",
				"This sequence only counts polygons, in contrast to A006533, which also counts the n segments of the circumscribed circle delimited by the edges of the regular n-gon. Therefore, a(n) = A006533(n) - n. See also A006561 which counts the number of intersection points, and A350000 which considers iterated \"cutting along diagonals\". - _M. F. Hasler_, Dec 13 2021"
			],
			"reference": [
				"Jean Meeus, Wiskunde Post (Belgium), Vol. 10, 1972, pp. 62-63.",
				"C. A. Pickover, The Mathematics of Oz, Problem 58 \"The Beauty of Polygon Slicing\", Cambridge University Press, 2002.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A007678/b007678.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Lars Blomberg, Scott R. Shannon, and N. J. A. Sloane, \u003ca href=\"http://neilsloane.com/doc/rose_5.pdf\"\u003eGraphical Enumeration and Stained Glass Windows, 1: Rectangular Grids\u003c/a\u003e, (2020). Also arXiv:2009.07918.",
				"Robert Dougherty-Bliss, \u003ca href=\"https://gist.github.com/rwbogl/e0c6e2ba5e901188497131ac11a88f2e\"\u003eFirst draft of Python program to produce colored drawings of these figures\u003c/a\u003e, Github, Feb 09 2020.",
				"M. Griffiths, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL13/Griffiths2/griffiths.html\"\u003eCounting the regions in a regular drawing of K_{n,n}\u003c/a\u003e, J. Int. Seq. 13 (2010) # 10.8.5.",
				"M. F. Hasler, \u003ca href=\"/A006561/a006561.html\"\u003eInteractive illustration of A006561(n) \u0026 A006533(n)\u003c/a\u003e; \u003ca href=\"/A006533/a006533.png\"\u003ecolored version for n=6\u003c/a\u003e \u003ca href=\"/A006533/a006533_1.png\"\u003eand for n=8\u003c/a\u003e.",
				"Sascha Kurz, \u003ca href=\"http://www.mathe2.uni-bayreuth.de/sascha/oeis/drawing/drawing.html\"\u003em-gons in regular n-gons\u003c/a\u003e (in German).",
				"J. Meeus \u0026 N. J. A. Sloane, \u003ca href=\"/A006532/a006532_1.pdf\"\u003eCorrespondence, 1974-1975\u003c/a\u003e",
				"B. Poonen and M. Rubinstein, \u003ca href=\"https://doi.org/10.1137/S0895480195281246\"\u003eThe Number of Intersection Points Made by the Diagonals of a Regular Polygon\u003c/a\u003e, SIAM J. Discrete Mathematics 11, no. 1 (1998), pp. 135-156; DOI:10.1137/S0895480195281246. [\u003ca href=\"http://math.mit.edu/~poonen/papers/ngon.pdf\"\u003eAuthor's copy\u003c/a\u003e]. The latest arXiv version \u003ca href=\"http://arxiv.org/abs/math/9508209\"\u003earXiv:math/9508209\u003c/a\u003e has corrected some some typos in the published version.",
				"B. Poonen and M. Rubinstein, \u003ca href=\"http://math.mit.edu/~poonen/papers/ngon.m\"\u003eMathematica programs for these sequences\u003c/a\u003e",
				"J. F. Rigby, \u003ca href=\"https://doi.org/10.1007/BF00147438\"\u003eMultiple intersections of diagonals of regular polygons, and related topics\u003c/a\u003e, Geom. Dedicata 9 (1980), 207-238.",
				"M. Rubinstein, \u003ca href=\"/A006561/a006561_3.pdf\"\u003eDrawings for n=4,5,6,...\u003c/a\u003e",
				"Scott R. Shannon, \u003ca href=\"/A007678/a007678.png\"\u003eColored illustration for n = 17\u003c/a\u003e",
				"Scott R. Shannon, \u003ca href=\"/A007678/a007678_1.png\"\u003eColored illustration for n = 18\u003c/a\u003e",
				"Scott R. Shannon, \u003ca href=\"/A007678/a007678_2.png\"\u003eColored illustration for n = 19\u003c/a\u003e",
				"Scott R. Shannon, \u003ca href=\"/A007678/a007678_3.png\"\u003eColored illustration for n = 23\u003c/a\u003e",
				"Scott R. Shannon, \u003ca href=\"/A007678/a007678_4.png\"\u003eColored illustration for n = 27\u003c/a\u003e",
				"Scott R. Shannon, \u003ca href=\"/A007678/a007678_6.png\"\u003eColored illustration for n = 40\u003c/a\u003e",
				"Scott R. Shannon, \u003ca href=\"/A007678/a007678_7.png\"\u003eColored illustration for n = 41 (1st version)\u003c/a\u003e",
				"Scott R. Shannon, \u003ca href=\"/A007678/a007678_8.png\"\u003eColored illustration for n = 41 (2nd version)\u003c/a\u003e",
				"Scott R. Shannon, \u003ca href=\"/A007678/a007678_9.png\"\u003eColored illustration for n = 41 (3rd version)\u003c/a\u003e. This variation has coloring based on the number of edges of the polygon: red = 3-gon, orange = 4-gon, yellow = 5-gon, light-green = 6-gon etc.",
				"N. J. A. Sloane (in collaboration with Scott R. Shannon), \u003ca href=\"/A331452/a331452.pdf\"\u003eArt and Sequences\u003c/a\u003e, Slides of guest lecture in Math 640, Rutgers Univ., Feb 8, 2020. Mentions this sequence.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CircuitRank.html\"\u003eCircuit Rank\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PolygonDiagonalIntersectionGraph.html\"\u003ePolygon Diagonal Intersection Graph\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RegularPolygonDivisionbyDiagonals.html\"\u003eRegular Polygon Division by Diagonals\u003c/a\u003e",
				"\u003ca href=\"/index/Pol#Poonen\"\u003eSequences formed by drawing all diagonals in regular polygon\u003c/a\u003e",
				"\u003ca href=\"/index/Ch#CHORD\"\u003eSequences related to chord diagrams\u003c/a\u003e"
			],
			"formula": [
				"For odd n \u003e 3, a(n) = sumstep {i=5, n, 2, (i-2)*floor(n/2)+(i-4)*ceiling(n/2)+1} + x*(x+1)*(2*x+1)/6*n), where x = (n-5)/2. Simplifying the floor/ceiling components gives the PARI code below. - _Jon Perry_, Jul 08 2003",
				"For odd n, a(n) = (24 - 42*n + 23*n^2 - 6*n^3 + n^4)/24. - _Graeme McRae_, Dec 24 2004",
				"a(n) = A006533(n) - n. - _T. D. Noe_, Dec 23 2006",
				"For odd n, binomial transform of [1, 10, 29, 36, 16, 0, 0, 0, ...] = [1, 11, 50, 154, ...]. - _Gary W. Adamson_, Aug 02 2011",
				"a(n) = A135565(n) - A007569(n) + 1. - _Max Alekseyev_",
				"See the Mma code in A006533 for the explicit Poonen-Rubenstein formula that holds for all n. - _N. J. A. Sloane_, Jan 23 2020"
			],
			"mathematica": [
				"del[m_,n_]:=If[Mod[n,m]==0,1,0]; R[n_]:=If[n\u003c3, 0, (n^4-6n^3+23n^2-42n+24)/24 + del[2,n](-5n^3+42n^2-40n-48)/48 - del[4,n](3n/4) + del[6,n](-53n^2+310n)/12 + del[12,n](49n/2) + del[18,n]*32n + del[24,n]*19n - del[30,n]*36n - del[42,n]*50n - del[60,n]*190n - del[84,n]*78n - del[90,n]*48n - del[120,n]*78n - del[210,n]*48n]; Table[R[n], {n,1,1000}] (* _T. D. Noe_, Dec 21 2006 *)"
			],
			"program": [
				"(PARI) /* Only for odd n \u003e 3, not suitable for other values of n! */ { a(n)=local(nr,x,fn,cn,fn2); nr=0; fn=floor(n/2); cn=ceil(n/2); fn2=(fn-1)^2-1; nr=fn2*n+fn+(n-2)*fn+cn; x=(n-5)/2; if (x\u003e0,nr+=x*(x+1)*(2*x+1)/6*n); nr; } \\\\ _Jon Perry_, Jul 08 2003",
				"(PARI) apply( {A007678(n)=if(n%2, (((n-6)*n+23)*n-42)*n/24+1, ((n^3/2 -17*n^2/4 +22*n -if(n%4, 31, 40) +!(n%6)*(310 -53*n))/12 +!(n%12)*49/2 +!(n%18)*32 +!(n%24)*19 -!(n%30)*36 -!(n%42)*50 -!(n%60)*190 -!(n%84)*78 -!(n%90)*48 -!(n%120)*78 -!(n%210)*48)*n)}, [1..44]) \\\\ _M. F. Hasler_, Aug 06 2021",
				"(Python)",
				"def d(n,m): return not n % m",
				"def A007678(n): return (1176*d(n,12)*n - 3744*d(n,120)*n + 1536*d(n,18)*n - d(n,2)*(5*n**3 - 42*n**2 + 40*n + 48) - 2304*d(n,210)*n + 912*d(n,24)*n - 1728*d(n,30)*n - 36*d(n,4)*n - 2400*d(n,42)*n - 4*d(n,6)*n*(53*n - 310) - 9120*d(n,60)*n - 3744*d(n,84)*n - 2304*d(n,90)*n + 2*n**4 - 12*n**3 + 46*n**2 - 84*n)//48 + 1 # _Chai Wah Wu_, Mar 08 2021"
			],
			"xref": [
				"Cf. A001006, A054726, A006533, A006561, A006600, A007569 (number of vertices), A006522, A135565 (number of line segments).",
				"A062361 gives number of triangles, A331450 and A331451 give distribution of polygons by number of sides.",
				"A333654, A335614, A335646, A337330 give the number of internal n-gon to k-gon contacts for n\u003e=3, k\u003e=n."
			],
			"keyword": "nonn,nice",
			"offset": "1,4",
			"author": "_N. J. A. Sloane_, Bjorn Poonen (poonen(AT)math.princeton.edu)",
			"ext": [
				"More terms from _Graeme McRae_, Dec 26 2004",
				"a(1) = a(2) = 0 prepended by _Max Alekseyev_, Dec 01 2011"
			],
			"references": 119,
			"revision": 128,
			"time": "2021-12-13T02:46:31-05:00",
			"created": "1994-09-19T03:00:00-04:00"
		}
	]
}