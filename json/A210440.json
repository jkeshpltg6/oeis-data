{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A210440",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 210440,
			"data": "0,4,16,40,80,140,224,336,480,660,880,1144,1456,1820,2240,2720,3264,3876,4560,5320,6160,7084,8096,9200,10400,11700,13104,14616,16240,17980,19840,21824,23936,26180,28560,31080,33744,36556,39520,42640,45920,49364,52976",
			"name": "a(n) = 2*n*(n+1)*(n+2)/3.",
			"comment": [
				"Number of tin boxes necessary to build a tetrahedron with side length n, as shown in the link.",
				"This sequence is related to A028552 by a(n) = n*A028552(n)-sum(A028552(i), i=0..n-1) with n\u003e0. Also, 4*A001296(n) = n*a(n)-sum(a(i), i=0..n-1) with n\u003e0. [_Bruno Berselli_, Jan 21 2013]",
				"If \"0\" is prepended, a(n) is the convolution of 2n with itself. - _Wesley Ivan Hurt_, Mar 14 2015"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A210440/b210440.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"P. Gallais, \u003ca href=\"http://images.math.cnrs.fr/Ceci-n-est-pas-une-mise-en-boite.html\"\u003eCeci n’est pas une mise en boîte !\u003c/a\u003e, Images des Mathématiques, CNRS, 2012.",
				"P. Gallais, \u003ca href=\"http://images.math.cnrs.fr/La-vis-sans-fin.html\"\u003eLa vis ... sans fin\u003c/a\u003e, Images des Mathématiques, CNRS, 2012.",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-6,4,-1)."
			],
			"formula": [
				"a(n) = 4*A000292(n).",
				"a(n+1)-a(n) = A046092(n+1).",
				"From _Bruno Berselli_, Jan 20 2013: (Start)",
				"G.f.: 4*x/(1-x)^4.",
				"a(n) = -a(-n-2) = 4*a(n-1)-6*a(n-2)+4*a(n-3)-a(n-4).",
				"a(n)-a(-n) = A217873(n).",
				"a(n)+a(-n) = A016742(n).",
				"(n-1)*a(n)-n*a(n-1) = A130809(n+1) with n\u003e1.",
				"(End)",
				"G.f.: 2*x*W(0) , where W(k) = 1 + 1/( 1 - x*(k+2)*(k+4)/(x*(k+2)*(k+4) + (k+1)*(k+2)/W(k+1) )) ); (continued fraction). - _Sergei N. Gladkovskii_, Aug 24 2013",
				"a(n) = sum_{i=1..n} i*(2n-i+3). - _Wesley Ivan Hurt_, Oct 03 2013"
			],
			"maple": [
				"A210440:=n-\u003e2*n*(n+1)*(n+2)/3; seq(A210440(k), k=0..100); # _Wesley Ivan Hurt_, Sep 25 2013"
			],
			"mathematica": [
				"Table[2n(n+1)(n+2)/3,{n,0,50}] (* or *) LinearRecurrence[{4,-6,4,-1},{0,4,16,40},50] (* _Harvey P. Dale_, Feb 13 2013 *)",
				"CoefficientList[Series[4 x/(1 - x)^4, {x, 0, 40}], x] (* _Vincenzo Librandi_, Jun 24 2014 *)"
			],
			"program": [
				"(Maxima) A210440(n):=2*n*(n+1)*(n+2)/3$ makelist(A210440(n),n,0,20); /* _Martin Ettl_, Jan 22 2013 */",
				"(MAGMA) [2*n*(n+1)*(n+2)/3: n in [0..50]]; // _Vincenzo Librandi_, Jun 24 2014"
			],
			"xref": [
				"Cf. A000292, A028552, A033488 (partial sums), A046092, A130809."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Michel Marcus_, Jan 20 2013",
			"references": 2,
			"revision": 53,
			"time": "2015-06-13T00:54:12-04:00",
			"created": "2013-01-20T10:39:43-05:00"
		}
	]
}