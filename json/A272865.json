{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A272865",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 272865,
			"data": "4,24,160,132,936,5700,720,5312,33264,198144,3940,29880,190980,1155600,6823620,21672,167712,1088856,6670656,39786120,233908896,119812,941640,6189540,38300976,230340740,1363667256,7997325700",
			"name": "Triangle read by rows, T(n,k) are covariances of inverse power traces of complex Wishart matrices with parameter c=2, for n\u003e=1 and 1\u003c=k\u003c=n.",
			"comment": [
				"These numbers provide the covariances of power traces of the time-delay matrix when the scattering matrix belongs to the Dyson ensembles.",
				"Relation with A047781 and A002002. See eq. (60) and (61) in Cunden et al., J. Phys. A: Math. Theor. 49, 18LT01 (2016)."
			],
			"reference": [
				"F. D. Cunden, \"Statistical distribution of the Wigner-Smith time-delay matrix moments for chaotic cavities\", Phys. Rev. E 91, 060102(R) (2015).",
				"F. D. Cunden, F. Mezzadri, N. Simm and P. Vivo, \"Correlators for the Wigner-Smith time-delay matrix of chaotic cavities\", J. Phys. A: Math. Theor. 49, 18LT01 (2016).",
				"F. D. Cunden, F. Mezzadri, N. O'Connell and N. Simm, \"Moments of Random Matrices and Hypergeometric Orthogonal Polynomials\", Commun. Math. Phys. 369, 1091-1145 (2019)."
			],
			"link": [
				"F. D. Cunden, \u003ca href=\"http://arxiv.org/abs/1412.2172\"\u003eStatistical distribution of the Wigner-Smith time-delay matrix moments for chaotic cavities\u003c/a\u003e, arXiv:1412.2172 [cond-mat.mes-hall], 2014-2015.",
				"F. D. Cunden, F. Mezzadri, N. Simm and P. Vivo, \u003ca href=\"http://arxiv.org/abs/1601.06690\"\u003eCorrelators for the Wigner-Smith time-delay matrix of chaotic cavities\u003c/a\u003e, arXiv:1601.06690 [math-ph], 2016.",
				"F. D. Cunden, F. Mezzadri, N. O'Connell and N. Simm, \u003ca href=\"https://arxiv.org/abs/1805.08760\"\u003eMoments of Random Matrices and Hypergeometric Orthogonal Polynomials\u003c/a\u003e, arXiv:1805.08760 [math-ph], 2018."
			],
			"formula": [
				"G.f.: ((x*y)/(x-y)^2)*((x*y-3(x+y)+1)/(sqrt(x^2-6x+1)*sqrt(y^2-6y+1))-1).",
				"T(n,1)/4 = A050151(n) for n\u003e=1. - _Peter Luschny_, May 08 2016"
			],
			"example": [
				"Triangle starts:",
				"4;",
				"24,   160;",
				"132,  936,   5700;",
				"720,  5312,  33264,  198144;",
				"3940, 29880, 190980, 1155600, 6823620;"
			],
			"maple": [
				"P := (n,k) -\u003e simplify(n*hypergeom([1-k,k+1],[1],-1)*hypergeom([1-n,n+1],[2],-1)): seq(seq(4*(n*k)*(P(n,k)+P(k,n))/(n+k),k=1..n),n=1..7); # _Peter Luschny_, May 08 2016"
			],
			"mathematica": [
				"Clear[\"Global`*\"];(*Wigner-Smith Covariance*)",
				"P[k_] := Sum[Binomial[k - 1, j] Binomial[k + j, j], {j, 0, k - 1}]",
				"Q[k_] := Sum[Binomial[k, j + 1] Binomial[k + j, j], {j, 0, k - 1}]",
				"a[k1_, k2_] := 4 (k1 k2)/(k1 + k2) (P[k1] Q[k2] + P[k2] Q[k1])",
				"L = 10; Table[a[k, l], {k, 1, L}, {l, 1, k}]"
			],
			"xref": [
				"Cf. A002002, A047781, A050151."
			],
			"keyword": "nonn,tabl",
			"offset": "1,1",
			"author": "_Fabio Deelan Cunden_, May 08 2016",
			"references": 0,
			"revision": 18,
			"time": "2019-07-20T12:27:14-04:00",
			"created": "2016-05-09T04:35:26-04:00"
		}
	]
}