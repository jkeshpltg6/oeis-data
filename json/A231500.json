{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A231500",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 231500,
			"data": "0,1,2,6,7,11,15,24,25,29,33,42,46,55,64,80,81,85,89,98,102,111,120,136,140,149,158,174,183,199,215,240,241,245,249,258,262,271,280,296,300,309,318,334,343,359,375,400,404,413,422,438,447,463,479,504,513,529,545,570,586,611,636,672,673,677,681,690,694",
			"name": "a(n) = Sum_{i=0..n} wt(i)^2, where wt(i) = A000120(i).",
			"comment": [
				"Stolarsky (1977) has an extensive bibliography."
			],
			"link": [
				"Ivan Neretin, \u003ca href=\"/A231500/b231500.txt\"\u003eTable of n, a(n) for n = 0..1024\u003c/a\u003e",
				"Jean Coquet, \u003ca href=\"https://doi.org/10.1016/0022-314X(86)90067-3\"\u003ePower sums of digital sums\u003c/a\u003e, J. Number Theory 22 (1986), no. 2, 161-176.",
				"P. J. Grabner, P. Kirschenhofer, H. Prodinger, R. F. Tichy, \u003ca href=\"http://math.sun.ac.za/~hproding/abstract/abs_80.htm\"\u003eOn the moments of the sum-of-digits function\u003c/a\u003e, \u003ca href=\"http://math.sun.ac.za/~hproding/pdffiles/st_andrews.pdf\"\u003ePDF\u003c/a\u003e, Applications of Fibonacci numbers, Vol. 5 (St. Andrews, 1992), 263-271, Kluwer Acad. Publ., Dordrecht, 1993.",
				"J.-L. Mauclaire, Leo Murata, \u003ca href=\"https://dx.doi.org/10.3792/pjaa.59.274\"\u003eOn q-additive functions. I\u003c/a\u003e, Proc. Japan Acad. Ser. A Math. Sci. 59 (1983), no. 6, 274-276.",
				"J.-L. Mauclaire, Leo Murata, \u003ca href=\"https://dx.doi.org/10.3792/pjaa.59.441\"\u003eOn q-additive functions. II\u003c/a\u003e, Proc. Japan Acad. Ser. A Math. Sci. 59 (1983), no. 9, 441-444.",
				"K. B. Stolarsky, \u003ca href=\"http://dx.doi.org/10.1137/0132060\"\u003ePower and exponential sums of digital sums related to binomial coefficient parity\u003c/a\u003e, SIAM J. Appl. Math., 32 (1977), 717-730.",
				"J. R. Trollope, \u003ca href=\"http://www.jstor.org/stable/2687954\"\u003eAn explicit expression for binary digital sums\u003c/a\u003e, Math. Mag. 41 1968 21-25."
			],
			"formula": [
				"Stolarsky (1977) studies the asymptotics."
			],
			"maple": [
				"digsum:=proc(n,B) local a; a := convert(n, base, B):",
				"add(a[i], i=1..nops(a)): end;",
				"f:=proc(n,k,B) global digsum; local i;",
				"add( digsum(i,B)^k,i=0..n); end;",
				"[seq(f(n,1,2),n=0..100)]; #A000788",
				"[seq(f(n,2,2),n=0..100)]; #A231500",
				"[seq(f(n,3,2),n=0..100)]; #A231501",
				"[seq(f(n,4,2),n=0..100)]; #A231502"
			],
			"mathematica": [
				"FoldList[#1 + DigitCount[#2, 2, 1]^2 \u0026, 0, Range[1, 68]] (* _Ivan Neretin_, May 21 2015 *)"
			],
			"program": [
				"(PARI) a(n) = sum(i=0, n, hammingweight(i)^2); \\\\ _Michel Marcus_, Sep 20 2017"
			],
			"xref": [
				"Cf. A000120, A000788, A231501, A231502."
			],
			"keyword": "nonn,base",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_, Nov 12 2013",
			"references": 3,
			"revision": 20,
			"time": "2017-09-20T12:04:12-04:00",
			"created": "2013-11-12T13:03:37-05:00"
		}
	]
}