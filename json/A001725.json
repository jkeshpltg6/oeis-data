{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001725",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1725,
			"id": "M4243 N1772",
			"data": "1,6,42,336,3024,30240,332640,3991680,51891840,726485760,10897286400,174356582400,2964061900800,53353114214400,1013709170073600,20274183401472000,425757851430912000,9366672731480064000,215433472824041472000,5170403347776995328000",
			"name": "a(n) = n!/5!.",
			"comment": [
				"The asymptotic expansion of the higher-order exponential integral E(x,m=1,n=6) ~ exp(-x)/x*(1 - 6/x + 42/x^2 - 336/x^3 + 3024/x^4 - 30240/x^5 + 332640/x^6 - 3991680/x^7 + ...) leads to the sequence given above. See A163931 and A130534 for more information. - _Johannes W. Meijer_, Oct 20 2009"
			],
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A001725/b001725.txt\"\u003eTable of n, a(n) for n = 5..300\u003c/a\u003e",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=265\"\u003eEncyclopedia of Combinatorial Structures 265\u003c/a\u003e",
				"Wolfdieter Lang, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL3/LANG/lang.html\"\u003eOn generalizations of Stirling number triangles\u003c/a\u003e, J. Integer Seqs., Vol. 3 (2000), #00.2.4.",
				"D. S. Mitrinovic and R. S. Mitrinovic, \u003ca href=\"http://pefmath2.etf.rs/files/54/107.pdf\"\u003eTableaux d'une classe de nombres reliés aux nombres de Stirling. II\u003c/a\u003e, Univ. Beograd. Publ. Elektrotehn. Fak. Ser. Mat. Fiz. No. 107-108 1963 1-77.",
				"Alexsandar Petojevic, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/index.html\"\u003eThe Function vM_m(s; a; z) and Some Well-Known Sequences\u003c/a\u003e, Journal of Integer Sequences, Vol. 5 (2002), Article 02.1.7",
				"\u003ca href=\"/index/Di#divseq\"\u003eIndex to divisibility sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Fa#factorial\"\u003eIndex entries for sequences related to factorial numbers\u003c/a\u003e"
			],
			"formula": [
				"E.g.f. if offset 0: 1/(1-x)^6.",
				"a(n) = A173333(n,5). - _Reinhard Zumkeller_, Feb 19 2010",
				"G.f.: G(0)/2, where G(k)= 1 + 1/(1 - x*(k+6)/(x*(k+6) + 1/G(k+1))); (continued fraction). - _Sergei N. Gladkovskii_, Jun 06 2013",
				"G.f.: W(0)/(40*x^2) -1/(20*x^2) -1/(5*x) , where W(k) = 1 + 1/( 1 - x*(k+4)/( x*(k+4) + 1/W(k+1) )); (continued fraction). - _Sergei N. Gladkovskii_, Aug 21 2013",
				"a(n) = A245334(n,n-5) / 6. - _Reinhard Zumkeller_, Aug 31 2014",
				"E.g.f.: x^5 / (5! * (1 - x)). - _Ilya Gutkovskiy_, Jul 09 2021"
			],
			"mathematica": [
				"lst={};Do[AppendTo[lst, n!/5! ], {n, 5, 5!}];lst (* _Vladimir Joseph Stephan Orlovsky_, Oct 25 2008 *)",
				"Range[5,30]!/120 (* _Harvey P. Dale_, Dec 20 2014 *)"
			],
			"program": [
				"(PARI) a(n)=n!/120 \\\\ _Charles R Greathouse IV_, Jul 19 2011",
				"(MAGMA) [Factorial(n)/120: n in [5..25]]; // _Vincenzo Librandi_, Jul 20 2011",
				"(Haskell)",
				"a001725 = (flip div 120) . a000142 -- _Reinhard Zumkeller_, Aug 31 2014"
			],
			"xref": [
				"a(n)= A049374(n-4), n \u003e= 1 (first column of triangle). Cf. A049460, A051339. a(n)= A051338(n-5, 0)*(-1)^(n-1) (first unsigned column of triangle).",
				"Cf. A245334, A000142."
			],
			"keyword": "nonn,easy",
			"offset": "5,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Harvey P. Dale_, Dec 20 2014"
			],
			"references": 37,
			"revision": 54,
			"time": "2021-07-10T03:04:19-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}