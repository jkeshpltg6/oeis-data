{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A307510",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 307510,
			"data": "0,0,0,0,1,0,0,2,0,0,4,0,3,8,0,6,16,0,12,4,9,24,8,18,0,16,36,12,32,0,24,54,0,48,20,36,81,40,72,30,64,0,60,108,45,96,40,90,48,80,144,60,135,72,120,54,0,192,108,180,96,160,72,162,256,144,240,100",
			"name": "a(n) is the greatest product i*j*k*l where i^2 + j^2 + k^2 + l^2 = n and 0 \u003c= i \u003c= j \u003c= k \u003c= l.",
			"comment": [
				"The sequence is well defined as every nonnegative integer can be represented as a sum of four squares in at least one way."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A307510/b307510.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A307510/a307510.png\"\u003eColored scatterplot of the first 20000 terms\u003c/a\u003e (where the color is function of the parity of n)",
				"Rémy Sigrist, \u003ca href=\"/A307510/a307510.txt\"\u003eC program for A307510\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Lagrange%27s_four-square_theorem\"\u003eLagrange's four-square theorem\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 0 iff n belongs to A000534.",
				"a(n) \u003c= (n/4)^2, with equality if and only if n is an even square. - _Robert Israel_, Apr 15 2019"
			],
			"example": [
				"For n = 34:",
				"- 34 can be expressed in 4 ways as a sum of four squares:",
				"    i^2 + j^2 + k^2 + l^2   i*j*k*l",
				"    ---------------------   -------",
				"    0^2 + 0^2 + 3^2 + 5^2         0",
				"    0^2 + 3^2 + 3^2 + 4^2         0",
				"    1^2 + 1^2 + 4^2 + 4^2        16",
				"    1^2 + 2^2 + 2^2 + 5^2        20",
				"- a(34) = max(0, 16, 20) = 20."
			],
			"maple": [
				"g:= proc(n, k) option remember; local a;",
				"  if k = 1 then if issqr(n) then return sqrt(n) else return -infinity fi fi;",
				"  max(0,seq(a*procname(n-a^2, k-1), a=1..floor(sqrt(n))))",
				"end proc:",
				"seq(g(n, 4), n=0..100); # _Robert Israel_, Apr 15 2019"
			],
			"mathematica": [
				"Array[Max[Times @@ # \u0026 /@ PowersRepresentations[#, 4, 2]] \u0026, 68, 0] (* _Michael De Vlieger_, Apr 13 2019 *)"
			],
			"program": [
				"(C) See Links section."
			],
			"xref": [
				"See A307531 for the additive variant.",
				"Cf. A000534, A002635, A122922."
			],
			"keyword": "nonn,look",
			"offset": "0,8",
			"author": "_Rémy Sigrist_, Apr 11 2019",
			"references": 2,
			"revision": 19,
			"time": "2019-04-15T16:27:01-04:00",
			"created": "2019-04-13T22:10:50-04:00"
		}
	]
}