{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A333420",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 333420,
			"data": "1,2,3,6,25,6,24,343,110,10,120,6561,3375,324,15,720,161051,144400,17576,756,21,5040,4826809,7962624,1336336,64000,1521,28,40320,170859375,535387328,130691232,7595536,185193,2756,36,3628800,6975757441",
			"name": "Table T(n,k) read by upward antidiagonals. T(n,k) is the maximum value of Product_{i=1..n} Sum_{j=1..k} r[(i-1)*k+j] among all permutations r of {1..kn}.",
			"comment": [
				"A dual sequence to A331889.",
				"   k         1          2         3         4       5        6         7       8       9",
				"  --------------------------------------------------------------------------------------",
				"n  1|        1          3         6        10      15       21        28      36      45",
				"   2|        2         25       110       324     756     1521      2756    4624    7310",
				"   3|        6        343      3375     17576   64000   185193    456533 1000000 2000376",
				"   4|       24       6561    144400   1336336 7595536 31640625 106131204",
				"   5|      120     161051   7962624 130691232",
				"   6|      720    4826809 535387328",
				"   7|     5040  170859375",
				"   8|    40320 6975757441",
				"   9|  3628800",
				"  10| 39916800"
			],
			"link": [
				"Chai Wah Wu, \u003ca href=\"https://arxiv.org/abs/2002.10514\"\u003eOn rearrangement inequalities for multiple sequences\u003c/a\u003e, arXiv:2002.10514 [math.CO], 2020."
			],
			"formula": [
				"T(n,k) \u003c= floor((k*(k*n+1)/2)^n) with equality if k = 2*t+n*u for nonnegative integers t and u.",
				"T(n,1) = n! = A000142(n).",
				"T(1,k) = k*(k+1)/2 = A000217(k).",
				"T(n,2) = (2*n+1)^n = A085527(n).",
				"If n is even, k is odd and k \u003e= n-1, then T(n,k) = ((k^2*(k*n+1)^2-1)/4)^(n/2)."
			],
			"program": [
				"(Python)",
				"from itertools import combinations, permutations",
				"from sympy import factorial",
				"def T(n,k): # T(n,k) for A333420",
				"    if k == 1:",
				"        return int(factorial(n))",
				"    if n == 1:",
				"        return k*(k+1)//2",
				"    if k % 2 == 0 or (k \u003e= n-1 and n % 2 == 1):",
				"        return (k*(k*n+1)//2)**n",
				"    if k \u003e= n-1 and n % 2 == 0 and k % 2 == 1:",
				"        return ((k**2*(k*n+1)**2-1)//4)**(n//2)",
				"    nk = n*k",
				"    nktuple = tuple(range(1,nk+1))",
				"    nkset = set(nktuple)",
				"    count = 0",
				"    for firsttuple in combinations(nktuple,n):",
				"        nexttupleset = nkset-set(firsttuple)",
				"        for s in permutations(sorted(nexttupleset),nk-2*n):",
				"            llist = sorted(nexttupleset-set(s),reverse=True)",
				"            t = list(firsttuple)",
				"            for i in range(0,k-2):",
				"                itn = i*n",
				"                for j in range(n):",
				"                        t[j] += s[itn+j]",
				"            t.sort()",
				"            w = 1",
				"            for i in range(n):",
				"                w *= llist[i]+t[i]",
				"            if w \u003e count:",
				"                count = w",
				"    return count"
			],
			"xref": [
				"Cf. A000142, A000217, A085527, A331889."
			],
			"keyword": "nonn,more,tabl",
			"offset": "1,2",
			"author": "_Chai Wah Wu_, Mar 23 2020",
			"references": 2,
			"revision": 20,
			"time": "2020-03-31T10:40:40-04:00",
			"created": "2020-03-31T01:45:11-04:00"
		}
	]
}