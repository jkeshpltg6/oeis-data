{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A292686",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 292686,
			"data": "1,101,101000101,101000101000000000101000101,101000101000000000101000101000000000000000000000000000101000101000000000101000101",
			"name": "Sierpinski-type iteration: start with a(0)=1, at each step, replace 0 with 000 and 1 with 101.",
			"comment": [
				"See A292687 for the decimal representation of a(n) viewed as a \"binary number\", i.e., as written in base 2.",
				"The Sierpinski carpet (A153490) can be seen as 2-dimensional version of this 1-dimensional variant. The classical Sierpinski gasket triangle (Pascal's triangle mod 2) and \"Rule 18\" (or Rule 90, A070886) and \"Rule 22\" (A071029) have similar graphs.",
				"The n-th term a(n) has 3^n digits, the middle third of which are all zero. The digits of a(n) are again the first and last 3^n digits of a(n+1), separated by 3^n zeros."
			],
			"link": [
				"Michael Coons and James Evans, \u003ca href=\"https://arxiv.org/abs/2011.10722\"\u003eA sequential view of self--similar measures, or, What the ghosts of Mahler and Cantor can teach us about dimension\u003c/a\u003e, arXiv:2011.10722 [math.NT], 2020. See Figure 2 p. 2."
			],
			"formula": [
				"a(n+1) = convert(5*a(n), from base 8, to base 2).",
				"a(n+1) = (100^(3^n)+1)*a(n).",
				"a(n) = Product_{k=0 .. n-1} (100^(3^k)+1)."
			],
			"example": [
				"a(0) = 1 -\u003e 101 = a(1);",
				"a(1) = 101 -\u003e concat(101,000,101) = 101000101 = a(2)."
			],
			"program": [
				"(PARI) a(n,a=1)=for(k=1,n,a=fromdigits(binary(a)*5,8));fromdigits(binary(a),10) \\\\ Illustration of the first formula.",
				"(PARI) A292686(n)=prod(k=0,n-1,100^(3^k)+1)"
			],
			"xref": [
				"Cf. A292687 for the decimal representation of a(n) viewed as a \"binary number\", i.e., as written in base 2.",
				"Cf. A153490 (Sierpinski carpet), A047999 (Sierpinski gasket = Pascal's triangle mod 2), A070886 (Rule 18 / Rule 90), A071029 (Rule 22).",
				"Cf. A088917."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_M. F. Hasler_, Oct 20 2017",
			"references": 3,
			"revision": 21,
			"time": "2020-11-24T06:34:13-05:00",
			"created": "2017-10-21T21:31:55-04:00"
		}
	]
}