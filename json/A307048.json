{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A307048",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 307048,
			"data": "2,1,6,5,10,4,14,7,18,13,22,8,26,3,30,21,34,12,38,9,42,29,46,16,50,23,54,37,58,20,62,19,66,45,70,24,74,17,78,53,82,28,86,39,90,61,94,32,98,15,102,69,106,36,110,25,114,77,118,40,122,55",
			"name": "Permutation of the positive integers derived from the terms of A322469 having the form 6*k - 2.",
			"comment": [
				"The sequence is the flattened form of an irregular table U(i, j) similar to table T(i, j) in A322469. U(i, j) = k is defined only for the elements T(i, j) which have the form 6*k - 2, so the table is sparsely filled.",
				"Like in A322469, the columns in table U contain arithmetic progressions.",
				"a(n) is a permutation of the positive integers, since A322469 is one, and since there is a one-to-one mapping between any a(n) = k and some A322469(m) = 6*k - 2.",
				"There is a hierarchy of such permutations of the positive integers derived by mapping the terms of the form 6*k - 2 to k:",
				"  Level 1: A322469",
				"  Level 2: A307048 (this sequence)",
				"  Level 3: A160016 = 2, 1, 4, 6, 8, 3, ... period of (3 even, 1 odd number)",
				"  Level 4: A000027 = 1, 2, 3, 4 ... (the positive integers)",
				"  Level 5: A000027"
			],
			"example": [
				"Table U(i, j) begins:",
				"   i\\j   1  2  3  4  5  6  7",
				"   -------------------------",
				"   1:",
				"   4:          2",
				"   7:                   1",
				"  10:",
				"  13:          6",
				"  16:                5",
				"  19:",
				"  22:         10",
				"  25:             4",
				"  28:",
				"  31:         14",
				"-----",
				"T(4, 3) = 10 = 6*2 - 2, therefore U(4, 3) = 2.",
				"T(7, 6) =  4 = 6*1 - 2, therefore U(7, 6) = 1."
			],
			"program": [
				"(Perl 5) # derived from A322469",
				"  use integer; my $n = 1; my $i = 1; my $an;",
				"  while ($i \u003c= 1000) { # next row",
				"    $an = 4 * $i - 1; \u0026term();",
				"    while ($an % 3 == 0) {",
				"      $an /= 3; \u0026term();",
				"      $an *= 2; \u0026term();",
				"    } # while divisible by 3",
				"    $i ++;",
				"  } # while next row",
				"  sub term {",
				"    if (($an + 2) % 6 == 0) {",
				"      my $bn = ($an + 2) / 6;",
				"      print \"$n $bn\\n\"; $n ++;",
				"    }",
				"  }"
			],
			"xref": [
				"Cf. A000027, A160016, A322469."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_Georg Fischer_, Mar 21 2019",
			"references": 7,
			"revision": 9,
			"time": "2019-03-25T06:29:58-04:00",
			"created": "2019-03-25T06:29:58-04:00"
		}
	]
}