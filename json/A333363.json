{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A333363",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 333363,
			"data": "3,2,5,3,2,7,3,2,5,3,2,9,3,2,5,3,2,7,3,2,5,3,2,11,3,2,5,3,2,7,3,2,5,3,2,9,3,2,5,3,2,7,3,2,5,3,2,13,3,2,5,3,2,7,3,2,5,3,2,9,3,2,5,3,2,7,3,2,5,3,2,11,3,2,5,3,2,7,3,2,5,3,2,9,3,2,5,3,2,7,3,2,5,3,2,15",
			"name": "Horizontal visibility sequence at the onset of chaos in the 3-period cascade.",
			"comment": [
				"This sequence represents the horizontal visibility of the points of the chaotic time series at the onset of chaos in the 3-period cascade of the logistic (unimodal) map.",
				"Observation: if the sequence is written as a square array with six columns read by rows we have that, at least for the first 16 rows, the n-th row is \"3, 2, 5, 3, 2\" together with (6 + A037227(n)), see the example. - _Omar E. Pol_, Mar 16 2020"
			],
			"link": [
				"Juan C. Nuño and Francisco J. Muñoz. \u003ca href=\"https://arxiv.org/abs/2002.10423\"\u003eUniversal visibility patterns of unimodal maps\u003c/a\u003e, arXiv:2002.10423 [nlin.CD], 2020.",
				"Juan C. Nuño and Francisco J. Muñoz, \u003ca href=\"https://doi.org/10.1063/5.0006652\"\u003eUniversal visibility patterns of unimodal maps\u003c/a\u003e, Chaos, 30, 063105 (2020).",
				"Juan Carlos Nuño and Francisco J. Muñoz, \u003ca href=\"https://arxiv.org/abs/2009.14629\"\u003eOn the ubiquity of the ruler sequence\u003c/a\u003e, arXiv:2009.14629 [math.HO], 2020."
			],
			"formula": [
				"Conjectured: a(n) = 2*A007814(n/3) + 5 if 3|n and a(n) = 4 - (n mod 3) otherwise. - _Giovanni Resta_, Mar 16 2020"
			],
			"example": [
				"From _Omar E. Pol_, Mar 16 2020: (Start)",
				"Written as a square array with six columns read by rows the sequence begins:",
				"3, 2, 5, 3, 2,  7;",
				"3, 2, 5, 3, 2,  9;",
				"3, 2, 5, 3, 2,  7;",
				"3, 2, 5, 3, 2, 11;",
				"3, 2, 5, 3, 2,  7;",
				"3, 2, 5, 3, 2,  9;",
				"3, 2, 5, 3, 2,  7;",
				"3, 2, 5, 3, 2, 13;",
				"3, 2, 5, 3, 2,  7;",
				"3, 2, 5, 3, 2,  9;",
				"3, 2, 5, 3, 2,  7;",
				"3, 2, 5, 3, 2, 11;",
				"3, 2, 5, 3, 2,  7;",
				"3, 2, 5, 3, 2,  9;",
				"3, 2, 5, 3, 2,  7;",
				"3, 2, 5, 3, 2, 15;",
				"(End)"
			],
			"mathematica": [
				"L[n_] := L[n] = Block[{s = {3, 2, 2*n+3}}, Do[s = Join[L[i], s], {i, n-1}]; s]; L[6] (* _Giovanni Resta_, Mar 16 2020 *)"
			],
			"program": [
				"( R )",
				"visibsuc3 \u003c- function(n){",
				"    suc \u003c- c(3,2, 2*(n+1)+1)",
				"    if(n\u003e1){",
				"    for(i in 1:(n-1)){",
				"    suc \u003c- c(visibsuc3(i), suc)",
				"    }",
				"   }",
				"   return(suc)",
				"  }"
			],
			"xref": [
				"Cf. A007814, A037227."
			],
			"keyword": "nonn,tabf",
			"offset": "1,1",
			"author": "_Francisco J. Muñoz_ and _Juan Carlos Nuño_, Mar 16 2020",
			"references": 0,
			"revision": 33,
			"time": "2020-10-02T09:03:52-04:00",
			"created": "2020-05-05T21:49:59-04:00"
		}
	]
}