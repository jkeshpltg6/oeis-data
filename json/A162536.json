{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A162536",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 162536,
			"data": "1,2,21,4,5,6,21,16,81,10,341,12,1365,42,285,16,85,18,87381,20,21,22,1398101,24,125,26,81,84,89478485,90,341,256,1815,102,1365,36,22906492245,38,117,80,349525,42,5461,44,4545,598,23456248059221,48,1029,50,1479,52",
			"name": "a(n) is the smallest positive multiple k of n such that every length of the runs of 0's and 1's in the binary representation of k divides n.",
			"comment": [
				"By \"run\" of 0's or 1's, it is meant: Think of binary k as a string of 0's and 1's. A single run of the digit b (0 or 1) is made up completely of consecutive digits all equal to b, and is bounded on its ends by either the digit 1-b or the end of the string."
			],
			"link": [
				"Giovanni Resta, \u003ca href=\"/A162536/b162536.txt\"\u003eTable of n, a(n) for n = 1..400\u003c/a\u003e"
			],
			"example": [
				"For n = 9, we check: 9 in binary is 1001, which has a run of two 0's, and 2 does not divide 9. Checking further: 2*9 = 18 = 10010, which still doesn't work. 3*9 = 27 = 11011 in binary, which has two runs of two 1's. 4*9 = 36 = 100100 in binary, 5*9 = 45 = 101101 in binary, 6*9 = 54 = 110110 in binary, 7*9 = 63 = 111111 in binary, 8*9 = 72 = 1001000 in binary, none of which work. But 9*9 = 81 = 1010001 in binary, which has three runs of one 1 each, a run of one 0, and a run of three 0's. Since 9 is divisible by both of these lengths (1 and 3), a(9) = 81."
			],
			"mathematica": [
				"a[n_] := Block[{m}, If[n\u003e2 \u0026\u0026 PrimeQ[n], m=1; While[Mod[m,n] \u003e 0, m=4*m+1], m=n; While[! AllTrue[ Union[ Length /@ Split[ IntegerDigits[m, 2]]], Mod[n, #] == 0 \u0026], m += n]]; m]; Array[a, 60] (* _Giovanni Resta_, Aug 11 2019 *)"
			],
			"xref": [
				"Cf. A162534, A162537."
			],
			"keyword": "nonn,base",
			"offset": "1,2",
			"author": "_Leroy Quet_, Jul 05 2009",
			"ext": [
				"More terms from _Sean A. Irvine_, Jan 26 2011",
				"More terms from _Giovanni Resta_, Aug 11 2019"
			],
			"references": 3,
			"revision": 20,
			"time": "2021-01-31T20:28:09-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}