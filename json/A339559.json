{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A339559",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 339559,
			"data": "0,0,1,0,2,1,4,3,7,6,14,14,23,27,41,47,70,84,114,141,190,225,303,370,475,578,738,890,1131,1368,1698,2058,2549,3048,3759,4505,5495,6574,7966,9483,11450",
			"name": "Number of integer partitions of n that have an even number of parts and cannot be partitioned into distinct pairs of distinct parts, i.e., that are not the multiset union of any set of edges.",
			"comment": [
				"The multiplicities of such a partition form a non-graphical partition."
			],
			"link": [
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/GraphicalPartition.html\"\u003eGraphical partition.\u003c/a\u003e"
			],
			"example": [
				"The a(2) = 1 through a(10) = 14 partitions (empty column indicated by dot):",
				"  11   .   22     2111   33       2221     44         3222       55",
				"           1111          2211     4111     2222       6111       3322",
				"                         3111     211111   3311       222111     3331",
				"                         111111            5111       321111     4222",
				"                                           221111     411111     4411",
				"                                           311111     21111111   7111",
				"                                           11111111              222211",
				"                                                                 322111",
				"                                                                 331111",
				"                                                                 421111",
				"                                                                 511111",
				"                                                                 22111111",
				"                                                                 31111111",
				"                                                                 1111111111",
				"For example, the partition y = (4,4,3,3,2,2,1,1,1,1) can be partitioned into a multiset of edges in just three ways:",
				"  {{1,2},{1,2},{1,3},{1,4},{3,4}}",
				"  {{1,2},{1,3},{1,3},{1,4},{2,4}}",
				"  {{1,2},{1,3},{1,4},{1,4},{2,3}}",
				"None of these are strict, so y is counted under a(22)."
			],
			"mathematica": [
				"strs[n_]:=If[n\u003c=1,{{}},Join@@Table[Map[Prepend[#,d]\u0026,Select[strs[n/d],Min@@#\u003ed\u0026]],{d,Select[Rest[Divisors[n]],And[SquareFreeQ[#],PrimeOmega[#]==2]\u0026]}]];",
				"Table[Length[Select[IntegerPartitions[n],EvenQ[Length[#]]\u0026\u0026strs[Times@@Prime/@#]=={}\u0026]],{n,0,15}]"
			],
			"xref": [
				"A320894 ranks these partitions (using Heinz numbers).",
				"A338915 allows equal pairs (x,x).",
				"A339560 counts the complement in even-length partitions.",
				"A339564 counts factorizations of the same type.",
				"A000070 counts non-multigraphical partitions of 2n, ranked by A339620.",
				"A000569 counts graphical partitions, ranked by A320922.",
				"A001358 lists semiprimes, with squarefree case A006881.",
				"A002100 counts partitions into squarefree semiprimes.",
				"A058696 counts partitions of even numbers, ranked by A300061.",
				"A209816 counts multigraphical partitions, ranked by A320924.",
				"A320655 counts factorizations into semiprimes.",
				"A320656 counts factorizations into squarefree semiprimes.",
				"A339617 counts non-graphical partitions of 2n, ranked by A339618.",
				"A339655 counts non-loop-graphical partitions of 2n, ranked by A339657.",
				"The following count partitions of even length and give their Heinz numbers:",
				"- A027187 has no additional conditions (A028260).",
				"- A096373 cannot be partitioned into strict pairs (A320891).",
				"- A338914 can be partitioned into strict pairs (A320911).",
				"- A338915 cannot be partitioned into distinct pairs (A320892).",
				"- A338916 can be partitioned into distinct pairs (A320912).",
				"- A339560 can be partitioned into distinct strict pairs (A339561).",
				"Cf. A001055, A001221, A005117, A007717, A025065, A030229, A089259, A292432, A320893, A338899, A338903, A339619."
			],
			"keyword": "nonn,more",
			"offset": "0,5",
			"author": "_Gus Wiseman_, Dec 10 2020",
			"references": 19,
			"revision": 12,
			"time": "2020-12-18T07:58:49-05:00",
			"created": "2020-12-18T07:58:49-05:00"
		}
	]
}