{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A113430",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 113430,
			"data": "1,-1,0,-1,0,0,0,1,1,0,0,0,0,0,-1,0,0,0,0,0,-1,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,-1,0,0,0,0,0,0,0,0,0,-1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0",
			"name": "Expansion of f(-x, -x^2) * f(-x^10, -x^20) / f(-x^2, -x^8) in powers of x where f(, ) is Ramanujan's general theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"This is an example of the quintuple product identity in the form f(a*b^4, a^2/b) - (a/b) * f(a^4*b, b^2/a) = f(-a*b, -a^2*b^2) * f(-a/b, -b^2) / f(a, b) where a = x^4, b = x."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A113430/b113430.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/QuintupleProductIdentity.html\"\u003eQuintuple Product Identity\u003c/a\u003e"
			],
			"formula": [
				"Expansion of f(x^7, x^8) - x * f(x^2, x^13) in power of x.",
				"Expansion of G(x^2) * f(-x) where G() is the g.f. of A003114.",
				"Euler transform of period 10 sequence [ -1, 0, -1, -1, -1, -1, -1, 0, -1, -1, ...].",
				"|a(n)| is the characteristic function of the numbers in A093722.",
				"The exponents in the q-series q * A(q^120) are the square of the numbers in A057538.",
				"G.f.: Prod_{k\u003e0} (1 - x^k) / ((1 - x^(10*k - 2)) * (1 - x^(10*k - 8))) = Sum_{k in Z} x^((15*k^2 + k) / 2) - x^((15*k^2 - 11*k + 2) / 2)."
			],
			"example": [
				"G.f. = 1 - x - x^3 + x^7 + x^8 - x^14 - x^20 + x^29 + x^31 - x^42 - x^52 + ...",
				"G.f. = q - q^121 - q^361 + q^841 + q^961 - q^1681 - q^2401 + q^3481 + q^3721 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ x] / (QPochhammer[ x^2, x^10] QPochhammer[ x^8, x^10]), {x, 0, n}]; (* _Michael Somos_, Jan 06 2016 *)"
			],
			"program": [
				"(PARI) {a(n) = my(m); if( n\u003c0 || !issquare( n*120 + 1, \u0026m) || 1!=gcd(m, 30), 0, (-1)^(m%30\\10))};",
				"(PARI)  {a(n) = if( n\u003c0, 0, polcoeff( prod( k=1, n, 1 - x^k * [1, 1, 0, 1, 1, 1, 1, 1, 0, 1][k%10 + 1], 1 + x * O(x^n)), n))};"
			],
			"xref": [
				"Cf. A003114, A010815, A057538, A093722."
			],
			"keyword": "sign",
			"offset": "0,1",
			"author": "_Michael Somos_, Oct 31 2005",
			"references": 4,
			"revision": 19,
			"time": "2021-03-12T22:24:43-05:00",
			"created": "2006-01-24T03:00:00-05:00"
		}
	]
}