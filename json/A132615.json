{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A132615",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 132615,
			"data": "1,1,1,1,1,1,6,3,1,1,80,25,5,1,1,1666,378,56,7,1,1,47232,8460,1020,99,9,1,1,1694704,252087,26015,2134,154,11,1,1,73552752,9392890,855478,61919,3848,221,13,1,1,3744491970,420142350,34461260,2257413,125760,6290,300,15,1,1",
			"name": "Triangle T, read by rows, where row n+1 of T = row n of T^(2n-1) with appended '1' for n\u003e=0 with T(0,0)=1.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A132615/b132615.txt\"\u003eRows n = 0..140, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n+1,1) is divisible by 2n-1 for n\u003e=1."
			],
			"example": [
				"Triangle begins:",
				"  1;",
				"  1, 1;",
				"  1, 1, 1;",
				"  6, 3, 1, 1;",
				"  80, 25, 5, 1, 1;",
				"  1666, 378, 56, 7, 1, 1;",
				"  47232, 8460, 1020, 99, 9, 1, 1;",
				"  1694704, 252087, 26015, 2134, 154, 11, 1, 1;",
				"  73552752, 9392890, 855478, 61919, 3848, 221, 13, 1, 1; ...",
				"GENERATE T FROM ODD MATRIX POWERS OF T.",
				"Matrix cube, T^3, begins:",
				"  1;",
				"  3, 1;",
				"  6, 3, 1; \u003c-- row 3 of T",
				"  31, 12, 3, 1;",
				"  357, 100, 18, 3, 1;",
				"  6786, 1455, 205, 24, 3, 1; ...",
				"where row 3 of T = row 2 of T^3 with appended '1'.",
				"Matrix fifth power, T^5, begins:",
				"  1;",
				"  5, 1;",
				"  15, 5, 1;",
				"  80, 25, 5, 1; \u003c-- row 4 of T",
				"  855, 215, 35, 5, 1;",
				"  15171, 3065, 410, 45, 5, 1; ...",
				"where row 4 of T = row 3 of T^5 with appended '1'.",
				"Matrix seventh power, T^7, begins:",
				"  1;",
				"  7, 1;",
				"  28, 7, 1;",
				"  161, 42, 7, 1;",
				"  1666, 378, 56, 7, 1; \u003c-- row 5 of T",
				"  28119, 5348, 679, 70, 7, 1; ...",
				"where row 5 of T = row 4 of T^7 with appended '1'.",
				"ALTERNATE GENERATING METHOD.",
				"Row 4: start with a '1' followed by 4 zeros;",
				"take partial sums and append 2 zeros; then",
				"take partial sums thrice more:",
				"  (1), 0, 0, 0, 0;",
				"  1, 1, 1, 1, (1), 0, 0;",
				"  1, 2, 3, 4, 5, 5, (5);",
				"  1, 3, 6, 10, 15, 20, (25);",
				"  1, 4, 10, 20, 35, 55, (80);",
				"the final nonzero terms form row 4: [80, 25, 5, 1, 1].",
				"Row 5: start with a '1' followed by 6 zeros;",
				"take partial sums and append 4 zeros;",
				"take partial sums and append 2 zeros; then",
				"take partial sums thrice more:",
				"  (1), 0, 0, 0, 0, 0, 0;",
				"  1, 1, 1, 1, 1, 1, (1), 0, 0, 0, 0;",
				"  1, 2, 3, 4, 5, 6, 7, 7, 7, 7, (7), 0, 0;",
				"  1, 3, 6, 10, 15, 21, 28, 35, 42, 49, 56, 56, (56);",
				"  1, 4, 10, 20, 35, 56, 84, 119, 161, 210, 266, 322, (378);",
				"  1, 5, 15, 35, 70, 126, 210, 329, 490, 700, 966, 1288, (1666);",
				"the final nonzero terms form row 5: [1666, 378, 56, 7, 1, 1].",
				"Continuing in this way produces all the rows of this triangle."
			],
			"maple": [
				"b:= proc(n) option remember;",
				"      Matrix(n, (i,j)-\u003e T(i-1,j-1))^(2*n-3)",
				"    end:",
				"T:= proc(n,k) option remember;",
				"     `if`(n=k, 1, `if`(k\u003en, 0, b(n)[n,k+1]))",
				"    end:",
				"seq(seq(T(n,k), k=0..n), n=0..10);  # _Alois P. Heinz_, Apr 13 2020"
			],
			"mathematica": [
				"b[n_] := b[n] = MatrixPower[Table[T[i-1, j-1], {i, n}, {j, n}], 2n-3];",
				"T[n_, k_] := T[n, k] = If[n == k, 1, If[k \u003e n, 0, b[n][[n, k + 1]]]];",
				"Table[T[n, k], {n, 0, 10}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Apr 27 2020, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(PARI) {T(n, k)=local(A=vector(n+1), p); A[1]=1; for(j=1, n-k-1, p=(n-1)*(n-2)-(n-j-1)*(n-j-2); A=Vec((Polrev(A)+x*O(x^p))/(1-x))); A=Vec((Polrev(A)+x*O(x^p))/(1-x)); A[p+1]}"
			],
			"xref": [
				"Cf. columns: A132616, A132617, A132618; A132619; variants: A132610, A101479.",
				"Cf. A304190, A304191, A304193."
			],
			"keyword": "nonn,tabl",
			"offset": "0,7",
			"author": "_Paul D. Hanna_, Aug 24 2007",
			"references": 11,
			"revision": 16,
			"time": "2020-04-27T06:21:00-04:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}