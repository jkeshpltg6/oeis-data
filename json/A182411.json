{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A182411",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 182411,
			"data": "1,2,2,6,4,6,20,10,12,20,70,28,28,40,70,252,84,72,90,140,252,924,264,198,220,308,504,924,3432,858,572,572,728,1092,1848,3432,12870,2860,1716,1560,1820,2520,3960,6864,12870,48620,9724,5304,4420,4760,6120,8976",
			"name": "Triangle T(n,k) = (2*k)!*(2*n)!/(k!*n!*(k+n)!) with k=0..n, read by rows.",
			"comment": [
				"This is a companion to the triangle A068555.",
				"Row sum is 2*A132310(n-1) + A000984(n) for n\u003e0, where A000984(n) = T(n,0) = T(n,n). Also:",
				"T(n,1)  = -A002420(n+1).",
				"T(n,2)  =  A002421(n+2).",
				"T(n,3)  = -A002422(n+3) = 2*A007272(n).",
				"T(n,4)  =  A002423(n+4).",
				"T(n,5)  = -A002424(n+5).",
				"T(n,6)  =  A020923(n+6).",
				"T(n,7)  = -A020925(n+7).",
				"T(n,8)  =  A020927(n+8).",
				"T(n,9)  = -A020929(n+9).",
				"T(n,10) =  A020931(n+10).",
				"T(n,11) = -A020933(n+11)."
			],
			"reference": [
				"Umberto Scarpis, Sui numeri primi e sui problemi dell'analisi indeterminata in Questioni riguardanti le matematiche elementari, Nicola Zanichelli Editore (1924-1927, third edition), page 11.",
				"J. V. Uspensky and M. A. Heaslet, Elementary Number Theory, McGraw-Hill, NY, 1939, p. 103."
			],
			"link": [
				"Alexander Borisov, \u003ca href=\"https://arxiv.org/abs/math/0505167\"\u003eQuotient singularities, integer ratios of factorials and the Riemann Hypothesis\u003c/a\u003e, arXiv:math/0505167 [math.NT], 2005; International Mathematics Research Notices, Vol. 2008, Article ID rnn052, page 2 (Theorem 2).",
				"Ira Gessel, \u003ca href=\"http://people.brandeis.edu/~gessel/homepage/slides/int-quot.pdf\"\u003eInteger quotients of factorials and algebraic multivariable hypergeometric series\u003c/a\u003e, MIT Combinatorics Seminar, September 2011 (slides).",
				"Kevin Limanta and Norman Wildberger, \u003ca href=\"https://arxiv.org/abs/2108.10191\"\u003eSuper Catalan Numbers, Chromogeometry, and Fourier Summation over Finite Fields\u003c/a\u003e, arXiv:2108.10191 [math.CO], 2021. See Table 1 p. 2 where terms are shown as an array."
			],
			"example": [
				"Triangle begins:",
				"      1;",
				"      2,    2;",
				"      6,    4,    6;",
				"     20,   10,   12,   20;",
				"     70,   28,   28,   40,   70;",
				"    252,   84,   72,   90,  140,  252;",
				"    924,  264,  198,  220,  308,  504,  924;",
				"   3432,  858,  572,  572,  728, 1092, 1848,  3432;",
				"  12870, 2860, 1716, 1560, 1820, 2520, 3960,  6864, 12870;",
				"  48620, 9724, 5304, 4420, 4760, 6120, 8976, 14586, 25740, 48620;",
				"  ...",
				"Sum_{k=0..8} T(8,k) = 12870 + 2860 + 1716 + 1560 + 1820 + 2520 + 3960 + 6864 + 12870 = 2*A132310(7) + A000984(8) = 2*17085 + 12870 = 47040."
			],
			"mathematica": [
				"Flatten[Table[Table[(2 k)! ((2 n)!/(k! n! (k + n)!)), {k, 0, n}], {n, 0, 9}]]"
			],
			"program": [
				"(MAGMA)",
				"[Factorial(2*k)*Factorial(2*n)/(Factorial(k)*Factorial(n)*Factorial(k+n)): k in [0..n], n in [0..9]];"
			],
			"xref": [
				"Cf. A000984, A002420-A020933, A068555, A132310."
			],
			"keyword": "nonn,tabl,look,easy",
			"offset": "0,2",
			"author": "_Bruno Berselli_, Apr 27 2012",
			"references": 2,
			"revision": 38,
			"time": "2021-08-24T06:50:23-04:00",
			"created": "2012-05-01T16:19:12-04:00"
		}
	]
}