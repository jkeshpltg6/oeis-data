{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A298932",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 298932,
			"data": "1,1,2,0,3,2,4,0,4,4,6,0,5,3,6,0,6,4,4,0,8,4,6,0,9,6,6,0,6,6,12,0,8,4,12,0,8,7,8,0,9,6,8,0,12,8,6,0,8,6,14,0,12,6,12,0,8,8,12,0,13,6,12,0,18,10,8,0,8,12,12,0,16,7,14,0,12,8,12,0,16",
			"name": "Expansion of f(-x^3)^3 * phi(-x^12) / (f(-x) * chi(-x^4)) in powers of x where phi(), chi(), f() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A298932/b298932.txt\"\u003eTable of n, a(n) for n = 0..2500\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(-1/2) * eta(q^3)^3 * eta(q^8) * eta(q^12)^2 / (eta(q) * eta(q^4) * eta(q^24)) in powers of q.",
				"Euler transform of period 24 sequence [1, 1, -2, 2, 1, -2, 1, 1, -2, 1, 1, -3, 1, 1, -2, 1, 1, -2, 1, 2, -2, 1, 1, -3, ...].",
				"a(4*n + 3) = 0. a(3*n + 2) = 2 * A213607(n). a(n) = A298931(3*n). a(2*n) = A298933(n)."
			],
			"example": [
				"G.f. = 1 + x + 2*x^2 + 3*x^4 + 2*x^5 + 4*x^6 + 4*x^8 + 4*x^9 + ...",
				"G.f. = q + q^3 + 2*q^5 + 3*q^9 + 2*q^11 + 4*q^13 + 4*q^17 + 4*q^19 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ x^3]^3 QPochhammer[ -x^4, x^4] EllipticTheta[ 4, 0, x^12] / QPochhammer[ x], {x, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^3 + A)^3 * eta(x^8 + A) * eta(x^12 + A)^2 / (eta(x + A) * eta(x^4 + A) * eta(x^24 + A)), n))};"
			],
			"xref": [
				"Cf. A213607, A298931, A298933."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Michael Somos_, Jan 29 2018",
			"references": 3,
			"revision": 9,
			"time": "2021-03-12T22:24:48-05:00",
			"created": "2018-01-29T15:23:52-05:00"
		}
	]
}