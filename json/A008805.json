{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A008805",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 8805,
			"data": "1,1,3,3,6,6,10,10,15,15,21,21,28,28,36,36,45,45,55,55,66,66,78,78,91,91,105,105,120,120,136,136,153,153,171,171,190,190,210,210,231,231,253,253,276,276,300,300,325,325,351,351,378,378,406,406,435,435",
			"name": "Triangular numbers repeated.",
			"comment": [
				"Number of choices for nonnegative integers x,y,z such that x and y are even and x + y + z = n.",
				"Diagonal sums of A002260, when arranged as a number triangle. - _Paul Barry_, Feb 28 2003",
				"a(n) = number of partitions of n+4 such that the differences between greatest and smallest parts are 2: a(n-4) = A097364(n,2) for n\u003e3. - _Reinhard Zumkeller_, Aug 09 2004",
				"For n \u003e= i, i=4,5, a(n-i) is the number of incongruent two-color bracelets of n beads, i from them are black (cf. A005232, A032279), having a diameter of symmetry. - _Vladimir Shevelev_, May 03 2011",
				"Prefixing A008805 by 0,0,0,0 gives the sequence c(0), c(1), ... defined by c(n)=number of (w,x,y) such that w = 2x+2y, where w,x,y are all in {1,...,n}; see A211422. - _Clark Kimberling_, Apr 15 2012",
				"Partial sums of positive terms of A142150. - _Reinhard Zumkeller_, Jul 07 2012",
				"The sum of the first parts of the nondecreasing partitions of n+2 into exactly two parts, n \u003e= 0. - _Wesley Ivan Hurt_, Jun 08 2013",
				"Number of the distinct symmetric pentagons in a regular n-gon, see illustration for some small n in links. - _Kival Ngaokrajang_, Jun 25 2013",
				"a(n) is the number of nonnegative integer solutions to the equation x + y + z = n such that x + y \u003c= z. For example, a(4) = 6 because we have 0+0+4 = 0+1+3 = 0+2+2 = 1+0+3 = 1+1+2 = 2+0+2. - _Geoffrey Critzer_, Jul 09 2013",
				"a(n) is the number of distinct opening moves in n X n tic-tac-toe. - _I. J. Kennedy_, Sep 04 2013",
				"a(n) is the number of symmetry-allowed, linearly-independent terms at n-th order in the series expansion of the T2 X t2 vibronic perturbation matrix, H(Q) (cf. Opalka \u0026 Domcke). - _Bradley Klee_, Jul 20 2015",
				"a(n-1) also gives the number of D_4 (dihedral group of order 4) orbits of an n X n square grid with squares coming in either of two colors and only one square has one of the colors. - _Wolfdieter Lang_, Oct 03 2016",
				"Also, this sequence is the third column in the triangle of the coefficients of the sum of two consecutive Fibonacci polynomials F(n+1, x) and F(n, x) (n\u003e=0) in ascending powers of x. - _Mohammad K. Azarian_, Jul 18 2018"
			],
			"reference": [
				"H. D. Brunk, An Introduction to Mathematical Statistics, Ginn, Boston, 1960; p. 360."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A008805/b008805.txt\"\u003eTable of n, a(n) for n = 0..3000\u003c/a\u003e",
				"G. E. Andrews, M. Beck, N. Robbins, \u003ca href=\"http://arxiv.org/abs/1406.3374\"\u003ePartitions with fixed differences between largest and smallest parts\u003c/a\u003e, arXiv preprint arXiv:1406.3374 [math.NT], 2014.",
				"P. Flajolet and R. Sedgewick, \u003ca href=\"http://algo.inria.fr/flajolet/Publications/books.html\"\u003eAnalytic Combinatorics\u003c/a\u003e, 2009; see page 46.",
				"Kival Ngaokrajang, \u003ca href=\"/A008805/a008805.jpg\"\u003eThe distinct symmetric 5-gons in a regular n-gon for n = 6..13\u003c/a\u003e",
				"D. Opalka and W. Domcke, \u003ca href=\"http://dx.doi.org/10.1063/1.3382912\"\u003eHigh-order expansion of T2xt2 Jahn-Teller potential energy surfaces in tetrahedral molecules\u003c/a\u003e, J. Chem. Phys., 132, 154108 (2010).",
				"V. Shevelev, \u003ca href=\"https://arxiv.org/abs/0710.1370\"\u003eA problem of enumeration of two-color bracelets with several variations\u003c/a\u003e, arXiv:0710.1370 [math.CO], 2007-2011.",
				"\u003ca href=\"/index/Tu#2wis\"\u003eIndex entries for two-way infinite sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,2,-2,-1,1).",
				"\u003ca href=\"/index/Mo#Molien\"\u003eIndex entries for Molien series\u003c/a\u003e"
			],
			"formula": [
				"G.f.: 1/((1-x)*(1-x^2)^2) = 1/((1+x)^2*(1-x)^3).",
				"E.g.f.: (exp(x)*(2*x^2 +12*x+ 11) - exp(-x)*(2*x -5))/16.",
				"a(-n) = a(-5+n).",
				"a(n) = binomial(floor(n/2)+2, 2). - _Vladimir Shevelev_, May 03 2011",
				"From _Paul Barry_, May 31 2003: (Start)",
				"a(n) = ((2*n +5)*(-1)^n + (2*n^2 +10*n +11))/16.",
				"a(n) = Sum_{k=0..n} ((k+2)*(1+(-1)^k))/4. (End)",
				"From _Paul Barry_, Apr 16 2005: (Start)",
				"a(n) = Sum_{k=0..n} floor((k+2)/2)*(1-(-1)^(n+k-1))/2.",
				"a(n) = Sum_{k=0..floor(n/2)} floor((n-2k+2)/2). (End)",
				"A signed version is given by Sum_{k=0..n} (-1)^k*floor(k^2/4). - _Paul Barry_, Aug 19 2003",
				"a(n) = A108299(n-2,n)*(-1)^floor((n+1)/2) for n\u003e1. - _Reinhard Zumkeller_, Jun 01 2005",
				"a(n+1) = [Sum_{k=1..n} k mod (n+1)] + a(n), with n\u003e=1 and a(1)=1. - _Paolo P. Lava_, Mar 19 2007",
				"a(n) = A004125(n+3) - A049798(n+2). - _Carl Najafi_, Jan 31 2013",
				"a(n) = Sum_{i=1..floor((n+2)/2)} i. - _Wesley Ivan Hurt_, Jun 08 2013",
				"a(n) = (1/2)*floor((n+2)/2)*(floor((n+2)/2)+1). - _Wesley Ivan Hurt_, Jun 08 2013",
				"From _Wesley Ivan Hurt_, Apr 22 2015: (Start)",
				"a(n) = a(n-1) +2*a(n-2) -2*a(n-3) -a(n-4) +a(n-5).",
				"a(n) = (2*n +3 +(-1)^n)*(2*n +7 +(-1)^n)/32. (End)",
				"a(n-1) = A054252(n,1) = A054252(n^2-1), n \u003e= 1. See a Oct 03 2016 comment above. - _Wolfdieter Lang_, Oct 03 2016",
				"a(n) = A000217(A008619(n)). - _Guenther Schrack_, Sep 12 2018"
			],
			"example": [
				"a(5) = 6, since (5) + 2 = 7 has three nondecreasing partitions with exactly 2 parts: (1,6),(2,5),(3,4). The sum of the first parts of these partitions = 1 + 2 + 3 = 6. - _Wesley Ivan Hurt_, Jun 08 2013"
			],
			"maple": [
				"A008805:=n-\u003e(2*n+3+(-1)^n)*(2*n+7+(-1)^n)/32: seq(A008805(n), n=0..50); # _Wesley Ivan Hurt_, Apr 22 2015"
			],
			"mathematica": [
				"CoefficientList[Series[1/(1-x^2)^2/(1-x), {x, 0, 50}], x]",
				"Table[Binomial[Floor[n/2] + 2, 2], {n, 0, 57}] (* _Michael De Vlieger_, Oct 03 2016 *)"
			],
			"program": [
				"(PARI) a(n)=(n\\2+2)*(n\\2+1)/2",
				"(Haskell)",
				"import Data.List (transpose)",
				"a008805 = a000217 . (`div` 2) . (+ 1)",
				"a008805_list = drop 2 $ concat $ transpose [a000217_list, a000217_list]",
				"-- _Reinhard Zumkeller_, Feb 01 2013",
				"(MAGMA) [(2*n+3+(-1)^n)*(2*n+7+(-1)^n)/32 : n in [0..50]]; // _Wesley Ivan Hurt_, Apr 22 2015",
				"(Sage) [(2*n +3 +(-1)^n)*(2*n +7 +(-1)^n)/32 for n in (0..60)] # _G. C. Greubel_, Sep 12 2019",
				"(GAP) List([0..60], n-\u003e (2*n +3 +(-1)^n)*(2*n +7 +(-1)^n)/32); # _G. C. Greubel_, Sep 12 2019"
			],
			"xref": [
				"Cf. A000217, A002260, A006918 (partial sums), A054252."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"references": 65,
			"revision": 120,
			"time": "2021-09-26T01:52:30-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}