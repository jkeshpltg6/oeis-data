{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A063902",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 63902,
			"data": "1,1,2,10,80,1000,17600,418000,12848000,496672000,23576960000,1348404640000,91442700800000,7255463564800000,665885747225600000,69994087116448000000,8354181454767104000000,1123646013779238400000000,169165728883243642880000000",
			"name": "a(n+1) = sum{j = 0,...n}[C(2n,2j)a(j)a(n-j)] with a(0) = 1.",
			"link": [
				"Vaclav Kotesovec, \u003ca href=\"/A063902/b063902.txt\"\u003eTable of n, a(n) for n = 0..270\u003c/a\u003e",
				"Markus Kuba, Alois Panholzer, \u003ca href=\"http://arxiv.org/abs/1411.4587\"\u003eCombinatorial families of multilabelled increasing trees and hook-length formulas\u003c/a\u003e, arXiv:1411.4587 [math.CO], 2014."
			],
			"formula": [
				"E.g.f. satisfies: A(x) = exp( Integral 1/A(x) * Integral A(x)^2 dx dx ), where A(x) = Sum_{n\u003e=0} a(n)*x^(1*n)/(2*n)! and the constant of integration is zero. - _Paul D. Hanna_, Jun 02 2015",
				"From _Vaclav Kotesovec_, Jun 14 2015: (Start)",
				"a(n) ~ c * d^n * n!^2 * sqrt(n), where d = A258895 = 32*Pi / (Gamma(1/6) * Gamma(1/3))^2 = 2^(17/3) * Pi^2 / (3*Gamma(1/3)^6) = 0.452104299183420528841..., c = 1.53043521765866544548745...  = 2^(20/3) * Pi^(3/2) / Gamma(1/3)^6 = 192*sqrt(Pi) / (Gamma(1/3)*Gamma(1/6))^2.",
				"a(n) ~ 3 * 2^(5*n+7) * Pi^(n+3/2) * n^(2*n+3/2) / (exp(2*n) * Gamma(1/6)^(2*n+2) * Gamma(1/3)^(2*n+2)).",
				"a(n) ~ 2^((17*n+23)/3) * Pi^(2*n+5/2) * n^(2*n+3/2) / (3^n * exp(2*n) * Gamma(1/3)^(6*n+6)).",
				"(End)",
				"Define e.g.f. A(x) = Sum){n\u003e=0} a(n)*x^(2*n)/(2*n)!. Then 3*A'(x)^2 = 2*A(x)^3 - 2. - _Michael Somos_, Jan 07 2022"
			],
			"example": [
				"a(3) = 1*a(0)a(2) + 6*a(1)a(1) + 1*a(2)a(0) = 2+6+2 = 10.",
				"E.g.f.: A(x) = 1 + x^2/2! + 2*x^4/4! + 10*x^6/6! + 80*x^8/8! + ...",
				"G.f. = 1 + x + 2*x^2 + 10*x^3 + 80*x^4 + 1000*x^8 + 17600*x^9 + ..."
			],
			"mathematica": [
				"Clear[a]; a[n_]:=a[n]=If[n\u003c2,1,Sum[a[j]*a[n-1-j]*Binomial[2*n-2,2*j],{j,0,n-1}]]; Table[a[n], {n,0,30}] (* _Vaclav Kotesovec_, Jun 14 2015 *)"
			],
			"program": [
				"(PARI) /* E.g.f. A(x) = exp( Integral 1/A(x) * Integral A(x)^2 dx dx ) */",
				"{a(n) = local(A=1+x); for(i=1,n, A = exp( intformal( 1/A * intformal( A^2 + x*O(x^n)) ) ) ); n!*polcoeff(A,n)}",
				"for(n=0,20,print1(a(2*n),\", \")) \\\\ _Paul D. Hanna_, Jun 02 2015",
				"(PARI) /* By definition: */",
				"{a(n) = if(n==0,1,sum(k=0,n-1, binomial(2*n-2,2*k)*a(k)*a(n-k-1)))}",
				"for(n=0,20,print1(a(n),\", \")) \\\\ _Paul D. Hanna_, Jun 02 2015"
			],
			"xref": [
				"a(n+1) = sum[C(n, j)a(j)a(n-j)] would give factorials A000142, a(n+1) = sum[C(2n, 2j)a(j)a(n-j)]/a(n) would give Catalan numbers A000108, a(n+1) = sum[C(n, j)a(j)a(n-j)]/a(n) would give central binomials A001405.",
				"Cf. A007558, A258895."
			],
			"keyword": "nonn,changed",
			"offset": "0,3",
			"author": "_Henry Bottomley_, Aug 30 2001",
			"ext": [
				"More terms from _Vaclav Kotesovec_, Jun 14 2015"
			],
			"references": 4,
			"revision": 26,
			"time": "2022-01-07T17:07:28-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}