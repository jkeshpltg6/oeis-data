{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A082147",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 82147,
			"data": "1,1,9,89,945,10577,123129,1476841,18130401,226739489,2878666857,37006326777,480750990993,6301611631473,83240669582937,1106980509493641,14808497812637121,199138509770855489,2690461489090104009",
			"name": "a(0)=1; for n \u003e= 1, a(n) = Sum_{k=0..n} 8^k*N(n,k) where N(n,k) = (1/n)*C(n,k)*C(n,k+1) are the Narayana numbers (A001263).",
			"comment": [
				"More generally coefficients of (1 + m*x - sqrt(m^2*x^2 - (2*m+4)*x+1))/( (2*m+2)*x) are given by a(n) = Sum_{k=0..n} (m+1)^k*N(n,k)).",
				"The Hankel transform of this sequence is 8^C(n+1,2). - _Philippe Deléham_, Oct 29 2007",
				"Shifts left when INVERT transform applied eight times. - _Benedict W. J. Irwin_, Feb 07 2016"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A082147/b082147.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"Paul Barry, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL9/Barry/barry91.html\"\u003eOn Integer-Sequence-Based Constructions of Generalized Pascal Triangles\u003c/a\u003e, Journal of Integer Sequences, Vol. 9 (2006), Article 06.2.4."
			],
			"formula": [
				"G.f.: (1 + 7*x - sqrt(49*x^2-18*x+1))/(16*x).",
				"a(n) = Sum_{k=0..n} A088617(n, k)*8^k*(-7)^(n-k). - _Philippe Deléham_, Jan 21 2004",
				"a(n) = (9(2n-1)a(n-1) - 49(n-2)a(n-2)) / (n+1) for n \u003e= 2, a(0) = a(1) = 1. - _Philippe Deléham_, Aug 19 2005",
				"a(n) = upper left term in M^n, M = the production matrix:",
				"  1, 1",
				"  8, 8, 8",
				"  1, 1, 1, 1",
				"  8, 8, 8, 8, 8",
				"  1, 1, 1, 1, 1, 1",
				"  ...",
				"- _Gary W. Adamson_, Jul 08 2011",
				"a(n) ~ sqrt(16+18*sqrt(2))*(9+4*sqrt(2))^n/(16*sqrt(Pi)*n^(3/2)). - _Vaclav Kotesovec_, Oct 14 2012",
				"G.f.: 1/(1 - x/(1 - 8*x/(1 - x/(1 - 8*x/(1 - x/(1 - ...)))))), a continued fraction. - _Ilya Gutkovskiy_, Apr 21 2017",
				"a(n) = hypergeom([1 - n, -n], [2], 8). - _Peter Luschny_, Mar 19 2018"
			],
			"maple": [
				"A082147_list := proc(n) local j, a, w; a := array(0..n); a[0] := 1;",
				"for w from 1 to n do a[w] := a[w-1]+8*add(a[j]*a[w-j-1],j=1..w-1) od;",
				"convert(a, list) end: A082147_list(18); # _Peter Luschny_, May 19 2011"
			],
			"mathematica": [
				"Table[SeriesCoefficient[(1+7*x-Sqrt[49*x^2-18*x+1])/(16*x),{x,0,n}],{n,0,20}] (* _Vaclav Kotesovec_, Oct 14 2012 *)",
				"f[n_] := Sum[ 8^k*Binomial[n, k]*Binomial[n, k + 1]/n, {k, 0, n}]; f[0] = 1; Array[f, 21, 0] (* _Robert G. Wilson v_, Feb 24 2018 *)",
				"a[n_] := Hypergeometric2F1[1 - n, -n, 2, 8];",
				"Table[a[n], {n, 0, 18}] (* _Peter Luschny_, Mar 19 2018 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c1,1,sum(k=0,n,8^k/n*binomial(n,k)*binomial(n,k+1)))",
				"(MAGMA) Q:=Rationals(); R\u003cx\u003e:=PowerSeriesRing(Q, 40); Coefficients(R!((1+7*x-Sqrt(49*x^2-18*x+1))/(16*x))) // _G. C. Greubel_, Feb 05 2018",
				"(GAP) a:=n-\u003eSum([0..n],k-\u003e8^k*(1/n)*Binomial(n,k)*Binomial(n,k+1));;",
				"Concatenation([1],List([1..18],n-\u003ea(n))); # _Muniru A Asiru_, Feb 10 2018"
			],
			"xref": [
				"Cf. A001003, A007564, A059231."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Benoit Cloitre_, May 10 2003",
			"references": 7,
			"revision": 56,
			"time": "2018-03-19T11:10:54-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}