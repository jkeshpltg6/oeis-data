{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A238539",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 238539,
			"data": "1,35,399,7735,112871,1893255,29593159,479082695,7620584391,122287263175,1953732901319,31282632909255,500338874618311,8006888009380295,128098480026087879,2049669505409577415,32793961486615474631,524709388585350492615,8395302178969583120839",
			"name": "A fourth-order linear divisibility sequence: a(n) := (1/9)*(2^n + (-1)^n)*(2^(3*n) - (-1)^n).",
			"comment": [
				"This is a divisibility sequence, that is, if n | m then a(n) | a(m). This is a consequence of the following more general result: The polynomials P(n,x,y) := (x^n + y^n)*(x^(3*n) - y^(3*n)) form a divisibility sequence in the polynomial ring Z[x,y]. See the Bala link.",
				"The sequence satisfies a homogeneous linear recurrence of the fourth order. However, it does not belong to the family of linear divisibility sequences of the fourth order studied by Williams and Guy, which have o.g.f.s of the form x*(1 - q*x^2)/Q(x), Q(x) a quartic polynomial and q an integer.",
				"For sequences of a similar type see A238536 through A238541."
			],
			"link": [
				"Peter Bala, \u003ca href=\"/A238536/a238536.pdf\"\u003eA family of linear divisibility sequences of order four\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Divisibility_sequence\"\u003eDivisibility sequence\"\u003c/a\u003e",
				"H. C. Williams and R. K. Guy, \u003ca href=\"http://dx.doi.org/10.1142/S1793042111004587\"\u003eSome fourth-order linear divisibility sequences\u003c/a\u003e, Intl. J. Number Theory 7 (5) (2011) 1255-1277.",
				"H. C. Williams and R. K. Guy, \u003ca href=\"http://www.emis.de/journals/INTEGERS/papers/a17self/a17self.pdf\"\u003eSome Monoapparitic Fourth Order Linear Divisibility Sequences\u003c/a\u003e Integers, Volume 12A (2012) The John Selfridge Memorial Volume",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (7,138,112,-256)."
			],
			"formula": [
				"a(n) = (1/9)*(2^n + (-1)^n)*(2^(3*n) - (-1)^n) = (1/9)*(4^n - 1)*(8^n - (-1)^n)/(2^n - (-1)^n).",
				"O.g.f.: x*(1 + 28*x + 16*x^2)/((1 - x)*(1 + 2*x)*(1 + 8*x)*(1 - 16*x)).",
				"Recurrence equation: a(n) = 7*a(n-1) + 138*a(n-2) + 112*a(n-4) - 256*a(n-4)."
			],
			"maple": [
				"seq(1/9*(2^n + (-1)^n)*(2^(3*n) - (-1)^n), n = 1..20);"
			],
			"xref": [
				"Cf. A238536, A238537, A238538, A238540, A238541."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Peter Bala_, Mar 01 2014",
			"references": 4,
			"revision": 18,
			"time": "2021-02-06T21:50:54-05:00",
			"created": "2014-03-01T10:27:12-05:00"
		}
	]
}