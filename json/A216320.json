{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A216320",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 216320,
			"data": "1,1,1,1,2,1,2,1,2,1,3,3,1,4,4,2,1,3,3,1,4,4,2,1,5,5,5,5,1,2,2,2,1,3,2,6,3,6,1,3,6,3,6,2,1,4,2,4,1,8,8,4,4,8,8,2,1,8,8,8,4,8,2,4,1,6,6,3,3,2,1,9,9,3,9,3,9,9,9,1,4,4,2,2,4,4,2,1,3,6,2,3,6",
			"name": "Irregular triangle: row n lists the Modd n order of the odd members of the reduced smallest nonnegative residue class modulo n.",
			"comment": [
				"The length of row n is delta(n):=A055034(n).",
				"For the multiplicative group Modd n see a comment on A203571, and also on A216319.",
				"A216319(n,k)^a(n,k) == +1 (Modd n), n \u003e= 1.",
				"If the Modd n order of an (odd) element from row n of A216319 is delta(n) (the row length) then this element is a primitive root Modd n. There is no primitive root Modd n if no such element of order delta(n) exists. For example, n = 12, 20, ... (see A206552 for more of these n values). There are phi(delta(n)) = A216321(n) such primitive roots Modd n if there exists one, where phi=A000010 (Euler's totient). The multiplicative group Modd n is cyclic if and only if there exists a primitive root Modd n. The multiplicative group Modd n is isomorphic to the Galois group G(Q(rho(n)/Q) with the algebraic number rho(n) := 2*cos(Pi/n), n\u003e=1."
			],
			"formula": [
				"a(n,k) = order of A216319(n,k) Modd n, n\u003e=1, k=1, 2, ..., A055034(n). This means: A216319(n,k)^a(n,k) == +1 (Modd n), n\u003e=1, and a(n,k) is the smallest positive integer exponent satisfying this congruence. For Modd n see a comment on A203571."
			],
			"example": [
				"The table a(n,k) begins:",
				"n\\k 1  2  3  4  5  6  7  8  9 ...",
				"1   1",
				"2   1",
				"3   1",
				"4   1  2",
				"5   1  2",
				"6   1  2",
				"7   1  3  3",
				"8   1  4  4  2",
				"9   1  3  3",
				"10  1  4  4  2",
				"11  1  5  5  5  5",
				"12  1  2  2  2",
				"13  1  3  2  6  3  6",
				"14  1  3  6  3  6  2",
				"15  1  4  2  4",
				"16  1  8  8  4  4  8  8  2",
				"17  1  8  8  8  4  8  2  4",
				"18  1  6  6  3  3  2",
				"19  1  9  9  3  9  3  9  9  9",
				"20  1  4  4  2  2  4  4  2",
				"...",
				"a(7,2) = 3 because A216319(7,2) = 3 and 3^1 == 3 (Modd 7);",
				"  3^2 = 9 == 5 (Modd 7) because floor(9/7)= 1 which is odd, therefore 9 (Modd 7) = -9 (mod 7) = 5; 3^3 == 5*3 (Modd n)",
				"  = +1 because floor(15/7)=2 which is even, therefore 15 (Modd 7) = 15 (modd 7) = +1.",
				"Row n=12 is the first row without an order = delta(n) (row length), in this case 4. Therefore there is no primitive root Modd 12, and the multiplicative group Modd 12 is non-cyclic.",
				"  Its cycle structure is [[5,1],[7,1],[11,1]] which is the group Z_2 x Z_2 (the Klein 4-group)."
			],
			"xref": [
				"Cf. A203571, A216321."
			],
			"keyword": "nonn,easy,tabf",
			"offset": "1,5",
			"author": "_Wolfdieter Lang_, Sep 21 2012",
			"references": 5,
			"revision": 16,
			"time": "2015-08-11T15:41:47-04:00",
			"created": "2012-09-22T17:33:16-04:00"
		}
	]
}