{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A060839",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 60839,
			"data": "1,1,1,1,1,1,3,1,3,1,1,1,3,3,1,1,1,3,3,1,3,1,1,1,1,3,3,3,1,1,3,1,1,1,3,3,3,3,3,1,1,3,3,1,3,1,1,1,3,1,1,3,1,3,1,3,3,1,1,1,3,3,9,1,3,1,3,1,1,3,1,3,3,3,1,3,3,3,3,1,3,1,1,3,1,3,1,1,1,3,9,1,3,1,3,1,3,3,3,1,1,1,3,3,3",
			"name": "Number of solutions to x^3 == 1 mod n.",
			"comment": [
				"Sum(k=1,n,a(k)) appears to be asymptotic to C*n*Log(n) with C=0.4... - _Benoit Cloitre_, Aug 19 2002 [C = (11/(6*Pi*sqrt(3))) * Product_{p prime == 1 (mod 3)} (1 - 2/(p*(p+1))) = 0.3170565167... (Finch and Sebah, 2006). - _Amiram Eldar_, Mar 26 2021]",
				"Multiplicative with a(3) = 1, a(3^e) = 3, e \u003e= 2, a((3k+1)^e) = 3, a((3k+2)^e) = 1. _David W. Wilson_, May 22 2005"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A060839/b060839.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Steven Finch, Greg Martin and Pascal Sebah, \u003ca href=\"https://doi.org/10.1090/S0002-9939-10-10341-4\"\u003eRoots of unity and nullity modulo n\u003c/a\u003e, Proc. Amer. Math. Soc., Vol. 138, No. 8 (2010), pp. 2729-2743.",
				"Steven Finch and Pascal Sebah, \u003ca href=\"https://arxiv.org/abs/math/0604465\"\u003eSquares and Cubes Modulo n\u003c/a\u003e, arXiv:math/0604465 [math.NT], 2006-2016."
			],
			"formula": [
				"Let b(n) be the number of primes dividing n which are congruent to 1 mod 3 (sequence A005088); then a(n) is 3^b(n) if n is not divisible by 9 and 3^(b(n) + 1) if n is divisible by 9.",
				"Sequence is multiplicative. - _David W. Wilson_, Jan 06 2005"
			],
			"example": [
				"a(7) = 3 because the three solutions to x^3 == 1 mod 7 are x = 1,2,4."
			],
			"maple": [
				"A060839 := proc(n)",
				"    local a,pf,p,r;",
				"    a := 1 ;",
				"    for pf in ifactors(n)[2] do",
				"        p := op(1,pf);",
				"        r := op(2,pf);",
				"        if p = 2 then",
				"            ;",
				"        elif p =3 then",
				"            if r \u003e= 2 then",
				"                a := a*3 ;",
				"            end if;",
				"        else",
				"            if modp(p,3) = 2 then",
				"                ;",
				"            else",
				"                a := 3*a ;",
				"            end if;",
				"        end if;",
				"    end do:",
				"    a ;",
				"end proc:",
				"seq(A060839(n),n=1..40) ; # _R. J. Mathar_, Mar 02 2015"
			],
			"mathematica": [
				"a[n_] := Sum[ If[ Mod[k^3-1, n] == 0, 1, 0], {k, 1, n}]; Table[ a[n], {n, 1, 105}](* _Jean-François Alcover_, Nov 14 2011, after Pari *)"
			],
			"program": [
				"(PARI) a(n)=sum(i=1,n,if((i^3-1)%n,0,1))"
			],
			"xref": [
				"Cf. A005088, A060594."
			],
			"keyword": "nonn,nice,easy,mult",
			"offset": "1,7",
			"author": "Ahmed Fares (ahmedfares(AT)my-deja.com), May 02 2001",
			"references": 16,
			"revision": 31,
			"time": "2021-03-26T08:39:31-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}