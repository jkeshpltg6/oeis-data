{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A299090",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 299090,
			"data": "0,1,1,2,1,1,1,2,2,1,1,2,1,1,1,3,1,2,1,2,1,1,1,2,2,1,2,2,1,1,1,3,1,1,1,2,1,1,1,2,1,1,1,2,2,1,1,3,2,2,1,2,1,2,1,2,1,1,1,2,1,1,2,3,1,1,1,2,1,1,1,2,1,1,2,2,1,1,1,3,3,1,1,2,1,1,1,2,1,2,1,2,1,1,1,3,1,2,2,2,1,1,1,2,1",
			"name": "Number of \"digits\" in the binary representation of the multiset of prime factors of n.",
			"comment": [
				"a(n) is also the binary weight of the largest multiplicity in the multiset of prime factors of n.",
				"Any finite multiset m has a unique binary representation as a finite word bin(m) = s_k..s_1 such that: (1) each \"digit\" s_i is a finite set, (2) the leading term s_k is nonempty, and (3) m = 1*s_1 + 2*s_2 + 4*s_3 + 8*s_4 + ... + 2^(k-1)*s_k where + is multiset union, 1*S = S as a multiset, and n*S = 1*S + (n-1)*S for n \u003e 1. The word bin(m) can be thought of as a finite 2-adic set. For example,",
				"bin({1,1,1,1,2,2,3,3,3}) = {1}{2,3}{3},",
				"bin({1,1,1,1,1,2,2,2,2}) = {1,2}{}{1},",
				"bin({1,1,1,1,1,2,2,2,3}) = {1}{2}{1,2,3}.",
				"a(n) is the least k such that columns indexed k or greater in A329050 contain no divisors of n. - _Peter Munn_, Feb 10 2020"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A299090/b299090.txt\"\u003eTable of n, a(n) for n = 1..65537\u003c/a\u003e",
				"\u003ca href=\"/index/Eu#epf\"\u003eIndex entries for sequences computed from exponents in factorization of n\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A070939(A051903(n)), n\u003e1.",
				"If m is a set then bin(m) has only one \"digit\" m; so a(n) = 1 if n is squarefree.",
				"If m is of the form n*{x} then bin(m) is obtained by listing the binary digits of n and replacing 0 -\u003e {}, 1 -\u003e {x}; so a(p^n) = binary weight of n.",
				"a(n) = A061395(A225546(n)). - _Peter Munn_, Feb 10 2020"
			],
			"example": [
				"36 has prime factors {2,2,3,3} with binary representation {2,3}{} so a(36) = 2.",
				"Binary representations of the prime multisets of each positive integer begin: {}, {2}, {3}, {2}{}, {5}, {2,3}, {7}, {2}{2}, {3}{}, {2,5}, {11}, {2}{3}, {13}, {2,7}, {3,5}, {2}{}{}."
			],
			"mathematica": [
				"Table[If[n===1,0,IntegerLength[Max@@FactorInteger[n][[All,2]],2]],{n,100}]"
			],
			"program": [
				"(PARI)",
				"A051903(n) = if((1==n),0,vecmax(factor(n)[, 2]));",
				"A299090(n) = if(1==n,0,#binary(A051903(n))); \\\\ _Antti Karttunen_, Jul 29 2018"
			],
			"xref": [
				"Cf. A001511, A051903, A052409, A070939, A112798, A329050.",
				"Related to A061395 via A225546."
			],
			"keyword": "nonn,base",
			"offset": "1,4",
			"author": "_Gus Wiseman_, Feb 02 2018",
			"ext": [
				"More terms from _Antti Karttunen_, Jul 29 2018"
			],
			"references": 8,
			"revision": 18,
			"time": "2020-03-01T20:18:50-05:00",
			"created": "2018-02-23T22:13:27-05:00"
		}
	]
}