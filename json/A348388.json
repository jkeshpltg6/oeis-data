{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A348388",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 348388,
			"data": "1,2,3,1,4,1,5,2,1,6,2,1,7,3,1,1,8,3,2,1,9,4,2,1,1,10,4,2,1,1,11,5,3,2,1,1,12,5,3,2,1,1,13,6,3,2,1,1,1,14,6,4,2,2,1,1,15,7,4,3,2,1,1,1,16,7,4,3,2,1,1,1,17,8,5,3,2,2,1,1,1,18,8,5,3,2,2,1,1,1,19,9,5,4,3,2,1,1,1,1",
			"name": "Irregular triangle read by rows: T(n, k) = floor((n-k)/k), for k = 1, 2, ..., floor(n/2) and n \u003e= 2.",
			"comment": [
				"This irregular triangle T(n, k) gives the number of multiples of number k, larger than k and not exceeding n, for k = 1, 2, ..., floor(n/2), for n \u003e= 2. See A348389 for the array of these multiples.",
				"The length of row n is floor(n/2) = A004526(n), for n \u003e= 2.",
				"The row sums give A002541(n). See the formula given there by _Wesley Ivan Hurt_, May 08 2016.",
				"The columns give the k-fold repeated positive integers k, for k \u003e= 1."
			],
			"link": [
				"Karl-Heinz Hofmann, \u003ca href=\"/A348388/b348388.txt\"\u003eTable of n, a(n) for n = 2..10101\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) =  floor((n-k)/k), for k = 1, 2, ..., floor(n/2) and n \u003e= 2.",
				"G.f. of column k: G(k, x) = x^(2*k)/((1 - x)*(1 - x^k))."
			],
			"example": [
				"The irregular triangle T(n, k) begins:",
				"n\\k   1 2 3 4 5 6 7 8 9 10 ...",
				"------------------------------",
				"2:    1",
				"3:    2",
				"4:    3 1",
				"5:    4 1",
				"6:    5 2 1",
				"7:    6 2 1",
				"8:    7 3 1 1",
				"9:    8 3 2 1",
				"10:   9 4 2 1 1",
				"11:  10 4 2 1 1",
				"12:  11 5 3 2 1 1",
				"13:  12 5 3 2 1 1",
				"14:  13 6 3 2 1 1 1",
				"15:  14 6 4 2 2 1 1",
				"16:  15 7 4 3 2 1 1 1",
				"17:  16 7 4 3 2 1 1 1",
				"18:  17 8 5 3 2 2 1 1 1",
				"19:  18 8 5 3 2 2 1 1 1",
				"20:  19 9 5 4 3 2 1 1 1  1",
				"..."
			],
			"mathematica": [
				"T[n_, k_] := Floor[(n - k)/k]; Table[T[n, k], {n, 2, 20}, {k, 1, Floor[n/2]}] // Flatten (* _Amiram Eldar_, Nov 02 2021 *)"
			],
			"program": [
				"(Python)",
				"def A348388row(n): return [(n - k) // k for k in range(1, 1 + n // 2)]",
				"for n in range(2, 21): print(A348388row(n))  # _Peter Luschny_, Nov 05 2021"
			],
			"xref": [
				"Columns k (with varying offsets): A000027, A004526, A008620, A008621, A002266, A097992, ...",
				"Cf. A002541, A004526, A010766, A348389."
			],
			"keyword": "nonn,easy,tabf",
			"offset": "2,2",
			"author": "_Wolfdieter Lang_, Oct 31 2021",
			"references": 2,
			"revision": 24,
			"time": "2021-11-06T11:08:05-04:00",
			"created": "2021-11-05T13:51:53-04:00"
		}
	]
}