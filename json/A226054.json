{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A226054",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 226054,
			"data": "1,1,2,1,3,4,5,6,7,11,15,17,22,24,34,40,48,56,69,84,104,118,144,164,200,234,273,318,372,436,511,582,681,775,906,1036,1192,1362,1562,1784,2046,2315,2647,2988,3409,3860,4371,4936,5573,6288,7104,7967,8979,10052",
			"name": "McKay-Thompson series of class 45A for the Monster group with a(0) = 1.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A226054/b226054.txt\"\u003eTable of n, a(n) for n = -1..1000\u003c/a\u003e",
				"Vaclav Kotesovec, \u003ca href=\"https://arxiv.org/abs/1509.08708\"\u003eA method of finding the asymptotics of q-series based on the convolution of generating functions\u003c/a\u003e, arXiv:1509.08708 [math.CO], 2015-2016."
			],
			"formula": [
				"Expansion of (eta(q^3) * eta(q^15))^2 / (eta(q) * eta(q^5) * eta(q^9) * eta(q^45)) in powers of q.",
				"Euler transform of period 45 sequence [ 1, 1, -1, 1, 2, -1, 1, 1, 0, 2, 1, -1, 1, 1, -2, 1, 1, 0, 1, 2, -1, 1, 1, -1, 2, 1, 0, 1, 1, -2, 1, 1, -1, 1, 2, 0, 1, 1, -1, 2, 1, -1, 1, 1, 0, ...].",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2)) where f(u, v) = u*v * (u*v + 3) - (u+v) * (u^2 + u*v + v^2).",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (45 t)) = f(t) where q = exp(2 Pi i t).",
				"a(n) = A058684(n) unless n=0.",
				"a(n) ~ exp(4*Pi*sqrt(n/5)/3) / (5^(1/4) * sqrt(6) * n^(3/4)). - _Vaclav Kotesovec_, Oct 14 2015"
			],
			"example": [
				"1/q + 1 + 2*q + q^2 + 3*q^3 + 4*q^4 + 5*q^5 + 6*q^6 + 7*q^7 + 11*q^8 + ..."
			],
			"mathematica": [
				"nmax=60; CoefficientList[Series[Product[(1-x^(3*k))^2 * (1-x^(15*k))^2 / ((1-x^k) * (1-x^(5*k)) * (1-x^(9*k)) * (1-x^(45*k))),{k,1,nmax}],{x,0,nmax}],x] (* _Vaclav Kotesovec_, Oct 14 2015 *)",
				"eta[q_] := q^(1/24)*QPochhammer[q]; a:= CoefficientList[Series[q*(eta[q^3]*eta[q^15])^2/(eta[q]*eta[q^5]*eta[q^9]*eta[q^45]), {q, 0, 60}], q]; Table[a[[n]], {n, 1, 50}] (* _G. C. Greubel_, Jun 20 2018 *)"
			],
			"program": [
				"(PARI) {a(n) = local(A); if( n\u003c-1, 0, n++; A = x * O(x^n); polcoeff( (eta(x^3 + A) * eta(x^15 + A))^2 / (eta(x + A) * eta(x^5 + A) * eta(x^9 + A) * eta(x^45 + A)), n))}"
			],
			"xref": [
				"Cf. A058684."
			],
			"keyword": "nonn",
			"offset": "-1,3",
			"author": "_Michael Somos_, May 24 2013",
			"references": 2,
			"revision": 14,
			"time": "2018-06-20T06:55:08-04:00",
			"created": "2013-05-25T01:41:59-04:00"
		}
	]
}