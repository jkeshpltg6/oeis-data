{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005250",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5250,
			"id": "M0994",
			"data": "1,2,4,6,8,14,18,20,22,34,36,44,52,72,86,96,112,114,118,132,148,154,180,210,220,222,234,248,250,282,288,292,320,336,354,382,384,394,456,464,468,474,486,490,500,514,516,532,534,540,582,588,602,652",
			"name": "Record gaps between primes.",
			"comment": [
				"Here a \"gap\" means prime(n+1) - prime(n), but in other references it can mean prime(n+1) - prime(n) - 1.",
				"a(n+1)/a(n) \u003c= 2, for all n \u003c= 80, and a(n+1)/a(n) \u003c 1 + f(n)/a(n) with f(n)/a(n) \u003c= epsilon for some function f(n) and with 0 \u003c epsilon \u003c= 1. It also appears, with the small amount of data available, for all n \u003c= 80, that a(n+1)/a(n) ~ 1. - _John W. Nicholson_, Jun 08 2014, updated Aug 05 2019",
				"Equivalent to the above statement, A053695(n) = a(n+1) - a(n) \u003c= a(n). - _John W. Nicholson_, Jan 20 2016",
				"Conjecture: a(n) = O(n^2); specifically, a(n) \u003c= n^2. - _Alexei Kourbatov_, Aug 05 2017",
				"Conjecture: below the k-th prime, the number of maximal gaps is about 2*log(k), i.e., about twice as many as the expected number of records in a sequence of k i.i.d. random variables (see arXiv:1709.05508 for a heuristic explanation). - _Alexei Kourbatov_, Mar 16 2018"
			],
			"reference": [
				"B. C. Berndt, Ramanujan's Notebooks Part IV, Springer-Verlag, see p. 133.",
				"R. K. Guy, Unsolved Problems in Number Theory, A8.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Rodolfo Ruiz-Huidobro, \u003ca href=\"/A005250/b005250.txt\"\u003eTable of n, a(n) for n = 1..80\u003c/a\u003e (terms 1..77 from John W. Nicholson)",
				"Jens Kruse Andersen, \u003ca href=\"http://primerecords.dk/primegaps/gaps20.htm\"\u003eThe Top-20 Prime Gaps\u003c/a\u003e",
				"Jens Kruse Andersen, \u003ca href=\"http://primerecords.dk/primegaps/megagap2.htm\"\u003eNew record prime gap\u003c/a\u003e",
				"Jens Kruse Andersen, \u003ca href=\"http://primerecords.dk/primegaps/maximal.htm\"\u003eMaximal gaps\u003c/a\u003e",
				"Alex Beveridge, \u003ca href=\"/A005250/a005250.txt\"\u003eTable giving known values of A000101(n), A005250(n), A107578(n)\u003c/a\u003e",
				"R. P. Brent, J. H. Osborn and W. D. Smith, \u003ca href=\"http://arxiv.org/abs/1211.3248\"\u003eLower bounds on maximal determinants of +-1 matrices via the probabilistic method\u003c/a\u003e, arXiv preprint arXiv:1211.3248 [math.CO], 2012.",
				"C. K. Caldwell, \u003ca href=\"http://www.utm.edu/research/primes/notes/gaps.html#table\"\u003eTable of prime gaps\u003c/a\u003e",
				"C. K. Caldwell, \u003ca href=\"http://www.utm.edu/research/primes/notes/GapsTable.html\"\u003eGaps up to 1132\u003c/a\u003e",
				"R. K. Guy, \u003ca href=\"/A000978/a000978.pdf\"\u003eLetter to N. J. A. Sloane, Aug 1986\u003c/a\u003e",
				"R. K. Guy, \u003ca href=\"/A005667/a005667.pdf\"\u003eLetter to N. J. A. Sloane, 1987\u003c/a\u003e",
				"Lutz Kämmerer, \u003ca href=\"https://arxiv.org/abs/2012.14263\"\u003eA fast probabilistic component-by-component construction of exactly integrating rank-1 lattices and applications\u003c/a\u003e, arXiv:2012.14263 [math.NA], 2020.",
				"Alexei Kourbatov, \u003ca href=\"http://arxiv.org/abs/1301.2242\"\u003eMaximal gaps between prime k-tuples: a statistical approach\u003c/a\u003e, arXiv preprint arXiv:1301.2242 [math.NT], 2013 and \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL16/Kourbatov/kourbatov3.html\"\u003eJ. Int. Seq. 16 (2013) #13.5.2\u003c/a\u003e.",
				"Alexei Kourbatov, \u003ca href=\"http://arxiv.org/abs/1309.4053\"\u003eTables of record gaps between prime constellations\u003c/a\u003e, arXiv preprint arXiv:1309.4053 [math.NT], 2013.",
				"Alexei Kourbatov, \u003ca href=\"http://arxiv.org/abs/1401.6959\"\u003eThe distribution of maximal prime gaps in Cramer's probabilistic model of primes\u003c/a\u003e, arXiv preprint arXiv:1401.6959 [math.NT], 2014.",
				"Alexei Kourbatov, \u003ca href=\"http://arxiv.org/abs/1506.03042\"\u003eUpper bounds for prime gaps related to Firoozbakht's conjecture\u003c/a\u003e, arXiv:1506.03042 [math.NT], 2015; and \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL18/Kourbatov/kourb7.html\"\u003eJ. Int. Seq. 18 (2015) #15.11.2\u003c/a\u003e.",
				"Alexei Kourbatov, \u003ca href=\"http://arxiv.org/abs/1503.01744\"\u003eVerification of the Firoozbakht conjecture for primes up to four quintillion\u003c/a\u003e, arXiv:1503.01744 [math.NT], 2015; and \u003ca href=\"http://dx.doi.org/10.12988/imf.2015.5322\"\u003eInt. Math. Forum, 10 (2015), 283-288\u003c/a\u003e.",
				"Alexei Kourbatov, \u003ca href=\"https://arxiv.org/abs/1610.03340\"\u003eOn the distribution of maximal gaps between primes in residue classes\u003c/a\u003e, arXiv preprint arXiv:1610.03340 [math.NT], 2016.",
				"Alexei Kourbatov, \u003ca href=\"https://arxiv.org/abs/1709.05508\"\u003eOn the nth record gap between primes in an arithmetic progression\u003c/a\u003e, arXiv:1709.05508 [math.NT], 2017; and \u003ca href=\"https://doi.org/10.12988/imf.2018.712103\"\u003eInt. Math. Forum, 13 (2018), 65-78\u003c/a\u003e.",
				"Alexei Kourbatov and Marek Wolf, \u003ca href=\"https://arxiv.org/abs/1901.03785\"\u003ePredicting maximal gaps in sets of primes\u003c/a\u003e, arXiv:1901.03785 [math.NT], 2019.",
				"Ya-Ping Lu and Shu-Fang Deng, \u003ca href=\"https://arxiv.org/abs/2007.15282\"\u003eAn upper bound for the prime gap\u003c/a\u003e, arXiv:2007.15282 [math.GM], 2020.",
				"Thomas R. Nicely, \u003ca href=\"https://faculty.lynchburg.edu/~nicely/index.html\"\u003eSome Results of Computational Research in Prime Numbers\u003c/a\u003e [See local copy in A007053]",
				"Thomas R. Nicely, \u003ca href=\"https://faculty.lynchburg.edu/~nicely/gaps/gaplist.html\"\u003eFirst occurrence prime gaps\u003c/a\u003e [For local copy see A000101]",
				"Tomás Oliveira e Silva, \u003ca href=\"http://sweet.ua.pt/tos/gaps.html\"\u003eGaps between consecutive primes\u003c/a\u003e",
				"D. Shanks, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1964-0167472-8\"\u003eOn maximal gaps between successive primes\u003c/a\u003e, Mathematics of Computation, 18(88), 646-651. (1964).",
				"Matt Visser, \u003ca href=\"https://arxiv.org/abs/1904.00499\"\u003eVerifying the Firoozbakht, Nicholson, and Farhadian conjectures up to the 81st maximal prime gap\u003c/a\u003e, arXiv:1904.00499 [math.NT], 2019.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PrimeGaps.html\"\u003ePrime Gaps\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Prime_gap\"\u003ePrime gap\u003c/a\u003e",
				"Robert G. Wilson v, \u003ca href=\"/A005250/a005250.pdf\"\u003eNotes (no date)\u003c/a\u003e",
				"Marek Wolf, \u003ca href=\"http://arxiv.org/abs/1010.3945\"\u003eA Note on the Andrica Conjecture\u003c/a\u003e, arXiv:1010.3945 [math.NT], 2010.",
				"J. Young and A. Potler, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1989-0947470-1\"\u003eFirst occurrence prime gaps\u003c/a\u003e, Math. Comp., 52 (1989), 221-224.",
				"\u003ca href=\"/index/Pri#gaps\"\u003eIndex entries for primes, gaps between\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A000101(n) - A002386(n) = A008996(n-1) + 1. - _M. F. Hasler_, Dec 13 2007",
				"a(n+1) = 1 + Sum_{i=1..n} A053695(i). - _John W. Nicholson_, Jan 20 2016"
			],
			"mathematica": [
				"nn=10^7;Module[{d=Differences[Prime[Range[nn]]],ls={1}},Table[If[d[[n]]\u003e Last[ls],AppendTo[ls,d[[n]]]],{n,nn-1}];ls] (* _Harvey P. Dale_, Jul 23 2012 *)"
			],
			"program": [
				"(PARI) p=q=2;g=0;until( g\u003c(q=nextprime(1+p=q))-p \u0026 print1(g=q-p,\",\"),) \\\\ _M. F. Hasler_, Dec 13 2007",
				"(PARI) p=2; g=0;m=g; forprime(q=3,10^13,g=q-p;if(g\u003em,print(g\", \",p,\", \",q);m=g);p=q) \\\\ _John W. Nicholson_, Dec 18 2016",
				"(Haskell)",
				"a005250 n = a005250_list !! (n-1)",
				"a005250_list = f 0 a001223_list",
				"   where f m (x:xs) = if x \u003c= m then f m xs else x : f x xs",
				"-- _Reinhard Zumkeller_, Dec 12 2012"
			],
			"xref": [
				"Records in A001223. For positions of records see A005669.",
				"Cf. A000040, A002386, A000101, A008996, A058320, A107578."
			],
			"keyword": "nonn,nice",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, _R. K. Guy_, May 20 1991",
			"ext": [
				"More terms from Andreas Boerner (andreas.boerner(AT)altavista.net), Jul 11 2000",
				"Additional comments from _Frank Ellermann_, Apr 20 2001",
				"More terms from _Robert G. Wilson v_, Jan 03 2002, May 01 2006"
			],
			"references": 60,
			"revision": 229,
			"time": "2021-10-28T13:04:17-04:00",
			"created": "1991-05-20T03:00:00-04:00"
		}
	]
}