{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A145702",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 145702,
			"data": "1,-1,0,-1,1,0,0,-1,1,-1,1,-1,2,-1,1,-1,2,-2,1,-2,3,-3,2,-3,4,-3,2,-4,5,-4,4,-5,6,-6,5,-6,8,-7,6,-8,11,-10,8,-11,13,-11,10,-13,16,-15,14,-17,20,-18,17,-20,24,-23,21,-25,31,-29,26,-32,37,-34,32",
			"name": "Expansion of chi(-x) * chi(x^5) in powers of x where chi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(1/4) * eta(q) * eta(q^10)^2 / eta(q^2) / eta(q^5) / eta(q^20) in powers of q.",
				"Euler transform of period 20 sequence [ -1, 0, -1, 0, 0, 0, -1, 0, -1, -1, -1, 0, -1, 0, 0, 0, -1, 0, -1, 0, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (640 t)) = 2^(1/2) g(t) where q = exp(2 Pi i t) and g() is the g.f. for A145703.",
				"G.f.: Product_{k\u003e0} (1 - x^(2*k - 1)) * (1 + x^(10*k - 5)).",
				"a(n) = (-1)^n * A139632(n). a(2*n) = A139631(n). a(2*n + 1) = - A145703(n).",
				"a(n) = -(-1)^floor(n/2) * A145704(n) = (-1)^floor((n + 1)/2) * A145705(n). - _Michael Somos_, Sep 06 2015"
			],
			"example": [
				"G.f. = 1 - x - x^3 + x^4 - x^7 + x^8 - x^9 + x^10 - x^11 + 2*x^12 - x^13 + ...",
				"G.f. = 1/q - q^3 - q^11 + q^15 - q^27 + q^31 - q^35 + q^39 - q^43 + 2*q^47 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ x, x^2] QPochhammer[ -x^5, x^10], {x, 0, n}]; (* _Michael Somos_, Sep 06 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A) * eta(x^10 + A)^2 / eta(x^2 + A) / eta(x^5 + A) / eta(x^20 + A), n))};"
			],
			"xref": [
				"Cf. A139631, A139632, A147503, A145704, A145705."
			],
			"keyword": "sign",
			"offset": "0,13",
			"author": "_Michael Somos_, Oct 17 2008",
			"references": 1,
			"revision": 10,
			"time": "2021-03-12T22:24:45-05:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}