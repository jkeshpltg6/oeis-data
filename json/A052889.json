{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A052889",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 52889,
			"data": "0,1,2,6,20,75,312,1421,7016,37260,211470,1275725,8142840,54776761,387022118,2863489830,22127336720,178162416499,1491567656472,12959459317021,116654844101140,1086207322942812,10447135955448522,103654461984288429,1059648140522024304",
			"name": "Number of rooted set partitions.",
			"comment": [
				"Total number of blocks of size one in all set partitions of set {1..n}. - _Wouter Meeussen_, Jul 28 2003",
				"With offset 1, number of permutations beginning with 12 and avoiding 12-3.",
				"a(n) = number of partitions of {1...n+1} containing exactly one pair of consecutive integers, counted within a block. With offset t-1, number of partitions of {1...N} containing one string of t consecutive integers, where N=n+j, t=2+j, j = 0,1,2,.... - _Augustine O. Munagi_, Apr 10 2005"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A052889/b052889.txt\"\u003eTable of n, a(n) for n = 0..575\u003c/a\u003e",
				"Adam M. Goyt and Lara K. Pudwell, \u003ca href=\"http://arxiv.org/abs/1203.3786\"\u003eAvoiding colored partitions of two elements in the pattern sense\u003c/a\u003e, arXiv preprint arXiv:1203.3786 [math.CO], 2012. - From _N. J. A. Sloane_, Sep 17 2012",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=865\"\u003eEncyclopedia of Combinatorial Structures 865\u003c/a\u003e",
				"S. Kitaev, \u003ca href=\"https://www.mat.univie.ac.at/~slc/wpapers/s48kitaev.html\"\u003eGeneralized pattern avoidance with additional restrictions\u003c/a\u003e, Sem. Lothar. Combinat. B48e (2003); arXiv:math/0205215 [math.CO].",
				"S. Kitaev and T. Mansour, \u003ca href=\"https://arxiv.org/abs/math/0205182\"\u003eSimultaneous avoidance of generalized patterns\u003c/a\u003e, arXiv:math/0205182 [math.CO], 2002.",
				"A. O. Munagi, \u003ca href=\"http://www.emis.de/journals/HOA/IJMMS/2005/3451.pdf\"\u003eSet Partitions with Successions and Separations\u003c/a\u003e, IJMMS 2005:3 (2005),451-463."
			],
			"formula": [
				"E.g.f.: exp(exp(x)-1)*x.",
				"a(n) = n*Bell(n-1). - _Vladeta Jovovic_, Sep 14 2003",
				"Let A be the upper Hessenberg matrix of order n defined by: A[i,i-1]=-1, A[i,j]=binomial(j-1,i-1), (i\u003c=j), and A[i,j]=0 otherwise. Then, for n\u003e=1, a(n)=(-1)^(n-1)*coeff(charpoly(A,x),x). - _Milan Janjic_, Jul 08 2010"
			],
			"example": [
				"a(3) = 6 because the partitions of {1, 2, 3, 4} containing a pair of consecutive integers are 124/3, 134/2, 14/23, 12/3/4, 1/23/4, 1/2/34."
			],
			"maple": [
				"spec := [S,{B=Set(C),C=Set(Z,1 \u003c= card),S=Prod(Z,B)},labeled]: seq(combstruct[count](spec,size=n), n=0..20);",
				"Explanation of above combstruct commands using generating functions, from _Mitch Harris_, Jul 28 2003:",
				"Z = an atom (each atom used is labeled), gf: Z(x) = x",
				"C = Set(Z, card \u003c= 1) is the set of positive integers; gf: C(x) = e^(Z(x)) - 1 = e^x - 1 (the -1 removes the empty set); [x^n]C = 1 means there is exactly one set with n atoms since each atom is labeled",
				"B = Set(C) the set of (ordered) sets of integers = ordered set partitions; gf: B(x) = e^C(x) = e^(e^x - 1)",
				"S = Prod(Z, B) pairs of an atom (Z) and an ordered set partition = an ordered set partition with an adjoining single atom. The adjoining atom corresponds to choosing a \"root\" in the partition; gf: S(x) = x B(x) = x*e^(e^x-1)",
				"A052889 := n -\u003e `if`(n=0,0,n*combinat[bell](n-1)):",
				"seq(A052889(n),n=0..20); # _Peter Luschny_, Apr 19 2011"
			],
			"mathematica": [
				"a=Exp[x]-1; Range[0, 20]! CoefficientList[Series[ x Exp[a], {x, 0, 20}], x] (* _Geoffrey Critzer_, Nov 25 2011 *)"
			],
			"xref": [
				"Second column of triangle A033306.",
				"Cf. A000110.",
				"Column k=1 of A175757."
			],
			"keyword": "easy,nonn",
			"offset": "0,3",
			"author": "encyclopedia(AT)pommard.inria.fr, Jan 25 2000",
			"references": 12,
			"revision": 59,
			"time": "2020-08-29T02:43:10-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}