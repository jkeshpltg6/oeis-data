{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A228667",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 228667,
			"data": "1,2,1,2,1,1,2,2,-2,-2,2,-3,3,2,-3,2,2,2,-3,3,-3,2,-3,3,-2,-2,2,-3,3,-3,3,2,-3,3,-3,2,2,2,-3,3,-3,3,-3,2,-3,3,-3,3,-2,-2,2,-3,3,-3,3,-3,3,2,-3,3,-3,3,-3,2,2,2,-3,3,-3,3,-3,3,-3,2,-3,3",
			"name": "Array:  row n shows the accelerated continued fraction of F(n+1)/F(n), where F = A000045 (Fibonacci numbers).",
			"comment": [
				"The accelerated continued fraction (ACF) of a positive rational number x/y, where GCD(x,y) = 1, is defined by the algorithm below.  The number of terms in ACF(x/y) is \u003c= the number of terms in the classical continued fraction CF(x/y).",
				"Step 1.  Put w = Mod[x,y].  If w=0, put c(0) = x/y and Stop.",
				"If 0 \u003c w \u003c= y/2, put c(0) = floor(x/y), u-\u003ey, v-\u003ew, f-\u003e1, go to step 2;",
				"if w\u003ey/2, put c(0) = 1 + floor(x/y), u-\u003ey, v-\u003ey - w, f-\u003e-1, go to step 2.",
				"For i\u003e=2, Step i is in 5 cases, as follows:",
				"Case 0.1:  f = 1 and w = 0.  Put w = Mod[x,y] and c(i) = u/v and Stop.",
				"Case 0.2:  f = -1 and w = 0.  Put w = Mod[x,y] and c(i) = -u/v and Stop.",
				"Case 1:  f = 1 and w \u003c= v/2.  Put w = Mod[x,y] and c(i) = floor(u/v), u-\u003ev, v-\u003ew, f-\u003e1, go to step i+1.",
				"Case 2:  f = 1 and w \u003e v/2.  Put w = Mod[x,y] and c(i) = 1 + floor(u/v), u-\u003ey, v-\u003ev - w, f-\u003e-1, go to step i+1.",
				"Case 3:  f = -1 and w \u003c= v/2.  Put w = Mod[x,y] and c(i) = -floor(u/v), u-\u003ev, v-\u003ew, f-\u003e-1, go to step i+1.",
				"Case 4:  f = -1 and w \u003e v/2.  Put w = Mod[x,y] and c(i) = -1 - floor(u/v), u-\u003ey, v-\u003ev - w, f-\u003e-f, go to step i+1.",
				"(End)"
			],
			"example": [
				"x/y ......... ACF(x/y)",
				"1/1 ......... 1",
				"2/1 ......... 2",
				"3/2 ......... 1,2",
				"5/3 ......... 1,1,2",
				"8/5 ......... 2,-2,-2",
				"13/8 ........ 2,-3,3",
				"21/13 ....... 2,-3,2,2",
				"34/21 ....... 2,-3,3,-3",
				"55/34 ....... 2,-3,3,-2,-2",
				"89/55 ....... 2,-3,3,-3,3"
			],
			"mathematica": [
				"$MaxExtraPrecision = Infinity; aCF[rational_] := Module[{steps = {}, stop = False, i = 0, x = Numerator[rational], y = Denominator[rational], w, u, v, f, c},(*Step 1*)w = Mod[x, y]; Which[w == 0, c[i] = x/y; stop = True; AppendTo[steps, \"A\"], 0 \u003c w \u003c= y/2, c[i] = Floor[x/y]; {u, v, f} = {y, w, 1}; AppendTo[steps, \"B\"], w \u003e y/2, c[i] = 1 + Floor[x/y]; {u, v, f} = {y, y - w, -1}; AppendTo[steps, \"C\"]];  i++; (*Step 2*)While[stop =!= True, w = Mod[u, v]; Which[f == 1 \u0026\u0026 w == 0, c[i] = u/v; stop = True; AppendTo[steps, \"0.1\"], f == -1 \u0026\u0026 w == 0, c[i] = -u/v; stop = True; AppendTo[steps, \"0.2\"], f == 1 \u0026\u0026 w \u003c= v/2, c[i] = Floor[u/v]; {u, v, f} = {v, w, 1}; AppendTo[steps, \"1\"], f == 1 \u0026\u0026 w \u003e v/2, c[i] = 1 + Floor[u/v]; {u, v, f} = {v, v - w, -1}; AppendTo[steps, \"2\"], f == -1 \u0026\u0026 w \u003c= v/2, c[i] = -Floor[u/v]; {u, v, f} = {v, w, -1}; AppendTo[steps, \"3\"], f == -1 \u0026\u0026 w \u003e v/2, c[i] = -1 - Floor[u/v]; {u, v, f} = {v, v - w, -f}; AppendTo[steps, \"4\"]]; i++];  (*Display results*){FromContinuedFraction[#], {\"Steps\", steps}, {\"ACF\", #}, {\"CF\", ContinuedFraction[x/y]}} \u0026[Map[c, Range[i] - 1]]]",
				"Table[aCF[Fibonacci[n + 1]/Fibonacci[n]], {n, 1, 20}]",
				"(* _Peter J. C. Moses_, Aug 28 2013 *)"
			],
			"xref": [
				"Cf. A000045, A228668, A228489."
			],
			"keyword": "tabf,easy,sign",
			"offset": "0,2",
			"author": "_Clark Kimberling_, Aug 29 2013",
			"references": 2,
			"revision": 14,
			"time": "2016-12-04T19:46:32-05:00",
			"created": "2013-09-03T16:26:14-04:00"
		}
	]
}