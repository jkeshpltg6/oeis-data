{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A298155",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 298155,
			"data": "1,6,5,28,3,2,11,4680,1,2,357,76,5,6,23,16,9,770,1,348,403,2,75,8,7,1998,1,340,1353,86,19,672,235,26,9,4,1,36570,7,88,3,2,295,2196,17,98,39,400,1943,114,11,8804,68985,2,1,24,1,790,3,364,1909,3366,185",
			"name": "For any n \u003e= 0 and k \u003e 0, the prime(k)-adic valuation of a(n) equals the prime(k)-adic valuation of n + k (where prime(k) denotes the k-th prime).",
			"comment": [
				"This sequence has similarities with A102370: here, for k \u003e 0, a(n) and n + k have the same prime(k)-adic valuation, there, for k \u003e= 0, A102370(n) and n + k have the same k-th binary digit (the least significant binary digit having index 0).",
				"For any positive number, say k, we can use the Chinese remainder theorem to find a term that is a multiple of k; this term has index \u003c k.",
				"a(n) is even iff n is odd.",
				"See A298161 for the indices of ones in the sequence."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A298155/b298155.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e"
			],
			"formula": [
				"For any n \u003e= 0:",
				"- a(n) = Product_{ k \u003e 0 } A000040(k)^A060175(n + k, k) (this product is well defined as only finitely many terms are \u003e 1),",
				"- A007814(a(n)) = A007814(n + 1),",
				"- A007949(a(n)) = A007949(n + 2),",
				"- A112765(a(n)) = A112765(n + 3),",
				"- A214411(a(n)) = A214411(n + 4),",
				"- gcd(n, a(n)) = 1.",
				"For any n \u003e 0:",
				"- a(A073605(n)) is a multiple of A002110(n)."
			],
			"example": [
				"For n = 7:",
				"- the 2-adic valuation of 7 + 1 is 3,",
				"- the 3-adic valuation of 7 + 2 is 2,",
				"- the 5-adic valuation of 7 + 3 is 1,",
				"- the 7-adic valuation of 7 + 4 is 0,",
				"- the 11-adic valuation of 7 + 5 is 0,",
				"- the 13-adic valuation of 7 + 6 is 1,",
				"- for k \u003e 6, the prime(k)-adic valuation of 7 + k is 0,",
				"- hence a(7) = 2^3 * 3^2 * 5^1 * 13^1 = 4680."
			],
			"maple": [
				"f:= proc(n) local v, p, k;",
				"  v:= 1: p:= 1:",
				"  for k from 1 do",
				"    p:= nextprime(p);",
				"    if p \u003e n+k then return v fi;",
				"    v:= v * p^padic:-ordp(n+k,p)",
				"  od",
				"end proc:",
				"map(f, [$0..100]); # _Robert Israel_, Jan 16 2018"
			],
			"mathematica": [
				"f[n_] := Module[{v = 1, p = 1, k}, For[k = 1, True, k++, p = NextPrime[p]; If[p \u003e n + k, Return[v]]; v *= p^IntegerExponent[n + k, p]]];",
				"f /@ Range[0, 100] (* _Jean-François Alcover_, Jul 30 2020, after Maple *)"
			],
			"program": [
				"(PARI) a(n) = my (v=1, k=0); forprime(p=1, oo, k++; if (n+k \u003c p, break); v *= p^valuation(n+k,p)); return (v)"
			],
			"xref": [
				"Cf. A000040, A002110, A007814, A007949, A060175, A073605, A102370, A112765, A214411, A298161."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Rémy Sigrist_, Jan 13 2018",
			"references": 2,
			"revision": 30,
			"time": "2020-07-30T07:18:00-04:00",
			"created": "2018-01-17T09:45:00-05:00"
		}
	]
}