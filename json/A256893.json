{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A256893",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 256893,
			"data": "1,0,1,0,3,1,0,13,9,1,0,75,79,18,1,0,541,765,265,30,1,0,4683,8311,3870,665,45,1,0,47293,100989,59101,13650,1400,63,1,0,545835,1362439,960498,278901,38430,2618,84,1,0,7087261,20246445,16700545,5844510,1012431,92610,4494,108,1",
			"name": "Exponential Riordan array [1, 1/(2-e^x)].",
			"comment": [
				"This is also the matrix product of the Stirling set numbers and the unsigned Lah numbers.",
				"This is also the Bell transform of A000670(n+1). For the definition of the Bell transform see A264428. - _Peter Luschny_, Jan 29 2016"
			],
			"formula": [
				"Row sums are given by A075729.",
				"T(n,1) = A000670(n) for n\u003e=1.",
				"T(n,k) = n!/k! * [x^n] (1/(2-exp(x))-1)^k. - _Alois P. Heinz_, Apr 17 2015"
			],
			"example": [
				"Number triangle starts:",
				"  1;",
				"  0,    1;",
				"  0,    3,     1;",
				"  0,   13,     9,     1;",
				"  0,   75,    79,    18,    1;",
				"  0,  541,   765,   265,   30,   1;",
				"  ..."
			],
			"maple": [
				"T:= (n, k)-\u003e n!*coeff(series((1/(2-exp(x))-1)^k/k!, x, n+1), x, n):",
				"seq(seq(T(n, k), k=0..n), n=0..10);  # _Alois P. Heinz_, Apr 17 2015",
				"# The function BellMatrix is defined in A264428.",
				"BellMatrix(n -\u003e polylog(-n-1, 1/2)/2, 9); # _Peter Luschny_, Jan 29 2016"
			],
			"mathematica": [
				"T[n_, k_] := n!*SeriesCoefficient[(1/(2 - Exp[x]) - 1)^k/k!, {x, 0, n}];",
				"Table[T[n, k], {n, 0, 10}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, May 23 2016, after _Alois P. Heinz_ *)",
				"(* The function BellMatrix is defined in A264428. *)",
				"BellMatrix[PolyLog[-#-1, 1/2]/2\u0026, 9] (* _Jean-François Alcover_, May 23 2016, after _Peter Luschny_ *)",
				"RiordanArray[d_, h_, n_] := RiordanArray[d, h, n, False];",
				"RiordanArray[d_Function|d_Symbol, h_Function|h_Symbol, n_, exp_:(True | False)] := Module[{M, td, th, k, m},",
				"M[_, _] = 0;",
				"td = PadRight[CoefficientList[d[x] + O[x]^n, x], n];",
				"th = PadRight[CoefficientList[h[x] + O[x]^n, x], n];",
				"For[k = 0, k \u003c= n - 1, k++, M[k, 0] = td[[k + 1]]];",
				"For[k = 1, k \u003c= n - 1, k++,",
				"  For[m = k, m \u003c= n - 1, m++,",
				"    M[m, k] = Sum[M[j, k - 1]*th[[m - j + 1]], {j, k - 1, m - 1}]]];",
				"If[exp,",
				"  u = 1;",
				"  For[k = 1, k \u003c= n - 1, k++,",
				"    u *= k;",
				"    For[m = 0, m \u003c= k, m++,",
				"      j = If[m == 0, u, j/m];",
				"      M[k, m] *= j]]];",
				"Table[M[m, k], {m, 0, n - 1}, {k, 0, m}]];",
				"RiordanArray[1\u0026, 1/(2 - Exp[#])\u0026, 10, True] // Flatten (* _Jean-François Alcover_, Jul 16 2019, after Sage program *)"
			],
			"program": [
				"(Sage)",
				"def riordan_array(d, h, n, exp=false):",
				"    def taylor_list(f,n):",
				"        t = SR(f).taylor(x,0,n-1).list()",
				"        return t + [0]*(n-len(t))",
				"    td = taylor_list(d,n)",
				"    th = taylor_list(h,n)",
				"    M = matrix(QQ,n,n)",
				"    for k in (0..n-1): M[k,0] = td[k]",
				"    for k in (1..n-1):",
				"        for m in (k..n-1):",
				"            M[m,k] = add(M[j,k-1]*th[m-j] for j in (k-1..m-1))",
				"    if exp:",
				"        u = 1",
				"        for k in (1..n-1):",
				"            u *= k",
				"            for m in (0..k):",
				"                j = u if m==0 else j/m",
				"                M[k,m] *= j",
				"    return M",
				"riordan_array(1, 1/(2-exp(x)), 8, exp=true)",
				"# As a matrix product:",
				"def Lah(n, k):",
				"    if n == k: return 1",
				"    if k\u003c0 or  k\u003en: return 0",
				"    return (k*n*gamma(n)^2)/(gamma(k+1)^2*gamma(n-k+1))",
				"matrix(ZZ, 8, stirling_number2)*matrix(ZZ, 8, Lah)"
			],
			"xref": [
				"Cf. A088729 which is a variant based on an (1,1)-offset of the number triangles.",
				"Cf. A131222 which is the matrix product of the unsigned Lah numbers and the Stirling cycle numbers.",
				"Cf. A000670, A075729."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Peter Luschny_, Apr 17 2015",
			"references": 67,
			"revision": 27,
			"time": "2019-07-19T11:45:46-04:00",
			"created": "2015-04-19T03:08:22-04:00"
		}
	]
}