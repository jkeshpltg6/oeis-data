{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A267483",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 267483,
			"data": "1,1,0,-1,1,1,1,2,-2,-3,1,1,0,-2,4,7,-4,-5,1,1,1,3,-6,-13,11,16,-6,-7,1,1,0,-3,9,22,-24,-40,22,29,-8,-9,1,1,1,4,-12,-34,46,86,-62,-91,37,46,-10,-11,1,1,0,-4,16,50,-80,-166,148,239,-128,-174,56,67,-12,-13,1,1,1,5,-20,-70,130,296,-314,-553,367,541,-230,-297,79,92,-14,-15,1,1",
			"name": "Triangle of coefficients of Gaussian polynomials [2n+3,2]_q represented as finite sum of terms (1+q^2)^k*q^(g-k), where k = 0,1,...,g with g=2n+1.",
			"comment": [
				"The entry a(n,k), n \u003e= 0, k = 0,1,...,g, where g=2n+1, of this irregular triangle is the coefficient of (1+q^2)^k*q^(g-k) in the representation of the Gaussian polynomial [2n+3,2]_q = Sum_{k=0..g) a(n,k)*(1+q^2)^k*q^(g-k).",
				"Row n is of length 2n+2.",
				"The sequence arises in the formal derivation of the stability polynomial B(x) = sum_{i=0..N} d_i T(iM,x) of rank N, and degree L, where T(iM,x) denotes the Chebyshev polynomial of the first kind (A053120) of degree iM. The coefficients d_i are determined by order conditions on the stability polynomial.",
				"Conjecture: More generally, the Gaussian polynomial [2*n+m+1-(m mod 2),m]_q = Sum_{k=0..g(m;n)} a(m;n,k)*(1+q^2)^k*q^(g(m;n)-k), for m \u003e= 0, n \u003e= 0, where g(m;n) = m*n if m is odd and (2*n+1)*m/2 if m is even, and the tabf array entries a(m;n,k) are the coefficients of the g.f. for the row n polynomials G(m;n,x) = (d^m/dt^m)G(m;n,t,x)/m!|_{t=0}, with G(m;n,t,x) = (1+t)*Product_{k=1..n+(m - m (mod 2))/2}(1 + t^2 + 2*t*T(k,x/2) (Chebyshev's T-polynomials). Hence a(m;n,k) = [x^k]G(m;n,x), for k=0..g(m;n). The present entry is the instance m = 2. (Thanks to _Wolfdieter Lang_ for clarifying the text on the general prescription of a(m;n,k).)"
			],
			"link": [
				"Stephen O'Sullivan, \u003ca href=\"/A267483/b267483.txt\"\u003eTable of n, a(n) for n = 0..991\u003c/a\u003e",
				"S. O'Sullivan, \u003ca href=\"http://dx.doi.org/10.1016/j.jcp.2015.07.050\"\u003eA class of high-order Runge-Kutta-Chebyshev stability polynomials\u003c/a\u003e, Journal of Computational Physics, 300 (2015), 665-678.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Gaussian_binomial_coefficient\"\u003eGaussian binomial coefficients\u003c/a\u003e."
			],
			"formula": [
				"G.f. for row polynomial: G(n,x) = (d^2/dt^2)((1+t)*Product_{i=1..n+1}(1+t^2+2t*T(i,x/2)))/2!|_{t=0}."
			],
			"example": [
				"1,1;",
				"0,-1,1,1;",
				"1,2,-2,-3,1,1;",
				"0,-2,4,7,-4,-5,1,1;",
				"1,3,-6,-13,11,16,-6,-7,1,1;",
				"0,-3,9,22,-24,-40,22,29,-8,-9,1,1;",
				"1,4,-12,-34,46,86,-62,-91,37,46,-10,-11,1,1;",
				"0,-4,16,50,-80,-166,148,239,-128,-174,56,67,-12,-13,1,1;",
				"1,5,-20,-70,130,296,-314,-553,367,541,-230,-297,79,92,-14,-15,1,1;"
			],
			"maple": [
				"A267483 := proc (n, k) local y: y := expand(subs(t = 0, diff((1+t)*product(1+t^2+2*t*ChebyshevT(i, x/2), i = 1 .. n+1),t$2)/2)): if k = 0 then subs(x = 0, y) else subs(x = 0, diff(y, x$k)/k!) end if: end proc: seq(seq(A267483(n, k), k = 0 .. 2*n+1), n = 0 .. 20);",
				"# More efficient:",
				"N:= 20: # to get rows 0 to N",
				"P[0]:= (1+t)*(t^2 + t*x + 1):",
				"B[0]:= 1+x:",
				"for n from 1 to N do",
				"  P[n]:= expand(series(P[n-1]*(1+t^2+2*t*orthopoly[T](n+1,x/2)),t,3));",
				"  B[n]:= coeff(P[n],t,2);",
				"od:",
				"seq(seq(coeff(B[n],x,j),j=0..2*n+1),n=0..N); # From A267120 entry by _Robert Israel_"
			],
			"mathematica": [
				"row[n_] := 1/2! D[(1+t)*Product[1+t^2+2*t*ChebyshevT[i, x/2], {i, 1, n+1}], {t, 2}] /. t -\u003e 0 // CoefficientList[#, x]\u0026; Table[row[n], {n, 0, 20}] // Flatten (* From A267120 entry by _Jean-François Alcover_ *)"
			],
			"xref": [
				"Cf. A053120, A267120, A267482, A267484, A267485, A267486."
			],
			"keyword": "sign,tabf",
			"offset": "0,8",
			"author": "_Stephen O'Sullivan_, Jan 15 2016",
			"references": 6,
			"revision": 34,
			"time": "2017-12-18T22:48:17-05:00",
			"created": "2016-02-12T12:00:42-05:00"
		}
	]
}