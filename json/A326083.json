{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A326083",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 326083,
			"data": "1,2,3,5,7,12,16,27,37,58,80,131,171,277,380,580,785,1250,1655,2616,3516,5344,7257,11353,14931,23204,31379,47511,63778,98681,130503,201357,270038,407429,548090,840171,1110429,1701872,2284325,3440337,4601656",
			"name": "Number of subsets of {1..n} containing all of their pairwise sums \u003c= n.",
			"comment": [
				"The summands are allowed to be equal. The case where they must be distinct is A326080. If A007865 counts sum-free sets, this sequence counts sum-closed sets. This is different from sum-full sets (A093971).",
				"From _Gus Wiseman_, Jul 08 2019: (Start)",
				"Also the number of subsets of {1..n} containing no sum of any multiset of the elements. For example, the a(0) = 1 through a(6) = 16 subsets are:",
				"  {}  {}   {}   {}     {}     {}       {}",
				"      {1}  {1}  {1}    {1}    {1}      {1}",
				"           {2}  {2}    {2}    {2}      {2}",
				"                {3}    {3}    {3}      {3}",
				"                {2,3}  {4}    {4}      {4}",
				"                       {2,3}  {5}      {5}",
				"                       {3,4}  {2,3}    {6}",
				"                              {2,5}    {2,3}",
				"                              {3,4}    {2,5}",
				"                              {3,5}    {3,4}",
				"                              {4,5}    {3,5}",
				"                              {3,4,5}  {4,5}",
				"                                       {4,6}",
				"                                       {5,6}",
				"                                       {3,4,5}",
				"                                       {4,5,6}",
				"(End)"
			],
			"formula": [
				"For n \u003e 0, a(n) = A103580(n) + 1."
			],
			"example": [
				"The a(0) = 1 through a(6) = 16 subsets:",
				"  {}  {}   {}     {}       {}         {}           {}",
				"      {1}  {2}    {2}      {3}        {3}          {4}",
				"           {1,2}  {3}      {4}        {4}          {5}",
				"                  {2,3}    {2,4}      {5}          {6}",
				"                  {1,2,3}  {3,4}      {2,4}        {3,6}",
				"                           {2,3,4}    {3,4}        {4,5}",
				"                           {1,2,3,4}  {3,5}        {4,6}",
				"                                      {4,5}        {5,6}",
				"                                      {2,4,5}      {2,4,6}",
				"                                      {3,4,5}      {3,4,6}",
				"                                      {2,3,4,5}    {3,5,6}",
				"                                      {1,2,3,4,5}  {4,5,6}",
				"                                                   {2,4,5,6}",
				"                                                   {3,4,5,6}",
				"                                                   {2,3,4,5,6}",
				"                                                   {1,2,3,4,5,6}",
				"The a(7) = 27 subsets:",
				"  {}  {4}  {36}  {246}  {2467}  {24567}  {234567}  {1234567}",
				"      {5}  {45}  {356}  {3467}  {34567}",
				"      {6}  {46}  {367}  {3567}",
				"      {7}  {47}  {456}  {4567}",
				"           {56}  {457}",
				"           {57}  {467}",
				"           {67}  {567}"
			],
			"mathematica": [
				"Table[Length[Select[Subsets[Range[n]],SubsetQ[#,Select[Plus@@@Tuples[#,2],#\u003c=n\u0026]]\u0026]],{n,0,10}]"
			],
			"xref": [
				"Cf. A007865, A050291, A051026, A054519, A085489, A093971, A103580, A120641, A151897, A326020, A326023, A326076, A326080."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Gus Wiseman_, Jun 05 2019",
			"references": 16,
			"revision": 13,
			"time": "2019-07-23T04:25:09-04:00",
			"created": "2019-06-07T07:46:03-04:00"
		}
	]
}