{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A256269",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 256269,
			"data": "1,-1,0,0,-1,0,0,0,0,4,-2,0,0,-2,0,0,-1,0,4,0,0,0,0,0,0,-3,0,0,0,0,0,0,0,0,-2,0,4,-2,0,0,-2,0,0,0,0,8,0,0,0,-1,0,0,-2,0,0,0,0,0,-2,0,0,-2,0,0,-1,0,0,0,0,0,0,0,4,-2,0,0,0,0,0,0,0,4",
			"name": "Expansion of psi(-q) * chi(q^3) * phi(q^9) in powers of q where phi(), psi(), chi() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A256269/b256269.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of eta(q) * eta(q^4) * eta(q^6)^2 * eta(q^18)^5 / (eta(q^2) * eta(q^3) * eta(q^12) * eta(q^9)^2 * eta(q^36)^2) in powers of q.",
				"Euler transform of period 36 sequence [ -1, 0, 0, -1, -1, -1, -1, -1, 2, 0, -1, -1, -1, 0, 0, -1, -1, -4, -1, -1, 0, 0, -1, -1, -1, 0, 2, -1, -1, -1, -1, -1, 0, 0, -1, -2, ...].",
				"a(3*n + 1) = - A122865(n). a(6*n + 4) = - A122856(n). a(3*n + 2) = a(4*n + 3) = a(9*n + 3) = a(9*n + 6) = 0."
			],
			"example": [
				"G.f. = 1 - q - q^4 + 4*q^9 - 2*q^10 - 2*q^13 - q^16 + 4*q^18 - 3*q^25 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 2, Pi/4, q^(1/2)] / (2^(1/2) q^(1/8)) QPochhammer[ -q^3, q^6] EllipticTheta[ 3, 0, q^9], {q, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, n==0, (-1)^(n%3) * (n%3\u003c2) * sumdiv(n, d, [ 0, 1, 2, -1][d%4 + 1] * if(d%9, 1, 4) * (-1)^((d%8==6) + n+d)))};",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A) * eta(x^4 + A) * eta(x^6 + A)^2 * eta(x^18 + A)^5 / (eta(x^2 + A) * eta(x^3 + A) * eta(x^12 + A) * eta(x^9 + A)^2 * eta(x^36 + A)^2), n))};",
				"(MAGMA) A := Basis( ModularForms( Gamma1(36), 1), 82); A[1] - A[2] - A[5] + 4*A[10] - 2*A[11] - 2*A[14] - A[17] + 4*A[19];"
			],
			"xref": [
				"Cf. A122856, A122865."
			],
			"keyword": "sign",
			"offset": "0,10",
			"author": "_Michael Somos_, Jun 01 2015",
			"references": 6,
			"revision": 16,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2015-06-01T18:49:50-04:00"
		}
	]
}