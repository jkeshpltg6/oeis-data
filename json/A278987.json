{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A278987",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 278987,
			"data": "0,1,0,1,1,0,1,1,1,0,1,4,1,1,0,1,11,4,1,1,0,1,26,11,4,1,1,0,1,57,41,11,4,1,1,0,1,120,162,41,11,4,1,1,0,1,247,610,162,41,11,4,1,1,0,1,502,2165,715,162,41,11,4,1,1,0,1,1013,7327,3425,715,162,41,11,4,1,1,0",
			"name": "Array read by antidiagonals downwards: T(b,n) = number of words of length n over an alphabet of size b that are in standard order and which have the property that every letter that appears in the word is repeated.",
			"comment": [
				"We study words made of letters from an alphabet of size b, where b \u003e= 1. We assume the letters are labeled {1,2,3,...,b}. There are b^n possible words of length n.",
				"We say that a word is in \"standard order\" if it has the property that whenever a letter i appears, the letter i-1 has already appeared in the word. This implies that all words begin with the letter 1."
			],
			"link": [
				"Joerg Arndt and N. J. A. Sloane, \u003ca href=\"/A278984/a278984.txt\"\u003eCounting Words that are in \"Standard Order\"\u003c/a\u003e"
			],
			"formula": [
				"The number of words of length n over an alphabet of size b that are in standard order and in which every symbol that appears in a word is repeated is Sum_{j = 1..b} A008299(n,j)."
			],
			"example": [
				"The array begins:",
				"0,.1,..1,...1,...1,...1,...1,....1..; b=1,",
				"0,.1,..4,...8,..16,..32,..64,..128..; b=2,",
				"0,.1,..4,..14,..41,.122,.365,.1094..; b=3,",
				"0,.1,..4,..14,..51,.187,.715,.2795..; b=4,",
				"0,.1,..4,..14,..51,.202,.855,.3845..; b=5,",
				"0,.1,..4,..14,..51,.202,.876,.4111..; b=6,",
				"...",
				"Rows b=1 through b=4 of the array are A000012, A000295 (or A130103), A278988, A278989."
			],
			"maple": [
				"with(combinat);",
				"A008299 := proc(n,k) local i,j,t1;",
				"if k\u003c1 or k\u003efloor(n/2) then t1:=0; else",
				"t1 := add( (-1)^i*binomial(n, i)*add( (-1)^j*(k - i - j)^(n - i)/(j!*(k - i - j)!), j = 0..k - i), i = 0..k); fi; t1; end;",
				"f3:=proc(L,b) global A008299; local i; add(A008299(L,i),i=1..b); end;",
				"Q3:=b-\u003e[seq(f3(L,b),L=1..40)];",
				"for b from 1 to 6 do lprint(Q3(b)); od:"
			],
			"xref": [
				"The words for b=9 are listed in A273978."
			],
			"keyword": "nonn,tabl",
			"offset": "1,12",
			"author": "_Joerg Arndt_ and _N. J. A. Sloane_, Dec 06 2016",
			"references": 4,
			"revision": 18,
			"time": "2016-12-06T20:44:07-05:00",
			"created": "2016-12-06T11:31:20-05:00"
		}
	]
}