{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A329126",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 329126,
			"data": "1,110,101010,111100,100010001000100010,1111110,10000010000010000010000010000010000010,11111111000,1010101010101010100,1100110011001100110,100000000010000000001000000000100000000010000000001000000000100000000010000000001000000000100000000010,11111111111100",
			"name": "a(n) is the least positive number which yields a multiple of n when its decimal digits (which are necessarily 0's and 1's) are read in any base.",
			"comment": [
				"a(n) might be called the \"smallest trans-basic multiple of n.\"",
				"In order to be a valid binary number, the terms may contain only 0's and 1's.",
				"The number of 1's in a(n) is conjectured to be n; the number of 0's separating each one digit is usually A268336(n)-1 for small n. The number of trailing 0's is A051903(n).",
				"The first 21 terms have been found and verified to be minimal via an advanced search; the 13th term (see b-file) contains 146 digits, and in general for every prime n the corresponding a(n) is conjectured to contain 2+(n-1)^2 0's and 1's.",
				"A lower bound for a(n) is given by a(A032742(n)). Proof: If a(n) were smaller than a(A032742(n)), then a(A032742(n)) would not be the smallest trans-basic multiple of A032742(n); a(n) would be. By definition a(n) is the smallest trans-basic multiple of n, so we have a contradiction; QED.",
				"To verify a trans-basic multiple of n for n \u003e 2, one must only: A) make sure the string has some multiple of n of '1' digits; B) make sure the string ends with at least one '0' digit; and C) check that, for all prime bases below n, the resulting number is divisible by n. If these three conditions are met, the string is a trans-basic multiple of n.",
				"While the formula given below is guaranteed to provide a trans-basic multiple of n, it does not always yield a(n) which by definition is the smallest such number. [Corrected by _M. F. Hasler_, Nov 14 2019]",
				"From _N. J. A. Sloane_, Nov 12 2019: (Start)",
				"For each n, the values of (string a(n) read in base b)/n for b = 1,2,3,... give a sequence of integers.",
				"For n=1 this is the all-1's sequence A000012.",
				"For n=2, a(2) = 110 which in base b is b+b^2. Divided by 2 we get (b+b^2)/2, which evaluated at b = 1,2,3,4,... is 1,3,6,10,..., the triangular numbers A000217.",
				"For n=3, we get (b+b^3+b^5)/3, which is A220892.",
				"For n=4, we get A328994. (End)",
				"See A329000 = (1, 6, 42, 60, 139810, 126, ...) for a(n) converted from base 2 to base 10, i.e., the numbers which yield the terms here when written in base 2. - _M. F. Hasler_, Nov 09 2021"
			],
			"link": [
				"Alon Ran, \u003ca href=\"/A329126/b329126.txt\"\u003eTable of n, a(n) for n = 1..21\u003c/a\u003e",
				"Alon Ran, \u003ca href=\"/A329126/a329126.txt\"\u003eComments on this sequence\u003c/a\u003e"
			],
			"formula": [
				"To generate an upper bound on a(n), start with n 1's (this is required to ensure that it is divisible by n in bases n+1, 2n+1, etc.)",
				"Next, place A268336(n)-1 0's in between the 1's (this ensures that the powers that are added will always sum to 0 (mod n)).",
				"Finally, add A051903(n) 0's on the right (this is to ensure that the number will be divisible by n in bases that are roots of factors of n).",
				"Note that this formula does not always yield the minimal solution a(n). For instance, a(10) is obtained from the above result by grouping the 1's in pairs and separating the pairs by two 0's.",
				"a(n) \u003c= A329338(n), with equality except for n = 10, 14, 15, ... - _M. F. Hasler_, Nov 14 2019"
			],
			"example": [
				"a(3) = 101010:",
				"  101010_2  =     42 =    14*3;",
				"  101010_3  =    273 =    91*3;",
				"  101010_4  =   1092 =   364*3;",
				"  101010_5  =   3255 =  1085*3;",
				"  101010_6  =   7998 =  2666*3;",
				"  101010_7  =  17157 =  5719*3;",
				"  101010_8  =  33288 = 11096*3;",
				"  101010_9  =  59787 = 19929*3;",
				"  101010_10 = 101010 = 33670*3;",
				"  101010_11 = 162393 = 54131*3;",
				"  101010_12 = 250572 = 83524*3;",
				"and so on. All the resulting values are multiples of 3."
			],
			"program": [
				"(PARI) see A329338 for an upper bound which equals a(n) in many cases, e.g., all n \u003c 14 except for n = 10. - _M. F. Hasler_, Nov 10 2021"
			],
			"xref": [
				"Cf. A000012, A000217, A051903, A220892, A268336, A329338 (an upper bound), A328994.",
				"A329000 gives a(n) read in base 2 and converted to base 10.",
				"See also A329443."
			],
			"keyword": "nonn,base,nice,more,changed",
			"offset": "1,2",
			"author": "_Alon Ran_, Nov 05 2019",
			"ext": [
				"I have weakened some of the assertions in the Comments section, since they seemed to be unproved. See Alon Ran's comments (see Links). - _N. J. A. Sloane_, Dec 02 2019",
				"Definition corrected, following a remark by _Don Reble_, by _M. F. Hasler_, Nov 09 2021",
				"The present definition has been reworded by _Peter Munn_, Nov 17 2021, and by _N. J. A. Sloane_, Nov 29 2021"
			],
			"references": 9,
			"revision": 119,
			"time": "2022-01-14T15:33:31-05:00",
			"created": "2019-11-11T22:26:51-05:00"
		}
	]
}