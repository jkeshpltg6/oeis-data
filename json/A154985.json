{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A154985",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 154985,
			"data": "1,1,1,1,6,1,1,17,17,1,1,38,154,38,1,1,79,872,872,79,1,1,160,3991,14064,3991,160,1,1,321,16791,157575,157575,16791,321,1,1,642,68312,1451486,4815630,1451486,68312,642,1,1,1283,274394,12266038,107115116,107115116,12266038,274394,1283,1",
			"name": "Triangle T(n, k, m) = coefficients of p(x, n, m) where p(x,n,m) = (x+1)*p(x, n-1, m) + (2^(m+n-1) + 2^(n-2)*[n\u003e=3])*x*p(x, n-2, m) and m=1, read by rows.",
			"comment": [
				"Row sums are: {1, 2, 8, 36, 232, 1904, 22368, 349376, 7856512, 239313664, 10534962688, ...}."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A154985/b154985.txt\"\u003eRows n = 0..50 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k, m) = coefficients of p(x, n, m) where p(x,n,m) = (x+1)*p(x, n-1, m) + (2^(m+n-1) + 2^(n-2)*[n\u003e=3])*x*p(x, n-2, m) and m=1.",
				"T(n, k, m) = T(n-1, k, m) + T(n-1, k-1, m) + (2^(n+m-1) + 2^(n-2)*[n\u003e=3])*T(n-2, k-1, m) with T(n, 0, m) = T(n, n, m) = 1 and m=1. - _G. C. Greubel_, Mar 01 2021"
			],
			"example": [
				"Triangle begins as:",
				"  1;",
				"  1,    1;",
				"  1,    6,      1;",
				"  1,   17,     17,        1;",
				"  1,   38,    154,       38,         1;",
				"  1,   79,    872,      872,        79,         1;",
				"  1,  160,   3991,    14064,      3991,       160,        1;",
				"  1,  321,  16791,   157575,    157575,     16791,      321,      1;",
				"  1,  642,  68312,  1451486,   4815630,   1451486,    68312,    642,    1;",
				"  1, 1283, 274394, 12266038, 107115116, 107115116, 12266038, 274394, 1283, 1;"
			],
			"mathematica": [
				"(* First program *)",
				"p[x_, n_, m_]:= p[x,n,m] = If[n\u003c2, n*x+1, (x+1)*p[x, n-1, m] + 2^(m+n-1)*x*p[x, n-2, m] + Boole[n\u003e=3]*2^(n-2)*x*p[x, n-2, m] ];",
				"Table[CoefficientList[ExpandAll[p[x,n,1]], x], {n,0,10}]//Flatten (* modified by _G. C. Greubel_, Mar 01 2021 *)",
				"(* Second program *)",
				"T[n_, k_, m_]:= T[n, k, m] = If[k==0 || k==n, 1, T[n-1, k, m] + T[n-1, k-1, m] +(2^(m+n-1) + Boole[n\u003e=3]*2^(n-2))*T[n-2, k-1, m] ];",
				"Table[T[n, k, 1], {n,0,12}, {k,0,n}]//Flatten (* _G. C. Greubel_, Mar 01 2021 *)"
			],
			"program": [
				"(Sage)",
				"def T(n,k,m):",
				"    if (k==0 or k==n): return 1",
				"    elif (n\u003c3): return T(n-1, k, m) + T(n-1, k-1, m) + 2^(n+m-1)*T(n-2, k-1, m)",
				"    else: return T(n-1, k, m) + T(n-1, k-1, m) + (2^(n+m-1) +2^(n-2))*T(n-2, k-1, m)",
				"flatten([[T(n,k,1) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, Mar 01 2021",
				"(Magma)",
				"function T(n,k,m)",
				"  if k eq 0 or k eq n then return 1;",
				"  elif (n lt 3) then return T(n-1, k, m) + T(n-1, k-1, m) + 2^(n+m-1)*T(n-2, k-1, m);",
				"  else return T(n-1, k, m) + T(n-1, k-1, m) + (2^(n+m-1)+2^(n-2))*T(n-2, k-1, m);",
				"  end if; return T;",
				"end function;",
				"[T(n,k,1): k in [0..n], n in [0..12]]; // _G. C. Greubel_, Mar 01 2021"
			],
			"xref": [
				"Cf. A154983 (m=0), this sequence (m=1), A154984 (m=2).",
				"Cf. A154979, A154980, A154982, A154986."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Roger L. Bagula_, Jan 18 2009",
			"ext": [
				"Edited by _G. C. Greubel_, Mar 01 2021"
			],
			"references": 4,
			"revision": 5,
			"time": "2021-03-01T21:51:27-05:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}