{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A245669",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 245669,
			"data": "1,3,3,1,0,3,6,3,3,6,6,3,0,6,6,1,6,9,6,0,0,12,12,3,7,6,9,6,0,12,6,3,6,6,12,3,0,12,12,6,6,12,18,6,0,12,12,3,7,15,12,0,0,9,12,6,12,18,6,6,0,18,18,1,12,12,18,6,0,12,12,9,12,18,15,6,0,18",
			"name": "Expansion of q * f(q, q^5)^3 in powers of q where f() is Ramanujan's two-variable theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A245669/b245669.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q * (chi(q) * psi(-q^3))^3 in powers of q where chi(), psi() are Ramanujan theta functions.",
				"Expansion of (eta(q^2)^2 * eta(q^3) * eta(q^12) / (eta(q) * eta(q^4) * eta(q^6)))^3 in powers of q.",
				"Euler transform of period 12 sequence [ 3, -3, 0, 0, 3, -3, 3, 0, 0, -3, 3, -3, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (12 t)) = 2^(3/2) (t/i)^(3/2) g(t) where q = exp(2 Pi i t) and g(t) is the g.f. for A245668.",
				"G.f.: x * (Sum_{k in Z} x^(3*k^2 + 2*k))^3.",
				"Convolution cube of A089801.",
				"a(2*n + 2) = A005885(n)."
			],
			"example": [
				"G.f. = q + 3*q^2 + 3*q^3 + q^4 + 3*q^6 + 6*q^7 + 3*q^8 + 3*q^9 + 6*q^10 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ ((EllipticTheta[ 3, 0, q^(1/3)] - EllipticTheta[ 3, 0, q^3]) / 2)^3, {q, 0, n}];",
				"a[ n_] := SeriesCoefficient[ (QPochhammer[ -q, q^2] EllipticTheta[ 2, Pi/4, q^(3/2)])^3 / (2^(3/2) q^(1/8)), {q, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = local(A); if( n\u003c1, 0, n--; A = x * O(x^n); polcoeff( (eta(x^2 + A)^2 * eta(x^3 + A) * eta(x^12 + A) / (eta(x + A) * eta(x^4 + A) * eta(x^6 + A)))^3, n))};"
			],
			"xref": [
				"Cf. A005885, A089801, A245668."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Michael Somos_, Jul 28 2014",
			"references": 2,
			"revision": 12,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2014-07-28T20:08:56-04:00"
		}
	]
}