{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A096920",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 96920,
			"data": "1,2,1,2,3,2,4,4,4,6,7,8,8,10,11,14,16,16,20,22,24,28,32,34,39,44,48,54,60,66,73,82,88,98,108,118,132,144,156,172,188,204,224,244,265,290,316,340,372,404,436,474,513,554,600,650,700,756,816,878,948,1022,1096,1182",
			"name": "Expansion of q^(-1/12) * eta(q^2)^4 / (eta(q)^2 * eta(q^4)) in powers of q.",
			"comment": [
				"Ramanujan theta functions: f(q) := Prod_{k\u003e=1} (1-(-q)^k) (see A121373), phi(q) := theta_3(q) := Sum_{k=-oo..oo} q^(k^2) (A000122), psi(q) := Sum_{k=0..oo} q^(k*(k+1)/2) (A010054), chi(q) := Prod_{k\u003e=0} (1+q^(2k+1)) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A096920/b096920.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"a(n) = b(n)+b(n-1)+b(n-3)+b(n-6)+...+b(n-k*(k+1)/2)+..., where b() is A000700(). E.g., a(8) = b(8)+b(7)+b(5)+b(2) = 2+1+1+0 = 4.",
				"G.f.: Product_{k\u003e0} (1 - x^(4*k)) * (1 + x^(2*k-1))^2. - _Michael Somos_, Mar 25 2008",
				"Expansion of psi(q) * chi(q) = f(q) / chi(-q) = f(q)^2 / phi(-q^2) = phi(-q^2) / chi(-q)^2 = phi(q) / chi(-q^2) = psi(q)^2 / f(-q^4) = f(-q^4) * chi(q)^2 where phi(), psi(), chi(), f() are Ramanujan theta functions.",
				"Euler transform of period 4 sequence [ 2, -2, 2, -1, ...]. - _Michael Somos_, Mar 25 2008",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (576 t)) = 12^(1/2) (t/i)^(1/2) g(t) where q = exp(2 Pi i t) and g() is g.f. for A138559.",
				"a(n) ~ exp(Pi*sqrt(n/6)) / (2*sqrt(2*n)). - _Vaclav Kotesovec_, Sep 07 2015"
			],
			"example": [
				"q + 2*q^13 + q^25 + 2*q^37 + 3*q^49 + 2*q^61 + 4*q^73 + 4*q^85 + 4*q^97 + ..."
			],
			"mathematica": [
				"nmax = 40; CoefficientList[Series[Product[(1 - x^(4*k)) * (1 + x^(2*k-1))^2, {k, 1, nmax}], {x, 0, nmax}], x] (* _Vaclav Kotesovec_, Sep 07 2015 *)",
				"eta[q_]:= q^(1/24)*QPochhammer[q]; a:= CoefficientList[Series[q^(-1/12) *eta[q^2]^4/(eta[q]^2*eta[q^4]), {q, 0, 50}], q]; Table[a[[n]], {n, 0, 50}] (* _G. C. Greubel_, May 09 2018 *)"
			],
			"program": [
				"(PARI) {a(n) = local(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A)^4 / eta(x + A)^2 / eta(x^4 + A), n))} /* _Michael Somos_, Mar 25 2008 */"
			],
			"keyword": "easy,nonn",
			"offset": "0,2",
			"author": "_Vladeta Jovovic_, Aug 18 2004",
			"references": 2,
			"revision": 18,
			"time": "2021-03-12T22:24:43-05:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}