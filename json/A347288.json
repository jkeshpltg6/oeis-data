{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A347288",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 347288,
			"data": "1,2,4,3,8,3,16,9,5,32,27,25,7,64,27,25,7,128,81,25,7,256,243,125,49,11,512,243,125,49,11,1024,729,625,343,121,13,2048,729,625,343,121,13,4096,2187,625,343,121,13,8192,6561,3125,2401,1331,169,17",
			"name": "Irregular triangle T(n,k) starting with 2^n followed by p_k^e_k = p_k^floor(log_p_k(p_(k-1)^e_(k-1))) such that e_k \u003e 0.",
			"comment": [
				"T(0,1) = 1 by convention.",
				"T(n,1) = 2^n. T(n,k) = p_k^e_k such that p_k^T(n,k) is the largest 1 \u003c p_k^e_k \u003c p_(k-1)^e_(k-1)."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A347288/b347288.txt\"\u003eTable of n, a(n) for n = 0..10367\u003c/a\u003e (rows 0 \u003c= n \u003c= 300, flattened)"
			],
			"formula": [
				"T(n,1) = 2^n; T(n,k) = p_k^floor(log_p_k(p_(k-1)^T(n,k-1))).",
				"A347385(n,k) = p_k^T(n,k).",
				"A089576(n) = row lengths.",
				"A347284(n) = product of row n."
			],
			"example": [
				"Row 0 contains {1} by convention.",
				"Row 1 contains {2} since no nonzero exponent e exists such that 3^e \u003c 2^1.",
				"Row 2 contains {4,3} since 3^1 \u003c 2^2 yet 3^2 \u003e 2^2. (We assume hereinafter that the powers listed are the largest possible smaller than the immediately previous term.)",
				"Row 3 contains {8,3} since 2^3 \u003e 3^1.",
				"Row 4 contains {16,9,5} since 2^4 \u003e 3^2 \u003e 5^1, etc.",
				"Triangle begins:",
				"         2      3      5      7     11    13    17  ...",
				"--------------------------------------------------",
				"0:       1",
				"1:       2",
				"2:       4      3",
				"3:       8      3",
				"4:      16      9      5",
				"5:      32     27     25      7",
				"6:      64     27     25      7",
				"7:     128     81     25      7",
				"8:     256    243    125     49     11",
				"9:     512    243    125     49     11",
				"10:   1024    729    625    343    121    13",
				"11:   2048    729    625    343    121    13",
				"12:   4096   2187    625    343    121    13",
				"13:   8192   6561   3125   2401   1331   169   17",
				"14:  16384   6561   3125   2401   1331   169   17",
				"..."
			],
			"mathematica": [
				"{{1}}~Join~Array[Most@ NestWhile[Block[{p = Prime[#2]}, Append[#1, p^Floor@ Log[p, #1[[-1]]]]] \u0026 @@ {#, Length@ # + 1} \u0026, {2^#}, #[[-1]] \u003e 1 \u0026] \u0026, 1 (* _Michael De Vlieger_, Aug 28 2021 *)"
			],
			"xref": [
				"Cf. A000217, A000961, A089576, A347284, A347285."
			],
			"keyword": "nonn,tabf,easy",
			"offset": "0,2",
			"author": "_Michael De Vlieger_, Aug 28 2021",
			"references": 2,
			"revision": 9,
			"time": "2021-09-25T14:54:55-04:00",
			"created": "2021-09-16T02:47:22-04:00"
		}
	]
}