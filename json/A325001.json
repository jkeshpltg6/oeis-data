{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A325001",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 325001,
			"data": "1,2,1,3,4,1,4,9,5,1,5,16,15,6,1,6,25,34,21,7,1,7,36,65,56,28,8,1,8,49,111,125,84,36,9,1,9,64,175,246,210,120,45,10,1,10,81,260,441,461,330,165,55,11,1,11,100,369,736,917,792,495,220,66,12,1",
			"name": "Array read by descending antidiagonals: A(n,k) is the number of achiral colorings of the facets (or vertices) of a regular n-dimensional simplex using up to k colors.",
			"comment": [
				"For n=1, the figure is a line segment with two vertices. For n=2, the figure is a triangle with three edges. For n=3, the figure is a tetrahedron with four triangular faces. The Schläfli symbol, {3,...,3}, of the regular n-dimensional simplex consists of n-1 threes. Each of its n+1 facets is a regular (n-1)-dimensional simplex. An achiral coloring is the same as its reflection."
			],
			"link": [
				"Robert A. Russell, \u003ca href=\"/A325001/b325001.txt\"\u003eTable of n, a(n) for n = 1..1275\u003c/a\u003e"
			],
			"formula": [
				"A(n,k) = binomial(n+k,n+1) - binomial(k,n+1).",
				"A(n,k) = Sum_{j=1..n} A325003(n,j) * binomial(k,j).",
				"A(n,k) = 2*A325000(n,k) - A324999(n,k) = A324999(n,k) - 2*A325000(n,k-n) = A325000(n,k) - A325000(n,k-n).",
				"G.f. for row n: (x - x^(n+1)) / (1-x)^(n+2).",
				"Linear recurrence for row n: A(n,k) = Sum_{j=1..n+1} -binomial(j-n-2,j) * A(n,k-j).",
				"G.f. for column k: (1 - (1-x^2)^k) / (x*(1-x)^k)."
			],
			"example": [
				"The array begins with A(1,1):",
				"  1  2  3   4   5    6    7     8     9    10    11     12     13 ...",
				"  1  4  9  16  25   36   49    64    81   100   121    144    169 ...",
				"  1  5 15  34  65  111  175   260   369   505   671    870   1105 ...",
				"  1  6 21  56 125  246  441   736  1161  1750  2541   3576   4901 ...",
				"  1  7 28  84 210  461  917  1688  2919  4795  7546  11452  16848 ...",
				"  1  8 36 120 330  792 1715  3424  6399 11320 19118  31032  48672 ...",
				"  1  9 45 165 495 1287 3003  6434 12861 24265 43593  75087 124683 ...",
				"  1 10 55 220 715 2002 5005 11440 24309 48610 92323 167740 293215 ...",
				"  ...",
				"For A(2,2)=4, the triangle may have 0, 1, 2, or 3 edges of one color."
			],
			"mathematica": [
				"Table[Binomial[d+1,n+1] - Binomial[d+1-n,n+1], {d,1,15}, {n,1,d}] // Flatten"
			],
			"xref": [
				"Cf. A324999 (oriented), A325000 (unoriented), A325000(n,k-n) (chiral), A325003 (exactly k colors), A327086 (edges, ridges), A337886 (faces, peaks), A325007 (orthotope facets, orthoplex vertices), A325015 (orthoplex facets, orthotope vertices).",
				"Rows 1-4 are A000027, A000290, A006003, A132366(n-1).",
				"Column 2 is A162880."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Robert A. Russell_, Mar 23 2019",
			"references": 11,
			"revision": 21,
			"time": "2020-10-20T08:17:16-04:00",
			"created": "2019-05-25T11:43:40-04:00"
		}
	]
}