{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A046051",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 46051,
			"data": "0,1,1,2,1,3,1,3,2,3,2,5,1,3,3,4,1,6,1,6,4,4,2,7,3,3,3,6,3,7,1,5,4,3,4,10,2,3,4,8,2,8,3,7,6,4,3,10,2,7,5,7,3,9,6,8,4,6,2,13,1,3,7,7,3,9,2,7,4,9,3,14,3,5,7,7,4,8,3,10,6,5,2,14,3,5,6,10,1,13,5,9,3,6,5,13,2,5,8",
			"name": "Number of prime factors of Mersenne number M(n) = 2^n - 1 (counted with multiplicity).",
			"comment": [
				"Length of row n of A001265."
			],
			"link": [
				"Sean A. Irvine, \u003ca href=\"/A046051/b046051.txt\"\u003eTable of n, a(n) for n = 1..1206\u003c/a\u003e (terms 1..500 from T. D. Noe)",
				"J. Brillhart et al., \u003ca href=\"http://dx.doi.org/10.1090/conm/022\"\u003eFactorizations of b^n +- 1\u003c/a\u003e, Contemporary Mathematics, Vol. 22, Amer. Math. Soc., Providence, RI, 3rd edition, 2002.",
				"Alex Kontorovich, Jeff Lagarias, \u003ca href=\"https://arxiv.org/abs/1808.03235\"\u003eOn Toric Orbits in the Affine Sieve\u003c/a\u003e, arXiv:1808.03235 [math.NT], 2018.",
				"R. Mestrovic, \u003ca href=\"http://arxiv.org/abs/1202.3670\"\u003eEuclid's theorem on the infinitude of primes: a historical survey of its proofs (300 BC--2012) and another new proof\u003c/a\u003e, arXiv preprint arXiv:1202.3670 [math.HO], 2012-2018.",
				"S. S. Wagstaff, Jr., \u003ca href=\"http://www.cerias.purdue.edu/homes/ssw/cun/index.html\"\u003eThe Cunningham Project\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/MersenneNumber.html\"\u003eMersenne Number\u003c/a\u003e"
			],
			"formula": [
				"Mobius transform of A085021. - _T. D. Noe_, Jun 19 2003",
				"a(n) = A001222(A000225(n)). - _Michel Marcus_, Jun 06 2019"
			],
			"example": [
				"a(4) = 2 because 2^4 - 1 = 15 = 3*5.",
				"From _Gus Wiseman_, Jul 04 2019: (Start)",
				"The sequence of Mersenne numbers together with their prime indices begins:",
				"        1: {}",
				"        3: {2}",
				"        7: {4}",
				"       15: {2,3}",
				"       31: {11}",
				"       63: {2,2,4}",
				"      127: {31}",
				"      255: {2,3,7}",
				"      511: {4,21}",
				"     1023: {2,5,11}",
				"     2047: {9,24}",
				"     4095: {2,2,3,4,6}",
				"     8191: {1028}",
				"    16383: {2,14,31}",
				"    32767: {4,11,36}",
				"    65535: {2,3,7,55}",
				"   131071: {12251}",
				"   262143: {2,2,2,4,8,21}",
				"   524287: {43390}",
				"  1048575: {2,3,3,5,11,13}",
				"(End)"
			],
			"maple": [
				"with(numtheory): P:=proc(n) local a,k; a:=ifactors(2^n-1)[2];",
				"add(a[k][2],k=1..nops(a)); end: seq(P(i),i=1..99); # _Paolo P. Lava_, Jul 18 2018"
			],
			"mathematica": [
				"a[q_] := Module[{x, n}, x=FactorInteger[2^n-1]; n=Length[x]; Sum[Table[x[i]][2]], {i, n}][j]], {j, n}]]",
				"a[n_Integer] := PrimeOmega[2^n - 1]; Table[a[n], {n,200}] (* _Vladimir Joseph Stephan Orlovsky_, Jul 22 2011 *)"
			],
			"program": [
				"(PARI) a(n)=bigomega(2^n-1) \\\\ _Charles R Greathouse IV_, Apr 01 2013"
			],
			"xref": [
				"Cf. A000043, A000668, A001348, A054988, A054989, A054990, A054991, A054992, A057951-A057958, A085021.",
				"Cf. A000225, A001221, A001222, A046800, A049093, A059305, A325610, A325611, A325612, A325625."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Eric W. Weisstein_",
			"references": 43,
			"revision": 59,
			"time": "2019-07-04T10:25:18-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}