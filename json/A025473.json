{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A025473",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 25473,
			"data": "1,2,3,2,5,7,2,3,11,13,2,17,19,23,5,3,29,31,2,37,41,43,47,7,53,59,61,2,67,71,73,79,3,83,89,97,101,103,107,109,113,11,5,127,2,131,137,139,149,151,157,163,167,13,173,179,181,191,193,197,199,211,223,227,229,233,239",
			"name": "a(1) = 1; for n \u003e 1, a(n) = prime root of n-th prime power (A000961).",
			"comment": [
				"This sequence is related to the cyclotomic sequences A013595 and A020500, leading to the procedure used in the Mathematica program. - _Roger L. Bagula_, Jul 08 2008",
				"From _Reinhard Zumkeller_, Jun 26 2011: (Start)",
				"A000961(n) = a(n)^A025474(n); A056798(n) = a(n)^(2*A025474(n));",
				"A192015(n) = A025474(n)*a(n)^(A025474(n)-1). (End)",
				"\"LCM numeral system\": a(n+1) is radix for index n, n \u003e= 0; a(-n+1) is 1/radix for index n, n \u003c 0. - _Daniel Forgues_, May 03 2014"
			],
			"reference": [
				"Paul J. McCarthy, Algebraic Extensions of Fields, Dover books, 1976, pages 40, 69"
			],
			"link": [
				"David Wasserman, \u003ca href=\"/A025473/b025473.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"OEIS Wiki, \u003ca href=\"/wiki/LCM_numeral_system\"\u003eLCM numeral system\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CyclotomicPolynomial.html\"\u003eCyclotomic Polynomial\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A006530(A000961(n)) = A020639(A000961(n)). - _David Wasserman_, Feb 16 2006"
			],
			"maple": [
				"cvm := proc(n, level) local f,opf; if n \u003c 2 then RETURN() fi;",
				"f := ifactors(n); opf := op(1,op(2,f)); if nops(op(2,f)) \u003e 1 or",
				"op(2,opf) \u003c= level then RETURN() fi; op(1,opf) end:",
				"A025473_list := n -\u003e [1,seq(cvm(i,0),i=1..n)];",
				"A025473_list(240); # _Peter Luschny_, Sep 21 2011"
			],
			"mathematica": [
				"a = Join[{1}, Flatten[Table[If[PrimeQ[Apply[Plus, CoefficientList[Cyclotomic[n, x], x]]], Apply[Plus, CoefficientList[Cyclotomic[n, x], x]], {}], {n, 1, 1000}]]] (* _Roger L. Bagula_, Jul 08 2008 *)",
				"Join[{1}, First@ First@# \u0026 /@ FactorInteger@ Select[Range@ 240, PrimePowerQ]] (* _Robert G. Wilson v_, Aug 17 2017 *)"
			],
			"program": [
				"(Sage)",
				"def A025473_list(n) :",
				"    R = [1]",
				"    for i in (2..n) :",
				"        if i.is_prime_power() :",
				"            R.append(prime_divisors(i)[0])",
				"    return R",
				"A025473_list(239) # _Peter Luschny_, Feb 07 2012",
				"(Haskell)",
				"a025473 = a020639 . a000961 -- _Reinhard Zumkeller_, Aug 14 2013",
				"(PARI) print1(1); for(n=2,1e3, if(isprimepower(n,\u0026p), print1(\", \"p))) \\\\ _Charles R Greathouse IV_, Apr 28 2014"
			],
			"xref": [
				"Cf. A013595, A020500, A025476."
			],
			"keyword": "easy,nonn,nice",
			"offset": "1,2",
			"author": "_David W. Wilson_, Dec 11 1999",
			"ext": [
				"Offset corrected by _David Wasserman_, Dec 22 2008"
			],
			"references": 29,
			"revision": 51,
			"time": "2017-08-28T11:49:14-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}