{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A307247",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 307247,
			"data": "0,0,0,1,1,1,2,0,0,0,1,1,1,2,0,0,0,0,0,0,1,1,1,2,0,0,0,1,1,1,2,0,0,0,0,0,0,1,1,1,2,0,0,0,1,1,1,2,0,0,0,1,1,1,2,0,0,0,0,0,0,1,1,1,2,0,0,0,1,1,1,2,0,0,0,0,0,0,1,1,1,2,0,0,0,1",
			"name": "Second digit in the expansion of n in Fraenkel's exotic ternary representation.",
			"comment": [
				"Let {p_i, i \u003e= 0} = {1,3,7,17,41,99,...} denote the numerators of successive convergents to sqrt(2) (see A001333). Then any n \u003e= 0 has a unique representation as n = Sum_{i \u003e= 0} d_i*p_i, with 0 \u003c= d_i \u003c= 2, d_{i+1}=2 =\u003e d_i=0. Sequence gives a(n+1) = d_1.",
				"Let x be the 3-symbol Pell word A294180 = 1, 2, 3, 1, 2, 3, 1, 1, 2, 3, 1, 2, ...  Let delta be the morphism",
				"      1 -\u003e 000, 2 -\u003e 111, 3 -\u003e 2.",
				"Then delta(x) = (a(n)). This can be proved by induction, starting from the knowledge that the sequence of first digits d_0 = d_0(n) of n in the exotic ternary expansion shifted by 1 is equal to x (see A263844).",
				"More generally, the sequence of k-th digits d_k shifted by 1 is equal to delta_k(x), where the morphism delta_k is given by",
				"      1 -\u003e U_k, 2 -\u003e V_k, 3 -\u003e W_k.",
				"Here U_k is a concatenation of p_{k+1} letters 0, V_k is a concatenation of p_{k+1} letters 1, and W_k is a concatenation of p_k letters 2."
			],
			"link": [
				"Michel Dekking, \u003ca href=\"/A307247/b307247.txt\"\u003eTable of n, a(n) for n = 1..5000\u003c/a\u003e (restored by _Georg Fischer_, Apr 05 2019)",
				"F. Michel Dekking, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL19/Dekking/dekk4.html\"\u003eMorphisms, Symbolic Sequences, and Their Standard Forms\u003c/a\u003e, Journal of Integer Sequences, Vol. 19 (2016), Article 16.1.1.",
				"Aviezri S. Fraenkel, \u003ca href=\"http://dx.doi.org/10.1016/S0012-365X(00)00138-2\"\u003eOn the recurrence f(m+1)= b(m)*f(m)-f(m-1) and applications\u003c/a\u003e, Discrete Mathematics 224 (2000), pp. 273-279.",
				"A. S. Fraenkel, \u003ca href=\"/A263844/a263844.png\"\u003eAn exotic ternary representation of the first few positive integers\u003c/a\u003e (Table 2 from Fraenkel (2000).)"
			],
			"xref": [
				"Cf. A263844, A001333."
			],
			"keyword": "nonn",
			"offset": "1,7",
			"author": "_Michel Dekking_, Apr 01 2019",
			"references": 1,
			"revision": 34,
			"time": "2019-04-05T17:42:57-04:00",
			"created": "2019-04-04T23:13:42-04:00"
		}
	]
}