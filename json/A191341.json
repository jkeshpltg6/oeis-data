{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A191341",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 191341,
			"data": "3,11,51,227,963,3971,16131,65027,261123,1046531,4190211,16769027,67092483,268402691,1073676291,4294836227,17179607043,68718952451,274876858371,1099509530627,4398042316803,17592177655811,70368727400451,281474943156227,1125899839733763",
			"name": "a(n) = 4^n - 2*2^n + 3.",
			"comment": [
				"Also the number of dominating sets for the complete bipartite graph K_{n,n}. - _Eric W. Weisstein_, Apr 24 2017",
				"For n \u003e 1, a(n) is the largest integer such that the binary representations of a(n)-1 and a(n)+1 both contain exactly n 0's and n 1's."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A191341/b191341.txt\"\u003eTable of n, a(n) for n = 1..500\u003c/a\u003e (corrected by Ray Chandler, Jan 19 2019)",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/DominatingSet.html\"\u003eDominating Set\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CompleteBipartiteGraph.html\"\u003eComplete Bipartite Graph\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (7,-14,8)."
			],
			"formula": [
				"a(n) = 4^n - 2^(n+1) + 3. - _Nathaniel Johnston_, May 30 2011",
				"a(n) = 7*a(n-1) - 14*a(n-2) + 8*a(n-3). - _Eric W. Weisstein_, Jun 29 2017",
				"G.f.: (3 -10*x +16*x^2)/((1-x)*(1-2*x)*(1-4*x)). - _R. J. Mathar_, Jun 02 2011 ( corrected by _G. C. Greubel_, Feb 10 2019 )",
				"E.g.f.: -2 + 3*exp(x) - 2*exp(2*x) + exp(4*x). - _G. C. Greubel_, Feb 10 2019"
			],
			"mathematica": [
				"Table[3 - 2^(1+n) + 4^n, {n, 20}] (* _Eric W. Weisstein_, Apr 24 2017 *)",
				"With[{r = Range[10^5]}, 2 + SplitBy[Cases[Transpose[{Partition[Tally[#][[All, 2]] \u0026 /@ IntegerDigits[r, 2], 2, 1, 1], r}], {{{n_, n_}, {n_, n_}}, p_} :\u003e {n, p}], First][[All, -1, -1]]] (* _Eric W. Weisstein_, Apr 24 2017 *)",
				"LinearRecurrence[{7, -14, 8}, {3, 11, 51}, 20] (* _Eric W. Weisstein_, Jun 29 2017 *)",
				"CoefficientList[Series[(3-10x+16x^2)/((1-x)(1-2x)(1-4x)), {x, 0, 20}], x] (* _Eric W. Weisstein_, Jun 29 2017 *)"
			],
			"program": [
				"(PARI) a(n)=4^n-2*2^n+3 \\\\ _Charles R Greathouse IV_, Jun 08 2011",
				"(MAGMA) [4^n - 2^(n+1) + 3: n in [1..30]]; // _Vincenzo Librandi_, Jun 09 2011",
				"(Sage) [(2^n-1)^2+2 for n in (1..30)] # _G. C. Greubel_, Feb 10 2019",
				"(GAP) List([1..30], n -\u003e (2^n-1)^2+2); # _G. C. Greubel_, Feb 10 2019"
			],
			"xref": [
				"Cf. A031443 (digitally balanced numbers), A191292, A191296."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_Juri-Stepan Gerasimov_, May 30 2011",
			"ext": [
				"Definition changed to closed-form formula and original definition clarified and moved to comment by _Eric W. Weisstein_, Apr 24 2017"
			],
			"references": 4,
			"revision": 45,
			"time": "2019-02-11T03:01:05-05:00",
			"created": "2011-05-31T16:37:11-04:00"
		}
	]
}