{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002893",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2893,
			"id": "M2998 N1214",
			"data": "1,3,15,93,639,4653,35169,272835,2157759,17319837,140668065,1153462995,9533639025,79326566595,663835030335,5582724468093,47152425626559,399769750195965,3400775573443089,29016970072920387,248256043372999089",
			"name": "a(n) = Sum_{k=0..n} binomial(n,k)^2 * binomial(2*k,k).",
			"comment": [
				"This is the Taylor expansion of a special point on a curve described by Beauville. - _Matthijs Coster_, Apr 28 2004",
				"a(n) is the (2n)th moment of the distance from the origin of a 3-step random walk in the plane. - Peter M. W. Gill (peter.gill(AT)nott.ac.uk), Feb 27 2004",
				"a(n) is the number of Abelian squares of length 2n over a 3-letter alphabet. - _Jeffrey Shallit_, Aug 17 2010",
				"Consider 2D simple random walk on honeycomb lattice. a(n) gives number of paths of length 2n ending at origin. - _Sergey Perepechko_, Feb 16 2011",
				"Row sums of A318397 the square of A008459. - _Peter Bala_, Mar 05 2013",
				"Conjecture: For each n=1,2,3,... the polynomial g_n(x) = Sum_{k=0..n, binomial(n,k)^2*binomial(2k,k)*x^k is irreducible over the field of rational numbers. - _Zhi-Wei Sun_, Mar 21 2013",
				"This is one of the Apery-like sequences - see Cross-references. - _Hugo Pfoertner_, Aug 06 2017",
				"a(n) is the sum of the squares of the coefficients of (x + y + z)^n. - _Michael Somos_, Aug 25 2018",
				"a(n) is the constant term in the expansion of (1 + (1 + x) * (1 + y) + (1 + 1/x) * (1 + 1/y))^n. - _Seiichi Manyama_, Oct 28 2019"
			],
			"reference": [
				"Matthijs Coster, Over 6 families van krommen [On 6 families of curves], Master's Thesis (unpublished), Aug 26 1983.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A002893/b002893.txt\"\u003eTable of n, a(n) for n = 0..1051\u003c/a\u003e (terms 0..100 from T. D. Noe)",
				"B. Adamczewski, J. P. Bell, and E. Delaygue, \u003ca href=\"https://arxiv.org/abs/1603.04187\"\u003eAlgebraic independence of G-functions and congruences à la Lucas\"\u003c/a\u003e, arXiv preprint arXiv:1603.04187 [math.NT], 2016.",
				"David H. Bailey, Jonathan M. Borwein, David Broadhurst and M. L. Glasser, \u003ca href=\"http://arxiv.org/abs/0801.0891\"\u003eElliptic integral evaluations of Bessel moments\u003c/a\u003e, arXiv:0801.0891 [hep-th], 2008.",
				"P. Barrucand, \u003ca href=\"http://dx.doi.org/10.1137/1017013\"\u003eA combinatorial identity, Problem 75-4\u003c/a\u003e, SIAM Rev., 17 (1975), 168. \u003ca href=\"http://dx.doi.org/10.1137/1018056\"\u003eSolution\u003c/a\u003e by D. R. Breach, D. McCarthy, D. Monk and P. E. O'Neil, SIAM Rev. 18 (1976), 303.",
				"P. Barrucand, \u003ca href=\"/A002893/a002893.pdf\"\u003eProblem 75-4, A Combinatorial Identity\u003c/a\u003e, SIAM Rev., 17 (1975), 168. [Annotated scanned copy of statement of problem]",
				"Arnaud Beauville, \u003ca href=\"http://gallica.bnf.fr/ark:/12148/bpt6k5543443c/f31.item\"\u003eLes familles stables de courbes sur P_1 admettant quatre fibres singulières\u003c/a\u003e, Comptes Rendus, Académie Sciences Paris, no. 294, May 24 1982.",
				"Jonathan M. Borwein, \u003ca href=\"https://www.carma.newcastle.edu.au/jon/beauty.pdf\"\u003eA short walk can be beautiful\u003c/a\u003e, 2015.",
				"Jonathan M. Borwein, Dirk Nuyens, Armin Straub and James Wan, \u003ca href=\"http://www.carma.newcastle.edu.au/~jb616/walks.pdf\"\u003eRandom Walk Integrals\u003c/a\u003e, 2010.",
				"Jonathan M. Borwein and Armin Straub, \u003ca href=\"http://carma.newcastle.edu.au/~jb616/wmi-paper.pdf\"\u003eMahler measures, short walks and log-sine integrals\u003c/a\u003e.",
				"Jonathan M. Borwein, Armin Straub and Christophe Vignat, \u003ca href=\"http://www.carma.newcastle.edu.au/jon/dwalks.pdf\"\u003eDensities of short uniform random walks, Part II: Higher dimensions\u003c/a\u003e, Preprint, 2015",
				"Jonathan M. Borwein, Armin Straub and James Wan, \u003ca href=\"http://dx.doi.org/10.1080/10586458.2013.748379\"\u003eThree-Step and Four-Step Random Walk Integrals\u003c/a\u003e, Exper. Math., 22 (2013), 1-14.",
				"Charles Burnette and Chung Wong, \u003ca href=\"https://arxiv.org/abs/1609.05580\"\u003eAbelian Squares and Their Progenies\u003c/a\u003e, arXiv:1609.05580 [math.CO], 2016.",
				"David Callan, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL11/Callan2/callan204.html\"\u003eA combinatorial interpretation for an identity of Barrucand\u003c/a\u003e, JIS 11 (2008) 08.3.4",
				"M. Coster, \u003ca href=\"/A001850/a001850_1.pdf\"\u003eEmail, Nov 1990\u003c/a\u003e",
				"Eric Delaygue, \u003ca href=\"http://arxiv.org/abs/1310.4131\"\u003eArithmetic properties of Apery-like numbers\u003c/a\u003e, arXiv preprint arXiv:1310.4131 [math.NT], 2013.",
				"C. Domb, \u003ca href=\"http://dx.doi.org/10.1080/00018736000101199\"\u003eOn the theory of cooperative phenomena in crystals\u003c/a\u003e, Advances in Phys., 9 (1960), 149-361.",
				"Jeffrey S. Geronimo, Hugo J. Woerdeman, and Chung Y. Wong, \u003ca href=\"https://arxiv.org/abs/2101.00525\"\u003eThe autoregressive filter problem for multivariable degree one symmetric polynomials\u003c/a\u003e, arXiv:2101.00525 [math.CA], 2021.",
				"Ofir Gorodetsky, \u003ca href=\"https://arxiv.org/abs/2102.11839\"\u003eNew representations for all sporadic Apéry-like sequences, with applications to congruences\u003c/a\u003e, arXiv:2102.11839 [math.NT], 2021. See C p. 2.",
				"Victor J. W. Guo, \u003ca href=\"http://arxiv.org/abs/1201.0617\"\u003eProof of two conjectures of Z.-W. Sun on congruences for Franel numbers\u003c/a\u003e, arXiv preprint arXiv:1201.0617 [math.NT], 2012.",
				"Victor J. W. Guo, Guo-Shuai Mao and Hao Pan, \u003ca href=\"http://arxiv.org/abs/1511.04005\"\u003eProof of a conjecture involving Sun polynomials\u003c/a\u003e, arXiv preprint arXiv:1511.04005 [math.NT], 2015.",
				"E. Hallouin and M. Perret, \u003ca href=\"http://arxiv.org/abs/1503.06591\"\u003eA Graph Aided Strategy to Produce Good Recursive Towers over Finite Fields\u003c/a\u003e, arXiv preprint arXiv:1503.06591 [math.NT], 2015.",
				"J. A. Hendrickson, Jr., \u003ca href=\"http://dx.doi.org/10.1080/00949659508811639\"\u003eOn the enumeration of rectangular (0,1)-matrices\u003c/a\u003e, Journal of Statistical Computation and Simulation, 51 (1995), 291-313.",
				"Tanya Khovanova and Konstantin Knop, \u003ca href=\"http://arxiv.org/abs/1409.0250\"\u003eCoins of three different weights\u003c/a\u003e, arXiv:1409.0250 [math.HO], 2014.",
				"Murray S. Klamkin, ed., \u003ca href=\"http://dx.doi.org/10.1137/1.9781611971729\"\u003eProblems in Applied Mathematics: Selections from SIAM Review\u003c/a\u003e, SIAM, 1990; see pp. 148-149.",
				"Amita Malik and Armin Straub, \u003ca href=\"https://doi.org/10.1007/s40993-016-0036-8\"\u003eDivisibility properties of sporadic Apéry-like numbers\u003c/a\u003e, Research in Number Theory, 2016, 2:5",
				"Mathematics.StackExchange, \u003ca href=\"http://math.stackexchange.com/questions/2006632/sum-involving-the-product-of-binomial-coefficients\"\u003esum involving the product of binomial coefficients\u003c/a\u003e, Nov 10 2016.",
				"L. B. Richmond and Jeffrey Shallit, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v16i1r72\"\u003eCounting abelian squares\u003c/a\u003e, Electronic J. Combinatorics 16 (1), #R72, June 2009. [From _Jeffrey Shallit_, Aug 17 2010]",
				"Armin Straub, \u003ca href=\"http://arminstraub.com/pub/dissertation\"\u003eArithmetic aspects of random walks and methods in definite integration\u003c/a\u003e, Ph. D. Dissertation, School Of Science And Engineering, Tulane University, 2012. - From _N. J. A. Sloane_, Dec 16 2012",
				"Zhi-Hong Sun, \u003ca href=\"https://arxiv.org/abs/1803.10051\"\u003eCongruences for Apéry-like numbers\u003c/a\u003e, arXiv:1803.10051 [math.NT], 2018.",
				"Zhi-Hong Sun, \u003ca href=\"https://arxiv.org/abs/2004.07172\"\u003eNew congruences involving Apéry-like numbers\u003c/a\u003e, arXiv:2004.07172 [math.NT], 2020.",
				"Zhi-Wei Sun, \u003ca href=\"http://math.nju.edu.cn/~zwsun/150f.pdf\"\u003eConnections between p = x^2+3y^2 and Franel numbers\u003c/a\u003e, J. Number Theory 133(2013), 2919-2928.",
				"Zhi-Wei Sun, \u003ca href=\"http://dx.doi.org/10.1007/s11139-015-9727-3\"\u003eCongruences involving g_n(x)=sum_{k=0..n}binom(n,k)^2*binom(2k,k)*x^k\u003c/a\u003e, Ramanujan J., in press. Doi: 10.1007/s11139-015-9727-3.",
				"Brani Vidakovic, \u003ca href=\"https://www.jstor.org/stable/2684723\"\u003eAll roads lead to Rome--even in the honeycomb world\u003c/a\u003e, Amer. Statist., 48 (1994) no. 3, 234-236.",
				"Yi Wang and Bao-Xuan Zhu, \u003ca href=\"http://arxiv.org/abs/1303.5595\"\u003eProofs of some conjectures on monotonicity of number-theoretic and combinatorial sequences\u003c/a\u003e, arXiv preprint arXiv:1303.5595 [math.CO], 2013.",
				"D. Zagier, \u003ca href=\"http://people.mpim-bonn.mpg.de/zagier/files/tex/AperylikeRecEqs/fulltext.pdf\"\u003eIntegral solutions of Apery-like recurrence equations\u003c/a\u003e. See line C in sporadic solutions table of page 5."
			],
			"formula": [
				"a(n) = Sum_{m=0..n} binomial(n, m) * A000172(m). [Barrucand]",
				"D-finite with recurrence: (n+1)^2 a(n+1) = (10*n^2+10*n+3) * a(n) - 9*n^2 * a(n-1). - _Matthijs Coster_, Apr 28 2004",
				"Sum_{n\u003e=0} a(n)*x^n/n!^2 = BesselI(0, 2*sqrt(x))^3. - _Vladeta Jovovic_, Mar 11 2003",
				"a(n) = Sum_{p+q+r=n} (n!/(p!*q!*r!))^2 with p, q, r \u003e= 0. - _Michael Somos_, Jul 25 2007",
				"a(n) = 3*A087457(n) for n\u003e0. - _Philippe Deléham_, Sep 14 2008",
				"a(n) = hypergeom([1/2, -n, -n], [1, 1], 4). - _Mark van Hoeij_, Jun 02 2010",
				"G.f.: 2*sqrt(2)/Pi/sqrt(1-6*z-3*z^2+sqrt((1-z)^3*(1-9*z))) *  EllipticK(8*z^(3/2)/(1-6*z-3*z^2+sqrt((1-z)^3*(1-9*z)))). - _Sergey Perepechko_, Feb 16 2011",
				"G.f.: Sum_{n\u003e=0} (3*n)!/n!^3 * x^(2*n)*(1-x)^n / (1-3*x)^(3*n+1). - _Paul D. Hanna_, Feb 26 2012",
				"Asymptotic: a(n) ~ 3^(2*n+3/2)/(4*Pi*n). - _Vaclav Kotesovec_, Sep 11 2012",
				"G.f.: 1/(1-3*x)*(1-6*x^2*(1-x)/(Q(0)+6*x^2*(1-x))), where Q(k)= (54*x^3 - 54*x^2 + 9*x -1)*k^2 + (81*x^3 - 81*x^2 + 18*x -2)*k + 33*x^3 - 33*x^2 +9*x - 1 - 3*x^2*(1-x)*(1-3*x)^3*(k+1)^2*(3*k+4)*(3*k+5)/Q(k+1) ; (continued fraction). - _Sergei N. Gladkovskii_, Jul 16 2013",
				"G.f.: G(0)/(2*(1-9*x)^(2/3) ), where G(k)= 1 + 1/(1 - 3*(3*k+1)^2*x*(1-x)^2/(3*(3*k+1)^2*x*(1-x)^2 - (k+1)^2*(1-9*x)^2/G(k+1) )); (continued fraction). - _Sergei N. Gladkovskii_, Jul 31 2013",
				"a(n) = [x^(2n)] 1/agm(sqrt((1-3*x)*(1+x)^3), sqrt((1+3*x)*(1-x)^3)). - _Gheorghe Coserea_, Aug 17 2016",
				"0 = +a(n)*(+a(n+1)*(+729*a(n+2) -1539*a(n+3) +243*a(n+4)) +a(n+2)*(-567*a(n+2) +1665*a(n+3) -297*a(n+4)) +a(n+3)*(-117*a(n+3) +27*a(n+4))) +a(n+1)*(+a(n+1)*(-324*a(n+2) +720*a(n+3) -117*a(n+4)) +a(n+2)*(+315*a(n+2) -1000*a(n+3) +185*a(n+4)) +a(n+3)*(+80*a(n+3) -19*a(n+4))) +a(n+2)*(+a(n+2)*(-9*a(n+2) +35*a(n+3) -7*a(n+4)) +a(n+3)*(-4*a(n+3) +a(n+4))) for all n in Z. - _Michael Somos_, Oct 30 2017",
				"G.f. y=A(x) satisfies: 0 = x*(x - 1)*(9*x - 1)*y'' + (27*x^2 - 20*x + 1)*y' + 3*(3*x - 1)*y. - _Gheorghe Coserea_, Jul 01 2018"
			],
			"example": [
				"G.f.: A(x) = 1 + 3*x + 15*x^2 + 93*x^3 + 639*x^4 + 4653*x^5 + 35169*x^6 + ...",
				"G.f.: A(x) = 1/(1-3*x) + 6*x^2*(1-x)/(1-3*x)^4 + 90*x^4*(1-x)^2/(1-3*x)^7 + 1680*x^6*(1-x)^3/(1-3*x)^10 + 34650*x^8*(1-x)^4/(1-3*x)^13 + ... - _Paul D. Hanna_, Feb 26 2012"
			],
			"maple": [
				"series(1/GaussAGM(sqrt((1-3*x)*(1+x)^3), sqrt((1+3*x)*(1-x)^3)), x=0, 42) # _Gheorghe Coserea_, Aug 17 2016",
				"A002893 := n -\u003e hypergeom([1/2, -n, -n], [1, 1], 4):",
				"seq(simplify(A002893(n)), n=0..20); # _Peter Luschny_, May 23 2017"
			],
			"mathematica": [
				"Table[Sum[Binomial[n,k]^2 Binomial[2k,k],{k,0,n}],{n,0,20}] (* _Harvey P. Dale_, Aug 19 2011 *)",
				"a[ n_] := If[ n \u003c 0, 0, HypergeometricPFQ[ {1/2, -n, -n}, {1, 1}, 4]]; (* _Michael Somos_, Oct 16 2013 *)",
				"a[n_] := SeriesCoefficient[BesselI[0, 2*Sqrt[x]]^3, {x, 0, n}]*n!^2; Table[a[n], {n, 0, 20}] (* _Jean-François Alcover_, Dec 30 2013 *)",
				"a[ n_] := If[ n \u003c 0, 0, Block[ {x, y, z},  Expand[(x + y + z)^n] /. {t_Integer -\u003e t^2, x -\u003e 1, y -\u003e 1, z -\u003e 1}]]; (* _Michael Somos_, Aug 25 2018 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, 0, n!^2 * polcoeff( besseli(0, 2*x + O(x^(2*n+1)))^3, 2*n))};",
				"(PARI) {a(n) = sum(k=0, n, binomial(n, k)^2 * binomial(2*k, k))}; /* _Michael Somos_, Jul 25 2007 */",
				"(PARI) {a(n)=polcoeff(sum(m=0,n, (3*m)!/m!^3 * x^(2*m)*(1-x)^m / (1-3*x+x*O(x^n))^(3*m+1)),n)} \\\\ _Paul D. Hanna_, Feb 26 2012",
				"(PARI) N = 42; x='x + O('x^N); v = Vec(1/agm(sqrt((1-3*x)*(1+x)^3), sqrt((1+3*x)*(1-x)^3))); vector((#v+1)\\2, k, v[2*k-1])  \\\\ _Gheorghe Coserea_, Aug 17 2016",
				"(MAGMA) [\u0026+[Binomial(n, k)^2 * Binomial(2*k, k): k in [0..n]]: n in [0..25]]; // _Vincenzo Librandi_, Aug 26 2018"
			],
			"xref": [
				"Cf. A000172, A002895, A000984, A006480, A087457, A274600, A318397.",
				"Cf. A169714 and A169715. -  _Peter Bala_, Mar 05 2013",
				"The Apéry-like numbers [or Apéry-like sequences, Apery-like numbers, Apery-like sequences] include A000172, A000984, A002893, A002895, A005258, A005259, A005260, A006077, A036917, A063007, A081085, A093388, A125143 (apart from signs), A143003, A143007, A143413, A143414, A143415, A143583, A183204, A214262, A219692,A226535, A227216, A227454, A229111 (apart from signs), A260667, A260832, A262177, A264541, A264542, A279619, A290575, A290576. (The term \"Apery-like\" is not well-defined.)",
				"For primes that do not divide the terms of the sequences A000172, A005258, A002893, A081085, A006077, A093388, A125143, A229111, A002895, A290575, A290576, A005259 see A260793, A291275-A291284 and A133370 respectively."
			],
			"keyword": "nonn,easy,walk,nice",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 79,
			"revision": 251,
			"time": "2021-02-25T21:20:36-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}