{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002585",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2585,
			"id": "M2697 N1081",
			"data": "3,7,31,211,2311,509,277,27953,703763,34231,200560490131,676421,11072701,78339888213593,13808181181,18564761860301,19026377261,525956867082542470777,143581524529603,2892214489673,16156160491570418147806951,96888414202798247,1004988035964897329167431269",
			"name": "Largest prime factor of 1 + (product of first n primes).",
			"comment": [
				"Based on Euclid's proof that there are infinitely many primes.",
				"The products of the first primes are called primorial numbers. - _Franklin T. Adams-Watters_, Jun 12 2014"
			],
			"reference": [
				"M. Kraitchik, On the divisibility of factorials, Scripta Math., 14 (1948), 24-26 (but beware errors).",
				"M. Kraitchik, Introduction à la Théorie des Nombres. Gauthier-Villars, Paris, 1952, p. 2.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"\u003ca href=\"/A002585/b002585.txt\"\u003eTable of n, a(n) for n = 1..98\u003c/a\u003e",
				"A. Borning, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1972-0308018-5 \"\u003eSome results for k!+-1 and 2.3.5...p+-1\u003c/a\u003e, Math. Comp., 26 (1972), 567-570.",
				"M. Kraitchik, \u003ca href=\"/A002582/a002582.pdf\"\u003eOn the divisibility of factorials\u003c/a\u003e, Scripta Math., 14 (1948), 24-26 (but beware errors). [Annotated scanned copy]",
				"S. Kravitz and D. E. Penney, \u003ca href=\"http://www.jstor.org/stable/2689826\"\u003eAn extension of Trigg's table\u003c/a\u003e, Math. Mag., 48 (1975), 92-96.",
				"S. Kravitz and D. E. Penney, \u003ca href=\"/A002584/a002584.pdf\"\u003eAn extension of Trigg's table\u003c/a\u003e, Math. Mag., 48 (1975), 92-96. [Annotated scanned copy; also letter from N. J. A. Sloane to John Selfridge]",
				"R. Mestrovic, \u003ca href=\"http://arxiv.org/abs/1202.3670\"\u003eEuclid's theorem on the infinitude of primes: a historical survey of its proofs (300 BC--2012) and another new proof\u003c/a\u003e, arXiv preprint arXiv:1202.3670 [math.HO], 2012-2018. - From _N. J. A. Sloane_, Jun 13 2012",
				"Hisanori Mishima, \u003ca href=\"http://www.asahi-net.or.jp/~KC2H-MSM/mathland/matha1/matha102.htm\"\u003eFactorizations of many number sequences\u003c/a\u003e",
				"Hisanori Mishima, \u003ca href=\"http://www.asahi-net.or.jp/~KC2H-MSM/mathland/matha1/matha103.htm\"\u003eFactorizations of many number sequences\u003c/a\u003e",
				"John Selfridge, Marvin Wunderlich, Robert Morris, N. J. A. Sloane, \u003ca href=\"/A002584/a002584_1.pdf\"\u003eCorrespondence, 1975\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/EuclidNumber.html\"\u003eEuclid Number.\u003c/a\u003e",
				"R. G. Wilson v, \u003ca href=\"/A038507/a038507.txt\"\u003eExplicit factorizations\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A006530(A006862(n)). - _Amiram Eldar_, Feb 13 2020"
			],
			"mathematica": [
				"FactorInteger[#][[-1,1]]\u0026/@Rest[FoldList[Times,1,Prime[Range[30]]]+1] (* _Harvey P. Dale_, Apr 10 2012 *)"
			],
			"program": [
				"(PARI) a(n)=my(f=factor(prod(i=1,n,prime(i))+1)[,1]); f[#f] \\\\ _Charles R Greathouse IV_, Feb 07 2017"
			],
			"xref": [
				"Cf. A002110, A002584, A006530, A006862, A051342."
			],
			"keyword": "nonn,nice",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Labos Elemer_, May 02 2000",
				"More terms from _Robert G. Wilson v_, Mar 24 2001",
				"Terms up to a(81) in b-file added by _Sean A. Irvine_, Apr 19 2014",
				"Terms a(82)-a(87) in b-file added by _Amiram Eldar_, Feb 13 2020",
				"Terms a(88)-a(98) in b-file added by _Max Alekseyev_, Aug 26 2021"
			],
			"references": 10,
			"revision": 68,
			"time": "2021-08-26T20:00:35-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}