{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000256",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 256,
			"id": "M2923 N1173",
			"data": "1,1,0,1,3,12,52,241,1173,5929,30880,164796,897380,4970296,27930828,158935761,914325657,5310702819,31110146416,183634501753,1091371140915,6526333259312,39246152584304,237214507388796,1440503185260748",
			"name": "Number of simple triangulations of the plane with n nodes.",
			"comment": [
				"A triangulation is simple if it contains no separating 3-cycle. The triangulations are rooted with three fixed exterior nodes. - _Andrew Howroyd_, Feb 24 2021"
			],
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence).",
				"W. T. Tutte, The enumerative theory of planar maps, pp. 437-448 of J. N. Srivastava, ed., A Survey of Combinatorial Theory, North-Holland, 1973."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A000256/b000256.txt\"\u003eTable of n, a(n) for n=3..200\u003c/a\u003e",
				"Hsien-Kuei Hwang, Mihyun Kang, and Guan-Huei Duh, \u003ca href=\"https://doi.org/10.4230/LIPIcs.AofA.2018.29\"\u003eAsymptotic Expansions for Sub-Critical Lagrangean Forms\u003c/a\u003e, LIPIcs Proceedings of Analysis of Algorithms 2018, Vol. 110. Schloss Dagstuhl-Leibniz-Zentrum für Informatik, 2018.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0912.0072\"\u003eUne méthode pour obtenir la fonction génératrice d'une série\u003c/a\u003e. FPSAC 1993, Florence. Formal Power Series and Algebraic Combinatorics; arXiv:0912.0072 [math.NT], 2009.",
				"P. N. Rathie, \u003ca href=\"http://dx.doi.org/10.1016/0095-8956(74)90055-0\"\u003eA census of simple planar triangulations\u003c/a\u003e, J. Combin. Theory, B 16 (1974), 134-138.",
				"William T. Tutte, \u003ca href=\"http://dx.doi.org/10.4153/CJM-1962-002-9\"\u003eA census of planar triangulations\u003c/a\u003e, Canad. J. Math. 14 (1962), 21-38.",
				"William T. Tutte, \u003ca href=\"http://dx.doi.org/10.4153/CJM-1963-029-x\"\u003eA Census of Planar Maps\u003c/a\u003e, Canad. J. Math. 15 (1963), 249-271."
			],
			"formula": [
				"a(n) = (1/4)*(7*binomial(3*n-9, n-4)-(8*n^2-43*n+57)*a(n-1)) / (8*n^2-51*n+81), n\u003e4. - _Vladeta Jovovic_, Aug 19 2004",
				"(1/4 + 7/8*n - 9/8*n^3)*a(n) + (-5/4 + 2/3*n + 59/12*n^2 - 13/3*n^3)*a(n+1) + (-1 - 2/3*n + n^2 + 2/3*n^3)*a(n+2). - _Simon Plouffe_, Feb 09 2012",
				"a(n) ~ 3^(3*n-6+1/2)/(2^(2*n+3)*sqrt(Pi)*n^(5/2)). - _Vaclav Kotesovec_, Aug 13 2013",
				"From _Gheorghe Coserea_, Jul 31 2017: (Start)",
				"G.f. y(x) satisfies (with offset 0):",
				"y(x*g^2) = 2 - 1/g, where g=A000260(x). (eqn 2.6 in Tutte's paper)",
				"0 = x*(x+4)^2*y^3 - x*(6*x^2+51*x+76)*y^2 + (12*x^3+108*x^2+115*x-1)*y - (8*x^3+76*x^2+54*x-1).",
				"0 = x*(27*x-4)*deriv(y,x) + x*(7*x+28)*y^2 - 2*(14*x^2+45*x+1)*y + 2*(14*x^2+34*x+1).",
				"(End)"
			],
			"maple": [
				"R := RootOf(x-t*(t-1)^2, t); ogf := series( (2*R^3+2*R^2-2*R-1)/((R-1)*(R+1)^2), x=0, 20); # _Mark van Hoeij_, Nov 08 2011"
			],
			"mathematica": [
				"r = Root[x - t*(t - 1)^2, t, 1] ; CoefficientList[ Series[(2*r^3 + 2*r^2 - 2*r - 1)/((r - 1)*(r + 1)^2), {x, 0, 24}], x] (* _Jean-François Alcover_, Mar 14 2012, after Maple *)"
			],
			"program": [
				"(PARI)",
				"A000260_ser(N) = {",
				"  my(v = vector(N, n, binomial(4*n+2, n+1)/((2*n+1)*(3*n+2))));",
				"  Ser(concat(1,v));",
				"};",
				"A000256_seq(N) = {",
				"  my(g = A000260_ser(N)); Vec(subst(2 - 1/g, 'x, serreverse(x*g^2)));",
				"};",
				"A000256_seq(24)",
				"\\\\ test: y = Ser(A000256_seq(200)); 0 == x*(x+4)^2*y^3 - x*(6*x^2+51*x+76)*y^2 + (12*x^3+108*x^2+115*x-1)*y - (8*x^3+76*x^2+54*x-1)",
				"\\\\ _Gheorghe Coserea_, Jul 31 2017",
				"(PARI) seq(n)={my(g=1+serreverse(x/(1+x)^4 + O(x*x^n) )); Vec(2 - sqrt(serreverse( x*(2-g)^2*g^4)/x ))} \\\\ _Andrew Howroyd_, Feb 23 2021"
			],
			"xref": [
				"First row of array in A210664."
			],
			"keyword": "nonn,nice",
			"offset": "3,5",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Vladeta Jovovic_, Aug 19 2004"
			],
			"references": 6,
			"revision": 88,
			"time": "2021-03-12T22:32:33-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}