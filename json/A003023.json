{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003023",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3023,
			"id": "M0062",
			"data": "0,1,1,2,1,1,1,2,3,3,1,6,1,4,4,5,1,3,1,6,2,5,1,4,2,6,2,1,1,14,1,2,5,7,2,3,1,6,2,3,1,13,1,4,6,7,1,5,3,2,3,8,1,12,2,4,2,3,1,10,1,8,2,3,2,11,1,4,3,5,1,8,1,4,4,4,2,10,1,6,4,5,1,5,2,8,6,6,1,9,3,5,3,3,3,8,1,2,3,4,1,17",
			"name": "\"Length\" of aliquot sequence for n.",
			"comment": [
				"The aliquot sequence for n is the trajectory of n under repeated application of the map x -\u003e sigma(x) - x.",
				"The trajectory will either have a transient part followed by a cyclic part, or will have an infinite transient part and never cycle.",
				"Sequence gives (length of transient part of trajectory) - 1 (if trajectory ends at 1), or provided that it ends in cycle [e.g., (6) or (220 284)], gives (length of transient part of trajectory) + (length of cycle) = length of trajectory. - Corrected by _Antti Karttunen_, Nov 03 2017",
				"See A098007 for a better version.",
				"The function sigma = A000203 is defined only on the positive integers and not for 0, so the trajectory ends when 0 is reached. - _M. F. Hasler_, Nov 16 2013"
			],
			"reference": [
				"G. Everest, A. van der Poorten, I. Shparlinski and T. Ward, Recurrence Sequences, Amer. Math. Soc., 2003; see esp. p. 255.",
				"R. K. Guy, Unsolved Problems in Number Theory, B6.",
				"R. K. Guy and J. L. Selfridge, Interim report on aliquot series, pp. 557-580 of Proceedings Manitoba Conference on Numerical Mathematics. University of Manitoba, Winnipeg, Oct 1971.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A003023/b003023.txt\"\u003eTable of n, a(n) for n = 1..275\u003c/a\u003e (fate of 276 is unknown)",
				"R. K. Guy and J. L. Selfridge, \u003ca href=\"/A003023/a003023.pdf\"\u003eInterim report on aliquot series\u003c/a\u003e, pp. 557-580 of Proceedings Manitoba Conference on Numerical Mathematics. University of Manitoba, Winnipeg, Oct 1971. [Annotated scanned copy]",
				"F. Richman, \u003ca href=\"http://math.fau.edu/Richman/mla/aliquot.htm\"\u003eAliquot series: Abundant, deficient, perfect\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/AliquotSequence.html\"\u003eAliquot Sequence\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Aliquot_sequence\"\u003eAliquot sequence\u003c/a\u003e"
			],
			"example": [
				"Examples of trajectories:",
				"1, 0, 0, ...",
				"2, 1, 0, 0, ...",
				"3, 1, 0, 0, ... (and similarly for any prime)",
				"4, 3, 1, 0, 0, ...",
				"5, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,",
				"6, 6, 6, ... (and similarly for any perfect number)",
				"8, 7, 1, 0, 0, ...",
				"9, 4, 3, 1, 0, 0, ...",
				"12, 16, 15, 9, 4, 3, 1, 0, 0, ...",
				"14, 10, 8, 7, 1, 0, 0, ...",
				"25, 6, 6, 6, ...",
				"28, 28, 28, ... (the next perfect number)",
				"30, 42, 54, 66, 78, 90, 144, 259, 45, 33, 15, 9, 4, 3, 1, 0, 0, ...",
				"42, 54, 66, 78, 90, 144, 259, 45, 33, 15, 9, 4, 3, 1, 0, 0, ..."
			],
			"maple": [
				"f:=proc(n) local t1, i,j,k; t1:=[n]; for i from 2 to 50 do j:= t1[i-1]; k:=sigma(j)-j; t1:=[op(t1), k]; od: t1; end; # produces trajectory for n"
			],
			"mathematica": [
				"f[x_] := (k++; DivisorSigma[1, x] - x); f[1] = 1;",
				"Table[k = 0; FixedPoint[f, n]; k, {n, 1, 102}]",
				"(* _Jean-François Alcover_, Jul 27 2011 *)"
			],
			"program": [
				"(MuPAD) s := func(_plus(op(numlib::divisors(n)))-n,n): A003023 := proc(n) local i,T,m; begin m := n; i := 1; while T[ m ]\u003c\u003e1 and m\u003c\u003e1 do T[ m ] := 1; m := s(m); i := i+1 end_while; i-1 end_proc:",
				"(Scheme)",
				"(define (A003023 n) (let loop ((visited (list n)) (i 0)) (let ((next (A001065 (car visited)))) (cond ((zero? next) i) ((member next visited) (+ 1 i)) (else (loop (cons next visited) (+ 1 i)))))))",
				"(define (A001065 n) (- (A000203 n) n)) ;; For an implementation of A000203, see under that entry.",
				";; _Antti Karttunen_, Nov 03 2017"
			],
			"xref": [
				"Cf. A001065, A098007.",
				"Cf. A059447 (least k such that n is the length of the aliquot sequence for k ending at 1)."
			],
			"keyword": "nonn,easy",
			"offset": "1,4",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Matthew Conroy_, Jan 16 2006"
			],
			"references": 10,
			"revision": 54,
			"time": "2017-11-04T01:07:46-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}