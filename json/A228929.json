{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A228929",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 228929,
			"data": "7,-113,4739,-46804,134370,-614063,1669512,-15474114,-86232481,1080357006,-8574121305,-24144614592,133884333083,-2239330253016,-6347915250018,14541933941298,-42301908155404,-298013673554972,5177473084279656,-46709468571434452,1201667304102142095,-68508286025632748778,850084640720511629243,-2458418086834560217354",
			"name": "Optimal ascending continued fraction expansion of Pi - 3.",
			"comment": [
				"Definition of the expansion: for a positive real number x, there is always a unique sequence of signed integers with increasing absolute value |a(i)|\u003e|a(i-1)| such that x =floor(x)+ 1/a(1) + 1/a(1)/a(2) + 1/a(1)/a(2)/a(3) + 1/a(1)/a(2)/a(3)/a(4) ... or equivalently x=floor(x)+1/a(1)*(1+1/a(2)*(1+1/a(3)*(1+1/a(4)*(1+...)))) giving the fastest converging series with this representation. This formula can be represented as a regular ascending continued fraction. The expansion is similar to Engel and Pierce expansions, but the sign of the terms is not predefined and determined by the algorithm for optimizing the convergence.",
				"For x rational number the expansion has a finite number of terms, for x irrational an infinite number. Empirically the sequence doesn't show any evident regularity except in some interesting cases."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A228929/b228929.txt\"\u003eTable of n, a(n) for n = 1..500\u003c/a\u003e"
			],
			"formula": [
				"Given a positive real number x, let z(0)=x-floor(x) and z(k+1)=abs(z(k))*round(1/abs(z(k)))-1 ; then a(n)=sign(z(n))*round(1/abs(z(n))) for n\u003e0."
			],
			"example": [
				"Pi = 3 + 1/7*(1 - 1/113*(1 + 1/4739*(1 - 1/46804*(1 + 1/134370*(1 - 1/614063*(1 + 1/1669512*(1 + ...)))))))."
			],
			"maple": [
				"# Slow procedure valid for every number",
				"ArticoExp := proc (n, q::posint)::list; local L, i, z; Digits := 50000; L := []; z := n-floor(n); for i to q+1 do if z = 0 then break end if; L := [op(L), round(1/abs(z))*sign(evalf(z))]; z := abs(z)*round(1/abs(z))-1 end do; return L end proc",
				"# Fast procedure, not suited for rational numbers",
				"ArticoExp := proc (n, q::posint)::list; local L, i, z; Digits := 50000; L := []; z := frac(evalf(n)); for i to q+1 do if z = 0 then break end if; L := [op(L), round(1/abs(z))*sign(z)]; z := abs(z)*round(1/abs(z))-1 end do; return L end proc",
				"# List the first 20 terms of the expansion of Pi",
				"ArticoExp(Pi,20)"
			],
			"mathematica": [
				"ArticoExp[x_, n_] :=  Round[1/#] \u0026 /@ NestList[Round[1/Abs[#]]*Abs[#] - 1 \u0026, FractionalPart[x], n]; Block[{$MaxExtraPrecision = 50000}, ArticoExp[Pi, 20]]"
			],
			"program": [
				"(DERIVE)",
				"ArticoExp(x, n) := VECTOR(ROUND(1, ABS(k))*SIGN(k), k, ITERATES(ROUND(1, ABS(u))*ABS(u) - 1, u, MOD(x), n))",
				"Precision:=Mixed",
				"PrecisionDigits:=10000",
				"ArticoExp(PI,20)"
			],
			"xref": [
				"Cf. A006784, A015884, A228930, A228931, A228932, A228933, A228934."
			],
			"keyword": "sign",
			"offset": "1,1",
			"author": "_Giovanni Artico_, Sep 08 2013",
			"references": 6,
			"revision": 27,
			"time": "2016-12-27T02:35:10-05:00",
			"created": "2013-09-13T16:27:00-04:00"
		}
	]
}