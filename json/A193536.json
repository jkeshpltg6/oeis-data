{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A193536",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 193536,
			"data": "1,1,2,1,5,5,4,2,14,21,30,38,40,32,16,42,84,168,322,578,952,1408,1808,1920,1536,768,132,330,840,2112,5168,12172,27352,58126,115636,212762,356352,532224,687104,732160,585728,292864,429,1287,3960",
			"name": "Triangle T(n,k), n\u003e=0, 0\u003c=k\u003c=C(n,2), read by rows: T(n,k) = number of k-length saturated chains in the poset of Dyck paths of semilength n ordered by inclusion.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A193536/b193536.txt\"\u003eRows n = 0..13, flattened\u003c/a\u003e",
				"J. Woodcock, \u003ca href=\"http://garsia.math.yorku.ca/~zabrocki/papers/DPfinal.pdf\"\u003eProperties of the poset of Dyck paths ordered by inclusion\u003c/a\u003e"
			],
			"example": [
				"Poset of Dyck paths of semilength n=3:",
				".",
				".      A       A:/\\      B:",
				".      |        /  \\      /\\/\\",
				".      B       /    \\    /    \\",
				".     / \\",
				".    C   D     C:        D:        E:",
				".     \\ /         /\\      /\\",
				".      E       /\\/  \\    /  \\/\\    /\\/\\/\\",
				".",
				"Saturated chains of length k=0: A, B, C, D, E (5); k=1: A-B, B-C, B-D, C-E, D-E (5); k=2: A-B-C, A-B-D, B-C-E, B-D-E (4), k=3: A-B-C-E, A-B-D-E (2) =\u003e [5,5,4,2].",
				"Triangle begins:",
				"    1;",
				"    1;",
				"    2,   1;",
				"    5,   5,   4,    2;",
				"   14,  21,  30,   38,   40,    32,    16;",
				"   42,  84, 168,  322,  578,   952,  1408,  1808,   1920,   1536,    768;",
				"  132, 330, 840, 2112, 5168, 12172, 27352, 58126, 115636, 212762, 356352, ..."
			],
			"maple": [
				"d:= proc(x, y, l) option remember;",
				"      `if`(x\u003c=1, [[y, l[]]], [seq(d(x-1, i, [y, l[]])[], i=x-1..y)])",
				"    end:",
				"T:= proc(n) option remember; local g, r, j;",
				"      g:= proc(l) option remember; local r, i;",
				"            r:= [1];",
				"            for i to n-1 do if l[i]\u003ei and (i=1 or l[i-1]\u003cl[i]) then",
				"              r:= zip((x, y)-\u003ex+y, r, [0, g(subsop(i=l[i]-1, l))[]], 0)",
				"            fi od; r",
				"          end;",
				"      r:= [];",
				"      for j in d(n, n, []) do",
				"        r:= zip((x, y)-\u003ex+y, r, g(j), 0)",
				"      od; r[]",
				"    end:",
				"seq(T(n), n=0..7);"
			],
			"mathematica": [
				"zip = With[{m = Max[Length[#1], Length[#2]]}, PadRight[#1, m] + PadRight[#2, m]]\u0026; d[x_, y_, l_] := d[x, y, l] = If[x \u003c= 1, {Prepend[l, y]}, Flatten[t = Table [d[x-1, i, Prepend[l, y]], {i, x-1, y}], 1]];",
				"T[n_] := T[n] = Module[{g, r0}, g[l_] := g[l] = Module[{r, i}, r = {1}; For[i = 1, i \u003c= n-1 , i++, If [l[[i]]\u003ei \u0026\u0026 (i == 1 || l[[i-1]] \u003c l[[i]]), r = zip[r, Join[{0}, g[ReplacePart[l, i -\u003e l[[i]]-1]]]]]]; r]; r0 = {}; Do[r0 = zip[r0, g[j]], {j, d[n, n, {}]}]; r0]; Table[T[n], {n, 0, 7}] // Flatten (* _Jean-François Alcover_, Feb 13 2017, translated from Maple *)"
			],
			"xref": [
				"Row sums give: A166860. Columns k=0,1 give: A000108, A002054(n-1). Last elements of rows give: A005118.  Row lengths give: A000124(n-1)."
			],
			"keyword": "nonn,tabf",
			"offset": "0,3",
			"author": "_Alois P. Heinz_, Jul 29 2011",
			"references": 3,
			"revision": 21,
			"time": "2018-12-29T21:08:11-05:00",
			"created": "2011-07-30T11:24:04-04:00"
		}
	]
}