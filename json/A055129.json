{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A055129",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 55129,
			"data": "1,1,2,1,3,3,1,4,7,4,1,5,13,15,5,1,6,21,40,31,6,1,7,31,85,121,63,7,1,8,43,156,341,364,127,8,1,9,57,259,781,1365,1093,255,9,1,10,73,400,1555,3906,5461,3280,511,10,1,11,91,585,2801,9331,19531,21845,9841,1023,11",
			"name": "Repunits in different bases: table by antidiagonals of numbers written in base k as a string of n 1's.",
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A055129/b055129.txt\"\u003eTable of n, a(n) for n = 1..11325\u003c/a\u003e (1 \u003c= n \u003c= 150)."
			],
			"formula": [
				"T(n, k) = (k^n-1)/(k-1) [with T(n, 1) = n] = T(n-1, k)+k^(n-1) = (k+1)*T(n-1, k)-k*T(n-2, k) [with T(0, k) = 0 and T(1, k) = 1].",
				"From _Werner Schulte_, Aug 29 2021 and Sep 18 2021: (Start)",
				"T(n,k) = 1 + k * T(n-1,k) for k \u003e 0 and n \u003e 1.",
				"Sum_{m=2..n} T(m-1,k)/Product_{i=2..m} T(i,k) = (1 - 1/Product_{i=2..n} T(i,k))/k for k \u003e 0 and n \u003e 1.",
				"Sum_{n \u003e 1} T(n-1,k)/Product_{i=2..n} T(i,k) = 1/k for k \u003e 0.",
				"Sum_{i=1..n} k^(i-1) / (T(i,k) * T(i+1,k)) = T(n,k) / T(n+1,k) for k \u003e 0 and n \u003e 0. (End)"
			],
			"example": [
				"T(3,5)=31 because 111 base 5 represents 25+5+1=31.",
				"      1       1       1       1       1       1       1",
				"      2       3       4       5       6       7       8",
				"      3       7      13      21      31      43      57",
				"      4      15      40      85     156     259     400",
				"      5      31     121     341     781    1555    2801",
				"      6      63     364    1365    3906    9331   19608",
				"      7     127    1093    5461   19531   55987  137257",
				"Starting with the second column, the q-th column list the numbers that are written as 11...1 in base q. - _John Keith_, Apr 12 2021"
			],
			"maple": [
				"A055129 := proc(n,k)",
				"    add(k^j,j=0..n-1) ;",
				"end proc: # _R. J. Mathar_, Dec 09 2015"
			],
			"mathematica": [
				"Table[FromDigits[ConstantArray[1, #], k] \u0026[n - k + 1], {n, 11}, {k, n, 1, -1}] // Flatten (* or *)",
				"Table[If[k == 1, n, (k^# - 1)/(k - 1) \u0026[n - k + 1]], {n, 11}, {k, n, 1, -1}] // Flatten (* _Michael De Vlieger_, Dec 11 2016 *)"
			],
			"xref": [
				"Rows include A000012, A000027, A002061, A053698, A053699, A053700. Columns (see recurrence) include A000027, A000225, A003462, A002450, A003463, A003464, A023000, A023001, A002275, A016123, A016125. Diagonals include A023037, A031973. Numbers in the table (apart from the first column and first two rows) are ordered in A053696."
			],
			"keyword": "base,easy,nonn,tabl",
			"offset": "1,3",
			"author": "_Henry Bottomley_, Jun 14 2000",
			"references": 13,
			"revision": 30,
			"time": "2021-10-16T18:29:44-04:00",
			"created": "2000-07-22T03:00:00-04:00"
		}
	]
}