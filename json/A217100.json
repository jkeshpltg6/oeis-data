{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A217100",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 217100,
			"data": "1,2,3,6,0,13,14,26,29,52,58,105,116,211,233,422,467,845,934,1690,1869,3380,3738,6761,7476,13523,14953,27046,29907,54093,59814,108186,119629,216372,239258,432745,478516,865491,957033,1730982,1914067,3461965,3828134",
			"name": "Greatest number such that the number of contiguous palindromic bit patterns in its binary representation is n, or 0, if there is no such number.",
			"comment": [
				"The set of numbers which have n contiguous palindromic bit patterns (in their binary representation) is not empty and finite, provided n\u003c\u003e5. Proof: compare A217101 for the proof of existence. The finiteness follows from A206925, since each number p \u003e 2^floor((n+1)/2) has at least A206925(p) \u003e= 2*floor(log_2(p)) \u003e 2*floor((n+1)/2) = n + n mod 2 palindromic substrings. Thus, there is a boundary b \u003c= 2^floor((n+1)/2) such that all numbers \u003e b have more than n palindromic substrings. It follows, that the set of numbers with n palindromic substrings is finite.",
				"a(5)=0, and this is the only zero term. Proof: cf. A217101.",
				"The binary expansion of a(n) has 1 + floor(n/2) digits (n\u003c\u003e5).",
				"a(2n) is the maximal number with n+1 binary digits such that the number of contiguous palindromic bit patterns in the binary representation is minimal (cf. A206926)."
			],
			"link": [
				"Hieronymus Fischer, \u003ca href=\"/A217100/b217100.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = max(k | A206925(k) = n), for n\u003c\u003e5.",
				"A206925(a(n)) = n, n\u003c\u003e5.",
				"a(n) =\u003e A217101(n), equality holds for n = 1, 2, 3 and 5, only.",
				"Iteration formula:",
				"a(n+2) = 2*a(n) + floor(52*2^floor((n+4-(-1)^n)/2)/63) mod 2, n\u003e5.",
				"a(n+2) = 2*a(n) + floor(52*2^((2*n+7-3*(-1)^n)/4)/63) mod 2, n\u003e5.",
				"Direct calculation formula:",
				"a(n) = floor(52*2^floor((n+1+(-1)^n)/2)/63) mod 2^floor((n+1+(-1)^n)/2)) + (1-(-1)^n)*2^floor((n-2)/2)), for n\u003e5.",
				"a(n) = floor(52*2^((2*n + 1 + 3*(-1)^n)/4)/63) + (1-(-1)^n)*2^((2*n - 5 + (-1)^n)/4), for n\u003e5.",
				"a(2k) = A206926(6k-9), k\u003e2.",
				"G.f.: x*(1 +2*x +x^2 +2*x^3 -6*x^4 +x^5 +14*x^6 +x^8 +x^11 -x^12 -x^13 -2*x^15 +7*x^16 -14*x^18)/((1-2*x^2)*(1-x^3)*(1+x^3)*(1+x^6)); also:",
				"x*(1 -x +3*x^2 +x*(3+14*x^5)/(1-2*x^2) +x^5*(1+x^3)*(1+x^6+x^8)/((1-2*x^2)*(1-x^12)))."
			],
			"example": [
				"a(3)=3, since 3=11_2 has 3 contiguous palindromic bit patterns, and this is the greatest such number.",
				"a(6)=13. Since 13=1101_2 has 6 contiguous palindromic bit patterns, and this is the greatest such number.",
				"a(8)=26. Since 26=11010_2 has 8 contiguous palindromic bit patterns (1, 1, 0, 1, 0, 11, 101 and 010), and this is the greatest such number.",
				"a(9)=27. Since 17=11011_2 has 9 contiguous palindromic bit patterns (1, 1, 0, 1, 1, 11, 11, 101 and 11011), and this is the greatest such number."
			],
			"xref": [
				"Cf. A006995, A206923, A206924, A206925, A206926, A070939, A217101."
			],
			"keyword": "base,nonn",
			"offset": "1,2",
			"author": "_Hieronymus Fischer_, Jan 23 2013",
			"references": 4,
			"revision": 17,
			"time": "2016-03-15T08:01:30-04:00",
			"created": "2013-02-07T08:28:22-05:00"
		}
	]
}