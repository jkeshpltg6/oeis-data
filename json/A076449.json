{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A076449",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 76449,
			"data": "1,2,25,13,37,107,127,113,167,1027,179,137,1036,1127,1013,1137,1235,1136,1123,1037,1139,1079,10124,10126,1349,1279,1237,3479,10699,1367,10179,1379,10127,10079,10138,10123,10234,10235,10247,10339,10267",
			"name": "Least number whose digits can be used to form exactly n different primes (not necessarily using all digits).",
			"comment": [
				"Smallest m such that A039993(m) = n. - _M. F. Hasler_, Mar 08 2014",
				"Mike Keith conjectures that a(n) always exists and reports that he has checked this for n \u003c= 66. - _N. J. A. Sloane_, Jan 25 2008"
			],
			"link": [
				"Michael S. Branicky, \u003ca href=\"/A076449/b076449.txt\"\u003eTable of n, a(n) for n = 0..2702\u003c/a\u003e (terms 1..457 from Robert G. Wilson v)",
				"Michael S. Branicky, \u003ca href=\"/A076449/a076449.txt\"\u003ePython program for A076449, A072857, A076730, A134596\u003c/a\u003e",
				"C. K. Caldwell, The Prime Glossary, \u003ca href=\"http://primes.utm.edu/glossary/page.php?sort=Primeval\"\u003ePrimeval Number\u003c/a\u003e",
				"J. P. Delahaye, Primes Hunters, \u003ca href=\"http://web.archive.org/web/20020901024608/http://www.pour-la-science.com/numeros/pls-258/logique.htm\"\u003e1379 is very primeval (in French)\u003c/a\u003e",
				"Mike Keith, \u003ca href=\"http://www.cadaeic.net/primeval.htm\"\u003eIntegers containing many embedded primes\u003c/a\u003e",
				"W. Schneider, \u003ca href=\"http://web.archive.org/web/2004/www.wschnei.de/digit-related-numbers/primeval-numbers.html\"\u003ePrimeval Numbers\u003c/a\u003e",
				"G. Villemin's Almanach of Numbers, \u003ca href=\"http://villemin.gerard.free.fr/Wwwgvmm/Premier/primeval.htm\"\u003eNombre Primeval de Mike Keith\u003c/a\u003e"
			],
			"formula": [
				"a(n) = min { m | A039993(m)=n } = min A039993^{-1}(n). - _M. F. Hasler_, Mar 08 2014"
			],
			"example": [
				"a(10) = 179 because 179 is the least number harboring ten primes (namely 7, 17, 19, 71, 79, 97, 179, 197, 719, 971)."
			],
			"mathematica": [
				"(* first do *) Needs[\"DiscreteMath`Combinatorica`\"] (* then *) f[n_] := Length[ Select[ FromDigits /@ Flatten[ Permutations /@ Subsets[ IntegerDigits[ n]], 1], PrimeQ[ # ] \u0026]]; t = Table[0, {50}]; Do[ a = f[n]; If[a \u003c 50 \u0026\u0026 t[[a + 1]] == 0, t[[a + 1]] = n], {n, 12500}]; t (* _Robert G. Wilson v_, Feb 12 2005 *)"
			],
			"program": [
				"(PARI) A076449(n)=for(m=1,9e9,A039993(m)==n\u0026\u0026return(n)) \\\\ Not very efficient. - _M. F. Hasler_, Mar 08 2014",
				"(Python) # see linked program"
			],
			"xref": [
				"Cf. A075053, A072857 gives a similar sequence, A134596."
			],
			"keyword": "base,nonn",
			"offset": "0,2",
			"author": "_Lekraj Beedassy_, Nov 07 2002",
			"ext": [
				"Edited by _Robert G. Wilson v_, Nov 24 2002",
				"Keith link repaired by _Charles R Greathouse IV_, Aug 13 2009",
				"Definition reworded by _M. F. Hasler_, Mar 08 2014",
				"a(26) corrected by _Robert G. Wilson v_, Mar 12 2014"
			],
			"references": 7,
			"revision": 56,
			"time": "2021-06-19T10:09:13-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}