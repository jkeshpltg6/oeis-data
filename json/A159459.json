{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A159459",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 159459,
			"data": "0,0,1,0,0,1,0,1,1,2,0,0,0,1,1,0,1,1,2,1,3,0,0,1,0,1,2,1,0,1,0,2,1,2,1,3,0,0,1,1,0,2,1,2,2,0,1,1,2,1,3,1,3,2,3,0,0,0,0,1,0,1,1,1,2,1,0,1,1,2,1,3,1,3,2,3,1,5,0,0,1,1,1,2,0,2,2,2,1,4,1,0,1,0,2,0,2,1,3,1,2,1,4,1,3",
			"name": "Rectangular array read by antidiagonals: a(n,m) = number of divisors of m that don't divide n.",
			"comment": [
				"a(n,1) = 0, for all n. a(1,m) = d(m)-1, for all m.",
				"From _Luc Rousseau_, Jul 27 2018: (Start)",
				"a(.,m) is periodic with period m.",
				"a(n,m) is the number of nonzero elements S(n) and S(n+m) have in common, where S(n) denotes the set of complex numbers k*(1-exp(i*2*Pi*n/k), for k positive integer. See illustration, section links.",
				"(End)"
			],
			"link": [
				"Luc Rousseau, \u003ca href=\"/A159459/a159459.png\"\u003eillustration, a(n,m) viewed as an intersection of sets of points\u003c/a\u003e"
			],
			"formula": [
				"a(n,m) = d(m) - d(gcd(n,m)), where d(m) = A000005(m)."
			],
			"example": [
				"From _Andrew Howroyd_, Jul 27 2018: (Start)",
				"Array begins",
				"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ...",
				"  1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 ...",
				"  1 1 0 1 1 0 1 1 0 1 1 0 1 1 0 1 1 0 ...",
				"  2 1 2 0 2 1 2 0 2 1 2 0 2 1 2 0 2 1 ...",
				"  1 1 1 1 0 1 1 1 1 0 1 1 1 1 0 1 1 1 ...",
				"  3 2 2 2 3 0 3 2 2 2 3 0 3 2 2 2 3 0 ...",
				"  1 1 1 1 1 1 0 1 1 1 1 1 1 0 1 1 1 1 ...",
				"  3 2 3 1 3 2 3 0 3 2 3 1 3 2 3 0 3 2 ...",
				"  2 2 1 2 2 1 2 2 0 2 2 1 2 2 1 2 2 0 ...",
				"  ...",
				"(End)"
			],
			"maple": [
				"A159459 := proc(n,m) numtheory[tau](m)-numtheory[tau](gcd(n,m)) ; end: for d from 2 to 20 do for m from 1 to d-1 do n := d-m ; printf(\"%d,\",A159459(n,m)) ; od: od: # _R. J. Mathar_, Apr 16 2009"
			],
			"mathematica": [
				"Table[DivisorSigma[0, #] - DivisorSigma[0, GCD[n, #]] \u0026[m - n + 1], {m, 13}, {n, m, 1, -1}] // Flatten (* _Michael De Vlieger_, Jul 30 2018 *)"
			],
			"program": [
				"(PARI) \\\\ port of R.J. Mathar's Maple program",
				"a(n,m)=numdiv(m)-numdiv(gcd(n,m))",
				"for(d=2,20,for(m=1,d-1,n=d-m;print1(a(n,m),\", \"))) \\\\ _Luc Rousseau_, Jul 27 2018"
			],
			"xref": [
				"Cf. A077478."
			],
			"keyword": "nonn,tabl",
			"offset": "1,10",
			"author": "_Leroy Quet_, Apr 12 2009",
			"ext": [
				"2 terms corrected by _R. J. Mathar_, Apr 16 2009"
			],
			"references": 1,
			"revision": 24,
			"time": "2018-08-02T13:04:41-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}