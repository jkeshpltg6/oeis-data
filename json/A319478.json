{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A319478",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 319478,
			"data": "2,2,2,3,2,4,5,5,2,3,3,5,3,6,7,7,2,4,9,9,4,4,10,11,11,5,5,3,3,13,3,3,2,11,11,5,3,3,6,13,13,13,6,6,6,15,15,15,6,7,5,5,17,17,18,5,7,7,7,19,19,20,20,7,2,4,8,22,4,4,17,23,6,6,8,24,19,19,6",
			"name": "a(n) is the least base b \u003e 1 such that the product n * n can be computed without carry by long multiplication.",
			"comment": [
				"Apparently, a(n) is also the least base b \u003e 1 where the square of the digital sum of n equals the digital sum of the square of n.",
				"The sequence is well defined as, for any n \u003e 0, n * n can be computed without carry in base n^2 + 1."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A319478/b319478.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A319478/a319478.png\"\u003eColored scatterplot of (n, a(n)) for n = 0..50000\u003c/a\u003e (where the color is function of the initial digit of n in base a(n))",
				"\u003ca href=\"/index/Ca#CARRYLESS\"\u003eIndex entries for sequences related to carryless arithmetic\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 2 iff n belongs to A131577.",
				"a(n * a(n)) \u003c= a(n).",
				"a(A061909(n)) \u003c= 10 for any n \u003e 0."
			],
			"mathematica": [
				"Array[Block[{b = 2}, While[AnyTrue[With[{d = IntegerDigits[#, b]}, Function[{s, t}, Total@ Map[PadLeft[#, t] \u0026, s]] @@ {#, Max[Length /@ #]} \u0026@ MapIndexed[Join[d #, ConstantArray[0, First@ #2 - 1]] \u0026, Reverse@ d]], # \u003e= b \u0026], b++]; b] \u0026, 79, 0] (* _Michael De Vlieger_, Nov 25 2018 *)"
			],
			"program": [
				"(PARI) a(n) = for (b=2, oo, my (d=if(n==0, [0], digits(n,b))); if (vecmax(d)^2\u003cb, my (s=0, ok=1); forstep (i=#d, 1, -1, s \\= b; my (t=d[i]*n); if (sumdigits(s+t,b)==sumdigits(s,b)+sumdigits(t,b), s += t, ok = 0; break)); if (ok, return (b))))"
			],
			"xref": [
				"See A321882 for the additive variant.",
				"Cf. A061909, A131577."
			],
			"keyword": "nonn,base",
			"offset": "0,1",
			"author": "_Rémy Sigrist_, Nov 21 2018",
			"references": 2,
			"revision": 44,
			"time": "2018-12-01T04:54:00-05:00",
			"created": "2018-11-26T17:05:40-05:00"
		}
	]
}