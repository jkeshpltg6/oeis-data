{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A252731",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 252731,
			"data": "1,1,-3,2,-2,-1,-4,-3,6,8,2,-3,4,-5,0,5,-1,-6,-1,-4,0,-10,6,0,-3,6,-6,3,8,3,-4,-4,12,-1,-3,15,-4,4,-2,2,1,6,-18,0,-9,-8,5,-24,-7,2,18,8,-6,6,2,-1,-15,9,8,12,1,0,3,-16,-10,-6,16,15,9,14,0,4",
			"name": "Fourier expansion of the unique newform on Gamma_0(44).",
			"comment": [
				"Fourier expansion denoted by f_44(tau) on p. 80 of Umbral Moonshine.",
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A252731/b252731.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"M. Cheng, J. Duncan, and J. Harvey, \u003ca href=\"https://arxiv.org/abs/1204.2779\"\u003eUmbral Moonshine\u003c/a\u003e, arXiv:1204.2779 [math.RT], 2012-2013.",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of 2*F(x^2)^5 / (F(x) * F(x^4)^2) - F(x)^3 / F(x^2) - 4*x * F(x) * F(x^2) in powers of x where F() is the g.f. for A030200.",
				"Expansion of f(-q^22) * phi(-q^22)^3 / chi(-q^2) - 2 * q^4 * f(-q^22) * (chi(-q) * psi(q^11)^3 + chi(q) * psi(-q^11)^3) in powers of q^2 where phi(), psi(), chi(), f() are Ramanujan theta functions.",
				"a(n) = b(2*n + 1) where b() is multiplicative with b(p^e) = (-1)^e if p = 11, b(p^e) = b(p)*b(p^(e-1)) - p*b(p^(e-2)) if p != 11.",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (44 t)) = 44 (t/i)^2 f(t) where q = exp(2 Pi i t)."
			],
			"example": [
				"G.f. = 1 + x - 3*x^2 + 2*x^3 - 2*x^4 - x^5 - 4*x^6 - 3*x^7 + 6*x^8 + ...",
				"G.f. = q + q^3 - 3*q^5 + 2*q^7 - 2*q^9 - q^11 - 4*q^13 - 3*q^15 + 6*q^17 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ x] QPochhammer[ x^11] (2 EllipticTheta[ 3, 0, x] EllipticTheta[ 3, 0, x^11] - EllipticTheta[ 4, 0, x] EllipticTheta[ 4, 0, x^11] - 4 x QPochhammer[ x^2] QPochhammer[ x^22]), {x, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = local(A, F1, F2, F4); if( n\u003c0, 0, A = x * O(x^n); F1 = eta(x + A) * eta(x^11 + A); F2 = subst(F1, x, x^2); F4 = subst(F1, x, x^4); polcoeff( 2*F2^5 / (F1 * F4^2) - F1^3 / F2 - 4*x * F1*F2, n))};",
				"(Sage) A = CuspForms( Gamma0(44), 2, prec=146) . basis(); A[0] + A[2];",
				"(MAGMA) A := Basis( CuspForms( Gamma0(44), 2), 146); A[1] + A[3];"
			],
			"xref": [
				"Cf. A030200."
			],
			"keyword": "sign",
			"offset": "0,3",
			"author": "_Michael Somos_, Dec 21 2014",
			"references": 1,
			"revision": 21,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2014-12-21T01:01:09-05:00"
		}
	]
}