{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A264405",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 264405,
			"data": "1,1,0,1,0,1,2,0,0,1,2,0,2,0,1,3,0,2,1,0,1,4,0,2,2,2,0,1,5,0,4,2,1,2,0,1,6,0,6,2,3,2,2,0,1,8,0,7,4,4,2,2,2,0,1,10,0,8,6,6,4,3,2,2,0,1,12,0,13,6,6,8,3,3,2,2,0,1,15,0,15,9,11,6,9,4,3,2,2,0,1,18,0,21,10,13,12,7,8,4,3,2,2,0,1",
			"name": "Triangle read by rows: T(n,k) is the number of integer partitions of n having k repeated parts (each occurrence is counted).",
			"comment": [
				"Compare with A264052 where only one occurrence of a repeated part is counted.",
				"Sum of entries in row n = number of partitions of n = A000041(n).",
				"Sum_{k\u003e=0} k*T(n,k) = A194452(n)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A264405/b264405.txt\"\u003eRows n = 0..200, flattened\u003c/a\u003e"
			],
			"formula": [
				"G.f.: G(t,x) = Product_{j\u003e=1}(1 + x^j + t^2*x^{2j}/(1 - tx^j))."
			],
			"example": [
				"T(4,2) = 2 because each of the partitions [2,2] and [2,1,1] have 2 repeated parts, while [4], [3,1], [1,1,1,1] have 0 or 4 repeated parts.",
				"Triangle starts:",
				"  1;",
				"  1, 0;",
				"  1, 0, 1;",
				"  2, 0, 0, 1;",
				"  2, 0, 2, 0, 1;",
				"  3, 0, 2, 1, 0, 1;"
			],
			"maple": [
				"g := product(1+x^j+t^2*x^(2*j)/(1-t*x^j), j = 1 .. 100): gser := simplify(series(g, x = 0, 30)): for n from 0 to 20 do P[n] := sort(coeff(gser, x, n)) end do: for n from 0 to 20 do seq(coeff(P[n], t, k), k = 0 .. n) end do; # yields sequence in triangular form",
				"# second Maple program:",
				"b:= proc(n, i) option remember; `if`(n=0, 1, `if`(i\u003c1, 0,",
				"      add(expand(b(n-i*j, i-1)*`if`(j\u003e1, x^j, 1)), j=0..n/i)))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=0..n))(b(n$2)):",
				"seq(T(n), n=0..14);  # _Alois P. Heinz_, Dec 07 2015"
			],
			"mathematica": [
				"b[n_, i_] := b[n, i] = If[n == 0, 1, If[i \u003c 1, 0, Sum[Expand[b[n - i*j, i - 1]*If[j \u003e 1, x^j, 1]], {j, 0, n/i}]]]; T[n_] := Function[p, Table[ Coefficient[p, x, i], {i, 0, n}]][b[n, n]]; Table[T[n], {n, 0, 14}] // Flatten (* _Jean-François Alcover_, Jan 23 2016, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A000009, A000041, A194452, A264052."
			],
			"keyword": "nonn,tabl",
			"offset": "0,7",
			"author": "_Emeric Deutsch_, Dec 07 2015",
			"references": 2,
			"revision": 13,
			"time": "2016-01-23T08:34:47-05:00",
			"created": "2015-12-07T11:07:52-05:00"
		}
	]
}