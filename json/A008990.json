{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A008990",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 8990,
			"data": "1,-1,-1,-19,-559,-29161,-2368081,-276580459,-43947282079,-9118829535121,-2394495729300961,-776228170682260099,-304471093666800990799,-142128398853646068197881,-77865168574139358455774641,-49474260304294496117945326939",
			"name": "Numerators of sequence having sqrt(cos(x)) as e.g.f. (even-indexed coefficients only).",
			"reference": [
				"R. P. Stanley, Enumerative Combinatorics, Cambridge, Vol. 2, 1999; see page 191."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A008990/b008990.txt\"\u003eTable of n, a(n) for n = 0..100\u003c/a\u003e"
			],
			"formula": [
				"E.g.f.: sqrt(cos(sqrt(2)*x)) = 1 - x^2/2! - x^4/4! - 19x^6/6! -... - _Ralf Stephan_, Mar 03 2005",
				"a(n) = sum(sum(binomial(k,j)*2^(n+2-2*k-j)*sum(binomial(j,i)*(j-2*i)^(2*n), i=0..floor((j-1)/2))*(-1)^(n+j+1), j=1..k)*C(k-1), k=1..2*n), n\u003e0, C(n) - Catalan numbers (A000108). - _Vladimir Kruchinin_, Sep 10 2010",
				"G.f.: 2/G(0) where G(k) = 2 - 4*x*(k+1)*(2*k-1)/G(k+1); (continued fraction). - _Sergei N. Gladkovskii_, Jan 12 2013",
				"G.f.:  Q(0), where Q(k) = 1 - x*(k+1)*(2*k-1)/( x*(k+1)*(2*k-1) + 2/Q(k+1) ); (continued fraction). - _Sergei N. Gladkovskii_, Nov 21 2013",
				"a(n) = (1/(4*n))*Sum_{k=1..n} C(2*n,2*k)*(-1)^(k)*2^(3*k)*(2^(2*k)-1)*B(2*k)*a(n-k), a(0)=1, where B(n) is Bernoulli numbers. - _Vladimir Kruchinin_, Jun 23 2015.",
				"The odd terms of EllipticE(x,2) act as a g.f. for 2^n*a(n)/(2*n+1)!. - _Benedict W. J. Irwin_, Jun 06 2016",
				"a(n) ~ -2^(5*n - 1) * n^(2*n - 1) / (Pi^(2*n - 1/2) * exp(2*n)). - _Vaclav Kotesovec_, Jun 11 2016"
			],
			"mathematica": [
				"n = 32; Partition[ CoefficientList[ Series[ Sqrt[Cos[Sqrt[2]*x]], {x, 0, n}], x]*Range[0, n]!, 2][[All, 1]] (* _Jean-François Alcover_, Aug 30 2011 *)",
				"Table[SeriesCoefficient[Series[EllipticE[x, 2], {x, 0, 41}], 2 n + 1] (2 n + 1)!/2^n, {n, 0, 20}] (* _Benedict W. J. Irwin_, Jun 06 2016 *)"
			],
			"program": [
				"(Maxima) C(n):=1/(n+1)*binomial(2*n,n); a(n):=sum(sum(binomial(k,j) *2^(n+2-2*k-j)*sum(binomial(j,i)*(j-2*i)^(2*n),i,0,floor((j-1)/2))*(-1)^(n+j+1),j,1,k)*C(k-1),k,1,2*n); /* _Vladimir Kruchinin_, Sep 10 2010 */",
				"(Maxima) a(n):=if n=0 then 1 else 1/(4*n)*sum(binomial(2*n,2*k)*(-1)^(k)*2^(3*k)*(2^(2*k)-1)*bern(2*k)*a(n-k),k,1,n);  /* _Vladimir Kruchinin_, Jun 23 2015 */"
			],
			"xref": [
				"Cf. A027641.",
				"Denominators are in A000079."
			],
			"keyword": "sign,frac,easy,nice",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_",
			"references": 1,
			"revision": 47,
			"time": "2021-12-16T22:42:59-05:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}