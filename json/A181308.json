{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A181308",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 181308,
			"data": "1,0,2,3,0,4,0,16,0,8,14,0,52,0,16,0,104,0,144,0,32,64,0,460,0,368,0,64,0,616,0,1624,0,896,0,128,292,0,3428,0,5056,0,2112,0,256,0,3456,0,14688,0,14528,0,4864,0,512,1332,0,23132,0,53920,0,39488,0,11008,0,1024,0",
			"name": "Triangle read by rows: T(n,k) is the number of 2-compositions of n having k columns with an odd sum (0\u003c=k\u003c=n). A 2-composition of n is a nonnegative matrix with two rows, such that each column has at least one nonzero entry and whose entries sum up to n.",
			"comment": [
				"The sum of entries in row n is A003480(n).",
				"T(n,k) = 0 if n and k have opposite parities.",
				"T(2n,0) = A060801(n).",
				"Sum(k*T(n,k), k=0..n) = A181326(n).",
				"For the statistic \"number of column with an even sum\" see A181327."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A181308/b181308.txt\"\u003eTable of n, a(n) for n = 0..140, flattened\u003c/a\u003e",
				"G. Castiglione, A. Frosini, E. Munarini, A. Restivo and S. Rinaldi, \u003ca href=\"http://dx.doi.org/10.1016/j.ejc.2006.06.020\"\u003eCombinatorial aspects of L-convex polyominoes\u003c/a\u003e, European J. Combin. 28 (2007), no. 6, 1724-1741."
			],
			"formula": [
				"G.f.: G(t,z) = (1-z)^2*(1+z)^2/(1-5z^2+2z^4-2tz).",
				"The g.f. of column k is (2z)^k*(1-z^2)^2/(1-5z^2+2z^4)^{k+1} (we have a Riordan array).",
				"The g.f. H(t,s,z), where z marks size and t (s) marks number of columns with an odd (even) sum, is H=(1-z^2)^2/(1-2z^2+z^4-2tz-3sz^2+sz^4)."
			],
			"example": [
				"T(2,2) = 4 because we have (1,0/0,1), (0,1/1,0), (1,1/0,0), and (0,0/1,1) (the 2-compositions are written as (top row/bottom row)).",
				"Triangle starts:",
				"1;",
				"0,  2;",
				"3,  0,  4;",
				"0, 16,  0, 8;",
				"14, 0, 52, 0, 16;"
			],
			"maple": [
				"G := (1-z^2)^2/(1-5*z^2+2*z^4-2*t*z): Gser := simplify(series(G, z = 0, 15)): for n from 0 to 11 do P[n] := sort(coeff(Gser, z, n)) end do; for n from 0 to 11 do seq(coeff(P[n], t, k), k = 0 .. n) end do; # yields sequence in triangular form",
				"# second Maple program:",
				"b:= proc(n) option remember; `if`(n=0, 1,",
				"       expand(add(add(`if`(i=0 and j=0, 0, b(n-i-j)*",
				"       `if`(irem(i+j,2)=1, x, 1)), i=0..n-j), j=0..n)))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=0..degree(p)))(b(n)):",
				"seq(T(n), n=0..15); # _Alois P. Heinz_, Mar 16 2014"
			],
			"mathematica": [
				"b[n_] := b[n] = If[n == 0, 1, Expand[Sum[Sum[If[i == 0 \u0026\u0026 j == 0, 0, b[n-i-j]* If[Mod[i+j, 2] == 1, x, 1]], {i, 0, n-j}], {j, 0, n}]]]; T[n_] := Function[{p}, Table[Coefficient[p, x, i], {i, 0, Exponent[p, x]}]][b[n]]; Table[T[n], {n, 0, 15}] // Flatten (* _Jean-François Alcover_, Feb 19 2015, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A003480, A060801, A181326, A181327."
			],
			"keyword": "nonn,tabl",
			"offset": "0,3",
			"author": "_Emeric Deutsch_, Oct 13 2010",
			"references": 3,
			"revision": 13,
			"time": "2015-02-19T05:11:48-05:00",
			"created": "2010-10-20T03:00:00-04:00"
		}
	]
}