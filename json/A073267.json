{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A073267",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 73267,
			"data": "0,0,1,2,1,2,2,0,1,2,2,0,2,0,0,0,1,2,2,0,2,0,0,0,2,0,0,0,0,0,0,0,1,2,2,0,2,0,0,0,2,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,2,0,2,0,0,0,2,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0",
			"name": "Number of compositions (ordered partitions) of n into exactly two powers of 2.",
			"comment": [
				"Starting with 1 = self-convolution of A036987, the characteristic function of the powers of 2. [_Gary W. Adamson_, Feb 23 2010]"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A073267/b073267.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Sen-Peng Eu, Shu-Chung Liu and Yeong-Nan Yeh, \u003ca href=\"http://dx.doi.org/10.1016/j.ejc.2007.06.019\"\u003eCatalan and Motzkin numbers modulo 4 and 8\u003c/a\u003e, Eur. J. Combinat. 29 (2008) 1449-1466."
			],
			"formula": [
				"G.f.: (Sum_{k\u003e=0} x^(2^k) )^2. - _Vladeta Jovovic_, Mar 28 2005",
				"a(n+1) = A000108(n) mod 4, n\u003e=1 [Theorem 2.3 of Eu et al.]. - _R. J. Mathar_, Feb 27 2008",
				"a(n) = sum (A209229(k)*A036987(n-k): k = 0..n), convolution of characteristic functions of 2^n and 2^n-1. [_Reinhard Zumkeller_, Mar 07 2012]",
				"a(n+2) = A000168(n) mod 4. - _John M. Campbell_, Jul 07 2016"
			],
			"example": [
				"For 2 there is only composition {1+1}, for 3 there is {1+2, 2+1}, for 4 {2+2}, for 5 {1+4, 4+1}, for 6 {2+4,4+2}, for 7 none, thus a(2)=1, a(3)=2, a(4)=1, a(5)=2, a(6)=2 and a(7)=0."
			],
			"maple": [
				"f:= proc(n) local d;",
				"d:= convert(convert(n,base,2),`+`);",
				"if d=2 then 2 elif d=1 then 1 else 0 fi",
				"end proc:",
				"0, 0, seq(f(n),n=2..100); # _Robert Israel_, Jul 07 2016"
			],
			"mathematica": [
				"Table[Count[Map[{#, n - #} \u0026, Range[0, n]], k_ /; Times @@ Boole@ Map[IntegerQ@ Log2@ # \u0026, k] == 1], {n, 0, 88}] (* _Michael De Vlieger_, Jul 08 2016 *)"
			],
			"program": [
				"(Haskell)",
				"a073267 n = sum $ zipWith (*) a209229_list $ reverse $ take n a036987_list",
				"-- _Reinhard Zumkeller_, Mar 07 2012",
				"(PARI)",
				"N=166; x='x+O('x^N);",
				"v=Vec( 'a0 + sum(k=0,ceil(log(N)/log(2)), x^(2^k) )^2 );",
				"v[1] -= 'a0;  v",
				"/* _Joerg Arndt_, Oct 21 2012 */"
			],
			"xref": [
				"The second row of the table A073265. The essentially same sequence 1, 1, 2, 1, 2, 2, 0, 1, ... occurs for first time in A073202 as row 105 (the fix count sequence of A073290). The positions of 1's for n \u003e 1 is given by the characteristic function of A000079, i.e. A036987 with offset 1 instead of 0 and the positions of 2's is given by A018900. Cf. also A023359.",
				"Cf. A036987. [_Gary W. Adamson_, Feb 23 2010]"
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_Antti Karttunen_, Jun 25 2002",
			"references": 18,
			"revision": 29,
			"time": "2016-07-28T21:54:12-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}