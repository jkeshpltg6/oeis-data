{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A302120",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 302120,
			"data": "3,11,1,311,5,7291,243,14462317,3364621,3337014731,3155743303,65528247068741,2627553901,1439156737843967,2213381206625,21757704362231905789,2627003970197650333,64925181492079668050329,523317843775891637,161371847993975070290712761,78461950306245817433389909",
			"name": "Absolute value of the numerators of a series converging to Euler's constant.",
			"comment": [
				"gamma = 3/4 - 11/96 - 1/72 - 311/46080 - 5/1152 - 7291/2322432 - ..., see formula (104) in the reference below."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A302120/b302120.txt\"\u003eTable of n, a(n) for n = 1..250\u003c/a\u003e",
				"Ia. V. Blagouchine, \u003ca href=\"http://math.colgate.edu/~integers/sjs3/sjs3.Abstract.html\"\u003eThree Notes on Ser's and Hasse's Representations for the Zeta-functions.\u003c/a\u003e INTEGERS, Electronic Journal of Combinatorial Number Theory, vol. 18A, Article #A3, pp. 1-45, 2018. \u003ca href=\"http://arxiv.org/abs/1606.02044\"\u003earXiv:1606.02044 [math.NT], 2016\u003c/a\u003e."
			],
			"formula": [
				"a(n) = abs(Numerators of ((1/2)*(-1)^(n+1)*(Sum_{l=0,n-1} (S_1(n-1,l)*((-1/2)^(l+1) + 1)/(l+1)))/(n!) + (-1)^(n+1)*(Sum_{l=1,n} S_1(n,l)/(l+1)))/(n*n!))), where S_1(x,y) are the signed Stirling numbers of the first kind."
			],
			"example": [
				"Numerators of 3/4, -11/96, -1/72, -311/46080, -5/1152, -7291/2322432, ..."
			],
			"maple": [
				"a:= proc(n) abs(numer((1/2)*(-1)^(n+1)*(add(Stirling1(n-1, l)*((-1/2)^(l+1)+1)/(l+1), l = 0 .. n-1))/(n)!+(-1)^(n+1)*(add(Stirling1(n, l)/(l+1), l = 1 .. n))/(n*(n)!))) end proc: seq(a(n), n=1..23);"
			],
			"mathematica": [
				"a[n_] := Numerator[(1/2)*(-1)^(n+1)*(Sum[StirlingS1[n-1,l]*((-1/2)^(l+1) + 1)/(l+1),{l,0,n-1}])/(n!) + (-1)^(n+1)*(Sum[StirlingS1[n, l]/(l+1),{l,1,n}])/(n*n!)]; Table[Abs[a[n]], {n, 1, 24}]"
			],
			"program": [
				"(PARI) a(n) = abs(numerator((1/2)*(-1)^(n+1)*(sum(l=0,n-1,stirling(n-1,l)*((-1/2)^(l+1) + 1)/(l+1))) /(n!) + (-1)^(n+1)*(sum(l=1,n,stirling(n,l)/(l+1)))/(n*n!)))",
				"(MAGMA) [3] cat [Abs(Numerator( (1/2)*(-1)^(n+1)*(\u0026+[StirlingFirst(n-1,k)*((-1/2)^(k+1) + 1)/(k+1): k in [1..n-1]])/Factorial(n) + (-1)^(n+1)*(\u0026+[StirlingFirst(n,k)/(k+1): k in [1..n]])/(n*Factorial(n)) )): n in [2..30]]; // _G. C. Greubel_, Oct 29 2018"
			],
			"xref": [
				"Cf. A302121 (denominators of this series), A262856, A262858."
			],
			"keyword": "frac,nonn",
			"offset": "1,1",
			"author": "_Iaroslav V. Blagouchine_, Apr 01 2018",
			"references": 2,
			"revision": 26,
			"time": "2018-10-29T20:16:47-04:00",
			"created": "2018-04-09T12:58:38-04:00"
		}
	]
}