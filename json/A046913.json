{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A046913",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 46913,
			"data": "1,3,1,7,6,3,8,15,1,18,12,7,14,24,6,31,18,3,20,42,8,36,24,15,31,42,1,56,30,18,32,63,12,54,48,7,38,60,14,90,42,24,44,84,6,72,48,31,57,93,18,98,54,3,72,120,20,90,60,42,62,96,8,127,84,36,68,126",
			"name": "Sum of divisors of n not congruent to 0 mod 3.",
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A046913/b046913.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Hershel M. Farkas, \u003ca href=\"https://doi.org/10.1007/s11139-004-0141-5\"\u003eOn an arithmetical function\u003c/a\u003e, Ramanujan J., Vol. 8, No. 3 (2004), pp. 309-315.",
				"Pavel Guerzhoy and Ka Lun Wong, \u003ca href=\"https://doi.org/10.1007/s11139-020-00296-5\"\u003eFarkas' identities with quartic characters\u003c/a\u003e, The Ramanujan Journal (2020), \u003ca href=\"https://arxiv.org/abs/1905.06506\"\u003epreprint\u003c/a\u003e, arXiv:1905.06506 [math.NT], 2019."
			],
			"formula": [
				"Multiplicative with a(3^e) = 1, a(p^e) = (p^(e+1)-1)/(p-1) for p\u003c\u003e3. - _Vladeta Jovovic_, Sep 11 2002",
				"G.f.: Sum_{k\u003e0} x^k*(1+2*x^k+2*x^(3*k)+x^(4*k))/(1-x^(3*k))^2. - _Vladeta Jovovic_, Dec 18 2002",
				"a(n) = A000203(3n)-3*A000203(n). - _Labos Elemer_, Aug 14 2003",
				"Equals A051731 * A091684, where A051731 = the inverse Mobius transform and A091684 = count with 3*n = 0: (1, 2, 0, 4, 5, 0, 7, ...). Example: a(4) = 7 = (1, 1, 0, 1) dot (1, 2, 0, 4) = (1 + 2 + 0 + 4), where (1, 1, 0, 1) = row 4 of A051731. - _Gary W. Adamson_, Jul 03 2008",
				"Dirichlet g.f.: zeta(s)*zeta(s-1)*(1-1/3^(s-1)). - _R. J. Mathar_, Feb 10 2011",
				"G.f. A(x) satisfies: 0 = f(A(x), A(x^2), A(x^4)) where f(u, v, w)= u^2 + 9 * v^2 + 16 * w^2 - 6 * u*v + 4 * u*w - 24 * v*w - v + w. - _Michael Somos_, Jul 19 2004",
				"L.g.f.: log(Product_{k\u003e=1} (1 - x^(3*k))/(1 - x^k)) = Sum_{n\u003e=1} a(n)*x^n/n. - _Ilya Gutkovskiy_, Mar 14 2018",
				"a(n) = A002324(n) + 3*Sum_{j=1, n-1} A002324(j)*A002324(n-j). See Farkas and Guerzhoy links. - _Michel Marcus_, Jun 01 2019",
				"a(3*n) = a(n). - _David A. Corneth_, Jun 01 2019",
				"Sum_{k=1..n} a(k) ~ Pi^2 * n^2 / 18. - _Vaclav Kotesovec_, Sep 17 2020"
			],
			"example": [
				"Divisors of 12 are 1 2 3 4 6 12 and discarding 3 6 and 12 we get a(12) = 1 + 2 + 4 = 7.",
				"x + 3*x^2 + x^3 + 7*x^4 + 6*x^5 + 3*x^6 + 8*x^7 + 15*x^8 + x^9 + 18*x^10 + ..."
			],
			"mathematica": [
				"Table[DivisorSigma[1, 3*w]-3*DivisorSigma[1, w], {w, 1, 256}]",
				"DivisorSum[#1, # \u0026, Mod[#, 3] != 0 \u0026] \u0026 /@ Range[68] (* _Jayanta Basu_, Jun 30 2013 *)",
				"f[p_, e_] := If[p == 3, 1, (p^(e+1)-1)/(p-1)]; a[1] = 1; a[n_] := Times @@ f @@@ FactorInteger[n]; Array[a, 100] (* _Amiram Eldar_, Sep 17 2020 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, 0, sigma(3*n) - 3 * sigma(n))} /* _Michael Somos_, Jul 19 2004 */",
				"(PARI) a(n) = sigma(n \\ 3^valuation(n, 3)) \\\\ _David A. Corneth_, Jun 01 2019",
				"(MAGMA) [SumOfDivisors(3*k)-3*SumOfDivisors(k):k in [1..70]]; // _Marius A. Burtea_, Jun 01 2019"
			],
			"xref": [
				"Cf. A035191.",
				"Cf. A000726, A051731, A091684, A000203, A002324."
			],
			"keyword": "nonn,mult",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"references": 22,
			"revision": 50,
			"time": "2020-09-17T04:10:26-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}