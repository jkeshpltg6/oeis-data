{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A339513",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 339513,
			"data": "1,0,-1,3,-2,-45,347,-756,-13031,184245,-810034,-11404503,264733177,-1931955480,-21453955777,796153961091,-8688345850874,-69492467459925,4300450718587619,-65896562313762012,-307002797419794407,37668399518087366325",
			"name": "Define R_{1}(x)=1, R_{n+1}(x)=(R_n(x)*2*x/(1+x^2))'; then a(n)=R_{n}(1).",
			"comment": [
				"Let (R_n) be the sequence of rational functions satisfying: R_1(x) = 1; R_{n+1}(x) = (R_n(x) * 2*x/(1+x^2))'. By definition, a(n) = R_n(1).",
				"Applying [Dominici, Theorem 4.1] proves that the e.g.f. of this sequence is the series reversion of log(1+x)/2 + x^2/4 + x/2."
			],
			"link": [
				"Luc Rousseau, \u003ca href=\"/A339513/b339513.txt\"\u003eTable of n, a(n) for n = 1..300\u003c/a\u003e",
				"D. Dominici, \u003ca href=\"http://arxiv.org/abs/math/0501052\"\u003eNested derivatives: A simple method for computing series expansions of inverse functions\u003c/a\u003e, arXiv:math/0501052 [math.CA], 2005."
			],
			"formula": [
				"a(n) = (Sum_{k=0..n-1} (-1)^k*A214406(n-1,k))/2^(n-1).",
				"a(n) = Sum_{P partition of n-1} A145271(P) * Product_{p part of P} A090932(p)*A075553(p+3).",
				"E.g.f.: series reversion of log(1+x)/2 + x^2/4 + x/2."
			],
			"example": [
				"R_1(x) = 1,",
				"  so a(1) = R_1(1) = 1.",
				"R_2(x) = (R_1(x)*2*x/(1+x^2))' = (1 * 2*x/(1+x^2))' = 2*(1-x^2)/(1+x^2)^2,",
				"  so a(2) = R_2(1) = 0.",
				"R_3(x) = (R_2(x)*2*x/(1+x^2))' = (2*(1-x^2)/(1+x^2)^2 * 2*x/(1+x^2))' = 4*(1-8*x^2+3*x^4)/(1+x^2)^4, so a(3) = R_3(1) = -1."
			],
			"program": [
				"(PARI)",
				"list_a(nmax)=my(n,r);n=1;r=1;print1(subst(r,x,1),\", \");while(n\u003cnmax,n++;r=(r*2*x/(1+x^2))';print1(subst(r,x,1),\", \"))",
				"list_a(50)",
				"(PARI) my(x='x+O('x^33)); Vec(serlaplace(serreverse(log(1+x)/2 + x^2/4 + x/2))) \\\\ _Joerg Arndt_, Dec 22 2020"
			],
			"xref": [
				"Cf. A214406, A145271, A090932, A075553."
			],
			"keyword": "sign",
			"offset": "1,4",
			"author": "_Luc Rousseau_, Dec 07 2020",
			"references": 1,
			"revision": 25,
			"time": "2021-01-04T19:32:20-05:00",
			"created": "2020-12-23T20:26:31-05:00"
		}
	]
}