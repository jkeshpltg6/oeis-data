{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A233697",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 233697,
			"data": "3,3,14,3,6,8,1,8,6,8,9,6,3,14,3,3,8,9,9,8,9,11,3,6,8,1,8,9,14,6,3,14,3,6,8,6,9,8,9,6,3,11,8,1,8,9,9,8",
			"name": "Duration in months between successive instances of a given Julian calendar day of the month falling on a specified day of the week.",
			"comment": [
				"A 28-year cycle exists as derived by multiplication of 4 years / leap cycle by 7 days of the week. Within such a cycle there are exactly 48 days when a given day of the month (selected from 1 to 28) falls on a specific day of the week.",
				"Fewer days result for the choices of 29, 30 and 31, each with their own unique sequence that repeats after 28 years. These results may be published as their own related yet nonetheless unique sequences.",
				"The average duration between such events is simply 7 months as trivially derived. However, the *actual* durations vary from 1 to 14 months with the following frequencies:",
				"    separation (months)      number of instances",
				"            1                         3",
				"            3                        10",
				"            6                         8",
				"            8                        12 (modal value)",
				"            9                         9",
				"           11                         2",
				"           14                         4",
				"The entire sequence of 48 time separations in months between successive instances of a given day of the month falling on a specified day of the week is provided above.",
				"(Account is not taken of the extra non-leap years introduced with the Gregorian calendar; i.e., those divisible by 100 yet NOT by 400, e.g., A.D. 2100, 2200 and 2300. Thus, the method in the example does not work without modification for time durations including such years or years such as A.D. 2201, 2202 or 2203; in these cases, the previous leap year is 4, 5, 6 or 7 years earlier.)"
			],
			"link": [
				"\u003ca href=\"/index/Ca#calendar\"\u003eIndex entries for sequences related to calendars\u003c/a\u003e"
			],
			"formula": [
				"Brute force calculation is more efficient than employing a formula, one that would be most involved and hence not meaningful in its individual terms. Calendar consultation provides a simple alternative."
			],
			"example": [
				"**** THIS FULL EXPLANATION IS REQUIRED TO PROVIDE AN EXAMPLE ****",
				"Designate \"year 0\" as any leap year, with the succeeding years as years 1, 2 and 3. The first term, 3, is the time in months between a date in January 0 (such as January 17, 2012 - a leap year) and the next time ANY \"17th\" falls on the same day of the week - April 17, 2012.",
				"The following tabulation identifies the remaining 47 month/year combinations within the sequence, with \"year\" being 0 through 3 as defined above.",
				"Jan0- 3 -Apr0-  3 -Jul0- 14 -Sep1-  3 -Dec1-  6 -Jun2-  8 -Feb3",
				"Feb3- 1 -Mar3-  8 -Nov3-  6 -May0-  8 -Jan1-  9 -Oct1-  6 -Apr2",
				"Apr2- 3 -Jul2- 14 -Sep3-  3 -Dec3-  3 -Mar0-  8 -Nov0-  9 -Aug1",
				"Aug1- 9 -May2-  8 -Jan3-  9 -Oct3- 11 -Sep0-  3 -Dec0-  6 -Jun1",
				"Jun1- 8 -Feb2-  1 -Mar2-  8 -Nov2-  9 -Aug3- 14 -Oct0-  6 -Apr1",
				"Apr1- 3 -Jul1- 14 -Sep2-  3 -Dec2-  6 -Jun3-  8 -Feb0-  6 -Aug0",
				"Aug0- 9 -May1-  8 -Jan2-  9 -Oct2-  6 -Apr3-  3 -Jul3- 11 -Jun0",
				"Jun0- 8 -Feb1-  1 -Mar1-  8 -Nov1-  9 -Aug2-  9 -May3-  8 -Jan0",
				"**** EXAMPLE ****",
				"It is December 13, 2013 - and a Friday. When are the next 5 \"Friday the 13ths\"?",
				"SOLUTION. 2013 is a \"year 1\". Locate December for year 1 in the table in the fifth column of months, first entry. By inspection it is 6 months to the next Friday the 13th in June 2014. Reading the next four entries, the remaining four events are in February 2015 (as \"Feb3\"), March 2015 (\"Mar3\"), November 2015 (\"Nov3\") and May 2016 (\"May0\")."
			],
			"program": [
				"(PARI)",
				"{a(n, list = 0) = if(n\u003c1 || n\u003e48, return);",
				"                                   \\\\ calculate (\\list) up to n-th term",
				"my(daysmod7 = [3, 0, 3, 2, 3, 2, 3, 3, 2, 3, 2, 3],",
				"                                   \\\\ days beyond 28 in months",
				"s = 0, c = 1, yr = 0, mo = 1, months = 0);",
				"                                   \\\\ yr = 0 means (Julian) leap year",
				"while(1,                           \\\\ find next cumulative whole week(s)",
				"  months++; s = (s + daysmod7[mo] + (mo==2 \u0026\u0026 yr==0))%7;",
				"                                   \\\\ count elapsed months",
				"  if(s==0, if(list, print1(months, \", \"));",
				"                                   \\\\ print if optional list \u003c\u003e 0",
				"           if(c==n, return(months), c++; months = 0));",
				"                                   \\\\ return n-th or continue",
				"  if(mo\u003c12, mo++, mo = 1; yr = (yr + 1)%4))}",
				"                                   \\\\ new month or new month/year",
				"a(48, 1) \\\\ print all 48 terms (and return 48th) - _Rick L. Shepherd_, Jul 06 2017"
			],
			"xref": [
				"Cf. A231010, A231013."
			],
			"keyword": "nonn,fini,full",
			"offset": "1,1",
			"author": "_Adam Helman_, Dec 14 2013",
			"ext": [
				"Lightly edited and terms/examples checked by _Rick L. Shepherd_, Jun 29 2017"
			],
			"references": 0,
			"revision": 35,
			"time": "2017-07-10T04:25:32-04:00",
			"created": "2013-12-19T11:43:15-05:00"
		}
	]
}