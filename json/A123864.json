{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A123864",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 123864,
			"data": "1,1,2,1,3,1,2,0,4,1,2,0,3,0,0,1,5,2,2,2,3,0,0,2,4,1,0,1,0,0,2,2,6,0,4,0,3,0,4,0,4,0,0,0,0,1,4,2,5,1,2,2,0,2,2,0,0,2,0,0,3,2,4,0,7,0,0,0,6,2,0,0,4,0,0,1,6,0,0,2,5,1,0,2,0,2,0,0,0,0,2,0,6,2,4,2,6,0,2,0,3,0,4,0,0",
			"name": "Expansion of (eta(q^3) * eta(q^5))^2 / (eta(q) * eta(q^15)) in powers of q.",
			"comment": [
				"Number 31 of the 74 eta-quotients listed in Table I of Martin (1996).",
				"Multiplicative because this sequence is the inverse Moebius transform of a multiplicative sequence Kronecker(-15, n). - _Andrew Howroyd_, Jul 27 2018"
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A123864/b123864.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Y. Martin, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9947-96-01743-6\"\u003eMultiplicative eta-quotients\u003c/a\u003e, Trans. Amer. Math. Soc. 348 (1996), no. 12, 4825-4856, see page 4852 Table I.",
				"Michael Somos, \u003ca href=\"/A030203/a030203.txt\"\u003eIndex to Yves Martin's list of 74 multiplicative eta-quotients and their A-numbers\u003c/a\u003e"
			],
			"formula": [
				"Euler transform of period 15 sequence [ 1, 1, -1, 1, -1, -1, 1, 1, -1, -1, 1, -1, 1, 1, -2, ...].",
				"Moebius transform is period 15 sequence [ 1, 1, 0, 1, 0, 0, -1, 1, 0, 0, -1, 0, -1, -1, 0, ...].",
				"G.f. A(q) satisfies 0 = f(A(q), A(q^2), A(q^4)) where f(u, v, w) = - v^3 + 4*u*v*w - 2*u*w^2 - u^2*w.",
				"G.f.: Product_{k\u003e0} ((1 - x^(3*k)) * (1 - x^(5*k)))^2 / ((1 - x^k) * (1 - x^(15*k))).",
				"G.f.: (1/2) * (Sum_{n,m in Z} x^(n^2 + n*m + 4*m^2) + x^(2*n^2 + n*m + 2*m^2)).",
				"a(15*n + 7) = a(15*n + 11) = a(15*n + 13) = a(15*n + 14) = 0. a(3*n) = a(n).",
				"a(n) = A035175(n) unless n=0. a(n) = |A106406(n)| unless n=0.",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (15 t)) = 15^(1/2) (t/i) f(t) where q = exp(2 Pi i t). - _Michael Somos_, Feb 10 2015",
				"a(n) = Sum_{d | n}  Kronecker(-15, d). - _Andrew Howroyd_, Jul 27 2018"
			],
			"example": [
				"G.f. = 1 + q + 2*q^2 + q^3 + 3*q^4 + q^5 + 2*q^6 + 4*q^8 + q^9 + 2*q^10 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ (QPochhammer[ q^3] QPochhammer[ q^5])^2 / ( QPochhammer[ q] QPochhammer[ q^15]), {q, 0, n}]; (* _Michael Somos_, Feb 10 2015 *)",
				"a[ n_] := If[ n \u003c 1, Boole[n == 0], Sum[ KroneckerSymbol[ -15, d], { d, Divisors[ n]}]]; (* _Michael Somos_, Feb 10 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, n==0, sumdiv( n, d, kronecker( -15, d)))};",
				"(PARI) {a(n) = if( n\u003c1, n==0, (qfrep( [2, 1; 1, 8],n, 1) + qfrep( [4, 1; 1, 4], n, 1))[n])};",
				"(PARI) {a(n) = local(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( (eta(x^3 + A) * eta(x^5 + A))^2 / (eta(x + A) * eta(x^15 + A)), n))};",
				"(MAGMA) A := Basis( ModularForms( Gamma1(15), 1), 106); A[1] + A[2] + 2*A[3] + A[4] + 3*A[5] + A[6] + 2*A[7]; /* _Michael Somos_, Feb 10 2015 */"
			],
			"xref": [
				"Cf. A035175, A106406."
			],
			"keyword": "nonn,mult",
			"offset": "0,3",
			"author": "_Michael Somos_, Oct 14 2006",
			"references": 4,
			"revision": 21,
			"time": "2018-11-22T21:50:11-05:00",
			"created": "2006-12-06T03:00:00-05:00"
		}
	]
}