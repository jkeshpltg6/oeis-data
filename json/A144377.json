{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A144377",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 144377,
			"data": "1,2,0,0,2,-2,-4,0,0,-2,4,8,0,0,4,-8,-14,0,0,-8,14,24,0,0,12,-22,-40,0,0,-20,36,64,0,0,32,-56,-98,0,0,-48,84,148,0,0,72,-126,-220,0,0,-106,184,320,0,0,152,-264,-460,0,0,-216,376,652,0,0,306,-528",
			"name": "Expansion of phi(q) / phi(q^5) in powers of q where phi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"reference": [
				"B. C. Berndt, Ramanujan's Notebooks Part IV, Springer-Verlag, see p. 235, Entry 67."
			],
			"link": [
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of eta(q^2)^5 * eta(q^5)^2 * eta(q^20)^2 / (eta(q)^2 * eta(q^4)^2 * eta(q^10)^5) in powers of q.",
				"Euler transform of period 20 sequence [ 2, -3, 2, -1, 0, -3, 2, -1, 2, 0, 2, -1, 2, -3, 0, -1, 2, -3, 2, 0, ...].",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2)) where f(u, v) = (u^4 - 2*u^2 +5) * (v^4 - 2*v^2 + 5) - 4 * (u^2 - 2*u*v - v^2)^2.",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^3)) where f(u, v) = (v^2 + 3*u*v - u^2) * (u^2 + v^2) - u*v * (5 + u^2*v^2).",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (20 t)) = 5^(1/2) g(t) where q = exp(2 Pi i t) and g() is the g.f. for A261968.",
				"G.f.: Product_{k\u003e0} P(20, x^k)^2 / (P(10, x^k)^3 * P(5, x^k)) where P(n, x) is the n-th cyclotomic polynomial.",
				"a(5*n + 2) = a(5*n + 3) = 0.",
				"a(n) = (-1)^n * A138527(n). Convolution inverse is A216968. - _Michael Somos_, Sep 06 2015"
			],
			"example": [
				"G.f. = 1 + 2*q + 2*q^4 - 2*q^5 - 4*q^6 - 2*q^9 + 4*q^10 + 8*q^11 + 4*q^14 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 3, 0, q] / EllipticTheta[ 3, 0, q^5], {q, 0, n}]; (* _Michael Somos_, Sep 06 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A)^5 * eta(x^5 + A)^2 * eta(x^20 + A)^2 / (eta(x + A)^2 * eta(x^4 + A)^2 * eta(x^10 + A)^5), n))};"
			],
			"xref": [
				"Cf. A138527, A261968."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Sep 18 2008",
			"references": 1,
			"revision": 10,
			"time": "2021-03-12T22:24:45-05:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}