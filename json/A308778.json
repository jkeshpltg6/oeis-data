{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A308778",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 308778,
			"data": "-1,-1,0,1,-1,0,2,1,1,-1,0,3,2,1,2,1,-1,0,4,3,2,2,4,3,1,-1,0,5,2,1,2,5,1,2,4,1,-1,0,6,4,3,2,2,5,2,2,6,5,1,-1,0,7,2,1,6,2,2,4,1,7,2,2,6,1,-1,0,8,7,4,4,2,7,2,5,1,1,4,2,4,7,1,-1,0",
			"name": "Central element(s) in the period of the continued fraction expansion of sqrt(n), or 0 if no such element exists, or -1 if n is a square.",
			"comment": [
				"The continued fraction expansion of sqrt(n) is periodic (where n is no square), and the period splits in two halves which are mirrored around the center. With r = floor(sqrt(n)) the expansion takes one of the forms:",
				"  [r; i, j, k, ..., m, m, ..., k, j, i, 2*r] (odd period length) or",
				"  [r; i, j, k, ..., m, ..., k, j, i, 2*r] (even period length)",
				"  [r; 2*r] (empty symmetric part, for n = r^2 + 1)",
				"This sequence lists the central element(s) m, or 0 for n = r^2 + 1, or -1 for n = r^2.",
				"a(k^2-1) = 1 for k \u003e= 2. - _Robert Israel_, Nov 04 2019"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A308778/b308778.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Georg Fischer, \u003ca href=\"https://github.com/gfis/fasces/blob/master/oeis/cfsqrt/sqrt20k.txt\"\u003eTable of the continued fractions of sqrt(0..20000)\u003c/a\u003e",
				"Oskar Perron, \u003ca href=\"https://archive.org/details/dielehrevondenk00perrgoog/page/n5\"\u003eDie Lehre von den Kettenbrüchen\u003c/a\u003e, B. G. Teubner (1913), section 24, p. 87 ff."
			],
			"example": [
				"CF(sqrt(2906)) = [53;1,9,1,3,1,3,1,1,14,1,5,2,2,5,1,14,1,1,3,1,3,1,9,1,106], odd period, two central elements, a(2906) = 2."
			],
			"maple": [
				"f:= proc(n) local L,m;",
				"  if issqr(n) then return -1",
				"  elif issqr(n-1) then return 0",
				"  fi;",
				"  L:= numtheory:-cfrac(sqrt(n),periodic,quotients);",
				"  m:= nops(L[2]);",
				"  L[2][floor(m/2)]",
				"end proc:",
				"map(f, [$0..100]); # _Robert Israel_, Nov 04 2019"
			],
			"mathematica": [
				"Array[Which[IntegerQ@ Sqrt@ #, -1, IntegerQ@ Sqrt[# - 1], 0, True, #[[Floor[Length[#]/2]]] \u0026@ Last@ ContinuedFraction@ Sqrt@ #] \u0026, 83, 0] (* _Michael De Vlieger_, Jul 07 2019 *)"
			],
			"xref": [
				"Cf. A031509-A031688."
			],
			"keyword": "sign,look",
			"offset": "0,7",
			"author": "_Georg Fischer_, Jun 24 2019",
			"references": 2,
			"revision": 18,
			"time": "2019-11-05T00:57:30-05:00",
			"created": "2019-07-08T04:01:10-04:00"
		}
	]
}