{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A106233",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 106233,
			"data": "0,1,3,5,5,0,-14,-41,-81,-121,-121,0,364,1093,2187,3281,3281,0,-9842,-29525,-59049,-88573,-88573,0,265720,797161,1594323,2391485,2391485,0,-7174454,-21523361,-43046721,-64570081,-64570081,0,193710244,581130733,1162261467",
			"name": "An inverse Catalan transform of A003462.",
			"comment": [
				"The g.f. is obtained from that of A003462 through the mapping g(x)-\u003eg(x(1-x)). A003462 may be retrieved through the mapping g(x)-\u003eg(xc(x)), where c(x) is the g.f. of A000108. Binomial transform of x(1+x)/(1+x^2+x^4).",
				"The sequence is identical to its sixth differences. See A140344. - _Paul Curtz_, Nov 09 2012"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A106233/b106233.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-7,6,-3)."
			],
			"formula": [
				"G.f.: x(1-x)/((1-x+x^2)*(1-3*x+3*x^2));",
				"a(n) = Sum_{k=0..floor(n/2)} C(n-k, k)*(-1)^k*(3^(n-k)-1)/2.",
				"a(n) = Sum_{k=0..n} A109466(n,k)*A003462(k). - _Philippe Deléham_, Oct 30 2008",
				"a(n) = (1/2)*[A057083(n) - [1,1,0,0,-1,-1]_6 ]. - _Ralf Stephan_, Nov 15 2010",
				"a(n) = 4*a(n-1) - 7*a(n-2) + 6*a(n-3) - 3*a(n-4) = A140343(n+2) - A140343(n+1). - _Paul Curtz_, Nov 09 2012",
				"a(n) is the binomial transform of the sequence 0, 1, 1, -1, -1, 0, ... = A103368(n+5). - _Paul Curtz_, Nov 09 2012"
			],
			"example": [
				"From _Paul Curtz_, Nov 09 2012: (Start)",
				"The sequence and its higher-order differences (periodic after 6 rows):",
				"   0,  1,  3,  5,  5,   0, -14, ...",
				"   1,  2,  2,  0, -5, -14, -27, ...",
				"   1,  0, -2, -5, -9, -13, -13, ...",
				"  -1, -2, -3, -4, -4,   0,  13, ...   = -A134581(n+1)",
				"  -1, -1, -1,  0,  4,  13,  27, ...",
				"   0,  0,  1,  4,  9,  14,  14, ...   = A140343(n+2)",
				"   0,  1,  3,  5,  5,   0, -14, ...",
				"(End)"
			],
			"mathematica": [
				"LinearRecurrence[{4, -7, 6, -3}, {0, 1, 3, 5}, 35] (* _Vincenzo Librandi_, Dec 24 2018 *)"
			],
			"program": [
				"(MAGMA) I:=[0,1,3,5]; [n le 4 select I[n] else 4*Self(n-1)-7*Self(n-2)+ 6*Self(n-3)-3*Self(n-4): n in [1..40]]; // _Vincenzo Librandi_, Dec 24 2018"
			],
			"xref": [
				"Cf. A103368."
			],
			"keyword": "easy,sign",
			"offset": "0,3",
			"author": "_Paul Barry_, Apr 26 2005",
			"references": 4,
			"revision": 30,
			"time": "2018-12-24T06:12:34-05:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}