{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A156709",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 156709,
			"data": "-1,0,-1,0,-1,0,-1,-1,-2,-1,-1,0,-1,0,-1,-1,-2,-2,-3,-2,-2,-1,-2,-1,-1,0,-1,-1,-2,-2,-2,-1,-2,-1,-2,-1,-2,-2,-2,-2,-2,-1,-2,-2,-3,-2,-2,-2,-3,-2,-2,-1,-1,0,-1,-1,-2,-2,-3,-2,-2,-2,-3,-2,-3,-2,-2,-2,-2,-1,-1,-1,-1",
			"name": "For all numbers k(n) congruent to -1 or +1 (mod 6) starting with k(n) = {5,7,11,13,...}, a(k(n)) is incremented by the congruence (mod 6) if k(n) is prime and by 0 if k(n) is composite.",
			"comment": [
				"The fact that a(k(n)) is predominantly negative exhibits the Chebyshev Bias (where the congruences that are not quadratic residues generally lead in the prime number races, at least for \"small\" integers, over the congruences that are quadratic residues).",
				"This bias seems caused (among other causes?) by the presence of all those squares (even powers) coprime to 6 taking away opportunities for primes to appear in the quadratic residue class +1 (mod 6), while the non-quadratic residue class -1 (mod 6) is squarefree.",
				"The density of squares congruent to +1 (mod 6) is 1/(6*sqrt(k(n))) since 1/3 of the squares are congruent to +1 (mod 6), while the density of primes in either residue classes -1 or +1 (mod 6) is 1/(phi(6)*log(k(n))), with phi(6) = 2.",
				"Here 1 is a quadratic residue mod 6, but 5 (or equivalently -1) is a quadratic non-residue mod 6. All the even powers (included in the squares) map congruences {-1, +1} to {+1, +1} respectively and so contribute to the bias, whereas all the odd powers map {-1, +1} to {-1, +1} respectively and so do not contribute to the bias.",
				"One would then expect the ratio of this bias, if caused exclusively by the even powers, relative to the number of primes in either congruences to asymptotically tend towards to 0 as k(n) increases (since 1/(6*sqrt(k(n))) is o(1/(phi(6)*log(k(n)))).",
				"The persistence or not of such bias in absolute value then does not contradict The Prime Number Theorem for Arithmetic Progressions (Dirichlet) which states that the asymptotic (relative) ratio of the count of prime numbers in each congruence class coprime to m tends to 1 in the limit towards infinity. (Cf. 'Prime Number Races' link below.)",
				"Also, even if this bias grows in absolute value, it is expected to be drowned out (albeit very slowly) by the increasing fluctuations in the number of primes in each congruence class coprime to 6 since, assuming the truth of the Riemann Hypothesis, their maximum amplitude would be, with x standing for k(n) in our case, h(x) = O(sqrt(x)*log(x)) \u003c= C * sqrt(x)*log(x) in absolute value which gives relative fluctuations of order h(x)/x = O(log(x)/sqrt(x)) \u003c= C*log(x)/sqrt(x) in the densities of primes pi(x, {6, 1})/x and pi(x, {6, 5})/x in either congruence class.",
				"Since 1/(6*sqrt(x)) is o(log(x)/sqrt(x)) the bias will eventually be overwhelmed by the \"pink noise or nearly 1/f noise\" corresponding to the fluctuations in the prime densities in either congruence classes. The falsehood of the Riemann Hypothesis would imply even greater fluctuations since the RH corresponds to the minimal h(x).",
				"We get pink noise or nearly 1/f noise if we consider the prime density fluctuations of pi(x, {6, k})/x as an amplitude spectrum over x (with a power density spectrum of (C*log(x)/sqrt(x))^2 = ((C*log(x))^2)/x and see x as the frequency f. This power density spectrum is then nearly 1/x and would have nearly equal energy (although slowly increasing as (C*log(x))^2) for each octave of x. (Cf. 'Prime Numbers: A Computational Perspective' link below.)",
				"Among the positive integers k(n) up to 100000 that are congruent to -1 or +1 (mod 6) [indexed from n = 1 to 33332, with k(n) = 6 ceiling(n/2) + (-1)^n], a tie, where a(k(n)) = 0, is attained or maintained for only 9 integers, and that bias favoring the non-quadratic residue class -1 (mod 6) never gets violated, i.e., a(k(n)) is never +1."
			],
			"reference": [
				"R. Crandall and C. Pomerance, \"Prime Numbers - A Computational Perspective\", Second Edition, Springer Verlag 2005, ISBN 0-387-25282-7"
			],
			"link": [
				"Daniel Forgues, \u003ca href=\"/A156709/b156709.txt\"\u003eTable of n, a(n) for n = 1..33332\u003c/a\u003e",
				"A. Granville and G. Martin, 2004, \u003ca href=\"http://www.arxiv.org/abs/math.NT/0408319\"\u003ePrime Number Races\u003c/a\u003e",
				"Eric Weisstein, \u003ca href=\"http://mathworld.wolfram.com/ChebyshevBias.html\"\u003eChebyshev Bias\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Pink_noise\"\u003ePink noise\u003c/a\u003e"
			],
			"xref": [
				"Cf. A156706 (whose sum of first n terms gives a(n) of this sequence).",
				"Cf. A156749 (which exhibits the Chebyshev Bias for congruences -1 or +1 (mod 4)).",
				"Cf. A156707 (whose sum of first n terms gives a(n) of A156749).",
				"Cf. A075743, Prime characteristic function of numbers congruent to -1 or +1 (mod 6).",
				"Cf. A101264, Prime characteristic function of numbers congruent to -1 or +1 (mod 4)."
			],
			"keyword": "sign",
			"offset": "1,9",
			"author": "_Daniel Forgues_, Feb 13 2009, Feb 14 2009, Feb 23 2009, Mar 01 2009, Mar 14 2009, Mar 29 2009",
			"references": 4,
			"revision": 13,
			"time": "2014-12-06T15:44:30-05:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}