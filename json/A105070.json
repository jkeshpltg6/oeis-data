{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A105070",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 105070,
			"data": "1,2,3,2,4,8,5,20,4,6,40,24,7,70,84,8,8,112,224,64,9,168,504,288,16,10,240,1008,960,160,11,330,1848,2640,880,32,12,440,3168,6336,3520,384,13,572,5148,13728,11440,2496,64,14,728,8008,27456,32032,11648,896,15,910,12012,51480,80080,43680,6720,128",
			"name": "T(n,k) = 2^k*binomial(n,2k+1), where 0 \u003c= k \u003c= floor((n-1)/2, n \u003e= 1.",
			"comment": [
				"Row n contains ceiling(n/2) terms. Row sums yield the Pell numbers (A000129). Column 1 yields A007290.",
				"Eigenvector equals A118397, so that A118397(n) = Sum_{k=0..[n/2]} T(n+1,k)*A118397(k) for n \u003e= 0. - _Paul D. Hanna_, May 08 2006",
				"Essentially a triangle, read by rows, given by (2, -1/2, 1/2, 0, 0, 0, 0, 0, 0, 0, ...) DELTA (0, 1, -1, 0, 0, 0, 0, 0, 0, 0, ...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, Dec 12 2011",
				"Subtriangle of the triangle given by (1, 1, -1, 1, 0, 0, 0, 0, 0, 0, 0, ...) DELTA (0, 0, 2, -2, 0, 0, 0, 0, 0, 0, 0, ...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, Apr 07 2012"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A105070/b105070.txt\"\u003eRows n = 1..100 of the triangle, flattened\u003c/a\u003e",
				"Rui Duarte and António Guedes de Oliveira, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Duarte/duarte7.html\"\u003eA Famous Identity of Hajós in Terms of Sets\u003c/a\u003e, Journal of Integer Sequences, Vol. 17 (2014), #14.9.1.",
				"J. Ivie, \u003ca href=\"https://www.fq.math.ca/Scanned/8-1/elementary8-1.pdf\"\u003eProblem B-161\u003c/a\u003e, Fibonacci Quarterly, 8 (1970), 107-108."
			],
			"formula": [
				"E.g.f.: exp(x)*sinh(x*sqrt(2*y))/sqrt(2*y), cf. A034867. - _Vladeta Jovovic_, Apr 06 2005",
				"From _Philippe Deléham_, Apr 07 2012: (Start)",
				"As DELTA-triangle T(n,k) with 0 \u003c= k \u003c= n:",
				"G.f.: (1-x+x^2-y*x^2)/(1-2*x+x^2-2*y*x^2).",
				"T(n,k) = 2*T(n-1,k) - T(n-2,k) + 2*T(n-2,k-1), T(0,0) = T(1,0) = 1, T(1,1) = T(2,1) = T(2,2) = 0, T(2,0) = 2 and T(n,k) = 0 if k\u003c0 or if k\u003en. (End)",
				"Sum_{k=0..floor((n-1)/2)} T(n,k) = { P(n) (A000129(n)), A215928(n), (-1)^(n-1) *A077985(n-1), -A176981(n+1), (-1)^(n-1)*A215936(n+2) }, for n \u003e= 1. - _G. C. Greubel_, Mar 15 2020"
			],
			"example": [
				"Triangle begins:",
				"  1;",
				"  2;",
				"  3,  2;",
				"  4,  8;",
				"  5, 20,  4;",
				"  6, 40, 24;",
				"(2, -1/2, 1/2, 0, 0, ...) DELTA (0, 1, -1, 0, 0, ...) begins:",
				"  1;",
				"  2,  0;",
				"  3,  2,  0;",
				"  4,  8,  0,  0;",
				"  5, 20,  4,  0,  0;",
				"  6, 40, 24,  0,  0,  0.",
				"(1, 1, -1, 1, 0, 0, ...) DELTA (0, 0, 2, -2, 0, 0, ...) begins:",
				"  1;",
				"  1,  0;",
				"  2,  0,  0;",
				"  3,  2,  0,  0;",
				"  4,  8,  0,  0,  0;",
				"  5, 20,  4,  0,  0,  0;",
				"  6, 40, 24,  0,  0,  0,  0. - _Philippe Deléham_, Apr 07 2012"
			],
			"maple": [
				"T:=(n,k)-\u003ebinomial(n,2*k+1)*2^k:for n from 1 to 15 do seq(T(n,k),k=0..floor((n-1)/2)) od; # yields sequence in triangular form"
			],
			"mathematica": [
				"u[1, x_] := 1; v[1, x_] := 1; z = 16;",
				"u[n_, x_] := u[n - 1, x] + 2 x*v[n - 1, x]",
				"v[n_, x_] := u[n - 1, x] + v[n - 1, x]",
				"Table[Factor[u[n, x]], {n, 1, z}]",
				"Table[Factor[v[n, x]], {n, 1, z}]",
				"cu = Table[CoefficientList[u[n, x], x], {n, 1, z}];",
				"TableForm[cu]",
				"Flatten[%]  (* A207536 *)",
				"Table[Expand[v[n, x]], {n, 1, z}]",
				"cv = Table[CoefficientList[v[n, x], x], {n, 1, z}];",
				"TableForm[cv]",
				"Flatten[%]  (* A105070 *)",
				"(* _Clark Kimberling_, Feb 18 2010 *)",
				"Table[2^k*Binomial[n, 2*k+1], {n, 15}, {k,0,Floor[(n-1)/2]}]//Flatten (* _G. C. Greubel_, Mar 15 2020 *)"
			],
			"program": [
				"(MAGMA) [2^k*Binomial(n,2*k+1): k in [0..Floor((n-1)/2)], n in [1..15]]; // _G. C. Greubel_, Mar 15 2020",
				"(Sage) [[2^k*binomial(n,2*k+1) for k in (0..floor((n-1)/2))] for n in (1..15)] # _G. C. Greubel_, Mar 15 2020"
			],
			"xref": [
				"Cf. A000129, A007290.",
				"Cf. A118397 (eigenvector)."
			],
			"keyword": "nonn,tabf",
			"offset": "1,2",
			"author": "_Emeric Deutsch_, Apr 05 2005",
			"references": 6,
			"revision": 33,
			"time": "2020-03-15T22:25:56-04:00",
			"created": "2005-04-09T03:00:00-04:00"
		}
	]
}