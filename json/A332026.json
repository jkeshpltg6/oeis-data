{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A332026",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 332026,
			"data": "3,4,3,5,4,4,6,5,5,5,7,6,6,6,6,8,7,7,7,7,7,9,8,8,8,8,8,8,10,9,9,9,9,9,9,9,11,10,10,10,10,10,10,10,10,12,11,11,11,11,11,11,11,11,11,13,12,12,12,12,12,12,12,12,12",
			"name": "Savannah problem: number of new possibilities after n weeks.",
			"comment": [
				"The Savannah math problem (_Ali Sada_, 26 Dec 2019 email to Seqfan list) is about a savannah ecosystem consisting of zebras, fed lions and hungry lions. Assume we start with an empty savannah. Each week, the following things happen in this order:",
				"1. All hungry lions (if any) die.",
				"2. All fed lions (if any) become hungry.",
				"3. One animal enters the savannah. This can be a zebra, a fed lion or a hungry lion.",
				"4. Let m = min(number of zebras, number of hungry lions); then m hungry lions eat m zebras and become m fed lions.",
				"The Savannah math problem is to determine how many distinct populations are possible after n weeks.",
				"After step 4, there are either zero zebras or zero hungry lions (or both). Therefore, all possible populations can be positioned in a two-dimensional table, with the number of zebras on the positive part of one axis, the number of hungry lions on the negative part of the same axis and the total number of lions on the other axis (thus, a hungry lion is a lion with a deficit of one zebra). Tabulating the minimum number of weeks in which each population can occur, we get:",
				"zebras",
				"   8 |   8    9   11   14   18",
				"   7 |   7    8   10   13   17",
				"   6 |   6    7    9   12   16",
				"   5 |   5    6    8   11   15",
				"   4 |   4    5    7   10   14",
				"   3 |   3    4    6    9   13",
				"   2 |   2    3    5    8   12",
				"   1 |   1    2    4    7   11",
				"   0 |   0    1    3    6   10",
				"  -1 |        1    2    5    9",
				"  -2 |             2    4    8",
				"  -3 |                  4    7",
				"  -4 |                       7",
				"     +------------------------------",
				"         0    1    2    3    4 lions",
				"A(n) is the number of n's in a sufficiently large version of this table.",
				"The row for Z=0 is equal to the sequence of triangular numbers (A000217).",
				"The number of columns with new entries in week n is the integer inverse of triangular number (A002024) plus 1. The number of new entries in week n is usually the number of columns, except when n is a triangular number plus 1; then a new column is started and the number of new entries is the number of columns plus 1."
			],
			"formula": [
				"a(n) = A002024(n) + A010054(n) + 1."
			],
			"example": [
				"After one week, there are 3 possible populations, depending on which animal entered the savannah: one zebra (Z), one fed lion (F), one hungry lion (H). After two weeks, from Z we get: 2Z, ZF, and (ZH-\u003e) F; from F (which becomes H in the second step) we get: (ZH-\u003e) F, FH and 2H. From H (which becomes the empty set in the first step): Z, F and H. Overall, there are 4 new possible populations that were not possible after the first week: 2Z, ZF, FH, and 2H."
			],
			"xref": [
				"Cf. A002024, A010054.",
				"See A332027 and A332028 for the total number of possibilities."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Jan Ritsema van Eck_ and _Ali Sada_, Feb 05 2020",
			"references": 2,
			"revision": 22,
			"time": "2020-02-08T09:22:28-05:00",
			"created": "2020-02-08T09:22:28-05:00"
		}
	]
}