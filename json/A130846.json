{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A130846",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 130846,
			"data": "2,3,23,4,235,35,26,347,237,58,2359,349,2610,311,235711,45712,2313,3813,2614,345915,235915,716,2371017,3417,2561118,3581119,2319,41220,237921,35791321,2561322,3423,23101423,824,2351525,3457111525,2671126,391627",
			"name": "Replace n with the concatenation of its anti-divisors.",
			"comment": [
				"Number of anti-divisors concatenated to form a(n) is A066272(n). We may consider prime values of the concatenated anti-divisor sequence and we may iterate it, i.e. n, a(n), a(a(n)), a(a(a(n))) which leads to questions of trajectory, cycles, fixed points.",
				"See A066272 for definition of anti-divisor.",
				"Primes in this sequence are at n=3,4,5,10,14,16,40,46,100,145,149,... - _R. J. Mathar_, Jul 24 2007"
			],
			"link": [
				"Jon Perry, \u003ca href=\"/A066272/a066272a.html\"\u003eThe Anti-Divisor\u003c/a\u003e, cached copy.",
				"Jonathan Vos Post, \u003ca href=\"/A130846/a130846.txt\"\u003eFactors of first 62 terms\u003c/a\u003e"
			],
			"example": [
				"3: 2, so a(3) = 2.",
				"4: 3, so a(4) = 3.",
				"5: 2, 3, so a(5) = 23.",
				"6: 4, so a(6) = 4.",
				"7: 2, 3, 5, so a(7) = 235.",
				"17: 2, 3, 5, 7, 11, so a(17) = 235711"
			],
			"maple": [
				"antiDivs := proc(n) local resul,odd2n,r ; resul := {} ; for r in ( numtheory[divisors](2*n-1) union numtheory[divisors](2*n+1) ) do if n mod r \u003c\u003e 0 and r\u003e 1 and r \u003c n then resul := resul union {r} ; fi ; od ; odd2n := numtheory[divisors](2*n) ; for r in odd2n do if ( r mod 2 = 1) and r \u003e 2 then resul := resul union {2*n/r} ; fi ; od ; RETURN(resul) ; end: A130846 := proc(n) cat(op(antiDivs(n))) ; end: seq(A130846(n),n=3..80) ; # _R. J. Mathar_, Jul 24 2007",
				"# Or:",
				"P:=proc(n) local a,k; a:=[]; for k from 2 to n-1 do",
				"if abs((n mod k)-k/2)\u003c1 then a:=[op(a),k]; fi; od;",
				"parse(cat(op(a))); end: seq(P(i),i=3..40); # _Paolo P. Lava_, Mar 29 2018"
			],
			"program": [
				"(Python)",
				"from sympy.ntheory.factor_ import antidivisors",
				"def A130846(n): return int(''.join(str(s) for s in antidivisors(n))) # _Chai Wah Wu_, Dec 08 2021"
			],
			"xref": [
				"Cf. A037278, A066272, A120712, A106708, A130799."
			],
			"keyword": "base,easy,nonn",
			"offset": "3,1",
			"author": "_Jonathan Vos Post_, Jul 20 2007, Jul 22 2007",
			"ext": [
				"More terms from _R. J. Mathar_, Jul 24 2007"
			],
			"references": 4,
			"revision": 21,
			"time": "2021-12-09T01:06:40-05:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}