{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006052",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6052,
			"id": "M5482",
			"data": "1,0,1,880,275305224",
			"name": "Number of magic squares of order n composed of the numbers from 1 to n^2, counted up to rotations and reflections.",
			"comment": [
				"a(4) computed by Frenicle de Bessy (1605 ? - 1675), published in 1693. The article mentions the 880 squares and considers also 5*5, 6*6, 8*8, and other squares. - _Paul Curtz_, Jul 13 and Aug 12 2011",
				"a(5) computed by Richard C. Schroeppel in 1973.",
				"According to Pinn and Wieczerkowski, a(6) = (0.17745 +- 0.00016) * 10^20. - _R. K. Guy_, May 01 2004"
			],
			"reference": [
				"E. R. Berlekamp, J. H. Conway and R. K. Guy, Winning Ways, Vol. II, pp. 778-783 gives the 880 4 X 4 squares.",
				"M. Gardner, Mathematical Games, Sci. Amer. Vol. 249 (No. 1, 1976), p. 118.",
				"M. Gardner, Time Travel and Other Mathematical Bewilderments. Freeman, NY, 1988, p. 216.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Ian Cameron, Adam Rogers and Peter Loly, \u003ca href=\"http://www.physics.umanitoba.ca/~icamern/Poland2012/Data/Bewedlo%20Codex.pdf\"\u003e\"The Library of Magical Squares\" -- a summary of the main results for the Shannon entropy of magic and Latin squares: isentropic clans and indexing, in celebration of George Styan's 75th\u003c/a\u003e.",
				"Frenicle de Bessy, \u003ca href=\"http://babel.hathitrust.org/cgi/pt?u=1\u0026amp;num=423\u0026amp;seq=11\u0026amp;view=image\u0026amp;size=100\u0026amp;id=ucm.5323750390\"\u003eDes carrez ou tables magiques\u003c/a\u003e, Divers ouvrages de mathématique et de physique (1693), pp. 423-483.",
				"Frenicle de Bessy, \u003ca href=\"http://babel.hathitrust.org/cgi/pt?u=1\u0026amp;num=484\u0026amp;seq=9\u0026amp;view=image\u0026amp;size=100\u0026amp;id=ucm.5323750390\"\u003eTable générale des carrez de quatre\u003c/a\u003e, Divers ouvrages de mathématique et de physique (1693), pp. 484-503.",
				"Skylar R. Croy, Jeremy A. Hansen, and Daniel J. McQuillan, \u003ca href=\"https://www.aaai.org/ocs/index.php/SOCS/SOCS16/paper/viewFile/13973/13254?fbclid=IwAR3ZZ24E8vLtbrdpQ-OijrEhiUydRed1_DZP-GJk9jxczLzuuBD29XuoalM\"\u003eCalculating the Number of Order-6 Magic Squares with Modular Lifting\u003c/a\u003e, Proceedings of the Ninth International Symposium on Combinatorial Search (SoCS 2016).",
				"Mahadi Hasan, Md. Masbaul Alam Polash, \u003ca href=\"https://doi.org/10.1007/978-981-13-8942-9_7\"\u003eAn Efficient Constraint-Based Local Search for Maximizing Water Retention on Magic Squares\u003c/a\u003e, Emerging Trends in Electrical, Communications, and Information Technologies, Lecture Notes in Electrical Engineering book series (LNEE 2019) Vol. 569, 71-79.",
				"I. Peterson, \u003ca href=\"http://www.maa.org/mathland/mathtrek_10_18_99.html\"\u003eMagic Tesseracts\u003c/a\u003e [Broken link?]",
				"K. Pinn and C. Wieczerkowski, \u003ca href=\"http://www.arXiv.org/abs/cond-mat/9804109\"\u003eNumber of magic squares from parallel tempering Monte Carlo\u003c/a\u003e, Internat. J. Modern Phys., 9 (4) (1998) 541-546.",
				"Artem Ripatti, \u003ca href=\"https://arxiv.org/abs/1807.02983\"\u003eOn the number of semi-magic squares of order 6\u003c/a\u003e, arXiv:1807.02983 [math.CO], 2018. See Table 1 p. 2.",
				"R. Schroeppel, \u003ca href=\"/A006052/a006052_2.pdf\"\u003eEmails to N. J. A. Sloane, Jun. 1991\u003c/a\u003e",
				"N. J. A. Sloane \u0026 J. R. Hendricks, \u003ca href=\"/A006052/a006052_3.pdf\"\u003eCorrespondence, 1974\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/MagicSquare.html\"\u003eMagic Square\u003c/a\u003e",
				"\u003ca href=\"/index/Mag#magic\"\u003eIndex entries for sequences related to magic squares\u003c/a\u003e"
			],
			"example": [
				"An illustration of the unique (up to rotations and reflections) magic square of order 3:",
				"  +---+---+---+",
				"  | 2 | 7 | 6 |",
				"  +---+---+---+",
				"  | 9 | 5 | 1 |",
				"  +---+---+---+",
				"  | 4 | 3 | 8 |",
				"  +---+---+---+"
			],
			"xref": [
				"Cf. A270876, A271103, A271104."
			],
			"keyword": "nonn,hard,nice,more",
			"offset": "1,4",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Definition corrected by _Max Alekseyev_, Dec 25 2015"
			],
			"references": 35,
			"revision": 75,
			"time": "2019-12-27T07:46:02-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}