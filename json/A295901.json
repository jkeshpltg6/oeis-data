{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A295901",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 295901,
			"data": "1,5,8,20,24,40,48,80,88,120,120,160,168,240,240,320,288,312,360,480,384,408,528,640,616,520,648,960,840,816,960,1280,1072,1440,1248,1248,1368,1224,1360,1920,1680,1920,1848,1632,1872,2640,2208,2560,2384,3016",
			"name": "Unique sequence satisfying SumXOR_{d divides n} a(d) = n^2 for any n \u003e 0, where SumXOR is the analog of summation under the binary XOR operation.",
			"comment": [
				"This sequence is a variant of A256739; both sequences have nice graphical features.",
				"Replacing \"SumXOR\" by \"Sum\" in the name leads to the Jordan function J_2 (A007434).",
				"For any sequence f of nonnegative integers with positive indices:",
				"- let x_f be the unique sequence satisfying SumXOR_{d divides n} x_f(d) = f(n) for any n \u003e 0,",
				"- in particular, x_A000027 = A256739 and x_A000290 = a (this sequence),",
				"- also, x_A178910 = A000027 and x_A055895 = A000079,",
				"- see the links section for a gallery of x_f plots for some classic f functions,",
				"- x_f(1) = f(1),",
				"- x_f(p) = f(1) XOR f(p) for any prime p,",
				"- x_A063524 = A008966,",
				"- x_f(n) = SumXOR_{d divides n and n/d is squarefree} f(d) for any n \u003e 0,",
				"- the function x: f -\u003e x_f is a bijection,",
				"- A000004 is the only fixed point of x (i.e. x_f = f if and only if f = A000004),",
				"- for any sequence f, x_{2*f} = 2 * x_f,",
				"- for any sequences g and f, x_{g XOR f} = x_g XOR x_f.",
				"From _Antti Karttunen_, Dec 29 2017: (Start)",
				"The transform x_f described above could be called \"Xor-Moebius transform of f\" because of its analogous construction to Möbius transform with A008683 replaced by A008966 and the summation replaced by cumulative XOR.",
				"(End)"
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A295901/b295901.txt\"\u003eTable of n, a(n) for n = 1..16383\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A295901/a295901_7.png\"\u003eColored scatterplot of the first 2^17-1 terms\u003c/a\u003e (where the color is function of A087207(n) % 8)",
				"Rémy Sigrist, \u003ca href=\"/A295901/a295901.png\"\u003eScatterplot of the first 2^16 terms of x_A000010 (Euler totient function)\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A295901/a295901_1.png\"\u003eScatterplot of the first 2^16 terms of x_A000203 (sigma)\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A295901/a295901_2.png\"\u003eScatterplot of the first 2^16 terms of x_A001157 (sigma_2)\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A295901/a295901_3.png\"\u003eScatterplot of the first 2^16 terms of x_A000040 (prime numbers)\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A295901/a295901_4.png\"\u003eScatterplot of the first 2^16 terms of x_A000720 (PrimePi)\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A295901/a295901_5.png\"\u003eScatterplot of the first 2^16 terms of x_A006370 (Collatz map)\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A295901/a295901_6.png\"\u003eScatterplot of the first 2^16 terms of x_A005132 (Recamán's sequence)\u003c/a\u003e"
			],
			"formula": [
				"a(n) = SumXOR_{d divides n and n/d is squarefree} d^2."
			],
			"program": [
				"(PARI) a(n{, f=k-\u003ek^2}) = my (v=0); fordiv(n,d,if (issquarefree(n/d), v=bitxor(v,f(d)))); return (v)"
			],
			"xref": [
				"Cf. A000004, A000010, A000027, A000040, A000079, A000290, A000720, A001157, A003987, A005132, A006370, A007434, A008966, A055895, A063524, A087207, A178910.",
				"Same transform applied to other sequences: A256739 (A000027), A296206 (A256739), A296207 (A227320), A296203 (A000203), A296208 (A005187), A297110 (A006068), A297108 (A248663), A297106 (A156552)."
			],
			"keyword": "nonn,base",
			"offset": "1,2",
			"author": "_Rémy Sigrist_, Nov 29 2017",
			"references": 15,
			"revision": 26,
			"time": "2017-12-30T10:01:38-05:00",
			"created": "2017-12-02T03:09:37-05:00"
		}
	]
}