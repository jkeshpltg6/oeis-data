{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A246830",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 246830,
			"data": "0,3,2,10,7,4,15,20,13,6,36,29,22,15,8,45,38,31,40,25,10,54,47,72,57,42,27,12,63,104,89,74,59,44,29,14,136,121,106,91,76,61,46,31,16,153,138,123,108,93,78,63,80,49,18,170,155,140,125,110,95,144,113,82,51,20",
			"name": "T(n,k) is the concatenation of n-k and n+k in binary; triangle T(n,k), n\u003e=0, 0\u003c=k\u003c=n, read by rows.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A246830/b246830.txt\"\u003eRows n = 0..127, flattened\u003c/a\u003e"
			],
			"example": [
				"Triangle T(n,k) begins:",
				"   0",
				"   3  2",
				"  10  7  4",
				"  15 20 13  6",
				"  36 29 22 15  8",
				"  45 38 31 40 25 10",
				"  54 47 72 57 42 27 12",
				"Triangle T(n,k) written in binary (with | denoting the concat operation) begins:",
				"     |0",
				"    1|1      |10",
				"   10|10    1|11     |100",
				"   11|11   10|100   1|101    |110",
				"  100|100  11|101  10|110   1|111    |1000",
				"  101|101 100|110  11|111  10|1000  1|1001  |1010",
				"  110|110 101|111 100|1000 11|1001 10|1010 1|1011 |1100"
			],
			"maple": [
				"f:= proc(i, j) local r, h, k; r:=0; h:=0; k:=j;",
				"      while k\u003e0 do r:=r+2^h*irem(k, 2, 'k'); h:=h+1 od; k:=i;",
				"      while k\u003e0 do r:=r+2^h*irem(k, 2, 'k'); h:=h+1 od; r",
				"    end:",
				"T:= (n, k)-\u003e f(n-k, n+k):",
				"seq(seq(T(n, k), k=0..n), n=0..14);"
			],
			"mathematica": [
				"f[i_, j_] := Module[{r, h, k, m}, r=0; h=0; k=j; While[k\u003e0, {k, m} = QuotientRemainder[k, 2]; r = r+2^h*m; h = h+1]; k=i; While[k\u003e0, {k, m} = QuotientRemainder[k, 2]; r = r+2^h*m; h = h+1]; r]; T[n_, k_] := f[n-k, n+k]; Table[Table[T[n, k], {k, 0, n}], {n, 0, 14}] // Flatten (* _Jean-François Alcover_, Oct 03 2016, adapted from Maple *)"
			],
			"program": [
				"(Haskell)",
				"import Data.Function (on)",
				"a246830 n k = a246830_tabl !! n !! k",
				"a246830_row n = a246830_tabl !! n",
				"a246830_tabl = zipWith (zipWith f) a051162_tabl a025581_tabl where",
				"   f x y = foldr (\\b v -\u003e 2 * v + b) 0 $ x |+| y",
				"   (|+|) = (++) `on` a030308_row",
				"-- _Reinhard Zumkeller_, Sep 04 2014",
				"(Python)",
				"A246830 = []",
				"for n in range(10**2):",
				"....for k in range(n):",
				"........A246830.append(int(bin(n-k)[2:]+bin(n+k)[2:],2))",
				"....A246830.append(2*n) # _Chai Wah Wu_, Sep 05 2014"
			],
			"xref": [
				"Column k=0 gives A020330.",
				"T(n+1,n) gives A080565(n+1).",
				"T(2n,n) gives A246831.",
				"Main diagonal gives A005843.",
				"Cf. A007088, A030308, A051162, A025581, A246520 (row maxima)."
			],
			"keyword": "nonn,tabl,base,look,nice",
			"offset": "0,2",
			"author": "_Alois P. Heinz_, Sep 04 2014",
			"references": 5,
			"revision": 28,
			"time": "2016-10-03T16:04:16-04:00",
			"created": "2014-09-04T13:04:56-04:00"
		}
	]
}