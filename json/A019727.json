{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A019727",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 19727,
			"data": "2,5,0,6,6,2,8,2,7,4,6,3,1,0,0,0,5,0,2,4,1,5,7,6,5,2,8,4,8,1,1,0,4,5,2,5,3,0,0,6,9,8,6,7,4,0,6,0,9,9,3,8,3,1,6,6,2,9,9,2,3,5,7,6,3,4,2,2,9,3,6,5,4,6,0,7,8,4,1,9,7,4,9,4,6,5,9,5,8,3,8,3,7,8,0,5,7,2,6",
			"name": "Decimal expansion of sqrt(2*Pi).",
			"comment": [
				"Pickover says that the expression: lim(n -\u003e infinity) e^n(n!) / (n^n * sqrt(n)) = sqrt(2*Pi) is beautiful because it connects Pi, e, radicals, factorials and infinite limits. - _Jason Earls_, Mar 16 2001",
				"Appears in the formula of the normal distribution. - _Johannes W. Meijer_, Feb 23 2013"
			],
			"reference": [
				"Mohammad K. Azarian, An Expression for Pi, Problem #870, College Mathematics Journal, Vol. 39, No. 1, January 2008, pp. 66. Solution appeared in Vol. 40, No. 1, January 2009, pp. 62-64.",
				"Steven R. Finch, Mathematical Constants, Cambridge University Press, 2003, Section 2.15 Glaisher-Kinkelin constant, p. 137.",
				"C. Pickover, Wonders of Numbers, Oxford University Press, NY, 2001, p. 307."
			],
			"link": [
				"Harry J. Smith, \u003ca href=\"/A019727/b019727.txt\"\u003eTable of n, a(n) for n = 1..20000\u003c/a\u003e",
				"K. Kimoto, N. Kurokawa, C. Sonoki, M. Wakayama, \u003ca href=\"http://www.math.u-ryukyu.ac.jp/~kimoto/pdf/kksw2.pdf\"\u003eSome examples of generalized zeta regularized products\u003c/a\u003e, Kodai Math. J. 27 (2004), 321-335.",
				"C. A. Pickover, \u003ca href=\"http://www.zentralblatt-math.org/zmath/en/search/?q=an:0983.00008\u0026amp;format=complete\"\u003eZentralblatt review of Wonders of Numbers, Adventures in Mathematics, Mind and Meaning\u003c/a\u003e",
				"Eric W. Weisstein, \u003ca href=\"http://mathworld.wolfram.com/NormalDistribution.html\"\u003eMathWorld: Normal Distribution\u003c/a\u003e",
				"\u003ca href=\"/index/Tra#transcendental\"\u003eIndex entries for transcendental numbers\u003c/a\u003e"
			],
			"formula": [
				"Equal to lim(n -\u003e infinity) e^n*(n!)/n^n*sqrt(n).",
				"Also equals Integral_{x \u003e= 0} W(1/x^2) where W is the Lambert function, which is also known as ProductLog. - _Jean-François Alcover_, May 27 2013",
				"Also equals the generalized Glaisher-Kinkelin constant A_0, see the Finch reference - _Jean-François Alcover_, Dec 23 2014",
				"Equals exp(-zeta'(0)). See Kimoto et al. - _Michel Marcus_, Jun 27 2019"
			],
			"example": [
				"2.506628274631000502415765284811045253006986740609938316629923576342293...."
			],
			"mathematica": [
				"RealDigits[Sqrt[2Pi],10,120][[1]] (* _Harvey P. Dale_, Dec 12 2012 *)"
			],
			"program": [
				"(PARI) default(realprecision, 20080); x=sqrt(2*Pi); for (n=1, 20000, d=floor(x); x=(x-d)*10; write(\"b019727.txt\", n, \" \", d)); \\\\ _Harry J. Smith_, May 31 2009",
				"(Maxima) fpprec: 100$ ev(bfloat(sqrt(2*%pi))); /* _Martin Ettl_, Oct 11 2012 */",
				"(MAGMA) R:= RealField(100); Sqrt(2*Pi(R)); // _G. C. Greubel_, Mar 08 2018"
			],
			"xref": [
				"Cf. A058293 (continued fraction), A231863 (inverse), A000796 (Pi)."
			],
			"keyword": "nonn,cons",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_",
			"references": 37,
			"revision": 72,
			"time": "2020-07-11T03:56:37-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}