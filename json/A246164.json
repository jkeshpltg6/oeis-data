{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A246164",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 246164,
			"data": "1,2,4,11,8,5,3,7,6,9,13,17,47,31,14,61,21,42,185,24,87,319,62,12,25,19,10,59,20,15,37,229,49,22,67,76,415,103,28,18,55,137,34,41,16,27,97,78,425,109,29,1627,222,54,283,433,79,373,3053,33,131,647,108,847,133,745,6943,44,193,1053,160,504,4333,587,99",
			"name": "Permutation of natural numbers: a(1) = 1, a(A065621(n)) = A014580(a(n-1)), a(A048724(n)) = A091242(a(n)), where A065621(n) and A048724(n) are the reversing binary representation of n and -n, respectively, and A014580 resp. A091242 are the binary coded irreducible resp. reducible polynomials over GF(2).",
			"comment": [
				"This is an instance of entanglement permutation, where the two complementary pairs to be entangled with each other are A065621/A048724 and A014580/A091242 (binary codes for irreducible and reducible polynomials over GF(2)).",
				"The former are themselves permutations of A000069/A001969 (odious and evil numbers), which means that this permutation shares many properties with A246162.",
				"For the comments about the cycle structure, please see A246163."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A246164/b246164.txt\"\u003eTable of n, a(n) for n = 1..195\u003c/a\u003e",
				"\u003ca href=\"/index/Ge#GF2X\"\u003eIndex entries for sequences operating on GF(2)[X]-polynomials\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"a(1) = 1, and for n \u003e 1, if A010060(n) = 1 [i.e. when n is an odious number], a(n) = A014580(a(A065620(n)-1)), otherwise a(n) = A091242(a(- (A065620(n)))). [A065620 Converts sum of powers of 2 in binary representation of n to an alternating sum].",
				"As a composition of related permutations:",
				"a(n) = A246202(A193231(n)).",
				"a(n) = A245702(A234025(n)).",
				"a(n) = A246162(A234612(n)).",
				"a(n) = A193231(A246204(A193231(n))).",
				"For all n \u003e 1, A091225(a(n)) = A010060(n). [Maps odious numbers to binary representations of irreducible GF(2) polynomials (A014580) and evil numbers to the corresponding representations of reducible polynomials (A091242), in some order. A246162 has the same property]."
			],
			"program": [
				"(Scheme, with memoization-macro definec, two alternative definitions)",
				"(definec (A246164 n) (cond ((= 1 n) n) ((= 1 (A010060 n)) (A014580 (A246164 (- (A065620 n) 1)))) (else (A091242 (A246164 (- (A065620 n)))))))",
				"(definec (A246164 n) (cond ((= 1 n) n) ((= 1 (A010060 n)) (A014580 (A246164 (- (A246160 n) 1)))) (else (A091242 (A246164 (A246159 n))))))"
			],
			"xref": [
				"Inverse: A246163.",
				"Similar or related permutations: A246206, A246202, A193231, A245702, A234025, A246162, A234612, A246204.",
				"Cf. A010060, A014580, A048724, A065620, A065621, A091242, A246159, A246160."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Antti Karttunen_, Aug 19 2014",
			"references": 7,
			"revision": 17,
			"time": "2014-08-20T23:59:02-04:00",
			"created": "2014-08-20T23:59:02-04:00"
		}
	]
}