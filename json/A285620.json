{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A285620",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 285620,
			"data": "1,2,2,4,3,8,4,12,8,20,8,48,14,48,44,88,36,192,60,336,200,416,188,1344,424,1400,944,3104,1182,8768,2192,8784,6768,16460,11144,46848,14602,58288,44424,138432,52488,355200,99880,432576,351712,762608,364724,2151936,798960",
			"name": "Number of circulant graphs on n vertices up to Cayley isomorphism.",
			"comment": [
				"Two circulant graphs are Cayley isomorphic if there is a d, which is necessarily prime to n, that transforms through multiplication modulo n the step values of one graph into those of the other. For squarefree n this is the only way that two circulant graphs can be isomorphic (See A049287)."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A285620/b285620.txt\"\u003eTable of n, a(n) for n = 1..200\u003c/a\u003e",
				"V. A. Liskovets and R. Poeschel, \u003ca href=\"http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.84.89\"\u003eOn the enumeration of circulant graphs of prime-power and squarefree orders.\u003c/a\u003e"
			],
			"mathematica": [
				"IsLeastPoint[s_, f_] := Module[{t = f[s]}, While[t \u003e s, t=f[t]]; Boole[s == t]];",
				"c[n_, k_] := Sum[IsLeastPoint[u, Abs[Mod[#*k + Quotient[n, 2], n] - Quotient[n, 2]]\u0026], {u, 1, n/2}];",
				"a[n_] := If[n \u003c 3, n, Sum[If[GCD[k, n] == 1, 2^c[n, k], 0]*2/EulerPhi[n], {k, 1, n/2}]];",
				"Array[a, 50] (* _Jean-François Alcover_, Jun 12 2017, translated from PARI *)"
			],
			"program": [
				"(PARI)",
				"IsLeastPoint(s,f)={my(t=f(s)); while(t\u003es,t=f(t));s==t}",
				"C(n,k)=sum(u=1,n/2,IsLeastPoint(u,v-\u003eabs((v*k+n\\2)%n-n\\2)));",
				"a(n)=if(n\u003c3, n, sum(k=1, n/2, if (gcd(k, n)==1, 2^C(n,k),0))*2/eulerphi(n));"
			],
			"xref": [
				"Cf. A049287, A056391 (circulant digraphs), A049297, A038782."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Andrew Howroyd_, Apr 22 2017",
			"references": 4,
			"revision": 20,
			"time": "2017-10-02T15:19:40-04:00",
			"created": "2017-04-24T00:23:55-04:00"
		}
	]
}