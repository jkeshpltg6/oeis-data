{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A269252",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 269252,
			"data": "-1,-1,1,1,2,1,2,1,2,2,2,1,3,1,3,2,2,1,14,1,2,2,3,1,2,5,2,36,2,1,2,1,15,-1,6,2,3,1,2,2,6,1,3,1,2,2,2,1,2,3,2,-1,3,1,2,2,2,6,3,1,2,1,30,3,2,2,2,1,2,5,2,1,5,1,6,3,2,6,3,1,8,6,14,1,3",
			"name": "Define a sequence by s(k) = n*s(k-1) - s(k-2), with s(0) = 1, s(1) = n - 1. a(n) is the smallest index k such that s(k) is prime, or -1 if no such k exists.",
			"comment": [
				"For n \u003e= 2, positive integer k yielding the smallest prime of the form (x^y + 1/x^y)/(x + 1/x), where x = (sqrt(n+2) +/- sqrt(n-2))/2 and y = 2*k + 1, or -1 if no such k exists.",
				"Every positive term belongs to A005097.",
				"For detailed theory, see [Hone]. - _L. Edson Jeffery_, Feb 09 2018"
			],
			"link": [
				"C. K. Caldwell, Top Twenty page, \u003ca href=\"http://primes.utm.edu/top20/page.php?id=47\"\u003eLehmer number\u003c/a\u003e",
				"Andrew N. W. Hone, et al., \u003ca href=\"https://arxiv.org/abs/1802.01793\"\u003eOn a family of sequences related to Chebyshev polynomials\u003c/a\u003e, arXiv:1802.01793 [math.NT], 2018.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Lehmer_number\"\u003eLehmer number\u003c/a\u003e"
			],
			"formula": [
				"If n is prime then a(n+1) = 1."
			],
			"example": [
				"Let b(k) be the recursive sequence defined by the initial conditions b(0) = 1, b(1) = 10, and the recursive equation b(k) = 11*b(k-1) - b(k-2). a(11) = 2 because b(2) = 109 is the smallest prime in b(k).",
				"Let c(k) be the recursive sequence defined by the initial conditions c(0) = 1, c(1) = 12, and the recursive equation c(k) = 13*c(k-1) - c(k-2). a(13) = 3 because c(3) = 2003 is the smallest prime in c(k)."
			],
			"mathematica": [
				"s[k_, m_] := s[k, m] = Which[k == 0, 1, k == 1, 1 + m, True, m s[k - 1, m] - s[k - 2, m]]; Table[SelectFirst[Range[120], PrimeQ@ Abs@ s[#, -n] \u0026] /. k_ /; MissingQ@ k -\u003e -1, {n, 85}] (* _Michael De Vlieger_, Feb 03 2018 *)"
			],
			"program": [
				"lst:=[]; for n in [1..85] do if n in [1, 2, 34, 52] then Append(~lst, -1); else a:=1; c:=1; t:=0; repeat b:=n*a-c; c:=a; a:=b; t+:=1; until IsPrime(a); Append(~lst, t); end if; end for; lst;"
			],
			"xref": [
				"Cf. A005097, A269251, A269253, A269254, A299045 (array used to compute this sequence).",
				"Cf. A294099, A298675, A298677, A298878, A299071, A285992, A299107, A299109, A088165, A117522, A299100, A299101, A113501."
			],
			"keyword": "sign",
			"offset": "1,5",
			"author": "_Arkadiusz Wesolowski_, Jul 09 2016",
			"references": 5,
			"revision": 49,
			"time": "2018-02-09T04:28:24-05:00",
			"created": "2016-07-24T20:06:07-04:00"
		}
	]
}