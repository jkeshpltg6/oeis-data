{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A034343",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 34343,
			"data": "1,2,4,8,16,36,80,194,506,1449,4631,17106,74820,404283,2815595,26390082,344330452,6365590987,167062019455,6182453531508,319847262335488,22968149462624180,2277881694784784852",
			"name": "Number of inequivalent binary linear codes of length n and any dimension k \u003c= n containing no column of zeros.",
			"comment": [
				"Comment from _N. J. A. Sloane_, Nov 27 2017 (Start)",
				"Also, (by taking duals) number of inequivalent binary linear codes of length n and any dimension k \u003c= n containing no codewords of weight 1.",
				"It follows from the theorem on page 64 of Schwarzenberger (1980), this is also the number of Bravais types of orthogonal lattices in dimension n. (End)",
				"Also the number of loopless binary matroids on n points."
			],
			"reference": [
				"R. L. E. Schwarzenberger, N-Dimensional Crystallography. Pitman, London, 1980, pages 64 and 65.",
				"M. Wild, Enumeration of binary and ternary matroids and other applications of the Brylawski-Lucas Theorem, Preprint No. 1693, Tech. Hochschule Darmstadt, 1994"
			],
			"link": [
				"Discrete algorithms at the University of Bayreuth, \u003ca href=\"http://www.algorithm.uni-bayreuth.de/en/research/SYMMETRICA/\"\u003eSymmetrica\u003c/a\u003e. [This package was used to compute T_{nk2} using the cycle index of PGL_k(2). Here a(n) = T_{nn2}.]",
				"Harald Fripertinger, \u003ca href=\"http://www.mathe2.uni-bayreuth.de/frib/codes/tables.html\"\u003eIsometry Classes of Codes\u003c/a\u003e.",
				"Harald Fripertinger, \u003ca href=\"http://www.mathe2.uni-bayreuth.de/frib/codes/tables_3.html\"\u003eTnk2: Number of the isometry classes of all binary (n,r)-codes for 1 \u003c= r \u003c= k without zero-columns\u003c/a\u003e. [This is a rectangular array whose main diagonal is a(n).]",
				"Harald Fripertinger, \u003ca href=\"https://imsc.uni-graz.at/fripertinger/codes_bms.html\"\u003eEnumeration of isometry classes of linear (n,k)-codes over GF(q) in SYMMETRICA\u003c/a\u003e, Bayreuther Mathematische Schriften 49 (1995), 215-223. [See pp. 216-218. A C-program is given for calculating T_{nk2} in Symmetrica. Here a(n) = T_{nn2}.]",
				"Harald Fripertinger, \u003ca href=\"https://doi.org/10.1016/S0024-3795(96)00530-7\"\u003eCycle of indices of linear, affine, and projective groups\u003c/a\u003e, Linear Algebra and its Applications 263 (1997), 133-156. [See p. 152 for the computation of T_{nk2}.]",
				"H. Fripertinger and A. Kerber, \u003ca href=\"https://doi.org/10.1007/3-540-60114-7_15\"\u003eIsometry classes of indecomposable linear codes\u003c/a\u003e. In: G. Cohen, M. Giusti, T. Mora (eds), Applied Algebra, Algebraic Algorithms and Error-Correcting Codes, 11th International Symposium, AAECC 1995, Lect. Notes Comp. Sci. 948 (1995), pp. 194-204. [The notation for A076832(n,k) is T_{nk2}. Here a(n) = A076832(n,k) = T_{nn2}.]",
				"R. L. E. Schwarzenberger, \u003ca href=\"https://doi.org/10.1017/S0305004100048696\"\u003eCrystallography in spaces of arbitrary dimension\u003c/a\u003e, Proc. Camb. Phil. Soc., 76(1) (1974), 23-32.",
				"David Slepian, \u003ca href=\"https://archive.org/details/bstj39-5-1219\"\u003eSome further theory of group codes\u003c/a\u003e, Bell System Tech. J. 39(5) (1960), 1219-1252.",
				"David Slepian, \u003ca href=\"https://doi.org/10.1002/j.1538-7305.1960.tb03958.x\"\u003eSome further theory of group codes\u003c/a\u003e, Bell System Tech. J. 39(5) (1960), 1219-1252.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Cycle_index\"\u003eCycle index\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Projective_linear_group\"\u003eProjective linear group\u003c/a\u003e.",
				"\u003ca href=\"/index/Coa#codes_binary_linear\"\u003eIndex entries for sequences related to binary linear codes\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A076832(n,n). - _Petros Hadjicostas_, Sep 30 2019"
			],
			"xref": [
				"Cf. A034337, A034338, A034339, A034340, A034341, A034342.",
				"A diagonal of A076832."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_.",
			"references": 6,
			"revision": 27,
			"time": "2019-10-01T09:09:39-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}