{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A338158",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 338158,
			"data": "20,30,12,1,175,450,425,180,33,2,980,3308,4458,3065,1140,225,22,1,4116,16468,27293,24262,12521,3796,653,58,2,14112,63522,120848,126518,79506,30681,7132,933,58,1,41580,204180,429030,503664,361690,163380,45885,7588,648,20",
			"name": "Triangle read by rows: T(n,k) is the coefficient of x^k in the ZZ polynomial of the hexagonal graphene flake O(3,3,n).",
			"comment": [
				"The maximum k for which T(n,k) is nonzero, denoted as Cl(n), is usually referred to as the Clar number of O(3,3,n); one has: Cl(1)=3, Cl(2)=5, Cl(3)=7, Cl(4)=8, and Cl(n)=9 for n\u003e4.",
				"T(n,k) denotes the number of Clar covers of order k in the hexagonal graphene flake O(3,3,n).",
				"The Kekulé number of O(3,3,n) is given by T(n, 0).",
				"ZZ polynomials of hexagonal graphene flakes O(3,3,n) with n=1..10 are listed in Eq.(36) of Chou, Li and Witek.",
				"ZZ polynomials of hexagonal graphene flakes O(3,3,n) with any n can be obtained from Eq.(13) of Witek, Langner, Mos and Chou.",
				"ZZ polynomials of hexagonal graphene flakes O(3,3,n) can be also computed using ZZDecomposer (see links below), a graphical program to compute ZZ polynomials of general benzenoids."
			],
			"link": [
				"C.-P. Chou, \u003ca href=\"https://bitbucket.org/solccp/zzdecomposer_binary/downloads/\"\u003eZZDecomposer\u003c\\a\u003e.",
				"C.-P. Chou, Y. Li and H. A. Witek, \u003ca href=\"http://match.pmf.kg.ac.rs/electronic_versions/Match68/n1/match68n1_31-64.pdf\"\u003eZhang-Zhang Polynomials of Various Classes of Benzenoid Systems\u003c/a\u003e, MATCH Commun. Math. Comput. Chem. 68 (2012), 31-64.",
				"C.-P. Chou and H. A. Witek, \u003ca href=\"http://match.pmf.kg.ac.rs/electronic_versions/Match71/n3/match71n3_741-764.pdf\"\u003eZZDecomposer: A Graphical Toolkit for Analyzing the Zhang-Zhang Polynomials of Benzenoid Structures\u003c/a\u003e, MATCH Commun. Math. Comput. Chem. 71 (2014), 741-764.",
				"C.-P. Chou and H. A. Witek, \u003ca href=\"http://match.pmf.kg.ac.rs/electronic_versions/Match72/n1/match72n1_75-104.pdf\"\u003eDetermination of Zhang-Zhang Polynomials for various Classes of Benzenoid Systems: Non-Heuristic Approach\u003c/a\u003e, MATCH Commun. Math. Comput. Chem. 72 (2014), 75-104.",
				"S. J. Cyvin and I. Gutman, \u003ca href=\"https://doi.org/10.1007/978-3-662-00892-8\"\u003eKekulé structures in benzenoid hydrocarbons\u003c/a\u003e, Lecture Notes in Chemistry, No. 46, Springer, New York, 1988 (see p. 105 for a graphical definition of O(3,3,n)).",
				"H. A. Witek, J. Langner, G. Mos, and C.-P. Chou, \u003ca href=\"http://match.pmf.kg.ac.rs/electronic_versions/Match78/n2/match78n2_487-504.pdf\"\u003eZhang-Zhang Polynomials of Regular 5-tier Benzeonid Strips\u003c/a\u003e, MATCH Commun. Math. Comput. Chem. 78 (2017), 487-504.",
				"H. Zhang and F. Zhang, \u003ca href=\"https://doi.org/10.1016/0166-218X(95)00081-2\"\u003eThe Clar covering polynomial of hexagonal systems I\u003c/a\u003e, Discrete Appl. Math. 69 (1996), 147-167 (ZZ polynomial is defined by Eq.(2.1) and working formula is given by Eq.(2.2))."
			],
			"formula": [
				"T(n,k) = Sum_{l=0..9} (binomial(k+l,k)*(binomial(9,k+l)*binomial(n,k+l)+(10*binomial(7,k+l-2)-binomial(6,k+l-2))*binomial(n+1,k+l)+(20*binomial(5,k+l-4)+binomial(3,k+l-3)-binomial(3,k+l-5))*binomial(n+2,k+l)+(10*binomial(3,k+l-6)+binomial(2,k+l-5)+binomial(3,k+l-5))*binomial(n+3,k+l)+binomial(2,k+l-7)*binomial(n+4,k+l)).",
				"This formula can be obtained by a double sum rotation from Eq.(13) of Witek, Langner, Mos and Chou. Eq.(13) was first discovered heuristically as Eq.(37) of Chou, Li and Witek; a formal proof was given in Eqs.(66-71) on pp. 100-102 of Chou and Witek."
			],
			"example": [
				"Triangle begins:",
				"       k=0    k=1    k=2    k=3    k=4    k=5   k=6  k=7 k=8 k=9",
				"n=1:    20     30     12      1",
				"n=2:   175    450    425    180     33      2",
				"n=3:   980   3308   4458   3065   1140    225    22    1",
				"n=4:  4116  16468  27293  24262  12521   3796   653   58   2",
				"n=5: 14112  63522 120848 126518  79506  30681  7132  933  58   1",
				"n=6: 41580 204180 429030 503664 361690 163380 45885 7588 648  20",
				"   ...",
				"Row n=4 corresponds to the polynomial 4116 + 16468*x + 27293*x^2 + 24262*x^3 + 12521*x^4 + 3796*x^5 + 653*x^6 + 58*x^7 + 2*x^8."
			],
			"maple": [
				"(n,k)-\u003eadd(binomial(i+k,k)*(binomial(9,i+k)*binomial(n,i+k)+(10*binomial(7,i+k-2)-binomial(6,i+k-2))*binomial(n+1,i+k)+(20*binomial(5,i+k-4)+binomial(3,i+k-3)-binomial(3,i+k-5))*binomial(n+2,i+k)+(10*binomial(3,i+k-6)+binomial(2,i+k-5)+binomial(3,i+k-5))*binomial(n+3,i+k)+binomial(2,i+k-7)*binomial(n+4,i+k)),i = 0..9)"
			],
			"xref": [
				"Column k=0 is A047819.",
				"Other representation of ZZ polynomials of O(3,3,n) is given by A338217."
			],
			"keyword": "nonn,tabf",
			"offset": "1,1",
			"author": "_Henryk A. Witek_, Oct 14 2020",
			"references": 1,
			"revision": 31,
			"time": "2021-01-09T02:10:00-05:00",
			"created": "2020-10-18T22:42:10-04:00"
		}
	]
}