{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A326722",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 326722,
			"data": "1,0,1,-1,0,1,0,-1,0,1,2,0,-4,0,2,0,5,0,-10,0,5,-16,0,48,0,-48,0,16,0,-61,0,183,0,-183,0,61,272,0,-1088,0,1632,0,-1088,0,272,0,1385,0,-5540,0,8310,0,-5540,0,1385,-7936,0,39680,0,-79360,0,79360,0,-39680,0,7936",
			"name": "T(n, k) = n! * [x^k] [y^n] sec(z)(x + z*sin(z)/y) where z = y*sqrt(x^2 - 1) for 0 \u003c= k \u003c= n+1 and T(-1, 0) = 1, triangle read by rows.",
			"formula": [
				"T(n, k) = A178111(n, k)*A000111(n-1) if n \u003e 1 else k^n, assuming 0 based triangle.",
				"Exponential generating functions for the columns (n \u003e= 0) are:",
				"egf_col0(x) = -tanh(x).",
				"egf_col1(x) = sech(x).",
				"egf_col2(x) = (tanh(x) + x*sech(x)^2)/2.",
				"egf_col3(x) = x*tanh(x)*sech(x)/2.",
				"egf_col4(x) = (tanh(x) + x*(2*x*tanh(x) - 1)*sech(x)^2)/8.",
				"egf_col5(x) = x*sech(x)*(x + tanh(x) - 2*x*sech(x)^2)/8.",
				"egf_col6(x) = (3*tanh(x) + x*sech(x)^2*(4*x^2 - 6*x^2*sech(x)^2 - 3))/48.",
				"A recurrence of the row polynomials based on offset 0 is given by the recurrence of the Euler-Bernoulli-Entringer numbers A008281 combined with Paul Barry's A178111. See the Maple script."
			],
			"example": [
				"[-1]     1;",
				"[ 0]     0,    1;",
				"[ 1]    -1,    0,     1;",
				"[ 2]     0,   -1,     0,     1;",
				"[ 3]     2,    0,    -4,     0,      2;",
				"[ 4]     0,    5,     0,   -10,      0,    5;",
				"[ 5]   -16,    0,    48,     0,    -48,    0,    16;",
				"[ 6]     0,  -61,     0,   183,      0, -183,     0,    61;",
				"[ 7]   272,    0, -1088,     0,   1632,    0, -1088,     0,    272;",
				"[ 8]     0, 1385,     0, -5540,      0, 8310,     0, -5540,      0, 1385;",
				"[ 9] -7936,    0, 39680,     0, -79360,    0, 79360,     0, -39680,    0, 7936;"
			],
			"maple": [
				"z := y*sqrt(x^2 - 1): gf := sec(z)*(x + z*sin(z)/y):",
				"ser := series(gf, y, 16): cy :=  n -\u003e convert(n!*coeff(ser, y, n), polynom):",
				"Trow := n -\u003e `if`(n=-1, [1], PolynomialTools:-CoefficientList(cy(n), x)):",
				"ListTools:-Flatten([seq(Trow(n), n=-1..9)]);",
				"# Alternatively, compute the row polynomials based on offset 0 by recurrence.",
				"RowPoly := proc(n) local E, P, L;",
				"    E := proc(n, k) option remember; if k = 0 then return(`if`(n = 0, 1, 0)) fi;",
				"        E(n, k-1) + E(n-1, n-k) end:",
				"    P := proc(n) option remember; `if`(n \u003c 2, x^n,",
				"        x*P(n-1) - ((1 + (-1)^n)/2)*P(n-2)) end:",
				"    # `if`(n = 0, 1, sort(expand(P(n)*E(n-1,n-1)), x, ascending)):",
				"    L := n -\u003e PolynomialTools:-CoefficientList(P(n), x):",
				"    `if`(n = 0, [1], L(n)*E(n-1, n-1)):",
				"end: for n from 0 to 9 do RowPoly(n) end;",
				"# Alternative:",
				"T := (n, k) -\u003e if n \u003c= 1 then k^n else A178111(n, k)*A000111(n-1) fi:",
				"seq(seq(T(n,k), k=0..n), n=0..10);"
			],
			"mathematica": [
				"z := y Sqrt[x^2 - 1]; gf := Sec[z](x + z Sin[z]/y); ser := Series[gf, {y, 0, 16}];",
				"cy[-1] := {1}; cy[n_] := n! Coefficient[ser, y, n];",
				"row[n_] := CoefficientList[cy[ n], x]; Table[row[n], {n, -1, 9}] // Flatten"
			],
			"program": [
				"(SageMath)",
				"def A326722(n,k):",
				"    if n == 0: return 1",
				"    if is_odd(n-k): return 0",
				"    b = I^(n-k)*binomial(floor(n/2),floor(k/2))",
				"    if is_odd(n): return b*I^(n-1)*euler_number(n-1)",
				"    return 2*b*psi(n-1, 1/2)/pi^n",
				"for n in range(11): print([A326722(n,k) for k in range(n+1)])"
			],
			"xref": [
				"Cf. A000364, A028296, A000182, A000111, A008281, A178111, A012816, A261042, A326721, A326723, A326724.",
				"T(n, 0) = -A155585(n) for n \u003e= 1.",
				"T(n, 1) =  A122045(n) for n \u003e= 0.",
				"|T(2*n-1, 2)| = A024255(n) for n \u003e= 0.",
				"T(n, 3) = A326719(n) for n \u003e= 0.",
				"T(n, 4) = A326718(n) for n \u003e= 0."
			],
			"keyword": "sign,tabl",
			"offset": "-1,11",
			"author": "_Peter Luschny_, Aug 08 2019",
			"references": 7,
			"revision": 28,
			"time": "2019-08-12T05:33:19-04:00",
			"created": "2019-08-09T07:02:21-04:00"
		}
	]
}