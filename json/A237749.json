{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A237749",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 237749,
			"data": "1,1,1,2,10,114,2608,107498,7325650,771505180",
			"name": "The number of possible orderings of the real numbers xi*xj (i \u003c= j), subject to the constraint that x1 \u003e x2 \u003e ... \u003e xn \u003e 0.",
			"comment": [
				"Also, the number of possible orderings of sums xi + xj (i \u003c= j), subject to the constraint that x1 \u003e x2 \u003e ... \u003e xn. Cf. A231085.",
				"Also, the minimum number of linear matrix inequalities needed to characterize the eigenvalues of quantum states that are \"PPT from spectrum\" when the local dimension is n (see Hildebrand link).",
				"A003121 is an upper bound on this sequence.",
				"Alternately, the number of combinatorially different Golomb rulers with n markings (Beck-Bogart-Pham). - _Charles R Greathouse IV_, Feb 18 2014",
				"From _Jon E. Schoenfield_, Jul 03 2015: (Start)",
				"The terms of this sequence would remain unchanged if it were required that each value of xi (and hence each pairwise product xi*xj) be an integer, and the addition of such a constraint suggests a systematic (albeit impractical for larger values of n) way to search through sets of n values of x to find a set that yields each of the a(n) possible orderings of the pairwise products: for x1 = n, n+1, n+2, ..., test every combination of n distinct positive integers of which x1 is the largest. Let M(n) be the smallest integer such that each of the a(n) possible orderings results from at least one combination of integers x1, x2, ..., xn where M(n) \u003e= x1 \u003e x2 \u003e ... \u003e xn; then values of M(n) for n = 2..6 are 2, 5, 13, 29, and 68, respectively.",
				"For any given value of x1, the number of distinct orderings of pairwise products resulting from the binomial(x1-1, n-1) possible combinations of the remaining integers x2..xn provides a lower bound L(x1) for a(n). In general, L(x1) is not monotonically nondecreasing; e.g., for n=6, the (weak) lower bound on a(6)=2608 provided by L(33) is 2428, and L(34)=2423 is weaker still. However -- at least up through n=6 -- each of the a(n) possible orderings results from at least one combination where x1 is exactly M(n); e.g., at n=6, one of the 2608 orderings is missing among all binomial(67,6) = 99,795,696 combinations where x1 \u003c 68, but all 2608 are present among the binomial(67,5) = 9,657,648 combinations where x1=68.",
				"For all n up to at least 6, the number of orderings found among all combinations where x1 \u003c M(n) is a(n)-1, and the one missing ordering of the pairwise products is the one in which xj*xn \u003e (x(j+1))^2 for j=1..n-1. (End)"
			],
			"link": [
				"S. Arunachalam, N. Johnston, V. Russo, \u003ca href=\"http://arxiv.org/abs/1405.5853\"\u003eIs separability from spectrum determined by the partial transpose?\u003c/a\u003e, arXiv preprint arXiv:1405.5853 [quant-ph], 2014-2015.",
				"Matthias Beck, Tristram Bogart, Tu Pham, \u003ca href=\"http://arxiv.org/abs/1110.6154\"\u003eEnumeration of Golomb Rulers and Acyclic Orientations of Mixed Graphs\u003c/a\u003e, arXiv:1110.6154 [math.CO], 2011, Section 5.",
				"R. Hildebrand, \u003ca href=\"http://dx.doi.org/10.1103/PhysRevA.76.052325\"\u003ePositive partial transpose from spectra\u003c/a\u003e. Phys. Rev. A, 76 (5) (2007) 052325, \u003ca href=\"http://arxiv.org/abs/quant-ph/0502170\"\u003e[arXiv]\u003c/a\u003e, arXiv:quant-ph/0502170, 2005.",
				"N. Johnston, \u003ca href=\"http://www.njohnston.ca/2014/02/counting-the-possible-orderings-of-pairwise-multiplication/\"\u003eCounting the possible orderings of pairwise multiplication\u003c/a\u003e",
				"Nathaniel Johnston, Olivia MacLean, \u003ca href=\"https://arxiv.org/abs/1807.06897\"\u003ePairwise Completely Positive Matrices and Conjugate Local Diagonal Unitary Invariant Quantum States\u003c/a\u003e, arXiv:1807.06897 [quant-ph], 2018.",
				"Antti Laaksonen, \u003ca href=\"https://www.cs.helsinki.fi/u/ahslaaks/orderings.html\"\u003eCounting Orderings of Sums\u003c/a\u003e",
				"Steven J. Miller, Carsten Peterson, \u003ca href=\"https://arxiv.org/abs/1709.00520\"\u003eA geometric perspective on the MSTD question\u003c/a\u003e, arXiv:1709.00606 [math.CO], 2017.",
				"Tu Pham, \u003ca href=\"http://math.sfsu.edu/beck/teach/masters/tu.pdf\"\u003eEnumeration of Golomb Rulers\u003c/a\u003e, Master's Thesis (2011) Table 3.1"
			],
			"example": [
				"a(3) = 2 because there are 2 possible orderings of the 6 products a1^2, a2^2, a3^2, a1*a2, a1*a3, a2*a3. Specifically, these orderings are:",
				"a1^2 \u003e a1a2 \u003e a2^2 \u003e a1a3 \u003e a2a3 \u003e a3^2 and",
				"a1^2 \u003e a1a2 \u003e a1a3 \u003e a2^2 \u003e a2a3 \u003e a3^2."
			],
			"xref": [
				"Cf. A003121, A231074, A231085, A259762."
			],
			"keyword": "nonn,hard,more,nice",
			"offset": "0,4",
			"author": "_Nathaniel Johnston_, Feb 12 2014",
			"ext": [
				"a(7) copied from Tu Pham by _Charles R Greathouse IV_, Feb 18 2014",
				"a(8)-a(9) from _Antti Laaksonen_, Jan 10 2019"
			],
			"references": 3,
			"revision": 62,
			"time": "2019-12-14T21:27:32-05:00",
			"created": "2014-02-14T17:09:22-05:00"
		}
	]
}