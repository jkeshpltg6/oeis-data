{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A108235",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 108235,
			"data": "1,1,0,0,8,21,0,0,3040,20505,0,0,10567748,103372655,0,0,142664107305,1836652173363,0,0",
			"name": "Number of partitions of {1,2,...,3n} into n triples (X,Y,Z) each satisfying X+Y=Z.",
			"comment": [
				"a(0)=1 by convention."
			],
			"link": [
				"Matthias Beck and Thomas Zaslavsky, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL13/Zaslavsky/sls.html\"\u003eSix Little Squares and How their Numbers Grow\u003c/a\u003e, Journal of Integer Sequences, 13 (2010), #10.6.2.",
				"Matheplanet \u003ca href=\"https://matheplanet.de/default3.html?article=1899\"\u003eCalculating sequence element a(16) of OEIS A108235\u003c/a\u003e",
				"R. J. Nowakowski, \u003ca href=\"/A104429/a104429.pdf\"\u003eGeneralizations of the Langford-Skolem problem\u003c/a\u003e, M.S. Thesis, Dept. Math., Univ. Calgary, May 1975. [Scanned copy, with permission.]",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Dancing_Links\"\u003eDancing Links\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 0 unless n == 0 or 1 (mod 4). For n == 0 or 1 (mod 4), a(n) = A002849(3n). See A002849 for references and further information."
			],
			"example": [
				"For m = 1 the unique solution is 1 + 2 = 3.",
				"For m = 4 there are 8 solutions:",
				"  1  5  6 | 1  5  6 | 2  5  7 | 1  6  7",
				"  2  8 10 | 3  7 10 | 3  6  9 | 4  5  9",
				"  4  7 11 | 2  9 11 | 1 10 11 | 3  8 11",
				"  3  9 12 | 4  8 12 | 4  8 12 | 2 10 12",
				"  --------+---------+---------+--------",
				"  2  4  6 | 2  6  8 | 3  4  7 | 3  5  8",
				"  1  9 10 | 4  5  9 | 1  8  9 | 2  7  9",
				"  3  8 11 | 3  7 10 | 5  6 11 | 4  6 10",
				"  5  7 12 | 1 11 12 | 2 10 12 | 1 11 12",
				".",
				"The 8 solutions for m = 4, one per line:",
				"  (1,  5,  6), (2,  8, 10), (3,  9, 12), (4,  7, 11);",
				"  (1,  5,  6), (2,  9, 11), (3,  7, 10), (4,  8, 12);",
				"  (1, 10, 11), (2,  5,  7), (3,  6,  9), (4,  8, 12);",
				"  (1,  6,  7), (2, 10, 12), (3,  8, 11), (4,  5,  9);",
				"  (1,  9, 10), (2,  4,  6), (3,  8, 11), (5,  7, 12);",
				"  (1, 11, 12), (2,  6,  8), (3,  7, 10), (4,  5,  9);",
				"  (1,  8,  9), (2, 10, 12), (3,  4,  7), (5,  6, 11);",
				"  (1, 11, 12), (2,  7,  9), (3,  5,  8), (4,  6, 10)."
			],
			"mathematica": [
				"Table[Length[Select[Subsets[Select[Subsets[Range[3 n], {3}], #[[1]] + #[[2]] == #[[3]] \u0026], {n}], Range[3 n] == Sort[Flatten[#]] \u0026]], {n, 0,",
				"5}]  (* Suitable only for n\u003c6. See Knuth's Dancing Links algorithm for n\u003e5. *) (* _Robert Price_, Apr 03 2019 *)"
			],
			"program": [
				"(Sage) A = lambda n:sum(1 for t in DLXCPP([(a-1,b-1,a+b-1) for a in (1..3*n) for b in (1..min(3*n-a,a-1))])) # _Tomas Boothby_, Oct 11 2013"
			],
			"xref": [
				"Cf. A002848, A002849, A161826, A202951, A202952."
			],
			"keyword": "nonn,more",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_, Feb 10 2010, based on posting to the Sequence Fans Mailing List by _Franklin T. Adams-Watters_, _R. K. Guy_, _R. H. Hardin_, _Alois P. Heinz_, _Andrew Weimholt_ and others.",
			"ext": [
				"a(12) from _R. H. Hardin_, Feb 11 2010",
				"a(12) confirmed and a(13) computed (using Knuth's dancing links algorithm) by _Alois P. Heinz_, Feb 11 2010",
				"a(13) confirmed by _Tomas Boothby_, Oct 11 2013",
				"a(16) from _Frank Niedermeyer_, Apr 19 2020",
				"a(17)-a(19) from _Frank Niedermeyer_, May 02 2020"
			],
			"references": 10,
			"revision": 66,
			"time": "2020-05-03T22:18:12-04:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}