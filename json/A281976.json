{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A281976",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 281976,
			"data": "1,2,3,2,2,3,3,2,1,3,4,2,1,2,2,2,2,3,5,2,3,3,2,1,1,4,5,4,2,2,4,3,3,3,6,2,6,5,3,3,3,7,6,2,2,5,4,1,2,3,7,6,8,4,5,5,2,4,5,2,3,5,3,4,2,5,9,4,5,4,5,1,3,5,4,5,5,4,2,3,3",
			"name": "Number of ways to write n as x^2 + y^2 + z^2 + w^2 with x,y,z,w nonnegative integers and z \u003c= w such that both x and x + 24*y are squares.",
			"comment": [
				"Conjecture: a(n) \u003e 0 for all n = 0,1,2,..., and a(n) = 1 only for n = 0, 16^k*m (k = 0,1,2,... and m = 8, 12, 23, 24, 47, 71, 168, 344, 632, 1724).",
				"By the linked JNT paper, any nonnegative integer can be written as the sum of a fourth power and three squares.",
				"We have verified a(n) \u003e 0 for all n = 0..10^7.",
				"See also A281977, A282013 and A282014 for similar conjectures.",
				"a(n) \u003c= A273404(n). Starts to differ from A273404 at n=145. - _R. J. Mathar_, Feb 12 2017",
				"Qing-Hu Hou at Tianjin Univ. has verified a(n) \u003e 0 for all n = 0..10^10.",
				"I would like to offer 2400 US dollars for the first proof of my conjecture that a(n) \u003e 0 for any nonnegative integer n. - _Zhi-Wei Sun_, Feb 14 2017"
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A281976/b281976.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2016.11.008\"\u003eRefining Lagrange's four-square theorem\u003c/a\u003e, J. Number Theory 175(2017), 167-190.",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1701.05868\"\u003eRestricted sums of four squares\u003c/a\u003e, arXiv:1701.05868 [math.NT], 2017.",
				"Zhi-Wei Sun, \u003ca href=\"http://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;d465bba.1702\"\u003eThe 24-conjecture with $2400 prize\u003c/a\u003e, a message to Number Theory List, Feb. 14, 2017."
			],
			"example": [
				"a(8) = 1 since 8 = 0^2 + 0^2 + 2^2 + 2^2 with 0 = 0^2 and 0 + 24*0 = 0^2.",
				"a(12) = 1 since 12 = 1^2 + 1^2 + 1^2 + 3^2 with 1 = 1^2 and 1 + 24*1 = 5^2.",
				"a(23) = 1 since 23 = 1^2 + 2^2 + 3^2 + 3^2 with 1 = 1^2 and 1 + 24*2 = 7^2.",
				"a(24) = 1 since 24 = 4^2 + 0^2 + 2^2 + 2^2 with 4 = 2^2 and 4 + 24*0 = 2^2.",
				"a(47) = 1 since 47 = 1^2 + 1^2 + 3^2 + 6^2 with 1 = 1^2 and 1 + 24*1 = 5^2.",
				"a(71) = 1 since 71 = 1^2 + 5^2 + 3^2 + 6^2 with 1 = 1^2 and 1 + 24*5 = 11^2.",
				"a(168) = 1 since 168 = 4^2 + 4^2 + 6^2 + 10^2 with 4 = 2^2 and 4 + 24*4 = 10^2.",
				"a(344) = 1 since 344 = 4^2 + 0^2 + 2^2 + 18^2 with 4 = 2^2 and 4 + 24*0 = 2^2.",
				"a(632) = 1 since 632 = 0^2 + 6^2 + 14^2 + 20^2 with 0 = 0^2 and 0 + 24*6 = 12^2.",
				"a(1724) = 1 since 1724 = 25^2 + 1^2 + 3^2 + 33^2 with 25 = 5^2 and 25 + 24*1 = 7^2."
			],
			"mathematica": [
				"SQ[n_]:=SQ[n]=IntegerQ[Sqrt[n]];",
				"Do[r=0;Do[If[SQ[n-x^4-y^2-z^2]\u0026\u0026SQ[x^2+24y],r=r+1],{x,0,n^(1/4)},{y,0,Sqrt[n-x^4]},{z,0,Sqrt[(n-x^4-y^2)/2]}];Print[n,\" \",r];Continue,{n,0,80}]"
			],
			"xref": [
				"Cf. A000118, A000290, A000583, A270969, A273404, A281939, A281941, A281975, A281977, A281980, A282013, A282014."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Zhi-Wei Sun_, Feb 04 2017",
			"references": 71,
			"revision": 31,
			"time": "2017-02-17T02:35:56-05:00",
			"created": "2017-02-04T11:23:16-05:00"
		}
	]
}