{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A263856",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 263856,
			"data": "1,2,2,4,4,3,2,6,9,5,11,4,3,11,14,6,13,9,11,17,3,20,14,5,2,9,23,20,12,4,31,17,5,23,12,32,17,22,32,15,26,14,42,2,11,37,29,46,27,14,9,48,6,40,2,43,22,51,18,12,43,17,39,56,14,32,45,6,50",
			"name": "Let S_n be the list of the first n primes written in binary, with least significant bits on the left, and sorted into lexicographic order; a(n) = position of n-th prime in S_n.",
			"comment": [
				"a(A264647(n)) = n and a(m) != n for m \u003c A264647(n). - _Reinhard Zumkeller_, Nov 19 2015",
				"A264662(n,a(n)) = A000040(n): a(n) = index of prime(n) in n-th row of triangle A264662. - _Reinhard Zumkeller_, Nov 20 2015"
			],
			"link": [
				"John Bodeen, \u003ca href=\"/A263856/b263856.txt\"\u003eTable of n, a(n) for n = 1..24771\u003c/a\u003e",
				"John Bodeen, \u003ca href=\"https://github.com/jbodeen/ava/blob/master/camlib/primes.ml\"\u003eOCaml program to generate the sequence\u003c/a\u003e"
			],
			"example": [
				"S_1 = [01], a(1) = 1;",
				"S_2 = [01, 11], a(2) = 2;",
				"S_3 = [01, 101, 11], a(3) = 2;",
				"S_4 = [01, 101, 11, 111], a(4) = 4;",
				"S_5 = [01, 101, 11, 1101, 111], a(5) = 4;",
				"S_5 = [01, 101, 1011, 11, 1101, 111], a(6) = 3;",
				"..."
			],
			"maple": [
				"s:= proc(n) s(n):= cat(\"\", convert(ithprime(n), base, 2)[]) end:",
				"a:= n-\u003e ListTools[BinarySearch](sort([seq(s(i), i=1..n)]), s(n)):",
				"seq(a(n), n=1..100);  # _Alois P. Heinz_, Nov 19 2015"
			],
			"mathematica": [
				"S[n_] := S[n] = SortBy[Prime[Range[n]], StringJoin @@ ToString /@ Reverse[IntegerDigits[#, 2]]\u0026];",
				"a[n_] := FirstPosition[S[n], Prime[n]][[1]];",
				"Table[a[n], {n, 1, 100}] (* _Jean-François Alcover_, Sep 22 2021 *)"
			],
			"program": [
				"(Haskell)",
				"import Data.List (insertBy); import Data.Function (on)",
				"import Data.List (elemIndex); import Data.Maybe (fromJust)",
				"a263856 n = a263856_list !! (n-1)",
				"a263856_list = f [] a004676_list where",
				"   f bps (x:xs) = y : f bps' xs where",
				"     y = fromJust (elemIndex x bps') + 1",
				"     bps' = insertBy (compare `on` (reverse . show)) x bps",
				"-- _Reinhard Zumkeller_, Nov 19 2015",
				"(Python)",
				"from sympy import prime",
				"def A263856(n):",
				"    return 1+sorted(format(prime(i),'b')[::-1] for i in range(1,n+1)).index(format(prime(n),'b')[::-1]) # _Chai Wah Wu_, Nov 22 2015"
			],
			"xref": [
				"A004676 is the sequence upon which the lexicographic ordering is based.",
				"Cf. A264596.",
				"Cf. A264647, A264662."
			],
			"keyword": "nonn,base",
			"offset": "1,2",
			"author": "_John Bodeen_, Oct 28 2015",
			"references": 4,
			"revision": 57,
			"time": "2021-09-22T05:57:52-04:00",
			"created": "2015-11-19T03:12:00-05:00"
		}
	]
}