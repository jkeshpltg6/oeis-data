{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A101368",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 101368,
			"data": "1,1,3,13,61,291,1393,6673,31971,153181,733933,3516483,16848481,80725921,386781123,1853179693,8879117341,42542407011,203832917713,976622181553,4679277990051,22419767768701,107419560853453,514678036498563,2465970621639361,11815175071698241,56609904736851843,271234348612560973",
			"name": "The sequence solves the following problem: find all the pairs (i,j) such that i divides 1+j+j^2 and j divides 1+i+i^2. In fact, the pairs (a(n),a(n+1)), n\u003e0, are all the solutions.",
			"comment": [
				"a(n) is prime exactly for n = 3, 4, 5, 8, 16, 20, 22, 23, 58, 302, 386, 449, 479, 880 up to 1000. - _Tomohiro Yamada_, Dec 23 2018"
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A101368/b101368.txt\"\u003eTable of n, a(n) for n = 1..1471\u003c/a\u003e",
				"T. Cai, Z. Shen and L. Jia, \u003ca href=\"http://arxiv.org/abs/1503.02798\"\u003eA congruence involving harmonic sums modulo p^alpha q^beta\u003c/a\u003e, arXiv preprint arXiv:1503.02798 [math.NT], 2015.",
				"W. W. Chao, \u003ca href=\"https://cms.math.ca/crux/v30/n7/page429-434.pdf\"\u003eProblem 2981\u003c/a\u003e, Crux Mathematicorum, 30 (2004), p. 430.",
				"Yasuaki Gyoda, \u003ca href=\"https://arxiv.org/abs/2109.09639\"\u003ePositive integer solutions to (x+y)^2+(y+z)^2+(z+x)^2=12xyz\u003c/a\u003e, arXiv:2109.09639 [math.NT], 2021. See Remark 3.3 p. 6.",
				"W. H. Mills, \u003ca href=\"http://projecteuclid.org/euclid.pjm/1103051516\"\u003eA system of quadratic Diophantine equations\u003c/a\u003e. Pacific J. Math. 3:1 (1953), 209-220.",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (6,-6,1)."
			],
			"formula": [
				"Recurrence: a(1)=a(2)=1 and a(n+1)=(1+a(n)+a(n)^2)/a(n-1) for n\u003e2.",
				"G.f.: x(1 - 5x + 3x^2) / [(1-x)(1 - 5x + x^2)]; a(n) = 2 * A089817(n-3) + 1, n\u003e2. - Conjectured by _Ralf Stephan_, Jan 14 2005, proved by _Max Alekseyev_, Aug 03 2006",
				"a(n) = 6a(n-1)-6a(n-2)+a(n-3), a(n) = 5a(n-1)-a(n-2)-1. - _Floor van Lamoen_, Aug 01 2006",
				"a(n) = (4/3 - (2/7)*sqrt(21))*((5 + sqrt(21))/2)^n + (4/3 + (2/7)*sqrt(21))*((5 - sqrt(21))/2)^n + 1/3. - _Floor van Lamoen_, Aug 04 2006"
			],
			"example": [
				"a(5) = 61 because (1 + a(4) + a(4)^2)/a(3) = (1 + 13 + 169)/3 = 61."
			],
			"maple": [
				"seq(coeff(series(x*(1-5*x+3*x^2)/((1-x)*(1-5*x+x^2)),x,n+1), x, n), n = 1 .. 30); # _Muniru A Asiru_, Dec 28 2018"
			],
			"mathematica": [
				"Rest@ CoefficientList[Series[x (1 - 5 x + 3 x^2)/((1 - x) (1 - 5 x + x^2)), {x, 0, 28}], x] (* or *)",
				"RecurrenceTable[{a[n] == (1 + a[n - 1] + a[n - 1]^2)/a[n - 2], a[1] == a[2] == 1}, a, {n, 1, 28}] (* or *)",
				"RecurrenceTable[{a[n] == 5 a[n - 1] - a[n - 2] - 1, a[1] == a[2] == 1}, a, {n, 1, 28}] (* or *)",
				"LinearRecurrence[{6, -6, 1}, {1, 1, 3}, 28] (* _Michael De Vlieger_, Aug 28 2016 *)"
			],
			"program": [
				"(PARI) Vec(x*(1-5*x+3*x^2)/((1-x)*(1-5*x+x^2)) + O(x^30)) \\\\ _Michel Marcus_, Aug 03 2016",
				"(PARI) a(n)=([0,1,0;0,0,1;1,-6,6]^n*[3;1;1])[1,1] \\\\ _Charles R Greathouse IV_, Aug 28 2016",
				"(MAGMA) [n le 2 select 1 else 5*Self(n-1)-Self(n-2)-1: n in [1..30]]; // _Vincenzo Librandi_, Dec 25 2018",
				"(GAP) a:=[1,1];; for n in [3..30] do a[n]:=5*a[n-1]-a[n-2]-1; od; Print(a); # _Muniru A Asiru_, Dec 28 2018"
			],
			"xref": [
				"Cf. A001519, A276160."
			],
			"keyword": "nonn,easy",
			"offset": "1,3",
			"author": "M. Benito, O. Ciaurri and E. Fernandez (oscar.ciaurri(AT)dmc.unirioja.es), Jan 13 2005",
			"references": 8,
			"revision": 35,
			"time": "2021-09-22T09:17:06-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}