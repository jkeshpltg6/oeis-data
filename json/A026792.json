{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A026792",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 26792,
			"data": "1,2,1,1,3,2,1,1,1,1,4,2,2,3,1,2,1,1,1,1,1,1,5,3,2,4,1,2,2,1,3,1,1,2,1,1,1,1,1,1,1,1,6,3,3,4,2,2,2,2,5,1,3,2,1,4,1,1,2,2,1,1,3,1,1,1,2,1,1,1,1,1,1,1,1,1,1,7,4,3,5,2,3,2,2,6,1,3,3,1,4,2,1,2,2,2,1,5,1,1,3,2,1,1,4,1,1,1,2,2,1",
			"name": "List of juxtaposed reverse-lexicographically ordered partitions of the positive integers.",
			"comment": [
				"The representation of the partitions (for fixed n) is as (weakly) decreasing lists of parts, the order between individual partitions (for the same n) is (list-)reversed lexicographic; see examples. [_Joerg Arndt_, Sep 03 2013]",
				"Written as a triangle; row n has length A006128(n); row sums give A066186. Also written as an irregular tetrahedron in which T(n,j,k) is the k-th largest part of the j-th partition of n; the sum of column k in the slice n is A181187(n,k); right border of the slices gives A182715. - _Omar E. Pol_, Mar 25 2012",
				"The equivalent sequence for compositions (ordered partitions) is A228351). - _Omar E. Pol_, Sep 03 2013",
				"This is the reverse-colexicographic order of integer partitions, or the reflected reverse-lexicographic order of reversed integer partitions. It is not reverse-lexicographic order (A080577), wherein we would have (3,1) before (2,2). - _Gus Wiseman_, May 12 2020"
			],
			"link": [
				"Robert Price, \u003ca href=\"/A026792/b026792.txt\"\u003eTable of n, a(n) for n = 1..3615, 15 rows.\u003c/a\u003e",
				"OEIS Wiki, \u003ca href=\"http://oeis.org/wiki/Orderings of partitions\"\u003eOrderings of partitions\u003c/a\u003e",
				"Wikiversity, \u003ca href=\"https://en.wikiversity.org/wiki/Lexicographic_and_colexicographic_order\"\u003eLexicographic and colexicographic order\u003c/a\u003e"
			],
			"example": [
				"E.g. the partitions of 3 (3,2+1,1+1+1) appear as the string 3,2,1,1,1,1.",
				"So the list begins:",
				"1",
				"2, 1, 1,",
				"3, 2, 1, 1, 1, 1,",
				"4, 2, 2, 3, 1, 2, 1, 1, 1, 1, 1, 1,",
				"5, 3, 2, 4, 1, 2, 2, 1, 3, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1,",
				"...",
				"From _Omar E. Pol_, Sep 03 2013: (Start)",
				"Illustration of initial terms:",
				"---------------------------------",
				"n  j     Diagram     Partition",
				"---------------------------------",
				".         _",
				"1  1     |_|         1;",
				".         _ _",
				"2  1     |_  |       2,",
				"2  2     |_|_|       1, 1;",
				".         _ _ _",
				"3  1     |_ _  |     3,",
				"3  2     |_  | |     2, 1,",
				"3  3     |_|_|_|     1, 1, 1;",
				".         _ _ _ _",
				"4  1     |_ _    |   4,",
				"4  2     |_ _|_  |   2, 2,",
				"4  3     |_ _  | |   3, 1,",
				"4  4     |_  | | |   2, 1, 1,",
				"4  5     |_|_|_|_|   1, 1, 1, 1;",
				"...",
				"(End)",
				"From _Gus Wiseman_, May 12 2020: (Start)",
				"This sequence can also be interpreted as the following triangle, whose n-th row is itself a finite triangle with A000041(n) rows. Showing these partitions as their Heinz numbers gives A334436.",
				"                             0",
				"                            (1)",
				"                          (2)(11)",
				"                        (3)(21)(111)",
				"                   (4)(22)(31)(211)(1111)",
				"             (5)(32)(41)(221)(311)(2111)(11111)",
				"  (6)(33)(42)(222)(51)(321)(411)(2211)(3111)(21111)(111111)",
				"(End)"
			],
			"mathematica": [
				"revcolex[f_,c_]:=OrderedQ[PadRight[{Reverse[c],Reverse[f]}]];",
				"Join@@Table[Sort[IntegerPartitions[n],revcolex],{n,0,8}] (* reverse-colexicographic order, _Gus Wiseman_, May 10 2020 *)",
				"- or -",
				"revlex[f_,c_]:=OrderedQ[PadRight[{c,f}]];",
				"Reverse/@Join@@Table[Sort[Reverse/@IntegerPartitions[n],revlex],{n,0,8}] (* reflected reverse-lexicographic order, _Gus Wiseman_, May 12 2020 *)"
			],
			"xref": [
				"Cf. A026791, A228531.",
				"The reflected version for reversed partitions is A080577.",
				"The partition minima appear to be A182715.",
				"The graded reversed version is A211992.",
				"The version for compositions is A228351.",
				"The Heinz numbers of these partitions are A334436.",
				"Cf. A000041, A036036, A036037, A193073, A296150, A331581, A334301, A334435, A334437, A334439."
			],
			"keyword": "nonn,tabf",
			"offset": "1,2",
			"author": "_Clark Kimberling_",
			"ext": [
				"Terms 81st, 83rd and 84th corrected by _Omar E. Pol_, Aug 16 2009"
			],
			"references": 45,
			"revision": 54,
			"time": "2020-06-06T21:31:23-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}