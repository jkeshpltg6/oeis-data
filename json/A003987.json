{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003987",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3987,
			"data": "0,1,1,2,0,2,3,3,3,3,4,2,0,2,4,5,5,1,1,5,5,6,4,6,0,6,4,6,7,7,7,7,7,7,7,7,8,6,4,6,0,6,4,6,8,9,9,5,5,1,1,5,5,9,9,10,8,10,4,2,0,2,4,10,8,10,11,11,11,11,3,3,3,3,11,11,11,11,12,10,8,10,12,2,0,2,12,10,8,10,12,13,13,9,9",
			"name": "Table of n XOR m (or Nim-sum of n and m) read by antidiagonals, i.e., with entries in the order (n,m) = (0,0), (0,1), (1,0), (0,2), (1,1), (2,0), ...",
			"comment": [
				"Another way to construct the array: construct an infinite square matrix starting in the top left corner using the rule that each entry is the smallest nonnegative number that is not in the row to your left or in the column above you.",
				"After a few moves the [symmetric] matrix looks like this:",
				"  0 1 2 3 4 5 ...",
				"  1 0 3 2 5 ...",
				"  2 3 0 1 ?",
				"  3 2 1",
				"  4 5 ?",
				"  5",
				"The ? is then replaced with a 6."
			],
			"reference": [
				"E. R. Berlekamp, J. H. Conway and R. K. Guy, Winning Ways, Academic Press, NY, 2 vols., 1982, see p. 60.",
				"J. H. Conway, On Numbers and Games. Academic Press, NY, 1976, pp. 51-53.",
				"Eric Friedman, Scott M. Garrabrant, Ilona K. Phipps-Morgan, A. S. Landsberg and Urban Larsson, Geometric analysis of a generalized Wythoff game, in Games of no Chance 5, MSRI publ. Cambridge University Press, date?",
				"D. Gale, Tracking the Automatic Ant and Other Mathematical Explorations, A Collection of Mathematical Entertainments Columns from The Mathematical Intelligencer, Springer, 1998; see p. 190. [From _N. J. A. Sloane_, Jul 14 2009]",
				"R. K. Guy, Impartial games, pp. 35-55 of Combinatorial Games, ed. R. K. Guy, Proc. Sympos. Appl. Math., 43, Amer. Math. Soc., 1991."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A003987/b003987.txt\"\u003eRows n = 0..100 of triangle, flattened\u003c/a\u003e",
				"J.-P. Allouche and J. Shallit, \u003ca href=\"https://doi.org/10.1016/S0304-3975(03)00090-2\"\u003eThe Ring of k-regular Sequences, II\u003c/a\u003e, Theoret. Computer Sci., 307 (2003), 3-29.",
				"Rémy Sigrist, \u003ca href=\"/A003987/a003987.png\"\u003eColored representation of T(x,y) for x = 0..1023 and y = 0..1023\u003c/a\u003e (where the hue is function of T(x,y) and black pixels correspond to zeros)",
				"N. J. A. Sloane, \u003ca href=\"http://neilsloane.com/doc/sg.txt\"\u003eMy favorite integer sequences\u003c/a\u003e, in Sequences and their Applications (Proceedings of SETA '98).",
				"N. J. A. Sloane, \u003ca href=\"https://arxiv.org/abs/2105.05111\"\u003eThe OEIS: A Fingerprint File for Mathematics\u003c/a\u003e, arXiv:2105.05111 [math.HO], 2021. Mentions this sequence.",
				"\u003ca href=\"/index/Ni#Nimsums\"\u003eIndex entries for sequences related to Nim-sums\u003c/a\u003e"
			],
			"formula": [
				"T(2i,2j) = 2T(i,j), T(2i+1,2j) = 2T(i,j) + 1."
			],
			"example": [
				"Table begins",
				"   0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, ...",
				"   1,  0,  3,  2,  5,  4,  7,  6,  9,  8, 11, 10, ...",
				"   2,  3,  0,  1,  6,  7,  4,  5, 10, 11,  8, ...",
				"   3,  2,  1,  0,  7,  6,  5,  4, 11, 10, ...",
				"   4,  5,  6,  7,  0,  1,  2,  3, 12, ...",
				"   5,  4,  7,  6,  1,  0,  3,  2, ...",
				"   6,  7,  4,  5,  2,  3,  0, ...",
				"   7,  6,  5,  4,  3,  2, ...",
				"   8,  9, 10, 11, 12, ...",
				"   9,  8, 11, 10, ...",
				"  10, 11,  8, ...",
				"  11, 10, ...",
				"  12, ...",
				"  ...",
				"The first few antidiagonals are",
				"   0;",
				"   1,  1;",
				"   2,  0,  2;",
				"   3,  3,  3,  3;",
				"   4,  2,  0,  2,  4;",
				"   5,  5,  1,  1,  5,  5;",
				"   6,  4,  6,  0,  6,  4,  6;",
				"   7,  7,  7,  7,  7,  7,  7,  7;",
				"   8,  6,  4,  6,  0,  6,  4,  6,  8;",
				"   9,  9,  5,  5,  1,  1,  5,  5,  9,  9;",
				"  10,  8, 10,  4,  2,  0,  2,  4, 10,  8, 10;",
				"  11, 11, 11, 11,  3,  3,  3,  3, 11, 11, 11, 11;",
				"  12, 10,  8, 10, 12,  2,  0,  2, 12, 10,  8, 10, 12;",
				"  ...",
				"[Symmetric] matrix in base 2:",
				"     0    1   10   11  100  101,  110  111 1000 1001 1010 1011 ...",
				"     1    0   11   10  101  100,  111  110 1001 1000 1011  ...",
				"    10   11    0    1  110  111,  100  101 1010 1011  ...",
				"    11   10    1    0  111  110,  101  100 1011  ...",
				"   100  101  110  111    0    1    10   11  ...",
				"   101  100  111  110    1    0    11  ...",
				"   110  111  100  101   10   11   ...",
				"   111  110  101  100   11  ...",
				"  1000 1001 1010 1011  ...",
				"  1001 1000 1011  ...",
				"  1010 1011  ...",
				"  1011  ...",
				"   ..."
			],
			"maple": [
				"nimsum := proc(a,b) local t1,t2,t3,t4,l; t1 := convert(a+2^20,base,2); t2 := convert(b+2^20,base,2); t3 := evalm(t1+t2); map(x-\u003ex mod 2, t3); t4 := convert(evalm(%),list); l := convert(t4,base,2,10); sum(l[k]*10^(k-1), k=1..nops(l)); end; # memo: adjust 2^20 to be much bigger than a and b",
				"AT := array(0..N,0..N); for a from 0 to N do for b from a to N do AT[a,b] := nimsum(a,b); AT[b,a] := AT[a,b]; od: od:",
				"# alternative:",
				"read(\"transforms\") :",
				"A003987 := proc(n,m)",
				"    XORnos(n,m) ;",
				"end proc: # _R. J. Mathar_, Apr 17 2013",
				"seq(seq(Bits:-Xor(k,m-k),k=0..m),m=0..20); # _Robert Israel_, Dec 31 2015"
			],
			"mathematica": [
				"Flatten[Table[BitXor[b, a - b], {a, 0, 10}, {b, 0, a}]] (* BitXor and Nim Sum are equivalent *)"
			],
			"program": [
				"(PARI) tabl(nn) = {for(n=0, nn, for(k=0, n, print1(bitxor(k, n - k),\", \");); print(););};",
				"tabl(13) \\\\ _Indranil Ghosh_, Mar 31 2017",
				"(Python)",
				"for n in range(14):",
				"    print([k^(n - k) for k in range(n + 1)]) # _Indranil Ghosh_, Mar 31 2017"
			],
			"xref": [
				"Initial rows are A001477, A004442, A004443, A004444, etc. Cf. A051775, A051776.",
				"Cf. A003986 (OR), A004198 (AND), A221146 (carries).",
				"Antidiagonal sums are in A006582."
			],
			"keyword": "tabl,nonn,nice,look",
			"offset": "0,4",
			"author": "_Marc LeBrun_",
			"references": 186,
			"revision": 76,
			"time": "2021-08-25T17:53:53-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}