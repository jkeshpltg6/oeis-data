{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A318253",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 318253,
			"data": "0,0,1,0,1,0,0,1,-2,0,0,1,-9,16,0,0,1,-34,477,-272,0,0,1,-125,11056,-74601,7936,0,0,1,-461,249250,-14873104,25740261,-353792,0,0,1,-1715,5699149,-2886735625,56814228736,-16591655817,22368256,0,0,1,-6434,132908041,-574688719793,122209131374375,-495812444583424,17929265150637,-1903757312,0",
			"name": "Coefficient of x of the OmegaPolynomials (A318146), T(n, k) = [x] P(n, k) with n\u003e=1 and k\u003e=0, square array read by ascending antidiagonals.",
			"comment": [
				"Because in the case n=2 these numbers are the classical signed tangent numbers (A000182) we call T(n, k) also 'generalized tangent numbers' when studied in connection with generalized Bernoulli numbers."
			],
			"formula": [
				"T(n, k) is the derivative of OmegaPolynomial(n, k) evaluated at x = 0.",
				"Apart from the border cases n=1 and k=0 the generalized tangent numbers are a subset of the André numbers A181937; more precisely: T(n, k) is 1 if k = 1 else if k = 0 or n = 1 then T(n, k) = 0 else T(n,k) = (-1)^(n+1)*A181937(n, n*k-1)."
			],
			"example": [
				"[n\\k][0  1     2        3              4                   5  ...]",
				"------------------------------------------------------------------",
				"[1]   0, 1,    0,       0,             0,                  0, ...  [A063524]",
				"[2]   0, 1,   -2,      16,          -272,               7936, ...  [A000182]",
				"[3]   0, 1,   -9,     477,        -74601,           25740261, ...  [A293951]",
				"[4]   0, 1,  -34,   11056,     -14873104,        56814228736, ...  [A273352]",
				"[5]   0, 1, -125,  249250,   -2886735625,    122209131374375, ...  [A318258]",
				"[6]   0, 1, -461, 5699149, -574688719793, 272692888959243481, ...",
				"        [A010763]"
			],
			"maple": [
				"# Prints square array row-wise. The function OmegaPolynomial is defined in A318146.",
				"for n from 1 to 6 do seq(coeff(OmegaPolynomial(n, k), x, 1), k=0..6) od;",
				"# In the sequence format:",
				"0, seq(seq(coeff(OmegaPolynomial(n-k+1, k), x, 1), k=0..n), n=1..9);",
				"# Alternatively, based on the recurrence of the André numbers:",
				"ANum := proc(m, n) option remember; if n = 0 then return 1 fi;",
				"`if`(modp(n, m) = 0, -1, 1);  [seq(m*k, k=0..(n-1)/m)];",
				"%%*add(binomial(n, k)*ANum(m, k), k in %) end:",
				"TNum := proc(n,k) if k=1 then 1 elif k=0 or n=1 then 0 else ANum(n, n*k-1) fi end:",
				"for n from 1 to 6 do seq(TNum(n, k), k = 0..6) od;"
			],
			"program": [
				"(Sage)",
				"# Prints the array row-wise. The function OmegaPolynomial is in A318146.",
				"for m in (1..6):",
				"    print([0] + [list(OmegaPolynomial(m, n))[1] for n in (1..6)])",
				"# Alternatively, based on the recurrence of the André numbers:",
				"@cached_function",
				"def ANum(m, n):",
				"    if n == 0: return 1",
				"    t = [m*k for k in (0..(n-1)//m)]",
				"    s = sum(binomial(n, k)*ANum(m, k) for k in t)",
				"    return -s if m.divides(n) else s",
				"def TNum(m, n):",
				"    if n == 1: return 1",
				"    if n == 0 or m == 1: return 0",
				"    return ANum(m, m*n-1)",
				"for m in (1..6): print([TNum(m, n) for n in (0..6)])"
			],
			"xref": [
				"Cf. A318146, A181937, A063524, A000182, A293951, A273352, A318258."
			],
			"keyword": "sign,tabl",
			"offset": "1,9",
			"author": "_Peter Luschny_, Aug 22 2018",
			"references": 4,
			"revision": 13,
			"time": "2018-08-26T05:31:14-04:00",
			"created": "2018-08-26T05:31:14-04:00"
		}
	]
}