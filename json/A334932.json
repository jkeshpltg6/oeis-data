{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A334932",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 334932,
			"data": "2535,3705,162279,237177,10385895,15179385,664697319,971480697,42540628455,62174764665,2722600221159,3979184938617,174246414154215,254667836071545,11151770505869799,16298741508578937,713713312375667175,1043119456549052025,45677651992042699239",
			"name": "Numbers that generate rotationally symmetrical XOR-triangles with a pattern of zero-triangles of edge length 3, some of which are clipped to result in some zero-triangles of edge length 2 at the edges.",
			"comment": [
				"Subset of A334769 which is a subset of A334556.",
				"Numbers m in this sequence A070939(m) (mod 3) = 0. All m have first and last bits = 1.",
				"The numbers in this sequence can be constructed using run lengths of bits thus: 12..(42)..3 or the reverse 3..(24)..21, with at least one copy of the pair of parenthetic numbers.",
				"Thus, the smallest number m has run lengths {1, 2, 4, 2, 3}, which is the binary 100111100111 = decimal 2535.",
				"2n has the reverse run length pattern as 2n - 1. a(3) has the run lengths {1, 2, 4, 2, 4, 2, 3}, while a(4) has {3, 2, 4, 2, 4, 2, 1}, etc."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A334932/b334932.txt\"\u003eTable of n, a(n) for n = 1..1104\u003c/a\u003e",
				"Michael De Vlieger, \u003ca href=\"/A334932/a334932.png\"\u003eDiagram montage\u003c/a\u003e of XOR-triangles resulting from a(n) with 1 \u003c= n \u003c= 32.",
				"Michael De Vlieger, \u003ca href=\"http://vincico.com/seq/a334769.html\"\u003eCentral zero-triangles in rotationally symmetrical XOR-Triangles\u003c/a\u003e, 2020.",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,65,0,-64).",
				"\u003ca href=\"/index/X#XOR-triangles\"\u003eIndex entries for sequences related to XOR-triangles\u003c/a\u003e"
			],
			"formula": [
				"From _Colin Barker_, Jun 09 2020: (Start)",
				"G.f.: 3*x*(13 + 19*x)*(65 - 64*x^2) / ((1 - x)*(1 + x)*(1 - 8*x)*(1 + 8*x)).",
				"a(n) = 65*a(n-2) - 64*a(n-4) for n\u003e4.",
				"a(n) = (1/21)*(-16 - 3*(-1)^n + 123*2^(5+3*n) - 85*(-1)^n*2^(5 + 3*n)) for n\u003e0.",
				"(End)"
			],
			"example": [
				"Diagrams of a(1)-a(4), replacing “0” with “.” and “1” with “@” for clarity:",
				"a(1) = 2535 (a(2) = 3705 appears as a mirror image):",
				"  @ . . @ @ @ @ . . @ @ @",
				"   @ . @ . . . @ . @ . .",
				"    @ @ @ . . @ @ @ @ .",
				"     . . @ . @ . . . @",
				"      . @ @ @ @ . . @",
				"       @ . . . @ . @",
				"        @ . . @ @ @",
				"         @ . @ . .",
				"          @ @ @ .",
				"           . . @",
				"            . @",
				"             @",
				".",
				"a(3) = 162279 (a(4) = 237177 appears as a mirror image):",
				"  @ . . @ @ @ @ . . @ @ @ @ . . @ @ @",
				"   @ . @ . . . @ . @ . . . @ . @ . .",
				"    @ @ @ . . @ @ @ @ . . @ @ @ @ .",
				"     . . @ . @ . . . @ . @ . . . @",
				"      . @ @ @ @ . . @ @ @ @ . . @",
				"       @ . . . @ . @ . . . @ . @",
				"        @ . . @ @ @ @ . . @ @ @",
				"         @ . @ . . . @ . @ . .",
				"          @ @ @ . . @ @ @ @ .",
				"           . . @ . @ . . . @",
				"            . @ @ @ @ . . @",
				"             @ . . . @ . @",
				"              @ . . @ @ @",
				"               @ . @ . .",
				"                @ @ @ .",
				"                 . . @",
				"                  . @",
				"                   @"
			],
			"mathematica": [
				"Array[FromDigits[Flatten@ MapIndexed[ConstantArray[#2, #1] \u0026 @@ {#1, Mod[First[#2], 2]} \u0026, If[EvenQ@ #1, Reverse@ #2, #2]], 2] \u0026 @@ {#, Join[{1, 2}, PadRight[{}, Ceiling[#, 2], {4, 2}], {3}]} \u0026, 19]",
				"(* Generate a textual plot of XOR-triangle T(n) *)",
				"xortri[n_Integer] := TableForm@ MapIndexed[StringJoin[ConstantArray[\" \", First@ #2 - 1], StringJoin @@ Riffle[Map[If[# == 0, \".\" (* 0 *), \"@\" (* 1 *)] \u0026, #1], \" \"]] \u0026, NestWhileList[Map[BitXor @@ # \u0026, Partition[#, 2, 1]] \u0026, IntegerDigits[n, 2], Length@ # \u003e 1 \u0026]]"
			],
			"xref": [
				"Cf. A334556, A334769, A334930, A334931."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_Michael De Vlieger_, May 16 2020",
			"references": 3,
			"revision": 17,
			"time": "2020-06-21T06:03:42-04:00",
			"created": "2020-05-23T23:57:52-04:00"
		}
	]
}