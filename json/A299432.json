{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A299432",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 299432,
			"data": "0,0,1,2,1,1,-1,1,1,-139,1,-571,-281,163879,-5221,5246819,5459,-534703531,91207079,-4483131259,-2650986803,432261921612371,-6171801683,6232523202521089,4283933145517,-25834629665134204969,11963983648109,-1579029138854919086429,-208697624924077,746590869962651602203151,-29320119130515566117",
			"name": "Numerators of coefficients in S(x) where: C(x)^(1/2) - S(x)^(1/2) = 1 such that C'(x)*S(x)^(1/2) = S'(x)*C(x)^(1/2) = 2*x*C(x).",
			"formula": [
				"The functions C = C(x) and S = S(x) satisfy:",
				"(1) sqrt(C) - sqrt(S) = 1.",
				"(2a) C'*sqrt(S) = S'*sqrt(C) = 2*x*C.",
				"(2b) C' = 2*x*C/sqrt(S).",
				"(2c) S' = 2*x*sqrt(C).",
				"(3a) C = 1 + Integral 2*x*C/sqrt(S) dx.",
				"(3b) S = Integral 2*x*sqrt(C) dx.",
				"(4a) sqrt(C) = exp( Integral x/(sqrt(C) - 1) dx ).",
				"(4b) sqrt(S) = exp( Integral x/sqrt(S) dx ) - 1.",
				"(5a) C - S = exp( Integral 2*x*C/(C*sqrt(S) + S*sqrt(C)) dx ).",
				"(5b) C - S = exp( Integral C'*S'/(C*S' + S*C') dx).",
				"(6a) sqrt(C) = exp( sqrt(C) - 1 - x^2/2 ).",
				"(6b) sqrt(C) = 1 + x^2/2 + Integral x/(sqrt(C) - 1) dx."
			],
			"example": [
				"G.f.: S(x) = x^2 + 2/3*x^3 + 1/6*x^4 + 1/90*x^5 - 1/810*x^6 + 1/15120*x^7 + 1/68040*x^8 - 139/24494400*x^9 + 1/1020600*x^10 - 571/12933043200*x^11 + ...",
				"Related power series begin:",
				"C(x) = 1 + 2*x + 5/3*x^2 + 13/18*x^3 + 43/270*x^4 + 5/432*x^5 - 19/17010*x^6 + 41/2721600*x^7 + 1/40824*x^8 - 7243/1175731200*x^9 + 923/1515591000*x^10 + ...",
				"sqrt(C) = 1 + x + 1/3*x^2 + 1/36*x^3 - 1/270*x^4 + 1/4320*x^5 + 1/17010*x^6 - 139/5443200*x^7 + 1/204120*x^8 - 571/2351462400*x^9 - 281/1515591000*x^10 + ... + A005447(n)/A005446(n)*x^n + ..."
			],
			"mathematica": [
				"terms = 30; c[x_] = Assuming[x \u003e 0, ProductLog[-1, -Exp[-1 - x^2/2]]^2 + O[x]^terms]; Integrate[2*x*Sqrt[c[x]] + O[x]^terms, x] // CoefficientList[#, x] \u0026 // Numerator (* _Jean-François Alcover_, Feb 22 2018 *)"
			],
			"program": [
				"(PARI) {a(n) = my(C=1, S=x^2); for(i=0,n, C = (1 + sqrt(S +O(x^(n+2))))^2; S = intformal( 2*x*sqrt(C) ) ); numerator(polcoeff(S,n))}",
				"for(n=0,30,print1(a(n),\", \"))"
			],
			"xref": [
				"Cf. A299433 (denominators in S), A299430/A299431 (C), A005447/A005446 (sqrt(C)."
			],
			"keyword": "sign,frac",
			"offset": "0,4",
			"author": "_Paul D. Hanna_, Feb 09 2018",
			"references": 6,
			"revision": 14,
			"time": "2018-02-23T03:41:23-05:00",
			"created": "2018-02-09T17:56:37-05:00"
		}
	]
}