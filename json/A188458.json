{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A188458",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 188458,
			"data": "1,1,-3,-11,57,361,-2763,-24611,250737,2873041,-36581523,-512343611,7828053417,129570724921,-2309644635483,-44110959165011,898621108880097,19450718635716001,-445777636063460643,-10784052561125704811,274613643571568682777",
			"name": "Expansion of e.g.f. exp(x)/cosh(2*x).",
			"comment": [
				"A signed version of A001586 (Springer numbers).",
				"Equals the logarithmic derivative of A188514 (ignoring the initial term of this sequence); note that the unsigned version (A001586) does not form a logarithmic derivative of an integer sequence."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A188458/b188458.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Sum_{k=1..n} -(-1)^(n*k)*C(n, k)*a(n-k) for n\u003e0 with a(0)=1.",
				"L.g.f.: log(1+x) = Sum_{n\u003e=1} a(n)*(x^n/n)/(1 + (-1)^n*x)^n.",
				"E.g.f.: 1 = Sum_{n\u003e=0} a(n)*exp(-(-1)^n*x)*x^n/n!.",
				"G.f.: 1 = Sum_{n\u003e=0} a(n)*x^n/(1 + (-1)^n*x)^(n+1).",
				"G.f.: 1 = Sum_{n\u003e=0} a(n)*C(n+m-1,n)*x^n/(1 + (-1)^n*x)^(n+m) for m\u003e=1.",
				"a(n) = Sum_{k=0..n} 2^k C(n,k) Euler(k). - _Peter Luschny_",
				"a(n) = (-1)^[n/2]*((1+I)/2)^n * Sum_{k=0..n} ((1-I)/(1+I))^k * Sum_{j=0..k} (-1)^(k-j)*C(n+1, k-j)*(2*j+1)^n. - _Peter Bala_",
				"O.g.f.: 1/(1-x/(1+4*x/(1-x- 4*x/(1+4*x/(1+x- 6*x/(1+6*x/(1+x- 8*x/(1+8*x/(1+x- 10*x/(1+10*x/(1+x- 12*x/(1+12*x/(1+x- ...))))))))))))) (continued fraction).",
				"E.g.f.: E(x) = exp(x)/cosh(2*x) = 2/G(0) where G(k)= 1 -((-1)^k)*3^k/(1 - x/(x + (k+1)*((-1)^k)*3^k/G(k+1))); (continued fraction, 3rd kind, 3-step). - _Sergei N. Gladkovskii_, Jun 07 2012",
				"a(n) ~ n! * (cos(n*Pi/2) + sin(n*Pi/2)) * 2^(2*n+3/2) / Pi^(n+1). - _Vaclav Kotesovec_, Oct 07 2013",
				"G.f.: conjecture T(0)/(1-x), where T(k) = 1 - 4*x^2*(k+1)^2/(4*x^2*(k+1)^2 + (1-x)^2/T(k+1) ); (continued fraction). - _Sergei N. Gladkovskii_, Nov 12 2013",
				"From _Peter Luschny_, Apr 19 2014: (Start)",
				"a(n) = 2^n*skp(n, 1/2), where skp(n,x) are the Swiss-Knife polynomials A153641.",
				"a(n) = 4^n*E(n, 3/4), where E(n,x) are Euler polynomials.",
				"a(n) = (8^n/((n+1)/2))*(B(n+1, 7/8) - B(n+1, 3/8)), where B(n,x) are the Bernoulli polynomials. (End)",
				"a(n) = 2^(3*n+1)*(Zeta(-n,3/8)-Zeta(-n,7/8)). - _Peter Luschny_, Oct 15 2015"
			],
			"example": [
				"E.g.f.: exp(x)/cosh(2*x) = 1 + x - 3*x^2/2! - 11*x^3/3! + 57*x^4/4! + 361*x^5/5! +...",
				"Illustration of other generating functions.",
				"E.g.f.: 1 = exp(-x) + exp(x)*x - 3*exp(-x)*x^2/2! - 11*exp(x)*x^3/3! +...",
				"L.g.f.: log(1+x) = x/(1-x) - 3*(x^2/2)/(1+x)^2 - 11*(x^3/3)/(1-x)^3 +...",
				"G.f.: 1 = 1/(1+x) + 1*x/(1-x)^2 - 3*x^2/(1+x)^3 - 11*x^3/(1-x)^4 +...",
				"G.f.: 1 = 1/(1+x)^2 + 1*2*x/(1-x)^3 - 3*3*x^2/(1+x)^4 - 11*4*x^3/(1-x)^5 +...",
				"G.f.: 1 = 1/(1+x)^3 + 1*3*x/(1-x)^4 - 3*6*x^2/(1+x)^5 - 11*10*x^3/(1-x)^6 +..."
			],
			"maple": [
				"seq(4^n*euler(n,3/4), n=0..20); # _Peter Luschny_, Apr 19 2014"
			],
			"mathematica": [
				"CoefficientList[Series[E^x/Cosh[2*x], {x, 0, 20}], x]* Range[0, 20]! (* _Vaclav Kotesovec_, Oct 07 2013 *)"
			],
			"program": [
				"(PARI) {a(n)=local(X=x+x*O(x^n));n!*polcoeff(exp(X)/cosh(2*X),n)}",
				"(PARI) {a(n)=n!*polcoeff(1-sum(k=0, n-1, a(k)*exp(-(-1)^k*x+x*O(x^n))*x^k/k!), n)}",
				"(PARI) {a(n)=polcoeff(1-sum(k=0, n-1, a(k)*x^k/(1+(-1)^k*x+x*O(x^n))^(k+1)), n)}",
				"(PARI) /* Holds for m\u003e=1: */",
				"{a(n)=local(m=1); polcoeff(1-sum(k=0, n-1, a(k)*binomial(m+k-1, k)*x^k/(1+(-1)^k*x+x*O(x^n))^(k+m)), n)/binomial(m+n-1, n)}",
				"(PARI) /* Recurrence: */",
				"{a(n)=if(n\u003c0,0,if(n==0,1, sum(k=1, n, -(-1)^(n*k)*binomial(n, k)*a(n-k))))}",
				"(PARI) {EULER(n)=n!*polcoeff(1/cosh(x+x*O(x^n)),n)}",
				"{a(n)=sum(k=0,n,2^k*binomial(n,k)*EULER(k))}",
				"(PARI) {a(n)=(-1)^(n\\2)*((1+I)/2)^n*sum(k=0, n, ((1-I)/(1+I))^k*sum(j=0, k, (-1)^(k-j)*binomial(n+1, k-j)*(2*j+1)^n))}"
			],
			"xref": [
				"Cf. A001586, A188514."
			],
			"keyword": "sign",
			"offset": "0,3",
			"author": "_Paul D. Hanna_, Apr 01 2011",
			"references": 10,
			"revision": 54,
			"time": "2015-10-16T02:19:23-04:00",
			"created": "2011-04-02T22:24:10-04:00"
		}
	]
}