{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A035324",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 35324,
			"data": "1,3,1,10,6,1,35,29,9,1,126,130,57,12,1,462,562,312,94,15,1,1716,2380,1578,608,140,18,1,6435,9949,7599,3525,1045,195,21,1,24310,41226,35401,19044,6835,1650,259,24,1,92378,169766,161052,97954,40963,12021,2450",
			"name": "A convolution triangle of numbers, generalizing Pascal's triangle A007318.",
			"comment": [
				"Replacing each '2' in the recurrence by '1' produces Pascal's triangle A007318(n-1,m-1). The columns appear as A001700, A008549, A045720, A045894, A035330...",
				"Triangle T(n,k), 1\u003c=k\u003c=n, given by (0, 3/1, 1/3, 5/3, 3/5, 7/5, 5/7, 9/7, 7/9, 11/9, 9/11, ...) DELTA (1, 0, 0, 0, 0, 0, 0, 0, 0, ...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, Jan 28 2012",
				"Riordan array (1, c(x)/sqrt(1-4x)) where c(x) = g.f. for Catalan numbers A000108, first column (k = 0) omitted. - _Philippe Deléham_, Jan 28 2012"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A035324/b035324.txt\"\u003eRows n = 1..120 of triangle, flattened\u003c/a\u003e",
				"Milan Janjić, \u003ca href=\"https://www.emis.de/journals/JIS/VOL21/Janjic2/janjic103.html\"\u003ePascal Matrices and Restricted Words\u003c/a\u003e, J. Int. Seq., Vol. 21 (2018), Article 18.5.2.",
				"Wolfdieter Lang, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL3/LANG/lang.html\"\u003eOn generalizations of Stirling number triangles\u003c/a\u003e, J. Integer Seqs., Vol. 3 (2000), #00.2.4.",
				"Wolfdieter Lang, \u003ca href=\"/A035324/a035324.txt\"\u003eFirst 10 rows.\u003c/a\u003e"
			],
			"formula": [
				"a(n+1, m) = 2*(2*n+m)*a(n, m)/(n+1) + m*a(n, m-1)/(n+1), n \u003e= m \u003e= 1; a(n, m) := 0, n\u003cm; a(n, 0) := 0, a(1, 1)=1;",
				"G.f. for column m: ((x*c(x)/sqrt(1-4*x))^m)/x, where c(x) = g.f. for Catalan numbers A000108.",
				"a(n, m) =: s2(3; n, m).",
				"With offset 0 (0\u003c=k\u003c=n), T(n,k) = Sum_{j\u003e=0} A039598(n,j)*binomial(j,k). - _Philippe Deléham_, Mar 30 2007",
				"T(n+1,n) = 3*n = A008585(n).",
				"T(n,k) = T(n-1,k-1) + 3*T(n-1,k) + Sum_{i\u003e=0} T(n-1,k+1+i)*(-1)^i. - _Philippe Deléham_, Feb 23 2012",
				"T(n,m) = Sum_{k=m..n} k*binomial(k-1,k-m)*2^(k-m)*binomial(2*n-k-1,n-k))/n. - _Vladimir Kruchinin_, Aug 07 2013"
			],
			"example": [
				"Triangle begins:",
				"1",
				"3     1",
				"10    6   1",
				"35   29   9   1",
				"126 130  57  12   1",
				"462 562 312  94  15   1",
				"Triangle (0,3,1/3,5/3,3/5,...) DELTA (1,0,0,0,0,0, ...) has an additional first column (1,0,0,...)."
			],
			"mathematica": [
				"a[n_, m_] /; n \u003e= m \u003e= 1 := a[n, m] = 2*(2*(n-1) + m)*(a[n-1, m]/n) + m*(a[n-1, m-1]/n); a[n_, m_] /; n \u003c m = 0; a[n_, 0] = 0; a[1, 1] = 1; Flatten[ Table[ a[n, m], {n, 1, 10}, {m, 1, n}]] (* _Jean-François Alcover_, Feb 21 2012, from first formula *)"
			],
			"program": [
				"(Haskell)",
				"a035324 n k = a035324_tabl !! (n-1) !! (k-1)",
				"a035324_row n = a035324_tabl !! (n-1)",
				"a035324_tabl = map snd $ iterate f (1, [1]) where",
				"   f (i, xs)  = (i + 1, map (`div` (i + 1)) $",
				"      zipWith (+) ((map (* 2) $ zipWith (*) [2 * i + 1 ..] xs) ++ [0])",
				"                  ([0] ++ zipWith (*) [2 ..] xs))",
				"-- _Reinhard Zumkeller_, Jun 30 2013",
				"(Sage)",
				"@cached_function",
				"def T(n, k):",
				"    if n == 0: return n^k",
				"    return sum(binomial(2*i-1, i)*T(n-1, k-i) for i in (1..k-n+1))",
				"A035324 = lambda n,k: T(k, n)",
				"for n in (1..8): print([A035324(n, k) for k in (1..n)]) # _Peter Luschny_, Aug 16 2016"
			],
			"xref": [
				"Cf. A000108, A007318, A039598.",
				"Row sums: A049027(n), n \u003e= 1.",
				"Alternating row sums give A000108 (Catalan numbers).",
				"If offset 0 (n \u003e= m \u003e= 0): convolution triangle based on A001700 (central binomial coeffs. of odd order)."
			],
			"keyword": "easy,nice,nonn,tabl",
			"offset": "1,2",
			"author": "_Wolfdieter Lang_",
			"references": 21,
			"revision": 58,
			"time": "2020-02-26T13:43:13-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}