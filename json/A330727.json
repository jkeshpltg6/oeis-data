{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A330727",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 330727,
			"data": "1,1,1,1,2,1,3,2,1,3,1,7,7,1,5,5,1,5,9,5,1,9,11,1,9,28,36,16,1,10,24,16,1,14,38,27,1,13,18,1,13,69,160,164,61,1,24,79,62,1,20,160,580,1022,855,272,1,19,59,45,1,27,138,232,123,1,17,77,121,61",
			"name": "Irregular triangle read by rows where T(n,k) is the number of balanced reduced multisystems of depth k whose degrees (atom multiplicities) are the prime indices of n.",
			"comment": [
				"A balanced reduced multisystem is either a finite multiset, or a multiset partition with at least two parts, not all of which are singletons, of a balanced reduced multisystem.",
				"A prime index of n is a number m such that prime(m) divides n. The multiset of prime indices of n is row n of A112798. A multiset whose multiplicities are the prime indices of n (such as row n of A305936) is generally not the same as the multiset of prime indices of n. For example, the prime indices of 12 are {1,1,2}, while a multiset whose multiplicities are {1,1,2} is {1,1,2,3}."
			],
			"formula": [
				"T(2^n,k) = A008826(n,k)."
			],
			"example": [
				"Triangle begins:",
				"   {}",
				"   1",
				"   1",
				"   1   1",
				"   1   2",
				"   1   3   2",
				"   1   3",
				"   1   7   7",
				"   1   5   5",
				"   1   5   9   5",
				"   1   9  11",
				"   1   9  28  36  16",
				"   1  10  24  16",
				"   1  14  38  27",
				"   1  13  18",
				"   1  13  69 160 164  61",
				"   1  24  79  62",
				"For example, row n = 12 counts the following multisystems:",
				"  {1,1,2,3}  {{1},{1,2,3}}    {{{1}},{{1},{2,3}}}",
				"             {{1,1},{2,3}}    {{{1,1}},{{2},{3}}}",
				"             {{1,2},{1,3}}    {{{1}},{{2},{1,3}}}",
				"             {{2},{1,1,3}}    {{{1,2}},{{1},{3}}}",
				"             {{3},{1,1,2}}    {{{1}},{{3},{1,2}}}",
				"             {{1},{1},{2,3}}  {{{1,3}},{{1},{2}}}",
				"             {{1},{2},{1,3}}  {{{2}},{{1},{1,3}}}",
				"             {{1},{3},{1,2}}  {{{2}},{{3},{1,1}}}",
				"             {{2},{3},{1,1}}  {{{2,3}},{{1},{1}}}",
				"                              {{{3}},{{1},{1,2}}}",
				"                              {{{3}},{{2},{1,1}}}"
			],
			"mathematica": [
				"nrmptn[n_]:=Join@@MapIndexed[Table[#2[[1]],{#1}]\u0026,If[n==1,{},Flatten[Cases[Reverse[FactorInteger[n]],{p_,k_}:\u003eTable[PrimePi[p],{k}]]]]];",
				"sps[{}]:={{}};sps[set:{i_,___}]:=Join@@Function[s,Prepend[#,s]\u0026/@sps[Complement[set,s]]]/@Cases[Subsets[set],{i,___}];",
				"mps[set_]:=Union[Sort[Sort/@(#/.x_Integer:\u003eset[[x]])]\u0026/@sps[Range[Length[set]]]];",
				"totm[m_]:=Prepend[Join@@Table[totm[p],{p,Select[mps[m],1\u003cLength[#]\u003cLength[m]\u0026]}],m];",
				"Table[Length[Select[totm[nrmptn[n]],Depth[#]==k\u0026]],{n,2,10},{k,2,Length[nrmptn[n]]}]"
			],
			"xref": [
				"Row sums are A318846.",
				"Final terms in each row are A330728.",
				"Row prime(n) is row n of A330784.",
				"Row 2^n is row n of A008826.",
				"Row n is row A181821(n) of A330667.",
				"Column k = 3 is A318284(n) - 2 for n \u003e 2.",
				"Cf. A000111, A002846, A005121, A292504, A318812, A318813, A318847, A318848, A318849, A330475, A330666, A330935."
			],
			"keyword": "nonn,tabf",
			"offset": "2,5",
			"author": "_Gus Wiseman_, Jan 04 2020",
			"references": 5,
			"revision": 5,
			"time": "2020-01-05T08:11:03-05:00",
			"created": "2020-01-05T08:11:03-05:00"
		}
	]
}