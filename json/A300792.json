{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A300792",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 300792,
			"data": "1,2,1,2,4,1,2,2,2,6,2,3,5,1,4,1,5,7,4,5,1,5,2,1,9,6,5,3,4,7,2,2,6,7,3,5,7,4,4,6,6,4,5,3,9,4,2,1,4,11,5,9,5,6,4,1,9,7,3,6,5,4,4,2,14,4,6,5,2,8,2,7,9,5,5,4,3,8,1,4",
			"name": "Number of ways to write n as x^2 + y^2 + z^2 + w^2, where w is a positive integer and x,y,z are nonnegative integers such that x or y or z is a square and 9*x^2 + 16*y^2 + 24*z^2 is also a square.",
			"comment": [
				"Conjecture 1: a(n) \u003e 0 for all n \u003e 0, and a(n) = 1 only for n = 16^k*m with k = 0,1,2,... and m = 1, 3, 6, 14, 21, 24, 56, 79, 119, 143, 248, 301, 383, 591, 728, 959, 1223, 1751, 2311, 6119.",
				"Conjecture 2: Any positive integer n can be written as x^2 + y^2 + z^2 + w^2, where w is a positive integer and x,y,z are nonnegative integers such that x or y or z is a square and 36*x^2 + 40*y^2 + 45*z^2 is also a square.",
				"See also A300791 for a similar conjecture."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A300792/b300792.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2016.11.008\"\u003eRefining Lagrange's four-square theorem\u003c/a\u003e, J. Number Theory 175(2017), 167-190.",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1701.05868\"\u003eRestricted sums of four squares\u003c/a\u003e, arXiv:1701.05868 [math.NT], 2017-2018."
			],
			"example": [
				"a(6) = 1 since 6 = 1^2 + 1^2 + 0^2 + 2^2 with 1 = 1^2 and 9*1^2 + 16*1^2 + 24*0^2 = 5^2.",
				"a(14) = 1 since 14 = 1^2 + 0^2 + 3^2 + 2^2 with 1 = 1^2 and 9*1^2 + 16*0^2 + 24*3^2 = 15^2.",
				"a(728) = 1 since 728 = 10^2 + 0^2 + 12^2 + 22^2 with 0 = 0^2 and 9*10^2 + 16*0^2 + 24*12^2 = 66^2.",
				"a(959) = 1 since 959 = 25^2 + 18^2 + 3^2 + 1^2 with 25 = 5^2 and 9*25^2 + 16*18^2 + 24*3^2 = 105^2.",
				"a(1751) = 1 since 1751 = 19^2 + 25^2 + 18^2 + 21^2 with 25 = 5^2 and 9*19^2 + 16*25^2 + 24*18^2 = 145^2.",
				"a(2311) = 1 since 2311 = 1^2 + 41^2 + 23^2 + 10^2 with 1 = 1^2 and 9*1^2 + 16*41^2 + 24*23^2 = 199^2.",
				"a(6119) = 1 since 6119 = 1^2 + 5^2 + 3^2 + 78^2 with 1 = 1^2 and 9*1^2 + 16*5^2 + 24*3^2 = 25^2."
			],
			"mathematica": [
				"SQ[n_]:=SQ[n]=IntegerQ[Sqrt[n]];",
				"tab={};Do[r=0;Do[If[(SQ[x]||SQ[y]||SQ[z])\u0026\u0026SQ[9x^2+16y^2+24z^2]\u0026\u0026SQ[n-x^2-y^2-z^2],r=r+1],{x,0,Sqrt[n-1]},{y,0,Sqrt[n-1-x^2]},{z,0,Sqrt[n-1-x^2-y^2]}];tab=Append[tab,r],{n,1,80}];Print[tab]"
			],
			"xref": [
				"Cf. A000118, A000290, A271510, A271513, A271518, A281976, A300666, A300667, A300708, A300712, A300751, A300752, A300791."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Zhi-Wei Sun_, Mar 12 2018",
			"references": 8,
			"revision": 7,
			"time": "2018-03-13T04:17:47-04:00",
			"created": "2018-03-13T04:17:47-04:00"
		}
	]
}