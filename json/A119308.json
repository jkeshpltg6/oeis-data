{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A119308",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 119308,
			"data": "1,2,1,3,5,1,4,14,9,1,5,30,40,14,1,6,55,125,90,20,1,7,91,315,385,175,27,1,8,140,686,1274,980,308,35,1,9,204,1344,3528,4116,2184,504,44,1,10,285,2430,8568,14112,11340,4410,780,54,1,11,385,4125",
			"name": "Triangle for first differences of Catalan numbers.",
			"comment": [
				"Row sums are A000245(n+1). Columns include A000330, A006414, as well as certain Kekulé numbers (A114242, A108647, ...).",
				"Diagonal sums are A188460.",
				"Coefficient array of the second column of the inverse of the Riordan array ((1+r*x)/(1+(r+1)x+r*x^2), x/(1+(r+1)x+r*x^2)). - _Paul Barry_, Apr 01 2011"
			],
			"link": [
				"Indranil Ghosh, \u003ca href=\"/A119308/b119308.txt\"\u003eRows 0..100, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = sum{j=0..n, C(n,j)*[k\u003c=j]*C(j+1,k+1)*C(k+1,j-k)/(j-k+1)}.",
				"Column k has g.f.: sum{j=0..k, C(k,j)*C(k+1,j)x^j/(j+1)}*x^k/(1-x)^(2(k+1)).",
				"T(n,k) = sum{j=0..n, C(n,j)*if(k\u003c=j, C(j+1,2(j-k))*A000108(j-k),0)}.",
				"G.f.: (((x-1)*sqrt(x^2*y^2+(-2*x^2-2*x)*y+x^2-2*x+1)+(-x^2-x)*y+x^2-2*x+1)/(2*x^3*y^2)). - _Vladimir Kruchinin_, Nov 15 2020",
				"T(n,k) = C(n+1,m))*(2*C(n+1,m+2)+C(n+1,m+1))/(n+1). - _Vladimir Kruchinin_, Nov 16 2020"
			],
			"example": [
				"Triangle begins:",
				"1;",
				"2,   1;",
				"3,   5,    1;",
				"4,  14,    9,    1;",
				"5,  30,   40,   14,    1;",
				"6,  55,  125,   90,   20,    1;",
				"7,  91,  315,  385,  175,   27,   1;",
				"8, 140,  686, 1274,  980,  308,  35,  1;",
				"9, 204, 1344, 3528, 4116, 2184, 504, 44, 1;"
			],
			"mathematica": [
				"a[k_,j_]:=If[k\u003c=j,Binomial[j+1,2(j-k)]*CatalanNumber[j-k],0];",
				"Flatten[Table[Sum[Binomial[n,j]*a[k,j],{j,0,n}],{n,0,10},{k,0,n}]] (* _Indranil Ghosh_, Mar 03 2017 *)"
			],
			"program": [
				"(PARI)",
				"catalan(n)=binomial(2*n,n)/(n+1);",
				"a(k,j)=if (k\u003c=j,binomial(j+1,2*(j-k))*catalan(j-k),0);",
				"tabl(nn)={for (n=0, nn, for (k=0, n, print1(sum(j=0, n, binomial(n,j)*a(k,j)),\", \"););print(););};",
				"tabl(10); \\\\ _Indranil Ghosh_, Mar 03 2017"
			],
			"xref": [
				"Cf. A000108, A001263."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "0,2",
			"author": "_Paul Barry_, May 13 2006",
			"references": 3,
			"revision": 32,
			"time": "2020-11-17T14:25:09-05:00",
			"created": "2006-05-19T03:00:00-04:00"
		}
	]
}