{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A051150",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 51150,
			"data": "1,-5,1,50,-15,1,-750,275,-30,1,15000,-6250,875,-50,1,-375000,171250,-28125,2125,-75,1,11250000,-5512500,1015000,-91875,4375,-105,1,-393750000,204187500,-41037500,4230625",
			"name": "Generalized Stirling number triangle of first kind.",
			"comment": [
				"a(n,m) = R_n^m(a=0, b=5) in the notation of the given 1961 and 1962 references.",
				"a(n,m) is a Jabotinsky matrix, i.e., the monic row polynomials E(n,x) := Sum_{m=1..n} a(n,m)*x^m = Product_{j=0..n-1} (x - 5*j), n \u003e= 1, and E(0,x) := 1 are exponential convolution polynomials (see A039692 for the definition and a Knuth reference).",
				"This is the signed Stirling1 triangle A008275 with diagonal d \u003e= 0 (main diagonal d = 0) scaled with 5^d."
			],
			"link": [
				"Wolfdieter Lang, \u003ca href=\"/A051150/a051150.txt\"\u003eFirst 10 rows\u003c/a\u003e.",
				"D. S. Mitrinovic, \u003ca href=\"https://gallica.bnf.fr/ark:/12148/bpt6k762d/f996.image.r=1961%20mitrinovic\"\u003eSur une classe de nombres reliés aux nombres de Stirling\u003c/a\u003e, Comptes rendus de l'Académie des sciences de Paris, t. 252 (1961), 2354-2356. [The numbers R_n^m(a,b) are first introduced.]",
				"D. S. Mitrinovic and R. S. Mitrinovic, \u003ca href=\"https://www.jstor.org/stable/43667130\"\u003eTableaux d'une classe de nombres reliés aux nombres de Stirling\u003c/a\u003e, Univ. Beograd. Publ. Elektrotehn. Fak. Ser. Mat. Fiz., No. 77 (1962), 1-77. [Special cases of the numbers R_n^m(a,b) are tabulated.]"
			],
			"formula": [
				"a(n, m) = a(n-1, m-1) - 5*(n-1)*a(n-1, m) for n \u003e= m \u003e= 1; a(n, m) := 0 for n \u003c m; a(n, 0) := 0 for n \u003e= 1; a(0, 0) = 1.",
				"E.g.f. for the m-th column of the signed triangle: (log(1 + 5*x)/5)^m/m!.",
				"a(n, m) = S1(n, m)*5^(n-m), with S1(n, m) := A008275(n, m) (signed Stirling1 triangle)."
			],
			"example": [
				"Triangle a(n,m) (with rows n \u003e= 1 and columns m = 1..n) begins:",
				"        1;",
				"       -5,      1;",
				"       50,    -15,      1;",
				"     -750,    275,    -30,   1;",
				"    15000,  -6250,    875, -50,    1;",
				"  -375000, 171250, -28125, 2125, -75, 1;",
				"  ...",
				"3rd row o.g.f.: E(3,x) = 50*x - 15*x^2 + x^3."
			],
			"xref": [
				"First (m=1) column sequence: A052562(n-1).",
				"Row sums (signed triangle): A008546(n-1)*(-1)^(n-1).",
				"Row sums (unsigned triangle): A008548(n).",
				"Cf. A008275 (Stirling1 triangle, b=1), A039683 (b=2), A051141 (b=3), A051142 (b=4)."
			],
			"keyword": "sign,easy,tabl",
			"offset": "1,2",
			"author": "_Wolfdieter Lang_",
			"references": 8,
			"revision": 19,
			"time": "2020-06-09T06:55:51-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}