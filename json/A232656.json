{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A232656",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 232656,
			"data": "1,4,9,16,21,36,49,40,81,84,101,96,85,196,189,136,145,180,325,336,153,404,529,216,521,340,729,496,393,756,901,520,509,292,1029,384,685,652,765,840,801,612,1849,1016,1701,1060,737,504,2401,2084,1305,1360,1405,1476,521,1096,1629,1572",
			"name": "The number of pairs of numbers below n that, when generating a Fibonacci-like sequence modulo n, contain zeros.",
			"comment": [
				"a(n) = n^2 iff n is in A064414, a(n) is not equal to n^2 iff n is in A230457.",
				"a(n) + A232357(n) = n^2."
			],
			"link": [
				"B. Avila and T. Khovanova, \u003ca href=\"http://arxiv.org/abs/1403.4614\"\u003eFree Fibonacci Sequences\u003c/a\u003e, arXiv preprint arXiv:1403.4614 [math.NT], 2014 and \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Avila/avila4.html\"\u003eJ. Int. Seq. 17 (2014) # 14.8.5\u003c/a\u003e."
			],
			"formula": [
				"Conjecture: a(n) = Sum_{d|n} phi(d)*A001177(d), where phi = Euler's totient function (A000010). - _Logan J. Kleinwaks_, Oct 28 2017",
				"Sum_{d|n} phi(d)*A001177(d) = Sum_{k=1..n} A001177(n/gcd(n,k)) = Sum_{k=1..n} A001177(gcd(n,k))phi(gcd(n,k)/phi(n/gcd(n,k)). - _Richard L. Ollerton_, May 09 2021"
			],
			"example": [
				"The sequence 2,1,3,4,2,1 is the sequence of Lucas numbers modulo 5. Lucas numbers are never divisible by 5. The 4 pairs (2,1), (1,3), (3,4), (4,2) are the only pairs that can generate a sequence modulo 5 that doesn't contain zeros. Thus, a(5) = 21, as 21 other pairs generate sequences that do contain zeros.",
				"Any Fibonacci like sequence contains elements divisible by 2, 3, or 4. Thus, a(2) = 4, a(3) = 9, a(4) = 16."
			],
			"mathematica": [
				"fibLike[list_] := Append[list, list[[-1]] + list[[-2]]]; Table[k^2 -Count[Flatten[Table[Count[Nest[fibLike, {n, m}, k^2]/k, _Integer], {n, k - 1}, {m, k - 1}]], 0], {k, 70}]"
			],
			"xref": [
				"Cf. A064414, A230457, A232357."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Brandon Avila_ and _Tanya Khovanova_, Nov 27 2013",
			"references": 3,
			"revision": 32,
			"time": "2021-05-09T09:53:16-04:00",
			"created": "2013-12-02T05:35:59-05:00"
		}
	]
}