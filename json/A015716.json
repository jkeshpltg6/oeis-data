{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A015716",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 15716,
			"data": "1,0,1,1,1,1,1,0,1,1,1,1,1,1,1,2,2,1,1,1,1,2,2,1,2,1,1,1,3,2,2,1,2,1,1,1,3,3,3,2,2,2,1,1,1,5,4,4,3,2,2,2,1,1,1,5,5,4,3,3,3,2,2,1,1,1,7,6,5,5,4,3,3,2,2,1,1,1,8,7,6,6,4,4,4,3,2,2,1,1",
			"name": "Triangle read by rows: T(n,k) is the number of partitions of n into distinct parts, one of which is k (1\u003c=k\u003c=n).",
			"comment": [
				"Row sums yield A015723. T(n,1)=A025147(n-1); T(n,2)=A015744(n-2); T(n,3)=A015745(n-3); T(n,4)=A015746(n-4); T(n,5)=A015750(n-5). - _Emeric Deutsch_, Mar 29 2006",
				"Number of parts of size k in all partitions of n into distinct parts. Number of partitions of n-k into distinct parts not including a part of size k. - _Franklin T. Adams-Watters_, Jan 24 2012"
			],
			"link": [
				"Mircea Merca, \u003ca href=\"/A015716/b015716.txt\"\u003eTable of n, a(n) for n = 1..7260\u003c/a\u003e"
			],
			"formula": [
				"G.f.: G(t,x) = Product_{j\u003e=1} (1+x^j) * Sum_{i\u003e=1} t^i*x^i/(1+x^i). - _Emeric Deutsch_, Mar 29 2006",
				"From _Mircea Merca_, Feb 28 2014: (Start)",
				"a(n) = A238450(n) + A238451(n).",
				"T(n,k) = Sum_{j=1..floor(n/k)} (-1)^(j-1)*A000009(n-j*k).",
				"G.f.: for column k: q^k/(1+q^k)*(-q;q)_{inf}. (End)"
			],
			"example": [
				"T(8,3)=2 because we have [5,3] and [4,3,1].",
				"Triangle begins:",
				"n/k 1 2 3 4 5 6 7 8 9 10",
				"01: 1",
				"02: 0 1",
				"03: 1 1 1",
				"04: 1 0 1 1",
				"05: 1 1 1 1 1",
				"06: 2 2 1 1 1 1",
				"07: 2 2 1 2 1 1 1",
				"08: 3 2 2 1 2 1 1 1",
				"09: 3 3 3 2 2 2 1 1 1",
				"10: 5 4 4 3 2 2 2 1 1 1",
				"...",
				"The strict integer partitions of 6 are {(6), (5,1), (4,2), (3,2,1)}, with multiset union {1,1,2,2,3,4,5,6}, with multiplicities (2,2,1,1,1,1), which is row n = 6. - _Gus Wiseman_, May 07 2019"
			],
			"maple": [
				"g:=product(1+x^j,j=1..50)*sum(t^i*x^i/(1+x^i),i=1..50): gser:=simplify(series(g,x=0,18)): for n from 1 to 14 do P[n]:=sort(coeff(gser,x^n)) od: for n from 1 to 14 do seq(coeff(P[n],t,j),j=1..n) od; # yields sequence in triangular form - _Emeric Deutsch_, Mar 29 2006",
				"seq(seq(coeff(x^k*(product(1+x^j, j=1..n))/(1+x^k), x, n), k=1..n), n=1..13); # _Mircea Merca_, Feb 28 2014"
			],
			"mathematica": [
				"z = 15; d[n_] := d[n] = Select[IntegerPartitions[n], DeleteDuplicates[#] == # \u0026]; p[n_, k_] := p[n, k] = d[n][[k]]; s[n_] := s[n] = Flatten[Table[p[n, k], {k, 1, PartitionsQ[n]}]]; t[n_, k_] := Count[s[n], k]; u = Table[t[n, k], {n, 1, z}, {k, 1, n}]; TableForm[u] (* A015716 as a triangle *)",
				"v = Flatten[u] (* A015716 as a sequence *)",
				"(* _Clark Kimberling_, Mar 14 2014 *)"
			],
			"xref": [
				"Cf. A015723, A015744, A015745, A015746, A015750, A025147, A027293, A066633.",
				"Cf. A006128, A022629, A066189, A246867, A325504, A325506, A325513."
			],
			"keyword": "nonn,tabl",
			"offset": "1,16",
			"author": "_Clark Kimberling_",
			"references": 5,
			"revision": 43,
			"time": "2019-05-09T15:02:17-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}