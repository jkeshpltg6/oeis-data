{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A134398",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 134398,
			"data": "1,1,1,1,3,1,1,5,5,1,1,7,10,7,1,1,9,16,16,9,1,1,11,23,29,23,11,1,1,13,31,47,47,31,13,1,1,15,40,71,86,71,40,15,1,1,17,50,102,146,146,102,50,17,1,1,19,61,141,234,277,234,141,61,19,1",
			"name": "Triangle read by rows: T(n, k) = (k-1)*(n-k) + binomial(n-1,k-1).",
			"comment": [
				"Row sums = A116725: (1, 2, 5, 12, 26, 52, ...)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A134398/b134398.txt\"\u003eRows n = 1..100 of triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = A077028(n,k) + A007318(n,k) - 1.",
				"Let p(x, n) = (1+x)^n + (1/2) * Sum_{j=1..n-1} (n-j)*j*x^j*(1 + x^(n - 2*j)) with p(x, 0) = 1, then T(n, k) = Coefficients(p(x,n)). - _Roger L. Bagula_, Nov 02 2008",
				"T(n, k) = (k-1)*(n-k) + binomial(n-1,k-1). - _G. C. Greubel_, Nov 29 2019"
			],
			"example": [
				"First few rows of the triangle are:",
				"  1;",
				"  1,  1;",
				"  1,  3,  1;",
				"  1,  5,  5,  1;",
				"  1,  7, 10,  7,  1;",
				"  1,  9, 16, 16,  9,  1;",
				"  1, 11, 23, 29, 23, 11,  1;",
				"  1, 13, 31, 47, 47, 31, 13,  1;",
				"  1, 15, 40, 71, 86, 71, 40, 15, 1;",
				"..."
			],
			"maple": [
				"seq(seq( (k-1)*(n-k) + binomial(n-1,k-1), k=1..n), n=1..10); # _G. C. Greubel_, Nov 29 2019"
			],
			"mathematica": [
				"p[x_, n_]:= p[x,n]= If[n==0, 1, (x+1)^n +Sum[(n-m)*m*x^m*(1 +x^(n-2*m)), {m, 1, n- 1}]/2]; Table[CoefficientList[FullSimplify[ExpandAll[p[x, n]]], x], {n, 0, 10}]//Flatten (* _Roger L. Bagula_, Nov 02 2008 *)",
				"Table[(k-1)*(n-k) + Binomial[n-1, k-1], {n,10}, {k,n}]//Flatten (* _G. C. Greubel_, Nov 29 2019 *)"
			],
			"program": [
				"(PARI) T(n,k) = (k-1)*(n-k) + binomial(n-1,k-1); \\\\ _G. C. Greubel_, Nov 29 2019",
				"(MAGMA) [(k-1)*(n-k) + Binomial(n-1,k-1): k in [1..n], n in [1..10]]; // _G. C. Greubel_, Nov 29 2019",
				"(Sage) [[(k-1)*(n-k) + binomial(n-1,k-1) for k in (1..n)] for n in (1..10)] # _G. C. Greubel_, Nov 29 2019",
				"(GAP) Flat(List([1..10], n-\u003e List([1..n], k-\u003e (k-1)*(n-k) + Binomial(n-1,k-1) ))); # _G. C. Greubel_, Nov 29 2019"
			],
			"xref": [
				"Cf. A007318, A077028, A116725."
			],
			"keyword": "nonn,tabl",
			"offset": "1,5",
			"author": "_Gary W. Adamson_, Oct 23 2007",
			"ext": [
				"Extended by _Roger L. Bagula_, Nov 02 2008",
				"Edited by _G. C. Greubel_, Nov 29 2019"
			],
			"references": 2,
			"revision": 12,
			"time": "2019-11-30T09:16:11-05:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}