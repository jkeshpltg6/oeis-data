{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A212846",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 212846,
			"data": "1,-1,-1,3,15,-21,-441,-477,19935,101979,-1150281,-14838957,60479055,2328851979,3529587879,-403992301437,-3333935426625,72778393505979,1413503392326039,-10851976875907917,-554279405351601105,-713848745428080021",
			"name": "Polylogarithm li(-n,-1/2) multiplied by (3^(n+1))/2.",
			"comment": [
				"Apart from sign, same as A087674: a(n) = A087674*(-1)^n",
				"Given integers n, p, q, 0\u003cp\u003cq, the value of li(-n,-p/q)=sum(k\u003e=0, ((k^n)/(-p/q)^k) ) = s(n), multiplied by ((p+q)^(n+1))/q is an integer a(n). For this sequence set p=1 and q=2."
			],
			"link": [
				"Stanislav Sykora, \u003ca href=\"/A212846/b212846.txt\"\u003eTable of n, a(n) for n = 0..100\u003c/a\u003e",
				"OEIS-Wiki, \u003ca href=\"http://oeis.org/wiki/Eulerian_polynomials\"\u003eEulerian polynomials\u003c/a\u003e",
				"S. Sykora, \u003ca href=\"http://dx.doi.org/10.3247/SL1Math06.002\"\u003eFinite and Infinite Sums of the Power Series (k^p)(x^k)\u003c/a\u003e, Stan's Library Vol. I, April 2006, updated March 2012. See Eq.(29).",
				"Eric W. Weisstein, \u003ca href=\"http://mathworld.wolfram.com/Polylogarithm.html\"\u003e MathWorld: Polylogarithm\u003c/a\u003e"
			],
			"formula": [
				"General recurrence: s(n+1)=(-p/(p+q))*SUM(C(n+1,i)*s(i)), where i=0,1,2,...,n, C(n,m) are binomial coefficients, and the starting value is s(0)=SUM((-p/q)^k)=q/(p+q). For this sequence set p=1 and q=2.",
				"From _Peter Bala_, Jun 24 2012: (Start)",
				"E.g.f.: A(x) = 3/(2+exp(3*x)).",
				"The compositional inverse (A(-x) - 1)^(-1) = x + x^2/2 + 3*x^3/3 + 5*x^4/4 + 11*x^5/5 + ... is the logarithmic generating function for A001045.",
				"(End)",
				"a(n+1) = -3*a(n) + 2*sum(k=0..n, binomial(n,k)*a(k)*a(n-k) ), with a(0) = 1. - _Peter Bala_, Mar 12 2013",
				"Let A(x) be the g.f. of A212846, B(x) the g.f. of A087674, then A(x) = B(-x).",
				"G.f.: 1/Q(0), where Q(k)= 1 + x*(k+1)/(1 - x*(2*k+2)/Q(k+1)); (continued fraction). - _Sergei N. Gladkovskii_, May 20 2013",
				"O.g.f.: Sum_{n\u003e=0} n!*(-x)^n / Product_{k=0..n} (1-3*k*x). - _Paul D. Hanna_, May 30 2013",
				"For n\u003e0, a(n) = -A179929(n)/2. - _Stanislav Sykora_, May 15 2014"
			],
			"example": [
				"a(5) = polylog(-5,-1/2)*3^6/2 = -21.",
				"E.g.f.: A(x) = 1 - x - x^2/2! + 3*x^3/3! + 15*x^4/4! - 21*x^5/5! + ...",
				"O.g.f.: G(x) = 1 - x - x^2 + 3*x^3 + 15*x^4 - 21*x^5 - 441*x^6 +...",
				"where G(x) = 1 - x/(1-3*x) + 2!*x^2/((1-3*x)*(1-6*x)) - 3!*x^3/((1-3*x)*(1-6*x)*(1-9*x)) + 4!*x^4/((1-3*x)*(1-6*x)*(1-9*x)*(1-12*x)) +..."
			],
			"maple": [
				"seq(add((-1)^(n-k)*combinat[eulerian1](n,k)*2^k,k=0..n),n=0..21); # _Peter Luschny_, Apr 21 2013"
			],
			"mathematica": [
				"f[n_] := PolyLog[-n, -1/2] 3^(n + 1)/2; Array[f, 21] (* _Robert G. Wilson v_, May 28 2012 *)",
				"a[ n_] := If[ n \u003c 0, 0, n! 3/2 SeriesCoefficient[ 1 / (1 + Exp[3 x] / 2), {x, 0, n}]]; (* _Michael Somos_, Aug 27 2018 *)"
			],
			"program": [
				"(PARI) /* for this sequence, run limnpq(nmax,1,2)): */",
				"limnpq(nmax,p,q) = {",
				"  f=vector(nmax+1);f[1]=q/(p+q);r=-p/(p+q);",
				"  for (i=2,nmax+1,p1=i-1;bc=1;m=p1;s=0;",
				"    for(j=1,i-1,p2=j-1;if (p2,bc=bc*m/p2;m=m-1;);",
				"    s=s+bc*f[j];);f[i]=r*s;);",
				"fac=(p+q)/q;",
				"for(i=1,nmax+1,f[i]=f[i]*fac;fac=(p+q)*fac;",
				"  write(\"outputfile\",i-1,\" \",f[i]););}",
				"(PARI) x='x+O('x^66); Vec(serlaplace(3/(2+exp(3*x)))) \\\\ _Joerg Arndt_, Apr 21 2013",
				"(PARI) /* O.g.f.: */",
				"{a(n)=polcoeff(sum(m=0, n, m!*(-x)^m/prod(k=1, m, 1-3*k*x+x*O(x^n))), n)} \\\\ _Paul D. Hanna_, May 30 2013"
			],
			"xref": [
				"Similar cases: A210246 (p=1,q=3), A212847 (p=2,q=3)",
				"Cf. A210244 (similar).",
				"Cf. A213127 through A213157.",
				"Cf. A001045, A087674, A179929."
			],
			"keyword": "sign",
			"offset": "0,4",
			"author": "_Stanislav Sykora_, May 28 2012",
			"references": 39,
			"revision": 51,
			"time": "2018-08-27T17:48:34-04:00",
			"created": "2012-06-03T06:46:03-04:00"
		}
	]
}