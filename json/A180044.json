{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A180044",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 180044,
			"data": "7,23,48,22,47,45,45,21,44,163,2105352,162,43,486266,3157729,9859600,5110605,161,6146018,280,8225424,9135075,1684,6185169,1363,159,351,59907600,950,1675,9879408,1358,949,158,95468562,4399220,83722500",
			"name": "Let the n-th Carmichael number A002997(n) = p1*p2*...*pr, where p1 \u003c p2 \u003c ... \u003c pr are primes. Then a(n) = (p1-1) * (p1*p2*...*pr - 1)^(r-2) / ((p2-1)*...*(pr-1)).",
			"comment": [
				"a(n) is always an integer as proved at the Alekseyev link.",
				"The conjecture referred to in A162290 was generalized as follows: Let k be an r-factor Carmichael number (p_1 \u003c p_2 \u003c ... \u003c p_r). Then (p_1-1)*(k-1)^(r-2)/((p_2-1)*(p_3-1)*...*(p_r-1)) is an integer. This was proved by _Max Alekseyev_ (see link).",
				"Contains A162290 as a subsequence."
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A180044/b180044.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Max Alekseyev, \u003ca href=\"http://www.mersenneforum.org/showthread.php?p=55271\u0026amp;postcount=14\"\u003ePomerance's proof\u003c/a\u003e."
			],
			"example": [
				"Since A002997(11) = 41041 = 7*11*13*41, we have a(11) = (6*41040^2) / (10*12*40) = 2105352."
			],
			"mathematica": [
				"lim = 1000001; CarmichaelQ[n_] := Divisible[n - 1, CarmichaelLambda[n]] \u0026\u0026 ! PrimeQ[n]; cc = Select[Table[k, {k, 561, lim, 2}], CarmichaelQ]; lg = Length[cc]; a[n_] := (c = cc[[n]]; pp = FactorInteger[c][[All, 1]]; r = Length[pp]; (pp[[1]] - 1)*((Times @@ pp - 1)^(r - 2)/ Times @@ (Drop[pp, 1] - 1))); Table[a[n], {n, 1, lg}] (* _Jean-François Alcover_, Sep 28 2011 *)"
			],
			"program": [
				"(MAGMA) [ (d[1]-1)*(n-1)^(r-2) / \u0026*[ d[i]-1: i in [2..r] ]: n in [3..700000 by 2] | not IsPrime(n) and IsSquarefree(n) and forall(t){x: x in d | (n-1) mod (x-1) eq 0} where r is #d where d is PrimeDivisors(n)]; // _Klaus Brockhaus_, Aug 10 2010"
			],
			"xref": [
				"Cf. A104016, A104017, A002997, A162290."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_A.K. Devaraj_, Aug 08 2010",
			"ext": [
				"Edited and extended by _Max Alekseyev_ and _Klaus Brockhaus_, Aug 10 2010"
			],
			"references": 1,
			"revision": 24,
			"time": "2021-04-01T10:12:24-04:00",
			"created": "2010-08-27T03:00:00-04:00"
		}
	]
}