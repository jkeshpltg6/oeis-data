{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A034342",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 34342,
			"data": "1,2,4,8,16,36,80,194,505,1439,4559,16580,70491,361339,2278637,17745061,166540680,1794040168,20987476447,254684454328,3107605995993,37418101305011,439995775004885,5025781692126252,55627454599068011,596148004493419480,6186335017615750870,62196701669630203157",
			"name": "Number of binary [ n,8 ] codes of dimension \u003c= 8 without zero columns.",
			"comment": [
				"To get the g.f. of this sequence (with a constant 1), modify the Sage program below (cf. function f). It is too complicated to write it here. - _Petros Hadjicostas_, Sep 30 2019"
			],
			"link": [
				"Discrete algorithms at the University of Bayreuth, \u003ca href=\"http://www.algorithm.uni-bayreuth.de/en/research/SYMMETRICA/\"\u003eSymmetrica\u003c/a\u003e. [This package was used by Harald Fripertinger to compute T_{nk2} = A076832(n,k) using the cycle index of PGL_k(2). Here k = 8. That is, a(n) = T_{n,8,2} = A076832(n,8), but we start at n = 1 rather than at n = 8.]",
				"Harald Fripertinger, \u003ca href=\"http://www.mathe2.uni-bayreuth.de/frib/codes/tables.html\"\u003eIsometry Classes of Codes\u003c/a\u003e.",
				"Harald Fripertinger, \u003ca href=\"http://www.mathe2.uni-bayreuth.de/frib/codes/tables_3.html\"\u003eTnk2: Number of the isometry classes of all binary (n,r)-codes for 1 \u003c= r \u003c= k without zero-columns\u003c/a\u003e. [This is a rectangular array whose lower triangle is A076832(n,k). Here we have column k = 8.]",
				"Harald Fripertinger, \u003ca href=\"https://imsc.uni-graz.at/fripertinger/codes_bms.html\"\u003eEnumeration of isometry classes of linear (n,k)-codes over GF(q) in SYMMETRICA\u003c/a\u003e, Bayreuther Mathematische Schriften 49 (1995), 215-223. [See pp. 216-218. A C-program is given for calculating T_{nk2} in Symmetrica. Here k = 8.]",
				"Harald Fripertinger, \u003ca href=\"https://doi.org/10.1016/S0024-3795(96)00530-7\"\u003eCycle of indices of linear, affine, and projective groups\u003c/a\u003e, Linear Algebra and its Applications 263 (1997), 133-156. [See p. 152 for the computation of T_{nk2} = A076832(n,k). Here k = 8.]",
				"H. Fripertinger and A. Kerber, \u003ca href=\"https://doi.org/10.1007/3-540-60114-7_15\"\u003eIsometry classes of indecomposable linear codes\u003c/a\u003e. In: G. Cohen, M. Giusti, T. Mora (eds), Applied Algebra, Algebraic Algorithms and Error-Correcting Codes, 11th International Symposium, AAECC 1995, Lect. Notes Comp. Sci. 948 (1995), pp. 194-204. [The notation for A076832(n,k) is T_{nk2}. Here k = 8.]",
				"David Slepian, \u003ca href=\"https://archive.org/details/bstj39-5-1219\"\u003eSome further theory of group codes\u003c/a\u003e, Bell System Tech. J. 39(5) (1960), 1219-1252.",
				"David Slepian, \u003ca href=\"https://doi.org/10.1002/j.1538-7305.1960.tb03958.x\"\u003eSome further theory of group codes\u003c/a\u003e, Bell System Tech. J. 39(5) (1960), 1219-1252.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Cycle_index\"\u003eCycle index\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Projective_linear_group\"\u003eProjective linear group\u003c/a\u003e."
			],
			"program": [
				"(Sage) # Fripertinger's method to find the g.f. of column k for small k:",
				"def Tcol(k, length):",
				"    G = PSL(k, GF(2))",
				"    D = G.cycle_index()",
				"    f = sum(i[1]*prod(1/(1-x^j) for j in i[0]) for i in D)",
				"    return f.taylor(x, 0, length).list()",
				"# For instance the Taylor expansion for column k = 8 gives a(n):",
				"print(Tcol(8, 30)) # _Petros Hadjicostas_, Sep 30 2019"
			],
			"xref": [
				"Column k=8 of A076832 (starting at n=8).",
				"Cf. A034337."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_.",
			"ext": [
				"More terms from _Petros Hadjicostas_, Sep 30 2019"
			],
			"references": 2,
			"revision": 27,
			"time": "2019-10-01T09:10:01-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}