{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006842",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6842,
			"id": "M0041",
			"data": "0,1,0,1,1,0,1,1,2,1,0,1,1,1,2,3,1,0,1,1,1,2,1,3,2,3,4,1,0,1,1,1,1,2,1,3,2,3,4,5,1,0,1,1,1,1,2,1,2,3,1,4,3,2,5,3,4,5,6,1,0,1,1,1,1,1,2,1,3,2,3,1,4,3,5,2,5,3,4,5,6,7,1,0,1,1,1,1,1,2,1,2,1,3,2,3,4,1,5,4,3,5,2,5",
			"name": "Triangle read by rows: row n gives numerators of Farey series of order n.",
			"reference": [
				"A. H. Beiler, Recreations in the Theory of Numbers, Dover, NY, 1964",
				"Bruckheimer, Maxim, and Abraham Arcavi. \"Farey series and Pick’s area theorem.\" The mathematical intelligencer 17.4 (1995): 64-67.",
				"Cobeli, Cristian, and Alexandru Zaharescu. \"The Haros-Farey sequence at two hundred years.\" Acta Univ. Apulensis Math. Inform 5 (2003): 1-38.",
				"J. H. Conway and R. K. Guy, The Book of Numbers, Copernicus Press, NY, 1996, p. 152.",
				"L. E. Dickson, History of the Theory of Numbers. Carnegie Institute Public. 256, Washington, DC, Vol. 1, 1919; Vol. 2, 1920; Vol. 3, 1923. See Vol. 1.",
				"Guthery, Scott B. A motif of mathematics. Docent Press, 2011.",
				"G. H. Hardy and E. M. Wright, An Introduction to the Theory of Numbers. 3rd ed., Oxford Univ. Press, 1954, p. 23.",
				"W. J. LeVeque, Topics in Number Theory. Addison-Wesley, Reading, MA, 2 vols., 1956, Vol. 1, p. 154.",
				"A. O. Matveev, Farey Sequences, De Gruyter, 2017.",
				"I. Niven and H. S. Zuckerman, An Introduction to the Theory of Numbers. 2nd ed., Wiley, NY, 1966, p. 141.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A006842/b006842.txt\"\u003eTable of n, a(n) for n = 1..10563\u003c/a\u003e",
				"Andrey O. Matveev, \u003ca href=\"http://arxiv.org/abs/0801.1981\"\u003eNeighboring Fractions in Farey Subsequences\u003c/a\u003e, arXiv:0801.1981 [math.NT], 2008-2010.",
				"N. J. A. Sloane, \u003ca href=\"/stern_brocot.html\"\u003eStern-Brocot or Farey Tree\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/FareySequence.html\"\u003eFarey Sequence.\u003c/a\u003e",
				"\u003ca href=\"/index/St#Stern\"\u003eIndex entries for sequences related to Stern's sequences\u003c/a\u003e"
			],
			"example": [
				"0/1, 1/1;",
				"0/1, 1/2, 1/1;",
				"0/1, 1/3, 1/2, 2/3, 1/1;",
				"0/1, 1/4, 1/3, 1/2, 2/3, 3/4, 1/1;",
				"0/1, 1/5, 1/4, 1/3, 2/5, 1/2, 3/5, 2/3, 3/4, 4/5, 1/1;",
				"... = A006842/A006843"
			],
			"maple": [
				"Farey := proc(n) sort(convert(`union`({0},{seq(seq(m/k,m=1..k),k=1..n)}),list)) end: seq(numer(Farey(i)),i=1..5); # _Peter Luschny_, Apr 28 2009"
			],
			"mathematica": [
				"Farey[n_] := Union[ Flatten[ Join[{0}, Table[a/b, {b, n}, {a, b}]]]]; Flatten[ Table[ Numerator[ Farey[n]], {n, 0, 9}]] (* _Robert G. Wilson v_, Apr 08 2004 *)",
				"Table[FareySequence[n] // Numerator, {n, 1, 9}] // Flatten (* _Jean-François Alcover_, Sep 25 2018 *)"
			],
			"program": [
				"(PARI) row(n) = {vf = [0]; for (k=1, n, for (m=1, k, vf = concat(vf, m/k););); vf = vecsort(Set(vf)); for (i=1, #vf, print1(numerator(vf[i]), \", \"));} \\\\ _Michel Marcus_, Jun 27 2014"
			],
			"xref": [
				"Row n has A005728(n) terms. - _Michel Marcus_, Jun 27 2014",
				"Cf. A006843 (denominators), A049455, A049456, A007305, A007306. Also A177405/A177407."
			],
			"keyword": "nonn,nice,frac,tabf",
			"offset": "1,9",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Robert G. Wilson v_, Apr 08 2004"
			],
			"references": 25,
			"revision": 53,
			"time": "2019-11-22T04:20:56-05:00",
			"created": "1991-07-25T03:00:00-04:00"
		}
	]
}