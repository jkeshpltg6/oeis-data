{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A173419",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 173419,
			"data": "0,1,2,2,3,3,4,3,3,4,4,4,5,4,4,3,4,4,5,4,5,5,5,4,4,5,4,5,5,5,5,4,5,5,5,4,5,5,5,5,6,5,6,6,5,6,6,5,5,5,6,6,6,5,6,5,6,6,6,5,6,5,5,4,5,5,6,5,6,6,6,5,6,6,5,6,6,5,5,5,4,5,5,5,6,6,6,6,6,5,6,6,6,6,6,5,6,6,5,5,6,6,6,6,6,6,6,5",
			"name": "Length of shortest computation yielding n using addition, subtraction and multiplication (starting from 1).",
			"comment": [
				"Let x_0 = 1 and x_m = n, with each x_k = x_i + x_j, x_k = x_i * x_j, or x_k = x_i - x_j for some 0 \u003c= i,j \u003c k. a(n) is the least such m.",
				"Shub \u0026 Smale ask if there is a c such that a(n!) \u003c= (log n)^c for all n.",
				"If for any sequence of nonzero integers (m_i) there is no constant c such that a(n! * m_n) \u003c= (log n)^c, then \"the Hilbert Nullstellensatz is intractable, and consequently the algebraic version of 'NP != P' is true\" (Shub \u0026 Smale).",
				"Conjecture: if n is prime then a(n) \u003e= a(n-1). The conjecture is true for n \u003c 1800. - _Dmitry Kamenetsky_, Dec 26 2019"
			],
			"reference": [
				"R. K. Guy, Unsolved Problems Number Theory, Sect. F26."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A173419/b173419.txt\"\u003eTable of n, a(n) for n = 1..1800\u003c/a\u003e",
				"Peter Borwein and Joe Hobart, \u003ca href=\"http://www.jstor.org/stable/10.4169/amer.math.monthly.119.07.584\"\u003eThe extraordinary power of division in straight line programs\u003c/a\u003e, American Mathematical Monthly 119:7 (2012), pp. 584-592.",
				"F. Cucker, M. Shub, and S. Smale, \u003ca href=\"http://www6.cityu.edu.hk/ma/doc/people/smales/pap96.pdf\"\u003eSeparation of Complexity classes in Koiran's weak model\u003c/a\u003e, Theoretical Computer Science 133:1 (1994), pp. 3-14.",
				"W. DeMelo and B. F. Svaiter, \u003ca href=\"http://www.ams.org/journals/proc/1996-124-05/S0002-9939-96-03173-5/S0002-9939-96-03173-5.pdf\"\u003eThe cost of computing integers\u003c/a\u003e, Proc. Amer. Math. Soc. 124 (1996), pp. 1377-1378.",
				"P. Koiran, \u003ca href=\"http://perso.ens-lyon.fr/pascal.koiran/Publis/tau.springer.pdf\"\u003eValiant's model and the cost of computing integers\u003c/a\u003e, Comput. Complex. 13 (2004), pp. 131-146.",
				"Carlos Gustavo T. de A. Moreira, \u003ca href=\"http://w3.impa.br/~gugu/GU.ps\"\u003eOn asymptotic estimates for arithmetic cost functions\u003c/a\u003e, Proceedings of the American Mathematical Society 125:2 (1997), pp. 347-353.",
				"Richard J. Mathar, \u003ca href=\"/A173419/a173419.txt\"\u003eExtended list of chain examples\u003c/a\u003e",
				"Michael Shub and Steve Smale, \u003ca href=\"http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.100.5035\"\u003eOn the intractability of Hilbert's Nullstellensatz and an algebraic version of \"NP = P\"\u003c/a\u003e, Duke Mathematical Journal 81:1 (1995), pp. 47-54.",
				"Al Zimmermann's Programming Contests, \u003ca href=\"http://azspcs.com/Contest/Factorials\"\u003eFactorials\u003c/a\u003e",
				"\u003ca href=\"/index/Com#complexity\"\u003eIndex to sequences related to the complexity of n\u003c/a\u003e"
			],
			"formula": [
				"a(n) \u003c= 2 log_2(n).",
				"a(n) \u003e= log_2(log_2(n)) + 1.",
				"a(n) \u003e= log_2(n)/log_2(log_2(n)) for almost all n, as proved by Moreira (improving DeMelo \u0026 Svaiter)."
			],
			"example": [
				"For n = 9, one sequence is (1, 1 + 1 = 2, 1 + 2 = 3, 3 * 3 = 9). Since no shorter sequence is possible, a(9) = 3.",
				"For n = 96, one sequence is (1, 1 + 1 = 2, 2 + 2 = 4, 2 + 4 = 6, 4*4 = 16, 6*16 = 96); no shorter is possible so a(96) = 5."
			],
			"maple": [
				"g:= f-\u003eseq(f union {t}, t={seq(seq({i+j, i-j, i*j}[], j=f), i=f)} minus f):",
				"F:= proc(n) F(n):= map(g, F(n-1)) end: F(0):= {{1}}:",
				"S:= proc(n) S(n):= map(x-\u003ex[], F(n)) end:",
				"a:= proc(n) local k; for k from 0 while not(n in S(k)) do od; k end:",
				"seq(a(n), n=1..110);  # _Alois P. Heinz_, Sep 24 2012"
			],
			"xref": [
				"Records are essentially A141414.",
				"Cf. A003313 (shortest chain using just addition), A005245 (number of 1s using just addition and multiplication), A217032(n):=A173419(n!)."
			],
			"keyword": "nice,nonn",
			"offset": "1,3",
			"author": "_Charles R Greathouse IV_, Feb 17 2010, Apr 22 2010",
			"references": 16,
			"revision": 59,
			"time": "2020-01-14T00:59:58-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}