{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A036913",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 36913,
			"data": "2,6,12,18,30,42,60,66,90,120,126,150,210,240,270,330,420,462,510,630,660,690,840,870,1050,1260,1320,1470,1680,1890,2310,2730,2940,3150,3570,3990,4620,4830,5460,5610,5670,6090,6930,7140,7350,8190,9240,9660",
			"name": "Sparsely totient numbers; numbers n such that m \u003e n implies phi(m) \u003e phi(n).",
			"comment": [
				"The paper by Masser and Shiu lists 150 terms of this sequence less than 10^6. For odd prime p, they show that p# and p*p# are in this sequence, where p# denotes the primorial (A002110). - _T. D. Noe_, Jun 14 2006"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A036913/b036913.txt\"\u003eTable of n, a(n) for n = 1..5000\u003c/a\u003e",
				"Roger C. Baker and Glyn Harman, \u003ca href=\"http://www.numdam.org/item?id=AFST_1996_6_5_2_183_0\"\u003eSparsely totient numbers\u003c/a\u003e, Annales de la Faculté des Sciences de Toulouse Ser. 6, 5 no. 2 (1996), 183-190.",
				"Glyn Harman, \u003ca href=\"https://doi.org/10.1017/S0017089500008417\"\u003eOn sparsely totient numbers\u003c/a\u003e, Glasgow Math. J. 33 (1991), 349-358.",
				"D. W. Masser and P. Shiu, \u003ca href=\"https://projecteuclid.org/download/pdf_1/euclid.pjm/1102702441\"\u003eOn sparsely totient numbers\u003c/a\u003e, Pacific J. Math. 121, no. 2 (1986), 407-426.",
				"Michael De Vlieger, \u003ca href=\"/A036913/a036913.txt\"\u003eLargest k such that A002110(k) | a(n) and A287352(a(n))\u003c/a\u003e.",
				"Michael De Vlieger, \u003ca href=\"/A036913/a036913_1.txt\"\u003eFirst term m \u003e prime(n)^2 in A036913 such that gcd(prime(n), m) = 1\u003c/a\u003e."
			],
			"example": [
				"This sequence contains 60 because of all the numbers whose totient is \u003c=16, 60 is the largest such number. [From _Graeme McRae_, Feb 12 2009]",
				"From _Michael De Vlieger_, Jun 25 2017: (Start)",
				"Positions of primorials A002110(k) in a(n):",
				"     n     k       a(n) = A002110(k)",
				"  ----------------------------------",
				"     1     1                       2",
				"     2     2                       6",
				"     5     3                      30",
				"    13     4                     210",
				"    31     5                    2310",
				"    69     6                   30030",
				"   136     7                  510510",
				"   231     8                 9699690",
				"   374     9               223092870",
				"   578    10              6469693230",
				"   836    11            200560490130",
				"  1169    12           7420738134810",
				"  1591    13         304250263527210",
				"  2149    14       13082761331670030",
				"  2831    15      614889782588491410",
				"  3667    16    32589158477190044730",
				"  4661    17  1922760350154212639070",
				"(End)"
			],
			"mathematica": [
				"nn=10000; lastN=Table[0,{nn}]; Do[e=EulerPhi[n]; If[e\u003c=nn, lastN[[e]]=n], {n,10nn}]; mx=0; lst={}; Do[If[lastN[[i]]\u003emx, mx=lastN[[i]]; AppendTo[lst,mx]], {i,Length[lastN]}]; lst - _T. D. Noe_, Jun 14 2006",
				"Union@ FoldList[Max, Values[#][[All, -1]]] \u0026@ KeySort@ PositionIndex@ EulerPhi@ Range[10^4] (* _Michael De Vlieger_, Jun 06 2017, Version 10 *)"
			],
			"xref": [
				"Cf. A097942 (highly totient numbers). Records in A006511 (see also A132154)."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_David W. Wilson_",
			"references": 12,
			"revision": 34,
			"time": "2021-03-17T23:41:56-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}