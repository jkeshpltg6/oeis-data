{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A287674",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 287674,
			"data": "1,0,0,1,1,1,0,0,1,1,0,0,1,1,1,0,0,1,1,1,0,0,1,1,0,0,1,1,1,0,0,1,1,0,0,1,1,1,0,0,1,1,1,0,0,1,1,0,0,1,1,1,0,0,1,1,1,0,0,1,1,0,0,1,1,1,0,0,1,1,0,0,1,1,1,0,0,1,1,1,0,0,1,1,0,0",
			"name": "{0-\u003e1, 1-\u003e001}-transform of the infinite Fibonacci word A003849.",
			"comment": [
				"Appears to be the same as A286749 shifted by one place/index. - _R. J. Mathar_, Jun 14 2017",
				"From _Michel Dekking_, Sep 05 2020: (Start)",
				"This sequence is a morphic sequence, i.e., the letter-to-letter image of the fixed point of a morphism mu. In fact, one can take the alphabet {1,2,3,4} with the morphism",
				"      mu:  1-\u003e12341, 2-\u003e1, 3-\u003e2, 4-\u003e34,",
				"and the letter-to-letter map g defined by",
				"      g:  1-\u003e1, 2-\u003e0, 3-\u003e0, 4-\u003e1.",
				"Then (a(n)) = g(x), where x = 123411234... is the unique fixed point of the morphism mu.",
				"In my paper  \"Morphic words, Beatty sequences and integer images of the Fibonacci language\" the transform map 0-\u003e1, 1-\u003e001 is called a decoration. It is well-known that decorated fixed points are morphic sequences, and the 'natural' algorithm to achieve this yields a morphism on an alphabet of 1+3 = 4 symbols. In general there are several choices for mu. Here we have chosen mu such that it is the SAME morphism as used to generate A286749 as a morphic sequence.",
				"This will make it possible to prove the remarkable conjecture by  R. J. Mathar from June 14, 2017. Note that A286749 is a letter to letter projection of the SAME sequence x  = 123411234... but by a different  letter to letter map f:",
				"        f:  1-\u003e1, 2-\u003e1, 3-\u003e0, 4-\u003e0.",
				"How does one prove that A286749(n+1) = A287674(n) = a(n), for n\u003e0?",
				"The crucial observation is that x is a concatenation of the two words 12341 and 1234. This follows directly from mu(12341)= 12341123412341, and mu(1234)=123411234. Next one observes that",
				"    f(12341) = 11001 = 1g(12341)1^{-1},",
				"    f(1234) = 1100 = 1g(1234)1^{-1}.",
				"This property implies then that A286749(n+1) = a(n).",
				"(End)"
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A287674/b287674.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"M. Dekking, \u003ca href=\"https://doi.org/10.1016/j.tcs.2019.12.036\"\u003eMorphic words, Beatty sequences and integer images of the Fibonacci language\u003c/a\u003e, Theoretical Computer Science 809, 407-417 (2020)."
			],
			"example": [
				"As a word, A003849 = 0100101001001010010100100..., and replacing each 0 by 1 and each 1 by 001 gives 100111001100111001110011001..."
			],
			"mathematica": [
				"s = Nest[Flatten[# /. {0 -\u003e {0, 1}, 1 -\u003e {0}}] \u0026, {0}, 10] (* A003849 *)",
				"w = StringJoin[Map[ToString, s]]",
				"w1 = StringReplace[w, {\"0\" -\u003e \"1\", \"1\" -\u003e \"001\"}]",
				"st = ToCharacterCode[w1] - 48    (* A287674 *)",
				"Flatten[Position[st, 0]]  (* A287675 *)",
				"Flatten[Position[st, 1]]  (* A287676 *)"
			],
			"xref": [
				"Cf. A287675, A287676."
			],
			"keyword": "nonn,easy",
			"offset": "1",
			"author": "_Clark Kimberling_, Jun 02 2017",
			"references": 4,
			"revision": 13,
			"time": "2020-09-06T03:42:43-04:00",
			"created": "2017-06-02T15:02:00-04:00"
		}
	]
}