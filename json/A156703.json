{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A156703",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 156703,
			"data": "5,75,8,875,9,9375,95,96,96875,975,98,984375,9875,99,992,9921875,99375,995,996,99609375,996875,9975,998,998046875,9984,9984375,99875,999,9990234375,9992,99921875,999375,9995,99951171875,9996,999609375,99968,9996875,99975",
			"name": "String of digits encountered in decimal expansion of successive ratios k/(k+1), treating only non-repeating expansions, with decimal point and leading and trailing zeros removed.",
			"comment": [
				"The sequence seems infinite and may be volatile in its extrema.",
				"Conjecture: subsets of the sequence (as it fills out) will correspond to the odd integers by length.",
				"Thus, there are 3 single-digit entries in range {1-9}, ending at 9; 5 two-digit entries in range {10-99} ending at 99; 7 three-digit entries in range {100-999} ending at 999, etc. The remainder set of course are all repeating decimals.",
				"Denominators of the ratios that yield each term must be terms of A003592 (i.e., any integer m whose distinct prime factors p also divide 10, or m regular to 10), since only these denominators produce non-repeating decimal expansions. - _Michael De Vlieger_, Dec 30 2015"
			],
			"reference": [
				"G. H. Hardy and E. M. Wright, An Introduction to the Theory of Numbers, Sixth Edition, Oxford University Press, 2008, pages 141-144 (including Theorem 135)."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A156703/b156703.txt\"\u003eTable of n, a(n) for n = 1..6000\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/DecimalExpansion.html\"\u003eDecimal Expansion\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics,\u003ca href=\"http://mathworld.wolfram.com/RegularNumber.html\"\u003eRegular Number\u003c/a\u003e",
				"Wikimedia Commons, \u003ca href=\"http://commons.wikimedia.org/wiki/File:OEIS_A156703.svg\"\u003eAlternate plot\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 10^d*(k-1)/k where k = A003592(n+1) = 2^i*5^j and d=max(i,j). - _Robert Israel_, Dec 29 2015"
			],
			"example": [
				"1/2 = 0.5 (non-repeating), which yields a(1) = 5.",
				"2/3 = 0.6666... (repeating, so does not yield a term in the sequence).",
				"3/4 = 0.75 (non-repeating), which yields a(2) = 75.",
				"4/5 = 0.8 (non-repeating), which yields a(3) = 8."
			],
			"maple": [
				"N:= 10^5: # to get terms for denominators \u003c= N",
				"B:= sort([seq(seq(2^i*5^j,i=0..ilog2(N/5^j)),j=0..ilog(N,5))]):",
				"seq(10^max(padic:-ordp(n,2),padic:-ordp(n,5))*(n-1)/n, n=B[2..-1]); # _Robert Israel_, Dec 29 2015"
			],
			"mathematica": [
				"FromDigits@ First@ # \u0026 /@ RealDigits@ Apply[#1/#2 \u0026, Transpose@ {# - 1, #} \u0026@ Select[Range@ 10000, AllTrue[First /@ FactorInteger@ #, MemberQ[{2, 5}, #] \u0026] \u0026], 1] (* _Michael De Vlieger_, Dec 30 2015, Version 10 *)",
				"FromDigits@ First@# \u0026 /@ RealDigits@ Apply[#1/#2 \u0026, Transpose@ {# - 1, #} \u0026@ Select[Range@ 10000, First@ Union@ Map[MemberQ[{2, 5}, #] \u0026, First /@ FactorInteger@ #] \u0026], 1] (* _Michael De Vlieger_, Dec 30 2015, Version 6 *)"
			],
			"program": [
				"(PARI)",
				"list(maxx)={my(N, vf=List()); maxx++;for(n=0, log(maxx)\\log(5),",
				"N=5^n; maxVal= 0;while(N\u003c=maxx, if (N != 1, listput(vf, (N-1)/N));",
				"N\u003c\u003c=1;)); vf = vecsort(Vec(vf));for (i=1,length(vf),",
				"while(denominator(vf[i]) != 1, vf[i] *= 10););print(vf);}",
				"\\\\ adapted from A158911 code, courtesy _Michel Marcus_, Dec 29 2015",
				"(Python)",
				"import string,copy",
				"from decimal import *",
				"getcontext().prec = 200",
				"maxx=1000",
				"n=1",
				"maxLen=0",
				"while n\u003cmaxx:",
				"    q=Decimal(n)/Decimal(n+1)",
				"    ratio=str(q)",
				"    myLen=len(ratio)",
				"    ratio.replace(\" \",\"\")",
				"    if len(ratio[2:])\u003c15:",
				"        print(ratio[2:])",
				"    else:",
				"        strCopy=copy.copy(ratio[2:])",
				"        match=0",
				"        maxCnt=0",
				"        keyStr=' '",
				"        subLen=n",
				"        cap=len(ratio[2:])",
				"        for j5 in range(0,cap ):",
				"            for i5 in range(subLen,1,-1):",
				"                if i5\u003c=j5:",
				"                    break",
				"                subStr=strCopy[j5:i5]",
				"                if len(subStr)\u003c1:",
				"                    continue",
				"                match=strCopy.count(subStr)",
				"                z=match*len(subStr)",
				"                if z\u003emaxCnt and match\u003e1:",
				"                    if len(subStr)==1 and z\u003csubLen:",
				"                        maxCnt=z",
				"                        keyStr=copy.copy(subStr)",
				"                    else:",
				"                        maxCnt=z",
				"                        keyStr=copy.copy(subStr)",
				"        if maxCnt\u003e4:",
				"            pass",
				"        else:",
				"            print(ratio[2:])",
				"        getcontext().prec = max(2*subLen,200)",
				"    n+=1",
				"# _Bill McEachen_, Dec 28 2015"
			],
			"xref": [
				"Cf. A003592, A158911. See comment at A158911."
			],
			"keyword": "easy,nonn,base",
			"offset": "1,1",
			"author": "_Bill McEachen_, Feb 13 2009",
			"references": 4,
			"revision": 52,
			"time": "2020-03-23T06:46:10-04:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}