{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A342955",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 342955,
			"data": "0,0,0,0,1,0,0,1,1,0,0,1,2,1,0,0,1,2,2,1,0,0,1,2,3,2,1,0,0,1,2,3,3,2,1,0,0,1,2,3,4,3,2,1,0,0,1,2,3,4,4,3,2,1,0,0,1,2,3,4,5,4,3,2,1,0,0,0,2,3,4,5,5,4,3,2,0,0,0,1,0,3,4,5,6,5,4,3,0,1,0",
			"name": "Array T(n,k), n, k \u003e= 0, read by antidiagonals; the i-th decimal digit of T(n, k) is the smallest of the i-th digits of n and of k.",
			"comment": [
				"This sequence has similarities with lunar addition (A087061); here we take the smallest, there the largest digits.  It is \"lunar multiplication\" of corresponding digits.",
				"The bitwise AND operator (A004198) is the binary analog."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A342955/b342955.txt\"\u003eTable of n, a(n) for n = 0..10010\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A342955/a342955.png\"\u003eColored representation of the array for n, k \u003c 1000\u003c/a\u003e (where the color is function of T(n, k))"
			],
			"formula": [
				"T(n, k) = T(k, n).",
				"T(m, T(n, k)) = T(T(m, n), k).",
				"T(n, n) = n.",
				"T(n, 0) = 0.",
				"T(n, k) + A087061(n, k) = n + k."
			],
			"example": [
				"Array T(n, k) begins:",
				"  n\\k|  0  1  2  3  4  5  6  7  8  9  10  11  12  13",
				"  ---+----------------------------------------------",
				"    0|  0  0  0  0  0  0  0  0  0  0   0   0   0   0",
				"    1|  0  1  1  1  1  1  1  1  1  1   0   1   1   1",
				"    2|  0  1  2  2  2  2  2  2  2  2   0   1   2   2",
				"    3|  0  1  2  3  3  3  3  3  3  3   0   1   2   3",
				"    4|  0  1  2  3  4  4  4  4  4  4   0   1   2   3",
				"    5|  0  1  2  3  4  5  5  5  5  5   0   1   2   3",
				"    6|  0  1  2  3  4  5  6  6  6  6   0   1   2   3",
				"    7|  0  1  2  3  4  5  6  7  7  7   0   1   2   3",
				"    8|  0  1  2  3  4  5  6  7  8  8   0   1   2   3",
				"    9|  0  1  2  3  4  5  6  7  8  9   0   1   2   3",
				"   10|  0  0  0  0  0  0  0  0  0  0  10  10  10  10",
				"   11|  0  1  1  1  1  1  1  1  1  1  10  11  11  11",
				"   12|  0  1  2  2  2  2  2  2  2  2  10  11  12  12",
				"   13|  0  1  2  3  3  3  3  3  3  3  10  11  12  13"
			],
			"program": [
				"(PARI) T(n,k,base=10) = if (n==0 || k==0, 0, T(n\\base,k\\base)*base + min(n%base, k%base))"
			],
			"xref": [
				"Cf. A004197 (numerical minimum), A004198 (bit-wise minimum), A087061 (digit-wise maximum)."
			],
			"keyword": "nonn,tabl,base,easy,look",
			"offset": "0,13",
			"author": "_Rémy Sigrist_, Apr 03 2021",
			"references": 1,
			"revision": 59,
			"time": "2021-04-05T03:57:06-04:00",
			"created": "2021-04-04T00:58:48-04:00"
		}
	]
}