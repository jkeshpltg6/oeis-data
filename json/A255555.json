{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A255555",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 255555,
			"data": "1,3,2,7,4,5,15,8,10,6,31,16,19,11,9,63,32,38,22,18,12,127,64,74,42,35,23,13,255,128,146,82,70,46,25,14,511,256,290,162,138,89,49,26,17,1023,512,578,322,274,176,97,50,34,20,2047,1024,1154,642,546,350,193,98,67,39,21,4095,2048,2306,1282,1090,695,385,194,134,78,41,24",
			"name": "Square array A(row,col) read by downwards antidiagonals: A(1,1) = 1, A(row,1) = A055938(row-1), and for col \u003e 1, A(row,col) = A005187(1+A(row,col-1)).",
			"comment": [
				"The array is read by antidiagonals: A(1,1), A(1,2), A(2,1), A(1,3), A(2,2), A(3,1), etc.",
				"Provided that I understand Kimberling's terminology correctly, this array is the dispersion of sequence b(n) = A005187(n+1), for n\u003e=1: A005187[2..] = [3, 4, 7, 8, 10, 11, ...]. The left column is the complement of that sequence, which is {1} followed by A055938. - _Antti Karttunen_, Apr 17 2015"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A255555/b255555.txt\"\u003eTable of n, a(n) for n = 1..10440; the first 144 antidiagonals of array\u003c/a\u003e",
				"Clark Kimberling, \u003ca href=\"http://faculty.evansville.edu/ck6/integer/intersp.html\"\u003eInterspersions and Dispersions\u003c/a\u003e.",
				"Clark Kimberling, \u003ca href=\"http://www.jstor.org/discover/10.2307/2159163\"\u003eInterspersions and Dispersions\u003c/a\u003e, Proceedings of the American Mathematical Society, 117 (1993) 313-321.",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"A(1,1) = 1, A(row,1) = A055938(row-1), and for col \u003e 1, A(row,col) = A005187(1+A(row,col-1))."
			],
			"example": [
				"The top left corner of the array:",
				"   1,  3,  7,  15,  31,  63,  127,  255,  511, 1023,  2047,  4095",
				"   2,  4,  8,  16,  32,  64,  128,  256,  512, 1024,  2048,  4096",
				"   5, 10, 19,  38,  74, 146,  290,  578, 1154, 2306,  4610,  9218",
				"   6, 11, 22,  42,  82, 162,  322,  642, 1282, 2562,  5122, 10242",
				"   9, 18, 35,  70, 138, 274,  546, 1090, 2178, 4354,  8706, 17410",
				"  12, 23, 46,  89, 176, 350,  695, 1387, 2770, 5535, 11067, 22128",
				"  13, 25, 49,  97, 193, 385,  769, 1537, 3073, 6145, 12289, 24577",
				"  14, 26, 50,  98, 194, 386,  770, 1538, 3074, 6146, 12290, 24578",
				"  17, 34, 67, 134, 266, 530, 1058, 2114, 4226, 8450, 16898, 33794",
				"  20, 39, 78, 153, 304, 606, 1207, 2411, 4818, 9631, 19259, 38512",
				"  ..."
			],
			"program": [
				"(Scheme)",
				"(define (A255555 n) (A255555bi (A002260 n) (A004736 n)))",
				"(define (A255555bi row col) (if (= 1 col) (if (= 1 row) 1 (A055938 (- row 1))) (A005187 (+ 1 (A255555bi row (- col 1))))))"
			],
			"xref": [
				"Inverse permutation: A255556.",
				"Transpose: A255557.",
				"Row 1: A000225.",
				"Cf. A005187, A055938.",
				"Cf. A255559 (column index), A255560 (row index).",
				"Cf. also A254105, A256995 (variants), A233275-A233278."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Antti Karttunen_, Apr 13 2015",
			"references": 9,
			"revision": 20,
			"time": "2016-11-30T22:10:28-05:00",
			"created": "2015-04-19T00:52:23-04:00"
		}
	]
}