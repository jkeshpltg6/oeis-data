{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A087687",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 87687,
			"data": "1,4,9,8,25,36,49,32,99,100,121,72,169,196,225,64,289,396,361,200,441,484,529,288,725,676,891,392,841,900,961,256,1089,1156,1225,792,1369,1444,1521,800,1681,1764,1849,968,2475,2116,2209,576,2695,2900,2601",
			"name": "Number of solutions to x^2 + y^2 + z^2 == 0 (mod n).",
			"comment": [
				"To show that a(n) is multiplicative is simple number theory. If gcd(n,m)=1, then any solution of x^2 + y^2 + z^2 == 0 (mod n) and any solution (mod m) can combined to a solution (mod nm) using the Chinese Remainder Theorem and any solution (mod nm) gives solutions (mod n) and (mod m). Hence a(nm) = a(n)*a(m). - _Torleiv Kløve_, Jan 26 2009"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A087687/b087687.txt\"\u003eTable of n, a(n) for n = 1..20000\u003c/a\u003e (first 80 terms from Robert Gerbicz)",
				"C. Calderón and M. J. De Velasco, \u003ca href=\"https://doi.org/10.1007/BF01377596\"\u003eOn divisors of a quadratic form\u003c/a\u003e, Boletim da Sociedade Brasileira de Matemática, Vol. 31, No. 1 (2000), pp. 81-91; \u003ca href=\"http://emis.icm.edu.pl/journals/em/docs/boletim/vol311/v31-1-a6-2000.pdf\"\u003ealternative link\u003c/a\u003e.",
				"László Toth, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Toth/toth12.html\"\u003eCounting Solutions of Quadratic Congruences in Several Variables Revisited\u003c/a\u003e, J. Int. Seq., Vol. 17 (2014), Article # 14.11.6; \u003ca href=\"http://arxiv.org/abs/1404.4214\"\u003earXiv preprint\u003c/a\u003e, arXiv:1404.4214 [math.NT], 2014.",
				"\u003ca href=\"/index/Su#sums_of_squares\"\u003eIndex to sequences related to sums of squares\u003c/a\u003e"
			],
			"formula": [
				"a(2^k) = 2^(k + ceiling(k/2)). For odd primes p, a(p^(2k-1)) = p^(3k-2)*(p^k + p^(k-1) - 1) and a(p^(2k)) = p^(3k-1)*(p^(k+1) + p^k - 1). - _Martin Fuller_, Jan 26 2009",
				"Sum_{k=1..n} a(k) ~ (4*zeta(3))/(15*zeta(4)) * n^3 + O(n^2 * log(n)) (Calderón and de Velasco, 2000). - _Amiram Eldar_, Mar 04 2021"
			],
			"maple": [
				"A087687 := proc(n)",
				"    a := 1;",
				"    for pe in ifactors(n)[2] do",
				"        p := op(1,pe) ;",
				"        e := op(2,pe) ;",
				"        if p = 2 then",
				"            a := a*p^(e+ceil(e/2)) ;",
				"        elif type(e,'odd') then",
				"            a := a*p^((3*e-1)/2)*(p^((e+1)/2)+p^((e-1)/2)-1) ;",
				"        else",
				"            a := a*p^(3*e/2-1)*(p^(e/2+1)+p^(e/2)-1) ;",
				"        end if;",
				"    end do:",
				"    a ;",
				"end proc:",
				"seq(A087687(n),n=1..100) ; # _R. J. Mathar_, Jun 25 2018"
			],
			"mathematica": [
				"a[n_] := Module[{k=1}, Do[{p, e} = pe; k = k*If[p == 2, p^(e + Ceiling[ e/2]), If[OddQ[e], p^((3*e-1)/2)*(p^((e+1)/2) + p^((e-1)/2) - 1), p^(3*e/2 - 1)*(p^(e/2 + 1) + p^(e/2) - 1)]], {pe, FactorInteger[n]}]; k];",
				"Array[a, 100] (* _Jean-François Alcover_, Jul 10 2018, after _R. J. Mathar_ *)"
			],
			"program": [
				"(PARI) a(n)=local(v=vector(n),w);for(i=1,n,v[i^2%n+1]++);w=vector(n,i,sum(j=1,n,v[j]*v[(i-j)%n+1]));sum(j=1,n,w[j]*v[(1-j)%n+1]) \\\\ _Martin Fuller_",
				"(PARI) a(n)={my(f=factor(n)); prod(i=1, #f~, my([p,e]=f[i,]); if(p==2, 2^(e+(e+1)\\2), p^(e+(e-1)\\2)*(p^(e\\2)*(p+1) - 1)))} \\\\ _Andrew Howroyd_, Aug 06 2018"
			],
			"xref": [
				"Cf. A086933, A062775.",
				"Different from A064549."
			],
			"keyword": "mult,look,nonn",
			"offset": "1,2",
			"author": "Yuval Dekel (dekelyuval(AT)hotmail.com), Sep 27 2003",
			"ext": [
				"More terms from _Robert Gerbicz_, Aug 22 2006",
				"Edited by _Steven Finch_, Feb 06 2009, Feb 12 2009"
			],
			"references": 5,
			"revision": 49,
			"time": "2021-03-04T07:03:51-05:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}