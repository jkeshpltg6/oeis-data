{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A009998",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 9998,
			"data": "1,1,1,1,2,1,1,4,3,1,1,8,9,4,1,1,16,27,16,5,1,1,32,81,64,25,6,1,1,64,243,256,125,36,7,1,1,128,729,1024,625,216,49,8,1,1,256,2187,4096,3125,1296,343,64,9,1,1,512,6561,16384,15625,7776,2401,512,81,10,1",
			"name": "Triangle in which j-th entry in i-th row is (j+1)^(i-j).",
			"comment": [
				"Read as a square array this is the Hilbert transform of triangle A123125 (see A145905 for the definition of this term). For example, the fourth row of A123125 is (0,1,4,1) and the expansion (x + 4*x^2 + x^3)/(1-x)^4 = x + 8*x^2 + 27*x^3 + 64*x^4 + ... generates the entries in the fourth row of this array read as a square. - _Peter Bala_, Oct 28 2008"
			],
			"reference": [
				"M. Abramowitz and I. A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, 1964 (and various reprintings), p. 24."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A009998/b009998.txt\"\u003eRows n=0..50 of triangle, flattened\u003c/a\u003e",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"Peter Luschny, \u003ca href=\"http://oeis.org/wiki/User:Peter_Luschny/FigurateNumber\"\u003eFigurate number - a very short introduction\u003c/a\u003e"
			],
			"formula": [
				"T(n,n) = 1; T(n,k) = (k+1)*T(n-1,k) for k=0..n-1. - _Reinhard Zumkeller_, Feb 02 2014",
				"T(n,m) = (m+1)*Sum_{k=0..n-m}((n+1)^(k-1)*(n-m)^(n-m-k)*(-1)^(n-m-k)*binomial(n-m-1,k-1)). - _Vladimir Kruchinin_, Sep 12 2015"
			],
			"example": [
				"Triangle begins:",
				"  1;",
				"  1,  1;",
				"  1,  2,  1;",
				"  1,  4,  3,  1;",
				"  1,  8,  9,  4,  1;",
				"  1, 16, 27, 16,  5,  1;",
				"  1, 32, 81, 64, 25,  6,  1;",
				"  ...",
				"From _Gus Wiseman_, May 01 2021: (Start)",
				"The rows of the triangle are obtained by reading antidiagonals upward in the following table of A(k,n) = n^k, with offset k = 0, n = 1:",
				"         n=1:     n=2:     n=3:     n=4:     n=5:     n=6:",
				"   k=0:   1        1        1        1        1        1",
				"   k=1:   1        2        3        4        5        6",
				"   k=2:   1        4        9       16       25       36",
				"   k=3:   1        8       27       64      125      216",
				"   k=4:   1       16       81      256      625     1296",
				"   k=5:   1       32      243     1024     3125     7776",
				"   k=6:   1       64      729     4096    15625    46656",
				"   k=7:   1      128     2187    16384    78125   279936",
				"   k=8:   1      256     6561    65536   390625  1679616",
				"   k=9:   1      512    19683   262144  1953125 10077696",
				"  k=10:   1     1024    59049  1048576  9765625 60466176",
				"(End)"
			],
			"maple": [
				"E := (n,x) -\u003e `if`(n=0,1,x*(1-x)*diff(E(n-1,x),x)+E(n-1,x)*(1+(n-1)*x));",
				"G := (n,x) -\u003e E(n,x)/(1-x)^(n+1);",
				"A009998 := (n,k) -\u003e coeff(series(G(n-k,x),x,18),x,k);",
				"seq(print(seq(A009998(n,k),k=0..n)),n=0..6);",
				"# _Peter Luschny_, Aug 02 2010"
			],
			"mathematica": [
				"Flatten[Table[(j+1)^(i-j),{i,0,20},{j,0,i}]] (* _Harvey P. Dale_, Dec 25 2012 *)"
			],
			"program": [
				"(Haskell)",
				"a009998 n k = (k + 1) ^ (n - k)",
				"a009998_row n = a009998_tabl !! n",
				"a009998_tabl = map reverse a009999_tabl",
				"-- _Reinhard Zumkeller_, Feb 02 2014",
				"(PARI) T(i,j)=(j+1)^(i-j) \\\\ _Charles R Greathouse IV_, Feb 06 2017"
			],
			"xref": [
				"Row sums give A026898.",
				"Cf. A088956, A123125, A179927.",
				"Column n = 2 of the array is A000079.",
				"Column n = 3 of the array is A000244.",
				"Row k = 2 of the array is A000290.",
				"Row k = 3 of the array is A000578.",
				"Diagonal n = k of the array is A000312.",
				"Diagonal n = k + 1 of the array is A000169.",
				"Diagonal n = k + 2 of the array is A000272.",
				"The transpose of the array is A009999.",
				"The numbers of divisors of the entries are A343656 (row sums: A343657).",
				"A007318 counts k-sets of elements of {1..n}.",
				"A059481 counts k-multisets of elements of {1..n}.",
				"Cf. A002109, A062319, A066959, A143773, A176029, A204688, A326358, A327527."
			],
			"keyword": "tabl,nonn,easy,nice",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"a(62) corrected to 512 by _T. D. Noe_, Dec 20 2007"
			],
			"references": 25,
			"revision": 41,
			"time": "2021-05-20T23:04:57-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}