{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A072447",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 72447,
			"data": "1,1,8,378,252000,17197930224",
			"name": "Number of subsets S of the power set P{1,2,...,n} such that: {1}, {2},..., {n} are all elements of S; {1,2,...n} is an element of S; if X and Y are elements of S and X and Y have a nonempty intersection, then the union of X and Y is an element of S.",
			"comment": [
				"If this sequence were defined similarly to A326877, we would have a(1) = 0. - _Gus Wiseman_, Aug 01 2019",
				"From _Gus Wiseman_, Aug 01 2019: (Start)",
				"We define a connectedness system to be a set of finite nonempty sets (edges) that is closed under taking the union of any two overlapping edges. It is connected if it is empty or contains an edge with all the vertices. a(n) is the number of connected connectedness systems on n vertices without singletons. For example, the a(3) = 8 connected connectedness systems without singletons are:",
				"  {{1,2,3}}",
				"  {{1,2},{1,2,3}}",
				"  {{1,3},{1,2,3}}",
				"  {{2,3},{1,2,3}}",
				"  {{1,2},{1,3},{1,2,3}}",
				"  {{1,2},{2,3},{1,2,3}}",
				"  {{1,3},{2,3},{1,2,3}}",
				"  {{1,2},{1,3},{2,3},{1,2,3}}",
				"(End)"
			],
			"link": [
				"Wim van Dam, \u003ca href=\"http://www.cs.berkeley.edu/~vandam/subpowersets/sequences.html\"\u003eSub Power Set Sequences\u003c/a\u003e",
				"Gus Wiseman, \u003ca href=\"http://www.mathematica-journal.com/2017/12/every-clutter-is-a-tree-of-blobs/\"\u003eEvery Clutter Is a Tree of Blobs\u003c/a\u003e, The Mathematica Journal, Vol. 19, 2017."
			],
			"formula": [
				"a(n \u003e 1) = A326868(n)/2^n. - _Gus Wiseman_, Aug 01 2019"
			],
			"example": [
				"a(3) = 8 because of the 8 sets: {{1}, {2}, {3}, {1, 2, 3}}; {{1}, {2}, {3}, {1, 2}, {1, 2, 3}}; {{1}, {2}, {3}, {1, 3}, {1, 2, 3}}; {{1}, {2}, {3}, {2, 3}, {1, 2, 3}}; {{1}, {2}, {3}, {1, 2}, {1, 3}, {1, 2, 3}}; {{1}, {2}, {3}, {1, 2}, {2, 3}, {1, 2, 3}}; {{1}, {2}, {3}, {1, 3}, {2, 3}, {1, 2, 3}}; {{1}, {2}, {3}, {1, 2}, {1, 3}, {2, 3}, {1, 2, 3}}."
			],
			"mathematica": [
				"Table[Length[Select[Subsets[Subsets[Range[n],{2,n}]],(n==0||MemberQ[#,Range[n]])\u0026\u0026SubsetQ[#,Union@@@Select[Tuples[#,2],Intersection@@#!={}\u0026]]\u0026]],{n,0,4}] (* returns a(1) = 0 similar to A326877. - _Gus Wiseman_, Aug 01 2019 *)"
			],
			"xref": [
				"The unlabeled case is A072445.",
				"The non-connected case is A072446.",
				"The case with singletons is A326868.",
				"The covering version is A326877.",
				"Cf. A072444, A326866, A326869, A326870, A326873, A326879."
			],
			"keyword": "nonn,more",
			"offset": "1,3",
			"author": "Wim van Dam (vandam(AT)cs.berkeley.edu), Jun 18 2002",
			"references": 14,
			"revision": 9,
			"time": "2019-08-02T00:01:16-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}