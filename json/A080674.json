{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A080674",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 80674,
			"data": "0,4,20,84,340,1364,5460,21844,87380,349524,1398100,5592404,22369620,89478484,357913940,1431655764,5726623060,22906492244,91625968980,366503875924,1466015503700,5864062014804,23456248059220,93824992236884,375299968947540,1501199875790164",
			"name": "a(n) = (4/3)*(4^n - 1).",
			"comment": [
				"a(n) is the number of steps which are made when generating all n-step random walks that begin in a given point P on a two-dimensional square lattice. To make one step means to move along one edge on the lattice. - Pawel P. Mazur (Pawel.Mazur(AT)pwr.wroc.pl), Mar 10 2005",
				"Conjectured to be the number of integers from 0 to (10^n)-1 that lack 0, 1, 2, 3, 4 and 5 as a digit. - _Alexandre Wajnberg_, Apr 25 2005",
				"Gives the values of m such that binomial(4*m + 4,m) is odd. Cf. A002450, A020988 and A263132. - _Peter Bala_, Oct 11 2015",
				"Also the partial sums of 4^n for n\u003e0, cf. A000302. - _Robert G. Wilson v_, Sep 18 2016"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A080674/b080674.txt\"\u003eTable of n, a(n) for n = 0..170\u003c/a\u003e",
				"P. Bala, \u003ca href=\"/A002450/a002450.txt\"\u003eA characterization of A002450, A020988 and A080674.\u003c/a\u003e",
				"Mattia Fregola, \u003ca href=\"https://docs.google.com/spreadsheets/d/1xApi5KNDGye7I2lxsdlbvng_r7On6G2AyPxUduD3ANw/edit?usp=sharing\"\u003eCellular Automata RULE13 generating OEIS sequence A080674\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (5,-4)."
			],
			"formula": [
				"a(n) = Sum_{i = 1..n} 4^i. - Adam McDougall (mcdougal(AT)stolaf.edu), Sep 29 2004",
				"a(n) = 4*a(n-1) + 4. - _Alexandre Wajnberg_, Apr 25 2005",
				"a(n) = 4^n + a(n-1) (with a(0) = 0). - _Vincenzo Librandi_, Aug 08 2010",
				"From _Colin Barker_, Oct 12 2015: (Start)",
				"a(n) = 5*a(n-1) - 4*a(n-2).",
				"G.f.: 4*x / ((x-1)*(4*x-1)).",
				"(End)"
			],
			"mathematica": [
				"Table[4*(4^n-1)/3,{n,0,100}]  (* _Vladimir Joseph Stephan Orlovsky_, Jan 30 2012 *)",
				"LinearRecurrence[{5,-4},{0,4},40] (* _Harvey P. Dale_, May 05 2018 *)"
			],
			"program": [
				"(MAGMA) [(4/3)*(4^n-1): n in [0..40] ]; // _Vincenzo Librandi_, Apr 28 2011",
				"(PARI) vector(100, n, n--; (4/3)*(4^n-1)) \\\\ _Altug Alkan_, Oct 11 2015",
				"(PARI) Vec(4*x/((x-1)*(4*x-1)) + O(x^40)) \\\\ _Colin Barker_, Oct 12 2015"
			],
			"xref": [
				"a(n) = 2 * A020988(n) = A002450(n+1) - 1 = 4 * A002450(n).",
				"Row n = 4 of A228275.",
				"Cf. A000301, A002450, A263132."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Mar 02 2003",
			"references": 16,
			"revision": 54,
			"time": "2018-09-03T04:10:19-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}