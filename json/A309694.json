{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A309694",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 309694,
			"data": "0,0,0,0,2,2,6,4,14,14,28,24,48,44,74,68,112,106,158,144,214,206,286,268,370,352,466,444,584,562,716,680,864,838,1038,996,1230,1188,1440,1392,1682,1634,1944,1876,2228,2174,2548,2472,2892,2816,3260,3176,3670",
			"name": "Sum of the even parts appearing among the largest parts of the partitions of n into 3 parts.",
			"link": [
				"Colin Barker, \u003ca href=\"/A309694/b309694.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Par#part\"\u003eIndex entries for sequences related to partitions\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_19\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,-1,1,1,-1,3,-3,2,-2,-2,2,-3,3,-1,1,1,-1,1,-1)."
			],
			"formula": [
				"a(n) = Sum_{j=1..floor(n/3)} Sum_{i=j..floor((n-j)/2)} (n-i-j) * ((n-i-j-1) mod 2).",
				"From _Colin Barker_, Aug 23 2019: (Start)",
				"G.f.: 2*x^4*(1 + 3*x^2 - x^3 + 6*x^4 - x^5 + 7*x^6 - x^7 + 6*x^8 - x^9 + 3*x^10 - x^11 + x^12) / ((1 - x)^4*(1 + x)^3*(1 - x + x^2)^2*(1 + x^2)^2*(1 + x + x^2)^2).",
				"a(n) = a(n-1) - a(n-2) + a(n-3) + a(n-4) - a(n-5) + 3*a(n-6) - 3*a(n-7) + 2*a(n-8) - 2*a(n-9) - 2*a(n-10) + 2*a(n-11) - 3*a(n-12) + 3*a(n-13) - a(n-14) + a(n-15) + a(n-16) - a(n-17) + a(n-18) - a(n-19) for n\u003e18.",
				"(End)"
			],
			"example": [
				"Figure 1: The partitions of n into 3 parts for n = 3, 4, ...",
				"                                                          1+1+8",
				"                                                   1+1+7  1+2+7",
				"                                                   1+2+6  1+3+6",
				"                                            1+1+6  1+3+5  1+4+5",
				"                                     1+1+5  1+2+5  1+4+4  2+2+6",
				"                              1+1+4  1+2+4  1+3+4  2+2+5  2+3+5",
				"                       1+1+3  1+2+3  1+3+3  2+2+4  2+3+4  2+4+4",
				"         1+1+1  1+1+2  1+2+2  2+2+2  2+2+3  2+3+3  3+3+3  3+3+4    ...",
				"-----------------------------------------------------------------------",
				"  n  |     3      4      5      6      7      8      9     10      ...",
				"-----------------------------------------------------------------------",
				"a(n) |     0      2      2      6      4     14     14     28      ...",
				"-----------------------------------------------------------------------"
			],
			"mathematica": [
				"Table[Sum[Sum[(n - i - j) * Mod[n - i - j - 1, 2], {i, j, Floor[(n - j)/2]}], {j, Floor[n/3]}], {n, 0, 80}]",
				"LinearRecurrence[{1, -1, 1, 1, -1, 3, -3, 2, -2, -2, 2, -3, 3, -1, 1, 1, -1, 1, -1}, {0, 0, 0, 0, 2, 2, 6, 4, 14, 14, 28, 24, 48, 44, 74, 68, 112, 106, 158}, 80]"
			],
			"program": [
				"(PARI) concat([0,0,0,0], Vec(2*x^4*(1 + 3*x^2 - x^3 + 6*x^4 - x^5 + 7*x^6 - x^7 + 6*x^8 - x^9 + 3*x^10 - x^11 + x^12) / ((1 - x)^4*(1 + x)^3*(1 - x + x^2)^2*(1 + x^2)^2*(1 + x + x^2)^2) + O(x^40))) \\\\ _Colin Barker_, Aug 23 2019"
			],
			"xref": [
				"Cf. A026923, A026927, A309683, A309684, A309685, A309686, A309687, A309688, A309689, A309690, A309692."
			],
			"keyword": "nonn,easy",
			"offset": "0,5",
			"author": "_Wesley Ivan Hurt_, Aug 12 2019",
			"references": 12,
			"revision": 14,
			"time": "2019-09-01T11:25:19-04:00",
			"created": "2019-08-12T22:35:21-04:00"
		}
	]
}