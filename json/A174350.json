{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A174350",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 174350,
			"data": "3,5,7,11,13,23,17,19,31,89,29,37,47,359,139,41,43,53,389,181,199,59,67,61,401,241,211,113,71,79,73,449,283,467,293,1831,101,97,83,479,337,509,317,1933,523,107,103,131,491,409,619,773,2113,1069,887",
			"name": "Square array: row n \u003e= 1 lists the primes p for which the next prime is p+2n; read by antidiagonals.",
			"comment": [
				"Every odd prime p = prime(i), i \u003e 1, occurs in this array, in row (prime(i+1) - prime(i))/2. Polignac's conjecture states that each row contains an infinite number of indices. In case this does not hold, we can use the convention to continue finite rows with 0's, to ensure the sequence is well defined. - _M. F. Hasler_, Oct 19 2018"
			],
			"link": [
				"Robert G. Wilson v, \u003ca href=\"/A174350/b174350.txt\"\u003eFalling antidiagonals 1..115, flattened\u003c/a\u003e First 50 from T. D. Noe",
				"Fred B. Holt and Helgi Rudd, \u003ca href=\"http://arxiv.org/abs/1402.1970\"\u003eOn Polignac's Conjecture\u003c/a\u003e, arxiv:1402.1970 [math.NT], 2014."
			],
			"formula": [
				"a(n) = A000040(A174349(n)). - _Michel Marcus_, Mar 30 2016"
			],
			"example": [
				"Upper left hand corner of the array:",
				"     3     5    11    17    29    41    59    71   101 ...",
				"     7    13    19    37    43    67    79    97   103 ...",
				"    23    31    47    53    61    73    83   131   151 ...",
				"    89   359   389   401   449   479   491   683   701 ...",
				"   139   181   241   283   337   409   421   547   577 ...",
				"   199   211   467   509   619   661   797   997  1201 ...",
				"   113   293   317   773   839   863   953  1409  1583 ...",
				"  1831  1933  2113  2221  2251  2593  2803  3121  3373 ...",
				"   523  1069  1259  1381  1759  1913  2161  2503  2861 ...",
				"  (...)",
				"Row 1: p(2) = 3, p(3) = 5, p(5) = 11, p(7) = 17,... these being the primes for which the next prime is 2 greater: (lesser of) twin primes A001359.",
				"Row 2: p(4) = 7, p(6) = 13, p(8) = 19,... these being the primes for which the next prime is 4 greater: (lesser of) cousin primes A029710."
			],
			"mathematica": [
				"rows = 10; t2 = {}; Do[t = {}; p = Prime[2]; While[Length[t] \u003c rows - off + 1, nextP = NextPrime[p]; If[nextP - p == 2*off, AppendTo[t, p]]; p = nextP]; AppendTo[t2, t], {off, rows}]; Table[t2[[b, a - b + 1]], {a, rows}, {b, a}] (* _T. D. Noe_, Feb 11 2014 *)",
				"t[r_, 0] = 2; t[r_, c_] := Block[{p = NextPrime@ t[r, c - 1], q}, q = NextPrime@ p; While[ p + 2r != q, p = q; q = NextPrime@ q]; p]; Table[ t[r - c + 1, c], {r, 10}, {c, r, 1, -1}] (* _Robert G. Wilson v_, Nov 06 2020 *)"
			],
			"program": [
				"(PARI) A174350_row(g, N=50, i=0, p=prime(i+1), L=[])={g*=2; forprime(q=1+p, , i++; if(p+g==p=q, L=concat(L, q-g); N--||return(L)))} \\\\ Returns the first N terms of row g. - _M. F. Hasler_, Oct 19 2018"
			],
			"xref": [
				"Cf. A000040, A001223, A174349.",
				"Rows 1, 2, 3, ...: A001359, A029710, A031924, A031926, A031928 (row 5), A031930, A031932, A031934, A031936, A031938 (row 10), A061779, A098974, A124594, A124595, A124596 (row 15), A126784, A134116, A134117, A134118, A126721 (row 20), A134120, A134121, A134122, A134123, A134124 (row 25), A204665, A204666, A204667, A204668, A126771 (row 30), A204669, A204670.",
				"Rows 35, 40, 45, 50, ...: A204792, A126722, A204764, A050434 (row 50), A204801, A204672, A204802, A204803, A126724 (row 75), A184984, A204805, A204673, A204806, A204807 (row 100); A224472 (row 150).",
				"Column 1: A000230.",
				"Column 2: A046789."
			],
			"keyword": "nonn,tabl",
			"offset": "1,1",
			"author": "_Clark Kimberling_, Mar 16 2010",
			"ext": [
				"Definition corrected and other edits by _M. F. Hasler_, Oct 19 2018"
			],
			"references": 2,
			"revision": 37,
			"time": "2020-11-07T22:18:36-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}