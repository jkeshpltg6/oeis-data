{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A327884",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 327884,
			"data": "1,1,1,2,1,1,5,4,3,1,15,11,9,4,1,52,41,35,20,5,1,203,162,150,90,30,6,1,877,715,672,455,175,42,7,1,4140,3425,3269,2352,1015,280,56,8,1,21147,17722,17271,13132,6237,1890,420,72,9,1,115975,98253,97155,76540,39480,12978,3150,600,90,10,1",
			"name": "Number T(n,k) of set partitions of [n] such that at least one of the block sizes is k or k=0; triangle T(n,k), n\u003e=0, 0\u003c=k\u003c=n, read by rows.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A327884/b327884.txt\"\u003eRows n = 0..140, flattened\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Iverson_bracket\"\u003eIverson bracket\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Partition_of_a_set\"\u003ePartition of a set\u003c/a\u003e"
			],
			"formula": [
				"E.g.f. of column k: exp(exp(x)-1) - [k\u003e0] * exp(exp(x)-1-x^k/k!).",
				"T(n,0) - T(n,1) = A000296(n)."
			],
			"example": [
				"T(4,1) = 11: 123|4, 124|3, 12|3|4, 134|2, 13|2|4, 1|234, 1|23|4, 14|2|3, 1|24|3, 1|2|34, 1|2|3|4.",
				"T(4,2) = 9: 12|34, 12|3|4, 13|24, 13|2|4, 14|23, 1|23|4, 14|2|3, 1|24|3, 1|2|34.",
				"T(4,3) = 4: 123|4, 124|3, 134|2, 1|234.",
				"T(4,4) = 1: 1234.",
				"T(5,1) = 41: 1234|5, 1235|4, 123|4|5, 1245|3, 124|3|5, 12|34|5, 125|3|4, 12|35|4, 12|3|45, 12|3|4|5, 1345|2, 134|2|5, 13|24|5, 135|2|4, 13|25|4, 13|2|45, 13|2|4|5, 14|23|5, 1|2345, 1|234|5, 15|23|4, 1|235|4, 1|23|45, 1|23|4|5, 145|2|3, 14|25|3, 14|2|35, 14|2|3|5, 15|24|3, 1|245|3, 1|24|35, 1|24|3|5, 15|2|34, 1|25|34, 1|2|345, 1|2|34|5, 15|2|3|4, 1|25|3|4, 1|2|35|4, 1|2|3|45, 1|2|3|4|5.",
				"Triangle T(n,k) begins:",
				"      1;",
				"      1,     1;",
				"      2,     1,     1;",
				"      5,     4,     3,     1;",
				"     15,    11,     9,     4,    1;",
				"     52,    41,    35,    20,    5,    1;",
				"    203,   162,   150,    90,   30,    6,   1;",
				"    877,   715,   672,   455,  175,   42,   7,  1;",
				"   4140,  3425,  3269,  2352, 1015,  280,  56,  8, 1;",
				"  21147, 17722, 17271, 13132, 6237, 1890, 420, 72, 9, 1;",
				"  ..."
			],
			"maple": [
				"b:= proc(n, k) option remember; `if`(n=0, 1, add(",
				"      `if`(j=k, 0, b(n-j, k)*binomial(n-1, j-1)), j=1..n))",
				"    end:",
				"T:= (n, k)-\u003e b(n, 0)-`if`(k=0, 0, b(n, k)):",
				"seq(seq(T(n, k), k=0..n), n=0..11);"
			],
			"mathematica": [
				"b[n_, k_] := b[n, k] = If[n == 0, 1, Sum[If[j == k, 0, b[n - j,k] Binomial[ n - 1, j - 1]], {j, 1, n}]];",
				"T[n_, k_] := b[n, 0] - If[k == 0, 0, b[n, k]];",
				"Table[T[n, k], {n, 0, 11}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Apr 30 2020, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Columns k=0-3 give: A000110, A000296(n+1), A327885, A328153.",
				"T(2n,n) gives A276961.",
				"Cf. A080510, A327869."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_Alois P. Heinz_, Sep 28 2019",
			"references": 6,
			"revision": 26,
			"time": "2020-04-30T11:33:10-04:00",
			"created": "2019-09-28T21:36:21-04:00"
		}
	]
}