{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A050985",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 50985,
			"data": "1,2,3,4,5,6,7,1,9,10,11,12,13,14,15,2,17,18,19,20,21,22,23,3,25,26,1,28,29,30,31,4,33,34,35,36,37,38,39,5,41,42,43,44,45,46,47,6,49,50,51,52,53,2,55,7,57,58,59,60,61,62,63,1,65,66,67,68,69,70,71,9,73,74,75",
			"name": "Cubefree part of n.",
			"comment": [
				"This is an unusual sequence in the sense that the 83.2% of the integers that belong to A004709 occur infinitely many times, whereas the remaining 16.8% of the integers that belong to A046099 never occur at all. - _Ant King_, Sep 22 2013"
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A050985/b050985.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (terms 1..1000 from Harvey P. Dale)",
				"Henry Bottomley, \u003ca href=\"http://fs.gallup.unm.edu/Bottomley-Sm-Mult-Functions.htm\"\u003eSome Smarandache-type multiplicative sequences\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CubefreePart.html\"\u003eCubefree Part\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/DirichletGeneratingFunction.html\"\u003eDirichlet Generating Function\u003c/a\u003e."
			],
			"formula": [
				"Multiplicative with p^e -\u003e p^(e mod 3), p prime. - _Reinhard Zumkeller_, Nov 22 2009",
				"Dirichlet g.f.: zeta(3s)*zeta(s-1)/zeta(3s-3). - _R. J. Mathar_, Feb 11 2011",
				"a(n) = n/A008834(n). - _R. J. Mathar_, Dec 08 2015",
				"Sum_{k=1..n} a(k) ~ Pi^6 * n^2 / (1890*Zeta(3)). - _Vaclav Kotesovec_, Feb 08 2019"
			],
			"maple": [
				"A050985 := proc(n)",
				"    n/A008834(n) ;",
				"end proc:",
				"seq(A050985(n),n=1..40) ; # _R. J. Mathar_, Dec 08 2015"
			],
			"mathematica": [
				"cf[n_]:=Module[{tr=Transpose[FactorInteger[n]],ex,cb},ex= tr[[2]]- Mod[ tr[[2]],3];cb=Times@@(First[#]^Last[#]\u0026/@Transpose[{tr[[1]], ex}]);n/cb]; Array[cf,75] (* _Harvey P. Dale_, Jun 03 2012 *)",
				"f[p_, e_] := p^Mod[e, 3]; a[n_] := Times @@ (f @@@ FactorInteger[n]); Array[a, 100] (* _Amiram Eldar_, Sep 07 2020 *)"
			],
			"program": [
				"(Python)",
				"from operator import mul",
				"from functools import reduce",
				"from sympy import factorint",
				"def A050985(n):",
				"    return 1 if n \u003c=1 else reduce(mul,[p**(e % 3) for p,e in factorint(n).items()])",
				"# _Chai Wah Wu_, Feb 04 2015",
				"(PARI) a(n) = my(f=factor(n)); f[,2] = apply(x-\u003e(x % 3), f[,2]); factorback(f); \\\\ _Michel Marcus_, Jan 06 2019"
			],
			"xref": [
				"Cf. A007913, A008834, A053165, A004709, A046099, A301596, A301597."
			],
			"keyword": "nonn,easy,mult",
			"offset": "1,2",
			"author": "_Eric W. Weisstein_, Dec 11 1999",
			"references": 17,
			"revision": 61,
			"time": "2020-09-07T06:28:56-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}