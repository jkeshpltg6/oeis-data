{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A301316",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 301316,
			"data": "0,2,3,7,0,13,35,49,64,81,11,1,0,57,1,1,85,1,38,1,1,133,184,1,1,521,1,1,522,1,589,1,1,885,1,1,259,381,1,1,656,1,559,1,1,553,282,1,1,1,1,1,1802,1,1,1,1,2553,1593,1,3416,993,1,1,1,1,804",
			"name": "a(n) = ((n-1)! + 1) mod n^2.",
			"comment": [
				"By definition, when n \u003e 1, a(n) = 0 then n is a Wilson prime (A007540).",
				"For a(n) to equal 1, (n-1)! must be divisible by n^2 which is the prevailing case for large n. For example, all n which are a product of more than two distinct primes belong to this category. So do all proper powers of primes except 2^2, 2^3, and 3^2. Obviously, when a(n) = 1, then also A055976(n) = 1.",
				"The cases of a(n) \u003e 1 include, for example, all primes other than Wilson's and all numbers of the form n=2*p, where p is a prime."
			],
			"link": [
				"Stanislav Sykora, \u003ca href=\"/A301316/b301316.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Wilson_prime\"\u003eWilson prime\u003c/a\u003e"
			],
			"formula": [
				"a(n) = ((n-1)! + 1) mod n^2. - _Jon E. Schoenfield_, Mar 18 2018",
				"a(n) = A038507(n-1) mod A000290(n). - _Michel Marcus_, Mar 20 2018"
			],
			"example": [
				"From _Muniru A Asiru_, Mar 20 2018: (Start)",
				"((1-1)! + 1) mod 1^2 = (0! +1) mod 1 = 2 mod 1 = 0.",
				"((2-1)! + 1) mod 2^2 = (1! +1) mod 4 = 2 mod 4 = 2.",
				"((3-1)! + 1) mod 3^2 = (2! +1) mod 9 = 3 mod 9 = 3.",
				"((4-1)! + 1) mod 4^2 = (3! +1) mod 16 = 7 mod 16 = 7.",
				"((5-1)! + 1) mod 5^2 = (4! +1) mod 25 = 25 mod 25 = 0.",
				"... (End)"
			],
			"maple": [
				"seq((factorial(n-1)+1) mod n^2,n=1..60); # _Muniru A Asiru_, Mar 20 2018"
			],
			"mathematica": [
				"Array[Mod[(# - 1)! + 1, #^2] \u0026, 67] (* _Michael De Vlieger_, Apr 21 2018 *)"
			],
			"program": [
				"(PARI) a(n) = ((n-1)! + 1) % n^2; \\\\ _Michel Marcus_, Mar 18 2018",
				"(GAP) List([1..60],n-\u003e(Factorial(n-1)+1) mod n^2); # _Muniru A Asiru_, Mar 20 2018"
			],
			"xref": [
				"Cf. A000290, A038507, A007540, A055976, A301317."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Stanislav Sykora_, Mar 18 2018",
			"references": 2,
			"revision": 26,
			"time": "2018-06-14T17:53:52-04:00",
			"created": "2018-06-14T17:53:52-04:00"
		}
	]
}