{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A212169",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 212169,
			"data": "1,2,4,12,24,36,48,120,240,360,720,1680,5040,10080,15120,20160,25200,45360,50400,110880,221760,332640,554400,665280,2882880,8648640,14414400,17297280,43243200,294053760",
			"name": "List of highly composite numbers (A002182) with an exponent in its prime factorization that is at least as great as the number of positive exponents; intersection of A002182 and A212165.",
			"comment": [
				"Sequence can be used to find the largest highly composite number in subsequences of A212165 (of which there are several in the database).",
				"Ramanujan showed that, in the canonical prime factorization of a highly composite number with largest prime factor p(n), the largest exponent cannot exceed 2[log p(n+1)/log 2]. (See formula 54 on page 15 of the Ramanujan paper.) This limit is less than n for all n \u003e= 9 (and p(n) \u003e= 23).",
				"1. Direct calculation verifies this for 9 \u003c= n \u003c= 11.",
				"2. Nagura proved that, for any integer m \u003e= 25, there is always a prime between m and 1.2*m.  Let n = 11, at which point p(11) = 31 and log p(n+1)/log 2 = log 37/log 2 = 5.209453.... Since log 1.2/log 2 is only .263034..., it follows that n must increase by at least 3k before 2[log p(n+1)/log 2] can increase by 2k, for all values of k. Therefore, 2[log p(n+1)/log 2] can never catch up to p(n) for n \u003e 11.",
				"665280 = 2^6*3^3*5*7*11 is the largest highly composite number whose prime factorization contains an exponent that is strictly greater than the number of positive exponents in that factorization (including the implied 1s)."
			],
			"reference": [
				"S. Ramanujan, Highly composite numbers, Proc. Lond. Math. Soc. 14 (1915), 347-409; reprinted in Collected Papers, Ed. G. H. Hardy et al., Cambridge 1927; Chelsea, NY, 1962."
			],
			"link": [
				"A. Flammenkamp, \u003ca href=\"http://wwwhomes.uni-bielefeld.de/achim/highly.txt\"\u003eList of the first 1200 highly composite numbers\u003c/a\u003e",
				"J. Nagura, \u003ca href=\"http://projecteuclid.org/euclid.pja/1195570997\"\u003eOn the interval containing at least one prime number\u003c/a\u003e, Proc. Japan Acad., 28 (1952), 177-181.",
				"S. Ramanujan, \u003ca href=\"http://www.imsc.res.in/~rao/ramanujan/CamUnivCpapers/Cpaper15/page15.htm\"\u003eHighly Composite Numbers (p. 15)\u003c/a\u003e (pages 11-12 introduce some of the notation in formula 54)"
			],
			"example": [
				"A002182(62)= 294053760 = 2^7*3^3*5*7*11*13*17 has 7 positive exponents in its prime factorization, including 5 implied 1s. The maximal exponent in its prime factorization is also 7. Therefore, 294053760 qualifies for this sequence."
			],
			"mathematica": [
				"okQ[n_] := Module[{f = Transpose[FactorInteger[n]][[2]]}, Max[f] \u003e= Length[f]]; a = 0; t = {}; Do[b = DivisorSigma[0, n]; If[b \u003e a, a = b; If[okQ[n], AppendTo[t, n]]], {n, 10^6}]; t (* _T. D. Noe_, May 24 2012 *)"
			],
			"xref": [
				"A212165 also includes all terms in A006939, A066120, A087980, A130091, A138534, A141586, A166475, A181555, A181813-A181814, A181818, A181823-A181825, A182763.",
				"Other finite subsequences of A002182 include A072938, A095921, A106037, A136253, A162935, A166981, A168263."
			],
			"keyword": "nonn,fini,full",
			"offset": "1,2",
			"author": "_Matthew Vandermast_, May 22 2012",
			"references": 1,
			"revision": 8,
			"time": "2012-10-10T10:47:49-04:00",
			"created": "2012-05-24T11:34:02-04:00"
		}
	]
}