{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A210705",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 210705,
			"data": "1,0,0,1,0,0,1,1,0,1,1,0,1,1,1,2,1,1,2,1,1,4,2,1,4,2,1,4,4,2,5,4,2,6,4,5,8,5,5,9,6,5,13,8,6,14,9,7,15,14,10,18,15,11,21,16,17,26,19,18,29,22,19,37,28,23,41,31,27,45,41,34,53,45,38,61",
			"name": "Number of partitions of n into parts congruent to +-3, +-7, +-15, +-21, +-33, +-35, +-39, +-49, +-51, +-57 (mod 126).",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A210705/b210705.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of chi(-x^9) * chi(-x^21) / (chi(-x^3) * chi(-x^7)) in powers of x where chi() is a Ramanujan theta function.",
				"Expansion of (G(x^126) * H(x) - x^25 * G(x) * H(x^126)) / (G(x^63) * G(x^2) + x^13 * H(x^63) * H(x^2)) where G(x) and H(x) respectively are the g.f. of A003114 and A003106.",
				"Expansion of q^(5/6) * eta(q^6) * eta(q^9) * eta(q^14) * eta(q^21) / ( eta(q^3) * eta(q^7) * eta(q^18) * eta(q^42) ) in powers of q.",
				"Euler transform of a period 126 sequence.",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (4536 t)) = f(t) where q = exp(2 Pi i t)."
			],
			"example": [
				"1 + x^3 + x^6 + x^7 + x^9 + x^10 + x^12 + x^13 + x^14 + 2*x^15 + x^16 + ...",
				"q^-5 + q^13 + q^31 + q^37 + q^49 + q^55 + q^67 + q^73 + q^79 + 2*q^85 + ..."
			],
			"mathematica": [
				"eta[q_]:= q^(1/24)*QPochhammer[q]; a[n_]:= SeriesCoefficient[q^(5/6)* eta[q^6]*eta[q^9]*eta[q^14]*eta[q^21]/(eta[q^3]*eta[q^7]*eta[q^18] *eta[q^42]), {q, 0, n}]; Table[a[n], {n, 0, 50}] (* _G. C. Greubel_, Apr 18 2018 *)"
			],
			"program": [
				"(PARI) {a(n) = local(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^6 + A) * eta(x^9 + A) * eta(x^14 + A) * eta(x^21 + A) / ( eta(x^3 + A) * eta(x^7 + A) * eta(x^18 + A) * eta(x^42 + A) ), n))}"
			],
			"xref": [
				"Cf. A003106, A003114."
			],
			"keyword": "nonn",
			"offset": "0,16",
			"author": "_Michael Somos_, Aug 31 2013",
			"ext": [
				"Name changed by _David A. Corneth_, Apr 18 2018"
			],
			"references": 1,
			"revision": 26,
			"time": "2021-03-12T22:24:46-05:00",
			"created": "2013-08-31T04:48:33-04:00"
		}
	]
}