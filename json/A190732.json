{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A190732",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 190732,
			"data": "1,1,2,8,3,7,9,1,6,7,0,9,5,5,1,2,5,7,3,8,9,6,1,5,8,9,0,3,1,2,1,5,4,5,1,7,1,6,8,8,1,0,1,2,5,8,6,5,7,9,9,7,7,1,3,6,8,8,1,7,1,4,4,3,4,2,1,2,8,4,9,3,6,8,8,2",
			"name": "Decimal expansion of 2/sqrt(Pi).",
			"comment": [
				"According to Weisstein, some mathematicians define erf(z) without reference to this constant.",
				"Also equals the average absolute value of the difference of two independent normally distributed random numbers with mean 0 and variance 1. - _Jean-François Alcover_, Oct 31 2014",
				"Limit_{n-\u003einfinity} (2^(1 - 2 n^2) n binomial(2 n^2, n^2) is proper to compute this constant (and also Pi) in a base of power 2. - _Ralf Steiner_, Apr 23 2017",
				"A gauge point marked \"c\" on slide rule calculating devices in the 20th century. The Pickworth reference notes its use \"in calculating the contents of cylinders\". - _Peter Munn_, Aug 14 2020"
			],
			"reference": [
				"Chi Keung Cheung et al., Getting Started with Mathematica, 2nd Ed. New York: J. Wiley (2005) p. 79.",
				"C. N. Pickworth, The Slide Rule, 24th Ed., Pitman, London (1945), p 53, Gauge Points."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A190732/b190732.txt\"\u003eTable of n, a(n) for n = 1..5000\u003c/a\u003e",
				"S. R. Finch, \u003ca href=\"http://arxiv.org/abs/1111.4976\"\u003eMean width of a regular simplex\u003c/a\u003e, arxiv:1111.4976 [math.MG], 2016, mu_2.",
				"International Slide Rule Museum, \u003ca href=\"https://www.sliderulemuseum.com/SR_Terms.htm#C\"\u003eSlide Rule Terms, Glossary and Encyclopedia\u003c/a\u003e, entry for \"C\".",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Erf.html\"\u003eErf\u003c/a\u003e"
			],
			"formula": [
				"Equals Sum_{n\u003e=0} (-1)^n*Gamma((n+1)/2)/Gamma(n/2+1). - _Jean-François Alcover_, Jun 12 2013",
				"Equals 1/A019704. - _Michel Marcus_, Jan 09 2017",
				"Equals Limit_{n-\u003einfinity} A285388(n)/A285389(n). - _Ralf Steiner_, Apr 22 2017"
			],
			"example": [
				"1.12837916709551257..."
			],
			"mathematica": [
				"RealDigits[2/Sqrt[Pi], 10, 100][[1]]",
				"RealDigits[Limit[2^(1 - 2 m^2) m Binomial[2 m^2, m^2], m -\u003e Infinity], 10, 100][[1]] (* _Ralf Steiner_, Apr 22 2017 *)"
			],
			"program": [
				"(PARI) 2/sqrt(Pi) \\\\ _G. C. Greubel_, Jan 09 2017"
			],
			"xref": [
				"Cf. A002161, A019704, A285388, A285389."
			],
			"keyword": "nonn,cons",
			"offset": "1,3",
			"author": "_Alonso del Arte_, May 17 2011",
			"references": 8,
			"revision": 59,
			"time": "2020-08-15T02:02:37-04:00",
			"created": "2011-05-17T22:34:40-04:00"
		}
	]
}