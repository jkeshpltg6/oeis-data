{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A195459",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 195459,
			"data": "1,1,3,2,4,3,6,4,9,4,10,6,12,6,12,8,16,9,18,8,18,10,22,12,20,12,27,12,28,12,30,16,30,16,24,18,36,18,36,16,40,18,42,20,36,22,46,24,42,20,48,24,52,27,40,24,54,28,58,24,60,30,54,32,48,30,66,32,66,24,70,36,72,36,60,36,60,36,78,32,81,40,82",
			"name": "a(n) = phi(3*n)/2.",
			"comment": [
				"Compare the o.g.f. of this sequence to the following identity:",
				"  Sum_{n\u003e=1} -moebius(3*n)*x^n/(1-x^n) = Sum_{n\u003e=0} x^(3^n).",
				"Here phi(n) = A000010(n), the Euler totient function of n.",
				"a(n) = b(n)*c(n) where b(n) = 1, 1, 3, 2, 1,.. is a multiplicative function with b(p^e) = p^(e-1) for p\u003c\u003e3 and p(3^e)=3^e, and where c(n) = 1, 1, 1, 1, 4, 1, 6, 1, 1... is a multiplicative function with c(p^e)=p-1 for p \u003c\u003e 3 and c(3^e)=1. - _R. J. Mathar_, Jul 02 2013"
			],
			"link": [
				"Paul D. Hanna, \u003ca href=\"/A195459/b195459.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"M. Picquet, \u003ca href=\"http://gallica.bnf.fr/ark:/12148/bpt6k433703n/f35.image\"\u003e Applications de la représentation des courbes du troisième degré\u003c/a\u003e, Journal de l'École Polytechnique, Paris, 35 (1884), pp. 31-100."
			],
			"formula": [
				"O.g.f.: Sum_{n\u003e=1} -moebius(3*n)*x^n/(1-x^n)^2 = Sum_{n\u003e=1} phi(3*n)/2*x^n.",
				"a(n) = n*Prod_{p | n, p prime, p != 3} (1 - 1/p). [Picquet, p. 73.]",
				"a(n) = phi(n)/2*(((2*n^2+1) mod 3)+2). - _Gary Detlefs_, Dec 06 2021"
			],
			"example": [
				"G.f.: A(x) = x + x^2 + 3*x^3 + 2*x^4 + 4*x^5 + 3*x^6 + 6*x^7 + 4*x^8 +...",
				"where A(x) = x/(1-x)^2 - x^2/(1-x^2)^2 + 0*x^3/(1-x^3)^2 + 0*x^4/(1-x^4)^2 - x^5/(1-x^5)^2 + 0*x^6/(1-x^6)^2 - x^7/(1-x^7)^2 + 0*x^8/(1-x^8)^2 + 0*x^9/(1-x^9)^2 + x^10/(1-x^10)^2 - x^11/(1-x^11)^2 +...+ -moebius(3*n)*x^n/(1-x^n)^2 +..."
			],
			"program": [
				"(PARI) {a(n)=polcoeff(sum(m=1, n, -moebius(3*m)*x^m/(1-x^m+x*O(x^n))^2), n)}",
				"(PARI) a(n) = eulerphi(3*n)/2; \\\\ _Michel Marcus_, Jun 07 2020"
			],
			"xref": [
				"Cf. A000010 (phi), A023022 (phi/2), A062570."
			],
			"keyword": "nonn,mult",
			"offset": "1,3",
			"author": "_Paul D. Hanna_, Sep 18 2011",
			"ext": [
				"Picquet formula and reference added by _N. J. A. Sloane_, Nov 23 2011"
			],
			"references": 3,
			"revision": 39,
			"time": "2021-12-06T12:56:31-05:00",
			"created": "2011-09-18T23:02:37-04:00"
		}
	]
}