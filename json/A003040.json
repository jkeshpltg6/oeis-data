{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003040",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3040,
			"id": "M0811",
			"data": "1,1,2,3,6,16,35,90,216,768,2310,7700,21450,69498,292864,1153152,4873050,16336320,64664600,249420600,1118939184,5462865408,28542158568,117487079424,547591590000,2474843571200,12760912164000,57424104738000,295284192952320",
			"name": "Highest degree of an irreducible representation of symmetric group S_n of degree n.",
			"comment": [
				"Highest number of standard tableaux of the Ferrers diagrams of the partitions of n. Example: a(4) = 3 because to the partitions 4, 31, 22, 211, and 1111 there correspond 1, 3, 2, 3, and 1 standard tableaux, respectively. - _Emeric Deutsch_, Oct 02 2015"
			],
			"reference": [
				"J. H. Conway, R. T. Curtis, S. P. Norton, R. A. Parker and R. A. Wilson, ATLAS of Finite Groups. Oxford Univ. Press, 1985.",
				"D. E. Littlewood, The Theory of Group Characters and Matrix Representations of Groups. 2nd ed., Oxford University Press, 1950, p. 265.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Eric M. Schmidt, \u003ca href=\"/A003040/b003040.txt\"\u003eTable of n, a(n) for n = 1..80\u003c/a\u003e",
				"S. Comét, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1960-0119451-0\"\u003eImproved methods to calculate the characters of the symmetric group\u003c/a\u003e, Math. Comp. 14 (1960) 104-117.",
				"J. McKay, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1976-0404414-X\"\u003eThe largest degrees of irreducible characters of the symmetric group\u003c/a\u003e. Math. Comp. 30 (1976), no. 135, 624-631. (Gives first 75 terms.)",
				"J. McKay, \u003ca href=\"/A003040/a003040a.jpg\"\u003ePage 1 of 5 pages of tables from Math. Comp. paper\u003c/a\u003e [reports 29th term incorrectly]",
				"J. McKay, \u003ca href=\"/A003040/a003040b.jpg\"\u003ePage 2 of 5 pages of tables from Math. Comp. paper\u003c/a\u003e",
				"J. McKay, \u003ca href=\"/A003040/a003040c.jpg\"\u003ePage 3 of 5 pages of tables from Math. Comp. paper\u003c/a\u003e",
				"J. McKay, \u003ca href=\"/A003040/a003040d.jpg\"\u003ePage 4 of 5 pages of tables from Math. Comp. paper\u003c/a\u003e",
				"J. McKay, \u003ca href=\"/A003040/a003040e.jpg\"\u003ePage 5 of 5 pages of tables from Math. Comp. paper\u003c/a\u003e",
				"Igor Pak, Greta Panova, Damir Yeliussizov, \u003ca href=\"https://arxiv.org/abs/1804.04693\"\u003eOn the largest Kronecker and Littlewood-Richardson coefficients\u003c/a\u003e, arXiv:1804.04693 [math.CO], 2018.",
				"R. P. Stanley, \u003ca href=\"/A003277/a003277.pdf\"\u003eLetter to N. J. A. Sloane, c. 1991\u003c/a\u003e"
			],
			"example": [
				"a(5) = 6 because the degrees for S_5 are 1,1,4,4,5,5,6."
			],
			"program": [
				"(Sage)",
				"def A003040(n):",
				"    res = 1",
				"    for P in Partitions(n):",
				"        res = max(res, P.dimension())",
				"    return res",
				"# _Eric M. Schmidt_, May 07 2013"
			],
			"xref": [
				"A117500 gives the corresponding partitions of n.",
				"Cf. A003869, A003870, A003871, A003872, A003873, A003874, A003875, A003876, A003877."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_ and _Richard Stanley_",
			"ext": [
				"Entry revised and extended by _N. J. A. Sloane_, Apr 28 2006",
				"a(29) corrected by _Eric M. Schmidt_, May 07 2013"
			],
			"references": 9,
			"revision": 42,
			"time": "2020-02-22T06:31:38-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}