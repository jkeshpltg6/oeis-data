{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A100861",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 100861,
			"data": "1,1,1,1,1,3,1,6,3,1,10,15,1,15,45,15,1,21,105,105,1,28,210,420,105,1,36,378,1260,945,1,45,630,3150,4725,945,1,55,990,6930,17325,10395,1,66,1485,13860,51975,62370,10395,1,78,2145,25740,135135,270270,135135,1,91,3003,45045,315315,945945,945945,135135",
			"name": "Triangle of Bessel numbers read by rows: T(n,k) is the number of k-matchings of the complete graph K(n).",
			"comment": [
				"Row n contains 1 + floor(n/2) terms. Row sums yield A000085. T(2n,n) = T(2n-1,n-1) = (2n-1)!! (A001147).",
				"Inverse binomial transform is triangle with T(2n,n) = (2n-1)!!, 0 otherwise. - _Paul Barry_, May 21 2005",
				"Equivalently, number of involutions of n with k pairs. - _Franklin T. Adams-Watters_, Jun 09 2006",
				"From _Gary W. Adamson_, Dec 09 2009: (Start)",
				"If considered as an infinite lower triangular matrix (cf. A144299),",
				"lim_{n-\u003e} A100861^n = A118930: (1, 1, 2, 4, 13, 41, ...).",
				"(End)",
				"Sum_{k=0..floor(n/2)} T(n,k)m^(n-2k)s^(2k) is the n-th non-central moment of the normal probability distribution with mean m and standard deviation s. - _Stanislav Sykora_, Jun 19 2014",
				"Row n is the list of coefficients of the independence polynomial of the n-triangular graph. - _Eric W. Weisstein_, Nov 11 2016",
				"Restating the 2nd part of the Name, row n is the list of coefficients of the matching-generating polynomial of the complete graph K_n. - _Eric W. Weisstein_, Apr 03 2018"
			],
			"reference": [
				"C. D. Godsil, Algebraic Combinatorics, Chapman \u0026 Hall, New York, 1993.",
				"M. Abramowitz and I. Stegun, Handbook of Mathematical Functions (1983 reprint), 10th edition, 1964, expression 22.3.11 in page 775."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A100861/b100861.txt\"\u003eRows n = 0..100, flattened\u003c/a\u003e",
				"Paul Barry, \u003ca href=\"https://arxiv.org/abs/1804.05027\"\u003eThe Gamma-Vectors of Pascal-like Triangles Defined by Riordan Arrays\u003c/a\u003e, arXiv:1804.05027 [math.CO], 2018.",
				"G. Le Caer, \u003ca href=\"http://dx.doi.org/10.1007/s10955-011-0245-4\"\u003eA new family of solvable Pearson-Dirichlet random walks\u003c/a\u003e, Journal of Statistical Physics 144:1 (2011), pp. 23-45.",
				"J. Y. Choi and J. D. H. Smith, \u003ca href=\"http://dx.doi.org/10.1016/S0012-365X(02)00549-6\"\u003eOn the unimodality and combinatorics of Bessel numbers\u003c/a\u003e, Discrete Math., 264 (2003), 45-53.",
				"T. Copeland, \u003ca href=\"http://tcjpn.wordpress.com/2012/11/29/infinigens-the-pascal-pyramid-and-the-witt-and-virasoro-algebras/\"\u003eInfinitesimal Generators, the Pascal Pyramid, and the Witt and Virasoro Algebras\u003c/a\u003e, 2012.",
				"John Engbers, David Galvin, Clifford Smyth, \u003ca href=\"https://arxiv.org/abs/1610.05803\"\u003eRestricted Stirling and Lah numbers and their inverses\u003c/a\u003e, arXiv:1610.05803 [math.CO], 2016.",
				"Mikael Fremling, \u003ca href=\"https://arxiv.org/1810.10391\"\u003eOn the modular covariance properties of composite fermions on the torus\u003c/a\u003e, arXiv:1810.10391 [cond-mat.str-el], 2018.",
				"A. Hernando, R. Hernando, A. Plastino and A. R. Plastino, \u003ca href=\"http://arxiv.org/abs/1201.0905\"\u003eThe workings of the Maximum Entropy Principle in collective human behavior\u003c/a\u003e, arXiv preprint arXiv:1201.0905 [stat.AP], 2012.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CompleteGraph.html\"\u003eComplete Graph\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/IndependencePolynomial.html\"\u003eIndependence Polynomial\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Matching-GeneratingPolynomial.html\"\u003eMatching-Generating Polynomial\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/TriangularGraph.html\"\u003eTriangular Graph\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Normal_distribution\"\u003eNormal distribution\u003c/a\u003e, section 'Moments'",
				"J. Zhou, \u003ca href=\"http://arxiv.org/abs/1405.5296\"\u003eQuantum deformation theory of the Airy curve and the mirror symmetry of a point\u003c/a\u003e, arXiv preprint arXiv:1405.5296 [math.AG], 2014."
			],
			"formula": [
				"T(n, k) = n!/(k!(n-2k)!*2^k).",
				"E.g.f.: exp(z+tz^2/2).",
				"G.f.: g(t, z) satisfies the differential equation g = 1 + zg + tz^2*(d/dz)(zg).",
				"Row generating polynomial = P[n] = [-i*sqrt(t/2)]^n*H(n, i/sqrt(2t)), where H(n, x) is a Hermite polynomial and i=sqrt(-1). Row generating polynomials P[n] satisfy P[0]=1, P[n] = P[n-1] + (n-1)tP[n-2].",
				"T(n, k) = binomial(n, 2k)(2k-1)!!. - _Paul Barry_, May 21 2002 [Corrected by Roland Hildebrand, Mar 06 2009]",
				"T(n,k) = (n-2k+1)*T(n-1,k-1) + T(n-1,k). - _Franklin T. Adams-Watters_, Jun 09 2006",
				"E.g.f.: 1 + (x+y*x^2/2)/(E(0)-(x+y*x^2/2)), where E(k) = 1 + (x+y*x^2/2)/(1 + (k+1)/E(k+1) ); (continued fraction). - _Sergei N. Gladkovskii_, Nov 08 2013",
				"T(n,k) = A144299(n,k), k=0..n/2. - _Reinhard Zumkeller_, Jan 02 2014"
			],
			"example": [
				"T(4, 2) = 3 because in the graph with vertex set {A, B, C, D} and edge set {AB, BC, CD, AD, AC, BD} we have the following three 2-matchings: {AB, CD},{AC, BD} and {AD, BC}.",
				"Triangle starts:",
				"[0] 1;",
				"[1] 1;",
				"[2] 1,  1;",
				"[3] 1,  3;",
				"[4] 1,  6,   3;",
				"[5] 1, 10,  15;",
				"[6] 1, 15,  45,   15;",
				"[7] 1, 21, 105,  105;",
				"[8] 1, 28, 210,  420, 105;",
				"[9] 1, 36, 378, 1260, 945.",
				".",
				"From _Eric W. Weisstein_, Nov 11 2016: (Start)",
				"As polynomials:",
				"1,",
				"1,",
				"1 + x,",
				"1 + 3*x,",
				"1 + 6*x + 3*x^2,",
				"1 + 10*x + 15*x^2,",
				"1 + 15*x + 45*x^2 + 15*x^3. (End)"
			],
			"maple": [
				"P[0]:=1: for n from 1 to 14 do P[n]:=sort(expand(P[n-1]+(n-1)*t*P[n-2])) od: for n from 0 to 14 do seq(coeff(t*P[n],t^k),k=1..1+floor(n/2)) od; # yields the sequence in triangular form",
				"# Alternative:",
				"A100861 := proc(n,k)",
				"    n!/k!/(n-2*k)!/2^k ;",
				"end proc:",
				"seq(seq(A100861(n,k),k=0..n/2),n=0..10) ; # _R. J. Mathar_, Aug 19 2014"
			],
			"mathematica": [
				"Table[Table[n!/(i! 2^i (n - 2 i)!), {i, 0, Floor[n/2]}], {n, 0, 10}] // Flatten  (* _Geoffrey Critzer_, Mar 27 2011 *)",
				"CoefficientList[Table[2^(n/2) (-(1/x))^(-n/2) HypergeometricU[-n/2, 1/2, -1/(2 x)], {n, 0, 10}], x] // Flatten (* _Eric W. Weisstein_, Apr 03 2018 *)",
				"CoefficientList[Table[(-I)^n Sqrt[x/2]^n HermiteH[n, I/Sqrt[2 x]], {n, 0, 10}], x] // Flatten (* _Eric W. Weisstein_, Apr 03 2018 *)"
			],
			"program": [
				"(PARI) T(n,k)=if(k\u003c0 || 2*k\u003en, 0, n!/k!/(n-2*k)!/2^k) /* _Michael Somos_, Jun 04 2005 */",
				"(Haskell)",
				"a100861 n k = a100861_tabf !! n !! k",
				"a100861_row n = a100861_tabf !! n",
				"a100861_tabf = zipWith take a008619_list a144299_tabl",
				"-- _Reinhard Zumkeller_, Jan 02 2014"
			],
			"xref": [
				"Other versions of this same triangle are given in A144299, A001497, A001498, A111924.",
				"Cf. A000085 (row sums).",
				"Cf. A118930, A008619."
			],
			"keyword": "nonn,tabf,nice,changed",
			"offset": "0,6",
			"author": "_Emeric Deutsch_, Jan 08 2005",
			"references": 23,
			"revision": 101,
			"time": "2022-01-14T07:31:24-05:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}