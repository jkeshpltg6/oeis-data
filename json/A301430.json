{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A301430",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 301430,
			"data": "3,0,2,3,1,6,1,4,2,3,5,7,0,6,5,6,3,7,9,4,7,7,6,9,9,0,0,4,8,0,1,9,9,7,1,5,6,0,2,4,1,2,7,9,5,1,8,9,3,6,9,6,4,5,4,5,8,8,6,7,8,4,1,2,8,8,8,6,5,4,4,8,7,5,2,4,1,0,5,1,0,8,9,9,4,8,7,4,6,7,8,1,3,9,7,9,2,7,2,7,0,8,5,6,7,7",
			"name": "Decimal expansion of an analog of the Landau-Ramanujan constant for Loeschian numbers which are sums of two squares.",
			"comment": [
				"This is the decimal expansion of the number alpha such that the number of positive integers \u003c= N which are sums of two squares and are also represented by the quadratic form x^2 + xy + y^2 is asymptotic to alpha*N*(log(N))^(-3/4).",
				"Based on the constants Zeta(m=12,n=5,s=2) = 1.0482019036007..., Zeta(m=12,n=7,s=2) = 1.0262021468... and Zeta(m=12,n=11,s=2) = 1.01177863 ... read from arXiv:1008.2547 we have Product_{p == 5, 7, 11(mod 12)} (1-1/p^2)^(-1/2) = sqrt( Zeta(m=12,n=5,s=2) * Zeta(m=12,n=7,s=2) * Zeta(m=12,n=11,s=2) ) as a factor in the formulas. - _R. J. Mathar_, Feb 04 2021"
			],
			"link": [
				"Salma Ettahri, Olivier Ramaré, and Léon Surel, \u003ca href=\"https://arxiv.org/abs/1908.06808\"\u003eFast multi-precision computation of some Euler products\u003c/a\u003e, arXiv:1908.06808 [math.NT], 2019.",
				"Étienne Fouvry, Claude Levesque, and Michel Waldschmidt, \u003ca href=\"http://arxiv.org/abs/1712.09019\"\u003eRepresentation of integers by cyclotomic binary forms\u003c/a\u003e, arXiv:1712.09019 [math.NT], 2017 and \u003ca href=\"https://doi.org/10.4064/aa171012-24-12\"\u003eActa Arithmetica\u003c/a\u003e, online 15 March 2018."
			],
			"formula": [
				"Equals (3^(1/4)/2^(5/4)) * Pi^(1/2) * (log(2 + sqrt(3)))^(1/4) / Gamma(1/4) * Product_{p == 5, 7, 11 (mod 12), p prime} (1 - 1/p^2)^(-1/2).",
				"One can base the definition on p(n) = A167135(n). Setting r(n) = (Product_{k=1..n} p(k)^2) / (Product_{k=1..n} (p(k)^2 - 1)) the rational sequence r(n) starts 4/3, 3/2, 25/16, 1225/768, 29645/18432, ... -\u003e L. Then A301430 = sqrt(L)*M with M = ((arccosh(2)/6)^(1/4)*Gamma(3/4))/(2*sqrt(Pi)). - _Peter Luschny_, Mar 29 2018"
			],
			"example": [
				"0.30231614235706563794776990048019971560241279..."
			],
			"maple": [
				"Digits:= 1000: with(numtheory):",
				"B:= evalf(3^(1/4)*Pi^(1/2)*log(2+sqrt(3))^(1/4)/(2^(5/4)*GAMMA(1/4))):",
				"for t to 500 do p:=ithprime(t): if `or`(`or`(`mod`(p, 12) = 5, `mod`(p, 12) = 7), `mod`(p, 12) = 11) then B:= evalf(B/(1-1/p^2)^(1/2)) end if end do: B;"
			],
			"mathematica": [
				"prec := 200; B = N[(Sqrt[Pi] ((3 Log[2 + Sqrt[3]])/2)^(1/4))/(2 Gamma[1/4]), prec];",
				"For[n = 3, n \u003c 50000, n++, p = Prime[n];",
				"If[Mod[p, 12] != 1, B = B / Sqrt[(1 - 1/p) (1 + 1/p)]]]",
				"Print[B] (* _Peter Luschny_, Mar 23 2018 *)",
				"(* -------------------------------------------------------------------------- *)",
				"S[m_, n_, s_] := (t = 1; sums = 0; difs = 1; While[Abs[difs] \u003e 10^(-digits - 5) || difs == 0, difs = (MoebiusMu[t]/t) * Log[If[s*t == 1, DirichletL[m, n, s*t], Sum[Zeta[s*t, j/m]*DirichletCharacter[m, n, j]^t, {j, 1, m}]/m^(s*t)]]; sums = sums + difs; t++]; sums);",
				"P[m_, n_, s_] := 1/EulerPhi[m] * Sum[Conjugate[DirichletCharacter[m, r, n]] * S[m, r, s], {r, 1, EulerPhi[m]}] + Sum[If[GCD[p, m] \u003e 1 \u0026\u0026 Mod[p, m] == n, 1/p^s, 0], {p, 1, m}];",
				"Z[m_, n_, s_] := (w = 1; sumz = 0; difz = 1; While[Abs[difz] \u003e 10^(-digits - 5), difz = P[m, n, s*w]/w; sumz = sumz + difz; w++]; Exp[sumz]);",
				"$MaxExtraPrecision = 1000; digits = 121; RealDigits[Chop[N[(3^(1/4)/2^(5/4)) * Pi^(1/2) * (Log[2 + Sqrt[3]])^(1/4) / Gamma[1/4] * Sqrt[Z[12, 5, 2] * Z[12, 7, 2] * Z[12, 11, 2]], digits]], 10, digits-1][[1]] (* _Vaclav Kotesovec_, Jan 15 2021 *)"
			],
			"xref": [
				"Cf. A003136, A064533, A167135, A301429, A340552."
			],
			"keyword": "nonn,cons",
			"offset": "0,1",
			"author": "_Michel Waldschmidt_, Mar 21 2018",
			"ext": [
				"Offset corrected by _Vaclav Kotesovec_, Mar 25 2018",
				"a(6)-a(10) from _Peter Luschny_, Mar 29 2018",
				"More digits from Ettahri article added by _Vaclav Kotesovec_, May 12 2020",
				"More digits from _Vaclav Kotesovec_, Jan 15 2021"
			],
			"references": 12,
			"revision": 75,
			"time": "2021-02-04T06:36:35-05:00",
			"created": "2018-03-24T03:14:19-04:00"
		}
	]
}