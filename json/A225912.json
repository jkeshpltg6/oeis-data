{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A225912",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 225912,
			"data": "0,1,-8,20,0,-74,96,-24,0,157,-432,124,0,478,704,-1480,0,-1198,792,3044,0,-480,-4320,184,0,2351,3344,-1720,0,-3282,5184,-5728,0,2480,-4752,1776,0,10326,-6688,9560,0,-8886,-8448,-9188,0,-11618,32832,23664,0,-16231",
			"name": "Expansion of q * (phi(-q^2) * psi(-q)^2)^4 in powers of q where phi(), psi() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A225912/b225912.txt\"\u003eTable of n, a(n) for n = 0..2500\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of (eta(q)^2 * eta(q^4))^4 in powers of q.",
				"Euler transform of period 4 sequence [-8, -8, -8, -12, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (8 t)) = 2^14 (t/i)^6 g(t) where q = exp(2 Pi i t) and g() is the g.f. for A225872.",
				"G.f.: x * (Product_{k\u003e0} (1 - x^k)^2 * (1 - x^(4*k)))^4."
			],
			"example": [
				"G.f. = q - 8*q^2 + 20*q^3 - 74*q^5 + 96*q^6 - 24*q^7 + 157*q^9 - 432*q^10 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ -(EllipticTheta[ 4, 0, q^2] EllipticTheta[ 2, 0, I q^(1/2)]^2 / 4 )^4, {q, 0, n}];",
				"a[ n_] := SeriesCoefficient[ q (QPochhammer[ q]^2 QPochhammer[ q^4])^4, {q, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c1, 0, n--; A = x * O(x^n); polcoeff( (eta(x + A)^2 * eta(x^4 + A))^4, n))};"
			],
			"xref": [
				"Cf. A225923 (bisection?)"
			],
			"keyword": "sign",
			"offset": "0,3",
			"author": "_Michael Somos_, May 20 2013",
			"references": 2,
			"revision": 20,
			"time": "2021-02-23T05:30:57-05:00",
			"created": "2013-05-20T16:26:07-04:00"
		}
	]
}