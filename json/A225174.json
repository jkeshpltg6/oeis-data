{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A225174",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 225174,
			"data": "1,1,1,1,2,1,1,1,1,1,1,1,3,1,1,1,1,1,1,1,1,1,2,1,4,1,2,1,1,1,3,1,1,3,1,1,1,1,1,1,5,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,1,1,1,6,1,1,1,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,7,1,1,1,1,1,1,1,1,3,1,5,1,1,1,1,5,1,3,1,1",
			"name": "Square array read by antidiagonals: T(m,n) = greatest common unitary divisor of m and n.",
			"reference": [
				"M. Lal, H. Wareham and R. Mifflin, Iterates of the bi-unitary totient function, Utilitas Math., 10 (1976), 347-350."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A225174/b225174.txt\"\u003eTable of n, a(n) for n = 1..20100; the first 200 antidiagonals of the array\u003c/a\u003e"
			],
			"formula": [
				"T(m,n) = T(n,m) = A165430(n,m)."
			],
			"example": [
				"Array begins",
				"1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...",
				"1, 2, 1, 1, 1, 2, 1, 1, 1, 2, 1, 1, ...",
				"1, 1, 3, 1, 1, 3, 1, 1, 1, 1, 1, 3, ...",
				"1, 1, 1, 4, 1, 1, 1, 1, 1, 1, 1, 4, ...",
				"1, 1, 1, 1, 5, 1, 1, 1, 1, 5, 1, 1, ...",
				"1, 2, 3, 1, 1, 6, 1, 1, 1, 2, 1, 3, ...",
				"1, 1, 1, 1, 1, 1, 7, 1, 1, 1, 1, 1, ...",
				"1, 1, 1, 1, 1, 1, 1, 8, 1, 1, 1, 1, ...",
				"...",
				"The unitary divisors of 3 are 1 and 3, those of 6 are 1,2,3,6; so T(6,3) = T(3,6) = 3."
			],
			"maple": [
				"# returns the greatest common unitary divisor of m and n",
				"f:=proc(m,n)",
				"local i,ans;",
				"ans:=1;",
				"for i from 1 to min(m,n) do",
				"if ((m mod i) = 0) and (igcd(i,m/i) = 1)  then",
				"   if ((n mod i) = 0) and (igcd(i,n/i) = 1)  then ans:=i; fi;",
				"fi;",
				"od;",
				"ans; end;"
			],
			"mathematica": [
				"f[m_, n_] := Module[{i, ans=1}, For[i=1, i\u003c=Min[m, n], i++, If[Mod[m, i]==0 \u0026\u0026 GCD[i, m/i]==1, If[Mod[n, i]==0 \u0026\u0026 GCD[i, n/i]==1, ans=i]]]; ans];",
				"Table[f[m-n+1, n], {m, 1, 14}, {n, 1, m}] // Flatten (* _Jean-François Alcover_, Jun 19 2018, translated from Maple *)"
			],
			"program": [
				"(PARI)",
				"up_to = 20100; \\\\ = binomial(200+1,2)",
				"A225174sq(m,n) = { my(a=min(m,n),b=max(m,n),md=0); fordiv(a,d,if(0==(b%d)\u0026\u00261==gcd(d,a/d)\u0026\u00261==gcd(d,b/d),md=d)); (md); };",
				"A225174list(up_to) = { my(v = vector(up_to), i=0); for(a=1,oo, for(col=1,a, if(i++ \u003e up_to, return(v)); v[i] = A225174sq((a-(col-1)),col))); (v); };",
				"v225174 = A225174list(up_to);",
				"A225174(n) = v225174[n]; \\\\ _Antti Karttunen_, Nov 28 2018"
			],
			"xref": [
				"See A034444, A077610 for unitary divisors of n.",
				"Different from A059895."
			],
			"keyword": "nonn,tabl,look",
			"offset": "1,5",
			"author": "_N. J. A. Sloane_, May 01 2013",
			"references": 4,
			"revision": 24,
			"time": "2018-11-28T20:58:31-05:00",
			"created": "2013-05-01T13:41:57-04:00"
		}
	]
}