{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A195820",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 195820,
			"data": "0,1,1,3,2,7,5,12,13,22,22,43,43,67,81,117,133,195,223,312,373,492,584,782,925,1190,1433,1820,2170,2748,3268,4075,4872,5997,7150,8781,10420,12669,15055,18198,21535,25925,30602,36624,43201,51428,60478,71802,84215",
			"name": "Total number of smallest parts in all partitions of n that do not contain 1 as a part.",
			"comment": [
				"Total number of smallest parts in all partitions of the head of the last section of the set of partitions of n."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A195820/b195820.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"G. E. Andrews, \u003ca href=\"http://www.math.psu.edu/vstein/alg/antheory/preprint/andrews/17.pdf\"\u003eThe number of smallest parts in the partitions of n\u003c/a\u003e",
				"A. Folsom and K. Ono, \u003ca href=\"http://www.math.wisc.edu/~ono/reprints/111.pdf\"\u003eThe spt-function of Andrews\u003c/a\u003e",
				"F. G. Garvan, \u003ca href=\"http://arxiv.org/abs/1011.1957\"\u003eCongruences for Andrews' spt-function modulo 32760 and extension of Atkin's Hecke-type partition congruences\u003c/a\u003e",
				"F. G. Garvan, \u003ca href=\"http://www.math.ufl.edu/~fgarvan/papers/spt2.pdf\"\u003eCongruences for Andrews' spt-function modulo powers of 5, 7 and 13\u003c/a\u003e",
				"K. Ono, \u003ca href=\"http://www.mathcs.emory.edu/~ono/publications-cv/pdfs/131.pdf\"\u003eCongruences for the Andrews spt-function\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Spt_function\"\u003eSpt function\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A092269(n) - A000070(n-1).",
				"G.f.: Sum_{i\u003e=2} x^i/(1 - x^i) * Product_{j\u003e=i} 1/(1 - x^j). - _Ilya Gutkovskiy_, Apr 03 2017",
				"a(n) ~ exp(Pi*sqrt(2*n/3)) / (8*sqrt(3)*n) * (1 - (72 + 5*Pi^2)*sqrt(6) / (144*Pi*sqrt(n))). - _Vaclav Kotesovec_, Jul 31 2017"
			],
			"example": [
				"For n = 8 the seven partitions of 8 that do not contain 1 as a part are:",
				".  (8)",
				".  (4) + (4)",
				".   5  + (3)",
				".   6  + (2)",
				".   3  +  3  + (2)",
				".   4  + (2) + (2)",
				".  (2) + (2) + (2) + (2)",
				"Note that in every partition the smallest parts are shown between parentheses. The total number of smallest parts is 1+2+1+1+1+2+4 = 12, so a(8) = 12."
			],
			"maple": [
				"b:= proc(n, i) option remember;",
				"      `if`(n=0 or i\u003c2, 0, b(n, i-1)+",
				"       add(`if`(n=i*j, j, b(n-i*j, i-1)), j=1..n/i))",
				"    end:",
				"a:= n-\u003e b(n, n):",
				"seq(a(n), n=1..60); # _Alois P. Heinz_, Apr 09 2012"
			],
			"mathematica": [
				"Table[s = Select[IntegerPartitions[n], ! MemberQ[#, 1] \u0026]; Plus @@ Table[Count[x, Min[x]], {x, s}], {n, 50}] (* _T. D. Noe_, Oct 19 2011 *)",
				"b[n_, i_] := b[n, i] = If[n==0 || i\u003c2, 0, b[n, i-1] + Sum[If[n== i*j, j, b[n-i*j, i-1]], {j, 1, n/i}]]; a[n_] := b[n, n]; Table[a[n], {n, 1, 60}] (* _Jean-François Alcover_, Oct 12 2015, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(Sage)",
				"def A195820(n):",
				"    return sum(list(p).count(min(p)) for p in Partitions(n,min_part=2))",
				"# [D. S. McNeil, Oct 19 2011]"
			],
			"xref": [
				"Cf. A000041, A000070, A002865, A092269, A135010, A138121, A138135, A138137, A182984."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Omar E. Pol_, Oct 19 2011",
			"ext": [
				"More terms from _D. S. McNeil_, Oct 19 2011"
			],
			"references": 13,
			"revision": 48,
			"time": "2020-03-05T07:05:09-05:00",
			"created": "2011-10-19T21:21:40-04:00"
		}
	]
}