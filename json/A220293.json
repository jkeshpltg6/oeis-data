{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A220293",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 220293,
			"data": "11,17,29,41,47,59,67,71,97,101,107,127,149,151,167,179,223,229,233,239,241,263,269,281,307,311,347,349,367,373,401,409,419,431,433,443,461,487,503,569,571,587,593,599,601,607,641,643,647,653",
			"name": "Chebyshev numbers C_2(n): a(n) is the smallest number such that if x \u003e= a(n), then theta(x) - theta(x/2) \u003e= n*log(x), where theta(x) = sum_{prime p \u003c= x} log p.",
			"comment": [
				"Up to a(100)=1489, only two terms of the sequence (a(17)=223 and a(36)=443) are not Ramanujan numbers (A104272), and the sequence is missing only the following Ramanujan numbers up to 1489: 2, 181, 227, 439, 491, 1283, and 1301. The latter observation shows how closely the ratio theta(x)/log(x) approximates the number of primes \u003c= x (i.e., pi(x)).",
				"A generalization: for a real number v\u003e1, the v-Chebyshev number C_v(n) is the smallest integer k such that if x\u003e=k, then theta(x)-theta(x/v)\u003e=n*log x. In particular, a(n)=C_2(n). For another example, if v=4/3, then, at least up to 3319, all (4/3)-Chebyshev numbers are (4/3)-Ramanujan primes as in Shevelev's link (cf. A193880, where c=1/v=3/4 is excepted), and in this case the sequence is missing only the following (4/3)-Ramanujan numbers up to 3319: 11 and 1567.",
				"Like Chebyshev numbers, all v-Chebyshev numbers are primes."
			],
			"link": [
				"N. Amersi, O. Beckwith, S. J. Miller, R. Ronan, and J. Sondow, \u003ca href=\"http://arxiv.org/abs/1108.0475\"\u003eGeneralized Ramanujan primes\u003c/a\u003e, arXiv 2011.",
				"N. Amersi, O. Beckwith, S. J. Miller, R. Ronan, J. Sondow, \u003ca href=\"http://link.springer.com/chapter/10.1007/978-1-4939-1601-6_1\"\u003eGeneralized Ramanujan primes\u003c/a\u003e, Combinatorial and Additive Number Theory, Springer Proc. in Math. \u0026 Stat., CANT 2011 and 2012, Vol. 101 (2014), 1-13",
				"V. Shevelev, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL15/Shevelev/shevelev19.html\"\u003eRamanujan and Labos primes, their generalizations, and classifications of primes\u003c/a\u003e, J. Integer Seq. 15 (2012) Article 12.5.4",
				"Vladimir Shevelev, Charles R. Greathouse IV, and Peter J. C. Moses, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL16/Moses/moses1.html\"\u003eOn intervals (kn, (k+1)n) containing a prime for all n\u003e1\u003c/a\u003e, Journal of Integer Sequences, Vol. 16 (2013), Article 13.7.3. \u003ca href=\"http://arxiv.org/abs/1212.2785\"\u003earXiv:1212.2785\u003c/a\u003e"
			],
			"formula": [
				"For n \u003e= 2, A104272(n) \u003c= a(n-1) \u003c= prime(3n)."
			],
			"xref": [
				"Cf. A104272."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Vladimir Shevelev_, _Charles R Greathouse IV_ and _Peter J. C. Moses_, Dec 09 2012",
			"references": 4,
			"revision": 46,
			"time": "2014-11-08T18:52:52-05:00",
			"created": "2012-12-27T11:59:53-05:00"
		}
	]
}