{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A271510",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 271510,
			"data": "1,3,3,2,4,4,1,1,3,4,5,2,3,5,2,1,4,5,5,3,4,2,2,1,1,8,5,4,4,4,2,2,3,3,7,2,6,7,3,3,5,6,4,6,2,4,4,1,3,6,9,4,8,5,6,2,2,6,10,4,1,5,3,7,4,10,3,5,5,2,4,1,5,6,7,2,6,1,7,4,4",
			"name": "Number of ordered ways to write n as x^2 + y^2 + z^2 + w^2 with x \u003e= y \u003e= 0, z \u003e= 0 and w \u003e= 0 such that x^2 + 8*y^2 + 16*z^2 is a square.",
			"comment": [
				"Conjecture: (i) a(n) \u003e 0 for all n = 0,1,2,..., and a(n) = 1 only for n = 0, 7, 23, 71, 77, 105, 191, 215, 311, 335, 2903, 4^k*q (k = 0,1,2,... and q = 6, 15, 47, 138).",
				"(ii) Any natural number can be written as x^2 + y^2 + z^2 + w^2 with x \u003e= y \u003e= 0, z \u003e=0 and w \u003e= 0 such that 4*x^2 + 21*y^2 + 24*z^2 (or 5*x^2 + 40*y^2 + 4*z^2, 20*x^2 + 85*y^2 +16*z^2, 25*x^2 + 480*y^2 + 96*z^2, 36*x^2 + 45*y^2 + 40*z^2, 40*x^2 + 72*y^2 + 9*z^2) is a square.",
				"(iii) For any ordered pair (b, c) = (48, 112), (63, 7), (112, 1008), (136, 24), (136, 216), (360, 40), (840, 280), (1008, 112), each natural number can be written as x^2 + y^2 + z^2 + w^2 with x \u003e= y \u003e= 0, z \u003e=0 and w \u003e= 0 such that 9*x^2 + b*y^2 + c*z^2 is a square.",
				"(iv) For any ordered pair (b, c) = (80, 25), (81, 48), (144, 9), (144, 153), (177, 48), each natural number can be written as x^2 + y^2 + z^2 + w^2 with x \u003e= y \u003e= 0, z \u003e=0 and w \u003e= 0 such that 16*x^2 + b*y^2 + c*z^2 is a square.",
				"This conjecture is much stronger than Lagrange's four-square theorem. It is apparent that a(m^2*n) \u003e= a(n) for all m,n = 1,2,3,....",
				"See also A271513 and A271518 for related conjectures."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A271510/b271510.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2016.11.008\"\u003eRefining Lagrange's four-square theorem\u003c/a\u003e, J. Number Theory 175(2017), 167-190. Also available from \u003ca href=\"http://arxiv.org/abs/1604.06723\"\u003earXiv:1604.06723 [math.NT]\u003c/a\u003e, 2016-2017."
			],
			"example": [
				"a(6) = 1 since 6 = 1^2 + 1^2 + 0^2 + 2^2 with 1 = 1 and 1^2 + 8*1^2 + 16*0^2 = 3^2.",
				"a(7) = 1 since 7 = 1^2 + 1^2 + 1^2 + 2^2 with 1 = 1 and 1^2 + 8*1^2 + 16*1^2 = 5^2.",
				"a(15) = 1 since 15 = 3^2 + 1^2 + 2^2 + 1^2 with 3 \u003e 1 and 3^2 + 8*1^2 + 16*2^2 = 9^2.",
				"a(23) = 1 since 23 = 3^2 + 1^2 + 2^2 + 3^2 with 3 \u003e 1 and 3^2 + 8*1^2 + 16*2^2 = 9^2.",
				"a(47) = 1 since 47 = 3^2 + 2^2 + 5^2 + 3^2 with 3 \u003e 2 and 3^2 + 8*2^2 + 16*5^2 = 21^2.",
				"a(71) = 1 since 71 = 7^2 + 2^2 + 3^2 + 3^2 with 7 \u003e 2 and 7^2 + 8*2^2 + 16*3^2 = 15^2.",
				"a(77) = 1 since 77 = 5^2 + 4^2 + 6^2 + 0^2 with 5 \u003e 4 and 5^2 + 8*4^2 + 16*6^2 = 27^2.",
				"a(105) = 1 since 105 = 6^2 + 2^2 + 4^2 + 7^2 with 6 \u003e 2 and 6^2 + 8*2^2 + 16*4^2 = 18^2.",
				"a(138) = 1 since 138 = 3^2 + 2^2 + 5^2 + 10^2 with 3 \u003e 2 and 3^2 + 8*2^2 + 16*5^2 = 21^2.",
				"a(191) = 1 since 191 = 9^2 + 3^2 + 1^2 + 10^2 with 9 \u003e 3 and 9^2 + 8*3^2 + 16*1^2 = 13^2.",
				"a(215) = 1 since 215 = 11^2 + 7^2 + 6^2 + 3^2 with 11 \u003e 7 and 11^2 + 8*7^2 + 16*6^2 = 33^2.",
				"a(311) = 1 since 311 = 15^2 + 6^2 + 1^2 + 7^2 with 15 \u003e 6 and 15^2 + 8*6^2 + 16*1^2 = 23^2.",
				"a(335) = 1 since 335 = 17^2 + 1^2 + 3^2 + 6^2 with 17 \u003e 1 and 17^2 + 8*1^2 + 16*3^2 = 21^2.",
				"a(2903) = 1 since 2903 = 49^2 + 14^2 + 15^2 + 9^2 with 49 \u003e 14 and 49^2 + 8*14^2 + 16*15^2 = 87^2."
			],
			"mathematica": [
				"SQ[n_]:=SQ[n]=IntegerQ[Sqrt[n]]",
				"Do[r=0;Do[If[SQ[n-x^2-y^2-z^2]\u0026\u0026SQ[x^2+8y^2+16z^2],r=r+1],{y,0,Sqrt[n/2]},{x,y,Sqrt[n-y^2]},{z,0,Sqrt[n-x^2-y^2]}];Print[n,\" \",r];Continue,{n,0,80}]"
			],
			"xref": [
				"Cf. A000118, A000290, A270969, A271513, A271518."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Zhi-Wei Sun_, Apr 09 2016",
			"references": 53,
			"revision": 31,
			"time": "2017-02-11T11:21:46-05:00",
			"created": "2016-04-09T12:09:54-04:00"
		}
	]
}