{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A198631",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 198631,
			"data": "1,1,0,-1,0,1,0,-17,0,31,0,-691,0,5461,0,-929569,0,3202291,0,-221930581,0,4722116521,0,-968383680827,0,14717667114151,0,-2093660879252671,0,86125672563201181,0,-129848163681107301953,0,868320396104950823611,0",
			"name": "Numerators of the rational sequence with e.g.f. 1/(1+exp(-x)).",
			"comment": [
				"Numerators of the row sums of the Euler triangle A060096/A060097.",
				"The corresponding denominator sequence looks like 2*A006519(n+1) when n is odd.",
				"Also numerator of the value at the origin of the n-th derivative of the standard logistic function. - _Enrique Pérez Herrero_, Feb 15 2016"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A198631/b198631.txt\"\u003eTable of n, a(n) for n = 0..550\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/SigmoidFunction.html\"\u003eSigmoid Function\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Logistic_function\"\u003eLogistic Function\u003c/a\u003e."
			],
			"formula": [
				"a(n) = numerator(sum(E(n,m),m=0..n)), n\u003e=0, with the Euler triangle E(n,m)=A060096(n,m)/A060097(n,m).",
				"E.g.f.: 2/(1+exp(-x)) (see a comment in  A060096).",
				"r(n) := sum(E(n,m),m=0..n) = ((-1)^n)*sum(((-1)^m)*m!*S2(n,m)/2^m, m=0..n), n\u003e=0, where S2 are the Stirling numbers of the second kind A048993. From the e.g.f. with y=exp(-x), dx=-y*dy, putting y=1 at the end. - _Wolfdieter Lang_, Nov 03 2011",
				"a(n) = numerator(euler(n,1)/(2^n-1)) for n \u003e 0. - _Peter Luschny_, Jul 14 2013",
				"a(n) = numerator(2*(2^n-1)*B(n,1)/n) for n \u003e 0, B(n,x) the Bernoulli polynomials. - _Peter Luschny_, May 24 2014",
				"Numerators of the Taylor series coefficients 4*(2^(n+1)-1)*B(n+1)/(n+1) for n\u003e0 of 1 + 2 * tanh(x/2) (cf. A000182 and A089171). - _Tom Copeland_, Oct 19 2016",
				"a(n) = -2*zeta(-n)*A335956(n+1). - _Peter Luschny_, Jul 21 2020"
			],
			"example": [
				"The rational sequence r(n) = a(n) / A006519(n+1) starts:",
				"1, 1/2, 0, -1/4, 0, 1/2, 0, -17/8, 0, 31/2, 0, -691/4, 0, 5461/2, 0, -929569/16, 0, 3202291/2, 0, -221930581/4, 0, 4722116521/2, 0, -968383680827/8, 0, 14717667114151/2, 0, -2093660879252671/4, ..."
			],
			"maple": [
				"seq(denom(euler(i,x))*euler(i,1),i=0..33); # _Peter Luschny_, Jun 16 2012"
			],
			"mathematica": [
				"Join[{1},Table[Numerator[EulerE[n,1]/(2^n-1)], {n, 34}]] (* _Peter Luschny_, Jul 14 2013 *)"
			],
			"program": [
				"(Sage)",
				"def A198631_list(n):",
				"    x = var('x')",
				"    s = (1/(1+exp(-x))).series(x,n+2)",
				"    return [(factorial(i)*s.coefficient(x,i)).numerator() for i in (0..n)]",
				"A198631_list(34) # _Peter Luschny_, Jul 12 2012",
				"(Sage) # Alternatively:",
				"def A198631_list(len):",
				"    e, f, R, C = 2, 1, [], [1]+[0]*(len-1)",
				"    for n in (1..len-1):",
				"        for k in range(n, 0, -1):",
				"            C[k] = -C[k-1] / (k+1)",
				"        C[0] = -sum(C[k] for k in (1..n))",
				"        R.append(numerator((e-1)*f*C[0]))",
				"        f *= n; e \u003c\u003c= 1",
				"    return R",
				"print(A198631_list(36)) # _Peter Luschny_, Feb 21 2016"
			],
			"xref": [
				"Cf. A000182, A060096, A060097, A006519, A002425, A089171, A090681."
			],
			"keyword": "sign,easy,frac",
			"offset": "0,8",
			"author": "_Wolfdieter Lang_, Oct 31 2011",
			"ext": [
				"New name, a simpler standalone definition by _Peter Luschny_, Jul 13 2012",
				"Second comment corrected by _Robert Israel_, Feb 21 2016"
			],
			"references": 23,
			"revision": 81,
			"time": "2020-07-21T11:48:00-04:00",
			"created": "2011-10-31T11:54:52-04:00"
		}
	]
}