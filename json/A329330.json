{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A329330",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 329330,
			"data": "1,1,1,1,2,1,1,3,3,1,1,4,4,4,1,1,5,5,5,5,1,1,6,7,7,7,6,1,1,7,12,9,9,12,7,1,1,8,9,20,11,20,9,8,1,1,9,15,11,35,35,11,15,9,1,1,10,11,28,13,8,13,28,11,10,1,1,11,21,13,45,63,63,45,13,21,11,1",
			"name": "Multiplication operation of a ring over the positive integers that has A059897(.,.) as addition operation and is isomorphic to GF(2)[x] with polynomial x^i mapped to A050376(i+1). Square array read by descending antidiagonals: A(n,k), n \u003e= 1, k \u003e= 1.",
			"comment": [
				"When creating A329329, the author realized it was isomorphic to multiplication in the GF(2)[x,y] polynomial ring. However, A329329 was unusual in having A059897(.,.) as additive operator, whereas the equivalent univariate polynomial ring, GF(2)[x], is more commonly mapped (to integers) with bitwise exclusive-or (A003987) representing polynomial addition (and A048720(.,.) representing polynomial multiplication). This sequence shows how multiplication in GF(2)[x] can look when mapped to integers with A059897(.,.) representing polynomial addition.",
				"The group defined by the binary operation A059897(.,.) over the positive integers is commutative with all elements self-inverse, and isomorphic to the additive group of the polynomial ring GF(2)[x]. There is a unique isomorphism extending each bijective mapping between respective minimal generating sets. The lexicographically earliest minimal generating set for the A059897 group is A050376, often called the Fermi-Dirac primes. The most meaningful generating set for the additive group of GF(2)[x] is {x^i: i \u003e= 0).",
				"Using f to denote the intended isomorphism from GF(2)[x], we specify f(x^i) = A050376(i+1). This maps minimal generating sets of the additive groups, so the definition of f is completed by specifying f(a+b) = A059897(f(a), f(b)). We then calculate the image under f of polynomial multiplication in GF(2)[x], giving us this sequence as the matching multiplication operation for an isomorphic ring over the positive integers. Using g to denote the inverse of f, A(n,k) = f(g(n) * g(k)).",
				"Note that A050376 is closed with respect to A(.,.).",
				"Recall that GF(2)[x] is more usually mapped to integers with A003987(.,.) as addition and A048720(.,.) as multiplication. With this usual mapping, under which A000079(i) is the image of x^i, A052330(.) is the relevant isomorphism from nonnegative integers under A048720(.,.) and A003987(.,.) to positive integers under A(.,.) and A059897(.,.), with A052331(.) its inverse."
			],
			"link": [
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Distributive.html\"\u003eDistributive\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Group.html\"\u003eGroup\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Ring.html\"\u003eRing\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Generating_set_of_a_group\"\u003eGenerating set of a group\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Polynomial_ring\"\u003ePolynomial ring\u003c/a\u003e"
			],
			"formula": [
				"A(n,k) = A052330(A048720(A052331(n), A052331(k))), n \u003e= 1, k \u003e= 1.",
				"A059897-based definition: (Start)",
				"A(A050376(i), A050376(j)) = A050376(i+j-1).",
				"A(A059897(n,k), m) = A059897(A(n,m), A(k,m)).",
				"A(m, A059897(n,k)) = A059897(A(m,n), A(m,k)).",
				"(End)",
				"Derived identities: (Start)",
				"A(n,1) = A(1,n) = 1.",
				"A(n,2) = A(2,n) = n.",
				"A(n,k) = A(k,n).",
				"A(n, A(m,k)) = A(A(n,m), k).",
				"(End)",
				"A(A300841(n), k) = A(n, A300841(k)) = A300841(A(n,k)).",
				"A(n,3) = A(3,n) = A300841(n).",
				"A(n,4) = A(4,n) = A300841^2(n).",
				"A(n,5) = A(5,n) = A300841^3(n).",
				"A(A050376(m), 6) = A(6, A050376(m)) = A240521(m).",
				"A(n,7) = A(7,n) = A300841^4(n).",
				"A(A050376(m), 8) = A(8, A050376(m)) = A240522(m).",
				"A(n,9) = A(9,n) = A300841^5(n).",
				"A(A050376(m), 10) = A(10, A050376(m)) = A240536(m).",
				"A(A050376(m), 12) = A(12, A050376(m)) = A300841(A240521(m)).",
				"A(A050376(m), 24) = A(24, A050376(m)) = A240524(m).",
				"A(A050376(m), 30) = A(30, A050376(m)) = A241025(m).",
				"A(A050376(m), 40) = A(40, A050376(m)) = A241024(m)."
			],
			"example": [
				"Square array A(n,k) begins:",
				"  n\\k |  1    2    3    4    5    6    7    8    9   10   11   12",
				"  ----+----------------------------------------------------------",
				"   1  |  1    1    1    1    1    1    1    1    1    1    1    1",
				"   2  |  1    2    3    4    5    6    7    8    9   10   11   12",
				"   3  |  1    3    4    5    7   12    9   15   11   21   13   20",
				"   4  |  1    4    5    7    9   20   11   28   13   36   16   35",
				"   5  |  1    5    7    9   11   35   13   45   16   55   17   63",
				"   6  |  1    6   12   20   35    8   63  120   99  210  143   15",
				"   7  |  1    7    9   11   13   63   16   77   17   91   19   99",
				"   8  |  1    8   15   28   45  120   77   14  117  360  176  420",
				"   9  |  1    9   11   13   16   99   17  117   19  144   23  143",
				"  10  |  1   10   21   36   55  210   91  360  144   22  187  756",
				"  11  |  1   11   13   16   17  143   19  176   23  187   25  208",
				"  12  |  1   12   20   35   63   15   99  420  143  756  208   28"
			],
			"xref": [
				"Cf. A000079, A050376, A329329.",
				"Distributes over A059897, and isomorphic to A048720 over A003987, with A052331 (inverse A052330) as isomorphism.",
				"Row/column 3: A300841.",
				"Row/column k sorted into increasing order: A003159 (k=3), A339690 (k=4), A000379 (k=6).",
				"Subsequences of row/column k: A240521 (k=6), A240522 (k=8), A240536 (k=10), A240524 (k=24), A241025 (k=30), A241024 (k=40)."
			],
			"keyword": "nonn,tabl",
			"offset": "1,5",
			"author": "_Peter Munn_, Nov 10 2019",
			"references": 1,
			"revision": 31,
			"time": "2021-08-23T13:39:16-04:00",
			"created": "2020-04-09T21:43:59-04:00"
		}
	]
}