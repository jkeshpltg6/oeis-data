{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A112148",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 112148,
			"data": "1,0,6,-4,-3,12,-8,-12,30,-20,-30,72,-46,-60,156,-96,-117,300,-188,-228,552,-344,-420,1008,-603,-732,1770,-1048,-1245,2976,-1776,-2088,4908,-2900,-3420,7992,-4658,-5460,12756,-7408,-8583,19944,-11564,-13344,30756,-17744,-20448,46944",
			"name": "McKay-Thompson series of class 12B for the Monster group.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A112148/b112148.txt\"\u003eTable of n, a(n) for n = -1..1000\u003c/a\u003e",
				"D. Ford, J. McKay and S. P. Norton, \u003ca href=\"http://dx.doi.org/10.1080/00927879408825127\"\u003eMore on replicable functions\u003c/a\u003e, Commun. Algebra 22, No. 13, 5175-5193 (1994).",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e",
				"\u003ca href=\"/index/Mat#McKay_Thompson\"\u003eIndex entries for McKay-Thompson series for Monster simple group\u003c/a\u003e"
			],
			"formula": [
				"Expansion of -5 + (1/q) * (phi(q)^3 * psi(-q)) / (phi(q^3) * psi(-q^3)^3) in powers of q where phi(), psi() are Ramanujan theta functions.",
				"a(n) = -(-1)^n * A007258(n). - _Michael Somos_, May 20 2015",
				"a(n) = A187146(n) = A187147(n) = A187148(n) unless n=0. - _Michael Somos_, May 20 2015"
			],
			"example": [
				"T12B = 1/q + 6*q - 4*q^2 - 3*q^3 + 12*q^4 - 8*q^5 - 12*q^6 + 30*q^7 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ -5 + 2 EllipticTheta[3, 0, q]^3 EllipticTheta[2, Pi/4, q^(1/2)] / (EllipticTheta[3, 0, q^3] EllipticTheta[2, Pi/4, q^(3/2)]^3), {q, 0, n}]; (* _Michael Somos_, May 20 2015 *)",
				"QP = QPochhammer; s = -5*q +QP[q^2]^14/(QP[q]^5*QP[q^3]*QP[q^4]^5* QP[q^6]^2*QP[q^12]) + O[q]^50; CoefficientList[s, q] (* _Jean-François Alcover_, Nov 16 2015, adapted from PARI *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c-1, 0, n++; A =x * O(x^n); polcoeff( -5 * x + eta(x^2 + A)^14 / (eta(x + A)^5 * eta(x^3 + A) * eta(x^4 + A)^5 * eta(x^6 + A)^2 * eta(x^12 + A)), n))}; /* _Michael Somos_, May 20 2015 */"
			],
			"xref": [
				"Cf. A007258, A187146, A187147, A187148."
			],
			"keyword": "sign",
			"offset": "-1,3",
			"author": "_Michael Somos_, Aug 28 2005",
			"references": 4,
			"revision": 23,
			"time": "2021-03-12T22:24:43-05:00",
			"created": "2005-09-21T03:00:00-04:00"
		}
	]
}