{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A277536",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 277536,
			"data": "1,0,1,0,0,2,0,0,3,6,0,0,8,24,24,0,0,10,170,180,120,0,0,54,900,1980,1440,720,0,0,-42,6566,19530,21840,12600,5040,0,0,944,44072,224112,305760,248640,120960,40320,0,0,-5112,365256,2650536,4818744,4536000,2993760,1270080,362880",
			"name": "T(n,k) is the n-th derivative of the difference between the k-th tetration of x (power tower of order k) and its predecessor (or 0 if k=0) at x=1; triangle T(n,k), n\u003e=0, 0\u003c=k\u003c=n, read by rows.",
			"comment": [
				"T(n,k) is defined for all n,k \u003e= 0.  The triangle contains only the terms with k\u003c=n.  T(n,k) = 0 for k\u003en."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A277536/b277536.txt\"\u003eRows n = 0..140, flattened\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PowerTower.html\"\u003ePower Tower\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Knuth%27s_up-arrow_notation\"\u003eKnuth's up-arrow notation\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Tetration\"\u003eTetration\u003c/a\u003e"
			],
			"formula": [
				"E.g.f. of column k\u003e0: (x+1)^^k - (x+1)^^(k-1), e.g.f. of column k=0: 1.",
				"T(n,k) = [(d/dx)^n (x^^k - x^^(k-1))]_{x=1} for k\u003e0, T(n,0) = A000007(n).",
				"T(n,k) = A277537(n,k) - A277537(n,k-1) for k\u003e0, T(n,0) = A000007(n).",
				"T(n,k) = n * A295027(n,k) for n,k \u003e 0."
			],
			"example": [
				"Triangle T(n,k) begins:",
				"  1;",
				"  0, 1;",
				"  0, 0,   2;",
				"  0, 0,   3,     6;",
				"  0, 0,   8,    24,     24;",
				"  0, 0,  10,   170,    180,    120;",
				"  0, 0,  54,   900,   1980,   1440,    720;",
				"  0, 0, -42,  6566,  19530,  21840,  12600,   5040;",
				"  0, 0, 944, 44072, 224112, 305760, 248640, 120960, 40320;",
				"  ..."
			],
			"maple": [
				"f:= proc(n) option remember; `if`(n\u003c0, 0,",
				"      `if`(n=0, 1, (x+1)^f(n-1)))",
				"    end:",
				"T:= (n, k)-\u003e n!*coeff(series(f(k)-f(k-1), x, n+1), x, n):",
				"seq(seq(T(n, k), k=0..n), n=0..12);",
				"# second Maple program:",
				"b:= proc(n, k) option remember; `if`(n=0, 1, `if`(k=0, 0,",
				"      -add(binomial(n-1, j)*b(j, k)*add(binomial(n-j, i)*",
				"      (-1)^i*b(n-j-i, k-1)*(i-1)!, i=1..n-j), j=0..n-1)))",
				"    end:",
				"T:= (n, k)-\u003e b(n, min(k, n))-`if`(k=0, 0, b(n, min(k-1, n))):",
				"seq(seq(T(n, k), k=0..n), n=0..12);"
			],
			"mathematica": [
				"f[n_] := f[n] = If[n \u003c 0, 0, If[n == 0, 1, (x + 1)^f[n - 1]]];",
				"T[n_, k_] := n!*SeriesCoefficient[f[k] - f[k - 1], { x, 0, n}];",
				"Table[T[n, k], {n, 0, 12}, {k, 0, n}] // Flatten",
				"(* second program: *)",
				"b[n_, k_] := b[n, k] = If[n == 0, 1, If[k == 0, 0, -Sum[Binomial[n - 1, j]*b[j, k]*Sum[Binomial[n - j, i]*(-1)^i*b[n - j - i, k - 1]*(i - 1)!, {i, 1, n - j}], {j, 0, n - 1}]]];",
				"T[n_, k_] := (b[n, Min[k, n]] - If[k == 0, 0, b[n, Min[k - 1, n]]]);",
				"Table[T[n, k], {n, 0, 12}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, May 28 2018, from Maple *)"
			],
			"xref": [
				"Columns k=0-2 give: A000007, A063524, A005727 (for n\u003e1).",
				"Main diagonal gives A000142.",
				"Row sums give A033917.",
				"T(n+1,n)/3 gives A005990.",
				"T(2n,n) gives A290023.",
				"Cf. A277537, A295027."
			],
			"keyword": "sign,tabl",
			"offset": "0,6",
			"author": "_Alois P. Heinz_, Oct 19 2016",
			"references": 6,
			"revision": 34,
			"time": "2021-10-25T14:17:37-04:00",
			"created": "2016-10-19T20:38:51-04:00"
		}
	]
}