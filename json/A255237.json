{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A255237",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 255237,
			"data": "1,2,1,0,1,-1,1,-1,0,1,0,-1,1,-2,0,1,0,0,-1,1,0,0,-1,0,1,-1,-1,0,1,2,0,-2,0,1,0,0,0,0,-1,1,-1,0,-1,0,1,0,0,0,0,0,-1,1,-2,0,2,0,-2,0,1,-1,-2,-1,1,1",
			"name": "Array of conversion coefficients for the minimal polynomials C of 2 cos(Pi/n) in terms of Chebyshev's S-polynomials.",
			"comment": [
				"The row length sequence is 1, 1 +  A055034(n), n \u003e= 1.",
				"For the minimal polynomial C(n, x) of the algebraic number rho(n) := 2*cos(Pi/n) (the length ratio of the smallest diagonal and the side of a regular n-gon) see the coefficient array in A187360. The coefficient triangle of Chebyshev's S-polynomials is given in A049310.",
				"The conversion is C(n, x) = sum(T(n, m)*S(m, x), m=0..delta(n)), for n \u003e= 0 with C(0, x) := 1 (undefined product), delta(0) = 0 and delta(n) = A055034(n), n \u003e= 1.",
				"Originally Ahmet Zahid KÜÇÜK observed the structure for prime n. The precise formula for odd primes prime(n) = A000040(n), n \u003e= 2, is C(prime(n), x) = S((prime(n)-1)/2, x) - S((prime(n)-3)/2, x).",
				"This is equivalent to  C(prime(n),x) = (-1)^((p(n)-1)/2)*S(prime(n)-1,I*sqrt(x-2)), with I^2 = -1.",
				"  Proof: The known identity S(n, x) - S(n-1, x) = (-1)^n*S(2*n, I*sqrt(x-2)) (from bisection). The degrees of the monic polynomials of both sides match, as do the known zeros.",
				"The row sums give 1, 3, 1, 0, 0, 0, -1, 0, 0, -1, 1, 0, -1, 0, -1, -2, 0, 0, -1, 0 ...",
				"The alternating row sums give 1, 1, -1, -2, 0, 2, -1, -2, 0, -1, 1, -2, -1, 2, -1, 0, 0, 2, -1, -2, ...",
				"For the reverse problem, the factorization of S polynomials into C polynomials see a Apr 12 2018 comment in A049310. - _Wolfdieter Lang_, Apr 12 2018"
			],
			"formula": [
				"The conversion is C(n, x) = sum(T(n, m)*S(m, x), m = 0..delta(n)), that is",
				"  T(n, m) = [S(m, x)] C(n, x), n \u003e= 0, m = 0, ..., delta(n), with C(0, x) := 1, delta(0) = 0 and delta(n) = A055034(n), n \u003e= 1. For the C and S polynomials see A187360 and A049310, respectively.",
				"For  n \u003e= 2: T(prime(n), (prime(n) -1)/2) = +1, T(prime(n), (prime(n) -3)/2) = -1 and T(prime(n), m) = 0 otherwise."
			],
			"example": [
				"The array T(n, m) begins:",
				"n\\m   0  1  2  3  4  5  6 ...",
				"0:    1",
				"1:    2  1",
				"2:    0  1",
				"3:   -1  1",
				"4:   -1  0  1",
				"5:    0 -1  1",
				"6:   -2  0  1",
				"7:    0  0 -1  1",
				"8:    0  0 -1  0  1",
				"9:   -1 -1  0  1",
				"10:   2  0 -2  0  1",
				"11:   0  0  0  0 -1  1",
				"12:  -1  0 -1  0  1",
				"13:   0  0  0  0  0 -1  1",
				"14:  -2  0  2  0 -2  0  1",
				"15:  -1 -2 -1  1  1",
				"...",
				"n=0: C(0, x) = 1 = 1*S(0, x),",
				"n=1: C(1, x) = 2 + x = 2*S(0, x) + 1*S(1, x),",
				"n=2: C(2, x) = x = 0*S(0, x) + 1*S(1, x),",
				"n=3: C(3, x) = -1 + x = -1*S(0, x) + 1*S(1, x),",
				"n=4: C(4, x) = -2 + x^2 = -1*S(0, x) + 0 + 1*S(2, x) = -1 + (-1 + x^2), ..."
			],
			"xref": [
				"Cf. A187360, A055034, A049310."
			],
			"keyword": "sign,easy,tabf",
			"offset": "0,2",
			"author": "Ahmet Zahid KÜÇÜK and _Wolfdieter Lang_, Mar 11 2015",
			"references": 2,
			"revision": 18,
			"time": "2018-04-12T14:27:38-04:00",
			"created": "2015-03-15T01:17:32-04:00"
		}
	]
}