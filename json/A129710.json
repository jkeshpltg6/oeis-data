{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A129710",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 129710,
			"data": "1,2,2,1,2,3,2,5,1,2,7,4,2,9,9,1,2,11,16,5,2,13,25,14,1,2,15,36,30,6,2,17,49,55,20,1,2,19,64,91,50,7,2,21,81,140,105,27,1,2,23,100,204,196,77,8,2,25,121,285,336,182,35,1,2,27,144,385,540,378,112,9,2,29,169,506",
			"name": "Triangle read by rows: T(n,k) is the number of Fibonacci binary words of length n and having k 01 subwords (0 \u003c= k \u003c= floor(n/2)). A Fibonacci binary word is a binary word having no 00 subword.",
			"comment": [
				"Also number of Fibonacci binary words of length n and having k 10 subwords.",
				"Row n has 1+floor(n/2) terms.",
				"Row sums are the Fibonacci numbers (A000045).",
				"T(n,0)=2 for n \u003e= 1.",
				"Sum_{k\u003e=0} k*T(n,k) = A023610(n-2).",
				"Triangle, with zeros omitted, given by (2, -1, 0, 0, 0, 0, 0, 0, 0, ...) DELTA (0, 1/2, -1/2, 0, 0, 0, 0, 0, 0, 0, ...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, Jan 14 2012",
				"Riordan array ((1+x)/(1-x), x^2/(1-x)), zeros omitted. - _Philippe Deléham_, Jan 14 2012"
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A129710/b129710.txt\"\u003eTable of n, a(n) for n = 0..10200\u003c/a\u003e (rows 0 \u003c= n \u003c= 200, flattened.)",
				"Thomas Grubb and Frederick Rajasekaran, \u003ca href=\"https://arxiv.org/abs/2009.00650\"\u003eSet Partition Patterns and the Dimension Index\u003c/a\u003e, arXiv:2009.00650 [math.CO], 2020. Mentions this sequence."
			],
			"formula": [
				"T(n,k) = binomial(n-k,k) + binomial(n-k-1,k) for n \u003e= 1 and 0 \u003c= k \u003c= floor(n/2).",
				"G.f. = G(t,z) = (1+z)/(1-z-tz^2).",
				"Sum_{k=0..n} T(n,k)*x^k = (-1)^n*A078050(n), A057079(n), A040000(n), A000045(n+2), A000079(n), A006138(n), A026597(n), A133407(n), A133467(n), A133469(n), A133479(n), A133558(n), A133577(n), A063092(n) for x = -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 respectively. - _Philippe Deléham_, Jan 14 2012",
				"T(n,k) = T(n-1,k) + T(n-2,k-1) with T(0,0)=1, T(1,0)=2, T(1,1)=0 and T(n,k) = 0 if k \u003e n or if k \u003c 0. - _Philippe Deléham_, Jan 14 2012"
			],
			"example": [
				"T(5,2)=4 because we have 10101, 01101, 01010 and 01011.",
				"Triangle starts:",
				"  1;",
				"  2;",
				"  2, 1;",
				"  2, 3;",
				"  2, 5, 1;",
				"  2, 7, 4;",
				"  2, 9, 9, 1;",
				"Triangle (2, -1, 0, 0, 0, 0, 0, ...) DELTA (0, 1/2, -1/2, 0, 0, 0, 0, 0, 0, ...) begins:",
				"  1;",
				"  2, 0;",
				"  2, 1, 0;",
				"  2, 3, 0, 0;",
				"  2, 5, 1, 0, 0;",
				"  2, 7, 4, 0, 0, 0;",
				"  2, 9, 9, 1, 0, 0, 0;"
			],
			"maple": [
				"T:=proc(n,k) if n=0 and k=0 then 1 elif k\u003c=floor(n/2) then binomial(n-k,k)+binomial(n-k-1,k) else 0 fi end: for n from 0 to 18 do seq(T(n,k),k=0..floor(n/2)) od; # yields sequence in triangular form"
			],
			"mathematica": [
				"MapAt[# - 1 \u0026, #, 1] \u0026@ Table[Binomial[n - k, k] + Binomial[n - k - 1, k], {n, 0, 16}, {k, 0, Floor[n/2]}] // Flatten (* _Michael De Vlieger_, Nov 15 2019 *)"
			],
			"xref": [
				"Cf. A000045, A023610.",
				"Cf. A029635, A029653.",
				"Columns: A040000, A005408, A000290, A000330, A002415, A005585, A040977, A050486, A053347, A054333, A054334, A057788."
			],
			"keyword": "nonn,tabf",
			"offset": "0,2",
			"author": "_Emeric Deutsch_, May 12 2007",
			"references": 2,
			"revision": 24,
			"time": "2020-09-03T12:19:07-04:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}