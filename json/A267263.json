{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A267263",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 267263,
			"data": "0,1,1,2,1,2,1,2,2,3,2,3,1,2,2,3,2,3,1,2,2,3,2,3,1,2,2,3,2,3,1,2,2,3,2,3,2,3,3,4,3,4,2,3,3,4,3,4,2,3,3,4,3,4,2,3,3,4,3,4,1,2,2,3,2,3,2,3,3,4,3,4,2,3,3,4,3,4,2,3,3,4,3,4,2,3,3,4,3,4,1,2",
			"name": "Number of nonzero digits in representation of n in primorial base.",
			"link": [
				"Cade Brown, \u003ca href=\"/A267263/b267263.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"\u003ca href=\"/index/Pri#primorialbase\"\u003eIndex entries for sequences related to primorial base\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A001221(A276086(n)). - _Antti Karttunen_, Aug 21 2016"
			],
			"example": [
				"a(3) = 2 because 3 written in primorial base is 11 with 2 nonzero digits."
			],
			"maple": [
				"a:= proc(n) local m, p, r; m, p, r:= n, 2, 0;",
				"       while m\u003e0 do r:= r+`if`(irem(m, p, 'm')\u003e0, 1, 0);",
				"                    p:= nextprime(p)",
				"       od; r",
				"    end:",
				"seq(a(n), n=0..100);  # _Alois P. Heinz_, Jan 15 2016"
			],
			"mathematica": [
				"Table[Length[IntegerDigits[n, MixedRadix@ Prime@ Reverse@ Range@ PrimePi@ n] /. 0 -\u003e Nothing], {n, 0, 120}] (* _Michael De Vlieger_, Jan 12 2016, Version 10.2 *)",
				"f[n_] := Block[{a = {{0, n}}}, Do[AppendTo[a, {First@ #, Last@ #} \u0026@ QuotientRemainder[a[[-1, -1]], Times @@ Prime@ Range[# - i]]], {i, 0, #}] \u0026@ NestWhile[# + 1 \u0026, 0, Times @@ Prime@ Range[# + 1] \u003c= n \u0026]; Rest[a][[All, 1]]]; Table[Count[f@ n, d_ /; d \u003e 0], {n, 0, 73}] (* _Michael De Vlieger_, Aug 29 2016 *)"
			],
			"program": [
				"(PARI) cnz(n) = my(d = digits(n)); sum(k=1, #d, d[k]!=0);",
				"A049345(n, p=2) = if(n\u003cp, n, A049345(n\\p, nextprime(p+1))*10 + n%p)",
				"a(n) = cnz(A049345(n)); \\\\ _Michel Marcus_, Jan 12 2016",
				"(PARI) a(n)=my(s); forprime(p=2,, if(n%p, s++, if(n==0, return(s))); n\\=p) \\\\ _Charles R Greathouse IV_, Nov 17 2016"
			],
			"xref": [
				"Cf. A001221, A002110, A049345, A235168, A276086.",
				"Cf. A060735 (positions of ones).",
				"A060130 is an analogous sequence for the factorial base, from which this differs for the first time at n=30, where a(30) = 1, while A060130(30) = 2."
			],
			"keyword": "nonn,base",
			"offset": "0,4",
			"author": "_Cade Brown_, Jan 12 2016",
			"references": 29,
			"revision": 70,
			"time": "2016-11-17T10:18:01-05:00",
			"created": "2016-01-15T15:40:34-05:00"
		}
	]
}