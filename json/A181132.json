{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A181132",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 181132,
			"data": "0,0,1,1,3,4,5,5,8,10,12,13,15,16,17,17,21,24,27,29,32,34,36,37,40,42,44,45,47,48,49,49,54,58,62,65,69,72,75,77,81,84,87,89,92,94,96,97,101,104,107,109,112,114,116,117,120,122,124,125,127,128,129,129,135,140,145",
			"name": "a(0)=0; thereafter a(n) = total number of 0's in binary expansions of 1, ..., n.",
			"comment": [
				"The graph of this sequence is a version of the Takagi curve: see Lagarias (2012), Section 9, especially Theorem 9.1. - _N. J. A. Sloane_, Mar 12 2016"
			],
			"link": [
				"Harvey P. Dale, \u003ca href=\"/A181132/b181132.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Hsien-Kuei Hwang, S. Janson, T.-H. Tsai, \u003ca href=\"http://140.109.74.92/hk/wp-content/files/2016/12/aat-hhrr-1.pdf\"\u003eExact and asymptotic solutions of the recurrence f(n) = f(floor(n/2)) + f(ceiling(n/2)) + g(n): theory and applications\u003c/a\u003e, Preprint 2016.",
				"Hsien-Kuei Hwang, S. Janson, T.-H. Tsai, \u003ca href=\"https://doi.org/10.1145/3127585\"\u003eExact and Asymptotic Solutions of a Divide-and-Conquer Recurrence Dividing at Half: Theory and Applications\u003c/a\u003e, ACM Transactions on Algorithms, 13:4 (2017), #47; DOI: 10.1145/3127585.",
				"Jeffrey C. Lagarias, \u003ca href=\"https://arxiv.org/abs/1112.4205\"\u003eThe Takagi function and its properties\u003c/a\u003e, arXiv:1112.4205 [math.CA], 2011-2012.",
				"Jeffrey C. Lagarias, \u003ca href=\"http://hdl.handle.net/2433/198081\"\u003eThe Takagi function and its properties\u003c/a\u003e, In Functions in number theory and their probabilistic aspects, 153--189, RIMS Kôkyûroku Bessatsu, B34, Res. Inst. Math. Sci. (RIMS), Kyoto, 2012. MR3014845."
			],
			"formula": [
				"a(n) = A059015(n)-1. - _Klaus Brockhaus_, Oct 08 2010",
				"a(0)=0; thereafter a(2n) = a(n)+a(n-1)+n, a(2n+1) = 2a(n)+n. G.f.: (1/(1-x)^2) * Sum_{k \u003e= 0} x^(2^(k+1))/(1+x^(2^k)). - _N. J. A. Sloane_, Mar 10 2016"
			],
			"mathematica": [
				"Accumulate[Count[IntegerDigits[#,2],0]\u0026/@Range[70]] (* _Harvey P. Dale_, May 16 2012 *)",
				"Join[{0},Accumulate[DigitCount[Range[70],2,0]]] (* _Harvey P. Dale_, Jun 09 2016 *)"
			],
			"program": [
				"(Other) microsoft word macro: Sub concatcount() Dim base As Integer Dim digit As Integer Dim counter As Integer Dim standin As Integer Dim max As Integer Let base = 2 Let digit = 0 Let max = 100 Let counter = 0 For n = 1 To max Let standin = n Do While standin \u003e 0 If standin Mod base = digit Then Let counter = counter + 1 Let standin = standin - (standin Mod base) If standin \u003e 0 Then Let standin = standin / base Loop Selection.TypeText Text:=Str(counter) Next n End Sub",
				"(MAGMA) [ n eq 1 select 0 else Self(n-1)+(#B-\u0026+B) where B is Intseq(n, 2): n in [1..70] ]; // _Klaus Brockhaus_, Oct 08 2010",
				"(PARI) a(n)=my(m=logint(n,2)); 1 + (m+1)*(n+1) - 2^(m+1) + sum(j=1,m+1,my(t=floor(n/2^j + 1/2));(n\u003e\u003ej)*(2*n + 2 - (1 + (n\u003e\u003ej))\u003c\u003cj) - (2*n + 2 - t\u003c\u003cj)*t)/2 \\\\ _Charles R Greathouse IV_, Dec 14 2015"
			],
			"xref": [
				"Cf. A000120, A023416, A000788, A059015, A268289."
			],
			"keyword": "base,easy,nonn",
			"offset": "0,5",
			"author": "_Dylan Hamilton_, Oct 05 2010",
			"ext": [
				"a(0)=0 added by _N. J. A. Sloane_, Mar 10 2016 (simplifies the recurrence, makes entry consistent with A059015 and other closely related sequences)."
			],
			"references": 2,
			"revision": 46,
			"time": "2019-02-04T01:25:12-05:00",
			"created": "2010-10-20T03:00:00-04:00"
		}
	]
}