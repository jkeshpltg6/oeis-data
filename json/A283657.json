{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A283657",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 283657,
			"data": "0,1,2,3,4,5,6,7,8,9,10,11,12,13,16,17,19,20,23,28,31,32,40,43,61,64,79,92,101,104,127,128,148,167,191,199,256,313,347,356,596,692,701,1004,1228,1268,1709,2617,3539,3824,5807,10501,10691,11279,12391,14479",
			"name": "Numbers m such that 2^m + 1 has at most 2 distinct prime factors.",
			"comment": [
				"Using comment in A283364, note that if a(n) is odd \u003e 9, then it is prime.",
				"503 \u003c= a(41) \u003c= 596. - _Robert Israel_, Mar 13 2017",
				"Could (4^p + 1)/5^t be prime, where p is prime, 5^t is the highest power of 5 dividing 4^p + 1, other than for p=2, 3 and 5? - _Vladimir Shevelev_, Mar 14 2017",
				"In his message to seqfans from Mar 15 2017, _Jack Brennen_ beautifully proved that there are no more primes of such form. From his proof one can see also that there are no terms of the form 2*p \u003e 10 in the sequence. - _Vladimir Shevelev_, Mar 15 2017",
				"Where A046799(n)=2. - _Robert G. Wilson v_, Mar 15 2017",
				"From _Giuseppe Coppoletta_, May 16 2017: (Start)",
				"The only terms that are not in A066263 are those m giving 2^m + 1 = prime (i.e. m = 0 and any number m such that 2^m + 1 is a Fermat prime) and the values of m giving 2^m + 1 = power of a prime, giving m = 3 as the only possible case (by Mihăilescu-Catalan's result, see links).",
				"For the relation with Fermat numbers and for other possible terms to check, see comments in A073936 and A066263.",
				"All terms after a(59) refer to probabilistic primality tests for 2^a(n) + 1  (see Caldwell's link for the list of the largest certified Wagstaff primes).",
				"After a(65), the values 267017, 269987, 374321, 986191, 4031399 and 4101572 are also terms, but there still remains the remote possibility of some gaps in between. In addition, 13347311 and 13372531 are also terms, but possibly much further along in the numbering (see comments in A000978).",
				"(End)."
			],
			"link": [
				"Giuseppe Coppoletta, \u003ca href=\"/A283657/b283657.txt\"\u003eTable of n, a(n) for n = 1..65\u003c/a\u003e",
				"Jack Brennen, \u003ca href=\"http://list.seqfan.eu/pipermail/seqfan/2017-March/017363.html\"\u003ePrimes of the form (4^p+1)/5^t, Seqfan (Mar 15 2017)\u003c/a\u003e.",
				"C. Caldwell's The Top Twenty \u003ca href=\"http://primes.utm.edu/top20/page.php?id=67\"\u003eWagstaff primes\u003c/a\u003e.",
				"Mersennewiki, \u003ca href=\"http://mersennewiki.org/index.php/2_Plus_Tables\"\u003eFactorizations Of Cunningham Numbers C+(2,n) (tables)\u003c/a\u003e.",
				"Samuel S. Wagstaff, \u003ca href=\"http://www.cerias.purdue.edu/homes/ssw/cun/index.html\"\u003eThe Cunningham Project\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CatalansConjecture.html\"\u003eCatalan's Conjecture\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ZsigmondyTheorem.html\"\u003eZsigmondy Theorem\u003c/a\u003e."
			],
			"example": [
				"0 is a term as 2^0 + 1 = 2 is a prime.",
				"10 is a term as 2^10 + 1 = 5^2 * 41.",
				"14 is not a term as 2^14 + 1 = 5 * 29 * 113."
			],
			"maple": [
				"# this uses A002587[i] for i\u003c=500, e.g., from the b-file for that sequence",
				"count:= 0:",
				"for i from 0 to 500 do",
				"  m:= 0;",
				"  r:= (2^i+1);",
				"  if i::odd then",
				"    m:= 1;",
				"    r:= r/3^padic:-ordp(r,3);",
				"  elif i \u003e 2 then",
				"    q:= max(numtheory:-factorset(i));",
				"    if q \u003e 2 then",
				"      m:= 1;",
				"      r:= r/B[i/q]^padic:-ordp(r,A002587[i/q]);",
				"    fi",
				"  fi;",
				"  if r mod B[i] = 0 then m:= m+1;",
				"      j:= padic:-ordp(r, A002587[i]);",
				"      r:= r/B[i]^j;",
				"  fi;",
				"  mmax:= m;",
				"  if isprime(r) then m:= m+1; mmax:= m",
				"  elif r \u003e 1 then mmax:= m+2",
				"  fi;",
				"  if mmax \u003c= 2 or (m \u003c= 1 and m + nops(numtheory:-factorset(r)) \u003c= 2) then",
				"       count:= count+1;",
				"     A[count]:= i;",
				"  fi",
				"od:",
				"seq(A[i],i=1..count); # _Robert Israel_, Mar 13 2017"
			],
			"mathematica": [
				"Select[Range[0, 313], PrimeNu[2^# + 1]\u003c3 \u0026] (* _Indranil Ghosh_, Mar 13 2017 *)"
			],
			"program": [
				"(PARI) for(n=0, 313, if(omega(2^n + 1)\u003c3, print1(n,\", \"))) \\\\ _Indranil Ghosh_, Mar 13 2017"
			],
			"xref": [
				"Cf. A002587, A046799, A283364, A073936, A000978, A127317, A092559, A019434, A066263.",
				"Contains 4*A057182."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Vladimir Shevelev_, Mar 13 2017",
			"ext": [
				"a(16)-a(38) from _Peter J. C. Moses_, Mar 13 2017",
				"a(39)-a(40) from _Robert Israel_, Mar 13 2017",
				"a(41)-a(65) from _Giuseppe Coppoletta_, May 08 2017"
			],
			"references": 3,
			"revision": 64,
			"time": "2018-02-11T10:22:53-05:00",
			"created": "2017-03-24T22:09:49-04:00"
		}
	]
}