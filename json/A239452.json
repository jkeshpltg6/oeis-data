{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A239452",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 239452,
			"data": "2,2,2,4,2,3,2,8,8,5,2,4,2,7,4,16,2,9,2,5,6,11,2,9,7,13,26,4,2,6,2,32,10,17,6,9,2,19,12,16,2,7,2,12,8,23,2,16,18,25,16,9,2,27,10,8,18,29,2,16,2,31,8,64,5,3,2,17,22,11,2,9,2,37,24,20,21",
			"name": "Smallest integer m \u003e 1 such that m^n == m (mod n).",
			"comment": [
				"Composite n are Fermat weak pseudoprimes to base a(n).",
				"If n \u003e 2 is prime then a(n) = 2. The converse is false : a(341) = 2 and 341 isn't prime.",
				"a(n) \u003c= A105222(n). a(n) = A105222(n) if and only if a(n) is coprime to n.",
				"For n \u003e 1, a(n) \u003c= n and if a(n) = n, then A105222(n) = n+1.",
				"It seems that a(n) = n if and only if n = 2^k with k \u003e 0, a(n) = n-1 if and only if n = 3^k with k \u003e 0, a(2n) = n if and only if n = p^k where p is an odd prime and k \u003e 0. - _Thomas Ordowski_, Oct 19 2017"
			],
			"link": [
				"Giovanni Resta, \u003ca href=\"/A239452/b239452.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Gérard P. Michon, \u003ca href=\"http://www.numericana.com/answer/pseudo.htm#weak\"\u003eWeak pseudoprimes to base a\u003c/a\u003e"
			],
			"example": [
				"We have 2^4 != 2, 3^4 != 3, but 4^4 == 4 (mod 4), so a(4) = 4."
			],
			"maple": [
				"L:=NULL:for n to 100 do for a from 2 while a^n - a mod n !=0 do od; L:=L,a od: L;"
			],
			"mathematica": [
				"a[n_] := Block[{m = 2}, While[PowerMod[m, n, n] != Mod[m, n], m++]; m]; Array[a, 100] (* _Giovanni Resta_, Mar 19 2014 *)"
			],
			"program": [
				"(Haskell)",
				"import Math.NumberTheory.Moduli (powerMod)",
				"a239452 n = head [m | m \u003c- [2..], powerMod m n n == mod m n]",
				"-- _Reinhard Zumkeller_, Mar 19 2014",
				"(Python)",
				"L=[];",
				"for n in range(1,101):",
				"...a=2",
				"...while (a**n - a) % n != 0:",
				"......a+=1",
				"...L=L+[a]",
				"L",
				"(PARI) a(n)=my(m=2); while(Mod(m,n)^n!=m, m++); m \\\\ _Charles R Greathouse IV_, Mar 21 2014"
			],
			"xref": [
				"Cf. A105222."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Robert FERREOL_, Mar 19 2014",
			"ext": [
				"a(20)-a(77) from _Giovanni Resta_, Mar 19 2014"
			],
			"references": 4,
			"revision": 38,
			"time": "2017-11-16T02:42:50-05:00",
			"created": "2014-03-19T17:29:46-04:00"
		}
	]
}