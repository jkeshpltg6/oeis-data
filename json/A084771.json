{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A084771",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 84771,
			"data": "1,5,33,245,1921,15525,127905,1067925,9004545,76499525,653808673,5614995765,48416454529,418895174885,3634723102113,31616937184725,275621102802945,2407331941640325,21061836725455905,184550106298084725",
			"name": "Coefficients of expansion of 1/sqrt(1 - 10*x + 9*x^2); also, a(n) is the central coefficient of (1 + 5*x + 4*x^2)^n.",
			"comment": [
				"Also number of paths from (0,0) to (n,0) using steps U=(1,1), H=(1,0) and D=(1,-1), the U steps come in four colors and the H steps come in five colors. - _N-E. Fahssi_, Mar 30 2008",
				"Number of lattice paths from (0,0) to (n,n) using steps (1,0), (0,1), and three kinds of steps (1,1). - _Joerg Arndt_, Jul 01 2011",
				"Sums of squares of coefficients of (1+2*x)^n. - _Joerg Arndt_, Jul 06 2011",
				"The Hankel transform of this sequence gives A103488. - _Philippe Deléham_, Dec 02 2007",
				"Partial sums of A085363. - _J. M. Bergot_, Jun 12 2013",
				"Diagonal of rational functions 1/(1 - x - y - 3*x*y), 1/(1 - x - y*z - 3*x*y*z). - _Gheorghe Coserea_, Jul 06 2018"
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A084771/b084771.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e (terms 0..200 from Vincenzo Librandi)",
				"T. Amdeberhan, \u003ca href=\"http://mathoverflow.net/questions/271037/\"\u003eIn search of multiple expressions for a sequence\u003c/a\u003e",
				"Paul Barry and Aoife Hennessy, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL15/Barry2/barry190r.html\"\u003eGeneralized Narayana Polynomials, Riordan Arrays, and Lattice Paths\u003c/a\u003e, Journal of Integer Sequences, Vol. 15, 2012, #12.4.8.",
				"Hacène Belbachir and Abdelghani Mehdaoui, \u003ca href=\"https://doi.org/10.2989/16073606.2020.1729269\"\u003eRecurrence relation associated with the sums of square binomial coefficients\u003c/a\u003e, Quaestiones Mathematicae (2021) Vol. 44, Issue 5, 615-624.",
				"Hacène Belbachir, Abdelghani Mehdaoui, and László Szalay, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL22/Szalay/szalay42.html\"\u003eDiagonal Sums in the Pascal Pyramid, II: Applications\u003c/a\u003e, J. Int. Seq., Vol. 22 (2019), Article 19.3.5.",
				"Curtis Greene, \u003ca href=\"https://doi.org/10.1016/0097-3165(88)90018-0\"\u003ePosets of shuffles\u003c/a\u003e, Journal of Combinatorial Theory, Series A 47.2 (1988): 191-206. See Eq. (30).",
				"Christopher Huffaker, Nathan McCue, Cameron N. Miller, and Kayla S. Miller, \u003ca href=\"http://arxiv.org/abs/1508.06542\"\u003eThe M\u0026M Game: From Morsels to Modern Mathematics\u003c/a\u003e, arXiv:1508.06542 [math.HO], 2015.",
				"Tony D. Noe, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL9/Noe/noe35.html\"\u003eOn the Divisibility of Generalized Central Trinomial Coefficients\u003c/a\u003e, Journal of Integer Sequences, Vol. 9 (2006), Article 06.2.7.",
				"Michael Z. Spivey and Laura L. Steil, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL9/Spivey/spivey7.html\"\u003eThe k-Binomial Transforms and the Hankel Transform\u003c/a\u003e, Journal of Integer Sequences, Vol. 9 (2006), Article 06.1.1."
			],
			"formula": [
				"G.f.: 1 / sqrt(1 - 10*x + 9*x^2).",
				"Binomial transform of A059304. G.f.: Sum_{k\u003e=0} binomial(2*k, k)*(2*x)^k/(1-x)^(k+1). E.g.f.: exp(5*x)*BesselI(0, 4*x). - _Vladeta Jovovic_, Aug 20 2003",
				"a(n) = sum(k=0..n, sum(j=0..n-k, C(n,j)*C(n-j,k)*C(2*n-2*j,n-j) ) ). - _Paul Barry_, May 19 2006",
				"a(n) = sum(k=0..n, 4^k*(C(n,k))^2 ). - heruneedollar (heruneedollar(AT)gmail.com), Mar 20 2010",
				"a(n) ~ 3^(2*n+1)/(2*sqrt(2*Pi*n)). - _Vaclav Kotesovec_, Sep 11 2012",
				"D-finite with recurrence: n*a(n) + 5*(-2*n+1)*a(n-1) + 9*(n-1)*a(n-2) = 0. - _R. J. Mathar_, Nov 26 2012",
				"a(n) = hypergeom([-n,1/2], [1], -8). - _Peter Luschny_, Apr 26 2016",
				"From _Michael Somos_, Jun 01 2017: (Start)",
				"a(n) = -3 * 9^n * a(-1-n) for all n in Z.",
				"0 = a(n)*(+81*a(n+1) -135*a(n+2) +18*a(n+3)) +a(n+1)*(-45*a(n+1) +100*a(n+2) -15*a(n+3)) +a(n+2)*(-5*a(n+2) +a(n+3)) for all n in Z. (End)"
			],
			"example": [
				"G.f.: 1/sqrt(1-2*b*x+(b^2-4*c)*x^2) yields central coefficients of (1+b*x+c*x^2)^n."
			],
			"maple": [
				"seq(simplify(hypergeom([-n,1/2], [1], -8)),n=0..19); # _Peter Luschny_, Apr 26 2016"
			],
			"mathematica": [
				"Table[n! SeriesCoefficient[E^(5 x) BesselI[0, 4 x], {x, 0, n}], {n, 0, 30}] (* _Vincenzo Librandi_, May 10 2013 *)",
				"Table[Hypergeometric2F1[-n, -n, 1, 4], {n, 0, 19}] (* _Vladimir Reshetnikov_, Nov 29 2013 *)",
				"CoefficientList[Series[1/Sqrt[1-10x+9x^2],{x,0,30}],x] (* _Harvey P. Dale_, Mar 08 2016 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, -3 * 9^n * a(-1-n), sum(k=0,n, binomial(n, k)^2 * 4^k))}; /* _Michael Somos_, Oct 08 2003 */",
				"(PARI) {a(n) = if( n\u003c0, -3 * 9^n * a(-1-n), polcoeff((1 + 5*x + 4*x^2)^n, n))}; /* _Michael Somos_, Oct 08 2003 */",
				"(PARI) /* as lattice paths: same as in A092566 but use */",
				"steps=[[1,0], [0,1], [1,1], [1,1], [1,1]]; /* note the triple [1,1] */",
				"/* _Joerg Arndt_, Jul 01 2011 */",
				"(PARI) a(n)={local(v=Vec((1+2*x)^n));sum(k=1,#v,v[k]^2);} /* _Joerg Arndt_, Jul 06 2011 */",
				"(PARI) a(n)={local(v=Vec((1+2*I*x)^n)); sum(k=1,#v, real(v[k])^2+imag(v[k])^2);} /* _Joerg Arndt_, Jul 06 2011 */",
				"(GAP) List([0..20],n-\u003eSum([0..n],k-\u003eBinomial(n,k)^2*4^k)); # _Muniru A Asiru_, Jul 29 2018"
			],
			"xref": [
				"Cf. A001850, A246923 (a(n)^2)."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Paul D. Hanna_, Jun 10 2003",
			"references": 17,
			"revision": 91,
			"time": "2021-11-07T09:51:43-05:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}