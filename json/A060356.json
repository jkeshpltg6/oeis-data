{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A060356",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 60356,
			"data": "0,1,0,3,4,65,306,4207,38424,573057,7753510,134046671,2353898196,47602871329,1013794852266,23751106404495,590663769125296,15806094859299329,448284980183376078,13515502344669830287",
			"name": "Expansion of e.g.f.: -LambertW(-x/(1+x)).",
			"comment": [
				"Also the number of labeled lone-child-avoiding rooted trees with n nodes. A rooted tree is lone-child-avoiding if it has no unary branchings, meaning every non-leaf node covers at least two other nodes. The unlabeled version is A001678(n + 1). - _Gus Wiseman_, Jan 20 2020"
			],
			"link": [
				"Harry J. Smith, \u003ca href=\"/A060356/b060356.txt\"\u003eTable of n, a(n) for n = 0..100\u003c/a\u003e",
				"David Callan, \u003ca href=\"http://arxiv.org/abs/1406.7784\"\u003eA sign-reversing involution to count labeled lone-child-avoiding trees\u003c/a\u003e, arXiv:1406.7784 [math.CO], (30-June-2014).",
				"Gus Wiseman, \u003ca href=\"https://docs.google.com/document/d/e/2PACX-1vS1zCO9fgAIe5rGiAhTtlrOTuqsmuPos2zkeFPYB80gNzLb44ufqIqksTB4uM9SIpwlvo-oOHhepywy/pub\"\u003eSequences counting series-reduced and lone-child-avoiding trees by number of vertices.\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Sum_{k=1..n} (-1)^(n-k)*n!/k!*binomial(n-1, k-1)*k^(k-1). a(n) = Sum_{k=0..n} Stirling1(n, k)*A058863(k). - _Vladeta Jovovic_, Sep 17 2003",
				"a(n) ~ n^(n-1) * (1-exp(-1))^(n+1/2). - _Vaclav Kotesovec_, Nov 27 2012",
				"a(n) = n * A108919(n). - _Gus Wiseman_, Dec 31 2019"
			],
			"example": [
				"From _Gus Wiseman_, Dec 31 2019: (Start)",
				"Non-isomorphic representatives of the a(7) = 4207 trees, written as root[branches], are:",
				"  1[2,3[4,5[6,7]]]",
				"  1[2,3[4,5,6,7]]",
				"  1[2[3,4],5[6,7]]",
				"  1[2,3,4[5,6,7]]",
				"  1[2,3,4,5[6,7]]",
				"  1[2,3,4,5,6,7]",
				"(End)"
			],
			"maple": [
				"seq(coeff(series( -LambertW(-x/(1+x)), x, n+1), x, n)*n!, n = 0..20); # _G. C. Greubel_, Mar 16 2020"
			],
			"mathematica": [
				"CoefficientList[Series[-LambertW[-x/(1+x)], {x, 0, 20}], x]* Range[0, 20]! (* _Vaclav Kotesovec_, Nov 27 2012 *)",
				"sps[{}]:={{}};sps[set:{i_,___}]:=Join@@Function[s,Prepend[#,s]\u0026/@sps[Complement[set,s]]]/@Cases[Subsets[set],{i,___}];",
				"a[n_]:=If[n==1,1,n*Sum[Times@@a/@Length/@stn,{stn,Select[sps[Range[n-1]],Length[#]\u003e1\u0026]}]];",
				"Array[a,10] (* _Gus Wiseman_, Dec 31 2019 *)"
			],
			"program": [
				"(PARI) { for (n=0, 100, f=n!; a=sum(k=1, n, (-1)^(n - k)*f/k!*binomial(n - 1, k - 1)*k^(k - 1)); write(\"b060356.txt\", n, \" \", a); ) } \\\\ _Harry J. Smith_, Jul 04 2009",
				"(PARI) my(x='x+O('x^20)); concat([0], Vec(serlaplace(-lambertw(-x/(1+x))))) \\\\ _G. C. Greubel_, Feb 19 2018",
				"(GAP) List([0..20],n-\u003eSum([1..n],k-\u003e(-1)^(n-k)*Factorial(n)/Factorial(k) *Binomial(n-1,k-1)*k^(k-1))); # _Muniru A Asiru_, Feb 19 2018"
			],
			"xref": [
				"Cf. A052871, A060313.",
				"Cf. A008297.",
				"Column k=0 of A231602.",
				"The unlabeled version is A001678(n + 1).",
				"The case where the root is fixed is A108919.",
				"Unlabeled rooted trees are counted by A000081.",
				"Lone-child-avoiding rooted trees with labeled leaves are A000311.",
				"Matula-Goebel numbers of lone-child-avoiding rooted trees are A291636.",
				"Singleton-reduced rooted trees are counted by A330951.",
				"Cf. A000669, A004111, A005121, A048816, A292504, A316651, A316652, A318231, A318813, A330465, A330624."
			],
			"keyword": "easy,nonn",
			"offset": "0,4",
			"author": "_Vladeta Jovovic_, Apr 01 2001",
			"references": 29,
			"revision": 33,
			"time": "2020-04-02T03:20:32-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}