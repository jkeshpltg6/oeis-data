{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A286783",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 286783,
			"data": "1,3,15,5,105,77,7,945,1044,234,9,10395,14784,5390,550,11,135135,227877,113126,19760,1105,13,2027025,3862305,2371845,586425,58275,1995,15,34459425,71983440,51607716,16271380,2356234,147560,3332,17,654729075,1469813400,1185214452,446964322,84487110,7888876,333564,5244,19,13749310575,32718512925,28937407212,12516198870,2884205268,358182846,23006928,690480,7875,21",
			"name": "Triangle T(n,k) read by rows: coefficients of polynomials P_n(t) defined in Formula section.",
			"comment": [
				"Row n\u003e0 contains n terms.",
				"T(n,k) is the number of Feynman's diagrams with k fermionic loops in the order n of the perturbative expansion in dimension zero for the polarization function in a many-body theory of fermions with two-body interaction (see Molinari link)."
			],
			"link": [
				"Gheorghe Coserea, \u003ca href=\"/A286783/b286783.txt\"\u003eRows n=0..123, flattened\u003c/a\u003e",
				"Luca G. Molinari, \u003ca href=\"https://arxiv.org/abs/cond-mat/0401500\"\u003eHedin's equations and enumeration of Feynman's diagrams\u003c/a\u003e, arXiv:cond-mat/0401500 [cond-mat.str-el], 2005."
			],
			"formula": [
				"A(x;t) = Sum_{n\u003e=0} P_n(t)*x^n = (1 + x*s + 2*x^2*deriv(s,x))/(1-x*s)^2, where s(x;t) = A286781(x;t) and P_n(t) = Sum_{k=0..n-1} T(n,k)*t^k for n\u003e0.",
				"A001147(n+1)=T(n,0), A001700(n)=P_n(-1), A286794(n)=P_n(1)."
			],
			"example": [
				"A(x;t) = 1 + 3*x + (15 + 5*t)*x^2 + (105 + 77*t + 7*t^2)*x^3 + ...",
				"Triangle starts:",
				"n\\k  [0]       [1]       [2]       [3]       [4]      [5]     [6]   [7]",
				"[0]  1;",
				"[1]  3;",
				"[2]  15,       5;",
				"[3]  105,      77,       7;",
				"[4]  945,      1044,     234,      9;",
				"[5]  10395,    14784,    5390,     550,      11;",
				"[6]  135135,   227877,   113126,   19760,    1105,    13;",
				"[7]  2027025,  3862305,  2371845,  586425,   58275,   1995,   15;",
				"[8]  34459425, 71983440, 51607716, 16271380, 2356234, 147560, 3332, 17;",
				"[9]  ..."
			],
			"mathematica": [
				"max = 11; y0[x_, t_] = 1; y1[x_, t_] = 0; For[n = 1, n \u003c= max, n++, y1[x_, t_] = (1 + x*y0[x, t] + 2*x^2*D[y0[x, t], x])*(1 - x*y0[x, t]*(1 - t))/(1 - x*y0[x, t])^2 + O[x]^n // Normal; y0[x_, t_] = y1[x, t] // Simplify];",
				"s = y0[x, t];",
				"se = (1 + x*s + 2*x^2*D[s, x])/(1 - x*s)^2  + O[x]^max // Normal;",
				"row[n_] := row[n] = CoefficientList[Coefficient[se, x, n], t];",
				"T[0, 0] = 1; T[n_, k_] := row[n][[k + 1]];",
				"Table[T[n, k], {n, 0, max-1}, {k, 0, If[n == 0, 0, n-1]}] // Flatten (* _Jean-François Alcover_, May 19 2017, adapted from PARI *)"
			],
			"program": [
				"(PARI)",
				"A286781_ser(N,t='t) = {",
				"  my(x='x+O('x^N), y0=1+O('x^N), y1=0, n=1);",
				"  while(n++,",
				"    y1 = (1 + x*y0 + 2*x^2*y0')*(1 - x*y0*(1-t))/(1-x*y0)^2;",
				"    if (y1 == y0, break()); y0 = y1;);",
				"  y0;",
				"};",
				"A286783_ser(N,t='t) = {",
				"  my(s=A286781_ser(N,t)); (1 + x*s + 2*x^2*deriv(s,'x))/(1-x*s)^2;",
				"};",
				"concat(apply(p-\u003eVecrev(p), Vec(A286783_ser(10))))"
			],
			"keyword": "nonn,tabf",
			"offset": "0,2",
			"author": "_Gheorghe Coserea_, May 14 2017",
			"references": 12,
			"revision": 29,
			"time": "2017-05-19T11:41:06-04:00",
			"created": "2017-05-17T22:52:10-04:00"
		}
	]
}