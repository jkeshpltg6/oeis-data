{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A261693",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 261693,
			"data": "0,1,3,1,7,5,3,1,15,13,11,9,7,5,3,1,31,29,27,25,23,21,19,17,15,13,11,9,7,5,3,1,63,61,59,57,55,53,51,49,47,45,43,41,39,37,35,33,31,29,27,25,23,21,19,17,15,13,11,9,7,5,3,1,127,125,123,121,119,117,115,113,111,109,107,105,103,101,99,97,95,93",
			"name": "Irregular triangle read by rows in which row n lists the positive odd numbers in decreasing order starting with 2^n - 1. T(0, 1) = 0 and T(n, k) for n \u003e= 1, 1 \u003c= k \u003c= 2^(n-1).",
			"comment": [
				"Also the first differences of A261692.",
				"Number of cells turned ON at n-th stage of the cellular automaton of A261692.",
				"This irregular triangle A (instead of T) appears also in the linearization of the following product of Chebyshev T polynomials (A053120): PrT(n) := Product_{j=1..n} T(2^j, x)  = (1/2^(n-1))*Sum_{k=1..2^(n-1)} T(2*A(n, k), x), for n \u003e= 1. Proof via 2*T(n, x)*T(m, x) = T(n+m, x) + T(|n-m|, x). - _Wolfdieter Lang_, Oct 26 2019"
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/wiki/Catalog_of_Toothpick_and_CA_Sequences_in_OEIS\"\u003eCatalog of Toothpick and Cellular Automata Sequences in the OEIS\u003c/a\u003e",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = 2^n + 1 - 2*k, n \u003e= 1, 1 \u003c= k \u003c= 2^(n-1), and T(0, 0) = 0.",
				"As a sequence: a(n) = A262621(n)/4, n \u003e= 1, and a(0) = 0."
			],
			"example": [
				"With the terms written as an irregular triangle T in which row lengths are the terms of A011782 the sequence begins:",
				"0;",
				"1;",
				"3, 1;",
				"7, 5, 3, 1;",
				"15, 13, 11, 9, 7, 5, 3, 1;",
				"31, 29, 27, 25, 23, 21, 19, 17, 15, 13, 11, 9, 7, 5, 3, 1;",
				"...",
				"-------------------------------------------------------------------------------",
				"From _Wolfdieter Lang_, Oct 26 2019: (Start)",
				"Chebyshev T(2^j)-products (the argument x is here omitted):",
				"n = 1: T(2) = (2^0)*T(2*1),",
				"n = 2: T(2)*T(4) = (1/2)*(T(2*3) + T(2*1)) = (T(6) + T(2))/2,",
				"n = 3: T(2)*T(4)*T(8) =  (1/2^2)*(T(2*7) + T(2*5) + T(2*3) + T(2*1))",
				"       = (T(14) + T(10) + T(6) + T(2))/4.",
				"... (End)"
			],
			"maple": [
				"A261693 := n -\u003e Bits:-Nor(2*n, 2*n):",
				"seq(A261693(n), n=0..81); # _Peter Luschny_, Sep 23 2019"
			],
			"mathematica": [
				"Table[Reverse[2 Range[2^(n - 1)] - 1], {n, 0, 7}] /. {} -\u003e 0 // Flatten (* _Michael De Vlieger_, Oct 05 2015 *)"
			],
			"program": [
				"(PARI) tabf(nn) = {for (n=0, nn, print1(n, \":\"); for (k=1, 2^(n-2), print1(2^(n-1) - 2*k + 1, \", \");); print(););} \\\\ _Michel Marcus_, Oct 27 2015"
			],
			"xref": [
				"Cf. A036563, A005408, A011782, A080079, A099375, A147582, A262617, A261692, A262621.",
				"Column 1 is A000225. Row sums give A000302, n \u003e= 1."
			],
			"keyword": "nonn,easy,tabf",
			"offset": "0,3",
			"author": "_Omar E. Pol_, Sep 25 2015",
			"ext": [
				"Corrections by _Wolfdieter Lang_, Nov 15 2019"
			],
			"references": 5,
			"revision": 47,
			"time": "2019-11-15T13:55:42-05:00",
			"created": "2015-11-17T21:10:20-05:00"
		}
	]
}