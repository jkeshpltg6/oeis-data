{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A152874",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 152874,
			"data": "2,4,2,8,8,8,24,36,48,12,72,144,288,144,72,288,720,1728,1296,864,144,1152,3456,10368,10368,10368,3456,1152,5760,20160,69120,86400,103680,51840,23040,2880,28800,115200,460800,691200,1036800,691200,460800",
			"name": "Triangle read by rows: T(n,k) is the number of permutations of {1,2,...,n} with k parity changes (n\u003e=2; 1\u003c=k \u003c=n-1; the permutation 372185946 has 5 parity changes: 37-2-1-8-59-46.",
			"comment": [
				"Sum of entries in row n is n! (A000142(n)).",
				"T(n,n-1) = A092186(n).",
				"T(n,1) = A152875(n).",
				"Sum_{k=1..n-1} k*T(n,k) = 2*A077613(n)."
			],
			"formula": [
				"T(2n,k) = (n!)^2*a(n,k), where a(n,k) is the number of lattice paths from (0,0) to (n,n) with steps N=(0,1) and E=(1,0) and having k turns;",
				"a(n,k) = 2*binomial(n-1,k/2-1)*binomial(n-1,k/2) if k even;",
				"a(n,k) = 2*(binomial(n-1,(k-1)/2))^2 if k odd.",
				"T(2n+1,k) = n!*(n+1)!*b(n,k), where b(n,k) is the number of lattice paths from (0,0) to (n,n+1) with steps N=(0,1) and E=(1,0) and having k turns;",
				"b(n,k) = binomial(n,k/2)*binomial(n-1,k/2-1) + binomial(n,k/2-1)*binomial(n-1,k/2) = (binomial(n,k/2))^2*k(2n-k+1)/(n(2n-k+2)) if k even;",
				"b(n,k) = 2*binomial(n,(k-1)/2)*binomial(n-1,(k-1)/2) if k odd."
			],
			"example": [
				"T(4,3)=8 because we have 1243, 1423, 4132, 4312, 2134, 2314, 3241 and 3421.",
				"Triangle starts:",
				"   2;",
				"   4,   2;",
				"   8,   8,   8;",
				"  24,  36,  48,  12;",
				"  72, 144, 288, 144,  72;"
			],
			"maple": [
				"ae := proc (n, k) if `mod`(k, 2) = 0 then 2*factorial(n)^2*binomial(n-1, (1/2)*k-1)*binomial(n-1, (1/2)*k) else 2*factorial(n)^2*binomial(n-1, (1/2)*k-1/2)^2 end if end proc: ao := proc (n, k) if `mod`(k, 2) = 0 then factorial(n)*factorial(n+1)*(binomial(n, (1/2)*k)*binomial(n-1, (1/2)*k-1)+binomial(n, (1/2)*k-1)*binomial(n-1, (1/2)*k)) else 2*factorial(n)*factorial(n+1)*binomial(n, (1/2)*k-1/2)*binomial(n-1, (1/2)*k-1/2) end if end proc: T := proc (n, k) if `mod`(n, 2) = 0 then ae((1/2)*n, k) else ao((1/2)*n-1/2, k) end if end proc: for n from 2 to 10 do seq(T(n, k), k = 1 .. n-1) end do; # yields sequence in triangular form"
			],
			"xref": [
				"Cf. A000142, A077613, A092186."
			],
			"keyword": "nonn,tabl",
			"offset": "2,1",
			"author": "_Emeric Deutsch_, Dec 15 2008",
			"references": 1,
			"revision": 8,
			"time": "2017-07-21T10:52:21-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}