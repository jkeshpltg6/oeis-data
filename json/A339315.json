{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A339315",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 339315,
			"data": "1,3,8,34,55,144,610,233,12166,2584,4181,68260,46368,75025,3917414,464656,1346269,16349962",
			"name": "a(n) is the smallest number k such that k^2+1 divided by its largest prime factor is equal to F(2*n-1) for n \u003e 0, or 0 if no such k exists, where F(n) is the Fibonacci sequence.",
			"comment": [
				"a(n) is the smallest number k such that A248516(k) = A001519(n) for n \u003e 0, or 0 if no such k exists, where A001519(n) = F(2*n-1) (bisection of the Fibonacci sequence), with F(n) = A000045(n).",
				"We observe that a(2 + 3m) = A001519(1 + 3m) = A000045(1 + 6m) for m = 2, 3, 4, 5. For n = 6, this property no longer works.",
				"For k \u003e 0, a(3k - 1) is odd, a(3k) and a(3k+1) are even.",
				"We observe that a(n)^2 + 1 is the product of two prime Fibonacci numbers for n = 2, 3, 4, 6, 7.",
				"The first 18 terms of the sequence are Fibonacci numbers, except a(9), a(12), a(15), a(16) and a(18).",
				"The corresponding sequence b(n) = (a(n)^2+1)/ A001519(n) is 2, 5, 13, 89, 89, 233, 1597, 89, 92681, 1597, 1597, 162593, 28657, 28657, 29842993, 160373, 514229. We observe that a majority of terms of b(n) are prime Fibonacci numbers, except b(9), b(12), b(15) and b(16)."
			],
			"example": [
				"a(4) = 34 because 34^2 + 1 = 13*89 = 1157, and 1157/89 = 13 = A248516(34) = A001519(4).",
				"A curiosity: a(22) = 1134903170 = F(45) with F(45)^2 + 1 = F(43)*F(47) where F(43) and F(47) are prime Fibonacci numbers."
			],
			"maple": [
				"with(numtheory):with(combinat,fibonacci):",
				"nn:=100:n0:=20:",
				"for n from 1 to n0 do:",
				"  ii:=0:",
				"  for m from 1 to 10^10 while(ii=0) do:",
				"   x:=m^2+1:y:=factorset(x):n1:=nops(y):",
				"   z:=x/y[n1]:",
				"    if z = fibonacci(2*n-1)",
				"     then",
				"     ii:=1:printf(`%d %d \\n`,n,m):",
				"     else",
				"    fi:",
				"  od:",
				"od:"
			],
			"program": [
				"(PARI) a(n) = {my(k=1, f=fibonacci(2*n-1)); while ((k^2+1)/vecmax(factor(k^2+1)[,1]) != f, k++); k;} \\\\ _Michel Marcus_, Nov 30 2020"
			],
			"xref": [
				"Cf. A000045, A001519, A001906, A002522, A005478, A014442, A245236, A245306, A248516, A281618."
			],
			"keyword": "nonn,hard",
			"offset": "1,2",
			"author": "_Michel Lagneau_, Nov 30 2020",
			"references": 0,
			"revision": 15,
			"time": "2021-07-19T01:23:12-04:00",
			"created": "2020-12-08T21:47:28-05:00"
		}
	]
}