{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A331889",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 331889,
			"data": "1,3,2,6,10,6,10,28,54,24,15,60,214,402,120,21,110,594,2348,3810,720,28,182,1334,8556,32808,43776,5040,36,280,2614",
			"name": "Table T(n,k) read by upward antidiagonals. T(n,k) is the minimum value of Sum_{i=1..n} Product_{j=1..k} r[(i-1)*k+j] among all permutations r of {1..kn}.",
			"comment": [
				"   k    1   2     3     4     5     6     7     8      9      10       11        12",
				"  ---------------------------------------------------------------------------------",
				"n  1|   1   2     6    24   120   720  5040 40320 362880 3628800 39916800 479001600",
				"   2|   3  10    54   402  3810 43776",
				"   3|   6  28   214  2348 32808",
				"   4|  10  60   594  8556",
				"   5|  15 110  1334",
				"   6|  21 182  2614",
				"   7|  28 280",
				"   8|  36 408",
				"   9|  45 570",
				"  10|  55 770"
			],
			"link": [
				"C. W. Wu, \u003ca href=\"https://arxiv.org/abs/2002.10514\"\u003eOn rearrangement inequalities for multiple sequences\u003c/a\u003e, arXiv:2002.10514 [math.CO], 2020."
			],
			"formula": [
				"T(n,k) \u003e= ceiling(n*((kn)!)^(1/n)).",
				"T(n,1) = n*(n+1)/2 = A000217(n).",
				"T(1,k) = k! = A000142(k).",
				"T(n,3) = A072368(n).",
				"T(n,2) = n*(n+1)*(2*n+1)/3 = A006331(n)."
			],
			"program": [
				"(Python)",
				"from itertools import combinations, permutations",
				"from sympy import factorial",
				"def T(n,k): # T(n,k) for A331889",
				"    if k == 1:",
				"        return n*(n+1)//2",
				"    if n == 1:",
				"        return int(factorial(k))",
				"    if k == 2:",
				"        return n*(n+1)*(2*n+1)//3",
				"    nk = n*k",
				"    nktuple = tuple(range(1,nk+1))",
				"    nkset = set(nktuple)",
				"    count = int(factorial(nk))",
				"    for firsttuple in combinations(nktuple,n):",
				"        nexttupleset = nkset-set(firsttuple)",
				"        for s in permutations(sorted(nexttupleset),nk-2*n):",
				"            llist = sorted(nexttupleset-set(s),reverse=True)",
				"            t = list(firsttuple)",
				"            for i in range(0,k-2):",
				"                itn = i*n",
				"                for j in range(n):",
				"                        t[j] *= s[itn+j]",
				"            t.sort()",
				"            v = 0",
				"            for i in range(n):",
				"                v += llist[i]*t[i]",
				"            if v \u003c count:",
				"                count = v",
				"    return count"
			],
			"xref": [
				"Cf. A000142, A000217, A006331, A072368."
			],
			"keyword": "nonn,more,tabl",
			"offset": "1,2",
			"author": "_Chai Wah Wu_, Mar 20 2020",
			"references": 3,
			"revision": 21,
			"time": "2020-03-31T01:36:31-04:00",
			"created": "2020-03-30T20:57:34-04:00"
		}
	]
}