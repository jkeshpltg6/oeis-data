{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A169950",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 169950,
			"data": "1,1,1,1,2,1,1,5,1,1,1,8,4,2,1,1,13,8,8,1,1,1,20,15,18,7,2,1,1,33,23,45,13,11,1,1,1,48,44,86,36,28,10,2,1,1,75,64,184,70,84,18,14,1,1,1,100,117,332,166,188,68,36,13,2,1,1,145,173,657,282,482,134,132,23,17,1,1",
			"name": "Consider the 2^n monic polynomials f(x) with coefficients 0 or 1 and degree n. Sequence gives triangle read by rows, in which T(n,k) (n\u003e=0) is the number of such polynomials of thickness k (1 \u003c= k \u003c= n+1).",
			"comment": [
				"The thickness of a polynomial f(x) is the magnitude of the largest coefficient in the expansion of f(x)^2."
			],
			"link": [
				"Gheorghe Coserea, \u003ca href=\"/A169950/b169950.txt\"\u003eRows n = 0..33, flattened\u003c/a\u003e",
				"\u003ca href=\"/index/Ca#CARRYLESS\"\u003eIndex entries for sequences related to carryless arithmetic\u003c/a\u003e"
			],
			"formula": [
				"Wanted: a recurrence. Are any of A169940-A169954 related to any other entries in the OEIS?"
			],
			"example": [
				"Triangle begins:",
				"n\\k  [1]   [2]   [3]   [4]   [5]   [6]   [7]   [8]   [9]   [10]  [11]  [12]",
				"[0]  1;",
				"[1]  1,    1;",
				"[2]  1,    2,    1;",
				"[3]  1,    5,    1,    1;",
				"[4]  1,    8,    4,    2,    1;",
				"[5]  1,    13,   8,    8,    1,    1;",
				"[6]  1,    20,   15,   18,   7,    2,    1;",
				"[7]  1,    33,   23,   45,   13,   11,   1,    1;",
				"[8]  1,    48,   44,   86,   36,   28,   10,   2,    1;",
				"[9]  1,    75,   64,   184,  70,   84,   18,   14,   1,    1;",
				"[10] 1,    100,  117,  332,  166,  188,  68,   36,   13,   2,    1;",
				"[11] 1,    145,  173,  657,  282,  482,  134,  132,  23,   17,   1,    1;",
				"[12] ...",
				"For n = 3, the eight polynomials, their squares and thicknesses are as follows:",
				"x^3, x^6, 1",
				"x^3+1, x^6+2*x^3+1, 2",
				"x^3+x, x^6+2*x^4+x^2, 2",
				"x^3+x+1, x^6+2*x^4+2*x^3+x^2+2*x+1, 2",
				"x^3+x^2, x^6+2*x^5+x^4, 2",
				"x^3+x^2+1, x^6+2*x^5+2*x^3+x^4+2*x^2+1, 2",
				"x^3+x^2+x, x^6+2*x^5+3*x^4+2*x^3+x^2, 3",
				"x^3+x^2+x+1, x^6+2*x^5+3*x^4+4*x^3+3*x^2+2*x+1, 4",
				"Hence T(3,1) = 1, T(3,2) = 5, T(3,3) = 1, T(3,4) = 1."
			],
			"mathematica": [
				"Last /@ Tally@ # \u0026 /@ Table[Max@ CoefficientList[SeriesData[x, 0, #, 0, 2^n, 1]^2, x] \u0026@ IntegerDigits[#, 2] \u0026 /@ Range[2^n, 2^(n + 1) - 1], {n, 12}] // Flatten (* _Michael De Vlieger_, Jun 08 2016 *)"
			],
			"program": [
				"(PARI)",
				"seq(n) = {",
				"  my(a = vector(n+1, k, vector(k)), x='x);",
				"  for(k = 1, 2^(n+1)-1, my(pol = Pol(binary(k), x));",
				"       a[poldegree(pol)+1][vecmax(Vec(sqr(pol)))]++);",
				"  return(a);",
				"};",
				"concat(seq(11))  \\\\ _Gheorghe Coserea_, Jun 06 2016"
			],
			"xref": [
				"Related to thickness: A169940-A169954, A061909, A274036."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_, Aug 01 2010",
			"ext": [
				"Rows 17-30 of the triangle from _Nathaniel Johnston_, Nov 15 2010"
			],
			"references": 7,
			"revision": 22,
			"time": "2016-06-09T03:28:43-04:00",
			"created": "2010-08-27T03:00:00-04:00"
		}
	]
}