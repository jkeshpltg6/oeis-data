{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A285099",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 285099,
			"data": "0,0,0,1,0,2,2,1,0,3,3,1,3,2,2,1,0,4,4,1,4,2,2,1,4,3,3,1,3,2,2,1,0,5,5,1,5,2,2,1,5,3,3,1,3,2,2,1,5,4,4,1,4,2,2,1,4,3,3,1,3,2,2,1,0,6,6,1,6,2,2,1,6,3,3,1,3,2,2,1,6,4,4,1,4,2,2,1,4,3,3,1,3,2,2,1,6,5,5,1,5,2,2,1,5,3,3,1,3,2,2,1,5,4,4,1,4,2,2,1,4",
			"name": "a(n) is the zero-based index of the second least significant 1-bit in the base-2 representation of n, or 0 if there are fewer than two 1-bits in n.",
			"link": [
				"Antti Karttunen, \u003ca href=\"/A285099/b285099.txt\"\u003eTable of n, a(n) for n = 0..8192\u003c/a\u003e",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e"
			],
			"formula": [
				"If A000120(n) \u003c 2, a(n) = 0, otherwise a(n) = A007814(A129760(n)) = A007814(n AND (n-1)). [Where AND is bitwise-and, A004198].",
				"From _Jeffrey Shallit_, Apr 19 2020: (Start)",
				"This is a 2-regular sequence, satisfying the identities",
				"a(4n) = -a(n) + a(2n),",
				"a(4n+2) = a(4n+1),",
				"a(8n+1) = -a(2n+1) + 2a(4n+1),",
				"a(8n+3) = a(4n+3),",
				"a(8n+5) = 2a(4n+3),",
				"a(8n+7) = a(4n+3). (End)"
			],
			"example": [
				"For n = 3, \"11\" in binary, the second least significant 1-bit (the second 1-bit from the right) is at position 1 (when the rightmost position is position 0), thus a(3) = 1.",
				"For n = 4, \"100\" in binary, there is just one 1-bit present, thus a(4) = 0.",
				"For n = 5, \"101\" in binary, the second 1-bit from the right is at position 2, thus a(5) = 2.",
				"For n = 25, \"11001\" in binary, the second 1-bit from the right is at position 3, thus a(25) = 3."
			],
			"mathematica": [
				"a007814[n_]:=IntegerExponent[n, 2]; a[n_]:=If[DigitCount[n, 2, 1]\u003c2, 0, a007814[BitAnd[n, n - 1]]]; Table[a[n], {n, 0, 150}] (* _Indranil Ghosh_, Apr 20 2017 *)"
			],
			"program": [
				"(Scheme) (define (A285099 n) (if (\u003c= (A000120 n) 1) 0 (A007814 (A004198bi n (- n 1))))) ;; A004198bi implements bitwise-and.",
				"(Python)",
				"import math",
				"def a007814(n): return int(math.log(n - (n \u0026 n - 1), 2))",
				"def a(n): return 0 if bin(n)[2:].count(\"1\") \u003c 2 else a007814(n \u0026 (n - 1)) # _Indranil Ghosh_, Apr 20 2017"
			],
			"xref": [
				"Cf. A000120, A004198, A007814, A129760, A285097."
			],
			"keyword": "nonn,base",
			"offset": "0,6",
			"author": "_Antti Karttunen_, Apr 19 2017",
			"references": 5,
			"revision": 22,
			"time": "2020-05-06T13:24:19-04:00",
			"created": "2017-04-20T12:30:34-04:00"
		}
	]
}