{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A325002",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 325002,
			"data": "1,2,1,2,2,1,3,3,2,1,4,6,4,2,1,5,10,10,5,2,1,6,15,20,15,6,2,1,7,21,35,35,21,7,2,1,8,28,56,70,56,28,8,2,1,9,36,84,126,126,84,36,9,2,1,10,45,120,210,252,210,120,45,10,2,1,11,55,165,330,462,462,330,165,55,11,2",
			"name": "Triangle read by rows: T(n,k) is the number of oriented colorings of the facets (or vertices) of a regular n-dimensional simplex using exactly k colors.",
			"comment": [
				"For n=1, the figure is a line segment with two vertices. For n=2, the figure is a triangle with three edges. For n=3, the figure is a tetrahedron with four faces. The Schläfli symbol, {3,...,3}, of the regular n-dimensional simplex consists of n-1 threes. Each of its n+1 facets is a regular (n-1)-dimensional simplex. Two oriented colorings are the same if one is a rotation of the other; chiral pairs are counted as two. The only chiral pair occurs when k=n+1; for k \u003c= n all the colorings are achiral."
			],
			"link": [
				"Robert A. Russell, \u003ca href=\"/A325002/b325002.txt\"\u003eTable of n, a(n) for n = 1..1325\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = binomial(n,k-1) + [k==n+1] = A007318(n,k-1) + [k==n+1].",
				"T(n,k) = 2*A007318(n,k-1) - A325003(n,k) = [k==n+1] + A325003(n,k).",
				"G.f. for row n: x * (1+x)^n + x^(n+1).",
				"G.f. for column k\u003e1: x^(k-1)/(1-x)^k + x^(k-1)."
			],
			"example": [
				"Triangle begins with T(1,1):",
				"1  2",
				"1  2   2",
				"1  3   3   2",
				"1  4   6   4    2",
				"1  5  10  10    5    2",
				"1  6  15  20   15    6    2",
				"1  7  21  35   35   21    7    2",
				"1  8  28  56   70   56   28    8    2",
				"1  9  36  84  126  126   84   36    9    2",
				"1 10  45 120  210  252  210  120   45   10    2",
				"1 11  55 165  330  462  462  330  165   55   11    2",
				"1 12  66 220  495  792  924  792  495  220   66   12   2",
				"1 13  78 286  715 1287 1716 1716 1287  715  286   78  13   2",
				"1 14  91 364 1001 2002 3003 3432 3003 2002 1001  364  91  14  2",
				"1 15 105 455 1365 3003 5005 6435 6435 5005 3003 1365 455 105 15 2",
				"For T(3,2)=3, the tetrahedron may have one, two, or three faces of one color."
			],
			"mathematica": [
				"Table[Binomial[n,k-1] + Boole[k==n+1], {n,1,15}, {k,1,n+1}] \\\\ Flatten"
			],
			"xref": [
				"Cf. A007318(n,k-1) (unoriented), A325003 (achiral), A325001 (up to k colors).",
				"Other n-dimensional polytopes: A325008 (orthotope), A325016 (orthoplex)."
			],
			"keyword": "nonn,tabf",
			"offset": "1,2",
			"author": "_Robert A. Russell_, Mar 23 2019",
			"references": 8,
			"revision": 13,
			"time": "2019-05-25T11:43:50-04:00",
			"created": "2019-05-25T11:43:50-04:00"
		}
	]
}