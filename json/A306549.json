{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A306549",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 306549,
			"data": "1,1,2,1,6,2,3,1,24,6,8,2,12,3,4,1,120,24,30,6,40,8,10,2,60,12,15,3,20,4,5,1,720,120,144,24,180,30,36,6,240,40,48,8,60,10,12,2,360,60,72,12,90,15,18,3,120,20,24,4,30,5,6,1,5040,720,840,120,1008",
			"name": "a(n) is the product of the positions of the zeros in the binary expansion of n (the most significant bit having position 1).",
			"comment": [
				"Apparently, the variant where the least significant bit has position 1 corresponds to A124773."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A306549/b306549.txt\"\u003eTable of n, a(n) for n = 0..16384\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A070939(n)! / A306286(n).",
				"a(2*n) = a(n) * (1+A070939(n)).",
				"a(2*n+1) = a(n).",
				"a(2^k) = (k+1)! for any k \u003e= 0.",
				"a(2^k-1) = 1 for any k \u003e= 0.",
				"a(2^k-2) = k for any k \u003e= 1."
			],
			"example": [
				"The first terms, alongside the positions of zeros and the binary representation of n, are:",
				"  n   a(n)  Pos.zeros  bin(n)",
				"  --  ----  ---------  ------",
				"   0     1  {1}             0",
				"   1     1  {}              1",
				"   2     2  {2}            10",
				"   3     1  {}             11",
				"   4     6  {2,3}         100",
				"   5     2  {2}           101",
				"   6     3  {3}           110",
				"   7     1  {}            111",
				"   8    24  {2,3,4}      1000",
				"   9     6  {2,3}        1001",
				"  10     8  {2,4}        1010",
				"  11     2  {2}          1011",
				"  12    12  {3,4}        1100",
				"  13     3  {3}          1101",
				"  14     4  {4}          1110",
				"  15     1  {}           1111"
			],
			"program": [
				"(PARI) a(n) = my (b=binary(n)); prod(k=1, #b, if (b[k]==0, k, 1))"
			],
			"xref": [
				"Cf. A070939, A124773, A306286."
			],
			"keyword": "nonn,base,easy",
			"offset": "0,3",
			"author": "_Rémy Sigrist_, May 04 2019",
			"references": 2,
			"revision": 21,
			"time": "2019-05-05T10:59:05-04:00",
			"created": "2019-05-04T18:28:57-04:00"
		}
	]
}