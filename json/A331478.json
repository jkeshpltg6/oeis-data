{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A331478",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 331478,
			"data": "0,1,2,0,3,1,4,2,5,3,6,4,7,0,5,8,1,6,9,2,7,10,3,8,11,4,9,12,5,10,13,6,11,14,0,7,12,15,1,8,13,16,2,9,14,17,3,10,15,18,4,11,16,19,5,12,17,20,6,13,18,21,7,14,19,22,8,15,20,23,0,9,16,21,24,1",
			"name": "Irregular triangle T(n,k) = n - (s - k + 1)^2 for 1 \u003c= k \u003c= s, with s = floor(sqrt(n)).",
			"comment": [
				"Row n begins with n - floor(sqrt(n)).",
				"Zero appears in row n for n that are perfect squares. Let r = sqrt(n). For perfect square n, there exists a partition of n that consists of a run of r parts that are each r themselves; e.g., for n = 4, we have {2, 2}, for n = 9, we have {3, 3, 3}. It is clear through the Ferrers diagram of these partitions that they are equivalent to their Durfee square, thus n - s^2 = 0.",
				"Since the partitions of any n contain Durfee squares in the range of 1 \u003c= s \u003c= floor(sqrt(n)) (with perfect square n also including k = 0), the distinct Durfee square excesses must be the differences n - s^2 for 1 \u003c= s \u003c= floor(sqrt(n)).",
				"We borrow the term \"square excess\" from A053186(n), which is simply the difference n - floor(sqrt(n)).",
				"Row n of this sequence contains distinct Durfee square excesses among all integer partitions of n (see example below)."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A331478/b331478.txt\"\u003eTable of n, a(n) for n = 1..10125\u003c/a\u003e (rows 1 \u003c= n \u003c= 625, flattened)",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/DurfeeSquare.html\"\u003eDurfee Square.\u003c/a\u003e"
			],
			"formula": [
				"Let s = floor(sqrt(n));",
				"T(n,1) = A053186(n) = n - s;",
				"T(n,k) = T(n,1) + partial sums of 2(s - k + 1) + 1 for 2 \u003c= k \u003c= s + 1.",
				"A000196(n) = Length of row n.",
				"A022554(n) = Sum of row n.",
				"Last term in row n = T(n, A000196(n)) = n - 1."
			],
			"example": [
				"Table begins:",
				"   1:  0;",
				"   2:  1;",
				"   3:  2;",
				"   4:  0,  3;",
				"   5:  1,  4;",
				"   6:  2,  5;",
				"   7:  3,  6;",
				"   8:  4,  7;",
				"   9:  0,  5,  8;",
				"  10:  1,  6,  9;",
				"  11:  2,  7, 10;",
				"  12:  3,  8, 11;",
				"  13:  4,  9, 12;",
				"  14:  5, 10, 13;",
				"  15:  6, 11, 14;",
				"  16:  0,  7, 12, 15;",
				"  ...",
				"For n = 4, the partitions are {4}, {3, 1}, {2, 2}, {2, 1, 1}, {1, 1, 1, 1}. The partition {2, 2} has Durfee square s = 2; for all partitions except {2, 2}, we have Durfee square with s = 1. Therefore we have two unique solutions to n - s^2 for n = 4, i.e., {0, 3}, so row 4 contains these values."
			],
			"mathematica": [
				"Array[# - Reverse@ Range[Sqrt@ #]^2 \u0026, 625] // Flatten"
			],
			"xref": [
				"Cf. A000196, A022554, A053186, A117522."
			],
			"keyword": "nonn,easy,tabf",
			"offset": "1,3",
			"author": "_Michael De Vlieger_, Jan 17 2020",
			"references": 1,
			"revision": 10,
			"time": "2020-03-16T16:32:12-04:00",
			"created": "2020-03-13T20:47:39-04:00"
		}
	]
}