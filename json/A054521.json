{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A054521",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 54521,
			"data": "1,1,0,1,1,0,1,0,1,0,1,1,1,1,0,1,0,0,0,1,0,1,1,1,1,1,1,0,1,0,1,0,1,0,1,0,1,1,0,1,1,0,1,1,0,1,0,1,0,0,0,1,0,1,0,1,1,1,1,1,1,1,1,1,1,0,1,0,0,0,1,0,1,0,0,0,1,0,1,1,1,1,1,1,1,1,1,1,1,1,0,1,0",
			"name": "Triangle read by rows: T(n,k) = 1 if gcd(n, k) = 1, T(n,k) = 0 otherwise (n \u003e= 1, 1 \u003c= k \u003c= n).",
			"comment": [
				"Row sums = phi(n), A000010: (1, 1, 2, 2, 4, 2, 6, ...). - _Gary W. Adamson_, May 20 2007",
				"Characteristic function of A169581: a(A169581(n)) = 1; a(A169582(n)) = 0. - _Reinhard Zumkeller_, Dec 02 2009",
				"The function T(n,k) = T(k,n) is defined for k \u003e n but only the values for 1 \u003c= k \u003c= n as a triangular array are listed here.",
				"T(n,k) = |K(n-k|k)| where K(i|j) is the Kronecker symbol. - _Peter Luschny_, Aug 05 2012",
				"Twice the sum over the antidiagonals, starting with entry T(n-1,1), for n \u003e= 3, is the same as the row n sum (i.e., phi(n): 2*Sum_{k=1..floor(n/2)} T(n-k,k) = phi(n), n \u003e= 3). - _Wolfdieter Lang_, Apr 26 2013",
				"The number of zeros in the n-th row of the triangle is cototient(n) = A051953(n). - _Omar E. Pol_, Apr 21 2017",
				"This triangle is the j = 1 sub-triangle of A349221(n,k) = Sum_{j\u003e=1} [k|binomial(n-1,k-1) AND gcd(n,k) = j], n \u003e= 1, 1 \u003c= k \u003c= n, where [] is the Iverson bracket. - _Richard L. Ollerton_, Dec 14 2021"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A054521/b054521.txt\"\u003eRows n = 1..125 of triangle, flattened\u003c/a\u003e",
				"Jakub Jaroslaw Ciaston, \u003ca href=\"/plot2a?name1=A054531\u0026amp;name2=A164306\u0026amp;tform1=untransformed\u0026amp;tform2=untransformed\u0026amp;shift=0\u0026amp;radiop1=xy\u0026amp;drawpoints=true\"\u003eA054531 vs A164306\u003c/a\u003e (plot shows these ones)",
				"\u003ca href=\"/index/Ch#char_fns\"\u003eIndex entries for characteristic functions\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = A063524(A050873(n,k)). - _Reinhard Zumkeller_, Dec 02 2009, corrected Sep 03 2015",
				"T(n,k) = A054431(n,k) = A054431(k,n). - _R. J. Mathar_, Jul 21 2016"
			],
			"example": [
				"The triangle T(n,k) begins:",
				"n\\k  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 ...",
				"1:   1",
				"2:   1  0",
				"3:   1  1  0",
				"4:   1  0  1  0",
				"5:   1  1  1  1  0",
				"6:   1  0  0  0  1  0",
				"7:   1  1  1  1  1  1  0",
				"8:   1  0  1  0  1  0  1  0",
				"9:   1  1  0  1  1  0  1  1  0",
				"10:  1  0  1  0  0  0  1  0  1  0",
				"11:  1  1  1  1  1  1  1  1  1  1  0",
				"12:  1  0  0  0  1  0  1  0  0  0  1  0",
				"13:  1  1  1  1  1  1  1  1  1  1  1  1  0",
				"14:  1  0  1  0  1  0  0  0  1  0  1  0  1  0",
				"15:  1  1  0  1  0  0  1  1  0  0  1  0  1  1  0",
				"... (Reformatted by _Wolfdieter Lang_, Apr 26 2013)",
				"Sums over antidiagonals: n = 3: 2*T(2,1) = 2 = T(3,1) + T(3,2) = phi(3). n = 4: 2*(T(3,1) + T(2,2)) = 2 = phi(4), etc. - _Wolfdieter Lang_, Apr 26 2013"
			],
			"maple": [
				"A054521_row := n -\u003e seq(abs(numtheory[jacobi](n-k,k)),k=1..n);",
				"for n from 1 to 13 do A054521_row(n) od; # _Peter Luschny_, Aug 05 2012"
			],
			"mathematica": [
				"T[ n_, k_] := Boole[ n\u003e0 \u0026\u0026 k\u003e0 \u0026\u0026 GCD[ n, k] == 1] (* _Michael Somos_, Jul 17 2011 *)",
				"T[ n_, k_] := If[ n\u003c1 || k\u003c1, 0, If[ k\u003en, T[ k, n], If[ k==1, 1, If[ n\u003ek, T[ k, Mod[ n, k, 1]], 0]]] (* _Michael Somos_, Jul 17 2011 *)"
			],
			"program": [
				"(PARI) {T(n, k) = n\u003e0 \u0026\u0026 k\u003e0 \u0026\u0026 gcd(n, k)==1} /* _Michael Somos_, Jul 17 2011 */",
				"(Sage)",
				"def A054521_row(n): return [abs(kronecker_symbol(n-k,k)) for k in (1..n)]",
				"for n in (1..13): print(A054521_row(n)) # _Peter Luschny_, Aug 05 2012",
				"(Haskell)",
				"a054521 n k = a054521_tabl !! (n-1) !! (k-1)",
				"a054521_row n = a054521_tabl !! (n-1)",
				"a054521_tabl = map (map a063524) a050873_tabl",
				"a054521_list = concat a054521_tabl",
				"-- _Reinhard Zumkeller_, Sep 03 2015"
			],
			"xref": [
				"Cf. A051731, A054522, A215200.",
				"Cf. A050873, A063524.",
				"Cf. A349221."
			],
			"keyword": "nonn,tabl",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_, Apr 09 2000",
			"references": 58,
			"revision": 74,
			"time": "2021-12-14T22:57:39-05:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}