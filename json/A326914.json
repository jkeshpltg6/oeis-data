{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A326914",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 326914,
			"data": "1,1,2,2,5,1,12,15,18,64,52,20,166,340,203,18,332,1315,1866,877,15,566,3895,9930,10710,4140,11,864,9770,39960,74438,64520,21147,6,1214,21848,134871,386589,564508,408096,115975,3,1596,44880,402756,1668338,3652712",
			"name": "Number T(n,k) of colored integer partitions of n using all colors of a k-set such that all parts have different color patterns and a pattern for part i has i distinct colors in increasing order; triangle T(n,k), n\u003e=0, min(j:A001787(j)\u003e=n)\u003c=k\u003c=n, read by rows.",
			"comment": [
				"T(n,k) is defined for all n\u003e=0 and k\u003e=0.  The triangle displays only positive terms.  All other terms are zero."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A326914/b326914.txt\"\u003eRows n = 0..200, flattened\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Partition_(number_theory)\"\u003ePartition (number theory)\u003c/a\u003e"
			],
			"formula": [
				"Sum_{k=1..n} k * T(n,k) = A327115(n).",
				"T(n*2^(n-1),n) = T(A001787(n),n) = 1.",
				"T(n*2^(n-1)-1,n) = n for n \u003e= 2."
			],
			"example": [
				"T(4,3) = 12: 3abc1a, 3abc1b, 3abc1c, 2ab2ac, 2ab2bc, 2ac2bc, 2ab1a1c, 2ab1b1c, 2ac1a1b, 2ac1b1c, 2bc1a1b, 2bc1a1c.",
				"Triangle T(n,k) begins:",
				"  1;",
				"     1;",
				"        2;",
				"        2,  5;",
				"        1, 12,   15;",
				"           18,   64,    52;",
				"           20,  166,   340,    203;",
				"           18,  332,  1315,   1866,    877;",
				"           15,  566,  3895,   9930,  10710,   4140;",
				"           11,  864,  9770,  39960,  74438,  64520,  21147;",
				"            6, 1214, 21848, 134871, 386589, 564508, 408096, 115975;",
				"  ..."
			],
			"maple": [
				"C:= binomial:",
				"g:= proc(n) option remember; n*2^(n-1) end:",
				"h:= proc(n) option remember; local k; for k from",
				"      `if`(n=0, 0, h(n-1)) do if g(k)\u003e=n then return k fi od",
				"    end:",
				"b:= proc(n, i, k) option remember; `if`(n=0, 1, `if`(i\u003c1, 0, add(",
				"      b(n-i*j, min(n-i*j, i-1), k)*C(C(k, i), j), j=0..n/i)))",
				"    end:",
				"T:= (n, k)-\u003e add(b(n$2, i)*(-1)^(k-i)*C(k, i), i=0..k):",
				"seq(seq(T(n, k), k=h(n)..n), n=0..12);"
			],
			"mathematica": [
				"c = Binomial;",
				"g[n_] := g[n] = n*2^(n - 1);",
				"h[n_] := h[n] = Module[{k}, For[k = If[n == 0, 0, h[n - 1]], True, k++, If[g[k] \u003e= n, Return[k]]]];",
				"b[n_, i_, k_] := b[n, i, k] = If[n == 0, 1, If[i \u003c 1, 0, Sum[b[n - i*j, Min[n - i*j, i - 1], k] c[c[k, i], j], {j, 0, n/i}]]];",
				"T[n_, k_] := Sum[b[n, n, i] (-1)^(k - i) c[k, i], {i, 0, k}];",
				"Table[Table[T[n, k], {k, h[n], n}], {n, 0, 12}] // Flatten (* _Jean-François Alcover_, Dec 17 2020, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Main diagonal gives A000110.",
				"Row sums give A116539.",
				"Column sums give A003465.",
				"Cf. A001787, A255903, A326962 (this triangle read by columns), A327115, A327116, A327117."
			],
			"keyword": "nonn,tabf",
			"offset": "0,3",
			"author": "_Alois P. Heinz_, Sep 13 2019",
			"references": 9,
			"revision": 50,
			"time": "2020-12-17T07:54:56-05:00",
			"created": "2019-09-13T16:38:53-04:00"
		}
	]
}