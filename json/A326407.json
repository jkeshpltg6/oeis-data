{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A326407",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 326407,
			"data": "2,-1,-1,2,-1,5,-1,2,1,3,-1,4,-1,3,2,0,-1,3,-1,3,3,2,-1,1,0,2,3,2,-1,3,-1,1,2,2,2,0,-1,1,2,3,-1,3,-1,3,3,2,-1,1,0,1,2,2,-1,2,3,2,3,2,-1,2,-1,3,2,0,1,2,-1,2,1,1,-1,3,-1,1,1,1,3,3,-1,1,0",
			"name": "Minesweeper sequence of positive integers arranged on a 2D grid along a square array that grows by alternately adding a row at its bottom edge and a column at its right edge.",
			"comment": [
				"Place positive integers on a 2D grid starting with 1 in the top left corner and continue along an increasing square array as in A060734.",
				"Replace each prime with -1 and each nonprime with the number of primes in adjacent grid cells around them.",
				"n is replaced by a(n).",
				"This sequence treats prime numbers as \"mines\" and fills gaps according to the rules of the classic Minesweeper game.",
				"a(n) \u003c 6 (conjectured).",
				"a(n) = 5 for n={6} (conjectured).",
				"Set of n such that a(n) = 4 is unbounded (conjectured)."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A326407/b326407.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Michael De Vlieger, \u003ca href=\"/A326407/a326407.png\"\u003eMinesweeper-style graph\u003c/a\u003e read along original mapping, replacing -1 with a \"mine\", and 0 with blank space.",
				"Michael De Vlieger, \u003ca href=\"/A326407/a326407_1.png\"\u003eSquare plot of a million terms\u003c/a\u003e read along original mapping, with black indicating a prime and levels of gray commensurate to a(n).",
				"Witold Tatkiewicz, \u003ca href=\"https://pastebin.com/SvHPVVvp\"\u003elink for Java program\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Minesweeper_(video_game)\"\u003eMinesweeper game\u003c/a\u003e"
			],
			"example": [
				"Consider positive integers distributed onto the plane along an increasing square array:",
				"   1  4  9 16 25 36",
				"   2  3  8 15 24 35",
				"   5  6  7 14 23 34",
				"  10 11 12 13 22 33",
				"  17 18 19 20 21 32",
				"  26 27 28 29 30 31",
				"...",
				"1 is not prime and in adjacent grid cells there are 2 primes: 2 and 3. Therefore a(1) = 2.",
				"2 is prime, therefore a(2) = -1.",
				"8 is not prime and in adjacent grid cells there are 2 primes: 3, and 7. Therefore a(8) = 2.",
				"Replacing n with a(n) in the plane described above, and using \".\" for a(n) = 0 and \"*\" for negative a(n), we produce a graph resembling Minesweeper, where the mines are situated at prime n:",
				"  2  2  1  .  .  .  .  .  .  .  .  . ...",
				"  *  *  2  2  1  2  1  2  1  1  .  1",
				"  *  5  *  3  *  2  *  3  *  2  1  1",
				"  3  *  4  *  2  2  2  *  3  *  1  1",
				"  *  3  *  3  3  1  3  2  3  1  2  1",
				"  2  3  2  *  3  *  3  *  1  .  1  *",
				"  *  1  2  3  *  3  *  2  1  .  2  3",
				"  1  2  2  *  2  3  2  3  1  2  2  *",
				"  1  2  *  2  1  1  *  3  *  2  *  2",
				"  2  *  3  2  .  2  3  *  3  3  1  1",
				"  *  3  *  1  1  2  *  3  *  2  1  .",
				"  1  2  1  2  2  *  3  3  2  *  1  1",
				"...",
				"In order to produce the sequence, the graph is read along the original mapping."
			],
			"mathematica": [
				"Block[{n = 12, s}, s = ArrayPad[Array[If[#1 \u003c 2 #2 - 1, #2^2 + #2 - #1, (#1 - #2)^2 + #2] \u0026 @@ {#1 + #2 - 1, #2} \u0026, {# + 1, # + 1}], 1] \u0026@ n; Table[If[PrimeQ@ m, -1, Count[#, _?PrimeQ] \u0026@ Union@ Map[s[[#1, #2]] \u0026 @@ # \u0026, Join @@ Array[FirstPosition[s, m] + {##} - 2 \u0026, {3, 3}]]], {m, PolygonalNumber@ n}]] (* _Michael De Vlieger_, Oct 02 2019 *)"
			],
			"program": [
				"(Java) See Links section."
			],
			"xref": [
				"Cf. A060734 - plane mapping",
				"Different arrangements of integers:",
				"Cf. A326405 - antidiagonals,",
				"Cf. A326406 - triangle maze,",
				"Cf. A326408 - square maze,",
				"Cf. A326409 - Hamiltonian path,",
				"Cf. A326410 - Ulam's spiral."
			],
			"keyword": "sign,tabl",
			"offset": "1,1",
			"author": "_Witold Tatkiewicz_, Oct 02 2019",
			"references": 8,
			"revision": 27,
			"time": "2020-03-20T22:54:18-04:00",
			"created": "2019-10-04T08:18:49-04:00"
		}
	]
}