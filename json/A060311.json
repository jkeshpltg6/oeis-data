{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A060311",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 60311,
			"data": "1,0,1,3,10,45,241,1428,9325,67035,524926,4429953,40010785,384853560,3925008361,42270555603,478998800290,5693742545445,70804642315921,918928774274028,12419848913448565,174467677050577515,2542777209440690806,38388037137038323353",
			"name": "E.g.f.: exp((exp(x)-1)^2/2).",
			"comment": [
				"After the first term, this is the Stirling transform of the sequence of moments of the standard normal (or \"Gaussian\") probability distribution. It is not itself a moment sequence of any probability distribution. - Michael Hardy (hardy(AT)math.umn.edu), May 29 2005",
				"a(n) is the number of simple labeled graphs on n nodes in which each component is a complete bipartite graph. - _Geoffrey Critzer_, Dec 03 2011"
			],
			"reference": [
				"I. P. Goulden and D. M. Jackson, Combinatorial Enumeration, John Wiley and Sons, N.Y., 1983, Ex. 3.3.5b."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A060311/b060311.txt\"\u003eTable of n, a(n) for n = 0..518\u003c/a\u003e (first 101 terms from Harry J. Smith)",
				"P. Barry, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Barry2/barry281.html\"\u003eConstructing Exponential Riordan Arrays from Their A and Z Sequences\u003c/a\u003e, Journal of Integer Sequences, 17 (2014), #14.2.6.",
				"Vaclav Kotesovec, \u003ca href=\"http://oeis.org/A216688/a216688.pdf\"\u003eAsymptotic solution of the equations using the Lambert W-function\u003c/a\u003e"
			],
			"formula": [
				"E.g.f. A(x) = B(exp(x)-1) where B(x)=exp(x^2/2) is e.g.f. of A001147(2n), hence a(n) is the Stirling transform of A001147(2n). - _Michael Somos_, Jun 01 2005",
				"From _Vaclav Kotesovec_, Aug 06 2014: (Start)",
				"a(n) ~ exp(1/2*(exp(r)-1)^2 - n) * n^(n+1/2) / (r^n * sqrt(exp(r)*r*(-1-r+exp(r)*(1+2*r)))), where r is the root of the equation exp(r)*(exp(r) - 1)*r = n.",
				"(a(n)/n!)^(1/n) ~ 2*exp(1/LambertW(2*n)) / LambertW(2*n).",
				"(End)"
			],
			"maple": [
				"a:= proc(n) option remember; `if`(n=0, 1, add(a(n-j)",
				"      *binomial(n-1, j-1)*Stirling2(j, 2), j=2..n))",
				"    end:",
				"seq(a(n), n=0..25);  # _Alois P. Heinz_, Sep 02 2019"
			],
			"mathematica": [
				"a = Exp[x] - 1; Range[0, 20]! CoefficientList[Series[Exp[a^2/2], {x, 0, 20}], x] (* _Geoffrey Critzer_, Dec 03 2011 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c0, 0, n!*polcoeff( exp((exp(x+x*O(x^n))-1)^2/2), n)) /* _Michael Somos_, Jun 01 2005 */",
				"(PARI) { for (n=0, 100, write(\"b060311.txt\", n, \" \", n!*polcoeff(exp((exp(x + x*O(x^n)) - 1)^2/2), n)); ) } \\\\ _Harry J. Smith_, Jul 03 2009"
			],
			"xref": [
				"Column k=2 of A324162.",
				"Cf. A052859."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_Vladeta Jovovic_, Mar 27 2001",
			"references": 6,
			"revision": 39,
			"time": "2019-09-02T21:00:21-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}