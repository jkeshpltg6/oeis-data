{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A238743",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 238743,
			"data": "1,1,1,1,7,1,1,26,26,1,1,56,208,56,1,1,124,992,992,124,1,1,182,3224,6944,3224,182,1,1,342,8892,42408,42408,8892,342,1,1,448,21888,153216,339264,153216,21888,448,1,1,702,44928,590976,1920672,1920672,590976,44928",
			"name": "Triangle read by rows: T(n,k) = A059382(n)/(A059382(k)*A059382(n-k)).",
			"comment": [
				"We assume that A059382(0)=1 since it would be the empty product.",
				"These are the generalized binomial coefficients associated with the Jordan totient function J_3 given in A059376.",
				"Another name might be the 3-totienomial coefficients."
			],
			"link": [
				"Tom Edgar, \u003ca href=\"http://www.emis.de/journals/INTEGERS/papers/o62/o62.Abstract.html\"\u003eTotienomial Coefficients\u003c/a\u003e, INTEGERS, 14 (2014), #A62.",
				"Tom Edgar and Michael Z. Spivey, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL19/Edgar/edgar3.html\"\u003eMultiplicative functions, generalized binomial coefficients, and generalized Catalan numbers\u003c/a\u003e, Journal of Integer Sequences, Vol. 19 (2016), Article 16.1.6.",
				"Donald E. Knuth and Herbert S. Wilf, \u003ca href=\"http://www.math.upenn.edu/~wilf/website/dm36.pdf\"\u003eThe power of a prime that divides a generalized binomial coefficient\u003c/a\u003e, J. Reine Angew. Math., 396:212-219, 1989."
			],
			"formula": [
				"T(n,k) = A059382(n)/(A059382(k)* A059382(n-k)).",
				"T(n,k) = prod_{i=1..n} A059376(i)/(prod_{i=1..k} A059376(i)*prod_{i=1..n-k} A059376(i)).",
				"T(n,k) = A059376(n)/n*(k/A059376(k)*T(n-1,k-1)+(n-k)/A059376(n-k)*T(n-1,k))."
			],
			"example": [
				"The first five terms in the third Jordan totient function are 1,7,26,56,124 and so T(4,2) = 56*26*7*1/((7*1)*(7*1))=208 and T(5,3) = 124*56*26*7*1/((26*7*1)*(7*1))=992.",
				"The triangle begins",
				"1",
				"1 1",
				"1 7   1",
				"1 26  26   1",
				"1 56  208  56   1",
				"1 124 992  992  124  1",
				"1 182 3224 6944 3224 182 1"
			],
			"program": [
				"(Sage)",
				"q=100 #change q for more rows",
				"P=[0]+[i^3*prod([1-1/p^3 for p in prime_divisors(i)]) for i in [1..q]]",
				"[[prod(P[1:n+1])/(prod(P[1:k+1])*prod(P[1:(n-k)+1])) for k in [0..n]] for n in [0..len(P)-1]] #generates the triangle up to q rows."
			],
			"xref": [
				"Cf. A059382, A059376, A238688, A238453."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Tom Edgar_, Mar 04 2014",
			"references": 3,
			"revision": 19,
			"time": "2016-01-25T14:22:05-05:00",
			"created": "2014-03-05T09:12:32-05:00"
		}
	]
}