{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A321169",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 321169,
			"data": "3,2,43,79,241,727,3643,15307,19681,164023,1673053,885733,2657203,18600433,23914843,100442347,358722673,645700813,4519905703,18983603959,48427561123,31381059607,261508830073,1307544150373,3295011258943,24006510600883,12709329141643,53379182394907,190639937124673,2615579937350539",
			"name": "a(n) is the smallest prime p such that p + 2 is a product of n primes (counted with multiplicity).",
			"comment": [
				"a(n) ~ c * 3^n. - _David A. Corneth_, Jan 11 2019"
			],
			"link": [
				"David A. Corneth, \u003ca href=\"/A321169/b321169.txt\"\u003eTable of n, a(n) for n = 1..801\u003c/a\u003e"
			],
			"example": [
				"a(1) = 3 as 3 + 2 = 5 (prime),",
				"a(2) = 2 as 2 + 2 = 4 = 2*2 (semiprime),",
				"a(3) = 43 as 43 + 2 = 45 = 3*3*5  (3-almost prime),",
				"a(4) = 79 as 79 + 2 = 81 = 3*3*3*3 (4-almost prime)."
			],
			"mathematica": [
				"ptns[n_, 0] := If[n == 0, {{}}, {}]; ptns[n_, k_] := Module[{r}, If[n \u003c k, Return[{}]]; ptns[n, k] = 1 + Union @@ Table[PadRight[#, k] \u0026 /@ ptns[n - k, r], {r, 0, k}]]; a[n_] := Module[{i, l, v}, v = Infinity; For[i = n, True, i++, l = (Times @@ Prime /@ # \u0026) /@ ptns[i, n]; If[Min @@ l \u003e v, Return[v]]; minp = Min @@ Select[l - 2, PrimeQ]; If[minp \u003c v, v = minp]]] ; Array[a, 10] (* after _Amarnath Murthy_ at A073919 *)"
			],
			"program": [
				"(PARI) a(n) = forprime(p=2, , if (bigomega(p+2) == n, return (p))); \\\\ _Michel Marcus_, Jan 10 2019",
				"(PARI) a(n) = {my(p3 = 3^n, u, c); if(n \u003c= 2, return(4 - n)); if(isprime(p3 - 2), return(p3 - 2)); forprime(p = 5, oo, if(isprime(p3 / 3 * p - 2), u = p3 / 3 * p - 2; break ) ); for(i = 2, n, if(p3 * (5/3)^i \u003e u, return(u)); for(j = 1, oo, if(p3 * j \\ 3^i \u003e u, next(2)); if(bigomega(j) == i, if(isprime(p3 / 3^(i) * j - 2), u = p3 / 3^(i) * j - 2; next(2) ) ) ) ); return(u) } \\\\ _David A. Corneth_, Jan 11 2019"
			],
			"xref": [
				"Cf. A001222, A001358, A014612, A014613, A014614, A046306, A046308, A046310, A046312, A046314, A073919, A118883."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Amiram Eldar_ and _Zak Seidov_, Jan 10 2019",
			"references": 1,
			"revision": 27,
			"time": "2019-01-11T15:05:37-05:00",
			"created": "2019-01-10T23:37:35-05:00"
		}
	]
}