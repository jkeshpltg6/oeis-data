{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A102662",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 102662,
			"data": "1,1,3,1,5,3,1,7,11,3,1,9,23,17,3,1,11,39,51,23,3,1,13,59,113,91,29,3,1,15,83,211,255,143,35,3,1,17,111,353,579,489,207,41,3,1,19,143,547,1143,1323,839,283,47,3,1,21,179,801,2043,3045,2651,1329,371,53,3,1,23,219",
			"name": "Triangle read by rows: T(1,1)=1,T(2,1)=1,T(2,2)=3, T(k-1,r-1)+T(k-1,r)+T(k-2,r-1).",
			"comment": [
				"Generalization of A008288 (use initial terms 1,1,3). Triangle seen as lower triangular matrix: The absolute values of the coefficients of the characteristic polynomials of the n X n matrix are the (n+1)th row of A038763. Row sums give A048654."
			],
			"reference": [
				"Boris A. Bondarenko, \"Generalized Pascal Triangles and Pyramids\" Fibonacci Association, 1993, p. 37"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A102662/b102662.txt\"\u003eRows n=0..149 of triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"A102662=v and A207624=u, defined together as follows:",
				"u(n,x)=u(n-1,x)+v(n-1,x), v(n,x)=2x*u(n-1,x)+x*v(n-1,x)+1,",
				"where u(1,x)=1, v(1,x)=1; see the Mathematica section.",
				"[From Clark Kimberling, Feb 20 2012]"
			],
			"example": [
				"Triangle begins:",
				"1",
				"1 3",
				"1 5 3",
				"1 7 11 3",
				"1 9 23 17 3"
			],
			"mathematica": [
				"u[1, x_] := 1; v[1, x_] := 1; z = 16;",
				"u[n_, x_] := u[n - 1, x] + v[n - 1, x]",
				"v[n_, x_] := 2 x*u[n - 1, x] + x*v[n - 1, x] + 1",
				"Table[Factor[u[n, x]], {n, 1, z}]",
				"Table[Factor[v[n, x]], {n, 1, z}]",
				"cu = Table[CoefficientList[u[n, x], x], {n, 1, z}];",
				"TableForm[cu]",
				"Flatten[%]    (* A207624 *)",
				"Table[Expand[v[n, x]], {n, 1, z}]",
				"cv = Table[CoefficientList[v[n, x], x], {n, 1, z}];",
				"TableForm[cv]",
				"Flatten[%]    (* A102662 *)",
				"(* _Clark Kimberling_, Feb 20 2012 *)"
			],
			"program": [
				"(PARI) T(k,r)=if(r\u003ek,0,if(k==1,1,if(k==2,if(r==1,1,3),if(r==1,1,if(r==k,3,T(k-1,r-1)+T(k-1,r)+T(k-2,r-1)))))) BM(n) = M=matrix(n,n);for(i=1,n, for(j=1,n,M[i,j]=T(i,j)));M M=BM(10) for(i=1,10,s=0;for(j=1,i,s+=M[i,j]);print1(s,\",\"))",
				"(Haskell)",
				"a102662 n k = a102662_tabl !! n !! k",
				"a102662_row n = a102662_tabl !! n",
				"a102662_tabl = [1] : [1,3] : f [1] [1,3] where",
				"   f xs ys = zs : f ys zs where",
				"     zs = zipWith (+) ([0] ++ xs ++ [0]) $",
				"                      zipWith (+) ([0] ++ ys) (ys ++ [0])",
				"-- _Reinhard Zumkeller_, Feb 23 2012"
			],
			"xref": [
				"Cf. A038763, A048654, A008288."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "Lambert Klasen (lambert.klasen(AT)gmx.net) and _Gary W. Adamson_, Feb 03 2005",
			"references": 3,
			"revision": 13,
			"time": "2014-08-03T14:27:17-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}