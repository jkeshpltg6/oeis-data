{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A344813",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 344813,
			"data": "0,1,5,13,25,18,32,50,41,61,85,72,98,85,61,74,100,89,65,80,58,73,97,90,116,106,117,149,130,113,145,181,162,128,145,113,130,164,149,117,136,106,125,97,73,90,68,50,36,50,37,53,73,58,80,106,89,117,100,74,52,34,20,10,17,9,17,10,4,5",
			"name": "Square of the distance from the origin for a square lattice Moore neighborhood self-avoiding walk using the rules given in the Comments.",
			"comment": [
				"On a square lattice, where the walk can step to any of the eight unvisited nearest neighboring points (Moore neighborhood), start at the origin on the square numbered 1 and then step directly north to a square numbered 2. From then on sum the numbers in the last two visited squares, where each square is numbered with the next distinct integer after 2 as it is visited. If that sum is a prime number, the next step is to an unvisited square that is as far away from the origin as possible. If the sum is not prime then step to an unvisited square as close as possible to the origin. In either case if two squares exist that are the same distance from the origin then step to the square that is as far away from, if the sum is prime, or as close to, if the sum is composite, to the square with number 2. If these distances are the same, then step similarly to the square with number 3.",
				"The sequence gives the square of the distance from the origin for the visited squares of this walk. The sequence is finite. After 128 steps, 129 visited squares, the walk ends as all eight neighboring squares have been visited."
			],
			"link": [
				"Eric Angelini, \u003ca href=\"http://cinquantesignes.blogspot.com/2021/05/a-self-trapped-sum.html\"\u003eA self-trapped sum?\u003c/a\u003e, personal blog \"Cinquante signes\", May 20, 2021.",
				"Scott R. Shannon, \u003ca href=\"/A344813/a344813_3.jpg\"\u003eImage of the full walk\u003c/a\u003e. The colors are graduated across the spectrum to show the relative step order."
			],
			"example": [
				"a(3) = 5. The second square has coordinates (0,1) and the sum of the first two numbers is 1 + 2 = 3, which is prime. Therefore, to move as far away from the origin as possible, a step to (1,2) is taken, which has a square distance of 5 units from the origin. Note that a step to (-1,2) could have also been taken and would lead to the same walk by symmetry.",
				"a(6) = 18 as a(5) is at coordinates (3,4) and the sum of the last two square numbers is 4 + 5 = 9, which is composite. Therefore, to step to a square as close as possible to the origin, a step to (3,3) is taken, which has a square distance of 18 units from the origin.",
				"a(9) = 41 as a(8) is at coordinates (5,5) and the sum of the last two square numbers is 7 + 8 = 15, which is composite. Two squares as close as possible to the origin are available, (4,5) and (5,4), both of which have a square distance from the origin of 41 units. Since (4,5) has a square distance of 32 units from the square numbered 2, and (5,4) has a square distance of 34 units from 2, the former is chosen."
			],
			"xref": [
				"Cf. A272763, A272773, A000040, A002808."
			],
			"keyword": "nonn,fini,walk",
			"offset": "1,3",
			"author": "_Eric Angelini_ and _Scott R. Shannon_, May 29 2021",
			"references": 1,
			"revision": 26,
			"time": "2021-05-31T21:34:03-04:00",
			"created": "2021-05-31T21:34:03-04:00"
		}
	]
}