{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A213080",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 213080,
			"data": "1,0,4,6,3,3,5,0,6,6,7,7,0,5,0,3,1,8,0,9,8,0,9,5,0,6,5,6,9,7,7,7,6,0,3,7,1,0,1,9,7,4,2,1,8,1,1,3,2,6,4,4,4,2,4,4,1,5,8,7,5,3,4,0,4,2,0,3,5,7,5,1,5,6,3,7,4,4,5,7,0,7,2,5,4,8,5,8",
			"name": "Decimal expansion of Product_{n\u003e=1} n! /(sqrt(2*Pi*n) * (n/e)^n * (1+1/n)^(1/12)).",
			"comment": [
				"Just as Stirling's formula for the asymptotic expansion of n! involves the constant sqrt{2 Pi}, the asymptotic expansion of the product of all binomial coefficients in a row of Pascal's triangle involves a constant, the reciprocal of the constant C defined and evaluated here."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A213080/b213080.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Michael D. Hirschhorn, \u003ca href=\"/A213080/a213080.pdf\"\u003eOn the asymptotic behavior of Product_{k=0..n} C(n,k)\u003c/a\u003e, Fib. Q., 51 (2013), 163-173.",
				"Bernd C. Kellner, \u003ca href=\"http://arxiv.org/abs/math.NT/0604505\"\u003eOn asymptotic constants related to products of Bernoulli numbers and factorials\u003c/a\u003e, arXiv:math/0604505 [math.NT], p. 7."
			],
			"formula": [
				"Let A denote the Glaisher-Kinkelin constant. Then",
				"C = (exp(1)^(1/12)*(2*Pi)^(1/4))/A^2 = exp(2*zeta'(-1)-1/12)*(2*Pi)^(1/4).",
				"A closely related constant is K = Product_{n\u003e=1} (n!*(e/n)^(n+1/2))/ ((1+1/(n+1/2))^(1/12)*sqrt(2*Pi*e)) = (2^(1/6)*(3*e)^(1/12)*Pi^(1/4))/A^2 = exp(2*zeta'(-1)-1/12)*2^(1/6)*3^(1/12)*Pi^(1/4) = 1.082293504658977773529439... - _Peter Luschny_, Jun 22 2012",
				"sqrt(C) = Limit_{n\u003e=1} (Product_{k=1..n-1} k!) / f(n) where f(n) = (2*Pi)^(n/2-1/8)*exp(1/24-3/4*n^2)*n^(1/2*n^2-1/12). - _Peter Luschny_, Jun 23 2012"
			],
			"example": [
				"1.0463350667705031..."
			],
			"maple": [
				"exp(2*Zeta(1,-1)-1/12)*(2*Pi)^(1/4); evalf(%,100); # _Peter Luschny_, Jun 22 2012"
			],
			"mathematica": [
				"RealDigits[(Exp[1]^(1/12) (2 Pi)^(1/4))/Glaisher^2, 10, 100][[1]] (*_Peter Luschny_, Jun 22 2012 *)"
			],
			"program": [
				"(Sage)",
				"import mpmath",
				"mpmath.mp.pretty=True; mpmath.mp.dps = 200 #precision",
				"mpmath.exp(2*mpmath.zeta(-1,1,1)-1/12)*(2*pi)^(1/4) # _Peter Luschny_, Jun 22 2012",
				"(PARI) exp(2*zeta'(-1)-1/12)*(2*Pi)^(1/4) \\\\ _Charles R Greathouse IV_, Dec 12 2013"
			],
			"xref": [
				"Cf. A074962, A000178, A084448, A241140, A272097."
			],
			"keyword": "nonn,cons",
			"offset": "1,3",
			"author": "_Michael David Hirschhorn_, Jun 04 2012",
			"references": 3,
			"revision": 47,
			"time": "2018-07-27T04:34:30-04:00",
			"created": "2012-06-26T16:52:25-04:00"
		}
	]
}