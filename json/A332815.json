{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A332815",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 332815,
			"data": "1,2,3,4,5,6,9,8,7,10,15,12,25,18,27,16,13,14,21,20,35,30,45,24,49,50,75,36,125,54,81,32,11,26,39,28,65,42,63,40,91,70,105,60,175,90,135,48,169,98,147,100,245,150,225,72,343,250,375,108,625,162,243,64,17,22,33,52,55,78,117,56,77,130,195,84",
			"name": "a(n) = A108548(A005940(1+n)).",
			"comment": [
				"This is variant of Doudna-sequence, A005940 and thus can be represented as a binary tree. Each child to the left is obtained by applying A332818 to the parent, and each child to the right is obtained by doubling the parent:",
				"                                      1",
				"                                      |",
				"                   ...................2...................",
				"                  3                                       4",
				"        5......../ \\........6                   9......../ \\........8",
				"       / \\                 / \\                 / \\                 / \\",
				"      /   \\               /   \\               /   \\               /   \\",
				"     /     \\             /     \\             /     \\             /     \\",
				"    7       10         15       12         25       18         27       16",
				"  13 14   21  20     35  30   45  24     49  50   75  36    125  54   81  32",
				"etc.",
				"Note the indexing: the sequence starts with a(0)=1, as is natural for sequences based on maps from base-2 expansion to prime factorization. This is",
				"in contrast to A005940, which for historical reasons starts from offset 1.",
				"For any n \u003e 1, A332893(n) gives the value of the parent node. For any n \u003e= 1, A332894(n) gives the distance to 1, and A332899(n) gives the number of odd numbers that occur (inclusively) on the path from 1 to n."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A332815/b332815.txt\"\u003eTable of n, a(n) for n = 0..8191\u003c/a\u003e",
				"Antti Karttunen, \u003ca href=\"/A332815/a332815.txt\"\u003eData supplement: n, a(n) computed for n = 0..65537\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A108548(A005940(1+n))."
			],
			"program": [
				"(PARI)",
				"up_to = 26927;",
				"A005940(n) = { my(p=2, t=1); n--; until(!n\\=2, if((n%2), (t*=p), p=nextprime(p+1))); t }; \\\\ From A005940",
				"A108546list(up_to) = { my(v=vector(up_to), p,q); v[1] = 2; v[2] = 3; v[3] = 5; for(n=4,up_to, p = v[n-2]; q = nextprime(1+p); while(q%4 != p%4, q=nextprime(1+q)); v[n] = q); (v); };",
				"v108546 = A108546list(up_to);",
				"A108546(n) = v108546[n];",
				"A108548(n) = { my(f=factor(n)); f[,1] = apply(A108546,apply(primepi,f[,1])); factorback(f); };",
				"A332815(n) = A108548(A005940(1+n));"
			],
			"xref": [
				"Cf. A332816 (inverse permutation).",
				"Cf. A005940, A108548, A332818, A332893, A332894, A332897, A332898, A332899.",
				"Cf. A108546 (the left edge of the tree from 2 downward)."
			],
			"keyword": "nonn,tabf",
			"offset": "0,2",
			"author": "_Antti Karttunen_, Feb 28 2020",
			"references": 10,
			"revision": 17,
			"time": "2020-03-04T18:08:24-05:00",
			"created": "2020-03-04T18:08:24-05:00"
		}
	]
}