{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A114205",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 114205,
			"data": "5,0,25,2,1,0,125,0,1,0,8,0,0,0,625,0,0,0,5,0,0,0,41,4,0,0,3,0,0,0,3125,0,0,0,2,0,0,0,25,0,0,0,2,0,0,0,208,0,2,0,1,0,0,0,17,0,0,0,1,0,0,0,15625,0,0,0,1,0,0,0,13,0,0,1,1,0,0,0,125,0,0,0,1,0,0,0,11,0,0",
			"name": "Write decimal expansion of 1/n as 0.PPP...PQQQ..., where QQQ... is the cyclic part. If the expansion does not terminate, any leading 0's in QQQ... are regarded as being at the end of the PPP...P part. Sequence gives PPP...P, right justified, with leading zeros omitted.",
			"comment": [
				"b(n) = A114206(n) gives the length of P (including leading zeros), c(n) = A036275(n) gives the smallest cycle in QQQ... (including terminating zeros) and d(n) = A051626(n) gives the length of that cycle.",
				"Thus 1/n = 10^(-b(n)) * ( a(n) + c(n)/(10^d(n) - 1) ). When c(n)=d(n)=0, the fraction c(n)/(10^d(n) - 1), which is 0/0, evaluates (by definition) to 0."
			],
			"example": [
				"n .. expansion of 1/n .... a b c d",
				"2 .50000000000000000000... 5 1 0 0",
				"3 .33333333333333333333... 0 0 3 1",
				"4 .25000000000000000000... 25 2 0 0",
				"5 .20000000000000000000... 2 1 0 0",
				"6 .16666666666666666667... 1 1 6 1",
				"7 .14285714285714285714... 0 0 142857 6",
				"8 .12500000000000000000... 125 3 0 0",
				"9 .11111111111111111111... 0 0 1 1",
				"10 .1000000000000000000... 1 1 0 0",
				"11 .0909090909090909090... 0 1 90 2",
				"12 .0833333333333333333... 8 2 3 1",
				"13 .0769230769230769230... 0 1 769230 6",
				"14 .0714285714285714285... 0 1 714285 6",
				"15 .0666666666666666666... 0 1 6 1",
				"16 .0625000000000000000... 625 4 0 0"
			],
			"maple": [
				"A114205 := proc(n) local sh,lpow,mpow,a,b ; lpow:=1 ; while true do for mpow from lpow-1 to 0 by -1 do if (10^lpow-10^mpow) mod n =0 then a := (10^lpow-10^mpow)/n ; sh := 10^(lpow-mpow)-1 ; b := a mod sh ; a := floor(a/sh) ; while b\u003e0 and b*10 \u003c sh+1 do a := 10*a ; b := 10*b ; end ; RETURN(a) ; fi ; od ; lpow := lpow+1 ; od ; end: for n from 2 to 600 do printf(\"%d %d \",n,A114205(n)) ; od ; # _R. J. Mathar_, Oct 19 2006"
			],
			"mathematica": [
				"fa[n_] := Block[{p},p = First[RealDigits[1/n]];If[ ! IntegerQ[Last[p]], p = Most[p]];FromDigits[p]];Table[fa[n], {n, 100}] (* _Ray Chandler_, Oct 18 2006 *)",
				"Mathematica code from _Hans Havermann_, Oct 19 2006:",
				"r[x_] := RealDigits[1/x]",
				"w[x_] := First[r[x]]",
				"f[x_] := First[w[x]]",
				"l[x_] := Last[w[x]]",
				"z[x_] := Last[r[x]]",
				"a[x_] := Which[IntegerQ[l[x]], FromDigits[w[x]], IntegerQ[f[x]] ==False, 0, True, FromDigits[Drop[w[x],-1]]]",
				"b[x_] := Which[IntegerQ[l[x]], Length[w[x]]-1*z[x], IntegerQ[f[x]] ==False, -1*z[x], True, Length[Drop[w[x],-1]]-1*z[x]]",
				"c[x_] := Which[IntegerQ[l[x]], 0, IntegerQ[f[x]]==False, FromDigits[f[x]], True, FromDigits[l[x]]]",
				"d[x_] := Which[IntegerQ[l[x]], 0, IntegerQ[f[x]]==False, Length[f[x]], True, Length[l[x]]]"
			],
			"xref": [
				"Cf. A114206, A036275, A051626, A060284, A007732."
			],
			"keyword": "nonn,base",
			"offset": "2,1",
			"author": "_N. J. A. Sloane_, Oct 17 2006",
			"ext": [
				"More terms from _Ray Chandler_ and _Hans Havermann_, Oct 18 2006",
				"I would also like to get programs that produce this and A114206, A036275, A051626 in Maple."
			],
			"references": 7,
			"revision": 17,
			"time": "2019-11-01T16:07:51-04:00",
			"created": "2006-01-24T03:00:00-05:00"
		}
	]
}