{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A004736",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 4736,
			"data": "1,2,1,3,2,1,4,3,2,1,5,4,3,2,1,6,5,4,3,2,1,7,6,5,4,3,2,1,8,7,6,5,4,3,2,1,9,8,7,6,5,4,3,2,1,10,9,8,7,6,5,4,3,2,1,11,10,9,8,7,6,5,4,3,2,1,12,11,10,9,8,7,6,5,4,3,2,1,13,12,11,10,9,8,7,6,5,4,3,2,1,14,13,12,11,10,9",
			"name": "Triangle read by rows: row n lists the first n positive integers in decreasing order.",
			"comment": [
				"Old name: Triangle T(n,k) = n-k, n \u003e= 1, 0 \u003c= k \u003c n. Fractal sequence formed by repeatedly appending strings m m-1 . . . 2 1.",
				"The PARI functions t1 (this sequence), t2 (A002260) can be used to read a square array T(n,k) (n \u003e= 1, k \u003e= 1) by antidiagonals upwards: n -\u003e T(t1(n), t2(n)). - _Michael Somos_, Aug 23 2002, edited by _M. F. Hasler_, Mar 31 2020",
				"A004736 is the mirror of the self-fission of the polynomial sequence (q(n,x)) given by q(n,x) = x^n+  x^(n-1) + ... + x + 1. See A193842 for the definition of fission. - _Clark Kimberling_, Aug 07 2011",
				"Seen as flattened list: a(A000217(n)) = 1; a(A000124(n)) = n and a(m) \u003c\u003e n for m \u003c A000124(n). - _Reinhard Zumkeller_, Jul 22 2012",
				"Sequence B is called a reverse reluctant sequence of sequence A, if B is triangle array read by rows: row number k lists first k elements of the sequence A in reverse order. Sequence A004736 is the reverse reluctant sequence of sequence 1,2,3,... (A000027). - _Boris Putievskiy_, Dec 13 2012",
				"The row sums equal A000217(n). The alternating row sums equal A004526(n+1). The antidiagonal sums equal A002620(n+1) respectively A008805(n-1). - _Johannes W. Meijer_, Sep 28 2013",
				"From _Peter Bala_, Jul 29 2014: (Start)",
				"Riordan array (1/(1-x)^2,x). Call this array M and for k = 0,1,2,... define M(k) to be the lower unit triangular block array",
				"/I_k 0\\",
				"\\ 0  M/",
				"having the k X k identity matrix I_k as the upper left block; in particular, M(0) = M. Then the infinite matrix product M(0)*M(1)*M(2)*... is equal to A078812. (End)",
				"T(n, k) gives the number of subsets of [n] := {1, 2, ..., n} with k consecutive numbers (consecutive k-subsets of [n]).  - _Wolfdieter Lang_, May 30 2018",
				"a(n) gives the distance from (n-1) to the smallest triangular number \u003e (n-1). - _Ctibor O. Zizka_, Apr 09 2020",
				"To construct the sequence, start from 1,2,,3,,,4,,,,5,,,,,6... where there are n commas after each \"n\". Then fill the empty places by the sequence itself. - _Benoit Cloitre_, Aug 17 2021"
			],
			"reference": [
				"H. S. M. Coxeter, Regular Polytopes, 3rd ed., Dover, NY, 1973, pp 159-162."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A004736/b004736.txt\"\u003eRows n = 1..100 of triangle, flattened\u003c/a\u003e",
				"Isabel Cação, Helmuth R. Malonek, Maria Irene Falcão and Graça Tomaz, \u003ca href=\"https://www.emis.de/journals/JIS/VOL21/Falcao/falcao2.html\"\u003eCombinatorial Identities Associated with a Multidimensional Polynomial Sequence\u003c/a\u003e, J. Int. Seq., Vol. 21 (2018), Article 18.7.4.",
				"Glen Joyce C. Dulatre, Jamilah V. Alarcon, Vhenedict M. Florida and Daisy Ann A. Disu, \u003ca href=\"https://web.archive.org/web/20190407151217/http://www.dmmmsu-sluc.com/wp-content/uploads/2018/03/CAS-Monitor-2016-2017-1.pdf\"\u003eOn Fractal Sequences\u003c/a\u003e, DMMMSU-CAS Science Monitor (2016-2017) Vol. 15 No. 2, 109-113.",
				"Clark Kimberling, \u003ca href=\"http://faculty.evansville.edu/ck6/integer/fractals.html\"\u003eFractal sequences\u003c/a\u003e",
				"Clark Kimberling, \u003ca href=\"http://matwbn.icm.edu.pl/ksiazki/aa/aa73/aa7321.pdf\"\u003eNumeration systems and fractal sequences\u003c/a\u003e, Acta Arithmetica 73 (1995) 103-117.",
				"Boris Putievskiy, \u003ca href=\"http://arxiv.org/abs/1212.2732\"\u003eTransformations Integer Sequences And Pairing Functions\u003c/a\u003e arXiv:1212.2732 [math.CO], 2012.",
				"F. Smarandache, \u003ca href=\"http://www.gallup.unm.edu/~smarandache/Sequences-book.pdf\"\u003eSequences of Numbers Involved in Unsolved Problems\u003c/a\u003e.",
				"Michael Somos, \u003ca href=\"/A073189/a073189.txt\"\u003eSequences used for indexing triangular or square arrays\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/SmarandacheSequences.html\"\u003eSmarandache Sequences\u003c/a\u003e"
			],
			"formula": [
				"a(n+1) = 1 + A025581(n).",
				"a(n) = (2 - 2*n + round(sqrt(2*n)) + round(sqrt(2*n))^2)/2. - _Brian Tenneson_, Oct 11 2003",
				"G.f.: 1 / ((1-x)^2 * (1-x*y)). - _Ralf Stephan_, Jan 23 2005",
				"Recursion: e(n,k) = (e(n - 1, k)*e(n, k - 1) + 1)/e(n - 1, k - 1). - _Roger L. Bagula_, Mar 25 2009",
				"a(n) = (t*t+3*t+4)/2-n, where t = floor((-1+sqrt(8*n-7))/2). - _Boris Putievskiy_, Dec 13 2012",
				"From _Johannes W. Meijer_, Sep 28 2013: (Start)",
				"T(n, k) = n - k + 1, n \u003e= 1 and 1 \u003c= k \u003c= n.",
				"T(n, k) = A002260(n+k-1, n-k+1). (End)",
				"a(n) = A000217(A002024(n)) - n + 1. - _Enrique Pérez Herrero_, Aug 29 2016"
			],
			"example": [
				"The triangle T(n, k) starts:",
				"n\\k  1   2   3  4  5  6  7  8  9 10 11 12 ...",
				"1:   1",
				"2:   2   1",
				"3:   3   2   1",
				"4:   4   3   2  1",
				"5:   5   4   3  2  1",
				"6:   6   5   4  3  2  1",
				"7:   7   6   5  4  3  2  1",
				"8:   8   7   6  5  4  3  2  1",
				"9:   9   8   7  6  5  4  3  2  1",
				"10: 10   9   8  7  6  5  4  3  2  1",
				"11: 11  10   9  8  7  6  5  4  3  2  1",
				"12: 12  11  10  9  8  7  6  5  4  3  2  1",
				"... Reformatted. - _Wolfdieter Lang_, Feb 04 2015",
				"T(6, 3) = 4 because the four consecutive 3-subsets of [6] = {1, 2, ..., 6} are {1, 2, 3}, {2, 3, 4}, {3, 4, 5} and {4, 5, 6}. - _Wolfdieter Lang_, May 30 2018"
			],
			"maple": [
				"A004736 := proc(n,m) n-m+1 ; end:",
				"T := (n, k) -\u003e n-k+1: seq(seq(T(n,k), k=1..n), n=1..13); # _Johannes W. Meijer_, Sep 28 2013"
			],
			"mathematica": [
				"Flatten[ Table[ Reverse[ Range[n]], {n, 12}]] (* _Robert G. Wilson v_, Apr 27 2004 *)",
				"Table[Range[n,1,-1],{n,20}]//Flatten (* _Harvey P. Dale_, May 27 2020 *)"
			],
			"program": [
				"(PARI) {a(n) = 1 + binomial(1 + floor(1/2 + sqrt(2*n)), 2) - n}",
				"(PARI) {t1(n) = binomial( floor(3/2 + sqrt(2*n)), 2) - n + 1} /* A004736 */",
				"(PARI) {t2(n) = n - binomial( floor(1/2 + sqrt(2*n)), 2)} /* A002260 */",
				"(PARI) apply( A004736(n)=1-n+(n=sqrtint(8*n)\\/2)*(n+1)\\2, [1..99]) \\\\ _M. F. Hasler_, Mar 31 2020",
				"(Excel) =if(row()\u003e=column();row()-column()+1;\"\") [_Mats Granvik_, Jan 19 2009]",
				"(Haskell)",
				"a004736 n k = n - k + 1",
				"a004736_row n = a004736_tabl !! (n-1)",
				"a004736_tabl = map reverse a002260_tabl",
				"-- _Reinhard Zumkeller_, Aug 04 2014, Jul 22 2012",
				"(Python)",
				"def agen(rows):",
				"    for n in range(1, rows+1): yield from range(n, 0, -1)",
				"print([an for an in agen(13)]) # _Michael S. Branicky_, Aug 17 2021"
			],
			"xref": [
				"Cf. A000217, A002024, A002262, A003056, A025581.",
				"Ordinal transform of A002260. See also A078812.",
				"Cf. A141419 (partial sums per row).",
				"Cf. A134546 (T * A051731, matrix product).",
				"See A001511 for definition of ordinal transform."
			],
			"keyword": "nonn,easy,tabl,nice",
			"offset": "1,2",
			"author": "R. Muller",
			"ext": [
				"New name from _Omar E. Pol_, Jul 15 2012"
			],
			"references": 322,
			"revision": 149,
			"time": "2021-08-18T10:14:48-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}