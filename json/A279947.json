{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A279947",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 279947,
			"data": "1,-1,2,-2,0,-1,0,-2,3,-2,2,0,0,-2,0,0,3,0,4,-2,0,-1,0,-4,2,0,2,0,0,-2,0,0,2,-3,2,-2,0,-2,0,-2,3,-2,2,0,0,0,0,0,4,0,2,-4,0,-2,0,-2,1,0,6,0,0,0,0,0,2,-3,2,-2,0,0,0,-2,4,-4,2,0,0,-2,0",
			"name": "Expansion of f(x^2, x^2) * f(-x, -x^5) in powers of x where f(, ) is Ramanujan's general theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A279947/b279947.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of chi(-x) * phi(x^2) * psi(x^3) in powers of x where phi(), psi(), chi() are Ramanujan theta functions.",
				"Expansion of q^(-1/3) * eta(q) * eta(q^4)^5 * eta(q^6)^2 / (eta(q^2)^3 * eta(q^3) * eta(q^8)^2) in powers of q.",
				"Euler transform of period 24 sequence [ -1, 2, 0, -3, -1, 1, -1, -1, 0, 2, -1, -4, -1, 2, 0, -1, -1, 1, -1, -3, 0, 2, -1, -2, ...].",
				"a(n) = b(3*n + 1) where b() is multiplicative with b(2^e) = -(-1)^e if e\u003e0, b(3^e) = (-1)^e, b(p^e) = e+1 if p == 1, 7 (mod 24), b(p^e) = (e+1) * (-1)^e if p == 5, 11 (mod 24), b(p^e) = (1 + (-1)^e) / 2 if p == 13, 17, 19, 23 (mod 24).",
				"a(n) = (-1)^n * A263571(n) = A128581(3*n + 1) = - A190611(3*n + 1) = - A261122(6*n + 2).",
				"a(2*n) = A261115(n).",
				"a(2*n + 1) = - A263548(n).",
				"a(8*n + 4) = a(8*n + 6) = 0.",
				"a(4*n + 1) = -a(n).",
				"a(4*n + 3) = -2 * A128582(n).",
				"a(8*n) = A113780(n).",
				"a(8*n + 2) = 2 * A260089(n).",
				"a(16*n + 3) = -2 * A128583(n).",
				"a(16*n + 7) = -2 * A128591(n)."
			],
			"example": [
				"G.f. = 1 - x + 2*x^2 - 2*x^3 - x^5 - 2*x^7 + 3*x^8 - 2*x^9 + 2*x^10 + ...",
				"G.f. = q - q^4 + 2*q^7 - 2*q^10 - q^16 - 2*q^22 + 3*q^25 - 2*q^28 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 0, 0, With[ {m = 3 n + 1}, (-1)^n DivisorSum[ m, KroneckerSymbol[ 2, #] KroneckerSymbol[ -3, m/#] \u0026]]];",
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 3, 0, x^2] QPochhammer[ x^6] QPochhammer[ x, x^6] QPochhammer[ x^5, x^6], {x, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = my(m); if( n\u003c0, 0, m = 3*n + 1; (-1)^n * sumdiv( m, d, kronecker( 2, d) * kronecker( -3, m/d)))};",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A) * eta(x^4 + A)^5 * eta(x^6 + A)^2 / (eta(x^2 + A)^3 * eta(x^3 + A) * eta(x^8 + A)^2), n))};",
				"(PARI) {a(n) = my(A, p, e); if( n\u003c0, 0, A = factor(3*n + 1); prod( k=1, matsize(A)[1], [p, e] = A[k, ]; if( p==2, -(-1)^e, p==3, (-1)^e, p%24==1 || p%24==7, e+1, p%24==5 || p%24==11, (e+1)*(-1)^e, !(e%2))))};"
			],
			"xref": [
				"Cf. A113780, A128581, A128582, A128583, A128591, A190611, A260089, A261115, A261122, A263548, A263571."
			],
			"keyword": "sign",
			"offset": "0,3",
			"author": "_Michael Somos_, Dec 23 2016",
			"references": 1,
			"revision": 14,
			"time": "2021-03-12T22:24:48-05:00",
			"created": "2016-12-23T11:46:27-05:00"
		}
	]
}