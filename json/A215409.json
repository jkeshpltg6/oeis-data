{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A215409",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 215409,
			"data": "3,3,3,2,1,0",
			"name": "The Goodstein sequence G_n(3).",
			"comment": [
				"G_0(m) = m. To get the 2nd term, write m in hereditary base 2 notation (see links), change all the 2s to 3s, and then subtract 1 from the result. To get the 3rd term, write the 2nd term in hereditary base 3 notation, change all 3s to 4s, and subtract 1 again. Continue until the result is zero (by Goodstein's Theorem), when the sequence terminates.",
				"Decimal expansion of 33321/100000. - _Natan Arie Consigli_, Jan 23 2015"
			],
			"link": [
				"R. L. Goodstein, \u003ca href=\"http://www.jstor.org/stable/2268019\"\u003eOn the Restricted Ordinal Theorem\u003c/a\u003e, J. Symb. Logic 9, 33-41, 1944.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/HereditaryRepresentation.html\"\u003eHereditary Representation\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/GoodsteinSequence.html\"\u003eGoodstein Sequence\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/GoodsteinsTheorem.html\"\u003eGoodstein's Theorem\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Goodstein\u0026#39;s_theorem#Hereditary_base-n_notation\"\u003eHereditary base-n notation\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Goodstein\u0026#39;s_theorem#Goodstein_sequences\"\u003eGoodstein sequence\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Goodstein\u0026#39;s_theorem\"\u003eGoodstein's Theorem\u003c/a\u003e",
				"Reinhard Zumkeller, \u003ca href=\"/A211378/a211378.hs.txt\"\u003eHaskell programs for Goodstein sequences\u003c/a\u003e"
			],
			"formula": [
				"a(0) = a(1) = a(2) = 3; a(3) = 2; a(4) = 1; a(n) = 0, n \u003e 4;",
				"From _Iain Fox_, Dec 12 2017: (Start)",
				"G.f.: 3 + 3*x + 3*x^2 + 2*x^3 + x^4.",
				"E.g.f.: 3 + 3*x + (3/2)*x^2 + (1/3)*x^3 + (1/24)*x^4.",
				"a(n) = floor(2 - (4/Pi)*arctan(n-3)), n \u003e= 0.",
				"(End)"
			],
			"example": [
				"a(0) = 3 = 2^1 + 1;",
				"a(1) = 3^1 + 1 - 1 = 3^1 = 3;",
				"a(2) = 4^1 - 1 = 3;",
				"a(3) = 3 - 1 = 2;",
				"a(4) = 2 - 1 = 1;",
				"a(5) = 1 - 1 = 0."
			],
			"mathematica": [
				"PadRight[CoefficientList[Series[3 + 3 x + 3 x^2 + 2 x^3 + x^4, {x, 0, 4}], x], 6] (* _Michael De Vlieger_, Dec 12 2017 *)"
			],
			"program": [
				"(Haskell)  see Link",
				"(PARI) B(n, b)=sum(i=1, #n=digits(n, b), n[i]*(b+1)^if(#n\u003cb+i, #n-i, B(#n-i, b)))",
				"a(n) = my(x=3); for(i=1, n, x=B(x, i+1)-1; if(x==0, break())); x \\\\ (uses definition of sequence) _Iain Fox_, Dec 13 2017",
				"(PARI) first(n) = my(res = vector(n)); res[1] = res[2] = res[3] = 3; res[4] = 2; res[5] = 1; res; \\\\ _Iain Fox_, Dec 12 2017",
				"(PARI) first(n) = Vec(3 + 3*x + 3*x^2 + 2*x^3 + x^4 + O(x^n)) \\\\ _Iain Fox_, Dec 12 2017",
				"(PARI) a(n) = floor(2 - (4/Pi)*atan(n-3)) \\\\ _Iain Fox_, Dec 12 2017"
			],
			"xref": [
				"Cf. A056004, A057650, A059934, A059935, A059936, A271977.",
				"Cf. A056041, A056193, A266204, A271554, A222117, A059933, A211378."
			],
			"keyword": "cons,easy,nonn,fini,full",
			"offset": "0,1",
			"author": "_Jonathan Sondow_, Aug 10 2012",
			"ext": [
				"Corrected by _Natan Arie Consigli_, Jan 23 2015"
			],
			"references": 24,
			"revision": 55,
			"time": "2020-01-11T15:57:46-05:00",
			"created": "2012-08-10T13:38:13-04:00"
		}
	]
}