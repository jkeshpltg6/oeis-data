{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A197739",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 197739,
			"data": "4,7,7,6,5,8,3,0,9,0,6,2,2,5,4,6,3,9,0,8,1,9,2,8,5,5,1,2,5,7,8,7,8,8,7,7,1,2,1,7,0,7,3,4,7,5,0,5,0,0,2,7,4,5,4,7,9,8,4,9,0,6,4,6,6,0,9,5,6,0,2,2,9,5,1,9,8,8,2,2,7,6,9,3,6,9,5,8,0,1,2,9,2,8,1,4,0,3,6",
			"name": "Decimal expansion of least x\u003e0 having sin(2x)=3*sin(6x).",
			"comment": [
				"This constant is the least x\u003e0 for which the function f(x)=(sin(x))^2+(cos(3x))^2 has its maximal value.  Least positive solutions of the equations f(x)=m/2, f(x)=m/3, f(x)=1, and f(x)=1/2 are given by sequences shown in the guide below.",
				"In general, suppose that b and c are distinct positive real numbers.  Let f(x)=(sin(bx))^2+cos((cx))^2.  The extrema of f are the solutions of b*sin(2bx)=c*sin(2cx).",
				"In the following guide, constants x given by the sequences (or explicit number) listed for each b,c are, in this order:",
				"(1) least x\u003e0 such that f(x)=(its maximum, m)",
				"(2) m, the maximum of f",
				"(3) least x\u003e0 having f(x)=m/2",
				"(4) least x\u003e0 having f(x)=m/3",
				"(5) least x\u003e0 having f(x)=1",
				"(6) least x\u003e0 having f(x)=1/2",
				"...",
				"(b,c)=(1,2):  A195700, x=25/16, A197589, A197591,",
				"  A019670, A197592",
				"(b,c)=(1,3):  A197739, A197588, A197590, A197755,",
				"  A003881, A197488",
				"(b,c)=(1,4):  A197758, A197759, A197760, A197761,",
				"  A019692 (x=pi/5), A003881",
				"(b,c)=(1,pi): A197821, A197822, A197823, A197824,",
				"  A197726, A197826",
				"(b,c)=(1,2*pi): A197827, A197828, A197829, A197830,",
				"  A197700, A197832",
				"(b,c)=(1,3*pi): A197833, A197834, A197835, A197836,",
				"  A197837, A197838"
			],
			"example": [
				"x=0.47765830906225463908192855125787887712170734750500..."
			],
			"mathematica": [
				"b = 1; c = 3;",
				"f[x_] := Cos[b*x]^2; g[x_] := Sin[c*x]^2; s[x_] := f[x] + g[x];",
				"r = x /. FindRoot[b*Sin[2 b*x] == c*Sin[2 c*x], {x, .47, .48}, WorkingPrecision -\u003e 110]",
				"RealDigits[r]  (* A197739 *)",
				"m = s[r]",
				"RealDigits[m]  (* A197588 *)",
				"Plot[{b*Sin[2 b*x], c*Sin[2 c*x]}, {x, 0, Pi}]",
				"d = m/2; t = x /. FindRoot[s[x] == d, {x, 0.7, 0.8}, WorkingPrecision -\u003e 110]",
				"RealDigits[t]  (* A197590 *)",
				"Plot[{s[x], d}, {x, 0, Pi}, AxesOrigin -\u003e {0, 0}]",
				"d = m/3; t = x /. FindRoot[s[x] == d, {x, 0.8, 0.9}, WorkingPrecision -\u003e 110]",
				"RealDigits[t]  (* A197755 *)",
				"Plot[{s[x], d}, {x, 0, Pi}, AxesOrigin -\u003e {0, 0}]",
				"d = 1; t = x /. FindRoot[s[x] == d, {x, 0.7, 0.8}, WorkingPrecision -\u003e 110]",
				"RealDigits[t]  (* A003881 *)",
				"Plot[{s[x], d}, {x, 0, Pi}, AxesOrigin -\u003e {0, 0}]",
				"d = 1/2; t = x /. FindRoot[s[x] == d, {x, .9, .93}, WorkingPrecision -\u003e 110]",
				"RealDigits[t]  (* A197488 *)",
				"Plot[{s[x], d}, {x, 0, Pi}, AxesOrigin -\u003e {0, 0}]",
				"RealDigits[ ArcTan[ Sqrt[ 2-Sqrt[3] ] ], 10, 99] // First (* _Jean-François Alcover_, Feb 27 2013 *)"
			],
			"xref": [
				"Cf. A197739, A197588."
			],
			"keyword": "nonn,cons",
			"offset": "0,1",
			"author": "_Clark Kimberling_, Oct 18 2011",
			"references": 29,
			"revision": 17,
			"time": "2013-10-02T15:46:39-04:00",
			"created": "2011-10-18T22:10:03-04:00"
		}
	]
}