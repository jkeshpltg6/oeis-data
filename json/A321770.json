{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A321770",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 321770,
			"data": "5,13,29,17,25,73,53,89,169,85,65,97,37,41,137,109,233,425,205,193,305,125,185,505,349,505,985,509,337,481,173,149,373,241,277,565,305,157,205,65,61,221,185,445,797,377,389,629,265,493,1325,905,1261,2477",
			"name": "Consider the tree of triples P(n, k) with n \u003e 0 and 0 \u003c k \u003c= 3^(n-1), such that P(1, 1) = [3; 4; 5] and each triple t on some row branches to the triples A*t, B*t, C*t on the next row (with A = [1, -2, 2; 2, -1, 2; 2, -2, 3], B = [1, 2, 2; 2, 1, 2; 2, 2, 3] and C = [-1, 2, 2; -2, 1, 2; -2, 2, 3]); T(n, k) is the third component of P(n, k).",
			"comment": [
				"The tree P runs uniquely through every primitive Pythagorean triple.",
				"See A321768 for additional comments about P.",
				"All terms are odd."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A321770/b321770.txt\"\u003eRows n = 1..9, flattened\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Tree_of_primitive_Pythagorean_triples\"\u003eTree of primitive Pythagorean triples\u003c/a\u003e",
				"\u003ca href=\"/index/Ps#PyTrip\"\u003eIndex entries related to Pythagorean Triples\u003c/a\u003e"
			],
			"formula": [
				"a(n)^2 = A321768(n)^2 + A321769(n)^2.",
				"Empirically:",
				"- T(n, 1) = A001844(n),",
				"- T(n, (3^(n-1) + 1)/2) = A001653(n+1),",
				"- T(n, 3^(n-1)) = A053755(n)."
			],
			"example": [
				"The first rows are:",
				"   5",
				"   13, 29, 17",
				"   25, 73, 53, 89, 169, 85, 65, 97, 37"
			],
			"program": [
				"(PARI) M = [[1, -2, 2; 2, -1, 2; 2, -2, 3], [1, 2, 2; 2, 1, 2; 2, 2, 3], [-1, 2, 2; -2, 1, 2; -2, 2, 3]];",
				"T(n, k) = my (t=[3; 4; 5], d=digits(3^(n-1)+k-1, 3)); for (i=2, #d, t = M[d[i]+1] * t); return (t[3, 1])"
			],
			"xref": [
				"See A321768 and A321769 for the other components.",
				"Cf. A001653, A001844, A053755."
			],
			"keyword": "nonn,tabf",
			"offset": "1,1",
			"author": "_Rémy Sigrist_, Nov 18 2018",
			"references": 7,
			"revision": 13,
			"time": "2018-12-01T04:54:16-05:00",
			"created": "2018-11-29T16:02:58-05:00"
		}
	]
}