{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A156140",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 156140,
			"data": "-1,0,1,3,2,7,5,8,3,13,10,17,7,18,11,15,4,21,17,30,13,35,22,31,9,32,23,37,14,33,19,24,5,31,26,47,21,58,37,53,16,59,43,70,27,65,38,49,11,50,39,67,28,73,45,62,17,57,40,63,23,52,29,35,6,43,37,68,31,87,56,81,25,94,69",
			"name": "Accumulation of Stern's diatomic series: a(0)=-1, a(1)=0, and a(n+1) = (2e(n)+1)*a(n) - a(n-1) for n \u003e 1, where e(n) is the highest power of 2 dividing n.",
			"link": [
				"Charles R Greathouse IV, \u003ca href=\"/A156140/b156140.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e"
			],
			"formula": [
				"Let b(n) = A002487(n), Stern's diatomic series.",
				"a(n+1)*b(n) - a(n)*b(n+1) = 1 for n \u003e= 0.",
				"a(2n+1) = a(n) + a(n+1) + b(n) + b(n+1) for n \u003e= 0.",
				"a(2n) = a(n) + b(n) for n \u003e= 0.",
				"a(2^n + k) = -n*a(k) + (n^2 + n + 1)*b(k) for 0 \u003c= k \u003c= 2^n.",
				"b(2^n + k) = -a(k) + (n + 1)*b(k) for 0 \u003c= k \u003c= 2^n.",
				"a(2^m + k) = b(2^m+k)*m + b(k), m \u003e= 0, 0 \u003c= k \u003c 2^m. - _Yosu Yurramendi_, Mar 09 2018",
				"a(2^(m+1)+2^m+1) = 2*m+1, m \u003e= 0. - _Yosu Yurramendi_, Mar 09 2018",
				"From _Yosu Yurramendi_, May 08 2018: (Start)",
				"a(2^m) = m, m \u003e= 0.",
				"a(2^r*(2*k+1)) = a(2^r*(2*k)) + a(2^r*(2*k+2)), r = m - floor(log_2(k)) - 1, m \u003e 0, 1 \u003c= k \u003c 2^m.",
				"(End)"
			],
			"maple": [
				"A156140 := proc(n)",
				"    option remember ;",
				"    if n \u003c= 1 then",
				"        n-1 ;",
				"    else",
				"        (2*A007814(n-1)+1)*procname(n-1)-procname(n-2) ;",
				"    end if;",
				"end proc:",
				"seq(A156140(n),n=0..80) ; # _R. J. Mathar_, Mar 14 2009"
			],
			"mathematica": [
				"Fold[Append[#1, (2 IntegerExponent[#2, 2] + 1) #1[[-1]] - #1[[-2]] ] \u0026, {-1, 0}, Range[73]] (* _Michael De Vlieger_, Mar 09 2018 *)"
			],
			"program": [
				"(PARI) first(n)=my(v=vector(n+1)); v[1]=-1; v[2]=0; for(k=1,n-1,v[k+2]=(2*valuation(k,2)+1)*v[k+1] - v[k]); v \\\\ _Charles R Greathouse IV_, Apr 05 2016",
				"(PARI) fusc(n)=my(a=1, b=0); while(n\u003e0, if(bitand(n, 1), b+=a, a+=b); n\u003e\u003e=1); b",
				"a(n)=my(m=1,s,t); if(n==0, return(-1)); while(n%2==0, s+=fusc(n\u003e\u003e=1)); while(n\u003e1, t=logint(n,2); n-=2^t; s+=m*fusc(n)*(t^2+t+1); m*=-t); m*(n-1) + s \\\\ _Charles R Greathouse IV_, Dec 13 2016",
				"(R)",
				"a \u003c- c(0,1)",
				"maxlevel \u003c- 6 # by choice",
				"for(m in 1:maxlevel) {",
				"  a[2^(m+1)] \u003c- m + 1",
				"  for(k in 1:(2^m-1)) {",
				"    r \u003c- m - floor(log2(k)) - 1",
				"    a[2^r*(2*k+1)] \u003c- a[2^r*(2*k)] + a[2^r*(2*k+2)]",
				"}}",
				"a",
				"# _Yosu Yurramendi_, May 08 2018"
			],
			"xref": [
				"Cf. A002487, A007814.",
				"From _Yosu Yurramendi_, Mar 09 2018: (Start)",
				"a(2^m +  0) = A000027(m),   m \u003e= 0.",
				"a(2^m +  1) = A002061(m+2), m \u003e= 1.",
				"a(2^m +  2) = A002522(m),   m \u003e= 2.",
				"a(2^m +  3) = A033816(m-1), m \u003e= 2.",
				"a(2^m +  4) = A002061(m),   m \u003e= 2.",
				"a(2^m +  5) = A141631(m),   m \u003e= 3.",
				"a(2^m +  6) = A084849(m-1), m \u003e= 3.",
				"a(2^m +  7) = A056108(m-1), m \u003e= 3.",
				"a(2^m +  8) = A000290(m-1), m \u003e= 3.",
				"a(2^m +  9) = A185950(m-1), m \u003e= 4.",
				"a(2^m + 10) = A144390(m-1), m \u003e= 4.",
				"a(2^m + 12) = A014106(m-2), m \u003e= 4.",
				"a(2^m + 16) = A028387(m-3), m \u003e= 4.",
				"a(2^m + 18) = A250657(m-4), m \u003e= 5.",
				"a(2^m + 20) = A140677(m-3), m \u003e= 5.",
				"a(2^m + 32) = A028872(m-2), m \u003e= 5.",
				"a(2^m -  1) = A005563(m-1), m \u003e= 0.",
				"a(2^m -  2) = A028387(m-2), m \u003e= 2.",
				"a(2^m -  3) = A033537(m-2), m \u003e= 2.",
				"a(2^m -  4) = A008865(m-1), m \u003e= 3.",
				"a(2^m -  7) = A140678(m-3), m \u003e= 3.",
				"a(2^m -  8) = A014209(m-3), m \u003e= 4.",
				"a(2^m - 16) = A028875(m-2), m \u003e= 5.",
				"a(2^m - 32) = A108195(m-5), m \u003e= 6.",
				"(End)"
			],
			"keyword": "sign,look,easy",
			"offset": "0,4",
			"author": "Arie Werksma (Werksma(AT)Tiscali.nl), Feb 04 2009",
			"references": 5,
			"revision": 43,
			"time": "2021-06-27T07:55:20-04:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}