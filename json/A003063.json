{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003063",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3063,
			"data": "-1,-1,1,11,49,179,601,1931,6049,18659,57001,173051,523249,1577939,4750201,14283371,42915649,128878019,386896201,1161212891,3484687249,10456158899,31372671001,94126401611,282395982049,847221500579,2541731610601,7625329049531,22876255584049",
			"name": "a(n) = 3^(n-1)-2^n.",
			"comment": [
				"Binomial transform of A000918: (-1, 0, 2, 6, 14, 30,...). - _Gary W. Adamson_, Mar 23 2012",
				"This sequence demonstrates 2^n as a loose lower bound for g(n) in Waring's problem. Since 3^n \u003e 2(2^n) for all n \u003e 2, the number 2^(n + 1) - 1 requires 2^n n-th powers for its representation since 3^n is not available for use in the sum: the gulf between the relevant powers of 2 and 3 widens considerably as n gets progressively larger. - _Alonso del Arte_, Feb 01 2013"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A003063/b003063.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"D. Knuth, \u003ca href=\"/A003063/a003063.pdf\"\u003eLetter to N. J. A. Sloane, date unknown\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (5,-6)."
			],
			"formula": [
				"Let b(n) = 2*(3/2)^n - 1. Then a(n) = -b(1-n)*3^(n-1) for n \u003e 0. A083313(n) = A064686(n) = b(n)*2^(n-1) for n \u003e 0. - _Michael Somos_, Aug 06 2006",
				"a(n) = 5*a(n-1)-6*a(n-2). G.f.: x*(4*x-1) / ((2*x-1)*(3*x-1)). - _Colin Barker_, May 27 2013"
			],
			"example": [
				"a(3) = 1 because 3^2 - 2^3 = 9 - 8 = 1.",
				"a(4) = 11 because 3^3 - 2^4 = 27 - 16 = 11.",
				"a(5) = 49 because 3^4 - 2^5 = 81 - 32 = 49."
			],
			"mathematica": [
				"Table[3^(n - 1) - 2^n, {n, 25}] (* _Alonso del Arte_, Feb 01 2013 *)",
				"LinearRecurrence[{5,-6},{-1,-1},30] (* _Harvey P. Dale_, Feb 02 2015 *)"
			],
			"program": [
				"(PARI) a(n)=3^(n-1)-2^n \\\\ _Charles R Greathouse IV_, Oct 07 2015"
			],
			"xref": [
				"Cf. A000918."
			],
			"keyword": "sign,easy",
			"offset": "1,4",
			"author": "Henrik Johansson (Henrik.Johansson(AT)Nexus.SE)",
			"ext": [
				"A few more terms from _Alonso del Arte_, Feb 01 2013"
			],
			"references": 14,
			"revision": 35,
			"time": "2017-06-09T20:41:48-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}