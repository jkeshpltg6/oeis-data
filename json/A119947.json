{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A119947",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 119947,
			"data": "1,3,1,11,5,1,25,13,7,1,137,77,47,9,1,49,29,19,37,11,1,363,223,153,319,107,13,1,761,481,341,743,533,73,15,1,7129,4609,3349,2509,1879,275,191,17,1,7381,4861,3601,2761,2131,1627,1207,121,19,1,83711,55991,42131,32891,25961",
			"name": "Triangle of numerators in the square of the matrix A[i,j] = 1/i for j \u003c= i, 0 otherwise.",
			"comment": [
				"The triangle of the corresponding denominators is A119948. The rationals appear in lowest terms (while in A027446 they are row-wise on the least common denominator).",
				"The triangle with row number i multiplied with the least common multiple (LCM) of its denominators yields A027446.",
				"First column is A001008. - _Tilman Neumann_, Oct 01 2008",
				"Column 2 is A064169. - _Clark Kimberling_, Aug 13 2012",
				"Third diagonal (11, 13, 47, ...) is A188386. - _Clark Kimberling_, Aug 13 2012"
			],
			"link": [
				"Wolfdieter Lang, \u003ca href=\"/A119947/a119947.txt\"\u003eFirst ten rows and rationals.\u003c/a\u003e"
			],
			"formula": [
				"a(i,j) = numerator(r(i,j)) with r(i,j):=(A^2)[i,j], where the matrix A has elements a[i,j] = 1/i if j\u003c=i, 0 if j\u003ei, (lower triangular)."
			],
			"example": [
				"The rationals are [1]; [3/4, 1/4]; [11/18, 5/18, 1/9]; [25/48, 13/48, 7/48, 1/16]; ... See the W. Lang link for more.",
				"From _Clark Kimberling_, Aug 13 2012: (Start)",
				"As a triangle given by f(n,m) = Sum_{h=m..n} 1/h, the first six rows are:",
				"    1",
				"    3    1",
				"   11    5    1",
				"   25   13    7    1",
				"  137   77   47    9    1",
				"   49   29   19   37   11    1",
				"  363  223  153  319  107   13    1",
				"(End)"
			],
			"mathematica": [
				"f[n_, m_] := Numerator[Sum[1/k, {k, m, n}]]",
				"Flatten[Table[f[n, m], {n, 1, 10}, {m, 1, n}]]",
				"TableForm[Table[f[n, m], {n, 1, 10}, {m, 1, n}]] (* _Clark Kimberling_, Aug 13 2012 *)"
			],
			"program": [
				"(PARI) A119947_upto(n)={my(M=matrix(n,n,i,j,(j\u003c=i)/i)^2);vector(n,r,apply(numerator,M[r,1..r]))} \\\\ _M. F. Hasler_, Nov 05 2019"
			],
			"xref": [
				"Cf. A002024: i appears i times (denominators in row i of the matrix A).",
				"Row sums give A119949. Row sums of the triangle of rationals always give 1.",
				"For the cube of this matrix see the rational triangle A119935/A119932 and A027447; see A027448 for the fourth power.",
				"Cf. A001008, A027446, A064169, A119948, A188386."
			],
			"keyword": "nonn,easy,frac,tabl",
			"offset": "1,2",
			"author": "_Wolfdieter Lang_, Jul 20 2006",
			"ext": [
				"Edited by _M. F. Hasler_, Nov 05 2019"
			],
			"references": 5,
			"revision": 30,
			"time": "2019-11-06T12:41:23-05:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}