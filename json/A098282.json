{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A098282",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 98282,
			"data": "1,2,3,6,4,31,7,55,4,33,5,30,32,1,4,19,8,112,56,16,27,4,4,26,2,20,223,102,34,14,6,162,2,9,10,75,31,113,21,100,33,20,2,23,30,57,5,28,24,30,224,269,20,295,11,85,103,140,9,71,113,55,34,110,76,49,57",
			"name": "Iterate the map k -\u003e A087712(k) starting at n; a(n) is the number of steps at which we see a repeated term for the first time; or -1 if the trajectory never repeats.",
			"comment": [
				"The old entry with this A-number was a duplicate of A030298.",
				"a(52) is currently unknown. - _Donovan Johnson_",
				"a(52)-a(10000) were found using a conjunction of Mathematica and Kim Walisch's primecount program. The additional values of the prime-counting function can be found in the second a-file. - _Matthew House_, Dec 23 2016"
			],
			"link": [
				"Matthew House, \u003ca href=\"/A098282/b098282.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Farideh Firoozbakht, \u003ca href=\"/A098282/a098282.txt\"\u003eNotes on the missing terms in this sequence\u003c/a\u003e",
				"Matthew House, \u003ca href=\"/A098282/a098282_1.txt\"\u003eValues found using primecount\u003c/a\u003e"
			],
			"example": [
				"1 -\u003e 1; 1 step to see a repeat, so a(1) = 1.",
				"2 -\u003e 1 -\u003e 1; 2 steps to see a repeat.",
				"3 -\u003e 2 -\u003e 1 -\u003e 1; 3 steps to see a repeat.",
				"4 -\u003e 11 -\u003e 5 -\u003e 3 -\u003e 2 -\u003e 1 -\u003e 1; 6 steps to see a repeat.",
				"6 -\u003e 12 -\u003e 112 -\u003e 11114 -\u003e 1733 -\u003e 270 -\u003e 12223 -\u003e 7128 -\u003e 11122225 -\u003e 33991010 -\u003e 13913661 -\u003e 2107998 -\u003e 12222775 -\u003e 33910130 -\u003e 131212367 -\u003e 56113213 -\u003e 6837229 -\u003e 4201627 -\u003e 266366 -\u003e 112430 -\u003e 131359 -\u003e 7981 -\u003e 969 -\u003e 278 -\u003e 134 -\u003e 119 -\u003e 47 -\u003e 15 -\u003e 23 -\u003e 9 -\u003e 22 -\u003e 15; 31 steps to see a repeat.",
				"9 -\u003e 22 -\u003e 15 -\u003e 23 -\u003e 9; 4 steps to see a repeat.",
				"From _David Applegate_ and _N. J. A. Sloane_, Feb 09 2009: (Start)",
				"The trajectories of the numbers 1 through 17, up to and including the first repeat, are as follows. Note that a(n) is one less than the number of terms shown.",
				"[1, 1]",
				"[2, 1, 1]",
				"[3, 2, 1, 1]",
				"[4, 11, 5, 3, 2, 1, 1]",
				"[5, 3, 2, 1, 1]",
				"[6, 12, 112, 11114, 1733, 270, 12223, 7128, 11122225, 33991010, 13913661, 2107998, 12222775, 33910130, 131212367, 56113213, 6837229, 4201627, 266366, 112430, 131359, 7981, 969, 278, 134, 119, 47, 15, 23, 9, 22, 15]",
				"[7, 4, 11, 5, 3, 2, 1, 1]",
				"[8, 111, 212, 1116, 112211, 52626, 124441, 28192, 11111152, 111165448, 1117261018, 1910112963, 252163429, 42205629, 2914219, 454002, 127605, 231542, 110938, 15631, 44510, 13605, 23155, 3582, 12246, 12637, 1509, 296, 11112, 111290, 131172, 1127117, 76613, 9470, 13161, 21328, 11111114, 14142115, 3625334, 1125035, 348169, 78151, 11369, 1373, 220, 1135, 349, 70, 134, 119, 47, 15, 23, 9, 22, 15]",
				"[9, 22, 15, 23, 9]",
				"[10, 13, 6, 12, 112, 11114, 1733, 270, 12223, 7128, 11122225, 33991010, 13913661, 2107998, 12222775, 33910130, 131212367, 56113213, 6837229, 4201627, 266366, 112430, 131359, 7981, 969, 278, 134, 119, 47, 15, 23, 9, 22, 15]",
				"[11, 5, 3, 2, 1, 1]",
				"[12, 112, 11114, 1733, 270, 12223, 7128, 11122225, 33991010, 13913661, 2107998, 12222775, 33910130, 131212367, 56113213, 6837229, 4201627, 266366, 112430, 131359, 7981, 969, 278, 134, 119, 47, 15, 23, 9, 22, 15]",
				"[13, 6, 12, 112, 11114, 1733, 270, 12223, 7128, 11122225, 33991010, 13913661, 2107998, 12222775, 33910130, 131212367, 56113213, 6837229, 4201627, 266366, 112430, 131359, 7981, 969, 278, 134, 119, 47, 15, 23, 9, 22, 15]",
				"[14, 14]",
				"[15, 23, 9, 22, 15]",
				"[16, 1111, 526, 156, 1126, 1103, 185, 312, 11126, 1734, 1277, 206, 127, 31, 11, 5, 3, 2, 1, 1]",
				"[17, 7, 4, 11, 5, 3, 2, 1, 1]",
				"For n = 18 see A077960.",
				"(End)"
			],
			"maple": [
				"with(numtheory):",
				"f := proc(n) local t1, v, r, x, j;",
				"if (n = 1) then return 1; end if;",
				"t1 := ifactors(n): v := 0;",
				"for x in op(2,t1) do r := pi(x[1]):",
				"for j from 1 to x[2] do",
				"v := v * 10^length(r) + r;",
				"end do; end do; v; end proc;",
				"t := proc(n) local v, l, s; v := n; s := {v}; l := [v]; v := f(v);",
				"while not v in s do s := s union {v}; l := [op(l),v]; v := f(v); end do;",
				"[op(l),v];",
				"end proc; [seq(nops(t(n))-1, n=1..17)];",
				"# _David Applegate_ and _N. J. A. Sloane_, Feb 09 2009"
			],
			"mathematica": [
				"f[n_] := If[n==1,1,FromDigits@ Flatten[ IntegerDigits@# \u0026 /@ (PrimePi@#",
				"\u0026 /@ Flatten[ Table[ First@#, {Last@#}] \u0026 /@ FactorInteger@n])]];",
				"g[n_] := Length@ NestWhileList[f, n, UnsameQ, All] - 1; Array[g, 39]",
				"(* _Robert G. Wilson v_, Feb 02 2009; modified slightly by _Farideh Firoozbakht_, Feb 10 2009 *)"
			],
			"program": [
				"(GBnums)",
				"void ea (n)",
				"{",
				"mpz u[] ; // factors",
				"mpz tr[]; // sequence",
				"print(n);",
				"while(n \u003e 1)",
				"{",
				"lfactors(u,n); // factorize into u",
				"vmap(u,pi); // replace factors by rank",
				"n = catv(u); // concatenate",
				"print(n);",
				"if(vsearch(tr,n) \u003e 0) break; // loop found",
				"vpush(tr,n); // remember n",
				"}",
				"println('');",
				"}",
				"// _Jacques Tramu_",
				"(Haskell)",
				"import Data.List (genericIndex)",
				"a098282 n = f [n] where",
				"   f xs = if y `elem` xs then length xs else f (y:xs) where",
				"     y = genericIndex (map a087712 [1..]) (head xs - 1)",
				"-- _Reinhard Zumkeller_, Jul 14 2013"
			],
			"xref": [
				"Cf. A087712, A007097, A077960. See also A145077, A145078, A145079, A144760, A144813, A144814, A144915, A144914.",
				"See A156055 for another version."
			],
			"keyword": "nonn,base,nice",
			"offset": "1,2",
			"author": "_Eric Angelini_, Feb 02 2009",
			"ext": [
				"a(8) and a(10) found by _Jacques Tramu_",
				"Extended through a(39) by _Robert G. Wilson v_, Feb 02 2009",
				"Terms through a(39) corrected by _Farideh Firoozbakht_, Feb 10 2009",
				"a(40)-a(51) from _Donovan Johnson_, Jan 08 2011",
				"More terms from and a(40) corrected by _Matthew House_, Dec 23 2016"
			],
			"references": 12,
			"revision": 31,
			"time": "2016-12-23T21:56:36-05:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}