{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A269385",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 269385,
			"data": "1,2,4,3,8,9,6,5,16,21,18,19,12,15,10,7,32,45,42,49,36,51,38,31,24,33,30,35,20,27,14,11,64,93,90,109,84,123,98,85,72,105,102,125,76,111,62,55,48,69,66,79,60,87,70,59,40,57,54,65,28,39,22,13,128,189,186,229,180,267,218,191,168,249,246,305,196,291,170,151,144",
			"name": "Tree of Ludic sieve, mirrored: a(0) = 1, a(1) = 2; after which, a(2n) = 2*a(n), a(2n+1) = A269379(a(n)).",
			"comment": [
				"Permutation of natural numbers obtained from the Ludic sieve. Note the indexing: Domain starts from 0, range from 1.",
				"This sequence can be represented as a binary tree. Each left hand child is obtained by doubling the parent's contents, and each right hand child is obtained by applying A269379 to the parent's contents:",
				"                                     1",
				"                                     |",
				"                  ...................2...................",
				"                 4                                       3",
				"       8......../ \\........9                   6......../ \\........5",
				"      / \\                 / \\                 / \\                 / \\",
				"     /   \\               /   \\               /   \\               /   \\",
				"    /     \\             /     \\             /     \\             /     \\",
				"  16       21         18       19         12       15         10       7",
				"32  45   42  49     36  51   38  31     24  33   30  35     20  27   14 11",
				"etc.",
				"Sequence A269387 is obtained from the mirror image of the same tree."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A269385/b269385.txt\"\u003eTable of n, a(n) for n = 0..8191\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"a(0) = 1, a(1) = 2; after which, a(2n) = 2*a(n), a(2n+1) = A269379(a(n)).",
				"As a composition of related permutations:",
				"a(n) = A269171(A252755(n)).",
				"a(n) = A260741(A269375(n)).",
				"Other identities. For all n \u003e= 2:",
				"A000035(a(n)) = A000035(n). [This permutation preserves the parity of n from a(2)=4 onward.]"
			],
			"program": [
				"(Scheme, with memoization-macro definec)",
				"(definec (A269385 n) (cond ((\u003c= n 1) (+ n 1)) ((even? n) (* 2 (A269385 (/ n 2)))) (else (A269379 (A269385 (/ (- n 1) 2))))))"
			],
			"xref": [
				"Inverse: A269386.",
				"Cf. A000035, A269379.",
				"Cf. A003309 (right edge of the tree).",
				"Related or similar permutations: A163511, A260741, A269387, A269171.",
				"Cf. also A252755, A269375."
			],
			"keyword": "nonn,tabf",
			"offset": "0,2",
			"author": "_Antti Karttunen_, Mar 01 2016",
			"references": 7,
			"revision": 11,
			"time": "2016-03-03T22:51:22-05:00",
			"created": "2016-03-03T22:51:22-05:00"
		}
	]
}