{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005811",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5811,
			"id": "M0110",
			"data": "0,1,2,1,2,3,2,1,2,3,4,3,2,3,2,1,2,3,4,3,4,5,4,3,2,3,4,3,2,3,2,1,2,3,4,3,4,5,4,3,4,5,6,5,4,5,4,3,2,3,4,3,4,5,4,3,2,3,4,3,2,3,2,1,2,3,4,3,4,5,4,3,4,5,6,5,4,5,4,3,4,5,6,5,6,7,6,5,4,5,6,5,4,5",
			"name": "Number of runs in binary expansion of n (n\u003e0); number of 1's in Gray code for n.",
			"comment": [
				"Starting with a(1) = 0 mirror all initial 2^k segments and increase by one.",
				"a(n) gives the net rotation (measured in right angles) after taking n steps along a dragon curve. - Christopher Hendrie (hendrie(AT)acm.org), Sep 11 2002",
				"This sequence generates A082410: (0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, ...) and A014577; identical to the latter except starting 1, 1, 0, ...; by writing a \"1\" if a(n+1) \u003e a(n); if not, write \"0\". E.g., A014577(2) = 0, since a(3) \u003c a(2), or 1 \u003c 2. - _Gary W. Adamson_, Sep 20 2003",
				"Starting with 1 = partial sums of A034947: (1, 1, -1, 1, 1, -1, -1, 1, 1, 1, ...). - _Gary W. Adamson_, Jul 23 2008",
				"The composer Per Nørgård's name is also written in the OEIS as Per Noergaard.",
				"Can be used as a binomial transform operator: Let a(n) = the n-th term in any S(n); then extract 2^k strings, adding the terms. This results in the binomial transform of S(n). Say S(n) = 1, 3, 5, ...; then we obtain the strings: (1), (3, 1), (3, 5, 3, 1), (3, 5, 7, 5, 3, 5, 3, 1), ...; = the binomial transform of (1, 3, 5, ...) = (1, 4, 12, 32, 80, ...). Example: the 8-bit string has a sum of 32 with a distribution of (1, 3, 3, 1) or one 1, three 3's, three 5's, and one 7; as expected. - _Gary W. Adamson_, Jun 21 2012",
				"Considers all positive odd numbers as nodes of a graph. Two nodes are connected if and only if the sum of the two corresponding odd numbers is a power of 2. Then a(n) is the distance between 2n + 1 and 1. - _Jianing Song_, Apr 20 2019"
			],
			"reference": [
				"Danielle Cox and K. McLellan, A problem on generation sets containing Fibonacci numbers, Fib. Quart., 55 (No. 2, 2017), 105-113.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A005811/b005811.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Joerg Arndt, \u003ca href=\"http://www.jjj.de/fxt/#fxtbook\"\u003eMatters Computational (The Fxtbook)\u003c/a\u003e",
				"J.-P. Allouche, G.-N. Han and J. Shallit, \u003ca href=\"https://arxiv.org/abs/2006.08909\"\u003eOn some conjectures of P. Barry\u003c/a\u003e, arXiv:2006.08909 [math.NT], 2020.",
				"J.-P. Allouche and J. Shallit, \u003ca href=\"http://www.math.jussieu.fr/~allouche/kreg2.ps\"\u003eThe Ring of k-regular Sequences, II\u003c/a\u003e",
				"J.-P. Allouche and J. Shallit, \u003ca href=\"http://dx.doi.org/10.1016/S0304-3975(03)00090-2\"\u003eThe ring of k-regular sequences, II\u003c/a\u003e, Theoret. Computer Sci., 307 (2003), 3-29.",
				"Chandler Davis and Donald E. Knuth, Number Representations and Dragon Curves -- I and II, Journal of Recreational Mathematics, volume 3, number 2, April 1970, pages 66-81, and number 3, July 1970, pages 133-149.  Reprinted with addendum in Donald E. Knuth, \u003ca href=\"http://www-cs-faculty.stanford.edu/~uno/fg.html\"\u003eSelected Papers on Fun and Games\u003c/a\u003e, 2010, pages 571-614.  Equation 3.2 g(n) = a(n-1).",
				"P. Flajolet et al., \u003ca href=\"http://algo.inria.fr/flajolet/Publications/FlGrKiPrTi94.pdf\"\u003eMellin Transforms And Asymptotics: Digital Sums\u003c/a\u003e, Theoret. Computer Sci. 23 (1994), 291-314.",
				"P. Flajolet and Lyle Ramshaw, \u003ca href=\"http://dx.doi.org/10.1137/0209014\"\u003eA note on Gray code and odd-even merge\u003c/a\u003e, SIAM J. Comput. 9 (1980), 142-158.",
				"S. Kropf and S. Wagner, \u003ca href=\"https://arxiv.org/abs/1605.03654\"\u003eq-Quasiadditive functions\u003c/a\u003e, arXiv:1605.03654 [math.CO], 2016.",
				"Sara Kropf, S. Wagner, \u003ca href=\"https://arxiv.org/abs/1608.03700\"\u003eOn q-Quasiadditive and q-Quasimultiplicative Functions\u003c/a\u003e, arXiv preprint arXiv:1608.03700 [math.CO], 2016.",
				"Shuo Li, \u003ca href=\"https://arxiv.org/abs/2007.08317\"\u003ePalindromic length sequence of the ruler sequence and of the period-doubling sequence\u003c/a\u003e, arXiv:2007.08317 [math.CO], 2020.",
				"Helmut Prodinger and Friedrich J. Urbanek, \u003ca href=\"https://doi.org/10.1016/0012-365X(79)90135-3\"\u003eInfinite 0-1-Sequences Without Long Adjacent Identical Blocks\u003c/a\u003e, Discrete Mathematics, volume 28, issue 3, 1979, pages 277-289.  Also \u003ca href=\"http://finanz.math.tugraz.at/~prodinger/pdffiles/long_adjacent.pdf\"\u003efirst author's copy\u003c/a\u003e.  Their \"variation\" v(k) at definition 3.4 is a(k).",
				"Jeffrey Shallit, \u003ca href=\"http://www.fq.math.ca/Papers1/43-3/paper43-3-9.pdf\"\u003eThe mathematics of Per Noergaard's rhythmic infinity system\u003c/a\u003e, Fib. Q., 43 (2005), 262-268.",
				"Ralf Stephan, \u003ca href=\"/somedcgf.html\"\u003eSome divide-and-conquer sequences ...\u003c/a\u003e",
				"Ralf Stephan, \u003ca href=\"/A079944/a079944.ps\"\u003eTable of generating functions\u003c/a\u003e",
				"\u003ca href=\"/index/Cor#core\"\u003eIndex entries for \"core\" sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e"
			],
			"formula": [
				"a(2^k + i) = a(2^k - i + 1) + 1 for k \u003e= 0 and 0 \u003c i \u003c= 2^k. - _Reinhard Zumkeller_, Aug 14 2001",
				"a(2n+1) = 2a(n) - a(2n) + 1, a(4n) = a(2n), a(4n+2) = 1 + a(2n+1).",
				"a(j+1) = a(j) + (-1)^A014707(j). - Christopher Hendrie (hendrie(AT)acm.org), Sep 11 2002",
				"G.f.: (1/(1-x)) * Sum_{k\u003e=0} x^2^k/(1+x^2^(k+1)). - _Ralf Stephan_, May 02 2003",
				"Delete the 0, make subsets of 2^n terms; and reverse the terms in each subset to generate A088696. - _Gary W. Adamson_, Oct 19 2003",
				"a(0) = 0, a(2n) = a(n) + [n odd], a(2n+1) = a(n) + [n even]. - _Ralf Stephan_, Oct 20 2003",
				"a(n) = Sum_{k=1..n} (-1)^((k/2^A007814(k)-1)/2) = Sum_{k=1..n} (-1)^A025480(k-1). - _Ralf Stephan_, Oct 29 2003",
				"a(n) = A069010(n) + A033264(n). - _Ralf Stephan_, Oct 29 2003",
				"a(0) = 0 then a(n) = a(floor(n/2)) + (a(floor(n/2)) + n) mod 2. - _Benoit Cloitre_, Jan 20 2014",
				"a(n) = A037834(n) + 1."
			],
			"example": [
				"Considered as a triangle with 2^k terms per row, the first few rows are:",
				"1",
				"2, 1",
				"2, 3, 2, 1",
				"2, 3, 4, 3, 2, 3, 2, 1",
				"... n-th row becomes right half of next row; left half is mirrored terms of n-th row increased by one. - _Gary W. Adamson_, Jun 20 2012"
			],
			"maple": [
				"A005811 := proc(n)",
				"    local i, b, ans;",
				"    if n = 0 then",
				"        return 0 ;",
				"    end if;",
				"    ans := 1;",
				"    b := convert(n, base, 2);",
				"    for i from nops(b)-1 to 1 by -1 do",
				"        if b[ i+1 ]\u003c\u003eb[ i ] then",
				"            ans := ans+1",
				"        fi",
				"    od;",
				"    return ans ;",
				"end proc:",
				"seq(A005811(i), i=1..50) ;"
			],
			"mathematica": [
				"Table[ Length[ Length/@Split[ IntegerDigits[ n, 2 ] ] ], {n, 1, 255} ]"
			],
			"program": [
				"(PARI) a(n)=sum(k=1,n,(-1)^((k/2^valuation(k,2)-1)/2))",
				"(PARI) a(n)=if(n\u003c1,0,a(n\\2)+(a(n\\2)+n)%2) \\\\ _Benoit Cloitre_, Jan 20 2014",
				"(PARI) a(n) = hammingweight(bitxor(n, n\u003e\u003e1));  \\\\ _Gheorghe Coserea_, Sep 03 2015",
				"(Haskell)",
				"import Data.List (group)",
				"a005811 0 = 0",
				"a005811 n = length $ group $ a030308_row n",
				"a005811_list = 0 : f [1] where",
				"   f (x:xs) = x : f (xs ++ [x + x `mod` 2, x + 1 - x `mod` 2])",
				"-- _Reinhard Zumkeller_, Feb 16 2013, Mar 07 2011",
				"(Python)",
				"def a(n): return bin(n^(n\u003e\u003e1))[2:].count(\"1\") # _Indranil Ghosh_, Apr 29 2017"
			],
			"xref": [
				"Cf. A037834 (-1), A088748 (+1), A246960 (mod 4), A034947 (first differences), A000975 (indices of record highs).",
				"Cf. A056539, A014707, A014577, A082410, A030308, A090079, A044813, A165413, A226227, A226228, A226229."
			],
			"keyword": "easy,nonn,core,nice,hear",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_, _Jeffrey Shallit_, _Simon Plouffe_",
			"ext": [
				"Additional description from _Wouter Meeussen_"
			],
			"references": 126,
			"revision": 139,
			"time": "2021-08-27T06:17:18-04:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}