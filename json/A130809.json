{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A130809",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 130809,
			"data": "8,32,80,160,280,448,672,960,1320,1760,2288,2912,3640,4480,5440,6528,7752,9120,10640,12320,14168,16192,18400,20800,23400,26208,29232,32480,35960,39680,43648,47872,52360,57120,62160,67488,73112,79040,85280",
			"name": "If X_1, ..., X_n is a partition of a 2n-set X into 2-blocks then a(n) is equal to the number of 3-subsets of X containing none of X_i, (i=1,...,n).",
			"comment": [
				"Uncentered octahedral numbers: take a simple cubical grid of size n X n X n where n = 2k is an even number, n \u003e= 6. Retain all points that are at Manhattan distance n or greater from all 8 corners of the cube, and discard all other points. The number of points that remain is a(k). If n were to be an odd number, the same operation would yield the centered octahedral numbers A001845. - _Arun Giridhar_, Mar 06 2014",
				"For an (n+2)-dimensional Rubik's cube, the number of cubes that have exactly 3 exposed facets. - _Phil Scovis_, Aug 03 2009",
				"a(n) is the number of 2-simplices in an n-cross polytope. - _Arkadiusz Wesolowski_, Oct 16 2012",
				"a(n) is also the number of unit tetrahedra in an (n+1)-scaled octahedron composed of the tetrahedral-octahedral honeycomb. - _Jason Pruski_, Aug 31 2017"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A130809/b130809.txt\"\u003eTable of n, a(n) for n = 3..1000\u003c/a\u003e",
				"H. J. Brothers, \u003ca href=\"http://www.brotherstechnology.com/docs/Pascal\u0026#39;s_Prism_(supplement).pdf\"\u003ePascal's Prism: Supplementary Material\u003c/a\u003e",
				"Milan Janjic, \u003ca href=\"http://www.pmfbl.org/janjic/\"\u003eTwo Enumerative Functions\u003c/a\u003e",
				"Luis Manuel Rivera, \u003ca href=\"http://arxiv.org/abs/1406.3081\"\u003eInteger sequences and k-commuting permutations\u003c/a\u003e, arXiv preprint arXiv:1406.3081 [math.CO], 2014.",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-6,4,-1)"
			],
			"formula": [
				"a(n) = (4/3)*n*(n-1)*(n-2).",
				"a(n) = C(n,n-3)*8, n \u003e= 3. - _Zerinvary Lajos_, Dec 07 2007",
				"G.f.: 8*x^3/(1-x)^4. - _Colin Barker_, Apr 14 2012",
				"For n\u003e1, a(n) = a(n-1) + A056220(n-1) + A056220(n-2). - _Bruce J. Nicholson_, Feb 14 2018"
			],
			"maple": [
				"a:=n-\u003e4/3*n*(n-1)*(n-2);"
			],
			"mathematica": [
				"Table[(4/3) n (n - 1) (n - 2), {n, 3, 41}] (* or *)",
				"Table[Binomial[n, n - 3] 2^3, {n, 3, 41}] (* or *)",
				"DeleteCases[#, 0] \u0026@ CoefficientList[Series[8 x^3/(1 - x)^4, {x, 0, 41}], x] (* _Michael De Vlieger_, Aug 31 2017 *)"
			],
			"program": [
				"(MAGMA) [(4/3)*n*(n-1)*(n-2): n in [3..60]]; // _Vincenzo Librandi_, Oct 03 2017",
				"(PARI) a(n) = 4*n*(n-1)*(n-2)/3; \\\\ _Andrew Howroyd_, Nov 06 2018"
			],
			"xref": [
				"Cf. A000079, A001787, A001788, A001789, A002409, A003472, A038207, A046092, A054849, A054851, A056220, A140325, A140354."
			],
			"keyword": "nonn,easy",
			"offset": "3,1",
			"author": "_Milan Janjic_, Jul 16 2007",
			"references": 16,
			"revision": 73,
			"time": "2018-11-06T11:41:27-05:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}