{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A023003",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 23003,
			"data": "1,4,14,40,105,252,574,1240,2580,5180,10108,19208,35693,64960,116090,203984,353017,602348,1014580,1688400,2778517,4524760,7296752,11658920,18468245,29015700,45235414,70005376,107585845,164245380,249162620,375704920,563251038",
			"name": "Number of partitions of n into parts of 4 kinds.",
			"comment": [
				"a(n) is Euler transform of A010709. - _Alois P. Heinz_, Oct 17 2008"
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A023003/b023003.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e (first 501 terms from T. D. Noe)",
				"Roland Bacher, P. De La Harpe, \u003ca href=\"https://hal.archives-ouvertes.fr/hal-01285685/document\"\u003eConjugacy growth series of some infinitely generated groups\u003c/a\u003e, 2016, hal-01285685v2.",
				"Vaclav Kotesovec, \u003ca href=\"https://arxiv.org/abs/1509.08708\"\u003eA method of finding the asymptotics of q-series based on the convolution of generating functions\u003c/a\u003e, arXiv:1509.08708 [math.CO], Sep 30 2015, p. 8.",
				"P. Nataf, M. Lajkó, A. Wietek, K. Penc, F. Mila, A. M. Läuchli, \u003ca href=\"https://arxiv.org/abs/1601.00958\"\u003eChiral spin liquids in triangular lattice SU (N) fermionic Mott insulators with artificial gauge fields\u003c/a\u003e, arXiv preprint arXiv:1601.00958 [cond-mat.quant-gas], 2016.",
				"N. J. A. Sloane, \u003ca href=\"/transforms.txt\"\u003eTransforms\u003c/a\u003e",
				"\u003ca href=\"/index/Pro#1mxtok\"\u003eIndex entries for expansions of Product_{k \u003e= 1} (1-x^k)^m\u003c/a\u003e"
			],
			"formula": [
				"G.f.: Product_{m\u003e=1} 1/(1-x^m)^4.",
				"a(0)=1, a(n) = (1/n) * Sum_{k=0..n-1} 4*a(k)*sigma_1(n-k). - _Joerg Arndt_, Feb 05 2011",
				"a(n) ~ exp(2 * Pi * sqrt(2*n/3)) / (2^(7/4) * 3^(5/4) * n^(7/4)) * (1 - (35*sqrt(3)/(16*Pi) + Pi/(3*sqrt(3))) / sqrt(n)). - _Vaclav Kotesovec_, Feb 28 2015, extended Jan 16 2017",
				"G.f.: exp(4*Sum_{k\u003e=1} x^k/(k*(1 - x^k))). - _Ilya Gutkovskiy_, Feb 06 2018"
			],
			"maple": [
				"with(numtheory): a:= proc(n) option remember; `if`(n=0, 1, add(add(d*4, d=divisors(j)) *a(n-j), j=1..n)/n) end: seq(a(n), n=0..40); # _Alois P. Heinz_, Oct 17 2008"
			],
			"mathematica": [
				"nmax=50; CoefficientList[Series[Product[1/(1-x^k)^4,{k,1,nmax}],{x,0,nmax}],x] (* _Vaclav Kotesovec_, Feb 28 2015 *)",
				"CoefficientList[1/QPochhammer[x]^4 + O[x]^40, x] (* _Jean-François Alcover_, Jan 31 2016 *)"
			],
			"program": [
				"(PARI) \\ps100",
				"for(n=0,100,print1((polcoeff(1/eta(x)^4,n,x)),\",\"))",
				"(Julia) # DedekindEta is defined in A000594.",
				"A023003List(len) = DedekindEta(len, -4)",
				"A023003List(33) |\u003e println # _Peter Luschny_, Mar 10 2018"
			],
			"xref": [
				"Cf. 4th column of A144064."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_David W. Wilson_",
			"references": 10,
			"revision": 61,
			"time": "2019-05-23T15:41:24-04:00",
			"created": "1998-06-14T03:00:00-04:00"
		}
	]
}