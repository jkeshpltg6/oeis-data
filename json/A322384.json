{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A322384",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 322384,
			"data": "1,3,1,13,4,1,67,21,7,1,411,131,46,11,1,2911,950,341,101,16,1,23563,7694,2871,932,197,22,1,213543,70343,26797,9185,2311,351,29,1,2149927,709015,275353,98317,27568,5119,583,37,1,23759791,7867174,3090544,1141614,343909,73639,10366,916,46,1",
			"name": "Number T(n,k) of entries in the k-th cycles of all permutations of [n] when cycles are ordered by decreasing lengths (and increasing smallest elements); triangle T(n,k), n\u003e=1, 1\u003c=k\u003c=n, read by rows.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A322384/b322384.txt\"\u003eRows n = 1..141, flattened\u003c/a\u003e",
				"Andrew V. Sills, \u003ca href=\"https://arxiv.org/abs/1912.05306\"\u003eInteger Partitions Probability Distributions\u003c/a\u003e, arXiv:1912.05306 [math.CO], 2019.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Permutation\"\u003ePermutation\u003c/a\u003e"
			],
			"example": [
				"The 6 permutations of {1,2,3} are:",
				"  (1)     (2) (3)",
				"  (1,2)   (3)",
				"  (1,3)   (2)",
				"  (2,3)   (1)",
				"  (1,2,3)",
				"  (1,3,2)",
				"so there are 13 elements in the first cycles, 4 in the second cycles and only 1 in the third cycles.",
				"Triangle T(n,k) begins:",
				"       1;",
				"       3,     1;",
				"      13,     4,     1;",
				"      67,    21,     7,    1;",
				"     411,   131,    46,   11,    1;",
				"    2911,   950,   341,  101,   16,   1;",
				"   23563,  7694,  2871,  932,  197,  22,  1;",
				"  213543, 70343, 26797, 9185, 2311, 351, 29, 1;",
				"  ..."
			],
			"maple": [
				"b:= proc(n, l) option remember; `if`(n=0, add(l[-i]*",
				"      x^i, i=1..nops(l)), add(binomial(n-1, j-1)*",
				"      b(n-j, sort([l[], j]))*(j-1)!, j=1..n))",
				"    end:",
				"T:= n-\u003e (p-\u003e (seq(coeff(p, x, i), i=1..n)))(b(n, [])):",
				"seq(T(n), n=1..12);"
			],
			"mathematica": [
				"b[n_, l_] := b[n, l] = If[n == 0, Sum[l[[-i]]*x^i, {i, 1, Length[l]}], Sum[Binomial[n-1, j-1]*b[n-j, Sort[Append[l, j]]]*(j-1)!, {j, 1, n}]];",
				"T[n_] := CoefficientList[b[n, {}], x] // Rest;",
				"Array[T, 12] // Flatten  (* _Jean-François Alcover_, Feb 26 2020, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Columns k=1-10 give: A028418, A332851, A332852, A332853, A332854, A332855, A332856, A332857, A332858, A332859.",
				"Row sums give A001563.",
				"T(2n,n) gives A332928.",
				"Cf. A185105, A322383."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Alois P. Heinz_, Dec 05 2018",
			"references": 15,
			"revision": 31,
			"time": "2021-12-07T22:44:34-05:00",
			"created": "2018-12-08T20:02:17-05:00"
		}
	]
}