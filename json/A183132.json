{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A183132",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 183132,
			"data": "10,5,36,858,234,5577,1521,3549,8281,910,100,50,25,180,3388,924,252,6006,1638,39039,10647,24843,57967,6370,700,300,7150,1950,46475,12675,29575,3250,360,6776,1848,504,12012,3276,78078,21294,507507,138411,322959,753571",
			"name": "Successive integers produced by Conway's PRIMEGAME using Kilminster's Fractran program with only nine fractions.",
			"comment": [
				"The exponents of exact powers of 10 in this sequence are 1, followed by the successive primes (A008578)."
			],
			"reference": [
				"D. Olivastro, Ancient Puzzles. Bantam Books, NY, 1993, p. 21."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A183132/b183132.txt\"\u003eTable of n, a(n) for n = 1..10547\u003c/a\u003e",
				"J. H. Conway, \u003ca href=\"http://dx.doi.org/10.1007/978-1-4612-4808-8_2\"\u003eFRACTRAN: a simple universal programming language for arithmetic\u003c/a\u003e, in T. M. Cover and Gopinath, eds., Open Problems in Communication and Computation, Springer, NY 1987, pp. 4-26.",
				"Esolang wiki, \u003ca href=\"http://www.esolangs.org/wiki/Fractran\"\u003eFractran\u003c/a\u003e",
				"Chaim Goodman-Strauss, \u003ca href=\"http://www.ams.org/notices/201003/rtx100300343p.pdf\"\u003eCan’t Decide? Undecide!\u003c/a\u003e, Notices of the AMS, Volume 57, Number 3, pp. 343-356, March 2010.",
				"R. K. Guy, \u003ca href=\"http://www.jstor.org/stable/2690263\"\u003eConway's prime producing machine\u003c/a\u003e, Math. Mag. 56 (1983), no. 1, 26-33.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/FRACTRAN.html\"\u003eFRACTRAN\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/FRACTRAN\"\u003eFRACTRAN\u003c/a\u003e."
			],
			"maple": [
				"l:= [3/11, 847/45, 143/6, 7/3, 10/91, 3/7, 36/325, 1/2, 36/5]:",
				"a:= proc(n) option remember;",
				"      global l;",
				"      local p, k;",
				"      if n=1 then 10",
				"             else p:= a(n-1);",
				"                  for k while not type(p*l[k], integer)",
				"                  do od; p*l[k]",
				"      fi",
				"    end:",
				"seq(a(n), n=1..50);"
			],
			"mathematica": [
				"l = {3/11, 847/45, 143/6, 7/3, 10/91, 3/7, 36/325, 1/2, 36/5};",
				"a[n_] := a[n] = Module[{p, k}, If[n == 1, 10, p = a[n - 1]; For[k = 1, !IntegerQ[p*l[[k]]], k++]; p*l[[k]]]];",
				"Array[a, 50] (* _Jean-François Alcover_, May 28 2018, from Maple *)"
			],
			"program": [
				"(Python)",
				"from fractions import Fraction",
				"nums = [ 3, 847, 143, 7, 10, 3,  36, 1, 36]",
				"dens = [11,  45,   6, 3, 91, 7, 325, 2,  5]",
				"PRIMEGAME = [Fraction(num, den) for num, den in zip(nums, dens)]",
				"def succ(n, program):",
				"    for i in range(len(program)):",
				"      if (n*program[i]).denominator == 1: return (n*program[i]).numerator",
				"def orbit(start, program, steps):",
				"    orb = [start]",
				"    for s in range(1, steps): orb.append(succ(orb[-1], program))",
				"    return orb",
				"print(orbit(10, PRIMEGAME, steps=44)) # _Michael S. Branicky_, Oct 05 2021"
			],
			"xref": [
				"Cf. A183133, A008578, A007542, A007546, A007547."
			],
			"keyword": "easy,look,nonn",
			"offset": "1,1",
			"author": "_Alois P. Heinz_, Dec 26 2010",
			"references": 5,
			"revision": 38,
			"time": "2021-10-05T08:28:19-04:00",
			"created": "2010-12-26T04:21:18-05:00"
		}
	]
}