{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A087061",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 87061,
			"data": "0,1,1,2,1,2,3,2,2,3,4,3,2,3,4,5,4,3,3,4,5,6,5,4,3,4,5,6,7,6,5,4,4,5,6,7,8,7,6,5,4,5,6,7,8,9,8,7,6,5,5,6,7,8,9,10,9,8,7,6,5,6,7,8,9,10,11,11,9,8,7,6,6,7,8,9,11,11,12,11,12,9,8,7,6,7,8,9,12,11,12,13,12,12,13,9,8",
			"name": "Array T(n,k) = lunar sum n+k (n \u003e= 0, k \u003e= 0) read by antidiagonals.",
			"comment": [
				"There are no carries in lunar arithmetic. For each pair of lunar digits, to Add, take the lArger, but to Multiply, take the sMaller. For example:",
				"     169",
				"   + 248",
				"   ------",
				"     269",
				"and",
				"       169",
				"     x 248",
				"     ------",
				"       168",
				"      144",
				"   + 122",
				"   --------",
				"     12468",
				"Addition and multiplication are associative and commutative and multiplication distributes over addition. E.g., 357 * (169 + 248) = 357 * 269 = 23567 = 13567 + 23457 = (357 * 169) + (357 * 248). Note that 0 + x = x and 9*x = x for all x.",
				"We have changed the name from \"dismal arithmetic\" to \"lunar arithmetic\" - the old name was too depressing. - _N. J. A. Sloane_, Aug 06 2014"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A087061/b087061.txt\"\u003eTable of n, a(n) for n = 0..10010\u003c/a\u003e",
				"D. Applegate, \u003ca href=\"/A087061/a087061.txt\"\u003eC program for lunar arithmetic and number theory\u003c/a\u003e",
				"D. Applegate, M. LeBrun and N. J. A. Sloane, \u003ca href=\"http://arxiv.org/abs/1107.1130\"\u003eDismal Arithmetic\u003c/a\u003e, arXiv:1107.1130 [math.NT], 2011.",
				"Brady Haran and N. J. A. Sloane, \u003ca href=\"https://youtu.be/cZkGeR9CWbk\"\u003ePrimes on the Moon (Lunar Arithmetic)\u003c/a\u003e, Numberphile video, Nov 2018.",
				"Rémy Sigrist, \u003ca href=\"/A087061/a087061.png\"\u003eColored representation of the array for n, k \u003c 1000\u003c/a\u003e (where the color is function of T(n, k))",
				"\u003ca href=\"/index/Di#dismal\"\u003eIndex entries for sequences related to dismal (or lunar) arithmetic\u003c/a\u003e"
			],
			"example": [
				"Lunar addition table begins:",
				"   0  1  2  3  4  5  6  7  8  9 10 11 12 13 ...",
				"   1  1  2  3  4  5  6  7  8  9 11 11 12 13 ...",
				"   2  2  2  3  4  5  6  7  8  9 12 12 12 13 ...",
				"   3  3  3  3  4  5  6  7  8  9 13 13 13 13 ...",
				"   4  4  4  4  4  5  6  7  8  9 14 14 14 14 ...",
				"   5  5  5  5  5  5  6  7  8  9 15 15 15 15 ...",
				"   6  6  6  6  6  6  6  7  8  9 16 16 16 16 ...",
				"   7  7  7  7  7  7  7  7  8  9 17 17 17 17 ...",
				"   8  8  8  8  8  8  8  8  8  9 18 18 18 18 ...",
				"   9  9  9  9  9  9  9  9  9  9 19 19 19 19 ...",
				"  10 11 12 13 14 15 16 17 18 19 10 11 12 13 ...",
				"  11 11 12 13 14 15 16 17 18 19 11 11 12 13 ...",
				"    ..."
			],
			"maple": [
				"Maple programs for lunar arithmetic are in A087062."
			],
			"mathematica": [
				"ladd[x_, y_] := FromDigits[MapThread[Max, IntegerDigits[#, 10, Max @@ IntegerLength /@ {x, y}] \u0026 /@ {x, y}]]; Flatten[Table[ladd[k, n - k], {n, 0, 13}, {k, 0, n}]] (* _Davin Park_, Sep 29 2016 *)"
			],
			"program": [
				"(PARI) ladd=A087061(m,n)=fromdigits(vector(if(#(m=digits(m))\u003e#n=digits(n),#n=Vec(n,-#m),#m\u003c#n,#m=Vec(m,-#n),#n),k,max(m[k],n[k]))) \\\\  _M. F. Hasler_, Nov 12 2017, updated Nov 15 2018"
			],
			"xref": [
				"Cf. A087062 (multiplication), A087097 (primes)."
			],
			"keyword": "nonn,tabl,nice,base,look",
			"offset": "0,4",
			"author": "_Marc LeBrun_, Oct 09 2003",
			"ext": [
				"Edited by _M. F. Hasler_, Nov 12 2017"
			],
			"references": 42,
			"revision": 63,
			"time": "2021-04-02T02:46:07-04:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}