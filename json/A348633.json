{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A348633",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 348633,
			"data": "2,13,17,19,101,3,131,5,151,7,181,31,41,61,71,313,191,317,1013,1019,1213,1217,1319,1613,1619,1913,10141,419,10151,613,10181,617,12101,619,12161,719,13121,919,13151,2131,2141,2161,3121,3181,3191,5101,5171,6101,6121,6131,6151,7121,7151,8101,8161,8171,8191,9151,9161,9181,21013,13171,21017,15101",
			"name": "Lexicographically earliest sequence of distinct primes where the concatenation of all terms alternates a 1 digit with a not-1 digit.",
			"comment": [
				"Primes containing two adjacent ones are necessarily excluded."
			],
			"link": [
				"Hans Havermann, \u003ca href=\"/A348633/b348633.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"mathematica": [
				"q[p_, m_] := AllTrue[IntegerDigits[p][[m ;; -1 ;; 2]], # == 1 \u0026] \u0026\u0026 AllTrue[IntegerDigits[p][[3 - m ;; -1 ;; 2]], # != 1 \u0026]; seq[max_] := Module[{p = Select[Range[3, max], PrimeQ], p1, p2, s = {2}}, p1 = Select[p, q[#, 1] \u0026]; p2 = Select[p, q[#, 2] \u0026]; While[p1 != {} \u0026\u0026 p2 != {}, If[Mod[s[[-1]], 10] == 1, AppendTo[s, p2[[1]]]; p2 = Drop[p2, 1], AppendTo[s, p1[[1]]]; p1 = Drop[p1, 1]]]; s]; seq[2*10^4] (* _Amiram Eldar_, Oct 28 2021 *)"
			],
			"program": [
				"Python)",
				"from sympy import isprime",
				"from itertools import product",
				"def eo1(): # generates +ive terms with every other digit 1 and not-1 or v.v.",
				"    yield from range(1, 10)",
				"    digits = 2",
				"    while True:",
				"        for not1s in product(\"023456789\", repeat=digits//2):",
				"            yield(int(\"1\" + \"1\".join(not1s) + \"1\"*(digits%2)))",
				"        for first in \"23456789\":",
				"            for not1s in product(\"023456789\", repeat=(digits-1)//2):",
				"                yield(int(\"1\".join((first,)+not1s) + \"1\"*(digits%2==0)))",
				"        digits += 1",
				"def aupton(terms):",
				"    alst, aset, astr = [2], {2}, \"2\"",
				"    kgen = {True: eo1(), False: eo1()}",
				"    for n in range(2, terms+1):",
				"        mustbegin1 = (astr[-1] != \"1\")",
				"        k = next(kgen[mustbegin1]); sk = str(k)",
				"        while k in aset or ((sk[0] == \"1\") != mustbegin1) or not isprime(k):",
				"            k = next(kgen[mustbegin1]); sk = str(k)",
				"        alst.append(k); aset.add(k); astr += sk",
				"    return alst",
				"print(aupton(64)) # _Michael S. Branicky_, Oct 28 2021"
			],
			"xref": [
				"Cf. A000040, A276103."
			],
			"keyword": "nonn,base",
			"offset": "1,1",
			"author": "_Hans Havermann_, Oct 26 2021",
			"references": 1,
			"revision": 13,
			"time": "2021-12-15T01:55:21-05:00",
			"created": "2021-12-15T01:55:21-05:00"
		}
	]
}