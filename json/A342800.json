{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A342800",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 342800,
			"data": "0,0,0,0,0,0,24,72,0,0,1704,5184,0,0,193344,600504,0,0,34321512,141520752,0,0,9205815672,37962945288,0,0",
			"name": "Number of self-avoiding polygons on a 3-dimensional cubic lattice where each walk consists of steps with incrementing length from 1 to n.",
			"comment": [
				"This sequence gives the number of self-avoiding polygons (closed-loop self-avoiding walks) on a 3D cubic lattice where the walk starts with a step length of 1 which then increments by 1 after each step up until the step length is n. Like A334720 and A335305 only n values corresponding to even triangular numbers can form closed loops. All possible paths are counted, including those that are equivalent via rotation and reflection."
			],
			"link": [
				"A. J. Guttmann and A. R. Conway, \u003ca href=\"http://dx.doi.org/10.1007/PL00013842\"\u003eSelf-Avoiding Walks and Polygons\u003c/a\u003e, Annals of Combinatorics 5 (2001) 319-345."
			],
			"example": [
				"a(1) to a(6) = 0 as no self-avoiding closed-loop walk is possible.",
				"a(7) = 24 as there is one walk which forms a closed loop which can be walked in 24 different ways on a 3D cubic lattice. These walks, and those for n(8) = 72, are purely 2-dimensional. See A334720 for images of these walks.",
				"a(11) = 1704. These walks consist of 120 purely 2-dimensional walks and 1584 3-dimensional walks. One of these 3-dimensional walks is:",
				".",
				"                                /|",
				"                               / |                        z  y",
				"                              /  |                        | /",
				"                        7 +y /   |                        |/",
				"                            /    | 8 -z                   |----- x",
				"             6 +x          /     |",
				"  |---.---.---.---.---.---/      |               9 +x",
				"  |                              |---.---.---.---.---.---.---.---.---/",
				"  | 5 +z                                                            /",
				"  |                                                                /",
				"  |---.---.---.---/                                               /",
				"        4 -x     /  3 +y                                         /",
				"                /                                               /  10 -y",
				"                | 2 +z                                         /",
				"                |                                             /",
				"                | 1 +z                                       /",
				"                X---.---.---.---.---.---.---.---.---.---.---/",
				"                                     11 -x",
				"."
			],
			"xref": [
				"Cf. A334720, A334877, A342807, A001413, A002896, A002899, A010566, A335305."
			],
			"keyword": "nonn,more",
			"offset": "1,7",
			"author": "_Scott R. Shannon_, Mar 21 2021",
			"references": 1,
			"revision": 12,
			"time": "2021-03-28T00:19:47-04:00",
			"created": "2021-03-28T00:19:47-04:00"
		}
	]
}