{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A065442",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 65442,
			"data": "1,6,0,6,6,9,5,1,5,2,4,1,5,2,9,1,7,6,3,7,8,3,3,0,1,5,2,3,1,9,0,9,2,4,5,8,0,4,8,0,5,7,9,6,7,1,5,0,5,7,5,6,4,3,5,7,7,8,0,7,9,5,5,3,6,9,1,4,1,8,4,2,0,7,4,3,4,8,6,6,9,0,5,6,5,7,1,1,8,0,1,6,7,0,1,5,5,5,7,5,8,9,7,0,4",
			"name": "Decimal expansion of Erdős-Borwein constant Sum_{k\u003e=1} 1/(2^k - 1).",
			"comment": [
				"Also the decimal expansion of the (finite) value of Sum_{ k \u003e= 1, k has no digit equal to 0 in base 2 } 1/k. - _Robert G. Wilson v_, Aug 03 2010",
				"This constant is irrational (Erdős, 1948; Borwein 1992). - _Amiram Eldar_, Aug 01 2020"
			],
			"reference": [
				"Steven R. Finch, Mathematical Constants, Cambridge, 2003, pp. 354-361.",
				"Paul Halmos, \"Problems for Mathematicians, Young and Old\", Dolciani Mathematical Expositions, 1991, p. 258."
			],
			"link": [
				"Harry J. Smith, \u003ca href=\"/A065442/b065442.txt\"\u003eTable of n, a(n) for n = 1..2000\u003c/a\u003e",
				"David H. Bailey and Richard E. Crandall, \u003ca href=\"https://projecteuclid.org/euclid.em/1057864662\"\u003eRandom generators and normal numbers\u003c/a\u003e, Experimental Mathematics, Vol. 11, No. 4 (2002), pp. 527-546.",
				"Robert Baillie, \u003ca href=\"http://arxiv.org/abs/0806.4410\"\u003eSumming The Curious Series Of Kempner and Irwin\u003c/a\u003e,  arXiv:0806.4410v2 [math.CA], 2008.",
				"Peter Borwein, \u003ca href=\"https://doi.org/10.1017/S030500410007081X\"\u003eOn the Irrationality of Certain Series\u003c/a\u003e, Math. Proc. Cambridge Philos. Soc., Vol. 112, No. 1 (1992), pp. 141-146, \u003ca href=\"http://www.cecm.sfu.ca/personal/pborwein/PAPERS/P59.pdf\"\u003ealternative link\u003c/a\u003e.",
				"Richard Crandall, \u003ca href=\"http://www.emis.de/journals/INTEGERS/papers/m23/m23.Abstract.html\"\u003eThe googol-th bit of the Erdős-Borwein constant\u003c/a\u003e, Integers, 12 (2012), A23.",
				"Paul Erdős, \u003ca href=\"https://users.renyi.hu/~p_erdos/1948-04.pdf\"\u003eOn Arithmetical Properties of Lambert Series\u003c/a\u003e, J. Indian Math. Soc., Vol. 12 (1948), 63-66.",
				"Steven R. Finch, \u003ca href=\"http://www.people.fas.harvard.edu/~sfinch/constant/dig/dig.html\"\u003eDigital Search Tree Constants\u003c/a\u003e [Broken link]",
				"Steven R. Finch, \u003ca href=\"http://web.archive.org/web/20010413214937/http://www.mathsoft.com:80/asolve/constant/dig/dig.html\"\u003eDigital Search Tree Constants\u003c/a\u003e [From the Wayback machine]",
				"Nobushige Kurokawa and Yuichiro Taguchi, \u003ca href=\"http://dx.doi.org/10.3792/pjaa.94.13\"\u003eA p-analogue of Euler’s constant and congruence zeta functions\u003c/a\u003e, Proc. Japan Acad. Ser. A Math. Sci., Volume 94, Number 2 (2018), 13-16.",
				"László Tóth, \u003ca href=\"https://arxiv.org/abs/1608.00795\"\u003eAlternating sums concerning multiplicative arithmetic functions\u003c/a\u003e, arXiv preprint arXiv:1608.00795 [math.NT], 2016.",
				"Eric Weisstein's Mathworld, \u003ca href=\"http://mathworld.wolfram.com/Erdos-BorweinConstant.html\"\u003eErdos-Borwein Constant\u003c/a\u003e, \u003ca href=\"http://mathworld.wolfram.com/TreeSearching.html\"\u003eTree Searching\u003c/a\u003e, \u003ca href=\"http://mathworld.wolfram.com/DoubleSeries.html\"\u003eDouble Series\u003c/a\u003e, \u003ca href=\"http://mathworld.wolfram.com/IrrationalNumber.html\"\u003eIrrational Number\u003c/a\u003e",
				"Rimer Zurita \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL23/Zurita/zur3.html\"\u003eGeneralized Alternating Sums of Multiplicative Arithmetic Functions\u003c/a\u003e, J. Int. Seq., Vol. 23 (2020), Article 20.10.4."
			],
			"formula": [
				"Note: Sum_{k\u003e=1} d(k)/2^k = Sum_{k\u003e=1} 1/(2^k - 1).",
				"Fast computation via Lambert series: 1.60669515... = Sum_{n\u003e=1} x^(n^2)*(1+x^n)/(1-x^n) where x=1/2. - _Joerg Arndt_, May 24 2011",
				"Equals Sum_{k\u003e=0} 1/sigma(2^k). - _Paolo P. Lava_, Feb 10 2014",
				"Equals (1/2) * A211705. - _Amiram Eldar_, Aug 01 2020"
			],
			"example": [
				"1.60669515241529176378330152319092458048057967150575643577807955369..."
			],
			"maple": [
				"# Uses Lambert series, cf. formula by Arndt:",
				"evalf( add( (1/2)^(n^2)*(1 + 2/(2^n - 1)), n = 1..20 ), 105);",
				"# _Peter Bala_, Jan 22 2021"
			],
			"mathematica": [
				"RealDigits[ Sum[1/(2^k - 1), {k, 350}], 10, 111][[1]] (* _Robert G. Wilson v_, Nov 05 2006 *)",
				"(* first install irwinSums.m, see reference, then *) First@ RealDigits@ iSum[0, 0, 111, 2] (* _Robert G. Wilson v_, Aug 03 2010 *)",
				"RealDigits[(Log[2] - 2 QPolyGamma[0, 1, 2])/Log[4], 10, 100][[1]] (* _Fred Daniel Kline_, May 23 2011 *)",
				"x = 1/2; RealDigits[ Sum[ DivisorSigma[0, k] x^k, {k, 1000}], 10, 105][[1]] (* _Robert G. Wilson v_, Oct 12 2014 after an observation and formula of _Amarnath Murthy_, see A073668 *)"
			],
			"program": [
				"(PARI) A065442(n)= s=0; for(x=1,n,s=s+1.0/(2^x-1)); s",
				"(PARI) default(realprecision, 2080); x=suminf(k=1, 1/(2^k - 1)); for (n=1, 2000, d=floor(x); x=(x-d)*10; write(\"b065442.txt\", n, \" \", d)) \\\\ _Harry J. Smith_, Oct 19 2009",
				"(PARI) k=1.; suminf(n=1, k\u003e\u003e=1; k^n*(1+k)/(1-k)) \\\\ _Charles R Greathouse IV_, Jun 03 2015"
			],
			"xref": [
				"See A038631 for continued fraction.",
				"Cf. A000005, A000203 (see formulas), A211705, A211706."
			],
			"keyword": "nonn,cons",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, Nov 18 2001",
			"ext": [
				"More terms from Randall L. Rathbun, Jan 16 2002"
			],
			"references": 40,
			"revision": 91,
			"time": "2021-02-09T19:40:37-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}