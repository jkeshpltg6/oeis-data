{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A331423",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 331423,
			"data": "0,1,0,7,0,13,0,19,0,25,0,31,0,37,6,43,0,49,0,61,0,61,0,91,0,73,0,79,0,91,0,91,0,97,12,103,0,109,0,133,0,133,0,127,42,133,0,187,0,145,0,151,0,157,12,175,0,169,0,235,0,181,48,187,6,205,0,199,0",
			"name": "Divide each side of a triangle into n\u003e=1 equal parts and trace the corresponding cevians, i.e., join every point, except for the first and last ones, with the opposite vertex. a(n) is the number of points at which three cevians meet.",
			"comment": [
				"Denote the cevians by a0, a1,...,an, b0, b1,...,bn, c0, c1,...,cn. For any given n, the indices (i,j,k) of (ai, bj, ck) meeting at a point are the integer solutions of:",
				"n^3 - (i + j + k)*n^2 + (j*k + k*i + i*j)*n - 2*i*j*k = 0,  with 0 \u003c i, j, k \u003c n",
				"or, equivalently and shorter,",
				"(n-i)*(n-j)*(n-k) - i*j*k = 0, with 0 \u003c i, j, k \u003c n.",
				"From _N. J. A. Sloane_, Feb 14 2020: (Start)",
				"Stated another way, a(n) = number of triples (i,j,k) in [1,n-1] X [1,n-1] X [1,n-1] such that (i/(n-i))*(j/(n-j))*(k/(n-k)) = 1.",
				"This is the quantity N3 mentioned in A091908.",
				"Indices of zeros are precisely all odd numbers except those listed in A332378.",
				"(End)"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A331423/b331423.txt\"\u003eTable of n, a(n) for n = 1..1045\u003c/a\u003e (first 200 terms from Robert Israel)",
				"Peter Kagey, \u003ca href=\"/A331423/a331423.png\"\u003eAn illustration of A331423(4) = 7\u003c/a\u003e.",
				"Hugo Pfoertner, \u003ca href=\"http://www.randomwalk.de/sequences/a091908.pdf\"\u003eVisualization of diagonal intersections in an equilateral triangle.\u003c/a\u003e"
			],
			"maple": [
				"Ceva:= proc(n) local a, i, j, k; a:=0;",
				"for i from 1 to n-1 do",
				"for j from 1 to n-1 do",
				"for k from 1 to n-1 do",
				"if i*j*k/((n-i)*(n-j)*(n-k)) = 1 then a:=a+1; fi;",
				"od: od: od: a; end;",
				"t1:=[seq(Ceva(n), n=1..80)];  # _N. J. A. Sloane_, Feb 14 2020"
			],
			"mathematica": [
				"CevIntersections[n_] := Length[Solve[(n - i)*(n - j)*(n - k) - i*j*k == 0 \u0026\u0026 0 \u003c i \u003c n \u0026\u0026  0 \u003c j \u003c n \u0026\u0026 0 \u003c k \u003c n, {i, j, k}, Integers]];",
				"Map[CevIntersections[#] \u0026, Range[50]]"
			],
			"program": [
				"(PARI) A331423(n) = sum(i=1, n-1, sum(j=1, n-1, sum(k=1, n-1, (1==(i*j*k)/((n-i)*(n-j)*(n-k)))))); \\\\ (After the Maple program) - _Antti Karttunen_, Dec 12 2021"
			],
			"xref": [
				"Cf. A091908, A332378. Bisections are A331425, A331428."
			],
			"keyword": "nonn,look",
			"offset": "1,4",
			"author": "_César Eliud Lozada_, Jan 16 2020",
			"references": 5,
			"revision": 44,
			"time": "2021-12-12T16:08:23-05:00",
			"created": "2020-02-14T11:48:22-05:00"
		}
	]
}