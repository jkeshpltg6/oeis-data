{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A285950",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 285950,
			"data": "1,3,4,5,7,8,10,12,13,14,16,18,19,21,22,23,25,26,28,30,31,33,34,35,37,39,40,41,43,44,46,48,49,50,52,54,55,57,58,59,61,63,64,65,67,68,70,72,73,75,76,77,79,80,82,84,85,86,88,90,91,93,94,95,97,98",
			"name": "Positions of 0's in A285949; complement of A285951.",
			"comment": [
				"Conjecture: 3n/2 - a(n) is in {0, 1/2, 1} for all n \u003e= 1.",
				"From _Michel Dekking_, Sep 03 2019: (Start)",
				"Proof of the conjecture by Kimberling: more is true. Here follows a proof of the formula below. Let T be the transform T(01)=0, T(1)=0.",
				"Consider the return word structure of A285949 for the word 0:",
				"     A285949 = 01| 0| 0| 01| 0| 01| 01| 0| 0| 01| 01|  ....",
				"[See Justin \u0026 Vuillon (2000) for definition of return word. - _N. J. A. Sloane_, Sep 23 2019]",
				"The two return words are v:=0 and w:=01. Always v = T(1)and w = T(01) in this decomposition of the image T(A010060) of A010060 under the transform. It follows that the return words occur as the Thue-Morse word 21121221211... on the alphabet {2,1}. But the lengths of the return words corresponds to the differences between the indices where the 0's occur in A285949, which generate (a(n)).",
				"As the Thue-Morse word is a concatenation of 12 and 21 which, considered as integers, both add to 3, it follows that a(2n+1) = 3n+1. Similarly, it follows that a(2n) = 3n - A010060(n).",
				"(End)"
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A285950/b285950.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Jacques Justin and Laurent Vuillon, \u003ca href=\"http://www.numdam.org/item/ITA_2000__34_5_343_0/\"\u003eReturn words in Sturmian and episturmian words\u003c/a\u003e, RAIRO-Theoretical Informatics and Applications 34.5 (2000): 343-356."
			],
			"formula": [
				"a(2n) = 3n - A010060(n); a(2n+1) = 3n + 1. - _Michel Dekking_, Sep 03 2019"
			],
			"example": [
				"As a word, A285949 = 0100010010100010100100010..., in which 0 is in positions 1,3,4,5,7,..."
			],
			"mathematica": [
				"s = Nest[Flatten[# /. {0 -\u003e {0, 1}, 1 -\u003e {1, 0}}] \u0026, {0}, 7]  (* Thue-Morse, A010060 *)",
				"w = StringJoin[Map[ToString, s]]",
				"w1 = StringReplace[w, {\"0\" -\u003e \"01\", \"1\" -\u003e \"0\"}]  (* A285949, word *)",
				"st = ToCharacterCode[w1] - 48 (* A285949, sequence *)",
				"Flatten[Position[st, 0]]  (* A285950 *)",
				"Flatten[Position[st, 1]]  (* A285951 *)"
			],
			"xref": [
				"Cf. A010060, A003849, A285949, A285951, A285952."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Clark Kimberling_, May 02 2017",
			"references": 6,
			"revision": 27,
			"time": "2019-10-03T08:53:16-04:00",
			"created": "2017-05-03T08:34:05-04:00"
		}
	]
}