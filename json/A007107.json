{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007107",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7107,
			"id": "M4668",
			"data": "1,0,0,1,9,216,7570,357435,22040361,1721632024,166261966956,19459238879565,2714812050902545,445202898702992496,84798391618743138414,18567039007438379656471,4631381194792101913679985,1305719477625154539392776080,413153055417968797025496881656",
			"name": "Number of labeled 2-regular digraphs with n nodes.",
			"comment": [
				"Or number of n X n matrices with exactly two 1's in each row and column which are not in the main diagonal, other entries 0 (cf. A001499). - _Vladimir Shevelev_, Mar 22 2010",
				"Number of 2-factors of the n-crown graph. - _Andrew Howroyd_, Feb 28 2016"
			],
			"reference": [
				"R. W. Robinson, personal communication.",
				"R. W. Robinson, Numerical implementation of graph counting algorithms, AGRC Grant, Math. Dept., Univ. Newcastle, Australia, 1982.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A007107/b007107.txt\"\u003eTable of n, a(n) for n = 0..254\u003c/a\u003e (first 49 terms from R. W. Robinson)",
				"O. Gonzalez, C. Beltran and I. Santamaria, \u003ca href=\"http://arxiv.org/abs/1301.6196\"\u003eOn the Number of Interference Alignment Solutions for the K-User MIMO Channel with Constant Coefficients\u003c/a\u003e, arXiv preprint arXiv:1301.6196 [cs.IT], 2013. - From _N. J. A. Sloane_, Feb 19 2013",
				"R. J. Mathar, \u003ca href=\"/A007107/a007107.pdf\"\u003eOEIS A007107\u003c/a\u003e, Mar 15 2019"
			],
			"formula": [
				"a(n) = Sum_{k=0..n} Sum_{s=0..k} Sum_{j=0..n-k} (-1)^(k+j-s)*n!*(n-k)!*(2n-k-2j-s)!/(s!*(k-s)!*(n-k-j)!^2*j!*2^(2n-2k-j)). - _Shanzhen Gao_, Nov 05 2007",
				"a(n) ~ 2*sqrt(Pi) * n^(2*n+1/2) / exp(2*n+5/2). - _Vaclav Kotesovec_, May 09 2014"
			],
			"maple": [
				"a:= proc(n) option remember; `if`(n\u003c5, ((n-1)*(n-2)/2)^2,",
				"      (n-1)*(2*(n^3-2*n^2+n+1)*a(n-1)/(n-2)+((n^2-2*n+2)*",
				"      (n+1)*a(n-2) +(2*n^2-6*n+1)*n*a(n-3)+(n-3)*(a(n-4)*",
				"      (n^3-5*n^2+3)-(n-4)*(n-1)*(n+1)*a(n-5))))/(2*n))",
				"    end:",
				"seq(a(n), n=0..20);  # _Alois P. Heinz_, Apr 10 2017"
			],
			"mathematica": [
				"Table[Sum[Sum[Sum[(-1)^(k+j-s)*n!*(n-k)!*(2n-k-2j-s)!/(s!*(k-s)!*(n-k-j)!^2*j!*2^(2n-2k-j)),{j,0,n-k}],{s,0,k}],{k,0,n}],{n,0,20}] (* _Vaclav Kotesovec_, May 09 2014 after _Shanzhen Gao_ *)"
			],
			"program": [
				"(PARI) a(n)=sum(k=0,n, sum(s=0,k, sum(j=0,n-k, (-1)^(k+j-s)*n!*(n-k)!*(2*n-k-2*j-s)!/(s!*(k-s)!*(n-k-j)!^2*j!*2^(2*n-2*k-j))))) \\\\ _Charles R Greathouse IV_, Feb 08 2017"
			],
			"xref": [
				"Cf. column t=0 of A284989.",
				"Cf. A007108 (log transform), A197458 (row and column sum \u003c=2), A219889 (unlabeled)."
			],
			"keyword": "nonn,nice",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_",
			"references": 14,
			"revision": 56,
			"time": "2019-03-15T10:12:33-04:00",
			"created": "1994-04-28T03:00:00-04:00"
		}
	]
}