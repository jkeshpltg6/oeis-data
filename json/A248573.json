{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A248573",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 248573,
			"data": "1,2,4,8,5,16,3,10,32,6,20,21,64,12,13,40,42,128,24,26,80,84,85,256,48,17,52,53,160,168,170,512,96,11,34,104,35,106,320,336,113,340,341,1024,192,7,22,68,69,208,23,70,212,213,640,672,75,226,680,227,682,2048",
			"name": "An irregular triangle giving the Collatz-Terras tree.",
			"comment": [
				"From _Wolfdieter Lang_, Oct 31 2014: (Start)",
				"(old name corrected)",
				"Irregular triangle CT(l, m) such that the first three rows l = 0, 1 and 2 are 1, 2, 4, respectively, and for l \u003e 3 the row entries CT(l, m) are obtained from replacing the numbers of row l-1 by (2*x-1)/3, 2*x if they are 2 (mod 3) and by 2*x otherwise.",
				"The modified Collatz (or Collatz-Terras) map sends a positive number x to x/2 if it is even and to (3*x+1)/2 if it is odd (see A060322). The present tree (without the complete tree originating at CT(2,1) = 1) can be considered as an incomplete binary tree, with nodes (vertices) of out-degree 2 if they are 2 (mod 3) and out-degree 1 otherwise. In the example below, the edges (branches) could be labeled L (left) or V (vertical).",
				"The row length sequence is A060322(l+1), l\u003e=0. (End)",
				"The Collatz conjecture is true if and only if all odd numbers appear in this sequence.",
				"This sequence is similar to A127824."
			],
			"link": [
				"Sebastian Karlsson, \u003ca href=\"/A248573/b248573.txt\"\u003eRows l = 0..35, flattened\u003c/a\u003e",
				"Riho Terras, \u003ca href=\"http://matwbn.icm.edu.pl/ksiazki/aa/aa30/aa3034.pdf\"\u003eA stopping time problem on the positive integers\u003c/a\u003e, Acta Arith. 30 (1976) 241-252.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CollatzProblem.html\"\u003eCollatz Problem\u003c/a\u003e."
			],
			"example": [
				"The irregular triangle CT(l,m) begins:",
				"l\\m   1   2  3   4   5   6   7   8   9  10  11   12  13   14   15  16  17   18   19  20  21   22   23   24 ...",
				"0:    1",
				"1:    2",
				"2:    4  here the 1, which would generate the complete tree again, is omitted",
				"3:    8",
				"4:    5  16",
				"5:    3  10 32",
				"6:    6  20 21  64",
				"7:   12  13 40  42 128",
				"8:   24  26 80  84  85 256",
				"9:   48  17 52  53 160 168 170 512",
				"10:  96  11 34 104  35 106 320 336 113 340 341 1024",
				"11: 192   7 22  68  69 208  23  70 212 213 640  672  75  226  680 227 682 2048",
				"12: 384  14 44  45 136 138 416  15  46 140 141  424 426 1280 1344 150 452  453 1360 151 454 1364 1365 4096",
				"... reformatted, and extended - _Wolfdieter Lang_, Oct 31 2014",
				"--------------------------------------------------------------------------------------------------------------",
				"From _Wolfdieter Lang_, Oct 31 2014: (Start)",
				"The Collatz-Terras tree starting with 4 looks like (numbers x == 2 (mod 3) are marked with a left bar, and the left branch ends then in (2*x-1)/3 and the vertical one in 2*x)",
				"l=2:                                                                                        4",
				"l=3:                                                                                       |8",
				"l=4:                                                    |5                                 16",
				"l=5:    3                                               10                                |32",
				"l=6:    6                                              |20   21                            64",
				"l=7:   12                     13                        40   42                          |128",
				"l=8:   24                    |26                       |80   84            85             256",
				"l=9:   48           |17       52              |53      160  168          |170            |512",
				"l=10:  96     |11    34     |104        |35   106      320  336     |113  340      |341  1024",
				"l=11: 192   7  22   |68  69  208   23|   70   212  213 640  672  75  226  680  227  682  2048",
				"...",
				"E.g., x = 7 = CT(11, 2) leads back to 4 via 7, 11, 17, 26, 13, 20, 10, 5, 8, 4, and from there back to 2, 1.",
				"(End)",
				"--------------------------------------------------------------------------------------------------------------"
			],
			"xref": [
				"Cf. A127824, A060322, A088975."
			],
			"keyword": "nonn,tabf",
			"offset": "0,2",
			"author": "_Nico Brown_, Oct 08 2014",
			"ext": [
				"Edited. New name (old corrected name as comment). - _Wolfdieter Lang_, Oct 31 2014"
			],
			"references": 8,
			"revision": 44,
			"time": "2021-01-17T12:58:15-05:00",
			"created": "2014-10-30T18:48:20-04:00"
		}
	]
}