{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A247665",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 247665,
			"data": "2,3,4,5,7,9,8,11,13,17,19,23,15,29,14,31,37,41,43,47,53,59,61,67,71,73,25,27,79,83,16,49,89,97,101,103,107,109,113,121,127,131,137,139,149,151,157,163,167,169,173,179,181,191,85,193,57,197,199,211,223",
			"name": "a(1)=2; thereafter a(n) is the smallest number not yet used which is compatible with the condition that a(n) is relatively prime to the next n terms.",
			"comment": [
				"It appears that a(k) is even iff k = 2^i-1 (cf. A248379). It also appears that all powers of 2 occur in the sequence. (_Amarnath Murthy_)",
				"The indices of even terms and their values are [1, 2], [3, 4], [7, 8], [15, 14], [31, 16], [63, 32], [127, 64], [255, 128], [511, 122], ...",
				"Will the numbers 6, 10, 21, 22, ... ever occur? 12, 18, 20, ... are also missing, but if 6 never appears then neither will 12, etc.",
				"A related question: are all terms deficient? - _Peter Munn_, Jul 20 2017"
			],
			"reference": [
				"_Amarnath Murthy_, Email to _N. J. A. Sloane_, Oct 05 2014."
			],
			"link": [
				"Russ Cox, \u003ca href=\"/A247665/b247665.txt\"\u003eTable of n, a(n) for n = 1..100000\u003c/a\u003e",
				"Russ Cox, \u003ca href=\"/A247665/a247665.go.txt\"\u003eGo program\u003c/a\u003e (can compute 5 million terms in about 5 minutes)",
				"Russ Cox, \u003ca href=\"/A247665/a247665.txt\"\u003eTable of n, a(n) for n = 1..203299677, composite a(n) only\u003c/a\u003e; a(203299678) \u003e 2^32."
			],
			"example": [
				"a(1) = 2 must be rel. prime to a(2), so a(2)=3.",
				"a(2) = 3 must be rel. prime to a(3) and a(4), so we can take them to be 4 and 5.",
				"a(3) = 4 must be rel. prime to a(5), a(6), so we must take them to be 7,9.",
				"a(4) = 5 must be rel. prime to a(7), a(8), so we must take them to be 8,11.",
				"At each step after the first, we must choose two new numbers, and we must make sure that not only are they rel. prime to a(n), they are also rel. prime to all a(i), i\u003en, that have been already chosen."
			],
			"program": [
				"(PARI) m=100; v=vector(m); u=vectorsmall(100*m); for(n=1, m, for(i=2, 10^9, if(!u[i], for(j=(n+1)\\2, n-1, if(gcd(v[j], i)\u003e1, next(2))); v[n]=i; u[i]=1; break))); v \\\\ _Jens Kruse Andersen_, Oct 08 2014",
				"(Haskell)",
				"a247665 n = a247665_list !! (n-1)",
				"a247665_list = 2 : 3 : f [3] [4..] where",
				"   f (x:xs) zs = ys ++ f (xs ++ ys) (zs \\\\ ys) where",
				"     ys = [v, head [w | w \u003c- vs, gcd v w == 1]]",
				"     (v:vs) = filter (\\u -\u003e gcd u x == 1 \u0026\u0026 all ((== 1) . (gcd u)) xs) zs",
				"-- _Reinhard Zumkeller_, Oct 09 2014",
				"(Sage)",
				"# s is the starting point (2 in A247665).",
				"def gen(s):",
				"    sequence = [s]",
				"    available = list(range(2,2*s))",
				"    available.pop(available.index(s))",
				"    yield s",
				"    while True:",
				"        available.extend(range(available[-1]+1,next_prime(available[-1])+1))",
				"        for i,e in enumerate(available):",
				"            if all(gcd(e, sequence[j])==1 for j in range(-len(sequence)//2,0)):",
				"                available.pop(i)",
				"                sequence.append(e)",
				"                yield(e)",
				"                break",
				"g = gen(2)",
				"[next(g) for i in range(40)]  # (gets first 40 terms of A247665)",
				"# _Nadia Heninger_, Oct 28 2014"
			],
			"xref": [
				"Cf. A248379, A248381, A248388, A248389, A248390, A248391, A249049, A249050, A249058, A249556.",
				"Indices of primes and prime powers: A248387, A248918.",
				"Lengths of runs of primes: A249033.",
				"A090252 = similar to A247665 but start with a(1)=1. A249559 starts with a(1)=3.",
				"A249064 is a different generalization.",
				"A064413 is another similar sequence."
			],
			"keyword": "nonn,look",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_, Oct 06 2014 and Oct 08 2014",
			"ext": [
				"More terms from _Jens Kruse Andersen_, Oct 06 2014",
				"Further terms from _Russ Cox_, Oct 08 2014"
			],
			"references": 23,
			"revision": 85,
			"time": "2020-02-27T16:55:30-05:00",
			"created": "2014-10-06T15:07:21-04:00"
		}
	]
}