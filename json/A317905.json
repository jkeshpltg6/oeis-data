{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A317905",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 317905,
			"data": "0,1,1,2,1,2,1,1,1,1,1,1,4,1,1,2,1,1,1,1,2,3,2,1,1,1,1,2,1,1,2,1,1,1,1,1,1,2,1,2,1,1,1,2,2,1,1,1,3,1,3,1,1,1,1,1,1,6,1,1,3,1,1,1,1,2,2,2,1,1,1,1,2,1,1,2,1,1,1,1,1,1,2,1,5,1,1",
			"name": "Convergence speed of m^^m, where m = A067251(n) and n \u003e= 2. a(n) = f(m, m) - f(m, m - 1), where f(x, y) corresponds to the maximum value of k, such that x^^y == x^^(y + 1) (mod 10^k).",
			"comment": [
				"It is possible to anticipate the convergence speed of a^^m, where ^^ indicates tetration or hyper-4 (e.g., 3^^4=3^(3^(3^3))), simply looking at the congruence (mod 25) of m. In fact, assuming m \u003e 2, a(n) = 1 for any m == 2, 3, 4, 6, 8, 9, 11, 12, 13, 14, 16, 17, 19, 21, 22, 23 (mod 25), and a(n) \u003e= 2 otherwise.",
				"It follows that 32/45 = 71.11% of the a(n) assume unitary value.",
				"You can also obtain an arbitrary high convergence speed, such as taking the beautiful base b = 999...99 (9_9_9... n times), which gives a(n) = len(b), for any len(b) \u003e 1. Thus, 99...9^^m == 99...9^^(m + 1) (mod m*10^len(b)), as proved by Ripà in \"La strana coda della serie n^n^...^n\", pages 25-26. In fact, m = 99...9 == 24 (mod 25) and a(m=24) \u003e 1.",
				"From _Marco Ripà_, Dec 19 2021: (Start)",
				"Knowing the congruence speed of a given base is very useful in order to calculate the exact number of stable digits of all its tetrations of height b \u003e 1. As an example, let us consider all the a(n) such that n is congruent to 4 (mod 9) (i.e., all the tetration bases belonging to the congruence class 5 (mod 10)). Then, the exact number of stable digits (#S(m, b)) of any tetration m^^b (i.e., the number of its last \"frozen\" digits) such that m is congruent to 5 (mod 10), for any b \u003e= 3, can automatically be calculated by simply knowing that (under the stated constraint) the congruence speed of the m corresponds to the 2-adic valuation of (m^2 - 1) minus 1. Thus, let k = 1, 2, 3, ..., and we have that",
				"If m = 20*k - 5, then #S(m, b \u003e 2) = b*(v_2(m^2 - 1) - 1) + 1;",
				"If m = 20*k + 5, then #S(m, b \u003e 2) = (b + 1)*(v_2(m^2 - 1) - 1);",
				"If m = 5, then #S(m, 1) = 1, #S(m, 2) = 3, #S(m, 3) = 4, #S(m, b \u003e 3) = 2.",
				"(End)"
			],
			"reference": [
				"Marco Ripà, La strana coda della serie n^n^...^n, Trento, UNI Service, Nov 2011. ISBN 978-88-6178-789-6"
			],
			"link": [
				"Michel Marcus, \u003ca href=\"/A317905/a317905.gp.txt\"\u003ereducetower.gp\u003c/a\u003e (from Math StackExchange).",
				"Math StackExchange, \u003ca href=\"https://math.stackexchange.com/questions/630134/calculating-an-pmod-m-in-the-general-case\"\u003eCalculating a^n (mod m) in the general case\u003c/a\u003e",
				"Marco Ripà, \u003ca href=\"https://www.researchgate.net/publication/328493277_On_the_Convergence_Speed_of_Tetration\"\u003eOn the Convergence Speed of Tetration\u003c/a\u003e, ResearchGate (2018).",
				"Marco Ripà, \u003ca href=\"https://doi.org/10.7546/nntdm.2020.26.3.245-260\"\u003eOn the constant congruence speed of tetration\u003c/a\u003e, Notes on Number Theory and Discrete Mathematics, Volume 26, 2020, Number 3, Pages 245—260.",
				"Marco Ripà, \u003ca href=\"https://doi.org/10.7546/nntdm.2021.27.4.43-61\"\u003eThe congruence speed formula\u003c/a\u003e, Notes on Number Theory and Discrete Mathematics, 2021, 27(4), 43-61.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Tetration\"\u003eTetration\u003c/a\u003e"
			],
			"formula": [
				"For any c, if n == 5,7,14,17,22,23,24,29,32,39,41,45,46 (mod 45), a(n+45*c) \u003e= 2; a(n) = 1 otherwise. - _Marco Ripà_, Sep 28 2018",
				"If n == 4 (mod 9), then a(n) = v_2(a(n)^2 - 1) - 1, where v_2(x) indicates the 2-adic valuation of x. - _Marco Ripà_, Dec 19 2021"
			],
			"example": [
				"For m = 25, a(23) = 3 implies that 25^^(25+i) freezes 3*i \"new\" rightmost digits (i \u003e= 0)."
			],
			"program": [
				"(PARI) \\\\ uses reducetower.gp from links",
				"f2(x,y) = my(k=0); while(reducetower(x, 10^k, y) == reducetower(x, 10^k, y+1), k++); k;",
				"f1(n) = polcoef(x*(x+1)*(x^4-x^3+x^2-x+1)*(x^4+x^3+x^2+x+1) / ((x-1)^2*(x^2+x+1)*(x^6+x^3+1)) + O(x^(n+1)), n, x); \\\\ A067251",
				"a(n) =  my(m=f1(n)); f2(m, m) - f2(m, m-1);",
				"lista(nn) = {for (n=2, nn, print1(a(n), \", \"););} \\\\ _Michel Marcus_, Jan 27 2021"
			],
			"xref": [
				"Cf. A067251, A317824, A317903, A349425."
			],
			"keyword": "nonn,base",
			"offset": "2,4",
			"author": "_Marco Ripà_, Aug 10 2018",
			"ext": [
				"Edited by _Jinyuan Wang_, Aug 30 2020"
			],
			"references": 13,
			"revision": 96,
			"time": "2021-12-20T14:37:57-05:00",
			"created": "2018-09-23T23:12:05-04:00"
		}
	]
}