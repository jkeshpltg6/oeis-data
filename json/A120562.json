{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A120562",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 120562,
			"data": "1,1,1,2,1,2,2,3,1,3,2,3,2,4,3,5,1,4,3,4,2,5,3,5,2,5,4,6,3,7,5,8,1,6,4,5,3,7,4,7,2,6,5,7,3,8,5,8,2,7,5,7,4,9,6,10,3,9,7,10,5,12,8,13,1",
			"name": "Sum of binomial coefficients binomial(i+j, i) modulo 2 over all pairs (i,j) of positive integers satisfying 3i+j=n.",
			"comment": [
				"a(n) is the number of 'vectors' (..., e_k, e_{k-1}, ..., e_0) with e_k in {0,1,3} such that Sum_{k} e_k 2^k = n. a(2^n-1) = F(n+1)*a(2^{k+1}+j) + a(j) = a(2^k+j) + a(2^{k-1}+j) if 2^k \u003e 4j. This sequence corresponds to the pair (3,1) as Stern's diatomic sequence [A002487] corresponds to (2,1) and Gould's sequence [A001316] corresponds to (1,1). There are many interesting similarities to A000119, the number of representations of n as a sum of distinct Fibonacci numbers.",
				"A120562 can be generated from triangle A177444. Partial sums of A120562 = A177445. - _Gary W. Adamson_, May 08 2010",
				"The Ca1 and Ca2 triangle sums, see A180662 for their definitions, of Sierpinski's triangle A047999 equal this sequence. Some A120562(2^n-p) sequences, 0 \u003c= p \u003c= 32, lead to known sequences, see the crossrefs. - _Johannes W. Meijer_, Jun 05 2011"
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A120562/b120562.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"S. Northshield, \u003ca href=\"http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.530.857\"\u003eSums across Pascal's triangle modulo 2\u003c/a\u003e, Congressus Numerantium, 200, pp. 35-52, 2010."
			],
			"formula": [
				"Recurrence; a(0)=a(1)=1, a(2*n)=a(n) and a(2*n+1)=a(n)+a(n-1).",
				"G.f.: A(x) = Product_{i\u003e=0} (1 + x^(2^i) + x^(3*2^i)) = (1 + x + x^3)*A(x^2).",
				"a(n-1) \u003c\u003c n^x with x = log_2(phi) = 0.69424... - _Charles R Greathouse IV_, Dec 27 2011"
			],
			"example": [
				"a(2^n)=1 since a(2n)=a(n)."
			],
			"maple": [
				"p := product((1+x^(2^i)+x^(3*2^i)), i=0..25): s := series(p, x, 1000): for k from 0 to 250 do printf(`%d, `, coeff(s, x, k)) od:",
				"A120562:=proc(n) option remember; if n \u003c0 then A120562(n):=0 fi: if (n=0 or n=1) then 1 elif n mod 2 = 0 then A120562(n/2) else A120562((n-1)/2) + A120562((n-3)/2); fi; end: seq(A120562(n),n=0..64); # _Johannes W. Meijer_, Jun 05 2011"
			],
			"mathematica": [
				"a[0] = a[1] = 1; a[n_?EvenQ] := a[n] = a[n/2]; a[n_?OddQ] := a[n] = a[(n-1)/2] + a[(n-1)/2 - 1]; Table[a[n], {n, 0, 64}] (* _Jean-François Alcover_, Sep 29 2011 *)",
				"Nest[Append[#1, If[EvenQ@ #2, #1[[#2/2 + 1]], Total@ #1[[#2 ;; #2 + 1]] \u0026 @@ {#1, (#2 - 1)/2}]] \u0026 @@ {#, Length@ #} \u0026, {1, 1}, 10^4 - 1] (* _Michael De Vlieger_, Feb 19 2019 *)"
			],
			"xref": [
				"Cf. A001316 (1,1), A002487 (2,1), A120562 (3,1), A112970 (4,1), A191373 (5,1).",
				"Cf. A177444, A177445. - _Gary W. Adamson_, May 08 2010",
				"Cf. A000012 (p=0), A000045 (p=1, p=2, p=4, p=8, p=16, p=32), A000071 (p=3, p=6, p=12, p=13, p=24, p=26), A001610 (p=5, p=10, p=20), A001595 (p=7, p=14, p=28), A014739 (p=11, p=22, p=29), A111314 (p=15, p=30), A027961 (p=19), A154691 (p=21), A001911 (p=23). - _Johannes W. Meijer_, Jun 05 2011"
			],
			"keyword": "easy,nonn",
			"offset": "0,4",
			"author": "Sam Northshield (samuel.northshield(AT)plattsburgh.edu), Aug 07 2006",
			"ext": [
				"Reference edited and link added by _Jason G. Wurtzel_, Aug 22 2010"
			],
			"references": 10,
			"revision": 45,
			"time": "2019-02-20T08:20:57-05:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}