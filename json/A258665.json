{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A258665",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 258665,
			"data": "0,0,0,1,5,20,116,791,6203,55000,543576,5922813,70518113,910704988,12678282940,189251856883,3015212009143,51067548545968,916175515710896,17355891466436025,346195661281979133,7252651426282955236,159210312386078554436,3654549974493252076175",
			"name": "A total of n married couples, including a mathematician M and his wife, are to be seated at the 2n chairs around a circular table, with no man seated next to his wife. After the ladies are seated at every other chair, M is the first man allowed to choose one of the remaining chairs. The sequence gives the number of ways of seating the other men, with no man seated next to his wife, if M chooses the chair that is 5 seats clockwise from his wife's chair.",
			"comment": [
				"This is a variation of the classic ménage problem (cf. A000179).",
				"It is known [Riordan, ch. 8, ex. 7(b)] that, after the ladies are seated at every other chair, the number U_n of ways of seating the men in the ménage problem has asymptotic expansion U_n ~ e^(-2)*n!*(1 + Sum_{k\u003e=1}(-1)^k/(k!(n-1)_k)), where (n)_k = n*(n-1)*...*(n-k+1).",
				"Therefore, it is natural to conjecture that a(n) ~ e^(-2)*n!/(n-2)*(1 + sum{k\u003e=1}(-1)^k/(k!(n-1)_k))."
			],
			"reference": [
				"I. Kaplansky and J. Riordan, The problème des ménages, Scripta Math. 12, (1946), 113-124.",
				"J. Riordan, An Introduction to Combinatorial Analysis, Wiley, 1958, chs. 7, 8."
			],
			"link": [
				"Peter J. C. Moses, \u003ca href=\"/A258665/a258665.pdf\"\u003eSeatings for 6 couples\u003c/a\u003e",
				"I. Kaplansky and J. Riordan, \u003ca href=\"/A000166/a000166_1.pdf\"\u003eThe problème des ménages\u003c/a\u003e, Scripta Math. 12, (1946), 113-124. [Scan of annotated copy]",
				"E. Lucas, \u003ca href=\"https://archive.org/details/thoriedesnombre00lucagoog/page/n495\"\u003eSur le problème des ménages\u003c/a\u003e, Théorie des nombres, Paris, 1891, 491-496.",
				"Vladimir Shevelev, Peter J. C. Moses, \u003ca href=\"http://arxiv.org/abs/1101.5321\"\u003eThe ménage problem with a known mathematician\u003c/a\u003e, arXiv:1101.5321 [math.CO], 2011, 2015.",
				"Vladimir Shevelev and Peter J. C. Moses, \u003ca href=\"http://www.emis.de/journals/INTEGERS/papers/q72/q72.Abstract.html\"\u003eAlice and Bob go to dinner:A variation on menage\u003c/a\u003e, INTEGERS, Vol. 16(2016), #A72.",
				"J. Touchard, \u003ca href=\"http://gallica.bnf.fr/ark:/12148/bpt6k31506/f631.image\"\u003eSur un problème de permutations\u003c/a\u003e, C.R. Acad. Sci. Paris, 198 (1934), 631-633."
			],
			"formula": [
				"a(n) = Sum_{0\u003c=k\u003c=n-1} (-1)^k*(n-k-1)! * Sum_{max(k-n+3, 0)\u003c=j\u003c=min(k,2)} binomial(4-j, j)*binomial(2*n-k+j-6, k-j)."
			],
			"mathematica": [
				"enumerateSeatings[pairs_,d_]:=If[d==1||d\u003e=2pairs-1||EvenQ[d],{},",
				"Map[#[[1]]\u0026,DeleteCases[Map[{#,Differences[#]}\u0026[Riffle[Range[pairs],#]]\u0026,Map[Insert[#,1,(d+1)/2]\u0026,Permutations[#,{Length[#]}]\u0026[Rest[Range[pairs]]]]],{{___},{___,0,___}}]]];",
				"enumerateSeatings[6,5]",
				"a[pairs_,d_]:=If[pairs\u003c=#-1||EvenQ[d]||d==1,0,Sum[((-1)^k)*(pairs-k-1)!Sum[Binomial[2#-j-4,j]*Binomial[2(pairs-#)-k+j+2,k-j],{j,Max[#+k-pairs-1,0],Min[k,#-2]}],{k,0,pairs-1}]]\u0026[(d+3)/2];",
				"Table[a[n,5],{n,15}] (* _Peter J. C. Moses_, Jun 13 2015 *)"
			],
			"program": [
				"(PARI) a(n) = sum(k=0,n-1, (-1)^k*(n-k-1)! * sum(j=max(k-n+3, 0), min(k,2), binomial(4-j, j)*binomial(2*n-k+j-6, k-j))); \\\\ _Michel Marcus_, Jun 13 2015"
			],
			"xref": [
				"Cf. A000179, A258664, A258666, A258667, A258673, A259212."
			],
			"keyword": "nonn",
			"offset": "1,5",
			"author": "_Vladimir Shevelev_ and _Peter J. C. Moses_, Jun 07 2015",
			"references": 9,
			"revision": 67,
			"time": "2020-07-20T16:20:55-04:00",
			"created": "2015-06-18T05:55:08-04:00"
		}
	]
}