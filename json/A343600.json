{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A343600",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 343600,
			"data": "0,1,2,3,4,7,6,5,8,9,12,21,10,13,16,19,22,25,18,15,24,11,14,17,20,23,26,27,36,63,30,39,48,57,66,75,28,31,34,37,40,43,46,49,52,55,58,61,64,67,70,73,76,79,54,45,72,33,42,51,60,69,78,29,32,35,38,41",
			"name": "For any positive number n, the ternary representation of a(n) is obtained by left-rotating the ternary representation of n until a nonzero digit appears again as the leftmost digit; a(0) = 0.",
			"comment": [
				"This sequence is a permutation of the nonnegative integers with inverse A343601."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A343600/b343600.txt\"\u003eTable of n, a(n) for n = 0..6561\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"A053735(a(n)) = A053735(n).",
				"A081604(a(n)) = A081604(n).",
				"a^k(n) = n for k = A160384(n) (where a^k denotes the k-th iterate of a)."
			],
			"example": [
				"The first terms, in base 10 and in base 3, are:",
				"  n   a(n)  ter(n)  ter(a(n))",
				"  --  ----  ------  ---------",
				"   0     0       0          0",
				"   1     1       1          1",
				"   2     2       2          2",
				"   3     3      10         10",
				"   4     4      11         11",
				"   5     7      12         21",
				"   6     6      20         20",
				"   7     5      21         12",
				"   8     8      22         22",
				"   9     9     100        100",
				"  10    12     101        110",
				"  11    21     102        210",
				"  12    10     110        101",
				"  13    13     111        111",
				"  14    16     112        121"
			],
			"program": [
				"(PARI) a(n, base=3) = { my (d=digits(n, base)); for (k=2, #d, if (d[k], return (fromdigits(concat(d[k..#d], d[1..k-1]), base)))); n }"
			],
			"xref": [
				"Cf. A053735, A081604, A139708 (binary variant), A160384, A343601 (inverse)."
			],
			"keyword": "nonn,base,easy",
			"offset": "0,3",
			"author": "_Rémy Sigrist_, Apr 21 2021",
			"references": 3,
			"revision": 10,
			"time": "2021-04-23T01:20:26-04:00",
			"created": "2021-04-22T21:57:16-04:00"
		}
	]
}