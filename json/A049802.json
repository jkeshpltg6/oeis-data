{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A049802",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 49802,
			"data": "0,0,1,0,2,2,4,0,3,4,7,4,7,8,11,0,4,6,10,8,12,14,18,8,12,14,18,16,20,22,26,0,5,8,13,12,17,20,25,16,21,24,29,28,33,36,41,16,21,24,29,28,33,36,41,32,37,40,45,44,49,52,57,0,6,10,16,16",
			"name": "a(n) = n mod 2 + n mod 4 + ... + n mod 2^k, where 2^k \u003c= n \u003c 2^(k+1).",
			"comment": [
				"There is the following connection between this sequence and A080277: A080277(n) = n + n*floor(log_2(n)) - a(n). Since A080277(n) is the solution to a prototypical recurrence in the analysis of the algorithm Merge Sort, that is, T(0) := 0, T(n) := 2*T(floor(n/2)) + n, the sequence a(n) seems to be the major obstacle when trying to find a simple, sum-free solution to this recurrence. It seems hard to get rid of the sum. - Peter C. Heinig (algorithms(AT)gmx.de), Oct 21 2006",
				"When n = 2^k with k \u003e 0 then a(n+1) = k. For this reason, when n-1 is a Mersenne prime then n - 1 = M(p) = 2^p - 1 = 2^a(n+1) - 1 and p = a(n+1) is prime. - _David Morales Marciel_, Oct 23 2015"
			],
			"link": [
				"Metin Sariyar, \u003ca href=\"/A049802/b049802.txt\"\u003eTable of n, a(n) for n = 1..32000\u003c/a\u003e (terms 1..1000 from Paolo P. Lava)",
				"B. Dearden, J. Iiams, and J. Metzger, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL14/Dearden/dearden3r.html\"\u003eA Function Related to the Rumor Sequence Conjecture\u003c/a\u003e, J. Int. Seq. 14 (2011), #11.2.3, Example 7."
			],
			"formula": [
				"From _Robert Israel_, Oct 23 2015: (Start)",
				"a(2*n) = 2*a(n).",
				"a(2*n+1) = 2*a(n) + A070939(n) for n \u003e= 1.",
				"G.f. A(x) satisfies A(x) = 2*(1+x)*A(x^2) + (x/(1-x^2))*Sum_{i\u003e=1} x^(2^i). (End)"
			],
			"maple": [
				"with(numtheory); P:=proc(q) local a,b,c,n; a:=0;",
				"for n from 1 to q do a:=convert(n,binary,decimal); b:=1; c:=0;",
				"while (a mod 10^b)\u003ca do c:=c+convert((a mod 10^b),decimal,binary);",
				"b:=b+1; od; print(c); od; end: P(1000); # _Paolo P. Lava_, Aug 22 2013",
				"f:= proc(n) option remember; local m;",
				"    if n::even then 2*procname(n/2)",
				"    else m:= (n-1)/2; 2*procname(m) + ilog2(m) + 1",
				"    fi",
				"end proc:",
				"f(1):= 0:",
				"map(f, [$1..1000]); # _Robert Israel_, Oct 23 2015"
			],
			"mathematica": [
				"Table[n * Floor@Log[2,n] - Sum[Floor[n*2^-k]*2^k, {k, Log[2,n]}], {n, 100}] (* _Federico Provvedi_, Aug 17 2013 *)"
			],
			"program": [
				"(PARI) a(n) = sum(k=1, logint(n, 2), n % 2^k); \\\\ _Michel Marcus_, Dec 12 2019"
			],
			"xref": [
				"Cf. A049803, A049804, A070939, A080277, A330358."
			],
			"keyword": "nonn",
			"offset": "1,5",
			"author": "_Clark Kimberling_",
			"references": 4,
			"revision": 56,
			"time": "2019-12-13T02:59:14-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}