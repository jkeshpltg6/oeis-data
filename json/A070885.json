{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A070885",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 70885,
			"data": "1,3,6,9,15,24,36,54,81,123,186,279,420,630,945,1419,2130,3195,4794,7191,10788,16182,24273,36411,54618,81927,122892,184338,276507,414762,622143,933216,1399824,2099736,3149604,4724406,7086609,10629915",
			"name": "a(n) = (3/2)*a(n-1) if a(n-1) is even; (3/2)*(a(n-1)+1) if a(n-1) is odd.",
			"comment": [
				"The smallest positive number such that A024629(a(n)) has n digits, per page 9 of the Tanton reference in Links. - _Glen Whitney_, Sep 17 2017"
			],
			"reference": [
				"Wolfram, S. A New Kind of Science. Champaign, IL: Wolfram Media, 2002, p. 123."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A070885/b070885.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"B. Chen, R. Chen, J. Guo, S. Lee et al., \u003ca href=\"http://arxiv.org/abs/1808.04304\"\u003eOn Base 3/2 and its Sequences\u003c/a\u003e, arXiv:1808.04304 [math.NT], 2018.",
				"James Tanton, \u003ca href=\"http://gdaymath.com/wp-content/uploads/2017/04/Chapter-9-EXPLODING-DOTS.pdf\"\u003eExploding Dots, Chapter 9\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/WolframSequences.html\"\u003eWolfram Sequences\u003c/a\u003e"
			],
			"formula": [
				"For n \u003e 1, a(n) = 3*A061419(n) = 3*floor(K*(3/2)^n) where K=1.08151366859... - _Benoit Cloitre_, Aug 18 2002",
				"a(n) = 3*ceiling(a(n-1)/2). - _Benoit Cloitre_, Apr 25 2003",
				"a(n+1) = a(n) + A081848(n), for n \u003e 1. - _Reinhard Zumkeller_, Sep 05 2014"
			],
			"maple": [
				"A070885 := proc(n)",
				"    option remember;",
				"    if n = 1 then",
				"        return 1;",
				"    elif type(procname(n-1),'even') then",
				"        procname(n-1) ;",
				"    else",
				"        procname(n-1)+1 ;",
				"    end if;",
				"    %*3/2 ;",
				"end proc:",
				"seq(A070885(n),n=1..80) ; # _R. J. Mathar_, Jun 18 2018"
			],
			"mathematica": [
				"NestList[If[EvenQ[#],3/2 #,3/2 (#+1)]\u0026,1,40] (* _Harvey P. Dale_, May 18 2018 *)"
			],
			"program": [
				"(Haskell)",
				"a070885 n = a070885_list !! (n-1)",
				"a070885_list = 1 : map (flip (*) 3 . flip div 2 . (+ 1)) a070885_list",
				"-- _Reinhard Zumkeller_, Sep 05 2014"
			],
			"xref": [
				"The constant K is 2/3*K(3) (see A083286). - _Ralf Stephan_, May 29 2003",
				"Cf. A003312.",
				"Cf. A081848.",
				"Cf. A205083 (parity of terms)."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Eric W. Weisstein_, May 14 2002",
			"references": 15,
			"revision": 59,
			"time": "2021-02-12T12:12:28-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}