{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A155078",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 155078,
			"data": "3,4,5,4,3,3,4,4,5,3,13,3,15,4,8,4,8,5,21,8,4,22,25,6,27,26,5,4,31,3,4,4,8,8,32,3,39,38,38,8,43,4,45,22,8,23,19,6,9,50,8,26,24,5,46,4,5,29,18,3,63,4,5,4,7,6,69,8,25,30,47,6,4,74,17,38,79,12,60,8,79,82,85,4,8,43",
			"name": "The representation of n=1,2,3,... in binary is a divisor of the same representation in another base. The sequence is the first such base.",
			"comment": [
				"The pattern of solutions for each binary representation is notable. For 1001= decimal 9, the bases as solutions are 5,8,11,14,... whereas the pattern for 111=decimal 7 is 4,9,11,16,18,....",
				"The binary representation of n corresponds to the unique polynomial p_n(x) with coefficients in {0,1} such that p(2) = n. a(n) is the least x \u003e= 3 such that p_n(x) == 0 mod n. Thus 3 \u003c= a(n) \u003c= n + 2. - _Robert Israel_, Dec 15 2014",
				"From _Rémy Sigrist_, Mar 15 2017: (Start)",
				"If n is even then a(n) \u003c= max(4, n).",
				"If n is odd then a(n) \u003c= n + 2.",
				"If n is odd then n and a(n) are coprime.",
				"If a(n)=4 then n belongs to A062846.",
				"(End)"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A155078/b155078.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"It is simply a matter of converting a binary number to another base to see if the resulting number is a multiple of n. The lowest other base is listed."
			],
			"example": [
				"The n-th term is solved by converting the decimal n to binary then asking to what other base is this representation a multiple of n. For the 5th term, the binary representation is 101; if this is converted to base 3, 101 = 9+0+1 = 10, a multiple of 5. The base 3 is the first base producing a multiple of n: the 5th term is therefore 3."
			],
			"maple": [
				"A155078 := proc(n) local bdgs,b ; bdgs := convert(n,base,2) ; for b from 3 do add(op(i,bdgs)*b^(i-1),i=1..nops(bdgs)) ; if mod n = 0 then RETURN(b); fi; od: end: seq(A155078(n),n=1..100) ; # _R. J. Mathar_, Mar 14 2009",
				"# second Maple program:",
				"f:= proc(n) local b, L,r, sols;",
				"     L:= convert(n,base,2);",
				"     r:= add(L[i]*b^(i-1),i=1..nops(L));",
				"     sols:= subs(0=n,1=n+1,2=n+2,map(t -\u003e rhs(op(t)),{msolve(r,n)})) ;",
				"     min(sols);",
				"end proc:",
				"3, seq(f(n),n=2..100); # _Robert Israel_, Dec 15 2014"
			],
			"xref": [
				"Cf. A062846."
			],
			"keyword": "base,easy,nonn",
			"offset": "1,1",
			"author": "_J. M. Bergot_, Jan 19 2009",
			"ext": [
				"Corrected and extended by _R. J. Mathar_, Mar 14 2009"
			],
			"references": 3,
			"revision": 24,
			"time": "2017-06-13T18:53:45-04:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}