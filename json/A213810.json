{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A213810",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 213810,
			"data": "14561,14083,13613,13151,12697,12251,11813,11383,10961,10547,10141,9743,9353,8971,8597,8231,7873,7523,7181,6847,6521,6203,5893,5591,5297,5011,4733,4463,4201,3947,3701,3463,3233,3011,2797,2591,2393,2203,2021,1847,1681,1523",
			"name": "a(n) = 4*n^2 - 482*n + 14561.",
			"comment": [
				"A \"prime-generating\" polynomial: This polynomial generates 88 distinct primes for n from 0 to 99, just two primes fewer than the record held by the polynomial discovered by N. Boston and M. L. Greenwood, that is 41*n^2 - 4641*n + 88007 (this polynomial is sometimes cited as 41*n^2 + 33*n - 43321, which is the same for the input values [-57, 42], see the references below).",
				"The nonprime terms in the first 100 are: 10961 = 97*113; 10547 = 53*199; 9353 = 47*199; 7181 = 43*167; 6847 = 41*167; 5893 = 71*83; 3233 = 53*61; 2021 = 43*47; 1681 = 41^2; 1763 = 41*43; 2491 = 47*53; 4331 = 61*71.",
				"For n = m + 41 we obtain the polynomial 4*m^2 - 154*m + 1523, which generates 40 primes in a row starting from m=0 (polynomial already reported, see the link below)."
			],
			"reference": [
				"W. Narkiewicz, The Development of Prime Number Theory: from Euclid to Hardy and Littlewood, Springer Monographs in Mathematics, 2000, page 43."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A213810/b213810.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"R. A. Mollin, \u003ca href=\"http://www.jstor.org/stable/2975080\"\u003ePrime-Producing Quadratics\u003c/a\u003e, The American Mathematical Monthly, Vol. 104, No. 6 (1997), pp. 529-544.",
				"Carlos Rivera, \u003ca href=\"http://www.primepuzzles.net/puzzles/puzz_232.htm\"\u003ePuzzle 232: Primes and Cubic polynomials\u003c/a\u003e, The Prime Puzzles \u0026 Problems Connection.",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-3,1)."
			],
			"formula": [
				"a(n) = 4*n^2 - 482*n + 14561.",
				"G.f.: (-15047*x^2+29600*x-14561)/(x-1)^3. - _Alexander R. Povolotsky_, Jun 21 2012",
				"a(n) = 3*a(n-1) - 3*a(n-2) + a(n-3). - _G. C. Greubel_, Feb 26 2017"
			],
			"mathematica": [
				"Table[4n^2-482n+14561,{n,0,41}] (* _Harvey P. Dale_, Sep 09 2014 *)",
				"LinearRecurrence[{3,-3,1},{14561, 14083, 13613}, 50] (* or *) CoefficientList[Series[ (-15047*x^2+29600*x-14561)/(x-1)^3, {x,0,50}], x] (* _G. C. Greubel_, Feb 26 2017 *)"
			],
			"program": [
				"(PARI) x='x+O('x^50); Vec((-15047*x^2+29600*x-14561)/(x-1)^3) \\\\ _G. C. Greubel_, Feb 26 2017"
			],
			"xref": [
				"Cf. A181973, A211773, A211775."
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "_Marius Coman_, Jun 20 2012",
			"ext": [
				"Edited by _N. J. A. Sloane_, Nov 12 2016"
			],
			"references": 1,
			"revision": 42,
			"time": "2019-11-05T05:55:15-05:00",
			"created": "2012-07-20T04:09:24-04:00"
		}
	]
}