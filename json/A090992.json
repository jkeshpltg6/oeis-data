{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A090992",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 90992,
			"data": "7,13,24,45,84,158,296,557,1045,1966,3691,6942,13038,24516,46055,86585,162680,305809,574624,1080106,2029680,3814941,7169145,13474502,25322375,47592650,89441626,168100324,315917527,593742597,1115852904,2097145317",
			"name": "Number of meaningful differential operations of the n-th order on the space R^7.",
			"comment": [
				"Also number of meaningful compositions of the n-th order of the differential operations and Gateaux directional derivative on the space R^6. - Branko Malesevic and Ivana Jovovic (ivana121(AT)EUnet.yu), Jun 21 2007",
				"Also (starting 4,7,...) the number of zig-zag paths from top to bottom of a rectangle of width 8, whose color is that of the top right corner. - _Joseph Myers_, Dec 23 2008"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A090992/b090992.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"B. Malesevic, \u003ca href=\"https://www.jstor.org/stable/43666958\"\u003eSome combinatorial aspects of differential operation composition on the space R^n\u003c/a\u003e, Univ. Beograd, Publ. Elektrotehn. Fak., Ser. Mat. 9 (1998), 29-33.",
				"Branko Malesevic, \u003ca href=\"http://arxiv.org/abs/0704.0750\"\u003eSome combinatorial aspects of differential operation compositions on space R^n\u003c/a\u003e, arXiv:0704.0750 [math.DG], 2007.",
				"B. Malesevic and I. Jovovic, \u003ca href=\"http://arXiv.org/abs/0706.0249\"\u003eThe Compositions of the Differential Operations and Gateaux DirectionalDerivative\u003c/a\u003e, arXiv:0706.0249 [math.CO], 2007.",
				"Joseph Myers, \u003ca href=\"http://www.polyomino.org.uk/publications/2008/bmo1-2009-q1.pdf\"\u003eBMO 2008--2009 Round 1 Problem 1---Generalisation\u003c/a\u003e",
				"\u003ca href=\"http://www.bmoc.maths.org/home/bmo1-2009.pdf\"\u003e2008/9 British Mathematical Olympiad Round 1: Thursday, 4 December 2008\u003c/a\u003e, Problem 1 [From _Joseph Myers_, Dec 23 2008]",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,3,-2,-1)."
			],
			"formula": [
				"a(n+4) = a(n+3) + 3*a(n+2) - 2*a(n+1) - a(n).",
				"G.f.: x*(7+6*x-10*x^2-4*x^3)/((1-x)*(1-3*x^2-x^3)). - _Colin Barker_, Mar 08 2012"
			],
			"maple": [
				"NUM := proc(k :: integer) local i,j,n,Fun,Identity,v,A; n := 7; # \u003c- DIMENSION Fun := (i,j)-\u003epiecewise(((j=i+1) or (i+j=n+1)),1,0); Identity := (i,j)-\u003epiecewise(i=j,1,0); v := matrix(1,n,1); A := piecewise(k\u003e1,(matrix(n,n,Fun))^(k-1),k=1,matrix(n,n,Identity)); return(evalm(v\u0026*A\u0026*transpose(v))[1,1]); end:"
			],
			"mathematica": [
				"LinearRecurrence[{1, 3, -2, -1}, {7, 13, 24, 45}, 32] (* _Jean-François Alcover_, Nov 25 2017 *)"
			],
			"program": [
				"(PARI) my(x='x+O('x^40)); Vec(x*(7+6*x-10*x^2-4*x^3)/((1-x)*(1-3*x^2-x^3))) \\\\ _G. C. Greubel_, Feb 02 2019",
				"(MAGMA) m:=40; R\u003cx\u003e:=PowerSeriesRing(Integers(), m); Coefficients(R!(  x*(7+6*x-10*x^2-4*x^3)/((1-x)*(1-3*x^2-x^3)) )); // _G. C. Greubel_, Feb 02 2019",
				"(Sage) a=(x*(7+6*x-10*x^2-4*x^3)/((1-x)*(1-3*x^2-x^3))).series(x, 40).coefficients(x, sparse=False); a[1:] # _G. C. Greubel_, Feb 02 2019",
				"(GAP) a:=[7,13,24,45];; for n in [5..40] do a[n]:=a[n-1]+3*a[n-2] - 2*a[n-3] - a[n-4]; od; a; # _G. C. Greubel_, Feb 02 2019"
			],
			"xref": [
				"Cf. A090989-A090995.",
				"Partial sums of pairwise sums of A065455.",
				"Cf. A000079, A007283, A020701, A020714, A129638."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_Branko Malesevic_, Feb 29 2004",
			"ext": [
				"More terms from Branko Malesevic and Ivana Jovovic (ivana121(AT)EUnet.yu), Jun 21 2007",
				"More terms from _Joseph Myers_, Dec 23 2008"
			],
			"references": 5,
			"revision": 30,
			"time": "2019-03-21T11:46:55-04:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}