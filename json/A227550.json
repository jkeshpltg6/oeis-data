{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A227550",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 227550,
			"data": "1,1,1,2,2,2,6,4,4,6,24,10,8,10,24,120,34,18,18,34,120,720,154,52,36,52,154,720,5040,874,206,88,88,206,874,5040,40320,5914,1080,294,176,294,1080,5914,40320,362880,46234,6994,1374,470,470,1374,6994,46234,362880,3628800",
			"name": "A triangle formed like Pascal's triangle, but with factorial(n) on the borders instead of 1.",
			"comment": [
				"A003422 gives the second column (after 0)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A227550/b227550.txt\"\u003eRows n = 0..70, flattened\u003c/a\u003e"
			],
			"formula": [
				"From _G. C. Greubel_, May 02 2021: (Start)",
				"T(n, k) = T(n-1, k-1) + T(n-1, k) with T(n, 0) = T(n, n) = n!.",
				"Sum_{k=0..n} T(n, k) = 2^n * (1 +Sum_{j=1..n-1} j*j!/2^j) = A140710(n). (End)"
			],
			"example": [
				"Triangle begins:",
				"       1;",
				"       1,     1;",
				"       2,     2,    2;",
				"       6,     4,    4,    6;",
				"      24,    10,    8,   10,  24;",
				"     120,    34,   18,   18,  34, 120;",
				"     720,   154,   52,   36,  52, 154,  720;",
				"    5040,   874,  206,   88,  88, 206,  874, 5040;",
				"   40320,  5914, 1080,  294, 176, 294, 1080, 5914, 40320;",
				"  362880, 46234, 6994, 1374, 470, 470, 1374, 6994, 46234, 362880;"
			],
			"mathematica": [
				"t = {}; Do[r = {}; Do[If[k == 0||k == n, m = n!, m = t[[n, k]] + t[[n, k + 1]]]; r = AppendTo[r, m], {k, 0, n}]; AppendTo[t, r], {n, 0, 10}]; t = Flatten[t]"
			],
			"program": [
				"(Haskell)",
				"a227550 n k = a227550_tabl !! n !! k",
				"a227550_row n = a227550_tabl !! n",
				"a227550_tabl = map fst $ iterate",
				"   (\\(vs, w:ws) -\u003e (zipWith (+) ([w] ++ vs) (vs ++ [w]), ws))",
				"   ([1], a001563_list)",
				"-- _Reinhard Zumkeller_, Aug 05 2013",
				"(Magma)",
				"function T(n,k)",
				"  if k eq 0 or k eq n then return Factorial(n);",
				"  else return T(n-1,k-1) + T(n-1,k);",
				"  end if; return T;",
				"end function;",
				"[T(n,k): k in [0..n], n in [0..12]]; // _G. C. Greubel_, May 02 2021",
				"(Sage)",
				"def T(n,k): return factorial(n) if (k==0 or k==n) else T(n-1, k-1) + T(n-1, k)",
				"flatten([[T(n,k) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, May 02 2021"
			],
			"xref": [
				"Cf. similar triangles with t on the borders: A007318 (t = 1), A028326 (t = 2), A051599 (t = prime(n)), A051601 (t = n), A051666 (t = n^2), A108617 (t = fibonacci(n)), A134636 (t = 2n+1), A137688 (t = 2^n), A227075 (t = 3^n).",
				"Cf. A003422.",
				"Cf. A227791 (central terms), A001563, A074911."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_Vincenzo Librandi_, Aug 04 2013",
			"references": 6,
			"revision": 24,
			"time": "2021-05-03T01:24:41-04:00",
			"created": "2013-08-04T05:28:19-04:00"
		}
	]
}