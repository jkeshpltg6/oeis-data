{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A087062",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 87062,
			"data": "1,1,1,1,2,1,1,2,2,1,1,2,3,2,1,1,2,3,3,2,1,1,2,3,4,3,2,1,1,2,3,4,4,3,2,1,1,2,3,4,5,4,3,2,1,10,2,3,4,5,5,4,3,2,10,11,10,3,4,5,6,5,4,3,10,11,11,11,10,4,5,6,6,5,4,10,11,11,11,12,11,10,5,6,7,6,5,10,11,12,11,11,12,12",
			"name": "Array T(n,k) = lunar product n*k (n \u003e= 1, k \u003e= 1) read by antidiagonals.",
			"comment": [
				"See A087061 for definition. Note that 0+x = x and 9*x = x for all x.",
				"This differs from A003983 at a(46): min(1,10)=1, while lunar product 10*1 = 10.",
				"We have now changed the name from \"dismal arithmetic\" to \"lunar arithmetic\" - the old name was too depressing. - _N. J. A. Sloane_, Aug 06 2014"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A087062/b087062.txt\"\u003eTable of n, a(n) for n = 1..10011\u003c/a\u003e",
				"D. Applegate, \u003ca href=\"/A087061/a087061.txt\"\u003eC program for lunar arithmetic and number theory\u003c/a\u003e",
				"D. Applegate, M. LeBrun and N. J. A. Sloane, \u003ca href=\"http://arxiv.org/abs/1107.1130\"\u003eDismal Arithmetic\u003c/a\u003e, arXiv:1107.1130 [math.NT], 2011.",
				"Brady Haran and N. J. A. Sloane, \u003ca href=\"https://youtu.be/cZkGeR9CWbk\"\u003ePrimes on the Moon (Lunar Arithmetic)\u003c/a\u003e, Numberphile video, Nov 2018.",
				"\u003ca href=\"/index/Di#dismal\"\u003eIndex entries for sequences related to dismal (or lunar) arithmetic\u003c/a\u003e"
			],
			"example": [
				"Lunar multiplication table begins:",
				"1 1 1 1 1 ...",
				"1 2 2 2 2 ...",
				"1 2 3 3 3 ...",
				"1 2 3 4 4 ...",
				"1 2 3 4 5 ..."
			],
			"maple": [
				"# convert decimal to string: rec := proc(n) local t0,t1,e,l; if n \u003c= 0 then RETURN([[0],1]); fi; t0 := n mod 10; t1 := (n-t0)/10; e := [t0]; l := 1; while t1 \u003c\u003e 0 do t0 := t1 mod 10; t1 := (t1-t0)/10; l := l+1; e := [op(e),t0]; od; RETURN([e,l]); end;",
				"# convert string to decimal: cer := proc(ep) local i,e,l,t1; e := ep[1]; l := ep[2]; t1 := 0; if l \u003c= 0 then RETURN(t1); fi; for i from 1 to l do t1 := t1+10^(i-1)*e[i]; od; RETURN(t1); end;",
				"# lunar addition: dadd := proc(m,n) local i,r1,r2,e1,e2,l1,l2,l,l3,t0; r1 := rec(m); r2 := rec(n); e1 := r1[1]; e2 := r2[1]; l1 := r1[2]; l2 := r2[2]; l := max(l1,l2); l3 := min(l1,l2); t0 := array(1..l); for i from 1 to l3 do t0[i] := max(e1[i],e2[i]); od; if l\u003el3 then for i from l3+1 to l do if l1\u003el2 then t0[i] := e1[i]; else t0[i] := e2[i]; fi; od; fi; cer([t0,l]); end;",
				"# lunar multiplication: dmul := proc(m,n) local k,i,j,r1,r2,e1,e2,l1,l2,l,t0; r1 := rec(m); r2 := rec(n); e1 := r1[1]; e2 := r2[1]; l1 := r1[2]; l2 := r2[2]; l := l1+l2-1; t0 := array(1..l); for i from 1 to l do t0[i] := 0; od; for i from 1 to l2 do for j from 1 to l1 do k := min(e2[i],e1[j]); t0[i+j-1] := max(t0[i+j-1],k); od; od; cer([t0,l]); end;"
			],
			"mathematica": [
				"ladd[x_, y_] := FromDigits[MapThread[Max, IntegerDigits[#, 10, Max@IntegerLength[{x, y}]] \u0026 /@ {x, y}]];",
				"lmult[x_, y_] := Fold[ladd, 0, Table[10^i, {i, IntegerLength[y] - 1, 0, -1}]*FromDigits /@ Transpose@Partition[Min[##] \u0026 @@@ Tuples[IntegerDigits[{x, y}]], IntegerLength[y]]];",
				"Flatten[Table[lmult[k, n - k + 1], {n, 1, 13}, {k, 1, n}]] (* _Davin Park_, Oct 06 2016 *)"
			],
			"program": [
				"(Python)",
				"def lunar_add(n,m):",
				"    sn, sm = str(n), str(m)",
				"    l = max(len(sn),len(sm))",
				"    return int(''.join(max(i,j) for i,j in zip(sn.rjust(l,'0'),sm.rjust(l,'0'))))",
				"def lunar_mul(n,m):",
				"    sn, sm, y = str(n), str(m), 0",
				"    for i in range(len(sm)):",
				"        c = sm[-i-1]",
				"        y = lunar_add(y,int(''.join(min(j,c) for j in sn))*10**i)",
				"    return y # _Chai Wah Wu_, Sep 06 2015",
				"(PARI) lmul=A087062(m,n,d(n)=Vecrev(digits(n)))={sum(i=1,#(n=d(n))-1+#m=d(m), vecmax(vector(min(i,#n),j,if(#m\u003ei-j,min(n[j],m[i-j+1]))))*10^i)\\10} \\\\ _M. F. Hasler_, Nov 13 2017"
			],
			"xref": [
				"Cf. A087061 (addition), A003983 (min), A087097 (lunar primes).",
				"See A261684 for a version that includes the zero row and column."
			],
			"keyword": "nonn,tabl,nice,base,look",
			"offset": "1,5",
			"author": "_Marc LeBrun_, Oct 09 2003",
			"ext": [
				"Maple programs from _N. J. A. Sloane_.",
				"Incorrect comment and Mathematica program removed by _David Applegate_, Jan 03 2012",
				"Edited by _M. F. Hasler_, Nov 13 2017"
			],
			"references": 30,
			"revision": 55,
			"time": "2018-11-15T10:52:59-05:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}