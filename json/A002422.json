{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002422",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2422,
			"id": "M4692 N2003",
			"data": "1,-10,30,-20,-10,-12,-20,-40,-90,-220,-572,-1560,-4420,-12920,-38760,-118864,-371450,-1179900,-3801900,-12406200,-40940460,-136468200,-459029400,-1556708400,-5318753700,-18296512728,-63334082520",
			"name": "Expansion of (1-4*x)^(5/2).",
			"reference": [
				"A. Fletcher, J. C. P. Miller, L. Rosenhead and L. J. Comrie, An Index of Mathematical Tables. Vols. 1 and 2, 2nd ed., Blackwell, Oxford and Addison-Wesley, Reading, MA, 1962, Vol. 1, p. 55.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence).",
				"T. N. Thiele, Interpolationsrechnung. Teubner, Leipzig, 1909, p. 164."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A002422/b002422.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e (terms 0..100 from Vincenzo Librandi)",
				"N. J. A. Sloane, \u003ca href=\"/A000984/a000984.pdf\"\u003eNotes on A984 and A2420-A2424\u003c/a\u003e"
			],
			"formula": [
				"a(n+3) = -2 * A007272(n).",
				"a(n) = Sum_{m=0..n} binomial(n, m) * K_m(6), where K_m(x) = K_m(n, 2, x) is a Krawtchouk polynomial. - Alexander Barg (abarg(AT)research.bell-labs.com).",
				"a(n) ~ -15/8*Pi^(-1/2)*n^(-7/2)*2^(2*n)*{1 + 35/8*n^-1 + ...}. - Joe Keane (jgk(AT)jgk.org), Nov 22 2001",
				"a(n) = -(15/8)*4^n*Gamma(n-5/2)/(sqrt(Pi)*Gamma(1+n)). - _Peter Luschny_, Dec 14 2015",
				"a(n) = (-4)^n*binomial(5/2, n). - _Peter Luschny_, Oct 22 2018",
				"D-finite with recurrence: n*a(n) +2*(-2*n+7)*a(n-1)=0. - _R. J. Mathar_, Jan 16 2020"
			],
			"maple": [
				"A002422 := n -\u003e -(15/8)*4^n*GAMMA(n-5/2)/(sqrt(Pi)*GAMMA(1+n)):",
				"seq(A002422(n), n=0..26); # _Peter Luschny_, Dec 14 2015"
			],
			"mathematica": [
				"CoefficientList[Series[(1-4x)^{5/2},{x,0,30}],x] (* _Vincenzo Librandi_, Jun 11 2012 *)"
			],
			"program": [
				"(PARI) vector(30, n, n--; (-4)^n*binomial(5/2, n)) \\\\ _G. C. Greubel_, Jul 03 2019",
				"(MAGMA) R\u003cx\u003e:=PowerSeriesRing(Rationals(), 30); Coefficients(R!( (1-4*x)^(5/2) )); // _G. C. Greubel_, Jul 03 2019",
				"(Sage) [(-4)^n*binomial(5/2, n) for n in (0..30)] # _G. C. Greubel_, Jul 03 2019"
			],
			"xref": [
				"Cf. A007054, A004001, A002420, A002421, A002423, A002424, A007272."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 7,
			"revision": 47,
			"time": "2020-01-30T21:29:14-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}