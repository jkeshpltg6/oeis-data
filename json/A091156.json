{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A091156",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 91156,
			"data": "1,1,1,1,1,4,1,11,2,1,26,15,1,57,69,5,1,120,252,56,1,247,804,364,14,1,502,2349,1800,210,1,1013,6455,7515,1770,42,1,2036,16962,27940,11055,792,1,4083,43086,95458,57035,8217,132,1,8178,106587,305812,257257",
			"name": "Triangle read by rows: T(n,k) is the number of Dyck paths of semilength n, having k long ascents (i.e., ascents of length at least 2). Rows are of length 1,1,2,2,3,3,... .",
			"comment": [
				"Also number of ordered trees with n edges, having k branch nodes (i.e., vertices of outdegree at least 2).",
				"Also number of Łukasiewicz paths of length n having k fall steps (1,-1) that start at an odd level. A Łukasiewicz path of length n is a path in the first quadrant from (0,0) to (n,0) using rise steps (1,k) for any positive integer k, level steps (1,0) and fall steps (1,-1) (see R. P. Stanley, Enumerative Combinatorics, Vol. 2, Cambridge Univ. Press, Cambridge, 1999, p. 223, Exercise 6.19w; the integers are the slopes of the steps). Example: T(4,2)=2 because we have U(D)U(D) and U(3)(D)D(D), where U=(1,1), D=(1,-1), U(3)=(1,3) and the fall steps that start at an odd level are shown between parentheses. Row n has 1+floor(n/2) terms. Row sums are the Catalan numbers (A000108). T(2n,n)=A000108(n). T(2n+1,n)=A001791(n+1)=binomial(2n+2,n). - _Emeric Deutsch_, Jan 06 2005",
				"Also number of Dyck paths of semilength n with k UUD's - I. Tasoulas (jtas(AT)unipi.gr), Feb 19 2006",
				"T(n,k) = number of Dyck n-paths whose decomposition into 2-step subpaths contains k UUs. For example, T(4,2)=2 counts UU|UU|DD|DD, UU|DD|UU|DD (vertical bars indicate path decomposition). - _David Callan_, Jun 07 2006",
				"T(n,k) = number of binary trees on n-1 edges containing k right edges whose child vertex has no right child. Under Knuth's \"natural\" correspondence, such a vertex in binary (n-1)-tree ~ a vertex of outdegree \u003e=2 in ordered n-tree. - _David Callan_, Sep 25 2006",
				"T(n,k) = number of binary trees on n-1 edges containing k left edges whose child vertex has no left child. Under \"natural\" correspondence, such a vertex in binary (n-1)-tree ~ a leaf edge with no left neighbor edge and not incident to the root in ordered n-tree ~ a UUD in Dyck n-path. - _David Callan_, Sep 25 2006",
				"T(n,k) = number of permutations of length n avoiding 321 (classically) with k descents. - _Andrew Baxter_, May 17 2011."
			],
			"reference": [
				"R. P. Stanley, Enumerative Combinatorics, Vol. 1, 1986; See Exercise 3.71(f)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A091156/b091156.txt\"\u003eRows n = 0..200, flattened\u003c/a\u003e",
				"M. Barnabei et al., \u003ca href=\"http://arxiv.org/abs/0910.0963\"\u003eThe descent statistic over 123-avoiding permutations\u003c/a\u003e, arXiv:0910.0963 [math.CO], 2009.",
				"A. M. Baxter, \u003ca href=\"https://pdfs.semanticscholar.org/2c5d/79e361d3aecb25c380402144177ad7cd9dc8.pdfindex.html\"\u003eAlgorithms for Permutation Statistics\u003c/a\u003e, Ph. D. Dissertation, Rutgers University, May 2011. See p. 88.",
				"Michael Bukata, Ryan Kulwicki, Nicholas Lewandowski, Lara Pudwell, Jacob Roth, Teresa Wheeland, \u003ca href=\"https://arxiv.org/abs/1812.07112\"\u003eDistributions of Statistics over Pattern-Avoiding Permutations\u003c/a\u003e, arXiv:1812.07112 [math.CO], 2018.",
				"Colin Defant, \u003ca href=\"https://arxiv.org/abs/1809.03123\"\u003eStack-Sorting Preimages of Permutation Classes\u003c/a\u003e, arXiv:1809.03123 [math.CO], 2018.",
				"Katie R. Gedeon, \u003ca href=\"https://arxiv.org/abs/1610.05349\"\u003eKazhdan-Lusztig polynomials of thagomizer matroids\u003c/a\u003e, arXiv:1610.05349 [math.CO], 2016.",
				"Y. Park, S. Park, \u003ca href=\"http://dx.doi.org/10.4134/JKMS.2013.50.3.529\"\u003eAvoiding permutations and the Narayana numbers\u003c/a\u003e, J. Korean Math. Soc. 50 (2013), No. 3, pp. 529-541.",
				"Lara Pudwell, \u003ca href=\"http://permutationpatterns.com/slides/Pudwell.pdf\"\u003eOn the distribution of peaks (and other statistics)\u003c/a\u003e, 16th International Conference on Permutation Patterns, Dartmouth College, 2018.",
				"A. Sapounakis, I. Tasoulas and P. Tsikouras, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2007.03.005\"\u003eCounting strings in Dyck paths\u003c/a\u003e, Discrete Math., 307 (2007), 2909-2924.",
				"Chao-Jen Wang, \u003ca href=\"http://people.brandeis.edu/~gessel/homepage/students/wangthesis.pdf\"\u003eApplications of the Goulden-Jackson cluster method to counting Dyck paths by occurrences of subwords\u003c/a\u003e.",
				"\u003ca href=\"/index/Lu#Lukasiewicz\"\u003eIndex entries for sequences related to Łukasiewicz\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = (1/(n+1)) * binomial(n+1, k) * Sum_{j=0..n-2k} binomial(k+j-1, k-1)*binomial(n+1-k, n-2k-j).",
				"G.f. G(t, z) satisfies z*(1-z+t*z)*G^2 - G + 1 = 0.",
				"T(n,k) = n!*(1+k)/((n-2*k)!*(1+k)!^2)*hypergeom([k,2*k-n], [k+2], -1). - _Peter Luschny_, Oct 16 2015",
				"T(n,k) = A055151(n,k)*hypergeom([k,2*k-n],[k+2],-1). - _Peter Luschny_, Oct 16 2015"
			],
			"example": [
				"T(4,1) = 11 because among the 14 Dyck paths of semilength 4, the paths that do not have exactly one long ascent are UDUDUDUD (no long ascent), UUDDUUDD (two long ascents) and UUDUUDDD (two long ascents). Here U=(1,1) and D=(1,-1).",
				"Triangle begins:",
				"  1;",
				"  1;",
				"  1,    1;",
				"  1,    4;",
				"  1,   11,    2;",
				"  1,   26,   15;",
				"  1,   57,   69,    5;",
				"  1,  120,  252,   56;",
				"  1,  247,  804,  364,   14;",
				"  1,  502, 2349, 1800,  210;",
				"  1, 1013, 6455, 7515, 1770, 42;",
				"  ..."
			],
			"maple": [
				"a := (n,k)-\u003ebinomial(n+1,k)* add(binomial(k+j-1,k-1)*binomial(n+1-k, n-2*k-j), j=0..n-2*k)/(n+1); seq(seq(a(n,k), k=0..floor(n/2)),n=0..15);",
				"seq(seq(simplify(n!*(1+k)/((n-2*k)!*(1+k)!^2)*hypergeom([k,2*k-n],[k+2],-1)),k=0.. floor(n/2)),n=0..15); # _Peter Luschny_, Oct 16 2015",
				"# alternative Maple program:",
				"b:= proc(x, y) option remember; `if`(y\u003ex or y\u003c0, 0,",
				"      `if`(x=0, 1, expand(b(x-1, y)*`if`(y=0, 1, 2)*z+",
				"       b(x-1, y+1) +b(x-1, y-1))))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, z, n-2*i), i=0..n/2))(b(n, 0)):",
				"seq(T(n), n=0..15);  # _Alois P. Heinz_, Aug 07 2018"
			],
			"mathematica": [
				"T[n_, k_] := Binomial[n+1, k]*Sum[Binomial[k+j-1, k-1]*Binomial[n+1-k, n- 2*k-j], {j, 0, n-2*k}]/(n+1); Table[T[n, k], {n, 0, 15}, {k, 0, Floor[n/2 ]}] // Flatten (* _Jean-François Alcover_, Jan 31 2016 *)"
			],
			"program": [
				"(PARI)",
				"tabf(nn) = {for(n=-1, nn, for(k=0, floor(n/2), if(binomial(n+1,k) * sum(j=0, n-2*k, binomial(k+j-1,k-1) * binomial(n+1-k,n-2*k-j))/(n+1)==0,print1(\"1, \"), print1(binomial(n+1,k) * sum(j=0, n-2*k, binomial(k+j-1,k-1) * binomial(n+1-k,n-2*k-j))/(n+1),\", \"));); print();); };",
				"tabf(16); \\\\ _Indranil Ghosh_, Mar 05 2017"
			],
			"xref": [
				"Cf. A000108, A001791, A243752.",
				"T(n,k) are rational multiples of A055151."
			],
			"keyword": "nonn,tabf",
			"offset": "0,6",
			"author": "_Emeric Deutsch_, Feb 22 2004",
			"ext": [
				"Edited by _Andrew Baxter_, May 17 2011"
			],
			"references": 5,
			"revision": 76,
			"time": "2021-09-08T08:56:43-04:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}