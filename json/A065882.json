{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A065882",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 65882,
			"data": "1,2,3,1,1,2,3,2,1,2,3,3,1,2,3,1,1,2,3,1,1,2,3,2,1,2,3,3,1,2,3,2,1,2,3,1,1,2,3,2,1,2,3,3,1,2,3,3,1,2,3,1,1,2,3,2,1,2,3,3,1,2,3,1,1,2,3,1,1,2,3,2,1,2,3,3,1,2,3,1,1,2,3,1,1,2,3,2,1,2,3,3,1,2,3,2,1,2,3,1,1,2,3,2,1",
			"name": "Ultimate modulo 4: right-hand nonzero digit of n when written in base 4.",
			"comment": [
				"From _Bradley Klee_, Sep 12 2015: (Start)",
				"In some guise, this sequence is a linear encoding of the three fixed-point half-hex tilings (cf. Baake \u0026 Grimm, Frettlöh). Applying a permutation, morphism x -\u003e 123x becomes x -\u003e x123, which has three fixed points. Applying a partition, morphism x -\u003e x123 becomes x -\u003e{{3,2},{x,1}} or",
				"            3 2 3 2",
				"            3 1 2 1",
				"     3 2    3 2 3 2",
				"x -\u003e x 1 -\u003e x 1 1 1 -\u003e etc.,",
				"which is the substitution rule for the half-hex tiling when the numbers 1,2,3 determine the direction of a dissecting diameter inscribed on each hexagon.",
				"(End)"
			],
			"reference": [
				"M. Baake and U. Grimm, Aperiodic Order Vol. 1, Cambridge University Press, 2013, page 205."
			],
			"link": [
				"Harry J. Smith, \u003ca href=\"/A065882/b065882.txt\"\u003eTable of n, a(n) for n=1..1000\u003c/a\u003e",
				"D. Frettlöh, \u003ca href=\"https://www.math.uni-bielefeld.de/~frettloe/papers/diss.pdf\"\u003eNichtperiodische Pflasterungen mit ganzzahligem Inflationsfaktor\u003c/a\u003e, Dissertation, Universität Dortmund, 2002.",
				"\u003ca href=\"/index/Fi#final\"\u003eIndex entries for sequences related to final digits of numbers\u003c/a\u003e"
			],
			"formula": [
				"If n mod 4 = 0 then a(n) = a(n/4), otherwise a(n) = n mod 4. a(n) = A065883(n) mod 4.",
				"Fixed point of the morphism: 1 -\u003e1231, 2 -\u003e1232, 3 -\u003e1233, starting from a(1) = 1. Sequence read mod 2 gives A035263. a(n) = A007913(n) mod 4. - _Philippe Deléham_, Mar 28 2004",
				"G.f. g(x) satisfies g(x) = g(x^4) + (x + 2 x^2 + 3 x^3)/(1 - x^4). - _Bradley Klee_, Sep 12 2015"
			],
			"example": [
				"a(7)=3 and a(112)=3, since 7 is written in base 4 as 13 and 112 as 1300."
			],
			"maple": [
				"f:= proc(n)",
				"local x:=n;",
				"   while x mod 4 = 0 do x:= x/4 od:",
				"   x mod 4;",
				"end proc;",
				"map(f, [$1..100]); # _Robert Israel_, Jan 05 2016"
			],
			"mathematica": [
				"Nest[ Flatten[ # /. {1 -\u003e {1, 2, 3, 1}, 2 -\u003e {1, 2, 3, 2}, 3 -\u003e {1, 2, 3, 3}}] \u0026, {1}, 4] (* _Robert G. Wilson v_, May 07 2005 *)",
				"b[n_] := CoefficientList[Series[",
				"    With[{f0 = (x + 2 x^2 + 3 x^3)/(1 - x^4)},",
				"     Nest[ (# /. x -\u003e x^4) + f0 \u0026, f0, Ceiling[Log[4, n/3]]]],",
				"{x, 0, n}], x][[2 ;; -1]]; b[100](* _Bradley Klee_, Sep 12 2015 *)",
				"Table[Mod[n/4^IntegerExponent[n, 4], 4], {n, 1, 120}] (* _Clark Kimberling_, Oct 19 2016 *)"
			],
			"program": [
				"(PARI) baseE(x, b)= { local(d, e=0, f=1); while (x\u003e0, d=x%b; x\\=b; e+=d*f; f*=10); return(e) } { for (n=1, 1000, a=baseE(n, 4); while (a%10 == 0, a\\=10); write(\"b065882.txt\", n, \" \", a%10) ) } \\\\ _Harry J. Smith_, Nov 03 2009",
				"(PARI) a(n) = (n/4^valuation(n,4))%4; \\\\ _Joerg Arndt_, Sep 13 2015"
			],
			"xref": [
				"In base 2 this is A000012, base 3 A060236 and base 10 A065881.",
				"Defining relations for g.f. similar to A014577.",
				"Cf. A010873, A037898, A065883, A190593."
			],
			"keyword": "base,nonn",
			"offset": "1,2",
			"author": "_Henry Bottomley_, Nov 26 2001",
			"references": 8,
			"revision": 45,
			"time": "2016-10-19T10:51:12-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}