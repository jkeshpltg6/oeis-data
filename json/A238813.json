{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A238813",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 238813,
			"data": "1,-1,1,-1,1,-191,29,-2833,140051,-6525613,38899057,-532493977,4732769,-12945933911,168070910246641,-4176262284636781,345687837634435,-26305470121572878741,1747464708706073081,-2811598717039332137041,166748874686794522517053",
			"name": "Numerators of the coefficients of Euler-Ramanujan’s harmonic number expansion into negative powers of a triangular number.",
			"comment": [
				"H_k = Sum_{i=1..k} 1/i = log(2*m)/2 + gamma + Sum_{n\u003e=1} R_n/m^n, where m = k(k+1)/2 is the k-th triangular number. This sequence lists the numerators of R_n (denominators are listed in A093334)."
			],
			"link": [
				"Stanislav Sykora, \u003ca href=\"/A238813/b238813.txt\"\u003eTable of n, a(n) for n = 1..296\u003c/a\u003e",
				"Chao-Ping Chen, \u003ca href=\"http://dx.doi.org/10.1007/s11139-015-9670-3\"\u003eOn the coefficients of asymptotic expansion for the harmonic number by Ramanujan\u003c/a\u003e, The Ramanujan Journal, (2016) 40: 279.",
				"Feng, L. and Wang, W., \u003ca href=\"http://dx.doi.org/10.1007/s00025-016-0597-9\"\u003eRiordan Array Approach to the Coefficients of Ramanujan's Harmonic Number Expansion\u003c/a\u003e, Results Math (2017) 71: 1413.",
				"M. B. Villarino, \u003ca href=\"https://www.emis.de/journals/JIPAM/article1026.html?sid=1026\"\u003eRamanujan’s Harmonic Number Expansion into Negative Powers of a Triangular Number\u003c/a\u003e, Journal of Inequalities in Pure and Applied Mathematics, Volume 9, Issue 3, Article 89 (also arXiv:0707.3950v2 [math.CA] 28 Jul 2007)."
			],
			"formula": [
				"R(n) = (-1)^(n-1)/(2*n*8^n)*(1 + Sum_{i=1..n} (-4)^i*binomial(n,i)* B_2i(1/2)), a(n) = denominator(R_n), and B_2i(x) is the (2i)-th Bernoulli polynomial.",
				"From _Peter Luschny_, Aug 13 2017: (Start)",
				"a(n) = -numerator(A212196(n)/2^n), A212196 the Bernoulli median numbers.",
				"a(n) = -numerator((Sum_{k=0..n} binomial(n,k)*bernoulli(n+k))/2^n).",
				"a(n) = -numerator(I(n)/2^n) with I(n) = (-1)^n*Integral_{x=0..1} S(n,x)^2 and S(n,x) = Sum_{k=0..n} Stirling2(n,k)*k!*(-x)^k. (End)"
			],
			"example": [
				"R_9 = 140051/17459442 = a(9)/A093334(9)."
			],
			"maple": [
				"a := n -\u003e - numer(add(binomial(n,k)*bernoulli(n+k), k=0..n)/2^n);",
				"seq(a(n), n=1..21); # _Peter Luschny_, Aug 13 2017"
			],
			"mathematica": [
				"Table[Numerator[-Sum[Binomial[n,k]*BernoulliB[n+k]/2^n,{k,0,n}]], {n,1,25}] (* _G. C. Greubel_, Aug 30 2018 *)"
			],
			"program": [
				"(PARI) Rn(nmax)= {local(n,k,v,R);v=vector(nmax);x=1/2;",
				"for(n=1,nmax,R=1;for(k=1,n,R+=(-4)^k*binomial(n,k)*eval(bernpol(2*k)));",
				"R*=(-1)^(n-1)/(2*n*8^n);v[n]=R);return (v);}",
				"// returns an array v[1..nmax] of the rational coefficients"
			],
			"xref": [
				"Cf. A000217 (triangular numbers), A001620 (gamma), A093334 (denominators).",
				"Cf. A212196."
			],
			"keyword": "sign,frac",
			"offset": "1,6",
			"author": "_Stanislav Sykora_, Mar 05 2014",
			"references": 5,
			"revision": 22,
			"time": "2018-08-30T15:56:23-04:00",
			"created": "2014-03-06T18:43:22-05:00"
		}
	]
}