{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A284256",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 284256,
			"data": "0,0,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0,1,0,1,0,0,0,1,0,1,0,1,0,0,1,1,0,0,0,1,1,1,0,1,0,1,0,1,0,0,0,2,1,1,0,0,0,1,1,1,0,1,0,1,0,0,0,1,0,1,1,2,0,0,0,1,0,1,0,1,0,1,0,1,0,1,0,1,1,1,0,1,0,1,1,1,0,0,0,2,1,2,0,1,0,1,0,1,0,0,0,2,1,1,0,1,0,1,1,1,0,1",
			"name": "a(n) = number of prime factors of n that are \u003e the square of smallest prime factor of n (counted with multiplicity), a(1) = 0.",
			"link": [
				"Antti Karttunen, \u003ca href=\"/A284256/b284256.txt\"\u003eTable of n, a(n) for n = 1..10001\u003c/a\u003e"
			],
			"formula": [
				"If A284252(n) = 1, a(n) = 0, otherwise a(n) = 1 + a(A284253(n)).",
				"a(n) = A001222(A284254(n)).",
				"a(n) = A001222(n) - A284257(n)."
			],
			"example": [
				"For n = 10 = 2*5, there is a single prime factor 5 that is \u003e 2^2, thus a(10) = 1.",
				"For n = 15 = 3*5, there are no prime factors larger than 3^2, thus a(15) = 0.",
				"For n = 50 = 2*5*5, the prime factors larger than 2^2 are 5*5, thus a(50) = 2."
			],
			"mathematica": [
				"Table[If[n == 1, 0, Count[#, d_ /; d \u003e First[#]^2] \u0026@ Flatten@ Map[ConstantArray[#1, #2] \u0026 @@ # \u0026, FactorInteger@ n]], {n, 120}] (* _Michael De Vlieger_, Mar 24 2017 *)"
			],
			"program": [
				"(Scheme, with memoization-macro definec)",
				"(definec (A284256 n) (if (= 1 (A284252 n)) 0 (+ 1 (A284256 (A284253 n)))))",
				"(PARI) A(n) = if(n\u003c2, return(1), my(f=factor(n)[, 1]); for(i=2, #f, if(f[i]\u003ef[1]^2, return(f[i]))); return(1));",
				"a(n) = if(A(n)==1, 0, 1 + a(n/A(n)));",
				"for(n=1, 150, print1(a(n),\", \")) \\\\ _Indranil Ghosh_, after _David A. Corneth_, Mar 24 2017",
				"(Python)",
				"from sympy import primefactors",
				"def A(n):",
				"    pf = primefactors(n)",
				"    if pf: min_pf2 = min(pf)**2",
				"    for i in pf:",
				"        if i \u003e min_pf2: return i",
				"    return 1",
				"def a(n): return 0 if A(n)==1 else 1 + a(n//A(n))",
				"print([a(n) for n in range(1, 151)]) # _Indranil Ghosh_, Mar 24 2017"
			],
			"xref": [
				"Cf. A001222, A020639, A284252, A284253, A284254, A284257, A284258, A284259, A284260.",
				"Cf. A251726 (gives the positions of zeros after the initial a(1)=0)."
			],
			"keyword": "nonn",
			"offset": "1,50",
			"author": "_Antti Karttunen_, Mar 24 2017",
			"references": 9,
			"revision": 26,
			"time": "2021-03-28T07:01:38-04:00",
			"created": "2017-03-24T21:58:53-04:00"
		}
	]
}