{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A306234",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 306234,
			"data": "1,1,1,1,1,3,4,3,1,1,5,13,15,13,5,1,1,7,28,67,76,67,28,7,1,1,9,49,179,411,455,411,179,49,9,1,1,11,76,375,1306,2921,3186,2921,1306,375,76,11,1,1,13,109,679,3181,10757,23633,25487,23633,10757,3181,679,109,13,1",
			"name": "Number T(n,k) of occurrences of k in a (signed) displacement set of a permutation of [n] divided by |k|!; triangle T(n,k), n\u003e=1, 1-n\u003c=k\u003c=n-1, read by rows.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A306234/b306234.txt\"\u003eRows n = 1..142, flattened\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Permutation\"\u003ePermutation\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = T(n,-k).",
				"T(n,k) = -1/|k|! * Sum_{j=1..n} (-1)^j * binomial(n-|k|,j) * (n-j)!.",
				"T(n,k) = (n-|k|)! [x^(n-|k|)] (1-exp(-x))/(1-x)^(|k|+1).",
				"T(n+1,n) = 1.",
				"T(n,k) = A306461(n,k) / |k|!.",
				"Sum_{k=1-n..n-1} |k|! * T(n,k) = A306455(n)."
			],
			"example": [
				"Triangle T(n,k) begins:",
				"  :                                 1                              ;",
				"  :                           1,    1,    1                        ;",
				"  :                     1,    3,    4,    3,    1                  ;",
				"  :               1,    5,   13,   15,   13,    5,   1             ;",
				"  :          1,   7,   28,   67,   76,   67,   28,   7,  1         ;",
				"  :      1,  9,  49,  179,  411,  455,  411,  179,  49,  9,  1     ;",
				"  :  1, 11, 76, 375, 1306, 2921, 3186, 2921, 1306, 375, 76, 11, 1  ;"
			],
			"maple": [
				"b:= proc(s, d) option remember; (n-\u003e `if`(n=0, add(x^j, j=d),",
				"      add(b(s minus {i}, d union {n-i}), i=s)))(nops(s))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i)/abs(i)!, i=1-n..n-1))(b({$1..n}, {})):",
				"seq(T(n), n=1..8);",
				"# second Maple program:",
				"T:= (n, k)-\u003e -add((-1)^j*binomial(n-abs(k), j)*(n-j)!, j=1..n)/abs(k)!:",
				"seq(seq(T(n, k), k=1-n..n-1), n=1..9);"
			],
			"mathematica": [
				"T[n_, k_] := (-1/Abs[k]!) Sum[(-1)^j Binomial[n-Abs[k], j] (n-j)!, {j, 1, n}];",
				"Table[T[n, k], {n, 1, 9}, {k, 1-n, n-1}] // Flatten (* _Jean-François Alcover_, Feb 15 2021 *)"
			],
			"xref": [
				"Columns k=0-10 give (offsets may differ): A002467, A180191, A324352, A324353, A324354, A324355, A324356, A324357, A324358, A324359, A324360.",
				"Row sums give A306525.",
				"T(n+1,n) gives A000012.",
				"T(n+2,n) gives A005408.",
				"T(n+2,n-1) gives A056107.",
				"T(2n,n) gives A324361.",
				"Cf. A000142, A306455, A306461, A324224, A324362."
			],
			"keyword": "nonn,tabf",
			"offset": "1,6",
			"author": "_Alois P. Heinz_, Feb 17 2019",
			"references": 18,
			"revision": 58,
			"time": "2021-02-15T04:41:39-05:00",
			"created": "2019-02-18T09:26:27-05:00"
		}
	]
}