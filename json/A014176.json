{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A014176",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 14176,
			"data": "2,4,1,4,2,1,3,5,6,2,3,7,3,0,9,5,0,4,8,8,0,1,6,8,8,7,2,4,2,0,9,6,9,8,0,7,8,5,6,9,6,7,1,8,7,5,3,7,6,9,4,8,0,7,3,1,7,6,6,7,9,7,3,7,9,9,0,7,3,2,4,7,8,4,6,2,1,0,7,0,3,8,8,5,0,3,8,7,5,3,4,3,2,7,6,4,1,5,7",
			"name": "Decimal expansion of the silver mean, 1+sqrt(2).",
			"comment": [
				"From _Hieronymus Fischer_, Jan 02 2009: (Start)",
				"Set c:=1+sqrt(2). Then the fractional part of c^n equals 1/c^n, if n odd. For even n, the fractional part of c^n is equal to 1-(1/c^n).",
				"c:=1+sqrt(2) satisfies c-c^(-1)=floor(c)=2, hence c^n + (-c)^(-n) = round(c^n) for n\u003e0, which follows from the general formula of A001622.",
				"1/c = sqrt(2)-1.",
				"See A001622 for a general formula concerning the fractional parts of powers of numbers x\u003e1, which satisfy x-x^(-1)=floor(x).",
				"Other examples of constants x satisfying the relation x-x^(-1)=floor(x) include A001622 (the golden ratio: where floor(x)=1) and A098316 (the \"bronze\" ratio: where floor(x)=3). (End)",
				"In terms of continued fractions the constant c can be described by c=[2;2,2,2,...]. - _Hieronymus Fischer_, Oct 20 2010",
				"Side length of smallest square containing five circles of diameter 1. - _Charles R Greathouse IV_, Apr 05, 2011",
				"Largest radius of four circles tangent to a circle of radius 1. - _Charles R Greathouse IV_, Jan 14 2013",
				"An analog of Fermat theorem: for prime p, round(c^p) == 2 (mod p). - _Vladimir Shevelev_, Mar 02 2013",
				"n*(1+sqrt(2)) is the perimeter of a 45-45-90 triangle with hypotenuse n. - _Wesley Ivan Hurt_, Apr 09 2016",
				"This algebraic integer of degree 2, with minimal polynomial x^2 - 2*x - 1, is also the length ratio diagonal/side of the second largest diagonal in the regular octagon (not counting the side). The other two diagonal/side ratios are A179260 and A121601. - _Wolfdieter Lang_, Oct 28 2020"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A014176/b014176.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Nicholas R. Beaton, Mireille Bousquet-Mélou, Jan de Gier, Hugo Duminil-Copin, and Anthony J. Guttmann, \u003ca href=\"https://arxiv.org/abs/1109.0358\"\u003eThe critical fugacity for surface adsorption of self-avoiding walks on the honeycomb lattice is 1+sqrt(2)\u003c/a\u003e, arXiv:1109.0358 [math-ph], 2011-2013.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/SilverRatio.html\"\u003eSilver Ratio\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Exact_trigonometric_constants\"\u003eExact trigonometric constants\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Metallic_mean\"\u003eMetallic mean\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/silver_ratio\"\u003eSilver ratio\u003c/a\u003e"
			],
			"formula": [
				"1+sqrt(2) = lim A179807(n+1)/A179807(n) as n -\u003e infinity. (conjecture)",
				"Equals cot(Pi/8) = tan(Pi*3/8). - _Bruno Berselli_, Dec 13 2012, and _M. F. Hasler_, Jul 08 2016",
				"Silver mean = 2 + Sum_{n\u003e=0} (-1)^n/(P(n-1)*P(n)), where P(n) is the n-th Pell number (A000129). - _Vladimir Shevelev_, Feb 22 2013",
				"Equals exp(arcsinh(1)) which is exp(A091648). - _Stanislav Sykora_, Nov 01 2013",
				"exp(asinh(cos(Pi/n))) -\u003e sqrt(2)+1 as n -\u003e infinity. - _Geoffrey Caveney_, Apr 23 2014",
				"exp(asinh(cos(Pi/2 - log(sqrt(2)+1)*i))) = exp(asinh(sin(log(sqrt(2)+1)*i))) = i. - _Geoffrey Caveney_, Apr 23 2014",
				"Equals Product_{k\u003e=1} A047621(k) / A047522(k) = (3/1) * (5/7) * (11/9) * (13/15) * (19/17) * (21/23) * ... . - _Dimitris Valianatos_, Mar 27 2019"
			],
			"example": [
				"2.414213562373095..."
			],
			"maple": [
				"Digits:=100: evalf(1+sqrt(2)); # _Wesley Ivan Hurt_, Apr 09 2016"
			],
			"mathematica": [
				"RealDigits[1 + Sqrt@ 2, 10, 111] (* Or *)",
				"RealDigits[Exp@ ArcSinh@ 1, 10, 111][[1]] (* _Robert G. Wilson v_, Aug 17 2011 *)",
				"Circs[n_] := With[{r = Sin[Pi/n]/(1 - Sin[Pi/n])}, Graphics[Append[",
				"  Table[Circle[(r + 1) {Sin[2 Pi k/n], Cos[2 Pi k/n]}, r], {k, n}],   {Blue, Circle[{0, 0}, 1]}]]] Circs[4] (* _Charles R Greathouse IV_, Jan 14 2013 *)"
			],
			"program": [
				"(PARI) 1+sqrt(2) \\\\ _Charles R Greathouse IV_, Jan 14 2013"
			],
			"xref": [
				"Cf. A002193, A000032, A006497, A080039, A179260, A121601.",
				"See A098316 for [3;3,3,...]; A098317 for [4;4,4,...]; A098318 for [5;5,5,...]. - _Hieronymus Fischer_, Oct 20 2010"
			],
			"keyword": "nonn,cons,easy",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_",
			"references": 47,
			"revision": 98,
			"time": "2020-11-01T01:48:12-05:00",
			"created": "1998-06-14T03:00:00-04:00"
		}
	]
}