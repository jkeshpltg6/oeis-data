{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A080930",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 80930,
			"data": "1,5,20,70,224,672,1920,5280,14080,36608,93184,232960,573440,1392640,3342336,7938048,18677760,43581440,100925440,232128512,530579456,1205862400,2726297600,6134169600,13740539904,30651973632,68115496960",
			"name": "a(n) = 2^(n-3)*(n+2)*(n+3)*(n+4)/3.",
			"comment": [
				"Old definition was \"Sequence associated with recurrence a(n)=2*a(n-1)+k(k+2)*a(n-2)\". See the first comment in A080928.",
				"The fourth column of triangle A080928 (after 0) is 4*a(n)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A080930/b080930.txt\"\u003eTable of n, a(n) for n = 0..300\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (8,-24,32,-16)."
			],
			"formula": [
				"G.f.: (1-x)*(1-2*x+2*x^2)/(1-2*x)^4 = (1-3*x+4*x^2-2*x^3)/(1-2*x)^4.",
				"a(n) = binomial(n+3,3)*2^(n-3), n\u003e0. - _Zerinvary Lajos_, Oct 29 2006",
				"a(n) = 8*a(n-1) - 24*a(n-2) + 32*a(n-3) - 16*a(n-4) for n\u003e3, a(0)=1, a(1)=5, a(2)=20, a(3)=70. - _Bruno Berselli_, Aug 06 2013",
				"E.g.f.: (3 +9*x +6*x^2 +x^3)*exp(2*x)/3. - _G. C. Greubel_, Aug 27 2019",
				"From _Amiram Eldar_, Jan 07 2022: (Start)",
				"Sum_{n\u003e=0} 1/a(n) = 48*log(2) - 32.",
				"Sum_{n\u003e=0} (-1)^n/a(n) = 176 - 432*log(3/2). (End)"
			],
			"maple": [
				"[seq (binomial(n+3,3)*2^(n-3),n=1..27)]; # _Zerinvary Lajos_, Oct 29 2006"
			],
			"mathematica": [
				"CoefficientList[Series[(1-x)(1 -2x +2x^2)/(1-2x)^4, {x, 0, 30}], x] (* _Vincenzo Librandi_, Aug 06 2013 *)",
				"LinearRecurrence[{8, -24, 32, -16}, {1, 5, 20, 70}, 30] (* _Bruno Berselli_, Aug 06 2013 *)"
			],
			"program": [
				"(MAGMA) [Binomial(n+3,3)*2^(n-3): n in [1..30]]; // _Vincenzo Librandi_, Aug 06 2013",
				"(PARI) a(n)=2^(n-3)*(n+2)*(n+3)*(n+4)/3 \\\\ _Charles R Greathouse IV_, Oct 07 2015",
				"(Sage) [2^(n-2)*binomial(n+4,3) for n in (0..30)] # _G. C. Greubel_, Aug 27 2019",
				"(GAP) List([0..30], n-\u003e 2^(n-2)*Binomial(n+4,3)); # _G. C. Greubel_, Aug 27 2019"
			],
			"xref": [
				"Cf. A080928."
			],
			"keyword": "nonn,easy,changed",
			"offset": "0,2",
			"author": "_Paul Barry_, Feb 26 2003",
			"ext": [
				"Edited by _Bruno Berselli_, Aug 06 2013"
			],
			"references": 6,
			"revision": 26,
			"time": "2022-01-07T06:15:57-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}