{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A094642",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 94642,
			"data": "4,5,1,5,8,2,7,0,5,2,8,9,4,5,4,8,6,4,7,2,6,1,9,5,2,2,9,8,9,4,8,8,2,1,4,3,5,7,1,7,9,4,6,7,8,5,5,5,0,5,6,3,1,7,3,9,2,9,4,3,0,6,1,9,7,8,7,4,4,1,4,7,9,1,5,1,3,1,3,6,4,1,7,7,7,5,9,9,4,3,2,7,9,0,7,1,0,2,0,1,6,0,0,0,8",
			"name": "Decimal expansion of log(Pi/2).",
			"reference": [
				"George Boros and Victor Moll, Irresistible Integrals: Symbolics, Analysis and Experiments in the Evaluation of Integrals, Cambridge University Press, Cambridge, 2004, Chap. 7.",
				"Jonathan Borwein and Peter Borwein, Pi and the AGM, John Wiley \u0026 Sons, New York, 1987, Chap. 11."
			],
			"link": [
				"Dirk Huylebrouck, \u003ca href=\"https://www.maa.org/sites/default/files/pdf/upload_library/22/Ford/Huylebrouck222-231.pdf\"\u003eSimilarities in irrationality proofs for Pi, ln2, zeta(2) and zeta(3)\u003c/a\u003e, Amer. Math. Monthly, Vol. 108, No. 3 (2001), pp. 222-231.",
				"Jonathan Sondow, \u003ca href=\"https://www.jstor.org/stable/30037575\"\u003eA faster product for pi and a new integral for ln(pi/2)\u003c/a\u003e, The American Mathematical Monthly, Vol. 112, No. 8 (2005), pp. 729-734; \u003ca href=\"http://www.jstor.org/stable/27642026\"\u003eEditor's endnotes\u003c/a\u003e, ibid., Vol. 113, No. 7 (2006), pp. 670-671; \u003ca href=\"https://arxiv.org/abs/math/0401406\"\u003earXiv preprint\u003c/a\u003e, arXiv:math/0401406 [math.NT], 2004."
			],
			"formula": [
				"Equals Sum_{n\u003e=1} zeta(2*n)/(n*2^(2*n)) (cf. Boros \u0026 Moll p. 131). - _Jean-François Alcover_, Apr 29 2013",
				"Equals Re(log(log(I))). - _Stanislav Sykora_, May 09 2015",
				"Equals Integral_{-oo..+oo} -log(1/2 + i*z)/cosh(Pi*z) dz, where i is the imaginary unit. - _Peter Luschny_, Apr 08 2018",
				"Equals Integral_{0..Pi/2} (2/(Pi-2*t)-tan(t)) dt. - _Clark Kimberling_, Jul 10 2020",
				"Equals -Sum_{k\u003e=1} log(1 - 1/(2*k)^2). - _Amiram Eldar_, Aug 12 2020",
				"Equals Sum_{k\u003e=1} (-1)^(k+1) * log(1 + 1/k). - _Amiram Eldar_, Jun 26 2021"
			],
			"example": [
				"log(Pi/2) = 0.45158270528945486472619522989488214357179467855505..."
			],
			"mathematica": [
				"RealDigits[ Log[Pi/2], 10, 111][[1]]"
			],
			"program": [
				"(PARI) log(Pi/2) \\\\ _Charles R Greathouse IV_, Jun 23 2014"
			],
			"xref": [
				"Cf. A019669, A094643."
			],
			"keyword": "cons,easy,nonn",
			"offset": "0,1",
			"author": "_Jonathan Sondow_ and _Robert G. Wilson v_, May 18 2004",
			"references": 8,
			"revision": 47,
			"time": "2021-06-26T08:59:39-04:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}