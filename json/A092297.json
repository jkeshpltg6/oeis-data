{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A092297",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 92297,
			"data": "0,6,6,18,30,66,126,258,510,1026,2046,4098,8190,16386,32766,65538,131070,262146,524286,1048578,2097150,4194306,8388606,16777218,33554430,67108866,134217726,268435458,536870910,1073741826,2147483646",
			"name": "Number of ways of 3-coloring an annulus consisting of n zones joined like a pearl necklace.",
			"comment": [
				"A circular domain means a domain between two concentric circles and it is divided into n parts by n boundary lines perpendicular to the circles. Both sides of a line must have different colors. How many ways of coloring are there?",
				"a(n) is also the multiple of six that's nearest to 2^n. - _David Eppstein_, Aug 31 2010",
				"a(n) apparently is the trace of the n-th power of the adjacency matrix of the complete 3-graph, a 3 X 3 matrix with diagonal elements all zero and off-diagonal all ones (cf. A001045). If so, a(n) is the number of closed walks on the graph of length n. - _Tom Copeland_, Nov 06 2012",
				"For n \u003e= 2, a(n) is the number of length n words on 3 letters with no two consecutive like letters including the first and the last. Cf. A218034. - _Geoffrey Critzer_, Apr 05 2014"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A092297/b092297.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"K. Böhmová, C. Dalfó, C. Huemer, \u003ca href=\"http://upcommons.upc.edu/bitstream/handle/2117/80848/Kautz-subdigraphs.pdf\"\u003eOn cyclic Kautz digraphs\u003c/a\u003e, Preprint 2016.",
				"Cristina Dalfó, \u003ca href=\"https://arxiv.org/abs/1709.01882\"\u003eFrom subKautz digraphs to cyclic Kautz digraphs\u003c/a\u003e, arXiv:1709.01882 [math.CO], 2017.",
				"C. Dalfó, \u003ca href=\"https://dx.doi.org/10.1016/j.laa.2017.05.046\"\u003eThe spectra of subKautz and cyclic Kautz digraphs\u003c/a\u003e, Linear Algebra and its Applications, 531 (2017), p. 210-219.",
				"P. P. Martin, S. F. Zakaria, \u003ca href=\"https://doi.org/10.1088/1742-5468/ab2905\"\u003eZeros of the 4-state Potts model partition function for the square lattice revisited\u003c/a\u003e, J. Stat. Mech. 084003 (2019). eq. (7).",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,2)."
			],
			"formula": [
				"a(n) = 2^n + 2*(-1)^n; recurrence a(1)=0, a(2)=6, a(n) = 2*a(n-2) + a(n-1).",
				"O.g.f: -6*x^2/((1+x)*(2*x-1)) = -3 - 1/(2*x-1) + 2/(1+x). - _R. J. Mathar_, Dec 02 2007",
				"a(n) = 6*A001045(n-1). - _R. J. Mathar_, Aug 30 2008",
				"a(n) = (-1)^n * a(2-n) * 2^(n-1) for all n in Z. - _Michael Somos_, Oct 25 2014"
			],
			"example": [
				"a(2)=6 because we can color one zone in 3 colors and the other in 2, so 2*3=6 in all."
			],
			"mathematica": [
				"nn=28;Drop[CoefficientList[Series[6x^2/(1+x)^2/(1-3x/(1+x)),{x,0,nn}],x],1] (* _Geoffrey Critzer_, Apr 05 2014 *)",
				"a[ n_] := 2 (2^(n - 1) + (-1)^n); (* _Michael Somos_, Oct 25 2014 *)",
				"a[ n_] := If[ n \u003c 1, -(-2)^(n - 1) a[2 - n] , (-1)^n HypergeometricPFQ[ Table[ -2, {k, n}], Table[ 1, {k, n - 1}], 1]] (* _Michael Somos_, Oct 25 2014 *)"
			],
			"program": [
				"(MAGMA) [2^n+2*(-1)^n : n in [1..40]]; // _Vincenzo Librandi_, Sep 27 2011",
				"(PARI) {a(n) = 2 * (2^(n-1) - (-1)^n)}; /* _Michael Somos_, Oct 25 2014 */"
			],
			"xref": [
				"Column k=3 of A106512.",
				"Cf. A001045."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "S. B. Step (stepy(AT)vesta.ocn.ne.jp), Feb 06 2004",
			"references": 11,
			"revision": 60,
			"time": "2019-11-19T04:22:17-05:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}