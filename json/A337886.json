{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A337886",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 337886,
			"data": "1,2,1,3,5,1,4,15,28,1,5,34,387,768,1,6,65,2784,202203,302032,1,7,111,13125,11230976,7109211078,3098988832,1,8,175,46836,254729375,9393953524224,50669807706182691,1831011525739328,1",
			"name": "Array read by descending antidiagonals: T(n,k) is the number of achiral colorings of the triangular faces of a regular n-dimensional simplex using k or fewer colors.",
			"comment": [
				"An achiral arrangement is identical to its reflection. An n-simplex has n+1 vertices. For n=2, the figure is a triangle with one triangular face. For n=3, the figure is a tetrahedron with 4 triangular faces. For higher n, the number of triangular faces is C(n+1,3).",
				"Also the number of achiral colorings of the peaks of a regular n-dimensional simplex. A peak of an n-simplex is an (n-3)-dimensional simplex."
			],
			"link": [
				"E. M. Palmer and R. W. Robinson, \u003ca href=\"https://doi.org/10.1007/BF02392038\"\u003eEnumeration under two representations of the wreath product\u003c/a\u003e, Acta Math., 131 (1973), 123-143."
			],
			"formula": [
				"The algorithm used in the Mathematica program below assigns each permutation of the vertices to a partition of n+1. It then determines the number of permutations for each partition and the cycle index for each partition using a formula for binary Lyndon words. If the value of m is increased, one can enumerate colorings of higher-dimensional elements beginning with T(m,1).",
				"T(n,k) = A337884(n,k) - A337883(n,k) = A337883(n,k) - 2*A337885(n,k) = A337884(n,k) - A337885(n,k)."
			],
			"example": [
				"Table begins with T(2,1):",
				"1   2      3        4         5          6           7            8 ...",
				"1   5     15       34        65        111         175          260 ...",
				"1  28    387     2784     13125      46836      137543       349952 ...",
				"1 768 202203 11230976 254729375 3267720576 28271133933 183296831488 ...",
				"For T(3,4)=34, the 34 achiral arrangements are AAAA, AAAB, AAAC, AAAD, AABB, AABC, AABD, AACC, AACD, AADD, ABBB, ABBC, ABBD, ABCC, ABDD, ACCC, ACCD, ACDD, ADDD, BBBB, BBBC, BBBD, BBCC, BBCD, BBDD, BCCC, BCCD, BCDD, BDDD, CCCC, CCCD, CCDD, CDDD, and DDDD."
			],
			"mathematica": [
				"m=2; (* dimension of color element, here a triangular face *)",
				"lw[n_,k_]:=lw[n, k]=DivisorSum[GCD[n,k],MoebiusMu[#]Binomial[n/#,k/#]\u0026]/n (*A051168*)",
				"cxx[{a_, b_},{c_, d_}]:={LCM[a, c], GCD[a, c] b d}",
				"compress[x:{{_, _} ...}] := (s=Sort[x];For[i=Length[s],i\u003e1,i-=1,If[s[[i,1]]==s[[i-1,1]], s[[i-1,2]]+=s[[i,2]]; s=Delete[s,i], Null]]; s)",
				"combine[a : {{_, _} ...}, b : {{_, _} ...}] := Outer[cxx, a, b, 1]",
				"CX[p_List, 0] := {{1, 1}} (* cycle index for partition p, m vertices *)",
				"CX[{n_Integer}, m_] := If[2m\u003en, CX[{n}, n-m], CX[{n},m] = Table[{n/k, lw[n/k, m/k]}, {k, Reverse[Divisors[GCD[n, m]]]}]]",
				"CX[p_List, m_Integer] := CX[p, m] = Module[{v = Total[p], q, r}, If[2 m \u003e v, CX[p, v - m], q = Drop[p, -1]; r = Last[p]; compress[Flatten[Join[{{CX[q, m]}}, Table[combine[CX[q, m - j], CX[{r}, j]], {j, Min[m, r]}]], 2]]]]",
				"pc[p_] := Module[{ci, mb}, mb = DeleteDuplicates[p]; ci = Count[p, #] \u0026/@ mb; Total[p]!/(Times @@ (ci!) Times @@ (mb^ci))] (* partition count *)",
				"row[n_Integer] := row[n] = Factor[Total[If[OddQ[Total[1-Mod[#, 2]]], pc[#] j^Total[CX[#, m+1]][[2]], 0] \u0026 /@ IntegerPartitions[n+1]]/((n+1)!/2)]",
				"array[n_, k_] := row[n] /. j -\u003e k",
				"Table[array[n,d+m-n], {d,8}, {n,m,d+m-1}] // Flatten"
			],
			"xref": [
				"Cf. A337883 (oriented), A337884 (unoriented), A337885 (chiral), A051168 (binary Lyndon words).",
				"Other elements: A325001 (vertices), A327086 (edges).",
				"Other polytopes: A337890 (orthotope), A337894 (orthoplex).",
				"Rows 2-4 are A000027, A006003, A331353."
			],
			"keyword": "nonn,tabl",
			"offset": "2,2",
			"author": "_Robert A. Russell_, Sep 28 2020",
			"references": 10,
			"revision": 4,
			"time": "2020-09-28T21:41:18-04:00",
			"created": "2020-09-28T21:41:18-04:00"
		}
	]
}