{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A083884",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 83884,
			"data": "1,5,41,365,3281,29525,265721,2391485,21523361,193710245,1743392201,15690529805,141214768241,1270932914165,11438396227481,102945566047325,926510094425921,8338590849833285,75047317648499561,675425858836496045,6078832729528464401",
			"name": "a(n) = (3^(2*n) + 1) / 2.",
			"comment": [
				"Number of compositions of even natural numbers into n parts \u003c= 8. - _Adi Dani_, May 28 2011",
				"a(n) for n \u003e= 1 gives the number of line segments in the n-th iteration of the Peano curve given by plotting (A163528, A163529) or by (Siromoney 1982) when parallel line segments that are connected end-to-end are counted as a single line segment. - _Jason V. Morgan_, Oct 08 2021"
			],
			"reference": [
				"Siromoney, R., \u0026 Subramanian, K.G. (1982). Space-filling curves and infinite graphs. Graph-Grammars and Their Application to Computer Science."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A083884/b083884.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"Roberto Amato, \u003ca href=\"https://arxiv.org/abs/1912.05925\"\u003eA note on Pythagorean Triples\u003c/a\u003e, arXiv:1912.05925 [math.HO], 2019. See Example 2.1 p. 4.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (10,-9)."
			],
			"formula": [
				"a(0) = 1, a(n) = 9*a(n-1) - 4.",
				"a(n) = Sum_{k=0..n} binomial(2*n, 2*k)*4^k.",
				"a(n) = A002438(n) / A000364(n); A000364(n) : Euler numbers.",
				"G.f.: (1-5*x)/((1-x)*(1-9*x)).",
				"a(n) = (3^n + 1^n + (-1)^n + (-3)^n)/4.",
				"E.g.f.: exp(3*x) + exp(x) + exp(-x) + exp(-3*x).",
				"Each term expresses a Pythagorean relationship, along with (a(n)-1) and a power of 3, n\u003e0, such that sqrt((a(n))^2 - (a(n)-1)^2) = 3^n. E.g., 365^2 - 364^2 - 3^3 = 27 (the Pythagorean triangle (365, 364, 27)). - _Gary W. Adamson_, Jun 25 2006",
				"a(n) = 10*a(n-1) - 9*a(n-2). - _Wesley Ivan Hurt_, Apr 21 2021"
			],
			"example": [
				"From _Adi Dani_, May 28 2011: (Start)",
				"a(2)=41: there are 41 compositions of even natural numbers into 2 parts \u003c=8:",
				"(0,0);",
				"(0,2),(2,0),(1,1);",
				"(0,4),(4,0),(1,3),(3,1),(2,2);",
				"(0,6),(6,0),(1,5),(5,1),(2,4),(4,2),(3,3);",
				"(0,8),(8,0),(1,7),(7,1),(2,6),(6,2),(3,5),(5,3),(4,4);",
				"(2,8),(8,2),(3,7),(7,3),(4,6),(6,4),(5,5);",
				"(4,8),(8,4),(5,7),(7,5),(6,6);",
				"(6,8),(8,6),(7,7);",
				"(8,8).  (End)"
			],
			"mathematica": [
				"f[n_] := (3^(2n)+1)/2; Table[f@i, {i,0,20}] (* _Michael De Vlieger_, Jan 28 2015 *)"
			],
			"program": [
				"(MAGMA) [(3^(2*n) + 1) / 2: n in [0..20]]; // _Vincenzo Librandi_, Jun 16 2011",
				"(PARI) a(n)=(3^(2*n)+1)/2 \\\\ _Charles R Greathouse IV_, Sep 24 2015"
			],
			"xref": [
				"Cf. A000364, A002438, A007853, A083885, A086645."
			],
			"keyword": "easy,nonn",
			"offset": "0,2",
			"author": "_Paul Barry_, May 09 2003",
			"ext": [
				"Additional comments from _Philippe Deléham_, Jul 10 2005"
			],
			"references": 13,
			"revision": 54,
			"time": "2021-11-02T10:10:07-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}