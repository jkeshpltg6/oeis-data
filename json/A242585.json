{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A242585",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 242585,
			"data": "1,9,9,45,9,405,15,189,81,729,27,6075,27,1215,2025,729,81,45927,27,32805,2025,6561,81,229635,243,2187,2187,18225,9,7381125,243,24057,2187,19683,3645,6200145,729,19683,19683,1240029,81,22143375,243,295245,492075,19683",
			"name": "Number of divisors of the n-th positive number that is both triangular and square.",
			"comment": [
				"Number of divisors of A001110(n).",
				"Since each term in A001110 is a square, each term in this sequence is an odd number.",
				"Adding terms to this sequence requires obtaining the prime factorization of terms from A001110. This is facilitated by the observation that A001110(n) = (A000129(n)*A001333(n))^2, but after a few hundred terms, it becomes difficult to obtain the prime factorization of some of the values of A000129(n) and A001333(n).",
				"Obviously, a(1) is the only term whose value is 1. It can be shown (see A081978) that 15 appears only at a(7).",
				"Conjectures:",
				"(1) There exist only 5 triangular numbers with exactly 9 divisors: the 2nd, 3rd, 5th, 29th, and 59th terms of A001110.",
				"(2) A001110(4)=41616 is the only triangular number with exactly 45 divisors.",
				"(3) A001110(6)=48024900 is the only triangular number with exactly 405 divisors.",
				"(4) A001110(8)=55420693056 is the only triangular number with exactly 189 divisors."
			],
			"link": [
				"Jon E. Schoenfield, \u003ca href=\"/A242585/b242585.txt\"\u003eTable of n, a(n) for n = 1..300\u003c/a\u003e",
				"Jon E. Schoenfield, \u003ca href=\"/A242585/a242585.txt\"\u003eTable of values of n \u003c= 300 such that n-th square triangular number has exactly k divisors\u003c/a\u003e"
			],
			"formula": [
				"Conjecture: a(n) == 0 mod 9 for n different from 1 and 7 [tested up to n = 300]. - _Ivan N. Ianakiev_, May 29 2014"
			],
			"example": [
				"a(2) = 9 because A001110(2) = 36 = 2^2 * 3^2 has (2+1)*(2+1) = 9 divisors.",
				"a(4) = 45 because A001110(4) = 41616 = 2^4 * 3^2 * 17^2 has (4+1)*(2+1)*(2+1) divisors.",
				"a(6) = 405 because A001110(6) = 48024900 = 2^2 * 3^4 * 5^2 * 7^2 * 11^2 has (2+1)*(4+1)*(2+1)*(2+1)*(2+1) = 405 divisors."
			],
			"program": [
				"(MAGMA) a:=0; b:=1; NumberOfDivisors(b); for n in [2..46 by 2] do a:=34*b-a+2; NumberOfDivisors(a); b:=34*a-b+2; NumberOfDivisors(b); end for;"
			],
			"xref": [
				"Cf. A001110, A000217, A081978."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Jon E. Schoenfield_, May 25 2014",
			"references": 4,
			"revision": 17,
			"time": "2017-10-31T22:45:18-04:00",
			"created": "2014-05-26T01:11:50-04:00"
		}
	]
}