{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A256111",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 256111,
			"data": "0,1,5,13,26,50,65,85,116,100,97,85,85,90,128,205,293,409,481,586,730,845,890,841,833,745,514,244,65,17,106,338,698,1117,1225,1193,1040,986,1037,1060,850,477,197,85,80,232,530,757,650,522,225,16,50,333,797",
			"name": "a(n) = squared distance to the origin of the n-th vertex on a Babylonian Spiral.",
			"comment": [
				"A Babylonian spiral is constructed by starting with a zero vector and progressively concatenating the next longest vector with integral endpoints on a Cartesian grid.  (The squares of the lengths of these vectors are A001481.)  The direction of the new vector is chosen to create the clockwise spiral which minimizes the change in direction from the previous vector.",
				"     . . . . . . . . . . . . . . . . . . . . . .",
				"     . . . . . . . . . . . . . . . . . . . . . .",
				"     . . . 14. . . . . . . . . . . . . . . . . .",
				"     . . . . . . . . . . . . . . . . . . . . . .",
				"     . . . . . . . . . . . . . . . . . . . . . .",
				"     . . . . . . . . . . . . . . . . . . . . . .",
				"     . . . . . . . . . . . . . . . . . . . . . .",
				"     . . 13. . . . . . . . . . . . . . . . . . .",
				"     . . . . . . . . . . . . 2 . 3 . . . . . . .",
				"     . . . . . . . . . . . 1 . . . . 4 . . . . .",
				"     . . . . . . . . . . . o . . . . . . . . . .",
				"     . . . . . . . . . . . . . . . . . . 5 . . .",
				"     . . 12. . . . . . . . . . . . . . . . . . .",
				"     . . . . . . . . . . . . . . . . . . . . . .",
				"     . . . . . . . . . . . . . . . . . . 6 . . .",
				"     . . . . . . . . . . . . . . . . . . . . . .",
				"     . . . . 11. . . . . . . . . . . . . . . . .",
				"     . . . . . . . . . . . . . . . . . 7 . . . .",
				"     . . . . . . . . . . . . . . . . . . . . . .",
				"     . . . . . . . 10. . . . . . . . . . . . . .",
				"     . . . . . . . . . . . 9 . . . 8 . . . . . .",
				"     . . . . . . . . . . . . . . . . . . . . . .",
				"The name is chosen to mislead school students into making an incorrect hypothesis about the Babylonian Spiral's long-term behavior."
			],
			"link": [
				"Lars Blomberg, \u003ca href=\"/A256111/b256111.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Lars Blomberg, Illustrations of \u003ca href=\"/A256111/a256111.png\"\u003e100\u003c/a\u003e, \u003ca href=\"/A256111/a256111_1.png\"\u003e1000\u003c/a\u003e and \u003ca href=\"/A256111/a256111_2.png\"\u003e10000\u003c/a\u003e terms",
				"MathPickle, \u003ca href=\"http://mathpickle.com/project/babylonian-spiral\"\u003eBabylonian Spiral\u003c/a\u003e"
			],
			"example": [
				"On the above diagram, point 4 is distance sqrt(26) from the origin, so a(4) = 26."
			],
			"mathematica": [
				"NextVec[{x_, y_}] :=",
				"Block[{n = x^2 + y^2 + 1}, While[SquaresR[2, n] == 0, n++];",
				"  TakeSmallestBy[",
				"     Union[Flatten[(Transpose[",
				"        Transpose[Tuples[{1, -1},2]] #] \u0026 /@",
				"        ({{#[[1]], #[[2]]}, {#[[2]], #[[1]]}})) \u0026 /@",
				"     PowersRepresentations[n, 2, 2], 2]],",
				"  Mod[ArcTan[#[[2]], #[[1]]] - ArcTan[y, x], 2 Pi] \u0026, 1][[1]]",
				"]",
				"Norm[#]^2 \u0026 /@ Accumulate[NestList[NextVec, {0, 1}, 50]] (* _Alex Meiburg_, Dec 29 2017 *)"
			],
			"xref": [
				"x-coordinates given in A297346. y-coordinates given in A297347."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Gordon Hamilton_, Mar 14 2015",
			"ext": [
				"Corrected a(16) and more terms from _Lars Blomberg_, Nov 17 2016"
			],
			"references": 6,
			"revision": 35,
			"time": "2018-09-14T06:22:36-04:00",
			"created": "2015-03-15T01:28:13-04:00"
		}
	]
}