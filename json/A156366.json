{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A156366",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 156366,
			"data": "1,1,1,3,1,12,9,1,33,99,27,1,78,594,702,81,1,171,2718,8154,4617,243,1,360,10719,65232,96471,29160,729,1,741,38637,421713,1265139,1043199,180063,2187,1,1506,131472,2382318,12651390,21440862,10649232,1097874",
			"name": "Triangle of coefficients of p(n, x) where p(n, x) is defined as p(n, x) = (1-3*x)^(n+1)*PolyLog(-n, 3*x)/(3*x), read by rows.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A156366/b156366.txt\"\u003eRows n = 0..50 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = [x^k]( p(n, x) ), where p(n, x) is defined as p(n, x) = (1-3*x)^(n+1)*Sum_{j \u003e= 0} ( (j+1)^n*(3*x)^j ), or p(n, x) = (1-3*x)^(n+1)* PolyLog(-n, 3*x)/(3*x).",
				"From _G. C. Greubel_, Jan 02 2022: (Start)",
				"T(n, k) = 3^k * Sum_{j=0..k} binomial(n+1, j)*(-1)^j*(k-j+1)^n.",
				"T(n, k) = 3^k * A008292(n, k+1).",
				"T(n, 0) = 1.",
				"T(n, n-1) = 3^n, for n \u003e= 1. (End)"
			],
			"example": [
				"Triangle begins as:",
				"  1;",
				"  1;",
				"  1,    3;",
				"  1,   12,      9;",
				"  1,   33,     99,      27;",
				"  1,   78,    594,     702,       81;",
				"  1,  171,   2718,    8154,     4617,      243;",
				"  1,  360,  10719,   65232,    96471,    29160,      729;",
				"  1,  741,  38637,  421713,  1265139,  1043199,   180063,    2187;",
				"  1, 1506, 131472, 2382318, 12651390, 21440862, 10649232, 1097874, 6561;"
			],
			"mathematica": [
				"(* First program *)",
				"p[x_, n_]:= (1-3*x)^(1+n)*PolyLog[-n, 3*x]/(3*x);",
				"T[n_]:= CoefficientList[Series[p[x, n], {x,0,30}], x];",
				"Table[T[n], {n,0,10}]//Flatten (* modified by _G. C. Greubel_, Jan 02 2022 *)",
				"(* Second program *)",
				"T[n_, k_]:= 3^k*Sum[(k-j+1)^n*Binomial[n+1, j]*(-1)^j, {j,0,k}];",
				"Join[{1}, Table[T[n, k], {n, 10}, {k, 0, n-1}]]//Flatten (* _G. C. Greubel_, Jan 02 2022 *)"
			],
			"program": [
				"(MAGMA) T:= func\u003c n,k | n eq 0 and k eq 0 select 1 else 3^k*(\u0026+[(-1)^j*Binomial(n+1,j)*(k-j+1)^n: j in [0..k]]) \u003e;",
				"[1] cat [T(n,k): k in [0..n-1], n in [0..10]]; // _G. C. Greubel_, Jan 02 2022",
				"(Sage)",
				"def T(n, k): return 3^k*sum((-1)^j*binomial(n+1,j)*(k-j+1)^n for j in (0..k))",
				"[1]+flatten([[T(n,k) for k in (0..n-1)] for n in (0..10)]) # _G. C. Greubel_, Jan 02 2022"
			],
			"xref": [
				"Cf. A000244, A008292."
			],
			"keyword": "nonn,tabf,changed",
			"offset": "0,4",
			"author": "_Roger L. Bagula_, Feb 08 2009",
			"ext": [
				"Edited by _G. C. Greubel_, Jan 02 2022"
			],
			"references": 1,
			"revision": 16,
			"time": "2022-01-04T04:11:08-05:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}