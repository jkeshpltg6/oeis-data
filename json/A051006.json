{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A051006",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 51006,
			"data": "4,1,4,6,8,2,5,0,9,8,5,1,1,1,1,6,6,0,2,4,8,1,0,9,6,2,2,1,5,4,3,0,7,7,0,8,3,6,5,7,7,4,2,3,8,1,3,7,9,1,6,9,7,7,8,6,8,2,4,5,4,1,4,4,8,8,6,4,0,9,6,0,6,1,9,3,5,7,3,3,4,1,9,6,2,9,0,0,4,8,4,2,8,4,7,5,7,7,7,9,3,9,6,1,6",
			"name": "Prime constant: decimal value of (A010051 interpreted as a binary number).",
			"comment": [
				"From Ferenc Adorjan (fadorjan(AT)freemail.hu): (Start)",
				"Decimal expansion of the representation of the sequence of primes by a single real in (0,1).",
				"Any monotonic integer sequence can be represented by a real number in (0, 1) such a way that in the binary representation of the real, the n-th fractional digit is 1 if and only if n is in the sequence.",
				"Examples of the inverse mapping are A092855 and A092857. (End)",
				"Is the prime constant an EL number? See Chow's 1999 article. - _Lorenzo Sauras Altuzarra_, Oct 05 2020"
			],
			"link": [
				"Harry J. Smith, \u003ca href=\"/A051006/b051006.txt\"\u003eTable of n, a(n) for n = 0..20000\u003c/a\u003e",
				"Ferenc Adorjan, \u003ca href=\"http://web.axelero.hu/fadorjan/aronsf.pdf\"\u003eBinary mapping of monotonic sequences and the Aronson function\u003c/a\u003e",
				"Timothy Y. Chow, \u003ca href=\"http://timothychow.net/closedform.pdf\"\u003eWhat is a Closed-Form Number?\u003c/a\u003e, The American Mathematical Monthly, Vol. 106, No. 5. (May, 1999), pp. 440-448.",
				"Igor Pak, \u003ca href=\"https://arxiv.org/abs/1803.06636\"\u003eComplexity problems in enumerative combinatorics\u003c/a\u003e, arXiv:1803.06636 [math.CO], 2018.",
				"Simon Plouffe, \u003ca href=\"http://www.plouffe.fr/simon/constants/primesbin.txt\"\u003ePrimes coded in binary to 1000 digits\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PrimeConstant.html\"\u003ePrime Constant\u003c/a\u003e"
			],
			"formula": [
				"Prime constant C = Sum_{k\u003e=1} 1/2^p(k), where p(k) is the k-th prime. - _Alexander Adamchuk_, Aug 22 2006",
				"From _Amiram Eldar_, Aug 11 2020: (Start)",
				"Equals Sum_{k\u003e=1} A010051(k)/2^k.",
				"Equals Sum_{k\u003e=1} 1/A034785(k).",
				"Equals (1/2) * A119523.",
				"Equals Sum_{k\u003e=1} pi(k)/2^(k+1), where pi(k) = A000720(k). (End)"
			],
			"example": [
				"0.414682509851111660... (base 10) = .01101010001010001010001... (base 2)."
			],
			"maple": [
				"a := n -\u003e ListTools:-Reverse(convert(floor(evalf[1000](sum(1/2^ithprime(k), k = 1 .. infinity)*10^(n+1))), base, 10))[n+1]: - _Lorenzo Sauras Altuzarra_, Oct 05 2020"
			],
			"mathematica": [
				"RealDigits[ FromDigits[ {{Table[ If[ PrimeQ[n], 1, 0], {n, 370}]}, 0}, 2], 10, 111][[1]] (* _Robert G. Wilson v_, Jan 15 2005 *)",
				"RealDigits[Sum[1/2^Prime[k], {k, 1000}], 10, 100][[1]] (* _Alexander Adamchuk_, Aug 22 2006 *)"
			],
			"program": [
				"(PARI) { mt(v)= /*Returns the binary mapping of v monotonic sequence as a real in (0,1)*/ local(a=0.0,p=1,l);l=matsize(v)[2]; for(i=1,l,a+=2^(-v[i])); return(a)} \\\\ Ferenc Adorjan",
				"(PARI) { default(realprecision, 20080); x=0; m=67000; for (n=1, m, if (isprime(n), a=1, a=0); x=2*x+a; ); x=10*x/2^m; for (n=0, 20000, d=floor(x); x=(x-d)*10; write(\"b051006.txt\", n, \" \", d)); } \\\\ _Harry J. Smith_, Jun 15 2009",
				"(PARI) suminf(n=1,.5^prime(n)) \\\\ Then: digits(%\\.1^default(realprecision)) to get seq. of digits. N.B.: Functions sumpos() and sumnum() yield much less accurate results. - _M. F. Hasler_, Jul 04 2017"
			],
			"xref": [
				"Cf. A000720, A010051, A034785, A051007, A132800, A092857, A092858, A092859, A092860, A092861, A092862, A092863, A092874, A119523."
			],
			"keyword": "nonn,cons",
			"offset": "0,1",
			"author": "_Eric W. Weisstein_",
			"references": 44,
			"revision": 56,
			"time": "2020-10-31T14:13:27-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}