{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A348507",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 348507,
			"data": "0,1,1,5,1,6,1,19,7,8,1,24,1,10,9,65,1,30,1,34,11,14,1,84,11,16,37,44,1,42,1,211,15,20,13,108,1,22,17,122,1,54,1,64,51,26,1,276,15,58,21,74,1,138,17,160,23,32,1,156,1,34,65,665,19,78,1,94,27,74,1,360,1,40,69,104,19,90,1,406,175,44,1,204",
			"name": "a(n) = A003959(n) - n, where A003959 is multiplicative with a(p^e) = (p+1)^e.",
			"comment": [
				"a(p*(n/p)) - (n/p) = (p+1)*a(n/p) holds for all prime divisors p of n, which can be seen by expanding the left hand side as (A003959(p*(n/p)) - (p*(n/p))) - (n/p) = (p+1)*A003959(n/p)-((p+1)*(n/p)) = (p+1)*(A003959(n/p)-(n/p)) = (p+1)*a(n/p). This implies that a(n) \u003e= A003415(n) for all n. (See also comments in A348970). - _Antti Karttunen_, Nov 06 2021"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A348507/b348507.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Antti Karttunen, \u003ca href=\"/A348507/a348507.txt\"\u003eData supplement: n, a(n) computed for n = 1..65537\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A003959(n) - n.",
				"a(n) = A348508(n) + n.",
				"a(n) = A001065(n) + A348029(n).",
				"From _Antti Karttunen_, Nov 06 2021: (Start)",
				"a(n) = Sum_{d|n} A348971(d).",
				"a(n) = A003415(n) + A348970(n).",
				"For all n \u003e= 1, A322582(n) \u003c= A003415(n) \u003c= a(n).",
				"For n \u003e 1, a(n) = a(A032742(n))*(1+A020639(n)) + A032742(n). [See the comments above, and compare this with _Reinhard Zumkeller_'s May 09 2011 recursive formula for A003415]",
				"(End)"
			],
			"mathematica": [
				"f[p_, e_] := (p + 1)^e; a[1] = 0; a[n_] := Times @@ f @@@ FactorInteger[n] - n; Array[a, 100] (* _Amiram Eldar_, Oct 30 2021 *)"
			],
			"program": [
				"(PARI)",
				"A003959(n) = { my(f = factor(n)); for(i=1, #f~, f[i, 1]++); factorback(f); };",
				"A348507(n) = (A003959(n) - n);",
				"(PARI)",
				"A020639(n) = if(1==n,n,(factor(n)[1, 1]));",
				"A348507(n) = { my(s=0, m=1, spf); while(n\u003e1, spf = A020639(n); n /= spf; s += m*n; m *= (1+spf)); (s); }; \\\\ (Compare this with similar programs given in A003415 and in A322582) - _Antti Karttunen_, Nov 06 2021"
			],
			"xref": [
				"Cf. A001065, A003415, A003959, A020639, A032742, A348029, A348508, A348929 [= gcd(n,a(n))], A348950, A348970.",
				"Cf. A348971 (Möbius transform) and A349139, A349140, A349141, A349142, A349143 (other Dirichlet convolutions).",
				"Cf. also A168065 (the arithmetic mean of this and A322582), A168066."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Antti Karttunen_, Oct 30 2021",
			"references": 14,
			"revision": 44,
			"time": "2021-11-11T19:30:45-05:00",
			"created": "2021-10-31T01:40:53-04:00"
		}
	]
}