{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A121576",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 121576,
			"data": "1,2,1,6,5,1,24,24,8,1,114,123,51,11,1,600,672,312,87,14,1,3372,3858,1914,618,132,17,1,19824,22992,11904,4218,1068,186,20,1,120426,140991,75183,28383,8043,1689,249,23,1,749976,884112,481704,190347,58398,13929,2508,321,26,1",
			"name": "Riordan array (2-2*x-sqrt(1-8*x+4*x^2), (1-2*x-sqrt(1-8*x+4*x^2))/2).",
			"comment": [
				"Inverse of Riordan array (1/(1+2*x), x*(1-x)/(1+2*x)).",
				"Row sums are A047891; first column is A054872. Signed version given by A121575.",
				"Triangle T(n,k), 0 \u003c= k \u003c= n, read by rows, given by [2, 1, 3, 1, 3, 1, 3, 1, 3, ...] DELTA [1, 0, 0, 0, 0, 0, 0, 0, 0, ...] where DELTA is the operator defined in A084938. - _Philippe Deléham_, Aug 09 2006"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A121576/b121576.txt\"\u003eRows n=0..100 of triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = [x^(n-k)](1-2*x-2*x^2)*(1+2*x)^n/(1-x)^(n+1) = (1/2)*Sum_{i=0..n-k} binomial(n,i) * binomial(2*n-k-i,n) * (4 - 9*i + 3*i^2 - 6*(i-1)*n + 2*n^2)/((n-i+2)*(n-i+1))*2^i. - _Emanuele Munarini_, May 18 2011"
			],
			"example": [
				"Triangle begins",
				"     1;",
				"     2,    1;",
				"     6,    5,    1;",
				"    24,   24,    8,   1;",
				"   114,  123,   51,  11,   1;",
				"   600,  672,  312,  87,  14,  1;",
				"  3372, 3858, 1914, 618, 132, 17, 1;",
				"From _Paul Barry_, Apr 27 2009: (Start)",
				"Production matrix is",
				"  2, 1,",
				"  2, 3, 1,",
				"  2, 3, 3, 1,",
				"  2, 3, 3, 3, 1,",
				"  2, 3, 3, 3, 3, 1,",
				"  2, 3, 3, 3, 3, 3, 1,",
				"  2, 3, 3, 3, 3, 3, 3, 1",
				"In general, the production matrix of the inverse of (1/(1-rx),x(1-x)/(1-rx)) is",
				"  -r, 1,",
				"  -r, 1 - r, 1,",
				"  -r, 1 - r, 1 - r, 1,",
				"  -r, 1 - r, 1 - r, 1 - r, 1,",
				"  -r, 1 - r, 1 - r, 1 - r, 1 - r, 1,",
				"  -r, 1 - r, 1 - r, 1 - r, 1 - r, 1 - r, 1,",
				"  -r, 1 - r, 1 - r, 1 - r, 1 - r, 1 - r, 1 - r, 1 (End)"
			],
			"mathematica": [
				"Flatten[Table[Sum[Binomial[n,i]Binomial[2n-k-i,n](4-9i+3i^2-6(i-1)n+2n^2)/((n-i+2)(n-i+1))2^i,{i,0,n-k}]/2,{n,0,8},{k,0,n}]]",
				"(* _Emanuele Munarini_, May 18 2011 *)"
			],
			"program": [
				"(Maxima) create_list(sum(binomial(n,i)*binomial(2*n-k-i,n)*(4-9*i+3*i^2-6*(i-1)*n+2*n^2)/((n-i+2)*(n-i+1))*2^i,i,0,n-k)/2,n,0,8,k,0,n);  /* Emanuele Munarini, May 18 2011 */",
				"(PARI) for(n=0,10, for(k=0,n, print1(sum(j=0, n-k, 2^j*binomial(n,j) *binomial(2*n-k-j, n)*(4-9*j+3*j^2-6*(j-1)*n + 2*n^2)/((n-j+2)*(n-j+1)))/2, \", \"))) \\\\ _G. C. Greubel_, Nov 02 2018",
				"(MAGMA) [[(\u0026+[ 2^j*Binomial(n,j)*Binomial(2*n-k-j, n)*(4-9*j+3*j^2-6*(j-1)*n + 2*n^2)/((n-j+2)*(n-j+1))/2: j in [0..(n-k)]]): k in [0..n]]: n in [0..10]]; // _G. C. Greubel_, Nov 02 2018"
			],
			"keyword": "easy,nonn,tabl",
			"offset": "0,2",
			"author": "_Paul Barry_, Aug 08 2006",
			"references": 6,
			"revision": 19,
			"time": "2020-01-22T02:40:54-05:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}