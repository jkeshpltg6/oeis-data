{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A108950",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 108950,
			"data": "1,1,2,3,4,7,9,14,18,27,35,49,64,86,113,148,192,247,319,404,517,649,822,1024,1285,1590,1979,2436,3007,3682,4515,5501,6703,8131,9851,11899,14344,17252,20703,24804,29640,35377,42115,50085,59415,70420,83261,98365,115947,136557",
			"name": "Number of partitions of n with more odd parts than even parts.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A108950/b108950.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"B. Kim, E. Kim, and J. Lovejoy, \u003ca href=\"https://doi.org/10.1016/j.ejc.2020.103159\"\u003eParity bias in partitions\u003c/a\u003e, European J. Combin., 89 (2020), 103159, 19 pp."
			],
			"formula": [
				"G.f.: Sum_{k\u003e=0} x^k*(1-x^(2*k))/Product_{i=1..k} (1-x^(2*i))^2. - _Vladeta Jovovic_, Aug 19 2007",
				"a(n) = A130780(n) - A045931(n) = A171967(n) - A108949(n). - _Reinhard Zumkeller_, Jan 21 2010",
				"a(n) = Sum_{k=1..n} A240009(n,k). - _Alois P. Heinz_, Mar 30 2014",
				"G.f.: (Product_{k\u003e=1} 1/(1-x^(2*k-1)))*Sum_{n\u003e=1} q^(2*n^2-n)*(1-q^(2*n))/Product_{k=1..n} (1-q^(2*k))^2. - _Jeremy Lovejoy_, Jan 12 2021"
			],
			"example": [
				"a(4) = 3: {[3,1], [2,1,1], [1,1,1,1]}; a(5) = 4: {[5], [3,1,1], [2,1,1,1], [1,1,1,1,1]}."
			],
			"maple": [
				"with(combinat,partition):oddbigrevn:=proc(n::nonnegint) local evencount,oddcount,bigcount,parts,i,j; printlevel:=-1;bigcount:=0; partitions:=partition(n);for i from 1 to nops(partitions) do evencount:=0; oddcount:=0;for j from 1 to nops(partitions[i]) do if (op(j,partitions[i]) mod 2 \u003c\u003e0) then oddcount:=oddcount+1 fi; if (op(j,partitions[i]) mod 2 =0) then evencount:=evencount+1 fi od; if (evencount\u003coddcount) then bigcount:=bigcount+1 fi od; printlevel:=1; return(bigcount) end proc; seq(oddbigrevn(i),i=1..42);",
				"# second Maple program:",
				"b:= proc(n, i, t) option remember; `if`(n=0,",
				"      `if`(t\u003e0, 1, 0), `if`(i\u003c1, 0, b(n, i-1, t)+",
				"      `if`(i\u003en, 0, b(n-i, i, t+(2*irem(i, 2)-1)))))",
				"    end:",
				"a:= n-\u003e b(n$2, 0):",
				"seq(a(n), n=1..80);  # _Alois P. Heinz_, Mar 30 2014"
			],
			"mathematica": [
				"p[n_] := p[n] = Select[IntegerPartitions[n], Count[#, _?OddQ] \u003e Count[#, _?EvenQ] \u0026]; t = Table[p[n], {n, 0, 15}] (* partitions of n with # odd parts \u003e # even parts *)",
				"TableForm[t] (* partitions, vertical format *)",
				"Table[Length[p[n]], {n, 1, 30}] (* A108950 *)",
				"(* _Peter J. C. Moses_, Mar 10 2014 *)",
				"b[n_, i_, t_] := b[n, i, t] = If[n==0, If[t\u003e0, 1, 0], If[i\u003c1, 0, b[n, i-1, t] + If[i\u003en, 0, b[n-i, i, t + (2*Mod[i, 2]-1)]]]]; a[n_] := b[n, n, 0]; Table[a[n], {n, 1, 80}] (* _Jean-François Alcover_, Nov 16 2015, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A045931 for #even parts = #odd parts, A108949 for #even parts \u003e #odd parts.",
				"Cf. A171966, A171967. - _Reinhard Zumkeller_, Jan 21 2010"
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Len Smiley_, Jul 21 2005",
			"ext": [
				"More terms from _Joerg Arndt_, Oct 04 2012"
			],
			"references": 9,
			"revision": 33,
			"time": "2021-08-19T20:06:44-04:00",
			"created": "2005-09-21T03:00:00-04:00"
		}
	]
}