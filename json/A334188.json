{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A334188",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 334188,
			"data": "0,1,-1,2,-6,-2,3,-7,-5,-3,8,4,-8,-4,-12,9,7,5,-9,-11,-13,10,18,6,-26,-10,-18,-14,11,17,19,-27,-25,-19,-17,-15,40,12,16,20,-28,-24,-20,-16,-48,41,39,13,15,21,-29,-23,-21,-47,-49,42,34,38,14,22,-34,-30",
			"name": "T(n, k) is the number of steps from the point (0, 0) to the point (k, n) along the space filling curve U described in Comments section; a negative value corresponds to moving backwards; square array T(n, k), n, k \u003e= 0 read by antidiagonals downwards.",
			"comment": [
				"We start with a unit square U_0 oriented counterclockwise, the origin being at the left bottom corner:",
				"         +---\u003c---+",
				"         |       |",
				"         v       ^",
				"         |       |",
				"         O---\u003e---+",
				"The configuration U_{k+1} is obtained by connecting four copies of the configuration U_k as follows:",
				"             |   |                               |   |",
				"         .   +   +   .                       .   +   +   .",
				"     U_k     ^   v     U_k                       ^   v",
				"         .   +   +   .                       .   +   +   .",
				"             |   |                               |   |",
				"    -+-\u003e-+---+   +---+-\u003e-+-             -+-\u003e-+   +   +   +-\u003e-+-",
				"                                --\u003e          v   |   |   ^",
				"    -+-\u003c-+---+   +---+-\u003c-+-             -+-\u003c-+   +-\u003c-+   +-\u003c-+-",
				"             |   |",
				"         .   +   +   .                       .   +-\u003e-+   .",
				"     U_k     ^   v     U_k                       ^   v",
				"         .   +   +   .                       .   +   +   .",
				"             |   |                               |   |",
				"For any k \u003e= 0, U_k is a closed curve with length 4^(k+1) and visiting every lattice point (x, y) with 0 \u003c= x, y \u003c 2^(k+1).",
				"The space filling curve U corresponds to the limit of U_k as k tends to infinity, and is a variant of H-order curve.",
				"U visits once every lattice points with nonnegative coordinates and has a single connected component."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A334188/b334188.txt\"\u003eTable of n, a(n) for n = 0..5049\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A334188/a334188.png\"\u003eRepresentation of U_k for k = 0..5\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A334188/a334188_1.png\"\u003eColored representation of U_7\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A334188/a334188_2.png\"\u003eColored representation of the table for 0 \u003c= x, y, \u003c= 1023\u003c/a\u003e (where the hue is function of T(y, x))",
				"Rémy Sigrist, \u003ca href=\"/A334188/a334188.gp.txt\"\u003ePARI program for A334188\u003c/a\u003e"
			],
			"example": [
				"Square array starts:",
				"  n\\k|    0    1    2    3    4    5    6    7",
				"  ---+----------------------------------------",
				"    0|    0....1....2....3    8....9...10...11",
				"     |    |              |    |              |",
				"    1|   -1   -6...-7    4    7   18...17   12",
				"     |    |    |    |    |    |    |    |    |",
				"    2|   -2   -5   -8    5....6   19   16   13",
				"     |    |    |    |              |    |    |",
				"    3|   -3...-4   -9  -26..-27   20   15...14",
				"     |              |    |    |    |",
				"    4|  -12..-11..-10  -25  -28   21...22...23",
				"     |    |              |    |              |",
				"    5|  -13  -18..-19  -24  -29  -34..-35   24",
				"     |    |    |    |    |    |    |    |    |",
				"    6|  -14  -17  -20  -23  -30  -33  -36   25..",
				"     |    |    |    |    |    |    |    |",
				"    7|  -15..-16  -21..-22  -31..-32  -37 -102..",
				"     |                                  |    |"
			],
			"program": [
				"(PARI) See Links section."
			],
			"xref": [
				"See A163334, A323335 and A334232 for similar sequences.",
				"See A334220, A334221, A334222 and A334223 for the coordinates of the curve."
			],
			"keyword": "sign,look,tabl",
			"offset": "0,4",
			"author": "_Rémy Sigrist_, Apr 18 2020",
			"references": 6,
			"revision": 30,
			"time": "2021-02-25T21:24:36-05:00",
			"created": "2020-04-21T09:14:35-04:00"
		}
	]
}