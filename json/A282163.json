{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A282163",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 282163,
			"data": "1,154836,985320,1108536,1113959,1492260,1576696,1632708,1649238,1684540,1805570,1988008,2508792,2548810,2550408,2659260,2698740,2746590,2995122,3074552,3286710,3330795,3538458,3574200,3730155,4039932,4160240,4318548,4374370,4426695,4523985",
			"name": "Numbers k such that the central binomial coefficient C(2*k,k) is divisible by k^3.",
			"comment": [
				"Equivalently, numbers k such that the k-th Catalan number C(2*k,k)/(k+1) is divisible by k^3. - _Lucian Craciun_, Feb 09 2017",
				"The asymptotic density of this sequence is 0.000031511777... (Ford and Konyagin, 2021). - _Amiram Eldar_, Jan 26 2021"
			],
			"link": [
				"Lucian Craciun, \u003ca href=\"/A282163/b282163.txt\"\u003eTable of n, a(n) for n = 1..16000\u003c/a\u003e (corrected and extended by Giovanni Resta)",
				"Kevin Ford and Sergei Konyagin, \u003ca href=\"https://doi.org/10.1090/tran/8183\"\u003eDivisibility of the central binomial coefficient binomial(2n, n)\u003c/a\u003e, Trans. Amer. Math. Soc., Vol. 374, No. 2 (2021), pp. 923-953; \u003ca href=\"https://arxiv.org/abs/1909.03903\"\u003earXiv preprint\u003c/a\u003e, arXiv:1909.03903 [math.NT], 2019-2020.",
				"Wikipedia Mathematics Reference Desk, \u003ca href=\"http://en.wikipedia.org/wiki/Wikipedia:Reference_desk/Archives/Mathematics/2017_February_9#n4_divides_Central_binomial_coefficient\"\u003en^4 Divides Central Binomial Coefficient\u003c/a\u003e."
			],
			"example": [
				"The central binomial coefficient C(2*154836,154836) is divisible by 154836^3."
			],
			"maple": [
				"A282163 := proc (n, m) local a, cbc, k; a := {}; cbc := binomial(2*n, n); for k from n+1 to m do cbc := cbc*(4-2/k); if type(cbc/k^3, integer) then a := `union`(a, {k}) end if end do; a end proc; A282163(0, 10^6)"
			],
			"mathematica": [
				"Select[Table[n, {n, 10^6}], IntegerQ[Binomial[2#, #]/#^3] \u0026] (* for small n *)",
				"n := 0; m := 10^6; A282163 := {}; cbc := Binomial[2n, n]; For[k := n+1, k \u003c= m, k++, {cbc *= 4-2/k, If[IntegerQ[cbc/k^3], A282163 = Append[A282163, k]]}] (* for large m *)",
				"A282163:={}; k:=3; For[n:=1, n\u003c=10^6, n++, {f=FactorInteger[n], For[j:=1, j\u003c=Length[f], j++, {b=True, If[Sum[Floor[2n/f[[j, 1]]^i]-2 Floor[n/f[[j, 1]]^i], {i, 1, Length[IntegerDigits[2n, f[[j, 1]]]]}]\u003cf[[j, 2]]k, {b=False, Break[]}]}], If[b, A282163=Append[A282163, n]]}] (* Legendre's formula for drastic time reduction, _Lucian Craciun_, Feb 28 2017; optimized by _Lucian Craciun_, Mar 02 2017 *)"
			],
			"xref": [
				"Cf. A000108, A000984, A014847, A121943, A282346, A283073, A283074, A282672."
			],
			"keyword": "nonn,changed",
			"offset": "1,2",
			"author": "_Lucian Craciun_, Feb 07 2017",
			"references": 7,
			"revision": 56,
			"time": "2022-01-10T05:27:04-05:00",
			"created": "2017-02-08T02:40:19-05:00"
		}
	]
}