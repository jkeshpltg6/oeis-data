{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A061552",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 61552,
			"data": "1,1,2,6,23,103,513,2762,15793,94776,591950,3824112,25431452,173453058,1209639642,8604450011,62300851632,458374397312,3421888118907,25887131596018,198244731603623,1535346218316422,12015325816028313,94944352095728825,757046484552152932,6087537591051072864",
			"name": "Number of 1324-avoiding permutations of length n.",
			"reference": [
				"Miklós Bóna, Combinatorics of Permutations. Discrete Mathematics and its Applications (Boca Raton), 2nd edn. CRC Press, Boca Raton (2012)."
			],
			"link": [
				"David Bevan, \u003ca href=\"/A061552/b061552.txt\"\u003eTable of n, a(n) for n = 0..50\u003c/a\u003e (from the Conway/Guttmann reference; terms 0..31 by Joerg Arndt, taken from the Johansson/Nakamura reference; terms 37..50 by Bjarki Ágúst Guðmundsson, taken from the Conway/Guttmann/Zinn-Justin reference).",
				"M. H. Albert, M. Elder, A. Rechnitzer, P. Westcott, and M. Zabrocki, \u003ca href=\"http://arxiv.org/abs/math/0502504\"\u003eOn the Wilf-Stanley limit of 4231-avoiding permutations and a conjecture of Arratia\u003c/a\u003e, arXiv:math/0502504 [math.CO], 2005.",
				"R. Arratia, \u003ca href=\"https://doi.org/10.37236/1477\"\u003eOn the Stanley-Wilf conjecture for the number of permutations avoiding a given pattern.\u003c/a\u003e, Electron. J. Combin. 6, N1 (1999).",
				"David Bevan, \u003ca href=\"http://arxiv.org/abs/1406.2890\"\u003ePermutations avoiding 1324 and patterns in Łukasiewicz paths\u003c/a\u003e, arXiv:1406.2890 [math.CO], 2014-2015.",
				"David Bevan, Robert Brignall, Andrew Elvey Price and Jay Pantone, \u003ca href=\"https://doi.org/10.1016/j.ejc.2020.103115\"\u003eA structural characterisation of Av(1324) and new bounds on its growth rate\u003c/a\u003e, European J. Combin. 88 (2020).",
				"Miklós Bóna, \u003ca href=\"http://arxiv.org/abs/1207.2379\"\u003eA new upper bound for 1324-avoiding permutations\u003c/a\u003e, arXiv:1207.2379 [math.CO], 2012.",
				"Miklós Bóna, \u003ca href=\"http://dx.doi.org/10.1017/S0963548314000091\"\u003eA new upper bound for 1324-avoiding permutations\u003c/a\u003e, Combin. Probab. Comput. 23(5), 717-724 (2014).",
				"Miklós Bóna, \u003ca href=\"http://arxiv.org/abs/1404.4033\"\u003eA new record for 1324-avoiding permutations\u003c/a\u003e, arXiv:1404.4033 [math.CO], 2014.",
				"Miklós Bóna, \u003ca href=\"http://dx.doi.org/10.1007/s40879-014-0020-6\"\u003eA new record for 1324-avoiding permutations\u003c/a\u003e, European Journal of Mathematics (2015) 1:198-206, DOI 10.1007/s40879-014-0020-6.",
				"A. Claesson, V. Jelínek, and E. Steingrímsson, \u003ca href=\"http://dx.doi.org/10.1016/j.jcta.2012.05.006\"\u003eUpper bounds for the Stanley-Wilf limit of 1324 and other layered patterns\u003c/a\u003e. J. Combin. Theory Ser. A 119(8), 1680-1691 (2012).",
				"Andrew R. Conway and Anthony J. Guttmann, \u003ca href=\"http://arxiv.org/abs/1405.6802\"\u003eOn the growth rate of 1324-avoiding permutations\u003c/a\u003e, arXiv:1405.6802 [math.CO], (2014).",
				"Andrew R. Conway, Anthony J. Guttmann and Paul Zinn-Justin, \u003ca href=\"https://arxiv.org/abs/1709.01248\"\u003e1324-avoiding permutations revisited\u003c/a\u003e, arXiv preprint arXiv:1709.01248 [math.CO], 2017.",
				"Steven Finch, \u003ca href=\"http://www.people.fas.harvard.edu/~sfinch/csolve/av.pdf\"\u003ePattern-Avoiding Permutations\u003c/a\u003e [Broken link?]",
				"Steven Finch, \u003ca href=\"/A240885/a240885.pdf\"\u003ePattern-Avoiding Permutations\u003c/a\u003e [Cached copy, with permission]",
				"A. L. L. Gao, S. Kitaev, and P. B. Zhang, \u003ca href=\"https://arxiv.org/abs/1605.05490\"\u003eOn pattern avoiding indecomposable permutations\u003c/a\u003e, arXiv:1605.05490 [math.CO], 2016.",
				"Fredrik Johansson and Brian Nakamura, \u003ca href=\"http://arxiv.org/abs/1309.7117\"\u003eUsing functional equations to enumerate 1324-avoiding permutations\u003c/a\u003e, arXiv:1309.7117 [math.CO], (2013).",
				"A. Marcus and G. Tardos, \u003ca href=\"http://dx.doi.org/10.1016/j.jcta.2004.04.002\"\u003eExcluded permutation matrices and the Stanley-Wilf conjecture\u003c/a\u003e, J. Combin. Theory Ser. A 107(1), 153-160 (2004).",
				"B. K. Nakamura, \u003ca href=\"https://rucore.libraries.rutgers.edu/rutgers-lib/40633/\"\u003eComputational methods in permutation patterns\u003c/a\u003e, PhD Dissertation, Rutgers University, May 2013.",
				"Brian Nakamura and Doron Zeilberger, \u003ca href=\"http://arxiv.org/abs/1209.2353\"\u003eUsing Noonan-Zeilberger Functional Equations to enumerate (in Polynomial Time!) Generalized Wilf classes\u003c/a\u003e, arXiv preprint arXiv:1209.2353 [math.CO], 2012.",
				"Anthony Zaleski and Doron Zeilberger, \u003ca href=\"https://arxiv.org/abs/1712.10072\"\u003eOn the Intriguing Problem of Counting (n+1,n+2)-Core Partitions into Odd Parts\u003c/a\u003e, arXiv:1712.10072 [math.CO], 2017."
			],
			"example": [
				"a(4)=23 because all 24 permutations of length 4, except 1324 itself, avoid the pattern 1324."
			],
			"maple": [
				"count1324 := proc(n::nonnegint) if (n\u003c4) then return n!; fi; if (n=4) then return 23; fi; return nodes([5,5,5,5], n-5) + nodes([5,3,5,5], n-5) + nodes([5,4,4,5], n-5) + nodes([5,5,4,5], n-5) + nodes([4,3,4], n-5) + nodes([5,3,4,5], n-5); end:",
				"nodes := proc(p, h) option remember; local i, j, s, l; if (h=0) then return convert(p, `+`); fi; s := 0; for j to nops(p) do l := p[j]+1; for i from 2 to j do l := l, `min`(j+1, p[i]); od; for i from j+1 to p[j] do l := l, p[i-1]+1; od; s := s+nodes([l], h-1); od; return s; end:"
			],
			"mathematica": [
				"a[n_] := n!/;n\u003c4; a[4]=23; a[n_] := Total[nodes[#,n-5]\u0026/@{{4,3,4},{5,3,4,5},{5,3,5,5},{5,4,4,5},{5,5,4,5},{5,5,5,5}}]; nodes[p_,0]:=Total[p]; nodes[p_,h_] := nodes[p,h] = Sum[nodes[Join[{p[[j]]+1}, Min[j+1,#]\u0026/@p[[2;;j]], p[[j;;p[[j]]-1]]+1],h-1], {j,Length[p]}]; Array[a,12] (* _David Bevan_, May 25 2012 *)"
			],
			"xref": [
				"A005802, A022558, A061552 are representatives for the three Wilf classes for length-four avoiding permutations (cf. A099952)."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "Darko Marinov (marinov(AT)lcs.mit.edu), May 17 2001",
			"ext": [
				"More terms from _Vincent Vatter_, Feb 26 2005",
				"a(23)-a(25) added from the Albert et al. paper by _N. J. A. Sloane_, Mar 29 2013"
			],
			"references": 14,
			"revision": 95,
			"time": "2021-04-06T03:39:54-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}