{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001713",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1713,
			"id": "M5060 N2190",
			"data": "1,18,245,3135,40369,537628,7494416,109911300,1698920916,27679825272,474957547272,8572072384512,162478082312064,3229079010579072,67177961946534528,1460629706845766400,33139181950164806400,783398920650352012800,19268391564147377318400",
			"name": "Generalized Stirling numbers.",
			"comment": [
				"The asymptotic expansion of the higher order exponential integral E(x,m=4,n=3) ~ exp(-x)/x^4*(1 - 18/x + 245/x^2 - 3135/x^3 + 40369/x^4 - 537628/x^5 + ...) leads to the sequence given above. See A163931 and A163934 for more information. - _Johannes W. Meijer_, Oct 20 2009",
				"From _Petros Hadjicostas_, Jun 12 2020: (Start)",
				"For nonnegative integers n, m and complex numbers a, b (with b \u003c\u003e 0), the numbers R_n^m(a,b) were introduced by Mitrinovic (1961) and Mitrinovic and Mitrinovic (1962) using slightly different notation.",
				"These numbers are defined via the g.f. Product_{r=0..n-1} (x - (a + b*r)) = Sum_{m=0..n} R_n^m(a,b)*x^m for n \u003e= 0.",
				"As a result, R_n^m(a,b) = R_{n-1}^{m-1}(a,b) - (a + b*(n-1))*R_{n-1}^m(a,b) for n \u003e= m \u003e= 1 with R_0^0(a,b) = 1, R_1^0(a,b) = a, R_1^1(a,b) = 1, and R_n^m(a,b) = 0 for n \u003c m.",
				"With a = 0 and b = 1, we get the Stirling numbers of the first kind S1(n,m) = R_n^m(a=0, b=1) = A048994(n,m) for n, m \u003e= 0.",
				"We have R_n^m(a,b) = Sum_{k=0}^{n-m} (-1)^k * a^k * b^(n-m-k) * binomial(m+k, k) * S1(n, m+k) for n \u003e= m \u003e= 0.",
				"For the current sequence, a(n) = R_{n+3}^3(a=-3, b=-1) for n \u003e= 0. (End)"
			],
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A001713/b001713.txt\"\u003eTable of n, a(n) for n = 0..100\u003c/a\u003e",
				"D. S. Mitrinovic, \u003ca href=\"https://gallica.bnf.fr/ark:/12148/bpt6k762d/f996.image.r=1961%20mitrinovic\"\u003eSur une classe de nombres reliés aux nombres de Stirling\u003c/a\u003e, Comptes rendus de l'Académie des sciences de Paris, t. 252 (1961), 2354-2356. [The numbers R_n^m(a,b) are introduced.]",
				"D. S. Mitrinovic and R. S. Mitrinovic, \u003ca href=\"https://www.jstor.org/stable/43667130\"\u003eTableaux d'une classe de nombres reliés aux nombres de Stirling\u003c/a\u003e, Univ. Beograd. Publ. Elektrotehn. Fak. Ser. Mat. Fiz., No. 77 (1962), 1-77 [jstor stable version].",
				"D. S. Mitrinovic and M. S. Mitrinovic, \u003ca href=\"http://pefmath2.etf.rs/files/47/77.pdf\"\u003eTableaux d'une classe de nombres reliés aux nombres de Stirling\u003c/a\u003e, Univ. Beograd. Publ. Elektrotehn. Fak. Ser. Mat. Fiz. No. 77 (1962), 1-77."
			],
			"formula": [
				"E.g.f.: Sum_{n\u003e=0} a(n)*x^(n+3)/(n+3)! = (log(1 - x)/(x - 1))^3/6. - _Vladeta Jovovic_, May 05 2003 [Edited by _Petros Hadjicostas_, Jun 13 2020]",
				"a(n) = Sum_{k=0..n} (-1)^(n+k) * binomial(k+3, 3) * 3^k * Stirling1(n+3, k+3). - Borislav Crstici (bcrstici(AT)etv.utt.ro), Jan 26 2004",
				"If we define f(n,i,a) = Sum_{k=0..n-i} binomial(n,k) * Stirling1(n-k,i) * Product_{j=0..k-1} (-a-j), then a(n-3) = |f(n,3,3)| for n \u003e= 3. - _Milan Janjic_, Dec 21 2008",
				"From _Petros Hadjicostas_, Jun 12 2020: (Start)",
				"a(n) = [x^3] Product_{r=0}^{n+2} (x + 3 + r) = (Product_{r=0}^{n+2} (r+3)) * Sum_{0 \u003c= i \u003c j \u003c k \u003c= n+2} 1/((3+i)*(3+j)*(3+k)).",
				"Since a(n) = R_{n+3}^3(a=-3, b=-1), A001712(n) = R_{n+2}^2(a=-3,b=-1), and A001711(n) = R_{n+1}^1(a=-3, b=-1), the equation R_{n+3}^3(a=-3,b=-1) = R_{n+2}^2(a=-3,b=-1) + (n+5)*R_{n+2}^3(a=-3,b=-1) implies the following:",
				"(i) a(n) = A001712(n) + (n+5)*a(n-1) for n \u003e= 1.",
				"(ii) a(n) = A001711(n) + (2*n+9)*a(n-1) - (n+4)^2*a(n-2) for n \u003e= 2.",
				"(iii) a(n) = (n+2)!/2 + 3*(n+4)*a(n-1) - (3*n^2+21*n+37)*a(n-2) + (n+3)^3*a(n-3) for n \u003e= 3.",
				"(iv) a(n) = 2*(2*n+7)*a(n-1) - (6*n^2+36*n+55)*a(n-2) + (2*n^2+10*n+13)*(2*n+5)*a(n-3) - (n+2)^4*a(n-4) for n \u003e= 4. (End)"
			],
			"mathematica": [
				"nn = 23; t = Range[0, nn]! CoefficientList[Series[-Log[1 - x]^3/(6*(1 - x)^3), {x, 0, nn}], x]; Drop[t, 3] (* _T. D. Noe_, Aug 09 2012 *)"
			],
			"program": [
				"(PARI) a(n) = sum(k=0, n, (-1)^(n+k)*binomial(k+3, 3)*3^k*stirling(n+3, k+3, 1)); \\\\ _Michel Marcus_, Jan 20 2016",
				"(PARI) b(n) = prod(r=0, n+2, r+3);",
				"c(n) = sum(i=0, n+2, sum(j=i+1, n+2, sum(k=j+1, n+2, 1/((3+i)*(3+j)*(3+k)))));",
				"for(n=0, 18, print1(b(n)*c(n), \", \")) \\\\ _Petros Hadjicostas_, Jun 12 2020"
			],
			"xref": [
				"Cf. A000254, A001706, A001711, A001712, A001719, A048994."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Vladeta Jovovic_, May 05 2003"
			],
			"references": 5,
			"revision": 63,
			"time": "2020-06-26T13:58:50-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}