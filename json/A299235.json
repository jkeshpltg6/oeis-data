{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A299235",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 299235,
			"data": "1,0,2,1,1,3,0,2,2,2,1,1,4,3,1,0,3,2,3,2,3,2,2,1,2,1,5,4,4,3,2,1,1,0,4,3,3,2,4,3,3,2,4,3,3,2,3,2,2,1,3,2,2,1,6,5,5,4,5,4,4,3,3,2,2,1,2,1,1,0,5,4,4,3,4,3,3,2,5,4,4,3,4,3,3,2",
			"name": "Number of 2's in the n-th {2,3}-power tower; see Comments.",
			"comment": [
				"Suppose that S is a set of real numbers.  As S-power-tower, t, is a number t = x(1)^x(2)^...^x(k), where k \u003e= 1 and x(i) is in S for i = 1..k.  We represent t by (x(1),x(2),...,x(k), which for k \u003e 1 is defined as (x(1),((x(2),...,x(k-1)); (2,3,2) means 2^9.  The number k is the *height* of t.  If every element of S exceeds 1 and all the power towers are ranked in increasing order, the position of each in the resulting sequence is its *rank*.  See A299229 for a guide to related sequences.",
				"Every nonnegative integer occurs infinitely many times in the sequence.  In particular, a(n) = 0 when the tower consists exclusively of 3's.  The position of the n-th 0 in the sequence is the rank of the n-th {3}-power tower, given by 9*2^(n-2)-2 for n \u003e 1."
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A299235/b299235.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"t(80) = (3,2,2,2,2,3), so that a(80) = 4."
			],
			"mathematica": [
				"t[1] = {2}; t[2] = {3}; t[3] = {2, 2}; t[4] = {2, 3}; t[5] = {3, 2};",
				"t[6] = {2, 2, 2}; t[7] = {3, 3}; t[8] = {3, 2, 2}; t[9] = {2, 2, 3};",
				"t[10] = {2, 3, 2}; t[11] = {3, 2, 3}; t[12] = {3, 3, 2};",
				"z = 190; g[k_] := If[EvenQ[k], {2}, {3}]; f = 6;",
				"While[f \u003c 13, n = f; While[n \u003c z, p = 1;",
				"  While[p \u003c 12, m = 2 n + 1; v = t[n]; k = 0;",
				"    While[k \u003c 2^p, t[m + k] = Join[g[k], t[n + Floor[k/2]]]; k = k + 1];",
				"   p = p + 1; n = m]]; f = f + 1]",
				"Table[Count[t[n], 2], {n, 1, 100}];  (* A299235 *)",
				"Table[Count[t[n], 3], {n, 1, 100}];  (* A299236 *)"
			],
			"xref": [
				"Cf. A299229, A299236 (complement)."
			],
			"keyword": "nonn,easy",
			"offset": "1,3",
			"author": "_Clark Kimberling_, Feb 06 2018",
			"references": 3,
			"revision": 6,
			"time": "2018-02-07T13:21:48-05:00",
			"created": "2018-02-07T13:21:48-05:00"
		}
	]
}