{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A156725",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 156725,
			"data": "1,1,1,1,-2,1,1,22,22,1,1,-440,4840,-440,1,1,12760,2807200,2807200,12760,1,1,-484880,3093534400,-61870688000,3093534400,-484880,1,1,22789360,5525052438400,3204530414272000,3204530414272000,5525052438400,22789360,1",
			"name": "Triangle T(n, k, m) = t(n,m)/( t(k,m) * t(n-k,m) ) with T(n, 0, m) = T(n, n, m) = 1, where t(n, m) = Product_{j=1..n} Product_{i=1..j-1} ( 1 - (m+1)*(3*i-2) ) and m = 2, read by rows.",
			"comment": [
				"Row sums are: {1, 2, 0, 46, 3962, 5639922, -55684588958, 6420110978999522, 8653645559546848833282, 120959123027642635275104364802, ...}."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A156725/b156725.txt\"\u003eRows n = 0..30 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k, m) = t(n,m)/( t(k,m) * t(n-k,m) ) with T(n, 0, m) = T(n, n, m) = 1, where t(n, m) = Product_{j=1..n} Product_{i=1..j-1} ( 1 - (m+1)*(3*i-2) ) and m = 2.",
				"T(n, k, m, p, q) = (-p*(m+1))^(k*(n-k)) * (f(n,m,p,q)/(f(k,m,p,q)*f(n-k,m,p,q))) where Product_{j=1..n} Pochhammer( (q*(m+1) -1)/(p*(m+1)), j) for (m, p, q) = (2, 3, -2). - _G. C. Greubel_, Feb 26 2021"
			],
			"example": [
				"Triangle begins as:",
				"  1;",
				"  1,       1;",
				"  1,      -2,          1;",
				"  1,      22,         22,            1;",
				"  1,    -440,       4840,         -440,          1;",
				"  1,   12760,    2807200,      2807200,      12760,       1;",
				"  1, -484880, 3093534400, -61870688000, 3093534400, -484880, 1;"
			],
			"mathematica": [
				"(* First program *)",
				"t[n_, k_]:= If[k==0, n!, Product[1 -(3*i-2)*(k+1), {j,n}, {i,0,j-1}] ];",
				"T[n_, k_, m_]:= If[n==0, 1, t[n, m]/(t[k, m]*t[n-k, m])];",
				"Table[T[n,k,2], {n,0,12}, {k,0,n}]//Flatten (* modified by _G. C. Greubel_, Feb 26 2021 *)",
				"(* Second program *)",
				"f[n_, m_, p_, q_]:= Product[Pochhammer[(q*(m+1) -1)/(p*(m+1)), j], {j,n}];",
				"T[n_, k_, m_, p_, q_]:= (-p*(m+1))^(k*(n-k))*(f[n,m,p,q]/(f[k,m,p,q]*f[n-k,m,p,q]));",
				"Table[T[n,k,2,3,-2], {n,0,12}, {k,0,n}]//Flatten (* _G. C. Greubel_, Feb 26 2021 *)"
			],
			"program": [
				"(Sage)",
				"@CachedFunction",
				"def f(n, m, p, q): return product( rising_factorial( (q*(m+1)-1)/(p*(m+1)), j) for j in (1..n))",
				"def T(n, k, m, p, q): return (-p*(m+1))^(k*(n-k))*(f(n,m,p,q)/(f(k,m,p,q)*f(n-k,m,p,q)))",
				"flatten([[T(n,k,2,3,-2) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, Feb 26 2021",
				"(Magma)",
				"f:= func\u003c n,m,p,q | n eq 0 select 1 else m eq 0 select Factorial(n) else (\u0026*[ 1 -(p*i+q)*(m+1): i in [0..j], j in [0..n-1]]) \u003e;",
				"T:= func\u003c n,k,m,p,q | f(n,m,p,q)/(f(k,m,p,q)*f(n-k,m,p,q)) \u003e;",
				"[T(n,k,2,3,-2): k in [0..n], n in [0..12]]; // _G. C. Greubel_, Feb 26 2021"
			],
			"xref": [
				"Cf. A007318 (m=0), A156722 (m=1), this sequence (m=2), A156727 (m=3).",
				"Cf. A156691, A156697, A156730."
			],
			"keyword": "sign,tabl",
			"offset": "0,5",
			"author": "_Roger L. Bagula_, Feb 14 2009",
			"ext": [
				"Edited by _G. C. Greubel_, Feb 26 2021"
			],
			"references": 6,
			"revision": 5,
			"time": "2021-02-26T20:14:23-05:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}