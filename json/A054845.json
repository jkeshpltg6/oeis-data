{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A054845",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 54845,
			"data": "0,0,1,1,0,2,0,1,1,0,1,1,1,1,0,1,0,2,1,1,0,0,0,2,1,0,1,0,1,1,1,2,0,0,0,0,2,1,0,1,0,3,1,1,0,0,0,1,1,1,0,0,1,2,0,0,1,0,1,2,2,1,0,0,0,0,0,2,1,0,0,2,2,1,0,1,0,1,1,1,0,0,0,3,1,0,0,0,1,1,2,0,0,0,0,1,0,2,1,0,2,2",
			"name": "Number of ways of representing n as the sum of one or more consecutive primes.",
			"comment": [
				"Moser shows that the average order of a(n) is log 2, that is, sum(i=1..n, a(i)) ~ n log 2. This shows that a(n) = 0 infinitely often (and with positive density); Moser asks if a(n) = 1 infinitely often, if a(n) = k is solvable for all k, whether these have positive density, and whether the sequence is bounded. - _Charles R Greathouse IV_, Mar 21 2011"
			],
			"reference": [
				"R. K. Guy, Unsolved Problems In Number Theory, C2.",
				"Leo Moser, Notes on number theory. III. On the sum of consecutive primes, Canad. Math. Bull. 6 (1963), pp. 159-161."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A054845/b054845.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"C. Rivera, \u003ca href=\"http://www.primepuzzles.net/problems/prob_009.htm\"\u003eProblem 9\u003c/a\u003e, Prime Puzzles."
			],
			"formula": [
				"G.f.: Sum_{i\u003e=1} Sum_{j\u003e=i} Product_{k=i..j} x^prime(k). - _Ilya Gutkovskiy_, Apr 18 2019"
			],
			"example": [
				"a(5)=2 because of 2+3 and 5. a(17)=2 because of 2+3+5+7 and 17.",
				"41 = 41 = 11+13+17 = 2+3+5+7+11+13, so a(41)=3."
			],
			"maple": [
				"A054845 := proc(n)",
				"    local a,mipri,npr,ps ;",
				"    a := 0 ;",
				"    for mipri from 1 do",
				"        for npr from 1 do",
				"            ps := add(ithprime(i),i=mipri..mipri+npr-1) ;",
				"            if ps = n then",
				"                a := a+1 ;",
				"            elif ps \u003en then",
				"                break;",
				"            end if;",
				"        end do:",
				"        if ithprime(mipri) \u003e n then",
				"            break ;",
				"        end if;",
				"    end do:",
				"    a ;",
				"end proc: # _R. J. Mathar_, Nov 27 2018"
			],
			"mathematica": [
				"f[n_] := Block[{p = Prime@ Range@ PrimePi@ n}, len = Length@ p; Count[(Flatten[#, 1] \u0026)[Table[ p[[i ;; j]], {i, len}, {j, i, len}]], q_ /; Total@ q == n]]; f[0] = 0; Array[f, 102, 0](* _Jean-François Alcover_, Feb 16 2011 *) (* fixed by _Robert G. Wilson v_ *)",
				"nn=100; p=Prime[Range[PrimePi[nn]]]; t=Table[0,{nn}]; Do[s=0; j=i; While[s=s+p[[j]]; s\u003c=nn,t[[s]]++; j++], {i,Length[p]}]; Join[{0},t]"
			],
			"program": [
				"(PARI){/* program gives nn+1 values of a(n) for n=0..nn */",
				"nn=2000;t=vector(nn+1);forprime(x=2,nn,s=x;",
				"  forprime(y=x+1,nn,if(s\u003c=nn,t[s+1]++,break());s=s+y));t} \\\\ _Zak Seidov_, Feb 17 2011",
				"(MAGMA) S:=[0]; for n in [1..104] do count:=0; for q in PrimesUpTo(n) do p:=q; s:=p; while s lt n do p:=NextPrime(p); s+:=p; end while; if s eq n then count+:=1; end if; end for; Append(~S, count); end for; S; // _Klaus Brockhaus_, Feb 17 2011",
				"(Perl) use ntheory \":all\"; my $n=10000; my @W=(0)x($n+1); forprimes { my $s=$_; do { $W[$s]++; $s += ($_=next_prime($_)); } while $s \u003c= $n; } $n; print \"$_ $W[$_]\\n\" for 0..$#W;  # _Dana Jacobsen_, Aug 22 2018"
			],
			"xref": [
				"Cf. A000586, A054859."
			],
			"keyword": "nice,nonn",
			"offset": "0,6",
			"author": "_Jud McCranie_, May 25 2000",
			"ext": [
				"Edited by _N. J. A. Sloane_, Oct 27 2008 at the suggestion of Jake M. Foster"
			],
			"references": 15,
			"revision": 47,
			"time": "2020-05-15T13:56:18-04:00",
			"created": "2000-06-15T03:00:00-04:00"
		}
	]
}