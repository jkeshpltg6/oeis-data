{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A158830",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 158830,
			"data": "1,1,0,2,0,0,5,1,0,0,14,10,0,0,0,42,70,8,0,0,0,132,424,160,4,0,0,0,429,2382,1978,250,1,0,0,0,1430,12804,19508,6276,302,0,0,0,0,4862,66946,168608,106492,15674,298,0,0,0,0,16796,343772,1337684,1445208,451948",
			"name": "Triangle, read by rows n\u003e=1, where row n is the n-th differences of column n of array A158825, where the g.f. of row n of A158825 is the n-th iteration of x*Catalan(x).",
			"link": [
				"Paul D. Hanna, \u003ca href=\"/A158830/b158830.txt\"\u003eTable of n, a(n), n = 1..1326 (rows 1..51).\u003c/a\u003e",
				"Toufik Mansour, Howard Skogman, Rebecca Smith, \u003ca href=\"https://arxiv.org/abs/1704.04288\"\u003ePassing through a stack k times\u003c/a\u003e, arXiv:1704.04288 [math.CO], 2017."
			],
			"formula": [
				"Row sums equal the factorial numbers.",
				"G.f. of row n = (1-x)^n*[g.f. of column n of A158825] where the g.f. of row n of array A158825 is the n-th iteration of x*C(x) and C(x) is the g.f. of the Catalan sequence A000108.",
				"Row-reversal is triangle A122890 where g.f. of row n of A122890 = (1-x)^n*[g.f. of column n of A122888], and the g.f. of row n of array A122888 is the n-th iteration of x+x^2."
			],
			"example": [
				"Triangle begins:",
				".1;",
				".1,0;",
				".2,0,0;",
				".5,1,0,0;",
				".14,10,0,0,0;",
				".42,70,8,0,0,0;",
				".132,424,160,4,0,0,0;",
				".429,2382,1978,250,1,0,0,0;",
				".1430,12804,19508,6276,302,0,0,0,0;",
				".4862,66946,168608,106492,15674,298,0,0,0,0;",
				".16796,343772,1337684,1445208,451948,33148,244,0,0,0,0;",
				".58786,1744314,10003422,16974314,9459090,1614906,61806,162,0,0,0,0;",
				".208012,8780912,71692452,180308420,161380816,51436848,5090124,103932,84,0,0,0,0;",
				"....",
				"where the g.f. of row n is (1-x)^n*[g.f. of column n of A158825];",
				"g.f. of row n of array A158825 is the n-th iteration of x*C(x):",
				".1,1,2,5,14,42,132,429,1430,4862,16796,58786,208012,742900,...;",
				".1,2,6,21,80,322,1348,5814,25674,115566,528528,2449746,...;",
				".1,3,12,54,260,1310,6824,36478,199094,1105478,6227712,...;",
				".1,4,20,110,640,3870,24084,153306,993978,6544242,43652340,...;",
				".1,5,30,195,1330,9380,67844,500619,3755156,28558484,...;",
				".1,6,42,315,2464,19852,163576,1372196,11682348,100707972,...;",
				".1,7,56,476,4200,38052,351792,3305484,31478628,303208212,...;",
				".1,8,72,684,6720,67620,693048,7209036,75915708,807845676,...;",
				"....",
				"ROW-REVERSAL yields triangle A122890:",
				".1;",
				".0,1;",
				".0,0,2;",
				".0,0,1,5;",
				".0,0,0,10,14;",
				".0,0,0,8,70,42;",
				".0,0,0,4,160,424,132;",
				".0,0,0,1,250,1978,2382,429;",
				".0,0,0,0,302,6276,19508,12804,1430; ...",
				"where g.f. of row n = (1-x)^n*[g.f. of column n of A122888];",
				"g.f. of row n of A122888 is the n-th iteration of x+x^2:",
				".1;",
				".1,1;",
				".1,2,2,1;",
				".1,3,6,9,10,8,4,1;",
				".1,4,12,30,64,118,188,258,302,298,244,162,84,32,8,1; ..."
			],
			"mathematica": [
				"nmax = 11;",
				"f[0][x_] := x; f[n_][x_] := f[n][x] = f[n - 1][x + x^2] // Expand;",
				"T = Table[SeriesCoefficient[f[n][x], {x, 0, k}], {n, 0, nmax}, {k, 1, nmax}];",
				"row[n_] := CoefficientList[(1-x)^n*(T[[All, n]].x^Range[0, nmax])+O[x]^nmax, x] // Reverse;",
				"Table[row[n], {n, 1, nmax}] // Flatten (* _Jean-François Alcover_, Oct 26 2018 *)"
			],
			"program": [
				"(PARI) {T(n, k)=local(F=x, CAT=serreverse(x-x^2+x*O(x^(n+2))), M, N, P); M=matrix(n+2, n+2, r, c, F=x; for(i=1, r, F=subst(F, x, CAT)); polcoeff(F, c)); Vec(truncate(Ser(vector(n+1,r,M[r,n+1])))*(1-x)^(n+1) +x*O(x^k))[k+1]}"
			],
			"xref": [
				"Cf. A158825, A122890 (row-reversal), A122888, columns: A000108, A122892."
			],
			"keyword": "nonn,tabl",
			"offset": "1,4",
			"author": "_Paul D. Hanna_, Mar 28 2009",
			"ext": [
				"Edited by _N. J. A. Sloane_, Oct 04 2010, to make entries, offset, b-file and link to b-file all consistent."
			],
			"references": 4,
			"revision": 17,
			"time": "2018-10-26T11:47:13-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}