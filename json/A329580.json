{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A329580",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 329580,
			"data": "0,1,2,3,4,5,6,90,7,11,8,9,10,12,13,30,29,31,14,16,15,17,22,42,19,25,18,24,20,23,28,33,43,35,36,38,26,21,32,27,34,71,37,39,40,44,63,64,68,41,46,183,50,45,333,51,98,47,58,62,69,65,48,101,66,49,61,78,57,53,180,52,55,96,631,54,56,83,75,95,74,116,60",
			"name": "For every n \u003e= 0, exactly 10 sums are prime among a(n+i) + a(n+j), 0 \u003c= i \u003c j \u003c 8: lexicographically earliest such sequence of distinct nonnegative numbers.",
			"comment": [
				"That is, there are 10 primes, counted with multiplicity, among the 28 pairwise sums of any 8 consecutive terms.",
				"Is this a permutation of the nonnegative integers?",
				"If so, then the restriction to [1..oo) is a permutation of the positive integers, but not the lexicographically earliest one with this property, which starts (1, 2, 3, 4, 5, 6, 7, 19, 10, 8, 9, 12, 11, 18, 13, 29, ...).",
				"We remark the surprisingly large numbers 333 and 631 among the first terms."
			],
			"example": [
				"In P(7) := {0, 1, 2, 3, 4, 5, 6} there are already S(7) := 10 primes 0+2, 0+3, 0+5, 1+2, 1+4, 1+6, 2+3, 2+5, 3+4, 5+6 among the pairwise sums, so the next term a(7) must not produce any more primes when added to elements of P(7). We find that a(7) = 90 is the smallest possible term.",
				"Then in P(8) = {1, 2, 3, 4, 5, 6, 90} there are S(8) = 7 primes among the pairwise sums, so a(8) must produce 3 more primes when added to elements of P(8). We find a(8) = 7 is the smallest possibility (with 4+7, 6+7 and 90+7).",
				"And so on."
			],
			"program": [
				"(PARI) A329580(n,show=0,o=0,N=10,M=7,p=[],U,u=o)={for(n=o,n-1, if(show\u003e0,print1(o\", \"), show\u003c0,listput(L,o)); U+=1\u003c\u003c(o-u); U\u003e\u003e=-u+u+=valuation(U+1,2); p=concat(if(#p\u003e=M,p[^1],p),o); my(c=N-sum(i=2,#p, sum(j=1,i-1, isprime(p[i]+p[j]))));if(#p\u003cM\u0026\u0026sum(i=1,#p,isprime(p[i]+u))\u003c=c,o=u)|| for(k=u,oo,bittest(U,k-u)|| sum(i=1,#p,isprime(p[i]+k))!=c||[o=k,break]));show\u0026\u0026print([u]);o} \\\\ optional args: show=1: print a(o..n-1), show=-1: append them on global list L, in both cases print [least unused number] at the end; o=1: start at a(1)=1; N, M: find N primes using M+1 terms"
			],
			"xref": [
				"Cf. A329579 (9 primes using 7 consecutive terms), A329425 (6 primes using 5 consecutive terms).",
				"Cf. A329455 (4 primes using 5 consecutive terms), A329455 (3 primes using 5 consecutive terms), A329453 (2 primes using 5 consecutive terms), A329452 (2 primes using 4 consecutive terms).",
				"Cf. A329577 (7 primes using 7 consecutive terms), A329566 (6 primes using 6 consecutive terms), A329449 (4 primes using 4 consecutive terms).",
				"Cf. A329454 (3 primes using 4 consecutive terms), A329411 (2 primes using 3 consecutive terms), A329333 (1 odd prime using 3 terms), A329450 (0 primes using 3 terms).",
				"Cf. A329405 ff: other variants defined for positive integers."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_M. F. Hasler_, Nov 17 2019",
			"references": 4,
			"revision": 10,
			"time": "2019-11-28T11:03:18-05:00",
			"created": "2019-11-17T09:34:38-05:00"
		}
	]
}