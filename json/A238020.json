{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A238020",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 238020,
			"data": "1,1,1,1,2,2,4,5,10,15,33,52,126,213,537,991,2563,5118,13670,29171,81069,180813,525755,1216996,3693934,8843831,27797975,69106326,223116931,577433770,1903516721,5136516772,17257698892,48388514996,166022450140,481137194184",
			"name": "Number of nonconsecutive chess tableaux with n cells.",
			"comment": [
				"A standard Young tableau (SYT) with cell(i,j)+i+j == 1 mod 2 for all cells where entries m and m+1 never appear in the same row is called a nonconsecutive chess tableau."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A238020/b238020.txt\"\u003eTable of n, a(n) for n = 0..50\u003c/a\u003e",
				"T. Y. Chow, H. Eriksson and C. K. Fan, \u003ca href=\"http://www.combinatorics.org/Volume_11/Abstracts/v11i2a3.html\"\u003eChess tableaux\u003c/a\u003e, Elect. J. Combin., 11 (2) (2005), #A3.",
				"Jonas Sjöstrand, \u003ca href=\"https://arxiv.org/abs/math/0309231v3\"\u003eOn the sign-imbalance of partition shapes\u003c/a\u003e, arXiv:math/0309231v3 [math.CO], 2005.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Young_tableau\"\u003eYoung tableau\u003c/a\u003e"
			],
			"example": [
				"a(6) = 4:",
				"[1]   [1 6]   [1 4]   [1 4]",
				"[2]   [2]     [2 5]   [2 5]",
				"[3]   [3]     [3]     [3 6]",
				"[4]   [4]     [6]",
				"[5]   [5]",
				"[6]"
			],
			"maple": [
				"b:= proc(l, t) option remember; local n, s;",
				"      n, s:= nops(l), add(i, i=l); `if`(s=0, 1, add(`if`(t\u003c\u003ei and",
				"      irem(s+i-l[i], 2)=1 and l[i]\u003e`if`(i=n, 0, l[i+1]), b(subsop(",
				"      i=`if`(i=n and l[n]=1, [][], l[i]-1), l), i), 0), i=1..n))",
				"    end:",
				"g:= (n, i, l)-\u003e `if`(n=0 or i=1, b([l[], 1$n], 0), `if`(i\u003c1, 0,",
				"                 add(g(n-i*j, i-1, [l[], i$j]), j=0..n/i))):",
				"a:= n-\u003e g(n, n, []):",
				"seq(a(n), n=0..32);"
			],
			"mathematica": [
				"b[l_, t_] := b[l, t] = Module[{ n = Length[l], s = Total[l]}, If[s == 0, 1, Sum[If[t != i \u0026\u0026 Mod[s + i - l[[i]], 2] == 1 \u0026\u0026 l[[i]] \u003e If[i == n, 0, l[[i + 1]]], b[ReplacePart[l, i -\u003e If[i == n \u0026\u0026 l[[n]] == 1, Nothing, l[[i]] - 1]], i], 0], {i, 1, n}]]];",
				"g[n_, i_, l_] := If[n == 0 || i == 1, b[Join[l, Table[1, n]], 0], If[i \u003c 1, 0, Sum[g[n - i*j, i - 1, Join[l, Table[i, j]]], {j, 0, n/i}]]];",
				"a[n_] := g[n, n, {}];",
				"Table[a[n], {n, 0, 32}] (* _Jean-François Alcover_, Nov 08 2017, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A214088, A214459, A214460, A214461, A237770, A238014, A238184."
			],
			"keyword": "nonn",
			"offset": "0,5",
			"author": "_Alois P. Heinz_, Feb 17 2014",
			"references": 3,
			"revision": 22,
			"time": "2017-11-08T10:26:52-05:00",
			"created": "2014-02-23T19:36:05-05:00"
		}
	]
}