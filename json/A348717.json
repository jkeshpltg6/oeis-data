{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A348717",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 348717,
			"data": "1,2,2,4,2,6,2,8,4,10,2,12,2,14,6,16,2,18,2,20,10,22,2,24,4,26,8,28,2,30,2,32,14,34,6,36,2,38,22,40,2,42,2,44,12,46,2,48,4,50,26,52,2,54,10,56,34,58,2,60,2,62,20,64,14,66,2,68,38,70,2,72,2",
			"name": "a(n) is the least k such that A003961^i(k) = n for some i \u003e= 0 (where A003961^i denotes the i-th iterate of A003961).",
			"comment": [
				"All terms except a(1) = 1 are even.",
				"To compute a(n) for n \u003e 1:",
				"- if n = Product_{j = 1..o} prime(p_j)^e_j (where prime(i) denotes the i-th prime number, p_1 \u003c ... \u003c p_o and e_1 \u003e 0)",
				"- then a(n) = Product_{j = 1..o} prime(p_j + 1 - p_1)^e_j.",
				"This sequence has similarities with A304776: here we shift down prime indexes, there prime exponents."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A348717/b348717.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = n iff n belongs to A004277.",
				"A003961^(A055396(n)-1)(a(n)) = n for any n \u003e 1.",
				"a(n) = 2 iff n belongs to A000040 (prime numbers).",
				"a(n) = 4 iff n belongs to A001248 (squares of prime numbers).",
				"a(n) = 6 iff n belongs to A006094 (products of two successive prime numbers).",
				"a(n) = 8 iff n belongs to A030078 (cubes of prime numbers).",
				"a(n) = 10 iff n belongs to A090076.",
				"a(n) = 12 iff n belongs to A251720.",
				"a(n) = 14 iff n belongs to A090090.",
				"a(n) = 16 iff n belongs to A030514.",
				"a(n) = 30 iff n belongs to A046301.",
				"a(n) = 32 iff n belongs to A050997.",
				"a(n) = 36 iff n belongs to A166329."
			],
			"mathematica": [
				"a[1] = 1; a[n_] := Module[{f = FactorInteger[n], d}, d = PrimePi[f[[1, 1]]] - 1; Times @@ ((Prime[PrimePi[#[[1]]] - d]^#[[2]]) \u0026 /@ f)]; Array[a, 100] (* _Amiram Eldar_, Oct 31 2021 *)"
			],
			"program": [
				"(PARI) a(n) = { my (f=factor(n)); if (#f~\u003e0, my (pi1=primepi(f[1,1])); for (k=1, #f~, f[k,1] = prime(primepi(f[k,1])-pi1+1))); factorback(f) }"
			],
			"xref": [
				"Cf. A000040, A001248, A003961, A004277, A006094, A030078, A030514, A046301, A050997, A055396, A090076, A090090, A166329, A251720, A304776."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Rémy Sigrist_, Oct 31 2021",
			"references": 1,
			"revision": 14,
			"time": "2021-11-01T03:22:22-04:00",
			"created": "2021-11-01T00:38:16-04:00"
		}
	]
}