{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A112798",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 112798,
			"data": "1,2,1,1,3,1,2,4,1,1,1,2,2,1,3,5,1,1,2,6,1,4,2,3,1,1,1,1,7,1,2,2,8,1,1,3,2,4,1,5,9,1,1,1,2,3,3,1,6,2,2,2,1,1,4,10,1,2,3,11,1,1,1,1,1,2,5,1,7,3,4,1,1,2,2,12,1,8,2,6,1,1,1,3,13,1,2,4,14,1,1,5,2,2,3,1,9,15,1,1,1,1",
			"name": "Table where n-th row is factorization of n, with each prime p_i replaced by i.",
			"comment": [
				"This is an enumeration of all partitions.",
				"Technically this is an enumeration of all multisets (finite weakly increasing sequences of positive integers) rather than integer partitions. - _Gus Wiseman_, Dec 12 2016",
				"A000040(a(n)) is a prime factor of A082288(n). - _Reinhard Zumkeller_, Feb 03 2008",
				"Row n is the partition with Heinz number n. We define the Heinz number of a partition p = [p_1, p_2, ..., p_r] as Product(p_j-th prime, j=1..r) (concept used by _Alois P. Heinz_ in A215366 as an \"encoding\" of a partition). For example, for the partition [1, 1, 2, 4, 10] we get 2*2*3*7*29 = 2436. For a given n, the 2nd Maple program yields row n; for example, we obtain at once B(2436) = [1,1,2,4,10]. - _Emeric Deutsch_, Jun 04 2015",
				"From _Emeric Deutsch_, May 05 2015: (Start)",
				"Number of entries in row n is bigomega(n) (i.e., the number of prime factors of n, multiplicities included).",
				"Product of entries in row n = A003963(n).",
				"Row n contains the Matula numbers of the rooted trees obtained from the rooted tree with Matula number n by deleting the edges emanating from the root. Example: row 8 is 1,1,1; indeed the rooted tree with Matula number 8 is \\|/ and deleting the edges emanating from the root we obtain the one-vertex trees ..., having Matula numbers 1, 1, 1. Example: row 7 is 4; indeed, the rooted tree with Matula number 7 is Y and deleting the edges emanating from the root we obtain the rooted tree V, having Matula number 4.",
				"The Matula (or Matula-Goebel) number of a rooted tree can be defined in the following recursive manner: to the one-vertex tree there corresponds the number 1; to a tree T with root degree 1 there corresponds the t-th prime number, where t is the Matula-Goebel number of the tree obtained from T by deleting the edge emanating from the root; to a tree T with root degree m \u003e= 2 there corresponds the product of the Matula-Goebel numbers of the m branches of T. (End)"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A112798/b112798.txt\"\u003eRows n = 2..3275, flattened\u003c/a\u003e",
				"E. Deutsch, \u003ca href=\"http://arxiv.org/abs/1111.4288\"\u003eRooted tree statistics from Matula numbers\u003c/a\u003e, arXiv:1111.4288 [math.CO], 2011.",
				"E. Deutsch, \u003ca href=\"http://dx.doi.org/10.1016/j.dam.2012.05.012\"\u003eRooted tree statistics from Matula numbers\u003c/a\u003e, Discrete Appl. Math., 160, 2012, 2314-2322.",
				"F. Goebel, \u003ca href=\"http://dx.doi.org/10.1016/0095-8956(80)90049-0\"\u003eOn a 1-1-correspondence between rooted trees and natural numbers\u003c/a\u003e, J. Combin. Theory, B 29 (1980), 141-143.",
				"I. Gutman and A. Ivic, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(95)00182-V\"\u003eOn Matula numbers\u003c/a\u003e, Discrete Math., 150, 1996, 131-142.",
				"I. Gutman and Yeong-Nan Yeh, \u003ca href=\"http://www.emis.de/journals/PIMB/067/3.html\"\u003eDeducing properties of trees from their Matula numbers\u003c/a\u003e, Publ. Inst. Math., 53 (67), 1993, 17-22.",
				"D. W. Matula, \u003ca href=\"http://www.jstor.org/stable/2027327\"\u003eA natural rooted tree enumeration by prime factorization\u003c/a\u003e, SIAM Rev. 10 (1968) 273.",
				"\u003ca href=\"/index/Mat#matula\"\u003eIndex entries for sequences related to Matula-Goebel numbers\u003c/a\u003e",
				"\u003ca href=\"/index/Pri#prime_indices\"\u003eIndex entries for sequences computed from indices in prime factorization\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = A000720(A027746(n,k)); A027746(n,k) = A000040(T(n,k)).",
				"Also T(n,k) = A049084(A027746(n,k)). - _Reinhard Zumkeller_, Aug 04 2014"
			],
			"example": [
				"Row 20 is 1,1,3 because the prime factors of 20, namely 2,2,5 are the 1st, 1st, 3rd primes.",
				"Table begins:",
				"1;",
				"2;",
				"1, 1;",
				"3;",
				"1, 2;",
				"4;",
				"1, 1, 1;",
				"The sequence of all finite multisets of positive integers begins: (), (1), (2), (11), (3), (12), (4), (111), (22), (13), (5), (112), (6), (14), (23), (1111), (7), (122), (8), (113), (24), (15), (9), (1112), (33), (16), (222), (114). - _Gus Wiseman_, Dec 12 2016"
			],
			"maple": [
				"T:= n-\u003e sort([seq(numtheory[pi](i[1])$i[2], i=ifactors(n)[2])])[]:",
				"seq(T(n), n=2..50);  # _Alois P. Heinz_, Aug 09 2012",
				"with(numtheory): B := proc (n) local nn, j, m: nn := op(2, ifactors(n)); for j to nops(nn) do m[j] := op(j, nn) end do: [seq(seq(pi(op(1, m[i])), q = 1 .. op(2, m[i])), i = 1 .. nops(nn))] end proc: # _Emeric Deutsch_, Jun 04 2015. (This is equivalent to the first Maple program.)"
			],
			"mathematica": [
				"PrimePi /@ Flatten[Table[#1, {#2}] \u0026 @@@ FactorInteger@ #] \u0026 /@ Range@ 60 // Flatten // Rest (* _Michael De Vlieger_, May 09 2015 *)"
			],
			"program": [
				"(Haskell)",
				"a112798 n k = a112798_tabf !! (n-2) !! (n-1)",
				"a112798_row n = a112798_tabf !! (n-2)",
				"a112798_tabf = map (map a049084) $ tail a027746_tabf",
				"-- _Reinhard Zumkeller_, Aug 04 2014",
				"(PARI) row(n)=my(v=List(),f=factor(n)); for(i=1,#f~,for(j=1,f[i,2], listput(v,primepi(f[i,1])))); Vec(v) \\\\ _Charles R Greathouse IV_, Nov 09 2021"
			],
			"xref": [
				"Row lengths are A001222. Cf. A000040, A027746, A000720, A036036.",
				"Cf. A056239 (row sums).",
				"Cf. A003963 (row products).",
				"Cf. A049084, A215366, A241918, A265146, A275024."
			],
			"keyword": "nonn,tabf",
			"offset": "2,2",
			"author": "_Franklin T. Adams-Watters_, Jan 22 2006",
			"references": 1018,
			"revision": 64,
			"time": "2021-11-09T13:50:23-05:00",
			"created": "2006-01-24T03:00:00-05:00"
		}
	]
}