{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A330102",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 330102,
			"data": "0,1,1,3,4,5,5,7,1,3,3,11,33,19,19,15,4,5,33,19,20,21,37,23,5,7,19,15,37,23,51,31,4,33,5,19,20,37,21,23,5,19,7,15,37,51,23,31,20,37,37,51,52,53,53,55,21,23,23,31,53,55,55,63,64,65,65,67,68,69,69",
			"name": "BII-number of the VDD-normalization of the set-system with BII-number n.",
			"comment": [
				"First differs from A330101 at a(148) = 274, A330101(148) = 545, with corresponding set-systems 274: {{2},{1,3},{1,4}} and 545: {{1},{2,3},{2,4}}.",
				"A set-system is a finite set of finite nonempty sets of positive integers.",
				"We define the VDD (vertex-degrees decreasing) normalization of a set-system to be obtained by first normalizing so that the vertices cover an initial interval of positive integers, then applying all permutations to the vertex set, then selecting only the representatives whose vertex-degrees are weakly decreasing, and finally taking the least of these representatives, where the ordering of sets is first by length and then lexicographically.",
				"For example, 156 is the BII-number of {{3},{4},{1,2},{1,3}}, which has the following normalizations, together with their BII-numbers:",
				"  Brute-force:    2067: {{1},{2},{1,3},{3,4}}",
				"  Lexicographic:   165: {{1},{4},{1,2},{2,3}}",
				"  VDD:             525: {{1},{3},{1,2},{2,4}}",
				"  MM:              270: {{2},{3},{1,2},{1,4}}",
				"  BII:             150: {{2},{4},{1,2},{1,3}}",
				"A binary index of n is any position of a 1 in its reversed binary expansion. The binary indices of n are row n of A048793. We define the set-system with BII-number n to be obtained by taking the binary indices of each binary index of n. Every set-system has a different BII-number. For example, 18 has reversed binary expansion (0,1,0,0,1), and since the binary indices of 2 and 5 are {2} and {1,3} respectively, the BII-number of {{2},{1,3}} is 18. Elements of a set-system are sometimes called edges."
			],
			"link": [
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Idempotent\"\u003eIdempotence\u003c/a\u003e"
			],
			"example": [
				"56 is the BII-number of {{3},{1,3},{2,3}}, which has VDD-normalization {{1},{1,2},{1,3}} with BII-number 21, so a(56) = 21."
			],
			"mathematica": [
				"bpe[n_]:=Join@@Position[Reverse[IntegerDigits[n,2]],1];",
				"fbi[q_]:=If[q=={},0,Total[2^q]/2];",
				"sysnorm[m_]:=If[Union@@m!={}\u0026\u0026Union@@m!=Range[Max@@Flatten[m]],sysnorm[m/.Rule@@@Table[{(Union@@m)[[i]],i},{i,Length[Union@@m]}]],First[Sort[sysnorm[m,1]]]];",
				"sysnorm[m_,aft_]:=If[Length[Union@@m]\u003c=aft,{m},With[{mx=Table[Count[m,i,{2}],{i,Select[Union@@m,#\u003e=aft\u0026]}]},Union@@(sysnorm[#,aft+1]\u0026/@Union[Table[Map[Sort,m/.{par+aft-1-\u003eaft,aft-\u003epar+aft-1},{0,1}],{par,First/@Position[mx,Max[mx]]}]])]];",
				"Table[fbi[fbi/@sysnorm[bpe/@bpe[n]]],{n,0,100}]"
			],
			"xref": [
				"This sequence is idempotent and its image/fixed points are A330100.",
				"Non-isomorphic multiset partitions are A007716.",
				"Unlabeled spanning set-systems counted by vertices are A055621.",
				"Unlabeled set-systems counted by weight are A283877.",
				"Cf. A000120, A000612, A048793, A070939, A300913, A319559, A321405, A326031, A326754, A330061, A330101.",
				"Other fixed points:",
				"- Brute-force: A330104 (multisets of multisets), A330107 (multiset partitions), A330099 (set-systems).",
				"- Lexicographic: A330120 (multisets of multisets), A330121 (multiset partitions), A330110 (set-systems).",
				"- VDD: A330060 (multisets of multisets), A330097 (multiset partitions), A330100 (set-systems).",
				"- MM: A330108 (multisets of multisets), A330122 (multiset partitions), A330123 (set-systems).",
				"- BII: A330109 (set-systems)."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_Gus Wiseman_, Dec 04 2019",
			"references": 11,
			"revision": 6,
			"time": "2019-12-05T08:23:47-05:00",
			"created": "2019-12-05T08:23:47-05:00"
		}
	]
}