{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A103295",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 103295,
			"data": "1,1,1,3,4,9,17,33,63,128,248,495,988,1969,3911,7857,15635,31304,62732,125501,250793,503203,1006339,2014992,4035985,8080448,16169267,32397761,64826967,129774838,259822143,520063531,1040616486,2083345793,4168640894,8342197304,16694070805,33404706520,66832674546,133736345590",
			"name": "Number of complete rulers with length n.",
			"comment": [
				"For definitions, references and links related to complete rulers see A103294.",
				"Also the number of compositions of n whose consecutive subsequence-sums cover an initial interval of the positive integers. For example, (2,3,1) is such a composition because (1), (2), (3), (3,1), (2,3), and (2,3,1) are subsequences with sums covering {1..6}. - _Gus Wiseman_, May 17 2019",
				"a(n) ~ c*2^n, where 0.2427 \u003c c \u003c 0.2459. - _Fei Peng_, Oct 17 2019"
			],
			"link": [
				"Scott Harvey-Arnold, Steven J. Miller, and Fei Peng, \u003ca href=\"https://arxiv.org/abs/2001.08931\"\u003eDistribution of missing differences in diffsets\u003c/a\u003e, arXiv:2001.08931 [math.CO], 2020.",
				"Peter Luschny, \u003ca href=\"http://oeis.org/wiki/User:Peter_Luschny/PerfectRulers\"\u003ePerfect rulers\u003c/a\u003e",
				"Hugo Pfoertner, \u003ca href=\"http://www.randomwalk.de/sequences/a103295.txt\"\u003eCount complete rulers of given length.\u003c/a\u003e FORTRAN program.",
				"\u003ca href=\"/index/Per#perul\"\u003eIndex entries for sequences related to perfect rulers.\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Sum_{i=0..n} A103294(n, i) = Sum_{i=A103298(n)..n} A103294(n, i)."
			],
			"example": [
				"a(4) = 4 counts the complete rulers with length 4, {[0,2,3,4],[0,1,3,4],[0,1,2,4],[0,1,2,3,4]}.",
				"From _Gus Wiseman_, May 17 2019: (Start)",
				"The a(1) = 1 through a(6) = 17 rulers:",
				"  {0,1}  {0,1,2}  {0,1,3}    {0,1,2,4}    {0,1,2,5}      {0,1,4,6}",
				"                  {0,2,3}    {0,1,3,4}    {0,1,3,5}      {0,2,5,6}",
				"                  {0,1,2,3}  {0,2,3,4}    {0,2,4,5}      {0,1,2,3,6}",
				"                             {0,1,2,3,4}  {0,3,4,5}      {0,1,2,4,6}",
				"                                          {0,1,2,3,5}    {0,1,2,5,6}",
				"                                          {0,1,2,4,5}    {0,1,3,4,6}",
				"                                          {0,1,3,4,5}    {0,1,3,5,6}",
				"                                          {0,2,3,4,5}    {0,1,4,5,6}",
				"                                          {0,1,2,3,4,5}  {0,2,3,5,6}",
				"                                                         {0,2,4,5,6}",
				"                                                         {0,3,4,5,6}",
				"                                                         {0,1,2,3,4,6}",
				"                                                         {0,1,2,3,5,6}",
				"                                                         {0,1,2,4,5,6}",
				"                                                         {0,1,3,4,5,6}",
				"                                                         {0,2,3,4,5,6}",
				"                                                         {0,1,2,3,4,5,6}",
				"The a(1) = 1 through a(6) = 17 compositions are:",
				"  (1)  (11)  (12)   (112)   (113)    (132)",
				"             (21)   (121)   (122)    (231)",
				"             (111)  (211)   (221)    (1113)",
				"                    (1111)  (311)    (1122)",
				"                            (1112)   (1131)",
				"                            (1121)   (1212)",
				"                            (1211)   (1221)",
				"                            (2111)   (1311)",
				"                            (11111)  (2121)",
				"                                     (2211)",
				"                                     (3111)",
				"                                     (11112)",
				"                                     (11121)",
				"                                     (11211)",
				"                                     (12111)",
				"                                     (21111)",
				"                                     (111111)",
				"(End)"
			],
			"mathematica": [
				"Table[Length[Select[Join@@Permutations/@IntegerPartitions[n],SubsetQ[ReplaceList[#,{___,s__,___}:\u003ePlus[s]],Range[n]]\u0026]],{n,0,15}] (* _Gus Wiseman_, May 17 2019 *)"
			],
			"xref": [
				"Cf. A103300 (Perfect rulers with length n). Main diagonal of A349976.",
				"Cf. A000079, A126796, A143823, A169942, A325676, A325677, A325683, A325684, A325685."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_Peter Luschny_, Feb 28 2005",
			"ext": [
				"a(30)-a(36) from _Hugo Pfoertner_, Mar 17 2005",
				"a(37)-a(38) from _Hugo Pfoertner_, Dec 10 2021",
				"a(39) from _Hugo Pfoertner_, Dec 16 2021"
			],
			"references": 34,
			"revision": 51,
			"time": "2021-12-16T10:38:01-05:00",
			"created": "2005-04-09T03:00:00-04:00"
		}
	]
}