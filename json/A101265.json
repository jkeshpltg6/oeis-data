{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A101265",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 101265,
			"data": "1,2,6,21,77,286,1066,3977,14841,55386,206702,771421,2878981,10744502,40099026,149651601,558507377,2084377906,7779004246,29031639077,108347552061,404358569166,1509086724602,5631988329241,21018866592361,78443478040202,292755045568446",
			"name": "a(1) = 1, a(2) = 2, a(3) = 6; a(n) = 5*a(n-1) - 5*a(n-2) + a(n-3) for n\u003e3.",
			"comment": [
				"Let M = [ 1, 1, 0; 1, 3, 1; 0, 1, 1]; then [1,0,0]*M^n = [a(n), A001353(n), A061278(n-1)] for n\u003e1; Further a(n) = A061278(n) + 1; A001353(n) is first difference of a(n) and first difference of A061278(n). Let v(n) = [1,0,0]*M^n, then sum(v_i(n)) = A001075(n) for n\u003e=0; and v_1(n) + v_3(n) = A001835(n) for n\u003e=0; Characteristic polynomial of M = x^3 - 5x^2 + 5x - 1; a(n)/a(n-1) tends to 2 + sqrt(3) = 3.732.... (see A019973) (a root of the polynomial and an eigenvalue of the matrix).",
				"a(n) = 1 + A061278(n). Also numbers n such that the RootMeanSquare(1,...,6*n-5) is an integer. - _Ctibor O. Zizka_, Dec 17 2008",
				"a(n) blue and b(n) red balls in an urn. Draw 3 balls without replacement. Then Probability(3 red balls)=Probability(1 red and 2 blue balls); binomial(b(n),3)=binomial(b(n),1)*binomial(a(n),2); b(n)=A179167(n). - _Paul Weisenhorn_, Jul 01 2010",
				"Conjecture: consecutive terms of this sequence and consecutive terms of A032908 provide all the positive integer solutions of (a+b)*(a+b+1) == 0 mod (a*b). - _Robert Israel_, Aug 26 2015",
				"Conjecture is true: see StackExchange link. - _Robert Israel_, Sep 06 2015"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A101265/b101265.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"R. Israel, W. Jagy et al, \u003ca href=\"http://math.stackexchange.com/questions/1417404/diophantine-equation-xyxy1-kxy-0\"\u003eDiophantine equation (x+y)(x+y+1)-kxy=0\u003c/a\u003e, Math StackExchange, Sep 1 2015.",
				"Giovanni Lucca, \u003ca href=\"http://forumgeom.fau.edu/FG2016volume16/FG2016volume16.pdf#page=423\"\u003eCircle Chains Inscribed in Symmetrical Lenses and Integer Sequences\u003c/a\u003e, Forum Geometricorum, Volume 16 (2016) 419-427.",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (5,-5,1)."
			],
			"formula": [
				"a(n) = A005246(n)*A005246(n+1). a(n+1) = a(n)*(a(n)+1)/a(n-1). - _Franklin T. Adams-Watters_, Apr 24 2006",
				"a(n) = (A001835(n) + 1) / 2. - _Ralf Stephan_, May 16 2007",
				"O.g.f.: x*(1-3*x+x^2)/((1-x)*(1-4*x+x^2)). - _R. J. Mathar_, Aug 22 2008",
				"a(n) = 4*a(n-1) - a(n-2) - 1. - _N. Sato_, Jan 21 2010",
				"a(n) = (6+(3+r)*(2+r)^(n-1) + (3-r)*(2-r)^(n-1))/12; r=sqrt(3). - _Paul Weisenhorn_, Jul 01 2010",
				"a(n+1) = a(n) * (a(n) + 1) / a(n-1) for n\u003e1 with a(0)=1, a(1)=1. - _Paul D. Hanna_, Apr 08 2012",
				"From _Peter Bala_, May 01 2012: (Start)",
				"a(n+1) = 1 + Sum {k = 1..n} 2^(k-1)*binomial(n+k,2*k).",
				"Row sums of A211955.",
				"a(n) = T(n,u)*T(n+1,u)/u with u = sqrt(3) and T(n,x) denotes the Chebyshev polynomial of the first kind.",
				"Sum_{n \u003e= 0} 1/a(n) = sqrt(3). In fact, 3 - (Sum_{n = 0..2*N} 1/a(n))^2 = 2/(A001835(N+1))^2 and 3 - (Sum_{n = 0..2*N+1} 1/a(n))^2 = 3/(A001075(N+1))^2. (End)",
				"From _Robert Israel_, Aug 26 2015: (Start)",
				"(a(n) + a(n+1))*(a(n) + a(n+1) + 1) = 6 * a(n) * a(n+1).",
				"a(n+1) = 2*a(n) + (sqrt(12*a(n)^2 - 12*a(n) + 1) - 1)/2. (End)",
				"a(n) = (ChebyshevU(n, 2) - ChebyshevU(n-1, 2) + 1)/2 = (ChebyshevT(n, 2) + ChebyshevU(n, 2) + 2)/4. - _G. C. Greubel_, Dec 23 2019"
			],
			"example": [
				"For n=5 a(5)=77; b(5)=134; binomial(134,3)=392084; binomial(134,1)*binomial(77,2)=134*2926=392084. - _Paul Weisenhorn_, Jul 01 2010"
			],
			"maple": [
				"r:=sqrt(3): for n from 1 to 100 do a[n]:=(6+(3+r)*(2+r)^(n-1)+(3-r)*(2-r)^(n-1))/12: end do: # _Paul Weisenhorn_, Jul 01 2010",
				"r:=sqrt(3): a[n]:=round((6+(3+r)*(2+r)^(n-1))/12): # _Paul Weisenhorn_, Jul 01 2010",
				"f:= proc(n)",
				"  option remember; local x;",
				"  x:= procname(n-1);",
				"  2*x + (sqrt(12*x^2 - 12*x + 1) - 1)/2",
				"end proc:",
				"f(1):= 1:",
				"map(f, [$1..30]); # _Robert Israel_, Aug 26 2015",
				"seq( simplify((ChebyshevU(n,2) - Chebyshev(n-1,2) + 1)/2), n=0..20); # _G. C. Greubel_, Dec 23 2019"
			],
			"mathematica": [
				"LinearRecurrence[{5,-5,1},{1,2,6},25] (* _Ray Chandler_, Jan 27 2014 *)",
				"CoefficientList[Series[(1-3x+x^2)/((1-x)(1-4x+x^2)), {x, 0, 33}], x] (* _Vincenzo Librandi_, Sep 07 2015 *)",
				"Table[(ChebyshevU[n, 2] - ChebyshevU[n-1, 2] + 1)/2, {n, 0, 20}] (* _G. C. Greubel_, Dec 23 2019 *)"
			],
			"program": [
				"(PARI) M = [ 1, 1, 0; 1, 3, 1; 0, 1, 1]; for(i=1,30,print1(([1,0,0]*M^i)[1],\",\"))",
				"(PARI) {a(n)=polcoeff(x*(1-3*x+x^2)/((1-x)*(1-4*x+x^2)+x*O(x^n)),n)}",
				"(PARI) {a(n)=if(n==0,1,if(n==1,1,a(n-1)*(a(n-1)+1)/a(n-2)))} /* _Paul D. Hanna_, Apr 08 2012 */",
				"(PARI) vector(21, n, (polchebyshev(n, 2, 2) - polchebyshev(n-1, 2, 2) + 1)/2 ) \\\\ _G. C. Greubel_, Dec 23 2019",
				"(Haskell)",
				"a101265 n = a101265_list !! (n-1)",
				"a101265_list = 1 : 2 : 6 : zipWith (+) a101265_list",
				"    (map (* 5) $ tail $ zipWith (-) (tail a101265_list) a101265_list)",
				"-- _Reinhard Zumkeller_, May 18 2014",
				"(MAGMA) I:=[1,2,6]; [n le 3 select I[n] else 5*Self(n-1) - 5*Self(n-2) + Self(n-3): n in [1..30]]; // _Vincenzo Librandi_, Sep 07 2015",
				"(Sage) [(chebyshev_U(n,2) - chebyshev_U(n-1,2) + 1)/2 for n in (0..20)] # _G. C. Greubel_, Dec 23 2019",
				"(GAP) a:=[1,2,6];; for n in [4..20] do a[n]:=5a[n-1]-5*a[n-2]+a[n-3]; od; a; #",
				"_G. C. Greubel_, Dec 23 2019"
			],
			"xref": [
				"Cf. A001075, A001353, A001835, A005246, A019973, A032908, A061278, A211955."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "Lambert Klasen (lambert.klasen(AT)gmx.net) and _Gary W. Adamson_, Jan 25 2005",
			"ext": [
				"a(26)-a(27) from _Vincenzo Librandi_, Sep 07 2015"
			],
			"references": 11,
			"revision": 57,
			"time": "2019-12-24T08:30:59-05:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}