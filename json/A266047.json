{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A266047",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 266047,
			"data": "1,2,4,8,12,16,32,36,48,64,72,128,144,180,192,256,288,432,512,576,720,768,900,1024,1152,1296,1728,1800,2048,2304,2592,2880,3072,3600,4096,4608,5184,6300,6480,6912,7200,8192,9216,10368,10800,11520,12288,14400,15552,16384,18432",
			"name": "Smallest integers of each prime signature of prime factorization palindromes (A265640).",
			"comment": [
				"A subsequence of A025487.",
				"According to Hardy and Ramanujan, the number Q(x) of numbers",
				"2^b_2*3^b_3*...*p^b_p \u003c= x,       (1)",
				"where b_2\u003e=b_3\u003e=...\u003e=b_p, is of order e^(2Pi/sqrt(3)(1+o(1))sqrt(log x/loglog x)).",
				"If all b_i=2*c_i are even, then the number of such numbers is Q(sqrt(x)). Note that, if in (1) c_p\u003e0, where p is n-th prime, then c_r\u003e0, r\u003cp. Thus 2*3*...*p_n \u003c=  2^c_2* ... p^c_p \u003c= sqrt(x). By the PNT, 2*3*...*p_n=e^(n+o(n)). Then n\u003c=log(x)/2(1+o(log(x))) and for n\u003e=2 [Dusart], Eq(4.2),",
				"p\u003c=e*n*log(n)\u003ce/2*log(x*loglogx). (2)",
				"Let K(x) be the number of a(n)\u003c=x, q=nextprime(p). Then K(x)\u003c=Q(sqrt(x))(1+Sum_{prime p}1/p)+1/3, where p satisfies (2) (+1/3, taking into account 1/q).",
				"By [Rosser], Sum_{p\u003c=x}1/p=loglog(x)+0.261497...+o(1). Hence K(x)\u003c=Q(sqrt(x))*(loglog(e/2*log(x*loglogx))+1.594830...+o(1)).",
				"Asymptotics of K(x) remain open."
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A266047/b266047.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"P. Dusart, \u003ca href=\"http://arxiv.org/abs/1002.0442\"\u003eEstimates of some functions over primes without R.H.\u003c/a\u003e, arXiv:1002.0442 [math.NT], 2010.",
				"G. H. Hardy and S. Ramanujan, \u003ca href=\"http://www.imsc.res.in/~rao/ramanujan/CamUnivCpapers/Cpaper34/page1.htm\"\u003eAsymptotic formulas concerning the distribution of integers of various types\u003c/a\u003e, Proc. London Math. Soc, Ser. 2, Vol. 16 (1917), pp. 112-132.",
				"J. B. Rosser. \u003ca href=\"http://dx.doi.org/10.2307/2371291\"\u003eExplicit bounds for some functions of prime numbers\u003c/a\u003e. Amer. J. Math. 63 (1941), 211-232."
			],
			"xref": [
				"Cf. A025487, A265640, A265641."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Vladimir Shevelev_, Dec 20 2015",
			"references": 2,
			"revision": 32,
			"time": "2019-06-21T10:58:49-04:00",
			"created": "2015-12-23T13:53:24-05:00"
		}
	]
}