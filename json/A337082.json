{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A337082",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 337082,
			"data": "2,2,2,2,5,5,1,2,5,4,3,3,4,7,3,2,8,8,4,6,10,6,3,5,5,9,4,2,8,10,2,2,9,4,5,6,5,7,3,4,10,10,1,4,9,6,2,3,6,8,6,4,11,12,4,7,10,5,3,5,5,9,5,2,14,16,3,9,18,9,3,8,9,11,7,5,12,14,3,6,16,11,5,12,12,10,4,6,15,17,6,5,12,9,4,5,7,12,7,7",
			"name": "Number of ways to write n as x^2 + y^2 + z^2 + w^2 with 2*x^2 + 4*y^2 - 7*x*y a power of two (including 2^0 = 1), where x, y, z, w are nonnegative integers with z \u003c= w.",
			"comment": [
				"Conjecture: a(n) \u003e 0 for all n \u003e 0. Moreover, any positive integer n congruent to 1 or 2 modulo 4 can be written as x^2 + y^2 + z^2 + w^2 with x, y, z, w nonnegative integers such that 2*x^2 + 4*y^2 - 7*x*y = 4^k for some positive integer k.",
				"We have verified this for all n = 1..10^8.",
				"See also A338139 for a similar conjecture."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A337082/b337082.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2016.11.008\"\u003eRefining Lagrange's four-square theorem\u003c/a\u003e, J. Number Theory 175(2017), 167-190. See also \u003ca href=\"http://arxiv.org/abs/1604.06723\"\u003earXiv:1604.06723 [math.NT]\u003c/a\u003e.",
				"Zhi-Wei Sun, \u003ca href=\"https://doi.org/10.1142/S1793042119501045\"\u003eRestricted sums of four squares\u003c/a\u003e, Int. J. Number Theory 15(2019), 1863-1893.  See also \u003ca href=\"http://arxiv.org/abs/1701.05868\"\u003earXiv:1701.05868 [math.NT]\u003c/a\u003e.",
				"Zhi-Wei Sun, \u003ca href=\"https://arxiv.org/abs/2010.05775\"\u003eSums of four squares with certain restrictions\u003c/a\u003e, arXiv:2010.05775 [math.NT], 2020."
			],
			"example": [
				"a(7) = 1, and 7 = 1^2 + 2^2 + 1^2 + 1^2 with 2*1^2 + 4*2^2 - 7*1*2 = 2^2.",
				"a(43) = 1, and 43 = 4^2 + 1^2 + 1^2 + 5^2 with 2*4^2 + 4*1^2 - 7*4*1 = 2^3.",
				"a(283) = 1, and 283 = 4^2 + 7^2 + 7^2 + 13^2 with 2*4^2 + 4*7^2 - 7*4*7 = 2^5.",
				"a(2731) = 1, and 2731 = 5^2 + 7^2 + 16^2 + 49^2 with 2*5^2 + 4*7^2 - 7*5*7 = 2^0.",
				"a(25475) = 1, and 25475 = 68^2 + 95^2 + 45^2 + 99^2 with 2*68^2 + 4*95^2 - 7*68*95 = 2^7."
			],
			"mathematica": [
				"SQ[n_]:=SQ[n]=IntegerQ[Sqrt[n]];",
				"PQ[n_]:=PQ[n]=n\u003e0\u0026\u0026IntegerQ[Log[2,n]];",
				"tab={};Do[r=0;Do[If[SQ[n-x^2-y^2-z^2]\u0026\u0026PQ[2x^2+4*y^2-7*x*y],r=r+1],{x,0,Sqrt[n]},{y,Boole[x==0],Sqrt[n-x^2]},{z,0,Sqrt[(n-x^2-y^2)/2]}];tab=Append[tab,r],{n,1,100}];tab"
			],
			"xref": [
				"Cf. A000079, A000118, A000290, A000302, A279612, A282463, A338094, A338095, A338096, A338103, A338119, A338121, A338139."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Zhi-Wei Sun_, Oct 12 2020",
			"references": 3,
			"revision": 25,
			"time": "2021-01-19T21:01:09-05:00",
			"created": "2020-10-13T04:02:29-04:00"
		}
	]
}