{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A283424",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 283424,
			"data": "1,2,1,5,3,1,15,10,4,1,52,37,17,5,1,203,151,76,26,6,1,877,674,362,137,37,7,1,4140,3263,1842,750,225,50,8,1,21147,17007,9991,4307,1395,345,65,9,1,115975,94828,57568,25996,8944,2392,502,82,10,1",
			"name": "Number T(n,k) of blocks of size \u003e= k in all set partitions of [n], assuming that every set partition contains one block of size zero; triangle T(n,k), n\u003e=0, 0\u003c=k\u003c=n, read by rows.",
			"comment": [
				"T(n,k) is defined for all n,k \u003e= 0.  The triangle contains only the terms with k\u003c=n.  T(n,k) = 0 for k\u003en."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A283424/b283424.txt\"\u003eRows n = 0..140, flattened\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Partition_of_a_set\"\u003ePartition of a set\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = Sum_{j=0..n-k} binomial(n,j) * Bell(j).",
				"T(n,k) = Bell(n+1) - Sum_{j=0..k-1} binomial(n,j) * Bell(n-j).",
				"T(n,k) = Sum_{j=k..n} A056857(n+1,j) = Sum_{j=k..n} A056860(n+1,n+1-j).",
				"Sum_{k=0..n} T(n,k) = n*Bell(n)+Bell(n+1) = A124427(n+1).",
				"Sum_{k=1..n} T(n,k) = n*Bell(n) = A070071(n).",
				"T(n,0)-T(n,1) = Bell(n)."
			],
			"example": [
				"T(3,2) = 4 because the number of blocks of size \u003e= 2 in all set partitions of [3] (123, 12|3, 13|2, 1|23, 1|2|3) is 1+1+1+1+0 = 4.",
				"Triangle T(n,k) begins:",
				"      1;",
				"      2,     1;",
				"      5,     3,    1;",
				"     15,    10,    4,    1;",
				"     52,    37,   17,    5,    1;",
				"    203,   151,   76,   26,    6,   1;",
				"    877,   674,  362,  137,   37,   7,  1;",
				"   4140,  3263, 1842,  750,  225,  50,  8, 1;",
				"  21147, 17007, 9991, 4307, 1395, 345, 65, 9, 1;",
				"  ..."
			],
			"maple": [
				"T:= proc(n, k) option remember; `if`(k\u003en, 0,",
				"      binomial(n, k)*combinat[bell](n-k)+T(n, k+1))",
				"    end:",
				"seq(seq(T(n, k), k=0..n), n=0..14);"
			],
			"mathematica": [
				"T[n_, k_] := Sum[Binomial[n, j]*BellB[j], {j, 0, n - k}];",
				"Table[T[n, k], {n, 0, 14}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Apr 30 2018 *)"
			],
			"xref": [
				"Columns k=0-10 give: A000110(n+1), A138378 or A005493(n-1), A124325, A288785, A288786, A288787, A288788, A288789, A288790, A288791, A288792.",
				"Row sums give A124427(n+1).",
				"T(2n,n) gives A286896.",
				"Cf. A005493, A056857, A056860, A070071, A285595."
			],
			"keyword": "nonn,tabl",
			"offset": "0,2",
			"author": "_Alois P. Heinz_, May 14 2017",
			"references": 13,
			"revision": 35,
			"time": "2021-08-03T20:43:22-04:00",
			"created": "2017-05-15T09:30:48-04:00"
		}
	]
}