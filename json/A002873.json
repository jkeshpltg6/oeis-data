{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002873",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2873,
			"id": "M2872 N1154",
			"data": "1,1,3,10,53,265,1700,13097,96796,829080,8009815,75604892,808861988,9175286549,106167118057,1320388106466,16950041305210,233232366601078,3243603207488124,47776065074368313,733990397879859192,11515503147927664816,189107783918416912912",
			"name": "The maximal number of partitions of {1..2n} that are invariant under a permutation consisting of n 2-cycles, and which have the same number of nonempty parts.",
			"comment": [
				"Previous name was: Sorting numbers (see Motzkin article for details).",
				"Since a(n) by definition is the largest among some positive integers, whose sum is A002872(n), we always have the relation a(n) \u003c= A002872(n); and for n \u003e 0 the inequality is strict, since then that sum consists of more than one term. - _Jörgen Backelin_, Jan 13 2016"
			],
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A002873/b002873.txt\"\u003eTable of n, a(n) for n = 0..514\u003c/a\u003e",
				"Victor Meally, \u003ca href=\"/A002868/a002868.pdf\"\u003eComparison of several sequences given in Motzkin's paper \"Sorting numbers for cylinders...\"\u003c/a\u003e, letter to N. J. A. Sloane, N. D.",
				"T. S. Motzkin, \u003ca href=\"/A000262/a000262.pdf\"\u003eSorting numbers for cylinders and other classification numbers\u003c/a\u003e, in Combinatorics, Proc. Symp. Pure Math. 19, AMS, 1971, pp. 167-176. [Annotated, scanned copy]",
				"OEIS Wiki, \u003ca href=\"http://oeis.org/wiki/Sorting_numbers\"\u003eSorting numbers\u003c/a\u003e",
				"\u003ca href=\"/index/So#sorting\"\u003eIndex entries for sequences related to sorting\u003c/a\u003e"
			],
			"example": [
				"There are three partitions of {1,2,3,4} into two (nonempty) parts, and which are invariant under the permutation (1,2)(3,4), namely {{1,2}, {3,4}}, {{1,3}, {2,4}}, and {{1,4}, {2,3}}. There are also one such partition with just one part, two with three parts, and one with four parts; but three is the largest of these amounts. Thus, a(2) = 3.",
				"Similarly, there are ten (1,2)(3,4)(5,6) invariant partitions of {1,2,3,4,5,6} into three nonempty parts, and no larger amount into any other given number of parts, whence a(3) = 10."
			],
			"xref": [
				"Cf. A000262 (the parent sequence of this family), A002872.",
				"Maximum row values of A293181."
			],
			"keyword": "nonn,nice",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Name changed and example added by _Jörgen Backelin_, Jan 13 2016",
				"a(7)-a(8) from _Sean A. Irvine_, Jun 19 2016",
				"a(9)-a(22) from _Andrew Howroyd_, Oct 01 2017"
			],
			"references": 11,
			"revision": 68,
			"time": "2018-04-24T16:52:44-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}