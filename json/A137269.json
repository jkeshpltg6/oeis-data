{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A137269",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 137269,
			"data": "0,1,1,2,1,0,2,1,0,1,2,0,5,1,0,4,3,0,8,2,0,2,2,0,10,1,0,5,4,0,8,1,0,4,2,0,17,151,0,7,4,0,13,3,0,812,3,0,17,4,0,12,1,0,13,1,0,6,2,0,18,1,0,11,1000,0,24,2,0,5,1,0,25,1,0,10,2,0,23,2,0,9,1",
			"name": "Number of primes with maximal digit product for a digit sum of n.",
			"comment": [
				"From _Chai Wah Wu_, Dec 04 2015, Nov 03 2018: (Start)",
				"If n \u003e 3 and n == 0 mod 3 then a(n) = 0 since the digit sum is a multiple of 3.",
				"Primes with digit product maximal among all numbers with the same digit sum (not just maximal among primes) only contain the digits 2, 3 or 4. A digit 0 leads to a digit product 0 which is not maximal. A digit 1 with another digit d (since 1 is not prime, there must be another digit d) can be replaced with the digit d+1 (if d \u003c 9) which preserves the digit sum, but strictly increases the digit product (if d = 9, 1 and 9 can be replaced with 3, 3 and 4 which again increases the digit product). For a digit d \u003e 4, there is a series of digits from the set {2,3,4} whose sum is d and whose product is strictly larger than d. For instance, 5 -\u003e {2,3} whose product is 6. 6 -\u003e {3,3}, 7 -\u003e {3,4}, 8 -\u003e {2,3,3}, 9 -\u003e {3,3,3}. Thus the digit d in a number can be replaced with digits 2, 3, 4 to obtain a number with the same digit sum and a larger digit product. Furthermore, the digits 2 and 4 cannot both appear, the digit 2 cannot appear more than twice and the digit 4 cannot appear more than once since {3,3} also sums to 6 and has product 9 \u003e 8.",
				"This analysis implies the following for n \u003e 3. If n == 1 mod 3, then primes with maximal digit product among all numbers with the same digit sum (if they exist) have digits 3 and either two digits 2 or a single digit 4. If n == 2 mod 3, then primes with maximal digit product among all numbers with the same digit sum (if they exist) have digits 3 and a single digit 2. Values for n for which such primes do not exist are 4, 38, 46, 65, 94, ...  In these cases a(n) can still be \u003e 0, but the digit product of these primes is not maximal among all numbers with digit sum n. So far, it seems that in these cases (except for n = 4) these primes also only contain the digits 2, 3, or 4.",
				"(End)"
			],
			"link": [
				"Chai Wah Wu, \u003ca href=\"/A137269/b137269.txt\"\u003eTable of n, a(n) for n = 1..802\u003c/a\u003e"
			],
			"example": [
				"a(19)=8 and a(20)=2 because we respectively have the 8 primes 333433, 334333, 343333, 2332333, 2333323, 3223333, 3233323, 3332233 all with a maximal digit product of 3^5*2^2 = 972 for a digit sum of 19 and the 2 primes 3233333, 3333233 with maximal digit product 3^6*2 = 1458 for digit sum 20."
			],
			"mathematica": [
				"Needs[\"Combinatorica`\"]; Table[If[And[n \u003e 3, Divisible[n, 3]], 0, Length@ MaximalBy[Select[FromDigits /@ Flatten[Map[Permutations, Combinatorica`Partitions@ n], 1] /. x_ /; EvenQ@ x -\u003e Nothing, PrimeQ], Times @@ IntegerDigits@ # \u0026]], {n, 24}] (* _Michael De Vlieger_, Dec 11 2015, Version 10 *)"
			],
			"xref": [
				"Cf. A000792, A133223, A136150, A137248, A138975."
			],
			"keyword": "nonn,base",
			"offset": "1,4",
			"author": "_Lekraj Beedassy_, Apr 05 2008",
			"ext": [
				"a(25) and a(28) corrected and a(29)-a(83) added by _Chai Wah Wu_, Nov 30 2015"
			],
			"references": 4,
			"revision": 43,
			"time": "2018-11-06T13:20:03-05:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}