{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A330773",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 330773,
			"data": "1,1,1,3,1,5,1,11,3,5,1,27,1,5,5,49,1,27,1,27,5,5,1,163,3,5,11,27,1,49,1,261,5,5,5,231,1,5,5,163,1,49,1,27,27,5,1,1109,3,27,5,27,1,163,5,163,5,5,1,435,1,5,27,1631,5,49,1,27,5,49,1,2055,1,5,27,27,5,49,1",
			"name": "Number of perfect compositions of n.",
			"comment": [
				"A perfect composition of n is one whose sequence of parts contains one composition of every positive integer less than n."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A330773/b330773.txt\"\u003eTable of n, a(n) for n = 0..20000\u003c/a\u003e",
				"A. O. Munagi, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL23/Munagi/munagi13.html\"\u003ePerfect Compositions of Numbers\u003c/a\u003e, J. Integer Seq. 23 (2020), art. 20.5.1."
			],
			"formula": [
				"a(1)=1, a(n) = Sum_{k=1..Omega(n+1)} k! * A251683(n+1,k), n\u003e1."
			],
			"example": [
				"a(7) = 11 because the perfect compositions are 1111111, 1222, 2221, 1114, 4111, 124, 142, 214, 241, 412, 421.",
				"For example, 241 generates the compositions of 1,...,6: 1,2,21,4,41,24."
			],
			"maple": [
				"b:= proc(n) option remember; expand(x*(1+add(b(n/d),",
				"       d=numtheory[divisors](n) minus {1, n})))",
				"    end:",
				"a:= n-\u003e (p-\u003e add(coeff(p, x, i)*i!, i=1..degree(p)))(b(n+1)):",
				"seq(a(n), n=0..100);  # _Alois P. Heinz_, Jan 15 2020"
			],
			"mathematica": [
				"b[n_] := b[n] = x(1+Sum[b[n/d], {d, Divisors[n]~Complement~{1, n}}]);",
				"a[n_] := With[{p = b[n+1]}, Sum[Coefficient[p, x, i] i!, {i, Exponent[p, x]}]];",
				"a /@ Range[0, 100] (* _Jean-François Alcover_, Nov 17 2020, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A001222, A002033, A074206, A251683, A330774."
			],
			"keyword": "nonn,easy",
			"offset": "0,4",
			"author": "_Augustine O. Munagi_, Dec 30 2019",
			"references": 2,
			"revision": 24,
			"time": "2020-11-17T05:40:27-05:00",
			"created": "2020-01-16T05:34:12-05:00"
		}
	]
}