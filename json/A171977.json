{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A171977",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 171977,
			"data": "2,4,2,8,2,4,2,16,2,4,2,8,2,4,2,32,2,4,2,8,2,4,2,16,2,4,2,8,2,4,2,64,2,4,2,8,2,4,2,16,2,4,2,8,2,4,2,32,2,4,2,8,2,4,2,16,2,4,2,8,2,4,2",
			"name": "a(n) = 2^(k+1) where 2^k is the highest power of 2 dividing n.",
			"comment": [
				"When read as a triangle in which the n-th row has 2^n terms, every row is the last half of the next one. All the terms are powers of 2. First column = 2*A000079.",
				"The original definition was: a(n) = (A000265(2n+1) - 1) / A000265(2n).",
				"a(n) seems to be the denominator of Euler(2*n+1,1) but I have no proof of this.",
				"a(n) is also gcd[C(2n,1), C(2n,3), ..., C(2n,2n-1)]. - _Franz Vrabec_, Oct 22 2012",
				"a(n) is also the ratio r(2n) = s2(2n)/s1(2n) where s1(2n) is the sum of the odd unitary divisors of 2n and s2(2n) is the sum of the even unitary divisors of 2n. - _Michel Lagneau_, Dec 19 2013",
				"a(n) or a(n)/2 = A006519(n) is known as the Steinhaus sequence in probability theory, proposed as a sequence of asymptotically fair premiums for the St. Petersburg game. - _Peter Kern_, Aug 28 2015",
				"After the all-1's sequence this is the next sequence in lexicographical order such that the gap between a(n) and the next occurrence of a(n) is given by a(n). - _Scott R. Shannon_, Oct 16 2019",
				"First 2^(k-1) - 1 terms are also the areas of the successive rectangles and squares of width 2 that are adjacent to any of the four sides of the toothpick structure of A139250 after 2^k stages, with k \u003e= 2. For example: if k = 5 the areas after 32 stages are [2, 4, 2, 8, 2, 4, 2, 16, 2, 4, 2, 8, 2, 4, 2] respectively, the same as the first 15 terms of this sequence. - _Omar E. Pol_, Dec 29 2020"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A171977/b171977.txt\"\u003eTable of n, a(n) for n = 1..16383\u003c/a\u003e",
				"Sandor Csörgö and Gordon Simons, \u003ca href=\"http://www.math.uni.wroc.pl/~pms/files/14.2/Abstract/14.2.1.abs.pdf\"\u003eOn Steinhaus' resolution of the St. Petersburg paradox\u003c/a\u003e, Probab. Math. Statist. 14 (1993), 157--172. MR1321758 (96b:60017). - _Peter Kern_, Aug 28 2015",
				"Roger B. Eggleton, Aviezri S. Fraenkel, and R. Jamie Simpson, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(93)90153-K\"\u003eBeatty sequences and Langford sequences\u003c/a\u003e, Graph theory and combinatorics (Marseille-Luminy, 1990). Discrete Math. 111 (1993), no. 1-3, 165--178. MR1210094 (94a:11018). See Example 2.6. - _N. J. A. Sloane_, Mar 18 2012",
				"Hugo Steinhaus, \u003ca href=\"http://pldml.icm.edu.pl/pldml/element/bwmeta1.element.bwnjournal-article-cmv2i1p56bwm\"\u003eThe so-called Petersburg paradox\u003c/a\u003e, Colloq. Math. 2 (1949), 56--58. MR0039937 (12,619e)."
			],
			"formula": [
				"a(n) = (A000265(2*n+1)-1)/A000265(2*n).",
				"a(n) = -(-n XOR n).  XOR the bitwise operation on the two's complement representation for negative integers. - _Peter Luschny_, Jun 01 2011",
				"a(n) = A038712(n)+1. - _Franz Vrabec_, Mar 03 2012",
				"a(n) = 2^A001511(n). - _Franz Vrabec_, Oct 22 2012",
				"a(n) = A046161(n)/A046161(n-1). - _Johannes W. Meijer_, Nov 04 2012",
				"a(n) = 2^(1 + (A183063(n)/A001227(n))). - _Omar E. Pol_, Nov 06 2018",
				"a(n) = 2*A006519(n). - _Antti Karttunen_, Nov 06 2018"
			],
			"maple": [
				"a := proc(n) local k: k:=1: while frac(n/2^k) = 0 do k := k+1 end do: k := k-1: a(n) := 2^(k+1) end: seq(a(n), n=1..63); # _Johannes W. Meijer_, Nov 04 2012",
				"seq(2^(1 + padic[ordp](n, 2)), n = 1..63); # _Peter Luschny_, Nov 27 2020"
			],
			"mathematica": [
				"Table[-BitXor[-i,i], {i, 200}] (* _Peter Luschny_, Jun 01 2011 *)",
				"a[n_] := 2^(IntegerExponent[n, 2] + 1); Array[a, 100] (* _Jean-François Alcover_, May 09 2017 *)"
			],
			"program": [
				"(PARI) A171977(n) = 2^(1+valuation(n,2)); \\\\ _Antti Karttunen_, Nov 06 2018"
			],
			"xref": [
				"Cf. A000079, A000265, A006519, A038712, A139250."
			],
			"keyword": "nonn,tabf",
			"offset": "1,1",
			"author": "_Paul Curtz_, Nov 19 2010",
			"ext": [
				"I edited this sequence, based on an email message from the author. - _N. J. A. Sloane_, Nov 20 2010",
				"Definition simplified by _N. J. A. Sloane_, Mar 18 2012"
			],
			"references": 12,
			"revision": 101,
			"time": "2021-01-06T20:53:54-05:00",
			"created": "2010-11-12T14:19:25-05:00"
		}
	]
}