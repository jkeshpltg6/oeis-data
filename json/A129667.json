{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A129667",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 129667,
			"data": "1,-1,-1,-1,-1,1,-1,0,-1,1,-1,1,-1,1,1,0,-1,1,-1,1,1,1,-1,0,-1,1,0,1,-1,-1,-1,1,1,1,1,1,-1,1,1,0,-1,-1,-1,1,1,1,-1,0,-1,1,1,1,-1,0,1,0,1,1,-1,-1,-1,1,1,0,1,-1,-1,1,1,-1,-1,0,-1,1,1,1,1,-1,-1,0,0,1,-1,-1,1,1,1,0,-1,-1,1,1,1,1,1,-1,-1,1,1,1,-1,-1,-1,0,-1,1,-1",
			"name": "Dirichlet inverse of the Abelian group count (A000688).",
			"comment": [
				"The simple formula which gives the value of this multiplicative function for the power of any prime can be derived from Euler's celebrated \"Pentagonal Number Theorem\" (applied to the generating function of the partition function A000041 on which A000688 is based)."
			],
			"link": [
				"R. J. Mathar, \u003ca href=\"/A129667/b129667.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"G. P. Michon, \u003ca href=\"http://www.numericana.com/answer/numbers.htm#partitions\"\u003ePartition Function\u003c/a\u003e and Pentagonal Numbers.",
				"G. P. Michon, \u003ca href=\"http://www.numericana.com/answer/numbers.htm#multiplicative\"\u003eMultiplicative Functions\u003c/a\u003e."
			],
			"formula": [
				"Multiplicative function for which a(p^e) either vanishes or is equal to (-1)^m, for any prime p, if e is either m(3m-1)/2 or m(3m+1)/2 (these integers are the pentagonal numbers of the first and second kind, A000326 and A005449).",
				"Dirichlet g.f.: 1 / Product_{k\u003e=1} zeta(k*s). - _Ilya Gutkovskiy_, Nov 06 2020"
			],
			"example": [
				"a(8) and a(27) are zero because the sequence vanishes for the cubes of primes. Not so with fifth powers of primes (since 5 is a pentagonal number) so a(32) is nonzero."
			],
			"maple": [
				"A000326inv := proc(n)",
				"    local x,a ;",
				"    for x from 0 do",
				"        a := x*(3*x-1)/2 ;",
				"        if a \u003e n then",
				"            return -1 ;",
				"        elif a = n then",
				"            return x;",
				"        end if;",
				"    end do:",
				"end proc:",
				"A005449inv := proc(n)",
				"    local x,a ;",
				"    for x from 0 do",
				"        a := x*(3*x+1)/2 ;",
				"        if a \u003e n then",
				"            return -1 ;",
				"        elif a = n then",
				"            return x;",
				"        end if;",
				"    end do:",
				"end proc:",
				"A129667 := proc(n)",
				"    local a,e1,e2 ;",
				"    a := 1 ;",
				"    for pe in ifactors(n)[2] do",
				"        e1 := A000326inv(op(2,pe)) ;",
				"        e2 := A005449inv(op(2,pe)) ;",
				"        if e1 \u003e= 0 then",
				"            a := a*(-1)^e1 ;",
				"        elif e2 \u003e= 0 then",
				"            a := a*(-1)^e2 ;",
				"        else",
				"            a := 0 ;",
				"        end if;",
				"    end do:",
				"    a;",
				"end proc: # _R. J. Mathar_, Nov 24 2017"
			],
			"mathematica": [
				"a[n_] := a[n] = If[n == 1, 1, -Sum[FiniteAbelianGroupCount[n/d] a[d], {d, Most @ Divisors[n]}]];",
				"Array[a, 100] (* _Jean-François Alcover_, Feb 16 2020 *)"
			],
			"xref": [
				"Cf. A000041, A000326, A000688, A005449, A023900, A101035."
			],
			"keyword": "mult,easy,sign",
			"offset": "1,1",
			"author": "_Gerard P. Michon_, Apr 28 2007, May 01 2007",
			"references": 6,
			"revision": 11,
			"time": "2020-11-08T08:41:33-05:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}