{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001001",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1001,
			"data": "1,7,13,35,31,91,57,155,130,217,133,455,183,399,403,651,307,910,381,1085,741,931,553,2015,806,1281,1210,1995,871,2821,993,2667,1729,2149,1767,4550,1407,2667,2379,4805,1723,5187,1893,4655,4030,3871,2257,8463,2850,5642,3991,6405,2863",
			"name": "Number of sublattices of index n in generic 3-dimensional lattice.",
			"comment": [
				"These sublattices are in 1-1 correspondence with matrices",
				"[a b d]",
				"[0 c e]",
				"[0 0 f]",
				"with acf = n, b = 0..c-1, d = 0..f-1, e = 0..f-1. The sublattice is primitive if gcd(a,b,c,d,e,f) = 1.",
				"Equals row sums of triangle A127108. - _Gary W. Adamson_, Jul 27 2008",
				"Total area of all distinct rectangles whose side lengths are divisors of n, and whose length is an integer multiple of the width. - _Wesley Ivan Hurt_, Aug 23 2020"
			],
			"reference": [
				"M. Baake, \"Solution of coincidence problem...\", in R. V. Moody, ed., Math. of Long-Range Aperiodic Order, Kluwer 1997, pp. 9-44.",
				"R. P. Stanley, Enumerative Combinatorics, Cambridge, Vol. 2, 1999; see Problem 5.13(d), pp. 76 and 113."
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A001001/b001001.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (terms 1..1000 from T. D. Noe)",
				"M. Baake, N. Neumarker, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL12/Baake/baake7.html\"\u003eA Note on the Relation Between Fixed Point and Orbit Count Sequences\u003c/a\u003e, JIS 12 (2009) 09.4.4, Section 3.",
				"J. Liouville, \u003ca href=\"http://sites.mathdoc.fr/JMPA/PDF/JMPA_1857_2_2_A4_0.pdf\"\u003eThéorème concernant les sommes de diviseurs des nombres\u003c/a\u003e, Journal de mathématiques pures et appliquées 2e série, tome 2 (1857), p. 56-.",
				"V. A. Liskovets and A. Mednykh, \u003ca href=\"https://doi.org/10.1080/00927870008826924\"\u003eEnumeration of subgroups in the fundamental groups of orientable circle bundles over surfaces\u003c/a\u003e, Commun. in Algebra, 28, No. 4 (2000), 1717-1738.",
				"Y. Puri and T. Ward, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL4/WARD/short.html\"\u003eArithmetic and growth of periodic orbits\u003c/a\u003e, J. Integer Seqs., Vol. 4 (2001), #01.2.1.",
				"J. S. Rutherford, \u003ca href=\"https://dx.doi.org/10.1107/S0108767392000898\"\u003eThe enumeration and symmetry-significant properties of derivative lattices\u003c/a\u003e, Act. Cryst. (1992) A48, 500-508",
				"J. S. Rutherford, \u003ca href=\"https://doi.org/10.1107/S0108767392007657\"\u003eThe enumeration and symmetry-significant properties of derivative lattices II\u003c/a\u003e, Acta Cryst. A49 (1993), 293-300. [_N. J. A. Sloane_, Mar 14 2009]",
				"Tad White, \u003ca href=\"http://arxiv.org/abs/1304.2830\"\u003eCounting Free Abelian Actions\u003c/a\u003e, arXiv:1304.2830 [math.CO], 2013.",
				"\u003ca href=\"/index/Su#sublatts\"\u003eIndex entries for sequences related to sublattices\u003c/a\u003e"
			],
			"formula": [
				"If n = Product p^m, a(n) = Product (p^(m + 1) - 1)(p^(m + 2) - 1)/(p - 1)(p^2 - 1). Or, a(n) = Sum_{d|n} sigma(n/d)*d^2, Dirichlet convolution of A000290 and A000203.",
				"a(n) = Sum_{d|n} d*sigma(d). - _Vladeta Jovovic_, Apr 06 2001",
				"Multiplicative with a(p^e) = ((p^(e+1)-1)(p^(e+2)-1))/((p-1)(p^2-1)). - _David W. Wilson_, Sep 01 2001",
				"Dirichlet g.f.: zeta(s)*zeta(s-1)*zeta(s-2).",
				"L.g.f.: -log(Product_{k\u003e=1} (1 - x^k)^sigma(k)) = Sum_{n\u003e=1} a(n)*x^n/n. - _Ilya Gutkovskiy_, May 23 2018",
				"a(n) = Sum_{d1|n, d2|n, d1|d2} d1*d2. - _Wesley Ivan Hurt_, Aug 23 2020"
			],
			"maple": [
				"nmax := 100:",
				"L12 := [seq(1,i=1..nmax) ];",
				"L27 := [seq(i,i=1..nmax) ];",
				"L290 := [seq(i^2,i=1..nmax) ];",
				"DIRICHLET(L12,L27) ;",
				"DIRICHLET(%,L290) ; # _R. J. Mathar_, Sep 25 2017"
			],
			"mathematica": [
				"a[n_] := Sum[ d*DivisorSigma[1, d], {d, Divisors[n]}]; Table[ a[n], {n, 1, 42}] (* _Jean-François Alcover_, Jan 20 2012, after _Vladeta Jovovic_ *)",
				"f[p_, e_] := Product[(p^(e + k) - 1)/(p^k - 1), {k, 1, 2}]; a[1] = 1; a[n_] := Times @@ (f @@@ FactorInteger[n]); Array[a, 100] (* _Amiram Eldar_, Aug 29 2019 *)"
			],
			"program": [
				"(PARI)",
				"N=17; default(seriesprecision,N); x=z+O(z^(N+1))",
				"c=sum(j=1,N,j*x^j);",
				"t=1/prod(j=1,N, eta(x^(j))^j)",
				"t=log(t)",
				"t=serconvol(t,c)",
				"Vec(t)",
				"/* _Joerg Arndt_, May 03 2008 */",
				"(PARI) a(n)=sumdiv(n,d, d * sumdiv(d,t, t ) );  /* _Joerg Arndt_, Oct 07 2012 */",
				"(PARI) a(n)=sumdivmult(n,d, sigma(d)*d) \\\\ _Charles R Greathouse IV_, Sep 09 2014"
			],
			"xref": [
				"Cf. A060983, A064987 (Mobius transform).",
				"Cf. A061256, A127108, A226313, A301777.",
				"Primes in this sequence are in A053183.",
				"Cf. A038991, A038992, A038993, A038994, A038995, A038996, A038997, A038998, A038999."
			],
			"keyword": "nonn,easy,nice,mult",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"references": 45,
			"revision": 82,
			"time": "2020-08-24T22:45:52-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}