{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A086270",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 86270,
			"data": "1,3,1,6,4,1,10,9,5,1,15,16,12,6,1,21,25,22,15,7,1,28,36,35,28,18,8,1,36,49,51,45,34,21,9,1,45,64,70,66,55,40,24,10,1,55,81,92,91,81,65,46,27,11,1,66,100,117,120,112,96,75,52,30,12,1,78,121,145,153,148,133,111",
			"name": "Rectangular array T(k,n) of polygonal numbers, by antidiagonals.",
			"comment": [
				"The antidiagonal sums 1, 4, 11, 25, 50, ... are the numbers A006522(n) for n\u003e=3.",
				"This is the accumulation array (Cf. A144112) of A144257 (which is the weight array of A086270). [_Clark Kimberling_, Sep 16 2008]",
				"By rows, the sequence beginning (1, N,...) is the binomial transform of (1, (N-1), (N-2), 0, 0, 0,...); and is the second partial sum of (1, (N-2), (N-2), (N-2),...). Example: The sequence (1, 4, 9, 16, 25,...) is the binomial transform of (1, 3, 2, 0, 0, 0,...) and the second partial sum of (1, 2, 2, 2,...). - _Gary W. Adamson_, Aug 23 2015"
			],
			"link": [
				"Clark Kimberling and John E. Brown, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL7/Kimberling/kimber67.html\"\u003ePartial Complements and Transposable Dispersions\u003c/a\u003e, J. Integer Seqs., Vol. 7, 2004.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Polygonal_number#Table_of_values\"\u003ePolygonal number: Table of values\u003c/a\u003e."
			],
			"formula": [
				"T(n, k) = n*binomial(k, 2) + k = A057145(n+2,k).",
				"2*T(n, k) = T(n+r, k) + T(n-r, k), where r = 0, 1, 2, 3, ..., n-1 (see table in Example field). [_Bruno Berselli_, Dec 19 2014]"
			],
			"example": [
				"First 6 rows:",
				"=========================================",
				"n\\k|  1   2    3    4    5    6     7",
				"---|-------------------------------------",
				"1  |  1   3    6   10   15   21    28 ... (A000217, triangular numbers)",
				"2  |  1   4    9   16   25   36    49 ... (A000290, squares)",
				"3  |  1   5   12   22   35   51    70 ... (A000326, pentagonal numbers)",
				"4  |  1   6   15   28   45   66    91 ... (A000384, hexagonal numbers)",
				"5  |  1   7   18   34   55   81   112 ... (A000566, heptagonal numbers)",
				"6  |  1   8   21   40   65   96   133 ... (A000567, octagonal numbers)",
				"...",
				"The array formed by the complements: A183225."
			],
			"mathematica": [
				"t[n_, k_] := n*Binomial[k, 2] + k; Table[ t[k, n - k + 1], {n, 12}, {k, n}] // Flatten"
			],
			"program": [
				"(MAGMA) T:=func\u003ch,i | h*Binomial(i,2)+i\u003e; [T(k,n-k+1): k in [1..n], n in [1..12]]; // _Bruno Berselli_, Dec 19 2014"
			],
			"xref": [
				"Cf. A086271, A086272, A086273, A139601.",
				"Cf. A144257. [_Clark Kimberling_, Sep 16 2008]"
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Jul 14 2003",
			"ext": [
				"Extended by _Clark Kimberling_, Jan 01 2011"
			],
			"references": 15,
			"revision": 34,
			"time": "2016-07-28T13:53:44-04:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}