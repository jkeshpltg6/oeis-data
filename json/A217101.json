{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A217101",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 217101,
			"data": "1,2,3,4,0,7,8,18,17,15,16,42,33,68,31,32,133,65,267,130,63,64,260,129,341,258,447,127,128,682,257,1040,514,895,1029,255,256,1919,513,2056,1026,1791,2053,2052,511,512,5376,1025,5461,2050,3583,4101,4100,8203,1023,1024,8200",
			"name": "Minimal number such that the number of contiguous palindromic bit patterns in its binary representation is n, or 0, if there is no such number.",
			"comment": [
				"The set of numbers which have n contiguous palindromic bit patterns (in their binary representation) is not empty, provided n\u003c\u003e5. Proof: For even n we have A206925(A206927(n/2)) = 2*(n/2) = n. For n=1,3,7,9 we get A206925(k)=n if we set k=1,3,8,17. For odd n\u003e10 we define b(n) := 14*2^((n-9)/2)+A206927((n-9)/2). The b(n) have the binary expansion 11110, 111100, 1111001, 11110010, 111100101, 1111001011, 11110010110, 111100101100, 1111001011001, 11110010110010, 111100101100101, ..., for n=11, 13, 15, 17, ... . Evidently, b(n) is constructed by the concatenation of 111 with repeated bit patterns of 100101 (=37) truncated to 4+(n-9)/2 digits. As a result, the number of contiguous palindromic bit patterns of b(n) is A206925(111_2) + 3 + A206925(A206927((n-9)/2)) = 6 + 3 + n - 9 = n. This proves that there is always a number with n contiguous palindromic bit patterns.",
				"a(5)=0, and this is the only zero term. Proof: The inequality A206925(n) \u003e= 2*floor(log_2(n)) (cf. A206925) implies A206925(n) \u003e 5 for n \u003e= 8. By direct search we find A206925(n)\u003c\u003e5 for n=1..7. Thus, there is no k with A206925(k)=5, which implies a(5)=0."
			],
			"link": [
				"Hieronymus Fischer, \u003ca href=\"/A217101/b217101.txt\"\u003eTable of n, a(n) for n = 1..300\u003c/a\u003e"
			],
			"formula": [
				"a(n) = min(k | A206925(k) = n), for n\u003c\u003e5.",
				"A206925(a(n)) = n, n\u003c\u003e5.",
				"a(n) \u003c= A217100(n), equality holds for n = 1, 2, 3 and 5, only.",
				"a(A000217(n))    = 2^n - 1.",
				"a(A000217(n)+1)  = 2^n.",
				"a(A000217(n)+3)  = 2^(n+1)+1, n\u003e2.",
				"a(A000217(n)+5)  = 2^(n+2)+2, n\u003e4.",
				"a(A000217(n)+6)  = 2^(n+3) - 2^n - 1, n\u003e5.",
				"a(A000217(n)+7)  = 2^(n+3)+5, n\u003e6.",
				"a(A000217(n)+8)  = 2^(n+3)+4, n\u003e7.",
				"a(A000217(n)+9)  = 2^(n+4)+11, n\u003e8.",
				"a(A000217(n)+10) = 2^(n+4) - 2^n - 1, n\u003e9.",
				"a(A000217(n)+11) = 21*2^n, n\u003e10.",
				"a(A000217(n)+12) = 2^(n+4)+8, n\u003e11.",
				"a(A000217(n)+13) = 2^(n+5)+18, n\u003e12."
			],
			"example": [
				"a(3) = 3, since 3=11_2 has 3 contiguous palindromic bit patterns, and this is the least such number.",
				"a(6) = 7. Since 7=111_2 has 6 contiguous palindromic bit patterns, and this is the least such number.",
				"a(8) = 18. Since 18=10010_2 has 8 contiguous palindromic bit patterns (1, 0, 0, 1, 0, 00, 010 and 1001), and this is the least such number.",
				"a(9) = 17. Since 17=10001_2 has 9 contiguous palindromic bit patterns (1, 0, 0, 0, 1, 00, 00, 000, and 10001), and this is the least such number."
			],
			"xref": [
				"Cf. A006995, A206923, A206924, A206925, A206926, A070939, A217100."
			],
			"keyword": "nonn,base",
			"offset": "1,2",
			"author": "_Hieronymus Fischer_, Jan 23 2013",
			"references": 4,
			"revision": 14,
			"time": "2021-09-23T01:27:02-04:00",
			"created": "2013-02-07T07:46:29-05:00"
		}
	]
}