{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A057660",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 57660,
			"data": "1,3,7,11,21,21,43,43,61,63,111,77,157,129,147,171,273,183,343,231,301,333,507,301,521,471,547,473,813,441,931,683,777,819,903,671,1333,1029,1099,903,1641,903,1807,1221,1281,1521,2163,1197,2101,1563,1911,1727",
			"name": "a(n) = Sum_{k=1..n} n/gcd(n,k).",
			"comment": [
				"Also sum of the orders of the elements in a cyclic group with n elements, i.e., row sums of A054531. - Avi Peretz (njk(AT)netvision.net.il), Mar 31 2001",
				"Also inverse Moebius transform of EulerPhi(n^2), A002618.",
				"Sequence is multiplicative with a(p^e) = (p^(2*e+1)+1)/(p+1). Example: a(10) = a(2)*a(5) = 3*21 = 63.",
				"The lowest outliers, such that a(n)/((n-1)*n + 1) is a record low, seems to be given, except for 4, by A051451(n, n \u003e= 3) = {6, 12, 60, 420, 840, 2520, 27720, ...}. If true, is there a proof? - _Daniel Forgues_, May 04 2013",
				"a(n) is the number of pairs (a, b) such that the equation ax = b is solvable in the ring (Zn, +, x). See the Mathematical Reflections link. - _Michel Marcus_, Jan 07 2017"
			],
			"reference": [
				"David M. Burton, Elementary Number Theory, Allyn and Bacon Inc., Boston MA, 1976, p. 152.",
				"H. W. Gould and Temba Shonhiwa, Functions of GCD's and LCM's, Indian J. Math. (Allahabad), Vol. 39, No. 1 (1997), pp. 11-35.",
				"H. W. Gould and Temba Shonhiwa, A generalization of Cesaro's function and other results, Indian J. Math. (Allahabad), Vol. 39, No. 2 (1997), pp. 183-194."
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A057660/b057660.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (terms 1..1000 from T. D. Noe)",
				"H. Amiri and S. M. Jafarian Amiri, \u003ca href=\"http://dx.doi.org/10.1142/S0219498811004057\"\u003eSum of element orders on finite groups of the same order\u003c/a\u003e, J. Algebra Appl. 10 (2011), no. 2, 187--190. MR2795731 (2012d:20050)",
				"Habib Amiri, S. M. Jafarian Amiri and I. M. Isaacs, \u003ca href=\"http://dx.doi.org/10.1080/00927870802502530\"\u003eSums of element orders in finite groups\u003c/a\u003e Comm. Algebra 37 (2009), no. 9, 2978--2980. MR2554185 (2010i:20022)",
				"Miriam Mahannah El-Farrah, \u003ca href=\"https://digitalcommons.wku.edu/theses/1518/\"\u003eExpectation Numbers of Cyclic Groups\u003c/a\u003e, MS Thesis, Western Kentucky University, August 2015.",
				"Steven R. Finch, \u003ca href=\"https://doi.org/10.1017/9781316997741\"\u003eMathematical Constants II\u003c/a\u003e, Encyclopedia of Mathematics and Its Applications, Cambridge University Press, Cambridge, 2018, p. 154-5.",
				"Walther Janous, \u003ca href=\"http://www.jstor.org/stable/2695482\"\u003eProblem 10829\u003c/a\u003e, Amer. Math. Monthly, 107 (2000), p. 753.",
				"Yadollah Marefat, Ali Iranmanesh and Abolfazl Tehranian, \u003ca href=\"http://dx.doi.org/10.1142/S0219498813500266\"\u003eOn the sum of element orders of finite simple groups\u003c/a\u003e, J. Algebra Applications, 12 (2013), #1350026.",
				"Mathematical Reflections, \u003ca href=\"https://www.awesomemath.org/wp-pdf-files/math-reflections/mr-2015-04/mr_3_2015_solutions.pdf\"\u003eSolution to Problem U338\u003c/a\u003e, Issue 4, 2015, p 17.",
				"Joachim von zur Gathen, Arnold Knopfmacher, Florian Luca, Lutz G. Lucht and Igor E. Shparlinski, \u003ca href=\"http://jtnb.cedram.org/item?id=JTNB_2004__16_1_107_0\"\u003eAverage order of cyclic groups\u003c/a\u003e, J. Théorie Nombres Bordeaux 16 (1) (2004) 107-123."
			],
			"formula": [
				"a(n) = Sum_{d|n} d*A000010(d) = Sum_{d|n} d*A054522(n,d), sum of d times phi(d) for all divisors d of n, where phi is Euler's phi function.",
				"a(n) = sigma_2(n^2)/sigma_1(n^2) = A001157(A000290(n))/A000203(A000290(n)) = A001157(A000290(n))/A065764(n). - _Labos Elemer_, Nov 21 2001",
				"a(n) = Sum_{d|n} A000010(d^2). - _Enrique Pérez Herrero_, Jul 12 2010",
				"a(n) \u003c= (n-1)*n + 1, with equality if and only if n is noncomposite. - _Daniel Forgues_, Apr 30 2013",
				"G.f.: Sum_{n \u003e= 1} n*phi(n)*x^n/(1 - x^n) = x + 3*x^2 + 7*x^3 + 11*x^4 + .... Dirichlet g.f.: sum {n \u003e= 1} a(n)/n^s = zeta(s)*zeta(s-2)/zeta(s-1) for Re s \u003e 3.  Cf. A078747 and A176797. - _Peter Bala_, Dec 30 2013",
				"a(n) = Sum_{i=1..n} numerator(n/i). - _Wesley Ivan Hurt_, Feb 26 2017",
				"L.g.f.: -log(Product_{k\u003e=1} (1 - x^k)^phi(k)) = Sum_{n\u003e=1} a(n)*x^n/n. - _Ilya Gutkovskiy_, May 21 2018",
				"From _Richard L. Ollerton_, May 10 2021: (Start)",
				"a(n) = Sum_{k=1..n} lcm(n,k)/k.",
				"a(n) = Sum_{k=1..n} gcd(n,k)*phi(gcd(n,k))/phi(n/gcd(n,k)). (End)",
				"From _Vaclav Kotesovec_, Jun 13 2021: (Start)",
				"Sum_{k=1..n} a(k)/k ~ 3*zeta(3)*n^2/Pi^2.",
				"Sum_{k=1..n} k^2/a(k) ~ A345294 * n.",
				"Sum_{k=1..n} k*A000010(k)/a(k) ~ A345295 * n. (End)"
			],
			"mathematica": [
				"Table[ DivisorSigma[ 2, n^2 ] / DivisorSigma[ 1, n^2 ], {n, 1, 128} ]",
				"Table[Total[Denominator[Range[n]/n]], {n, 55}] (* _Alonso del Arte_, Oct 07 2011 *)",
				"f[p_, e_] := (p^(2*e + 1) + 1)/(p + 1); a[n_] := Times @@ (f @@@ FactorInteger[n]); Array[a, 100] (* _Amiram Eldar_, Nov 21 2020 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c1,0,sumdiv(n,d,d*eulerphi(d)))",
				"(PARI) a(n)=sumdivmult(n,d, eulerphi(d)*d) \\\\ _Charles R Greathouse IV_, Sep 09 2014",
				"(Haskell)",
				"a057660 n = sum $ map (div n) $ a050873_row n",
				"-- _Reinhard Zumkeller_, Nov 25 2013"
			],
			"xref": [
				"Cf. A000010, A000203, A000290, A001157, A018804, A050873, A051193, A054522, A057661, A061255, A065764, A078747, A174405, A176797, A226512."
			],
			"keyword": "easy,nice,nonn,mult",
			"offset": "1,2",
			"author": "_Henry Gould_, Oct 15 2000",
			"ext": [
				"More terms from _James A. Sellers_, Oct 16 2000"
			],
			"references": 53,
			"revision": 112,
			"time": "2021-06-13T10:32:26-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}