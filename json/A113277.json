{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A113277",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 113277,
			"data": "1,2,0,0,0,-4,0,0,-5,0,0,0,0,0,0,0,7,0,0,0,0,8,0,0,0,0,0,0,0,0,0,0,0,-10,0,0,0,0,0,0,-11,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,13,0,0,0,0,0,0,0,0,14,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,-16,0,0,0,0,0,0,0,0,0,0,-17,0,0,0,0",
			"name": "Expansion of q^(-1/3) * eta(q^2)^5 / eta(q)^2 in powers of q.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A113277/b113277.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Y. Martin and K. Ono, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9939-97-03928-2\"\u003eEta-quotients and elliptic curves\u003c/a\u003e, Proc. Amer. Math. Soc. 125 (1997), no. 11, 3169-3176. MR1401749 (97m:11057)",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"a(n) = b(3*n + 1) where b(n) is multiplicative and a(p^e) = 0 if e is odd, a(3^e) = 0^e, a(2^e) = -(-2)^(e/2), a(p^e) = p^(e/2) if p == 1 (mod 3), a(p^e) = (-p)^(e/2) if p == 2 (mod 3).",
				"Euler transform of period 2 sequence [ 2, -3, ...].",
				"G.f.: Sum_{k} (3*k + 1) * (-x)^(3*k^2 + 2*k) = Product_{k\u003e0} (1 - x^k)^3 * (1 + x^k)^5.",
				"Expansion of psi(x^2) * f(x)^2 = phi(x) * f(-x^4)^2 in powers of x where phi(), psi(), f() are Ramanujan theta functions.",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (48 t)) = 3456^(1/2) (t/i)^(3/2) g(t) where q = exp(2 Pi i t) and g() is g.f. for A080332.",
				"a(4*n + 2) = a(4*n + 3) = a(5*n + 2) = a(5*n + 4) = a(8*n + 4) = 0. a(25*n + 8) = -5 * a(n). A114855(n) = (-1)^n * a(n). a(4*n + 1) = 2 * A114855(n). a(8*n) = A080332(n)."
			],
			"example": [
				"1 + 2*x - 4*x^5 - 5*x^8 + 7*x^16 + 8*x^21 - 10*x^33 - 11*x^40 + 13*x^56 + ...",
				"q + 2*q^4 - 4*q^16 - 5*q^25 + 7*q^49 + 8*q^64 - 10*q^100 - 11*q^121 +..."
			],
			"mathematica": [
				"QP = QPochhammer; s = QP[q^2]^5/QP[q]^2 + O[q]^100; CoefficientList[s, q] (* _Jean-François Alcover_, Nov 25 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = local(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A)^5 / eta(x + A)^2, n))}",
				"(PARI) {a(n) = if(issquare( 3*n + 1,\u0026n), n * (-1)^(n%3 + n), 0)}",
				"(PARI) {a(n) = local(A, p, e); if( n\u003c0, 0, n = 3*n + 1; A = factor(n); prod (k=1, matsize(A)[1], if( p=A[k,1], e=A[k,2]; if( e%2, 0, (-1)^(p==2) * (-(-1)^(p%3) * p)^(e/2)))))}"
			],
			"xref": [
				"Cf. A080332, A114855."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Oct 21 2005",
			"references": 4,
			"revision": 22,
			"time": "2021-03-12T22:24:43-05:00",
			"created": "2006-01-24T03:00:00-05:00"
		}
	]
}