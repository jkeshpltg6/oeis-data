{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A245824",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 245824,
			"data": "1,4,14,49,86,301,454,886,1589,1849,3101,3986,6418,13766,9761,13951,19049,22463,26798,31754,48181,57026,75266,128074,298154,51529,85699,93793,100561,111139,137987,196249,199591,203878,263431,295969",
			"name": "Triangle read by rows: row n\u003e=1 contains in increasing order the Matula numbers of the rooted binary trees with n leaves.",
			"comment": [
				"The Matula-Goebel number of a rooted tree can be defined in the following recursive manner: to the one-vertex tree there corresponds the number 1; to a tree T with root degree 1 there corresponds the t-th prime number, where t is the Matula-Goebel number of the tree obtained from T by deleting the edge emanating from the root; to a tree T with root degree m\u003e=2 there corresponds the product of the Matula-Goebel numbers of the m branches of T.",
				"Row n contains A001190(n) entries (the Wedderburn-Etherington numbers)."
			],
			"link": [
				"Gus Wiseman, \u003ca href=\"/A245824/b245824.txt\"\u003eTable of n, a(n) for n = 1..94\u003c/a\u003e",
				"Emeric Deutsch, \u003ca href=\"http://arxiv.org/abs/1111.4288\"\u003eTree statistics from Matula numbers\u003c/a\u003e, arXiv:1111.4288 [math.CO], (18-November-2011)",
				"Emeric Deutsch, \u003ca href=\"http://dx.doi.org/10.1016/j.dam.2012.05.012\"\u003eRooted tree statistics from Matula numbers\u003c/a\u003e, Discrete Appl. Math., 160, 2012, 2314-2322.",
				"F. Goebel, \u003ca href=\"http://dx.doi.org/10.1016/0095-8956(80)90049-0\"\u003eOn a 1-1-correspondence between rooted trees and natural numbers\u003c/a\u003e, J. Combin. Theory, B 29 (1980), 141-143.",
				"I. Gutman and A. Ivic, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(95)00182-V\"\u003eOn Matula numbers\u003c/a\u003e, Discrete Math., 150, 1996, 131-142.",
				"I. Gutman and Yeong-Nan Yeh, \u003ca href=\"http://www.emis.de/journals/PIMB/067/3.html\"\u003eDeducing properties of trees from their Matula numbers\u003c/a\u003e, Publ. Inst. Math., 53 (67), 1993, 17-22.",
				"D. Matula, \u003ca href=\"http://www.jstor.org/stable/2027327\"\u003eA natural rooted tree enumeration by prime factorization\u003c/a\u003e, SIAM Rev. 10 (1968) 273."
			],
			"formula": [
				"Let H[n] denote the set of binary rooted trees with n leaves or, with some abuse, the set of their Matula numbers (for example, H[1]={1}, H[2]={4}). Each binary rooted tree with n leaves is obtained by identifying the roots of an \"elevated\" tree from H[k] and of an \"elevated\" tree from H[n-k] (k=1,..., floor(n/2)). The Maple program is based on this. It makes use of the fact that the Matula number of the \"elevation\" of a rooted tree with Matula number q has Matula number equal to the q-th prime. The shown program determines H[m] for m=3...9 but shows only H[9]."
			],
			"example": [
				"Row 2 is: 4 (the Matula number of the rooted tree V)",
				"Triangle starts:",
				"1;",
				"4;",
				"14;",
				"49, 86;",
				"301, 454, 886;",
				"1589, 1849, 3101, 3986, 6418, 13766;"
			],
			"mathematica": [
				"nn=9;",
				"allbin[n_]:=allbin[n]=If[n===1,{{}},Join@@Function[c,Union[Sort/@Tuples[allbin/@c]]]/@Select[IntegerPartitions[n-1],Length[#]===2\u0026]];",
				"MGNumber[{}]:=1;MGNumber[x:{__}]:=Times@@Prime/@MGNumber/@x;",
				"Table[Sort[MGNumber/@allbin[n]],{n,1,2nn,2}] (* _Gus Wiseman_, Aug 28 2017 *)"
			],
			"xref": [
				"Cf. A000081, A001190, A007097, A061773, A111299 (the ordered sequence of all numbers appearing in this sequence), A280994."
			],
			"keyword": "nonn,tabf",
			"offset": "1,2",
			"author": "_Emeric Deutsch_, Aug 02 2014",
			"ext": [
				"Ordering of terms corrected by _Gus Wiseman_, Aug 29 2017"
			],
			"references": 3,
			"revision": 28,
			"time": "2017-08-29T14:49:24-04:00",
			"created": "2014-08-02T16:56:48-04:00"
		}
	]
}