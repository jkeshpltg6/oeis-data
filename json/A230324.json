{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A230324",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 230324,
			"data": "2,1,-1,-2,1,6,-3,-34,17,310,-155,-4146,2073,76454,-38227,-1859138,929569,57641238,-28820619,-2219305810,1109652905,103886563462,-51943281731,-5810302084962,2905151042481,382659344967926",
			"name": "a(n) = A226158(n) - 2*A226158(n+1).",
			"comment": [
				"The array A(n,k) = A(n-1,k+1) - A(n-1,k) of the sequence in the first row and higher-order sequences in followup rows starts:",
				"   2,  1,  -1,   -2,     1,     6,    -3, ...",
				"  -1, -2,  -1,    3,     5,    -9,   -31, ...",
				"  -1,  1,   4,    2,   -14,   -22,    82, ...",
				"   2,  3,  -2,  -16,    -8,   104,   160, ...",
				"   1, -5, -14,    8,   112,    56, -1160, ...",
				"  -6, -9,  22,  104,   -56, -1216,  -608, ...",
				"  -3, 31,  82, -160, -1160,   608, 18880, ...",
				"etc.",
				"a(n) is an autosequence: Its inverse binomial transform is the sequence (up to a sign), which means top row and left column in the difference array have the same absolute values.",
				"The main diagonal is the double of the first upper diagonal: A(n,n) = 2*A(n,n+1).",
				"A(n,n+1) = (-1)^n*A005439(n), which also appears as the first upper diagonal of the difference array of A226158(n)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A230324/b230324.txt\"\u003eTable of n, a(n) for n = 0..500\u003c/a\u003e",
				"OEIS Wiki, \u003ca href=\"https://oeis.org/wiki/Autosequence\"\u003eAutosequence\u003c/a\u003e"
			],
			"formula": [
				"a(n)/2 + A164555(n)/A027642(n) = 2*A225825(n)/A141056(n)."
			],
			"example": [
				"a(0) =  0 - 2 * (-1) =  2,",
				"a(1) = -1 - 2 * (-1) =  1,",
				"a(2) = -1 - 2 *   0  = -1,",
				"a(3) =  0 - 2 *   1  = -2,",
				"a(4) =  1 - 2 *   0  =  1,",
				"a(5) =  0 - 2 * (-3) =  6."
			],
			"maple": [
				"A226158 := proc(n)",
				"    if n = 0 then",
				"        0;",
				"    else",
				"        Zeta(1-n)*2*n*(2^n-1) ;",
				"    end if;",
				"end proc:",
				"A230324 := proc(n)",
				"    A226158(n)-2*A226158(n+1) ;",
				"end proc: # _R. J. Mathar_, Oct 28 2013"
			],
			"mathematica": [
				"a[0] = 2; a[1] = 1; a[n_] := n EulerE[n-1, 0] - 2 (n+1) EulerE[n, 0];",
				"Table[a[n], {n, 0, 25}] (* _Jean-François Alcover_, Jun 07 2017 *)"
			],
			"xref": [
				"Cf. A050946."
			],
			"keyword": "sign",
			"offset": "0,1",
			"author": "_Paul Curtz_, Oct 16 2013",
			"references": 4,
			"revision": 22,
			"time": "2018-01-19T19:35:24-05:00",
			"created": "2013-10-29T05:15:07-04:00"
		}
	]
}