{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A137569",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 137569,
			"data": "1,-1,-1,1,-1,0,2,-1,-1,3,-2,-1,4,-3,-2,5,-4,-2,8,-6,-4,10,-7,-4,14,-10,-6,18,-13,-7,24,-17,-10,30,-21,-12,40,-28,-17,49,-35,-19,63,-44,-26,78,-55,-31,98,-69,-40,120,-84,-47,150,-105,-61,182,-127,-71",
			"name": "Expansion of f(-x) / f(-x^3) in powers of x where f() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A137569/b137569.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(1/12) eta(q) / eta(q^3) in powers of q.",
				"Euler transform of period 3 sequence [ -1, -1, 0, ...].",
				"Given g.f. A(x) then B(q) = A(q^6)^2 / q satisfies 0 = f(B(q), B(q^2), B(q^4)) where f(u, v, w) = 4*v^2 + (u^2 - v) * (w^2 + v).",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (432 t)) = 3^(1/2) g(t) where q = exp(2 Pi i t) and g() is the g.f. for A000726.",
				"G.f.: Product_{k\u003e0} (1 - x^(3*k-1)) * (1 - x^(3*k-2)).",
				"a(3*n) = A035943(n). a(3*n + 1) = - A035941(n). a(3*n + 2) = - A035940(n).",
				"Convolution inverse of A000726.",
				"Convolution square is A112157. Convolution 4th power is A058095. - _Michael Somos_, Oct 08 2015",
				"a(2*n) = A263050(n). a(2*n + 1) = - A263051(n).  - _Michael Somos_, Oct 08 2015",
				"G.f.: (Product_{k\u003e0} (1 + x^k + x^(2*k)))^-1. - _Michael Somos_, Oct 08 2015",
				"a(n) = -(1/n)*Sum_{k=1..n} A046913(k)*a(n-k), a(0) = 1. - _Seiichi Manyama_, Mar 25 2017"
			],
			"example": [
				"G.f. = 1 - x - x^2 + x^3 - x^4 + 2*x^6 - x^7 - x^8 + 3*x^9 - 2*x^10 - x^11 + ...",
				"G.f. = 1/q - q^11 - q^23 + q^35 - q^47 + 2*q^71 - q^83 - q^95 + 3*q^107 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ x] / QPochhammer[ x^3], {x, 0, n}]; (* _Michael Somos_, Oct 08 2015 *)",
				"a[ n_] := SeriesCoefficient[ QPochhammer[ x, x^3] QPochhammer[ x^2, x^3], {x, 0, n}]; (* _Michael Somos_, Oct 08 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A) / eta(x^3 + A), n))};"
			],
			"xref": [
				"Cf. A000726, A035940, A035941, A035943, A058095, A112157, A263050, A263051."
			],
			"keyword": "sign",
			"offset": "0,7",
			"author": "_Michael Somos_, Jan 26 2008",
			"references": 9,
			"revision": 17,
			"time": "2021-03-12T22:24:45-05:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}