{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A272199",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 272199,
			"data": "1,2,-9,-44,29,630,883,-6424,-24327,34858,385967,318780,-4380011,-12904162,31131819,230017744,55321841,-2879586990,-6478357913,24477915044,133174482957,-51863929658,-1834996137757,-2995761189960,17863427410921,74671750291322",
			"name": "Expansion of 1/(1 - 2*x + 13*x^2).",
			"comment": [
				"a(n) gives the coefficient c(13^n) of (eta(z^6))^4, a modular cusp form of weight 2, when expanded in powers of q = exp(2*Pi*i*z), Im(z) \u003e 0, assuming alpha-multiplicativity (not valid for p = 2 and 3) with alpha(x) = x (weight 2) and input c(13) = +2. Eta is the Dedekind function. See the Apostol reference, p. 138, eq. (54) for alpha-multiplicativity and p. 130, eq. (39) with k=2. See also A000727 where a(n)=c(13^n) = A000727((13^n-1)/6)=A000727(2*A091030(n)), n \u003e= 0. For the proof that alpha-multiplicativity leads to the recurrence involving Chebyshev's S polynomials see a comment on A168175, and the Apostol reference, Exercise 6., p. 139."
			],
			"reference": [
				"Tom M. Apostol, Modular Functions and Dirichlet Series in Number Theory, Second edition, Springer, 1990, pp. 130, 138 - 139."
			],
			"link": [
				"Indranil Ghosh, \u003ca href=\"/A272199/b272199.txt\"\u003eTable of n, a(n) for n = 0..1790\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,-13).",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e"
			],
			"formula": [
				"G.f.: 1/(1 - 2*x + 13*x^2).",
				"a(n) = 2*a(n-1) - 13*a(n-2), a(-1) = 0, a(0) = 1.",
				"a(n) = sqrt(13)^n * S(n, 2/sqrt(13)), n \u003e= 0, with Chebyshev's S polynomials (A049310).",
				"a(n) = (Ap^(n+1) - Am^(n+1))/(Ap - Am) with Ap:= 1 + 2*sqrt(3)*i and Am = 1 - 2*sqrt(3)*i, (Binet - de Moivre formula), where i is the imaginary unit.",
				"E.g.f.: (sqrt(3)*sin(2*sqrt(3)*x) + 6*cos(2*sqrt(3)*x))*exp(x)/6. - _Ilya Gutkovskiy_, Apr 27 2016"
			],
			"example": [
				"a(2) = c(13^2) = A000727(2*A091030(2)) =",
				"A000727(28) = -9."
			],
			"maple": [
				"a:=series(1/(1-2*x+13*x^2),x=0,26): seq(coeff(a,x,n),n=0..25); # _Paolo P. Lava_, Mar 27 2019"
			],
			"mathematica": [
				"CoefficientList[Series[1/(1 - 2 x + 13 x^2), {x, 0, 25}], x] (* _Michael De Vlieger_, Apr 27 2016 *)",
				"LinearRecurrence[{2, -13}, {1, 2}, 30] (* _Vincenzo Librandi_, Nov 25 2016 *)"
			],
			"program": [
				"(PARI) Vec(1/(1-2*x+13*x^2) + O(x^99)) \\\\ _Altug Alkan_, Apr 28 2016",
				"(MAGMA) I:=[1,2]; [n le 2 select I[n] else 2*Self(n-1)-13*Self(n-2): n in [1..30]]; // _Vincenzo Librandi_, Nov 25 2016"
			],
			"xref": [
				"Cf. A023000, A049310, A168175."
			],
			"keyword": "sign,easy",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Apr 27 2016",
			"references": 1,
			"revision": 34,
			"time": "2019-03-27T09:59:45-04:00",
			"created": "2016-04-30T17:06:45-04:00"
		}
	]
}