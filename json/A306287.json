{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A306287",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 306287,
			"data": "1,0,3,2,1,2,1,1,0,3,0,1,0,3,3,2,3,2,1,2,1,0,1,2,2,3,2,1,1,0,1,2,1,1,0,3,0,1,0,3,3,2,3,0,0,0,1,2,1,1,0,3,0,1,0,3,3,2,3,0,3,3,2,1,2,2,3,0,3,2,3,2,1,2,1,0,1,2,2,3,2,1,1,1,0",
			"name": "Irregular triangle T(n,k), 1 \u003c= n, 1 \u003c= k \u003c= (1/6)*(4+5*2^(2*n)), read by rows: T(n,k) determines absolute directions along the perimeter of the n-th Y-type Hilbert Tree.",
			"comment": [
				"The Y-type Hilbert trees are a sequence of polyominoes whose edges, all but one, are segments of the Hilbert curve described by A163540. One extra edge closes a loop around the perimeter (cf. Formula). The first Y-type tree is a monomino with four edges, and the second is the Y hexomino with 14 unit edges. All deeper trees are determined by iteration of replacement rules (cf. linked image \"First Six Y-type Trees\"). The Y-type Hilbert trees nest along the upper half plane according to the limit-periodic ruler function A001511. Such an arrangement reconstructs the Hilbert curve everywhere away from the ground axis (cf. linked image \"Limit-Periodic Construction\")."
			],
			"link": [
				"Bradley Klee, \u003ca href=\"/A306287/a306287.png\"\u003eLimit-Periodic Construction\u003c/a\u003e.",
				"Bradley Klee, \u003ca href=\"/A306287/a306287_1.png\"\u003eFirst Six Y-type Trees\u003c/a\u003e."
			],
			"formula": [
				"a(n,(1/6)*(4+5*2^(2*n))) = 2;",
				"a(n,k) = A163540( (1/12)*(8+7*2^(2*n)-3*(-1)^n *2^(2*n+1))-1+k )."
			],
			"example": [
				"T(1,k) = 1, 0, 3, 2;",
				"T(2,k) = 1, 2, 1, 1, 0, 3, 0, 1, 0, 3, 3, 2, 3, 2."
			],
			"mathematica": [
				"HC = {L[n_ /; EvenQ[n]] :\u003e {F[n], L[n], L[Mod[n + 1, 2]], R[n]},",
				"   R[n_ /; OddQ[n]] :\u003e {F[n], R[n], R[Mod[n + 1, 2]], L[n]},",
				"   R[n_ /; EvenQ[n]] :\u003e {L[n], R[Mod[n + 1, 2]], R[n], F[Mod[n + 1, 2]]},",
				"   L[n_ /; OddQ[n]] :\u003e {R[n], L[Mod[n + 1, 2]], L[n], F[Mod[n + 1, 2]]},",
				"   F[n_ /; EvenQ[n]] :\u003e {L[n], R[Mod[n + 1, 2]], R[n], L[Mod[n + 1, 2]]},",
				"   F[n_ /; OddQ[n]] :\u003e {R[n], L[Mod[n + 1, 2]], L[n], R[Mod[n + 1, 2]]}};",
				"TurnMap = {F[_] -\u003e 0, L[_] -\u003e 1, R[_] -\u003e -1};",
				"T1ind[1] = 1; T1ind[2] = 2; T1ind[n_] := 5*T1ind[n - 1] - 4*T1ind[n - 2];",
				"T1Vec[n_] := Append[Mod[FoldList[Plus, Flatten[Nest[# /. HC \u0026, F[0],",
				"        n] /. TurnMap][[T1ind[n] ;; -(T1ind[n] + 1)]]], 4], 2]",
				"Flatten[T1Vec /@ Range[5]]"
			],
			"xref": [
				"T-Type Trees: A306288. Cf. A163540, A001511, A246559."
			],
			"keyword": "tabf,nonn",
			"offset": "1,3",
			"author": "_Bradley Klee_, Feb 03 2019",
			"references": 3,
			"revision": 16,
			"time": "2019-02-06T12:58:15-05:00",
			"created": "2019-02-06T12:58:15-05:00"
		}
	]
}