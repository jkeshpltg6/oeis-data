{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A319516",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 319516,
			"data": "1,1,1,2,1,1,3,4,3,1,7,2,9,3,1,8,13,3,15,2,3,7,19,4,5,9,9,6,25,1,27,16,7,13,3,6,33,15,9,4,37,3,39,14,3,19,43,8,21,5,13,18,49,9,7,12,15,25,55,2,57,27,9,32,9,7,63,26,19,3,67,12,69,33,5,30,21,9,75,8",
			"name": "Number of integers x such that 1 \u003c= x \u003c= n and gcd(x,n) = gcd(x+2,n) = gcd(x+6,n) = gcd(x+8,n) = 1.",
			"comment": [
				"Equivalently, a(n) is the number of \"admissible\" residue classes modulo n which are allowed (by divisibility considerations) to contain infinitely many initial primes in prime quadruples (p, p+2, p+6, p+8). This is a generalization of Euler's totient function: the number of residue classes modulo n containing infinitely many primes.",
				"If n is prime, a(n) = max(1,n-4)."
			],
			"reference": [
				"V. A. Golubev, Sur certaines fonctions multiplicatives et le problème des jumeaux. Mathesis 67 (1958), 11-20.",
				"J. Sándor, B. Crstici, Handbook of Number Theory, vol.II. Kluwer, 2004, p.289."
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A319516/b319516.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"V. A. Golubev, \u003ca href=\"http://dml.cz/dmlcz/117061\"\u003eA generalization of the functions phi(n) and pi(x)\u003c/a\u003e. Časopis pro pěstování matematiky 78 (1953), 47-48.",
				"V. A. Golubev, \u003ca href=\"http://dml.cz/dmlcz/117442\"\u003eExact formulas for the number of twin primes and other generalizations of the function pi(x)\u003c/a\u003e. Časopis pro pěstování matematiky 87 (1962), 296-305.",
				"Alexei Kourbatov and Marek Wolf, \u003ca href=\"http://arxiv.org/abs/1901.03785\"\u003ePredicting maximal gaps in sets of primes\u003c/a\u003e, arXiv preprint arXiv:1901.03785 [math.NT], 2019."
			],
			"formula": [
				"Multiplicative with a(p^e) = p^(e-1) if p \u003c= 5; (p-4)*p^(e-1) if p \u003e 5."
			],
			"example": [
				"Some prime quadruples start with a prime congruent to 1 mod 4; others start with a prime congruent to 3 mod 4; that is, there are 2 \"admissible\" residue classes mod 4; therefore a(4)=2. All initial primes in prime quadruples are 5 mod 6; that is, there is only one \"admissible\" residue class mod 6; therefore a(6) = 1."
			],
			"mathematica": [
				"a[n_] := Sum[Boole[CoprimeQ[n, x] \u0026\u0026 CoprimeQ[n, x+2] \u0026\u0026 CoprimeQ[n, x+6] \u0026\u0026 CoprimeQ[n, x+8]], {x, 1, n}]; Array[a, 80] (* _Jean-François Alcover_, Jan 29 2019 *)",
				"f[p_, e_] := If[p \u003c 7, p^(e-1), (p-4)*p^(e-1)]; a[1] = 1; a[n_] := Times @@ f @@@ FactorInteger[n]; Array[a, 100] (* _Amiram Eldar_, Jan 22 2020 *)"
			],
			"program": [
				"(PARI) phi4(n) = sum(x=1, n, (gcd(n,x)==1) \u0026\u0026 (gcd(n,x+2)==1) \u0026\u0026 (gcd(n,x+6)==1) \u0026\u0026 (gcd(n,x+8)==1));",
				"for(n=1,80,print1(phi4(n)\",\"))"
			],
			"xref": [
				"Cf. similar generalizations of totient for k-tuples: A002472 (k=2), A319534 (k=3), A321029 (k=5), A321030 (k=6)."
			],
			"keyword": "nonn,mult",
			"offset": "1,4",
			"author": "_Alexei Kourbatov_, Sep 21 2018",
			"references": 5,
			"revision": 38,
			"time": "2020-01-22T06:14:06-05:00",
			"created": "2018-10-24T22:34:49-04:00"
		}
	]
}