{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A061554",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 61554,
			"data": "1,1,1,2,1,1,3,3,1,1,6,4,4,1,1,10,10,5,5,1,1,20,15,15,6,6,1,1,35,35,21,21,7,7,1,1,70,56,56,28,28,8,8,1,1,126,126,84,84,36,36,9,9,1,1,252,210,210,120,120,45,45,10,10,1,1,462,462,330,330,165,165,55,55,11,11,1,1",
			"name": "Square table read by antidiagonals: a(n,k) = binomial(n+k, floor(k/2)).",
			"comment": [
				"Equivalently, a triangle read by rows, where the rows are obtained by sorting the elements of the rows of Pascal's triangle (A007318) into descending order. - _Philippe Deléham_, May 21 2005",
				"Equivalently, as a triangle read by rows, this is T(n,k)=binomial(n,floor((n-k)/2)); column k then has e.g.f. Bessel_I(k,2x)+Bessel_I(k+1,2x). - _Paul Barry_, Feb 28 2006",
				"Antidiagonal sums are A037952(n+1) = C(n+1,[n/2]). Matrix inverse is the row reversal of triangle A066170. Eigensequence is A125094(n) = Sum_{k=0..n-1} A125093(n-1,k)*A125094(k). - _Paul D. Hanna_, Nov 20 2006",
				"Riordan array (1/(1-x-x^2*c(x^2)),x*c(x^2)); where c(x)=g.f.for Catalan numbers A000108. - _Philippe Deléham_, Mar 17 2007",
				"Triangle T(n,k), 0\u003c=k\u003c=n, read by rows given by: T(0,0)=1, T(n,k)=0 if k\u003c0 or if k\u003en, T(n,0)=T(n-1,0)+T(n-1,1), T(n,k)=T(n-1,k-1)+T(n-1,k+1) for k\u003e=1. - _Philippe Deléham_, Mar 27 2007",
				"This triangle belongs to the family of triangles defined by: T(0,0)=1, T(n,k)=0 if k\u003c0 or if k\u003en, T(n,0)=x*T(n-1,0)+T(n-1,1), T(n,k)=T(n-1,k-1)+y*T(n-1,k)+T(n-1,k+1) for k\u003e=1. Other triangles arise by choosing different values for (x,y): (0,0) -\u003e A053121; (0,1) -\u003e A089942; (0,2) -\u003e A126093; (0,3) -\u003e A126970; (1,0)-\u003e A061554; (1,1) -\u003e A064189; (1,2) -\u003e A039599; (1,3) -\u003e A110877; ((1,4) -\u003e A124576; (2,0) -\u003e A126075; (2,1) -\u003e A038622; (2,2) -\u003e A039598; (2,3) -\u003e A124733; (2,4) -\u003e A124575; (3,0) -\u003e A126953; (3,1) -\u003e A126954; (3,2) -\u003e A111418; (3,3) -\u003e A091965; (3,4) -\u003e A124574; (4,3) -\u003e A126791; (4,4) -\u003e A052179; (4,5) -\u003e A126331; (5,5) -\u003e A125906. - _Philippe Deléham_, Sep 25 2007",
				"T(n,k) is the number of paths from (0,k) to some (n,m) which never dip below y=0, touch y=0 at least once and are made up only of the steps (1,1) and (1,-1). This can be proved using the recurrence supplied by Deléham. - _Gerald McGarvey_, Oct 15 2008",
				"Triangle read by rows = partial sums of A053121 terms starting from the right. - _Gary W. Adamson_, Oct 24 2008",
				"As a subset of the \"family of triangles\" (Deleham comment of Sep 25 2007), beginning with a signed variant of A061554, M = (-1,0) =  (1; -1, 1; 2, -1, 1; -3, 3, -1, 1;...) successive binomial transforms of M yield (0,1) - A089942; (1,2) - A039599; (2,3) - A124733; (3,4) - A124574; (4,5) - A126331; ... such that the binomial transform of the triangle generated from (n,n+1) = the triangle generated from (n+1,n+2). Similarly, another subset beginning with A053121 - (0,0), and taking successive binomial transforms yields (1,1) - A064189; (2,2) - A039598; (3,3) - A091965, ... By rows, the triangle generated from (n,n) can be obtained by taking pairwise sums from the (n-1,n) triangle starting from the right. For example, row 2 of (1,2) - A039599 = (2, 3, 1); and taking pairwise sums from the right we obtain (5, 4, 1) = row 2 of (2,2) - A039598. - _Gary W. Adamson_, Aug 04 2011",
				"The triangle by rows (n) with alternating signs (+-+...) from the top as a set of simultaneous equations solves for diagonal lengths of odd N (N = 2n+1) regular polygons. The constants in each case are powers of c = 2*cos(2*Pi/N). By way of example, the first 3 rows relate to the heptagon and the simultaneous equations are (1,0,0) = 1; (-1,1,0) = c = 1.24697...; and (2,-1,1) = c^2. The answers are 1, 2.24697..., and 1.801...; the 3 distinct diagonal lengths of the heptagon with edge = 1. - _Gary W. Adamson_, Sep 07 2011"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A061554/b061554.txt\"\u003eTable of n, a(n) for the first 50 rows, flattened\u003c/a\u003e",
				"Johann Cigler, \u003ca href=\"http://arxiv.org/abs/1501.04750\"\u003eSome remarks and conjectures related to lattice paths in strips along the x-axis\u003c/a\u003e, arXiv:1501.04750 [math.CO], 2015.",
				"Aoife Hennessy, \u003ca href=\"http://repository.wit.ie/1693/1/AoifeThesis.pdf\"\u003eA Study of Riordan Arrays with Applications to Continued Fractions, Orthogonal Polynomials and Lattice Paths\u003c/a\u003e, Ph. D. Thesis, Waterford Institute of Technology, Oct. 2011.",
				"Sun, Yidong; Ma, Luping \u003ca href=\"https://doi.org/10.1016/j.ejc.2014.01.004\"\u003eMinors of a class of Riordan arrays related to weighted partial Motzkin paths\u003c/a\u003e.  Eur. J. Comb. 39, 157-169 (2014), Table 2.2."
			],
			"formula": [
				"As a triangle: T(n,k) = binomial(n,m) where m = floor((n+1)/2 - (-1)^(n-k)*(k+1)/2).",
				"a(0, k) = binomial(k, floor(k/2)) = A001405(k); for n\u003e0 T(n, k) = T(n+1, k-2) + T(n-1, k).",
				"n-th row = M^n * V, where M = the infinite tridiagonal matrix with all 1's in the super and subdiagonals and (1,0,0,0,...) in the main diagonal. V = the infinite vector [1,0,0,0,...]. Example: (3,3,1,1,0,0,0,...) = M^3 * V. - _Gary W. Adamson_, Nov 04 2006",
				"Sum_{k=0..n} T(m,k)*T(n,k) = T(m+n,0) = A001405(m+n). - _Philippe Deléham_, Feb 26 2007",
				"Sum_{k=0..n} T(n,k)=2^n. - _Philippe Deléham_, Mar 27 2007",
				"Sum_{k=0..n} T(n,k)*x^k = A127361(n), A126869(n), A001405(n), A000079(n), A127358(n), A127359(n), A127360(n) for x = -2, -1, 0, 1, 2, 3, 4 respectively. - _Philippe Deléham_, Dec 04 2009"
			],
			"example": [
				"The array starts:",
				"   1, 1,  2,  3,  6, 10,  20,  35,   70,  126, ...",
				"   1, 1,  3,  4, 10, 15,  35,  56,  126,  210, ...",
				"   1, 1,  4,  5, 15, 21,  56,  84,  210,  330, ...",
				"   1, 1,  5,  6, 21, 28,  84, 120,  330,  495, ...",
				"   1, 1,  6,  7, 28, 36, 120, 165,  495,  715, ...",
				"   1, 1,  7,  8, 36, 45, 165, 220,  715, 1001, ...",
				"   1, 1,  8,  9, 45, 55, 220, 286, 1001, 1365, ...",
				"   1, 1,  9, 10, 55, 66, 286, 364, 1365, 1820, ...",
				"   1, 1, 10, 11, 66, 78, 364, 455, 1820, 2380, ...",
				"   1, 1, 11, 12, 78, 91, 455, 560, 2380, 3060, ...",
				"Triangle (antidiagonal) version begins:",
				"    1;",
				"    1,   1;",
				"    2,   1,   1;",
				"    3,   3,   1,   1;",
				"    6,   4,   4,   1,   1;",
				"   10,  10,   5,   5,   1,   1;",
				"   20,  15,  15,   6,   6,   1,  1;",
				"   35,  35,  21,  21,   7,   7,  1,  1;",
				"   70,  56,  56,  28,  28,   8,  8,  1,  1;",
				"  126, 126,  84,  84,  36,  36,  9,  9,  1,  1;",
				"  252, 210, 210, 120, 120,  45, 45, 10, 10,  1, 1;",
				"  462, 462, 330, 330, 165, 165, 55, 55, 11, 11, 1, 1; ...",
				"Matrix inverse begins:",
				"   1;",
				"  -1,  1;",
				"  -1, -1,   1;",
				"   1, -2,  -1,   1;",
				"   1,  2,  -3,  -1,  1;",
				"  -1,  3,   3,  -4, -1,  1;",
				"  -1, -3,   6,   4, -5, -1,  1;",
				"   1, -4,  -6,  10,  5, -6, -1,  1;",
				"   1,  4, -10, -10, 15,  6, -7, -1, 1; ...",
				"From _Paul Barry_, May 21 2009: (Start)",
				"Production matrix is",
				"  1, 1,",
				"  1, 0, 1,",
				"  0, 1, 0, 1,",
				"  0, 0, 1, 0, 1,",
				"  0, 0, 0, 1, 0, 1,",
				"  0, 0, 0, 0, 1, 0, 1,",
				"  0, 0, 0, 0, 0, 1, 0, 1 (End)"
			],
			"maple": [
				"T := proc(n, k) option remember;",
				"if n = k then 1 elif k \u003c 0 or n \u003c 0 or k \u003e n then 0",
				"elif k = 0 then T(n-1, 0) + T(n-1, 1) else T(n-1, k-1) + T(n-1, k+1) fi end:",
				"for n from 0 to 9 do seq(T(n, k), k = 0..n) od; # _Peter Luschny_, May 25 2021"
			],
			"mathematica": [
				"t[n_, k_] = Binomial[n, Floor[(n+1)/2 - (-1)^(n-k)*(k+1)/2]]; Flatten[Table[t[n, k], {n, 0, 11}, {k, 0, n}]] (* _Jean-François Alcover_, May 31 2011 *)"
			],
			"program": [
				"(PARI) T(n,k)=binomial(n,(n+1)\\2-(-1)^(n-k)*((k+1)\\2))"
			],
			"xref": [
				"Rows are A001405, A037952, A037955, A037951, A037956, A037953, A037957 etc. Columns are truncated pairs of A000012, A000027, A000217, A000292, A000332, A000389, A000579, etc. Main diagonal is alternate values of A051036.",
				"Cf. A007318, A107430, A125094, A037952, A066170."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_Henry Bottomley_, May 17 2001",
			"ext": [
				"Entry revised by _N. J. A. Sloane_, Nov 22 2006"
			],
			"references": 44,
			"revision": 66,
			"time": "2021-09-28T15:46:29-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}