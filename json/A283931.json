{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A283931",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 283931,
			"data": "1,63,511",
			"name": "Numbers n such that tau(2^n) = tau(2^n + 1).",
			"comment": [
				"tau(n) is the number of divisors of n (A000005).",
				"There are no other terms \u003c= 880.",
				"Numbers n such that A046798(n) = n + 1.",
				"Numbers n such that A000005(A000079(n)) = A000005(A000051(n)).",
				"Corresponding values of tau(2^n): 2, 64, 512, ...",
				"Corresponding pairs of numbers (2^n, 2^n + 1): (2, 3); (9223372036854775808, 9223372036854775809); ...",
				"From _Jon E. Schoenfield_, Mar 26 2017 [expanded Mar 30 2017]: (Start)",
				"All terms in the sequence are odd. (If n were an even number n = 2k, then tau(2^n) = n + 1 = 2k + 1 would be odd, but tau(2^n + 1) = tau(2^(2k) + 1) = tau(4^k + 1) would be even, since 4^k + 1 can never be a square.)",
				"2^881 + 1 may be difficult to factor completely, but it is divisible by each of the two primes 22907 and 58147, yet it is divisible by neither 22907^2 nor 58147^2, so each of those two primes will appear in the factorization of 2^881 + 1 with multiplicity 1, and thus tau(2^881 + 1) is some multiple of 4, whereas tau(2^881) = 882 = 2*441 is not, so 881 is not a term. Using partial factorizations of 2^n + 1 for values of n \u003e 881, it is not difficult to show that there are no more terms \u003c 1087.",
				"Each of the known terms a(1), a(2), and a(3) is of the form 2^j - 1 (so tau(2^n) = 2^j); the corresponding values of j are 1, 6, and 9. Are there additional terms of this form? The sequence does not include 2^10 - 1 = 1023; even without completely factoring 2^1023 + 1, it can be seen that 3 must appear in that factorization with a multiplicity of 2, so tau(2^1023 + 1) is divisible by 3, and thus cannot be 1024. Since 2^2047 + 1 is 3 * 179 * 2796203 * 53484017 * 62020897 * 18584774046020617 times a 576-digit composite (coprime to each of those 6 prime factors listed), 2047 will be a term iff that 576-digit composite has exactly 2048/2^6 = 32 divisors.",
				"(End)"
			],
			"example": [
				"For n = 1, tau(2) = tau(3) = 2."
			],
			"program": [
				"(MAGMA) [n: n in [0..500] | NumberOfDivisors(2^n) eq NumberOfDivisors(2^n + 1)]"
			],
			"xref": [
				"Cf. A000005, A000051, A000079, A046798, A283930."
			],
			"keyword": "nonn,more,bref",
			"offset": "1,2",
			"author": "_Jaroslav Krizek_, Mar 18 2017",
			"references": 1,
			"revision": 27,
			"time": "2019-07-28T11:00:51-04:00",
			"created": "2017-03-19T01:40:52-04:00"
		}
	]
}