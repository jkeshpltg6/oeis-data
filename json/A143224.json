{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A143224",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 143224,
			"data": "0,9,36,37,46,49,85,102,107,118,122,127,129,140,157,184,194,216,228,360,365,377,378,406,416,487,511,571,609,614,672,733,767,806,813,863,869,916,923,950,978,988,1249,1279,1280,1385,1427,1437,1483,1539,1551,1690",
			"name": "Numbers n such that (number of primes between n^2 and (n+1)^2) = (number of primes between n and 2n).",
			"comment": [
				"The sequence gives the positions of zeros in A143223. The number of primes in question is A143225(n).",
				"Legendre's conjecture (still open) says there is always a prime between n^2 and (n+1)^2. Bertrand's postulate (actually a theorem due to Chebyshev) says there is always a prime between n and 2n."
			],
			"reference": [
				"M. Aigner and C. M. Ziegler, Proofs from The Book, Chapter 2, Springer, NY, 2001.",
				"G. H. Hardy and E. M. Wright, An Introduction to the Theory of Numbers. 5th ed., Oxford Univ. Press, 1989, p. 19.",
				"S. Ramanujan, Collected Papers of Srinivasa Ramanujan (G. H. Hardy, S. Aiyar, P. Venkatesvara and B. M. Wilson, eds.), Amer. Math. Soc., Providence, 2000, pp. 208-209. [_Jonathan Sondow_, Aug 03 2008]"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A143224/b143224.txt\"\u003eTable of n, a(n) for n=1..97\u003c/a\u003e (no other n \u003c 10^6)",
				"T. Hashimoto, \u003ca href=\"http://arxiv.org/abs/0807.3690\"\u003eOn a certain relation between Legendre's conjecture and Bertrand's postulate\u003c/a\u003e, arXiv:0807.3690 [math.GM], 2008.",
				"M. Hassani, \u003ca href=\"http://arXiv.org/abs/math/0607096\"\u003eCounting primes in the interval (n^2,(n+1)^2)\u003c/a\u003e, arXiv:math/0607096 [math.NT], 2006.",
				"J. Pintz, \u003ca href=\"http://www.renyi.hu/~pintz/pjapr.pdf\"\u003eLandau's problems on primes\u003c/a\u003e",
				"S. Ramanujan, \u003ca href=\"http://ramanujan.sirinudi.org/Volumes/published/ram24.html\"\u003eA proof of Bertrand's postulate\u003c/a\u003e, J. Indian Math. Soc., 11 (1919), 181-182.",
				"J. Sondow, \u003ca href=\"http://mathworld.wolfram.com/RamanujanPrime.html\"\u003e Ramanujan Prime in MathWorld\u003c/a\u003e",
				"J. Sondow and E. W. Weisstein, \u003ca href=\"http://mathworld.wolfram.com/BertrandsPostulate.html\"\u003e Bertrand's Postulate in MathWorld\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/LegendresConjecture.html\"\u003e Legendre's Conjecture in MathWorld\u003c/a\u003e"
			],
			"formula": [
				"A143223(a(n)) = 0."
			],
			"example": [
				"There is the same number of primes (namely 3) between 9^2 and 10^2 as between 9 and 2*9, so 9 is a term."
			],
			"maple": [
				"with(numtheory): A143224:=n-\u003e`if`(pi((n+1)^2)-pi(n^2) = pi(2*n)-pi(n), n, NULL): seq(A143224(n), n=0..2000); # _Wesley Ivan Hurt_, Jul 25 2017"
			],
			"mathematica": [
				"L={}; Do[If[PrimePi[(n+1)^2]-PrimePi[n^2] == PrimePi[2n]-PrimePi[n], L=Append[L,n]], {n,0,2000}]; L",
				"(* Second program *)",
				"With[{nn = 2000}, {0}~Join~Position[#, {0}][[All, 1]] \u0026@ Map[Differences, Transpose@ {Differences@ Array[PrimePi[#^2] \u0026, nn], Array[PrimePi[2 #] - PrimePi[#] \u0026, nn - 1]}]] (* _Michael De Vlieger_, Jul 25 2017 *)"
			],
			"program": [
				"(PARI) is(n) = primepi((n+1)^2)-primepi(n^2)==primepi(2*n)-primepi(n) \\\\ _Felix Fröhlich_, Jul 25 2017"
			],
			"xref": [
				"See A000720, A014085, A060715, A143223, A143225, A143226.",
				"Cf. A104272, A143227. [_Jonathan Sondow_, Aug 03 2008]"
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Jonathan Sondow_, Jul 31 2008",
			"references": 9,
			"revision": 21,
			"time": "2017-07-27T08:13:46-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}