{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A343505",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 343505,
			"data": "1,1,2,2,4,1,6,3,6,2,60,2,120,3,3,6,1008,4,51480,1,4,30,6930,1,140,36,60,20,16380,4,243374040,12,105,504,12,6,6126120,4680,168,3,314954640,10,209969760,24,4,180180,1790848659600,6,924,6,660,1260,8303710615200",
			"name": "a(n) is the least common multiple of the nonzero digits in factorial base expansion of 1/n.",
			"comment": [
				"See the Wikipedia link for the construction method of 1/n in factorial base."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A343505/b343505.txt\"\u003eTable of n, a(n) for n = 1..5000\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A343505/a343505.png\"\u003eColored logarithmic scatterplot of the sequence for n = 1..25000\u003c/a\u003e (where the color is function of A052126(n))",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Factorial_number_system#Fractional_values\"\u003eFactorial number system (Fractional values)\u003c/a\u003e",
				"\u003ca href=\"/index/Fa#facbase\"\u003eIndex entries for sequences related to factorial base representation\u003c/a\u003e"
			],
			"example": [
				"The first terms, alongside 1/n in factorial base, are:",
				"  n   a(n)   1/n in factorial base",
				"  --  -----  -----------------------------------------",
				"   1      1  1",
				"   2      1  0.1",
				"   3      2  0.0 2",
				"   4      2  0.0 1 2",
				"   5      4  0.0 1 0 4",
				"   6      1  0.0 1",
				"   7      6  0.0 0 3 2 0 6",
				"   8      3  0.0 0 3",
				"   9      6  0.0 0 2 3 2",
				"  10      2  0.0 0 2 2",
				"  11     60  0.0 0 2 0 5 3 1 4 0 10",
				"  12      2  0.0 0 2",
				"  13    120  0.0 0 1 4 1 2 5 4 8 5 0 12",
				"  14      3  0.0 0 1 3 3 3",
				"  15      3  0.0 0 1 3",
				"  16      6  0.0 0 1 2 3",
				"  17   1008  0.0 0 1 2 0 2 3 6 8 9 0 9 2 7 0 16",
				"  18      4  0.0 0 1 1 4",
				"  19  51480  0.0 0 1 1 1 6 2 0 9 5 2 6 11 11 13 8 0 18",
				"  20      1  0.0 0 1 1"
			],
			"program": [
				"(PARI) a(n) = my (v=1, f=1/n); for (r=2, oo, if (f==0, return (v), floor(f), v=lcm(v, floor(f))); f=frac(f)*r)"
			],
			"xref": [
				"Cf. A052126, A294168, A343504."
			],
			"keyword": "nonn,look,base",
			"offset": "1,3",
			"author": "_Rémy Sigrist_, Apr 17 2021",
			"references": 2,
			"revision": 10,
			"time": "2021-04-18T10:21:16-04:00",
			"created": "2021-04-18T09:04:16-04:00"
		}
	]
}