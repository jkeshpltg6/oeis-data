{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A340598",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 340598,
			"data": "0,1,0,3,3,10,60,210,700,3556,19845,105567,550935,3120832,19432413,127949250,858963105,5882733142,41636699676,307105857344,2357523511200,18694832699907,152228641035471,1270386473853510,10872532998387918,95531590347525151",
			"name": "Number of balanced set partitions of {1..n}.",
			"comment": [
				"A set partition is balanced if it has exactly as many blocks as the greatest size of a block."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A340598/b340598.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e"
			],
			"example": [
				"The a(1) = 1 through a(5) = 10 balanced set partitions (empty column indicated by dot):",
				"  {{1}}  .  {{1},{2,3}}  {{1,2},{3,4}}  {{1},{2},{3,4,5}}",
				"            {{1,2},{3}}  {{1,3},{2,4}}  {{1},{2,3,4},{5}}",
				"            {{1,3},{2}}  {{1,4},{2,3}}  {{1,2,3},{4},{5}}",
				"                                        {{1},{2,3,5},{4}}",
				"                                        {{1,2,4},{3},{5}}",
				"                                        {{1},{2,4,5},{3}}",
				"                                        {{1,2,5},{3},{4}}",
				"                                        {{1,3,4},{2},{5}}",
				"                                        {{1,3,5},{2},{4}}",
				"                                        {{1,4,5},{2},{3}}"
			],
			"mathematica": [
				"sps[{}]:={{}};sps[set:{i_,___}]:=Join@@Function[s,Prepend[#,s]\u0026/@sps[Complement[set,s]]]/@Cases[Subsets[set],{i,___}];",
				"Table[Length[Select[sps[Range[n]],Length[#]==Max@@Length/@#\u0026]],{n,0,8}]"
			],
			"program": [
				"(PARI) \\\\ D(n,k) counts balanced set partitions with k blocks.",
				"D(n,k)={my(t=sum(i=1, k, x^i/i!) + O(x*x^n)); n!*polcoef(t^k - (t-x^k/k!)^k, n)/k!}",
				"a(n)={sum(k=sqrtint(n), (n+1)\\2, D(n,k))} \\\\ _Andrew Howroyd_, Mar 14 2021"
			],
			"xref": [
				"The unlabeled version is A047993 (A106529).",
				"A000110 counts set partitions.",
				"A000670 counts ordered set partitions.",
				"A113547 counts set partitions by maximin.",
				"Other balance-related sequences:",
				"- A010054 counts balanced strict integer partitions (A002110).",
				"- A098124 counts balanced integer compositions.",
				"- A340596 counts co-balanced factorizations.",
				"- A340599 counts alt-balanced factorizations.",
				"- A340600 counts unlabeled balanced multiset partitions.",
				"- A340653 counts balanced factorizations.",
				"Cf. A000258, A001055, A006141, A008275, A008277, A008278, A095149."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_Gus Wiseman_, Jan 20 2021",
			"ext": [
				"Terms a(12) and beyond from _Andrew Howroyd_, Mar 14 2021"
			],
			"references": 13,
			"revision": 12,
			"time": "2021-03-14T12:36:53-04:00",
			"created": "2021-01-22T20:27:52-05:00"
		}
	]
}