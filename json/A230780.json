{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A230780",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 230780,
			"data": "1,2,3,4,5,6,8,9,10,11,12,15,16,17,18,20,22,23,24,25,27,29,30,32,33,34,36,40,41,44,45,46,47,48,50,51,53,54,55,58,59,60,64,66,68,69,71,72,75,80,81,82,83,85,87,88,89,90,92,94,96,99,100,101,102,106,107",
			"name": "Positive numbers without a prime factor congruent to 1 (mod 6).",
			"comment": [
				"The sequence is closed under multiplication. Primitive elements are 3 and the primes of form 3*k+2.",
				"a(n)^2 is not expressible as x^2+xy+y^2 with x and y positive integers.",
				"Analog of A004144 (nonhypotenuse numbers) for 120-degree angle triangles: a(n) is not the length of the longest side of such a triangle with integer sides.",
				"It might have been natural to include 0 in this sequence. - _M. F. Hasler_, Mar 04 2018"
			],
			"link": [
				"Ray Chandler, \u003ca href=\"/A230780/b230780.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (first 254 terms from Jean-Christophe Hervé)",
				"F. Javier de Vega, \u003ca href=\"https://arxiv.org/abs/2003.13378\"\u003eAn extension of Furstenberg's theorem of the infinitude of primes\u003c/a\u003e, arXiv:2003.13378 [math.NT], 2020.",
				"August Lösch, \u003ca href=\"http://archive.org/stream/economicsoflocat00ls#page/116/mode/2up\"\u003eEconomics of Location\u003c/a\u003e (1954), see pp. 117f.",
				"U. P. Nair, \u003ca href=\"https://arxiv.org/abs/math/0408107\"\u003eElementary results on the binary quadratic form a^2+ab+b^2\u003c/a\u003e, arXiv:math/0408107 [math.NT], 2004.",
				"\u003ca href=\"/index/Aa#A2\"\u003eIndex entries for sequences related to A2 = hexagonal = triangular lattice\u003c/a\u003e"
			],
			"formula": [
				"A005088(a(n)) = 0."
			],
			"mathematica": [
				"Join[{1}, Select[Range[2, 110], ! MemberQ[Union[Mod[Transpose[ FactorInteger[#]][[1]], 6]], 1] \u0026]] (* _T. D. Noe_, Nov 24 2013 *)",
				"Join[{1},Select[Range[110],NoneTrue[FactorInteger[#][[All,1]],Mod[#,6] == 1\u0026]\u0026]] (* Requires Mathematica version 10 or later *) (* _Harvey P. Dale_, Feb 03 2019 *)"
			],
			"program": [
				"(Haskell)",
				"a230780 n = a230780_list !! (n-1)",
				"a230780_list = filter (all (/= 1) . map (flip mod 6) . a027748_row) [1..]",
				"-- _Reinhard Zumkeller_, Apr 09 2014",
				"(PARI) is_A230780(n)=!setsearch(Set(factor(n)[,1]%6),1) \\\\ _M. F. Hasler_, Mar 04 2018"
			],
			"xref": [
				"Cf. A002476, A005088, complement of A050931.",
				"Cf. A004144 (analog for 4k+1 primes and right triangles).",
				"Cf. A027748."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Jean-Christophe Hervé_, Nov 23 2013",
			"references": 4,
			"revision": 47,
			"time": "2020-07-11T02:31:44-04:00",
			"created": "2013-11-24T21:25:29-05:00"
		}
	]
}