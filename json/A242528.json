{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A242528",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 242528,
			"data": "0,0,0,0,0,0,0,0,0,0,0,2,4,18,13,62,8,133,225,209,32,2644,4462,61341,113986,750294,176301,7575912,3575686,7705362,36777080,108638048,97295807",
			"name": "Number of cyclic arrangements of {0,1,...,n-1} such that both the difference and the sum of any two neighbors are prime.",
			"comment": [
				"a(n)=NPC(n;S;P) is the count of all neighbor-property cycles for a specific set S of n elements and a specific pair-property P. For more details, see the link and A242519.",
				"In this case the set is S={0 through n-1}. For the same pair-property P but the set S={1 through n}, see A227050."
			],
			"link": [
				"S. Sykora, \u003ca href=\"http://dx.doi.org/10.3247/SL5Math14.002\"\u003eOn Neighbor-Property Cycles\u003c/a\u003e, \u003ca href=\"http://ebyte.it/library/Library.html#math\"\u003eStan's Library\u003c/a\u003e, Volume V, 2014."
			],
			"example": [
				"For n=12 (the first n for which a(n)\u003e0) there are two such cycles:",
				"C_1={0, 5, 2, 9, 4, 1, 6, 11, 8, 3, 10, 7},",
				"C_2={0, 7, 10, 3, 8, 5, 2, 9, 4, 1, 6, 11}."
			],
			"mathematica": [
				"A242528[n_] :=",
				"Count[Map[lpf, Map[j0f, Permutations[Range[n - 1]]]], 0]/2;",
				"j0f[x_] := Join[{0}, x, {0}];",
				"lpf[x_] := Length[",
				"   Join[Select[asf[x], ! PrimeQ[#] \u0026],",
				"    Select[Differences[x], ! PrimeQ[#] \u0026]]];",
				"asf[x_] := Module[{i}, Table[x[[i]] + x[[i + 1]], {i, Length[x] - 1}]];",
				"Table[A242528[n], {n, 1, 8}]",
				"(* OR, a less simple, but more efficient implementation. *)",
				"A242528[n_, perm_, remain_] := Module[{opt, lr, i, new},",
				"   If[remain == {},",
				"     If[PrimeQ[First[perm] - Last[perm]] \u0026\u0026",
				"       PrimeQ[First[perm] + Last[perm]], ct++];",
				"     Return[ct],",
				"     opt = remain; lr = Length[remain];",
				"     For[i = 1, i \u003c= lr, i++,",
				"      new = First[opt]; opt = Rest[opt];",
				"      If[! (PrimeQ[Last[perm] - new] \u0026\u0026 PrimeQ[Last[perm] + new]),",
				"       Continue[]];",
				"      A242528[n, Join[perm, {new}],",
				"       Complement[Range[n - 1], perm, {new}]];",
				"      ];",
				"     Return[ct];",
				"     ];",
				"   ];",
				"Table[ct = 0; A242528[n, {0}, Range[n - 1]]/2, {n, 1, 18}]",
				"(* _Robert Price_, Oct 22 2018 *)"
			],
			"program": [
				"(C++) See the link."
			],
			"xref": [
				"Cf. A227050, A242519, A242520, A242521, A242522, A242523, A242524, A242525, A242526, A242527, A242529, A242530, A242531, A242532, A242533, A242534."
			],
			"keyword": "nonn,hard,more",
			"offset": "1,12",
			"author": "_Stanislav Sykora_, May 30 2014",
			"ext": [
				"a(29)-a(33) from _Fausto A. C. Cariboni_, May 20 2017"
			],
			"references": 20,
			"revision": 16,
			"time": "2018-10-23T03:03:02-04:00",
			"created": "2014-05-30T12:45:56-04:00"
		}
	]
}