{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A340910",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 340910,
			"data": "1,1,1,1,1,1,1,2,2,1,1,4,6,4,1,1,8,17,17,8,1,1,16,46,63,46,16,1,1,32,121,216,216,121,32,1,1,64,312,703,907,703,312,64,1,1,128,793,2205,3538,3538,2205,793,128,1,1,256,1995,6731,13096,16208,13096,6731,1995,256,1,1,512,4984,20139,46640,69476,69476,46640,20139,4984,512,1",
			"name": "Triangle of coefficients in g.f. A(x,y) which satisfies: A(x,y) = Sum_{n\u003e=0} x^n/(1 - x*y*A(x,y)^n).",
			"comment": [
				"The g.f. A(x,y) of this sequence is motivated by the following identity:",
				"Sum_{n\u003e=0} p^n/(1 - q*r^n) = Sum_{n\u003e=0} q^n/(1 - p*r^n) = Sum_{n\u003e=0} p^n*q^n*r^(n^2)*(1 - p*q*r^(2*n))/((1 - p*r^n)*(1-q*r^n)) ;",
				"here, p = x, q = x*y, and r = A(x,y)."
			],
			"formula": [
				"G.f. A(x,y) satisfies:",
				"(1) A(x,y) = Sum_{n\u003e=0} x^n/(1 - x*y*A(x,y)^n).",
				"(2) A(x,y) = Sum_{n\u003e=0} x^n*y^n/(1 - x*A(x,y)^n).",
				"(3) A(x,y) = Sum_{n\u003e=0} x^(2*n) * y^n * A(x)^(n^2) * (1 - x^2*y*A(x)^(2*n)) / ((1 - x*A(x,y)^n)*(1 - x*y*A(x,y)^n)). - _Paul D. Hanna_, Feb 20 2021",
				"(4) A(x*y, 1/y) = A(x, y)."
			],
			"example": [
				"G.f.: A(x,y) = 1 + (1 + y)*x^1 + (1 + y + y^2)*x^2 + (1 + 2*y + 2*y^2 + y^3)*x^3 + (1 + 4*y + 6*y^2 + 4*y^3 + y^4)*x^4 + (1 + 8*y + 17*y^2 + 17*y^3 + 8*y^4 + y^5)*x^5 + (1 + 16*y + 46*y^2 + 63*y^3 + 46*y^4 + 16*y^5 + y^6)*x^6 + ...",
				"where A(x,y) satisfies:",
				"A(x,y) = Sum_{n\u003e=0} x^n/(1 - x*y*A(x,y)^n),",
				"also",
				"A(x,y) = Sum_{n\u003e=0} x^n*y^n/(1 - x*A(x,y)^n).",
				"TRIANGLE.",
				"This triangle of coefficients T(n,k) of x^n*y^k in A(x,y) begins",
				"1;",
				"1, 1;",
				"1, 1, 1;",
				"1, 2, 2, 1;",
				"1, 4, 6, 4, 1;",
				"1, 8, 17, 17, 8, 1;",
				"1, 16, 46, 63, 46, 16, 1;",
				"1, 32, 121, 216, 216, 121, 32, 1;",
				"1, 64, 312, 703, 907, 703, 312, 64, 1;",
				"1, 128, 793, 2205, 3538, 3538, 2205, 793, 128, 1;",
				"1, 256, 1995, 6731, 13096, 16208, 13096, 6731, 1995, 256, 1;",
				"1, 512, 4984, 20139, 46640, 69476, 69476, 46640, 20139, 4984, 512, 1;",
				"1, 1024, 12397, 59375, 161375, 283599, 340458, 283599, 161375, 59375, 12397, 1024, 1; ..."
			],
			"program": [
				"(PARI) {T(n,k) = my(A=1); for(i=1,n, A = sum(m=0,n, x^m/(1 - x*y*A^m +x*O(x^n))) ); polcoeff(polcoeff(H=A,n,x),k,y)}",
				"for(n=0,12,for(k=0,n,print1(T(n,k),\", \"));print(\"\"))",
				"(PARI) {T(n,k) = my(A=1); for(i=1,n, A = sum(m=0,n, x^m*y^m/(1 - x*A^m +x*O(x^n))) ); polcoeff(polcoeff(H=A,n,x),k,y)}",
				"for(n=0,12,for(k=0,n,print1(T(n,k),\", \"));print(\"\"))"
			],
			"xref": [
				"Cf. A340911 (row sums), A340912 (central terms), A340333 (antidiagonal sums)."
			],
			"keyword": "nonn,tabl",
			"offset": "0,8",
			"author": "_Paul D. Hanna_, Jan 26 2021",
			"references": 3,
			"revision": 11,
			"time": "2021-02-20T14:48:44-05:00",
			"created": "2021-01-27T10:49:59-05:00"
		}
	]
}