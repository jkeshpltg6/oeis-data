{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A279197",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 279197,
			"data": "1,1,2,2,11,11,55,58,486,442,4218,3924,45096,42013,538537,505830,7368091",
			"name": "Number of self-conjugate inseparable solutions of X + Y = 2Z (integer, disjoint triples from {1,2,3,...,3n}).",
			"comment": [
				"In Richard Guy's letter, the term 50 is marked with a question mark. _Peter Kagey_ has shown that the value should be 55. - _N. J. A. Sloane_, Feb 15 2017",
				"From _Peter Kagey_, Feb 14 2017: (Start)",
				"An inseparable solution is one in which \"there is no j such that the first j of the triples are a partition of 1, ..., 3j\" (See A202705.)",
				"A self-conjugate solution is one in which for every triple (a, b, c) in the partition there exists a \"conjugate\" triple (m-a, m-b, m-c) or (m-b, m-a, m-c) where m = 3n+1.",
				"(End)"
			],
			"reference": [
				"R. K. Guy, Sedlacek's Conjecture on Disjoint Solutions of x+y= z, Univ. Calgary, Dept. Mathematics, Research Paper No. 129, 1971.",
				"R. K. Guy, Sedlacek's Conjecture on Disjoint Solutions of x+y= z, in Proc. Conf. Number Theory. Pullman, WA, 1971, pp. 221-223.",
				"R. K. Guy, Packing [1,n] with solutions of ax + by = cz; the unity of combinatorics, in Colloq. Internaz. Teorie Combinatorie. Rome, 1973, Atti Conv. Lincei. Vol. 17, Part II, pp. 173-179, 1976."
			],
			"link": [
				"R. K. Guy, Letter to N. J. A. Sloane, June 24 1971: \u003ca href=\"/A002572/a002572.jpg\"\u003efront\u003c/a\u003e, \u003ca href=\"/A002572/a002572_1.jpg\"\u003eback\u003c/a\u003e [Annotated scanned copy, with permission] See sequence \"I\".",
				"Peter Kagey, \u003ca href=\"/A279197/a279197.hs.txt\"\u003eHaskell program for A279197\u003c/a\u003e.",
				"Peter Kagey, \u003ca href=\"/A279197/a279197.txt\"\u003eSolutions for a(1)-a(10)\u003c/a\u003e.",
				"R. J. Nowakowski, \u003ca href=\"/A104429/a104429.pdf\"\u003eGeneralizations of the Langford-Skolem problem\u003c/a\u003e, M.S. Thesis, Dept. Math., Univ. Calgary, May 1975. [Scanned copy, with permission.]"
			],
			"example": [
				"Examples of solutions X,Y,Z for n=5:",
				"2,4,3",
				"5,7,6",
				"1,15,8",
				"9,11,10",
				"12,14,13",
				"and in his letter Richard Guy has drawn links pairing the first and fifth solutions, and the second and fourth solutions.",
				"For n = 2 the a(2) = 1 solution is",
				"[(2,6,4),(1,5,3)].",
				"For n = 3 the a(3) = 2 solutions are",
				"[(1,7,4),(3,9,6),(2,8,5)] and",
				"[(2,4,3),(6,8,7),(1,9,5)]."
			],
			"xref": [
				"All of A279197, A279198, A202705, A279199, A104429, A282615 are concerned with counting solutions to X+Y=2Z in various ways.",
				"See also A002848, A002849."
			],
			"keyword": "nonn,more",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_, Dec 15 2016",
			"ext": [
				"a(7) corrected and a(8)-a(13) added by _Peter Kagey_, Feb 14 2017",
				"a(14)-a(16) from _Fausto A. C. Cariboni_, Feb 27 2017",
				"a(17) from _Fausto A. C. Cariboni_, Mar 22 2017"
			],
			"references": 10,
			"revision": 43,
			"time": "2017-04-10T13:09:27-04:00",
			"created": "2016-12-15T17:46:48-05:00"
		}
	]
}