{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A338303",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 338303,
			"data": "6,4,4,5,2,1,7,8,3,0,6,7,2,7,4,4,4,2,0,9,9,2,7,3,1,1,9,0,3,8,0,1,6,9,0,2,9,2,8,9,0,8,1,2,3,8,7,7,9,9,1,8,5,7,6,5,1,4,2,5,5,2,7,5,7,7,6,8,6,8,6,1,6,8,3,6,7,8,7,4,3,3,4,1,4,0,8",
			"name": "Decimal expansion of Sum_{k\u003e=0} 1/(L(2*k) + 2), where L(k) is the k-th Lucas number (A000032).",
			"comment": [
				"Backstrom (1981) found that the sum is approximately equal to 1/8 + 1/(4*log(phi)), where phi is the golden ratio (A001622). The difference is less than 1/10^7. He called this difference \"a tantalizing problem\".",
				"Almkvist (1986) added a term to Backstrom's formula to get an even better approximation which differs from the exact value by less than 1/10^33: 1/8 + 1/(4*log(phi)) + Pi^2/(log(phi)^2 * (exp(Pi^2/log(phi)) - 2)). He found an exact formula from a quotient of two Jacobi theta functions (see the FORMULA section), and showed that both approximations are just the first terms in a rapidly converging series.",
				"Since exp(-Pi^2/log(phi)) = 1.23...*10^(-9) is small, the convergence is rapid: the number of terms needed in each of the two series in the formula to get 10^2, 10^3 and 10^4 decimal digits are merely 3, 10 and 32, respectively.",
				"Andre-Jeannin (1991) noted that if the summand 2 that is added to the Lucas numbers is replaced with sqrt(5), then the sum is Sum_{k\u003e=0} 1/(L(2*k) + sqrt(5)) = 1/phi (A094214)."
			],
			"reference": [
				"Jonathan M. Borwein and Peter B. Borwein, Pi and the AGM, Wiley, 1987, p. 99."
			],
			"link": [
				"Gert Almkvist, \u003ca href=\"https://www.fq.math.ca/Scanned/24-4/almkvist.pdf\"\u003eA solution to a tantalizing problem\u003c/a\u003e, The Fibonacci Quarterly, Vol. 24, No. 4 (1986), pp. 316-322.",
				"Richard Andre-Jeannin, \u003ca href=\"https://www.fq.math.ca/Scanned/29-3/andre-jeannin1.pdf\"\u003eSummation of certain reciprocal series related to Fibonacci and Lucas numbers\u003c/a\u003e, The Fibonacci Quarterly, Vol. 29, No. 3 (1991), pp. 200-204.",
				"Robert P. Backstrom, \u003ca href=\"https://www.fq.math.ca/Scanned/19-1/backstrom.pdf\"\u003eOn reciprocal series related to Fibonacci numbers with subscripts in arithmetic progression\u003c/a\u003e, The Fibonacci Quarterly, Vol. 19, No. 1 (1981), pp. 14-21. See section 6, pp. 19-20.",
				"Daniel Duverney and Iekata Shiokawa, \u003ca href=\"https://doi.org/10.1063/1.2841912\"\u003eOn series involving Fibonacci and Lucas numbers I\u003c/a\u003e, AIP Conference Proceedings, Vol. 976, No. 1 (2008), pp. 62-76, \u003ca href=\"https://danielduverney.fr/documents/theorie-des-nombres/FibonacciSeries.pdf\"\u003ealternative link\u003c/a\u003e."
			],
			"formula": [
				"Equals Sum_{k\u003e=0} 1/A240926(k).",
				"Equals 1/4 + Sum_{k\u003e=1} q^(2*k)/(1 + q^(2*k))^2, where q = 1/phi.",
				"Equals 1/8 + (1/(4*log(phi))) * (1 - (4*Pi^2/log(phi)) * (S(2)/(1 + 2*S(0)))), where S(m) = Sum_{k\u003e=1} (-1)^k * k^m * exp(-Pi^2*k^2/log(phi)) (Almkvist, 1986)."
			],
			"example": [
				"0.64452178306727444209927311903801690292890812387799..."
			],
			"mathematica": [
				"With[{lg = Log[GoldenRatio], kmax = 3}, sum[m_, k_] := (-1)^k*k^m*Exp[-Pi^2*k^2/lg]; RealDigits[1/8 + ( 1/(4*lg))*(1 - (4*Pi^2/lg)*(Sum[sum[2, k], {k, 1, kmax}]/(1 + 2*Sum[sum[0, k], {k, 1, kmax}]))), 10, 100][[1]]]"
			],
			"xref": [
				"Cf. A000032, A001622, A002390, A094214, A160509, A240926."
			],
			"keyword": "nonn,cons",
			"offset": "0,1",
			"author": "_Amiram Eldar_, Oct 21 2020",
			"references": 1,
			"revision": 5,
			"time": "2020-10-21T20:27:01-04:00",
			"created": "2020-10-21T20:27:01-04:00"
		}
	]
}