{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A330501",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 330501,
			"data": "3,2,7,0,13,23,19,31,71,109,31,47,37,71,43,163,67,71,271,79,61,307,67,331,73,103,79,271,211,151,541,127,97,883,103,181,109,151,281,379,163,167,127,571,179,433,139,191,631,199,151,487,157,271,163,223,227,541,421,239,181,311,251,811",
			"name": "Smallest prime of the form m(m+1)/2 - n(n-3)/2 with m \u003e= n, or 0 if no such prime exists.",
			"comment": [
				"A mathematical game for kids suggested by Peter Luschny on the SeqFan list:",
				"Start with n and then (cumulatively) add m = n, n+1, n+2, ... until a prime p = T(m) - n(n-3)/2 is reached. The only n which never yields a prime is n = 3."
			],
			"link": [
				"M. F. Hasler, \u003ca href=\"/A330501/b330501.txt\"\u003eTable of n, a(n) for n = 0..9999\u003c/a\u003e",
				"Peter Luschny, \u003ca href=\"http://list.seqfan.eu/pipermail/seqfan/2019-December/\"\u003eHopping for primes\u003c/a\u003e, SeqFan list, Dec 13 2019."
			],
			"example": [
				"Starting with n = 0, we add 0: sum = 0, not prime, then 1: sum = 1, not prime, then 2: sum = 3, a prime, so a(0) = 3.",
				"Starting with n = 1, we add 1: sum = 2, a prime, so a(1) = 2.",
				"Starting with n = 2, we add 2: sum = 4, not prime, then 3: sum = 7, a prime, so a(2) = 7.",
				"Starting with n = 3 = T(2) = 2(2+1)/2 (triangular number, cf. A000217), we add 3 to get T(2) + 3 = T(3) = 6, then 4 to get T(3) + 4 = T(4) = 10, and so on. A triangular number T(n) = n(n+1)/2 \u003e 3 is never prime, since either product of n and (n+1)/2, or product of n/2 and n+1. So a(3) = 0."
			],
			"mathematica": [
				"Array[If[# == 3, 0, Block[{m = #, p}, While[! PrimeQ[Set[p, m (m + 1)/2 - # (# - 3)/2]], m++]; p]] \u0026, 64, 0] (* _Michael De Vlieger_, Dec 16 2019 *)"
			],
			"program": [
				"(PARI) apply( {A330501(n,p=n)=if(n!=3,while(!isprime(p+=n),n++);p)}, [0..399])"
			],
			"xref": [
				"Cf. A000217 (triangular numbers n(n+1)/2), A000096 (n(n+3)/2).",
				"Cf. A329946 (primes not in this sequence), A330502 (corresponding m)."
			],
			"keyword": "nonn",
			"offset": "0,1",
			"author": "_M. F. Hasler_, following an idea of _Peter Luschny_, Dec 16 2019",
			"references": 4,
			"revision": 15,
			"time": "2019-12-16T21:25:26-05:00",
			"created": "2019-12-16T20:03:17-05:00"
		}
	]
}