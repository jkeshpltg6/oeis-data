{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007847",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7847,
			"data": "2,6,20,140,3254,252434,71343208,86246755608",
			"name": "Number of hyperplanes spanned by the vertices of an n-cube.",
			"comment": [
				"This is the count of (n-1)-dimensional hyperplanes spanned by any n vertices of a unit cube in dimension n. - _N. J. A. Sloane_, Apr 14 2020"
			],
			"reference": [
				"O. Aichholzer, F. Aurenhammer, Classifying Hyperplanes in Hypercubes, 10th European Workshop on Computational Geometry, Santander, Spain, March 1994."
			],
			"link": [
				"O. Aichholzer and F. Aurenhammer, \u003ca href=\"http://dx.doi.org/10.1137/S089548019426348X\"\u003eClassifying hyperplanes in hypercubes\u003c/a\u003e, SIAM J. Discrete Math, Vol. 9, 1996, 225 - 232."
			],
			"example": [
				"For n=2 there are the four edges of the square and the two diagonals, for a total of 6. - _N. J. A. Sloane_, Apr 14 2020",
				"Comment from _Tom Karzes_, Apr 14 2020 (Start):",
				"The classes of hyperplanes are listed below for d = 2-5.  Each class is shown below preceded by the number of instances of that class.",
				"I define two hyperplanes as being in the same class if the vertex set of one can be transformed to the vertex set of the other by some combination of (1) permuting the coordinates and (2) inverting some set of coordinates (1-\u003e0 and 0-\u003e1).",
				"For d=2 there are 2 classes hyperplanes (i.e., lines):",
				"      4:  00 01",
				"      2:  00 11",
				"This gives a total of 6.  The first class corresponds to the 4 perimeter slices.",
				"For d=3 there are 3 classes of hyperplanes (i.e., planes):",
				"      6:  000 001 010 011",
				"      6:  000 001 110 111",
				"      8:  000 011 101",
				"This gives a total of 20.  The first class corresponds to the 6 perimeter slices.",
				"For d=4 there are 6 classes of hyperplanes:",
				"      8:  0000 0001 0010 0011 0100 0101 0110 0111",
				"     12:  0000 0001 0010 0011 1100 1101 1110 1111",
				"     32:  0000 0001 0110 0111 1010 1011",
				"     16:  0000 0011 0101 1001",
				"      8:  0000 0011 0101 1010 1100 1111",
				"     64:  0000 0011 0101 1110",
				"This gives a total of 140.  The first class corresponds to the 8 perimeter slices.",
				"For d=5 there are 15 classes of hyperplanes:",
				"     10:  00000 00001 00010 00011 00100 00101 00110 00111 01000 01001 01010 01011 01100 01101 01110 01111",
				"     20:  00000 00001 00010 00011 00100 00101 00110 00111 11000 11001 11010 11011 11100 11101 11110 11111",
				"     80:  00000 00001 00010 00011 01100 01101 01110 01111 10100 10101 10110 10111",
				"     80:  00000 00001 00110 00111 01010 01011 10010 10011",
				"     40:  00000 00001 00110 00111 01010 01011 10100 10101 11000 11001 11110 11111",
				"    320:  00000 00001 00110 00111 01010 01011 11100 11101",
				"     32:  00000 00011 00101 01001 10001",
				"     32:  00000 00011 00101 01001 10010 10100 10111 11000 11011 11101",
				"     80:  00000 00011 00101 01001 10110 11010 11100 11111",
				"    160:  00000 00011 00101 01001 11110",
				"    160:  00000 00011 00101 01010 01100 01111 10110",
				"    320:  00000 00011 00101 01110 10110",
				"    320:  00000 00011 00101 01110 11000 11011 11101",
				"    640:  00000 00011 00101 01110 11001",
				"    960:  00000 00011 01101 10101 11010",
				"This gives a total of 3254.  The first class corresponds to the 10 perimeter slices.",
				"(End)"
			],
			"xref": [
				"See A333539 for the number of pieces formed when the cube is cut along these hyperplanes."
			],
			"keyword": "nonn,hard,more",
			"offset": "1,1",
			"author": "Oswin Aichholzer (oaich(AT)igi.tu-graz.ac.at)",
			"ext": [
				"Edited by _M. F. Hasler_, Apr 05 2015"
			],
			"references": 4,
			"revision": 14,
			"time": "2020-04-14T14:14:36-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}