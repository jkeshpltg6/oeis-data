{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A262045",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 262045,
			"data": "1,1,1,1,1,0,0,1,1,1,1,1,1,0,0,1,1,1,2,2,1,1,1,0,0,0,0,1,1,1,1,1,1,1,1,0,1,1,0,1,1,1,1,0,0,1,1,1,1,0,0,0,0,0,0,1,1,1,2,2,2,2,1,1,1,0,0,0,0,0,0,1,1,1,1,0,0,1,1,1,1,0,1,1,2,2,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1",
			"name": "Irregular triangle read by rows in which row n lists the elements of row n of A249223 and then the elements of the same row in reverse order.",
			"comment": [
				"The n-th row of the triangle has length 2*A003056(n).",
				"This sequence extends A249223 in the same manner as A237593 extends A237591.",
				"The entries in the n-th row of the triangle are the widths of the regions between the (n-1)-st and n-th Dyck paths for the symmetric representation of sigma(n) with each column representing the corresponding leg of the n-th path."
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/A262045/b262045.txt\"\u003eTable of n, a(n) for n = 1..3200\u003c/a\u003e [First 150 rows, based on G. C. Greubel's b-file for A249223}"
			],
			"formula": [
				"T(n, k) = T(n, 2*A003056(n) + 1 - k) = A249223(n, k), for 1 \u003c= n and 1 \u003c= k \u003c= A003056(n)."
			],
			"example": [
				"n\\k 1   2   3   4   5   6   7   8   9   10",
				"1   1   1",
				"2   1   1",
				"3   1   0   0   1",
				"4   1   1   1   1",
				"5   1   0   0   1",
				"6   1   1   2   2   1   1",
				"7   1   0   0   0   0   1",
				"8   1   1   1   1   1   1",
				"9   1   0   1   1   0   1",
				"10  1   1   1   0   0   1   1   1",
				"11  1   0   0   0   0   0   0   1",
				"12  1   1   2   2   2   2   1   1",
				"13  1   0   0   0   0   0   0   1",
				"14  1   1   1   0   0   1   1   1",
				"15  1   0   1   1   2   2   1   1   0   1",
				"16  1   1   1   1   1   1   1   1   1   1",
				"17  1   0   0   0   0   0   0   0   0   1",
				"18  1   1   2   1   1   1   1   2   1   1",
				"19  1   0   0   0   0   0   0   0   0   1",
				"20  1   1   1   1   2   2   1   1   1   1",
				"...",
				"The triangle shows that the region between a Dyck path for n and n-1 has width 1 if n is a power of 2. For n a prime the region is a horizontal rectangle of width (height) 1 and the vertical rectangle of width 1 which is its reflection. The Dyck paths and regions are shown below for n = 1..5 (see the A237593 example for n = 1..28):",
				"   _ _ _",
				"5 |_ _ _|",
				"4 |_ _  |_ _",
				"3 |_ _|_  | |",
				"2 |_  | | | |",
				"1 |_|_|_|_|_|"
			],
			"mathematica": [
				"(* functions a237048[ ] and row[ ] are defined in A237048 *)",
				"f[n_] :=Drop[FoldList[Plus, 0, Map[(-1)^(#+1)\u0026, Range[row[n]]] a237048[n]], 1]",
				"a262045[n_]:=Join[f[n], Reverse[f[n]]]",
				"Flatten[Map[a262045, Range[16]]](* data *)"
			],
			"xref": [
				"Cf. A000203, A003056, A196020, A236104, A237048, A237270, A237271, A237591, A237593, A249223, A262048."
			],
			"keyword": "nonn,tabf",
			"offset": "1,19",
			"author": "_Hartmut F. W. Hoft_, Sep 09 2015",
			"references": 24,
			"revision": 32,
			"time": "2021-02-28T04:29:44-05:00",
			"created": "2015-10-14T17:23:47-04:00"
		}
	]
}