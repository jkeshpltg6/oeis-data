{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A222362",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 222362,
			"data": "5,3,2,8,3,9,9,7,5,3,5,3,5,5,2,0,2,3,5,6,9,0,7,9,3,9,9,2,2,9,9,0,5,7,6,9,5,4,1,5,1,1,5,4,7,1,1,5,3,1,2,6,6,2,4,2,3,3,8,4,1,2,9,3,3,7,3,5,5,2,9,4,2,4,0,0,8,0,9,5,1,0,1,6,6,8,0,6,4,2,4,1,7,3,8,5,5,2,9,8,7,8,2,7,4,0,3,0,0,3",
			"name": "Decimal expansion of the ratio of the area of the latus rectum segment of any equilateral hyperbola to the square of its semi-axis: sqrt(2) - log(1 + sqrt(2)).",
			"comment": [
				"Just as circles are ellipses whose semi-axes are equal (and are called the radius of the circle), equilateral (or rectangular) hyperbolas are hyperbolas whose semi-axes are equal.",
				"Just as the ratio of the area of a circle to the square of its radius is always Pi, the ratio of the area of the latus rectum segment of any equilateral hyperbola to the square of its semi-axis is the universal equilateral hyperbolic constant sqrt(2) - log(1 + sqrt(2)).",
				"Note the remarkable similarity to sqrt(2) + log(1 + sqrt(2)), the universal parabolic constant A103710, which is a ratio of arc lengths rather than of areas.  Lockhart (2012) says \"the arc length integral for the parabola ... is intimately connected to the hyperbolic area integral ... I think it is surprising and wonderful that the length of one conic section is related to the area of another\".",
				"This constant is also the abscissa of the vertical asymptote of the involute of the logarithmic curve (starting point (1,0)). - _Jean-François Alcover_, Nov 25 2016"
			],
			"reference": [
				"H. Dörrie, 100 Great Problems of Elementary Mathematics, Dover, 1965, Problems 57 and 58.",
				"P. Lockhart, Measurement, Harvard University Press, 2012, p. 369."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A222362/b222362.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"J.-F. Alcover, \u003ca href=\"/A222362/a222362.pdf\"\u003eAsymptote\u003c/a\u003e of the logarithmic curve involute.",
				"I.N. Bronshtein, \u003ca href=\"http://books.google.com/books?id=gCgOoMpluh8C\u0026amp;lpg=PA202\u0026amp;vq=%22areas%20in%20the%20hyperbola%22\u0026amp;pg=PA202#v=onepage\u0026amp;q\u0026amp;f=false\"\u003eHandbook of Mathematics\u003c/a\u003e, 5th ed., Springer, 2007, p. 202, eq. (3.338a).",
				"S. R. Finch, \u003ca href=\"http://arxiv.org/abs/2001.00578\"\u003eMathematical Constants, Errata and Addenda\u003c/a\u003e, 2012, section 8.1.",
				"J. Pahikkala, \u003ca href=\"http://planetmath.org/arclengthofparabola\"\u003eArc Length Of Parabola\u003c/a\u003e, PlanetMath.",
				"S. Reese, J. Sondow, \u003ca href=\"http://mathworld.wolfram.com/UniversalParabolicConstant.html\"\u003eUniversal Parabolic Constant\u003c/a\u003e, MathWorld",
				"E.W. Weisstein, \u003ca href=\"http://mathworld.wolfram.com/RectangularHyperbola.html\"\u003eRectangular hyperbola\u003c/a\u003e, MathWorld",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Hyperbola#Rectangular_hyperbola_with_horizontal.2Fvertical_asymptotes_.28Cartesian_coordinates.29\"\u003eEquilateral hyperbola\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Universal_parabolic_constant\"\u003eUniversal parabolic constant\u003c/a\u003e",
				"\u003ca href=\"/index/Tra#transcendental\"\u003eIndex entries for transcendental numbers\u003c/a\u003e"
			],
			"formula": [
				"Sqrt(2) - arcsinh(1), also equals Integral_{1..infinity} 1/(x^2*(1+x)^(1/2)) dx. - _Jean-François Alcover_, Apr 16 2015",
				"Equals Integral_{x = 0..1} x^2/sqrt(1 + x^2) dx. - _Peter Bala_, Feb 28 2019"
			],
			"example": [
				"0.532839975353552023569079399229905769541511547115312662423384129337355..."
			],
			"maple": [
				"Digits:=100: evalf(sqrt(2)-arcsinh(1)); # _Wesley Ivan Hurt_, Nov 27 2016"
			],
			"mathematica": [
				"RealDigits[Sqrt[2] - Log[1 + Sqrt[2]], 10, 111][[1]]"
			],
			"program": [
				"(PARI) sqrt(2)-log(sqrt(2)+1) \\\\ _Charles R Greathouse IV_, Apr 18 2013",
				"(PARI) sqrt(2)-asinh(1) \\\\ _Charles R Greathouse IV_, Dec 04 2020",
				"(MAGMA) Sqrt(2) - Log(Sqrt(2)+1) // _G. C. Greubel_, Feb 02 2018"
			],
			"xref": [
				"Cf. A002193, A091648, A103710, A103711, A180434, A278386."
			],
			"keyword": "cons,easy,nonn",
			"offset": "0,1",
			"author": "Sylvester Reese and _Jonathan Sondow_, Mar 01 2013",
			"references": 5,
			"revision": 69,
			"time": "2020-12-04T01:17:14-05:00",
			"created": "2013-03-06T11:00:20-05:00"
		}
	]
}