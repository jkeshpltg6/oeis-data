{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A276066",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 276066,
			"data": "1,1,0,1,1,2,1,0,1,2,4,1,4,1,0,1,4,6,8,8,1,6,1,0,1,7,14,22,12,19,12,1,8,1,0,1,13,34,43,48,55,18,35,16,1,10,1,0,1,26,72,105,148,109,116,103,24,56,20,1,12,1,0,1,52,154,276,344,347,398,205,232,166,30,82,24,1,14,1,0,1",
			"name": "Triangle read by rows: T(n,k) is the number of bargraphs of semiperimeter n having a total of k double rises and double falls (n\u003e=2,k\u003e=0). A double rise (fall) in a bargraph is any pair of adjacent up (down) steps.",
			"comment": [
				"Number of entries in row n is 2n-3.",
				"Sum of entries in row n = A082582(n).",
				"T(n,0) = A023431(n-2) = A025246(n+1).",
				"Sum(k*T(n,k),k\u003e=0) = 2*A273714(n)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A276066/b276066.txt\"\u003eRows n = 2..140, flattened\u003c/a\u003e",
				"M. Bousquet-Mélou and A. Rechnitzer, \u003ca href=\"http://dx.doi.org/10.1016/S0196-8858(02)00553-5\"\u003eThe site-perimeter of bargraphs\u003c/a\u003e, Adv. in Appl. Math. 31 (2003), 86-112.",
				"Emeric Deutsch, S Elizalde, \u003ca href=\"http://arxiv.org/abs/1609.00088\"\u003eStatistics on bargraphs viewed as cornerless Motzkin paths\u003c/a\u003e, arXiv preprint arXiv:1609.00088, 2016"
			],
			"formula": [
				"G.f.: G = G(t,z) satisfies zG^2 - (1-z - t^2*z - 2tz^2+t^2*z^2)G + z^2 = 0.",
				"The g.f. B(t,s,z) of bargraphs, where t(s) marks double rises (falls) and z marks semiperimeter, satisfies zB^2 - (1-(1+ts)z +(ts- t-s)z^2)B + z^2 = 0."
			],
			"example": [
				"Row 4 is 1,2,1,0,1 because the 5 (=A082582(4)) bargraphs of semiperimeter 4 correspond to the compositions [1,1,1], [1,2], [2,1], [2,2], [3] and the corresponding drawings show that they have a total of  0, 1, 1, 2, 4 double rises and double falls, respectively.",
				"Triangle starts:",
				"1;",
				"1,0,1;",
				"1,2,1,0,1;",
				"2,4,1,4,1,0,1;",
				"4,6,8,8,1,6,1,0,1."
			],
			"maple": [
				"eq := z*G^2-(1-z-t^2*z-2*t*z^2+t^2*z^2)*G+z^2 = 0: G := RootOf(eq, G): Gser := simplify(series(G, z = 0, 22)): for n from 2 to 20 do P[n] := sort(coeff(Gser, z, n)) end do: for n from 2 to 20 do seq(coeff(P[n], t, j), j = 0 .. 2*n-4) end do; # yields sequence in triangular form.",
				"# second Maple program:",
				"b:= proc(n, y, t) option remember; expand(`if`(n=0, (1-t)*",
				"      z^(y-1), `if`(t\u003c0, 0, b(n-1, y+1, 1)*`if`(t=1, z, 1))+",
				"     `if`(t\u003e0 or y\u003c2, 0, b(n, y-1, -1)*`if`(t=-1, z, 1))+",
				"     `if`(y\u003c1, 0, b(n-1, y, 0))))",
				"    end:",
				"T:= n-\u003e(p-\u003eseq(coeff(p, z, i), i=0..degree(p)))(b(n, 0$2)):",
				"seq(T(n), n=2..12);  # _Alois P. Heinz_, Aug 25 2016"
			],
			"mathematica": [
				"b[n_, y_, t_] := b[n, y, t] = Expand[If[n == 0, (1 - t)*z^(y - 1), If[t \u003c 0, 0, b[n - 1, y + 1, 1]*If[t == 1, z, 1]] + If[t \u003e 0 || y \u003c 2, 0, b[n, y - 1, -1]*If[t == -1, z, 1]] + If[y \u003c 1, 0, b[n - 1, y, 0]]]]; T[n_] := Function[p, Table[Coefficient[p, z, i], {i, 0, Exponent[p, z]}]][b[n, 0, 0]]; Table[T[n], {n, 2, 12}] // Flatten (* _Jean-François Alcover_, Dec 02 2016 after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A023431, A025246, A082582, A273714."
			],
			"keyword": "nonn,tabf",
			"offset": "2,6",
			"author": "_Emeric Deutsch_ and _Sergi Elizalde_, Aug 25 2016",
			"references": 1,
			"revision": 18,
			"time": "2017-08-19T23:08:41-04:00",
			"created": "2016-08-25T15:15:50-04:00"
		}
	]
}