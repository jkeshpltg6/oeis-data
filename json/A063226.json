{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A063226",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 63226,
			"data": "3,7,13,17,23,27,33,37,43,47,53,57,63,67,73,77,83,87,93,97,103,107,113,117,123,127,133,137,143,147,153,157,163,167,173,177,183,187,193,197,203,207,213,217,223,227,233,237,243,247",
			"name": "Dimension of the space of weight 2n cuspidal newforms for Gamma_0(63).",
			"comment": [
				"Also, dimension of the space of weight 2n cuspidal newforms for Gamma_0(88). - _N. J. A. Sloane_, Nov 24 2016",
				"First differences are 4,6,4,6,4,6.... Also  values of k such that k^(10*n) mod 10 = 8*(n mod 2)+1. - _Gary Detlefs_, Jul 04 2014",
				"In other words, numbers n such that n^(2+4*k) + 1 is divisible by 10, for k \u003e= 0. - _Altug Alkan_, Mar 30 2016",
				"The rational generating function, the periodic first differences and Greubel's closed form are an immediate consequence of the structure of formula given by [Martin]. - _R. J. Mathar_, Apr 09 2016",
				"A quasipolynomial of order 2 and degree 1: a(n) = 5n - 3 if n is even and 5n - 2 if n is odd. - _Charles R Greathouse IV_, Nov 03 2021"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A063226/b063226.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"G. Martin, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2004.10.009\"\u003eDimensions of the spaces of cusp forms and newforms on Gamma_0(N) and Gamma_1(N)\u003c/a\u003e, J. Numb. Theory 112 (2005) 298-331, Theorem 1",
				"William A. Stein, \u003ca href=\"http://www.wstein.org/Tables/dimskg0new.gp\"\u003eDimensions of the spaces S_k^{new}(Gamma_0(N))\u003c/a\u003e",
				"William A. Stein, \u003ca href=\"http://wstein.org/Tables/\"\u003eThe modular forms database\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,1,-1)."
			],
			"formula": [
				"a(n) = 4*floor(n/2) + 6*floor((n-1)/2) + 3. - _Gary Detlefs_, Jul 04 2014",
				"G.f.: 3*x - x^2*(-7-6*x+3*x^2)/((1+x)*(x-1)^2). - _R. J. Mathar_, Jul 15 2015",
				"From _G. C. Greubel_, Mar 30 2016: (Start)",
				"a(n) = (1/2)*(10*n - 5 - (-1)^n).",
				"E.g.f.: (5*x + 3)*cosh(x) + (5*x + 2)*sinh(x). (End)"
			],
			"maple": [
				"# see A063195"
			],
			"mathematica": [
				"Table[4 Floor[n/2] + 6 Floor[(n - 1)/2] + 3, {n, 50}] (* or *)",
				"Table[SeriesCoefficient[3 x - x^2 (-7 - 6 x + 3 x^2)/((1 + x) (x - 1)^2), {x, 0, n}], {n, 50}] (* _Michael De Vlieger_, Mar 30 2016 *)",
				"LinearRecurrence[{1, 1, -1}, {3, 7, 13}, 100] (* _G. C. Greubel_, Mar 30 2016 *)"
			],
			"program": [
				"(PARI) x='x+O('x^99); Vec(3*x-x^2*(-7-6*x+3*x^2)/((1+x)*(x-1)^2)) \\\\ _Altug Alkan_, Mar 31 2016",
				"(PARI) a(n)=5*n-3+n%2 \\\\ _Charles R Greathouse IV_, Mar 31 2016"
			],
			"xref": [
				"Cf. A017305 (bisection), A017353 (bisection)."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_, Jul 10 2001",
			"references": 10,
			"revision": 53,
			"time": "2021-11-03T12:53:50-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}