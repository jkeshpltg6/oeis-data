{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A213548",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 213548,
			"data": "1,5,3,15,12,6,35,31,22,10,70,65,53,35,15,126,120,105,81,51,21,210,203,185,155,115,70,28,330,322,301,265,215,155,92,36,495,486,462,420,360,285,201,117,45,715,705,678,630,560,470,365,253,145,55,1001",
			"name": "Rectangular array:  (row n) = b**c, where b(h) = h, c(h) = m(m+1)/2, m = n-1+h, n\u003e=1, h\u003e=1, and ** = convolution.",
			"comment": [
				"Principal diagonal: A213549.",
				"Antidiagonal sums: A051836.",
				"Row 1, (1,2,3,...)**(1,3,6,...): A000332.",
				"Row 2, (1,2,3,...)**(3,6,10,...): A005718.",
				"Row 3, (1,2,3,...)**(6,10,15,...): k*(k+1)*(k^2 + 13*k + 58)/24.",
				"For a guide to related arrays, see A213500."
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A213548/b213548.txt\"\u003eAntidiagonals n = 1..60\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = 5*T(n,k-1) - 10*T(n,k-2) + 10*T(n,k-3) - 5*T(n,k-4) + T(n,k-5).",
				"G.f. for row n:  f(x)/g(x), where f(x) = n*(n+1) - 2*((n-1)^2)*x + n*(n-1)*x^2 and g(x) = 2*(1 - x)^5."
			],
			"example": [
				"Northwest corner (the array is read by falling antidiagonals):",
				".  1,  5,  15,  35,  70, ...",
				".  3, 12,  31,  65, 120, ...",
				".  6, 22,  53, 105, 185, ...",
				". 10, 35,  81, 155, 265, ...",
				". 15, 51, 115, 215, 360, ...",
				". 21, 70, 155, 285, 470, ...",
				"...",
				"T(5,1) = (1)**(15) = 15;",
				"T(5,2) = (1,2)**(15,21) = 1*21 + 2*15 = 51;",
				"T(5,3) = (1,2,3)**(15,21,28) = 1*28 + 2*21 + 3*15 = 115;",
				"T(4,4) = (1,2,3,4)**(10,15,21,28) = 1*28 + 2*21 + 3*15 + 4*10 = 155."
			],
			"mathematica": [
				"b[n_] := n; c[n_] := n (n + 1)/2",
				"t[n_, k_] := Sum[b[k - i] c[n + i], {i, 0, k - 1}]",
				"TableForm[Table[t[n, k], {n, 1, 10}, {k, 1, 10}]]",
				"Flatten[Table[t[n - k + 1, k], {n, 12}, {k, n, 1, -1}]]",
				"r[n_] := Table[t[n, k], {k, 1, 60}]  (* A213548 *)",
				"d = Table[t[n, n], {n, 1, 40}] (* A213549 *)",
				"s[n_] := Sum[t[i, n + 1 - i], {i, 1, n}]",
				"s1 = Table[s[n], {n, 1, 50}] (* A051836 *)"
			],
			"xref": [
				"Cf. A213500."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Jun 16 2012",
			"references": 6,
			"revision": 14,
			"time": "2016-10-05T08:39:24-04:00",
			"created": "2012-06-20T09:55:15-04:00"
		}
	]
}