{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A112128",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 112128,
			"data": "1,-2,4,-8,16,-28,48,-80,128,-202,312,-472,704,-1036,1504,-2160,3072,-4324,6036,-8360,11488,-15680,21264,-28656,38400,-51182,67864,-89552,117632,-153836,200352,-259904,335872,-432480,554952,-709728,904784,-1149916,1457136",
			"name": "Expansion of phi(q^4) / phi(q) in powers of q where phi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A112128/b112128.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"C. Adiga and N. Anitha, \u003ca href=\"http://dx.doi.org/10.1017/S0004972700034742\"\u003eA note on a continued fraction of Ramanujan\u003c/a\u003e, Bull. Austral. Math. Soc. 70 (2004), pp. 489-497. MR2103981 (2005g:11009)",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of (eta(q) / eta(q^16))^2 * (eta(q^8) / eta(q^2))^5 in powers of q.",
				"Euler transform of period 16 sequence [ -2, 3, -2, 3, -2, 3, -2, -2, -2, 3, -2, 3, -2, 3, -2, 0, ...].",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2)) where f(u, v) = u^2 - (1 - 2*u + 2*u^2) * (1 - 2*v + 2*v^2).",
				"G.f.: (Sum_{k in Z} x^(4*k^2)) / (Sum_{k in Z} x^(k^2)) = theta_3(0, x^4) / theta_3(0, x).",
				"G.f.: Product_{k\u003e0} ((1 + x^(2*k)) * (1 + x^(4*k)))^3 / ((1 + x^k) * (1 + x^(8*k)))^2.",
				"Expansion of continued fraction 1 / (1 + 2*x / (1 - x^2 + (x^1 + x^3)^2 / (1 - x^6 + (x^2 + x^6)^2 / (1 - x^10 + (x^3 + x^9)^2 / ...)))).",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (16 t)) = 1/2 * g(t) where q = exp(2 Pi i t) and g() is the g.f. for A208724.",
				"(-1)^n * a(n) = A208933(n). a(2*n) = A131126(n). a(2*n + 2) = -2 * A093160(n). - _Michael Somos_, Dec 11 2016",
				"Convolution inverse of A208274. - _Michael Somos_, Dec 11 2016",
				"a(n) ~ (-1)^n * exp(sqrt(n)*Pi) / (2^(7/2) * n^(3/4)). - _Vaclav Kotesovec_, Nov 15 2017"
			],
			"example": [
				"G.f. = 1 - 2*q + 4*q^2 - 8*q^3 + 16*q^4 - 28*q^5 + 48*q^6 - 80*q^7 + 128*q^8 + ..."
			],
			"mathematica": [
				"QP = QPochhammer; s = QP[q]^2*(QP[q^8]^5/QP[q^2]^5/QP[q^16]^2) + O[q]^40; CoefficientList[s, q] (* _Jean-François Alcover_, Nov 30 2015, adapted from PARI *)",
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 3, 0, q^4] / EllipticTheta[ 3, 0, q], {q, 0, n}]; (* _Michael Somos_, Dec 11 2016 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A)^2 * eta(x^8 + A)^5 / (eta(x^2 + A)^5 * eta(x^16 + A)^2), n))};"
			],
			"xref": [
				"Cf. A093160, A131126, A208724, A208933."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Aug 27 2005",
			"references": 5,
			"revision": 19,
			"time": "2021-03-12T22:24:43-05:00",
			"created": "2005-09-21T03:00:00-04:00"
		}
	]
}