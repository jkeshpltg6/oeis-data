{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003504",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3504,
			"id": "M0728",
			"data": "1,1,2,3,5,10,28,154,3520,1551880,267593772160,7160642690122633501504,4661345794146064133843098964919305264116096,1810678717716933442325741630275004084414865420898591223522682022447438928019172629856",
			"name": "a(0)=a(1)=1; thereafter a(n+1) = sum(a(k)^2,k=0..n)/n (a(n) is not always integral!).",
			"comment": [
				"The sequence appears with a different offset in some other sources. - _Michael Somos_, Apr 02 2006",
				"Also known as Göbel's (or Goebel's) Sequence. Asymptotically, a(n) ~ n*C^(2^n) where C=1.0478... (A115632). A more precise asymptotic formula is given in A116603. - _M. F. Hasler_, Dec 12 2007",
				"Let s(n) = (n-1)*a(n). By considering the p-adic representation of s(n) for primes p=2,3,...,43, one finds that a(44) is the first nonintegral value in this sequence. Furthermore, for n\u003e44, the valuation of s(n) w.r.t. 43 is -2^(n-44), implying that both s(n) and a(n) are nonintegral. - _M. F. Hasler_ and _Max Alekseyev_, Mar 03 2009",
				"a(44) is approximately  5.4093*10^178485291567. - _Hans Havermann_, Nov 14 2017. The fractional part is simply 24/43 (see page 709 of  Guy (1988)).",
				"The more precise asymptotic formula is a(n+1) ~ C^(2^n) * (n + 2 - 1/n + 4/n^2 - 21/n^3 + 138/n^4 - 1091/n^5 + ...). - _Michael Somos_, Mar 17 2012"
			],
			"reference": [
				"R. K. Guy, Unsolved Problems in Number Theory, 3rd edition, Sect. E15.",
				"Clifford Pickover, A Passion for Mathematics, 2005.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A003504/b003504.txt\"\u003eTable of n, a(n) for n=0..16\u003c/a\u003e",
				"R. K. Guy, \u003ca href=\"/A005169/a005169_6.pdf\"\u003eLetter to N. J. A. Sloane\u003c/a\u003e, Sep 25 1986.",
				"R. K. Guy, \u003ca href=\"http://www.jstor.org/stable/2322249\"\u003eThe strong law of small numbers\u003c/a\u003e. Amer. Math. Monthly 95 (1988), no. 8, 697-712.",
				"R. K. Guy, \u003ca href=\"/A005165/a005165.pdf\"\u003eThe strong law of small numbers\u003c/a\u003e. Amer. Math. Monthly 95 (1988), no. 8, 697-712. [Annotated scanned copy]",
				"H. W. Lenstra, Jr., R. K. Guy, and N. J. A. Sloane, \u003ca href=\"/A003504/a003504.pdf\"\u003eCorrespondence, 1975-1978\u003c/a\u003e",
				"N. Lygeros \u0026 M. Mizony, \u003ca href=\"http://igd.univ-lyon1.fr/home/mizony/premiers.html\"\u003eStudy of primality of terms of a_k(n)=(1+(sum from 1 to n-1)(a_k(i)^k))/(n-1)\u003c/a\u003e [Broken link?]",
				"D. Rusin, \u003ca href=\"http://www.math.niu.edu/~rusin/known-math/96/smallnums\"\u003eLaw of small numbers\u003c/a\u003e [Broken link]",
				"D. Rusin, \u003ca href=\"/A003504/a003504.txt\"\u003eLaw of small numbers\u003c/a\u003e [Cached copy]",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/GoebelsSequence.html\"\u003eGöbel's Sequence\u003c/a\u003e",
				"D. Zagier, \u003ca href=\"http://www-groups.mcs.st-andrews.ac.uk/~john/Zagier/Problems.html\"\u003eProblems posed at the St Andrews Colloquium, 1996\u003c/a\u003e",
				"D. Zagier, \u003ca href=\"http://www-groups.mcs.st-andrews.ac.uk/~john/Zagier/Solution5.3.html\"\u003eSolution: Day 5, problem 3\u003c/a\u003e"
			],
			"formula": [
				"a(n+1) = ((n-1) * a(n) + a(n)^2) / n if n\u003e1. - _Michael Somos_, Apr 02 2006",
				"0 = a(n)*(+a(n)*(a(n+1) - a(n+2)) - a(n+1) - a(n+1)^2) +a(n+1)*(a(n+1)^2 - a(n+2)) if n\u003e1. - _Michael Somos_, Jul 25 2016"
			],
			"example": [
				"a(3) = (1 * 2 + 2^2) / 2 = 3 given a(2) = 2."
			],
			"maple": [
				"a:=2: L:=1,1,a: n:=15: for k to n-2 do a:=a*(a+k)/(k+1): L:=L,a od:L; # _Robert FERREOL_, Nov 07 2015"
			],
			"mathematica": [
				"a[n_] := a[n] = Sum[a[k]^2, {k, 0, n-1}]/(n-1); a[0] = a[1] = 1; Table[a[n], {n, 0, 13}] (* _Jean-François Alcover_, Feb 06 2013 *)",
				"With[{n = 14}, Nest[Append[#, (#.#)/(Length[#] - 1)] \u0026, {1, 1}, n - 2]] (* _Jan Mangaldan_, Mar 21 2013 *)"
			],
			"program": [
				"(PARI) A003504(n,s=2)=if(n--\u003e0,for(k=1,n-1,s+=(s/k)^2);s/n,1) \\\\ _M. F. Hasler_, Dec 12 2007",
				"(Python)",
				"a=2; L=[1,1,a]; n=15",
				"for k in range(1,n-1):",
				"....a=a*(a+k)//(k+1)",
				"....L.append(a)",
				"L # _Robert FERREOL_, Nov 07 2015"
			],
			"xref": [
				"Cf. A005166, A005167, A108394, A115632, A116603 (asymptotic formula)."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_, _R. K. Guy_",
			"ext": [
				"a(0)..a(43) are integral, but from a(44) onwards every term is nonintegral - H. W. Lenstra, Jr.",
				"Corrected and extended by _M. F. Hasler_, Dec 12 2007",
				"Further corrections from _Max Alekseyev_, Mar 04 2009"
			],
			"references": 12,
			"revision": 77,
			"time": "2021-12-27T07:44:42-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}