{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A186361",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 186361,
			"data": "1,1,2,5,1,16,8,61,59,272,438,10,1385,3445,210,7936,29080,3304,50521,264871,47208,280,353792,2605002,658806,11200,2702765,27634817,9275838,303380,22368256,315591124,134010580,7016240,15400,199360981,3870632947,2005021876,151003996,1001000",
			"name": "Triangle read by rows: T(n,k) is the number of permutations of {1,2,...,n} having k cycles that are not up-down. A cycle (b(1), b(2), ...) is said to be up-down if, when written with its smallest element in the first position, it satisfies b(1)\u003cb(2)\u003eb(3)\u003c... .",
			"comment": [
				"Row n contains 1 + floor(n/3) entries.",
				"Sum of entries in row n is n!.",
				"T(n,0)=A000111(n+1) (the Euler or up-down numbers).",
				"Sum(k*T(n,k),k\u003e=0) = A186362(n)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A186361/b186361.txt\"\u003eRows n = 0..200, flattened\u003c/a\u003e",
				"E. Deutsch and S. Elizalde, \u003ca href=\"http://arxiv.org/abs/0909.5199v1\"\u003e Cycle up-down permutations\u003c/a\u003e, arXiv:0909.5199v1 [math.CO]."
			],
			"formula": [
				"E.g.f.=(1-sin z)^{s-1}/(1-z)^s.",
				"The trivariate e.g.f. H(t,s,z) of the permutations of {1,2,...,n} with respect to size (marked by z), number of up-down cycles (marked by t), and number of cycles that are not up-down (marked by s) is given by H(t,s,z)=(1-sin z)^{s-t}/(1-z)^s."
			],
			"example": [
				"T(3,1)=1 because we have (123).",
				"T(4,1)=8 because we have (1432), (1)(234), (1342), (1243), (123)(4), (1234), (124)(3), and (134)(2).",
				"Triangle starts:",
				"    1;",
				"    1;",
				"    2;",
				"    5,   1;",
				"   16,   8;",
				"   61,  59;",
				"  272, 438, 10;",
				"  ..."
			],
			"maple": [
				"G := (1-sin(z))^(t-1)/(1-z)^t: Gser := simplify(series(G, z = 0, 16)): for n from 0 to 13 do P[n] := sort(expand(factorial(n)*coeff(Gser, z, n))) end do: for n from 0 to 13 do seq(coeff(P[n], t, j), j = 0 .. floor((1/3)*n)) end do; # yields sequence in triangular form",
				"# second Maple program:",
				"g:= proc(u, o) option remember;",
				"      `if`(u+o=0, 1, add(g(o-1+j, u-j), j=1..u))",
				"    end:",
				"b:= proc(n) option remember; expand(`if`(n=0, 1, add(b(n-j)*",
				"      binomial(n-1, j-1)*((j-1)!*x-g(j-1, 0)*(x-1)), j=1..n)))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=0..degree(p)))(b(n)):",
				"seq(T(n), n=0..14);  # _Alois P. Heinz_, Apr 15 2017"
			],
			"mathematica": [
				"g[u_, o_] := g[u, o] = If[u + o == 0, 1, Sum[g[o-1+j, u-j], {j, 1, u}]];",
				"b[n_] := b[n] = Expand[If[n == 0, 1, Sum[b[n - j]*Binomial[n-1, j-1] * ((j - 1)!*x - g[j - 1, 0]*(x - 1)), {j, 1, n}]]];",
				"T[n_] := Function[p, Table[Coefficient[p, x, i], {i, 0, Exponent[p, x]}] ][b[n]];",
				"Table[T[n], {n, 0, 14}] // Flatten (* _Jean-François Alcover_, Nov 07 2017, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A000111, A186362, A186358."
			],
			"keyword": "nonn,tabf",
			"offset": "0,3",
			"author": "_Emeric Deutsch_, Feb 28 2011",
			"references": 2,
			"revision": 12,
			"time": "2020-08-30T13:36:49-04:00",
			"created": "2011-02-18T22:43:57-05:00"
		}
	]
}