{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A328963",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 328963,
			"data": "1,2,36,72,144,180,576,420,360,864,1296,720,36864,1080,1440,1260,5184,1800,2160,3360,5760,15552,4620,2520,150994944,6480,5400,13440,8640,6300,9663676416,5040,12960,9240,331776,7560,186624,248832,34560,10080,1327104,13860",
			"name": "Smallest k such that n = sigma_0(k) - ((bigomega(k)-1)*omega(k)), where sigma_0 = A000005, omega = A001221, bigomega = A001222.",
			"comment": [
				"a(n) = smallest k for which A328959(k) = n-2. a(31) \u003e 2^28. - _Antti Karttunen_, Nov 17 2019",
				"a(n) \u003c= 2^(n-1)*3^2, with equality for n = 3, 4, 5, 7, 13, 25, 31, 43,... . - _Giovanni Resta_, Nov 18 2019"
			],
			"example": [
				"The sequence of terms together with their prime signatures begins:",
				"        1: ()",
				"        2: (1)",
				"       36: (2,2)",
				"       72: (3,2)",
				"      144: (4,2)",
				"      180: (2,2,1)",
				"      576: (6,2)",
				"      420: (2,1,1,1)",
				"      360: (3,2,1)",
				"      864: (5,3)",
				"     1296: (4,4)",
				"      720: (4,2,1)",
				"    36864: (12,2)",
				"     1080: (3,3,1)",
				"     1440: (5,2,1)",
				"     1260: (2,2,1,1)",
				"     5184: (6,4)",
				"     1800: (3,2,2)",
				"     2160: (4,3,1)",
				"     3360: (5,1,1,1)",
				"     5760: (7,2,1)",
				"    15552: (6,5)",
				"     4620: (2,1,1,1,1)",
				"     2520: (3,2,1,1)",
				"150994944: (24,2)"
			],
			"mathematica": [
				"dat=Table[DivisorSigma[0,n]-(PrimeOmega[n]-1)*PrimeNu[n],{n,1000}];",
				"Table[Position[dat,i][[1,1]],{i,First[Split[Union[dat],#2==#1+1\u0026]]}]"
			],
			"program": [
				"(PARI)",
				"search_up_to = 2^28;",
				"A307408(n) = 2+((bigomega(n)-1)*omega(n));",
				"A328959(n) = (numdiv(n) - A307408(n));",
				"A328963(search_up_to) = { my(m=Map(),t,lista=List([])); for(n=1,search_up_to,t =",
				"A328959(n); if(!mapisdefined(m,t+2), mapput(m,t+2,n))); for(u=1,oo,if(!mapisdefined(m,u,\u0026t),return(Vec(lista)), listput(lista,t))); };",
				"v328963 = A328963(search_up_to);",
				"A328963(n) = v328963[n]; \\\\ _Antti Karttunen_, Nov 17 2019"
			],
			"xref": [
				"Positions of first appearances in A328959.",
				"All terms are in A025487.",
				"Cf. A000005, A001221, A001222, A113901, A124010, A307409, A320632, A323023, A328956, A328958, A328963, A328964, A328965."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Gus Wiseman_, Nov 02 2019",
			"ext": [
				"Definition corrected and terms a(25) - a(30) added by _Antti Karttunen_, Nov 17 2019",
				"a(31)-a(42) from _Giovanni Resta_, Nov 18 2019"
			],
			"references": 11,
			"revision": 33,
			"time": "2019-11-18T10:43:48-05:00",
			"created": "2019-11-03T19:50:43-05:00"
		}
	]
}