{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A269922",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 269922,
			"data": "21,483,483,6468,15018,6468,66066,258972,258972,66066,570570,3288327,5554188,3288327,570570,4390386,34374186,85421118,85421118,34374186,4390386,31039008,313530000,1059255456,1558792200,1059255456,313530000,31039008",
			"name": "Triangle read by rows: T(n,f) is the number of rooted maps with n edges and f faces on an orientable surface of genus 2.",
			"comment": [
				"Row n contains n-3 terms."
			],
			"link": [
				"Gheorghe Coserea, \u003ca href=\"/A269922/b269922.txt\"\u003eRows n = 4..204, flattened\u003c/a\u003e",
				"Sean R. Carrell, Guillaume Chapuy, \u003ca href=\"http://arxiv.org/abs/1402.6300\"\u003eSimple recurrence formulas to count maps on orientable surfaces\u003c/a\u003e, arXiv:1402.6300 [math.CO], 2014."
			],
			"example": [
				"Triangle starts:",
				"n\\f  [1]        [2]        [3]        [4]        [5]        [6]",
				"[4]  21;",
				"[5]  483,       483;",
				"[6]  6468,      15018,     6468;",
				"[7]  66066,     258972,    258972,    66066;",
				"[8]  570570,    3288327,   5554188,   3288327,   570570;",
				"[9]  4390386,   34374186,  85421118,  85421118,  34374186,  4390386;",
				"[10] ..."
			],
			"mathematica": [
				"Q[0, 1, 0] = 1; Q[n_, f_, g_] /; n\u003c0 || f\u003c0 || g\u003c0 = 0;",
				"Q[n_, f_, g_] := Q[n, f, g] = 6/(n+1)((2n-1)/3 Q[n-1, f, g] + (2n-1)/3 Q[n - 1, f-1, g] + (2n-3)(2n-2)(2n-1)/12 Q[n-2, f, g-1] + 1/2 Sum[l = n-k; Sum[v = f-u; Sum[j = g-i; Boole[l \u003e= 1 \u0026\u0026 v \u003e= 1 \u0026\u0026 j \u003e= 0] (2k-1)(2l-1) Q[k-1, u, i] Q[l-1, v, j], {i, 0, g}], {u, 1, f}], {k, 1, n}]);",
				"Table[Q[n, f, 2], {n, 4, 10}, {f, 1, n-3}] // Flatten (* _Jean-François Alcover_, Aug 10 2018 *)"
			],
			"program": [
				"(PARI)",
				"N = 10; G = 2; gmax(n) = min(n\\2, G);",
				"Q = matrix(N + 1, N + 1);",
				"Qget(n, g) = { if (g \u003c 0 || g \u003e n/2, 0, Q[n+1, g+1]) };",
				"Qset(n, g, v) = { Q[n+1, g+1] = v };",
				"Quadric({x=1}) = {",
				"  Qset(0, 0, x);",
				"  for (n = 1, length(Q)-1, for (g = 0, gmax(n),",
				"    my(t1 = (1+x)*(2*n-1)/3 * Qget(n-1, g),",
				"       t2 = (2*n-3)*(2*n-2)*(2*n-1)/12 * Qget(n-2, g-1),",
				"       t3 = 1/2 * sum(k = 1, n-1, sum(i = 0, g,",
				"       (2*k-1) * (2*(n-k)-1) * Qget(k-1, i) * Qget(n-k-1, g-i))));",
				"    Qset(n, g, (t1 + t2 + t3) * 6/(n+1))));",
				"};",
				"Quadric('x);",
				"concat(apply(p-\u003eVecrev(p/'x), vector(N+1 - 2*G, n, Qget(n-1 + 2*G, G))))"
			],
			"xref": [
				"Columns f=1-10 give: A006298 f=1, A288082 f=2, A288083 f=3, A288084 f=4, A288085 f=5, A288086 f=6, A288087 f=7, A288088 f=8, A288089 f=9, A288090 f=10.",
				"Row sums give A006301 (column 2 of A269919).",
				"Cf. A006299 (row maxima), A269921."
			],
			"keyword": "nonn,tabl",
			"offset": "4,1",
			"author": "_Gheorghe Coserea_, Mar 15 2016",
			"references": 14,
			"revision": 32,
			"time": "2018-08-10T17:36:22-04:00",
			"created": "2016-03-16T11:19:11-04:00"
		}
	]
}