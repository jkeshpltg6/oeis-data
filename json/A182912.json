{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A182912",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 182912,
			"data": "1,0,1,-1,-257,-53,5741173,37529,-710165119,-3376971533,360182239526821,104939254406053,-508096766056991140541,-70637580369737593,289375690552473442964467,796424971106808496421869",
			"name": "Numerators of an asymptotic series for the Gamma function (G. Nemes)",
			"comment": [
				"G_n = A182912(n)/A182913(n). These rational numbers provide the coefficients for an asymptotic expansion of the Gamma function."
			],
			"reference": [
				"G. Nemes, More Accurate Approximations for the Gamma Function, Thai Journal of Mathematics Volume 9(1) (2011), 21-28."
			],
			"link": [
				"Peter Luschny, \u003ca href=\"http://www.luschny.de/math/factorial/approx/SimpleCases.html\"\u003eApproximation Formulas for the Factorial Function.\u003c/a\u003e"
			],
			"formula": [
				"Gamma(x+1) ~ x^x e^(-x) sqrt(2Pi(x+1/6)) Sum_{n\u003e=0} G_n / (x+1/4)^n."
			],
			"example": [
				"G_0 = 1, G_1 = 0, G_2 = 1/144, G_3 = -1/12960."
			],
			"maple": [
				"G := proc(n) option remember; local j,J;",
				"J := proc(k) option remember; local j; `if`(k=0,1,",
				"(J(k-1)/k-add((J(k-j)*J(j))/(j+1),j=1..k-1))/(1+1/(k+1))) end:",
				"add(J(2*j)*2^j*6^(j-n)*GAMMA(1/2+j)/(GAMMA(n-j+1)*GAMMA(1/2+j-n)),j=0..n)-add(G(j)*(-4)^(j-n)*(GAMMA(n)/(GAMMA(n-j+1)*GAMMA(j))),j=1..n-1) end:",
				"A182912 := n -\u003e numer(G(n)); seq(A182912(i),i=0..15);"
			],
			"mathematica": [
				"G[n_] := G[n] = Module[{j, J}, J[k_] := J[k] = Module[{j}, If[k == 0, 1, (J[k-1]/k - Sum[J[k-j]*J[j]/(j+1), {j, 1, k-1}])/(1+1/(k+1))]]; Sum[J[2*j]*2^j*6^(j-n)*Gamma[1/2+j]/(Gamma[n-j+1]*Gamma[1/2+j-n]), {j, 0, n}] - Sum[G[j]*(-4)^(j-n)*Gamma[n]/(Gamma[n-j+1]*Gamma[j]), {j, 1, n-1}]]; A182912[n_] := Numerator[G[n]]; Table[A182912[i], {i, 0, 15}] (* _Jean-François Alcover_, Jan 06 2014, translated from Maple *)"
			],
			"xref": [
				"Cf. A001163, A001164, A182913."
			],
			"keyword": "sign,frac",
			"offset": "0,5",
			"author": "_Peter Luschny_, Feb 09 2011",
			"references": 3,
			"revision": 8,
			"time": "2014-01-06T14:55:34-05:00",
			"created": "2010-12-13T20:53:31-05:00"
		}
	]
}