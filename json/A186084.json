{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A186084",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 186084,
			"data": "1,0,1,0,0,1,0,0,1,1,0,0,0,2,1,0,0,0,1,3,1,0,0,0,0,3,4,1,0,0,0,0,1,6,5,1,0,0,0,0,1,4,10,6,1,0,0,0,0,0,3,10,15,7,1,0,0,0,0,0,2,8,20,21,8,1,0,0,0,0,0,1,7,19,35,28,9,1,0,0,0,0,0,0,5,18,40,56,36,10,1,0,0,0,0,0,0,3,16,41,76,84,45,11,1,0,0,0,0,0,0,1,12,41,86,133,120,55,12,1",
			"name": "Triangle T(n,k) read by rows: number of 1-dimensional sand piles (see A186085) with n grains and base length k.",
			"comment": [
				"Compositions of n into k nonzero parts such that the first and last parts are 1 and the absolute difference between consecutive parts is \u003c=1.",
				"Row sums are A186085.",
				"Column sums are the Motzkin numbers (A001006).",
				"First nonzero entry in row n appears in column A055086(n).",
				"From _Joerg Arndt_, Nov 06 2012: (Start)",
				"The transposed triangle (with zeros omitted) is A129181.",
				"For large k, the columns end in reverse([1, 1, 3, 5, 9, 14, 24, 35, ...]) for k even (cf. A053993) and reverse([1, 2, 3, 6, 10, 16, 26, 40, 60, 90, ...]) for k odd (cf. A201077).",
				"The diagonals below the main diagonal are (apart from leading zeros), n, n*(n+1)/2, n*(n+1)*(n+2)/6, and the e-th diagonal appears to have a g.f. of the form f(x)/(1-x)^e.",
				"(End)"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A186084/b186084.txt\"\u003eRows n = 0..140, flattened\u003c/a\u003e",
				"Joerg Arndt, \u003ca href=\"/A186084/a186084.gp.txt\"\u003ethe first 36 rows\u003c/a\u003e as Pari script."
			],
			"formula": [
				"G.f. A(x,y) satisfies:  A(x,y) = 1/(1 - x*y - x^3*y^2*A(x, x*y) ). [_Paul D. Hanna_, Feb 22 2011]",
				"G.f.: (formatting to make the structure apparent)",
				"A(x,y) = 1 /",
				"(1 - x^1*y / (1 - x^2*y / (1 - x^5*y^2 /",
				"(1 - x^3*y / (1 - x^4*y / (1 - x^9*y^2 /",
				"(1 - x^5*y / (1 - x^6*y / (1 - x^13*y^2 /",
				"(1 - x^7*y / (1 - x^8*y / (1 - x^17*y^2 / (1 -...)))))))))))))",
				"(continued fraction). [_Paul D. Hanna_]",
				"G.f.: A(x,y) = 1/(1-x*y - x^3*y^2/(1-x^2*y - x^5*y^2/(1-x^3*y - x^7*y^2/(1 -...)))) (continued fraction). [_Paul D. Hanna_]"
			],
			"example": [
				"Triangle begins:",
				"1;",
				"0,1;",
				"0,0,1;",
				"0,0,1,1;",
				"0,0,0,2,1;",
				"0,0,0,1,3,1;",
				"0,0,0,0,3,4,1;",
				"0,0,0,0,1,6,5,1;",
				"0,0,0,0,1,4,10,6,1;",
				"0,0,0,0,0,3,10,15,7,1;",
				"0,0,0,0,0,2,8,20,21,8,1;",
				"0,0,0,0,0,1,7,19,35,28,9,1;"
			],
			"maple": [
				"b:= proc(n, i) option remember; `if`(n=0, `if`(i=1, 1, 0),",
				"      `if`(n\u003c0 or i\u003c1, 0, expand(x*add(b(n-i, i+j), j=-1..1)) ))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=0..degree(p)))(b(n, 1)):",
				"seq(T(n), n=0..20);  # _Alois P. Heinz_, Jul 24 2013"
			],
			"mathematica": [
				"b[n_, i_] := b[n, i] = If[n == 0, If[i == 1, 1, 0], If[n\u003c0 || i\u003c1, 0, Expand[ x*Sum[b[n-i, i+j], {j, -1, 1}]]]]; T[n_] := Function[{p}, Table[Coefficient[p, x, i], {i, 0, Exponent[p, x]}]][b[n, 1]]; Table[T[n], {n, 0, 20}] // Flatten (* _Jean-François Alcover_, Feb 18 2015, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(PARI) {T(n,k)=local(A=1+x*y);for(i=1,n,A=1/(1-x*y-x^3*y^2*subst(A,y,x*y+x*O(x^n))));polcoeff(polcoeff(A,n,x),k,y)}",
				"/* _Paul D. Hanna_ */"
			],
			"xref": [
				"Cf. A186085 (sand piles with n grains, row sums), A001006 (Motzkin numbers, column sums), A055086.",
				"Cf. A186505 (antidiagonal sums)."
			],
			"keyword": "nonn,tabl",
			"offset": "0,14",
			"author": "_Joerg Arndt_, Feb 13 2011",
			"references": 6,
			"revision": 44,
			"time": "2015-02-18T03:43:16-05:00",
			"created": "2011-02-12T04:09:50-05:00"
		}
	]
}