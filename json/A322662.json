{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A322662",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 322662,
			"data": "1,13,25,109,121,193,325,493,529,661,829,1129,1189,1405,1657,2101,2149,2281,2533,3133,3337,3709,4309,4909,5065,5449,5917,6757,6877,7381,7873,8845,8893,9025,9277,9877,10165,10849,11737",
			"name": "a(n) is to A151723(n+1) as A319018(n+1) is to A147562(n+1), n \u003e= 0.",
			"comment": [
				"Also the number of ON cells after n generations in a knight's-move, one-neighbor, accumulative cellular automaton on the hexagonal lattice A_2. Define v(m)=2*sqrt(3)*[cos(m*Pi/3+Pi/6), sin(m*Pi/3+Pi/6)], vL(m)=2*v(m)+v(m+1), vR(m)=2*v(m)+v(m-1). The set of \"knight's moves\", M={vL(m):m=1,2,..6} U {vR(m):m=1,2,..6}, follows from an analogy between Z^2 and A_2. At each generation all ON cells remain ON while an OFF cell turns ON if and only if it has exactly one M-neighbor in the previous generation.",
				"Fractal Structure Theorem (FST). A pair of lattice vectors M={v1,v2} generate a wedge, W = {x*v1 + y*v2 : x\u003e=0, y\u003e=0}. Define W-Subsets T_k such that T_{k+1}= T_k U { 2^n*v1 + v : v in T_k } U {2^n*v2 + v : v in T_k}, T_0 = { [0,0] }. The limit set T_{oo} is a fractal, and acquires the topology of a binary tree when points are connected by either v1 or v2. As a tree, T_k has height 2^k-1, with 2^k vertices at maximum depth, along a line in the direction v1-v2. Assume a one-M-neighbor, accumulative cellular automaton on W, where all vertices in T_k are ON. In the next generation, the front F_k={2^k*v1+m*(v2-v1) : 0\u003c=m\u003c=2^k} contains only two ON cells, {2^k*v1,2^k*v2}. The spacing, 2^k-1, is wide enough to turn ON two copies of T_k, one starting from each of the two ON cells in F_k. Thus T_{k+1} is also ON. Whenever only T_0 is ON as an initial condition, by induction, T_{oo} is ultimately ON.",
				"The FST applies here to 12 distinct wedges: with {v1,v2}={vL(m), vR(m)} or with (v1,v2)={vL(m), vR(m+1)}, and m=1,2,..6. The triangle inequality ensures that paths including other vectors cannot reach the front F_k by generation 2^k. However, other vectors do generate retrogressive growth, which turns ON many additional cells.",
				"The FST applies to a wide range of Cellular Automata. Wolfram's one-dimensional rule 90 gives the most elementary example where T_{oo} determines every ON cell. The tree structure T_{oo} also occurs with two dimensional, accumulative, one-neighbor C.A. such as A151723, A319018, A147562. Also try: M={[0,1],[0,-1],[2,1],[-2,-1]}.",
				"According to S. Ulam (Cf. Links), some version of the FST was already known to J. Holladay circa 1960.",
				"The FST implies scale resonance between this cellular automaton and the arrowed half hexagon tiling (Cf. Links)."
			],
			"link": [
				"Bradley Klee, \u003ca href=\"/A322662/a322662.png\"\u003eLog-Periodic Coloring over Arrowed Half Hexagon tiling\u003c/a\u003e.",
				"Bradley Klee, \u003ca href=\"/A322662/a322662_1.png\"\u003eLog-Periodic Coloring to Stage 64\u003c/a\u003e.",
				"Bradley Klee, \u003ca href=\"/A322662/a322662_2.png\"\u003eT_n Tree Structure, n=1,2,3,4\u003c/a\u003e.",
				"Bradley Klee, \u003ca href=\"http://demonstrations.wolfram.com/LimitPeriodicTilings/\"\u003eLimit-Periodic Tilings\u003c/a\u003e, Wolfram Demonstrations Project (2015).",
				"S. M. Ulam, \u003ca href=\"/A002858/a002858.pdf\"\u003eOn some mathematical problems connected with patterns of growth of figures\u003c/a\u003e, pp. 216 of R. E. Bellman, ed., Mathematical Problems in the Biological Sciences, Proc. Sympos. Applied Math., Vol. 14, Amer. Math. Soc., 1962 [Annotated scanned copy]"
			],
			"mathematica": [
				"HexStar=2*Sqrt[3]*{Cos[#*Pi/3+Pi/6],Sin[#*Pi/3+Pi/6]}\u0026/@Range[0,5];",
				"MoveSet=Join[2*HexStar+RotateRight[HexStar],2*HexStar+RotateLeft[HexStar]];",
				"Clear@Pts;Pts[0] = {{0, 0}};",
				"Pts[n_]:=Pts[n]=With[{pts=Pts[n-1]},Union[pts,Cases[Tally[Flatten[pts/.{x_,y_}:\u003e Evaluate[{x,y}+#\u0026/@MoveSet],1]],{x_,1}:\u003ex]]];Length[Pts[#]]\u0026/@Range[0,32]"
			],
			"xref": [
				"Hexagonal: A151723. Square: A319018, A147562. Tree: A006046, A267700, A038573. A322663."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Bradley Klee_, Dec 22 2018",
			"references": 2,
			"revision": 23,
			"time": "2019-01-11T19:50:19-05:00",
			"created": "2018-12-24T08:57:12-05:00"
		}
	]
}