{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A087204",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 87204,
			"data": "2,1,-1,-2,-1,1,2,1,-1,-2,-1,1,2,1,-1,-2,-1,1,2,1,-1,-2,-1,1,2,1,-1,-2,-1,1,2,1,-1,-2,-1,1,2,1,-1,-2,-1,1,2,1,-1,-2,-1,1,2,1,-1,-2,-1,1,2,1,-1,-2,-1,1,2,1,-1,-2,-1,1,2,1,-1,-2,-1,1,2,1,-1,-2,-1,1,2,1,-1,-2,-1,1,2,1,-1,-2,-1,1,2,1,-1,-2,-1,1,2,1,-1,-2,-1,1",
			"name": "Period 6: repeat [2, 1, -1, -2, -1, 1].",
			"comment": [
				"Satisfies (a(n))^2 = a(2n) + 2. Shifted differences of itself.",
				"Multiplicative with a(2^e) = -1, a(3^e) = -2, a(p^e) = 1 otherwise. - _David W. Wilson_, Jun 12 2005",
				"Moebius transform is length 6 sequence [1, -2, -3, 0, 0, 6]. - _Michael Somos_, Oct 22 2006"
			],
			"reference": [
				"A. T. Benjamin and J. J. Quinn, Proofs that really count: the art of combinatorial proof, M.A.A. 2003, id. 176."
			],
			"link": [
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Lucas_sequence#Specific_names\"\u003eLucas sequence\u003c/a\u003e",
				"\u003ca href=\"/index/Rea#recur1\"\u003eIndex entries for recurrences a(n) = k*a(n - 1) +/- a(n - 2)\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,-1)."
			],
			"formula": [
				"a(n) = a(n-1) - a(n-2), starting with a(0) = 2 and a(1) = 1.",
				"G.f.: (2-x)/(1-x+x^2).",
				"a(n) = Sum_{k\u003e=0} (-1)^k*n/(n-k)*C(n-k, k).",
				"a(n) = (1/2)*((-1)^floor(n/3) + 2*(-1)^floor((n+1)/3) + (-1)^floor((n+2)/3)).",
				"a(n) = -(1/6)*((n mod 6)+2*((n+1) mod 6)+((n+2) mod 6)-((n+3) mod 6)-2*((n+4) mod 6)-((n+5) mod 6)). - _Paolo P. Lava_, Oct 09 2006",
				"a(n) = a(-n) = -a(n-3) for all n in Z. - _Michael Somos_, Oct 22 2006",
				"E.g.f. 2*exp(x/2)*cos(sqrt(3)*x/2). - _Sergei N. Gladkovskii_, Aug 12 2012",
				"a(n) = r^n + s^n, with r=(1+i*sqrt(3))/2 and s=(1-i*sqrt(3))/2 the roots of 1-x+x^2. - _Ralf Stephan_, Jul 19 2013",
				"a(n) = 2*cos(n*Pi/3). - _Wesley Ivan Hurt_, Jun 19 2016"
			],
			"example": [
				"a(2) = -1 = a(1) - a(0) = 1 - 2 = ((1+sqrt(-3))/2)^2 + ((1-sqrt(-3))/2)^2 = -1 = -2/4 + 2*sqrt(-3)/4 - 2/4 -2 sqrt(-3)/4 = -1.",
				"G.f. = 2 + x - x^2 - 2*x^3 - x^4 + x^5 + 2*x^6 + x^7 - x^8 - 2*x^9 - x^10 + ..."
			],
			"maple": [
				"A087204:=n-\u003e[2, 1, -1, -2, -1, 1][(n mod 6)+1]: seq(A087204(n), n=0..100); # _Wesley Ivan Hurt_, Jun 19 2016"
			],
			"mathematica": [
				"PadLeft[{}, 108, {2,1,-1,-2,-1,1}] (* _Harvey P. Dale_, Sep 11 2011 *)",
				"a[ n_] := {1, -1, -2, -1, 1, 2}[[Mod[n, 6, 1]]]; (* _Michael Somos_, Jan 29 2015 *)",
				"a[ n_] := 2 Re[ Exp[ Pi I n / 3]]; (* _Michael Somos_, Mar 29 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = [2, 1, -1, -2, -1, 1][n%6 + 1]}; /* _Michael Somos_, Oct 22 2006 */",
				"(Sage) [lucas_number2(n,1,1) for n in range(0, 102)] # _Zerinvary Lajos_, Apr 30 2009",
				"(MAGMA) \u0026cat[[2, 1, -1, -2, -1, 1]^^20]; // _Wesley Ivan Hurt_, Jun 19 2016"
			],
			"xref": [
				"Essentially the same as A057079 and A100051. Pairwise sums of A010892."
			],
			"keyword": "sign,easy,mult",
			"offset": "0,1",
			"author": "Nikolay V. Kosinov (kosinov(AT)unitron.com.ua), Oct 19 2003",
			"ext": [
				"Edited by _Ralf Stephan_, Feb 04 2005"
			],
			"references": 11,
			"revision": 53,
			"time": "2021-05-23T02:39:40-04:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}