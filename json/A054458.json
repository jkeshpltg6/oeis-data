{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A054458",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 54458,
			"data": "1,3,1,7,6,1,17,23,9,1,41,76,48,12,1,99,233,204,82,15,1,239,682,765,428,125,18,1,577,1935,2649,1907,775,177,21,1,1393,5368,8680,7656,4010,1272,238,24,1,3363,14641,27312,28548,18358,7506,1946,308,27,1,8119",
			"name": "Convolution triangle based on A001333(n), n \u003e= 1.",
			"comment": [
				"In the language of the Shapiro et al. reference (given in A053121) such a lower triangular (ordinary) convolution array, considered as a matrix, belongs to the Bell-subgroup of the Riordan-group.",
				"The G.f. for the row polynomials p(n,x) (increasing powers of x) is LPell(z)/(1-x*z*LPell(z)) with LPell(z) given in 'Formula'.",
				"Column sequences are A001333(n+1), A054459(n), A054460(n) for m=0..2.",
				"Mirror image of triangle in A209696. - _Philippe Deléham_, Mar 24 2012",
				"Subtriangle of the triangle given by (0, 3, -2/3, -1/3, 0, 0, 0, 0, 0, 0, 0, ...) DELTA (1, 0, 0, 0, 0, 0, 0, 0, 0, 0, ...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, Mar 25 2012",
				"Riordan array ((1+x)/(1-2*x-x^2), (x+x^2)/(1-2*x-x^2)). - _Philippe Deléham_, Mar 25 2012"
			],
			"link": [
				"Milan Janjić, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL21/Janjic/janjic93.html\"\u003eWords and Linear Recurrences\u003c/a\u003e, J. Int. Seq. 21 (2018), #18.1.4."
			],
			"formula": [
				"a(n, m) := ((n-m+1)*a(n, m-1) + (2n-m)*a(n-1, m-1) + (n-1)*a(n-2, m-1))/(4*m), n \u003e= m \u003e= 1; a(n, 0)= A001333(n+1); a(n, m) := 0 if n\u003cm.",
				"G.f. for column m: LPell(x)*(x*LPell(x))^m, m \u003e= 0, with LPell(x)= (1+x)/(1-2*x-x^2) = g.f. for A001333(n+1).",
				"G.f.:  (1+x)/(1-2*x-y*x-x^2-y*x^2). - _Philippe Deléham_, Mar 25 2012",
				"T(n,k) = 2*T(n-1,k) + T(n-1,k-1) + T(n-2,k) + T(n-2,k-1), T(0,0) = T(1,1) = 1, T(1,0) = 3 and T(n,k) = 0 if k\u003c0 or if k\u003en. - _Philippe Deléham_, Mar 25 2012",
				"Sum_{k, 0\u003c=k\u003c=n} T(n,k)*x^k = A040000(n), A001333(n+1), A055099(n), A126473(n), A126501(n), A126528(n) for x = -1, 0, 1, 2, 3, 4 respectively. - _Philippe Deléham_, Mar 25 2012"
			],
			"example": [
				"{1}; {3,1}; {7,6,1}; {17,23,9,1};...",
				"Fourth row polynomial (n=3): p(3,x)= 17+23*x+9*x^2+x^3",
				"Triangle begins :",
				"1",
				"3, 1",
				"7, 6, 1",
				"17, 23, 9, 1",
				"41, 76, 48, 12, 1",
				"99, 233, 204, 82, 15, 1",
				"239, 682, 765, 428, 125, 18, 1.- _Philippe Deléham_, Mar 25 2012",
				"(0, 3, -2/3, -1/3, 0, 0, 0, ...) DELTA (1, 0, 0, 0, ...) begins :",
				"1",
				"0, 1",
				"0, 3, 1",
				"0, 7, 6, 1",
				"0, 17, 23, 9, 1",
				"0, 41, 76, 48, 12, 1",
				"0, 99, 233, 204, 82, 15, 1",
				"0, 239, 682, 765, 428, 125, 15, 1. - _Philippe Deléham_, Mar 25 2012"
			],
			"xref": [
				"Cf. A002203(n+1)/2. Row sums: A055099(n)."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Apr 26 2000",
			"references": 9,
			"revision": 17,
			"time": "2018-04-25T19:08:06-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}