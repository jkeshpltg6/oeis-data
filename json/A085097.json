{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A085097",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 85097,
			"data": "1,-1,2,0,-1,-2,-1,0,-3,1,-1,0,-1,1,-2,0,-1,3,-1,0,-2,1,-1,0,0,1,0,0,-1,2,-1,0,-2,1,1,0,-1,1,-2,0,-1,2,-1,0,3,1,-1,0,0,0,-2,0,-1,0,1,0,-2,1,-1,0,-1,1,3,0,1,2,-1,0,-2,-1,-1,0,-1,1,0,0,1,2,-1,0,0,1,-1,0,1,1,-2,0,-1,-3,1,0,-2,1,1,0,-1,0,3,0,-1,2,-1,0,2,1,-1,0",
			"name": "Ramanujan sum c_n(3).",
			"reference": [
				"T. M. Apostol, Introduction to Analytic Number Theory, Springer-Verlag, 1976.",
				"E. C. Titchmarsh and D. R. Heath-Brown, The theory of the Riemann zeta-function, 2nd ed., 1986.",
				"R. D. von Sterneck, Ein Analogon zur additiven Zahlentheorie, Sitzungsber. Acad. Wiss. Sapientiae Math.-Naturwiss. Kl. 111 (1902), 1567-1601 (Abt. IIa). [It seems that his father, Robert Freiherr Daublebsky von Sterneck, had exactly the same name.]"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A085097/b085097.txt\"\u003eTable of n, a(n) for n = 1..65537\u003c/a\u003e",
				"Tom M. Apostol, \u003ca href=\"https://projecteuclid.org/download/pdf_1/euclid.pjm/1102968273\"\u003eArithmetical properties of generalized Ramanujan sums\u003c/a\u003e, Pacific J. Math. 41 (1972), 281-293.",
				"Eckford Cohen, \u003ca href=\"https://dx.doi.org/10.1073/pnas.41.11.939\"\u003eA class of arithmetic functions\u003c/a\u003e, Proc. Natl. Acad. Sci. USA 41 (1955), 939-944.",
				"A. Elashvili, M. Jibladze, and D. Pataraia, \u003ca href=\"http://dx.doi.org/10.1023/A:1018727630642\"\u003eCombinatorics of necklaces and \"Hermite reciprocity\"\u003c/a\u003e, J. Algebraic Combin. 10 (1999), 173-188.",
				"M. L. Fredman, \u003ca href=\"https://doi.org/10.1016/0097-3165(75)90008-4\"\u003eA symmetry relationship for a class of partitions\u003c/a\u003e, J. Combinatorial Theory Ser. A 18 (1975), 199-202.",
				"Otto Hölder, \u003ca href=\"http://matwbn.icm.edu.pl/ksiazki/pmf/pmf43/pmf4312.pdf\"\u003eZur Theorie der Kreisteilungsgleichung K_m(x)=0\u003c/a\u003e, Prace mat.-fiz. 43 (1936), 13-23.",
				"C. A. Nicol, \u003ca href=\"https://dx.doi.org/10.1073/pnas.39.9.963\"\u003eOn restricted partitions and a generalization of the Euler phi number and the Moebius function\u003c/a\u003e, Proc. Natl. Acad. Sci. USA 39(9) (1953), 963-968.",
				"C. A. Nicol and H. S. Vandiver, \u003ca href=\"https://dx.doi.org/10.1073/pnas.40.9.825 \"\u003eA von Sterneck arithmetical function and restricted partitions with respect to a modulus\u003c/a\u003e, Proc. Natl. Acad. Sci. USA 40(9) (1954), 825-835.",
				"K. G. Ramanathan, \u003ca href=\"https://www.ias.ac.in/article/fulltext/seca/020/01/0062-0069\"\u003eSome applications of Ramanujan's trigonometrical sum C_m(n)\u003c/a\u003e, Proc. Indian Acad. Sci., Sect. A 20 (1944), 62-69.",
				"Srinivasa Ramanujan, \u003ca href=\"http://ramanujan.sirinudi.org/Volumes/published/ram21.pdf\"\u003eOn certain trigonometric sums and their applications in the theory of numbers\u003c/a\u003e, Trans. Camb. Phil. Soc. 22 (1918), 259-276.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Ramanujan%27s_sum\"\u003eRamanujan's sum\u003c/a\u003e.",
				"Aurel Wintner, \u003ca href=\"https://www.jstor.org/stable/2371672\"\u003eOn a statistics of the Ramanujan sums\u003c/a\u003e, Amer. J. Math., 64(1) (1942), 106-114."
			],
			"formula": [
				"a(n) = phi(n)*mu(n/gcd(n, 3)) / phi(n/gcd(n, 3)).",
				"Dirichlet g.f.: (1+3^(1-s))/zeta(s). [Titchmarsh eq. (1.5.4.)] - _R. J. Mathar_, Mar 26 2011"
			],
			"mathematica": [
				"f[list_, i_] := list[[i]]; nn = 105; a =Table[MoebiusMu[n], {n, 1, nn}]; b =Table[If[IntegerQ[3/n], n, 0], {n, 1, nn}];Table[DirichletConvolve[f[a, n], f[b, n], n, m], {m, 1, nn}] (* _Geoffrey Critzer_, Dec 30 2015 *)"
			],
			"program": [
				"(PARI) a(n)=eulerphi(n)*moebius(n/gcd(n,3))/eulerphi(n/gcd(n,3))"
			],
			"xref": [
				"Cf. A086831, A085906."
			],
			"keyword": "sign,easy,mult",
			"offset": "1,3",
			"author": "Yuval Dekel (dekelyuval(AT)hotmail.com), Aug 10 2003",
			"ext": [
				"More terms from _Benoit Cloitre_, Aug 12 2003"
			],
			"references": 7,
			"revision": 29,
			"time": "2021-01-17T11:05:15-05:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}