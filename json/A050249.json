{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A050249",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 50249,
			"data": "294001,505447,584141,604171,971767,1062599,1282529,1524181,2017963,2474431,2690201,3085553,3326489,4393139,5152507,5564453,5575259,6173731,6191371,6236179,6463267,6712591,7204777,7469789,7469797",
			"name": "Weakly prime numbers (changing any one decimal digit always produces a composite number). Also called digitally delicate primes.",
			"comment": [
				"Tao proved that this sequence is infinite. - _T. D. Noe_, Mar 01 2011",
				"For the following values 5, 6, 7, 8, 9, 10 of k, the number of terms \u003c 10^k in this sequence is 0, 5, 35, 334, 3167, 32323. - _Jean-Marc Rebert_, Nov 10 2015"
			],
			"link": [
				"Klaus Brockhaus and Jean-Marc Rebert, \u003ca href=\"/A050249/b050249.txt\"\u003eTable of n, a(n) for n = 1..3167\u003c/a\u003e (first 1317 terms from Klaus Brockhaus)",
				"Michael Filaseta and Jacob Juillerat, \u003ca href=\"https://arxiv.org/abs/2101.08898\"\u003eConsecutive primes which are widely digitally delicate\u003c/a\u003e, arXiv:2101.08898 [math.NT], 2021.",
				"Jon Grantham, \u003ca href=\"https://arxiv.org/abs/2109.03923\"\u003eFinding a Widely Digitally Delicate Prime\u003c/a\u003e, arXiv:2109.03923 [math.NT], 2021.",
				"Jackson Hopper and Paul Pollack, \u003ca href=\"http://arxiv.org/abs/1510.03401\"\u003eDigitally delicate primes\u003c/a\u003e, arXiv:1510.03401 [math.NT], 2015.",
				"Jeremiah T. Southwick, \u003ca href=\"https://scholarcommons.sc.edu/etd/5879/\"\u003eTwo Inquiries Related to the Digits of Prime Numbers\u003c/a\u003e, Ph. D. Dissertation, University of South Carolina (2020).",
				"Terence Tao, \u003ca href=\"http://arxiv.org/abs/0802.3361\"\u003eA remark on primality testing and decimal expansions\u003c/a\u003e, arXiv:0802.3361 [math.NT], 2008-2010; Journal of the Australian Mathematical Society 91:3 (2011), pp. 405-413.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/WeaklyPrime.html\"\u003eWeakly Prime\u003c/a\u003e"
			],
			"mathematica": [
				"fQ[n_] := Block[{d = IntegerDigits@ n, t = {}}, Do[AppendTo[t, FromDigits@ ReplacePart[d, i -\u003e #] \u0026 /@ DeleteCases[Range[0, 9], x_ /; x == d[[i]]]], {i, Length@ d}]; ! AnyTrue[Flatten@ t, PrimeQ]] ; Select[Prime@ Range[10^5], fQ] (* _Michael De Vlieger_, Nov 10 2015, Version 10 *)"
			],
			"program": [
				"(MAGMA) IsA118118:=function(n); D:=Intseq(n); return forall{ \u003ck, j\u003e: k in [1..#D], j in [0..9] | j eq D[k] or not IsPrime(Seqint(S)) where S:=Insert(D, k, k, [j]) }; end function; [ p: p in PrimesUpTo(8000000) | IsA118118(p) ]; // _Klaus Brockhaus_, Feb 28 2011",
				"(PARI) isokp(n) = {v = digits(n); for (k=1, #v, w = v; for (j=0, 9, if (j != v[k], w[k] = j; ntest = subst(Pol(w), x, 10); if (isprime(ntest), return(0));););); return (1);}",
				"lista(nn) = {forprime(p=2, nn, if (isokp(p), print1(p, \", \")););} \\\\ _Michel Marcus_, Dec 15 2015"
			],
			"xref": [
				"Cf. A118118, A158124 (weakly primes), A158125 (weakly primes).",
				"Cf. A137985 (analogous base-2 sequence), A186995 (weak primes in base n)."
			],
			"keyword": "nonn,base",
			"offset": "1,1",
			"author": "_Eric W. Weisstein_",
			"ext": [
				"Edited by _Charles R Greathouse IV_, Aug 02 2010"
			],
			"references": 17,
			"revision": 77,
			"time": "2021-09-10T03:33:12-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}