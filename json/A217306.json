{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A217306",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 217306,
			"data": "1,2,11,17,47,83,269,263,479,839,1559,1579,2999,5039,9355,9479,14759,56131,56135,61343,56879,336791,341351,336815,341279,341275,2020727,2020895,2047651,2020891,4055159,12098587,12125347,12285907,15737755,19128523,39190247",
			"name": "Minimal natural number (in decimal representation) with n prime substrings in base-6 representation (substrings with leading zeros are considered to be nonprime).",
			"comment": [
				"The sequence is well-defined in that for each n the set of numbers with n prime substrings is not empty. Proof: Define m(0):=1, m(1):=2 and m(n+1):=6*m(n)+2 for n\u003e0. This results in m(n)=2*sum_{j=0..n-1} 6^j = 2*(6^n - 1)/5 or m(n)=1, 2, 22, 222, 2222, 22222, …, (in base-6) for n=0,1,2,3,…. Evidently, for n\u003e0 m(n) has n 2’s and these are the only prime substrings in base-6 representation. This is why every substring of m(n) with more than one digit is a product of two integers \u003e 1 (by definition) and can therefore not be prime number.",
				"No term is divisible by 6."
			],
			"link": [
				"Hieronymus Fischer, \u003ca href=\"/A217306/b217306.txt\"\u003eTable of n, a(n) for n = 0..60\u003c/a\u003e"
			],
			"formula": [
				"a(n) \u003e 6^floor(sqrt(8*n-7)-1)/2), for n\u003e0.",
				"a(n) \u003c= 2*(6^n - 1)/5, n\u003e0.",
				"a(n+1) \u003c= 6*a(n)+2."
			],
			"example": [
				"a(1) = 2 = 2_6, since 2 is the least number with 1 prime substring in base-6 representation.",
				"a(2) = 11 = 15_6, since 11 is the least number with 2 prime substrings in base-6 representation (5_6=5 and 15_6=11).",
				"a(3) = 17 = 25_6, since 17 is the least number with 3 prime substrings in base-6 representation (2_6, 5_6, and 25_6).",
				"a(4) = 47 = 115_6, since 47 is the least number with 4 prime substrings in base-6 representation (5_6, 11_6=7, 15_6=11, and 115_6=47).",
				"a(8) = 479 = 2115_6, since 479 is the least number with 8 prime substrings in base-6 representation (2_6, 5_6, 11_6=7, 15_6=11, 21_6=13, 115_6=47, 211_6=79, and 2115_6=479)."
			],
			"xref": [
				"Cf. A019546, A035232, A039996, A046034, A069489, A085823, A211681, A211682, A211684, A211685.",
				"Cf. A035244, A079397, A213300-A213321.",
				"Cf. A217302-A217309."
			],
			"keyword": "nonn,base",
			"offset": "0,2",
			"author": "_Hieronymus Fischer_, Nov 22 2012",
			"references": 2,
			"revision": 12,
			"time": "2014-10-24T04:15:13-04:00",
			"created": "2012-12-05T17:15:12-05:00"
		}
	]
}