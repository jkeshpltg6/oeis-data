{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A182522",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 182522,
			"data": "1,1,2,3,6,9,18,27,54,81,162,243,486,729,1458,2187,4374,6561,13122,19683,39366,59049,118098,177147,354294,531441,1062882,1594323,3188646,4782969,9565938,14348907,28697814,43046721,86093442,129140163,258280326,387420489",
			"name": "a(0) = 1; thereafter a(2*n + 1) = 3^n, a(2*n + 2) = 2 * 3^n.",
			"comment": [
				"This is simply the classic sequence A038754 prefixed by a 1. - _N. J. A. Sloane_, Nov 23 2017",
				"Binomial transform is A057960.",
				"Range of row n of the circular Pascal array of order 6. - _Shaun V. Ault_, May 30 2014",
				"a(n) is also the number of achiral color patterns in a row or cycle of length n using three or fewer colors. Two color patterns are the same if we permute the colors, so ABCAB=BACBA. For a cycle, we can rotate the colors, so ABCAB=CABAB. A row is achiral if it is the same as some color permutation of its reverse. Thus the reversal of ABCAB is BACBA, which is equivalent to ABCAB when we permute A and B. A cycle is achiral if it is the same as some rotation of some color permutation of its reverse. Thus CABAB reversed is BABAC. We can permute A and B to get ABABC and then rotate to get CABAB, so CABAB is achiral. It is interesting that the number of achiral color patterns is the same for rows and cycles. - _Robert A. Russell_, Mar 10 2018"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A182522/b182522.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Shaun V. Ault and Charles Kicey, \u003ca href=\"http://arxiv.org/abs/1407.2197\"\u003eCounting paths in corridors using circular Pascal arrays\u003c/a\u003e, arXiv:1407.2197 [math.CO], 2014.",
				"Shaun V. Ault and Charles Kicey, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2014.05.020\"\u003eCounting paths in corridors using circular Pascal arrays\u003c/a\u003e, Discrete Mathematics, Volume 332, October 2014, Pages 45-54.",
				"Johann Cigler, \u003ca href=\"http://arxiv.org/abs/1501.04750\"\u003eSome remarks and conjectures related to lattice paths in strips along the x-axis\u003c/a\u003e, arXiv:1501.04750 [math.CO], 2015-2016.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,3)."
			],
			"formula": [
				"G.f.: (1 + x - x^2) / (1 - 3*x^2).",
				"Expansion of 1 / (1 - x / (1 - x / (1 + x / (1 + x)))) in powers of x.",
				"a(n+1) = A038754(n).",
				"a(n) = Sum_{k=0..n} A123149(n,k). - _Philippe Deléham_, May 04 2012",
				"a(n) = (3-(1+(-1)^n)*(3-2*sqrt(3))/2)*sqrt(3)^(n-3) for n\u003e0, a(0)=1. - _Bruno Berselli_, Mar 19 2013",
				"a(0) = 1, a(1) = 1, a(n) = a(n-1) + a(n-2) if n is odd, and a(n) = a(n-1) + a(n-2) + a(n-3) if n is even. - _Jon Perry_, Mar 19 2013",
				"For odd n = 2m-1, a(2m-1) = T(m,1)+T(m,2)+T(m,3) for triangle T(m,k) of A140735; for even n = 2m, a(2m) = T(m,1)+T(m,2)+T(m,3) for triangle T(m,k) of A293181. - _Robert A. Russell_, Mar 10 2018",
				"From _Robert A. Russell_, Oct 21 2018: (Start)",
				"a(2m) = S2(m+3,3) - 4*S2(m+2,3) + 5*S2(m+1,3) - 2*S2(m,3).",
				"a(2m-1) = S2(m+2,3) - 3*S2(m+1,3) + 2*S2(m,3), where S2(n,k) is the Stirling subset number A008277.",
				"a(n) = 2*A001998(n-1) - A124302(n) = A124302(n) - 2*A107767(n-1) = A001998(n-1) - A107767(n-1).",
				"a(n) = 2*A056353(n) - A002076(n) = A002076(n) - 2*A320743(n) = A056353(n) - A320743(n).",
				"a(n) = A057427(n) + A052551(n-2) + A304973(n). (End)"
			],
			"example": [
				"1 + x + 2*x^2 + 3*x^3 + 6*x^4 + 9*x^5 + 18*x^6 + 27*x^7 + 54*x^8 + ...",
				"From _Robert A. Russell_, Mar 10 2018: (Start)",
				"For a(4) = 6, the achiral color patterns for rows are AAAA, AABB, ABAB, ABBA, ABBC, and ABCA.  Note that for cycles AABB=ABBA and ABBC=ABCA.  The achiral patterns for cycles are AAAA, AAAB, AABB, ABAB, ABAC, and ABBC.  Note that AAAB and ABAC are not achiral rows.",
				"For a(5) = 9, the achiral color patterns (for both rows and cycles) are AAAAA, AABAA, ABABA, ABBBA, AABCC, ABACA, ABBBC, ABCAB, and ABCBA. (End)"
			],
			"mathematica": [
				"Join[{1}, RecurrenceTable[{a[1] == 1, a[2] == 2, a[n] == 3 a[n - 2]}, a, {n, 40}]] (* _Bruno Berselli_, Mar 19 2013 *)",
				"CoefficientList[Series[(1 + x - x^2)/(1 - 3*x^2), {x,0,50}], x] (* _G. C. Greubel_, Apr 14 2017 *)",
				"Table[If[EvenQ[n], StirlingS2[(n+6)/2,3] - 4 StirlingS2[(n+4)/2,3] + 5 StirlingS2[(n+2)/2,3] - 2 StirlingS2[n/2,3], StirlingS2[(n+5)/2,3] - 3 StirlingS2[(n+3)/2,3] + 2 StirlingS2[(n+1)/2,3]], {n,0,40}] (* _Robert A. Russell_, Oct 21 2018 *)",
				"Join[{1},Table[If[EvenQ[n],2 3^((n-2)/2),3^((n-1)/2)],{n,40}]] (* _Robert A. Russell_, Oct 28 2018 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, n==0, n--; (n%2 + 1) * 3^(n \\ 2))}",
				"(MAGMA) I:=[1,1,2]; [n le 3 select I[n] else 3*Self(n-2): n in [1..40]]; // _Bruno Berselli_, Mar 19 2013",
				"(Maxima) makelist(if n=0 then 1 else (1+mod(n-1,2))*3^floor((n-1)/2), n, 0, 40); /* _Bruno Berselli_, Mar 19 2013 */",
				"(PARI) x='x+O('x^50); Vec((1 + x - x^2) / (1 - 3*x^2)) \\\\ _G. C. Greubel_, Apr 14 2017"
			],
			"xref": [
				"Cf. A038754 (essentially the same sequence), A057960.",
				"Row sums of triangle in A123149. - _Philippe Deléham_, May 04 2012",
				"Also row sums of triangle in A169623.",
				"Column 3 of A305749.",
				"Cf. A124302 (oriented), A001998 (unoriented), A107767 (chiral), for rows, varying offsets.",
				"Cf. A002076 (oriented), A056353 (unoriented), A320743 (chiral), for cycles."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Michael Somos_, May 03 2012",
			"ext": [
				"Edited by _Bruno Berselli_, Mar 19 2013",
				"Definition simplified by _N. J. A. Sloane_, Nov 23 2017"
			],
			"references": 9,
			"revision": 82,
			"time": "2018-12-26T03:42:05-05:00",
			"created": "2012-05-03T19:36:06-04:00"
		}
	]
}