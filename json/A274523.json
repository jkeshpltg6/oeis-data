{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A274523",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 274523,
			"data": "1,0,0,0,1,2,5,8,14,20,30,40,55,70,91,112,141,170,209,250,305,364,444,534,655,796,984,1208,1504,1860,2322,2882,3597,4460,5546,6852,8471,10406,12773,15584,18984,22994,27794,33422,40099,47882,57046,67676,80111",
			"name": "Number of integer partitions of n whose Durfee square has sides of even size.",
			"comment": [
				"A partition of n has a Durfee square of side s if s is the largest number such that the partition contains at least s parts with values \u003e= s."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A274523/b274523.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Aubrey Blecher, Arnold Knopfmacher and Augustine Munagi, \u003ca href=\"https://www.researchgate.net/publication/287067866_Durfee_square_areas_and_associated_partition_identities\"\u003eDurfee square areas and associated partition identities\u003c/a\u003e, 2014.",
				"David A. Corneth, \u003ca href=\"/A274523/a274523.png\"\u003eIllustration of a(6)\u003c/a\u003e",
				"P. Flajolet and R. Sedgewick, \u003ca href=\"http://algo.inria.fr/flajolet/Publications/books.html\"\u003eAnalytic Combinatorics\u003c/a\u003e, 2009; see page 45.",
				"Ljuben R. Mutafchiev, \u003ca href=\"http://dx.doi.org/10.1016/S0377-0427(01)00467-8\"\u003eOn the size of the Durfee square of a random integer partition\u003c/a\u003e, Journal of Computational and Applied Mathematics, Volume 142, Issue 1, 1 May 2002, Pages 173-184.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Durfee_square\"\u003eDurfee square\u003c/a\u003e."
			],
			"formula": [
				"G.f.: Sum_{k\u003e=0} x^((2k)^2)/ Product_{i=1..2k} (1-x^i)^2.",
				"a(n) ~ exp(Pi*sqrt(2*n/3)) / (8*sqrt(3)*n). - _Vaclav Kotesovec_, May 21 2018"
			],
			"example": [
				"a(6)=5 because we have: 4+2, 3+3, 3+2+1, 2+2+2, 2+2+1+1 all having a Durfee square of side s=2."
			],
			"maple": [
				"b:= proc(n, i) option remember; `if`(n=0, 1,",
				"      `if`(i\u003c1, 0, b(n, i-1)+`if`(i\u003en, 0, b(n-i, i))))",
				"    end:",
				"T:= proc(n, k) option remember;",
				"      add(b(m, k)*b(n-k^2-m, k), m=0..n-k^2)",
				"    end:",
				"a:= n-\u003e add(`if`(k::even, T(n,k), 0), k=0..floor(sqrt(n))):",
				"seq(a(n), n=0..70);  # _Alois P. Heinz_, Jun 27 2016"
			],
			"mathematica": [
				"nn = 40; CoefficientList[ Series[Sum[z^((2 h)^2)/Product[(1 - z^i), {i, 1, 2 h}]^2, {h, 0, nn}], {z, 0, nn}], z]",
				"(* or by brute force *)",
				"Table[Count[Map[EvenQ, Map[DurfeeSquare, IntegerPartitions[n]]],",
				"  True], {n, 0, 30}]",
				"(* A program translated from Maple: *)",
				"b[n_, i_] := b[n, i] = If[n == 0, 1,",
				"     If[i \u003c 1, 0, b[n, i - 1] + If[i \u003e n, 0, b[n - i, i]]]];",
				"T[n_, k_] := T[n, k] = Sum[b[m, k]*b[n - k^2 - m, k], {m, 0, n - k^2}];",
				"a[n_] := Sum[If[EvenQ[k], T[n, k], 0], {k, 0, Floor[Sqrt[n]]}];",
				"a /@ Range[0, 70] (* _Jean-François Alcover_, May 31 2021, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A115994."
			],
			"keyword": "nonn",
			"offset": "0,6",
			"author": "_Geoffrey Critzer_, Jun 26 2016",
			"references": 1,
			"revision": 43,
			"time": "2021-05-31T05:36:03-04:00",
			"created": "2016-06-27T10:03:22-04:00"
		}
	]
}