{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A347191",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 347191,
			"data": "2,4,4,8,4,10,6,10,6,16,4,16,8,12,8,18,4,24,8,16,8,20,6,20,12,16,8,32,4,28,8,14,16,24,8,24,8,20,8,40,4,32,12,16,12,24,6,36,12,24,8,32,8,40,16,20,8,32,4,32,12,16,24,32,8,32,8,32,8,60,4,30,12,16,24,32,8,48,10,24",
			"name": "Number of divisors of n^2-1.",
			"comment": [
				"Inspired by problem A1885 in Diophante (see link).",
				"As n^2-1 \u003e 0 is never square, all terms are even.",
				"a(n) = 2 iff n = 2.",
				"a(n) = 4 iff n = 3 or iff n is average of twin prime pairs 'n-1' and 'n+1'; i.e. n is a member of ({3} Union A014574) or equivalently n is a term of A129297 \\ {0,1,2}.",
				"a(n) = 6 iff n is such that the two adjacent integers of n are a prime and a square of another prime: 8, 10, 24, 48, 168, 360, ... (A347194)."
			],
			"link": [
				"Diophante, \u003ca href=\"http://www.diophante.fr/problemes-par-themes/arithmetique-et-algebre/a1-pot-pourri/3848-a1885-caches-derriere-leurs-diviseurs\"\u003eA1885, Cachés derrière leurs diviseurs\u003c/a\u003e (in French).",
				"Adrian Dudek, \u003ca href=\"http://arxiv.org/abs/1507.08893\"\u003eOn the Number of Divisors of n^2-1\u003c/a\u003e, arXiv:1507.08893 [math.NT], 2015."
			],
			"formula": [
				"a(n) = A000005(A005563(n-1)).",
				"a(n) = 2 * A129296(n)."
			],
			"example": [
				"a(5) = tau(5^2-1) = tau(24) = 8.",
				"a(18) = tau(18^2-1) = tau(17*19) = 4, 18 is average of twin primes 17 and 19."
			],
			"maple": [
				"with(numtheory):",
				"seq(tau(n^2-1), n=2..81);"
			],
			"mathematica": [
				"a[n_] := Length[Divisors[n^2 - 1]]; Table[a[n], {n, 2, 81}] (* _Robert P. P. McKone_, Aug 22 2021 *)",
				"Table[DivisorSigma[0, n^2 - 1], {n, 2, 100}] (* _Vaclav Kotesovec_, Aug 23 2021 *)"
			],
			"program": [
				"(PARI) a(n) = numdiv(n^2-1); \\\\ _Michel Marcus_, Aug 23 2021",
				"(PARI) a(n)=my(a=valuation(n-1,2),b=valuation(n+1,2)); numdiv((n-1)\u003e\u003ea)*numdiv((n+1)\u003e\u003eb)*(a+b+1) \\\\ _Charles R Greathouse IV_, Sep 17 2021",
				"(PARI) first(n)=my(v=vector(n-1),x=[1,factor(1)],y=[2,factor(2)]); forfactored(k=3,n+1,  my(e=max(valuation(x[1],2), valuation(k[1],2))); v[k[1]-2]=numdiv(k)*numdiv(x)*(e+2)/(2*e+2); x=y; y=k); v \\\\ _Charles R Greathouse IV_, Sep 17 2021",
				"(Python)",
				"from math import prod",
				"from sympy import factorint",
				"def a(n):",
				"    ft = factorint(n+1, multiple=True) + factorint(n-1, multiple=True)",
				"    return prod((e + 1) for e in (ft.count(f) for f in set(ft)))",
				"print([a(n) for n in range(2, 82)]) # _Michael S. Branicky_, Sep 17 2021"
			],
			"xref": [
				"Cf. A000005, A005563, A014574, A129296, A129297.",
				"Cf. A347192 (records), A347193 (smallest k with a(k) = n), A347194 (a(n)=6)."
			],
			"keyword": "nonn",
			"offset": "2,1",
			"author": "_Bernard Schott_, Aug 22 2021",
			"references": 3,
			"revision": 37,
			"time": "2021-09-17T14:07:40-04:00",
			"created": "2021-09-17T14:07:40-04:00"
		}
	]
}