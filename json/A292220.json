{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A292220",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 292220,
			"data": "1,1,-4,30,-336,5040,-95040,2162160,-57657600,1764322560,-60949324800,2346549004800,-99638080819200,4626053752320000,-233153109116928000,12677700308232960000,-739781100339240960000,46113021921146019840000,-3058021453718104473600000",
			"name": "Expansion of the exponential generating function (1/2)*(1 + 4*x)*(1 - (1 + 4*x)^(-1/2))/x.",
			"comment": [
				"This gives one half of the z-sequence entries for the generalized unsigned Lah number Sheffer matrix Lah[4,1] = A048854.",
				"For Sheffer a- and z-sequences see a W. Lang link under A006232 with the references for the Riordan case, and also the present link for a proof."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A292220/b292220.txt\"\u003eTable of n, a(n) for n = 0..366\u003c/a\u003e",
				"Wolfdieter Lang, \u003ca href=\"/A290597/a290597.log.txt\"\u003eNote on a- and z-sequences of Sheffer number triangles for certain generalized Lah numbers.\u003c/a\u003e"
			],
			"formula": [
				"a(n) = [x^n/n!] (1/2)*(1 + 4*x)*(1 - (1 + 4*x)^(-1/2))/x.",
				"a(0) = 1, a(n) = -(-2)^n*Product_{j=1..n} (2*j - 1)/(n+1) =  -((-2)^n/(n+1))*A001147(n), n \u003e= 1.",
				"a(n) ~ -(-1)^n * n^(n-1) * 2^(2*n + 1/2) / exp(n). - _Vaclav Kotesovec_, Sep 18 2017",
				"a(n+1) = -2*(1 + 2*n)*(1 + n)*a(n)/(2 + n) for n \u003e= 1. - _Robert Israel_, May 10 2020"
			],
			"example": [
				"The sequence z(4,1;n) = 2*a(n) begins: {2,2,-8,60,-672,10080,-190080,4324320,-115315200,3528645120,-121898649600,...}."
			],
			"maple": [
				"f:= gfun:-rectoproc({a(n+1) = -2*(1 + 2*n)*(1 + n)*a(n)/(2 + n),a(0)=1,a(1)=1},a(n),remember):",
				"map(f, [$0..30]); # _Robert Israel_, May 10 2020"
			],
			"mathematica": [
				"With[{nn=20},CoefficientList[Series[1/2 (1+4x) (1-(1+4x)^(-1/2))/x,{x,0,nn}],x] Range[ 0,nn]!] (* _Harvey P. Dale_, Aug 01 2021 *)"
			],
			"xref": [
				"Cf. A001147, A006232 (link), A048854, A292221 (z[4,3]/2)."
			],
			"keyword": "sign,easy",
			"offset": "0,3",
			"author": "_Wolfdieter Lang_, Sep 13 2017",
			"references": 3,
			"revision": 15,
			"time": "2021-08-01T12:59:52-04:00",
			"created": "2017-09-14T09:32:25-04:00"
		}
	]
}