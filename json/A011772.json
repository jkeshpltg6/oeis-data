{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A011772",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 11772,
			"data": "1,3,2,7,4,3,6,15,8,4,10,8,12,7,5,31,16,8,18,15,6,11,22,15,24,12,26,7,28,15,30,63,11,16,14,8,36,19,12,15,40,20,42,32,9,23,46,32,48,24,17,39,52,27,10,48,18,28,58,15,60,31,27,127,25,11,66,16,23,20,70,63,72,36,24",
			"name": "Smallest number m such that m(m+1)/2 is divisible by n.",
			"comment": [
				"The graph of the function is split into rays of which the densest ones are y(n) = n-1 = a(n) iff n is an odd prime power, and y(n) = n/2 = a(n) or a(n)+1 if n = 8k-2 (except for k = 9, 10, 14, 16, 19, 24, ...) or 8k+2 (except for k = 8, 11, 16, 17, 19, 26, 33, ...). The next most-frequent rays are similar: y(n) = n/r for r = 3, 4, 5, ... and r = 4/3, etc. - _M. F. Hasler_, May 30 2021",
				"a(n) = A344005(2*n). - _N. J. A. Sloane_, Jul 06 2021"
			],
			"reference": [
				"C. Ashbacher, The Pseudo-Smarandache Function and the Classical Functions of Number Theory, Smarandache Notions Journal, Vol. 9, No. 1-2, 1998, 79-82."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A011772/b011772.txt\"\u003eTable of n, a(n) for n = 1..16383\u003c/a\u003e (first 1000 terms from T. D. Noe)",
				"Jason Earls, \u003ca href=\"https://www.researchgate.net/publication/262314336_The_Smarandache_sum_of_composites_between_factors_function\"\u003eThe Smarandache sum of composites between factors function\u003c/a\u003e, in Smarandache Notions Journal (2004), Vol. 14.1, page 246.",
				"K. Kashihara, \u003ca href=\"http://www.gallup.unm.edu/~smarandache/Kashihara.pdf\"\u003eComments and Topics on Smarandache Notions and Problems\u003c/a\u003e, Erhus University Press, 1996, 50 pages. See p. 35.",
				"K. Kashihara, \u003ca href=\"/A011772/a011772.pdf\"\u003eComments and Topics on Smarandache Notions and Problems\u003c/a\u003e, Erhus University Press, 1996, 50 pages. [Cached copy] See p. 35.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PseudosmarandacheFunction.html\"\u003ePseudosmarandache Function\u003c/a\u003e"
			],
			"formula": [
				"a(2^k) = 2^(k+1)-1; a(m) = m-1 for odd prime powers m. - _Reinhard Zumkeller_, Feb 26 2003",
				"a(n) \u003c= 2n-1 for all numbers n; a(n) \u003c= n-1 for odd n. - _Stefan Steinerberger_, Apr 03 2006",
				"a(n) \u003e= (sqrt(8n+1)-1)/2 for all n. - _Charles R Greathouse IV_, Jun 25 2017",
				"a(n) \u003c n-1 for all n except the prime powers where a(n) = n-1 (n odd) or 2n-1 (n = 2^k). - _M. F. Hasler_, May 30 2021"
			],
			"mathematica": [
				"Table[m := 1; While[Not[IntegerQ[(m*(m + 1))/(2n)]], m++ ]; m, {n, 1, 90}] (* _Stefan Steinerberger_, Apr 03 2006 *)",
				"(Sqrt[1+8#]-1)/2\u0026/@Flatten[With[{r=Accumulate[Range[300]]},Table[ Select[r, Divisible[#,n]\u0026,1],{n,80}]]] (* _Harvey P. Dale_, Feb 05 2012 *)"
			],
			"program": [
				"(Haskell)",
				"import Data.List (findIndex)",
				"import Data.Maybe (fromJust)",
				"a011772 n = (+ 1) $ fromJust $",
				"   findIndex ((== 0) . (`mod` n)) $ tail a000217_list",
				"-- _Reinhard Zumkeller_, Mar 23 2013",
				"(PARI) a(n)=if(n==1,return(1)); my(f=factor(if(n%2,n,2*n)), step=vecmax(vector(#f~, i, f[i,1]^f[i,2]))); forstep(m=step,2*n,step, if(m*(m-1)/2%n==0, return(m-1)); if(m*(m+1)/2%n==0, return(m))) \\\\ _Charles R Greathouse IV_, Jun 25 2017",
				"(Python 3.8+)",
				"from math import isqrt",
				"def A011772(n):",
				"    m = (isqrt(8*n+1)-1)//2",
				"    while (m*(m+1)) % (2*n):",
				"        m += 1",
				"    return m # _Chai Wah Wu_, May 30 2021"
			],
			"xref": [
				"A066561(n)=A000217(a(n)).",
				"Cf. A343995, A343996, A343997, A343998, A345984 (partial sums).",
				"Cf. also A080982, A344005."
			],
			"keyword": "nonn,easy,look,nice",
			"offset": "1,2",
			"author": "Kenichiro Kashihara (Univxiq(AT)aol.com)",
			"ext": [
				"More terms from _Stefan Steinerberger_, Apr 03 2006"
			],
			"references": 82,
			"revision": 77,
			"time": "2021-07-09T01:37:32-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}