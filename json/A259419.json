{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A259419",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 259419,
			"data": "1,2,1,-3,-7,-17,2,101,247,571,-1669,-13766,-43101,-205897,1640929,8217293,101727662,173114917,-5439590147,-70987557871,-993401657957,2105332714614,208894282701569,3110590234593151,37334338699443371,-891470356091782993,-33292234868859857114",
			"name": "A Somos-4 like sequence connected with the elliptic curve y^2 + y = x^3 - 3x + 4.",
			"comment": [
				"All the terms of the sequence are integers. Moreover, a(n)^2 is the denominator of the x-coordinate of (2n+3)P, where P = (4,7) is the point on the elliptic curve E: y^2 + y = x^3 - 3x + 4."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A259419/b259419.txt\"\u003eTable of n, a(n) for n = 0..195\u003c/a\u003e",
				"Alexi Block Gorman, Tyler Genao, Heesu Hwang, Noam Kantor, Sarah Parsons, Jeremy Rouse, \u003ca href=\"http://arxiv.org/abs/1508.02464\"\u003eThe density of primes dividing a particular non-linear recurrence sequence\u003c/a\u003e, arXiv:1508.02464 [math.NT], 2015.",
				"C. Kimberling, \u003ca href=\"http://www.fq.math.ca/Scanned/17-1/kimberling1.pdf\"\u003eStrong divisibility sequences and some conjectures\u003c/a\u003e, Fib. Quart., 17 (1979), 13-17."
			],
			"formula": [
				"a(n) = (a(n-1)*a(n-3) - a(n-2)^2)/a(n-4) if n is not 2 mod 3, and a(n) = (a(n-1)*a(n-3) - 3*a(n-2)^2)/a(n-4) if n is 2 mod 3.",
				"a(n) = - a(-3-n) for all n in Z. - _Michael Somos_, Aug 13 2015",
				"a(n)*a(n+7) = -1*a(n+1)*a(n+6) +5*a(n+3)*a(n+4) for all n in Z. - _Michael Somos_, Aug 13 2015",
				"a(n)*a(n+8) = -4*a(n+2)*a(n+6) +5*a(n+3)*a(n+5) for all n in Z. - _Michael Somos_, Aug 13 2015",
				"Let t(n) be a strong elliptic divisibility sequence as given in [Kimberling, p. 16] where x = 5^(1/4), y = 3^(1/3), z = 1. Then a(n) = t(2*n + 3) / if( 3|n, y, 1). - _Michael Somos_, Aug 13 2015"
			],
			"example": [
				"If P = (4,7), then (2*4+3)*P = (-104/49, 181/343). The denominator of the x-coordinate is 49 = a(4)^2."
			],
			"mathematica": [
				"a[ n_] := Module[ {v, m, s = 1}, m = If[ n \u003c -1, s = -1; -3 - n, n] + 5; v = Join[{-2, -1, -1, 1, 1, 2, 1}, Table[0, {m - 7}]]; Do[ v[[k]] = (5 v[[k - 3]] v[[k - 4]] - v[[k - 1]] v[[k - 6]]) / v[[k - 7]], {k, 8, m}]; s v[[m]]]; (* _Michael Somos_, Aug 13 2015 *)"
			],
			"program": [
				"(PARI) a = vector(99); a[1]=2; a[2]=1; a[3] = -3; a[4] = -7; for(n=5,#a,if(Mod(n,3)==Mod(2,3),a[n]=(a[n-1]*a[n-3]-3*a[n-2]^2)/a[n-4],a[n]=(a[n-1]*a[n-3]-a[n-2]^2)/a[n-4])); a",
				"(PARI) {a(n) = my(v, s=1); if( n\u003c-1, n = -3-n; s = -1); n += 5; v = concat( [-2, -1, -1, 1, 1, 2, 1], vector( max(0, n-7))); for(k=8, n, v[k] = (5 * v[k-3] * v[k-4] - v[k-1] * v[k-6]) / v[k-7]); s * v[n]}; /* _Michael Somos_, Aug 13 2015 */"
			],
			"xref": [
				"Cf. A006720."
			],
			"keyword": "easy,sign",
			"offset": "0,2",
			"author": "_Jeremy Rouse_, Jun 26 2015",
			"references": 1,
			"revision": 22,
			"time": "2016-07-25T03:24:28-04:00",
			"created": "2015-06-27T07:36:14-04:00"
		}
	]
}