{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A054434",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 54434,
			"data": "1,88179840,43252003274489856000,177628724197557644876978255387965784064000000000,282870942277741856536180333107150328293127731985672134721536000000000000000",
			"name": "Number of possible positions in an n X n X n Rubik's cube reachable from the starting position.",
			"comment": [
				"The sequence counts possible positions of the Rubik's cube considering the positions which are related through rotations of the cube as a whole (there are 24 of those) as distinct. At odd n, the orientation of the cube as a whole is usually considered fixed by the central squares of each face (i. e., the cube as a whole cannot be rotated) so there is a difference compared to A075152 only in the case of even n. - _Andrey Zabolotskiy_, Jun 07 2016"
			],
			"link": [
				"Francocube forum, \u003ca href=\"http://forum.francocube.com/viewtopic.php?t=3212\"\u003e[4x4x4] Les maths du 4x4x4\u003c/a\u003e",
				"Georges Helm, \u003ca href=\"http://cube.helm.lu/myweb/cubeold.htm\"\u003eRubik's Cube\u003c/a\u003e",
				"M. E. Larsen, \u003ca href=\"http://www.jstor.org/stable/2322445\"\u003eRubik's Revenge: The Group Theoretical Solution\u003c/a\u003e, Amer. Math. Monthly 92, 381 (1985), DOI:10.2307/2322445.",
				"Christopher Mowla, \u003ca href=\"https://docs.google.com/file/d/0Bx0h7-zg0f4NTUM1MWUtaDJrbms\"\u003eMath 3900\u003c/a\u003e",
				"Robert Munafo, \u003ca href=\"http://mrob.com/cube/cube-puzzles.html\"\u003eRubik's Cube and other Cuboid Puzzles\u003c/a\u003e",
				"Philippe Picart, \u003ca href=\"http://trucsmaths.free.fr/rubik.htm\"\u003eLe Rubik's cube\u003c/a\u003e",
				"E. Rubik, \u003ca href=\"http://www.rubiks.com/\"\u003eRubik Cube Site\u003c/a\u003e",
				"Jaap Scherphuis, \u003ca href=\"http://www.jaapsch.net/puzzles/\"\u003ePuzzle Pages\u003c/a\u003e",
				"Xavier Servantie, \u003ca href=\"http://web.archive.org/web/20030621195327/http://www-ensimag.imag.fr/eleves/Xavier.Servantie/rubik/index.eng.html\"\u003eAll about Rubik's cube\u003c/a\u003e",
				"Author?, \u003ca href=\"http://rubiks.cube.resolve.free.fr/chiffres.htm\"\u003eRubik's Cube\u003c/a\u003e",
				"\u003ca href=\"/index/Ru#Rubik\"\u003eIndex entries for sequences related to Rubik cube\u003c/a\u003e"
			],
			"formula": [
				"From _Andrey Zabolotskiy_, Jun 24 2016: (Start)",
				"a(n) = A075152(n)*24 if n is even,",
				"a(n) = A075152(n) if n is odd.",
				"a(2) = Sum(A080629) = Sum(A080630). (End)",
				"a(1)=1; a(2)=24*7!*3^6; a(3)=8!*3^7*12!*2^10; a(n)=a(n-2)*24^6*(24!/24^6)^(n-2). - _Herbert Kociemba_, Dec 08 2016"
			],
			"example": [
				"From _Andrey Zabolotskiy_, Jun 24 2016 [following Munafo]: (Start)",
				"a(4) = 8! * 3^7 * 24! * 24! / 4!^6 is constituted by:",
				"8! permutation of corners",
				"× (12*2)! permutation of edges",
				"× (6*4)! permutation of centers",
				"× 1 (combination of permutations must be even, but we can achieve what appears to be an odd permutation of the other pieces in the cube by \"hiding\" a transposition within the indistinguishable pieces of one color)",
				"× 3^8 orientations of corners",
				"/ 3 total orientation of corners must be zero",
				"× 1 (orientations of edges and centers are determined by their position)",
				"/ 4!^6 the four center pieces of each color are indistinguishable",
				"(End)"
			],
			"mathematica": [
				"f[1]=1; f[2]=24*7!3^6; f[3]=8!3^7 12!2^10; f[n_]:=f[n-2]*24^6*(24!/24^6)^(n-2); Table[f[n], {n, 1, 10}] (* _Herbert Kociemba_, Dec 08 2016 *)"
			],
			"xref": [
				"See A075152, A007458 for other versions."
			],
			"keyword": "nonn,nice",
			"offset": "1,2",
			"author": "Antreas P. Hatzipolakis",
			"ext": [
				"a(4) and a(5) corrected and definition clarified by _Andrey Zabolotskiy_, Jun 24 2016"
			],
			"references": 14,
			"revision": 44,
			"time": "2017-06-26T03:45:27-04:00",
			"created": "2000-06-15T03:00:00-04:00"
		}
	]
}