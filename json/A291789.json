{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A291789",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 291789,
			"data": "270,396,606,712,851,852,1148,1416,2032,2488,2960,4110,5512,6918,8076,10780,16044,23784,33720,55240,73230,97672,118470,169840,247224,350260,442848,728448,1213440,2124864,4080384,8159616,13515078,15767596,18626016,29239504,39012864,62623600,92580308",
			"name": "Trajectory of 270 under repeated application of k -\u003e (phi(k)+sigma(k))/2.",
			"comment": [
				"The ultimate fate of this trajectory is presently unknown. It may reach a fractional value (when it dies), it may reach a prime (which would be a fixed point), it may enter a cycle of length greater than 1, or it may be unbounded. - _Hugo Pfoertner_ and _N. J. A. Sloane_, Sep 18 2017"
			],
			"link": [
				"Sean A. Irvine, \u003ca href=\"/A291789/b291789.txt\"\u003eTable of n, a(n) for n = 0..515\u003c/a\u003e [Terms through a(250) from Hugo Pfoertner, terms a(251)-a(356) from N. J. A. Sloane]",
				"Sean A. Irvine, \u003ca href=\"/A291789/a291789_1.png\"\u003eIllustration of A291789 showing a(n+1)/a(n) (red), cumulative mean of a(n+1)/a(n) (green), and power of 2 in a(n) (blue)\u003c/a\u003e",
				"Hugo Pfoertner, \u003ca href=\"/A291789/a291789.png\"\u003eIllustration of A291789 using a recursive 5th order Butterworth filter with normalized cut-off frequency of 0.1 (0.5\u003c-\u003eNyquist frequency) to smooth the data.\u003c/a\u003e",
				"N. J. A. Sloane, Three (No, 8) Lovely Problems from the OEIS, Experimental Mathematics Seminar, Rutgers University, Oct 05 2017, \u003ca href=\"https://vimeo.com/237029685\"\u003ePart I\u003c/a\u003e, \u003ca href=\"https://vimeo.com/237030304\"\u003ePart 2\u003c/a\u003e, \u003ca href=\"https://oeis.org/A290447/a290447_slides.pdf\"\u003eSlides.\u003c/a\u003e (Mentions this sequence)"
			],
			"maple": [
				"orbit:= proc(n, m) uses numtheory;",
				"  local V,k;",
				"  V:= Vector(m);",
				"  V[1]:= n;",
				"  for k from 2 to m do V[k]:= (sigma(V[k-1])+ phi(V[k-1]))/2 od:",
				"  convert(V,list)",
				"end proc:",
				"orbit(270, 200); # _Robert Israel_, Sep 07 2017"
			],
			"mathematica": [
				"NestWhileList[If[! IntegerQ@ #, -1/2, (DivisorSigma[1, #] + EulerPhi@ #)/2] \u0026, 270, Nor[! IntegerQ@ #, SameQ@ ##] \u0026, 2, 38] (* _Michael De Vlieger_, Sep 19 2017 *)"
			],
			"xref": [
				"Cf. A000010, A000203, A289997, A290001, A291790, A291787, A291804, A291805.",
				"See A291914 and A292108 for the \"big picture\"."
			],
			"keyword": "nonn",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_, Sep 03 2017",
			"references": 7,
			"revision": 52,
			"time": "2017-10-09T15:28:31-04:00",
			"created": "2017-09-03T14:59:57-04:00"
		}
	]
}