{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A008811",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 8811,
			"data": "0,1,2,3,4,7,10,13,16,21,26,31,36,43,50,57,64,73,82,91,100,111,122,133,144,157,170,183,196,211,226,241,256,273,290,307,324,343,362,381,400,421,442,463,484,507,530,553,576",
			"name": "Expansion of x*(1+x^4)/((1-x)^2*(1-x^4)).",
			"comment": [
				"Number of 0..n-1 arrays of 5 elements with zero 2nd differences. - _R. H. Hardin_, Nov 15 2011"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A008811/b008811.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Daniel Gabric and Joe Sawada, \u003ca href=\"http://www.cis.uoguelph.ca/~sawada/papers/discrep.pdf\"\u003eInvestigating the discrepancy property of de Bruijn sequences\u003c/a\u003e, University of Guelph (Canada, 2020).",
				"János Pach and Pankaj K. Agarwal, \u003ca href=\"https://archive.org/details/combinatorialgeo0000pach/page/220/mode/2up\"\u003eCombinatorial Geometry\u003c/a\u003e, p. 220, 1995, Problem 13.10.",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,-1,0,1,-2,1)."
			],
			"formula": [
				"G.f.: x*(1+x^4)/((1-x)^2*(1-x^4)).",
				"a(n) = 2*a(n-1) -a(n-2) +a(n-4) -2*a(n-5) +a(n-6). - _R. H. Hardin_, Nov 15 2011",
				"a(n) = (-2*(1+(-1)^n)*(-1)^floor(n/2) + 2*n^2 + 5 - (-1)^n)/8. - _Tani Akinari_, Jul 24 2013",
				"E.g.f.: ((2 + x + x^2)*cosh(x) + (3 + x + x^2)*sinh(x) - 2*cos(x))/4. - _Stefano Spezia_, May 26 2021"
			],
			"maple": [
				"f := n-\u003en^2/4+3*n/2+g(n);",
				"g := n-\u003eif n mod 2 = 0 then 3 elif n mod 4 = 1 then 9/4 else 13/4; fi;",
				"seq(f(n), n=-3..50);"
			],
			"mathematica": [
				"CoefficientList[Series[x*(1+x^4)/((1-x)^2*(1-x^4)), {x,0,60}], x] (* _G. C. Greubel_, Sep 12 2019 *)"
			],
			"program": [
				"(PARI) concat([0], Vec(x*(1+x^4)/((1-x)^2*(1-x^4))+O(x^60))) \\\\ _Charles R Greathouse IV_, Sep 26 2012, modified by _G. C. Greubel_, Sep 12 2019",
				"(MAGMA) R\u003cx\u003e:=PowerSeriesRing(Integers(), 60); [0] cat Coefficients(R!( x*(1+x^4)/((1-x)^2*(1-x^4)) )); // _G. C. Greubel_, Sep 12 2019",
				"(Sage)",
				"def A008811_list(prec):",
				"    P.\u003cx\u003e = PowerSeriesRing(ZZ, prec)",
				"    return P(x*(1+x^4)/((1-x)^2*(1-x^4))).list()",
				"A008811_list(60) # _G. C. Greubel_, Sep 12 2019",
				"(GAP) a:=[0,1,2,3,4,7];; for n in [7..60] do a[n]:=2*a[n-1]-a[n-2] +a[n-4]-2*a[n-5]+a[n-6]; od; a; # _G. C. Greubel_, Sep 12 2019"
			],
			"xref": [
				"Cf. A129756 (first differences).",
				"Cf. Expansions of the form (1+x^m)/((1-x)^2*(1-x^m)): A000290 (m=1), A000982 (m=2), A008810 (m=3), this sequence (m=4), A008812 (m=5), A008813 (m=6), A008814 (m=7), A008815 (m=8), A008816 (m=9), A008817 (m=10)."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"references": 11,
			"revision": 33,
			"time": "2021-05-29T20:03:08-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}