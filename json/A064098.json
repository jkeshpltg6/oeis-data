{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A064098",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 64098,
			"data": "1,1,1,2,5,29,433,37666,48928105,5528778008357,811537892743746482789,13460438563050022083842073547074914,32770967840592833551621556305285371426044732591005957081",
			"name": "a(n+1) = (a(n)^2 + a(n-1)^2)/a(n-2), with a(1) = a(2) = a(3) = 1.",
			"comment": [
				"This sequence was introduced by Dana Scott but possibly studied earlier by others. - _James Propp_, Jan 27 2005",
				"Sequence gives the upper-left entries of the respective matrices",
				"[1 1] [1 0] [2 1] [5 2] [29 12] [433 179] [37666 15571]",
				"[1 2] [0 1] [1 1] [2 1] [12 5], [179 74], [15571 6437], ...",
				"satisfying the recurrence M(n) = M(n-1) M(n-3)^(-1) M(n-1) (note that the Fibonacci numbers satisfy the additive version of this recurrence). - _James Propp_, Jan 27 2005",
				"Define b(1) = b(2) = b(3) = 1; b(n) = (b(n-1) + b(n-2))^2/b(n-3); then a(n) = sqrt(b(n)). - _Benoit Cloitre_, Jul 28 2002",
				"Any 3 successive terms of the sequence satisfy the Markov equation x^2 + y^2 + z^2 = 3 xyz. Therefore from the 3rd term on this is a subsequence of the Markov numbers, A002559. Also, we conjecture that the limit of log(log(a(n)))/n is log((sqrt(5) + 1)/2). - Martin Giese (martin.giese(AT)oeaw.ac.at), Oct 13 2005",
				"A subsequence of the Markoff numbers A002559. - _Andrew Hone_, Jan 16 2006",
				"The recursion exhibits the Laurent phenomenon. Let F(n) = Fibonacci(n), e(n) = F(n) - 1, a(1) = a1, a(2) = a2, a(3) = a3, a(n) = (a(n-1)^2 + a(n-3)^2) / a(n-3), b(n) = a(n) * a1^e(n-1) * a2^e(n-2) * a3^e(n-3). Then b(n) for n \u003e 1 is an irreducible polynomial in {a1^2, a2^2, a3^2}, b(n) = (b(n-1)^2 + (b(n-2) * a1^F(n-4) * a2^F(n-5) * a3^F(n-6))^2) / b(n-3), and a(n) = a(n-1) * a(n-2) * (a1^2 + a2^2 + a3^2) / (a1 * a2 * a3) - a(n-3). - _Michael Somos_, Jan 12 2013"
			],
			"link": [
				"Harry J. Smith, \u003ca href=\"/A064098/b064098.txt\"\u003eTable of n, a(n) for n = 1..18\u003c/a\u003e",
				"S. Fomin and A. Zelevinsky, \u003ca href=\"http://dx.doi.org/10.1006/aama.2001.0770\"\u003eThe Laurent Phenomenon\u003c/a\u003e, Advances in Applied Mathematics, 28 (2002), 119-144.",
				"Andrew N. W. Hone, \u003ca href=\"https://arxiv.org/abs/math/0601324\"\u003eDiophantine non-integrability of a third order recurrence with the Laurent property\u003c/a\u003e, arXiv:math/0601324 [math.NT], 2006.",
				"Andrew N. W. Hone, \u003ca href=\"https://doi.org/10.1088/0305-4470/39/12/L01\"\u003eDiophantine non-integrability of a third order recurrence with the Laurent property\u003c/a\u003e, J. Phys. A: Math. Gen. 39 (2006), L171-L177.",
				"Andrew N. W. Hone, \u003ca href=\"https://arxiv.org/abs/2109.08217\"\u003eGrowth of Mahler measure and algebraic entropy of dynamics with the Laurent property\u003c/a\u003e, arXiv:2109.08217 [math.NT], 2021.",
				"KöMaL-Mathematical and Physical Journal for Secondary Schools, \u003ca href=\"https://www.komal.hu/verseny/2001-04/mat.e.shtml\"\u003eNew advanced problems: proposed problem A265\u003c/a\u003e, April 2001.",
				"L. J. Mordell, \u003ca href=\"http://jlms.oxfordjournals.org/content/s1-28/4/500.extract\"\u003eOn the integer solutions of the equation x^2+y^2+z^2+2xyz=n\u003c/a\u003e, J. Lond. Math. Soc. 28 (1953), 500-510.",
				"J. Propp, \u003ca href=\"http://arxiv.org/abs/math/0511633\"\u003eThe combinatorics of frieze patterns and Markoff numbers\u003c/a\u003e, arXiv:math/0511633 [math.CO], 2005-2008.",
				"Matthew Christopher Russell, \u003ca href=\"https://doi.org/10.7282/T3MC926D\"\u003eUsing experimental mathematics to conjecture and prove theorems in the theory of partitions and commutative and non-commutative recurrences\u003c/a\u003e, PhD Dissertation, Mathematics Department, Rutgers University, May 2016."
			],
			"formula": [
				"Conjecture: lim_{n -\u003e infinity} log(log(a(n)))/n exists = 0.48.... - _Benoit Cloitre_, Aug 07 2002. This is true - see below.",
				"For this subsequence of the Markoff numbers, we have 2^(F(n-1) - 1) \u003c a(n) \u003c 3^(F(n-1) - 1) for n \u003e 4, where F(n) are the Fibonacci numbers with F(0)=0, F(1)=1, F(n+1) = F(n) + F(n-1). Hence log(log(a(n)))/n tends to log((1 + sqrt(5))/2) as previously conjectured. - _Andrew Hone_, Jan 16 2006",
				"a(n) = 3 * a(n-1) * a(n-2) - a(n-3). a(4-n) = a(n) for all n in Z. - _Michael Somos_, Jan 12 2013",
				"a(n) ~ 1/3 * c^(((1 + sqrt(5))/2)^n), where c = 1.2807717799265504005186306582930649245... . - _Vaclav Kotesovec_, May 06 2015"
			],
			"example": [
				"G.f. = x + x^2 + x^3 + 2*x^4 + 5*x^5 + 29*x^6 + 433*x^7 + 37666*x^8 + ..."
			],
			"maple": [
				"f:=proc(n) option remember; global K; local i;",
				"if n \u003c= K then 1",
				"else add(f(n-i)^2,i=1..K-1)/f(n-K); fi; end;",
				"K:=3;",
				"[seq(f(n),n=1..10)]; # _N. J. A. Sloane_, Mar 17 2017"
			],
			"mathematica": [
				"a[n_] := (a[n - 1]^2 + a[n - 2]^2)/a[n - 3]; a[1] = a[2] = a[3] = 1; Array[a, 13] (* Or *)",
				"a[n_] := 3 a[n - 1]*a[n - 2] - a[n - 3]; a[1] = a[2] = a[3] = 1; Array[a, 13] (* _Robert G. Wilson v_, Dec 26 2012 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, n = 4-n); if( n\u003c4, 1, 3 * a(n-1) * a(n-2) - a(n-3))}; /* _Michael Somos_, Jan 12 2013 */",
				"(PARI) { a=a3=a2=a1=1; for (n = 1, 18, if (n\u003e3, a=(a1^2 + a2^2)/a3; a3=a2; a2=a1; a1=a); write(\"b064098.txt\", n, \" \", a) ) } /* _Harry J. Smith_, Sep 06 2009 */"
			],
			"xref": [
				"Cf. A002559, A072878, A072879, A072880."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Santi Spadaro_, Sep 16 2001",
			"ext": [
				"Entry improved by comments from _Michael Somos_, Sep 25 2001"
			],
			"references": 9,
			"revision": 75,
			"time": "2021-09-20T11:42:02-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}