{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A143953",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 143953,
			"data": "1,1,1,1,1,3,1,1,8,4,1,1,21,14,5,1,1,55,48,21,6,1,1,144,162,85,29,7,1,1,377,537,335,133,38,8,1,1,987,1748,1286,589,193,48,9,1,1,2584,5594,4815,2526,940,266,59,10,1,1,6765,17629,17619,10518,4413,1405,353,71,11,1",
			"name": "Triangle read by rows: T(n,k) is the number of Dyck paths of semilength n having k peaks in their peak plateaux (0\u003c=k\u003c=n-1). A peak plateau is a run of consecutive peaks that is preceded by an upstep and followed by a down step; a peak consists of an upstep followed by a downstep.",
			"comment": [
				"Row n has n terms (n\u003e=1).",
				"Row sums are the Catalan numbers (A000108).",
				"T(n,1)=A001906(n-1)=Fibonacci(2n-2).",
				"Sum(k*T(n,k),k=0..n-1))=A143954(n).",
				"For the statistic \"number of peak plateaux\", see A143952."
			],
			"formula": [
				"The g.f. G=G(t,z) satisfies z(1-z)(1-tz)G^2-(1-z+z^2-tz)G+(1-z)(1-tz) = 0 (for the explicit form of G see the Maple program).",
				"The trivariate g.f. g=g(x,y,z) of Dyck paths with respect to number of peak plateaux, number of peaks in the peak plateaux and semilength, marked, by x, y and z, respectively satisfies g=1+zg[g+xyz/(1-yz)-z/(1-z)]."
			],
			"example": [
				"T(4,2)=4 because we have UDU(UDUD)D, U(UDUD)DUD, U(UD)DU(UD)D and UU(UDUD)DD (the peaks in the peak plateaux are shown between parentheses). The triangle starts:",
				"1;",
				"1;",
				"1,1;",
				"1,3,1;",
				"1,8,4,1;",
				"1,21,14,5,1;",
				"1,55,48,21,6,1;"
			],
			"maple": [
				"C:=proc(z) options operator, arrow: (1/2-(1/2)*sqrt(1-4*z))/z end proc: G:=(1-z)*(1-t*z)*C(z*(1-z)^2*(1-t*z)^2/(1-z+z^2-t*z)^2)/(1-z+z^2-t*z): Gser:= simplify(series(G,z=0,15)): for n from 0 to 11 do P[n]:=sort(coeff(Gser,z,n)) end do: 1; for n to 11 do seq(coeff(P[n],t,j),j=0..n-1) end do; # yields sequence in triangular form"
			],
			"xref": [
				"Cf. A000045, A000108, A001906, A143952, A143954."
			],
			"keyword": "nonn,tabf",
			"offset": "0,6",
			"author": "_Emeric Deutsch_, Oct 10 2008",
			"references": 2,
			"revision": 7,
			"time": "2017-06-11T08:35:44-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}