{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A122197",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 122197,
			"data": "1,1,1,2,1,2,1,2,3,1,2,3,1,2,3,4,1,2,3,4,1,2,3,4,5,1,2,3,4,5,1,2,3,4,5,6,1,2,3,4,5,6,1,2,3,4,5,6,7,1,2,3,4,5,6,7,1,2,3,4,5,6,7,8,1,2,3,4,5,6,7,8,1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9,10,1,2,3,4,5",
			"name": "Fractal sequence: count up to successive integers twice.",
			"comment": [
				"Fractal - deleting the first occurrence of each integer leaves the original sequence. Also, deleting the all 1's leaves the original sequence plus 1. New values occur at square indices. 1's occur at indices m^2+1 and m^2+m+1. Ordinal transform of A122196.",
				"Except for its initial 1, A122197 is the natural fractal sequence of A002620; that is, A122197(n+1) is the number of the row of A194061 that contains n.  See A194029 for definition of natural fractal sequence.  - _Clark Kimberling_, Aug 12 2011",
				"From _Johannes W. Meijer_, Sep 09 2013: (Start)",
				"Triangle read by rows formed from antidiagonals of triangle A002260.",
				"The row sums equal A008805(n-1) and the antidiagonal sums equal A211534(n+5). (End)"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A122197/b122197.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"From _Boris Putievskiy_, Sep 09 2013: (Start)",
				"a(n) = (A001477(n-1) mod A000194(n-1)) + 1 for n\u003e= 2 with a(1) = 1.",
				"a(n) = ((n - 1) mod (t+1)) + 1, where t = floor((sqrt(4*n-3)-1)/2). -",
				"From _Johannes W. Meijer_, Sep 09 2013: (Start)",
				"T(n, k) = k for n \u003e= 1 and 1 \u003c= k \u003c= (n+1)/2; T(n, k) = 0 elsewhere.",
				"T(n, k) = A002260(n-k, k) (End)",
				"a(n) = n - floor(sqrt(n) + 1/2)*floor(sqrt(n-1)). - _Ridouane Oudra_, Jun 08 2020",
				"a(n) = A339399(2n-1). - _Wesley Ivan Hurt_, Jan 09 2022"
			],
			"example": [
				"The first few rows of the sequence a(n) as a triangle T(n, k):",
				"n/k  1   2   3",
				"1    1",
				"2    1",
				"3    1,  2",
				"4    1,  2",
				"5    1,  2,  3",
				"6    1,  2,  3"
			],
			"maple": [
				"From _Johannes W. Meijer_, Sep 09 2013: (Start)",
				"a := proc(n) local t: t := floor((sqrt(4*n-3)-1)/2): (n-1) mod (t+1) + 1 end: seq(a(n), n=1..105); # End first program",
				"T := proc(n, k): if n \u003c 1 then return(0) elif k \u003c 1 or k\u003e floor((n+1)/2) then return(0) else k fi: end: seq(seq(T(n, k), k=1..floor((n+1)/2)), n=1..19); # End second program. (End)"
			],
			"mathematica": [
				"With[{c=Table[Range[n],{n,10}]},Flatten[Riffle[c,c]]] (* _Harvey P. Dale_, Apr 19 2013 *)"
			],
			"program": [
				"(Haskell)",
				"import Data.List (transpose, genericIndex)",
				"a122197 n k = genericIndex (a122197_row n) (k - 1)",
				"a122197_row n = genericIndex a122197_tabf (n - 1)",
				"a122197_tabf = concat $ transpose [a002260_tabl, a002260_tabl]",
				"a122197_list = concat a122197_tabf",
				"-- _Reinhard Zumkeller_, Aug 07 2015, Jul 19 2012",
				"(PARI) a(n)=n - (sqrtint(4*n) + 1)\\2*sqrtint(n-1) \\\\ _Charles R Greathouse IV_, Jun 08 2020"
			],
			"xref": [
				"Cf. A122196, A000290, A033638, A002260, A001477, A000194, A339399."
			],
			"keyword": "easy,nonn,tabf,changed",
			"offset": "1,4",
			"author": "_Franklin T. Adams-Watters_, Aug 25 2006",
			"references": 12,
			"revision": 28,
			"time": "2022-01-09T00:19:47-05:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}