{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A271608",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 271608,
			"data": "1,2,1,2,1,1,2,1,2,1,3,3,3,3,3,3,4,2,6,4,2,1,1,8,4,5,2,2,7,10,9,2,3,4,5,6,6,5,2,7,11,11,4,1,5,8,13,8,6,5,3,8,8,12,7,3,8,18,16,12,2,7,10,15,11,10,4,4,11,15,22",
			"name": "Number of ordered ways to write n as pen(u) + pen(v) + pen(x) + pen(y) + pen(z) with u,v,x,y,z nonnegative integers such that u + 2*v + 4*x + 5*y + 6*z is a pentagonal number, where pen(k) denotes the pentagonal number k*(3k-1)/2.",
			"comment": [
				"Conjecture: (i) a(n) \u003e 0 for all n = 0,1,2,..., and a(n) = 1 only for n = 0, 2, 4, 5, 7, 9, 21, 22, 43. Also, every n = 0,1,2,... can be written as pen(u) + pen(v) + pen(x) + pen(y) + pen(z) with u,v,x,y,z nonnegative integers such that 3*u + 5*v + 11*x + 16*y + 19*z is also a pentagonal number.",
				"(ii) Any integer n \u003e 43 can be written as the sum of five pentagonal numbers u, v, x, y and z such that u + 2*v + 5*x + 7*y + 10*z is also a pentagonal number. Also, each integer n \u003e 10 can be written as the sum of five pentagonal numbers u, v, x, y and z such that u + 2*v + 5*x + 7*y + 10*z is a square.",
				"(iii) Any natural number n can be written as u^2 + v^2 + x^2 + y^2 + z^2 with u^2 + 2*v^2 + 3*x^2 + 4*y^2 + 5*z^2 a square, where u, v, x, y and z are integers.",
				"As conjectured by Fermat and proved by Cauchy, each natural number can be written as the sum of five pentagonal numbers.",
				"See also A271510, A271513, A271518 and A271644 for some similar conjectures refining Lagrange's four-square theorem."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A271608/b271608.txt\"\u003eTable of n, a(n) for n = 0..1500\u003c/a\u003e",
				"Z.-W. Sun, \u003ca href=\"http://math.scichina.com:8081/sciAe/EN/abstract/abstract517007.shtml\"\u003eOn universal sums of polygonal numbers\u003c/a\u003e, Sci. China Math. 58(2015), no. 7, 1367-1396."
			],
			"example": [
				"a(7) = 1 since 7 = 5 + 0 + 1 + 0 + 1 = pen(2) + pen(0) + pen(1) + pen(0) + pen(1) with 2 + 2*0 + 4*1 + 5*0 + 6*1 = 12 = pen(3).",
				"a(9) = 1 since 9 = 1 + 1 + 5 + 1 + 1 = pen(1) + pen(1) + pen(2) + pen(1) + pen(1) with 1 + 2*1 + 4*2 + 5*1 + 6*1 = 22 = pen(4).",
				"a(22) = 1 since 22 = 0 + 0 + 5 + 12 + 5 = pen(0) + pen(0) + pen(2) + pen(3) + pen(2) with 0 + 2*0 + 4*2 + 5*3 + 6*2 = 35 = pen(5).",
				"a(43) = 1 since 43 = 5 + 1 + 35 + 1 + 1 = pen(2) + pen(1) + pen(5) + pen(1) + pen(1) with 2 + 2*1 + 4*5 + 5*1 + 6*1 = 35 = pen(5)."
			],
			"mathematica": [
				"SQ[n_]:=SQ[n]=IntegerQ[Sqrt[n]]",
				"pen[x_]:=pen[x]=x*(3x-1)/2",
				"pQ[n_]:=pQ[n]=SQ[24n+1]\u0026\u0026(n==0||Mod[Sqrt[24n+1]+1,6]==0)",
				"Do[r=0;Do[If[pQ[n-pen[x]-pen[y]-pen[z]-pen[w]]\u0026\u0026pQ[x+2y+4z+5w+6*Floor[(Sqrt[24(n-pen[x]-pen[y]-pen[z]-pen[w])+1]+1)/6]],r=r+1],{x,0,(Sqrt[24n+1]+1)/6},{y,0,(Sqrt[24(n-pen[x])+1]+1)/6},{z,0,(Sqrt[24(n-pen[x]-pen[y])+1]+1)/6},{w,0,(Sqrt[24(n-pen[x]-pen[y]-pen[z])+1]+1)/6}];Print[n,\" \",r];Label[aa];Continue,{n,0,70}]"
			],
			"xref": [
				"Cf. A000290, A000326, A271510, A271513, A271518, A271644."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Zhi-Wei Sun_, Apr 10 2016",
			"references": 40,
			"revision": 10,
			"time": "2016-04-11T21:16:45-04:00",
			"created": "2016-04-11T21:16:45-04:00"
		}
	]
}