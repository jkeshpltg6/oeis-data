{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A104584",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 104584,
			"data": "0,1,7,12,26,35,57,70,100,117,155,176,222,247,301,330,392,425,495,532,610,651,737,782,876,925,1027,1080,1190,1247,1365,1426,1552,1617,1751,1820,1962,2035,2185,2262,2420",
			"name": "a(n) = (1/2) * ( 3*n^2 + n*(-1)^n ).",
			"comment": [
				"Previous name was: Pentagonal wave sequence of the first kind.",
				"Odd-indexed terms = A033570, pentagonal numbers with odd index (1, 12, 35, 70, ...). Even-indexed terms = A049453, 2nd pentagonal numbers with even index (0, 7, 26, 57, 100, ...).",
				"Companion sequence A104585 (Pentagonal wave sequence of the second kind), switches odd with even applications and vice versa. The pentagonal wave sequence triangle A104586 has A104584 in odd columns and A104585 in even columns.",
				"Exponents of q in the identity Sum_{n \u003e= 0} ( q^n*Product_{k = 1..n} (1 - q^(4*k-3)) ) = 1 + q - q^7 - q^12 + q^26 + q^35 - - + + .... Compare with Euler's pentagonal number theorem: Product_{n \u003e= 1} (1 - q^n) = 1 - Sum_{n \u003e= 1} ( q^n*Product_{k = 1..n-1} (1 - q^k) ) = 1 - q - q^2 + q^5 + q^7 - q^12 - q^15  + + - - .... - _Peter Bala_, Dec 03 2020"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A104584/b104584.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,2,-2,-1,1).",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Pentagonal_number_theorem\"\u003ePentagonal number theorem\u003c/a\u003e"
			],
			"formula": [
				"a(n) = a(n-1) + 2*a(n-2) - 2*a(n-3) - a(n-4) + a(n-5). - _Vincenzo Librandi_, Apr 04 2013",
				"a(n) = (1/2) * (3*n^2 + n*(-1)^n ). - _Ralf Stephan_, May 20 2007",
				"G.f. -x*(1+6*x+3*x^2+2*x^3) / ( (1+x)^2*(x-1)^3 ). - _R. J. Mathar_, Jan 10 2011"
			],
			"example": [
				"a(5) = 35 = A000326(5).",
				"a(6) = 57 = A005449(6)."
			],
			"mathematica": [
				"Table[(1/2) (3 n^2 + n (-1)^n), {n, 0, 100}] (* _Vincenzo Librandi_, Apr 04 2013 *)"
			],
			"program": [
				"(MAGMA) I:=[0, 1, 7, 12, 26]; [n le 5 select I[n] else Self(n-1)+2*Self(n-2)-2*Self(n-3)-Self(n-4)+Self(n-5): n in [1..50]]; // _Vincenzo Librandi_, Apr 04 2013"
			],
			"xref": [
				"Cf. A000326, A005449, A049453, A033568, A104585, A104586."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Gary W. Adamson_, Mar 17 2005",
			"ext": [
				"Better name, using formula from _Ralf Stephan_, _Joerg Arndt_, Sep 17 2013"
			],
			"references": 3,
			"revision": 34,
			"time": "2020-12-05T11:24:13-05:00",
			"created": "2005-04-09T03:00:00-04:00"
		}
	]
}