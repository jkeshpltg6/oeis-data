{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A141419",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 141419,
			"data": "1,2,3,3,5,6,4,7,9,10,5,9,12,14,15,6,11,15,18,20,21,7,13,18,22,25,27,28,8,15,21,26,30,33,35,36,9,17,24,30,35,39,42,44,45,10,19,27,34,40,45,49,52,54,55",
			"name": "Triangle read by rows: T(n, k) = A000217(n) - A000217(n - k) with 1 \u003c= k \u003c= n.",
			"comment": [
				"As a rectangle, the accumulation array of A051340.",
				"From _Clark Kimberling_, Feb 05 2011: (Start)",
				"Here all the weights are divided by two where they aren't in Cahn.",
				"As a rectangle, A141419 is in the accumulation chain",
				"... \u003c A051340 \u003c A141419 \u003c A185874 \u003c A185875 \u003c A185876 \u003c ...",
				"(See A144112 for the definition of accumulation array.)",
				"row 1: A000027",
				"col 1: A000217",
				"diag (1,5,...): A000326 (pentagonal numbers)",
				"diag (2,7,...): A005449 (second pentagonal numbers)",
				"diag (3,9,...): A045943 (triangular matchstick numbers)",
				"diag (4,11,...): A115067",
				"diag (5,13,...): A140090",
				"diag (6,15,...): A140091",
				"diag (7,17,...): A059845",
				"diag (8,19,...): A140672",
				"(End)",
				"Let N=2*n+1 and k=1,2,...,n. Let A_{N,n-1} = [0,...,0,1; 0,...,0,1,1; ...; 0,1,...,1; 1,...,1], an n X n unit-primitive matrix (see [Jeffery]). Let M_n=[A_{N,n-1}]^4. Then t(n,k)=[M_n]_(1,k), that is, the n-th row of the triangle is given by the first row of M_n. - _L. Edson Jeffery_, Nov 20 2011",
				"Conjecture. Let N=2*n+1 and k=1,...,n. Let A_{N,0}, A_{N,1}, ..., A_{N,n-1} be the n X n unit-primitive matrices (again see [Jeffery]) associated with N, and define the Chebyshev polynomials of the second kind by the recurrence U_0(x) = 1, U_1(x) = 2*x and U_r(x) = 2*x*U_(r-1)(x) - U_(r-2)(x)  (r\u003e1). Define the column vectors V_(k-1) = (U_(k-1)(cos(Pi/N)), U_(k-1)(cos(3*Pi/N)), ..., U_(k-1)(cos((2*n-1)*Pi/N)))^T, where T denotes matrix transpose. Let S_N = [V_0, V_1, ..., V_(n-1)] be the n X n matrix formed by taking V_(k-1) as column k-1. Let X_N = [S_N]^T*S_N, and let [X_N]_(i,j) denote the entry in row i and column j of X_N, i,j in {0,...,n-1}. Then t(n,k) = [X_N]_(k-1,k-1), and row n of the triangle is given by the main diagonal entries of X_N. Remarks: Hence t(n,k) is the sum of squares t(n,k) = sum[m=1,...,n (U_(k-1)(cos((2*m-1)*Pi/N)))^2]. Finally, this sequence is related to A057059, since X_N = [sum_{m=1,...,n} A057059(n,m)*A_{N,m-1}] is also an integral linear combination of unit-primitive matrices from the N-th set. - _L. Edson Jeffery_, Jan 20 2012",
				"Row sums: n*(n+1)*(2*n+1)/6. - _L. Edson Jeffery_, Jan 25 2013",
				"n-th row = partial sums of n-th row of A004736. - _Reinhard Zumkeller_, Aug 04 2014",
				"T(n,k) is the number of distinct sums made by at most k elements in {1, 2, ... n}, for 1 \u003c= k \u003c= n, e.g., T(6,2) = the number of distinct sums made by at most 2 elements in {1,2,3,4,5,6}. The sums range from 1, to 5+6=11. So there are 11 distinct sums. - _Derek Orr_, Nov 26 2014",
				"A number n occurs in this sequence A001227(n) times, the number of odd divisors of n, see A209260. - _Hartmut F. W. Hoft_, Apr 14 2016",
				"Conjecture: 2*n + 1 is composite if and only if gcd(t(n,m),m) != 1, for some m. - _L. Edson Jeffery_, Jan 30 2018",
				"From _Peter Munn_, Aug 21 2019 in respect of the sequence read as a triangle: (Start)",
				"A number m can be found in column k if and only if A286013(m, k) is nonzero, in which case m occurs in column k on row A286013(m, k).",
				"The first occurrence of m is in row A212652(m) column A109814(m), which is the rightmost column in which m occurs. This occurrence determines where m appears in A209260. The last occurrence of m is in row m column 1.",
				"Viewed as a sequence of rows, consider the subsequences (of rows) that contain every positive integer. The lexicographically latest of these subsequences consists of the rows with row numbers in A270877; this is the only one that contains its own row numbers only once.",
				"(End)"
			],
			"reference": [
				"R. N. Cahn, Semi-Simple Lie Algebras and Their Representations, Dover, NY, 2006, ISBN 0-486-44999-8, p. 139."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A141419/b141419.txt\"\u003eRows n = 1..125 of triangle, flattened\u003c/a\u003e",
				"Johann Cigler, \u003ca href=\"https://arxiv.org/abs/1611.05252\"\u003eSome elementary observations on Narayana polynomials and related topics\u003c/a\u003e, arXiv:1611.05252 [math.CO], 2016. See p. 24.",
				"Carlton Gamer, David W. Roeder, and John J. Watkins, \u003ca href=\"https://www.jstor.org/stable/2689901\"\u003eTrapezoidal Numbers\u003c/a\u003e, Mathematics Magazine 58:2 (1985), pp. 108-110.",
				"L. E. Jeffery, \u003ca href=\"/wiki/User:L._Edson_Jeffery/Unit-Primitive_Matrices\"\u003eUnit-primitive matrices\u003c/a\u003e",
				"M. A. Nyblom, \u003ca href=\"https://www.fq.math.ca/Scanned/39-3/nyblom.pdf\"\u003eOn the representation of the integers as a difference of nonconsecutive triangular numbers\u003c/a\u003e, Fibonacci Quarterly 39:3 (2001), pp. 256-263."
			],
			"formula": [
				"t(n,m) = m*(2*n - m + 1)/2.",
				"t(n,m) = A000217(n) - A000217(n-m). - _L. Edson Jeffery_, Jan 16 2013",
				"Let v = d*h with h odd be an integer factorization, then v = t(d+(h-1)/2, h) if h+1 \u003c= 2*d, and v = t(d+(h-1)/2, 2*d) if h+1 \u003e 2*d; see A209260. - _Hartmut F. W. Hoft_, Apr 14 2016",
				"G.f.: y*(-x + y)/((-1 + x)^2*(-1 + y)^3). - _Stefano Spezia_, Oct 14 2018",
				"T(n, 2) = A060747(n) for n \u003e 1. T(n, 3) = A008585(n - 1) for n \u003e 2. T(n, 4) = A016825(n - 2) for n \u003e 3. T(n, 5) = A008587(n - 2) for n \u003e 4. T(n, 6) = A016945(n - 3) for n \u003e 5. T(n, 7) = A008589(n - 3) for n \u003e 6. T(n, 8) = A017113(n - 4) for n \u003e 7.r n \u003e 5. T(n, 7) = A008589(n - 3) for n \u003e 6. T(n, 8) = A017113(n - 4) for n \u003e 7. T(n, 9) = A008591(n - 4) for n \u003e 8. T(n, 10) = A017329(n - 5) for n \u003e 9. T(n, 11) = A008593(n - 5) for n \u003e 10.  T(n, 12) = A017593(n - 6) for n \u003e 11. T(n, 13) = A008595(n - 6) for n \u003e 12. T(n, 14) = A147587(n - 7) for n \u003e 13. T(n, 15) = A008597(n - 7) for n \u003e 14. T(n, 16) = A051062(n - 8) for n \u003e 15. T(n, 17) = A008599(n - 8) for n \u003e 16. - _Stefano Spezia_, Oct 14 2018",
				"T(2*n-k, k) = A070543(n, k). - _Peter Munn_, Aug 21 2019"
			],
			"example": [
				"As a triangle:",
				"   1,",
				"   2,  3,",
				"   3,  5,  6,",
				"   4,  7,  9, 10,",
				"   5,  9, 12, 14, 15,",
				"   6, 11, 15, 18, 20, 21,",
				"   7, 13, 18, 22, 25, 27, 28,",
				"   8, 15, 21, 26, 30, 33, 35, 36,",
				"   9, 17, 24, 30, 35, 39, 42, 44, 45,",
				"  10, 19, 27, 34, 40, 45, 49, 52, 54, 55;",
				"As a rectangle:",
				"   1   2   3   4   5   6   7   8   9  10",
				"   3   5   7   9  11  13  15  17  19  21",
				"   6   9  12  15  18  21  24  27  30  33",
				"  10  14  18  22  26  30  34  38  42  46",
				"  15  20  25  30  35  40  45  50  55  60",
				"  21  27  33  39  45  51  57  63  69  75",
				"  28  35  42  49  56  63  70  77  84  91",
				"  36  44  52  60  68  76  84  92 100 108",
				"  45  54  63  72  81  90  99 108 117 126",
				"  55  65  75  85  95 105 115 125 135 145",
				"Since the odd divisors of 15 are 1, 3, 5 and 15, number 15 appears four times in the triangle at t(3+(5-1)/2, 5) in column 5 since 5+1 \u003c= 2*3, t(5+(3-1)/2, 3), t(1+(15-1)/2, 2*1) in column 2 since 15+1 \u003e 2*1, and t(15+(1-1)/2, 1). - _Hartmut F. W. Hoft_, Apr 14 2016"
			],
			"maple": [
				"a:=(n,k)-\u003ek*n-binomial(k,2): seq(seq(a(n,k),k=1..n),n=1..12); # _Muniru A Asiru_, Oct 14 2018"
			],
			"mathematica": [
				"T[n_, m_] = m*(2*n - m + 1)/2; a = Table[Table[T[n, m], {m, 1, n}], {n, 1, 10}]; Flatten[a]"
			],
			"program": [
				"(Haskell)",
				"a141419 n k =  k * (2 * n - k + 1) `div` 2",
				"a141419_row n = a141419_tabl !! (n-1)",
				"a141419_tabl = map (scanl1 (+)) a004736_tabl",
				"-- _Reinhard Zumkeller_, Aug 04 2014"
			],
			"xref": [
				"Cf. A000330 (row sums), A004736, A057059, A070543.",
				"A144112, A051340, A141419, A185874, A185875, A185876 are accumulation chain related.",
				"A141418 is a variant.",
				"Cf. A001227, A209260. - _Hartmut F. W. Hoft_, Apr 14 2016",
				"A109814, A212652, A270877, A286013 relate to where each natural number appears in this sequence.",
				"A000027, A000217, A000326, A005449, A045943, A059845, A115067, A140090, A140091, A140672 are rows, columns or diagonals - refer to comments."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "1,2",
			"author": "_Roger L. Bagula_, Aug 05 2008",
			"ext": [
				"Simpler name by _Stefano Spezia_, Oct 14 2018"
			],
			"references": 16,
			"revision": 96,
			"time": "2019-10-02T21:30:41-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}