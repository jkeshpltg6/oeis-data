{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000590",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 590,
			"id": "M4908 N2104",
			"data": "1,13,104,663,3705,19019,92092,427570,1924065,8454225,36463440,154969620,650872404,2707475148,11173706960,45812198536,186803188858,758201178306,3065415516592,12352414499425,49634247352235,198954083924505,795816335698020,3177498557750790",
			"name": "a(n) = 13*binomial(2n,n-6)/(n+7).",
			"comment": [
				"Number of lattice paths from (0,0) to (n,n) with steps E=(1,0) and N=(0,1) which touch but do not cross the line x-y=6. - _Herbert Kociemba_, May 24 2004",
				"Number of standard tableaux of shape (n+6,n-6). - _Emeric Deutsch_, May 30 2004",
				"a(n) = A214292(2*n-1,n-7) for n \u003e 6. - _Reinhard Zumkeller_, Jul 12 2012"
			],
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A000590/b000590.txt\"\u003eTable of n, a(n) for n = 6..200\u003c/a\u003e",
				"R. K. Guy, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL3/GUY/catwalks.html\"\u003eCatwalks, Sandsteps and Pascal Pyramids\u003c/a\u003e, J. Integer Seqs., Vol. 3 (2000), #00.1.6.",
				"A. Papoulis, \u003ca href=\"http://www.jstor.org/stable/43636019\"\u003eA new method of inversion of the Laplace transform\u003c/a\u003e, Quart. Applied Math. 14 (1956), 405ff.",
				"A. Papoulis, \u003ca href=\"/A000108/a000108_8.pdf\"\u003eA new method of inversion of the Laplace transform\u003c/a\u003e, Quart. Appl. Math 14 (1957), 405-414. [Annotated scan of selected pages]",
				"J. Riordan, \u003ca href=\"https://doi.org/10.1090/S0025-5718-1975-0366686-9\"\u003eThe distribution of crossings of chords joining pairs of 2n points on a circle\u003c/a\u003e, Math. Comp., 29 (1975), 215-222."
			],
			"formula": [
				"G.f.: x^6*C(x)^13, where C(x)=[1-sqrt(1-4x)]/(2x) is g.f. for the Catalan numbers (A000108). - _Emeric Deutsch_, May 30 2004",
				"Let A be the Toeplitz matrix of order n defined by: A[i,i-1]=-1, A[i,j]=Catalan(j-i), (i\u003c=j), and A[i,j]=0, otherwise. Then, for n\u003e=12, a(n-6)=(-1)^(n-12)*coeff(charpoly(A,x),x^12). [_Milan Janjic_, Jul 08 2010]",
				"-(n+7)*(n-6)*a(n) +2*n*(2*n-1)*a(n-1)=0. - _R. J. Mathar_, Jun 20 2013"
			],
			"program": [
				"(PARI) a(n) = 13*binomial(2*n,n-6)/(n+7); \\\\ _Michel Marcus_, Oct 16 2017"
			],
			"keyword": "nonn",
			"offset": "6,2",
			"author": "_N. J. A. Sloane_.",
			"references": 7,
			"revision": 36,
			"time": "2017-10-16T12:17:51-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}