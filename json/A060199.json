{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A060199",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 60199,
			"data": "0,4,5,9,12,17,21,29,32,39,49,52,58,73,76,88,92,109,117,125,140,151,159,176,188,199,207,233,247,254,267,284,305,320,346,338,373,385,416,418,437,458,481,504,517,551,555,583,599,636,648,678,686,733,723,753,810",
			"name": "Number of primes between n^3 and (n+1)^3.",
			"comment": [
				"Ingham showed that for n large enough and k=5/8, prime(n+1)-prime(n) = O(prime(n)^k). Ingham's result implies that there is a prime between sufficiently large consecutive cubes. Therefore a(n) is nonzero for n sufficiently large. Using the Riemann Hypothesis, Caldwell and Cheng prove there is a prime between all consecutive cubes. The question is undecided for squares. Many authors have reduced the value of k. The best value of k is 21/40, proved by Baker, Harman and Pintz in 2001. - corrected by _Jonathan Sondow_, May 19 2013",
				"Conjecture: There are always more than 3 primes between two consecutive nonzero cubes. - _Cino Hilliard_, Jan 05 2003",
				"Dudek (2014), correcting a claim of Cheng, shows that a(n) \u003e 0 for n \u003e exp(exp(33.217)) = 3.06144... * 10^115809481360808. - _Charles R Greathouse IV_, Jun 27 2014",
				"Cully-Hugill shows the above for n \u003e exp(exp(32.892)) = 6.92619... * 10^83675518094285. - _Charles R Greathouse IV_, Aug 02 2021"
			],
			"link": [
				"Charles R Greathouse IV, \u003ca href=\"/A060199/b060199.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"R. C. Baker, G. Harman and J. Pintz, \u003ca href=\"http://www.cs.umd.edu/~gasarch/BLOGPAPERS/BakerHarmanPintz.pdf\"\u003eThe difference between consecutive primes, II\u003c/a\u003e, Proc. London Math. Soc. (3) 83 (2001), no. 3, 532-562.",
				"Chris K. Caldwell and Yuanyou Cheng, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL8/Caldwell/caldwell78.html\"\u003eDetermining Mills's Constant and a Note on Honaker's Problem\u003c/a\u003e, Journal of Integer Sequences, Vol. 8 (2005), Article 05.4.1.",
				"Y.-Y. F.-R. Cheng, \u003ca href=\"http://arxiv.org/abs/0810.2113\"\u003eExplicit Estimate on Primes between consecutive cubes\u003c/a\u003e, Rocky Mountain Journal of Mathematics 40:1 (2010), pp. 117-153. arXiv:0810.2113 [math.NT], 2008-2013.",
				"Michaela Cully-Hugill, \u003ca href=\"https://arxiv.org/abs/2107.14468\"\u003ePrimes between consecutive powers\u003c/a\u003e, arXiv:2107.14468 [math.NT]",
				"Adrian Dudek, \u003ca href=\"http://arxiv.org/abs/1401.4233\"\u003eAn explicit result for primes between cubes\u003c/a\u003e arXiv:1401.4233 [math.NT], 2014.",
				"Adrian Dudek, An explicit result for primes between cubes, Functiones et Approximatio Commentarii Mathematici Vol. 55, Issue 2 (Dec 2016), pp. 177-197. See also \u003ca href=\"https://arxiv.org/abs/1611.07251\"\u003eExplicit Estimates in the Theory of Prime Numbers\u003c/a\u003e, arXiv:1611.07251 [math.NT], 2016; PhD thesis, Australian National University, 2016.",
				"A. E. Ingham, \u003ca href=\"http://dx.doi.org/10.1093/qmath/os-8.1.255\"\u003eOn the difference between consecutive primes\u003c/a\u003e, Quart. J. Math. Oxford 8 (1937), 255-266.",
				"MacTutor, \u003ca href=\"http://www-groups.dcs.st-and.ac.uk/~history/Mathematicians/Ingham.html\"\u003eA. E. Ingham Biography\u003c/a\u003e"
			],
			"formula": [
				"Table[PrimePi[(j+1)^3]-PrimePi[j^3], {j, 1, 100}]"
			],
			"example": [
				"n = 2: there are 5 primes between 8 and 27, 11,13,17,19,23.",
				"n = 9, n+1 = 10: PrimePi(1000)-PrimePi(729) = 168-129 = a(9) = 39."
			],
			"mathematica": [
				"PrimePi[(#+1)^3]-PrimePi[#^3]\u0026/@Range[0,60] (* _Harvey P. Dale_, Feb 08 2013 *)",
				"Last[#]-First[#]\u0026/@Partition[PrimePi[Range[0,60]^3],2,1] (* _Harvey P. Dale_, Feb 02 2015 *)"
			],
			"program": [
				"(PARI) cubespr(n) = { for(x=0,n, ct=0; for(y=x^3,(x+1)^3, if(isprime(y), ct++; )); if(ct\u003e=0,print1(ct, \", \"))) } \\\\ _Cino Hilliard_, Jan 05 2003",
				"(MAGMA) [0] cat [#PrimesInInterval(n^3, (n+1)^3): n in [1..70]]; // _Vincenzo Librandi_, Feb 13 2016",
				"(Python)",
				"from sympy import primepi",
				"def a(n): return primepi((n+1)**3) - primepi(n**3)",
				"print([a(n) for n in range(57)]) # _Michael S. Branicky_, Jun 22 2021"
			],
			"xref": [
				"First differences of A038098.",
				"Cf. A000720, A014085, A014220, A061235, A062517."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Labos Elemer_, Mar 19 2001",
			"ext": [
				"Corrected and added more detail to the Ingham references. - _T. D. Noe_, Sep 23 2008",
				"Combined two comments, correcting a bad error in the first comment. - _T. D. Noe_, Sep 27 2008",
				"Edited by _N. J. A. Sloane_, Jan 17 2009 at the suggestion of _R. J. Mathar_"
			],
			"references": 15,
			"revision": 65,
			"time": "2021-08-02T16:58:36-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}