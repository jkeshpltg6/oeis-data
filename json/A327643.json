{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A327643",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 327643,
			"data": "1,1,1,3,6,24,84,498,2220,15108,92328,773580,5636460,53563476,471562512,5270698716,52117937052,637276396764,7317811499736,100453675122444,1276319138168796,19048874583061716,270233458572751440,4442429353548965628,68384217440167826412",
			"name": "Number of refinement sequences n -\u003e ... -\u003e {1}^n, where in each step one part is replaced by a partition of itself into two smaller parts (in weakly decreasing order).",
			"comment": [
				"Number of proper (n-1)-times partitions of n, cf. A327639.",
				"Might be called \"Half-Factorial numbers\" analog to the \"Half-Catalan numbers\" (A000992).",
				"The recursion formula is a special case of the formula given in A327729.",
				"a(n+1)/(n*a(n)) tends to 0.67617164... - _Vaclav Kotesovec_, Apr 28 2020"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A327643/b327643.txt\"\u003eTable of n, a(n) for n = 1..481\u003c/a\u003e",
				"Vaclav Kotesovec, \u003ca href=\"/A327643/a327643.jpg\"\u003ePlot of a(n+1)/(n*a(n)) for n = 1..10000\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Partition_(number_theory)\"\u003ePartition (number theory)\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Sum_{j=1..floor(n/2)} C(n-2,j-1) a(j)*a(n-j) for n \u003e 1, a(1) = 1.",
				"a(n) = A327639(n,n-1) = A327631(n,n-1)/n."
			],
			"example": [
				"a(1) = 1:",
				"  1",
				"a(2) = 1:",
				"  2 -\u003e 11",
				"a(3) = 1:",
				"  3 -\u003e 21 -\u003e 111",
				"a(4) = 3:",
				"  4 -\u003e 31 -\u003e 211 -\u003e 1111",
				"  4 -\u003e 22 -\u003e 112 -\u003e 1111",
				"  4 -\u003e 22 -\u003e 211 -\u003e 1111",
				"a(5) = 6:",
				"  5 -\u003e 41 -\u003e 311 -\u003e 2111 -\u003e 11111",
				"  5 -\u003e 41 -\u003e 221 -\u003e 1121 -\u003e 11111",
				"  5 -\u003e 41 -\u003e 221 -\u003e 2111 -\u003e 11111",
				"  5 -\u003e 32 -\u003e 212 -\u003e 1112 -\u003e 11111",
				"  5 -\u003e 32 -\u003e 212 -\u003e 2111 -\u003e 11111",
				"  5 -\u003e 32 -\u003e 311 -\u003e 2111 -\u003e 11111"
			],
			"maple": [
				"b:= proc(n, i, k) option remember; `if`(n=0 or k=0, 1, `if`(i\u003e1,",
				"      b(n, i-1, k), 0) +b(i$2, k-1)*b(n-i, min(n-i, i), k))",
				"    end:",
				"a:= n-\u003e add(b(n$2, i)*(-1)^(n-1-i)*binomial(n-1, i), i=0..n-1):",
				"seq(a(n), n=1..29);",
				"# second Maple program:",
				"a:= proc(n) option remember; `if`(n=1, 1,",
				"      add(a(j)*a(n-j)*binomial(n-2, j-1), j=1..n/2))",
				"    end:",
				"seq(a(n), n=1..29);"
			],
			"mathematica": [
				"a[n_] := a[n] = Sum[Binomial[n-2, j-1] a[j] a[n-j], {j, n/2}]; a[1] = 1;",
				"Array[a, 25] (* _Jean-François Alcover_, Apr 28 2020 *)"
			],
			"xref": [
				"Cf. A000142, A000992, A002846 (only one part of each size is replaceable), A327631, A327639, A327697, A327698, A327699, A327702, A327729."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Alois P. Heinz_, Sep 20 2019",
			"references": 9,
			"revision": 63,
			"time": "2020-04-28T09:32:06-04:00",
			"created": "2019-09-20T16:24:45-04:00"
		}
	]
}