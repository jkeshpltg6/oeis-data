{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A322596",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 322596,
			"data": "1,1,1,1,1,1,1,2,1,1,1,2,2,1,1,1,3,4,3,1,1,1,3,5,5,3,1,1,1,4,7,9,7,4,1,1,1,4,10,14,14,10,4,1,1,1,5,12,21,26,21,12,5,1,1,1,5,15,30,42,42,30,15,5,1,1,1,6,19,42,66,77,66,42,19,6,1,1,1,6,22,55,99,132,132,99,55,22,6,1,1",
			"name": "Square array read by descending antidiagonals (n \u003e= 0, k \u003e= 0): let b(n,k) = (n+k)!/((n+1)!*k!); then T(n,k) = b(n,k) if b(n,k) is an integer, and T(n,k) = floor(b(n,k)) + 1 otherwise.",
			"comment": [
				"For n \u003e= 1, T(n,k) is the number of nodes in n-dimensional space for Mysovskikh's cubature formula which is exact for any polynomial of degree k of n variables."
			],
			"link": [
				"Ronald Cools, \u003ca href=\"http://nines.cs.kuleuven.be/ecf/\"\u003eEncyclopaedia of Cubature Formulas\u003c/a\u003e",
				"Ivan P. Mysovskikh, \u003ca href=\"https://doi.org/10.1016/0041-5553(64)90212-5\"\u003eOn the construction of cubature formulae for very simple domains\u003c/a\u003e, USSR Computational Mathematics and Mathematical Physics, Volume 4, Issue 1, 1964, 1-17."
			],
			"example": [
				"Array begins:",
				"  1, 1, 1,  1,  1,   1,   1,    1,    1,    1, ...",
				"  1, 1, 2,  2,  3,   3,   4,    4,    5,    5, ...",
				"  1, 1, 2,  4,  5,   7,  10,   12,   15,   19, ...",
				"  1, 1, 3,  5,  9,  14,  21,   30,   42,   55, ...",
				"  1, 1, 3,  7, 14,  26,  42,   66,   99,  143, ...",
				"  1, 1, 4, 10, 21,  42,  77,  132,  215,  334, ...",
				"  1, 1, 4, 12, 30,  66, 132,  246,  429,  715, ...",
				"  1, 1, 5, 15, 42,  99, 215,  429,  805, 1430, ...",
				"  1, 1, 5, 19, 55, 143, 334,  715, 1430, 2702, ...",
				"  1, 1, 6, 22, 72, 201, 501, 1144, 2431, 4862, ...",
				"  ...",
				"As triangular array, this begins:",
				"  1;",
				"  1, 1;",
				"  1, 1,  1;",
				"  1, 2,  1,  1;",
				"  1, 2,  2,  1,  1;",
				"  1, 3,  4,  3,  1,  1;",
				"  1, 3,  5,  5,  3,  1,  1;",
				"  1, 4,  7,  9,  7,  4,  1,  1;",
				"  1, 4, 10, 14, 14, 10,  4,  1, 1;",
				"  1, 5, 12, 21, 26, 21, 12,  5, 1, 1;",
				"  1, 5, 15, 30, 42, 42, 30, 15, 5, 1, 1;",
				"  ..."
			],
			"program": [
				"(Maxima)",
				"b(n, k) := (n + k)!/((n + 1)!*k!)$",
				"T(n, k) := if integerp(b(n, k)) then b(n, k) else floor(b(n, k)) + 1$",
				"create_list(T(k, n - k), n, 0, 15, k, 0, n);"
			],
			"xref": [
				"Main diagonal: A000108.",
				"Cf. A007318, A037306, A046854, A047996, A065941, A241926, A267632."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "0,8",
			"author": "_Franck Maminirina Ramaharo_, Jan 22 2019",
			"references": 1,
			"revision": 10,
			"time": "2021-02-15T03:44:44-05:00",
			"created": "2019-01-24T05:49:42-05:00"
		}
	]
}