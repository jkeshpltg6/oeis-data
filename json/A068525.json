{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A068525",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 68525,
			"data": "4,12,60,72,240,192,2112,1152,14592,26112,15360,139968,138240,675840,2101248,737280,4866048,786432,22118400,36175872,194641920,63700992,138412032,169869312,1321205760,11123294208,16357785600,25669140480",
			"name": "Smallest k-almost prime between twin primes (for k \u003e= 2).",
			"comment": [
				"Because it is unknown whether an infinite number of twin primes exist, it is unknown whether this sequence is infinite."
			],
			"link": [
				"Donovan Johnson, \u003ca href=\"/A068525/b068525.txt\"\u003eTable of n, a(n) for n = 2..431\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/AlmostPrime.html\"\u003eAlmost Prime\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/TwinPrimes.html\"\u003eTwin Primes\u003c/a\u003e"
			],
			"example": [
				"a(6)=240 because 240=2^4*3*5 is a 6-almost prime, 239 and 241 are twin primes and there is no 6-almost prime smaller than 240 which is between a pair of twin primes."
			],
			"mathematica": [
				"f[n_] := Plus @@ Last /@ FactorInteger@n; p = 3; t = Table[0, {30}]; While[p \u003c 26*10^9, If[ PrimeQ[p + 2], a = f[p + 1]; If[ t[[a]] == 0, t[[a]] = p + 1; Print[{a, p + 1}]]]; p = NextPrime@p]; t (* _Robert G. Wilson v_, Aug 02 2010 *)"
			],
			"program": [
				"(PARI) v=vector(32) for(n=3,2250000000, if(n%1000000==0,print(n)); if(isprime(n) \u0026\u0026 isprime(n+2),k=bigomega(n+1); if(v[k]==0,v[k]=n+1; print(v[k],\", \",k)))); v",
				"\\\\ The PARI program prints a progress mark per million integers examined. v[k] is loaded with the first k-almost prime encountered between primes and is printed upon discovery. The entire vector is printed at program completion (or can be printed after interrupting the PARI program with CTRL-C)."
			],
			"xref": [
				"Cf. A001358 (semiprimes, with links to other almost primes), A001359 (lesser of twin primes), A014574, A075590."
			],
			"keyword": "nonn",
			"offset": "2,1",
			"author": "_Rick L. Shepherd_, Mar 21 2002",
			"ext": [
				"a(27) - a(29) from _Robert G. Wilson v_, Aug 02 2010"
			],
			"references": 5,
			"revision": 20,
			"time": "2017-11-08T17:01:52-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}