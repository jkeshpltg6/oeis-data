{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000560",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 560,
			"id": "M1420 N0557",
			"data": "1,2,5,12,33,87,252,703,2105,6099,18689,55639,173423,526937,1664094,5137233,16393315,51255709,164951529,521138861,1688959630,5382512216,17547919924,56335234064,184596351277,596362337295,1962723402375",
			"name": "Number of ways of folding a strip of n labeled stamps.",
			"reference": [
				"A. Sade, Sur les Chevauchements des Permutations, published by the author, Marseille, 1949.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence).",
				"M. B. Wells, Elements of Combinatorial Computing. Pergamon, Oxford, 1971, p. 238."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A000560/b000560.txt\"\u003eTable of n, a(n) for n = 2..44\u003c/a\u003e (derived from A000682)",
				"CombOS - Combinatorial Object Server, \u003ca href=\"http://combos.org/meander\"\u003eGenerate meanders and stamp foldings\u003c/a\u003e",
				"P. Di Francesco, O. Golinelli and E. Guitter, \u003ca href=\"https://arxiv.org/abs/hep-th/9607039\"\u003eMeanders: a direct enumeration approach\u003c/a\u003e, arXiv:hep-th/9607039, 1996; Nucl. Phys. B 482 [FS] (1996), 497-535.",
				"R. Dickau, \u003ca href=\"http://www.robertdickau.com/stampfolding.html\"\u003eStamp Folding\u003c/a\u003e",
				"R. Dickau, \u003ca href=\"/A000136/a000136_2.pdf\"\u003eStamp Folding\u003c/a\u003e [Cached copy, pdf format, with permission]",
				"I. Jensen, \u003ca href=\"http://www.ms.unimelb.edu.au/~iwan/\"\u003eHome page\u003c/a\u003e",
				"I. Jensen, \u003ca href=\"http://dx.doi.org/10.1088/0305-4470/33/34/301\"\u003eA transfer matrix approach to the enumeration of plane meanders\u003c/a\u003e, J. Phys. A 33, 5953-5963 (2000).",
				"I. Jensen and A. J. Guttmann, \u003ca href=\"http://dx.doi.org/10.1088/0305-4470/33/21/101\"\u003eCritical exponents of plane meanders\u003c/a\u003e J. Phys. A 33, L187-L192 (2000).",
				"J. E. Koehler, \u003ca href=\"http://dx.doi.org/10.1016/S0021-9800(68)80048-1\"\u003eFolding a strip of stamps\u003c/a\u003e, J. Combin. Theory, 5 (1968), 135-152.",
				"W. F. Lunnon, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1968-0221957-8\"\u003eA map-folding problem\u003c/a\u003e, Math. Comp. 22 (1968), 193-199.",
				"David Orden, \u003ca href=\"http://mappingignorance.org/2014/07/07/many-ways-can-fold-strip-stamps/\"\u003eIn how many ways can you fold a strip of stamps?\u003c/a\u003e, 2014.",
				"A. Panayotopoulos, P. Vlamos, \u003ca href=\"https://doi.org/10.1007/s11786-015-0234-0\"\u003ePartitioning the Meandering Curves\u003c/a\u003e, Mathematics in Computer Science (2015) p 1-10.",
				"Albert Sade, \u003ca href=\"/A000108/a000108_17.pdf\"\u003eSur les Chevauchements des Permutations\u003c/a\u003e, published by the author, Marseille, 1949. [Annotated scanned copy]",
				"J. Sawada and R. Li, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v19i2p43\"\u003eStamp foldings, semi-meanders, and open meanders: fast generation algorithms\u003c/a\u003e, Electronic Journal of Combinatorics, Volume 19 No. 2 (2012), P#43 (16 pages).",
				"J. Touchard, \u003ca href=\"http://dx.doi.org/10.4153/CJM-1950-035-6\"\u003eContributions à l'étude du problème des timbres poste\u003c/a\u003e, Canad. J. Math., 2 (1950), 385-398.",
				"M. B. Wells, \u003ca href=\"/A000170/a000170.pdf\"\u003eElements of Combinatorial Computing\u003c/a\u003e, Pergamon, Oxford, 1971. [Annotated scanned copy of pages 237-240]",
				"\u003ca href=\"/index/Fo#fold\"\u003eIndex entries for sequences obtained by enumerating foldings\u003c/a\u003e"
			],
			"formula": [
				"a(n) = (1/2)*A000682(n+1) for n \u003e= 2.",
				"a(n) = A000136(n+1)/(2*n+2) for n \u003e= 2. - _Jean-François Alcover_, Sep 06 2019 (from formula in A000136)"
			],
			"mathematica": [
				"A000682 = Import[\"https://oeis.org/A000682/b000682.txt\", \"Table\"][[All, 2]];",
				"a[n_] := A000682[[n + 1]]/2;",
				"a /@ Range[2, 44] (* _Jean-François Alcover_, Sep 03 2019 *)",
				"A000136 = Import[\"https://oeis.org/A000136/b000136.txt\", \"Table\"][[All, 2]];",
				"a[n_] := A000136[[n + 1]]/(2 n + 2);",
				"a /@ Range[2, 44] (* _Jean-François Alcover_, Sep 06 2019 *)"
			],
			"xref": [
				"Cf. A000136, A000682, A001011."
			],
			"keyword": "nonn,nice",
			"offset": "2,2",
			"author": "_N. J. A. Sloane_, _Stéphane Legendre_",
			"ext": [
				"Computed to n = 45 by Iwan Jensen - see link in A000682."
			],
			"references": 9,
			"revision": 80,
			"time": "2019-09-06T11:08:01-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}