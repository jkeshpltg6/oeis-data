{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A243512",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 243512,
			"data": "1,2,120,4,9,14,25,8,26,42,34,20,121,27,169,16,58,39,289,48,74,114,82,52,529,94,760,133,106,68,841,32,122,186,172,93,522,70,146,217,81,63,1656,50,504,258,178,116,2209,75,194,231,202,80,2809,36,218,343,226,148,3481,130,3721,64,332,164,108000,136",
			"name": "Least index i for which A243473(i)=n, or 0 if no such index exists.",
			"comment": [
				"Motivated by the observation that some small numbers (2,12,14,18,...) occur only very late in the recently added sequence A243473, but all numbers seem to appear sooner or later. (The definition is completed by \"0 if no such index exists\" to guarantee well-definedness in absence of a proof, but I conjecture that no such 0 will ever occur.)",
				"Least i such that sigma(i)/i = (k+n)/k for some k. - _Michel Marcus_, Sep 09 2015"
			],
			"link": [
				"Charles R Greathouse IV, \u003ca href=\"/A243512/b243512.txt\"\u003eTable of n, a(n) for n = 0..629\u003c/a\u003e"
			],
			"example": [
				"For n=0, 1 satisfies sigma(1)/1 = 1/1 and 1/1 = (1+0)/1; so a(0)=1.",
				"For n=2, 2 satisfies sigma(2)/2 = 3/2 and 3/2 = (2+1)/2; so a(1)=2.",
				"For n=3, 120 satisfies sigma(120)/120 = 3/1 and 3/1 = (1+2)/1; so a(2)=120."
			],
			"mathematica": [
				"f[n_] := Block[{r = DivisorSigma[1, n]/n}, Numerator[r] - Denominator@ r]; Table[i = 1; While[f@ i != n, i++]; i, {n, 0, 67}] (* _Michael De Vlieger_, Sep 09 2015 *)"
			],
			"program": [
				"(PARI) A243473(n)=my(t=sigma(n,-1)); numerator(t)-denominator(t)",
				"v=vector(77); for(n=2,108000,t=A243473(n); if(t\u003c=#v \u0026\u0026 !v[t], v[t]=n)); concat(1,v) \\\\ _Charles R Greathouse IV_, Jun 05 2014"
			],
			"xref": [
				"Cf. A000203, A001065, A014567, A017665, A017666, A053813."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_M. F. Hasler_, Jun 05 2014",
			"ext": [
				"a(42)-a(67) from _Charles R Greathouse IV_, Jun 05 2014"
			],
			"references": 2,
			"revision": 22,
			"time": "2015-09-09T10:56:28-04:00",
			"created": "2014-06-05T23:14:09-04:00"
		}
	]
}