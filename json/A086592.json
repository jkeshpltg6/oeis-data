{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A086592",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 86592,
			"data": "2,3,3,4,4,5,5,5,5,7,7,7,7,8,8,6,6,9,9,10,10,11,11,9,9,12,12,11,11,13,13,7,7,11,11,13,13,14,14,13,13,17,17,15,15,18,18,11,11,16,16,17,17,19,19,14,14,19,19,18,18,21,21,8,8,13,13,16,16,17,17,17,17,22,22,19,19,23",
			"name": "Denominators in left-hand half of Kepler's tree of fractions.",
			"comment": [
				"Form a tree of fractions by beginning with 1/1 and then giving every node i/j two descendants labeled i/(i+j) and j/(i+j).",
				"Level n of the left-hand half of the tree consists of 2^(n-1) nodes: 1/2; 1/3, 2/3; 1/4, 3/4, 2/5, 3/5; 1/5, 4/5, 3/7, 4/7, 2/7, 5/7, 3/8, 5/8; ... .",
				"The right-hand half is identical to the left-hand half. - _Michel Dekking_, Oct 05 2017",
				"n\u003e1 occurs in this sequence phi(n) = A000010(n) times, as it occurs in A007306 (_Franklin T. Adams-Watters_' comment), that is the sequence obtained by adding numerator and denominator in the Calkin-Wilf enumeration system of positive rationals. A020650(n)/A020651(n) is also an enumeration system of all positive rationals (Yu-Ting system), and in each level m \u003e= 0 (ranks between 2^m and 2^(m+1)-1) rationals are the same in both systems. Thus a(n) has the same terms in each level as A007306. The same property occurs in all numerator+denominator sequences of enumeration systems of positive rationals, as, for example, A007306 (A007305+A047679), A071585 (A229742+A071766), and A268087 (A162909+A162910). - _Yosu Yurramendi_, Apr 06 2016"
			],
			"reference": [
				"Johannes Kepler, Mysterium cosmographicum, Tuebingen, 1596, 1621, Caput XII.",
				"Johannes Kepler, Harmonice Mundi, Linz, 1619, Liber III, Caput II.",
				"Johannes Kepler, The Harmony of the World [1619], trans. E. J. Aiton, A. M. Duncan and J. V. Field, American Philosophical Society, Philadelphia, 1997, p. 163."
			],
			"link": [
				"Johannes Kepler, \u003ca href=\"http://archive.org/details/ioanniskepplerih00kepl\"\u003eHarmonices mundi libri V ...\u003c/a\u003e (A Latin original scanned in Internet Archive. The fraction-tree is illustrated on the page 27 of the third book (Liber III), which is on the page 117 of the PDF-document.)",
				"Johannes Kepler, \u003ca href=\"http://web.archive.org/web/20081009062459/http://ndirty.cute.fi/~karttu/Kepler/a086592.htm\"\u003eExcerpt from the Chapter II of the Book III of the Harmony of the World: On the seven harmonic divisions of the string\u003c/a\u003e (Illustrates the A020651/A086592-tree).",
				"OEIS Wiki, \u003ca href=\"http://oeis.org/wiki/Historical_sequences\"\u003eHistorical sequences\u003c/a\u003e",
				"Pelegrí Viader, Jaume Paradís and Lluís Bibiloni, \u003ca href=\"https://doi.org/10.1006/jnth.1998.2294\"\u003eA New Light on Minkowski's ?(x) Function\u003c/a\u003e, J. Number Theory, 73 (2) (1998), 212-227. See p. 215.",
				"\u003ca href=\"/index/Fo#fraction_trees\"\u003eIndex entries for fraction trees\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A020650(n) + A020651(n) = A020650(2n).",
				"a(n) = A071585(A059893(n)), a(A059893(n)) = A071585(n), n \u003e 0. - _Yosu Yurramendi_, May 30 2017",
				"a(2*n-1) = A086593(n); a(2*n) = A086593(n+1), n \u003e 0. - _Yosu Yurramendi_, May 16 2018",
				"a(n) = A007306(A231551(n)), n \u003e 0. - _Yosu Yurramendi_, Aug 07 2021"
			],
			"mathematica": [
				"(* b = A020650 *) b[1] = 1; b[2] = 2; b[3] = 1; b[n_] := b[n] = Switch[ Mod[n, 4], 0, b[n/2 + 1] + b[n/2], 1, b[(n - 1)/2 + 1], 2, b[(n - 2)/2 + 1] + b[(n - 2)/2], 3, b[(n - 3)/2]]; a[n_] := b[2n]; Array[a, 100] (* _Jean-François Alcover_, Jan 22 2016 *)"
			],
			"program": [
				"(R)",
				"maxlevel \u003c- 15",
				"d \u003c- c(1,2)",
				"for(m in 0:maxlevel)",
				"for(k in 1:2^m) {",
				"   d[2^(m+1)    +k] \u003c- d[k] + d[2^m+k]",
				"   d[2^(m+1)+2^m+k] \u003c- d[2^(m+1)+k]",
				"}",
				"b \u003c- vector()",
				"for(m in 0:maxlevel) for(k in 0:(2^m-1)) b[2^m+k] \u003c- d[2^(m+1)+k]",
				"a \u003c- vector()",
				"for(n in 1:2^maxlevel) {a[2*n-1] \u003c- b[n]; a[2*n] \u003c- b[n+1]}",
				"a[1:128]",
				"# _Yosu Yurramendi_, May 16 2018"
			],
			"xref": [
				"Bisection of A020650.",
				"See A093873/A093875 for the full tree.",
				"A020651 gives the numerators. Bisection: A086593. Cf. A002487, A004169."
			],
			"keyword": "nonn,frac,tabf",
			"offset": "1,1",
			"author": "_Antti Karttunen_, Aug 28 2003",
			"ext": [
				"Entry revised by _N. J. A. Sloane_, May 24 2004"
			],
			"references": 12,
			"revision": 50,
			"time": "2021-08-22T17:31:52-04:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}