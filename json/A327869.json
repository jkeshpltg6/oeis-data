{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A327869",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 327869,
			"data": "1,1,1,1,0,1,4,3,3,1,5,4,0,4,1,16,5,10,10,5,1,82,66,75,60,15,6,1,169,112,126,35,140,21,7,1,541,456,196,336,280,224,28,8,1,2272,765,1548,1848,1386,630,336,36,9,1,17966,15070,15525,16080,14070,3780,1050,480,45,10,1",
			"name": "Sum T(n,k) of multinomials M(n; lambda), where lambda ranges over all partitions of n into distinct parts incorporating k; triangle T(n,k), n\u003e=0, 0\u003c=k\u003c=n, read by rows.",
			"comment": [
				"Here we assume that every list of parts has at least one 0 because its addition does not change the value of the multinomial.",
				"Number T(n,k) of set partitions of [n] with distinct block sizes and one of the block sizes is k. T(5,3) = 10: 123|45, 124|35, 125|34, 12|345, 134|25, 135|24, 13|245, 145|23, 14|235, 15|234."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A327869/b327869.txt\"\u003eRows n = 0..140, flattened\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Multinomial_theorem#Multinomial_coefficients\"\u003eMultinomial coefficients\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Partition_(number_theory)\"\u003ePartition (number theory)\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Partition_of_a_set\"\u003ePartition of a set\u003c/a\u003e"
			],
			"example": [
				"Triangle T(n,k) begins:",
				"      1;",
				"      1,     1;",
				"      1,     0,     1;",
				"      4,     3,     3,     1;",
				"      5,     4,     0,     4,     1;",
				"     16,     5,    10,    10,     5,    1;",
				"     82,    66,    75,    60,    15,    6,    1;",
				"    169,   112,   126,    35,   140,   21,    7,   1;",
				"    541,   456,   196,   336,   280,  224,   28,   8,  1;",
				"   2272,   765,  1548,  1848,  1386,  630,  336,  36,  9,  1;",
				"  17966, 15070, 15525, 16080, 14070, 3780, 1050, 480, 45, 10, 1;",
				"  ..."
			],
			"maple": [
				"with(combinat):",
				"T:= (n, k)-\u003e add(multinomial(add(i, i=l), l[], 0),",
				"             l=select(x-\u003e nops(x)=nops({x[]}) and",
				"             (k=0 or k in x), partition(n))):",
				"seq(seq(T(n, k), k=0..n), n=0..11);",
				"# second Maple program:",
				"b:= proc(n, i, k) option remember; `if`(i*(i+1)/2\u003cn, 0,",
				"     `if`(n=0, 1, `if`(i\u003c2, 0, b(n, i-1, `if`(i=k, 0, k)))+",
				"     `if`(i=k, 0, b(n-i, min(n-i, i-1), k)/i!)))",
				"    end:",
				"T:= (n, k)-\u003e n!*(b(n$2, 0)-`if`(k=0, 0, b(n$2, k))):",
				"seq(seq(T(n, k), k=0..n), n=0..11);"
			],
			"mathematica": [
				"b[n_, i_, k_] := b[n, i, k] = If[i(i+1)/2 \u003c n, 0, If[n==0, 1, If[i\u003c2, 0, b[n, i-1, If[i==k, 0, k]]] + If[i==k, 0, b[n-i, Min[n-i, i-1], k]/i!]]];",
				"T[n_, k_] := n! (b[n, n, 0] - If[k == 0, 0, b[n, n, k]]);",
				"Table[T[n, k], {n, 0, 11}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Apr 28 2020, from 2nd Maple program *)"
			],
			"xref": [
				"Columns k=0-3 give: A007837, A327876, A327881, A328155.",
				"Row sums give A327870.",
				"T(2n,n) gives A328156.",
				"Cf. A327801, A327884."
			],
			"keyword": "nonn,tabl",
			"offset": "0,7",
			"author": "_Alois P. Heinz_, Sep 28 2019",
			"references": 9,
			"revision": 31,
			"time": "2020-04-28T07:33:06-04:00",
			"created": "2019-09-28T16:30:19-04:00"
		}
	]
}