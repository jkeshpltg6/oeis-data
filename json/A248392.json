{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A248392",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 248392,
			"data": "1,1,1,0,1,1,0,1,1,1,1,1,0,0,1,1,1,0,0,1,1,0,1,0,0,0,1,1,0,1,1,1,0,1,1,1,1,1,0,1,1,1,0,0,1,1,1,1,0,1,1,0,0,1,1,1,1,1,0,0,1,0,0,0,1,1,1,0,0,1,1,1,0,1,1,0,1,0,0,1,1,1,0,0,1,1,0,1,1,0,0,1,1,0,0,1,1,1,1,1,0,0,1,0,0,0,1",
			"name": "\"Look and say\" sequence, but say everything mod 2; starting with 1.",
			"comment": [
				"In the \"Look and Say\" sequence A005150 you read the previous term from left to right. For each run of consecutive equal digits, you say the length of the run followed by the digit itself.",
				"After 6666777 for example you would say 4 6's, 3 7's, and the next term would be 4637. But now we say both the length of the run and the digit itself mod 2, so 6666777 would be read as 0 0's, 1 1, and the next term would be 0011.",
				"We start with 1, so the initial terms are 1, 11, 01, 1011, 111001, etc. (see the Example lines).",
				"Since this process leads to \"numbers\" with leading zeros, we give the sequence of successive digits instead of the successive \"numbers\" (so 1; 1,1; 0,1; 1,0,1,1; ...)."
			],
			"reference": [
				"Alex Kontorovich, Verbal communication to _N. J. A. Sloane_, Oct 16 2014, describing work that he and Sam Payne did around 1998."
			],
			"link": [
				"Alex Kontorovich, \u003ca href=\"http://math.rutgers.edu/~alexk/progs.html\"\u003ePrograms\u003c/a\u003e",
				"Alex Kontorovich, \u003ca href=\"/A248392/a248392.jpg\"\u003eIllustration of initial terms\u003c/a\u003e"
			],
			"example": [
				"The initial \"numbers\" are:",
				"                         1",
				"                        11",
				"                        01",
				"                      1011",
				"                    111001",
				"                    110011",
				"                    010001",
				"                  10111011",
				"                1110111001",
				"                1110110011",
				"                1110010001",
				"                1100111011",
				"                0100111001",
				"              101100110011",
				"            11100100010001",
				"            11001110111011",
				"            01001110111001",
				"          1011001110110011",
				"        111001001110010001",
				"        110011001100111011",
				"        010001000100111001",
				"      10111011101100110011",
				"    1110111011100100010001",
				"    1110111011001110111011",
				"    1110111001001110111001",
				"    1110110011001110110011",
				"    1110010001001110010001",
				"    1100111011001100111011",
				"    0100111001000100111001",
				"  101100110011101100110011",
				"11100100010011100100010001",
				"11001110110011001110111011",
				"...",
				"The illustration gives a longer list and shows the fractal-like structure more clearly."
			],
			"maple": [
				"# a[n] is the n-th \"number\" read from right to left.",
				"a[1]:=[1]: a[2]:=[1,1]: a[3]:=[1,0]: a[4]:=[1,1,0,1]:",
				"M:=32:",
				"for n from 5 to M do",
				"s:=a[n-1][1]; a[n]:=[]; r:=1;",
				"   for i from 2 to nops(a[n-1]) do",
				"      t:=a[n-1][i];",
				"      if s=t then r:=r+1;",
				"      else a[n]:=[op(a[n]), s, r mod 2]; s:=t; r:=1;",
				"      fi;",
				"                                od:",
				"      a[n]:=[op(a[n]), s, r mod 2];",
				"od:",
				"for n from 1 to M do m:=nops(a[n]); lprint([seq(a[n][m-i+1],i=1..m)]); od:"
			],
			"xref": [
				"Cf. A005150, A248393 (number of 1's in n-th \"number\"), A248396."
			],
			"keyword": "nonn,base",
			"offset": "1",
			"author": "_N. J. A. Sloane_, Oct 17 2014",
			"references": 3,
			"revision": 32,
			"time": "2014-10-18T23:23:13-04:00",
			"created": "2014-10-17T18:31:16-04:00"
		}
	]
}