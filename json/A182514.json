{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A182514",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 182514,
			"data": "2,3,7,113,1327,1693182318746371",
			"name": "Primes prime(n) such that (prime(n+1)/prime(n))^n \u003e n.",
			"comment": [
				"The Firoozbakht conjecture: (prime(n+1))^(1/(n+1)) \u003c prime(n)^(1/n), or prime(n+1) \u003c prime(n)^(1+1/n), prime(n+1)/prime(n) \u003c prime(n)^(1/n), (prime(n+1)/prime(n))^n \u003c prime(n).",
				"Using the Mathematica program shown below, I have found no further terms below 2^27. I conjecture that this sequence is finite and that the terms stated are the only members. - _Robert G. Wilson v_, May 06 2012 [Warning: this conjecture may be false! - _N. J. A. Sloane_, Apr 25 2014]",
				"I conjecture the contrary: the sequence is infinite. Note that 10^13 \u003c a(6) \u003c= 1693182318746371. - _Charles R Greathouse IV_, May 14 2012",
				"[Stronger than Firoozbakht] conjecture: All (prime(n+1)/prime(n))^n values, with n \u003e= 5, are less than n*log(n). - _John W. Nicholson_, Dec 02 2013, Oct 19 2016",
				"The Firoozbakht conjecture can be rewritten as (log(prime(n+1)) / log(prime(n)))^n \u003c (1+1/n)^n. This suggests the [weaker than Firoozbakht] conjecture: (log(prime(n+1))/log(prime(n)))^n \u003c e. - _Daniel Forgues_, Apr 26 2014",
				"All a(n) \u003c= a(6) are in A002386, A205827, and A111870.",
				"The inequality in the definition is equivalent to the inequality prime(n+1)-prime(n) \u003e log(n)*log(prime(n)) for sufficiently large n. - _Thomas Ordowski_, Mar 16 2015",
				"Prime indices, A000720(a(n)) = 1, 2, 4, 30, 217, 49749629143526. - _John W. Nicholson_, Oct 25 2016"
			],
			"reference": [
				"Farhadian, R. (2017). On a New Inequality Related to Consecutive Primes. OECONOMICA, vol 13, pp. 236-242."
			],
			"link": [
				"Reza Farhadian, \u003ca href=\"http://www.primepuzzles.net/conjectures/Reza%20Faradian%20Conjecture.pdf\"\u003eA New Conjecture On the primes\u003c/a\u003e, Preprint, 2016.",
				"R. Farhadian, and R. Jakimczuk, \u003ca href=\"https://doi.org/10.12988/imf.2017.7335\"\u003eOn a New Conjecture of Prime Numbers\u003c/a\u003e Int. Math. Forum, vol. 12, 2017, pp. 559-564.",
				"Luan Alberto Ferreira, \u003ca href=\"http://arxiv.org/abs/1604.03496\"\u003eSome consequences of the Firoozbakht's conjecture\u003c/a\u003e, arXiv:1604.03496v2 [math.NT], 2016.",
				"Luan Alberto Ferreira, Hugo Luiz Mariano, \u003ca href=\"https://doi.org/10.1007/s40863-018-0113-0\"\u003ePrime gaps and the Firoozbakht Conjecture\u003c/a\u003e, São Paulo J. Math. Sci. (2018), 1-11.",
				"A. Kourbatov, \u003ca href=\"http://arxiv.org/abs/1503.01744\"\u003eVerification of the Firoozbakht conjecture for primes up to four quintillion\u003c/a\u003e, arXiv:1503.01744 [math.NT], 2015.",
				"A. Kourbatov, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL18/Kourbatov/kourb7.html\"\u003eUpper bounds for prime gaps related to Firoozbakht's conjecture\u003c/a\u003e, J. Int. Seq. 18 (2015) 15.11.2.",
				"Carlos Rivera, \u003ca href=\"http://www.primepuzzles.net/conjectures/conj_078.htm\"\u003eConjecture 78. P_n^((P_n+1/P_n)^n) \u003c= n^P_n\u003c/a\u003e, 2016.",
				"Nilotpal Kanti Sinha, \u003ca href=\"http://arxiv.org/abs/1010.1399\"\u003eOn a new property of primes that leads to a generalization of Cramer's conjecture\u003c/a\u003e, arXiv:1010.1399 [math.NT], 2010.",
				"Matt Visser, \u003ca href=\"https://arxiv.org/abs/1904.00499\"\u003eVerifying the Firoozbakht, Nicholson, and Farhadian conjectures up to the 81st maximal prime gap\u003c/a\u003e, arXiv:1904.00499 [math.NT], 2019.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Firoozbakht%E2%80%99s_conjecture\"\u003eFiroozbakht’s conjecture\u003c/a\u003e"
			],
			"example": [
				"7 is in the list because, being the 4th prime, and 11 the fifth prime, we verify that (11/7)^4 = 6.09787588507... which is greater than 4.",
				"11 is not on the list because (13/11)^5 = 2.30543740804... and that is less than 5."
			],
			"mathematica": [
				"Prime[Select[Range[1000], (Prime[# + 1]/Prime[#])^# \u003e # \u0026]] (* _Alonso del Arte_, May 04 2012 *)",
				"firoozQ[n_, p_, q_] := n * Log[q] \u003e Log[n] + n * Log[p]; k = 1; p = 2; q = 3; While[ k \u003c 2^27, If[ firoozQ[k, p, q], Print[{k, p}]]; k++; p = q; q = NextPrime@ q] (* _Robert G. Wilson v_, May 06 2012 *)"
			],
			"program": [
				"(PARI) n=1;p=2;forprime(q=3,1e6,if((q/p*1.)^n++\u003en, print1(p\", \"));p=q) \\\\ _Charles R Greathouse IV_, May 14 2012",
				"(PARI) for(n=1,75,if((A000101[n]/A002386[n]*1.)^A005669[n]\u003e=A005669[n], print1(A002386[n],\", \"))) \\\\ Each sequence is read in as a vector as to overcome PARI's primelimit \\\\ _John W. Nicholson_, Dec 01 2013",
				"(PARI) q=3;n=2; forprime(p=5, 10^9,result=(p/q)^n/(n*log(n));if(result\u003e1, print(q,\" \",p, \" \", n, \" \", result));n++;q=p) \\\\ for stronger than Firoozbakht conjecture \\\\ _John W. Nicholson_, Mar 16 2015, Oct 19 2016"
			],
			"xref": [
				"Cf. A111870."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Thomas Ordowski_, May 04 2012",
			"ext": [
				"a(6) from _John W. Nicholson_, Dec 01 2013"
			],
			"references": 6,
			"revision": 138,
			"time": "2019-04-02T04:06:22-04:00",
			"created": "2012-05-05T17:36:55-04:00"
		}
	]
}