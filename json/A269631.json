{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A269631",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 269631,
			"data": "0,1,10,2,11,12,20,21,22,23,24,25,26,27,28,29,32,42,52,62,72,82,92,102,3,13,112,30,120,31,121,33,122,34,123,35,124,36,125,37,126,38,127,39,128,43,129,53,132,63,142,73,152,83,162,93,172,103,113,130,131,133,134,135",
			"name": "a(1) = 0; a(n+1) is the smallest integer not yet used that contains the number of decimal digits of a(n) as a substring.",
			"comment": [
				"Conjecture: a(n) is a permutation of the nonnegative integers.",
				"The following table shows:",
				"C = number of terms calculated",
				"F = number of terms less than C",
				"+------------+----------+-----------+",
				"| C          | F        | %         |",
				"+------------+----------+------------",
				"| 10         | 2        | 20.0      |",
				"| 100        | 39       | 39.0      |",
				"| 1000       | 532      | 53.2      |",
				"| 10000      | 6379     | 63.79     |",
				"| 100000     | 71609    | 71.609    |",
				"| 1000000    | 765630   | 76.563    |",
				"| 10000000   | 7907944  | 79.07944  |",
				"| 100000000  | 81251152 | 81.251152 |",
				".",
				"and the growth of percentage seems to support the conjecture."
			],
			"link": [
				"Francesco Di Matteo, \u003ca href=\"/A269631/b269631.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e"
			],
			"example": [
				"a(2) = 1 because a(1) = 0 and 0 has 1 decimal digit;",
				"a(3) = 10 because a(2) = 1, so 1 has 1 digit, and 10 is the first integer not yet used that contains \"1\";",
				"a(4) = 2 because a(3) = 10 and 10 has 2 digits;",
				"a(5) = 11 because a(4) = 2, so 2 has 1 digit, and 11 is the first integer not yet used that contains \"1\"; ..."
			],
			"mathematica": [
				"a = {0, 1}; Do[AppendTo[a, SelectFirst[Range[10^3], And[! MemberQ[a, #], MemberQ[IntegerDigits@ #, IntegerLength@ a[[n - 1]]]] \u0026]], {n, 3, 64}]; a (* _Michael De Vlieger_, Apr 01 2016 *)"
			],
			"program": [
				"(Python)",
				"# This routine is a little bit more complex compared to the same with",
				"# the a(n) terms 'storage' (now we memorize only the highest number",
				"# for every k-digits) but is very much faster.",
				"print(\"0\", end=',')",
				"lista = [-1,0,0,0,0,0,0,0,0,0]",
				"a = 1",
				"for g in range (1,100):",
				"    b = len(str(a))",
				"    val = lista[b] + 1",
				"    flag = 0",
				"    while flag == 0:",
				"        sval = str(val)",
				"        while str(b) not in sval:",
				"            val += 1",
				"            sval = str(val)",
				"        k = len(sval)",
				"        comp = k",
				"        for s in range(k):",
				"            if lista[int(sval[s])] \u003c val:",
				"                comp -= 1",
				"        if comp == 0:",
				"            flag = 1",
				"        else:",
				"            val +=1",
				"    print(val, end=',')",
				"    lista[b] = val",
				"    a = val",
				"# _Francesco Di Matteo_, Mar 02 2016",
				"(PARI) findnew(nbd, vsa) = {k=0; while (vecsearch(vsa, k) || !vecsearch(vecsort(digits(k)), nbd), k++); k;}",
				"listd(nn) = {va = vector(nn); print1(va[1], \", \"); vsa = vecsort(va,,8); for (n=2, nn, nbd = #Str(va[n-1]); na = findnew(nbd, vsa); print1(na, \", \"); va[n] = na; vsa = vecsort(va,,8););} \\\\ _Michel Marcus_, Mar 08 2016"
			],
			"keyword": "nonn,base",
			"offset": "1,3",
			"author": "_Francesco Di Matteo_, Mar 01 2016",
			"references": 1,
			"revision": 38,
			"time": "2021-05-11T05:55:34-04:00",
			"created": "2016-04-09T13:38:11-04:00"
		}
	]
}