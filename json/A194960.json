{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A194960",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 194960,
			"data": "1,2,3,2,3,4,3,4,5,4,5,6,5,6,7,6,7,8,7,8,9,8,9,10,9,10,11,10,11,12,11,12,13,12,13,14,13,14,15,14,15,16,15,16,17,16,17,18,17,18,19,18,19,20,19,20,21,20,21,22,21,22,23,22,23,24,23,24,25,24,25,26,25,26",
			"name": "a(n) = floor((n+2)/3) + ((n-1) mod 3).",
			"comment": [
				"The sequence is formed by concatenating triples of the form (n,n+1,n+2) for n\u003e=1.  See A194961 and A194962 for the associated fractalization and interspersion. The sequence can be obtained from A008611 by deleting its first four terms.",
				"The sequence contains every positive integer n exactly min(n,3) times. - _Wesley Ivan Hurt_, Dec 17 2013"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A194960/b194960.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,0,1,-1)."
			],
			"formula": [
				"a(n) = ((-1)^n*A130772(n))+n+4)/3. G.f. -x*(-1-x-x^2+2*x^3) / ((1+x+x^2)*(x-1)^2). - _R. J. Mathar_, Sep 07 2011",
				"a(n) = A006446(n)/floor(sqrt(A006446(n))). - _Benoit Cloitre_, Jan 15 2012",
				"a(n) = a(n-1) + a(n-3) - a(n-4). - _Vincenzo Librandi_, Dec 17 2013",
				"a(n) = a(n-3) + 1, n \u003e= 1, with input a(-2) = 0, a(-1) = 1 and a(0) = 2. Proof trivial. a(n) = A008611(n+3), n \u003e= -2. See the first comment above. - _Wolfdieter Lang_, May 06 2017",
				"From _Guenther Schrack_, Nov 09 2020: (Start)",
				"a(n) = n - 2*floor((n-1)/3);",
				"a(n) = (n + 2 + 2*((n-1) mod 3))/3;",
				"a(n) = (3*n + 12 + 2*(w^(2*n)*(1 - w) + w^n*(2 + w)))/9, where w = (-1 + sqrt(-3))/2;",
				"a(n) = (n + 4 + 2*A049347(n))/3;",
				"a(n) = (2*n + 3 - A330396(n-1))/3. (End)"
			],
			"maple": [
				"A194960:=n-\u003efloor((n+2)/3)+((n-1) mod 3); seq(A194960(n), n=1..100); # _Wesley Ivan Hurt_, Dec 17 2013"
			],
			"mathematica": [
				"p[n_] := Floor[(n + 2)/3] + Mod[n - 1, 3]",
				"Table[p[n], {n, 1, 90}]  (* A194960 *)",
				"g[1] = {1}; g[n_] := Insert[g[n - 1], n, p[n]]",
				"f[1] = g[1]; f[n_] := Join[f[n - 1], g[n]]",
				"f[20]  (* A194961 *)",
				"row[n_] := Position[f[30], n];",
				"u = TableForm[Table[row[n], {n, 1, 5}]]",
				"v[n_, k_] := Part[row[n], k];",
				"w = Flatten[Table[v[k, n - k + 1], {n, 1, 13},",
				"{k, 1, n}]]  (* A194962 *)",
				"q[n_] := Position[w, n]; Flatten[",
				"Table[q[n], {n, 1, 80}]]  (* A194963 *)",
				"CoefficientList[Series[(1 + x + x^2 - 2 x^3)/((1 + x + x^2) (x - 1)^2), {x, 0, 100}], x] (* _Vincenzo Librandi_, Dec 17 2013 *)"
			],
			"program": [
				"(MAGMA) I:=[1,2,3,2]; [n le 4 select I[n] else Self(n-1)+Self(n-3)-Self(n-4): n in [1..100]]; // _Vincenzo Librandi_, Dec 17 2013",
				"(PARI) a(n)=(n+2)\\3 + (n-1)%3 \\\\ _Charles R Greathouse IV_, Sep 02 2015"
			],
			"xref": [
				"Cf. A008611, A047878, A049347, A194961, A194962, A194963, A330396."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Sep 06 2011",
			"references": 7,
			"revision": 43,
			"time": "2020-12-18T14:04:33-05:00",
			"created": "2011-09-07T12:16:17-04:00"
		}
	]
}