{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006530",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6530,
			"id": "M0428",
			"data": "1,2,3,2,5,3,7,2,3,5,11,3,13,7,5,2,17,3,19,5,7,11,23,3,5,13,3,7,29,5,31,2,11,17,7,3,37,19,13,5,41,7,43,11,5,23,47,3,7,5,17,13,53,3,11,7,19,29,59,5,61,31,7,2,13,11,67,17,23,7,71,3,73,37,5,19,11,13,79,5,3,41,83,7,17,43",
			"name": "Gpf(n): greatest prime dividing n, for n \u003e= 2; a(1)=1.",
			"comment": [
				"The initial term a(1)=1 is purely conventional: The unit 1 is not a prime number, although it has been considered so in the past. 1 is the empty product of prime numbers, thus 1 has no largest prime factor. - _Daniel Forgues_, Jul 05 2011",
				"Greatest noncomposite number dividing n, (cf. A008578). - _Omar E. Pol_, Aug 31 2013",
				"Conjecture: Let a, b be nonzero integers and f(n) denote the maximum prime factor of a*n + b if a*n + b \u003c\u003e 0 and f(n)=0 if a*n + b=0 for any integer n. Then the set {n, f(n), f(f(n)), ...} is finite of bounded size. - _M. Farrokhi D. G._, Jan 10 2021"
			],
			"reference": [
				"M. Abramowitz and I. A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, 1964 (and various reprintings), p. 844.",
				"D. S. Mitrinovic et al., Handbook of Number Theory, Kluwer, Section IV.1.",
				"H. L. Montgomery, Ten Lectures on the Interface Between Analytic Number Theory and Harmonic Analysis, Amer. Math. Soc., 1996, p. 210.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Daniel Forgues, \u003ca href=\"/A006530/b006530.txt\"\u003eTable of n, a(n) for n = 1..100000\u003c/a\u003e [First 10000 terms from T. D. Noe]",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"K. Alladi and P. Erdős, \u003ca href=\"http://projecteuclid.org/euclid.pjm/1102811427\"\u003eOn an additive arithmetic function\u003c/a\u003e, Pacific J. Math., Volume 71, Number 2 (1977), 275-294. MR 0447086 (56 #5401).",
				"G. Back and M. Caragiu, \u003ca href=\"http://www.fq.math.ca/Papers1/48-4/Back_Caragiu.pdf\"\u003eThe greatest prime factor and recurrent sequences\u003c/a\u003e, Fib. Q., 48 (2010), 358-362.",
				"A. E. Brouwer, \u003ca href=\"/A046670/a046670.pdf\"\u003eTwo number theoretic sums\u003c/a\u003e, Stichting Mathematisch Centrum. Zuivere Wiskunde, Report ZW 19/74 (1974): 3 pages. [Cached copy, included with the permission of the author]",
				"Paul Erdős, Andrew Granville, Carl Pomerance and Claudia Spiro, \u003ca href=\"http://math.dartmouth.edu/~carlp/iterate.pdf\"\u003eOn the normal behavior of the iterates of some arithmetic functions\u003c/a\u003e, Analytic number theory, Birkhäuser Boston, 1990, pp. 165-204.",
				"Paul Erdős, Andrew Granville, Carl Pomerance and Claudia Spiro, \u003ca href=\"/A000010/a000010_1.pdf\"\u003eOn the normal behavior of the iterates of some arithmetic functions\u003c/a\u003e, Analytic number theory, Birkhäuser Boston, 1990, pp. 165-204. [Annotated copy with A-numbers]",
				"Mats Granvik, \u003ca href=\"/A006530/a006530_2.txt\"\u003eMathematica program to check the conjectured formula\u003c/a\u003e, Feb 28 2021.",
				"Nathan McNew, \u003ca href=\"https://doi.org/10.1080/10586458.2016.1155188\"\u003eThe Most Frequent Values of the Largest Prime Divisor Function\u003c/a\u003e, Exper. Math., 2017, Vol. 26, No. 2, 210-224.",
				"OEIS Wiki, \u003ca href=\"/wiki/Greatest_prime_factor_of_n\"\u003eGreatest prime factor of n\u003c/a\u003e",
				"H. P. Robinson, \u003ca href=\"/A006530/a006530.pdf\"\u003eLetter to N. J. A. Sloane, Oct 1981\u003c/a\u003e",
				"David Singmaster, \u003ca href=\"/A005178/a005178.pdf\"\u003eLetter to N. J. A. Sloane\u003c/a\u003e, Oct 03 1982.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/GreatestPrimeFactor.html\"\u003eGreatest Prime Factor\u003c/a\u003e",
				"\u003ca href=\"/index/Cor#core\"\u003eIndex entries for \"core\" sequences\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A027748(n, A001221(n)) = A027746(n, A001222(n)); a(n)^A071178(n) = A053585(n). - _Reinhard Zumkeller_, Aug 27 2011",
				"a(n) = A000040(A061395(n)). - _M. F. Hasler_, Jan 16 2015",
				"a(n) = n + 1 - Sum_{k=1..n} (floor((k!^n)/n) - floor(((k!^n)-1)/n)). - _Anthony Browne_, May 11 2016",
				"n/a(n) = A052126(n). - _R. J. Mathar_, Oct 03 2016",
				"If A020639(n) = n [when n is 1 or a prime] then a(n) = n, otherwise a(n) = a(A032742(n)). - _Antti Karttunen_, Mar 12 2017",
				"a(n) has average order Pi^2*n/(12 log n) [Brouwer]. See also A046670. - _N. J. A. Sloane_, Jun 26 2017"
			],
			"maple": [
				"with(numtheory,divisors); A006530 := proc(n) local i,t1,t2,t3,t4,t5; t1 := divisors(n); t2 := convert(t1,list); t3 := sort(t2); t4 := nops(t3); t5 := 1; for i from 1 to t4 do if isprime(t3[t4+1-i]) then return t3[t4+1-i]; fi; od; 1; end;",
				"# alternative",
				"A006530 := n-\u003emax(1,op(numtheory[factorset](n))); # _Peter Luschny_, Nov 02 2010"
			],
			"mathematica": [
				"Table[ FactorInteger[n][[ -1, 1]], {n, 100}] (* _Ray Chandler_, Nov 12 2005 and modified by _Robert G. Wilson v_, Jul 16 2014 *)"
			],
			"program": [
				"(PARI) A006530(n)=if(n\u003e1,vecmax(factor(n)[,1]),1) \\\\ Edited to cover n=1. - _M. F. Hasler_, Jul 30 2015",
				"(MAGMA) [ #f eq 0 select 1 else f[ #f][1] where f is Factorization(n): n in [1..86] ] // _Klaus Brockhaus_, Oct 23 2008",
				"(Scheme)",
				";; The following uses macro definec for the memoization (caching) of the results. A naive implementation of A020639 can be found under that entry. It could be also defined with definec to make it faster on the later calls. See http://oeis.org/wiki/Memoization#Scheme",
				"(definec (A006530 n) (let ((spf (A020639 n))) (if (= spf n) spf (A006530 (/ n spf)))))",
				";; _Antti Karttunen_, Mar 12 2017"
			],
			"xref": [
				"Cf. A000040, A020639 (smallest prime divisor), A034684, A028233, A034699, A053585.",
				"See also A032742, A052126, A070087, A070089, A061395, A175723.",
				"Cf. A046670 (partial sums), A104350 (partial products).",
				"See A124661 for \"popular\" primes."
			],
			"keyword": "nonn,nice,easy,core",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Edited by _M. F. Hasler_, Jan 16 2015"
			],
			"references": 926,
			"revision": 212,
			"time": "2021-03-11T02:51:22-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}