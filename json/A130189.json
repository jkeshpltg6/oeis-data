{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A130189",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 130189,
			"data": "1,-1,5,-7,68,-167,2057,-4637,75703,-39941,676360,-902547,602501827,-432761746,2438757091,-8997865117,346824403906,-1857709421899,325976550837563,-282728710837871,39928855264303811,-16874802689368067,162083496666375118,-3212329557624761759",
			"name": "Numerators of z-sequence for the Sheffer matrix (triangle) A094816 (coefficients of Poisson-Charlier polynomials).",
			"comment": [
				"The denominators are given in A130190.",
				"This z-sequence is useful for the recurrence for S(n,m=0):= A094816(n,0) (first column): S(n,0) = n*Sum_{j=0..n-1} z(j)*S(n-1,j), n \u003e= 1, S(0,0)=1.",
				"See the W. Lang link under A006232 with a summary on a- and z-sequences for Sheffer matrices."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A130189/b130189.txt\"\u003eTable of n, a(n) for n = 0..569\u003c/a\u003e",
				"W. Lang, \u003ca href=\"/A130189/a130189.txt\"\u003eRationals, z-sequence. \u003c/a\u003e"
			],
			"formula": [
				"E.g.f. for rationals z(n)=a(n)/A130190(n) (in lowest terms): (1-exp(-h(x)))/h(x) with h(x):=1-exp(-x).",
				"Numerator of (-1)^n Sum_{k=0..n} A048993(n,k)/(k+1). - _Peter Luschny_, Apr 28 2009"
			],
			"example": [
				"Rationals z(n): [1, -1/2, 5/6, -7/4, 68/15, -167/12, 2057/42, -4637/24, ...].",
				"Recurrence from z(n) sequence for S(n,0)= A094816(n,0) for n=4: 1 = S(4,0) = 4*(1*1 - (1/2)*8 + (5/6)*6 - (7/4)*1) with the 3rd row [1,8,6,1] of A094816."
			],
			"maple": [
				"seq(numer((-1)^n*add(Stirling2(n,k)/(k+1),k=0..n)),n=0..20); # _Peter Luschny_, Apr 28 2009"
			],
			"mathematica": [
				"Table[(-1)^n*Numerator[Sum[StirlingS2[n, k]/(k + 1), {k, 0, n}]], {n, 0, 50}] (* _G. C. Greubel_, Jul 10 2018 *)"
			],
			"program": [
				"(PARI) a(n) = (-1)^n*numerator(sum(k=0, n, stirling(n, k, 2)/(k+1))); \\\\ _Michel Marcus_, Jan 15 2015"
			],
			"xref": [
				"Cf. A027641/A027642 (Bernoulli numbers) provide the a-sequence for the Sheffer matrix A094816."
			],
			"keyword": "sign,frac,easy",
			"offset": "0,3",
			"author": "_Wolfdieter Lang_, Jun 01 2007",
			"references": 4,
			"revision": 25,
			"time": "2021-08-06T05:07:43-04:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}