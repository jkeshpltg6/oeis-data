{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A187660",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 187660,
			"data": "1,1,-1,1,-1,-1,1,-2,-1,1,1,-2,-3,1,1,1,-3,-3,4,1,-1,1,-3,-6,4,5,-1,-1,1,-4,-6,10,5,-6,-1,1,1,-4,-10,10,15,-6,-7,1,1,1,-5,-10,20,15,-21,-7,8,1,-1,1,-5,-15,20,35,-21,-28,8,9,-1,-1,1,-6,-15,35,35,-56,-28,36,9,-10,-1,1",
			"name": "Triangle read by rows: T(n,k) = (-1)^(floor(3*k/2))*binomial(floor((n+k)/2),k), 0 \u003c= k \u003c= n.",
			"comment": [
				"Conjecture: (i) Let n \u003e 1 and N=2*n+1. Row n of T gives the coefficients of the characteristic polynomial p_N(x)=Sum_{k=0..n} T(n,k)*x^(n-k) of the n X n Danzer matrix D_{N,n-1} = {{0,...,0,1}, {0,...,0,1,1}, ..., {0,1,...,1}, {1,...,1}}. (ii) Let S_0(t)=1, S_1(t)=t and S_r(t)=t*S_(r-1)(t)-S_(r-2)(t), r \u003e 1 (cf. A049310). Then p_N(x)=0 has solutions w_{N,j}=S_(n-1)(phi_{N,j}), where phi_{N,j}=2*(-1)^(j+1)*cos(j*Pi/N), j = 1..n. - _L. Edson Jeffery_, Dec 18 2011"
			],
			"link": [
				"L. E. Jeffery, \u003ca href=\"/wiki/User:L._Edson_Jeffery/Unit-Primitive_Matrices\"\u003eDanzer matrices\u003c/a\u003e",
				"Guoce Xin and Yueming Zhong, \u003ca href=\"https://arxiv.org/abs/2201.02376\"\u003eProving some conjectures on Kekulé numbers for certain benzenoids by using Chebyshev polynomials\u003c/a\u003e, arXiv:2201.02376 [math.CO], 2022."
			],
			"formula": [
				"T(n,k) = (-1)^n*A066170(n,k).",
				"abs(T(n,k)) = A046854(n,k) = abs(A066170(n,k)) = abs(A130777(n,k)).",
				"abs(T(n,k)) = A065941(n,n-k) = abs(A108299(n,n-k))."
			],
			"example": [
				"Triangle begins:",
				"  1;",
				"  1,  -1;",
				"  1,  -1,  -1;",
				"  1,  -2,  -1,   1;",
				"  1,  -2,  -3,   1,   1;",
				"  1,  -3,  -3,   4,   1,  -1;",
				"  1,  -3,  -6,   4,   5,  -1,  -1;",
				"  1,  -4,  -6,  10,   5,  -6,  -1,   1;",
				"  1,  -4, -10,  10,  15,  -6,  -7,   1,   1;",
				"  1,  -5, -10,  20,  15, -21,  -7,   8,   1,  -1;",
				"  1,  -5, -15,  20,  35, -21, -28,   8,   9,  -1,  -1;",
				"  1,  -6, -15,  35,  35, -56, -28,  36,   9, -10,  -1,   1;"
			],
			"maple": [
				"A187660 := proc(n,k): (-1)^(floor(3*k/2))*binomial(floor((n+k)/2),k) end: seq(seq(A187660(n,k), k=0..n), n=0..11); # _Johannes W. Meijer_, Aug 08 2011"
			],
			"mathematica": [
				"t[n_, k_] := (-1)^Floor[3 k/2] Binomial[Floor[(n + k)/2], k]; Table[t[n, k], {n, 0, 11}, {k, 0, n}] (* _L. Edson Jeffery_, Oct 20 2017 *)"
			],
			"xref": [
				"Signed version of A046854.",
				"Absolute values of a(n) form a reflected version of A065941, which is considered the main entry.",
				"Cf. A046854, A066170, A130777, A267482."
			],
			"keyword": "sign,easy,tabl,changed",
			"offset": "0,8",
			"author": "_L. Edson Jeffery_, Mar 12 2011",
			"ext": [
				"Edited and corrected by _L. Edson Jeffery_, Oct 20 2017"
			],
			"references": 6,
			"revision": 48,
			"time": "2022-01-10T03:29:08-05:00",
			"created": "2011-03-12T09:58:33-05:00"
		}
	]
}