{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A116914",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 116914,
			"data": "1,1,5,16,58,211,781,2920,11006,41746,159154,609324,2341060,9021559,34855741,134972368,523689718,2035462990,7923732118,30889008112,120566373676,471134916286,1842964183570,7216096752496,28279240308268,110913181145716,435333520075796,1709861650762900",
			"name": "Number of UUDD's, where U=(1,1) and D=(1,-1), in all hill-free Dyck paths of semilength n (a hill in a Dyck path is a peak at level 1).",
			"comment": [
				"Catalan transform of A034299. - _R. J. Mathar_, Jun 29 2009"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A116914/b116914.txt\"\u003eTable of n, a(n) for n = 2..1000\u003c/a\u003e",
				"David Anderson, E. S. Egge, M. Riehl, L. Ryan, R. Steinke, Y. Vaughan, \u003ca href=\"http://arxiv.org/abs/1605.06825\"\u003ePattern Avoiding Linear Extensions of Rectangular Posets\u003c/a\u003e, arXiv:1605.06825 [math.CO], 2016.",
				"Colin Defant, \u003ca href=\"https://arxiv.org/abs/1905.02309\"\u003eProofs of Conjectures about Pattern-Avoiding Linear Extensions\u003c/a\u003e, arXiv:1905.02309 [math.CO], 2019.",
				"E. Deutsch and L. Shapiro, \u003ca href=\"http://dx.doi.org/10.1016/S0012-365X(01)00121-2\"\u003eA survey of the Fine numbers\u003c/a\u003e, Discrete Math., 241 (2001), 241-265."
			],
			"formula": [
				"a(n) = Sum_{k=0..floor(n/2)} k*A105640(n,k).",
				"G.f.: x*(1 + 5*x - (1-x)*sqrt(1-4*x))/(2*(2+x)^2*sqrt(1-4*x)).",
				"a(n+2) = A126258(2*n,n). - _Philippe Deléham_, Mar 13 2007",
				"a(n) ~ 2^(2*n-1)/(9*sqrt(Pi*n)). - _Vaclav Kotesovec_, Mar 20 2014"
			],
			"example": [
				"a(4)=5 because in the 6 (=A000957(5)) hill-free Dyck paths of semilength 4, namely UU(UUDD)DD, UUUDUDDD, UUD(UUDD)D, UUDUDUDD, U(UUDD)UDD and (UUDD)(UUDD) (U=(1,1), D=(1,-1)) we have altogether 5 UUDD's (shown between parentheses)."
			],
			"maple": [
				"G:=z*(1+5*z-(1-z)*sqrt(1-4*z))/2/(2+z)^2/sqrt(1-4*z): Gser:=series(G,z=0,31): seq(coeff(Gser,z^n),n=2..28);"
			],
			"mathematica": [
				"Rest[Rest[CoefficientList[Series[x*(1+5*x-(1-x)*Sqrt[1-4*x])/2/(2+x)^2/Sqrt[1-4*x], {x, 0, 40}], x]]] (* _Vaclav Kotesovec_, Mar 20 2014 *)"
			],
			"program": [
				"(PARI) my(x='x+O('x^40)); Vec(x*(1+5*x-(1-x)*sqrt(1-4*x))/(2*(2+x)^2 *sqrt(1-4*x))) \\\\ _G. C. Greubel_, Feb 08 2017",
				"(MAGMA) R\u003cx\u003e:=PowerSeriesRing(Rationals(), 40); Coefficients(R!( x*(1 + 5*x-(1-x)*Sqrt(1-4*x))/(2*(2+x)^2*Sqrt(1-4*x)) )); // _G. C. Greubel_, May 08 2019",
				"(Sage) a=(x*(1+5*x-(1-x)*sqrt(1-4*x))/(2*(2+x)^2*sqrt(1-4*x))).series(x, 40).coefficients(x, sparse=False); a[2:] # _G. C. Greubel_, May 08 2019"
			],
			"xref": [
				"Cf. A105640."
			],
			"keyword": "nonn",
			"offset": "2,3",
			"author": "_Emeric Deutsch_, May 08 2006",
			"references": 3,
			"revision": 31,
			"time": "2019-05-08T04:37:17-04:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}