{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A084911",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 84911,
			"data": "7,5,2,0,1,0,7,4,2,3",
			"name": "Decimal expansion of linear asymptotic constant B in Sum_{k=1..n} 1/A000688(k) = ~B*n + ...",
			"reference": [
				"Steven R. Finch, Mathematical Constants, Cambridge University Press, 2003, Section 5.1 Abelian group enumeration constants, p. 274."
			],
			"link": [
				"Jean-Marie De Koninck and Aleksandar Ivić, \u003ca href=\"https://www.sciencedirect.com/bookseries/north-holland-mathematics-studies/vol/43\"\u003eTopics in Arithmetical Functions: Asymptotic Formulae for Sums of Reciprocals of Arithmetical Functions and Related Fields\u003c/a\u003e, Amsterdam, Netherlands: North-Holland, 1980. See p. 16.",
				"László Tóth, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL20/Toth/toth25.html\"\u003eAlternating sums concerning multiplicative arithmetic functions\u003c/a\u003e, Journal of Integer Sequences, Vol. 20 (2017), Article 17.2.1; \u003ca href=\"https://arxiv.org/abs/1608.00795\"\u003earXiv preprint\u003c/a\u003e, arXiv:1608.00795 [math.NT], 2016.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/AbelianGroup.html\"\u003eAbelian Group\u003c/a\u003e."
			],
			"formula": [
				"Equals Product_{p prime} (1-Sum_{k \u003e= 2} (1/P(k-1)-1/P(k)/p^k), where P(k) is the unrestricted partition function. - _Jean-François Alcover_, Apr 29 2016",
				"Equals lim_{n-\u003eoo} (1/n) * Sum_{k=1..n} 1/A000688(k). - _Amiram Eldar_, Oct 16 2020"
			],
			"example": [
				"0.7520107423..."
			],
			"mathematica": [
				"digits = 10; m0 (* initial number of primes *) = 10^6; dm = 2*10^5; PP = PartitionsP; DP[n_] := DP[n] = (1/PP[n - 1] - 1 /PP[n]) // N[#, digits + 5]\u0026; pmax = Prime[1000];",
				"nmax[p_ /; p \u003c= pmax] := nmax[p] = Module[{n}, For[n = 2, n \u003c 1000, n++, If[Abs[1/PP[n - 1] - 1 /PP[n]]/p^n \u003c 10^-100, Return[n]]]]; nmax[p_ /; p \u003e pmax] := nmax[pmax];",
				"s[p_] := Sum[DP[n]/p^n, {n, 2, nmax[p]}] ;",
				"f[m_] := f[m] = Product[1 - s[p], {p, Prime[Range[m]]}]; f[m0]; f[m = m0 + dm]; While[RealDigits[f[m], 10, digits + 2][[1]] != RealDigits[f[m - dm], 10, digits + 2][[1]], m = m + dm; Print[m, \" \", RealDigits[f[m]]]];",
				"A0 = f[m]; RealDigits[A0, 10, digits][[1]] (* _Jean-François Alcover_, Apr 29 2016 *)"
			],
			"xref": [
				"Cf. A000688, A021002, A084892, A084893, A272339."
			],
			"keyword": "nonn,cons,more",
			"offset": "0,1",
			"author": "_Eric W. Weisstein_, Jun 11 2003",
			"ext": [
				"Data corrected by _Jean-François Alcover_, Apr 29 2016"
			],
			"references": 3,
			"revision": 21,
			"time": "2020-11-29T05:53:22-05:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}