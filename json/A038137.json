{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A038137",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 38137,
			"data": "1,1,1,1,2,2,1,3,5,3,1,4,9,10,5,1,5,14,22,20,8,1,6,20,40,51,38,13,1,7,27,65,105,111,71,21,1,8,35,98,190,256,233,130,34,1,9,44,140,315,511,594,474,235,55,1,10,54,192,490,924,1295,1324,942,420,89,1,11,65,255",
			"name": "Reflection of A037027: T(n,m) = U(n,n-m), m=0..n, where U is as in A037027.",
			"comment": [
				"Number of lattice paths from (0,0) to (n,k) using steps (1,0), (1,1), (2,2). - _Joerg Arndt_, Jul 01 2011",
				"The n-th diagonal D(n) = {T(n,0), T(n+1,1), ..., T(n+m,m), ...} of the triangle has generating function F(x) = 1/(1 - x - x^2)^(n+1) for n = 0,1,2,.... - _L. Edson Jeffery_, Mar 20 2011",
				"Let p(n,x) denote the Fibonacci polynomial, defined by p(1,x) = 1, p(2,x) = x, and p(n,x) = x*p(n-1,x) + p(n-2,x) for n \u003e= 3. Let q(n,x) be the numerator polynomial of the rational function p(n, 1 + 1/x). The coefficients of the polynomial q(n,x) are given by the (n-1)-th row of T(n,k). E.g., p(5,x) = 1 + 3*x^2 + x^4 gives q(5,x) = 1 + 4*x + 9*x^2 + 10*x^2 + 5*x^4. - _Clark Kimberling_, Nov 04 2013"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A038137/b038137.txt\"\u003eRows n = 0..150 of triangle, flattened\u003c/a\u003e",
				"Pieter Moree, \u003ca href=\"https://arxiv.org/abs/math/0311205\"\u003eConvoluted convolved Fibonacci numbers\u003c/a\u003e, arXiv:math/0311205 [math.CO], 2003.",
				"Pieter Moree, \u003ca href=\"http://emis.impa.br/EMIS/journals/JIS/VOL7/Moree/moree12.html\"\u003eConvoluted convolved Fibonacci numbers\u003c/a\u003e, Journal of Integer Sequences, Vol. 7 (2004), Article 04.2.2.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"https://mathworld.wolfram.com/FibonacciPolynomial.html\"\u003eFibonacci polynomial\u003c/a\u003e."
			],
			"formula": [
				"From _Paul Barry_, Oct 24 2005: (Start)",
				"G.f.: 1/(1 - x - x*y - x^2*y^2).",
				"T(n,k) = Sum_{j=0..n} C((n+j)/2, j) * (1 + (-1)^(n+j)) * C(j, n-k)/2. (End)",
				"T(n,k) = T(n-1,k) + T(n-1,k-1) + T(n-2,k-2), T(n,k) = 0 if n \u003c 0 or if n \u003c k, and T(0,0) = 1. - _Philippe Deléham_, Nov 30 2006",
				"Sum_{k=0..n} (-1)^k*T(n,k) = A059841(n). - _Philippe Deléham_, Nov 30 2006",
				"T(n,k) = A208336(n+1,k).- _Philippe Deléham_, Apr 05 2012"
			],
			"example": [
				"Triangle T(n,k) (with rows n \u003e= 0 and columns 0 \u003c= k \u003c= n) begins",
				"  1;",
				"  1, 1;",
				"  1, 2,  2;",
				"  1, 3,  5,  3;",
				"  1, 4,  9, 10,   5;",
				"  1, 5, 14, 22,  20,   8;",
				"  1, 6, 20, 40,  51,  38, 13;",
				"  1, 7, 27, 65, 105, 111, 71, 21;",
				"  ..."
			],
			"program": [
				"(Haskell)",
				"a038137 n k = a038137_tabl !! n !! k",
				"a038137_row n = a038137_tabl !! n",
				"a038137_tabl = map reverse a037027_tabl",
				"-- _Reinhard Zumkeller_, Jul 08 2012"
			],
			"xref": [
				"Row sums are Pell numbers A000129.",
				"Diagonal sums are unsigned version of A077930.",
				"Cf. A037027, A059841, A208336."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "0,5",
			"author": "_Floor van Lamoen_",
			"ext": [
				"Title corrected by _L. Edson Jeffery_, Apr 23 2011",
				"Corrected by _Philippe Deléham_, Apr 05 2012"
			],
			"references": 9,
			"revision": 62,
			"time": "2021-02-11T03:57:28-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}