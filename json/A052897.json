{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A052897",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 52897,
			"data": "1,2,8,44,304,2512,24064,261536,3173888,42483968,621159424,9841950208,167879268352,3065723549696,59651093528576,1231571119812608,26883546193002496,618463501807058944",
			"name": "Expansion of e.g.f.: exp(2*x/(1-x)).",
			"comment": [
				"Previous name was: A simple grammar."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A052897/b052897.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=873\"\u003eEncyclopedia of Combinatorial Structures 873\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/transforms.txt\"\u003eTransforms\u003c/a\u003e",
				"\u003ca href=\"/index/La#Laguerre\"\u003eIndex entries for sequences related to Laguerre polynomials\u003c/a\u003e"
			],
			"formula": [
				"E.g.f.: exp(2*x/(1-x)). - _Vladeta Jovovic_, Jan 04 2001",
				"Recurrence: {a(0)=1, a(1)=2, (n^2+n)*a(n) + (-4-2*n)*a(n+1) + a(n+2)}.",
				"LAH transform of A000079: a(n) = Sum_{k=0..n) 2^k*n!/k!*binomial(n-1, k-1). - _Vladeta Jovovic_, Oct 17 2003",
				"a(n) = n!*L(n,-1,-2). - _Karol A. Penson_, Oct 16 2006 [Here L(n,a,x) is the n-th generalized Laguerre polynomial with parameter a, evaluated at x. L(n,a,x) is 1 if n=0, a+1-x if n=1 and otherwise (2*n+a-1-x)/n*L(n-1,a,x)-(n+a-1)/n*L(n-2,a,x). - _Peter Luschny_, Nov 20 2011]",
				"a(n) ~ 2^(-1/4)*exp(2*sqrt(2*n)-n-1)*n^(n-1/4) * (1 + 7/(48*sqrt(2*n))). - _Vaclav Kotesovec_, Oct 09 2012, extended Dec 01 2021",
				"E.g.f.: 1 + 2*x/((1-x)*T(0) - x), where T(k) = 4*k+1 + x^2/((4*k+3)*(1-x)^2 + x^2/T(k+1) ); (continued fraction). - _Sergei N. Gladkovskii_, Nov 30 2013",
				"E.g.f.: exp(Sum_{k\u003e=1} 2*x^k). - _Vaclav Kotesovec_, Mar 07 2015",
				"a(n) = Sum_{k=0..n} binomial(n,k)*l(k)*l(n-k), where l(m) = A000262(m). - _Emanuele Munarini_, Aug 31 2017"
			],
			"maple": [
				"L := proc(n,a,x) if n=0 then 1 elif n=1 then a+1-x else (2*n+a-1-x)/n*L(n-1,a,x) - (n+a-1)/n*L(n-2,a,x) fi end: A052897 := n -\u003e n!*L(n,-1,-2): seq(A052897(n),n=0..17); # _Peter Luschny_, Nov 20 2011",
				"spec := [S,{B=Set(C),C=Sequence(Z,1 \u003c= card),S=Prod(B,B)},labeled]: seq(combstruct[count](spec,size=n), n=0..20);"
			],
			"mathematica": [
				"Range[0, 19]! CoefficientList[ Series[E^(2*x/(1 - x)), {x, 0, 19}], x] (* _Zerinvary Lajos_, Mar 21 2007 *)",
				"Table[n!*LaguerreL[n, -1, -2], {n,0,30}] (* _G. C. Greubel_, Feb 23 2021 *)"
			],
			"program": [
				"(PARI) a=Vec(exp(2*x/(1-x)));for(n=2,#a-1,a[n+1]*=n!);a \\\\ _Charles R Greathouse IV_, Nov 20 2011",
				"(MAGMA) m:=25; R\u003cx\u003e:=PowerSeriesRing(Rationals(), m); b:=Coefficients(R!(Exp(2*x/(1 - x)))); [Factorial(n-1)*b[n]: n in [1..m]]; // _G. C. Greubel_, May 15 2018",
				"(Magma) [Factorial(n)*Evaluate(LaguerrePolynomial(n, -1), -2): n in [0..25]]; // _G. C. Greubel_, Feb 23 2021",
				"(Sage) [factorial(n)*gen_laguerre(n, -1, -2) for n in (0..25)] # _G. C. Greubel_, Feb 23 2021"
			],
			"xref": [
				"Row sums of A059110.",
				"Cf. A000262, A025168, A255806, A317364."
			],
			"keyword": "easy,nonn",
			"offset": "0,2",
			"author": "encyclopedia(AT)pommard.inria.fr, Jan 25 2000",
			"ext": [
				"New name using e.g.f., _Vaclav Kotesovec_, Feb 25 2014"
			],
			"references": 11,
			"revision": 61,
			"time": "2021-12-01T10:39:29-05:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}