{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A236333",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 236333,
			"data": "1,3,2,4,4,2,9,5,2,16,6,2,25,7,2,36,8,2,49,9,2,64,10,2,81,11,2,100,12,2,121,13,2,144,14,2,169,15,2,196,16,2,225,17,2,256,18,2,289,19,2,324,20,2,361,21,2,400,22,2,441,23,2,484,24,2,529,25,2,576,26,2,625,27,2,676,28,2,729,29,2",
			"name": "The (n-2)-th (n\u003e=3) triple of terms gives coefficients of double trinomial P_n(x) = ((n-2)^2*x^2 + n*x + 2)/2 (see comment).",
			"comment": [
				"Let {G_n(k)}_(k\u003e=0) be sequence of n-gonal numbers. Then G_n(P_n(k)) = G_n(P_n(k)-1) + G_n((n-2)*k+1)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A236333/b236333.txt\"\u003eTable of n, a(n) for n = 3..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,0,3,0,0,-3,0,0,1)."
			],
			"formula": [
				"If n==0 (mod 3), then a(n) = n^2/9;",
				"if n==1 (mod 3), then a(n) = (n+5)/3;",
				"if n==2 (mod 3), then a(n) = 2.",
				"G.f.: -x^3*(2*x^8+2*x^7-4*x^5-5*x^4+x^3+2*x^2+3*x+1) / ((x-1)^3*(x^2+x+1)^3). - _Colin Barker_, Jan 23 2014"
			],
			"example": [
				"Let n=5, k=4. Then G_5(k)=k*(3*k-1)/2 (Cf. A000326) and the double trinomial 2*P_5(x)= 9*x^2+5*x+2, P_5(4)=(9*4^2+5*4+2)/2=83,",
				"Thus, we have G_5(83)=G_5(82)+G_5(13), or 83*124 = 41*245 + 13*19 = 10292."
			],
			"mathematica": [
				"a[n_]:=Which[Mod[n,3]==0,n^2/9,Mod[n,3]==1,(n+5)/3,True,2]; Map[a,Range[3,103]]",
				"CoefficientList[Series[(-1-3 x-2 x^2-x^3+5 x^4+4 x^5-2 x^7-2 x^8)/((-1+x)^3 (1+x+x^2)^3),{x,0,100}],x]"
			],
			"program": [
				"(PARI) Vec(-x^3*(2*x^8+2*x^7-4*x^5-5*x^4+x^3+2*x^2+3*x+1)/((x-1)^3*(x^2+x+1)^3) + O(x^100)) \\\\ _Colin Barker_, Jan 23 2014",
				"(MAGMA) I:=[1,3,2,4,4,2,9,5,2]; [n le 9 select I[n] else 3*Self(n-3)-3*Self(n-6)+Self(n-9): n in [1..90]]; // _Vincenzo Librandi_, Feb 02 2014"
			],
			"xref": [
				"Cf. A000217, A000290, A000326, A000384, A000566, A000567, A001106, A001107, A051682, A051624, A051865-A051876."
			],
			"keyword": "nonn,easy",
			"offset": "3,2",
			"author": "_Vladimir Shevelev_, Jan 22 2014",
			"references": 1,
			"revision": 20,
			"time": "2015-06-13T00:54:57-04:00",
			"created": "2014-01-27T13:02:43-05:00"
		}
	]
}