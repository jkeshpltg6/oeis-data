{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A278120",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 278120,
			"data": "-1,1,5,25,1033,15745,1599895,12116675,1519810267,5730215335,2762191322225,155865304045375,275016025098532093,129172331662700995,358829725148742321475,524011363178245785875,10072731258491333905209253,1181576300858987307102335,68622390512340213600239902775",
			"name": "a(n) is numerator of rational z(n) associated with the non-orientable map asymptotics constant p((n+1)/2).",
			"link": [
				"Gheorghe Coserea, \u003ca href=\"/A278120/b278120.txt\"\u003eTable of n, a(n) for n = 0..201\u003c/a\u003e",
				"S. R. Carrell, \u003ca href=\"https://arxiv.org/abs/1406.1760v2\"\u003eThe Non-Orientable Map Asymptotics Constant pg\u003c/a\u003e, arXiv:1406.1760 [math.CO], 2014.",
				"Stavros Garoufalidis, Marcos Marino, \u003ca href=\"https://arxiv.org/abs/0812.1195v4\"\u003eUniversality and asymptotics of graph counting problems in nonorientable surfaces\u003c/a\u003e, arXiv:0812.1195 [math.CO], 2008."
			],
			"formula": [
				"a(n) = numerator(z(n)), where z(n) = 1/2 * (y(n/2)/3^(n/2) + (5*n-6)/6 * z(n-1) + Sum {k=1..n-1} z(k)*z(n-k)), with z(0) = -1, y(n) = A269418(n)/A269419(n) and y(n+1/2) = 0 for all n.",
				"p((n+1)/2) = 4 * (A278120(n)/A278121(n)) * (3/2)^((n+1)/2) / gamma((5*n-1)/4), where p((n+1)/2) is the non-orientable map asymptotics constant for type g=(n+1)/2 and gamma is the Gamma function."
			],
			"example": [
				"For n=2 we have p(3/2) = 4 * (5/144) * (3/2)^(3/2) / gamma(9/4) = 2/(sqrt(6)*gamma(1/4)).",
				"For n=4 we have p(5/2) = 4 * (1033/27648) * (3/2)^(5/2) / gamma(19/4) = 1033/(13860*sqrt(6)*gamma(3/4)).",
				"n   z(n)                   p((n+1)/2)",
				"0   -1                     3/(sqrt(6)*gamma(3/4))",
				"1   1/12                   1/2",
				"2   5/144                  2/(sqrt(6)*gamma(1/4))",
				"3   25/864                 5/(36*sqrt(Pi))",
				"4   1033/27684             1033/(13860*sqrt(6)*gamma(3/4))",
				"5   15745/248832           3149/442368",
				"6   1599895/11943936       319979/(18796050*sqrt(6)*gamma(1/4))",
				"7   12116675/35831808      484667/(560431872*sqrt(Pi))",
				"8   1519810267/1528823808  1519810267/(4258429005600*sqrt(6)*gamma(3/4))",
				"9   5730215335/1719926784  1146043067/41094783959040",
				"..."
			],
			"program": [
				"(PARI)",
				"A269418_seq(N) = {",
				"  my(y  = vector(N)); y[1] = 1/48;",
				"  for (n = 2, N,",
				"       y[n] = (25*(n-1)^2-1)/48 * y[n-1] + 1/2*sum(k = 1, n-1, y[k]*y[n-k]));",
				"  concat(-1, y);",
				"};",
				"seq(N) = {",
				"  my(y = A269418_seq(N), z = vector(N)); z[1] = 1/12;",
				"  for (n = 2, N,",
				"       my(t1 = if(n%2, 0, y[1+n\\2]/3^(n\\2)),",
				"          t2 = sum(k=1, n-1, z[k]*z[n-k]));",
				"      z[n] = (t1 + (5*n-6)/6 * z[n-1] + t2)/2);",
				"  concat(-1, z);",
				"};",
				"apply(numerator, seq(18))"
			],
			"xref": [
				"Cf. A269418, A269419, A278121 (denominator)."
			],
			"keyword": "sign,frac",
			"offset": "0,3",
			"author": "_Gheorghe Coserea_, Nov 12 2016",
			"references": 3,
			"revision": 32,
			"time": "2016-11-24T09:31:16-05:00",
			"created": "2016-11-24T09:31:16-05:00"
		}
	]
}