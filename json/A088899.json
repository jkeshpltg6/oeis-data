{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A088899",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 88899,
			"data": "4,4,4,4,4,4,4,4,4,4,4,4,4,4,12,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,12,4,4,4,4,12,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,12,4,4,4,4,4,4,4,4,4,4",
			"name": "T(n, k) = number of ordered pairs of integers (x,y) with x^2/n^2 + y^2/k^2 = 1, 1 \u003c= k \u003c= n; triangular array, read by rows.",
			"comment": [
				"T(n,k) is the number of lattice points on the circumference of an ellipse with semimajor axis = n, semiminor axis = k and center = (0,0)."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A088899/b088899.txt\"\u003eRows n = 1..225 of triangle, flattened\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Ellipse.html\"\u003eEllipse\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A088897(n) - A088898(n);",
				"T(n,n) = A046109(n)."
			],
			"example": [
				"From _Antti Karttunen_, Nov 08 2018: (Start)",
				"Triangle begins:",
				"---------------------------------------------------------------",
				"k=    1   2   3   4   5   6   7   8   9  10  11  12  13  14  15",
				"---------------------------------------------------------------",
				"n= 1: 4;",
				"n= 2: 4,  4;",
				"n= 3: 4,  4,  4;",
				"n= 4: 4,  4,  4,  4;",
				"n= 5: 4,  4,  4,  4, 12;",
				"n= 6: 4,  4,  4,  4,  4,  4;",
				"n= 7: 4,  4,  4,  4,  4,  4,  4;",
				"n= 8: 4,  4,  4,  4,  4,  4,  4,  4;",
				"n= 9: 4,  4,  4,  4,  4,  4,  4,  4,  4;",
				"n=10: 4,  4,  4,  4, 12,  4,  4,  4,  4, 12;",
				"n=11: 4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4;",
				"n=12: 4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4;",
				"n=13: 4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4, 12;",
				"n=14: 4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4;",
				"n=15: 4,  4,  4,  4, 12,  4,  4,  4,  4, 12,  4,  4,  4,  4, 12;",
				"---",
				"T(5,5) = 12 as there are following 12 solutions for pair (5,5): (5, 0), (4, 3), (3, 4), (0, 5), (-3, 4), (-4, 3), (-5, 0), (-4, -3), (-3, -4), (0, -5), (3, -4), (4, -3).",
				"T(15,10) = 12, as there are following 12 solutions for pair (15,10): (-15,0), (-12,-6), (-12,6), (-9,-8), (-9,8), (0,-10), (0,10), (9,-8), (9,8), (12,-6), (12,6), (15,0).",
				"(End)"
			],
			"mathematica": [
				"T[n_, k_] := Reduce[x^2/n^2 + y^2/k^2 == 1, {x, y}, Integers] // Length;",
				"Table[T[n, k], {n, 1, 14}, {k, 1, n}] // Flatten (* _Jean-François Alcover_, Sep 27 2021 *)"
			],
			"program": [
				"(PARI)",
				"up_to = 105;",
				"A088899tr(n,k) = { my(s=0, t=(n^2)*(k^2)); for(x=-n,n,for(y=-k,k,if((x*x*k*k)+(y*y*n*n) == t, s++))); (s); };",
				"A088899list(up_to) = { my(v = vector(up_to), i=0); for(n=1,oo, for(k=1,n, if(i++ \u003e up_to, return(v)); v[i] = A088899tr(n,k))); (v); };",
				"v088899 = A088899list(up_to);",
				"A088899(n) = v088899[n]; \\\\ _Antti Karttunen_, Nov 07 2018"
			],
			"xref": [
				"Cf. A046109, A088897, A088898."
			],
			"keyword": "nonn,tabl",
			"offset": "1,1",
			"author": "_Reinhard Zumkeller_, Oct 21 2003",
			"references": 3,
			"revision": 24,
			"time": "2021-09-27T07:58:13-04:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}