{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006983",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6983,
			"id": "M4482",
			"data": "0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,8,12,26,160,441,1152,3001,7901,20566,54541,144161,378197,990981,2578081,6674067,17086918",
			"name": "Number of simple perfect squared squares of order n up to symmetry.",
			"comment": [
				"A squared rectangle (which may be a square) is a rectangle dissected into a finite number of two or more squares. If no two squares have the same size, the squared rectangle is perfect. A squared rectangle is simple if it does not contain a smaller squared rectangle. The order of a squared rectangle is the number of constituent squares. - _Geoffrey H. Morley_, Oct 17 2012"
			],
			"reference": [
				"J.-P. Delahaye, Les inattendus mathematiques, Belin-Pour la Science, Paris, 2004, pp. 95-96.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Stuart E. Anderson, \u003ca href=\"http://www.squaring.net\"\u003ePerfect Squared Rectangles and Squared Squares\u003c/a\u003e",
				"Stuart E. Anderson, \u003ca href=\"/A006983/a006983.txt\"\u003eSimple perfect squared squares in orders 27 to 37 - methods used and people involved. \u003c/a\u003e",
				"C. J. Bouwkamp, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(92)90531-J\"\u003eOn some new simple perfect squared squares\u003c/a\u003e, Discrete Math. 106-107 (1992) 67-75.",
				"C. J. Bouwkamp and A. J. W. Duijvestijn, \u003ca href=\"http://alexandria.tue.nl/repository/books/391207.pdf\"\u003eCatalogue of Simple Perfect Squared Squares of orders 21 through 25\u003c/a\u003e, EUT Report 92-WSK-03, Eindhoven University of Technology, Eindhoven, The Netherlands, November 1992.",
				"C. J. Bouwkamp and A. J. W. Duijvestijn, \u003ca href=\"http://alexandria.tue.nl/repository/books/430534.pdf\"\u003eAlbum of Simple Perfect Squared Squares of order 26\u003c/a\u003e, EUT Report 94-WSK-02, Eindhoven University of Technology, Eindhoven, The Netherlands, December 1994.",
				"G. Brinkmann and B. D. McKay, \u003ca href=\"http://users.cecs.anu.edu.au/~bdm/papers/plantri-full.pdf\"\u003eFast generation of planar graphs\u003c/a\u003e, MATCH Commun. Math. Comput. Chem., 58 (2007), 323-357.",
				"Gunnar Brinkmann and Brendan McKay, \u003ca href=\"http://users.cecs.anu.edu.au/~bdm/plantri/\"\u003eplantri and fullgen\u003c/a\u003e programs for generation of certain types of planar graph.",
				"Gunnar Brinkmann and Brendan McKay, \u003ca href=\"/A000103/a000103_1.pdf\"\u003eplantri and fullgen\u003c/a\u003e programs for generation of certain types of planar graph [Cached copy, pdf file only, no active links, with permission]",
				"A. J. W. Duijvestijn, \u003ca href=\"/A006983/a006983.jpg\"\u003eIllustration for a(21)=1\u003c/a\u003e (The unique simple squared square of order 21. Reproduced with permission of the discoverer.)",
				"A. J. W. Duijvestijn, \u003ca href=\"http://doc.utwente.nl/17948/1/Duijvestijn93simple.pdf\"\u003eSimple perfect squared squares and 2x1 squared rectangles of orders 21 to 24\u003c/a\u003e, J. Combin. Theory Ser. B 59 (1993), 26-34.",
				"A. J. W. Duijvestijn, Simple perfect squared squares and 2x1 squared rectangles of order 25, Math. Comp. 62 (1994), 325-332. \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1994-1208220-9\"\u003edoi:10.1090/S0025-5718-1994-1208220-9\u003c/a\u003e",
				"A. J. W. Duijvestijn, Simple perfect squares and 2x1 squared rectangles of order 26, Math. Comp. 65 (1996), 1359-1364. \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-96-00705-3\"\u003edoi:10.1090/S0025-5718-96-00705-3\u003c/a\u003e [\u003ca href=\"http://www.squaring.net/downloads/TableI\"\u003eTableI List of Simple Perfect Squared Squares of order 26\u003c/a\u003e and \u003ca href=\"http://www.squaring.net/downloads/TableII\"\u003eTableII List of Simple Perfect Squared 2x1 Rectangles of order 26\u003c/a\u003e are now on squaring.net and no longer located as described in the paper.]",
				"I. Gambini, \u003ca href=\"http://alain.colmerauer.free.fr/alcol/ArchivesPublications/Gambini/carres.pdf\"\u003eQuant aux carrés carrelés\u003c/a\u003e, Thesis, Université de la Méditerranée Aix-Marseille II, 1999, p. 25.",
				"Ed Pegg Jr., \u003ca href=\"https://community.wolfram.com/groups/-/m/t/2044450\"\u003eAdvances in Squared Squares\u003c/a\u003e, Wolfram Community Bulletin, Jul 23 2020",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PerfectSquareDissection.html\"\u003ePerfect Square Dissection\u003c/a\u003e",
				"\u003ca href=\"/index/Sq#squared_squares\"\u003eIndex entries for squared squares\u003c/a\u003e"
			],
			"xref": [
				"Cf. A002962, A002881, A002839, A014530.",
				"Cf. A181340, A181735, A217155, A217156.",
				"Cf. A129947, A217149, A228953 (related to sizes of the squares).",
				"Cf. A349205, A349206, A349207, A349208, A349209, A349210 (related to ratios of element and square sizes)."
			],
			"keyword": "nonn,hard,more,nice",
			"offset": "1,22",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Leading term changed from 0 to 1, Apr 15 1996",
				"More terms from _Stuart E Anderson_, May 08 2003, Nov 2010",
				"Leading term changed back to 0, Dec 25 2010 (cf. A178688)",
				"a(29) added by _Stuart E Anderson_, Aug 22 2010; contributors to a(29) include _Ed Pegg Jr_ and Stephen Johnson",
				"a(29) changed to 7901, identified a duplicate tiling in order 29. - _Stuart E Anderson_, Jan 07 2012",
				"a(28) changed to 3000, identified a duplicate tiling in order 28. - _Stuart E Anderson_, Jan 14 2012",
				"a(28) changed back to 3001 after a complete recount of order 28 SPSS recalculated from c-nets with cleansed data, established the correct total of 3001. - _Stuart E Anderson_, Jan 24 2012",
				"Definition clarified by _Geoffrey H. Morley_, Oct 17 2012",
				"a(30) added by _Stuart E Anderson_, Apr 10 2013",
				"a(31), a(32) added by _Stuart E Anderson_, Sep 29 2013",
				"a(33), a(34) and a(35) added by _Stuart E Anderson_, May 02 2016",
				"Moved comments on orders 27 to 35 to a linked file.  _Stuart E Anderson_, May 02 2016",
				"a(36) and a(37) enumerated by Jim Williams, added by _Stuart E Anderson_, Jul 26 2020."
			],
			"references": 29,
			"revision": 171,
			"time": "2021-12-01T14:09:42-05:00",
			"created": "1994-04-28T03:00:00-04:00"
		}
	]
}