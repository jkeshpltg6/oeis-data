{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007723",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7723,
			"data": "1,1,2,1,2,2,1,2,3,2,1,2,4,4,2,1,2,5,8,5,2,1,2,6,15,16,6,2,1,2,7,26,52,32,7,2,1,2,8,42,152,203,64,8,2,1,2,9,64,392,1144,877,128,9,2,1,2,10,93,904,5345,10742,4140,256,10,2,1,2,11,130,1899,20926,102050,122772,21147",
			"name": "Triangle a(n,k) of number of M-sequences read by antidiagonals.",
			"reference": [
				"S. Linusson, The number of M-sequences and f-vectors, Combinatorica, 19 (1999), 255-266."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A007723/b007723.txt\"\u003eRows n = 0..50, flattened\u003c/a\u003e",
				"S. Linusson, \u003ca href=\"http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.51.3876\"\u003eThe number of M-sequences and f-vectors\u003c/a\u003e, Combinatorica, 19 (1999), 255-266."
			],
			"formula": [
				"a(p, n) = Sum_{k=-1..n} Lp(n, k) where Lp(n, k) satisfies the recurrence: Lp(n, k) = Sum_{i=k..n} L(p-1, n, i)*L(p, i-1, k-1) for p, n \u003e= 1, k \u003e= 0 with the boundary conditions: Lp(n, n) = Lp(n, -1) = 1 for all p \u003e= 1, n \u003e= -1; L0(n, n) = L0(n, -1) = 1 and L0(n, k) = 0 for k different from -1 or n. - Pab Ter (pabrlos2(AT)yahoo.com), Nov 10 2005"
			],
			"example": [
				"Triangle begins:",
				"  1;",
				"  1, 2;",
				"  1, 2,  2;",
				"  1, 2,  3,  2;",
				"  1, 2,  4,  4,   2;",
				"  1, 2,  5,  8,   5,    2;",
				"  1, 2,  6, 15,  16,    6,     2;",
				"  1, 2,  7, 26,  52,   32,     7,    2;",
				"  1, 2,  8, 42, 152,  203,    64,    8,   2;",
				"  1, 2,  9, 64, 392, 1144,   877,  128,   9,  2;",
				"  1, 2, 10, 93, 904, 5345, 10742, 4140, 256, 10, 2;",
				"  ... - _Vincenzo Librandi_, Jul 24 2013"
			],
			"maple": [
				"L:=proc(p,n,k) options remember: local i: if (k=-1 or k=n) and n\u003e=-1 and p\u003e=1 then RETURN(1) elif p=0 and (k=-1 or k=n) then RETURN(1) elif p=0 and (k\u003c\u003e-1 and k\u003c\u003en) then RETURN(0) elif p\u003e=1 and n\u003e=1 then RETURN(add(L(p-1,n,i)*L(p,i-1,k-1),i=k..n)) fi: end; M:=(p,n)-\u003eadd(L(p,n,k),k=-1..n); seq(seq(M(n-i+1,i-1),i=0..n+1),n=-1..12); # first method (Pab Ter)",
				"L:=proc(p,n,k) options remember: local i: if (k=-1 or k=n) and n\u003e=-1 and p\u003e=1 then RETURN(1) elif p=0 and (k=-1 or k=n) then RETURN(1) elif p=0 and (k\u003c\u003e-1 and k\u003c\u003en) then RETURN(0) elif p\u003e=1 and n\u003e=1 then RETURN(add(L(p-1,n,i)*L(p,i-1,k-1),i=k..n)) fi: end; M:=proc(p,n) options remember: local i: if n\u003c1 and n\u003e-2 and p\u003e=0 then RETURN([1,2][n+2]) elif p=0 and n\u003e=0 then RETURN(2) elif p\u003e=1 and n\u003e=1 then RETURN(1+add(L(p-1,n,i)*M(p,i-1),i=0..n)) fi: end; seq(seq(M(n-i+1,i-1),i=0..n+1),n=-1..12); # 2nd method (Pab Ter)"
			],
			"mathematica": [
				"a[p_, n_] := Sum[ lp[p, n, k], {k, -1, n}]; lp[p_ /; p \u003e= 1, n_ /; n \u003e= 1, k_ /; k \u003e= 0] := lp[p, n, k] = Sum[ lp[p-1, n, i] lp[p, i-1, k-1], {i, k, n}]; lp[p_ /; p \u003e= 1, n_ /; n \u003e= -1, n_ /; n \u003e= -1] := lp[p, n, n] = 1; lp[p_ /; p \u003e= 1, n_ /; n \u003e= -1, -1] := lp[p, n, -1] = 1; lp[0, n_, n_] := lp[0, n, n] = 1; lp[0, n_, -1] := lp[0, n, -1] = 1; lp[0, n_, k_] /; k != -1 \u0026\u0026 k != n := lp[0, n, k] = 0; m[p_, n_] := m[p, n] = Sum[ lp[p, n, k], {k, -1, n}]; row[n_] := Table[ m[n-i, i-1], {i, 0, n}]; Flatten[ Table[ row[n], {n, 0, 11}]] (* _Jean-François Alcover_, Dec 16 2011, after Pab Ter *)"
			],
			"xref": [
				"Cf. A003659, A011819, A011820, etc.",
				"Cf. A007065, A007625."
			],
			"keyword": "nonn,nice,easy,tabl",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from Pab Ter (pabrlos2(AT)yahoo.com), Nov 10 2005"
			],
			"references": 1,
			"revision": 29,
			"time": "2020-09-16T06:31:47-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}