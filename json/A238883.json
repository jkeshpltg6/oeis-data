{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A238883",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 238883,
			"data": "1,2,3,4,1,4,3,8,1,2,10,3,2,14,5,2,1,20,3,4,2,1,30,3,2,1,6,36,13,2,3,2,52,10,4,6,3,2,70,9,9,4,6,3,94,16,6,5,10,2,2,122,24,4,8,1,12,2,2,1,160,33,4,12,6,4,9,2,1,206,37,18,14,6,2,6,8",
			"name": "Array:  row n gives number of times each upper triangular partition U(p) occurs as p ranges through the partitions of n.",
			"comment": [
				"Suppose that p is a partition.  Let u, v, w be the number of 1's above, on, and below the principal antidiagonal, respectively, of the Ferrers matrix of p defined at A237981.  The upper triangular partition of p, denoted by U(p), is {u,v} if w = 0 and {u,v,w} otherwise.  In row n, the counted partitions are taken in Mathematica order (i.e., reverse lexicographic).  A000041 = sum of numbers in row n, and A238884(n) = (number of numbers in row n) = number of upper triangular partitions of n."
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A238883/b238883.txt\"\u003eTable of n, a(n) for n = 1..300\u003c/a\u003e",
				"Clark Kimberling and Peter J. C. Moses, \u003ca href=\"http://faculty.evansville.edu/ck6/GalleryThree/Introduction3.html\"\u003eFerrers Matrices and Related Partitions of Integers\u003c/a\u003e"
			],
			"example": [
				"First 12 rows:",
				"1",
				"2",
				"3",
				"4 .. 1",
				"4 .. 3",
				"8 .. 1 .. 2",
				"10 . 3 .. 2",
				"14 . 5 .. 2 .. 1",
				"20 . 3 .. 4 .. 2 .. 1",
				"30 . 3 .. 2 .. 1 .. 6",
				"36 . 13 . 2 .. 3 .. 2",
				"52 . 10 . 4 .. 6 .. 3 .. 2",
				"Row 6 arises as follows:  there are 3 upper triangular (UT) partitions:  51, 33, 321, of which 51 is produced from the 8 partitions  6, 51, 42, 411, 3111, 2211, 21111, and 111111, while the UT partition 33 is produced from the single partition 321, and the only other UT partition of 6, namely 321, is produced from the partitions 33 and 222.  (For example, the rows of the Ferrers matrix of 222 are (1,1,0), (1,1,0), (1,1,0), with principal antidiagonal (0,1,1), so that u = 3, v = 2, w = 1.)"
			],
			"mathematica": [
				"ferrersMatrix[list_] := PadRight[Map[Table[1, {#}] \u0026, #], {#, #} \u0026[Max[#, Length[#]]]] \u0026[list]; ut[list_] := Select[Map[Total[Flatten[#]] \u0026, {LowerTriangularize[#, -1], Diagonal[#], UpperTriangularize[#, 1]}] \u0026[Reverse[ferrersMatrix[list]]], # \u003e 0 \u0026];",
				"t[n_] := #[[Reverse[Ordering[PadRight[Map[First[#] \u0026, #]]]]]] \u0026[  Tally[Map[Reverse[Sort[#]] \u0026, Map[ut, IntegerPartitions[n]]]]]",
				"u[n_] := Table[t[n][[k]][[1]], {k, 1, Length[t[n]]}]; v[n_] := Table[t[n][[k]][[2]], {k, 1, Length[t[n]]}]; TableForm[Table[t[n], {n, 1, 12}]]",
				"z = 20; Table[Flatten[u[n]], {n, 1, z}]",
				"Flatten[Table[u[n], {n, 1, z}]]",
				"Table[v[n], {n, 1, z}]",
				"Flatten[Table[v[n], {n, 1, z}]] (* A238883 *)",
				"Table[Length[v[n]], {n, 1, z}]  (* A238884 *)",
				"(* _Peter J. C. Moses_, Mar 04 2014 *)"
			],
			"xref": [
				"Cf. A238884, A238885, A238886, A237981, A000041."
			],
			"keyword": "nonn,tabf,easy",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Mar 06 2014",
			"references": 4,
			"revision": 11,
			"time": "2014-03-14T00:03:56-04:00",
			"created": "2014-03-14T00:03:56-04:00"
		}
	]
}