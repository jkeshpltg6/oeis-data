{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A176898",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 176898,
			"data": "5,231,14586,1062347,84021990,7012604550,607892634420,54200780036595,4938927219474990,457909109348466930,43057935618181929900,4096531994713828810686,393617202432246696493436,38142088615983865845923052,3723160004902167033863327592",
			"name": "a(n) = binomial(6*n, 3*n)*binomial(3*n, n)/(2*(2*n+1)*binomial(2*n, n)).",
			"comment": [
				"During April 26-28, 2010, _Zhi-Wei Sun_ introduced this new sequence and proved that a(n) = binomial(6n,3n)*binomial(3n,n)/(2*(2n+1)*binomial(2n,n)) is a positive integer for every n=1,2,3,... He also observed that a(n) is odd if and only if n is a power of two, and that 3a(n)=0 (mod 2n+3). By Stirling's formula, we have lim_n (8n*sqrt(n*Pi)a(n)/108^n) = 1. It is interesting to find a combinatorial interpretation or recursion for the sequence.",
				"From _Tatiana Hessami Pilehrood_, Dec 01 2015: (Start)",
				"_Zhi-Wei Sun_ formulated two conjectures concerning a(n) (see Conjectures 1.1 and 1.2 in Z.-W. Sun, \"Products and sums divisible by central binomial coefficients\" and Conjecture A89 in \"Open conjectures on congruences\"). The first conjecture states that Sum_{n=1..p-1} a(n)/(108^n) is congruent to 0 or -1 modulo a prime p \u003e 3 depending on whether p is congruent to +-1 or +-5 modulo 12, respectively.",
				"The second conjecture asks about an exact formula for a companion sequence of a(n). Both conjectures as well as many numerical congruences involving a(n) and (2n+1)a(n) were solved by Kh. Hessami Pilehrood and T. Hessami Pilehrood, see the link below. (End)"
			],
			"link": [
				"Indranil Ghosh, \u003ca href=\"/A176898/b176898.txt\"\u003eTable of n, a(n) for n = 1..450\u003c/a\u003e",
				"Kh. Hessami Pilehrood, T. Hessami Pilehrood, \u003ca href=\"http://arxiv.org/abs/1504.07944\"\u003eJacobi polynomials and congruences involving some higher-order Catalan numbers and binomial coefficients\u003c/a\u003e, preprint, arXiv:1504.07944 [math.NT], 2015.",
				"K. H. Pilehrood, T. H. Pilehrood, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL18/Pilehrood/pile5.html\"\u003eJacobi Polynomials and Congruences Involving Some Higher-Order Catalan Numbers and Binomial Coefficients\u003c/a\u003e, J. Int. Seq. 18 (2015) 15.11.7",
				"M. R. Sepanski, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v21i1p32\"\u003eOn Divisibility of Convolutions of Central Binomial Coefficients\u003c/a\u003e, Electronic Journal of Combinatorics, 21 (1) 2014, #P1.32.",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1004.4623\"\u003eProducts and sums divisible by central binomial coefficients\u003c/a\u003e, preprint, arXiv:1004.4623 [math.NT], 2010.",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/0911.5665\"\u003eOpen conjectures on congruences\u003c/a\u003e, preprint, arXiv:0911.5665 [math.NT], 2009-2011.",
				"Brian Y. Sun, J. X. Meng, \u003ca href=\"https://arxiv.org/abs/1606.08153\"\u003eProof of a Conjecture of Z.-W. Sun on Trigonometric Series\u003c/a\u003e, arXiv preprint arXiv:1606.08153 [math.CO], 2016."
			],
			"formula": [
				"G.f.: (1-6*s)/((12*s-1)*(8*s-2)) - 1/2, where x+(3*s-1)*(12*s-1)^2*s*(4*s-1)^2 = 0. - _Mark van Hoeij_, May 06 2013"
			],
			"example": [
				"For n=2 we have a(2) = binomial(12,6)*binomial(6,2)/(2*(2*2+1)*binomial(4,2)) = 231."
			],
			"maple": [
				"ogf := eval((1-6*s)/((12*s-1)*(8*s-2)) - 1/2, s=RootOf(x+(3*s-1)*(12*s-1)^2*s*(4*s-1)^2,s));",
				"series(ogf,x=0,30); # _Mark van Hoeij_, May 06 2013"
			],
			"mathematica": [
				"S[n_]:=Binomial[6n,3n]Binomial[3n,n]/(2(2n+1)Binomial[2n,n]) Table[S[n],{n,1,50}]"
			],
			"program": [
				"(MAGMA) [Binomial(6*n, 3*n)*Binomial(3*n, n)/(2*(2*n+1)*Binomial(2*n, n)): n in [1..15]]; // _Vincenzo Librandi_, Dec 02 2015",
				"(PARI) a(n) = binomial(6*n, 3*n) * binomial(3*n, n) / (2*(2*n+1) * binomial(2*n, n)); \\\\ _Indranil Ghosh_, Mar 05 2017",
				"(Python)",
				"import math",
				"f=math.factorial",
				"def C(n,r): return f(n)/f(r)/f(n-r)",
				"def A176898(n): return C(6*n, 3*n) * C(3*n, n) / (2*(2*n+1) * C(2*n, n)) # _Indranil Ghosh_, Mar 05 2017"
			],
			"xref": [
				"Cf. A000984, A173774, A176285, A176477."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Zhi-Wei Sun_, Apr 28 2010",
			"references": 2,
			"revision": 44,
			"time": "2018-06-09T11:25:58-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}