{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A181648",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 181648,
			"data": "1,1,1,2,2,3,1,2,3,2,4,3,3,3,4,3,2,2,6,5,3,5,3,5,4,5,3,4,5,4,5,4,5,7,6,7,3,3,7,4,8,4,4,5,7,6,5,6,7,8,6,4,6,9,6,8,6,4,4,4,11,7,4,11,4,9,6,7,8,7,11,5,5,8,8,10,6,5,10,6,8,6,7,7,8",
			"name": "Expansion of x^(-2/3) * psi(x) * c(x^2) / 3 in powers of x where psi() is a Ramanujan theta function and c() is a cubic AGM theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A181648/b181648.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(-19/24) * eta(q^2) * eta(q^6)^3 / eta(q) in powers of q.",
				"Euler transform of period 6 sequence [ 1, 0, 1, 0, 1, -3, ...].",
				"3 * a(n) = A008443(3*n + 2)."
			],
			"example": [
				"1 + x + x^2 + 2*x^3 + 2*x^4 + 3*x^5 + x^6 + 2*x^7 + 3*x^8 + 2*x^9 + 4*x^10 + ...",
				"q^19 + q^43 + q^67 + 2*q^91 + 2*q^115 + 3*q^139 + q^163 + 2*q^187 + 3*q^211 + ..."
			],
			"mathematica": [
				"A181648[n_]:= SeriesCoefficient[QPochhammer[q^2, q^2]*QPochhammer[q^6, q^6]^3/QPochhammer[q, q], {q, 0, n}]; Table[A181648[n], {n, 0, 50}] (* _G. C. Greubel_, Dec 24 2017 *)"
			],
			"program": [
				"(PARI) {a(n) = local(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^+2 + A) * eta(x^6 + A)^3 / eta(x + A), n))}"
			],
			"xref": [
				"Cf. A008443."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_Michael Somos_, Jun 22 2012",
			"references": 4,
			"revision": 24,
			"time": "2021-03-12T22:24:46-05:00",
			"created": "2012-06-23T04:18:59-04:00"
		}
	]
}