{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A060041",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 60041,
			"data": "5,2875,609250,317206375,242467530000,229305888887625,248249742118022000,295091050570845659250,375632160937476603550000,503840510416985243645106250,704288164978454686113488249750,1017913203569692432490203659468875,1512323901934139334751675234074638000",
			"name": "Certain numbers a(n) related to Gromov-Witten invariants N_n in dimension n (see formula (7.45) on p. 202 of Cox and Katz).",
			"comment": [
				"These integers are actually instanton numbers (or PBS states degeneracies). - Daniel Grunberg (grunberg(AT)mpim-bonn.mpg.de), Aug 18 2004.",
				"Equal to the number of degree-n rational curves on a general quintic for n \u003c= 9, but (if Clemens's conjecture is true) not for n = 10 (see A076912)."
			],
			"reference": [
				"J. Bertin and C. Peters, Variations of Hodge structure ..., pp. 151-232 of J. Bertin et al., eds., Introduction to Hodge Theory, Amer. Math. Soc. and Soc. Math. France, 2002; see p. 220.",
				"D. A. Cox and S. Katz, Mirror Symmetry and Algebraic Geometry, Amer. Math. Soc., 1999."
			],
			"link": [
				"T. D. Noe and Gheorghe Coserea, \u003ca href=\"/A060041/b060041.txt\"\u003eTable of n, a(n) for n = 0..301\u003c/a\u003e, first 101 terms from T. D. Noe.",
				"V. Batyrev, \u003ca href=\"http://www.ams.org/journal-getitem?pii=S0273-0979-00-00875-2\"\u003eReview of \"Mirror Symmetry and Algebraic Geometry\"\u003c/a\u003e, by D. A. Cox and S. Katz, Bull. Amer. Math. Soc., 37 (No. 4, 2000), 473-476.",
				"P. Candelas et al., \u003ca href=\"http://dx.doi.org/10.1016/0550-3213(91)90292-6\"\u003eA pair of Calabi-yau manifolds as an exactly soluble superconformal theory\u003c/a\u003e, Nuclear Phys. B 359 (1991), 21-74.",
				"R. H. Dijkgraaf, \u003ca href=\"http://www.bourbaphy.fr/dijkgraaf.pdf\"\u003eThe Mathematics of String Theory\u003c/a\u003e, pp. 58ff in \"Aspects De La Physique En 2005: Einstein 1905-2005\", Numéro special de la Gazette des mathématiciens. Supplément au no. 106, Oct 2005, Société Mathématique de France, Paris.",
				"Steven R. Finch, \u003ca href=\"/A013587/a013587.pdf\"\u003eEnumerative geometry\u003c/a\u003e, February 24, 2014. [Cached copy, with permission of the author]",
				"Trygve Johnsen and Steven L. Kleiman, \u003ca href=\"http://arxiv.org/abs/alg-geom/9510015\"\u003eRational curves of degree at most 9 on a general quintic threefold\u003c/a\u003e, arXiv:alg-geom/9510015, 1995.",
				"Trygve Johnsen and Steven L. Kleiman, \u003ca href=\"http://arxiv.org/abs/alg-geom/9601024\"\u003eToward Clemens' Conjecture in degrees between 10 and 24\u003c/a\u003e, arXiv:alg-geom/9601024, 1996.",
				"B. Mazur, \u003ca href=\"http://dx.doi.org/10.1090/S0273-0979-04-01024-9\"\u003ePerturbations, deformations and variations ...\u003c/a\u003e, Bull. Amer. Math. Soc., 41 (2004), 307-336.",
				"David R. Morrison, \u003ca href=\"http://arXiv.org/abs/alg-geom/9609021\"\u003eMathematical Aspects of Mirror Symmetry\u003c/a\u003e, arXiv:alg-geom/9609021, 1996; in Complex Algebraic Geometry (J. Kollár, ed.), IAS/Park City Math. Series, vol. 3, 1997, pp. 265-340.",
				"R. Pandharipande, \u003ca href=\"http://www.numdam.org/book-part/SB_1997-1998__40__307_0/\"\u003eRational curves on hypersurfaces (after A. Givental)\u003c/a\u003e, Séminaire Bourbaki, Vol. 1997/98. Astérisque No. 252 (1998), Exp. No. 848, 5, 307-340."
			],
			"example": [
				"G.f. = 5 + 2875*x + 609250*x^2 + 317206375*x^3 + 242467530000*x^4 + ..."
			],
			"mathematica": [
				"nn=20; y0[x_]:=Sum[(5n)!/(n!)^5 x^n, {n, 0, nn}]; y1[x_]:=Sum[((5n)!/(n!)^5 5 Sum[1/j, {j, n+1, 5n}]) x^n, {n, 0, nn}]; qq=Series[x Exp[y1[x]/y0[x]], {x, 0, nn}]; x[q_]=InverseSeries[qq, q]; s1=(q/x[q] D[x[q], q])^3 5/((1-5^5 x[q]) y0[x[q]]^2); s2=Series[5+Sum[n[d] d^3 q^d/(1-q^d), {d, 1, nn}], {q, 0, nn}]; sol=Solve[s1==s2]; t=Table[n[d]/.sol, {d, 1, nn}]//Flatten; (* Daniel Grunberg (grunberg(AT)mpim-bonn.mpg.de), Aug 18 2004 *)"
			],
			"program": [
				"(PARI) {a(n) = local(A1, A2, A3); if( n\u003c1, 5*(n==0), A1 = sum( k=0, n, (5*k)! / k!^5 * (-x)^k, x * O(x^n)); A2 = -x * exp(5 / A1 * sum( k=0, n, (sum( i=1, 5*k, 1/i) - sum( i=1, k, 1/i)) * (5*k)! / k!^5 * (-x)^k, x * O(x^n))); A3 = subst(5 / A1^2 / (1 + 5^5*x) / (x * A2'/A2)^3, x, serreverse(A2)); sumdiv( n, k, moebius(n / k) * polcoeff(A3, k))/n^3)}; /* _Michael Somos_, Mar 27 2004 */",
				"(PARI)",
				"cumsum(v) = for(i=2, #v, v[i] += v[i-1]); v;",
				"A060345_list(N) = {",
				"  my(x = 'x + O('x^(N+1)), h = cumsum(vector(5*N, n, 1/n)),",
				"     y0 = sum(n=0, N, (5*n)!/n!^5 * x^n),",
				"     y1 = 5 * sum(n = 1, N, ((5*n)!/n!^5 * (h[5*n] - h[n])) * x^n),",
				"     Qx = x * exp(y1/y0), Xq = serreverse(Qx));",
				"  Vec(5 * (x * Xq'/Xq)^3 / ((1 - 3125*Xq) * sqr(subst(y0, 'x, Xq))));",
				"};",
				"seq(N) = {",
				"  my(v1 = A060345_list(N+1),",
				"     v2 = dirmul(vector(N, n, moebius(n)), vector(N, n, v1[n+1])));",
				"  concat(5, vector(#v2, n, v2[n]/n^3));",
				"};",
				"seq(20)  \\\\ _Gheorghe Coserea_, Jul 28 2016"
			],
			"xref": [
				"Cf. A060345, A076912."
			],
			"keyword": "nonn,nice",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_, Mar 19 2001",
			"references": 8,
			"revision": 47,
			"time": "2018-11-28T03:37:50-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}