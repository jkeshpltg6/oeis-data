{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A307407",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 307407,
			"data": "16,4,5,1,10,2,3,40,12,13,64,20,21,88,28,29,9,58,112,36,37,136,44,45,160,52,53,17,106,34,35,11,70,22,23,7,46,14,15,184,60,61,208,68,69,232,76,77,25,154,50,51,256,84,85,280,92,93",
			"name": "Irregular table read by rows: rows list terms that map to the nodes in the graph of the \"3x+1\" (or Collatz) problem.",
			"comment": [
				"The construction is similar to that in A322469. The sequence is the flattened form of an irregular table S(i, j) (see the example below) which has rows i \u003e= 1 consisting of subsequences of varying length.",
				"Like Truemper (cf. link), we denote the mapping x -\u003e 2*x by \"m\" (\"multiply\"), the mapping x -\u003e (x - 1)/3 by \"d\" (\"divide\"), and the combined mapping \"dm\" x -\u003e (x - 1)/3 * 2 by \"s\" (\"squeeze\").  The d mapping is defined only if it is possible, that is, if x - 1 is divisible by 3. We write m, d and s as infix operation words, for example \"4 mms 10\", and we use exponents for repeated operations, for example \"mms^2 = mmss\".",
				"Row i in table S is constructed by the following algorithm: Start with 6 * i - 2 in column j = 1. The following columns j are defined in groups of four by the operations:",
				"  k   j=4*k+2    j=4*k+3    j=4*k+4    j=4*k+5",
				"  --------------------------------------------------",
				"  0   mm         dmm        mmd        dmmd",
				"  1   mms        dmms       mmsd       dmmsd",
				"  2   mms^2      dmms^2     mms^2d     dmms^2d",
				"  ...",
				"  k   mms^k      dmms^k     mm(s^k)d   dmm(s^k)d",
				"The construction for the row terminates at the first column where a d operation is no longer possible. This point is always reached. This can be proved by the observation that, for any row i in S, there is a unique mapping x -\u003e (x + 2)/6 of the terms in column 1, 2, 5, 9, 13, ... 4*m+1 to the terms in row i of table T in A322469. The row construction process in A322469 stops, therefore it stops also in the sequence defined here.",
				"Conjecture: The sequence is a permutation of the positive numbers."
			],
			"link": [
				"Georg Fischer, \u003ca href=\"https://github.com/gfis/fasces/blob/master/oeis/collatz/segment.pl\"\u003ePerl program for the generation of related sequences\u003c/a\u003e.",
				"Manfred Trümper, \u003ca href=\"http://dx.doi.org/10.1155/2014/756917\"\u003eThe Collatz Problem in the Light of an Infinite Free Semigroup\u003c/a\u003e, Chinese Journal of Mathematics, Vol. 2014, Article ID 756917, 21 pages."
			],
			"example": [
				"Table S(i, j) begins:",
				"  i\\j    1   2   3   4   5   6   7   8   9  10  11  12  13  14  15",
				"  ----------------------------------------------------------------",
				"  1:    16   4   5   1  10   2   3",
				"  2:    40  12  13",
				"  3:    64  20  21",
				"  4:    88  28  29   9  58",
				"  5:   112  36  37",
				"  6:   136  44  45",
				"  7:   160  52  53  17 106  34  35  11  70  22  23   7  46  14  15",
				"  8:   184  60  61"
			],
			"program": [
				"(Perl) cf. link."
			],
			"xref": [
				"Cf. A160016 (level 3), A307048 (level 2), A322469 (level 1)."
			],
			"keyword": "nonn,easy,tabf",
			"offset": "1,1",
			"author": "_Georg Fischer_, Apr 14 2019",
			"references": 3,
			"revision": 14,
			"time": "2021-06-04T10:51:52-04:00",
			"created": "2019-05-09T10:09:57-04:00"
		}
	]
}