{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A151906",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 151906,
			"data": "0,1,4,4,4,12,4,4,12,12,12,36,4,4,12,12,12,36,12,12,36,36,36,108,4,4,12,12,12,36,12,12,36,36,36,108,12,12,36,36,36,108,36,36,108,108,108,324,4,4,12,12,12,36,12,12,36,36,36,108,12,12,36,36,36,108,36,36,108,108,108",
			"name": "a(0) = 0, a(1) = 1; for n\u003e1, a(n) = 8*A151905(n) + 4.",
			"comment": [
				"Consider the Holladay-Ulam CA shown in Fig. 2 and Example 2 of the Ulam article. Then a(n) is the number of cells turned ON in generation n."
			],
			"reference": [
				"S. Ulam, On some mathematical problems connected with patterns of growth of figures, pp. 215-224 of R. E. Bellman, ed., Mathematical Problems in the Biological Sciences, Proc. Sympos. Applied Math., Vol. 14, Amer. Math. Soc., 1962."
			],
			"link": [
				"David Applegate, \u003ca href=\"/A139250/a139250.anim.html\"\u003eThe movie version\u003c/a\u003e",
				"David Applegate, Omar E. Pol and N. J. A. Sloane, \u003ca href=\"/A000695/a000695_1.pdf\"\u003eThe Toothpick Sequence and Other Sequences from Cellular Automata\u003c/a\u003e, Congressus Numerantium, Vol. 206 (2010), 157-191. [There is a typo in Theorem 6: (13) should read u(n) = 4.3^(wt(n-1)-1) for n \u003e= 2.]",
				"N. J. A. Sloane, \u003ca href=\"/wiki/Catalog_of_Toothpick_and_CA_Sequences_in_OEIS\"\u003eCatalog of Toothpick and Cellular Automata Sequences in the OEIS\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/A151907/a151907.jpg\"\u003eIllustration of initial terms\u003c/a\u003e (annotated copy of figure on p. 222 of Ulam)"
			],
			"formula": [
				"The three trisections are essentially A147582, A147582 and 3*A147582 respectively. More precisely, For t \u003e= 1, a(3t) = a(3t+1) = A147582(t+1) = 4*3^(wt(t)-1), a(3t+2) = 4*A147582(t+1) = 4*3^wt(t). See A151907 for explanation."
			],
			"example": [
				"From _Omar E. Pol_, Apr 02 2018: (Start)",
				"Note that this sequence also can be written as an irregular triangle read by rows in which the row lengths are the terms of A011782 multiplied by 3, as shown below:",
				"0,1, 4;",
				"4,4,12;",
				"4,4,12,12,12,36;",
				"4,4,12,12,12,36,12,12,36,36,36,108;",
				"4,4,12,12,12,36,12,12,36,36,36,108,12,12,36,36,36,108,36,36,108,108,108,324;",
				"4,4,12,12,12,36,12,12,36,36,36,108,12,12,36,36,36,108,36,36,108,108,108,... (End)"
			],
			"maple": [
				"f := proc(n) local j; j:=n mod 6; if (j\u003c=1) then 0 elif (j\u003c=4) then 1 else 2; fi; end;",
				"wt := proc(n) local w,m,i; w := 0; m := n; while m \u003e 0 do i := m mod 2; w := w+i; m := (m-i)/2; od; w; end;",
				"A151904 := proc(n) local k,j; k:=floor(n/6); j:=n-6*k; (3^(wt(k)+f(j))-1)/2; end;",
				"A151905 := proc (n) local k,j;",
				"if (n=0) then 0;",
				"elif (n=1) then 1;",
				"elif (n=2) then 0;",
				"else k:=floor( log(n/3)/log(2) ); j:=n-3*2^k; A151904(j); fi;",
				"end;",
				"A151906 := proc(n);",
				"if (n=0) then 0;",
				"elif (n=1) then 1;",
				"else 8*A151905(n) + 4;",
				"fi;",
				"end;"
			],
			"xref": [
				"Cf. A151904, A151905, A151907, A139250, A151895, A151896."
			],
			"keyword": "nonn,tabf",
			"offset": "0,3",
			"author": "_David Applegate_ and _N. J. A. Sloane_, Jul 31 2009, Aug 03 2009",
			"references": 8,
			"revision": 18,
			"time": "2021-02-24T02:48:18-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}