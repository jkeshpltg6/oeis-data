{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A216652",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 216652,
			"data": "1,1,1,2,1,2,1,4,1,4,6,1,6,6,1,6,12,1,8,18,1,8,24,24,1,10,30,24,1,10,42,48,1,12,48,72,1,12,60,120,1,14,72,144,120,1,14,84,216,120,1,16,96,264,240,1,16,114,360,360,1,18,126,432,600,1,18,144,552,840",
			"name": "Triangular array read by rows: T(n,k) is the number of compositions of n into exactly k distinct parts.",
			"comment": [
				"Same as A072574, with zeros dropped. [_Joerg Arndt_, Oct 20 2012]",
				"Row sums = A032020.",
				"Row n contains A003056(n) = floor((sqrt(8*n+1)-1)/2) terms (number of terms increases by one at each triangular number)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A216652/b216652.txt\"\u003eRows n = 1..500, flattened\u003c/a\u003e",
				"B. Richmond and A. Knopfmacher, \u003ca href=\"http://dx.doi.org/10.1007/BF01827930\"\u003eCompositions with distinct parts\u003c/a\u003e, Aequationes Mathematicae 49 (1995), pp. 86-97."
			],
			"formula": [
				"G.f.: Sum_{i\u003e=0} Product_{j=1..i} y*j*x^j/(1-x^j).",
				"T(n,k) = A008289(n,k)*k!."
			],
			"example": [
				"Triangle starts:",
				"[ 1]  1;",
				"[ 2]  1;",
				"[ 3]  1, 2;",
				"[ 4]  1, 2;",
				"[ 5]  1, 4;",
				"[ 6]  1, 4, 6;",
				"[ 7]  1, 6, 6;",
				"[ 8]  1, 6, 12;",
				"[ 9]  1, 8, 18;",
				"[10]  1, 8, 24, 24;",
				"[11]  1, 10, 30, 24;",
				"[12]  1, 10, 42, 48;",
				"[13]  1, 12, 48, 72;",
				"[14]  1, 12, 60, 120;",
				"[15]  1, 14, 72, 144, 120;",
				"[16]  1, 14, 84, 216, 120;",
				"[17]  1, 16, 96, 264, 240;",
				"[18]  1, 16, 114, 360, 360;",
				"[19]  1, 18, 126, 432, 600;",
				"[20]  1, 18, 144, 552, 840;",
				"T(5,2) = 4 because we have: 4+1, 1+4, 3+2, 2+3."
			],
			"maple": [
				"b:= proc(n, k) option remember; `if`(n\u003c0, 0, `if`(n=0, 1,",
				"      `if`(k\u003c1, 0, b(n, k-1) +b(n-k, k))))",
				"    end:",
				"T:= (n, k)-\u003e b(n-k*(k+1)/2, k)*k!:",
				"seq(seq(T(n, k), k=1..floor((sqrt(8*n+1)-1)/2)), n=1..24);  # _Alois P. Heinz_, Sep 12 2012"
			],
			"mathematica": [
				"nn=20;f[list_]:=Select[list,#\u003e0\u0026];Map[f,Drop[CoefficientList[Series[ Sum[Product[j y x^j/(1-x^j),{j,1,k}],{k,0,nn}],{x,0,nn}],{x,y}],1]]//Flatten"
			],
			"xref": [
				"Cf. A003056, A008289, A072574, A097910."
			],
			"keyword": "nonn,tabf",
			"offset": "1,4",
			"author": "_Geoffrey Critzer_, Sep 12 2012",
			"references": 10,
			"revision": 17,
			"time": "2020-07-24T21:59:50-04:00",
			"created": "2012-09-12T20:59:13-04:00"
		}
	]
}