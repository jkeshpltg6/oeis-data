{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A331394",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 331394,
			"data": "1,1,1,2,3,5,6,7,11,16,19,25,38,51,63,88,127,165,214,303,419,544,731,1025,1382,1819,2487,3432,4583,6125,8406,11447,15291,20656,28259,38185,51238,69571,94703,127608,172047,233845",
			"name": "Number of ways of 4-coloring the Fibonacci square spiral tiling of n squares with colors introduced in order.",
			"comment": [
				"The Fibonacci square spiral tiling is the pattern formed by tiling the plane using squares with side-lengths of successive Fibonacci numbers (so the k-th square is of size F(k)), in a spiral pattern.",
				"The Fibonacci square spiral tiling for 6 squares:",
				"   _____ ___ _______________",
				"  |     |   |               |",
				"  |     |_ _|               |",
				"  |_____|_|_|               |",
				"  |         |               |",
				"  |         |               |",
				"  |         |               |",
				"  |         |               |",
				"  |_________|_______________|",
				"In a 4-coloring of the Fibonacci square spiral tiling, the square k cannot be the same color as squares k-4, k-3, or k-1. When k-1 is the same color as k-3, k can be colored in 2 different ways.",
				"The first 3 squares must be colored ABC but for k\u003e3 square k can be the same color as square k-2."
			],
			"link": [
				"Michael C. Case, \u003ca href=\"/A331394/b331394.txt\"\u003eTable of n, a(n) for n = 1..200\u003c/a\u003e [a(192) corrected by _Georg Fischer_, Apr 15 2020]",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Fibonacci_number\"\u003eFibonacci number\u003c/a\u003e"
			],
			"formula": [
				"a(n) = a(n-1) - a(n-2) + 2*a(n-3) for n \u003e= 7.",
				"G.f.: x*(1 + x)*(1 - x + 2*x^2 - 2*x^3 + 2*x^4)/(1 - x + x^2 - 2*x^3).",
				"a(n)/a(n-1) approaches the only real solution of x^3 - x^2 + x - 2 = 0, x = (1 - 2*(2/(47 + 3*sqrt(249)))^(1/3) + ((47 + 3*sqrt(249))/2)^(1/3))/3 = 1.35320996419932... ."
			],
			"example": [
				"There are 3 ways to 4-color a Fibonacci square spiral tiling of 5 squares:",
				"   _____ ___   _____ ___   _____ ___",
				"  |     | C | |     | C | |     | C |",
				"  |  B  |_ _| |  B  |_ _| |  D  |_ _|",
				"  |_____|A|B| |_____|A|B| |_____|A|B|",
				"  |         | |         | |         |",
				"  |         | |         | |         |",
				"  |    C    | |    D    | |    C    |",
				"  |         | |         | |         |",
				"  |_________| |_________| |_________| so a(5)=3.",
				"There are 7 ways to 4-color a Fibonacci square spiral tiling of 8 squares (ABCBCADA, ABCBCDAD, ABCBDADA, ABCBDADC, ABCDCABA, ABCDCDAB, ABCDCDBA), so a(7) = 8."
			],
			"mathematica": [
				"Rest@ CoefficientList[Series[x (1 + x) (1 - x + 2 x^2 - 2 x^3 + 2 x^4)/(1 - x + x^2 - 2 x^3), {x, 0, 42}], x] (* _Michael De Vlieger_, Jan 31 2020 *)"
			],
			"xref": [
				"Cf. A000045, A216790, A077951."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Michael C. Case_, Jan 15 2020",
			"references": 1,
			"revision": 24,
			"time": "2020-04-15T15:36:10-04:00",
			"created": "2020-02-28T22:44:18-05:00"
		}
	]
}