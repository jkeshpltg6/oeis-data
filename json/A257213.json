{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A257213",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 257213,
			"data": "1,2,3,2,3,3,4,4,3,5,4,4,5,5,5,4,6,6,5,5,7,6,6,6,5,7,7,7,6,6,8,8,7,7,7,6,8,8,8,8,7,7,9,9,9,8,8,8,7,10,9,9,9,9,8,8,10,10,10,10,9,9,9,8,11,11,10,10,10,10,9,9,11,11,11,11,11,10,10,10",
			"name": "Least d\u003e0 such that floor(n/d) = floor(n/(d+1)).",
			"comment": [
				"For n \u003e 1: a(A043548(n)) = n. - _Reinhard Zumkeller_, Apr 19 2015"
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A257213/b257213.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) \u003e= A003059(n+1) = floor(sqrt(n))+1 \u003e= A003059(n) = ceiling(sqrt(n)) \u003e= A257212(n), with strict inequality (in the middle relation) when n is a square.",
				"a(k^2-1) = k for k \u003e 1. Proof: For n=k^2-1=(k-1)*(k+1), floor(n/k) = k-1 = n/(k+1), but n/(k-1)=k+1 and when denominators decrease further, this keeps increasing.",
				"a(k^2) \u003e= k+d when k \u003e d*(d-1). Proof: This follows from k^2/(k+d) = k-d+d^2/(k+d), which shows that a(k) \u003e= d when k \u003e d*(d-1).",
				"a(n) = A259361(n) + 1 + floor(sqrt((A232091(n+1) - 1 - n) + A079813(n+1)) + A079813(n+1)/2) = floor((sqrt(4*n+1)+1)/2) + floor(sqrt(ceiling((n+1) / ceiling(sqrt(n+1)) + 1) * ceiling(sqrt(n+1)) - round(sqrt(n+1)) - n - 1) + (ceiling(sqrt(n+1)) - round(sqrt(n+1)))/2). - _Haofen Liang_, Aug 25 2021"
			],
			"example": [
				"a(0)=1 because 0/1 = 0/2.",
				"a(1)=2 because [1/1] = 1 \u003e [1/2] = 0 = [1/3], where [x] := floor(x).",
				"a(2)=3 because [2/1] = 2 \u003e [2/2] = 1 \u003e [2/3] = 0 = [2/4]."
			],
			"mathematica": [
				"f[n_] := Block[{d, k}, Reap@ For[k = 0, k \u003c= n, k++, d = 1; While[Floor[k/d] != Floor[k/(d + 1)], d++]; Sow@ d] // Flatten // Rest]; f@ 79 (* _Michael De Vlieger_, Apr 18 2015 *)"
			],
			"program": [
				"(PARI) A257213(n)=for(d=sqrtint(n)+1,n+1,n\\d==n\\(d+1)\u0026\u0026return(d))",
				"(Haskell)",
				"a257213 n = head [d | d \u003c- [1..], div n d == div n (d + 1)]",
				"-- _Reinhard Zumkeller_, Apr 19 2015"
			],
			"xref": [
				"Cf. A003059, A257212.",
				"Cf. A043548."
			],
			"keyword": "nonn,nice,hear",
			"offset": "0,2",
			"author": "_M. F. Hasler_, Apr 18 2015",
			"references": 4,
			"revision": 40,
			"time": "2021-09-26T14:09:45-04:00",
			"created": "2015-04-18T15:12:03-04:00"
		}
	]
}