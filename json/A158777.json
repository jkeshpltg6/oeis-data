{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A158777",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 158777,
			"data": "1,1,0,1,2,0,2,0,1,6,0,6,0,3,0,1,24,0,24,0,12,0,4,0,25,120,0,120,0,60,0,20,0,245,0,121,720,0,720,0,360,0,120,0,2190,0,1446,0,361,5040,0,5040,0,2520,0,840,0,20370,0,15162,0,5047,0,841,40320,0,40320,0,20160,0,6720,0",
			"name": "Irregular array T(n,k), read by rows: row n is the polynomial expansion in t of p(x,t) = exp(t*x)/(1 - x/t - t^4 * x^4) with weighting factors t^n*n!.",
			"comment": [
				"Row sums are A334157: {1, 2, 5, 16, 89, 686, 5917, 54860, 588401, 7370074, ...}.",
				"Outer diagonal is A330045: {1, 1, 1, 1, 25, 121, 361, 841, 42001, 365905, ...}.",
				"From _Petros Hadjicostas_, Apr 15 2020: (Start)",
				"To prove the general formula below for T(n,2*m), let v = x/t in the equation Sum_{n,k \u003e= 0} (T(n,k)/n!) * (x/t)^n * t^k = p(x,t). We get Sum_{n,k \u003e= 0} (T(n,k)/n!) * v^n * t^k = exp(t^2*v)/(1 - v - t^8*v^4).",
				"Using the Taylor expansions of exp(t^2*v) and 1/(1 - v - t^8*v^4) around v = 0 (from array A180184), we get that Sum_{n,k \u003e= 0} (T(n,k)/n!) * v^n * t^k = (Sum_{n \u003e= 0} (t^2*v)^n/n!) * (Sum_{n \u003e= 0} Sum_{h=0..floor(n/4)} binomial(n - 3*h, h)*t^(8*h)*v^n).",
				"Using the Cauchy product of the above two series, for each n \u003e= 0, we get Sum_{k \u003e= 0} (T(n,k)/n!)*t^k = Sum_{l=0..n} Sum_{h=0..floor(l/4)} (binomial(l - 3*h, h)/(n-l)!)*t^(8*h+2*n-2*l). This implies that T(n,k) = 0 for odd k \u003e= 1.",
				"Letting k = 2*m = 8*h + 2*n - 2*l and s = n - l, we get Sum_{m \u003e= 0} (T(n, 2*m)/n!)*t^(2*m) = Sum_{m \u003e= 0} Sum_{s=0..m with 4|(m-s)} (binomial(n - s - 3*(m - s)/4, (m - s)/4)/s!)*t^(2*m) (and also that T(n,2*j) = 0 for j \u003e m). Equating coefficients, we get the formula for T(n,2*m) shown below. (End)"
			],
			"formula": [
				"T(n,k) = [t^k] (t^n * n! * ([x^n] p(x,t))), where p(x,t) = exp(t*x)/(1 - x/t - t^4*x^4).",
				"From _Petros Hadjicostas_, Apr 15 2020: (Start)",
				"Sum_{n,k \u003e= 0} (T(n,k)/n!) * (x/t)^n * t^k = p(x,t).",
				"T(n,0) = n! = A000142(n) for n \u003e= 0; T(n,2) = n! for n \u003e= 1; T(n,4) = n!/2 = A001710(n) for n \u003e= 2; T(n,6) = n!/6 = A001715(n) for n \u003e= 3.",
				"T(n,2*m) = n! * Sum_{s = 0..m with 4|(m-s)} binomial(n - s - 3*(m-s)/4, (m-s)/4)/s! for n \u003e= 0 and 0 \u003c= m \u003c= n.",
				"T(n,2*n) = A330045(n) for n \u003e= 0. (End)"
			],
			"example": [
				"Array T(n,k) (with n \u003e= 0 and 0 \u003c= k \u003c= 2*n) begins as follows:",
				"     1;",
				"     1, 0,    1;",
				"     2, 0,    2, 0,    1;",
				"     6, 0,    6, 0,    3, 0,   1;",
				"    24, 0,   24, 0,   12, 0,   4, 0,    25;",
				"   120, 0,  120, 0,   60, 0,  20, 0,   245, 0,   121;",
				"   720, 0,  720, 0,  360, 0, 120, 0,  2190, 0,  1446, 0,  361;",
				"  5040, 0, 5040, 0, 2520, 0, 840, 0, 20370, 0, 15162, 0, 5047, 0, 841;",
				"  ..."
			],
			"maple": [
				"# Triangle T(n, k) without the zeros (even k):",
				"W := proc(n, m) local v, s, h; v := 0;",
				"for s from 0 to m do",
				"if 0 = (m - s) mod 4 then",
				"h := (m - s)/4;",
				"v := v + binomial(n - s - 3*h, h)/s!;",
				"end if; end do; n!*v; end proc;",
				"for n1 from 0 to 20 do",
				"seq(W(n1,m1), m1=0..n1); end do; # _Petros Hadjicostas_, Apr 15 2020"
			],
			"mathematica": [
				"(* Generates the sequence in the data section *)",
				"Table[Expand[t^n*n!*SeriesCoefficient[Series[Exp[t*x]/(1 - x/t - t^4*x^4), {x, 0, 20}], n]], {n, 0, 10}];",
				"a = Table[CoefficientList[Expand[t^n*n!*SeriesCoefficient[Series[Exp[t*x]/(1 - x/t - t^4*x^4), {x, 0, 20}], n]], t], {n, 0, 10}];",
				"Flatten[%]",
				"(* Generates row sums *)",
				"Table[Apply[Plus, CoefficientList[Expand[t^n*n!*SeriesCoefficient[Series[Exp[t*x]/( 1 - x/t - t^4*x^4), {x, 0, 20}], n]], t]], {n, 0, 10}];"
			],
			"xref": [
				"Cf. A000142, A001710, A001715, A180184, A330045, A334157."
			],
			"keyword": "nonn,tabf",
			"offset": "0,5",
			"author": "_Roger L. Bagula_, Mar 26 2009",
			"ext": [
				"Various sections edited by _Petros Hadjicostas_, Apr 13 2020"
			],
			"references": 2,
			"revision": 63,
			"time": "2020-04-23T22:24:13-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}