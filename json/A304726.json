{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A304726",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 304726,
			"data": "3,8,35,120,323,728,1443,2600,4355,6888,10403,15128,21315,29240,39203,51528,66563,84680,106275,131768,161603,196248,236195,281960,334083,393128,459683,534360,617795,710648,813603,927368,1052675,1190280,1340963,1505528,1684803",
			"name": "a(n) = n^4 + 4*n^2 + 3.",
			"comment": [
				"Alternating sum of all points on the fourth row of the Hosoya triangle composed of Fibonacci polynomials, where F_{0}(n) = 1 and F_{1}(n) = n, hence a(n) = F_{5}(n)/F_{1}(n) for n\u003e0 (see Florez et al. reference, page 7, Table 4 and following sum).",
				"Apart from 8, all terms belong to A217554 because a(n) = (n^2+1)^2 + (n+1)^2 + (n-1)^2 = (n^2+2)^2 - 1. - _Bruno Berselli_, Jun 04 2018"
			],
			"link": [
				"Muniru A Asiru, \u003ca href=\"/A304726/b304726.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Rigoberto Florez, Robinson A. Higuita, and Antara Mukherjee, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Mukherjee/mukh2.html\"\u003eAlternating Sums in the Hosoya Polynomial Triangle\u003c/a\u003e, Journal of Integer Sequences, Vol. 17 (2014), Article 14.9.5.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/FibonacciPolynomial.html\"\u003eFibonacci Polynomial\u003c/a\u003e.",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (5,-10,10,-5,1)."
			],
			"formula": [
				"G.f.: (3 - 7*x + 25*x^2 - 5*x^3 + 8*x^4)/(1-x)^5.",
				"a(n) = 5*a(n-1) - 10*a(n-2) + 10*a(n-3) - 5*a(n-4) + a(n-5).",
				"a(n) = A059100(n)^2 - 1."
			],
			"maple": [
				"seq((n^2+2)^2-1,n=0..40); # _Muniru A Asiru_, Jun 03 2018"
			],
			"mathematica": [
				"Table[n^4 + 4 n^2 + 3, {n, 0, 35}]",
				"LinearRecurrence[{5,-10,10,-5,1},{3,8,35,120,323},40] (* _Harvey P. Dale_, Mar 04 2021 *)"
			],
			"program": [
				"(MAGMA) [n^4+4*n^2+3: n in [0..40]];",
				"(GAP) List([0..40], n -\u003e (n^2+2)^2-1); # _Muniru A Asiru_, Jun 03 2018"
			],
			"xref": [
				"Cf. A058071, A059100, A217554.",
				"Subsequence of A005563."
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "_Vincenzo Librandi_, May 31 2018",
			"references": 1,
			"revision": 60,
			"time": "2021-03-04T13:32:50-05:00",
			"created": "2018-06-04T09:56:30-04:00"
		}
	]
}