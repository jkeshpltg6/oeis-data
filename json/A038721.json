{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A038721",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 38721,
			"data": "2,18,110,570,2702,12138,52670,223290,931502,3842058,15718430,63928410,258885902,1045076778,4208939390,16921719930,67944897902,272553908298,1092539107550,4377127901850,17529428119502,70180466208618",
			"name": "k=2 column of A038719.",
			"comment": [
				"For n\u003e=1, a(n) is equal to the number of functions f: {1,2,...,n+1}-\u003e{1,2,3,4} such that Im(f) contains 2 fixed elements. - Aleksandar M. Janjic and _Milan Janjic_, Feb 27 2007",
				"Let P(A) be the power set of an n-element set A and R be a relation on P(A) such that for all x, y of P(A), xRy if x is not a subset of y and y is not a subset of x. Then a(n+1) = |R|. [From _Ross La Haye_, Mar 19 2009]",
				"Number of ordered (n+1)-tuples of positive integers, whose minimum is 0 and maximum 3. - _Ovidiu Bagdasar_, Sep 19 2014"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A038721/b038721.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"O. Bagdasar, \u003ca href=\"http://www.np.ac.rs/downloads/publications/VOL6_Br_2/vol6br2-3.pdf\"\u003eOn Some Functions Involving the lcm and gcd of Integer Tuples\u003c/a\u003e, Scientific publications of the state university of Novi Pazar, Ser. A: Appl. Maths. Inform. and Mech., Vol. 6, 2 (2014), 91-100.",
				"K. S. Immink, \u003ca href=\"http://www.exp-math.uni-essen.de/~immink/pdf/jsac13.pdf\"\u003eCoding Schemes for Multi-Level Channels that are Intrinsically Resistant Against Unknown Gain and/or Offset Using Reference Symbols\u003c/a\u003e, 2013.",
				"Milan Janjic, \u003ca href=\"http://www.pmfbl.org/janjic/\"\u003eEnumerative Formulas for Some Functions on Finite Sets\u003c/a\u003e",
				"Ross La Haye, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL12/LaHaye/lahaye5.html\"\u003eBinary Relations on the Power Set of an n-Element Set\u003c/a\u003e, Journal of Integer Sequences, Vol. 12 (2009), Article 09.2.6.",
				"R. B. Nelsen and H. Schmidt, Jr., \u003ca href=\"http://www.jstor.org/stable/2690450\"\u003eChains in power sets\u003c/a\u003e, Math. Mag., 64 (1991), 23-31.",
				"\u003ca href=\"/index/Pos#posets\"\u003eIndex entries for sequences related to posets\u003c/a\u003e",
				"\u003ca href=\"/index/Rec\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (9,-26,24)."
			],
			"formula": [
				"4^(n+1) - 2*3^(n+1) + 2^(n+1).",
				"a(1)=2, a(2)=18, a(3)=110, a(n)=9*a(n-1)-26*a(n-2)+24*a(n-3). - _Harvey P. Dale_, Aug 16 2012",
				"G.f.: -2*x/((2*x-1)*(3*x-1)*(4*x-1)). - _Colin Barker_, Nov 27 2012"
			],
			"mathematica": [
				"Table[4^n-2*3^n+2^n,{n,2,30}] (* or *) LinearRecurrence[{9,-26,24},{2,18,110},30] (* _Harvey P. Dale_, Aug 16 2012 *)"
			],
			"program": [
				"(Haskell)",
				"import Data.List (transpose)",
				"a038721 n = a038721_list !! (n-1)",
				"a038721_list = (transpose a038719_tabl) !! 2",
				"-- _Reinhard Zumkeller_, Jul 08 2012"
			],
			"xref": [
				"Cf. A038720."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_, May 02 2000",
			"ext": [
				"More terms from Larry Reeves (larryr(AT)acm.org), May 09 2000"
			],
			"references": 4,
			"revision": 47,
			"time": "2015-06-13T00:49:19-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}