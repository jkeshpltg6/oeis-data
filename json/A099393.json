{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A099393",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 99393,
			"data": "1,5,19,71,271,1055,4159,16511,65791,262655,1049599,4196351,16781311,67117055,268451839,1073774591,4295032831,17180000255,68719738879,274878431231,1099512676351,4398048608255,17592190238719",
			"name": "a(n) = 4^n + 2^n - 1.",
			"comment": [
				"Number of occurrences of letter 2 in the (n+1)-st Peano word.",
				"a(n) = A020522(n) + A000225(n+1) = A083420(n) - A020522(n); in binary representation: a leading one followed by n zeros then by n ones; A000120(a(n)) = n+1; A023416(a(n))=n; A070939(a(n)) = 2*n+1; 2*A020522(n)+1 = A030101(a(n)). - _Reinhard Zumkeller_, Feb 07 2006",
				"The number of involutions in group G_n G_{n+1} = G_n(operation) D_8. For example, Q_8-\u003e1 involution; D_8-\u003e5 involutions - _Roger L. Bagula_, Aug 08 2007"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A099393/b099393.txt\"\u003eTable of n, a(n) for n = 0..170\u003c/a\u003e",
				"A. M. Cohen and D. E. Taylor, \u003ca href=\"https://doi.org/10.1080/00029890.2007.11920454\"\u003eOn a Certain Lie Algebra Defined By a Finite Group\u003c/a\u003e, American Mathematical Monthly, volume 114, number 7, August-September 2007, pages 633-638.  Also \u003ca href=\"https://www.maths.usyd.edu.au/u/pubs/publist/preprints/2006/cohen-12.html\"\u003epreprint\u003c/a\u003e.  a(n) = t_n in proof of theorem 6.2.",
				"Sergey Kitaev and Toufik Mansour, \u003ca href=\"http://arXiv.org/abs/math.CO/0210268\"\u003eThe Peano curve and counting occurrences of some patterns\u003c/a\u003e, arXiv:math/0210268 [math.CO], 2002.  Section 3 lemma 1, d_2^n = a(n-1).",
				"Sergey Kitaev, Toufik Mansour, and Patrice Séébold, \u003ca href=\"https://doi.org/10.25596/jalc-2004-439\"\u003eGenerating the Peano curve and counting occurrences of some patterns\u003c/a\u003e, Journal of Automata, Languages and Combinatorics, volume 9, number 4, 2004, pages 439-455.  Also at \u003ca href=\"https://www.researchgate.net/publication/230802620_Generating_the_Peano_curve_and_counting_occurrences_of_some_patterns\"\u003eResearchGate\u003c/a\u003e.  Section 4, |P_n|_r = a(n-1).",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (7,-14,8)."
			],
			"formula": [
				"a(n) = 2^(2*n-1) + 2*a(n-1) + 1. - _Roger L. Bagula_, Aug 08 2007",
				"From _Mohammad K. Azarian_, Jan 15 2009: (Start)",
				"G.f.: 1/(1-4*x) + 1/(1-2*x) - 1/(1-x).",
				"E.g.f.: e^(4*x) + e^(2*x) - e^x. (End)",
				"a(n) = A279396(n+4, 4). - _Wolfdieter Lang_, Jan 10 2017"
			],
			"example": [
				"n=5: a(5)=4^5+2^5-1=1024+32-1=1055 -\u003e '10000011111'."
			],
			"mathematica": [
				"LinearRecurrence[{7,-14,8},{1,5,19},30] (* _Harvey P. Dale_, Sep 06 2015 *)"
			],
			"program": [
				"(MAGMA) [4^n + 2^n - 1: n in [0..60]]; // _Vincenzo Librandi_, Apr 26 2011",
				"(PARI) a(n)=4^n+2^n-1; \\\\ _Charles R Greathouse IV_, Sep 24 2015"
			],
			"xref": [
				"Equals A063376(n) - 1.",
				"Cf. A279396."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Ralf Stephan_, Oct 20 2004",
			"references": 15,
			"revision": 37,
			"time": "2021-01-11T10:05:44-05:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}