{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A087910",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 87910,
			"data": "1,2,2,5,8,5,5,13,9,10,10,12,12,12,12,22,17,18,18,21,22,21,21,27,25,26,26,27,27,27,27,40,33,34,34,37,39,37,37,48,41,42,42,44,44,44,44,54,49,50,50,53,54,53,53,58,57,59,62,58,58,58",
			"name": "Exponent of the greatest power of 2 dividing the numerator of 2^1/1 + 2^2/2 + 2^3/3 + ... + 2^n/n.",
			"comment": [
				"Problem 9 of the 2002 Sydney University Mathematical Society Problems competition asked for a proof that a(n) tends to infinity with n. While this is immediate from the theory of the 2-adic logarithm, elementary proofs are available.",
				"a(n) tends to infinity with n implies that log(-1) = 0 in the 2-adic field, by setting x = 2 in -log(1-x) = Sum_{k\u003e=1} x^k/k. - _Jianing Song_, Aug 05 2019"
			],
			"reference": [
				"A. M. Robert, A Course in p-adic Analysis, Springer, 2000; see p. 278."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A087910/b087910.txt\"\u003eTable of n, a(n) for n = 1..5000\u003c/a\u003e",
				"Sydney University Mathematical Society \u003ca href=\"http://www.maths.usyd.edu.au/u/SUMS/\"\u003eProblems Competitions\u003c/a\u003e, see Problem 9 of the 2002 competition."
			],
			"formula": [
				"a(n) = A007814(A108866(n)). - _Michel Marcus_, Feb 22 2020",
				"Sum_{k=1..n} 2^k/k = (2^n/n)*Sum_{k=0..n-1} 1/binomial(n-1,k), so a(n) \u003e= n - v(n,2) - max_{k=0..n-1} v(binomial(n-1,k),2) = n - A007814(n) - A119387(n) = n - floor(log_2(n)), where v(n,2) is the 2-adic valuation of n. It seems that the equality holds if and only if n = 2^m - 1 for some m. - _Jianing Song_, Feb 22 2020"
			],
			"example": [
				"a(5) = 8 as 2^1/1 + 2^2/2 + 2^3/3 + 2^4/4 + 2^5/5 = 256/15 whose numerator is divisible by 2^8 but not by 2^9."
			],
			"maple": [
				"S:= 0:",
				"for n from 1 to 100 do",
				"  S:= S + 2^n/n;",
				"  a[n]:= padic:-ordp(numer(S),2);",
				"od:",
				"seq(a[n],n=1..100); # _Robert Israel_, Jun 09 2015"
			],
			"mathematica": [
				"s[n_] := -2^(n + 1) LerchPhi[2, 1, n + 1] - I Pi;",
				"a[n_] := IntegerExponent[Numerator[Simplify[s[n]]], 2];",
				"Array[a, 62] (* _Peter Luschny_, Feb 22 2020 *)"
			],
			"program": [
				"(PARI) a(n) = valuation(sum(k=1,n,2^k/k), 2) \\\\ _Jianing Song_, Feb 22 2020"
			],
			"xref": [
				"Cf. A108866, A007814, A119387, A000523."
			],
			"keyword": "easy,nonn",
			"offset": "1,2",
			"author": "_Robin Chapman_, Oct 17 2003",
			"references": 2,
			"revision": 25,
			"time": "2020-02-22T10:35:35-05:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}