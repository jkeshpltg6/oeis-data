{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A181477",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 181477,
			"data": "1,5,25,85,275,751,1955,4615,10460,22220,45628,89420,170340,313140,562020,980628,1676370,2800410,4596290,7399930,11732006,18297950,28155910,42716750,64037980,94823756,138922300,201325900,288988100",
			"name": "a(n) has generating function 1/((1-x)^k*(1-x^2)^(k*(k-1)/2)) for k=5.",
			"comment": [
				"a(n-1,k) is conjectured to also be the count of monomials (or terms) in the Schur polynomials of k variables and degree n, summed over all partitions of n in at most k parts (zero-padded to length k)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A181477/b181477.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Schur_polynomial\"\u003eSchur Polynomial\u003c/a\u003e"
			],
			"example": [
				"a(3)=85 since the Schur polynomial of 5 variables and degree 4 starts off as x[1]*x[2]*x[3]*x[4] + x[1]*x[2]*x[3]*x[5] + ... + x[4]*x[5]^3 + x[5]^4. The exponents collect to the padded partitions of 4 as 5*p(1) + 40*p(2) + 30*p(3) + 150*p(4) + 50*p(5) where p(1) is the lexicographically first padded partition of 4: {4,0,0,0}, a coded form of monomials x[i]^4, and p(5) stands for {1,1,1,1}, coding x[i]x[j]x[k]x[l] with all indices different."
			],
			"mathematica": [
				"Tr[toz/@(Function[q,PadRight[q,k]]/@ (TransposePartition/@ Partitions[n,k]))/. w[arg__] -\u003e 1 ]; with toz[p_]:=Block[{a,q,e,w}, u1=Expand[q Together[Expand[schur[p]]] +q a]/. Plus-\u003e List ; u2=u1/. Times-\u003ew /. q-\u003eSequence[]/. w[i_Integer, r__]-\u003e i w[r] /. x[_]^(e_:1) -\u003ee ; u3=Plus@@ u2/. w[arg__]:\u003e Reverse@ Sort@ w[arg] /. w[a]-\u003e0 ]; and schur[p_]:=Block[{le=Length[p],n=Tr[p]}, Together[Expand[Factor[Det[Outer[ #2^#1\u0026,p+le-Range[le] , Array[x,le]]]]/Factor[Det[Outer[ #2^#1\u0026,Range[le-1,0,-1] , Array[x,le]]]] ]] ]"
			],
			"xref": [
				"For k=2 (two variables): A002620, k=3: A038163, k=4: A054498 k=6: A181478, k=7: A181479, k=8: A181480.",
				"Column k=5 of A210391. - _Alois P. Heinz_, Mar 22 2012"
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Wouter Meeussen_, Oct 24 2010",
			"references": 5,
			"revision": 21,
			"time": "2020-04-16T18:45:13-04:00",
			"created": "2010-11-10T03:00:00-05:00"
		}
	]
}