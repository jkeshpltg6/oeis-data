{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003261",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3261,
			"id": "M4379",
			"data": "1,7,23,63,159,383,895,2047,4607,10239,22527,49151,106495,229375,491519,1048575,2228223,4718591,9961471,20971519,44040191,92274687,192937983,402653183,838860799,1744830463,3623878655,7516192767",
			"name": "Woodall (or Riesel) numbers: n*2^n - 1.",
			"comment": [
				"For n\u003e1, a(n) is base at which zero is reached for the function \"write f(j) in base j, read as base j+1 and then subtract 1 to give f(j+1)\" starting from f(n) = n^2 - 1. - _Henry Bottomley_, Aug 06 2000",
				"Sequence corresponds also to the maximum chain length of the classic puzzle whereby, under agreed commercial terms, an asset of unringed golden chain, when judiciously fragmented into as few as n pieces and n-1 opened links (through n-1 cuts), might be used to settle debt sequentially, with a golden link covering for unit cost. Here beside the n-1 opened links, the n fragmented pieces have lengths n, 2*n, 4*n, ..., 2^(n-1)*n. For instance, the chain of original length a(5)=159, if segregated by 4 cuts into 5+1+10+1+20+1+40+1+80, may be used to pay sequentially, i.e., a link-cost at a time, for an equivalent cost up to 159 links, to the same creditor. - _Lekraj Beedassy_, Feb 06 2003"
			],
			"reference": [
				"A. Brousseau, Number Theory Tables. Fibonacci Association, San Jose, CA, 1973, p. 159.",
				"K. R. Bhutani and A. B. Levin, \"The Problem of Sawing a Chain\", Journal of Recreational Mathematics 2002-3 31(1) 32-35.",
				"G. Everest, A. van der Poorten, I. Shparlinski and T. Ward, Recurrence Sequences, Amer. Math. Soc., 2003; see esp. p. 255.",
				"M. Gardner, Martin Gardner's Sixth Book of Mathematical Diversions from Scientific American, \"Gold Links\", Problem 4, pp. 50-51; 57-58, University of Chicago Press, 1983.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A003261/b003261.txt\"\u003eTable of n, a(n) for n = 1..300\u003c/a\u003e",
				"Ray Ballinger, \u003ca href=\"http://web.archive.org/web/20161028080439/http://www.prothsearch.net/woodall.html\"\u003eWoodall Primes: Definition and Status\u003c/a\u003e",
				"Alfred Brousseau, \u003ca href=\"http://www.fq.math.ca/fibonacci-tables.html\"\u003eFibonacci and Related Number Theoretic Tables\u003c/a\u003e, Fibonacci Association, San Jose, CA, 1972. See p. 159.",
				"C. K. Caldwell, \u003ca href=\"http://primes.utm.edu/glossary/page.php?sort=WoodallNumber\"\u003eWoodall Numbers\u003c/a\u003e",
				"Paul Leyland, \u003ca href=\"http://www.leyland.vispa.com/numth/factorization/cullen_woodall/cw.htm\"\u003eFactors of Cullen and Woodall numbers\u003c/a\u003e",
				"Paul Leyland, \u003ca href=\"http://www.leyland.vispa.com/numth/factorization/cullen_woodall/gcw.htm\"\u003eGeneralized Cullen and Woodall numbers\u003c/a\u003e",
				"D. Marques, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Marques/marques5r2.html\"\u003eOn Generalized Cullen and Woodall Numbers That are Also Fibonacci Numbers\u003c/a\u003e, Journal of Integer Sequences, 17 (2014), #14.9.4.",
				"Hisanori Mishima, \u003ca href=\"http://www.asahi-net.or.jp/~KC2H-MSM/mathland/matha1/matha122.htm\"\u003eFactorizations of many number sequences: Riesel numbers, n=1..100\u003c/a\u003e, \u003ca href=\"http://www.asahi-net.or.jp/~KC2H-MSM/mathland/matha1/matha123.htm\"\u003en=101..200\u003c/a\u003e, \u003ca href=\"http://www.asahi-net.or.jp/~KC2H-MSM/mathland/matha1/matha124.htm\"\u003en=201..300\u003c/a\u003e, \u003ca href=\"http://www.asahi-net.or.jp/~KC2H-MSM/mathland/matha1/matha125.htm\"\u003en=301..323\u003c/a\u003e.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"T. Sillke, \u003ca href=\"http://www.mathematik.uni-bielefeld.de/~sillke/PUZZLES/chain-link-pay\"\u003eUsing Chains Links To Pay For A Room\u003c/a\u003e",
				"Amelia Carolina Sparavigna, \u003ca href=\"https://doi.org/10.5281/zenodo.2634312\"\u003eOn the generalized sums of Mersenne, Fermat, Cullen and Woodall Numbers\u003c/a\u003e, Politecnico di Torino (Italy, 2019).",
				"Amelia Carolina Sparavigna, \u003ca href=\"https://doi.org/10.18483/ijSci.2044\"\u003eComposition Operations of Generalized Entropies Applied to the Study of Numbers\u003c/a\u003e, International Journal of Sciences (2019) Vol. 8, No. 4, 87-92.",
				"Amelia Carolina Sparavigna, \u003ca href=\"https://doi.org/10.5281/zenodo.3471358\"\u003eThe groupoids of Mersenne, Fermat, Cullen, Woodall and other Numbers and their representations by means of integer sequences\u003c/a\u003e, Politecnico di Torino, Italy (2019), [math.NT].",
				"Amelia Carolina Sparavigna, \u003ca href=\"https://doi.org/10.18483/ijSci.2188\"\u003eSome Groupoids and their Representations by Means of Integer Sequences\u003c/a\u003e, International Journal of Sciences (2019) Vol. 8, No. 10.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/WoodallNumber.html\"\u003eWoodall Number.\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Woodall_number\"\u003eWoodall number\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (5,-8,4)."
			],
			"formula": [
				"G.f.: x*(-1-2*x+4*x^2) / ( (x-1)*(-1+2*x)^2 ). - _Simon Plouffe_ in his 1992 dissertation",
				"Binomial transform of A133653 and double binomial transform of [1, 5, -1, 1, -1, 1, ...]. - _Gary W. Adamson_, Sep 19 2007",
				"a(n) = -(2)^n * A006127(-n) for all n in Z. - _Michael Somos_, Nov 04 2018"
			],
			"example": [
				"G.f. = x + 7*x^2 + 23*x^3 + 63*x^4 + 159*x^5 + 383*x^6 + 895*x^7 + ... - _Michael Somos_, Nov 04 2018"
			],
			"mathematica": [
				"Table[n*2^n-1,{n,3*4!}] (* _Vladimir Joseph Stephan Orlovsky_, Apr 25 2010 *)"
			],
			"program": [
				"(Haskell)",
				"a003261 = (subtract 1) . a036289  -- _Reinhard Zumkeller_, Mar 05 2012",
				"(PARI) A003261(n)=n*2^n-1  \\\\ _M. F. Hasler_, Oct 31 2012",
				"(MAGMA) [n*2^n -1: n in [1..30]]; // _G. C. Greubel_, Nov 04 2018",
				"(Python) [n*2**n - 1 for n in range(1, 29)] # _Michael S. Branicky_, Jan 07 2021"
			],
			"xref": [
				"Cf. A002234, A002064, A005849, A050918, A006127.",
				"a(n) = A036289(n) - 1 = A002064(n) - 2.",
				"Cf. A133653."
			],
			"keyword": "nonn,easy,nice",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"references": 30,
			"revision": 89,
			"time": "2021-03-12T22:32:35-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}