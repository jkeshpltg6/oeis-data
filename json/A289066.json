{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A289066",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 289066,
			"data": "3,1,3,10,39,184,1047,7000,53571,460936,4404603,46296040,530878719,6595091944,88232942847,1264741738120,19337532032091,314144393039176,5403576523773603,98110258621524520,1875097757416854999,37629001852534817704,791088129700026499047",
			"name": "Recurrence a(n+2) = Sum_{k=0..n} binomial(n,k)*a(k)*a(n+1-k) with a(0)=3, a(1)=1.",
			"comment": [
				"One of a family of integer sequences whose e.g.f.s satisfy the differential equation f''(z) = f'(z)f(z). For more details, see A289064."
			],
			"link": [
				"Stanislav Sykora, \u003ca href=\"/A289066/b289066.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"S. Sykora, \u003ca href=\"http://dx.doi.org/10.3247/SL6Math17.001\"\u003eSequences related to the differential equation f'' = af'f\u003c/a\u003e, Stan's Library, Vol. VI, Jun 2017."
			],
			"formula": [
				"E.g.f.: -sqrt(7)/tanh(z*sqrt(7)/2 - arccosh(3/sqrt(2))).",
				"E.g.f. for the sequence (-1)^(n+1)*a(n): -sqrt(7)/tanh(z*sqrt(7)/2 + arccosh(3/sqrt(2))).",
				"a(n) ~ 2 * n! * 7^((n+1)/2) / log(8 + 3*sqrt(7))^(n+1). - _Vaclav Kotesovec_, Jun 24 2017"
			],
			"maple": [
				"f:= proc(n) option remember; add(binomial(n-2,k)*procname(k)*procname(n-1-k),k=0..n-2) end proc:",
				"f(0):= 3: f(1):= 1:",
				"map(f, [$0..50]); # _Robert Israel_, Jul 20 2017"
			],
			"mathematica": [
				"a[n_] := a[n] = Sum[Binomial[n-2, k]*a[k]*a[n-k-1], {k, 0, n-2}]; a[0] = 3; a[1] = 1; Array[a, 23, 0] (* _Jean-François Alcover_, Jul 20 2017 *)"
			],
			"program": [
				"(PARI) c0=3; c1=1; nmax = 200; a = vector(nmax+1); a[1]=c0; a[2]=c1; for(m=0,#a-3,a[m+3]=sum(k=0,m,binomial(m,k)*a[k+1]*a[m+2-k])); a"
			],
			"xref": [
				"Sequences for other starting pairs: A000111 (1,1), A289064 (1,-1), A289065 (2,-1), A289067 (3,-1), A289068 (1,-2), A289069 (3,-2), A289070 (0,3)."
			],
			"keyword": "nonn",
			"offset": "0,1",
			"author": "_Stanislav Sykora_, Jun 23 2017",
			"references": 15,
			"revision": 25,
			"time": "2018-08-17T19:47:21-04:00",
			"created": "2017-07-08T00:57:23-04:00"
		}
	]
}