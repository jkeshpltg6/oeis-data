{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A035206",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 35206,
			"data": "1,1,2,1,3,6,1,4,12,6,12,1,5,20,20,30,30,20,1,6,30,30,15,60,120,20,60,90,30,1,7,42,42,42,105,210,105,105,140,420,140,105,210,42,1,8,56,56,56,28,168,336,336,168,168,280,840,420,840,70,280,1120,560,168,420,56,1,9,72",
			"name": "Number of multisets associated with least integer of each prime signature.",
			"comment": [
				"a(n,k) multiplied by A036038(n,k) yields A049009(n,k).",
				"a(n,k) enumerates distributions of n identical objects (balls) into m of altogether n distinguishable boxes. The k-th partition of n, taken in the Abramowitz-Stegun (A-St) order, specifies the occupation of the m =m(n,k)= A036043(n,k) boxes. m = m(n,k) is the number of parts of the k-th partition of n. For the A-St ordering see pp.831-2 of the reference given in A117506. _Wolfdieter Lang_, Nov 13 2007.",
				"The sequence of row lengths is p(n)= A000041(n) (partition numbers).",
				"For the A-St order of partitions see the Abramowitz-Stegun reference given in A117506.",
				"The corresponding triangle with summed row entries which belong to partitions of the same number of parts k is A103371. [From _Wolfdieter Lang_, Jul 11 2012]"
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A035206/b035206.txt\"\u003eTable of n, a(n) for n = 0..2713\u003c/a\u003e (rows 0..20)",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"Wolfdieter Lang, \u003ca href=\"/A035206/a035206.pdf\"\u003eFirst 10 rows and more.\u003c/a\u003e"
			],
			"formula": [
				"a(n,k) = A048996(n,k)*binomial(n,m(n,k)),n\u003e=1, k=1,...,p(n) and m(n,k):=A036043(n,k) gives the number of parts of the k-th partition of n."
			],
			"example": [
				"n\\k 1  2  3  4   5   6   7   8   9  10  11  12  13 14 15",
				"0   1",
				"1   1",
				"2   2  1",
				"3   3  6  1",
				"4   4 12  6 12   1",
				"5   5 20 20 30  30  20   1",
				"6   6 30 30 15  60 120  20  60  90  30   1",
				"7   7 42 42 42 105 210 105 105 140 420 140 105 210 42  1",
				"...",
				"Row No. 8:  8  56 56 56 28 168 336 336 168 168 280  840 420 840 70 280 1120 560 168 420 56 1",
				"Row No. 9: 9 72 72 72 72 252 504 504 252 252 504 84 504 1512 1512 1512 1512 504 630 2520 1260 3780 630 504 2520 1680 252 756 72 1",
				"[rewritten and extended table by _Wolfdieter Lang_, Jul 11 2012]",
				"a(5,5) relates to the partition (1,2^2) of n=5. Here m=3 and 5 indistinguishable (identical) balls are put into boxes b1,...,b5 with m=3 boxes occupied; one with one ball and two with two balls.",
				"Therefore a(5,5) = binomial(5,3)*3!/(1!*2!) = 10*3 = 30. _Wolfdieter Lang_, Nov 13 2007."
			],
			"program": [
				"(PARI)",
				"C(sig)={my(S=Set(sig)); binomial(vecsum(sig), #sig)*(#sig)!/prod(k=1, #S, (#select(t-\u003et==S[k], sig))!)}",
				"Row(n)={apply(C, [Vecrev(p) | p\u003c-partitions(n)])}",
				"{ for(n=0, 7, print(Row(n))) } \\\\ _Andrew Howroyd_, Oct 18 2020"
			],
			"xref": [
				"Cf. A036038, A048996, A049009.",
				"Cf. A001700 (row sums).",
				"Cf. A103371(n-1, m-1) (triangle obtained after summing in every row the numbers with like part numbers m)."
			],
			"keyword": "nonn,tabf,easy",
			"offset": "0,3",
			"author": "_Alford Arnold_",
			"ext": [
				"More terms from _Joshua Zucker_, Jul 27 2006",
				"a(0)=1 prepended by _Andrew Howroyd_, Oct 18 2020"
			],
			"references": 15,
			"revision": 26,
			"time": "2020-10-19T23:13:34-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}