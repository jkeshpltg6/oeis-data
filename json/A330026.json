{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A330026",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 330026,
			"data": "1,1,2,2,5,4,5,4,5,6,10,9,8,5,8,7,15,12,14,17,12,15,11,16,16,18,13,14,14,21,45,29,34,26,32,25,25,25,22,20,26,20,32,24,33,23,38,48,36,34,40,30,31,30,37,31,33,39,37,38,32,48,41,44,36,52,54,43,43,51",
			"name": "a(n) is the number of integers k \u003e prime(n) such that all rows (after the initial row) of the triangle of absolute differences of the (n+1)-tuple (prime(1), ..., prime(n), k) all start with 1.",
			"comment": [
				"Suggested by Gilbreath's conjecture (see A036262).",
				"The sequence of primes considered in Gilbreath's conjecture is just one possibility out of all the sequences that can be generated starting from (2,3,...,prime(n)).",
				"Conjecture: Prime(n+1) is always between prime(n)+2 and prime(n)+2*a(n)."
			],
			"link": [
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/GilbreathsConjecture.html\"\u003eGilbreath's Conjecture\u003c/a\u003e"
			],
			"formula": [
				"Given the sequence of primes S = (2,3,5,...,prime(n)) and the triangle of the forward absolute differences of S, let d(r) be the last rightmost absolute difference of row r, for r\u003e1. Then a(n) = (1+ Sum_{r\u003e1} d(r)) / 2."
			],
			"example": [
				"a(1) = 1, because considering the sequence (2,k), k=3 is the only solution.",
				"a(2) = 1, because considering the sequence (2,3,k), k=5 is the only solution.",
				"To calculate a(3), consider the sequence (2,3,5) and calculate the triangle of absolute differences:",
				"2,3,5",
				"1,2",
				"1",
				"Consider the rightmost diagonal, which contains the absolute differences (2,1), then a(3) is given by a(3) = [(2+1) + 1]/2 = 2.",
				"That is, a(3) is the result of summing the two differences contained in the rightmost diagonal (2,1). To this sum we add 1 and divide the total by 2.",
				"So there are 2 integers k \u003e prime(3) = 5 -- namely, 7,9 -- such that the forward absolute differences of (2,3,5,k) start with 1.",
				"To be explicit, there are two triangles of absolute differences resulting from (2,3,5,k), namely",
				"2 3 5 7",
				"1 2 2",
				"1 0",
				"1",
				"and",
				"2 3 5 9",
				"1 2 4",
				"1 2",
				"1",
				"a(24) = 16 because there are 16 integers k \u003e prime(25) = 97 -- namely, 99,101,...,129 -- such that all rows (after the first row) of the triangle of absolute differences of (2,3,5,7,11,13,17,...,97,k) start with 1."
			],
			"program": [
				"(Python)",
				"# run the function find_M(n), where n is the last prime number of your list of consecutive primes starting from 2",
				"from sympy import isprime",
				"def primes_less_N(n):",
				"    primes = []",
				"    for i in range(2,n+1):",
				"        if(isprime(i)==True):",
				"            primes.append(i)",
				"    return primes",
				"def find_M(n):",
				"    primes = primes_less_N(n)",
				"    l = len(primes)",
				"    count = 1",
				"    if count == 1:",
				"        a = [abs(x - primes[i - 1]) for i, x in enumerate(primes)][1:]",
				"        count += 1",
				"    list_f = []",
				"    if count \u003e 1:",
				"        while count \u003cl+1:",
				"            list_f.append(a[len(a)-1])",
				"            b = [abs(x - a[i-1]) for i, x in enumerate(a)][1:]",
				"            a=b",
				"            count +=1",
				"    return (sum(list_f)+1)/2",
				"(PARI) diffs(v) = {while (#v != 1, v = vector(#v-1, k, abs(v[k+1] - v[k]));); v[1];}",
				"a(n) = {my(v = primes(n), m = vecmax(v)+1, nb = 0); if (!(m%2), m++); forstep (k=m, oo, 2, if (diffs(concat(v, k)) == 1, nb++, if (nb, break));); nb;} \\\\ _Michel Marcus_, Dec 03 2019"
			],
			"xref": [
				"Cf. A036262, A036261."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Fabio Mercurio_, Nov 27 2019",
			"references": 0,
			"revision": 68,
			"time": "2019-12-10T12:04:09-05:00",
			"created": "2019-12-10T12:01:03-05:00"
		}
	]
}