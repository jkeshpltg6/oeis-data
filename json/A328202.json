{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A328202",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 328202,
			"data": "6,10,5,7,14,6,3,11,11,13,13,1,2,34,17,19,19,1,1,23,23,5,5,3,3,29,29,31,62,2,1,1,1,37,37,1,1,41,41,43,43,1,1,47,47,7,7,1,1,53,53,1,1,1,1,59,59,61,61,1,2,2,1,67,67,1,1,71,71,73,73,1,1,1,1",
			"name": "a(n) is the greatest common divisor of all the numbers in row n of Pascal's triangle excluding 1 and n.",
			"comment": [
				"If p is prime, a(p) is a multiple of p and no term a(n) where n \u003c p is a multiple of p.",
				"From _Robert Israel_, Oct 16 2019: (Start)",
				"If p is prime, a(p^k) and a(p^k+1) are divisible by p for all k\u003e=1.",
				"If p is a Fermat prime \u003e 3, a(p)=2*p.",
				"If p is a Mersenne prime, a(p+1)=2*p.",
				"Conjecture: these are the only cases where a(n)\u003en. (End)"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A328202/b328202.txt\"\u003eTable of n, a(n) for n = 4..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A014963(n) * A014963(n - 1) (conjecture). - _Jon Maiga_, Oct 08 2019"
			],
			"example": [
				"For n=8, take row 8 of Pascal's triangle:",
				"1 8 28 56 70 56 28 8 1,",
				"remove the first and last 2 numbers:",
				"28 56 70 56 28,",
				"the greatest common divisor of 28, 56, 70, 56, 28 is 14, thus a(8)=14."
			],
			"maple": [
				"f:= proc(n) local k,g;",
				"  g:= binomial(n,2);",
				"  for k from 3 to n/2 do",
				"    g:= igcd(g,binomial(n,k));",
				"    if g = 1 then return g fi;",
				"  od;",
				"  g",
				"end proc:",
				"map(f, [$4..100]);# _Robert Israel_, Oct 16 2019"
			],
			"mathematica": [
				"a[n_] := GCD @@ Binomial[n, Range[2, n/2]]; a /@ Range[4, 90] (* _Giovanni Resta_, Oct 08 2019 *)"
			],
			"program": [
				"(PARI) a(n) = gcd(vector((n+1)\\2-1, k, binomial(n, k+1))); \\\\ _Michel Marcus_, Oct 08 2019"
			],
			"xref": [
				"Cf. A007318 (Pascal's triangle), A014963."
			],
			"keyword": "nonn,look",
			"offset": "4,1",
			"author": "_Joel Kaufmann_, Oct 07 2019",
			"ext": [
				"More terms from _Giovanni Resta_, Oct 08 2019"
			],
			"references": 2,
			"revision": 40,
			"time": "2019-10-25T01:18:24-04:00",
			"created": "2019-10-14T13:57:19-04:00"
		}
	]
}