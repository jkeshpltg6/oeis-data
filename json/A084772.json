{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A084772",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 84772,
			"data": "1,6,46,396,3606,33876,324556,3151896,30915046,305543556,3038019876,30354866856,304523343996,3065412858696,30946859111256,313206733667376,3176825392214406,32284147284682596,328643023505612596",
			"name": "Coefficients of 1/sqrt(1 - 12*x + 16*x^2); also, a(n) is the central coefficient of (1 + 6*x + 5*x^2)^n.",
			"comment": [
				"Diagonal of rational functions 1/(1 - x - y - 4*x*y), 1/(1 - x - y*z - 4*x*y*z). - _Gheorghe Coserea_, Jul 06 2018"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A084772/b084772.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"Hacène Belbachir and Abdelghani Mehdaoui, \u003ca href=\"https://doi.org/10.2989/16073606.2020.1729269\"\u003eRecurrence relation associated with the sums of square binomial coefficients\u003c/a\u003e, Quaestiones Mathematicae (2021) Vol. 44, Issue 5, 615-624.",
				"Hacène Belbachir, Abdelghani Mehdaoui, and László Szalay, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL22/Szalay/szalay42.html\"\u003eDiagonal Sums in the Pascal Pyramid, II: Applications\u003c/a\u003e, J. Int. Seq., Vol. 22 (2019), Article 19.3.5.",
				"H. A. Verrill, \u003ca href=\"http://arxiv.org/abs/math/0407327\"\u003eSums of squares of binomial coefficients, with applications to Picard-Fuchs equations\u003c/a\u003e, arXiv:math/0407327 [math.CO], 2008, Theorem 8."
			],
			"formula": [
				"a(n) = Sum_{k=0..n} 5^k*C(n,k)^2. - _Benoit Cloitre_, Oct 26 2003",
				"E.g.f.: exp(6*x)*BesselI(0, 2*sqrt(5)*x). - _Paul Barry_, Sep 20 2004",
				"Asymptotic: a(n) ~ (1+sqrt(5))^(2*n+1)/(2*5^(1/4)*sqrt(Pi*n)). - _Vaclav Kotesovec_, Sep 11 2012",
				"D-finite with recurrence: n*a(n) + 6*(1-2*n)*a(n-1) + 16*(n-1)*a(n-2) = 0. - _R. J. Mathar_, Nov 09 2012"
			],
			"example": [
				"G.f.: 1/sqrt(1 - 2*b*x + (b^2-4*c)*x^2) yields central coefficients of (1 + b*x + c*x^2)^n."
			],
			"mathematica": [
				"Table[n! SeriesCoefficient[E^(6 x) BesselI[0, 2 Sqrt[5] x], {x, 0, n}], {n, 0, 20}] (* _Vincenzo Librandi_, May 10 2013 *)",
				"CoefficientList[Series[1/Sqrt[1-12x+16x^2],{x,0,30}],x] (* _Harvey P. Dale_, Apr 17 2015 *)"
			],
			"program": [
				"(PARI) for(n=0,30,t=polcoeff((1+6*x+5*x^2)^n,n,x); print1(t\",\"))",
				"(GAP) List([0..20],n-\u003eSum([0..n],k-\u003eBinomial(n,k)^2*5^k)); # _Muniru A Asiru_, Jul 29 2018"
			],
			"xref": [
				"Cf. A001850."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Paul D. Hanna_, Jun 10 2003",
			"references": 4,
			"revision": 56,
			"time": "2021-09-08T19:08:57-04:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}