{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A202917",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 202917,
			"data": "1,1,1,1,6,1,1,1,1,1,1,60,10,60,1,1,1,10,10,1,1,1,126,21,1260,21,126,1,1,1,21,21,21,21,1,1",
			"name": "For n \u003e= 0, let n!^(1) = A053657(n+1) and, for 0 \u003c= m \u003c= n, C^(1)(n,m) = n!^(1)/(m!^(1)*(n-m)!^(1)). The sequence gives a triangle of numbers C^(1)(n,m) with rows of length n+1.",
			"comment": [
				"1) Note that A053657(n+1) is the LCM of the denominators of the coefficients of the polynomials Q^(1)_n(x) which, for integer x=k, are defined by the recursion Q^(1)_0(x)=1, for n\u003e=1, Q^(1)_n(x) = Sum_{i=1..k} i*Q^(1)_(n-1)(i). Also note that Q^(1)_n(k) = S(k+n,k), where the numbers S(l,m) are Stirling numbers of the second kind. The sequence of polynomials {Q^(1)_n(x)} includes the family of sequences of polynomials {{Q^(r)_n}}_(r\u003e=0) described in a comment at A175669. In particular, the LCM of the denominators of the coefficients of Q^(0)_n(x) is n!.",
				"2) This triangle differs from triangle A186430 which is defined according to the theory of factorials over sets by Bhargava. Unfortunately, this theory does not have a conversion theorem. Therefore it is not known if there is a set A such that n!^(1) = n!_A in the Bhargava sense.",
				"3) If p is an odd prime, then the (p-1)-th row contains two 1's and p-2 numbers that are multiples of p. For a conjectural generalization, see comment in A175669."
			],
			"formula": [
				"A007814(C^(1)(n,m)) = A007814(C(n,m))."
			],
			"example": [
				"Triangle begins",
				"n/m.|..0.....1.....2.....3.....4.....5.....6.....7",
				"==================================================",
				".0..|..1",
				".1..|..1.....1",
				".2..|..1.....6.....1",
				".3..|..1.....1 ... 1  .....1",
				".4..|..1....60....10......60.....1",
				".5..|..1.....1....10......10.....1.....1",
				".6..|..1...126....21....1260....21...126.....1",
				".7..|..1.....1....21......21....21....21.....1.....1",
				".8..|"
			],
			"mathematica": [
				"A053657[n_] := Product[p^Sum[Floor[(n-1)/((p-1) p^k)], {k, 0, n}], {p, Prime[Range[n]]}]; f1[n_] := A053657[n+1]; C1[n_, m_] := f1[n]/(f1[m] * f1[n-m]); Table[C1[n, m], {n, 0, 10}, {m, 0, n}] // Flatten (* _Jean-François Alcover_, Nov 22 2016 *)"
			],
			"xref": [
				"Cf. A175669, A053657, A202339, A202367, A202368, A202369"
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Vladimir Shevelev_ and _Peter J. C. Moses_, Dec 26 2011",
			"references": 10,
			"revision": 23,
			"time": "2017-03-24T00:33:24-04:00",
			"created": "2011-12-31T11:01:27-05:00"
		}
	]
}