{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A080332",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 80332,
			"data": "1,-5,7,0,0,-11,0,13,0,0,0,0,-17,0,0,19,0,0,0,0,0,0,-23,0,0,0,25,0,0,0,0,0,0,0,0,-29,0,0,0,0,31,0,0,0,0,0,0,0,0,0,0,-35,0,0,0,0,0,37,0,0,0,0,0,0,0,0,0,0,0,0,-41,0,0,0,0,0,0,43,0,0,0,0,0,0,0,0,0,0,0,0,0,0,-47,0,0,0,0,0,0",
			"name": "G.f.: Product_{n\u003e0} (1 - x^n)^3 * (1 - x^(2*n - 1))^2 = Sum_{n in Z} (6*n + 1) * x^(n*(3*n + 1)/2).",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"reference": [
				"J. M. Borwein, P. B. Borwein, Pi and the AGM, Wiley, 1987, p. 306.",
				"N. J. Fine, Basic Hypergeometric Series and Applications, Amer. Math. Soc., 1988; p. 83, Eq. (32.6); p. 84, Eq. (32.63).",
				"S. Ramanujan, Notebooks, Tata Institute of Fundamental Research, Bombay 1957 Vol. 1, see page 266.  MR0099904 (20 #6340)"
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A080332/b080332.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"G.f.: theta_4(x)^2 * (Sum_{n in Z} (-1)^n * x^(n*(3*n + 1)/2)).",
				"Expansion of f(-x)^2 * phi(x) = f(-x^2) * phi(-x^2)^2 in powers of x^2 where phi(), f() are Ramanujan theta functions. - _Michael Somos_, Feb 18 2003",
				"Expansion of q^(-1/24) * eta(q)^5 / eta(q^2)^2 in powers of q.",
				"Euler transform of period 2 sequence [-5, -3, ...]. - _Michael Somos_, Sep 09 2007",
				"a(n) = b(24*n + 1) where b(n) is multiplicative with b(2^e) = 0^e, b(3^e) = 0^e, b(p^e) = (1+(-1)^e)/2* p^(e/2) if p == 1 (mod 6), b(p^e) = (1+(-1)^e)/2 * (-p)^(e/2) if p == 5 (mod 6). - _Michael Somos_, May 26 2005",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (48 t)) = 32^(1/2) (t/i)^(3/2) g(t) where q = exp(2 Pi i t) and g() is the g.f. for A113277. - _Michael Somos_, Feb 18 2003",
				"a(5*n + 3) = a(5*n + 4) = a(7*n + 3) = a(7*n + 4) = a(7*n + 6) = 0. a(25*n + 1) = -5 * a(n). - _Michael Somos_, Feb 18 2003"
			],
			"example": [
				"G.f. = 1 - 5*x + 7*x^2 - 11*x^5 + 13*x^7 - 17*x^12 + 19*x^15 - 23*x^22 + ...",
				"G.f. = q - 5*q^25 + 7*q^49 - 11*q^121 + 13*q^169 - 17*q^289 + 19*q^361 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ x]^5 / QPochhammer[ x^2]^2, {x, 0, n}]; (* _Michael Somos_, Mar 11 2015 *)",
				"a[ n_] := SeriesCoefficient[ QPochhammer[ x] EllipticTheta[ 4, 0, x]^2, {x, 0, n}]; (* _Michael Somos_, Mar 11 2015 *)",
				"a[ n_] := SeriesCoefficient[ QPochhammer[ x]^2 EllipticTheta[ 3, 0, x], {x, 0, 2 n}]; (* _Michael Somos_, Mar 11 2015 *)",
				"a[ n_] := With[{m = Sqrt[24 n + 1]}, If[ IntegerQ[ m], m KroneckerSymbol[ -3, m], 0]]; (* _Michael Somos_, Mar 11 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A, p, e); if( n\u003c1, n==0, A = factor(24*n + 1); prod( k=1, matsize(A)[1], [p, e] = A[k,]; if( (p\u003c5) || (e%2), 0, if( p%6 == 1, p, -p)^(e\\2))))}; /* _Michael Somos_, May 26 2005 */",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A)^5 / eta(x^2 + A)^2, n))};",
				"(PARI) {a(n) = if( issquare( 24*n + 1, \u0026n), n * kronecker( -3, n), 0)};"
			],
			"xref": [
				"Cf. A010815, A113277."
			],
			"keyword": "sign,easy",
			"offset": "0,2",
			"author": "_Michael Somos_, Feb 18 2003",
			"ext": [
				"Definition changed by _N. J. A. Sloane_, Aug 14 2007"
			],
			"references": 5,
			"revision": 34,
			"time": "2021-01-04T06:21:40-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}