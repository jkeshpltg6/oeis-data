{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A280000",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 280000,
			"data": "1,0,1,1,3,5,12,25,57,128,296,688,1618,3839,9170,22065,53370,129807,317080,777887,1915247,4731932,11726476,29143123,72614115,181363151,453975928,1138697689,2861607677,7204169689",
			"name": "Number of free pure symmetric multifunctions in one symbol with n positions.",
			"comment": [
				"A free pure symmetric multifunction (PSM) in one symbol x is either (case 1) the symbol x, or (case 2) an expression of the form h[g_1,...,g_k] where h is a PSM in x, each of the g_i for i=1..(k\u003e0) is a PSM in x, and for i \u003c j we have g_i \u003c= g_j under a canonical total ordering such as the Mathematica ordering. The number of positions in a PSM is the number of brackets [...] plus the number of x's."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A280000/b280000.txt\"\u003eTable of n, a(n) for n = 1..200\u003c/a\u003e"
			],
			"example": [
				"Sequence of free pure symmetric multifunctions (second column) together with their numbers of positions (first column) and j-numbers (third column, see A279944 for details) begins:",
				"1 x            1",
				"3 x[x]         2",
				"4 x[x,x]       8",
				"5 x[x][x]      3",
				"5 x[x[x]]      4",
				"5 x[x,x,x]     128",
				"6 x[x,x][x]    12",
				"6 x[x][x,x]    27",
				"6 x[x,x[x]]    32",
				"6 x[x,x,x,x]   32768",
				"6 x[x[x,x]]    262144",
				"7 x[x][x][x]   5",
				"7 x[x[x]][x]   6",
				"7 x[x][x[x]]   9",
				"7 x[x[x][x]]   16",
				"7 x[x[x[x]]]   64",
				"7 x[x,x,x][x]  145",
				"7 x[x,x][x,x]  1728",
				"7 x[x,x,x[x]]  2048",
				"7 x[x][x,x,x]  2187",
				"7 x[x,x,x,x,x] 2147483648",
				"7 x[x,x[x,x]]  137438953472",
				"7 x[x[x,x,x]]  1378913...3030144"
			],
			"mathematica": [
				"multing[t_,n_]:=Array[(t+#-1)/#\u0026,n,1,Times];",
				"a[n_]:=If[n===1,1,Sum[a[k]*Sum[Product[multing[a[First[s]],Length[s]],{s,Split[p]}],{p,IntegerPartitions[n-k-1]}],{k,1,n-2}]];",
				"Array[a,15]"
			],
			"program": [
				"(PARI) EulerT(v)={Vec(exp(x*Ser(dirmul(v,vector(#v,n,1/n))))-1, -#v)}",
				"seq(n)={my(v=[1]); for(n=2, n, my(t=EulerT(v)); v=concat(v, sum(k=1, n-2, v[k]*t[n-k-1]))); v} \\\\ _Andrew Howroyd_, Aug 19 2018"
			],
			"xref": [
				"Cf. A005043 (non-symmetric case), A279944."
			],
			"keyword": "nonn",
			"offset": "1,5",
			"author": "_Gus Wiseman_, Dec 24 2016",
			"references": 28,
			"revision": 8,
			"time": "2018-08-19T16:46:12-04:00",
			"created": "2016-12-24T14:26:17-05:00"
		}
	]
}