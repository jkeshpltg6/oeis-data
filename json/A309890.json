{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A309890",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 309890,
			"data": "1,1,2,1,1,2,2,4,4,1,1,2,1,1,2,2,4,4,2,4,4,5,5,8,5,5,9,1,1,2,1,1,2,2,4,4,1,1,2,1,1,2,2,4,4,2,4,4,5,5,8,5,5,9,2,4,4,5,5,10,5,5,10,10,11,13,10,11,10,11,13,10,10,12,13,10,13,11,12,20,11,1,1,2,1,1,2,2,4,4,1,1,2,1,1,2,2,4,4,2",
			"name": "Lexicographically earliest sequence of positive integers without triples in weakly increasing arithmetic progression.",
			"comment": [
				"Formal definition: lexicographically earliest sequence of positive integers a(n) such that for any i \u003e 0, there is no n \u003e 0 such that 2a(n+i) = a(n) + a(n+2i) AND a(n) \u003c= a(n+i) \u003c= a(n+2i).",
				"Sequence suggested by _Richard Stanley_ as a variant of A229037. They differ from the 55th term. The numbers n for which a(n) = 1 are given by A003278, or equally by A005836 (_Richard Stanley_).",
				"The sequence defined by c(n) = 1 if a(n) = 1 and otherwise c(n) = 0 is A039966 (although with a different offset). - _N. J. A. Sloane_, Dec 01 2019",
				"Pleasant to listen to (button above) with Instrument no. 13: Marimba (and for better listening, save and convert to MP3)."
			],
			"link": [
				"Sébastien Palcoux, \u003ca href=\"/A309890/b309890.txt\"\u003eTable of n, a(n) for n = 1..100000\u003c/a\u003e",
				"S. Palcoux, \u003ca href=\"https://mathoverflow.net/q/338415\"\u003eOn the first sequence without triple in arithmetic progression\u003c/a\u003e (version: 2019-08-21), second part, MathOverflow",
				"Sébastien Palcoux, \u003ca href=\"/A309890/a309890.txt\"\u003eTable of n, a(n) for n = 1..1000000\u003c/a\u003e",
				"Sébastien Palcoux, \u003ca href=\"/A309890/a309890.png\"\u003eDensity plot of the first 1000000 terms\u003c/a\u003e"
			],
			"program": [
				"(SageMath)",
				"# %attach SAGE/ThreeFree.spyx",
				"from sage.all import *",
				"cpdef ThreeFree(int n):",
				"     cdef int i,j,k,s,Li,Lj",
				"     cdef list L,Lb",
				"     cdef set b",
				"     L=[1,1]",
				"     for k in range(2,n):",
				"          b=set()",
				"          for i in range(k):",
				"               if 2*((i+k)/2)==i+k:",
				"                    j=(i+k)/2",
				"                    Li,Lj=L[i],L[j]",
				"                    s=2*Lj-Li",
				"                    if s\u003e0 and Li\u003c=Lj:",
				"                         b.add(s)",
				"          if 1 not in b:",
				"               L.append(1)",
				"          else:",
				"               Lb=list(b)",
				"               Lb.sort()",
				"               for t in Lb:",
				"                    if t+1 not in b:",
				"                         L.append(t+1)",
				"                         break",
				"     return L"
			],
			"xref": [
				"Cf. A003278, A005836, A039966, A229037."
			],
			"keyword": "nonn,look,easy,hear",
			"offset": "1,3",
			"author": "_Sébastien Palcoux_, Aug 21 2019",
			"references": 5,
			"revision": 58,
			"time": "2019-12-01T16:57:56-05:00",
			"created": "2019-08-21T20:23:19-04:00"
		}
	]
}