{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A285328",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 285328,
			"data": "1,1,1,2,1,1,1,4,3,1,1,6,1,1,1,8,1,12,1,10,1,1,1,18,5,1,9,14,1,1,1,16,1,1,1,24,1,1,1,20,1,1,1,22,15,1,1,36,7,40,1,26,1,48,1,28,1,1,1,30,1,1,21,32,1,1,1,34,1,1,1,54,1,1,45,38,1,1,1,50,27,1,1,42,1,1,1,44,1,60,1,46,1,1,1,72,1,56,33,80,1,1,1,52,1,1,1,96",
			"name": "a(n) = 1 if n is squarefree (A005117), otherwise a(n) = Max {m \u003c n | same prime factors as n, ignoring multiplicity}.",
			"link": [
				"Antti Karttunen, \u003ca href=\"/A285328/b285328.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"If A008683(n) \u003c\u003e 0, a(n) = 1, otherwise a(n) = the largest number k \u003c n for which A007947(k) = A007947(n).",
				"Other identities. For all n \u003e= 1:",
				"a(A065642(n)) = n."
			],
			"example": [
				"From _Michael De Vlieger_, Dec 31 2018: (Start)",
				"a(1) = 1 since 1 is squarefree.",
				"a(2) = 1 since 2 is squarefree.",
				"a(4) = 2 since 4 is not squarefree and 2 is the largest number less than 4 that has all the distinct prime divisors that 4 has.",
				"a(6) = 1 since 6 is squarefree.",
				"a(12) = 6 since 12 is not squarefree and 6 is the largest number less than 12 that has all the distinct prime divisors that 12 has. (6 is also the squarefree root of 12).",
				"a(16) = 8 since 16 is not squarefree and 8 is the largest number less than 16 that has all the distinct prime divisors that 16 has.",
				"a(18) = 12 since 18 is not squarefree and 12 is the largest number less than 18 that has all the distinct prime divisors that 18 has.",
				"(End)"
			],
			"mathematica": [
				"Table[With[{r = DivisorSum[n, EulerPhi[#] Abs@ MoebiusMu[#] \u0026]}, If[MoebiusMu@ n != 0, 1, SelectFirst[Range[n - 2, 2, -1], DivisorSum[#, EulerPhi[#] Abs@ MoebiusMu[#] \u0026] == r \u0026]]], {n, 108}] (* _Michael De Vlieger_, Dec 31 2018 *)"
			],
			"program": [
				"(Scheme)",
				"(definec (A285328 n) (if (not (zero? (A008683 n))) 1 (let ((k (A007947 n))) (let loop ((n (- n k))) (if (= (A007947 n) k) n (loop (- n k)))))))",
				"(PARI)",
				"A007947(n) = factorback(factorint(n)[, 1]); \\\\ From _Andrew Lelechenko_, May 09 2014",
				"A285328(n) = { my(r=A007947(n)); if(core(n)==n,1,n = n-r; while(A007947(n) \u003c\u003e r, n = n-r); n); }; \\\\ After Python-code below - _Antti Karttunen_, Apr 17 2017",
				"A285328(n) = { my(r); if((n \u003e 1 \u0026\u0026 !bitand(n,(n-1))),(n/2), r=A007947(n); if(r==n,1,n = n-r; while(A007947(n) \u003c\u003e r, n = n-r); n)); }; \\\\ Version optimized for powers of 2.",
				"(Python)",
				"from operator import mul",
				"from sympy import primefactors",
				"from sympy.ntheory.factor_ import core",
				"def a007947(n): return 1 if n\u003c2 else reduce(mul, primefactors(n))",
				"def a(n):",
				"    if core(n) == n: return 1",
				"    r = a007947(n)",
				"    k = n - r",
				"    while k\u003e0:",
				"        if a007947(k) == r: return k",
				"        else: k -= r",
				"print([a(n) for n in range(1, 121)]) # _Indranil Ghosh_ and _Antti Karttunen_, Apr 17 2017"
			],
			"xref": [
				"A left inverse of A065642.",
				"Cf. A005117, A007947, A008479, A008683, A284571, A285111, A285331, A285329.",
				"Cf. also A079277."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Antti Karttunen_, Apr 17 2017",
			"references": 16,
			"revision": 34,
			"time": "2021-03-13T12:43:49-05:00",
			"created": "2017-04-18T15:43:58-04:00"
		}
	]
}