{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A265381",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 265381,
			"data": "1,3,7,14,29,59,119,238,477,955,1911,3822,7645,15291,30583,61166,122333,244667,489335,978670,1957341,3914683,7829367,15658734,31317469,62634939,125269879,250539758,501079517,1002159035,2004318071,4008636142,8017272285",
			"name": "Decimal representation of the middle column of the \"Rule 158\" elementary cellular automaton starting with a single ON (black) cell.",
			"link": [
				"Robert Price, \u003ca href=\"/A265381/b265381.txt\"\u003eTable of n, a(n) for n = 0..999\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ElementaryCellularAutomaton.html\"\u003eElementary Cellular Automaton\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Rule158.html\"\u003eRule 158\u003c/a\u003e",
				"Stephen Wolfram, \u003ca href=\"http://wolframscience.com/\"\u003eA New Kind of Science\u003c/a\u003e, Wolfram Media, 2002; p. 55.",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_Elementary_Cellular_Automata\"\u003eIndex to Elementary Cellular Automata\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,0,0,1,-2)."
			],
			"formula": [
				"From _Colin Barker_, Dec 07 2015 and Apr 16 2019: (Start)",
				"a(n) = (-45+5*(-1)^n-(6-i*3)*(-i)^n-(6+3*i)*i^n+7*2^(4+n))/60 where i = sqrt(-1).",
				"a(n) = 2*a(n-1)+a(n-4)-2*a(n-5) for n\u003e4.",
				"G.f.: (1+x+x^2) / ((1-x)*(1+x)*(1-2*x)*(1+x^2)).",
				"(End)",
				"a(n) = floor(7*2^(n+2)/15) for n\u003e=0. - _Karl V. Keller, Jr._, Oct 01 2020"
			],
			"example": [
				"From _Michael De Vlieger_, Dec 09 2015: (Start)",
				"First 8 rows at left, ignoring \"0\" outside of range of 1's, the center column values in parentheses. The center column values up to that row are concatenated then converted into decimal at right:",
				"             Rule 158                   Binary     Decimal",
				"                (1)                 -\u003e         1 =   1",
				"              1 (1) 1               -\u003e        11 =   3",
				"            1 1 (1) 0 1             -\u003e       111 =   7",
				"          1 1 1 (0) 0 1 1           -\u003e      1110 =  14",
				"        1 1 1 0 (1) 1 1 0 1         -\u003e     11101 =  29",
				"      1 1 1 0 0 (1) 1 0 0 1 1       -\u003e    111011 =  59",
				"    1 1 1 0 1 1 (1) 0 1 1 1 0 1     -\u003e   1110111 = 119",
				"  1 1 1 0 0 1 1 (0) 0 1 1 0 0 1 1   -\u003e  11101110 = 238",
				"1 1 1 0 1 1 1 0 (1) 1 1 0 1 1 1 0 1 -\u003e 111011101 = 477",
				"(End)"
			],
			"mathematica": [
				"f[n_] := Block[{w = {}}, Do[AppendTo[w, Boole[Mod[k, 4] != 3]], {k, 0, n}]; FromDigits[w, 2]]; Table[f@ n, {n, 0, 32}] (* _Michael De Vlieger_, Dec 09 2015 *)"
			],
			"program": [
				"(Python) print([7*2**(n+2)//15 for n in range(34)]) # _Karl V. Keller, Jr._, Oct 01 2020"
			],
			"xref": [
				"Cf. A071037, A265380 (binary)."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Robert Price_, Dec 07 2015",
			"references": 4,
			"revision": 27,
			"time": "2020-10-24T17:05:54-04:00",
			"created": "2015-12-13T08:08:46-05:00"
		}
	]
}