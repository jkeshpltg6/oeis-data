{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A341915",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 341915,
			"data": "0,1,3,2,5,7,6,4,9,13,15,11,10,14,12,8,17,25,29,21,23,31,27,19,18,26,30,22,20,28,24,16,33,49,57,41,45,61,53,37,39,55,63,47,43,59,51,35,34,50,58,42,46,62,54,38,36,52,60,44,40,56,48,32,65,97,113",
			"name": "For any nonnegative number n with runs in binary expansion (r_1, ..., r_w), a(n) = Sum_{k = 1..w} 2^(r_1 + ... + r_k - 1).",
			"comment": [
				"This sequence is a permutation of the nonnegative integers with inverse A341916.",
				"This sequence has connections with A003188; here we compute partials sums of runs from left to right, there from right to left."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A341915/b341915.txt\"\u003eTable of n, a(n) for n = 0..8191\u003c/a\u003e",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A059893(A003188(n)).",
				"a(n) = Sum_{k = 1..A005811(n)} 2^((Sum_{m = 1..k} A101211(m))-1).",
				"a(n) \u003c 2^k for any n \u003c 2^k.",
				"A000120(a(n)) = A000120(A003188(n)) = A005811(n)."
			],
			"example": [
				"For n = 23,",
				"- the binary representation of 23 is \"10111\",",
				"- the corresponding run lengths are (1, 1, 3),",
				"- so a(23) = 2^(1-1) + 2^(1+1-1) + 2^(1+1+3-1) = 19."
			],
			"mathematica": [
				"a[n_] := If[n == 0, 0, 2^((Length /@ Split[IntegerDigits[n, 2]] // Accumulate)-1) // Total];",
				"Table[a[n], {n, 0, 100}] (* _Jean-François Alcover_, Jan 02 2022 *)"
			],
			"program": [
				"(PARI) a(n) = { my (v=0); while (n, my (w=valuation(n+n%2,2)); n\\=2^w; v=2^w*(1+v)); v/2 }"
			],
			"xref": [
				"Cf. A003188, A005811, A059893, A101211, A341916 (inverse), A341943 (fixed points)."
			],
			"keyword": "nonn,look,base",
			"offset": "0,3",
			"author": "_Rémy Sigrist_, Feb 23 2021",
			"references": 3,
			"revision": 19,
			"time": "2022-01-02T07:25:45-05:00",
			"created": "2021-02-27T11:18:44-05:00"
		}
	]
}