{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A115627",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 115627,
			"data": "1,1,1,3,1,3,1,1,4,2,1,4,2,1,1,7,2,1,1,7,4,1,1,8,4,2,1,8,4,2,1,1,10,5,2,1,1,10,5,2,1,1,1,11,5,2,2,1,1,11,6,3,2,1,1,15,6,3,2,1,1,15,6,3,2,1,1,1,16,8,3,2,1,1,1,16,8,3,2,1,1,1,1",
			"name": "Irregular triangle read by rows: T(n,k) = multiplicity of prime(k) as a divisor of n!.",
			"comment": [
				"The factorization of n! is n! = 2^T(n,1)*3^T(n,2)*...*p_(pi(n))^T(n,pi(n)) where p_k = k-th prime, pi(n) = A000720(n).",
				"Nonzero terms of A085604; T(n,k) = A085604(n,k), k = 1..A000720(n). - _Reinhard Zumkeller_, Nov 01 2013",
				"For n=2, 3, 4 and 5, all terms of the n-th row are odd. Are there other such rows? - _Michel Marcus_, Nov 11 2018",
				"From _Gus Wiseman_, May 15 2019: (Start)",
				"Differences between successive rows are A067255, so row n is the sum of the first n row-vectors of A067255 (padded with zeros on the right so that all n row-vectors have length A000720(n)). For example, the first 10 rows of A067255 are",
				"  {}",
				"  1",
				"  0 1",
				"  2 0",
				"  0 0 1",
				"  1 1 0",
				"  0 0 0 1",
				"  3 0 0 0",
				"  0 2 0 0",
				"  1 0 1 0",
				"with column sums (8,4,2,1), which is row 10.",
				"(End)",
				"For all prime p \u003e 7, 3*p \u003e 2*nextprime(p), so for any n \u003e 21 there will always be a prime p dividing n! with exponent 2 and there are no further rows with all entries odd. - _Charlie Neder_, Jun 03 2019"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A115627/b115627.txt\"\u003eRows n = 2..300, flattened\u003c/a\u003e",
				"H. T. Davis, \u003ca href=\"/A002443/a002443.pdf\"\u003eTables of the Mathematical Functions\u003c/a\u003e, Vols. 1 and 2, 2nd ed., 1963, Vol. 3 (with V. J. Fisher), 1962; Principia Press of Trinity Univ., San Antonio, TX. [Annotated scan of pages 204-208 of Volume 2.] See Table 2 on page 206.",
				"Wenguang Zhai, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2009.02.016\"\u003eOn the prime power factorization of n!\u003c/a\u003e, Journal of Number Theory, Volume 129, Issue 8, August 2009, pages 1820-1836.",
				"\u003ca href=\"/index/Fa#factorial\"\u003eIndex entries for sequences related to factorial numbers\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = Sum_{i=1..inf} floor(n/(p_k)^i). (Although stated as an infinite sum, only finitely many terms are nonzero.)",
				"T(n,k) = Sum_{i=1..floor(log(n)/log(p_k)} floor(u_i) where u_0 = n and u_(i+1) = floor((u_i)/p_k). - _David A. Corneth_, Jun 22 2014"
			],
			"example": [
				"From _Gus Wiseman_, May 09 2019: (Start)",
				"Triangle begins:",
				"   1",
				"   1  1",
				"   3  1",
				"   3  1  1",
				"   4  2  1",
				"   4  2  1  1",
				"   7  2  1  1",
				"   7  4  1  1",
				"   8  4  2  1",
				"   8  4  2  1  1",
				"  10  5  2  1  1",
				"  10  5  2  1  1  1",
				"  11  5  2  2  1  1",
				"  11  6  3  2  1  1",
				"  15  6  3  2  1  1",
				"  15  6  3  2  1  1  1",
				"  16  8  3  2  1  1  1",
				"  16  8  3  2  1  1  1  1",
				"  18  8  4  2  1  1  1  1",
				"(End)",
				"m such that 5^m||101!: floor(log(101)/log(5)) = 2 terms. floor(101/5) = 20. floor(20/5) = 4. So m = u_1 + u_2 = 20 + 4 = 24. - _David A. Corneth_, 22 Jun 2014"
			],
			"maple": [
				"A115627 := proc(n,k) local d,p; p := ithprime(k) ; n-add(d,d=convert(n,base,p)) ; %/(p-1) ; end proc: # _R. J. Mathar_, Oct 29 2010"
			],
			"mathematica": [
				"Flatten[Table[Transpose[FactorInteger[n!]][[2]], {n, 2, 20}]] (* _T. D. Noe_, Apr 10 2012 *)",
				"T[n_, k_] := Module[{p, jm}, p = Prime[k]; jm = Floor[Log[p, n]]; Sum[Floor[n/p^j], {j, 1, jm}]]; Table[Table[T[n, k], {k, 1, PrimePi[n]}], {n, 2, 20}] // Flatten (* _Jean-François Alcover_, Feb 23 2015 *)"
			],
			"program": [
				"(Haskell)",
				"a115627 n k = a115627_tabf !! (n-2) !! (k-1)",
				"a115627_row = map a100995 . a141809_row . a000142",
				"a115627_tabf = map a115627_row [2..]",
				"-- _Reinhard Zumkeller_, Nov 01 2013",
				"(PARI) a(n)=my(i=2);while(n-primepi(i)\u003e1,n-=primepi(i);i++);p=prime(n-1);sum(j=1,log(i)\\log(p),i\\=p) \\\\ _David A. Corneth_, 21 Jun 2014"
			],
			"xref": [
				"Row lengths are A000720.",
				"Row-sums are A022559.",
				"Row-products are A135291.",
				"Row maxima are A011371.",
				"Columns include A011371, A054861, A027868, A054896, A090617, A064458, A090620.",
				"Cf. A090622, A090623, A000142, A115628.",
				"Cf. A085604, A141809.",
				"Cf. A034876, A067255, A071626, A076934, A322583, A325272, A325273, A325276, A325508, A325509."
			],
			"keyword": "nonn,tabf",
			"offset": "2,4",
			"author": "_Franklin T. Adams-Watters_, Jan 26 2006",
			"references": 30,
			"revision": 81,
			"time": "2019-06-11T00:11:32-04:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}