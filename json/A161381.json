{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A161381",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 161381,
			"data": "1,1,2,1,4,8,1,6,24,48,1,8,48,192,384,1,10,80,480,1920,3840,1,12,120,960,5760,23040,46080,1,14,168,1680,13440,80640,322560,645120,1,16,224,2688,26880,215040,1290240,5160960,10321920,1,18,288,4032,48384,483840,3870720,23224320,92897280,185794560",
			"name": "Triangle read by rows: T(n,k) = n!*2^k/(n-k)! (n \u003e= 0, 0 \u003c= k \u003c= n).",
			"comment": [
				"From _Dennis P. Walsh_, Nov 20 2012: (Start)",
				"T(n,k) is the number of functions f:[k]-\u003e[2n] such that, if f(x)=f(y) or f(x)=2n+1-f(y), then x=y.",
				"We call such functions injective-plus.",
				"Equivalently, T(n,k) gives the number of ways to select k couples from n couples, then choose one person from each of the k selected couples, and then arrange those k individuals in a line. For example, T(50,10) is the number of ways to select 10 U.S. senators, one from each of ten different states, and arrange the senators in a reception line for a visiting dignitary. (End)"
			],
			"link": [
				"Mathieu Guay-Paquet and Jeffrey Shallit, \u003ca href=\"http://arxiv.org/abs/0901.1397\"\u003eAvoiding Squares and Overlaps Over the Natural Numbers\u003c/a\u003e, arXiv:0901.1397 [math.CO], 2009.",
				"Mathieu Guay-Paquet and Jeffrey Shallit, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2009.06.004\"\u003eAvoiding Squares and Overlaps Over the Natural Numbers\u003c/a\u003e, Discrete Math., 309 (2009), 6245-6254.",
				"Dennis Walsh, \u003ca href=\"http://frank.mtsu.edu/~dwalsh/INJECTM3.pdf\"\u003eNotes on injective-plus functions\u003c/a\u003e"
			],
			"formula": [
				"From _Dennis P. Walsh_, Nov 20 2012: (Start)",
				"E.g.f. for column k : exp(x)*(2*x)^k.",
				"G.f.  for column k : (2*x)^k*k!/(1 - x)^(k+1).",
				"T(n,k) = 2^(k-n)*Sum_{j = 0..n} (binomial(n,j)T(j,i)T(n-j,k-i). (End)",
				"From _Peter Bala_, Feb 20 2016: (Start)",
				"T(n, k) = 2*n*T(n-1, k-1) = 2*k*T(n-1, k-1) + T(n-1, k) = n*T(n-1, k)/(n - k) = 2*(n - k + 1)*T(n, k-1).",
				"G.f. Sum_{n \u003e= 1} (2*n*x*t)^(n-1)/(1 - (2*n*t - 1)*x)^n = 1 + (1 + 2*t)*x + (1 + 4*t + 8*t^2)*x^2 + ....",
				"E.g.f. exp(x)/(1 - 2*x*t) =  1 + (1 + 2*t)*x + (1 + 4*t + 8*t^2)*x^2/2! + ....",
				"E.g.f. for row n: (1 + 2*x)^n",
				"Row reversed triangle is the exponential Riordan array [1/(1 - 2*x), x]. (End)"
			],
			"example": [
				"Triangle begins:",
				"  1",
				"  1  2",
				"  1  4  8",
				"  1  6 24  48",
				"  1  8 48 192  384",
				"  1 10 80 480 1920 3840",
				"For n=2 and k=2, T(2,2)=8 since there are exactly 8 functions f from {1,2} to {1,2,3,4} that are injective-plus. Letting f = \u003cf(1),f(2)\u003e, the 8 functions are \u003c1,2\u003e, \u003c1,3\u003e, \u003c2,1\u003e, \u003c2,4\u003e, \u003c3,1\u003e, \u003c3,4\u003e, \u003c4,2\u003e,and \u003c4,3\u003e. - _Dennis P. Walsh_, Nov 20 2012"
			],
			"maple": [
				"seq(seq(2^k*n!/(n-k)!,k=0..n),n=0..20); # _Dennis P. Walsh_, Nov 20 2012"
			],
			"mathematica": [
				"Flatten@Table[Pochhammer[n - k + 1, k] 2^k, {n, 0, 20}, {k, 0, n}] (* J. Mulder (jasper.mulder(AT)planet.nl), Jan 28 2010 *)"
			],
			"program": [
				"(MAGMA) /* As triangle */ [[Factorial(n)*2^k/Factorial((n-k)): k in [0..n]]: n in [0.. 15]]; // _Vincenzo Librandi_, Dec 23 2015"
			],
			"xref": [
				"A010844 (row sums). Cf. A008279."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_, Nov 28 2009",
			"ext": [
				"More terms from J. Mulder (jasper.mulder(AT)planet.nl), Jan 28 2010"
			],
			"references": 0,
			"revision": 30,
			"time": "2017-03-24T00:47:51-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}