{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001999",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1999,
			"id": "M3055 N1239",
			"data": "3,18,5778,192900153618,7177905237579946589743592924684178",
			"name": "a(n) = a(n-1)*(a(n-1)^2 - 3).",
			"comment": [
				"The next terms in the sequence contain 102 and 305 digits. - _Harvey P. Dale_, Jun 09 2011",
				"From _Peter Bala_, Nov 13 2012: (Start)",
				"The present sequence is the case x = 3 of the following general remarks. For other cases see A219160 (x = 4), A219161 (x = 5) and A112845 (x = 6).",
				"Let x \u003e 2 and let alpha := {x + sqrt(x^2 - 4)}/2. Define a sequence a(n) (which depends on x) by setting a(n) = alpha^(3^n) + (1/alpha)^(3^n). Then it is easy to verify that the sequence a(n) satisfies the recurrence equation a(n+1) = a(n)^3 - 3*a(n) with the initial condition a(0) = x.",
				"We have the following identity, valid for x \u003e 2: sqrt((x + 2)/(x - 2)) = (1 + 2/(x-1))*sqrt((y + 2)/(y - 2)), where y = x^3 - 3*x. Iterating the identity produces the product expansion sqrt((x+2)/(x-2)) = product {n = 0..inf} (1 + 2/(a(n) - 1)), with a(0) = x and a(n+1) = a(n)^3 - 3*a(n). The rate of convergence is cubic (Fine).",
				"For similar results to the above see A001566 and A219162.",
				"(End)"
			],
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A001999/b001999.txt\"\u003eTable of n, a(n) for n = 0..7\u003c/a\u003e",
				"A. V. Aho and N. J. A. Sloane, \u003ca href=\"https://www.fq.math.ca/Scanned/11-4/aho-a.pdf\"\u003eSome doubly exponential sequences\u003c/a\u003e, Fibonacci Quarterly, Vol. 11, No. 4 (1973), pp. 429-437; \u003ca href=\"http://neilsloane.com/doc/doubly.html\"\u003ealternative link\u003c/a\u003e.",
				"E. B. Escott, \u003ca href=\"http://www.jstor.org/stable/2301484\"\u003eRapid method for extracting a square root\u003c/a\u003e, Amer. Math. Monthly, Vol. 44, No. 10 (1937), pp. 644-646.",
				"N. J. Fine, \u003ca href=\"http://www.jstor.org/stable/2321014\"\u003eInfinite products for k-th roots\u003c/a\u003e, Amer. Math. Monthly Vol. 84, No. 8 (Oct. 1977), pp. 629-630.",
				"Walther Janous, \u003ca href=\"https://www.fq.math.ca/Scanned/39-2/elementary39-2.pdf\"\u003eProblem B-916\u003c/a\u003e, Elementary Problems and Solutions, The Fibonacci Quarterly, Vol. 39, No. 2 (2001), p. 181; \u003ca href=\"https://www.fq.math.ca/Scanned/40-1/elementary40-1.pdf\"\u003eSubscript Is Power\u003c/a\u003e, Solution to Problem B-916 by H.-J. Seiffert, ibid., Vol. 40, No. 1 (2002), p. 86.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PierceExpansion.html\"\u003ePierce Expansion\u003c/a\u003e."
			],
			"formula": [
				"a(n) = 2*F(2*3^n+1) - F(2*3^n) = ceiling(tau^(2*3^n)) where F(k) = A000045(k) is the k-th Fibonacci number and tau is the golden ratio. - _Benoit Cloitre_, Nov 29 2002",
				"From _Peter Bala_, Nov 13 2012: (Start)",
				"a(n) = ((3 + sqrt(5))/2)^(3^n) + ((3 - sqrt(5))/2)^(3^n).",
				"Product_{n \u003e= 0} (1 + 2/(a(n) - 1)) = sqrt(5).",
				"a(n) = A002814(n+1) + 1.",
				"(End)",
				"a(n) = 2*T(3^n,3/2), where T(n,x) denotes the n-th Chebyshev polynomial of the first kind. Cf. A219161. - _Peter Bala_, Feb 01 2017",
				"From _Amiram Eldar_, Jan 12 2022: (Start)",
				"a(n) = A000032(2*3^n).",
				"a(n) = A006267(n)^2 + 2.",
				"Product_{k=0..n} (a(k)-1) = Fibonacci(3^(n+1)) = A045529(n+1) (Janous, 2001). (End)"
			],
			"mathematica": [
				"NestList[#(#^2-3)\u0026,3,6] (* _Harvey P. Dale_, Jun 09 2011 *)",
				"RecurrenceTable[{a[n] == a[n - 1]^3 - 3*a[n - 1], a[0] == 3}, a, {n,",
				"  0, 5}] (* _G. C. Greubel_, Dec 30 2016 *)"
			],
			"program": [
				"(PARI) a(n)=2*fibonacci(2*3^n+1)-fibonacci(2*3^n)"
			],
			"xref": [
				"Cf. A000032, A006276, A001566, A002814, A045529, A112845, A219160, A219161, A219162."
			],
			"keyword": "nonn,easy,nice,changed",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_",
			"references": 14,
			"revision": 51,
			"time": "2022-01-12T10:58:11-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}