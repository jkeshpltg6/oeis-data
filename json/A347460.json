{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A347460",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 347460,
			"data": "1,1,1,2,1,2,1,3,2,2,1,4,1,2,2,4,1,4,1,4,2,2,1,6,2,2,3,4,1,5,1,5,2,2,2,7,1,2,2,6,1,5,1,4,4,2,1,8,2,4,2,4,1,5,2,6,2,2,1,10,1,2,4,6,2,5,1,4,2,5,1,10,1,2,4,4,2,5,1,8,4,2,1,10,2,2",
			"name": "Number of distinct possible alternating products of factorizations of n.",
			"comment": [
				"We define the alternating product of a sequence (y_1,...,y_k) to be Product_i y_i^((-1)^(i-1)).",
				"A factorization of n is a weakly increasing sequence of positive integers \u003e 1 with product n."
			],
			"example": [
				"The a(n) alternating products for n = 1, 4, 8, 12, 24, 30, 36, 48, 60, 120:",
				"  1  4  8    12   24   30    36   48    60    120",
				"     1  2    3    6    10/3  9    12    15    30",
				"        1/2  3/4  8/3  5/6   4    16/3  20/3  40/3",
				"             1/3  2/3  3/10  1    3     15/4  15/2",
				"                  3/8  2/15  4/9  3/4   12/5  24/5",
				"                  1/6        1/4  1/3   3/5   10/3",
				"                             1/9  3/16  5/12  5/6",
				"                                  1/12  4/15  8/15",
				"                                        3/20  3/10",
				"                                        1/15  5/24",
				"                                              2/15",
				"                                              3/40",
				"                                              1/30"
			],
			"mathematica": [
				"facs[n_]:=If[n\u003c=1,{{}},Join@@Table[Map[Prepend[#,d]\u0026,Select[facs[n/d],Min@@#\u003e=d\u0026]],{d,Rest[Divisors[n]]}]];",
				"altprod[q_]:=Product[q[[i]]^(-1)^(i-1),{i,Length[q]}];",
				"Table[Length[Union[altprod/@facs[n]]],{n,100}]"
			],
			"xref": [
				"Positions of 1's are 1 and A000040.",
				"Positions of 2's appear to be A001358.",
				"Positions of 3's appear to be A030078.",
				"Dominates A038548, the version for reverse-alternating product.",
				"Counting only integers gives A046951.",
				"The even-length case is A072670.",
				"The version for partitions (not factorizations) is A347461, reverse A347462.",
				"The odd-length case is A347708.",
				"The length-3 case is A347709.",
				"A001055 counts factorizations (strict A045778, ordered A074206).",
				"A056239 adds up prime indices, row sums of A112798.",
				"A103919 counts partitions by sum and alternating sum (reverse: A344612).",
				"A108917 counts knapsack partitions, ranked by A299702.",
				"A276024 counts distinct positive subset-sums of partitions, strict A284640.",
				"A292886 counts knapsack factorizations, by sum A293627.",
				"A299701 counts distinct subset-sums of prime indices, positive A304793.",
				"A301957 counts distinct subset-products of prime indices.",
				"A304792 counts distinct subset-sums of partitions.",
				"Cf. A002033, A119620, A143823, A325770, A339846, A339890, A347437, A347438, A347439, A347440, A347442, A347456."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Gus Wiseman_, Oct 06 2021",
			"references": 21,
			"revision": 9,
			"time": "2021-10-27T22:22:45-04:00",
			"created": "2021-10-27T22:22:45-04:00"
		}
	]
}