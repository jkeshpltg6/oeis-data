{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A226697",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 226697,
			"data": "37,62,43,56,35,60,41,50,44,55,36,61,42,49,34,59,63,38,53,46,57,40,51,48,54,45,64,39,52,47,58,33,1,26,15,20,7,32,13,22,16,19,8,25,14,21,6,31,27,2,17,10,29,4,23,12,18,9,28,3,24,11,30,5",
			"name": "Central symmetric closed knight's tour on an 8x8 board, attributed to Euler. (n,m) position after (a(n,m)-1)th move.",
			"comment": [
				"Each row length is 8, and there are 8 rows.",
				"This is an 8x8 matrix with entries a(n,m) = a(8*(n-1) + m), n, m =1, 2, ..., 8, with the given length 64 sequence {a(n)}. The knight starts (move No. 0) at the square with position (n,m) = (5,1) because a(5,1) = 1. The square position (n,m) of the knight after move No. k is found from a(n,m) = k + 1, for k = 0, 1, ..., 63. See A226698 for the inverse sequence:",
				"  A226698(a(n)-1) = n, n= 1, 2, ..., 64.",
				"This is a Hamiltonian path which is re-entrant because with move No. 64 one can close the path at position (5,1) to obtain a Hamiltonian circuit.",
				"In the Gardner reference this path is attributed to L. Euler (1759). The lower half of the board is covered first. The closed path has central symmetry. Each symmetric pair has an absolute difference of 32: |a(n,m) - a(9-n,9-m)| = 32, n,m =1, 2, ..., 8.",
				"For a figure with this central symmetric Hamiltonian cycle see also Diagram 4 on p. 24 of the Elkies-Stanley reference."
			],
			"reference": [
				"Martin Gardner, Mathematical Magic Show, The Math. Assoc. of Am., Washington DC, 1989, Ch. 14, Knights of the Square Table,Fig. 86, p. 191. German Translation: Mathematische Hexereien, Ullstein, 1977, Abb. 86, S. 186."
			],
			"link": [
				"N. D. Elkies and R. P. Stanley, \u003ca href=\"http://dx.doi.org/10.1007/BF02985635\"\u003eThe mathematical knight\u003c/a\u003e, Math. Intelligencer, 25 (No. 1) (2003), 22-34."
			],
			"example": [
				"The board as an 8x8 matrix a(n,m)",
				"n\\m  1   2   3   4   5   6   7   8",
				"1:  37  62  43  56  35  60  41  50",
				"2:  44  55  36  61  42  49  34  59",
				"3:  63  38  53  46  57  40  51  48",
				"4:  54  45  64  39  52  47  58  33",
				"5:   1  26  15  20   7  32  13  22",
				"6:  16  19   8  25  14  21   6  31",
				"7:  27   2  17  10  29   4  23  12",
				"8:  18   9  28   3  24  11  30   5",
				"(5,1) marks the start position of the knight because a(n,m) = 1 (move 0). Move 1 leads to square (7,2) because a(7,2) = 2, etc. Move 63 leads to a(4,3) = 64, the end of the re-entrant Hamiltonian path on the board.",
				"Central symmetry: a(4,8) = 32 + 1 = 33, a(4,7) = 32 + 26  = 58, etc. such that the upper half can be found from the lower half."
			],
			"xref": [
				"Cf. A226698 (inverse)."
			],
			"keyword": "nonn,fini,full,tabf",
			"offset": "1,1",
			"author": "_Wolfdieter Lang_, Jun 25 2013",
			"references": 1,
			"revision": 19,
			"time": "2019-01-01T06:37:33-05:00",
			"created": "2013-06-26T12:25:26-04:00"
		}
	]
}