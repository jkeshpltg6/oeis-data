{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A090452",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 90452,
			"data": "1,1,3,2,1,7,16,15,5,1,12,51,105,114,63,14,1,18,118,396,771,910,644,252,42,1,25,230,1110,3235,6083,7580,6240,3270,990,132,1,33,402,2600,10365,27483,50464,65331,59625,37620,15642,3861,429,1,42,651,5390,27825,97188",
			"name": "Scaled array A078740 ((3,2)-Stirling2).",
			"comment": [
				"This scaled Stirling2 array will be called s2_{3,2}(n,m).",
				"The sequence of row lengths is [1,3,5,7,...]=A005408(n-1).",
				"The generating function for the sequence from column nr. m is G(m,x)=(x^ceiling(m/2))*P(m,x)/(1-x)^(2*m-3) with the row polynomials of array A091029(m,k).",
				"The generating functions of the column sequences obey the hypergeometric differential-difference eq.:x*(1-x)*G''(m,x) + 2*(1-m*x)*G'(m,x) - m*(m-1)*G(m,x) = 2*m*x*G'(m-1,x) + 2*m*(m-1)*G(m-1,x) + m*(m-1)*G(m-2,x), m\u003e=3; with G(2,x)=x/(1-x) and G(1,x)=0. The primes denote differentiation w.r.t. x."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A090452/b090452.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (rows 1 \u003c= n \u003c= 100, flattened.)",
				"Paul Barry, \u003ca href=\"https://www.emis.de/journals/JIS/VOL22/Barry3/barry422.html\"\u003eGeneralized Catalan Numbers Associated with a Family of Pascal-like Triangles\u003c/a\u003e, J. Int. Seq., Vol. 22 (2019), Article 19.5.8.",
				"W. Lang, \u003ca href=\"/A090452/a090452.txt\"\u003eFirst 8 rows\u003c/a\u003e."
			],
			"formula": [
				"a(n, m)=(m!/((n+1)!*n!))*A078740(n, m), n\u003e=1, 2\u003c= m \u003c=2*n.",
				"Recursion: a(n, m)=((n+m-1)*(n+m-2)*a(n-1, m)+2*(n+m-2)*m*a(n-1, m-1)+m*(m-1)*a(n-1, m-2))/((n+1)*n), n\u003e=2, 2\u003c=m\u003c=2*n, a(1, 2)=1, a(n, 0) := 0, a(n, 1) := 0 (from the recursion of array A078740)."
			],
			"example": [
				"[1]; [1,3,2]; [1,7,16,15,5]; [1,12,51,105,114,63,14]; ..."
			],
			"mathematica": [
				"Table[(-1)^m*m!*HypergeometricPFQ[{2 - m, n + 1, n + 2}, {2, 3}, 1]/(2 (m - 2)!), {n, 8}, {m, 2, 2 n}] // Flatten (* _Michael De Vlieger_, Nov 21 2019, after _Jean-François Alcover_ at A078740. *)"
			],
			"xref": [
				"a(n, 2*n)=A000108(n) (Catalan), n\u003e=1, a(n, 2*n-1)=3*A002054(n-1), n\u003e=2, a(n, 2*n-2)=A091031(n), n\u003e=2.",
				"The column sequences (without leading zeros) are: A000012 (powers of 1), A055998, A090453-4, A091026-7, etc.",
				"Cf. A090442 (row sums). The alternating row sums are 0 except for row n=1 which gives 1."
			],
			"keyword": "nonn,easy,tabf",
			"offset": "1,3",
			"author": "_Wolfdieter Lang_, Dec 23 2003",
			"references": 9,
			"revision": 16,
			"time": "2019-11-21T19:13:14-05:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}