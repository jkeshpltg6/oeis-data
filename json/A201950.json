{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A201950",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 201950,
			"data": "1,0,2,6,28,160,1078,8358,73260,716112,7721844,91039740,1164932470,16077368580,238037983558,3763371442530,63276351409092,1127406030014112,21218146474666864,420611921077524912,8759617763834095796,191208185756772875880",
			"name": "Central coefficients in Product_{k=0..n-1} (1 + k*x + x^2).",
			"link": [
				"Vaclav Kotesovec, \u003ca href=\"/A201950/b201950.txt\"\u003eTable of n, a(n) for n = 0..300\u003c/a\u003e"
			],
			"formula": [
				"Central terms of rows in irregular triangle A201949.",
				"a(n) = (n-1)*a(n-1) + 2*A201952(n-2) for n\u003e0.",
				"E.g.f.: BesselI(0, 2*log(1 - x)). - _Ilya Gutkovskiy_, Feb 22 2019",
				"E.g.f.: Sum_{n\u003e=0} log(1 - x)^(2*n) / n!^2. [After _Ilya Gutkovskiy_ - _Paul D. Hanna_, Feb 24 2019]"
			],
			"example": [
				"The coefficients in Product_{k=0..n-1} (1+k*x+x^2) form triangle A201949:",
				"(1);",
				"1,(0), 1;",
				"1, 1,(2), 1, 1;",
				"1, 3, 5, (6), 5, 3, 1;",
				"1, 6, 15, 24, (28), 24, 15, 6, 1;",
				"1, 10, 40, 90, 139, (160), 139, 90, 40, 10, 1;",
				"1, 15, 91, 300, 629, 945, (1078), 945, 629, 300, 91, 15, 1;",
				"1, 21, 182, 861, 2520, 5019, 7377, (8358), 7377, 5019, 2520, 861, 182, 21, 1;",
				"1, 28, 330, 2156, 8729, 23520, 45030, 65016, (73260), 65016, 45030, 23520, 8729, 2156, 330, 28, 1; ...",
				"where coefficients in parenthesis form the initial terms of this sequence."
			],
			"mathematica": [
				"Flatten[{1,Table[Coefficient[Expand[Product[1 + k*x + x^2,{k,0,n-1}]],x^n],{n,1,20}]}] (* _Vaclav Kotesovec_, Feb 10 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = polcoeff( prod(k=1,n,1+(k-1)*x+x^2+x*O(x^n)), n)}",
				"for(n=0,30, print1(a(n),\", \"))",
				"(PARI) /* From series BesselI(0, 2*log(1 - x)), after _Ilya Gutkovskiy_ */",
				"{a(n) = n!*polcoeff( sum(m=0,n, log(1 - x +x*O(x^n))^(2*m)/m!^2), n)}",
				"for(n=0,30, print1(a(n),\", \")) \\\\ _Paul D. Hanna_, Feb 24 2019"
			],
			"xref": [
				"Cf. A201949, A201951, A201952, A201953.",
				"Cf. A324304 (variant)."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Paul D. Hanna_, Dec 06 2011",
			"references": 8,
			"revision": 29,
			"time": "2019-02-28T23:41:03-05:00",
			"created": "2011-12-07T18:46:24-05:00"
		}
	]
}