{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A249417",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 249417,
			"data": "2,0,9,3,4,0,6,6,4,9,6,7,8,3,2,1,8,0,6,9,2,0,1,6,1,8,1,1,2,5,0,0,8,1,8,2,8,6,0,0,5,4,6,9,0,5,2,0,7,9,5,8,5,2,0,5,3,0,2,3,7,8,0,6,6,8,9,4,7,2,6,9,5,7,8,0,3,9,2,8,1,0,3,7,5,5,7,5,9,5,8,6,6,0,4,3,1,2,2,0,5,6,5",
			"name": "Decimal expansion of E(T_{1,0}), the expected \"first-passage\" time required for an Ornstein-Uhlenbeck process to cross the level 1, given that it started at level 0.",
			"comment": [
				"Following Steven Finch, it is assumed that the values of the parameters of the stochastic differential equation dX_t = -rho (X_t - mu) dt + sigma dW_t, satisfied by the process, are mu = 0, rho = 1 and sigma^2 = 2."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A249417/b249417.txt\"\u003eTable of n, a(n) for n = 1..5000\u003c/a\u003e",
				"Steven R. Finch, \u003ca href=\"/A249417/a249417.pdf\"\u003eOrnstein-Uhlenbeck Process\u003c/a\u003e, May 15, 2004. [Cached copy, with permission of the author]",
				"Michael Kopp, Elma Nassar, Etienne Pardoux, \u003ca href=\"https://doi.org/10.1007/s00285-018-1258-2\"\u003ePhenotypic lag and population extinction in the moving-optimum model: insights from a small-jumps limit\u003c/a\u003e, Journal of Mathematical Biology (2018), Vol. 77, Issue 5, 1431-1458.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Ornstein%E2%80%93Uhlenbeck_process\"\u003eOrnstein-Uhlenbeck process\u003c/a\u003e"
			],
			"formula": [
				"E(T_{a,0}) = sqrt(Pi/2)*integrate_{0..a} (1 + erf(t/sqrt(2)))*exp(t^2/2) dt.",
				"E(T_{a,0}) = (1/2)*sum_{k \u003e= 1} (sqrt(2)*a)^k/k!*Gamma(k/2).",
				"E(T_{a,0}) = (1/2)*(Pi*erfi(a/sqrt(2)) + a^2 * 2F2(1,1; 3/2,2; a^2/2)), where erfi is the imaginary error function, and 2F2 the hypergeometric function."
			],
			"example": [
				"2.09340664967832180692016181125008182860054690520795852..."
			],
			"mathematica": [
				"Ex[T[a_, 0]] := (1/2)*(Pi*Erfi[a/Sqrt[2]] + a^2*HypergeometricPFQ[{1, 1}, {3/2, 2}, a^2/2]); RealDigits[Ex[T[1, 0]], 10, 103] // First"
			],
			"xref": [
				"Cf. A249418."
			],
			"keyword": "nonn,cons",
			"offset": "1,1",
			"author": "_Jean-François Alcover_, Oct 28 2014",
			"references": 10,
			"revision": 19,
			"time": "2018-10-15T22:16:07-04:00",
			"created": "2014-10-28T11:24:41-04:00"
		}
	]
}