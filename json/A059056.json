{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A059056",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 59056,
			"data": "1,0,0,1,1,0,4,0,1,10,24,27,16,12,0,1,297,672,736,480,246,64,24,0,1,13756,30480,32365,21760,10300,3568,970,160,40,0,1,925705,2016480,2116836,1418720,677655,243360,67920,14688,2655,320,60,0,1",
			"name": "Penrice Christmas gift numbers, Card-matching numbers (Dinner-Diner matching numbers): Triangle T(n,k) = number of ways to get k matches for a deck with n cards, 2 of each kind.",
			"comment": [
				"This is a triangle of card matching numbers. A deck has n kinds of cards, 2 of each kind. The deck is shuffled and dealt in to n hands with 2 cards each. A match occurs for every card in the j-th hand of kind j. Triangle T(n,k) is the number of ways of achieving exactly k matches (k=0..2n). The probability of exactly k matches is T(n,k)/((2n)!/2^n).",
				"Rows are of length 1,3,5,7,... = A005408(n). [Edited by _M. F. Hasler_, Sep 21 2015]",
				"Analogous to A008290. - _Zerinvary Lajos_, Jun 10 2005"
			],
			"reference": [
				"F. N. David and D. E. Barton, Combinatorial Chance, Hafner, NY, 1962, Ch. 7 and Ch. 12.",
				"J. Riordan, An Introduction to Combinatorial Analysis, Wiley, 1958, pp. 174-178.",
				"R. P. Stanley, Enumerative Combinatorics Volume I, Cambridge University Press, 1997, p. 71."
			],
			"link": [
				"F. F. Knudsen and I. Skau, \u003ca href=\"http://www.jstor.org/stable/2691467\"\u003eOn the Asymptotic Solution of a Card-Matching Problem\u003c/a\u003e, Mathematics Magazine 69 (1996), 190-197.",
				"Barbara H. Margolius, \u003ca href=\"http://academic.csuohio.edu/bmargolius/homepage/dinner/dinner/cardentry.htm\"\u003eDinner-Diner Matching Probabilities\u003c/a\u003e",
				"B. H. Margolius, \u003ca href=\"http://www.jstor.org/stable/3219303\"\u003eThe Dinner-Diner Matching Problem\u003c/a\u003e, Mathematics Magazine, 76 (2003), 107-118.",
				"S. G. Penrice, \u003ca href=\"http://www.jstor.org/stable/2324927\"\u003eDerangements, permanents and Christmas presents\u003c/a\u003e, The American Mathematical Monthly 98(1991), 617-620.",
				"\u003ca href=\"/index/Ca#cardmatch\"\u003eIndex entries for sequences related to card matching\u003c/a\u003e"
			],
			"formula": [
				"G.f.: sum(coeff(R(x, n, k), x, j)*(t-1)^j*(n*k-j)!, j=0..n*k) where n is the number of kinds of cards, k is the number of cards of each kind (here k is 2) and R(x, n, k) is the rook polynomial given by R(x, n, k)=(k!^2*sum(x^j/((k-j)!^2*j!))^n (see Stanley or Riordan). coeff(R(x, n, k), x, j) indicates the j-th coefficient on x of the rook polynomial."
			],
			"example": [
				"There are 4 ways of matching exactly 2 cards when there are 2 different kinds of cards, 2 of each in each of the two decks so T(2,2)=4.",
				"Triangle begins:",
				"1",
				"\"0\", 0, 1",
				"1, '0', \"4\", 0, 1",
				"10, 24, 27, '16', \"12\", 0, 1",
				"297, 672, 736, 480, 246, '64', \"24\", 0, 1",
				"13756, 30480, 32365, 21760, 10300, 3568, 970, '160', \"40\", 0, 1",
				"925705, 2016480, 2116836, 1418720, 677655, 243360, 67920, 14688, 2655, '320', \"60\", 0, 1",
				"Diagonal \" \": T(n,2n-2) = 0, 4, 12, 24, 40, 60, 84, 112, 144, ... equals A046092",
				"Diagonal ' ': T(n,2n-3) = 0, 16, 64, 160, 320, 560, 896, 1344, ... equals A102860"
			],
			"maple": [
				"p := (x,k)-\u003ek!^2*sum(x^j/((k-j)!^2*j!),j=0..k); R := (x,n,k)-\u003ep(x,k)^n; f := (t,n,k)-\u003esum(coeff(R(x,n,k),x,j)*(t-1)^j*(n*k-j)!,j=0..n*k);",
				"for n from 0 to 7 do seq(coeff(f(t,n,2),t,m)/2^n,m=0..2*n); od;"
			],
			"mathematica": [
				"p[x_, k_] := k!^2*Sum[ x^j/((k-j)!^2*j!), {j, 0, k}];",
				"R[x_, n_, k_] := p[x, k]^n;",
				"f[t_, n_, k_] := Sum[ Coefficient[ R[x, n, k], x, j]*(t-1)^j*(n*k-j)!, {j, 0, n*k}];",
				"Table[ Coefficient[ f[t, n, 2]/2^n, t, m], {n, 0, 6}, {m, 0, 2*n}] // Flatten",
				"(* _Jean-François Alcover_, Sep 17 2012, translated from Maple *)"
			],
			"xref": [
				"Cf. A059056-A059071, A008290."
			],
			"keyword": "nonn,tabf,nice",
			"offset": "0,7",
			"author": "Barbara Haas Margolius (margolius(AT)math.csuohio.edu)",
			"ext": [
				"Additional comments from _Zerinvary Lajos_, Jun 18 2007",
				"Edited by _M. F. Hasler_, Sep 21 2015"
			],
			"references": 22,
			"revision": 24,
			"time": "2017-09-26T05:08:10-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}