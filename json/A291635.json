{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A291635",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 291635,
			"data": "0,1,1,0,1,3,1,0,1,2,2,0,3,7,3,0,4,4,1,0,4,7,3,0,3,5,2,0,4,6,2,0,2,3,3,0,4,8,3,0,5,8,2,0,2,5,2,0,5,8,4,0,4,5,2,0,5,6,4,0,1,8,5,0,3,9,3,0,6,8,3,0,5,13,5,0,9,9,2,0,4,6,6,0,7,11,4,0,8,10,5,0,2,11,5,0,3,10,4,0",
			"name": "Number of ways to write n as x^2 + y^2 + z^2 + w^2 with x,y,z,w nonnegative integers such that p = x + 2*y + 5*z, p - 2, p + 4 and p + 10 are all prime.",
			"comment": [
				"Conjecture: a(n) \u003e 0 for all n \u003e 1 not divisible by 4.",
				"This is stronger than the conjecture in A291624. Obviously, it implies that there are infinitely many prime quadruples (p-2, p, p+4, p+10).",
				"We have verified that a(n) \u003e 0 for any integer 1 \u003c n \u003c 10^7 not divisible by 4."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A291635/b291635.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2016.11.008\"\u003eRefining Lagrange's four-square theorem\u003c/a\u003e, J. Number Theory 175(2017), 167-190.",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1701.05868\"\u003eRestricted sums of four squares\u003c/a\u003e, arXiv:1701.05868 [math.NT], 2017."
			],
			"example": [
				"a(61) = 1 since 61 = 4^2 + 0^2 + 3^2 + 6^2 with 4 + 2*0 + 5*3 = 19, 19 - 2 = 17, 19 + 4 = 23 and 19 + 10 = 29 all prime.",
				"a(253) = 1 since 253 = 12^2 + 8^2 + 3^2 + 6^2 with 12 + 2*8 + 5*3 = 43, 43 - 2 = 41, 43 + 4 = 47 and 43 + 10 = 53 all prime.",
				"a(725) = 1 since 725 = 7^2 + 0^2 + 0^2 + 26^2 with 7 + 2*0 + 5*0 = 7, 7 - 2 = 5, 7 + 4 = 11 and 7 + 10 = 17 all prime.",
				"a(1511) = 1 since 1511 = 18^2 + 15^2 + 11^2 + 29^2 with 18 + 2*15 + 5*11 = 103, 103 - 2 = 101, 103 + 4 = 107 and 103 + 10 = 113 all prime."
			],
			"mathematica": [
				"SQ[n_]:=SQ[n]=IntegerQ[Sqrt[n]];",
				"PQ[p_]:=PQ[p]=PrimeQ[p]\u0026\u0026PrimeQ[p-2]\u0026\u0026PrimeQ[p+4]\u0026\u0026PrimeQ[p+10]",
				"Do[r=0;Do[If[SQ[n-x^2-y^2-z^2]\u0026\u0026PQ[x+2y+5z],r=r+1],{x,0,Sqrt[n]},{y,0,Sqrt[n-x^2]},{z,0,Sqrt[n-x^2-y^2]}];Print[n,\" \",r],{n,1,100}]"
			],
			"xref": [
				"Cf. A000040, A000118, A000290, A022004, A172454, A271518, A281976, A290935, A291150, A291191, A291455, A291624."
			],
			"keyword": "nonn,look",
			"offset": "1,6",
			"author": "_Zhi-Wei Sun_, Aug 28 2017",
			"references": 2,
			"revision": 19,
			"time": "2017-08-29T14:19:41-04:00",
			"created": "2017-08-28T10:47:09-04:00"
		}
	]
}