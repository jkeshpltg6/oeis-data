{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A309076",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 309076,
			"data": "0,1,-1,2,3,-3,-2,-4,5,6,4,7,8,-8,-7,-9,-6,-5,-11,-10,-12,13,14,12,15,16,10,11,9,18,19,17,20,21,-21,-20,-22,-19,-18,-24,-23,-25,-16,-15,-17,-14,-13,-29,-28,-30,-27,-26,-32,-31,-33",
			"name": "The Zeckendorf representation of n read as a NegaFibonacci representation.",
			"comment": [
				"Every nonnegative integer has a unique Zeckendorf representation (A014417) as a sum of nonconsecutive Fibonacci numbers F_{k}, with k\u003e1. Likewise, every integer has a unique NegaFibonacci representation as a sum of nonconsecutive F_{-k}, with k\u003e0 (A215022 for the positive integers, A215023 for the negative). So the F_{k} summing to n are transformed to F_{-k+1} and summed. Since the representations are unique and mapped one-to-one, every integer appears exactly once in the sequence.",
				"a(n) changes sign at each Fibonacci number, since NegaFibonacci representations with an odd number of fibits are positive and those with an even number are negative."
			],
			"reference": [
				"D. E. Knuth, The Art of Computer Programming, Vol. 4A, Section 7.1.3, p. 169.",
				"E. Zeckendorf, Représentation des nombres naturels par une somme des nombres de Fibonacci ou de nombres de Lucas, Bull. Soc. Roy. Sci. Liège 41, 179-182, 1972."
			],
			"link": [
				"Garth A. T. Rose, \u003ca href=\"/A309076/b309076.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Sean A. Irvine, \u003ca href=\"https://github.com/archmageirvine/joeis/blob/master/src/irvine/oeis/a309/A309076.java\"\u003eJava program\u003c/a\u003e (github)",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Zeckendorf%27s_theorem\"\u003eZeckendorf's theorem\u003c/a\u003e"
			],
			"example": [
				"10 is 8 + 2, or F_6 + F_3. a(10) is then F_{-5} + F_{-2} = 5 + (-1) = 4."
			],
			"program": [
				"(Sage)",
				"def a309076(n):",
				"    result = 0",
				"    lnphi = ln((1+sqrt(5))/2)",
				"    while n \u003e 0:",
				"        k = floor(ln(n*sqrt(5)+1/2)/lnphi)",
				"        n = n - fibonacci(k)",
				"        result = result + fibonacci(1 - k)",
				"    return result"
			],
			"xref": [
				"Cf. A000045, A014417, A215022, A215023."
			],
			"keyword": "base,sign",
			"offset": "0,4",
			"author": "_Garth A. T. Rose_, Jul 10 2019",
			"references": 1,
			"revision": 26,
			"time": "2019-08-22T04:51:16-04:00",
			"created": "2019-08-22T04:51:16-04:00"
		}
	]
}