{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A330665",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 330665,
			"data": "1,1,1,1,1,1,1,1,1,1,1,2,1,1,1,2,1,2,1,2,1,1,1,5,1,1,1,2,1,3,1,5,1,1,1,7,1,1,1,5,1,3,1,2,2,1,1,16,1,2,1,2,1,5,1,5,1,1,1,11,1,1,2,16,1,3,1,2,1,3,1,27,1,1,2,2,1,3,1,16,2,1,1,11,1",
			"name": "Number of balanced reduced multisystems of maximal depth whose atoms are the prime indices of n.",
			"comment": [
				"First differs from A317145 at a(32) = 5, A317145(32) = 4.",
				"A balanced reduced multisystem is either a finite multiset, or a multiset partition with at least two parts, not all of which are singletons, of a balanced reduced multisystem.",
				"A prime index of n is a number m such that prime(m) divides n. The multiset of prime indices of n is row n of A112798.",
				"Also series/singleton-reduced factorizations of n with Omega(n) levels of parentheses. See A001055, A050336, A050338, A050340, etc."
			],
			"formula": [
				"a(2^n) = A000111(n - 1).",
				"a(product of n distinct primes) = A006472(n)."
			],
			"example": [
				"The a(n) multisystems for n = 2, 6, 12, 24, 48:",
				"  {1}  {1,2}  {{1},{1,2}}  {{{1}},{{1},{1,2}}}  {{{{1}}},{{{1}},{{1},{1,2}}}}",
				"              {{2},{1,1}}  {{{1,1}},{{1},{2}}}  {{{{1}}},{{{1,1}},{{1},{2}}}}",
				"                           {{{1}},{{2},{1,1}}}  {{{{1},{1}}},{{{1}},{{1,2}}}}",
				"                           {{{1,2}},{{1},{1}}}  {{{{1},{1,1}}},{{{1}},{{2}}}}",
				"                           {{{2}},{{1},{1,1}}}  {{{{1,1}}},{{{1}},{{1},{2}}}}",
				"                                                {{{{1}}},{{{1}},{{2},{1,1}}}}",
				"                                                {{{{1}}},{{{1,2}},{{1},{1}}}}",
				"                                                {{{{1},{1}}},{{{2}},{{1,1}}}}",
				"                                                {{{{1},{1,2}}},{{{1}},{{1}}}}",
				"                                                {{{{1,1}}},{{{2}},{{1},{1}}}}",
				"                                                {{{{1}}},{{{2}},{{1},{1,1}}}}",
				"                                                {{{{1},{2}}},{{{1}},{{1,1}}}}",
				"                                                {{{{1,2}}},{{{1}},{{1},{1}}}}",
				"                                                {{{{2}}},{{{1}},{{1},{1,1}}}}",
				"                                                {{{{2}}},{{{1,1}},{{1},{1}}}}",
				"                                                {{{{2},{1,1}}},{{{1}},{{1}}}}"
			],
			"mathematica": [
				"primeMS[n_]:=If[n==1,{},Flatten[Cases[FactorInteger[n],{p_,k_}:\u003eTable[PrimePi[p],{k}]]]];",
				"sps[{}]:={{}};sps[set:{i_,___}]:=Join@@Function[s,Prepend[#,s]\u0026/@sps[Complement[set,s]]]/@Cases[Subsets[set],{i,___}];",
				"mps[set_]:=Union[Sort[Sort/@(#/.x_Integer:\u003eset[[x]])]\u0026/@sps[Range[Length[set]]]];",
				"totm[m_]:=Prepend[Join@@Table[totm[p],{p,Select[mps[m],1\u003cLength[#]\u003cLength[m]\u0026]}],m];",
				"Table[Length[Select[totm[primeMS[n]],Length[#]\u003c=1||Depth[#]==PrimeOmega[n]\u0026]],{n,100}]"
			],
			"xref": [
				"The last nonzero term in row n of A330667 is a(n).",
				"The chain version is A317145.",
				"The non-maximal version is A318812.",
				"Unlabeled versions are A330664 and A330663.",
				"Other labeled versions are A330675 (strongly normal) and A330676 (normal).",
				"Cf. A001055, A005121, A005804, A050336, A213427, A292505, A317144, A318849, A320160, A330474, A330475, A330679."
			],
			"keyword": "nonn",
			"offset": "1,12",
			"author": "_Gus Wiseman_, Dec 27 2019",
			"references": 9,
			"revision": 4,
			"time": "2019-12-29T08:44:00-05:00",
			"created": "2019-12-29T08:44:00-05:00"
		}
	]
}