{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A303638",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 303638,
			"data": "1,2,0,6,0,3,24,0,12,8,120,0,540,40,0,720,0,6120,240,0,144,5040,0,83160,1680,0,1008,840,40320,0,1310400,13440,0,8064,6720,5760,362880,0,321012720,120960,0,72576,60480,51840,0,3628800,0,9394509600,207648000,0,725760,604800,518400,0,0",
			"name": "Coefficients of a representation of gamma_{n-1}(1) - gamma_{n-1}(n) where gamma_n(x) are the generalized Euler-Stieltjes constants, triangle read by rows, for n \u003e= 1 and 0 \u003c= k \u003c= n-1.",
			"link": [
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Stieltjes_constants#Generalized_Stieltjes_constants\"\u003eGeneralized Stieltjes constants\u003c/a\u003e"
			],
			"formula": [
				"gamma_{n-1}(1) - gamma_{n-1}(n) = (1/n!) Sum_{k=1..n-1} T(n,k)*(log(k))^(n-1) where T(n, k) = 0 if k is a prime power (in the sense of A025475).",
				"-Gamma(n)*B^(n)(0,n) = n!*gamma_{n-1} - Sum_{k=1..n-1} T(n,k)(log(k))^(n-1) where Gamma(n) is Euler's Gamma function and B^(n)(0,n) is the n-th derivative of the generalized Bernoulli function B(s, a) with respect to s.",
				"Four cases can be distinguished:",
				"(1) If k=0 then T(n, k) = n!,",
				"(2) else if k is prime then T(n, k) = Sum_{v=1..m} v^(n-1)*k^(-v) where m = ilog_k(n-1) and ilog is the integer base k logarithm,",
				"(3) else if k is a prime power in the sense of A025475 then T(n, k) = 0,",
				"(4) else (k is composite but not a prime power) T(n, k) = n!/k."
			],
			"example": [
				"The triangle starts:",
				"[n\\k][      0  1           2          3  4       5       6       7  8  9]",
				"[ 1] [      1]",
				"[ 2] [      2, 0]",
				"[ 3] [      6, 0,          3]",
				"[ 4] [     24, 0,         12,         8]",
				"[ 5] [    120, 0,        540,        40, 0]",
				"[ 6] [    720, 0,       6120,       240, 0,    144]",
				"[ 7] [   5040, 0,      83160,      1680, 0,   1008,    840]",
				"[ 8] [  40320, 0,    1310400,     13440, 0,   8064,   6720,   5760]",
				"[ 9] [ 362880, 0,  321012720,    120960, 0,  72576,  60480,  51840, 0]",
				"[10] [3628800, 0, 9394509600, 207648000, 0, 725760, 604800, 518400, 0, 0]"
			],
			"maple": [
				"Trow := proc(n) local h, r, e, f;",
				"h := (n, k) -\u003e `if`(k = 1, x[0], h(n, k-1) - log(k-1)^n/(k-1));",
				"r := `if`(n = 0, 1, n!*h(n-1,n)); f := k -\u003e (-x[k])^(1/(n-1));",
				"e := eval(subs(ln = f, r)); seq(coeff(e, x[i]), i=0..n-1) end:",
				"seq(Trow(n), n=1..10);",
				"# Alternative:",
				"T := proc(n, k) local ispp, omega:",
				"  omega := n -\u003e nops(numtheory:-factorset(n)):",
				"  ispp  := n -\u003e not isprime(n) and omega(n) = 1:",
				"  if k = 0 then return n! fi;",
				"  if isprime(k) then",
				"     add(v^(n-1)*k^(-v), v=1..ilog[k](n-1)):",
				"     return n!*% fi:",
				"  if k = 1 or ispp(k) then return 0 fi:",
				"  return n!/k end:",
				"seq(seq(T(n,k), k=(0..n-1)), n=1..10);"
			],
			"mathematica": [
				"T[n_, k_] := Module[{s}, If[k == 0, Return[n!]]; If[PrimeQ[k], s = Sum[v^(n-1) k^(-v), {v, 1, Log[k, n-1]}]; Return[n! s]]; If[k == 1 || PrimePowerQ[k], Return[0]]; n!/k];",
				"Table[T[n, k], {n, 1, 10}, {k, 0, n-1}] // Flatten (* _Jean-François Alcover_, Jul 22 2019, from 2nd Maple program *)"
			],
			"xref": [
				"See the cross-references in A301816 for the values of some Stieltjes constants.",
				"Row sums are A303938."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Peter Luschny_, Apr 27 2018",
			"references": 1,
			"revision": 20,
			"time": "2019-07-22T05:45:13-04:00",
			"created": "2018-06-03T11:11:07-04:00"
		}
	]
}