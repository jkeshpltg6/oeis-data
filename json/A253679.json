{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A253679",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 253679,
			"data": "23,118,333,716,1315,2178,3353,4888,6831,9230,12133,15588,19643,24346,29745,35888,42823,50598,59261,68860,79443,91058,103753,117576,132575,148798,166293,185108,205291,226890,249953,274528,300663,328406,357805,388908,421763,456418,492921,531320,571663,613998,658373,704836,753435,804218,857233,912528,970151,1030150,1092573,1157468",
			"name": "Numbers a(n) that are the starting terms in the sum of an odd number of consecutive cubes equal to a square.",
			"comment": [
				"Numbers a(n) such that a^3 + (a+1)^3 + ... + (a+M-1)^3 = c^2 has nontrivial solutions over the integers where M is an odd positive integer.",
				"To every odd positive integer M corresponds a sum of M consecutive cubes starting at a having at least one nontrivial solution. For n\u003e=1, M(n)=(2n+1) (A005408), a(n) = M^3 - (3M-1)/2 = (2n+1)^3 - (3n+1) and c(n)= M*(M^2-1)*(2M^2-1)/2 = 2n*(n+1)*(2n+1)*(8n*(n+1)+1) (A253680).",
				"The trivial solutions with M \u003c 1 and a \u003c 2 are not considered here.",
				"Stroeker stated that all odd values of M yield a solution to  a^3 + (a+1)^3 + ... + (a+M-1)^3 = c^2. This was further demonstrated by Pletser."
			],
			"link": [
				"Vladimir Pletser, \u003ca href=\"/A253679/b253679.txt\"\u003eTable of n, a(n) for n = 1..50000\u003c/a\u003e",
				"Vladimir Pletser, \u003ca href=\"/A253679/a253679.txt\"\u003eFile Triplets (M,a,c) for M=(2n+1)\u003c/a\u003e",
				"Vladimir Pletser, \u003ca href=\"http://www.researchgate.net/profile/Vladimir_Pletser/publication/271272786\"\u003eNumber of terms, first term and square root of sums of consecutive cubed integers equal to integer squares\u003c/a\u003e, Research Gate, 2015.",
				"V. Pletser, \u003ca href=\"http://arxiv.org/abs/1501.06098\"\u003eGeneral solutions of sums of consecutive cubed integers equal to squared integers\u003c/a\u003e, arXiv:1501.06098 [math.NT], 2015.",
				"R. J. Stroeker, \u003ca href=\"http://www.numdam.org/item?id=CM_1995__97_1-2_295_0\"\u003eOn the sum of consecutive cubes being a perfect square\u003c/a\u003e, Compositio Mathematica, 97 no. 1-2 (1995), pp. 295-307.",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-6,4,-1)."
			],
			"formula": [
				"a(n) = (2n+1)^3 - (3n+1).",
				"a(n) = 4*a(n-1) - 6*a(n-2) + 4*a(n-3) - a(n-4). - _Colin Barker_, Jan 09 2015",
				"G.f.: -x*(x^2-26*x-23) / (x-1)^4. - _Colin Barker_, Jan 09 2015"
			],
			"example": [
				"For n=1, M(n)=3, a(n)=23, c(n)=204.",
				"See \"File Triplets (M,a,c) for M=(2n+1)\" link."
			],
			"maple": [
				"for n from 1 to 50 do a:=(2*n+1)^3-(3*n+1): print (a); end do:"
			],
			"mathematica": [
				"a253679[n_] := (2 # + 1)^3 - (3 # + 1) \u0026 /@ Range@ n; a253679[52] (* _Michael De Vlieger_, Jan 10 2015 *)"
			],
			"program": [
				"(PARI) Vec(-x*(x^2-26*x-23)/(x-1)^4 + O(x^100)) \\\\ _Colin Barker_, Jan 09 2015"
			],
			"xref": [
				"Cf. A116108, A116145, A126200, A126203, A163392, A163393, A253680, A253681."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_Vladimir Pletser_, Jan 08 2015",
			"references": 8,
			"revision": 33,
			"time": "2017-02-04T01:05:12-05:00",
			"created": "2015-01-15T12:24:01-05:00"
		}
	]
}