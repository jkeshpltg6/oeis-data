{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A207974",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 207974,
			"data": "1,1,1,1,2,1,1,3,1,1,1,4,2,2,1,1,5,2,4,1,1,1,6,3,6,3,2,1,1,7,3,9,3,5,1,1,1,8,4,12,6,8,4,2,1,1,9,4,16,6,14,4,6,1,1,1,10,5,20,10,20,10,10,5,2,1",
			"name": "Triangle related to A152198.",
			"comment": [
				"Row sums are A027383(n).",
				"Diagonal sums are alternately A014739(n) and A001911(n+1).",
				"The matrix inverse starts",
				"1;",
				"-1,1;",
				"1,-2,1;",
				"1,-1,-1,1;",
				"-1,2,0,-2,1;",
				"-1,1,2,-2,-1,1;",
				"1,-2,-1,4,-1,-2,1;",
				"1,-1,-3,3,3,-3,-1,1;",
				"-1,2,2,-6,0,6,-2,-2,1;",
				"-1,1,4,-4,-6,6,4,-4,-1,1;",
				"1,-2,-3,8,2,-12,2,8,-3,-2,1;",
				"apparently related to A158854. - _R. J. Mathar_, Apr 08 2013",
				"From _Gheorghe Coserea_, Jun 11 2016: (Start)",
				"T(n,k) is the number of terms of the sequence A057890 in the interval [2^n,2^(n+1)-1] having binary weight k+1.",
				"T(n,k) = A007318(n,k) (mod 2) and the number of odd terms in row n of the triangle is 2^A000120(n).",
				"(End)"
			],
			"link": [
				"Gheorghe Coserea, \u003ca href=\"/A207974/b207974.txt\"\u003eRows n = 0..200, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = T(n-1,k-1) - (-1)^k*T(n-1,k), k\u003e0 ; T(n,0) = 1.",
				"T(2n,2k) = T(2n+1,2k) = binomial(n,k) = A007318(n,k).",
				"T(2n+1,2k+1) = A110813(n,k).",
				"T(2n+2,2k+1) = 2*A135278(n,k).",
				"T(n,2k) + T(n,2k+1) = A152201(n,k).",
				"T(n,2k) = A152198(n,k).",
				"T(n+1,2k+1) = A152201(n,k).",
				"T(n,k) = T(n-2,k-2) + T(n-2,k).",
				"T(2n,n) = A128014(n+1).",
				"T(n,k) = card {p, 2^n \u003c= A057890(p) \u003c= 2^(n+1)-1 and A000120(A057890(p)) = k+1}. - _Gheorghe Coserea_, Jun 09 2016",
				"P_n(x) = Sum_{k=0..n} T(n,k)*x^k = ((2+x+(n mod 2)*x^2)*(1+x^2)^(n\\2) - 2)/x. - _Gheorghe Coserea_, Mar 14 2017"
			],
			"example": [
				"Triangle begins :",
				"n\\k  [0] [1] [2] [3] [4] [5] [6] [7] [8] [9]",
				"[0]  1;",
				"[1]  1,  1;",
				"[2]  1,  2,  1;",
				"[3]  1,  3,  1,  1;",
				"[4]  1,  4,  2,  2,  1;",
				"[5]  1,  5,  2,  4,  1,  1;",
				"[6]  1,  6,  3,  6,  3,  2,  1;",
				"[7]  1,  7,  3,  9,  3,  5,  1,  1;",
				"[8]  1,  8,  4,  12, 6,  8,  4,  2,  1;",
				"[9]  1,  9,  4,  16, 6,  14, 4,  6,  1,  1;",
				"[10] ..."
			],
			"maple": [
				"A207974 := proc(n,k)",
				"    if k = 0 then",
				"        1;",
				"    elif k \u003c 0 or k \u003e n then",
				"        0 ;",
				"    else",
				"        procname(n-1,k-1)-(-1)^k*procname(n-1,k) ;",
				"    end if;",
				"end proc: # _R. J. Mathar_, Apr 08 2013"
			],
			"program": [
				"(PARI)",
				"seq(N) = {",
				"  my(t = vector(N+1, n, vector(n, k, k==1 || k == n)));",
				"  for(n = 2, N+1, for (k = 2, n-1,",
				"      t[n][k] = t[n-1][k-1] + (-1)^(k%2)*t[n-1][k]));",
				"  return(t);",
				"};",
				"concat(seq(10))  \\\\ _Gheorghe Coserea_, Jun 09 2016",
				"(PARI)",
				"P(n) = ((2+x+(n%2)*x^2) * (1+x^2)^(n\\2) - 2)/x;",
				"concat(vector(11, n, Vecrev(P(n-1)))) \\\\ _Gheorghe Coserea_, Mar 14 2017"
			],
			"xref": [
				"Cf. Columns : A000012, A000027, A004526, A002620, A008805, A006918, A058187",
				"Cf. Diagonals : A000012, A000034, A052938, A097362",
				"Cf. A007318, A110813, A135278, A152201",
				"Related to thickness: A000120, A027383, A057890, A274036."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "0,5",
			"author": "_Philippe Deléham_, Feb 22 2012",
			"references": 2,
			"revision": 37,
			"time": "2017-03-14T20:47:33-04:00",
			"created": "2012-02-23T12:09:28-05:00"
		}
	]
}