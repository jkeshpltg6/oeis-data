{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007053",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7053,
			"id": "M1018",
			"data": "0,1,2,4,6,11,18,31,54,97,172,309,564,1028,1900,3512,6542,12251,23000,43390,82025,155611,295947,564163,1077871,2063689,3957809,7603553,14630843,28192750,54400028,105097565,203280221,393615806,762939111,1480206279,2874398515,5586502348,10866266172,21151907950,41203088796,80316571436,156661034233,305761713237,597116381732,1166746786182,2280998753949,4461632979717,8731188863470,17094432576778,33483379603407,65612899915304,128625503610475",
			"name": "Number of primes \u003c= 2^n.",
			"comment": [
				"Conjecture: The number 4 is the only perfect power in this sequence. In other words, it is impossible to have a(n) = x^m for some integers n \u003e 3, m \u003e 1 and x \u003e 1. - _Zhi-Wei Sun_, Sep 30 2015"
			],
			"reference": [
				"Jens Franke et al., pi(10^24), Posting to the Number Theory Mailing List, Jul 29 2010.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"David Baugh, \u003ca href=\"/A007053/b007053.txt\"\u003eTable of n, a(n) for n = 0..92\u003c/a\u003e (terms n = 87..92 found using Kim Walisch's primecount program, terms n = 0..86 from Charles R Greathouse IV and Douglas B. Staple, [a(0)-a(75) from Tomás Oliveira e Silva, a(76)-a(77) from Jens Franke et al., Jul 29 2010, a(78)-a(80) from Jens Franke et al. on the RH, verified unconditionally by Douglas B. Staple, and a(81)-a(86) from Douglas B. Staple])",
				"Andrew R. Booker, \u003ca href=\"http://primes.utm.edu/nthprime/\"\u003eThe Nth Prime Page\u003c/a\u003e",
				"S. W. Golomb, \u003ca href=\"/A007053/a007053.pdf\"\u003eLetter to N. J. A. Sloane, Jul. 1991\u003c/a\u003e",
				"Thomas R. Nicely, \u003ca href=\"https://faculty.lynchburg.edu/~nicely/index.html\"\u003eSome Results of Computational Research in Prime Numbers\u003c/a\u003e",
				"Thomas R. Nicely, \u003ca href=\"/A007053/a007053_1.pdf\"\u003eSome Results of Computational Research in Prime Numbers\u003c/a\u003e [Local copy, pdf only]",
				"Tomás Oliveira e Silva, \u003ca href=\"http://sweet.ua.pt/tos/primes.html\"\u003eTables of values of pi(x) and of pi2(x)\u003c/a\u003e",
				"Tomás Oliveira e Silva, \u003ca href=\"http://sweet.ua.pt/tos/bib/5.4.pdf\"\u003eComputing pi(x): the combinatorial method\u003c/a\u003e, Revista Do Detua, Vol. 4, No 6, March 2006.",
				"Douglas B. Staple, \u003ca href=\"http://arxiv.org/abs/1503.01839\"\u003eThe combinatorial algorithm for computing pi(x)\u003c/a\u003e, arXiv:1503.01839 [math.NT], 2015.",
				"\u003ca href=\"/index/Pri#primepop\"\u003eIndex entries for sequences related to numbers of primes in various ranges\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A060967(2n). - _R. J. Mathar_, Sep 15 2012"
			],
			"example": [
				"pi(2^3)=4 since first 4 primes are 2,3,5,7 all \u003c=2^3=8."
			],
			"mathematica": [
				"Table[PrimePi[2^n], {n, 0, 46}] (* _Robert G. Wilson v_ *)"
			],
			"program": [
				"(PARI) a(n) = primepi(1\u003c\u003cn); \\\\ _John W. Nicholson_, May 16 2011"
			],
			"xref": [
				"Cf. A006880, A036378."
			],
			"keyword": "nonn,nice",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_, _Mira Bernstein_, _Robert G. Wilson v_, S. W. Golomb",
			"ext": [
				"More terms from _Jud McCranie_",
				"Extended to n = 52 by _Warren D. Smith_, Dec 11 2000, computed with Meissel-Lehmer-Legendre inclusion exclusion formula code he wrote back in 1985, recently re-run.",
				"Extended to n = 86 by _Douglas B. Staple_, Dec 18 2014"
			],
			"references": 123,
			"revision": 105,
			"time": "2021-10-28T12:55:06-04:00",
			"created": "1994-04-28T03:00:00-04:00"
		}
	]
}