{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A303545",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 303545,
			"data": "0,0,1,0,2,2,3,0,1,3,6,4,7,5,2,0,6,2,9,6,9,11,14,8,8,10,5,6,12,4,10,0,4,9,5,4,15,16,13,12,24,18,28,18,16,22,28,16,17,16,20,20,25,10,12,12,17,22,24,8,21,13,3,0,5,8,26,18,16,10,25,8,28,21",
			"name": "For any n \u003e 0 and prime number p, let d_p(n) be the distance from n to the nearest p-smooth number; a(n) = Sum_{i prime} d_i(n).",
			"comment": [
				"For any n \u003e 0 and prime number p \u003e= A006530(n), d_p(n) = 0; hence the series in the name contains only finitely many nonzero terms and is well defined.",
				"See also A303548 for a similar sequence."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A303545/b303545.txt\"\u003eTable of n, a(n) for n = 1..32768\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A303545/a303545.png\"\u003eColored pin plot of the first 3 * 512 terms\u003c/a\u003e (where the color is function of the prime p in the term d_p(n))",
				"\u003ca href=\"/index/Di#distance_to_the_nearest\"\u003eIndex entries for sequences related to distance to nearest element of some set\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 0 iff n is a power of 2.",
				"a(2 * n) \u003c= 2 * a(n).",
				"a(n) \u003e= A053646(n) + A301574(n) (as d_2 = A053646 and d_3 = A301574)."
			],
			"example": [
				"For n = 42:",
				"- d_2(42) = |42 - 32| = 10,",
				"- d_3(42) = |42 - 36| = |42 - 48| = 6,",
				"- d_5(42) = |42 - 40| = 2,",
				"- d_p(42) = 0 for any prime number p \u003e= 7,",
				"- hence a(42) = 10 + 6 + 2 = 18."
			],
			"program": [
				"(PARI) gpf(n) = if (n==1, 1, my (f=factor(n)); f[#f~, 1])",
				"a(n) = my (v=0, pi=primepi(gpf(n))); for (d=0, oo, my (o=min(primepi(gpf(n-d)), primepi(gpf(n+d)))); if (o\u003cpi, v += d*(pi-o); pi=o); if (o\u003c=1, return (v)))"
			],
			"xref": [
				"Cf. A006530, A053646, A301574, A303548."
			],
			"keyword": "nonn,look",
			"offset": "1,5",
			"author": "_Rémy Sigrist_, Apr 26 2018",
			"references": 5,
			"revision": 26,
			"time": "2018-04-28T19:53:24-04:00",
			"created": "2018-04-27T17:07:00-04:00"
		}
	]
}