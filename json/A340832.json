{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A340832",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 340832,
			"data": "0,0,1,0,1,0,1,0,2,0,1,1,1,0,2,0,1,1,1,0,2,0,1,1,2,0,3,0,1,2,1,0,2,0,2,2,1,0,2,1,1,1,1,0,4,0,1,2,2,1,2,0,1,2,2,1,2,0,1,3,1,0,4,0,2,1,1,0,2,2,1,3,1,0,4,0,2,1,1,1,5,0,1,3,2,0,2,0,1,5,2,0,2,0,2,2,1,1,4,1,1,1,1,0,5,0,1,6",
			"name": "Number of factorizations of n into factors \u003e 1 with odd least factor.",
			"link": [
				"Antti Karttunen, \u003ca href=\"/A340832/b340832.txt\"\u003eTable of n, a(n) for n = 1..20000\u003c/a\u003e"
			],
			"example": [
				"The a(n) factorizations for n = 45, 108, 135, 180, 252:",
				"  (45)     (3*36)     (135)      (3*60)     (3*84)",
				"  (5*9)    (9*12)     (3*45)     (5*36)     (7*36)",
				"  (3*15)   (3*4*9)    (5*27)     (9*20)     (9*28)",
				"  (3*3*5)  (3*6*6)    (9*15)     (5*6*6)    (3*3*28)",
				"           (3*3*12)   (3*5*9)    (3*3*20)   (3*4*21)",
				"           (3*3*3*4)  (3*3*15)   (3*4*15)   (3*6*14)",
				"                      (3*3*3*5)  (3*5*12)   (3*7*12)",
				"                                 (3*6*10)   (3*3*4*7)",
				"                                 (3*3*4*5)"
			],
			"mathematica": [
				"facs[n_]:=If[n\u003c=1,{{}},Join@@Table[Map[Prepend[#,d]\u0026,Select[facs[n/d],Min@@#\u003e=d\u0026]],{d,Rest[Divisors[n]]}]];",
				"Table[Length[Select[facs[n],OddQ@*Min]],{n,100}]"
			],
			"program": [
				"(PARI) A340832(n, m=n, fc=1) = if(1==n, (m%2)\u0026\u0026!fc, my(s=0); fordiv(n, d, if((d\u003e1)\u0026\u0026(d\u003c=m), s += A340832(n/d, d, 0*fc))); (s)); \\\\ _Antti Karttunen_, Dec 13 2021"
			],
			"xref": [
				"Positions of 0's are A340854.",
				"Positions of nonzero terms are A340855.",
				"The version for partitions is A026804.",
				"Odd-length factorizations are counted by A339890.",
				"The version looking at greatest factor is A340831.",
				"- Factorizations -",
				"A001055 counts factorizations.",
				"A045778 counts strict factorizations.",
				"A316439 counts factorizations by product and length.",
				"A340101 counts factorizations into odd factors, odd-length case A340102.",
				"A340607 counts factorizations with odd length and greatest factor.",
				"A340653 counts balanced factorizations.",
				"- Odd -",
				"A000009 counts partitions into odd parts.",
				"A026424 lists numbers with odd Omega.",
				"A027193 counts partitions of odd length.",
				"A058695 counts partitions of odd numbers (A300063).",
				"A066208 lists numbers with odd-indexed prime factors.",
				"A067659 counts strict partitions of odd length (A030059).",
				"A174726 counts ordered factorizations of odd length.",
				"A244991 lists numbers whose greatest prime index is odd.",
				"A340692 counts partitions of odd rank.",
				"Cf. A050320, A160786, A340385, A340596, A340599, A340654, A340655, A340931."
			],
			"keyword": "nonn",
			"offset": "1,9",
			"author": "_Gus Wiseman_, Feb 04 2021",
			"ext": [
				"Data section extended up to 108 terms by _Antti Karttunen_, Dec 13 2021"
			],
			"references": 15,
			"revision": 9,
			"time": "2021-12-13T16:15:05-05:00",
			"created": "2021-02-04T20:53:14-05:00"
		}
	]
}