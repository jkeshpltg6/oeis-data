{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A307672",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 307672,
			"data": "0,1,2,1,0,1,2,3,2,1,0,5,0,1,2,1,0,1,2,3,4,3,2,1,2,3,2,1,0,5,0,1,0,5,4,5,0,1,2,1,0,1,2,3,2,1,0,5,0,1,2,1,0,1,2,3,4,3,2,3,4,5,4,3,2,1,2,3,2,1,0,1,2,3,4,3,2,1,2,3,2,1,0,5,0",
			"name": "The right half of a bi-infinite word invariant under the balanced morphism, {0-\u003e501, 1-\u003e210, 2-\u003e123, 3-\u003e432, 4-\u003e345, 5-\u003e054}, starting from axiom a(0)=0.",
			"comment": [
				"The adjective \"balanced\" indicates that the fixed point a(0)=0 descends through iteration along a central dividing line, which bisects the ternary family tree into left and right halves, equal by node cardinality (see examples). From the original Gosper and Ziegler-Hunts reference (see links), a(k) = d(k) mod 6. The function d(k) draws left and right halves of the Sierpiński Arrowhead curve (see links). Alphanumeric transformation {0-\u003ea, 2-\u003eb, 4-\u003ec, 3-\u003eA, 5-\u003eB, 1-\u003eC} obtains d(k) in the form of lettered sets. By design, letters {a,b,c} occur only on even indices, while letters {A,B,C} occur only on odd indices. According to the principal eigenvector of the substitution system, occurrence tallies should asymptotically approach a uniform distribution over the six numbers or letters.",
				"From _Peter Munn_, May 28 2019: (Start)",
				"The sequence maps to half of an infinite Sierpinski arrowhead curve by mapping the values 0..5 to six unit vectors spaced at equal angles (Pi/3) in counterclockwise (or clockwise) order, then placing the vector image of each sequence term head to tail. Curve edges indexed 0..121 form the upper half of the curve in Figure 5 of the Gosper \u0026 Ziegler-Hunts reference (see links). The figure has the vector image of 0 pointing upwards, the red-colored segment runs from index -40 to +40 and the blue-colored segment from 41 to 121.",
				"The arrowhead curve (both halves and continued to infinity) will align with an infinite Sierpinski gasket so that each of its edges is contained in the boundary of either the plane sector occupied by the gasket or a triangular region of the gasket's complement. Every length 3 segment of these boundaries contains exactly one edge of the arrowhead curve. See the link for the aligned curves.",
				"For a given triangular boundary (or a given edge of the gasket sector boundary) the indices of the arrowhead edges it contains differ by multiples of 4. The edges in the gasket sector boundary are listed (absolute value of index) in A191108. Otherwise, edge n seems to be contained in a triangular boundary of side 2^(A307744(n)-1).",
				"The arrowhead curve divides the plane into two regions. Denote the region that is wholly within the sector occupied by the gasket as the inside of the arrowhead. The curve's even-indexed edges are in triangular boundaries that lie inside the arrowhead, and the odd-indexed edges are not.",
				"When the term-to-vector map described above is applied to the sequence bisections, we get related curves. The even-indexed curve reproduces the boundaries of all triangular regions of the gasket's complement, of unit side and greater, that lie inside the arrowhead; the odd-indexed curve reproduces the boundaries of the equivalent regions outside the arrowhead plus the gasket sector boundary. See the link for the aligned curves.",
				"Recall that every length 3 boundary segment contains exactly one arrowhead edge. In the curve drawn by a(0), a(2), a(4), ... the image of a(6n) co-incides with the image of a(2n) in the arrowhead curve, and the images of a(6n-2), a(6n) and (6n+2) form a length 3 boundary segment. Similarly, in the curve drawn by a(1), a(3), a(5), ... the image of a(6n+3) co-incides with the image of a(2n+1) in the arrowhead curve, and the images of a(6n+1), a(6n+3) and a(6n+5) form a length 3 boundary segment.",
				"One bisection produces vectors that draw triangular boundaries clockwise, the other counterclockwise. This must be so, because (1) the full sequence alternates odd and even, (2) opposite vectors are images of numbers with opposite parity, and (3) the gasket complement's triangular regions have the same orientation.",
				"(End)"
			],
			"link": [
				"Peter Munn, \u003ca href=\"/A307672/b307672.txt\"\u003eTable of n, a(n) for n = 0..5000\u003c/a\u003e",
				"R. W. Gosper and J. Ziegler-Hunts, \u003ca href=\"https://www.gathering4gardner.org/g4g10gift/math/Gosper_Bill-Strange_Series_for_Sierpinskis_Gasket.pdf\"\u003eStrange Series for Sierpinski’s Gasket\u003c/a\u003e, G4G10 Exchange Book, Vol. 2 (2012), 59-60.",
				"Sean A. Irvine, \u003ca href=\"https://github.com/archmageirvine/joeis/blob/master/src/irvine/oeis/a307/A307672.java\"\u003eJava program\u003c/a\u003e (github).",
				"Bradley Klee, \u003ca href=\"/A307672/a307672.png\"\u003eSierpiński Arrowhead, Right Half\u003c/a\u003e.",
				"Bradley Klee, \u003ca href=\"/A307672/a307672_1.png\"\u003eSierpiński Arrowhead, Left and Right\u003c/a\u003e.",
				"Peter Munn, \u003ca href=\"/A307672/a307672_2.png\"\u003eMapped curves aligned with Sierpiński Gasket\u003c/a\u003e."
			],
			"example": [
				"The full ternary tree starts:",
				"      0",
				"     501",
				"  054501210"
			],
			"mathematica": [
				"Arrowhead = {0-\u003e{5, 0, 1}, 1-\u003e{2, 1, 0}, 2-\u003e{1, 2, 3}, 3-\u003e{4, 3, 2}, 4-\u003e{3, 4, 5}, 5-\u003e{0, 5, 4}};",
				"aR[n_]:=Nest[Part[Flatten[#/.Arrowhead], 2;;-1]\u0026,{0},n];aR[7]",
				"(* Second program: *)",
				"S = SubstitutionSystem[{0 -\u003e {5, 0, 1}, 1 -\u003e {2, 1, 0}, 2 -\u003e {1, 2, 3}, 3 -\u003e {4, 3, 2}, 4 -\u003e {3, 4, 5}, 5 -\u003e {0, 5, 4}}, {0}, 5][[-1]]; S[[Ceiling[ Length[S]/2];;]] (* _Jean-François Alcover_, May 08 2019 *)"
			],
			"xref": [
				"Cf. A010060, A053838, A287385, A306287, A306288, A307744, A308364.",
				"Lists that identify significant subsets of the Sierpinski arrowhead edges: A019989, A019990, A019991, A191108.",
				"Cf. A156595 (draws the Sierpinski gasket)."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Bradley Klee_ and _Peter Munn_, Apr 20 2019",
			"references": 10,
			"revision": 50,
			"time": "2021-06-09T02:57:02-04:00",
			"created": "2019-04-30T18:46:16-04:00"
		}
	]
}