{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A122890",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 122890,
			"data": "1,0,1,0,0,2,0,0,1,5,0,0,0,10,14,0,0,0,8,70,42,0,0,0,4,160,424,132,0,0,0,1,250,1978,2382,429,0,0,0,0,302,6276,19508,12804,1430,0,0,0,0,298,15674,106492,168608,66946,4862,0,0,0,0,244,33148,451948,1445208",
			"name": "Triangle, read by rows, where the g.f. of row n divided by (1-x)^n yields the g.f. of column n in the triangle A122888, for n\u003e=1.",
			"comment": [
				"Main diagonal forms the Catalan numbers (A000108). Row sums gives the factorials. In table A122888, row n lists the coefficients of x^k, k = 1..2^n, in the n-th self-composition of (x + x^2) for n \u003e= 0.",
				"Parker gave the following combinatorial interpretation of the numbers: For n \u003e 0, T(n, j) is the number of sequences c_1c_2...c_n of positive integers such that 1 \u003c= c_i \u003c= i for each i in {1, 2, .., n} with exactly j - 1 values of i such that c_i \u003c= c_{i+1}. - _Peter Luschny_, May 05 2013"
			],
			"link": [
				"Toufik Mansour, Mark Shattuck, \u003ca href=\"https://www.dmlett.com/archive/v4/DML20_v4_p42_49.pdf\"\u003eStatistics on bargraphs of inversion sequences of permutations\u003c/a\u003e, Discrete Math. Lett. (2020) Vol. 4, 42-49.",
				"Toufik Mansour, Howard Skogman, Rebecca Smith, \u003ca href=\"https://arxiv.org/abs/1704.04288\"\u003ePassing through a stack k times\u003c/a\u003e, arXiv:1704.04288 [math.CO], 2017.",
				"Susan Field Parker, \u003ca href=\"http://people.brandeis.edu/~gessel/homepage/students/parkerthesis.pdf\"\u003eThe Combinatorics of Functional Composition and Inversion\u003c/a\u003e, Ph.D. Dissertation, Brandeis Univ. (1993) (Section 2.3.4, p. 27,28.)"
			],
			"formula": [
				"From _Paul D. Hanna_, Apr 11 2009: (Start)",
				"G.f. of row n: (1-x)^n*[g.f. of column n of A122888] where the g.f. of row n of A122888 is the n-th iteration of x+x^2.",
				"Row-reversal forms triangle A158830 where g.f. of row n of A158830 = (1-x)^n*[g.f. of column n of A158825], and the g.f. of row n of array A158825 is the n-th iteration of x*C(x) and C(x) is the g.f. of the Catalan sequence A000108. (End)"
			],
			"example": [
				"Triangle begins:",
				".1;",
				".0,1;",
				".0,0,2;",
				".0,0,1,5;",
				".0,0,0,10,14;",
				".0,0,0,8,70,42;",
				".0,0,0,4,160,424,132;",
				".0,0,0,1,250,1978,2382,429;",
				".0,0,0,0,302,6276,19508,12804,1430;",
				".0,0,0,0,298,15674,106492,168608,66946,4862;",
				".0,0,0,0,244,33148,451948,1445208,1337684,343772,16796;",
				".0,0,0,0,162,61806,1614906,9459090,16974314,10003422,1744314,58786;",
				".0,0,0,0,84,103932,5090124,51436848,161380816,180308420,71692452,8780912,208012;",
				"Table A122888 starts:",
				".1;",
				".1, 1;",
				".1, 2, 2, 1;",
				".1, 3, 6, 9, 10, 8, 4, 1;",
				".1, 4, 12, 30, 64, 118, 188, 258, 302, 298, 244, 162, 84, 32, 8, 1;",
				".1, 5, 20, 70, 220, 630, 1656, 4014, 8994, 18654, 35832, 63750,...;",
				".1, 6, 30, 135, 560, 2170, 7916, 27326, 89582, 279622, 832680,...;",
				"where row n gives the g.f. of the n-th self-composition of (x+x^2).",
				"From _Paul D. Hanna_, Apr 11 2009: (Start)",
				"ROW-REVERSAL yields triangle A158830:",
				".1;",
				".1,0;",
				".2,0,0;",
				".5,1,0,0;",
				".14,10,0,0,0;",
				".42,70,8,0,0,0;",
				".132,424,160,4,0,0,0;",
				".429,2382,1978,250,1,0,0,0; ...",
				"where",
				"g.f. of row n of A158830 = (1-x)^n*[g.f. of column n of A158825];",
				"g.f. of row n of A158825 = n-th iteration of x*Catalan(x).",
				"RELATED ARRAY A158825 begins:",
				".1,1,2,5,14,42,132,429,1430,4862,16796,58786,...;",
				".1,2,6,21,80,322,1348,5814,25674,115566,528528,...;",
				".1,3,12,54,260,1310,6824,36478,199094,1105478,...;",
				".1,4,20,110,640,3870,24084,153306,993978,...;",
				".1,5,30,195,1330,9380,67844,500619,3755156,...;",
				".1,6,42,315,2464,19852,163576,1372196,11682348,...;",
				".1,7,56,476,4200,38052,351792,3305484,31478628,...;",
				".1,8,72,684,6720,67620,693048,7209036,75915708,...; ...",
				"which consists of successive iterations of x*Catalan(x).",
				"(End)"
			],
			"mathematica": [
				"nmax = 11;",
				"f[0][x_] := x; f[n_][x_] := f[n][x] = f[n - 1][x + x^2] // Expand; T = Table[ SeriesCoefficient[f[n][x], {x, 0, k}], {n, 0, nmax}, {k, 1, nmax}];",
				"row[n_] := CoefficientList[(1-x)^n*(T[[All, n]].x^Range[0, nmax])+O[x]^nmax, x];",
				"Table[row[n], {n, 1, nmax}] // Flatten (* _Jean-François Alcover_, Jul 13 2018 *)"
			],
			"xref": [
				"Cf. A122888; A122891 (column sums); diagonals: A122892, A000108.",
				"Cf. related tables: A158830, A158825. [_Paul D. Hanna_, Apr 11 2009]"
			],
			"keyword": "nonn,tabl",
			"offset": "0,6",
			"author": "_Paul D. Hanna_, Sep 18 2006",
			"references": 7,
			"revision": 30,
			"time": "2020-11-12T20:34:16-05:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}