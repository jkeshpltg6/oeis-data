{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A135416",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 135416,
			"data": "1,0,2,0,0,0,4,0,0,0,0,0,0,0,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,16,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,32,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0",
			"name": "a(n) = A036987(n)*(n+1)/2.",
			"comment": [
				"Guy Steele defines a family of 36 integer sequences, denoted here by GS(i,j) for 1 \u003c= i, j \u003c= 6, as follows. a[1]=1; a[2n] = i-th term of {0,1,a[n],a[n]+1,2a[n],2a[n]+1}; a[2n+1] = j-th term of {0,1,a[n],a[n]+1,2a[n],2a[n]+1}. The present sequence is GS(1,5).",
				"The full list of 36 sequences:",
				"GS(1,1) = A000007",
				"GS(1,2) = A000035",
				"GS(1,3) = A036987",
				"GS(1,4) = A007814",
				"GS(1,5) = A135416 (the present sequence)",
				"GS(1,6) = A135481",
				"GS(2,1) = A135528",
				"GS(2,2) = A000012",
				"GS(2,3) = A000012",
				"GS(2,4) = A091090",
				"GS(2,5) = A135517",
				"GS(2,6) = A135521",
				"GS(3,1) = A036987",
				"GS(3,2) = A000012",
				"GS(3,3) = A000012",
				"GS(3,4) = A000120",
				"GS(3,5) = A048896",
				"GS(3,6) = A038573",
				"GS(4,1) = A135523",
				"GS(4,2) = A001511",
				"GS(4,3) = A008687",
				"GS(4,4) = A070939",
				"GS(4,5) = A135529",
				"GS(4,6) = A135533",
				"GS(5,1) = A048298",
				"GS(5,2) = A006519",
				"GS(5,3) = A080100",
				"GS(5,4) = A087808",
				"GS(5,5) = A053644",
				"GS(5,6) = A000027",
				"GS(6,1) = A135534",
				"GS(6,2) = A038712",
				"GS(6,3) = A135540",
				"GS(6,4) = A135542",
				"GS(6,5) = A054429",
				"GS(6,6) = A003817",
				"(with a(0)=1): Moebius transform of A038712."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A135416/b135416.txt\"\u003eTable of n, a(n) for n = 1..65537\u003c/a\u003e",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e"
			],
			"formula": [
				"G.f.: sum{k\u003e=1, 2^(k-1)*x^(2^k-1) }.",
				"Recurrence: a(2n+1) = 2a(n), a(2n) = 0, starting a(1) = 1."
			],
			"maple": [
				"GS:=proc(i,j,M) local a,n; a:=array(1..2*M+1); a[1]:=1;",
				"for n from 1 to M do",
				"a[2*n] :=[0,1,a[n],a[n]+1,2*a[n],2*a[n]+1][i];",
				"a[2*n+1]:=[0,1,a[n],a[n]+1,2*a[n],2*a[n]+1][j];",
				"od: a:=convert(a,list); RETURN(a); end;",
				"GS(1,5,200):"
			],
			"mathematica": [
				"i = 1; j = 5; Clear[a]; a[1] = 1; a[n_?EvenQ] := a[n] = {0, 1, a[n/2], a[n/2]+1, 2*a[n/2], 2*a[n/2]+1}[[i]]; a[n_?OddQ] := a[n] = {0, 1, a[(n-1)/2], a[(n-1)/2]+1, 2*a[(n-1)/2], 2*a[(n-1)/2]+1}[[j]]; Array[a, 105] (* _Jean-François Alcover_, Sep 12 2013 *)"
			],
			"program": [
				"(PARI)",
				"A048298(n) = if(!n,0,if(!bitand(n,n-1),n,0));",
				"A135416(n) = (A048298(n+1)/2); \\\\ _Antti Karttunen_, Jul 22 2018"
			],
			"xref": [
				"Equals A048298(n+1)/2. Cf. A036987, A182660."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_, based on a message from Guy Steele and _Don Knuth_, Mar 01 2008",
			"ext": [
				"Formulae and comments by _Ralf Stephan_, Jun 20 2014"
			],
			"references": 31,
			"revision": 21,
			"time": "2018-07-22T22:55:05-04:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}