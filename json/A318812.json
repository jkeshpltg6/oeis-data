{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A318812",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 318812,
			"data": "1,1,1,1,1,1,1,2,1,1,1,3,1,1,1,6,1,3,1,3,1,1,1,11,1,1,2,3,1,4,1,20,1,1,1,15,1,1,1,11,1,4,1,3,3,1,1,51,1,3,1,3,1,11,1,11,1,1,1,21,1,1,3,90,1,4,1,3,1,4,1,80,1,1,3,3,1,4,1,51,6,1,1",
			"name": "Number of total multiset partitions of the multiset of prime indices of n. Number of total factorizations of n.",
			"comment": [
				"A total multiset partition of m is either m itself or a total multiset partition of a multiset partition of m that is neither minimal nor maximal.",
				"a(n) depends only on the prime signature of n. - _Andrew Howroyd_, Dec 30 2019"
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A318812/b318812.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"a(product of n distinct primes) = A005121(n).",
				"a(prime^n) = A318813(n)."
			],
			"example": [
				"The a(24) = 11 total multiset partitions:",
				"  {1,1,1,2}",
				"  {{1},{1,1,2}}",
				"  {{2},{1,1,1}}",
				"  {{1,1},{1,2}}",
				"  {{1},{1},{1,2}}",
				"  {{1},{2},{1,1}}",
				"  {{{1}},{{1},{1,2}}}",
				"  {{{1}},{{2},{1,1}}}",
				"  {{{2}},{{1},{1,1}}}",
				"  {{{1,2}},{{1},{1}}}",
				"  {{{1,1}},{{1},{2}}}",
				"The a(24) = 11 total factorizations:",
				"  24,",
				"  (2*12), (3*8), (4*6),",
				"  (2*2*6), (2*3*4),",
				"  ((2)*(2*6)), ((6)*(2*2)), ((2)*(3*4)), ((3)*(2*4)), ((4)*(2*3))."
			],
			"mathematica": [
				"facs[n_]:=If[n\u003c=1,{{}},Join@@Table[Map[Prepend[#,d]\u0026,Select[facs[n/d],Min@@#\u003e=d\u0026]],{d,Rest[Divisors[n]]}]];",
				"totfac[n_]:=1+Sum[totfac[Times@@Prime/@f],{f,Select[facs[n],1\u003cLength[#]\u003cPrimeOmega[n]\u0026]}];",
				"Array[totfac,100]"
			],
			"program": [
				"(PARI)",
				"MultEulerT(u)={my(v=vector(#u)); v[1]=1; for(k=2, #u, forstep(j=#v\\k*k, k, -k, my(i=j, e=0); while(i%k==0, i/=k; e++; v[j]+=binomial(e+u[k]-1, e)*v[i]))); v}",
				"seq(n)={my(v=vector(n, i, isprime(i)), u=vector(n), m=logint(n,2)+1); for(r=1, m, u += v*sum(j=r, m, (-1)^(j-r)*binomial(j-1, r-1)); v=MultEulerT(v)); u[1]=1; u} \\\\ _Andrew Howroyd_, Dec 30 2019"
			],
			"xref": [
				"Cf. A000110, A001055, A002846, A005121, A213427, A281113, A281118, A281119, A317145, A318813."
			],
			"keyword": "nonn",
			"offset": "1,8",
			"author": "_Gus Wiseman_, Sep 04 2018",
			"references": 31,
			"revision": 11,
			"time": "2019-12-31T06:49:32-05:00",
			"created": "2018-09-05T17:14:46-04:00"
		}
	]
}