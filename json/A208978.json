{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A208978",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 208978,
			"data": "1,1,-1,1,1,-2,-1,-2,0,0,-1,1,-1,1,0,-1,-1,1,2,0,1,-2,1,1,0,1,2,1,0,1,-1,0,-1,1,0,1,-2,-2,2,0,-1,-1,0,-1,0,1,2,-2,1,0,-2,-2,-1,0,-1,1,-1,-1,-1,0,0,1,-1,1,0,0,2,2,-1,0,-1,-1,2,-2,0,0,2",
			"name": "Expansion of f(x) * f(x^3) where f() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Number 72 of the 74 eta-quotients listed in Table I of Martin (1996)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A208978/b208978.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Y. Martin, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9947-96-01743-6\"\u003eMultiplicative eta-quotients\u003c/a\u003e, Trans. Amer. Math. Soc. 348 (1996), no. 12, 4825-4856, see page 4852 Table I.",
				"Michael Somos, \u003ca href=\"/A030203/a030203.txt\"\u003eIndex to Yves Martin's list of 74 multiplicative eta-quotients and their A-numbers\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(-1/6) * (eta(q^2) * eta(q^6))^3 / (eta(q) * eta(q^3) * eta(q^4) * eta(q^12)) in powers of q.",
				"Euler transform of period 12 sequence [ 1, -2, 2, -1, 1, -4, 1, -1, 2, -2, 1, -2, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (432 t)) = 432^(1/2) (t/i) f(t) where q = exp(2 Pi i t).",
				"a(n) = b(6*n + 1) where b(n) is multiplicative with b(2^e) = b(3^e) = 0^e, b(p^e) = (1 + (-1)^e) / 2 if p == 5 (mod 6), b(p^e) = (-1)^(floor(p/6) * e) * t if p == 1 (mod 6) where t = e+1 if p = x^2 + 27*y^2, and t = [1, -1, 0] depending on e (mod 3) otherwise.",
				"G.f.: Product_{k\u003e0} (1 - (-x)^k) * (1 - (-x)^(3*k)).",
				"a(n) = (-1)^n * A030203(n)."
			],
			"example": [
				"G.f. = 1 + x - x^2 + x^3 + x^4 - 2*x^5 - x^6 - 2*x^7 - x^10 + x^11 - x^12 + ...",
				"G.f. = q + q^7 - q^13 + q^19 + q^25 - 2*q^31 - q^37 - 2*q^43 - q^61 + q^67 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ -x] QPochhammer[ -x^3] , {x, 0, n}]; (* _Michael Somos_, Jun 09 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A, p, e); if( n\u003c0, 0, n = 6*n + 1; A = factor(n); prod( k=1, matsize(A)[1], [p, e] = A[k,]; if( p\u003c5, 0, p%6==5, (1 + (-1)^e)/2, (-1)^(p\\6*e) * if( (p-1) / znorder( Mod(2, p))%3, kronecker( e+1, 3), e+1))))};",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( (eta(x^2 + A) * eta(x^6 + A))^3 / (eta(x + A) * eta(x^3 + A) * eta(x^4 + A) * eta(x^12 + A)), n))};"
			],
			"xref": [
				"Cf. A030203."
			],
			"keyword": "sign",
			"offset": "0,6",
			"author": "_Michael Somos_, Mar 03 2012",
			"references": 2,
			"revision": 24,
			"time": "2021-03-12T22:24:46-05:00",
			"created": "2012-03-05T12:26:51-05:00"
		}
	]
}