{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A220347",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 220347,
			"data": "1,2,3,4,6,5,8,12,10,7,16,24,20,14,11,32,48,40,28,22,9,64,96,80,56,44,18,15,128,192,160,112,88,36,30,23,256,384,320,224,176,72,60,46,19,512,768,640,448,352,144,120,92,38,13,1024,1536,1280,896,704,288",
			"name": "Permutation of natural numbers: a(1) = 1, a(triangular(n)) = (2*a(n))-1, a(nontriangular(n)) = 2*n, where triangular = A000217, nontriangular = A014132.",
			"comment": [
				"Inverse permutation of A183079, when seen as a flattened sequence."
			],
			"link": [
				"Reinhard Zumkeller (first 250 terms) \u0026  Antti Karttunen, \u003ca href=\"/A220347/b220347.txt\"\u003eTable of n, a(n) for n = 1..10440\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"a(1) = 1; for n \u003e 1: if A010054(n) = 1 [i.e., if n is triangular], then a(n) = (2*a(A002024(n)))-1, otherwise a(n) = 2*a(A083920(n)). - _Antti Karttunen_, May 18 2015"
			],
			"mathematica": [
				"a[n_] := a[n] = With[{r = (-1 + Sqrt[8n + 1])/2}, Which[n \u003c= 1, n, IntegerQ[r], 2 a[Floor[Sqrt[2n] + 1/2]] - 1, True, 2 a[n - Floor[r]]]];",
				"Table[a[n], {n, 1, 100}] (* _Jean-François Alcover_, Dec 05 2021 *)"
			],
			"program": [
				"(Haskell)",
				"import Data.List (elemIndex)",
				"import Data.Maybe (fromJust)",
				"a220347 =  (+ 1) . fromJust . (`elemIndex` a183079_list)",
				"(Scheme, with memoizing definec-macro)",
				"(definec (A220347 n) (cond ((\u003c= n 1) n) ((zero? (A010054 n)) (* 2 (A220347 (A083920 n)))) (else (+ -1 (* 2 (A220347 (A002024 n)))))))",
				";; _Antti Karttunen_, May 18 2015"
			],
			"xref": [
				"Inverse: A183079.",
				"Cf. A000217, A002024, A010054, A014132, A083920, A220348.",
				"Cf. also a similar permutation A257797 from which this differs for the first time at n=15, where a(15) = 11, while A257797(15) = 9."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Reinhard Zumkeller_, Dec 12 2012",
			"ext": [
				"Old name moved to comments by _Antti Karttunen_, May 18 2015"
			],
			"references": 4,
			"revision": 20,
			"time": "2021-12-05T17:27:26-05:00",
			"created": "2012-12-12T16:04:06-05:00"
		}
	]
}