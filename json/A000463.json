{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000463",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 463,
			"data": "1,1,2,4,3,9,4,16,5,25,6,36,7,49,8,64,9,81,10,100,11,121,12,144,13,169,14,196,15,225,16,256,17,289,18,324,19,361,20,400,21,441,22,484,23,529,24,576,25,625,26,676,27,729,28,784,29,841,30,900,31,961,32,1024,33,1089,34,1156,35,1225,36,1296",
			"name": "n followed by n^2.",
			"comment": [
				"Eigensequence of a triangle with nonnegative integers interlaced with zeros (1, 0, 2, 0, 3, ...) as the right and left borders, with the rest zeros. - _Gary W. Adamson_, Aug 01 2016"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A000463/b000463.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,3,0,-3,0,1)."
			],
			"formula": [
				"a(n) = ((((-1)^(n+1))+1)/4)(n+1) - ((((-1)^(n+1))-1)/8)n^2 - _Sam Alexander_",
				"G.f.: (1+x-x^2+x^3)/((1-x)^3(1+x)^3).",
				"a(n) = if(n mod 2, (n+1)/2, (n/2)^2). - _Gerald Hillier_, Sep 25 2008",
				"a(n) = floor((n+1) / 2) ^ (2 - n mod 2). - _Reinhard Zumkeller_, Aug 15 2011",
				"E.g.f.: (x + 2)*(sinh(x) + x*cosh(x))/4. - _Ilya Gutkovskiy_, Aug 02 2016"
			],
			"example": [
				"G.f. = x + x^2 + 2*x^3 + 4*x^4 + 3*x^5 + 9*x^6 + 4*x^7 + 16*x^8 + ..."
			],
			"maple": [
				"seq(seq(n^k, k=1..2), n=1..36); # _Zerinvary Lajos_, Jun 29 2007"
			],
			"mathematica": [
				"Array[{#, #^2} \u0026, 36, 0] // Flatten",
				"Riffle[Range[40], Range[40]^2] (* _Bruno Berselli_, Jul 15 2013 *)",
				"a[ n_] := If[ OddQ @ n, (n + 1) / 2, n^2 / 4]; (* _Michael Somos_, May 28 2014 *)"
			],
			"program": [
				"(MAGMA) \u0026cat[ [ n, n^2 ]: n in [1..36] ]; // _Klaus Brockhaus_, Apr 20 2009",
				"(Haskell)",
				"a000463 n = a000463_list !! (n-1)",
				"a000463_list = concatMap (\\x -\u003e [x,x^2]) [1..]",
				"-- _Reinhard Zumkeller_, Apr 13 2011",
				"(PARI) {a(n) = if( n%2, (n + 1) / 2, n^2 / 4)}; /* _Michael Somos_, May 28 2014 */"
			],
			"xref": [
				"Cf. A188652 (first differences), A188653 (second differences), A159693 (partial sums), A000290 (squares)."
			],
			"keyword": "nonn,easy,look",
			"offset": "1,3",
			"author": "_Dominick Cancilla_",
			"ext": [
				"Square of 14 corrected by _Sean A. Irvine_, Oct 25 2010"
			],
			"references": 25,
			"revision": 54,
			"time": "2017-01-31T01:28:10-05:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}