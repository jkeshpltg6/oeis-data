{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A257989",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 257989,
			"data": "-1,2,-2,3,0,4,-3,2,0,5,-2,6,0,3,-4,7,1,8,-1,4,0,9,-3,3,0,2,-1,10,1,11,-5,5,0,4,-2,12,0,6,-3,13,1,14,-1,3,0,15,-4,4,1,7,-1,16,2,5,-2,8,0,17,-1,18,0,4,-6,6,1,19,-1,9,1,20,-3,21,0,3,-1,5,1,22,-4,2,0,23,-1,7,0,10,-2,24,2,6,-1",
			"name": "The crank of the partition having Heinz number n.",
			"comment": [
				"The crank of a partition p is defined to be (i) the largest part of p if there is no 1 in p and (ii) (the number of parts larger than the number of 1's) minus (the number of 1's).",
				"We define the Heinz number of a partition p = [p_1, p_2, ..., p_r] as Product(p_j-th prime, j=1...r) (concept used by _Alois P. Heinz_ in A215366 as an \"encoding\" of a partition). For example, for the partition [1, 1, 2, 4, 10] we get 2*2*3*7*29 = 2436.",
				"In the Maple program the subprogram B yields the partition with Heinz number n, the subprogram b yields the number of 1's in the partition with Heinz number n and the subprogram c yields the number of parts that are larger than the number of 1's in the partition with the Heinz number n."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A257989/b257989.txt\"\u003eTable of n, a(n) for n = 2..10000\u003c/a\u003e",
				"G. E. Andrews and F. Garvan, \u003ca href=\"http://dx.doi.org/10.1090/S0273-0979-1988-15637-6\"\u003eDyson's crank of a partition\u003c/a\u003e, Bull. Amer. Math. Soc., 18 (1988), 167-171.",
				"B. C. Berndt, H. H. Chan, S. H. Chan, W.-C. Liaw, \u003ca href=\"http://dx.doi.org/10.1016/j.jcta.2004.06.013\"\u003eCranks and dissections in Ramanujan's lost notebook\u003c/a\u003e, J. Comb. Theory, Ser. A, 109, 2005, 91-120.",
				"B. C. Berndt, H. H. Chan, S. H. Chan, W.-C. Liaw, \u003ca href=\"http://www.math.uiuc.edu/~berndt/articles/finalproblem.pdf\"\u003eCranks - really the final problem\u003c/a\u003e, Ramanujan J., 23, 2010, 3-15.",
				"G. E. Andrews, K. Ono, \u003ca href=\"http://dx.doi.org/10.1073/pnas.0507844102\"\u003eRamanujan's congruences and Dyson's crank\u003c/a\u003e, Proc. Natl. Acad. Sci. USA, 102, 2005, 15277.",
				"FindStat, \u003ca href=\"http://www.findstat.org/StatisticsDatabase/St000474/\"\u003eDyson's crank of a partition.\u003c/a\u003e",
				"K. Mahlburg, \u003ca href=\"http://dx.doi.org/10.1073/pnas.0506702102\"\u003ePartition congruences and the Andrews-Garvan-Dyson crank\u003c/a\u003e, Proc. Natl. Acad. Sci. USA, 102, 2005, 15373-15376.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Crank_of_a_partition\"\u003eCrank of a partition\u003c/a\u003e"
			],
			"example": [
				"a(12) = - 2 because the partition with Heinz number 12 = 2*2*3 is [1,1,2], the number of parts larger than the number of 1's is 0 and the number of 1's is 2; 0 - 2 = -2.",
				"a(945) = 4 because the partition with Heinz number 945 = 3^3 * 5 * 7 is [2,2,2,3,4] which has no part 1; the largest part is 4.",
				"From _Gus Wiseman_, Apr 05 2021: (Start)",
				"The partitions (center) with each Heinz number (left), and the corresponding terms (right):",
				"   2:    (1)    -\u003e -1",
				"   3:    (2)    -\u003e  2",
				"   4:   (1,1)   -\u003e -2",
				"   5:    (3)    -\u003e  3",
				"   6:   (2,1)   -\u003e  0",
				"   7:    (4)    -\u003e  4",
				"   8:  (1,1,1)  -\u003e -3",
				"   9:   (2,2)   -\u003e  2",
				"  10:   (3,1)   -\u003e  0",
				"  11:    (5)    -\u003e  5",
				"  12:  (2,1,1)  -\u003e -2",
				"  13:    (6)    -\u003e  6",
				"  14:   (4,1)   -\u003e  0",
				"  15:   (3,2)   -\u003e  3",
				"  16: (1,1,1,1) -\u003e -4",
				"(End)"
			],
			"maple": [
				"with(numtheory): a := proc (n) local B, b, c: B := proc (n) local nn, j, m: nn := op(2, ifactors(n)): for j to nops(nn) do m[j] := op(j, nn) end do; [seq(seq(pi(op(1, m[i])), q = 1 .. op(2, m[i])), i = 1 .. nops(nn))] end proc: b := proc (n) if `mod`(n, 2) = 1 then 0 else 1+b((1/2)*n) end if end proc: c := proc (n) local b, B, ct, i: b := proc (n) if `mod`(n, 2) = 1 then 0 else 1+b((1/2)*n) end if end proc: B := proc (n) local nn, j, m: nn := op(2, ifactors(n)): for j to nops(nn) do m[j] := op(j, nn) end do: [seq(seq(pi(op(1, m[i])), q = 1 .. op(2, m[i])), i = 1 .. nops(nn))] end proc: ct := 0: for i to bigomega(n) do if b(n) \u003c B(n)[i] then ct := ct+1 else  end if end do: ct end proc: if b(n) = 0 then max(B(n)) else c(n)-b(n) end if end proc: seq(a(n), n = 2 .. 150);"
			],
			"mathematica": [
				"B[n_] := Module[{nn, j, m}, nn =  FactorInteger[n]; For[j = 1, j \u003c= Length[nn], j++, m[j] = nn[[j]]]; Flatten[Table[Table[PrimePi[m[i][[1]]], {q, 1, m[i][[2]]}], {i, 1, Length[nn]}]]];",
				"b[n_] := b[n] = If[OddQ[n], 0, 1 + b[n/2]];",
				"c[n_] := Module[{ct, i}, ct = 0; For[i = 1, i \u003c= PrimeOmega[n], i++, If[ b[n] \u003c B[n][[i]], ct++]]; ct];",
				"a[n_] := If[b[n] == 0, Max[B[n]], c[n] - b[n]];",
				"Table[a[n], {n, 2, 100}] (* _Jean-François Alcover_, Apr 25 2017, after _Emeric Deutsch_ *)",
				"primeMS[n_]:=If[n==1,{},Flatten[Cases[FactorInteger[n],{p_,k_}:\u003eTable[PrimePi[p],{k}]]]];",
				"ck[y_]:=With[{w=Count[y,1]},If[w==0,Max@@y,Count[y,_?(#\u003ew\u0026)]-w]];",
				"Table[ck[primeMS[n]],{n,2,30}] (* _Gus Wiseman_, Apr 05 2021 *)"
			],
			"xref": [
				"Cf. A215366, A257988.",
				"Indices of zeros are A342192.",
				"A001522 counts partitions of crank 0.",
				"A003242 counts anti-run compositions.",
				"A064391 counts partitions by crank.",
				"A064428 counts partitions of nonnegative crank.",
				"Cf. A000005, A000726, A056239, A112798, A124010, A224958, A325351, A325352."
			],
			"keyword": "sign",
			"offset": "2,2",
			"author": "_Emeric Deutsch_, May 18 2015",
			"references": 10,
			"revision": 27,
			"time": "2021-04-05T21:07:39-04:00",
			"created": "2015-05-18T21:47:16-04:00"
		}
	]
}