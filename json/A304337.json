{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A304337",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 304337,
			"data": "1,2,4,3,1,5,6,2,4,8,7,3,1,5,9,10,6,2,4,8,12,11,7,3,1,5,9,13,14,10,6,2,4,8,12,16,15,11,7,3,1,5,9,13,17,18,14,10,6,2,4,8,12,16,20,19,15,11,7,3,1,5,9,13,17,21,22,18,14,10,6,2,4,8,12,16,20,24,23,19,15,11,7,3,1",
			"name": "Lexicographically earliest fractal-like sequence such that the erasure of all pairs of contiguous terms of opposite parity leaves the sequence unchanged.",
			"comment": [
				"The sequence is fractal-like as it embeds an infinite number of copies of itself.",
				"The sequence was built according to these rules (see, in the Example section, the parenthesization technique):",
				"  1) no overlapping pairs of parentheses;",
				"  2) always start the content inside a pair of parentheses with the smallest integer X not yet present inside another pair of parentheses;",
				"  3) always end the content inside a pair of parentheses with the smallest integer Y not yet present inside another pair of parentheses such Y is not of the same parity as X;",
				"  4) after a(1) = 1 and a(2) = 2, always try to extend the sequence with a duplicate of the oldest term of the sequence not yet duplicated; if this leads to a contradiction, open a new pair of parentheses."
			],
			"link": [
				"Eric Angelini, \u003ca href=\"/A304337/b304337.txt\"\u003eTable of n, a(n) for n = 1..1954\u003c/a\u003e"
			],
			"example": [
				"Parentheses are added around each pair of terms of opposite parity:",
				"(1,2),(4,3),1,(5,6),2,4,(8,7),3,1,5,(9,10),6,2,4,8,(12,11),7,3,1,5,9,(13,14),10,6,2,4,8,12,(16,15),11,7,3,1,5,9,13,(17,18),14,10,6,",
				"Erasing all the parenthesized contents yields",
				"(...),(...),1,(...),2,4,(...),3,1,5,(....),6,2,4,8,(.....),7,3,1,5,9,(.....),10,6,2,4,8,12,(.....),11,7,3,1,5,9,13,(.....),14,10,6,",
				"We see that the remaining terms slowly rebuild the starting sequence."
			],
			"xref": [
				"Cf. A303845 (same idea, but pairs of contiguous terms are erased if a prime by concatenation arises), A303948 (if pair has at least one digit in common), A303953 (if pair sums up to a square)."
			],
			"keyword": "nonn,base",
			"offset": "1,2",
			"author": "_Eric Angelini_, May 11 2018",
			"references": 1,
			"revision": 8,
			"time": "2018-06-03T16:48:24-04:00",
			"created": "2018-06-03T16:48:24-04:00"
		}
	]
}