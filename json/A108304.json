{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A108304",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 108304,
			"data": "1,1,2,5,15,52,202,859,3930,19095,97566,520257,2877834,16434105,96505490,580864901,3573876308,22426075431,143242527870,929759705415,6123822269373,40877248201308,276229252359846,1887840181793185,13037523684646810,90913254352507057",
			"name": "Number of set partitions of {1, ..., n} that avoid 3-crossings.",
			"comment": [
				"There is also a sum-formula for a(n). See Bousquet-Mélou and Xin.",
				"Also partitions avoiding a certain pattern (see J. Bloom and S. Elizalde). - _N. J. A. Sloane_, Jan 02 2013"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A108304/b108304.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Jonathan Bloom and Sergi Elizalde, \u003ca href=\"http://arxiv.org/abs/1211.3442\"\u003ePattern avoidance in matchings and partitions\u003c/a\u003e, arXiv preprint arXiv:1211.3442 [math.CO], 2012.",
				"Alin Bostan, Jordan Tirrell, Bruce W. Westbury, Yi Zhang, \u003ca href=\"https://arxiv.org/abs/1911.10288\"\u003eOn sequences associated to the invariant theory of rank two simple Lie algebras\u003c/a\u003e, arXiv:1911.10288 [math.CO], 2019.",
				"Alin Bostan, Jordan Tirrell, Bruce W. Westbury and Yi Zhang, \u003ca href=\"https://arxiv.org/abs/2110.13753\"\u003eOn some combinatorial sequences associated to invariant theory\u003c/a\u003e, arXiv:2110.13753 [math.CO], 2021.",
				"M. Bousquet-Mélou and G. Xin, \u003ca href=\"https://arxiv.org/abs/math/0506551\"\u003eOn partitions avoiding 3-crossings\u003c/a\u003e, arXiv:math/0506551 [math.CO], 2005-2006.",
				"Sophie Burrill, Sergi Elizalde, Marni Mishna and Lily Yen, \u003ca href=\"http://arxiv.org/abs/1108.5615\"\u003eA generating tree approach to k-nonnesting partitions and permutations\u003c/a\u003e, arXiv preprint arXiv:1108.5615 [math.CO], 2011.",
				"Wei Chen, \u003ca href=\"http://summit.sfu.ca/item/14626\"\u003eEnumeration of Set Partitions Refined by Crossing and Nesting Numbers\u003c/a\u003e, MS Thesis, Department of Mathematics. Simon Fraser University, Fall 2014.",
				"W. Chen, E. Deng, R. Du, R. Stanley, and C. Yan, \u003ca href=\"https://arxiv.org/abs/math/0501230\"\u003eCrossings and nestings of matchings and partitions\u003c/a\u003e, arXiv:math/0501230 [math.CO], 2005.",
				"Andrew R. Conway, Miles Conway, Andrew Elvey Price and Anthony J. Guttmann, \u003ca href=\"https://arxiv.org/abs/2111.01279\"\u003ePattern-avoiding ascent sequences of length 3\u003c/a\u003e, arXiv:2111.01279 [math.CO], Nov 01 2021.",
				"P. Duncan and Einar Steingrimsson, \u003ca href=\"http://arxiv.org/abs/1109.3641\"\u003ePattern avoidance in ascent sequences\u003c/a\u003e, arXiv preprint arXiv:1109.3641 [math.CO], 2011.",
				"Juan B. Gil, Jordan O. Tirrell, \u003ca href=\"https://arxiv.org/abs/1806.09065\"\u003eA simple bijection for classical and enhanced k-noncrossing partitions\u003c/a\u003e, arXiv:1806.09065 [math.CO], 2018.",
				"Zhicong Lin, \u003ca href=\"https://arxiv.org/abs/1706.07213\"\u003eRestricted inversion sequences and enhanced 3-noncrossing partitions\u003c/a\u003e, arXiv:1706.07213 [math.CO], 2017.",
				"Zhicong Lin, Sherry H. F. Yan, \u003ca href=\"https://doi.org/10.1016/j.amc.2019.124672\"\u003eVincular patterns in inversion sequences\u003c/a\u003e, Applied Mathematics and Computation (2020), Vol. 364, 124672.",
				"T. Mansour and M. Shattuck, \u003ca href=\"http://arxiv.org/abs/1207.3755\"\u003eSome enumerative results related to ascent sequences\u003c/a\u003e, arXiv preprint arXiv:1207.3755 [math.CO], 2012. - _N. J. A. Sloane_, Dec 22 2012",
				"Marni Mishna and Lily Yen, \u003ca href=\"http://arxiv.org/abs/1106.5036\"\u003eSet partitions with no k-nesting\u003c/a\u003e, arXiv preprint arXiv:1106.5036 [math.CO], 2011."
			],
			"formula": [
				"Recurrence: (9*n^2+27*n) * a(n) + (-10*n^2-64*n-84) * a(n+1) + (n^2+13*n+42) * a(n+2) = 0.",
				"a(n) = (-18*(n+1)*(4*n^5+73*n^4+530*n^3+1928*n^2+3654*n+2916)*A002893(n)+(8*n^6+17156*n^2+6084*n^3+17496+27612*n+1358*n^4+162*n^5) *A002893(n+1))/ (3*n*(n+2)^2*(n+3)^2*(n+4)^2*(n+5)). - _Mark van Hoeij_, Nov 05 2011",
				"G.f.: (1+7*x-20*x^2+30*x^3-18*x^4-(3*x+1)^2*(x-1)^2*hypergeom([-2/3, -1/3],[2],27*x*(x-1)^2/(3*x+1)^3))/(6*x^4). - _Mark van Hoeij_, Nov 05 2011",
				"a(n) ~ 5 * sqrt(3) * 3^(2*n+9) / (32*Pi*n^7), Bousquet-Mélou and Xin, 2006. - _Vaclav Kotesovec_, Aug 23 2014"
			],
			"example": [
				"There are 203 partitions of 6 elements, but a(6)=202 because the partition (1,4)(2,5)(3,6) has a 3-crossing.",
				"G.f. = 1 + x + 2*x^2 + 5*x^3 + 15*x^4 + 52*x^5 + 202*x^6 + 859*x^7 + ..."
			],
			"mathematica": [
				"a[0] = a[1] = 1; a[n_] := a[n] = (2*(5*n^2 + 12*n - 2)*a[n-1] + 9*(-n^2 + n + 2)*a[n-2])/((n+4)*(n+5)); Table[a[n], {n, 0, 30}] (* _Jean-François Alcover_, Mar 30 2015 *)"
			],
			"program": [
				"(PARI)",
				"v = vector(66,n,n);",
				"for (n=1, #v-2, v[n+2] = ((10*n^2+64*n+84)*v[n+1]-(9*n^2+27*n)*v[n]) / (n^2+13*n+42) );",
				"vector(#v+1,n, if(n==1,1,v[n-1])) \\\\ _Joerg Arndt_, Sep 01 2012"
			],
			"keyword": "easy,nonn",
			"offset": "0,3",
			"author": "_Mireille Bousquet-Mélou_, Jun 29 2005",
			"ext": [
				"More terms added by _Joerg Arndt_, Sep 01 2012"
			],
			"references": 8,
			"revision": 81,
			"time": "2021-11-04T04:09:26-04:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}