{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A051301",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 51301,
			"data": "2,2,3,7,5,11,7,71,61,19,11,39916801,13,83,23,59,17,661,19,71,20639383,43,23,47,811,401,1697,10888869450418352160768000001,29,14557,31,257,2281,67,67411,137,37,13763753091226345046315979581580902400000001",
			"name": "Smallest prime factor of n!+1.",
			"comment": [
				"Theorem: For any N, there is a prime \u003e N. Proof: Consider any prime factor of N! + 1.",
				"Cf. Wilson's Theorem (1770): p | (p-1)! + 1 if and only if p is a prime.",
				"If n is in A002981, then a(n) = n!+1. - _Chai Wah Wu_, Jul 15 2019"
			],
			"reference": [
				"Albert H. Beiler, \"Recreations in The Theory of Numbers, The Queen of Mathematics Entertains,\" Dover Publ. NY, 1966, Page 49.",
				"M. Kraitchik, On the divisibility of factorials, Scripta Math., 14 (1948), 24-26 (but beware errors)."
			],
			"link": [
				"Chai Wah Wu, \u003ca href=\"/A051301/b051301.txt\"\u003eTable of n, a(n) for n = 0..138\u003c/a\u003e n = 0..100 derived from Hisanori Mishima's data by T. D. Noe.",
				"A. Borning, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1972-0308018-5 \"\u003eSome results for k!+-1 and 2.3.5...p+-1\u003c/a\u003e, Math. Comp., 26 (1972), 567-570.",
				"P. Erdős and C. L. Stewart, \u003ca href=\"http://www.renyi.hu/~p_erdos/1976-27.pdf\"\u003eOn the greatest and least prime factors of n! + 1\u003c/a\u003e, J. London Math. Soc. (2) 13:3 (1976), pp. 513-519.",
				"M. Kraitchik, \u003ca href=\"/A002582/a002582.pdf\"\u003eOn the divisibility of factorials\u003c/a\u003e, Scripta Math., 14 (1948), 24-26 (but beware errors). [Annotated scanned copy]",
				"Hisanori Mishima, \u003ca href=\"http://www.asahi-net.or.jp/~KC2H-MSM/mathland/matha1/matha103.htm\"\u003eFactorizations of many number sequences\u003c/a\u003e",
				"Hisanori Mishima, \u003ca href=\"http://www.asahi-net.or.jp/~KC2H-MSM/mathland/matha1/matha104.htm\"\u003eFactorizations of many number sequences\u003c/a\u003e",
				"R. G. Wilson v, \u003ca href=\"/A038507/a038507.txt\"\u003eExplicit factorizations\u003c/a\u003e"
			],
			"formula": [
				"Erdős \u0026 Stewart show that a(n) \u003e n + (l-o(l))log n/log log n except when n + 1 is prime, and that a(n) \u003e n + e(n)sqrt(n) for almost all n where e(n) is any function with lim e(n) = 0. - _Charles R Greathouse IV_, Dec 05 2012",
				"By Wilson's theorem, a(n) \u003e=  n + 1 with equality if and only if n + 1 is prime. - _Chai Wah Wu_, Jul 14 2019"
			],
			"example": [
				"a(3) = 7 because 3! + 1 = 7.",
				"a(4) = 5 because 4! + 1 = 25 = 5^2. (5! + 1 is also the square of a prime).",
				"a(6) = 7 because 6! + 1 = 721 = 7 * 103."
			],
			"maple": [
				"with(numtheory): A051301 := n -\u003e sort(convert(divisors(n!+1),list))[2]; # Corrected by _Peter Luschny_, Jul 17 2009"
			],
			"mathematica": [
				"Do[ Print[ FactorInteger[ n! + 1, FactorComplete -\u003e True ] [ [ 1, 1 ] ] ], {n, 0, 38} ]",
				"FactorInteger[#][[1,1]]\u0026/@(Range[0,40]!+1) (* _Harvey P. Dale_, Oct 16 2021 *)"
			],
			"program": [
				"(PARI) a(n)=factor(n!+1)[1,1] \\\\ _Charles R Greathouse IV_, Dec 05 2012"
			],
			"xref": [
				"Cf. A002583, A002981, A038507, A096225."
			],
			"keyword": "nonn",
			"offset": "0,1",
			"author": "_Labos Elemer_",
			"references": 12,
			"revision": 36,
			"time": "2021-10-16T19:46:08-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}