{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A143488",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 143488,
			"data": "1,1,1,2,2,1,1,1,2,2,3,3,4,4,4,3,3,4,4,4,3,3,2,2,1,1,1,2,2,1,1,1,2,2,3,3,4,4,4,3,3,4,4,4,3,3,2,2,1,1,1,2,2,1,1,1,2,2,3,3,4,4,4,3,3,4,4,4,3,3,2,2,1,1,1,2,2,1,1,1,2,2,3,3,4,4,4,3,3,4,4,4,3,3,2,2,1,1,1,2,2,1,1,1,2",
			"name": "\"Fourth down, Extream [sic] between the two farthest Bells from it\" in bell-ringing is a sequence of permutations p_1=(1,2,3,4), p_2=(1,2,4,3), .. which runs through all permutations of {1,2,3,4} with period 24; sequence gives position of bell 1 (the treble bell) in n-th permutation.",
			"comment": [
				"Start with (1,2,3,4), i.e. the first permutation of {1,2,3} followed by 4; then for each next permutation, transpose 4 one to the left; if at position 1, replace {1,2,3} recursively by the next permutation of these numbers. Thereafter, for each next permutation, transpose 4 to the right. And so on."
			],
			"link": [
				"Richard Duckworth and Fabian Stedman, \u003ca href=\"http://www.gutenberg.org/files/18567/18567-h/18567-h.htm\"\u003eTintinnalogia, or, the Art of Ringing\u003c/a\u003e, Project Gutenberg.",
				"\u003ca href=\"/index/Be#bell_ringing\"\u003eIndex entries for sequences related to bell ringing\u003c/a\u003e"
			],
			"formula": [
				"Period 24.",
				"From _Chai Wah Wu_, Jan 15 2020: (Start)",
				"a(n) = a(n-1) - a(n-12) + a(n-13) for n \u003e 13.",
				"G.f.: x*(-2*x^12 - x^10 - x^8 + x^5 - x^3 - 1)/(x^13 - x^12 + x - 1). (End)"
			],
			"example": [
				"The full list of the 24 permutations is as follows (the present sequence gives position of bell 1):",
				"1 2 3 4",
				"1 2 4 3",
				"1 4 2 3",
				"4 1 2 3",
				"4 1 3 2",
				"1 4 3 2",
				"1 3 4 2",
				"1 3 2 4",
				"3 1 2 4",
				"3 1 4 2",
				"3 4 1 2",
				"4 3 1 2",
				"4 3 2 1",
				"3 4 2 1",
				"3 2 4 1",
				"3 2 1 4",
				"2 3 1 4",
				"2 3 4 1",
				"2 4 3 1",
				"4 2 3 1",
				"4 2 1 3",
				"2 4 1 3",
				"2 1 4 3",
				"2 1 3 4"
			],
			"maple": [
				"ring:= proc(k::nonnegint) local p,i,left,l,nf, ini; if k\u003c=1 then proc() [1$k] end else ini:= proc() p:= ring(k-1); i:= k; left:= true; l:= p(); nf:= k! end; ini(); proc() local ll; ll:= [seq(l[t], t=1..(i-1)), k, seq(l[t], t=i..(k-1))]; if left then if i\u003e1 then i:= i-1 else left:= false; l:=p() fi else if i\u003ck then i:= i+1 else left:= true; l:=p() fi fi; nf:= nf-1; if nf = 0 then ini() fi; ll end fi end: bell := proc(k) option remember; local p; p:= ring(k); [seq(p(), i=1..k!)] end: indx:= proc(l, k) local i; for i from 1 to nops(l) do if l[i]=k then break fi od; i end: a:= n-\u003e indx(bell(4)[modp(n-1,24)+1], 1): seq(a(n), n=1..121);"
			],
			"mathematica": [
				"LinearRecurrence[",
				"     {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 1},",
				"     {1, 1, 1, 2, 2, 1, 1, 1, 2, 2, 3, 3, 4, 4, 4, 3, 3, 4, 4, 4, 3}, 105]",
				"(* _Jean-François Alcover_, Mar 15 2021 *)"
			],
			"xref": [
				"Cf. A143484, A143485, A143486, A143487, A143489, A143490, A090281."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Alois P. Heinz_, Aug 19 2008",
			"references": 4,
			"revision": 17,
			"time": "2021-03-15T08:27:54-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}