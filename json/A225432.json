{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A225432",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 225432,
			"data": "1,1,2,1,2,10,1,5,250,106,1138,2,25,146,298,5,17,1,97,253970,2,226,3034,9148450,2050,10,157,126890,1,14341370,5,110671282,986,7586,530,130,173,5129602",
			"name": "Twice the coefficient of sqrt(q) in e^h, where e is the fundamental unit and h is the class number of Q(sqrt(q)), q prime and congruent to 1 mod 4. (The coefficient lies in (1/2)Z, so twice it is an integer.)",
			"comment": [
				"This also arises in the relation satisfied by Euler classes in the connective K-theory of the classifying space of the group of order pq, where p=(q-1)/2. See p. 39 in Bruner and Greenlees, cited below. Take an irreducible representation of the cyclic group of order q which generates the representations as a ring, induce it up to the group of order pq, and let z be its Euler class in ku^{2p}(BG_{pq}). Then z satisfies the relation z^3 -2bq z^2 + qz = 0. This follows from the arithmetic fact that in Q(sort(q)) we have the relation e^h = a + b sqrt(q), as shown on pp. 39-42 of Bruner and Greenlees.",
				"This is closely related to the subsequence of A078357 containing those entries such that the corresponding entry in A077426 is prime. However, a(22) = 226 (corresponding to e^3 = 1710 + 113*sqrt(229)) does not occur in A078357, and more such terms appear after this."
			],
			"reference": [
				"R. R. Bruner and J. P. C. Greenlees, The Connective K-theory of Finite Groups, Memoirs AMS, Vol. 165, No. 785, 2003."
			],
			"link": [
				"R. R. Bruner and J. P. C. Greenlees, \u003ca href=\"https://www.semanticscholar.org/paper/The-Connective-K-Theory-of-Finite-Groups-Bruner-Greenlees/ad7f06aae2f629052f1472f94959ba941a4835c6\"\u003eThe Connective K-theory of Finite Groups\u003c/a\u003e, Semantic Scholar."
			],
			"program": [
				"# MAGMA code to generate all terms for which the prime q is less than or equal to 4N+1 (an initial segment of the sequence). (Note that the brute force computation of the fundamental unit is very inefficient, and will have trouble producing the 39th term.)",
				"pr := [4*n+1 : n in [1..N] | IsPrime(4*n+1)];",
				"for i in [1..#pr] do",
				"q := pr[i];",
				"Q\u003cs\u003e := QuadraticField(q);",
				"h := ClassNumber(Q);",
				"x := 1;",
				"while not IsSquare(x*x*q-4) do",
				"x := x+1;",
				"end while;",
				"x := x/2;",
				"tr,y := IsSquare(x*x*q-1);",
				"e := y + x*s;",
				"eh := e^h;",
				"b := (eh-Trace(eh)/2)/s;",
				"print i,2*b;",
				"end for;"
			],
			"xref": [
				"Cf. A077426, A078357."
			],
			"keyword": "nonn,more",
			"offset": "1,3",
			"author": "_Robert R. Bruner_, May 07 2013",
			"references": 0,
			"revision": 21,
			"time": "2020-06-23T06:17:54-04:00",
			"created": "2013-05-07T16:23:28-04:00"
		}
	]
}