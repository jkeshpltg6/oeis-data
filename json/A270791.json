{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A270791",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 270791,
			"data": "1,1,1,158,558,135,2339,18378,13689,1575,1354,18908,28764,9660,675,617926,13447818,34604118,23001156,4534875,218295,525206428,16383145284,63886133214,70424606988,26926791930,3567422250,127702575,50531787,2134308548,11735772822,19350632598,12106771137,3063221550,295973325,8292375",
			"name": "Triangle read by rows: coefficients of polynomials P_n(x) arising from RNA combinatorics.",
			"comment": [
				"\"... polynomials like these with nonnegative integral coefficients might reasonably be expected to be generating polynomials for some as yet unknown fatgraph structure.\""
			],
			"link": [
				"Gheorghe Coserea, \u003ca href=\"/A270791/b270791.txt\"\u003eRows n = 1..100, flattened\u003c/a\u003e",
				"J. E. Andersen, R. C. Penner, C. M. Reidys, M. S. Waterman, \u003ca href=\"https://doi.org/10.1007/s00285-012-0594-x\"\u003eTopological classification and enumeration of RNA structures by genus\u003c/a\u003e, J. Math. Biol. 65 (2013) 1261-1278",
				"R. C. Penner, \u003ca href=\"http://dx.doi.org/10.1090/bull/1524\"\u003eModuli Spaces and Macromolecules\u003c/a\u003e, Bull. Amer. Math. Soc., 53 (2015), 217-268. See p. 259."
			],
			"formula": [
				"The g.f. for column g\u003e0 of triangle A035309 is x^(2*g) * A270790(g) * P_g(x) / (1-4*x)^(3*g-1/2), where P_g(x) is the polynomial associated with row g of the triangle. - _Gheorghe Coserea_, Apr 17 2016"
			],
			"example": [
				"For n = 3 we have P_3(x) = 158*x^2 + 558*x + 135.",
				"For n = 4 we have P_4(x) = 2339*x^3 + 18378*x^2 + 13689*x + 1575.",
				"Triangle begins:",
				"n\\k  [1]        [2]        [3]        [4]        [5]        [6]",
				"[1]  1;",
				"[2]  1,         1;",
				"[3]  158        558,       135;",
				"[4]  2339,      18378,     13689,     1575;",
				"[5]  1354,      18908,     28764,     9660,      675;",
				"[6]  617926,    13447818,  34604118,  23001156,  4534875,   218295;",
				"[7]  ..."
			],
			"program": [
				"(PARI)",
				"G = 8; N = 3*G + 1; F = 1; gmax(n) = min(n\\2, G);",
				"Q = matrix(N+1, G+1); Qn() = (matsize(Q)[1] - 1);",
				"Qget(n, g) = { if (g \u003c 0 || g \u003e n/2, 0, Q[n+1, g+1]) };",
				"Qset(n, g, v) = { Q[n+1, g+1] = v };",
				"Quadric({x=1}) = {",
				"  Qset(0, 0, x);",
				"  for (n = 1, Qn(), for (g = 0, gmax(n),",
				"    my(t1 = (1+x)*(2*n-1)/3 * Qget(n-1, g),",
				"       t2 = (2*n-3)*(2*n-2)*(2*n-1)/12 * Qget(n-2, g-1),",
				"       t3 = 1/2 * sum(k = 1, n-1, sum(i = 0, g,",
				"       (2*k-1) * (2*(n-k)-1) * Qget(k-1, i) * Qget(n-k-1, g-i))));",
				"    Qset(n, g, (t1 + t2 + t3) * 6/(n+1))));",
				"};",
				"Quadric('x + O('x^(F+1)));",
				"Kol(g) = vector(Qn()+2-F-2*g, n, polcoeff(Qget(n+F-2 + 2*g, g), F, 'x));",
				"P(g) = {",
				"  my(x = 'x + O('x^(G+2)));",
				"  return(Pol(Ser(Kol(g)) * (1-4*x)^(3*g-1/2), 'x));",
				"};",
				"concat(vector(G, g, Vec(P(g) / content(P(g)))))  \\\\ _Gheorghe Coserea_, Apr 17 2016"
			],
			"xref": [
				"Cf. A035309, A035319, A270790."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "1,4",
			"author": "_N. J. A. Sloane_, Mar 28 2016",
			"ext": [
				"More terms from _Gheorghe Coserea_, Apr 17 2016"
			],
			"references": 3,
			"revision": 51,
			"time": "2018-12-24T21:25:45-05:00",
			"created": "2016-03-28T09:29:05-04:00"
		}
	]
}