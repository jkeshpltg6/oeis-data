{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A338571",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 338571,
			"data": "1,8,96,2520,552600",
			"name": "Number of polyhedra formed when the five Platonic solids, in the order tetrahedron, octahedron, cube, icosahedron, dodecahedron, are internally cut by all the planes defined by any three of their vertices.",
			"comment": [
				"For a Platonic solid create all possible planes defined by connecting any three of its vertices. For example, in the case of a cube this results in fourteen planes; six planes between the pairs of parallel edges connected to each end of the face diagonals, and eight planes from connecting the three vertices adjacent to each corner vertex. Use all the resulting planes to cut the solid into individual smaller polyhedra. The sequence lists the numbers of resulting polyhedra for the Platonic solids, ordered by number of vertices: tetrahedron, octahedron, cube, icosahedron, dodecahedron.",
				"See A338622 for the number and images of the k-faced polyhedra in each dissection for each of the five solids.",
				"The author thanks _Zach J. Shannon_ for producing the images for this sequence."
			],
			"link": [
				"Hyung Taek Ahn and Mikhail Shashkov, \u003ca href=\"https://cnls.lanl.gov/~shashkov/papers/ahn_geometry.pdf\"\u003eGeometric Algorithms for 3D Interface Reconstruction\u003c/a\u003e.",
				"Polyhedra.mathmos.net, \u003ca href=\"http://www.srcf.ucam.org/~rjw62/polyhedra/entry/platonicsolids.html\"\u003eThe Platonic Solids\u003c/a\u003e.",
				"Scott R. Shannon, \u003ca href=\"/A338571/a338571_6.png\"\u003eTetrahedron, showing the one polyhedra pre and post-cutting\u003c/a\u003e. The tetrahedron has no internal cutting planes so remains unaltered.",
				"Scott R. Shannon, \u003ca href=\"/A338571/a338571_2.png\"\u003eOctahedron, showing the 8 polyhedra post-cutting\u003c/a\u003e.  All pieces have 4 faces. The plane cuts are along the edges of the octahedron and thus only 3 internal cutting planes exist, each along the three 2D axial planes.",
				"Scott R. Shannon, \u003ca href=\"/A338571/a338571.png\"\u003eCube, showing the 14 plane cuts on the external edges and faces\u003c/a\u003e.",
				"Scott R. Shannon, \u003ca href=\"/A338571/a338571_1.png\"\u003eCube, showing the 96 polyhedra post-cutting and exploded\u003c/a\u003e. Each piece has been moved away from the origin by a distance proportional to the average distance of its vertices from the origin. Red shows the 4-faced polyhedra, orange the 5-faced polyhedra. The later form a perfect octahedron inside the original cube, the points of which touch the cube's surface. See A338622.",
				"Scott R. Shannon, \u003ca href=\"/A338571/a338571_3.png\"\u003eIcosahedron, showing the 47 plane cuts on the external edges and faces\u003c/a\u003e.",
				"Scott R. Shannon, \u003ca href=\"/A338571/a338571_5.png\"\u003eIcosahedron, showing the 2520 polyhedra post-cutting\u003c/a\u003e. Red shows the 4-faced polyhedra, orange the 5-faced polyhedra.",
				"Scott R. Shannon, \u003ca href=\"/A338571/a338571_4.png\"\u003eIcosahedron, showing the 2520 polyhedra post-cutting and exploded\u003c/a\u003e. Red shows the 4-faced polyhedra, orange the 5-faced polyhedra.",
				"Scott R. Shannon, \u003ca href=\"/A338571/a338571_7.png\"\u003eDodecahedron, showing the 307 plane cuts on the external edges and faces\u003c/a\u003e.",
				"Scott R. Shannon, \u003ca href=\"/A338571/a338571_8.png\"\u003eDodecahedron, showing the 552600 polyhedra post-cutting\u003c/a\u003e. The 4,5,6,7,8 faced polyhedra are colored red, orange, yellow, green and blue respectively. The 9 and 10 faced polyhedra are all internal.",
				"Scott R. Shannon, \u003ca href=\"/A338571/a338571_9.png\"\u003eDodecahedron, showing the 552600 polyhedra post-cutting and exploded\u003c/a\u003e. Zooming in shows the vast array of polyhedra.",
				"Scott R. Shannon, \u003ca href=\"/A338571/a338571_10.png\"\u003eDodecahedron, close-up of the post-cutting and exploded image\u003c/a\u003e.",
				"Zach J. Shannon, \u003ca href=\"https://www.youtube.com/watch?v=bqtkif44ULQ\"\u003eAnimation showing the 96 polyhedra for the cube and the 2520 polyhedra for the icosahedron\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PlatonicSolid.html\"\u003ePlatonic Solid\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Platonic_solid\"\u003ePlatonic solid\u003c/a\u003e."
			],
			"example": [
				"a(1) = 1. The tetrahedron has no internal cutting planes so the single polyhedron after cutting is the tetrahedron itself.",
				"a(2) = 8. The octahedron has 3 internal cutting planes resulting in 8 polyhedra.",
				"a(3) = 96. The cube has 14 internal cutting planes resulting in 96 polyhedra. See also A333539.",
				"a(4) = 2520. The icosahedron has 47 cutting planes resulting in 2520 polyhedra.",
				"See A338622 for a breakdown of the above totals into the corresponding number of k-faced polyhedra.",
				"a(5) = 552600. The dodecahedron has 307 internal cutting planes resulting in 552600 polyhedra. It is the only Platonic solid which produces polyhedra with 6 or more faces."
			],
			"xref": [
				"Cf. A338622 (number of k-faced polyhedra in each dissection), A333539 (n-dimensional cube), A053016, A063722, A063723, A098427."
			],
			"keyword": "nonn,fini,full",
			"offset": "1,2",
			"author": "_Scott R. Shannon_, Nov 03 2020",
			"references": 8,
			"revision": 54,
			"time": "2021-07-27T01:39:12-04:00",
			"created": "2020-11-05T15:49:03-05:00"
		}
	]
}