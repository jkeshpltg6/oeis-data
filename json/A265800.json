{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A265800",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 265800,
			"data": "5,3,5,11,31,37,47,157,571,911,1021,1487,2351,3571,24709,25463,69247,80803",
			"name": "Numerators of primes-only best approximates (POBAs) to the golden ratio, tau; see Comments.",
			"comment": [
				"Suppose that x \u003e 0. A fraction p/q of primes is a primes-only best approximate (POBA), and we write \"p/q in B(x)\", if 0 \u003c |x - p/q| \u003c |x - u/v| for all primes u and v such that v \u003c q, and also, |x - p/q| \u003c |x - p'/q| for every prime p' except p. Note that for some choices of x, there are values of q for which there are two POBAs. In these cases, the greater is placed first; e.g., B(3) = (7/2, 5/2, 17/5, 13/5, 23/7, 19/7, ...). See A265759 for a guide to related sequences.",
				"How is this related to A165572? - _R. J. Mathar_, Jan 10 2016"
			],
			"example": [
				"The POBAs to tau start with 5/2, 3/2, 5/3, 11/7, 31/19, 37/23, 47/29, 157/97, 571/353, 911/563. For example, if p and q are primes and q \u003e 29, then 47/29 is closer to tau than p/q is."
			],
			"mathematica": [
				"x = GoldenRatio; z = 1000; p[k_] := p[k] = Prime[k];",
				"t = Table[Max[Table[NextPrime[x*p[k], -1]/p[k], {k, 1, n}]], {n, 1, z}];",
				"d = DeleteDuplicates[t]; tL = Select[d, # \u003e 0 \u0026] (* lower POBA *)",
				"t = Table[Min[Table[NextPrime[x*p[k]]/p[k], {k, 1, n}]], {n, 1, z}];",
				"d = DeleteDuplicates[t]; tU = Select[d, # \u003e 0 \u0026] (* upper POBA *)",
				"v = Sort[Union[tL, tU], Abs[#1 - x] \u003e Abs[#2 - x] \u0026];",
				"b = Denominator[v]; s = Select[Range[Length[b]], b[[#]] == Min[Drop[b, # - 1]] \u0026];",
				"y = Table[v[[s[[n]]]], {n, 1, Length[s]}] (* POBA, A265800/A265801 *)",
				"Numerator[tL]   (* A265796 *)",
				"Denominator[tL] (* A265797 *)",
				"Numerator[tU]   (* A265798 *)",
				"Denominator[tU] (* A265799 *)",
				"Numerator[y]    (* A265800 *)",
				"Denominator[y]  (* A265801 *)"
			],
			"xref": [
				"Cf. A000040, A265759, A265796, A265797, A265798, A265799, A265801."
			],
			"keyword": "nonn,frac,more",
			"offset": "1,1",
			"author": "_Clark Kimberling_, Dec 29 2015",
			"ext": [
				"a(15)-a(18) from _Robert Price_, Apr 06 2019"
			],
			"references": 8,
			"revision": 14,
			"time": "2019-04-07T00:02:11-04:00",
			"created": "2016-01-02T04:35:46-05:00"
		}
	]
}