{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A160498",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 160498,
			"data": "1,0,0,0,0,0,2,0,2,0,0,0,2,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,2,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,4,0,0,0,2,0,0,0,0,0,2,0,0,0,0,0,2,0,0",
			"name": "Number of cubic primitive Dirichlet characters modulo n.",
			"comment": [
				"Also called primitive Dirichlet characters of order 3.",
				"Mobius transform of A060839.",
				"C. David, J. Fearnley \u0026 H. Kisilevsky prove that Sum_{k=1..n} a(k) ~ C*n, with C = (11*sqrt(3)/(18*Pi)) * Product_{primes p == 1 (mod 3)} (1 - 2/(p*(p+1))) = 0.3170565167922841205670156...; they credit Cohen, F. Diaz y Diaz, \u0026 M. Olivier 2002 (see Proposition 5.2. and Corollary 5.3.). - _Charles R Greathouse IV_, Aug 26 2009 [corrected by _Vaclav Kotesovec_, Sep 16 2020]",
				"a(n) is the number of primitive Dirichlet characters modulo n such that all entries are 0 or a cubic root of unity: 1, w = (-1 + sqrt(3)*i)/2 or w^2 = (-1 - sqrt(3)*i)/2). - _Jianing Song_, Feb 27 2019",
				"Every term is 0 or a power of 2. - _Jianing Song_, Mar 02 2019",
				"From _Jianing Song_, Apr 03 2021: (Start)",
				"For n \u003e= 2, a(n) is the number of cyclic cubic fields with discriminant n^2. See A343023 for detailed information.",
				"The first occurrence of 2^t is 9*A121940(t-1) for t \u003e= 2. (End)"
			],
			"link": [
				"Jianing Song, \u003ca href=\"/A160498/b160498.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"C. David, J. Fearnley and H. Kisilevsky,\u003ca href=\"http://www.emis.de/journals/EM/expmath/volumes/13/13.html\"\u003eOn the vanishing of twisted L-functions of elliptic curves\u003c/a\u003e, Experim. Math. 13 (2004) 185-198.",
				"Steven R. Finch, \u003ca href=\"https://web.archive.org/web/20160511040501/http://www.people.fas.harvard.edu/~sfinch/csolve/char.pdf\"\u003eCubic and quartic characters\u003c/a\u003e.",
				"Steven R. Finch, \u003ca href=\"http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.638.5547\"\u003eCubic and quartic characters\u003c/a\u003e.",
				"Steven R. Finch, \u003ca href=\"https://arxiv.org/abs/0907.4894\"\u003eQuartic and Octic Characters Modulo n\u003c/a\u003e, arXiv:0907.4894 [math.NT], 2016.",
				"Vaclav Kotesovec, \u003ca href=\"/A160498/a160498.jpg\"\u003ePlot of Sum_{k=1..n} a(k) / n for n = 1..10000000\u003c/a\u003e"
			],
			"formula": [
				"Multiplicative with a(p^e) = 2 if p^e = 9 or p == 1 (mod 3) and e = 1, otherwise 0. - _Jianing Song_, Mar 02 2019",
				"a(n) = 2*A343023(n) for n \u003e= 2. - _Jianing Song_, Apr 03 2021"
			],
			"example": [
				"From _Jianing Song_, Mar 02 2019: (Start)",
				"Let w = (-1 + sqrt(3)*i)/2 be one of the primitive 3rd root of unity.",
				"For n = 7, the 2 cubic primitive Dirichlet characters modulo n are [0, 1, w, w^2, w^2, w, 1] and [0, 1, w^2, w, w, w^2, 1], so a(7) = 2.",
				"For n = 9, the 2 cubic primitive Dirichlet characters modulo n are [0, 1, w, 0, w^2, w^2, 0, w, 1] and [0, 1, w^2, 0, w, w, 0, w^2, 1], so a(9) = 2. (End)"
			],
			"mathematica": [
				"A060839[n_] := Sum[If[Mod[k^3 - 1, n] == 0, 1, 0], {k, 1, n}]; a[n_] := Sum[ MoebiusMu[n/d]*A060839[d], {d, Divisors[n]}]; Table[a[n], {n, 2, 81}] (* _Jean-François Alcover_, Jun 19 2013 *)",
				"f[3, 2] = 2; f[p_, e_] := If[Mod[p, 3] == 1 \u0026\u0026 e == 1, 2, 0]; a[1] = 1; a[n_] := Times @@ f @@@ FactorInteger[n]; Array[a, 100] (* _Amiram Eldar_, Sep 16 2020 *)"
			],
			"program": [
				"(PARI) a(n)=sum(d=1, n, if(n%d==0, moebius(n/d)*sum(i=1, d, if((i^3-1)%d, 0, 1)), 0)) \\\\ _Steven Finch_, Jun 09 2009",
				"(PARI) A005088(n)=my(f=factor(n)[,1]); sum(i=1,#f,f[i]%3==1)",
				"A060839(n)=3^((n%9==0)+A005088(n))",
				"a(n)=sumdiv(n,d,moebius(n/d)*A060839(d)) \\\\ _Charles R Greathouse IV_, Aug 26 2009",
				"(PARI) a(n) = my(L=factor(n), w=omega(n)); for(i=1, w, if(!((L[i, 1]%3==1 \u0026\u0026 L[i, 2]==1) || L[i, 1]^L[i, 2] == 9), return(0))); 2^w \\\\ _Jianing Song_, Apr 03 2021"
			],
			"xref": [
				"Cf. A114643 (number of quadratic primitive Dirichlet characters modulo n), A160499 (number of quartic primitive Dirichlet characters modulo n).",
				"Cf. A060839 (number of solutions to x^3 == 1 (mod n)).",
				"Cf. A121940, A343023."
			],
			"keyword": "mult,nonn",
			"offset": "1,7",
			"author": "_Steven Finch_, May 15 2009",
			"ext": [
				"a(1) = 1 prepended by _Jianing Song_, Feb 27 2019"
			],
			"references": 15,
			"revision": 58,
			"time": "2021-04-04T00:57:16-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}