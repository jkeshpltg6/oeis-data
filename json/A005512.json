{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005512",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5512,
			"id": "M3261",
			"data": "1,1,0,4,5,96,427,6448,56961,892720,11905091,211153944,3692964145,75701219608,1613086090995,38084386700896,949168254452993,25524123909350112,725717102391257347,21955114496683796680",
			"name": "Number of series-reduced labeled trees with n nodes.",
			"reference": [
				"F. Bergeron, G. Labelle and P. Leroux, Combinatorial Species and Tree-Like Structures, Cambridge, 1998, p. 188 (3.1.94)",
				"F. Harary and E. M. Palmer, Graphical Enumeration. New York: Academic Press, 1973. (gives g.f. for unlabeled series-reduced trees)",
				"R. C. Read, personal communication.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A005512/b005512.txt\"\u003eTable of n, a(n) for n=1..100\u003c/a\u003e",
				"David Callan, \u003ca href=\"http://arxiv.org/abs/1406.7784\"\u003eA sign-reversing involution to count labeled lone-child-avoiding trees\u003c/a\u003e, arXiv:1406.7784 [math.CO], (30-June-2014)",
				"D. M. Jackson, \u003ca href=\"/A005512/a005512.pdf\"\u003eLetter to N. J. A. Sloane, May 1975\u003c/a\u003e",
				"P. Leroux and B. Miloudi, \u003ca href=\"http://www.labmath.uqam.ca/~annales/volumes/16-1/PDF/053-080.pdf\"\u003eGénéralisations de la formule d'Otter\u003c/a\u003e, Ann. Sci. Math. Quebec 16 (1992), no 1, 53-80.",
				"P. Leroux and B. Miloudi, \u003ca href=\"/A000081/a000081_2.pdf\"\u003eGénéralisations de la formule d'Otter\u003c/a\u003e, Ann. Sci. Math. Québec, Vol. 16, No. 1, pp. 53-80, 1992. (Annotated scanned copy)",
				"A. Meir and J. W. Moon, \u003ca href=\"http://dx.doi.org/10.1112/S0025579300002552\"\u003eOn nodes of degree two in random trees\u003c/a\u003e, Mathematika 15 1968 188-192.",
				"R. C. Read, \u003ca href=\"http://dx.doi.org/10.1111/j.1749-6632.1970.tb56486.x\"\u003eSome Unusual Enumeration Problems\u003c/a\u003e, Annals of the New York Academy of Sciences, Vol. 175, 1970, 314-326.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Series-ReducedTree.html\"\u003eSeries-reduced Tree\u003c/a\u003e",
				"\u003ca href=\"/index/Tra#trees\"\u003eIndex entries for sequences related to trees\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A060313(n)/n.",
				"a(n) = Sum_{k=0..n-2} (-1)^k*(n-k)^(n-k-2)*binomial(n, k)*(n-2)!/(n-k-2)!, n\u003e=2.",
				"E.g.f.: (1+x)*B(x)*(1-B(x)/2), where B(x) is e.g.f. for A060356. - _Vladeta Jovovic_, Dec 17 2004",
				"a(n) ~ (1-exp(-1))^(n+1/2)*n^(n-2). - _Vaclav Kotesovec_, Aug 07 2013"
			],
			"example": [
				"a(6) = 96 because there are two unlabeled series-reduced trees on six vertices, the star and the tree with two vertices of degree three and four leaves; the first of these can be labeled in 6 ways and the second in 90, for a total of 96. - Isabel C. Lugo (izzycat(AT)gmail.com), Aug 19 2004"
			],
			"maple": [
				"A005512 := proc(n)",
				"    if n = 1 then",
				"        1;",
				"    else",
				"        add( (-1)^(n-r)*binomial(n,r)*r^(r-2)/(r-2)!,r=2..n) ;",
				"        %*(n-2)! ;",
				"    end if;",
				"end proc: # _R. J. Mathar_, Sep 09 2014"
			],
			"mathematica": [
				"a[1] = a[2] = 1; a[3] = 0; a[n_] := n!*(n-2)!*Sum[ (-1)^k*(n-k)^(n-k-3) / (k!*(n-k-2)!^2*(n-k-1)), {k, 0, n-2}]; Table[a[n], {n, 1, 20}](* _Jean-François Alcover_, Feb 16 2012, after given formula *)",
				"u[1, 1] = 1; u[2, 1] = 0; u[2, 2] = 1; u[3, k_] := 0;",
				"u[n_, k_] /; k \u003c= 0 := 0;",
				"u[n_, k_] /; k \u003e= 1 :=",
				"u[n, k] = (n (n - k) u[n - 1, k - 1] + n (n - 1) (n - 3) u[n - 2, k - 1])/k;",
				"Table[Sum[u[n, m], {m, 1, n}], {n, 50}] (* _David Callan_, Jun 25 2014, fast generation, after R. C. Read link *)"
			],
			"program": [
				"(PARI) a(n) = if(n\u003c=1, n==1, sum(k=0, n-2, (-1)^k*(n-k)^(n-k-2)*binomial(n, k)*(n-2)!/(n-k-2)!)) \\\\ _Andrew Howroyd_, Dec 18 2017",
				"(MAGMA) [1] cat [Factorial(n-2)*(\u0026+[(-1)^k*Binomial(n,k)*(n-k)^(n-k-2)/Factorial(n-k-2): k in [0..n-2]]): n in [2..20]]",
				"(Sage) [1]+[factorial(n-2)*sum((-1)^k*binomial(n,k)*(n-k)^(n-k-2)/factorial( n-k-2) for k in (0..n-2)) for n in (2..20)] # _G. C. Greubel_, Mar 07 2020"
			],
			"xref": [
				"Cf. A000014 (unlabeled analog), A060313."
			],
			"keyword": "nonn,nice",
			"offset": "1,4",
			"author": "_N. J. A. Sloane_, _Simon Plouffe_",
			"ext": [
				"Formula from _Christian G. Bower_, Jan 16 2004"
			],
			"references": 9,
			"revision": 73,
			"time": "2020-03-07T23:26:06-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}