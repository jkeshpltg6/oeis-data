{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A271674",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 271674,
			"data": "1,0,220,7920,548460,42276960,3818372800,385303564800,42556023409900,5056698223684800,638162986199119920,84683717201322993600,11723112517163129913600,1682392957299926013542400,249030549709148521993536000,37864267170542400351711467520",
			"name": "Number of n-step excursions on the 11-dimensional f.c.c. lattice.",
			"comment": [
				"a(n) = number of walks in the integer lattice Z^11 starting and ending at the origin, using only the steps of the form (s_1, ..., s_11) with s_1^2 + ... + s_11^2 = 2, i.e., each possible step has precisely two nonzero entries which can be +1 or -1."
			],
			"link": [
				"Christoph Koutschan, \u003ca href=\"/A271674/b271674.txt\"\u003eTable of n, a(n) for n = 0..433\u003c/a\u003e",
				"S. Hassani, C. Koutschan, J-M. Maillard, N. Zenine, \u003ca href=\"http://arxiv.org/abs/1601.05657\"\u003eLattice Green Functions: the d-dimensional face-centred cubic lattice, d = 8, 9, 10, 11, 12\u003c/a\u003e, arXiv:1601.05657 [math-ph], 2016.",
				"S. Hassani, C. Koutschan, J-M. Maillard, N. Zenine, \u003ca href=\"http://dx.doi.org/10.1088/1751-8113/49/16/164003\"\u003eLattice Green functions: the d-dimensional face-centred cubic lattice, d = 8, 9, 10, 11, 12\u003c/a\u003e, Journal of Physics A: Mathematical and Theoretical 49(16) (2016), 164003.",
				"C. Koutschan, \u003ca href=\"http://www.koutschan.de/data/fcc1/\"\u003eComputations for higher-dimensional fcc lattices\u003c/a\u003e.",
				"C. Koutschan, \u003ca href=\"http://www.koutschan.de/data/fcc1/fcc11_mop.txt\"\u003eDifferential operator annihilating the generating function\u003c/a\u003e."
			],
			"formula": [
				"The probability generating function P(z) = Sum_{n\u003e=0} a(n)*(z/220)^n is given by the 11-fold integral (1/Pi)^11 Int_{0..Pi} ... Int_{0..Pi} 1/(1-z*lambda_11) dk_1 ... dk_11, where the structure function is defined as lambda_11 = (1/binomial(11,2)) Sum_{i=1..11} Sum_{j=(i+1)..11} cos(k_i)*cos(k_j). The function P(z) conjecturally satisfies a linear ODE of order 27 with polynomial coefficients of degree 409 (see link above).",
				"Hence a(n) conjecturally satisfies a linear recurrence equation with polynomial coefficients."
			],
			"example": [
				"There is one walk with no steps.",
				"No walk with a single steps returns to the origin.",
				"The number of returning walks with two steps is exactly the number of allowed steps (called the coordination number of the lattice): a(2) = 4*binomial(11,2)."
			],
			"maple": [
				"nmax := 50: tt := [seq([seq(add(binomial(2*p,p)*binomial(2*j,2*p-n)*binomial(2*n+2*j-2*p,n+j-p), p = floor((n+1)/2)..floor((n+2*j)/2)), j = 0..floor((nmax-n)/2))], n = 0..nmax)]: for d1 from 3 to 11 do tt := [seq([seq(add(binomial(n,p)*add(binomial(2*j,2*q-p)*binomial(2*j+2*p-2*q,j+p-q)*tt[n-p+1,q+1], q = floor((p+1)/2)..floor((p+2*j)/2)), p = 0..n), j = 0..floor((nmax-n)/2))], n = 0..nmax)]: od: [seq(tt[n+1,1], n = 0..nmax)];"
			],
			"mathematica": [
				"nmax = 50; T = Table[Sum[Binomial[2 p, p]*Binomial[2 j, 2 p - n]*Binomial[2 n + 2 j - 2 p, n + j - p], {p, Floor[(n + 1)/2], Floor[(n + 2 j)/2]}], {n, 0, nmax}, {j, 0, Floor[(nmax - n)/2]}]; Do[T = Table[Sum[Binomial[n, p]*Sum[Binomial[2 j, 2 q - p]*Binomial[2 j + 2 p - 2 q, j + p - q]*T[[n - p + 1, q + 1]], {q, Floor[(p + 1)/2], Floor[(p + 2 j)/2]}], {p, 0, n}], {n, 0, nmax}, {j, 0, If[d1 \u003c 11, Floor[(nmax - n)/2], 0]}], {d1, 3, 11}]; First /@ T"
			],
			"xref": [
				"Cf. A002899 (d = 3, i.e., excursions on the 3-dimensional f.c.c. lattice), A271432 (d = 4), A271650 (d = 5), A271651 (d = 6), A271670 (d = 7), A271671 (d = 8), A271672 (d = 9), A271673 (d = 10), this sequence (d = 11)."
			],
			"keyword": "nonn,walk",
			"offset": "0,3",
			"author": "_Christoph Koutschan_, Apr 12 2016",
			"references": 8,
			"revision": 13,
			"time": "2016-04-13T09:09:10-04:00",
			"created": "2016-04-12T12:32:49-04:00"
		}
	]
}