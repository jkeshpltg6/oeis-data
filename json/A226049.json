{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A226049",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 226049,
			"data": "3,4,28,986,1759060,4906125653220,26098452883705403280615128,1041141217110191230944018432385108773652321818272607",
			"name": "Denominators of signed Egyptian fractions with sums converging to the golden ratio.",
			"comment": [
				"Suppose that f(n) is a strictly decreasing sequence of positive numbers having limit 0.  Let g = (inverse of f).  An algorithm for signed Egyptian fractions for r follows:",
				"Let a(1) be the least positive integer k such that f(1)+...+f(k) \u003e= r.  Let p(r) = f(1)+...+f(a(1)).  If p(r) = r the algorithm terminates.",
				"Otherwise, let a(2) be the greatest k such that p(r)-f(k) \u003c= r, so that a(2) = floor[g(p(r)-r)].  If p(r)-f(a(2)) = r, the algorithm terminates.",
				"Otherwise, let a(3) be the least k such that p(r)-f(a(2))+f(k) \u003e= r, so that a(3) = floor[g(r-p(r)+f(a(2)))].  If p(r)-f(a(2))+f(a(3)) = r, the algorithm terminates.",
				"Otherwise, the algorithm continues inductively:",
				"If n is odd, let a(n+1) be the greatest k such that p(r)-f(a(2))+f(a(3))-...+f(a(n))-f(k) \u003c= r, so that a(n+1) = floor[g(p(r)-r-f(a(2))+f(a(3))-...-f(a(n)))].  If p(r)-f(a(2))+ f(a(3))- ...-f(a(n)) = r, the algorithm terminates.",
				"If n is even, let a(n+1) be the least k such that",
				"p(r)- f(a(2))+f(a(3))-...-f(a(n))+f(k) \u003e= r, so that a(n+1) = floor[g(p(r)-r-f(a(2))+ f(a(3))-...+f(a(n)))].  If p(r)-f(a(2))+f(a(3))-...+f(a(n)) = r, the algorithm terminates.",
				"Taking r = (1+sqrt(5))/2 and f(n) = 1/n gives the present sequence, and",
				"r = 1/1 + 1/2 + 1/3 - 1/4 + 1/28 - 1/986 + 1/a(5) - 1/a(6) + ...",
				"The 14th partial sum differs from r by less than 10^(-1000).",
				"In the following guide to related sequences, EM represents the Euler-Mascheroni constant.:",
				"r ............... f(n) ... a(n)",
				"(1+sqrt(5))/2 ... 1/n .... A226049",
				"e ............... 1/n .... A226050",
				"pi .............. 1/n .... A226051",
				"sqrt(2) ......... 1/n .... A226052",
				"sqrt(1/2) ....... 1/n .... A226053",
				"EM ...............1/n .... A226058"
			],
			"reference": [
				"Mohammad K. Azarian, Problem 123, Missouri Journal of Mathematical Sciences, Vol. 10, No. 3, Fall 1998, p. 176.  Solution published in Vol. 12, No. 1, Winter 2000, pp. 61-62."
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A226049/b226049.txt\"\u003eTable of n, a(n) for n = 1..12\u003c/a\u003e"
			],
			"example": [
				"1 + 1/2 \u003c r \u003c 1 + 1/2 + 1/3, where r = golden ratio = (1 + sqrt(5))/2; so a(1) = 3.",
				"1 + 1/2 + 1/3 - 1/4 \u003c r, so a(2) = 4.",
				"1 + 1/2 + 1/3 - 1/4 + 1/27 \u003c r \u003c 1 + 1/2 + 1/3 - 1/4 + 1/28, so a(3) = 28."
			],
			"mathematica": [
				"$MaxExtraPrecision = Infinity;",
				"nn = 10; f[n_] := 1/n; r = GoldenRatio; s = 0; b[1] = NestWhile[# + 1 \u0026, 1, ! (s += f[#]) \u003e r \u0026]; u[1] = Sum[f[n], {n, 1, b[1]}]; c[1] = Floor[1/(u[1] - r)]; v[1] = u[1] - 1/c[1]; n = 1; While[n \u003c nn/2, n++; b[n] = Floor[1/(r - v[n - 1])]; u[n] = v[n - 1] + 1/b[n]; c[n] = Floor[1/(u[n] - r)]; v[n] = u[n] - 1/c[n]]; a = Riffle[Table[b[i], {i, 1, nn/2}], Table[c[i], {i, 1, nn/2}]]"
			],
			"xref": [
				"Cf. A226050."
			],
			"keyword": "nonn,frac",
			"offset": "1,1",
			"author": "_Clark Kimberling_, May 24 2013",
			"references": 12,
			"revision": 14,
			"time": "2018-02-22T22:59:22-05:00",
			"created": "2013-05-26T15:38:40-04:00"
		}
	]
}