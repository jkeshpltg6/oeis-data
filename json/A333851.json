{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A333851",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 333851,
			"data": "2,2,1,1,1,1,1,2,2,1,1,1,2,2,3,3,2,2,2,2,2,10,2,1,1,1,1,2,2,2,2,1,1,1,1,2,2,2,2,2,2,2,2,1,1,6,6,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,14,38,2,2",
			"name": "Irregular triangle read by rows: T(n, k) = gcd(A333850(n, k), 2*A333855(n)), for n \u003e= 1, and k = 1,2, ..., A135303(A333855(n)).",
			"comment": [
				"The length of row n is A135303(A333855(n)) (the B numbers for A333855(n))."
			],
			"link": [
				"Wolfdieter Lang, \u003ca href=\"https://arxiv.org/abs/2008.04300\"\u003eOn the Equivalence of Three Complete Cyclic Systems of Integers\u003c/a\u003e, arXiv:2008.04300 [math.NT], 2020."
			],
			"formula": [
				"T(n, k) = gcd((A333850(n, k), 2*A333855(n)), for n \u003e= 1, and k = 1, 2, ..., A135303(A333855(n)) (B numbers \u003e= 2 for A333855(n))."
			],
			"example": [
				"The irregular triangle T(n, k) begins (here A(n) = A333855(n)):",
				"n,  A(n) \\ k   1     2    3    4   5  6  7  8  9 ...",
				"----------------------------------------------------------------",
				"1,   17:       2     2",
				"2,   31:       1     1    1",
				"3,   33:       1     1",
				"4,   41:       2     2",
				"5,   43:       1     1    1",
				"6,   51:       2     2",
				"7,   57:       3     3",
				"8,   63:       2     2    2",
				"9,   65:       2     2   10    2",
				"10,  73:       1     1    1    1",
				"11,  85:       2     2    2    2",
				"12,  89:       1     1    1    1",
				"13,  91:       2     2    2",
				"14,  93:       2     2    2",
				"15,  97:       2     2",
				"16,  99:       1     1",
				"17, 105:       6     6",
				"18, 109:       2     2    2",
				"19, 113:       2     2    2    2",
				"20, 117:       2     2    2",
				"21, 119:       2     2",
				"22, 123:       2     2",
				"23, 127:       1     1    1    1   1  1  1  1  1",
				"24, 129:       1     1    1    1   1  1",
				"25, 133:       2    14   38",
				"26, 137:       2     2",
				"..."
			],
			"program": [
				"(PARI) RRS(n) = select(x-\u003e(((x%2)==1) \u0026\u0026 (gcd(n, x)==1)), [1..n]);",
				"isok8(m, n) = my(md = Mod(2, 2*n+1)^m); (md==1) || (md==-1);",
				"A003558(n) = my(m=1); while(!isok8(m, n) , m++); m;",
				"B(n) = eulerphi(n)/(2*A003558((n-1)/2));",
				"fmiss(rrs, qs) = {for (i=1, #rrs, if (! setsearch(qs, rrs[i]), return (rrs[i])););}",
				"listb(nn) = {my(v=List()); forstep (n=3, nn, 2, my(bn = B(n)); if (bn \u003e= 2, listput(v, n););); Vec(v);}",
				"pergcd(n) = {my(bn = B(n)); if (bn \u003e= 2, my(vn = vector(bn)); my(q=1, qt = List()); my(p = A003558((n-1)/2)); my(rrs = RRS(n)); for (k=1, bn, my(qp = List()); q = fmiss(rrs, Set(qt)); listput(qp, q); listput(qt, q); for (i=1, p-1, q = abs(n-2*q); listput(qp, q); listput(qt, q);); vn[k] = gcd(vecsum(Vec(qp)), 2*n);); return (vn););}",
				"listag(nn) = {my(v = listb(nn)); vector(#v, k, pergcd(v[k]));} \\\\ _Michel Marcus_, Jun 14 2020"
			],
			"xref": [
				"Cf. A333848, A333850, A333854, A333855."
			],
			"keyword": "nonn,tabf",
			"offset": "1,1",
			"author": "_Wolfdieter Lang_, Jun 08 2020",
			"ext": [
				"Some incorrect terms were found by _Michel Marcus_, Jun 11 2010"
			],
			"references": 2,
			"revision": 16,
			"time": "2020-10-15T16:36:44-04:00",
			"created": "2020-06-19T04:54:38-04:00"
		}
	]
}