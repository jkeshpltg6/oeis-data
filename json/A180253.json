{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A180253",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 180253,
			"data": "0,3,4,9,6,24,8,21,16,36,12,64,14,48,48,45,18,87,20,96,64,72,24,144,36,84,52,128,30,216,32,93,96,108,96,229,38,120,112,216,42,288,44,192,174,144,48,304,64,201,144,224,54,276,144,288,160,180,60,552,62,192,232,189",
			"name": "Call two divisors of n adjacent if the larger is a prime times the smaller. a(n) is the sum of elements of all pairs of adjacent divisors of n.",
			"comment": [
				"The pairs of adjacent divisors of n are counted in A062799(n).",
				"For each divisor d of n we can check in how many pairs it occurs. For each prime divisor p of n, see the exponent of p in the factorization of d. If it's positive (p|d) then it occurs once more. If d*p doesn't divide n, add one to the frequency as well. - _David A. Corneth_, Dec 17 2018"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A180253/b180253.txt\"\u003eTable of n, a(n) for n = 1..20000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Sum_{d|n} d*Sum_{p|d} (1 + 1/p) where p is restricted to primes.",
				"a(n) = Sum_{d|n} A069359(d) + Sum_{d|n} d*A001221(d).",
				"a(n) = A323599(n) + A329354(n) = A323599(n) + A328260(n) + A329375(n). - _Antti Karttunen_, Nov 15 2019",
				"a(p^k) = (p^k - 1)*(p + 1)/(p - 1).",
				"a(p_1*p_2*...*p_m) = m*(p_1 + 1)*(p_2 + 1)*...*(p_m + 1).",
				"a(p*q^k) = (p + 1)*(2*q^k + 3*q^(k - 1) + 3*q^(k - 2) + ... + 3*q + 2).",
				"a(p*q*r^k) = (p + 1)*(q + 1)*(3*r^k + 4*r^(k - 1) + 4*r^(k - 2) + ... + 4*r + 3) and similar for a larger number of distinct prime factors of n."
			],
			"example": [
				"a(4) = (1 + 2) + (2 + 4) = 9.",
				"a(120) = a(3*5*2^3) = 4*6*(3*8 + 4*4 + 4*2 + 3) = 1224."
			],
			"mathematica": [
				"divisorSumPrime[n_] := DivisorSum[n, 1+1/# \u0026, PrimeQ[#] \u0026]; a[n_] := DivisorSum[n, #*divisorSumPrime[#]\u0026 ]; Array[a, 70] (* _Amiram Eldar_, Dec 17 2018 *)"
			],
			"program": [
				"(PARI) a(n) = sumdiv(n, d, d*sumdiv(d, p, isprime(p)*(1+1/p))); \\\\ _Michel Marcus_, Dec 17 2018",
				"(PARI) a(n) = my(f = factor(n), res = 0); fordiv(n, d, for(i = 1, #f~, v = valuation(d, f[i, 1]); res+=(d * ((v \u003e 0) + (v \u003c f[i, 2]))))); res \\\\ _David A. Corneth_, Dec 17 2018"
			],
			"xref": [
				"Cf. A001221, A062799, A069359, A179926, A180026, A323599, A328260, A329354, A329375."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Vladimir Shevelev_, Aug 20 2010",
			"ext": [
				"Definition rephrased, entries checked, one example added. - _R. J. Mathar_, Oct 25 2010"
			],
			"references": 4,
			"revision": 27,
			"time": "2019-11-15T21:32:05-05:00",
			"created": "2010-08-27T03:00:00-04:00"
		}
	]
}