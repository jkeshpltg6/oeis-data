{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A224697",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 224697,
			"data": "1,1,1,1,1,1,1,1,1,1,1,1,2,1,1,1,1,2,2,1,1,1,1,3,3,3,1,1,1,1,3,4,4,3,1,1,1,1,4,5,7,5,4,1,1,1,1,4,7,9,9,7,4,1,1,1,1,5,8,14,11,14,8,5,1,1,1,1,5,10,17,20,20,17,10,5,1,1",
			"name": "Number A(n,k) of different ways to divide an n X k rectangle into subsquares, considering only the list of parts; square array A(n,k), n \u003e= 0, k \u003e= 0, read by antidiagonals.",
			"link": [
				"Alois P. Heinz and Christopher Hunt Gribble, \u003ca href=\"/A224697/b224697.txt\"\u003eAntidiagonals n = 0..27, flattened\u003c/a\u003e (first 25 antidiagonals from Alois P. Heinz)"
			],
			"example": [
				"A(4,5) = 9 because there are 9 ways to divide a 4 X 5 rectangle into subsquares, considering only the list of parts: [20(1 X 1)], [16(1 X 1), 1(2 X 2)], [12(1 X 1), 2(2 X 2)], [11(1 X 1), 1(3 X 3)], [8(1 X 1), 3(2 X 2)], [7(1 X 1), 1(2 X 2), 1(3 X 3)], [4(1 X 1), 4(2 X 2)], [4(1 X 1), 1(4 X 4)], [3(1 X 1), 2(2 X 2), 1(3 X 3)].  There is no way to divide this rectangle into [2(1 X 1), 2(3 X 3)].",
				"Square array A(n,k) begins:",
				"  1, 1, 1,  1,  1,  1,  1,   1,   1,   1, ...",
				"  1, 1, 1,  1,  1,  1,  1,   1,   1,   1, ...",
				"  1, 1, 2,  2,  3,  3,  4,   4,   5,   5, ...",
				"  1, 1, 2,  3,  4,  5,  7,   8,  10,  12, ...",
				"  1, 1, 3,  4,  7,  9, 14,  17,  24,  29, ...",
				"  1, 1, 3,  5,  9, 11, 20,  26,  36,  48, ...",
				"  1, 1, 4,  7, 14, 20, 31,  47,  71,  95, ...",
				"  1, 1, 4,  8, 17, 26, 47,  57, 102, 143, ...",
				"  1, 1, 5, 10, 24, 36, 71, 102, 148, 238, ...",
				"  1, 1, 5, 12, 29, 48, 95, 143, 238, 312, ..."
			],
			"maple": [
				"b:= proc(n, l) option remember; local i, k, s, t;",
				"      if max(l[])\u003en then {} elif n=0 or l=[] then {[]}",
				"    elif min(l[])\u003e0 then t:=min(l[]); b(n-t, map(h-\u003eh-t, l))",
				"    else for k do if l[k]=0 then break fi od; s:={};",
				"         for i from k to nops(l) while l[i]=0 do s:=s union",
				"             map(x-\u003esort([x[], 1+i-k]), b(n, [l[j]$j=1..k-1,",
				"                 1+i-k$j=k..i, l[j]$j=i+1..nops(l)]))",
				"         od; s",
				"      fi",
				"    end:",
				"A:= (n, k)-\u003e `if`(n\u003e=k, nops(b(n, [0$k])), nops(b(k, [0$n]))):",
				"seq(seq(A(n, d-n), n=0..d), d=0..12);"
			],
			"mathematica": [
				"b[n_, l_] := b[n, l] = Module[{i, k, m, s, t}, Which[Max[l] \u003e n, {}, n == 0 || l == {}, {{}}, Min[l] \u003e 0, t = Min[l]; b[n-t, l-t], True, k = Position[l, 0, 1][[1, 1]]; s = {}; For[i = k, i \u003c= Length[l] \u0026\u0026 l[[i]] == 0, i++, s = s ~Union~ Map[Function[x, Sort[Append[x, 1+i-k]]], b[n, Join[l[[1 ;; k-1]], Array[1+i-k\u0026, i-k+1], l[[i+1 ;; -1]] ] ]]]; s]]; a[n_, k_] := If[n \u003e= k, Length @ b[n, Array[0\u0026, k]], Length @ b[k, Array[0\u0026, n]]]; Table[Table[a[n, d-n], {n, 0, d}], {d, 0, 12}] // Flatten (* _Jean-François Alcover_, Dec 19 2013, translated from Maple *)"
			],
			"xref": [
				"Columns (or rows) k=0+1, 2-5 give: A000012, A008619, A001399, A008763(n+4), A187753.",
				"Main diagonal gives: A034295.",
				"Cf. A225622."
			],
			"keyword": "nonn,tabl",
			"offset": "0,13",
			"author": "_Alois P. Heinz_, Apr 15 2013",
			"references": 14,
			"revision": 34,
			"time": "2019-11-11T21:36:29-05:00",
			"created": "2013-04-19T11:06:00-04:00"
		}
	]
}