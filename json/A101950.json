{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A101950",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 101950,
			"data": "1,1,1,0,2,1,-1,1,3,1,-1,-2,3,4,1,0,-4,-2,6,5,1,1,-2,-9,0,10,6,1,1,3,-9,-15,5,15,7,1,0,6,3,-24,-20,14,21,8,1,-1,3,18,-6,-49,-21,28,28,9,1,-1,-4,18,36,-35,-84,-14,48,36,10,1,0,-8,-4,60,50,-98,-126,6,75,45,11,1,1,-4,-30,20,145,36,-210",
			"name": "Product of A049310 and A007318 as lower triangular matrices.",
			"comment": [
				"A Chebyshev and Pascal product.",
				"Row sums are n+1, diagonal sums the constant sequence 1 resp. A023434(n+1). Riordan array (1/(1-x+x^2),x/(1-x+x^2)).",
				"Apart from signs, identical with A104562.",
				"Subtriangle of the triangle given by [0,1,-1,1,0,0,0,0,0,0,0,...] DELTA [1,0,0,0,0,0,0,0,0,0,0,...] where DELTA is the operator defined in A084938. - _Philippe Deléham_, Jan 27 2010",
				"The Fi1 and Fi2 sums lead to A004525 and the Gi1 sums lead to A077889, see A180662 for the definitions of these triangle sums. - _Johannes W. Meijer_, Aug 06 2011"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A101950/b101950.txt\"\u003eTable of n, a(n) for n = 0..1325\u003c/a\u003e",
				"J. R. Dias, \u003ca href=\"http://www.stkpula.hr/ccacaa/CCA-PDF/cca2004/v77-n1_n2/CCA_77_2004_325-330_dias.pdf\"\u003e Properties and relationships of conjugated polyenes having a reciprocal eigenvalue spectrum - dendralene and radialene hydrocarbons \u003c/a\u003e, Croatica Chem. Acta, 77 (2004), 325-330. [p. 328]."
			],
			"formula": [
				"T(n, k) = Sum_{j=0..n} (-1)^((n-j)/2)*C((n+j)/2,j)*(1+(-1)^(n+j))*C(j,k)/2.",
				"T(0,0) = 1, T(n,k) = 0,if k\u003en or if k\u003c0, T(n,k) = T(n-1,k-1) + T(n-1,k) - T(n-2,k). - _Philippe Deléham_, Jan 26 2010",
				"p(n,x) = (x+1)*p(n-1,x)-p(n-2,x) with p(0,x) = 1 and p(1,x) = x+1 [Dias].",
				"G.f.: 1/(1-x-x^2-y*x). - _Philippe Deléham_, Feb 10 2012",
				"T(n,0) = A010892(n), T(n+1,1) = A099254(n), T(n+2,2) = A128504(n). - _Philippe Deléham_, Mar 07 2014",
				"T(n,k) = C(n,k)*hypergeom([(k-n)/2, (k-n+1)/2], [-n], 4)) for n\u003e=1. - _Peter Luschny_, Apr 25 2016"
			],
			"example": [
				"Triangle begins:",
				"1,",
				"1,1,",
				"0,2,1,",
				"-1,1,3,1,",
				"-1,-2,3,4,1,",
				"..",
				"Triangle [0,1,-1,1,0,0,0,0,...] DELTA [1,0,0,0,0,0,...] begins : 1 ; 0,1 ; 0,1,1 ; 0,0,2,1 ; 0,-1,1,3,1 ; 0,-1,-2,3,4,1 ; ... - _Philippe Deléham_, Jan 27 2010"
			],
			"maple": [
				"A101950 := proc(n,k) local j,k1: add((-1)^((n-j)/2)*binomial((n+j)/2,j)*(1+(-1)^(n+j))* binomial(j,k)/2, j=0..n) end: seq(seq(A101950(n,k),k=0..n), n=0..11); # _Johannes W. Meijer_, Aug 06 2011"
			],
			"mathematica": [
				"T[0, 0] = 1; T[n_, k_] /; k\u003en || k\u003c0 = 0; T[n_, k_] := T[n, k] = T[n-1, k-1]+T[n-1, k]-T[n-2, k]; Table[T[n, k], {n, 0, 12}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Mar 07 2014, after _Philippe Deléham_ *)"
			],
			"xref": [
				"Cf. A104562."
			],
			"keyword": "easy,sign,tabl",
			"offset": "0,5",
			"author": "_Paul Barry_, Dec 22 2004",
			"ext": [
				"Typo in formula corrected and information added by _Johannes W. Meijer_, Aug 06 2011"
			],
			"references": 56,
			"revision": 35,
			"time": "2016-04-25T09:19:34-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}