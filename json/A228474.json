{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A228474",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 228474,
			"data": "0,1,4,2,24,26,3,1725,12,14,4,26,123,125,15,5,119,781802,20,22,132896,6,51,29,31,1220793,23,25,7,429,8869123,532009,532007,532009,532011,26,8,94,213355,213353,248,33,31,33,1000,9,144,110,112,82,84,210,60,34",
			"name": "Number of steps required to reach zero in the wrecker ball sequence starting with n: On the k-th step (k = 1, 2, 3, ...) move a distance of k in the direction of zero. If the result has occurred before, move a distance of k away from zero instead. Set a(n) = -1 if 0 is never reached.",
			"comment": [
				"This is a Recamán-like sequence (cf. A005132).",
				"The n-th triangular number A000217(n) has a(A000217(n)) = n.",
				"a(n) + 1 = length of row n in tables A248939 and A248973. - _Reinhard Zumkeller_, Oct 20 2014",
				"_Hans Havermann_, running code from _Hugo van der Sanden_, has found that a(11281) is 3285983871526. - _N. J. A. Sloane_, Mar 22 2019",
				"If a(n) != -1 then floor((a(n)-1)/2)+n is odd. - _Robert Gerbicz_, Mar 28 2019"
			],
			"link": [
				"Jon E. Schoenfield, \u003ca href=\"/A228474/b228474.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Gordon Hamilton, \u003ca href=\"http://www.youtube.com/watch?v=mQdNaofLqVc\"\u003eWrecker Ball Sequences\u003c/a\u003e, Video, 2013. [The sequence is mentioned about 4.5 minutes in to the video. The video begins by discussing A005132. - _N. J. A. Sloane_, Apr 25 2019]",
				"Hans Havermann, \u003ca href=\"/A228474/a228474.txt\"\u003eTable of n, a(n) for n = 0..36617\u003c/a\u003e",
				"Hans Havermann, \u003ca href=\"/A228474/a228474.png\"\u003eLog-plot of terms through n = 36617\u003c/a\u003e",
				"Hans Havermann, \u003ca href=\"http://gladhoboexpress.blogspot.com/2019/04/sharp-peaks-and-high-plateaus.html\"\u003eSharp peaks and high plateaus\u003c/a\u003e An overview of large knowns and unknowns up to index 10^6.",
				"\u003ca href=\"/index/Rea#Recaman\"\u003eIndex entries for sequences related to Recamán's sequence\u003c/a\u003e"
			],
			"example": [
				"a(2) = 4 because 2 -\u003e 1 -\u003e -1 -\u003e -4 -\u003e 0.",
				"See A248940 for the full 1725-term trajectory of 7. See A248941 for a bigger example, which shows the start of the 701802-term trajectory of 17. - _N. J. A. Sloane_, Mar 07 2019"
			],
			"maple": [
				"# To compute at most the first M steps of the trajectory of n:",
				"f:=proc(n) local M,i,j,traj,h;",
				"  M:=200; traj:=[n]; h:=n; s:=1;",
				"  for i from 1 to M do j:=h-s*i;",
				"    if member(j,traj,'p') then s:=-s; fi;",
				"    h:=h-s*i; traj:=[op(traj),h];",
				"    if h=0 then return(\"steps, trajectory =\", i, traj); fi;",
				"    s:=sign(h);",
				"  od;",
				"  lprint(\"trajectory so far = \", traj); error(\"Need to increase M\");",
				"end;  # _N. J. A. Sloane_, Mar 07 2019"
			],
			"mathematica": [
				"{0}~Join~Array[-1 + Length@ NestWhile[Append[#1, If[FreeQ[#1, #3], #3, Sign[#1[[-1]] ] (Abs[#1[[-1]] ] + #2)]] \u0026 @@ {#1, #2, Sign[#1[[-1]] ] (Abs[#1[[-1]] ] - #2)} \u0026 @@ {#, Length@ #} \u0026, {#}, Last@ # != 0 \u0026] \u0026, 16] (* _Michael De Vlieger_, Mar 27 2019 *)"
			],
			"program": [
				"(PARI) a(n)={my(M=Map(),k=0); while(n, k++; mapput(M,n,1); my(t=if(n\u003e0, -k, +k)); n+=if(mapisdefined(M,n+t),-t,t)); k} \\\\ _Charles R Greathouse IV_, Aug 18 2014, revised _Andrew Howroyd_, Feb 28 2018 [Warning: requires latest PARI. - _N. J. A. Sloane_, Mar 09 2019]",
				"(Haskell) a228474 = subtract 1 . length . a248939_row  -- _Reinhard Zumkeller_, Oct 20 2014",
				"(C++) #include \u003cmap\u003e",
				"  int A228474(long n) { int c=0, s; for(std::map\u003clong,bool\u003e seen; n; n += seen[n-(s=n\u003e0?c:-c)] ? s:-s) { seen[n]=true; ++c; } return c; } // _M. F. Hasler_, Mar 18 2019",
				"(Julia)",
				"function A228474(n)",
				"    k, position, beenhere = 0, n, [n]",
				"    while position != 0",
				"        k += 1",
				"        step = position \u003e 0 ? k : -k",
				"        position += (position - step) in beenhere ? step : -step",
				"        push!(beenhere, position)",
				"    end",
				"    return length(beenhere) - 1",
				"end",
				"println([A228474(n) for n in 0:16]) # _Peter Luschny_, Mar 24 2019"
			],
			"xref": [
				"Cf. A248939 (rows = the full sequences), A248961 (row sums), A248973 (partial sums per row), A248952 (min per row), A248953 (max per row), A001532.",
				"Cf. also A248940 (row 7), A248941 (row 17), A248942 (row 20).",
				"Cf. also A000217, A005132 (Recamán)."
			],
			"keyword": "walk,nonn,look,hear",
			"offset": "0,3",
			"author": "_Gordon Hamilton_, Aug 23 2013",
			"ext": [
				"More terms from _Jon E. Schoenfield_, Jan 10 2014",
				"Escape clause in definition added by _N. J. A. Sloane_, Mar 07 2019",
				"Edited by _M. F. Hasler_, Mar 18 2019"
			],
			"references": 42,
			"revision": 105,
			"time": "2019-05-16T01:32:32-04:00",
			"created": "2013-08-25T12:26:13-04:00"
		}
	]
}