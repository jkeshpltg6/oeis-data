{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A033469",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 33469,
			"data": "1,12,240,1344,3840,33792,5591040,49152,16711680,104595456,173015040,289406976,22900899840,201326592,116769423360,7689065201664,1095216660480,51539607552,65942866278481920,824633720832,7438196161904640,3971435999526912",
			"name": "Denominator of Bernoulli(2n,1/2).",
			"comment": [
				"From the von Staudt-Clausen theorem it follows that a(n) can be computed without using Bernoulli polynomials or the 'denominator'-function (see the Sage implementation). - _Peter Luschny_, Mar 24 2014"
			],
			"reference": [
				"J. R. Philip, The symmetrical Euler-Maclaurin summation formula, Math. Sci., 6, 1981, pp. 35-41."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A033469/b033469.txt\"\u003eTable of n, a(n) for n = 0..250\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/vonStaudt-ClausenTheorem.html\"\u003evon Staudt-Clausen Theorem\u003c/a\u003e.",
				"\u003ca href=\"/index/Be#Bernoulli\"\u003eIndex entries for sequences related to Bernoulli numbers.\u003c/a\u003e"
			],
			"formula": [
				"a(n) = denominator(2*(2*Pi)^(-2*n)*(2*n)!*Li_{2*n}(-1)). - _Peter Luschny_, Jun 29 2012",
				"a(n) = A081294(n) * A002445(n) for n \u003e 0. - _Paul Curtz_, Apr 17 2013",
				"Apparently, denominators of the fractions with e.g.f. (x/2) / sinh(x/2). - _Tom Copeland_, Sep 17 2016"
			],
			"maple": [
				"with(numtheory); seq(denom(bernoulli(2*n, 1/2)), n=0..20);"
			],
			"mathematica": [
				"Table[ BernoulliB[2*n, 1/2] // Denominator, {n, 0, 18}] (* _Jean-François Alcover_, Apr 15 2013 *)",
				"a[ n_] := If[ n \u003c 0, 0, (2 n)! SeriesCoefficient[ x/2 / Sinh[x/2], {x, 0, 2 n}] // Denominator]; (* _Michael Somos_, Sep 21 2016 *)"
			],
			"program": [
				"(PARI) a(n)=denominator(subst(bernpol(2*n,x),x,1/2)); \\\\ _Joerg Arndt_, Apr 17 2013",
				"(Sage)",
				"def A033469(n):",
				"    if n == 0: return 1",
				"    M = map(lambda i: i+1, divisors(2*n))",
				"    return 2^(2*n-1)*mul(filter(lambda s: is_prime(s), M))",
				"[A033469(n) for n in (0..21)] # _Peter Luschny_, Mar 24 2014"
			],
			"xref": [
				"Cf. A001896."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Joerg Arndt_, Apr 17 2013"
			],
			"references": 7,
			"revision": 42,
			"time": "2016-09-21T22:55:18-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}