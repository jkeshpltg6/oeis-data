{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A325664",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 325664,
			"data": "1,0,1,-3,7,-15,30,-55,90,-125,125,0,-450,1625,-4250,9500,-18999,34357,-55454,75735,-70890,-26333,379049,-1352078,3713650,-9000225,20136806,-42409968,84819937,-161567265,292710630,-501416815,801992970,-1167081365,1453179125",
			"name": "First term of n-th difference sequence of (floor[k*r]), r = sqrt(2), k \u003e= 0.",
			"link": [
				"Clark Kimberling, \u003ca href=\"/A325664/b325664.txt\"\u003eTable of n, a(n) for n = 1..200\u003c/a\u003e"
			],
			"formula": [
				"From _Robert Israel_, Jun 04 2019: (Start)",
				"a(n) = Sum_{0\u003c=k\u003c=n} (-1)^(n-k)*binomial(n,k)*A001951(k).",
				"G.f.: g(x) = (1+x)^(-1)*h(x/(1+x)) where h is the G.f. of A001951. (End)"
			],
			"example": [
				"The sequence (floor(k*r)) for k\u003e=0: 0, 1, 2, 4, 5, 7, 8, 9, 11, 12, ...",
				"1st difference sequence:  1,  1,  2,  1,  2,  1,  1,  2,  1,  2,  1,  1,  2, 1, ...",
				"2nd difference sequence:  0,  1, -1,  1, -1,  0,  1, -1,  1, -1,  0,  1, -1, ...",
				"3rd difference sequence:  1, -2,  2, -2,  1,  1, -2,  2, -2,  1,  1, -2,  2, ...",
				"4th difference sequence: -3,  4, -4,  3,  0, -3,  4, -4,  3,  0, -3,  4, -4, ...",
				"5th difference sequence:  7, -8,  7, -3, -3,  7, -8,  7, -3, -3,  7, -8,  7, ..."
			],
			"maple": [
				"N:= 50: # for a(1)..a(N)",
				"L:= [seq(floor(sqrt(2)*n),n=0..N)]: Res:= NULL:",
				"for i from 1 to N do",
				"   L:= L[2..-1]-L[1..-2];",
				"   Res:= Res, L[1];",
				"od:",
				"Res; # _Robert Israel_, Jun 04 2019"
			],
			"mathematica": [
				"Table[First[Differences[Table[Floor[Sqrt[2]*n], {n, 0, 50}], n]], {n, 1, 50}]"
			],
			"xref": [
				"Cf. A001951.",
				"Guide to related sequences:",
				"A325664, r = sqrt(2)",
				"A325665, r = -sqrt(2)",
				"A325666, r = sqrt(3)",
				"A325667, r = -sqrt(3)",
				"A325668, r = sqrt(5)",
				"A325669, r = -sqrt(5)",
				"A325670, r = sqrt(6)",
				"A325671, r = -sqrt(6)",
				"A325672, r = sqrt(7)",
				"A325673, r = -sqrt(7)",
				"A325674, r = sqrt(8)",
				"A325675, r = -sqrt(8)",
				"A325729, r = sqrt(1/2)",
				"A325730, r = sqrt(1/3)",
				"A325731, r = sqrt(2/3)",
				"A325732, r = sqrt(3/4)",
				"A325733, r = 1/2 + sqrt(2)",
				"A325734, r = e",
				"A325735, r = -e",
				"A325736, r = 2e",
				"A325737, r = 3e",
				"A325738, r = e/2",
				"A325739, r = Pi",
				"A325740, r = 2Pi",
				"A325741, r = Pi/2",
				"A325742, r = Pi/3",
				"A325743, r = Pi/4",
				"A325744, r = Pi/6",
				"A325745, r = tau = golden ratio = (1 + sqrt(5))/2",
				"A325746, r = -tau",
				"A325747, r = tau^2 = 1 + tau",
				"A325748, r = 1/e",
				"A325749, r = e/(e-1)",
				"A325750, r = (1+sqrt(3))/2",
				"A325751, r = log 2",
				"A325752, r = log 3"
			],
			"keyword": "easy,sign",
			"offset": "1,4",
			"author": "_Clark Kimberling_, May 12 2019",
			"references": 27,
			"revision": 16,
			"time": "2019-06-18T13:36:22-04:00",
			"created": "2019-05-13T01:11:12-04:00"
		}
	]
}