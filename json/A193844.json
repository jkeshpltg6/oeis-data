{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A193844",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 193844,
			"data": "1,1,3,1,5,7,1,7,17,15,1,9,31,49,31,1,11,49,111,129,63,1,13,71,209,351,321,127,1,15,97,351,769,1023,769,255,1,17,127,545,1471,2561,2815,1793,511,1,19,161,799,2561,5503,7937,7423,4097,1023",
			"name": "Triangular array:  the fission of ((x+1)^n) by ((x+1)^n); i.e., the self-fission of Pascal's triangle.",
			"comment": [
				"See A193842 for the definition of fission of two sequences of polynomials or triangular arrays.",
				"A193844 is also the fission of (p1(n,x)) by (q1(n,x)), where p1(n,x)=x^n+x^(n-1)+...+x+1 and q1(n,x)=(x+2)^n.",
				"Essentially A119258 but without the main diagonal. - _Peter Bala_, Jul 16 2013",
				"From _Robert Coquereaux_, Oct 02 2014: (Start)",
				"This is also a rectangular array A(n,p) read down the antidiagonals:",
				"1 1 1 1 1 1 1 1 1",
				"3 5 7 9 11 13 15 17 19",
				"7 17 31 49 71 97 127 161 199",
				"15 49 111 209 351 545 799 1121 1519",
				"31 129 351 769 1471 2561 4159 6401 9439",
				"...",
				"Calling Gr(n) the Grassmann algebra with n generators, A(n,p) is the dimension of the space of Gr(n)-valued symmetric multilinear forms with vanishing graded divergence. If p is odd A(n,p) is the dimension of the cyclic cohomology group of order p of the Z2 graded algebra Gr(n). If p is even, the dimension of this cohomology group is A(n,p)+1. A(n,p) = 2^n*A059260(p,n-1)-(-1)^p.",
				"(End)",
				"The n-th row are also the coefficients of the polynomial P=sum_{k=0..n} (X+2)^k (in falling order, i.e., that of X^n first). - _M. F. Hasler_, Oct 15 2014"
			],
			"link": [
				"Jean-François Chamayou, \u003ca href=\"http://arxiv.org/abs/1410.1708\"\u003eA Random Difference Equation with Dufresne Variables revisited\u003c/a\u003e, arXiv:1410.1708 [math.PR], 2014.",
				"R. Coquereaux and E. Ragoucy, \u003ca href=\"http://dx.doi.org/10.1016/0393-0440(94)00014-U\"\u003eCurrents on Grassmann algebras\u003c/a\u003e, J. of Geometry and Physics, 1995, Vol 15, pp 333-352.",
				"R. Coquereaux and E. Ragoucy, \u003ca href=\"http://arxiv.org/abs/hep-th/9310147\"\u003eCurrents on Grassmann algebras\u003c/a\u003e, arXiv:hep-th/9310147, 1993.",
				"C. Kassel, \u003ca href=\"http://dx.doi.org/10.1007/BF01459145\"\u003eA Künneth formula for the cyclic cohomology of Z2-graded algebras\u003c/a\u003e, Math.  Ann. 275 (1986) 683."
			],
			"formula": [
				"From _Peter Bala_, Jul 16 2013: (Start)",
				"T(n,k) = sum {i = 0..k} (-1)^i*binomial(n+1,k-i)*2^(k-i).",
				"O.g.f.: 1/( (1 - x*t)*(1 - (2*x + 1)*t) ) = 1 + (1 + 3*x)*t + (1 + 5*x + 7*x^2)*t^2 + ....",
				"The n-th row polynomial R(n,x) = 1/(x+1)*( (2*x+1)^(n+1) - x^(n+1) ). (End)",
				"T(n,k) = T(n-1,k) + 3*T(n-1,k-1) - T(n-2,k-1) - 2*T(n-2,k-2), T(0,0) = 1, T(1,0) = 1, T(1,1) = 3, T(n,k) = 0 if k\u003c0 or if k\u003en. - _Philippe Deléham_, Jan 17 2014",
				"T(n,k) = 2^k*binomial(n+1,k)*hyper2F1(1,-k,-k+n+2, 1/2). - _Peter Luschny_, Jul 23 2014"
			],
			"example": [
				"First six rows:",
				"1",
				"1....3",
				"1....5....7",
				"1....7....17....15",
				"1....9....31....49....31",
				"1....11...49....111...129...63"
			],
			"maple": [
				"A193844 := (n,k) -\u003e 2^k*binomial(n+1,k)*hypergeom([1,-k],[-k+n+2],1/2);",
				"for n from 0 to 5 do seq(round(evalf(A193844(n,k))),k=0..n) od; # _Peter Luschny_, Jul 23 2014",
				"# Alternatively",
				"p := (n,x) -\u003e add(x^k*(1+2*x)^(n-k), k=0..n): for n from 0 to 7 do [n], PolynomialTools:-CoefficientList(p(n,x), x) od; # _Peter Luschny_, Jun 18 2017"
			],
			"mathematica": [
				"z = 10;",
				"p[n_, x_] := (x + 1)^n;",
				"q[n_, x_] := (x + 1)^n",
				"p1[n_, k_] := Coefficient[p[n, x], x^k];",
				"p1[n_, 0] := p[n, x] /. x -\u003e 0;",
				"d[n_, x_] := Sum[p1[n, k]*q[n - 1 - k, x], {k, 0, n - 1}]",
				"h[n_] := CoefficientList[d[n, x], {x}]",
				"TableForm[Table[Reverse[h[n]], {n, 0, z}]]",
				"Flatten[Table[Reverse[h[n]], {n, -1, z}]]  (* A193844 *)",
				"TableForm[Table[h[n], {n, 0, z}]]",
				"Flatten[Table[h[n], {n, -1, z}]]  (* A193845 *)"
			],
			"program": [
				"(Sage) # uses[fission from A193842]",
				"p = lambda n,x: (x+1)^n",
				"A193844_row = lambda n: fission(p, p, n)",
				"for n in range(7): print(A193844_row(n)) # _Peter Luschny_, Jul 23 2014"
			],
			"xref": [
				"Cf. A193842, A193845, A119258.",
				"Columns, diagonals: A000225, A000337, A055580, A027608, A211386, A211388, A000012, A005408, A056220, A199899.",
				"A145661 is an essentially identical triangle."
			],
			"keyword": "nonn,tabl",
			"offset": "0,3",
			"author": "_Clark Kimberling_, Aug 07 2011",
			"references": 6,
			"revision": 52,
			"time": "2020-03-26T04:26:16-04:00",
			"created": "2011-08-07T16:13:57-04:00"
		}
	]
}