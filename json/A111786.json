{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A111786",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 111786,
			"data": "1,-1,1,1,-2,1,-1,2,1,-3,1,1,-2,-2,3,3,-4,1,-1,2,2,1,-3,-6,-1,4,6,-5,1,1,-2,-2,-2,3,6,3,3,-4,-12,-4,5,10,-6,1,-1,2,2,2,1,-3,-6,-6,-3,-3,4,12,6,12,1,-5,-20,-10,6,15,-7,1,1,-2,-2,-2,-2,3,6,6,3,3,6,1,-4,-12,-12,-12,-12,-4,5,20,10,30,5,-6,-30,-20,7,21,-8,1,-1",
			"name": "Array used to obtain the complete symmetric function in n variables in terms of the elementary symmetric functions; irregular triangle T(n,k), read by rows, with n \u003e= 1 and 1 \u003c= k \u003c= A000041(n).",
			"comment": [
				"The unsigned numbers give A048996. They are not listed on pp. 831-832 of Abramowitz and Stegun (reference given in A103921). One could call these numbers M_0 (like M_1, M_2, M_3 given in A036038, A036039, A036040, resp.).",
				"The sequence of row lengths is A000041(n) (partition numbers).",
				"The sign is (-1)^(n + m(n,k)) with m(n,k) the number of parts of the k-th partition of n taken in the mentioned order. For m(n,k), see A036043.",
				"The row sum is 1 for n = 1, and 0 otherwise. The unsigned row sum is 2^(n-1) = A000079(n-1) for n \u003e= 1.",
				"The complete symmetric polynomial is also h(n; a[1],...,a[n]) = Det(A_n) with the matrix elements of the n X n matrix A_n given by A_n(k, k+1) = 1 for 1 \u003c= k \u003c n, A(k, m) = a[k-m+1] for n \u003e=  k \u003e= m \u003e= 1, and 0 otherwise. [For an explanation of this statement, see the example for n = 4 below. See also p. 3 in MacMahon (1960).]"
			],
			"reference": [
				"V. Krishnamurthy, Combinatorics, Ellis Horwood, Chichester, 1986, p. 55, eqs. (48) and (50)."
			],
			"link": [
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"Wolfdieter Lang, \u003ca href=\"/A111786/a111786.pdf\"\u003eFirst 10 rows\u003c/a\u003e.",
				"P. A. MacMahon, \u003ca href=\"http://www.hti.umich.edu/cgi/t/text/text-idx?c=umhistmath;idno=ABU9009\"\u003eCombinatory analysis\u003c/a\u003e (2 vols.), Chelsea, NY, 1960; see p. 4.",
				"OEIS, \u003ca href=\"https://oeis.org/wiki/Orderings_of_partitions\"\u003eOrderings of partitions\u003c/a\u003e."
			],
			"formula": [
				"The complete symmetric row polynomials h(n; a[1], ..., a[n]):= sum k over partitions of n of T(n, k)* A[k], with A[k] := a[1]^e(k, 1) * a[2]^e(k, 2) * ... * a[n]^e(k, n) is the k-th partition of n, in Abramowitz-Stegun order (see A105805 for this reference), is [1^e(k, 1), 2^e(k, 2), ..., n^e(k, n)], for k = 1..p(n), where p(n) = A000041(n) (partition numbers).",
				"G.f.: A(x) = 1/(1 + Sum_{j = 1..infinity} (-1)^j * a[j]).",
				"T(n, k) is the coefficient of x^n and a[1]^e(k, 1) * a[2]^e(k, 2) * ... * a[n]^e(k, n) in A(x) if the k-th partition of n, counted using the Abramowitz-Stegun order, is [1^e(k, 1), 2^e(k, 2), ..., n^e(k, n)] with e(k, j) \u003e= 0 (and if e(k, j) = 0 then j^0 is not recorded).",
				"T(n, k) = (-1)^(n + m(n, k)) * m(n, k)!/(Product_{j = 1..n} e(k, j)!), where m(n, k) := Sum_{j = 1..n} e(k, j), with [1^e(k, 1), 2^e(k, 2), ..., n^e(k, n)] being the k-th partition of n in the mentioned order. Here m(n, k) is the number of parts of the k-th partition of n. For m(n,k), see A036043."
			],
			"example": [
				"Triangle T(n,k) (with rows n \u003e= 1 and columns k \u003e= 1) begins as follows:",
				"   1;",
				"  -1,  1;",
				"   1, -2,  1;",
				"  -1,  2,  1, -3,  1;",
				"   1, -2, -2,  3,  3, -4,  1;",
				"  -1,  2,  2,  1, -3, -6, -1, 4, 6, -5, 1,",
				"   ...",
				"h(4; a[1],...,a[4])= -1*a[4] + 2*a[1]*a[3] + 1*a[2]^2 - 3*a[1]^2*a[2] + a[1]^4.",
				"Consider variables x_1, x_2, x_3, x_4, and let a[1] = Sum_i x_i, a[2] = Sum_{i,j} x_i*x_j, a[3] = Sum_{i,j,k} x_i*x_j*x_k, and a[4] = x1*x2*x3*x4, where in all the sums no term is repeated twice.",
				"Then h(4; a[1],...,a[4]) = Sum_i x_i^4 + Sum_{i,j} x_i^3*x_j + Sum_{i,j} x_i^2*x_j^2 + Sum_{i,j,k} x_i^2*x_j*x_k + Sum_{i,j,k,m} x_i*x_j*x_k*x_m, where again in all the sums no term is repeated twice. Thus, indeed, h is the complete symmetric polynomial in four variables x_1, x_2, x_3, x_4."
			],
			"xref": [
				"Cf. A000041, A000079, A036038, A036039, A036040, A036043, A048996, A103921, A105805, A115131, A210258."
			],
			"keyword": "sign,tabf",
			"offset": "1,5",
			"author": "_Wolfdieter Lang_, Aug 23 2005",
			"ext": [
				"Various sections edited by _Petros Hadjicostas_, Dec 15 2019"
			],
			"references": 7,
			"revision": 44,
			"time": "2019-12-21T18:25:35-05:00",
			"created": "2005-09-21T03:00:00-04:00"
		}
	]
}