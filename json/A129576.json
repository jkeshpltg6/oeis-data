{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A129576",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 129576,
			"data": "1,3,2,0,2,3,2,0,1,6,2,0,2,0,2,0,3,6,0,0,2,3,2,0,2,6,2,0,0,0,4,0,2,3,2,0,2,6,0,0,1,6,2,0,4,0,2,0,0,6,2,0,2,0,2,0,3,6,2,0,2,0,0,0,2,9,2,0,0,6,2,0,4,0,2,0,2,0,0,0,2,6,4,0,0,3,4",
			"name": "Expansion of phi(x) * chi(x) * psi(-x^3) in powers of x where phi(), chi(), psi() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Cubic AGM theta functions: a(q) (see A004016), b(q) (A005928), c(q) (A005882). - _Michael Somos_, Jun 28 2017"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A129576/b129576.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"From _Michael Somos_, Jun 28 2017: (Start)",
				"Expansion of q^(-1/3) * (2*c(q) + c(-q)) / 3 = q^(-1/3) * (c(q) + 2*c(q^4)) / 3 in powers of q where c() is a cubic AGM theta function.",
				"Expansion of (a(q) - a(q^3) + 2*a(q^4) - 2*a(q^12)) / 6 in powers of q where a() is a cubic AGM theta function.",
				"Expansion of q^(-1/3) * eta(q^2)^7 * eta(q^3) * eta(q^12) / (eta(q)^3 * eta(q^4)^3 * eta(q^6)) in powers of q. (End)",
				"Euler transform of period 12 sequence [3, -4, 2, -1, 3, -4, 3, -1, 2, -4, 3, -2, ...].",
				"a(n) = b(3*n + 1) where b() is multiplicative and b(2^e) = 3 * (1 + (-1)^e) / 2 if e\u003e0, a(3^e) = 0^e, a(p^e) = e+1 if p == 1 (mod 3), a(p^e) = (1 + (-1)^e)/2 if p == 2 (mod 3).",
				"a(n) = A096936(3*n + 1) = A112298(3*n + 1).",
				"2 * a(n) = A033716(3*n + 1). - _Michael Somos_, Sep 03 2016",
				"a(n) = (-1)^n * A122161(n). - _Michael Somos_, Jun 28 2017"
			],
			"example": [
				"G.f. = 1 + 3*x + 2*x^2 + 2*x^4 + 3*x^5 + 2*x^6 + x^8 + 6*x^9 + 2*x^10 + ...",
				"G.f. = q + 3*q^4 + 2*q^7 + 2*q^13 + 3*q^16 + 2*q^19 + q^25 + 6*q^28 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ 2^(-1/2) x^(-3/8) EllipticTheta[ 3, 0, x] QPochhammer[ -x, x^2] EllipticTheta[ 2, Pi/4, x^(3/2)], {x, 0, n}]; (* _Michael Somos_, Nov 11 2015 *)",
				"a[ n_] := Length @ FindInstance[ x^2 + 3 y^2 == 3 n + 1, {x, y}, Integers, 10^9] / 2; (* _Michael Somos_, Sep 03 2016 *)",
				"a[ n_] := If[ n \u003c 1, Boole[n == 0], Times @@ (Which[# == 3, Boole[#2 == 0], # == 2, 3 (1 + (-1)^#2)/2, Mod[#, 3] == 2, (1 + (-1)^#2)/2, True, #2 + 1] \u0026 @@@ FactorInteger[3 n + 1])]; (* _Michael Somos_, Jun 28 2017 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, 0, n = 3*n + 1; sumdiv(n, d, kronecker(-3, d) * [0, 1, -2, 1] [n/d%4 + 1] ))};",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A)^7 * eta(x^3 + A) * eta(x^12 + A) / (eta(x + A)^3 * eta(x^4 + A)^3 * eta(x^6 + A)), n))};",
				"(PARI) {a(n) = my(A, p, e); if( n\u003c0, 0, n = 3*n + 1; A = factor(n); prod( k=1, matsize(A)[1], [p, e] = A[k, ]; if( p==2, 3*(1-e%2), p==3, 0, p%3==2, 1-e%2, e+1)))}; /* _Michael Somos_, Jun 28 2017 */"
			],
			"xref": [
				"Cf. A033716, A096936, A112298, A122861."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Michael Somos_, Apr 23 2007",
			"references": 3,
			"revision": 27,
			"time": "2021-03-12T22:24:44-05:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}