{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A193356",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 193356,
			"data": "1,0,3,0,5,0,7,0,9,0,11,0,13,0,15,0,17,0,19,0,21,0,23,0,25,0,27,0,29,0,31,0,33,0,35,0,37,0,39,0,41,0,43,0,45,0,47,0,49,0,51,0,53,0,55,0,57,0,59,0,61,0,63,0,65,0,67,0,69,0,71,0,73,0,75",
			"name": "If n is even then 0, otherwise n.",
			"comment": [
				"Multiplicative with a(2^e)=0 if e\u003e0 and a(p^e)=p^e for odd primes p. - _R. J. Mathar_, Aug 01 2011",
				"A005408 and A000004 interleaved (the usual OEIS policy is not to include sequences like this where alternate terms are zero; this is an exception). - _Omar E. Pol_, Feb 02 2013",
				"Row sums of A057211. - _Omar E. Pol_, Mar 05 2014",
				"Column k=2 of triangle A196020. - _Omar E. Pol_, Aug 07 2015",
				"a(n) is the determinant of the (n+2) X (n+2) circulant matrix with the first row [0,0,1,1,...,1]. This matrix is closely linked with the famous ménage problem (see also comments of Vladimir Shevelev in sequence A000179). Namely it defines the class of permutations p of 1,2,...,n+2 such that p(i)\u003c\u003ei and p(i)\u003c\u003ei+1 for i=1,2,...,n+1, and p(n+2)\u003c\u003e1,n+2. And a(n) is also the difference between the number of even and odd such permutations. - _Dmitry Efimov_, Feb 02 2016"
			],
			"reference": [
				"Franz Lemmermeyer, Reciprocity Laws. From Euler to Eisenstein, Springer, 2000, p. 237, eq. (8.5)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A193356/b193356.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"C. Kravvaritis, \u003ca href=\"http://dx.doi.org/10.2478/spma-2014-0019\"\u003eDeterminant evaluations for binary circulant matrices\u003c/a\u003e, Special Matrices, V2(1) (2014), 187-199.",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,2,0,-1)."
			],
			"formula": [
				"a(n) = n^k mod 2n, for any k\u003e=2, also for k=n.",
				"Dirichlet g.f.: (1-2^(1-s))*zeta(s-1). - _R. J. Mathar_, Aug 01 2011",
				"G.f.: x*(1+x^2)/(1-x^2)^2. - _Philippe Deléham_, Feb 13 2012",
				"a(n) = A027656(A042948(n-1)) = (1-(-1)^n)*n/2. - _Bruno Berselli_, Feb 19 2012",
				"a(n) = n * (n mod 2). - _Wesley Ivan Hurt_, Jun 29 2013",
				"G.f.: sum(n\u003e=1, A000010(n)*x^n/(1+x^n)). - _Mircea Merca_, Feb 22 2014",
				"a(n) = 2*a(n-2)-a(n-4), for n\u003e4. - _Wesley Ivan Hurt_, Aug 07 2015",
				"E.g.f.: x*cosh(x). - _Robert Israel_, Feb 03 2016",
				"a(n) = Product_{k=1..floor(n/2)}(sin(2*Pi*k/n))^2, for n \u003e= 1 (with the empty product put to 1). Trivial for even n from the factor 0 for k = n/2. For odd n see, e.g., the Lemmermeyer reference, eq. (8.5) on p. 237. - _Wolfdieter Lang_, Aug 29 2016",
				"a(n) = Sum_{k=1..n} (-1)^((n-k)*k). - _Rick L. Shepherd_, Sep 18 2020"
			],
			"maple": [
				"A193356:=n-\u003e(1-(-1)^n)*n/2: seq(A193356(n), n=1..100); # _Wesley Ivan Hurt_, Aug 07 2015"
			],
			"mathematica": [
				"Table[PowerMod[n,n,2*n], {n,200}]"
			],
			"program": [
				"(PARI) a(n)=if(n%2,n) \\\\ _Charles R Greathouse IV_, Jul 24 2011",
				"(MAGMA) I:=[1,0,3,0]; [n le 4 select I[n] else 2*Self(n-2)-Self(n-4): n in [1..80]]; // _Vincenzo Librandi_, Feb 24 2014"
			],
			"xref": [
				"Cf. A000004, A000010, A005408, A027656, A042948, A057211, A196020."
			],
			"keyword": "nonn,easy,mult",
			"offset": "1,3",
			"author": "_José María Grau Ribas_, Jul 24 2011",
			"ext": [
				"Formula for a(n) extended by _Wolfdieter Lang_, Dec 21 2011"
			],
			"references": 26,
			"revision": 89,
			"time": "2020-09-19T02:54:10-04:00",
			"created": "2011-07-26T19:40:11-04:00"
		}
	]
}