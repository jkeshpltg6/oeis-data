{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A049798",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 49798,
			"data": "0,0,0,1,0,2,2,2,3,7,2,7,10,8,8,15,11,19,16,15,22,32,19,25,34,34,33,46,33,47,47,48,61,65,45,62,77,79,68,87,74,94,97,86,105,127,98,114,120,124,129,154,141,151,142,147,172,200,151,180",
			"name": "a(n) = (1/2)*Sum_{k = 1..n} T(n,k), array T as in A049800.",
			"comment": [
				"a(n) is the sum of the remainders after dividing each larger part by its corresponding smaller part for each partition of n+1 into two parts. - _Wesley Ivan Hurt_, Dec 20 2020"
			],
			"link": [
				"Lei Zhou, \u003ca href=\"/A049798/b049798.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Sum_{k=2..floor((n+1)/2)} ((n+1) mod k). - _Lei Zhou_, Mar 10 2014",
				"a(n) = A004125(n+1) - A008805(n-2), for n \u003e= 2. - _Carl Najafi_, Jan 31 2013",
				"a(n) = Sum_{i = 1..ceiling(n/2)} ((n-i+1) mod i). - _Wesley Ivan Hurt_, Jan 05 2017"
			],
			"example": [
				"From _Lei Zhou_, Mar 10 2014: (Start)",
				"For n = 3, n+1 = 4, floor((n+1)/2) = 2, mod(4,2) = 0, and so a(3) = 0.",
				"For n = 4, n+1 = 5, floor((n+1)/2) = 2, mod(5,2) = 1, and so a(4) = 1.",
				"...",
				"For n = 12, n+1 = 13, floor((n+1)/2) = 6, mod(13,2) = 1, mod(13,3) = 1, mod(13,4) = 1, mod(13,5) = 3, mod(13,6) = 1, and so a(12) = 1 + 1 + 1 + 3 + 1 = 7. (End)"
			],
			"maple": [
				"seq( add( (n+1) mod floor((k+1)/2), k=1..n)/2, n=1..60); # _G. C. Greubel_, Dec 09 2019"
			],
			"mathematica": [
				"Table[Sum[Mod[n+1, Floor[(k+1)/2]], {k,n}]/2, {n, 60}] (* _G. C. Greubel_, Dec 09 2019 *)"
			],
			"program": [
				"(Sage)",
				"def a(n):",
				"    return sum([(n+1)%k for k in range(2,floor((n+3)/2))])",
				"# _Ralf Stephan_, Mar 14 2014",
				"(PARI) vector(60, n, sum(k=1,n, lift(Mod(n+1, (k+1)\\2)) )/2 ) \\\\ _G. C. Greubel_, Dec 09 2019",
				"(MAGMA) [ (\u0026+[(n+1) mod Floor((k+1)/2): k in [1..n]])/2: n in [1..60]]; // _G. C. Greubel_, Dec 09 2019",
				"(GAP) List([1..60], n-\u003e Sum([1..n], k-\u003e (n+1) mod Int((k+1)/2))/2 ); # _G. C. Greubel_, Dec 09 2019"
			],
			"xref": [
				"Cf. A004125, A008611, A008805, A049797, A049799, A049801.",
				"Half row sums of A049800."
			],
			"keyword": "nonn,easy",
			"offset": "1,6",
			"author": "_Clark Kimberling_",
			"references": 6,
			"revision": 76,
			"time": "2020-12-20T22:25:03-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}