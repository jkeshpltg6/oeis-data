{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A099174",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 99174,
			"data": "1,0,1,1,0,1,0,3,0,1,3,0,6,0,1,0,15,0,10,0,1,15,0,45,0,15,0,1,0,105,0,105,0,21,0,1,105,0,420,0,210,0,28,0,1,0,945,0,1260,0,378,0,36,0,1,945,0,4725,0,3150,0,630,0,45,0,1,0,10395,0,17325,0,6930,0,990,0,55",
			"name": "Triangle read by rows: coefficients of modified Hermite polynomials.",
			"comment": [
				"Absolute values of A066325.",
				"T(n,k) is the number of involutions of {1,2,...,n}, having k fixed points (0 \u003c= k \u003c= n). Example: T(4,2)=6 because we have 1243,1432,1324,4231,3214 and 2134. - _Emeric Deutsch_, Oct 14 2006",
				"Riordan array [exp(x^2/2),x]. - _Paul Barry_, Nov 06 2008",
				"Same as triangle of Bessel numbers of second kind, B(n,k) (see Cheon et al., 2013). - _N. J. A. Sloane_, Sep 03 2013",
				"The modified Hermite polynomial h(n,x) (as in the Formula section) is the numerator of the rational function given by f(n,x) = x + (n-2)/f(n-1,x), where f(x,0) = 1. - _Clark Kimberling_, Oct 20 2014",
				"Second lower diagonal T(n,n-2) equals positive triangular numbers A000217 \\ {0}. - _M. F. Hasler_, Oct 23 2014",
				"From _James East_, Aug 17 2015: (Start)",
				"T(n,k) is the number of R-classes (equivalently, L-classes) in the D-class consisting of all rank k elements of the Brauer monoid of degree n.",
				"For n \u003c k with n == k (mod 2), T(n,k) is the rank (minimal size of a generating set) and idempotent rank (minimal size of an idempotent generating set) of the ideal consisting of all rank \u003c= k elements of the Brauer monoid. (End)",
				"This array provides the coefficients of a Laplace-dual sequence H(n,x) of the Dirac delta function, delta(x), and its derivatives, formed by taking the inverse Laplace transform of these modified Hermite polynomials. H(n,x) = h(n,D) delta(x) with h(n,x) as in the examples and the lowering and raising operators L = -x and R = -x + D = -x + d/dx such that L H(n,x) = n * H(n-1,x) and R H(n,x) = H(n+1,x). The e.g.f. is exp[t H(.,x)] = e^(t^2/2) e^(t D) delta(x) = e^(t^2/2) delta(x+t). - _Tom Copeland_, Oct 02 2016",
				"Antidiagonals of this entry are rows of A001497. - _Tom Copeland_, Oct 04 2016",
				"This triangle is the reverse of that in Table 2 on p. 7 of the Artioli et al. paper and Table 6.2 on p. 234 of Licciardi's thesis, with associations to the telephone numbers. - _Tom Copeland_, Jun 18 2018 and Jul 08 2018",
				"See A344678 for connections to a Heisenberg-Weyl algebra of differential operators, matching and independent edge sets of the regular n-simplices with partially labeled vertices, and telephone switchboard scenarios. - _Tom Copeland_, Jun 02 2021"
			],
			"link": [
				"M. Artioli, G. Dattoli, S. Licciardi, and S. Pagnutti, \u003ca href=\"https://arxiv.org/abs/1703.07262\"\u003eMotzkin Numbers: an Operational Point of View\u003c/a\u003e, arXiv:1703.07262 [math.CO], 2017.",
				"Paul Barry, \u003ca href=\"http://arxiv.org/abs/1102.0921\"\u003eRiordan array, orthogonal polynomials as moments, and Hankel transforms\u003c/a\u003e, arXiv:1102.0921 [math.CO], 2011.",
				"G.-S. Cheon, J.-H. Jung and L. W. Shapiro, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2013.05.001\"\u003eGeneralized Bessel numbers and some combinatorial settings\u003c/a\u003e, Discrete Math., 313 (2013), 2127-2138.",
				"T. Copeland, \u003ca href=\"https://tcjpn.wordpress.com/2020/07/15/juggling-zeros-in-the-matrix-example-ii/\"\u003eJuggling Zeros in the Matrix (Example II)\u003c/a\u003e, 2020.",
				"James East and Robert D. Gray, \u003ca href=\"http://arxiv.org/abs/1404.2359\"\u003eDiagram monoids and Graham-Houghton graphs: idempotents and generating sets of ideals\u003c/a\u003e, arXiv:1404.2359 [math.GR], 2014. See Theorem 8.4 and Table 7. - _James East_, Aug 17 2015",
				"A. Horzela, P. Blasiak, G. E. H. Duchamp, K. A. Penson and A. I. Solomon, \u003ca href=\"http://arXiv.org/abs/quant-ph/0409152\"\u003eA product formula and combinatorial field theory\u003c/a\u003e, arXiv:quant-ph/0409152, 2004.",
				"Alexander Kreinin, \u003ca href=\"http://arxiv.org/abs/1405.5852\"\u003eCombinatorial Properties of Mills' Ratio\u003c/a\u003e, arXiv:1405.5852 [math.CO], 2014. See Table 2. - _N. J. A. Sloane_, May 29 2014",
				"S. Licciardi, \u003ca href=\"https://arxiv.org/abs/1803.03108\"\u003eUmbral Calculus, a Different Mathematical Language\u003c/a\u003e, arXiv:1803.03108 [math.CA], 2018.",
				"R. Paris, \u003ca href=\"http://dx.doi.org/10.1016/S0377-0427(02)00553-8\"\u003eA uniform asymptotic expansion for the incomplete gamma function\u003c/a\u003e, Journal of Computational and Applied Mathematics, 148 (2002), p. 223-239 (See p. 329 and A137286. From _Tom Copeland_, Jan 03 2016).",
				"R. Sazdanovic, \u003ca href=\"http://www.math.toronto.edu/~drorbn/SK11/Sazdanovic.pdf \"\u003eA categorification of the polynomial ring\u003c/a\u003e, slide presentation, 2011",
				"S. Yang and Z. Qiao, \u003ca href=\"http://jmre.dlut.edu.cn/en/ch/reader/view_abstract.aspx?file_no=20110406\"\u003eThe Bessel numbers and Bessel matrices\u003c/a\u003e, Jrn. Math. Rsch. and Exposition, July 2011, Vol. 31, No. 4, pp.627-636. DOI:10.3770/j.issn:1000-341X.2011.04.006."
			],
			"formula": [
				"h(k, x) = (-I/sqrt(2))^k * H(k, I*x/sqrt(2)), H(n, x) the Hermite polynomials (A060821, A059343).",
				"T(n,k) = n!/(2^((n-k)/2)*((n-k)/2)!k!) if n-k \u003e= 0 is even; 0 otherwise. - _Emeric Deutsch_, Oct 14 2006",
				"G.f.: 1/(1-x*y-x^2/(1-x*y-2*x^2/(1-x*y-3*x^2/(1-x*y-4*x^2/(1-... (continued fraction). - _Paul Barry_, Apr 10 2009",
				"E.g.f.: exp(y*x + x^2/2). - _Geoffrey Critzer_, May 08 2012",
				"Recurrence: T(0,0)=1, T(0,k)=0 for k\u003e0 and for n \u003e= 1 T(n,k) = T(n-1,k-1) + (k+1)*T(n-1,k+1). - _Peter Luschny_, Oct 06 2012",
				"T(n+2,n) = A000217(n+1), n \u003e= 0. - _M. F. Hasler_, Oct 23 2014",
				"The row polynomials P(n,x) = (a. + x)^n, umbrally evaluated with (a.)^n = a_n = aerated A001147, are an Appell sequence with dP(n,x)/dx = n * P(n-1,x). The umbral compositional inverses (cf. A001147) of these polynomials are given by the same polynomials signed, A066325. - _Tom Copeland_, Nov 15 2014",
				"From _Tom Copeland_, Dec 13 2015: (Start)",
				"The odd rows are (2x^2)^n x n! L(n,-1/(2x^2),1/2), and the even, (2x^2)^n n! L(n,-1/(2x^2),-1/2) in sequence with n= 0,1,2,... and L(n,x,a) = Sum_{k=0..n} binomial(n+a,k+a) (-x)^k/k!, the associated Laguerre polynomial of order a. The odd rows are related to A130757, and the even to A176230 and A176231. Other versions of this entry are A122848, A049403, A096713 and A104556, and reversed A100861, A144299, A111924. With each non-vanishing diagonal divided by its initial element A001147(n), this array becomes reversed, aerated A034839.",
				"Create four shift and stretch matrices S1,S2,S3, and S4 with all elements zero except S1(2n,n) = 1 for n \u003e= 1, S2(n,2n) = 1 for n \u003e= 0, S3(2n+1,n) = 1 for n \u003e= 1, and S4(n,2n+1) = 1 for n \u003e= 0. Then this entry's lower triangular matrix is T = Id + S1 * (A176230-Id) * S2 + S3 * (unsigned A130757-Id) * S4 with Id the identity matrix. The sandwiched matrices have infinitesimal generators with the nonvanishing subdiagonals A000384(n\u003e0) and A014105(n\u003e0).",
				"As an Appell sequence, the lowering and raising operators are L = D and R = x + dlog(exp(D^2/2))/dD = x + D, where D = d/dx, L h(n,x) = n h(n-1,x), and R h(n,x) = h(n+1,x), so R^n 1 = h(n,x). The fundamental moment sequence has the e.g.f. e^(t^2/2) with coefficients a(n) = aerated A001147, i.e., h(n,x) = (a. + x)^n, as noted above. The raising operator R as a matrix acting on o.g.f.s (formal power series) is the transpose of the production matrix P below, i.e., (1,x,x^2,...)(P^T)^n (1,0,0,...)^T = h(n,x).",
				"For characterization as a Riordan array and associations to combinatorial structures, see the Barry link and the Yang and Qiao reference. For relations to projective modules, see the Sazdanovic link.",
				"(End)",
				"From the Appell formalism, e^(D^2/2) x^n = h_n(x), the n-th row polynomial listed below, and e^(-D^2/2) x^n = u_n(x), the n-th row polynomial of A066325. Then R = e^(D^2/2) * x * e^(-D^2/2) is another representation of the raising operator, implied by the umbral compositional inverse relation h_n(u.(x)) = x^n. - _Tom Copeland_, Oct 02 2016",
				"h_n(x) = p_n(x-1), where p_n(x) are the polynomials of A111062, related to the telephone numbers A000085. - _Tom Copeland_, Jun 26 2018",
				"From _Tom Copeland_, Jun 06 2021: (Start)",
				"In the power basis x^n, the matrix infinitesimal generator M = A132440^2/2, when acting on a row vector for an o.g.f., is the matrix representation for the differential operator D^2/2.",
				"e^{M} gives the coefficients of the Hermite polynomials of this entry.",
				"The only nonvanishing subdiagonal of M, the second subdiagonal (1,3,6,10,...), gives, aside from the initial 0, the triangular numbers A000217, the number of edges of the n-dimensional simplices with (n+1) vertices. The perfect matchings of these simplices are the aerated odd double factorials A001147 noted above, the moments for the Hermite polynomials.",
				"The polynomials are also generated from A036040 with x[1] = x, x[2] = 1, and the other indeterminates equal to zero. (End)"
			],
			"example": [
				"h(0,x) = 1",
				"h(1,x) = x",
				"h(2,x) = x^2 + 1",
				"h(3,x) = x^3 + 3*x",
				"h(4,x) = x^4 + 6*x^2 + 3",
				"h(5,x) = x^5 + 10*x^3 + 15*x",
				"h(6,x) = x^6 + 15*x^4 + 45*x^2 + 15",
				"From _Paul Barry_, Nov 06 2008: (Start)",
				"Triangle begins",
				"   1,",
				"   0,  1,",
				"   1,  0,  1,",
				"   0,  3,  0,  1,",
				"   3,  0,  6,  0,  1,",
				"   0, 15,  0, 10,  0,  1,",
				"  15,  0, 45,  0, 15,  0,  1",
				"Production array starts",
				"  0, 1,",
				"  1, 0, 1,",
				"  0, 2, 0, 1,",
				"  0, 0, 3, 0, 1,",
				"  0, 0, 0, 4, 0, 1,",
				"  0, 0, 0, 0, 5, 0, 1 (End)"
			],
			"maple": [
				"T:=proc(n,k) if n-k mod 2 = 0 then n!/2^((n-k)/2)/((n-k)/2)!/k! else 0 fi end: for n from 0 to 12 do seq(T(n,k),k=0..n) od; # yields sequence in triangular form; _Emeric Deutsch_, Oct 14 2006"
			],
			"mathematica": [
				"nn=10;a=y x+x^2/2!;Range[0,nn]!CoefficientList[Series[Exp[a],{x,0,nn}],{x,y}]//Grid  (* _Geoffrey Critzer_, May 08 2012 *)",
				"H[0, x_] = 1; H[1, x_] := x; H[n_, x_] := H[n, x] = x*H[n-1, x]-(n-1)* H[n-2, x]; Table[CoefficientList[H[n, x], x], {n, 0, 11}] // Flatten // Abs (* _Jean-François Alcover_, May 23 2016 *)",
				"T[ n_, k_] := If[ n \u003c 0, 0, Coefficient[HermiteH[n, x I/Sqrt[2]] (Sqrt[1/2]/I)^n, x, k]]; (* _Michael Somos_, May 10 2019 *)"
			],
			"program": [
				"(Sage)",
				"def A099174_triangle(dim):",
				"    M = matrix(ZZ,dim,dim)",
				"    for n in (0..dim-1): M[n,n] = 1",
				"    for n in (1..dim-1):",
				"        for k in (0..n-1):",
				"            M[n,k] = M[n-1,k-1]+(k+1)*M[n-1,k+1]",
				"    return M",
				"A099174_triangle(9)  # _Peter Luschny_, Oct 06 2012",
				"(PARI) T(n,k)=if(k\u003c=n \u0026\u0026 k==Mod(n,2), n!/k!/(k=(n-k)/2)!\u003e\u003ek) \\\\ _M. F. Hasler_, Oct 23 2014",
				"(Python)",
				"import sympy",
				"from sympy import Poly",
				"from sympy.abc import x, y",
				"def H(n, x): return 1 if n==0 else x if n==1 else x*H(n - 1, x) - (n - 1)*H(n - 2, x)",
				"def a(n): return [abs(cf) for cf in Poly(H(n, x), x).all_coeffs()[::-1]]",
				"for n in range(21): print(a(n)) # _Indranil Ghosh_, May 26 2017"
			],
			"xref": [
				"Row sums (polynomial values at x=1) are A000085.",
				"Polynomial values: A005425 (x=2), A202834 (x=3), A202879(x=4).",
				"Cf. A000217, A001147, A059343, A060821, A066325.",
				"Cf. A000384. A014105, A034839, A049403, A096713, A100861, A104556, A122848, A130757, A176230, A176231.",
				"Cf. A137286.",
				"Cf. A001497.",
				"Cf. A111062, A344678."
			],
			"keyword": "nonn,tabl",
			"offset": "0,8",
			"author": "_Ralf Stephan_, on a suggestion of _Karol A. Penson_, Oct 13 2004",
			"references": 21,
			"revision": 160,
			"time": "2021-06-07T09:51:00-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}