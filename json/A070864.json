{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A070864",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 70864,
			"data": "1,1,3,3,3,5,3,5,5,5,7,5,7,5,7,7,7,9,7,9,7,9,7,9,9,9,11,9,11,9,11,9,11,9,11,11,11,13,11,13,11,13,11,13,11,13,11,13,13,13,15,13,15,13,15,13,15,13,15,13,15,13,15,15,15,17,15,17,15,17,15,17,15,17,15,17,15,17,15",
			"name": "a(1) = a(2) = 1; a(n) = 2 + a(n - a(n-1)).",
			"reference": [
				"S. Wolfram, A New Kind of Science, Wolfram Media, 2002; p. 129."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A070864/b070864.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Nick Hobson, \u003ca href=\"/A070864/a070864.py.txt\"\u003ePython program for this sequence\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/WolframSequences.html\"\u003eWolfram Sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Ho#Hofstadter\"\u003eIndex entries for Hofstadter-type sequences\u003c/a\u003e"
			],
			"formula": [
				"Conjecture. Let a(1)=a(2)=1 and for n \u003e 2 let k = floor(sqrt(n+1))-1 and d=n-k(k+2). Then, if (d is 0, 1, or 2) OR (d=0 mod 2), a(n)=2k+1; otherwise a(n)=2k+3. This has been verified for n \u003c= 15000. Thus the asymptotic behavior appears to be a(n) ~ floor(sqrt(n+1)). - _John W. Layman_, May 21 2002",
				"By induction, a(1)=a(2)=1, a(3)=a(4)=a(5)=3 and for k \u003e= 3 we obtain the following formulas for the 2k-1 consecutive values from a(k^2-2k+2) up to a(k^2+1): a(k^2+1) = a(k^2) = 2k-1, if 1 \u003c= i \u003c= 2k-3 then a(k^2-i) = 2k-2-(-1)^i, hence asymptotically a(n) ~ 2*sqrt(n). - _Benoit Cloitre_, Jul 28 2002",
				"a(n) = 2*floor(n^(1/2)) + r where r is in {-1,1}. More precisely, let g(n) = round(sqrt(n)) - floor(sqrt(n+1)-1/sqrt(n+1)); then for n \u003e= 1 we get: a(2*n) = 2*floor(sqrt(2*n)) - 2*g(ceiling(n/2)) + 1 and something similar for a(2*n+1). - _Benoit Cloitre_, Mar 06 2009",
				"a(n) = 2*floor(n^(1/2)) - (-1)^(n + ceiling(n^(1/2))) for n \u003e 0. - Branko Curgus, Feb 10 2011"
			],
			"example": [
				"If k = 4, a(4^2+1) = a(17) = a(16) = 2*4 - 1 = 7, a(15) = 2*4 - 2 - (-1)^1 = 7, a(14) = 2*4 - 2 - (-1)^2 = 5, a(13)=7, a(12)=5, a(11)=7."
			],
			"mathematica": [
				"a[1] = a[2] = 1; a[n_] := a[n] = 2 + a[n - a[n - 1]]; Table[ a[n], {n, 1, 80}]"
			],
			"xref": [
				"Cf. A010000."
			],
			"keyword": "nonn,easy",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_, May 19 2002",
			"ext": [
				"More terms from _Jason Earls_, May 19 2002"
			],
			"references": 3,
			"revision": 33,
			"time": "2019-06-10T00:57:55-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}