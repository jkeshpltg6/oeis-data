{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A059867",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 59867,
			"data": "1,2,2,4,4,8,8,8,8,16,16,32,32,64,64,16,16,32,32,64,64,128,128,128,128,256,256,512,512,1024,1024,32,32,64,64,128,128,256,256,256,256,512,512,1024,1024,2048,2048,512,512,1024,1024,2048,2048,4096,4096,4096,4096",
			"name": "Number of irreducible representations of the symmetric group S_n that have odd degree.",
			"comment": [
				"Ayyer et al. (2016, 2016) obtain this sequence (which they call \"odd partitions\") as the number of partitions of n such that the dimension of the corresponding irreducible representation of S_n is odd."
			],
			"link": [
				"Eric M. Schmidt, \u003ca href=\"/A059867/b059867.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Arvind Ayyer, Amritanshu Prasad, Steven Spallone, \u003ca href=\"http://arxiv.org/abs/1601.01776\"\u003eOdd partitions in Young's lattice\u003c/a\u003e, arXiv:1601.01776 [math.CO], 2016.",
				"Arvind Ayyer, A. Prasad, S. Spallone, \u003ca href=\"http://arxiv.org/abs/1604.08837\"\u003eRepresentations of symmetric groups with non-trivial determinant\u003c/a\u003e, arXiv preprint arXiv:1604.08837 [math.RT], 2016. See Eq. (14).",
				"I. G. Macdonald, \u003ca href=\"https://doi.org/10.1112/blms/3.2.189\"\u003eOn the degrees of the irreducible representations of symmetric groups\u003c/a\u003e, Bulletin of the London Mathematical Society, 3(2):189-192, 1971.",
				"John McKay, \u003ca href=\"http://dx.doi.org/10.1016/0021-8693(72)90066-X\"\u003eIrreducible representations of odd degree\u003c/a\u003e, Journal of Algebra 20, 1972 pages 416-418.",
				"Igor Pak, Greta Panova, \u003ca href=\"https://doi.org/10.1016/j.laa.2020.05.005\"\u003eBounds on Kronecker coefficients via contingency tables\u003c/a\u003e, Linear Algebra and its Applications (2020), Vol. 602, 157-178."
			],
			"formula": [
				"If n = sum 2^e[i] in binary, then the number of odd degree irreducible complex representations of S_n is 2^sum e[i]. In words: write n in binary and take the product of the powers of 2 that appear.",
				"G.f.: prod(k\u003e=0, 1 + 2^k * x^2^k). a(n) = 2^A073642(n). - _Ralf Stephan_, Jun 02 2003",
				"a(1)=1, a(2n) = 2^e1(n)*a(n), a(2n+1) = a(2n), where e1(n) = A000120(n). - _Ralf Stephan_, Jun 19 2003"
			],
			"example": [
				"a(3) = 2 because S_3 the degrees of the irreducible representations of S_3 are 1,1,2."
			],
			"mathematica": [
				"a[n_] := 2^Total[Flatten[Position[Reverse[IntegerDigits[n, 2]], 1]] - 1];",
				"Array[a, 60] (* _Jean-François Alcover_, Jul 21 2018 *)"
			],
			"program": [
				"(Sage) def A059867(n) : dig = n.digits(2); return prod(2^n for n in range(len(dig)) if dig[n]==1) # _Eric M. Schmidt_, Apr 27 2013",
				"(PARI) A059867(n)={my(d=binary(n));prod(k=1,#d,if(d[#d+1-k],2^(k-1),1));} \\\\ _Joerg Arndt_, Apr 29 2013",
				"(PARI) a(n) = {my(b = Vecrev(binary(n))); 2^sum(k=1, #b, (k-1)*b[k]);} \\\\ _Michel Marcus_, Jan 11 2016"
			],
			"xref": [
				"Cf. A000120, A029930; A029931: the bisection of log_2(a(n)); A073642, A089248."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "Noam Katz (noamkj(AT)hotmail.com), Feb 28 2001",
			"ext": [
				"More terms from Larry Reeves (larryr(AT)acm.org), Mar 27 2001"
			],
			"references": 8,
			"revision": 31,
			"time": "2020-06-17T17:03:48-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}