{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A321127",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 321127,
			"data": "0,1,0,2,2,0,5,8,3,0,10,24,21,8,1,0,17,56,80,64,30,8,1,0,26,110,220,270,220,122,45,10,1,0,37,192,495,820,952,804,497,220,66,12,1,0,50,308,973,2030,3059,3472,3017,2004,1001,364,91,14,1",
			"name": "Irregular triangle read by rows: row n gives the coefficients in the expansion of ((x + 1)^(2*n) + (x^2 - 1)*(2*(x + 1)^n - 1))/x.",
			"comment": [
				"These are the coefficients of the Kauffman bracket polynomial evaluated at the shadow diagram of the two-bridge knot with Conway's notation C(n,n). Hence, T(n,k) gives the corresponding number of Kauffman states having exactly k circles."
			],
			"reference": [
				"Louis H. Kauffman, Formal Knot Theory, Princeton University Press, 1983."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A321127/b321127.txt\"\u003eTable of n, a(n) for n = 0..14282\u003c/a\u003e (rows 0 \u003c= n \u003c= 120, flattened).",
				"Louis H. Kauffman, \u003ca href=\"https://doi.org/10.1016/0040-9383(87)90009-7\"\u003eState models and the Jones polynomial\u003c/a\u003e, Topology Vol. 26 (1987), 395-407.",
				"Kelsey Lafferty, \u003ca href=\"https://scholar.rose-hulman.edu/rhumj/vol14/iss2/7/\"\u003eThe three-variable bracket polynomial for reduced, alternating links\u003c/a\u003e, Rose-Hulman Undergraduate Mathematics Journal Vol. 14 (2013), 98-113.",
				"Matthew Overduin, \u003ca href=\"https://www.math.csusb.edu/reu/OverduinPaper.pdf\"\u003eThe three-variable bracket polynomial for two-bridge knots\u003c/a\u003e, California State University REU, 2013.",
				"Franck Ramaharo, \u003ca href=\"https://arxiv.org/abs/1902.08989\"\u003eA generating polynomial for the two-bridge knot with Conway's notation C(n,r)\u003c/a\u003e, arXiv:1902.08989 [math.CO], 2019.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/2-bridge_knot\"\u003e2-bridge knot\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Bracket_polynomial\"\u003eBracket polynomial\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = 0 if k = 0, n^2 + 1 if k = 1, and C(2*n, k + 1) - 2*(C(n, k + 1) + C(n, k - 1)) otherwise.",
				"T(n,1) = A002522(n).",
				"T(n,2) = A300401(n,n).",
				"T(n,n) = A001791(n) + A005843(n) - A063524(n).",
				"T(n,k) = A094527(n,k-n+1) if  n + 1 \u003c k \u003c 2*n  and n \u003e 2.",
				"G.f.: x*(1 - (1 + x + x^2)*y + (1 + x)*(2 - x^2)*y^2)/((1 - y)*(1 - y - x*y)*(1 - (1 + x)^2*y)).",
				"E.g.f.: (exp((1 + x)^2*y) - (exp(x) + 2*exp((1 + x)*y))*(1 - x^2))/x."
			],
			"example": [
				"Triangle begins:",
				"n\\k | 0   1    2    3    4    5    6    7    8   9  11 12",
				"----+----------------------------------------------------",
				"  0 | 0   1",
				"  1 | 0   2    2",
				"  2 | 0   5    8    3",
				"  3 | 0  10   24   21    8    1",
				"  4 | 0  17   56   80   64   30    8    1",
				"  5 | 0  26  110  220  270  220  122   45   10   1",
				"  6 | 0  37  192  495  820  952  804  497  220  66  12  1",
				"  ..."
			],
			"mathematica": [
				"row[n_] := CoefficientList[Expand[((x + 1)^(2*n) + (x^2 - 1)*(2*(x + 1)^n - 1))/x], x]; Array[row, 12, 0] // Flatten"
			],
			"program": [
				"(Maxima) T(n, k) := if k = 1 then n^2 + 1 else  ((4*k - 2*n)/(k + 1))*binomial(n + 1, k) + binomial(2*n, k + 1)$",
				"create_list(T(n, k), n, 0, 12, k, 0, max(2*n - 1, n + 1));"
			],
			"xref": [
				"Row sums: A000302.",
				"Row 1 is row 2 in A300453.",
				"Row 2 is also row 2 in A300454 and A316659.",
				"Cf. A299989, A300184, A300192, A316989."
			],
			"keyword": "nonn,easy,tabf",
			"offset": "0,4",
			"author": "_Franck Maminirina Ramaharo_, Nov 19 2018",
			"references": 3,
			"revision": 12,
			"time": "2019-04-10T21:55:17-04:00",
			"created": "2018-11-24T01:27:37-05:00"
		}
	]
}