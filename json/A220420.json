{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A220420",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 220420,
			"data": "1,2,1,4,1,0,1,14,1,-4,1,-8,1,-16,1,196,1,-54,1,-92,1,-184,1,144,1,-628,1,-1040,1,-2160,1,41102,1,-7708,1,-12932,1,-27592,1,54020,1,-98496,1,-173720,1,-364720,1,853624,1,-1341970,1,-2383916,1,-4918536,1",
			"name": "Express the Sum_{n\u003e=0} p(n)*x^n, where p(n) is the partition function, as a product Product_{k\u003e=1} (1 + a(k)*x^k).",
			"comment": [
				"This is the PPE (power product expansion) of A000041.",
				"When n is odd, a(n) = 1.",
				"When n is even, a(n) = 2, 4, 0, 14, -4, -8, -16, 196, -54, -92, -184, 144, -628, -1040, -2160, 41102, ...",
				"Alkauskas (2016, Problem 3, p. 3) conjectured that a(8*k+2), a(8*k+4), and a(8*k+6) are all negative, and a(8*k) is positive for k \u003e= 1. [This statement is not wholly true for k = 0.] - _Petros Hadjicostas_, Oct 07 2019"
			],
			"link": [
				"Giedrius Alkauskas, \u003ca href=\"http://arxiv.org/abs/0801.0805\"\u003eOne curious proof of Fermat's little theorem\u003c/a\u003e, arXiv:0801.0805 [math.NT], 2008.",
				"Giedrius Alkauskas, \u003ca href=\"https://www.jstor.org/stable/40391097\"\u003eA curious proof of Fermat's little theorem\u003c/a\u003e, Amer. Math. Monthly 116(4) (2009), 362-364.",
				"Giedrius Alkauskas, \u003ca href=\"https://arxiv.org/abs/1609.09842\"\u003eAlgebraic functions with Fermat property, eigenvalues of transfer operator and Riemann zeros, and other open problems\u003c/a\u003e, arXiv:1609.09842 [math.NT], 2016.",
				"H. Gingold, H. W. Gould, and Michael E. Mays, \u003ca href=\"https://www.researchgate.net/publication/268023169_Power_product_expansions\"\u003ePower Product Expansions\u003c/a\u003e, Utilitas Mathematica 34 (1988), 143-161.",
				"H. Gingold and A. Knopfmacher, \u003ca href=\"https://doi.org/10.4153/CJM-1995-062-9\"\u003eAnalytic properties of power product expansions\u003c/a\u003e, Canadian Journal of Mathematics 47(6) (1995), 1219-1239.",
				"W. Lang, \u003ca href=\"/A157162/a157162.txt\"\u003eRecurrences for the general problem\u003c/a\u003e."
			],
			"formula": [
				"From _Petros Hadjicostas_, Oct 04 2019: (Start)",
				"Define (A(m,n): n,m \u003e= 1) by A(m=1,n) = p(n) = A000041(n) for n \u003e= 1, A(m,n) = 0 for m \u003e n \u003e= 1 (upper triangular), and A(m,n) = A(m-1,n) - A(m-1,m-1) * A(m,n-m+1) for n \u003e= m \u003e= 2. Then a(n) = A(n,n). [Theorem 3 in Gingold et al. (1988).]",
				"a(n) = Sum_{s|n} s/n + Sum_{s|n, s \u003e 1} (-a(n/s))^s/s. [Eq. (1) in Alkauskas (2008, 2009).]",
				"(End)"
			],
			"mathematica": [
				"terms = 55; sol[0] = {};",
				"sol[m_] := sol[m] = Join[sol[m - 1], If[OddQ[m], {a[m] -\u003e 1}, First @ Solve[Thread[Table[PartitionsP[n], {n, 0, m}] == CoefficientList[ (Product[1 + a[n]*x^n, {n, 1, m}] /. sol[m - 1]) + O[x]^(m + 1), x]]]]];",
				"Array[a, terms] /. sol[terms] (* _Jean-François Alcover_, Dec 06 2018, corrected Oct 03 2019 *)",
				"(* Second program: *)",
				"A[m_, n_] := A[m, n] = Which[m == 1, PartitionsP[n], m \u003e n \u003e= 1, 0, True, A[m - 1, n] - A[m - 1, m - 1]*A[m, n - m + 1] ];",
				"a[n_] := A[n, n];",
				"a /@ Range[1, 55] (* _Jean-François Alcover_, Oct 03 2019, using the formula given by _Petros Hadjicostas_ *)"
			],
			"program": [
				"(PARI) a(m) = {default(seriesprecision, m+1); ak = vector(m); pol = 1 / eta(x + x * O(x^m)); ak[1] = polcoeff(pol, 1); for (k=2, m, pol = taylor(pol / (1+ak[k-1]*x^(k-1)), x); ak[k] = polcoeff(pol, k, x);); for (k=1, m, print1(ak[k], \", \"););}"
			],
			"xref": [
				"Cf. A000041, A147541, A170908, A170909, A170910, A170911, A170912, A170913, A170914, A170915, A170916, A170917, A220418, A290261."
			],
			"keyword": "sign",
			"offset": "1,2",
			"author": "_Michel Marcus_, Dec 14 2012",
			"references": 6,
			"revision": 54,
			"time": "2019-10-07T02:42:31-04:00",
			"created": "2012-12-17T11:46:42-05:00"
		}
	]
}