{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A121412",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 121412,
			"data": "1,1,1,3,1,1,18,4,1,1,170,30,5,1,1,2220,335,45,6,1,1,37149,4984,581,63,7,1,1,758814,92652,9730,924,84,8,1,1,18301950,2065146,199692,17226,1380,108,9,1,1,508907970,53636520,4843125,387567,28365,1965,135,10,1,1",
			"name": "Triangular matrix T, read by rows, where row n of T equals row (n-1) of T^(n+1) with an appended '1'.",
			"comment": [
				"Related to the number of subpartitions of a partition as defined in A115728; for examples involving column k of successive matrix powers, see A121430, A121431, A121432 and A121433. Essentially the same as triangle A101479, but this form best illustrates the nice properties of this triangle."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A121412/b121412.txt\"\u003eRows n = 0..45, flattened\u003c/a\u003e"
			],
			"formula": [
				"G.f.: Column k of successive powers of T satisfy the amazing relation given by: 1 = Sum_{n\u003e=0}(1-x)^(n+1)*x^(n(n+1)/2+k*n)*Sum_{j=0..n+k}[T^(j+1)](n+k,k)*x^j."
			],
			"example": [
				"Triangle T begins:",
				"1;",
				"1, 1;",
				"3, 1, 1;",
				"18, 4, 1, 1;",
				"170, 30, 5, 1, 1;",
				"2220, 335, 45, 6, 1, 1;",
				"37149, 4984, 581, 63, 7, 1, 1;",
				"758814, 92652, 9730, 924, 84, 8, 1, 1;",
				"18301950, 2065146, 199692, 17226, 1380, 108, 9, 1, 1;",
				"508907970, 53636520, 4843125, 387567, 28365, 1965, 135, 10, 1, 1;",
				"To get row 4 of T, append '1' to row 3 of matrix power T^5:",
				"1;",
				"5, 1;",
				"25, 5, 1;",
				"170, 30, 5, 1; ...",
				"To get row 5 of T, append '1' to row 4 of matrix power T^6:",
				"1;",
				"6, 1;",
				"33, 6, 1;",
				"233, 39, 6, 1;",
				"2220, 335, 45, 6, 1; ...",
				"Likewise, get row n of T by appending '1' to row (n-1) of T^(n+1)."
			],
			"mathematica": [
				"T[n_, k_] := Module[{A = {{1}}, B}, Do[B = Array[0\u0026, {m, m}]; Do[Do[B[[i, j]] = If[j == i, 1, MatrixPower[A, i][[i-1, j]]], {j, 1, i}], {i, 1, m}]; A = B, {m, 1, n+1}]; A[[n+1, k+1]]];",
				"Table[T[n, k], {n, 0, 9}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Oct 03 2019 *)"
			],
			"program": [
				"(PARI) {T(n, k)=local(A=Mat(1), B); for(m=1, n+1, B=matrix(m, m); for(i=1, m, for(j=1, i, if(j==i, B[i, j]=1, B[i, j]=(A^i)[i-1, j]); )); A=B); return((A^1)[n+1, k+1])}"
			],
			"xref": [
				"Cf. A121416 (T^2), A121420 (T^3), columns: A121413, A121414, A121415; related tables: A121424, A121426, A121428; related subpartitions: A121430, A121431, A121432, A121433."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_Paul D. Hanna_, Jul 30 2006",
			"references": 30,
			"revision": 11,
			"time": "2019-10-03T10:01:56-04:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}