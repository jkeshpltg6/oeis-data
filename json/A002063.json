{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002063",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2063,
			"data": "9,36,144,576,2304,9216,36864,147456,589824,2359296,9437184,37748736,150994944,603979776,2415919104,9663676416,38654705664,154618822656,618475290624,2473901162496,9895604649984,39582418599936,158329674399744,633318697598976",
			"name": "a(n) = 9*4^n.",
			"comment": [
				"a(n) is twice the area of the trapezoid created by the four points (2^n,2^(n+1)), (2^(n+1), 2^n), (2^(n+1), 2^(n+2)), (2^(n+2), 2^(n+1)). - _J. M. Bergot_, May 23 2014",
				"These are squares that can be expressed as sum of exactly two distinct powers of two. For instance, a(4) = 9*4^4 = 2304 = 2^11 + 2^8 . It is conjectured that these are the only squares with this characteristic (tested on squares up to (10^7)^2). - _Andres Cicuttin_, Apr 23 2016",
				"Conjecture is true. It is equivalent to prove that the Diophantine equation m^2 = 2^k*(1+2^h), where h\u003e0, has solutions only when h=3. Dividing by 2^k we must obtain an odd square on the left, since 1+2^h is odd, so we can write (2*r+1)^2 = 1+2^h. Expanding, we have 4*r*(r+1) = 2^h, from which it follows that r must be equal to 1 and thus h=3, since r and r+1 must be powers of 2. - _Giovanni Resta_, Jul 27 2017"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A002063/b002063.txt\"\u003eTable of n, a(n) for n = 0..500\u003c/a\u003e",
				"Milan Janjic, \u003ca href=\"http://www.pmfbl.org/janjic/\"\u003eEnumerative Formulas for Some Functions on Finite Sets\u003c/a\u003e",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_01\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4)."
			],
			"formula": [
				"From _Philippe Deléham_, Nov 23 2008: (Start)",
				"a(n) = 4*a(n-1), n \u003e 0; a(0)=9.",
				"G.f.: 9/(1-4*x). (End)",
				"a(n) = 9*A000302(n). - _Michel Marcus_, Apr 23 2016",
				"E.g.f.: 9*exp(4*x). - _Ilya Gutkovskiy_, Apr 23 2016",
				"a(n) = 2^(2*n+3) + 2^(2*n). - _Andres Cicuttin_, Apr 26 2016",
				"a(n) = A004171(n+1) + A000302(n). - _Zhandos Mambetaliyev_, Nov 19 2016"
			],
			"mathematica": [
				"9*4^Range[0, 100] (* _Vladimir Joseph Stephan Orlovsky_, Jun 09 2011 *)",
				"NestList[4#\u0026,9,30] (* _Harvey P. Dale_, Jan 15 2019 *)"
			],
			"program": [
				"(MAGMA) [9*4^n: n in [0..30]]; // _Vincenzo Librandi_, May 19 2011",
				"(PARI) a(n)=9\u003c\u003cn \\\\ _Charles R Greathouse IV_, Apr 17 2012"
			],
			"xref": [
				"Essentially the same as A055841. First differences of A002001.",
				"Cf. A000302."
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_",
			"references": 22,
			"revision": 92,
			"time": "2019-01-15T16:12:27-05:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}