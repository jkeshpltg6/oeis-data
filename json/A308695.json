{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A308695",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 308695,
			"data": "1,2,1,8,4,2,1,128,64,32,16,8,4,2,1,6300,3150,26,13,579,1069378,534689,10,5,387304,193652,96826,48413,141015,298082,149041,2958,1479",
			"name": "a(n) is the minimum positive integer m such that m * 2^(n + 2) + 1 is a prime number which does not divide ((F(n + 2) - 1)^m - 1)/(F(n + 2) - 2), where F(n) is the n-th Fermat number (A000215).",
			"comment": [
				"Note that some terms are obtained by dividing the previous one by 2.",
				"From _Jinyuan Wang_, Feb 18 2020: (Start)",
				"a(n) is the least m such that q = m*2^(n + 2) + 1 is a prime factor of F(n + 2) - 2. Proof: if r(n + 2)/s(n + 2) = ((F(n + 2) - 1)^m - 1)/(F(n + 2) - 2) is not divisible by q, then q divides s(n + 2) because r(n + 2) is always divisible by q (by Fermat's little theorem). Also note that if F(n + 2) - 1 == 1 (mod q), then r(n + 2)/s(n + 2) = Sum_{i = 0..m-1} A001146(n + 2)^i == m (mod q). In conclusion, prime q = m*2^(n + 2) + 1 does not divide r(n + 2)/s(n + 2) if and only if q divides F(n + 2) - 2 = Product_{i = 0..n + 1} F(i).",
				"a(n) always exists because prime factors of F(n) are of the form k*2^(n + 2) + 1. a(n) is not greater than the smallest such k. (End)"
			],
			"link": [
				"Lorenzo Sauras Altuzarra, \u003ca href=\"https://arxiv.org/abs/2002.03075\"\u003eSome arithmetical problems that are obtained by analyzing proofs and infinite graphs\u003c/a\u003e, arXiv:2002.03075 [math.NT], 2020."
			],
			"example": [
				"2 is the minimum positive integer m such that m * 2^(1 + 2) + 1 is a prime number (note that 2 * 2^(1 + 2) + 1 = 17) which does not divide ((F(1 + 2) - 1)^m - 1)/(F(1 + 2) - 2) (note that ((F(1 + 2) - 1)^2 - 1)/(F(1 + 2) - 2) = 257, which is a prime number)."
			],
			"maple": [
				"A308695:=proc(n)",
				"   local m:",
				"   m:=1:",
				"   while not isprime(m*2^(n+2)+1) or (2^(2^(n+2))-1) mod (m*2^(n+2)+1) != 0 do",
				"      m:=m+1:",
				"   od:",
				"   return m:",
				"end proc:"
			],
			"mathematica": [
				"Array[Block[{m = 1}, While[Nand[PrimeQ[#4], Mod[((#3 - 1)^#1 - 1)/(#3 - 2), #4] != 0] \u0026 @@ {m, #, 2^(2^(# + 2)) + 1, m*2^(# + 2) + 1}, m++]; m] \u0026, 14] (* _Michael De Vlieger_, Feb 14 2020 *)"
			],
			"program": [
				"(PARI) F(n) = 2^(2^n) + 1;",
				"a(n) = {my(m=1); while (!isprime(p=(m*2^(n+2)+1)) || !((((F(n+2)-1)^m-1)/ (F(n+2)-2)) % p), m++); m;} \\\\ _Michel Marcus_, Feb 14 2020",
				"(PARI) a(n) = {my(d=4*2^n, q=1); for(m=1, oo, q+=d; if(ispseudoprime(q) \u0026\u0026 Mod(2, q)^d==1, return(m))); } \\\\ _Jinyuan Wang_, Feb 18 2020"
			],
			"xref": [
				"Cf. A000215 (Fermat numbers), A001146."
			],
			"keyword": "nonn,more",
			"offset": "0,2",
			"author": "_Lorenzo Sauras Altuzarra_, Feb 11 2020",
			"ext": [
				"a(15)-a(32) from _Jinyuan Wang_, Feb 18 2020"
			],
			"references": 2,
			"revision": 73,
			"time": "2020-03-03T14:18:35-05:00",
			"created": "2020-03-03T14:18:35-05:00"
		}
	]
}