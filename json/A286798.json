{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A286798",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 286798,
			"data": "1,1,4,2,27,22,248,264,30,2830,3610,830,8,38232,55768,18746,1078,593859,961740,414720,46986,576,10401712,18326976,9457788,1593664,62682,112,202601898,382706674,226526362,49941310,3569882,45296,4342263000,8697475368,5740088706,1540965514,160998750,4909674,16896,101551822350,213865372020,154271354280,48205014786,6580808784,337737294,4200032,2560",
			"name": "Triangle T(n,k) read by rows: coefficients of polynomials P_n(t) defined in Formula section. .",
			"comment": [
				"Row n\u003e0 contains floor(2*(n+1)/3) terms."
			],
			"link": [
				"Gheorghe Coserea, \u003ca href=\"/A286798/b286798.txt\"\u003eRows n=0..123, flattened\u003c/a\u003e",
				"Luca G. Molinari, Nicola Manini, \u003ca href=\"https://arxiv.org/abs/cond-mat/0512342\"\u003eEnumeration of many-body skeleton diagrams\u003c/a\u003e, arXiv:cond-mat/0512342 [cond-mat.str-el], 2006."
			],
			"formula": [
				"y(x;t) = Sum_{n\u003e=0} P_n(t)*x^n satisfies x^2*deriv(y,x) = (1 - y + x*y^2 + 2*x^2*t*y^3)/(t - (2+t)*y - 3*x*t*y^2), with y(0;t) = 1, where P_n(t) = Sum_{k=0..floor((2*n-1)/3)} T(n,k)*t^k for n\u003e0.",
				"A000699(n+1)=T(n,0), A000108(n)=P_n(-1), A286799(n)=P_n(1)."
			],
			"example": [
				"A(x;t) = 1 + x + (4 + 2*t)*x^2 + (27 + 22*t)*x^3 + (248 + 264*t + 30*t^2)*x^4 +",
				"Triangle starts:",
				"n\\k  [0]        [1]        [2]        [3]       [4]      [5]",
				"[0]  1;",
				"[1]  1;",
				"[2]  4,         2;",
				"[3]  27,        22;",
				"[4]  248,       264,       30;",
				"[5]  2830,      3610,      830,       8;",
				"[6]  38232,     55768,     18746,     1078;",
				"[7]  593859,    961740,    414720,    46986,    576;",
				"[8]  10401712,  18326976,  9457788,   1593664,  62682,   112;",
				"[9]  202601898, 382706674, 226526362, 49941310, 3569882, 45296;",
				"[10] ..."
			],
			"mathematica": [
				"max = 12; y0[x_, t_] = 1; y1[x_, t_] = 0; For[n = 1, n \u003c= max, n++, y1[x_, t_] = 1 + x y0[x, t]^2 + 3 t x^3 y0[x, t]^2 D[y0[x, t], x] + x^2 (2 y0[x, t] D[y0[x, t], x] + t (2 y0[x, t]^3 - D[y0[x, t], x] + y0[x, t] D[y0[x, t], x])) + O[x]^n // Normal // Simplify; y0[x_, t_] = y1[x, t]];",
				"P[n_, t_] := Coefficient[y0[x, t] , x, n];",
				"row[n_] := CoefficientList[P[n, t], t];",
				"Table[row[n], {n, 0, max}] // Flatten (* _Jean-François Alcover_, May 24 2017, adapted from PARI *)"
			],
			"program": [
				"(PARI)",
				"A286795_ser(N, t='t) = {",
				"  my(x='x+O('x^N), y0=1, y1=0, n=1);",
				"  while(n++,",
				"    y1 = (1 + x*(1 + 2*t + x*t^2)*y0^2 + t*(1-t)*x^2*y0^3 + 2*x^2*y0*y0');",
				"    y1 = y1 / (1+2*x*t); if (y1 == y0, break()); y0 = y1;); y0;",
				"};",
				"A286798_ser(N,t='t) = {",
				"  my(v = A286795_ser(N,t)); subst(v, 'x, serreverse(x/(1-x*t*v)));",
				"};",
				"concat(apply(p-\u003eVecrev(p), Vec(A286798_ser(12))))",
				"\\\\ test: y=A286798_ser(50); x^2*y' == (1 - y + x*y^2 + 2*x^2*t*y^3)/(t - (2+t)*y - 3*x*t*y^2)"
			],
			"xref": [
				"Cf. A286781, A286782, A286783, A286784, A286785."
			],
			"keyword": "nonn,tabf",
			"offset": "0,3",
			"author": "_Gheorghe Coserea_, May 21 2017",
			"references": 4,
			"revision": 19,
			"time": "2017-05-26T22:18:24-04:00",
			"created": "2017-05-26T22:18:24-04:00"
		}
	]
}