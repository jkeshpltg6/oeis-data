{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005179",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5179,
			"id": "M1026",
			"data": "1,2,4,6,16,12,64,24,36,48,1024,60,4096,192,144,120,65536,180,262144,240,576,3072,4194304,360,1296,12288,900,960,268435456,720,1073741824,840,9216,196608,5184,1260,68719476736,786432,36864,1680,1099511627776,2880",
			"name": "Smallest number with exactly n divisors.",
			"comment": [
				"A number n is called ordinary iff a(n)=A037019(n). Brown shows that the ordinary numbers have density 1 and all squarefree numbers are ordinary. See A072066 for the extraordinary or exceptional numbers. - _M. F. Hasler_, Oct 14 2014",
				"Subsequence of A025487. Therefore, a(n) is even for n \u003e 1. - _David A. Corneth_, Jun 23 2017"
			],
			"reference": [
				"M. Abramowitz and I. A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, 1964 (and various reprintings), p. 840.",
				"L. E. Dickson, History of the Theory of Numbers. Carnegie Institute Public. 256, Washington, DC, Vol. 1, 1919; Vol. 2, 1920; Vol. 3, 1923, see vol. 1, p. 52.",
				"J. Roberts, Lure of the Integers, Math. Assoc. America, 1992, p. 86.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Don Reble, \u003ca href=\"/A005179/b005179.txt\"\u003eTable of n, a(n) for n = 1..2000\u003c/a\u003e",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"R. Brown, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2005.04.004\"\u003eThe minimal number with a given number of divisors\u003c/a\u003e, Journal of Number Theory 116 (2006) 150-158.",
				"M. E. Grost, \u003ca href=\"http://www.jstor.org/stable/2315183\"\u003eThe smallest number with a given number of divisors\u003c/a\u003e, Amer. Math. Monthly, 75 (1968), 725-729.",
				"J. Roberts, \u003ca href=\"/A007415/a007415.pdf\"\u003eLure of the Integers\u003c/a\u003e, Annotated scanned copy of pp. 81, 86 with notes.",
				"Anna K. Savvopoulou and Christopher M. Wedrychowicz, \u003ca href=\"http://dx.doi.org/10.1007/s11139-014-9572-9\"\u003eOn the smallest number with a given number of divisors\u003c/a\u003e, The Ramanujan Journal, 2015, Vol. 37, pp. 51-64.",
				"David Singmaster, \u003ca href=\"/A005178/a005178.pdf\"\u003eLetter to N. J. A. Sloane\u003c/a\u003e, Oct 3 1982.",
				"T. Verhoeff, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/trapzoid.html\"\u003eRectangular and Trapezoidal Arrangements\u003c/a\u003e, J. Integer Sequences, Vol. 2, 1999, #99.1.6.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Divisor.html\"\u003eDivisor\u003c/a\u003e",
				"R. G. Wilson v, \u003ca href=\"/A005179/a005179.pdf\"\u003eLetter to N. J. A. Sloane\u003c/a\u003e, Dec 17 1991."
			],
			"formula": [
				"a(p) = 2^(p-1) for primes p: a(A000040(n)) = A061286(n); a(p^2) = 6^(p-1) for primes p: a(A001248(n)) = A061234(n); a(p*q) = 2^(q-1)*3^(p-1) for primes p\u003c=q: a(A001358(n)) = A096932(n); a(p*m*q) = 2^(q-1) * 3^(m-1) * 5^(p-1) for primes p\u003cm\u003cq: A005179(A007304(n)) = A061299(n). - _Reinhard Zumkeller_, Jul 15 2004",
				"a(p^n) = (2*3...*p_n)^(p-1) for p \u003e log p_n / log 2. Unpublished proof from Andrzej Schinzel. - _Thomas Ordowski_, Jul 22 2005",
				"If p is a prime and n=p^k then a(p^k)=(2*3*...*s_k)^(p-1) where (s_k) is the numbers of the form q^(p^j) for every q and j\u003e=0, according to Grost (1968), Theorem 4. For example, if p=2 then a(2^k) is the product of the first k members of the A050376 sequence: number of the form q^(2^j) for j\u003e=0, according to Ramanujan (1915). - _Thomas Ordowski_, Aug 30 2005",
				"a(2^k) = A037992(k). - _Thomas Ordowski_, Aug 30 2005"
			],
			"maple": [
				"A005179_list := proc(SearchLimit, ListLength)",
				"local L, m, i, d; m := 1;",
				"L := array(1..ListLength,[seq(0,i=1..ListLength)]);",
				"for i from 1 to SearchLimit while m \u003c= ListLength do",
				"  d := numtheory[tau](i);",
				"  if d \u003c= ListLength and 0 = L[d] then L[d] := i;",
				"  m := m + 1; fi",
				"od:",
				"print(L) end: A005179_list(65537,18);",
				"# If a '0' appears in the list the search limit has to be increased. - _Peter Luschny_, Mar 09 2011"
			],
			"mathematica": [
				"a = Table[ 0, {43} ]; Do[ d = Length[ Divisors[ n ]]; If[ d \u003c 44 \u0026\u0026 a[[ d ]] == 0, a[[ d]] = n], {n, 1, 1099511627776} ]; a",
				"(* Second program: *)",
				"Function[s, Map[Lookup[s, #] \u0026, Range[First@ Complement[Range@ Max@ #, #] - 1]] \u0026@ Keys@ s]@ Map[First, KeySort@ PositionIndex@ Table[DivisorSigma[0, n], {n, 10^7}]] (* _Michael De Vlieger_, Dec 11 2016, Version 10 *)",
				"mp[1, m_] := {{}}; mp[n_, 1] := {{}}; mp[n_?PrimeQ, m_] := If[m \u003c n, {}, {{n}}]; mp[n_, m_] := Join @@ Table[Map[Prepend[#, d] \u0026, mp[n/d, d]], {d, Select[Rest[Divisors[n]], # \u003c= m \u0026]}]; mp[n_] := mp[n, n]; Table[mulpar = mp[n] - 1; Min[Table[Product[Prime[s]^mulpar[[j, s]], {s, 1, Length[mulpar[[j]]]}], {j, 1, Length[mulpar]}]], {n, 1, 100}] (* _Vaclav Kotesovec_, Apr 04 2021 *)"
			],
			"program": [
				"(PARI) {(prodR(n,maxf)=my(dfs=divisors(n),a=[],r); for(i=2,#dfs, if( dfs[i]\u003c=maxf, if(dfs[i]==n, a=concat(a,[[n]]), r=prodR(n/dfs[i],min(dfs[i],maxf)); for(j=1,#r, a=concat(a,[concat(dfs[i],r[j])]))))); a); A005179(n)=my(pf=prodR(n,n),a=1,b); for(i=1,#pf, b=prod(j=1,length(pf[i]),prime(j)^(pf[i][j]-1)); if(b\u003ca || i==1, a=b)); a}",
				"for(n=1,100, print1(A005179(n)\", \")) \\\\ _R. J. Mathar_, May 26 2008, edited by _M. F. Hasler_, Oct 11 2014",
				"(Haskell)",
				"import Data.List (elemIndex)",
				"import Data.Maybe (fromJust)",
				"a005179 n = succ $ fromJust $ elemIndex n $ map a000005 [1..]",
				"-- _Reinhard Zumkeller_, Apr 01 2011"
			],
			"xref": [
				"Cf. A000005, A007416, A099316, A003586, A025487, A099311, A099313, A050376, A037992, A061799, A262981, A262983."
			],
			"keyword": "nonn,nice,easy",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, David Singmaster",
			"ext": [
				"More terms from _David W. Wilson_"
			],
			"references": 155,
			"revision": 100,
			"time": "2021-04-04T04:34:22-04:00",
			"created": "1991-05-16T03:00:00-04:00"
		}
	]
}