{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A087097",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 87097,
			"data": "19,29,39,49,59,69,79,89,90,91,92,93,94,95,96,97,98,99,109,209,219,309,319,329,409,419,429,439,509,519,529,539,549,609,619,629,639,649,659,709,719,729,739,749,759,769,809,819,829,839,849,859,869,879,901,902,903,904,905,906,907,908,909,912,913,914,915,916,917,918,919,923,924,925,926,927,928,929,934,935,936,937,938,939,945,946,947,948,949,956,957,958,959,967,968,969,978,979,989",
			"name": "Lunar primes (formerly called dismal primes) (cf. A087062).",
			"comment": [
				"9 is the multiplicative unit. A number is a lunar prime if it is not a lunar product (see A087062 for definition) r*s where neither r nor s is 9.",
				"All lunar primes must contain a 9, so this is a subsequence of A011539.",
				"Also, numbers k such that the lunar sum of the lunar prime divisors of k is k. - _N. J. A. Sloane_, Aug 23 2010",
				"We have changed the name from \"dismal arithmetic\" to \"lunar arithmetic\" - the old name was too depressing. - _N. J. A. Sloane_, Aug 06 2014",
				"(Lunar) composite numbers are not necessarily a product of primes. (For example 1 = 1*x for any x in {1, ..., 9} is not a prime but can't be written as the product of primes.) Therefore, to establish primality, it is not sufficient to consider only products of primes; one has to consider possible products of composite numbers as well. - _M. F. Hasler_, Nov 16 2018"
			],
			"link": [
				"David Applegate and N. J. A. Sloane, \u003ca href=\"/A087097/b087097.txt\"\u003eTable of n, a(n) for n = 1..22095\u003c/a\u003e [all primes with at most 5 digits]",
				"D. Applegate, \u003ca href=\"/A087061/a087061.txt\"\u003eC program for lunar arithmetic and number theory\u003c/a\u003e",
				"D. Applegate, M. LeBrun and N. J. A. Sloane, \u003ca href=\"http://arxiv.org/abs/1107.1130\"\u003eDismal Arithmetic\u003c/a\u003e, arXiv:1107.1130 [math.NT], 2011.",
				"Brady Haran and N. J. A. Sloane, \u003ca href=\"https://youtu.be/cZkGeR9CWbk\"\u003ePrimes on the Moon (Lunar Arithmetic)\u003c/a\u003e, Numberphile video, Nov 2018.",
				"\u003ca href=\"/index/Di#dismal\"\u003eIndex entries for sequences related to dismal (or lunar) arithmetic\u003c/a\u003e"
			],
			"formula": [
				"The set { m in A011539 | 9\u003cm\u003c100 or A054054(m) \u003c min(A000030(m),A010879(m)) } (9ish numbers A011539 with 2 digits or such that the smallest digit is strictly smaller than the first and the last digit) is equal to this sequence up to a(1656) = 10099. The next larger 9ish number 10109 is also in that set but is the lunar square of 109, thus not in this sequence of primes. - _M. F. Hasler_, Nov 16 2018"
			],
			"example": [
				"8 is not prime since 8 = 8*8. 9 is not prime since it is the multiplicative unit. 10 is not prime since 10 = 10*8. Thus 19 is the smallest prime."
			],
			"program": [
				"(PARI) A87097=select( is_A087097(n)={my(d); if( n\u003c100, n\u003e88||(n%10==9\u0026\u0026n\u003e9), vecmax(d=digits(n))\u003c9, 0, #d\u003c5, vecmin(d)\u003cmin(d[1],d[#d]), my(m); !for(L=#d\\/2,#d-1, forvec(v=vector(L,i,[i==1,9]),vecmax(n)\u003c9\u0026\u0026next; m=fromdigits(v); for(k=10^(#d-L),10^(#d-L+1)-1, A087062(m,k)==n\u0026\u0026return))))}, [1..999])) \\\\  _M. F. Hasler_, Nov 16 2018"
			],
			"xref": [
				"Cf. A087019, A087061, A087062, A087636, A087638, A087984."
			],
			"keyword": "nonn,easy,base",
			"offset": "1,1",
			"author": "_Marc LeBrun_, Oct 20 2003",
			"references": 28,
			"revision": 57,
			"time": "2021-05-08T22:59:01-04:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}