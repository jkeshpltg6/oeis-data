{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A117731",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 117731,
			"data": "1,7,37,533,1627,18107,237371,95549,1632341,155685007,156188887,3602044091,18051406831,54260455193,225175759291,13981692518567,14000078506967,98115155543129,3634060848592973,3637485804655193",
			"name": "Numerator of the fraction n*Sum_{k=1..n} 1/(n+k).",
			"comment": [
				"a(n) almost always equals A082687(n), but differs for n in A125740.",
				"p divides a((p-1)/3) for primes p in A002476, that is, primes of form 6*n + 1. - _Alexander Adamchuk_, Jul 16 2006"
			],
			"link": [
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/HarmonicNumber.html\"\u003eHarmonic Number\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/HilbertMatrix.html\"\u003eHilbert Matrix\u003c/a\u003e."
			],
			"formula": [
				"a(n) = numerator(n*Sum_{k=1..n} 1/(n+k)).",
				"a(n) = numerator(n*(Psi(2*n+1) - Psi(n+1))).",
				"a(n) = numerator(n*Sum_{k=1..2*n} (-1)^(k+1)/k).",
				"a(n) = numerator(n*A058313(2*n)/A058312(2*n)).",
				"a(n) = numerator(Sum_{j=1..n} Sum_{i=1..n} 1/(i+j-1)), which is the numerator of the sum of all matrix elements of n X n Hilbert Matrix M(i,j) = 1/(i+j-1), (i,j = 1..n). The denominator is A117664(n). - _Alexander Adamchuk_, Apr 23 2006"
			],
			"example": [
				"The first few fractions are 1/2, 7/6, 37/20, 533/210, 1627/504, 18107/4620, 237371/51480, ... = A117731/A296519.",
				"For n=2, the n X n Hilbert matrix is",
				"  1 1/2",
				"  1/2 1/3",
				"Thus, a(2) = numerator(1 + 1/2 + 1/2 + 1/3) = numerator(7/3) = 7.",
				"The n X n Hilbert matrix begins as follows:",
				"    1 1/2 1/3 1/4  1/5  1/6  1/7  1/8 ...",
				"  1/2 1/3 1/4 1/5  1/6  1/7  1/8  1/9 ...",
				"  1/3 1/4 1/5 1/6  1/7  1/8  1/9 1/10 ...",
				"  1/4 1/5 1/6 1/7  1/8  1/9 1/10 1/11 ...",
				"  1/5 1/6 1/7 1/8  1/9 1/10 1/11 1/12 ...",
				"  1/6 1/7 1/8 1/9 1/10 1/11 1/12 1/13 ...",
				"  ..."
			],
			"mathematica": [
				"Numerator[Table[n Sum[1/(n + k), {k, n}], {n, 1, 100}]]",
				"Numerator[Table[Sum[Sum[1/(i + j - 1), {i, n}], {j, n}], {n, 30}]] (* _Alexander Adamchuk_, Apr 23 2006 *)",
				"Table[n (HarmonicNumber[2 n] - HarmonicNumber[n]), {n, 20}] // Numerator (* _Eric W. Weisstein_, Dec 14 2017 *)"
			],
			"program": [
				"(PARI) a(n) = numerator(n*sum(k=1, n, 1/(n+k))); \\\\ _Michel Marcus_, Dec 14 2017"
			],
			"xref": [
				"Cf. A296519 (denominators).",
				"Cf. A001008, A002476, A005249, A058313, A058312, A082687, A086881, A098118, A117664, A125740."
			],
			"keyword": "frac,nonn",
			"offset": "1,2",
			"author": "_Alexander Adamchuk_, Apr 14 2006",
			"ext": [
				"Various sections edited by _Petros Hadjicostas_ and _Michel Marcus_, May 07 2020"
			],
			"references": 9,
			"revision": 37,
			"time": "2020-05-09T05:16:23-04:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}