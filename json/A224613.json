{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A224613",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 224613,
			"data": "12,28,39,60,72,91,96,124,120,168,144,195,168,224,234,252,216,280,240,360,312,336,288,403,372,392,363,480,360,546,384,508,468,504,576,600,456,560,546,744,504,728,528,720,720,672,576,819,684,868,702,840,648",
			"name": "a(n) = sigma(6*n).",
			"comment": [
				"Conjectures: sigma(6n) \u003e sigma(6n - 1) and sigma(6n) \u003e sigma(6n + 1).",
				"Conjectures are false. Try prime 73961483429 for n. One finds sigma(6*73961483429) \u003c sigma(6*73961483429+1). The number n = 105851369791 provides a counterexample for the other case. - _T. D. Noe_, Apr 22 2013",
				"Sum of the divisors of the numbers k which have the property that the width associated to the vertex of the first (also the last) valley of the smallest Dyck path of the symmetric representation of sigma(k) is equal to 2 (see example). Other positive integers have width 0 or 1 associated to the mentioned valley. - _Omar E. Pol_, Aug 11 2021"
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A224613/b224613.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A000203(6n).",
				"a(n) = A000203(A008588(n)). - _Omar E. Pol_, Aug 11 2021"
			],
			"example": [
				"From _Omar E. Pol_, Aug 11 2021: (Start)",
				"Illustration of initial terms:",
				"----------------------------------------------------------------------",
				"   n    6*n   a(n)    Diagram:  1           2           3           4",
				"----------------------------------------------------------------------",
				"                                _           _           _           _",
				"                               | |         | |         | |         | |",
				"                               | |         | |         | |         | |",
				"                          * _ _| |         | |         | |         | |",
				"                           |  _ _|         | |         | |         | |",
				"                      _ _ _| |_|           | |         | |         | |",
				"   1     6     12    |_ _ _ _|      * _ _ _| |         | |         | |",
				"                                    _|  _ _ _|         | |         | |",
				"                                * _|  _| |             | |         | |",
				"                                 |  _|  _|    * _ _ _ _| |         | |",
				"                                 | |_ _|       |  _ _ _ _|         | |",
				"                      _ _ _ _ _ _| |          _| | |               | |",
				"   2    12     28    |_ _ _ _ _ _ _|        _|  _|_|    * _ _ _ _ _| |",
				"                                      * _ _|  _|         |  _ _ _ _ _|",
				"                                       |  _ _|        _ _| | |",
				"                                       | |_ _|      _|  _ _| |",
				"                                       | |        _|  _|  _ _|",
				"                      _ _ _ _ _ _ _ _ _| |       |  _|  _|",
				"   3    18     39    |_ _ _ _ _ _ _ _ _ _|  * _ _| |  _|",
				"                                             |  _ _| |",
				"                                             | |_ _ _|",
				"                                             | |",
				"                                             | |",
				"                      _ _ _ _ _ _ _ _ _ _ _ _| |",
				"   4    24     60    |_ _ _ _ _ _ _ _ _ _ _ _ _|",
				".",
				"Note that the mentioned vertices are aligned on two straight lines that meet at point (3,3).",
				"a(n) equals the area (also the number of cells) in the n-th diagram. (End)"
			],
			"mathematica": [
				"DivisorSigma[1,6*Range[60]] (* _Harvey P. Dale_, Apr 16 2016 *)"
			],
			"program": [
				"(PARI) a(n)=sigma(6*n) \\\\ _Charles R Greathouse IV_, Apr 22 2013",
				"(Python)",
				"from sympy import divisor_sigma",
				"def a(n):  return divisor_sigma(6*n)",
				"print([a(n) for n in range(1, 54)]) # _Michael S. Branicky_, Dec 28 2021"
			],
			"xref": [
				"Sigma(k*n): A000203 (k=1), A062731 (k=2), A144613 (k=3), A193553 (k=4), this sequence (k=6), A283078 (k=7).",
				"Cf. A000203 (sigma(n)), A053224 (n: sigma(n) \u003c sigma(n+1)).",
				"Cf. A067825 (even n: sigma(n)\u003c sigma(n+1)).",
				"Cf. A008588, A237593."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Zak Seidov_, Apr 22 2013",
			"ext": [
				"Corrected by _Harvey P. Dale_, Apr 16 2016"
			],
			"references": 25,
			"revision": 54,
			"time": "2021-12-28T14:19:03-05:00",
			"created": "2013-04-22T15:40:01-04:00"
		}
	]
}