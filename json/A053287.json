{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A053287",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 53287,
			"data": "1,2,6,8,30,36,126,128,432,600,1936,1728,8190,10584,27000,32768,131070,139968,524286,480000,1778112,2640704,8210080,6635520,32400000,44717400,113467392,132765696,533826432,534600000,2147483646,2147483648,6963536448,11452896600",
			"name": "Euler totient function (A000010) of 2^n - 1.",
			"comment": [
				"Number of elements of multiplicative order 2^n - 1 in GF(2^n).",
				"n divides a(n) because 2^a(n) mod 2^n - 1 is 1, 2^n mod 2^n - 1 is 1, so n | a(n). A011260(n) = a(n)/n. - _Jinyuan Wang_, Oct 31 2018",
				"The set {a(n)/(2^n-1)} is dense in [0, 1] (Luca, 2003). - _Amiram Eldar_, Mar 04 2021"
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A053287/b053287.txt\"\u003eTable of n, a(n) for n = 1..1206\u003c/a\u003e (terms 1..100 from T. D. Noe, terms 101..250 from Jianing Song, terms 251..400 from Michel Marcus)",
				"Florian Luca, \u003ca href=\"http://dml.cz/dmlcz/129330\"\u003eOn the sum of divisors of the Mersenne numbers\u003c/a\u003e, Mathematica Slovaca, Vol. 53. No. 5 (2003), pp. 457-466."
			],
			"formula": [
				"a(n) = A000010(A000225(n)).",
				"a(A000079(n-1)) = A058891(n).",
				"a(n) = A000010(2^n-1) or also a(n) = A062401(2^(n-1)) = phi(sigma(2^(n-1))). - _Labos Elemer_, Jul 19 2004"
			],
			"maple": [
				"a := n -\u003e numtheory:-phi(2^n - 1): seq(a(n), n=1..32); # _Zerinvary Lajos_, Oct 05 2007"
			],
			"mathematica": [
				"EulerPhi[2^Range[25] - 1] (* _Giovanni Resta_, Sep 06 2019 *)"
			],
			"program": [
				"(PARI) a(n) = eulerphi(2^n-1) \\\\ _Michael B. Porter_, Oct 06 2009",
				"(MAGMA) [EulerPhi(2^n-1): n in [1..40]]; // _Vincenzo Librandi_, Jul 15 2015",
				"(GAP) List([1..35],n-\u003ePhi(2^n-1)); # _Muniru A Asiru_, Oct 31 2018"
			],
			"xref": [
				"Cf. A000010, A000225, A051953, A057764, A011260."
			],
			"keyword": "nonn,easy,nice",
			"offset": "1,2",
			"author": "_Labos Elemer_, Mar 03 2000",
			"references": 13,
			"revision": 70,
			"time": "2021-03-04T07:02:45-05:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}