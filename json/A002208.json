{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002208",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2208,
			"id": "M3737 N1527",
			"data": "1,1,5,3,251,95,19087,5257,1070017,25713,26842253,4777223,703604254357,106364763817,1166309819657,25221445,8092989203533249,85455477715379,12600467236042756559,1311546499957236437,8136836498467582599787",
			"name": "Numerators of coefficients for numerical integration.",
			"comment": [
				"The denominators are given in A002209.",
				"a(n) is (-1)^n times the numerator of the \"reverse\" multiple zeta value zeta_n^R(0,0,...,0) for n \u003e 0. - _Jonathan Sondow_, Nov 29 2006",
				"a(n) = A191578(2*n,n)/(2*n)!, n \u003e 0. - _Vladimir Kruchinin_, Feb 02 2013",
				"The denominators are given in A002209."
			],
			"reference": [
				"E. Isaacson and H. B. Keller, Analysis of Numerical Methods, ISBN 0 471 42865 5, 1966, John Wiley and Sons, pp. 318-319.",
				"Charles Jordan, Calculus of Finite Differences, Chelsea 1965, p. 529.",
				"N. E. Nørlund, Vorlesungen über Differenzenrechnung, Springer-Verlag, Berlin, 1924.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A002208/b002208.txt\"\u003eTable of n, a(n) for n = 0..100\u003c/a\u003e",
				"S. Akiyama and Y. Tanigawa, \u003ca href=\"http://math.tsukuba.ac.jp/~akiyama/papers/Mzvnrev.pdf\"\u003eMultiple zeta values at non-positive integers\u003c/a\u003e, Ramanujan J. 5 (2001), 327-351.",
				"D. Kruchinin and V. Kruchinin, \u003ca href=\"http://cs.uwaterloo.ca/journals/JIS/VOL18/Kruchinin/kruch9.html\"\u003eA Generating Function for the Diagonal T2n,n in Triangles\u003c/a\u003e, Journal of Integer Sequence, Vol. 18 (2015), article 15.4.6.",
				"Guodong Liu, \u003ca href=\"http://www.fq.math.ca/Papers1/45-2/quartliu02_2007.pdf\"\u003eSome computational formulas for Norlund numbers\u003c/a\u003e, Fib. Quart., 45 (2007), 133-137.",
				"A. N. Lowan and H. Salzer, \u003ca href=\"http://dx.doi.org/10.1002/sapm194322149\"\u003eTable of coefficients in numerical integration formulas\u003c/a\u003e, J. Math. Phys., 22 (1943), 49-50.",
				"A. N. Lowan and H. Salzer, \u003ca href=\"/A002206/a002206.pdf\"\u003eTable of coefficients in numerical integration formulas\u003c/a\u003e, J. Math. Phys. Mass. Inst. Tech. 22 (1943), 49-50.[Annotated scanned copy]",
				"M. O. Rubinstein, \u003ca href=\"https://doi.org/10.1007/s11139-010-9276-8\"\u003eIdentities for the Riemann zeta function\u003c/a\u003e, Ramanujan J. 27, No. 1, 29-42 (2012) and \u003ca href=\"https://arxiv.org/abs/0812.2592\"\u003earXiv:0812.2592\u003c/a\u003e.",
				"\u003ca href=\"/index/Be#Bernoulli\"\u003eIndex entries for sequences related to Bernoulli numbers\u003c/a\u003e."
			],
			"formula": [
				"G.f. of rationals a(n)/A002209(n): -x/((1-x)*log(1-x)).",
				"Let K_i = a(i)/A002209(i), for i \u003e= 1, and [i n] = Stirling numbers of the first kind (A048994), {i n} = Stirling numbers of the second kind (A048993) and B_i the original Bernoulli numbers (A164555/A027642). Then K_i = ((-1)^(i-1) / (i-1)!)*Sum_{n=1..i} [i n]*B_n/n and B_i = i*Sum_{n=1..i} (-1)^(n-1)*{i n}*(n-1)!*K_n. - Rudi Huysmans, rudi_huysmans(AT)hotmail.com [see the second Mathematica program for K_n = a[n_] with B_k = (-1)^k * BernoulliB[k]. - _Wolfdieter Lang_, Aug 09 2017]",
				"a(n) = numerator((-1)^n*Sum_{k=0..n} (k!*Stirling2(n,k)* Stirling1(n+k,n))/(n+k)!). - _Vladimir Kruchinin_, Feb 02 2013",
				"a(n) = numerator(v(n)), where v(n) = 1 - Sum_{i=0..n-1} v(i)/(n-i+1), v(0)=1. - _Vladimir Kruchinin_, Aug 28 2013",
				"a(n) = numerator((1/(n-1)!)*Sum_{k=0..n} ((-1)^(n-k)*binomial(2*n,n-k)*Stirling2(n+k,k))/(n+k)), n \u003e 0, a(0)=1. - _Vladimir Kruchinin_, Apr 05 2016",
				"a(n) = numerator(((-1)^n/n!)*Sum_{k=0..n} Stirling1(n+1,k+1)/(k+1)). - _Vladimir Kruchinin_, Oct 12 2016"
			],
			"example": [
				"1, 1/2, 5/12, 3/8, 251/720, 95/288, 19087/60480, 5257/17280, 1070017/3628800, 25713/89600, 26842253/95800320, 4777223/17418240, 703604254357/2615348736000, 106364763817/402361344000, ... = A002208/A002209."
			],
			"maple": [
				"r := proc(n) option remember; if n=0 then 1 else 1 - add(r(k)/(n-k+1), k=0..n-1) fi end: seq(numer(r(n)), n=0..20); # _Peter Luschny_, Feb 16 2020"
			],
			"mathematica": [
				"Numerator/@CoefficientList[Series[-x/((1-x)Log[1-x]),{x,0,20}],x] (* _Harvey P. Dale_, May 04 2011 *)",
				"a[0] = 1; a[n_] := (-1)^n*Sum[(-1)^(k+1)*BernoulliB[k]*StirlingS1[n, k]/k, {k, 1, n}]/(n-1)!; Table[a[n], {n, 0, 20}] // Numerator (* _Jean-François Alcover_, Sep 27 2012, after Rudi Huysmans's formula *)"
			],
			"program": [
				"(Maxima)",
				"a(n):=if n=0 then 1 else  1/(n-1)!*sum(((-1)^(n-k)*binomial(2*n,n-k)*stirling2(n+k,k))/(n+k),k,0,n); /* _Vladimir Kruchinin_, Apr 05 2016 */",
				"a(n):=num(((-1)^(n)*sum(stirling1(n+1,k+1)/(k+1),k,0,n))/(n)!); /* _Vladimir Kruchinin_, Oct 12 2016 */"
			],
			"xref": [
				"Cf. A002209. See also A002657, A002790, A006232, A006233, A002206, A002207, A191578."
			],
			"keyword": "frac,nonn,easy,nice",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"references": 21,
			"revision": 93,
			"time": "2020-02-16T16:55:49-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}