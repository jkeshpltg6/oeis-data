{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A039621",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 39621,
			"data": "1,-1,1,4,-3,1,-27,19,-6,1,256,-175,55,-10,1,-3125,2101,-660,125,-15,1,46656,-31031,9751,-1890,245,-21,1,-823543,543607,-170898,33621,-4550,434,-28,1,16777216,-11012415,3463615,-688506,95781,-9702,714,-36,1,-387420489",
			"name": "Triangle of Lehmer-Comtet numbers of 2nd kind.",
			"comment": [
				"Also the Bell transform of (-n)^n adding 1,0,0,0,... as column 0. For the definition of the Bell transform see A264428. - _Peter Luschny_, Jan 16 2016"
			],
			"link": [
				"D. H. Lehmer, \u003ca href=\"http://dx.doi.org/10.1216/RMJ-1985-15-2-461\"\u003eNumbers Associated with Stirling Numbers and x^x\u003c/a\u003e, Rocky Mountain J. Math., 15(2) 1985, pp. 461-475."
			],
			"formula": [
				"(k-1)!*a(n, k) = Sum_{i=0..k-1}((-1)^(n-k-i)*binomial(k-1, i)*(n-i-1)^(n-1)).",
				"a(n,k) = (-1)^(n-k)*T(k,n-k,n-k), n\u003e=k, where T(n,k,m)=m*T(n,m-1,k)+T(n-1,k,m+1), T(n,0,m)=1. - _Vladimir Kruchinin_, Mar 07 2020"
			],
			"example": [
				"Triangle begins:",
				"    1;",
				"   -1,    1;",
				"    4,   -3,  1;",
				"  -27,   19, -6,   1;",
				"  256, -175, 55, -10, 1;"
			],
			"mathematica": [
				"a[1, 1] = 1; a[n_, k_] := 1/(k-1)! Sum[((-1)^(n-k-i)*Binomial[k-1, i]*(n-i-1)^(n-1)), {i, 0, k-1}];",
				"Table[a[n, k], {n, 1, 10}, {k, 1, n}]//Flatten (* _Jean-François Alcover_, Jun 03 2019 *)"
			],
			"program": [
				"(PARI) tabl(nn) = {for (n = 1, nn, for (k = 1, n, print1(sum(i = 0, k-1,(-1)^(n-k-i)*binomial(k-1, i)*(n-i-1)^(n-1))/(k-1)!, \", \");); print(););} \\\\ _Michel Marcus_, Aug 28 2013",
				"(Sage) # uses[bell_matrix from A264428]",
				"# Adds 1,0,0,0,... as column 0 at the left side of the triangle.",
				"bell_matrix(lambda n: (-n)^n, 7) # _Peter Luschny_, Jan 16 2016",
				"(Maxima)",
				"T(n,k,m):=if k\u003c0 or n\u003c0 then 0 else if k=0 then 1 else m*T(n,k-1,m)+T(n-1,k,m+1);",
				"a(n,k):=if n\u003ck then 0 else  (-1)^(n-k)*T(k,n-k,n-k); /* _Vladimir Kruchinin_, Mar 07 2020"
			],
			"xref": [
				"Cf. A008296 (matrix inverse). Also A045531 (for column |a(n, 2)|). A185164."
			],
			"keyword": "tabl,sign",
			"offset": "1,4",
			"author": "_Len Smiley_",
			"references": 4,
			"revision": 36,
			"time": "2020-03-28T14:01:48-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}