{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007377",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7377,
			"id": "M0485",
			"data": "0,1,2,3,4,5,6,7,8,9,13,14,15,16,18,19,24,25,27,28,31,32,33,34,35,36,37,39,49,51,67,72,76,77,81,86",
			"name": "Numbers k such that the decimal expansion of 2^k contains no 0.",
			"comment": [
				"It is an open problem of long standing to show that 86 is the last term.",
				"A027870(a(n)) = A224782(a(n)) = 0. - _Reinhard Zumkeller_, Apr 30 2013",
				"See A030700 for the analog for 3^k, which seems to end with k=68. - _M. F. Hasler_, Mar 07 2014",
				"Checked up to k = 10^10. - _David Radcliffe_, Dec 29 2015"
			],
			"reference": [
				"J. S. Madachy, Mathematics on Vacation, Scribner's, NY, 1966, p. 126.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"David Radcliffe, \u003ca href=\"/A007377/a007377.py.txt\"\u003ePython script to search for powers with no zero digits\u003c/a\u003e",
				"W. Schneider, \u003ca href=\"/A007496/a007496.html\"\u003eNoZeros: Powers n^k without Digit Zero\u003c/a\u003e [Cached copy]",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Zero.html\"\u003eZero\u003c/a\u003e",
				"R. G. Wilson, V, \u003ca href=\"/A007376/a007376.pdf\"\u003eLetter to N. J. A. Sloane, Oct. 1993\u003c/a\u003e"
			],
			"maple": [
				"remove(t -\u003e has(convert(2^t,base,10),0),[$0..1000]); # _Robert Israel_, Dec 29 2015"
			],
			"mathematica": [
				"Do[ If[ Union[ RealDigits[ 2^n ] [[1]]] [[1]] != 0, Print[ n ] ], {n, 1, 60000}]",
				"Select[Range@1000, First@Union@IntegerDigits[2^# ] != 0 \u0026]",
				"Select[Range[0,100],DigitCount[2^#,10,0]==0\u0026] (* _Harvey P. Dale_, Feb 06 2015 *)"
			],
			"program": [
				"(MAGMA) [ n: n in [0..50000] | not 0 in Intseq(2^n) ];  // _Bruno Berselli_, Jun 08 2011",
				"(Perl) use bignum;",
				"for(0..99) {",
				"  if((1\u003c\u003c$_) =~ /^[1-9]+$/) {",
				"    print \"$_, \"",
				"  }",
				"} # _Charles R Greathouse IV_, Jun 30 2011",
				"(PARI) for(n=0,99,if(vecmin(eval(Vec(Str(2^n)))),print1(n\", \"))) \\\\ _Charles R Greathouse IV_, Jun 30 2011",
				"(Haskell)",
				"import Data.List (elemIndices)",
				"a007377 n = a007377_list !! (n-1)",
				"a007377_list = elemIndices 0 a027870_list",
				"-- _Reinhard Zumkeller_, Apr 30 2013",
				"(Python)",
				"def ok(n): return '0' not in str(2**n)",
				"print(list(filter(ok, range(10**4)))) # _Michael S. Branicky_, Aug 08 2021"
			],
			"xref": [
				"Cf. A027870, A030700, A102483.",
				"Cf. similar sequences listed in A035064.",
				"Cf. A031142."
			],
			"keyword": "base,nonn",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_, _Robert G. Wilson v_",
			"ext": [
				"a(1) = 0 prepended by _Reinhard Zumkeller_, Apr 30 2013"
			],
			"references": 54,
			"revision": 63,
			"time": "2021-08-08T20:59:42-04:00",
			"created": "1994-05-09T03:00:00-04:00"
		}
	]
}