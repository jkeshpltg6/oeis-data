{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A224069",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 224069,
			"data": "1,-1,1,3,-4,1,-25,36,-12,1,543,-800,288,-32,1,-29281,43440,-16000,1920,-80,1,3781503,-5621952,2085120,-256000,11520,-192,1,-1138779265,1694113344,-629658624,77844480,-3584000,64512,-448,1,783702329343,-1166109967360,433693016064,-53730869248,2491023360,-45875200,344064,-1024,1",
			"name": "Matrix inverse of A111636.",
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A224069/b224069.txt\"\u003eRows n = 0..50, flattened\u003c/a\u003e",
				"W. Wang and T. Wang, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2007.12.037\"\u003eGeneralized Riordan array\u003c/a\u003e, Discrete Mathematics, Vol. 308, No. 24, 6466-6500, (2008)."
			],
			"formula": [
				"T(n,k) = (-1)^(n-k)*A003024(n-k)*A111636(n,k) = (-1)^(n-k)*A003024(n-k)*2^(k*(n-k))*binomial(n,k).",
				"sum {k = 1..n} k*2^k*T(n,k) = 0 for n \u003e= 1.",
				"Let E(x) = sum {n \u003e= 0} x^n/(n!*2^binomial(n,2)) = 1 + x + x^2/(2!*2) + x^3/(3!*2^3) + .... Then a generating function for this triangle is E(x*z)/E(z) = 1 + (x - 1)*z + (x^2 - 4*x + 3)*z^2/(2!*2) + (x^3 - 12*x^2 + 36*x - 25)*z^3/(3!*2^3) + ....",
				"This triangle is a generalized Riordan array (1/E(x), x) with respect to the sequence n!*2^C(n,2), as defined by Wang and Wang.",
				"The row polynomials R(n,x) satisfy the recurrence equation R(n,x) = x^n - sum {k = 0..n-1} binomial(n,k)*2^(k*(n-k))*R(k,x) with R(0,x) = 1, as well as R'(n,2*x) = n*2^(n-1)*R(n-1,x) (the ' denotes differentiation w.r.t. x). The row polynomials appear to have only positive real zeros of multiplicity 1. Moreover, if r(n,1) \u003c r(n,2) \u003c ... \u003c r(n,n) denotes the zeros of R(n,x) arranged in increasing order then it appears that lim {n -\u003e inf} r(n,i) exists for each fixed 1 \u003c= i \u003c= n. An example is given below."
			],
			"example": [
				"Triangle begins",
				"n\\k.|......0......1......2......3......4......5",
				"= = = = = = = = = = = = = = = = = = = = = = = =",
				".0..|......1",
				".1..|.....-1......1",
				".2..|......3.....-4......1",
				".3..|....-25.....36....-12......1",
				".4..|....543...-800....288....-32......1",
				".5..|.-29281..43440.-16000...1920....-80......1",
				"...",
				"The sequence of zeros of R(10,x) begins 1, 3.280147..., 9.112469..., 23.366923..., 57.084317....",
				"The sequence of zeros of R(20,x) begins 1, 3.280163..., 9.112696..., 23.369274..., 57.105379...."
			],
			"mathematica": [
				"max = 8; A111636 = Table[ Binomial[n, k]*2^(k*(n - k)), {n, 0, max}, {k, 0, max}]; t = Inverse[ A111636 ]; Table[ t[[n, k]], {n, 1, max+1}, {k, 1, n}] // Flatten (* _Jean-François Alcover_, Apr 10 2013 *)"
			],
			"xref": [
				"Cf. A003024 (first column), A111636 (matrix inverse)."
			],
			"keyword": "sign,easy,tabl",
			"offset": "0,4",
			"author": "_Peter Bala_, Apr 09 2013",
			"references": 2,
			"revision": 22,
			"time": "2013-07-19T12:33:15-04:00",
			"created": "2013-04-11T18:44:47-04:00"
		}
	]
}