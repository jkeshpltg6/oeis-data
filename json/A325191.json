{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A325191",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 325191,
			"data": "0,0,2,0,3,3,0,4,6,4,0,5,10,10,5,0,6,15,20,15,6,0,7,21,35,35,21,7,0,8,28,56,70,56,28,8,0,9,36,84,126,126,84,36,9,0,10,45,120,210,252,210,120,45,10,0,11,55,165,330,462",
			"name": "Number of integer partitions of n such that the difference between the length of the minimal triangular partition containing and the maximal triangular partition contained in the Young diagram is 1.",
			"comment": [
				"The Heinz numbers of these partitions are given by A325196.",
				"Under the Bulgarian solitaire step, these partitions form cycles of length \u003e= 2.  Length \u003e= 2 means not the length=1 self-loop which occurs from the triangular partition when n is a triangular number.  See A074909 for self-loops included. - _Kevin Ryde_, Sep 27 2019"
			],
			"link": [
				"FindStat, \u003ca href=\"http://www.findstat.org/StatisticsDatabase/St000380\"\u003eSt000380: Half the perimeter of the largest rectangle that fits inside the diagram of an integer partition\u003c/a\u003e",
				"FindStat, \u003ca href=\"http://www.findstat.org/StatisticsDatabase/St000384\"\u003eSt000384: The maximal part of the shifted composition of an integer partition\u003c/a\u003e",
				"FindStat, \u003ca href=\"http://www.findstat.org/StatisticsDatabase/St000783\"\u003eSt000783: The maximal number of occurrences of a colour in a proper colouring of a Ferrers diagram\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/GraphDistance.html\"\u003eGraph Distance\u003c/a\u003e"
			],
			"formula": [
				"Positions of zeros are A000217 = n * (n + 1) / 2.",
				"a(n) = A074909(n) - A010054(n). - _Kevin Ryde_, Sep 27 2019"
			],
			"example": [
				"The a(2) = 2 through a(12) = 10 partitions (empty columns not shown):",
				"  (2)   (22)   (32)   (322)   (332)   (432)   (4322)   (4332)",
				"  (11)  (31)   (221)  (331)   (422)   (3321)  (4331)   (4422)",
				"        (211)  (311)  (421)   (431)   (4221)  (4421)   (4431)",
				"                      (3211)  (3221)  (4311)  (5321)   (5322)",
				"                              (3311)          (43211)  (5331)",
				"                              (4211)                   (5421)",
				"                                                       (43221)",
				"                                                       (43311)",
				"                                                       (44211)",
				"                                                       (53211)"
			],
			"mathematica": [
				"otb[ptn_]:=Min@@MapIndexed[#1+#2[[1]]-1\u0026,Append[ptn,0]];",
				"otbmax[ptn_]:=Max@@MapIndexed[#1+#2[[1]]-1\u0026,Append[ptn,0]];",
				"Table[Length[Select[IntegerPartitions[n],otb[#]+1==otbmax[#]\u0026]],{n,0,30}]"
			],
			"program": [
				"(PARI) a(n) = my(t=ceil(sqrtint(8*n+1)/2), r=n-t*(t-1)/2); if(r==0,0, binomial(t,r)); \\\\ _Kevin Ryde_, Sep 27 2019"
			],
			"xref": [
				"Column k=1 of A325200.",
				"Cf. A060687, A065770, A071724, A256617, A325166, A325169, A325178, A325179, A325181, A325187, A325188, A325189, A325195, A325196."
			],
			"keyword": "nonn,look",
			"offset": "0,3",
			"author": "_Gus Wiseman_, Apr 11 2019",
			"references": 8,
			"revision": 22,
			"time": "2019-09-30T02:23:44-04:00",
			"created": "2019-04-11T20:56:36-04:00"
		}
	]
}