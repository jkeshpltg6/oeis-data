{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A160242",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 160242,
			"data": "1,2,1,2,2,2,1,2,2,2,2,2,1,2,2,2,2,2,2,2,1,2,2,2,2,2,2,2,2,2,1,2,2,2,2,2,2,2,2,2,2,2,1,2,2,2,2,2,2,2,2,2,2,2,2,2,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2",
			"name": "Triangle A(n,m) read by rows: a quarter of the Fourier coefficient [cos(m*t)] of the shifted Boubaker polynomial B_n(2*cos t)-2*cos(n*t).",
			"comment": [
				"Starting from the polynomials B_n(x) defined in A137276 and A135929, we insert x=2*cos(t), and define the Fourier coefficients A(n,m) by B_n(2*cos t)-2*cos(n*t) = 4*sum(m=0,..,n-2) A(n,m)*cos(m*t).",
				"A(n,m) is not an integer for n=0, so the table starts at n=1. Furthermore, A(n,m)=0 if n-m is odd, these regular zeros are skipped as usual, so effectively the first table entry appears at n=2.",
				"Simpler definition from R. J. Mathar, Apr 15 2010: a(n)=1 if n =0 or n in A002061, otherwise a(n)=2. So this is a kind of characteristic function of the central polygonal numbers A002061."
			],
			"link": [
				"A. Luzon and M. A. Morón, \u003ca href=\"http://arxiv.org/abs/0904.2672\"\u003eRecurrence relations for polynomial sequences via Riordan matrices\u003c/a\u003e, arXiv:0904.2672 [math.CO]"
			],
			"example": [
				"Using T^m =cos(m*t) as a notational shortcut, the expansions start",
				"; B_1(2 cos t)-2*cos 1 t = 0",
				"1 ; B_2(2 cos t)-2*cos 2 t = 1",
				"0 2 ; B_3(2 cos t)-2*cos 3 t = 2*T",
				"1 0 2 ; B_4(2 cos t)-2*cos 4 t = 1+2*T^2",
				"0 2 0 2 ; B_5(2 cos t)-2*cos 5 t = 2*T+2*T^3",
				"1 0 2 0 2 ; B_6(2 cos t)-2*cos 6 t = 1+2*T^2+2*T^4",
				"0 2 0 2 0 2 ; B_7(2 cos t)-2*cos 7 t = 2*T+2*T^3+2*T^5",
				"1 0 2 0 2 0 2 ; B_8(2 cos t)-2*cos 8 t = 1+2*T^2+2*T^4+2*T^6",
				"0 2 0 2 0 2 0 2 ; B_9(2 cos t)-2*cos 9 t = 2*T+2*T^3+2*T^5+2*T^7",
				"1 0 2 0 2 0 2 0 2 ; B_10(2 cos t)-2*cos 10 t = 1+2*T^2+2*T^4+2*T^6+2*T^8",
				"0 2 0 2 0 2 0 2 0 2 ; B_11(2 cos t)-2*cos 11 t = 2*T^3+2*T^5+2*T^7+2*T^9+2*T"
			],
			"mathematica": [
				"centralPolygonalQ[n_] := Resolve[Exists[k, k\u003e0, n == k^2-k+1], Integers];",
				"b[n_] := If[n == 0 || centralPolygonalQ[n], 1, 2];",
				"a[n_] := b[n-1];",
				"Table[a[n], {n, 2, 106}] (* _Jean-François Alcover_, Oct 31 2018, after _R. J. Mathar_ *)"
			],
			"keyword": "nonn,tabl",
			"offset": "2,2",
			"author": "_Haydar Rahmanov_, May 05 2009",
			"ext": [
				"Definition clarified, publication title corrected, sequence extended by _R. J. Mathar_, Dec 07 2009"
			],
			"references": 0,
			"revision": 14,
			"time": "2018-10-31T11:45:56-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}