{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A275530",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 275530,
			"data": "3,3,3,9,3,3,3,113,331,513,827,799,3291,5041,71,220221,23891,11559",
			"name": "Smallest positive integer m such that (m^(2^n) + 1)/2 is prime.",
			"comment": [
				"The terms of this sequence with n \u003e 11 correspond to probable primes which are too large to be proven prime currently. - _Serge Batalov_, Apr 01 2018",
				"a(15) is a statistically significant outlier; the sequence (m^(2^15)+1)/2 may require a double-check with software that is not GWNUM-based. - _Serge Batalov_, Apr 01 2018"
			],
			"link": [
				"Richard Fischer, \u003ca href=\"http://www.fermatquotient.com/PrimSerien/GenFermOdd.txt\"\u003eGeneralized Fermat numbers with odd base\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Fermat_number\"\u003eFermat number\u003c/a\u003e"
			],
			"example": [
				"a(7) = 113 since 113 is the smallest positive integer m such that (m^(2^7)+1)/2 is prime."
			],
			"maple": [
				"a:= proc(n) option remember; local m; for m by 2",
				"      while not isprime((m^(2^n)+1)/2) do od; m",
				"    end:",
				"seq(a(n), n=0..8);"
			],
			"mathematica": [
				"Table[m = 1; While[! PrimeQ[(m^(2^n) + 1)/2], m++]; m, {n, 0, 9}] (* _Michael De Vlieger_, Sep 23 2016 *)"
			],
			"program": [
				"(PARI) a(n) = {my(m = 1); while (! isprime((m^(2^n)+1)/2), m += 2); m;} \\\\ _Michel Marcus_, Aug 01 2016",
				"(Python)",
				"from sympy import isprime",
				"def a(n):",
				"  m, pow2 = 1, 2**n",
				"  while True:",
				"    if isprime((m**pow2 + 1)//2): return m",
				"    m += 2",
				"print([a(n) for n in range(9)]) # _Michael S. Branicky_, Mar 03 2021"
			],
			"xref": [
				"Cf. A056993, A027862."
			],
			"keyword": "nonn,more",
			"offset": "0,1",
			"author": "_Walter Kehowski_, Jul 31 2016",
			"ext": [
				"a(13)-a(14) from _Robert Price_, Sep 23 2016",
				"a(15) from _Serge Batalov_, Mar 29 2018",
				"a(16) from _Serge Batalov_, Mar 30 2018",
				"a(17) from _Serge Batalov_, Apr 01 2018"
			],
			"references": 2,
			"revision": 58,
			"time": "2021-03-04T01:30:07-05:00",
			"created": "2016-08-01T12:41:59-04:00"
		}
	]
}