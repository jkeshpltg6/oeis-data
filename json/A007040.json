{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007040",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7040,
			"id": "M0354",
			"data": "2,2,6,6,10,20,28,46,78,122,198,324,520,842,1366,2206,3570,5780,9348,15126,24478,39602,64078,103684,167760,271442,439206,710646,1149850,1860500,3010348,4870846,7881198,12752042,20633238,33385284,54018520",
			"name": "Number of (marked) cyclic n-bit binary strings containing no runs of length \u003e 2.",
			"comment": [
				"For n \u003e= 3, also the number of maximal independent vertex sets (and minimal vertex covers) in the n-prism graph. - _Eric W. Weisstein_, Mar 30 and Aug 07 2017",
				"From _Petros Hadjicostas_, Jul 08 2018: (Start)",
				"Let q and m be positive integers. We denote by f1(m,q,n) the number of (marked) cyclic q-ary strings of length n that contain no runs of lengths \u003e m when no wrapping around is allowed, and by f2(m,q,n) when wrapping around is allowed.",
				"It is clear that f1(m,q,n) = f2(m,q,n) for n \u003e m, but f1(m,q,n) = q^n and f2(m,q,n) = q^n - q when 1 \u003c= n \u003c= m.",
				"Burstein and Wilf (1997) and Edlin and Zeilberger (2000) considered f1(m,q,n) while Hadjicostas and Zhang considered f2(m,q,n).",
				"Let g(m, q, x) = (m+1-m*q*x)/(1-q*x+(q-1)*x^(m+1)) - (m+1)/(1-x^(m+1)).",
				"By generalizing Moser (1991, 1993), Burstein and Wilf (1997) proved that the g.f. of the numbers f1(m,q,n) is F1(m,q,x) = ((1-x^m)/(1-x))*(q*x + (q-1)*x* g(m, q, x)).",
				"Using the above formula by Burstein and Wilf (1997), Hadjicostas and Zhang (2018) proved that the g.f. of the numbers f2(m,q,n) is F2(m,q,x) = ((q-1)*x*(1-x^m)/(1-x))*g(m, q, x).",
				"A necklace is an unmarked cyclic string. If f3(m,q,n) is the number of q-ary necklaces of length n with no runs of length \u003e m (and wrapping around is allowed), then f3(m,q,n) = (1/n)*Sum_{d|n} phi(n/d)*f2(m,q,d), where phi(.) is Euler's totient function. Using this formula and F2(m,q,x), Hadjicostas and Zhang (2018) proved that the g.f. of the numbers f3(m,q,n) is given by F3(m,q,x) = -(q-1)*x*(1-x^m)/((1-x)*(1-x^(m+1))) - Sum_{s\u003e=1} (phi(s)/s)*log(1 - (q-1)*(x^s - x^(s*(m+1)))/(1-x^s)).",
				"For the current sequence, we have q = 2 and m = 2. We have a(n) = f1(m=2, q=2, n) = f2(m=2, q=2, n) for n \u003e= 3, but for a(1) and a(2) it is unclear what approach the author of the sequence is following. He has a(1) = q^1 = 2, but a(2) = q^2 - q = 2^2 - 2 = 2. (Note that, for q = m = 2, we have f1(m=2, q=2, 1) = 2, f1(m=2, q=2, 2) = 4, f2(m=2, q=2, 1) = 0, and f2(m=2, q=2, 2) = 2.)",
				"If A(x) is the g.f. of the current sequence, we have A(x) = F1(m=2,q=2, x) - 2*x^2 = F2(m=2, q=2, x) + 2*x.",
				"When m = 1 and q = 3, we have f1(m=1, q=3, n) = number of marked cyclic words on three letters with no two consecutive like letters. We have f1(m=1, q=3, n) = A092297(n) for n \u003e= 2. This was first stated in the comments of that sequence by G. Critzer.",
				"When m = 1 and q = 4, we have f1(m=1, q=4, n) = number of marked cyclic words on four letters with no two consecutive like letters. We have f1(m=1, q=4, n) = A218034(n) for n \u003e= 1. This was first stated in the comments of that sequence by J. Arndt.",
				"A generalization of the above formula by Burstein and Wilf (1997) was given by Taylor (2014) in Section 5 of his paper. (End)"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A007040/b007040.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Z. Agur, A. S. Fraenkel, and S. T. Klein, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(88)90005-2\"\u003eThe number of fixed points of the majority rule\u003c/a\u003e, Discr. Math., 70 (1988), 295-302.",
				"A. Burstein and H. S. Wilf, \u003ca href=\"https://www.fq.math.ca/Scanned/35-3/burstein.pdf\"\u003eOn cyclic strings without long constant blocks\u003c/a\u003e, Fibonacci Quarterly, 35 (1997), 240-247.",
				"A. E. Edlin and D. Zeilberger, \u003ca href=\"https://doi.org/10.1006/aama.2000.0696\"\u003eThe Goulden-Jackson cluster method for cyclic words\u003c/a\u003e, Adv. Appl. Math., 25 (2000), 228-232.",
				"W. Florek, \u003ca href=\"http://doi.org/10.1016/j.amc.2018.06.014\"\u003eA class of generalized Tribonacci sequences applied to counting problems\u003c/a\u003e, Appl. Math. Comput., 338 (2018), 809-821.",
				"Petros Hadjicostas and Lingyun Zhang, \u003ca href=\"https://doi.org/10.1016/j.disc.2018.03.007\"\u003eOn cyclic strings avoiding a pattern\u003c/a\u003e, Discrete Mathematics, 341 (2018), 1662-1674.",
				"Matthew Macauley, Jon McCammond, and Henning S. Mortveit, \u003ca href=\"http://www.emis.de/journals/JACO/Volume33_1/hgv665924j44t770.html\"\u003eDynamics groups of asynchronous cellular automata\u003c/a\u003e, Journal of Algebraic Combinatorics, 33(1) (2011), 11-35.",
				"A. McLeod and W. O. J. Moser, \u003ca href=\"http://www.jstor.org/stable/27642988\"\u003eCounting cyclic binary strings\u003c/a\u003e, Math. Mag., 80(1) (2007), 29-37.",
				"W. O. J. Moser, \u003ca href=\"/A007040/a007040.pdf\"\u003eOn cyclic binary n-bit strings\u003c/a\u003e, Report from the Department of Mathematics and Statistics, McGill University, 1991. (Annotated scanned copy)",
				"W. O. J. Moser, \u003ca href=\"http://www.fq.math.ca/Scanned/31-1/moser.pdf\"\u003eCyclic binary strings without long runs of like (alternating) bits\u003c/a\u003e, Fibonacci Quart. 31(1) (1993), 2-6.",
				"Noriaki Sannomiya, Hosho Katsura, and Yu Nakayama, \u003ca href=\"http://arxiv.org/abs/1612.02285\"\u003eSupersymmetry breaking and Nambu-Goldstone fermions with cubic dispersion\u003c/a\u003e, arXiv:1612.02285 [cond-mat.str-el], 2016. See Table I, line 2.",
				"Jair Taylor, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v21i2p1\"\u003eCounting Words with Laguerre Series\u003c/a\u003e, Electron. J. Combin., 21 (2014), P2.1.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/MaximalIndependentVertexSet.html\"\u003eMaximal Independent Vertex Set\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/MinimalVertexCover.html\"\u003eMinimal Vertex Cover\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PrismGraph.html\"\u003ePrism Graph\u003c/a\u003e.",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,1,2,1)."
			],
			"formula": [
				"a(n) = a(n-2) + 2*a(n-3) + a(n-4), n \u003e= 7. - _David W. Wilson_",
				"a(n) = n*Sum_{k=1..n} binomial(2*k, n-2*k)/k for n \u003e 1 with a(0) = 0 and a(1) = 2. - _Vladimir Kruchinin_, Oct 12 2011",
				"G.f.: 2*x*(1 + x + 2*x^2 - x^4)/((1 - x - x^2)*(1 + x + x^2)). - _Colin Barker_, Mar 15 2012",
				"a(n) = A000032(n) + 2*cos(2*Pi*n/3) for n \u003e 1. - _Eric W. Weisstein_, Mar 30 2017",
				"a(n) = 2*A100886(n-1), n \u003e 1. - _R. J. Mathar_, Jan 20 2018",
				"a(n) = A000032(n) - A061347(n) for n \u003e 1. - _Wojciech Florek_, Feb 18 2018"
			],
			"mathematica": [
				"Join[{2}, LinearRecurrence[{0, 1, 2, 1}, {2, 6, 6, 10}, 40]] (* _Harvey P. Dale_, Nov 09 2011 *)",
				"Join[{2}, Table[n Sum[Binomial[2 k, n - 2 k]/k, {k, n}], {n, 2, 40}]] (* _Harvey P. Dale_, Nov 09 2011 *)",
				"Table[LucasL[n] + 2 Cos[2 n Pi/3], {n, 20}] (* _Eric W. Weisstein_, Mar 30 2017 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c3,2,([0,1,0,0; 0,0,1,0; 0,0,0,1; 1,2,1,0]^(n-2)*[2;6;6;10])[1,1]) \\\\ _Charles R Greathouse IV_, Jun 15 2015"
			],
			"xref": [
				"Cf. A000032, A007039, A316660."
			],
			"keyword": "nonn,nice,easy",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Name clarified by _Petros Hadjicostas_, Jul 08 2018"
			],
			"references": 8,
			"revision": 139,
			"time": "2020-07-13T06:35:29-04:00",
			"created": "1994-04-28T03:00:00-04:00"
		}
	]
}