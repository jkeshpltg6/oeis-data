{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A175330",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 175330,
			"data": "2,1,5,3,9,1,17,19,21,29,5,33,41,43,37,49,57,1,67,65,73,67,81,65,97,101,99,105,97,113,3,129,137,129,149,149,129,163,165,161,177,181,129,193,197,195,211,195,225,225,233,225,241,1,257,261,269,261,273,281",
			"name": "a(n) = bitwise AND of prime(n) and prime(n+1).",
			"comment": [
				"Read each binary representation of the primes from right to left and then AND respective digits to form the binary equivalent of each term of this sequence.",
				"Indices of 1's: 2, 6, 18, 54, 564, 3512, 6542, 564163, 2063689, 54400028, ... - _Alex Ratushnyak_, Apr 22 2012"
			],
			"link": [
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Bitwise operation\"\u003eBitwise operation\u003c/a\u003e"
			],
			"example": [
				"For n = 15, a(15) = 37 because the 15th prime is 47 and the 16th is 53, which have binary representations of 101111 and 110101 respectively; the bitwise AND of these values is 100101 which is the binary representation of 37:",
				"  101111",
				"\u0026 110101",
				"--------",
				"  100101"
			],
			"maple": [
				"read(\"transforms\") ; A175330 := proc(n) ANDnos(ithprime(n),ithprime(n+1)) ; end proc: seq(A175330(n),n=1..60) ; # _R. J. Mathar_, Apr 15 2010",
				"# second Maple program:",
				"a:= n-\u003e Bits[And](ithprime(n), ithprime(n+1)):",
				"seq(a(n), n=1..70);  # _Alois P. Heinz_, Apr 15 2020"
			],
			"mathematica": [
				"a[n_] := Prime[n]~BitAnd~Prime[n+1];",
				"Array[a, 60] (* _Jean-François Alcover_, Jan 11 2021 *)"
			],
			"program": [
				"(PARI) a(n) = bitand(prime(n), prime(n+1)); \\\\ _Michel Marcus_, Apr 16 2020",
				"(Scala) val prime: LazyList[Int] = 2 #:: LazyList.from(3).filter(i =\u003e prime.takeWhile {",
				"   j =\u003e j * j \u003c= i",
				"}.forall {",
				"   k =\u003e i % k != 0",
				"})",
				"(0 to 63).map(n =\u003e prime(n) \u0026 prime(n + 1)) // _Alonso del Arte_, Apr 18 2020"
			],
			"xref": [
				"Cf. A000040, A175329 (bitwise OR).",
				"Cf. A129760 (bitwise AND of n and n-1)."
			],
			"keyword": "base,nonn",
			"offset": "1,1",
			"author": "_Leroy Quet_, Apr 07 2010",
			"ext": [
				"More terms from _R. J. Mathar_, Apr 15 2010"
			],
			"references": 7,
			"revision": 33,
			"time": "2021-01-11T10:04:07-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}