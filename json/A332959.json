{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A332959",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 332959,
			"data": "1,3,12,15,180,240,105,5040,6720,7000,945,151200,352800,315000,272160,10395,6029100,21067200,20790000,17962560,13311144,135135,276215940,1387386000,1765764000,1471133664,1211314104,787218432,2027025,14983768800,105945840000,165225060000,146023637760,121131410400,94466211840,54717165360",
			"name": "Triangle read by rows: T(n,k) is the number of labeled forests with n trees and 2n nodes and with the largest tree having exactly k nodes, (n \u003e= 1, 2 \u003c= k \u003c= n+1).",
			"comment": [
				"The first formula is based on Kolchin's formula (1.4.2) [see the Kolchin reference]."
			],
			"reference": [
				"V. F. Kolchin, Random Graphs. Encyclopedia of Mathematics and Its Applications 53. Cambridge Univ. Press, Cambridge, 1999, pp 30-31."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A332959/b332959.txt\"\u003eTable of n, a(n) for n = 1..1275\u003c/a\u003e (first 50 rows)"
			],
			"formula": [
				"T(n,k) = ((2*n)!/n!) * Sum_{compositions p_1 + ... + p_n = 2*n, 1 \u003c= p_i \u003c= k}",
				"  Product_{j=1..n} f(p_j) / p_j!, where f(p_j) = A000272(p_j) = p_j^(p_j-2)."
			],
			"example": [
				"Triangle T(n,k) begins:",
				"      1;",
				"      3,      12;",
				"     15,     180,      240;",
				"    105,    5040,     6720,     7000;",
				"    945,  151200,   352800,   315000,   272160;",
				"  10395, 6029100, 21067200, 20790000, 17962560, 13311144;",
				"  ...",
				"The graphs for T(2,2) and T(2,3) are illustrated below:",
				"   o---o   :   o   o",
				"           :   |",
				"   o---o   :   o---o",
				"T(2,2) = 3 since the graph on the left has 3 labelings.",
				"T(2,3) = 12 since the graph on the right has 12 labelings."
			],
			"program": [
				"(PARI)",
				"T(n, k) = { my(S = 0);",
				"  forpart(a = 2*n,",
				"    if(a[n] == k,",
				"      my(D = Set(a));",
				"      my(Pr = prod(j=1, #D, my(p = D[j], c = #select(x-\u003ex==p, Vec(a))); p^((p-2)*c) / (p!^c*c!)));",
				"       S += n!*Pr )",
				"  , [1, k], [n, n]); (2*n)! / n! * S };",
				"(PARI)",
				"B(n,k)={my(p=sum(j=1, k, j^(j-2)*x^j/j!)); (2*n)!*polcoef( polcoef( exp(y*p + O(x*x^(2*n))), 2*n, x), n, y)}",
				"T(n,k)={B(n,k)-B(n,k-1)} \\\\ _Andrew Howroyd_, May 08 2020"
			],
			"xref": [
				"Columns k=2..3 are A001147, A332960.",
				"Row sums give A302112.",
				"Main diagonal is A332958.",
				"Cf. A000272, A001147, A302112, A332958, A332960."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "1,2",
			"author": "_Washington Bomfim_, Apr 13 2020",
			"references": 3,
			"revision": 67,
			"time": "2020-05-17T13:49:47-04:00",
			"created": "2020-05-09T07:46:08-04:00"
		}
	]
}