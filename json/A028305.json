{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A028305",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 28305,
			"data": "1,0,1,1,0,1,2,2,0,2,9,6,3,0,6,44,31,19,11,0,15,265,180,105,54,32,0,84,1854,1255,771,411,281,138,0,330,14833,9949,6052,3583,2057,1366,668,0,1812,133496,89162,55340,32135,19026,12685,6753,4305,0,9978,1334961,886837,547922,331930,193538,117323,79291,45536,25959,0,65503",
			"name": "Triangle of numbers of permutations eliminating just k cards out of n in game of Mousetrap.",
			"comment": [
				"Triangle T(n,k), 0 \u003c= k \u003c= n"
			],
			"reference": [
				"R. K. Guy, Unsolved Problems Number Theory, E37.",
				"R. K. Guy and R. J. Nowakowski, \"Mousetrap,\" in D. Miklos, V. T. Sos and T. Szonyi, eds., Combinatorics, Paul Erdős is Eighty. Bolyai Society Math. Studies, Vol. 1, pp. 193-206, 1993.",
				"S. Washburn, T. Marlowe and C. T. Ryan, Discrete Mathematics, Addison-Wesley, 1999, page 326."
			],
			"link": [
				"Georg Fischer, \u003ca href=\"/A028305/b028305.txt\"\u003eTable of n, a(n) for n = 0..107\u003c/a\u003e (terms 36..65 from Martin Renner)",
				"Arthur Cayley, \u003ca href=\"http://gdz.sub.uni-goettingen.de/dms/load/img/?PPN=PPN600494829_0015\u0026amp;IDDOC=649134\"\u003eOn the game of Mousetrap\u003c/a\u003e, in: Quarterly Journal of Pure and Applied Mathematics 15 (1878), p. 8-10.",
				"R. K. Guy and R. J. Nowakowski, \u003ca href=\"/A002467/a002467_1.pdf\"\u003eMousetrap\u003c/a\u003e, Preprint, Feb 10 1993 [Annotated scanned copy]",
				"Adolph Steen, \u003ca href=\"http://gdz.sub.uni-goettingen.de/dms/load/img/?PPN=PPN600494829_0015\u0026amp;IDDOC=649134\"\u003eSome formulas respecting the game of Mousetrap\u003c/a\u003e, Quarterly Journal of Pure and Applied Mathematics 15 (1878), p. 230-241."
			],
			"formula": [
				"T(n,0) = A000166(n), T(n,1) = A007710(n), T(n,n-1) = A000004(n) = 0, T(n,n) = A007709(n)."
			],
			"example": [
				"Triangle begins:",
				"     1,",
				"     0,    1,",
				"     1,    0,   1,",
				"     2,    2,   0,   2,",
				"     9,    6,   3,   0,   6,",
				"    44,   31,  19,  11,   0,  15,",
				"   265,  180, 105,  54,  32,   0, 84,",
				"  1854, 1255, 771, 411, 281, 138,  0, 330,",
				"  ..."
			],
			"maple": [
				"A028305:=proc(n)",
				"  local P, j, M, K, A, i, K_neu, k, m;",
				"  P:=combinat[permute](n):",
				"  for j from 0 to n do",
				"    M[j]:=0:",
				"  od:",
				"  for j from 1 to nops(P) do",
				"    K:=P[j]:",
				"    A:=[]:",
				"    for i while nops(K)\u003e0 do",
				"      K_neu:=[]:",
				"      for k from 1 to n do",
				"        m:=nops(K);",
				"        if k mod m = 0 then",
				"          if K[m]=k then",
				"            K_neu:=[seq(K[j],j=1..m-1)];",
				"            A:=[op(A),k];",
				"          else next;",
				"          fi;",
				"        else",
				"          if K[k mod m]=k then",
				"            K_neu:=[seq(K[j],j=(k mod m)+1..m),seq(K[j],j=1..(k mod m)-1)];",
				"            A:=[op(A),k];",
				"          else next;",
				"          fi;",
				"        fi;",
				"        if nops(K_neu)\u003c\u003e0 then break; fi;",
				"      od;",
				"      if nops(K_neu)\u003c\u003e0 then",
				"        K:=K_neu;",
				"      else break;",
				"      fi;",
				"    od:",
				"    M[nops(A)]:=M[nops(A)]+1;",
				"  od:",
				"  seq(M[j],j=0..n);",
				"end:",
				"# _Martin Renner_, Sep 03 2015"
			],
			"xref": [
				"Cf. A000166, A007709, A007710."
			],
			"keyword": "tabl,nonn,hard,more",
			"offset": "0,7",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"a(36)-a(65) from _Martin Renner_, Sep 02 2015"
			],
			"references": 4,
			"revision": 33,
			"time": "2020-02-24T04:26:47-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}