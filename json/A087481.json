{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A087481",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 87481,
			"data": "2,4,4,16,12,48,64,192,260,1024,1128,4096,4480,13310,20620,65434,76376,262144,358532",
			"name": "Number of polynomials of the form x^n +- x^(n-1) +- x^(n-2) +- ... +- 1 irreducible over the integers.",
			"comment": [
				"For each n, there are 2^n polynomials to consider. All 2^n polynomials are irreducible for n = 1, 2, 4, 10, 12, 18, which is sequence A071642. For those values of n, n+1 is a prime in Artin's primitive root conjecture (A001122).",
				"Since p(x) is irreducible iff (-1)^n*p(-x) is irreducible, all terms are even. - _Robert Israel_, Dec 22 2014"
			],
			"link": [
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/IrreduciblePolynomial.html\"\u003eIrreducible Polynomial\u003c/a\u003e",
				"Math Overflow, \u003ca href=\"http://mathoverflow.net/questions/7969/irreducible-polynomials-with-constrained-coefficients/8086#8086\"\u003eIrreducible polynomials with constrained coefficients\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 2^n for n a term of A071642; see first comment."
			],
			"maple": [
				"f:= proc(n) local t, j, p0, p;",
				"   p0:= add(x^j, j = 0 .. n);",
				"   2*nops(select(s -\u003e irreduc(p0 - 2*add(x^(j-1), j = s)), combinat:-powerset(n-1)));",
				"end proc:",
				"seq(f(n),n=1..18); # _Robert Israel_, Dec 22 2014"
			],
			"mathematica": [
				"Irreducible[p_, n_] := Module[{f}, f=FactorList[p, Modulus-\u003en]; Length[f]==1 || Simplify[p-f[[2, 1]]]===0]; Table[xx=x^Range[0, n-1]; cnt=0; Do[p=x^n+xx.(2*IntegerDigits[i, 2, n]-1); If[Irreducible[p, 0], cnt++ ], {i, 0, 2^n-1}]; cnt, {n, 18}]"
			],
			"xref": [
				"Cf. A001122, A071642, A087482 (irreducible binary polynomials)."
			],
			"keyword": "nonn,more",
			"offset": "1,1",
			"author": "_T. D. Noe_, Sep 09 2003",
			"ext": [
				"a(19) from _Robert Israel_, Dec 22 2014"
			],
			"references": 2,
			"revision": 21,
			"time": "2015-01-05T14:00:34-05:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}