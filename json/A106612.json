{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A106612",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 106612,
			"data": "0,1,2,3,4,5,6,7,8,9,10,1,12,13,14,15,16,17,18,19,20,21,2,23,24,25,26,27,28,29,30,31,32,3,34,35,36,37,38,39,40,41,42,43,4,45,46,47,48,49,50,51,52,53,54,5,56,57,58,59,60,61,62,63,64,65,6,67,68,69,70,71,72,73,74,75",
			"name": "a(n) = numerator of n/(n+11).",
			"comment": [
				"In general, the numerators of n/(n+p) for prime p and n \u003e= 0, form a sequence with the g.f.: x/(1-x)^2 - (p-1)*x^p/(1-x^p)^2. - _Paul D. Hanna_, Jul 27 2005",
				"a(n) \u003c\u003e n iff n = 11 * k, in this case, a(n) = k. - _Bernard Schott_, Feb 19 2019"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A106612/b106612.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_22\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,-1)."
			],
			"formula": [
				"G.f.: x/(1-x)^2 - 10*x^11/(1-x^11)^2. - _Paul D. Hanna_, Jul 27 2005",
				"a(n) = lcm(n,11)/11.",
				"From _R. J. Mathar_, Apr 18 2011: (Start)",
				"a(n) = A109052(n)/11.",
				"Dirichlet g.f.: zeta(s-1)*(1-10/11^s). (End)",
				"a(n) = 2*a(n-11) - a(n-22). - _G. C. Greubel_, Feb 19 2019"
			],
			"maple": [
				"seq(numer(n/(n+11)),n=0..80); # _Muniru A Asiru_, Feb 19 2019"
			],
			"mathematica": [
				"f[n_]:=Numerator[n/(n+11)];Array[f,100,0] (* _Vladimir Joseph Stephan Orlovsky_, Feb 17 2011 *)",
				"LinearRecurrence[{0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,-1},{0,1,2,3,4,5,6,7,8,9,10,1,12,13,14,15,16,17,18,19,20,21},80] (* _Harvey P. Dale_, Jul 05 2021 *)"
			],
			"program": [
				"(sage) [lcm(n,11)/11 for n in range(0, 54)] # _Zerinvary Lajos_, Jun 09 2009",
				"(MAGMA) [Numerator(n/(n+11)): n in [0..100]]; // _Vincenzo Librandi_, Apr 18 2011",
				"(PARI) vector(100, n, n--; numerator(n/(n+11))) \\\\ _G. C. Greubel_, Feb 19 2019",
				"(GAP) List([0..80],n-\u003eNumeratorRat(n/(n+11))); # _Muniru A Asiru_, Feb 19 2019"
			],
			"xref": [
				"Cf. A109052, A137564 (differs, e.g., for n=100).",
				"Cf. Sequences given by the formula numerator(n/(n + k)): A026741 (k = 2), A051176 (k = 3), A060819 (k = 4), A060791 (k = 5), A060789 (k = 6), A106608 thru A106611 (k = 7 thru 10), A051724 (k = 12), A106614 thru A106621 (k = 13 thru 20)."
			],
			"keyword": "nonn,frac,mult,easy",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_, May 15 2005",
			"references": 23,
			"revision": 42,
			"time": "2021-07-22T10:37:05-04:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}