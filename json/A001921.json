{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001921",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1921,
			"id": "M4455 N1885",
			"data": "0,7,104,1455,20272,282359,3932760,54776287,762935264,10626317415,148005508552,2061450802319,28712305723920,399910829332567,5570039304932024,77580639439715775,1080558912851088832,15050244140475527879,209622859053806301480",
			"name": "a(n) = 14*a(n-1) - a(n-2) + 6 for n\u003e1, a(0)=0, a(1)=7.",
			"comment": [
				"(a(n)+1)^3 - a(n)^3 is a square (that of A001570(n)).",
				"The ratio A001570(n)/a(n) tends to sqrt(3) = 1.73205... as n increases. - _Pierre CAMI_, Apr 21 2005",
				"Define a(1)=0 a(2)=7 such that 3*(a(1)^2) + 3*a(1) + 1 = j(1)^2 = 1^2 and 3*(a(2)^2) + 3*a(2) + 1 = j(2)^2 = 13^2. Then a(n) = a(n-2) + 8*sqrt(3*(a(n-1)^2) + 3*a(n-1) + 1). Another definition : a(n) such that 3*(a(n)^2) + 3*a(n) + 1 = j(n)^2. - _Pierre CAMI_, Mar 30 2005",
				"a(n) = A001353(n)*A001075(n+1). For n\u003e0, the triple {a(n), a(n)+1=A001922(n), A001570(n)} forms a near-isosceles triangle with angle 2*Pi/3 bounded by the consecutive sides. - _Lekraj Beedassy_, Jul 21 2006",
				"Numbers n such that A003215(n) is a square, cf. A006051. - _Joerg Arndt_, Jan 02 2017"
			],
			"reference": [
				"J. D. E. Konhauser et al., Which Way Did the Bicycle Go?, MAA 1996, p. 104.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A001921/b001921.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"J. Brenner and E. P. Starke, \u003ca href=\"http://www.jstor.org/stable/2306254\"\u003eProblem E702\u003c/a\u003e, Amer. Math. Monthly, 53 (1946), 465.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/HexNumber.html\"\u003eHex Number\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (15,-15,1)."
			],
			"formula": [
				"G.f.: x*(-7 + x)/(x - 1)/(x^2 - 14*x + 1) (see _Simon Plouffe_ in Maple section).",
				"a(n) = -1/2 - (1/6)*sqrt(3)*(7-4*sqrt(3))^n + (1/6)*sqrt(3)*(7+4*sqrt(3))^n + (1/4)*(7+4*sqrt(3))^n + (1/4)*(7-4*sqrt(3))^n. - _Paolo P. Lava_, Jun 19 2008",
				"a(n) = (A028230(n+1)-1)/2. - _R. J. Mathar_, Mar 19 2009",
				"a(n) = 15*a(n-1) - 15*a(n-2) + a(n-3). - _Colin Barker_, Jan 06 2015",
				"a(n) = -1 - a(-1-n) for all n in Z. - _Michael Somos_, Aug 17 2018"
			],
			"example": [
				"G.f. = 7*x + 104*x^2 + 1455*x^3 + 20272*x^4 + 282359*x^5 + 3932760*x^6 + ... - _Michael Somos_, Aug 17 2018"
			],
			"maple": [
				"A001921:=z*(-7+z)/(z-1)/(z**2-14*z+1); # Conjectured by _Simon Plouffe_ in his 1992 dissertation."
			],
			"mathematica": [
				"t = {0, 7}; Do[AppendTo[t, 14*t[[-1]] - t[[-2]] + 6], {20}]; t (* _T. D. Noe_, Aug 17 2012 *)",
				"LinearRecurrence[{15, -15, 1}, {0, 7, 104}, 19] (* _Michael De Vlieger_, Jan 02 2017 *)",
				"a[ n_] := -1/2 + (ChebyshevT[n + 1, 7] - ChebyshevT[n, 7]) / 12; (* _Michael Somos_, Aug 17 2018 *)"
			],
			"program": [
				"(PARI) concat(0, Vec(x*(x-7)/((x-1)*(x^2-14*x+1)) + O(x^100))) \\\\ _Colin Barker_, Jan 06 2015",
				"(PARI) {a(n) = -1/2 + (polchebyshev(n + 1, 1, 7) - polchebyshev(n, 1, 7)) / 12}; /* _Michael Somos_, Aug 17 2018 */",
				"(MAGMA) [Round(-1/2 - (1/6)*Sqrt(3)*(7-4*Sqrt(3))^n + (1/6)*Sqrt(3)*(7+4*Sqrt(3))^n + (1/4)*(7+4*Sqrt(3))^n + (1/4)*(7-4*Sqrt(3))^n): n in [0..50]]; // _G. C. Greubel_, Nov 04 2017"
			],
			"xref": [
				"Cf. A001570, A001922, A006051.",
				"Cf. numbers m such that k*A000217(m)+1 is a square: A006451 for k=1; A233450 for k=3; A001652 for k=4; A129556 for k=5; this sequence for k=6. - _Bruno Berselli_, Dec 16 2013"
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _James A. Sellers_, Jul 04 2000"
			],
			"references": 20,
			"revision": 65,
			"time": "2021-03-12T22:32:35-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}