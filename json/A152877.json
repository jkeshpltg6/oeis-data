{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A152877",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 152877,
			"data": "1,1,2,4,2,16,0,8,60,24,24,12,288,144,216,0,72,1584,1296,1152,576,288,144,10368,9216,10368,4608,4608,0,1152,74880,83520,86400,60480,31680,17280,5760,2880,604800,748800,892800,576000,460800,172800,144000,0,28800",
			"name": "Triangle read by rows: T(n,k) is the number of permutations of {1,2,...,n} having k consecutive triples of the form (odd,even,odd) and (even,odd,even) (0\u003c=k\u003c=n-2).",
			"comment": [
				"Row n has n-1 entries (n\u003e=2).",
				"Sum of entries in row n is n! (A000142(n)).",
				"T(n,0) = A152876(n).",
				"T(n,n-2) = A092186(n).",
				"T(2n+1,2n-2) = A047677(n) = 2*n!*(n+1)!. - _Alois P. Heinz_, Nov 10 2013"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A152877/b152877.txt\"\u003eRows n = 0..142, flattened\u003c/a\u003e",
				"E. Munarini and N. Zagaglia Salvi, \u003ca href=\"http://www.emis.de/journals/SLC/wpapers/s49zagaglia.html\"\u003eBinary strings without zigzags\u003c/a\u003e, Sem. Lotharingien de Combinatoire, 49, 2004, B49h."
			],
			"formula": [
				"It would be good to have a formula or generating function for this sequence (a formula for column 0 is given in A152876).",
				"Sum_{k\u003e=1} k*T(n,k) = A329550(n). - _Alois P. Heinz_, Nov 16 2019"
			],
			"example": [
				"T(3,1) = 2 because we have 123 and 321.",
				"Triangle starts:",
				"      1;",
				"      1;",
				"      2;",
				"      4,    2;",
				"     16,    0,     8;",
				"     60,   24,    24,   12;",
				"    288,  144,   216,    0,   72;",
				"   1584, 1296,  1152,  576,  288, 144;",
				"  10368, 9216, 10368, 4608, 4608,   0, 1152;",
				"  ..."
			],
			"maple": [
				"b:= proc(o, u, t) option remember; `if`(u+o=0, 1, expand(",
				"      o*b(o-1, u, [2, 2, 5, 5, 2][t])*`if`(t=4, x, 1)+",
				"      u*b(o, u-1, [3, 4, 3, 3, 4][t])*`if`(t=5, x, 1)))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=0..degree(p)))(",
				"               b(ceil(n/2), floor(n/2), 1)):",
				"seq(T(n), n=0..12);  # _Alois P. Heinz_, Nov 10 2013"
			],
			"mathematica": [
				"b[o_, u_, t_] := b[o, u, t] = If[u+o == 0, 1, Expand[o*b[o-1, u, {2, 2, 5, 5, 2}[[t]]]*If[t == 4, x, 1] + u*b[o, u-1, {3, 4, 3, 3, 4}[[t]]]*If[t == 5, x, 1]]]; T[n_] := Function[{p}, Table[Coefficient[p, x, i], {i, 0, Exponent[p, x]}]] [b[Ceiling[n/2], Floor[n/2], 1]]; Table[T[n], {n, 0, 12}] // Flatten (* _Jean-François Alcover_, May 27 2015, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A000142, A047677, A152876, A092186, A329550."
			],
			"keyword": "nonn,tabf",
			"offset": "0,3",
			"author": "_Emeric Deutsch_, Dec 17 2008",
			"ext": [
				"More terms from _Alois P. Heinz_, Nov 10 2013"
			],
			"references": 5,
			"revision": 28,
			"time": "2019-11-16T17:01:59-05:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}