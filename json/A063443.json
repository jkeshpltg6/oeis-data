{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A063443",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 63443,
			"data": "1,1,2,5,35,314,6427,202841,12727570,1355115601,269718819131,94707789944544,60711713670028729,69645620389200894313,144633664064386054815370,540156683236043677756331721,3641548665525780178990584908643,44222017282082621251230960522832336",
			"name": "Number of ways to tile an n X n square with 1 X 1 and 2 X 2 tiles.",
			"comment": [
				"a(n) is also the number of ways to populate an n-1 X n-1 chessboard with nonattacking kings (including the case of zero kings). Cf. A193580. - _Andrew Woods_, Aug 27 2011",
				"Also the number of vertex covers and independent vertex sets of the n-1 X n-1 king graph."
			],
			"reference": [
				"S. R. Finch, Mathematical Constants, Cambridge, 2003, p. 343"
			],
			"link": [
				"Andrew Woods and Vaclav Kotesovec and Johan Nilsson, \u003ca href=\"/A063443/b063443.txt\"\u003eTable of n, a(n) for n = 0..40\u003c/a\u003e (terms 0..21 from Andrew Woods, terms 22..24 from Vaclav Kotesovec and terms 25..40 from Johan Nilsson)",
				"Vaclav Kotesovec, \u003ca href=\"https://oeis.org/wiki/User:Vaclav_Kotesovec\"\u003eNon-attacking chess pieces\u003c/a\u003e, 6ed, 2013, p. 68-69.",
				"R. J. Mathar, \u003ca href=\"http://arxiv.org/abs/1609.03964\"\u003eTiling n x m rectangles with 1 x 1 and s x s squares\u003c/a\u003e, arXiv:1609.03964 [math.CO], 2016, Section 4.1.",
				"J. Nilsson, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL20/Nilsson/nilsson15.html\"\u003eOn Counting the Number of Tilings of a Rectangle with Squares of Size 1 and 2\u003c/a\u003e, Journal of Integer Sequences, Vol. 20 (2017), Article 17.2.2.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"https://mathworld.wolfram.com/IndependentVertexSet.html\"\u003eIndependent Vertex Set\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/KingGraph.html\"\u003eKing Graph\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/VertexCover.html\"\u003eVertex Cover\u003c/a\u003e"
			],
			"formula": [
				"Lim_{n -\u003e infinity} (a(n))^(1/n^2) = A247413 = 1.342643951124... . - _Brendan McKay_, 1996"
			],
			"mathematica": [
				"Needs[\"LinearAlgebra`MatrixManipulation`\"] Remove[mat] step[sa[rules1_, {dim1_, dim1_}], sa[rules2_, {dim2_, dim2_}]] := sa[Join[rules2, rules1 /. {x_Integer, y_Integer} -\u003e {x + dim2, y}, rules1 /. {x_Integer, y_Integer} -\u003e {x, y + dim2}], {dim1 + dim2, dim1 + dim2}] mat[0] = sa[{{1, 1} -\u003e 1}, {1, 1}]; mat[1] = sa[{{1, 1} -\u003e 1, {1, 2} -\u003e 1, {2, 1} -\u003e 1}, {2, 2}]; mat[n_] := mat[n] = step[mat[n - 2], mat[n - 1]]; A[n_] := mat[n] /. sa -\u003e SparseArray; F[n_] := MatrixPower[A[n], n + 1][[1, 1]]; (* Mark McClure (mcmcclur(AT)bulldog.unca.edu), Mar 19 2006 *)",
				"$RecursionLimit = 1000; Clear[a, b]; b[n_, l_List] := b[n, l] = Module[{m=Min[l], k}, If[m\u003e0, b[n-m, l-m], If[n == 0, 1, k=Position[l, 0, 1, 1][[1, 1]]; b[n, ReplacePart[l, k -\u003e 1]] + If[n\u003e1 \u0026\u0026 k\u003cLength[l] \u0026\u0026 l[[k+1]] == 0, b[n, ReplacePart[l, {k -\u003e 2, k+1 -\u003e 2}]], 0]]]]; a[n_] := a[n] = If[n\u003c2, 1, b[n, Table[0, {n}]]]; Table[Print[a[n]]; a[n], {n, 0, 17}] (* _Jean-François Alcover_, Dec 11 2014, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A001045, A006506, A054854, A054855, A063650-A063653, A067966, etc.",
				"Cf. A045846, A211348, A247413, A201513.",
				"Cf. A212269, A067958.",
				"a(n) = row sum n-1 of A193580.",
				"Main diagonal of A245013."
			],
			"keyword": "nonn,nice,hard",
			"offset": "0,3",
			"author": "Reiner Martin (reinermartin(AT)hotmail.com), Jul 23 2001",
			"ext": [
				"4 more terms from _R. H. Hardin_, Jan 23 2002",
				"2 more terms from Keith Schneider (kschneid(AT)bulldog.unca.edu), Mar 19 2006",
				"5 more terms from _Andrew Woods_, Aug 27 2011",
				"a(22)-a(24) in b-file from _Vaclav Kotesovec_, May 01 2012",
				"a(0) inserted by _Alois P. Heinz_, Sep 17 2014",
				"a(25)-a(40) in b-file from _Johan Nilsson_, Mar 10 2016"
			],
			"references": 29,
			"revision": 97,
			"time": "2020-08-24T11:56:43-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}