{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A179046",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 179046,
			"data": "1,0,0,1,1,1,1,1,1,2,2,3,3,4,4,5,5,6,7,8,9,11,12,14,16,18,20,23,25,28,32,35,39,44,49,54,61,67,75,83,92,101,113,123,136,150,165,180,199,217,239,261,286,312,343,373,408,445,486,528,577,626,682,741,805,873,949,1027,1114",
			"name": "Partitions into distinct parts with minimal difference 3 and minimal part \u003e= 3.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A179046/b179046.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e"
			],
			"formula": [
				"G.f.: sum(n\u003e=0, x^(3*n*(n+1)/2) / prod(k=1,n,1-x^k) ), this is a special case of the g.f. sum(n\u003e=0, x^(D*n*(n+1)/2) / prod(k=1,n,1-x^k) ) for partitions into distinct parts with minimal difference D and minimal part \u003e= D. - _Joerg Arndt_, Apr 07 2011",
				"The g.f. for partitions into distinct parts with minimal difference D and no restriction on the minimal part is sum(n\u003e=0, x^(D*n*(n+1)/2 - (D-1)*n) / prod(k=1..n, 1-x^k) ). - _Joerg Arndt_, Mar 31 2014"
			],
			"example": [
				"a(13)=4 because there are 4 such partitions of 13: 3+10=4+9=5+8=13.",
				"a(0)=1 because the condition is void for the empty list."
			],
			"maple": [
				"b:= proc(n, i) option remember; `if`(n=0, 1,",
				"      `if`(n\u003ei*(i+1)/2-3, 0, b(n, i-1)+",
				"      `if`(i\u003en, 0, b(n-i, i-3))))",
				"    end:",
				"a:= n-\u003e b(n$2):",
				"seq(a(n), n=0..80);  # _Alois P. Heinz_, Apr 02 2014"
			],
			"mathematica": [
				"b[n_, i_] := b[n, i] = If[n == 0, 1,",
				"  If[n \u003e i(i+1)/2 - 3, 0, b[n, i - 1] +",
				"  If[i \u003e n, 0, b[n - i, i - 3]]]];",
				"a[n_] := b[n, n];",
				"a /@ Range[0, 80] (* _Jean-François Alcover_, Nov 20 2020, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(Sage)",
				"A179046 = lambda n: Partitions(n,max_slope=-3).filter(lambda x: not x or min(x) \u003e= 3).cardinality() # _D. S. McNeil_, Jan 04 2011",
				"(PARI)",
				"N=66; x='x+O('x^N);",
				"gf = sum(n=0,N, x^(3*n*(n+1)/2)/prod(k=1,n,1-x^k));",
				"v = Vec(gf)",
				"/* _Joerg Arndt_, Apr 07 2011 */"
			],
			"xref": [
				"Cf. A003106 (min diff=2, min part=2), A000009 (min diff=1, min part=1).",
				"Cf. A003114 (min diff=2), A025157 (min diff=3), A025158 (min diff=4), A025159 (min diff=5), A025160 (min diff=6), A025161 (min diff=7), A025162 (min diff=8)."
			],
			"keyword": "nonn",
			"offset": "0,10",
			"author": "_Joerg Arndt_, Jan 04 2011",
			"references": 1,
			"revision": 34,
			"time": "2020-11-20T05:53:47-05:00",
			"created": "2010-11-12T14:28:32-05:00"
		}
	]
}