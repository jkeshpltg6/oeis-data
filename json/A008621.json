{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A008621",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 8621,
			"data": "1,1,1,1,2,2,2,2,3,3,3,3,4,4,4,4,5,5,5,5,6,6,6,6,7,7,7,7,8,8,8,8,9,9,9,9,10,10,10,10,11,11,11,11,12,12,12,12,13,13,13,13,14,14,14,14,15,15,15,15,16,16,16,16,17,17,17,17,18,18,18,18,19,19,19,19,20,20,20,20,21,21",
			"name": "Expansion of 1/((1-x)*(1-x^4)).",
			"comment": [
				"Arises from Gleason's theorem on self-dual codes: 1/((1-x^2)*(1-x^8)) is the Molien series for the real 2-dimensional Clifford group (a dihedral group of order 16) of genus 1.",
				"Thickness of the hypercube graph Q_n. - _Eric W. Weisstein_, Sep 09 2008",
				"Count of odd numbers between consecutive quarter-squares, A002620. Oppermann's conjecture states that for each count there will be at least one prime. - _Fred Daniel Kline_, Sep 10 2011",
				"Number of partitions into parts 1 and 4. - _Joerg Arndt_, Jun 01 2013"
			],
			"reference": [
				"D. J. Benson, Polynomial Invariants of Finite Groups, Cambridge, 1993, p. 100.",
				"F. J. MacWilliams and N. J. A. Sloane, Theory of Error-Correcting Codes, 1977, Chapter 19, Problem 3, p. 602."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A008621/b008621.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=211\"\u003eEncyclopedia of Combinatorial Structures 211\u003c/a\u003e",
				"G. Nebe, E. M. Rains and N. J. A. Sloane, \u003ca href=\"http://neilsloane.com/doc/cliff2.html\"\u003eSelf-Dual Codes and Invariant Theory\u003c/a\u003e, Springer, Berlin, 2006.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/GraphThickness.html\"\u003eGraph Thickness\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Oppermann%27s_conjecture\"\u003eOppermann's Conjecture\u003c/a\u003e",
				"\u003ca href=\"/index/Mo#Molien\"\u003eIndex entries for Molien series\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,0,0,1,-1)."
			],
			"formula": [
				"a(n) = floor(n/4) + 1.",
				"a(n) = A010766(n+4, 4).",
				"a(n) = {Sum{k=0..n, (k+1)cos(pi*(n-k)/2}+1/4[cos(n*Pi/2)+1+(-1)^n] }/2 - _Paolo P. Lava_, Oct 09 2006",
				"Also, a(n) = ceiling((n+1)/4), n \u003e= 0. - _Mohammad K. Azarian_, May 22 2007",
				"a(n) = Sum_{i=0..n} A121262(i) = n/4 +5/8 +(-1)^n/8 + A057077(n)/4. - _R. J. Mathar_, Mar 14 2011",
				"a(x,y):= floor(x/2) + floor(y/2) - x where x=A002620(n) and y=A002620(n+1), n\u003e2. - _Fred Daniel Kline_, Sep 10 2011",
				"a(0)=1, a(1)=1, a(2)=1, a(3)=1, a(4)=2, a(n) = a(n-1) + a(n-4) - a(n-5). - _Harvey P. Dale_, Feb 19 2012",
				"G.f.: 1 / ( (1+x)*(1+x^2)*(x-1)^2 ). - _R. J. Mathar_, Jun 04 2021",
				"a(n)+a(n-1) = A004524(n+3). a(n)+a(n-2)=A008619(n). - _R. J. Mathar_, Jun 04 2021"
			],
			"mathematica": [
				"Table[Floor[n/4]+1, {n, 0, 80}] (* _Stefan Steinerberger_, Apr 03 2006 *)",
				"CoefficientList[Series[1/((1-x)(1-x^4)),{x,0,80}],x] (* _Harvey P. Dale_, Feb 19 2012 *)",
				"Flatten[ Table[ PadRight[{},4,n],{n,19}]] (* _Harvey P. Dale_, Feb 19 2012 *)"
			],
			"program": [
				"(PARI) a(n)=n\\4+1 \\\\ _Charles R Greathouse IV_, Feb 06 2017"
			],
			"xref": [
				"Cf. A008718, A024186, A110160, A110868, A110869, A110876, A110880, A002265, A008620."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Stefan Steinerberger_, Apr 03 2006"
			],
			"references": 24,
			"revision": 69,
			"time": "2021-06-04T14:45:29-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}