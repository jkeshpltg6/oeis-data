{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A093683",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 93683,
			"data": "4,25,174,1270,10250,86027,738597,6497407,58047180,524733511",
			"name": "Number of pairs of twin primes \u003c= 10^n-th prime.",
			"comment": [
				"This sequence is \u003e= the values of pi(10^n): 4, 25, 168, 1229, . . . in A006880."
			],
			"reference": [
				"Enoch Haga, \"Wandering through a prime number desert,\" Table 6, in Exploring prime numbers on your PC and the Internet, 2001 (ISBN 1-885794-17-7)."
			],
			"link": [
				"Soren Laing Aletheia-Zomlefer, Lenny Fukshansky, and Stephan Ramon Garcia, \u003ca href=\"https://arxiv.org/abs/1807.08899\"\u003eThe Bateman-Horn Conjecture: Heuristics, History, and Applications\u003c/a\u003e, arXiv:1807.08899 [math.NT], 2018-2019. See Table 5 p. 40.",
				"Thomas R. Nicely, \u003ca href=\"https://faculty.lynchburg.edu/~nicely/counts.html\"\u003eTwin prime count\u003c/a\u003e.",
				"\u003ca href=\"/index/Pri#primepop\"\u003eIndex entries for sequences related to numbers of primes in various ranges\u003c/a\u003e"
			],
			"formula": [
				"Count twin primes \u003c= p_{10^n}: 10th prime, 100th prime, etc."
			],
			"example": [
				"a(1) = 4 because there are 4 twin primes \u003c= 29, the 10th prime: (3,5), (5,7), (11,13), and (17,19). (29,31) is not counted because it is not entirely \u003c= 29."
			],
			"mathematica": [
				"NextPrim[n_] := Block[{k = n + 1}, While[ !PrimeQ[k], k++ ]; k]; c = 0; p = q = 1; Do[l = Prime[10^n]; While[q \u003c= l, If[p + 2 == q, c++ ]; p = q; q = NextPrim[p]]; Print[c], {n, 12}] (* _Robert G. Wilson v_, Apr 10 2004 *)"
			],
			"program": [
				"(Python)",
				"from sympy import prime, sieve # use primerange for larger terms",
				"def afind(terms):",
				"  c, prevp = 0, 1",
				"  for n in range(1, terms+1):",
				"    for p in sieve.primerange(prevp+1, prime(10**n)+1):",
				"      if prevp == p - 2: c += 1",
				"      prevp = p",
				"    print(c, end=\", \")",
				"afind(6) # _Michael S. Branicky_, Apr 25 2021"
			],
			"xref": [
				"See A049035 for another version. - _R. J. Mathar_, Sep 05 2008",
				"Cf. A006880, A007508, A049035."
			],
			"keyword": "nonn,more",
			"offset": "1,1",
			"author": "_Enoch Haga_, Apr 09 2004",
			"ext": [
				"a(9) from _Michael S. Branicky_, Apr 25 2021",
				"a(10) from _Eduard Roure Perdices_, May 08 2021"
			],
			"references": 2,
			"revision": 35,
			"time": "2021-10-28T13:09:16-04:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}