{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A052857",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 52857,
			"data": "0,1,2,15,184,3145,68976,1846999,58413440,2130740721,88061420800,4066862460991,207556068584448,11600364266112505,704664527894104064,46226086991634882375,3256882066245640093696,245279323467051422886241",
			"name": "A simple grammar.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A052857/b052857.txt\"\u003eTable of n, a(n) for n = 0..250\u003c/a\u003e",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=825\"\u003eEncyclopedia of Combinatorial Structures 825\u003c/a\u003e",
				"\u003ca href=\"/index/La#Laguerre\"\u003eIndex entries for sequences related to Laguerre polynomials\u003c/a\u003e"
			],
			"formula": [
				"E.g.f.: exp(RootOf(exp(_Z)*x*_Z+exp(_Z)*x-_Z))*x.",
				"a(n) = (n-1)!*Sum_{k=1..n-1} n^k*binomial(n-2,k-1)/k!, a(1)=1. - _Vladimir Kruchinin_, May 10 2011",
				"a(n) = n!*hypergeom([-n+2], [2], -n) for n\u003e=2. - _Peter Luschny_, Apr 20 2016",
				"a(n) ~ exp(n/phi - n) * phi^(2*n-2) * n^(n-1) / 5^(1/4), where phi = A001622 = (1+sqrt(5))/2 is the golden ratio. - _Vaclav Kotesovec_, Oct 01 2017",
				"E.g.f. A(x) satisfies: A(x) = x*exp(A(x)/(1 - A(x))). - _Ilya Gutkovskiy_, Apr 04 2019",
				"a(n) = n * (n-2)! * LaguerreL(n-2, 1, -n) with a(0) = 0 and a(1) = 1. - _G. C. Greubel_, Feb 23 2021"
			],
			"maple": [
				"spec := [S,{C=Set(B),S=Prod(Z,C),B=Sequence(S,1\u003c= card)},labeled]:",
				"seq(combstruct[count](spec,size=n), n=0..20);",
				"# Alternatively:",
				"a := n -\u003e `if`(n\u003c2,n, n!*hypergeom([-n+2],[2],-n));",
				"seq(simplify(a(n)), n=0..17); # _Peter Luschny_, Apr 20 2016"
			],
			"mathematica": [
				"Table[If[0\u003c=n\u003c=1, n, (n-1)! Sum[(n^k Binomial[n-2, k-1])/k!, {k,n-1}]], {n,0,20}] (* _Michael De Vlieger_, Apr 20 2016 *)",
				"Table[If[n\u003c2, n, n*(n-2)!*LaguerreL[n-2, 1, -n]], {n, 0, 20}] (* _G. C. Greubel_, Feb 23 2021 *)"
			],
			"program": [
				"(Maxima)",
				"a(n):=if n=1 then 1 else ((n-1)!*sum((n^k*binomial(n-2,k-1))/k!,k,1,n-1)); \\\\ _Vladimir Kruchinin_, May 10 2011",
				"(Sage) [n if n\u003c2 else n*factorial(n-2)*gen_laguerre(n-2, 1, -n) for n in (0..20)] # _G. C. Greubel_, Feb 23 2021",
				"(Magma) [n lt 2 select n else n*Factorial(n-2)*Evaluate(LaguerrePolynomial(n-2, 1), -n): n in [0..20]]; // _G. C. Greubel_, Feb 23 2021"
			],
			"keyword": "easy,nonn",
			"offset": "0,3",
			"author": "encyclopedia(AT)pommard.inria.fr, Jan 25 2000",
			"references": 2,
			"revision": 30,
			"time": "2021-02-24T08:19:53-05:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}