{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A034839",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 34839,
			"data": "1,1,1,1,1,3,1,6,1,1,10,5,1,15,15,1,1,21,35,7,1,28,70,28,1,1,36,126,84,9,1,45,210,210,45,1,1,55,330,462,165,11,1,66,495,924,495,66,1,1,78,715,1716,1287,286,13",
			"name": "Triangular array formed by taking every other term of each row of Pascal's triangle.",
			"comment": [
				"Number of compositions of n having k parts greater than 1. Example: T(5,2)=5 because we have 3+2, 2+3, 2+2+1, 2+1+2 and 1+2+2. Number of binary words of length n-1 having k runs of consecutive 1's. Example: T(5,2)=5 because we have 1010, 1001, 0101, 1101 and 1011. - _Emeric Deutsch_, Mar 30 2005",
				"From _Gary W. Adamson_, Oct 17 2008: (Start)",
				"Received from _Herb Conn_:",
				"Let T = tan x, then",
				"tan x = T",
				"tan 2x = 2T / (1 - T^2)",
				"tan 3x = (3T - T^3) / (1 - 3T^2)",
				"tan 4x = (4T - 4T^3) / (1 - 6T^2 + T^4)",
				"tan 5x = (5T - 10T^3 + T^5) / (1 - 10T^2 + 5T^4)",
				"tan 6x = (6T - 20T^3 + 6T^5) / (1 - 15T^2 + 15T^4 - T^6)",
				"tan 7x = (7T - 35T^3 + 21T^5 - T^7) / (1 - 21T^2 + 35T^4 - 7T^6)",
				"tan 8x = (8T - 56T^3 + 56T^5 - 8T^7) / (1 - 28T^2 + 70T^4 - 28T^6 + T^8)",
				"tan 9x = (9T - 84T^3 + 126T^5 - 36T^7 + T^9) / (1 - 36 T^2 + 126T^4 - 84T^6 + 9T^8)",
				"... To get the next one in the series, (tan 10x), for the numerator add:",
				"9....84....126....36....1 previous numerator +",
				"1....36....126....84....9 previous denominator =",
				"10..120....252...120...10 = new numerator",
				"For the denominator add:",
				"......9.....84...126...36...1 = previous numerator +",
				"1....36....126....84....9.... = previous denominator =",
				"1....45....210...210...45...1 = new denominator",
				"...where numerators = A034867, denominators = A034839",
				"(End)",
				"Triangle, with zeros omitted, given by (1, 0, 1, 0, 0, 0, 0, 0, 0, 0, ...) DELTA (0, 1, -1, 0, 0, 0, 0, 0, 0, 0, ...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, Dec 12 2011",
				"The row (1,66,495,924,495,66,1) plays a role in expansions of powers of the Dedekind eta function. See the Chan link, p. 534. - _Tom Copeland_, Dec 12 2016",
				"Binomial(n,2k) is also the number of permutations avoiding both 123 and 132 with k ascents, i.e., positions with w[i]\u003cw[i+1]. - _Lara Pudwell_, Dec 19 2018",
				"Coefficients in expansion of ((x-1)^n+(x+1)^n)/2 or ((x-i)^n+(x+i)^n)/2 with alternating sign. - _Eugeniy Sokol_, Sep 20 2020"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A034839/b034839.txt\"\u003eTable of n, a(n) for the first 101 rows, flattened\u003c/a\u003e",
				"M. Bukata, R. Kulwicki, N. Lewandowski, L. Pudwell, J. Roth, and T. Wheeland, \u003ca href=\"https://arxiv.org/abs/1812.07112\"\u003eDistributions of Statistics over Pattern-Avoiding Permutations\u003c/a\u003e, arXiv preprint arXiv:1812.07112 [math.CO], 2018.",
				"H. Chan, S. Cooper, and P. Toh, \u003ca href=\"http://dx.doi.org/10.1016/j.aim.2005.12.003\"\u003eThe 26th power of Dedekind's eta function\u003c/a\u003e Advances in Mathematics, 207 (2006) 532-543.",
				"T. Copeland, \u003ca href=\"https://tcjpn.wordpress.com/2020/07/15/juggling-zeros-in-the-matrix-example-ii/\"\u003eJuggling Zeros in the Matrix: Example II\u003c/a\u003e, 2020.",
				"C. Corsani, D. Merlini, and R. Sprugnoli, \u003ca href=\"http://dx.doi.org/10.1016/S0012-365X(97)00110-6\"\u003eLeft-inversion of combinatorial sums\u003c/a\u003e Discrete Mathematics, 180 (1998) 107-122.",
				"S.-M. Ma, \u003ca href=\"http://arxiv.org/abs/1205.0735\"\u003eOn some binomial coefficients related to the evaluation of tan(nx)\u003c/a\u003e, arXiv preprint arXiv:1205.0735 [math.CO], 2012. - From _N. J. A. Sloane_, Oct 13 2012",
				"K. Oliver and H. Prodinger, The continued fraction expansion of Gauss' hypergeometric function and a new application to the tangent function, Transactions of the Royal Society of South Africa, Vol. 76 (2012), 151-154, \u003ca href=\"http://dx.doi.org/10.1080/0035919X.2012.727363\"\u003e[DOI]\u003c/a\u003e; \u003ca href=\"http://math.sun.ac.za/~hproding/pdffiles/Avery-contribution-July-2012.pdf\"\u003e[PDF]\u003c/a\u003e. - From _N. J. A. Sloane_, Jan 03 2013",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Tangent.html\"\u003eTangent\u003c/a\u003e [From _Eric W. Weisstein_, Oct 18 2008]"
			],
			"formula": [
				"E.g.f.: exp(x)*cosh(x*sqrt(y)). - _Vladeta Jovovic_, Mar 20 2005",
				"From _Emeric Deutsch_, Mar 30 2005: (Start)",
				"T(n, k) = binomial(n, 2*k), for n \u003e= 0 and k = 0, 1, ... floor(n/2).",
				"G.f.: (1-z)/((1-z)^2 - t*z^2). (End)",
				"O.g.f. for column no. k is (1/(1-x))*(x/(1-x))^(2*k), k \u003e= 0 [from the g.f. given in the preceding formula]. - _Wolfdieter Lang_, Jan 18 2013",
				"From _Peter Bala_, Jul 14 2015: (Start)",
				"Stretched Riordan array ( 1/(1 - x ), x^2/(1 - x)^2 ) in the terminology of Corsani et al.",
				"Denote this array by P. Then P * A007318 = A201701.",
				"P * transpose(P) is A119326 read as a square array.",
				"Let Q denote the array ( (-1)^k*binomial(2*n,k) )n,k\u003e=0. Q is a signed version of A034870. Then Q*P = the identity matrix, that is, Q is a left-inverse array of P (see Corsani et al., p. 111).",
				"P * A034870 = A080928. (End)",
				"Even rows are A086645. An aerated version of this array is A099174 with each diagonal divided by the first element of the diagonal, the double factorials A001147. - _Tom Copeland_, Dec 12 2015"
			],
			"example": [
				"Triangluar array T(n, k) begins:",
				"  1",
				"  1",
				"  1  1",
				"  1  3",
				"  1  6  1",
				"  1 10  5",
				"  1 15 15 1 ...",
				"- _Philippe Deléham_, Dec 12 2011"
			],
			"maple": [
				"for n from 0 to 13 do seq(binomial(n,2*k),k=0..floor(n/2)) od;# yields sequence in triangular form; # _Emeric Deutsch_, Mar 30 2005"
			],
			"mathematica": [
				"u[1, x_] := 1; v[1, x_] := 1; z = 12;",
				"u[n_, x_] := u[n - 1, x] + x*v[n - 1, x]",
				"v[n_, x_] := u[n - 1, x] + v[n - 1, x]",
				"cu = Table[CoefficientList[u[n, x], x], {n, 1, z}];",
				"TableForm[cu]  (* A034839 as a triangle *)",
				"cv = Table[CoefficientList[v[n, x], x], {n, 1, z}];",
				"TableForm[cv]  (* A034867 as a triangle *)",
				"(* _Clark Kimberling_, Feb 18 2012 *)",
				"Table[Binomial[n, k], {n, 0, 13}, {k, 0, Floor[n, 2], 2}] // Flatten (* _Michael De Vlieger_, Dec 13 2016 *)"
			],
			"program": [
				"(PARI) for(n=0,15, for(k=0,floor(n/2), print1(binomial(n, 2*k), \", \"))) \\\\ _G. C. Greubel_, Feb 23 2018",
				"(MAGMA) /* As a triangle */ [[Binomial(n,2*k):k in [0..Floor(n/2)]] : n in [0..10]]; // _G. C. Greubel_, Feb 23 2018"
			],
			"xref": [
				"Cf. A007318, A034867, A034870, A080928, A119326, A201701.",
				"Cf. A008619 (row lengths), A086645."
			],
			"keyword": "nonn,easy,tabf",
			"offset": "0,6",
			"author": "_N. J. A. Sloane_",
			"references": 36,
			"revision": 103,
			"time": "2020-09-19T23:23:47-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}