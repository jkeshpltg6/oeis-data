{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A324478",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 324478,
			"data": "1,6,252,18480,1801800,209513304,27485041584,3937652896320,603400560305400,97512510301206000,16452310738019476320,2876570958459008603520,518262201015698050067520,95794174581229987212924000,18101994022606737439599480000",
			"name": "a(n) = (6/((n+1)*(n+2)*(n+3))) * multinomial(4*n;n,n,n,n).",
			"comment": [
				"Theorem (Luis Fredes, Mar 04 2019): (Start)",
				"a(n) is an integer for all n \u003e= 0.",
				"Proof:",
				"a(n) = (6/((n+1)*(n+2)*(n+3)))*multinomial(4*n;n,n,n,n) = multinomial(4*n;n,n,n,n) - multinomial(4*n;n+3,n-3,n,n) + 3*multinomial(4*n;n+2,n-2,n,n) + 15*multinomial(4*n;n+3,n-2,n-1,n) - 18*multinomial(4*n;n+3,n-1,n-1,n-1).",
				"The right hand side is equal (after some manipulation) to",
				"    (f(n)/((n+1)*(n+2)*(n+3)))*multinomial(4*n;n,n,n,n)",
				"where f(n) = (n+1)*(n+2)*(n+3) - (n-1)*(n-2)*n + 3*(n-1)*n*(n+3) + 15*(n-1)*n^2 - 18*n^3. QED",
				"(End)"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A324478/b324478.txt\"\u003eTable of n, a(n) for n = 0..400\u003c/a\u003e",
				"Luis Fredes, Avelio Sepulveda, \u003ca href=\"https://arxiv.org/abs/1901.04981\"\u003eTree-decorated planar maps\u003c/a\u003e, arXiv:1901.04981 [math.CO], 2019. See Remark 4.6."
			],
			"formula": [
				"From _Vaclav Kotesovec_, Jul 21 2019: (Start)",
				"For n\u003e0, a(n) = 6*(4*n)! / ((n!)^3 * (n+3)!).",
				"a(n) ~ 6 * 2^(8*n - 1/2) / (Pi^(3/2) * n^(9/2)). (End)"
			],
			"maple": [
				"a:= n-\u003e 6*combinat[multinomial](4*n, n$4)/((n+1)*(n+2)*(n+3)):",
				"seq(a(n), n=0..20);  # _Alois P. Heinz_, Mar 11 2019"
			],
			"mathematica": [
				"c[m_,n_]:=2m Product[1/(n+i), {i, m}] (Multinomial@@ConstantArray[n, m+1]);{1}~Join~Array[c[3, #]\u0026, 20] (* _Vincenzo Librandi_, Mar 11 2019 *)",
				"Flatten[{1, Table[6*(4*n)! / ((n!)^3 * (n+3)!), {n, 1, 15}]}] (* _Vaclav Kotesovec_, Jul 21 2019 *)"
			],
			"xref": [
				"Cf. A324152."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Mar 10 2019, following a suggestion from Luis Fredes and Avelio Sepulveda.",
			"references": 2,
			"revision": 15,
			"time": "2019-07-21T03:22:46-04:00",
			"created": "2019-03-10T00:55:11-05:00"
		}
	]
}