{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A079314",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 79314,
			"data": "1,2,2,4,2,4,4,10,2,4,4,10,4,10,10,28,2,4,4,10,4,10,10,28,4,10,10,28,10,28,28,82,2,4,4,10,4,10,10,28,4,10,10,28,10,28,28,82,4,10,10,28,10,28,28,82,10,28,28,82,28,82,82,244,2,4,4,10,4,10,10,28,4,10,10,28,10,28,28,82,4",
			"name": "Number of first-quadrant cells (including the two boundaries) born at stage n of the Holladay-Ulam cellular automaton.",
			"comment": [
				"See the main entry for this CA, A147562, for further information.",
				"When I first read the Singmaster MS in 2003 I misunderstood the definition of the CA. In fact once cells are ON they stay ON. The other version, when cells can change state from ON to OFF, is described in A079317. - _N. J. A. Sloane_, Aug 05 2009",
				"The pattern has 4-fold symmetry; sequence just counts cells in one quadrant."
			],
			"reference": [
				"D. Singmaster, On the cellular automaton of Ulam and Warburton, M500 Magazine of the Open University, #195 (December 2003), pp. 2-7."
			],
			"link": [
				"David Applegate, Omar E. Pol and N. J. A. Sloane, \u003ca href=\"/A000695/a000695_1.pdf\"\u003eThe Toothpick Sequence and Other Sequences from Cellular Automata\u003c/a\u003e, Congressus Numerantium, Vol. 206 (2010), 157-191. [There is a typo in Theorem 6: (13) should read u(n) = 4.3^(wt(n-1)-1) for n \u003e= 2.]",
				"Omar E. Pol, \u003ca href=\"http://www.polprimos.com/imagenespub/polca032.jpg\"\u003eIllustration of initial terms (Overlapping squares)\u003c/a\u003e [From _Omar E. Pol_, Nov 20 2009]",
				"D. Singmaster, \u003ca href=\"/A079314/a079314.pdf\"\u003eOn the cellular automaton of Ulam and Warburton\u003c/a\u003e, 2003 [Cached copy, included with permission]",
				"N. J. A. Sloane, \u003ca href=\"/wiki/Catalog_of_Toothpick_and_CA_Sequences_in_OEIS\"\u003eCatalog of Toothpick and Cellular Automata Sequences in the OEIS\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"http://arxiv.org/abs/1503.01168\"\u003eOn the Number of ON Cells in Cellular Automata\u003c/a\u003e, arXiv:1503.01168, 2015"
			],
			"formula": [
				"For n \u003e 0, a(n) = 3^(A000120(n)-1) + 1.",
				"For n \u003e 0, a(n) = A147582(n)/4 + 1.",
				"Partial sums give A151922. [From _Omar E. Pol_, Nov 20 2009]"
			],
			"example": [
				"Contribution from _Omar E. Pol_, Jul 18 2009: (Start)",
				"If written as a triangle:",
				"1;",
				"2;",
				"2,4;",
				"2,4,4,10;",
				"2,4,4,10,4,10,10,28;",
				"2,4,4,10,4,10,10,28,4,10,10,28,10,28,28,82;",
				"2,4,4,10,4,10,10,28,4,10,10,28,10,28,28,82,4,10,10,28,10,28,28,82,10,28;...",
				"Rows converge to A151712.",
				"(End)"
			],
			"xref": [
				"Cf. A147582, A079315-A079319, A151713, A139250.",
				"Cf. A151922, A160407. [From _Omar E. Pol_, Nov 20 2009]"
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Feb 12 2003",
			"ext": [
				"Edited by _N. J. A. Sloane_, Aug 05 2009"
			],
			"references": 16,
			"revision": 29,
			"time": "2021-02-24T02:48:18-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}