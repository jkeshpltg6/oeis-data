{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A174151",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 174151,
			"data": "1,1,1,1,12,1,1,60,60,1,1,180,900,180,1,1,420,6300,6300,420,1,1,840,29400,88200,29400,840,1,1,1512,105840,740880,740880,105840,1512,1,1,2520,317520,4445280,10372320,4445280,317520,2520,1,1,3960,831600,20956320,97796160,97796160,20956320,831600,3960,1",
			"name": "Triangle T(n, k) = c(n)/(c(k)*c(n-k)) where c(n) = 12*Product_{j=3..n} ( 12 * binomial(j+2, 4) ) with c(0) = c(1) = 1, read by rows.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A174151/b174151.txt\"\u003eRows n = 0..50 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = c(n)/(c(k)*c(n-k)) where c(n) = 12*Product_{j=3..n} ( 12 * binomial(j+2, 4) ) with c(0) = c(1) = 1.",
				"T(n, k) = (6*k/((k+1)^2*(k+2))*Product_{j=0..3} binomial(n+j-1, k) with T(n, 0) = T(n, n) = 1. - _G. C. Greubel_, Apr 16 2021"
			],
			"example": [
				"Triangle begins as:",
				"  1;",
				"  1,    1;",
				"  1,   12,      1;",
				"  1,   60,     60,        1;",
				"  1,  180,    900,      180,        1;",
				"  1,  420,   6300,     6300,      420,        1;",
				"  1,  840,  29400,    88200,    29400,      840,        1;",
				"  1, 1512, 105840,   740880,   740880,   105840,     1512,      1;",
				"  1, 2520, 317520,  4445280, 10372320,  4445280,   317520,   2520,    1;",
				"  1, 3960, 831600, 20956320, 97796160, 97796160, 20956320, 831600, 3960, 1;"
			],
			"mathematica": [
				"(* First program *)",
				"c[n_]:= If[n\u003c2, 1, 12*Product[i*(i^2-1)*(i+2)/2], {i,3,n}]];",
				"T[n_, m_] = c[n]/(c[m]*c[n-m]);",
				"Table[T[n, m], {n, 0, 10}, {m,0,n}]//Flatten",
				"(* Second program *)",
				"T[n_, k_]:= If[k==0 || k==n, 1, (6*k/((k+1)^2*(k+2)))*Product[Binomial[n+j-1,k], {j,0,3}] ];",
				"Table[T[n, k], {n, 0, 10}, {k, 0, n}]//Flatten (* _G. C. Greubel_, Apr 16 2021 *)"
			],
			"program": [
				"(Magma)",
				"function T(n,k)",
				"  if k eq 0 or k eq n then return 1;",
				"  else return (6*k/((k+1)^2*(k+2)))*(\u0026*[Binomial(n+j-1,k): j in [0..3]]);",
				"  end if; return T;",
				"end function;",
				"[T(n,k): k in [0..n], n in [0..12]]; // _G. C. Greubel_, Apr 16 2021",
				"(Sage)",
				"@CachedFunction",
				"def T(n, k):",
				"    if (k==0 or k==n): return 1",
				"    else: return (6*k/((k+1)^2*(k+2)))*product(binomial(n+j-1,k) for j in (0..3))",
				"flatten([[T(n, k) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, Apr 16 2021"
			],
			"xref": [
				"Cf. A174150."
			],
			"keyword": "nonn,tabl,easy,less",
			"offset": "0,5",
			"author": "_Roger L. Bagula_, Mar 10 2010",
			"ext": [
				"Edited by _G. C. Greubel_, Apr 16 2021"
			],
			"references": 2,
			"revision": 8,
			"time": "2021-04-16T02:29:06-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}