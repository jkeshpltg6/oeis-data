{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A208747",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 208747,
			"data": "1,1,2,1,2,8,1,2,12,24,1,2,16,40,80,1,2,20,56,160,256,1,2,24,72,256,576,832,1,2,28,88,368,992,2112,2688,1,2,32,104,496,1504,3968,7552,8704,1,2,36,120,640,2112,6464,15232,26880,28160,1,2,40,136,800",
			"name": "Triangle of coefficients of polynomials u(n,x) jointly generated with A208748; see the Formula section.",
			"comment": [
				"For a discussion and guide to related arrays, see A208510.",
				"Subtriangle of the triangle T(n,k) given by (1, 0, -1, 1, 0, 0, 0, 0, 0, 0, 0, ...) DELTA (0, 2, 2, -2, 0, 0, 0, 0, 0, 0, 0, ...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, Mar 14 2012"
			],
			"formula": [
				"u(n,x)=u(n-1,x)+2x*v(n-1,x),",
				"v(n,x)=2x*u(n-1,x)+2x*v(n-1,x),",
				"where u(1,x)=1, v(1,x)=1.",
				"T(n,k) = A208342(n,k)*2^k. - _Philippe Deléham_, Mar 05 2012",
				"T(n,k) = T(n-1,k) + 2*T(n-1,k-1) - 2*T(n-2,k-1) + 4*T(n-2,k-2), T(1,0) = T(2,0) = 1, T(2,1) = 2, T(n,k) = 0 if k\u003c0 or if k\u003e=n. - _Philippe Deléham_, Mar 14 2012",
				"G.f.: -x*y/(-1+2*x*y-2*x^2*y+4*x^2*y^2+x). - _R. J. Mathar_, Aug 11 2015"
			],
			"example": [
				"First five rows:",
				"1",
				"1...2",
				"1...2...8",
				"1...2...12...24",
				"1...2...16...40...80",
				"First five polynomials u(n,x):",
				"1",
				"1 + 2x",
				"1 + 2x + 8x^2",
				"1 + 2x + 12x^2 + 24x^3",
				"1 + 2x + 16x^2 + 40x^3 + 80x^4",
				"(1, 0, -1, 1, 0, 0, ...) DELTA (0, 0, 0, -2, 0, 0, ...) begins :",
				"1",
				"1, 0",
				"1, 2, 0",
				"1, 2, 8, 0",
				"1, 2, 12, 24, 0",
				"1, 2, 16, 40, 80, 0",
				"1, 2, 20, 56, 160, 256, 0",
				"1, 2, 24, 72, 256, 576, 832, 0. - _Philippe Deléham_, Mar 14 2012"
			],
			"mathematica": [
				"u[1, x_] := 1; v[1, x_] := 1; z = 16;",
				"u[n_, x_] := u[n - 1, x] + 2 x*v[n - 1, x];",
				"v[n_, x_] := 2 x*u[n - 1, x] + 2 x*v[n - 1, x];",
				"Table[Expand[u[n, x]], {n, 1, z/2}]",
				"Table[Expand[v[n, x]], {n, 1, z/2}]",
				"cu = Table[CoefficientList[u[n, x], x], {n, 1, z}];",
				"TableForm[cu]",
				"Flatten[%]    (* A208747 *)",
				"Table[Expand[v[n, x]], {n, 1, z}]",
				"cv = Table[CoefficientList[v[n, x], x], {n, 1, z}];",
				"TableForm[cv]",
				"Flatten[%]    (* A208748 *)"
			],
			"xref": [
				"Cf. A208748, A208510."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Clark Kimberling_, Mar 02 2012",
			"references": 4,
			"revision": 16,
			"time": "2015-08-11T13:54:40-04:00",
			"created": "2012-03-02T14:55:58-05:00"
		}
	]
}