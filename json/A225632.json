{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A225632",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 225632,
			"data": "1,1,2,1,3,6,1,4,12,1,6,30,60,1,6,30,60,1,12,84,420,1,15,120,840,1,20,180,1260,2520,1,30,210,840,2520,1,30,420,4620,13860,27720,1,60,660,4620,13860,27720,1,60,780,8580,60060,180180,360360",
			"name": "Irregular table read by rows: n-th row gives distinct values of successively iterated Landau-like functions for n, starting with the initial value 1.",
			"comment": [
				"The leftmost column of table (the initial term of each row, T(n,1)) is 1, corresponding to lcm(1,1,...,1) computed from the {1+1+...+1} partition of n, after which, on the same row, each further term T(n,i) is computed by finding such a partition [p1,p2,...,pk] of n so that value of lcm(T(n,i-1), p1,p2,...,pk) is maximized, until finally A003418(n) is reached, which will be listed as the last term of row n (as the result would not change after that, if we continued the same process)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A225632/b225632.txt\"\u003eRows n = 1..150, flattened\u003c/a\u003e",
				"\u003ca href=\"/index/Lc#lcm\"\u003eIndex entries for sequences related to lcm's\u003c/a\u003e"
			],
			"example": [
				"The first fifteen rows of table are:",
				"  1;",
				"  1,   2;",
				"  1,   3,    6;",
				"  1,   4,   12;",
				"  1,   6,   30,    60;",
				"  1,   6,   30,    60;",
				"  1,  12,   84,   420;",
				"  1,  15,  120,   840;",
				"  1,  20,  180,  1260,   2520;",
				"  1,  30,  210,   840,   2520;",
				"  1,  30,  420,  4620,  13860,  27720;",
				"  1,  60,  660,  4620,  13860,  27720;",
				"  1,  60,  780,  8580,  60060, 180180, 360360;",
				"  1,  84, 1260, 16380, 180180, 360360;",
				"  1, 105, 4620, 60060, 180180, 360360;"
			],
			"maple": [
				"b:= proc(n, i) option remember; `if`(n=0, {1},",
				"      `if`(i\u003c1, {}, {seq(map(x-\u003eilcm(x, `if`(j=0, 1, i)),",
				"       b(n-i*j, i-1))[], j=0..n/i)}))",
				"    end:",
				"T:= proc(n) option remember; local d, h, l, ll;",
				"      l:= b(n$2); ll:= NULL; d:=1; h:=0;",
				"      while d\u003c\u003eh do ll:= ll, d; h:= d;",
				"        d:= max(seq(ilcm(h, i), i=l))",
				"      od; ll",
				"    end:",
				"seq(T(n), n=1..20);  # _Alois P. Heinz_, May 29 2013"
			],
			"mathematica": [
				"b[n_, i_] := b[n, i] = If[n==0, {1}, If[i\u003c1, {}, Table[Map[Function[{x}, LCM[x, If[j==0, 1, i]]], b[n-i*j, i-1]], {j, 0, n/i}]]]; T[n_] := T[n] = Module[{d, h, l, ll}, l=b[n, n]; ll={}; d=1; h=0; While[d != h, AppendTo[ll, d]; h=d; d = Max[ Table[LCM[h, i], {i, l}]]]; ll]; Table[T[n], {n, 1, 20}] // Flatten (* _Jean-François Alcover_, Jul 29 2015, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(Scheme with _Antti Karttunen_'s IntSeq-library):",
				"(definec (A225632 n) (A225630bi (Aux_for_225632 n) (- n (A225635 (Aux_for_225632 n))))) ;; Scheme-definition for A225630bi given in A225630.",
				"(define Aux_for_225632 (COMPOSE -1+ (LEAST-GTE-I 1 1 A225635) 1+)) ;; Auxiliary function not submitted separately, which gives the row-number for the n-th term.",
				";; It starts as 1,2,2,3,3,3,4,4,4,5,5,5,5,6,6,6,6,7,7,7,7,8,8,8,8,9,9,9,9,9,..."
			],
			"xref": [
				"Cf. A225634 (length of n-th row), A000793 (n\u003e=2 gives the second column).",
				"Cf. A225629 (second largest/rightmost term of n-th row).",
				"Cf. A003418 (largest/rightmost term of n-th row).",
				"Cf. A225630, A225631, A225635, A212721.",
				"Cf. A225642 (row n starts from n instead of 1).",
				"Cf. A226055 (the first term common with A225642 on the n-th row).",
				"Cf. A225638 (distance to that first common term from the beginning of the row n).",
				"Cf. A226056 (number of trailing terms common with A225642 on the n-th row)."
			],
			"keyword": "nonn,tabf",
			"offset": "1,3",
			"author": "_Antti Karttunen_, May 13 2013",
			"references": 14,
			"revision": 27,
			"time": "2018-03-13T04:10:42-04:00",
			"created": "2013-05-20T00:18:11-04:00"
		}
	]
}