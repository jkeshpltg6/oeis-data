{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001635",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1635,
			"id": "M0762 N0289",
			"data": "0,2,3,6,10,11,21,30,48,72,110,171,260,401,613,942,1445,2216,3401,5216,8004,12278,18837,28899,44335,68018,104349,160089,245601,376791,578057,886830,1360538,2087279,3202216,4912704,7536863,11562737,17739062,27214520",
			"name": "A Fielder sequence: a(n) = a(n-1) + a(n-2) - a(n-6), n \u003e= 7.",
			"comment": [
				"This is an application of the general formula that Paul Barry gives for sequence A000129 to the subsequence of odd-indexed terms. - Pat Costello (pat.costello(AT)eku.edu), May 20 2003"
			],
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A001635/b001635.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Daniel C. Fielder, \u003ca href=\"http://www.fq.math.ca/Scanned/6-3/fielder.pdf\"\u003eSpecial integer sequences controlled by three parameters\u003c/a\u003e, Fibonacci Quarterly 6, 1968, 64-70.",
				"Daniel C. Fielder, \u003ca href=\"http://www.fq.math.ca/Scanned/6-3/errata.pdf\"\u003eErrata:Special integer sequences controlled by three parameters\u003c/a\u003e, Fibonacci Quarterly 6, 1968, 64-70.",
				"D. Fielder, \u003ca href=\"/A027907/a027907_1.pdf\"\u003eLetter to N. J. A. Sloane, Jun. 1991\u003c/a\u003e",
				"D. C. Fielder and C. O. Alford, \u003ca href=\"/A001635/a001635.pdf\"\u003eSimulation concepts for studying incomplete (but potentially recursive) sequences\u003c/a\u003e, IASTED International Symposium Simulation and Modeling '89, Lugano, Switzerland, June 19-22, 1989. (Annotated scanned copy)",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1, 1, 0, 0, 0, -1)."
			],
			"formula": [
				"G.f.: x^2*(2 + x + x^2 + x^3 - 5*x^4)/(1 - x - x^2 + x^6).",
				"a(n) = a(n-2) + a(n-3) + a(n-4) + a(n-5), n \u003e= 6.",
				"a(n) = Sum_{k=0..n} C(2*n+1, 2*k+1) * 2^k. - Pat Costello (pat.costello(AT)eku.edu), May 20 2003"
			],
			"maple": [
				"A001635:=-z*(2+3*z+4*z**2+5*z**3)/(-1+z**2+z**3+z**4+z**5); # conjectured (correctly) by _Simon Plouffe_ in his 1992 dissertation",
				"(Maple) a := n -\u003e (Matrix([[5,-1$3,3,4]]). Matrix(6, (i,j)-\u003e if (i=j-1) then 1 elif j=1 then [1$2,0$3,-1][i] else 0 fi)^n)[1,1] ; seq (a(n), n=1..39);  # _Alois P. Heinz_, Aug 01 2008"
			],
			"mathematica": [
				"LinearRecurrence[{1, 1, 0, 0, 0, -1}, {0, 2, 3, 6, 10, 11}, 50] (* _T. D. Noe_, Aug 09 2012 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c0,0,polcoeff(x^2*(2+x+x^2+x^3-5*x^4)/(1-x-x^2+x^6)+x*O(x^n),n))",
				"(MAGMA) I:=[0, 2, 3, 6, 10, 11]; [n le 6 select I[n] else Self(n-1) + Self(n-2) - Self(n-6): n in [1..30]]; // _G. C. Greubel_, Jan 09 2018"
			],
			"xref": [
				"Cf. A000129."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Edited by _Michael Somos_, Feb 17 2002"
			],
			"references": 1,
			"revision": 46,
			"time": "2021-03-12T22:32:35-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}