{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A291784",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 291784,
			"data": "1,2,3,4,5,7,7,8,9,11,11,14,13,15,16,16,17,21,19,22,22,23,23,28,25,27,27,30,29,40,31,32,34,35,36,42,37,39,40,44,41,54,43,46,48,47,47,56,49,55,52,54,53,63,56,60,58",
			"name": "a(n) = (psi(n) + phi(n))/2.",
			"comment": [
				"This is (A001615 + A000010)/2. It is easy to see that this is always an integer.",
				"If n is a power of a prime (including 1 and primes), then a(n) = n, and in any other case a(n) \u003e n. - _M. F. Hasler_, Sep 09 2017",
				"If n is in A006881, then a(n)=n+1. - _Robert Israel_, Feb 10 2019"
			],
			"reference": [
				"Richard K. Guy, Unsolved Problems in Number Theory, 3rd Edition, Springer, 2004. See Section B41 (page 96 of 2nd ed., pages 147ff of 3rd ed.)."
			],
			"link": [
				"Hugo Pfoertner, \u003ca href=\"/A291784/b291784.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"N. J. A. Sloane, Three (No, 8) Lovely Problems from the OEIS, Experimental Mathematics Seminar, Rutgers University, Oct 05 2017, \u003ca href=\"https://vimeo.com/237029685\"\u003ePart I\u003c/a\u003e, \u003ca href=\"https://vimeo.com/237030304\"\u003ePart 2\u003c/a\u003e, \u003ca href=\"https://oeis.org/A290447/a290447_slides.pdf\"\u003eSlides.\u003c/a\u003e (Mentions this sequence)"
			],
			"maple": [
				"f:= proc(n) local P, p;",
				"  P:= numtheory:-factorset(n);",
				"  n*(mul((p-1)/p, p=P) + mul((p+1)/p, p=P))/2",
				"end proc:",
				"map(f, [$1..100]); # _Robert Israel_, Feb 10 2019"
			],
			"mathematica": [
				"psi[n_] := If[n == 1, 1, n*Times @@ (1 + 1/FactorInteger[n][[All, 1]])];",
				"a[n_] := (psi[n] + EulerPhi[n])/2;",
				"Array[a, 100] (* _Jean-François Alcover_, Feb 25 2019 *)"
			],
			"program": [
				"(PARI) A291784(n)=(eulerphi(n)+n*sumdivmult(n,d,issquarefree(d)/d))\\2 \\\\ _M. F. Hasler_, Sep 03 2017"
			],
			"xref": [
				"Cf. A000010, A001615, A291785, A291786, A291787, A291788."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, Sep 02 2017",
			"references": 15,
			"revision": 34,
			"time": "2021-11-18T05:10:51-05:00",
			"created": "2017-09-02T22:11:26-04:00"
		}
	]
}