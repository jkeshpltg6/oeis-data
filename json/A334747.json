{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A334747",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 334747,
			"data": "2,3,6,8,10,5,14,12,18,15,22,24,26,21,30,32,34,27,38,40,42,33,46,20,50,39,54,56,58,7,62,48,66,51,70,72,74,57,78,60,82,35,86,88,90,69,94,96,98,75,102,104,106,45,110,84,114,87,118,120,122,93,126,128,130,55",
			"name": "Let p be the smallest prime not dividing the squarefree part of n. Multiply n by p and divide by the product of all smaller primes.",
			"comment": [
				"A bijection from the positive integers to the nonsquares, A000037.",
				"A003159 (which has asymptotic density 2/3) lists index n such that a(n) = 2n. The sequence maps the terms of A003159 1:1 onto A036554, defining a bijection between them.",
				"Similarly, bijections are defined from A007417 to A325424, from A325424 to A145204\\{0}, and from the first in each of the following pairs to the nonsquare integers in the second: (A145204\\{0}, A036668), (A036668, A007417), (A036554, A003159), (A332820, A332821), (A332821, A332822), (A332822, A332820). Note that many of these are between sets where membership depends on whether a number's squarefree part divides by 2 and/or 3.",
				"Starting from 1, and iterating the sequence as a(1) = 2, a(2) = 3, a(3) = 6, a(6) = 5, a(5) = 10, etc., runs through the squarefree numbers in the order they appear in A019565. - _Antti Karttunen_, Jun 08 2020"
			],
			"formula": [
				"a(n) = n * m / A034386(m-1), where m = A053669(A007913(n)).",
				"a(n) = A331590(2, n) = A225546(2 * A225546(n)).",
				"a(A019565(n)) = A019565(n+1).",
				"a(k * m^2) = a(k) * m^2.",
				"a(A003961(n)) = 2 * A003961(n).",
				"a(2 * A003961(n)) = A003961(a(n)).",
				"a(A002110(n)) = prime(n+1).",
				"A048675(a(n)) = A048675(n) + 1.",
				"A008833(a(n)) = A008833(n).",
				"A267116(a(n)) = A267116(n) OR 1, where OR denotes the bitwise operation A003986.",
				"a(A003159(n)) = A036554(n) = 2 * A003159(n).",
				"A334870(a(n)) = n. - _Antti Karttunen_, Jun 08 2020"
			],
			"example": [
				"168 = 42*4 has squarefree part 42 (and square part 4). The smallest prime absent from 42 = 2*3*7 is 5 and the product of all smaller primes is 2*3 = 6. So a(168) = 168*5/6 = 140."
			],
			"program": [
				"(PARI) a(n) = {my(c=core(n), m=n); forprime(p=2, , if(c % p, m*=p; break, m/=p)); m;} \\\\ _Michel Marcus_, May 22 2020"
			],
			"xref": [
				"Permutation of A000037.",
				"Row 2, and therefore column 2, of A331590. Cf. A334748 (row 3).",
				"A007913, A034386, A053669, A225546 are used in formulas defining the sequence.",
				"The formula section details how the sequence maps the terms of A002110, A003961, A019565; and how f(a(n)) relates to f(n) for f = A008833, A048675, A267116; making use of A003986.",
				"Subsequences: A016825 (odd bisection), A036554, A329575.",
				"Bijections are defined that relate to A003159, A007417, A036668, A145204, A325424, A332820, A332821, A332822.",
				"Cf. also binary trees A334860, A334866 and A334870 (a left inverse)."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_Peter Munn_, May 09 2020",
			"references": 9,
			"revision": 29,
			"time": "2020-06-19T04:24:47-04:00",
			"created": "2020-06-19T04:24:47-04:00"
		}
	]
}