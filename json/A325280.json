{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A325280",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 325280,
			"data": "1,0,1,0,1,1,0,1,1,1,0,1,2,1,1,0,1,1,2,3,0,0,1,3,4,3,0,0,0,1,1,4,8,1,0,0,0,1,3,6,9,3,0,0,0,0,1,2,8,12,7,0,0,0,0,0,1,3,11,17,10,0,0,0,0,0,0,1,1,11,26,17,0,0,0,0,0,0,0,1,5,19,25,27",
			"name": "Triangle read by rows where T(n,k) is the number of integer partitions of n with adjusted frequency depth k.",
			"comment": [
				"The adjusted frequency depth of an integer partition is 0 if the partition is empty, and otherwise it is one plus the number of times one must take the multiset of multiplicities to reach a singleton. For example, the partition (32211) has adjusted frequency depth 5 because we have: (32211) -\u003e (221) -\u003e (21) -\u003e (11) -\u003e (2).",
				"The term \"frequency depth\" appears to have been coined by Clark Kimberling in A225485 and A225486, and can be applied to both integers (A323014) and integer partitions (A325280)."
			],
			"example": [
				"Triangle begins:",
				"  1",
				"  0  1",
				"  0  1  1",
				"  0  1  1  1",
				"  0  1  2  1  1",
				"  0  1  1  2  3  0",
				"  0  1  3  4  3  0  0",
				"  0  1  1  4  8  1  0  0",
				"  0  1  3  6  9  3  0  0  0",
				"  0  1  2  8 12  7  0  0  0  0",
				"  0  1  3 11 17 10  0  0  0  0  0",
				"  0  1  1 11 26 17  0  0  0  0  0  0",
				"  0  1  5 19 25 27  0  0  0  0  0  0  0",
				"  0  1  1 17 44 38  0  0  0  0  0  0  0  0",
				"  0  1  3 25 53 52  1  0  0  0  0  0  0  0  0",
				"  0  1  3 29 63 76  4  0  0  0  0  0  0  0  0  0",
				"  0  1  4 37 83 98  8  0  0  0  0  0  0  0  0  0  0",
				"Row n = 9 counts the following partitions:",
				"  (9)  (333)        (54)      (441)       (3321)",
				"       (111111111)  (63)      (522)       (4221)",
				"                    (72)      (711)       (4311)",
				"                    (81)      (3222)      (5211)",
				"                    (432)     (6111)      (32211)",
				"                    (531)     (22221)     (42111)",
				"                    (621)     (33111)     (321111)",
				"                    (222111)  (51111)",
				"                              (411111)",
				"                              (2211111)",
				"                              (3111111)",
				"                              (21111111)"
			],
			"mathematica": [
				"fdadj[ptn_List]:=If[ptn=={},0,Length[NestWhileList[Sort[Length/@Split[#]]\u0026,ptn,Length[#]\u003e1\u0026]]];",
				"Table[Length[Select[IntegerPartitions[n],fdadj[#]==k\u0026]],{n,0,16},{k,0,n}]"
			],
			"xref": [
				"Row sums are A000041. Column k = 2 is A032741. Column k = 3 is A325245.",
				"Cf. A181819, A225486, A323014, A323023, A325254, A325258, A325277.",
				"Integer partition triangles: A008284 (first omega), A116608 (second omega), A325242 (third omega), A325268 (second-to-last omega), A225485 or A325280 (length/frequency depth)."
			],
			"keyword": "nonn,tabl",
			"offset": "0,13",
			"author": "_Gus Wiseman_, Apr 18 2019",
			"references": 36,
			"revision": 5,
			"time": "2019-04-19T08:10:25-04:00",
			"created": "2019-04-19T08:10:25-04:00"
		}
	]
}