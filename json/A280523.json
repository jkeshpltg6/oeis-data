{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A280523",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 280523,
			"data": "1,3,10,30,84,227,603,1589,4172,10936,28646,75013,196405,514215,1346254,3524562,9227448,24157799,63245967,165580121,433494416,1134903148,2971215050,7778742025,20365011049,53316291147,139583862418",
			"name": "a(n) = Fibonacci(2n + 1) - n.",
			"comment": [
				"Old (and equivalent) definition: these are the indices of records in the Fibonachos sequence A280521: the least k such that A280521(k) = n.",
				"Conjecture: a(n) = A215004(2n - 2). - _Peter Kagey_. The conjecture is true. See link for analysis of connections with A215004 and A280521. - _Nathan Fox_, Jan 27 2017",
				"Define the n-th Fibonacci compositions CF(n) by CF(1)={(1)}, CF(2)={(2)}, and CF(n) is 1 adjoined at the end of each composition in CF(n-1) union 2 adjoined to the end of each composition in CF(n-2). The union is disjoint, so |CF(n)| is the n-th Fibonacci number. Define the weight of a composition c by 2^(number of singletons in c). For example, 2122 has 1 singleton and weight 2. Let s(n) be the sum of the weights of CF(n). Conjecture: a(n)= s(2n+4)-s(2n+3). - _George Beck_, Jan 31 2020"
			],
			"link": [
				"Nathan Fox, \u003ca href=\"/A280523/a280523.pdf\"\u003eProof that a(n) = Fibonacci(2n + 1) - n\u003c/a\u003e.",
				"Murray Tannock, \u003ca href=\"https://skemman.is/bitstream/1946/25589/1/msc-tannock-2016.pdf\"\u003eEquivalence classes of mesh patterns with a dominating pattern\u003c/a\u003e, MSc Thesis, Reykjavik Univ., May 2016. See Appendix B2",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (5,-8,5,-1)."
			],
			"formula": [
				"G.f.: -x*(-1+2*x-3*x^2+x^3) / ( (x^2-3*x+1)*(x-1)^2 ). - _R. J. Mathar_, Mar 11 2017",
				"a(n) = 5*a(n-1)-8*a(n-2)+5*a(n-3)-a(n-4). - _Wesley Ivan Hurt_, Apr 26 2021"
			],
			"example": [
				"a(1) = 1    because A280521(1)    = 1;",
				"a(2) = 3    because A280521(3)    = 2;",
				"a(3) = 10   because A280521(10)   = 3;",
				"a(4) = 30   because A280521(30)   = 4;",
				"a(5) = 84   because A280521(84)   = 5;",
				"a(6) = 227  because A280521(227)  = 6;",
				"a(7) = 603  because A280521(603)  = 7;",
				"a(8) = 1589 because A280521(1589) = 8."
			],
			"mathematica": [
				"Table[Fibonacci[2n + 1] - n, {n, 30}] (* _Alonso del Arte_, Jan 29 2017 *)"
			],
			"program": [
				"(PARI) F=vector(64,n,fibonacci(n+2)-1); \\\\ Resize as needed",
				"A280521(n)=my(s); while(n, s++; t=setsearch(F,n,1); if(t, n-=F[t-1], return(s))); s",
				"first(n)=my(v=vector(n),k,t,mn=1,gaps=n); while(gaps, t=A280521(k++); if(t\u003e=mn \u0026\u0026 t\u003c=n \u0026\u0026 v[t]==0, v[t]=k; while(mn\u003c=n \u0026\u0026 v[mn], mn++); print(\"a(\"t\") = \"k); gaps--)); v \\\\ _Charles R Greathouse IV_, Jan 04 2017"
			],
			"xref": [
				"Cf. A001519, A215004, A280521."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Peter Kagey_, Jan 04 2017",
			"ext": [
				"Corrected and extended by _Charles R Greathouse IV_, Jan 04 2017",
				"a(26) from _Charles R Greathouse IV_, Jan 09 2017",
				"a(27) from _Charles R Greathouse IV_, Jan 16 2017",
				"Replaced old definition by simple formula established by _Nathan Fox_. - _N. J. A. Sloane_, Jan 30 2017"
			],
			"references": 4,
			"revision": 70,
			"time": "2021-04-26T14:14:37-04:00",
			"created": "2017-01-06T16:16:05-05:00"
		}
	]
}