{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A091048",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 91048,
			"data": "0,0,0,0,0,0,0,0,0,1,1,1,1,1,2,2,3,4,0,1,1,1,2,2,4,2,3,3,0,2,2,2,3,1,3,4,3,4,0,2,2,2,3,3,1,2,1,4,0,3,3,3,4,2,4,3,2,5,0,3,4,4,2,3,2,5,5,5,0,4,3,6,1,4,5,5,3,4,0,7,2,5,6,6,4,5,1,8,0,0,0,0,0,0,0,0,0,0,0,1",
			"name": "a(n) = the number of steps needed to reach the final value of n via repeated interpretation of n as a base b+1 number where b is the largest digit of n.",
			"comment": [
				"Any value of n with at least one digit 9 will not reduce further since 9+1 is 10 and n in base 10 is n. Also any single-digit number will likewise not reduce further. Such values of n therefore require 0 steps to reduce. Many terms reduce in very few steps and others take longer (88 for example, takes 8 steps). There is no maximum number of steps. See A091049 to see the first term requiring n steps. See A091047 to see the actual unchanging value reached for each value of n."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A091048/b091048.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"C. Seggelin, \u003ca href=\"https://web.archive.org/web/20040109233146/http://www.plastereddragon.com/maths/bases.htm\"\u003eInteresting Base Conversions\u003c/a\u003e."
			],
			"example": [
				"a(18)=4 because (1) 18 in base 9 is 17. (2) 17 in base 8 is 15. (3) 15 in base 6 is 11. (4) 11 in base 2 is 3. 3 does not reduce further because 3 in base 4 is 3. Thus 18 reduces to 3 in 4 steps."
			],
			"maple": [
				"f:= proc(n) option remember;",
				"   local L,b,i;",
				"   if n \u003c 10 then return 0 fi;",
				"   L:= convert(n,base,10);",
				"   b:= max(L)+1;",
				"   if b = 10 then 0 else 1+procname(add(L[i]*b^(i-1),i=1..nops(L))) fi",
				"end proc:",
				"map(f, [$1..100]); # _Robert Israel_, Feb 19 2018"
			],
			"xref": [
				"Cf. A054055 (largest digit of n) A068505 (n as base b+1 number where b=largest digit of n) A091047 (a(n) = the final value of n reached through repeated interpretation of n as a base b+1 number where b is the largest digit of n) A091049 (a(n) = first term which reduces to an unchanging value in n steps via repeated interpretation of a(n) as a base b+1 number where b is the largest digit of a(n))."
			],
			"keyword": "base,nonn",
			"offset": "1,15",
			"author": "Chuck Seggelin (barkeep(AT)plastereddragon.com), Dec 15 2003",
			"references": 3,
			"revision": 8,
			"time": "2018-12-08T03:04:37-05:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}