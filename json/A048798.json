{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A048798",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 48798,
			"data": "1,4,9,2,25,36,49,1,3,100,121,18,169,196,225,4,289,12,361,50,441,484,529,9,5,676,1,98,841,900,961,2,1089,1156,1225,6,1369,1444,1521,25,1681,1764,1849,242,75,2116,2209,36,7,20,2601,338,2809,4,3025,49,3249",
			"name": "Smallest k \u003e 0 such that n*k is a perfect cube.",
			"comment": [
				"Note that in general the smallest number k(\u003e0) such that nk is a perfect m-th power (rather obviously) = (the smallest m-th power divisible by n)/n and also (slightly less obviously) =n^(m-1)/(the number of solutions of x^m==0 mod n)^m. - _Henry Bottomley_, Mar 03 2000",
				"Multiplicative with a(p^e) = p^(2 - e%3). - _Mitch Harris_, May 17 2005"
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A048798/b048798.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (terms 1..5000 from Peter Kagey)",
				"Krassimir T. Atanassov, \u003ca href=\"http://nntdm.net/volume-05-1999/number-2/80-82/\"\u003eOn the 22nd, the 23rd and 24th Smarandache Problems\u003c/a\u003e, Notes on Number Theory and Discrete Mathematics, Sophia, Bulgaria, Vol. 5, No. 2 (1998), pp. 80-82.",
				"Krassimir T. Atanassov, \u003ca href=\"http://www.gallup.unm.edu/~smarandache/Atanassov-SomeProblems.pdf\"\u003eOn Some of Smarandache's Problems\u003c/a\u003e, American Research Press, 1999, 16-21.",
				"Henry Bottomley, \u003ca href=\"http://fs.gallup.unm.edu/Bottomley-Sm-Mult-Functions.htm\"\u003eSome Smarandache-type multiplicative sequences\u003c/a\u003e.",
				"Marcela Popescu and Mariana Nicolescu, \u003ca href=\"https://search.proquest.com/docview/221244131/fulltext/1AA67E34DB1A4DA2PQ/1?accountid=6724\"\u003eAbout the Smarandache Complementary Cubic Function\u003c/a\u003e, Smarandache Notions Journal, Vol. 7, No. 1-2-3, 1996, pp. 54-62.",
				"Florentin Smarandache, \u003ca href=\"http://www.gallup.unm.edu/~smarandache/OPNS.pdf\"\u003eOnly Problems, Not Solutions!\u003c/a\u003e."
			],
			"formula": [
				"a(n) = A053149(n)/n = n^2/A000189(n)^3."
			],
			"example": [
				"a(12) = a(2*2*3) = 2*3*3 = 18 since 12*18 = 6^3.",
				"a(28) = a(2*2*7) = 2*7*7 = 98 since 28*98 = 14^3."
			],
			"mathematica": [
				"a[n_] := For[k = 1, True, k++, If[ Divisible[c = k^3, n], Return[c/n]]]; Table[a[n], {n, 1, 60}] (* _Jean-François Alcover_, Sep 03 2012 *)",
				"f[p_, e_] := p^(Mod[3 - e, 3]); a[n_] := Times @@ (f @@@ FactorInteger[n]); Array[a, 100] (* _Amiram Eldar_, Sep 10 2020 *)"
			],
			"program": [
				"(PARI) a(n)=my(f=factor(n));prod(i=1,#f[,1],f[i,1]^(-f[i,2]%3)) \\\\ _Charles R Greathouse IV_, Feb 27 2013",
				"(PARI) a(n)=for(k=1,n^2,if(ispower(k*n,3),return(k)))",
				"vector(100,n,a(n)) \\\\ _Derek Orr_, Feb 07 2015"
			],
			"xref": [
				"Cf. A000189, A007913, A053149.",
				"Cf. A254767 (analogous sequence with the restriction that k \u003e n)."
			],
			"keyword": "nonn,easy,mult",
			"offset": "1,2",
			"author": "Charles T. Le (charlestle(AT)yahoo.com)",
			"ext": [
				"More terms from _Patrick De Geest_, Feb 15 2000"
			],
			"references": 10,
			"revision": 47,
			"time": "2020-09-10T09:27:53-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}