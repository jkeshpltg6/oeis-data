{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003313",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3313,
			"id": "M0255",
			"data": "0,1,2,2,3,3,4,3,4,4,5,4,5,5,5,4,5,5,6,5,6,6,6,5,6,6,6,6,7,6,7,5,6,6,7,6,7,7,7,6,7,7,7,7,7,7,8,6,7,7,7,7,8,7,8,7,8,8,8,7,8,8,8,6,7,7,8,7,8,8,9,7,8,8,8,8,8,8,9,7,8,8,8,8,8,8,9,8,9,8,9,8,9,9,9,7,8,8,8,8",
			"name": "Length of shortest addition chain for n.",
			"comment": [
				"Equivalently, minimal number of multiplications required to compute the n-th power."
			],
			"reference": [
				"Bahig, Hatem M.; El-Zahar, Mohamed H.; Nakamula, Ken; Some results for some conjectures in addition chains, in Combinatorics, computability and logic, pp. 47-54, Springer Ser. Discrete Math. Theor. Comput. Sci., Springer, London, 2001.",
				"D. Bleichenbacher and A. Flammenkamp, An Efficient Algorithm for Computing Shortest Addition Chains, Preprint, 1997.",
				"A. Flammenkamp, Drei Beitraege zur diskreten Mathematik: Additionsketten, No-Three-in-Line-Problem, Sociable Numbers, Diplomarbeit, Bielefeld 1991.",
				"Gashkov, S. B. and Kochergin, V. V.; On addition chains of vectors, gate circuits and the complexity of computations of powers [translation of Metody Diskret. Anal. No. 52 (1992), 22-40, 119-120; 1265027], Siberian Adv. Math. 4 (1994), 1-16.",
				"Gioia, A. A. and Subbarao, M. V., The Scholz-Brauer problem in addition chains, II, in Proceedings of the Eighth Manitoba Conference on Numerical Mathematics and Computing (Univ. Manitoba, Winnipeg, Man., 1978), pp. 251-274, Congress. Numer., XXII, Utilitas Math., Winnipeg, Man., 1979.",
				"D. E. Knuth, The Art of Computer Programming, vol. 2, Seminumerical Algorithms, 2nd ed., Fig. 14 on page 403; 3rd edition, 1998, p. 465.",
				"D. E. Knuth, website, further updates to Vol. 2 of TAOCP.",
				"Rabin, Michael O., and Shmuel Winograd. \"Fast evaluation of polynomials by rational preparation.\" Communications on Pure and Applied Mathematics25.4 (1972): 433-458. See Table p. 455.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"D. W. Wilson and Antoine Mathys, \u003ca href=\"/A003313/b003313.txt\"\u003eTable of n, a(n) for n = 1..100000\u003c/a\u003e (10001 terms from D. W. Wilson)",
				"F. Bergeron, J. Berstel, S. Brlek, C Duboc, \u003ca href=\"http://dx.doi.org/10.1016/0196-6774(89)90036-9\"\u003eAddition chains using continued fractions\u003c/a\u003e, J. Algorithms 10 (1989), 403-412.",
				"Daniel Bleichenbacher, \u003ca href=\"http://www.bell-labs.com/user/bleichen/diss/thesis.html\"\u003eEfficiency and Security of Cryptosystems based on Number Theory.\u003c/a\u003e PhD Thesis, Diss. ETH No. 11404, Zürich 1996. See p. 61.",
				"Alfred Brauer, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9904-1939-07068-7\"\u003eOn addition chains\u003c/a\u003e Bull. Amer. Math. Soc. 45, (1939). 736-739.",
				"Peter Downey, Benton Leong, and Ravi Sethi, \u003ca href=\"http://dx.doi.org/10.1137/0210047\"\u003eComputing sequences with addition chains\u003c/a\u003e SIAM J. Comput. 10 (1981), 638-646.",
				"M. Elia and F. Neri, \u003ca href=\"https://doi.org/10.1007/978-1-4612-3352-7_13\"\u003eA note on addition chains and some related conjectures\u003c/a\u003e, (Naples/Positano, 1988), pp. 166-181 of R. M. Capocelli, ed., Sequences, Springer-Verlag, NY 1990.",
				"Christian Elsholtz et al., \u003ca href=\"http://mathoverflow.net/questions/217411/upper-bound-on-length-of-addition-chain/218608#218608\"\u003eUpper bound on length of addition chain\u003c/a\u003e, Math Overflow, Sep 18 2015.",
				"P. Erdős, \u003ca href=\"http://matwbn.icm.edu.pl/ksiazki/aa/aa6/aa618.pdf\"\u003eRemarks on number theory. III. On addition chains\u003c/a\u003e, Acta Arith. 6 1960 77-81.",
				"Achim Flammenkamp, \u003ca href=\"http://wwwhomes.uni-bielefeld.de/achim/addition_chain.html\"\u003eShortest addition chains\u003c/a\u003e",
				"A. A. Gioia, M. V. Subbarao, and M. Sugunamma, \u003ca href=\"http://dx.doi.org/10.1215/S0012-7094-62-02948-4\"\u003eThe Scholz-Brauer problem in addition chains\u003c/a\u003e, Duke Math. J. 29 1962 481-487.",
				"Anastasiya Gorodilova, Sergey Agievich, Claude Carlet, Evgeny Gorkunov, Valeriya Idrisova, Nikolay Kolomeec, Alexandr Kutsenko, Svetla Nikova, Alexey Oblaukhov, Stjepan Picek, Bart Preneel, Vincent Rijmen, Natalia Tokareva, \u003ca href=\"https://arxiv.org/abs/1806.02059\"\u003eProblems and solutions of the Fourth International Students' Olympiad in Cryptography NSUCRYPTO\u003c/a\u003e, arXiv:1806.02059 [cs.CR], 2018.",
				"R. L. Graham, A. C.-C. Yao, F. F. Yao, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(78)90111-5\"\u003eAddition chains with multiplicative cost\u003c/a\u003e Discrete Math. 23 (1978), 115-119.",
				"D. Knuth, \u003ca href=\"/A003063/a003063.pdf\"\u003eLetter to N. J. A. Sloane, date unknown\u003c/a\u003e",
				"D. E. Knuth, \u003ca href=\"http://www-cs-faculty.stanford.edu/~knuth/programs.html\"\u003eSee the achain-all program\u003c/a\u003e",
				"D. P. McCarthy, \u003ca href=\"http://www.jstor.org/stable/2007998\"\u003eEffect of improved multiplication efficiency on exponentiation algorithms derived from addition chains\u003c/a\u003e Math. Comp. 46 (1986), 603-608.",
				"Alec Mihailovs, \u003ca href=\"/A003313/a003313b.txt\"\u003eNotes on using Flammenkamp's tables\u003c/a\u003e",
				"Jorge Olivos, \u003ca href=\"http://dx.doi.org/10.1016/0196-6774(81)90003-1\"\u003eOn vectorial addition chains\u003c/a\u003e J. Algorithms 2 (1981), 13-21.",
				"Hugo Pfoertner, \u003ca href=\"/A003313/a003313.txt\"\u003eAddition chains\u003c/a\u003e",
				"Kari Ragnarsson, Bridget Eileen Tenner, \u003ca href=\"http://arxiv.org/abs/0802.2550\"\u003eObtainable Sizes of Topologies on Finite Sets\u003c/a\u003e, Oct 06 2008, Journal of Combinatorial Theory, Series A 117 (2010) 138-151.",
				"Arnold Schönhage, \u003ca href=\"http://dx.doi.org/10.1016/0304-3975(75)90008-0\"\u003eA lower bound for the length of addition chains\u003c/a\u003e Theor. Comput. Sci. 1 (1975), 1-12.",
				"Edward G. Thurber, \u003ca href=\"http://projecteuclid.org/euclid.pjm/1102945286\"\u003eThe Scholz-Brauer problem on addition chains\u003c/a\u003e Pacific J. Math. 49 (1973), 229-242.",
				"Edward G. Thurber, \u003ca href=\"http://dx.doi.org/10.1215/S0012-7094-73-04085-4\"\u003eOn addition chains l(mn)\u003c=l(n)-b and lower bounds for c(r)\u003c/a\u003e Duke Math. J. 40 (1973), 907-913.",
				"Edward G. Thurber, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(76)90105-9\"\u003eAddition chains and solutions of l(2n)=l(n) and l(2^n-1)=n+l(n)-1\u003c/a\u003e, Discrete Math., Vol. 16 (1976), 279-289.",
				"Edward G. Thurber, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(93)90303-B\"\u003eAddition chains-an erratic sequence\u003c/a\u003e Discrete Math. 122 (1993), 287-305.",
				"Edward G. Thurber, \u003ca href=\"http://dx.doi.org/10.1137/S0097539795295663\"\u003eEfficient generation of minimal length addition chains\u003c/a\u003e, SIAM J. Comput. 28 (1999), 1247-1263.",
				"W. R. Utz, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9939-1953-0054618-X\"\u003eA note on the Scholz-Brauer problem in addition chains\u003c/a\u003e, Proc. Amer. Math. Soc. 4, (1953). 462-463.",
				"Emanuel Vegh, \u003ca href=\"http://dx.doi.org/10.1016/0097-3165(75)90098-9\"\u003eA note on addition chains\u003c/a\u003e, J. Combinatorial Theory Ser. A 19 (1975), 117-118.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/AdditionChain.html\"\u003eAddition Chain\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ScholzConjecture.html\"\u003eScholz Conjecture\u003c/a\u003e",
				"C. T. Whyburn, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9939-1965-0180517-7\"\u003eA note on addition chains\u003c/a\u003e Proc. Amer. Math. Soc. 16 1965 1134.",
				"\u003ca href=\"/index/Com#complexity\"\u003eIndex to sequences related to the complexity of n\u003c/a\u003e"
			],
			"formula": [
				"a(n*m) \u003c= a(n)+a(m). In particular, a(n^k) \u003c= k * a(n). - _Max Alekseyev_, Jul 22 2005",
				"For all n \u003e= 2, a(n) \u003c= (4/3)*floor(log_2 n) + 2. - _Jonathan Vos Post_, Oct 08 2008",
				"From _Achim Flammenkamp_, Oct 26 2016: (Start)",
				"a(n) \u003c= 9/log_2(71) log_2(n), for all n.",
				"It is conjectured by D. E. Knuth, K. Stolarsky et al. that for all n: floor(log_2(n)) + ceiling(log_2(v(n))) \u003c= a(n). (End)",
				"a(n) \u003c= A014701(n). - _Charles R Greathouse IV_, Jan 03 2018"
			],
			"example": [
				"For n \u003c 149 and for many higher values of n, a(n) is the depth of n in a tree whose first 6 levels are shown below. The path from the root of the tree to n gives an optimal addition chain. (See Knuth, Vol. 2, Sect. 4.6.3, Fig. 14 and Ex. 5):",
				"                  1",
				"                  |",
				"                  2",
				"                 / \\",
				"                /   \\",
				"               /     \\",
				"              /       \\",
				"             /         \\",
				"            3           4",
				"           / \\           \\",
				"          /   \\           \\",
				"         /     \\           \\",
				"        /       \\           \\",
				"       5         6           8",
				"      / \\        |         /   \\",
				"     /   \\       |        /     \\",
				"    7    10      12      9       16",
				"   /    /  \\    /  \\    /  \\    /  \\",
				"  14   11  20  15  24  13  17  18  32",
				"E.g., a(15) = 5 and an optimal chain for 15 is 1, 2, 3, 6, 12, 15.",
				"It is not possible to extend the tree to include the optimal addition chains for all n. For example, the chains for 43, 77, and 149 are incompatible. See the link to Achim Flammenkamp's web page on addition chains."
			],
			"xref": [
				"Cf. A003064, A003065, A005766, A230528, A014701."
			],
			"keyword": "nonn,nice,look",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Jud McCranie_, Nov 01 2001"
			],
			"references": 57,
			"revision": 127,
			"time": "2020-05-25T09:27:45-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}