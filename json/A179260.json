{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A179260",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 179260,
			"data": "1,8,4,7,7,5,9,0,6,5,0,2,2,5,7,3,5,1,2,2,5,6,3,6,6,3,7,8,7,9,3,5,7,6,5,7,3,6,4,4,8,3,3,2,5,1,7,2,7,2,8,4,9,7,2,2,3,0,1,9,5,4,6,2,5,6,1,0,7,0,0,1,5,0,0,2,2,0,4,7,1,7,4,2,9,6,7,9,8,6,9,7,0,0,6,8,9,1,9,2",
			"name": "Decimal expansion of the connective constant of the honeycomb lattice.",
			"comment": [
				"This is the case n=8 of the ratio Gamma(1/n)*Gamma((n-1)/n)/(Gamma(2/n)*Gamma((n-2)/n)). - _Bruno Berselli_, Dec 13 2012",
				"An algebraic integer of degree 4: largest root of x^4 - 4x^2 + 2. - _Charles R Greathouse IV_, Nov 05 2014",
				"This number is also the length ratio of the shortest diagonal (not counting the side) of the octagon and the side. This ratio is A121601 for the longest diagonal. - _Wolfdieter Lang_, May 11 2017 [corrected Oct 28 2020]",
				"From _Wolfdieter Lang_, Apr 29 2018: (Start)",
				"This constant appears in a historic problem posed by Adriaan van Roomen (Adrianus Romanus) in his Ideae mathematicae from 1593, solved by Viète. See the Havil reference, problem 3, pp. 69-74. See also the comments in A302711 with the Romanus link and his Exemplum tertium.",
				"This problem is equivalent to R(45, 2*sin(Pi/120)) = 2*sin(3*Pi/8) with a special case of monic Chebyshev polynomials of the first kind, named R, given in A127672. For the constant 2*sin(Pi/120) see A302715. (End)"
			],
			"reference": [
				"Julian Havil, The Irrationals, A Story of the Numbers You Can't Count On, Princeton University Press, Princeton and Oxford, 2012, pp. 69-74.",
				"N. Madras and G. Slade, Self-avoiding walks, Probability and its Applications. Birkhäuser Boston, Inc. Boston, MA (1993)."
			],
			"link": [
				"Hugo Duminil-Copin and Stanislav Smirnov, \u003ca href=\"http://arxiv.org/abs/1007.0575\"\u003eThe connective constant of the honeycomb lattice equals sqrt(2+sqrt2)\u003c/a\u003e, arXiv:1007.0575 [math-ph], 2011.",
				"Hugo Duminil-Copin and Stanislav Smirnov, \u003ca href=\"http://dx.doi.org/10.4007/annals.2012.175.3.14\"\u003eThe connective constant of the honeycomb lattice equals sqrt(2+sqrt2)\u003c/a\u003e, Ann. Math. 175 (2012), pp. 1653-1665.",
				"S. Finch, \u003ca href=\"http://arxiv.org/abs/2001.00578\"\u003eErrata and Addenda to Mathematical Constants\u003c/a\u003e, Jun 23 2012, Section 5.10; arXiv:2001.00578 [math.HO], 2020.",
				"Pierre-Louis Giscard, \u003ca href=\"http://images.math.cnrs.fr/Que-sait-on-compter-sur-un-graphe-Partie-3.html\"\u003eQue sait-on compter sur un graphe. Partie 3\u003c/a\u003e (in French), Images des Mathématiques, CNRS, 2020.",
				"G. Lawler, O. Schramm and W. Werner, \u003ca href=\"http://arxiv.org/abs/math/0204277\"\u003eOn the scaling limit of planar self-avoiding walk\u003c/a\u003e, Fractal Geometry and applications: a jubilee of Benoit Mandelbrot, Part 2, 339-364. Proc.",
				"B. Nienhuis, \u003ca href=\"http://dx.doi.org/10.1103/PhysRevLett.49.1062\"\u003eExact critical point and critical exponents of O(n) models in two dimensions\u003c/a\u003e, Phys. Rev. Lett. 49 (1982), 1062-1065.",
				"Jonathan Sondow and Huang Yi, \u003ca href=\"http://arxiv.org/abs/1005.2712\"\u003eNew Wallis- and Catalan-type infinite products for Pi, e, and sqrt(2+sqrt(2))\u003c/a\u003e, arXiv:1005.2712 [math.NT], 2010.",
				"Jonathan Sondow and Huang Yi, \u003ca href=\"http://www.jstor.org/stable/10.4169/000298910x523399\"\u003eNew Wallis- and Catalan-type infinite products for Pi, e, and sqrt(2+sqrt(2))\u003c/a\u003e, Amer. Math. Monthly 117 (2010) 912-917.",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials\u003c/a\u003e."
			],
			"formula": [
				"sqrt(2+sqrt(2)) = (2/1)(6/7)(10/9)(14/15)(18/17)(22/23)... (see Sondow-Yi 2010).",
				"Equals 1/A154739. - _R. J. Mathar_, Jul 11 2010",
				"Equals 2*A144981. - _Paul Muljadi_, Aug 23 2010",
				"log (A001668(n)) ~ n log k where k = sqrt(2+sqrt(2)). - _Charles R Greathouse IV_, Nov 08 2013",
				"2*cos(Pi/8) = sqrt(2+sqrt(2)). See a remark on the smallest diagonal in the octagon above. - _Wolfdieter Lang_, May 11 2017",
				"Equals also 2*sin(3*Pi/8). See the comment on van Roomen's third problem above. - _Wolfdieter Lang_, Apr 29 2018"
			],
			"example": [
				"1.84775906502257351225636637879357657364483325172728497223019546256107001500..."
			],
			"mathematica": [
				"RealDigits[Sqrt[2+Sqrt[2]],10,120][[1]] (* _Harvey P. Dale_, Jan 19 2014 *)"
			],
			"program": [
				"(PARI) sqrt(2+sqrt(2)) \\\\ _Charles R Greathouse IV_, Nov 05 2014"
			],
			"xref": [
				"Cf. A002193, A101464, A127672, A144981, A154739, A302715, A121601."
			],
			"keyword": "cons,nonn",
			"offset": "1,2",
			"author": "_Jonathan Vos Post_, Jul 06 2010",
			"references": 16,
			"revision": 74,
			"time": "2020-12-21T07:17:54-05:00",
			"created": "2010-07-11T03:00:00-04:00"
		}
	]
}