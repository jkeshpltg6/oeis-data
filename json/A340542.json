{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A340542",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 340542,
			"data": "1,2,2,2,3,3,3,4,4,3,4,4,3,5,5,3,5,5,3,5,5,3,5,6,4,5,6,4,5,5,3,5,5,5,7,5,5,7,5,3,5,5,3,7,7,3,7,8,4,5,6,4,5,7,5,5,7,5,5,5,3,7,7,5,9,7,5,7,5,3,5,5,3,7,7,5,9,7,5,8,6,3,6,8,5,5,7",
			"name": "Number of Fibonacci divisors of Fibonacci(n)^2 + 1.",
			"comment": [
				"A Fibonacci divisor of a number k is a Fibonacci number that divides k.",
				"It is interesting to compare this sequence with A339669.",
				"We observe that a(2n) = A339669(2n) if n = 5*k + 2 or n = 5*k + 3, with k \u003e= 0, because Lucas(2n)^2 = 5*Fibonacci(2n)^2 + 4 (see A005248: all nonnegative integer solutions of the Pell equation a(n)^2 - 5*b(n)^2 = +4 together with b(n)= A001906(n), n\u003e=0. - from _Wolfdieter Lang_, Aug 31 2004).",
				"So, Lucas(2n)^2 + 1 = 5*(Fibonacci(2n)^2 + 1). Lucas(2n)^2 + 1 and Fibonacci(2n)^2 + 1 have the same Fibonacci divisors for n = 5*k + 2 or n = 5*k + 3. For the other values of n = 5*k, 5*k + 1 or 5*k + 4, 5 is a Fibonacci divisor of Lucas(2n)^2 + 1 but not of Fibonacci(2n)^2 + 1. So for these last three values of n, a(2n) = A339669(2n) - 1 (except for m = 1 and 2, 5*F(m) is never a Fibonacci number)."
			],
			"example": [
				"a(13) = 5 because the 5 Fibonacci divisors of Fibonacci(13)^2 + 1 = 233^2 + 1 are 1, 2, 5, 89 and 610.",
				"a(16) = 5 because the 5 Fibonacci divisors of Fibonacci(16)^2 + 1 = 987^2 + 1 are 1, 2, 5, 610, and 1597.",
				"Remark: the 5 Fibonacci divisors of Lucas(16)^2 + 1 = 2207^2 + 1 are 1, 2, 5, 610, and 1597, the index 16 = 2*8 with 8 of the form 5*k + 3."
			],
			"maple": [
				"with(combinat,fibonacci):nn:=100:F:={}:",
				"for k from 0 to nn do:",
				"  F:=F union {fibonacci(k)}:",
				"od:",
				"   for m from 0 to 90 do:",
				"    f:=fibonacci(m)^2+1:d:=numtheory[divisors](f):",
				"    lst:= F intersect d: n1:=nops(lst):printf(`%d, `,n1):",
				"   od:"
			],
			"program": [
				"(PARI) isfib(n) = my(k=n^2); k+=(k+1)\u003c\u003c2; issquare(k) || (n\u003e0 \u0026\u0026 issquare(k-8)); \\\\ A010056",
				"a(n) = sumdiv(fibonacci(n)^2+1, d, isfib(d)); \\\\ _Michel Marcus_, Jan 12 2021"
			],
			"xref": [
				"Cf. A000032, A000045, A005248, A010056, A339461, A339669."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Michel Lagneau_, Jan 12 2021",
			"references": 0,
			"revision": 23,
			"time": "2021-01-30T18:02:09-05:00",
			"created": "2021-01-30T18:02:09-05:00"
		}
	]
}