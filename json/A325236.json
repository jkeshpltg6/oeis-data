{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A325236",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 325236,
			"data": "1,2,3,15,21,231,273,255,285,167739,56751695,7599867,3829070245,567641679,510795753,39169969059,704463969,3717740976339,42917990271,547701649495,45484457928390429,59701280265935165",
			"name": "Squarefree k such that phi(k)/k - 1/2 is positive and minimal for k with gpf(k) = prime(n).",
			"comment": [
				"Let gpf(k) = A006530(k) and let phi(n) = A000010(n) for k in A005117.",
				"There are 2^(n-1) numbers k with gpf(k) = prime(n), since we can only either have p_i^0 or p_i^1 where p_i | k and i \u003c= n. For example, for n = 2, there are only 2 squarefree numbers k with prime(2) = 3 as greatest prime factor. These are 3 = 2^0 * 3^1, and 6 = 2^1 * 3^1. We observe that we can write multiplicities of the primes as A067255(k), and thus for the example derive 3 = \"0,1\" and 6 = \"1,1\". Thus for n = 3, we have 5 = \"0,0,1\", 15 = \"0,1,1\", 10 = \"1,0,1\", and 30 = \"1,1,1\". This establishes the possible values of k with respect to n. We choose the value of k in n for which phi(k)/k - 1/2 is positive and minimal.",
				"We know that prime k (in A000040) have phi(k)/k = A006093(n)/A000040(n) and represent maxima in n. We likewise know primorials k (in A002110) have phi(k)/k = A038110(n)/A060753(n) and represent minima in n. This sequence shows squarefree numbers k with gpf(k) = n such that their value phi(k)/k is closest to but more than 1/2.",
				"Apart from a(1) = 2, all terms are odd. For n \u003e 1 and k even, phi(k)/k - 1/2 is negative."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A325236/a325236.png\"\u003ePlot of A325236(n) among terms in row n of A307540\u003c/a\u003e."
			],
			"example": [
				"First terms of this sequence appear in the chart below between asterisks.",
				"The values of n appear in the header, values of k followed parenthetically by phi(k)/k appear in column n. The x axis plots k according to primepi(gpf(k)), while the y axis plots k according to phi(k)/k:",
				"    0       1          2             3             4",
				"    .       .          .             .             .",
				"-- *1* -----------------------------------------------",
				"  (1/1)     .          .             .             .",
				"    .       .          .             .             .",
				"    .       .          .             .             .",
				"    .       .          .             .             7",
				"    .       .          .             5           (6/7)",
				"    .       .          .           (4/5)           .",
				"    .       .          .             .             .",
				"    .       .          .             .            35",
				"    .       .         *3*            .          (24/35)",
				"    .       .        (2/3)           .             .",
				"    .       .          .             .             .",
				"    .       .          .             .             .",
				"    .       .          .             .           *21*",
				"    .       .          .             .           (4/7)",
				"    .       .          .           *15*            .",
				"    .       .          .          (8/15)           .",
				"    .      *2*         .             .             .",
				"----------(1/2)---------------------------------------",
				"    .       .          .             .             .",
				"    .       .          .             .            105",
				"    .       .          .             .          (16/35)",
				"    .       .          .             .            14",
				"    .       .          .            10           (3/7)",
				"    .       .          .           (2/5)           .",
				"    .       .          .             .             .",
				"    .       .          .             .            70",
				"    .       .          6             .          (12/35)",
				"    .       .        (1/3)           .             .",
				"    .       .          .             .            42",
				"    .       .          .            30           (2/7)",
				"    .       .          .          (4/15)           .",
				"    .       .          .             .            210",
				"    .       .          .             .           (8/35)",
				"...",
				"a(3) = 15 for the following reasons. There are 4 possible values of k with n = 3. These are 5, 15, 10, and 30 with phi(k)/k = 4/5, 8/15, 2/5, and 4/15, respectively. Subtracting 1/2 from each of the latter values, we derive 3/10, 1/30, -1/10, and -7/30 respectively. Since the smallest of these differences is 3/10 pertaining to k = 15, a(3) = 15."
			],
			"mathematica": [
				"With[{e = 15}, Map[MinimalBy[#, If[# \u003c 0, # + 1, #] \u0026[#[[2]] - 1/2] \u0026] \u0026, SplitBy[#, Last]] \u0026@ Array[{#2, EulerPhi[#2]/#2, If[! IntegerQ@ #, 0, #] \u0026[1 + Floor@ Log2@ #1]} \u0026 @@ {#, Times @@ MapIndexed[Prime[First@ #2]^#1 \u0026, Reverse@ IntegerDigits[#, 2]]} \u0026, 2^(e + 1), 0]][[All, 1, 1]]"
			],
			"xref": [
				"Cf. A000010, A000040, A002110, A005117, A006093, A006530, A038110, A060753, A307540, A325237."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Michael De Vlieger_, Apr 19 2019",
			"references": 2,
			"revision": 9,
			"time": "2019-04-20T03:02:23-04:00",
			"created": "2019-04-19T18:34:17-04:00"
		}
	]
}