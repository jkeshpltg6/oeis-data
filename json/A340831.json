{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A340831",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 340831,
			"data": "0,0,1,0,1,1,1,0,2,1,1,1,1,1,2,0,1,2,1,2,2,1,1,1,2,1,3,2,1,2,1,0,2,1,2,3,1,1,2,2,1,3,1,2,4,1,1,1,2,2,2,2,1,4,2,2,2,1,1,4,1,1,4,0,2,3,1,2,2,2,1,4,1,1,4,2,2,3,1,3,5,1,1,5,2,1,2,3,1,5,2,2,2,1,2,1,1,2,4,4,1,3,1,3,5,1,1,6",
			"name": "Number of factorizations of n into factors \u003e 1 with odd greatest factor.",
			"link": [
				"Antti Karttunen, \u003ca href=\"/A340831/b340831.txt\"\u003eTable of n, a(n) for n = 1..20000\u003c/a\u003e"
			],
			"example": [
				"The a(n) factorizations for n = 45, 108, 135, 180, 252:",
				"  (45)      (4*27)        (135)       (4*45)        (4*63)",
				"  (5*9)     (2*6*9)       (3*45)      (12*15)       (12*21)",
				"  (3*15)    (3*4*9)       (5*27)      (4*5*9)       (4*7*9)",
				"  (3*3*5)   (2*2*27)      (9*15)      (2*2*45)      (6*6*7)",
				"            (2*2*3*9)     (3*5*9)     (2*6*15)      (2*2*63)",
				"            (2*2*3*3*3)   (3*3*15)    (3*4*15)      (2*6*21)",
				"                          (3*3*3*5)   (2*2*5*9)     (3*4*21)",
				"                                      (3*3*4*5)     (2*2*7*9)",
				"                                      (2*2*3*15)    (2*3*6*7)",
				"                                      (2*2*3*3*5)   (3*3*4*7)",
				"                                                    (2*2*3*21)",
				"                                                    (2*2*3*3*7)"
			],
			"mathematica": [
				"facs[n_]:=If[n\u003c=1,{{}},Join@@Table[Map[Prepend[#,d]\u0026,Select[facs[n/d],Min@@#\u003e=d\u0026]],{d,Rest[Divisors[n]]}]];",
				"Table[Length[Select[facs[n],OddQ@*Max]],{n,100}]"
			],
			"program": [
				"(PARI) A340831(n, m=n, fc=1) = if(1==n, !fc, my(s=0); fordiv(n, d, if((d\u003e1)\u0026\u0026(d\u003c=m)\u0026\u0026(!fc||(d%2)), s += A340831(n/d, d, 0*fc))); (s)); \\\\ _Antti Karttunen_, Dec 13 2021"
			],
			"xref": [
				"Positions of 0's are A000079.",
				"The version for partitions is A027193.",
				"The version for prime indices is A244991.",
				"The version looking at length instead of greatest factor is A339890.",
				"The version that also has odd length is A340607.",
				"The version looking at least factor is A340832.",
				"- Factorizations -",
				"A001055 counts factorizations.",
				"A045778 counts strict factorizations.",
				"A316439 counts factorizations by product and length.",
				"A340101 counts factorizations into odd factors, odd-length case A340102.",
				"A340653 counts balanced factorizations.",
				"- Odd -",
				"A000009 counts partitions into odd parts.",
				"A024429 counts set partitions of odd length.",
				"A026424 lists numbers with odd Omega.",
				"A058695 counts partitions of odd numbers.",
				"A066208 lists numbers with odd-indexed prime factors.",
				"A067659 counts strict partitions of odd length (A030059).",
				"A174726 counts ordered factorizations of odd length.",
				"A340692 counts partitions of odd rank.",
				"Cf. A026804, A050320, A061395, A339846, A340385, A340596, A340599, A340654, A340655, A340785, A340854, A340855."
			],
			"keyword": "nonn",
			"offset": "1,9",
			"author": "_Gus Wiseman_, Feb 04 2021",
			"ext": [
				"Data section extended up to 108 terms by _Antti Karttunen_, Dec 13 2021"
			],
			"references": 10,
			"revision": 12,
			"time": "2021-12-13T16:14:54-05:00",
			"created": "2021-02-04T20:53:07-05:00"
		}
	]
}