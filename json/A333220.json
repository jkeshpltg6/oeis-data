{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A333220",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 333220,
			"data": "0,1,2,3,4,6,8,7,10,12,16,14,32,24,20,15,64,26,128,28,40,48,256,30,36,96,42,56,512,52,1024,31,80,192,72,58,2048,384,160,60,4096,104,8192,112,84,768,16384,62,136,100,320,224,32768,106,144,120,640,1536",
			"name": "The number k such that the k-th composition in standard order consists of the prime indices of n in weakly increasing order.",
			"comment": [
				"The k-th composition in standard order (row k of A066099) is obtained by taking the set of positions of 1's in the reversed binary expansion of k, prepending 0, taking first differences, and reversing again.",
				"A prime index of n is a number m such that prime(m) divides n. The multiset of prime indices of n is row n of A112798."
			],
			"formula": [
				"A000120(a(n)) = A056239(n)."
			],
			"example": [
				"The sequence of terms together with the corresponding compositions begins:",
				"      0: ()             128: (8)             2048: (12)",
				"      1: (1)             28: (1,1,3)          384: (1,8)",
				"      2: (2)             40: (2,4)            160: (2,6)",
				"      3: (1,1)           48: (1,5)             60: (1,1,1,3)",
				"      4: (3)            256: (9)             4096: (13)",
				"      6: (1,2)           30: (1,1,1,2)        104: (1,2,4)",
				"      8: (4)             36: (3,3)           8192: (14)",
				"      7: (1,1,1)         96: (1,6)            112: (1,1,5)",
				"     10: (2,2)           42: (2,2,2)           84: (2,2,3)",
				"     12: (1,3)           56: (1,1,4)          768: (1,9)",
				"     16: (5)            512: (10)           16384: (15)",
				"     14: (1,1,2)         52: (1,2,3)           62: (1,1,1,1,2)",
				"     32: (6)           1024: (11)             136: (4,4)",
				"     24: (1,4)           31: (1,1,1,1,1)      100: (1,3,3)",
				"     20: (2,3)           80: (2,5)            320: (2,7)",
				"     15: (1,1,1,1)      192: (1,7)            224: (1,1,6)",
				"     64: (7)             72: (3,4)          32768: (16)",
				"     26: (1,2,2)         58: (1,1,2,2)        106: (1,2,2,2)"
			],
			"mathematica": [
				"primeMS[n_]:=If[n==1,{},Flatten[Cases[FactorInteger[n],{p_,k_}:\u003eTable[PrimePi[p],{k}]]]];",
				"fbi[q_]:=If[q=={},0,Total[2^q]/2];",
				"Table[fbi[Accumulate[Reverse[primeMS[n]]]],{n,100}]"
			],
			"xref": [
				"The version with prime indices taken in weakly decreasing order is A233249.",
				"A partial inverse is A333219.",
				"Cf. A000120, A029931, A048793, A056239, A066099, A070939, A112798, A114994, A225620, A228351, A333221."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Gus Wiseman_, Mar 17 2020",
			"references": 22,
			"revision": 9,
			"time": "2020-03-17T21:18:16-04:00",
			"created": "2020-03-17T21:18:16-04:00"
		}
	]
}