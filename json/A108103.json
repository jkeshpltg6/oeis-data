{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A108103",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 108103,
			"data": "1,2,1,3,1,2,1,3,1,3,1,2,1,3,1,2,1,3,1,3,1,2,1,3,1,3,1,2,1,3,1,2,1,3,1,3,1,2,1,3,1,2,1,3,1,3,1,2,1,3,1,3,1,2,1,3,1,2,1,3,1,3,1,2,1,3,1,3,1,2,1,3,1,2,1,3,1,3,1,2,1,3,1,2,1,3,1,3,1,2,1,3,1,3,1,2,1,3,1,2,1,3,1,3,1",
			"name": "Fixed point of the square of the morphism: 1-\u003e3, 2-\u003e1, 3-\u003e121, starting with 1.",
			"comment": [
				"Old name was: A Fibonacci like substitution for three-symbol substitution with characteristic polynomial: x^3-2*x-1.",
				"This sequence gives a three-symbol substitution for A095345.",
				"From _Michel Dekking_, Jan 06 2018: (Start) What is probably meant by this statement is that A095345 is a morphic sequence, i.e., a letter-to-letter projection of a fixed point of the morphism tau given by tau(1)=121, tau(2)=3, tau(3)=313, followed by the morphism pi given by pi(1)=1, pi(2)=1, pi(3)=3.",
				"This deserves a proof. In fact a proof can only be given if one formulates a joint statement about the sequences v:=A095345=1113111313... and w:=A095346=3131113...., because these two sequences are defined in a loop. Let D be the so-called differentiation operator which maps a word to the lengths of its runs, as studied in [Dekking, 1981]. For example D(1113111) = 313.",
				"The words v and w by definition satisfy D(v)=w, D(w)=v. They are in fact points of period 2 for D (cf. [Dekking,1995]).",
				"Claim: v=A095345 equals pi(x), where x is the fixed point of tau with x(1)=1, and w=A095346 equals pi(y), where y is the fixed point of tau with y(1)=3.",
				"Proof: This is easily shown by induction on n=2,3,..., proving that tau^(n+1)(1) and tau^(n)(3) satisfy D(pi(tau^(n+1)(1)) = pi(tau^n(3)) \u0026 D(pi(tau^n(3)) = pi(tau^n(1)).",
				"(End)",
				"Real Salem Roots: {{x -\u003e -1.}, {x -\u003e -0.618034}, {x -\u003e 1.61803}}.",
				"From _Michel Dekking_, Dec 27 2017: (Start)",
				"Let tau be the morphism squared: tau(1)=121, tau(2)=3, tau(3)=313.",
				"Then tau(a)=a.",
				"Claims:",
				"(A) a(2n-1) = 1 for n = 1,2,....",
				"(B) a(2n) = A282162(n-1) for n = 1,2,....",
				"Proof of (A): Obviously 2 only occurs in 121, but this implies that also 3 only occurs in 131.",
				"Proof of (B): Let R be the 'remove 1' operator, e.g.,  R(12131) = 23.",
				"Let psi be the square of the Fibonacci morphism on the alphabet {3,2}: psi(3)=323, psi(2)=32. One proves by induction that R(tau^k(1))3 = 2psi^(k-1)(3) and R(tau^k(2))2 = 3psi^(k-1)(2) for k=1,2,.... This implies (B): see CROSSREFS in A282162.",
				"We give the more complicated induction step of the two:",
				"       R(tau^(k+1)(2))2 = R(tau^k(3))2 =",
				"       R(tau^(k-1)(3))R(tau^(k-1)(1))R(tau^(k-1)(3))2 =",
				"       R(tau^k(2))2psi^(k-2)(3)3^(-1)R(tau^k(2))2 =",
				"       3psi^(k-2)(2)psi^(k-2)(3)psi^(k-1)(2) = 3psi^(k-2)(32332)=",
				"       3psi^k(2).",
				"(End)"
			],
			"reference": [
				"F. M. Dekking: \"What is the long range order in the Kolakoski sequence?\" in: The Mathematics of Long-Range Aperiodic Order, ed. R. V. Moody, Kluwer, Dordrecht (1997), pp. 115-125."
			],
			"link": [
				"Michel Dekking, \u003ca href=\"/A108103/b108103.txt\"\u003eTable of n, a(n) for n = 1..1200\u003c/a\u003e",
				"F. M. Dekking, \u003ca href=\"http://www.digizeitschriften.de/dms/img/?PID=GDZPPN002544490\"\u003eOn the structure of self-generating sequences\u003c/a\u003e, Seminar on Number Theory, 1980-1981 (Talence, 1980-1981), Exp. No. 31, 6 pp., Univ. Bordeaux I, Talence, 1981. Math. Rev. 83e:10075.",
				"F. M. Dekking, \u003ca href=\"http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.28.6839\"\u003eWhat Is the Long Range Order in the Kolakoski Sequence?\u003c/a\u003e, Report 95-100, Technische Universiteit Delft, 1995."
			],
			"formula": [
				"1-\u003e121, 2-\u003e3, 3-\u003e313."
			],
			"mathematica": [
				"s[1] = {3}; s[2] = {1}; s[3] = {1, 2, 1}; t[a_] := Flatten[s /@ a]; p[0] = {1}; p[1] = t[p[0]]; p[n_] := t[p[n - 1]] a = p[12]",
				"Nest[Flatten[# /. {1 -\u003e {3}, 2 -\u003e {1}, 3 -\u003e {1, 2, 1}}] \u0026, {1}, 10] (* _Robert G. Wilson v_, Nov 05 2015 *)"
			],
			"xref": [
				"Cf. A000045, A066983, A282162, A095345, A095346."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Roger L. Bagula_, Jun 03 2005",
			"ext": [
				"New name from _Joerg Arndt_, Jan 17 2013",
				"New name from _Robert G. Wilson v_, Nov 05 2015",
				"Name corrected by _Michel Dekking_, Dec 27 2017",
				"Offset 1 from _Michel Dekking_, Jan 01 2020"
			],
			"references": 7,
			"revision": 48,
			"time": "2020-01-03T16:30:24-05:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}