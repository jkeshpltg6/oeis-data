{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A142175",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 142175,
			"data": "1,1,1,1,8,1,1,36,36,1,1,133,420,133,1,1,449,3334,3334,449,1,1,1446,21939,49364,21939,1446,1,1,4534,130044,560957,560957,130044,4534,1,1,13991,724222,5459561,10284514,5459561,724222,13991,1,1,42747,3880014",
			"name": "T(n,k) = (1/4)*A007318(n,k) - (3/2)*A008292(n+1,k+1) + (9/4)*A060187(n+1,k+1), triangle read by rows (0 \u003c= k \u003c= n).",
			"comment": [
				"Row n gives the coefficients in the expansion of (1/4)*(1 + x)^n + (9/4)*2^n*(1 - x)^(1 + n)*Phi(x, -n, 1/2) - (3/2)*(1 - x)^(n + 2)*Phi(x, -1 - n, 1), where Phi is the Lerch transcendant."
			],
			"link": [
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Lerch_zeta_function\"\u003eLerch zeta function\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Polylogarithm\"\u003ePolylogarithm\u003c/a\u003e"
			],
			"formula": [
				"E.g.f.: (exp((1 + x)*y) - 6*(1 - x)^2*exp(y*(1 - x))/(1 - x*exp(y*(1 - x)))^2 + 9*(1 - x)*exp((1 - x)*y)/(1 - x*exp(2*(1 - x)*y)))/4. - _Franck Maminirina Ramaharo_, Oct 20 2018"
			],
			"example": [
				"Triangle begins:",
				"     1;",
				"     1,    1;",
				"     1,    8,      1;",
				"     1,   36,     36,      1;",
				"     1,  133,    420,    133,      1;",
				"     1,  449,   3334,   3334,    449,      1;",
				"     1, 1446,  21939,  49364,  21939,   1446,    1;",
				"     1, 4534, 130044, 560957, 560957, 130044, 4534, 1;",
				"      ... reformatted. - _Franck Maminirina Ramaharo_, Oct 21 2018"
			],
			"mathematica": [
				"p[x_, n_] = 1/4*(1 + x)^n + 9/4*2^n*(1 - x)^(1 + n)*LerchPhi[x, -n, 1/2] - 3/2*(1 - x)^(2 + n)*PolyLog[-1 - n, x]/x;",
				"Table[CoefficientList[FullSimplify[p[x, n]], x], {n, 0, 10}]// Flatten"
			],
			"program": [
				"(Maxima)",
				"A008292(n, k) := sum((-1)^j*(k - j)^n*binomial(n + 1, j), j, 0, k)$",
				"A060187(n, k) := sum((-1)^(k - j)*binomial(n, k - j)*(2*j - 1)^(n - 1), j, 1, k)$",
				"T(n, k) := (binomial(n, k) - 6*A008292(n + 1, k + 1) + 9*A060187(n + 1, k + 1))/4$",
				"create_list(T(n, k), n, 0, 10, k, 0, n);",
				"/* _Franck Maminirina Ramaharo_, Oct 20 2018 */"
			],
			"xref": [
				"Triangles related to Eulerian numbers: A008292, A046802, A060187, A123125.",
				"Cf. A142147, A168287, A168288, A168289, A168290, A168291, A168292, A168293."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "0,5",
			"author": "_Roger L. Bagula_ and _Gary W. Adamson_, Sep 16 2008",
			"ext": [
				"Edited, new name, and offset corrected by _Franck Maminirina Ramaharo_, Oct 19 2018"
			],
			"references": 9,
			"revision": 15,
			"time": "2018-10-22T10:35:07-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}