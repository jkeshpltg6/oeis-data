{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A051049",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 51049,
			"data": "1,1,4,7,16,31,64,127,256,511,1024,2047,4096,8191,16384,32767,65536,131071,262144,524287,1048576,2097151,4194304,8388607,16777216,33554431,67108864,134217727,268435456,536870911,1073741824",
			"name": "Number of moves needed to solve an (n+1)-ring baguenaudier if two simultaneous moves of the two end rings are counted as one.",
			"comment": [
				"Might be called the \"Purkiss sequence\", after Henry John Purkiss who in 1865 found that this is the number of moves for the accelerated Chinese Rings puzzle (baguenaudier). [Email from _Andreas M. Hinz_, Feb 15 2017, who also pointed out that there was an error in the definition in this entry]. - _N. J. A. Sloane_, Feb 18 2017",
				"The row sums of triangle A166692. - _Paul Curtz_, Oct 20 2009",
				"The inverse binomial transform equals (-1)^n*A062510(n) with an extra leading term 1. - _Paul Curtz_, Oct 20 2009",
				"This is the sequence A(1,1;1,2;1) of the family of sequences [a,b:c,d:k] considered by G. Detlefs, and treated as A(a,b;c,d;k) in the W. Lang link given below. - _Wolfdieter Lang_, Oct 18 2010",
				"Also, the decimal representation of the x-axis, from the origin to the right edge, of the n-th stage of growth of the two-dimensional cellular automaton defined by Rules 261, 269, 277, 285, 293, 301, 309, 317, 325, 333, 341, 349, 357, 365, 37, and 381, based on the 5-celled von Neumann neighborhood. - _Robert Price_, Jan 02 2017"
			],
			"reference": [
				"Andreas M. Hinz, The Lichtenberg sequence, Fib. Quart., 55 (2017), 2-12."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A051049/b051049.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Paul Barry, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL19/Barry/barry321.html\"\u003eJacobsthal Decompositions of Pascal's Triangle, Ternary Trees, and Alternating Sign Matrices\u003c/a\u003e, Journal of Integer Sequences, 19, 2016, #16.3.5.",
				"A. M. Hinz, S. Klavžar, U. Milutinović, C. Petr, \u003ca href=\"http://dx.doi.org/10.1007/978-3-0348-0237-6\"\u003eThe Tower of Hanoi - Myths and Maths\u003c/a\u003e, Birkhäuser 2013. See page 56. \u003ca href=\"http://tohbook.info\"\u003eBook's website\u003c/a\u003e",
				"Wolfdieter Lang, \u003ca href=\"/A051049/a051049.pdf\"\u003eNotes on certain inhomogeneous three term recurrences.\u003c/a\u003e [From _Wolfdieter Lang_, Oct 18 2010]",
				"N. J. A. Sloane, \u003ca href=\"http://arxiv.org/abs/1503.01168\"\u003eOn the Number of ON Cells in Cellular Automata\u003c/a\u003e, arXiv:1503.01168 [math.CO], 2015.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Baguenaudier.html\"\u003eBaguenaudier\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ElementaryCellularAutomaton.html\"\u003eElementary Cellular Automaton\u003c/a\u003e",
				"S. Wolfram, \u003ca href=\"http://wolframscience.com/\"\u003eA New Kind of Science\u003c/a\u003e",
				"Wolfram Research, \u003ca href=\"http://atlas.wolfram.com/\"\u003eWolfram Atlas of Simple Programs\u003c/a\u003e",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_2D_5-Neighbor_Cellular_Automata\"\u003eIndex to 2D 5-Neighbor Cellular Automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_Elementary_Cellular_Automata\"\u003eIndex to Elementary Cellular Automata\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,1,-2)."
			],
			"formula": [
				"a(n) = (2^(n+1) - (1 + (-1)^(n+1)))/2. - _Paul Barry_, Apr 24 2003",
				"a(n+2) = a(n+1) + 2*a(n) + 1, a(0)=a(1)=1. - _Paul Barry_, May 01 2003",
				"From _Paul Barry_, Sep 19 2003: (Start)",
				"G.f.: (1 - x + x^2)/((1 - x^2)*(1 - 2*x));",
				"e.g.f.: exp(2*x) - sinh(x). (End)",
				"a(n) = ((Sum_{k=0..n} 2^k) + (-1)^n)/2 = (A000225(n+1) + (-1)^n)/2. - _Paul Barry_, May 27 2003",
				"(a(n+1) - a(n))/3 = A001045(n). - _Paul Barry_, May 27 2003",
				"a(n) = Sum_{k=0..floor(n/2)} binomial(n+1, 2*k). - _Paul Barry_, May 27 2003",
				"a(n) = (Sum_{k=0..n} binomial(n,k) + (-1)^(n-k)) - 1. - _Paul Barry_, Jul 21 2003",
				"a(n) = Sum_{k=0..n} Sum_{j=0..n-k, (j-k) mod 2 = 0} binomial(n-k, j). - _Paul Barry_, Jan 25 2005",
				"Row sums of triangle A135221. - _Gary W. Adamson_, Nov 23 2007",
				"a(n) = A001045(n+1) + A000975(n+1) - A000079(n). - _Paul Curtz_, Oct 20 2009",
				"a(n) = 2*a(n-1) + a(n-2) - 2*a(n-3), a(0) = a(1) = 1, a(2) = 4. Observed by G. Detlefs. See the W. Lang link. - _Wolfdieter Lang_, Oct 18 2010",
				"a(n) = 3*a(n-1) - 2*a(n-2) + 3*(-1)^n. - _Gary Detlefs_, Dec 21 2010",
				"a(n) = 3* A000975(n-1) + 1, n \u003e 0. - _Gary Detlefs_, Dec 21 2010",
				"a(n+2) = A001969(2^n+1) + A000069(2^n); evil + odious. - _Johannes W. Meijer_, Jun 24 2011, Jun 26 2011",
				"E.g.f.: exp(2x) - sinh(x) = Q(0); Q(k) = 1 - k!*x^(k+1)/((2*k + 1)!*2^k - 2*(((2*k + 1)!*2^k)^2)/((2*k + 1)!*2^(k+1) - x^k*(k + 1)!/Q(k+1))); (continued fraction). - _Sergei N. Gladkovskii_, Nov 16 2011",
				"a(n) = Sum_{k=0..n} Sum_{i=0..n} C(k-1,i). - _Wesley Ivan Hurt_, Sep 21 2017",
				"a(n) = A000975(n+1) - A001045(n). - _Yuchun Ji_, Jul 08 2018"
			],
			"maple": [
				"A051049 := proc(n): (2^(n+1)-(1+(-1)^(n+1)))/2 end: seq(A051049(n),n=0..30); # _Johannes W. Meijer_, Jun 24 2011"
			],
			"mathematica": [
				"b[n_?EvenQ] := 2^(n-1) - 1; b[n_?OddQ] := 2^(n-1); Table[b[n], {n, 50}]]",
				"(* Second program: *)",
				"LinearRecurrence[{2, 1, -2}, {1, 1, 4}, 40] (* _Jean-François Alcover_, Jan 08 2019 *)"
			],
			"program": [
				"(MAGMA) [(2^(n+1)-(1+(-1)^(n+1)))/2: n in [0..40]]; // _Vincenzo Librandi_, Aug 14 2011",
				"(PARI) a(n)=2^(n-1)-(n%2==0) \\\\ _Charles R Greathouse IV_, Mar 22 2013"
			],
			"xref": [
				"Cf. A000975, A135221. Row sums of A131086."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Eric W. Weisstein_",
			"ext": [
				"Edited and information added by _Johannes W. Meijer_, Jun 24 2011"
			],
			"references": 19,
			"revision": 109,
			"time": "2020-02-29T02:32:41-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}