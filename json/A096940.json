{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A096940",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 96940,
			"data": "5,1,5,1,6,5,1,7,11,5,1,8,18,16,5,1,9,26,34,21,5,1,10,35,60,55,26,5,1,11,45,95,115,81,31,5,1,12,56,140,210,196,112,36,5,1,13,68,196,350,406,308,148,41,5,1,14,81,264,546,756,714,456,189,46,5,1,15,95,345,810,1302",
			"name": "Pascal (1,5) triangle.",
			"comment": [
				"This is the fifth member, q=5, in the family of (1,q) Pascal triangles: A007318 (Pascal (q=1), A029635 (q=2) (but with a(0,0)=2, not 1), A095660, A095666.",
				"This is an example of a Riordan triangle (see A053121 for a comment and the 1991 Shapiro et al. reference on the Riordan group) with o.g.f. of column nr. m of the type g(x)*(x*f(x))^m with f(0)=1. Therefore the o.g.f. for the row polynomials p(n,x) = Sum_{m=0..n} a(n,m)*x^m is G(z,x)=g(z)/(1-x*z*f(z)). Here: g(x)=(5-4*x)/(1-x), f(x)=1/(1-x), hence G(z,x)=(5-4*z)/(1-(1+x)*z).",
				"The SW-NE diagonals give Sum_{k=0..ceiling((n-1)/2)} a(n-1-k, k) = A022096(n-2), n\u003e=2, with n=1 value 5. Observation by _Paul Barry_, Apr 29 2004. Proof via recursion relations and comparison of inputs."
			],
			"link": [
				"David A. Corneth, \u003ca href=\"/A096940/b096940.txt\"\u003eTable of n, a(n) for n = 0..9999\u003c/a\u003e",
				"W. Lang, \u003ca href=\"/A096940/a096940.txt\"\u003eFirst 10 rows\u003c/a\u003e."
			],
			"formula": [
				"Recursion: a(n, m)=0 if m\u003en, a(0, 0)= 5; a(n, 0)=1 if n\u003e=1; a(n, m) = a(n-1, m) + a(n-1, m-1).",
				"G.f. column m (without leading zeros): (5-4*x)/(1-x)^(m+1), m\u003e=0.",
				"a(n,k) = (1+4*k/n)*binomial(n,k), for n\u003e0. - _Mircea Merca_, Apr 08 2012"
			],
			"example": [
				"Triangle begins:",
				"  5;",
				"  1,  5;",
				"  1,  6,  5;",
				"  1,  7, 11,   5;",
				"  1,  8, 18,  16,   5;",
				"  1,  9, 26,  34,  21,   5;",
				"  1, 10, 35,  60,  55,  26,   5;",
				"  1, 11, 45,  95, 115,  81,  31,   5;",
				"  1, 12, 56, 140, 210, 196, 112,  36,   5;",
				"  1, 13, 68, 196, 350, 406, 308, 148,  41,  5;",
				"  1, 14, 81, 264, 546, 756, 714, 456, 189, 46, 5; etc."
			],
			"maple": [
				"a(n,k):=piecewise(n=0,5,0\u003cn,(1+4*k/n)*binomial(n,k)) # _Mircea Merca_, Apr 08 2012"
			],
			"program": [
				"(PARI) a(n) = {if(n \u003c= 1, return(5 - 4*(n==1))); my(m = (sqrtint(8*n + 1) - 1)\\2, t = n - binomial(m + 1, 2)); (1+4*t/m)*binomial(m,t)} \\\\ _David A. Corneth_, Aug 28 2019"
			],
			"xref": [
				"Row sums: A007283(n-1), n\u003e=1, 5 if n=0; g.f.: (5-4*x)/(1-2*x). Alternating row sums are [5, -4, followed by 0's].",
				"Column sequences (without leading zeros) give for m=1..9, with n\u003e=0: A000027(n+5), A056000(n-1), A096941-7."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "0,1",
			"author": "_Wolfdieter Lang_, Jul 16 2004",
			"references": 13,
			"revision": 31,
			"time": "2019-08-29T11:57:26-04:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}