{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A048268",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 48268,
			"data": "6643,10,46,67,92,121,154,191,232,277,326,379,436,497,562,631,704,781,862,947,1036,1129,1226,1327,1432,1541,1654,1771,1892,2017,2146,2279,2416,2557,2702,2851,3004,3161,3322,3487,3656,3829,4006,4187,4372,4561",
			"name": "Smallest palindrome greater than n in bases n and n+1.",
			"comment": [
				"From _A.H.M. Smeets_, Jun 19 2019: (Start)",
				"In the following, dig(expr) stands for the digit that represents the value of expression expr, and . stands for concatenation.",
				"As for the naming of this sequence, the trivial 1 digit palindromes 0..dig(n-1) are excluded.",
				"If a number m is palindromic in bases n and n+1, then m has an odd number of digits when represented in base n.",
				"All three digit numbers in base n, that are palindromic in bases n and n+1 are given by:",
				"101_3                         22_4                      for n = 3,",
				"232_n                         1.dig(n).1_(n+1)",
				"343_n                         2.dig(n-1).2_(n+1)",
				"up to and including",
				"dig(n-2).dig(n-1).dig(n-2)_n  dig(n-3).4.dig(n-3)_(n+1) for n \u003e 3, and",
				"dig(n-1).0.dig(n-1)_n         dig(n-3).5.dig(n-3)_(n+1) for n \u003e 4.",
				"Let d_L(n) be the number of integers with L digits in base n (L being odd), being palindromic in bases n and n+1, then:",
				"d_1(n) = n for n \u003e= 2 (see above),",
				"d_3(n) = n-2 for n \u003e= 5 (see above),",
				"d_5(n) = n-1 for n \u003e= 7 and n == 1 (mod 3),",
				"d_5(n) = n-4 for n \u003e= 7 and n in {0, 2} (mod 3), and",
				"it seems that d_7(n) is of order O(n^2*log(n)) for n large enough. (End)"
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A048268/b048268.txt\"\u003eTable of n, a(n) for n = 2..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-3,1)."
			],
			"formula": [
				"a(n) = 2n^2 + 3n + 2 for n \u003e= 4 (which is 232_n and 1n1_(n+1)).",
				"a(n) = A130883(n+1) for n \u003e 3. - _Robert G. Wilson v_, Oct 08 2014",
				"From _Colin Barker_, Jun 30 2019: (Start)",
				"G.f.: x^2*(6643 - 19919*x + 19945*x^2 - 6684*x^3 + 19*x^4) / (1 - x)^3.",
				"a(n) = 3*a(n-1) - 3*a(n-2) + a(n-3) for n\u003e6.",
				"(End)"
			],
			"example": [
				"a(14) = 2*14^2 + 3*14 + 2 = 436, which is 232_14 and 1e1_15."
			],
			"mathematica": [
				"Do[ k = n + 2; While[ RealDigits[ k, n + 1 ][ [ 1 ] ] != Reverse[ RealDigits[ k, n + 1 ][ [ 1 ] ] ] || RealDigits[ k, n ][ [ 1 ] ] != Reverse[ RealDigits[ k, n ][ [ 1 ] ] ], k++ ]; Print[ k ], {n, 2, 75} ]",
				"palQ[n_Integer, base_Integer] := Block[{idn = IntegerDigits[n, base]}, idn == Reverse[idn]]; f[n_] := Block[{k = n + 2}, While[ !palQ[k, n] || !palQ[k, n + 1], k++ ]; k]; Table[ f[n], {n, 2, 48}] (* _Robert G. Wilson v_, Sep 29 2004 *)"
			],
			"program": [
				"(PARI) isok(j, n) = my(da=digits(j,n), db=digits(j,n+1)); (Vecrev(da)==da) \u0026\u0026 (Vecrev(db)==db);",
				"a(n) = {my(j = n); while(! isok(j, n), j++); j;} \\\\ _Michel Marcus_, Nov 16 2017",
				"(PARI) Vec(x^2*(6643 - 19919*x + 19945*x^2 - 6684*x^3 + 19*x^4) / (1 - x)^3 + O(x^50)) \\\\ _Colin Barker_, Jun 30 2019"
			],
			"xref": [
				"Cf. A029965, A029966, A048269, A060792, A097928, A097929, A097930, A097931, A099145, A099146, A099147, A099153."
			],
			"keyword": "nonn,easy,base",
			"offset": "2,1",
			"author": "Ulrich Schimke (ulrschimke(AT)aol.com)",
			"ext": [
				"More terms from _Robert G. Wilson v_, Aug 14 2000"
			],
			"references": 27,
			"revision": 39,
			"time": "2019-06-30T10:39:52-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}