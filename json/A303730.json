{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A303730",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 303730,
			"data": "1,0,1,3,10,35,128,483,1866,7344,29342,118701,485249,2001467,8319019,34810084,146519286,619939204,2635257950,11248889770,48198305528,207222648334,893704746508,3865335575201,16761606193951,72860178774410,317418310631983,1385703968792040",
			"name": "Number of noncrossing path sets on n nodes with each path having at least two nodes.",
			"comment": [
				"Paths are constructed using noncrossing line segments between the vertices of a regular n-gon. Isolated vertices are not allowed.",
				"A noncrossing path set is a noncrossing forest (A054727) where each tree is restricted to being a path."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A303730/b303730.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"Isaac DeJager, Madeleine Naquin, Frank Seidl, \u003ca href=\"https://www.valpo.edu/mathematics-statistics/files/2019/08/Drube2019.pdf\"\u003eColored Motzkin Paths of Higher Order\u003c/a\u003e, VERUM 2019."
			],
			"formula": [
				"G.f.: G(x)/x where G(x) is the reversion of x*(1 - 2*x)^2/(1 - 4*x + 5*x^2 - x^3)."
			],
			"example": [
				"Case n=3: There are 3 possibilities:",
				".",
				"     o       o       o",
				"    /         \\     / \\",
				"   o---o   o---o   o   o",
				".",
				"Case n=4: There are 10 possibilities:",
				".",
				"   o   o   o   o   o---o   o---o   o---o",
				"   |   |   |   |   |       |   |       |",
				"   o   o   o---o   o---o   o   o   o---o",
				".",
				"   o---o   o---o   o---o   o   o   o   o",
				"             /       \\     | / |   | \\ |",
				"   o---o   o---o   o---o   o   o   o   o",
				"."
			],
			"mathematica": [
				"InverseSeries[x*(1 - 2*x)^2/(1 - 4*x + 5*x^2 - x^3) + O[x]^30, x] // CoefficientList[#, x]\u0026 // Rest (* _Jean-François Alcover_, Jul 03 2018, from PARI *)"
			],
			"program": [
				"(PARI) Vec(serreverse(x*(1 - 2*x)^2/(1 - 4*x + 5*x^2 - x^3) + O(x^30)))"
			],
			"xref": [
				"Cf. A054727, A303729."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_Andrew Howroyd_, Apr 29 2018",
			"references": 4,
			"revision": 21,
			"time": "2019-12-15T11:29:59-05:00",
			"created": "2018-05-01T03:05:08-04:00"
		}
	]
}