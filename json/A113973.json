{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A113973",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 113973,
			"data": "1,-2,4,-2,2,0,4,-4,4,-2,0,0,2,-4,8,0,2,0,4,-4,0,-4,0,0,4,-2,8,-2,4,0,0,-4,4,0,0,0,2,-4,8,-4,0,0,8,-4,0,0,0,0,2,-6,4,0,4,0,4,0,8,-4,0,0,0,-4,8,-4,2,0,0,-4,0,0,0,0,4,-4,8,-2,4,0,8,-4,0,-2,0,0,4,0,8,0,0,0,0,-8,0,-4,0,0,4,-4,12,0,2,0,0,-4,8",
			"name": "Expansion of phi(x^3)^3/phi(x) where phi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) := Prod_{k\u003e=1} (1-(-q)^k) (see A121373), phi(q) := theta_3(q) := Sum_{k=-oo..oo} q^(k^2) (A000122), psi(q) := Sum_{k=0..oo} q^(k*(k+1)/2) (A010054), chi(q) := Prod_{k\u003e=0} (1+q^(2k+1)) (A000700)."
			],
			"reference": [
				"B. C. Berndt, Ramanujan's Notebooks Part V, Springer-Verlag, see p. 375 Entry 35."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A113973/b113973.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"a(n)=-2*b(n) where b(n) is multiplicative and b(2^e) = (1-3(-1)^e)/2 if e\u003e0, b(3^e) = 1, b(p^e) = e+1 if p == 1 (mod 6), b(p^e) = (1+(-1)^e)/2 if p == 5 (mod 6).",
				"Euler transform of period 12 sequence [ -2, 3, 4, 1, -2, -6, -2, 1, 4, 3, -2, -2, ...].",
				"Moebius transform is period 12 sequence [ -2, 6, 0, -2, 2, 0, -2, 2, 0, -6, 2, 0, ...].",
				"Expansion of (eta(q)^2*eta(q^4)^2*eta(q^6)^15)/ (eta(q^2)^5*eta(q^3)^6*eta(q^12)^6) in powers of q.",
				"G.f.: theta_3(q^3)^3/theta_3(q).",
				"G.f.: 1+2( Sum_{k\u003e0} x^(3k-1)/(1-(-x)^(3k-1)) - x^(3k-2)/(1-(-x)^(3k-2))) = 1 +2( Sum_{k\u003e0} (-1)^k x^k/(1+x^k+x^(2k)) +2 x^(4k)/(1+x^(4k)+x^(8k)) )."
			],
			"mathematica": [
				"s = EllipticTheta[3, 0, q^3]^3/EllipticTheta[3, 0, q] + O[q]^105; CoefficientList[s, q] (* _Jean-François Alcover_, Dec 04 2015 *)"
			],
			"program": [
				"(PARI) {a(n)=local(x); if(n\u003c1, n==0, x=valuation(n,2); if(n%2,-2,(3-(-1)^x))*sumdiv(n/2^x,d, kronecker(-3,d)))}",
				"(PARI) {a(n)=local(A,p,e); if(n\u003c1, n==0, A=factor(n); -2*prod(k=1,matsize(A)[1], if(p=A[k,1], e=A[k,2]; if(p==2, (-3+(-1)^e)/2, if(p==3, 1, if(p%6==1, e+1, !(e%2)))))))}",
				"(PARI) {a(n)=if(n\u003c1, n==0, -2*direuler(p=2,n, if(p==2, 2-(1+2*X)/(1-X^2), 1/(1-X)/(1-kronecker(-3,p)*X)))[n])}",
				"(PARI) {a(n)=local(A); if(n\u003c0, 0, A=sum(k=1,sqrtint(n), 2*x^k^2, 1+x*O(x^n)); polcoeff( subst(A+x*O(x^(n\\3)),x,x^3)^3/A, n))}",
				"(PARI) {a(n)=local(A); if(n\u003c0, 0, A=x*O(x^n); polcoeff( eta(x+A)^2*eta(x^4+A)^2*eta(x^6+A)^15/ eta(x^2+A)^5/eta(x^3+A)^6/eta(x^12+A)^6, n))}"
			],
			"xref": [
				"a(n)=-2*A113974(n) if n\u003e0."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Nov 10 2005",
			"references": 10,
			"revision": 16,
			"time": "2021-03-12T22:24:43-05:00",
			"created": "2006-01-24T03:00:00-05:00"
		}
	]
}