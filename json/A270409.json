{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A270409",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 270409,
			"data": "14,386,5868,2310,65954,100156,614404,2278660,570570,5030004,36703824,34374186,37460376,472592916,1059255456,211083730,259477218,5188948072,22555934280,16476937840,1697186964,50534154408,375708427812,647739636160,111159740692",
			"name": "Triangle read by rows: T(n,g) is the number of rooted maps with n edges and 5 faces on an orientable surface of genus g.",
			"comment": [
				"Row n contains floor((n-2)/2) terms."
			],
			"link": [
				"Gheorghe Coserea, \u003ca href=\"/A270409/b270409.txt\"\u003eRows n = 4..104, flattened\u003c/a\u003e",
				"Sean R. Carrell, Guillaume Chapuy, \u003ca href=\"http://arxiv.org/abs/1402.6300\"\u003eSimple recurrence formulas to count maps on orientable surfaces\u003c/a\u003e, arXiv:1402.6300 [math.CO], 2014."
			],
			"example": [
				"Triangle starts:",
				"n\\g    [0]           [1]           [2]           [3]",
				"[4]    14;",
				"[5]    386;",
				"[6]    5868,         2310;",
				"[7]    65954,        100156;",
				"[8]    614404,       2278660,      570570;",
				"[9]    5030004,      36703824,     34374186;",
				"[10]   37460376,     472592916,    1059255456,   211083730;",
				"[11]   259477218,    5188948072,   22555934280,  16476937840;",
				"[12]   ..."
			],
			"mathematica": [
				"Q[0, 1, 0] = 1; Q[n_, f_, g_] /; n \u003c 0 || f \u003c 0 || g \u003c 0 = 0;",
				"Q[n_, f_, g_] := Q[n, f, g] = 6/(n+1) ((2n-1)/3 Q[n-1, f, g] + (2n-1)/3 Q[n - 1, f-1, g] + (2n-3) (2n-2) (2n-1)/12 Q[n-2, f, g-1] + 1/2 Sum[l = n-k; Sum[v = f-u; Sum[j = g-i; Boole[l \u003e= 1 \u0026\u0026 v \u003e= 1 \u0026\u0026 j \u003e= 0] (2k-1) (2l-1) Q[k - 1, u, i] Q[l - 1, v, j], {i, 0, g}], {u, 1, f}], {k, 1, n}]);",
				"T[n_, g_] := Q[n, 5, g];",
				"Table[T[n, g], {n, 4, 12}, {g, 0, Quotient[n-2, 2]-1}] // Flatten (* _Jean-François Alcover_, Oct 18 2018 *)"
			],
			"program": [
				"(PARI)",
				"N = 12; F = 5; gmax(n) = n\\2;",
				"Q = matrix(N + 1, N + 1);",
				"Qget(n, g) = { if (g \u003c 0 || g \u003e n/2, 0, Q[n+1, g+1]) };",
				"Qset(n, g, v) = { Q[n+1, g+1] = v };",
				"Quadric({x=1}) = {",
				"  Qset(0, 0, x);",
				"  for (n = 1, length(Q)-1, for (g = 0, gmax(n),",
				"    my(t1 = (1+x)*(2*n-1)/3 * Qget(n-1, g),",
				"       t2 = (2*n-3)*(2*n-2)*(2*n-1)/12 * Qget(n-2, g-1),",
				"       t3 = 1/2 * sum(k = 1, n-1, sum(i = 0, g,",
				"       (2*k-1) * (2*(n-k)-1) * Qget(k-1, i) * Qget(n-k-1, g-i))));",
				"    Qset(n, g, (t1 + t2 + t3) * 6/(n+1))));",
				"};",
				"Quadric('x + O('x^(F+1)));",
				"v = vector(N+2-F, n, vector(1 + gmax(n-1), g, polcoeff(Qget(n+F-2, g-1), F)));",
				"concat(v)"
			],
			"xref": [
				"Cf. A270408."
			],
			"keyword": "nonn,tabf",
			"offset": "4,1",
			"author": "_Gheorghe Coserea_, Mar 17 2016",
			"references": 6,
			"revision": 24,
			"time": "2018-10-18T06:22:17-04:00",
			"created": "2016-03-18T03:53:56-04:00"
		}
	]
}