{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A083711",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 83711,
			"data": "1,1,1,2,1,4,1,5,3,7,1,14,1,13,8,20,1,33,1,40,14,44,1,85,6,79,25,117,1,181,1,196,45,233,17,389,1,387,80,545,1,750,1,839,165,1004,1,1516,12,1612,234,2040,1,2766,48,3142,388,3720,1,5295,1,5606,663,7038,83,9194,1,10379,1005",
			"name": "A083710(n) - A000041(n-1).",
			"comment": [
				"Number of integer partitions of n with no 1's with a part dividing all the others. If n \u003e 0, we can assume such a part is the smallest. - _Gus Wiseman_, Apr 18 2021"
			],
			"reference": [
				"L. M. Chawla, M. O. Levan and J. E. Maxfield, On a restricted partition function and its tables, J. Natur. Sci. and Math., 12 (1972), 95-101."
			],
			"formula": [
				"a(n) = Sum_{ d|n, d\u003cn} A000041(d-1)."
			],
			"example": [
				"From _Gus Wiseman_, Apr 18 2021: (Start)",
				"The a(6) = 4 through a(12) = 13 partitions:",
				"  (6)      (7)  (8)        (9)      (10)         (11)  (12)",
				"  (3,3)         (4,4)      (6,3)    (5,5)              (6,6)",
				"  (4,2)         (6,2)      (3,3,3)  (8,2)              (8,4)",
				"  (2,2,2)       (4,2,2)             (4,4,2)            (9,3)",
				"                (2,2,2,2)           (6,2,2)            (10,2)",
				"                                    (4,2,2,2)          (4,4,4)",
				"                                    (2,2,2,2,2)        (6,3,3)",
				"                                                       (6,4,2)",
				"                                                       (8,2,2)",
				"                                                       (3,3,3,3)",
				"                                                       (4,4,2,2)",
				"                                                       (6,2,2,2)",
				"                                                       (4,2,2,2,2)",
				"                                                       (2,2,2,2,2,2)",
				"(End)"
			],
			"maple": [
				"with(combinat): with(numtheory): a := proc(n) c := 0: l := sort(convert(divisors(n), list)): for i from 1 to nops(l)-1 do c := c+numbpart(l[i]-1) od: RETURN(c): end: for j from 2 to 100 do printf(`%d,`,a(j)) od: # _James A. Sellers_, Jun 21 2003"
			],
			"xref": [
				"Allowing 1's gives A083710.",
				"The strict case is A098965.",
				"The complement (except also without 1's) is counted by A338470.",
				"The dual version is A339619.",
				"A000005 counts divisors.",
				"A000041 counts partitions.",
				"A000070 counts partitions with a selected part.",
				"A006128 counts partitions with a selected position.",
				"A018818 counts partitions into divisors (strict: A033630).",
				"A167865 counts strict chains of divisors \u003e 1 summing to n.",
				"A339564 counts factorizations with a selected factor.",
				"Cf. A001787, A001792, A015723, A097986, A098743, A130689, A130714, A264401, A339563, A342193."
			],
			"keyword": "nonn,easy",
			"offset": "1,4",
			"author": "_N. J. A. Sloane_, Jun 16 2003",
			"ext": [
				"More terms from _James A. Sellers_, Jun 21 2003"
			],
			"references": 14,
			"revision": 19,
			"time": "2021-04-22T22:01:22-04:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}