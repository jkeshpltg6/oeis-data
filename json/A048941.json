{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A048941",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 48941,
			"data": "2,4,1,10,16,2,6,20,4,3,30,8,8,34,340,4,5,394,48,10,10,52,16,5,22,3040,6,46,70,12,12,74,50,6,64,26,6964,20,7,48670,96,14,14,100,36,7,970,178,30,302,198,1060,8,39,126,16,16,130,97684,8,25,502,6960,34",
			"name": "a(n) is twice the coefficient of 1 in the fundamental unit of Q(sqrt(A000037(n))) where A000037 lists the nonsquare numbers (Version 1).",
			"comment": [
				"From _Sean A. Irvine_, Jul 16 2021: (Start)",
				"These values are computed by Algorithm 5.7.2 in Cohen.",
				"Other methods of computation (see A346419) give different results, with the first difference at n=14.",
				"(End)",
				"a(n) is the smallest positive integer x satisfying the Pell equation x^2 - D*y^2 = +-4, where D = A000037(n). - _Jinyuan Wang_, Sep 08 2021"
			],
			"reference": [
				"Henri Cohen, A Course in Computational Algebraic Number Theory, Springer-Verlag, 1993."
			],
			"link": [
				"Jinyuan Wang, \u003ca href=\"/A048941/b048941.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"S. R. Finch, \u003ca href=\"https://web.archive.org/web/20160511035657/http://www.people.fas.harvard.edu/~sfinch/csolve/clss.pdf\"\u003eClass number theory\u003c/a\u003e",
				"Steven R. Finch, \u003ca href=\"/A000924/a000924.pdf\"\u003eClass number theory\u003c/a\u003e [Cached copy, with permission of the author]",
				"Sean A. Irvine, \u003ca href=\"https://github.com/archmageirvine/joeis/blob/master/src/irvine/oeis/a048/A048941.java\"\u003eJava program\u003c/a\u003e (github)",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/FundamentalUnit.html\"\u003eFundamental Unit\u003c/a\u003e."
			],
			"program": [
				"(PARI) a(n) = my(A, D=n+(1+sqrtint(4*n))\\2, d=sqrtint(D), p, q, t, u1, u2, v1, v2); if(d%2==D%2, p=d, p=d-1); u1=-p; u2=2; v1=1; v2=0; q=2; while(v2==0 || q!=t, A=(p+d)\\q; t=p; p=A*q-p; if(t==p \u0026\u0026 v2!=0, return((u2^2+D*v2^2)/q), t=A*u2+u1; u1=u2; u2=t; t=A*v2+v1; v1=v2; v2=t; t=q; q=(D-p^2)/q)); (u1*u2+D*v1*v2)/q; \\\\ _Jinyuan Wang_, Sep 08 2021"
			],
			"xref": [
				"Cf. A000037, A048942, A346419, A346420."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Eric W. Weisstein_",
			"ext": [
				"Name edited by _Michel Marcus_, Jun 26 2020",
				"Entry revised by _Sean A. Irvine_, Jul 13 2021"
			],
			"references": 6,
			"revision": 47,
			"time": "2021-09-08T13:19:59-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}