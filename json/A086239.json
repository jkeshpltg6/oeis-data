{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A086239",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 86239,
			"data": "3,3,4,9,8,1,3,2,5,2,9,9,9,9,3,1,8,1,0,6,3,3,1,7,1,2,1,4,8,7,5,4,3,5,7,3,7,7,9,9,7,5,3,8,0,7,5,5,0,7,7,0,4,8,1,0,8,0,2,0,5,7,8,8,4,5,2,2,2,8,4,3,2,7,1,8,8,4,1,1,0,6,2,4,8,9,9,6,3,1,0,2,9,8,0,3,3,4,5,3,9,2,4,8,6",
			"name": "Decimal expansion of Sum_{k\u003e=2} c(k)/prime(k), where c(k) = -1 if p == 1 (mod 4) and c(k) = +1 if p == 3 (mod 4).",
			"comment": [
				"This is Sum_{p prime, p\u003e=3} -(-4/p)/p where (-4/.) is the Legendre symbol and is equal to - L(1,(-4/.)) plus an absolutely convergent sum (and therefore converges)."
			],
			"reference": [
				"Henri Cohen, Number Theory, Volume II: Analytic and Modern Tools, GTM Vol. 240, Springer, 2007; see pp. 208-209.",
				"S. R. Finch, Mathematical Constants, Encyclopedia of Mathematics and its Applications, vol. 94, Cambridge University Press, pp. 94-98."
			],
			"link": [
				"Julien Benney, Mark Underwood, Andrew J. Walker and David Broadhurst, \u003ca href=\"/A086239/a086239.txt\"\u003eIs this a convergent series and if so what is its sum?\u003c/a\u003e, digest of 12 messages in primenumbers Yahoo group, Oct 26 - Oct 30, 2009. [Cached copy]",
				"David Broadhurst, \u003ca href=\"http://groups.yahoo.com/group/primenumbers/message/21083\"\u003epost in primenumbers group\u003c/a\u003e, Oct 29 2009. [Broken link]",
				"Henri Cohen, \u003ca href=\"http://www.math.u-bordeaux.fr/~cohen/hardylw.dvi\"\u003eHigh Precision Computation of Hardy-Littlewood Constants\u003c/a\u003e, (1991).",
				"Henri Cohen, \u003ca href=\"/A221712/a221712.pdf\"\u003eHigh-precision computation of Hardy-Littlewood constants\u003c/a\u003e. [pdf copy, with permission]",
				"David Dummit, Andrew Granville, and Hershy Kisilevsky, \u003ca href=\"https://arxiv.org/abs/1411.4594\"\u003eBig biases amongst products of two primes\u003c/a\u003e, Mathematika 62 (2016), pp. 502-507.",
				"R. J. Mathar, \u003ca href=\"http://arxiv.org/abs/1008.2547\"\u003eTable of Dirichlet L-series and prime zeta modulo functions for small moduli\u003c/a\u003e, arXiv:1008.2547 [math.NT], 2010-2015, variable S(m=4,r=2,s=1) Section 3.1",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PrimeSums.html\"\u003ePrime Sums\u003c/a\u003e"
			],
			"example": [
				"0.33498132529999..."
			],
			"mathematica": [
				"Do[Print[N[Log[2]/2 + Sum[Log[2^(4*n)*(2^(2*n + 1) + 1)*(2^(2*n + 3) - 4)*(Zeta[4*n + 2] / (Zeta[2*n + 1, 1/4] - Zeta[2*n + 1, 3/4])^2)] * MoebiusMu[2*n + 1]/(4*n + 2), {n, 1, m}], 120]], {m, 20, 200, 20}] (* _Vaclav Kotesovec_, Jun 28 2020 *)",
				"S[m_, n_, s_] := (t = 1; sums = 0; difs = 1; While[Abs[difs] \u003e 10^(-digits - 5) || difs == 0, difs = (MoebiusMu[t]/t) * Log[If[s*t == 1, DirichletL[m, n, s*t], Sum[Zeta[s*t, j/m]*DirichletCharacter[m, n, j]^t, {j, 1, m}]/m^(s*t)]]; sums = sums + difs; t++]; sums); $MaxExtraPrecision = 1000; digits = 121; RealDigits[Chop[N[-S[4, 2, 1], digits]], 10, digits-1][[1]] (* _Vaclav Kotesovec_, Jan 22 2021 *)"
			],
			"program": [
				"(PARI) /* the given number of primes and terms in the sum yield over 105 correct digits */ P=vector(15, k, (2-prime(k)%4)/prime(k)); -sum(s=1,60, moebius(s)/s*log( prod( k=2, #P, 1-P[k]^s, if(s%2, if(s==1, Pi/4, sumalt(k=0,(-1)^k/(2*k+1)^s)),zeta(s)*(1-1/2^s) ))), sum(k=2,#P, P[k], .)) \\\\ _M. F. Hasler_, Oct 29 2009"
			],
			"xref": [
				"Cf. A166509."
			],
			"keyword": "nonn,cons",
			"offset": "0,1",
			"author": "_Eric W. Weisstein_, Jul 13 2003",
			"ext": [
				"Edited by _N. J. A. Sloane_, Jun 10 2008",
				"Corrected a(9) and example, added a(10)-a(104) following Broadhurst and Cohen. - _M. F. Hasler_, Oct 29 2009"
			],
			"references": 5,
			"revision": 47,
			"time": "2021-08-28T04:53:04-04:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}