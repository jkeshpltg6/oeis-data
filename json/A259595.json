{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A259595",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 259595,
			"data": "3,7,27,71,267,703,2643,6959,26163,68887,258987,681911,2563707,6750223,25378083,66820319,251217123,661452967,2486793147,6547709351,24616714347,64815640543,243680350323,641608696079,2412186788883,6351271320247,23878187538507",
			"name": "Numerators of the other-side convergents to sqrt(6).",
			"comment": [
				"Suppose that a positive irrational number r has continued fraction [a(0), a(1), ... ]. Define sequences p(i), q(i), P(i), Q(i) from the numerators and denominators of finite continued fractions as follows:",
				"p(i)/q(i) = [a(0), a(1), ... a(i)] and P(i)/Q(i) = [a(0), a(1), ..., a(i) + 1]. The fractions p(i)/q(i) are the convergents to r, and the fractions P(i)/Q(i) are introduced here as the \"other-side convergents\" to r, because p(2k)/q(2k) \u003c r \u003c P(2k)/Q(2k) and P(2k+1)/Q(2k+1) \u003c r \u003c p(2k+1)/q(2k+1), for k \u003e= 0. Closeness of P(i)/Q(i) to r is indicated by",
				"|r - P(i)/Q(i)| \u003c |p(i)/q(i) - P(i)/Q(i)| = 1/(q(i)Q(i)), for i \u003e= 0."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A259595/b259595.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,10,0,-1)."
			],
			"formula": [
				"p(i)*Q(i) - P(i)*q(i) = (-1)^(i+1), for i \u003e= 0, where a(i) = P(i).",
				"a(n) = 10*a(n-2) - a(n-4) for n\u003e3. - _Colin Barker_, Jul 21 2015",
				"G.f.: (x^3-3*x^2+7*x+3) / (x^4-10*x^2+1). - _Colin Barker_, Jul 21 2015"
			],
			"example": [
				"For r = sqrt(6), the first 7 other-side convergents are 3, 7/3, 27/11, 71/29, 267/109, 703/287, 2643/1079. A comparison of convergents with other-side convergents:",
				"i    p(i)/q(i)           P(i)/Q(i)    p(i)*Q(i)-P(i)*q(i)",
				"0    2/1     \u003c sqrt(6) \u003c    3/1               -1",
				"1    5/2     \u003e sqrt(6) \u003e    7/3                1",
				"2    22/9    \u003c sqrt(6) \u003c   27/11              -1",
				"3    49/20   \u003e sqrt(6) \u003e   71/29               1",
				"4    218/89  \u003c sqrt(6) \u003c  267/109             -1",
				"5    485/198 \u003e sqrt(6) \u003e  703/287              1"
			],
			"mathematica": [
				"r = Sqrt[6]; a[i_] := Take[ContinuedFraction[r, 35], i];",
				"b[i_] := ReplacePart[a[i], i -\u003e Last[a[i]] + 1];",
				"t = Table[FromContinuedFraction[b[i]], {i, 1, 35}]",
				"u = Denominator[t]  (* A259594 *)",
				"v = Numerator[t]    (* A259595 *)"
			],
			"program": [
				"(PARI) Vec((x^3-3*x^2+7*x+3)/(x^4-10*x^2+1) + O(x^50)) \\\\ _Colin Barker_, Jul 21 2015"
			],
			"xref": [
				"Cf. A041006, A041007, A259594."
			],
			"keyword": "nonn,easy,frac",
			"offset": "0,1",
			"author": "_Clark Kimberling_, Jul 20 2015",
			"references": 2,
			"revision": 15,
			"time": "2015-08-16T12:04:02-04:00",
			"created": "2015-07-31T04:28:27-04:00"
		}
	]
}