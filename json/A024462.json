{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A024462",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 24462,
			"data": "1,1,1,1,2,1,1,5,7,3,1,8,22,24,9,1,11,46,90,81,27,1,14,79,228,351,270,81,1,17,121,465,1035,1323,891,243,1,20,172,828,2430,4428,4860,2916,729,1,23,232,1344,4914,11718,18144,17496,9477,2187,1,26,301,2040,8946,26460,53298,71928,61965,30618,6561",
			"name": "Triangle T(n,k) read by rows, arising in enumeration of catafusenes.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A024462/b024462.txt\"\u003eRows n = 0..20 of triangle, flattened\u003c/a\u003e",
				"S. J. Cyvin, B. N. Cyvin, and J. Brunvoll, \u003ca href=\"https://hrcak.srce.hr/177109\"\u003eUnbranched catacondensed polygonal systems containing hexagons and tetragons\u003c/a\u003e, Croatica Chem. Acta, 69 (1996), 757-774; see Table III (p. 767)."
			],
			"formula": [
				"T(n, k) = 3 * T(n-1, k-1) + T(n-1, k), starting with [1], [1, 1], [1, 2, 1].",
				"From _Petros Hadjicostas_, May 27 2019: (Start)",
				"T(n, k) = (n-2)!/(k! * (n-k)!) * (9*n*(n-1) - 4*k*(3*n-k-2)) * 3^(k-2) for n \u003e= max(k, 2) and k \u003e= 0. (See the top formula of p. 767 in Cyvin et al. (1996).)",
				"Bivariate g.f.: Sum_{n, k \u003e= 0} T(n, k) * x^n * y^k = 1 + x * (1 + y) + x^2 * (1 + y)^2/(1 - x - 3 * x * y).",
				"(End)"
			],
			"example": [
				"Triangle begins (rows indexed by n \u003e= 0 and columns by k \u003e= 0):",
				"   1;",
				"   1,  1;",
				"   1,  2,   1;",
				"   1,  5,   7,   3;",
				"   1,  8,  22,  24,    9;",
				"   1, 11,  46,  90,   81,   27;",
				"   1, 14,  79, 228,  351,  270,   81;",
				"   1, 17, 121, 465, 1035, 1323,  891, 243;",
				"   1, 20, 172, 828, 2430, 4428, 4860, 2916, 729;",
				"   ..."
			],
			"maple": [
				"## The following Maple program gives the Taylor expansion of the bivariate g.f. of T(n,k) in powers of x:",
				"T := proc (x, y) 1+x*(y+1)+x^2*(y+1)^2/(1-x-3*y*x) end proc;",
				"expand(taylor(T(x, y), x = 0, 20)); ## _Petros Hadjicostas_, May 27 2019"
			],
			"mathematica": [
				"T[n_, 0]:= 1; T[n_, k_]:= If[k\u003c0 || k\u003en, 0, If[n==1 \u0026\u0026 k==1, 1, If[n==2 \u0026\u0026 k==1, 2, If[k==n \u0026\u0026 n\u003e=2, 3^(n-2), 3*T[n-1, k-1] + T[n-1, k]]]]];",
				"Table[T[n, k], {n, 0, 10}, {k, 0, n}]//Flatten (* _G. C. Greubel_, May 30 2019 *)"
			],
			"program": [
				"(PARI) T(n,k)=if(n\u003c0||k\u003c0||k\u003en,0,if(n\u003c3,[[1],[1,1],[1,2,1]][n+1][k+1],3*T(n-1,k-1)+T(n-1,k))) \\\\ _Ralf Stephan_, Jan 25 2005",
				"(Sage)",
				"def T(n, k):",
				"    if (k\u003c0 and k\u003en): return 0",
				"    elif (k==0): return 1",
				"    elif (n==k==1): return 1",
				"    elif (n==2 and k==1): return 2",
				"    elif (n\u003e=2 and k==n): return 3^(n-2)",
				"    else: return 3*T(n-1, k-1) + T(n-1, k)",
				"[[T(n, k) for k in (0..n)] for n in (0..12)] # _G. C. Greubel_, May 30 2019"
			],
			"xref": [
				"Cf. A038763.",
				"Left-edge columns (essentially) include A016789 and A038764. Right-edge diagonal columns (essentially) include A000244, A038765, and A081892. Row sums are (essentially) A000302."
			],
			"keyword": "tabl,nonn,easy",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_, May 03 2000",
			"ext": [
				"More terms from _James A. Sellers_, May 03 2000",
				"Edited by _Ralf Stephan_, Jan 25 2005"
			],
			"references": 6,
			"revision": 40,
			"time": "2019-05-31T05:11:40-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}