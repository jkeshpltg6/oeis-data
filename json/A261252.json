{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A261252",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 261252,
			"data": "1,-1,2,-4,7,-10,14,-22,33,-45,62,-88,122,-163,216,-290,386,-502,650,-846,1093,-1393,1768,-2248,2844,-3565,4454,-5566,6927,-8566,10562,-13014,15986,-19543,23832,-29032,35272,-42700,51578,-62226,74906,-89909,107712",
			"name": "Expansion of f(-x^3) * f(-x^6) / (f(x) * f(-x^4)) in powers of x where f() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A261252/b261252.txt\"\u003eTable of n, a(n) for n = 0..2500\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of f(-x^3)^3 / (f(-x^2)^2 * f(x, x^2)) in powers of x where f(,) is a Ramanujan theta function.",
				"Expansion of (chi(-x^3)^3 / chi(-x)) * (psi(x^3) / psi(x))^2 in powers of x where chi(), psi() are Ramanujan theta functions.",
				"Expansion of q^(-1/6) * eta(q) * eta(q^3) * eta(q^6) / eta(q^2)^3 in powers of q.",
				"Euler transform of period 6 sequence [ -1, 2, -2, 2, -1, 0, ...].",
				"Given g.f. A(x), then B(q) = q * A(q^6) satisfies 0 = f(B(q), B(q^2)) where f(u, v) = (3*u^2 - v)^3 * v^3 - 4 * u^2 * (v - u^2) * (2*u^2 - v).",
				"a(n) = A261251(3*n).",
				"Convolution inverse is A132179.",
				"a(n) ~ (-1)^n * exp(Pi*sqrt(2*n/3)) / (6^(5/4) * n^(3/4)). - _Vaclav Kotesovec_, Jun 06 2018"
			],
			"example": [
				"G.f. = 1 - x + 2*x^2 - 4*x^3 + 7*x^4 - 10*x^5 + 14*x^6 - 22*x^7 + ...",
				"G.f. = q - q^7 + 2*q^13 - 4*q^19 + 7*q^25 - 10*q^31 + 14*q^37 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ x^3] QPochhammer[ x^6] / (QPochhammer[ -x] QPochhammer[ x^4]), {x, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A) * eta(x^3 + A) * eta(x^6 + A) / eta(x^2 + A)^3, n))};"
			],
			"xref": [
				"Cf. A132179, A261251."
			],
			"keyword": "sign",
			"offset": "0,3",
			"author": "_Michael Somos_, Aug 12 2015",
			"references": 2,
			"revision": 11,
			"time": "2021-03-12T22:24:48-05:00",
			"created": "2015-08-12T14:05:28-04:00"
		}
	]
}