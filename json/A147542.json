{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A147542",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 147542,
			"data": "1,2,1,4,2,1,4,18,8,8,18,17,40,50,88,396,210,296,492,690,1144,1776,2786,3545,6704,10610,16096,25524,39650,63544,97108,269154,236880,389400,589298,956000,1459960,2393538,3604880,5739132,9030450,14777200",
			"name": "Product(1 + a(n)*x^n, n=1..infinity) = sum(F(k+1)*x^k, k=1..infinity) = 1/(1-x-x^2), where F(n) = A000045(n) (Fibonacci numbers).",
			"comment": [
				"A formal infinite product representation for the Fibonacci numbers (A000045(n+1)).",
				"For references see A147541. [From _R. J. Mathar_, Mar 12 2009]"
			],
			"link": [
				"Jean-François Alcover, \u003ca href=\"/A147542/b147542.txt\"\u003eTable of n, a(n) for n = 1..200\u003c/a\u003e",
				"W. Lang \u003ca href=\"/A147542/a147542.txt\"\u003eTwo recurrences for the general problem.\u003c/a\u003e",
				"R. J. Mathar, \u003ca href=\"http://list.seqfan.eu/pipermail/seqfan/2008-November/000113.html\"\u003eRe: polynomial-to-product transform\u003c/a\u003e, Maple code (2008). [From _R. J. Mathar_, Mar 12 2009]"
			],
			"formula": [
				"Comments from Wolfdieter Lang, Mar 06 2009 (Start): Recurrence I: With FP(n,m) the set of partitions of n with m distinct parts (which could be called fermionic partitions (fp)):",
				"a(n)= F(n+1) - sum(sum(product(a(k[j]),j=1..m),fp from FP(n,m)),m=2..maxm(n)), with maxm(n):=A003056(n) and the distinct parts k[j], j=1,...,m, of the partition fp of n, n\u003e=3. Inputs a(1)=F(2)=1, a(2)=F(3)=2. See the array A008289(n,m) for the cardinality of the set FP(n,m).",
				"Recurrence II: With the definition of FP(n,m) from the above recurrence I, P(n,m) the general set of partitions of n with m parts, and the multinomial numbers M_0 (given for every partition under A048996):",
				"a(n) = sum((d/n)*(-a(d)^(n/d)),d|n with 1\u003cd\u003cn) + sum(((-1)^(m-1))*(1/m)*sum(M_0(p)*F(2)^e(1)*...*F(n+1)^e(n),p=(1^e(1),...,n^e(n)) from P(n,m)),m=1..n-1), n\u003e=2; a(1)=F(2)=1. The exponents e(j)\u003e=0 satisfy sum(j*e(j),j=1..n)=n and sum(e(j),j=1..m). The M_0 numbers are m!/product(e(j)!,j=1..n).",
				"Example of recurrence I: a(4) = F(5) - a(1)*a(3) = 5 - 1*1 = 4.",
				"Example of recurrence II: a(4)= 2*(-1)^2 + (1*F(5)-(1/2)*(2*F(2)*F(4) + 1*F(3)^2) + (1/3)*3*F(2)^2*F(3)) = 4. (End)"
			],
			"mathematica": [
				"m = 200;",
				"sol = Thread[CoefficientList[Sum[Log[1 + a[n] x^n], {n, 1, m}] - Log[1/(1 - x - x^2)] + O[x]^(m + 1), x] == 0] // Solve // First;",
				"Array[a, m] /. sol (* _Jean-François Alcover_, Oct 22 2019 *)"
			],
			"xref": [
				"Cf. A000045, A137852, A006973, A157159."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Neil Fernandez_, Nov 06 2008",
			"ext": [
				"More terms and revised description from _Wolfdieter Lang_ Mar 06 2009",
				"Edited by _N. J. A. Sloane_, Mar 11 2009 at the suggestion of Vladeta Jovovic",
				"More terms from _R. J. Mathar_, Mar 12 2009"
			],
			"references": 9,
			"revision": 13,
			"time": "2019-10-22T16:26:40-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}