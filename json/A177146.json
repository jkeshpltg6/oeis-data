{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A177146",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 177146,
			"data": "0,-3,15,-45,0,1260,-11340,56700,0,-3742200,48648600,-340540200,0,40864824000,-694702008000,6252318072000,0,-1187940433680000,24946749107280000,-274414240180080000,0,75738330289702080000,-1893458257242552000000,24614957344153176000000,0",
			"name": "n-th derivative of arctan(x) at x = 1, n \u003e= 4.",
			"comment": [
				"d^ny/dx^n = (((-1)^(n-1))*(n-1)!)*sin(n*arctan(1/x)) /(1+x^2)^(n/2) - (proof by recurrence). If n = 1, 2, 3, the values of the derivatives at x=1 are respectively 1/2, -1/2, 1/2.",
				"d^ny/dx^n = n!*sum(k=1..n, (binomial(n-1,k-1)*(-1)^(n-k)*x^(n-k)*(1+x^2)^(-n)*(-1)^((k-1)/2)*(1+(-1)^(k-1)))/(2*k)). - _Vladimir Kruchinin_, Apr 22 2011"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A177146/b177146.txt\"\u003eTable of n, a(n) for n = 4..400\u003c/a\u003e"
			],
			"formula": [
				"a(n) = (((-1)^(n-1))*(n-1)!)*sin(n*arctan(1))/2^(n/2).",
				"a(n) = 2^(-n-1)*n!*sum(k=1..n, (((-1)^(k-1)+1)*(-1)^(n-k+(k-1)/2)*binomial(n-1,k-1))/k). - _Vladimir Kruchinin_, Apr 22 2011",
				"abs(a(n)) = abs(integrate(x=0..infty, sin(x)*exp(-x)*x^(n-1))) (see Mathematica code below). - _John M. Campbell_, Jun 21 2011",
				"E.g.f.: arctan(x+1). - _Alois P. Heinz_, Feb 14 2015"
			],
			"example": [
				"a(5) = -3 because d^5y/dx^5 = 384*x^4/(1 + x^2)^5 - 288*x^2/(1 + x^2)^4 + 24/(1 + x^2)^3, and for x=1 we obtain 384/32 - 288/16 + 24/8 = -3."
			],
			"maple": [
				"# First program, with the formula:",
				"n0:= 50: T:=array(1..n0+1):for n from 1 to n0 do:T[n]:=(((-1)^(n-1))*(n-1) !)*sin(n*arctan(1)) /(2^(n/2)):od:print(T):",
				"# Second program, with the Maple instruction D(f):",
				"n0:= 50: T:=array(1..n0+1):f:=x-\u003earctan(x):for n from 1 to n0 do:D(f): T[n]:=(D(f)(1)):f:=D(f):od: print(T):",
				"# third Maple program:",
				"a:= n-\u003e n!*coeff(series(arctan(x+1), x, n+1), x, n):",
				"seq(a(n), n=4..40);  # _Alois P. Heinz_, Feb 14 2015"
			],
			"mathematica": [
				"Table[Abs[Integrate[Sin[x]*E^(-x)*(x^(n - 1)), {x, 0, Infinity}]], {n, 4, 28}] (* _John M. Campbell_, Jun 21 2011 *)"
			],
			"program": [
				"(Maxima)",
				"a(n):=2^(-n-1)*n!*sum((((-1)^(k-1)+1)*(-1)^(n-k+(k-1)/2)*binomial(n-1,k-1))/k,k,1,n); /* _Vladimir Kruchinin_, Apr 22 2011 */"
			],
			"xref": [
				"Cf. A005359 (n-th derivatives of arctan(x) at x = 0)."
			],
			"keyword": "sign",
			"offset": "4,2",
			"author": "_Michel Lagneau_, May 03 2010",
			"references": 1,
			"revision": 25,
			"time": "2015-02-23T21:29:03-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}