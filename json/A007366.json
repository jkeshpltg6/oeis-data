{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007366",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7366,
			"id": "M4685",
			"data": "1,10,22,28,30,46,52,54,58,66,70,78,82,102,106,110,126,130,136,138,148,150,166,172,178,190,196,198,210,222,226,228,238,250,262,268,270,282,292,294,306,310,316,330,342,346,358,366,372,378,382,388,418,430,438",
			"name": "Numbers k such that phi(x) = k has exactly 2 solutions.",
			"comment": [
				"From _Torlach Rush_, Dec 21 2017: (Start)",
				"Properties of the two inverses of the terms of this sequence, I1 and I2:",
				"- I1 is nonsquarefree iff I2 is nonsquarefree.",
				"- The sum of the Dirichlet inverse of Euler's totient function for I1 and I2 is always 0.",
				"- The larger value of the Dirichlet inverse of Euler's totient function of I1 and I2 equals a(n) iff both I1 and I2 are squarefree.",
				"- (I1+I2) is divisible by 3.",
				"(End)",
				"If the number of distinct prime factors of k equals the number of solutions of k = phi(x), then the greatest common divisor of the solutions is the least solution. - _Torlach Rush_, Jul 24 2018",
				"For n \u003e 1, a(n) is nonsquarefree if the lesser solution is divisible by an even number of primes. - _Torlach Rush_, Dec 22 2017",
				"Contains {2*3^(6k+1): k \u003e= 1} as a subsequence. This is the simplest proof for the infinity of these numbers (see Sierpiński, Exercise 12, p. 237). - _Franz Vrabec_, Aug 21 2021"
			],
			"reference": [
				"M. Abramowitz and I. A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, 1964 (and various reprintings), p. 840.",
				"W. Sierpiński, Elementary Theory of Numbers, Warszawa, 1964.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A007366/b007366.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"R. G. Wilson v, \u003ca href=\"/A007015/a007015.pdf\"\u003eLetter to N. J. A. Sloane, Jul. 1992\u003c/a\u003e"
			],
			"formula": [
				"#({phi^-1(a(n))}) = 2. - _Torlach Rush_, Dec 22 2017"
			],
			"example": [
				"10 = phi(11) = phi(22)."
			],
			"maple": [
				"select(nops@numtheory:-invphi=2, [$1..1000]); # _Robert Israel_, Dec 20 2017"
			],
			"mathematica": [
				"a = Table[ 0, {500} ]; Do[ p = EulerPhi[ n ]; If[ p \u003c 501, a[ [ p ] ]++ ], {n, 1, 500} ]; Select[ Range[ 500 ], a[ [ # ] ] == 2 \u0026 ]",
				"(* Second program: *)",
				"With[{nn = 1325}, TakeWhile[Union@ Select[KeyValueMap[{#1, Length@ #2} \u0026, PositionIndex@ Array[EulerPhi, nn]], Last@ # == 2 \u0026][[All, 1]], # \u003c nn/3 \u0026] ] (* _Michael De Vlieger_, Dec 20 2017 *)"
			],
			"xref": [
				"Cf. A000010, A007367."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, _Mira Bernstein_, _Robert G. Wilson v_",
			"references": 16,
			"revision": 92,
			"time": "2021-09-20T00:58:38-04:00",
			"created": "1994-05-09T03:00:00-04:00"
		}
	]
}