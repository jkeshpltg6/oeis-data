{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A195601",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 195601,
			"data": "1,2,2,2,2,5,5,5,20,36,78,842,5291,10373,17340,28619,35586,93572,98045,2470364,13603654,14328528,16490766,833971648,1788088151,9592330101,10952282168,40005288076,54302548920,118523737357,776601533408,1241894797770,24485470725324",
			"name": "Engel expansion of beta = 3/(2*log(alpha/2)); alpha = A195596.",
			"comment": [
				"beta = 1.95302570335815413945406288542575380414251340201036319609354... is used to measure the expected height of random binary search trees.",
				"Cf. A006784 for definition of Engel expansion."
			],
			"reference": [
				"F. Engel, Entwicklung der Zahlen nach Stammbrüchen, Verhandlungen der 52. Versammlung deutscher Philologen und Schulmänner in Marburg, 1913, pp. 190-191."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A195601/b195601.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"F. Engel, \u003ca href=\"/A006784/a006784.pdf\"\u003eEntwicklung der Zahlen nach Stammbruechen\u003c/a\u003e, Verhandlungen der 52. Versammlung deutscher Philologen und Schulmaenner in Marburg, 1913, pp. 190-191. English translation by Georg Fischer, included with his permission.",
				"P. Erdős and Jeffrey Shallit, \u003ca href=\"http://www.numdam.org/item?id=JTNB_1991__3_1_43_0\"\u003eNew bounds on the length of finite Pierce and Engel series\u003c/a\u003e, Sem. Theor. Nombres Bordeaux (2) 3 (1991), no. 1, 43-53.",
				"B. Reed, \u003ca href=\"http://doi.acm.org/10.1145/765568.765571\"\u003eThe height of a random binary search tree\u003c/a\u003e, J. ACM, 50 (2003), 306-332.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/EngelExpansion.html\"\u003eEngel Expansion\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Engel_expansion\"\u003eEngel Expansion\u003c/a\u003e",
				"\u003ca href=\"/index/El#Engel\"\u003eIndex entries for sequences related to Engel expansions\u003c/a\u003e"
			],
			"formula": [
				"beta = 3/(2*log(alpha/2)) = 3*alpha/(2*alpha-2), where alpha = A195596 = -1/W(-exp(-1)/2) and W is the Lambert W function.",
				"A195582(n)/A195583(n) = alpha*log(n) - beta*log(log(n)) + O(1)."
			],
			"maple": [
				"alpha:= solve(alpha*log((2*exp(1))/alpha)=1, alpha):",
				"beta:= 3/(2*log(alpha/2)):",
				"engel:= (r, n)-\u003e `if`(n=0 or r=0, NULL, [ceil(1/r), engel(r*ceil(1/r)-1, n-1)][]):",
				"Digits:=400: engel(evalf(beta), 39);"
			],
			"mathematica": [
				"f:= N[-1/ProductLog[-1/(2*E)], 500001]; EngelExp[A_, n_]:= Join[Array[1 \u0026, Floor[A]], First@Transpose@NestList[{Ceiling[1/Expand[#[[1]] #[[2]] - 1]], Expand[#[[1]] #[[2]] - 1]} \u0026, {Ceiling[1/(A - Floor[A])], A - Floor[A]}, n - 1]]; EngelExp[N[3/(2*Log[f/2]), 500000], 25] (* _G. C. Greubel_, Oct 21 2016 *)"
			],
			"xref": [
				"Cf. A195599 (decimal expansion), A195600 (continued fraction), A195581, A195582, A195583, A195596, A195597, A195598."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Alois P. Heinz_, Sep 21 2011",
			"references": 7,
			"revision": 32,
			"time": "2020-11-22T17:36:47-05:00",
			"created": "2011-09-21T17:35:41-04:00"
		}
	]
}