{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A292671",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 292671,
			"data": "1,2,4,4,6,9,4,6,11,16,8,7,13,18,25,8,8,13,18,27,36,8,10,13,20,29,38,49,8,10,13,20,32,38,51,64,16,13,14,22,33,40,53,66,81,16,15,14,22,33,40,56,66,83,100,16,16,15,23,33,41,57,68,85,102",
			"name": "Upper right triangle A(m,n) = least number of symbols required to fill a grid of size n X n row by row in the greedy way such that in no row or column or m X m square a symbol occurs more than once.",
			"comment": [
				"Consider the symbols as positive integers. By the greedy way we mean to fill the grid row by row from left to right always with the least possible positive integer such that the three constraints (on rows, columns and rectangular blocks) are satisfied. In contrast to the sudoku case, the m X m rectangles have \"floating\" borders, so the constraint is actually equivalent to say that any element must be different from all neighbors in a Moore neighborhood of range m-1 (having up to (2m-1)^2 grid points). See A292673 for examples.",
				"One can consider the infinite square array A(m,n) defined in the same way, but the lower triangular part of it is uninteresting: for m\u003en one has A(m,n) = n^2, i.e., the columns continue below the diagonal indefinitely with the same value, n^2."
			],
			"link": [
				"Eric W. Weisstein, \u003ca href=\"http://mathworld.wolfram.com/MooreNeighborhood.html\"\u003eMoore Neighborhood\u003c/a\u003e, on MathWorld--A Wolfram Web Resource."
			],
			"example": [
				"The infinite square array would look as follows: (but the sequence only lists the upper right triangle: 1; 2, 4; 4, 6, 9; 4, 6, 11, 16; ...):",
				"   [1  2  4  4  8  8  8  8 16 ...]  m=1: A(1,n) = 2^ceil(log_2(n)) = A062383(n-1)",
				"   [1\\_4  6  6  7  8 10 10 13 ...]  m=2: A(2,n) = A292672(n)",
				"   [1  4\\_9 11 13 13 13 13 14 ...]  m=3: A(3,n) = A292673(n) : see here",
				"   [1  4  9\\16 18 18 20 20 22 ...]  m=4: A(4,n) = A292674(n) :  for examples",
				"   [1  4  9 16\\25 27 29 32 33 ...]  m=5: A(5,n) = A292675(n)",
				"   [1  4  9 16 25\\36 38 38 40 ...]  m=6: A(6,n) = A292676(n)",
				"   [1  4  9 16 25 36\\49 51 53 ...]  m=7: A(7,n) = A292677(n)",
				"   [1  4  9 16 25 36 49\\64 66 ...]  m=8: A(8,n) = A292678(n)",
				"   [1  4  9 16 25 36 49 64\\81 ...]  m=9: A(9,n) = A292679(n)",
				"   [...   ...  ...  ...  ...  ...]"
			],
			"program": [
				"(PARI) A(m, n, g=matrix(n, n))={my(ok(g, k, i, j, m)=if(m, ok(g[i, ], k)\u0026\u0026ok(g[, j], k)\u0026\u0026ok(concat(Vec(g[max(1, i-m+1)..i, max(1, j-m+1)..min(#g, j+m-1)])), k), !setsearch(Set(g), k))); for(i=1, n, for(j=1, n, for(k=1, n^2, ok(g, k, i, j, m)\u0026\u0026(g[i, j]=k)\u0026\u0026break))); vecmax(g)} \\\\ without \"vecmax\" the program returns the full n X n board."
			],
			"xref": [
				"Cf. A292670, A292672, A292673, ..., A292679"
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_M. F. Hasler_, Sep 20 2017",
			"references": 9,
			"revision": 14,
			"time": "2017-09-25T12:00:32-04:00",
			"created": "2017-09-21T15:34:56-04:00"
		}
	]
}