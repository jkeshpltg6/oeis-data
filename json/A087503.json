{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A087503",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 87503,
			"data": "1,3,6,12,21,39,66,120,201,363,606,1092,1821,3279,5466,9840,16401,29523,49206,88572,147621,265719,442866,797160,1328601,2391483,3985806,7174452,11957421,21523359,35872266,64570080,107616801,193710243",
			"name": "a(n) = 3(a(n-2) + 1), with a(0) = 1, a(1) = 3.",
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A087503/b087503.txt\"\u003eTable of n, a(n) for n = 0..2000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1, 3, -3)."
			],
			"formula": [
				"a(n) = a(n-1) + A038754(n). (i.e., partial sums of A038754).",
				"From _Hieronymus Fischer_, Sep 19 2007, formulas adjusted to offset, Dec 29 2012: (Start)",
				"G.f.: g(x) = (1+2x)/((1-3x^2)(1-x)).",
				"a(n) = (3/2)*(3^((n+1)/2)-1) if n is odd, else a(n) = (3/2)*(5*3^((n-2)/2)-1).",
				"a(n) = (3/2)*(3^floor((n+1)/2) + 3^floor(n/2) - 3^floor((n-1)/2)-1).",
				"a(n) = 3^floor((n+1)/2) + 3^floor((n+2)/2)/2 - 3/2.",
				"a(n) = A132667(a(n+1)) - 1.",
				"a(n) = A132667(a(n-1) + 1) for n \u003e 0.",
				"A132667(a(n)) = a(n-1) + 1 for n \u003e 0.",
				"Also numbers such that: a(0)=1, a(n) = a(n-1) + (p-1)*p^((n+1)/2 - 1) if n is odd, else a(n) = a(n-1) + p^(n/2), where p=3.",
				"(End)",
				"a(n) = A052993(n)+2*A052993(n-1). - _R. J. Mathar_, Sep 10 2021"
			],
			"maple": [
				"A087503 := proc(n)",
				"    option remember;",
				"    if n \u003c=1 then",
				"        op(n+1,[1,3]) ;",
				"    else",
				"        3*procname(n-2)+3 ;",
				"    end if;",
				"end proc:",
				"seq(A087503(n),n=0..20) ; # _R. J. Mathar_, Sep 10 2021"
			],
			"mathematica": [
				"RecurrenceTable[{a[0]==1,a[1]==3,a[n]==3(a[n-2]+1)},a,{n,40}] (* or *) LinearRecurrence[{1,3,-3},{1,3,6},40] (* _Harvey P. Dale_, Jan 01 2015 *)"
			],
			"program": [
				"(MAGMA) [(3/2)*(3^Floor((n+1)/2)+3^Floor(n/2)-3^Floor((n-1)/2)-1): n in [0..40]]; // _Vincenzo Librandi_, Aug 16 2011"
			],
			"xref": [
				"Sequences with similar recurrence rules: A027383 (p=2), A133628 (p=4), A133629 (p=5).",
				"Other related sequences for different p: A016116 (p=2), A038754 (p=3), A084221 (p=4), A133632 (p=5).",
				"See A133629 for general formulas with respect to the recurrence rule parameter p.",
				"Related sequences: A132666, A132667, A132668, A132669."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Reinhard Zumkeller_, Sep 11 2003",
			"ext": [
				"Additional comments from _Hieronymus Fischer_, Sep 19 2007",
				"Edited by _N. J. A. Sloane_, May 04 2010. I merged two essentially identical entries with different offsets, so some of the formulas may need to be adjusted.",
				"Formulas and MAGMA prog adjusted to offset 0 by _Hieronymus Fischer_, Dec 29 2012"
			],
			"references": 10,
			"revision": 29,
			"time": "2021-09-10T06:28:39-04:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}