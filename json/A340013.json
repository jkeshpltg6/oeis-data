{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A340013",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 340013,
			"data": "1,3,7,4,6,27,15,11,7,15,45,10,45,38,45,39,95,30,31,52,93,102,95,48,22,84,127,54,94,40,19,145,87,129,49,22,85,68,66,88,90,78,146,95,156,78,71,79,225,60,65,175,66,305,192,196,215,205,420,101,186,213,160",
			"name": "The prime gap, divided by two, which surrounds n!.",
			"comment": [
				"A theorem states that between (n+1)! + 2 and (n+1)! + (n+1) inclusive, there are n consecutive composite integers, namely 2, 3, 4, ..., n, n+1.",
				"Records: 1, 3, 7, 27, 45, 95, 102, 127, 145, 146, 156, 225, 305, 420, 804, 844, 1173, 1671, 1725, 1827, 2570, 2930, 3318, 5142, 5946, 6837, 7007, 8208, 10221, ..., ."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A340013/b340013.txt\"\u003eTable of n, a(n) for n = 3..607\u003c/a\u003e"
			],
			"formula": [
				"a(n) = (A037151(n) - A006990(n))/2 = (A033932(n) + A033933(n))/2.",
				"a(n) = A054588(n)/2 = A058054(n)/2. - _Alois P. Heinz_, Jan 09 2021"
			],
			"example": [
				"For a(1), there are no positive primes which surround 1!. Therefore a(1) is undefined.",
				"For a(2), there are two contiguous primes {2, 3} with 2 being 2!. The prime gap is 1. However, the two primes do not surround 2!, so a(2) is undefined.",
				"For a(3), the following set of numbers, {5, 6, 7}, with 3! being in the middle. The prime gap is 2; therefore, a(3) = 1.",
				"For a(4), the following set of numbers, {23, 24, 25, 26, 27, 28, 29} with 4! in between the two primes 23 \u0026 29. The prime gap is 6, so a(4) = 3."
			],
			"maple": [
				"a:= n-\u003e (f-\u003e (nextprime(f-1)-prevprime(f+1))/2)(n!):",
				"seq(a(n), n=3..70);  # _Alois P. Heinz_, Jan 09 2021"
			],
			"mathematica": [
				"a[n_] := (NextPrime[n!, 1] - NextPrime[n!, -1])/2; Array[a, 70, 3]"
			],
			"program": [
				"(PARI) a(n) = (nextprime(n!+1) - precprime(n!-1))/2; \\\\ _Michel Marcus_, Jan 11 2021",
				"(Python)",
				"from sympy import factorial, nextprime, prevprime",
				"def A340013(n):",
				"    f = factorial(n)",
				"    return (nextprime(f)-prevprime(f))//2 # _Chai Wah Wu_, Jan 23 2021"
			],
			"xref": [
				"Cf. A000142, A002981, A006990, A033932, A033933, A037151, A054588, A058054, A073308."
			],
			"keyword": "nonn",
			"offset": "3,2",
			"author": "_Robert G. Wilson v_, Jan 09 2021",
			"references": 3,
			"revision": 37,
			"time": "2021-01-23T23:25:48-05:00",
			"created": "2021-01-23T07:48:25-05:00"
		}
	]
}