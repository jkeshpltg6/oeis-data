{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A046716",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 46716,
			"data": "1,1,1,1,3,1,1,6,8,1,1,10,29,24,1,1,15,75,145,89,1,1,21,160,545,814,415,1,1,28,301,1575,4179,5243,2372,1,1,36,518,3836,15659,34860,38618,16072,1,1,45,834,8274,47775,163191,318926,321690,125673,1,1,55,1275,16290",
			"name": "Coefficients of a special case of Poisson-Charlier polynomials.",
			"comment": [
				"Diagonals: A000012, A000217; A000012, A002104. - _Philippe Deléham_, Jun 12 2004",
				"The sequence a(n) = Sum_{k = 0..n} T(n,k)*x^(n-k) is the binomial transform of the sequence b(n) = (n+x-1)! / (x-1)!. - _Philippe Deléham_, Jun 18 2004"
			],
			"link": [
				"E. A. Enneking and J. C. Ahuja, \u003ca href=\"http://www.fq.math.ca/Scanned/14-1/enneking.pdf\"\u003eGeneralized Bell numbers\u003c/a\u003e, Fib. Quart., 14 (1976), 67-73.",
				"C. Radoux, \u003ca href=\"http://www.mat.univie.ac.at/~slc/opapers/s28radoux.html\"\u003eDéterminants de Hankel et théorème de Sylvester\u003c/a\u003e, Séminaire Lotharingien de Combinatoire, B28b (1992), 9 pp."
			],
			"formula": [
				"Reference gives a recurrence.",
				"Sum_{k = 0..n} T(n, k)*x^(n-k) = A000522(n), A001339(n), A082030(n) for x = 1, 2, 3 respectively. Sum_{k = 0..n} T(n, k)*2^k = A081367(n). - _Philippe Deléham_, Jun 12 2004",
				"Let P(x, n) = Sum_{k = 0..n} T(n, k)*x^k, then Sum_{n\u003e=0} P(x, n)*t^n / n! = exp(xt)/(1-xt)^(1/x). - _Philippe Deléham_, Jun 12 2004",
				"T(n, 0) = 1, T(n, k) = (-1)^k * Sum_{i=n-k..n} (-1)^i*C(n, i)*S1(i, n-k), where S1 = Stirling numbers of first kind (A008275)."
			],
			"example": [
				"Triangle starts:",
				"  1;",
				"  1,  1;",
				"  1,  3,  1;",
				"  1,  6,  8,  1;",
				"  1, 10, 29, 24,  1;",
				"  ..."
			],
			"maple": [
				"a := proc(n,k) option remember;",
				"   if k = 0 then 1",
				"elif k \u003c 0 then 0",
				"elif k = n then (-1)^n",
				"else a(n-1,k) - n*a(n-1,k-1) - (n-1)*a(n-2,k-2) fi end:",
				"A046716 := (n,k) -\u003e abs(a(n,k));",
				"seq(seq(A046716(n,k),k=0..n),n=0..9); # _Peter Luschny_, Apr 05 2011"
			],
			"mathematica": [
				"t[_, 0] = 1; t[n_, k_] := (-1)^k*Sum[(-1)^i*Binomial[n, i]*StirlingS1[i, n-k], {i, n-k, n}]; Table[t[n, k] // Abs, {n, 0, 10}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Jan 10 2014 *)"
			],
			"keyword": "nonn,tabl,easy",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Vladeta Jovovic_, Jun 15 2004"
			],
			"references": 15,
			"revision": 30,
			"time": "2020-01-21T00:05:10-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}