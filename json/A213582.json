{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A213582",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 213582,
			"data": "1,5,2,16,9,3,42,27,13,4,99,68,38,17,5,219,156,94,49,21,6,466,339,213,120,60,25,7,968,713,459,270,146,71,29,8,1981,1470,960,579,327,172,82,33,9,4017,2994,1972,1207,699,384,198,93,37,10,8100,6053,4007,2474,1454,819,441,224,104,41,11",
			"name": "Rectangular array:  (row n) = b**c, where b(h) = -1 + 2^h, c(h) = n-1+h, n\u003e=1, h\u003e=1, and ** = convolution.",
			"comment": [
				"Principal diagonal: A213583.",
				"Antidiagonal sums: A156928.",
				"Row 1, (1,3,7,15,31,...)**(1,2,3,4,5,...): A002662.",
				"Row 2, (1,3,7,15,31,...)**(2,3,4,5,6,...)",
				"Row 3, (1,3,7,15,31,...)**(3,4,5,6,7,...)",
				"For a guide to related arrays, see A213500."
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A213582/b213582.txt\"\u003eAntidiagonals n = 1..60, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = 5*T(n,k-1) - 9*T(n,k-2) + 7*T(n,k-3) - 2*T(n,k-4).",
				"G.f. for row n: f(x)/g(x), where f(x) = n - (n-1)*x and g(x) = (1-2*x) *(1-x)^3.",
				"T(n,k) = 2*(n+1)*(2^k - 1) - k*(k + 2*n + 3)/2. - _G. C. Greubel_, Jul 08 2019"
			],
			"example": [
				"Northwest corner (the array is read by falling antidiagonals):",
				"1...5....16...42....99....219",
				"2...9....27...68....156...339",
				"3...13...38...94....213...459",
				"4...17...49...120...270...579",
				"5...21...60...146...327...699",
				"6...25...71...172...384...819"
			],
			"mathematica": [
				"(* First program *)",
				"b[n_]:= 2^n - 1; c[n_]:= n;",
				"T[n_, k_]:= Sum[b[k-i] c[n+i], {i, 0, k-1}]",
				"TableForm[Table[T[n, k], {n, 1, 10}, {k, 1, 10}]]",
				"Flatten[Table[T[n-k+1, k], {n, 12}, {k, n, 1, -1}]] (* A213582 *)",
				"r[n_]:= Table[T[n, k], {k, 40}]",
				"Table[T[n, n], {n, 1, 40}] (* A213583 *)",
				"s[n_]:= Sum[T[i, n+1-i], {i, 1, n}]",
				"Table[s[n], {n, 1, 50}] (* A156928 *)",
				"(* Second program *)",
				"Table[2*(k+1)*(2^(n-k+1) -1) -(n-k+1)*(n+k+4)/2, {n, 12}, {k, n}]//Flatten (* _G. C. Greubel_, Jul 08 2019 *)"
			],
			"program": [
				"(PARI) t(n,k) = 2*(k+1)*(2^(n-k+1) -1) -(n-k+1)*(n+k+4)/2;",
				"for(n=1,12, for(k=1,n, print1(t(n,k), \", \"))) \\\\ _G. C. Greubel_, Jul 08 2019",
				"(MAGMA) [[2*(k+1)*(2^(n-k+1) -1) -(n-k+1)*(n+k+4)/2: k in [1..n]]: n in [1..12]]; // _G. C. Greubel_, Jul 08 2019",
				"(Sage) [[2*(k+1)*(2^(n-k+1) -1) -(n-k+1)*(n+k+4)/2 for k in (1..n)] for n in (1..12)] # _G. C. Greubel_, Jul 08 2019",
				"(GAP) Flat(List([1..12], n-\u003e List([1..n], k-\u003e 2*(k+1)*(2^(n-k+1) -1) -(n-k+1)*(n+k+4)/2 ))) # _G. C. Greubel_, Jul 08 2019"
			],
			"xref": [
				"Cf. A213500, A213571."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Jun 19 2012",
			"references": 4,
			"revision": 11,
			"time": "2019-07-08T17:56:20-04:00",
			"created": "2012-06-21T22:27:41-04:00"
		}
	]
}