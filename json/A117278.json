{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A117278",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 117278,
			"data": "1,1,0,1,1,1,0,1,1,1,1,1,0,1,1,1,0,1,2,1,0,2,1,1,1,1,0,2,2,1,0,1,2,2,1,1,1,1,2,2,2,1,0,2,1,3,2,1,1,0,1,3,2,3,2,1,0,2,2,3,3,2,1,1,1,0,4,3,3,3,2,1,0,2,2,4,3,4,2,1,1,1,1,3,4,5,3,3,2,1,0,2,2,6,4,4,4,2,1,1,0,1,5,3,6",
			"name": "Triangle read by rows: T(n,k) is the number of partitions of n into k prime parts (n\u003e=2, 1\u003c=k\u003c=floor(n/2)).",
			"comment": [
				"Row n has floor(n/2) terms. Row sums yield A000607. T(n,1) = A010051(n) (the characteristic function of the primes). T(n,2) = A061358(n). Sum(k*T(n,k), k\u003e=1) = A084993(n)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A117278/b117278.txt\"\u003eRows n = 2..200, flattened\u003c/a\u003e"
			],
			"formula": [
				"G.f.: G(t,x) = -1+1/product(1-tx^(p(j)), j=1..infinity), where p(j) is the j-th prime."
			],
			"example": [
				"T(12,3) = 2 because we have [7,3,2] and [5,5,2].",
				"Triangle starts:",
				"  1;",
				"  1;",
				"  0, 1;",
				"  1, 1;",
				"  0, 1, 1;",
				"  1, 1, 1;",
				"  0, 1, 1, 1;",
				"  0, 1, 2, 1;",
				"  ..."
			],
			"maple": [
				"g:=1/product(1-t*x^(ithprime(j)),j=1..30): gser:=simplify(series(g,x=0,30)): for n from 2 to 22 do P[n]:=sort(coeff(gser,x^n)) od: for n from 2 to 22 do seq(coeff(P[n],t^j),j=1..floor(n/2)) od; # yields sequence in triangular form",
				"# second Maple program:",
				"b:= proc(n, i) option remember;",
				"      `if`(n=0, [1], `if`(i\u003c1, [], zip((x, y)-\u003ex+y, b(n, i-1),",
				"       [0, `if`(ithprime(i)\u003en, [], b(n-ithprime(i), i))[]], 0)))",
				"    end:",
				"T:= n-\u003e subsop(1=NULL, b(n, numtheory[pi](n)))[]:",
				"seq(T(n), n=2..25);  # _Alois P. Heinz_, Nov 16 2012"
			],
			"mathematica": [
				"(* As triangle: *) nn=20;a=Product[1/(1-y x^i),{i,Table[Prime[n],{n,1,nn}]}];Drop[CoefficientList[Series[a,{x,0,nn}],{x,y}],2,1]//Grid (* _Geoffrey Critzer_, Oct 30 2012 *)"
			],
			"program": [
				"(PARI)",
				"parts(n, pred)={prod(k=1, n, if(pred(k), 1/(1-y*x^k) + O(x*x^n), 1))}",
				"{my(n=15); apply(p-\u003eVecrev(p/y), Vec(parts(n, isprime)-1))} \\\\ _Andrew Howroyd_, Dec 28 2017"
			],
			"xref": [
				"Columns k=1-10 give: A010051, A061358, A068307, A259194, A259195, A259196, A259197, A259198, A259200, A259201.",
				"Row sums give A000607.",
				"T(A000040(n),n) gives A259254(n).",
				"Cf. A084993, A219180."
			],
			"keyword": "nonn,tabf",
			"offset": "2,19",
			"author": "_Emeric Deutsch_, Mar 07 2006",
			"references": 21,
			"revision": 32,
			"time": "2021-09-08T18:54:09-04:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}