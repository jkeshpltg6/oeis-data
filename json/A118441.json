{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A118441",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 118441,
			"data": "0,1,0,-4,2,0,-12,12,3,0,32,-48,-24,4,0,80,-160,-120,40,5,0,-192,480,480,-240,-60,6,0,-448,1344,1680,-1120,-420,84,7,0,1024,-3584,-5376,4480,2240,-672,-112,8,0,2304,-9216,-16128,16128,10080,-4032,-1008,144,9,0",
			"name": "Triangle L, read by rows, equal to the matrix log of A118435, with the property that L^2 consists of a single diagonal (two rows down from the main diagonal).",
			"comment": [
				"L = log(A118435) = log(H*[C^-1]*H], where C=Pascal's triangle and H=A118433 where H^2 = I (identity matrix)."
			],
			"formula": [
				"For even exponents of L, L^(2m) is a single diagonal:",
				"if n == k+2m, then [L^(2m)](n,k) = n!/k!*2^(n-k-2m)/(n-k-2m)!; else if n != k+2m: [L^(2m)](n,k) = 0.",
				"For odd exponents of L:",
				"if n \u003e= k+2m+1, then [L^(2m+1)](n,k) = n!/k!*2^(n-k-2m-1)/(n-k-2m-1)!*(-1)^(m+[(n+1)/2]-[k/2]+n-k); else if n \u003c k+2m+1: [L^(2m)](n,k) = 0.",
				"Unsigned row sums equals A027471(n+1) = n*3^(n-1)."
			],
			"example": [
				"The matrix log, L = log(H*[C^-1]*H], begins:",
				"     0;",
				"     1,     0;",
				"    -4,     2,      0;",
				"   -12,    12,      3,     0;",
				"    32,   -48,    -24,     4,     0;",
				"    80,  -160,   -120,    40,     5,     0;",
				"  -192,   480,    480,  -240,   -60,     6,     0;",
				"  -448,  1344,   1680, -1120,  -420,    84,     7,   0;",
				"  1024, -3584,  -5376,  4480,  2240,  -672,  -112,   8,  0;",
				"  2304, -9216, -16128, 16128, 10080, -4032, -1008, 144,  9,  0;",
				"  ...",
				"The matrix square, L^2, is a single diagonal:",
				"  0;",
				"  0, 0;",
				"  2, 0,  0;",
				"  0, 6,  0,  0;",
				"  0, 0, 12,  0,  0;",
				"  0, 0,  0, 20,  0,  0;",
				"  0, 0,  0,  0, 30,  0,  0;",
				"  ...",
				"From _Peter Luschny_, Apr 23 2020: (Start)",
				"In unsigned form and without the main diagonal, as computed by the Maple script:",
				"  [0], [0]",
				"  [1], [1]",
				"  [2], [4,   2]",
				"  [3], [12,  12,   3]",
				"  [4], [32,  48,   24,   4]",
				"  [5], [80,  160,  120,  40,   5]",
				"  [6], [192, 480,  480,  240,  60,  6]",
				"  [7], [448, 1344, 1680, 1120, 420, 84, 7] (End)"
			],
			"maple": [
				"# Generalized Worpitzky transform of the harmonic numbers.",
				"CL := p -\u003e PolynomialTools:-CoefficientList(expand(p), x):",
				"H := n -\u003e add(1/k, k=1..n):",
				"Trow := proc(n) local k,v; if n=0 then return [0] fi;",
				"add(add((-1)^(n-v)*binomial(k,v)*H(k)*(-x+v-1)^n, v=0..k), k=0..n); CL(%) end:",
				"for n from 0 to 7 do Trow(n) od; # _Peter Luschny_, Apr 23 2020"
			],
			"program": [
				"(PARI) /* From definition of L as matrix log of H*C^-1*H: */",
				"{L(n,k)=local(H=matrix(n+1,n+1,r,c,if(r\u003e=c,binomial(r-1,c-1)*(-1)^(r\\2-(c-1)\\2+r-c))),C=matrix(n+1,n+1,r,c,if(r\u003e=c,binomial(r-1,c-1))),N=(H*C^-1*H)); Log=sum(p=1,n+1,-(N^0-N)^p/p);Log[n+1,k+1]}",
				"for(n=0, 10, for(k=0, n, print1(L(n, k), \", \")); print(\"\"))",
				"(PARI) /* The matrix power L^m is given by: */",
				"{L(n,k,m)=if(m%2==0,if(n==k+m,n!/k!*2^(n-k-m)/(n-k-m)!), if(n\u003e=k+m,n!/k!*2^(n-k-m)/(n-k-m)!*(-1)^(m\\2+(n+1)\\2-k\\2+n-k)))}",
				"for(n=0, 10, for(k=0, n, print1(L(n, k,1), \", \")); print(\"\"))"
			],
			"xref": [
				"Cf. A118435 (exp(L)), A118442 (column 0), A118443 (row sums), A027471 (unsigned row sums); A118433 (self-inverse triangle), A001815 (column 1?), A001789 (third of column 2?)."
			],
			"keyword": "sign,tabl",
			"offset": "0,4",
			"author": "_Paul D. Hanna_, Apr 28 2006",
			"references": 5,
			"revision": 19,
			"time": "2020-04-23T18:49:21-04:00",
			"created": "2006-05-19T03:00:00-04:00"
		}
	]
}