{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A182097",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 182097,
			"data": "1,0,1,1,1,2,2,3,4,5,7,9,12,16,21,28,37,49,65,86,114,151,200,265,351,465,616,816,1081,1432,1897,2513,3329,4410,5842,7739,10252,13581,17991,23833,31572,41824,55405,73396,97229,128801,170625,226030,299426,396655,525456,696081,922111,1221537,1618192,2143648,2839729,3761840,4983377,6601569,8745217",
			"name": "Expansion of 1/(1-x^2-x^3).",
			"comment": [
				"Number of compositions (ordered partitions) into parts 2 and 3. [_Joerg Arndt_, Aug 21 2013]",
				"a(n) is the top left entry of the n-th power of any of the 3X3 matrices [0, 1, 1; 0, 0, 1; 1, 0, 0], [0, 1, 0; 1, 0, 1; 1, 0, 0], [0, 1, 1; 1, 0, 0; 0, 1, 0] or [0, 0, 1; 1, 0, 0; 1, 1, 0]. - _R. J. Mathar_, Feb 03 2014",
				"Conjectured values of d(n), the dimension of a Z-module in MZV(conv). See the Waldschmidt link. - _Michael Somos_, Mar 14 2014",
				"Shannon et al. (2006) call these the Van der Laan numbers. - _N. J. A. Sloane_, Jan 11 2022"
			],
			"reference": [
				"A. G. Shannon, P. G. Anderson and A. F. Horadam, Properties of Cordonnier, Perrin and Van der Laan numbers, International Journal of Mathematical Education in Science and Technology, Volume 37:7 (2006), 825-831. See R_n.",
				"Michel Waldschmidt, \"Multiple Zeta values and Euler-Zagier numbers\", in Number theory and discrete mathematics, International conference in honour of Srinivasa Ramanujan, Center for Advanced Study in Mathematics, Panjab University, Chandigarh, (Oct 02, 2000)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A182097/b182097.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"M. Hoffman, \u003ca href=\"http://dx.doi.org/10.1006/jabr.1997.7127\"\u003eThe algebra of multiharmonic series\u003c/a\u003e, Journ. of Alg., Vol. 192, Issue 2 (Aug 1997), 477-495.",
				"I. E. Leonard and A. C. F. Liu, \u003ca href=\"http://www.jstor.org/stable/10.4169/amer.math.monthly.119.04.333\"\u003eA familiar recurrence occurs again\u003c/a\u003e, Amer. Math. Monthly, 119 (2012), 333-336.",
				"R. J. Mathar, \u003ca href=\"https://arxiv.org/abs/1406.7788\"\u003eTilings of rectangular regions by rectangular tiles: Counts derived from transfer matrices\u003c/a\u003e, arXiv:1406.7788 (2014), eq. (32).",
				"Michel Waldschmidt, \u003ca href=\"https://webusers.imj-prg.fr/~michel.waldschmidt/articles/pdf/Transparents.pdf\"\u003eMultiple Zeta values and Euler-Zagier numbers\u003c/a\u003e, Slides, Number theory and discrete mathematics, International conference in honour of Srinivasa Ramanujan, Center for Advanced Study in Mathematics, Panjab University, Chandigarh, (Oct 02, 2000).",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,1,1)."
			],
			"formula": [
				"G.f.: 1 / (1 - x^2 - x^3).",
				"a(n) = A000931(n+3).",
				"a(n) = A176971(-n). a(n) = a(n-2) + a(n-3) for all n in Z. - _Michael Somos_, Dec 13 2013",
				"a(n-7) = A133034(n).  a(n-5) = A078027(n).  a(n-3) = A000931(n).  a(n+2) = A134816(n).  a(n+4) = A164001(n) if n\u003e1. - _Michael Somos_, Dec 13 2013"
			],
			"example": [
				"G.f. = 1 + x^2 + x^3 + x^4 + 2*x^5 + 2*x^6 + 3*x^7 + 4*x^8 + 5*x^9 + ..."
			],
			"mathematica": [
				"a[ n_] := If[n \u003c 0, SeriesCoefficient[ (1 + x) / (1 + x - x^3), {x, 0, -n}], SeriesCoefficient[ 1 / (1 - x^2 - x^3), {x, 0, n}]]; (* _Michael Somos_, Dec 13 2013 *)",
				"CoefficientList[Series[1/(1-x^2-x^3),{x,0,60}],x] (* or *) LinearRecurrence[ {0,1,1},{1,0,1},70] (* _Harvey P. Dale_, Dec 04 2014 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, polcoeff( (1 + x) / (1 + x - x^3) + x * O(x^-n), -n), polcoeff( 1 / (1 - x^2 - x^3) + x * O(x^n), n))}; /* _Michael Somos_, Dec 13 2013 */",
				"(PARI) Vec(1/(1-x^2-x^3) + O(x^99)) \\\\ _Altug Alkan_, Sep 02 2016",
				"(MAGMA) m:=50; R\u003cx\u003e:=PowerSeriesRing(Integers(), m); Coefficients(R!(1/(1-x^2-x^3))); // _G. C. Greubel_, Aug 11 2018"
			],
			"xref": [
				"The following are basically all variants of the same sequence: A000931, A078027, A096231, A124745, A133034, A134816, A164001, A182097, A228361 and probably A020720. However, each one has its own special features and deserves its own entry."
			],
			"keyword": "nonn,easy,changed",
			"offset": "0,6",
			"author": "_N. J. A. Sloane_, Apr 11 2012",
			"references": 26,
			"revision": 56,
			"time": "2022-01-11T17:57:49-05:00",
			"created": "2012-04-11T16:26:59-04:00"
		}
	]
}