{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A065602",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 65602,
			"data": "1,1,1,3,2,1,8,6,3,1,24,18,10,4,1,75,57,33,15,5,1,243,186,111,54,21,6,1,808,622,379,193,82,28,7,1,2742,2120,1312,690,311,118,36,8,1,9458,7338,4596,2476,1164,474,163,45,9,1,33062,25724,16266,8928,4332,1856,692,218",
			"name": "Triangle T(n,k) giving number of hill-free Dyck paths of length 2n and having height of first peak equal to k.",
			"comment": [
				"A Riordan triangle.",
				"Subtriangle of triangle in A167772. [_Philippe Deléham_, Nov 14 2009]",
				"Riordan array (f(x), x*g(x)) where f(x) is the g.f. of A000958 and g(x) is the g.f. of A000108. [_Philippe Deléham_, Jan 23 2010]",
				"T(n,k) = A167772(n-1,k-1), k=2..n. - _Reinhard Zumkeller_, May 15 2014"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A065602/b065602.txt\"\u003eRows n=2..125 of triangle, flattened\u003c/a\u003e",
				"E. Deutsch and L. Shapiro, \u003ca href=\"https://doi.org/10.1016/S0012-365X(01)00121-2\"\u003eA survey of the Fine numbers\u003c/a\u003e, Discrete Math., 241 (2001), 241-265."
			],
			"formula": [
				"T(n, k)= sum((k-1+2j)*binomial(2n-k-1-2j, n-1)/(2n-k-1-2j), j=0..floor((n-k)/2)). G.f.=t^2*z^2*C/[(1-z^2*C^2)(1-tzC)], where C=(1-sqrt(1-4z))/(2z) is the Catalan function. - _Emeric Deutsch_, Feb 23 2004"
			],
			"example": [
				"Example: T(3,2)=1 reflecting the unique Dyck path (UUDUDD) of length 6, with no hills and height of first peak equal to 2.",
				"Triangle begins:",
				"  1;",
				"  1,1;",
				"  3,2,1;",
				"  8,6,3,1;",
				"  ..."
			],
			"maple": [
				"a := proc(n,k) if n=0 and k=0 then 1 elif k\u003c2 or k\u003en then 0 else sum((k-1+2*j)*binomial(2*n-k-1-2*j,n-1)/(2*n-k-1-2*j),j=0..floor((n-k)/2)) fi end: seq(seq(a(n,k),k=2..n),n=1..14);"
			],
			"mathematica": [
				"nmax = 12; t[n_, k_] := Sum[(k-1+2j)*Binomial[2n-k-1-2j, n-1] / (2n-k-1-2j), {j, 0, (n-k)/2}]; Flatten[ Table[t[n, k], {n, 2, nmax}, {k, 2, n}]] (* _Jean-François Alcover_, Nov 08 2011, after Maple *)"
			],
			"program": [
				"(Haskell)",
				"a065602 n k = sum",
				"   [(k-1+2*j) * a007318' (2*n-k-1-2*j) (n-1) `div` (2*n-k-1-2*j) |",
				"    j \u003c- [0 .. div (n-k) 2]]",
				"a065602_row n = map (a065602 n) [2..n]",
				"a065602_tabl = map a065602_row [2..]",
				"-- _Reinhard Zumkeller_, May 15 2014"
			],
			"xref": [
				"Row sums give A000957 (the Fine sequence). First column is A000958.",
				"Cf. A007318."
			],
			"keyword": "nonn,tabl,easy,nice",
			"offset": "2,4",
			"author": "_N. J. A. Sloane_, Dec 02 2001",
			"ext": [
				"More terms from _Emeric Deutsch_, Feb 23 2004"
			],
			"references": 3,
			"revision": 21,
			"time": "2019-04-06T09:29:48-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}