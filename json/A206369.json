{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A206369",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 206369,
			"data": "1,1,2,3,4,2,6,5,7,4,10,6,12,6,8,11,16,7,18,12,12,10,22,10,21,12,20,18,28,8,30,21,20,16,24,21,36,18,24,20,40,12,42,30,28,22,46,22,43,21,32,36,52,20,40,30,36,28,58,24,60,30,42,43,48,20,66,48,44,24,70,35",
			"name": "a(p^k) = p^k - p^(k-1) + p^(k-2) - ... +- 1, and then extend by multiplicativity.",
			"comment": [
				"For more information see the Comments in A061020.",
				"a(n) is the number of integers j such that 1 \u003c= j \u003c= n and gcd(n,j) is a perfect square. For example, a(12) = 6 because |{1,4,5,7,8,11}|=6 and the respective GCDs with 12 are 1,4,1,1,4,1, which are squares. - _Geoffrey Critzer_, Feb 16 2015",
				"If m is squarefree (A005117), then a(m) = A000010(m) where A000010 is the Euler totient function. - _Michel Marcus_, Nov 08 2017",
				"Also it appears that the primorials (A002110) is the sequence of indices of minimum records for a(n)/n, and these records are A038110(n)/A060753(n). - _Michel Marcus_, Nov 09 2017",
				"Also called rho(n). When rho(n) | n, then n is called k-imperfect, with k = n/rho(n), cf. A127724. - _M. F. Hasler_, Feb 13 2020"
			],
			"reference": [
				"P. J. McCarthy, Introduction to Arithmetical Functions, Springer Verlag, 1986, page 25."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A206369/b206369.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"László Tóth, \u003ca href=\"http://arxiv.org/abs/1111.4842\"\u003eA survey of the alternating sum-of-divisors function\u003c/a\u003e, arXiv:1111.4842 [math.NT], 2011-2014."
			],
			"formula": [
				"a(n) = abs(A061020(n)).",
				"a(n) = n*Sum_{d|n} lambda(d)/d, where lambda(n) is A008836(n). - _Enrique Pérez Herrero_, Sep 23 2012",
				"Dirichlet g.f.: zeta(s - 1)*zeta(2*s)/zeta(s). - _Geoffrey Critzer_, Feb 25 2015",
				"From _Michel Marcus_, Nov 05 2017: (Start)",
				"a(2^n) = A001045(n+1);",
				"a(3^n) = A015518(n+1);",
				"a(5^n) = A015531(n+1);",
				"a(7^n) = A015552(n+1);",
				"a(11^n) = A015592(n+1). (End)",
				"a(p^k) = p^k - a(p^(k - 1)) for k \u003e 0 and prime p. - _David A. Corneth_, Nov 09 2017",
				"a(n) = Sum_{d|n, d is a perfect square} phi(n/d), where phi(k) is the Euler totient function. - _Daniel Suteu_, Jun 27 2018",
				"a(p^k) = A071324(p^k), for k \u003e= 0 and prime p. - _Michel Marcus_, Aug 11 2018",
				"Sum_{k=1..n} a(k) ~ Pi^2 * n^2 / 30. - _Vaclav Kotesovec_, Feb 07 2019",
				"G.f.: Sum_{k\u003e=1} lambda(k)*x^k/(1 - x^k)^2. - _Ilya Gutkovskiy_, May 23 2019",
				"a(n) = Sum_{i=1..n} A010052(gcd(n,i)). - _Ridouane Oudra_, Nov 24 2019",
				"a(p^k) = round(p^(k+1)/(p+1)). - _M. F. Hasler_, Feb 13 2020"
			],
			"maple": [
				"a:= n-\u003e mul(add(i[1]^(i[2]-j)*(-1)^j, j=0..i[2]), i=ifactors(n)[2]):",
				"seq(a(n), n=1..100);  # _Alois P. Heinz_, Nov 03 2017"
			],
			"mathematica": [
				"Table[Length[Select[Range[n], IntegerQ[GCD[n, #]^(1/2)] \u0026]], {n, 72}] (* _Geoffrey Critzer_, Feb 16 2015 *)",
				"a[n_] := n*DivisorSum[n, LiouvilleLambda[#]/#\u0026]; Array[a, 72] (* _Jean-François Alcover_, Dec 04 2017, after _Enrique Pérez Herrero_ *)",
				"f[p_,e_] := Sum[(-1)^(e-k)*p^k, {k,0,e}]; a[1] = 1; a[n_] := Times @@ (f @@@ FactorInteger[n]); Array[a, 100] (* _Amiram Eldar_, Jan 01 2020 *)"
			],
			"program": [
				"(Haskell)",
				"a206369 n = product $",
				"   zipWith h (a027748_row n) (map toInteger $ a124010_row n) where",
				"           h p e = sum $ take (fromInteger e + 1) $",
				"                         iterate ((* p) . negate) (1 - 2 * (e `mod` 2))",
				"-- _Reinhard Zumkeller_, Feb 08 2012",
				"(PARI) a(n) = sum(k=1, n, issquare(gcd(n, k)));",
				"(PARI) ak(p,e)=my(s=1); for(i=1,e, s=s*p + (-1)^i); s",
				"a(n)=my(f=factor(n)); prod(i=1,#f~, ak(f[i,1],f[i,2])) \\\\ _Charles R Greathouse IV_, Dec 27 2016",
				"(PARI) a(n) = sumdiv(n, d, eulerphi(n/d) * issquare(d)); \\\\ _Daniel Suteu_, Jun 27 2018",
				"(PARI) apply( {A206369(n)=vecprod([f[1]^(f[2]+1)\\/(f[1]+1)|f\u003c-factor(n)~])}, [1..99]) \\\\ _M. F. Hasler_, Feb 13 2020"
			],
			"xref": [
				"Cf. A061020, A206368.",
				"Cf. A027748 row, A124010, A206475 (first differences).",
				"Cf. A078429.",
				"Cf. A127724 (k-imperfect), A127725 (2-imperfect), A127726 (3-imperfect)."
			],
			"keyword": "nonn,mult",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_, Feb 06 2012",
			"references": 37,
			"revision": 93,
			"time": "2020-03-01T12:14:46-05:00",
			"created": "2012-02-06T23:24:15-05:00"
		}
	]
}