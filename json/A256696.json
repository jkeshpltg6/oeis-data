{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A256696",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 256696,
			"data": "0,1,2,4,-1,4,8,-4,1,8,-2,8,-1,8,16,-8,1,16,-8,2,16,-8,4,-1,16,-4,16,-4,1,16,-2,16,-1,16,32,-16,1,32,-16,2,32,-16,4,-1,32,-16,4,32,-16,8,-4,1,32,-16,8,-2,32,-16,8,-1,32,-8,32,-8,1,32,-8,2,32",
			"name": "R(k), the minimal alternating binary representation of k, concatenated for k = 0, 1, 2,....",
			"comment": [
				"Suppose that b = (b(0), b(1), ... ) is an increasing sequence of positive integers satisfying b(0) = 1 and b(n+1) \u003c= 2*b(n) for n \u003e= 0.  Let B(n) be the least b(m) \u003e= n.  Let R(0) = 1, and for n \u003e 0, let R(n) = B(n) - R(B(n) - n).  The resulting sum of the form R(n) = B(n) - B(m(1)) + B(m(2)) - ... + ((-1)^k)*B(k) is the minimal alternating b-representation of n.  The sum B(n) + B(m(2)) + ... is the positive part of R(n), and the sum B(m(1)) + B(m(3)) + ... , the nonpositive part of R(n).  The number ((-1)^k)*B(k) is the trace of n.",
				"If b(n) = 2^n, the sum R(n) is the minimal alternating binary representation of n.",
				"A055975 = trace of n, for n \u003e= 1.",
				"A091072 gives the numbers having positive trace.",
				"A091067 gives the numbers having negative trace.",
				"A072339 = number of terms in R(n).",
				"A073122 = sum of absolute values of the terms in R(n)."
			],
			"reference": [
				"D. E. Knuth, The Art of Computer Programming. Addison-Wesley, Reading, MA, 1981, Vol. 2 (2nd ed.), p. 196, Exercise 27."
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A256696/b256696.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e"
			],
			"example": [
				"R(0) = 0",
				"R(1) = 1",
				"R(2) = 2",
				"R(3) = 4 - 1",
				"R(4) = 4",
				"R(9) = 8 - 4 + 1",
				"R(11) = 16 - 8 + 4 - 1"
			],
			"mathematica": [
				"z = 100; b[n_] := 2^n; bb = Table[b[n], {n, 0, 40}];",
				"s[n_] := Table[b[n + 1], {k, 1, b[n]}];",
				"h[0] = {1}; h[n_] := Join[h[n - 1], s[n - 1]];",
				"g = h[10]; r[0] = {0};",
				"r[n_] := If[MemberQ[bb, n], {n}, Join[{g[[n]]}, -r[g[[n]] - n]]]",
				"u = Flatten[Table[r[n], {n, 0, z}]]"
			],
			"xref": [
				"Cf. A000079, A256655 (Fibonacci based), A055975, A091072, A091067, A072339, A073122, A256701, A256702."
			],
			"keyword": "easy,sign,base",
			"offset": "0,3",
			"author": "_Clark Kimberling_, Apr 09 2015",
			"references": 7,
			"revision": 13,
			"time": "2021-12-26T05:29:48-05:00",
			"created": "2015-04-13T09:39:20-04:00"
		}
	]
}