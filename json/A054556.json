{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A054556",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 54556,
			"data": "1,4,15,34,61,96,139,190,249,316,391,474,565,664,771,886,1009,1140,1279,1426,1581,1744,1915,2094,2281,2476,2679,2890,3109,3336,3571,3814,4065,4324,4591,4866,5149,5440,5739,6046,6361,6684,7015,7354,7701,8056",
			"name": "a(n) = 4*n^2 - 9*n + 6.",
			"comment": [
				"Move in 1-4 direction in a spiral organized like A068225 etc.",
				"Equals binomial transform of [1, 3, 8, 0, 0, 0, ...]. - _Gary W. Adamson_, Apr 30 2008",
				"Ulam's spiral (N spoke). - _Robert G. Wilson v_, Oct 31 2011",
				"Also, numbers of the form m*(4*m+1)+1 for nonpositive m. - _Bruno Berselli_, Jan 06 2016"
			],
			"link": [
				"Ivan Panchenko, \u003ca href=\"/A054556/b054556.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Franck Ramaharo, \u003ca href=\"https://arxiv.org/abs/1802.07701\"\u003eStatistics on some classes of knot shadows\u003c/a\u003e, arXiv:1802.07701 [math.CO], 2018.",
				"Robert G. Wilson v, \u003ca href=\"/A244677/a244677.jpg\"\u003eCover of the March 1964 issue of Scientific American\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-3,1)."
			],
			"formula": [
				"a(n)^2 = Sum_{i = 0..2*(4*n-5)} (4*n^2-13*n+9+i)^2*(-1)^i = ((n-1)*(4*n-5)+1)^2. - _Bruno Berselli_, Apr 29 2010",
				"a(0)=1, a(1)=4, a(2)=15; for n \u003e 2, a(n) = 3*a(n-1) - 3*a(n-2) + a(n-3). - _Harvey P. Dale_, Aug 21 2011",
				"G.f.: -(6*x^2+x+1)/(x-1)^3. - _Harvey P. Dale_, Aug 21 2011",
				"From _Franck Maminirina Ramaharo_, Mar 09 2018: (Start)",
				"a(n) = binomial(2*n - 2, 2) + 2*(n - 1)^2 + 1.",
				"a(n) = A000384(n-1) + A058331(n-1).",
				"a(n) = A130883(n-1) + A001105(n-1). (End)"
			],
			"maple": [
				"a:=n-\u003e4*n^2-9*n+6: seq(a(n),n=0..50); # _Muniru A Asiru_, Mar 09 2018"
			],
			"mathematica": [
				"a[n_] := 4*n^2 - 9*n + 6; Array[a, 40] (* _Vladimir Joseph Stephan Orlovsky_, Sep 01 2008 *)",
				"LinearRecurrence[{3,-3,1},{1,4,15},50] (* _Harvey P. Dale_, Sep 06 2015 *)",
				"CoefficientList[Series[-(6x^2 + x + 1)/(x - 1)^3, {x, 0, 49}], x] (* _Robert G. Wilson v_, Mar 12 2018 *)"
			],
			"program": [
				"(PARI) a(n)=4*n^2-9*n+6 \\\\ _Charles R Greathouse IV_, Sep 24 2015",
				"(MAGMA) [4*n^2-9*n+6 : n in [1..50]]; // _Vincenzo Librandi_, Mar 10 2018"
			],
			"xref": [
				"Cf. A054555, A068225, A054552, A054554, A054567, A054569, A033951.",
				"Cf. A266883: m*(4*m+1)+1 for m = 0,-1,1,-2,2,-3,3,...",
				"Sequences on the four axes of the square spiral: Starting at 0: A001107, A033991, A007742, A033954; starting at 1: A054552, A054556, A054567, A033951.",
				"Sequences on the four diagonals of the square spiral: Starting at 0: A002939 = 2*A000384, A016742 = 4*A000290, A002943 = 2*A014105, A033996 = 8*A000217; starting at 1: A054554, A053755, A054569, A016754.",
				"Sequences obtained by reading alternate terms on the X and Y axes and the two main diagonals of the square spiral: Starting at 0: A035608, A156859, A002378 = 2*A000217, A137932 = 4*A002620; starting at 1: A317186, A267682, A002061, A080335."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Enoch Haga_, _G. L. Honaker, Jr._, Apr 10 2000",
			"ext": [
				"Edited by _Frank Ellermann_, Feb 24 2002",
				"Incorrect formula deleted by _N. J. A. Sloane_, Aug 02 2009"
			],
			"references": 36,
			"revision": 69,
			"time": "2018-07-27T17:43:41-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}