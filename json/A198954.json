{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A198954",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 198954,
			"data": "1,3,0,5,0,0,7,0,0,0,9,0,0,0,0,11,0,0,0,0,0,13,0,0,0,0,0,0,15,0,0,0,0,0,0,0,17,0,0,0,0,0,0,0,0,19,0,0,0,0,0,0,0,0,0,21,0,0,0,0,0,0,0,0,0,0,23,0,0,0,0,0,0,0,0,0,0,0,25,0,0,0,0,0,0,0,0,0,0,0,0,27,0,0,0,0,0,0,0",
			"name": "Expansion of the rotational partition function for a heteronuclear diatomic molecule.",
			"comment": [
				"The partition function of a heteronuclear diatomic molecule is Sum_{J\u003e=0} (2*J + 1) * exp( - J * (J + 1) * hbar^2 / (2 * I * k * T)) where I is the moment of inertia, hbar is reduced Planck's constant, k is Boltzmann's constant, and T is temperature. The degeneracy for the J-th energy level is 2*J + 1.",
				"As triangle: triangle T(n,k), read by rows, given by (3,-4/3,1/3,0,0,0,0,0,0,0,...) DELTA (0,0,0,0,0,0,0,0,0,0,...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, Nov 01 2011",
				"Note that the g.f. theta_1'(0, q^(1/2)) / (2 * q^(1/8)) = 1 - 3*q  + 5*q^3 - 7*q^6 + 9*q^10 + ... which is the same as this sequence except the signs alternate. - _Michael Somos_, Aug 26 2015"
			],
			"reference": [
				"G. H. Wannier, Statistical Physics, Dover Publications, 1987, see p. 215 equ. (11.13)."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A198954/b198954.txt\"\u003eTable of n, a(n) for n = 0..10011\u003c/a\u003e"
			],
			"formula": [
				"G.f.: Sum_{k\u003e=0} (2*k + 1) * x^( (k^2 + k) / 2). This is related to Jacobi theta functions.",
				"a(n) = (t*(t+1)-2*n-1)*(t-r), where t = floor(sqrt(2*(n+1))+1/2) and r = floor(sqrt(2*n)+1/2). - _Mikael Aaltonen_, Jan 15 2015",
				"a(n) = A053187(2n+1) - A053187(2n). - _Robert Israel_, Jan 15 2015",
				"a(n) = abs(A010816(n)). - _Joerg Arndt_, Jan 16 2015"
			],
			"example": [
				"G.f. = 1 + 3*x + 5*x^3 + 7*x^6 + 9*x^10 + 11*x^15 + 13*x^21 + 15*x^28 + ...",
				"G.f. = 1 + 3*q^2 + 5*q^6 + 7*q^12 + 9*q^20 + 11*q^30 + 13*q^42 + 15*q^56 + ...",
				"Triangle begins:",
				"   1;",
				"   3, 0;",
				"   5, 0, 0;",
				"   7, 0, 0, 0;",
				"   9, 0, 0, 0, 0;",
				"  11, 0, 0, 0, 0, 0;",
				"  13, 0, 0, 0, 0, 0, 0;",
				"  15, 0, 0, 0, 0, 0, 0, 0;",
				"  17, 0, 0, 0, 0, 0, 0, 0, 0;"
			],
			"maple": [
				"seq(op([2*i+1,0$i]), i=0..10); # _Robert Israel_, Jan 15 2015"
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 0, 0, With[ {m = Sqrt[8 n + 1]}, If[ IntegerQ[m], m KroneckerSymbol[ 4, m], 0]]]; (* _Michael Somos_, Aug 26 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = my(m); if( issquare( 8*n + 1, \u0026m), m, 0)};"
			],
			"xref": [
				"Cf. A053187, A107270."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "0,2",
			"author": "_Michael Somos_, Oct 31 2011",
			"references": 3,
			"revision": 44,
			"time": "2020-01-22T20:12:14-05:00",
			"created": "2011-11-01T11:37:56-04:00"
		}
	]
}