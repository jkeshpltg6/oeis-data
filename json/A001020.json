{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001020",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1020,
			"id": "M4807 N2054",
			"data": "1,11,121,1331,14641,161051,1771561,19487171,214358881,2357947691,25937424601,285311670611,3138428376721,34522712143931,379749833583241,4177248169415651,45949729863572161,505447028499293771,5559917313492231481,61159090448414546291",
			"name": "Powers of 11: a(n) = 11^n.",
			"comment": [
				"The compositions of n in which each natural number is colored by one of p different colors are called p-colored compositions of n. For n \u003e= 1, a(n) equals the number of 11-colored compositions of n such that no adjacent parts have the same color. - _Milan Janjic_, Nov 17 2011",
				"a(n), for n \u003c= 4, gives the n-th row of Pascal's triangle (A007318); a(n), n \u003e= 5 \"sort of\" gives the n-th row of Pascal's triangle, but now the binomial coefficients with more than one digit overlap. - _Daniel Forgues_, Aug 12 2012",
				"Numbers n such that sigma(11*n) = 11*n + sigma(n). - _Jahangeer Kholdi_, Nov 13 2013"
			],
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A001020/b001020.txt\"\u003eTable of n, a(n) for n = 0..100\u003c/a\u003e",
				"P. J. Cameron, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL3/groups.html\"\u003eSequences realized by oligomorphic permutation groups\u003c/a\u003e, J. Integ. Seqs. Vol. 3 (2000), #00.1.5.",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=275\"\u003eEncyclopedia of Combinatorial Structures 275\u003c/a\u003e",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"Y. Puri and T. Ward, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL4/WARD/short.html\"\u003eArithmetic and growth of periodic orbits\u003c/a\u003e, J. Integer Seqs., Vol. 4 (2001), #01.2.1.",
				"\u003ca href=\"/index/Rec#order_01\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (11)."
			],
			"formula": [
				"G.f.: 1/(1-11*x).",
				"E.g.f.: exp(11*x).",
				"a(n) = 11*a(n-1), n \u003e 0; a(0)=1. - _Philippe Deléham_, Nov 23 2008"
			],
			"maple": [
				"A001020:=-1/(-1+11*z); # _Simon Plouffe_ in his 1992 dissertation"
			],
			"mathematica": [
				"Table[11^n,{n,0,40}] (* _Vladimir Joseph Stephan Orlovsky_, Feb 15 2011 *)"
			],
			"program": [
				"(MAGMA) [11^n: n in [0..100]]; // _Vincenzo Librandi_, Apr 24 2011",
				"(Maxima) makelist(11*n,n,0,20); /* _Martin Ettl_, Dec 17 2012 */",
				"(PARI) a(n)=n^11 \\\\ _Charles R Greathouse IV_, Sep 24 2015"
			],
			"xref": [
				"Cf. A096884, A097659, A007318."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 77,
			"revision": 96,
			"time": "2021-03-12T22:32:34-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}