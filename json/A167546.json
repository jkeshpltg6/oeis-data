{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A167546",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 167546,
			"data": "1,1,1,2,4,1,6,12,7,1,24,48,32,10,1,120,240,160,62,13,1,720,1440,960,384,102,16,1,5040,10080,6720,2688,762,152,19,1,40320,80640,53760,21504,6144,1336,212,22,1",
			"name": "The ED1 array read by antidiagonals",
			"comment": [
				"The coefficients in the upper right triangle of the ED1 array (m \u003e n) were found with the a(n,m) formula while the coefficients in the lower left triangle of the ED1 array (m \u003c= n) were found with the recurrence relation, see below. We use for the array rows the letter n (\u003e= 1) and for the array columns the letter m (\u003e= 1).",
				"Our procedure for finding the coefficients in the lower left triangle can be compared with the procedure that De Smit and Lenstra used to fill in the hole in the middle of 'The Print Gallery' by M. C. Escher, see the links. In this lithograph Escher made use of the so-called Droste effect, hence we propose to call this square array of numbers the ED1 array.",
				"For the ED2, ED3 and ED4 arrays see A167560, A167572 and A167584."
			],
			"link": [
				"B. de Smit and H.W. Lenstra, \u003ca href=\"http://escherdroste.math.leidenuniv.nl/\"\u003eThe Mathematical Structure of Escher's Print Gallery\u003c/a\u003e, Notices of the AMS, Volume 50, Number 4, pp. 446-457, April 2003.",
				"Johannes W. Meijer, The four Escher-Droste arrays, \u003ca href=\"/A167546/a167546.jpg\"\u003ejpg image\u003c/a\u003e, Mar 08 2013.",
				"A. Ryabov, P. Chvosta, \u003ca href=\"http://arxiv.org/abs/1402.1949\"\u003eTracer dynamics in a single-file system with absorbing boundary\u003c/a\u003e, arXiv preprint arXiv:1402.1949 [cond-mat.stat-mech], 2014."
			],
			"formula": [
				"a(n,m) = (2*(m-1)!/(m-n-1)!)*Integral_{y\u003e=0} sinh(y*(2*n-1))/cosh(y)^(2*m-1) for m \u003e n.",
				"The (n-1)-differences of the n-th array row lead to the recurrence relation",
				"Sum_{k=0..n-1} (-1)^k*binomial(n-1,k)*a(n,m-k) = (2*n-1)*(n-1)!",
				"which in its turn leads to, see also A167557,",
				"a(n,m) = 4^(m-1)*(m-1)!*(n+m-2)!/(2*m-2)! for m \u003c= n."
			],
			"example": [
				"The ED1 array begins with:",
				"1, 1, 1, 1, 1, 1, 1, 1, 1, 1",
				"1, 4, 7, 10, 13, 16, 19, 22, 25, 28",
				"2, 12, 32, 62, 102, 152, 212, 282, 362, 452",
				"6, 48, 160, 384, 762, 1336, 2148, 3240, 4654, 6432",
				"24, 240, 960, 2688, 6144, 12264, 22200, 37320, 59208, 89664",
				"120, 1440, 6720, 21504, 55296, 122880, 245640, 452880, 783144, 1285536"
			],
			"maple": [
				"nmax:=10; mmax:=10; for n from 1 to nmax do for m from 1 to n do a(n,m) := 4^(m-1)*(m-1)!*(n-1+m-1)!/(2*m-2)! od; for m from n+1 to mmax do a(n,m):= (2*n-1)*(n-1)! + sum((-1)^(k-1)*binomial(n-1,k)*a(n,m-k),k=1..n-1) od; od: for n from 1 to nmax do for m from 1 to n do d(n,m):=a(n-m+1,m) od: od: T:=1: for n from 1 to nmax do for m from 1 to n do a(T):= d(n,m): T:=T+1: od: od: seq(a(n),n=1..T-1);"
			],
			"mathematica": [
				"nmax = 10; mmax = 10; For[n = 1, n \u003c= nmax, n++, For[m = 1, m \u003c= n, m++, a[n, m] = 4^(m - 1)*(m - 1)!*((n - 1 + m - 1)!/(2*m - 2)!)]; For[m = n + 1, m \u003c= mmax, m++, a[n, m] = (2*n - 1)*(n - 1)! + Sum[(-1)^(k - 1)*Binomial[n - 1, k]*a[n, m - k], {k, 1, n - 1}]]; ]; For[n = 1, n \u003c= nmax, n++, For[m = 1, m \u003c= n, m++, d[n, m] = a[n - m + 1, m]]; ]; t = 1; For[n = 1, n \u003c= nmax, n++, For[m = 1, m \u003c= n, m++, a[t] = d[n, m]; t = t + 1]]; Table[a[n], {n, 1, t - 1}] (* _Jean-François Alcover_, Dec 20 2011, translated from Maple *)"
			],
			"xref": [
				"A000012, A016777, 2*A005891, A167547, A167548 and A167549 equal the first sixth rows of the array.",
				"A000142 equals the first column of the array.",
				"A167550 equals the a(n, n+1) diagonal of the array.",
				"A047053 equals the a(n, n) diagonal of the array.",
				"A167558 equals the a(n+1, n) diagonal of the array.",
				"A167551 equals the row sums of the ED1 array read by antidiagonals.",
				"A167552 is a triangle related to the a(n) formulas of rows of the ED1 array.",
				"A167556 is a triangle related to the GF(z) formulas of the rows of the ED1 array.",
				"A167557 is the lower left triangle of the ED1 array.",
				"Cf. A068424 (the (m-1)!/(m-n-1)! factor), A007680 (the (2*n-1)*(n-1)! factor).",
				"Cf. A167560 (ED2 array), A167572 (ED3 array), A167584 (ED4 array)."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "1,4",
			"author": "_Johannes W. Meijer_, Nov 10 2009",
			"references": 17,
			"revision": 22,
			"time": "2019-06-13T03:36:21-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}