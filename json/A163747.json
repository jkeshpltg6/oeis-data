{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A163747",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 163747,
			"data": "0,-1,-1,2,5,-16,-61,272,1385,-7936,-50521,353792,2702765,-22368256,-199360981,1903757312,19391512145,-209865342976,-2404879675441,29088885112832,370371188237525,-4951498053124096,-69348874393137901",
			"name": "Expansion of e.g.f. 2*exp(x)*(1-exp(x))/(1+exp(2*x)).",
			"comment": [
				"The real part of the exponential expansion of 2*((1+i)/(1+i*exp(z))-1) = (-1-i)*z + (-1/2+i/2)*z^2 + (1/3+i/3)*z^3 + (5/24-5i/24)*z^4 + (-2/15-2i/15)*z^5 + ...  where i is the imaginary unit.",
				"From _Paul Curtz_, Mar 12 2013: (Start)",
				"a(n) is an autosequence of the first kind; a(n) and successive differences are:",
				"0,   -1,   -1,      2,      5,    -16,    -61;",
				"-1,   0,    3,      3,    -21,    -45,    333;",
				"1,    3,    0,    -24,    -24,    378,    780;",
				"2,   -3,  -24,      0,    402,    402, -11214;",
				"-5, -21,   24,    402,      0, -11616, -11616;",
				"-16, 45,  378,   -402, -11616,      0, 514608;",
				"61, 333, -780, -11214,  11616, 514608,      0;",
				"The main diagonal is A000004. The inverse binomial transform is the signed sequence.",
				"The first two upper diagonals are A002832 (median Euler numbers) signed.",
				"Sum of the antidiagonals: 0, -2, 0, 10, 0, ... = 2*A122045(n+1) (End)"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A163747/b163747.txt\"\u003eTable of n, a(n) for n = 0..485\u003c/a\u003e",
				"Toufik Mansour, Howard Skogman, and Rebecca Smith, \u003ca href=\"https://arxiv.org/abs/1808.04199\"\u003ePassing through a stack k times with reversals\u003c/a\u003e, arXiv:1808.04199 [math.CO], 2018.",
				"A. Randrianarivony and J. Zeng, \u003ca href=\"http://dx.doi.org/10.1006/aama.1996.0001\"\u003eUne famille de polynomes qui interpole plusieurs suites classiques de nombres\u003c/a\u003e, Adv. Appl. Math. 17 (1996), 1-26. See Section 6 (negative of the zeroth column of matrix a_{n,k} on p. 18)."
			],
			"formula": [
				"G.f.: -x/W(0), where W(k) = 1 - x + (4*k+3)*(k+1)*x^2 / (1 + (4*k+5)*(k+1)*x^2 / W(k+1) ); (continued fraction). - _Sergei N. Gladkovskii_, Jan 22 2015",
				"a(n) ~ n! * (cos(Pi*n/2) - sin(Pi*n/2)) * 2^(n+2) / Pi^(n+1). - _Vaclav Kotesovec_, Apr 23 2015",
				"a(n) = (A122045(n) - 2^n(2*Euler(n,1) + Euler(n,3/2)))/2 + 1, where Euler(n,x) is the n-th Euler polynomial. - _Benedict W. J. Irwin_, May 24 2016",
				"a(n) = 2*4^n*(HurwitzZeta(-n, 1/4) - HurwitzZeta(-n, 3/4)) + HurwitzZeta(-n, 1)*(4^(n+1) - 2^(n+1)). - _Peter Luschny_, Jul 21 2020",
				"a(n) = 2^n*(Euler(n, 1/2) - Euler(n, 1)). - _Peter Luschny_, Mar 19 2021",
				"a(n) = ((-2)^(n + 1)*(1 - 2^(n + 1))*Bernoulli(n + 1))/(n + 1) + Euler(n). - _Peter Luschny_, May 06 2021",
				"a(n) = n!*Re([x^n]((2 - 2*i)/(i + exp(-x)))). - _Peter Luschny_, Aug 09 2021"
			],
			"maple": [
				"A163747 := proc(n) exp(t)*(1-exp(t))/(1+exp(2*t)) ; coeftayl(%,t=0,n) ; 2*%*n! ; end proc: # _R. J. Mathar_, Sep 11 2011",
				"seq((euler(n) - 2^n*(2*euler(n,1)+euler(n,3/2)))/2 + 1, n=0..30); # _Robert Israel_, May 24 2016",
				"egf := (2 - 2*I)/(exp(-x) + I); ser := series(egf, x, 24):",
				"seq(n!*Re(coeff(ser, x, n)), n = 0..22); # _Peter Luschny_, Aug 09 2021"
			],
			"mathematica": [
				"f[t_] = (1 + I)/(1 + I*Exp[t]) - 1;",
				"Table[Re[2*n!*SeriesCoefficient[Series[f[t], {t, 0, 30}], n]], {n, 0, 30}]",
				"max = 20; Clear[g]; g[max + 2] = 1; g[k_] := g[k] = 1 - x + (4*k+3)*(k+1)*x^2 /( 1 + (4*k+5)*(k+1)*x^2 / g[k+1]); gf = -x/g[0]; CoefficientList[Series[gf, {x, 0, max}], x] (* _Vaclav Kotesovec_, Jan 22 2015, after _Sergei N. Gladkovskii_ *)",
				"Table[(EulerE[n] - 2^n (2 EulerE[n, 1] + EulerE[n, 3/2]))/2 + 1, {n, 0, 20}] (* _Benedict W. J. Irwin_, May 24 2016 *)"
			],
			"xref": [
				"Variant: A163982.",
				"Cf. A000004, A000111, A002832, A122045.",
				"Minus the zeroth column of A323833."
			],
			"keyword": "sign",
			"offset": "0,4",
			"author": "_Roger L. Bagula_, Aug 03 2009",
			"references": 9,
			"revision": 67,
			"time": "2021-08-09T15:43:56-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}