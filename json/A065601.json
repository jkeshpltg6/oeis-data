{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A065601",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 65601,
			"data": "0,1,0,2,4,13,40,130,432,1466,5056,17672,62460,222853,801592,2903626,10582816,38781310,142805056,528134764,1960825672,7305767602,27307800400,102371942932,384806950624,1450038737668,5476570993440,20727983587220,78606637060012",
			"name": "Number of Dyck paths of length 2n with exactly 1 hill.",
			"comment": [
				"Convolution of A000957(n) with itself gives a(n-1)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A065601/b065601.txt\"\u003eTable of n, a(n) for n = 0..500\u003c/a\u003e",
				"Naiomi Cameron, J. E. McLeod, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL19/McLeod/mcleod3.html\"\u003eReturns and Hills on Generalized Dyck Paths\u003c/a\u003e, Journal of Integer Sequences, Vol. 19, 2016, #16.6.1.",
				"E. Deutsch, \u003ca href=\"http://dx.doi.org/10.1016/S0012-365X(98)00371-9\"\u003eDyck path enumeration\u003c/a\u003e, Discrete Math., 204, 1999, 167-202.",
				"E. Deutsch and L. Shapiro, \u003ca href=\"http://dx.doi.org/10.1016/S0012-365X(01)00121-2\"\u003eA survey of the Fine numbers\u003c/a\u003e, Discrete Math., 241 (2001), 241-265.",
				"S. Kitaev, J. Remmel and M. Tiefenbruck, \u003ca href=\"http://arxiv.org/abs/1201.6243\"\u003eMarked mesh patterns in 132-avoiding permutations I\u003c/a\u003e, arXiv preprint arXiv:1201.6243 [math.CO], 2012. - From _N. J. A. Sloane_, May 09 2012",
				"Sergey Kitaev, Jeffrey Remmel, Mark Tiefenbruck, \u003ca href=\"http://www.emis.de/journals/INTEGERS/papers/p16/p16.Abstract.html\"\u003eQuadrant Marked Mesh Patterns in 132-Avoiding Permutations II\u003c/a\u003e, Electronic Journal of Combinatorial Number Theory, Volume 15 #A16. (\u003ca href=\"http://arxiv.org/abs/1302.2274\"\u003earXiv:1302.2274\u003c/a\u003e)"
			],
			"formula": [
				"Reference gives g.f.'s.",
				"Conjecture: 2*(n+1)*a(n) +(-3*n+2)*a(n-1) +2*(-9*n+19)*a(n-2) +4*(-2*n+3)*a(n-3)=0. - _R. J. Mathar_, Dec 10 2013",
				"a(n) ~ 2^(2*n+3) / (27 * sqrt(Pi) * n^(3/2)). - _Vaclav Kotesovec_, Feb 12 2014"
			],
			"maple": [
				"b:= proc(x, y, h, z) option remember;",
				"     `if`(x\u003c0 or y\u003cx, 0, `if`(x=0 and y=0, `if`(h, 0, 1),",
				"      b(x-1, y, h, is(x=y))+ `if`(h and z, b(x, y-1, false$2),",
				"     `if`(z, 0, b(x, y-1, h, false)))))",
				"    end:",
				"a:= n-\u003e b(n$2, true$2):",
				"seq(a(n), n=0..30);  # _Alois P. Heinz_, May 10 2012",
				"# second Maple program:",
				"series(((1-sqrt(1-4*x))/(3-sqrt(1-4*x)))^2/x, x=0, 30);  # _Mark van Hoeij_, Apr 18 2013"
			],
			"mathematica": [
				"CoefficientList[Series[((1-Sqrt[1-4*x])/(3-Sqrt[1-4*x]))^2/x, {x, 0, 20}], x] (* _Vaclav Kotesovec_, Feb 12 2014 *)",
				"Table[Sum[(-1)^j*(j+1)*(j+2)*Binomial[2*n-1-j,n],{j,0,n-1}]/(n+1),{n,0,30}] (* _Vaclav Kotesovec_, May 18 2015 *)"
			],
			"xref": [
				"2nd column of A065600. Cf. A000957."
			],
			"keyword": "nonn,easy",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_, Dec 02 2001",
			"ext": [
				"More terms from _Emeric Deutsch_, Dec 03 2001"
			],
			"references": 4,
			"revision": 32,
			"time": "2017-06-28T07:51:08-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}