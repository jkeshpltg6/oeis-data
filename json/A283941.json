{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A283941",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 283941,
			"data": "1,4,2,9,6,3,16,12,8,5,25,20,15,11,7,37,30,24,19,14,10,51,43,35,29,23,18,13,67,58,49,41,34,28,22,17,85,75,65,56,47,40,33,27,21,106,94,83,73,63,54,46,39,32,26,129,116,103,92,81,71,61,53,45,38,31",
			"name": "Interspersion of the signature sequence of sqrt(5).",
			"comment": [
				"Row n is the ordered sequence of numbers k such that A023117(k)=n.  As a sequence, A283941 is a permutation of the positive integers.  This is a transposable interspersion; i.e., every row intersperses all other rows, and every column intersperses all other columns."
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A283941/b283941.txt\"\u003eAntidiagonals n = 1..60, flattened\u003c/a\u003e",
				"Clark Kimberling and John E. Brown, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL7/Kimberling/kimber67.html\"\u003ePartial Complements and Transposable Dispersions\u003c/a\u003e, J. Integer Seqs., Vol. 7, 2004."
			],
			"example": [
				"Northwest corner:",
				"1   4   9   16  25  37  51  67",
				"2   6   12  20  30  43  58  76",
				"3   8   15  24  35  49  65  83",
				"5   11  19  29  41  56  73  92",
				"7   14  23  34  47  63  81  101",
				"10  18  28  40  54  71  90  111"
			],
			"mathematica": [
				"r = Sqrt[5]; z = 100;",
				"s[0] = 1; s[n_] := s[n] = s[n - 1] + 1 + Floor[n*r];",
				"u = Table[n + 1 + Sum[Floor[(n - k)/r], {k, 0, n}], {n, 0, z}] (* A022780 , col 1 of A283941 *)",
				"v = Table[s[n], {n, 0, z}] (* A022779, row 1 of A283941*)",
				"w[i_, j_] := u[[i]] + v[[j]] + (i - 1)*(j - 1) - 1;",
				"Grid[Table[w[i, j], {i, 1, 10}, {j, 1, 10}]] (* A283941, array *)",
				"Flatten[Table[w[k, n - k + 1], {n, 1, 20}, {k, 1, n}]] (* A283941, sequence *)"
			],
			"program": [
				"(PARI)",
				"r = sqrt(5);",
				"z = 100;",
				"s(n) = if(n\u003c1, 1, s(n - 1) + 1 + floor(n*r));",
				"p(n) = n + 1 + sum(k=0, n, floor((n - k)/r));",
				"u = v = vector(z + 1);",
				"for(n=1, 101, (v[n] = s(n - 1)));",
				"for(n=1, 101, (u[n] = p(n - 1)));",
				"w(i, j) = u[i] + v[j] + (i - 1) * (j - 1) - 1;",
				"tabl(nn) = {for(n=1, nn, for(k=1, n, print1(w(k, n - k + 1), \", \"); );",
				"print(); ); };",
				"tabl(20) \\\\ _Indranil Ghosh_, Mar 21 2017",
				"(Python)",
				"from sympy import sqrt",
				"import math",
				"def s(n): return 1 if n\u003c1 else s(n - 1) + 1 + int(math.floor(n*sqrt(5)))",
				"def p(n): return n + 1 + sum([int(math.floor((n - k)/sqrt(5))) for k in range(0, n+1)])",
				"v=[s(n) for n in range(0, 101)]",
				"u=[p(n) for n in range(0, 101)]",
				"def w(i,j): return u[i - 1] + v[j - 1] + (i - 1) * (j - 1) - 1",
				"for n in range(1, 11):",
				"....print [w(k, n - k + 1) for k in range(1, n + 1)] # _Indranil Ghosh_, Mar 21 2017"
			],
			"xref": [
				"Cf. A002163, A023117, A022797, A022780."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Mar 19 2017",
			"ext": [
				"Edited by _Clark Kimberling_, Feb 27 2018"
			],
			"references": 1,
			"revision": 22,
			"time": "2019-12-07T12:18:28-05:00",
			"created": "2017-03-19T19:29:23-04:00"
		}
	]
}