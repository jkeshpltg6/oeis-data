{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A166107",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 166107,
			"data": "2,-10,46,-334,982,-10942,140986,-425730,7201374,-137366646,410787198,-9473047614,236302407090,-710245778490,20563663645710,-638377099140510,1912749274005030,-67020067316087550,2477305680740159850",
			"name": "A sequence related to the Madhava-Gregory-Leibniz formula for Pi.",
			"comment": [
				"The EG1 matrix is defined in A162005. The first column of this matrix leads to the function PLS(z) = sum(2*eta(2*m-1)*z^(2*m-2), m=1..infinity) = 2*log(2) - Psi(z) - Psi(-z) + Psi(z/2) + Psi(-z/2). The values of this function for z=n+1/2 are related to Pi in a curious way.",
				"Gauss's digamma theorem leads to PLS(z=n+1/2) = (-1)^n*4*sum((-1)^(k+1)/(2*k-1), k=1..n) + 2/(2*n+1). Now we define PLS(z=n+1/2) = a(n)/p(n) with a(n) the sequence given above and for p(n) we choose the esthetically nice p(n) = (2*n-1)!!/(floor((n-2)/3)*2+1)!!, n=\u003e0. For even values of n the limit(a(2*n)/p(2*n), n=infinity) = Pi and for odd values of n the limit(a(2*n+1)/p(2*n+1), n=infinity) = - Pi. We observe that the a(n)/p(n) formulas resemble the partial sums of the Madhava-Gregory-Leibniz series for Pi = 4*(1-1/3+1/5-1/7+ ...), see the examples. The 'extra term' that appears in the a(n)/p(n) formulas, i.e., 2/(2*n+1), speeds up the convergence of abs(a(n)/p(n)) significantly. The first appearance of a digit in the decimal expansion of Pi occurs here for n: 1, 3, 9, 30, 74, 261, 876, 3056, .., cf. A126809. [Comment modified by the author, Oct 09 2009]"
			],
			"link": [
				"Frits Beukers, \u003ca href=\"http://www.nieuwarchief.nl/serie5/pdf/naw5-2000-01-4-372.pdf\"\u003eA rational approach to Pi\u003c/a\u003e, Nieuw Archief voor de Wiskunde, December 2000, pp. 372-379.",
				"Peter Borwein, \u003ca href=\"http://www.nieuwarchief.nl/serie5/pdf/naw5-2000-01-3-254.pdf\"\u003eThe amazing number Pi\u003c/a\u003e, Nieuw Archief voor de Wiskunde, September 2000, pp. 254-258.",
				"Johannes W. Meijer, \u003ca href=\"/A166107/a166107.jpg\"\u003eA modified Madhava-Gregory-Leibniz formula for Pi\u003c/a\u003e, Mar 02 2013.",
				"Pacific Institute for the Mathematical Sciences, \u003ca href=\"http://www.pims.math.ca/resources/publications/pi-sky\"\u003ePi in the Sky magazine\u003c/a\u003e, 2000-2013.",
				"Wislawa Szymborska, \u003ca href=\"http://katherinestange.com/mathweb/p_p2.html\"\u003eThe admirable number Pi\u003c/a\u003e, Miracle Fair, 2002.",
				"Eric. W. Weisstein, \u003ca href=\"http://mathworld.wolfram.com/GausssDigammaTheorem.html\"\u003eGauss's Digamma Theorem.\u003c/a\u003e, from Wolfram MathWorld.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Madhava_of_Sangamagrama\"\u003e Madhava of Sangamagrama\u003c/a\u003e."
			],
			"formula": [
				"a(n) = p(n)*(-1)^n*4*sum((-1)^(k+1)/(2*k-1), k=1..n) + 2/(2*n+1) with",
				"p(n) = doublefactorial(2*n+1)/doublefactorial(floor((n-1)/3)*2+1) = A220747(n)",
				"PLS(z) = 2*log(2) - Psi(z) - Psi(-z) + Psi(z/2) + Psi(-z/2)",
				"PLS(z=n+1/2) = a(n)/p(n) = (-1)^n*4*sum((-1)^(k+1)/(2*k-1), k=1..n) + 2/(2*n+1)",
				"PLS(z=2*n+5/2) - PLS(z=2*n+1/2) = 2/(4*n+5) - 4/(4*n+3) + 2/(4*n+1) which leads to:",
				"Pi = 2 + 16 * sum(1/((4*n+5)*(4*n+3)*(4*n+1)), n=0 .. infinity).",
				"PLS (z=2*n +7/2) - PLS(z=2*n+3/2) = 2/(4*n+7) - 4/(4*n+5) + 2/(4*n+3) which leads to:",
				"Pi = 10/3 - 16*sum(1/((4*n+7)*(4*n+5)*(4*n+3)), n=0 .. infinity).",
				"The combination of these two formulas leads to:",
				"Pi = 8/3 + 48* sum(1/((4*n+7)*(4*n+5)*(4*n+3)*(4*n+1)), n=0 .. infinity)."
			],
			"example": [
				"The first few values of a(n)/p(n) are: a(0)/p(0) = 2/1; a(1)/p(1) = - 4*(1) + 2/3 = -10/3; a(2)/p(2) = 4*(1-1/3) + 2/5 = 46/15; a(3)/p(3) = - 4*(1-1/3+1/5) + 2/7 = - 334/105; a(4)/p(4)= 4*(1-1/3+1/5-1/7) + 2/9 = 982/315; a(5)/p(5) = - 4*(1-1/3+1/5-1/7+1/9) + 2/11 = -10942/3465; a(6)/p(6) = 4*(1-1/3+1/5-1/7+1/9-1/11) + 2/13 = 140986/45045; a(7)/p(7) = - 4*(1-1/3+1/5-1/7+1/9-1/11+1/13) + 2/15 = - 425730/135135."
			],
			"maple": [
				"A166107 := n -\u003e A220747 (n)*((-1)^n*4*sum((-1)^(k+1)/(2*k-1), k=1..n) + 2/(2*n+1)): A130823 := n -\u003e floor((n-1)/3)*2+1: A220747 := n -\u003e doublefactorial(2*n+1) / doublefactorial(A130823(n)): seq(A166107(n), n=0..20);"
			],
			"xref": [
				"Cf. A162005, A001147, A130823, A025547, A220747, A000796, A157142, A126809, A133766, A133767, A154633."
			],
			"keyword": "easy,sign",
			"offset": "0,1",
			"author": "_Johannes W. Meijer_, Oct 06 2009, Feb 26 2013, Mar 02 2013",
			"references": 4,
			"revision": 16,
			"time": "2016-06-16T23:27:40-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}