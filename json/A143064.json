{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A143064",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 143064,
			"data": "1,1,0,0,0,-1,0,0,-1,0,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,-1,0,0,0,0,0,0,-1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,-1,0,0,0,0,0,0,0,0,0,0,-1,0,0,0,0,0,0,0,0",
			"name": "Expansion of a Ramanujan false theta series variation of A089801 in powers of x.",
			"reference": [
				"Srinivasa Ramanujan, The Lost Notebook and Other Unpublished Papers, Narosa Publishing House, New Delhi, 1988, p. 41, 13th equation."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A143064/b143064.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"G. E. Andrews, \u003ca href=\"http://www.jstor.org/stable/2331943\"\u003eAn introduction to Ramanujan's \"lost\" notebook\u003c/a\u003e, Amer. Math. Monthly 86 (1979), no. 2, 89-108.  See page 89, Equation (1.2), page 100, Equation (5.3)",
				"G. E. Andrews, \u003ca href=\"http://www.personal.psu.edu/gea1/pdf/320.pdf\"\u003eThe Bhargava-Adiga Summation and Partitions\u003c/a\u003e, 2016. See page 3, Equation (1.5)"
			],
			"formula": [
				"Expansion of Sum_{k\u003e=0} x^k / (Product_{j=0..k} ( 1 + x^(2*k + 1) ) ) in powers of x^2. - _Michael Somos_, Nov 04 2013",
				"a(n) = b(3*n + 1) where b() is multiplicative with b(p^(2*e)) = -(-1)^e if p = 2, b(p^(2*e)) = (-1)^e if p = 5 (mod 6), b(p^(2*e)) = 1 if p = 1 (mod 6), and b(p^(2*e-1)) = b(3^e) = 0 if e\u003e0. - _Michael Somos_, Jul 19 2013",
				"a(4*n + 2) = a(4*n + 3) = a(8*n + 4) = 0.",
				"a(8*n) = A143062(n). Convolution of A010054 with A143065. - _Michael Somos_, Jul 19 2013",
				"G.f.: Sum_{k\u003e=0} (-1)^k * x^(3*k^2 + 2*k) * ( 1 + x^(2*k + 1) ).",
				"G.f.: 1/(1 - x*(1-x)/(1 - x^2*(1-x^2)/(1 - x^3*(1-x^3)/(1 - x^4*(1-x^4)/(1 - ...))))), a continued fraction. - _Paul D. Hanna_, Jul 18 2013",
				"abs(a(n)) = A089801(n). - _Michael Somos_, Jun 30 2015",
				"G.f.: 1 + x*(1-x) + x^2*(1-x)*(1-x^3) + x^3*(1-x)*(1-x^3)*(1-x^5) + ... . - _Michael Somos_, Aug 03 2017"
			],
			"example": [
				"G.f. = 1 + x - x^5 - x^8 + x^16 + x^21 - x^33 - x^40 + x^56 + x^65 - x^85 - x^96 + ...",
				"G.f. = q + q^4 - q^16 - q^25 + q^49 + q^64 - q^100 - q^121 + q^169 + q^196 + ..."
			],
			"mathematica": [
				"a[ n_] := With[ {m = Sqrt[3 n + 1]}, If[ IntegerQ @ m, (-1)^Quotient[ m, 3], 0]]; (* _Michael Somos_, Jun 30 2015 *)",
				"a[ n_] := SeriesCoefficient[ Sum[ (-1)^k x^(3 k^2 + 2 k) (1 + x^(2 k + 1)), {k, 0, n}], {x, 0, n}]; (* _Michael Somos_, Nov 04 2013 *)",
				"a[ n_] := SeriesCoefficient[ Sum[ x^k QPochhammer[ x, x^2, k], {k, 0, n}], {x, 0, n}]; (* _Michael Somos_, Jun 30 2015 *)",
				"a[ n_] := SeriesCoefficient[ Sum[ x^k / QPochhammer[ -x, x^2, k + 1], {k, 0, 2 n}], {x, 0, 2 n}]; (* _Michael Somos_, Jun 30 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = my(m); if( issquare( 3*n + 1, \u0026m), (-1)^(m \\ 3) )};",
				"(PARI) {a(n) = my(A, p, e); if( n\u003c0, 0, n = 3*n + 1; A = factor(n); prod(k=1, matsize(A)[1], [p, e] = A[k, ]; if( e%2, 0, p==2, -(-1)^(e/2), p == 3, 0, p%6 == 1, 1, (-1)^(e/2))))}; /* _Michael Somos_, Jul 19 2013 */",
				"(PARI) /* Continued Fraction: */",
				"{a(n)=local(CF); CF=1+x; for(k=0, n, CF=1/(1 - x^(n-k+1)*(1 - x^(n-k+1))*CF+x*O(x^n))); polcoeff(CF, n)} \\\\ _Paul D. Hanna_, Jul 18 2013"
			],
			"xref": [
				"Cf. A089801, A010054, A143062, A143065.",
				"Column m=0 of A185646."
			],
			"keyword": "sign",
			"offset": "0,1",
			"author": "_Michael Somos_, Jul 21 2008",
			"references": 9,
			"revision": 30,
			"time": "2017-08-04T01:20:42-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}