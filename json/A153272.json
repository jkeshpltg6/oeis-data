{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A153272",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 153272,
			"data": "7,7,56,7,63,693,7,70,910,14560,7,77,1155,21945,504735,7,84,1428,31416,848232,27143424,7,91,1729,43225,1339975,49579075,2131900225,7,98,2058,57624,2016840,84707280,4150656720,232436776320,7,105,2415,74865,2919735,137227545,7547514975,475493443425,33760034483175",
			"name": "Triangle T(n, k) = Product_{j=0..k} (j*n + prime(m)), with T(n, 0) = prime(m) and m = 4, read by rows.",
			"comment": [
				"Row sums are {7, 63, 763, 15547, 527919, 28024591, 2182864327, 236674216947, 34243215666247, 6391699984166119, 1497639790982770659, ...}."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A153272/b153272.txt\"\u003eRows n = 0..100 of triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = Product_{j=0..k} (j*n + prime(m)), with T(n, 0) = prime(m) and m = 4."
			],
			"example": [
				"Triangle begins as:",
				"  7;",
				"  7, 56;",
				"  7, 63,  693;",
				"  7, 70,  910, 14560;",
				"  7, 77, 1155, 21945,  504735;",
				"  7, 84, 1428, 31416,  848232, 27143424;",
				"  7, 91, 1729, 43225, 1339975, 49579075, 2131900225;"
			],
			"maple": [
				"m:=4; seq(seq(`if`(k=0, ithprime(m), mul(j*n + ithprime(m), j=0..k)), k=0..n), n=0..10); # _G. C. Greubel_, Dec 03 2019"
			],
			"mathematica": [
				"T[n_, k_, m_]:= If[k==0, Prime[m], Product[j*n + Prime[m], {j,0,k}]];",
				"Table[T[n,k,4], {n,0,10}, {k,0,n}]//Flatten"
			],
			"program": [
				"(PARI) T(n,k) = my(m=4); if(k==0, prime(m), prod(j=0,k, j*n + prime(m)) ); \\\\ _G. C. Greubel_, Dec 03 2019",
				"(MAGMA) m:=4;",
				"function T(n,k)",
				"  if k eq 0 then return NthPrime(m);",
				"  else return (\u0026*[j*n + NthPrime(m): j in [0..k]]);",
				"  end if; return T; end function;",
				"[T(n,k): k in [0..n], n in [0..10]]; // _G. C. Greubel_, Dec 03 2019",
				"(Sage)",
				"def T(n, k):",
				"    m=4",
				"    if (k==0): return nth_prime(m)",
				"    else: return product(j*n + nth_prime(m) for j in (0..k))",
				"[[T(n, k) for k in (0..n)] for n in (0..10)] # _G. C. Greubel_, Dec 03 2019"
			],
			"xref": [
				"Cf. A153270 (m=2), A153271 (m=3), this sequence (m=4).",
				"Cf. A001730, A051579, A051604."
			],
			"keyword": "nonn,tabl",
			"offset": "0,1",
			"author": "_Roger L. Bagula_, Dec 22 2008",
			"ext": [
				"Edited by _G. C. Greubel_, Dec 03 2019"
			],
			"references": 3,
			"revision": 10,
			"time": "2019-12-04T20:58:29-05:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}