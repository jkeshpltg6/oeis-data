{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A303656",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 303656,
			"data": "0,1,1,2,1,3,2,3,2,4,3,4,2,4,4,3,2,4,4,3,2,4,3,4,1,4,5,6,4,6,5,5,6,6,5,8,4,6,6,5,4,7,5,7,5,6,4,5,3,4,7,6,7,8,5,4,7,5,5,9,3,6,5,6,4,6,5,7,7,4,5,5,5,4,6,5,6,10,5,4,5,7,4,9,2,9,8,5,6,6",
			"name": "Number of ways to write n as a^2 + b^2 + 3^c + 5^d, where a,b,c,d are nonnegative integers with a \u003c= b.",
			"comment": [
				"Conjecture: a(n) \u003e 0 for all n \u003e 1. In other words, any integer n \u003e 1 can be written as the sum of two squares, a power of 3 and a power of 5.",
				"It has been verified that a(n) \u003e 0 for all n = 2..2*10^10.",
				"It seems that any integer n \u003e 1 also can be written as the sum of two squares, a power of 2 and a power of 3.",
				"The author would like to offer 3500 US dollars as the prize for the first proof of his conjecture that a(n) \u003e 0 for all n \u003e 1. - _Zhi-Wei Sun_, Jun 05 2018"
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A303656/b303656.txt\"\u003eTable of n, a(n) for n = 1..100000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2016.11.008\"\u003eRefining Lagrange's four-square theorem\u003c/a\u003e, J. Number Theory 175(2017), 167-190.",
				"Zhi-Wei Sun, \u003ca href=\"http://maths.nju.edu.cn/~zwsun/179b.pdf\"\u003eNew conjectures on representations of integers (I)\u003c/a\u003e, Nanjing Univ. J. Math. Biquarterly 34(2017), no. 2, 97-120.",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1701.05868\"\u003eRestricted sums of four squares\u003c/a\u003e, arXiv:1701.05868 [math.NT], 2017-2018."
			],
			"example": [
				"a(2) = 1 with 2 = 0^2 + 0^2 + 3^0 + 5^0.",
				"a(5) = 1 with 5 = 0^2 + 1^2 + 3^1 + 5^0.",
				"a(25) = 1 with 25 = 1^2 + 4^2 + 3^1 + 5^1."
			],
			"mathematica": [
				"SQ[n_]:=SQ[n]=IntegerQ[Sqrt[n]];",
				"f[n_]:=f[n]=FactorInteger[n];",
				"g[n_]:=g[n]=Sum[Boole[Mod[Part[Part[f[n],i],1],4]==3\u0026\u0026Mod[Part[Part[f[n],i],2],2]==1],{i,1,Length[f[n]]}]==0;",
				"QQ[n_]:=QQ[n]=(n==0)||(n\u003e0\u0026\u0026g[n]);",
				"tab={};Do[r=0;Do[If[QQ[n-3^k-5^m],Do[If[SQ[n-3^k-5^m-x^2],r=r+1],{x,0,Sqrt[(n-3^k-5^m)/2]}]],{k,0,Log[3,n]},{m,0,If[n==3^k,-1,Log[5,n-3^k]]}];tab=Append[tab,r],{n,1,90}];Print[tab]"
			],
			"xref": [
				"Cf. A000244, A000290, A000351, A001481, A273812, A302982, A302984, A303233, A303234, A303338, A303363, A303389, A303393, A303399, A303428, A303401, A303429, A303432, A303434, A303539, A303540, A303541, A303543, A303601, A303637, A303639, A303702, A303821."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Zhi-Wei Sun_, Apr 27 2018",
			"references": 22,
			"revision": 35,
			"time": "2018-06-20T22:18:22-04:00",
			"created": "2018-04-28T11:31:46-04:00"
		}
	]
}