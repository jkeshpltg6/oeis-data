{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A213948",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 213948,
			"data": "1,1,1,1,2,2,1,4,4,4,1,7,10,8,8,1,12,24,20,16,16,1,20,52,56,40,32,32,1,33,112,144,112,80,64,64,1,54,238,344,320,224,160,128,128,1,88,496,828,848,640,448,320,256,256",
			"name": "Triangle, by rows, generated from the INVERT transforms of (1, 1, 2, 4, 8, 16, ...).",
			"comment": [
				"Row sums = A001519, the odd-indexed Fibonacci terms. The triangle is a companion to A213947, having row sums of the even-indexed Fibonacci terms."
			],
			"formula": [
				"Create an array in which the n-th row is the INVERT transform of the first n terms in the sequence (1, 1, 2, 4, 8, 16, 32, ...):",
				"  1,    1,    1,    1,    1,    1,",
				"  1,    2,    3,    5,    8,   13,    (essentially A000045)",
				"  1,    2,    5,    9,   18,   37,    (essentially A077947)",
				"  1,    2,    5,   13,   26,   57,",
				"Terms of the n-th row of the triangle are the finite differences downwards the n-th column of this array."
			],
			"example": [
				"First few rows of the triangle are:",
				"  1;",
				"  1,   1;",
				"  1,   2,   2;",
				"  1,   4,   4,   4;",
				"  1,   7,  20,   8,   8;",
				"  1,  12,  24,  20,  16,  16;",
				"  1,  20,  52,  56,  40,  32,  32;",
				"  1,  33, 112, 144, 112,  80,  64,  64;",
				"  1,  54, 238, 344, 320, 224, 160, 128, 128;",
				"  1,  88, 496, 828, 848, 640, 448, 320, 256, 256;",
				"  ..."
			],
			"maple": [
				"read(\"transforms\") ;",
				"A213948i := proc(n,k)",
				"    if n = 1 then",
				"        L := [1,seq(0,i=0..k)] ;",
				"    else",
				"        L := [1,seq(2^i,i=0..n-2),seq(0,i=0..k)] ;",
				"    end if;",
				"    INVERT(L) ;",
				"    op(k,%) ;",
				"end proc:",
				"A213948 := proc(n,k)",
				"    if k = 1 then",
				"        1;",
				"    else",
				"        A213948i(k,n)-A213948i(k-1,n) ;",
				"    end if;",
				"end proc: # _R. J. Mathar_, Jun 30 2012"
			],
			"xref": [
				"Cf. A001519, A213947, A000071 (2nd column), A020714 (subdiagonal), A005009 (subdiagonal)."
			],
			"keyword": "nonn,tabl",
			"offset": "1,5",
			"author": "_Gary W. Adamson_, Jun 25 2012",
			"references": 2,
			"revision": 15,
			"time": "2019-12-11T07:29:54-05:00",
			"created": "2012-07-04T14:34:29-04:00"
		}
	]
}