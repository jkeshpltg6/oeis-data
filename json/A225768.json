{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A225768",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 225768,
			"data": "0,1,1,2,1,18,1,2,0,2,1,54,1,28,3,2,1,18,1,2,399,26,1,6,5,2,21,0,1,288,1,4,3,2,105,6,1,2,33,2,1,546,1,2,3,2,1,6,35,2,51,20,1,12,5,28,9,4,1,18,1,4,63,2,0,18,1,2,3,28,1,6,1,2,15,2,35,24,1,12,3,4,1,42,115,2,111,2,1,18,91,6,3,2,3,6,1,28,3,2",
			"name": "Least k \u003e 0 such that k^6 + n is prime, or 0 if k^6 + n is never prime.",
			"comment": [
				"Motivated by the \"particularly poor polynomial\" n^6+1091 (composite for n=1,...,3905) mentioned on Weisstein's page about prime generating polynomials.",
				"We have a(n) = 0 if n is a cube n = g^3 with g \u003e 1 because then k^6 + g^3 = (k^2 + g)*(k^4 - k^2*g + g^2) ), which can be prime only when n = g = 1. - _T. D. Noe_, Nov 18 2013",
				"By the theorem of Brillhart, Filaseta and Odlyzko (see link), if a(n) \u003e n \u003e 1 then x^6 + n must be irreducible.  If x^6 + n is irreducible, the Bunyakovsky conjecture implies a(n) is finite. - _Robert Israel_, Apr 25 2016"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A225768/b225768.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"J. Brillhart, M. Filaseta, A. Odlyzko, \u003ca href=\"https://dx.doi.org/10.4153%2FCJM-1981-080-0\"\u003eOn an irreducibility theorem of A. Cohn\u003c/a\u003e. Canadian Journal of Mathematics 33(1981), 1055-1059.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Prime-GeneratingPolynomial.html\"\u003ePrime-Generating Polynomial\u003c/a\u003e."
			],
			"maple": [
				"f:= proc(n) local exact, x,k,F,nf,F1,C;",
				"    iroot(n,3,exact);",
				"    if exact and n \u003e 1 then return 0 fi;",
				"    if irreduc(x^6+n) then",
				"       for k from 1+(n mod 2) by 2 do if isprime(k^6+n) then return k fi od",
				"    else",
				"       F:= factors(x^6+n)[2]; #",
				"       F1:= map(t -\u003e t[1],F);",
				"       nf:= nops(F);",
				"       C:= map(t -\u003e op(map(rhs@op,{isolve(t^2-1)})),F1);",
				"       for k in sort(convert(select(type,C,positive),list)) do",
				"         if isprime(k^6+n) then return k fi",
				"       od:",
				"       0",
				"    fi",
				"end proc:",
				"map(f, [$0..100]); # _Robert Israel_, Apr 25 2016"
			],
			"mathematica": [
				"{0, 1}~Join~Table[If[IrreduciblePolynomialQ[x^6 + n], SelectFirst[Range[1 + Mod[n, 2], 10^3, 2], PrimeQ[#^6 + n] \u0026], 0], {n, 2, 120}] (* _Michael De Vlieger_, Apr 25 2016, Version 10 *)"
			],
			"program": [
				"(PARI) {(a,b=6)-\u003e#factor(x^b+a)~==1 \u0026 for(n=1, 9e9, ispseudoprime(n^b+a)\u0026return(n)); a==1\u0026return(1);print1(\"/*\"a\":\", factor(x^b+a)\"*/\")} /* For illustrative purpose only. The polynomial x^6+a is factored to avoid an infinite loop when it is composite. But there could be x such that this is prime, when all factors but one are 1 (not for exponent b=6, but, e.g., x=4 for exponent b=4), see A225766. */"
			],
			"xref": [
				"See A085099, A225765 - A225770 for the k^2, k^3, ..., k^8 analogs."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_M. F. Hasler_, Jul 25 2013",
			"references": 6,
			"revision": 30,
			"time": "2016-04-27T15:11:27-04:00",
			"created": "2013-07-26T11:25:55-04:00"
		}
	]
}