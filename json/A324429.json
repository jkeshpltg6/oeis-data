{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A324429",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 324429,
			"data": "1,0,1,0,2,1,0,11,3,1,0,74,24,6,1,0,652,225,57,10,1,0,7069,2489,678,141,17,1,0,90946,32326,9375,2107,352,28,1,0,1353554,483968,146334,35568,6722,832,46,1,0,22870541,8211543,2555228,661329,137225,21510,1973,75,1",
			"name": "Number T(n,k) of labeled cyclic chord diagrams having n chords and minimal chord length k (or k=0 if n=0); triangle T(n,k), n\u003e=0, 0\u003c=k\u003c=n, read by rows.",
			"comment": [
				"T(n,k) is defined for all n,k \u003e= 0. The triangle contains only the terms with 0 \u003c= k \u003c= n. T(n,k) = 0 for k \u003e n."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A324429/b324429.txt\"\u003eRows n = 0..19, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = A324428(n,k) - A324428(n,k+1) for k \u003e 0, T(n,0) = A000007(n)."
			],
			"example": [
				"Triangle T(n,k) begins:",
				"  1;",
				"  0,       1;",
				"  0,       2,      1;",
				"  0,      11,      3,      1;",
				"  0,      74,     24,      6,     1;",
				"  0,     652,    225,     57,    10,    1;",
				"  0,    7069,   2489,    678,   141,   17,   1;",
				"  0,   90946,  32326,   9375,  2107,  352,  28,  1;",
				"  0, 1353554, 483968, 146334, 35568, 6722, 832, 46, 1;",
				"  ..."
			],
			"maple": [
				"b:= proc(n, f, m, l, j) option remember; (k-\u003e `if`(n\u003cadd(i, i=f)+m+",
				"      add(i, i=l), 0, `if`(n=0, 1, add(`if`(f[i]=0, 0, b(n-1,",
				"      subsop(i=0, f), m+l[1], [subsop(1=[][], l)[], 0], max(0, j-1))),",
				"      i=max(1, j+1)..min(k, n-1))+`if`(m=0, 0, m*b(n-1, f, m-1+l[1],",
				"      [subsop(1=[][], l)[], 0], max(0, j-1)))+b(n-1, f, m+l[1],",
				"      [subsop(1=[][], l)[], 1], max(0, j-1)))))(nops(l))",
				"    end:",
				"A:= (n, k)-\u003e `if`(n=0 or k\u003c2, doublefactorial(2*n-1),",
				"              b(2*n-k+1, [1$k-1], 0, [0$k-1], k-1)):",
				"T:= (n, k)-\u003e `if`(n=k, 1, A(n, k)-A(n, k+1)):",
				"seq(seq(T(n, k), k=0..n), n=0..10);"
			],
			"mathematica": [
				"b[n_, f_List, m_, l_List, j_] := b[n, f, m, l, j] = Function[k, If[n \u003c Total[f] + m +  Total[l], 0, If[n == 0, 1, Sum[If[f[[i]] == 0, 0, b[n - 1, ReplacePart[f, i -\u003e 0], m + l[[1]], Append[ReplacePart[l, 1 -\u003e Nothing], 0], Max[0, j - 1]]], {i, Max[1, j + 1], Min[k, n - 1]}] + If[m == 0, 0, m*b[n - 1, f, m - 1 + l[[1]], Append[ReplacePart[l, 1 -\u003e Nothing], 0], Max[0, j-1]]] + b[n-1, f, m + l[[1]], Append[ReplacePart[ l, 1 -\u003e Nothing], 1], Max[0, j - 1]]]]][Length[l]];",
				"A[n_, k_] := If[n == 0 || k \u003c 2, 2^(n-1) Pochhammer[3/2, n-1], b[2n-k+1, Table[1, {k - 1}], 0, Table[0, {k - 1}], k - 1]];",
				"T[n_, k_] := If[n == k, 1, A[n, k] - A[n, k + 1]];",
				"Table[T[n, k], {n, 0, 10}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Apr 27 2020, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Columns k=0-10 give: A000007, A324445, A324446, A324447, A324448, A324449, A324450, A324451, A324452, A324453, A324454.",
				"Row sums give A001147.",
				"Main diagonal gives A000012.",
				"T(n+1,n) gives A001610.",
				"Cf. A293157, A293881, A324428."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Alois P. Heinz_, Feb 27 2019",
			"references": 12,
			"revision": 24,
			"time": "2020-04-27T06:32:13-04:00",
			"created": "2019-02-28T11:18:16-05:00"
		}
	]
}