{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A152244",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 152244,
			"data": "1,6,0,2,-18,0,-22,0,0,26,12,0,25,54,0,-46,0,0,26,-132,0,-22,0,0,-45,0,0,0,156,0,74,-36,0,122,0,0,-46,150,0,-142,-162,0,0,0,0,-44,-276,0,2,0,0,194,0,0,-214,156,0,0,396,0,121,0,0,146,-132,0,52,0,0,-22,0,0,0,-270,0,-286,0,0,-118",
			"name": "Expansion of a(x) * f(-x^3)^4 in powers of x where f() is a Ramanujan theta function and a() is a cubic AGM function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Cubic AGM theta functions: a(q) (see A004016), b(q) (A005928), c(q) (A005882)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A152244/b152244.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(-1/2) *( (eta(q)*eta(q^3))^3 + 9*(eta(q^3)*eta(q^9))^3 ) in powers of q.",
				"a(n) = b(2*n + 1) where b() is multiplicative with b(2^e) = 0^e, b(3^e) = -2 * (-3)^e if e\u003e0, b(p^e) = p^e * (1 + (-1)^e) / 2 if p == 5 (mod 6), b(p^e) = b(p) * b(p^(e-1)) - p^2 * b(p^(e-2)) if p == 1 (mod 6).",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (36 t)) = 139968^(1/2) (t/i)^3 g(t) where q = exp(2 Pi i t) and g() is the g.f. for A152243.",
				"a(3*n) = A152243(n). a(3*n + 1) = 6 * A030208(n). a(3*n + 2) = 0."
			],
			"example": [
				"G.f. = 1 + 6*x + 2*x^3 - 18*x^4 - 22*x^6 + 26*x^9 + 12*x^10 + 25*x^11 + ...",
				"G.f. = q + 6*q^3 + 2*q^7 - 18*q^9 - 22*q^13 + 26*q^19 + 12*q^21 + 25*q^25 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ (QPochhammer[ x] QPochhammer[ x^3])^3 + 9 x (QPochhammer[ x^3] QPochhammer[ x^9])^3 , {x, 0, n}]; (* _Michael Somos_, Sep 02 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( (eta(x + A) * eta(x^3 + A))^3 + 9 * x * (eta(x^3 + A) * eta(x^9 + A))^3, n))};"
			],
			"xref": [
				"Cf. A030208, A152243."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Nov 30 2008",
			"references": 2,
			"revision": 12,
			"time": "2021-03-12T22:24:45-05:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}