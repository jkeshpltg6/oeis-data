{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A027472",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 27472,
			"data": "1,9,54,270,1215,5103,20412,78732,295245,1082565,3897234,13817466,48361131,167403915,573956280,1951451352,6586148313,22082967873,73609892910,244074908070,805447196631,2646469360359,8661172452084,28242953648100,91789599356325,297398301914493,960825283108362,3095992578904722",
			"name": "Third convolution of the powers of 3 (A000244).",
			"comment": [
				"Third column of A027465.",
				"With offset = 2, a(n) is the number of length n words on alphabet {u,v,w,z} such that each word contains exactly 2 u's. - _Zerinvary Lajos_, Dec 29 2007"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A027472/b027472.txt\"\u003eTable of n, a(n) for n = 3..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (9,-27,27)."
			],
			"formula": [
				"Numerators of sequence a[3,n] in (b^2)[i,j]) where b[i,j] = binomial(i-1, j-1)/2^(i-1) if j \u003c= i, 0 if j \u003e i.",
				"From _Wolfdieter Lang_: (Start)",
				"a(n) = 3^(n-3)*binomial(n-1, 2).",
				"G.f.: (x/(1-3*x))^3. (Third convolution of A000244, powers of 3.) (End)",
				"a(n) = |A075513(n, 2)|/9, n \u003e= 3.",
				"a(n) = A152818(n-3,2)/2 = A006043(n-3)/2. - _Paul Curtz_, Jan 07 2009",
				"The sequence 0, 1, 9, 54, ... has e.g.f.: (x + 3*x^2/2)*exp(3*x)/. - _Paul Barry_, Jul 23 2003",
				"E.g.f.: E(0) where E(k) = 1 + 3*(2*k+3)*x/((2*k+1)^2 - 3*x*(k+2)*(2*k+1)^2/(3*x*(k+2) + 2*(k+1)^2/E(k+1))); (continued fraction, 3-step). - _Sergei N. Gladkovskii_, Nov 23 2012",
				"With offset=2 e.g.f.: x^2*exp(3*x)/2. - _Geoffrey Critzer_, Oct 03 2013",
				"From _Amiram Eldar_, Jan 05 2022: (Start)",
				"Sum_{n\u003e=3} 1/a(n) = 6 - 12*log(3/2).",
				"Sum_{n\u003e=3} (-1)^(n+1)/a(n) = 24*log(4/3) - 6. (End)"
			],
			"mathematica": [
				"nn=41; Drop[Range[0,nn]!CoefficientList[Series[Exp[x]^3 x^2/2!,{x,0,nn}],x],2] (* _Geoffrey Critzer_, Oct 03 2013 *)",
				"LinearRecurrence[{9,-27,27}, {1,9,54}, 40] (* _G. C. Greubel_, May 12 2021 *)"
			],
			"program": [
				"(Sage) [3^(n-3)*binomial(n-1,2) for n in range(3, 40)] # _Zerinvary Lajos_, Mar 10 2009",
				"(PARI) a(n)=([0,1,0; 0,0,1; 27,-27,9]^(n-3)*[1;9;54])[1,1] \\\\ _Charles R Greathouse IV_, Oct 03 2016",
				"(MAGMA) [3^(n-3)*Binomial(n-1, 2): n in [3..40]]; // _G. C. Greubel_, May 12 2021"
			],
			"xref": [
				"Cf. A000244, A006043, A027465, A075513, A152818.",
				"Sequences similar to the form q^(n-2)*binomial(n, 2): A000217 (q=1), A001788 (q=2), this sequence (q=3), A038845 (q=4), A081135 (q=5), A081136 (q=6), A027474 (q=7), A081138 (q=8), A081139 (q=9), A081140 (q=10), A081141 (q=11), A081142 (q=12), A027476 (q=15)."
			],
			"keyword": "nonn,easy,changed",
			"offset": "3,2",
			"author": "_Olivier Gérard_",
			"ext": [
				"Corrected by _T. D. Noe_, Nov 07 2006",
				"Better name from _Wolfdieter Lang_",
				"Terms a(23) onward added by _G. C. Greubel_, May 12 2021"
			],
			"references": 41,
			"revision": 57,
			"time": "2022-01-05T05:28:04-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}