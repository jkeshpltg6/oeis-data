{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A257727",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 257727,
			"data": "1,2,3,4,5,6,7,8,10,12,9,14,11,16,20,24,13,18,15,28,22,32,17,40,48,26,36,30,21,56,25,44,64,34,80,96,19,52,72,60,29,42,23,112,50,88,33,128,68,160,192,38,41,104,144,120,58,84,49,46,27,224,100,176,66,256,37,136,320,384,31,76,57,82,208,288,240,116,45",
			"name": "Permutation of natural numbers: a(1) = 1, a(oddprime(n)) = 1 + 2*a(n), a(not_an_oddprime(n)) = 2*a(n-1).",
			"comment": [
				"Here oddprime(n) = n-th odd prime = A065091(n) = A000040(n+1), not_an_oddprime(n) = n-th natural number which is not an odd prime = A065090(n)."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A257727/b257727.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"a(1) = 1; a(2) = 2; and for n \u003e 2, if A010051(n) = 1 [i.e., when n is a prime], then a(n) = 1 + 2*a(A000720(n)-1), otherwise a(n) = 2*a(A062298(n)).",
				"As a composition of other permutations:",
				"a(n) = A246377(A257730(n)).",
				"a(n) = A257725(A257801(n))."
			],
			"example": [
				"For n=2, which is the second natural number \u003e= 1 that is not an odd prime [2 = A065090(2)], we compute 2*a(1) = 2 = a(2).",
				"For n=4, which is A065090(3), we compute 2*a(3-1) = 2*2 = 4.",
				"For n=5, and 5 is the second odd prime [5 = A065091(2)], thus a(5) = 1 + 2*a(2) = 5.",
				"For n=9, which is the sixth natural number \u003e= 1 not an odd prime (9 = A065090(6)), we compute 2*a(6-1) = 2*5 = 10.",
				"For n=11, which is the fourth odd prime [11 = A065091(4)], we compute 1 + 2*a(4) = 1 + 2*4 = 9, thus a(11) = 9."
			],
			"program": [
				"(Scheme, with memoizing definec-macro)",
				"(definec (A257727 n) (cond ((\u003c= n 2) n) ((= 1 (A010051 n)) (+ 1 (* 2 (A257727 (+ -1 (A000720 n)))))) (else (* 2 (A257727 (A062298 n))))))"
			],
			"xref": [
				"Inverse: A257728.",
				"Cf. A000720, A010051, A062298, A065090, A065091.",
				"Related or similar permutations: A246377, A246378, A257725, A257730, A257801."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Antti Karttunen_, May 09 2015",
			"references": 4,
			"revision": 13,
			"time": "2015-05-10T11:00:30-04:00",
			"created": "2015-05-10T11:00:30-04:00"
		}
	]
}