{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006216",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6216,
			"id": "M1466",
			"data": "2,5,14,46,178,800,4094,23536,150178,1053440,8057774,66750976,595380178,5688903680,57975175454,627692271616,7195247514178,87056789995520,1108708685037134,14825405274259456,207676251991176178",
			"name": "Number of down-up permutations of n+4 starting with 4.",
			"comment": [
				"Entringer numbers."
			],
			"reference": [
				"R. C. Entringer, A combinatorial interpretation of the Euler and Bernoulli numbers, Nieuw Archief voor Wiskunde, 14 (1966), 241-246.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A006216/b006216.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"B. Bauslaugh and F. Ruskey, \u003ca href=\"http://webhome.cs.uvic.ca/~ruskey/Publications/AlternatingLex/AlternatingLex.html\"\u003eGenerating alternating permutations lexicographically\u003c/a\u003e, Nordisk Tidskr. Informationsbehandling (BIT) 30 (1990) 16-26.",
				"J. Millar, N. J. A. Sloane and N. E. Young, A new operation on sequences: the Boustrophedon transform, J. Combin. Theory, 17A (1996) 44-54 (\u003ca href=\"http://neilsloane.com/doc/bous.txt\"\u003eAbstract\u003c/a\u003e, \u003ca href=\"http://neilsloane.com/doc/bous.pdf\"\u003epdf\u003c/a\u003e, \u003ca href=\"http://neilsloane.com/doc/bous.ps\"\u003eps\u003c/a\u003e).",
				"C. Poupard, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(82)90293-X\"\u003eDe nouvelles significations énumeratives des nombres d'Entringer\u003c/a\u003e, Discrete Math., 38 (1982), 265-271."
			],
			"formula": [
				"a(n) = 3*E(n+2) - E(n), where E(j) = A000111(j) = j!*[x^j](sec(x) + tan(x)) are the up/down or Euler numbers. - _Emeric Deutsch_, May 15 2004",
				"E.g.f.: 6/(cos(x)*(1 - sin(x))) - tan(x) - 4*sec(x). - _Sergei N. Gladkovskii_, Jun 04 2015",
				"a(n) ~ 3*n^2 * 2^(n+4) * n! / Pi^(n+3). - _Vaclav Kotesovec_, Jun 04 2015"
			],
			"example": [
				"a(1) = 5 because we have 41325, 41523, 42314, 42513 and 43512."
			],
			"maple": [
				"f:=sec(x)+tan(x): fser:=series(f,x=0,30): E[0]:=1: for n from 1 to 25 do E[n]:=n!*coeff(fser,x^n) od: seq(3*E[n+2]-E[n],n=0..20);"
			],
			"mathematica": [
				"e[0] = e[1] = 1; e[n_] := 2*Sum[ 4^m*Sum[ (i-(n-1)/2)^(n-1)*Binomial[ n-2*m-1, i-m]*(-1)^(n-i-1), {i, m, (n-1)/ 2}], {m, 0, (n-2)/2}]; a[0]=2; a[n_] := 3e[n+2] - e[n]; Table[a[n], {n, 0, 20}] (* _Jean-François Alcover_, Jan 27 2012, after _Emeric Deutsch_ *)"
			],
			"program": [
				"(PARI) {a(n) = local(v=[1], t); if( n\u003c0, 0, for(k=2, n+4, t=0; v = vector(k, i, if( i\u003e1, t += v[k+1-i]))); v[4])}; /* _Michael Somos_, Feb 03 2004 */"
			],
			"xref": [
				"Cf. A000111. Column k=3 in A008282."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_",
			"references": 1,
			"revision": 39,
			"time": "2021-02-17T07:35:23-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}