{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000689",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 689,
			"data": "1,2,4,8,6,2,4,8,6,2,4,8,6,2,4,8,6,2,4,8,6,2,4,8,6,2,4,8,6,2,4,8,6,2,4,8,6,2,4,8,6,2,4,8,6,2,4,8,6,2,4,8,6,2,4,8,6,2,4,8,6,2,4,8,6,2,4,8,6,2,4,8,6,2,4,8,6,2,4,8,6",
			"name": "Final decimal digit of 2^n.",
			"comment": [
				"These are the analogs of the powers of 2 in carryless arithmetic mod 10.",
				"Let G = {2,4,8,6}. Let o be defined as XoY = least significant digit in XY. Then (G,o) is an Abelian group wherein 2 is a generator (Also see the first comment under A001148). - _K.V.Iyer_, Mar 12 2010",
				"This is also the decimal expansion of 227/1818. - _Kritsada Moomuang_, Dec 21 2021"
			],
			"link": [
				"David Applegate, Marc LeBrun and N. J. A. Sloane, \u003ca href=\"http://neilsloane.com/doc/carry1.pdf\"\u003eCarryless Arithmetic (I): The Mod 10 Version\u003c/a\u003e",
				"\u003ca href=\"/index/Ca#CARRYLESS\"\u003eIndex entries for sequences related to carryless arithmetic\u003c/a\u003e",
				"\u003ca href=\"/index/Fi#final\"\u003eIndex entries for sequences related to final digits of numbers\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,-1,1)."
			],
			"formula": [
				"Periodic with period 4.",
				"a(n) = 2^n mod 10.",
				"a(n) = A002081(n) - A002081(n-1), for n \u003e 0.",
				"a(n) = (1/6)*{8*(n mod 4)-[(n+1) mod 4]+2*[(n+2) mod 4]+11*[(n+3) mod 4]}-5*{1-[((n+1)!+1) mod (n+1)]}, with n\u003e=0. - _Paolo P. Lava_, Jun 25 2007; corrected by _Paolo P. Lava_, Mar 23 2010",
				"a(n) = +a(n-1) -a(n-2) +a(n-3), n\u003e3. G.f.: (x+3*x^2+5*x^3+1)/((1-x) * (1+x^2)). - _R. J. Mathar_, Apr 13 2010",
				"a(n) = 5+(1/2)*[(1+3*I)*I^n+(1-3*I)*(-I)^n]-5*[C(2*n,n) mod 2], with n\u003e=0. - _Paolo P. Lava_, May 10 2010",
				"For n\u003e=1, a(n) = 10 - (4x^3 +47x -27x^2)/3, where x = (n+3) mod 4 + 1.",
				"For n\u003e=1, a(n) = A070402(n) + 5*floor( ((n-1) mod 4)/2 ).",
				"G.f.: 1 / (1 - 2*x / (1 + 5*x^3 / (1 + x / (1 - 3*x / (1 + 3*x))))). - _Michael Somos_, May 12 2012",
				"a(n) = 5 + cos((n*Pi)/2) - 3*sin((n*Pi)/2) for n\u003e=1. - _Kritsada Moomuang_, Dec 21 2021"
			],
			"example": [
				"G.f. = 1 + 2*x + 4*x^2 + 8*x^3 + 6*x^4 + 2*x^5 + 4*x^6 + 8*x^7 + 6*x^8 + ..."
			],
			"maple": [
				"P:=proc(n) local a,i; for i from 0 by 1 to n do a:=1/6*(8*(i mod 4)-((i+1) mod 4)+2*((i+2) mod 4)+11*((i+3) mod 4))-5*(1-(((i+1)!+1) mod (i+1))); print(a); od; end: P(100); # _Paolo P. Lava_, Jun 25 2007"
			],
			"mathematica": [
				"Table[PowerMod[2, n, 10], {n, 0, 200}] (* _Vladimir Joseph Stephan Orlovsky_, Jun 10 2011 *)"
			],
			"program": [
				"(Sage) [power_mod(2,n,10)for n in range(0, 81)] #  _Zerinvary Lajos_, Nov 03 2009",
				"(PARI) for(n=0,80, if(n,{x=(n+3)%4+1; print1(10-(4*x^3+47*x-27*x^2)/3,\", \")},{print1(\"1, \")}))",
				"(MAGMA) [2^n mod 10: n in [0..150]]; // _Vincenzo Librandi_, Apr 12 2011",
				"(Haskell)",
				"a000689 n = a000689_list !! n",
				"a000689_list = 1 : cycle [2,4,8,6]  -- _Reinhard Zumkeller_, Sep 15 2011"
			],
			"xref": [
				"Cf. A173635."
			],
			"keyword": "nonn,base,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_.",
			"references": 10,
			"revision": 67,
			"time": "2021-12-22T16:12:39-05:00",
			"created": "1998-06-14T03:00:00-04:00"
		}
	]
}