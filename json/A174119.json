{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A174119",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 174119,
			"data": "1,1,1,1,1,1,1,5,5,1,1,14,70,14,1,1,30,420,420,30,1,1,55,1650,4620,1650,55,1,1,91,5005,30030,30030,5005,91,1,1,140,12740,140140,300300,140140,12740,140,1,1,204,28560,519792,2042040,2042040,519792,28560,204,1,1,285,58140,1627920,10581480,19399380,10581480,1627920,58140,285,1",
			"name": "Triangle T(n, k) = ((n-k)/6)*binomial(n-1, k-1)*binomial(2*n, 2*k) with T(n, 0) = T(n, n) = 1, read by rows.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A174119/b174119.txt\"\u003eRows n = 0..100 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = c(n)/(c(k)*c(n-k)) where c(n) = Product_{j=2..n} j*(j-1)*(2*j-1)/6 for n \u003e 2 otherwise 1.",
				"From _G. C. Greubel_, Feb 11 2021: (Start)",
				"T(n, k) = ((n-k)/6)*binomial(n-1, k-1)*binomial(2*n, 2*k) with T(n, 0) = T(n, n) =1.",
				"Sum_{k=0..n} T(n, k) = (n*(n-1)*(2*n-1)/6)*HypergeometricPFQ[{1-n, 3/2-n, 2-n}, {3/2, 2}, -1] + 2 - [n=0] (n*(n-1)*(2*n-1)/6)*A196148[n-2] + 2 - [n=0]. (End)"
			],
			"example": [
				"Triangle begins as:",
				"  1;",
				"  1,   1;",
				"  1,   1,     1;",
				"  1,   5,     5,       1;",
				"  1,  14,    70,      14,        1;",
				"  1,  30,   420,     420,       30,        1;",
				"  1,  55,  1650,    4620,     1650,       55,        1;",
				"  1,  91,  5005,   30030,    30030,     5005,       91,       1;",
				"  1, 140, 12740,  140140,   300300,   140140,    12740,     140,     1;",
				"  1, 204, 28560,  519792,  2042040,  2042040,   519792,   28560,   204,   1;",
				"  1, 285, 58140, 1627920, 10581480, 19399380, 10581480, 1627920, 58140, 285, 1;"
			],
			"mathematica": [
				"(* First program *)",
				"c[n_]:= If[n\u003c2, 1, Product[j*(j-1)*(2*j-1)/6, {j, 2, n}]];",
				"T[n_, k_]:= c[n]/(c[k]*c[n-k]);",
				"Table[T[n, k], {n, 0, 12}, {k, 0, n}]//Flatten",
				"(* Second program *)",
				"T[n_, k_]:= If[k==0 || k==n, 1, ((n-k)/6)*Binomial[n-1, k-1]*Binomial[2*n, 2*k]];",
				"Table[T[n, k], {n, 0, 12}, {k, 0, n}]//Flatten (* _G. C. Greubel_, Feb 11 2021 *)"
			],
			"program": [
				"(Sage)",
				"def T(n,k): return 1 if (k==0 or k==n) else ((n-k)/6)*binomial(n-1, k-1)*binomial(2*n, 2*k)",
				"flatten([[T(n,k) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, Feb 11 2021",
				"(Magma)",
				"T:= func\u003c n,k | k eq 0 or k eq n select 1 else ((n-k)/6)*Binomial(n-1, k-1)*Binomial(2*n, 2*k) \u003e;",
				"[T(n,k): k in [0..n], n in [0..12]]; // _G. C. Greubel_, Feb 11 2021"
			],
			"xref": [
				"Cf. A174116, A174117, A174124, A174125.",
				"Cf. A111910, A196148."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "0,8",
			"author": "_Roger L. Bagula_, Mar 08 2010",
			"ext": [
				"Edited by _G. C. Greubel_, Feb 11 2021"
			],
			"references": 6,
			"revision": 12,
			"time": "2021-02-12T07:52:26-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}