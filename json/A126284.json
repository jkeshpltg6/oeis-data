{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A126284",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 126284,
			"data": "1,7,23,59,135,291,607,1243,2519,5075,10191,20427,40903,81859,163775,327611,655287,1310643,2621359,5242795,10485671,20971427,41942943,83885979,167772055,335544211,671088527,1342177163,2684354439",
			"name": "a(n) = 5*2^n - 4*n - 5.",
			"comment": [
				"Row sums of A125233.",
				"A triangle with left and right borders being the odd numbers 1,3,5,7,... will give the same partial sums for the sum of its rows. - _J. M. Bergot_, Sep 29 2012",
				"The triangle in the above comment is constructed the same way as Pascal's triangle, i.e., C(n, k) = C(n-1, k) + C(n-1, k-1). - _Michael B. Porter_, Oct 03 2012"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A126284/b126284.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-5,2)."
			],
			"formula": [
				"a(n) = right term of M^n * [1,0,0], where M = the 3 X 3 matrix [1,0,0; 1,1,0; 1,4,2].",
				"a(1) = 1; a(2) = 7; a(n) = 4*a(n-1) - 5*a(n-2) + 2*a(n-3), n \u003e 2.",
				"The 6th diagonal from the right of A126277.",
				"G.f.: x*(1+3*x)/(1-4*x+5*x^2-2*x^3). - _Colin Barker_, Feb 12 2012",
				"E.g.f.: 5*exp(2*x) - (5+4*x)*exp(x). - _G. C. Greubel_, Oct 23 2018"
			],
			"example": [
				"a(5) = 135 since M^5 * [1,0,0] = [1,5,135].",
				"a(6) = 291 = 4*a(5) - 5*a(4) + 2*a(3) = 4*135 - 5*59 + 2*23."
			],
			"maple": [
				"A126284:=n-\u003e5*2^n-4*n-5; seq(A126284(n), n=1..50); # _Wesley Ivan Hurt_, Mar 27 2014"
			],
			"mathematica": [
				"lst={};s=0;Do[s+=s+n;AppendTo[lst, s], {n, 1, 6!, 4}];lst (* _Vladimir Joseph Stephan Orlovsky_, Oct 18 2008 *)",
				"CoefficientList[Series[(1 + 3 x)/(1 - 4 x + 5 x^2 - 2 x^3), {x, 0, 50}], x] (* _Vincenzo Librandi_, Mar 28 2014 *)"
			],
			"program": [
				"(PARI) a(n)=5\u003c\u003cn-4*n-5 \\\\ _Charles R Greathouse IV_, Oct 03 2012",
				"(MAGMA) [5*2^n - 4*n - 5: n in [1..30]]; // _G. C. Greubel_, Oct 23 2018",
				"(GAP) List([1..30],n-\u003e5*2^n-4*n-5); # _Muniru A Asiru_, Oct 24 2018"
			],
			"xref": [
				"Cf. A000384, A125233, A126277."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Gary W. Adamson_, Dec 24 2006",
			"ext": [
				"More terms from _Vladimir Joseph Stephan Orlovsky_, Oct 18 2008",
				"New definition from _R. J. Mathar_, Sep 29 2012"
			],
			"references": 2,
			"revision": 48,
			"time": "2018-10-24T09:51:02-04:00",
			"created": "2007-01-12T03:00:00-05:00"
		}
	]
}