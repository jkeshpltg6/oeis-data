{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A285863",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 285863,
			"data": "1,-3,3,0,-27,0,243,0,-2187,0,98415,0,-122408577,0,11160261,0,-51899996619,0,5664991530321,0,-202943637014337,0,8938507796555139,0,-22252066887294301257,0,7246946747292751629,0,-181103830292539169071623",
			"name": "Numerators of Bernoulli numbers 3^n*B(n), with B(n) = A027641(n)/A027642(n).",
			"comment": [
				"The denominators are given in A285068.",
				"In general the numbers B(d;n) = d^n*B(n), for n \u003e= 0, have e.g.f. d*x/(exp(d*x) - 1). They are also the exponential convolution of the generalized Bernoulli numbers B[d,a](n), obtained from the generalized Stirling2 numbers S2[d,a], with the sequence {(-a)^n}_{n\u003e=0}. See a comment in A157817 for the  B[4,1] and B[4,3] examples.",
				"These numbers B(d;n) and their polynomials B(d;n,x) = Sum_{m=0..n} binomial(n, m)*B(d;n-m)*x^m are used in the generalized so-called Faulhaber formula for the sums of powers of arithmetic progressions defined by SP(d,a;n,m) := Sum_{j=0..m} (a + d*j)^n = Sum_{k=0..n} binomial(n, k)*a^(n-k)*d^k*SP(k,m) with SP(k,m) = SP(1,0;k,m), n \u003e= 0, m \u003e= 0, and 0^0 := 1.",
				"The Faulhaber formula is: SP(d,a;n,m) = (1/(d*(n+1)))*[B(d;n+1,x = a+d*(m+1)) - B(d;n+1,x = d) - B(d;n+1,x = a)  + B(d;n+1,x=0) + d^(n+1)*[n=0]]. Here [n=0] is the Kronecker delta_{n,0} symbol: 1 if n=0 and 0 otherwise.",
				"A simpler version of the Faulhaber formula is for a=0: SP(d,0;0,m) = m+1 and  SP(d,0;n,m) = d^n*(1/(n+1))*(B(n+1, x = m+1) - B(n+1, x=1)) for n \u003e= 1, and for a an integer \u003e= 1: Sum_{k=0..n} binomial(n, k)*a^(n-k) * d^k * (1/(k+1)) * (B(k+1, x=m+1) - B(k+1, x=1)). Here B(n, x) =  B(1;n,x) are the usual Bernoulli polynomials from A196838/A196839 or A053382/A053383."
			],
			"link": [
				"Indranil Ghosh, \u003ca href=\"/A285863/b285863.txt\"\u003eTable of n, a(n) for n = 0..300\u003c/a\u003e",
				"Wolfdieter Lang, \u003ca href=\"https://arxiv.org/abs/1707.04451\"\u003eOn Sums of Powers of Arithmetic Progressions, and Generalized Stirling, Eulerian and Bernoulli numbers\u003c/a\u003e, arXiv:1707.04451 [math.NT], 2017."
			],
			"formula": [
				"a(n) = numerator(r(n)) with r(n) = 3^n*A027641(n)/A027642(n), n \u003e= 0.",
				"E.g.f. {r(n)}_{n\u003e=0}: 3*x/(exp(3*x) - 1)."
			],
			"maple": [
				"seq(numer(3^n*bernoulli(n)), n=0..28); # _Peter Luschny_, Jul 17 2017"
			],
			"mathematica": [
				"Table[Numerator[3^n*BernoulliB[n]], {n, 0, 100}] (* _Indranil Ghosh_, Jul 18 2017 *)"
			],
			"program": [
				"(Python)",
				"from sympy import bernoulli",
				"def a(n): return (3**n * bernoulli(n)).numerator()",
				"print([a(n) for n in range(51)]) # _Indranil Ghosh_, Jul 18 2017"
			],
			"xref": [
				"Cf. A027641/A027642, A285068."
			],
			"keyword": "sign,easy,frac",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Apr 29 2017",
			"references": 4,
			"revision": 31,
			"time": "2020-05-04T14:08:39-04:00",
			"created": "2017-04-30T23:12:41-04:00"
		}
	]
}