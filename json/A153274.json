{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A153274",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 153274,
			"data": "2,6,15,24,105,280,120,945,3640,9945,720,10395,58240,208845,576576,5040,135135,1106560,5221125,17873856,49579075,40320,2027025,24344320,151412625,643458816,2131900225,5925744000,362880,34459425,608608000,4996616625,26381811456,104463111025,337767408000,939536222625",
			"name": "Triangle, read by rows, T(n,k) = k^(n+1) * Pochhammer(1/k, n+1).",
			"comment": [
				"A Pochhammer function-based triangular sequence.",
				"Row sums are: {2, 21, 409, 14650, 854776, 73920791, 8878927331, 1413788600036, 288152651134776, 73152069870215127, ...}."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A153274/b153274.txt\"\u003eRows n = 1..100 of triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = k^(n+1) * Pochmammer(1/k, n+1).",
				"T(n, k) = Product_{j=0..n} (j*k + 1). - _G. C. Greubel_, Mar 05 2020"
			],
			"example": [
				"Triangle begins as:",
				"      2;",
				"      6,      15;",
				"     24,     105,      280;",
				"    120,     945,     3640,      9945;",
				"    720,   10395,    58240,    208845,    576576;",
				"   5040,  135135,  1106560,   5221125,  17873856,   49579075;",
				"  40320, 2027025, 24344320, 151412625, 643458816, 2131900225, 5925744000;"
			],
			"maple": [
				"seq(seq( k^(n+1)*pochhammer(1/k, n+1), k=1..n), n=1..12); # _G. C. Greubel_, Mar 05 2020"
			],
			"mathematica": [
				"Table[Apply[Plus, CoefficientList[j*k^n*Pochhammer[(j+k)/k, n], j]], {n, 12}, {k,n}]//Flatten (* modified by _G. C. Greubel_, Mar 05 2020 *)",
				"Table[k^(n+1)*Pochhammer[1/k, n+1], {n,12}, {k,n}]//Flatten (* _G. C. Greubel_, Mar 05 2020 *)"
			],
			"program": [
				"(PARI) T(n, k) = prod(j=0, n, j*k+1);",
				"for(n=1, 12, for(k=1, n, print1(T(n, k), \", \"))) \\\\ _G. C. Greubel_, Mar 05 2020",
				"(MAGMA) [(\u0026*[j*k+1: j in [0..n]]): k in [1..n], n in [1..12]]; // _G. C. Greubel_, Mar 05 2020",
				"(Sage) [[k^(n+1)*rising_factorial(1/k,n+1) for k in (1..n)] for n in (1..12)] # _G. C. Greubel_, Mar 05 2020",
				"(GAP) Flat(List([1..12], n-\u003e List([1..n], k-\u003e Product([0..n], j-\u003e j*k+1 )))); # _G. C. Greubel_, Mar 05 2020"
			],
			"xref": [
				"Cf. A000142, A001147, A007559, A007696, A008548, A008542, A045754, A045755, A045756.",
				"Cf. A142589, A256268."
			],
			"keyword": "nonn,tabl",
			"offset": "1,1",
			"author": "_Roger L. Bagula_, Dec 22 2008",
			"ext": [
				"Edited by _G. C. Greubel_, Mar 05 2020"
			],
			"references": 2,
			"revision": 6,
			"time": "2020-03-05T04:57:36-05:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}