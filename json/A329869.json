{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A329869",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 329869,
			"data": "0,1,2,1,2,1,4,5,11,19,36,77,138,252,528,1072,2204,4634,9575,19732,40754",
			"name": "Number of compositions of n with runs-resistance equal to cuts-resistance minus 1.",
			"comment": [
				"A composition of n is a finite sequence of positive integers summing to n.",
				"For the operation of taking the sequence of run-lengths of a finite sequence, runs-resistance is defined to be the number of applications required to reach a singleton.",
				"For the operation of shortening all runs by 1, cuts-resistance is defined to be the number of applications required to reach an empty word."
			],
			"link": [
				"Claude Lenormand, \u003ca href=\"/A318921/a318921.pdf\"\u003eDeux transformations sur les mots\u003c/a\u003e, Preprint, 5 pages, Nov 17 2003."
			],
			"example": [
				"The a(1) = 1 through a(9) = 19 compositions:",
				"  1   2   3   4   5   6      7       8        9",
				"      11      22      33     11113   44       11115",
				"                      11112  31111   11114    12222",
				"                      21111  111211  41111    22221",
				"                             112111  111122   51111",
				"                                     111311   111222",
				"                                     113111   111411",
				"                                     211112   114111",
				"                                     221111   211113",
				"                                     1111121  222111",
				"                                     1211111  311112",
				"                                              1111131",
				"                                              1111221",
				"                                              1112112",
				"                                              1121112",
				"                                              1221111",
				"                                              1311111",
				"                                              2111211",
				"                                              2112111",
				"For example, the runs-resistance of (1221111) is 3 because we have: (1221111) -\u003e (124) -\u003e (111) -\u003e (3), while the cuts-resistance is 4 because we have: (1221111) -\u003e (2111) -\u003e (11) -\u003e (1) -\u003e (), so (1221111) is counted under a(9)."
			],
			"mathematica": [
				"runsres[q_]:=Length[NestWhileList[Length/@Split[#]\u0026,q,Length[#]\u003e1\u0026]]-1;",
				"degdep[q_]:=Length[NestWhileList[Join@@Rest/@Split[#]\u0026,q,Length[#]\u003e0\u0026]]-1;",
				"Table[Length[Select[Join@@Permutations/@IntegerPartitions[n],runsres[#]+1==degdep[#]\u0026]],{n,0,10}]"
			],
			"xref": [
				"The version for binary indices is A329866.",
				"Compositions counted by runs-resistance are A329744.",
				"Compositions counted by cuts-resistance are A329861.",
				"Cf. A003242, A098504, A114901, A242882, A318928, A319411, A319416, A319420, A319421, A329864, A329865, A329867, A329868."
			],
			"keyword": "nonn,more",
			"offset": "0,3",
			"author": "_Gus Wiseman_, Nov 23 2019",
			"references": 3,
			"revision": 4,
			"time": "2019-11-24T10:00:52-05:00",
			"created": "2019-11-24T10:00:52-05:00"
		}
	]
}