{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A076831",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 76831,
			"data": "1,1,1,1,2,1,1,3,3,1,1,4,6,4,1,1,5,10,10,5,1,1,6,16,22,16,6,1,1,7,23,43,43,23,7,1,1,8,32,77,106,77,32,8,1,1,9,43,131,240,240,131,43,9,1,1,10,56,213,516,705,516,213,56,10,1,1,11,71,333,1060,1988,1988",
			"name": "Triangle T(n,k) read by rows giving number of inequivalent binary linear [n,k] codes (n \u003e= 0, 0 \u003c= k \u003c= n).",
			"comment": [
				"\"The familiar appearance of the first few rows [...] provides a good example of the perils of too hasty extrapolation in mathematics.\" - Slepian.",
				"The difference between this triangle and the one for which it can be so easily mistaken is A250002. - _Tilman Piesk_, Nov 10 2014."
			],
			"reference": [
				"M. Wild, Enumeration of binary and ternary matroids and other applications of the Brylawski-Lucas Theorem, Preprint No. 1693, Tech. Hochschule Darmstadt, 1994"
			],
			"link": [
				"Harald Fripertinger, \u003ca href=\"http://www.mathe2.uni-bayreuth.de/frib/codes/tables.html\"\u003eIsometry Classes of Codes\u003c/a\u003e.",
				"Harald Fripertinger, \u003ca href=\"http://www.mathe2.uni-bayreuth.de/frib/codes/tables_6.html\"\u003eWnk2: Number of the isometry classes of all binary (n,k)-codes\u003c/a\u003e. [This is a rectangular array whose lower triangle contains T(n,k).]",
				"H. Fripertinger and A. Kerber, \u003ca href=\"https://doi.org/10.1007/3-540-60114-7_15\"\u003eIsometry classes of indecomposable linear codes\u003c/a\u003e. In: G. Cohen, M. Giusti, T. Mora (eds), Applied Algebra, Algebraic Algorithms and Error-Correcting Codes, 11th International Symposium, AAECC 1995, Lect. Notes Comp. Sci. 948 (1995), pp. 194-204. [Apparently, the notation for T(n,k) is W_{nk2}; see p. 197.]",
				"Petros Hadjicostas, \u003ca href=\"/A076831/a076831.txt\"\u003eGenerating function for column k=4\u003c/a\u003e.",
				"Petros Hadjicostas, \u003ca href=\"/A076831/a076831_1.txt\"\u003eGenerating function for column k=5\u003c/a\u003e.",
				"Petros Hadjicostas, \u003ca href=\"/A076831/a076831_2.txt\"\u003eGenerating function for column k=6\u003c/a\u003e.",
				"Kent E. Morrison, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL9/Morrison/morrison37.html\"\u003eInteger Sequences and Matrices Over Finite Fields\u003c/a\u003e, Journal of Integer Sequences, Vol. 9 (2006), Article 06.2.1.",
				"David Slepian, \u003ca href=\"https://archive.org/details/bstj39-5-1219\"\u003eSome further theory of group codes\u003c/a\u003e, Bell System Tech. J. 39(5) (1960), 1219-1252.",
				"David Slepian, \u003ca href=\"https://doi.org/10.1002/j.1538-7305.1960.tb03958.x\"\u003eSome further theory of group codes\u003c/a\u003e, Bell System Tech. J. 39(5) (1960), 1219-1252.",
				"Marcel Wild, \u003ca href=\"http://dx.doi.org/10.1006/eujc.1996.0026\"\u003eConsequences of the Brylawski-Lucas Theorem for binary matroids\u003c/a\u003e, European Journal of Combinatorics 17 (1996), 309-316.",
				"Marcel Wild, \u003ca href=\"http://dx.doi.org/10.1006/ffta.1999.0273\"\u003eThe asymptotic number of inequivalent binary codes and nonisomorphic binary matroids\u003c/a\u003e, Finite Fields and their Applications 6 (2000), 192-202.",
				"Marcel Wild, \u003ca href=\"https://doi.org/10.1137/S0895480104445538\"\u003eThe asymptotic number of binary codes and binary matroids\u003c/a\u003e, SIAM  J. Discrete Math. 19 (2005), 691-699. [This paper apparently corrects some errors in previous papers.]",
				"\u003ca href=\"/index/Coa#codes_binary_linear\"\u003eIndex entries for sequences related to binary linear codes\u003c/a\u003e"
			],
			"formula": [
				"From _Petros Hadjicostas_, Sep 30 2019: (Start)",
				"T(n,k) = Sum_{i = k..n} A034253(i,k) for 1 \u003c= k \u003c= n.",
				"G.f. for column k=2: -(x^3 - x - 1)*x^2/((x^2 + x + 1)*(x + 1)*(x - 1)^4).",
				"G.f. for column k=3: -(x^12 - 2*x^11 + x^10 - x^9 - x^6 + x^4 - x - 1)*x^3/((x^6 + x^5 + x^4 + x^3 + x^2 + x + 1)*(x^2 + x + 1)^2*(x^2 + 1)*(x + 1)^2*(x - 1)^8).",
				"G.f. for column k \u003e= 4: modify the Sage program below (cf. function f). It is too complicated to write it here. (See also some of the links above.)",
				"(End)"
			],
			"example": [
				"     k    0   1   2   3    4    5    6    7    8   9  10  11        sum",
				"   n",
				"   0      1                                                           1",
				"   1      1   1                                                       2",
				"   2      1   2   1                                                   4",
				"   3      1   3   3   1                                               8",
				"   4      1   4   6   4    1                                         16",
				"   5      1   5  10  10    5    1                                    32",
				"   6      1   6  16  22   16    6    1                               68",
				"   7      1   7  23  43   43   23    7    1                         148",
				"   8      1   8  32  77  106   77   32    8    1                    342",
				"   9      1   9  43 131  240  240  131   43    9   1                848",
				"  10      1  10  56 213  516  705  516  213   56  10   1           2297",
				"  11      1  11  71 333 1060 1988 1988 1060  333  71  11   1       6928"
			],
			"program": [
				"(Sage) # Fripertinger's method to find the g.f. of column k \u003e= 2 (for small k):",
				"def A076831col(k, length):",
				"    G1 = PSL(k, GF(2))",
				"    G2 = PSL(k-1, GF(2))",
				"    D1 = G1.cycle_index()",
				"    D2 = G2.cycle_index()",
				"    f1 = sum(i[1]*prod(1/(1-x^j) for j in i[0]) for i in D1)",
				"    f2 = sum(i[1]*prod(1/(1-x^j) for j in i[0]) for i in D2)",
				"    f = (f1 - f2)/(1-x)",
				"    return f.taylor(x, 0, length).list()",
				"# For instance the Taylor expansion for column k = 4 gives",
				"print(A076831col(4, 30)) # _Petros Hadjicostas_, Sep 30 2019"
			],
			"xref": [
				"Cf. A006116, A022166, A076766 (row sums).",
				"A034356 gives same table but with the k=0 column omitted.",
				"Columns include A000012 (k=0), A000027 (k=1), A034198 (k=2), A034357 (k=3), A034358 (k=4), A034359 (k=5), A034360 (k=6), A034361 (k=7), A034362 (k=8)."
			],
			"keyword": "nonn,tabl,nice",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_, Nov 21 2002",
			"references": 16,
			"revision": 56,
			"time": "2019-10-09T02:52:07-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}