{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A208760",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 208760,
			"data": "1,1,3,1,5,8,1,7,20,22,1,9,36,72,60,1,11,56,158,244,164,1,13,80,288,632,796,448,1,15,108,470,1320,2376,2528,1224,1,17,140,712,2420,5592,8544,7872,3344,1,19,176,1022,4060,11372,22368,29712,24144,9136",
			"name": "Triangle of coefficients of polynomials v(n,x) jointly generated with A208759; see the Formula section.",
			"comment": [
				"For a discussion and guide to related arrays, see A208510.",
				"Subtriangle of the triangle given by (1, 0, -1/3, 1/3, 0, 0, 0, 0, 0, 0, 0, ...) DELTA (0, 3, -1/3, -2/3, 0, 0, 0, 0, 0, 0, 0, ...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, Mar 18 2012"
			],
			"formula": [
				"u(n,x) = u(n-1,x) + 2x*v(n-1,x),",
				"v(n,x) = (x+1)*u(n-1,x) + 2x*v(n-1,x),",
				"where u(1,x)=1, v(1,x)=1.",
				"From _Philippe Deléham_, Mar 18 2012: (Start)",
				"As DELTA-triangle with 0 \u003c= k \u003c= n:",
				"G.f.: (1-2*y*x+y*x^2-2*y^2*x^2)/(1-x-2*y*x-2*y^2*x^2).",
				"T(n,k) = T(n-1,k) + 2*T(n-1,k-1) + 2*T(n-2,k-2), T(0,0) = T(1,0) = T(2,0) = 1, T(1,1) = T(2,2) = 0, T(2,1) = 3 and T(n,k) = 0 if k \u003c 0 or if k \u003e n. (End)"
			],
			"example": [
				"First five rows:",
				"  1;",
				"  1,  3;",
				"  1,  5,  8;",
				"  1,  7, 20, 22;",
				"  1,  9, 36, 72, 60;",
				"First five polynomials v(n,x):",
				"  1",
				"  1 + 3x",
				"  1 + 5x +  8x^2",
				"  1 + 7x + 20x^2 + 22x^3",
				"  1 + 9x + 36x^2 + 72x^3 + 60x^4",
				"From _Philippe Deléham_, Mar 18 2012: (Start)",
				"(1, 0, -1/3, 1/3, 0, 0, ...) DELTA (0, 3, -1/3, -2/3, 0, 0, ...) begins:",
				"  1;",
				"  1,   0;",
				"  1,   3,   0;",
				"  1,   5,   8,   0;",
				"  1,   7,  20,  22,   0;",
				"  1,   9,  36,  72,  60,   0;",
				"  1,  11,  56, 158, 244, 164,  0; (End)"
			],
			"mathematica": [
				"u[1, x_] := 1; v[1, x_] := 1; z = 16;",
				"u[n_, x_] := u[n - 1, x] + 2 x*v[n - 1, x];",
				"v[n_, x_] := (x + 1)*u[n - 1, x] + 2 x*v[n - 1, x];",
				"Table[Expand[u[n, x]], {n, 1, z/2}]",
				"Table[Expand[v[n, x]], {n, 1, z/2}]",
				"cu = Table[CoefficientList[u[n, x], x], {n, 1, z}];",
				"TableForm[cu]",
				"Flatten[%]  (* A208759 *)",
				"Table[Expand[v[n, x]], {n, 1, z}]",
				"cv = Table[CoefficientList[v[n, x], x], {n, 1, z}];",
				"TableForm[cv]",
				"Flatten[%]  (* A208760 *)"
			],
			"xref": [
				"Cf. A208759, A208510."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Clark Kimberling_, Mar 02 2012",
			"references": 3,
			"revision": 14,
			"time": "2020-01-24T03:28:06-05:00",
			"created": "2012-03-02T14:57:23-05:00"
		}
	]
}