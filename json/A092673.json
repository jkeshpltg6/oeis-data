{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A092673",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 92673,
			"data": "1,-2,-1,1,-1,2,-1,0,0,2,-1,-1,-1,2,1,0,-1,0,-1,-1,1,2,-1,0,0,2,0,-1,-1,-2,-1,0,1,2,1,0,-1,2,1,0,-1,-2,-1,-1,0,2,-1,0,0,0,1,-1,-1,0,1,0,1,2,-1,1,-1,2,0,0,1,-2,-1,-1,1,-2,-1,0,-1,2,0,-1,1,-2,-1,0,0,2,-1,1,1,2,1,0,-1,0,1,-1,1,2,1,0,-1,0,0,0,-1,-2,-1,0,-1,2",
			"name": "a(n) = moebius(n) - moebius(n/2) where moebius(n) is zero if n is not an integer.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Setting x=1 gives us phi(n) (A000010). Setting x=0 gives A092674.",
				"Apparently the Dirichlet inverse of A001511. - _R. J. Mathar_, Dec 22 2010",
				"Given A = A115359 as an infinite lower triangular matrix and B = the Mobius sequence as a vector, A092673 = A*B. - _Gary W. Adamson_, Mar 14 2011",
				"Empirical: Letting M(n) denote the n X n matrix whereby the (i,j)-entry of M(n) is Sum_{k=1..j} floor(i/k), we have that a(n) is the (n,1)-entry of the inverse of M(n). - _John M. Campbell_, Aug 30 2017",
				"John Campbell's statement is proved at the Math StackExchange link. - _Sungjin Kim_, Jul 17 2019"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A092673/b092673.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Math StackExchange, \u003ca href=\"https://math.stackexchange.com/questions/2666770/what-is-the-inverse-of-left-sum-k-1j-left-lfloor-fracik-right-rf/2670083#2670083\"\u003eWhat is the inverse of ... ?\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Let t(n) = binomial(n+1,2); s[1]=x; for i \u003e= 2, s[i] = t(i)-Sum_{j=1..i-1} s[j]*floor(i/j); a(n) = coefficient of x in s[n]. - _Jon Perry_",
				"a(n) is multiplicative with a(2)= -2, a(4)= 1, a(2^e)= 0 if e\u003e2. a(p)= -1, a(p^e)= 0 if e\u003e1, p\u003e2. - _Michael Somos_, Mar 26 2007",
				"a(8*n)= 0. a(2*n + 1) = moebius(2*n + 1). a(2*n) = moebius(2*n) - moebius(n). - _Michael Somos_, Mar 26 2007",
				"|a(n)| \u003c= 2.",
				"1 / (1 + x) = Product_{k\u003e0} f(-x^k)^a(k) where f() is a Ramanujan theta function. - _Michael Somos_, Mar 26 2007",
				"Dirichlet g.f.: (1-2^(-s))/zeta(s). - _Ralf Stephan_, Mar 24 2015"
			],
			"example": [
				"The first few s[n] are:",
				"x, -2*x + 3, -x + 3, x + 1, -x + 5, 2*x, -x + 7, 4, 6, 2*x + 2, -x + 11, -x + 5, -x + 13, 2*x + 4, x + 7, 8, -x + 17, 6, -x + 19, -x + 9, x + 11, 2*x + 8, -x + 23, 8, 20, 2*x + 10, 18, -x + 13, -x + 29, -2*x + 10, -x + 31, 16, x + 19.",
				"x - 2*x^2 - x^3 + x^4 - x^5 + 2*x^6 - x^7 + 2*x^10 - x^11 +..."
			],
			"maple": [
				"A092673:= proc(n) if n::odd then numtheory:-mobius(n) else numtheory:-mobius(n) - numtheory:-mobius(n/2) fi end proc:",
				"map(A092673, [$1..100]); # _Robert Israel_, Dec 31 2015"
			],
			"mathematica": [
				"f[n_] := MoebiusMu[n] - If[OddQ@n, 0, MoebiusMu[n/2]]; Array[f, 105] (* _Robert G. Wilson v_ *)"
			],
			"program": [
				"(PARI) s=vector(2000); t(n)=binomial(n+1,2); s[1]=x; for(i=2,2000, s[i]=t(i)-sum(j=1,i-1, s[j]*floor(i/j))); for(i=1,2000,print1(\",\"polcoeff(s[i],1)))",
				"(PARI) {a(n) = if( n\u003c1, 0, moebius(n) - if( n%2, 0, moebius(n/2)))} /* _Michael Somos_, Mar 26 2007 */",
				"(PARI) {a(n) = local(A, B, m); if( n\u003c1, 0, A = x * O(x^n); B = 1 + x + A; for( k=1, n, B *= eta(x^k + A)^( m = polcoeff(B, k))); m)} /* _Michael Somos_, Mar 26 2007 */",
				"(PARI) a(n)=my(o=valuation(n%8,2)); if(o==0, moebius(n), if(o==1, 2*moebius(n), if(o==2, moebius(n/4), 0))) \\\\ _Charles R Greathouse IV_, Feb 07 2013"
			],
			"xref": [
				"Cf. A008683 (moebius(n)), A092674, A115359."
			],
			"keyword": "sign,mult",
			"offset": "1,2",
			"author": "_Jon Perry_, Mar 02 2004",
			"references": 15,
			"revision": 48,
			"time": "2021-03-12T22:24:42-05:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}