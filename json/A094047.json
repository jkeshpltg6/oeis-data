{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A094047",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 94047,
			"data": "0,0,2,12,312,9600,416880,23879520,1749363840,159591720960,17747520940800,2363738855385600,371511874881100800,68045361697964851200,14367543450324474009600,3464541314885011705344000",
			"name": "Number of seating arrangements of n couples around a round table (up to rotations) so that each person sits between two people of the opposite sex and no couple is seated together.",
			"comment": [
				"Also, the number of Hamiltonian directed circuits in the crown graph of order n.",
				"Or the number of those 3 X n Latin rectangles (cf. A000186) the second row of which is a full cycle. - _Vladimir Shevelev_, Mar 22 2010"
			],
			"reference": [
				"V. S. Shevelev, Reduced Latin rectangles and square matrices with equal row and column sums, Diskr.Mat.(J. of the Akademy of Sciences of Russia) 4(1992),91-110."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A094047/b094047.txt\"\u003eTable of n, a(n) for n = 1..253\u003c/a\u003e",
				"M. A. Alekseyev, Weighted de Bruijn Graphs for the Menage Problem and Its Generalizations. Lecture Notes in Computer Science 9843 (2016), 151-162. doi:\u003ca href=\"http://doi.org/10.1007/978-3-319-44543-4_12\"\u003e10.1007/978-3-319-44543-4_12\u003c/a\u003e,  arXiv:\u003ca href=\"http://arxiv.org/abs/1510.07926\"\u003e1510.07926\u003c/a\u003e, [math.CO], 2015-2016.",
				"H. M. Taylor, \u003ca href=\"/A000179/a000179.pdf\"\u003eA problem on arrangements\u003c/a\u003e, Mess. Math., 32 (1902), 60ff. [Annotated scanned copy]",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CrownGraph.html\"\u003eCrown Graph\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/HamiltonianCycle.html\"\u003eHamiltonian Cycle\u003c/a\u003e"
			],
			"formula": [
				"For n\u003e1, a(n) = (-1)^n * 2 * (n-1)! + n! * Sum_{j=0..n-1} (-1)^j * (n-j-1)! * binomial(2*n-j-1,j). - _Max Alekseyev_, Feb 10 2008",
				"a(n) = A059375(n) / (2*n) = A000179(n) * (n-1)!.",
				"Conjecture: a(n) +(-n^2+2*n-3)*a(n-1) -(n-2)*(n^2-3*n+5)*a(n-2) -3*(n-2)*(n-3)*a(n-3) +(n-2)*(n-3)*(n-4)*a(n-4)=0. - _R. J. Mathar_, Nov 02 2015",
				"Conjecture: (-n+2)*a(n) +(n-1)*(n^2-3*n+3)*a(n-1) +(n-2)*(n-1)*(n^2-3*n+3)*a(n-2) +(n-2)*(n-3)*(n-1)^2*a(n-3)=0. - _R. J. Mathar_, Nov 02 2015",
				"a(n) = (n-1) * (n * (a(n-1) + a(n-2)) - 4 * (-1)^n * (n-3)!) for n \u003e 3. - _Seiichi Manyama_, Jan 18 2020"
			],
			"maple": [
				"A094047 := proc(n)",
				"    if n \u003c 3 then",
				"        0;",
				"    else",
				"        (-1)^n*2*(n-1)!+n!*add( (-1)^j*(n-j-1)!*binomial(2*n-j-1,j),j=0..n-1) ;",
				"    end if;",
				"end proc: # _R. J. Mathar_, Nov 02 2015"
			],
			"mathematica": [
				"Join[{0},Table[(-1)^n 2(n-1)!+n!Sum[(-1)^j (n-j-1)!Binomial[2n-j-1,j],{j,0,n-1}],{n,2,20}]] (* _Harvey P. Dale_, Mar 07 2012 *)"
			],
			"xref": [
				"Cf. A059375 (rotations are counted as different)",
				"Cf. A000179, A114939, A137729."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Matthijs Coster_, Apr 29 2004",
			"ext": [
				"Better definition from _Joel B. Lewis_, Jun 30 2007",
				"Formula and further terms from _Max Alekseyev_, Feb 10 2008"
			],
			"references": 19,
			"revision": 50,
			"time": "2020-01-18T08:48:49-05:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}