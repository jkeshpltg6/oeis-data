{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A262785",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 262785,
			"data": "1,1,3,2,3,2,3,3,1,3,5,3,2,3,3,4,2,2,6,4,4,2,6,2,2,5,2,7,4,4,4,5,3,1,7,2,5,4,4,5,4,3,3,5,1,6,4,3,1,3,5,3,8,2,7,6,3,2,4,5,3,4,2,6,5,4,5,9,2,3,9,1,5,5,7,4,3,5,5,7,3,5,7,5,3,8,4,7,4,2,9,7,6,2,9,6,1,3,3,9",
			"name": "Number of ordered ways to write n as x^2 + y^2 + p*(p+d)/2, where 0 \u003c= x \u003c= y, d is 1 or -1, and p is prime.",
			"comment": [
				"Conjecture: (i) a(n) \u003e 0 for all n \u003e 0, and a(n) = 1 only for n = 1, 2, 9, 34, 45, 49, 72, 97, 241, 337, 538.",
				"(ii) Any integer n \u003e 9 can be written as x^2 + y^2 + z*(z+1), where x,y,z are nonnegative integers with z-1 or z+1 prime.",
				"In 2015, the author refined a result of Euler by proving that any positive integer can be written as the sum of two squares and a positive triangular number."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A262785/b262785.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://dx.doi.org/10.4064/aa127-2-1\"\u003eMixed sums of squares and triangular numbers\u003c/a\u003e, Acta Arith. 127(2007), 103-113.",
				"Zhi-Wei Sun, \u003ca href=\"http://oeis.org/abs/1502.03056\"\u003eOn universal sums ax^2+by^2+f(z), aT_x+bT_y+f(z) and aT_x+by^2+f(z)\u003c/a\u003e, arXiv:1502.03056 [math.NT], 2015."
			],
			"example": [
				"a(1) = 1 since 1 = 0^2 + 0^2 + 2*(2-1)/2 with 2 prime.",
				"a(2) = 1 since 2 = 0^2 + 1^2 + 2*(2-1)/2 with 2 prime.",
				"a(3) = 3 since 3 = 0^2 + 0^2 + 2*(2+1)/2 = 0^2 + 0^2 + 3*(3-1)/2 = 1^2 + 1^2 + 2*(2-1)/2 with 2 and 3 both prime.",
				"a(9) = 1 since 9 = 2^2 + 2^2 + 2*(2-1)/2 with 2 prime.",
				"a(34) = 1 since 34 = 2^2 + 3^2 + 7*(7-1)/2 with 7 prime.",
				"a(45) = 1 since 45 = 1^2 + 4^2 + 7*(7+1)/2 with 7 prime.",
				"a(49) = 1 since 49 = 3^2 + 5^2 + 5*(5+1)/2 with 5 prime.",
				"a(72) = 1 since 72 = 1^2 + 4^2 + 11*(11-1)/2 with 11 prime.",
				"a(97) = 1 since 97 = 1^2 + 9^2 + 5(5+1)/2 with 5 prime.",
				"a(241) = 1 since 241 = 1^2 + 15^2 + 5*(5+1)/2 with 5 prime.",
				"a(337) = 1 since 337 = 5^2 + 6^2 + 23*(23+1)/2 with 23 prime.",
				"a(538) = 1 since 538 = 3^2 + 8^2 + 31*(31-1)/2 with 31 prime."
			],
			"mathematica": [
				"SQ[n_]:=IntegerQ[Sqrt[n]]",
				"f[d_,n_]:=Prime[n](Prime[n]+(-1)^d)/2",
				"Do[r=0;Do[If[SQ[n-f[d,k]-x^2],r=r+1],{d,0,1},{k,1,PrimePi[(Sqrt[8n+1]-(-1)^d)/2]},{x,0,Sqrt[(n-f[d,k])/2]}];Print[n,\" \",r];Continue,{n,1,100}]"
			],
			"xref": [
				"Cf. A000040, A000290, A000217, A001481, A254885, A262311, A262781."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Zhi-Wei Sun_, Oct 01 2015",
			"references": 7,
			"revision": 8,
			"time": "2015-10-02T03:38:08-04:00",
			"created": "2015-10-02T03:38:08-04:00"
		}
	]
}