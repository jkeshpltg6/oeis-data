{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A322226",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 322226,
			"data": "1,1,-2,-14,36,736,-1819,-88227,163185,19160769,-15294993,-6611719113,-4120456053,3331391605595,7909978546731,-2311176691053557,-10230565685787877,2114443392338548619,14290732101459857168,-2468208492961535459212,-23089123066195736850322,3581650102772343613724618,43704098963536055443651815,-6326399008631824968253597825,-96742680150222152446019734205",
			"name": "a(n) = A322227(n) / (n*(n+1)/2), where A322227(n) = [x^(n-1)] Product_{k=1..n} (k + x - k*x^2), for n \u003e= 1.",
			"link": [
				"Paul D. Hanna, \u003ca href=\"/A322226/b322226.txt\"\u003eTable of n, a(n) for n = 1..301\u003c/a\u003e"
			],
			"example": [
				"The irregular triangle A322225 formed from coefficients of x^k in Product_{m=1..n} (m + x - m*x^2), for n \u003e= 0, k = 0..2*n, begins",
				"1;",
				"1, 1, -1;",
				"2, 3, -3, -3, 2;",
				"6, 11, -12, -21, 12, 11, -6;",
				"24, 50, -61, -140, 75, 140, -61, -50, 24;",
				"120, 274, -375, -1011, 540, 1475, -540, -1011, 375, 274, -120;",
				"720, 1764, -2696, -8085, 4479, 15456, -5005, -15456, 4479, 8085, -2696, -1764, 720;",
				"5040, 13068, -22148, -71639, 42140, 169266, -50932, -221389, 50932, 169266, -42140, -71639, 22148, 13068, -5040; ...",
				"in which the central terms equal A322228.",
				"RELATED SEQUENCES.",
				"Note that the terms in the secondary diagonal A322227 in the above triangle",
				"[1, 3, -12, -140, 540, 15456, -50932, -3176172, 7343325, 1053842295, ...]",
				"may be divided by triangular numbers to obtain this sequence",
				"[1, 1, -2, -14, 36, 736, -1819, -88227, 163185, 19160769, -15294993, ...]."
			],
			"mathematica": [
				"a[n_] := SeriesCoefficient[Product[k + x - k x^2, {k, 1, n}], {x, 0, n - 1}]/(n (n + 1)/2);",
				"Array[a, 25] (* _Jean-François Alcover_, Dec 29 2018 *)"
			],
			"program": [
				"(PARI) {A322225(n, k) = polcoeff( prod(m=1, n, m + x - m*x^2) +x*O(x^k), k)}",
				"/* Print the irregular triangle */",
				"for(n=0, 10, for(k=0, 2*n, print1( A322225(n, k), \", \")); print(\"\"))",
				"/* Print this sequence */",
				"for(n=1, 30, print1( A322225(n, n-1)/(n*(n+1)/2), \", \"))"
			],
			"xref": [
				"Cf. A322227, A322228.",
				"Cf. A322236 (variant), A322894 (variant)."
			],
			"keyword": "sign",
			"offset": "1,3",
			"author": "_Paul D. Hanna_, Dec 15 2018",
			"references": 5,
			"revision": 14,
			"time": "2019-01-05T02:28:20-05:00",
			"created": "2018-12-15T19:33:24-05:00"
		}
	]
}