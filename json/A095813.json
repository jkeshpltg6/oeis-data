{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A095813",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 95813,
			"data": "1,-1,0,-1,1,4,-4,-1,-3,3,12,-12,-2,-8,8,31,-30,-5,-20,19,72,-68,-12,-44,41,154,-144,-24,-90,84,312,-289,-48,-178,164,603,-554,-92,-336,307,1122,-1024,-168,-612,557,2024,-1836,-300,-1087,983,3552,-3206,-522,-1880,1692,6088,-5472,-886,-3180",
			"name": "Expansion of q * chi(-q) / chi(-q^5)^5 in powers of q where chi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A095813/b095813.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of (1 - (phi(-q) / phi(-q^5))^2) / 4 in powers of q where  phi() is a Ramanujan theta function.",
				"Expansion of (eta(q) * eta(q^10)^5) / (eta(q^2) * eta(q^5)^5) in powers of q.",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2)) where f(u, v) = u^2 - v + 2*u*v + 4*u*v^2.",
				"G.f. A(x) satisfies A(x^2) = -A(x) * A(-x).",
				"Euler transform of period 10 sequence [ -1, 0, -1, 0, 4, 0, -1, 0, -1, 0, ...].",
				"G.f.: x * (Product_{k\u003e=1} ((1 - x^k) * (1-x^(10*k))^5) / ((1 - x^(2*k)) * (1 - x^(5*k))^5)).",
				"a(n) = A138522(n) unless n = 0. Convolution inverse is A132980.",
				"Empirical: Sum_{n\u003e=1} a(n)/exp(Pi*n) = -13/8 - (5/8)*sqrt(5) + (5/8)*sqrt(10 + 6*sqrt(5)). - _Simon Plouffe_, Mar 01 2021"
			],
			"example": [
				"q - q^2 - q^4 + q^5 + 4*q^6 - 4*q^7 - q^8 - 3*q^9 + 3*q^10 + 12*q^11 + ..."
			],
			"program": [
				"(PARI) {a(n) = local(A); if( n\u003c1, 0, n--; A = x * O(x^n); polcoeff( eta(x + A) * eta(x^10 + A)^5 / (eta(x^2 + A) * eta(x^5 + A)^5), n))}"
			],
			"xref": [
				"Cf. A132980, A138522."
			],
			"keyword": "sign",
			"offset": "1,6",
			"author": "_Michael Somos_, Jun 07 2004",
			"references": 6,
			"revision": 24,
			"time": "2021-03-11T18:18:26-05:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}