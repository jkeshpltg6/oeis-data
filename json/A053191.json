{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A053191",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 53191,
			"data": "1,4,18,32,100,72,294,256,486,400,1210,576,2028,1176,1800,2048,4624,1944,6498,3200,5292,4840,11638,4608,12500,8112,13122,9408,23548,7200,28830,16384,21780,18496,29400,15552,49284,25992,36504,25600,67240",
			"name": "a(n) = n^2 * phi(n).",
			"comment": [
				"Number of invertible 2 X 2 symmetric matrices over Z(n). - _T. D. Noe_, Jan 13 2006",
				"Note that A115077 gives the number of 2 X 2 symmetric matrices having nonzero determinant. However, for composite n, a nonzero determinant is not sufficient for the matrix to be invertible; the determinant must also be relatively prime to n. - _T. D. Noe_, Jan 13 2006",
				"Also Euler phi function of n^3.",
				"For n^k, EulerPhi(n^k) = n^(k-1)*EulerPhi(n). The same holds if Phi is replaced by the cototient function.",
				"Also, the sum of the degrees of the irreducible representations of the group GL(2,Z_n) (sequence A000252). - Sharon Sela (sharonsela(AT)hotmail.com), Feb 06 2002"
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A053191/b053191.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = n^2 * phi(n) = A000010(n^3).",
				"Dirichlet g.f.: zeta(s-3)/zeta(s-2). - _R. J. Mathar_, Feb 09 2011",
				"The n-th term of the Dirichlet inverse is n^2 * A023900(n) = (-1)^omega(n) * a(n) / A003557(n), where omega = A001221. - _Álvar Ibeas_, Nov 24 2017",
				"Sum_{n\u003e=1} 1/a(n) = Product_{p prime} (1 + p/(p^4 - p^3 - p + 1)) = 1.38097852211302096879... - _Amiram Eldar_, Dec 06 2020"
			],
			"example": [
				"n=5: n^3 = 125, EulerPhi(125) = 125 - 25 = 100."
			],
			"maple": [
				"with(numtheory):a:=n-\u003ephi(n^3): seq(a(n), n=1..41); # _Zerinvary Lajos_, Oct 07 2007"
			],
			"mathematica": [
				"Table[cnt=0; Do[m={{a, b}, {b, c}}; If[Det[m, Modulus-\u003en]\u003e0 \u0026\u0026 MatrixQ[Inverse[m, Modulus-\u003en]], cnt++ ], {a, 0, n-1}, {b, 0, n-1}, {c, 0, n-1}]; cnt, {n, 2, 50}] (* _T. D. Noe_, Jan 13 2006 *)",
				"Table[n^2*EulerPhi[n],{n,1,40}] (* _Vladimir Joseph Stephan Orlovsky_, Nov 10 2009 *)"
			],
			"program": [
				"(Sage) [n^2*euler_phi(n) for n in range(1, 42)] # _Zerinvary Lajos_, Jun 06 2009",
				"(MAGMA) [ n^2*EulerPhi(n) : n in [1..100] ]; // _Vincenzo Librandi_, Apr 21 2011",
				"(PARI) a(n) = n^2*eulerphi(n); \\\\ _Michel Marcus_, Oct 31 2017"
			],
			"xref": [
				"Cf. A000252 (number of invertible 2 X 2 matrices over Z(n)), A115075, A115076, A115077.",
				"Cf. A000010, A051953, A002618, A053650, A053192, A001248, A319087."
			],
			"keyword": "nonn,mult",
			"offset": "1,2",
			"author": "_Labos Elemer_, Mar 02 2000",
			"ext": [
				"Edited by _N. J. A. Sloane_ at the suggestion of _Andrew S. Plewe_, Jun 05 2007"
			],
			"references": 25,
			"revision": 50,
			"time": "2021-08-20T07:13:06-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}