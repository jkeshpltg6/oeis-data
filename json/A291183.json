{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A291183",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 291183,
			"data": "4,22,116,608,3180,16618,86812,453440,2368292,12369174,64601428,337397536,1762142540,9203221994,48066074172,251036784256,1311100720708,6847542588950,35762957380148,186780746599392,975507894703660,5094827328491242,26608975328086364",
			"name": "p-INVERT of the positive integers, where p(S) = 1 - 4*S + 2*S^2.",
			"comment": [
				"Suppose s = (c(0), c(1), c(2), ...) is a sequence and p(S) is a polynomial. Let S(x) = c(0)*x + c(1)*x^2 + c(2)*x^3 + ... and T(x) = (-p(0) + 1/p(S(x)))/x. The p-INVERT of s is the sequence t(s) of coefficients in the Maclaurin series for T(x). Taking p(S) = 1 - S gives the \"INVERT\" transform of s, so that p-INVERT is a generalization of the \"INVERT\" transform (e.g., A033453).",
				"See A290890 for a guide to related sequences."
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A291183/b291183.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (8,-16,8,-1)"
			],
			"formula": [
				"G.f.: (2 (2 - 5 x + 2 x^2))/(1 - 8 x + 16 x^2 - 8 x^3 + x^4).",
				"a(n) = 8*a(n-1) - 16*a(n-2) + 8*a(n-3) - a(n-4)."
			],
			"mathematica": [
				"z = 60; s = x/(1 - x)^2; p = 1 - 4 s + 2 s^2;",
				"Drop[CoefficientList[Series[s, {x, 0, z}], x], 1] (* A000027 *)",
				"Drop[CoefficientList[Series[1/p, {x, 0, z}], x], 1]  (* A291183 *)",
				"LinearRecurrence[{8, -16, 8, -1}, {4, 22, 116, 608}, 40] (* _Vincenzo Librandi_, Aug 20 2017 *)"
			],
			"program": [
				"(MAGMA) I:=[4,22,116,608]; [n le 4 select I[n] else 8*Self(n-1)-16*Self(n-2)+8*Self(n-3)-Self(n-4): n in [1..40]]; // _Vincenzo Librandi_, Aug 20 2017"
			],
			"xref": [
				"Cf. A000027, A290890."
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "_Clark Kimberling_, Aug 19 2017",
			"references": 2,
			"revision": 10,
			"time": "2017-08-21T13:05:12-04:00",
			"created": "2017-08-21T13:05:12-04:00"
		}
	]
}