{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A103371",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 103371,
			"data": "1,2,1,3,6,1,4,18,12,1,5,40,60,20,1,6,75,200,150,30,1,7,126,525,700,315,42,1,8,196,1176,2450,1960,588,56,1,9,288,2352,7056,8820,4704,1008,72,1,10,405,4320,17640,31752,26460,10080,1620,90,1,11,550,7425,39600,97020",
			"name": "Number triangle T(n,k) = C(n,n-k)*C(n+1,n-k).",
			"comment": [
				"Columns include A000027, A002411, A004302, A108647, A134287. Row sums are C(2n+1,n+1) or A001700.",
				"T(n-1,k-1) is the number of ways to put n identical objects into k of altogether n distinguishable boxes. See the partition array A035206 from which this triangle arises after summing over all entries related to partitions with fixed part number k.",
				"T(n, k) is also the number of order-preserving full transformations (of an n-chain) of height k (height(alpha) = |Im(alpha)|). - _Abdullahi Umar_, Oct 02 2008",
				"The o.g.f. of the (n+1)-th diagonal is given by G(n, x) = (n+1)*Sum_{k=1..n} A001263(n, k)*x^(k-1) / (1 - x)^(2*n+1), for n \u003e= 1 and for n = 0 it is G(0, x) = 1/(1-x). - _Wolfdieter Lang_, Jul 31 2017"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A103371/b103371.txt\"\u003eRows n = 0..125 of table, flattened\u003c/a\u003e",
				"Per Alexandersson, Svante Linusson, Samu Potka, and Joakim Uhlin, \u003ca href=\"https://arxiv.org/abs/2010.11157\"\u003eRefined Catalan and Narayana cyclic sieving\u003c/a\u003e, arXiv:2010.11157 [math.CO], 2020.",
				"P. Barry and A. Hennessy, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL14/Barry2/barry126.html\"\u003eA Note on Narayana Triangles and Related Polynomials, Riordan Arrays, and MIMO Capacity Calculations \u003c/a\u003e, J. Int. Seq. 14 (2011) # 11.3.8",
				"R. Cori and G. Hetyei, \u003ca href=\"http://arxiv.org/abs/1306.4628\"\u003eCounting genus one partitions and permutations\u003c/a\u003e, arXiv:1306.4628 [math.CO], 2013.",
				"Wolfdieter Lang, \u003ca href=\"https://arxiv.org/abs/1708.01421\"\u003eOn Generating functions of Diagonals Sequences of Sheffer and Riordan Number Triangles\u003c/a\u003e, arXiv:1708.01421 [math.NT], August 2017.",
				"A. Laradji and A. Umar, \u003ca href=\"http://dx.doi.org/10.1007/s00233-005-0553-6\"\u003eCombinatorial results for semigroups of order-preserving full transformations\u003c/a\u003e, Semigroup Forum 72 (2006), 51-62."
			],
			"formula": [
				"Number triangle T(n, k) = C(n, n-k)*C(n+1, n-k) = C(n, k)*C(n+1, k+1); Column k of this triangle has g.f. Sum_{j=0..k} (C(k, j)*C(k+1, j) * x^(k+j))/(1-x)^(2*k+2); coefficients of the numerators are the rows of the reverse triangle C(n, k)*C(n+1, k).",
				"T(n,k) = C(n, k)*Sum_{j=0..(n-k)} C(n-j, k). - _Paul Barry_, Jan 12 2006",
				"T(n,k) = (n+1-k)*N(n+1,k+1), with N(n,k):=A001263(n,k), the Narayana triangle (with offset [1,1)]",
				"O.g.f.: ((1-(1-y)*x)/sqrt((1-(1+y)*x)^2-4*x^2*y) -1)/2, (from o.g.f. of A001263, Narayana triangle). _Wolfdieter Lang_, Nov 13 2007.",
				"From _Peter Bala_, Jan 24 2008: (Start)",
				"Matrix product of A007318 and A122899.",
				"O.g.f. for row n: (1-x)^n*P(n,1,0,(1+x)/(1-x)) = 1/(2*x)*(1-x)^(n+1)*( Legendre_P(n+1,(1+x)/(1-x)) - Legendre_P(n,(1+x)/(1-x)) ), where P(n,a,b,x) denotes the Jacobi polynomial.",
				"O.g.f. for column k: x^k/(1-x)^(k+2)*P(k,0,1,(1+x)/(1-x)). Compare with A008459. (End)",
				"Let S(n,k) = binomial(2*n,n)^(k+1)*((n+1)^(k+1)-n^(k+1))/(n+1)^k. Then T(2*n,n) = S(n,1). (Cf. A194595, A197653, A197654). - _Peter Luschny_, Oct 20 2011",
				"T(n,k) = A003056(n+1,k+1)*C(n,k)^2/(k+1). - _Peter Luschny_, Oct 29 2011",
				"T(n, k) = A007318(n, k)*A135278(n, k), n \u003e= k \u003e= 0. - _Wolfdieter Lang_, Jul 31 2017"
			],
			"example": [
				"The triangle T(n, k) begins:",
				"n\\k  0   1    2     3     4     5     6    7  8 9 ...",
				"0:   1",
				"1:   2   1",
				"2:   3   6    1",
				"3:   4  18   12     1",
				"4:   5  40   60    20     1",
				"5:   6  75  200   150    30     1",
				"6:   7 126  525   700   315    42     1",
				"7:   8 196 1176  2450  1960   588    56    1",
				"8:   9 288 2352  7056  8820  4704  1008   72  1",
				"9:  10 405 4320 17640 31752 26460 10080 1620 90 1",
				"...  reformatted. - _Wolfdieter Lang_, Jul 31 2017",
				"From _R. J. Mathar_, Mar 29 2013: (Start)",
				"The matrix inverse starts",
				"       1;",
				"      -2,       1;",
				"       9,      -6,      1;",
				"     -76,      54,    -12,      1;",
				"    1055,    -760,    180,    -20,   1;",
				"  -21906,   15825,  -3800,    450, -30,   1;",
				"  636447, -460026, 110775, -13300, 945, -42, 1; (End)",
				"O.g.f. of 4th diagonal [4, 40,200, ...] is G(3, x) = 4*(1 + 3*x + x^2)/(1 - x)^7, from the n = 3 row [1, 3, 1] of A001263. See a comment above. - _Wolfdieter Lang_, Jul 31 2017"
			],
			"maple": [
				"A103371 := (n,k) -\u003e binomial(n,k)^2*(n+1)/(k+1);",
				"seq(print(seq(A103371(n, k), k=0..n)), n=0..7); # _Peter Luschny_, Oct 19 2011"
			],
			"mathematica": [
				"Flatten[Table[Binomial[n,n-k]Binomial[n+1,n-k],{n,0,10},{k,0,n}]] (* _Harvey P. Dale_, May 26 2014 *)"
			],
			"program": [
				"(Maxima) create_list(binomial(n,k)*binomial(n+1,k+1),n,0,12,k,0,n); /* _Emanuele Munarini_, Mar 11 2011 */",
				"(Haskell)",
				"a103371 n k = a103371_tabl !! n !! k",
				"a103371_row n = a103371_tabl !! n",
				"a103371_tabl = map reverse a132813_tabl",
				"-- _Reinhard Zumkeller_, Apr 04 2014",
				"(MAGMA) /* As triangle */ [[Binomial(n,n-k)*Binomial(n+1,n-k): k in [0..n]]: n in [0.. 15]]; // _Vincenzo Librandi_, Aug 01 2017",
				"(PARI) for(n=0,10, for(k=0,n, print1(binomial(n,k)*binomial(n+1,k+1), \", \"))) \\\\ _G. C. Greubel_, Nov 09 2018"
			],
			"xref": [
				"Cf. A008459, A122899, A194595, A197653.",
				"Cf. A007318, A000894 (central terms), A132813 (mirrored).",
				"Cf. A000027, A002411, A004302, A108647, A134287, A135278, A001263."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "0,2",
			"author": "_Paul Barry_, Feb 03 2005",
			"references": 24,
			"revision": 76,
			"time": "2021-08-10T11:10:02-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}