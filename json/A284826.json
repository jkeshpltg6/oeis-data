{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A284826",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 284826,
			"data": "1,0,0,1,0,1,0,3,1,0,2,1,0,7,6,1,0,6,6,1,0,14,25,10,1,0,12,24,10,1,0,31,90,65,15,1,0,27,89,65,15,1,0,63,301,350,140,21,1,0,56,295,349,140,21,1,0,123,965,1701,1050,266,28,1",
			"name": "Irregular triangle T(n,k) for 1 \u003c= k \u003c= (n+1)/2: T(n,k) = number of primitive (aperiodic) palindromic structures of length n using exactly k different symbols.",
			"comment": [
				"Permuting the symbols will not change the structure."
			],
			"reference": [
				"M. R. Nester (1999). Mathematical investigations of some plant interaction designs. PhD Thesis. University of Queensland, Brisbane, Australia. [See A056391 for pdf file of Chap. 2]"
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A284826/b284826.txt\"\u003eTable of n, a(n) for n = 1..930\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = (Sum_{j=0..k} (-1)^j * binomial(k, j) * A284823(n, k-j)) / k!.",
				"T(n, k) = Sum_{d | n} mu(n/d) * stirling2(ceiling(d/2), k)."
			],
			"example": [
				"Triangle starts:",
				"1",
				"0",
				"0   1",
				"0   1",
				"0   3    1",
				"0   2    1",
				"0   7    6     1",
				"0   6    6     1",
				"0  14   25    10     1",
				"0  12   24    10     1",
				"0  31   90    65    15     1",
				"0  27   89    65    15     1",
				"0  63  301   350   140    21    1",
				"0  56  295   349   140    21    1",
				"0 123  965  1701  1050   266   28   1",
				"0 120  960  1700  1050   266   28   1",
				"0 255 3025  7770  6951  2646  462  36  1",
				"0 238 2999  7760  6950  2646  462  36  1",
				"0 511 9330 34105 42525 22827 5880 750 45 1",
				"0 495 9305 34095 42524 22827 5880 750 45 1",
				"--------------------------------------------",
				"For n=5, structures with 2 symbols are aabaa, ababa and abbba, so T(5,2) = 3.",
				"For n=6, structures with 2 symbols are aabbaa and abbbba, so T(6,2) = 2.",
				"(In this case, the structure abaaba is excluded because it is not primitive.)"
			],
			"mathematica": [
				"T[n_, k_] := DivisorSum[n, MoebiusMu[n/#]*StirlingS2[Ceiling[#/2], k]\u0026];",
				"Table[T[n, k], {n, 1, 15}, {k, 1, Floor[(n+1)/2]}] // Flatten (* _Jean-François Alcover_, Jun 12 2017, from 2nd formula *)"
			],
			"program": [
				"(PARI)",
				"b(n,k) = sumdiv(n,d, moebius(n/d) * k^(ceil(d/2)));",
				"a(n,k) = sum(j=0,k, b(n,k-j)*binomial(k,j)*(-1)^j)/k!;",
				"for(n=1, 20, for(k=1, ceil(n/2), print1( a(n,k),\", \");); print(););"
			],
			"xref": [
				"Columns 2-6 are A056481, A056482, A056483, A056484, A056485.",
				"Partial row sums include A056476, A056477, A056478, A056479, A056480.",
				"Row sums are A284841.",
				"Cf. A284823."
			],
			"keyword": "nonn,tabf",
			"offset": "1,8",
			"author": "_Andrew Howroyd_, Apr 03 2017",
			"references": 16,
			"revision": 28,
			"time": "2017-06-12T08:08:48-04:00",
			"created": "2017-04-03T20:34:37-04:00"
		}
	]
}