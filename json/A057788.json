{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A057788",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 57788,
			"data": "1,13,90,442,1729,5733,16744,44200,107406,243542,520676,1058148,2057510,3848222,6953544,12183560,20764055,34512075,56071470,89224590,139299615,213696795,322561200,479634480,703323660,1018031196,1455797448,2058314440,2879378332",
			"name": "Expansion of (1+x)/(1-x)^12.",
			"comment": [
				"1/2^10 of twelfth unsigned column of triangle A053120 (T-Chebyshev, rising powers, zeros omitted).",
				"If a 2-set Y and an (n-3)-set Z are disjoint subsets of an n-set X then a(n-12) is the number of 12-subsets of X intersecting both Y and Z. - _Milan Janjic_, Sep 08 2007",
				"11-dimensional square numbers, tenth partial sums of binomial transform of [1,2,0,0,0,...]. a(n) = sum_{i=0..n} C(n+10,i+10)*b(i), where b(i)=[1,2,0,0,0,...]. - Borislav St. Borisov (b.st.borisov(AT)abv.bg), Mar 05 2009",
				"2*a(n) is number of ways to place 10 queens on an (n+10) X (n+10) chessboard so that they diagonally attack each other exactly 45 times. The maximal possible attack number, p=binomial(k,2) =45 for k=10 queens, is achievable only when all queens are on the same diagonal. In graph-theory representation they thus form the corresponding complete graph. - _Antal Pinter_, Dec 27 2015"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A057788/b057788.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Milan Janjic, \u003ca href=\"http://www.pmfbl.org/janjic/\"\u003eTwo Enumerative Functions\u003c/a\u003e",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_12\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (12,-66, 220,-495,792,-924,792,-495,220,-66,12,-1)."
			],
			"formula": [
				"a(n) = 2*C(n+11, 11) - C(n+10, 10). - _Paul Barry_, Mar 04 2003",
				"a(n) = C(n+10,10) + 2*C(n+10,11). - Borislav St. Borisov (b.st.borisov(AT)abv.bg), Mar 05 2009",
				"a(n) = C(n+10,10)*(2n+11)/11. - _Antal Pinter_, Dec 27 2015",
				"a(n) = 12*a(n-1)-66*a(n-2)+220*a(n-3)-495*a(n-4)+792*a(n-5)-924*a(n-6)+792*a(n-7)-495*a(n-8)+220*a(n-9)-66*a(n-10)+12*a(n-11)-a(n-12) for n \u003e11. - _Vincenzo Librandi_, Feb 14 2016",
				"a(n) = (2*n+11)*binomial(n+10, 10)/11. - _G. C. Greubel_, Dec 02 2018"
			],
			"maple": [
				"A057788 := proc(n)",
				"        1/39916800*(2*n+11) *(n+10) *(n+9) *(n+8) *(n+7) *(n+6) *(n+5) *(n+4) *(n+3) *(n+2) *(n+ 1) ; end proc: # _R. J. Mathar_, Mar 22 2011"
			],
			"mathematica": [
				"Table[(2*n+11)*Binomial[n+10, 10]/11, {n,0,40}] (* _G. C. Greubel_, Dec 02 2018 *)",
				"CoefficientList[Series[(1 + x) / (1 - x)^12, {x, 0, 40}], x] (* _Vincenzo Librandi_, Feb 14 2016 *)"
			],
			"program": [
				"(PARI) Vec((1+x)/(1-x)^12+O(x^99)) \\\\ _Charles R Greathouse IV_, Sep 23 2012",
				"(MAGMA) [Binomial(n+10,10)*(2*n+11)/11: n in [0..40]]; // _Vincenzo Librandi_, Feb 14 2016",
				"(Sage) [(2*n+11)*binomial(n+10, 10)/11 for n in range(40)] # _G. C. Greubel_, Dec 02 2018",
				"(GAP) List([0..30], n -\u003e (2*n+11)*Binomial(n+10, 10)/11); # _G. C. Greubel_, Dec 02 2018"
			],
			"xref": [
				"Cf. A054334, A054333, A053347, A002415, A005585, A040977, A050486.",
				"Partial sums of A054334.",
				"Sixth column of A111125 (s=5, without leading zeros). - _Wolfdieter Lang_, Oct 18 2012"
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Nov 04 2000",
			"references": 11,
			"revision": 42,
			"time": "2018-12-03T12:53:33-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}