{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A058986",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 58986,
			"data": "0,1,3,4,5,7,8,9,10,11,13,14,15,16,17,18,19,20,22",
			"name": "Sorting by prefix reversal (or \"flipping pancakes\"). You can only reverse segments that include the initial term of the current permutation; a(n) is the number of reversals that are needed to transform an arbitrary permutation of n letters to the identity permutation.",
			"comment": [
				"\"The chef in our place is sloppy and when he prepares a stack of pancakes they come out all different sizes. Therefore when I deliver them to a customer, on the way to the table I rearrange them (so that the smallest winds up on top and so on, down to the largest at the bottom) by grabbing several from the top and flipping them over, repeating this (varying the number I flip) as many times as necessary. If there are n pancakes, what is the maximum number of flips (as a function a(n) of n) that I will ever have to use to rearrange them?\" [Dweighter]",
				"J. K. McLean (jkmclean(AT)webone.com.au): If the worst case for n pancakes is x flips, then the worst case for n+1 pancakes can be no greater than x+2 flips. Getting the n+1 pancake to the bottom of the pile will require 0, 1 or 2 flips, after which you can sort the n remaining pancakes in at most x flips.",
				"Comments based on email message from Brian Hayes, Oct 10 2007: (Start)",
				"We are interested in the diameter of the graph where the vertices are all possible permutations of n elements and an edge connects p(i) and p(j) if some allowed reversal transforms p(i) into p(j).",
				"There are at least two dimensions to consider in describing the various sorting-by-reversal problems: (a) Are the elements of the sequence signed or unsigned? and (b) Are we constrained to work only from one end of the sequence?",
				"The standard pancake problem has unsigned elements and allows moves only from the top of the stack; the diameter is given by the present sequence.",
				"The \"burnt-pancake\" problem has signed elements and allows moves only from the top of the stack. This is sequence A078941 (and also A078942).",
				"The biologically-inspired sorting problems I was writing about in the Amer. Scientist 2007 column dispense with the one-end-only constraint. You're allowed to reverse any segment of contiguous elements, anywhere in the permutation. For the unsigned case, a(n) = n-1 (cf. Kececioglu and Sankoff).",
				"Finally there is the signed case without the one-end constraint. This was the main subject of my column and corresponds to sequence A131209. (End)",
				"Brian Goodwin (brian_goodwin(AT)yahoo.com), Aug 22 2005, comments that the terms so far match the beginning of the following triangle:",
				"                    0",
				"                    1",
				"                 3  4  5",
				"              7  8  9 10 11",
				"          13 14 15 16 17 18 19",
				"       21 22 23 24 25 26 27 28 29",
				"    31 32 ...",
				"Is this a coincidence? Answer from Mikola Lysenko (mclysenk(AT)mtu.edu), Dec 09 2006: Unfortunately, Yes! That triangular sequence has the closed form: a(n) = n - 1 + floor(sqrt(n-2)). However, Gates and Papadimitrou establish a lower bound on the pancake sequence of at least (17/16)*n. For sufficiently large n, this is always larger than the number in the triangle.",
				"Marc Lebrun writes that in 1975 he was involved with a group called \"People's Computer Company\" and among the many early computer games they created and popularized was one called \"Reverse\", which we published in our newspaper. See link.",
				"M. Peczarski and I affirm the value a(19) = 22 as given by Simon Singh. Firstly, a(19) \u003e= 22 as stated in the paper by Heydari Sudborough. This statement is mentioned on page 93. There two permutations of order 19 are given which need at least 22 flips. These permutations are 1,7,5,3,6,4,2,8,14,12,10,13,11,9,15,17,19,16,18 and 1,7,5,3,6,4,2,8,14,12,10,13,11,9,15,18,16,19,17. Making use of a branch-and-bound algorithm, we can confirm that their statement is correct. Together with the result that a(18) = 20, this gives a(19) = 22. Both values a(18) = 20 and a(19) = 22 were also proved in the paper by Cibulka. - _Gerold Jäger_, Oct 29 2020"
			],
			"reference": [
				"J. J. Chew, III (jjchew(AT)math.utoronto.ca), personal communication, Jan 15 and Feb 08 2001, computed a(10) - a(13).",
				"E. Györi and G. Turán, Stack of pancakes, Studia Sci. Math. Hungar., 13 (1978), 133-137."
			],
			"link": [
				"Shogo Asai, Yuusuke Kounoike, Yuji Shinano and Keiichi Kaneko, \u003ca href=\"https://doi.org/10.1007/11823285_117\"\u003eComputing the Diameter of 17-Pancake Graph Using a PC Cluster\u003c/a\u003e, Proc. Euro-Par 2006, LNCS 4128, pp. 1114-1124, 2006 Springer Verlag.",
				"Vineet Bafna and Pavel Pevzner, \u003ca href=\"https://doi.org/10.1137/S0097539793250627\"\u003eGenome rearrangements and sorting by reversals\u003c/a\u003e, SIAM Journal on Computing 25:272-289 (1996).",
				"Anne Bergeron, \u003ca href=\"https://doi.org/10.1016/j.dam.2004.04.010\"\u003eA very elementary presentation of the Hannenhalli-Pevzner theory\u003c/a\u003e, Discrete Applied Mathematics 146:134-145 (2005).",
				"Anne Bergeron, and Francois Strasbourg, \u003ca href=\"https://www.researchgate.net/publication/221313399_Experiments_in_Computing_Sequences_of_Reversals\"\u003eExperiments in computing sequences of reversals\u003c/a\u003e, Proceedings of the First International Workshop on Algorithms in Bioinformatics, 2001, pp. 164-174. Berlin: Springer-Verlag.",
				"Laurent Bulteau, Guillaume Fertin, Irena Rusu, \u003ca href=\"https://arxiv.org/abs/1111.0434\"\u003ePancake Flipping is Hard\u003c/a\u003e, arXiv:1111.0434 [cs.CC], Nov 10, 2011.",
				"Alberto Caprara, \u003ca href=\"https://www.cs.oberlin.edu/~asharp/cs365/papers/C97.pdf\"\u003eSorting by reversals is difficult\u003c/a\u003e, Proceedings of RECOMB '97: The First International Conference on Computational Molecular Biology, 1997, pp. 75-83. New York: ACM Press.",
				"B. Chitturi, W. Fahle, Z. Meng, L. Morales, C. O. Shields, I. H. Sudborough and W. Voit, \u003ca href=\"https://doi.org/10.1016/j.tcs.2008.04.045\"\u003eAn (18/11)n upper bound for sorting by prefix reversals\u003c/a\u003e, Theoret. Comput. Sci. 410 (2009), no. 36, 3372-3390.",
				"J. Cibulka, \u003ca href=\"https://doi.org/10.1016/j.tcs.2010.11.028\"\u003eOn average and highest number of flips in pancake sorting\u003c/a\u003e, Theoret. Comput. Sci. 412 (2011), 822-834",
				"Cristina Dalfó, Miquel Angel Fiol, \u003ca href=\"https://arxiv.org/abs/1906.05851\"\u003eSpectra and eigenspaces from regular partitions of Cayley (di)graphs of permutation groups\u003c/a\u003e, arXiv:1906.05851 [math.CO], 2019.",
				"F. Javier de Vega, \u003ca href=\"https://arxiv.org/abs/2003.13378\"\u003eAn extension of Furstenberg's theorem of the infinitude of primes\u003c/a\u003e, arXiv:2003.13378 [math.NT], 2020.",
				"Harry Dweighter [\"Harried Waiter\", pseudonym of Jacob E Goodman], \u003ca href=\"http://www.jstor.org/stable/2318260\"\u003eProblem E2569\u003c/a\u003e, Amer. Math. Monthly, 82 (1975), 1010. \u003ca href=\"https://www.jstor.org/stable/2318878\"\u003eComments\u003c/a\u003e by M. R. Garey, D. S. Johnson and S. Lin, loc. cit. 84 (1977), 296.",
				"W. H. Gates and C. H. Padadimitriou, \u003ca href=\"https://doi.org/10.1016/0012-365X(79)90068-2\"\u003eBounds for sorting by prefix reversal\u003c/a\u003e, Discrete Math. 27 (1979), 47-57.",
				"Sridhar Hannenhalli and Pavel A. Pevzner, \u003ca href=\"https://www.researchgate.net/publication/2667364_Transforming_Cabbage_into_Turnip_Polynomial_Algorithm_for_Sorting_Signed_Permutations_by_Reversals\"\u003eTransforming cabbage into turnip: polynomial algorithm for sorting signed permutations by reversals\u003c/a\u003e, Journal of the ACM 48:1-27 (1999).",
				"Brian Hayes, \u003ca href=\"https://www.jstor.org/stable/27859020\"\u003eComputing Science: Sorting out the genome\u003c/a\u003e, Amer. Scientist, 95 (2007), 386-391.",
				"M. H. Heydari and I. Hal Sudborough, \u003ca href=\"http://dx.doi.org/10.1006/jagm.1997.0874\"\u003eOn the diameter of the pancake network\u003c/a\u003e,J. Algorithms 25 (1997) no 1, 67-94.",
				"J. D. Kececioglu, and D. Sankoff, \u003ca href=\"https://www.researchgate.net/publication/226954865_Exact_and_Approximation_Algorithms_for_Sorting_by_Reversals_with_Application_to_Genome_Rearrangement\"\u003eExact and approximation algorithms for sorting by reversals, with application to genome rearrangement\u003c/a\u003e, Algorithmica (1995) 13:180.",
				"Yuusuke Kounoike, Keiichi Kaneko and Yuji Shinano, \u003ca href=\"https://doi.org/10.1109/ISPAN.2005.31\"\u003eComputing the Diameters of 14- and 15-pancake Graphs\u003c/a\u003e, Proc. International Symposium on Parallel Architectures, Algorithms and Networks(ISPAN 2005), pp. 490-495.",
				"Marc Lebrun et al., \u003ca href=\"http://www.svipx.com/pcc/gameslist.html\"\u003eSee under V1N5\u003c/a\u003e",
				"Ed Pegg, Jr., \u003ca href=\"http://www.mathpuzzle.com/pancakes.htm\"\u003ePancakes\u003c/a\u003e",
				"Ivars Peterson, \u003ca href=\"http://www.sciencenews.org/articles/20060902/mathtrek.asp\"\u003ePancake Sorting\u003c/a\u003e.",
				"Ivars Peterson, \u003ca href=\"http://www.maa.org/mathtourist/mathtourist_10_9_08.html\"\u003eImproved Pancake Sorting\u003c/a\u003e",
				"J. Sawada, A. Williams, \u003ca href=\"http://www.cis.uoguelph.ca/~sawada/papers/pancake_successor.pdf\"\u003eSuccessor rules for flipping pancakes and burnt pancakes\u003c/a\u003e, Preprint 2015; Theoretical Computer Science, Volume 609, Part 1, Jan 04 2016, Pages 60-75.",
				"Simon Singh, \u003ca href=\"http://www.theguardian.com/science/blog/2013/nov/14/flipping-pancakes-mathematics-jacob-goodman\"\u003eFlipping pancakes with mathematics\u003c/a\u003e, Blog Posting, Nov 14 2013. [States that a(18)=20, a(19)=22]",
				"N. J. A. Sloane, \u003ca href=\"http://neilsloane.com/doc/sg.txt\"\u003eMy favorite integer sequences\u003c/a\u003e, in Sequences and their Applications (Proceedings of SETA '98).",
				"Katie Steckles and Brady Haran, \u003ca href=\"https://youtu.be/m3drS_8BpU0\"\u003ePancake Numbers\u003c/a\u003e, Numberphile video (2017).",
				"Eric Tannier, and Marie-France Sagot, \u003ca href=\"https://doi.org/10.1007/978-3-540-27801-6_1\"\u003eSorting by reversals in subquadratic time\u003c/a\u003e, Proceedings of the 15th Annual Symposium on Combinatorial Pattern Matching, 2004, pp. 1-13. Berlin: Springer-Verlag.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PancakeSorting.html\"\u003ePancake Sorting\u003c/a\u003e",
				"Douglas B. West, \u003ca href=\"http://www.math.uiuc.edu/~west/openp/pancake.html\"\u003eThe Pancake Problems (1975, 1979, 1973)\u003c/a\u003e",
				"\u003ca href=\"/index/So#sorting\"\u003eIndex entries for sequences related to sorting\u003c/a\u003e"
			],
			"formula": [
				"It is known that a(n) \u003e= n+1 for n \u003e= 6, a(n) \u003e= (17/16)*n if n is a multiple of 16 (so a(32) \u003e= 34) and a(n) \u003c= (5*n+5)/3.",
				"There is an improved asymptotic upper bound of (18/11)*n + O(1) for the number of prefix reversals to sort permutations of length n given in the Chitturi et al. paper. - Ivan Hal Sudborough (hal_sud(AT)yahoo.com), Jul 02 2008"
			],
			"example": [
				"For n = 3, the stack of pancakes with radii (1, 3, 2) requires a(3) = 3 flips to sort: Starting with (1, 3, 2), flip the top two pancakes to get (3, 1, 2), then flip the entire stack to get (2, 1, 3), then flip the top two pancakes again to get (1, 2, 3)."
			],
			"xref": [
				"Cf. A067607, A078941, A078942, A092113."
			],
			"keyword": "nonn,nice,hard,more",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_, Jan 17 2001, Oct 12 2007",
			"ext": [
				"Typo in value for a(5) corrected by _Ed Pegg Jr_, Jan 02 2002",
				"a(14)-a(17) from Ivan Hal Sudborough (hal_sud(AT)yahoo.com), Jul 02 2008. The new upper bounds for n = 14, 15, 16 and 17 are found in the articles by Asai et al. and Kounoike et al.",
				"Simon Singh's blog gives values for a(18) and a(19). It is not clear if these have been proved to be correct. - _N. J. A. Sloane_, Dec 11 2013"
			],
			"references": 7,
			"revision": 145,
			"time": "2021-12-16T22:31:24-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}