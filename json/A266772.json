{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A266772",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 266772,
			"data": "1,0,1,0,2,0,3,0,5,1,7,1,11,2,15,3,22,5,30,7,41,11,54,15,73,22,94,30,123,41,157,54,201,73,252,94,318,123,393,157,488,201,598,252,732,318,887,393,1076,488,1291,598,1549,732,1845,887,2194,1076,2592,1291,3060,1549,3589,1845,4206,2194,4904,2592,5708,3060,6615,3589",
			"name": "Molien series for invariants of finite Coxeter group D_9.",
			"comment": [
				"The Molien series for the finite Coxeter group of type D_k (k \u003e= 3) has G.f. = 1/Prod_i (1-x^(1+m_i)) where the m_i are [1,3,5,...,2k-3,k-1]. If k is even only even powers of x appear, and we bisect the sequence."
			],
			"reference": [
				"J. E. Humphreys, Reflection Groups and Coxeter Groups, Cambridge, 1990. See Table 3.1, page 59."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A266772/b266772.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Mo#Molien\"\u003eIndex entries for Molien series\u003c/a\u003e"
			],
			"formula": [
				"G.f.: 1/((1-t^2)*(1-t^4)*(1-t^6)*(1-t^8)*(1-t^9)*(1-t^10)*(1-t^12)*(1-t^14)*(1-t^16))."
			],
			"maple": [
				"seq(coeff(series(1/((1-x^9)*mul(1-x^(2*j), j=1..8)), x, n+1), x, n), n = 0..80); # _G. C. Greubel_, Feb 03 2020"
			],
			"mathematica": [
				"CoefficientList[Series[1/((1-x^9)*Product[1-x^(2*j), {j,8}]), {x,0,80}], x] (* _G. C. Greubel_, Feb 03 2020 *)"
			],
			"program": [
				"(PARI) Vec(1/((1-x^9)*prod(j=1,8,1-x^(2*j))) +O('x^80)) \\\\ _G. C. Greubel_, Feb 03 2020",
				"(MAGMA) R\u003cx\u003e:=PowerSeriesRing(Integers(), 80); Coefficients(R!( 1/((1-x^9)*(\u0026*[1-x^(2*j): j in [1..8]])) )); // _G. C. Greubel_, Feb 03 2020",
				"(Sage)",
				"def A266772_list(prec):",
				"    P.\u003cx\u003e = PowerSeriesRing(ZZ, prec)",
				"    return P( 1/((1-x^9)*product(1-x^(2*j) for j in (1..8))) ).list()",
				"A266772_list(80) # _G. C. Greubel_, Feb 03 2020"
			],
			"xref": [
				"Molien series for finite Coxeter groups D_3 through D_12 are A266755, A266769, A266768, A003402, and A266770-A266775."
			],
			"keyword": "nonn",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_, Jan 11 2016",
			"references": 1,
			"revision": 8,
			"time": "2020-02-03T15:05:30-05:00",
			"created": "2016-01-11T13:38:22-05:00"
		}
	]
}