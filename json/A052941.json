{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A052941",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 52941,
			"data": "1,3,11,40,146,533,1946,7105,25941,94713,345806,1262570,4609761,16830668,61450341,224360935,819162731,2990839648,10919834926,39869337325,145566674726,531477526653,1940474094561,7084852176865",
			"name": "Expansion of (1-x)/(1 - 4*x + x^2 + x^3).",
			"comment": [
				"a(n) = term (3,1) in M^n, M = the 3 X 3 matrix [1,1,2; 1,2,1; 1,1,1]. - _Gary W. Adamson_, Mar 12 2009"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A052941/b052941.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Paul Barry, \u003ca href=\"https://arxiv.org/abs/2104.01644\"\u003eCentered polygon numbers, heptagons and nonagons, and the Robbins numbers\u003c/a\u003e, arXiv:2104.01644 [math.CO], 2021.",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=932\"\u003eEncyclopedia of Combinatorial Structures 932\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-1,-1)."
			],
			"formula": [
				"G.f.: (1-x)/(1 - 4*x + x^2 + x^3).",
				"a(n) = 4*a(n-1) - a(n-2) - a(n-3).",
				"a(n) = Sum_{alpha=RootOf(1-4*z+z^2+z^3)} (3-alpha^2)*alpha^(-1-n)/13."
			],
			"maple": [
				"spec:= [S,{S=Sequence(Union(Z,Z,Prod(Union(Sequence(Z),Z),Z)))},unlabeled ]: seq(combstruct[count ](spec,size=n), n=0..20);",
				"seq(coeff(series((1-x)/(1-4*x+x^2+x^3), x, n+1), x, n), n = 0..30); # _G. C. Greubel_, Oct 18 2019"
			],
			"mathematica": [
				"LinearRecurrence[{4,-1,-1},{1,3,11},30] (* _Vincenzo Librandi_, Jun 22 2012 *)"
			],
			"program": [
				"(MAGMA) I:=[1, 3, 11]; [n le 3 select I[n] else 4*Self(n-1)-Self(n-2)-Self(n-3): n in [1..30]]; // _Vincenzo Librandi_, Jun 22 2012",
				"(PARI) my(x='x+O('x^30)); Vec((1-x)/(1-4*x+x^2+x^3)) \\\\ _Altug Alkan_, Sep 21 2018",
				"(Sage)",
				"def A052941_list(prec):",
				"    P.\u003cx\u003e = PowerSeriesRing(ZZ, prec)",
				"    return P((1-x)/(1-4*x+x^2+x^3)).list()",
				"A052941_list(30) # _G. C. Greubel_, Oct 18 2019",
				"(GAP) a:=[1,3,11];; for n in [4..30] do a[n]:=4*a[n-1]-a[n-2]-a[n-3]; od; a; # _G. C. Greubel_, Oct 18 2019"
			],
			"keyword": "easy,nonn",
			"offset": "0,2",
			"author": "encyclopedia(AT)pommard.inria.fr, Jan 25 2000",
			"ext": [
				"More terms from _James A. Sellers_, Jun 06 2000"
			],
			"references": 1,
			"revision": 36,
			"time": "2021-06-30T18:59:14-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}