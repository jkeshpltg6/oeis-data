{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A123853",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 123853,
			"data": "1,3,-15,113,-5397,84813,-3267755,74391561,-15633072909,465681118929,-31041303829713,1145088996404679,-185348722911971841,8165727090278785521,-778296382754673737187,39898888480559205453945,-35033447016186321707305533",
			"name": "Numerators in an asymptotic expansion for the cubic recurrence sequence A123851.",
			"comment": [
				"A cubic analog of the asymptotic expansion A116603 of Somos's quadratic recurrence sequence A052129. Denominators are A123854."
			],
			"reference": [
				"S. R. Finch, Mathematical Constants, Cambridge University Press, Cambridge, 2003, p. 446."
			],
			"link": [
				"T. M. Apostol, \u003ca href=\"https://projecteuclid.org/euclid.pjm/1103052188\"\u003eOn the Lerch zeta function\u003c/a\u003e, Pacific J. Math. 1 (1951), 161-167. [In Eq. (3.7), p. 166, the index in the summation for the Apostol-Bernoulli numbers should start at s = 0, not at s = 1. - _Petros Hadjicostas_, Aug 09 2019]",
				"Jonathan Sondow and Petros Hadjicostas, \u003ca href=\"https://arxiv.org/abs/math/0610499\"\u003eThe generalized-Euler-constant function gamma(z) and a generalization of Somos's quadratic recurrence constant\u003c/a\u003e, arXiv:math/0610499 [math.CA], 2006.",
				"Jonathan Sondow and Petros Hadjicostas, \u003ca href=\"https://doi.org/10.1016/j.jmaa.2006.09.081 \"\u003eThe generalized-Euler-constant function gamma(z) and a generalization of Somos's quadratic recurrence constant\u003c/a\u003e, J. Math. Anal. Appl. 332 (2007), 292-314.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/SomossQuadraticRecurrenceConstant.html\"\u003eSomos's Quadratic Recurrence Constant\u003c/a\u003e.",
				"Aimin Xu, \u003ca href=\"https://doi.org/10.1142/S1793042119501112\"\u003eAsymptotic expansion related to the Generalized Somos Recurrence constant\u003c/a\u003e, International Journal of Number Theory 15(10) (2019), 2043-2055. [The author gives recurrences and other formulas for the coefficients of the asymptotic expansion using the Apostol-Bernoulli numbers (see the reference above) and the Bell polynomials. - _Petros Hadjicostas_, Aug 09 2019]"
			],
			"example": [
				"A123851(n) ~ c^(3^n)*n^(-1/2)/(1 + 3/(4*n) - 15/(32*n^2) + 113/(128*n^3) - 5397/(2048*n^4) + ...) where c = 1.1563626843322... is the cubic recurrence constant A123852."
			],
			"maple": [
				"f:=proc(t,x) exp(sum(ln(1+m*x)/t^m,m=1..infinity)); end; for j from 0 to 29 do numer(coeff(series(f(3,x),x=0,30),x,j)); od;"
			],
			"program": [
				"(PARI) {a(n) = local(A); if(n \u003c 0, 0, A = 1 + O(x) ; for( k = 1, n, A = truncate(A) + x * O(x^k); A += x^k * polcoeff( 3/4 * (subst(1/A, x, x^2/(1-x^2))^2/(1-x^2) - 1/subst(A, x, x^2)^(2/3)), 2*k ) ); numerator( polcoeff( A, n ) ) ) } /* _Michael Somos_, Aug 23 2007 */"
			],
			"xref": [
				"Cf. A052129, A112302, A116603, A123851, A123852, A123854 (denominators)."
			],
			"keyword": "frac,sign",
			"offset": "0,2",
			"author": "_Petros Hadjicostas_ and _Jonathan Sondow_, Oct 15 2006",
			"references": 6,
			"revision": 33,
			"time": "2020-05-14T13:51:02-04:00",
			"created": "2006-12-06T03:00:00-05:00"
		}
	]
}