{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A178690",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 178690,
			"data": "0,0,0,36,432,3660,27000,185556,1223712,7862940,49653000,309776676,1915868592,11772890220,71992229400,438593697396,2664227115072,16146540253500,97676540188200,590011376299716,3559691497843152,21455715437760780,129219925869401400",
			"name": "Expansion of (exp(3*x)-1)*(exp(2*x)-1)*(exp(x)-1).",
			"comment": [
				"a(n) is the number of 3 X n matrices with the following properties:",
				"    i) Each row has at least one nonzero entry.",
				"   ii) Each column has exactly one nonzero entry.",
				"  iii) The nonzero entries in row m, 1 \u003c= m \u003c= 3, are in {1,2,...,m}.",
				"This sequence counts such 3 X n matrices but the results are easily generalized for any such m X n matrix."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A178690/b178690.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (18,-121,372,-508,240)."
			],
			"formula": [
				"E.g.f.: (exp(3*x)-1)*(exp(2*x)-1)*(exp(x)-1).",
				"G.f.: 12*x^3*(3-18*x+20*x^2)/((1-x)*(1-2*x)*(1-4*x)*(1-5*x)*(1-6*x)). - _Colin Barker_, Nov 30 2014",
				"For n \u003e 0, a(n) = 1 + 2^n - 4^n - 5^n + 6^n. - _Vaclav Kotesovec_, Dec 01 2014",
				"a(n) = 18*a(n-1) - 121*a(n-2) + 372*a(n-3) - 508*a(n-4) + 240*a(n-5). - _Vaclav Kotesovec_, Dec 01 2014"
			],
			"maple": [
				"a:=series((exp(3*x)-1)*(exp(2*x)-1)*(exp(x)-1),x=0,23): seq(n!*coeff(a,x,n),n=0..22); # _Paolo P. Lava_, Mar 28 2019"
			],
			"mathematica": [
				"a=Exp[x]-1;b=Exp[2x]-1;c=Exp[3x]-1;Range[0,20]! CoefficientList[Series[a b c,{x,0,20}],x]"
			],
			"program": [
				"(PARI) concat([0,0,0], Vec(-12*x^3*(20*x^2-18*x+3)/((x-1)*(2*x-1)*(4*x-1)*(5*x-1)*(6*x-1)) + O(x^30))) \\\\ _Colin Barker_, Dec 01 2014",
				"(MAGMA) m:=30; R\u003cx\u003e:=PowerSeriesRing(Rationals(), m); b:=Coefficients(R!( (Exp(3*x)-1)*(Exp(2*x)-1)*(Exp(x)-1) )); [0,0,0] cat [Factorial(n+2)*b[n]: n in [1..m-3]]; // _G. C. Greubel_, Jan 26 2019",
				"(Sage) m = 30; T = taylor((exp(3*x)-1)*(exp(2*x)-1)*(exp(x)-1), x, 0, m); [factorial(n)*T.coefficient(x, n) for n in (0..m)] # _G. C. Greubel_, Jan 26 2019"
			],
			"xref": [
				"Cf. A083321, which is essentially the case for m=2."
			],
			"keyword": "nonn,easy",
			"offset": "0,4",
			"author": "_Geoffrey Critzer_, Dec 25 2010",
			"references": 1,
			"revision": 37,
			"time": "2019-03-28T10:55:32-04:00",
			"created": "2010-11-12T14:27:31-05:00"
		}
	]
}