{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A066678",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 66678,
			"data": "1,2,6,4,10,6,28,8,18,10,22,12,52,28,30,16,102,18,190,20,42,22,46,24,100,52,54,28,58,30,310,32,66,102,70,36,148,190,78,40,82,42,172,44,180,46,282,48,196,100,102,52,106,54,110,56,228,58,708,60,366,310,126,64",
			"name": "Totients of the least numbers for which the totient is divisible by n.",
			"comment": [
				"From _Alonso del Arte_, Feb 03 2017: (Start)",
				"One of the less obvious consequences of Dirichlet's theorem on primes in arithmetic progression is that this sequence is well-defined for all positive integers.",
				"Suppose n is a nontotient (see A007617). Obviously a(n) != n. Dirichlet's theorem assures us that, if nothing else, there are infinitely many primes of the form nk + 1 for k positive (and in this case, k \u003e 1). Then phi(nk + 1) = nk, suggesting a(n) = nk corresponding to the smallest k.",
				"Of course not all a(n) are 1 less than a prime, such as 8, 20, 24, 54, etc. (End)"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A066678/b066678.txt\"\u003eTable of n, a(n) for n = 1..5000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A000010(A061026(n))."
			],
			"example": [
				"a(23) = 46 because there is no solution to phi(x) = 23 but there are solutions to phi(x) = 46, like x = 47.",
				"a(24) = 24 because there are solutions to phi(x) = 24, such as x = 35."
			],
			"mathematica": [
				"EulerPhi[mulTotientList = ConstantArray[1, 70]; k = 1; While[Length[vac = Rest[Flatten[Position[mulTotientList, 1]]]] \u003e 0, k++; mulTotientList[[Intersection[Divisors[EulerPhi[k]], vac]]] *= k]; mulTotientList] (* _Vincenzo Librandi_ Feb 04 2017 *)",
				"a[n_] := For[k=1, True, k++, If[Divisible[t = EulerPhi[k], n], Return[t]]];",
				"Array[a, 64] (* _Jean-François Alcover_, Jul 30 2018 *)"
			],
			"program": [
				"(Sage)",
				"def A066678(n):",
				"    s = 1",
				"    while euler_phi(s) % n: s += 1",
				"    return euler_phi(s)",
				"print([A066678(n) for n in (1..64)]) # _Peter Luschny_, Feb 05 2017"
			],
			"xref": [
				"Cf. A000010, A066674, A066675, A066676, A066677, A067005, A061026."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Labos Elemer_, Dec 22 2001",
			"references": 8,
			"revision": 26,
			"time": "2020-03-05T16:59:21-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}