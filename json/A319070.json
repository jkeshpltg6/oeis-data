{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A319070",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 319070,
			"data": "0,1,4,4,16,7,36,12,24,19,100,17,144,39,44,32,256,33,324,41,72,103,484,40,160,147,108,65,784,57,900,80,152,259,228,66,1296,327,204,93,1600,99,1764,137,160,487,2116,92,504,165,332,185,2704,135,388",
			"name": "a(n) is the area of the surface made of the rectangles with vertices (d, n/d), (D, n/d), (D, n/D), (d, n/D) for all (d, D), pair of consecutive divisors of n.",
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A319070/b319070.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Luc Rousseau, \u003ca href=\"/A319070/a319070.png\"\u003eDiagram illustrating a(10)=19.\u003c/a\u003e"
			],
			"formula": [
				"a(1) = 0.",
				"a(p) = (p-1)^2 for p a prime number.",
				"a(p^k) = (p-1)^2*k*p^(k-1) for p^k a prime power.",
				"a(p*q) = 2*(p-1)^2*q + (q-p)^2 for p and q primes (p \u003c q).",
				"a(n) = (n/2 - 1)^2 + 3 if n=2*p with p a prime greater than 2.",
				"a(n) = (n/p + F(p-1))^2 + p^2 - F(p-1)^2 if n = p*q, p \u003c q primes; where F denotes the Fibonacci polynomial, F(x) = x^2 - x - 1 (see A165900).",
				"For more complex factorization patterns of n, the formula depends on the factorization pattern of the sequence of divisors of n (see A191743 or A290110), e.g.:",
				"a(p^2*q) = 4*p*q*(p-1)^2 + (q-p^2)^2 if 1 \u003c p \u003c p^2 \u003c q \u003c p*q \u003c p^2*q,",
				"but",
				"a(p^2*q) = 2*p*q*(p-1)^2 + 2*p*(q-p)^2 + (p^2-q)^2 if 1 \u003c p \u003c q \u003c p^2 \u003c p*q \u003c p^2*q.",
				"a(n) = Sum_{i=1..tau(n)-1} (d_[tau(n)-i+1] - d_[tau(n)-i])*(d_[i+1] - d_[i]), where {d_i}, i=1..tau(n) is the increasing sequence of divisors of n. - _Ridouane Oudra_, Oct 17 2021"
			],
			"example": [
				"The divisors of n=12 are {1, 2, 3, 4, 6, 12}. The widths of the rectangles from the definition are obtained by difference: {1, 1, 1, 2, 6}. By symmetry, their heights are the same, but in reverse order: {6, 2, 1, 1, 1}. The sought total area is the sum of products width*height of each rectangle, in other terms it is the dot product 1*6 + 1*2 + 1*1 + 2*1 + 6*1. Result: 17. So, a(12)=17."
			],
			"mathematica": [
				"a[n_] := Module[{x = Differences[Divisors[n]]}, Plus @@ (x*Reverse[x])];",
				"Table[a[n], {n, 1, 55}]"
			],
			"program": [
				"(PARI) arect(n, d, D) = (D-d)*(n/d - n/D);",
				"a(n) = my(vd = divisors(n)); sum(k=1, #vd-1, arect(n, vd[k], vd[k+1])); \\\\ _Michel Marcus_, Oct 28 2018"
			],
			"xref": [
				"Cf. A191743, A290110 (introducing factorization patterns of sequences of divisors).",
				"Cf. A165900 (the Fibonacci polynomial)."
			],
			"keyword": "nonn,look",
			"offset": "1,3",
			"author": "_Luc Rousseau_, Sep 09 2018",
			"references": 1,
			"revision": 31,
			"time": "2021-10-25T16:18:36-04:00",
			"created": "2018-12-01T08:33:15-05:00"
		}
	]
}