{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001287",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1287,
			"id": "M4794 N2046",
			"data": "1,11,66,286,1001,3003,8008,19448,43758,92378,184756,352716,646646,1144066,1961256,3268760,5311735,8436285,13123110,20030010,30045015,44352165,64512240,92561040,131128140,183579396,254186856,348330136,472733756,635745396",
			"name": "a(n) = binomial coefficient C(n,10).",
			"comment": [
				"Product of 10 consecutive numbers divided by 10!. - _Artur Jasinski_, Dec 02 2007",
				"In this sequence only 11 is prime. - _Artur Jasinski_, Dec 02 2007",
				"With a different offset, number of n-permutations (n\u003e=10) of 2 objects: u,v, with repetition allowed, containing exactly 10 u's. Example: a(1)=11 because we have uuuuuuuuuuv, uuuuuuuuuvu, uuuuuuuuvuu, uuuuuuuvuuu, uuuuuuvuuuu, uuuuuvuuuuu, uuuuvuuuuuu, uuuvuuuuuuu, uuvuuuuuuuu, uvuuuuuuuuu and vuuuuuuuuuu. - _Zerinvary Lajos_, Aug 03 2008",
				"a(9+k) is the number of times that each digit appears repeated inside a list made with all the possible base 10 numbers of k digits such that their digits are read in ascending order from left to right. - _R. J. Cano_ Jul 20 2014",
				"a(n) = fallfac(n,10)/10! = binomial(n, 10) is also the number of independent components of an antisymmetric tensor of rank 10 and dimension n \u003e= 10 (for n=1..9 this becomes 0). Here fallfac is the falling factorial. - _Wolfdieter Lang_, Dec 10 2015"
			],
			"reference": [
				"M. Abramowitz and I. A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, 1964 (and various reprintings), p. 828.",
				"Albert H. Beiler, Recreations in the Theory of Numbers, Dover, NY, 1964, p. 196.",
				"L. E. Dickson, History of the Theory of Numbers. Carnegie Institute Public. 256, Washington, DC, Vol. 1, 1919; Vol. 2, 1920; Vol. 3, 1923, see vol. 2, p. 7.",
				"J. C. P. Miller, editor, Table of Binomial Coefficients. Royal Society Mathematical Tables, Vol. 3, Cambridge Univ. Press, 1954.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A001287/b001287.txt\"\u003eTable of n, a(n) for n = 10..1000\u003c/a\u003e",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"Peter J. Cameron, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL3/groups.html\"\u003eSequences realized by oligomorphic permutation groups\u003c/a\u003e, J. Integ. Seqs. Vol. 3 (2000), #00.1.5.",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=260\"\u003eEncyclopedia of Combinatorial Structures 260\u003c/a\u003e.",
				"Milan Janjic, \u003ca href=\"http://www.pmfbl.org/janjic/\"\u003eTwo Enumerative Functions\u003c/a\u003e.",
				"\u003ca href=\"/index/Rec#order_11\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (11,-55,165,-330,462,-462,330,-165,55,-11,1)."
			],
			"formula": [
				"a(n) = A110555(n+1,10). - _Reinhard Zumkeller_, Jul 27 2005",
				"a(n+9) = n(n+1)(n+2)(n+3)(n+4)(n+5)(n+6)(n+7)(n+8)(n+9)/10!. - _Artur Jasinski_, Dec 02 2007; _R. J. Mathar_, Jul 07 2009",
				"G.f.: x^10/(1-x)^11. - _Zerinvary Lajos_, Aug 06 2008; _R. J. Mathar_, Jul 07 2009",
				"Sum_{k\u003e=10} 1/a(k) = 10/9. - _Tom Edgar_, Sep 10 2015",
				"Sum_{n\u003e=10} (-1)^n/a(n) = A001787(10)*log(2) - A242091(10)/9! = 5120*log(2) - 447047/126 = 0.9215009748... - _Amiram Eldar_, Dec 10 2020"
			],
			"maple": [
				"seq(binomial(n,10),n=10..31); # _Zerinvary Lajos_, Aug 06 2008"
			],
			"mathematica": [
				"Table[n (n + 1) (n + 2) (n + 3) (n + 4) (n + 5) (n + 6) (n + 7) (n + 8) (n + 9)/10!, {n, 1, 100}] (* _Artur Jasinski_, Dec 02 2007 *)",
				"Table[Binomial[n, 10], {n, 10, 20}] (* _Zerinvary Lajos_, Jan 31 2010 *)"
			],
			"program": [
				"(MAGMA) [Binomial(n,10): n in [10..40]]; // _Vincenzo Librandi_, Sep 11 2015",
				"(PARI) a(n)=binomial(n,10) \\\\ _Charles R Greathouse IV_, Sep 24 2015",
				"(Python)",
				"A001287_list, m = [], [1]*11",
				"for _ in range(10**2):",
				"    A001287_list.append(m[-1])",
				"    for i in range(10):",
				"        m[i+1] += m[i] # _Chai Wah Wu_, Jan 24 2016"
			],
			"xref": [
				"Cf. A110555, A001787, A242091."
			],
			"keyword": "nonn,easy",
			"offset": "10,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Formulas valid for different offsets rewritten by _R. J. Mathar_, Jul 07 2009",
				"Extended by _Ray Chandler_, Oct 25 2011"
			],
			"references": 19,
			"revision": 80,
			"time": "2020-12-12T03:15:10-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}