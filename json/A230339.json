{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A230339",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 230339,
			"data": "0,1,1,19,17,55,83,119,82,73,95,121,227,559,679,815,484,1139,443,171,295,2023,2299,2599,1462,3275,3653,451,749,551,5455,5983,3272,7139,7769,8435,1523,3293,3553,11479,6170,13243,14189,15179,8107,5765",
			"name": "Numerator of Sum_{k=1..n} 1/(k(k+1)(k+2)(k+3)) = Sum_{k=1..n} 1/Pochhammer(k,4).",
			"reference": [
				"L. B. W. Jolley, Summation of Series, Second revised ed., Dover, 1961, p.38, (202) and (201)."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A230339/b230339.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Eric Weisstein's MathWorld, \u003ca href=\"http://mathworld.wolfram.com/PochhammerSymbol.html\"\u003ePochhammer Symbol\u003c/a\u003e"
			],
			"formula": [
				"Numerator(1/18 - 1/(3*(n+1)*(n+2)*(n+3))) (from the generic formula Sum_{k=1..n} 1/Pochhammer(k, m) = 1/((m-1)*(m-1)!) - 1/((m-1)*Pochhammer(n+1, m-1)) with m = 4).",
				"G.f. for the rationals r(n) = (1/18)*n*(11+n^2+6*n)/((1+n)*(n+2)*(n+3)) = a(n)/A230340(n): (1/18)*(1 - hypergeometric([1, 3], [4], -x/(1-x)))/(1-x) = (6*x - 15*x^2 + 11*x^3 +  6*(1 - 3*x + 3*x^2 - x^3)*log(1-x))/(36*x^3*(1-x)). - _Wolfdieter Lang_, Mar 08 2018",
				"a(n) = numerator(1/18 - 1/(3*(n+1)*(n+2)*(n+3))). - _Colin Barker_, Jul 30 2019"
			],
			"example": [
				"1/(1*2*3*4) + 1/(2*3*4*5) + 1/(3*4*5*6) = 19/360, so a(3) = 19.",
				"The rationals r(n) = a(n)/A230340(n) begin: 0, 1/24, 1/20, 19/360, 17/315, 55/1008, 83/1512, 119/2160, 82/1485, 73/1320, 95/1716, 121/2184, 227/4095, 559/10080, 679/12240, 815/14688, ... - _Wolfdieter Lang_, Mar 08 2018"
			],
			"mathematica": [
				"a[n_] := Numerator[1/18 - 1/(3*(n+1)*(n+2)*(n+3))]; Table[a[n], {n, 0, 100}]"
			],
			"program": [
				"PARI a(n) = numerator(1/18 - 1/(3*(n+1)*(n+2)*(n+3))) \\\\ _Colin Barker_, Jul 30 2019"
			],
			"xref": [
				"Cf. A001563, A052762, A094258, A125650, A230328, A230340 (denominators)."
			],
			"keyword": "nonn,frac,easy",
			"offset": "0,4",
			"author": "_Jean-François Alcover_, Oct 16 2013",
			"references": 3,
			"revision": 30,
			"time": "2019-07-30T06:37:24-04:00",
			"created": "2013-10-19T02:05:12-04:00"
		}
	]
}