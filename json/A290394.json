{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A290394",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 290394,
			"data": "2,11,11,29,29,37,37,53,127,127,127,127,127,149,149,149,211,223,223,223,307,307,331,331,331,331,331,331,331,541,541,541,541,541,541,541,541,541,541,1361,1361,1361,1361,1361,1361,1361,1361,1361,1361,1361,1361,1361,1361,1361,1361,1361,1361,1361,1361,1361,1361,1361,1361,1361,1361,1361,1361,1361,1361,1693",
			"name": "First k-Ramanujan prime, where k = 1 + 1/n.",
			"comment": [
				"For real k \u003e 1, the first k-Ramanujan prime is the smallest integer m with pi(x) - pi(x/k) \u003e= 1 for all real x \u003e= m. For 0 \u003c c \u003c 1, the first c-Ramanujan prime is the first k-Ramanujan prime with k = 1/c.",
				"Axler (2015, Cor. 2.4 and Prop. 2.5(ii)) and Axler and Leßmann (2017, Theorem 1) computed the first k-Ramanujan prime for all k \u003e= 1.000040690557321. With k = 1 + 1/n, this gives 1 \u003c= n \u003c= 24575; in particular, a(24575) = 2898359. They also give the isolated result a(28313999) = 10726905041 on p. 646.",
				"The Mathematica program below is based on their algorithm but uses only part of their data (compare A277719) and is valid only for 1 \u003c= n \u003c= 1014; in particular, a(1014) = 48731. Their algorithm uses their result that for N \u003e 1 the N-th prime p_N is the first k-Ramanujan prime if and only if p_N \u003e k*p_{N-1} and p_n \u003c= k*p_{n-1} for all n \u003e N.",
				"See A104272 for additional comments, references, links, formulas, examples, programs, and cross-refs."
			],
			"link": [
				"N. Amersi, O. Beckwith, S. J. Miller, R. Ronan, J. Sondow, \u003ca href=\"https://doi.org/10.1007/978-1-4939-1601-6_1\"\u003eGeneralized Ramanujan primes\u003c/a\u003e, Combinatorial and Additive Number Theory, Springer Proc. in Math. \u0026 Stat., CANT 2011 and 2012, Vol. 101 (2014), 1-13; \u003ca href=\"http://arxiv.org/abs/1108.0475\"\u003earXiv:1108.0475 [math.NT], 2011\u003c/a\u003e.",
				"Christian Axler, \u003ca href=\"http://dx.doi.org/10.1007/s11139-015-9693-9\"\u003eOn generalized Ramanujan primes\u003c/a\u003e, Ramanujan J., online 30 April 2015, 1-30.",
				"Christian Axler and Thomas Leßmann, \u003ca href=\"http://arxiv.org/abs/1504.05485\"\u003eAn explicit upper bound for the first k-Ramanujan prime\u003c/a\u003e, arXiv:1504.05485 [math.NT], 2015.",
				"Christian Axler and Thomas Leßmann, \u003ca href=\"http://www.jstor.org/stable/10.4169/amer.math.monthly.124.7.642\"\u003eOn the first k-Ramanujan prime\u003c/a\u003e, Amer. Math. Monthly, 124 (2017), 642-646; correction by J. Sondow, \u003ca href=\"http://www.jstor.org/stable/10.4169/amer.math.monthly.124.10.983\"\u003eEditor's endnotes\u003c/a\u003e, Amer. Math. Monthly, 124 (2017), 985.",
				"V. Shevelev, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL15/Shevelev/shevelev19.html\"\u003eRamanujan and Labos primes, their generalizations, and classifications of primes\u003c/a\u003e, J. Integer Seq. 15 (2012) Article 12.5.4."
			],
			"example": [
				"a(1) = first 2-Ramanujan prime = first 1/2-Ramanujan prime = first Ramanujan prime = A104272(1) = 2.",
				"a(3) = first 4/3-Ramanujan prime = first 3/4-Ramanujan prime = A193880(1) = 11."
			],
			"mathematica": [
				"A = {3, 5, 7, 10, 12, 16, 31, 35, 47, 48, 63, 67, 100, 218, 264, 298, 328,  368, 430, 463, 591, 651, 739, 758, 782, 843, 891, 929, 1060, 1184, 1230, 1316, 1410, 1832, 2226, 3386, 3645, 3794, 3796, 4523, 4613, 4755, 5009, 5950}; kR1[k_] := If[k \u003e= 5/3, 2, (m = 1;",
				"   While[k \u003e= Prime[A[[m]]]/Prime[A[[m]] - 1] ||",
				"     k \u003c Prime[A[[m + 1]]]/Prime[A[[m + 1]] - 1], m++];",
				"   Prime[A[[m]]])]; Table[kR1[1 + 1/n], {n, 70}]"
			],
			"xref": [
				"Cf. A104272, A164952, A193761, A193880, A277718, A277719."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Jonathan Sondow_, Jul 29 2017",
			"references": 5,
			"revision": 20,
			"time": "2018-01-01T13:17:41-05:00",
			"created": "2017-07-29T21:44:26-04:00"
		}
	]
}