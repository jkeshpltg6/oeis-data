{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A297615",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 297615,
			"data": "1,2,4,6,5,8,3,20,16,7,10,18,23,14,22,12,9,26,24,35,32,11,38,36,17,30,42,15,28,34,29,44,66,31,40,46,48,21,76,58,47,54,78,45,60,13,70,82,33,88,56,41,74,62,27,50,68,59,52,72,19,136,64,43,92,80,84",
			"name": "Triangular array T(n, k) read by rows, n \u003e 0, 0 \u003c k \u003c= n: T(n, k) = least unused positive value (reading rows from left to right) such that each triple of pairwise adjacent terms sums to a prime.",
			"comment": [
				"Each term may be involved in up to six sums:",
				"- T(1, 1) is involved in one sum,",
				"- For any r \u003e 1, T(r, 1) and T(r, r) are involved in three sums:",
				"- For any r \u003e 1 and c such that 1 \u003c c \u003c r, T(r, c) is involved in six sums.",
				"Among each triple of pairwise adjacent terms, we cannot have all values equal mod 3 or all values distinct mod 3; this gives rise to the patterns visible in the illustration in the Links section.",
				"T(n, k) is odd iff n + k == 2 mod 3.",
				"See also A297673 for a similar triangle."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A297615/b297615.txt\"\u003eRows n = 1..100, flattened\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A297615/a297615.gp.txt\"\u003ePARI program for A297615\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A297615/a297615.png\"\u003eColored representation of the first 500 rows\u003c/a\u003e (where the color is function of T(n, k) mod 3)"
			],
			"example": [
				"Triangle begins:",
				"   1:                       1",
				"   2:                     2,  4",
				"   3:                   6,  5,  8",
				"   4:                 3, 20, 16,  7",
				"   5:              10, 18, 23, 14, 22",
				"   6:            12,  9, 26, 24, 35, 32",
				"   7:          11, 38, 36, 17, 30, 42, 15",
				"   8:        28, 34, 29, 44, 66, 31, 40, 46",
				"   9:      48, 21, 76, 58, 47, 54, 78, 45, 60",
				"  10:    13, 70, 82, 33, 88, 56, 41, 74, 62, 27",
				"The term T(1, 1) = 1 is involved in the following sum:",
				"  -  1 +  2 +  4 =   7.",
				"The term T(4, 4) = 7 is involved in the following sums:",
				"  -  8 + 16 +  7 =  31,",
				"  - 16 +  7 + 14 =  37,",
				"  -  7 + 14 + 22 =  43.",
				"The term T(7, 6) = 42 is involved in the following sums:",
				"  - 35 + 32 + 42 = 109,",
				"  - 35 + 30 + 42 = 107,",
				"  - 32 + 42 + 15 =  89,",
				"  - 30 + 42 + 31 = 103,",
				"  - 42 + 31 + 40 = 113,",
				"  - 42 + 15 + 40 =  97."
			],
			"program": [
				"(PARI) See Links section."
			],
			"xref": [
				"Cf. A297673."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Rémy Sigrist_, Jan 01 2018",
			"references": 2,
			"revision": 16,
			"time": "2018-11-17T20:55:46-05:00",
			"created": "2018-01-03T17:55:34-05:00"
		}
	]
}