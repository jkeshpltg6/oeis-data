{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A164554",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 164554,
			"data": "2,71,101,181,239,241,269,349,373,409,419,433,439,491,593,599,601,607,647,653,659,823,827,857,947,1021,1031,1061,1063,1091,1103,1301,1427,1429,1447,1451,1489,1553,1559,1567,1601,1607,1609,1789,1867,1871,1913,1999,2003",
			"name": "Ramanujan primes A104272(n) for which A104272(n) = A080359(n).",
			"comment": [
				"For every n\u003e=1, A104272(n) \u003e= A080359(n), and the sequence shows where the inequality becomes an equality.",
				"Let prime(m) \u003c a(n)/2 \u003c prime(m+1); then there exist primes p\u003cq such that p is in the interval (2*Prime(m), a(n)) and q is in the interval (a(n), 2*Prime(m+1)).",
				"For example, a(2) = 71, 31 \u003c a(2)/2 \u003c 37 and intervals (62,71), (71,74) contain the primes p = 67 and q = 73 respectively.",
				"Let us call a prime p compatible with another prime q, if the intervals (p/2,q/2) and (p,q], if q\u003ep, (or intervals (q/2,p/2) and (q,p], if q\u003cp) contain the same number of primes. If p is compatible with no other prime, we call it a peculiar prime. The sequence lists the peculiar primes. [_Vladimir Shevelev_, Apr 25 2012]"
			],
			"link": [
				"V. Shevelev, \u003ca href=\"http://arXiv.org/abs/0908.2319\"\u003eOn critical small intervals containing primes\u003c/a\u003e, arXiv:0908.2319 [math.NT] [From _Vladimir Shevelev_, Aug 20 2009]",
				"V. Shevelev, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL15/Shevelev/shevelev19.html\"\u003eRamanujan and Labos primes, their generalizations, and classifications of primes\u003c/a\u003e, J. Integer Seq. 15 (2012) Article 12.5.4",
				"J. Sondow, J. W. Nicholson, and T. D. Noe, \u003ca href=\"http://arxiv.org/abs/1105.2249\"\u003e Ramanujan Primes: Bounds, Runs, Twins, and Gaps\u003c/a\u003e, J. Integer Seq. 14 (2011) Article 11.6.2"
			],
			"formula": [
				"All solutions of the equation A104272(x)=A080359(x) are x=pi(a(n))-pi(a(n)/2). - Vladimir Shevelev, Apr 25 2012"
			],
			"example": [
				"a(2)=71, such that 31\u003c71/2\u003c37, and we see that p=67 is in interval (62, 71) and q=73 is in interval (71, 74)."
			],
			"mathematica": [
				"nn = 200; t = Table[0, {nn+1}]; s = 0;",
				"Do[If[PrimeQ[k], s++]; If[PrimeQ[k/2], s--]; If[s \u003c= nn \u0026\u0026 t[[s+1]] == 0, t[[s+1]] = k], {k, Prime[3nn]}",
				"];",
				"A080359 = Rest[t];",
				"R = Table[0, {nn}]; s = 0;",
				"Do[If[PrimeQ[k], s++]; If[PrimeQ[k/2], s--]; If[s \u003c nn, R[[s+1]] = k], {k, Prime[3nn]}",
				"];",
				"A104272 = R+1;",
				"Intersection[A104272, A080359] (* _Jean-François Alcover_, Oct 28 2018, after _T. D. Noe_ in A104272 *)"
			],
			"xref": [
				"Cf. A104272, A080359, A164368, A164333, A164288, A164294, A212493, A212541."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Vladimir Shevelev_, Aug 15 2009",
			"ext": [
				"Terms beyond 659 from _R. J. Mathar_, Dec 17 2009"
			],
			"references": 14,
			"revision": 27,
			"time": "2018-10-28T09:14:45-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}