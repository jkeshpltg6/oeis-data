{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000352",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 352,
			"id": "M3954 N1629",
			"data": "5,29,118,418,1383,4407,13736,42236,128761,390385,1179354,3554454,10696139,32153963,96592972,290041072,870647517,2612991141,7841070590,23527406090,70590606895,211788597919,635399348208,1906265153508",
			"name": "One half of the number of permutations of [n] such that the differences have three runs with the same signs.",
			"reference": [
				"L. Comtet, Advanced Combinatorics, Reidel, 1974, p. 260, #13",
				"F. N. David, M. G. Kendall and D. E. Barton, Symmetric Function and Allied Tables, Cambridge, 1966, p. 260.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A000352/b000352.txt\"\u003eTable of n, a(n) for n = 4..400\u003c/a\u003e",
				"E. Rodney Canfield and Herbert S. Wilf, \u003ca href=\"http://arXiv.org/abs/math.CO/0609704\"\u003eCounting permutations by their runs up and down\u003c/a\u003e, arXiv:math/0609704 [math.CO], 2006.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (7,-17,17,-6)."
			],
			"formula": [
				"a(n) = (3^n-4*2^n-2*n+11)/4, n\u003e=4. - _Tim Monahan_, Jul 14 2011",
				"G.f.: x^4*(5-6*x)/((1-3*x)*(1-2*x)*(1-x)^2).",
				"Limit_{n-\u003einfinity} 4*a(n)/3^n = 1. - _Philippe Deléham_, Feb 22 2004"
			],
			"example": [
				"a(4)=5 because the permutations of [4] with three sign runs are 1324, 1423, 2143, 2314, 2413 and their reversals."
			],
			"maple": [
				"A000352:=-(-5+6*z)/(3*z-1)/(2*z-1)/(z-1)**2; # [Conjectured by _Simon Plouffe_ in his 1992 dissertation.] [correct up to offset]",
				"a:= n-\u003e (Matrix([[0,0,1,2]]). Matrix(4, (i,j)-\u003e if (i=j-1) then 1 elif j=1 then [7,-17,17,-6][i] else 0 fi)^n)[1,4]: seq(a(n), n=4..27); # _Alois P. Heinz_, Aug 26 2008"
			],
			"mathematica": [
				"nn = 40; CoefficientList[Series[x^4*(5 - 6*x)/((1 - 3*x)*(1 - 2*x)*(1 - x)^2), {x, 0, nn}], x] (* _T. D. Noe_, Jun 19 2012 *)"
			],
			"program": [
				"(PARI) a(n) = (3^n-4*2^n-2*n+11)/4;"
			],
			"xref": [
				"a(n) = T(n, 3), where T(n, k) is the array defined in A008970.",
				"Cf. A000486, A000506."
			],
			"keyword": "nonn,changed",
			"offset": "4,1",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Edited by _Emeric Deutsch_, Feb 18 2004"
			],
			"references": 4,
			"revision": 59,
			"time": "2022-01-05T00:29:43-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}