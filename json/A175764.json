{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A175764",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 175764,
			"data": "1,0,9,1,0,1,2,1,1,1,1,1,8,1,2,1,4,1,1,1,1,1,9,1,5,1,3,1,0,1,1,1,3,1,2,1,6,1,1,1,1,1,5,1,2,1,10,1,1,1,1,1,1,1,9,1,10,1,1,1,1,1,1,1,2,1,6,1,1,1,1,1,10,1,9,1,5,1,1,1,1,1,2,1,2,1,6,1,1,1,1,1,5,1,2,1,3,1,1,1,1,1,11",
			"name": "Number of iterations of the mapping k-\u003ef(k) to reach one of 2, 5, or 29, starting with k=n, and with f(k)=(k^2+4)/d, where d is the next-to-largest divisor of k^2+4, or -1 if the sequence never reaches one of the required values.",
			"comment": [
				"It appears that the sequence always reaches 2, 5, or 29 for any initial value n. Is this easy to prove?",
				"It appears that a(n) is 1 whenever n\u003e29 and n mod 10 is one of {0,1,2,4,6,8,9}. This has been verified to n=5000. Also, it appears that a(n) is 9 whenever n mod 130 is one of {3,23,55,75,107,127}. This has also been verified to n=5000. Are these conjectures easy to prove?",
				"From _Antti Karttunen_, May 19 2021: (Start)",
				"Note that the first four terms of iteration 47017 -\u003e 2210598293 -\u003e 4886744813014513853 -\u003e 23880274867524255960728999629928905613 are all primes (see also A231120), but then (4+(23880274867524255960728999629928905613^2)) is composite, and its smallest prime divisor is 1946761. Actually, a(23880274867524255960728999629928905613) = 2, thus a(47017) = 5.",
				"(End)"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A175764/b175764.txt\"\u003eTable of n, a(n) for n = 1..47016\u003c/a\u003e"
			],
			"example": [
				"For n=3, we have 3 -\u003e (3^2+4)/d = 13/1 -\u003e (13^2+4)/d = 173/1 -\u003e (173^2+4)/d = 29933/809 = 37, since the divisors of 29933 are {1,37,809,29933}. Continuing, we get the orbit {3,13,173,37,1373,1217,97,9413,89,5,29,5,29,...}, showing that 5 is reached after 9 steps, after which the orbit is periodic {...,5,29,5,29,...}. Thus a(3)=9."
			],
			"program": [
				"(PARI)",
				"A175764(n) = if(2==n||5==n||29==n,0,1+A175764(f(n)));",
				"f(k) = { my(u=(4+(k^2)), ds=divisors(u)); (u/ds[#ds-1]); };",
				"\\\\ Alternatively, \"f\" could be defined as:",
				"f(k) = { my(u=(4+(k^2))); (u/A032742(u)); };",
				"A032742(n) = if(1==n||isprime(n),1,forprime(p=2,n,if(!(n%p),return(n/p)))); \\\\ And not requiring full factorization when this is used. - _Antti Karttunen_, May 19 2021"
			],
			"xref": [
				"Cf. A032742, A076423, A087717, A231120."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_John W. Layman_, Aug 30 2010",
			"references": 1,
			"revision": 13,
			"time": "2021-05-20T08:24:52-04:00",
			"created": "2010-09-12T03:00:00-04:00"
		}
	]
}