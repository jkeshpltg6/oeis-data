{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A334100",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 334100,
			"data": "1,2,3,4,5,7,8,6,9,19,16,10,11,21,43,32,12,13,23,47,127,64,17,14,27,49,129,283,128,20,15,29,57,133,301,659,256,24,18,31,59,139,329,817,1319,512,34,22,33,63,141,343,827,1699,3957,1024,40,25,35,67,147,347,839,1787,4079,9227,2048,48,26,37,69,161,361,849,1849,4613,9233,21599",
			"name": "Square array where the row n lists all numbers k for which A329697(k) = n, read by falling antidiagonals.",
			"comment": [
				"Array is read by descending antidiagonals with (n,k) = (0,1), (0,2), (1,1), (0,3), (1,2), (2,1), ... where A(n,k) is the k-th solution x to A329697(x) = n. The row indexing (n) starts from 0, and column indexing (k) from 1.",
				"Any odd prime that appears on row n is 1+{some term on row n-1}.",
				"The e-th powers of the terms on row n form a subset of terms on row (e*n). More generally, a product of terms that occur on rows i_1, i_2, ..., i_k can be found at row (i_1 + i_2 + ... + i_k), because A329697 is completely additive.",
				"The binary weight (A000120) of any term on row n is at most 2^n."
			],
			"link": [
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"example": [
				"The top left corner of the array:",
				"  n\\k |    1     2     3     4     5     6     7     8     9    10",
				"------+----------------------------------------------------------------",
				"   0  |    1,    2,    4,    8,   16,   32,   64,  128,  256,  512, ...",
				"   1  |    3,    5,    6,   10,   12,   17,   20,   24,   34,   40, ...",
				"   2  |    7,    9,   11,   13,   14,   15,   18,   22,   25,   26, ...",
				"   3  |   19,   21,   23,   27,   29,   31,   33,   35,   37,   38, ...",
				"   4  |   43,   47,   49,   57,   59,   63,   67,   69,   71,   77, ...",
				"   5  |  127,  129,  133,  139,  141,  147,  161,  163,  171,  173, ...",
				"   6  |  283,  301,  329,  343,  347,  361,  379,  381,  383,  387, ...",
				"   7  |  659,  817,  827,  839,  849,  863,  883,  889,  893,  903, ...",
				"   8  | 1319, 1699, 1787, 1849, 1977, 1979, 1981, 2021, 2039, 2083, ...",
				"   9  | 3957, 4079, 4613, 4903, 5097, 5179, 5361, 5377, 5399, 5419, ...",
				"etc.",
				"Note that the row 9 is the first one which begins with composite, as 3957 = 3*1319. The next such rows are row 15 and row 22. See A334099."
			],
			"mathematica": [
				"Block[{nn = 16, s}, s = Values@ PositionIndex@ Array[-1 + Length@ NestWhileList[# - #/FactorInteger[#][[-1, 1]] \u0026, #, # != 2^IntegerExponent[#, 2] \u0026] \u0026, 2^nn]; Table[s[[#, k]] \u0026[m - k + 1], {m, nn - Ceiling[nn/4]}, {k, m, 1, -1}]] // Flatten (* _Michael De Vlieger_, Apr 30 2020 *)"
			],
			"program": [
				"(PARI)",
				"up_to = 105; \\\\ up_to = 1081; \\\\ = binomial(46+1,2)",
				"A329697(n) = if(!bitand(n,n-1),0,1+A329697(n-(n/vecmax(factor(n)[, 1]))));",
				"memoA334100sq = Map();",
				"A334100sq(n, k) = { my(v=0); if(!mapisdefined(memoA334100sq,[n,k-1],\u0026v),if(1==k, v=0, v = A334100sq(n, k-1))); for(i=1+v,oo,if(A329697(i)==(n-1),mapput(memoA334100sq,[n,k],i); return(i))); };",
				"A334100list(up_to) = { my(v = vector(up_to), i=0); for(a=1,oo, for(col=1,a, i++; if(i \u003e up_to, return(v)); v[i] = A334100sq(col,(a-(col-1))))); (v); };",
				"v334100 = A334100list(up_to);",
				"A334100(n) = v334100[n];"
			],
			"xref": [
				"Cf. A329697.",
				"Cf. A334099 (the leftmost column).",
				"Cf. A000079, A334101, A334102, A334103, A334104, A334105, A334106 for the rows 0-6.",
				"Cf. A019434, A334092, A334093, A334094, A334095, A334096 for the primes on the rows 1-6.",
				"Cf. also irregular triangle A334111."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Antti Karttunen_, Apr 14 2020",
			"references": 14,
			"revision": 26,
			"time": "2020-05-27T02:11:38-04:00",
			"created": "2020-05-27T02:11:38-04:00"
		}
	]
}