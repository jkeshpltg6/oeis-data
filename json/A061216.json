{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A061216",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 61216,
			"data": "1,4,6,80,12,224,18,440,17472,30,39168,1520,42,2024,124800,175392,60,261888,4760,72,438672,6560,635712,74718720,9800,102,11024,108,12320,356925975275520,16640,2405568,138,61857653760,150,3651648,4095360",
			"name": "a(n) = product of all even numbers between n-th prime and (n+1)-st prime.",
			"comment": [
				"Previous name used \"even composite numbers\", but if an even number is strictly between two primes, it is composite. So the word 'composite' isn't needed in the title. - _David A. Corneth_, Aug 21 2016"
			],
			"link": [
				"Harry J. Smith, \u003ca href=\"/A061216/b061216.txt\"\u003eTable of n, a(n) for n = 1..2000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 2^((prime(n+1)-prime(n))/2) * ((prime(n+1)-1)/2)!/(prime(n)-1)/2)! for n \u003e= 2. - _Robert Israel_, Aug 28 2016"
			],
			"example": [
				"a(4) = 80 = 8 * 10, as 7 is the 4th prime and 11 is the 5th prime.",
				"a(9) = 17472. Let p_(n) = prime(n). p_(9) = 23, p_(10) = 29. The number of even numbers between 23 and 29 is floor((29 - 23) / 2) = 3. So a(9) is 2^3 * (23 + 1)/2 * ... * (29 - 1)/2 = 17472. - _David A. Corneth_, Aug 21 2016"
			],
			"maple": [
				"f:= proc(n) local p,q;",
				"  p:= ithprime(n); q:= ithprime(n+1);",
				"  2^((q-p)/2)*floor(q/2)!/floor(p/2)!",
				"end proc:",
				"f(1):= 1:",
				"map(f, [$1..100]); # _Robert Israel_, Aug 28 2016"
			],
			"mathematica": [
				"f[n_]:=Module[{pn=Prime[n],pn1=Prime[n+1]},Times@@Range[pn+1,pn1,2]]; Table[f[i], {i, 45}] (* _Harvey P. Dale_, Jan 16 2011 *)"
			],
			"program": [
				"(PARI) for(n=1,50,p=1;for(k=prime(n)+1, prime(n+1)-1,if(k%2==0,p=p*k));print1(p\",\"))",
				"(PARI) n=0; q=2; forprime (p=3, prime(2001), a=1; for (i=q + 1, p - 1, if (i%2==0, a*=i)); q=p; write(\"b061216.txt\", n++, \" \", a) ) \\\\ _Harry J. Smith_, Jul 19 2009",
				"(PARI) a(n) = {my(p1 = prime(n), p2 = nextprime(p1 + 1));",
				"2^((p2-p1)\\2) * prod(i=(p1+1)\\2,(p2-1)\\2,i)} \\\\ _David A. Corneth_, Aug 21 2016"
			],
			"xref": [
				"Cf. A061214, A061215."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Amarnath Murthy_, Apr 22 2001",
			"ext": [
				"Corrected and extended by _Ralf Stephan_, Mar 22 2003",
				"Name simplified by _David A. Corneth_, Aug 21 2016"
			],
			"references": 1,
			"revision": 33,
			"time": "2020-03-22T07:59:45-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}