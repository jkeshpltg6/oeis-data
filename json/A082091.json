{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A082091",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 82091,
			"data": "1,2,2,3,2,2,2,3,3,2,2,3,2,2,2,4,2,3,2,3,2,2,2,3,3,2,3,3,2,2,2,3,2,2,2,4,2,2,2,3,2,2,2,3,3,2,2,4,3,3,2,3,2,3,2,3,2,2,2,3,2,2,3,3,2,2,2,3,2,2,2,3,2,2,3,3,2,2,2,4,4,2,2,3,2,2,2,3,2,3,2,3,2,2,2,3,2,3,3,4,2,2,2,3,2,2",
			"name": "a(n) = one more than the number of iterations of A005361 needed to reach 1 from the starting value n.",
			"link": [
				"Antti Karttunen, \u003ca href=\"/A082091/b082091.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"\u003ca href=\"/index/Eu#epf\"\u003eIndex entries for sequences computed from exponents in factorization of n\u003c/a\u003e"
			],
			"formula": [
				"a(1) = 1, and for n \u003e 1, a(n) = 1 + a(A005361(n))."
			],
			"example": [
				"For n = 2 = 2^1, A005361(2) = 1, so we reach 1 in one step, and thus a(2) = 1+1 = 2.",
				"For n = 4 = 2^2, A005361(4) = 2; A005361(2) = 1, so we reach 1 in two steps, and thus a(4) = 2+1 = 3.",
				"For n = 6 = 2^1 * 3^1, A005361(6) = 1*1 = 1, so we reach 1 in one step, and thus a(6) = 1+1 = 2.",
				"For n = 64 = 2^6, A005361(64) = 6, thus a(64) = 1 + a(6) = 3.",
				"For n = 10! = 3628800 = 2^8 * 3^4 * 5^2 * 7*1, A005361(3628800) = 64, thus a(3628800) = 1 + a(64) = 4."
			],
			"mathematica": [
				"ffi[x_] := Flatten[FactorInteger[x]] lf[x_] := Length[FactorInteger[x]] ep[x_] := Table[Part[ffi[x], 2*w], {w, 1, lf[x]}] expr[x_] := Apply[Times, ep[x]] Table[Length[FixedPointList[expr, w]]-1, {w, 2, 128}]",
				"(* Second program: *)",
				"Table[Length@ NestWhileList[Apply[Times, FactorInteger[#][[All, -1]]] \u0026, n, # != 1 \u0026], {n, 105}] (* _Michael De Vlieger_, Jul 29 2017 *)"
			],
			"program": [
				"(PARI)",
				"A005361(n) = factorback(factor(n)[, 2]); \\\\ This function from _Charles R Greathouse IV_, Nov 07 2014",
				"A082091(n) = if(1==n,1,1+A082091(A005361(n))); \\\\ _Antti Karttunen_, Jul 28 2017",
				"(PARI) first(n) = my(v = vector(n)); v[1] = 1; for(i=2, n, v[i] = v[factorback(factor(i)[, 2])] + 1); v \\\\ _David A. Corneth_, Jul 28 2017",
				"(Scheme) (define (A082091 n) (if (= 1 n) n (+ 1 (A082091 (A005361 n))))) ;; _Antti Karttunen_, Jul 28 2017"
			],
			"xref": [
				"Cf. A001414, A005361, A008475, A056239, A082083-A082086, A082090."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Labos Elemer_, Apr 09 2003",
			"ext": [
				"Term a(1)=1 prepended, Name and Example sections edited by _Antti Karttunen_, Jul 28 2017"
			],
			"references": 2,
			"revision": 20,
			"time": "2017-08-02T12:02:11-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}