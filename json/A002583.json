{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002583",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2583,
			"id": "M0294 N0312",
			"data": "2,2,3,7,5,11,103,71,661,269,329891,39916801,2834329,75024347,3790360487,46271341,1059511,1000357,123610951,1713311273363831,117876683047,2703875815783,93799610095769647,148139754736864591,765041185860961084291,38681321803817920159601",
			"name": "Largest prime factor of n! + 1.",
			"comment": [
				"Theorem: For any N, there is a prime \u003e N. Proof: Consider any prime factor of N!+1.",
				"Cf. Wilson's theorem (1770): p | (p-1)! + 1 iff p is a prime.",
				"If n is in A002981, then a(n) = n!+1. - _Chai Wah Wu_, Jul 15 2019"
			],
			"reference": [
				"M. Kraitchik, On the divisibility of factorials, Scripta Math., 14 (1948), 24-26 (but beware errors).",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Georg Fischer, \u003ca href=\"/A002583/b002583.txt\"\u003eTable of n, a(n) for n = 0..139\u003c/a\u003e (first 101 terms originally derived from Hisanori Mishima's data by T. D. Noe)",
				"A. Borning, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1972-0308018-5 \"\u003eSome results for k!+-1 and 2.3.5...p+-1\u003c/a\u003e, Math. Comp., 26 (1972), 567-570.",
				"P. Erdős and C. L. Stewart, \u003ca href=\"http://www.renyi.hu/~p_erdos/1976-27.pdf\"\u003eOn the greatest and least prime factors of n! + 1\u003c/a\u003e, J. London Math. Soc. (2) 13:3 (1976), pp. 513-519.",
				"M. Kraitchik, \u003ca href=\"/A002582/a002582.pdf\"\u003eOn the divisibility of factorials\u003c/a\u003e, Scripta Math., 14 (1948), 24-26 (but beware errors). [Annotated scanned copy]",
				"Li Lai, \u003ca href=\"https://arxiv.org/abs/2103.14894\"\u003eOn the largest prime divisor of n! + 1\u003c/a\u003e, arXiv:2103.14894 [math.NT], 2021.",
				"Hisanori Mishima, \u003ca href=\"http://www.asahi-net.or.jp/~KC2H-MSM/mathland/matha1/matha102.htm\"\u003eFactorizations of many number sequences\u003c/a\u003e",
				"Hisanori Mishima, \u003ca href=\"http://www.asahi-net.or.jp/~KC2H-MSM/mathland/matha1/matha104.htm\"\u003eFactorizations of many number sequences\u003c/a\u003e",
				"H. P. Robinson and N. J. A. Sloane, \u003ca href=\"/A002037/a002037.pdf\"\u003eCorrespondence, 1971-1972\u003c/a\u003e",
				"Blake C. Stacey, \u003ca href=\"https://doi.org/10.1007/978-3-030-76104-2_1\"\u003eEquiangular Lines\u003c/a\u003e, Ch. 1, A First Course in the Sporadic SICs, SpringerBriefs in Math. Phys. (2021) Vol. 41, see page 5.",
				"R. G. Wilson v, \u003ca href=\"/A038507/a038507.txt\"\u003eExplicit factorizations\u003c/a\u003e"
			],
			"formula": [
				"Erdős \u0026 Stewart show that a(n) \u003e n + (1-o(1))log n/log log n and lim sup a(n)/n \u003e 2. - _Charles R Greathouse IV_, Dec 05 2012",
				"Lai proves that lim sup a(n)/n \u003e 7.238. - _Charles R Greathouse IV_, Jun 22 2021"
			],
			"example": [
				"(0!+1)=[2], (1!+1)=[2], (2!+1)=[3], (3!+1)=[7], (4!+1)=25=5*[5], (5!+1)=121=11*[11], (6!+1)=721=7*[103], (7!+1)=5041=71*[71], etc. - Mitch Cervinka (puritan(AT)toast.net), May 11 2009"
			],
			"mathematica": [
				"PrimeFactors[n_]:=Flatten[Table[ #[[1]],{1}]\u0026/@FactorInteger[n]]; Table[PrimeFactors[n!+1][[ -1]],{n,0,35}] ..and/or.. Table[FactorInteger[n!+1,FactorComplete-\u003eTrue][[ -1,1]],{n,0,35}] (* _Vladimir Joseph Stephan Orlovsky_, Aug 12 2009 *)",
				"FactorInteger[#][[-1,1]]\u0026/@(Range[0,30]!+1) (* _Harvey P. Dale_, Sep 04 2017 *)"
			],
			"program": [
				"(PARI) a(n)=my(f=factor(n!+1)[,1]);f[#f] \\\\ _Charles R Greathouse IV_, Dec 05 2012",
				"(MAGMA) [Maximum(PrimeDivisors(Factorial(n)+1)): n in [0..30]]; // _Vincenzo Librandi_, Feb 14 2020"
			],
			"xref": [
				"Cf. A002582, A002981, A038507, A051301, A056111, A096225."
			],
			"keyword": "nonn,nice",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Robert G. Wilson v_, Aug 01 2000",
				"Corrected by _Jud McCranie_, Jan 03 2001"
			],
			"references": 11,
			"revision": 57,
			"time": "2021-09-15T11:01:59-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}