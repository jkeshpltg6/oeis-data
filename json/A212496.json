{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A212496",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 212496,
			"data": "-1,-2,-1,0,1,2,3,2,1,2,3,2,3,4,3,4,5,4,5,4,3,4,5,6,5,6,7,6,7,6,7,6,5,6,5,6,7,8,7,8,9,8,9,8,9,10,11,10,9,8,7,6,7,8,7,8,7,8,9,10",
			"name": "a(n) = Sum_{k=1..n} (-1)^{k-Omega(k)} with Omega(k) the total number of prime factors of k (counted with multiplicity).",
			"comment": [
				"On May 16 2012, _Zhi-Wei Sun_ conjectured that a(n) is positive for each n\u003e4. He has verified this for n up to 10^{10}, and shown that the conjecture implies the famous Riemann Hypothesis. Moreover, he guessed that a(n)\u003esqrt(n) for any n\u003e324 (and also a(n)\u003csqrt(n)log(log(n)) for n\u003e5892); this implies that the sequence contains all natural numbers.",
				"Sun also conjectured that b(n)=sum_{k=1}^n(-1)^{k-Omega(k)}/k\u003c0 for all n=1,2,3,..., and verified this for n up to 2*10^9. Moreover, he guessed that b(n)\u003c-1/sqrt(n) for all n\u003e1, and b(n)\u003e-log(log(n))/sqrt(n) for n\u003e2008."
			],
			"link": [
				"Zhi-Wei Sun,  \u003ca href=\"/A212496/b212496.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1204.6689\"\u003eOn a pair of zeta functions\u003c/a\u003e, preprint, arxiv:1204.6689.",
				"Zhi-Wei Sun, \u003ca href=\"http://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;b3990068.1205\"\u003eOn the parities of Omega(n)-n\u003c/a\u003e, a message to Number Theory List, May 18, 2012.",
				"Zhi-Wei Sun, \u003ca href=\"http://math.nju.edu.cn/~zwsun/b212496.rar\"\u003eTable of n, a(n) for n = 1..10^7\u003c/a\u003e (rar-compressed)"
			],
			"example": [
				"We have a(4)=0 since (-1)^{1-Omega(1)} + (-1)^{2-Omega(2)} + (-1)^{3-Omega(3)} + (-1)^{4-Omega(4)} = -1 - 1 + 1 + 1 = 0."
			],
			"mathematica": [
				"PrimeDivisor[n_]:=Part[Transpose[FactorInteger[n]],1]",
				"Omega[n_]:=If[n==1,0,Sum[IntegerExponent[n,Part[PrimeDivisor[n],i]],{i,1,Length[PrimeDivisor[n]]}]]",
				"s[0]=0",
				"s[n_]:=s[n]=s[n-1]+(-1)^(n-Omega[n])",
				"Do[Print[n,\" \",s[n]],{n,1,100000}]",
				"Accumulate[Table[(-1)^(n-PrimeOmega[n]),{n,1000}]] (* _Harvey P. Dale_, Oct 07 2013 *)"
			],
			"program": [
				"(PARI) a(n)=sum(k=1,n, (-1)^(bigomega(k)+k)) \\\\ _Charles R Greathouse IV_, Jul 31 2016"
			],
			"xref": [
				"Cf. A008836, A002819."
			],
			"keyword": "sign,nice",
			"offset": "1,2",
			"author": "_Zhi-Wei Sun_, May 19 2012",
			"references": 1,
			"revision": 39,
			"time": "2016-07-31T01:47:05-04:00",
			"created": "2012-05-19T09:46:12-04:00"
		}
	]
}