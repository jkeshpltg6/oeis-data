{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A276428",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 276428,
			"data": "0,1,0,1,2,3,3,6,7,12,15,22,27,40,49,68,87,116,145,193,239,311,387,494,611,776,952,1193,1464,1817,2214,2733,3315,4060,4911,5974,7195,8713,10448,12585,15048,18039,21486,25660,30462,36231,42888,50820,59972,70843,83354",
			"name": "Sum over all partitions of n of the number of distinct parts i of multiplicity i.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A276428/b276428.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Sum_{k\u003e=0} k*A276427(n,k).",
				"G.f.: g(x) = Sum_{i\u003e=1} (x^{i^2}*(1-x^i))/Product_{i\u003e=1} (1-x^i)."
			],
			"example": [
				"a(5) = 3 because in the partitions [1,1,1,1,1], [1,1,1,2], [1',2',2], [1,1,3], [2,3], [1',4], [5] of 5 only the marked parts satisfy the requirement."
			],
			"maple": [
				"g := (sum(x^(i^2)*(1-x^i), i = 1 .. 200))/(product(1-x^i, i = 1 .. 200)): gser := series(g, x = 0, 53): seq(coeff(gser, x, n), n = 0 .. 50);",
				"# second Maple program:",
				"b:= proc(n, i) option remember; `if`(n=0, [1, 0],",
				"     `if`(i\u003c1, 0, add((p-\u003e p+`if`(i\u003c\u003ej, 0,",
				"      [0, p[1]]))(b(n-i*j, i-1)), j=0..n/i)))",
				"    end:",
				"a:= n-\u003e b(n$2)[2]:",
				"seq(a(n), n=0..60);  # _Alois P. Heinz_, Sep 19 2016"
			],
			"mathematica": [
				"b[n_, i_] := b[n, i] = Expand[If[n==0, 1, If[i\u003c1, 0, Sum[If[i==j, x, 1]*b[n - i*j, i-1], {j, 0, n/i}]]]]; T[n_] := Function[p, Table[Coefficient[p, x, i], {i, 0, Exponent[p, x]}]][b[n, n]]; a[n_] := (row = T[n]; row.Range[0, Length[row]-1]); Table[a[n], {n, 0, 60}] // Flatten (* _Jean-François Alcover_, Nov 28 2016, after _Alois P. Heinz_'s Maple code for A276427 *)"
			],
			"program": [
				"(PARI) apply( A276428(n,s,c)={forpart(p=n,c=1;for(i=1,#p,p[i]==if(i\u003c#p, p[i+1])\u0026\u0026c++\u0026\u0026next; c==p[i]\u0026\u0026s++; c=1));s}, [0..20]) \\\\ _M. F. Hasler_, Oct 27 2019"
			],
			"xref": [
				"Cf. A276427, A276434, A277101; A114638, A116861."
			],
			"keyword": "nonn",
			"offset": "0,5",
			"author": "_Emeric Deutsch_, Sep 19 2016",
			"references": 10,
			"revision": 19,
			"time": "2019-10-27T20:58:31-04:00",
			"created": "2016-09-19T19:41:40-04:00"
		}
	]
}