{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005177",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5177,
			"id": "M0347",
			"data": "1,1,1,1,2,2,5,4,17,22,167,539,18979,389436,50314796,2942198440,1698517036411,442786966115560,649978211591600286,429712868499646474880,2886054228478618211088773,8835589045148342277771518309,152929279364927228928021274993215,1207932509391069805495173301992815105,99162609848561525198669168640159162918815",
			"name": "Number of connected regular graphs with n nodes.",
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"E. Friedman, \u003ca href=\"/A000088/a000088a.gif\"\u003eIllustration of small graphs\u003c/a\u003e",
				"Daniel R. Herber, \u003ca href=\"https://www.engr.colostate.edu/~drherber/files/Herber2020b.pdf\"\u003eEnhancements to the perfect matching approach for graph enumeration-based engineering challenges\u003c/a\u003e, Proceedings of the ASME 2020 International Design Engineering Technical Conferences and Computers and Information in Engineering Conference (IDETC/CIE 2020).",
				"Jason Kimberley, \u003ca href=\"/wiki/User:Jason_Kimberley/C_k-reg_girth_ge_g_index\"\u003eIndex of sequences counting connected k-regular simple graphs with girth at least g\u003c/a\u003e",
				"Markus Meringer, \u003ca href=\"http://www.mathe2.uni-bayreuth.de/markus/reggraphs.html\"\u003eGENREG: A program for Connected Regular Graphs\u003c/a\u003e",
				"M. Meringer, \u003ca href=\"http://dx.doi.org/10.1002/(SICI)1097-0118(199902)30:2\u0026lt;137::AID-JGT7\u0026gt;3.0.CO;2-G\"\u003eFast generation of regular graphs and construction of cages\u003c/a\u003e, J. Graph Theory 30 (2) (1999) 137-146. [_Jason Kimberley_, Sep 23 2009]",
				"Peter Steinbach, \u003ca href=\"/A000088/a000088_17.pdf\"\u003eField Guide to Simple Graphs, Volume 1\u003c/a\u003e, Part 17 (For Volumes 1, 2, 3, 4 of this book see A000088, A008406, A000055, A000664, respectively.)",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RegularGraph.html\"\u003eRegular Graph.\u003c/a\u003e"
			],
			"formula": [
				"a(n) = sum of the n-th row of A068934.",
				"a(n) = A165647(n) - A165648(n).",
				"This sequence is the inverse Euler transformation of A165647."
			],
			"xref": [
				"Regular simple graphs of any degree: this sequence (connected), A068932 (disconnected), A005176 (not necessarily connected), A275420 (multisets).",
				"Connected regular graphs of any degree with girth at least g: this sequence (g=3), A186724 (g=4), A186725 (g=5), A186726 (g=6), A186727 (g=7), A186728 (g=8), A186729 (g=9).",
				"Connected regular simple graphs: this sequence (any degree), A068934 (triangular array); specified degree k: A002851 (k=3), A006820 (k=4), A006821 (k=5), A006822 (k=6), A014377 (k=7), A014378 (k=8), A014381 (k=9), A014382 (k=10), A014384 (k=11). - _Jason Kimberley_, Nov 03 2011"
			],
			"keyword": "nonn,nice,hard",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _David Wasserman_, Mar 08 2002",
				"a(15) from _Giovanni Resta_, Feb 05 2009",
				"Terms are sums of the output from M. Meringer's genreg software. To complete a(16) it was run by _Jason Kimberley_, Sep 23 2009",
				"a(0)=1 (due to the empty graph being vacuously connected and regular) inserted by _Jason Kimberley_, Apr 11 2012",
				"a(17)-a(21) from _Andrew Howroyd_, Mar 10 2020",
				"a(22)-a(24) from _Andrew Howroyd_, May 19 2020"
			],
			"references": 30,
			"revision": 56,
			"time": "2020-10-14T23:11:35-04:00",
			"created": "1991-05-16T03:00:00-04:00"
		}
	]
}