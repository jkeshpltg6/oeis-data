{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A113301",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 113301,
			"data": "0,1,5,18,62,211,715,2420,8188,27701,93713,317030,1072506,3628263,12274327,41523752,140473848,475219625,1607656477,5438662906,18398864822,62242913851,210566269283,712340586524,2409830942708,8152399683933,27579370581033,93300342369742",
			"name": "Sum of odd-indexed terms of tribonacci numbers.",
			"comment": [
				"A000073 is the tribonacci numbers. A113300 is the sum of even-indexed terms of tribonacci numbers. A099463 is the bisection of the tribonacci numbers. A113300(n) + A113301(n) = cumulative sum of tribonacci numbers = A008937(n). Primes in A113300 include a(2) = 5, a(5) = 211, a(9) = 27701, .... A113300 is semiprime for n = 4, 10, 14, ..."
			],
			"link": [
				"Harvey P. Dale, \u003ca href=\"/A113301/b113301.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-2,0,-1)."
			],
			"formula": [
				"a(n) = Sum_{j=0..n} A000073(2*j+1).",
				"a(n) + A113300(n) = A008937(n).",
				"a(n) = 4*a(n-1) - 2*a(n-2) - a(n-4), a(0)=0, a(1)=1, a(2)=5, a(3)=18. - _Harvey P. Dale_, Apr 12 2013",
				"G.f.: x*(1+x) / ((1-x)*(1-3*x-x^2-x^3)). - _Colin Barker_, May 06 2013"
			],
			"example": [
				"a(0) = 0 = A000073(1);",
				"a(1) = 0+1 = A000073(1) + A000073(3) = 1;",
				"a(2) = 0+1+4 = A000073(1) + A000073(3) + A000073(5) = 5, prime;",
				"a(3) = 0+1+4+13 = A000073(1) + A000073(3) + A000073(5) + A000073(7) = 18;",
				"a(4) = 0+1+4+13+44 = A000073(1) + A000073(3) + A000073(5) + A000073(7) + A000073(9) = 62 = 2 * 31, semiprime;",
				"a(5) = 0+1+4+13+44+149 = A000073(1) + A000073(3) + A000073(5) + A000073(7) + A000073(9) + A000073(11) = 211, prime."
			],
			"mathematica": [
				"Accumulate[Take[LinearRecurrence[{1,1,1},{0,1,1},40],{1,-1,2}]] (* or *) LinearRecurrence[{4,-2,0,-1},{0,1,5,18},30] (* _Harvey P. Dale_, Apr 12 2013 *)"
			],
			"program": [
				"(MAGMA) I:=[0,1,5,18]; [n le 4 select I[n] else 4*Self(n-1) - 2*Self(n-2) -Self(n-4): n in [1..41]]; // _G. C. Greubel_, Nov 20 2021",
				"(Sage)",
				"@CachedFunction",
				"def T(n): # A000073",
				"    if (n\u003c2): return 0",
				"    elif (n==2): return 1",
				"    else: return T(n-1) +T(n-2) +T(n-3)",
				"def A113301(n): return sum(T(2*j+1) for j in (0..n))",
				"[A113301(n) for n in (0..40)] # _G. C. Greubel_, Nov 20 2021"
			],
			"xref": [
				"Cf. A000073, A008937, A099463, A113300."
			],
			"keyword": "easy,nonn",
			"offset": "0,3",
			"author": "_Jonathan Vos Post_, Oct 24 2005",
			"ext": [
				"More terms from _Colin Barker_, May 06 2013"
			],
			"references": 2,
			"revision": 19,
			"time": "2021-11-21T07:36:50-05:00",
			"created": "2006-01-24T03:00:00-05:00"
		}
	]
}