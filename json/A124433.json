{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A124433",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 124433,
			"data": "1,0,-1,0,-1,0,-1,1,0,-1,0,-1,2,0,-1,0,-1,2,-1,0,-1,1,0,-1,2,0,-1,0,-1,4,-3,0,-1,0,-1,2,0,-1,2,0,-1,3,-3,1,0,-1,0,-1,4,-3,0,-1,0,-1,4,-3,0,-1,2,0,-1,2,0,-1,0,-1,6,-9,4,0,-1,1,0,-1,2,0,-1,2,-1,0,-1,4,-3,0,-1,0,-1,6,-6,0,-1,0,-1,4,-6,4,-1,0,-1,2,0,-1,2,0,-1",
			"name": "Irregular array {a(n,m)} read by rows where (sum{n\u003e=1} sum{m=1 to A001222(n)+1} a(n,m)*y^m/n^x) = 1/(zeta(x)-1+1/y) for all x and y where the double sum converges.",
			"comment": [
				"Row n has A001222(n)+1 terms. The polynomial P_n(y) = (sum{m=1 to A001222(n)+1} a(n,m)*y^m) is a generalization of the Mobius (Moebius) function, where P_n(1) = A008683(n).",
				"From _Gus Wiseman_, Aug 24 2020: (Start)",
				"Up to sign, also the number of strict length-k chains of divisors from n to 1, 1 \u003c= k \u003c= 1 + A001222(n). For example, row n = 36 counts the following chains (empty column indicated by dot):",
				"  .  36/1  36/2/1   36/4/2/1   36/12/4/2/1",
				"           36/3/1   36/6/2/1   36/12/6/2/1",
				"           36/4/1   36/6/3/1   36/12/6/3/1",
				"           36/6/1   36/9/3/1   36/18/6/2/1",
				"           36/9/1   36/12/2/1  36/18/6/3/1",
				"           36/12/1  36/12/3/1  36/18/9/3/1",
				"           36/18/1  36/12/4/1",
				"                    36/12/6/1",
				"                    36/18/2/1",
				"                    36/18/3/1",
				"                    36/18/6/1",
				"                    36/18/9/1",
				"(End)"
			],
			"link": [
				"Mohammad K. Azarian, \u003ca href=\"https://www.jstor.org/stable/2686918\"\u003eA Double Sum, Problem 440\u003c/a\u003e, College Mathematics Journal, Vol. 21, No. 5, Nov. 1990, p. 424. \u003ca href=\"https://www.jstor.org/stable/2686609\"\u003eSolution\u003c/a\u003e published in Vol. 22. No. 5, Nov. 1991, pp. 448-449."
			],
			"formula": [
				"a(1,1)=1. a(n,1) = 0 for n\u003e=2. a(n,m+1) = -sum{k|n,k \u003c n} a(k,m), where, for the purpose of this sum, a(k,m) = 0 if m \u003e A001222(k)+1."
			],
			"example": [
				"1/(zeta(x) - 1 + 1/y) = y - y^2/2^x - y^2/3^x + ( - y^2 + y^3)/4^x - y^2/5^x + ( - y^2 + 2y^3)/6^x - y^2/7^x + ...",
				"From _Gus Wiseman_, Aug 24 2020: (Start)",
				"The sequence of rows begins:",
				"     1: 1              16: 0 -1 3 -3 1     31: 0 -1",
				"     2: 0 -1           17: 0 -1            32: 0 -1 4 -6 4 -1",
				"     3: 0 -1           18: 0 -1 4 -3       33: 0 -1 2",
				"     4: 0 -1 1         19: 0 -1            34: 0 -1 2",
				"     5: 0 -1           20: 0 -1 4 -3       35: 0 -1 2",
				"     6: 0 -1 2         21: 0 -1 2          36: 0 -1 7 -12 6",
				"     7: 0 -1           22: 0 -1 2          37: 0 -1",
				"     8: 0 -1 2 -1      23: 0 -1            38: 0 -1 2",
				"     9: 0 -1 1         24: 0 -1 6 -9 4     39: 0 -1 2",
				"    10: 0 -1 2         25: 0 -1 1          40: 0 -1 6 -9 4",
				"    11: 0 -1           26: 0 -1 2          41: 0 -1",
				"    12: 0 -1 4 -3      27: 0 -1 2 -1       42: 0 -1 6 -6",
				"    13: 0 -1           28: 0 -1 4 -3       43: 0 -1",
				"    14: 0 -1 2         29: 0 -1            44: 0 -1 4 -3",
				"    15: 0 -1 2         30: 0 -1 6 -6       45: 0 -1 4 -3",
				"(End)"
			],
			"mathematica": [
				"f[l_List] := Block[{n = Length[l] + 1, c},c = Plus @@ Last /@ FactorInteger[n];Append[l, Prepend[ -Plus @@ Pick[PadRight[ #, c] \u0026 /@ l, Mod[n, Range[n - 1]], 0],0]]];Nest[f, {{1}}, 34] // Flatten(* _Ray Chandler_, Feb 13 2007 *)",
				"chnsc[n_]:=If[n==1,{{}},Prepend[Join@@Table[Prepend[#,n]\u0026/@chnsc[d],{d,DeleteCases[Divisors[n],1|n]}],{n}]];",
				"Table[(-1)^k*Length[Select[chnsc[n],Length[#]==k\u0026]],{n,30},{k,0,PrimeOmega[n]}] (* _Gus Wiseman_, Aug 24 2020 *)"
			],
			"xref": [
				"A008480 gives rows ends (up to sign).",
				"A008683 gives row sums (the Moebius function).",
				"A073093 gives row lengths.",
				"A074206 gives unsigned row sums.",
				"A097805 is the restriction to powers of 2 (up to sign).",
				"A251683 is the unsigned version with zeros removed.",
				"A334996 is the unsigned version (except with a(1) = 0).",
				"A334997 is an unsigned non-strict version.",
				"A337107 is the restriction to factorial numbers.",
				"A001055 counts factorizations.",
				"A001222 counts prime factors with multiplicity.",
				"A067824 counts strict chains of divisors starting with n.",
				"A074206 counts strict chains of divisors from n to 1.",
				"A122651 counts strict chains of divisors summing to n.",
				"A167865 counts strict chains of divisors \u003e 1 summing to n.",
				"A253249 counts strict chains of divisors.",
				"A337105 counts strict chains of divisors from n! to 1.",
				"Cf. A000005, A001221, A002033, A124010, A167865."
			],
			"keyword": "sign,tabf",
			"offset": "1,13",
			"author": "_Leroy Quet_, Dec 15 2006",
			"ext": [
				"Extended by _Ray Chandler_, Feb 13 2007"
			],
			"references": 4,
			"revision": 22,
			"time": "2020-08-25T09:45:55-04:00",
			"created": "2007-01-12T03:00:00-05:00"
		}
	]
}