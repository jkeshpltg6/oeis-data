{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A238745",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 238745,
			"data": "1,2,2,4,2,3,2,8,4,3,2,6,2,3,3,16,2,6,2,6,3,3,2,12,4,3,8,6,2,5,2,32,3,3,3,9,2,3,3,12,2,5,2,6,6,3,2,24,4,6,3,6,2,12,3,12,3,3,2,10,2,3,6,64,3,5,2,6,3,5,2,18,2,3,6,6,3,5,2,24,16,3,2",
			"name": "a(1) = 1; for n \u003e 1, if the first integer with the same prime signature as n is factorized into primorials as Product A002110(i)^e(i), then a(n) = Product prime(i)^e(i).",
			"comment": [
				"Alternate definition: a(1) = 1; for n \u003e 1, if row n of table A238744 is {k(1), k(2),...,k(A051903(n))}, then a(n) = Product {i = 1 to A051903(n)} prime(k(i)).",
				"Since the first integer of each prime signature (A025487) is always a product of primorials (A002110), there is always a value for a(n). Every positive integer appears in the sequence.",
				"a(m) = a(n) iff m and n have the same prime signature.  If the prime signatures of m and n are conjugate to each other when they are viewed as partitions, then a(n) = A181819(m) and a(m) = A181819(n)."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A238745/b238745.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A181819(A124859(n)).",
				"a(n) = A122111(A181819(n))."
			],
			"example": [
				"The first integer with the same prime signature as 40 is 24 = 2^3*3. Since the factorization of 24 into primorials is 24 = 2^2*6 = A002110(1)^2*A002110(2), a(24) = a(40) = prime(1)^2*prime(2) = 2^2*3 = 12."
			],
			"mathematica": [
				"f[n_] := Block[{k = 1, d, a}, While[n - Times @@ Prime@ Range[k + 1] \u003e= 0, k++]; If[n == Product[Prime@ i, {i, k}], Prime@ k, d = Select[Reverse@ FoldList[#1 #2 \u0026, Prime@ Range@ k], Divisible[n, #] \u0026]; If[AllTrue[#, IntegerQ], Times @@ Map[(FactorInteger[#1][[-1, 1]])^#2 \u0026 @@ # \u0026, Reverse@ Tally@ #], False] \u0026@ Rest@ NestWhileList[Function[P, {#1/P, P}]@ SelectFirst[d, Function[k, Divisible[#1, k]]] \u0026 @@ # \u0026, {n, 1}, First@ # \u003e 1 \u0026][[All, -1]]]]; Table[f@ Apply[Times, MapIndexed[Prime[First@ #2]^#1 \u0026, Sort[FactorInteger[n][[All, -1]], Greater]]] - Boole[n == 1], {n, 83}] (* _Michael De Vlieger_, May 19 2017, Version 10.2 *)"
			],
			"xref": [
				"Cf. A181815, A181819, A238744."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Matthew Vandermast_, Apr 28 2014",
			"references": 6,
			"revision": 14,
			"time": "2017-05-20T21:44:36-04:00",
			"created": "2014-04-29T00:05:58-04:00"
		}
	]
}