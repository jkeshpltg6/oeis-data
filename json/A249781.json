{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A249781",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 249781,
			"data": "1,-3,1,3,-1,1,-4,1,1,-1,0,-1,2,4,-1,-5,6,-3,-4,1,-4,8,0,-3,1,2,1,-4,-6,3,8,1,0,-10,4,3,2,-4,2,3,-6,4,-4,-8,-1,0,0,7,9,-3,6,-2,-6,1,0,4,-4,10,0,-3,-10,-8,-4,11,-2,-8,-4,10,0,-4,0,1,2,18",
			"name": "Expansion of q * f(-q)^2 * f(-q^15)^2 * chi(-q) * chi(-q^15) / (chi(-q^3) * chi(-q^5)) in powers of q where f(), chi() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A249781/b249781.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of eta(q)^3 * eta(q^6) * eta(q^10) * eta(q^15)^3 / (eta(q^2) * eta(q^3) * eta(q^5) * eta(q^30)) in powers of q.",
				"Euler transform of period 30 sequence [ -3, -2, -2, -2, -2, -2, -3, -2, -2, -2, -3, -2, -3, -2, -4, -2, -3, -2, -3, -2, -2, -2, -3, -2, -2, -2, -2, -2, -3, -4, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (30 t)) = 60 (t/i)^2 g(t) where q = exp(2 Pi i t) and g() is the g.f. for A249371."
			],
			"example": [
				"G.f. = q - 3*q^2 + q^3 + 3*q^4 - q^5 + q^6 - 4*q^7 + q^8 + q^9 - q^10 - q^12 + ..."
			],
			"mathematica": [
				"eta[q_]:= q^(1/24)*QPochhammer[q]; a[n_]:= SeriesCoefficient[eta[q]^3* eta[q^6]*eta[q^10]*eta[q^15]^3/(eta[q^2]*eta[q^3]*eta[q^5]*eta[q^30]), {q, 0, n}]; Table[a[n], {n,1,50}] (* _G. C. Greubel_, Mar 15 2018 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); n-=1; if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A)^3 * eta(x^6 + A) * eta(x^10 + A) * eta(x^15 + A)^3 / (eta(x^2 + A) * eta(x^3 + A) * eta(x^5 + A) * eta(x^30 + A)), n))};",
				"(PARI) q='q+O('q^99); Vec(eta(q)^3*eta(q^6)*eta(q^10)*eta(q^15)^3/(eta(q^2)*eta(q^3)*eta(q^5)*eta(q^30))) \\\\ _Altug Alkan_, Mar 16 2018",
				"(MAGMA) A := Basis( CuspForms( Gamma0(30), 2), 75); A[1] - 3*A[2] + A[3];"
			],
			"xref": [
				"Cf. A249371."
			],
			"keyword": "sign",
			"offset": "1,2",
			"author": "_Michael Somos_, Nov 05 2014",
			"references": 1,
			"revision": 12,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2014-11-05T15:34:13-05:00"
		}
	]
}