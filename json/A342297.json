{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A342297",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 342297,
			"data": "1,1,2,2,3,2,4,5,2,6,5,7,8,2,9,5,7,11,10,12,13,2,14,5,7,16,10,17,13,15,19,18,20,21,2,22,5,7,24,10,25,13,15,27,18,20,29,23,30,26,28,32,31,33,34,2,35,5,7,37,10,38,13,15,40,18,20,42,23,43,26,28,45,31,46,34,36,48",
			"name": "A Van Eck-like sequence based on the Fibonacci numbers. See Comments for definition.",
			"comment": [
				"We construct finite sequences S_0, S_1, S_2, ...; we let A_i  (i \u003e= 0) be the concatenation of S_0, ..., S_i; and the sequence itself is lim_{i -\u003e oo} A_i. All of S_i, A_i, and A have offset 1. F_i (i \u003e= 0) is the i-th Fibonacci number A000045(i).",
				"S_0 = [1] is special.",
				"For i \u003e= 1, S_i has length F_i, and is defined by:",
				"  S_i(j) = max r such that A_{i-1}(r) = j, for 1 \u003c= j \u003c= F_i.",
				"That is, S_i(j) is the index of the most recent occurrence of j in A_{i-1}.",
				"The sequence appears to be as follows. Following the initial 1, it has as a subsequence the Lower Wythoff sequence (A000201: 1, 3, 4, 6, 8, 9, 11, 12, 14, ...) whose terms appear at indices given by the Upper Wythoff sequence (A001950: 2, 5, 7, 10, 13, 15, 18, 20, ...). Interspersed with this is an infinite set of increasingly long initial segments of the Upper Wythoff sequence, each one ending when it has exceeded the most recently appearing term of the Lower Wythoff subsequence. This all shows up clearly on the scatterplot. - _Peter Munn_, Mar 14 2021"
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A342297/b342297.txt\"\u003eTable of n, a(n) for n = 1..17711\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A342297/a342297.gp.txt\"\u003ePARI program for A342297\u003c/a\u003e"
			],
			"example": [
				"Here are the initial stages of the construction:",
				"S_0 = [1].",
				"A_0 = S_0 = [1].",
				"S_1: When did 1 last appear in A_0? Answer: r=1, S_1 = [1], A_1 = [1,1].",
				"S_2: When did 1 last appear in A_1? Answer: r=2, S_2 = [2], A_2 = [1,1,2].",
				"S_3: When did 1 last appear in A_2? Answer: r=2.",
				"When did 2 last appear in A_2? Answer: r=3. So S_3 = [2,3], A_3 = [1,1,2,2,3].",
				"And so on."
			],
			"program": [
				"(PARI) See Links section."
			],
			"xref": [
				"Cf. A000045, A000201, A001950, A181391."
			],
			"keyword": "nonn,look",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_, Mar 13 2021, following a suggestion from a correspondent who wishes to remain anonymous. The definition given here is my interpretation of his construction.",
			"references": 2,
			"revision": 34,
			"time": "2021-03-14T20:39:07-04:00",
			"created": "2021-03-13T22:40:17-05:00"
		}
	]
}