{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A008718",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 8718,
			"data": "1,1,1,1,2,2,3,3,4,5,6,6,9,10,11,12,15,16,19,20,23,26,29,30,36,39,42,45,51,54,60,63,69,75,81,84,94,100,106,112,122,128,138,144,154,164,174,180,195,205,215,225,240,250,265,275,290,305,320,330,351,366",
			"name": "Expansion of g.f.: (1+x^9)/((1-x)*(1-x^4)*(1-x^6)*(1-x^12)).",
			"comment": [
				"Molien series for genus-2 weight enumerators of binary self-dual codes is (1+x^18)/((1-x^2)*(1-x^8)*(1-x^12)*(1-x^24)). Exponents have been divided by 2 to get the sequence.",
				"Or, Molien series for 4-dimensional representation of 2.{3,4,3}. This is the real 4-dimensional Clifford group of genus 2 and order 2304."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A008718/b008718.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"F. J. MacWilliams, C. L. Mallows and N. J. A. Sloane, \u003ca href=\"http://neilsloane.com/doc/gleason2.html\"\u003eGeneralizations of Gleason's theorem on weight enumerators of self-dual codes\u003c/a\u003e, IEEE Trans. Inform. Theory, 18 (1972), 794-805; see p. 802, col. 2, foot.",
				"G. Nebe, E. M. Rains and N. J. A. Sloane, \u003ca href=\"http://neilsloane.com/doc/cliff2.html\"\u003eSelf-Dual Codes and Invariant Theory\u003c/a\u003e, Springer, Berlin, 2006.",
				"\u003ca href=\"/index/Mo#Molien\"\u003eIndex entries for Molien series\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_20\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,0,1,0,-1,0,-1,1,0,0,0,1,-1,0,-1,0,1,0,1,-1)."
			],
			"formula": [
				"a(n) ~ 1/864*n^3. - _Ralf Stephan_, Apr 29 2014",
				"G.f.: ( 1-x^3+x^6 ) / ( (1-x+x^2) *(x^4-x^2+1) *(1+x)^2 *(x^2+1)^2 *(1+x+x^2)^2 *(x-1)^4 ). - _R. J. Mathar_, Dec 18 2014"
			],
			"maple": [
				"(1+x^9)/((1-x)*(1-x^4)*(1-x^6)*(1-x^12)); seq(coeff(series(%, x, n+1), x, n), n = 0..65); # modified by _G. C. Greubel_, Sep 09 2019"
			],
			"mathematica": [
				"CoefficientList[Series[(1+x^9)/((1-x)(1-x^4)(1-x^6)(1-x^12)), {x,0,65}], x] (* _Harvey P. Dale_, Apr 01 2011 *)",
				"LinearRecurrence[{1,0,1,0,-1,0,-1,1,0,0,0,1,-1,0,-1,0,1,0,1,-1}, {1,1,1, 1,2,2,3,3,4,5,6,6,9,10,11,12,15,16,19,20}, 65] (* _Ray Chandler_, Jul 16 2015 *)"
			],
			"program": [
				"(PARI) my(x='x+O('x^65)); Vec((1+x^9)/((1-x)*(1-x^4)*(1-x^6)*(1-x^12))) \\\\ _G. C. Greubel_, Sep 09 2019",
				"(MAGMA) R\u003cx\u003e:=PowerSeriesRing(Integers(), 65); Coefficients(R!( (1+x^9)/((1-x)*(1-x^4)*(1-x^6)*(1-x^12)) )); // _G. C. Greubel_, Sep 09 2019",
				"(Sage)",
				"def A008718_list(prec):",
				"    P.\u003cx\u003e = PowerSeriesRing(ZZ, prec)",
				"    return P((1+x^9)/((1-x)*(1-x^4)*(1-x^6)*(1-x^12))).list()",
				"A008718_list(65) # _G. C. Greubel_, Sep 09 2019"
			],
			"xref": [
				"Cf. A008621, A024186."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_",
			"references": 8,
			"revision": 32,
			"time": "2019-09-09T15:36:58-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}