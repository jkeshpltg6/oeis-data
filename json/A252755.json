{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A252755",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 252755,
			"data": "1,2,4,3,8,9,6,5,16,21,18,25,12,15,10,7,32,45,42,55,36,51,50,49,24,33,30,35,20,27,14,11,64,93,90,115,84,123,110,91,72,105,102,125,100,147,98,121,48,69,66,85,60,87,70,77,40,57,54,65,28,39,22,13,128,189,186,235,180,267,230,203,168,249,246,305,220,327,182,187,144",
			"name": "Tree of Eratosthenes, mirrored: a(0) = 1, a(1) = 2; after which, a(2n) = 2*a(n), a(2n+1) = A250469(a(n)).",
			"comment": [
				"This sequence can be represented as a binary tree. Each child to the left is obtained by doubling the parent, and each child to the right is obtained by applying A250469 to the parent:",
				"                                     1",
				"                                     |",
				"                  ...................2...................",
				"                 4                                       3",
				"       8......../ \\........9                   6......../ \\........5",
				"      / \\                 / \\                 / \\                 / \\",
				"     /   \\               /   \\               /   \\               /   \\",
				"    /     \\             /     \\             /     \\             /     \\",
				"  16       21         18       25         12       15         10       7",
				"32  45   42  55     36  51   50  49     24  33   30  35     20  27   14 11",
				"etc.",
				"Sequence A252753 is the mirror image of the same tree. A253555(n) gives the distance of n from 1 in both trees."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A252755/b252755.txt\"\u003eTable of n, a(n) for n = 0..8192\u003c/a\u003e",
				"Antti Karttunen, \u003ca href=\"/A135141/a135141.pdf\"\u003eEntanglement Permutations\u003c/a\u003e, 2016-2017",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"a(0) = 1, a(1) = 2; after which, a(2n) = 2*a(n), a(2n+1) = A250469(a(n)).",
				"As a composition of related permutations:",
				"a(n) = A252753(A054429(n)).",
				"a(n) = A250245(A163511(n))."
			],
			"mathematica": [
				"(* b = A250469 *) b[1] = 1; b[n_] := If[PrimeQ[n], NextPrime[n], m1 = p1 = FactorInteger[n][[1, 1]]; For[k1 = 1, m1 \u003c= n, m1 += p1; If[m1 == n, Break[]]; If[FactorInteger[m1][[1, 1]] == p1, k1++]]; m2 = p2 = NextPrime[p1]; For[k2 = 1, True, m2 += p2, If[FactorInteger[m2][[1, 1]] == p2, k2++]; If[k1 + 2 == k2, Return[m2]]]];",
				"a[0] = 1; a[1] = 2; a[n_] := a[n] = If[EvenQ[n], 2 a[n/2], b[a[(n-1)/2]]];",
				"Table[a[n], {n, 0, 100}] (* _Jean-François Alcover_, Mar 08 2016 *)"
			],
			"program": [
				"(Scheme, with memoization-macro definec)",
				"(definec (A252755 n) (cond ((\u003c= n 1) (+ n 1)) ((even? n) (* 2 (A252755 (/ n 2)))) (else (A250469 (A252755 (/ (- n 1) 2))))))"
			],
			"xref": [
				"Inverse: A252756.",
				"Row sums: A253787, products: A253788.",
				"Similar permutations: A163511, A252753, A054429, A163511, A250245, A269865.",
				"Cf. also: A249814 (Compare the scatterplots).",
				"Cf. A083221, A250469, A253555."
			],
			"keyword": "nonn,tabf,look",
			"offset": "0,2",
			"author": "_Antti Karttunen_, Jan 02 2015",
			"references": 24,
			"revision": 24,
			"time": "2019-12-10T12:09:47-05:00",
			"created": "2015-01-15T11:38:55-05:00"
		}
	]
}