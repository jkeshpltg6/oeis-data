{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A102314",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 102314,
			"data": "1,-1,0,-1,1,-1,1,-2,3,-2,3,-3,4,-4,4,-6,7,-7,7,-9,10,-12,13,-14,17,-18,19,-22,26,-28,29,-34,38,-41,44,-50,57,-60,65,-72,81,-86,94,-105,114,-124,133,-146,161,-174,187,-204,224,-240,258,-282,309,-332,354,-386,419,-450,481,-524,569,-606,651,-703",
			"name": "McKay-Thompson series of class 42C for the Monster group.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Given g.f. A(x), the second term of the left side of Cayley's identity is -A(q). - _Michael Somos_, Dec 03 2013"
			],
			"reference": [
				"A. Cayley, An elliptic-transcendant identity, Messenger of Math., 2 (1873), p. 179."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A102314/b102314.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"D. Ford, J. McKay and S. P. Norton, \u003ca href=\"http://dx.doi.org/10.1080/00927879408825127\"\u003eMore on replicable functions\u003c/a\u003e, Commun. Algebra 22, No. 13, 5175-5193 (1994).",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e",
				"\u003ca href=\"/index/Mat#McKay_Thompson\"\u003eIndex entries for McKay-Thompson series for Monster simple group\u003c/a\u003e"
			],
			"formula": [
				"Expansion of chi(-x) * chi(-x^7) in powers of x where chi() is a Ramanujan theta function.",
				"Expansion of q^(1/3) * eta(q) * eta(q^7) / (eta(q^2) * eta(q^14)) in powers of q.",
				"Euler transform of period 14 sequence [ -1, 0, -1, 0, -1, 0, -2, 0, -1, 0, -1, 0, -1, 0, ...].",
				"Given g.f. A(x), then B(q) = A(q^3) / q satisfies 0 = f(B(q), B(q^2)) where f(u, v) = v^2 - u^2*v - 2*u.",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (126 t)) = 2 g(t) where q = exp(2 Pi i t) and g() is the g.f. for A093950.",
				"G.f.: 1 / (Product_{k\u003e0} (1 + x^k) * (1 + x^(7*k))).",
				"a(n) = (-1)^n * A112212(n). a(2*n + 1) = - A093950(n). a(4*n) = A193826(n). a(4*n + 2) = A193883(n).",
				"Convolution inverse is A093950.",
				"a(n) ~ (-1)^n * exp(2*Pi*sqrt(n/21)) / (2 * 21^(1/4) * n^(3/4)). - _Vaclav Kotesovec_, Sep 07 2017"
			],
			"example": [
				"G.f. = 1 - x - x^3 + x^4 - x^5 + x^6 - 2*x^7 + 3*x^8 - 2*x^9 + 3*x^10 - 3*x^11 + ...",
				"T42C = 1/q - q^2 - q^8 + q^11 - q^14 + q^17 - 2*q^20 + 3*q^23 - 2*q^26 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ x, x^2] QPochhammer[ x^7, x^14], {x, 0, n}]; (* _Michael Somos_, Aug 06 2011 *)",
				"a[ n_] := SeriesCoefficient[ 1 / ( Product[ 1 + x^k, {k, n}] Product[ 1 + x^k, {k, 7, n, 7}] ), {x, 0, n}]; (* _Michael Somos_, Aug 06 2011 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A) * eta(x^7 + A) / (eta(x^2 + A) * eta(x^14 + A)), n))};"
			],
			"xref": [
				"Cf. A093950, A112212, A193826, A193883."
			],
			"keyword": "sign",
			"offset": "0,8",
			"author": "_Michael Somos_, Jan 03 2005",
			"references": 5,
			"revision": 31,
			"time": "2021-03-12T22:24:43-05:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}