{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A175644",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 175644,
			"data": "0,3,3,2,1,5,5,5,0,3,2,2,2,1,7,9,5,0,5,5,2,9,2,7,1,7,7,7,8,0,1,3,8,0,9,6,4,8,1,0,8,7,5,6,6,6,5,3,2,6,6,8,3,0,5,7,3,2,8,8,5,6,6,2,4,6,2,6,8,3,6,7,2,4,1,5,4,3,4,2,8,9,8,8,9,4,4,1,7,3,9,9,4,4,1,7,0,5,8,1,5,9,7,4,4,8",
			"name": "Decimal expansion of the sum 1/p^2 over primes p == 1 (mod 3).",
			"comment": [
				"The prime zeta modulo function at 2 for primes of the form 3k+1, which is P_{3,2}(2) = Sum_{p in A002476} 1/p^2 = 1/7^2 + 1/13^2 + 1/19^2 + 1/31^2 + ...",
				"The complementary Sum_{p in A003627} 1/p^2 is given by P_{3,2}(2) = A085548 - 1/3^2 - (this value here) = 0.307920758607736436842505075940... = A343612."
			],
			"link": [
				"Jean-François Alcover, \u003ca href=\"/A175644/b175644.txt\"\u003eTable of n, a(n) for n = 0..1003\u003c/a\u003e",
				"R. J. Mathar, \u003ca href=\"http://arxiv.org/abs/1008.2547\"\u003eTable of Dirichlet L-series and Prime Zeta Modulo Functions for Small Moduli\u003c/a\u003e, arXiv:1008.2547 [math.NT], 2010-2015.",
				"\u003ca href=\"/index/Z#zeta_function\"\u003eOEIS index to entries related to the (prime) zeta function\u003c/a\u003e."
			],
			"example": [
				"P_{3,1}(2) = 0.03321555032221795055292717778013809648108756665..."
			],
			"mathematica": [
				"(* A naive solution yielding 12 correct digits: *) s1 = s2 = 0.; Do[Switch[Mod[n, 3], 1, If[PrimeQ[n], s1 += 1/n^2], 2, If[PrimeQ[n], s2 += 1/n^2]], {n, 10^7}]; Join[{0}, RealDigits[(PrimeZetaP[2] + s1 - s2 - 1/9)/2, 10, 12][[1]]] (* _Jean-François Alcover_, Mar 15 2018 *)",
				"With[{s=2}, Do[Print[N[1/2 * Sum[(MoebiusMu[2*n + 1]/(2*n + 1)) * Log[(Zeta[s + 2*n*s]*(Zeta[s + 2*n*s, 1/6] - Zeta[s + 2*n*s, 5/6])) / ((1 + 2^(s + 2*n*s))*(1 + 3^(s + 2*n*s)) * Zeta[2*(1 + 2*n)*s])], {n, 0, m}], 120]], {m, 100, 500, 100}]] (* _Vaclav Kotesovec_, Jan 13 2021 *)"
			],
			"program": [
				"(PARI) From _M. F. Hasler_, Apr 23 2021: (Start)",
				"s=0; forprimestep(p=1, 1e8, 3, s+=1./p^2); s \\\\ For illustration: primes up to 10^N give only about 2N+2 (= 18 for N=8) correct digits.",
				"PrimeZeta31(s)=suminf(n=0,my(t=s+2*n*s); moebius(2*n+1)*log((zeta(t)*(zetahurwitz(t,1/6)-zetahurwitz(t,5/6)))/((1+2^t)*(1+3^t)*zeta(2*t)))/(4*n+2)) \\\\ Inspired from Kotesovec's Mmca code",
				"A175644_upto(N=100)={localprec(N+5);digits((PrimeZeta31(2)+1)\\.1^N)[^1]} \\\\ (End)"
			],
			"xref": [
				"Cf. A086032 (P_{4,1}(2): same for p==1 (mod 4)), A175645 (P_{3,1}(3): same for 1/p^3), A343612 (P_{3,2}(2): same for p==2 (mod 3)), A085548 (PrimeZeta(2))."
			],
			"keyword": "cons,nonn",
			"offset": "0,2",
			"author": "_R. J. Mathar_, Aug 01 2010",
			"ext": [
				"More digits from _Vaclav Kotesovec_, Jun 27 2020"
			],
			"references": 13,
			"revision": 24,
			"time": "2021-08-25T12:54:44-04:00",
			"created": "2010-08-27T03:00:00-04:00"
		}
	]
}