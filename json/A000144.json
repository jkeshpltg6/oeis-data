{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000144",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 144,
			"data": "1,20,180,960,3380,8424,16320,28800,52020,88660,129064,175680,262080,386920,489600,600960,840500,1137960,1330420,1563840,2050344,2611200,2986560,3358080,4194240,5318268,5878440,6299520,7862400,9619560",
			"name": "Number of ways of writing n as a sum of 10 squares.",
			"reference": [
				"E. Grosswald, Representations of Integers as Sums of Squares. Springer-Verlag, NY, 1985, p. 121.",
				"G. H. Hardy and E. M. Wright, An Introduction to the Theory of Numbers. 3rd ed., Oxford Univ. Press, 1954, p. 314.",
				"G. H. Hardy, Ramanujan: twelve lectures on subjects suggested by his life and work, Chelsea Publishing Company, New York 1959, p. 135 section 9.3. MR0106147 (21 #4881)"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A000144/b000144.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"H. H. Chan and C. Krattenthaler, \u003ca href=\"http://arXiv.org/abs/math.NT/0407061\"\u003eRecent progress in the study of representations of integers as sums of squares\u003c/a\u003e, arXiv:math/0407061 [math.NT], 2004.",
				"Shi-Chao Chen, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2010.01.011\"\u003eCongruences for rs(n)\u003c/a\u003e, Journal of Number Theory, Volume 130, Issue 9, September 2010, Pages 2028-2032.",
				"J. Liouville, \u003ca href=\"http://sites.mathdoc.fr/JMPA/afficher_notice.php?id=JMPA_1866_2_11_A1_0\"\u003eNombre des représentations d’un entier quelconque sous la forme d’une somme de dix carrés\u003c/a\u003e, Journal de mathématiques pures et appliquées 2e série, tome 11 (1866), p. 1-8.",
				"\u003ca href=\"/index/Su#ssq\"\u003eIndex entries for sequences related to sums of squares\u003c/a\u003e"
			],
			"formula": [
				"Euler transform of period 4 sequence [ 20, -30, 20, -10, ...]. - _Michael Somos_, Sep 12 2005",
				"Expansion of eta(q^2)^50 / (eta(q) * eta(q^4))^20 in powers of q. - _Michael Somos_, Sep 12 2005",
				"a(n) = 4/5 * (A050456(n) + 16*A050468(n) + 8*A030212(n)) if n\u003e0. - _Michael Somos_, Sep 12 2005",
				"a(n) = (20/n)*Sum_{k=1..n} A186690(k)*a(n-k), a(0) = 1. - _Seiichi Manyama_, May 27 2017"
			],
			"example": [
				"G.f. = 1 + 20*x + 180*x^2 + 960*x^3 + 3380*x^4 + 8424*x^5 + 16320*x^6 + ..."
			],
			"maple": [
				"(sum(x^(m^2),m=-10..10))^10;",
				"# Alternative:",
				"A000144list := proc(len) series(JacobiTheta3(0, x)^10, x, len+1);",
				"seq(coeff(%, x, j), j=0..len-1) end: A000144list(30); # _Peter Luschny_, Oct 02 2018"
			],
			"mathematica": [
				"Table[SquaresR[10, n], {n, 0, 30}] (* _Ray Chandler_, Jun 29 2008; updated by _T. D. Noe_, Jan 23 2012 *)",
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 3, 0, q]^10, {q, 0, n}]; (* _Michael Somos_, Aug 26 2015 *)",
				"nmax = 50; CoefficientList[Series[Product[(1 - x^k)^10 * (1 + x^k)^30 / (1 + x^(2*k))^20, {k, 1, nmax}], {x, 0, nmax}], x] (* _Vaclav Kotesovec_, Jun 24 2017 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, 0, polcoeff( sum(k=1, sqrtint(n), 2 * x^k^2, 1 + x * O(x^n))^10, n))}; /* _Michael Somos_, Sep 12 2005 */",
				"(Sage)",
				"Q = DiagonalQuadraticForm(ZZ, [1]*10)",
				"Q.representation_number_list(37) # _Peter Luschny_, Jun 20 2014"
			],
			"xref": [
				"Row d=10 of A122141 and of A319574, 10th column of A286815."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Extended by _Ray Chandler_, Nov 28 2006"
			],
			"references": 10,
			"revision": 52,
			"time": "2019-11-01T11:32:24-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}