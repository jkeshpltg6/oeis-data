{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A275110",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 275110,
			"data": "1,1,6,5,9,1,0,2,4,8,4",
			"name": "Decimal expansion of the sum of the alternating series of reciprocals of composite numbers with distinct prime factors.",
			"comment": [
				"The denominators in the terms used to generate the sequence exclude the powers of primes (A000961).",
				"From _Jon E. Schoenfield_, Apr 19 2018: (Start)",
				"The k-th term in the sum is t(k) = (-1)^(k+1)/A024619(k). The partial sums S(j) = Sum_{k=1..j} t(k) converge rather slowly; e.g., since A024619(921265) = 10^6, each partial sum is still differing from its predecessor by an absolute difference of about 0.000001 after 921265 terms.",
				"However, smoothing the sequence of partial sums by counting only half of the last term in each partial sum, i.e., S'(j) = (Sum_{k=1..j-1} t(k)) + (1/2)*t(j), yields a sequence that converges much more quickly. Faster still is the convergence of the sequence of partial sums S'(j) when only those values of j for which 1/t(j) is odd are considered, similar to the approach described at A275712. Here, however, the relative smoothness of the plot of S'(j), considering only those values of j for which 1/t(j) is odd, is interrupted by a significant step upward or downward each time the first such 1/t(j) is encountered that exceeds a new power of 2. (The absolute value of each such discontinuity is on the order of 1/(t(j))^2, and its sign depends on the parity of the number of terms in A024619 that are less than that power of 2.) In spite of these discontinuities, however, apparent convergence beyond the digits currently listed in the Data section is easy to obtain; see the table in the Example section.",
				"(End)"
			],
			"formula": [
				"Sum_{k\u003e=1} (-1)^(k+1)/A024619(k) = 1/6 - 1/10 + 1/12 - 1/14 + 1/15 - 1/18 + ..."
			],
			"example": [
				"0.11659102484...",
				"From _Jon E. Schoenfield_, Apr 19 2018: (Start)",
				"For b = 3..23, the table below lists the minimum and maximum values of the smoothed partial sum S'(j) (see Comments) among values of j such that 1/t(j) is an odd number in the interval [2^b, 2^(b+1)]. (The table begins at b=3 because the first odd denominator in the alternating sum is A024619(5) = 15, which lies in the interval [2^3, 2^4].)",
				".",
				"   b   min S'(j)  (1/t(j) odd)   max S'(j) (1/t(j) odd)",
				"  ==   =======================   =======================",
				"   3   0.111904761904761904...   0.111904761904761904...",
				"   4   0.115873015873015873...   0.115873015873015873...",
				"   5   0.116751763887986798...   0.116780441780441780...",
				"   6   0.116515193298484059...   0.116521345165903716...",
				"   7   0.116576739754580331...   0.116579123025777238...",
				"   8   0.116593064475078642...   0.116593574635271689...",
				"   9   0.116589740175207247...   0.116589805144898644...",
				"  10   0.116590699983114246...   0.116590715183253815...",
				"  11   0.116590952081202610...   0.116590954393007789...",
				"  12   0.116591012248587507...   0.116591013407441019...",
				"  13   0.116591027701733147...   0.116591027880912807...",
				"  14   0.116591024064904443...   0.116591024080256316...",
				"  15   0.116591025004372396...   0.116591025007229633...",
				"  16   0.116591024772646678...   0.116591024773252190...",
				"  17   0.116591024831104914...   0.116591024831253967...",
				"  18   0.116591024845789528...   0.116591024845809974...",
				"  19   0.116591024842154904...   0.116591024842161473...",
				"  20   0.116591024841250054...   0.116591024841250922...",
				"  21   0.116591024841022776...   0.116591024841022992...",
				"  22   0.116591024840966103...   0.116591024840966133...",
				"  23   0.116591024840980341...   0.116591024840980345...",
				"  24   0.116591024840976791...   0.116591024840976792...",
				"(End)"
			],
			"program": [
				"(Sage)",
				"composites = (i for i in NN if i\u003e3 and not i.is_prime() and not i.is_prime_power())",
				"s = RLF(0); s",
				"RealField(110)(s)",
				"for i in range(0,50000000): s += (-1)^i / next(composites)",
				"print(s) # _Terry D. Grant_, Aug 01 2016",
				"(PARI) approx(lim)=my(s=0.,r=1,t); forcomposite(n=6,lim, if(ispower(n,,\u0026t) \u0026\u0026 isprime(t), next); s += r/n; r=-r); t=floor(lim)+1; if(isprime(t), t++); t=s+r/t; [min(t,s), max(t,s)] \\\\ Gives lower and upper bounds",
				"\\\\ _Charles R Greathouse IV_, Jul 31 2016"
			],
			"xref": [
				"Cf. A024619, A269229."
			],
			"keyword": "nonn,cons,more",
			"offset": "0,3",
			"author": "_Terry D. Grant_, Jul 16 2016",
			"ext": [
				"2 more digits from _Charles R Greathouse IV_, Aug 02 2016"
			],
			"references": 3,
			"revision": 34,
			"time": "2020-02-26T08:14:03-05:00",
			"created": "2016-08-02T23:40:19-04:00"
		}
	]
}