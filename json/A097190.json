{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A097190",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 97190,
			"data": "1,1,24,1,36,612,1,48,1104,15912,1,60,1740,32130,417690,1,72,2520,56700,912492,11027016,1,84,3444,91350,1750014,25562628,292215924,1,96,4512,137808,3059856,52303968,710025264,7764594552,1,108,5724,197802,4992354",
			"name": "Triangle read by rows in which row n gives coefficients of polynomial R_n(y) that satisfies R_n(1/3) = 9^n, where R_n(y) forms the initial (n+1) terms of g.f. A097191(y)^(n+1).",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A097190/b097190.txt\"\u003eRows n = 0..50 of triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"G.f.: A(x, y) = 3*y/((1-27*x*y) + (3*y-1)*(1-27*x*y)^(8/9)).",
				"G.f.: A(x, y) = A097192(x*y)/(1 - x*A097193(x*y))."
			],
			"example": [
				"Row polynomials evaluated at y=1/3 equals powers of 9:",
				"9^1 = 1 + 24/3;",
				"9^2 = 1 + 36/3 + 612/3^2;",
				"9^3 = 1 + 48/3 + 1104/3^2 + 15912/3^3;",
				"9^4 = 1 + 60/3 + 1740/3^2 + 32130/3^3 + 417690/3^4;",
				"where A097191(y)^(n+1) has the same initial terms as the n-th row:",
				"A097191(y) = 1 + 12y + 60y^2 + 90y^3 - 558y^4 - 2916y^5 + 2160y^6 +...",
				"A097191(y)^2 = 1 + 24y +...",
				"A097191(y)^3 = 1 + 36y + 612y^2 +...",
				"A097191(y)^4 = 1 + 48y + 1104y^2 + 15912y^3 +...",
				"A097191(y)^5 = 1 + 60y + 1740y^2 + 32130y^3 + 417690y^4 +...",
				"Rows begin with n=0:",
				"  1;",
				"  1, 24;",
				"  1, 36,  612;",
				"  1, 48, 1104,  15912;",
				"  1, 60, 1740,  32130,  417690;",
				"  1, 72, 2520,  56700,  912492, 11027016;",
				"  1, 84, 3444,  91350, 1750014, 25562628, 292215924;",
				"  1, 96, 4512, 137808, 3059856, 52303968, 710025264, 7764594552; ..."
			],
			"mathematica": [
				"Table[SeriesCoefficient[3*y/((1-27*x*y) + (3*y-1)*(1-27*x*y)^(8/9)), {x, 0,n}, {y,0,k}], {n,0,12}, {k,0,n}]//Flatten (* _G. C. Greubel_, Sep 17 2019 *)"
			],
			"program": [
				"(PARI) {T(n,k)=if(n==0,1,if(k==0,1,if(k==n, 3^n*(9^n-sum(j=0,n-1, T(n,j)/3^j)), polcoeff((Ser(vector(n,i,T(n-1,i-1)),x) +x*O(x^k))^((n+1)/n),k,x))))}"
			],
			"xref": [
				"Cf. A097186, A097191, A097192, A097193, A097194, A097195."
			],
			"keyword": "nonn,tabl",
			"offset": "0,3",
			"author": "_Paul D. Hanna_, Aug 03 2004",
			"references": 6,
			"revision": 7,
			"time": "2019-09-17T15:41:43-04:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}