{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A250401",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 250401,
			"data": "1,9,197,503,6623,17813,340527,3763087,169947523,170436583,5295982873,90208585541,3343268872217,3348036962687,144143598106421,1659445372263179,11627213232841853,3879029288899801,352907045903771,10241306344308349,208368821623076563",
			"name": "Denominator of the harmonic mean of the first n nonzero octagonal numbers.",
			"comment": [
				"a(n+1), for n \u003e= 0, is also the numerator of the partial sums of the reciprocal octagonal numbers Sum_{k=0..n} 1/((k + 1)*(3*k + 1)) with the denominators given in A294512(n) [assuming that n+1 divides A250400(n+1) to give A294512(n) for n \u003e= 0]. - _Wolfdieter Lang_, Nov 01 2017"
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A250401/b250401.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e"
			],
			"formula": [
				"Denominator of 12*n/(Pi*sqrt(3) + 9*log(3) + 6*Psi(n+1/3) - 6*Psi(n+1)). - _Robert Israel_, Nov 01 2017"
			],
			"example": [
				"a(3) = 197 because the octagonal numbers A000567(n), for n = 1..3, are [1,8,21], and 3/(1/1 + 1/8 + 1/21) = 504/197."
			],
			"maple": [
				"f:= n -\u003e denom(n/add(1/(k*(3*k-2)),k=1..n)):",
				"map(f, [$1..40]); # _Robert Israel_, Nov 01 2017"
			],
			"mathematica": [
				"With[{s = Array[PolygonalNumber[8, #] \u0026, 21]}, Denominator@ Array[HarmonicMean@ Take[s, #] \u0026, Length@ s]] (* _Michael De Vlieger_, Nov 01 2017 *)"
			],
			"program": [
				"(PARI)",
				"harmonicmean(v) = #v / sum(k=1, #v, 1/v[k])",
				"s=vector(30); for(n=1, #s, s[n]=denominator(harmonicmean(vector(n, k, 3*k^2-2*k)))); s"
			],
			"xref": [
				"Cf. A000567 (octagonal numbers), A250400 (numerators), A294512."
			],
			"keyword": "nonn,frac,easy",
			"offset": "1,2",
			"author": "_Colin Barker_, Nov 21 2014",
			"references": 3,
			"revision": 19,
			"time": "2017-11-06T04:58:27-05:00",
			"created": "2014-11-21T07:48:23-05:00"
		}
	]
}