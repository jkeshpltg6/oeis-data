{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A132742",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 132742,
			"data": "1,3,7,5,1,1,7,7,7,7,9,1,1,1,1,11,7,7,7,7,7,1,1,1,1,1,1,1,3,7,7,7,7,7,7,7,5,1,1,1,1,1,1,1,1,7,7,7,7,7,7,7,7,7,7,9,1,1,1,1,1,1,1,1,1,1,11,7,7,7,7,7,7,7,7,7,7,7,1,1,1,1,1,1,1,1,1,1,1,1,1",
			"name": "Triangle T(n,m) = 1 + ((2*n*3^m) mod 12), read by rows.",
			"comment": [
				"T(n,m) differs from A132728 in the order in which n and m are handled."
			],
			"link": [
				"Stefano Spezia, \u003ca href=\"/A132742/b132742.txt\"\u003eFirst 150 rows of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n,m) = 1 + ((2*n*3^m) mod 12).",
				"T(n,m) = 1 + ((A005843(n)*A000244(m)) mod 12). - _Stefano Spezia_, Dec 26 2018",
				"Bivariate g.f.: -(4*x^7*y^2 + 8*x^6*y^2 - x^6*y - 7*x^5*y + 4*x^4*y^2 - 11*x^5 - x^4*y - 4*x^3*y^2 - 9*x^4 - 7*x^3*y - 7*x^3 - x^2*y - 5*x^2 - 7*x*y - 3*x - 1)/((1 - x^6)*(1 - x^2*y^2)). - _J. Douglas Morrison_, Jul 24 2021"
			],
			"example": [
				"n\\m|  0   1   2   3   4   5   6   7   8",
				"---+-----------------------------------",
				"0  |  1",
				"1  |  3   7",
				"2  |  5   1   1",
				"3  |  7   7   7   7",
				"4  |  9   1   1   1   1",
				"5  | 11   7   7   7   7   7",
				"6  |  1   1   1   1   1   1   1",
				"7  |  3   7   7   7   7   7   7   7",
				"9  |  5   1   1   1   1   1   1   1   1",
				"..."
			],
			"maple": [
				"a := (n, m) -\u003e (1 + ((2*n*3^m) mod 12)): seq(seq(a(n, m), m = 0 .. n), n = 0 .. 20) # _Stefano Spezia_, Dec 26 2018"
			],
			"mathematica": [
				"Flatten[Table[1 + Mod[2*n*3^m, 12], {n,0,20}, {m, 0, n}]] (* modified by _G. C. Greubel_, Feb 15 2021 *)"
			],
			"program": [
				"(GAP) Flat(List([0..20], n-\u003eList([0..n], m-\u003e(1 + ((2*n*3^m) mod 12))))); # _Stefano Spezia_, Dec 26 2018",
				"(MAGMA) [([1 + ((2*n*3^k) mod 12): k in [0..n]]): n in [0..20]]; // _Stefano Spezia_, Dec 26 2018",
				"(Maxima) sjoin(v, j) := apply(sconcat, rest(join(makelist(j, length(v)), v))); display_triangle(n) := for i from 0 thru n do disp(sjoin(makelist(1 + mod(2*i*3^j, 12), j, 0, i), \" \")); display_triangle(20); /* _Stefano Spezia_, Dec 26 2018 */",
				"(PARI) T(n, m) = 1 + ((2*n*3^m) % 12); \\\\ _Stefano Spezia_, Dec 26 2018",
				"(Magma)",
				"A132742:= func\u003c n,k | 1 + ((2*n*3^k) mod 12) \u003e;",
				"[A132742(n,k): k in [0..n], n in [0..15]]; // _G. C. Greubel_, Feb 15 2021"
			],
			"xref": [
				"Cf. A000244, A005843."
			],
			"keyword": "nonn,tabl,less",
			"offset": "0,2",
			"author": "_Roger L. Bagula_, Nov 17 2007",
			"ext": [
				"Edited by _Stefano Spezia_, Dec 26 2018"
			],
			"references": 2,
			"revision": 43,
			"time": "2021-09-03T15:12:49-04:00",
			"created": "2007-12-09T03:00:00-05:00"
		}
	]
}