{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A274263",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 274263,
			"data": "2,1,2,0,2,0,2,1,0,3,0,0,2,1,1,0,3,0,0,3,0,1,1,0,0,2,0,2,3,0,1,0,5,0,3,1,0,1,1,0,5,0,2,0,6,1,0,0,2,1,0,5,0,1,1,0,3,0,0,5,1,0,0,2,3,0,1,0,2,1,1,0,1,0,1,1,0,2,1,0,5,0,3,0,1,1,0,0,2,3,0,0,2,0,1,2",
			"name": "Integer part of the ratio of consecutive prime gaps.",
			"comment": [
				"It seems that the distribution of the ratios of consecutive prime gaps exhibits a quite symmetric pattern, in the sense that the relative frequency of each ratio is similar to that of the inverse of that ratio (at least for the first 2*10^5 primes). This is more clearly seen by mean of a histogram of the logarithm of the ratios which is nearly symmetric and nearly centered around zero (see link).",
				"Integer part of 2, 1, 2, 1/2, 2, 1/2, 2, 3/2, 1/3, 3, 2/3, 1/2,.... - _R. J. Mathar_, Jun 26 2016"
			],
			"link": [
				"Andres Cicuttin, \u003ca href=\"/A274263/a274263.pdf\"\u003eSeveral histograms of the logarithm of the ratio of consecutive prime gaps (prime(n+2)-prime(n+1))/(prime(n+1)-prime(n)) obtained for the first 2*10^5 primes with different bin sizes\u003c/a\u003e"
			],
			"formula": [
				"a(n) = floor( A001223(n+1)/A001223(n))."
			],
			"example": [
				"For n = 1 we have (prime(3)-prime(2))/(prime(2)-prime(1))) = (5-3)/(3-2) = 2 and its integer part is 2: a(1) = 2.",
				"For n = 4 we have (prime(6)-prime(5))/(prime(5)-prime(4))) = (13-11)/(11-7) = 1/2 an its integer part is 0: a(4) = 0."
			],
			"maple": [
				"A274264 := proc(n)",
				"    A001223(n+1)/A001223(n) ;",
				"    floor(%) ;",
				"end proc: # _R. J. Mathar_, Jun 26 2016"
			],
			"mathematica": [
				"Table[Floor[(Prime[j+2]-Prime[j+1])/(Prime[j+1]-Prime[j])],{j,1,200}];",
				"IntegerPart[#[[2]]/#[[1]]]\u0026/@Partition[Differences[Prime[Range[200]]],2,1] (* _Harvey P. Dale_, Mar 07 2018 *)"
			],
			"program": [
				"(PARI) a(n) = (prime(n+2)-prime(n+1))\\(prime(n+1)-prime(n)); \\\\ _Michel Marcus_, Jun 18 2016"
			],
			"xref": [
				"Cf. A001223."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Andres Cicuttin_, Jun 17 2016",
			"references": 9,
			"revision": 30,
			"time": "2018-03-07T11:16:45-05:00",
			"created": "2016-06-27T04:01:25-04:00"
		}
	]
}