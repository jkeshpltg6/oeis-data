{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A123335",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 123335,
			"data": "1,-1,3,-7,17,-41,99,-239,577,-1393,3363,-8119,19601,-47321,114243,-275807,665857,-1607521,3880899,-9369319,22619537,-54608393,131836323,-318281039,768398401,-1855077841,4478554083,-10812186007,26102926097,-63018038201,152139002499",
			"name": "a(n) = -2*a(n-1) + a(n-2) for n\u003e1, a(0)=1, a(1)=-1.",
			"comment": [
				"Inverse binomial transform of A077957 .",
				"The inverse of the g.f. is 3-x-2/(1+x) which generates 1, 1, -2, +2, -2, +2,... (-2, +2 periodically continued). - _Gary W. Adamson_, Jan 10 2011",
				"Pisano period lengths:  1, 1, 8, 4, 12, 8, 6, 4, 24, 12, 24, 8, 28, 6, 24, 8, 16, 24, 40, 12,... - _R. J. Mathar_, Aug 10 2012",
				"a(n) is the rational part of the Q(sqrt(2)) integer (sqrt(2) - 1)^n = a(n) + A077985(n-1)*sqrt(2), with A077985(-1) = 0. - _Wolfdieter Lang_, Dec 07 2014",
				"3^n*a(n) = A251732(n) gives the rational part of the integer in Q(sqrt(2)) giving the length of a variant of Lévy's C-curve at iteration step n. - _Wolfdieter Lang_, Dec 07 2014"
			],
			"link": [
				"Harvey P. Dale, \u003ca href=\"/A123335/b123335.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (-2,1)."
			],
			"formula": [
				"a(n) = (-1)^n*A001333(n).",
				"G.f.: (1+x)/(1+2*x-x^2).",
				"a(n) = 1/2*((-1-sqrt(2))^n+(-1+sqrt(2))^n). - _Paolo P. Lava_, Nov 19 2008",
				"a(n) = A077985(n) + A077985(n-1). - _R. J. Mathar_, Mar 28 2011",
				"G.f.: G(0)/2, where G(k)= 1 + 1/(1 - x*(2*k-1)/(x*(2*k+1) + 1/G(k+1))); (continued fraction). - _Sergei N. Gladkovskii_, Jun 19 2013"
			],
			"maple": [
				"a:= n-\u003e (M-\u003e M[2, 1]+M[2, 2])(\u003c\u003c2|1\u003e, \u003c1|0\u003e\u003e^(-n)):",
				"seq(a(n), n=0..33);  # _Alois P. Heinz_, Jun 22 2021"
			],
			"mathematica": [
				"LinearRecurrence[{-2,1},{1,-1},40] (* _Harvey P. Dale_, Nov 03 2011 *)"
			],
			"program": [
				"(PARI) x='x+O('x^50); Vec((1+x)/(1+2*x-x^2)) \\\\ _G. C. Greubel_, Oct 12 2017",
				"(MAGMA) [Round(1/2*((-1-Sqrt(2))^n+(-1+Sqrt(2))^n)): n in [0..30]]; // _G. C. Greubel_, Oct 12 2017"
			],
			"xref": [
				"Cf. A001333, A077985, A251732, A001541 (bisection), A002315 (bisection)."
			],
			"keyword": "sign,easy",
			"offset": "0,3",
			"author": "_Philippe Deléham_, Jun 27 2007",
			"ext": [
				"Corrected by _N. J. A. Sloane_, Oct 05 2008"
			],
			"references": 10,
			"revision": 45,
			"time": "2021-06-22T15:53:11-04:00",
			"created": "2006-12-06T03:00:00-05:00"
		}
	]
}