{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003082",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3082,
			"id": "M2543",
			"data": "1,1,3,6,11,18,32,48,75,111,160,224,313,420,562,738,956,1221,1550,1936,2405,2958,3609,4368,5260,6279,7462,8814,10356,12104,14093,16320,18834,21645,24783,28272,32158,36442,41187,46410,52151,58443,65345,72864",
			"name": "Number of multigraphs with 4 nodes and n edges.",
			"comment": [
				"Also, expansion of Molien series for representation Sym^2(R^n) of the automorphism group of the lattice D_3."
			],
			"reference": [
				"CRC Handbook of Combinatorial Designs, 1996, p. 650.",
				"J. L. Gross and J. Yellen, eds., Handbook of Graph Theory, CRC Press, 2004; p. 517.",
				"F. Harary and E. M. Palmer, Graphical Enumeration, Academic Press, NY, 1973, p. 88, (4.1.19).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A003082/b003082.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Axel Kleinschmidt and Valentin Verschinin, \u003ca href=\"https://arxiv.org/abs/1706.01889\"\u003eTetrahedral modular graph functions\u003c/a\u003e, arXiv:1706.01889 [hep-th], 2017, p. 20.",
				"P. Sarnak and A. Strömbergsson, \u003ca href=\"http://dx.doi.org/10.1007/s00222-005-0488-2\"\u003eMinima of Epstein's zeta function and heights of flat tori\u003c/a\u003e, Inventiones mathematicae, July 2006, Volume 165, Issue 1, pp 115-151.",
				"\u003ca href=\"/index/Rec#order_14\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,0,0,-2,-2,3,0,3,-2,-2,0,0,2,-1)."
			],
			"formula": [
				"G.f.: (x^8-x^7+x^6+x^4+x^2-x+1)/((x-1)^6*(x+1)^2*(x^2+1)*(x^2+x+1)^2).",
				"a(n) = 2*a(n-1)-2*a(n-4)-2*a(n-5)+3*a(n-6)+3*a(n-8)-2*a(n-9)-2*a(n-10)+2*a(n-13)-a(n-14). - _Wesley Ivan Hurt_, Apr 20 2021"
			],
			"mathematica": [
				"CoefficientList[Series[PairGroupIndex[SymmetricGroup[4], s] /.Table[s[i] -\u003e 1/(1 - x^i), {i, 1, 4}], {x, 0, 40}], x] (* _Geoffrey Critzer_, Nov 10 2011 *)",
				"LinearRecurrence[{2,0,0,-2,-2,3,0,3,-2,-2,0,0,2,-1},{1,1,3,6,11,18,32,48,75,111,160,224,313,420},50] (* _Harvey P. Dale_, Oct 09 2016 *)"
			],
			"program": [
				"(PARI) Vec((x^8-x^7+x^6+x^4+x^2-x+1)/((x-1)^6*(x+1)^2*(x^2+1)*(x^2+x+1)^2) + O(x^100)) \\\\ _Colin Barker_, Apr 02 2015"
			],
			"xref": [
				"Cf. A001399, A014395 (5 nodes), A014396, A014397, A014398, row 4 of A192517.",
				"Cf. A290778 (connected)."
			],
			"keyword": "easy,nonn,nice",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Entry improved by comments from _Vladeta Jovovic_, Dec 23 1999"
			],
			"references": 10,
			"revision": 45,
			"time": "2021-04-20T12:07:59-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}