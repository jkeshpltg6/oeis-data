{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A195586",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 195586,
			"data": "1,1,4,3,15,12,37,25,100,75,219,144,501,357,972,615,1995,1380,3665,2285,7052,4767,12255,7488,22305,14817,37524,22707,65775,43068,106837,63769,180436,116667,286251,169584,471173,301589,729404,427815,1169211,741396,1778545,1037149",
			"name": "G.f.: exp( Sum_{n\u003e=1} A163659(n^2)*x^n/n ), where x*exp(Sum_{n\u003e=1} A163659(n)*x^n/n) = S(x) is the g.f. of Stern's diatomic series (A002487).",
			"link": [
				"Paul D. Hanna, \u003ca href=\"/A195586/b195586.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e"
			],
			"formula": [
				"G.f.: exp( Sum_{n\u003e=1} A195587(n)*x^n/n ), where A195587(n) = A163659(n^2).",
				"G.f. A(x) satisfies:",
				"(1) A(x) = (1+x+x^2) * (1+x^2+x^4) * A(x^2)^2.",
				"(2) A(x) = (1+x+x^2) * Product_{n\u003e=0} ( 1 + x^(2*2^n) + x^(4*2^n) )^(3*2^n).",
				"(3) A(x) / A(-x) = (1+x+x^2) / (1-x+x^2).",
				"Bisections: let A(x) = B(x^2) + x*C(x^2), then",
				"(4) B(x) = (1+x) * C(x).",
				"(5) C(x) = (1+x+x^2)^3 * C(x^2)^2.",
				"(6) A(x) = (1+x+x^2) * C(x^2).",
				"(7) A(x)^3 = C(x) * C(x^2).",
				"(8) A(x)^2 = C(x) / (1+x+x^2).",
				"(9) A(x) = ( C(x)/A(x) - C(x^2)^2/A(x^2)^2 ) / (2*x)."
			],
			"example": [
				"G.f.: A(x) = 1 + x + 4*x^2 + 3*x^3 + 15*x^4 + 12*x^5 + 37*x^6 + 25*x^7 +...",
				"where",
				"log(A(x)) = x + 7*x^2/2 - 2*x^3/3 + 31*x^4/4 + x^5/5 - 14*x^6/6 + x^7/7 + 127*x^8/8 +...+ A195587(n)*x^n/n +...",
				"Let C(x) be the odd bisection of g.f. A(x):",
				"C(x) = 1 + 3*x + 12*x^2 + 25*x^3 + 75*x^4 + 144*x^5 + 357*x^6 + 615*x^7 + 1380*x^8 + 2285*x^9 + 4767*x^10 + 7488*x^11 + 14817*x^12 +...+ A237650(n)*x^n +...",
				"then C(x) equals the cube of an integer series:",
				"C(x)^(1/3) = 1 + x + 3*x^2 + 2*x^3 + 9*x^4 + 7*x^5 + 17*x^6 + 10*x^7 + 41*x^8 + 31*x^9 + 75*x^10 + 44*x^11 + 150*x^12 +...+ A237651(n)*x^n +...",
				"which equals A(x)/C(x^2)^(1/3).",
				"The g.f. may be expressed by the product:",
				"A(x) = (1+x+x^2) * (1+x^2+x^4)^3 * (1+x^4+x^8)^6 * (1+x^8+x^16)^12 * (1+x^16+x^32)^24 *...* (1 + x^(2*2^n) + x^(4*2^n))^(3*2^n) *..."
			],
			"program": [
				"(PARI) {A163659(n)=if(n\u003c1,0,if(n%3,1,-2)*sigma(2^valuation(n,2)))}",
				"{a(n)=polcoeff(exp(sum(k=1, n, A163659(k^2)*x^k/k)+x*O(x^n)), n)}",
				"for(n=0,50,print1(a(n),\", \"))",
				"(PARI) /* G.f.: A(x) = (1+x+x^2) * (1+x^2+x^4) * A(x^2)^2: */",
				"{a(n)=local(A=1+x); for(i=1, #binary(n), A=(1+x+x^2)*(1+x^2+x^4)*subst(A^2, x, x^2) +x*O(x^n)); polcoeff(A, n)}",
				"for(n=0, 50, print1(a(n), \", \"))",
				"(PARI) /* G.f.: (1+x+x^2) * Product_{n\u003e=0} (1 + x^(2*2^n) + x^(4*2^n))^(3*2^n): */",
				"{a(n)=local(A=1+x); A=(1+x+x^2)*prod(k=0, #binary(n), (1+x^(2*2^k)+x^(4*2^k)+x*O(x^n))^(3*2^k)); polcoeff(A, n)}",
				"for(n=0, 50, print1(a(n), \", \"))"
			],
			"xref": [
				"Cf. A195587, A163658, A163659, A237650, A002487; variant: A237646."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Paul D. Hanna_, Sep 20 2011",
			"ext": [
				"Entry and formulas revised by _Paul D. Hanna_, May 04 2014"
			],
			"references": 5,
			"revision": 29,
			"time": "2014-05-04T13:01:21-04:00",
			"created": "2011-09-20T21:39:22-04:00"
		}
	]
}