{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A053539",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 53539,
			"data": "0,1,16,192,2048,20480,196608,1835008,16777216,150994944,1342177280,11811160064,103079215104,893353197568,7696581394432,65970697666560,562949953421312,4785074604081152,40532396646334464,342273571680157696",
			"name": "a(n) = n * 8^(n-1).",
			"comment": [
				"The Szeged index of the hypercube Q_n (see the Ashrafi et al. reference (p. 45, last line). - _Emeric Deutsch_, Aug 06 2014"
			],
			"reference": [
				"Albert H. Beiler, Recreations in the Theory of Numbers, Dover, N.Y., 1964, pp. 194-196."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A053539/b053539.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"A. R. Ashrafi, B. Manoochehrian, H. Yousefi-Azari, \u003ca href=\"http://bims.iranjournals.ir/article_79.html\"\u003eOn Szeged polynomial of a graph\u003c/a\u003e, Bull. Iranian Math. Soc., 33, 2007, 37-46. - _Emeric Deutsch_, Aug 06 2014",
				"Frank Ellermann, \u003ca href=\"/A001792/a001792.txt\"\u003eIllustration of binomial transforms\u003c/a\u003e.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (16,-64)."
			],
			"formula": [
				"a(n) = 16*a(n-1) - 64*a(n-2), with a(0)=0, a(1)=1. - _Emeric Deutsch_, Aug 06 2014",
				"From _G. C. Greubel_, May 16 2019: (Start)",
				"G.f.: x/(1-8*x)^2.",
				"E.g.f.: x*exp(8*x). (End)",
				"From _Amiram Eldar_, Oct 28 2020: (Start)",
				"Sum_{n\u003e=1} 1/a(n) = 8*log(8/7).",
				"Sum_{n\u003e=1} (-1)^(n+1)/a(n) = 8*log(9/8). (End)"
			],
			"maple": [
				"a := proc(n) option remember; if n\u003c2 then n else 16*a(n-1)-64*a(n-2) end if end proc: seq(a(n), n = 0 .. 20); # _Emeric Deutsch_, Aug 06 2014"
			],
			"mathematica": [
				"Table[n 8^(n-1),{n,0,20}] (* or *) LinearRecurrence[{16,-64},{0,1},20] (* _Harvey P. Dale_, Feb 01 2017 *)"
			],
			"program": [
				"(MAGMA) [n*8^(n-1): n in [0..20]]; // _Vincenzo Librandi_, Feb 09 2011",
				"(PARI) a(n) = n*8^(n-1); \\\\ _Joerg Arndt_, Aug 07 2014",
				"(Sage) [n*8^(n-1) for n in (0..20)] # _G. C. Greubel_, May 16 2019",
				"(GAP) List([0..20], n-\u003e n*8^(n-1)) # _G. C. Greubel_, May 16 2019"
			],
			"xref": [
				"Binomial transform of A027473.",
				"Cf. A001787, A053464, A053469, A053540."
			],
			"keyword": "easy,nonn",
			"offset": "0,3",
			"author": "_Barry E. Williams_, Jan 15 2000",
			"ext": [
				"Offset corrected and name edited by _Emeric Deutsch_, Aug 06 2014"
			],
			"references": 5,
			"revision": 66,
			"time": "2020-10-28T09:30:53-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}