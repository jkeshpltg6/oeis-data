{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A284621",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 284621,
			"data": "1,5,11,15,21,27,31,37,41,47,53,57,63,69,73,79,83,89,95,99,105,109,115,121,125,131,137,141,147,151,157,163,167,173,179,183,189,193,199,205,209,215,219,225,231,235,241,247,251,257,261,267,273,277,283,287",
			"name": "Positions of 0 in A284620.",
			"comment": [
				"This sequence and A005843 and A130568 partition the positive integers into sequences with slopes t = 1+sqrt(5), u = 3+sqrt(5), v = 2, where 1/t + 1/u + 1/v = 1.  The positions of 1 in A284620 are given by A005843, and of 2, by A130568.",
				"From _Michel Dekking_, Mar 17 2020: (Start)",
				"This sequence is a generalized Beatty sequence.",
				"It was shown in the Comments of A284620 that A284620 is the letter-to-letter image of the fixed point x = ABCDABCDCD... of the morphism",
				"      mu:   A-\u003eAB, B-\u003eCD, C-\u003eABCD, D-\u003eCD,",
				"with the letter-to-letter map lambda defined by",
				"      lambda:  A-\u003e0, B-\u003e1, C-\u003e2, D-\u003e1.",
				"Note that A284620(n)=0  iff  x(n) = A, where x = ABCDABCDCD... is the fixed point of mu.  The return words of A in x are ABCD and ABCDCD.  Coding these two return words by their lengths, mu induces a morphism rho on the coded return words given by",
				"      rho(4) = 46, rho(4) = 466.",
				"The difference sequence (a(n+1)-a(n)) equals the unique fixed point r = 4646646466... of rho.",
				"The morphism g on the alphabet {a,b} given by",
				"       g(a) = ab, g(b) =abb",
				"was introduced in A284620. We see that rho is just an alphabet change of the morphism g.",
				"Let f given by f(b) = ba, f(a) = b be the Fibonacci morphism on the alphabet {b,a} with fixed point x_F = babbababba....",
				"Let x_G = ababbababb... be the fixed point of g. It is well-known (see, e.g., Lemma 12 in \"Morphic words...\"), that x_G =  a x_F.",
				"In general the partial sums of  x_F are equal to the generalized  Beatty sequence V  given by V(n) =  p*floor(n*phi) +q*n+r, where p = a-b  and  q = 2*b-a.  See Lemma 8 in the Allouche and Dekking paper. Here we obtain p = 2, q = 2. So a(n) = 2*floor((n-1)*phi) + 2*n - 1, for n\u003e0.",
				"(End)"
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A284621/b284621.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"J.-P. Allouche, F. M. Dekking, \u003ca href=\"https://doi.org/10.2140/moscow.2019.8.325\"\u003eGeneralized Beatty sequences and complementary triples\u003c/a\u003e, Moscow Journal of Combinatorics and Number Theory 8, 325-341 (2019).",
				"M. Dekking, \u003ca href=\"https://doi.org/10.1016/j.tcs.2019.12.036\"\u003eMorphic words, Beatty sequences and integer images of the Fibonacci language\u003c/a\u003e, Theoretical Computer Science  809,  407-417 (2020)."
			],
			"formula": [
				"a(n+1) = 2*A001950(n) + 1, n\u003e0. - _Michel Dekking_, Mar 17 2020"
			],
			"example": [
				"As a word, A284620 = 012101212101210121..., in which 0 is in positions 1,5,11,15,..."
			],
			"mathematica": [
				"s = Nest[Flatten[# /. {0 -\u003e {0, 1}, 1 -\u003e {0}}] \u0026, {0}, 13]  (* A003849 *)",
				"w = StringJoin[Map[ToString, s]]",
				"w1 = StringReplace[w, {\"00\" -\u003e \"2\"}]",
				"st = ToCharacterCode[w1] - 48 (* A284620 *)",
				"Flatten[Position[st, 0]]  (* A284621 *)",
				"Flatten[Position[st, 1]]  (* A005843 *)",
				"Flatten[Position[st, 2]]  (* A130568 *)"
			],
			"xref": [
				"Cf. A005843, A130568, A284620, A001950."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Clark Kimberling_, May 02 2017",
			"references": 2,
			"revision": 10,
			"time": "2020-03-17T12:14:48-04:00",
			"created": "2017-05-02T20:48:19-04:00"
		}
	]
}