{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A136656",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 136656,
			"data": "1,0,-2,0,6,4,0,-24,-36,-8,0,120,300,144,16,0,-720,-2640,-2040,-480,-32,0,5040,25200,27720,10320,1440,64,0,-40320,-262080,-383040,-199920,-43680,-4032,-128,0,362880,2963520,5503680,3764880,1142400,163968,10752,256,0,-3628800,-36288000,-82978560",
			"name": "Coefficients for rewriting generalized falling factorials into ordinary falling factorials.",
			"comment": [
				"Generalization of (signed) Lah number triangle A008297 (amended with a trivial row n=0 and a column k=0 in order to have a Sheffer triangle structure of the Jabotinsky type).",
				"product(s*t-j,j=0..n-1) := fallfac(s*t,n) (falling factorial with n factors) is called generalized factorial of t of order n and scale parameter s in the Charalambides reference p. 301 ch. 8.4.",
				"The s-family of triangles L(s;n,k) (in the Charalambides reference called C(n,k;-s)) is defined for integer s by fallfac(-s*t,n) = ((-1)^n)*risefac(s*t,n) = sum(L(s;n,k)*fallfac(t,k),k=0..n), n\u003e=0. risefac(x,n):=product(x+j,j=0..n-1) for the rising factorials.",
				"For positive s the signless triangles |L(s;n,k)| = L(s;n,k)*(-1)^n satisfies risefac(s*t,n) = sum(|L(s;n,k)|*fallfac(t,k),k=0..n), n\u003e=0.",
				"For negative s see the combinatorial interpretation given in the Charalambides reference, Example 8.8, p. 313: Coupon collector's problem.",
				"|T(n,k)| = B_{n,k}((j+2)!; j\u003e=0) where B_{n,k} are the partial Bell polynomials. - _Peter Luschny_, May 11 2015"
			],
			"reference": [
				"Ch. A. Charalambides, Enumerative Combinatorics, Chapman \u0026 Hall/CRC, Boca Raton, Florida, 2002, ch. 8.4 p. 301 ff. Table 8.3 (with row n=0 and column k=0 and s=-2)."
			],
			"link": [
				"M. Janjic, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL12/Janjic/janjic22.html\"\u003eSome classes of numbers and derivatives\u003c/a\u003e, JIS 12 (2009) 09.8.3",
				"W. Lang, \u003ca href=\"/A136656/a136656.txt\"\u003eFirst ten rows and more\u003c/a\u003e.",
				"Peter Luschny, \u003ca href=\"https://oeis.org/wiki/User:Peter_Luschny/Marginalia\"\u003eThe Bell Transform\u003c/a\u003e"
			],
			"formula": [
				"Recurrence: a(n,k) = 0 if n\u003ck; a(n,0) = 1 if n=0 else 0 and a(n,k) = (-2*k-(n-1))*a(n-1,k)-2*a(n-1,k-1). From the Charalambides reference Theorem 8.19, p. 309, for s=-2.",
				"E.g.f. column k: (1/(1+x)^2 - 1)^k/k!, k\u003e=0. From the Charalambides reference Theorem 8.14, p. 305 for s=-2.(hence a Sheffer triangle of Jabotinsky type).",
				"a(n,k) = sum(((-1)^(k-r))*binomial(k,r)*fallfac(-2*r,n),r=0..k)/k!, n\u003e=k\u003e=0. From the Charalambides reference Theorem 8.15, p. 306 for s=-2.",
				"a(n,k) = sum(S1(n,r)*S2(r,k)*(-2)^r,r=k..n) with the Stirling numbers S1(n,r)= A048993(n,r) and S2(r,k)= A048993(r,k). From the Charalambides reference Theorem 8.13, p.304 for s=-2.",
				"a(n,k) = sum(M_3(n,k,q)*product(fallfac(-2,j)^e(n,m,q,j),j=1..n),q=1..p(n,k)) if n\u003e=k\u003e=1, else 0. Here p(n,k)=A008284(n,m), the number of k parts partitions of n, the M_3 partition numbers are given in A036040 and e(n,m,q,j) is the exponent of j in the q-th k parts partition of n. Rewritten eq. (8.50), Theorem 8.16, p. 307, from the Charalambides reference for s=-2.",
				"Recurrence for the unsigned case: a(n,k) = Sum_{j=0..n-k} a(n-j-1,k-1)*C(n-1,j)*(j+2)! if k\u003c\u003e0 else k^n. - _Peter Luschny_, Mar 31 2015"
			],
			"example": [
				"Triangle starts:",
				"[1]",
				"[0,   -2]",
				"[0,    6,    4]",
				"[0,  -24,  -36,   -8]",
				"[0,  120,  300,  144,  16]",
				"...",
				"Recurrence: a(4,2) = -7*a(3,2)-2*a(3,1) = -7*(-36) -2*(-24) = 300.",
				"a(4,2)=300 as sum over the M3 numbers A036040 for the 2 parts partitions of 4: 4*fallfac(-2,1)^1*fallfac(-2,3)^1 + 3*fallfac(-2,2)^2 = 4*(-2)*(-24)+3*6^2 = 300.",
				"Row n=3: [0,-24,-36,-8] for the coefficients in rewriting fallfac(-2*t,3)=((-1)^3)*risefac(2*t,3) = ((-1)^3)*(2*t)*(2*t+1)*(2*t+2) = 0*1 -24*t -36*t*(t-1) -8*t*(t-1)*(t-2)."
			],
			"maple": [
				"# The function BellMatrix is defined in A264428.",
				"BellMatrix(n -\u003e (-1)^(n+1)*(n+2)!, 9); # _Peter Luschny_, Jan 27 2016"
			],
			"mathematica": [
				"fallfac[n_, k_] := Pochhammer[n - k + 1, k]; a[n_, k_] := Sum[(-1)^(k - r)*Binomial[k, r]*fallfac[-2*r, n], {r, 0, k}]/k!; Table[a[n, k], {n, 0, 9}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Jul 09 2013 *)",
				"BellMatrix[f_Function, len_] := With[{t = Array[f, len, 0]}, Table[BellY[n, k, t], {n, 0, len - 1}, {k, 0, len - 1}]];",
				"rows = 10;",
				"M = BellMatrix[(-1)^(# + 1)*(# + 2)!\u0026, rows];",
				"Table[M[[n, k]], {n, 1, rows}, {k, 1, n}] // Flatten (*_Jean-François Alcover_, Jun 23 2018, after _Peter Luschny_ *)"
			],
			"program": [
				"(Sage)",
				"@CachedFunction",
				"def T(n, k):  # unsigned case",
				"    if k == 0: return 1 if n == 0 else 0",
				"    return sum(T(n-j-1,k-1)*(j+1)*(j+2)*gamma(n)/gamma(n-j) for j in (0..n-k))",
				"for n in range(7): [T(n,k) for k in (0..n)] # _Peter Luschny_, Mar 31 2015"
			],
			"xref": [
				"Column sequences (unsigned) 2*A001710, 4*A136659, 8*A136660, 16*A136661 for k=1..4.",
				"Cf. A136657 without row n=0 and column k=0, divided by 2."
			],
			"keyword": "sign,easy,tabl",
			"offset": "0,3",
			"author": "_Wolfdieter Lang_, Feb 22 2008, Sep 09 2008",
			"references": 7,
			"revision": 29,
			"time": "2019-08-28T17:15:43-04:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}