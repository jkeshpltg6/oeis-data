{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A231535",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 231535,
			"data": "6,4,9,3,9,3,9,4,0,2,2,6,6,8,2,9,1,4,9,0,9,6,0,2,2,1,7,9,2,4,7,0,0,7,4,1,6,6,4,8,5,0,5,7,1,1,5,1,2,3,6,1,4,4,6,0,9,7,8,5,7,2,9,2,6,6,4,7,2,3,6,9,7,1,2,1,8,1,3,0,7,9,3,4,1,4,5,7,8,1,5,6,5,0,1,9,9,5,0,3,3,9,7,9,4",
			"name": "Decimal expansion of Pi^4/15.",
			"comment": [
				"Under proper scaling, the radiation distribution density function in terms of frequency is given by prl(x) = x^3/(exp(x)-1), the Planck's radiation law. This constant is the integral of prl(x) from 0 to infinity and leads to the total amount of electromagnetic radiation emitted by a body.",
				"Also, in an 8-dimensional unit-radius hypersphere, equals one-fifth of its surface (A164109), and twice the integral of r^2 over its volume."
			],
			"link": [
				"Stanislav Sykora, \u003ca href=\"/A231535/b231535.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Ilham A. Aliev, Ayhan Dil, \u003ca href=\"https://arxiv.org/abs/2008.02488\"\u003eTornheim-like series, harmonic numbers and zeta values\u003c/a\u003e, arXiv:2008.02488 [math.NT], 2020, p. 4.",
				"Stanislav Sykora, \u003ca href=\"http://dx.doi.org/10.3247/SL1Math05.002\"\u003eSurface Integrals over n-Dimensional Spheres\u003c/a\u003e",
				"Stanislav Sykora, \u003ca href=\"http://dx.doi.org/10.3247/SL1Math05.001\"\u003eVolume Integrals over n-Dimensional Ellipsoids\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Planck%27s_law\"\u003ePlank's Law\u003c/a\u003e",
				"Ke Xiao, \u003ca href=\"http://www.ejtp.com/articles/ejtpv8i25p379.pdf\"\u003eDimensionless Constants and Blackbody Radiation Laws\u003c/a\u003e, Electronic Journal of Theoretical Physics, 8(2011), 379-388, Eq.6."
			],
			"formula": [
				"Equals 6*zeta(4), see A013662. [_Bruno Berselli_, Nov 12 2013]",
				"Equals Gamma(4)*zeta(4) = 2*3*Product_{prime p} (p^4/(p^4-1)). - _Stanislav Sykora_, Oct 20 2014",
				"Equals Sum_(n, m \u003e=1} H(n+m)/(n*m*(n+m)) where H(n) is the n-th harmonic number. See Aliev and Dil. - _Michel Marcus_, Aug 07 2020",
				"From _Amiram Eldar_, Aug 14 2020: (Start)",
				"Equals Integral_{x=0..oo} x^3/(exp(x)-1) dx.",
				"Equals Integral_{x=0..1} log(x)^3/(x-1) dx. (End)"
			],
			"example": [
				"6.4939394022668291490960221792470074166485057115123614460978572926647..."
			],
			"mathematica": [
				"RealDigits[Pi^4/15, 10, 105][[1]] (* _Bruno Berselli_, Nov 12 2013 *)"
			],
			"program": [
				"(PARI) Pi^4/15 \\\\ _Michel Marcus_, Aug 07 2020",
				"(PARI) 6*prodeulerrat(p^4/(p^4-1)) \\\\ _Hugo Pfoertner_, Aug 07 2020"
			],
			"xref": [
				"Cf. A000796, A013662, A081819, A164109."
			],
			"keyword": "nonn,cons,easy",
			"offset": "1,1",
			"author": "_Stanislav Sykora_, Nov 12 2013",
			"references": 6,
			"revision": 37,
			"time": "2020-08-14T16:29:14-04:00",
			"created": "2013-11-12T21:02:44-05:00"
		}
	]
}