{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A025586",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 25586,
			"data": "1,2,16,4,16,16,52,8,52,16,52,16,40,52,160,16,52,52,88,20,64,52,160,24,88,40,9232,52,88,160,9232,32,100,52,160,52,112,88,304,40,9232,64,196,52,136,160,9232,48,148,88,232,52,160,9232,9232,56,196,88,304,160,184,9232",
			"name": "Largest value in '3x+1' trajectory of n.",
			"comment": [
				"Here by definition the trajectory ends when 1 is reached. Therefore this sequence differs for n = 1 and n = 2 from A056959, which considers the orbit ending in the infinite loop 1 -\u003e 4 -\u003e 2 -\u003e 1.",
				"a(n) = A220237(n,A006577(n)). - _Reinhard Zumkeller_, Jan 03 2013",
				"A006885 and A006884 give record values and where they occur. - _Reinhard Zumkeller_, May 11 2013",
				"For n \u003e 2, a(n) is divisible by 4. See the explanatory comment in A056959. - _Peter Munn_, Oct 14 2019"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A025586/b025586.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Philippe Picart, \u003ca href=\"http://trucsmaths.free.fr/js_syracuse.htm\"\u003eAlgorithme de Collatz et conjecture de Syracuse\u003c/a\u003e",
				"\u003ca href=\"/index/3#3x1\"\u003eIndex entries for sequences related to 3x+1 (or Collatz) problem\u003c/a\u003e"
			],
			"example": [
				"The 3x + 1 trajectory of 9 is 9, 28, 14, 7, 22, 11, 34, 17, 52, 26, 13, 40, 20, 10, 5, 16, 8, 4, 2, 1 (see A033479). Since the largest number in that sequence is 52, a(9) = 52."
			],
			"maple": [
				"a:= proc(n) option remember; `if`(n=1, 1,",
				"      max(n, a(`if`(n::even, n/2, 3*n+1))))",
				"    end:",
				"seq(a(n), n=1..87);  # _Alois P. Heinz_, Oct 16 2021"
			],
			"mathematica": [
				"collatz[a0_Integer, maxits_:1000] := NestWhileList[If[EvenQ[#], #/2, 3# + 1] \u0026, a0, Unequal[#, 1, -1, -10, -34] \u0026, 1, maxits]; (* collatz[n] function definition by Eric Weisstein *) Flatten[Table[Take[Sort[Collatz[n], Greater], 1], {n, 60}]] (* _Alonso del Arte_, Nov 14 2007 *)",
				"collatzMax[n_] := Module[{r = m = n}, While[m \u003e 2, If[OddQ[m], m = 3 * m + 1; If[m \u003e r, r = m], m = m/2]]; r]; Table[ collatzMax[n], {n, 100}] (* _Jean-François Alcover_, Jan 28 2015, after _Charles R Greathouse IV_ *)",
				"(* Using Weisstein's collatz[n] definition above *) Table[Max[collatz[n]], {n, 100}] (* _Alonso del Arte_, May 25 2019 *)"
			],
			"program": [
				"(PARI) a(n)=my(r=n);while(n\u003e2,if(n%2,n=3*n+1;if(n\u003er,r=n),n/=2));r \\\\ _Charles R Greathouse IV_, Jul 19 2011",
				"(Haskell)",
				"a025586 = last . a220237_row",
				"-- _Reinhard Zumkeller_, Jan 03 2013, Aug 29 2012",
				"(Python)",
				"def a(n):",
				"    if n\u003c2: return 1",
				"    l=[n, ]",
				"    while True:",
				"        if n%2==0: n//=2",
				"        else: n = 3*n + 1",
				"        if not n in l:",
				"            l+=[n, ]",
				"            if n\u003c2: break",
				"        else: break",
				"    return max(l)",
				"print([a(n) for n in range(1, 101)]) # _Indranil Ghosh_, Apr 14 2017",
				"(Scala) def collatz(n: Int): Int = (n % 2) match {",
				"  case 0 =\u003e n / 2",
				"  case 1 =\u003e 3 * n + 1",
				"}",
				"def collatzTrajectory(start: Int): List[Int] = if (start == 1) List(1)",
				"else {",
				"  import scala.collection.mutable.ListBuffer",
				"  var curr = start; var trajectory = new ListBuffer[Int]()",
				"  while (curr \u003e 1) { trajectory += curr; curr = collatz(curr) }",
				"  trajectory.toList",
				"}",
				"for (n \u003c- 1 to 100) yield collatzTrajectory(n).max // _Alonso del Arte_, Jun 02 2019"
			],
			"xref": [
				"Essentially the same as A056959: only a(1) and a(2) differ, see Comments.",
				"Cf. A006370, A006577, A006884, A006885, A220237."
			],
			"keyword": "nonn,nice,look",
			"offset": "1,2",
			"author": "_David W. Wilson_",
			"references": 52,
			"revision": 65,
			"time": "2021-10-16T19:03:35-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}