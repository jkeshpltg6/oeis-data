{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A162515",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 162515,
			"data": "0,1,1,0,1,0,1,1,0,2,0,1,0,3,0,1,1,0,4,0,3,0,1,0,5,0,6,0,1,1,0,6,0,10,0,4,0,1,0,7,0,15,0,10,0,1,1,0,8,0,21,0,20,0,5,0,1,0,9,0,28,0,35,0,15,0,1,1,0,10,0,36,0,56,0,35,0,6,0,1,0,11,0,45,0,84,0,70,0,21,0,1",
			"name": "Triangle of coefficients of polynomials defined by Binet form: P(n,x) = (U^n - L^n)/d, where U = (x + d)/2, L = (x - d)/2, d = sqrt(x^2 + 4).",
			"comment": [
				"Row sums 0,1,1,2,3,5,... are the Fibonacci numbers, A000045.",
				"Note that the coefficients are given in decreasing order. - _M. F. Hasler_, Dec 07 2011",
				"Essentially a mirror image of A168561. - _Philippe Deléham_, Dec 08 2013"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A162515/b162515.txt\"\u003eRows n = 0..101 of triangle\u003c/a\u003e",
				"T. Copeland, \u003ca href=\"https://tcjpn.wordpress.com/2015/10/12/the-elliptic-lie-triad-kdv-and-ricattt-equations-infinigens-and-elliptic-genera/\"\u003eAddendum to Elliptic Lie Triad\u003c/a\u003e"
			],
			"formula": [
				"P(n,x) = x*P(n-1, x) + P(n-2, x), where P(0,x)=0 and P(1,x)=1.",
				"T(n,k) = T(n-1, k) + T(n-2, k-2) for n\u003e=2. - _Philippe Deléham_, Dec 08 2013"
			],
			"example": [
				"Polynomial expansion:",
				"  0;",
				"  1;",
				"  x;",
				"  x^2 + 1;",
				"  x^3 + 2*x;",
				"  x^4 + 3*x^2 + 1;",
				"First rows:",
				"  0;",
				"  1;",
				"  1, 0;",
				"  1, 0, 1;",
				"  1, 0, 2, 0;",
				"  1, 0, 3, 0, 1;",
				"  1, 0, 4, 0, 3, 0;",
				"Row 6 matches P(6,x)=x^5 + 4*x^3 + 3*x."
			],
			"maple": [
				"0, seq(seq(`if`(`mod`(k,2)=0, binomial(n-k/2, k/2), 0), k = 0..n), n = 0..15); # _G. C. Greubel_, Jan 01 2020"
			],
			"mathematica": [
				"Join[{0}, Table[If[EvenQ[k], Binomial[n-k/2, k/2], 0], {n,0,15}, {k,0,n} ]//Flatten] (* _G. C. Greubel_, Jan 01 2020 *)"
			],
			"program": [
				"(PARI) row(n,d=sqrt(1+x^2/4+O(x^n))) = Vec(if(n,Pol(((x/2+d)^n-(x/2-d)^n)/d)\u003e\u003e1)) \\\\ _M. F. Hasler_, Dec 07 2011, edited Jul 05 2021",
				"(MAGMA)",
				"function T(n,k)",
				"  if (k mod 2) eq 0 then return Round( Gamma(n-k/2+1)/(Gamma(k/2+1)*Gamma(n-k+1)));",
				"  else return 0;",
				"  end if; return T; end function;",
				"[0] cat [T(n,k): k in [0..n], n in [0..15]]; // _G. C. Greubel_, Jan 01 2020",
				"(Sage)",
				"@CachedFunction",
				"def T(n, k):",
				"    if (k%2==0): return binomial(n-k/2, k/2)",
				"    else: return 0",
				"[0]+flatten([[T(n, k) for k in (0..n)] for n in (0..15)]) # _G. C. Greubel_, Jan 01 2020",
				"(GAP)",
				"T:= function(n,k)",
				"    if (k mod 2)=0 then return Binomial(n- k/2, k/2);",
				"    else return 0;",
				"    fi; end;",
				"Concatenation([0], Flat(List([0..15], n-\u003e List([0..n], k-\u003e T(n,k) ))) ); # _G. C. Greubel_, Jan 01 2020"
			],
			"xref": [
				"Cf. A000045, A049310, A053119, A162514, A162516, A162517."
			],
			"keyword": "nonn,tabf",
			"offset": "0,10",
			"author": "_Clark Kimberling_, Jul 05 2009",
			"references": 8,
			"revision": 42,
			"time": "2021-07-05T11:26:00-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}