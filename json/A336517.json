{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A336517",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 336517,
			"data": "1,0,2,-1,0,4,0,-2,0,8,7,0,-8,0,16,0,14,0,-80,0,32,-31,0,28,0,-80,0,64,0,-62,0,392,0,-224,0,128,127,0,-496,0,1568,0,-1792,0,256,0,762,0,-992,0,9408,0,-1536,0,512,-2555,0,1524,0,-4960,0,6272,0,-3840,0,1024",
			"name": "T(n, k) = numerator([x^k] b(n, x)), where b(n, x) = 2^n*Sum_{k=0..n} binomial(n, k) * Bernoulli(k, 1/2) * x^(n-k). Triangle read by rows, for 0 \u003c= k \u003c= n.",
			"comment": [
				"Consider polynomials B_a(n, x) = a^n*Sum_{k=0..n} binomial(n, k)*Bernoulli(k, 1/a)*x^(n - k), with a != 0. They form an Appell sequence, the case a = 1 are the Bernoulli polynomials. T(n, k) are the numerators of the coefficients of the polynomials in the case a = 2."
			],
			"formula": [
				"Denominator(b(n, 1)) = A141459(n).",
				"Numerator(b(n, -1)) = A285866(n).",
				"Numerator(b(n, 0) = A157779(n)."
			],
			"example": [
				"Rational polynomials start, coefficients of [numerators | denominators]:",
				"                                           [ [1], [ 1]]",
				"                                       [[0,   2], [ 1, 1]]",
				"                                   [[-1, 0,   4], [ 3, 1, 1]]",
				"                             [[0,    -2, 0,   8], [ 1, 1, 1, 1]]",
				"                          [[7, 0,    -8, 0,  16], [15, 1, 1, 1, 1]]",
				"                    [[0,   14, 0,   -80, 0,  32], [ 1, 3, 1, 3, 1, 1]]",
				"               [[-31, 0,   28, 0,   -80, 0,  64], [21, 1, 1, 1, 1, 1, 1]]",
				"           [[0,  -62, 0,  392, 0,  -224, 0, 128], [ 1, 3, 1, 3, 1, 1, 1, 1]]",
				"      [[127, 0, -496, 0, 1568, 0, -1792, 0, 256], [15, 1, 3, 1, 3, 1, 3, 1, 1]]",
				"   [[0, 762, 0, -992, 0, 9408, 0, -1536, 0, 512], [ 1, 5, 1, 1, 1, 5, 1, 1, 1, 1]]"
			],
			"maple": [
				"Bcp := n -\u003e 2^n*add(binomial(n,k)*bernoulli(k,1/2)*x^(n-k), k=0..n):",
				"polycoeff := p -\u003e seq(numer(coeff(p, x, k)), k = 0..degree(p, x)):",
				"Trow := n -\u003e polycoeff(Bcp(n)): seq(Trow(n), n=0..10);"
			],
			"xref": [
				"Cf. A285865 (denominators), A336454 (polynomial denominator), A141459, A157779, A285866."
			],
			"keyword": "sign,frac,tabl",
			"offset": "0,3",
			"author": "_Peter Luschny_, Jul 24 2020",
			"references": 2,
			"revision": 14,
			"time": "2020-07-26T12:23:50-04:00",
			"created": "2020-07-24T14:15:21-04:00"
		}
	]
}