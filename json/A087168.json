{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A087168",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 87168,
			"data": "1,-1,-1,7,-17,23,-1,-89,271,-457,287,967,-4049,8279,-8641,-7193,56143,-139657,194399,-24569,-703889,2209943,-3814273,2603047,7447951,-32756041,68476319,-74404793,-50690897,449691863,-1146312001,1640168551,-335257649,-5554901257",
			"name": "Expansion of (1 + 2*x)/(1 + 3*x + 4*x^2).",
			"comment": [
				"For positive n, a(n) equals 2^n times the permanent of the (2n) X (2n) tridiagonal matrix with 1/sqrt(2)'s along the main diagonal, and i's along the superdiagonal and the subdiagonal (i is the imaginary unit). - _John M. Campbell_, Jul 08 2011",
				"For n \u003e 3, equals -1 times the determinant of the (n-2) X (n-2) matrix with 2^2's along the superdiagonal, 3^2's along the main diagonal, 4^2's along the subdiagonal, etc., and 0's everywhere else. - _John M. Campbell_, Dec 01 2011"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A087168/b087168.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (-3,-4)."
			],
			"formula": [
				"G.f.: (1+2*x)/(1+3*x+4*x^2).",
				"a(n) = -3*a(n-1) - 4*a(n-2); a(0)=1, a(1)=-1.",
				"a(n) = Sum_{k=0..n} C(n+k,2*k)*(-2)^(n-k).",
				"a(n) = (1/14)*i*sqrt(7)*(-3/2 - (1/2)*i*sqrt(7))^n - (1/14)*i*sqrt(7)*(-3/2 + (1/2)*i*sqrt(7))^n + (1/2)*(-3/2 + (1/2)*i*sqrt(7))^n + (1/2)*(-3/2 - (1/2)*i*sqrt(7))^n, with n\u003e=0 and i=sqrt(-1). - _Paolo P. Lava_, Jun 12 2008",
				"a(n) = -a(-1-n) * 2^(2*n+1) = A001607(2*n + 1) for all n in Z. - _Michael Somos_, Sep 19 2014"
			],
			"example": [
				"G.f. = 1 - x - x^2 + 7*x^3 - 17*x^4 + 23*x^5 - x^6 - 89*x^7 + 271*x^8 + ..."
			],
			"mathematica": [
				"CoefficientList[Series[(1 + 2x)/(4x^2 + 3x + 1), {x, 0, 30}], x]",
				"Table[-Det[Array[Sum[KroneckerDelta[#1, #2+q]*(q+3)^2, {q, -1, n-2}] \u0026, {n-2, n-2}]], {n, 4, 50}] (* _John M. Campbell_, Dec 01 2011 *)",
				"LinearRecurrence[{-3,-4},{1,-1},40] (* _Harvey P. Dale_, Apr 23 2014 *)"
			],
			"program": [
				"(MAGMA) a087168:=func\u003c n | \u0026+[ Binomial(n+k, 2*k)*(-2)^(n-k): k in [0..n] ] \u003e; [ a087168(n): n in [0..35] ];",
				"(PARI) {a(n) = real( (-1 - quadgen(-7))^n )}; /* _Michael Somos_, Sep 19 2014 */"
			],
			"xref": [
				"Cf. A001607."
			],
			"keyword": "easy,sign",
			"offset": "0,4",
			"author": "Mario Catalani (mario.catalani(AT)unito.it), Aug 22 2003",
			"references": 6,
			"revision": 40,
			"time": "2021-06-12T06:51:28-04:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}