{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A193252",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 193252,
			"data": "1,75,365,1015,2169,3971,6565,10095,14705,20539,27741,36455,46825,58995,73109,89311,107745,128555,151885,177879,206681,238435,273285,311375,352849,397851,446525,499015,555465,616019,680821,750015,823745,902155,985389,1073591",
			"name": "Great rhombicuboctahedron with faces of centered polygons.",
			"comment": [
				"The sequence starts with a central dot and expands outward with (n-1) centered polygonal pyramids producing a great rhombicosidodecahedron. Each iteration requires the addition of (n-2) edge units and (n-1) vertices to complete the centered polygon of each face: centered squares, centered octagons and centered hexagons."
			],
			"link": [
				"Bruno Berselli, \u003ca href=\"/A193252/b193252.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"OEIS Wiki, \u003ca href=\"/wiki/(Centered_polygons)_pyramidal_numbers\"\u003e(Centered polygons) pyramidal numbers\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Tetrahedral_number\"\u003eTetrahedral number\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Triangular_number\"\u003eTriangular number\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Centered_polygonal_number\"\u003eCentered polygonal number\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-6,4,-1)."
			],
			"formula": [
				"a(n) = 24*n^3 - 36*n^2 + 14*n - 1.",
				"G.f.: x*(1+x)*(1+70*x+x^2)/(1-x)^4; a(n) = (2*n-1)*A069190(n). - _Bruno Berselli_, Jul 21 2011",
				"a(n) = 4*a(n-1) - 6*a(n-2) + 4*a(n-3) - a(n-4); a(0)=1, a(1)=75, a(2)=365, a(3)=1015. - _Harvey P. Dale_, Jul 27 2011",
				"a(n) = 72 * A000330(n-1) + A005408(n-1). - _Bruce J. Nicholson_, Feb 23 2019",
				"E.g.f.: 1 + (-1 + 2*x + 36*x^2 + 24*x^3)*exp(x). - _G. C. Greubel_, Feb 26 2019"
			],
			"mathematica": [
				"Table[24n^3-36n^2+14n-1,{n,40}] (* or *) LinearRecurrence[{4,-6,4,-1},{1,75,365,1015},40] (* _Harvey P. Dale_, Jul 27 2011 *)"
			],
			"program": [
				"(Excel) =24*ROW()^3-36*ROW()^2+14*ROW()-1",
				"(MAGMA) A069190:=func\u003cn | 12*n^2-12*n+1\u003e; [(2*n-1)*A069190(n): n in [1..40]];  // _Bruno Berselli_, Jul 21 2011",
				"(PARI) for(n=1,40, print1(24*n^3-36*n^2+14*n-1\", \"));  \\\\ _Bruno Berselli_, Jul 21 2011",
				"(Sage) [24*n^3 -36*n^2 +14*n -1 for n in (1..40)] # _G. C. Greubel_, Feb 26 2019",
				"(GAP) List([1..40], n-\u003e 24*n^3 -36*n^2 +14*n -1) # _G. C. Greubel_, Feb 26 2019"
			],
			"xref": [
				"First differences in 2*A158591.",
				"Cf. A001844 (centered square numbers), A016754 (centered octagonal numbers), A003215 (centered hexagonal numbers)."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Craig Ferguson_, Jul 19 2011",
			"references": 2,
			"revision": 64,
			"time": "2019-10-11T08:39:31-04:00",
			"created": "2011-07-20T11:25:03-04:00"
		}
	]
}