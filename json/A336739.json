{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A336739",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 336739,
			"data": "1,2,3,4,6,7,8,13,14,15,25,26,33,50,51,52,95,152,153,295,296,297,542,543,672,1329,1330,2055,4093,4094,4095,6992,10697,10698,21375,21376,21377,39051,55948,55949,86454,86455,130396,260765,260766,365839,731649,1100442",
			"name": "a(n) is the number of decompositions of H(n,1) into disjoint unions of H(j,k) where H(j,k) is the set of numbers { (2*i-1)*(2*k-1), 1 \u003c= i \u003c= j }.",
			"link": [
				"Miloslav Znojil, \u003ca href=\"https://arxiv.org/abs/2010.15014\"\u003eNon-Hermitian N-state degeneracies: unitary realizations via antisymmetric anharmonicities\u003c/a\u003e, arXiv:2010.15014 [quant-ph], 2020.",
				"Miloslav Znojil, \u003ca href=\"https://arxiv.org/abs/2102.12272\"\u003eQuantum phase transitions mediated by clustered non-Hermitian degeneracies\u003c/a\u003e, arXiv:2102.12272 [quant-ph], 2021.",
				"Miloslav Znojil, \u003ca href=\"https://doi.org/10.1103/PhysRevE.103.032120\"\u003eQuantum phase transitions mediated by clustered non-Hermitian degeneracies\u003c/a\u003e, Physical Review E 103 (2021), 032120.",
				"Miloslav Znojil, \u003ca href=\"https://arxiv.org/abs/2108.07110\"\u003eBose-Einstein condensation processes with nontrivial geometric multiplicites realized via PT-symmetric and exactly solvable linear-Bose-Hubbard building blocks\u003c/a\u003e, arXiv:2108.07110 [quant-ph], 2021."
			],
			"example": [
				"H(n,1) are the sets {1}, {1,3}, {1,3,5}, {1,3,5,7}, ...",
				"H(n,2) are the sets {3}, {3,9}, {3,9,15}, {3,9,15,21}, ...",
				"H(n,3) are the sets {5}, {5,15}, {5,15,25}, {5,15,25,35}, ...",
				"a(2) = 2 because there are two decompositions of H(2,1) = {1,3}: the trivial H(2,1) and H(1,1) + H(1,2) = {1} + {3}.",
				"The a(5) = 6 decompositions of {1,3,5,7,9} are:",
				"  {{1,3,5,7,9}},",
				"  {{1,3,5,7}, {9}},",
				"  {{1,3,5}, {7}, {9}},",
				"  {{1,3}, {5}, {7}, {9}},",
				"  {{1}, {3}, {5}, {7}, {9}},",
				"  {{3,9}, {1}, {5}, {7}}."
			],
			"program": [
				"(PARI)",
				"tiles(S,t)={((i,b)-\u003e1 + sum(i=1, i, if(!bitnegimply(S[i],b), self()(i-1, b-S[i]))))(#S, t)}",
				"H(j,k)={sum(i=1, j, 1\u003c\u003c((2*k-1)*(2*i-1)))}",
				"a(n)={my(S=concat(vector(n, k, vector(n\\(2*k-1), j, H(1+j,k))))); tiles(S, H(n,1))} \\\\ _Andrew Howroyd_, Oct 02 2020"
			],
			"xref": [
				"Cf. A335631."
			],
			"keyword": "nonn,changed",
			"offset": "1,2",
			"author": "_Miloslav Znojil_, Sep 30 2020",
			"ext": [
				"Terms a(13) and beyond from _Andrew Howroyd_, Oct 02 2020"
			],
			"references": 1,
			"revision": 69,
			"time": "2022-01-11T13:21:18-05:00",
			"created": "2020-10-29T19:23:53-04:00"
		}
	]
}