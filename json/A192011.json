{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A192011",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 192011,
			"data": "-1,2,0,2,0,1,2,0,-1,0,2,0,-3,0,-1,2,0,-5,0,0,0,2,0,-7,0,3,0,1,2,0,-9,0,8,0,1,0,2,0,-11,0,15,0,-2,0,-1,2,0,-13,0,24,0,-10,0,-2,0,2,0,-15,0,35,0,-25,0,0,0,1,2,0,-17,0,48,0,-49,0,10,0,3,0,2,0,-19,0,63,0,-84,0,35,0,3,0,-1",
			"name": "Let P(0,x) = -1, P(1,x) = 2*x, and  P(n,x) = x*P(n-1,x) - P(n-2,x) for n \u003e 1. This sequence is the triangle of polynomial coefficients in order of decreasing exponents.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A192011/b192011.txt\"\u003eRows n = 0..30 of triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = T(n-1, k) - T(n-2, k-2), where T(0, 0) = -1, T(n, 0) = 2 and 0 \u003c= k \u003c= n, n \u003e= 0. - _G. C. Greubel_, May 19 2019"
			],
			"example": [
				"The first few rows are",
				"  -1;",
				"   2,   0;",
				"   2,   0,   1;",
				"   2,   0,  -1,   0;",
				"   2,   0,  -3,   0,  -1;",
				"   2,   0,  -5,   0,   0,   0;",
				"   2,   0,  -7,   0,   3,   0,   1;",
				"   2,   0,  -9,   0,   8,   0,   1,   0;",
				"   2,   0, -11,   0,  15,   0,  -2,   0,  -1;",
				"   2,   0, -13,   0,  24,   0, -10,   0,  -2,   0;",
				"   2,   0, -15,   0,  35,   0, -25,   0,   0,   0,   1;"
			],
			"maple": [
				"A192011 := proc(n,k)",
				"        option remember;",
				"        if k\u003en or k \u003c0 or n\u003c0 then",
				"                0;",
				"        elif n= 0 then",
				"                -1;",
				"        elif k=0 then",
				"                2;",
				"        else",
				"                procname(n-1,k)-procname(n-2,k-2) ;",
				"        end if;",
				"end proc: # _R. J. Mathar_, Nov 03 2011"
			],
			"mathematica": [
				"p[0, _] = -1; p[1, x_] := 2x; p[n_, x_] := p[n, x] = x*p[n-1, x] - p[n-2, x]; row[n_] := CoefficientList[p[n, x], x]; Table[row[n] // Reverse, {n, 0, 9}] // Flatten (* _Jean-François Alcover_, Nov 26 2012 *)",
				"T[n_,k_]:= If[k\u003c0 || k\u003en, 0, If[n==0 \u0026\u0026 k==0, -1, If[k==0, 2, T[n-1,k] - T[n-2, k-2]]]]; Table[T[n,k], {n,0,10}, {k,0,n}]//Flatten (* _G. C. Greubel_, May 19 2019 *)"
			],
			"program": [
				"(PARI) {T(n,k) = if(k\u003c0 || k\u003en, 0, if(n==0 \u0026\u0026 k==0, -1, if(k==0, 2, T(n-1,k) - T(n-2,k-2)))) };",
				"for(n=0, 10, for(k=0, n, print1(T(n,k), \", \"))) \\\\ _G. C. Greubel_, May 19 2019",
				"(Sage)",
				"def T(n,k):",
				"    if (k\u003c0 or k\u003en): return 0",
				"    elif (n==0 and k==0): return -1",
				"    elif (k==0): return 2",
				"    else: return T(n-1,k) - T(n-2, k-2)",
				"[[T(n,k) for k in (0..n)] for n in (0..10)] # _G. C. Greubel_, May 19 2019"
			],
			"xref": [
				"Left hand diagonals are: T(n,0) = [-1,2,2,2,2,2,...], T(n,2) = A165747(n), T(n,4) = A067998(n+1), T(n,6) = -A058373(n), T(n,8) = (-1)^(n+1) * A167387(n+2) see also A052472(n."
			],
			"keyword": "sign,easy,tabl",
			"offset": "0,2",
			"author": "_Paul Curtz_, Jun 21 2011",
			"references": 9,
			"revision": 35,
			"time": "2019-05-21T03:49:00-04:00",
			"created": "2011-07-13T18:15:30-04:00"
		}
	]
}