{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A057092",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 57092,
			"data": "1,9,90,891,8829,87480,866781,8588349,85096170,843160671,8354311569,82777250160,820184055561,8126651751489,80521522263450,797833566134451,7905195795581109,78327264255440040,776092140459190341,7689774642431673429,76192801046017773930",
			"name": "Scaled Chebyshev U-polynomials evaluated at i*3/2. Generalized Fibonacci sequence.",
			"comment": [
				"a(n) gives the length of the word obtained after n steps with the substitution rule 0-\u003e1^9, 1-\u003e(1^9)0, starting from 0. The number of 1's and 0's of this word is 9*a(n-1) and 9*a(n-2), resp.",
				"a(n) gives the number of n-digit integers which have no digit repeated 3 times in a row. Example: a(2)= 90 which is all the 2-digit integers. a(3) = 891 = all 900 3-digit integers except 111, 222, 333, ..., 999. - _Toby Gottfried_, Apr 01 2013",
				"a(n) is the number of n-digit integers which do not have two consecutive zeros. - _Ran Pan_, Jan 26 2016"
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A057092/b057092.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Martin Burtscher, Igor Szczyrba, Rafał Szczyrba, \u003ca href=\"http://www.emis.de/journals/JIS/VOL18/Szczyrba/sz3.html\"\u003eAnalytic Representations of the n-anacci Constants and Generalizations Thereof\u003c/a\u003e, Journal of Integer Sequences, Vol. 18 (2015), Article 15.4.5.",
				"A. F. Horadam, \u003ca href=\"http://www.fq.math.ca/Scanned/5-5/horadam.pdf\"\u003eSpecial properties of the sequence W_n(a,b; p,q)\u003c/a\u003e, Fib. Quart., 5.5 (1967), 424-434. Case n-\u003en+1, a=0,b=1; p=9, q=9.",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"W. Lang, \u003ca href=\"http://www.fq.math.ca/Scanned/38-5/lang.pdf\"\u003eOn polynomials related to powers of the generating function of Catalan's numbers\u003c/a\u003e, Fib. Quart. 38 (2000) 408-419. Eqs.(39) and (45),rhs, m=9.",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (9,9)."
			],
			"formula": [
				"a(n) = 9*(a(n-1) + a(n-2)), a(-1)=0, a(0)=1.",
				"a(n) = S(n, i*3)*(-i*3)^n with S(n, x) := U(n, x/2), Chebyshev's polynomials of the 2nd kind, A049310.",
				"G.f.: 1/(1-9*x-9*x^2).",
				"a(n) = Sum_{k, 0\u003c=k\u003c=n}8^k*A063967(n,k). - _Philippe Deléham_, Nov 03 2006",
				"a(n) = (1/39)*[(9/2)+(3/2)*sqrt(13)]^(n+1)*sqrt(13)-(1/39)*sqrt(13)*[(9/2)-(3/2)*sqrt(13)]^(n+1), with n\u003e=0. - _Paolo P. Lava_, Nov 20 2008"
			],
			"mathematica": [
				"Join[{a=0,b=1},Table[c=9*b+9*a;a=b;b=c,{n,100}]] (* _Vladimir Joseph Stephan Orlovsky_, Jan 17 2011 *)",
				"LinearRecurrence[{9,9}, {1,9}, 50] (* _G. C. Greubel_, Jan 25 2018 *)"
			],
			"program": [
				"(Sage) [lucas_number1(n,9,-9) for n in range(1, 20)] # _Zerinvary Lajos_, Apr 26 2009",
				"(PARI) Vec(1/(1-9*x-9*x^2) + O(x^30)) \\\\ _Colin Barker_, Jun 14 2015",
				"(MAGMA) I:=[1,9]; [n le 2 select I[n] else 9*Self(n-1) + 9*Self(n-2): n in [1..30]]; // _G. C. Greubel_, Jan 25 2018"
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Aug 11 2000",
			"references": 10,
			"revision": 45,
			"time": "2019-12-07T12:18:22-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}