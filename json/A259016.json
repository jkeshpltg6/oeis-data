{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A259016",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 259016,
			"data": "0,0,1,0,2,2,0,3,3,3,0,5,5,5,4,0,11,11,11,4,5,0,10,10,10,4,11,6,0,29,29,29,4,10,13,7,0,78,78,78,4,29,41,6,8,0,141,141,141,4,78,35,13,19,9,0,266,266,266,4,141,38,41,15,23,10,0,147,147,147,4,266,163,35,14,83,29,11",
			"name": "A(n,k) = k-th pi-based antiderivative of n; square array A(n,k), n\u003e=0, k\u003e=0, read by antidiagonals.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A259016/b259016.txt\"\u003eAntidiagonals n = 0..20, flattened\u003c/a\u003e"
			],
			"formula": [
				"A(n,k) = min { m \u003e= 0 : A258851^k(m) = n }.",
				"A258850(A(n,k),k) = n.",
				"A(n,k) \u003c= A000040^k(n) for n\u003e0."
			],
			"example": [
				"A(5,3) = 29 -\u003e 10 -\u003e 11 -\u003e 5.",
				"A(5,4) = 78 -\u003e 127 -\u003e 31 -\u003e 11 -\u003e 5.",
				"Square array A(n,k) begins:",
				"  0,  0,  0,   0,    0,     0,     0,      0,     0,      0, ...",
				"  1,  2,  3,   5,   11,    10,    29,     78,   141,    266, ...",
				"  2,  3,  5,  11,   10,    29,    78,    141,   266,    147, ...",
				"  3,  5, 11,  10,   29,    78,   141,    266,   147,    194, ...",
				"  4,  4,  4,   4,    4,     4,     4,      4,     4,      4, ...",
				"  5, 11, 10,  29,   78,   141,   266,    147,   194,   1181, ...",
				"  6, 13, 41,  35,   38,   163,   138,    253,   346,   1383, ...",
				"  7,  6, 13,  41,   35,    38,   163,    138,   253,    346, ...",
				"  8, 19, 15,  14,   43,   191,   201,    217,  1113,   1239, ...",
				"  9, 23, 83, 431, 3001, 27457, 10626, 112087, 87306, 172810, ..."
			],
			"maple": [
				"with(numtheory):",
				"d:= n-\u003e n*add(i[2]*pi(i[1])/i[1], i=ifactors(n)[2]):",
				"A:= proc() local t, A; t, A:= proc()-1 end, proc()-1 end;",
				"      proc(n, k) local h;",
				"        while A(n, k) = -1 do",
				"          t(k):= t(k)+1; h:= (d@@k)(t(k));",
				"          if A(h, k) = -1 then A(h, k):= t(k) fi",
				"        od; A(n, k)",
				"      end",
				"    end():",
				"seq(seq(A(n, h-n), n=0..h), h=0..12);"
			],
			"mathematica": [
				"d[n_] := If[n == 0, 0, n*Total[Last[#]*PrimePi[First[#]]/First[#]\u0026 /@ FactorInteger[n]]];",
				"A[n_, k_] := For[m = 0, True, m++, If[Nest[d, m, k] == n, Return[m]]];",
				"Table[A[n, k-n], {k, 0, 12}, {n, 0, k}] // Flatten (* _Jean-François Alcover_, Mar 20 2017 *)"
			],
			"xref": [
				"Columns k=0-3 give: A001477, A258861, A258862, A258995.",
				"Rows n=0,1,4,7,8,9 give: A000004, A258975, A010709, A259168, A259169, A259170.",
				"Cf. A000040, A000720, A258850, A258851."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Alois P. Heinz_, Jun 16 2015",
			"references": 9,
			"revision": 24,
			"time": "2018-10-27T16:40:11-04:00",
			"created": "2015-06-19T09:51:20-04:00"
		}
	]
}