{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A259884",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 259884,
			"data": "1,3,4,4,4,7,8,4,5,8,12,8,4,12,8,8,9,12,16,4,12,15,8,8,8,20,20,8,8,12,16,16,8,15,20,12,12,16,16,12,13,24,20,8,8,24,24,8,16,12,28,16,12,28,8,20,13,20,28,16,20,24,16,8,8,27,36,12,16,28,24,12",
			"name": "Expansion of phi(x) * f(-x^3)^3 / f(-x) in powers of x where phi(), f() are Ramanujan theta functions.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A259884/b259884.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of phi(x) * c(x) * (1/3) * x^(-1/3) in powers of x where phi() is a Ramanujan theta function (A000122) and c() is a cubic AGM theta function (A005882).",
				"Expansion of q^(-1/3) * eta(q^2)^5 * eta(q^3)^3 / (eta(q)^3 * eta(q^4)^2) in powers of q.",
				"Euler transform of period 12 sequence [3, -2, 0, 0, 3, -5, 3, 0, 0, -2, 3, -3, ...]."
			],
			"example": [
				"G.f. = 1 + 3*x + 4*x^2 + 4*x^3 + 4*x^4 + 7*x^5 + 8*x^6 + 4*x^7 + 5*x^8 + ...",
				"G.f. = q + 3*q^4 + 4*q^7 + 4*q^10 + 4*q^13 + 7*q^16 + 8*q^19 + 4*q^22 + ..."
			],
			"mathematica": [
				"eta[q_]:= q^(1/24)*QPochhammer[q]; a[n_] := SeriesCoefficient[q^(-1/3)* eta[q^2]^5*eta[q^3]^3/(eta[q]^3*eta[q^4]^2), {q, 0, n}]; Table[a[n], {n, 0, 50}] (* _G. C. Greubel_, Mar 16 2018 *)",
				"a[n_]:= SeriesCoefficient[EllipticTheta[3,0,q]*QPochhammer[q^3]^3 /QPochhammer[q], {q,0,n}]; Table[a[n], {n,0,50}] (* _G. C. Greubel_, Mar 18 2018 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A)^5 * eta(x^3 + A)^3 / (eta(x + A)^3 * eta(x^4 + A)^2), n))};",
				"(PARI) q='q+O('q^99); Vec(eta(q^2)^5*eta(q^3)^3/(eta(q)^3*eta(q^4)^2)) \\\\ _Altug Alkan_, Mar 18 2018"
			],
			"xref": [
				"Cubic AGM theta functions: a(q) (see A004016), b(q) (A005928), c(q) (A005882).",
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Michael Somos_, Jul 07 2015",
			"references": 1,
			"revision": 36,
			"time": "2021-03-12T22:24:48-05:00",
			"created": "2015-07-07T10:18:33-04:00"
		}
	]
}