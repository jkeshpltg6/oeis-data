{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A243700",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 243700,
			"data": "1,3,2,5,9,7,8,13,15,11,14,16,26,24,41,29,18,28,20,30,22,32,25,33,43,45,31,37,50,52,54,56,58,35,87,38,55,67,40,60,72,44,63,77,79,47,70,49,121,88,53,129,94,96,98,100,59,89,105,107,62,158,113,65,102,68,103,189",
			"name": "The lexicographically earliest sequence of distinct terms with a(1) = 1 such that a(n) divides the sum of the first a(n) terms.",
			"comment": [
				"Once there is a k such that k \u003e n and a(k) \u003e n, n can no longer appear in the sequence, otherwise a(k) would be n. - _Franklin T. Adams-Watters_, Jun 11 2014",
				"If the sum a(1) + a(2) + ... + a(m) is not divisible by m, then m does not belong to this sequence. Sequence A019444 gives a variant of this sequence, where every positive integer is a term. - _Max Alekseyev_, Jun 11 2014",
				"Positive integers that do not appear in this sequence form A243864.",
				"Is there any index n \u003e 3 such that a(n) \u003c= n ? - _Max Alekseyev_, Jun 13 2014"
			],
			"reference": [
				"Eric Angelini, Posting to the Sequence Fans Mailing List, Jun 11 2014"
			],
			"link": [
				"Max Alekseyev, \u003ca href=\"/A243700/b243700.txt\"\u003eTable of n, a(n) for n = 1..100000\u003c/a\u003e (first 1100 terms from Jean-Marc Falcoz)"
			],
			"example": [
				"1 divides the sum of the first 1 term  (yes:  1/1=1)",
				"3 divides the sum of the first 3 terms (yes:  6/3=2)",
				"2 divides the sum of the first 2 terms (yes:  4/2=2)",
				"5 divides the sum of the first 5 terms (yes: 20/5=4)",
				"9 divides the sum of the first 9 terms (yes: 63/9=7)",
				"7 divides the sum of the first 7 terms (yes: 35/7=5)",
				"8 divides the sum of the first 8 terms (yes: 48/8=6)",
				"..."
			],
			"program": [
				"(PARI) { printA243700() = my( S=Set(), T=[], s=0, m=1, k); for(n=1,10^5, k=m; while( ((k==n || setsearch(S,n)) \u0026\u0026 Mod(s+k,n)) || if(k\u003cn,sum(i=1,k,T[i])%k) || setsearch(S,k), k++); S=setunion(S,[k]); T=concat(T,[k]);  s+=k; if(s%n,S=setunion(S,[n]);); while(setsearch(S,m),m++); print1(k,\", \"); ) } /* _Max Alekseyev_, Jun 13 2014 */"
			],
			"xref": [
				"Cf. A019444, A243864 (complement), A244010 (partial sums), A244011 (the quotients), A244016 (sorted), A244017, A244018."
			],
			"keyword": "nonn,nice",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, Jun 12 2014",
			"ext": [
				"First 1100 terms were computed by _Jean-Marc Falcoz_."
			],
			"references": 9,
			"revision": 40,
			"time": "2014-09-01T16:51:40-04:00",
			"created": "2014-06-12T17:04:43-04:00"
		}
	]
}