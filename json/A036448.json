{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A036448",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 36448,
			"data": "2,1,3,11,17,111,117,317,1317,3317,11317,17317,111317,117317,317317,1317317,3317317,11317317,17317317,111317317,117317317,317317317,1317317317,3317317317,11317317317,17317317317,111317317317,117317317317,317317317317,1317317317317,3317317317317",
			"name": "Smallest positive number containing n e's when spelled out in US English.",
			"comment": [
				"From _Michael S. Branicky_, Oct 24 2020: (Start)",
				"\"US English\" connotes that no \"and\" is used (\"one hundred one\") and, importantly here, that the names of large numbers follow the \"American system\" (Weisstein link), also known as the short scale (Wikipedia link). The previous a(8) and a(9) were based on \"eleven hundred and seventeen\" and \"seventeen hundred and seventeen\", which are less common written forms (Wikipedia English numbers link). To make the sequence precise, the common written form is adopted (\"one thousand one hundred seventeen\"; Wilson link; A000052 Example). Thus, a(n) is the least m such that A085513(m)=n.",
				"The sequence follows the pattern of 1(317)^n, 3(317)^n, 11(317)^n, 17(317)^n, 111(317)^n, 117(317)^n, 317(317)^n for n = 0 through 7 and whenever the largest named power has no \"e\". a(50) \u003e 10^21 = \"one sextillion\" which is the first power name that has an \"e\", breaking the pattern. In that case, a(50) = 1117(317)^6 and a(51) = 1(317)^7. Whenever the largest power has 1 \"e\" it follows this pattern. If it has m\u003e1 \"e\"'s, the first block of three is shifted lower to a(7-m). See Wikipedia link for Names of large numbers for power names.",
				"(End)"
			],
			"reference": [
				"Rodolfo Marcelo Kurchan, Problem 1882, Another Number Sequence, Journal of Recreational Mathematics, vol. 23, number 2, p. 141."
			],
			"link": [
				"Eric Weisstein's World of Mathematics, \u003ca href=\"https://mathworld.wolfram.com/LargeNumber.html\"\u003eLarge Number\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/English_numerals\"\u003eEnglish numerals\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Names_of_large_numbers\"\u003eNames of Large Numbers\u003c/a\u003e",
				"Robert G. Wilson v, \u003ca href=\"https://oeis.org/A001477/a001477.txt\"\u003eAmerican English names for the numbers from 0 to 100999 without spaces or hyphens\u003c/a\u003e"
			],
			"example": [
				"One has 1 e.",
				"Three has 2 e's."
			],
			"program": [
				"(Python)",
				"from num2words import num2words",
				"def A036448(n):",
				"    i = 1",
				"    while num2words(i).count(\"e\")!=n:",
				"        i += 1",
				"    return i",
				"print([A036448(n) for n in range(1,12)]) # _Michael S. Branicky_, Oct 23 2020"
			],
			"xref": [
				"Cf. A000027, A000052, A001477, A005589, A006933, A037196, A085513."
			],
			"keyword": "nonn,word",
			"offset": "0,1",
			"author": "_Rodolfo Kurchan_",
			"ext": [
				"a(8)-a(9) changed and a(11)-a(30) added by _Michael S. Branicky_, Oct 23 2020",
				"a(0)=2 inserted by _Sean A. Irvine_, Nov 02 2020"
			],
			"references": 1,
			"revision": 38,
			"time": "2020-12-06T12:37:16-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}