{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A319171",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 319171,
			"data": "1,1,1,2,1,1,5,2,1,1,14,5,2,1,1,51,15,5,2,1,1,267,67,15,5,2,1,1,2328,504,77,15,5,2,1,1,56092,9310,684,83,15,5,2,1,1,10494213,1396077,34297,860,87,15,5,2,1,1,49487365422",
			"name": "Square array, read by antidiagonals, upwards: T(n,k) is the number of groups of order prime(k+1)^n.",
			"comment": [
				"In 1960, Higman conjectured that the function f(n,p) giving the number of groups of prime-power order p^n, for fixed n and varying p, is a \"Polynomial in Residue Classes\" (PORC), i.e., there exist an integer M and polynomials q_i(x) in Z[x] (i = 1, 2, ..., M) such that if p = i mod M, then f(n,p) = q_i(p). The conjecture is confirmed for n \u003c= 7."
			],
			"link": [
				"H. U. Besche, B. Eick and E. A. O'Brien. \u003ca href=\"http://dx.doi.org/10.1142/S0218196702001115\"\u003eA Millennium Project: Constructing Small Groups\u003c/a\u003e, Internat. J. Algebra and Computation, 12 (2002), 623-644.",
				"Heiko Dietrich, \u003ca href=\"http://users.monash.edu/~heikod/icts2016/CPGmain.pdf\"\u003eComputational aspects of finite p-groups\u003c/a\u003e",
				"Groupprops, \u003ca href=\"https://groupprops.subwiki.org/wiki/Groups_of_prime_power_order\"\u003eGroups of prime power order\u003c/a\u003e",
				"Groupprops, \u003ca href=\"https://groupprops.subwiki.org/wiki/Higman%27s_PORC_conjecture\"\u003eHigman's PORC conjecture\u003c/a\u003e",
				"Groupprops, \u003ca href=\"https://groupprops.subwiki.org/wiki/PORC_function\"\u003ePORC function\u003c/a\u003e",
				"Graham Higman, \u003ca href=\"https://doi.org/10.1112/plms/s3-10.1.24\"\u003eEnumerating p-Groups. I: Inequalities\u003c/a\u003e, Proc. London Math. Soc. Vol. 10 (1960), 24-30.",
				"Graham Higman, \u003ca href=\"https://doi.org/10.1112/plms/s3-10.1.566\"\u003eEnumerating p-Groups. II: Problem whose solution is PORC\u003c/a\u003e, Proc. London Math. Soc. Vol. 10 (1960), 566-582.",
				"Eamonn O'Brien, \u003ca href=\"https://www.icts.res.in/sites/default/files/1284624569notes.pdf\"\u003ePolycyclic groups\u003c/a\u003e",
				"Gordon Royle, \u003ca href=\"http://staffhome.ecm.uwa.edu.au/~00013890/remote/cubcay/\"\u003eNumbers of Small Groups\u003c/a\u003e",
				"Michael Vaughan-Lee, \u003ca href=\"https://dx.doi.org/10.1365/s13291-012-0039-x\"\u003eGraham Higman’s PORC Conjecture\u003c/a\u003e, Jahresbericht der Deutschen Mathematiker-Vereinigung Vol. 114 (2012), 89-16.",
				"Michael Vaughan-Lee, \u003ca href=\"http://dx.doi.org/10.22108/ijgt.2015.5758\"\u003eGroups of order p^8 and exponent p\u003c/a\u003e, International Journal of Group Theory Vol. 4 (2015), 25-42.",
				"Brett E. Witty, \u003ca href=\"https://www.brettwitty.net/pages/phd.html\"\u003eEnumeration of groups of prime-power order\u003c/a\u003e, PhD thesis, 2006.",
				"\u003ca href=\"/index/Gre#groups\"\u003eIndex entries for sequences related to groups\u003c/a\u003e"
			],
			"formula": [
				"T(n,0) = A000679(n).",
				"T(n,1) = A090091(n).",
				"T(n,2) = A090130(n).",
				"T(n,3) = A090140(n).",
				"T(0,n) = 1, T(1,n) = 1, T(2,n) = 2 and T(3,n) = 5.",
				"T(4,0) = 14 and T(4,n) = 15, n \u003e 0.",
				"T(5,n) = A232105(n+1).",
				"T(6,n) = A232106(n+1).",
				"T(7,n) = A232107(n+1)."
			],
			"example": [
				"Array begins:",
				"  (p = 2) (p = 3) (p = 5) (p = 7) (p = 11) (p = 13) ...",
				"       1       1       1       1        1        1  ...",
				"       1       1       1       1        1        1  ...",
				"       2       2       2       2        2        2  ...",
				"       5       5       5       5        5        5  ...",
				"      14      15      15      15       15       15  ...",
				"      51      67      77      83       87       97  ...",
				"     267     504     684     860     1192     1476  ...",
				"    2328    9310   34297  113147   750735  1600573  ...",
				"     ..."
			],
			"maple": [
				"with(GroupTheory): T:=proc(n,k) NumGroups(ithprime(k+1)^n); end proc: seq(seq(T(n-k,k),k=0..n),n=0..10); # _Muniru A Asiru_, Oct 03 2018"
			],
			"mathematica": [
				"(* This program uses Higman's PORC functions to compute the rows 0 to 7 *)",
				"f[0, p_] := 1; f[1, p_] := 1; f[2, p_] := 2; f[3, p_] := 5;",
				"f[4, p_] := If[p == 2, 14, 15];",
				"f[5, p_] := If[p == 2, 51, If[p == 3, 67, 61 + 2*p + 2*GCD[p - 1, 3] + GCD[p - 1, 4]]];",
				"f[6, p_] := If[p == 2, 267, If[p == 3, 504, 3*p^2 + 39*p + 344 + 24*GCD[p - 1, 3] + 11*GCD[p - 1, 4] + 2*GCD[p - 1, 5]]];",
				"f[7, p_] := If[p == 2, 2328, If[p == 3, 9310, If[p == 5, 34297, 3*p^5 + 12*p^4 + 44*p^3 + 170*p^2 + 707*p + 2455 + (4*p^2 + 44*p + 291)*GCD[p - 1, 3] + (p^2 + 19*p + 135)*GCD[p - 1, 4] + (3*p + 31)*GCD[p - 1, 5] + 4*GCD[p - 1, 7] + 5*GCD[p - 1, 8] + GCD[p - 1, 9]]]];",
				"tabl[kk_] := TableForm[Table[f[n, Prime[k+1]], {n, 0, 7}, {k, 0, kk}]];"
			],
			"program": [
				"(GAP) # This program computes the first 45 terms, rows 0..8.",
				"P:=Filtered([1..300],IsPrime);;",
				"T1:=List([0..7],n-\u003eList([0..15],k-\u003eNumberSmallGroups(P[k+1]^n)));;",
				"T2:=[Flat(Concatenation(List([8],n-\u003eList([0],k-\u003eNumberSmallGroups(P[k+1]^n))),List([1..14],i-\u003e0)))];;",
				"T:=Concatenation(T1,T2);;",
				"b:=List([2..10],n-\u003eOrderedPartitions(n,2));;",
				"a:=Flat(List([1..Length(b)],i-\u003eList([1..Length(b[i])],j-\u003eT[b[i][j][2]][b[i][j][1]]))); # _Muniru A Asiru_, Oct 01 2018"
			],
			"xref": [
				"Inspired by A158106.",
				"Cf. A000001, A000679, A090091, A090130, A090140, A128604, A232105, A232106, A232107."
			],
			"keyword": "tabl,nonn,hard,more",
			"offset": "0,4",
			"author": "_Franck Maminirina Ramaharo_, Sep 12 2018",
			"references": 0,
			"revision": 34,
			"time": "2018-10-06T11:43:49-04:00",
			"created": "2018-10-06T05:19:43-04:00"
		}
	]
}