{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A285326",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 285326,
			"data": "0,2,4,4,8,6,8,8,16,10,12,12,16,14,16,16,32,18,20,20,24,22,24,24,32,26,28,28,32,30,32,32,64,34,36,36,40,38,40,40,48,42,44,44,48,46,48,48,64,50,52,52,56,54,56,56,64,58,60,60,64,62,64,64,128,66,68,68,72,70,72,72,80,74,76,76,80,78,80,80,96,82,84,84,88,86,88,88,96,90,92,92",
			"name": "a(0) = 0, for n \u003e 0, a(n) = n + A006519(n).",
			"comment": [
				"From _M. F. Hasler_, Oct 19 2019: (Start)",
				"This sequence is equal to itself multiplied by 2 and interleaved with the positive even numbers: We have a(2n-1) = 2n (n \u003e= 1) from the very definition, since A006519(m) = 1 for odd m. And a(2n) = 2n + A006519(2n) = 2*a(n), using A006519(2n) = 2*A006519(n).",
				"The sequence repeats the pattern [A, B, C, C] where in the n-th occurrence C = 4n, B = C - 2, A = C if n is even, A = C + 4 if n = 3 (mod 4), and A = 16*a((n-1)/4) otherwise. (End)"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A285326/b285326.txt\"\u003eTable of n, a(n) for n = 0..8191\u003c/a\u003e",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e"
			],
			"formula": [
				"a(0) = 0; for n \u003e 0, a(n) = n + A006519(n).",
				"a(n) = A048675(A065642(A019565(n))) = A048675(A285109(A019565(n))).",
				"For n \u003e= 1, a(n) = 2*A109168(n).",
				"a(n) = 2*A140472(n) and a(2n) = 2*a(n) and a(2^n) = 2^(n+1) for all n \u003e= 0, a(2n-1) = 2n for all n \u003e= 1. - _M. F. Hasler_, Oct 19 2019"
			],
			"mathematica": [
				"Table[If[n\u003e0, n + GCD[2^n, n], 0], {n, 0, 100}] (* _Indranil Ghosh_, Apr 20 2017 *)"
			],
			"program": [
				"(Scheme)",
				"(define (A285326 n) (if (zero? n) n (+ n (A006519 n))))",
				"(define (A285326 n) (A048675 (A065642 (A019565 n))))",
				"(Python)",
				"from sympy import gcd",
				"def a(n): return n + gcd(2**n, n) if n\u003e0 else 0 # _Indranil Ghosh_, Apr 20 2017",
				"(PARI) a(n) = if(n\u003e0, n + gcd(2^n, n), 0); \\\\ _Indranil Ghosh_, Apr 20 2017",
				"(PARI) A285326(n)=n+bitand(n,-n) \\\\ Or: {a(n)=-bitand(-n,bitneg(n))}, not faster. - _M. F. Hasler_, Oct 19 2019"
			],
			"xref": [
				"Row 2 of A285325 (after the initial zero).",
				"Cf. A006519, A019565, A048675, A065642, A129760, A285109.",
				"Cf. A109168 (same terms divided by 2), also A140472."
			],
			"keyword": "nonn,base",
			"offset": "0,2",
			"author": "_Antti Karttunen_, Apr 19 2017",
			"references": 4,
			"revision": 27,
			"time": "2019-10-21T12:11:59-04:00",
			"created": "2017-04-20T12:31:32-04:00"
		}
	]
}