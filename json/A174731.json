{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A174731",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 174731,
			"data": "1,1,1,1,-13,1,1,-74,-74,1,1,-278,-588,-278,1,1,-881,-3086,-3086,-881,1,1,-2539,-13207,-22097,-13207,-2539,1,1,-6884,-49724,-124694,-124694,-49724,-6884,1,1,-17884,-171184,-600424,-900892,-600424,-171184,-17884,1",
			"name": "Triangle T(n, k, q) = (1-q^n)*(1/k)*binomial(n-1, k-1)*binomial(n, k-1) - (1-q^n) + 1, for q = 2, read by rows.",
			"comment": [
				"From _G. C. Greubel_, Feb 09 2021: (Start)",
				"The triangle coefficients are connected to the Narayana numbers by T(n, k, q) = (1-q^n)*(A001263(n, k) - 1) + 1, for varying q values.",
				"The row sums of this class of sequences, for varying q, is given by Sum_{k=1..n} T(n, k, q) = q^n * n + (1 - q^n)*C_{n}, where C_{n} are the Catalan numbers (A000108). (End)"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A174731/b174731.txt\"\u003eRows n = 1..100 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k, q) = (1-q^n)*(1/k)*binomial(n-1, k-1)*binomial(n, k-1) - (1-q^n) + 1, for q = 2.",
				"From _G. C. Greubel_, Feb 09 2021: (Start)",
				"T(n, k, 2) = (1-2^n)*(A001263(n,k) - 1) + 1.",
				"Sum_{k=1..n} T(n, k, 2) = 2^n * n + (1 - 2^n)*A000108(n). (End)"
			],
			"example": [
				"Triangle begins as:",
				"  1;",
				"  1,      1;",
				"  1,    -13,       1;",
				"  1,    -74,     -74,        1;",
				"  1,   -278,    -588,     -278,        1;",
				"  1,   -881,   -3086,    -3086,     -881,        1;",
				"  1,  -2539,  -13207,   -22097,   -13207,    -2539,        1;",
				"  1,  -6884,  -49724,  -124694,  -124694,   -49724,    -6884,       1;",
				"  1, -17884, -171184,  -600424,  -900892,  -600424,  -171184,  -17884,      1;",
				"  1, -45011, -551396, -2576936, -5412692, -5412692, -2576936, -551396, -45011, 1;"
			],
			"mathematica": [
				"T[n_, k_, q_]:= 1 + (1-q^n)*(1/k)*(Binomial[n-1, k-1]*Binomial[n, k-1] - k);",
				"Table[T[n, k, 2], {n, 12}, {k, n}]//Flatten"
			],
			"program": [
				"(Sage)",
				"def T(n,k,q): return 1 +(1-q^n)*(1/k)*(binomial(n-1, k-1)*binomial(n, k-1) -k)",
				"flatten([[T(n,k,2) for k in (1..n)] for n in (1..12)]) # _G. C. Greubel_, Feb 09 2021",
				"(Magma)",
				"T:= func\u003c n,k,q | 1 +(1-q^n)*(1/k)*(Binomial(n-1, k-1)*Binomial(n, k-1) -k) \u003e;",
				"[T(n,k,2): k in [1..n], n in [1..12]]; // _G. C. Greubel_, Feb 09 2021"
			],
			"xref": [
				"Cf. A000108, A001263.",
				"Cf. A000012 (q=1), this sequence (q=2), A174732 (q=3), A174733 (q=4)."
			],
			"keyword": "sign,tabl",
			"offset": "1,5",
			"author": "_Roger L. Bagula_, Mar 28 2010",
			"ext": [
				"Edited by _G. C. Greubel_, Feb 09 2021"
			],
			"references": 3,
			"revision": 10,
			"time": "2021-02-09T21:40:14-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}