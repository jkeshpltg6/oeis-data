{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A192814",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 192814,
			"data": "1,1,1,9,49,225,1041,4873,22817,106753,499425,2336585,10931921,51145825,239289457,1119533257,5237818689,24505519873,114650876097,536402551689,2509598769265,11741342323937,54932733173713,257006830281609",
			"name": "Constant term in the reduction of the polynomial (2*x+1)^n by x^3 -\u003e x^2 + x + 1.  See Comments.",
			"comment": [
				"For discussions of polynomial reduction, see A192232 and A192744."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A192814/b192814.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (5,-3,7)."
			],
			"formula": [
				"a(n) = 5*a(n-1) - 3*a(n-2) + 7*a(n-3).",
				"G.f.: (1 -4*x -x^2) / (1 -5*x +3*x^2 -7*x^3). - _R. J. Mathar_, May 06 2014"
			],
			"maple": [
				"seq(coeff(series((1-4*x-x^2)/(1-5*x+3*x^2-7*x^3),x,n+1), x, n), n = 0 .. 25); # _Muniru A Asiru_, Jan 03 2019"
			],
			"mathematica": [
				"q = x^3; s = x^2 + x + 1; z = 40;",
				"p[n_, x_] := (2 x + 1)^n;",
				"Table[Expand[p[n, x]], {n, 0, 7}]",
				"reduce[{p1_, q_, s_, x_}] :=",
				"FixedPoint[(s PolynomialQuotient @@ #1 +",
				"       PolynomialRemainder @@ #1 \u0026)[{#1, q, x}] \u0026, p1]",
				"t = Table[reduce[{p[n, x], q, s, x}], {n, 0, z}];",
				"u1 = Table[Coefficient[Part[t, n], x, 0], {n, 1, z}] (* A192814 *)",
				"u2 = Table[Coefficient[Part[t, n], x, 1], {n, 1, z}] (* A192815 *)",
				"u2 = u2/2  (* A192816 *)",
				"LinearRecurrence[{5,-3,7}, {1,1,1}, 30] (* _G. C. Greubel_, Jan 03 2019 *)"
			],
			"program": [
				"(PARI) my(x='x+O('x^30)); Vec((1-4*x-x^2)/(1-5*x+3*x^2-7*x^3)) \\\\ _G. C. Greubel_, Jan 03 2019",
				"(MAGMA) m:=30; R\u003cx\u003e:=PowerSeriesRing(Integers(), m); Coefficients(R!( (1-4*x-x^2)/(1-5*x+3*x^2-7*x^3) )); // _G. C. Greubel_, Jan 03 2019",
				"(Sage) ((1-4*x-x^2)/(1-5*x+3*x^2-7*x^3)).series(x, 30).coefficients(x, sparse=False) # _G. C. Greubel_, Jan 03 2019",
				"(GAP) a:=[1,1,1];; for n in [4..25] do a[n]:=5*a[n-1]-3*a[n-2]+7*a[n-3]; od; Print(a); # _Muniru A Asiru_, Jan 03 2019"
			],
			"xref": [
				"Cf. A192744, A192232, A192815."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_Clark Kimberling_, Jul 10 2011",
			"references": 3,
			"revision": 14,
			"time": "2019-01-03T03:44:36-05:00",
			"created": "2011-07-11T11:11:05-04:00"
		}
	]
}