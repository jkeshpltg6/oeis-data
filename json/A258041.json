{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A258041",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 258041,
			"data": "1,0,1,1,4,10,31,94,303,986,3284,11099,38024,131694,460607,1624451,5771532,20640334,74246701,268478962,975436348,3559204700,13037907692,47931423574,176792821643,654078238224,2426705590840,9026907769955",
			"name": "Number of 312-avoiding derangements of {1,2,...,n}.",
			"comment": [
				"In the Mathematica recurrence below, a(n,k) is the number of 312-avoiding permutations of {1,2,...,n} with no entry moved k places to the right of its \"natural\" position; thus a(n,0) = a(n). The recurrence for a(n,k) counts these permutations by the position j of 1 in the permutation.",
				"Numerical evidence suggests lim_{n-\u003einf} a(n)/a(n-1) = 4 and lim_{n-\u003einf} A000108(n)/(n*a(n)) ~ .105."
			],
			"link": [
				"Vaclav Kotesovec, \u003ca href=\"/A258041/b258041.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Aaron Robertson, Dan Saracino, and Doron Zeilberger, \u003ca href=\"http://arxiv.org/abs/math/0203033\"\u003eRefined Restricted Permutations\u003c/a\u003e, arXiv:math/0203033 [math.CO], 2002.",
				"Aaron Robertson, Dan Saracino, and Doron Zeilberger, \u003ca href=\"http://www.math.rutgers.edu/~zeilberg/mamarim/mamarimPDF/DWilf.pdf\"\u003eRefined Restricted Permutations\u003c/a\u003e, Annals of Combinatorics 6 (2002) 427-444."
			],
			"formula": [
				"G.f.: 1/(1 + C(0)x -",
				"         x/(1 + C(1)x^2 -",
				"             x/(1 + C(2)x^3 -",
				"                x/(1 + C(3)x^4 -",
				"                   x/(1 + C(4)x^5 -",
				"                      x/(1 + C(5)x^6 - ...)))))))) [continued fraction] where C(n) = A000108(n) is the n-th Catalan number."
			],
			"example": [
				"a(4) = 4 counts 2143, 2341, 3421, 4321."
			],
			"mathematica": [
				"a[n_, k_] /; k \u003e= n := CatalanNumber[n]",
				"a[n_, k_] /; 0 \u003c= k \u003c n :=",
				"a[n, k] = Sum[a[j - 1, k + 1] a[n - j, k]  , {j, k}] + Sum[a[j - 1, k + 1] a[n - j, k],{j, k + 2, n}]",
				"a[n_] := a[n, 0]",
				"Table[a[n], {n, 0, 30}]"
			],
			"xref": [
				"Cf. A000108, A000166, A061547."
			],
			"keyword": "nonn",
			"offset": "0,5",
			"author": "_David Callan_, May 17 2015",
			"references": 1,
			"revision": 11,
			"time": "2015-05-18T14:37:17-04:00",
			"created": "2015-05-17T03:30:50-04:00"
		}
	]
}