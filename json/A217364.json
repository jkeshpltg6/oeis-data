{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A217364",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 217364,
			"data": "1,2,20,280,4560,80960,1520064,29680640,596593920,12262581760,256556410880,5445566730240,116974976102400,2538140268625920,55548588652625920,1224777962595287040,27180522769369333760,606648758810237337600",
			"name": "a(n) = 2^n*binomial(5*n, n)/(4*n+1).",
			"comment": [
				"Old name was: Series reversion of x - 2*x^5.",
				"Regular blocks of 3 intermediate zeros have been left out of the sequence: If y = x - 2x^5, then x = y + 2*y^5 + 20*y^9 + 280*y^13 + 4560*y^17 - ...",
				"a(n) is the number of lattice paths (Schroeder paths) from (0,0) to (n,5n) with unit steps N=(0,1), E=(1,0) and D=(1,1) staying weakly above the line y = 5x with the total number of occurrences of NE and D equal to n. - _Michael D. Weiner_, Aug 21 2019"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A217364/b217364.txt\"\u003eTable of n, a(n) for n = 0..724\u003c/a\u003e (terms n=1..76 from Robert Israel)",
				"D. Birmajer, J. B. Gil, J. D. Gil and M. D. Weiner, \u003ca href=\"http://arxiv.org/abs/1908.08103\"\u003eSchröder Coloring and Applications\u003c/a\u003e, arXiv:1908.08103 [math.CO], 2019.",
				"R. J. Mathar, \u003ca href=\"http://arxiv.org/abs/1211.3963\"\u003eSeries Expansion of Generalized Fresnel Integrals\u003c/a\u003e, arXiv:1211.3963 [math.CA], 2012."
			],
			"formula": [
				"Apparently (4*n+1)*(4*n)*(4*n-1)*(4*n-2)*a(n) - 10*(5*n-4)*(5*n-3)*(5*n-2)*(5*n-1)*a(n-1) = 0.",
				"a(n) ~ 5^(5*n)/(2^(7*n+4)*sqrt(Pi/10)*n^(3/2)). - _Vaclav Kotesovec_, Aug 20 2013",
				"a(n) = Gamma(n+1/5)*Gamma(n+2/5)*Gamma(n+3/5)*Gamma(n+4/5)*6250^(n+1)*sqrt(5)/ (25000*Pi^2*Gamma(4*n+2)). - _Robert Israel_, May 19 2014",
				"a(n) = 5*2^n*Gamma(5*n)/(Gamma(n)*Gamma(4*n+2)) for n \u003e 0. - _Peter Luschny_, May 19 2014",
				"G.f.: F([1/5, 2/5, 3/5, 4/5], [1/2, 3/4, 5/4], 3125*x/128), where F is the generalized hypergeometric function. - _Stefano Spezia_, Sep 03 2019",
				"G.f. A(x) satisfies: A(x) = 1 / (1 - 2 * x * A(x)^4). - _Ilya Gutkovskiy_, Nov 12 2021"
			],
			"maple": [
				"A217364 := j -\u003e pochhammer(1/5, j)*pochhammer(2/5, j)*pochhammer(3/5, j)*pochhammer(4/5, j)*6250^j/(4*j+1)!:",
				"seq(A217364(n),n=0..100); # _Robert Israel_, May 19 2014"
			],
			"mathematica": [
				"Table[CoefficientList[InverseSeries[Series[x-2*x^5,{x,0,100}],x],x][[4*n-2]],{n,1,20}] (* _Vaclav Kotesovec_,Aug 20 2013 *)",
				"Table[2^n Binomial[5 n, n] / (4 n + 1), {n, 0, 20}] (* _Michael D. Weiner_, Aug 21 2019 *)"
			],
			"program": [
				"(Sage)",
				"A217364 = lambda n: 5*2^n*gamma(5*n)/(gamma(n)*gamma(4*n+2)) if n \u003e 0 else 1",
				"[A217364(n) for n in (0..18)] # _Peter Luschny_, May 19 2014",
				"(PARI) for(n=0,20, print1(round(if(n==0, 1, 5*2^n*gamma(5*n)/(gamma(n)*gamma(4*n+2)))), \", \")) \\\\ _G. C. Greubel_, Apr 01 2017",
				"(PARI) {my(x='x+O('x^133), v=Vec(serreverse(x - 2*x^5))); vector(#v\\4, n, v[4*n-3] ) } \\\\ _Joerg Arndt_, Apr 02 2017",
				"(MAGMA) [2^n*Binomial(5*n, n)/(4*n+1): n in [0..25]]; // _Michael D. Weiner_, Aug 21 2019"
			],
			"xref": [
				"Cf. A153231 (x-2*x^3), A217360 (x-2*x^4)."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_R. J. Mathar_, Oct 01 2012",
			"ext": [
				"New definition and offset from _Michael D. Weiner_, Sep 03 2019"
			],
			"references": 1,
			"revision": 59,
			"time": "2021-11-12T10:12:25-05:00",
			"created": "2012-10-01T18:13:53-04:00"
		}
	]
}