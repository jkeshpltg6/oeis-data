{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A194923",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 194923,
			"data": "0,1,2,0,1,0,2,1,0,1,2,2,0,2,1,0,1,0,0,1,2,0,2,0,0,2,1,1,0,1,1,0,2,1,2,0,1,2,1,2,0,1,2,0,2,2,1,0,2,1,2,0,1,0,2,0,1,2,0,0,1,2,1,0,2,0,1,0,2,1,0,0,2,1,2,1,0,1,2,1,0,2,0,1,0,2,1,1,2,0,1,1,2,0,2,1,2,1,0,2,0,1,0,2,0",
			"name": "The (finite) list of ternary abelian squarefree words.",
			"comment": [
				"Lexicographically ordered list of words of increasing length L=1,2,3,... over the alphabet {0,1,2}, excluding those which contain two adjacent subsequences with the same multiset of symbols regardless of internal order. E.g., 0,0 or 1,1 or 2,2 or 0,1,0,1 or 0,1,2,1,0,2, etc.",
				"Peter Lawrence, Sep 06 2011: In other words, this is the sequence of all possible lists over the letters \"0\", \"1\", \"2\", such that within a list no two adjacent segments of any length contain the same multiset of symbols, first sorted by length of list, second lists of same length are sorted lexicographically. Recursively, to each list of length N create up to two lists of length N+1 by appending the two letters that are different from the last letter of the first list, and then check for and eliminate longer abelian squares; keeping all the lists sorted as in the previous description.",
				"The number of sequences of the successive lengths are 3, 6, 12, 18, 30, 30, 18, for total row lengths of 3, 12, 36, 72,150, 180, 126."
			],
			"link": [
				"Franklin T. Adams-Watters, \u003ca href=\"/A194923/b194923.txt\"\u003eTable of n, a(n) for n = 1..579 (Complete list)\u003c/a\u003e",
				"V. Keränen, \u003ca href=\"http://south.rotol.ramk.fi/keranen/ias2002/NewAbelianSquare-FreeDT0L-LanguagesOver4Letters.nb\"\u003eNew Abelian Square-Free DT0L-Languages over 4 Letters\u003c/a\u003e",
				"E. Weisstein, from MathWorld: \u003ca href=\"http://mathworld.wolfram.com/SquarefreeWord.html\"\u003eSquarefree Word\u003c/a\u003e"
			],
			"example": [
				"Starting with words of length 1, the allowed ones are:",
				"{{0}, {1}, {2}};",
				"{{0, 1}, {0, 2}, {1, 0}, {1, 2}, {2, 0}, {2, 1}};",
				"{{0, 1, 0}, {0, 1, 2}, {0, 2, 0}, {0, 2, 1}, {1, 0, 1}, {1, 0, 2}, {1, 2, 0}, {1, 2, 1}, {2, 0, 1}, {2, 0, 2}, {2, 1, 0}, {2, 1, 2}};",
				"{{0, 1,0, 2}, {0, 1, 2, 0}, {0, 1, 2, 1}, {0, 2, 0, 1}, {0, 2, 1, 0}, {0, 2, 1, 2}, {1, 0, 1, 2}, {1, 0, 2, 0}, {1, 0, 2, 1}, {1, 2, 0, 1}, {1, 2, 0, 2}, {1, 2, 1, 0}, {2, 0, 1, 0}, {2, 0, 1, 2}, {2, 0, 2, 1}, {2, 1, 0, 1}, {2, 1, 0, 2}, {2, 1, 2, 0}},",
				"{{0, 1, 0, 2, 0}, {0, 1, 0, 2, 1}, {0, 1, 2, 0, 1}, {0, 1, 2, 0, 2}, {0, 1, 2, 1, 0}, {0, 2, 0,1, 0}, {0, 2, 0, 1, 2}, {0, 2, 1, 0, 1}, {0, 2, 1, 0,2}, {0, 2, 1, 2, 0}, {1, 0,1, 2, 0}, {1, 0, 1, 2, 1}, {1, 0, 2, 0, 1}, {1, 0, 2, 1, 0}, {1, 0, 2, 1, 2}, {1, 2, 0, 1, 0}, {1, 2, 0, 1, 2}, {1, 2, 0, 2, 1}, {1, 2, 1, 0, 1}, {1, 2, 1, 0, 2}, {2, 0,1, 0, 2}, {2, 0, 1, 2, 0}, {2, 0, 1, 2, 1}, {2, 0, 2,1, 0}, {2, 0, 2, 1, 2}, {2,1, 0, 1, 2}, {2, 1, 0, 2, 0}, {2, 1, 0, 2, 1}, {2, 1, 2, 0, 1}, {2, 1, 2, 0, 2}},",
				"{{0, 1, 0, 2, 0, 1}, {0, 1, 0, 2, 1, 0}, {0, 1,0, 2, 1, 2}, {0, 1, 2, 0, 1, 0}, {0, 1, 2, 1, 0, 1}, {0, 2, 0, 1, 0, 2}, {0, 2, 0, 1, 2, 0}, {0, 2, 0, 1, 2, 1}, {0, 2, 1, 0, 2, 0}, {0, 2, 1, 2, 0, 2}, {1, 0, 1, 2, 0, 1}, {1, 0, 1, 2, 0, 2}, {1, 0, 1, 2, 1, 0}, {1, 0, 2, 0, 1, 0}, {1, 0, 2, 1, 0, 1}, {1, 2, 0, 1, 2, 1}, {1, 2, 0, 2, 1, 2}, {1, 2, 1, 0, 1, 2}, {1, 2, 1, 0, 2, 0}, {1, 2, 1, 0, 2, 1}, {2, 0, 1, 0, 2, 0}, {2, 0, 1, 2, 0, 2}, {2, 0, 2, 1, 0, 1}, {2, 0, 2, 1, 0, 2}, {2, 0, 2, 1, 2, 0}, {2, 1, 0, 1, 2, 1}, {2, 1, 0, 2, 1, 2}, {2, 1, 2, 0, 1, 0}, {2, 1, 2, 0, 1, 2}, {2, 1, 2, 0, 2, 1}},",
				"{{0, 1, 0, 2, 0, 1, 0}, {0,1, 0, 2, 1, 0, 1}, {0, 1, 2, 1, 0, 1, 2}, {0, 2, 0, 1, 0, 2, 0}, {0, 2, 0, 1, 2, 0, 2}, {0, 2, 1, 2, 0, 2, 1}, {1, 0, 1, 2, 0, 1, 0}, {1, 0, 1, 2, 1, 0, 1}, {1, 0, 2, 0, 1, 0, 2}, {1, 2, 0, 2, 1, 2, 0}, {1, 2, 1, 0, 1, 2, 1}, {1, 2, 1, 0, 2, 1, 2}, {2, 0, 1, 0, 2, 0, 1}, {2, 0, 2,1, 0, 2, 0}, {2, 0, 2, 1, 2, 0, 2}, {2,1, 0, 1, 2, 1, 0}, {2, 1, 2, 0, 1, 2, 1}, {2, 1, 2, 0, 2, 1, 2}}"
			],
			"mathematica": [
				"f[n_, k_] := NestList[ DeleteCases[ Flatten[ Map[ Table[ Append[#, i - 1], {i, k}] \u0026, #], 1], {___, u__, v__} /; Sort[{u}] == Sort[{v}]] \u0026, {{}}, n]; f[7, 3] // Flatten (* initially from _Roger L. Bagula_ and modified by _Robert G. Wilson v_, Sep 06 2011 *)"
			],
			"xref": [
				"Cf. A194942, A001285, A007413, A005679, A036584, A099054, A100337, A006156, A099951."
			],
			"keyword": "nonn,tabf,fini,full",
			"offset": "1,3",
			"author": "_M. F. Hasler_, Sep 04 2011, based on deleted sequence A138036 from _Roger L. Bagula_, May 02 2008",
			"ext": [
				"Edited by _Franklin T. Adams-Watters_, Sep 05 2011"
			],
			"references": 2,
			"revision": 42,
			"time": "2014-03-11T12:46:55-04:00",
			"created": "2011-09-04T21:47:06-04:00"
		}
	]
}