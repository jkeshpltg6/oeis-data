{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A100307",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 100307,
			"data": "1,4,10,40,82,328,820,3280,6562,26248,65620,262480,538084,2152336,5380840,21523360,43046722,172186888,430467220,1721868880,3529831204,14119324816,35298312040,141193248160,282472589764,1129890359056",
			"name": "Modulo 2 binomial transform of 3^n.",
			"comment": [
				"3^n may be retrieved through 3^n = Sum_{k=0..n}(-1)^A010060(n-k)*mod(binomial(n,k),2)*a(k)."
			],
			"link": [
				"Gheorghe Coserea, \u003ca href=\"/A100307/b100307.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"V. Shevelev, \u003ca href=\"http://arxiv.org/abs/1011.6083\"\u003eOn Stephan's conjectures concerning Pascal triangle modulo 2 and their polynomial generalization\u003c/a\u003e, J. of Algebra Number Theory: Advances and Appl., 7 (2012), no.1, 11-29."
			],
			"formula": [
				"a(n) = Sum_{k=0..n} mod(binomial(n, k), 2)*3^k.",
				"From _Vladimir Shevelev_, Dec 26-27 2013: (Start)",
				"Sum_{n\u003e=0}1/a(n)^r = Product_{k\u003e=0}(1 + 1/(3^(2^k)+1)^r),",
				"Sum_{n\u003e=0}(-1)^A000120(n)/a(n)^r = Product_{k\u003e=0}(1 - 1/(3^(2^k)+1)^r), where r\u003e0 is a real number.",
				"In particular,",
				"Sum_{n\u003e=0}1/a(n) = Product_{k\u003e=0}(1 + 1/(3^(2^k)+1)) = 1.391980...;",
				"Sum_{n\u003e=0}(-1)^A000120(n)/a(n) = 2/3.",
				"a(2^n) = 3^(2^n)+1, n\u003e=0.",
				"Note that analogs of Stephan's limit formulas (see Shevelev link) reduce to the relations:",
				"a(2^t*n+2^(t-1)) = 8*(3^(2^(t-1)+1))/(3^(2^(t-1))-1) * a(2^t*n+2^(t-1)-2), t\u003e=2.",
				"In particular, for t=2,3,4, we have the following formulas:",
				"a(4*n+2) = 10 * a(4*n);",
				"a(8*n+4) = 41/5 * a(8*n+2);",
				"a(16*n+8)= 3281/410 * a(16*n+6), etc. (End)",
				"From _Tom Edgar_, Oct 11 2015: (Start)",
				"a(n) = Product_{b_j != 0} a(2^j) where n = Sum_{j\u003e=0} b_j*2^j is the binary representation of n.",
				"a(2*k+1) = 4*a(2*k).",
				"(End)"
			],
			"mathematica": [
				"Table[Sum[Mod[Binomial[n,k],2]3^k,{k,0,n}],{n,0,30}] (* _Harvey P. Dale_, Aug 28 2013 *)"
			],
			"program": [
				"(Sage) [sum((binomial(n,k)%2)*3^k for k in [0..n]) for n in [0..100]] # _Tom Edgar_, Oct 11 2015",
				"(PARI) a(n) = subst(lift((Mod(1,2)+'x)^n), 'x, 3); \\\\ _Gheorghe Coserea_, Jun 11 2016"
			],
			"xref": [
				"Cf. A001316, A001317, A038183, A100308, A100309, A100310, A100311."
			],
			"keyword": "easy,nonn",
			"offset": "0,2",
			"author": "_Paul Barry_, Dec 06 2004",
			"references": 7,
			"revision": 44,
			"time": "2016-06-12T02:34:57-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}