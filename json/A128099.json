{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A128099",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 128099,
			"data": "1,1,1,2,1,4,1,6,4,1,8,12,1,10,24,8,1,12,40,32,1,14,60,80,16,1,16,84,160,80,1,18,112,280,240,32,1,20,144,448,560,192,1,22,180,672,1120,672,64,1,24,220,960,2016,1792,448,1,26,264,1320,3360,4032,1792,128,1,28",
			"name": "Triangle read by rows: T(n,k) is the number of ways to tile a 3 X n rectangle with k pieces of 2 X 2 tiles and 3n-4k pieces of 1 X 1 tiles (0 \u003c= k \u003c= floor(n/2)).",
			"comment": [
				"Row sums are the Jacobsthal numbers (A001045).",
				"Apparently, T(n,k)/2^n equals the probability P that n will occur as a partial sum in a randomly-generated infinite sequence of 1s and 2s with n compositions (ordered partitions) into (n-2k) 1s and k 2s. Example: T(6,2)=24; P = 3/8 (24/2^6) that 6 will occur as a partial sum in the sequence with 2 (6-2*2) 1s and 2 2s. - _Bob Selcoe_, Jul 06 2013",
				"From _Johannes W. Meijer_, Aug 28 2013: (Start)",
				"The antidiagonal sums are A077949 and the backwards antidiagonal sums are A052947.",
				"Moving the terms in each column of this triangle, see the example, upwards to row 0 gives the Pell-Jacobsthal triangle A013609 as a square array. (End)",
				"The numbers in rows of the triangle are along \"first layer\" skew diagonals pointing top-right in center-justified triangle given in A013609 ((1+2*x)^n) and along (first layer) skew diagonals pointing top-left in center-justified triangle given in A038207 ((2+x)^n), see links. - _Zagros Lalo_, Jul 31 2018",
				"If s(n) is the row sum at n, then the ratio s(n)/s(n-1) is approximately 2.000..., when n approaches infinity. - _Zagros Lalo_, Jul 31 2018",
				"It appears that the rows of this array are the coefficients of the Jacobsthal polynomials (see MathWorld link). - _Michel Marcus_, Jun 15 2019"
			],
			"reference": [
				"Shara Lalo and Zagros Lalo, Polynomial Expansion Theorems and Number Triangles, Zana Publishing, 2018, ISBN: 978-1-9995914-0-3, pp. 80-83, 357-358"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A128099/b128099.txt\"\u003eTable of n, a(n) for the first 100 rows, flattened\u003c/a\u003e",
				"Richard Fors, \u003ca href=\"http://www.math.kth.se/xComb/fors.pdf\"\u003eIndependence Complexes of Certain Families of Graphs\u003c/a\u003e, Master's thesis in Mathematics at KTH, presented Aug 19 2011.",
				"R. J. Mathar, \u003ca href=\"http://arxiv.org/abs/1609.03964\"\u003eTiling n x m rectangles with 1 x 1 and s x s squares\u003c/a\u003e arXiv:1609.03964  [math.CO] (2016).",
				"Zagros Lalo, \u003ca href=\"/A128099/a128099.pdf\"\u003eFirst layer skew diagonals in center-justified triangle of coefficients in expansion of (1 + 2x)^n\u003c/a\u003e",
				"Zagros Lalo, \u003ca href=\"/A128099/a128099_1.pdf\"\u003eFirst layer skew diagonals in center-justified triangle of coefficients in expansion of (2 + x)^n\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/JacobsthalPolynomial.html\"\u003eJacobsthal Polynomial\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = 2^k*binomial(n-k,k) = 2^k*A011973(n,k).",
				"G.f.: 1/(1-z-2*t*z^2).",
				"Sum_{k=0..floor(n/2)} k*(T(n,k) = A095977(n-1).",
				"From _Johannes W. Meijer_, Aug 28 2013: (Start)",
				"T(n, k) = 2*T(n-2, k-1) + T(n-1, k) with T(n, 0) = 1 and T(n, k) = 0 for k \u003c 0 and k \u003e floor(n/2).",
				"T(n, k) = A013609(n-k, k), n \u003e= 0 and 0 \u003c= k \u003c= floor(n/2). (End)"
			],
			"example": [
				"Triangle starts:",
				"  1;",
				"  1;",
				"  1,  2;",
				"  1,  4;",
				"  1,  6,  4;",
				"  1,  8, 12;",
				"  1, 10, 24,  8;",
				"  1, 12, 40, 32;"
			],
			"maple": [
				"T := proc(n,k) if k\u003c=n/2 then 2^k*binomial(n-k,k) else 0 fi end: for n from 0 to 16 do seq(T(n,k),k=0..floor(n/2)) od; # yields sequence in triangular form",
				"T := proc(n, k) option remember: if k\u003c0 or k \u003e floor(n/2) then return(0) fi: if k = 0 then return(1) fi: 2*procname(n-2, k-1) + procname(n-1, k): end: seq(seq(T(n, k), k=0..floor(n/2)), n=0..13); # _Johannes W. Meijer_, Aug 28 2013"
			],
			"mathematica": [
				"Table[2^k*Binomial[n - k, k] , {n,0,25}, {k,0,Floor[n/2]}] // Flatten  (* _G. C. Greubel_, Dec 28 2016 *)",
				"t[0, 0] = 1; t[n_, k_] := t[n, k] = If[n \u003c 0 || k \u003c 0, 0, t[n - 1, k] + 2 t[n - 2, k - 1]]; Table[t[n, k], {n, 0, 15}, {k, 0, Floor[n/2]}] // Flatten (* _Zagros Lalo_, Jul 31 2018 *)"
			],
			"xref": [
				"Cf. (Triangle sums) A001045, A095977, A077949, A052947, A113726, A052942, A077909.",
				"Cf. (Similar triangles) A008315, A011973, A102541.",
				"Cf. A013609, A038207",
				"Cf. A207538"
			],
			"keyword": "nonn,tabf",
			"offset": "0,4",
			"author": "_Emeric Deutsch_, Feb 18 2007",
			"references": 20,
			"revision": 49,
			"time": "2019-12-14T21:30:28-05:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}