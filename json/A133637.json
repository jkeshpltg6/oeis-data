{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A133637",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 133637,
			"data": "1,-1,0,2,-3,0,4,-6,0,10,-12,0,20,-24,0,36,-45,0,64,-78,0,112,-132,0,189,-222,0,308,-363,0,492,-576,0,778,-900,0,1210,-1392,0,1844,-2121,0,2776,-3180,0,4144,-4716,0,6114,-6936,0,8914,-10098,0,12884,-14550",
			"name": "Expansion of q^(-1) * psi(-q) / psi(-q^3)^3 in powers of q where psi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Cubic AGM theta functions: a(q) (see A004016), b(q) (A005928), c(q) (A005882)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A133637/b133637.txt\"\u003eTable of n, a(n) for n = -1..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of (3 * c(q^2)) / (c(q) * c(q^4)) in powers of q where c() is a cubic AGM function.",
				"Expansion of eta(q) * eta(q^4) * eta(q^6)^3 / (eta(q^2) * eta(q^3)^3 * eta(q^12)^3) in powers of q.",
				"Euler transform of period 12 sequence [ -1, 0, 2, -1, -1, 0, -1, -1, 2, 0, -1, 2, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (12 t)) = (3/4)^(1/2) (t/i)^(-1) g(t) where q = exp(2 Pi i t) and g(t) is the g.f. for A132974.",
				"a(3*n + 1) = 0. a(3*n) = - A132974(n).",
				"Convolution inverse of A113427.",
				"a(3*n - 1) = A263993(n). - _Michael Somos_, Oct 31 2015"
			],
			"example": [
				"G.f. = 1/q - 1 + 2*q^2 - 3*q^3 + 4*q^5 - 6*q^6 + 10*q^8 - 12*q^9 + 20*q^11 - ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ 2 EllipticTheta[ 2, Pi/4, q^(1/2)] / EllipticTheta[ 2, Pi/4, q^(3/2)]^3, {q, 0, n}]; (* _Michael Somos_, Oct 31 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c-1, 0, n++; A = x * O(x^n); polcoeff( eta(x + A) * eta(x^4 + A) * eta(x^6 + A)^3 / (eta(x^2 + A) * eta(x^3 + A)^3 * eta(x^12 + A)^3), n))};"
			],
			"xref": [
				"Cf. A113427, A132974, A263993."
			],
			"keyword": "sign",
			"offset": "-1,4",
			"author": "_Michael Somos_, Sep 18 2007",
			"references": 5,
			"revision": 11,
			"time": "2021-03-12T22:24:44-05:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}