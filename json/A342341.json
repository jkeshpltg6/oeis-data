{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A342341",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 342341,
			"data": "1,1,1,1,1,3,1,3,3,5,5,5,9,7,13,15,17,19,29,31,39,43,63,59,75,121,119,169,167,199,279,305,343,479,537,733,789,883,1057,1421,1545,1831,2409,2577,3343,4001,4657,5131,6065,7755,8841,10473,12995,14659,17671,20619,25157,28255,33131,38265,47699,53171,62611,80005,88519,105937,119989",
			"name": "Number of strict compositions of n with all adjacent parts (x, y) satisfying x \u003c 2y and y \u003c 2x.",
			"comment": [
				"Each quotient of adjacent parts is between 1/2 and 2 exclusive."
			],
			"link": [
				"Bert Dobbelaere, \u003ca href=\"/A342341/b342341.txt\"\u003eTable of n, a(n) for n = 0..100\u003c/a\u003e"
			],
			"example": [
				"The a(1) = 1 through a(17) = 17 compositions (A..G = 10..16):",
				"  1  2  3  4  5   6  7   8   9    A    B   C    D    E     F     G",
				"              23     34  35  45   46   47  57   58   59    69    6A",
				"              32     43  53  54   64   56  75   67   68    78    79",
				"                             234  235  65  345  76   86    87    97",
				"                             432  532  74  354  85   95    96    A6",
				"                                           435  346  347   357   358",
				"                                           453  643  356   456   457",
				"                                           534       653   465   475",
				"                                           543       743   546   547",
				"                                                     2345  564   574",
				"                                                     2354  645   745",
				"                                                     4532  654   754",
				"                                                     5432  753   853",
				"                                                           2346  2347",
				"                                                           6432  2356",
				"                                                                 6532",
				"                                                                 7432"
			],
			"mathematica": [
				"Table[Length[Select[Join@@Permutations/@Select[IntegerPartitions[n],UnsameQ@@#\u0026],And@@Table[#[[i]]\u003c2*#[[i-1]]\u0026\u0026#[[i-1]]\u003c2*#[[i]],{i,2,Length[#]}]\u0026]],{n,0,15}]"
			],
			"xref": [
				"The unordered version (partitions) is A342097 (non-strict: A342096).",
				"The non-strict version is A342330.",
				"The version allowing equality is A342342 (non-strict: A224957).",
				"A000929 counts partitions with adjacent parts x \u003e= 2y.",
				"A002843 counts compositions with adjacent parts x \u003c= 2y.",
				"A154402 counts partitions with adjacent parts x = 2y.",
				"A274199 counts compositions with adjacent parts x \u003c 2y.",
				"A342094 counts partitions with adjacent x \u003c= 2y (strict: A342095).",
				"A342098 counts partitions with adjacent parts x \u003e 2y.",
				"A342331 counts compositions with adjacent parts x = 2y or y = 2x.",
				"A342332 counts compositions with adjacent parts x \u003e 2y or y \u003e 2x.",
				"A342333 counts compositions with adjacent parts x \u003e= 2y or y \u003e= 2x.",
				"A342335 counts compositions with adjacent parts x \u003e= 2y or y = 2x.",
				"A342337 counts partitions with adjacent parts x = y or x = 2y.",
				"A342338 counts compositions with adjacent parts x \u003c 2y and y \u003c= 2x.",
				"Cf. A003114, A003242, A034296, A167606, A342083, A342084, A342087, A342191, A342334, A342336, A342339, A342340."
			],
			"keyword": "nonn",
			"offset": "0,6",
			"author": "_Gus Wiseman_, Mar 12 2021",
			"ext": [
				"More terms from _Bert Dobbelaere_, Mar 19 2021"
			],
			"references": 14,
			"revision": 10,
			"time": "2021-03-20T13:49:58-04:00",
			"created": "2021-03-13T12:19:55-05:00"
		}
	]
}