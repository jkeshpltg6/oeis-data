{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000707",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 707,
			"id": "M1646 N0644",
			"data": "1,1,2,6,20,71,259,961,3606,13640,51909,198497,762007,2934764,11333950,43874857,170193528,661386105,2574320659,10034398370,39163212165,153027659730,598577118991,2343628878849,9184197395425,36020235035016,141376666307608",
			"name": "Number of permutations of [1,2,...,n] with n-1 inversions.",
			"comment": [
				"Same as number of submultisets of size n-1 of the multiset with multiplicities [1,2,...,n-1]. - _Joerg Arndt_, Jan 10 2011. Stated another way, a(n-1) is the number of size n \"multisubsets\" (see example) of M = {a^1,b^2,c^3,d^4,...,#^n!}. - _Geoffrey Critzer_, Apr 01 2010, corrected by _Jacob Post_, Jan 03 2011",
				"For a more general result (taking multisubset of any size) see A008302. - _Jacob Post_, Jan 03 2011",
				"The number of ordered submultisets is found in A129481; credit for this observation should go to Marko Riedel at math.stackexchange. - _J. M. Bergot_, Aug 12 2016"
			],
			"reference": [
				"F. N. David, M. G. Kendall and D. E. Barton, Symmetric Function and Allied Tables, Cambridge, 1966, p. 241.",
				"S. R. Finch, Mathematical Constants, Cambridge, 2003, Section 5.14., p.356",
				"D. E. Knuth, The Art of Computer Programming. Addison-Wesley, Reading, MA, Vol. 3, p. 15.",
				"E. Netto, Lehrbuch der Combinatorik. 2nd ed., Teubner, Leipzig, 1927, p. 96.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A000707/b000707.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"R. K. Guy, \u003ca href=\"/A000707/a000707_2.pdf\"\u003eLetter to N. J. A. Sloane with attachment, Mar 1988\u003c/a\u003e",
				"B. H. Margolius, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL4/MARGOLIUS/inversions.html\"\u003ePermutations with inversions\u003c/a\u003e, J. Integ. Seqs. Vol. 4 (2001), #01.2.4.",
				"R. H. Moritz and R. C. Williams, \u003ca href=\"http://www.jstor.org/stable/2690326\"\u003eA coin-tossing problem and some related combinatorics\u003c/a\u003e, Math. Mag., 61 (1988), 24-29.",
				"E. Netto, \u003ca href=\"http://books.google.fr/books?id=VtIGAAAAYAAJ\u0026amp;pg=PA1\u0026amp;lpg=PA1\u0026amp;dq=Netto,+Lehrbuch+der+Combinatorik\u0026amp;source=bl\u0026amp;ots=bHKKIq5jjf\u0026amp;sig=Mivz_ekDkDAnSMqyXGcfgnqqWfk\u0026amp;hl=en\u0026amp;sa=X\u0026amp;ei=SOyJU96fKO3QsQTlxYDABQ\u0026amp;redir_esc=y\"\u003eLehrbuch der Combinatorik\u003c/a\u003e, 2nd ed., Teubner, Leipzig, 1st ed., 1901, p. 96.",
				"E. Netto, \u003ca href=\"https://archive.org/details/lehrbuchdercomb00nettgoog\"\u003eLehrbuch der Combinatorik\u003c/a\u003e, 2nd ed., Teubner, Leipzig, 1st ed., 1901, p. 96.",
				"E. Netto, \u003ca href=\"/A000707/a000707_1.pdf\"\u003eLehrbuch der Combinatorik\u003c/a\u003e, Chapter 4, annotated scanned copy of pages 92-99 only.",
				"J. Shallit, \u003ca href=\"/A000707/a000707.pdf\"\u003eLetter to N. J. A. Sloane, Oct 08 1980\u003c/a\u003e"
			],
			"formula": [
				"See A008302 for G.f.",
				"a(n) = 2^(2*n-2)/sqrt(Pi*n)*Q*(1+O(n^(-1))), where Q is a digital search tree constant, Q = product(1-1/(2^n),n=1..infinity) = QPochhammer[1/2, 1/2] = 0.288788095... (see A048651), corrected and extended by _Vaclav Kotesovec_, Mar 16 2014"
			],
			"example": [
				"a(4) = 6 because there are 6 multisubsets of {a,b,b,c,c,c} with cardinality =3: {a,b,b}, {a,b,c}, {a,c,c}, {b,b,c}, {b,c,c}, {c,c,c}. - _Geoffrey Critzer_, Apr 01 2010, corrected by _Jacob Post_, Jan 03 2011",
				"G.f. = x + x^2 + 2*x^3 + 6*x^4 + 20*x^5 + 71*x^6 + 259*x^7 + 961*x^8 + ..."
			],
			"mathematica": [
				"Table[SeriesCoefficient[ Series[Product[Sum[x^i, {i, 0, k}], {k, 0, n}], {x, 0, 20}], n], {n, 1, 20}] (* _Geoffrey Critzer_, Apr 01 2010 *)",
				"a[ n_] := SeriesCoefficient[ Product[ Sum[ x^i, {i, 0, k}], {k, 0, n}], {x, 0, n}]; (* _Michael Somos_, Aug 15 2016 *)"
			],
			"program": [
				"(PARI) {a(n) = my(v); if( n\u003c1, 0, sum(k=0, n!-1, v = numtoperm(n, k); n-1 == sum(i=1, n-1, sum(j=i+1, n, v[i]\u003ev[j]))))}; /* _Michael Somos_, Aug 15 2016 */"
			],
			"xref": [
				"One of the diagonals of triangle in A008302.",
				"Cf. A048651, A129481."
			],
			"keyword": "nonn,nice,easy",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _James A. Sellers_, Dec 16 1999",
				"Asymptotic formula from Barbara Haas Margolius (margolius(AT)math.csuohio.edu), May 31 2001",
				"Better definition from _Joerg Arndt_, Jan 10 2011"
			],
			"references": 14,
			"revision": 71,
			"time": "2017-08-05T23:01:52-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}