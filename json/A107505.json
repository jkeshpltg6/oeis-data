{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A107505",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 107505,
			"data": "1,2,6,8,14,12,24,16,30,26,36,24,56,2,48,48,62,36,78,40,84,64,72,48,120,62,6,80,112,60,144,64,126,96,108,96,182,76,120,8,180,84,192,88,168,156,144,96,248,114,186,144,14,108,240,144,240,160,180,120",
			"name": "Theta series of quadratic form with Gram matrix [ 2, 1, 0, 1; 1, 4, 1, 0; 0, 1, 4, -2; 1, 0, -2, 8].",
			"comment": [
				"Coefficients of a theta series associated with a certain \"Haupt-form\" of rank 4 and level 13.",
				"The Gram matrix is denoted by A in Parry 1979 on page 165."
			],
			"link": [
				"W. R. Parry, \u003ca href=\"http://gdz.sub.uni-goettingen.de/dms/resolveppn/?PPN=GDZPPN002196476\"\u003eA negative result on the representation of modular forms by theta series\u003c/a\u003e, J. Reine Angew. Math., 310 (1979), 151-170."
			],
			"formula": [
				"a(n) = 2 * b(n) where b() is multiplicative and b(13^e) = 1, b(p^e) = (p^(e+1) - 1) / (p - 1) otherwise. - _Michael Somos_, Mar 23 2012",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (13 t)) = 13 (t/i)^2 f(t) where q = exp(2 Pi i t). - _Michael Somos_, Mar 23 2012",
				"a(n) = 2 * A284587(n) if n\u003e1. - _Michael Somos_, Oct 23 2019"
			],
			"example": [
				"G.f. = 1 + 2*q + 6*q^2 + 8*q^3 + 14*q^4 + 12*q^5 + 24*q^6 + 16*q^7 + 30*q^8 + ..."
			],
			"mathematica": [
				"a[n_] := If[n == 0, 1, 2 DivisorSigma[1, n/13^IntegerExponent[n, 13]]];",
				"a /@ Range[0, 59] (* _Jean-François Alcover_, Oct 23 2019, after _Michael Somos_ *)",
				"a[n_] := If[n == 0, 1, 2 DivisorSum[n, Boole[!Divisible[#, 13]] # \u0026]];",
				"a /@ Range[0, 59] (* _Jean-François Alcover_, Oct 23 2019 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, n==0, 1, 2 * sigma(n / 13^valuation(n, 13)))}; /* _Michael Somos_, Mar 23 2012 */",
				"(PARI) {a(n) = my(G); if( n\u003c0, 0, G = [2, 1, 0, 1; 1, 4, 1, 0; 0, 1, 4, -2; 1, 0, -2, 8]; polcoeff( 1 + 2 * x * Ser(qfrep( G, n, 1)), n))}; /* _Michael Somos_, Mar 23 2012 */",
				"(Sage) ModularForms( Gamma0(13), 2, prec=100).0; # _Michael Somos_, Jun 27 2013",
				"(MAGMA) Basis( ModularForms( Gamma0(13), 2), 100) [1]; /* _Michael Somos_, Aug 15 2016 */",
				"(MAGMA) [Coefficient(Basis(ModularForms(Gamma0(13), 2))[1], n) : n in [0..100] ]; // _Vincenzo Librandi_, Jun 27 2017"
			],
			"xref": [
				"Cf. A284587."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, May 28 2005",
			"references": 1,
			"revision": 36,
			"time": "2019-10-24T10:57:14-04:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}