{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A232632",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 232632,
			"data": "0,1,-3,1,5,-5,1,-7,14,-7,1,-3,9,-6,1,-11,55,-77,44,-11,1,13,-91,182,-156,65,-13,1,1,-8,14,-7,1,17,-204,714,-1122,935,-442,119,-17,1,-19,285,-1254,2508,-2717,1729,-665,152,-19,1,1,-16,60,-78,44,-11,1,-23,506,-3289,9867,-16445,16744,-10948,4692,-1311,230,-23,1",
			"name": "Coefficient table for the minimal polynomials of s(2*l+1)^2 = (2*sin(Pi/(2*l+1)))^2.",
			"comment": [
				"The length of row l is  delta(2*l+1) + 1 = A055034(2*l+1) + 1, l \u003e= 0.",
				"See the comments on A232631 (even n case) for s(n) = 2*sin(Pi/n) and the minimal polynomial of s(n)^2. Here n = 2*l+1 and s(2*l+1)^2 = 4 - rho(2*l+1)^2 is an integer in the algebraic number field Q(rho(2*l+1)). The minimal polynomial of s(2*l+1)^2 is then MPs2(2*l+1, x) = product(x - 2*(1 + cos(Pi*rpnodd(2*l+1,j)/(2*l+1))), j=1..delta(2*l+1)), l \u003e= 0, where rpnodd(2*l+1) is the list of positive odd numbers \u003c 2*l+1 and relatively prime to 2*l+1. rpnodd(2*l+1,j) is the j-th member of this increasingly ordered list. Here the identity 4 - (2*cos(Pi*(2*k+1)/(2*l+1)))^2 = 2*(1 - cos(Pi*2*(2*k+1)/(2*l+1))) = 2*(1 - (- cos(Pi*(2*l+1 - 2*(2*k+1))/ (2*l+1)))) has been used, and for 2*k+1 \u003c 2*l+1 and  gcd(2*k+1, 2*l+1) = 1 this becomes the product given above because 1 = gcd(-(2*k+1), 2*l+1) =  gcd(-2*(2*k+1), 2*l+1) =  gcd(2*l+1, -2*(2*k+1) + (2*l+1)).",
				"This computation was motivated by a preprint of S. Mustonen, P. Haukkanen and J. K. Merikoski, called ``Polynomials associated with squared diagonals of regular polygons'', Nov 16 2013."
			],
			"formula": [
				"a(l,m) = [x^m] MPs2(2*l+1, x), l \u003e= 1, m = 0, 1, ...., delta(l), with the minimal polynomial MPs2(l, x) of (2*sin(Pi/(2*l+1)))^2, explained above in a comment."
			],
			"example": [
				"The table a(l,m) begins (n = 2*l+1):",
				"------------------------------------------------------------------------------------------------------",
				"n,   l\\m     0      1      2       3        4       5        6       7     8      9    10  11 ...",
				"1,   0:      0      1",
				"3,   1:     -3      1",
				"5,   2:      5     -5      1",
				"7,   3:     -7     14     -7       1",
				"9,   4:     -3      9     -6       1",
				"11,  5:    -11     55    -77      44      -11       1",
				"13,  6:     13    -91    182    -156       65     -13        1",
				"15,  7:      1     -8     14      -7        1",
				"17,  8:     17   -204    714   -1122      935    -442      119     -17      1",
				"19,  9:    -19    285  -1254    2508    -2717    1729     -665     152    -19     1",
				"21, 10:      1    -16     60     -78       44     -11        1",
				"23, 11:    -23    506  -3289    9867   -16445   16744   -10948    4692  -1311   230   -23   1",
				"25, 12:      5   -125    875   -2675     4300   -4005     2275    -800    170   -20     1",
				"27, 13:     -3     81   -540    1386    -1782    1287     -546     135    -18     1",
				"....",
				"n=29, l=14:  29,-1015,10556,-51272,140998,-243542,281010,-224808,127281,-51359,14674, -2900,377,-29,1.",
				"n=31, l=15: -31, 1240, -14756, 82212, -260338, 520676, -700910, 660858, -447051, 219604, -78430, 20150, -3627, 434, -31, 1.",
				"...",
				"The minimal polynomial of s(5)^2 = (2*sin(Pi/5))^2 = 4 - rho(5)^2",
				"= 2*(1 - cos(Pi*2/5)) = 2*(1 + cos(Pi*3/5)),  approx. 1.381966, is MPs2(5, x) =  product(x - 2*(1 + cos(Pi*rpnodd(5,j)/5)), j=1..2) = (x - 2*(1 + cos(Pi/5))*(x - 2*(1 + cos(Pi*3/5)) = (x - (2 + phi)*(x - (2 + 1 - phi)) = x^2 - 5*x + (6 + phi - phi^2) = x^2 - 5*x +5, where phi = rho(5) is the golden section.",
				"The row n=17 checks with WolframAlpha's MinimalPolynomial[(2*sin(Pi/17))^2 ,x] = 17-204 x+714 x^2-1122 x^3+935 x^4-442 x^5+119 x^6-17 x^7+x^8."
			],
			"mathematica": [
				"Flatten[ CoefficientList[ Table[ MinimalPolynomial[ (2*Sin[Pi/(2*l+1)])^2, x], {l, 0, 15}], x]] (adapted from Jean-François Alcover, A187360) - Wolfdieter Lang, Dec 23 2013"
			],
			"xref": [
				"Cf. A232631 (even n), A232633 (all n), A055034."
			],
			"keyword": "sign,tabf,easy",
			"offset": "0,3",
			"author": "_Wolfdieter Lang_, Dec 18 2013",
			"references": 3,
			"revision": 7,
			"time": "2013-12-25T02:22:49-05:00",
			"created": "2013-12-25T02:22:49-05:00"
		}
	]
}