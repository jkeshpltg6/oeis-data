{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A109974",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 109974,
			"data": "1,2,1,2,3,1,3,4,5,1,2,7,10,9,1,4,6,21,28,17,1,2,12,26,73,82,33,1,4,8,50,126,273,244,65,1,3,15,50,252,626,1057,730,129,1,4,13,85,344,1394,3126,4161,2188,257,1,2,18,91,585,2402,8052,15626,16513,6562,513,1,6,12",
			"name": "Array read by downwards antidiagonals: sigma_k(n) for n \u003e= 1, k \u003e= 0.",
			"comment": [
				"Rows sums are A108639. Antidiagonal sums are A109976. Matrix inverse is A109977.",
				"From _Wolfdieter Lang_, Jan 29 2016: (Start)",
				"The sum of the (k-1)th power of the divisors of n, sigma_(k-1)(n), appears also as eigenvalue lambda(k, n) of the Hecke operators T_n, n a positive integer, acting on the normalized Eisenstein series E_k(q) = ((2*Pi*i)^k/((k-1)!*Zeta(k))*G_k(q) with even k \u003e= 4 and q = 2*Pi*i*z, where z is from the upper half of the complex plane: T_n E_k = sigma_(k-1)(n)*E_k. These Eisenstein series are entire modular forms of weight k, and each E_k(q) is a simultaneous eigenform of the Hecke operators T_n, for every n \u003e= 1.",
				"This results from the Fourier coefficients of E_k(q) = Sum_{m\u003e=0} E(k, m)*q^m, with E(k, 0) =1 and E(k, m) = ((2*Pi*i)^k / ((k-1)!*Zeta(k))* sigma_(k-1)(m) for m \u003e= 1, together with the Fourier coefficients of T_n E_k. The eigenvalues lambda(n, k) = (Sum_{d | gcd(n,m)} d^{k-1}*E(k, m*n/d^2)) / E(k, m) for each m \u003e= 0. For m=0 this becomes lambda(n, k) = sigma_(k-1)(n).",
				"For Hecke operators, Fourier coefficients and simultaneous eigenforms see, e.g., the Koecher - Krieg reference, p. 207, eqs. (5) and (6) and p. 211, section 4, or the Apostol reference, p. 120, eq. (13), pp. 129 - 134. (End)"
			],
			"reference": [
				"Tom M. Apostol, Modular functions and Dirichlet series in number theory, second Edition, Springer, 1990, pp. 120, 129 - 134.",
				"Max Koecher and Aloys Krieg, Elliptische Funktionen und Modulformen, 2. Auflage, Springer, 2007, pp. 207, 211."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A109974/b109974.txt\"\u003eAntidiagonals k = 0..140, flattened\u003c/a\u003e"
			],
			"formula": [
				"Regarded as a triangle, T(n, k) = if(k\u003c=n, sigma(k-1, n-k+1), 0). - _Franklin T. Adams-Watters_, Jul 17 2006",
				"If the row index (the index of the antidiagonal of the array) is taken as m with offset 1 the triangle is T(m, k) = sigma_k(m-k), 1 \u003c= k+1 \u003c= m, otherwise 0. - _Wolfdieter Lang_, Jan 14 2016",
				"G.f. for the triangle with offset 1: G(x,y) = Sum_{j\u003e=1} x^j/((1-x^j)*(1-j*x*y)). - _Robert Israel_, Jan 14 2016"
			],
			"example": [
				"Start of array:",
				"1,  2,  2,   3,   2,    4, ...",
				"1,  3,  4,   7,   6,   12, ...",
				"1,  5, 10,  21,  26,   50, ...",
				"1,  9, 28,  73, 126,  252, ...",
				"1, 17, 82, 273, 626, 1394, ...",
				"...",
				"The triangle T(m, k) with row offset 1 starts:",
				"m\\k 0  1  2   3    4    5    6    7   8 9 ...",
				"1:  1",
				"2:  2  1",
				"3:  2  3  1",
				"4:  3  4  5   1",
				"5:  2  7 10   9    1",
				"6:  4  6 21  28   17    1",
				"7:  2 12 26  73   82   33    1",
				"8:  4  8 50 126  273  244   65    1",
				"9:  3 15 50 252  626 1057  730  129   1",
				"10: 4 13 85 344 1394 3126 4161 2188 257 1",
				"... - _Wolfdieter Lang_, Jan 14 2016"
			],
			"maple": [
				"with(numtheory):",
				"seq(seq(sigma[k](1+d-k), k=0..d), d=0..12);  # _Alois P. Heinz_, Feb 06 2013"
			],
			"mathematica": [
				"rows = 12; Flatten[ Table[ DivisorSigma[k-n, n], {k, 1, rows}, {n, k, 1, -1}]] (* _Jean-François Alcover_, Nov 15 2011 *)"
			],
			"xref": [
				"Rows: A000005, A000203, A001157, A001158, A001159, A001160, A013954-A013972; columns: A000051, A034472, A001576, A034474, A034488, A034491, A034496, A034513, A034517, A034524, A034660; row sums A108639; diagonals A082245, A023887; also see A082771, A109976, A109977, A109978."
			],
			"keyword": "easy,nonn,tabl,nice",
			"offset": "0,2",
			"author": "_Paul Barry_, Jul 06 2005",
			"references": 24,
			"revision": 41,
			"time": "2019-03-31T02:24:16-04:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}