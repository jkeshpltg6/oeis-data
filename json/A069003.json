{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A069003",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 69003,
			"data": "1,1,2,1,2,1,2,3,4,1,4,7,2,1,2,1,2,5,6,1,4,5,8,1,4,1,2,5,4,11,4,3,2,5,2,1,2,3,10,1,4,5,8,9,2,5,2,13,4,7,4,3,10,1,4,1,2,3,6,13,10,3,32,9,2,1,2,5,10,3,6,5,2,1,4,5,10,7,4,7,4,3,18,1,2,9,2,3,4,1,4,7,8,1,2,5,2,3,4,3",
			"name": "Smallest integer d such that n^2 + d^2 is a prime number.",
			"comment": [
				"With i being the imaginary unit, n + di is the smallest Gaussian prime with real part n and a positive imaginary part. Likewise for n - di. See A002145 for Gaussian primes with imaginary part 0. - _Alonso del Arte_, Feb 07 2011",
				"Conjecture: a(n) does not exceed 4*sqrt(n+1) for any positive integer n. - _Zhi-Wei Sun_, Apr 15 2013",
				"Conjecture holds for the first 15*10^6 terms. - _Joerg Arndt_, Aug 19 2014",
				"Infinitely many d exist such that n^2 + d^2 is prime, under Schinzel's Hypothesis H; see Sierpinski (1988), p. 221. - _Jonathan Sondow_, Nov 09 2015"
			],
			"reference": [
				"W. Sierpinski, Elementary Theory of Numbers, 2nd English edition, revised and enlarged by A. Schinzel, Elsevier, 1988."
			],
			"link": [
				"T. D. Noe and Zhi-Wei Sun (with the first 1000 terms from Noe), \u003ca href=\"/A069003/b069003.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/GaussianPrime.html\"\u003eGaussian Prime\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Schinzel%27s_hypothesis_H\"\u003eSchinzel's Hypothesis H\u003c/a\u003e"
			],
			"example": [
				"a(5)=2 because 2 is the smallest integer d such that 5^2+d^2 is a prime number."
			],
			"maple": [
				"f:= proc(n) local d;",
				"     for d from 1+(n mod 2) by 2 do",
				"       if isprime(n^2+d^2) then return d fi",
				"     od",
				"end proc:",
				"f(1):= 1:",
				"map(f, [$1..1000]); # _Robert Israel_, Jul 06 2015"
			],
			"mathematica": [
				"imP4P[n_] := Module[{k = 1}, While[Not[PrimeQ[n^2 + k^2]], k++]; k]; Table[imP4P[n], {n, 50}] (* _Alonso del Arte_, Feb 07 2011 *)"
			],
			"program": [
				"(PARI) a(n)=my(k);while(!isprime(n^2+k++^2),);k \\\\ _Charles R Greathouse IV_, Mar 20 2013"
			],
			"xref": [
				"Cf. A068486 (lists the prime numbers n^2 + d^2).",
				"Cf. A185636, A204065.",
				"Cf. A239388, A239389 (record values).",
				"Cf. A053000."
			],
			"keyword": "easy,nonn",
			"offset": "1,3",
			"author": "_T. D. Noe_, Apr 02 2002",
			"references": 19,
			"revision": 44,
			"time": "2015-11-09T10:03:21-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}