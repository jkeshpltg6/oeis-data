{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002559",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2559,
			"id": "M1432 N0566",
			"data": "1,2,5,13,29,34,89,169,194,233,433,610,985,1325,1597,2897,4181,5741,6466,7561,9077,10946,14701,28657,33461,37666,43261,51641,62210,75025,96557,135137,195025,196418,294685,426389,499393,514229,646018,925765",
			"name": "Markoff (or Markov) numbers: union of positive integers x, y, z satisfying x^2 + y^2 + z^2 = 3xyz.",
			"comment": [
				"A004280 gives indices of Fibonacci numbers (A000045) which are also Markoff (or Markov) numbers.",
				"As mentioned by Conway and Guy, all odd-indexed Pell numbers (A001653) also appear in this sequence. The positions of the Fibonacci and Pell numbers in this sequence are given in A158381 and A158384, respectively. - _T. D. Noe_, Mar 19 2009",
				"Assuming that each solution (x,y,z) is ordered x \u003c= y \u003c= z, the open problem is to prove that each z value occurs only once. There are no counterexamples in the first 1046858 terms, which have z values \u003c Fibonacci(5001) = 6.2763...*10^1044. - _T. D. Noe_, Mar 19 2009",
				"Zagier shows that there are C log^2 (3x) + O(log x (log log x)^2) Markoff numbers below x, for C = 0.180717.... - _Charles R Greathouse IV_, Mar 14 2010 [but see Thompson, below]",
				"The odd numbers in this sequence are of the form 4k+1. - _Paul Muljadi_, Jan 31 2011",
				"All prime divisors of Markov numbers (with exception 2) are of the form 4k+1. - _Artur Jasinski_, Nov 20 2011",
				"Kaneko extends a parameterization of Markoff numbers, citing Frobenius, and relates it to a conjectured behavior of the elliptic modular j-function at real quadratic numbers. - _Jonathan Vos Post_, May 06 2012",
				"Riedel (2012) claims a proof of the unicity conjecture: \"it will be shown that the largest member of [a Markoff] triple determines the other two uniquely.\" - _Jonathan Sondow_, Aug 21 2012",
				"There are 93 terms with each term \u003c= 2*10^9 in the sequence. The number of distinct prime divisors of any of the first 93 terms is less than 6. The second, third, 4th, 5th, 6th, 10th, 11th, 15th, 16th, 18th, 20th, 24th, 25th, 27th, 30th, 36th, 38th, 45th, 48th, 49th, 69th, 79th, 81st, 86th, 91st terms are primes. - _Shanzhen Gao_, Sep 18 2013",
				"Bourgain, Gamburd, and Sarnak have announced a proof that almost all Markoff numbers are composite--see A256395. Equivalently, the prime Markoff numbers A178444 have density zero among all Markoff numbers. (It is conjectured that infinitely many Markoff numbers are prime.) - _Jonathan Sondow_, Apr 30 2015",
				"According to Sarnak on Apr 30 2015, all claims to have proved the unicity conjecture have turned out to be false. - _Jonathan Sondow_, May 01 2015",
				"The numeric value of C = lim (number of Markoff numbers \u003c x) / log^2(3x) given in Zagier's paper and quoted above suffers from an accidentally omitted digit and rounding errors. The correct value is C = 0.180717104711806... (see A261613 for more digits). - _Christopher E. Thompson_, Aug 22 2015",
				"Named after the Russian mathematician Andrey Andreyevich Markov (1856-1922). - _Amiram Eldar_, Jun 10 2021"
			],
			"reference": [
				"Martin Aigner, Markov's theorem and 100 years of the uniqueness conjecture. A mathematical journey from irrational numbers to perfect matchings. Springer, 2013. x+257 pp. ISBN: 978-3-319-00887-5; 978-3-319-00888-2 MR3098784",
				"John H. Conway and Richard K. Guy, The Book of Numbers, Copernicus Press, NY, 1996, p. 187.",
				"Jean-Marie De Koninck, Those Fascinating Numbers, Amer. Math. Soc., 2009, page 86.",
				"Richard K. Guy, Unsolved Problems in Number Theory, D12.",
				"G. H. Hardy and E. M. Wright, An Introduction to the Theory of Numbers, 5th ed., Oxford Univ. Press, 1979, notes on ch. 24.6 (p. 412)",
				"Florian Luca and A. Srinivasan, Markov equation with Fibonacci components, Fib. Q., 56 (No. 2, 2018), 126-129.",
				"Richard A. Mollin, Advanced Number Theory with Applications, Chapman \u0026 Hall/CRC, Boca Raton, 2010, 123-125.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A002559/b002559.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Ryuji Abe and Benoît Rittaud, \u003ca href=\"https://doi.org/10.1016/j.disc.2017.07.010\"\u003eOn palindromes with three or four letters associated to the Markoff spectrum\u003c/a\u003e, Discrete Mathematics, Vol. 340, No. 12 (2017), pp. 3032-3043.",
				"Tom Ace, \u003ca href=\"https://minortriad.com/markoff.html\"\u003eMarkoff numbers\u003c/a\u003e.",
				"Enrico Bombieri, \u003ca href=\"http://dx.doi.org/10.1016/j.exmath.2006.10.002\"\u003eContinued fractions and the Markoff tree\u003c/a\u003e, Expo. Math., Vol. 25, No. 3 (2007), pp. 187-213.",
				"Jean Bourgain, Alex Gamburd and Peter Sarnak, \u003ca href=\"https://doi.org/10.1016/j.crma.2015.12.006\"\u003eMarkoff triples and strong approximation\u003c/a\u003e, Comptes Rendus Mathematique, Vol. 354, No. 2 (2016), pp. 131-135; \u003ca href=\"http://arxiv.org/abs/1505.06411\"\u003earXiv preprint\u003c/a\u003e, arXiv:1505.06411 [math.NT], 2015.",
				"Roger Descombes, \u003ca href=\"http://dx.doi.org/10.5169/seals-36333\"\u003eProblèmes d'approximation diophantienne\u003c/a\u003e, L'Enseignement Math. (2), Vol. 6 (1960), pp. 18-26.",
				"Roger Descombes, \u003ca href=\"/A002559/a002559.pdf\"\u003eProblèmes d'approximation diophantienne\u003c/a\u003e, L'Enseignement Math. (2), Vol. 6 (1960), pp. 18-26. [Annotated scanned copy]",
				"Jonathan David Evans and Ivan Smith, \u003ca href=\"https://doi.org/10.2140/gt.2018.22.1143\"\u003eMarkov numbers and Lagrangian cell complexes in the complex projective plane\u003c/a\u003e, Geometry \u0026 Topology, Vol. 22 (2018), pp. 1143-1180; \u003ca href=\"https://arxiv.org/abs/1606.08656\"\u003earXiv preprint\u003c/a\u003e, arXiv:1606.08656 [math.SG], 2016-2017.",
				"Carlos A. Gómez, Jhonny C. Gómez, and Florian Luca, \u003ca href=\"https://doi.org/10.33039/ami.2020.06.001\"\u003eMarkov triples with k-generalized Fibonacci components\u003c/a\u003e, Annales Mathematicae et Informaticae, Vol. 52 (2020), pp. 107-115.",
				"Richard K. Guy, \u003ca href=\"http://www.jstor.org/stable/2975688\"\u003eDon't try to solve these problems\u003c/a\u003e, Amer. Math. Monthly, Vol. 90, No. 1 (1983), pp. 35-41.",
				"Hayder Raheem Hashim and Szabolcs Tengely, \u003ca href=\"https://doi.org/10.1515/ms-2017-0414\"\u003eSolutions of a generalized markoff equation in Fibonacci numbers\u003c/a\u003e, Mathematica Slovaca, Vol. 70, No. 5 (2020), pp. 1069-1078.",
				"Masanobu Kaneko, \u003ca href=\"http://www.ams.org/amsmtgs/2190_abstracts/1078-11-124.pdf\"\u003eCongruences of Markoff numbers via Farey parametrization\u003c/a\u003e, Preliminary Report, Dec 2011, AMS 1078-11-124, listed in Abstracts of Papers Presented to AMS, Vol.33, No.2, Issue 168, Spring 2012.",
				"Clément Lagisquet, Edita Pelantová, Sébastien Tavenas, and Laurent Vuillon, \u003ca href=\"https://arxiv.org/abs/2010.10335\"\u003eOn the Markov numbers: fixed numerator, denominator, and sum conjectures\u003c/a\u003e, arXiv:2010.10335 [math.CO], 2020.",
				"Mong Lung Lang and Ser Peow Tan, \u003ca href=\"https://doi.org/10.1007/s10711-007-9189-x\"\u003eA simple proof of the Markoff conjecture for prime powers\u003c/a\u003e, Geometriae Dedicata, Vol. 129 (2007), pp. 15-22; \u003ca href=\"https://arxiv.org/abs/math/0508443\"\u003earXiv preprint\u003c/a\u003e, arXiv:math/0508443 [math.NT], 2005.",
				"Kyungyong Lee, Li Li, Michelle Rabideau and Ralf Schiffler, \u003ca href=\"https://arxiv.org/abs/2010.13010\"\u003eOn the ordering of the Markov numbers\u003c/a\u003e, arXiv:2010.13010 [math.NT], 2020.",
				"James Propp, \u003ca href=\"http://faculty.uml.edu/jpropp/markoff-talk.html\"\u003eThe combinatorics of Markov numbers\u003c/a\u003e, U. Wisconsin Combinatorics Seminar, April 4, 2005.",
				"S. G. Rayaguru, M. K. Sahukar, and G. K. Panda, \u003ca href=\"https://doi.org/10.7546/nntdm.2020.26.3.149-159\"\u003eMarkov equation with components of some binary recurrent sequences\u003c/a\u003e, Notes on Number Theory and Discrete Mathematics, Vol. 26, No. 3 (2020), pp. 149-159.",
				"Norbert Riedel, \u003ca href=\"http://arxiv.org/abs/1208.4032\"\u003eOn the Markoff Equation\u003c/a\u003e, arXiv:1208.4032 [math.NT], 2012-2015.",
				"Anitha Srinivasan, \u003ca href=\"http://dx.doi.org/10.5802/jtnb.701\"\u003eMarkoff numbers and ambiguous classes\u003c/a\u003e, Journal de théorie des nombres de Bordeaux, 21 no. 3 (2009), pp. 757-770.",
				"Anitha Srinivasan, \u003ca href=\"https://www.fq.math.ca/Papers1/58-5/srinivasan.pdf\"\u003eThe Markoff-Fibonacci Numbers\u003c/a\u003e, Fibonacci Quart., Vol. 58, No. 5 (2020), pp. 222-228.",
				"Michel Waldschmidt, \u003ca href=\"https://arxiv.org/abs/math/0312440\"\u003eOpen Diophantine problems\u003c/a\u003e, arXiv:math/0312440 [math.NT], 2003-2004.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"https://mathworld.wolfram.com/MarkovNumber.html\"\u003eMarkov Number\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Markov_number\"\u003eMarkov number\u003c/a\u003e.",
				"Don Zagier, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1982-0669663-7\"\u003e On the number of Markoff numbers below a given bound\u003c/a\u003e, Mathematics of Computation, Vol. 39, No. 160 (1982), pp. 709-723.",
				"Ying Zhang, \u003ca href=\"https://arxiv.org/abs/math/0606283\"\u003eAn Elementary Proof of Markoff Conjecture for Prime Powers\u003c/a\u003e, arXiv:math/0606283 [math.NT], 2006-2007.",
				"Ying Zhang, \u003ca href=\"http://dx.doi.org/10.4064/aa128-3-7\"\u003eCongruence and uniqueness of certain Markov numbers\u003c/a\u003e, Acta Arithmetica, Vol. 128 (2007), pp. 295-301."
			],
			"mathematica": [
				"m = {1}; Do[x = m[[i]]; y = m[[j]]; a = (3*x*y + Sqrt[ -4*x^2 - 4*y^2 + 9*x^2*y^2])/2; b = (3*x*y + Sqrt[ -4*x^2 - 4*y^2 + 9*x^2*y^2])/2; If[ IntegerQ[a], m = Union[ Join[m, {a}]]]; If[ IntegerQ[b], m = Union[ Join[m, {b}]]], {n, 8}, {i, Length[m]}, {j, i}]; Take[m, 40] (* _Robert G. Wilson v_, Oct 05 2005 *)",
				"terms = 40; depth0 = 10; Clear[ft]; ft[n_] := ft[n] = Module[{f}, f[] = {1, 2, 5}; f[ud___, u(*up*)] := f[ud, u] = Module[{g = f[ud]}, {g[[1]], g[[3]], 3*g[[1]]*g[[3]] - g[[2]]}]; f[ud___, d(*down*)] := f[ud, d] = Module[{g = f[ud]}, {g[[2]], g[[3]], 3*g[[2]]*g[[3]] - g[[1]]}]; f @@@ Tuples[{u, d}, n] // Flatten // Union // PadRight[#, terms]\u0026]; ft[n = depth0]; ft[n++]; While[ft[n] != ft[n - 1], n++]; Print[\"depth = n = \", n]; A002559 = ft[n] (* _Jean-François Alcover_, Aug 29 2017 *)"
			],
			"xref": [
				"Cf. A178444, A256395."
			],
			"keyword": "nonn,nice,easy",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_ and _J. H. Conway_",
			"ext": [
				"Name clarified by _Wolfdieter Lang_, Jan 22 2015"
			],
			"references": 43,
			"revision": 190,
			"time": "2021-06-10T05:23:57-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}