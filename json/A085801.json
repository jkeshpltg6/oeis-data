{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A085801",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 85801,
			"data": "1,1,1,2,5,4,7,6,7,9,11,10,13,13,13,14,17,16,19,18,19,21,23,22,25,25,25,26,29,28,31,30,31,33,35,34,37,37,37,38,41,40,43,42,43,45,47,46,49,49,49,50,53,52,55,54,55,57,59,58,61,61,61,62,65,64,67,66",
			"name": "Maximum number of nonattacking queens on an n X n toroidal board.",
			"comment": [
				"Independence number of the queens' graph on toroidal n X n board. - _Andrey Zabolotskiy_, Dec 11 2016"
			],
			"reference": [
				"G. Polya: Über die 'Doppelt-Periodischen' Loesungen des n-Damen-Problems, in: W. Ahrens: Mathematische Unterhaltungen und Spiele, Teubner, Leipzig, 1918, 364-374. Reprinted in: G. Polya: Collected Works, Vol. V, 237-247."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A085801/b085801.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Grant Cairns, \u003ca href=\"http://www.combinatorics.org/Volume_8/Abstracts/v8i1n6.html\"\u003eQueens on Non-square Tori\u003c/a\u003e, El. J. Combinatorics, N6, 2001",
				"Eldar Fischer, Tomer Kotek, and Johann A. Makowsky, \u003ca href=\"http://www.cs.technion.ac.il/~tkotek/pubfiles/12-FKM-final.pdf\"\u003eApplication of Logic to Combinatorial Sequences and Their Recurrence Relations\u003c/a\u003e",
				"V. Kotesovec, \u003ca href=\"https://oeis.org/wiki/User:Vaclav_Kotesovec\"\u003eNon-attacking chess pieces\u003c/a\u003e, 6ed, 2013, p. 751.",
				"P. Monsky, \u003ca href=\"http://www.jstor.org/stable/2325220\"\u003eProblem E3162\u003c/a\u003e, Amer. Math. Monthly 96 (1989), 258-259.",
				"Konrad Schlude and Ernst Specker, \u003ca href=\"https://doi.org/10.3929/ethz-a-006666110\"\u003eZum Problem der Damen auf dem Torus\u003c/a\u003e, Technical Report 412, Computer Science Department ETH Zurich, 2003."
			],
			"formula": [
				"G.f.: (2*x^12 - x^11 + 2*x^10 + 2*x^9 + x^8 - x^7 + 3*x^6 - x^5 + 3*x^4 + x^3 + 1)/(x^13 - x^12 - x + 1) = (2*x^12 - x^11 + 2*x^10 + 2*x^9 + x^8 - x^7 + 3*x^6 - x^5 + 3*x^4 + x^3 + 1)/((x - 1)^2*(x + 1)*(x^2 + 1)*(x^2 - x + 1)*(x^2 + x + 1)*(x^4 - x^2 + 1)). - _Joerg Arndt_, Dec 13 2010",
				"From _Andrey Zabolotskiy_, Dec 11 2016: (Start)",
				"a(n) = n if n = 1, 5, 7, 11 (mod 12);",
				"a(n) = n-1 if n = 2, 10 (mod 12);",
				"a(n) = n-2 otherwise.",
				"(End)"
			],
			"example": [
				"Four non-attacking queens can be placed on a 6 X 6 toroidal board:",
				"......",
				"..Q...",
				"....Q.",
				".Q....",
				"...Q..",
				"......",
				"But five queens cannot. Hence a(6) = 4."
			],
			"mathematica": [
				"(* Explicit formula, based on an article by Monsky: *)",
				"Table[n-1/6*(2*Cos[Pi*n/2]-3*Cos[Pi*n/3]+5*Cos[2*Pi*n/3]-Cos[Pi*n/6]-Cos[5*Pi*n/6]+3*Cos[Pi*n]+7),{n,1,100}] (* _Vaclav Kotesovec_, Dec 13 2010 *)"
			],
			"program": [
				"(PARI) a(n)=n-1/6*(2*cos(Pi*n/2)-3*cos(Pi*n/3)+5*cos(2*Pi*n/3)-cos(Pi*n/6)-cos(5*Pi*n/6)+3*cos(Pi*n)+7);",
				"vector(60,n,round(a(n))) \\\\ _Joerg Arndt_, Dec 13 2010"
			],
			"xref": [
				"Cf. A051906, A007705, A279402, A279404, A279405, A172517, A172518, A172519, A173775, A178722."
			],
			"keyword": "easy,nonn",
			"offset": "1,4",
			"author": "_Konrad Schlude_, Jul 24 2003",
			"references": 9,
			"revision": 48,
			"time": "2018-01-01T13:17:55-05:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}