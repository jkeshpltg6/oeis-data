{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A289741",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 289741,
			"data": "0,1,0,1,0,0,0,1,0,1,0,-1,0,-1,0,0,0,-1,0,-1,0,1,0,1,0,0,0,1,0,1,0,-1,0,-1,0,0,0,-1,0,-1,0,1,0,1,0,0,0,1,0,1,0,-1,0,-1,0,0,0,-1,0,-1,0,1,0,1,0,0,0,1,0,1,0,-1,0,-1,0,0,0,-1,0,-1,0",
			"name": "a(n) = Kronecker symbol (-20/n).",
			"comment": [
				"Period 20: repeat [0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, -1, 0, -1, 0, 0, 0, -1, 0, -1].",
				"This sequence is one of the three non-principal real Dirichlet characters modulo 20. The other two are Jacobi or Kronecker symbols {(20/n)} (or {(n/20)}) and {((-100)/n)} (A185276).",
				"Note that (Sum_{i=0..19} i*a(i))/(-20) = 2 gives the class number of the imaginary quadratic field Q(sqrt(-5)). The fact Q(sqrt(-5)) has class number 2 implies that Q(sqrt(-5)) is not a unique factorization domain."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A289741/b289741.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/KroneckerSymbol.html\"\u003eKronecker Symbol\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_10\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,0,0,0,0,0,0,0,0,-1)."
			],
			"formula": [
				"a(n) = 1 for n in A045797; -1 for n in A045798; 0 for n that are not coprime with 20.",
				"Completely multiplicative with a(p) = a(p mod 20) for primes p.",
				"a(n) = A080891(n)*A101455(n).",
				"a(n) = -a(n+10) = -a(-n) for all n in Z."
			],
			"mathematica": [
				"Array[KroneckerSymbol[-20, #]\u0026, 100, 0] (* _Amiram Eldar_, Jan 10 2019 *)"
			],
			"program": [
				"(PARI) a(n) = kronecker(-20, n)"
			],
			"xref": [
				"Cf. A035170 (inverse Moebius transform).",
				"Kronecker symbols {(d/n)} where d is a fundamental discriminant with |d| \u003c= 24: A109017 (d=-24), A011586 (d=-23), this sequence (d=-20), A011585 (d=-19), A316569 (d=-15), A011582 (d=-11), A188510 (d=-8), A175629 (d=-7), A101455 (d=-4), A102283 (d=-3), A080891 (d=5), A091337 (d=8), A110161 (d=12), A011583 (d=13), A011584 (d=17), A322829 (d=21), A322796 (d=24).",
				"Cf. A045797, A045798, A185276."
			],
			"keyword": "sign,mult,easy",
			"offset": "0,1",
			"author": "_Jianing Song_, Dec 27 2018",
			"references": 7,
			"revision": 49,
			"time": "2021-11-30T13:26:54-05:00",
			"created": "2019-01-10T08:30:05-05:00"
		}
	]
}