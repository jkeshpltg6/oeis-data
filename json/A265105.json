{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A265105",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 265105,
			"data": "1,2,3,1,4,1,2,5,1,2,2,1,6,1,2,2,3,0,2,7,1,2,2,3,2,2,0,2,1,8,1,2,2,3,2,4,0,2,1,2,0,2,9,1,2,2,3,2,4,2,2,1,2,0,4,0,0,2,1,10,1,2,2,3,2,4,2,4,1,2,0,4,0,2,2,1,0,2,0,2",
			"name": "Triangle T(n,k) of coefficients of q^k in LB_n(12/3), set partitions that avoid 12/3 with lb=k. Related to a restricted divisor function.",
			"comment": [
				"See Dahlberg et al. reference for definition of avoidance and lb."
			],
			"link": [
				"Jinyuan Wang, \u003ca href=\"/A265105/b265105.txt\"\u003eRows n = 1..50 of triangle, flattened\u003c/a\u003e",
				"S. Dahlberg, R. Dorward, J. Gerhard, T. Grubb, C. Purcell, L. Reppuhn, B. E. Sagan, \u003ca href=\"https://arxiv.org/abs/1502.00056\"\u003eSet partition patterns and statistics\u003c/a\u003e, arXiv:1502.00056 [math.CO], 2015.",
				"S. Dahlberg, R. Dorward, J. Gerhard, T. Grubb, C. Purcell, L. Reppuhn, B. E. Sagan, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2015.07.001\"\u003eSet partition patterns and statistics\u003c/a\u003e, Discrete Math., 339 (1): 1-16, 2016."
			],
			"formula": [
				"T(n,k) = #{d\u003e=1: d | k and d+(k/d)+1\u003c=n} + delta_{k,0}, where delta is the Kronecker delta function.",
				"Formula for generating function, fixing n: 1 + sum 1\u003c=m\u003c=n-1, sum 1\u003c=i\u003c=m, q^((n-m)(m-i)).",
				"When k\u003c=n-2, T(n,k) = A000005(k)."
			],
			"example": [
				"Triangle begins:",
				"1,",
				"2,",
				"3,1,",
				"4,1,2,",
				"5,1,2,2,1,",
				"6,1,2,2,3,0,2,",
				"7,1,2,2,3,2,2,0,2,1,",
				"8,1,2,2,3,2,4,0,2,1,2,0,2,",
				"9,1,2,2,3,2,4,2,2,1,2,0,4,0,0,2,1"
			],
			"mathematica": [
				"row[n_] := CoefficientList[1 + Sum[q^((n-m)(m-i)), {m, n-1}, {i, m}], q];",
				"Array[row, 10] // Flatten (* _Jean-François Alcover_, Sep 26 2018 *)"
			],
			"program": [
				"(PARI) T(n, k) = if (k==0, n, sumdiv(k, d, (d\u003e=1) \u0026\u0026 (d+(k/d)+1)\u003c=n));",
				"tabf(nn) = {for (n=1, nn, for (k=0, (n-1)^2\\4, print1(T(n, k), \", \");); print(););} \\\\ _Michel Marcus_, Apr 07 2016"
			],
			"xref": [
				"First column is A000027.",
				"Cf. A000005.",
				"Row sum is A000124.",
				"Row length (fixing n, degree of polynomial in k) is A002620."
			],
			"keyword": "nonn,tabf",
			"offset": "1,2",
			"author": "_Robert Dorward_, Apr 06 2016",
			"ext": [
				"More terms from _Jinyuan Wang_, Mar 06 2020"
			],
			"references": 1,
			"revision": 33,
			"time": "2020-03-06T08:10:20-05:00",
			"created": "2016-04-09T16:21:20-04:00"
		}
	]
}