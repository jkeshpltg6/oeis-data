{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A097834",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 97834,
			"data": "1,28,755,20357,548884,14799511,399037913,10759224140,290100013867,7821941150269,210902311043396,5686540457021423,153325690028535025,4134107090313424252,111467565748433919779,3005490168117402409781",
			"name": "Chebyshev polynomials S(n,27) + S(n-1,27) with Diophantine property.",
			"comment": [
				"(5*a(n))^2 - 29*b(n)^2 = -4 with b(n)=A097835(n) give all positive solutions of this Pell equation."
			],
			"link": [
				"Indranil Ghosh, \u003ca href=\"/A097834/b097834.txt\"\u003eTable of n, a(n) for n = 0..697\u003c/a\u003e (terms 0..200 from Vincenzo Librandi)",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"Giovanni Lucca, \u003ca href=\"http://forumgeom.fau.edu/FG2019volume19/FG201902index.html\"\u003eInteger Sequences and Circle Chains Inside a Hyperbola\u003c/a\u003e, Forum Geometricorum (2019) Vol. 19, 11-16.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (27,-1).",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e"
			],
			"formula": [
				"a(n) = S(n, 27) + S(n-1, 27) = S(2*n, sqrt(29)), with S(n, x)=U(n, x/2) Chebyshev's polynomials of the second kind, A049310. S(-1, x) = 0 = U(-1, x). S(n, 27)=A097781(n).",
				"a(n) = (-2/5)*i*((-1)^n)*T(2*n+1, 5*i/2) with the imaginary unit i and Chebyshev's polynomials of the first kind. See the T-triangle A053120.",
				"G.f.: (1+x)/(1-27*x+x^2).",
				"a(n) = - a(-1-n) for all n in Z. - _Michael Somos_, Nov 01 2008"
			],
			"example": [
				"All positive solutions of Pell equation x^2 - 29*y^2 = -4 are",
				"(5=5*1,1), (140=5*28,26), (3775=5*755,701), (101785=5*20357,18901), ..."
			],
			"mathematica": [
				"a[n_] := -2/5*I*(-1)^n*ChebyshevT[2*n + 1, 5*I/2]; Table[a[n], {n, 0, 15}] (* _Jean-François Alcover_, Jun 21 2013, from 2nd formula *)"
			],
			"program": [
				"(PARI) {a(n) = (-1)^n * subst(2 * I / 5 * poltchebi(2*n + 1), 'x, -5/2 * I)}; /* _Michael Somos_, Nov 04 2008 */"
			],
			"xref": [
				"A087130(2*n + 1) = 5 * a(n). - _Michael Somos_, Nov 01 2008"
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Sep 10 2004",
			"references": 3,
			"revision": 37,
			"time": "2020-01-23T03:45:59-05:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}