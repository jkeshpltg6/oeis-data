{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002700",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2700,
			"id": "M3147 N1275",
			"data": "3,40,336,2304,14080,79872,430080,2228224,11206656,55050240,265289728,1258291200,5888802816,27246198784,124822487040,566935683072,2555505541120,11441792876544,50921132261376,225399883694080,992858999881728,4354066045992960",
			"name": "Coefficients of Chebyshev polynomials: n*(2*n+1) * 4^(n-1).",
			"reference": [
				"C. Lanczos, Applied Analysis. Prentice-Hall, Englewood Cliffs, NJ, 1956, p. 518.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A002700/b002700.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"C. Lanczos, \u003ca href=\"/A002457/a002457.pdf\"\u003eApplied Analysis\u003c/a\u003e (Annotated scans of selected pages)",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (12,-48,64)."
			],
			"formula": [
				"a(n) = 12*a(n-1) - 48*a(n-2) + 64*a(n-3). - _Colin Barker_, Jun 15 2015",
				"a(n) = 1/2*Sum_{k = 0..2*n} k^2*binomial(2*n,k). Cf. A002699. - _Peter Bala_, Apr 09 2017"
			],
			"maple": [
				"A002700:=-(3+4*z)/(4*z-1)**3; # _Simon Plouffe_ in his 1992 dissertation."
			],
			"mathematica": [
				"Table[n*(2*n+1)*2^(2*n-2),{n,1,30}] (* _Vaclav Kotesovec_, Jun 03 2014 *)",
				"LinearRecurrence[{12,-48,64},{3,40,336},30] (* _Harvey P. Dale_, May 17 2018 *)"
			],
			"program": [
				"(PARI) Vec(-x*(4*x+3)/(4*x-1)^3 + O(x^30)) \\\\ _Colin Barker_, Jun 15 2015",
				"(MAGMA) [4^(n-1)*n*(2*n+1): n in [1..30]]; // _G. C. Greubel_, Jul 23 2019",
				"(Sage) [4^(n-1)*n*(2*n+1) for n in (1..30)] # _G. C. Greubel_, Jul 23 2019",
				"(GAP) List([1..30], n-\u003e 4^(n-1)*n*(2*n+1)); # _G. C. Greubel_, Jul 23 2019"
			],
			"xref": [
				"Cf. A002699."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_",
			"references": 6,
			"revision": 44,
			"time": "2021-03-12T22:32:35-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}