{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A276064",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 276064,
			"data": "1,1,1,1,1,2,1,2,2,2,1,4,2,4,2,6,3,8,3,8,4,4,12,4,4,10,12,6,16,12,5,16,24,8,24,28,6,26,40,8,10,36,52,8,8,40,60,32,13,56,84,32,11,58,96,80,17,84,136,88,15,80,160,160,16,23,120,220,192,16",
			"name": "Triangle read by rows: T(n,k) is the number of compositions of n with parts in {1,5} and having asymmetry degree equal to k (n\u003e=0; 0\u003c=k\u003c=floor(n/6)).",
			"comment": [
				"The asymmetry degree of a finite sequence of numbers is defined to be the number of pairs of symmetrically positioned distinct entries. Example: the asymmetry degree of (2,7,6,4,5,7,3) is 2, counting the pairs (2,3) and (6,5).",
				"Number of entries in row n is 1 + floor(n/6).",
				"Sum of entries in row n is A003520(n).",
				"T(n,0) = A226516(n+11).",
				"Sum_{k\u003e=0} k*T(n,k) = A276065(n)."
			],
			"reference": [
				"S. Heubach and T. Mansour, Combinatorics of Compositions and Words, CRC Press, 2010."
			],
			"link": [
				"Krithnaswami Alladi and V. E. Hoggatt, Jr. \u003ca href=\"http://www.fq.math.ca/Scanned/13-3/alladi1.pdf\"\u003eCompositions with Ones and Twos\u003c/a\u003e, Fibonacci Quarterly, 13 (1975), 233-239.",
				"V. E. Hoggatt, Jr., and Marjorie Bicknell, \u003ca href=\"http://www.fq.math.ca/Scanned/13-4/hoggatt1.pdf\"\u003ePalindromic compositions\u003c/a\u003e, Fibonacci Quart., Vol. 13(4), 1975, pp. 350-356."
			],
			"formula": [
				"G.f.: G(t,z) = (1+z+z^5)/(1-z^2-2tz^6-z^10). In the more general situation of compositions into a[1]\u003ca[2]\u003ca[3]\u003c..., denoting F(z) = Sum_{j\u003e=1} z^{a[j]}, we have G(t,z) =(1 + F(z))/(1 - F(z^2) - t(F(z)^2 - F(z^2))). In particular, for t=0 we obtain Theorem 1.2 of the Hoggatt et al. reference."
			],
			"example": [
				"Row 8 is [1,4] because the compositions of 8 with parts in {1,5} are 5111, 1511, 1151, 1115 and 11111111, having asymmetry degrees 1,1,1,1, and 0, respectively.",
				"Triangle starts:",
				"1;",
				"1;",
				"1;",
				"1;",
				"1;",
				"2;",
				"1, 2;",
				"2, 2."
			],
			"maple": [
				"G := (1+z+z^5)/(1-z^2-2*t*z^6-z^10): Gser := simplify(series(G, z = 0, 30)): for n from 0 to 25 do P[n] := sort(coeff(Gser, z, n)) end do: for n from 0 to 25 do seq(coeff(P[n], t, j), j = 0 .. degree(P[n])) end do; # yields sequence in triangular form"
			],
			"mathematica": [
				"Table[TakeWhile[BinCounts[#, {0, 1 + Floor[n/4], 1}], # != 0 \u0026] \u0026@ Map[Total, Map[Map[Boole[# \u003e= 1] \u0026, BitXor[Take[# - 1, Ceiling[Length[#]/2]], Reverse@ Take[# - 1, -Ceiling[Length[#]/2]]]] \u0026, Flatten[Map[Permutations, DeleteCases[IntegerPartitions@ n, {___, a_, ___} /; Nor[a == 1, a == 5]]], 1]]], {n, 0, 25}] // Flatten (* _Michael De Vlieger_, Aug 22 2016 *)"
			],
			"xref": [
				"Cf. A003520, A226516, A276065."
			],
			"keyword": "nonn,tabf",
			"offset": "0,6",
			"author": "_Emeric Deutsch_, Aug 22 2016",
			"references": 1,
			"revision": 12,
			"time": "2016-08-23T04:46:20-04:00",
			"created": "2016-08-23T04:46:20-04:00"
		}
	]
}