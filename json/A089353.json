{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A089353",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 89353,
			"data": "1,2,1,3,2,1,4,6,2,1,5,10,6,2,1,6,19,14,6,2,1,7,28,28,14,6,2,1,8,44,52,33,14,6,2,1,9,60,93,64,33,14,6,2,1,10,85,152,127,70,33,14,6,2,1,11,110,242,228,142,70,33,14,6,2,1,12,146,370,404,272,149,70,33,14,6,2,1,13",
			"name": "Triangle read by rows: T(n,m) = number of planar partitions of n with trace m.",
			"comment": [
				"Also number of partitions of n objects of 2 colors into k parts, each part containing at least one black object.",
				"T(n+m, m) = A005380(n), n \u003e= 1, for all m \u003e= n.  T(m, m) = 1 for m \u003e= 1. See the Stanley reference Exercise 7.99. With offset n=0 a column for m=0 with the only non-vanishing entry T(0, 0) = 1 could be added. - _Wolfdieter Lang_, Mar 09 2015"
			],
			"reference": [
				"G. E. Andrews, The Theory of Partitions, Addison-Wesley, 1976 (Ch. 11, Example 5 and Ch. 12, Example 5).",
				"R. P. Stanley, Enumerative Combinatorics, Cambridge University Press, Vol. 2, 1999; p. 365 and Exercise 7.99, p. 484 and pp. 548-549."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A089353/b089353.txt\"\u003eRows n = 1..200, flattened\u003c/a\u003e"
			],
			"formula": [
				"G.f.: Product_(k\u003e=1} 1/(1-q x^k)^k (with offset n=0 in x powers)."
			],
			"example": [
				"The triangle T(n,m) begins:",
				"n\\m  1   2   3   4   5   6  7  8  9 10 11 12 ...",
				"1:   1",
				"2:   2   1",
				"3:   3   2   1",
				"4:   4   6   2   1",
				"5:   5  10   6   2   1",
				"6:   6  19  14   6   2   1",
				"7:   7  28  28  14   6   2  1",
				"8:   8  44  52  33  14   6  2  1",
				"9:   9  60  93  64  33  14  6  2  1",
				"10: 10  85 152 127  70  33 14  6  2  1",
				"11: 11 110 242 228 142  70 33 14  6  2  1",
				"12: 12 146 370 404 272 149 70 33 14  6  2  1",
				"... reformatted, _Wolfdieter Lang_, Mar 09 2015"
			],
			"maple": [
				"b:= proc(n, i) option remember; expand(`if`(n=0, 1,",
				"      `if`(i\u003c1, 0, add(b(n-i*j, i-1)*x^j*",
				"       binomial(i+j-1, j), j=0..n/i))))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=1..degree(p)))(b(n$2)):",
				"seq(T(n), n=1..12);  # _Alois P. Heinz_, Apr 13 2017"
			],
			"mathematica": [
				"b[n_, i_] := b[n, i] = Expand[If[n == 0, 1, If[i \u003c 1, 0, Sum[b[n - i*j, i - 1]*x^j*Binomial[i + j - 1, j], {j, 0, n/i}]]]];",
				"T[n_] := Table[Coefficient[#, x, i], {i, 1, Exponent[#, x]}]]\u0026 @ b[n, n];",
				"Table[T[n], {n, 1, 12}] // Flatten (* _Jean-François Alcover_, May 19 2018, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A000219 (row sums), A005380, A005993 (trace 2), A050531 (trace 3), A089351 (trace 4)."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Wouter Meeussen_ and _Vladeta Jovovic_, Dec 26 2003",
			"ext": [
				"Edited by _Christian G. Bower_, Jan 08 2004"
			],
			"references": 7,
			"revision": 25,
			"time": "2018-05-19T12:35:48-04:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}