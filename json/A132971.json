{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A132971",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 132971,
			"data": "1,-1,-1,0,-1,1,0,0,-1,1,1,0,0,0,0,0,-1,1,1,0,1,-1,0,0,0,0,0,0,0,0,0,0,-1,1,1,0,1,-1,0,0,1,-1,-1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,-1,1,1,0,1,-1,0,0,1,-1,-1,0,0,0,0,0,1,-1",
			"name": "a(2*n) = a(n), a(4*n+1) = -a(n), a(4*n+3) = 0, with a(0) = 1.",
			"comment": [
				"If binary(n) has adjacent 1 bits then a(n) = 0 else a(n) = (-1)^A000120(n).",
				"Fibbinary numbers (A003714) gives the numbers n for which a(n) = A106400(n). - _Antti Karttunen_, May 30 2017"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A132971/b132971.txt\"\u003eTable of n, a(n) for n = 0..10922\u003c/a\u003e",
				"Paul Tarau, \u003ca href=\"https://doi.org/10.1007/978-3-642-23283-1_15\"\u003eEmulating Primality with Multiset Representations of Natural Numbers\u003c/a\u003e, in Theoretical Aspects of Computing, ICTAC 2011, Lecture Notes in Computer Science, 2011, Volume 6916/2011, 218-238, DOI: 10.1007/978-3-642-23283-1_15.",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e"
			],
			"formula": [
				"A024490(n) = number of solutions to 2^n \u003c= k \u003c 2^(n+1) and a(k) = 1.",
				"A005252(n) = number of solutions to 2^n \u003c= k \u003c 2^(n+1) and a(k) = -1.",
				"A027935(n-1) = number of solutions to 2^n \u003c= k \u003c 2^(n+1) and a(k) = 0.",
				"G.f. A(x) satisfies A(x) = A(x^2) - x * A(x^4).",
				"G.f. B(x) of A000621 satisfies B(x) = x * A(x^2) / A(x).",
				"a(n) = A008683(A005940(1+n)). [Analogous to Moebius mu] - _Antti Karttunen_, May 30 2017"
			],
			"example": [
				"G.f. = 1 - x - x^2 - x^4 + x^5 - x^8 + x^9 + x^10 - x^16 + x^17 + x^18 + ..."
			],
			"mathematica": [
				"m = 100; A[_] = 1;",
				"Do[A[x_] = A[x^2] - x A[x^4] + O[x]^m // Normal, {m}];",
				"CoefficientList[A[x], x] (* _Jean-François Alcover_, Nov 16 2019 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, n==0, if( n%2, if( n%4 \u003e 1, 0, -a((n-1)/4) ), a(n/2) ) )};",
				"(PARI) {a(n) = my(A, m); if( n\u003c0, 0, m = 1; A = 1 + O(x); while( m\u003c=n, m *= 2; A = subst(A, x, x^2) - x * subst(A, x, x^4) ); polcoeff(A, n)) };",
				"(Scheme) (define (A132971 n) (cond ((zero? n) 1) ((even? n) (A132971 (/ n 2))) ((= 1 (modulo n 4)) (- (A132971 (/ (- n 1) 4)))) (else 0))) ;; _Antti Karttunen_, May 30 2017",
				"(Python)",
				"from sympy import mobius, prime, log",
				"import math",
				"def A(n): return n - 2**int(math.floor(log(n, 2)))",
				"def b(n): return n + 1 if n\u003c2 else prime(1 + (len(bin(n)[2:]) - bin(n)[2:].count(\"1\"))) * b(A(n))",
				"def a(n): return mobius(b(n)) # _Indranil Ghosh_, May 30 2017"
			],
			"xref": [
				"Cf. A000120, A000621, A003714, A005252, A005940, A008683, A024490, A027935, A106400.",
				"Cf. A085357 (gives the absolute values: -1 -\u003e 1), A286576 (when reduced modulo 3: -1 -\u003e 2)."
			],
			"keyword": "sign",
			"offset": "0,1",
			"author": "_Michael Somos_, Sep 17 2007, Sep 19 2007",
			"references": 8,
			"revision": 26,
			"time": "2019-11-16T02:59:32-05:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}