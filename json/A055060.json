{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A055060",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 55060,
			"data": "1,7,8,7,2,3,1,6,5,0,1,8,2,9,6,5,9,3,3,0,1,3,2,7,4,8,9,0,3,3,7,0,0,8,3,8,5,3,3,7,9,3,1,4,0,2,9,6,1,8,1,0,9,9,7,7,8,4,7,8,1,4,7,0,5,0,5,5,5,7,4,9,1,7,5,0,6,0,5,6,8,6,9,9,1,3,1,0,0,1,8,6,3,4,0,7,5,3,3,3,0,2",
			"name": "Decimal expansion of Komornik-Loreti constant.",
			"reference": [
				"Steven R. Finch, Mathematical Constants, Cambridge University Press, 2003, pp. 438-439."
			],
			"link": [
				"Pieter Allaart, and Derong Kong, \u003ca href=\"https://arxiv.org/abs/2006.07927\"\u003eOn the smallest base in which a number has a unique expansion\u003c/a\u003e, arXiv:2006.07927 [math.NT], 2020.",
				"Jean-Paul Allouche and Michel Cosnard, \u003ca href=\"http://www.jstor.org/stable/2695302\"\u003eThe Komornik-Loreti constant is transcendental\u003c/a\u003e, Amer. Math. Monthly, Vol. 107, No. 5 (May, 2000), pp. 448-449, \u003ca href=\"http://www.math.jussieu.fr/~allouche/bibliorecente.html\"\u003epreprint\u003c/a\u003e.",
				"Vilmos Komornik and Paola Loreti, \u003ca href=\"http://www.jstor.org/stable/2589246\"\u003eUnique Developments in Non-Integer Bases\u003c/a\u003e, Amer. Math. Monthly, Vol. 105, No. 7 (Aug. - Sep., 1998), pp. 636-639.",
				"Vilmos Komornik, Derong Kong, \u003ca href=\"https://arxiv.org/abs/1705.00473\"\u003eBases with two expansions\u003c/a\u003e, arXiv:1705.00473 [math.NT], 2017.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Komornik-LoretiConstant.html\"\u003eKomornik-Loreti Constant\u003c/a\u003e.",
				"\u003ca href=\"/index/Tra#transcendental\"\u003eIndex entries for transcendental numbers\u003c/a\u003e"
			],
			"formula": [
				"This number q (say) is defined by 1 = Sum_{n \u003e= 1} A010060(n)/q^n.",
				"The unique positive solution of the equation Product_{k\u003e=0} (1 - 1/q^(2^k)) = (2-q)/(q-1) (Allouche and Cosnard, 2000). - _Amiram Eldar_, Oct 22 2020"
			],
			"example": [
				"1.787231650..."
			],
			"mathematica": [
				"First[ RealDigits[q /. FindRoot[ Sum[ Mod[ DigitCount[n, 2, 1], 2]/q^n, {n, 1, 2000}] == 1, {q, 1.8}, WorkingPrecision -\u003e 120], 10, 102]](* _Jean-François Alcover_, Jun 11 2012, after PARI *)"
			],
			"program": [
				"(PARI) solve(q=1.7,1.8,sum(n=1,2000,(subst(Pol(binary(n)),x,1)%2)/q^n)-1)"
			],
			"xref": [
				"The continued-fraction expansion of this number is in A080890.",
				"Cf. A010060."
			],
			"keyword": "nonn,cons,easy",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, Jun 11 2000",
			"ext": [
				"More terms from _Ralf Stephan_, Mar 30 2003"
			],
			"references": 3,
			"revision": 32,
			"time": "2020-10-22T04:44:59-04:00",
			"created": "2000-06-15T03:00:00-04:00"
		}
	]
}