{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A326409",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 326409,
			"data": "2,-1,-1,3,-1,3,-1,3,4,2,-1,3,-1,3,3,2,-1,4,-1,2,2,1,-1,2,3,1,1,2,-1,3,-1,3,3,2,3,2,-1,1,2,2,-1,2,-1,2,2,2,-1,1,1,0,1,2,-1,2,3,1,2,2,-1,2,-1,1,1,1,1,2,-1,1,2,1,-1,3,-1,2,2,1,2,3,-1,1",
			"name": "Minesweeper sequence of positive integers arranged on a 2D grid along Hamiltonian path.",
			"comment": [
				"Place positive integers on a 2D grid starting with 1 in the top left corner and continue along Hamiltonian path A163361 or A163363.",
				"Replace each prime with -1 and each nonprime by the number of primes in adjacent grid cells around it.",
				"n is replaced by a(n).",
				"This sequence treats prime numbers as \"mines\" and fills gaps according to rules of the classic Minesweeper game.",
				"a(n) \u003c 5.",
				"Set of n such that a(n) = 4 is unbounded (conjectured)."
			],
			"link": [
				"Alexander Bogomolny, \u003ca href=\"https://www.cut-the-knot.org/Curriculum/Geometry/PlaneFillingCurves.shtml\"\u003ePlane Filling Curves: Hilbert's \u0026 Moore's\u003c/a\u003e, Cut the Knot.org, retrieved October 2019.",
				"Eric W. Weisstein, \u003ca href=\"http://mathworld.wolfram.com/HilbertCurve.html\"\u003eMathWorld: Hilbert Curve\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Hilbert_curve\"\u003eHilbert Curve\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Minesweeper_(video_game)\"\u003eMinesweeper game\u003c/a\u003e."
			],
			"example": [
				"Consider positive integers distributed onto the plane along an increasing Hamiltonian path (in this case it starts downwards):",
				".",
				"   1   4---5---6  59--60--61  64--...",
				"   |   |       |   |       |   |",
				"   2---3   8---7  58--57  62--63",
				"           |           |",
				"  15--14   9--10  55--56  51--50",
				"   |   |       |   |       |   |",
				"  16  13--12--11  54--53--52  49",
				"   |                           |",
				"  17--18  31--32--33--34  47--48",
				"       |   |           |   |",
				"  20--19  30--29  36--35  46--45",
				"   |           |   |           |",
				"  21  24--25  28  37  40--41  44",
				"   |   |   |   |   |   |   |   |",
				"  22--23  26--27  38--39  42--43",
				".",
				"1 is not prime and in adjacent grid cells there are 2 primes: 2 and 3. Therefore a(1) = 2.",
				"2 is prime, therefore a(2) = -1.",
				"8 is not prime and in adjacent grid cells there are 3 primes: 5, 3 and 7. Therefore a(8) = 3.",
				"Replacing n with a(n) in the plane described above, and using \".\" for a(n) = 0 and \"*\" for negative a(n), we produce a graph resembling Minesweeper, where the mines are situated at prime n:",
				"  2   3---*---3   *---2---*   1 ...",
				"  |   |       |   |       |   |",
				"  *---*   3---*   2---2   1---1",
				"          |           |",
				"  3---3   4---2   3---1   1---.",
				"  |   |       |   |       |   |",
				"  2   *---3---*   2---*---2   1",
				"  |                           |",
				"  *---4   *---3---3---2   *---1",
				"      |   |           |   |",
				"  2---*   3---*   2---3   2---2",
				"  |           |   |           |",
				"  2   2---3   2   *   2---*   2",
				"  |   |   |   |   |   |   |   |",
				"  1---*   1---1   1---2   2---*",
				"In order to produce the sequence, the graph is read along its original mapping."
			],
			"mathematica": [
				"Block[{nn = 4, s, t, u}, s = ConstantArray[0, {2^#, 2^#}] \u0026[nn + 1]; t = First[HilbertCurve@ # /. Line -\u003e List] \u0026[nn + 1] \u0026[nn + 1]; s = ArrayPad[ReplacePart[s, Array[{1, 1} + t[[#]] -\u003e # \u0026, 2^(2 (nn + 1))]], {{1, 0}, {1, 0}}]; u = Table[If[PrimeQ@ m, -1, Count[#, _?PrimeQ] \u0026@ Union@ Map[s[[#1, #2]] \u0026 @@ # \u0026, Join @@ Array[FirstPosition[s, m] + {##} - 2 \u0026, {3, 3}]]], {m, (2^nn)^2}]]"
			],
			"xref": [
				"Cf. A163361 (plane mapping), A163363 (alternative plane mapping).",
				"Different arrangements of integers: A326405 (antidiagonals), A326406 (triangle maze), A326407 (square mapping), A326408 (square maze), A326410 (Ulam's spiral)."
			],
			"keyword": "sign,tabl",
			"offset": "1,1",
			"author": "_Witold Tatkiewicz_, Oct 07 2019",
			"references": 6,
			"revision": 21,
			"time": "2020-03-20T23:37:16-04:00",
			"created": "2019-10-23T16:16:01-04:00"
		}
	]
}