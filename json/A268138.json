{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A268138",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 268138,
			"data": "1,5,51,747,13245,264329,5721415,131425079,3159389817,78729848397,2019910325499,53087981674275,1423867359013749,38855956977763857,1076297858301372687,30203970496501504239,857377825323716359665,24586286492003180067989,711463902659879056604995,20756358426519694831851227",
			"name": "a(n) = (Sum_{k=0..n-1} A001850(k)*A001003(k+1))/n.",
			"comment": [
				"Conjecture: (i) All the terms are odd integers. Also, p | a(p) for any odd prime p.",
				"(ii) Let D_n(x) = Sum_{k=0..n} binomial(n,k)*binomial(n+k,k)*x^k = Sum_{k=0..n} binomial(n,k)^2*x^k*(x+1)^(n-k) for n \u003e= 0, and s_n(x) = Sum_{k=1..n} (binomial(n,k)*binomial(n,k-1)/n)*x^(k-1)*(x+1)^(n-k) = (Sum_{k=0..n} binomial(n,k)*binomial(n+k,k)*x^k/(k+1))/(x+1) for n \u003e 0. Then, for any positive integer n, all the coefficients of the polynomial (1/n)*Sum_{k=0..n-1} D_k(x)*s_{k+1}(x) are integral and the polynomial is irreducible over the field of rational numbers.",
				"The conjecture was essentially proved by the author in arXiv:1602.00574, except for the irreducibility of (Sum_{k=0..n-1} D_k(x)*s_{k+1}(x))/n. - _Zhi-Wei Sun_, Feb 01 2016"
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A268138/b268138.txt\"\u003eTable of n, a(n)for n = 1..100\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2011.06.005\"\u003eOn Delannoy numbers and Schroder numbers\u003c/a\u003e, J. Number Theory 131(2011), no.12, 2387-2397.",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1602.00574\"\u003eArithmetic properties of Delannoy numbers and Schröder numbers\u003c/a\u003e, preprint, arXiv:1602.00574 [math.CO], 2016."
			],
			"example": [
				"a(3) = 51 since (A001850(0)*A001003(1) + A001850(1)*A001003(2) + A001850(2)*A001003(3))/3 = (1*1 + 3*3 + 13*11)/3 = 153/3 = 51."
			],
			"mathematica": [
				"d[n_]:=Sum[Binomial[n,k]Binomial[n+k,k],{k,0,n}]",
				"s[n_]:=Sum[Binomial[n,k]Binomial[n,k-1]/n*2^(k-1),{k,1,n}]",
				"a[n_]:=Sum[d[k]s[k+1],{k,0,n-1}]/n",
				"Table[a[n],{n,1,20}]"
			],
			"xref": [
				"Cf. A001003, A001850, A006318, A268136, A268137."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Zhi-Wei Sun_, Jan 26 2016",
			"references": 3,
			"revision": 28,
			"time": "2019-11-03T16:29:46-05:00",
			"created": "2016-01-27T02:58:15-05:00"
		}
	]
}