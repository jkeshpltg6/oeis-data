{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A338264",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 338264,
			"data": "0,0,0,1,0,2,0,0,0,2,0,4,0,0,0,0,2,0,0,1,0,9,0,12,0,2,0,0,6,0,0,0,0,0,0,0,4,0,0,2,0,2,0,0,0,16,0,22,0,0,0,2,0,4,0,0,12,0,0,0,0,0,0,0",
			"name": "Triangle read by rows: T(n,k) is the frequency of occurrence of binary words, under iterations of making the n-bit binary representation of k palindromic, by binary-subtracting its bit reversal, 0 \u003c= k \u003c 2^n.",
			"comment": [
				"This sequence is a side result of the same process used for generating A338203.",
				"All operations are performed with n bits. In particular, the bit-reversal of k considers k to be an n-bit binary word and k is a palindrome if it is equal to its bit reversal in this sense. The subtraction is performed modulo 2^n.",
				"The sequence is divided into blocks of length 2^n, n = 1, 2, 3, ... .",
				"Sequence looks random if only a small amount of data is seen, but shows self-similarity and scale invariance if seen from a broader perspective.",
				"An example is provided in \"links\" section.",
				"T(n,0) is set to zero by definition.",
				"If for any number the operation of subtracting its binary reversed bitsequence is carried out, the result is either 0 or one of the numbers k in the set T(n,k) \u003e 0. Numbers k with bigger values T(n,k) are more likely the result of such operation."
			],
			"link": [
				"Thomas Scheuerle, \u003ca href=\"/A338264/b338264.txt\"\u003eTable of n, a(n) for n = 1..16382. Max: T(13,2^13-1)\u003c/a\u003e",
				"Thomas Scheuerle, \u003ca href=\"/A338264/a338264.png\"\u003eSee here selfsimilarity in graph of first 131070 elements\u003c/a\u003e",
				"Thomas Scheuerle, \u003ca href=\"/A338264/a338264_1.png\"\u003eFast Walsh-Hadamard transform (MATLAB)plot(fwht(a(1:2^16),2^16,'hadamard'))\u003c/a\u003e"
			],
			"example": [
				"Triangle begins:",
				"  0, 0;   Data starts with n=1",
				"  0, 1, 0, 2;",
				"  0, 0, 0, 2, 0, 4, 0, 0;",
				"  0, 0, 2, 0, 0, 1, 0, 9, 0, 12, 0, 2, 0, 0, 6, 0;",
				"  ...",
				"n=4 Four bit wordlength.",
				"For each row of n we let m iterate from 0 to 2^n-1.",
				"For this example let's take a snapshot at m=5 in binary : 0101",
				"0101 - 1010 = 1011 (2^4 + 5 - 10)=11 -\u003e T((2^n-1)+11):=T((2^n-1)+11)+1",
				"(we increment counts for frequency)",
				"1011 - 1101 = 1110 (2^4 + 11 - 13)=14 -\u003e T((2^n-1)+14):=T((2^n-1)+14)+1",
				"1110 - 0111 = 0111 (14 - 7)=7         -\u003e T((2^n-1)+7):=T((2^n-1)+7)+1",
				"0111 - 1110 = 1001 (2^4 + 7 -14)=9    -\u003e T((2^n-1)+9):=T((2^n-1)+9)+1",
				"Now 1001 is palindromic, we stop m=5 and proceed with m=6.",
				"If we did not stop we would reach zero. T(n,0) is zero by definition."
			],
			"program": [
				"(MATLAB)",
				"sequence = [];",
				"for numberofBits = 1:maxNumberofBits",
				"    numbersPerWordlength = 2^numberofBits;",
				"    frequency_list = zeros(1,numbersPerWordlength);",
				"    for n = 1:numbersPerWordlength",
				"        word = n-1;",
				"        while word \u003e 0",
				"            word_reversed = 0;",
				"            % reverse bit order",
				"            for i = 1:numberofBits",
				"                word_reversed = bitset(word_reversed,numberofBits-i+1,bitget(word,i));",
				"            end",
				"            % binary subtraction with worlength of numbersPerWordlength",
				"            if word \u003e= word_reversed",
				"              word = word-word_reversed;",
				"            else",
				"              word = numbersPerWordlength + word - word_reversed;",
				"            end",
				"            if word \u003e 0 % if == 0 it was already a palindrome",
				"              frequency_list(word+1) = frequency_list(word+1)+1;",
				"            end",
				"        end",
				"    end",
				"    sequence = [sequence frequency_list];",
				"end"
			],
			"xref": [
				"Cf. A338203."
			],
			"keyword": "nonn,base,tabf",
			"offset": "1,6",
			"author": "_Thomas Scheuerle_, Oct 19 2020",
			"references": 1,
			"revision": 34,
			"time": "2021-05-08T08:33:50-04:00",
			"created": "2020-11-28T19:54:11-05:00"
		}
	]
}