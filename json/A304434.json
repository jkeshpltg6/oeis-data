{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A304434",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 304434,
			"data": "3,5,6,9,10,12,13,14,15,17,20,24,25,26,28,29,30,34,35,36,37,39,40,41,42,45,48,50,51,52,53,55,57,58,60,61,63,65,68,70,71,72,73,74,75,78,80,82,85,87,89,90,91,95,96,97,98,100,101,102,104,105,106,109,110,111,113",
			"name": "Numbers n such that n^4 is the sum of two distinct perfect powers \u003e 1 (x^k + y^m; x, y, k, m \u003e= 2).",
			"comment": [
				"Motivated by the search of solutions to a^n + b^(2n+2)/4 = (perfect square), which arises when searching solutions to x^n + y^(n+1) = z^(n+2) of the form x = a*z, y = b*z. It turns out that many solutions are of the form a^n = d (b^(n+1) + d), where d is a perfect power."
			],
			"example": [
				"3^4 = 2^5 + 7^2; 5^4 = 7^2 + 24^2, ..."
			],
			"maple": [
				"N:= 200: # to get terms \u003c= N",
				"N4:= N^4:",
				"P:= {seq(seq(x^k,k=3..floor(log[x](N4))),x=2..floor(N4^(1/3)))}:",
				"filter:= proc(n) local n4, Pp;",
				"  n4:= n^4;",
				"  if remove(t -\u003e subs(t,x)\u003c=1 or subs(t,y)\u003c=1 or subs(t,x-y)=0, [isolve(x^2+y^2=n4)]) \u003c\u003e [] then return true fi;",
				"  Pp:= map(t -\u003en4-t, P minus {n4, n4/2});",
				"  (Pp intersect P \u003c\u003e {}) or (select(issqr,Pp) \u003c\u003e {})",
				"end proc:",
				"A:= select(filter, [$2..N]); # _Robert Israel_, May 24 2018"
			],
			"program": [
				"(PARI) L=200^4; P=List(); for(x=2, sqrtnint(L,3), for(k=3, logint(L, x), listput(P, x^k))); #P=Set(P) \\\\ This P = A076467 \\ {1} = A111231 \\ {0} up to limit L.",
				"is_A304434(n)={for(i=1, #s=sum2sqr(n=n^4), vecmin(s[i])\u003e1 \u0026\u0026 s[i][1]!=s[i][2] \u0026\u0026 return(1)); for(i=1, #P, n\u003eP[i]||return; ispower(n-P[i])\u0026\u0026 P[i]*2 != n \u0026\u0026 return(1))} \\\\ The above P must be computed up to L \u003e= n^4. For sum2sqr() see A133388."
			],
			"xref": [
				"Cf. A304433, A001597 (perfect powers), A076467 (third or higher powers)."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_M. F. Hasler_, May 22 2018",
			"references": 4,
			"revision": 19,
			"time": "2018-05-26T08:52:00-04:00",
			"created": "2018-05-26T08:52:00-04:00"
		}
	]
}