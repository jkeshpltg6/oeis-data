{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A245977",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 245977,
			"data": "2,2,1,2,1,2,2,1,2,2,1,2,1,2,2,1,2,1,2,2,1,2,2,1,2,1,2,2,1,2,2,1,2,1,2,2,1,2,1,2,2,1,2,2,1,2,1,2,2,1,2,1,2,2,1,2,2,1,2,1,2,2,1,2,2,1,2,1,2,2,1,2,1,2,2,1,2,2,1,2,1,2,2,1,2,2",
			"name": "Limit-reverse of the infinite Fibonacci word A014675 = (s(0),s(1),...) = (2,1,2,2,1,2,1,2, ...) using initial block (s(2),s(3)) = (2,2).",
			"comment": [
				"Suppose, as in A245920, that S = (s(0),s(1),s(2),...) is an infinite sequence such that every finite block of consecutive terms occurs infinitely many times in S.  (It is assumed that A014675 is such a sequence.)  Let B = B(m,k) = (s(m-k),s(m-k+1),...,s(m)) be such a block, where m \u003e= 0 and k \u003e= 0.  Let m(1) be the least i \u003e m such that (s(i-k), s(i-k+1),...,s(i)) = B(m,k), and put B(m(1),k+1) = (s(m(1)-k-1),s(m(1)-k),...,s(m(1))).  Let m(2) be the least i \u003e m(1) such that (s(i-k-1),s(i-k),...,s(i)) = B(m(1),k+1), and put B(m(2),k+2) = (s(m(2)-k-2),s(m(2)-k-1),...,s(m(2))).  Continuing in this manner gives a sequence of blocks B(m(n),k+n).  Let B'(n) = reverse(B(m(n),k+n)), so that for n \u003e= 1, B'(n) comes from B'(n-1) by suffixing a single term; thus the limit of B'(n) is defined; we call it the \"limit-reverse of S with initial block B(m,k)\", denoted by S*(m,k), or simply S*.",
				"...",
				"The sequence (m(i)), where m(0) = 0, is the \"index sequence for limit-reversing S with initial block B(m,k)\" or simply the index sequence for S*, as in A245921 and A245978.",
				"Apparently a(n) = A082389(n+2) = A059426(n+1). - _R. J. Mathar_, Sep 01 2014"
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A245977/b245977.txt\"\u003eTable of n, a(n) for n = 0..300\u003c/a\u003e"
			],
			"example": [
				"S = the infinite Fibonacci word A014675, with B = (s(2), s(3)); that is, (m,k) = (2,3)",
				"S = (2,1,2,2,1,2,1,2,2,1,2,2,1,2,1,2,2,1,2,...)",
				"B'(0) = (2,2)",
				"B'(1) = (2,2,1)",
				"B'(2) = (2,2,1,2)",
				"B'(3) = (2,2,1,2,1)",
				"B'(4) = (2,2,1,2,1,2)",
				"B'(5) = (2,2,1,2,1,2,2)",
				"S* = (2,2,1,2,1,2,2,1,2,2,1,2,1,2,2,1,2,1,...), with index sequence (3,8,11,16,21,29,...)"
			],
			"mathematica": [
				"z = 100; seqPosition2[list_, seqtofind_] := Last[Last[Position[Partition[list, Length[#], 1], Flatten[{___, #, ___}], 1, 2]]] \u0026[seqtofind]; x = GoldenRatio; s = Differences[Table[Floor[n*x], {n, 1, z^2}]]; ans = Join[{s[[p[0] = pos = seqPosition2[s, #] - 1]]}, #] \u0026[{s[[3]], s[[4]]}];  (* Initial block is (s(3),s(4))) [OR (s(2),s(3) if using offset 0] *); cfs = Table[s = Drop[s, pos - 1]; ans = Join[{s[[p[n] = pos = seqPosition2[s, #] - 1]]}, #] \u0026[ans], {n, z}]; rcf = Last[Map[Reverse, cfs]] (* A245977 *)"
			],
			"xref": [
				"Cf. A245978, A245979, A245920, A014675."
			],
			"keyword": "nonn",
			"offset": "0,1",
			"author": "_Clark Kimberling_ and _Peter J. C. Moses_, Aug 10 2014",
			"references": 3,
			"revision": 10,
			"time": "2014-09-01T05:00:02-04:00",
			"created": "2014-08-22T10:17:01-04:00"
		}
	]
}