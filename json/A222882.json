{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A222882",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 222882,
			"data": "2,2,5,4,9,2,2,4,6,2,8,8,8,2,6,4,7,6,6,2,6,8,1,8,4,7,5,9,5,2,8,7,2,3,5,5,7,8,7,1,6,6,1,5,9,8,6,0,5,3,5,1,8,8,9,1,3,8,3,1,1,6,1,8,8,5,9,1,7,2,9,2,8,9,5,9,7,1,3,9,3,4,1,0,5,8",
			"name": "Decimal expansion of Sierpiński's second constant, K2 = lim_{n-\u003einfinity} ((1/n) * (Sum_{i=1..n} A004018(i^2)) - 4/Pi * log(n)).",
			"comment": [
				"Sierpiński introduced three constants in his 1908 doctoral thesis. The first, K, is very well known, bears his name and its decimal expansion is given in A062089. However, the second and third of these constants appear to have been largely forgotten. This sequence gives the decimal expansion of the second one, K2, and A222883 gives the decimal expansion of the third , K3. The formula given below show that K2 is related to several other, naturally occurring constants."
			],
			"reference": [
				"Steven R. Finch, Mathematical Constants, Encyclopaedia of Mathematics and its Applications, Cambridge University Press (2003), p.123. Corrigenda in the link below.",
				"A. Schinzel, Wacław Sierpiński’s papers on the theory of numbers, Acta Arithmetica XXI, (1972), pp. 7-13. Corrigenda in \"Dzieje Matematyki Polskiej\" (Wrocław 2012), p.228 (in Polish)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A222882/b222882.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Steven R. Finch, \u003ca href=\"http://arxiv.org/abs/2001.00578\"\u003eErrata and Addenda to Mathematical Constants\u003c/a\u003e, (June 2012), pp. 15-16"
			],
			"formula": [
				"K2 = 4 / Pi * (eulergamma + K / Pi - 12 / Pi^2 * zeta'(2) + log(2) / 3 -1), where K  is Sierpiński's first constant (A062089) and eulergamma is the Euler-Mascheroni constant (A001620).",
				"K2 = 4 * (12 * log(Gamma(3/4)) - 9*log(Pi) + 72*log(A) - 5*log(2) + 3 * eulergamma - 3) / (3 * Pi), where A is the Glaisher-Kinkelin constant (A074962).",
				"K2 = 4 * (12 * log(Gamma(3/4)) + log(A^72 * e^(3*eulergamma - 3) / (32 * Pi^9))) / (3 * Pi).",
				"K2 = 4 / Pi * (log(e^(3*eulergamma - 1) / (2^(2/3) * G^2)) - 12 / Pi^2 * zeta'(2)), where G is Gauss’ AGM constant (A014549).",
				"K2 = 4 / Pi * (log(Pi^2 * e^(3*eulergamma - 1) / (2^(2/3) * L^2)) - 12 / Pi^2 * zeta'(2)), where L is Gauss’ lemniscate constant (A062539)."
			],
			"example": [
				"K2 = 2.25492246288826476626818475952872355787166159860535188913831..."
			],
			"mathematica": [
				"Take[Flatten[RealDigits[N[4(12 Log[Gamma[3/4]]-9 Log[Pi]+72 Log[Glaisher]-5 Log[2]+3 EulerGamma-3)/(3 Pi),100]]],86]"
			],
			"program": [
				"(PARI) 4/Pi*(log(exp(3*Euler-1)/(2^(2/3)/agm(sqrt(2),1)^2)) - 12/Pi^2*zeta'(2)) \\\\ _Charles R Greathouse IV_, Dec 12 2013"
			],
			"xref": [
				"Cf. A001620, A004018, A014549, A062089, A062539, A074962, A222883."
			],
			"keyword": "nonn,cons",
			"offset": "1,1",
			"author": "_Ant King_, Mar 11 2013",
			"ext": [
				"Minor edits by _Vaclav Kotesovec_, Nov 14 2014"
			],
			"references": 3,
			"revision": 31,
			"time": "2020-01-17T05:23:23-05:00",
			"created": "2013-03-11T08:52:34-04:00"
		}
	]
}