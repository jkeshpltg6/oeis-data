{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A347198",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 347198,
			"data": "0,1,1,0,0,1,0,1,1,1,0,0,0,1,1,0,1,1,1,1,0,0,0,0,1,0,1,0,0,1,0,1,0,1,1,1,0,1,0,0,1,1,0,0,1,1,1,1,1,0,0,0,0,0,1,0,1,0,0,0,1,0,0,1,0,0,1,1,1,0,0,1,1,0,1,0,1,0,1,1,0,1,1,1,1,0,1,0,0,0",
			"name": "Consider the sequence of binary words of integers, LSB to MSB S(k) = ( 0;1;01;11;001;101;011;... ). Start with a(0) = 0, extend this sequence with the minimum number of [0;1] such that it contains S(1) then S(2) and so forth, as sub-strings with LSB at any a(n). We extend the sequence as much as required to include S(k) first, in the next step we extend until it includes S(k+1) too.",
			"comment": [
				"It appears that in the long term the mean of this sequence is 1/2.",
				"This sequence is disjunctive but not normal. This means every finite string appears as a substring. But not each string of equal length appears with equal frequency. Compare to A076478 which is known to be disjunctive and normal.",
				"An interesting problem: If we choose an interval a(0);...;a(n) such that this subsequence contains all binary words of 0;1;2;...;k for given k, for which k will this subsequence have the shortest possible length required to obtain this property. Trivial examples would be n = 1 (k = 2): 01 -\u003e 0;1;01 and n = 2 (k = 3): 011 -\u003e 0;1;01;11. This would require n \u003c= floor(log_2(A056744(k)) + 1)."
			],
			"link": [
				"Thomas Scheuerle, \u003ca href=\"/A347198/a347198.svg\"\u003eSum_{k=0..n}(-1+2*a(k)) for n 0 to 10000. Shows some self-similarity.\u003c/a\u003e",
				"Cristian S. Calude, Lutz Priese and Ludwig Staiger, \u003ca href=\"https://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.34.1370\"\u003eDisjunctive sequences: An overview\u003c/a\u003e, University of Auckland, New Zealand (1997)."
			],
			"example": [
				"Actual sequence:  Next desired binary word:  Required extension:",
				"0                 1                         1",
				"01                01                        none",
				"01                11                        1",
				"011               001                       001",
				"011001            101                       01",
				"01100101          011                       none",
				"01100101          111                       11"
			],
			"program": [
				"(MATLAB)",
				"function a = A347198( max_n )",
				"a = 0; m = 1;",
				"    while length(a) \u003c max_n",
				"      b = bitget(m,1:64);",
				"      word = b(1:find(b == 1, 1, 'last' ));",
				"      if isempty(strfind(a, word))",
				"          offset = 0;",
				"          max_o = min(length(word),length(a));",
				"          for o = 1:max_o",
				"              if ~isequal(a(end-(o-1):end),word(1:o))",
				"                  break;",
				"              else",
				"                  offset = o;",
				"              end",
				"          end",
				"          a = [a word(1+offset:end)];",
				"      end",
				"      m = m+1;",
				"    end",
				"end"
			],
			"xref": [
				"Cf. A056744, A076478, A108737."
			],
			"keyword": "base,nonn",
			"offset": "0",
			"author": "_Thomas Scheuerle_, Aug 22 2021",
			"references": 1,
			"revision": 33,
			"time": "2021-10-09T06:30:19-04:00",
			"created": "2021-10-09T00:07:49-04:00"
		}
	]
}