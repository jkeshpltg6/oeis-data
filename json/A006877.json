{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006877",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6877,
			"id": "M0748",
			"data": "1,2,3,6,7,9,18,25,27,54,73,97,129,171,231,313,327,649,703,871,1161,2223,2463,2919,3711,6171,10971,13255,17647,23529,26623,34239,35655,52527,77031,106239,142587,156159,216367,230631,410011,511935,626331,837799",
			"name": "In the '3x+1' problem, these values for the starting value set new records for number of steps to reach 1.",
			"comment": [
				"Both the 3x+1 steps and the halving steps are counted."
			],
			"reference": [
				"Gonnet, Gaston H. \"Computations on the 3n+1 conjecture.\" Maple Technical Newsletter 6 (1991): 18-22.",
				"B. Hayes, Computer Recreations: On the ups and downs of hailstone numbers, Scientific American, 250 (No. 1, 1984), pp. 10-16.",
				"D. R. Hofstadter, Goedel, Escher, Bach: an Eternal Golden Braid, Random House, 1980, p. 400.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A006877/b006877.txt\"\u003eTable of n, a(n) for n = 1..130\u003c/a\u003e (from Eric Roosendaal's data)",
				"T. Ahmed, H. Snevily, \u003ca href=\"https://www.semanticscholar.org/paper/Are-There-an-Infinite-Number-of-Collatz-Integers-AHMED/2207919fcc0e2e4336faf345389c5dcd2daea50e\"\u003eAre there an infinite number of Collatz integers?\u003c/a\u003e, 2013.",
				"J. C. Lagarias, \u003ca href=\"http://www.cecm.sfu.ca/organics/papers/lagarias/paper/html/paper.html\"\u003eThe 3x+1 problem and its generalizations\u003c/a\u003e, Amer. Math. Monthly, 92 (1985), 3-23.",
				"G. T. Leavens and M. Vermeulen, \u003ca href=\"/A006877/a006877_1.pdf\"\u003e3x+1 search programs\u003c/a\u003e, Computers and Mathematics with Applications, 24 (1992), 79-99. (Annotated scanned copy)",
				"R. Munafo, \u003ca href=\"http://www.mrob.com/pub/seq/wondrous.html\"\u003eInteger Sequences Related to 3x+1 Collatz Iteration\u003c/a\u003e",
				"Eric Roosendaal, \u003ca href=\"http://www.ericr.nl/wondrous/delrecs.html\"\u003e3x+1 Delay Records\u003c/a\u003e",
				"R. G. Wilson, V, \u003ca href=\"/A006877/a006877.pdf\"\u003eLetter to N. J. A. Sloane with attachments, Jan. 1989\u003c/a\u003e",
				"R. G. Wilson, V, \u003ca href=\"/A006884/a006884.pdf\"\u003eTables of A6877, A6884, A6885, Jan. 1989\u003c/a\u003e",
				"\u003ca href=\"/index/Go#GEB\"\u003eIndex entries for sequences from \"Goedel, Escher, Bach\"\u003c/a\u003e",
				"\u003ca href=\"/index/3#3x1\"\u003eIndex entries for sequences related to 3x+1 (or Collatz) problem\u003c/a\u003e"
			],
			"maple": [
				"A006877 := proc(n) local a,L; L := 0; a := n; while a \u003c\u003e 1 do if a mod 2 = 0 then a := a/2; else a := 3*a+1; fi; L := L+1; od: RETURN(L); end;"
			],
			"mathematica": [
				"numberOfSteps[x0_] := Block[{x = x0, nos = 0}, While [x != 1 , If[Mod[x, 2] == 0 , x = x/2, x = 3*x + 1]; nos++]; nos]; a[1] = 1; a[n_] := a[n] = Block[{x = a[n-1] + 1}, record = numberOfSteps[x - 1]; While[ numberOfSteps[x] \u003c= record, x++]; x]; A006877 = Table[ Print[a[n]]; a[n], {n, 1, 44}](* _Jean-François Alcover_, Feb 14 2012 *)"
			],
			"program": [
				"(PARI) A006577(n)=my(s);while(n\u003e1,n=if(n%2,3*n+1,n/2);s++);s",
				"step(n,r)=my(t);forstep(k=bitor(n,1),2*n,2,t=A006577(k);if(t\u003er,return([k,t])));[2*n,r+1]",
				"r=0;print1(n=1);for(i=1,100,[n,r]=step(n,r); print1(\", \"n)) \\\\ _Charles R Greathouse IV_, Apr 01 2013"
			],
			"xref": [
				"Cf. A006884, A006885, A006877, A006878, A033492."
			],
			"keyword": "nonn,nice",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, _Robert Munafo_",
			"references": 29,
			"revision": 69,
			"time": "2019-10-20T21:46:30-04:00",
			"created": "1994-04-28T03:00:00-04:00"
		}
	]
}