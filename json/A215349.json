{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A215349",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 215349,
			"data": "1,-4,8,-16,30,-48,80,-128,197,-312,472,-704,1046,-1504,2160,-3072,4306,-6036,8360,-11488,15712,-21264,28656,-38400,51127,-67864,89552,-117632,153926,-200352,259904,-335872,432336,-554952,709728,-904784,1150142,-1457136",
			"name": "Expansion of q * phi(-q) * psi(q^8) / (phi(q) * phi(q^4)) in powers of q where phi(), psi() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A215349/b215349.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q * (f(-q) * f(-q^16) / (f(q) * f(q^4)))^2 = q * (chi(-q^2) * chi(-q^4) / (chi(q) * chi(-q^8))^2)^2 in powers of q where chi(), f() are Ramanujan theta functions.",
				"Expansion of (eta(q) * eta(q^4) * eta(q^16))^4 / (eta(q^2) * eta(q^8))^6 in powers of q.",
				"Euler transform of period 16 sequence [ -4, 2, -4, -2, -4, 2, -4, 4, -4, 2, -4, -2, -4, 2, -4, 0, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (16 t)) = f(t) where q = exp(2 Pi i t).",
				"a(n) = -(-1)^n * A215348(n). a(2*n) = -4 * A107035(n). Convolution inverse of A214035.",
				"a(n) ~ -(-1)^n * exp(sqrt(n)*Pi) / (8*sqrt(2)*n^(3/4)). - _Vaclav Kotesovec_, Sep 08 2017"
			],
			"example": [
				"q - 4*q^2 + 8*q^3 - 16*q^4 + 30*q^5 - 48*q^6 + 80*q^7 - 128*q^8 + 197*q^9 + ..."
			],
			"mathematica": [
				"a[n_]:= SeriesCoefficient[(EllipticTheta[3, 0, -q]*EllipticTheta[2, 0, q^4]/(EllipticTheta[3, 0, q]*EllipticTheta[3, 0, q^4]))/2, {q, 0, n}]; Table[a[n], {n,1,50}] (* _G. C. Greubel_, Jan 07 2018 *)"
			],
			"program": [
				"(PARI) {a(n) = local(A); if( n\u003c1, 0, n--; A = x * O(x^n); polcoeff( (eta(x + A) * eta(x^4 + A) * eta(x^16 + A))^4 / (eta(x^2 + A) * eta(x^8 + A))^6, n))}"
			],
			"xref": [
				"Cf. A107035, A214035, A215348."
			],
			"keyword": "sign",
			"offset": "1,2",
			"author": "_Michael Somos_, Aug 08 2012",
			"references": 5,
			"revision": 18,
			"time": "2021-03-12T22:24:46-05:00",
			"created": "2012-08-08T15:29:01-04:00"
		}
	]
}