{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A282013",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 282013,
			"data": "1,2,3,3,2,1,3,2,1,4,4,2,2,2,1,2,2,4,8,4,3,2,3,2,3,5,5,7,3,2,5,1,3,7,6,5,5,3,5,3,2,3,9,5,2,6,3,1,3,5,5,10,6,2,8,4,3,5,6,3,3,3,4,4,2,5,9,8,5,4,6,1,5,6,5,9,2,3,7,1,1",
			"name": "Number of ways to write n as x^2 + y^2 + z^2 + w^2 with x,y,z,w nonnegative integers such that both x and 49*x + 48*(y-z) are squares.",
			"comment": [
				"Conjecture: a(n) \u003e 0 for all n = 0,1,2,..., and a(n) = 1 only for n = 0, 16^k*m (k = 0,1,2,... and m = 5, 8, 14, 31, 47, 71, 79, 143, 248, 463, 1039).",
				"The author has proved that any nonnegative integer can be written as the sum of a fourth power and three squares.",
				"We have verified a(n) \u003e 0 for all n = 0..10^7.",
				"See also A281976, A281977 and A282014 for similar conjectures.",
				"Qing-Hu Hou at Tianjin University verified a(n) \u003e 0 for n up to 10^9. - _Zhi-Wei Sun_, Jun 02 2019"
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A282013/b282013.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2016.11.008\"\u003eRefining Lagrange's four-square theorem\u003c/a\u003e, J. Number Theory 175(2017), 167-190.",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1701.05868\"\u003eRestricted sums of four squares\u003c/a\u003e, arXiv:1701.05868 [math.NT], 2017."
			],
			"example": [
				"a(5) = 1 since 5 = 1^2 + 0^2 + 0^2 + 2^2 with 1 = 1^2 and 49*1 + 48*(0-0) = 7^2.",
				"a(8) = 1 since 8 = 0^2 + 2^2 + 2^2 + 0^2 with 0 = 0^2 and 49*0 + 48*(2-2) = 0^2.",
				"a(14) = 1 since 14 = 1^2 + 2^2 + 3^2 + 0^2 with 1 = 1^2 and 49*1 + 48*(2-3) = 1^2.",
				"a(31) = 1 since 31 = 1^2 + 1^2 + 2^2 + 5^2 with 1 = 1^2 and 49*1 + 48*(1-2) = 1^2.",
				"a(47) = 1 since 47 = 1^2 + 6^2 + 1^2 + 3^2 with 1 = 1^2 and 49*1 + 48*(6-1) = 17^2.",
				"a(71) = 1 since 71 = 1^2 + 5^2 + 6^2 + 3^2 with 1 = 1^2 and 49*1 + 48*(5-6) = 1^2.",
				"a(79) = 1 since 79 = 1^2 + 7^2 + 2^2 + 5^2 with 1 = 1^2 and 49*1 + 48*(7-2) = 17^2.",
				"a(143) = 1 since 143 = 1^2 + 5^2 + 6^2 + 9^2 with 1 = 1^2 and 49*1 + 48*(5-6) = 1^2.",
				"a(248) = 1 since 248 = 4^2 + 6^2 + 0^2 + 14^2 with 4 = 2^2 and 49*4 + 48*(6-0) = 22^2.",
				"a(463) = 1 since 463 = 9^2 + 6^2 + 15^2 + 11^2 with 9 = 3^2 and 49*9 + 48*(6-15) = 3^2.",
				"a(1039) = 1 since 1039 = 1^2 + 22^2 + 23^2 + 5^2 with 1 = 1^2 and 49*1 + 48*(22-23) = 1^2."
			],
			"mathematica": [
				"SQ[n_]:=SQ[n]=IntegerQ[Sqrt[n]];",
				"Do[r=0;Do[If[SQ[n-x^4-y^2-z^2]\u0026\u0026SQ[49x^2+48(y-z)],r=r+1],{x,0,n^(1/4)},{y,0,Sqrt[n-x^4]},{z,0,Sqrt[n-x^4-y^2]}];Print[n,\" \",r];Continue,{n,0,80}]"
			],
			"xref": [
				"Cf. A000118, A000290, A270969, A271518, A281939, A281941, A281975, A281976, A281977, A282014."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Zhi-Wei Sun_, Feb 04 2017",
			"references": 9,
			"revision": 15,
			"time": "2019-06-01T23:52:55-04:00",
			"created": "2017-02-05T02:01:45-05:00"
		}
	]
}