{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002419",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2419,
			"id": "M4699 N2008",
			"data": "1,10,40,110,245,476,840,1380,2145,3190,4576,6370,8645,11480,14960,19176,24225,30210,37240,45430,54901,65780,78200,92300,108225,126126,146160,168490,193285,220720,250976,284240,320705,360570,404040,451326",
			"name": "4-dimensional figurate numbers: (6*n-2)*binomial(n+2,3)/4.",
			"comment": [
				"a(n) is the n-th antidiagonal sum of the convolution array A213761. - _Clark Kimberling_, Jul 04 2012",
				"Convolution of A000027 with A000567 (excluding 0). - _Bruno Berselli_, Dec 07 2012",
				"a(n) = the sum of all the ways of adding the k-tuples of A016777(0) to A016777(n-1). For n=4, the terms are 1,4,7,10 giving (1)+(4)+(7)+(10)=22; (1+4)+(4+7)+(7+10)=33; (1+4+7)+(4+7+10)=33; (1+4+7+10)=22; adding 22+33+33+22=110. - _J. M. Bergot_, Jun 26 2017",
				"Also the number of chordless cycles in the (n+3)-crown graph. - _Eric W. Weisstein_, Jan 02 2018"
			],
			"reference": [
				"A. H. Beiler, Recreations in the Theory of Numbers, Dover, NY, 1964, p. 195.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A002419/b002419.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ChordlessCycle.html\"\u003eChordless Cycle\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CrownGraph.html\"\u003eCrown Graph\u003c/a\u003e",
				"\u003ca href=\"/index/Ps#pyramidal_numbers\"\u003eIndex to sequences related to pyramidal numbers\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (5,-10,10,-5,1)."
			],
			"formula": [
				"a(n) = (3*n-1)*binomial(n+2, 3)/2.",
				"G.f.: x*(1+5*x)/(1-x)^5. - _Simon Plouffe_ in his 1992 dissertation.",
				"Sum_{n\u003e=1} 1/a(n) = (-24+81*log(3) -9*Pi*sqrt(3))/14 = 1.143929... - _R. J. Mathar_, Mar 29 2011",
				"a(n) = (3*n^4 + 8*n^3 + 3*n^2 - 2*n)/12. - _Chai Wah Wu_, Jan 24 2016",
				"a(n) = A080852(6,n-1). - _R. J. Mathar_, Jul 28 2016",
				"E.g.f.: x*(12 + 48*x + 26*x^2 + 3*x^3)*exp(x)/12. - _G. C. Greubel_, Jul 03 2019"
			],
			"mathematica": [
				"CoefficientList[Series[(1+5*x)/(1-x)^5, {x,0,40}], x] (* _Vincenzo Librandi_, Jun 20 2013 *)",
				"LinearRecurrence[{5, -10, 10, -5, 1}, {1, 10, 40, 110, 245}, 40] (* _Harvey P. Dale_, Nov 30 2014 *)",
				"Table[n(n+1)(n+2)(3n-1)/12, {n, 40}] (* _Eric W. Weisstein_, Jan 02 2018 *)"
			],
			"program": [
				"(MAGMA) /* A000027 convolved with A000567 (excluding 0): */ A000567:=func\u003cn |  n*(3*n-2)\u003e; [\u0026+[(n-i+1)*A000567(i): i in [1..n]]: n in [1..40]]; // _Bruno Berselli_, Dec 07 2012",
				"(PARI) a(n)=(3*n-1)*binomial(n+2,3)/2 \\\\ _Charles R Greathouse IV_, Sep 24 2015",
				"(Python)",
				"A002419_list, m = [], [6, 1, 1, 1, 1]",
				"for _ in range(10**2):",
				"    A002419_list.append(m[-1])",
				"    for i in range(4):",
				"        m[i+1] += m[i] # _Chai Wah Wu_, Jan 24 2016",
				"(Sage) [n*(n+1)*(n+2)*(3*n-1)/12 for n in (1..40)] # _G. C. Greubel_, Jul 03 2019",
				"(GAP) List([1..40], n-\u003e n*(n+1)*(n+2)*(3*n-1)/12) # _G. C. Greubel_, Jul 03 2019"
			],
			"xref": [
				"Cf. A093563 ((6, 1) Pascal, column m=4).",
				"Cf. A000567, A002414 (first differences).",
				"Cf. A220212 for a list of sequences produced by the convolution of the natural numbers with the k-gonal numbers."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"references": 17,
			"revision": 76,
			"time": "2021-03-12T22:32:35-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}