{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A158092",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 158092,
			"data": "0,0,0,0,0,0,2,2,0,0,2,10,0,0,86,114,0,0,478,860,0,0,5808,10838,0,0,55626,100426,0,0,696164,1298600,0,0,7826992,14574366,0,0,100061106,187392994,0,0,1223587084,2322159814,0,0,16019866270,30353305134,0,0",
			"name": "Number of solutions to +- 1 +- 2^2 +- 3^2 +- 4^2 +- ... +- n^2 = 0.",
			"comment": [
				"Twice A083527.",
				"Number of partitions of the half of the n-th-square-pyramidal number into parts that are distinct square numbers in the range 1 to n^2. Example: a(7)=2 since, squarePyramidal(7)=140 and 70=1+4+16+49=9+25+36. - _Hieronymus Fischer_, Oct 20 2010",
				"Erdős \u0026 Surányi prove that this sequence is unbounded. More generally, there are infinitely many ways to write a given number k as such a sum. - _Charles R Greathouse IV_, Nov 05 2012",
				"The expansion and integral representation formulas below are due to Andrica \u0026 Tomescu. The asymptotic formula is a conjecture; see Andrica \u0026 Ionascu. - _Jonathan Sondow_, Nov 11 2013"
			],
			"link": [
				"Alois P. Heinz and Ray Chandler, \u003ca href=\"/A158092/b158092.txt\"\u003eTable of n, a(n) for n = 1..500\u003c/a\u003e (first 240 terms from Alois P. Heinz)",
				"D. Andrica and E. J. Ionascu, \u003ca href=\"http://www.dorinandrica.ro/files/presentation-INTEGERS-2013.pdf\"\u003eVariations on a result of Erdős and Surányi\u003c/a\u003e, INTEGERS 2013 slides.",
				"Dorin Andrica and Ioan Tomescu, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL5/Tomescu/tomescu4.html\"\u003eOn an Integer Sequence Related to a Product of Trigonometric Functions, and Its Combinatorial Relevance\u003c/a\u003e, J. Integer Sequences, 5 (2002), Article 02.2.4.",
				"P. Erdős and J. Surányi, \u003ca href=\"http://www.renyi.hu/~p_erdos/1959-05.pdf\"\u003eEgy additív számelméleti probléma\u003c/a\u003e (in Hungarian; Russian and German summaries), Mat. Lapok 10 (1959), pp. 284-290."
			],
			"formula": [
				"Constant term in the expansion of (x + 1/x)(x^4 + 1/x^4)..(x^n^2 + 1/x^n^2).",
				"a(n)=0 for any n == 1 or 2 (mod 4).",
				"Integral representation: a(n)=((2^n)/pi)*int_0^pi prod_{k=1}^n cos(x*k^2) dx",
				"Asymptotic formula: a(n) = (2^n)*sqrt(10/(pi*n^5))*(1+o(1)) as n--\u003einfty; n == -1 or 0 (mod 4).",
				"a(n) = 2 * A083527(n). - _T. D. Noe_, Mar 12 2009",
				"min{n : a(n) \u003e 0} = A231015(0) = 7. - _Jonathan Sondow_, Nov 06 2013"
			],
			"example": [
				"For n=8 the a(8)=2 solutions are: +1-4-9+16-25+36+49-64=0 and -1+4+9-16+25-36-49+64=0."
			],
			"maple": [
				"From _Pietro Majer_, Mar 15 2009: (Start)",
				"N:=60: p:=1: a:=[]: for n from 1 to N do p:=expand(p*(x^(n^2)+x^(-n^2))):",
				"a:=[op(a), coeff(p, x, 0)]: od:a; (End)",
				"# second Maple program:",
				"b:= proc(n, i) option remember; local m; m:= (1+(3+2*i)*i)*i/6;",
				"      `if`(n\u003em, 0, `if`(n=m, 1, b(abs(n-i^2), i-1) +b(n+i^2, i-1)))",
				"    end:",
				"a:= n-\u003e `if`(irem(n-1, 4)\u003c2, 0, 2*b(n^2, n-1)):",
				"seq(a(n), n=1..60);  # _Alois P. Heinz_, Nov 05 2012"
			],
			"mathematica": [
				"b[n_, i_] := b[n, i] = With[{m = (1+(3+2*i)*i)*i/6}, If[n\u003em, 0, If[n == m, 1, b[ Abs[n-i^2], i-1] + b[n+i^2, i-1]]]]; a[n_] := If[Mod[n-1, 4]\u003c2, 0, 2*b[n^2, n-1]]; Table[a[n], {n, 1, 60}] (* _Jean-François Alcover_, Mar 13 2015, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(PARI) a(n)=2*sum(i=0,2^(n-1)-1,sum(j=1,n-1,(-1)^bittest(i,j-1)*j^2)==n^2) \\\\ _Charles R Greathouse IV_, Nov 05 2012"
			],
			"xref": [
				"Cf. A063865, A158118, A019568, A083527, A231015."
			],
			"keyword": "nonn",
			"offset": "1,7",
			"author": "_Pietro Majer_, Mar 12 2009",
			"ext": [
				"a(51)-a(56) from _R. H. Hardin_, Mar 12 2009",
				"Edited by _N. J. A. Sloane_, Sep 15 2009"
			],
			"references": 12,
			"revision": 48,
			"time": "2021-12-30T01:17:56-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}