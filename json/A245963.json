{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A245963",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 245963,
			"data": "1,0,1,0,2,0,1,1,0,0,3,0,0,3,1,0,0,1,4,0,0,0,6,1,0,0,0,4,5,0,0,0,1,10,1,0,0,0,0,10,6,0,0,0,0,5,15,1,0,0,0,0,1,20,7,0,0,0,0,0,15,21,1,0,0,0,0,0,6,35,8,0,0,0,0,0,1,35,28,1,0,0,0,0,0,0,21,56,9,0,0,0,0,0,0,7,70,36,1",
			"name": "Triangle read by rows: T(n,k) is the number of maximal hypercubes Q(p) in the Fibonacci cube Gamma(n) (i.e., Q(p) is an induced subgraph of Gamma(n) that is not a subgraph of a subgraph of Gamma(n) that is isomorphic to the hypercube Q(p+1)).",
			"comment": [
				"The nonzero entries in columns 0,1,2,... are rows 0,2,3,... of the Pascal triangle.",
				"Row n contains 1+ceiling(n/2) entries.",
				"Sum of entries in row n = A000931(n+6) (the Padovan sequence).",
				"Sum_{k\u003e=0}k*T(n,k) = A228364(n+1)."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A245963/b245963.txt\"\u003eTable of n, a(n) for n = 0..10300\u003c/a\u003e (Rows 1 \u003c= n \u003c= 200).",
				"S. Klavzar, \u003ca href=\"http://dx.doi.org/10.1007/s10878-011-9433-z\"\u003eStructure of Fibonacci cubes: a survey\u003c/a\u003e, J. Comb. Optim., 25, 2013, 505-522.",
				"M. Mollard, \u003ca href=\"http://arxiv.org/abs/1201.1494\"\u003eMaximal hypercubes in Fibonacci and Lucas cubes\u003c/a\u003e, arXiv:1201.1494 [math.CO], 2012.",
				"M. Mollard, \u003ca href=\"http://dx.doi.org/10.1016/j.dam.2012.06.003\"\u003eMaximal hypercubes in Fibonacci and Lucas cubes\u003c/a\u003e, Discrete Appl. Math., 160, 2012, 2479-2483."
			],
			"formula": [
				"T(n,k) = binomial(k+1,n-2*k+1).",
				"G.f.: (1+t*z*(1+z))/(1-t*(1+z)*z^2)."
			],
			"example": [
				"Row 3 is 0,1,1. Indeed, the Fibonacci cube Gamma(3) is a square with an additional pendant edge attached to one of its vertices; the pendant edge is a maximal Q(1) and the square is a maximal Q(2).",
				"Triangle starts:",
				"  1;",
				"  0, 1;",
				"  0, 2;",
				"  0, 1, 1;",
				"  0, 0, 3;",
				"  0, 0, 3, 1;",
				"  0, 0, 1, 4;",
				"  0, 0, 0, 6, 1;"
			],
			"maple": [
				"T := proc (n, k) options operator, arrow: binomial(1+k, n-2*k+1) end proc: for n from 0 to 20 do seq(T(n, k), k = 0 .. (n+1)*(1/2)) end do; # yields sequence in triangular form"
			],
			"mathematica": [
				"Table[Binomial[k + 1, n - 2 k + 1], {n, 0, 17}, {k, 0, Ceiling[n/2]}] // Flatten (* _Michael De Vlieger_, Jul 16 2017 *)"
			],
			"xref": [
				"Cf. A000931, A228364, A245964."
			],
			"keyword": "nonn,tabf",
			"offset": "0,5",
			"author": "_Emeric Deutsch_, Aug 13 2014",
			"references": 2,
			"revision": 19,
			"time": "2017-07-17T01:42:18-04:00",
			"created": "2014-08-14T03:56:50-04:00"
		}
	]
}