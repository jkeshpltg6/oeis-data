{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A167928",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 167928,
			"data": "1,0,0,0,0,1,1,3,4,6,9,13,16,23,31,38,51,65,83,104,132,162,207,252,313,381,475,571,703,846,1032,1237,1502,1791,2164,2570,3086,3659,4375,5167,6146,7244,8584,10086,11909,13954,16421,19195,22510,26250,30696,35714",
			"name": "Number of partitions of n that do not contain 1 as a part and whose parts are not the same divisor of n.",
			"comment": [
				"Note that these partitions are located in the head of the last section of the set of partitions of n (see the shell model of partitions, here)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A167928/b167928.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Omar E. Pol, \u003ca href=\"http://www.polprimos.com/imagenespub/polpatru.jpg\"\u003eIllustration of the shell model of partitions (2D and 3D view)\u003c/a\u003e",
				"Omar E. Pol, \u003ca href=\"http://www.polprimos.com/imagenespub/polpa2dt.jpg\"\u003eIllustration of the shell model of partitions (2D view)\u003c/a\u003e",
				"Omar E. Pol, \u003ca href=\"http://www.polprimos.com/imagenespub/polpa3dt.jpg\"\u003eIllustration of the shell model of partitions (3D view)\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A002865(n) - A032741(n)."
			],
			"example": [
				"The partitions of 6 are:",
				"6 ....................... All parts are the same divisor of n.",
				"5 + 1 ................... Contains 1 as a part.",
				"4 + 2 ................... All parts are not the same divisor of n. \u003c------(1)",
				"4 + 1 + 1 ............... Contains 1 as a part.",
				"3 + 3 ................... All parts are the same divisor of n.",
				"3 + 2 + 1 ............... Contains 1 as a part.",
				"3 + 1 + 1 + 1 ........... Contains 1 as a part.",
				"2 + 2 + 2 ............... All parts are the same divisor of n.",
				"2 + 2 + 1 + 1 ........... Contains 1 as a part.",
				"2 + 1 + 1 + 1 + 1 ....... Contains 1 as a part.",
				"1 + 1 + 1 + 1 + 1 + 1 ... Contains 1 as a part.",
				"Then a(6) = 1."
			],
			"maple": [
				"b:= proc(n, i, t) option remember;",
				"      `if`(n=0, `if`(t\u003c\u003e1, 1, 0), `if`(i\u003c2, 0,",
				"      add(b(n-i*j, i-1, `if`(j=0, t, max(0, t-1))), j=0..n/i)))",
				"    end:",
				"a:= n-\u003e b(n, n, 2):",
				"seq(a(n), n=0..60);  # _Alois P. Heinz_, May 24 2013"
			],
			"mathematica": [
				"Prepend[Array[ n \\[Function] Length@Select[IntegerPartitions[n, All, Range[2, n - 1]], Length[Union[ # ]] \u003e 1 \u0026], 40], 1] (* J. Mulder (jasper.mulder(AT)planet.nl), Jan 25 2010 *)",
				"b[n_, i_, t_] := b[n, i, t] = If[n == 0, If[t != 1, 1, 0], If[i \u003c 2, 0, Sum[b[n - i*j, i - 1, If[j == 0, t, Max[0, t - 1]]], {j, 0, n/i}]]]; a[n_] := b[n, n, 2]; Table[a[n], {n, 0, 60}] (* _Jean-François Alcover_, Aug 29 2016, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A000041, A002865, A032741, A144300, A135010, A138121, A167929, A167930, A167932, A167934."
			],
			"keyword": "nonn",
			"offset": "0,8",
			"author": "_Omar E. Pol_, Nov 17 2009",
			"ext": [
				"More terms from J. Mulder (jasper.mulder(AT)planet.nl), Jan 25 2010",
				"More terms from _Alois P. Heinz_, May 24 2013"
			],
			"references": 1,
			"revision": 14,
			"time": "2016-08-29T07:29:10-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}