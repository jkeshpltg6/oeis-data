{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000243",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 243,
			"id": "M2803 N1128",
			"data": "1,3,9,26,75,214,612,1747,4995,14294,40967,117560,337830,972027,2800210,8075889,23315775,67380458,194901273,564239262,1634763697,4739866803,13752309730,39926751310,115988095896,337138003197",
			"name": "Number of trees with n nodes, 2 of which are labeled.",
			"reference": [
				"J. Riordan, An Introduction to Combinatorial Analysis, Wiley, 1958, p. 138.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A000243/b000243.txt\"\u003eTable of n, a(n) for n=2..200\u003c/a\u003e",
				"R. J. Mathar, \u003ca href=\"http://arxiv.org/abs/1603.00077\"\u003eTopologically distinct sets of non-intersecting circles in the plane\u003c/a\u003e, arXiv:1603.00077 [math.CO] (2016), Table 5.",
				"\u003ca href=\"/index/Ro#rooted\"\u003eIndex entries for sequences related to rooted trees\u003c/a\u003e",
				"\u003ca href=\"/index/Tra#trees\"\u003eIndex entries for sequences related to trees\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A000107(n) - A000081(n). - _Christian G. Bower_, Nov 15 1999",
				"G.f.: A(x) = B(x)^2/(1-B(x)), where B(x) is g.f. for rooted trees with n nodes, cf. A000081. - _Vladeta Jovovic_, Oct 19 2001",
				"a(n) = A000106(n) + A304068(n). - _Brendan McKay_, May 05 2018"
			],
			"maple": [
				"b:= proc(n) option remember; if n\u003c=1 then n else add(k*b(k)* s(n-1, k), k=1..n-1)/(n-1) fi end: s:= proc(n,k) option remember; add(b(n+1-j*k), j=1..iquo(n,k)) end: B:= proc(n) option remember; add(b(k)*x^k, k=1..n) end: a:= n-\u003e coeff(series(B(n-1)^2/(1-B(n-1)), x=0, n+1), x,n): seq(a(n), n=2..27); # _Alois P. Heinz_, Aug 21 2008"
			],
			"mathematica": [
				"b[n_] := b[n] = If[ n \u003c= 1 , n, Sum[k*b[k]*s[n - 1, k], {k, 1, n - 1}]/(n - 1) ]; s[n_, k_] := s[n, k] = Sum[ b[n + 1 - j*k], {j, 1, Quotient[n, k]}]; B[n_] := B[n] = Sum[ b[k]*x^k, {k, 1, n}]; a[n_] := Coefficient[ Series[ B[n - 1]^2/(1 - B[n - 1]), {x, 0, n + 1}], x, n]; Table[ a[n], {n, 2, 27}] (* _Jean-François Alcover_, Jan 25 2012, translated from Maple *)"
			],
			"xref": [
				"Cf. A000055, A000081, A000269, A000485, A000526, A000107, A000524, A000444, A000525."
			],
			"keyword": "nonn,easy,nice",
			"offset": "2,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms and new description from _Christian G. Bower_, Nov 15 1999"
			],
			"references": 15,
			"revision": 38,
			"time": "2020-05-02T15:18:58-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}