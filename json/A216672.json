{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A216672",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 216672,
			"data": "0,1,1,1,2,2,2,3,3,3,3,4,5,3,3,4,6,5,5,6,6,5,4,6,7,5,6,8,8,5,6,8,9,7,5,9,10,6,6,10,11,6,8,9,11,7,6,10,11,8,8,14,11,10,8,10,13,9,8,10,14,7,9,12,14,9,10,14,12,10,8,15,17,9,9,16,12,8,11",
			"name": "Total number of solutions to the equation x^2 + k*y^2 = n with x \u003e 0, y \u003e 0, k \u003e 0. (Order does not matter for the equation x^2 + y^2 = n.)",
			"comment": [
				"If the equation x^2 + y^2 = n has two solutions (x, y), (y, x) then they will be counted only once.",
				"No solutions can exist for the values of k \u003e= n.",
				"This sequence differs from A216503 since this sequence gives the total number of solutions to the equation x^2 + k*y^2 = n, whereas the sequence A216503 gives the number of distinct values of k for which a solution to the equation x^2 + k*y^2 = n can exist.",
				"Some values of k can clearly have more than one solution.",
				"For example, x^2 + k*y^2 = 33 is satisfiable for",
				"33 = 1^2 + 2*4^2.",
				"33 = 5^2 + 2*2^2.",
				"33 = 3^2 + 6*2^2.",
				"33 = 1^2 + 8*2^2.",
				"33 = 5^2 + 8*1^2.",
				"33 = 4^2 + 17*1^2.",
				"33 = 3^2 + 24*1^2.",
				"33 = 2^2 + 29*1^2.",
				"33 = 1^2 + 32*1^2.",
				"So for this sequence a(33) = 9.",
				"On the other hand, for the sequence A216503, there exist only 7 different values of k for which a solution to the equation mentioned above exists.",
				"So A216503(33) = 7."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A216672/b216672.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e"
			],
			"mathematica": [
				"nn = 100; t = Table[0, {nn}]; Do[n = x^2 + k*y^2; If[n \u003c= nn \u0026\u0026 (k \u003e 1 || k == 1 \u0026\u0026 x \u003c= y), t[[n]]++], {x, Sqrt[nn]}, {y, Sqrt[nn]}, {k, nn}] (* _T. D. Noe_, Sep 20 2012 *)"
			],
			"program": [
				"(PARI) for(n=1, 100, sol=0; for(k=1, n, for(x=1, n, if((issquare(n-k*x*x)\u0026\u0026n-k*x*x\u003e0\u0026\u0026k\u003e=2)||(issquare(n-x*x)\u0026\u0026n-x*x\u003e0\u0026\u0026k==1\u0026\u0026x*x\u003c=n-x*x), sol++))); print1(sol\", \")) /* _V. Raman_, Oct 16 2012 */"
			],
			"xref": [
				"Cf. A216503, A216504, A216505.",
				"Cf. A217834 (a variant of this sequence, when the order does matter for the equation x^2+y^2 = n, i.e. if the equation x^2+y^2 = n has got two solutions (x, y), (y, x) then they will be counted separately).",
				"Cf. A216673, A216674, A217834, A217840, A217956."
			],
			"keyword": "nonn",
			"offset": "1,5",
			"author": "_V. Raman_, Sep 13 2012",
			"ext": [
				"Ambiguity in name corrected by _V. Raman_, Oct 16 2012"
			],
			"references": 6,
			"revision": 20,
			"time": "2019-05-26T18:56:44-04:00",
			"created": "2012-09-15T00:46:11-04:00"
		}
	]
}