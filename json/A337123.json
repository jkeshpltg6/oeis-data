{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A337123",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 337123,
			"data": "0,1,2,1,4,5,7,5,9,8,12,7,11,7,11,9,14,10,18,11,21,7,9,11,16,4,8,9,7,12,18,13,14,11,10,9,18,7,12,10,18,12,22,5,11,13,16,13,22,8,9,16,13,9,13,14,10,11,10,10,20,15,9,10,13,8,22,10,10,10,12,13",
			"name": "a(n) is the number of primes p in the n-digit \"signed nonadjacent form\" such that p has 3 or fewer nonzero digits.",
			"comment": [
				"Sign nonadjacent form notation is defined by the publications listed in the reference.",
				"We use abbreviation SNF for \"signed nonadjacent form\" notation.",
				"This is an expansion of A337124 to include 2 and primes in the form of 2^k+1 and 2^k-1."
			],
			"reference": [
				"Joerg Arndt, Matters Computational - Ideas, Algorithms, Source Code, 2011, Springer, pp. 61-62."
			],
			"link": [
				"H. Prodinger, \u003ca href=\"http://www.emis.de/journals/INTEGERS/papers/a8/a8.Abstract.html\"\u003eOn binary representations of integers with digits -1,0,1\u003c/a\u003e, Integers 0 (2000), #A08."
			],
			"example": [
				"There is only one number in single-digit SNF notation, which is 1 and 1 is not a prime.  So a(1)=0;",
				"There is only one number in the two-digit SNF notation, which is 10 = 2 base 10 and it is a prime with one nonzero digit.  So a(2)=1;",
				"There are three numbers in three digits SNF notation: 10T = 3 base 10, 100 = 4 base 10, and 101 = 5 base 10.  There are two prime numbers among 3, 4, and 5 and both of them have two nonzero digits. So a(3)=2;",
				"...",
				"For seven-digit SNF numbers, 10T0T0T = 43 base 10 has 4 nonzero digits (excluded); 10T000T = 47 base 10 has 3 nonzero digits (included). Thereafter 10T0101 = 53: 4 digits, excluded; 1000T0T = 59: 3 digits, included; 1000T01 = 61: 3 digits, included; 100010T = 67: 3 digits, included; 100100T = 71: 3 digits, included; 1001001 = 73, 3 digits, included; 101000T = 79: 3 digits, included; 101010T = 83, 4 digits, excluded.  In total, 7 numbers fit the definition.  So a(7)=7."
			],
			"mathematica": [
				"Table[s1=2^(n-1);ct=0; If[n\u003e1, If[PrimeQ[s1+1],ct++]; If[PrimeQ[s1-1],ct++]; If[n\u003e=5, Do[s2=2^i; If[PrimeQ[s1+s2+1],ct++]; If[PrimeQ[s1+s2-1],ct++]; If[PrimeQ[s1-s2+1],ct++]; If[PrimeQ[s1-s2-1],ct++], {i,2,n-3}]]]; ct, {n,1,72}]"
			],
			"xref": [
				"Cf. A334913, A337124."
			],
			"keyword": "base,nonn",
			"offset": "1,3",
			"author": "_Lei Zhou_, Aug 17 2020",
			"references": 1,
			"revision": 16,
			"time": "2021-01-08T20:39:40-05:00",
			"created": "2020-09-20T00:12:15-04:00"
		}
	]
}