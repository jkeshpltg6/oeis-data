{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A106400",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 106400,
			"data": "1,-1,-1,1,-1,1,1,-1,-1,1,1,-1,1,-1,-1,1,-1,1,1,-1,1,-1,-1,1,1,-1,-1,1,-1,1,1,-1,-1,1,1,-1,1,-1,-1,1,1,-1,-1,1,-1,1,1,-1,1,-1,-1,1,-1,1,1,-1,-1,1,1,-1,1,-1,-1,1,-1,1,1,-1,1,-1,-1,1,1,-1,-1,1,-1,1,1,-1,1,-1,-1,1,-1,1,1,-1,-1,1,1,-1,1,-1,-1,1,1,-1,-1,1,-1,1",
			"name": "Thue-Morse sequence: let A_k denote the first 2^k terms; then A_0 = 1 and for k \u003e= 0, A_{k+1} = A_k B_k, where B_k is obtained from A_k by interchanging 1's and -1's.",
			"comment": [
				"See A010060, the main entry for the Thue-Morse sequence, for additional information. - _N. J. A. Sloane_, Aug 13 2014",
				"a(A000069(n)) = -1; a(A001969(n)) = +1. - _Reinhard Zumkeller_, Apr 29 2012",
				"Partial sums of every third terms give A005599. - _Reinhard Zumkeller_, May 26 2013",
				"Fixed point of the morphism 1 --\u003e 1,-1 and -1 --\u003e -1,1. - _Robert G. Wilson v_, Apr 07 2014",
				"Fibbinary numbers (A003714) gives the numbers n for which a(n) = A132971(n). - _Antti Karttunen_, May 30 2017"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A106400/b106400.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Joerg Arndt, \u003ca href=\"http://www.jjj.de/fxt/#fxtbook\"\u003eMatters Computational (The Fxtbook)\u003c/a\u003e",
				"Thomas Baruchel, \u003ca href=\"https://doi.org/10.1007/s42979-019-0049-1\"\u003eFlattening Karatsuba's Recursion Tree into a Single Summation\u003c/a\u003e, SN Computer Science (2020) Vol. 1, Article No. 48.",
				"Thomas Baruchel, \u003ca href=\"https://arxiv.org/abs/1912.00452\"\u003eA non-symmetric divide-and-conquer recursive formula for the convolution of polynomials and power series\u003c/a\u003e, arXiv:1912.00452 [math.NT], 2019.",
				"Yann Bugeaud and Guo-Niu Han, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v21i3p26/0\"\u003eA combinatorial proof of the non-vanishing of Hankel determinants of the Thue-Morse sequence\u003c/a\u003e, Electronic Journal of Combinatorics 21(3) (2014), #P3.26.",
				"Hao Fu, G.-N. Han, \u003ca href=\"https://arxiv.org/abs/1601.04370\"\u003eComputer assisted proof for Apwenian sequences related to Hankel determinants\u003c/a\u003e, arXiv preprint arXiv:1601.04370 [math.NT], 2016.",
				"Philip Lafrance, Narad Rampersad, Randy Yee, \u003ca href=\"http://arxiv.org/abs/1408.2277\"\u003eSome properties of a Rudin-Shapiro-like sequence\u003c/a\u003e, arXiv:1408.2277 [math.CO], 2014.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"https://mathworld.wolfram.com/Thue-MorseSequence.html\"\u003eThue-Morse sequence\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Bell_polynomials\"\u003eBell polynomials\u003c/a\u003e",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e"
			],
			"formula": [
				"a(n) = (-1)^A010060(n).",
				"a(n) = (-1)^wt(n), where wt(n) is the binary weight of n, A000120(n).",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2), A(x^4)) where f(u, v, w) = v^3 - 2*u*v*w + u^2*w.",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2), A(x^3), A(x^6)) where f(u1, u2, u3, u6) = u6*u1^3 - 3*u6*u2*u1^2 + 3*u6*u2^2*u1 - u3*u2^3.",
				"Euler transform of sequence b(n) where b(2^k) = -1 and zero otherwise.",
				"G.f.: Product_{k\u003e=0} (1 - x^(2^k)) = A(x) = (1-x) * A(x^2).",
				"a(n) = B_n(-A038712(1)*0!, ..., -A038712(n)*(n-1)!)/n!, where B_n(x_1, ..., x_n) is the n-th complete Bell polynomial. See the Wikipedia link for complete Bell polynomials , and A036040 for the coefficients of these partition polynomials. - _Gevorg Hmayakyan_, Jul 10 2016 (edited by - _Wolfdieter Lang_, Aug 31 2016)",
				"a(n) = A008836(A005940(1+n)). [Analogous to Liouville's lambda] - _Antti Karttunen_, May 30 2017",
				"a(n) = (-1)^A309303(n), see the closed form (5) in the MathWorld link. - _Vladimir Reshetnikov_, Jul 23 2019"
			],
			"example": [
				"G.f. = 1 - x - x^2 + x^3 - x^4 + x^5 + x^6 - x^7 - x^8 + x^9 + x^10 + ...",
				"The first 2^2 = 4 terms are 1, -1, -1, 1. Exchanging 1 and -1 gives -1, 1, 1, -1, which are a(4) through a(7). - _Michael B. Porter_, Jul 29 2016"
			],
			"maple": [
				"A106400 := proc(n)",
				"        1-2*A010060(n) ;",
				"end proc: # _R. J. Mathar_, Jul 22 2012",
				"subs(\"0\"=1,\"1\"=-1, StringTools:-Explode(StringTools:-ThueMorse(1000))); # _Robert Israel_, Sep 01 2015",
				"# third Maple program:",
				"a:= n-\u003e (-1)^add(i, i=Bits[Split](n)):",
				"seq(a(n), n=0..120);  # _Alois P. Heinz_, Apr 13 2020"
			],
			"mathematica": [
				"tm[0] = 0; tm[n_?EvenQ] := tm[n/2]; tm[n_] := 1 - tm[(n-1)/2]; Table[(-1)^tm[n], {n, 0, 101}] (* _Jean-François Alcover_, Oct 24 2013 *)",
				"Nest[ Flatten[# /. {1 -\u003e {1, -1}, -1 -\u003e {-1, 1}}] \u0026, {1}, 7] (* _Robert G. Wilson v_, Apr 07 2014 *)",
				"Table[Coefficient[Product[1 - x^(2^k), {k, 0, Log2[n + 1]}], x, n], {n, 0, 20}] (* _Vladimir Reshetnikov_, Nov 11 2016 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, n\u003e=0, a(n\\2) * (-1)^(n%2))};",
				"(PARI) {a(n) = my(A, m); if( n\u003c1, n==0, m=1; A = 1 + O(x); while( m\u003c=n, m*=2; A = subst(A, x, x^2) * (1-x)); polcoeff(A, n))};",
				"(PARI) a(n) = { 1 - 2 * (hammingweight(n) % 2) };  \\\\ _Gheorghe Coserea_, Aug 30 2015",
				"(PARI) apply( {A106400(n)=(-1)^hammingweight(n)}, [0..99]) \\\\ _M. F. Hasler_, Feb 07 2020",
				"(Haskell)",
				"import Data.List (transpose)",
				"a106400 n = a106400_list !! n",
				"a106400_list =  1 : concat",
				"   (transpose [map negate a106400_list, tail a106400_list])",
				"-- _Reinhard Zumkeller_, Apr 29 2012",
				"(MAGMA) [1-2*(\u0026+Intseq(n,2) mod(2)): n in [0..100]]; // _Vincenzo Librandi_, Sep 01 2015"
			],
			"xref": [
				"Convolution inverse of A018819.",
				"Cf. A010060 (0 -\u003e 1 \u0026 1 -\u003e -1), A000120, A000069, A001969, A003714, A005599, A038712, A005940, A008836, A132971."
			],
			"keyword": "sign,easy",
			"offset": "0,1",
			"author": "_Michael Somos_, May 02 2005",
			"references": 23,
			"revision": 112,
			"time": "2020-04-13T20:52:19-04:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}