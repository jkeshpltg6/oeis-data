{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A226164",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 226164,
			"data": "1,2,2,3,3,4,3,4,4,5,4,5,5,6,5,6,5,6,6,7,6,7,6,7,7,8,7,8,7,8,7,8,8,9,8,9,8,9,8,9,9,10,9,10,9,10,9,10,9,10,10,11,10,11,10,11,10,11,10,11,11,12,11,12,11,12,11,12,11,12,11,12,12,13,12,13,12,13,12,13,12",
			"name": "Sequence used for the quadratic irrational number belonging to the principal indefinite binary quadratic form.",
			"comment": [
				"For an indefinite binary quadratic form, denoted by [a, b, c] for F = F([a, b, c],[x, y]) = a*x^2 + b*x*y + c*y^2, the discriminant is D = b^2 - 4*a*c \u003e 0, not a square. See A079896 for the possible values.",
				"The principal form for a discriminant D, which is reduced (see the Scholz-Schoeneberg reference, p. 112), is defined as the unique form F_p(D) = [a=1, b(D), c(D)] with c(D) = -(D - b^2)/4. See the Buell reference, p. 26. One can show that b(D) = f(D) - 2 if D and f(D):=ceiling(sqrt(D(n))) have the same parity and b(D) = f(D) - 1 if D and f(D) have opposite parity. The principal root of a form [a, b, c] of discriminant D is omega(D) = (-b + sqrt(D))/2, the zero with positive square root of the polynomial P(x) = a*x^2 + b*x + c. See the Buell reference, p. 31 (and p. 18). We prefer to call omega the quadratic irrational belonging to the form F. For the principal form F_p(D) of discriminant D = D(n) = A079896(n), n \u003e= 0, this quadratic irrational is omega_p(D(n)) = (-b(D(n)) + sqrt(D))/2 where b(D(n)) is the present sequence a(n). (Note that this differs from the omega = omega(D) used in the Buell reference on p. 40 because another form of discriminant D has been chosen there, depending on the parity of D.)",
				"The (purely periodic) continued fraction expansion of omega_p(D(n)) plays a role for finding all solutions of the Pell equation x^2 + D(n)*y^2 = - 4 if a solution exists. See A226696 for these D values. For the Pell +4 equation which has solutions for every D(n) one finds the fundamental solution also from the continued fraction expansion of omega_p(D(n)).",
				"For more details see the W. Lang link \"Periods of indefinite Binary Quadratic Forms ...\" given in A225953."
			],
			"reference": [
				"D. A. Buell, Binary Quadratic Forms, Springer, 1989.",
				"A. Scholz and B. Schoeneberg, Einführung in die Zahlentheorie, Sammlung Goeschen Band 5131, Walter de Gruyter, 1973."
			],
			"formula": [
				"Define D(n) := A079896(n) and f(n) = ceiling(sqrt(D(n))).",
				"a(n) = f(n) - 2 if D(n) and f(n) have the same parity, and a(n) = f(n) - 1 if D(n) and f(n) have opposite parity."
			],
			"example": [
				"a(0) = 1 because D(0) = A079896(0) = 5 and f(0) = 3; both are odd, therefore a(0) = 3 - 2 = 1.",
				"a(1) = 2 from D(1) = 8, f(1) = 3, a(1) = f(1) - 1 = 2.",
				"The quadratic irrational (principal root) of the principal form of discriminant D(4) = 17 which is F_p(17) = [1, 3, -2], is omega_p(17) = (-3 + sqrt(17))/2 approximately 0.561552813.",
				"  f(17) = 5, a(4) = 5 - 2 = 3 = b(17)."
			],
			"xref": [
				"Cf. A079896, A226696, A225953."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Jul 20 2013",
			"references": 0,
			"revision": 8,
			"time": "2015-01-30T04:24:20-05:00",
			"created": "2013-07-21T18:21:04-04:00"
		}
	]
}