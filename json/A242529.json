{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A242529",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 242529,
			"data": "1,1,1,1,6,2,36,36,360,288,11016,3888,238464,200448,3176496,4257792,402573312,139511808,18240768000,11813990400,440506183680,532754620416,96429560832000,32681097216000,5244692024217600,6107246661427200,490508471914905600,468867166554931200,134183696369843404800",
			"name": "Number of cyclic arrangements (up to direction) of numbers 1,2,...,n such that any two neighbors are coprime.",
			"comment": [
				"a(n)=NPC(n;S;P) is the count of all neighbor-property cycles for a specific set S={1,2,...,n} of n elements and a specific pair-property P of \"being coprime\". For more details, see the link and A242519."
			],
			"link": [
				"S. Sykora, \u003ca href=\"http://dx.doi.org/10.3247/SL5Math14.002\"\u003eOn Neighbor-Property Cycles\u003c/a\u003e, \u003ca href=\"http://ebyte.it/library/Library.html#math\"\u003eStan's Library\u003c/a\u003e, Volume V, 2014.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Coprime_integers\"\u003eCoprime integers\u003c/a\u003e"
			],
			"formula": [
				"For n\u003e2, a(n) = A086595(n)/2."
			],
			"example": [
				"There are 6 such cycles of length n=5: C_1={1,2,3,4,5}, C_2={1,2,3,5,4},",
				"C_3={1,2,5,3,4}, C_4={1,2,5,4,3}, C_5={1,3,2,5,4}, and C_6={1,4,3,2,5}.",
				"For length n=6, the count drops to just 2:",
				"C_1={1,2,3,4,5,6}, C_2={1,4,3,2,5,6}."
			],
			"mathematica": [
				"A242529[n_] := Count[Map[lpf, Map[j1f, Permutations[Range[2, n]]]], 0]/2;",
				"j1f[x_] := Join[{1}, x, {1}];",
				"lpf[x_] := Length[Select[cpf[x], # != 1 \u0026]];",
				"cpf[x_] := Module[{i},",
				"   Table[GCD[x[[i]], x[[i + 1]]], {i, Length[x] - 1}]];",
				"Join[{1, 1}, Table[A242529[n], {n, 3, 10}]]",
				"(* OR, a less simple, but more efficient implementation. *)",
				"A242529[n_, perm_, remain_] := Module[{opt, lr, i, new},",
				"   If[remain == {},",
				"     If[GCD[First[perm], Last[perm]] == 1, ct++];",
				"     Return[ct],",
				"     opt = remain; lr = Length[remain];",
				"     For[i = 1, i \u003c= lr, i++,",
				"      new = First[opt]; opt = Rest[opt];",
				"      If[GCD[Last[perm], new] != 1, Continue[]];",
				"      A242529[n, Join[perm, {new}],",
				"       Complement[Range[2, n], perm, {new}]];",
				"      ];",
				"     Return[ct];",
				"     ];",
				"   ];",
				"Join[{1, 1},Table[ct = 0; A242529[n, {1}, Range[2, n]]/2, {n, 3, 12}] ](* _Robert Price_, Oct 25 2018 *)"
			],
			"program": [
				"(C++) See the link."
			],
			"xref": [
				"Cf. A242519, A242520, A242521, A242522, A242523, A242524, A242525, A242526, A242527, A242528, A242530, A242531, A242532, A242533, A242534."
			],
			"keyword": "nonn,hard",
			"offset": "1,5",
			"author": "_Stanislav Sykora_, May 30 2014",
			"ext": [
				"a(1) corrected, a(19)-a(29) added by _Max Alekseyev_, Jul 04 2014"
			],
			"references": 16,
			"revision": 20,
			"time": "2018-10-25T17:25:43-04:00",
			"created": "2014-05-30T12:47:06-04:00"
		}
	]
}