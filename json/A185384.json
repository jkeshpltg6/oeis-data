{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A185384",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 185384,
			"data": "1,2,1,5,6,2,13,24,15,3,34,84,78,32,5,89,275,340,210,65,8,233,864,1335,1100,510,126,13,610,2639,4893,5040,3115,1155,238,21,1597,7896,17080,21112,16310,8064,2492,440,34,4181,23256,57492,82908,76860,47502,19572,5184,801,55",
			"name": "A binomial transform of Fibonacci numbers.",
			"comment": [
				"Triangle begins:",
				"     1,",
				"     2,    1,",
				"     5,    6,     2,",
				"    13,   24,    15,    3,",
				"    34,   84,    78,    32,     5,",
				"    89,  275,   340,   210,    65,    8,",
				"   233,  864,  1335,  1100,   510,  126,   13,",
				"   610, 2639,  4893,  5040,  3115, 1155,  238,  21,",
				"  1597, 7896, 17080, 21112, 16310, 8064, 2492, 440, 34,",
				"  ...",
				"Diagonal: a(n,n) = F(n+1).",
				"First column: a(n,0) = F(2n+1) (A001519).",
				"Row sums: Sum_{k=0..n} a(n,k) = F(3n+1) (A033887).",
				"Alternated row sums: Sum_{k=0..n} (-1)^k * a(n,k) = 1.",
				"Diagonal sums: Sum_{k=0..floor(n/2)} a(n-k,k) = A208481(n).",
				"Alternated diagonal sums: Sum_{k=0..floor(n/2)} (-1)^k * a(n-k,k) = F(n+3)-1 (A000071).",
				"Row square-sums: Sum_{k=0..n} a(n,k)^2 = A208588(n).",
				"Central coefficients: a(2*n,n) = binomial(2n,n)*F(3n+1) (A208473), where F(n) are the Fibonacci numbers (A000045).",
				"Mirror image of the triangle in A122070. - _Philippe Deléham_, Mar 13 2012",
				"Subtriangle of (1, 1, 1, 0, 0, 0, 0, 0, 0, 0, ...) DELTA (0, 1, 1, -1, 0, 0, 0, 0, 0, 0, 0, ...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, Mar 13 2012"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A185384/b185384.txt\"\u003eTable of n, a(n) for the first 100 rows, flattened\u003c/a\u003e"
			],
			"formula": [
				"a(n,k) = Sum_{i=k..n} binomial(n,i)*binomial(i,k)*F(i+1).",
				"a(n,k) = binomial(n,i) * Sum_{i=k..n} binomial(n-k,n-i)*F(i+1).",
				"Explicit form: a(n,k) = binomial(n,k)*F(2*n-k+1).",
				"G.f.: (1-x)/(1-3*x+x^2-x*y-x^2*y-x^2*y^2).",
				"Recurrence: a(n+2,k+2) = 3*a(n+1,k+2) + a(n+1,k+1) - a(n,k+2) + a(n,k+1) + a(n,k).",
				"T(n,k) = A122070(n,n-k). - _Philippe Deléham_, Mar 13 2012"
			],
			"example": [
				"From _Philippe Deléham_, Mar 13 2012: (Start)",
				"(1, 1, 1, 0, 0, 0, ...) DELTA (0, 1, 1, -1, 0, 0, ...) begins:",
				"    1;",
				"    1,   0;",
				"    2,   1,    0;",
				"    5,   6,    2,    0;",
				"   13,  24,   15,    3,   0;",
				"   34,  84,   78,   32,   5,   0;",
				"   89, 275,  340,  210,  65,   8,  0;",
				"  233, 864, 1335, 1100, 510, 126, 13, 0;",
				"  ... (End)"
			],
			"mathematica": [
				"Flatten[Table[Sum[Binomial[n,i]Binomial[i,k]Fibonacci[i+1],{i,k,n}],{n,0,20},{k,0,n}]]",
				"CoefficientList[Series[CoefficientList[Series[(1 - x)/(1 - 3*x + x^2 - x*y - x^2*y - x^2*y^2), {x, 0, 10}], x], {y, 0, 10}], y] // Flatten (* _G. C. Greubel_, Jun 28 2017 *)"
			],
			"program": [
				"(Maxima) create_list(binomial(n,k)*fib(2*n-k+1),n,0,20,k,0,n);",
				"(PARI) for(n=0,10, for(k=0,n, print1(sum(i=k,n, binomial(n,i) * binomial(i,k) * fibonacci(i+1)), \", \"))) \\\\ _G. C. Greubel_, Jun 28 2017"
			],
			"xref": [
				"Cf. A000045, A001519, A033887, A208481, A000071, A208588, A208473.",
				"Cf. A122070."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "0,2",
			"author": "_Emanuele Munarini_, Feb 29 2012",
			"references": 7,
			"revision": 34,
			"time": "2020-01-22T02:42:06-05:00",
			"created": "2012-03-12T19:14:57-04:00"
		}
	]
}