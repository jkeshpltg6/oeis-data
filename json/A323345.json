{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A323345",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 323345,
			"data": "1,2,1,2,1,1,3,2,1,1,2,1,1,1,1,4,2,2,1,1,1,2,2,1,1,1,1,1,4,2,2,2,1,1,1,1,3,1,1,1,1,1,1,1,1,4,3,2,2,2,1,1,1,1,1,2,2,2,1,1,1,1,1,1,1,1,6,2,2,2,2,2,1,1,1,1,1,1,2,2,1,1,1,1,1,1,1,1,1,1,1",
			"name": "Square array read by ascending antidiagonals: T(n, k) is the number of partitions of n where parts, if sorted in ascending order, form an arithmetic progression (AP) with common difference of k; n \u003e= 1, k \u003e= 0.",
			"comment": [
				"T(n, k) is the number of positive integers in the sequence defined, for all i \u003e= 1, by x_1 = n and x_i = (i-1)*(x_(i-1)-k)/i; or defined equivalently by x_i=n/i-(k/2)*(i-1). An x_i positive and integer characterizes the AP-partition with smallest part x_i and number of parts i.",
				"T(n, k) is the number of i, positive integers, such that n - P(k+2,i) is both nonnegative and divisible by i, where P(r,i) denotes the i-th r-gonal number (see A057145)."
			],
			"formula": [
				"T(n, 0) = A000005(n), the number of divisors of n.",
				"T(n, 1) = A001227(n), the number of odd divisors of n.",
				"T(n, 2) = A038548(n), the number of divisors of n that are at most sqrt(n).",
				"T(n, 3) = A117277(n).",
				"The g.f. for column d is Sum_{k\u003e=1} x^(k*(d*k-d+2)/2)/(1-x^k) [information taken from A117277]. - _Joerg Arndt_, May 05 2020"
			],
			"example": [
				"There are 4 partitions of 150 such that the parts form an arithmetic progression with common difference of 9:",
				"150 = 150",
				"150 = 41 + 50 + 59",
				"150 = 24 + 33 + 42 + 51",
				"150 = 12 + 21 + 30 + 39 + 48",
				"Then, T(150,9) = 4.",
				"Array begins:",
				"     k 0 1 2 3 4 5 6 7 8 9",
				"   n +--------------------",
				"   1 | 1 1 1 1 1 1 1 1 1 1",
				"   2 | 2 1 1 1 1 1 1 1 1 1",
				"   3 | 2 2 1 1 1 1 1 1 1 1",
				"   4 | 3 1 2 1 1 1 1 1 1 1",
				"   5 | 2 2 1 2 1 1 1 1 1 1",
				"   6 | 4 2 2 1 2 1 1 1 1 1",
				"   7 | 2 2 1 2 1 2 1 1 1 1",
				"   8 | 4 1 2 1 2 1 2 1 1 1",
				"   9 | 3 3 2 2 1 2 1 2 1 1",
				"  10 | 4 2 2 1 2 1 2 1 2 1"
			],
			"mathematica": [
				"T[n_, k_] :=",
				"Module[{c = 0, i = 1, x = n},",
				"  While[x \u003e= 1, If[IntegerQ[x], c++]; i++; x = (i-1)*(x-k)/i]; c]",
				"A004736[n_] := Binomial[Floor[3/2 + Sqrt[2*n]], 2] - n + 1",
				"A002260[n_] := n - Binomial[Floor[1/2 + Sqrt[2*n]], 2]",
				"a[n_] := T[A004736[n], A002260[n] - 1]",
				"Table[a[n], {n, 1, 91}]",
				"(* Second program: *)",
				"nmax = 14;",
				"col[k_] := col[k] = CoefficientList[Sum[x^(n(k n - k + 2)/2 - 1)/(1 - x^n), {n, 1, nmax}] + O[x]^nmax, x];",
				"T[n_, k_] := col[k][[n]];",
				"Table[T[n-k, k], {n, 1, nmax}, {k, 0, n-1}] // Flatten (* _Jean-François Alcover_, Nov 30 2020 *)"
			],
			"program": [
				"(PARI)",
				"T(n,k)=c=0;i=1;x=n;while(x\u003e=1,if(frac(x)==0,c++);i++;x=n/i-(k/2)*(i-1));c",
				"for(s=1,13,for(k=0,s-1,n=s-k;print1(T(n,k),\", \")))"
			],
			"xref": [
				"Cf. A000005, A001227, A038548, A117277.",
				"Cf. A057145."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Luc Rousseau_, Jan 11 2019",
			"references": 6,
			"revision": 21,
			"time": "2020-11-30T08:54:53-05:00",
			"created": "2019-01-24T02:02:21-05:00"
		}
	]
}