{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A075778",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 75778,
			"data": "7,5,4,8,7,7,6,6,6,2,4,6,6,9,2,7,6,0,0,4,9,5,0,8,8,9,6,3,5,8,5,2,8,6,9,1,8,9,4,6,0,6,6,1,7,7,7,2,7,9,3,1,4,3,9,8,9,2,8,3,9,7,0,6,4,6,0,8,0,6,5,5,1,2,8,0,8,1,0,9,0,7,3,8,2,2,7,0,9,2,8,4,2,2,5,0,3,0,3,6,4,8,3,7,7",
			"name": "Decimal expansion of the real root of x^3 + x^2 - 1.",
			"comment": [
				"Also decimal expansion of the root of x^(1/sqrt(x+1)) = (1/sqrt(x+1))^x. The root of (1/x)^(1/sqrt(x+1)) = (1/sqrt(x+1))^(1/x) is the golden ratio. - _Michel Lagneau_, Apr 17 2012",
				"The following decomposition holds true: X^3 + X^2 - 1 = (X - r)*(X + i * e^(-i*a) * r^(-1/2))*(X - i * e^(i*a) * r^(-1/2)), where a = arcsin(1/(2*r^(3/2))), see A218197 for the decimal expansion of a and the paper of Witula et al. for details. - _Roman Witula_, Oct 22 2012"
			],
			"reference": [
				"Roman Witula, E. Hetmaniok, D. Slota, Sums of the powers of any order roots taken from the roots of a given polynomial, submitted to Proceedings of the 15th International Conference on Fibonacci Numbers and Their Applications, Eger, Hungary, 2012."
			],
			"link": [
				"H. R. P. Ferguson, \u003ca href=\"http://www.fq.math.ca/Scanned/14-3/ferguson.pdf\"\u003eOn a Generalization of the Fibonacci Numbers Useful in Memory Allocation Schema or All About the Zeroes of Z^k - Z^{k - 1} - 1, k \u003e 0\u003c/a\u003e, Fibonacci Quarterly, Volume 14, Number 3, October, 1976 (see Table 2 p. 238)."
			],
			"formula": [
				"Let 0 \u003c a \u003c 1 be any real number. Then a is the lesser and 1 is the greater and a^2/1 = 1/(a+1) and a^3 + a^2 - 1 = 0. Solving this using PARI we have 0.7548776662466927600495088964... . The general cubic can also be solved in radicals.",
				"Equals -(1/3) + (1/3)*(25/2 - (3*sqrt(69))/2)^(1/3) + (1/3)*((1/2)*(25 + 3*sqrt(69)))^(1/3)."
			],
			"example": [
				"0.7548776662466927600495088963585286918946066..."
			],
			"maple": [
				"A075778 := proc()",
				"        1/3-root[3](25/2-3*sqrt(69)/2)/3 -root[3](25/2+3*sqrt(69)/2)/3;",
				"          -% ;",
				"end proc: # _R. J. Mathar_, Jan 22 2013"
			],
			"mathematica": [
				"RealDigits[N[Solve[x^3 + x^2 - 1 == 0, x] [[1]] [[1, 2]], 111]] [[1]]",
				"RealDigits[x /. FindRoot[x^3 + x^2 == 1, {x, 1}, WorkingPrecision -\u003e 120]][[1]] (* _Harvey P. Dale_, Nov 23 2012 *)"
			],
			"program": [
				"(PARI) solve(x=0, 1, x^3+x^2-1)",
				"(PARI) polrootsreal(x^3 + x^2 - 1)[1] \\\\ _Charles R Greathouse IV_, Jul 23 2020"
			],
			"xref": [
				"Cf. A060006 (inverse), A210462, A210463."
			],
			"keyword": "nonn,cons",
			"offset": "0,1",
			"author": "_Cino Hilliard_, Oct 09 2002",
			"ext": [
				"More terms from _Robert G. Wilson v_, Oct 10 2002"
			],
			"references": 22,
			"revision": 49,
			"time": "2021-09-10T22:14:11-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}