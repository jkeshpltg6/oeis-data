{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A168017",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 168017,
			"data": "1,1,2,1,3,1,2,5,1,7,1,2,3,11,1,15,1,2,5,22,1,3,30,1,2,7,42,1,56,1,2,3,5,11,77,1,101,1,2,15,135,1,3,7,176,1,2,5,22,231,1,297,1,2,3,11,30,385,1,490,1,2,5,7,42,627,1,3,15,792,1,2,56,1002",
			"name": "Triangle read by rows in which row n lists the number of partitions of n into parts divisible by d, where d is a divisor of n listed in decreasing order.",
			"comment": [
				"Positive values of triangle A168016.",
				"The number of terms of row n is equal to the number of divisors of n: A000005(n).",
				"Note that the last term of each row is the number of partitions of n: A000041(n).",
				"Also, it appears that row n lists the partition numbers of the divisors of n. [_Omar E. Pol_, Nov 23 2009]"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A168017/b168017.txt\"\u003eRows n = 1..1400, flattened\u003c/a\u003e",
				"Omar E. Pol, \u003ca href=\"http://www.polprimos.com/imagenespub/polpa3dt.jpg\"\u003eIllustration of the partitions of n, for n = 1 .. 9\u003c/a\u003e"
			],
			"example": [
				"Consider row n=8: (1, 2, 5, 22). The divisors of 8 listed in decreasing order are 8, 4, 2, 1 (see A056538). There is 1 partition of 8 into parts divisible by 8. Also, there are 2 partitions of 8 into parts divisible by 4: {(8), (4+4)}; 5 partitions of 8 into parts divisible by 2: {(8), (6+2), (4+4), (4+2+2), (2+2+2+2)}; and 22 partitions of 8 into parts divisible by 1, because A000041(8)=22. Then row 8 is formed by 1, 2, 5, 22.",
				"Triangle begins:",
				"1;",
				"1,  2;",
				"1,  3;",
				"1,  2,  5;",
				"1,  7;",
				"1,  2,  3, 11;",
				"1, 15;",
				"1,  2,  5, 22;",
				"1,  3, 30;",
				"1,  2,  7, 42;",
				"1, 56;",
				"1,  2,  3,  5, 11, 77;"
			],
			"maple": [
				"with(numtheory):",
				"b:= proc(n, i, d) option remember;",
				"      if n\u003c0 then 0",
				"    elif n=0 then 1",
				"    elif i\u003c1 then 0",
				"    else b(n, i-d, d) +b(n-i, i, d)",
				"      fi",
				"    end:",
				"T:= proc(n) local l;",
				"      l:= sort([divisors(n)[]],`\u003e`);",
				"      seq(b(n, n, l[i]), i=1..nops(l))",
				"    end:",
				"seq(T(n), n=1..30); # _Alois P. Heinz_, Oct 21 2011"
			],
			"mathematica": [
				"b[n_, i_, d_] := b[n, i, d] = Which[n\u003c0, 0, n==0, 1, i\u003c1, 0, True, b[n, i - d, d] + b[n-i, i, d]]; T[n_] := Module[{l = Divisors[n] // Reverse}, Table[b[n, n, l[[i]]], {i, 1, Length[l]}]]; Table[T[n], {n, 1, 30}] // Flatten (* _Jean-François Alcover_, Dec 03 2015, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Row sums give A047968.",
				"Cf. A000005, A000041, A056538, A135010, A138121, A168016, A168018, A168019, A168020, A168021."
			],
			"keyword": "nonn,look,tabf",
			"offset": "1,3",
			"author": "_Omar E. Pol_, Nov 22 2009",
			"references": 7,
			"revision": 22,
			"time": "2017-04-02T00:33:36-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}