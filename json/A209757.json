{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A209757",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 209757,
			"data": "1,3,2,5,8,4,7,18,20,8,9,32,56,48,16,11,50,120,160,112,32,13,72,220,400,432,256,64,15,98,364,840,1232,1120,576,128,17,128,560,1568,2912,3584,2816,1280,256,19,162,816,2688,6048,9408,9984,6912,2816,512",
			"name": "Triangle of coefficients of polynomials v(n,x) jointly generated with A013609; see the Formula section.",
			"comment": [
				"For a discussion and guide to related arrays, see A208510.",
				"Subtriangle of the triangle given by (1, 2, -2, 1, 0, 0, 0, 0, 0, 0, 0, ...) DELTA (0, 2, 0, 0, 0, 0, 0, 0, 0, ...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, Mar 24 2012"
			],
			"formula": [
				"u(n,x) = x*u(n-1,x) + x*v(n-1,x) + 1,",
				"v(n,x) = (x+1)*u(n-1,x) + (x+1)*v(n-1,x) + 1,",
				"where u(1,x)=1, v(1,x)=1.",
				"From _Philippe Deléham_, Mar 24 2012: (Start)",
				"As DELTA-triangle T(n,k) with 0 \u003c= k \u003c= n:",
				"G.f.: (1 - x - 2*y*x + 2*x^2 + 2*x^2*y)/(1 - 2*x - 2*y*x + x^2 + 2*y*x^2).",
				"T(n,k) = 2*T(n-1,k) + 2*T(n-1,k-1) - T(n-2,k) - 2*T(n-2,k-2), T(0,0) = T(1,0) = 1, T(1,1) = T(2,2) = 0, T(2,0) = 3, T(2,1) = 2 and T(n,k) = 0 if k \u003c 0 or if k \u003e n.",
				"T(n,k) = 2^k*binomial(n-1,k)*(2*n-k-1)/(k+1). (End)",
				"From _Peter Bala_, Dec 21 2014: (Start)",
				"Following remarks assume an offset of 0.",
				"T(n,k) = 2^k * A110813(n,k).",
				"Riordan array ((1+x)/(1-x)^2, 2*x/(1-x)).",
				"exp(2*x) * e.g.f. for row n = e.g.f. for diagonal n. For example, for n = 3 we have exp(2*x)*(7 + 18*x + 20*x^2/2! + 8*x^3/3!) = 7 + 32*x + 120*x^2/2! + 400*x^3/3! + 1232*x^4/4! + .... The same property holds more generally for Riordan arrays of the form (f(x), 2*x/(1-x)). (End)"
			],
			"example": [
				"First five rows:",
				"  1;",
				"  3,  2;",
				"  5,  8,  4;",
				"  7, 18, 20,  8;",
				"  9, 32, 56, 48, 16;",
				"First three polynomials v(n,x):",
				"  1",
				"  3 + 2x",
				"  5 + 8x + 4x^2.",
				"From _Philippe Deléham_, Mar 24 2012: (Start)",
				"(1, 2, -2, 1, 0, 0, ...) DELTA (0, 2, 0, 0, 0, ...) begins:",
				"  1;",
				"  1,  0;",
				"  3,  2,  0;",
				"  5,  8,  4,  0;",
				"  7, 18, 20,  8,  0;",
				"  9, 32, 56, 48, 16,  0; (End)"
			],
			"mathematica": [
				"u[1, x_] := 1; v[1, x_] := 1; z = 16;",
				"u[n_, x_] := x*u[n - 1, x] + x*v[n - 1, x] + 1;",
				"v[n_, x_] := (x + 1)*u[n - 1, x] + (x + 1)*v[n - 1, x] + 1;",
				"Table[Expand[u[n, x]], {n, 1, z/2}]",
				"Table[Expand[v[n, x]], {n, 1, z/2}]",
				"cu = Table[CoefficientList[u[n, x], x], {n, 1, z}];",
				"TableForm[cu]",
				"Flatten[%]    (* A013609 *)",
				"Table[Expand[v[n, x]], {n, 1, z}]",
				"cv = Table[CoefficientList[v[n, x], x], {n, 1, z}];",
				"TableForm[cv]",
				"Flatten[%]    (* A209757 *)"
			],
			"xref": [
				"Cf. A013609, A208510, A110813."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Mar 23 2012",
			"references": 2,
			"revision": 17,
			"time": "2020-01-26T17:46:16-05:00",
			"created": "2012-03-24T12:28:01-04:00"
		}
	]
}