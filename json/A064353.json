{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A064353",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 64353,
			"data": "1,3,3,3,1,1,1,3,3,3,1,3,1,3,3,3,1,1,1,3,3,3,1,3,3,3,1,3,3,3,1,1,1,3,3,3,1,3,1,3,3,3,1,1,1,3,3,3,1,3,3,3,1,1,1,3,3,3,1,3,3,3,1,1,1,3,3,3,1,3,1,3,3,3,1,1,1,3,3,3,1,3,3,3,1,3,3,3,1,1,1,3,3,3,1,3,1,3,3,3,1,1,1,3,3",
			"name": "Kolakoski-(1,3) sequence: the alphabet is {1,3}, and a(n) is the length of the n-th run.",
			"comment": [
				"Historical note: the sequence (a(n)) was introduced (by me) in 1981 in a seminar in Bordeaux. It was remarked there that (a(n+1)) is a morphic sequence, i.e., a letter-to-letter projection of a fixed point of a morphism. The morphism is 1-\u003e3, 2-\u003e2, 3-\u003e343, 4-\u003e212. The letter-to-letter map is 1-\u003e1, 2-\u003e1, 3-\u003e3, 4-\u003e3. There it was also remarked that this allows one to compute the frequency of the letter 3, and an exact expression for this frequency involving sqrt(177) was given. - _Michel Dekking_, Jan 06 2018",
				"The frequency of the number '3' is 0.6027847... See UWC link. - _Jaap Spies_, Dec 12 2004",
				"13, 13331, 13331113331 are primes. - _Vincenzo Librandi_, Mar 02 2016",
				"Consider the Kolakoski sequence generalized to the alphabet {A,B}, where A=2p+1, B=2q+1. The fraction of symbols that are A approaches f_A, calculated as follows: x=(p+q+1)/3; y=((p-q)^2)/2; lambda = x + (x^3+y+sqrt(y^2+2*x^3*y))^(1/3) + (x^3+y-sqrt(y^2+2*x^3*y))^(1/3); f_A=(lambda-2q-1)/(2p-2q). The technique is the \"simple computation\" mentioned by Dekking and repeated in the UWC link. - _Ed Wynn_, Jul 29 2019"
			],
			"reference": [
				"F. M. Dekking: \"What is the long range order in the Kolakoski sequence?\" in: The Mathematics of Long-Range Aperiodic Order, ed. R. V. Moody, Kluwer, Dordrecht (1997), pp. 115-125.",
				"E. Angelini, \"Jeux de suites\", in Dossier Pour La Science, pp. 32-35, Volume 59 (Jeux math'), April/June 2008, Paris."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A064353/b064353.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Michael Baake and Bernd Sing, \u003ca href=\"https://arxiv.org/abs/math/0206098\"\u003eKolakoski-(3,1) is a (deformed) model set\u003c/a\u003e, arXiv:math/0206098 [math.MG], 2002-2003.",
				"F. M. Dekking, \u003ca href=\"http://www.digizeitschriften.de/dms/img/?PID=GDZPPN002544490\"\u003eOn the structure of self-generating sequences\u003c/a\u003e, Seminar on Number Theory, 1980-1981 (Talence, 1980-1981), Exp. No. 31, 6 pp., Univ. Bordeaux I, Talence, 1981. Math. Rev. 83e:10075.",
				"F. M. Dekking, \u003ca href=\"http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.28.6839\"\u003eWhat Is the Long Range Order in the Kolakoski Sequence?\u003c/a\u003e, Report 95-100, Technische Universiteit Delft, 1995.",
				"Jaap Spies, \u003ca href=\"http://www.jaapspies.nl/bookb5.pdf\"\u003eA Bit of Math, The Art of Problem Solving\u003c/a\u003e, Jaap Spies Publishers (2019).",
				"UWC, \u003ca href=\"http://www.math.leidenuniv.nl/%7Enaw/serie5/deel05/jun2004/pdf/uwc.pdf\"\u003eOpgave A\u003c/a\u003e (\u003ca href=\"http://www.jaapspies.nl/mathfiles/opgave2004-2A.pdf\"\u003esolution\u003c/a\u003e)"
			],
			"mathematica": [
				"A = {1, 3, 3, 3}; i = 3; next = 1; While[Length[A] \u003c 140, A = Join[A, next*Array[1\u0026, A[[i]]]]; i++; next = 4-next]; A (* _Jean-François Alcover_, Nov 12 2016, translated from MATLAB *)"
			],
			"program": [
				"(MATLAB) A = [1 3 3 3]; i = 3; next = 1; while length(A) \u003c 140 A = [A next*ones(1, A(i))]; i = i + 1; next = 4 - next; end",
				"(Haskell) -- from John Tromp's a000002.hs",
				"a064353 n = a064353_list !! (n-1)",
				"a064353_list = 1 : 3 : drop 2",
				"   (concat . zipWith replicate a064353_list . cycle $ [1, 3])",
				"-- _Reinhard Zumkeller_, Aug 02 2013"
			],
			"xref": [
				"Cf. A000002, A071820, A071907, A071928, A071942."
			],
			"keyword": "nonn,easy,nice",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _David Wasserman_, Jul 16 2002",
				"Edited by _Charles R Greathouse IV_, Apr 20 2010",
				"Restored the original definition, following a suggestion from _Jianing Song_. - _N. J. A. Sloane_, May 13 2021"
			],
			"references": 12,
			"revision": 62,
			"time": "2021-05-13T23:41:09-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}