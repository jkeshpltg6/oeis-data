{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A096726",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 96726,
			"data": "1,3,9,12,21,18,36,24,45,12,54,36,84,42,72,72,93,54,36,60,126,96,108,72,180,93,126,12,168,90,216,96,189,144,162,144,84,114,180,168,270,126,288,132,252,72,216,144,372,171,279,216,294,162,36,216,360,240,270,180,504",
			"name": "Expansion of eta(q^3)^10 / (eta(q) * eta(q^9))^3 in powers of q.",
			"comment": [
				"Cubic AGM theta functions: a(q) (see A004016), b(q) (A005928), c(q) (A005882)."
			],
			"reference": [
				"B. C. Berndt, Ramanujan's Notebooks Part III, Springer-Verlag, see p. 475 Entry 7(i)."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A096726/b096726.txt\"\u003eTable of n, a(n) for n = 0..16384\u003c/a\u003e",
				"B. C. Berndt, S. H. Chan, Z.-G. Liu and H. Yesilyurt, \u003ca href=\"http://www.math.uiuc.edu/~berndt/articles/partitions1.pdf\"\u003eA new identity for (q;q)10 [inf] with an application to Ramanujan's partition congruence modulo 11\u003c/a\u003e, Quart. J. of Math., 55 (2004), pp. 13-30.",
				"J. M. Borwein and P. B. Borwein, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9947-1991-1010408-0\"\u003eA cubic counterpart of Jacobi's identity and the AGM\u003c/a\u003e, Trans. Amer. Math. Soc., 323 (1991), no. 2, 691-701. MR1010408 (91e:33012)."
			],
			"formula": [
				"G.f. Prod_{k\u003e0} (1 - x^(3*k))^10 / ((1 - x^k) * (1 - x^(9*k)))^3 = 1 + Sum_{k\u003e0} k * (3*x^k / (1 - x^k) - 27 * x^(9*k) / (1 - x^(9*k))).",
				"Euler transform of period 9 sequence [ 3, 3, -7, 3, 3, -7, 3, 3, -4, ...].",
				"a(n) = 3 * b(n) where b(n) is multiplicative and b(3^e) = 1 + 3*(e\u003e0), b(p^e) = (p^(e+1) - 1) / (p - 1) otherwise.",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2), A(x^4)) where f(u, v, w) = u^2*w + 4*u*w^2 + v^3 - 6*u*v*w.",
				"Expansion of b(q^3)^3 / b(q) = c(q)^3 / (9*c(q^3)) = (a(q)^2 + 3*a(q^3)^2) / 4 = (a(q)^2 + a(q)*b(q) + b(q)^2) / 3 in powers of q where a(), b(), c() are cubic AGM theta functions.",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (9 t)) = 9 (t/i)^2 f(t) where q = exp(2 Pi i t). - _Michael Somos_, Aug 25 2014",
				"a(3*n + 2) = A281722(3*n + 2) + 27 * A033686(n). a(n) == A281722(n) (mod 27). - _Michael Somos_, Sep 04 2017"
			],
			"example": [
				"G.f. = 1 + 3*x + 9*x^2 + 12*x^3 + 21*x^4 + 18*x^5 + 36*x^6 + 24*x^7 + 45*x^8 + ..."
			],
			"mathematica": [
				"CoefficientList[ Series[1 + Sum[k(3x^k/(1 - x^k) - 27x^(9k)/(1 - x^(9k))), {k, 1, 60}], {x, 0, 60}], x] (* _Robert G. Wilson v_, Jul 14 2004 *)",
				"a[ n_] := If[ n \u003c 1, Boole[ n == 0], 3 Sum[ If[ Mod[ d, 9] \u003e 0, d, 0], {d, Divisors @ n }]]; (* _Michael Somos_, Aug 25 2014 *)",
				"a[ n_] := SeriesCoefficient[ QPochhammer[ q^3]^10 / (QPochhammer[ q] QPochhammer[ q^9])^3, {q, 0, n}]; (* _Michael Somos_, Aug 25 2014 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, n==0, 3 * sigma(n) - if( n%9==0, 27 * sigma(n/9)))};",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^3 + A)^10 / (eta(x + A) * eta(x^9 + A))^3, n))};",
				"(PARI) {a(n) = polcoeff( sum(k=1, n, k*3* (x^k / (1 - x^k) - 9*x^(9*k) / (1 - x^(9*k))), 1 + x * O(x^n)), n)};",
				"(MAGMA) A := Basis( ModularForms( Gamma0(9), 2), 61); A[1] + 3*A[2] + 9*A[3]; /* _Michael Somos_, Aug 25 2014 */"
			],
			"xref": [
				"Cf. A033686, A116607, A281722."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Michael Somos_, Jul 06 2004",
			"references": 5,
			"revision": 21,
			"time": "2019-12-28T10:11:36-05:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}