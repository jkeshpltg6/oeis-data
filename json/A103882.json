{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A103882",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 103882,
			"data": "1,2,12,92,780,7002,65226,623576,6077196,60110030,601585512,6078578508,61908797418,634756203018,6545498596110,67830161708592,705951252118284,7375213677918294,77310179609631564,812839595630249540,8569327862277434280,90562666977432643862",
			"name": "a(n) = Sum_{i=0..n} C(n+1,i)*C(n-1,i-1)*C(2n-i,n).",
			"comment": [
				"Number of permutations of n copies of 1..3 with all adjacent differences \u003c= 1 in absolute value. - _R. H. Hardin_, May 06 2010 [Cf. A177316. - _Peter Bala_, Jan 14 2020]"
			],
			"link": [
				"R. H. Hardin and Alois P. Heinz, \u003ca href=\"/A103882/b103882.txt\"\u003eTable of n, a(n) for n = 0..950\u003c/a\u003e (terms n=1..94 from R. H. Hardin)",
				"A. Straub, \u003ca href=\"https://arxiv.org/abs/1401.0854\"\u003eMultivariate Apéry numbers and supercongruences of rational functions\u003c/a\u003e, arXiv:1401.0854 [math.NT] (2014)."
			],
			"formula": [
				"a(n) = (A005258(n-1)+3*A005258(n))/5 (Apéry numbers). - _Mark van Hoeij_, Jul 13 2010",
				"n^2*(n-1)*(5*n-8)*a(n) = (n-1)*(55*n^3-143*n^2+102*n-24)*a(n-1) + n*(n-2)^2*(5*n-3)*a(n-2). - _Alois P. Heinz_, Jun 29 2015",
				"a(n) ~ phi^(5*n + 3/2) / (2*Pi*5^(1/4)*n), where phi = A001622 = (1+sqrt(5))/2 is the golden ratio. - _Vaclav Kotesovec_, Jul 21 2019",
				"From _Peter Bala_, Jan 14 2020: (Start)",
				"a(n) = Sum_{k = 0..n} C(n,k)^2*C(n+k-1,k). Cf. A005258.",
				"For any prime p \u003e= 5, a(n*p^k) == a(n*p^(k-1)) ( mod p^(3*k) ) for all positive integers n and k (follows from known congruences satisfied by the Apéry numbers A005258 - see Straub, Example 3.4). (End)",
				"a(n) = hypergeom([-n, -n, n], [1, 1], 1). - _Peter Luschny_, Jan 19 2020",
				"From _Peter Bala_, Dec 19 2020: (Start)",
				"a(n) = Sum_{k = 1..n} C(n,k)*C(n+k,k)*C(n-1,k-1) for n \u003e=1.",
				"a(n) = [x^n] P(n, (1 + x)/(1 - x)), where P(n,x) denotes the n-th Legendre polynomial. Cf. A156554. (End)"
			],
			"maple": [
				"a:= proc(n) option remember; `if`(n\u003c2, n+1,",
				"      ((n-1)*(55*n^3-143*n^2+102*n-24)*a(n-1)+",
				"      n*(5*n-3)*(n-2)^2*a(n-2))/((n-1)*(5*n-8)*n^2))",
				"    end:",
				"seq(a(n), n=0..30);  # _Alois P. Heinz_, Jun 29 2015",
				"# Alternative:",
				"a := n -\u003e hypergeom([-n, -n, n], [1, 1], 1):",
				"seq(simplify(a(n)), n=0..21); # _Peter Luschny_, Jan 19 2020"
			],
			"mathematica": [
				"Drop[Table[Sum[Sum[Multinomial[r, g, n + 1 - r - g] Binomial[n - 1,n - r] Binomial[n - 1, n - g], {g, 1, n}], {r, 1, n}], {n, 0, 18}], 1] (* _Geoffrey Critzer_, Jun 29 2015 *)",
				"Table[Sum[Binomial[n+1,k]Binomial[n-1,k-1]Binomial[2n-k,n],{k,0,n}],{n,0,30}] (* _Harvey P. Dale_, Jun 19 2021 *)"
			],
			"program": [
				"(MAGMA) [1] cat [\u0026+[Binomial(n+1, i)*Binomial(n-1, i-1) * Binomial(2*n-i, n): i in [0..n]]:n in  [1..21]]; // _Marius A. Burtea_, Jan 19 2020",
				"(MAGMA) [\u0026+[Binomial(n, k)^2*Binomial(n+k-1, k): k in [0..n]]:n in  [0..21]]; // _Marius A. Burtea_, Jan 19 2020",
				"(PARI) a(n) = polcoef(pollegendre(n, (1 + x)/(1 - x)) + O(x^(n+1)), n); \\\\ _Michel Marcus_, Dec 20 2020"
			],
			"xref": [
				"Equals A103881(n, n). Cf. A005258, A177316, A156554.",
				"Row n=3 of A331562."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Ralf Stephan_, Feb 20 2005",
			"ext": [
				"a(0)=1 prepended by _Alois P. Heinz_, Jun 29 2015"
			],
			"references": 7,
			"revision": 58,
			"time": "2021-10-06T14:18:07-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}