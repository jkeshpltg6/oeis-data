{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A292289",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 292289,
			"data": "6,12,14,30,33,56,60,39,64,132,138,182,189,110,84,306,315,380,390,174,272,552,564,155,402,360,259,870,885,992,1008,405,624,609,258,1406,1425,754,530,1722,1743,1892,1914,504,1120,2256,2280,399,1065,1037,897,2862",
			"name": "Smallest denominator of a proper fraction that has a nontrivial anomalous cancellation in base b.",
			"comment": [
				"See comments at A291093.",
				"For prime base p, (p + 1)/(p^2 + p) simplifies to 1/p by cancelling digit k = 1 in the numerator and denominator. This fraction is written \"11/110\" in base p and simplifies to \"1/10\" = 1/p.",
				"Smallest base b for which n/d, simplified, has a numerator greater than 1 is 51."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A292289/b292289.txt\"\u003eTable of n, a(n) for n = 2..120\u003c/a\u003e",
				"Michael De Vlieger, \u003ca href=\"/A292289/a292289.txt\"\u003eBase-b proper fractions n/d having nontrivial anomalous cancellation, with 2 \u003c= b \u003c= 120 and d \u003c= b^2 + b.\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/AnomalousCancellation.html\"\u003eAnomalous Cancellation\u003c/a\u003e"
			],
			"formula": [
				"a(p) = p^2 + p."
			],
			"example": [
				"a(5) = 30, the corresponding numerator is 6; these are written \"11/110\" in quinary, cancelling a 1 in both numerator and denominator yields \"1/10\" which is 1/5. 6/30 = 1/5.",
				"Table of smallest values correlated with least numerators:",
				"b = base and index.",
				"n = smallest numerator that pertains to d.",
				"d = smallest denominator that has a nontrivial anomalous cancellation in base b (this sequence).",
				"n/d = simplified ratio of numerator n and denominator d.",
				"k = base-b digit cancelled in the numerator and denominator to arrive at n/d.",
				"b-n+1 = difference between base and numerator plus one.",
				"b^2-d = difference between the square of the base and denominator.",
				".",
				"   b     n      d   n/d     k  b-n+1  b^2-d",
				"   -----------------------------------------",
				"   2     3      6   1/2     1    0     -2",
				"   3     4     12   1/3     1    0     -3",
				"   4     7     14   1/2     3    2      2",
				"   5     6     30   1/5     1    0     -5",
				"   6    11     33   1/3     5    4      3",
				"   7     8     56   1/7     1    0     -7",
				"   8    15     60   1/4     7    6      4",
				"   9    13     39   1/3     4    3     42",
				"  10    16     64   1/4     6    5     36",
				"  11    12    132   1/11    1    0    -11",
				"  12    23    138   1/6    11   10      6",
				"  13    14    182   1/13    1    0    -13",
				"  14    27    189   1/7    13   12      7",
				"  15    22    110   1/5     7    6    115",
				"  16    21     84   1/4     5    4    172"
			],
			"mathematica": [
				"Table[SelectFirst[Range[b, b^2 + b], Function[m, Map[{#, m} \u0026, #] \u0026@ Select[Range[b + 1, m - 1], Function[k, Function[{r, w, n, d}, AnyTrue[Flatten@ Map[Apply[Outer[Divide, #1, #2] \u0026, #] \u0026, Transpose@ MapAt[# /. 0 -\u003e Nothing \u0026, Map[Function[x, Map[Map[FromDigits[#, b] \u0026@ Delete[x, #] \u0026, Position[x, #]] \u0026, Intersection @@ {n, d}]], {n, d}], -1]], # == Divide @@ {k, m} \u0026]] @@ {k/m, #, First@ #, Last@ #} \u0026@ Map[IntegerDigits[#, b] \u0026, {k, m}] - Boole[Mod[{k, m}, b] == {0, 0}]] ] != {}]], {b, 2, 30}] (* _Michael De Vlieger_, Sep 13 2017 *)"
			],
			"xref": [
				"Cf. A291093/A291094, A292288 (numerators), A292393 (digit that is canceled)."
			],
			"keyword": "nonn,frac,base",
			"offset": "2,1",
			"author": "_Michael De Vlieger_, Sep 13 2017",
			"references": 3,
			"revision": 15,
			"time": "2017-09-18T17:27:07-04:00",
			"created": "2017-09-14T09:41:04-04:00"
		}
	]
}