{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A275435",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 275435,
			"data": "1,2,1,2,3,2,2,4,2,5,6,2,3,8,8,2,8,14,10,2,5,16,20,12,2,13,30,30,14,2,8,30,48,40,16,2,21,60,78,54,18,2,13,56,106,112,68,20,2,34,116,184,166,86,22,2,21,102,224,286,224,104,24,2,55,218,408,452,310,126,26,2",
			"name": "Triangle read by rows: T(n,k) is the number of 00-avoiding binary words of length n having degree of asymmetry equal to k (n\u003e=0; 0\u003c=k\u003c=floor(n/2)).",
			"comment": [
				"The degree of asymmetry of a finite sequence of numbers is defined to be the number of pairs of symmetrically positioned distinct entries. Example: the degree of asymmetry of (2,7,6,4,5,7,3) is 2, counting the pairs (2,3) and (6,5).",
				"A sequence is palindromic if and only if its degree of asymmetry is 0.",
				"Number of entries in row n is 1+floor(n/2).",
				"Sum of entries in row n is A000045(n+2) (Fibonacci).",
				"T(n,0) = A053602(n+2) (= number of palindromic 00-avoiding binary words of length n).",
				"Sum(k*T(n,k), k\u003e=0) = A275436(n)."
			],
			"formula": [
				"G.f.: G(t,z) = (1 + 2z + tz^2 +z^3 -tz^5)/(1 - z^2 - tz^2 - z^4 - tz^4 + tz^6)."
			],
			"example": [
				"Row 3 is [3,2] because the 00-avoiding binary words of length 3 are 010, 011, 101, 110, 111, having asymmetry degrees 0, 1, 0, 1, 0, respectively.",
				"Triangle starts:",
				"1;",
				"2;",
				"1,2;",
				"3,2;",
				"2,4,2;",
				"5,6,2."
			],
			"maple": [
				"G := (1+2*z+t*z^2+z^3-t*z^5)/(1-z^2-t*z^2-z^4-t*z^4+t*z^6): Gser := simplify(series(G, z = 0, 20)): for n from 0 to 18 do P[n] := sort(coeff(Gser, z, n)) end do: for n from 0 to 18 do seq(coeff(P[n], t, j), j = 0 .. (1/2)*n) end do; # yields sequence in triangular form"
			],
			"mathematica": [
				"Table[BinCounts[#, {0, Floor[n/2] + 1, 1}] \u0026@ Map[Total@ BitXor[Take[#, Ceiling[Length[#]/2]], Reverse@ Take[#, -Ceiling[Length[#]/2]]] \u0026, Select[PadLeft[IntegerDigits[#, 2], n] \u0026 /@ Range[0, 2^n - 1], Length@ SequenceCases[#, {0, 0}] == 0 \u0026]], {n, 0, 15}] // Flatten (* _Michael De Vlieger_, Aug 15 2016, Version 10.1 *)"
			],
			"xref": [
				"Cf. A000045, A053602, A275436"
			],
			"keyword": "nonn,tabf",
			"offset": "0,2",
			"author": "_Emeric Deutsch_, Aug 15 2016",
			"references": 1,
			"revision": 8,
			"time": "2016-08-16T04:20:46-04:00",
			"created": "2016-08-16T04:20:46-04:00"
		}
	]
}