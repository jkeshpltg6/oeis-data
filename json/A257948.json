{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A257948",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 257948,
			"data": "1,35,35,35,56,56,56,56,35,1,56,56,56,56,35,35,56,56,35,35,10,56,56,56,56,56,56,14,35,35,56,56,35,35,56,56,35,35,56,35,56,56,14,56,35,56,14,35,56,56,2,56,56,56,56,35,5,56,35,56,56,56,10,56,56,35,35,56,35,56,56,56,56,2,35,35,56,56,56,56,35,35,56,35,56,56,35,56,56,35,56,56,56,56,35,56,56,56,56,1",
			"name": "Length of cycle in which n ends under iteration of sum-of-squares-of-two-digits map s_2.",
			"comment": [
				"If n has an even number of digits, say n = abcdef, the map is n-\u003es_2(n):= (ab)^2+(cd)^2+(ef)^2. If n has an odd number of digits, say n = abcde, the map is n-\u003es_2(n):= (a)^2+(bc)^2+(de)^2.",
				"The following statements, densities and conjectures are based on calculations for n = 1...10000.",
				"The map s_2 has fixed points 1, 1233, 3388. These are cycles of length 1. For the two 4-digit numbers see A055616. There are more numbers that end under iteration of s_2 in 1233 or 3388. Like a(3312) = 1233 or a(3388) = 8833.",
				"The numbers that end under iteration of s_2 in 1 are called the bihappy numbers (A257795). They have a density of 0.33%.",
				"It is conjectured that iterations of s_2 always end in cycles of finite period length and besides the 1-cycles there are ten different cycles of length \u003e 1. The period lengths are 2, 2, 4, 5, 5, 6, 10, 14, 35 or 56.",
				"Two cycles with a period length of 2: 5965 =\u003e 7706 =\u003e 5965, first number that reaches this 2-cycle is 51, the second 2-cycle is: 3869 =\u003e 6205 =\u003e 3869, first number that reaches this 2-cycle is 562. Density of both 2-cycles together is 0.9%.",
				"Cycle with a period length of 4: 3460 =\u003e 4756 =\u003e 5345 =\u003e 4834 =\u003e 3460. First number to reach this 4-cycle is 342. Density is 0.69%.",
				"Two cycles with a period length of 5: (1781, 6850, 7124, 5617, 3425, 1781), first number to reach this 5-cycle is 57. And (3770, 6269, 8605, 7421, 5917), first number to reach this 5-cycle is 162. Density of both 5-cycles together is 1.78%.",
				"Cycle with a period length of 6: (4973, 7730, 6829, 5465, 7141, 6722, 4973). First number to reach this 6-cycle is 389. Density exactly 1%.",
				"Cycle with a period length of 10: (1268, 4768, 6833, 5713, 3418, 1480, 6596, 13441, 2838, 2228, 1268). First number to reach this 10-cycle is 21. Density 0.48%.",
				"Cycle with a period length of 14: (1946, 2477, 6505, 4250, 4264, 5860, 6964, 8857, 10993, 8731, 8530, 8125, 7186, 12437, 1946). First number to reach this 14-cycle is 28. Density 5.5%.",
				"Cycle with a period length of 35: (37, 1369, 4930, 3301, 1090, 8200, 6724, 5065, 6725, 5114, 2797, 10138, 1446, 2312, 673, 5365, 7034, 6056, 6736, 5785, 10474, 5493, 11565, 4451, 4537, 3394, 9925, 10426, 693, 8685, 14621, 2558, 3989, 9442, 10600, 37). First number to reach this 35-cycle is 2. Density is 27.89%.",
				"Cycle with a period length of 56: (41, 1681, 6817, 4913, 2570, 5525, 3650, 3796, 10585, 7251, 7785, 13154, 3878, 7528, 6409, 4177, 7610, 5876, 9140, 9881, 16165, 7947, 8450, 9556, 12161, 4163, 5650, 5636, 4432, 2960, 4441, 3617, 1585, 7450, 7976, 12017, 690, 8136, 7857, 9333, 9738, 10853, 2874, 6260, 7444, 7412, 5620, 3536, 2521, 1066, 4456, 5072, 7684, 12832, 1809, 405, 41). First number to reach this 56-cycle is 5. Density 61.38%.",
				"Density is calculated over s_2(1) till s_2(10000)."
			],
			"link": [
				"Pieter Post, \u003ca href=\"/A257948/b257948.txt\"\u003eTable of n, a(n) for n = 1..9999\u003c/a\u003e"
			],
			"example": [
				"s_2^[9](2)= 35, because 2^2=4=\u003e  4^2=16 =\u003e 16^2=256 =\u003e   2^2+56^2=3140 =\u003e   31^2+40^2=2561 =\u003e  25^2+61^2=4346 =\u003e 43^2+46^2=3965 =\u003e 39^2+65^2=5746 =\u003e 57^2+46^2=5365=\u003e 53^2+65^2= 7034. Nine iterations are needed to reach the 35-cycle.",
				"s_2^[3](51)=2, since 51^2 = 2601 =\u003e 26^2+1^2 = 677 =\u003e 6^+77^2 = 5965 =\u003e 59^2+ 65^2 = 7706 =\u003e 77^2+6^2 = 5965. Three iterations are needed to reach the 2-cycle."
			],
			"xref": [
				"Cf. A257795, A055616, A007770."
			],
			"keyword": "nonn,base",
			"offset": "1,2",
			"author": "_Pieter Post_, May 14 2015",
			"references": 1,
			"revision": 19,
			"time": "2015-07-19T11:05:38-04:00",
			"created": "2015-07-19T11:05:38-04:00"
		}
	]
}