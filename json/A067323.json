{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A067323",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 67323,
			"data": "1,2,1,5,3,2,14,9,7,5,42,28,23,19,14,132,90,76,66,56,42,429,297,255,227,202,174,132,1430,1001,869,785,715,645,561,429,4862,3432,3003,2739,2529,2333,2123,1859,1430,16796,11934,10504,9646,8986,8398,7810,7150,6292,4862",
			"name": "Catalan triangle A028364 with row reversion.",
			"comment": [
				"a(N,p) equals X_{N}(N+1,p) := T_{N,p} for alpha= 1 =beta and N\u003e=p\u003e=1 in the Derrida et al. 1992 reference. The one-point correlation functions \u003ctau_{K}\u003e_{N} for alpha= 1 =beta equal a(N,K)/C(N+1) with C(n)=A000108(n) (Catalan) in this reference. See also the Derrida et al. 1993 reference. In the Liggett 1999 reference mu_{N}{eta:eta(k)=1} of prop. 3.38, p. 275 is identical with \u003ctau_{k}\u003e_{N} and rho=0 and lambda=1.",
				"Identity for each row n\u003e=1: a(n,m)+a(n,n-m+1)= C(n+1), with C(n+1)=A000108(n+1)(Catalan) for every m=1..floor((n+1)/2). E.g., a(2k+1,k+1)=C(2*(k+1)).",
				"The first column sequences (diagonals of A028364) are: A000108(n+1), A000245, A067324-6 for m=0..4."
			],
			"reference": [
				"B. Derrida, E. Domany and D. Mukamel, An exact solution of a one-dimensional asymmetric exclusion model with open boundaries, J. Stat. Phys. 69, 1992, 667-687; eqs. (19) - (23), p. 672.",
				"B. Derrida, M. R. Evans, V. Hakim and V. Pasquier, Exact solution of a 1D asymmetric exclusion model using a matrix formulation, J. Phys. A 26, 1993, 1493-1517; eqs. (43), (44), pp. 1501-2 and eq.(81) with eqs.(80) and (81).",
				"T. M. Liggett, Stochastic Interacting Systems: Contact, Voter and Exclusion Processes, Springer, 1999, pp. 269, 275.",
				"G. Schuetz and E. Domany, Phase Transitions in an Exactly Soluble one-Dimensional Exclusion Process, J. Stat. Phys. 72 (1993) 277-295, eq. (2.18), p. 283, with eqs. (2.13)-(2.15)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A067323/b067323.txt\"\u003eRows n = 0..140, flattened\u003c/a\u003e",
				"W. Lang: \u003ca href=\"/A067323/a067323.txt\"\u003eFirst 10 rows.\u003c/a\u003e"
			],
			"formula": [
				"a(n,m) = A028364(n,n-m), n\u003e=m\u003e=0, else 0.",
				"G.f. for column m\u003e=1 (without leading zeros): (c(x)^3)sum(C(m-1, k)*c(x)^k, k=0..m-1), with C(n, m) := (m+1)*binomial(2*n-m, n-m)/(n+1) (Catalan convolutions A033184); and for m=0: c^2(x), where c(x) is g.f. of A000108 (Catalan).",
				"T(n,k) = Sum_{j\u003e=0} A039598(n-k,j)*A039599(k,j). - _Philippe Deléham_, Feb 18 2004",
				"G.f. for diagonal sequences: see g.f. for columns of A028364."
			],
			"example": [
				"Triangle begins:",
				"     1;",
				"     2,    1;",
				"     5,    3,    2;",
				"    14,    9,    7,    5;",
				"    42,   28,   23,   19,   14;",
				"   132,   90,   76,   66,   56,   42;",
				"   429,  297,  255,  227,  202,  174,  132;",
				"  1430, 1001,  869,  785,  715,  645,  561,  429;",
				"  4862, 3432, 3003, 2739, 2529, 2333, 2123, 1859, 1430;",
				"  ..."
			],
			"maple": [
				"b:= proc(n, i) option remember; `if`(n=0, 1, add(",
				"      expand(b(n-1, j)*`if`(i\u003en, x, 1)), j=1..i))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, n-i), i=0..n))(b((n+1)$2)):",
				"seq(T(n), n=0..10);  # _Alois P. Heinz_, Nov 28 2015"
			],
			"mathematica": [
				"t[n_, k_] := Sum[ CatalanNumber[n - j]*CatalanNumber[j], {j, 0, k}]; Flatten[ Table[t[n, k], {n, 0, 9}, {k, n, 0, -1}]] (* _Jean-François Alcover_, Jul 17 2013 *)"
			],
			"xref": [
				"Cf. A001700 (row sums).",
				"Cf. A039598, A039599.",
				"T(2n,n) gives A201205."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Feb 05 2002",
			"references": 12,
			"revision": 27,
			"time": "2019-08-28T16:22:16-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}