{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006509",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6509,
			"id": "M2539",
			"data": "1,3,6,11,4,15,2,19,38,61,32,63,26,67,24,71,18,77,16,83,12,85,164,81,170,73,174,277,384,275,162,35,166,29,168,317,468,311,148,315,142,321,140,331,138,335,136,347,124,351,122,355,116,357,106,363,100,369,98,375,94,377,84,391,80,393,76,407,70,417,68,421,62,429,56,435,52,441,44,445,36,455,34,465,898,459,902,453,910,449,912,1379,900,413,904,405,908,399,920,397,938,1485,928,365,934,1505,2082,1495,2088,1489,888,281,894,1511,892,261,0,643,1290,637,1296,635,1308,631,1314,623,1324,615,1334,607,1340",
			"name": "Cald's sequence: a(n+1) = a(n) - prime(n) if that value is positive and new, otherwise a(n) + prime(n) if new, otherwise 0.",
			"reference": [
				"F. Cald, Problem 356, Franciscan order, J. Rec. Math., 7 (No. 4, 1974), 318; 10 (No. 1, 1974), 62-64.",
				"\"Cald's Sequence\", Popular Computing (Calabasas, CA), Vol. 4 (No. 41, Aug 1976), pp. 16-17.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A006509/b006509.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"R. G. Wilson, \u003ca href=\"/A006509/a006509.pdf\"\u003eLetter to N. J. A. Sloane with attachment, Jan 1989\u003c/a\u003e"
			],
			"maple": [
				"M1:=500000; a:=array(0..M1); have:=array(0..M1); a[0]:=1;",
				"for n from 0 to M1 do have[n]:=0; od: have[0]:=1; have[1]:=1;",
				"M2:=2000; nmax:=M2; for n from 1 to M2 do p:=ithprime(n); i:=a[n-1]-p; j:=a[n-1]+p;",
				"if i \u003e= 1 and have[i]=0 then a[n]:=i; have[i]:=1;",
				"elif j \u003c= M1 and have[j]=0 then a[n]:=j; have[j]:=1;",
				"elif j \u003c= M1 then a[n]:=0; else nmax:=n-1; break; fi; od:",
				"# To get A006509:",
				"[seq(a[n],n=0..M2)];",
				"# To get A112877 (off by 1 because of different offset in A006509):",
				"zzz:=[]; for n from 0 to nmax do if a[n]=0 then zzz:=[op(zzz),n]; fi; od: [seq(zzz[i],i=1..nops(zzz))];"
			],
			"mathematica": [
				"lst = {1}; f := Block[{b = Last@lst, p = Prime@ Length@lst}, If[b \u003e p \u0026\u0026 !MemberQ[lst, b - p], AppendTo[lst, b - p], If[ !MemberQ[lst, b + p], AppendTo[lst, b + p], AppendTo[lst, 0]] ]]; Do[f, {n, 60}]; lst (* _Robert G. Wilson v_, Apr 25 2006 *)"
			],
			"program": [
				"(Haskell)",
				"a006509 n = a006509_list !! (n-1)",
				"a006509_list = 1 : f [1] a000040_list where",
				"   f xs'@(x:_) (p:ps) | x' \u003e 0 \u0026\u0026 x' `notElem` xs = x' : f (x':xs) ps",
				"                      | x'' `notElem` xs          = x'' : f (x'':xs) ps",
				"                      | otherwise                 = 0 : f (0:xs) ps",
				"                      where x' = x - p; x'' = x + p",
				"-- _Reinhard Zumkeller_, Oct 17 2011",
				"(Python)",
				"from sympy import primerange, prime",
				"def aupton(terms):",
				"  alst = [1]",
				"  for n, pn in enumerate(primerange(1, prime(terms)+1), start=1):",
				"    x, y = alst[-1] - pn, alst[-1] + pn",
				"    if x \u003e 0 and x not in alst: alst.append(x)",
				"    elif y \u003e 0 and y not in alst: alst.append(y)",
				"    else: alst.append(0)",
				"  return alst",
				"print(aupton(130)) # _Michael S. Branicky_, May 30 2021"
			],
			"xref": [
				"Cf. A005132, A093903. Zeros are in A112877. Sorted: A111338, A111339 (numbers missing from A006509).",
				"Cf. A117128, A117129, A064365, A005132, A000040."
			],
			"keyword": "nonn,nice,easy",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from Larry Reeves (larryr(AT)acm.org), Jul 20 2001",
				"Many more terms added by _N. J. A. Sloane_, Apr 20 2006, to show difference from A117128."
			],
			"references": 16,
			"revision": 39,
			"time": "2021-05-31T03:26:33-04:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}