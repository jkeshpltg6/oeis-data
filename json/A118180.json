{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A118180",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 118180,
			"data": "1,1,1,1,3,1,1,9,9,1,1,27,81,27,1,1,81,729,729,81,1,1,243,6561,19683,6561,243,1,1,729,59049,531441,531441,59049,729,1,1,2187,531441,14348907,43046721,14348907,531441,2187,1,1,6561,4782969,387420489,3486784401,3486784401,387420489,4782969,6561,1",
			"name": "Triangle T(n, k) = 3^(k*(n-k)), read by rows.",
			"comment": [
				"For any column vector C, the matrix product of T*C transforms the g.f. of C: Sum_{n\u003e=0} c(n)*x^n into the g.f.: Sum_{n\u003e=0} c(n)*x^n/(1-3^n*x)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A118180/b118180.txt\"\u003eRows n = 0..50 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"G.f.: A(x,y) = Sum_{n\u003e=0} x^n/(1-3^n*x*y). G.f. satisfies: A(x,y) = 1/(1-x*y) + x*A(x,3*y).",
				"Equals ConvOffsStoT transform of the 3^n series: (1, 3, 9, 27, ...); e.g., ConvOffs transform of (1, 3, 9, 27) = (1, 27, 81, 27, 1). - _Gary W. Adamson_, Apr 21 2008",
				"T(n,k) = (1/n)*( 3^(n-k)*k*T(n-1,k-1) + 3^k*(n-k)*T(n-1,k) ), where T(i,j)=0 if j\u003ei. - _Tom Edgar_, Feb 20 2014",
				"T(n, k, m) = (m+2)^(k*(n-k)) with m = 1. - _G. C. Greubel_, Jun 28 2021"
			],
			"example": [
				"A(x,y) = 1/(1-xy) + x/(1-3xy) + x^2/(1-9xy) + x^3/(1-27xy) + ...",
				"Triangle begins:",
				"  1;",
				"  1,    1;",
				"  1,    3,      1;",
				"  1,    9,      9,        1;",
				"  1,   27,     81,       27,        1;",
				"  1,   81,    729,      729,       81,        1;",
				"  1,  243,   6561,    19683,     6561,      243,      1;",
				"  1,  729,  59049,   531441,   531441,    59049,    729,    1;",
				"  1, 2187, 531441, 14348907, 43046721, 14348907, 531441, 2187, 1; ...",
				"The matrix inverse T^-1 starts:",
				"      1;",
				"     -1,     1;",
				"      2,    -3,     1;",
				"    -10,    18,    -9,    1;",
				"    134,  -270,   162,  -27,   1;",
				"  -4942, 10854, -7290, 1458, -81, 1; ...",
				"where [T^-1](n,k) = A118183(n-k)*(3^k)^(n-k)."
			],
			"maple": [
				"seq(seq( (3^k)^(n-k), k=0..n), n=0..12);"
			],
			"mathematica": [
				"T[n_, k_, m_]:= (m+2)^(k*(n-k)); Table[T[n,k,1], {n,0,12}, {k,0,n}]//Flatten (* _G. C. Greubel_, Jun 28 2021 *)"
			],
			"program": [
				"(PARI) T(n,k) = if(k\u003c0 || k\u003en, 0, 3^(k*(n-k)));",
				"(MAGMA)",
				"A118180:= func\u003c n, k, m | (m+2)^(k*(n-k)) \u003e;",
				"[A118180(n, k, 1): k in [0..n], n in [0..12]]; // _G. C. Greubel_, Jun 28 2021",
				"(Sage)",
				"def A118180(n, k, m): return (m+2)^(k*(n-k))",
				"flatten([[A118180(n, k, 1) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, Jun 28 2021"
			],
			"xref": [
				"Cf. A118181 (row sums), A118182 (antidiagonal sums), A118183, A118184.",
				"Cf. A117401 = ConvOffsStoT transform of 2^n.",
				"Cf. A117401 (m=0), this sequence (m=1), A118185 (m=2), A118190 (m=3), A158116 (m=4), A176642 (m=6), A158117 (m=8), A176627 (m=10), A176639 (m=13), A156581 (m=15)."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Paul D. Hanna_, Apr 15 2006",
			"references": 19,
			"revision": 22,
			"time": "2021-06-29T02:34:02-04:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}