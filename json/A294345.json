{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A294345",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 294345,
			"data": "0,0,0,0,6,0,10,15,14,21,0,35,22,33,26,94,0,142,34,142,38,142,0,357,46,202,0,302,0,591,58,334,62,491,0,980,0,217,74,821,0,1340,82,785,86,827,0,1987,94,1512,0,1353,0,2677,106,1421,0,1479,0,4242,118",
			"name": "Sum of the products of the smaller and larger parts of the Goldbach partitions of n into two distinct parts.",
			"comment": [
				"Sum of the areas of the distinct rectangles with prime length and width such that L + W = n, W \u003c L. For example, a(14) = 33; the only rectangle is 3 X 11 and 3*11 = 33 (the 7 X 7 rectangle is not considered since we have W \u003c L)."
			],
			"link": [
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/GoldbachPartition.html\"\u003eGoldbach Partition\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Goldbach%27s_conjecture\"\u003eGoldbach's conjecture\u003c/a\u003e",
				"\u003ca href=\"/index/Go#Goldbach\"\u003eIndex entries for sequences related to Goldbach conjecture\u003c/a\u003e",
				"\u003ca href=\"/index/Par#part\"\u003eIndex entries for sequences related to partitions\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Sum_{i=2..floor((n-1)/2)} i * (n-i) * c(i) * c(n-i), where c is the prime characteristic (A010051).",
				"a(k) = 0, for k in A166081. - _Michel Marcus_, Oct 30 2017"
			],
			"example": [
				"a(22) = 142; the Goldbach partitions of 22 are (19,3), (17,5) and (11,11) (we do not consider (11,11) since we only count prime parts which are distinct). Then the sum of the products of the smaller and larger parts from each partition is 19*3 + 17*5 = 142."
			],
			"maple": [
				"with(numtheory): A294345:=n-\u003eadd(i*(n-i)*(pi(i)-pi(i-1))*(pi(n-i)-pi(n-i-1)), i=2..floor((n-1)/2)): seq(A294345(n), n=1..100);"
			],
			"mathematica": [
				"Table[Sum[i (n - i) (PrimePi[i] - PrimePi[i - 1]) (PrimePi[n - i] - PrimePi[n - i - 1]), {i, 2, Floor[(n-1)/2]}], {n, 60}]",
				"Table[Total[Times@@@Select[IntegerPartitions[n,{2}],AllTrue[#,PrimeQ] \u0026\u0026 #[[1]]!=#[[2]]\u0026]],{n,70}] (* _Harvey P. Dale_, Jul 29 2021 *)"
			],
			"program": [
				"(PARI) a(n) = sum(i=1, (n-1)\\2, i*isprime(i)*(n-i)*isprime(n-i)); \\\\ _Michel Marcus_, Nov 08 2017"
			],
			"xref": [
				"Cf. A010051, A166081, A243485."
			],
			"keyword": "nonn,easy",
			"offset": "1,5",
			"author": "_Wesley Ivan Hurt_, Oct 28 2017",
			"references": 1,
			"revision": 34,
			"time": "2021-07-29T17:05:40-04:00",
			"created": "2017-12-19T02:31:06-05:00"
		}
	]
}