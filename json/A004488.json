{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A004488",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 4488,
			"data": "0,2,1,6,8,7,3,5,4,18,20,19,24,26,25,21,23,22,9,11,10,15,17,16,12,14,13,54,56,55,60,62,61,57,59,58,72,74,73,78,80,79,75,77,76,63,65,64,69,71,70,66,68,67,27,29,28,33,35,34,30,32,31,45,47,46,51",
			"name": "Tersum n + n.",
			"comment": [
				"Could also be described as \"Write n in base 3, then replace each digit with its base-3 negative\" as with A048647 for base 4. - _Henry Bottomley_, Apr 19 2000",
				"a(a(n)) = n, a self-inverse permutation of the nonnegative integers. - _Reinhard Zumkeller_, Dec 19 2003",
				"First 3^n terms of the sequence form a permutation s(n) of 0..3^n-1, n\u003e=1; the number of inversions of s(n) is A016142(n-1). - _Gheorghe Coserea_, Apr 23 2018"
			],
			"link": [
				"Alois P. Heinz and Gheorghe Coserea, \u003ca href=\"/A004488/b004488.txt\"\u003eTable of n, a(n) for n = 0..59048\u003c/a\u003e (first 6561 terms from Alois P. Heinz)",
				"\u003ca href=\"/index/Ca#CARRYLESS\"\u003eIndex entries for sequences related to carryless arithmetic\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"Tersum m + n: write m and n in base 3 and add mod 3 with no carries, e.g., 5 + 8 = \"21\" + \"22\" = \"10\" = 1.",
				"a(n) = Sum(3-d(i)-3*0^d(i): n=Sum(d(i)*3^d(i): 0\u003c=d(i)\u003c3)). - _Reinhard Zumkeller_, Dec 19 2003",
				"a(3*n) = 3*a(n), a(3*n+1) = 3*a(n)+2, a(3*n+2) = 3*a(n)+1. - _Robert Israel_, May 09 2014"
			],
			"maple": [
				"a:= proc(n) local t, r, i;",
				"      t, r:= n, 0;",
				"      for i from 0 while t\u003e0 do",
				"        r:= r+3^i *irem(2*irem(t, 3, 't'), 3)",
				"      od; r",
				"    end:",
				"seq(a(n), n=0..80);  # _Alois P. Heinz_, Sep 07 2011"
			],
			"mathematica": [
				"a[n_] := FromDigits[Mod[3-IntegerDigits[n, 3], 3], 3]; Table[a[n], {n, 0, 66}] (* _Jean-François Alcover_, Mar 03 2014 *)"
			],
			"program": [
				"(Haskell)",
				"a004488 0 = 0",
				"a004488 n = if d == 0 then 3 * a004488 n' else 3 * a004488 n' + 3 - d",
				"            where (n', d) = divMod n 3",
				"-- _Reinhard Zumkeller_, Mar 12 2014",
				"(PARI) a(n) = my(b=3); fromdigits(apply(d-\u003e(b-d)%b, digits(n, b)), b);",
				"vector(67, i, a(i-1))  \\\\ _Gheorghe Coserea_, Apr 23 2018",
				"(Python)",
				"from sympy.ntheory.factor_ import digits",
				"def a(n): return int(\"\".join([str((3 - i)%3) for i in digits(n, 3)[1:]]), 3) # _Indranil Ghosh_, Jun 06 2017"
			],
			"xref": [
				"Column k=0 of A253586, A253587.",
				"Column k=3 of A248813.",
				"Row / column 2 of A325820.",
				"Main diagonal of A004489.",
				"Cf. A048647, A055115, A055116, A055120, A059249, A117966, A117967, A117968, A225901, A242399, A244042, A263273, A289813, A289814, A289815, A289816, A289831, A289838, A300222, A321464."
			],
			"keyword": "nonn,base,look",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 48,
			"revision": 77,
			"time": "2021-04-06T04:47:58-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}