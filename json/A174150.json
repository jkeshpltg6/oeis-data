{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A174150",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 174150,
			"data": "1,1,1,1,6,1,1,12,12,1,1,30,60,30,1,1,60,300,300,60,1,1,105,1050,2625,1050,105,1,1,168,2940,14700,14700,2940,168,1,1,252,7056,61740,123480,61740,7056,252,1,1,360,15120,211680,740880,740880,211680,15120,360,1",
			"name": "Triangle T(n, k) = round(c(n)/(c(k)*c(n-k))) where c(n) = ((n-1)! * n! * (n+1)!)/ 2^(n-1) if n \u003e= 2, otherwise 1, read by rows.",
			"comment": [
				"Based on the SL(2,p) number of prime modular form group transforms: f(p) = p*(p^2-1) if p = 2, otherwise f(p) = p*(p^2-1)/2."
			],
			"reference": [
				"T. S. Blyth and E. F. Robertson, Essential Student Algebra: Groups, Volume 5, J. W. Arrowsmith, Bristol, 1986, page 14.",
				"Leonard Eugene Dickson, On Invariants and the Theory of Numbers, Dover, New York, 1966, page 34."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A174150/b174150.txt\"\u003eRows n = 0..50 of the triangle, flattened\u003c/a\u003e",
				"Leonard Eugene Dickson, \u003ca href=\"https://archive.org/details/madisoncolloquiu00amer/page/34\"\u003eOn Invariants and the Theory of Numbers\u003c/a\u003e, American Mathematical Society, Colloquium Lectures, 1913, p. 34."
			],
			"formula": [
				"T(n, k) = round(c(n)/(c(k)*c(n-k))) for n \u003e= 0 and k \u003e= 0, where c(n) = 2^(2-n)* Product_{j=2..n} j*(j^2 - 1) for n \u003e= 2 and otherwise 1.",
				"T(n, k) = (k/(2*(k+1))*Product_{j=0..2} binomial(n+j-1,k) with T(n,0) = T(n,n) = 1, T(n,1) = T(n,n-1) = 3*binomial(n+1,3) + 3*[n=2]. - _G. C. Greubel_, Apr 15 2021"
			],
			"example": [
				"Triangle T(n,m) (with rows n \u003e= 0 and columns m \u003e= 0) begins as follows:",
				"  1;",
				"  1,   1;",
				"  1,   6,     1;",
				"  1,  12,    12,      1;",
				"  1,  30,    60,     30,       1;",
				"  1,  60,   300,    300,      60,       1;",
				"  1, 105,  1050,   2625,    1050,     105,       1;",
				"  1, 168,  2940,  14700,   14700,    2940,     168,      1;",
				"  1, 252,  7056,  61740,  123480,   61740,    7056,    252,     1;",
				"  1, 360, 15120, 211680,  740880,  740880,  211680,  15120,   360,   1;",
				"  1, 495, 29700, 623700, 3492720, 6112260, 3492720, 623700, 29700, 495, 1;",
				"  ...",
				"Row sums are: 1, 2, 8, 26, 122, 722, 4937, 35618, 261578, 1936082, 14405492, ..."
			],
			"maple": [
				"g := n -\u003e `if`(n \u003c 2, 1, GAMMA(n)*GAMMA(n+1)*GAMMA(n+2)/2^(n-1)):",
				"T := (n, k) -\u003e round(g(n)/(g(k)*g(n-k))):",
				"seq(seq(T(n, k), k=0..n), n=0..12); # _Peter Luschny_, Sep 02 2019"
			],
			"mathematica": [
				"(* First program *)",
				"c[n_]:= If[n\u003c2, 1, 2^(2-n)*Product[i*(i^2 -1), {i,2,n}]];",
				"T[n_, k_]:= Round[c[n]/(c[k]*c[n-k])];",
				"Table[T[n, k], {n,0,10}, {k,0,n}]//Flatten",
				"(* Second program *)",
				"T[n_, k_]:= If[k==0 || k==n, 1, If[k==1 || k==n-1, 3*Binomial[n+1, 3] + 3*Boole[n==2], (k/(2*(k+1)))* Product[Binomial[n+j-1, k], {j,0,2}] ]]//Round;",
				"Table[T[n, k], {n, 0, 12}, {k, 0, n}]//Flatten (* _G. C. Greubel_, Apr 15 2021 *)"
			],
			"program": [
				"(Magma)",
				"function t(n,k)",
				"  if k eq 0 or k eq n then return 1;",
				"  elif k eq 1 and n eq 2 then return 6;",
				"  elif k eq 1 or k eq n-1 then return 3*Binomial(n+1,3);",
				"  else return (k/(2*(k+1)))*(\u0026*[Binomial(n+j-1,k): j in [0..2]]);",
				"  end if; return t;",
				"end function;",
				"[Round(t(n,k)): k in [0..n], n in [0..12]]; // _G. C. Greubel_, Apr 15 2021",
				"(Sage)",
				"@CachedFunction",
				"def t(n, k):",
				"    if (k==0 or k==n): return 1",
				"    elif (k==1 and n==2): return 6",
				"    elif (k==1 or k==n-1): return 3*binomial(n+1,3)",
				"    else: return (k/(2*(k+1)))*product(binomial(n+j-1,k) for j in (0..2))",
				"flatten([[t(n, k) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, Apr 15 2021"
			],
			"xref": [
				"Cf. A174151."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Roger L. Bagula_, Mar 10 2010",
			"ext": [
				"Edited and renamed by _Peter Luschny_, Sep 02 2019"
			],
			"references": 2,
			"revision": 42,
			"time": "2021-04-15T23:48:08-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}