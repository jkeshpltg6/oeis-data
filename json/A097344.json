{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A097344",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 97344,
			"data": "1,5,29,103,887,1517,18239,63253,332839,118127,2331085,4222975,100309579,184649263,1710440723,6372905521,202804884977,381240382217,13667257415003,25872280345103,49119954154463,93501887462903,4103348710010689,7846225754967739,75162749477272151",
			"name": "Numerators in binomial transform of 1/(n+1)^2.",
			"comment": [
				"Is this identical to A097345? - Aaron Gulliver, Jul 19 2007. The answer turns out to be No - see A134652.",
				"If the putative formula a(n)=A081528(n) sum{k=0..n, binomial(n, k)/(k+1)^2} were true, then this sequence coincides with A097345 according to Mathar's notes. However, the term n=9 in the binomial transform of 1/(n+1)^2 has the denominator 5040=A081528(9)/4=A081528(10)/5. So the formula cannot be true. - _M. F. Hasler_, Jan 25 2008",
				"a(n) is also the numerator of u(n+1) with u(n) = (1/n)*Sum_{k=1..n} (2^k-1)/k and we have the formula: polylog(2,x/(1-x)) = Sum_{n\u003e=1} u(n)*x^n on the interval [-1/2, 1/2]. - _Groux Roland_, Feb 01 2009"
			],
			"link": [
				"Chai Wah Wu, \u003ca href=\"/A097344/b097344.txt\"\u003eTable of n, a(n) for n = 0..500\u003c/a\u003e",
				"R. J. Mathar, \u003ca href=\"/A097345/a097345.pdf\"\u003eNotes on an attempt to prove that A097344 and A097345 are identical\u003c/a\u003e"
			],
			"formula": [
				"a(n) = numerator(b(n)), b(n) = 1/((n+1)^2)*((n)*(3*n+1)*b(n-1)-2*(n-1)*(n)*b(n-2)+1). - _Vladimir Kruchinin_, May 31 2016"
			],
			"example": [
				"The first values of the binomial transform of 1/(n+1)^2 are 1, 5/4, 29/18, 103/48, 887/300, 1517/360, 18239/2940, 63253/6720, 332839/22680, 118127/5040, 2331085/60984, ..."
			],
			"maple": [
				"f:=n-\u003enumer(add( binomial(n,k)/(k+1)^2, k=0..n));"
			],
			"mathematica": [
				"Table[HypergeometricPFQ[{1, 1, -n}, {2, 2}, -1] // Numerator, {n, 0, 24}] (* _Jean-François Alcover_, Oct 14 2013 *)"
			],
			"program": [
				"(PARI) A097344(n)=numerator(sum(k=0,n,binomial(n,k)/(k+1)^2)) \\\\ _M. F. Hasler_, Jan 25 2008",
				"(Python)",
				"from fractions import Fraction",
				"A097344_list, tlist = [1], [Fraction(1,1)]",
				"for i in range(1,100):",
				"    for j in range(len(tlist)):",
				"        tlist[j] *= Fraction(i,i-j)",
				"    tlist += [Fraction(1,(i+1)**2)]",
				"    A097344_list.append(sum(tlist).numerator) # _Chai Wah Wu_, Jun 04 2015",
				"(Maxima)",
				"a(n):=if n\u003c0 then 1 else 1/((n+1)^2)*((n)*(3*n+1)*a(n-1)-2*(n-1)*(n)*a(n-2)+1);",
				"makelist(num(a(n),n,0,10); /* _Vladimir Kruchinin_, Jun 01 2016 */",
				"(Sage)",
				"def A097344_list(size):",
				"    R, L = [1], [1]",
				"    inc = sqr = 1",
				"    for i in range(1, size):",
				"        for j in range(i):",
				"            L[j] *= i / (i - j)",
				"        inc += 2; sqr += inc",
				"        L.extend(1 / sqr)",
				"        R.append(sum(L).numerator())",
				"    return R",
				"A097344_list(50) # (after _Chai Wah Wu_) _Peter Luschny_, Jun 05 2016"
			],
			"xref": [
				"Cf. A097345, A134652."
			],
			"keyword": "easy,nonn,frac",
			"offset": "0,2",
			"author": "_Paul Barry_, Aug 06 2004",
			"ext": [
				"Edited and corrected by Daniel Glasscock (glasscock(AT)rice.edu), Jan 04 2008 and _M. F. Hasler_, Jan 25 2008",
				"Moved comment on numerators of a logarithmic g.f. over to A097345 - _R. J. Mathar_, Mar 04 2010"
			],
			"references": 4,
			"revision": 35,
			"time": "2020-04-11T06:04:41-04:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}