{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005914",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5914,
			"id": "M4931",
			"data": "1,14,50,110,194,302,434,590,770,974,1202,1454,1730,2030,2354,2702,3074,3470,3890,4334,4802,5294,5810,6350,6914,7502,8114,8750,9410,10094,10802,11534,12290,13070,13874,14702,15554,16430,17330,18254,19202,20174,21170",
			"name": "Number of points on surface of hexagonal prism: 12*n^2 + 2 for n \u003e 0 (coordination sequence for W(2)).",
			"comment": [
				"For n \u003e= 1, a(n) is equal to the number of functions f:{1,2,3,4}-\u003e{1,2,...,n,n+1} such that Im(f) contains 2 fixed elements. - Aleksandar M. Janjic and _Milan Janjic_, Feb 24 2007",
				"Equals binomial transform of [1, 13, 23, 1, -1, 1, -1, 1, ...]. - _Gary W. Adamson_, Apr 22 2008",
				"First bisection of A005918. After 1, all terms are in A000408 (see Formula section). - _Bruno Berselli_, Feb 07 2012",
				"Also sequence found by reading the segment (1, 14) together with the line from 14, in the direction 14, 50, ..., in the square spiral whose vertices are the generalized octagonal numbers A001082. - _Omar E. Pol_, Nov 02 2012",
				"Unique sequence such that for all n \u003e 0, n*a(1) + (n-1)*a(2) + (n-3)*a(3) + ... + 2*a(2) + a(1) = n^4. - _Warren Breslow_, Dec 12 2014"
			],
			"reference": [
				"Gmelin Handbook of Inorg. and Organomet. Chem., 8th Ed., 1994, TYPIX search code (229) cI2",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A005914/b005914.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"O. Bagdasar, \u003ca href=\"http://www.np.ac.rs/downloads/publications/VOL6_Br_2/vol6br2-3.pdf\"\u003eOn Some Functions Involving the lcm and gcd of Integer Tuples\u003c/a\u003e, Scientific Publications of the State University of Novi Pazar, Appl. Maths. Inform. and Mech., Vol. 6, 2 (2014), 91--100.",
				"R. W. Grosse-Kunstleve, \u003ca href=\"/A005897/a005897.html\"\u003eCoordination Sequences and Encyclopedia of Integer Sequences\u003c/a\u003e",
				"R. W. Grosse-Kunstleve, G. O. Brunner and N. J. A. Sloane, \u003ca href=\"http://neilsloane.com/doc/ac96cs/\"\u003eAlgebraic Description of Coordination Sequences and Exact Topological Densities for Zeolites\u003c/a\u003e, Acta Cryst., A52 (1996), pp. 879-889.",
				"Milan Janjic, \u003ca href=\"http://www.pmfbl.org/janjic/\"\u003eEnumerative Formulas for Some Functions on Finite Sets\u003c/a\u003e",
				"C. J. Pita Ruiz V., \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL16/Pita/pita19.html\"\u003eSome Number Arrays Related to Pascal and Lucas Triangles\u003c/a\u003e, J. Int. Seq. 16 (2013) #13.5.7",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"B. K. Teo and N. J. A. Sloane, \u003ca href=\"http://dx.doi.org/10.1021/ic00220a025\"\u003eMagic numbers in polygonal and polyhedral clusters\u003c/a\u003e, Inorgan. Chem. 24 (1985), 4545-4558.",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-3,1)."
			],
			"formula": [
				"G.f.: (1+x)*(1+10*x+x^2)/(1-x)^3. - _Simon Plouffe_ (see MAPLE line)",
				"a(n) = (2n-1)^2 + (2n)^2 + (2n+1)^2 for n \u003e 0. - _Bruno Berselli_, Jan 30 2012",
				"a(n) = 3*a(n-1) - 3*a(n-2) + a(n-3); a(0)=1, a(1)=14, a(2)=50, a(3)=110. - _Harvey P. Dale_, Oct 09 2012",
				"E.g.f.: exp(x)*(12*x^2 + 12*x + 2) - 1. - _Alois P. Heinz_, Sep 10 2013",
				"From _Bruce J. Nicholson_, Jan 19 2019: (Start)",
				"Sum_{i=1..n} a(i) = A005917(n+1).",
				"a(n) = A003154(n) + A003154(n+1). (End)"
			],
			"maple": [
				"A005914:=-(z+1)*(z**2+10*z+1)/(z-1)**3; # _Simon Plouffe_ in his 1992 dissertation."
			],
			"mathematica": [
				"Table[If[n == 0, 1, 12*n^2 + 2], {n, 0, 100}] (* _Vladimir Joseph Stephan Orlovsky_, Jun 19 2011 *)",
				"Join[{1},LinearRecurrence[{3,-3,1},{14,50,110},50]] (* _Harvey P. Dale_, Oct 09 2012 *)"
			],
			"program": [
				"(PARI) a(n)=12*n^2+2 \\\\ _Charles R Greathouse IV_, Jan 31 2012"
			],
			"xref": [
				"First differences of A005917.",
				"Cf. A206399.",
				"Cf. A003154."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_ and _Ralf W. Grosse-Kunstleve_",
			"references": 11,
			"revision": 85,
			"time": "2021-05-04T01:02:43-04:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}