{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A052908",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 52908,
			"data": "1,1,2,4,9,19,40,85,180,381,807,1709,3619,7664,16230,34370,72785,154136,326412,691239,1463829,3099934,6564695,13901980,29440065,62344891,132027067,279592219,592089264,1253860704,2655286560",
			"name": "Expansion of 1 + x/(1 - 2*x - x^3 + x^4).",
			"comment": [
				"Let A(r,n) count the total number of ordered arrangements of an n+r tiling of r red squares and white tiles of total length n, where the individual tile lengths can range from 1 to n. A(r,0) corresponds to a tiling of r red squares only. Let A_1(r,n) = Sum_{j=0..n} A(r,j). Then the expansion of 1/(1-2*x-x^3+x^4) = A_1(0,n) + A_1(1,n-3) + A_1(2, n-6) + ..., which generates a(n) without the initial 1. - _Gregory L. Simay_, May 24 2018"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A052908/b052908.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Jarib R. Acosta, Yadira Caicedo, Juan P. Poveda, José L. Ramírez, Mark Shattuck, \u003ca href=\"https://www.emis.de/journals/JIS/VOL22/Shattuck/shattuck13.html\"\u003eSome New Restricted n-Color Composition Functions\u003c/a\u003e, J. Int. Seq., Vol. 22 (2019), Article 19.6.4.",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=888\"\u003eEncyclopedia of Combinatorial Structures 888\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,0,1,-1)."
			],
			"formula": [
				"G.f.: (1-x)*(1-x^3)/(1-2*x-x^3+x^4).",
				"a(n) = 2*a(n-1) + a(n-3) - a(n-4), with a(0)=1, a(1)=1, a(2)=2, a(3)=4, a(4)=9.",
				"a(n) = Sum_{alpha=RootOf(1 -2*z -z^3 +z^4)} (1/643)*(168 - 74*alpha + 53*alpha^2 - 93*alpha^3)*alpha^(-1-n)."
			],
			"maple": [
				"spec := [S,{S=Sequence(Prod(Sequence(Z),Sequence(Prod(Z,Z,Z)),Z))},unlabeled]: seq(combstruct[count](spec,size=n), n=0..20);"
			],
			"mathematica": [
				"CoefficientList[Series[(1-x)(1-x^3)/(1 -2x -x^3 +x^4), {x, 0, 30}], x] (* _Michael De Vlieger_, Jun 14 2018 *)",
				"LinearRecurrence[{2,0,1,-1}, {1,1,2,4,9}, 30}] (* _G. C. Greubel_, Oct 14 2019 *)"
			],
			"program": [
				"(PARI) my(x='x+O('x^30)); Vec((1-x)*(1-x^3)/(1-2*x-x^3+x^4)) \\\\ _G. C. Greubel_, Oct 14 2019",
				"(MAGMA) R\u003cx\u003e:=PowerSeriesRing(Integers(), 30); Coefficients(R!( (1-x)*(1-x^3)/(1-2*x-x^3+x^4) )); // _G. C. Greubel_, Oct 14 2019",
				"(Sage)",
				"def A052908_list(prec):",
				"    P.\u003cx\u003e = PowerSeriesRing(ZZ, prec)",
				"    return P( (1-x)*(1-x^3)/(1-2*x-x^3+x^4) ).list()",
				"A052908_list(30) # _G. C. Greubel_, Oct 14 2019",
				"(GAP) a:=[1,2,4,9];; for n in [5..30] do a[n]:=2*a[n-1]+a[n-3]-a[n-4]; od; Concatenation([1], a); # _G. C. Greubel_, Oct 14 2019"
			],
			"keyword": "easy,nonn",
			"offset": "0,3",
			"author": "encyclopedia(AT)pommard.inria.fr, Jan 25 2000",
			"ext": [
				"More terms from _James A. Sellers_, Jun 05 2000"
			],
			"references": 2,
			"revision": 43,
			"time": "2019-11-12T19:58:35-05:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}