{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002189",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2189,
			"id": "M5039 N2175 N2326",
			"data": "17,73,241,1009,2641,8089,18001,53881,87481,117049,515761,1083289,3206641,3818929,9257329,22000801,48473881,48473881,175244281,427733329,427733329,898716289,2805544681,2805544681,2805544681",
			"name": "Pseudo-squares: a(n) = the least nonsquare positive integer which is 1 mod 8 and is a (nonzero) quadratic residue modulo the first n odd primes.",
			"reference": [
				"Michael A. Bender, R Chowdhury, A Conway, The I/O Complexity of Computing Prime Tables, In: Kranakis E., Navarro G., Chávez E. (eds) LATIN 2016: Theoretical Informatics. LATIN 2016. Lecture Notes in Computer Science, vol 9644. Springer, Berlin, Heidelberg. See Footnote 9.",
				"D. H. Lehmer, A sieve problem on \"pseudo-squares\", Math. Tables Other Aids Comp., 8 (1954), 241-242.",
				"D. H. Lehmer, E. Lehmer and D. Shanks, Integer sequences having prescribed quadratic character, Math. Comp., 24 (1970), 433-451.",
				"R. F. Lukes, C. D. Patterson and H. C. Williams, \"Some results on pseudosquares\", Mathematics of Computation 65:213 (1996), pp. 361-372.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence in two entries, N2175 and N2326.).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence).",
				"H. C. Williams and Jeffrey Shallit, Factoring integers before computers, pp. 481-531 of Mathematics of Computation 1943-1993 (Vancouver, 1993), Proc. Symp. Appl. Math., Vol. 48, Amer. Math. Soc. 1994.",
				"Kjell Wooding and H. C. Williams, \"Doubly-focused enumeration of pseudosquares and pseudocubes\". Proceedings of the 7th International Algorithmic Number Theory Symposium (ANTS VII, 2006)."
			],
			"link": [
				"Charles R Greathouse IV, \u003ca href=\"/A002189/b002189.txt\"\u003eTable of n, a(n) for n = 0..73\u003c/a\u003e (from Bernstein link)",
				"D. J. Bernstein, \u003ca href=\"http://cr.yp.to/focus.html\"\u003eDoubly focused enumeration of locally square polynomial values\u003c/a\u003e",
				"D. H. Lehmer, \u003ca href=\"/A002189/a002189_1.pdf\"\u003eA sieve problem on \"pseudo-squares\"\u003c/a\u003e, Math. Tables Other Aids Comp., 8 (1954), 241-242. [Annotated scanned copy]",
				"D. H. Lehmer, E. Lehmer and D. Shanks, \u003ca href=\"/A002189/a002189.pdf\"\u003eInteger sequences having prescribed quadratic character\u003c/a\u003e, Math. Comp., 24 (1970), 433-451 [Annotated scanned copy]",
				"Jonathan P. Sorenson, \u003ca href=\"http://arxiv.org/abs/1001.3316\"\u003eSieving for pseudosquares and pseudocubes in parallel using doubly-focused enumeration and wheel datastructures\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Pseudosquare.html\"\u003ePseudosquare\u003c/a\u003e"
			],
			"example": [
				"a(0) = 17 since 1 + 8*0 and 1 + 8*1 are squares, 17 = 1 + 8*2 is not and the quadratic residue condition is satisfied vacuosly. - _Michael Somos_, Nov 24 2018"
			],
			"mathematica": [
				"a[n_] := a[n] = (pp = Prime[ Range[2, n+1]]; k = If[ n == 0, 9, a[n-1] - 8]; While[ True, k += 8; If[ ! IntegerQ[ Sqrt[k]] \u0026\u0026 If[ Scan[ If[ ! (JacobiSymbol[k, #] == 1 ), Return[ False]] \u0026 , pp], , False, True], Break[]]]; k); Table[ Print[ an = a[n]]; an, {n, 0, 24}] (* _Jean-François Alcover_, Sep 30 2011 *)",
				"a[ n_] := If[ n \u003c 0, 0, Module[{k = If[ n == 0, 9, a[n - 1] - 8]}, While[ True, If[! IntegerQ[Sqrt[k += 8]] \u0026\u0026 Do[ If[ JacobiSymbol[k, Prime[i]] != 1, Return @ 0], {i, 2, n + 1}] =!= 0, Return @ k]]]]; (* _Michael Somos_, Nov 24 2018 *)"
			],
			"program": [
				"(PARI) a(n)=n=prime(n+1);for(s=4,1e9,forstep(k=(s^2+7)\u003e\u003e3\u003c\u003c3+1, s^2+2*s, 8, forprime(p=3, n, if(kronecker(k,p)\u003c1,next(2)));return(k))) \\\\ _Charles R Greathouse IV_, Mar 29 2012"
			],
			"xref": [
				"Cf. A018883, A045535, A090983."
			],
			"keyword": "nonn,nice",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_.",
			"ext": [
				"The PSAM reference gives a table through p = 223 (the b-file here has many more terms).",
				"More terms from _Don Reble_, Nov 14 2006",
				"Additional references from _Charles R Greathouse IV_, Oct 13 2008"
			],
			"references": 17,
			"revision": 59,
			"time": "2018-11-24T20:06:02-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}