{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A332584",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 332584,
			"data": "2,82,1888,6842,6,50,20,10,1320,28,208,32,66,148,1008,60,192,124536,282,46,128,32,28,86,40,33198,36,42,346,738,1532,246,70,68,102,306,56,20226,78316,10778,328,2432,738",
			"name": "a(n) = minimal value of n+k (with k \u003e= 1) such that the concatenation of the decimal digits of n,n+1,...,n+k is divisible by n+k+1, or -1 if no such n+k exists.",
			"comment": [
				"Certainly a(n) must be even, since no odd number can be divisible by an even number.",
				"The values of k = a(n)-n are given in the companion sequence A332580, which also has an extendedtable of values.",
				"A heuristic argument suggests that n+k should always exist."
			],
			"link": [
				"J. S. Myers, R. Schroeppel, S. R. Shannon, N. J. A. Sloane, and P. Zimmermann, \u003ca href=\"http://arxiv.org/abs/2004.14000\"\u003eThree Cousins of Recaman's Sequence\u003c/a\u003e, arXiv:2004:14000 [math.NT], April 2020."
			],
			"formula": [
				"a(n) = n + A332580(n) (trivially from the definitions)."
			],
			"example": [
				"a(1) = 2 as '1' || '2' = '12', which is divisible by 3 (where || denotes decimal concatenation).",
				"a(7) = 20 as '7' || '8' || '9' || '10' || '11' || '12' || ... || '20' = 7891011121314151617181920, which is divisible by 21.",
				"a(8) = 10 as '8' || '9' || '10' = 8910, which is divisible by 11.",
				"a(2) = 82: the concatenation 2 || 3 || ... || 82 is",
				"  23456789101112131415161718192021222324252627282930313233343536373839\\",
				"  40414243444546474849505152535455565758596061626364656667686970717273747\\",
				"  576777879808182, which is divisible by 83."
			],
			"maple": [
				"grow := proc(n,M) # searches out to a limit of M, returns [n,n+k] or [n,-1] if no k was found",
				"local R,i;",
				"R:=n;",
				"for i from n+1 to M do",
				"R:=R*10^length(i)+i;",
				"if (i mod 2) = 0 then",
				"if (R mod (i+1)) = 0 then return([n, i]); fi;",
				"fi;",
				"od:",
				"[n, -1];",
				"end;",
				"for n from 1 to 100 do lprint(grow(n,20000)); od;"
			],
			"program": [
				"(PARI) apply( {A332584(n,L=10^#Str(n),c=n)= until((c=c*L+n)%(n+1)==0, n++\u003cL||L*=10);n}, [1..17]) \\\\ _M. F. Hasler_, Feb 20 2020",
				"(Python)",
				"def A332584(n):",
				"    r, m = n, n + 1",
				"    while True:",
				"        r = r*10**(len(str(m))) + m",
				"        if m % 2 == 0 and r % (m+1) == 0:",
				"            return m",
				"        m += 1 # _Chai Wah Wu_, Jun 12 2020"
			],
			"xref": [
				"Cf. A061836 (multiplication instead of concatenation), A332580, A332585."
			],
			"keyword": "nonn,base,more",
			"offset": "1,1",
			"author": "_Scott R. Shannon_ and _N. J. A. Sloane_, Feb 16 2020",
			"references": 7,
			"revision": 47,
			"time": "2020-06-13T01:45:49-04:00",
			"created": "2020-02-17T10:10:17-05:00"
		}
	]
}