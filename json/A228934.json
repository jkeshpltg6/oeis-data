{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A228934",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 228934,
			"data": "2,4,15,-99,-199,-800,-79201,-316808,-12545596801,-50182387208,-314783998186522867201,-1259135992746091468808,-198177931028585663493396958369763763148801,-792711724114342653973587833479055052595208",
			"name": "Optimal ascending continued fraction expansion of sqrt(44) - 6.",
			"comment": [
				"See A228929 for the definition of \"optimal ascending continued fraction\".",
				"This is the first number whose expansion exhibits (in the first 20 terms) a different recurrence relation from that described in A228931.",
				"Conjecture: The terms of the expansion of sqrt(x) are all negative starting from a(4) and satisfy these recurrence relations for n\u003e=3: a(2n) = 4*a(2n-1) - 4 and a(2n+1) = -2*a(2n-1)^2 + 1.",
				"Numbers (in the range 1..1000) that exhibit this recurrence starting from some n are 44, 125, 154, 160, 176, 207, 208, 280, 352, 384, 459, 468, 500, 608, 616, 640, 665, 686, 704, 768, 800, 832, 864, 874, 875, 924."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A228934/b228934.txt\"\u003eTable of n, a(n) for n = 1..21\u003c/a\u003e"
			],
			"formula": [
				"a(2n) = 4*a(2n-1) - 4 and a(2n+1) = -2*a(2n-1)^2 + 1 for n \u003e= 3."
			],
			"example": [
				"sqrt(44) = 6 + 1/2*(1 + 1/4*(1 + 1/15*(1 - 1/99*(1 - 1/199*(1 - 1/800*(1 - 1/79201*(1 - 1/316808*(1 - 1/12545596801*(1 - ...)))))))))."
			],
			"maple": [
				"ArticoExp := proc (n, q::posint)::list; local L, i, z; Digits := 50000; L := []; z := frac(evalf(n)); for i to q+1 do if z = 0 then break end if; L := [op(L), round(1/abs(z))*sign(z)]; z := abs(z)*round(1/abs(z))-1 end do; return L end proc",
				"# List the first 20 terms of the expansion of sqrt(44)-6",
				"ArticoExp(sqrt(44),20)"
			],
			"mathematica": [
				"ArticoExp[x_, n_] := Round[1/#] \u0026 /@ NestList[Round[1/Abs[#]]*Abs[#] - 1 \u0026, FractionalPart[x], n]; Block[{$MaxExtraPrecision = 50000}, ArticoExp[Sqrt[44] - 6, 20]] (* _G. C. Greubel_, Dec 26 2016 *)"
			],
			"xref": [
				"Cf. A228929, A228931, A228932."
			],
			"keyword": "sign,cofr",
			"offset": "1,1",
			"author": "_Giovanni Artico_, Sep 11 2013",
			"ext": [
				"Minor typos corrected by _Giovanni Artico_, Sep 24 2013"
			],
			"references": 2,
			"revision": 34,
			"time": "2016-12-27T02:36:00-05:00",
			"created": "2013-09-20T12:09:38-04:00"
		}
	]
}