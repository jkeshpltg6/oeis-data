{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A100450",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 100450,
			"data": "1,7,19,51,99,195,291,483,675,963,1251,1731,2115,2787,3363,4131,4899,6051,6915,8355,9507,11043,12483,14595,16131,18531,20547,23139,25443,28803,31107,34947,38019,41859,45315,49923,53379,58851,63171,68547",
			"name": "Number of ordered triples (i,j,k) with |i| + |j| + |k| \u003c= n and gcd(i,j,k) \u003c= 1.",
			"comment": [
				"Note that gcd(0,m) = m for any m.",
				"I would also like to get the sequences of the numbers of distinct sums i+j+k (also distinct products i*j*k) over all ordered triples (i,j,k) with |i| + |j| + |k| \u003c= n; also over all ordered triples (i,j,k) with |i| + |j| + |k| \u003c= n and gcd(i,j,k) \u003c= 1.",
				"Also the sequences of the numbers of distinct sums i+j+k (also distinct products i*j*k) over all ordered triples (i,j,k) with i \u003e= 0, j \u003e= 0, k \u003e= 0 and i + j + k = n; also over all ordered triples (i,j,k) with i \u003e= 0, j \u003e= 0, k \u003e= 0, i + j + k = n and gcd(i,j,k) \u003c= 1.",
				"Also the number of ordered triples (i,j,k) with i \u003e= 0, j \u003e= 0, k \u003e= 0, i + j + k = n and gcd(i,j,k) \u003c= 1.",
				"From _Robert Price_, Mar 05 2013: (Start)",
				"The sequences that address the previous comments are:",
				"Distinct sums i+j+k with or without the GCD qualifier results in a(n)=2n+1 (A005408).",
				"Distinct products i*j*k without the GCD qualifier is given by A213207.",
				"Distinct products i*j*k with    the GCD qualifier is given by A213208.",
				"With the restriction i,j,k \u003e= 0 ...",
				"  Distinct sums or products equal to n is trivial and always equals one (A000012).",
				"  Distinct sums \u003c= n results in a(n)=n (A001477).",
				"  Distinct products \u003c= n without the GCD qualifier is given by A213213.",
				"  Distinct products \u003c= n with    the GCD qualifier is given by A213212.",
				"  Ordered triples with sum = n  without the GCD qualifier is A000217(n+1).",
				"  Ordered triples with sum = n  with    the GCD qualifier is A048240.",
				"  Ordered triples with sum \u003c= n without the GCD qualifier is A000292.",
				"  Ordered triples with sum \u003c= n with    the GCD qualifier is A048241. (End)",
				"This sequence (A100450) without the GCD qualifier results in A001845. - _Robert Price_, Jun 04 2013"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A100450/b100450.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e"
			],
			"formula": [
				"G.f.: (3 + Sum_{k\u003e=1} (moebius(k)*((1+x^k)/(1-x^k))^3))/(1-x). - _Vladeta Jovovic_, Nov 22 2004. [Sketch of proof: Let b(n) = number of ordered triples (i, j, k) with |i| + |j| + |k| = n and gcd(i, j, k) \u003c= 1. Then a(n) = A100450(n) = partial sums of b(n) and Sum_{d divides n} b(d) = 4*n^2+2 = A005899(n) with g.f. ((1+x)/(1-x))^3.]"
			],
			"maple": [
				"f:=proc(n) local i,j,k,t1,t2,t3; t1:=0; for i from -n to n do for j from -n to n do t2:=gcd(i,j); for k from -n to n do if abs(i) + abs(j) + abs(k) \u003c= n then t3:=gcd(t2,k); if t3 \u003c= 1 then t1:=t1+1; fi; fi; od: od: od: t1; end;"
			],
			"mathematica": [
				"f[n_] := Length[ Union[ Flatten[ Table[ If[ Abs[i] + Abs[j] + Abs[k] \u003c= n \u0026\u0026 GCD[i, j, k] \u003c= 1, {i, j, k}, {0, 0, 0}], {i, -n, n}, {j, -n, n}, {k, -n, n}], 2]]]; Table[ f[n], {n, 0, 40}] (* _Robert G. Wilson v_, Dec 14 2004 *)"
			],
			"xref": [
				"Cf. A000124, A000292, A018805, A027430, A048240, A048241, A100448, A100449, A213207, A213208, A213212, A213213."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Nov 21 2004",
			"references": 17,
			"revision": 45,
			"time": "2019-10-18T10:23:37-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}