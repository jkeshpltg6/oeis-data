{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A169594",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 169594,
			"data": "1,2,2,4,2,4,2,6,4,4,2,7,2,4,4,9,2,7,2,7,4,4,2,10,4,4,6,7,2,8,2,11,4,4,4,12,2,4,4,10,2,8,2,7,7,4,2,14,4,7,4,7,2,10,4,10,4,4,2,13,2,4,7,15,4,8,2,7,4,8,2,16,2,4,7,7,4,8,2,14,9,4,2,13,4,4,4,10,2,13,4,7,4,4,4,17,2,7",
			"name": "Number of divisors of n, counting divisor multiplicity in n.",
			"comment": [
				"The multiplicity of a divisor d \u003e 1 in n is defined as the largest power i for which d^i divides n; and for d = 1 it is defined as 1.",
				"a(n) is also the sum of the multiplicities of the divisors of n.",
				"In other words, a(n) = 1 + sum of the highest exponents e_i for which each number k_i in range 2 .. n divide n, as {k_i}^{e_i} | n. For nondivisors of n this exponent e_i is 0, for n itself it is 1. - _Antti Karttunen_, May 20 2017",
				"From _Gus Wiseman_, Mar 25 2021: (Start)",
				"Also the number of strict chains of divisors ending with n and having constant (equal) first quotients. The case starting with 1 is A089723. For example, the a(1) = 1 through a(12) = 7 chains are:",
				"  1  2    3    4      5    6    7    8        9      10    11    12",
				"     1|2  1|3  1|4    1|5  1|6  1|7  1|8      1|9    1|10  1|11  1|12",
				"               2|4         2|6       2|8      3|9    2|10        2|12",
				"               1|2|4       3|6       4|8      1|3|9  5|10        3|12",
				"                                     2|4|8                       4|12",
				"                                     1|2|4|8                     6|12",
				"                                                                 3|6|12",
				"(End)",
				"a(n) only depends on the prime signature of n. - _David A. Corneth_, Mar 28 2021"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A169594/b169594.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"The divisors of 8 are 1, 2, 4, 8 of multiplicity 1, 3, 1, 1, respectively. So a(8) = 1 + 3 + 1 + 1 = 6."
			],
			"mathematica": [
				"divmult[d_, n_] := Module[{output, i}, If[d == 1, output = 1, If[d == n, output = 1, i = 0; While[Mod[n, d^(i + 1)] == 0, i = i + 1]; output = i]]; output]; dmt0[n_] := Module[{divs, l}, divs = Divisors[n]; l = Length[divs]; Sum[divmult[divs[[i]], n], {i, 1, l}]]; Table[dmt0[i], {i, 1, 40}]",
				"Table[1 + DivisorSum[n, IntegerExponent[n, #] \u0026, # \u003e 1 \u0026], {n, 98}] (* _Michael De Vlieger_, May 20 2017 *)"
			],
			"program": [
				"(PARI)",
				"A286561(n,k) = { my(i=1); if(1==k, 1, while(!(n%(k^i)), i = i+1); (i-1)); };",
				"A169594(n) = sumdiv(n,d,A286561(n,d)); \\\\ _Antti Karttunen_, May 20 2017",
				"(PARI) a(n) = { if(n == 1, return(1)); my(f = factor(n), u = vecmax(f[, 2]), cf = f, res = numdiv(f) - u + 1); for(i = 2, u, cf[, 2] = f[, 2]\\i; res+=numdiv(factorback(cf)) ); res } \\\\ _David A. Corneth_, Mar 29 2021",
				"(Scheme)",
				"(define (A169594 n) (add (lambda (k) (A286561bi n k)) 1 n))",
				";; Implements sum_{i=lowlim..uplim} intfun(i)",
				"(define (add intfun lowlim uplim) (let sumloop ((i lowlim) (res 0)) (cond ((\u003e i uplim) res) (else (sumloop (1+ i) (+ res (intfun i)))))))",
				";; For A286561bi see A286561. - _Antti Karttunen_, May 20 2017",
				"(Python)",
				"def a286561(n, k):",
				"    i=1",
				"    if k==1: return 1",
				"    while n%(k**i)==0:",
				"        i+=1",
				"    return i-1",
				"def a(n): return sum([a286561(n, d) for d in divisors(n)]) # _Indranil Ghosh_, May 20 2017"
			],
			"xref": [
				"Cf. A168512.",
				"Row sums of A286561, A286563 and A286564.",
				"A001055 counts factorizations (strict: A045778, ordered: A074206).",
				"A057567 counts chains of divisors with weakly increasing first quotients.",
				"A067824 counts strict chains of divisors ending with n.",
				"A253249 counts strict chains of divisors.",
				"A334997 counts chains of divisors of n by length.",
				"A342086 counts chains of divisors with strictly increasing first quotients.",
				"A342496 counts partitions with equal first quotients (strict: A342515, ranking: A342522, ordered: A342495).",
				"A342530 counts chains of divisors with distinct first quotients.",
				"Cf. A003238, A003242, A069916, A122651, A318991, A318992, A325545."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Joseph L. Pe_, Dec 02 2009",
			"ext": [
				"Extended by _Ray Chandler_, Dec 08 2009"
			],
			"references": 17,
			"revision": 30,
			"time": "2021-03-29T10:37:49-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}