{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A272393",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 272393,
			"data": "1,0,1,1,9,51,369,2661,19929,151936,1178289,9259812,73593729,590475744,4776464121,38912018796,318971849625,2629040965776,21774894337449,181136924953317,1512731101731499,12678230972826340,106600213003114719",
			"name": "Degeneracies of entanglement witness eigenstates for n spin 4 irreducible representations.",
			"link": [
				"Gheorghe Coserea, \u003ca href=\"/A272393/b272393.txt\"\u003eTable of n, a(n) for n = 0..401\u003c/a\u003e",
				"Eliahu Cohen, Tobias Hansen, Nissan Itzhaki, \u003ca href=\"http://arxiv.org/abs/1511.06623\"\u003eFrom Entanglement Witness to Generalized Catalan Numbers\u003c/a\u003e, arXiv:1511.06623 [quant-ph], 2015.",
				"T. L. Curtright, T. S. Van Kortryk, and C. K. Zachos, \u003ca href=\"https://hal.archives-ouvertes.fr/hal-01345527\"\u003eSpin Multiplicities\u003c/a\u003e, hal-01345527, 2016."
			],
			"formula": [
				"a(n) ~ (3/40)^(3/2)*9^n/(sqrt(Pi)*n^(3/2)) * (1-129/(160*n)+O(1/n^2))). - _Thomas Curtright_, Jun 17 2016, updated Jul 26 2016",
				"D-finite with recurrence 8*n*(2*n - 5)*(2*n - 3)*(2*n - 1)*(4*n - 1)*(4*n + 1)*(5*n - 17)*(5*n - 12)*(5*n - 8)*(5*n - 7)*a(n) = (n-1)*(2*n - 5)*(2*n - 3)*(5*n - 17)*(5*n - 12)*(5*n - 3)*(5*n - 2)*(2101*n^3 - 6506*n^2 + 6608*n - 2200)*a(n-1) + 9*(n-1)*(2*n - 5)*(2*n - 1)*(4*n - 1)*(5*n - 17)*(5*n - 8)*(5*n - 7)*(385*n^3 - 1659*n^2 + 2008*n - 492)*a(n-2) - 81*(n-2)*(n-1)*(2*n - 3)*(5*n - 12)*(5*n - 3)*(5*n - 2)*(1020*n^4 - 8088*n^3 + 21761*n^2 - 22557*n + 7264)*a(n-3) - 729*(n-3)*(n-2)*(n-1)*(2*n - 5)*(2*n - 1)*(4*n - 1)*(5*n - 17)*(5*n - 8)*(5*n - 7)*(5*n - 2)*a(n-4) + 6561*(n-4)*(n-3)*(n-2)*(n-1)*(2*n - 3)*(2*n - 1)*(5*n - 12)*(5*n - 7)*(5*n - 3)*(5*n - 2)*a(n-5). - _Vaclav Kotesovec_, Jun 24 2016",
				"a(n) = (1/Pi)*int((sin(9x)/sin(x))^n*(sin(x))^2,x,0,2Pi). - _Thomas Curtright_, Jun 24 2016"
			],
			"mathematica": [
				"a[n_]:= 2/Pi*Integrate[Sqrt[(1-t)/t]*((4t-1)(4t(4t-3)^2-1))^n, {t, 0, 1}] (* _Thomas Curtright_, Jun 24 2016 *)",
				"a[n_]:= c[0, n, 4]-c[1, n, 4]; c[j_, n_, s_]:= Sum[(-1)^k*Binomial[n, k]*Binomial[j - (2*s + 1)*k + n + n*s - 1, j - (2*s + 1)*k + n*s], {k, 0, Floor[(j + n*s)/(2*s + 1)]}]; Table[a[n], {n, 0, 20}] (* _Thomas Curtright_, Jul 26 2016 *)",
				"a[n_]:= mult[0, n, 4]",
				"mult[j_,n_,s_]:=Sum[(-1)^(k+1)*Binomial[n,k]*Binomial[n*s+j-(2*s+1)*k+n- 1,n*s+j-(2*s+1)*k+1],{k,0,Floor[(n*s+j+1)/(2*s+1)]}] (* _Thomas Curtright_, Jun 14 2017 *)"
			],
			"program": [
				"(PARI)",
				"N = 26; S = 4;",
				"M = matrix(N+1, N*numerator(S)+1);",
				"Mget(n, j) =  { M[1 + n, 1 + j*denominator(S)] };",
				"Mset(n, j, v) = { M[1 + n, 1 + j*denominator(S)] = v };",
				"Minit() = {",
				"  my(step = 1/denominator(S));",
				"  Mset(0, 0, 1);",
				"  for (n = 1, N, forstep (j = 0, n*S, step,",
				"     my(acc = 0);",
				"     for (k = abs(j-S), min(j+S, (n-1)*S), acc += Mget(n-1, k));",
				"     Mset(n, j, acc)));",
				"};",
				"Minit();",
				"vector(1 + N\\denominator(S), n, Mget((n-1)*denominator(S),0))"
			],
			"xref": [
				"For spin S = 1/2, 1, 3/2, 2, 5/2, 3, 7/2, 4, 9/2, 5 we get A000108, A005043, A264607, A007043, A272391, A264608, A272392, this sequence, A272394, A272395.",
				"Cf. A348210 (column k=4)."
			],
			"keyword": "nonn",
			"offset": "0,5",
			"author": "_Gheorghe Coserea_, Apr 28 2016",
			"references": 8,
			"revision": 70,
			"time": "2021-10-12T07:55:43-04:00",
			"created": "2016-04-28T21:52:29-04:00"
		}
	]
}