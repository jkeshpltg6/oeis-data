{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A349056",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 349056,
			"data": "1,1,1,1,1,2,1,1,1,2,1,3,1,2,2,1,1,3,1,3,2,2,1,4,1,2,1,3,1,4,1,1,2,2,2,4,1,2,2,4,1,4,1,3,3,2,1,5,1,3,2,3,1,4,2,4,2,2,1,6,1,2,3,1,2,4,1,3,2,4,1,6,1,2,3,3,2,4,1,5,1,2,1,6,2,2,2",
			"name": "Number of weakly alternating permutations of the multiset of prime factors of n.",
			"comment": [
				"We define a sequence to be weakly alternating if it is alternately weakly increasing and weakly decreasing, starting with either. Then a sequence is alternating in the sense of A025047 iff it is a weakly alternating anti-run.",
				"A prime index of n is a number m such that prime(m) divides n. For n \u003e 1, the multiset of prime factors of n is row n of A027746. The prime indices A112798 can also be used."
			],
			"example": [
				"The following are the weakly alternating permutations for selected n:",
				"n = 2   6    12    24     48      60     90     120     180",
				"   ----------------------------------------------------------",
				"    2   23   223   2223   22223   2253   2335   22253   22335",
				"        32   232   2232   22232   2325   2533   22325   22533",
				"             322   2322   22322   2523   3253   22523   23253",
				"                   3222   23222   3252   3325   23252   23352",
				"                          32222   3522   3352   25232   25233",
				"                                  5232   3523   32225   25332",
				"                                         5233   32522   32325",
				"                                         5332   35222   32523",
				"                                                52223   33252",
				"                                                52322   33522",
				"                                                        35232",
				"                                                        52323",
				"                                                        53322"
			],
			"mathematica": [
				"primeMS[n_]:=If[n==1,{},Flatten[Cases[FactorInteger[n],{p_,k_}:\u003eTable[PrimePi[p],{k}]]]];",
				"whkQ[y_]:=And@@Table[If[EvenQ[m],y[[m]]\u003c=y[[m+1]],y[[m]]\u003e=y[[m+1]]],{m,1,Length[y]-1}];",
				"Table[Length[Select[Permutations[primeMS[n]],whkQ[#]||whkQ[-#]\u0026]],{n,100}]"
			],
			"xref": [
				"Counting all permutations of prime factors gives A008480.",
				"The variation counting anti-run permutations is A335452.",
				"The strong case is A345164, with twins A344606.",
				"Compositions of this type are counted by A349052, also A129852 and A129853.",
				"Compositions not of this type are counted by A349053, ranked by A349057.",
				"The version for patterns is A349058, strong A345194.",
				"The version for ordered factorizations is A349059, strong A348610.",
				"Partitions of this type are counted by A349060, complement A349061.",
				"The complement is counted by A349797.",
				"The non-alternating case is A349798.",
				"A001250 counts alternating permutations, complement A348615.",
				"A003242 counts Carlitz (anti-run) compositions.",
				"A025047 counts alternating or wiggly compositions, ranked by A345167.",
				"A056239 adds up prime indices, row sums of A112798, row lengths A001222.",
				"A071321 gives the alternating sum of prime factors, reverse A071322.",
				"A344616 gives the alternating sum of prime indices, reverse A316524.",
				"A345165 counts partitions w/o an alternating permutation, ranked by A345171.",
				"A345170 counts partitions w/ an alternating permutation, ranked by A345172.",
				"A348379 counts factorizations with an alternating permutation.",
				"A349800 counts weakly but not strongly alternating compositions.",
				"Cf. A028234, A051119, A096441, A335433, A335448, A344614, A344652, A344653, A345173, A345192."
			],
			"keyword": "nonn",
			"offset": "1,6",
			"author": "_Gus Wiseman_, Dec 02 2021",
			"references": 15,
			"revision": 10,
			"time": "2021-12-10T11:12:22-05:00",
			"created": "2021-12-10T11:12:22-05:00"
		}
	]
}