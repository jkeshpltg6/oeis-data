{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A093602",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 93602,
			"data": "1,8,1,3,7,9,9,3,6,4,2,3,4,2,1,7,8,5,0,5,9,4,0,7,8,2,5,7,6,4,2,1,5,5,7,3,2,2,8,4,0,6,6,2,4,8,0,9,2,7,4,0,5,7,5,5,6,9,8,8,4,9,3,5,3,8,8,1,2,3,1,8,1,1,2,6,3,5,3,8,8,3,6,8,4,1,2,4,9,8,8,2,1,2,0,6,0,1,6,8,8,5,6,2,2",
			"name": "Decimal expansion of Pi/sqrt(3) = sqrt(2*zeta(2)).",
			"comment": [
				"Volume of a cube with edge length 1 rotated about a space diagonal. See MathWorld Cube page. - _Francis Wolinski_, Mar 10 2019"
			],
			"link": [
				"Harry J. Smith, \u003ca href=\"/A093602/b093602.txt\"\u003eTable of n, a(n) for n = 1..20000\u003c/a\u003e",
				"Peter Bala, \u003ca href=\"/A002117/a002117.pdf\"\u003eNew series for old functions\u003c/a\u003e",
				"Jean Dolbeault, Ari Laptev and Michael Loss, \u003ca href=\"http://doi.org/10.4171/JEMS/142\"\u003eLieb-Thirring inequalities with improved constants\u003c/a\u003e, Vol. 10, No. 4 (2008), pp. 1121-1126, \u003ca href=\"https://arxiv.org/abs/0708.1165\"\u003epreprint\u003c/a\u003e, arXiv:0708.1165 [math.AP], 2007.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/No-Three-in-a-Line-Problem.html\"\u003eNo-Three-in-a-Line-Problem\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Cube.html\"\u003eCube\u003c/a\u003e",
				"\u003ca href=\"/index/Tra#transcendental\"\u003eIndex entries for transcendental numbers\u003c/a\u003e"
			],
			"formula": [
				"Equals Integral_{x=0..infinity} x^(1/3)/(1+x^2) dx. - _Jean-François Alcover_, May 24 2013",
				"Equals (3/2)*( Integral_{x=0..infinity} 1/(1+x+x^2) dx ). - _Bruno Berselli_, Jul 23 2013",
				"Equals Sum_{n \u003e= 0} (1/(6*n+1) - 4/(6*n+2) - 5/(6*n+3) - 1/(6*n+4) + 4/(6*n+5) + 5/(6*n+6)). - _Mats Granvik_, Sep 23 2013",
				"Equals (1/2) * Sum_{n \u003e= 0} (14*n + 11)*(-1/3)^n/((4*n + 1)*(4*n + 3)*binomial(4*n,2*n)). For more series representations of this type see the Bala link. - _Peter Bala_, Feb 04 2015",
				"From _Peter Bala_, Nov 02 2019: (Start)",
				"Equals 3*Sum_{n \u003e= 1}  1/( (3*n - 1)*(3*n - 2) ).",
				"Equals 2 - 6*Sum_{n \u003e= 1}  1/( (3*n - 1)*(3*n + 1)*(3*n + 2) ).",
				"Equals 5!*Sum_{n \u003e= 1}  1/( (3*n - 1)*(3*n - 2)*(3*n + 2)*(3*n + 4) ).",
				"Equals 3*( 1 - 2*Sum_{n \u003e= 1} 1/(9*n^2 - 1) ).",
				"Equals 1 + Sum_{n \u003e=1 } (-1)^(n+1)*(6*n + 1)/(n*(n + 1)*(3*n + 1)*(3*n - 2)).",
				"Equals (27/2)*Sum_{n \u003e= 1} (2*n + 1)/( (3*n - 1)*(3*n + 1)*(3*n + 2)*(3*n + 4) ).",
				"Equals 3*Integral_{x = 0..1} 1/(1 + x + x^2) dx.",
				"Equals 3*Integral_{x = 0..1} (1 + x)/(1 - x + x^2) dx.",
				"Equals 3*Integral_{x = 0..inf} cosh(x)/cosh(3*x) dx. (End)",
				"Equals Integral_{x = 0..oo} log(1+x^3)/x^3 dx. - _Amiram Eldar_, Aug 20 2020"
			],
			"example": [
				"Pi/sqrt(3) = 1.8137993642342178505940782576421557322840662480927405755..."
			],
			"mathematica": [
				"RealDigits[Pi/Sqrt[3],10,120][[1]] (* _Harvey P. Dale_, Mar 04 2012 *)"
			],
			"program": [
				"(PARI) default(realprecision, 20080); x=Pi*sqrt(3)/3; for (n=1, 20000, d=floor(x); x=(x-d)*10; write(\"b093602.txt\", n, \" \", d)); \\\\ _Harry J. Smith_, Jun 19 2009",
				"(MAGMA) SetDefaultRealField(RealField(100)); R:= RealField(); Pi(R)/Sqrt(3); // _G. C. Greubel_, Mar 10 2019",
				"(Sage) numerical_approx(pi/sqrt(3), digits=100) # _G. C. Greubel_, Mar 10 2019"
			],
			"xref": [
				"Continued fraction expansion is A132116. - _Jonathan Vos Post_, Aug 10 2007",
				"Equals twice A093766.",
				"Cf. A343235 (using the reciprocal)."
			],
			"keyword": "easy,nonn,cons",
			"offset": "1,2",
			"author": "_Lekraj Beedassy_, May 14 2004",
			"references": 17,
			"revision": 75,
			"time": "2021-04-19T11:34:51-04:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}