{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A129172",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 129172,
			"data": "1,0,1,0,0,1,1,1,0,0,0,1,1,3,2,1,1,1,0,0,0,0,1,1,4,5,5,3,5,4,3,2,1,1,1,0,0,0,0,0,1,1,5,7,12,10,11,12,14,12,10,8,10,7,5,4,3,2,1,1,1,0,0,0,0,0,0,1,1,6,9,19,23,28,26,36,38,38,32,36,36,34,29,27,25,21,15,16,13,10,7,5",
			"name": "Triangle read by rows: T(n,k) is the number of skew Dyck paths of semilength n such that the area between the x-axis and the path is k (n \u003e= 0, 0 \u003c= k \u003c= n^2).",
			"comment": [
				"A skew Dyck path is a path in the first quadrant which begins at the origin, ends on the x-axis, consists of steps U=(1,1)(up), D=(1,-1)(down) and L=(-1,-1)(left) so that up and left steps do not overlap. The length of the path is defined to be the number of its steps.",
				"Row n has n^2 + 1 terms, the first n of which are equal to 0.",
				"Row sums yield A002212.",
				"Sum of terms in column k is the Fibonacci number F(k) (k \u003e= 1; F(1)=1, F(2)=1; A000045)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A129172/b129172.txt\"\u003eRows n = 0..40, flattened\u003c/a\u003e",
				"E. Deutsch, E. Munarini and S. Rinaldi, \u003ca href=\"http://dx.doi.org/10.1016/j.jspi.2010.01.015\"\u003eSkew Dyck paths\u003c/a\u003e, J. Stat. Plann. Infer. 140 (8) (2010) 2191-2203.",
				"Emeric Deutsch, Emanuele Munarini and Simone Rinaldi, \u003ca href=\"https://doi.org/10.1016/j.jspi.2009.12.013\"\u003eSkew Dyck paths, area, and superdiagonal bargraphs\u003c/a\u003e, Journal of Statistical Planning and Inference, Vol. 140, Issue 6, June 2010, pp. 1550-1562."
			],
			"formula": [
				"Sum_{k=0..n^2} k*T(n,k) = A129173(n).",
				"G.f.: G(t,z) = H(t,1,z), where H(t,x,z) = 1+txzH(t,t^2*x,z)H(t,x,z) + z[H(t,t^2*x,z)-1] (H(t,x,z) is the trivariate g.f. for skew Dyck paths according to area, semiabscissa of the last point on the x-axis and semilength, marked by t,x and z, respectively)."
			],
			"example": [
				"T(4,7)=5 because we have UDUUUDLD, UDUUDUDL, UUDDUUDL, UUUDLDUD and UUUUDLLL.",
				"Triangle starts:",
				"  1;",
				"  0, 1;",
				"  0, 0, 1, 1, 1;",
				"  0, 0, 0, 1, 1, 3, 2, 1, 1, 1;",
				"  0, 0, 0, 0, 1, 1, 4, 5, 5, 3, 5, 4, 3, 2, 1, 1, 1;"
			],
			"maple": [
				"G:=(1-z+z*g[1])/(1-t*x*z*g[1]): for i from 1 to 9 do g[i]:=(1-z+z*g[i+1])/(1-t^(2*i+1)*x*z*g[i+1]) od: g[10]:=0: x:=1: Gser:=simplify(series(G,z=0,9)): for n from 0 to 7 do P[n]:=sort(coeff(Gser,z,n)) od: for n from 0 to 7 do seq(coeff(P[n],t,j),j=0..n^2) od; # yields sequence in triangular form",
				"# second Maple program:",
				"b:= proc(n, y, t) option remember; expand(`if`(y\u003en, 0,",
				"      `if`(n=0, 1, `if`(t\u003c0, 0, b(n-1, y+1, 1)*z^(y+1/2))+",
				"      `if`(y\u003c1, 0, b(n-1, y-1, 0)*z^(y-1/2))+",
				"      `if`(t\u003e0 or y\u003c1, 0, b(n-1, y-1, -1)*z^(1/2-y)))))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, z, i), i=0..n^2))(b(2*n, 0$2)):",
				"seq(T(n), n=0..8);  # _Alois P. Heinz_, Jun 19 2016"
			],
			"mathematica": [
				"b[n_, y_, t_] := b[n, y, t] = Expand[If[y \u003e n, 0, If[n == 0, 1, If[t \u003c 0, 0, b[n - 1, y + 1, 1]*z^(y + 1/2)] + If[y \u003c 1, 0, b[n - 1, y - 1, 0]*z^(y - 1/2)] + If[t \u003e 0 || y \u003c 1, 0, b[n - 1, y - 1, -1]*z^(1/2 - y)]]]]; T[n_] := Function[p, Table[Coefficient[p, z, i], {i, 0, n^2}]][b[2*n, 0, 0]]; Table[T[n], {n, 0, 8}] // Flatten (* _Jean-François Alcover_, Dec 20 2016, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A000045, A002212, A129173, A274372."
			],
			"keyword": "nonn,tabf,changed",
			"offset": "0,14",
			"author": "_Emeric Deutsch_, Apr 09 2007",
			"references": 3,
			"revision": 22,
			"time": "2022-01-10T10:43:09-05:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}