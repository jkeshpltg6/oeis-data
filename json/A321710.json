{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A321710",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 321710,
			"data": "1,3,12,1,56,15,288,165,8,1584,1611,252,9152,14805,4956,180,54912,131307,77992,9132,339456,1138261,1074564,268980,8064,2149888,9713835,13545216,6010220,579744,13891584,81968469,160174960,112868844,23235300,604800,91287552,685888171,1805010948,1877530740,684173164,57170880,608583680,5702382933,19588944336,28540603884,16497874380,2936606400,68428800,4107939840,47168678571,206254571236,404562365316,344901105444,108502598960,8099018496",
			"name": "Triangle read by rows: T(n,k) is the number of rooted hypermaps of genus k with n darts.",
			"comment": [
				"Row n contains floor((n+1)/2) = A008619(n-1) terms."
			],
			"link": [
				"Gheorghe Coserea, \u003ca href=\"/A321710/b321710.txt\"\u003eRows n = 1..42, flattened\u003c/a\u003e",
				"Alain Giorgetti and Timothy R. S. Walsh, \u003ca href=\"https://amc-journal.eu/index.php/amc/article/download/1115/1221\"\u003eEnumeration of hypermaps of a given genus\u003c/a\u003e, Ars Math. Contemp. 15 (2018) 225-266.",
				"Timothy R. Walsh, \u003ca href=\"http://www.info2.uqam.ca/~walsh_t/papers/GENERATING NONISOMORPHIC.pdf\"\u003eSpace-efficient generation of nonisomorphic maps and hypermaps\u003c/a\u003e",
				"T. R. Walsh, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL18/Walsh/walsh3.html\"\u003eSpace-Efficient Generation of Nonisomorphic Maps and Hypermaps\u003c/a\u003e, J. Int. Seq. 18 (2015) # 15.4.3.",
				"P. G. Zograf, \u003ca href=\"https://doi.org/10.1093/imrn/rnv077\"\u003eEnumeration of Grothendieck's Dessins and KP Hierarchy\u003c/a\u003e, International Mathematics Research Notices, Volume 2015, Issue 24, 1 January 2015, 13533-13544.",
				"Peter Zograf, \u003ca href=\"https://arxiv.org/abs/1312.2538\"\u003eEnumeration of Grothendieck's Dessins and KP Hierarchy\u003c/a\u003e, arXiv:1312.2538 [math.CO], 2014."
			],
			"formula": [
				"A000257(n)=T(n,0), A118093(n)=T(n,1), A214817(n)=T(n,2), A214818(n)=T(n,3), A060593(n)=T(2*n+1,n)=(2*n)!/(n+1), A003319(n+1)=Sum_{k=0..floor((n-1)/2)} T(n,k)."
			],
			"example": [
				"Triangle starts:",
				"n\\k  [0]       [1]        [2]         [3]         [4]        [5]",
				"[1]  1;",
				"[2]  3;",
				"[3]  12,       1;",
				"[4]  56,       15;",
				"[5]  288,      165,       8;",
				"[6]  1584,     1611,      252;",
				"[7]  9152,     14805,     4956,       180;",
				"[8]  54912,    131307,    77992,      9132;",
				"[9]  339456,   1138261,   1074564,    268980,     8064;",
				"[10] 2149888,  9713835,   13545216,   6010220,    579744;",
				"[11] 13891584, 81968469,  160174960,  112868844,  23235300,  604800;",
				"[12] 91287552, 685888171, 1805010948, 1877530740, 684173164, 57170880;",
				"[13] ..."
			],
			"program": [
				"(PARI)",
				"L1(f, N) = sum(i=2, N, (i-1)*t[i]*deriv(f, t[i-1]));",
				"M1(f, N) = {",
				"  sum(i=2, N, sum(j=1, i-1, (i-1)*t[j]*t[i-j]*deriv(f, t[i-1]) +",
				"      j*(i-j)*t[i+1]*deriv(deriv(f, t[j]), t[i-j])));",
				"};",
				"F(N) = {",
				"  my(u='x, v='x, f=vector(N)); t=vector(N+1, n, eval(Str(\"t\", n)));",
				"  f[1] = u*v*t[1];",
				"  for (n=2, N, f[n] = (u + v)*L1(f[n-1], n) + M1(f[n-1], n) +",
				"    sum(i=2, n-1, t[i+1]*sum(j=1, i-1,",
				"    j*(i-j)*sum(k=1, n-2, deriv(f[k], t[j])*deriv(f[n-1-k], t[i-j]))));",
				"    f[n] /= n);",
				"  f;",
				"};",
				"seq(N) = {",
				"  my(f=F(N), v=substvec(f, t, vector(#t, n, 'x)),",
				"     g=vector(#v, n, Polrev(Vec(n * v[n]))));",
				"  apply(p-\u003eVecrev(substpol(p, 'x^2, 'x)), g);",
				"};",
				"concat(seq(14))"
			],
			"xref": [
				"Columns k=0..9 give: A000257 (k=0), A118093 (k=1), A214817 (k=2), A214818 (k=3), A318104 (k=4), A321705 (k=5), A321706 (k=6), A321707 (k=7), A321708 (k=8), A321709 (k=9).",
				"Row sums give A003319(n+1).",
				"Cf. A008619, A060593."
			],
			"keyword": "nonn,tabf",
			"offset": "1,2",
			"author": "_Gheorghe Coserea_, Nov 17 2018",
			"references": 7,
			"revision": 23,
			"time": "2018-12-17T19:08:44-05:00",
			"created": "2018-12-17T19:07:30-05:00"
		}
	]
}