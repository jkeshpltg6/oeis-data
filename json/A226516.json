{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A226516",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 226516,
			"data": "0,0,0,0,0,0,1,0,0,0,0,1,1,1,1,1,2,1,2,1,2,2,3,3,4,4,6,5,8,6,10,8,13,11,17,15,23,20,31,26,41,34,54,45,71,60,94,80,125,106,166,140,220,185,291,245,385,325,510,431,676,571,896,756,1187,1001,1572,1326,2082,1757,2758,2328,3654,3084,4841,4085,6413",
			"name": "Number of (18,7)-reverse multiples with n digits.",
			"comment": [
				"Comment from _Emeric Deutsch_, Aug 21 2016 (Start):",
				"Given an increasing sequence of positive integers S = {a0, a1, a2, ... }, let",
				"          F(x) = x^{a0} + x^{a1} + x^{a2} + ... .",
				"Then the g. f. for the number of palindromic compositions of n with parts in S is (see Hoggatt and Bicknell, Fibonacci Quarterly, 13(4), 1975, 350 - 356):",
				"      (1 + F(x))/(1 - F(x^2))",
				"Playing with this, I have found easily that",
				"1. number of palindromic compositions of n into {3,4,5,...} = A226916(n+4);",
				"2. number of palindromic compositions of n into {1,4,7,10,13,...} = A226916(n+6);",
				"3. number of palindromic compositions of n into {1,4} = A226517(n+10);",
				"4. number of palindromic compositions of n into {1,5} = A226516(n+11).",
				"(End)"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A226516/b226516.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"http://arxiv.org/abs/1307.0453\"\u003e2178 And All That\u003c/a\u003e, Fib. Quart., 52 (2014), 99-120.",
				"\u003ca href=\"/index/Rec#order_10\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,1,0,0,0,0,0,0,0,1)."
			],
			"formula": [
				"G.f.: x^6*(1-x^2+x^5+x^6)/(1-x^2-x^10).",
				"a(n) = a(n-2) + a(n-10) for n\u003e12, with initial values a(0)-a(12) equal to 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1. [_Bruno Berselli_, Jun 17 2013]"
			],
			"maple": [
				"f:=proc(n) option remember;",
				"if",
				"n \u003c= 5 then 0",
				"elif n=6 then 1",
				"elif n \u003c= 10 then 0",
				"elif n \u003c= 12 then 1",
				"else f(n-2)+f(n-10)",
				"fi;",
				"end;",
				"[seq(f(n),n=0..100)]"
			],
			"mathematica": [
				"CoefficientList[Series[x^6 (1 - x^2 + x^5 + x^6) / (1 - x^2 - x^10), {x, 0, 80}], x] (* _Vincenzo Librandi_, Jun 18 2013 *)",
				"LinearRecurrence[{0,1,0,0,0,0,0,0,0,1},{0,0,0,0,0,0,1,0,0,0,0,1,1},80] (* _Harvey P. Dale_, Jun 17 2015 *)"
			],
			"xref": [
				"Cf. A214927, A226517, A226916."
			],
			"keyword": "nonn,easy,base",
			"offset": "0,17",
			"author": "_N. J. A. Sloane_, Jun 16 2013",
			"references": 4,
			"revision": 29,
			"time": "2016-08-21T11:35:21-04:00",
			"created": "2013-06-16T15:08:08-04:00"
		}
	]
}