{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A340654",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 340654,
			"data": "1,1,1,1,1,1,1,1,1,1,1,2,1,1,1,1,1,2,1,2,1,1,1,3,1,1,1,2,1,1,1,1,1,1,1,5,1,1,1,3,1,1,1,2,2,1,1,4,1,2,1,2,1,3,1,3,1,1,1,3,1,1,2,1,1,1,1,2,1,1,1,6,1,1,2,2,1,1,1,4,1,1,1,3,1,1,1",
			"name": "Number of cross-balanced factorizations of n.",
			"comment": [
				"We define a factorization of n into factors \u003e 1 to be cross-balanced if either (1) it is empty or (2) the maximum image of A001222 over the factors is A001221(n)."
			],
			"example": [
				"The cross-balanced factorizations for n = 12, 24, 36, 72, 144, 240:",
				"  2*6   4*6     4*9     2*4*9     4*4*9       8*30",
				"  3*4   2*2*6   6*6     2*6*6     4*6*6       12*20",
				"        2*3*4   2*2*9   3*4*6     2*2*4*9     5*6*8",
				"                2*3*6   2*2*2*9   2*2*6*6     2*4*30",
				"                3*3*4   2*2*3*6   2*3*4*6     2*6*20",
				"                        2*3*3*4   3*3*4*4     2*8*15",
				"                                  2*2*2*2*9   3*4*20",
				"                                  2*2*2*3*6   3*8*10",
				"                                  2*2*3*3*4   4*5*12",
				"                                              2*10*12",
				"                                              2*3*5*8",
				"                                              2*2*2*30",
				"                                              2*2*3*20",
				"                                              2*2*5*12"
			],
			"mathematica": [
				"facs[n_]:=If[n\u003c=1,{{}},Join@@Table[Map[Prepend[#,d]\u0026,Select[facs[n/d],Min@@#\u003e=d\u0026]],{d,Rest[Divisors[n]]}]];",
				"Table[Length[Select[facs[n],#=={}||PrimeNu[n]==Max[PrimeOmega/@#]\u0026]],{n,100}]"
			],
			"xref": [
				"Positions of terms \u003e 1 are A126706.",
				"Positions of 1's are A303554.",
				"The co-balanced version is A340596.",
				"The version for unlabeled multiset partitions is A340651.",
				"The balanced version is A340653.",
				"The twice-balanced version is A340655.",
				"A001055 counts factorizations.",
				"A045778 counts strict factorizations.",
				"A316439 counts factorizations by product and length.",
				"A320655 counts factorizations into semiprimes.",
				"Other balance-related sequences:",
				"- A010054 counts balanced strict partitions.",
				"- A047993 counts balanced partitions.",
				"- A098124 counts balanced compositions.",
				"- A106529 lists Heinz numbers of balanced partitions.",
				"- A340597 have an alt-balanced factorization.",
				"- A340598 counts balanced set partitions.",
				"- A340599 counts alt-balanced factorizations.",
				"- A340652 counts unlabeled twice-balanced multiset partitions.",
				"- A340656 have no twice-balanced factorizations.",
				"- A340657 have a twice-balanced factorization.",
				"Cf. A003963, A117409, A303975, A320656, A324518, A339846, A339890, A340608."
			],
			"keyword": "nonn",
			"offset": "1,12",
			"author": "_Gus Wiseman_, Jan 15 2021",
			"references": 22,
			"revision": 11,
			"time": "2021-01-19T21:52:20-05:00",
			"created": "2021-01-19T21:52:20-05:00"
		}
	]
}