{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A243987",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 243987,
			"data": "1,1,2,1,1,2,1,2,2,3,1,1,1,1,2,1,2,3,3,3,4,1,1,1,1,1,1,2,1,2,2,3,3,3,3,4,1,1,2,2,2,2,2,2,3,1,2,2,2,3,3,3,3,3,4,1,1,1,1,1,1,1,1,1,1,2,1,2,3,4,4,5,5,5,5,5,5,6,1,1,1,1,1,1,1,1,1,1,1,1,2,1,2,2,2,2,2,3,3,3,3,3,3,3,4,1,1,2,2,3,3,3,3,3,3,3,3,3,3,4",
			"name": "Triangle read by rows: T(n, k) is the number of divisors of n that are less than or equal to k for 1 \u003c= k \u003c= n.",
			"comment": [
				"This triangular sequence T(n,k) generalizes sequence A000005, the number of divisors of n; in particular, A000005(n) = T(n,n).",
				"Also, for prime p, T(p,k) = 1 when k \u003c p and T(p,p) = 2."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A243987/b243987.txt\"\u003eRows n = 1..125 of triangle, flattened\u003c/a\u003e",
				"Dennis P. Walsh, \u003ca href=\"http://capone.mtsu.edu/dwalsh/FACTORNK.pdf\"\u003eNotes on counting the divisors of n\u003c/a\u003e"
			],
			"formula": [
				"T(n,1) = 1; T(n,n) = A000005(n).",
				"T(n,k) = coefficient of the x^n term in the expansion of Sum(x^j/(1-x^j), j=1..k).",
				"T(n,k) = Sum_{j=1..k} A051731(n,j). - _Reinhard Zumkeller_, Apr 22 2015"
			],
			"example": [
				"T(6,4)=3 since there are 3 divisors of 6 that are less than or equal to 4, namely, 1, 2 and 3.",
				"T(n,k) as a triangle, n=1..15:",
				"1,",
				"1, 2,",
				"1, 1, 2,",
				"1, 2, 2, 3,",
				"1, 1, 1, 1, 2,",
				"1, 2, 3, 3, 3, 4,",
				"1, 1, 1, 1, 1, 1, 2,",
				"1, 2, 2, 3, 3, 3, 3, 4,",
				"1, 1, 2, 2, 2, 2, 2, 2, 3,",
				"1, 2, 2, 2, 3, 3, 3, 3, 3, 4",
				"1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2,",
				"1, 2, 3, 4, 4, 5, 5, 5, 5, 5, 5, 6,",
				"1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2,",
				"1, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 4,",
				"1, 1, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 4"
			],
			"maple": [
				"T:=(n,k)-\u003e1/n!*eval(diff(sum(x^j/(1-x^j),j=1..k),x$n),x=0):",
				"seq(seq(T(n,k), k=1..n), n=1..10);",
				"# Alternative:",
				"IversonBrackets := expr -\u003e subs(true=1, false=0, evalb(expr)):",
				"T := (n, k) -\u003e add(IversonBrackets(irem(n, j) = 0), j = 1..k):",
				"for n from 1 to 19 do seq(T(n, k), k = 1..n) od; # _Peter Luschny_, Jan 02 2021"
			],
			"program": [
				"(PARI) T(n, k) = sumdiv(n, d, d\u003c=k); \\\\ _Michel Marcus_, Jun 17 2014",
				"(Haskell)",
				"a243987 n k = a243987_tabl !! (n-1) !! (k-1)",
				"a243987_row n = a243987_tabl !! (n-1)",
				"a243987_tabl = map (scanl1 (+)) a051731_tabl",
				"-- _Reinhard Zumkeller_, Apr 22 2015"
			],
			"xref": [
				"Cf. A000005 (diagonal), A000012 (first column), A081307 (row sums), A027750 (divisors of n).",
				"Cf. A138553, A051731.",
				"Cf. A340260, A340261."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Dennis P. Walsh_, Jun 16 2014",
			"references": 5,
			"revision": 29,
			"time": "2021-01-02T17:13:09-05:00",
			"created": "2014-06-19T12:26:31-04:00"
		}
	]
}