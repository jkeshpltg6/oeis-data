{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A058055",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 58055,
			"data": "1,3,8,5,12,11,18,51,82,49,234,23,42,75,86,231,174,107,288,63,80,69,102,325,166,765,128,143,822,727,276,597,226,835,702,461,254,693,592,797,1284,349,370,2337,596,645,3012,1033,590,4083,1490,757,882,833,1668",
			"name": "a(n) is the smallest positive number m such that m^2 + n is the next prime \u003e m^2.",
			"comment": [
				"The primes are in A058056."
			],
			"link": [
				"T. D. Noe and Zak Seidov, \u003ca href=\"/A058055/b058055.txt\"\u003eTable of n, a(n) for n = 1..500\u003c/a\u003e (first 400 terms from T. D. Noe)"
			],
			"formula": [
				"a(n) = Min{ m \u003e 0 | m^2 + n is the next prime after m^2}.",
				"A053000(a(n)) = n. - _Zak Seidov_, Apr 12 2013"
			],
			"example": [
				"n=6: a(6)=11 and 11^2+6 is 127, a prime; n=97: a(97) = 2144 and 2144^2+97 = 4596833, the least prime of the form m^2+97."
			],
			"maple": [
				"for m from 1 to 10^5 do",
				"   r:= nextprime(m^2)-m^2;",
				"   if not assigned(R[r]) then R[r]:= m end if;",
				"end do:",
				"J:= map(op,{indices(R)}):",
				"N:= min({$1..J[-1]} minus J)-1:",
				"[seq(R[j],j=1..N)]; # _Robert Israel_, Aug 10 2012"
			],
			"mathematica": [
				"nn = 100; t = Table[0, {nn}]; found = 0; m = 0; While[found \u003c nn, m++; k = NextPrime[m^2] - m^2; If[k \u003c= nn \u0026\u0026 t[[k]] == 0, t[[k]] = m; found++]]; t (* _T. D. Noe_, Aug 10 2012 *)"
			],
			"program": [
				"(Sage)",
				"R = {}   # After Robert Israel's Maple script.",
				"for m in (1..2^12) :",
				"    r = next_prime(m^2) - m^2",
				"    if r not in R : R[r] = m",
				"L = sorted(R.keys())",
				"for i in (1..len(L)-1) :",
				"    if L[i] != L[i-1]+1 : break",
				"[R[k] for k in (1..i)]  # _Peter Luschny_, Aug 11, 2012"
			],
			"xref": [
				"Cf. A053000, A070316, A070317.",
				"See A085099, A215249 for other versions."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Labos Elemer_, Nov 20 2000",
			"ext": [
				"Definition corrected by _Zak Seidov_, Mar 03 2008, and again by _Franklin T. Adams-Watters_, Aug 10 2012"
			],
			"references": 5,
			"revision": 44,
			"time": "2020-03-06T12:27:51-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}