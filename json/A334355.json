{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A334355",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 334355,
			"data": "1,3,5,7,2,4,10,6,8,14,11,17,9,13,12,16,18,22,23,19,15,21,24,28,20,26,25,27,29,31,32,34,30,36,33,37,38,40,35,43,41,47,39,49,42,46,44,52,53,55,50,56,48,54,45,51,59,67,57,69,60,66,58,68,61,65,63,73,62,64,70,78,74,76,71,77,75,81,72,84,80",
			"name": "This viral sequence dies if any single term is increased by 1 (the virus). See how the contamination quickly spreads in the Comments section.",
			"comment": [
				"This is the lexicographically earliest sequence of positive distinct terms with the hereunder property:",
				"1) the sequence is stable as it is, but fragile;",
				"2) the stability is given by the fact that no pair of successive terms sums up to a prime;",
				"3) the fragility comes from the fact that the pair a(n)+1 and a(n+1), or the pair a(n)+1 and a(n-1) precisely sums up to a prime;",
				"4) should this happen, the said pair dies and infects its immediate neighbors L (the term to the left of the pair) and R (the term to the right);",
				"5) to infect a term means adding 1 (the virus) to it; if L is infected, L becomes L+1 and if R is infected, R becomes R+1;",
				"6) two infected neighbors L+1 and R+1 will be involved in a prime sum (with their immediate respective neighbor);",
				"7) the two new infected pairs will die and in turn infect their respective L and R neighbors;",
				"8) in the end, all terms die.",
				"The pairs of numbers in this sequence is forced to follow an interesting property that when 1 is added to each pair starting from a(1), the resulting number must alternate between being a prime and a nonprime. E.g., 1+3+1 = 5 (a prime), 3+5+1 = 9 (a nonprime), 5+7+1 = 13 (a prime), 7+2+1 = 10 (a nonprime).",
				"It is easy to show that if two or more pairs in succession form nonprimes when 1 is added to them then those pairs halt the spreading of the infection from either direction, thus the sequence cannot contain successive such pairs. However if we have two or more successive pairs that form primes when 1 is added to them then either (1) there is a pair that forms a nonprime when 1 is added at either the start or end of that series of pairs, or (2) the entire sequence is made from pairs such that adding 1 to any pair results in a prime.",
				"In the first case if such a nonprime pair is present then that halts the spread of the infection if the number two term to the left or right of this pair is incremented, depending on if this pair terminates or starts the series of prime-producing pairs. E.g., if the terms were ..., 5, 7, 9, 11, ..., where 5+7+1 = 13 (a prime), 7+9+1 = 17 (a prime), 9+11+1 = 21 (a nonprime), then incrementing the term 5, two terms to the left of 9, would result in the infection stopping at 9 and 11. We therefore can never have two or more successive pairs that produce a prime when 1 is added to them if anywhere in the sequence there is a pair such that adding 1 does not produce a prime.",
				"The second and only alternative is that the entire sequence is composed of pairs such that adding 1 produces a prime. However this would mean the sequence would be forced to start 1,3,7 since the present sequence 1,3,5 does not produce a prime with 3+5+1=9. However as we are looking for the earliest possible sequence which follows the infection rules then 1,3,5 comes before 1,3,7, and thus we must start with 1,3,5,... where 1+3+1=5 is a prime and 3+5+1=9 is a nonprime. This prime-nonprime starting pattern immediately locks the entire rest of the sequence into following the same pattern as we can never have two successive pairs that form either a prime or a nonprime."
			],
			"link": [
				"Scott R. Shannon, \u003ca href=\"/A334355/b334355.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"The sequence starts with 1,3,5,7,2,4,10,6,8,14,11,17",
				"Let's add 1 to a(7) = 10; a(7) becomes 11 and the sequence is now:",
				"1,3,5,7,2,4,11,6,8,14,11,17",
				"The pair (4,11) doesn't sum up to a prime and lives, but the pair (11,6) sums up to 17 (a prime) and dies (dots are dead digits):",
				"1,3,5,7,2,4,..,.,8,14,11,17",
				"In dying, the pair (11,6) adds 1 to its L and R neighbors:",
				"1,3,5,7,2,5,..,.,9,14,11,17",
				"The infection goes on, as two new dying pairs appear, (2,5) and (9,14) have prime sums and we get:",
				"1,3,5,7,.,.,..,.,.,..,11,17",
				"We add 1 to both lips of the wound:",
				"1,3,5,8,.,.,..,.,.,..,12,17",
				"And so on. The infection will stop to the left when it reaches a(1) but will infinitely proceed to the right."
			],
			"keyword": "nonn,base",
			"offset": "1,2",
			"author": "_Eric Angelini_ and _Scott R. Shannon_, Apr 24 2020",
			"references": 2,
			"revision": 37,
			"time": "2021-07-19T01:22:21-04:00",
			"created": "2020-04-29T20:24:12-04:00"
		}
	]
}