{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A180178",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 180178,
			"data": "1,1,1,0,2,1,1,1,3,1,1,2,3,4,1,1,4,4,6,5,1,1,4,9,8,10,6,1,1,5,12,17,15,15,7,1,1,6,15,28,30,26,21,8,1,1,7,21,38,56,51,42,28,9,1,1,8,27,56,85,102,84,64,36,10,1,1,9,34,80,130,172,175,134,93,45,11,1,1,10,42,108,200",
			"name": "Triangle read by rows: T(n,k) is the number of compositions of n without 3's and having k parts; 1 \u003c= k \u003c= n.",
			"reference": [
				"P. Chinn and S. Heubach, Compositions of n with no occurrence of k, Congressus Numerantium, 164 (2003), pp. 33-51 (see Table 5).",
				"R.P. Grimaldi, Compositions without the summand 1, Congressus Numerantium, 152, 2001, 33-43."
			],
			"link": [
				"P. Chinn and S. Heubach, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL6/Heubach/heubach5.html\"\u003eInteger Sequences Related to Compositions without 2's\u003c/a\u003e, J. Integer Seqs., Vol. 6, 2003."
			],
			"formula": [
				"Number of compositions of n without p's and having k parts = Sum_{j=(pk-n)/(p-1)..k} (-1)^(k-j)*binomial(k,j)*binomial(n-pk+pj-1, j-1).",
				"For a given p, the g.f. of the number of compositions without p's is G(t,z) = t*g(z)/(1-t*g(z)), where g(z) = z/(1-z) - z^p; here z marks sum of parts and t marks number of parts."
			],
			"example": [
				"T(7,4)=8 because we have (1,2,2,2), (2,1,2,2), (2,2,1,2), (2,2,2,1), (1,1,1,4), (1,1,4,1), (1,4,1,1), and (4,1,1,1).",
				"Triangle starts:",
				"  1;",
				"  1, 1;",
				"  0, 2, 1;",
				"  1, 1, 3, 1;",
				"  1, 2, 3, 4, 1;",
				"  1, 4, 4, 6, 5, 1;"
			],
			"maple": [
				"p := 3: T := proc (n, k) options operator, arrow: sum((-1)^(k-j)*binomial(k, j)*binomial(n-p*k+p*j-1, j-1), j = (p*k-n)/(p-1) .. k) end proc: for n to 13 do seq(T(n, k), k = 1 .. n) end do; # yields sequence in triangular form",
				"p := 3: g := z/(1-z)-z^p: G := t*g/(1-t*g): Gser := simplify(series(G, z = 0, 15)): for n to 13 do P[n] := sort(coeff(Gser, z, n)) end do: for n to 13 do seq(coeff(P[n], t, k), k = 1 .. n) end do; # yields sequence in triangular form",
				"with(combinat): m := 3: T := proc (n, k) local ct, i: ct := 0: for i to numbcomp(n, k) do if member(m, composition(n, k)[i]) = false then ct := ct+1 else end if end do: ct end proc: for n to 13 do seq(T(n, k), k = 1 .. n) end do; # yields sequence in triangular form"
			],
			"mathematica": [
				"p = 3; max = 14; g = z/(1-z) - z^p; G = t*g/(1-t*g); Gser = Series[G, {z, 0, max+1}]; t[n_, k_] := SeriesCoefficient[Gser, {z, 0, n}, {t, 0, k}]; Table[t[n, k], {n, 1, max}, {k, 1, n}] // Flatten (* _Jean-François Alcover_, Jan 28 2014, after Maple *)"
			],
			"xref": [
				"Cf. A011973, A180177, A180179, A180180, A180181, A180182, A180183."
			],
			"keyword": "nonn,tabl",
			"offset": "1,5",
			"author": "_Emeric Deutsch_, Aug 15 2010",
			"references": 6,
			"revision": 15,
			"time": "2019-11-15T21:30:10-05:00",
			"created": "2010-08-27T03:00:00-04:00"
		}
	]
}