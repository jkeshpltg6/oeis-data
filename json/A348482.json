{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A348482",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 348482,
			"data": "1,2,1,4,3,1,10,9,4,1,34,33,16,5,1,154,153,76,25,6,1,874,873,436,145,36,7,1,5914,5913,2956,985,246,49,8,1,46234,46233,23116,7705,1926,385,64,9,1,409114,409113,204556,68185,17046,3409,568,81,10,1",
			"name": "Triangle read by rows: T(n,k) = (Sum_{i=k..n} i!)/(k!) for 0 \u003c= k \u003c= n.",
			"comment": [
				"The matrix inverse M = T^(-1) has terms M(n,n) = 1 for n \u003e= 0, M(n,n-1) = -(n+1) for n \u003e 0, and M(n,n-2) = n for n \u003e 1, otherwise 0."
			],
			"formula": [
				"T(n,n) = 1 and T(2*n,n) = A109398(n) for n \u003e= 0; T(n,n-1) = n+1 for n \u003e 0; T(n,n-2) = n^2 for n \u003e 1.",
				"T(n,k) - T(n-1,k) = (n!) / (k!) = A094587(n,k) for 0 \u003c= k \u003c n.",
				"T(n,k) = (k+2) * (T(n,k+1) - T(n,k+2)) for 0 \u003c= k \u003c n-1.",
				"T(n,k) = (T(n,k-1) - 1) / k for 0 \u003c k \u003c= n.",
				"T(n,k) * T(n-1,k-1) - T(n-1,k) * T(n,k-1) = (n!) / (k!) for 0 \u003c k \u003c n.",
				"T(n,1) = T(n,0)-1 = Sum_{k=0..n-1} T(n,k)/(k+2) for n \u003e 0 (conjectured).",
				"Sum_{k=0..n} binomial(k+r,k) * (1-k) * T(n+r,k+r) = binomial(n+r+1,n) for n \u003e= 0 and r \u003e= 0.",
				"Sum_{k=0..n} (-1)^k * (k+1) * T(n,k) = (1 + (-1)^n) / 2 for n \u003e= 0.",
				"Sum_{k=0..n} (-1)^k * (k!) * T(n,k) = Sum_{k=0..n} (k!) * (1+(-1)^k) / 2 for n \u003e= 0.",
				"The row polynomials p(n,x) = Sum_{k=0..n} T(n,k) * x^k for n \u003e= 0 satisfy the following equations:",
				"  (a) p(n,x) - p'(n,x) = (x^(n+1)-1) / (x-1) for n \u003e= 0, where p' is the first derivative of p;",
				"  (b) p(n,x) - (n+1) * p(n-1,x) + n * p(n-2,x) = x^n for n \u003e 1.",
				"  (c) p(n,x) = (x+1) * p(n-1,x) + 1 + Sum_{i=1..n-1} (d/dx)^i p(n-1,x) for n \u003e 0 (conjectured).",
				"Row sums p(n,1) equal A002104(n+1) for n \u003e= 0.",
				"Alternating row sums p(n,-1) equal A173184(n) for n \u003e= 0 (conjectured)."
			],
			"example": [
				"The triangle T(n,k) for 0 \u003c= k \u003c= n starts:",
				"n\\k :       0       1       2      3      4     5    6   7   8  9",
				"=================================================================",
				"  0 :       1",
				"  1 :       2       1",
				"  2 :       4       3       1",
				"  3 :      10       9       4      1",
				"  4 :      34      33      16      5      1",
				"  5 :     154     153      76     25      6     1",
				"  6 :     874     873     436    145     36     7    1",
				"  7 :    5914    5913    2956    985    246    49    8   1",
				"  8 :   46234   46233   23116   7705   1926   385   64   9   1",
				"  9 :  409114  409113  204556  68185  17046  3409  568  81  10  1",
				"  etc."
			],
			"mathematica": [
				"T[n_, k_] := Sum[i!, {i, k, n}]/k!; Table[T[n, k], {n, 0, 9}, {k, 0, n}] // Flatten (* _Amiram Eldar_, Oct 20 2021 *)"
			],
			"xref": [
				"Cf. A109398, A094587, A002104 (row sums), A173184 (alt. row sums), A000012 (main diagonal), A000027(1st subdiagonal), A000290 (2nd subdiagonal), A081437 (3rd subdiagonal), A192398 (4th subdiagonal), A003422 (column 0), A007489 (column 1), A345889 (column 2)."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "0,2",
			"author": "_Werner Schulte_, Oct 20 2021",
			"references": 0,
			"revision": 7,
			"time": "2021-10-25T05:08:16-04:00",
			"created": "2021-10-25T05:08:16-04:00"
		}
	]
}