{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A191671",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 191671,
			"data": "1,2,5,3,7,9,4,10,12,13,6,14,16,18,17,8,19,22,24,23,21,11,26,30,32,31,28,25,15,35,40,43,42,38,34,29,20,47,54,58,56,51,46,39,33,27,63,72,78,75,68,62,52,44,37,36,84,96,104,100,91,83,70,59,50,41",
			"name": "Dispersion of A004772 (\u003e1 and congruent to 0 or 2 or 3 mod 4), by antidiagonals.",
			"comment": [
				"For a background discussion of dispersions, see A191426.",
				"...",
				"Each of the sequences (4n, n\u003e2), (4n+1, n\u003e0), (3n+2, n\u003e=0), generates a dispersion. Each complement (beginning with its first term \u003e1) also generates a dispersion. The six sequences and dispersions are listed here:",
				"...",
				"A191452=dispersion of A008586 (4k, k\u003e=1)",
				"A191667=dispersion of A016813 (4k+1, k\u003e=1)",
				"A191668=dispersion of A016825 (4k+2, k\u003e=0)",
				"A191669=dispersion of A004767 (4k+3, k\u003e=0)",
				"A191670=dispersion of A042968 (1 or 2 or 3 mod 4 and \u003e=2)",
				"A191671=dispersion of A004772 (0 or 1 or 3 mod 4 and \u003e=2)",
				"A191672=dispersion of A004773 (0 or 1 or 2 mod 4 and \u003e=2)",
				"A191673=dispersion of A004773 (0 or 2 or 3 mod 4 and \u003e=2)",
				"...",
				"EXCEPT for at most 2 initial terms (so that column 1 always starts with 1):",
				"A191452 has 1st col A042968, all else A008486",
				"A191667 has 1st col A004772, all else A016813",
				"A191668 has 1st col A042965, all else A016825",
				"A191669 has 1st col A004773, all else A004767",
				"A191670 has 1st col A008486, all else A042968",
				"A191671 has 1st col A016813, all else A004772",
				"A191672 has 1st col A016825, all else A042965",
				"A191673 has 1st col A004767, all else A004773",
				"...",
				"Regarding the dispersions A191670-A191673, there is a formula for sequences of the type \"(a or b or c mod m)\", (as in the Mathematica program below):",
				"   If f(n)=(n mod 3), then (a,b,c,a,b,c,a,b,c,...) is given by",
				"   a*f(n+2)+b*f(n+1)+c*f(n), so that \"(a or b or c mod m)\" is given by",
				"   a*f(n+2)+b*f(n+1)+c*f(n)+m*floor((n-1)/3)), for n\u003e=1."
			],
			"link": [
				"Ivan Neretin, \u003ca href=\"/A191671/b191671.txt\"\u003eTable of n, a(n) for n = 1..5050\u003c/a\u003e (first 100 antidiagonals, flattened)"
			],
			"example": [
				"Northwest corner:",
				"1....2....3....4....6",
				"5....7....10...14...19",
				"9....12...16...22...30",
				"13...18...24...32...43",
				"17...23...31...42...56"
			],
			"mathematica": [
				"(* Program generates the dispersion array T of the increasing sequence f[n] *)",
				"r = 40; r1 = 12;  c = 40; c1 = 12;",
				"a = 2; b = 3; c2 = 4; m[n_] := If[Mod[n, 3] == 0, 1, 0];",
				"f[n_] := a*m[n + 2] + b*m[n + 1] + c2*m[n] + 4*Floor[(n - 1)/3]",
				"Table[f[n], {n, 1, 30}]  (* A004772 *)",
				"mex[list_] := NestWhile[#1 + 1 \u0026, 1, Union[list][[#1]] \u003c= #1 \u0026, 1, Length[Union[list]]]",
				"rows = {NestList[f, 1, c]};",
				"Do[rows = Append[rows, NestList[f, mex[Flatten[rows]], r]], {r}];",
				"t[i_, j_] := rows[[i, j]];",
				"TableForm[Table[t[i, j], {i, 1, 10}, {j, 1, 10}]] (* A191671 *)",
				"Flatten[Table[t[k, n - k + 1], {n, 1, c1}, {k, 1, n}]] (* A191671  *)"
			],
			"xref": [
				"Cf. A016813, A004772, A191667, A191452."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Jun 11 2011",
			"references": 7,
			"revision": 11,
			"time": "2017-10-18T05:07:00-04:00",
			"created": "2011-06-11T17:38:43-04:00"
		}
	]
}