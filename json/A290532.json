{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A290532",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 290532,
			"data": "1,1,2,1,2,1,2,3,1,2,1,2,2,4,1,2,1,2,3,4,1,2,3,1,2,2,4,1,2,1,2,2,3,4,6,1,2,1,2,2,4,1,2,2,4,1,2,3,4,5,1,2,1,2,2,4,3,6,1,2,1,2,3,2,4,6,1,2,2,4,1,2,2,4,1,2,1,2,2,3,4,4,6,8,1,2,3",
			"name": "Irregular triangle read by rows in which row n lists the number of divisors of each divisor of n.",
			"comment": [
				"Or, in the triangle A027750, replace each element with the number of its divisors.",
				"The row of index n = p^m (p prime and m \u003e= 0) is equal to (1, 2, ..., m + 1);",
				"We observe an interesting property when the index n of the row n is the product of k distinct primes, k = 1,2,... For example:",
				"The index n is prime =\u003e row n = (1, 2);",
				"The index n equals the product of two distinct primes =\u003e row n = (1, 2, 2, 4);",
				"The index n equals the product of three distinct primes =\u003e row n = (1, 2, 2, 2, 4, 4, 4, 8) or a permutation of the elements;",
				"...",
				"Let us now consider Pascal's triangle (A007318(n) for n \u003e 0):",
				"1, 1;",
				"1, 2, 1;",
				"1, 3, 3, 1;",
				"1, 4, 6, 4, 1;",
				"...",
				"Row 1 of Pascal's triangle gives the number of \"1\" and the number of \"2\" respectively belonging to the row of index n = prime(m) of the sequence;",
				"Row 2 of Pascal's triangle gives the number of \"1\", the number of \"2\" and the number of \"4\" respectively belonging to the row of index n = p*q of the sequence, where p and q are distinct primes;",
				"Row 3 of Pascal's triangle gives the number of \"1\", the number of \"2\", the number of \"4\" and the number of \"8\" respectively belonging to the row of index n = p*q*r of the sequence, where p, q and r are distinct primes;",
				"...",
				"It is now easy to generalize this process by the following proposition.",
				"Proposition: binomial(m,k) is the number of terms of the form 2^k belonging to the row of index n in the sequence when n is the product of m distinct primes."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A290532/b290532.txt\"\u003eTable of n, a(n) for n = 1..10006\u003c/a\u003e (rows 1 to 1358, flattened)"
			],
			"formula": [
				"T(n, k) = tau(A027750(n, k))."
			],
			"example": [
				"Row 6 is (1, 2, 2, 4) because the 6th row of A027750 is [1, 2, 3, 6] and tau(1) = 1, tau(2) = 2, tau(3) = 2 and tau(6) = 4.",
				"Triangle begins:",
				"  1;",
				"  1, 2;",
				"  1, 2;",
				"  1, 2, 3;",
				"  1, 2;",
				"  1, 2, 2, 4;",
				"  1, 2;",
				"  1, 2, 3, 4;",
				"  1, 2, 3;",
				"  1, 2, 2, 4;",
				"  ..."
			],
			"maple": [
				"with(numtheory):nn:=100:",
				"for n from 1 to nn do:",
				"  d1:=divisors(n):n1:=nops(d1):",
				"   for i from 1 to n1 do:",
				"     n2:=tau(d1[i]):",
				"     printf(`%d, `,n2):",
				"   od:",
				"od:"
			],
			"mathematica": [
				"Table[DivisorSigma[0, Divisors@ n], {n, 25}] // Flatten (* _Michael De Vlieger_, Aug 07 2017 *)"
			],
			"program": [
				"(PARI) row(n) = apply(numdiv, divisors(n)); \\\\ _Michel Marcus_, Dec 27 2021"
			],
			"xref": [
				"Cf. A000005, A007318, A027750, A084997, A290478."
			],
			"keyword": "nonn,tabf",
			"offset": "1,3",
			"author": "_Michel Lagneau_, Aug 05 2017",
			"references": 2,
			"revision": 43,
			"time": "2021-12-28T04:18:02-05:00",
			"created": "2017-10-01T09:21:30-04:00"
		}
	]
}