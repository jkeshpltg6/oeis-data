{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A072704",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 72704,
			"data": "1,1,1,1,2,1,1,3,3,1,1,4,5,4,1,1,5,8,7,5,1,1,6,12,12,9,6,1,1,7,16,20,16,11,7,1,1,8,21,30,28,20,13,8,1,1,9,27,42,45,36,24,15,9,1,1,10,33,58,68,60,44,28,17,10,1,1,11,40,77,98,95,75,52,32,19,11,1",
			"name": "Triangle of number of weakly unimodal partitions/compositions of n into exactly k terms.",
			"reference": [
				"Rigoberto Flórez, Leandro Junes, José L. Ramírez, Enumerating several aspects of non-decreasing Dyck paths, Discrete Mathematics (2019) Vol. 342, Issue 11, 3079-3097. See page 3094, Table 4."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A072704/b072704.txt\"\u003eRows n = 1..141, flattened\u003c/a\u003e",
				"H. Bottomley, \u003ca href=\"/A001523/a001523.gif\"\u003eIllustration of initial terms\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/UnimodalSequence.html\"\u003eUnimodal Sequence\u003c/a\u003e"
			],
			"formula": [
				"G.f. with initial column 1, 0, 0, ...: 1 + Sum_{n\u003e=1} (t*x^n / ( ( Product_{k=1..n-1} (1 - t*x^k)^2 ) * (1 - t*x^n) ) ). - _Joerg Arndt_, Oct 01 2017"
			],
			"example": [
				"Rows start:",
				"01:  [1]",
				"02:  [1, 1]",
				"03:  [1, 2, 1]",
				"04:  [1, 3, 3, 1]",
				"05:  [1, 4, 5, 4, 1]",
				"06:  [1, 5, 8, 7, 5, 1]",
				"07:  [1, 6, 12, 12, 9, 6, 1]",
				"08:  [1, 7, 16, 20, 16, 11, 7, 1]",
				"09:  [1, 8, 21, 30, 28, 20, 13, 8, 1]",
				"10:  [1, 9, 27, 42, 45, 36, 24, 15, 9, 1]",
				"...",
				"T(6,3)=8 since 6 can be written as 1+1+4, 1+2+3, 1+3+2, 1+4+1, 2+2+2, 2+3+1, 3+2+1, or 4+1+1 but not 2+1+3 or 3+1+2."
			],
			"maple": [
				"b:= proc(n, i) option remember; local q; `if`(i\u003en, 0,",
				"      `if`(irem(n, i, 'q')=0, x^q, 0) +expand(",
				"      add(b(n-i*j, i+1)*(j+1)*x^j, j=0..n/i)))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=1..n))(b(n, 1)):",
				"seq(T(n), n=1..12);  # _Alois P. Heinz_, Mar 26 2014"
			],
			"mathematica": [
				"b[n_, i_] := b[n, i] = If[i\u003en, 0, If[Mod[n, i ] == 0, x^Quotient[n, i], 0] + Expand[ Sum[b[n-i*j, i+1]*(j+1)*x^j, {j, 0, n/i}]]]; T[n_] := Function[{p}, Table[Coefficient[p, x, i], {i, 1, n}]][b[n, 1]]; Table[T[n], {n, 1, 12}] // Flatten (* _Jean-François Alcover_, Feb 26 2015, after _Alois P. Heinz_ *)",
				"unimodQ[q_]:=Or[Length[q]\u003c=1,If[q[[1]]\u003c=q[[2]],unimodQ[Rest[q]],OrderedQ[Reverse[q]]]];",
				"Table[Length[Select[Join@@Permutations/@IntegerPartitions[n,{k}],unimodQ]],{n,0,10},{k,0,n}] (* _Gus Wiseman_, Mar 06 2020 *)"
			],
			"program": [
				"(PARI) \\\\ starting for n=0, with initial column 1, 0, 0, ...:",
				"N=25;  x='x+O('x^N);",
				"T=Vec(1 + sum(n=1, N, t*x^n / ( prod(k=1,n-1, (1 - t*x^k)^2 ) * (1 - t*x^n) ) ) )",
				"for(r=1,#T, print(Vecrev(T[r])) ); \\\\ _Joerg Arndt_, Oct 01 2017"
			],
			"xref": [
				"Cf. A059623, A072705. Row sums are A001523. First column is A057427, second is A000027 offset, third appears to be A000212 offset, right hand columns include A000012, A000027, A005408 and A008574.",
				"The case of partitions is A072233.",
				"Dominates A332670 (the version for negated compositions).",
				"The strict case is A072705.",
				"The case of constant compositions is A113704.",
				"Unimodal sequences covering an initial interval are A007052.",
				"Partitions whose run-lengths are unimodal are A332280.",
				"Cf. A107429, A115981, A227038, A328509, A332282, A332283, A332578, A332638, A332669, A332728."
			],
			"keyword": "nonn,tabl",
			"offset": "1,5",
			"author": "_Henry Bottomley_, Jul 04 2002",
			"references": 23,
			"revision": 25,
			"time": "2020-03-06T22:39:28-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}