{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A334056",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 334056,
			"data": "1,0,1,7,2,1,219,53,7,1,12861,2296,226,16,1,1215794,171785,13080,710,30,1,169509845,19796274,1228655,53740,1835,50,1,32774737463,3260279603,170725639,6250755,178325,4137,77,1,8400108766161,727564783392,32944247308,1036855344,25359670,507584,8428,112,1",
			"name": "Triangle read by rows: T(n,k) is the number of configurations with exactly k polyomino matchings in a generalized game of memory played on the path of length 3n.",
			"comment": [
				"In this generalized game of memory n indistinguishable triples of matched cards are placed on the vertices of the path of length 3n. A polyomino is a triple on three adjacent vertices. For dominoes in ordinary memory on the path of length 2n, see A079267.",
				"T(n,k) is the number of set partitions of {1..3n} into n sets of 3 with k of the sets being a contiguous set of elements. - _Andrew Howroyd_, Apr 16 2020"
			],
			"link": [
				"Donovan Young, \u003ca href=\"https://arxiv.org/abs/2004.06921\"\u003ePolyomino matchings in generalised games of memory and linear k-chord diagrams\u003c/a\u003e, arXiv:2004.06921 [math.CO], 2020. See also \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL23/Young/young5.html\"\u003eJ. Int. Seq.\u003c/a\u003e, Vol. 23 (2020), Article 20.9.1."
			],
			"formula": [
				"G.f.: Sum_{j\u003e=0} (3*j)! * y^j / (j! * 6^j * (1+(1-z)*y)^(3*j+1)).",
				"T(n,k) = Sum_{j=0..n-k} (-1)^(n-j-k)*(n+2*j)!/(6^j*j!*(n-j-k)!*k!). - _Andrew Howroyd_, Apr 16 2020"
			],
			"example": [
				"The first few rows of T(n,k) are:",
				"      1;",
				"      0,    1;",
				"      7,    2,   1;",
				"    219,   53,   7,  1;",
				"  12861, 2296, 226, 16, 1;",
				"  ...",
				"For n=2 and k=1 the polyomino must start either on the second vertex of the path, or the third, otherwise the remaining triple will also form a polyomino; thus T(2,1) = 2."
			],
			"mathematica": [
				"CoefficientList[Normal[Series[Sum[y^j*(3*j)!/6^j/j!/(1+y*(1-z))^(3*j+1),{j,0,20}],{y,0,20}]],{y,z}]"
			],
			"program": [
				"(PARI) T(n,k)={sum(j=0, n-k, (-1)^(n-j-k)*(n+2*j)!/(6^j*j!*(n-j-k)!*k!))} \\\\ _Andrew Howroyd_, Apr 16 2020"
			],
			"xref": [
				"Row sums are A025035.",
				"Cf. A079267, A334057, A334058, A325753."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_Donovan Young_, Apr 15 2020",
			"references": 5,
			"revision": 34,
			"time": "2021-01-29T20:46:38-05:00",
			"created": "2020-05-18T14:47:50-04:00"
		}
	]
}