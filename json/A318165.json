{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A318165",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 318165,
			"data": "1,3,13",
			"name": "The n^n dots problem (n times n): minimal number of straight lines (connected at their endpoints) required to pass through n^n dots arranged in an n X n X ... X n grid.",
			"comment": [
				"A generalization of the well-known \"Nine Dots Problem\".",
				"For any n \u003e 3, an upper bound for this problem is given by U(n) := (t + 1)*n^(n - 3) - 1, where \"t\" is the best known solution for the corresponding n X n X n case, and (for any n \u003e 5) t = floor((3/2)*n^2) - floor((n - 1)/4) + floor((n + 1)/4) - floor((n - 2)/4) + floor(n/2) + n - 2 (e.g., U(4) = 95, since 23 is the current upper bound for the 4 X 4 X 4 problem). In particular, it is easily possible to prove the existence of an Hamiltonian path without self crossing such that U(4) = 95 (in fact, an Hamiltonian path with link-length 23 for the 4 X 4 X 4 problem was explicitly shown in June 2020).",
				"A (trivial) lower bound is given by B(n):= (n^n - 1)/(n - 1) - _Marco Ripà_, Aug 25 2020"
			],
			"link": [
				"Marco Ripà, \u003ca href=\"/A225227/a225227.pdf\"\u003eThe Rectangular Spiral Solution for the n1 X n2 X ... X nk Points Problem\u003c/a\u003e",
				"Marco Ripà, \u003ca href=\"http://nntdm.net/volume-20-2014/number-1/59-71/\"\u003eThe rectangular spiral or the n1 X n2 X ... X nk Points Problem\u003c/a\u003e, Notes on Number Theory and Discrete Mathematics, 2014, 20(1), 59-71.",
				"Marco Ripà, \u003ca href=\"https://www.researchgate.net/publication/343050221_Solving_the_106_years_old_3k_Points_Problem_with_the_Clockwise-algorithm\"\u003eSolving the 106 years old 3^k Points Problem with the Clockwise-algorithm\u003c/a\u003e, ResearchGate, 2020 (DOI: 10.13140/RG.2.2.34972.92802).",
				"Marco Ripà, \u003ca href=\"https://www.researchgate.net/publication/342331014_Solving_the_n_1_n_2_n_3_Points_Problem_for_n_3_6\"\u003eSolving the n_1 \u003c= n_2 \u003c= n_3 Points Problem for n_3 \u003c 6\u003c/a\u003e, ResearchGate, 2020 (DOI: 10.13140/RG.2.2.12199.57769/1).",
				"Marco Ripà, \u003ca href=\"https://ejournal2.undip.ac.id/index.php/jfma/article/view/12053\"\u003eGeneral uncrossing covering paths inside the Axis-Aligned Bounding Box\u003c/a\u003e, Journal of Fundamental Mathematics and Applications, 2021, 4(2), 154-166.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Thinking_outside_the_box#Nine_dots_puzzle\"\u003eNine dots puzzle\u003c/a\u003e"
			],
			"example": [
				"For n = 3, a(3) = 13. You cannot touch (the centers of) the 3 X 3 X 3 dots using fewer than 13 straight lines, following the \"Nine Dots Puzzle\" basic rules."
			],
			"xref": [
				"Cf. A058992, A225227, A261547."
			],
			"keyword": "nonn,more,hard,bref",
			"offset": "1,2",
			"author": "_Marco Ripà_, Aug 20 2018",
			"ext": [
				"a(3) corrected by _Marco Ripà_, Aug 25 2020"
			],
			"references": 2,
			"revision": 29,
			"time": "2021-12-18T13:03:52-05:00",
			"created": "2018-09-23T23:12:41-04:00"
		}
	]
}