{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A094791",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 94791,
			"data": "1,1,0,1,1,1,1,3,5,2,1,6,17,20,9,1,10,45,100,109,44,1,15,100,355,694,689,265,1,21,196,1015,3094,5453,5053,1854,1,28,350,2492,10899,29596,48082,42048,14833,1,36,582,5460,32403,124908,309602,470328,391641,133496",
			"name": "Triangle read by rows giving coefficients of polynomials arising in successive differences of (n!)_{n\u003e=0}.",
			"comment": [
				"Let D_0(n)=n! and D_{k+1}(n)=D_{k}(n+1)-D_{k}(n), then D_{k}(n)=n!*P_{k}(n) where P_{k} is a polynomial with integer coefficients of degree k.",
				"The horizontal reversal of this triangle arises as a binomial convolution of the derangements coefficients der(n,i) (numbers of permutations of size n with i derangements = A098825(n,i) = number of permutations of size n with n-i rencontres = A008290(n,n-i), see formula section). - _Olivier Gérard_, Jul 31 2011"
			],
			"formula": [
				"T(n, n) = A000166(n).",
				"T(2, k) = A000217(k).",
				"Sum_{k=0..n} T(n,n-k)*x^k = Sum_{i=0..n} der(n,i)*binomial( n+x, i) (an analog of Worpitzky's identity). - _Olivier Gérard_, Jul 31 2011",
				"The n-th row polynomial R(n,x) = Sum _{k = 0..n} T(n,k)*x^k is P-recursive in the variable x: x*R(n,x) = (x+n+1)*R(n,x-1) - R(n,x-2). - _Peter Bala_, Jul 25 2021"
			],
			"example": [
				"D_3(n) = n!*(n^3 + 3*n^2 + 5*n + 2).",
				"D_4(n) = n!*(n^4 + 6*n^3 + 17*n^2 + 20*n + 9).",
				"Table begins:",
				"  1",
				"  1  0",
				"  1  1   1",
				"  1  3   5   2",
				"  1  6  17  20    9",
				"  1 10  45 100  109   44",
				"  1 15 100 355  694  689  265",
				"  ..."
			],
			"maple": [
				"with(LREtools): A094791_row := proc(n)",
				"delta(x!,x,n); simplify(%/x!); seq(coeff(%,x,n-j),j=0..n) end:",
				"seq(print(A094791_row(n)),n=0..9); # _Peter Luschny_, Jan 09 2015"
			],
			"mathematica": [
				"d[0][n_] := n!; d[k_][n_] := d[k][n] = d[k - 1][n + 1] - d[k - 1][n] // FullSimplify;",
				"row[k_] := d[k][n]/n! // FullSimplify // CoefficientList[#, n]\u0026 // Reverse;",
				"Array[row, 10, 0] // Flatten (* _Jean-François Alcover_, Aug 02 2019 *)"
			],
			"xref": [
				"Successive differences of factorial numbers: A001563, A001564, A001565, A001688, A001689, A023043.",
				"Rencontres numbers A008290. Partial derangements A098825.",
				"Row sum is A000255. Signed version in A126353.",
				"Cf. A094792, A094793, A094794, A094795."
			],
			"keyword": "nonn,tabl",
			"offset": "0,8",
			"author": "_Benoit Cloitre_, Jun 11 2004",
			"ext": [
				"Edited and T(0,0) corrected according to the author's definition by _Olivier Gérard_, Jul 31 2011"
			],
			"references": 5,
			"revision": 33,
			"time": "2021-08-14T15:33:34-04:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}