{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A336011",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 336011,
			"data": "1,1,7,2,3,8,4,9,1,9,9,6,2,1,1,9,7,1,6,5,7,2,2,3,8,9,6,0,7,0,4,6,3,2,0,2,0,2,2,4,8,0,8,9,1,1,8,6,1,1,1,9,7,7,6,8,0,5,3,2,7,5,8,0,2,9,7,7,2,4,4,0,2,0,6,8,8,1,7,6,8,6,7,8,4,0,9,9,2,9,5,3,1,2,5,2,5,7,8,1,2,8,4,1,4,7,6",
			"name": "Given the two curves y = (1 - exp(x/2))/(exp(x) + exp(x/2)) and y = (exp(-x) - 1)/2, draw a line tangent to both. This sequence is the decimal expansion of the y-coordinate (negated) of the point at which the line touches y = (1 - exp(x/2))/(exp(x) + exp(x/2)).",
			"comment": [
				"This constant is involved in the calculation of Gauchman's constant -A243261 (which equals A086278 - 1).",
				"Gauchman's constant is the point where the common tangent to the two curves y = (1 - exp(x/2))/(exp(x) + exp(x/2)) and y = (exp(-x) - 1)/2 intersects the y-axis."
			],
			"reference": [
				"Steven R. Finch, Mathematical Constants, Cambridge University Press, 2003, Section 3.1, Shapiro-Drinfeld constant, p. 210.",
				"Hillel Gauchman, Solution to Problem 10528(b), unpublished note, 1998."
			],
			"link": [
				"Vasile Cârtoaje, Jeremy Dawson and Hans Volkmer, \u003ca href=\"https://www.jstor.org/stable/3109827\"\u003eSolution to Problem 10528(a,b)\u003c/a\u003e, American Mathematical Monthly, 105 (1998), 473-474. [A comment was made about Hillel Gauchman's solution to part (b) of the problem that involves this constant, but no solution was published.]",
				"Petros Hadjicostas, \u003ca href=\"/A243261/a243261.pdf\"\u003ePlot of the curves y = (1 - exp(x/2))/(exp(x) + exp(x/2)) and y = (exp(-x) - 1)/2 and their common tangent\u003c/a\u003e, 2020."
			],
			"formula": [
				"We solve the following system of equations:",
				"exp(-c) = (exp(b/2) + 2*exp(b) - exp(3*b/2))/(exp(b) + exp(b/2))^2 and",
				"2*(1 - exp(b/2)) = (exp(b) + exp(b/2))*(exp(-c)*(1 + c - b) - 1).",
				"Then the constant equals (1 - exp(b/2))/(exp(b) + exp(b/2)).",
				"It turns out that b = -A335810 = -0.387552... and c = A335809 = 0.330604... even though A335810 and A335809 are also involved in the calculation of the Shapiro cyclic sum constant mu (A086278).",
				"As a result, the constant also equals A335825 - 1."
			],
			"example": [
				"0.11723849199621197165722389607046320202248089118..."
			],
			"program": [
				"(PARI) default(\"realprecision\", 200)",
				"c(b) = -log((exp(b/2) + 2*exp(b) - exp(3*b/2))/(exp(b) + exp(b/2))^2);",
				"a = solve(b=-2, 0, (exp(b) + exp(b/2))*(-1 + exp(-c(b))*(1 + c(b) - b)) - 2*(1 - exp(b/2)));",
				"(1 - exp(a/2))/(exp(a) + exp(a/2))"
			],
			"xref": [
				"Cf. A086278, A243261, A335809 (c), A335810 (-b), A335825 (1 plus the constant), A336029 (y-coordinate for c)."
			],
			"keyword": "nonn,cons",
			"offset": "0,3",
			"author": "_Petros Hadjicostas_, Jul 05 2020",
			"references": 1,
			"revision": 47,
			"time": "2020-07-13T10:08:02-04:00",
			"created": "2020-07-13T02:20:01-04:00"
		}
	]
}