{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A158112",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 158112,
			"data": "1,-2,2,0,260,-72384,40618368,-64586112000,322969883412000,-5357637135507147264,305435344239722874022912,-61394251001333873555321724928,44347756583930343994689166159720448",
			"name": "a(n) = [x^n] eta(x)^(2^n).",
			"comment": [
				"Here eta(q) is the q-expansion of the Dedekind eta function without the q^(1/24) factor (A010815)."
			],
			"formula": [
				"G.f.: A(x) = Sum_{n\u003e=0} log( eta(2^n*x) )^n/n!.",
				"G.f.: A(x) = Sum_{n\u003e=0} [ -Sum_{k\u003e=1} ( (2^n*x)^k/(1 - (2^n*x)^k) )/k ]^n/n!.",
				"a(n) = [x^n] Product_{k\u003e=1} (1-x^k)^(2^n)."
			],
			"example": [
				"G.f.: A(x) = 1 - 2*x + 2*x^2 + 260*x^4 - 72384*x^5 +...",
				"A(x) = 1 + log(eta(2*x)) + log(eta(4*x))^2/2! + log(eta(8*x))^3/3! +...",
				"...",
				"Given eta(x) = 1 - x - x^2 + x^5 + x^7 - x^12 - x^15 + x^22 +...",
				"then a(n) is the coefficient of x^n in eta(x)^(2^n):",
				"eta(x)^(2^0): [(1),-1,-1,0,0,1,0,1,0,0,0,0,-1,0,0,-1,0,0,0,0,..];",
				"eta(x)^(2^1): [1,(-2),-1,2,1,2,-2,0,-2,-2,1,0,0,2,3,-2,2,...];",
				"eta(x)^(2^2): [1,-4,(2),8,-5,-4,-10,8,9,0,14,-16,-10,-4,0,-8,...];",
				"eta(x)^(2^3): [1,-8,20,(0),-70,64,56,0,-125,-160,308,0,110,0,...];",
				"eta(x)^(2^4): [1,-16,104,-320,(260),1248,-3712,1664,6890,...];",
				"eta(x)^(2^5): [1,-32,464,-3968,21576,(-72384),109120,215296,...];",
				"eta(x)^(2^6): [1,-64,1952,-37632,512400,-5207936,(40618368),...]; ...",
				"where terms in parenthesis form the initial terms of this sequence."
			],
			"mathematica": [
				"a[n_] := SeriesCoefficient[QPochhammer[q]^(2^n), {q, 0, n}]; Table[a[n], {n, 0, 20}] (* _Jean-François Alcover_, Nov 24 2015 *)"
			],
			"program": [
				"(PARI) {a(n)=polcoeff(eta(x+x*O(x^n))^(2^n), n)}",
				"(PARI) {a(n)=polcoeff(sum(m=0,n,log(eta(2^m*x+x*O(x^n)))^m/m!), n)}",
				"(PARI) {a(n)=polcoeff(sum(m=0,n,sum(k=1,n,-(2^m*x)^k/(1-(2^m*x)^k)/k+x*O(x^n))^m/m!),n)}"
			],
			"xref": [
				"Cf. A010815, A158113, A158114, A158115, A158102, A158103, A158104, A158105."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Paul D. Hanna_, Mar 12 2009",
			"references": 8,
			"revision": 5,
			"time": "2015-11-24T03:27:38-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}