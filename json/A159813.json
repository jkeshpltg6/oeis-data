{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A159813",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 159813,
			"data": "1,-1,0,-1,0,0,1,1,-1,0,0,0,0,1,-2,1,0,3,0,0,-2,-2,0,0,-1,0,0,-1,0,2,0,-1,0,0,0,1,4,0,2,0,0,-2,0,0,0,-4,0,0,1,-1,0,0,0,0,0,-1,2,-2,0,2,0,0,-1,-1,-2,0,0,0,0,4,2,-3,0,2,0,0,2,2,-2,0",
			"name": "Expansion of eta(q) * eta(q^4) * eta(q^14)^4 / (eta(q^2) * eta(q^7) * eta(q^28)) in powers of q.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Unique cusp form of weight 3/2, level 28 and trivial character."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A159813/b159813.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q * psi(-q) * psi(-q^7) * phi(q^7) in powers of q where phi(), psi() are Ramanujan theta functions. - _Michael Somos_, Aug 15 2012",
				"Euler transform of period 28 sequence [ -1, 0, -1, -1, -1, 0, 0, -1, -1, 0, -1, -1, -1, -3, -1, -1, -1, 0, -1, -1, 0, 0, -1, -1, -1, 0, -1, -3, ...]. - _Michael Somos_, Aug 15 2012",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (28 t)) = 56^(1/2) (t/i)^(3/2) g(t) where q = exp(2 Pi i t) and g() is the g.f. for A215556. - _Michael Somos_, Aug 15 2012",
				"a(7*n + 3) = a(7*n + 5) = a(7*n + 6) = 0. a(7*n) = A215556(n). - _Michael Somos_, Aug 15 2012"
			],
			"example": [
				"G.f. = q - q^2 - q^4 + q^7 + q^8 - q^9 + q^14 - 2*q^15 + q^16 + 3*q^18 - 2*q^21 + ..."
			],
			"mathematica": [
				"max = 100; s28 = Table[{-1, 0, -1, -1, -1, 0, 0, -1, -1, 0, -1, -1, -1, -3, -1, -1, -1, 0, -1, -1, 0, 0, -1, -1, -1, 0, -1, -3}, {max/28 // Ceiling}] // Flatten; coes = Series[ 1 + Sum[a[n]*x^n, {n, 1, max}] - Product[1/(1 - x^n)^s28[[n]], {n, 1, max}], {x, 0, max}] // CoefficientList[#, x] \u0026; sol = Solve[Thread[coes == 0]]; Join[{1}, Table[a[n], {n, 1, max}] /. sol // First] (* _Jean-François Alcover_, Jun 20 2013 *)",
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 2, Pi/4, q^(1/2)] EllipticTheta[ 2, Pi/4, q^(7/2)] EllipticTheta[ 3, 0, q^7]/2 , {q, 0, n}]; (* _Michael Somos_, Aug 26 2015 *)",
				"a[ n_] := SeriesCoefficient[ 2^(-1/2) q^(7/8) EllipticTheta[ 2, Pi/4, q^(1/2)] QPochhammer[ -q^7] QPochhammer[ q^14], {q, 0, n}]; (* _Michael Somos_, Sep 06 2015 *)",
				"a[ n_] := SeriesCoefficient[ q Product[ (1 - q^k)^{1, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 3, 1, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 3}[[Mod[k, 28, 1]]], {k, n - 1}], {q, 0, n}]; (* _Michael Somos_, Sep 06 2015 *)"
			],
			"program": [
				"(MAGMA) Basis(CuspidalSubspace(HalfIntegralWeightForms(28,3/2)),100)",
				"(MAGMA) A := Basis( CuspForms( Gamma1(28), 3/2), 81); A[1] - A[2]; /* _Michael Somos_, Sep 06 2015 */",
				"(PARI) {a(n) = my(A); if( n\u003c1, 0, n--; A = x * O(x^n); polcoeff( eta(x + A) * eta(x^4 + A) * eta(x^14 + A)^4 / (eta(x^2 + A) * eta(x^7 + A) * eta(x^28 + A)), n))}; /* _Michael Somos_, Aug 15 2012 */"
			],
			"xref": [
				"Cf. A215556."
			],
			"keyword": "sign",
			"offset": "1,15",
			"author": "_Steven Finch_, Apr 22 2009",
			"references": 2,
			"revision": 27,
			"time": "2021-03-12T22:24:45-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}