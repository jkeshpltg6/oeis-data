{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A022290",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 22290,
			"data": "0,1,2,3,3,4,5,6,5,6,7,8,8,9,10,11,8,9,10,11,11,12,13,14,13,14,15,16,16,17,18,19,13,14,15,16,16,17,18,19,18,19,20,21,21,22,23,24,21,22,23,24,24,25,26,27,26,27,28,29,29,30,31,32,21,22,23,24,24,25,26",
			"name": "Replace 2^k in binary expansion of n with Fibonacci(k+2).",
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A022290/b022290.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e"
			],
			"formula": [
				"G.f.: 1/(1-x) * Sum_{k\u003e=0} F(k+2)*x^2^k/(1+x^2^k), where F = A000045.",
				"a(n) = Sum_{k\u003e=0} A030308(n,k)*A000045(k+2). - _Philippe Deléham_, Oct 15 2011",
				"a(A003714(n)) = n. - _R. J. Mathar_, Jan 31 2015",
				"a(A000225(n)) = A001911(n). - _Philippe Deléham_, Jun 05 2015",
				"From _Jeffrey Shallit_, Jul 17 2018: (Start)",
				"Can be computed from the recurrence:",
				"a(4*k)   = a(k)+a(2*k),",
				"a(4*k+1) = a(k)+a(2*k+1),",
				"a(4*k+2) = a(k)-a(2*k)+2*a(2*k+1),",
				"a(4*k+3) = a(k)-2*a(2*k)+3*a(2*k+1),",
				"and the initial terms a(0) = 0, a(1) = 1. (End)",
				"a(A003754(n)) = n-1. - _Rémy Sigrist_, Jan 28 2020"
			],
			"example": [
				"n=4 = 2^2 is replaced by A000045(2+2) =3. n=5 =2^2+2^0 is replaced by A000045(2+2)+A000045(0+2) = 3+1=4. - _R. J. Mathar_, Jan 31 2015",
				"This sequence regarded as a triangle with rows of lengths 1, 1, 2, 4, 8, 16, ...:",
				"0",
				"1",
				"2, 3",
				"3, 4, 5, 6",
				"5, 6, 7, 8, 8, 9, 10, 11",
				"8, 9, 10, 11, 11, 12, 13, 14, 13, 14, 15, 16, 16, 17, 18, 19",
				"... - _Philippe Deléham_, Jun 05 2015"
			],
			"maple": [
				"A022290 := proc(n)",
				"    dgs := convert(n,base,2) ;",
				"    add( op(i,dgs)*A000045(i+1),i=1..nops(dgs)) ;",
				"end proc: # _R. J. Mathar_, Jan 31 2015"
			],
			"mathematica": [
				"Table[Reverse[#].Fibonacci[1 + Range[Length[#]]] \u0026@ IntegerDigits[n, 2], {n, 0, 54}] (* IWABUCHI Yu(u)ki, Aug 01 2012 *)"
			],
			"program": [
				"(Haskell)",
				"a022290 0 = 0",
				"a022290 n = h n 0 $ drop 2 a000045_list where",
				"   h 0 y _      = y",
				"   h x y (f:fs) = h x' (y + f * r) fs where (x',r) = divMod x 2",
				"-- _Reinhard Zumkeller_, Oct 03 2012",
				"(PARI) my(m=Mod('x,'x^2-'x-1)); a(n) = subst(lift(subst(Pol(binary(n)), 'x,m)), 'x,2); \\\\ _Kevin Ryde_, Sep 22 2020"
			],
			"xref": [
				"Other sequences that are built by replacing 2^k in the binary representation with other numbers: A029931 (naturals), A054204 (even index Fibonaccis), A062877 (odd index Fibonaccis), A059590 (factorials), A089625 (primes).",
				"Cf. A003754."
			],
			"keyword": "nonn,tabf,base",
			"offset": "0,3",
			"author": "_Marc LeBrun_",
			"references": 22,
			"revision": 55,
			"time": "2021-06-13T07:45:43-04:00",
			"created": "1998-06-14T03:00:00-04:00"
		}
	]
}