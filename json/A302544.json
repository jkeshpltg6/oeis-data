{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A302544",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 302544,
			"data": "0,2,1,3,8,10,6,11,4,9,5,7,12,14,13,15,26,34,18,32,40,42,35,43,24,38,16,27,41,46,30,44,19,33,17,22,36,47,25,39,20,28,21,23,31,45,29,37,48,50,49,51,56,58,54,59,52,57,53,55,60,62,61,63,74,106,66",
			"name": "Lexicographically earliest sequence of distinct nonnegative numbers such that for any n \u003e= 0, A065359(a(n)) = - A065359(n).",
			"comment": [
				"This sequence is a self-inverse permutation of the nonnegative numbers, with fixed points A039004.",
				"We can build an analog of this sequence for any base b \u003e 1 by considering the alternating sum of digits in base b instead of A065359.",
				"This sequence has similarities with A298847.",
				"The scatter plots have an interesting, \"fibrous\" look. - _Antti Karttunen_, Jul 21 2018"
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A302544/b302544.txt\"\u003eTable of n, a(n) for n = 0..16384\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A302544/a302544.png\"\u003eColored scatterplot of the first 2^16 terms\u003c/a\u003e (where the color is function of A065359(n))",
				"Rémy Sigrist, \u003ca href=\"/A302544/a302544_1.png\"\u003eScatterplot of the first 3^10 terms of the base 3 analog of this sequence\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A302544/a302544_3.png\"\u003eScatterplot of the first 4^10 terms of the base 4 analog of this sequence\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A302544/a302544_2.png\"\u003eScatterplot of the first 10^6 terms of the base 10 analog of this sequence\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A302544/a302544_4.png\"\u003eScatterplot of the first 10!/2 terms of the factorial base analog of this sequence\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A302544/a302544.txt\"\u003eC++ program for A302544\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"example": [
				"The first terms, alongside the binary representations of n and of a(n), and A065359(n), are:",
				"  n   a(n)  bin(n)  bin(a(n))  A065359(n)",
				"  --  ----  ------  ---------  ----------",
				"   0     0       0       0      0",
				"   1     2       1      10      1",
				"   2     1      10       1     -1",
				"   3     3      11      11      0",
				"   4     8     100    1000      1",
				"   5    10     101    1010      2",
				"   6     6     110     110      0",
				"   7    11     111    1011      1",
				"   8     4    1000     100     -1",
				"   9     9    1001    1001      0",
				"  10     5    1010     101     -2",
				"  11     7    1011     111     -1",
				"  12    12    1100    1100      0",
				"  13    14    1101    1110      1",
				"  14    13    1110    1101     -1",
				"  15    15    1111    1111      0",
				"  16    26   10000   11010      1",
				"  17    34   10001  100010      2",
				"  18    18   10010   10010      0",
				"  19    32   10011  100000      1",
				"  20    40   10100  101000      2"
			],
			"program": [
				"(C++) See Links section."
			],
			"xref": [
				"Cf. A039004 (fixed points), A065359, A298847."
			],
			"keyword": "nonn,look,base",
			"offset": "0,2",
			"author": "_Rémy Sigrist_, Apr 09 2018",
			"references": 2,
			"revision": 27,
			"time": "2018-07-21T15:53:20-04:00",
			"created": "2018-04-10T14:14:56-04:00"
		}
	]
}