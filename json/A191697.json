{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A191697",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 191697,
			"data": "0,4,6,8,20,28,56,96,168,304,528,944,1664,2944,5216,9216,16320,28864,51072,90368,159872,282880,500480,885504,1566720,2771968,4904448,8677376,15352832,27163648,48060416,85032960,150448128,266186752,470962176,833269760",
			"name": "a(n) = r1^n + r2^n + r3^n where r1, r2, r3 are the three roots of x^3 - 2*x - 2 = 0.",
			"comment": [
				"Definition 1.1 defines F_3^n as a Boolean function and definition 1.3 defines the Fourier transform of a Boolean function. - _Michael Somos_, Aug 04 2012"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A191697/b191697.txt\"\u003eTable of n, a(n) for n = 1..2500\u003c/a\u003e",
				"X. Zhang, H. Guo, R. Feng and Y. Li, \u003ca href=\"https://doi.org/10.1016/j.disc.2011.03.012\"\u003eProof of a conjecture about rotation symmetric functions\u003c/a\u003e, Discrete Math., 311 (2011), 1281-1289.",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,2,2)."
			],
			"formula": [
				"a(n) = hat{F_3^n}(0), the Fourier transform evaluated at 0 of the Boolean function F_3^n defined by F_3^n(x_0, ..., x_{n-1}) = Sum_{ 0\u003ci\u003c=n-1} x_i x_{i+1(mod n)} x_{i+2(mod n)}. - _Michael Somos_, Aug 04 2012",
				"From _Michael Somos_, Aug 04 2012: (Start)",
				"G.f.: 2 * x^2 * (2 + 3*x) / (1 - 2*x^2 - 2*x^3).",
				"a(n + 3) = 2*a(n + 1) + 2*a(n). (End)",
				"a(n) = 4*A052907(n) +6*A052907(n-1). - _R. J. Mathar_, Aug 10 2012"
			],
			"example": [
				"G.f. = 4*x^2 + 6*x^3 + 8*x^4 + 20*x^5 + 28*x^6 + 56*x^7 + 96*x^8 + 168*x^9 + ..."
			],
			"mathematica": [
				"Rest[CoefficientList[Series[2*x^2*(2+3*x)/(1-2*x^2-2*x^3), {x, 0, 50}], x]] (* _G. C. Greubel_, Aug 13 2018 *)",
				"LinearRecurrence[{0, 2, 2}, {0, 4, 6}, 40] (* _Vincenzo Librandi_, Aug 14 2018 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, 0, polsym( x^3 - 2*x - 2, n)[n + 1])}; /* _Michael Somos_, Aug 04 2012 */",
				"(PARI) {a(n) = if( n\u003c1, 0, sum( x=0, 2^n-1, (-1)^sum( i=0, n-1, bittest(x, i) * bittest(x, (i+1)%n) * bittest(x, (i+2)%n))))}; /* _Michael Somos_, Aug 04 2012 */",
				"(MAGMA) m:=50; R\u003cx\u003e:=PowerSeriesRing(Integers(), m); [0] cat Coefficients(R!(2*x^2*(2+3*x)/(1-2*x^2-2*x^3))); // _G. C. Greubel_, Aug 13 2018",
				"(MAGMA) I:=[0,4,6]; [n le 3 select I[n] else 2*Self(n-2)+2*Self(n-3): n in [1..50]]; // _Vincenzo Librandi_, Aug 14 2018"
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, Jun 19 2011",
			"references": 1,
			"revision": 32,
			"time": "2018-08-14T09:00:41-04:00",
			"created": "2011-06-19T17:14:07-04:00"
		}
	]
}