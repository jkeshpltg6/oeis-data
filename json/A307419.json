{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A307419",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 307419,
			"data": "1,0,1,0,3,1,0,11,9,1,0,50,71,18,1,0,274,580,245,30,1,0,1764,5104,3135,625,45,1,0,13068,48860,40369,11515,1330,63,1,0,109584,509004,537628,203889,33320,2506,84,1,0,1026576,5753736,7494416,3602088,775929,81900,4326,108,1",
			"name": "Triangle of harmonic numbers T(n, k) = [t^n] Gamma(n+k+t)/Gamma(k+t) for n \u003e= 0 and 0 \u003c= k \u003c= n, read by rows.",
			"formula": [
				"E.g.f.: ((1-t)^(-x/(1-t)).",
				"T(n, k) = n!*Sum_{L1+L2+...+Lk=n} H(L1)H(L2)...H(Lk) with Li \u003e 0, where H(n) are the harmonic numbers A001008.",
				"T(n, k) = n!*Sum_{i=0..n-k} abs(Stirling1(n-i, k))/(n-i)!*binomial(i+k-1, i).",
				"T(n, k) = k! [x^k] (d^n/dx^n) ((log(1-x)/(x-1))^n/n!), the e.g.f. for column k where Col(k) = [T(n+k, k) for n = 0, 1, 2, ...]. - _Peter Luschny_, Apr 12 2019"
			],
			"example": [
				"Triangle starts:",
				"0: [1]",
				"1: [0,       1]",
				"2: [0,       3,       1]",
				"3: [0,      11,       9,       1]",
				"4: [0,      50,      71,      18,       1]",
				"5: [0,     274,     580,     245,      30,      1]",
				"6: [0,    1764,    5104,    3135,     625,     45,     1]",
				"7: [0,   13068,   48860,   40369,   11515,   1330,    63,    1]",
				"8: [0,  109584,  509004,  537628,  203889,  33320,  2506,   84,   1]",
				"9: [0, 1026576, 5753736, 7494416, 3602088, 775929, 81900, 4326, 108, 1]",
				"Col:   A000254, A001706, A001713, A001719, ..."
			],
			"maple": [
				"# Note that for n \u003e 16 Maple fails (at least in some versions) to compute the",
				"# terms properly. Inserting 'simplify' or numerical evaluation might help.",
				"A307419Row := proc(n) local ogf, ser; ogf := (n, k) -\u003e GAMMA(n+k+x)/GAMMA(k+x);",
				"ser := (n, k) -\u003e series(ogf(n,k),x,k+2); seq(coeff(ser(n,k),x,k),k=0..n) end: seq(A307419Row(n), n=0..9);",
				"# Alternatively by the egf for column k:",
				"A307419Col := proc(n, len) local f, egf, ser; f := (n,x) -\u003e (log(1-x)/(x-1))^n/n!;",
				"egf := (n,x) -\u003e diff(f(n, x), [x$n]); ser := n -\u003e series(egf(n, x), x, len);",
				"seq(k!*coeff(ser(n), x, k), k=0..len-1) end:",
				"seq(print(A307419Col(k, 10)), k=0..9); # _Peter Luschny_, Apr 12 2019"
			],
			"mathematica": [
				"f[n_, x_] := f[n, x] = D[(Log[1 - x]/(x - 1))^n/n!, {x, n}];",
				"T[n_, k_] := (n - k)! SeriesCoefficient[f[k, x], {x, 0, n - k}];",
				"Table[T[n, k], {n, 0, 9}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Jul 13 2019 *)"
			],
			"program": [
				"(Maxima) T(n,k):=n!*sum((binomial(k+i-1,i)*abs(stirling1(n-i,k)))/(n-i)!,i,0,n-k)",
				"(Maxima) taylor((1-t)^(-x/(1-t)),t,0,7,x,0,7);",
				"(Maxima) T(n,k):=coeff(taylor(gamma(n+k+t)/gamma(k+t),t,0,10),t,k);",
				"(PARI) T(n, k) = n!*sum(i=0, n-k, abs(stirling(n-i, k, 1))*binomial(i+k-1, i)/(n-i)!); \\\\ _Michel Marcus_, Apr 13 2019"
			],
			"xref": [
				"Row sums are A087761.",
				"Columns are A000007, A000254, A001706, A001713, A001719.",
				"Cf. A001008/A002805."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Vladimir Kruchinin_, Apr 08 2019",
			"references": 2,
			"revision": 36,
			"time": "2019-07-13T03:36:04-04:00",
			"created": "2019-04-13T02:16:03-04:00"
		}
	]
}