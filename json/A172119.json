{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A172119",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 172119,
			"data": "1,1,1,1,2,1,1,3,2,1,1,4,4,2,1,1,5,7,4,2,1,1,6,12,8,4,2,1,1,7,20,15,8,4,2,1,1,8,33,28,16,8,4,2,1,1,9,54,52,31,16,8,4,2,1,1,10,88,96,60,32,16,8,4,2,1,1,11,143,177,116,63,32,16,8,4,2,1,1,12,232,326,224,124,64,32,16",
			"name": "Sum the k preceding elements in the same column and add 1 every time.",
			"comment": [
				"Columns are related to Fibonacci n-step numbers. Are there closed forms for the sequences in the columns?",
				"We denote by a(n,k) the number which is in the (n+1)-th row and (k+1)-th-column. With help of the definition, we also have the recurrence relation: a(n+k+1, k) = 2*a(n+k, k) - a(n, k). We see on the main diagonal the numbers 1,2,4, 8, ..., which is clear from the formula for the general term d(n)=2^n. - _Richard Choulet_, Jan 31 2010",
				"Most of the paper by Dunkel (1925) is a study of the columns of this table. - _Petros Hadjicostas_, Jun 14 2019"
			],
			"link": [
				"O. Dunkel, \u003ca href=\"http://www.jstor.org/stable/2298801\"\u003eSolutions of a probability difference equation\u003c/a\u003e, Amer. Math. Monthly, 32 (1925), 354-370; see p. 356.",
				"T. Langley, J. Liese, and J. Remmel, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL14/Langley/langley2.html\"\u003eGenerating Functions for Wilf Equivalence Under Generalized Factor Order\u003c/a\u003e, J. Int. Seq. 14 (2011), # 11.4.2.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Fibonaccin-StepNumber.html\"\u003eFibonacci n-Step Number\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Fibonacci_number\"\u003eFibonacci number\u003c/a\u003e."
			],
			"formula": [
				"T(n,0) = 1.",
				"T(n,1) = n.",
				"T(n,2) = A000071(n+1).",
				"T(n,3) = A008937(n-2).",
				"The general term in the n-th row and k-th column is given by: a(n, k) = Sum_{j=0..floor(n/(k+1))} ((-1)^j binomial(n-k*j,n-(k+1)*j)*2^(n-(k+1)*j)). For example: a(5,3) = binomial(5,5)*2^5 - binomial(2,1)*2^1 = 28. The generating function of the (k+1)-th column satisfies: psi(k)(z)=1/(1-2*z+z^(k+1)) (for k=0 we have the known result psi(0)(z)=1/(1-z)). - _Richard Choulet_, Jan 31 2010 [By saying \"(k+1)-th column\" the author actually means \"k-th column\" for k = 0, 1, 2, ... - _Petros Hadjicostas_, Jul 26 2019]"
			],
			"example": [
				"Triangle begins:",
				"n\\k|....0....1....2....3....4....5....6....7....8....9...10",
				"---|-------------------------------------------------------",
				"0..|....1",
				"1..|....1....1",
				"2..|....1....2....1",
				"3..|....1....3....2....1",
				"4..|....1....4....4....2....1",
				"5..|....1....5....7....4....2....1",
				"6..|....1....6...12....8....4....2....1",
				"7..|....1....7...20...15....8....4....2....1",
				"8..|....1....8...33...28...16....8....4....2....1",
				"9..|....1....9...54...52...31...16....8....4....2....1",
				"10.|....1...10...88...96...60...32...16....8....4....2....1"
			],
			"maple": [
				"for k from 0 to 20 do for n from 0 to 20 do b(n):=sum((-1)^j*binomial(n-k*j,n-(k+1)*j)*2^(n-(k+1)*j),j=0..floor(n/(k+1))):od: seq(b(n),n=0..20):od; # _Richard Choulet_, Jan 31 2010",
				"A172119 := proc(n,k)",
				"    option remember;",
				"    if k = 0 then",
				"        1;",
				"    elif k \u003e n then",
				"        0;",
				"    else",
				"        1+add(procname(n-k+i,k),i=0..k-1) ;",
				"    end if;",
				"end proc:",
				"seq(seq(A172119(n,k),k=0..n),n=0..12) ; # _R. J. Mathar_, Sep 16 2017"
			],
			"mathematica": [
				"T[_, 0] = 1; T[n_, n_] = 1; T[n_, k_] /; k\u003en = 0; T[n_, k_] := T[n, k] = Sum[T[n-k+i, k], {i, 0, k-1}] + 1;",
				"Table[T[n, k], {n, 0, 12}, {k, 0, n}]//Flatten",
				"Table[Sum[(-1)^j*2^(n-k-(k+1)*j)*Binomial[n-k-k*j, n-k-(k+1)*j], {j, 0, Floor[(n-k)/(k+1)]}], {n,0,12}, {k,0,n}]//Flatten (* _G. C. Greubel_, Jul 27 2019 *)"
			],
			"program": [
				"(PARI) T(n,k) = if(k\u003c0 || k\u003en, 0, k==1 \u0026\u0026 k==n, 1, 1 + sum(j=1,k, T(n-j,k)));",
				"for(n=1,12, for(k=0,n, print1(T(n,k), \", \"))) \\\\ _G. C. Greubel_, Jul 27 2019",
				"(MAGMA)",
				"T:= func\u003c n,k | (\u0026+[(-1)^j*2^(n-k-(k+1)*j)*Binomial(n-k-k*j, n-k-(k+1)*j): j in [0..Floor((n-k)/(k+1))]]) \u003e;",
				"[[T(n,k): k in [0..n]]: n in [0..12]]; // _G. C. Greubel_, Jul 27 2019",
				"(Sage)",
				"@CachedFunction",
				"def T(n, k):",
				"    if (k==0 and k==n): return 1",
				"    elif (k\u003c0 or k\u003en): return 0",
				"    else: return 1 + sum(T(n-j, k) for j in (1..k))",
				"[[T(n, k) for k in (0..n)] for n in (0..12)] # _G. C. Greubel_, Jul 27 2019",
				"(GAP)",
				"T:= function(n,k)",
				"    if k=0 and k=n then return 1;",
				"    elif k\u003c0 or k\u003en then return 0;",
				"    else return 1 + Sum([1..k], j-\u003e T(n-j,k));",
				"    fi;",
				"  end;",
				"Flat(List([0..12], n-\u003e List([0..n], k-\u003e T(n,k) ))); # _G. C. Greubel_, Jul 27 2019"
			],
			"xref": [
				"Cf. A000071, A008937, A144428.",
				"Cf. (1-((-1)^T(n, k)))/2 = A051731, see formula by _Hieronymus Fischer_ in A022003."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Mats Granvik_, Jan 26 2010",
			"references": 15,
			"revision": 38,
			"time": "2019-07-28T17:05:15-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}