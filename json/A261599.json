{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A261599",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 261599,
			"data": "1,1,0,1,1,3,13,24,67,252,1795,4038,16812,61750,349806,3485026,10391070,49433135,240064988,1282012986,9167581934,131550811985,459677212302,2707382738558,14318807586215,94084166753923,601900541189696,5894253303715121",
			"name": "Number of primitive (aperiodic, or Lyndon) necklaces with n beads of unlabeled colors such that the numbers of beads per color are distinct.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A261599/b261599.txt\"\u003eTable of n, a(n) for n = 0..300\u003c/a\u003e",
				"F. Ruskey, \u003ca href=\"http://combos.org/necklace\"\u003eNecklaces, Lyndon words, De Bruijn sequences, etc.\u003c/a\u003e",
				"F. Ruskey, \u003ca href=\"/A000011/a000011.pdf\"\u003eNecklaces, Lyndon words, De Bruijn sequences, etc.\u003c/a\u003e [Cached copy, with permission, pdf format only]",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Necklace.html\"\u003eNecklace\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Lyndon_word\"\u003eLyndon word\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Necklace_(combinatorics)\"\u003eNecklace (combinatorics)\u003c/a\u003e",
				"\u003ca href=\"/index/Ne#necklaces\"\u003eIndex entries for sequences related to necklaces\u003c/a\u003e"
			],
			"formula": [
				"a(n) = (1/n) * Sum_{d | n} moebius(n/d) * A007837(d) for n\u003e0. - _Andrew Howroyd_, Dec 21 2017"
			],
			"example": [
				"a(4) = 1: 0001.",
				"a(5) = 3: 00001, 00011, 00101.",
				"a(6) = 13: 000001, 000011, 000101, 000112, 000121, 000122, 001012, 001021, 001022, 001102, 001201, 001202, 010102.",
				"a(7) = 24: 0000001, 0000011, 0000101, 0000111, 0000112, 0000121, 0000122, 0001001, 0001011, 0001012, 0001021, 0001022, 0001101, 0001102, 0001201, 0001202, 0010011, 0010012, 0010021, 0010022, 0010101, 0010102, 0010201, 0010202."
			],
			"maple": [
				"with(numtheory):",
				"b:= proc(n, i, g, d, j) option remember; `if`(i*(i+1)/2\u003cn or g\u003e0",
				"       and g\u003cd, 0, `if`(n=0, `if`(d=g, 1, 0), b(n, i-1, g, d, j)+",
				"      `if`(i\u003en, 0, binomial(n/j, i/j)*b(n-i, i-1, igcd(i, g), d, j))))",
				"    end:",
				"a:= n-\u003e `if`(n=0, 1, add(add((f-\u003e `if`(f=0, 0, f*b(n$2, 0, d, j)))(",
				"                     mobius(j)), j=divisors(d)), d=divisors(n))/n):",
				"seq(a(n), n=0..30);"
			],
			"mathematica": [
				"a[0] = 1; a[n_] := With[{P = Product[1 + x^k/k!, {k, 1, n}] + O[x]^(n+1) // Normal}, DivisorSum[n, MoebiusMu[n/#]*#!*Coefficient[P, x, #]\u0026]/n];",
				"Table[a[n], {n, 0, 30}] (* _Jean-François Alcover_, May 28 2018, after _Andrew Howroyd_ *)"
			],
			"program": [
				"(PARI) a(n)={if(n==0, 1, my(p=prod(k=1, n, (1+x^k/k!) + O(x*x^n))); sumdiv(n, d, moebius(n/d)*d!*polcoeff(p, d))/n)} \\\\ _Andrew Howroyd_, Dec 21 2017"
			],
			"xref": [
				"Cf. A007837, A261531, A261600."
			],
			"keyword": "nonn",
			"offset": "0,6",
			"author": "_Alois P. Heinz_, Aug 25 2015",
			"references": 4,
			"revision": 23,
			"time": "2019-04-30T08:22:30-04:00",
			"created": "2015-08-27T10:57:29-04:00"
		}
	]
}