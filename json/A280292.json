{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A280292",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 280292,
			"data": "0,0,0,2,0,0,0,4,3,0,0,2,0,0,0,6,0,3,0,2,0,0,0,4,5,0,6,2,0,0,0,8,0,0,0,5,0,0,0,4,0,0,0,2,3,0,0,6,7,5,0,2,0,6,0,4,0,0,0,2,0,0,3,10,0,0,0,2,0,0,0,7,0,0,5,2,0,0,0,6,9,0,0,2,0,0,0,4,0,3,0,2,0,0,0,8,0,7,3,7,0,0,0,4,0",
			"name": "a(n) = sopfr(n) - sopf(n).",
			"comment": [
				"Alladi and Erdős (1977) proved that for all numbers m\u003e=0, m!=1, the sequence of numbers k such that a(k) = m has a positive asymptotic density which is equal to a rational multiple of 1/zeta(2) = 6/Pi^2 (A059956). For example, when m=0, the sequence is the squarefree numbers (A005117), whose density is 6/Pi^2, and when m=2 the sequence is A081770, whose density is 1/Pi^2. - _Amiram Eldar_, Nov 02 2020"
			],
			"reference": [
				"Jean-Marie De Koninck and Aleksandar Ivić, Topics in Arithmetical Functions: Asymptotic Formulae for Sums of Reciprocals of Arithmetical Functions and Related Fields, Amsterdam, Netherlands: North-Holland, 1980. See pp. 164-166.",
				"Steven R. Finch, Mathematical Constants II, Encyclopedia of Mathematics and Its Applications, Cambridge University Press, Cambridge, 2018, p. 165."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A280292/b280292.txt\"\u003eTable of n, a(n) for n = 1..20000\u003c/a\u003e",
				"Krishnaswami Alladi and Paul Erdős, \u003ca href=\"https://projecteuclid.org/euclid.pjm/1102811427\"\u003eOn an additive arithmetic function\u003c/a\u003e, Pacific Journal of Mathematics, Vol. 71, No. 2 (1977), pp. 275-294, \u003ca href=\"https://msp.org/pjm/1977/71-2/pjm-v71-n2-p01-s.pdf\"\u003ealternative link\u003c/a\u003e.",
				"Jean-Marie De Koninck, Paul Erdős and Aleksandar Ivić, \u003ca href=\"https://doi.org/10.4153/CMB-1981-035-7\"\u003eReciprocals of certain large additive functions\u003c/a\u003e, Canadian Mathematical Bulletin, Vol. 24, No. 2 (1981), pp. 225-231.",
				"Aleksandar Ivić, \u003ca href=\"https://arxiv.org/abs/math/0311505\"\u003eOn certain large additive functions\u003c/a\u003e, arXiv:math/0311505 [math.NT], 2003."
			],
			"formula": [
				"a(n) = A001414(n) - A008472(n).",
				"a(A005117(n)) = 0.",
				"a(n) = A001414(A003557(n)). - _Antti Karttunen_, Oct 07 2017",
				"Additive with a(1) = 0 and a(p^e) = p*(e-1) for prime p and e \u003e 0. - _Werner Schulte_, Feb 24 2019",
				"From _Amiram Eldar_, Nov 02 2020: (Start)",
				"a(n) = a(A057521(n)).",
				"Sum_{n\u003c=x} a(n) ~ x*log(log(x)) + O(x) (Alladi and Erdős, 1977).",
				"Sum_{n\u003c=x, n nonsquarefree} 1/a(n) ~ c*x + O(sqrt(x)*log(x)), where c = Integral_{t=0..1} (F(t)-6/Pi^2)/t dt, and F(t) = Product_{p prime} (1-1/p)*(1-1/(t^p - p)) (De Koninck et al., 1981; Finch, 2018), or, equivalently c = Sum_{k\u003e=2} d(k)/k = 0.1039..., where d(k) = (6/Pi^2)*A338559(k)/A338560(k) is the asymptotic density of the numbers m with a(m) = k (Alladi and Erdős, 1977; Ivić, 2003). (End)"
			],
			"mathematica": [
				"Array[Total@ # - Total@ Union@ # \u0026@ Flatten[ConstantArray[#1, #2] \u0026 @@@ FactorInteger@ #] \u0026, 105] (* _Michael De Vlieger_, Feb 25 2019 *)"
			],
			"program": [
				"(PARI) sopfr(n) = my(f=factor(n)); sum(j=1, #f~, f[j, 1]*f[j, 2]);",
				"sopf(n) = my(f=factor(n)); sum(j=1, #f~, f[j, 1]);",
				"a(n) = sopfr(n) - sopf(n);"
			],
			"xref": [
				"Cf. A001414 (sopfr), A008472 (sopf).",
				"Cf. A003557, A005117, A013929.",
				"Cf. A280163, A280286.",
				"Cf. A013929, A057521, A059956, A081770, A338559, A338560."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Michel Marcus_, Dec 31 2016",
			"ext": [
				"More terms from _Antti Karttunen_, Oct 07 2017"
			],
			"references": 5,
			"revision": 22,
			"time": "2020-11-02T20:39:09-05:00",
			"created": "2016-12-31T06:35:40-05:00"
		}
	]
}