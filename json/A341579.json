{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A341579",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 341579,
			"data": "0,1,3,7,13,25,47,89,165,307,569,1057,1959,3633,6733,12483,23137,42889,79495,147353,273125,506259,938377,1739345,3223975,5975841,11076573,20531107,38055633,70538425,130747207,242347849,449206325,832631027,1543331769,2860658497",
			"name": "Number of steps needed to solve the Towers of Hanoi exchanging disks puzzle with 3 pegs and n disks.",
			"comment": [
				"Scorer, Grundy and Smith define a variation of the towers of Hanoi puzzle where the smallest disk moves freely and two disks can exchange positions when they differ in size by 1, are on different pegs, and each is top-most on its peg.  The aim is to move a stack of n disks from one peg to another.",
				"Stockmeyer et al. show that the number of steps in the solution is a(n), and that the sequence of steps is unique.  They offer as exercises for the reader to prove that the number of exchanges with the solution is a(n-1), and that a(n) is the largest number of steps between any two configurations (in other words a(n) is the diameter of the state graph)."
			],
			"link": [
				"Kevin Ryde, \u003ca href=\"/A341579/b341579.txt\"\u003eTable of n, a(n) for n = 0..700\u003c/a\u003e",
				"House of Graphs, graphs \u003ca href=\"https://hog.grinvin.org/ViewGraphInfo.action?id=44105\"\u003e44105\u003c/a\u003e, \u003ca href=\"https://hog.grinvin.org/ViewGraphInfo.action?id=44107\"\u003e44107\u003c/a\u003e, \u003ca href=\"https://hog.grinvin.org/ViewGraphInfo.action?id=44109\"\u003e44109\u003c/a\u003e.  Diameters = a(3..5).",
				"R. S. Scorer, P. M. Grundy, and C. A. B. Smith, \u003ca href=\"http://www.jstor.org/stable/3606393\"\u003eSome Binary Games\u003c/a\u003e, The Mathematical Gazette, July 1944, volume 28, number 280, pages 96-103, section 4(iii) Plane Network Game.",
				"Paul K. Stockmeyer, C. Douglass Bateman, James W. Clark, Cyrus R. Eyster, Matthew T. Harrison, Nicholas A. Loehr, Patrick J. Rodriguez, and Joseph R. Simmons III, \u003ca href=\"https://doi.org/10.1080/00207169508804452\"\u003eExchanging Disks in the Tower of Hanoi\u003c/a\u003e, International Journal of Computer Mathematics, volume 59, number 1-2, pages 37-47, 1995.  Also \u003ca href=\"http://www.cs.wm.edu/~pkstoc/gov.pdf\"\u003eauthor's copy\u003c/a\u003e.  a(n) = f(n) in section 3.",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,0,-1,2,-2).",
				"\u003ca href=\"/index/To#Hanoi\"\u003eIndex entries for sequences related to Towers of Hanoi\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 2*A341580(n-1) + 1 for n\u003e=1. [Stockmeyer et al.]",
				"a(n) = a(n-1) + a(n-2) + 2*a(n-4) + 3 for n\u003e=4. [Stockmeyer et al.]",
				"a(n) = 2*a(n-1) - a(n-3) + 2*a(n-4) - 2*a(n-5).",
				"G.f.: x*(1 + x + x^2) / ( (1-x) * (1 - x - x^2 - 2*x^4) ).",
				"G.f.: -1/(1-x) + (1 + x + x^2 + 2*x^3)/(1 - x - x^2 - 2*x^4)."
			],
			"example": [
				"As a graph where each vertex is a configuration of disks on pegs and each edge is a step (as drawn by Scorer et al.),",
				"                A",
				"               / \\",
				"              *---*          n=3 disks",
				"             /     \\          A to D",
				"            *       *      a(3) = 7 steps",
				"           / \\     / \\",
				"          *---B---*---*",
				"             /     \\",
				"        *   /       \\   *",
				"       / \\ /         \\ / \\",
				"      *---C           *---*",
				"     /     \\         /     \\",
				"    *       *-------*       *",
				"   / \\     / \\     / \\     / \\",
				"  D---*---*---*   *---*---*---*",
				"The formula using A341580 is A to B distance A341580(2) = 3, the same (by symmetry) from D to C, and +1 from B to C.  B to C is where the largest disk moves to the target peg (by exchange with the second-largest)."
			],
			"program": [
				"(PARI) my(p=Mod('x,'x^4-'x^3-'x^2-2)); a(n) = subst(lift(p^n),'x,2) - 1;"
			],
			"xref": [
				"Cf. A341580 (halfway), A341582 (first differences), A341583 (geometric length)."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Kevin Ryde_, Feb 15 2021",
			"references": 6,
			"revision": 16,
			"time": "2021-05-22T21:00:52-04:00",
			"created": "2021-02-15T22:56:03-05:00"
		}
	]
}