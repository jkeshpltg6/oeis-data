{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A162978",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 162978,
			"data": "1,0,1,4,15,52,257,1272,7679,47864,346113,2604380,22022143,194053836,1881735169,18998097328,207983607807,2366490065968,28880901505025,365599818496116,4922617151619071,68612903386404260,1010501269355233281,15376572385777544744",
			"name": "Number of fixed points in all alternating (i.e., down-up) permutations of {1,2,...,n}.",
			"comment": [
				"a(n) = Sum_{k\u003e=0} k*A162979(n,k).",
				"a(2n+1) = A162977(2n+1)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A162978/b162978.txt\"\u003eTable of n, a(n) for n = 1..485\u003c/a\u003e",
				"R. P. Stanley, \u003ca href=\"http://math.mit.edu/~rstan/transparencies/ida.pdf\"\u003eAlternating permutations\u003c/a\u003e Talk slides."
			],
			"formula": [
				"a(2n) = E(2n) + (-1)^n*E(0) + 2*Sum_{j=1..n-1} (-1)^j*E(2n-2j), a(2n+1) = Sum_{j=0..n} (-1)^j*E(2n+1-2j), where E(i) = A000111(i) are the Euler (or up-down) numbers."
			],
			"example": [
				"a(4)=4 because in the 5 (=A000111(4)) down-up permutations of {1,2,3,4}, namely 4132, 3142, 2143, 4231, and 3241, we have a total of 1+0+0+2+1=4 fixed points."
			],
			"maple": [
				"E := sec(x)+tan(x): Eser := series(E, x = 0, 30): for n from 0 to 27 do E[n] := factorial(n)*coeff(Eser, x, n) end do: for n to 12 do a[2*n] := E[2*n]+(-1)^n*E[0]+2*add((-1)^j*E[2*n-2*j], j = 1 .. n-1) end do: for n from 0 to 12 do a[2*n+1] := add((-1)^j*E[2*n+1-2*j], j = 0 .. n) end do: seq(a[n], n = 1 .. 25);"
			],
			"mathematica": [
				"a111[n_] := If[EvenQ[n], Abs[EulerE[n]], Abs[(2^(n+1) (2^(n+1) - 1) BernoulliB[n+1])/(n+1)]];",
				"a[n_?EvenQ] := With[{m = n/2}, a111[2m] + (-1)^m a111[0] + 2Sum[(-1)^j a111[2m - 2j], {j, 1, m-1}]];",
				"a[n_?OddQ] := With[{m = (n-1)/2}, Sum[(-1)^j a111[2m+1-2j], {j, 0, m}]];",
				"Array[a, 25] (* _Jean-François Alcover_, Jul 24 2018 *)"
			],
			"xref": [
				"Cf. A000111, A162977, A162979."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Emeric Deutsch_, Aug 06 2009",
			"references": 3,
			"revision": 16,
			"time": "2018-07-24T02:57:40-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}