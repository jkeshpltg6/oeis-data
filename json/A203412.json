{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A203412",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 203412,
			"data": "1,1,1,4,3,1,28,19,6,1,280,180,55,10,1,3640,2260,675,125,15,1,58240,35280,10360,1925,245,21,1,1106560,658000,190680,35385,4620,434,28,1,24344320,14266560,4090240,756840,100065,9828,714,36,1",
			"name": "Triangle read by rows, a(n,k), n\u003e=k\u003e=1, which represent the s=3, h=1 case of a two-parameter generalization of Stirling numbers arising in conjunction with normal ordering.",
			"comment": [
				"Also the Bell transform of the triple factorial numbers A007559 which adds a first column (1,0,0 ...) on the left side of the triangle. For the definition of the Bell transform see A264428. See A051141 for the triple factorial numbers A032031 and A004747 for the triple factorial numbers A008544 as well as A039683 and A132062 for the case of double factorial numbers. - _Peter Luschny_, Dec 23 2015"
			],
			"link": [
				"Richell O. Celeste, Roberto B. Corcino, Ken Joffaniel M. Gonzales. \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL20/Celeste/celeste3.html\"\u003e Two Approaches to Normal Order Coefficients\u003c/a\u003e. Journal of Integer Sequences, Vol. 20 (2017), Article 17.3.5.",
				"T. Mansour, M. Schork, and M. Shattuck, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v18i1p77/0\"\u003eOn a new family of generalized Stirling and Bell numbers\u003c/a\u003e, Electron. J. Combin. 18 (2011) #P77 (33 pp.).",
				"Toufik Mansour, Matthias Schork and Mark Shattuck, \u003ca href=\"http://dx.doi.org/10.1016/j.aml.2012.02.009\"\u003eOn the Stirling numbers associated with the meromorphic Weyl algebra\u003c/a\u003e, Applied Mathematics Letters, Volume 25, Issue 11, November 2012, Pages 1767-1771."
			],
			"formula": [
				"(1) Is given by the recurrence relation",
				"  a(n+1,k) = a(n,k-1)+(3*n-2*k)*a(n,k) if n\u003e=0 and k\u003e=1, along with the initial values a(n,0) = delta_{n,0} and a(0,k) = delta_{0,k} for all n,k\u003e=0.",
				"(2) Is given explicitly by",
				"  a(n,k) = (n!*3^n)/(k!*2^k)*Sum{j=0..k} (-1)^j*C(k,j)*C(n-2*j/3-1,n) for all n\u003e=k\u003e=1.",
				"a(n,1) = A007559(n-1). - _Peter Luschny_, Dec 21 2015"
			],
			"example": [
				"Triangle starts:",
				"[    1]",
				"[    1,     1]",
				"[    4,     3,     1]",
				"[   28,    19,     6,    1]",
				"[  280,   180,    55,   10,   1]",
				"[ 3640,  2260,   675,  125,  15,  1]",
				"[58240, 35280, 10360, 1925, 245, 21, 1]"
			],
			"maple": [
				"A203412 := (n,k) -\u003e (n!*3^n)/(k!*2^k)*add((-1)^j*binomial(k,j)*binomial(n-2*j/3-1, n), j=0..k): seq(seq(A203412(n,k),k=1..n),n=1..9); # _Peter Luschny_, Dec 21 2015"
			],
			"mathematica": [
				"Table[(n! 3^n)/(k! 2^k) Sum[ (-1)^j Binomial[k, j] Binomial[n - 2 j/3 - 1, n], {j, 0, k}], {n, 9}, {k, n}] // Flatten (* _Michael De Vlieger_, Dec 23 2015 *)"
			],
			"program": [
				"(Sage) # uses[bell_transform from A264428]",
				"triplefactorial = lambda n: prod(3*k + 1 for k in (0..n-1))",
				"def A203412_row(n):",
				"    trifact = [triplefactorial(k) for k in (0..n)]",
				"    return bell_transform(n, trifact)",
				"[A203412_row(n) for n in (0..8)] # _Peter Luschny_, Dec 21 2015"
			],
			"xref": [
				"Cf. A007559, A032031, A039683, A051141, A132062, A264428."
			],
			"keyword": "nonn,tabl",
			"offset": "1,4",
			"author": "_Mark Shattuck_, Jan 01 2012",
			"references": 4,
			"revision": 36,
			"time": "2020-07-03T14:55:05-04:00",
			"created": "2012-01-14T13:31:29-05:00"
		}
	]
}