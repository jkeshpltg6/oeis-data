{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001090",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1090,
			"id": "M4554 N1936",
			"data": "0,1,8,63,496,3905,30744,242047,1905632,15003009,118118440,929944511,7321437648,57641556673,453811015736,3572846569215,28128961537984,221458845734657,1743541804339272,13726875588979519,108071462907496880",
			"name": "a(n) = 8*a(n-1) - a(n-2); a(0) = 0, a(1) = 1.",
			"comment": [
				"Number of units of a(n) belongs to a periodic sequence: 0, 1, 8, 3, 6, 5, 4, 7, 2, 9. - _Mohamed Bouhamida_, Sep 04 2009",
				"This sequence gives the values of y in solutions of the Diophantine equation x^2 - 15*y^2 = 1; the corresponding values of x are in A001091. - _Vincenzo Librandi_, Nov 12 2010 [edited by _Jon E. Schoenfield_, May 02 2014]",
				"For n \u003e= 2, a(n) equals the permanent of the (n-1) X (n-1) tridiagonal matrix with 8's along the main diagonal, and i's along the superdiagonal and the subdiagonal (i is the imaginary unit). - _John M. Campbell_, Jul 08 2011",
				"For n \u003e= 1, a(n) equals the number of 01-avoiding words of length n-1 on alphabet {0,1,...,7}. - _Milan Janjic_, Jan 25 2015"
			],
			"reference": [
				"Bastida, Julio R. Quadratic properties of a linearly recurrent sequence. Proceedings of the Tenth Southeastern Conference on Combinatorics, Graph Theory and Computing (Florida Atlantic Univ., Boca Raton, Fla., 1979), pp. 163--166, Congress. Numer., XXIII-XXIV, Utilitas Math., Winnipeg, Man., 1979. MR0561042 (81e:10009) - From _N. J. A. Sloane_, May 30 2012",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A001090/b001090.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e (terms 0..100 from T. D. Noe)",
				"Marco Abrate, Stefano Barbero, Umberto Cerruti, Nadir Murru, \u003ca href=\"https://www.emis.de/journals/INTEGERS/papers/p38/p38.Abstract.html\"\u003ePolynomial sequences on quadratic curves\u003c/a\u003e, Integers, Vol. 15, 2015, #A38.",
				"Andersen, K., Carbone, L. and Penta, D., \u003ca href=\"https://pdfs.semanticscholar.org/8f0c/c3e68d388185129a56ed73b5d21224659300.pdf\"\u003eKac-Moody Fibonacci sequences, hyperbolic golden ratios, and real quadratic fields\u003c/a\u003e, Journal of Number Theory and Combinatorics, Vol 2, No. 3 pp 245-278, 2011. See Section 9.",
				"H. Brocard, \u003ca href=\"http://resolver.sub.uni-goettingen.de/purl?PPN598948236_0004/DMDLOG_0053\"\u003eNotes élémentaires sur le problème de Peel\u003c/a\u003e, Nouvelle Correspondance Mathématique, 4 (1878), 337-343.",
				"E. I. Emerson, \u003ca href=\"http://www.fq.math.ca/Scanned/7-3/emerson.pdf\"\u003eRecurrent Sequences in the Equation DQ^2=R^2+N\u003c/a\u003e, Fib. Quart., 7 (1969), pp. 231-242.",
				"A. F. Horadam, \u003ca href=\"http://www.fq.math.ca/Scanned/5-5/horadam.pdf\"\u003eSpecial properties of the sequence W_n(a,b; p,q)\u003c/a\u003e, Fib. Quart., 5.5 (1967), 424-434. Case a=0,b=1; p=8, q=-1.",
				"M. Janjic, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL18/Janjic/janjic63.html\"\u003eOn Linear Recurrence Equations Arising from Compositions of Positive Integers\u003c/a\u003e, Journal of Integer Sequences, Vol. 18 (2015), Article 15.4.7.",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"W. Lang, \u003ca href=\"http://www.fq.math.ca/Scanned/38-5/lang.pdf\"\u003eOn polynomials related to powers of the generating function of Catalan's numbers\u003c/a\u003e, Fib. Quart. 38,5 (2000) 408-419; Eq.(44), lhs, m=10.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (8,-1)."
			],
			"formula": [
				"15*a(n)^2 - A001091(n)^2 = -1.",
				"a(n) = sqrt((A001091(n)^2 - 1)/15).",
				"a(n) = S(2*n-1, sqrt(10))/sqrt(10) = S(n-1, 8); S(n, x) := U(n, x/2), Chebyshev polynomials of 2nd kind, A049310, with S(-1, x) := 0.",
				"From _Barry E. Williams_, Aug 18 2000: (Start)",
				"a(n) = {{(4+sqrt(15))^n} - {(4-sqrt(15))^n}}/2*sqrt(15).",
				"G.f.: x/(1-8*x+x^2). (End)",
				"Limit_{n-\u003einfinity} a(n)/a(n-1) = 4 + sqrt(15). - _Gregory V. Richardson_, Oct 13 2002",
				"From _Mohamed Bouhamida_, Feb 07 2007: (Start)",
				"a(n) = 7*(a(n-1) + a(n-2)) - a(n-3).",
				"a(n) = 9*(a(n-1) - a(n-2)) + a(n-3). (End)",
				"[A070997(n-1), a(n)] = [1,6; 1,7]^n * [1,0]. - _Gary W. Adamson_, Mar 21 2008",
				"a(-n) = -a(n). - _Michael Somos_, Apr 05 2008",
				"a(n+1) = Sum_{k=0..n} A101950(n,k)*7^k. - _Philippe Deléham_, Feb 10 2012",
				"From _Peter Bala_, Dec 23 2012: (Start)",
				"Product_{n \u003e= 1} (1 + 1/a(n)) = (1/3)*(3 + sqrt(15)).",
				"Product_{n \u003e= 2} (1 - 1/a(n)) = (1/8)*(3 + sqrt(15)).",
				"(End)",
				"a(n) = A136325(n)/3. - _Greg Dresden_, Sep 12 2019"
			],
			"example": [
				"G.f. = x + 8*x^2 + 63*x^3 + 496*x^4 + 3905*x^5 + 30744*x^6 + 242047*x^7 + ..."
			],
			"maple": [
				"A001090:=1/(1-8*z+z**2); # _Simon Plouffe_ in his 1992 dissertation",
				"seq( simplify(ChebyshevU(n-1, 4)), n=0..20); # _G. C. Greubel_, Dec 23 2019"
			],
			"mathematica": [
				"Table[GegenbauerC[n-1, 1, 4]], {n,0,20}] (* _Vladimir Joseph Stephan Orlovsky_, Sep 11 2008 *)",
				"LinearRecurrence[{8,-1},{0,1},30] (* _Harvey P. Dale_, Aug 29 2012 *)",
				"a[n_]:= ChebyshevU[n-1, 4]; (* _Michael Somos_, May 28 2014 *)",
				"CoefficientList[Series[x/(1-8*x+x^2), {x,0,20}], x] (* _G. C. Greubel_, Dec 20 2017 *)"
			],
			"program": [
				"(PARI) {a(n) = subst(poltchebi(n+1) - 4 * poltchebi(n), x, 4) / 15}; /* _Michael Somos_, Apr 05 2008 */",
				"(PARI) {a(n) = polchebyshev(n-1, 2, 4)}; /* _Michael Somos_, May 28 2014 */",
				"(PARI) my(x='x+O('x^30)); concat([0], Vec(x/(1-8*x-x^2))) \\\\ _G. C. Greubel_, Dec 20 2017",
				"(Sage) [lucas_number1(n,8,1) for n in range(22)] # _Zerinvary Lajos_, Jun 25 2008",
				"(Sage) [chebyshev_U(n-1,4) for n in (0..20)] # _G. C. Greubel_, Dec 23 2019",
				"(MAGMA) I:=[0,1]; [n le 2 select I[n] else 8*Self(n-1) - Self(n-2): n in [1..30]]; (* _G. C. Greubel_, Dec 20 2017 *)",
				"(GAP) m:=4;; a:=[0,1];; for n in [3..20] do a[n]:=2*m*a[n-1]-a[n-2]; od; a; # _G. C. Greubel_, Dec 23 2019"
			],
			"xref": [
				"Cf. A001091, A001906, A004187, A004254, A070997.",
				"Equals one-third A136325.",
				"Chebyshev sequence U(n, m): A000027 (m=1), A001353 (m=2), A001109 (m=3), this sequence (m=4), A004189 (m=5), A004191 (m=6), A007655 (m=7), A077412 (m=8), A049660 (m=9), A075843 (m=10), A077421 (m=11), A077423 (m=12), A097309 (m=13), A097311 (m=14), A097313 (m=15), A029548 (m=16), A029547 (m=17), A144128 (m=18), A078987 (m=19), A097316 (m=33).",
				"Cf. A323182."
			],
			"keyword": "nonn,easy,changed",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Wolfdieter Lang_, Aug 02 2000"
			],
			"references": 52,
			"revision": 136,
			"time": "2022-01-05T00:35:05-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}