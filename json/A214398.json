{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A214398",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 214398,
			"data": "1,1,1,1,4,1,1,10,9,1,1,20,45,16,1,1,35,165,136,25,1,1,56,495,816,325,36,1,1,84,1287,3876,2925,666,49,1,1,120,3003,15504,20475,8436,1225,64,1,1,165,6435,54264,118755,82251,20825,2080,81,1,1,220,12870,170544",
			"name": "Triangle where the g.f. of column k is 1/(1-x)^(k^2) for k\u003e=1, as read by rows n\u003e=1.",
			"comment": [
				"This is also the array A(n,k) read upwards antidiagonals, where the entry in row n and column k counts the vertex-labeled digraphs with n arcs and k vertices, allowing multi-edges and multi-loops (labeled analog to A138107). The binomial formula counts the weak compositions of distributing n arcs over the k^2 positions in the adjacency matrix. - _R. J. Mathar_, Aug 03 2017"
			],
			"link": [
				"Paul D. Hanna, \u003ca href=\"/A214398/b214398.txt\"\u003eRows n = 0..45, flattened.\u003c/a\u003e",
				"R. J. Mathar, \u003ca href=\"http://arxiv.org/abs/1709.09000\"\u003eStatistics on Small Graphs\u003c/a\u003e, arXiv:1709.09000 (2017) Table 80."
			],
			"formula": [
				"T(n,k) = binomial(k^2+n-k-1, n-k).",
				"Row sums form A178325.",
				"Central terms form A214400.",
				"T(n,n-2) = A037270(n-2). - _R. J. Mathar_, Aug 03 2017",
				"T(n,n-3) = (n^2-6*n+11)*(n^2-6*n+10)*(n-3)^2 /6. - _R. J. Mathar_, Aug 03 2017"
			],
			"example": [
				"Triangle begins:",
				"1;",
				"1, 1;",
				"1, 4, 1;",
				"1, 10, 9, 1;",
				"1, 20, 45, 16, 1;",
				"1, 35, 165, 136, 25, 1;",
				"1, 56, 495, 816, 325, 36, 1;",
				"1, 84, 1287, 3876, 2925, 666, 49, 1;",
				"1, 120, 3003, 15504, 20475, 8436, 1225, 64, 1;",
				"1, 165, 6435, 54264, 118755, 82251, 20825, 2080, 81, 1;",
				"1, 220, 12870, 170544, 593775, 658008, 270725, 45760, 3321, 100, 1; ..."
			],
			"maple": [
				"A214398 := proc(n,k)",
				"    binomial(k^2+n-k-1,n-k) ;",
				"end proc:",
				"seq(seq(A214398(n,k),k=1..n),n=1..10) ; # _R. J. Mathar_, Aug 03 2017"
			],
			"mathematica": [
				"nmax = 11;",
				"T[n_, k_] := SeriesCoefficient[1/(1-x)^(k^2), {x, 0, n-k}];",
				"Table[T[n, k], {n, 1, nmax}, {k, 1, n}] // Flatten"
			],
			"program": [
				"(PARI) T(n,k)=binomial(k^2+n-k-1,n-k)",
				"for(n=1,11,for(k=1,n,print1(T(n,k),\", \"));print(\"\"))"
			],
			"xref": [
				"Cf. A214400 (central terms), A178325 (row sums), A054688, A000290 (1st subdiagonal), A037270 (2nd subdiagonal).",
				"Cf. A230049."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "1,5",
			"author": "_Paul D. Hanna_, Jul 15 2012",
			"references": 6,
			"revision": 25,
			"time": "2017-12-14T07:38:31-05:00",
			"created": "2012-07-15T16:32:01-04:00"
		}
	]
}