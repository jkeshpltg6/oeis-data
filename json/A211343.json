{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A211343",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 211343,
			"data": "1,2,3,1,4,0,5,2,6,0,1,7,3,0,8,0,0,9,4,2,10,0,0,1,11,5,0,0,12,0,3,0,13,6,0,0,14,0,0,2,15,7,4,0,1,16,0,0,0,0,17,8,0,0,0,18,0,5,3,0,19,9,0,0,0,20,0,0,0,2,21,10,6,0,0,1,22,0,0,4,0,0,23,11,0,0,0,0,24,0,7,0,0,0",
			"name": "Triangle read by rows: T(n,k), n \u003e= 1, k \u003e= 1, in which column k lists the positive integers interleaved with k-1 zeros, and the first element of column k is in row k(k+1)/2.",
			"comment": [
				"The number of positive terms in row n is A001227(n).",
				"If n = 2^j then the only positive integer in row n is T(n,1) = n",
				"If n is an odd prime then the only two positive integers in row n are T(n,1) = n and T(n,2) = (n - 1)/2.",
				"From _Omar E. Pol_, Apr 30 2017: (Start)",
				"Conjecture 1: T(n,k) is the smallest part of the partition of n into k consecutive parts, if T(n,k) \u003e 0.",
				"Conjecture 2: the last positive integer in the row n is in the column A109814(n). (End)"
			],
			"link": [
				"Robert Price, \u003ca href=\"/A211343/b211343.txt\"\u003eTable of n, a(n) for n = 1..28864\u003c/a\u003e (rows n = 1..1000, flattened)"
			],
			"formula": [
				"T(n,k) = floor((1 + A196020(n,k))/2).",
				"T(n,k) = A237048(n,k)*A286001(n,k). - _Omar E. Pol_, Aug 13 2018"
			],
			"example": [
				"Triangle begins:",
				"   1;",
				"   2;",
				"   3,  1;",
				"   4,  0;",
				"   5,  2;",
				"   6,  0,  1;",
				"   7,  3,  0;",
				"   8,  0,  0;",
				"   9,  4,  2;",
				"  10,  0,  0,  1;",
				"  11,  5,  0,  0;",
				"  12,  0,  3,  0;",
				"  13,  6,  0,  0;",
				"  14,  0,  0,  2;",
				"  15,  7,  4,  0,  1;",
				"  16,  0,  0,  0,  0;",
				"  17,  8,  0,  0,  0;",
				"  18,  0,  5,  3,  0;",
				"  19,  9,  0,  0,  0;",
				"  20,  0,  0,  0,  2;",
				"  21, 10,  6,  0,  0,  1;",
				"  22,  0,  0,  4,  0,  0;",
				"  23, 11,  0,  0,  0,  0;",
				"  24,  0,  7,  0,  0,  0;",
				"  25, 12,  0,  0,  3,  0;",
				"  26,  0,  0,  5,  0,  0;",
				"  27, 13,  8,  0,  0,  2;",
				"  28,  0,  0,  0,  0,  0,  1;",
				"  ...",
				"In accordance with the conjectures, for n = 15 there are four partitions of 15 into consecutive parts: [15], [8, 7], [6, 5, 4] and [5, 4, 3, 2, 1]. The smallest parts of these partitions are 15, 7, 4, 1, respectively, so the 15th row of the triangle is [15, 7, 4, 0, 1]. - _Omar E. Pol_, Apr 30 2017"
			],
			"mathematica": [
				"a196020[n_, k_]:=If[Divisible[n - k(k + 1)/2, k], 2n/k - k, 0]; T[n_, k_]:= Floor[(1 + a196020[n, k])/2]; Table[T[n, k], {n, 28}, {k, Floor[(Sqrt[8n+1]-1)/2]}] // Flatten (* _Indranil Ghosh_, Apr 30 2017 *)"
			],
			"program": [
				"(Python)",
				"from sympy import sqrt",
				"import math",
				"def a196020(n, k):return 2*n/k - k if (n - k*(k + 1)/2)%k == 0 else 0",
				"def T(n, k): return int((1 + a196020(n, k))/2)",
				"for n in range(1, 29): print([T(n, k) for k in range(1, int((sqrt(8*n + 1) - 1)/2) + 1)]) # _Indranil Ghosh_, Apr 30 2017"
			],
			"xref": [
				"Columns 1-3: A000027, A027656, A175676.",
				"Column k starts in row A000217(k).",
				"Row n has length A003056(n).",
				"Cf. A000203, A001227, A196020, A212119, A235791, A236104, A237048, A237591, A237593, A286001."
			],
			"keyword": "nonn,tabf,look",
			"offset": "1,2",
			"author": "_Omar E. Pol_, Feb 05 2013",
			"references": 24,
			"revision": 54,
			"time": "2021-03-12T07:51:47-05:00",
			"created": "2013-02-11T16:15:21-05:00"
		}
	]
}