{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A329726",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 329726,
			"data": "2,4,6,2,10,12,2,16,18,2,22,4,2,28,30,2,2,36,2,40,42,4,46,6,2,52,2,2,58,60,2,8,66,2,70,72,2,2,78,2,82,8,2,88,18,2,2,96,2,100,102,8,106,108,2,112,2,4,2,10,2,4,126,2,130,18,2,136,138,2,2,8,2",
			"name": "Number of witnesses for Solovay-Strassen primality test of 2*n+1.",
			"comment": [
				"Number of bases b, 1 \u003c= b \u003c= 2*n, such that GCD(b, 2*n+1) = 1 and b^n == (b / 2*n+1) (mod 2*n+1), where (b / 2*n+1) is a Jacobi symbol.",
				"If 2*n+1 is composite then it is the number of bases b, 1 \u003c= b \u003c= 2*n, in which 2*n+1 is an Euler-Jacobi pseudoprime.",
				"Differs from A071294 from n = 22."
			],
			"reference": [
				"Paulo Ribenboim, The Little Book of Bigger Primes, 2nd ed., Springer-Verlag, New York, 2004, p. 96."
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A329726/b329726.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Louis Monier, \u003ca href=\"https://doi.org/10.1016/0304-3975(80)90007-9\"\u003eEvaluation and comparison of two efficient primality testing algorithms\u003c/a\u003e, Theoretical Computer Science, Vol. 11 (1980), pp. 97-108.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Euler-JacobiPseudoprime.html\"\u003eEuler-Jacobi Pseudoprime\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Euler-Jacobi_pseudoprime\"\u003eEuler-Jacobi pseudoprime\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Solovay%E2%80%93Strassen_primality_test\"\u003eSolovay-Strassen primality test\u003c/a\u003e."
			],
			"formula": [
				"a(n) = delta(n) * Product_{p|n} gcd((n-1)/2, p-1), where delta(n) = 2 if nu(n-1, 2) = min_{p|n} nu(p-1, 2), 1/2 if there is a prime p|n such that nu(p, n) is odd and nu(p-1, 2) \u003c nu(n-1, 2), and 1 otherwise, where nu(n, p) is the exponent of the highest power of p dividing n.",
				"a(p) = p-1 for prime p."
			],
			"example": [
				"a(1) = 2 since there are 2 bases b in which 2*1 + 1 = 3 is an Euler-Jacobi pseudoprime: b = 1 since GCD(1, 3) = 1 and 1^1 == (1 / 3) == 1 (mod 3), and b = 2 since GCD(2, 3) = 1 and 2^1 == (2 / 3) == -1 (mod 3)."
			],
			"mathematica": [
				"v[n_] := Min[IntegerExponent[#, 2]\u0026 /@ (FactorInteger[n][[;;, 1]] - 1)];",
				"pQ[n_, p_] := OddQ[IntegerExponent[n, p]] \u0026\u0026 IntegerExponent[p-1, 2] \u003c IntegerExponent[n-1, 2];",
				"psQ[n_] := AnyTrue[FactorInteger[n][[;;, 1]], pQ[n, #] \u0026];",
				"delta[n_] := If[IntegerExponent[n-1, 2] == v[n], 2, If[psQ[n], 1/2, 1]];",
				"a[n_] := delta[n] * Module[{p = FactorInteger[n][[;;, 1]]}, Product[GCD[(n-1)/2, p[[k]]-1], {k, 1, Length[p]}]];",
				"Table[a[n], {n, 3, 147, 2}]"
			],
			"xref": [
				"Cf. A006093, A007324, A007814, A047713, A063994, A071294."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Amiram Eldar_, Nov 20 2019",
			"references": 2,
			"revision": 30,
			"time": "2019-11-21T04:18:31-05:00",
			"created": "2019-11-20T19:44:02-05:00"
		}
	]
}