{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A262968",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 262968,
			"data": "1,2,4,8,14,24,38,60,92,138,204,296,424,600,840,1164,1598,2176,2940,3944,5256,6960,9164,12000,15634,20270,26160,33616,43020,54840,69648,88140,111164,139748,175136,218832,272646,338760,419792,518880,639780,786976,965820",
			"name": "Expansion of phi(-q^6) / phi(-q) in powers of q where phi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A262968/b262968.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of eta(q^2) * eta(q^6)^2 / (eta(q)^2 * eta(q^12)) in powers of q.",
				"Euler transform of period 12 sequence [ 2, 1, 2, 1, 2, -1, 2, 1, 2, 1, 2, 0, ...].",
				"a(n) = A262967(3*n).",
				"a(n) ~ 5^(1/4) * exp(sqrt(5*n/6)*Pi) / (2^(9/4) * 3^(3/4) * n^(3/4)). - _Vaclav Kotesovec_, Oct 06 2015"
			],
			"example": [
				"G.f. = 1 + 2*q + 4*q^2 + 8*q^3 + 14*q^4 + 24*q^5 + 38*q^6 + 60*q^7 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 4, 0, q^6] / EllipticTheta[ 4, 0, q], {q, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A) * eta(x^6 + A)^2 / (eta(x + A)^2 * eta(x^12 + A)), n))};"
			],
			"xref": [
				"Cf. A262967."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Michael Somos_, Oct 05 2015",
			"references": 2,
			"revision": 11,
			"time": "2021-03-12T22:24:48-05:00",
			"created": "2015-10-05T19:52:56-04:00"
		}
	]
}