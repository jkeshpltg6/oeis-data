{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A034801",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 34801,
			"data": "1,1,1,1,3,1,1,8,8,1,1,21,56,21,1,1,55,385,385,55,1,1,144,2640,6930,2640,144,1,1,377,18096,124410,124410,18096,377,1,1,987,124033,2232594,5847270,2232594,124033,987,1,1,2584,850136,40062659,274715376,274715376,40062659,850136,2584,1",
			"name": "Triangle of Fibonomial coefficients (k=2).",
			"reference": [
				"A. Brousseau, Fibonacci and Related Number Theoretic Tables. Fibonacci Association, San Jose, CA, 1972, p. 88."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A034801/b034801.txt\"\u003eRows n = 0..100 of triangle, flattened\u003c/a\u003e",
				"C. Pita, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL14/Pita/pita12.html\"\u003eOn s-Fibonomials\u003c/a\u003e, J. Int. Seq. 14 (2011) # 11.3.7.",
				"C. J. Pita Ruiz Velasco, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL14/Pita2/pita8.html\"\u003eSums of Products of s-Fibonacci Polynomial Sequences\u003c/a\u003e, J. Int. Seq. 14 (2011) # 11.7.6."
			],
			"formula": [
				"Fibonomial coefficients formed from sequence F_3k [ 2, 8, 34, ... ].",
				"T(n, k) = Product_{j=0..k-1} Fibonacci(2*(n-j)) / Product_{j=1..k} Fibonacci(2*j)."
			],
			"example": [
				"Triangle begins as:",
				"  1;",
				"  1,   1;",
				"  1,   3,      1;",
				"  1,   8,      8,       1;",
				"  1,  21,     56,      21,       1;",
				"  1,  55,    385,     385,      55,       1;",
				"  1, 144,   2640,    6930,    2640,     144,      1;",
				"  1, 377,  18096,  124410,  124410,   18096,    377,   1;",
				"  1, 987, 124033, 2232594, 5847270, 2232594, 124033, 987, 1;"
			],
			"maple": [
				"A034801 := proc(n,k)",
				"    mul(combinat[fibonacci](2*n-2*j),j=0..k-1) /",
				"    mul(combinat[fibonacci](2*j),j=1..k) ;",
				"end proc: # _R. J. Mathar_, Sep 02 2017"
			],
			"mathematica": [
				"F[n_, k_, q_]:= Product[Fibonacci[q*(n-j+1)]/Fibonacci[q*j], {j,k}];",
				"Table[F[n, k, 2], {n,0,10}, {k,0,n}]//Flatten (* _G. C. Greubel_, Nov 13 2019 *)"
			],
			"program": [
				"(PARI) F(n,k,q) = f=fibonacci; prod(j=1,k, f(q*(n-j+1))/f(q*j)); \\\\ _G. C. Greubel_, Nov 13 2019",
				"(Sage)",
				"def F(n,k,q):",
				"    if (n==0 and k==0): return 1",
				"    else: return product(fibonacci(q*(n-j+1))/fibonacci(q*j) for j in (1..k))",
				"[[F(n,k,2) for k in (0..n)] for n in (0..10)] # _G. C. Greubel_, Nov 13 2019",
				"(GAP)",
				"F:= function(n,k,q)",
				"    if n=0 and k=0 then return 1;",
				"    else return Product([1..k], j-\u003e Fibonacci(q*(n-j+1))/Fibonacci(q*j));",
				"    fi;",
				"  end;",
				"Flat(List([0..10], n-\u003e List([0..n], k-\u003e F(n,k,2) ))); # _G. C. Greubel_, Nov 13 2019"
			],
			"xref": [
				"Cf. A010048."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _James A. Sellers_, Feb 09 2000"
			],
			"references": 8,
			"revision": 17,
			"time": "2019-11-13T15:01:17-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}