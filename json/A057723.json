{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A057723",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 57723,
			"data": "1,2,3,6,5,6,7,14,12,10,11,18,13,14,15,30,17,24,19,30,21,22,23,42,30,26,39,42,29,30,31,62,33,34,35,72,37,38,39,70,41,42,43,66,60,46,47,90,56,60,51,78,53,78,55,98,57,58,59,90,61,62,84,126,65,66,67,102,69,70",
			"name": "Sum of positive divisors of n that are divisible by every prime that divides n.",
			"link": [
				"Ivan Neretin, \u003ca href=\"/A057723/b057723.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"If n = Product p_i^e_i then a(n) = Product (p_i + p_i^2 + ... + p_i^e_i).",
				"a(n) = rad(n)*sigma(n/rad(n)) = A007947(n)*A000203(A003557(n)). - _Ivan Neretin_, May 13 2015",
				"Dirichlet g.f.: zeta(s) * zeta(s-1) * Product(p prime, 1 - p^(-s) + p^(1-2*s)). - _Robert Israel_, May 13 2015",
				"Sum_{k=1..n} a(k) ~ c * Pi^2 * n^2 / 12, where c = A330596 = Product_{primes p} (1 - 1/p^2 + 1/p^3) = 0.7485352596823635646442150486379106016416403430053244045... - _Vaclav Kotesovec_, Dec 18 2019",
				"a(n) = Sum_{d|n, rad(d)=rad(n)} d. - _R. J. Mathar_, Jun 02 2020",
				"Lim_{n-\u003eoo} (1/n) * Sum_{k=1..n} a(k)/k = Product_{p prime}(1 + 1/(p*(p^2-1))) = 1.231291... (A065487). - _Amiram Eldar_, Jun 10 2020",
				"a(n) = Sum_{d|n, gcd(d, n/d) = 1} (-1)^omega(n/d) * sigma(d). - _Ilya Gutkovskiy_, Apr 15 2021"
			],
			"example": [
				"The divisors of 12 that are divisible by both 2 and 3 are 6 and 12. So a(12) = 6 + 12 = 18."
			],
			"maple": [
				"seq(mul(f[1]*(f[1]^f[2]-1)/(f[1]-1), f = ifactors(n)[2]), n = 1 .. 100); # _Robert Israel_, May 13 2015"
			],
			"mathematica": [
				"Table[(b = Times @@ FactorInteger[n][[All, 1]])*DivisorSigma[1, n/b], {n, 70}] (* _Ivan Neretin_, May 13 2015 *)"
			],
			"program": [
				"(MAGMA) [\u0026*PrimeDivisors(n)*SumOfDivisors(n div \u0026*PrimeDivisors(n)): n in [1..70]]; // _Vincenzo Librandi_, May 14 2015",
				"(PARI) a(n) = {my(f = factor(n)); for (i=1, #f~, f[i,2]=1); my(pp = factorback(f)); sumdiv(n, d, if (! (d % pp), d, 0));} \\\\ _Michel Marcus_, May 14 2015"
			],
			"xref": [
				"Row sums of triangle A284318.",
				"Cf. A000203 (sigma), A007947 (rad), A005361 (number of these divisors).",
				"Cf. A049060 and A060640 (other sigma-like functions).",
				"Cf. A065487, A330596."
			],
			"keyword": "nonn,mult",
			"offset": "1,2",
			"author": "_Leroy Quet_, Oct 27 2000",
			"references": 37,
			"revision": 37,
			"time": "2021-04-15T21:40:55-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}