{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A122652",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 122652,
			"data": "0,4,40,396,3920,38804,384120,3802396,37639840,372596004,3688320200,36510605996,361417739760,3577666791604,35415250176280,350574834971196,3470333099535680,34352756160385604,340057228504320360,3366219528882817996,33322138060323859600",
			"name": "a(0) = 0, a(1) = 4; for n \u003e 1, a(n) = 10*a(n-1) - a(n-2).",
			"comment": [
				"Kekulé numbers for the benzenoids P_2(n).",
				"a(n) are the values of m where A032528(m) - 1 has integer square roots. The roots are given by A001079. - _Richard R. Forberg_, Aug 05 2013",
				"Numbers n such that 6*n^2 + 4 is a square. - _Colin Barker_, Mar 17 2014"
			],
			"reference": [
				"S. J. Cyvin and I. Gutman, Kekulé structures in benzenoid hydrocarbons, Lecture Notes in Chemistry, No. 46, Springer, New York, 1988 (p. 283, K{P_2(n)})."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A122652/b122652.txt\"\u003eTable of n, a(n) for n = 0..1004\u003c/a\u003e",
				"Andersen, K., Carbone, L. and Penta, D., \u003ca href=\"https://pdfs.semanticscholar.org/8f0c/c3e68d388185129a56ed73b5d21224659300.pdf\"\u003eKac-Moody Fibonacci sequences, hyperbolic golden ratios, and real quadratic fields\u003c/a\u003e, Journal of Number Theory and Combinatorics, Vol 2, No. 3 pp 245-278, 2011. See Section 9.",
				"Hacène Belbachir, Soumeya Merwa Tebtoub, and László Németh, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL23/Nemeth/nemeth7.html\"\u003eEllipse Chains and Associated Sequences\u003c/a\u003e, J. Int. Seq., Vol. 23 (2020), Article 20.8.5.",
				"John M. Campbell, \u003ca href=\"http://arxiv.org/abs/1105.3399\"\u003eAn Integral Representation of Kekulé Numbers, and Double Integrals Related to Smarandache Sequences\u003c/a\u003e, arXiv preprint arXiv:1105.3399 [math.GM], 2011.",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (10,-1)."
			],
			"formula": [
				"a(n) = (1/6)*(5 + 2*sqrt(6))^n*sqrt(6) - (1/6)*sqrt(6)*(5 - 2*sqrt(6))^n, with n \u003e= 0. - _Paolo P. Lava_, Oct 02 2008",
				"G.f.: 4*x/(1 - 10*x + x^2). - _Philippe Deléham_, Nov 17 2008",
				"3*a(n)^2 + 2 = 2*A001079(n)^2. - _Charlie Marion_, Feb 01 2013",
				"a(n) = (2*arcsinh(sqrt(2))*sinh(2*n*arcsinh(sqrt(2)))/log(sqrt(2) + sqrt(3)))/sqrt(6). - _Artur Jasinski_, Aug 09 2016",
				"a(n) = 2*A001078(n). - _Bruno Berselli_, Nov 25 2016",
				"E.g.f.: sqrt(6)*exp(5*x)*sinh(2*sqrt(6)*x)/3. - _Franck Maminirina Ramaharo_, Jan 07 2019"
			],
			"mathematica": [
				"CoefficientList[Series[(4 z)/(z^2 - 10 z + 1), {z, 0, 200}], z] (* _Vladimir Joseph Stephan Orlovsky_, Jun 11 2011 *)",
				"LinearRecurrence[{10, -1}, {0, 4}, 21] (* _Jean-François Alcover_, Jan 07 2019 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c2,(n%2)*4,10*a(n-1)-a(n-2)) \\\\ _Benoit Cloitre_, Sep 23 2006"
			],
			"xref": [
				"Cf. A001078, A001079, A032528."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Sep 21 2006",
			"ext": [
				"More terms and better definition from _Benoit Cloitre_, Sep 23 2006"
			],
			"references": 4,
			"revision": 65,
			"time": "2021-01-05T19:06:03-05:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}