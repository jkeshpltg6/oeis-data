{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A027862",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 27862,
			"data": "5,13,41,61,113,181,313,421,613,761,1013,1201,1301,1741,1861,2113,2381,2521,3121,3613,4513,5101,7321,8581,9661,9941,10513,12641,13613,14281,14621,15313,16381,19013,19801,20201,21013,21841,23981,24421,26681",
			"name": "Primes of the form n^2 + (n+1)^2.",
			"comment": [
				"Also, primes of the form 4*k+1 which are the hypotenuse of one and only one right triangle with integral arms. - _Cino Hilliard_, Mar 16 2003",
				"Centered square primes (i.e., prime terms of centered squares A001844). - _Lekraj Beedassy_, Jan 21 2005",
				"Primes of the form 2*k*(k-1)+1. - _Juri-Stepan Gerasimov_, Apr 27 2010",
				"Equivalently, primes of the form (m^2+1)/2 (take m=2*n+1). These primes a(n) have nontrivial solutions of x^2==1 (Modd a(n)) given by x=x(n)=A002731(n). For Modd n see a comment on A203571. See also A206549 for such solutions for primes of the form 4*k+1, given in A002144.",
				"  E.g., a(3)=41, A002731(3)=9, 9^2=81, floor(81/41)=1 (odd),",
				"  -81 = -2*41 + 1 == 1(mod 41), hence 9^2==1(Modd 41). - _Wolfdieter Lang_, Feb 24 2012",
				"Also primes of the form 4*k+1 that are the smallest side length of one and only one integer Soddyian triangle (see A230812). - _Frank M Jackson_, Mar 13 2014",
				"Also, primes of the form (m^2+1)/2. - _Zak Seidov_, May 01 2014",
				"Note that ((2n+1)^2 + 1)/2 = n^2 + (n+1)^2. - _Thomas Ordowski_, May 25 2015",
				"Primes p such that 2p-1 is a square. - _Thomas Ordowski_, Aug 27 2016"
			],
			"reference": [
				"D. M. Burton, Elementary Number Theory, Allyn and Bacon Inc. Boston, MA, 1976, p. 271.",
				"Morris Kline, Mathematical Thought from Ancient to Modern Times, 1972. pp. 275.",
				"Daniel Shanks, An analytic criterion for the existence of infinitely many primes of the form 1/2 * (n^2 + 1), Illinois Journal of Mathematics 8:3 (1964), p. 377-379."
			],
			"link": [
				"T. D. Noe and Zak Seidov, \u003ca href=\"/A027862/b027862.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"P. De Geest, \u003ca href=\"http://www.worldofnumbers.com/index.html\"\u003eWorld!Of Numbers\u003c/a\u003e",
				"W. Sierpiński, \u003ca href=\"http://gdz.sub.uni-goettingen.de/dms/load/img/?PPN=GDZPPN002075016\u0026amp;IDDOC=243194\"\u003eSur les nombres triangulaires qui sont sommes de deux nombres triangulaires\u003c/a\u003e, Elem. Math., 17 (1962), pp. 63-65.",
				"Panayiotis G. Tsangaris, \u003ca href=\"http://ami.ektf.hu/uploads/papers/finalpdf/AAPASM_25_from39to53.pdf\"\u003eA sieve for all primes of the form x^2 + (x+1)^2\u003c/a\u003e, Acta Academiae Paedagogicae Agriensis, Sectio Mathematicae, 25 (1998), pp. 39-53."
			],
			"formula": [
				"a(n) = ((A002731(n)^2 - 1) / 2) + 1. - _Torlach Rush_, Mar 14 2014",
				"a(n) = ((A002731(n)^2 + 1) / 2). - _Zak Seidov_, May 01 2014"
			],
			"example": [
				"13 is in the sequence because it is prime and 13 = 2^2 + 3^2. - _Michael B. Porter_, Aug 27 2016"
			],
			"mathematica": [
				"Select[Table[n^2+(n+1)^2,{n,200}],PrimeQ] (* _Harvey P. Dale_, Aug 22 2012 *)",
				"Select[Total/@Partition[Range[200]^2,2,1],PrimeQ] (* _Harvey P. Dale_, Apr 20 2016 *)"
			],
			"program": [
				"(PARI) je=[]; for(n=1,500, if(isprime(n^2+(n+1)^2),je=concat(je,n^2+(n+1)^2))); je",
				"(PARI) fermat(n) = { for(x=1,n, y=2*x*(x+1)+1; if(isprime(y),print1(y\" \")) ) }",
				"(MAGMA) [ a: n in [0..150] | IsPrime(a) where a is n^2+(n+1)^2 ]; // _Vincenzo Librandi_, Dec 18 2010"
			],
			"xref": [
				"Primes p such that A079887(p) = 1.",
				"Primes arising in A002731, A027861 gives n values, A091277 gives prime index."
			],
			"keyword": "nonn,easy,nice",
			"offset": "1,1",
			"author": "_Patrick De Geest_",
			"ext": [
				"More terms from _Cino Hilliard_, Mar 16 2003"
			],
			"references": 40,
			"revision": 85,
			"time": "2019-11-03T10:11:01-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}