{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000146",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 146,
			"id": "M1717 N0680",
			"data": "1,1,1,1,1,1,2,-6,56,-528,6193,-86579,1425518,-27298230,601580875,-15116315766,429614643062,-13711655205087,488332318973594,-19296579341940067,841693047573682616,-40338071854059455412,2115074863808199160561,-120866265222965259346026",
			"name": "From von Staudt-Clausen representation of Bernoulli numbers: a(n) = Bernoulli(2n) + Sum_{(p-1)|2n} 1/p.",
			"comment": [
				"The von Staudt-Clausen theorem states that this number is always an integer."
			],
			"reference": [
				"G. H. Hardy and E. M. Wright, An Introduction to the Theory of Numbers, 5th ed., Oxford Univ. Press, 1979, Th. 118.",
				"Max Koecher, Klassische elementare Analysis, Birkhäuser, Basel, Boston, 1987, pp. 168-170.",
				"H. Rademacher, Topics in Analytic Number Theory, Springer, 1973, Section 5.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe and Seiichi Manyama, \u003ca href=\"/A000146/b000146.txt\"\u003eTable of n, a(n) for n = 1..317\u003c/a\u003e (first 100 terms from T. D. Noe)",
				"Joerg Arndt, \u003ca href=\"/A000146/a000146.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e (contains terms with more than 1000 decimal digits)",
				"Daniel Hoyt, \u003ca href=\"/A000146/a000146_1.txt\"\u003ePython 3 program for A000146.\u003c/a\u003e",
				"Donald E. Knuth and Thomas J. Buckholtz, \u003ca href=\"/A000182/a000182.pdf\"\u003eComputation of tangent, Euler and Bernoulli numbers\u003c/a\u003e, Math. Comp. 21 1967 663-688. [Annotated scanned copy]",
				"Donald E. Knuth and Thomas J. Buckholtz, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1967-0221735-9\"\u003eComputation of tangent, Euler and Bernoulli numbers\u003c/a\u003e, Math. Comp. 21 1967 663-688.",
				"R. Mestrovic, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Mestrovic/mes4.html\"\u003eOn a Congruence Modulo n^3 Involving Two Consecutive Sums of Powers\u003c/a\u003e, Journal of Integer Sequences, Vol. 17 (2014), 14.8.4.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/vonStaudt-ClausenTheorem.html\"\u003evon Staudt-Clausen Theorem\u003c/a\u003e",
				"\u003ca href=\"/index/Be#Bernoulli\"\u003eIndex entries for sequences related to Bernoulli numbers.\u003c/a\u003e"
			],
			"maple": [
				"A000146 := proc(n) local a ,i,p; a := bernoulli(2*n) ;for i from 1 do p := ithprime(i) ; if (2*n) mod (p-1) = 0 then a := a+1/p ; elif p-1 \u003e 2*n then break; end if; end do: a ; end proc: # _R. J. Mathar_, Jul 08 2011"
			],
			"mathematica": [
				"Table[ BernoulliB[2 n] + Total[ 1/Select[ Prime /@ Range[n+1], Divisible[2n, #-1] \u0026]], {n, 1, 22}] (* _Jean-François Alcover_, Oct 12 2011 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c1,0,sumdiv(2*n,d, isprime(d+1)/(d+1))+bernfrac(2*n))"
			],
			"xref": [
				"Cf. also A002882, A003245, A127187, A127188."
			],
			"keyword": "sign,nice,easy",
			"offset": "1,7",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Signs courtesy of Antreas P. Hatzipolakis (xpolakis(AT)hol.gr)",
				"More terms from _Michael Somos_"
			],
			"references": 10,
			"revision": 62,
			"time": "2020-12-04T19:12:12-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}