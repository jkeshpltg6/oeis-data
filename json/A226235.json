{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A226235",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 226235,
			"data": "1,-12,66,-220,495,-804,1068,-1596,3279,-6952,12276,-17844,23653,-34080,57168,-98428,154332,-215724,285388,-395784,600459,-931888,1365696,-1853076,2426189,-3277896,4689534,-6815008,9538632,-12664440,16403188,-21690876,29812932",
			"name": "Expansion of q * (chi(-q) / chi(-q^3))^12 in powers of q where chi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A226235/b226235.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Richard Moy, \u003ca href=\"http://arxiv.org/abs/1309.4320\"\u003eCongruences among power series coefficients of modular forms\u003c/a\u003e, arXiv:1309.4320 [math.NT], 2013.",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e",
				"Wenzhe Yang, \u003ca href=\"https://arxiv.org/abs/1911.02608\"\u003eApéry's irrationality proof, mirror symmetry and Beukers' modular forms\u003c/a\u003e, arXiv:1911.02608 [math.NT], 2019."
			],
			"formula": [
				"Expansion of q * (f(-q, -q^5) / f(-q^6))^12 in powers of q where f() is a Ramanujan theta function.",
				"Expansion of ((c(q^2) * b(q)) / (c(q) * b(q^2)))^3 in powers of q where b() and c() are cubic AGM theta functions.",
				"Expansion of (eta(q) * eta(q^6) / (eta(q^2) * eta(q^3)))^12 in powers of q.",
				"Euler transform of period 6 sequence [ -12, 0, 0, 0, -12, 0, ...].",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2), A(x^4)) where f(u, v, w)= (v - u^2) * (v - w^2) - u*w * (24*(1 + v^2) + 152*v).",
				"G.f. A(x) satisfies f(x) = g(A(x)) where f, g are the g.f. for A006353 and A005259.",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (6 t)) = f(t) where q = exp(2 Pi i t).",
				"G.f.: x * (Product_{k\u003e0} 1 - x^k + x^(2*k))^12 where 1 - x + x^2 is the 6th cyclotomic polynomial.",
				"Convolution inverse of A121665. Convolution 12th power of A109389.",
				"a(n) ~ (-1)^(n+1) * exp(2*Pi*sqrt(n/3)) / (2*3^(1/4)*n^(3/4)). - _Vaclav Kotesovec_, Mar 30 2017",
				"Empirical: Sum_{n\u003e=1} a(n)/exp(2*Pi*n) = 11 + 5*sqrt(3) - sqrt(189 + 114*sqrt(3)). - _Simon Plouffe_, Mar 02 2021"
			],
			"example": [
				"G.f. = q - 12*q^2 + 66*q^3 - 220*q^4 + 495*q^5 - 804*q^6 + 1068*q^7 - 1596*q^8 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ q (QPochhammer[ q] QPochhammer[ q^6] / (QPochhammer[ q^2] QPochhammer[ q^3]))^12, {q, 0, n}]",
				"nmax = 50; CoefficientList[Series[Product[((1 + x^(3*k))/(1 + x^k))^12, {k, 1, nmax}], {x, 0, nmax}], x] (* _Vaclav Kotesovec_, Mar 30 2017 *)"
			],
			"program": [
				"(PARI) {a(n) = local(A); if( n\u003c1, 0, n--; A = x * O(x^n); polcoeff( (eta(x + A) * eta(x^6 + A) / (eta(x^2 + A) * eta(x^3 + A)))^12, n))}"
			],
			"xref": [
				"Cf. A005259, A006353, A109389, A121665."
			],
			"keyword": "sign",
			"offset": "1,2",
			"author": "_Michael Somos_, Sep 18 2013",
			"references": 7,
			"revision": 35,
			"time": "2021-03-12T05:55:46-05:00",
			"created": "2013-09-18T12:51:39-04:00"
		}
	]
}