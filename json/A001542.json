{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001542",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1542,
			"id": "M2030 N0802",
			"data": "0,2,12,70,408,2378,13860,80782,470832,2744210,15994428,93222358,543339720,3166815962,18457556052,107578520350,627013566048,3654502875938,21300003689580,124145519261542,723573111879672",
			"name": "a(n) = 6*a(n-1) - a(n-2) for n \u003e 1, a(0)=0 and a(1)=2.",
			"comment": [
				"Consider the equation core(x) = core(2x+1) where core(x) is the smallest number such that x*core(x) is a square: solutions are given by a(n)^2, n \u003e 0. - _Benoit Cloitre_, Apr 06 2002",
				"Terms \u003e 0 give numbers k which are solutions to the inequality |round(sqrt(2)*k)/k - sqrt(2)| \u003c 1/(2*sqrt(2)*k^2). - _Benoit Cloitre_, Feb 06 2006",
				"Also numbers n such that A125650(6*n^2) is an even perfect square, where A124650(n) is a numerator of n(n+3)/(4(n+1)(n+2)) = Sum_{k=1..n} 1/(k*(k+1)*(k+2)). Sequence of numbers 6*n^2 is a bisection of A125651(n). - _Alexander Adamchuk_, Nov 30 2006",
				"The upper principal convergents to 2^(1/2), beginning with 3/2, 17/12, 99/70, 577/408, comprise a strictly decreasing sequence; essentially, numerators = A001541 and denominators = A001542. - _Clark Kimberling_, Aug 26 2008",
				"Even Pell numbers. - _Omar E. Pol_, Dec 10 2008",
				"Numbers n such that 2*n^2+1 is a square. - _Vladimir Joseph Stephan Orlovsky_, Feb 19 2010",
				"These are the integer square roots of the Half-Squares, A007590(n), which occur at values of n given by A001541. Also the numbers produced by adding m + sqrt(floor(m^2/2) + 1) when m = A002315. See array in A227972. - _Richard R. Forberg_, Aug 31 2013",
				"A001541(n)/a(n) is the closest rational approximation of sqrt(2) with a denominator not larger than a(n), and 2*a(n)/A001541(n) is the closest rational approximation of sqrt(2) with a numerator not larger than 2*a(n). These rational approximations together with those obtained from the sequences A001653 and A002315 give a complete set of closest rational approximations of sqrt(2) with restricted numerator as well as denominator. - _A.H.M. Smeets_, May 28 2017",
				"Conjecture: Numbers n such that c/m \u003c n for all natural a^2 + b^2 = c^2 (Pythagorean triples), a \u003c b \u003c c and a+b+c = m. Numbers which correspondingly minimize c/m are A002939. - _Lorraine Lee_, Jan 31 2020"
			],
			"reference": [
				"Jay Kappraff, Beyond Measure, A Guided Tour Through Nature, Myth and Number, World Scientific, 2002; p. 480-481.",
				"Thomas Koshy, Fibonacci and Lucas Numbers with Applications, 2001, Wiley, p. 77-79.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A001542/b001542.txt\"\u003eTable of n, a(n) for n = 0..100\u003c/a\u003e",
				"I. Adler, \u003ca href=\"http://www.fq.math.ca/Scanned/7-2/adler.pdf\"\u003eThree Diophantine equations - Part II\u003c/a\u003e, Fib. Quart., 7 (1969), 181-193.",
				"Christian Aebi and Grant Cairns, \u003ca href=\"https://arxiv.org/abs/2006.07566\"\u003eLattice Equable Parallelograms\u003c/a\u003e, arXiv:2006.07566 [math.NT], 2020.",
				"Hacène Belbachir, Soumeya Merwa Tebtoub, László Németh, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL23/Nemeth/nemeth7.html\"\u003eEllipse Chains and Associated Sequences\u003c/a\u003e, J. Int. Seq., Vol. 23 (2020), Article 20.8.5.",
				"H. Brocard, \u003ca href=\"https://gdz.sub.uni-goettingen.de/id/PPN598948236_0004?tify={%22pages%22:[186],%22view%22:%22info%22}\"\u003eNotes élémentaires sur le problème de Peel\u003c/a\u003e, Nouvelle Correspondance Mathématique, 4 (1878), 161-169.",
				"S. Falcon, \u003ca href=\"http://dx.doi.org/10.4236/am.2014.515216\"\u003eRelationships between Some k-Fibonacci Sequences, Applied Mathematics\u003c/a\u003e, 2014, 5, 2226-2234.",
				"R. J. Hetherington, \u003ca href=\"/A000129/a000129.pdf\"\u003eLetter to N. J. A. Sloane, Oct 26 1974\u003c/a\u003e",
				"J. M. Katri and D. R. Byrkit, \u003ca href=\"http://www.jstor.org/stable/2313820\"\u003eProblem E1976\u003c/a\u003e, Amer. Math. Monthly, 75 (1968), 683-684.",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"E. Kilic, Y. T. Ulutas, and N. Omur, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL14/Omur/omur6.html\"\u003eA Formula for the Generating Functions of Powers of Horadam's Sequence with Two Additional Parameters\u003c/a\u003e, J. Int. Seq. 14 (2011) #11.5.6, Table 3, k=1.",
				"D. H. Lehmer, \u003ca href=\"http://www.jstor.org/stable/1968268\"\u003eOn the multiple solutions of the Pell equation\u003c/a\u003e, Annals Math., 30 (1928), 66-72.",
				"D. H. Lehmer, \u003ca href=\"/A001542/a001542.pdf\"\u003eOn the multiple solutions of the Pell equation\u003c/a\u003e (annotated scanned copy)",
				"Mathematical Reflections, \u003ca href=\"https://www.awesomemath.org/wp-pdf-files/math-reflections/mr-2013-05/mr_4_2013_solutions.pdf\"\u003eSolution to Problem O271\u003c/a\u003e, Issue 5, 2013, p 22.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"B. Polster and M. Ross, \u003ca href=\"https://arxiv.org/abs/1503.04658\"\u003eMarching in squares\u003c/a\u003e, arXiv preprint arXiv:1503.04658 [math.HO], 2015.",
				"Mark A. Shattuck, \u003ca href=\"https://www.emis.de/journals/INTEGERS/papers/j5/j5.Abstract.html\"\u003eTiling proofs of some formulas for the Pell numbers of odd index\u003c/a\u003e, Integers, 9 (2009), 53-64.",
				"R. A. Sulanke, \u003ca href=\"https://math.boisestate.edu/~sulanke/PAPERS/cutpasteII.pdf\"\u003eMoments, Narayana numbers and the cut and paste for lattice paths\u003c/a\u003e",
				"Soumeya M. Tebtoub, Hacène Belbachir, and László Németh, \u003ca href=\"https://hal.archives-ouvertes.fr/hal-02918958/document#page=18\"\u003eInteger sequences and ellipse chains inside a hyperbola\u003c/a\u003e, Proceedings of the 1st International Conference on Algebras, Graphs and Ordered Sets (ALGOS 2020), hal-02918958 [math.cs], 17-18.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (6,-1)."
			],
			"formula": [
				"a(n) = 2*A001109(n).",
				"a(n) = ((3+2*sqrt(2))^n - (3-2*sqrt(2))^n) / (2*sqrt(2)).",
				"G.f.: 2*x/(1-6*x+x^2).",
				"a(n) = sqrt{2*(A001541(n))^2 - 2}/2. - _Barry E. Williams_, May 07 2000",
				"a(n) = (C^(2n) - C^(-2n))/sqrt(8) where C = sqrt(2) + 1. - _Gary W. Adamson_, May 11 2003",
				"For all terms x of the sequence, 2*x^2 + 1 is a square. Limit_{n-\u003einfinity} a(n)/a(n-1) = 3 + 2*sqrt(2). - _Gregory V. Richardson_, Oct 10 2002",
				"For n \u003e 0: a(n) = A001652(n) + A046090(n) - A001653(n); e.g. 70 = 119 + 120 - 169. Also a(n) = A001652(n - 1) + A046090(n - 1) + A001653(n - 1); e.g., 70 = 20 + 21 + 29. Also a(n)^2 + 1 = A001653(n - 1)*A001653(n); e.g., 12^2 + 1 = 145 = 5*29. Also a(n + 1)^2 = A084703(n + 1) = A001652(n)*A001652(n + 1) + A046090(n)*A046090(n + 1). - _Charlie Marion_, Jul 01 2003",
				"a(n) = ((1+sqrt(2))^(2*n)-(1-sqrt(2))^(2*n))/(2*sqrt(2)). - _Antonio Alberto Olivares_, Dec 24 2003",
				"2*A001541(k)*A001653(n)*A001653(n+k) = A001653(n)^2 + A001653(n+k)^2 + a2(k)^2; e.g., 2*3*5*29 = 5^2+29^2+2^2; 2*99*29*5741 = 2*99*29*5741 = 29^2+5741^2+70^2. - _Charlie Marion_, Oct 12 2007",
				"a(n) = sinh(2*n*arcsinh(1))/sqrt(2). - _Herbert Kociemba_, Apr 24 2008",
				"For n \u003e 0, a(n) = A001653(n) + A002315(n-1). - _Richard R. Forberg_, Aug 31 2013",
				"a(n) = 3*a(n-1) + 2*A001541(n-1); e.g., a(4) = 70 = 3*12+2*17. - _Zak Seidov_, Dec 19 2013",
				"a(n)^2 + 1^2 = A115598(n)^2 + (A115598(n)+1)^2. - _Hermann Stamm-Wilbrandt_, Jul 27 2014",
				"Sum _{n \u003e= 1} 1/( a(n) + 1/a(n) ) = 1/2. - _Peter Bala_, Mar 25 2015",
				"E.g.f.: exp(3*x)*sinh(2*sqrt(2)*x)/sqrt(2). - _Ilya Gutkovskiy_, Dec 07 2016",
				"A007814(a(n)) = A001511(n). See Mathematical Reflections link. - _Michel Marcus_, Jan 06 2017",
				"a(n) = -a(-n) for all n in Z. - _Michael Somos_, Jan 20 2017",
				"From _A.H.M. Smeets_, May 28 2017: (Start)",
				"A051009(n) = a(2^(n-2)).",
				"a(2n) =2*a(2)*A001541(n).",
				"A001541(n)/a(n) \u003e sqrt(2) \u003e 2*a(n)/A001541(n). (End)",
				"a(A298210(n)) = A002349(2*n^2). - _A.H.M. Smeets_, Jan 25 2018"
			],
			"example": [
				"G.f. = 2*x + 12*x^2 + 70*x^3 + 408*x^4 + 2378*x^5 + 13860*x^6 + ..."
			],
			"maple": [
				"A001542:=2*z/(1-6*z+z**2); # conjectured by _Simon Plouffe_ in his 1992 dissertation",
				"seq(combinat:-fibonacci(2*n, 2), n = 0..20); # _Peter Luschny_, Jun 28 2018"
			],
			"mathematica": [
				"LinearRecurrence[{6,-1},{0,2},30] (* _Harvey P. Dale_, Jun 11 2011 *)",
				"Fibonacci[2*Range[0,20], 2] (* _G. C. Greubel_, Dec 23 2019 *)"
			],
			"program": [
				"(Haskell)",
				"a001542 n = a001542_list !! n",
				"a001542_list =",
				"   0 : 2 : zipWith (-) (map (6 *) $ tail a001542_list) a001542_list",
				"-- _Reinhard Zumkeller_, Aug 14 2011",
				"(Maxima)",
				"a[0]:0$",
				"a[1]:2$",
				"a[n]:=6*a[n-1]-a[n-2]$",
				"A001542(n):=a[n]$",
				"makelist(A001542(x),x,0,30); /* _Martin Ettl_, Nov 03 2012 */",
				"(PARI)  {a(n) = imag( (3 + 2*quadgen(8))^n )}; /* _Michael Somos_, Jan 20 2017 */",
				"(PARI) vector(21, n, 2*polchebyshev(n-1, 2, 33) ) \\\\ _G. C. Greubel_, Dec 23 2019",
				"(Python)",
				"l=[0, 2]",
				"for n in range(2, 51): l+=[6*l[n - 1] - l[n - 2], ]",
				"print(l) # _Indranil Ghosh_, Jun 06 2017",
				"(MAGMA) I:=[0,2]; [n le 2 select I[n] else 6Self(n-1) -Self(n-2): n in [1..20]]; // _G. C. Greubel_, Dec 23 2019",
				"(Sage) [2*chebyshev_U(n-1,3) for n in (0..20)] # _G. C. Greubel_, Dec 23 2019",
				"(GAP) a:=[0,2];; for n in [3..20] do a[n]:=6*a[n-1]-a[n-2]; od; a; # _G. C. Greubel_, Dec 23 2019"
			],
			"xref": [
				"Bisection of Pell numbers A000129: {a(n)} and A001653(n+1), n \u003e= 0.",
				"Cf. A001108, A001353, A001541, A001835, A003499, A007805, A007913, A115598, A125650, A125651, A125652."
			],
			"keyword": "nonn,easy,nice,changed",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 62,
			"revision": 215,
			"time": "2022-01-05T00:42:57-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}