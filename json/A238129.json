{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A238129",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 238129,
			"data": "1,1,0,1,1,0,1,3,0,0,1,8,1,0,0,1,19,5,1,0,0,1,47,21,6,1,0,0,1,114,78,31,7,1,0,0,1,286,292,133,43,8,1,0,0,1,723,1028,586,215,57,9,1,0,0,1,1869,3691,2453,1073,325,73,10,1,0,0,1,4870,13004,10357,5058,1836,467,91,11,1,0,0",
			"name": "Triangle read by rows: T(n,k) gives the number of ballot sequences of length n having largest ascent k, n\u003e=0, 0\u003c=k\u003c=n.",
			"comment": [
				"Also number of standard Young tableaux with a pair of cells (v,v+1) such that v lies k rows below v+1, and no pair (u,u+1) with a larger such separation exists."
			],
			"link": [
				"Joerg Arndt and Alois P. Heinz, \u003ca href=\"/A238129/b238129.txt\"\u003eTable of n, a(n) for n = 0..35, flattened\u003c/a\u003e"
			],
			"example": [
				"Triangle starts:",
				"00: 1;",
				"01: 1,     0;",
				"02: 1,     1,     0;",
				"03: 1,     3,     0,     0;",
				"04: 1,     8,     1,     0,     0;",
				"05: 1,    19,     5,     1,     0,    0;",
				"06: 1,    47,    21,     6,     1,    0,    0;",
				"07: 1,   114,    78,    31,     7,    1,    0,   0;",
				"08: 1,   286,   292,   133,    43,    8,    1,   0,   0;",
				"09: 1,   723,  1028,   586,   215,   57,    9,   1,   0,  0;",
				"10: 1,  1869,  3691,  2453,  1073,  325,   73,  10,   1,  0, 0;",
				"11: 1,  4870, 13004, 10357,  5058, 1836,  467,  91,  11,  1, 0, 0;",
				"12: 1, 12943, 46452, 43462, 23953, 9631, 2941, 645, 111, 12, 1, 0, 0;",
				"..."
			],
			"maple": [
				"b:= proc(n, v, l) option remember; local m; m:=nops(l);",
				"      `if`(n\u003c1, 1, expand(add(`if`(i=1 or l[i-1]\u003el[i],",
				"       (p-\u003e`if`(v\u003ci, add(coeff(p, x, h)*`if`(h\u003ci-v,",
				"       x^(i-v), x^h), h=0..max(i-v, degree(p))), p))",
				"       (b(n-1, i, subsop(i=l[i]+1, l))), 0), i=1..m)+",
				"       (p-\u003eadd(coeff(p, x, h)*`if`(h\u003cm+1-v, x^(m+1-v), x^h),",
				"       h=0..max(m+1-v, degree(p))))(b(n-1, m+1, [l[], 1]))))",
				"    end:",
				"T:= n-\u003e(p-\u003eseq(coeff(p, x, i), i=0..n))(b(n-1, 1, [1])):",
				"seq(T(n), n=0..14);"
			],
			"mathematica": [
				"b[n_, v_, l_List] := b[n, v, l] = Module[{m = Length[l]}, If[n\u003c1, 1, Expand[Sum[If[i == 1 || l[[i-1]]\u003el[[i]], Function[{p}, If[v\u003ci, Sum[Coefficient[p, x, h]*If[h\u003ci-v, x^(i-v), x^h], {h, 0, Max[i-v, Exponent[p, x]]}], p]][b[n-1, i, ReplacePart[l, i -\u003e l[[i]]+1]]], 0], {i, 1, m}] + Function[{p}, Sum[Coefficient[p, x, h]*If[h\u003cm+1-v, x^(m+1-v), x^h], {h, 0, Max[m+1-v, Exponent[p, x]]}]][b[n-1, m+1, Append[l, 1]]]]]]; T[n_] := Function[{p}, Table[Coefficient[p, x, i], {i, 0, n}]][b[n-1, 1, {1}]]; Table[T[n], {n, 0, 14}] // Flatten (* _Jean-François Alcover_, Jan 07 2015, translated from Maple *)"
			],
			"xref": [
				"Columns k=0-10 give: A000012, A244208, A244198, A244199, A244200, A244201, A244202, A244203, A244204, A244205, A244206.",
				"Row sums are A000085.",
				"Cf. A238128."
			],
			"keyword": "nonn,tabl",
			"offset": "0,8",
			"author": "_Joerg Arndt_ and _Alois P. Heinz_, Feb 21 2014",
			"references": 12,
			"revision": 17,
			"time": "2015-01-07T05:23:03-05:00",
			"created": "2014-02-23T05:06:10-05:00"
		}
	]
}