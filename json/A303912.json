{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A303912",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 303912,
			"data": "1,1,1,1,1,1,1,1,2,1,1,1,3,3,1,1,1,4,6,6,1,1,1,5,10,19,10,1,1,1,6,15,44,57,28,1,1,1,7,21,85,197,258,63,1,1,1,8,28,146,510,1228,1110,190,1,1,1,9,36,231,1101,4051,7692,5475,546,1,1,1,10,45,344,2100,10632,33130,52828,27429,1708,1",
			"name": "Array read by antidiagonals: T(n,k) is the number of (planar) unlabeled k-ary cacti having n polygons.",
			"comment": [
				"A k-ary cactus is a planar k-gonal cactus with vertices on each polygon numbered 1..k counterclockwise with shared vertices having the same number. In total there are always exactly k ways to number a given cactus since all polygons are connected. See the reference for a precise definition."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A303912/b303912.txt\"\u003eTable of n, a(n) for n = 0..1274\u003c/a\u003e",
				"Miklos Bona, Michel Bousquet, Gilbert Labelle, Pierre Leroux, \u003ca href=\"https://arxiv.org/abs/math/9804119\"\u003eEnumeration of m-ary cacti\u003c/a\u003e, arXiv:math/9804119 [math.CO], 1998-1999.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Cactus_graph\"\u003eCactus graph\u003c/a\u003e",
				"\u003ca href=\"/index/Ca#cacti\"\u003eIndex entries for sequences related to cacti\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = (Sum_{d|n} phi(n/d)*binomial(k*d, d))/n - (k-1)*binomial(k*n, n)/((k-1)*n+1)) for n \u003e 0.",
				"T(n,k) ~ A070914(n,k-1)/n for fixed k \u003e 1."
			],
			"example": [
				"Array begins:",
				"===============================================================",
				"n\\k| 1   2     3      4       5        6        7         8",
				"---+-----------------------------------------------------------",
				"0  | 1   1     1      1       1        1        1         1 ...",
				"1  | 1   1     1      1       1        1        1         1 ...",
				"2  | 1   2     3      4       5        6        7         8 ...",
				"3  | 1   3     6     10      15       21       28        36 ...",
				"4  | 1   6    19     44      85      146      231       344 ...",
				"5  | 1  10    57    197     510     1101     2100      3662 ...",
				"6  | 1  28   258   1228    4051    10632    23884     47944 ...",
				"7  | 1  63  1110   7692   33130   107062   285390    662628 ...",
				"8  | 1 190  5475  52828  291925  1151802  3626295   9711032 ...",
				"9  | 1 546 27429 373636 2661255 12845442 47813815 147766089 ...",
				"..."
			],
			"mathematica": [
				"T[0, _] = 1;",
				"T[n_, k_] := DivisorSum[n, EulerPhi[n/#] Binomial[k #, #]\u0026]/n - (k-1) Binomial[n k, n]/((k-1) n + 1);",
				"Table[T[n-k, k], {n, 0, 12}, {k, n, 1, -1}] // Flatten (* _Jean-François Alcover_, May 22 2018 *)"
			],
			"program": [
				"(PARI) T(n,k)={if(n==0, 1, sumdiv(n, d, eulerphi(n/d)*binomial(k*d, d))/n - (k-1)*binomial(k*n, n)/((k-1)*n+1))}"
			],
			"xref": [
				"Columns 2..7 are A054357, A052393, A052394, A054363, A054366, A054369.",
				"Cf. A070914, A303694, A303913."
			],
			"keyword": "nonn,tabl",
			"offset": "0,9",
			"author": "_Andrew Howroyd_, May 02 2018",
			"references": 8,
			"revision": 22,
			"time": "2020-02-18T19:22:37-05:00",
			"created": "2018-05-02T15:21:30-04:00"
		}
	]
}