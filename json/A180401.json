{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A180401",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 180401,
			"data": "1,0,1,0,1,1,0,4,4,1,0,36,33,10,1,0,576,480,148,20,1,0,14400,10960,3281,483,35,1,0,518400,362880,103824,15552,1288,56,1,0,25401600,16465680,4479336,663633,57916,2982,84,1,0,1625702400,981872640,253732096,36690816,3252624,181312,6216,120,1",
			"name": "Stirling-like sequence obtained from bipartite 0-1 tableaux.",
			"comment": [
				"Gives the number of ways to construct pairs of permutations of an n-element set into k cycles such that the sum of the minima of the i-th cycle of the first permutation and the (k-i+1)-th cycle of the second permutation is n+1."
			],
			"link": [
				"K. J. M. Gonzales, \u003ca href=\"http://arxiv.org/abs/1008.4192\"\u003eEnumeration of Restricted Permutation Pairs and Partitions Pairs via 0-1 Tableaux\u003c/a\u003e, arXiv:1008.4192 [math.CO], 2010-2014.",
				"A. de Medicis and P. Leroux, \u003ca href=\"http://dx.doi.org/10.4153/CJM-1995-027-x\"\u003eGeneralized Stirling Numbers, Convolution Formulae and p,q-Analogues, Can. J. Math. 47 (1995), 474-499."
			],
			"formula": [
				"G.f.: sum_{all r=\u003e0} C(n,k) x^r = prod_{all v+w=n,0\u003c=v,w\u003c=n-1} (x+vw)",
				"Symm. f: C(n,k)=sum_{all 0 \u003c=i_1\u003ci_2\u003c...\u003ci_{n-k}\u003c=n-1}",
				"(i_1*(n-1)-i_1)*(i_2*(n-1)-i_2)*...*(i_{n-k}*(n-1)-i_{n-k})",
				"Recurrences: Let C(n,k;r)=sum_{all 0 \u003c=i_1\u003ci_2\u003c...\u003ci_{n-k}\u003c=n-1}",
				"(i_1*(r+(n-1)-i_1))*(i_2*(r+(n-1)-i_2))*...*(i_{n-k}*(r+(n-1)-i_{n-k})). Then,",
				"C(n,k)=C(n-1,k-1,1)+(n)C(n-1,k,1)"
			],
			"example": [
				"For n=6, C(6,0)=0, C(6,1)=0, C(6,2)=1, C(6,3)=32, C(6,4)=67, C(6,5)=20, C(6,6)=1"
			],
			"program": [
				"(R) ## Runs on R 2.7.1",
				"## Here, beta=r in recurrences",
				"cnk\u003c-function(n,k,beta=0){",
				"alpha=0",
				"as\u003c-function(j){j}",
				"bs\u003c-function(j){j}",
				"form.seq\u003c-function(n,fcn){ss\u003c-NULL;for(i in 0:n){ss\u003c-c(ss,fcn(i))};ss}",
				"seq.a\u003c-form.seq(n+alpha+1,as)",
				"seq.b\u003c-form.seq(n+beta+1,bs)",
				"v\u003c-function(i){i}",
				"w\u003c-function(i){i}",
				"if(n\u003ek){",
				"Atab\u003c-combn(1:n-1,n-k)",
				"Btab\u003c-n-1-Atab+beta",
				"Atab\u003c-Atab+alpha",
				"px\u003c-NULL",
				"for(i in 1:ncol(Atab)){",
				"partial\u003c-NULL",
				"for(j in 1:nrow(Atab)){",
				"partial\u003c-c(partial,(v(seq.a[Atab[j,i]+1])*w(seq.b[Btab[j,i]+1])))",
				"} # for(j in 1:nrow(Atab))",
				"px\u003c-c(px,prod(partial))",
				"}# for(i in 1:ncol(Atab))",
				"} # if(n\u003ek)",
				"if(n\u003ek) x\u003c-sum(px)",
				"if(n==k) x=1",
				"if(n\u003ck) x=0",
				"x",
				"}",
				"# Example",
				"cnk(7,4)"
			],
			"xref": [
				"Cf. A000292, A080251."
			],
			"keyword": "nonn,tabl",
			"offset": "1,8",
			"author": "_Ken Joffaniel M Gonzales_, Sep 02 2010, Sep 27 2010",
			"references": 0,
			"revision": 17,
			"time": "2017-05-14T02:58:56-04:00",
			"created": "2010-09-12T03:00:00-04:00"
		}
	]
}