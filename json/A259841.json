{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A259841",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 259841,
			"data": "1,3,1,15,5,2,117,37,17,7,1367,418,189,100,40,23329,7027,3058,1688,939,357,570933,171428,72194,39274,24050,13429,4820,19740068,5948380,2449366,1293768,807576,517548,283510,96030",
			"name": "Number T(n,k) of elements k in all n X n Tesler matrices of nonnegative integers; triangle T(n,k), n\u003e=1, 1\u003c=k\u003c=n, read by rows.",
			"comment": [
				"For the definition of Tesler matrices see A008608.",
				"Sum_{k=1..n} k * T(n,k) = A259787(n)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A259841/b259841.txt\"\u003eRows n = 1..20, flattened\u003c/a\u003e"
			],
			"example": [
				"There are two 2 X 2 Tesler matrices: [1,0; 0,1], [0,1; 0,2], containing three 1's and one 2, thus row 2 gives [3, 1].",
				"Triangle T(n,k) begins:",
				":      1;",
				":      3,      1;",
				":     15,      5,     2;",
				":    117,     37,    17,     7;",
				":   1367,    418,   189,   100,    40;",
				":  23329,   7027,  3058,  1688,   939,   357;",
				": 570933, 171428, 72194, 39274, 24050, 13429, 4820;"
			],
			"maple": [
				"g:= u-\u003e `if`(u=0, 0, x^u):",
				"b:= proc(n, i, l) option remember; (m-\u003e`if`(m=0, [1, g(n)], `if`(i=0,",
				"     (p-\u003ep+[0, p[1]*g(n)])(b(l[1]+1, m-1, subsop(1=NULL, l))), add(",
				"     (p-\u003ep+[0, p[1]*g(j)])(b(n-j, i-1, subsop(i=l[i]+j, l)))",
				"      , j=0..n))))(nops(l))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=1..n))(b(1,n-1,[0$(n-1)])[2]):",
				"seq(T(n), n=1..10);"
			],
			"mathematica": [
				"g[u_] := If[u == 0, 0, x^u];",
				"b[n_, i_, l_] := b[n, i, l] = Function[m, If[m == 0, {1, g[n]}, If[i == 0, # + {0, #[[1]] g[n]} \u0026 [b[l[[1]]+1, m-1, ReplacePart[l, 1 -\u003e Nothing]]], Sum[# + {0, #[[1]] g[j]} \u0026 [b[n-j, i-1, ReplacePart[l, i -\u003e l[[i]] + j]]], {j, 0, n}]]]][Length[l]];",
				"T[n_] := Table[Coefficient[#, x, i], {i, 1, n}] \u0026 [b[1, n-1, Table[0, {n-1}]][[2]]];",
				"Array[T, 10] // Flatten (* _Jean-François Alcover_, Oct 28 2020, after Maple *)"
			],
			"xref": [
				"Main diagonal gives A008608(n-1) for n\u003e1.",
				"Column k=1 gives A259843.",
				"Row sums give A259842.",
				"Cf. A259787."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Alois P. Heinz_, Jul 06 2015",
			"references": 5,
			"revision": 15,
			"time": "2020-10-28T10:14:18-04:00",
			"created": "2015-07-06T17:47:04-04:00"
		}
	]
}