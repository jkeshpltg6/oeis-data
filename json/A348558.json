{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A348558",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 348558,
			"data": "31,41,61,71,101,131,151,181,191,2131,2141,2161,3121,3181,3191,5101,5171,6101,6121,6131,6151,7121,7151,8101,8161,8171,8191,9151,9161,9181,10141,10151,10181,12101,12161,13121,13151,13171,15101,15121,15131,15161,16141",
			"name": "Primes where every other digit is 1 starting with the rightmost digit, and no other digit is 1.",
			"link": [
				"Michael S. Branicky, \u003ca href=\"/A348558/b348558.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"mathematica": [
				"Select[Prime@Range@10000,(n=#;s={EvenQ,OddQ};t=Take[IntegerDigits@n,{#}]\u0026/@Select[Range@i,#]\u0026/@If[EvenQ[i=IntegerLength@n],s,Reverse@s];Union@Flatten@First@t=={1}\u0026\u0026FreeQ[Flatten@Last@t,1])\u0026] (* _Giorgos Kalogeropoulos_, Oct 22 2021 *)"
			],
			"program": [
				"(Python)",
				"from sympy import primerange as primes",
				"def ok(p):",
				"    s = str(p)",
				"    if not all(s[i] == '1' for i in range(-1, -len(s)-1, -2)): return False",
				"    return all(s[i] != '1' for i in range(-2, -len(s)-1, -2))",
				"print(list(filter(ok, primes(1, 16142)))) # _Michael S. Branicky_, Oct 22 2021",
				"(Python) # faster version for generating large initial segments of sequence",
				"from sympy import isprime",
				"from itertools import product",
				"def eo1(maxdigits): # generator for every other digit is 1, no other 1's",
				"    yield 1",
				"    for d in range(2, maxdigits+1):",
				"        if d%2 == 0:",
				"            for f in \"23456789\":",
				"                f1 = f + \"1\"",
				"                for p in product(\"023456789\", repeat=(d-1)//2):",
				"                    yield int(f1 + \"\".join(p[i]+\"1\" for i in range(len(p))))",
				"        else:",
				"            for p in product(\"023456789\", repeat=(d-1)//2):",
				"                yield int(\"1\" + \"\".join(p[i]+\"1\" for i in range(len(p))))",
				"print(list(filter(isprime, eo1(5)))) # _Michael S. Branicky_, Oct 22 2021",
				"(MAGMA) f1:=func\u003cn|forall{i:i in [1..#Intseq(n) by 2]| Intseq(n)[i] eq 1}\u003e;  fc:=func\u003cn|forall{i:i in [2..#Intseq(n) by 2]| Intseq(n)[i] ne 1}\u003e; [p:p in PrimesUpTo(17000)|f1(p) and fc(p)]; // _Marius A. Burtea_, Oct 22 2021"
			],
			"xref": [
				"Cf. A000040, A348559, A348560, A348561."
			],
			"keyword": "nonn,base",
			"offset": "1,1",
			"author": "_Lars Blomberg_, Oct 22 2021",
			"references": 4,
			"revision": 18,
			"time": "2021-10-25T07:55:05-04:00",
			"created": "2021-10-23T00:11:11-04:00"
		}
	]
}