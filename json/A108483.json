{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A108483",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 108483,
			"data": "1,1,0,0,0,-1,0,1,1,0,-1,-1,-1,0,2,2,0,-1,-2,-2,0,3,3,0,-2,-3,-3,0,5,5,1,-3,-5,-5,0,7,7,1,-5,-8,-7,1,11,12,2,-7,-12,-11,1,15,16,3,-11,-18,-15,2,23,24,5,-15,-26,-22,3,31,33,7,-22,-37,-30,5,44,47,11,-30,-52,-42,6,59,63,15,-42,-72,-56,10,82,88,22",
			"name": "Expansion of f(-x^2, -x^5) / f(-x, -x^6) in powers of x where f() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"In Duke (2005) page 157 the g.f. is denoted by t(tau)."
			],
			"reference": [
				"W. Duke, Continued fractions and modular functions, Bull. Amer. Math. Soc. 42 (2005), 137-162."
			],
			"link": [
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e [This is just section 1 of the next item, but it is mentioned in over 1000 sequences. - _N. J. A. Sloane_, Nov 13 2019]",
				"Michael Somos, \u003ca href=\"/A108483/a108483.pdf\"\u003eA Multisection of q-Series\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Euler transform of period 7 sequence [ 1, -1, 0, 0, -1, 1, 0, ...]. - _Michael Somos_, Oct 03 2013",
				"Given g.f. A(x), then B(q) = q^-2*A(q^7) satisfies 0 = f(B(q), B(q^2)) where f(u, v) = v^3 - u^6 + 3*u^4*v + u^7*v^3 + u^2*v^9 + u^8*v^6 - 3*u^2*v^2 - 2*u*v^6 - 5*u^3*v^5 - u^5*v^4 - u^9*v^2 - u^4*v^8 - u^6*v^7.",
				"G.f.: Product_{k\u003e0} (1 - x^(7*k - 2)) * (1 - x^(7*k - 5)) / ((1 - x^(7*k - 1)) * (1 - x^(7*k - 6))).",
				"a(n) = A229894(7*n). - _Michael Somos_, Oct 03 2013"
			],
			"example": [
				"G.f. = 1 + x - x^5 + x^7 + x^8 - x^10 - x^11 - x^12 + 2*x^14 + 2*x^15 + ...",
				"G.f. = q^-2 + q^5 - q^33 + q^47 + q^54 - q^68 - q^75 - q^82 + 2*q^96 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ x^2, x^7] QPochhammer[ x^5, x^7] / (QPochhammer[ x, x^7] QPochhammer[ x^6, x^7]), {x, 0, n}]; (* _Michael Somos_, Oct 03 2013 *)",
				"a[ n_] := SeriesCoefficient[ Product[ (1 - x^k)^{-1, 1, 0, 0, 1, -1, 0}[[Mod[k, 7, 1]]], {k, n}], {x, 0, n}]; (* _Michael Somos_, May 03 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, 0, polcoeff( prod(k=1, n, (1 - x^k + x*O(x^n))^[ 0, -1, 1, 0, 0, 1, -1][k%7 + 1]), n))};"
			],
			"xref": [
				"Cf. A229894."
			],
			"keyword": "sign",
			"offset": "0,15",
			"author": "_Michael Somos_, Jun 04 2005",
			"references": 3,
			"revision": 14,
			"time": "2021-03-12T22:24:43-05:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}