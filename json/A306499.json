{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A306499",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 306499,
			"data": "2,2082927221,11100143,61463,2083,2,1217,5,3,719,2,11,3,2,7,17,11,2,7,5,2,13,2,3,23,7,3,2,13,19,2,23,17,2,5,2,7,3,2,13,3,2,19,7,2,31,31,5,17,2,13,13,3,47,2,5,3,2,37,2,47,2,5,7,2,43,2,3,11,5,3,2,29",
			"name": "a(n) is the smallest prime p such that Sum_{primes q \u003c= p} Kronecker(A003658(n),q) \u003e 0, or 0 if no such prime exists.",
			"comment": [
				"Let D be a fundamental discriminant (only the case where D is fundamental is considered because {Kronecker(D,k)} forms a primitive real Dirichlet character with period |D| if and only if D is fundamental), it seems that Sum_{primes q \u003c= p} Kronecker(D,p) \u003c= 0 occurs much more often than its opposite does. This can be seen as a variation of the well-known \"Chebyshev's bias\". Sequence gives the least prime that violates the inequality when D runs through all positive discriminants.",
				"For any D, the primes p such that Kronecker(D,p) = 1 has asymptotic density 1/2 in all the primes, so a(n) should be \u003e 0 for all n.",
				"Actually, for most n, a(n) is relatively small compared with A003658(n). There are only 52 n's in [1, 3044] (there are 3044 terms in A003658 below 10000) such that a(n) \u003e A003658(n). The largest terms among the 52 corresponding terms are a(2) = 2082927221 (with A003658(2) = 5), a(2193) = 718010179 (with A003658(2193) = 7213) and a(3) = 11100143 (with A003658(3) = 8)."
			],
			"link": [
				"Jianing Song, \u003ca href=\"/A306499/b306499.txt\"\u003eTable of n, a(n) for n = 1..3044\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Chebyshev%27s_bias\"\u003eChebyshev's bias\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 2 if A003658(n) == 1 (mod 8);",
				"a(n) = 3 if A003658(n) == 28, 40 (mod 48);",
				"a(n) = 5 if A003658(n) == 24, 61, 109, 156, 181, 204, 216, 229 (mod 240)."
			],
			"example": [
				"Let D = A003658(16) = 53, j(k) = Sum_{primes q \u003c= prime(k)} Kronecker(D,q).",
				"For k = 1, Kronecker(53,2) = -1, so j(1) = -1;",
				"For k = 2, Kronecker(53,3) = -1, so j(2) = -2;",
				"For k = 3, Kronecker(53,5) = -1, so j(3) = -3;",
				"For k = 4, Kronecker(53,7) = +1, so j(4) = -2;",
				"For k = 5, Kronecker(53,11) = +1, so j(5) = -1;",
				"For k = 6, Kronecker(53,13) = +1, so j(6) = 0;",
				"For k = 7, Kronecker(53,17) = +1, so j(7) = 1.",
				"The first time for j \u003e 0 occurs at the prime 17, so a(16) = 17."
			],
			"program": [
				"(PARI) b(n) = my(i=0); forprime(p=2,oo,i+=kronecker(n,p); if(i\u003e0, return(p)))",
				"for(n=1, 300, if(isfundamental(n), print1(b(n), \", \")))",
				"(Sage)",
				"def KroneckerSum():",
				"    yield 2",
				"    ind = 0",
				"    while True:",
				"        ind += 1",
				"        while not is_fundamental_discriminant(ind):",
				"            ind += 1",
				"        s, p = 0, 1",
				"        while s \u003c 1:",
				"            p = p.next_prime()",
				"            s += kronecker(ind, p)",
				"        yield p",
				"A306499 = KroneckerSum()",
				"print([next(A306499) for _ in range(71)]) # _Peter Luschny_, Feb 26 2019"
			],
			"xref": [
				"Cf. A003658, A306500 (the negative discriminants case).",
				"The indices of primes are given in A306502."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Jianing Song_, Feb 19 2019",
			"references": 5,
			"revision": 36,
			"time": "2020-03-24T17:09:37-04:00",
			"created": "2019-02-27T07:25:06-05:00"
		}
	]
}