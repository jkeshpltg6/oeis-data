{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A091648",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 91648,
			"data": "8,8,1,3,7,3,5,8,7,0,1,9,5,4,3,0,2,5,2,3,2,6,0,9,3,2,4,9,7,9,7,9,2,3,0,9,0,2,8,1,6,0,3,2,8,2,6,1,6,3,5,4,1,0,7,5,3,2,9,5,6,0,8,6,5,3,3,7,7,1,8,4,2,2,2,0,2,6,0,8,7,8,3,3,7,0,6,8,9,1,9,1,0,2,5,6,0,4,2,8,5,6",
			"name": "Decimal expansion of arccosh(sqrt(2)), the inflection point of sech(x).",
			"comment": [
				"Asymptotic growth constant in the exponent for the number of spanning trees on the 2 X infinity strip on the square lattice. - _R. J. Mathar_, May 14 2006",
				"Arccosh(sqrt(2)) = (1/2)*log((sqrt(2)+1)/(sqrt(2)-1)) = log(tan(3*Pi/8)) = int(1/cos(x),x=0..Pi/4). Therefore, in Gerardus Mercator's (conformal) map this is the value of the ordinate y/R (R radius of the spherical earth) for latitude phi = 45 degrees north, or Pi/4. See, e.g., the Eli Maor reference, eqs. (5) and (6). This is the latitude of, e.g., the Mission Point Lighthouse, Michigan, U.S.A. - _Wolfdieter Lang_, Mar 05 2013",
				"Decimal expansion of the arclength on the hyperbola y^2 - x^2 = 1 from (0,0) to (1,sqrt(2)). - _Clark Kimberling_, Jul 04 2020"
			],
			"reference": [
				"L. B. W. Jolley, Summation of Series, Dover (1961), Eq. (85) page 16-17.",
				"E. Maor, Trigonometric Delights, Princeton University Press, NJ, 1998, chapter 13, A Mapmaker's Paradise, pp. 163-180."
			],
			"link": [
				"Ivan Panchenko, \u003ca href=\"/A091648/b091648.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"D. H. Lehmer, \u003ca href=\"http://www.jstor.org/stable/2322496\"\u003eInteresting Series Involving the Central Binomial Coefficient\u003c/a\u003e, Am. Math. Monthly 92 (1985) 449.",
				"R. Shrock and F. Y. Wu, \u003ca href=\"http://dx.doi.org/10.1088/0305-4470/33/21/303\"\u003eSpanning trees on graphs and lattices in d dimensions\u003c/a\u003e, J Phys A: Math Gen 33 (2000) 3881-3902",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/HyperbolicSecant.html\"\u003eHyperbolic Secant\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/UniversalParabolicConstant.html\"\u003eUniversal Parabolic Constant\u003c/a\u003e"
			],
			"formula": [
				"Equals log(1 + sqrt(2)). - _Jonathan Sondow_, Mar 15 2005",
				"Equals (1/2)*log(3+2*sqrt(2)). - _R. J. Mathar_, May 14 2006",
				"Equals Sum_{n\u003e=1, n odd} binomial(2*n,n)/(n*4^n) [see Lehmer link]. - _R. J. Mathar_, Mar 04 2009",
				"Equals arcsinh(1), since arcsinh(x) = log(x+sqrt(x^2+1)). - _Stanislav Sykora_, Nov 01 2013",
				"Equals asin(i)/i. - _L. Edson Jeffery_, Oct 19 2014",
				"Equals (Pi/4) * 3F2(1/4, 1/2, 3/4; 1, 3/2; 1). - _Jean-François Alcover_, Apr 23 2015"
			],
			"example": [
				"0.8813735870195430252326093249797923090281603282616..."
			],
			"mathematica": [
				"RealDigits[Log[1 + Sqrt[2]], 10, 100][[1]] (* _Alonso del Arte_, Aug 11 2011 *)"
			],
			"program": [
				"(Maxima) fpprec : 100$ ev(bfloat(log(1 + sqrt(2)))); /* _Martin Ettl_, Oct 17 2012 */",
				"(PARI) asinh(1) \\\\ _Michel Marcus_, Oct 19 2014"
			],
			"xref": [
				"Cf. A103710, A103711, A103712, A181048."
			],
			"keyword": "nonn,cons,easy",
			"offset": "0,1",
			"author": "_Eric W. Weisstein_, Jan 24 2004",
			"references": 19,
			"revision": 72,
			"time": "2021-03-06T02:25:16-05:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}