{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A016969",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 16969,
			"data": "5,11,17,23,29,35,41,47,53,59,65,71,77,83,89,95,101,107,113,119,125,131,137,143,149,155,161,167,173,179,185,191,197,203,209,215,221,227,233,239,245,251,257,263,269,275,281,287,293,299,305,311,317,323,329,335",
			"name": "a(n) = 6*n + 5.",
			"comment": [
				"Apart from initial term(s), dimension of the space of weight 2n cusp forms for Gamma_0(18).",
				"Exponents e such that x^e + x - 1 is reducible.",
				"First differences of A141631 (2, 7, 18). Last digit is period 5: repeat 5, 1, 7, 3, 9, fifth quintuplet with A139788 (1, 7, 3, 9, 5) or A139788(n+4). Three other quintuplets are A139788(n+1) = 7, 3, 9, 5, 1, A139788(n+2) = 3, 9, 5, 1, 7 and A139788(n+3) = 9, 5, 1, 7, 3 (the five odd digits). - _Paul Curtz_, Sep 12 2008",
				"a(n-1), n \u003e= 1, appears as first column in the triangle A239127 related to the Collatz problem. - _Wolfdieter Lang_, Mar 14 2014",
				"Odd unlucky numbers in A050505. - _Fred Daniel Kline_, Feb 25 2017",
				"Intersection of A005408 and A016789. - _Bruno Berselli_, Apr 26 2018"
			],
			"link": [
				"Muniru A Asiru, \u003ca href=\"/A016969/b016969.txt\"\u003eTable of n, a(n) for n = 0..3000\u003c/a\u003e",
				"Mark W. Coffey, \u003ca href=\"http://arxiv.org/abs/1601.01673\"\u003eBernoulli identities, zeta relations, determinant expressions, Mellin transforms, and representation of the Hurwitz numbers\u003c/a\u003e, arXiv:1601.01673 [math.NT], 2016.",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e.",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=949\"\u003eEncyclopedia of Combinatorial Structures 949\u003c/a\u003e.",
				"D. H. Lehmer, \u003ca href=\"http://www.jstor.org/stable/1968647\"\u003eLacunary recurrence formulas for the numbers of Bernoulli and Euler\u003c/a\u003e, Annals Math., Vol. 36, No. 3 (1935), pp. 637-649.",
				"Amelia Carolina Sparavigna, \u003ca href=\"https://doi.org/10.5281/zenodo.4641886\"\u003eThe Pentagonal Numbers and their Link to an Integer Sequence which contains the Primes of Form 6n-1\u003c/a\u003e, Politecnico di Torino (Italy, 2021).",
				"Amelia Carolina Sparavigna, \u003ca href=\"https://doi.org/10.5281/zenodo.4662489\"\u003eBinary operations inspired by generalized entropies applied to figurate numbers\u003c/a\u003e, Politecnico di Torino (Italy, 2021).",
				"William A. Stein, \u003ca href=\"http://wstein.org/Tables/dimskg0n.gp\"\u003eDimensions of the spaces S_k(Gamma_0(N))\u003c/a\u003e.",
				"William A. Stein, \u003ca href=\"http://wstein.org/Tables/\"\u003eThe modular forms database\u003c/a\u003e.",
				"Leo Tavares, \u003ca href=\"/A016969/a016969.jpg\"\u003eIllustration: Twin Triangular Frames\u003c/a\u003e.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,-1)."
			],
			"formula": [
				"a(n) = A003415(A003415(A125200(n+1)))/2. - _Reinhard Zumkeller_, Nov 24 2006",
				"A008615(a(n)) = n+1. - _Reinhard Zumkeller_, Feb 27 2008",
				"a(n) = A007310(2*n+1); complement of A016921 with respect to A007310. - _Reinhard Zumkeller_, Oct 02 2008",
				"From _Klaus Brockhaus_, Jan 04 2009: (Start)",
				"G.f.: (5+x)/(1-x)^2.",
				"a(0) = 5; for n \u003e 0, a(n) = a(n-1)+6.",
				"(End)",
				"a(n) = A016921(n)+4 = A016933(n)+3 = A016945(n)+2 = A016957(n)+1. - _Klaus Brockhaus_, Jan 04 2009",
				"a(n) = floor((12n-1)/2) with offset 1..a(1)=5. - _Gary Detlefs_, Mar 07 2010",
				"a(n) = 4*(3*n+1) - a(n-1) (with a(0) = 5). - _Vincenzo Librandi_, Nov 20 2010",
				"a(n) = floor(1/(1/sin(1/n) - n)). - _Clark Kimberling_, Feb 19 2010",
				"a(n) = 3*Sum_{k = 0..n} binomial(6*n+5, 6*k+2)*Bernoulli(6*k+2). - _Michel Marcus_, Jan 11 2016",
				"a(n) = A049452(n+1) / (n+1). - _Torlach Rush_, Nov 23 2018",
				"a(n) = 2*A000217(n+2) - 1 - 2*A000217(n-1). See Twin Triangular Frames illustration. - _Leo Tavares_, Aug 25 2021",
				"Sum_{n\u003e=0} (-1)^n/a(n) = Pi/6 - sqrt(3)*arccoth(sqrt(3))/3. - _Amiram Eldar_, Dec 10 2021"
			],
			"mathematica": [
				"6Range[0, 59] + 5 (* or *) NestList[6 + # \u0026, 5, 60] (* _Harvey P. Dale_, Mar 09 2013 *)"
			],
			"program": [
				"(MAGMA) [ 6*n+5: n in [0..55] ]; // _Klaus Brockhaus_, Jan 04 2009",
				"(PARI) a(n)=6*n+5 \\\\ _Charles R Greathouse IV_, Jul 10 2016",
				"(Scala) (1 to 60).map(6 * _ - 1).mkString(\", \") // _Alonso del Arte_, Nov 23 2018",
				"(GAP) List([0..60],n-\u003e6*n+5); # _Muniru A Asiru_, Nov 24 2018"
			],
			"xref": [
				"Cf. A111863, A007310, A008588, A016921, A016933, A016945, A016957, A049452.",
				"Cf. A050505 (unlucky numbers).",
				"Cf. A000217."
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Klaus Brockhaus_, Jan 04 2009"
			],
			"references": 64,
			"revision": 128,
			"time": "2021-12-10T11:30:30-05:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}