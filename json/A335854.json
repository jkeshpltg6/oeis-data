{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A335854",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 335854,
			"data": "11,9,99,999,9999,9990,1,112,12,3,125,33,4,15,8,337,44,5,154,88,2,37,24,49,55,6,14,38,81,22,53,7,92,48,495,552,66,71,47,387,813,227,531,77,79,26,483,45,152,86,64,715,471,376,83,52,73,51,87,75,792,261,43,74,56,121,863,642,759,41,436,58,385,29",
			"name": "The digital-root sandwiches sequence (see Comments lines for definition).",
			"comment": [
				"Imagine we would have a pair of adjacent integers in the sequence like [1951, 2020]. A digital-root sandwich would then be made of the rightmost digit of a(n), the leftmost digit of a(n+1) and, in between, the digital root of their sum. The pair [1951, 2020] would then produce the DR-sandwich 132. Please note that the pair [2020, 1951] would produce the genuine DR-sandwich 011 (we keep the leading zero: these are sandwiches after all, not integers).",
				"Now we want the sequence to be the lexicographically earliest sequence of distinct positive terms such that the successive sandwiches emerging from the sequence rebuild it, digit after digit."
			],
			"link": [
				"Carole Dubois, \u003ca href=\"/A335854/b335854.txt\"\u003eTable of n, a(n) for n = 1..428\u003c/a\u003e"
			],
			"example": [
				"The first successive sandwiches are: 119, 999, 999, 999, 999, 011, 121, 231, ...",
				"The first one (119) is visible between a(1) = 11 and a(2) = 9; we get the sandwich by inserting the digital root of the sum 1 + 9 = 10 (which is 1) between 1 and 9.",
				"The second sandwich (999) is visible between a(2) = 9 and a(3) = 99; we get the sandwich by inserting the digital root of the sum 9 + 9 = 18 (which is 9) between 9 and 9.",
				"The third sandwich (999) is visible between a(3) = 99 and a(3) = 999; we get the sandwich by inserting the digital root of the sum 9 + 9 = 18 (which is 9) between 9 and 9.",
				"(...)",
				"The sixth sandwich (011) is visible between a(6) = 9990 and a(7) = 1; we get the sandwich by inserting the digital root of the sum 0 + 1 = 1 (which is 1) between 0 and 1; etc.",
				"The successive sandwiches rebuild, digit after digit, the starting sequence."
			],
			"xref": [
				"Cf. A335600 (first definition of such a sandwich) and A010888 (digital root of n)."
			],
			"keyword": "base,nonn",
			"offset": "1,1",
			"author": "_Carole Dubois_ and _Eric Angelini_, Jun 26 2020",
			"references": 3,
			"revision": 10,
			"time": "2020-07-02T05:21:47-04:00",
			"created": "2020-07-01T22:32:58-04:00"
		}
	]
}