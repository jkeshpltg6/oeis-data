{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A342479",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 342479,
			"data": "0,1,1,1,46,44,288,33216,613248,151296,391584768,2383570944,86830424064,206470840320,21270238986240,987259950858240,1262040231444480,3022250536693923840,3884253754215628800,1102040800033347993600,1892288242221318144000,5616902226049109065728000",
			"name": "a(n) is the numerator of the asymptotic density of numbers whose second smallest prime divisor (A119288) is prime(n).",
			"comment": [
				"The second smallest prime divisor of a number k is the second member in the ordered list of the distinct prime divisors of k. All the numbers that are not prime powers (A000961) have a second smallest prime divisor."
			],
			"reference": [
				"József Sándor and Borislav Crstici, Handbook of Number theory II, Kluwer Academic Publishers, 2004, Chapter 4, pp. 337-341."
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A342479/b342479.txt\"\u003eTable of n, a(n) for n = 1..376\u003c/a\u003e",
				"Paul Erdős and Gérald Tenenbaum, \u003ca href=\"https://doi.org/10.1112/plms/s3-59.3.417\"\u003eSur les densités de certaines suites d'entiers\u003c/a\u003e, Proc. London Math. Soc. (3), Vol. 59, No. 3 (1989), pp. 417-438; \u003ca href=\"https://users.renyi.hu/~p_erdos/1989-36.pdf\"\u003ealternative link\u003c/a\u003e."
			],
			"formula": [
				"a(n)/A342480(n) = (1/prime(n)) * Product_{q prime \u003c prime(n)} (1 - 1/q) * Sum_{q prime \u003c prime(n)} 1/(q-1).",
				"Sum_{n\u003e=1} a(n)/A342480(n) = 1 (since the asymptotic density of numbers without a second smallest prime divisor, i.e., the prime powers, is 0)."
			],
			"example": [
				"The fractions begin with 0, 1/6, 1/10, 1/15, 46/1155, 44/1365, 288/12155, 33216/1616615, 613248/37182145, 151296/11849255, 391584768/33426748355, ...",
				"a(1) = 0 since there are no numbers whose second smallest prime divisor is prime(1) = 2.",
				"a(2)/A342480(2) = 1/6 since the numbers whose second smallest prime divisor is prime(2) = 3 are the positive multiples of 6.",
				"a(3)/A342480(3) = 1/10 since the numbers whose second smallest prime divisor is prime(3) = 5 are the numbers congruent to {10, 15, 20} (mod 30) whose density is 3/30 = 1/10."
			],
			"mathematica": [
				"f[n_] := Module[{p = Prime[n], q}, q = Select[Range[p - 1], PrimeQ]; Plus @@ (1/(q - 1))*Times @@ ((q - 1)/q)/p]; Numerator @ Array[f, 30]"
			],
			"xref": [
				"Cf. A000961, A038110, A038111, A119288, A342480 (denominators)."
			],
			"keyword": "nonn,easy,frac",
			"offset": "1,5",
			"author": "_Amiram Eldar_, Mar 13 2021",
			"references": 2,
			"revision": 14,
			"time": "2021-03-14T05:20:20-04:00",
			"created": "2021-03-13T18:07:42-05:00"
		}
	]
}