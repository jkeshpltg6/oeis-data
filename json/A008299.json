{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A008299",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 8299,
			"data": "1,1,1,3,1,10,1,25,15,1,56,105,1,119,490,105,1,246,1918,1260,1,501,6825,9450,945,1,1012,22935,56980,17325,1,2035,74316,302995,190575,10395,1,4082,235092,1487200,1636635,270270,1,8177,731731,6914908,12122110",
			"name": "Triangle T(n,k) of associated Stirling numbers of second kind, n\u003e=2, 1\u003c=k\u003c=floor(n/2).",
			"comment": [
				"T(n,k) is the number of set partitions of [n] into k blocks of size at least 2. Compare with A008277 (blocks of size at least 1) and A059022 (blocks of size at least 3). See also A200091. Reading the table by diagonals gives A134991. The row generating polynomials are the Mahler polynomials s_n(-x). See [Roman, 4.9]. - _Peter Bala_, Dec 04 2011",
				"Row n gives coefficients of moments of Poisson distribution about the mean expressed as polynomials in lambda [Haight]. The coefficients of the moments about the origin are the Stirling numbers of the second kind, A008277. - _N. J. A. Sloane_, Jan 24 2020",
				"Rows are of lengths 1,1,2,2,3,3,..., a pattern typical of matrices whose diagonals are rows of another lower triangular matrix--in this instance those of A134991. - _Tom Copeland_, May 01 2017",
				"For a relation to decomposition of spin correlators see Table 2 of the Delfino and Vito paper. - _Tom Copeland_, Nov 11 2012"
			],
			"reference": [
				"L. Comtet, Advanced Combinatorics, Reidel, 1974, p. 222.",
				"Frank Avery Haight, \"Handbook of the Poisson distribution,\" John Wiley, 1967. See pages 6,7, but beware of errors. [Haight on page 7 gives five different ways to generate these numbers (see link)].",
				"J. Riordan, An Introduction to Combinatorial Analysis, Wiley, 1958, p. 76.",
				"S. Roman, The Umbral Calculus, Dover Publications, New York (2005), pp. 129-130."
			],
			"link": [
				"Vincenzo Librandi and Alois P. Heinz, \u003ca href=\"/A008299/b008299.txt\"\u003eRows n = 2..200, flattened\u003c/a\u003e (rows n = 2..104 from Vincenzo Librandi)",
				"Joerg Arndt and N. J. A. Sloane, \u003ca href=\"/A278984/a278984.txt\"\u003eCounting Words that are in \"Standard Order\"\u003c/a\u003e",
				"Peter Bala, \u003ca href=\"/A112007/a112007.txt\"\u003eDiagonals of triangles with generating function exp(t*F(x)).\u003c/a\u003e",
				"J. Fernando Barbero G., Jesús Salas, Eduardo J. S. Villaseñor, \u003ca href=\"http://arxiv.org/abs/1307.2010\"\u003eBivariate Generating Functions for a Class of Linear Recurrences. I. General Structure\u003c/a\u003e, arXiv:1307.2010 [math.CO], 2013.",
				"J. Fernando Barbero G., Jesús Salas, Eduardo J. S. Villaseñor, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v22i3p37\"\u003eGeneralized Stirling permutations and forests: Higher-order Eulerian and Ward numbers\u003c/a\u003e, Electronic Journal of Combinatorics 22(3) (2015), #P3.37.",
				"Tom Copeland, \u003ca href=\"http://tcjpn.wordpress.com/2015/12/21/generators-inversion-and-matrix-binomial-and-integral-transforms/\"\u003eGenerators, Inversion, and Matrix, Binomial, and Integral Transforms\u003c/a\u003e",
				"Tom Copeland, \u003ca href=\"http://tcjpn.wordpress.com/2008/09/13/short-note-on-lagrange-inversion/\"\u003eShort note on Lagrange inversion\u003c/a\u003e",
				"Gesualdo Delfino and Jacopo Viti, \u003ca href=\"http://arxiv.org/abs/1104.4323\"\u003ePotts q-color field theory and scaling random cluster model\u003c/a\u003e, arXiv:1104.4323 [hep-th], 2011.",
				"A. E. Fekete, \u003ca href=\"http://www.jstor.org/stable/2974533\"\u003eApropos two notes on notation\u003c/a\u003e, Amer. Math. Monthly, 101 (1994), 771-778.",
				"F. A. Haight, \u003ca href=\"/A008299/a008299_1.pdf\"\u003eHandbook of the Poisson distribution\u003c/a\u003e, John Wiley, 1967 [Annotated scan of page 7 only. Note that there is an error in the table.]",
				"MathStackExchange, \u003ca href=\"http://math.stackexchange.com/questions/1597737/mahler-polynomials-and-zeros-of-the-incomplete-gamma-function\"\u003eMahler polynomials and the zeros of the incomplete gamma function\u003c/a\u003e, a MathStackExchange question by Tom Copeland, Jan 06 2016.",
				"R. Paris, \u003ca href=\"http://dx.doi.org/10.1016/S0377-0427(02)00553-8\"\u003eA uniform asymptotic expansion for the incomplete gamma function\u003c/a\u003e, Journal of Computational and Applied Mathematics, 148 (2002), p. 223-239 (See 332. From Tom Copeland, Jan 03 2016).",
				"Andrew Elvey Price, Alan D. Sokal, \u003ca href=\"https://arxiv.org/abs/2001.01468\"\u003ePhylogenetic trees, augmented perfect matchings, and a Thron-type continued fraction (T-fraction) for the Ward polynomials\u003c/a\u003e, arXiv:2001.01468 [math.CO], 2020.",
				"L. M. Smiley, \u003ca href=\"http://arXiv.org/abs/math.CO/0006106\"\u003eCompletion of a Rational Function Sequence of Carlitz\u003c/a\u003e, arXiv:math/0006106 [math.CO], 2000.",
				"M. Z. Spivey, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL14/Spivey/spivey31.html\"\u003eOn Solutions to a General Combinatorial Recurrence\u003c/a\u003e, J. Int. Seq. 14 (2011) # 11.9.7.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/MahlerPolynomial.html\"\u003eMahler polynomial\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Mahler_polynomials\"\u003eMahler polynomials\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = abs(A137375(n,k)).",
				"E.g.f. with additional constant 1: exp(t*(exp(x)-1-x)) = 1+t*x^2/2!+t*x^3/3!+(t+3*t^2)*x^4/4!+....",
				"Recurrence relation: T(n+1,k) = k*T(n,k) + n*T(n-1,k-1).",
				"T(n,k) = A134991(n-k,k); A134991(n,k) = T(n+k,k).",
				"More generally, if S_r(n,k) gives the number of set partitions of [n] into k blocks of size at least r then we have the recurrence S_r(n+1,k)=k*S_r(n,k)+binomial(n,r-1)*S_r(n-r+1,k-1) (for this sequence, r=2), with associated e.g.f.: sum(S_r(n,k)*u^k*(t^n/n!), n\u003e=0, k\u003e=0)=exp(u*(e^t-sum(t^i/i!, i=0..r-1))).",
				"T(n,k) = Sum_{i=0..k} (-1)^i*binomial(n, i)*[sum_{j=0..k-i} (-1)^j*(k -i -j)^(n-i)/(j!*(k-i-j)!)]. - _David Wasserman_, Jun 13 2007",
				"G.f.: (R(0)-1)/(x^2*y), where R(k) = 1 - (k+1)*y*x^2/( (k+1)*y*x^2 -(1- k*x)*(1-x - k*x)/R(k+1) ); (continued fraction). - _Sergei N. Gladkovskii_, Nov 09 2013",
				"T(n,k) = Sum_{i=0..min(n,k)} (-1)^i * binomial(n,i) * stirling2(n-i,k-i) = Sum_{i=0..min(n,k)} (-1)^i * A007318(n,i) * A008277(n-i,k-i). - _Max Alekseyev_, Feb 27 2017",
				"T(n, k) = Sum_{j=0..n-k} binomial(j, n-2*k)*E2(n-k, n-k-j) where E2(n, k) are the second-order Eulerian numbers A340556. - _Peter Luschny_, Feb 11 2021"
			],
			"example": [
				"There are 3 ways of partitioning a set N of cardinality 4 into 2 blocks each of cardinality at least 2, so T(4,2)=3. Table begins:",
				"1;",
				"1;",
				"1, 3;",
				"1, 10;",
				"1, 25, 15;",
				"1, 56, 105;",
				"1, 119, 490, 105;",
				"1, 246, 1918, 1260;",
				"1, 501, 6825, 9450, 945;",
				"1, 1012, 22935, 56980, 17325;",
				"1, 2035, 74316, 302995, 190575, 10395;",
				"1, 4082, 235092, 1487200, 1636635, 270270;",
				"1, 8177, 731731, 6914908, 12122110, 4099095, 135135;",
				"...",
				"Reading the table by diagonals produces the triangle A134991."
			],
			"maple": [
				"A008299 := proc(n,k) local i,j,t1; if k\u003c1 or k\u003efloor(n/2) then t1 := 0; else",
				"t1 := add( (-1)^i*binomial(n, i)*add( (-1)^j*(k - i - j)^(n - i)/(j!*(k - i - j)!), j = 0..k - i), i = 0..k); fi; t1; end; # _N. J. A. Sloane_, Dec 06 2016",
				"G:= exp(lambda*(exp(x)-1-x)):",
				"S:= series(G,x,21):",
				"seq(seq(coeff(coeff(S,x,n)*n!,lambda,k),k=1..floor(n/2)),n=2..20); # _Robert Israel_, Jan 15 2020",
				"T := proc(n, k) option remember; if n \u003c 0 then return 0 fi; if k = 0 then return k^n fi; k*T(n-1, k) + (n-1)*T(n-2, k-1) end:",
				"seq(seq(T(n,k), k=1..n/2), n=2..9); # _Peter Luschny_, Feb 11 2021"
			],
			"mathematica": [
				"t[n_, k_] := Sum[ (-1)^i*Binomial[n, i]*Sum[ (-1)^j*(k - i - j)^(n - i)/(j!*(k - i - j)!), {j, 0, k - i}], {i, 0, k}]; Flatten[ Table[ t[n, k], {n, 2, 14}, {k, 1, Floor[n/2]}]] (* _Jean-François Alcover_, Oct 13 2011, after _David Wasserman_ *)",
				"Table[Sum[Binomial[n, k - j] StirlingS2[n - k + j, j] (-1)^(j + k), {j, 0, k}], {n, 15}, {k, n/2}] // Flatten (* _Eric W. Weisstein_, Nov 13 2018 *)"
			],
			"program": [
				"(PARI) {T(n, k) = if( n \u003c 1 || 2*k \u003e n, n==0 \u0026\u0026 k==0, sum(i=0, k, (-1)^i * binomial( n, i) * sum(j=0, k-i, (-1)^j * (k-i-j)^(n-i) / (j! * (k-i-j)!))))}; /* _Michael Somos_, Oct 19 2014 */",
				"(PARI) { T(n,k) = sum(i=0,min(n,k), (-1)^i * binomial(n,i) * stirling(n-i,k-i,2) ); } /* _Max Alekseyev_, Feb 27 2017 */"
			],
			"xref": [
				"Rows: A000247 (k=2), A000478 (k=3), A058844 (k=4).",
				"Row sums: A000296, diagonal: A259877.",
				"Cf. A059022, A059023, A059024, A059025, A134991, A137375, A200091, A269939, A340556."
			],
			"keyword": "nonn,tabf,nice,easy",
			"offset": "2,4",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Formula and cross-references from Barbara Haas Margolius (margolius(AT)math.csuohio.edu), Dec 14 2000",
				"Edited by _Peter Bala_, Dec 04 2011",
				"Edited by _N. J. A. Sloane_, Jan 24 2020"
			],
			"references": 30,
			"revision": 123,
			"time": "2021-02-12T00:11:54-05:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}