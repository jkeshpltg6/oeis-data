{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A337889",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 337889,
			"data": "0,0,0,0,0,0,0,1,40927,0,0,20,731279799,314824333015938998688,0,0,120,732272925320,38491882659300767730994725249684096,38343035259947576596859560773963975000551460473665493534170658111488,0",
			"name": "Array read by descending antidiagonals: T(n,k) is the number of chiral pairs of colorings of the square faces of a regular n-dimensional orthotope (hypercube) using k or fewer colors.",
			"comment": [
				"Each member of a chiral pair is a reflection, but not a rotation, of the other. Each face is a square bounded by four edges. For n=2, the figure is a square with one face. For n=3, the figure is a cube with 6 faces. For n=4, the figure is a tesseract with 24 faces. The number of faces is 2^(n-2)*C(n,2).",
				"Also the number of chiral pairs of colorings of peaks of an n-dimensional orthoplex. A peak is an (n-3)-dimensional simplex.",
				"The algorithm used in the Mathematica program below assigns each permutation of the axes to a partition of n and then considers separate conjugacy classes for axis reversals. It uses the formulas in Balasubramanian's paper. If the value of m is increased, one can enumerate colorings of higher-dimensional elements beginning with T(m,1)."
			],
			"link": [
				"K. Balasubramanian, \u003ca href=\"https://doi.org/10.33187/jmsm.471940\"\u003eComputational enumeration of colorings of hyperplanes of hypercubes for all irreducible representations and applications\u003c/a\u003e, J. Math. Sci. \u0026 Mod. 1 (2018), 158-180."
			],
			"formula": [
				"T(n,k) = A337887(n,k) - A337888(n,k) = (A337887(n,k) - A337890(n,k)) / 2 = A337888(n,k) - A337890(n,k)."
			],
			"example": [
				"Array begins with T(2,1):",
				"0     0         0            0               0                 0 ...",
				"0     0         1           20             120               455 ...",
				"0 40927 731279799 732272925320 155180061396500 12338466190481025 ..."
			],
			"mathematica": [
				"m=2; (* dimension of color element, here a square face *)",
				"Fi1[p1_] := Module[{g, h}, Coefficient[Product[g = GCD[k1, p1]; h = GCD[2 k1, p1]; (1+2x^(k1/g))^(r1[[k1]] g) If[Divisible[k1, h], 1, (1+2x^(2 k1/h))^(r2[[k1]] h/2)], {k1, Flatten[Position[cs, n1_ /; n1 \u003e 0]]}], x, n-m]];",
				"FiSum[] := (Do[Fi2[k2] = Fi1[k2], {k2, Divisors[per]}]; DivisorSum[per, DivisorSum[d1 = #, MoebiusMu[d1/#] Fi2[#] \u0026]/# \u0026]);",
				"CCPol[r_List] := (r1 = r; r2 = cs - r1; per = LCM @@ Table[If[cs[[j2]] == r1[[j2]], If[0 == cs[[j2]],1,j2], 2j2], {j2,n}]; If[EvenQ[Sum[If[EvenQ[j3], r1[[j3]], r2[[j3]]], {j3,n}]],1,-1]Times @@ Binomial[cs, r1] 2^(n-Total[cs]) b^FiSum[]);",
				"PartPol[p_List] := (cs = Count[p, #]\u0026/@ Range[n]; Total[CCPol[#]\u0026/@ Tuples[Range[0,cs]]]);",
				"pc[p_List] := Module[{ci, mb}, mb = DeleteDuplicates[p]; ci = Count[p, #]\u0026/@ mb; n!/(Times@@(ci!) Times@@(mb^ci))] (*partition count*)",
				"row[n_Integer] := row[n] = Factor[(Total[(PartPol[#] pc[#])\u0026/@ IntegerPartitions[n]])/(n! 2^n)]",
				"array[n_, k_] := row[n] /. b -\u003e k",
				"Table[array[n,d+m-n], {d,6}, {n,m,d+m-1}] // Flatten"
			],
			"xref": [
				"Cf. A337887 (oriented), A337888 (unoriented), A337890 (achiral).",
				"Other elements: A325014 (vertices), A337409 (edges).",
				"Other polytopes: A337885 (simplex), A337893 (orthoplex).",
				"Rows 2-4 are A000004, A093566(n+1), A331356."
			],
			"keyword": "tabl,nonn",
			"offset": "2,9",
			"author": "_Robert A. Russell_, Sep 28 2020",
			"references": 7,
			"revision": 10,
			"time": "2020-09-29T10:05:39-04:00",
			"created": "2020-09-28T21:41:42-04:00"
		}
	]
}