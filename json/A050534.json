{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A050534",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 50534,
			"data": "0,0,0,3,15,45,105,210,378,630,990,1485,2145,3003,4095,5460,7140,9180,11628,14535,17955,21945,26565,31878,37950,44850,52650,61425,71253,82215,94395,107880,122760,139128,157080,176715,198135,221445,246753,274170,303810,335790",
			"name": "Tritriangular numbers: a(n) = binomial(binomial(n,2),2) = n*(n+1)*(n-1)*(n-2)/8.",
			"comment": [
				"\"There are n straight lines in a plane, no two of which are parallel and no three of which are concurrent. Their points of intersection being joined, show that the number of new lines drawn is (1/8)n(n-1)(n-2)(n-3).\" (Schmall, 1915).",
				"Several different versions of this sequence are possible, beginning with either one, two or three 0's.",
				"If Y is a 3-subset of an n-set X then, for n\u003e=6, a(n-4) is the number of (n-6)-subsets of X which have exactly one element in common with Y. - _Milan Janjic_, Dec 28 2007",
				"Number of distinct ways to select 2 pairs of objects from a set of n+1 objects, when order doesn't matter. For example, with n = 3 (4 objects), the 3 possibilities are (12)(34), (13)(24), and (14)(23). - _Brian Parsonnet_, Jan 03 2012",
				"Partial sums of A027480. - _J. M. Bergot_, Jul 09 2013",
				"For the set {1,2,...,n}, the sum of the 2 smallest elements of all subsets with 3 elements is a(n) (see Bulut et al. link). - _Serhat Bulut_, Jan 20 2015",
				"a(n) is also the number of subgroups of S_{n+1} (the symmetric group on n+1 elements) that are isomorphic to D_4 (the dihedral group of order 8). - _Geoffrey Critzer_, Sep 13 2015",
				"a(n) is the coefficient of x1^(n-3)*x2^2 in exponential Bell polynomial B_{n+1}(x1,x2,...) (number of ways to select 2 pairs among n+1 objects, see above), hence its link with A000292 and A001296 (see formula). - _Cyril Damamme_, Feb 26 2018",
				"Also the number of 4-cycles in the complete graph K_{n+1}. - _Eric W. Weisstein_, Mar 13 2018",
				"Number of chiral pairs of colorings of the 4 edges or vertices of a square using n or fewer colors. Each member of a chiral pair is a reflection, but not a rotation, of the other. - _Robert A. Russell_, Oct 20 2020"
			],
			"reference": [
				"Arthur T. Benjamin and Jennifer Quinn, Proofs that really count: the art of combinatorial proof, M.A.A. 2003, id. 154.",
				"Louis Comtet, Advanced Combinatorics, Reidel, 1974, Problem 1, page 72.",
				"Richard P. Stanley, Enumerative Combinatorics, Cambridge, Vol. 2, 1999; see Problem 5.5, case k=2."
			],
			"link": [
				"William A. Tedeschi, \u003ca href=\"/A050534/b050534.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Serhat Bulut and Oktay Erkan Temizkan, \u003ca href=\"https://web.archive.org/web/20160708101054/http://matematikproje.com/dosyalar/7e1cdSubset_smallest_elements_Sum.pdf\"\u003eSubset Sum Problem\u003c/a\u003e, Jan 20 2015.",
				"A. Burstein, S. Kitaev and T. Mansour, \u003ca href=\"http://puma.dimai.unifi.it/19_2_3/3.pdf\"\u003ePartially ordered patterns and their combinatorial interpretations\u003c/a\u003e, PU. M. A., Vol. 19, No. 2-3 (2008), pp. 27-38.",
				"Louis H. Kauffman, \u003ca href=\"http://arxiv.org/abs/1109.1085\"\u003eNon-Commutative Worlds-Classical Constraints, Relativity and the Bianchi Identity\u003c/a\u003e, arXiv preprint arXiv:1109.1085 [math-ph], 2011. (See Appendix)",
				"Alexander Kreinin, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL19/Kreinin/kreinin4.html\"\u003eInteger Sequences and Laplace Continued Fraction\u003c/a\u003e, Journal of Integer Sequences, Vol. 19 (2016), Article 16.6.2.",
				"Ronald Orozco López, \u003ca href=\"https://www.researchgate.net/publication/350397609_Solution_of_the_Differential_Equation_ykeay_Special_Values_of_Bell_Polynomials_and_ka-Autonomous_Coefficients\"\u003eSolution of the Differential Equation y^(k)= e^(a*y), Special Values of Bell Polynomials and (k,a)-Autonomous Coefficients\u003c/a\u003e, Universidad de los Andes (Colombia 2021).",
				"Frank Ruskey and Jennifer Woodcock, \u003ca href=\"http://dx.doi.org/10.1007/978-3-642-25011-8_23\"\u003eThe Rand and block distances of pairs of set partitions\u003c/a\u003e, in Combinatorial Algorithms, 287-299, Lecture Notes in Comput. Sci., 7056, Springer, Heidelberg, 2011.",
				"C. N. Schmall, \u003ca href=\"http://www.jstor.org/stable/2973734\"\u003eProblem 432\u003c/a\u003e, The American Mathematical Monthly, Vol. 22, No. 4 (1915), p. 130.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CompleteGraph.html\"\u003eComplete Graph\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/GraphCycle.html\"\u003eGraph Cycle\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/TritriangularNumber.html\"\u003eTritriangular Number\u003c/a\u003e.",
				"Chai Wah Wu, \u003ca href=\"http://arxiv.org/abs/1407.5663\"\u003eGraphs whose normalized Laplacian matrices are separable as density matrices in quantum mechanics\u003c/a\u003e, arXiv:1407.5663 [quant-ph], 2014.",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (5,-10,10,-5,1)."
			],
			"formula": [
				"a(n) = 3*binomial(n+1, 4) = 3*A000332(n+1).",
				"From _Vladeta Jovovic_, May 03 2002: (Start)",
				"Recurrence: a(n) = 5*a(n-1) - 10*a(n-2) + 10*a(n-3) - 5*a(n-4) + a(n-5).",
				"G.f.: 3*x^3 / (1-x)^5. (End)",
				"a(n+1) = T(T(n)) - T(n); a(n+2) = T(T(n)+n) where T is A000217. - _Jon Perry_, Jun 11 2003",
				"a(n+1) = T(n)^2 - T(T(n)) where T is A000217. - _Jon Perry_, Jul 23 2003",
				"a(n) = T(T(n-1)-1) where T is A000217. - _Jon E. Schoenfield_, Dec 14 2014",
				"a(n) = 3*C(n, 4) + 3*C(n, 3), for n\u003e3.",
				"From _Alexander Adamchuk_, Apr 11 2006: (Start)",
				"a(n) = (1/2)*Sum_{k=1..n} k*(k-1)*(k-2).",
				"a(n) = A033487(n-2)/2, n\u003e1.",
				"a(n) = C(n-1,2)*C(n+1,2)/2, n\u003e2. (End)",
				"a(n) = A052762(n+1)/8. - _Zerinvary Lajos_, Apr 26 2007",
				"a(n) = (4x^4 - 4x^3 - x^2 + x)/2 where x = floor(n/2)*(-1)^n for n \u003e= 0. - _William A. Tedeschi_, Aug 24 2010",
				"E.g.f.: x^3*exp(x)*(4+x)/8. - _Robert Israel_, Nov 01 2015",
				"a(n) = Sum_{k=1..n} Sum_{i=1..k} (n-i-1)*(n-k). - _Wesley Ivan Hurt_, Sep 12 2017",
				"a(n) = A001296(n-1) - A000292(n-1). - _Cyril Damamme_, Feb 26 2018",
				"Sum_{n\u003e=3} 1/a(n) = 4/9. - _Vaclav Kotesovec_, May 01 2018",
				"a(n) = A006528(n) - A002817(n) = (A006528(n) - A002411(n)) / 2 = A002817(n) - A002411(n). - _Robert A. Russell_, Oct 20 2020",
				"Sum_{n\u003e=3} (-1)^(n+1)/a(n) = 32*log(2)/3 - 64/9. - _Amiram Eldar_, Jan 09 2022"
			],
			"example": [
				"For a(3)=3, the chiral pairs of square colorings are AABC-AACB, ABBC-ACBB, and ABCC-ACCB. - _Robert A. Russell_, Oct 20 2020"
			],
			"maple": [
				"[seq(binomial(n+1,4)*3,n=0..40)]; # _Zerinvary Lajos_, Jul 18 2006"
			],
			"mathematica": [
				"Table[Binomial[Binomial[n, 2], 2], {n, 0, 50}] (* _Stefan Steinerberger_, Apr 08 2006 *)",
				"LinearRecurrence[{5, -10, 10, -5, 1}, {0, 0, 0, 3, 15}, 40] (* _Harvey P. Dale_, Dec 14 2011 *)",
				"(* Start from _Eric W. Weisstein_, Mar 13 2018 *)",
				"Binomial[Binomial[Range[0, 20], 2], 2]",
				"Nest[Binomial[#, 2] \u0026, Range[0, 20], 2]",
				"Nest[PolygonalNumber[# - 1] \u0026, Range[0, 20], 2]",
				"CoefficientList[Series[3 x^3/(1 - x)^5, {x, 0, 20}], x]",
				"(* End *)"
			],
			"program": [
				"(Sage) [(binomial(binomial(n,2),2)) for n in range(0, 39)] # _Zerinvary Lajos_, Nov 30 2009",
				"(PARI) a(n)=n*(n+1)*(n-1)*(n-2)/8 \\\\ _Charles R Greathouse IV_, Nov 20 2012",
				"(MAGMA) [3*Binomial(n+1, 4): n in [0..40]]; // _Vincenzo Librandi_, Feb 14 2015",
				"(PARI) x='x+O('x^100); concat([0, 0, 0], Vec(3*x^3/(1-x)^5)) \\\\ _Altug Alkan_, Nov 01 2015",
				"(GAP) List([0..40],n-\u003e3*Binomial(n+1,4)); # _Muniru A Asiru_, Mar 20 2018"
			],
			"xref": [
				"Cf. A000217, A000332, A033487, A107394, A034827, A210569, Second column of triangle A001498.",
				"Cf. similar sequences listed in A241765.",
				"Cf. (square colorings) A006528 (oriented), A002817 (unoriented), A002411 (achiral),",
				"Row 2 of A325006 (orthoplex facets, orthotope vertices) and A337409 (orthotope edges, orthoplex ridges).",
				"Row 4 of A293496 (cycles of n colors using k or fewer colors)."
			],
			"keyword": "easy,nice,nonn,changed",
			"offset": "0,4",
			"author": "Klaus Strassburger (strass(AT)ddfi.uni-duesseldorf.de), Dec 29 1999",
			"ext": [
				"Additional comments from Antreas P. Hatzipolakis, May 03 2002"
			],
			"references": 48,
			"revision": 187,
			"time": "2022-01-09T03:20:07-05:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}