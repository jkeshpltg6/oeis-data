{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A162741",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 162741,
			"data": "1,1,1,1,1,2,2,1,1,1,3,4,3,2,1,1,1,4,7,7,5,3,2,1,1,1,5,11,14,12,8,5,3,2,1,1,1,6,16,25,26,20,13,8,5,3,2,1,1,1,7,22,41,51,46,33,21,13,8,5,3,2,1,1,1,8,29,63,92,97,79,54,34,21,13,8,5,3,2,1,1",
			"name": "Fibonacci-Pascal triangle; same as Pascal triangle, but beginning another Pascal triangle to the right of each row starting at row 2.",
			"comment": [
				"Intertwined Pascal-triangles;",
				"the first five rows seen as numbers in decimal representation: row(n) = 110*row(n-1) + 1. - corrected by _Reinhard Zumkeller_, Jul 16 2013"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A162741/b162741.txt\"\u003eRows n = 1..100 of triangle, flattened\u003c/a\u003e",
				"Richard L. Ollerton and Anthony G. Shannon, \u003ca href=\"http://www.fq.math.ca/Scanned/36-2/ollerton.pdf\"\u003eSome properties of generalized Pascal squares and triangles\u003c/a\u003e, Fib. Q., 36 (1998), 98-109. See Table 3.",
				"\u003ca href=\"/index/Pas#Pascal\"\u003eIndex entries for triangles and arrays related to Pascal's triangle\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = T(n-1,k-1) + T(n-1,k), T(n,1)=1 and for n\u003e1: T(n,2*n-2) = T(n,2*n-1)=1. - _Reinhard Zumkeller_, Jul 16 2013"
			],
			"example": [
				".                                           1",
				".                                       1,  1, 1",
				".                                   1,  2,  2, 1, 1",
				".                               1,  3,  4,  3, 2, 1, 1",
				".                           1,  4,  7,  7,  5, 3, 2, 1, 1",
				".                       1,  5, 11, 14, 12,  8, 5, 3, 2, 1, 1",
				".                   1,  6, 16, 25, 26, 20, 13, 8, 5, 3, 2, 1,1",
				".               1,  7, 22, 41, 51, 46, 33, 21,13, 8, 5, 3, 2,1,1",
				".           1,  8, 29, 63, 92, 97, 79, 54, 34,21,13, 8, 5, 3,2,1,1",
				".       1,  9, 37, 92,155,189,176,133, 88, 55,34,21,13, 8, 5,3,2,1,1",
				".    1,10, 46,129,247,344,365,309,221,143, 89,55,34,21,13, 8,5,3,2,1,1",
				". 1,11,56,175,376,591,709,674,530,364,232,144,89,55,34,21,13,8,5,3,2,1,1 ."
			],
			"mathematica": [
				"T[_, 1] = 1; T[n_, k_] /; k == 2*n-2 || k == 2*n-1 = 1; T[n_, k_] := T[n, k] = T[n-1, k-1] + T[n-1, k]; Table[T[n, k], {n, 1, 9}, {k, 1, 2*n-1}] // Flatten (* _Jean-François Alcover_, Oct 30 2017, after _Reinhard Zumkeller_ *)"
			],
			"program": [
				"(Haskell)",
				"a162741 n k = a162741_tabf !! (n-1) !! (k-1)",
				"a162741_row n = a162741_tabf !! (n-1)",
				"a162741_tabf = iterate",
				"   (\\row -\u003e zipWith (+) ([0] ++ row ++ [0]) (row ++ [0,1])) [1]",
				"-- _Reinhard Zumkeller_, Jul 16 2013"
			],
			"xref": [
				"Cf. A005408 (row length), A000225 (row sums), A000045 (central terms), A007318, A136431.",
				"Cf. A021113. - _Mark Dols_, Jul 18 2009",
				"Some other Fibonacci-Pascal triangles: A027926, A036355, A037027, A074829, A105809, A109906, A111006, A114197, A228074."
			],
			"keyword": "nonn,tabf",
			"offset": "1,6",
			"author": "_Mark Dols_, Jul 12 2009, Jul 19 2009",
			"references": 19,
			"revision": 23,
			"time": "2017-11-23T05:13:16-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}