{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A337358",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 337358,
			"data": "0,0,2,2,2,2,8,1,9,0,10,10,10,5,5,5,5,5,5,5,17,17,17,17,17,2,26,26,26,5,5,5,37,4,20,20,20,8,46,10,50,10,52,9,53,8,54,7,55,6,56,5,57,4,58,3,59,2,60,1,61,1,1,64,0,25,25,25,25,25,25,96,24,72,2,74,74,74",
			"name": "Imaginary part of a two-dimensional (complex) analog of the Recamán sequence (Full definition in the Comments).",
			"comment": [
				"This is a sequence of Gaussian integers - complex numbers a+bi, such that both a and b are integers.  It is a complex analog of the Recamán sequence.  Starting with n(0)=0 (0+0i), each subsequent value is an absolute distance n away from the previous value.  The following restrictions are applied:",
				"* For each value a+bi, both a and b must be nonnegative.",
				"* The specific value chosen is the one with the shortest absolute distance from the origin (0+0i).",
				"* In the case of a value where a and b are equal, both (a+n)=bi and a+(b+n)i will be equally close to the origin, so the arbitrary decision is made to increase the real value. If the opposite decision is made, the sequence is the same, reflected across the a+ai diagonal (switching the sequence of the real parts and the imaginary parts).  As far as this author has been able to trace the sequence so far, the only place where this happens is at n(0)=0, so n(1)=1 (instead of i).",
				"This is the sequence of the imaginary, or \"b\" values of these Gaussian integers.  The real, \"a\" values are found in sequence A335775.",
				"The original Recamán sequence has the added restriction that when making a step toward 0, the number arrived at cannot have previously appeared in the sequence.  If this restriction is lifted for the Recamán sequence, it becomes far less interesting, and forms a predictable pattern.  For this complex version of the Recamán sequence, not including this restriction does not render the sequence uninteresting.  The two-dimensional plane (one quarter of it) provides plenty of space for the sequence to expand into.  Even when a number is revisited, the sequence still does not fall into a predictable pattern.  Therefore, no such restriction was placed on this sequence."
			],
			"example": [
				"After the initial value 0 (0+0i), the next term is 1+0i, being a distance of 1 away from the previous value.  The next term is 1+2i, being a distance of 2 away from the previous term, and closer to the origin point than 3+0i.  3+0i is 3 away from the origin, while 1+2i is only sqrt(5), ~2.236 away from the origin.",
				"The next term is then 4+2i, being 3 away from the previous term, and closer to the origin than 1+5i.",
				"The next term is 0+2i, being 4 away from the previous term, and getting much closer to the origin.",
				"The next term is 5+2i.  And so on.",
				"Eventually, the sequence comes to the term 4+10i, being 12 away from the previous term.  The term after that is 16+5i, being exactly 13 away, and taking advantage of the 5,12,13 Pythagorean Triple.  Of all the Gaussian integers 13 away from the previous term 4+10i, and with nonnegative real and imaginary parts, 16+5i is the one closest to the origin.",
				"At many points in this sequence, a diagonal leap is made along one of the Pythagorean hypotenuses."
			],
			"xref": [
				"Cf. A335775 (the real part), A005132 (Recamán's sequence)."
			],
			"keyword": "nonn,hear",
			"offset": "0,3",
			"author": "_Philip Fleischmann_, Aug 24 2020",
			"references": 1,
			"revision": 16,
			"time": "2021-01-09T22:45:32-05:00",
			"created": "2020-09-08T03:40:20-04:00"
		}
	]
}