{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003958",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3958,
			"data": "1,1,2,1,4,2,6,1,4,4,10,2,12,6,8,1,16,4,18,4,12,10,22,2,16,12,8,6,28,8,30,1,20,16,24,4,36,18,24,4,40,12,42,10,16,22,46,2,36,16,32,12,52,8,40,6,36,28,58,8,60,30,24,1,48,20,66,16,44,24,70,4,72,36,32,18,60,24,78,4,16",
			"name": "If n = Product p(k)^e(k) then a(n) = Product (p(k)-1)^e(k).",
			"comment": [
				"Completely multiplicative.",
				"a(n) = A000010(n) iff n is squarefree (see A005117). - _Reinhard Zumkeller_, Nov 05 2004",
				"Dirichlet inverse of A097945. - _R. J. Mathar_, Aug 29 2011",
				"a(n) = abs(A125131(n)). - _Tom Edgar_, May 26 2014"
			],
			"link": [
				"T. D. Noe and Daniel Forgues, \u003ca href=\"/A003958/b003958.txt\"\u003eTable of n, a(n) for n = 1..100000\u003c/a\u003e (first 1000 terms from T. D. Noe)",
				"Vaclav Kotesovec, \u003ca href=\"/A003958/a003958.jpg\"\u003eGraph - the asymptotic ratio (10^7 terms)\u003c/a\u003e",
				"\u003ca href=\"/index/Di#divseq\"\u003eIndex to divisibility sequences\u003c/a\u003e"
			],
			"formula": [
				"Multiplicative with a(p^e) = (p-1)^e. - _David W. Wilson_, Aug 01 2001",
				"Sum_{k=1..n} a(k) ~ c * n^2, where c = Pi^4 / (315 * zeta(3)) = 0.25725505075419... - _Vaclav Kotesovec_, Jun 14 2020"
			],
			"maple": [
				"a:= n-\u003e mul((i[1]-1)^i[2], i=ifactors(n)[2]):",
				"seq(a(n), n=1..80);  # _Alois P. Heinz_, Sep 13 2017"
			],
			"mathematica": [
				"DirichletInverse[f_][1] = 1/f[1]; DirichletInverse[f_][n_] := DirichletInverse[f][n] = -1/f[1]*Sum[ f[n/d]*DirichletInverse[f][d], {d, Most[ Divisors[n]]}]; muphi[n_] := MoebiusMu[n]*EulerPhi[n]; Table[ DirichletInverse[ muphi][n], {n, 1, 81}] (* _Jean-François Alcover_, Dec 12 2011, after _R. J. Mathar_ *)",
				"a[1] = 1; a[n_] := (fi = FactorInteger[n]; Times @@ ((fi[[All, 1]] - 1)^fi[[All, 2]])); Table[a[n], {n, 1, 50}] (* _G. C. Greubel_, Jun 10 2016 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c1,0,direuler(p=2,n,1/(1-p*X+X))[n]) /* _Ralf Stephan_ */",
				"(Haskell)",
				"a003958 1 = 1",
				"a003958 n = product $ map (subtract 1) $ a027746_row n",
				"-- _Reinhard Zumkeller_, Apr 09 2012, Mar 02 2012"
			],
			"xref": [
				"Cf. A003959, A168065, A168066, A027746, A006093, A027748, A124010."
			],
			"keyword": "nonn,mult,nice",
			"offset": "1,3",
			"author": "_Marc LeBrun_",
			"ext": [
				"Definition reedited (from formula) by _Daniel Forgues_, Nov 17 2009"
			],
			"references": 80,
			"revision": 57,
			"time": "2020-06-14T15:15:08-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}