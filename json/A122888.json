{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A122888",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 122888,
			"data": "1,1,1,1,2,2,1,1,3,6,9,10,8,4,1,1,4,12,30,64,118,188,258,302,298,244,162,84,32,8,1,1,5,20,70,220,630,1656,4014,8994,18654,35832,63750,105024,160120,225696,293685,352074,387820,391232,359992,300664,226580",
			"name": "Triangle, read by rows, where row n lists the coefficients of x^k, k=1..2^n, in the n-th iteration of (x + x^2) for n\u003e=0.",
			"comment": [
				"T(n, k) is the number of strings of length k-1 on the alphabet {1, 2, ..., n} such that between every two occurrences of a letter i there is an occurrence of a letter strictly larger than i. For example, for n = 3, k = 4 we have the strings 121, 131, 232 and the six permutations of 123. - _Joel B. Lewis_, May 06 2008"
			],
			"link": [
				"Paul D. Hanna, \u003ca href=\"/A122888/b122888.txt\"\u003eTable of n, a(n) for n = 0..2046\u003c/a\u003e (rows 0..10)",
				"Art of Problem Solving forum, \u003ca href=\"http://www.artofproblemsolving.com/Forum/viewtopic.php?t=203662\"\u003eStrings on [n] with certain restrictions\u003c/a\u003e."
			],
			"formula": [
				"T(n,k) = [x^k] F_n(x) where F_{n+1}(x) = F_n(x+x^2) for n\u003e=1, with F_0(x)=x."
			],
			"example": [
				"Triangle begins:",
				"1;",
				"1, 1;",
				"1, 2, 2, 1;",
				"1, 3, 6, 9, 10, 8, 4, 1;",
				"1, 4, 12, 30, 64, 118, 188, 258, 302, 298, 244, 162, 84, 32, 8, 1;",
				"1, 5, 20, 70, 220, 630, 1656, 4014, 8994, 18654, 35832, 63750,...;",
				"1, 6, 30, 135, 560, 2170, 7916, 27326, 89582, 279622, 832680,...;",
				"1, 7, 42, 231, 1190, 5810, 27076, 121023, 520626, 2161158,...;",
				"1, 8, 56, 364, 2240, 13188, 74760, 409836, 2179556, 11271436,...;",
				"1, 9, 72, 540, 3864, 26628, 177744, 1153740, 7303164, 45179508,...;",
				"1, 10, 90, 765, 6240, 49260, 378312, 2836548, 20817588,...; ...",
				"Multiplying the g.f. of column k by (1-x)^k, k\u003e=1, with leading zeros,",
				"yields the g.f. of row k in the triangle A122890:",
				"1;",
				"0, 1;",
				"0, 0, 2;",
				"0, 0, 1, 5;",
				"0, 0, 0, 10, 14;",
				"0, 0, 0, 8, 70, 42;",
				"0, 0, 0, 4, 160, 424, 132;",
				"0, 0, 0, 1, 250, 1978, 2382, 429;",
				"0, 0, 0, 0, 302, 6276, 19508, 12804, 1430; ...",
				"in which the main diagonal is the Catalan numbers",
				"and the row sums form the factorials."
			],
			"maple": [
				"b:= proc(n) option remember; `if`(n=0, x,",
				"      expand((x-\u003e x+x^2)(b(n-1))))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=1..degree(p)))(b(n)):",
				"seq(T(n), n=0..5);  # _Alois P. Heinz_, Mar 14 2016"
			],
			"mathematica": [
				"f[0][x_] = x; f[n_][x_] := f[n][x] = f[n-1][x+x^2]; row[n_] := CoefficientList[f[n][x], x] // Rest; Table[row[n], {n, 0, 5} ] // Flatten (* _Jean-François Alcover_, Sep 10 2012 *)"
			],
			"program": [
				"(PARI) {T(n,k)=local(F=x+x^2, G=x+x*O(x^k)); if(n\u003c0, 0, for(i=1, n, G=subst(F, x, G)); return(polcoeff(G, k, x)))}",
				"for(n=0, 6, for(k=1, 2^n, print1(T(n, k), \", \")); print(\"\"))",
				"(Maxima) T(m,n):=if m=0 and n=1 then 1 else if m=0 and n\u003e1 then 0 else  if m=1 then binomial(1,n-1) else sum(binomial(i,n-i)*T(m-1,i),i,1,n); [_Vladimir Kruchinin_, May 19 2012]"
			],
			"xref": [
				"Cf. A007018 (row sums), diagonals: A112317, A112319, A122887; A092123 (largest term in row); A122889 (antidiagonal sums); A122890 (related triangle)."
			],
			"keyword": "nonn,tabf",
			"offset": "0,5",
			"author": "_Paul D. Hanna_, Sep 18 2006",
			"ext": [
				"Name changed slightly by _Paul D. Hanna_, Apr 29 2013"
			],
			"references": 33,
			"revision": 27,
			"time": "2016-03-19T10:32:11-04:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}