{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A226518",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 226518,
			"data": "0,1,0,1,0,0,1,0,-1,0,0,1,2,1,2,1,0,0,1,0,1,2,3,2,1,0,1,0,0,1,0,1,2,1,0,-1,-2,-1,0,-1,0,0,1,2,1,2,1,0,-1,0,1,0,-1,-2,-1,-2,-1,0,0,1,0,-1,0,1,2,3,2,3,2,3,2,1,0,-1,0,1,0,0,1,2,3,4,3,4,3,4,5,4,3,4,5,4,3,4,3,4,3,2,1,0",
			"name": "Irregular triangle read by rows: T(n,k) = Sum_{i=0..k} Legendre(i,prime(n)).",
			"comment": [
				"Strictly speaking, the symbol in the definition is the Legendre-Jacobi-Kronecker symbol, since the Legendre symbol is defined only for odd primes.",
				"The classical Polya-Vinogradov theorem gives an upper bound.",
				"There is a famous open problem concerning upper bounds on |T(n,k)| for small k."
			],
			"reference": [
				"R. Ayoub, An Introduction to the Analytic Theory of Numbers, Amer. Math. Soc., 1963; p. 320, Theorem 5.1.",
				"Beck, József. Inevitable randomness in discrete mathematics. University Lecture Series, 49. American Mathematical Society, Providence, RI, 2009. xii+250 pp. ISBN: 978-0-8218-4756-5; MR2543141 (2010m:60026). See page 23.",
				"Elliott, P. D. T. A. Probabilistic number theory. I. Mean-value theorems. Grundlehren der Mathematischen Wissenschaften, 239. Springer-Verlag, New York-Berlin, 1979. xxii+359+xxxiii pp. (2 plates). ISBN: 0-387-90437-9 MR0551361 (82h:10002a). See Vol. 1, p. 154."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A226518/b226518.txt\"\u003eRows n = 1..70, flattened\u003c/a\u003e",
				"D. A. Burgess, \u003ca href=\"http://dx.doi.org/10.1112/S0025579300001157\"\u003eThe distribution of quadratic residues and non-residues\u003c/a\u003e, Mathematika 4, 1957, 106--112. MR0093504 (20 #28)",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Legendre_symbol\"\u003eLegendre symbol\u003c/a\u003e."
			],
			"example": [
				"Triangle begins:",
				"[0, 1],",
				"[0, 1, 0],",
				"[0, 1, 0, -1, 0],",
				"[0, 1, 2, 1, 2, 1, 0],",
				"[0, 1, 0, 1, 2, 3, 2, 1, 0, 1, 0],",
				"[0, 1, 0, 1, 2, 1, 0, -1, -2, -1, 0, -1, 0],",
				"[0, 1, 2, 1, 2, 1, 0, -1, 0, 1, 0, -1, -2, -1, -2, -1, 0],",
				"[0, 1, 0, -1, 0, 1, 2, 3, 2, 3, 2, 3, 2, 1, 0, -1, 0, 1, 0],",
				"..."
			],
			"maple": [
				"with(numtheory);",
				"T:=(n,k)-\u003eadd(legendre(i,ithprime(n)),i=0..k);",
				"f:=n-\u003e[seq(T(n,k),k=0..ithprime(n)-1)];",
				"[seq(f(n),n=1..15)];"
			],
			"mathematica": [
				"Table[p = Prime[n]; Table[JacobiSymbol[k, p], {k, 0, p-1}] // Accumulate, {n, 1, 15}] // Flatten (* _Jean-François Alcover_, Mar 07 2014 *)"
			],
			"program": [
				"(PARI) print(\"# A226518 \");",
				"cnt=1; for(j5=1,9,summ=0; for(i5=0,prime(j5)-1, summ=summ+kronecker(i5,prime(j5)); print(cnt,\"  \",summ); cnt++)); \\\\ _Bill McEachen_, Aug 02 2013",
				"(Haskell)",
				"a226518 n k = a226518_tabf !! (n-1) !! k",
				"a226518_row n = a226518_tabf !! (n-1)",
				"a226518_tabf = map (scanl1 (+)) a226520_tabf",
				"-- _Reinhard Zumkeller_, Feb 02 2014"
			],
			"xref": [
				"Partial sums of rows of triangle in A226520.",
				"Cf. A165582. See A226519 for another version.",
				"Third and fourth columns give A226914, A226915. See also A226523.",
				"Cf. A165477 (131071st row)."
			],
			"keyword": "sign,tabf",
			"offset": "1,13",
			"author": "_N. J. A. Sloane_, Jun 19 2013",
			"references": 7,
			"revision": 44,
			"time": "2015-09-27T10:31:04-04:00",
			"created": "2013-06-19T08:28:31-04:00"
		}
	]
}