{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A187760",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 187760,
			"data": "1,3,2,4,1,3,5,3,2,4,6,4,1,3,5,7,5,3,2,4,6,8,6,4,1,3,5,7,9,7,5,3,2,4,6,8,10,8,6,4,1,3,5,7,9,11,9,7,5,3,2,4,6,8,10,12,10,8,6,4,1,3,5,7,9,11",
			"name": "Table T(n,k) read by antidiagonals. T(n,k)=n-k+1, if n\u003e=k, T(n,k)=k-n+2, if n \u003c k.",
			"comment": [
				"In general, let m be natural number. T(n,k)=n-k+1, if n\u003e=k, T(n,k)=k-n+m-1, if n \u003ck.  Table T(n,k) read by antidiagonals. The first column of the table T(n,1) is the sequence of the natural numbers A000027. In all columns with number k (k \u003e 1) the segment with the length of (k-1): {m+k-2, m+k-3, ..., m}  shifts the sequence A000027. For m=1 the result is A220073, for m=2 the result is A143182. This sequence is the result for m=3."
			],
			"link": [
				"Boris Putievskiy, \u003ca href=\"/A187760/b187760.txt\"\u003eRows n = 1..140 of triangle, flattened\u003c/a\u003e",
				"Boris Putievskiy, \u003ca href=\"http://arxiv.org/abs/1212.2732\"\u003eTransformations Integer Sequences And Pairing Functions\u003c/a\u003e arXiv:1212.2732 [math.CO], 2012."
			],
			"formula": [
				"For the general case, a(n) = |(t+1)^2 - 2n| + m*floor((t^2+3t+2-2n)/(t+1)), where t=floor((-1+sqrt(8*n-7))/2).",
				"For m=3, a(n) = |(t+1)^2 - 2n| + 3*floor((t^2+3t+2-2n)/(t+1)), where t=floor((-1+sqrt(8*n-7))/2)."
			],
			"example": [
				"The start of the sequence as table for the general case:",
				"1....m..m+1..m+2..m+3..m+4..m+5...",
				"2....1....m..m+1..m+2..m+3..m+4...",
				"3....2....1....m..m+1..m+2..m+3...",
				"4....3....2....1....m..m+1..m+2...",
				"5....4....3....2....1....m..m+1...",
				"6....5....4....3....2....1....m...",
				"7....6....5....4....3....2....1...",
				". . .",
				"The start of the sequence as triangle array read by rows for the general case:",
				"1;",
				"m,2;",
				"m+1,1,3;",
				"m+2,m,2,4;",
				"m+3,m+1,1,3,5;",
				"m+4,m+2,m,2,4,6;",
				"m+5,m+3,m+1,1,3,5,7;",
				". . .",
				"Row number r contains r numbers: m+r-2, m+r-4,...r-2,r."
			],
			"mathematica": [
				"T[n_, k_] := If[1 \u003c= k \u003c= n, n - k + 1, k - n + 2];",
				"Table[T[n - k + 1, k], {n, 1, 11}, {k, n, 1, -1}] // Flatten (* _Jean-François Alcover_, Nov 06 2018 *)"
			],
			"program": [
				"(Python)",
				"t=int((math.sqrt(8*n-7)-1)/2)",
				"result=abs((t+1)**2 - 2*n) + 3*int((t**2+3*t+2-2*n)/(t+1))"
			],
			"xref": [
				"Cf. A000027, A220073, A143182."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Boris Putievskiy_, Jan 04 2013",
			"references": 7,
			"revision": 17,
			"time": "2018-11-06T06:44:24-05:00",
			"created": "2013-01-14T12:39:38-05:00"
		}
	]
}