{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A031972",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 31972,
			"data": "0,1,6,39,340,3905,55986,960799,19173960,435848049,11111111110,313842837671,9726655034460,328114698808273,11966776581370170,469172025408063615,19676527011956855056,878942778254232811937,41660902667961039785742,2088331858752553232964199",
			"name": "a(n) = Sum_{k=1..n} n^k.",
			"comment": [
				"Sum of lengths of longest ending contiguous subsequences with the same value over all s in {1,...,n}^n: a(n) = Sum_{k=1..n} k*A228273(n,k). a(2) = 6 = 2+1+1+2: [1,1], [1,2], [2,1], [2,2]. - _Alois P. Heinz_, Aug 19 2013",
				"a(n) is the expected wait time to see the contiguous subword 11...1 (n copies of 1) over all infinite sequences on alphabet {1,2,...,n}. - _Geoffrey Critzer_, May 19 2014",
				"a(n) is the number of sequences of k elements from {1,2,...,n}, where 1\u003c=k\u003c=n. For example, a(2) = 6, counting the sequences, [1], [2], [1,1], [1,2], [2,1], [2,2]. Equivalently, a(n) is the number of bar graphs having a height and width of at most n. - _Emeric Deutsch_, Jan 24 2017.",
				"In base n, a(n) has n+1 digits: n 1's followed by a 0. - _Mathew Englander_, Oct 20 2020"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A031972/b031972.txt\"\u003eTable of n, a(n) for n = 0..386\u003c/a\u003e",
				"A. Blecher, C. Brennan, A. Knopfmacher and H. Prodinger, \u003ca href=\"http://dx.doi.org/10.1016/j.dam.2014.08.026\"\u003eThe height and width of bargraphs\u003c/a\u003e, Discrete Applied Math. 180, (2015), 36-44."
			],
			"formula": [
				"a(0)=0, a(1)=1; for n\u003e1 a(n) = (n^(n+1)-1)/(n-1) - 1. - _Benoit Cloitre_, Aug 17 2002",
				"a(n) = A031973(n)-1 for n\u003e0. - _Robert G. Wilson v_, Apr 15 2015",
				"a(n) = n*A023037(n) = n^n - 1 + A023037(n). - _Mathew Englander_, Oct 20 2020"
			],
			"maple": [
				"a:= n-\u003e `if`(n\u003c2, n, (n^(n+1)-n)/(n-1)):",
				"seq(a(n), n=0..20);  # _Alois P. Heinz_, Aug 15 2013"
			],
			"mathematica": [
				"f[n_]:=Sum[n^k,{k,n}];Array[f,30] (* _Vladimir Joseph Stephan Orlovsky_, Feb 14 2011*)"
			],
			"program": [
				"(Haskell)",
				"a031972 n = sum $ take n $ iterate (* n) n",
				"-- _Reinhard Zumkeller_, Nov 22 2014",
				"(MAGMA) [1] cat [(n^(n+1)-n)/(n-1): n in [2..20]]; // _Vincenzo Librandi_, Apr 16 2015"
			],
			"xref": [
				"Main diagonal of A228275.",
				"Cf. A031973, A228273, A023037, A226238."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_, Dec 11 1999",
			"ext": [
				"a(0)=0 prepended by _Alois P. Heinz_, Oct 22 2019"
			],
			"references": 10,
			"revision": 57,
			"time": "2020-10-27T00:56:01-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}