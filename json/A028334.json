{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A028334",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 28334,
			"id": "N0030",
			"data": "1,1,2,1,2,1,2,3,1,3,2,1,2,3,3,1,3,2,1,3,2,3,4,2,1,2,1,2,7,2,3,1,5,1,3,3,2,3,3,1,5,1,2,1,6,6,2,1,2,3,1,5,3,3,3,1,3,2,1,5,7,2,1,2,7,3,5,1,2,3,4,3,3,2,3,4,2,4,5,1,5,1,3,2,3,4,2,1,2,6,4,2,4,2,3,6,1,9,3,5,3,3,1,3",
			"name": "Differences between consecutive odd primes, divided by 2.",
			"comment": [
				"With an initial zero, gives the numbers of even numbers between two successive primes. - _Giovanni Teofilatto_, Nov 04 2005",
				"Equal to difference between terms in A067076. - _Eric Desbiaux_, Aug 07 2010",
				"The twin prime conjecture is that a(n) = 1 infinitely often. Yitang Zhang has proved that a(n) \u003c 3.5 x 10^7 infinitely often. - _Jonathan Sondow_, May 17 2013",
				"a(n) = 1 if, and only if, n + 1 is in A107770. - _Jason Kimberley_, Nov 13 2015"
			],
			"reference": [
				"Milton Abramowitz and Irene A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, 1964 (and various reprintings), p. 870.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A028334/b028334.txt\"\u003eTable of n, a(n) for n = 2..10000\u003c/a\u003e",
				"Milton Abramowitz and Irene A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"Yitang Zhang, \u003ca href=\"http://dx.doi.org/10.4007/annals.2014.179.3.7\"\u003eBounded gaps between primes\u003c/a\u003e, Annals of Mathematics, Pages 1121-1174 from Volume 179 (2014), Issue 3."
			],
			"formula": [
				"a(n) = (prime(n+1) - prime(n)) / 2, where prime(n) is the n-th prime.",
				"a(n) = A047160(A024675(n-1)). - _Jason Kimberley_, Nov 12 2015",
				"G.f.: (b(x)/((x + 1)/((1 - x)) - 1) - 1 - x/2)/x, where b(x) is the g.f. of A000040. - _Mario C. Enriquez_, Dec 10 2016"
			],
			"example": [
				"23 - 19 = 4, so a(8) = 4/2 = 2.",
				"29 - 23 = 6, so a(9) = 6/2 = 3.",
				"31 - 29 = 2, so a(10) = 2/2 = 1."
			],
			"mathematica": [
				"Table[(Prime[n + 1] - Prime[n])/2, {n, 2, 105}] (* _Robert G. Wilson v_ *)",
				"Differences[Prime[Range[2, 110]]]/2 (* _Harvey P. Dale_, Jan 25 2015 *)"
			],
			"program": [
				"(PARI) vector(10000,i,(prime(i+2)-prime(i+1))/2) \\\\ _Stanislav Sykora_, Nov 05 2014",
				"(MAGMA) [(NthPrime(n+1)-NthPrime(n))/2: n in [2..100]]; // _Vincenzo Librandi_, Dec 12 2016"
			],
			"xref": [
				"Cf. A005521.",
				"Equals A001223(n)/2 for n \u003e 1.",
				"Cf. A000230 (least prime with a gap of 2n to the next prime).",
				"Sequences related to the differences between successive primes: A001223 (Delta(p)), A028334, A080378, A104120, A330556-A330561."
			],
			"keyword": "nonn",
			"offset": "2,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Replaced multiplication by division in the cross-reference _R. J. Mathar_, Jan 23 2010",
				"Definition corrected by _Jonathan Sondow_, May 17 2013",
				"Edited by _Franklin T. Adams-Watters_, Aug 07 2014"
			],
			"references": 28,
			"revision": 71,
			"time": "2020-01-01T23:56:26-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}