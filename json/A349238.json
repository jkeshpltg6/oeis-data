{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A349238",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 349238,
			"data": "0,1,1,1,4,1,6,4,1,9,6,4,12,1,14,9,6,19,4,17,12,1,22,14,9,30,6,27,19,4,25,17,12,33,1,35,22,14,48,9,43,30,6,40,27,19,53,4,38,25,17,51,12,46,33,1,56,35,22,77,14,69,48,9,64,43,30,85,6,61,40,27",
			"name": "Reverse the digits in the Zeckendorf representation of n (A189920).",
			"comment": [
				"Fixed points a(n) = n are the Zeckendorf palindromes n = A094202.",
				"Apart from a(0)=0, all terms end with a 1 digit so are \"odd\" A003622.",
				"a(n) = 1 iff n is a Fibonacci number \u003e= 1 (A000045) since they are Zeckendorf 100..00 which reverses to 00..001.",
				"A given k first occurs as a(n) = k at its reversal n = a(k), and thereafter at this n with any number of least significant 0's appended.",
				"The equivalent reversal in binary is A030101 so that a conversion to Fibbinary (A003714) and back gives a(n) = A022290(A030101(A003714(n))).",
				"A reverse and reverse again loses any least significant 0 digits as per A348853 so that a(a(n)) = A348853(n)."
			],
			"link": [
				"Kevin Ryde, \u003ca href=\"/A349238/b349238.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Kevin Ryde, \u003ca href=\"/A349238/a349238.gp.txt\"\u003ePARI/GP Code\u003c/a\u003e"
			],
			"example": [
				"n    = 1445 = Zeckendorf 101000101001000",
				"a(n) =  313 = Zeckendorf 000100101000101 reversal"
			],
			"program": [
				"(PARI) See links.",
				"(Python)",
				"def NumToFib(n): # n \u003e 0",
				"    f0, f1, k = 1, 1, 0",
				"    while f0 \u003c= n:",
				"        f0, f1, k = f0+f1, f0, k+1",
				"    s = \"\"",
				"    while k \u003e 0:",
				"        f0, f1, k = f1, f0-f1, k-1",
				"        if f0 \u003c= n:",
				"            s, n = s+\"1\", n-f0",
				"        else:",
				"            s = s+\"0\"",
				"    return s",
				"def RevFibToNum(s):",
				"    f0, f1 = 1, 1",
				"    n, k = 0, 0",
				"    while k \u003c len(s):",
				"        if s[k] == \"1\":",
				"            n = n+f0",
				"        f0, f1, k = f0+f1, f0, k+1",
				"    return n",
				"n, a = 0, 0",
				"print(a, end = \", \")",
				"while n \u003c 71:",
				"    n += 1",
				"    print(RevFibToNum(NumToFib(n)), end = \", \") # _A.H.M. Smeets_, Nov 14 2021"
			],
			"xref": [
				"Cf. A189920 (Zeckendorf digits), A094202 (fixed points), A003622 (range), A348853 (delete trailing 0's).",
				"Cf. A003714 (Fibbinary), A022290 (its inverse).",
				"Cf. A343150 (reverse below MSB).",
				"Other base reversals: A030101 (binary), A004086 (decimal)."
			],
			"keyword": "base,easy,nonn",
			"offset": "0,5",
			"author": "_Kevin Ryde_, Nov 11 2021",
			"references": 4,
			"revision": 18,
			"time": "2021-11-20T20:46:47-05:00",
			"created": "2021-11-19T07:56:03-05:00"
		}
	]
}