{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A260418",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 260418,
			"data": "1,2,2,2,1,3,2,4,3,2,3,2,5,2,3,3,2,4,3,3,3,2,5,2,3,3,2,6,3,4,3,5,6,3,3,5,3,5,4,2,5,4,7,3,2,7,4,6,2,2,4,3,8,4,1,2,4,8,6,2,5,2,7,4,4,3,4,5,2,4,5,6,4,3,2,5,2,7,4,5,5,2,5,3,6,5,4,7,3,4,3,5,9,3,4,3,5,11,4,5,5",
			"name": "Number of ways to write 12*n+5 as 4*x^4 + 4*y^2 + z^2, where x is a nonnegative integer, and y and z are positive integers.",
			"comment": [
				"Conjecture: (i) a(n) \u003e 0 for all n = 0,1,2,..., and a(n) = 1 only for n = 0, 4, 54, 159, 289, 999, 1175, 1404, 16391, 39688.",
				"(ii) All the numbers 24*n+4 (n = 0,1,2,...) can be written as 3*x^4+24*y^2+z^2, where x,y,z are integers with x \u003e 0 and z \u003e 0.",
				"(iii) All the numbers 24*n+13 (n = 0,1,2,...) can be written as 3*x^4+9*y^2+z^2 with x,y,z positive integers.",
				"(iv) All the numbers 24*n+r (n = 0,1,2,...) can be written as a*x^4+b*y^2+c*z^2 with x an integer and y and z positive integers, provided that (r,a,b,c) is among the following quadruples: (5,3,1,1), (5,12,4,1), (7,3,6,1), (10,5,1,1), (11,3,8,3), (11,6,3,2), (17,9,4,1).",
				"See A290491 for a similar conjecture."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A260418/b260418.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://math.scichina.com:8081/sciAe/EN/abstract/abstract517007.shtml\"\u003eOn universal sums of polygonal numbers\u003c/a\u003e, Sci. China Math. 58(2015), 1367-1396.",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1502.03056\"\u003eOn universal sums x(ax+b)/2+y(cy+d)/2+z(ez+f)/2\u003c/a\u003e, arXiv:1502.03056 [math.NT], 2015-2017.",
				"Zhi-Wei Sun, \u003ca href=\"http://maths.nju.edu.cn/~zwsun/179b.pdf\"\u003eNew conjectures on representations of integers (I)\u003c/a\u003e, Nanjing Univ. J. Math. Biquarterly 34(2017), no. 2, 97-120."
			],
			"example": [
				"a(4) = 1 since 12*4+5 = 4*0^4 + 4*1^2 + 7^2.",
				"a(54) = 1 since 12*54+5 = 4*0^4 + 4*11^2 + 13^2.",
				"a(159) = 1 since 12*159+5 = 4*0^4 + 4*4^2 + 43^2.",
				"a(289) = 1 since 12*289+5 = 4*1^4 + 4*19^2 + 45^2.",
				"a(999) = 1 since 12*999+5 = 4*7^4 + 4*21^2 + 25^2.",
				"a(1175) = 1 since 12*1175+5 = 4*3^4 + 4*55^2 + 41^2.",
				"a(1404) = 1 since 12*1404+5 = 4*3^4 + 4*10^2 + 127^2.",
				"a(16391) = 1 since 12*16391+5 = 4*5^4 + 4*207^2 + 151^2.",
				"a(39688) = 1 since 12*39688+5 = 4*5^4 + 4*50^2 + 681^2."
			],
			"mathematica": [
				"SQ[n_]:=SQ[n]=IntegerQ[Sqrt[n]];",
				"Do[r=0;Do[If[SQ[12n+5-4x^4-4y^2],r=r+1],{x,0,((12n+5)/4)^(1/4)},{y,1,Sqrt[(12n+5-4x^4)/4]}];Print[n,\" \",r],{n,0,100}]"
			],
			"xref": [
				"Cf. A000290, A000583, A270566, A286885, A286944, A287616, A290342, A290472, A290491."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Zhi-Wei Sun_, Aug 04 2017",
			"references": 2,
			"revision": 25,
			"time": "2017-12-30T09:41:24-05:00",
			"created": "2017-08-04T22:51:34-04:00"
		}
	]
}