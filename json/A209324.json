{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A209324",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 209324,
			"data": "1,1,3,1,9,17,1,45,68,142,1,165,680,710,1569,1,855,6290,8520,9414,21576,1,3843,47600,134190,131796,151032,355081,1,21819,481712,1838900,2372328,2416512,2840648,6805296,1,114075,5025608,21488292,50609664,48934368,51131664,61247664,148869153",
			"name": "Triangular array read by rows: T(n,k) is the number of endofunctions f:{1,2,...,n}-\u003e {1,2,...,n} whose largest component has exactly k nodes; n\u003e=1, 1\u003c=k\u003c=n.",
			"comment": [
				"Here component means weakly connected component in the functional digraph of f.",
				"Row sums are n^n.",
				"T(n,n) = A001865.",
				"For the statistic \"length of the smallest component\", see A347999."
			],
			"reference": [
				"R. Sedgewick and P. Flajolet, Analysis of Algorithms, Addison Wesley, 1996, Chapter 8."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A209324/b209324.txt\"\u003eRows n = 1..141, flattened\u003c/a\u003e",
				"Steven Finch, \u003ca href=\"https://arxiv.org/abs/2111.05720\"\u003ePermute, Graph, Map, Derange\u003c/a\u003e, arXiv:2111.05720 [math.CO], 2021.",
				"D. Panario and B. Richmond, \u003ca href=\"https://doi.org/10.1007/s00453-001-0047-1\"\u003eExact largest and smallest size of components\u003c/a\u003e, Algorithmica, 31 (2001), 413-432."
			],
			"formula": [
				"E.g.f. for column k:  exp( Sum_{n=1..k} A001865(n) x^n/n!) - exp( Sum_{n=1..k-1} A001865(n) x^n/n!).",
				"Sum_{k=1..n} k * T(n,k) = A209327(n). - _Alois P. Heinz_, Dec 16 2021"
			],
			"example": [
				"Triangle T(n,k) begins:",
				"  1;",
				"  1,     3;",
				"  1,     9,     17;",
				"  1,    45,     68,     142;",
				"  1,   165,    680,     710,    1569;",
				"  1,   855,   6290,    8520,    9414,   21576;",
				"  1,  3843,  47600,  134190,  131796,  151032,  355081;",
				"  1, 21819, 481712, 1838900, 2372328, 2416512, 2840648, 6805296;",
				"  ..."
			],
			"maple": [
				"g:= proc(n) option remember; add(n^(n-j)*(n-1)!/(n-j)!, j=1..n) end:",
				"b:= proc(n, m) option remember; `if`(n=0, x^m, add(g(i)*",
				"      b(n-i, max(m, i))*binomial(n-1, i-1), i=1..n))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=1..n))(b(n, 0)):",
				"seq(T(n), n=1..12);  # _Alois P. Heinz_, Dec 16 2021"
			],
			"mathematica": [
				"nn=8;t=Sum[n^(n-1)x^n/n!,{n,1,nn}];c=Log[1/(1-t)];b=Drop[Range[0,nn]!CoefficientList[Series[c,{x,0,nn}],x],1];f[list_]:=Select[list,#\u003e0\u0026];Map[f,Drop[Transpose[Table[Range[0,nn]!CoefficientList[Series[ Exp[Sum[b[[i]]x^i/i!,{i,1,n+1}]]-Exp[Sum[b[[i]]x^i/i!,{i,1,n}]],{x,0,nn}],x],{n,0,nn-1}]],1]]//Grid",
				"(* Second program: *)",
				"g[n_] := g[n] = Sum[n^(n - j)*(n - 1)!/(n - j)!, {j, 1, n}];",
				"b[n_, m_] := b[n, m] = If[n == 0, x^m, Sum[g[i]*b[n - i, Max[m, i]]* Binomial[n - 1, i - 1], {i, 1, n}]];",
				"T[n_] := With[{p = b[n, 0]}, Table[Coefficient[p, x, i], {i, 1, n}]];",
				"Table[T[n], {n, 1, 12}] // Flatten (* _Jean-François Alcover_, Dec 30 2021, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Main diagonal gives A001865.",
				"Row sums give A000312.",
				"Cf. A209327, A347999."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Geoffrey Critzer_, Jan 19 2013",
			"references": 4,
			"revision": 45,
			"time": "2021-12-30T12:58:02-05:00",
			"created": "2013-01-20T14:15:27-05:00"
		}
	]
}