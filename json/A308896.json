{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A308896",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 308896,
			"data": "0,1,0,1,2,3,0,2,3,1,2,3,0,2,3,1,4,5,6,7,0,4,5,6,7,1,4,5,6,7,0,4,5,6,7,1,2,5,4,7,6,3,0,2,5,4,7,6,3,1,2,5,4,7,6,3,0,2,5,4,7,6,3,1,8,9,10,11,12,13,14,15,0,8,9,10,11,12,13,14,15,1,8",
			"name": "Walk a rook along the square spiral numbered 0, 1, 2, ... (cf. A274641); a(n) = mex of earlier values the rook can move to.",
			"comment": [
				"Analog of A308884 but using a rook rather than a knight.",
				"The array of values - see the illustration in the link - appears to have a number of interesting symmetries."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A308896/b308896.txt\"\u003eTable of n, a(n) for n = 0..16128\u003c/a\u003e",
				"F. Michel Dekking, Jeffrey Shallit, and N. J. A. Sloane, \u003ca href=\"https://www.combinatorics.org/ojs/index.php/eljc/article/view/v27i1p52/8039\"\u003eQueens in exile: non-attacking queens on infinite chess boards\u003c/a\u003e, Electronic J. Combin., 27:1 (2020), #P1.52.",
				"Rémy Sigrist, \u003ca href=\"/A308896/a308896_1.png\"\u003eColored representation of the spiral for -511 \u003c= x, y \u003c= 511\u003c/a\u003e (where dark pixels correspond to higher values and red pixels correspond to 0's)",
				"Rémy Sigrist, \u003ca href=\"/A308896/a308896_3.png\"\u003eScatterplot of (x,y) such that A(x,y) has bit b set to one for b = 0..6 and -63 \u003c= x \u003c= 64 and -63 \u003c= y \u003c= 64\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A308896/a308896.gp.txt\"\u003ePARI program for A308896\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/A308896/a308896.png\"\u003eInitial terms of spiral\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/A308896/a308896_2.txt\"\u003eExplicit formulas for the array in A308896\u003c/a\u003e, Jul 02 2019",
				"N. J. A. Sloane, \u003ca href=\"/A308896/a308896_2.png\"\u003eThe two kinds of sectors.\u003c/a\u003e (Rows y=1 and above form a sector of the first type, rows y=0 and below form the second type.)"
			],
			"formula": [
				"a(n) = 0, 1 iff n belongs to A002378, A085046, respectively. - _Rémy Sigrist_, Jul 02 2019",
				"For formulas for the terms in the array, see the \"Explicit formulas\" link."
			],
			"example": [
				"The central 21 X 21 portion of the plane:",
				"[ 4  1  3 30 31 28 29 26 27 24 25 22 23 20 21 18 19 16 17  2  0]",
				"[ 5  2  1 31 30 29 28 27 26 25 24 23 22 21 20 19 18 17 16  0  3]",
				"[18 17 16  1  3  6  7 12 13 14 15  8  9 10 11  4  5  2  0 31 30]",
				"[19 16 17  2  1  7  6 13 12 15 14  9  8 11 10  5  4  0  3 30 31]",
				"[16 19 18  5  4  1  3 14 15 12 13 10 11  8  9  2  0  7  6 29 28]",
				"[17 18 19  4  5  2  1 15 14 13 12 11 10  9  8  0  3  6  7 28 29]",
				"[22 21 20 11 10  9  8  1  3  6  7  4  5  2  0 15 14 13 12 27 26]",
				"[23 20 21 10 11  8  9  2  1  7  6  5  4  0  3 14 15 12 13 26 27]",
				"[20 23 22  9  8 11 10  5  4  1  3  2  0  7  6 13 12 15 14 25 24]",
				"[21 22 23  8  9 10 11  4  5  2  1  0  3  6  7 12 13 14 15 24 25]",
				"*26 25 24 15 14 13 12  7  6  3 *0* 1  2  5  4 11 10  9  8 23 22]",
				"[27 24 25 14 15 12 13  6  7  0  2  3  1  4  5 10 11  8  9 22 23]",
				"[24 27 26 13 12 15 14  3  0  4  5  6  7  1  2  9  8 11 10 21 20]",
				"[25 26 27 12 13 14 15  0  2  5  4  7  6  3  1  8  9 10 11 20 21]",
				"[30 29 28  7  6  3  0  8  9 10 11 12 13 14 15  1  2  5  4 19 18]",
				"[31 28 29  6  7  0  2  9  8 11 10 13 12 15 14  3  1  4  5 18 19]",
				"[28 31 30  3  0  4  5 10 11  8  9 14 15 12 13  6  7  1  2 17 16]",
				"[29 30 31  0  2  5  4 11 10  9  8 15 14 13 12  7  6  3  1 16 17]",
				"[ 6  3  0 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31  1  2]",
				"[ 7  0  2 17 16 19 18 21 20 23 22 25 24 27 26 29 28 31 30  3  1]",
				"[ 0  4  5 18 19 16 17 22 23 20 21 26 27 24 25 30 31 28 29  6  7]",
				"===============================**==============================="
			],
			"program": [
				"(PARI) See Links section."
			],
			"xref": [
				"Cf. A002378, A003987, A085046, A274641, A308884, A308897."
			],
			"keyword": "nonn,look",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_, Jul 02 2019",
			"ext": [
				"More terms from _Rémy Sigrist_, Jul 02 2019"
			],
			"references": 3,
			"revision": 46,
			"time": "2020-03-07T13:50:20-05:00",
			"created": "2019-07-02T00:14:12-04:00"
		}
	]
}