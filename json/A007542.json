{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007542",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7542,
			"id": "M2084",
			"data": "2,15,825,725,1925,2275,425,390,330,290,770,910,170,156,132,116,308,364,68,4,30,225,12375,10875,28875,25375,67375,79625,14875,13650,2550,2340,1980,1740,4620,4060,10780,12740,2380,2184,408,152",
			"name": "Successive integers produced by Conway's PRIMEGAME.",
			"comment": [
				"Conway's PRIMEGAME produces the terms 2^prime in increasing order.",
				"From _Daniel Forgues_, Jan 20 2016: (Start)",
				"Pairs (n, a(n)) such that a(n) = 2^k are (1, 2^1), (20, 2^2), (70, 2^3), (282, 2^5), (711, 2^7), (2376, 2^11), (3894, 2^13), (8103, 2^17), ...",
				"Numbers n such that a(n) = 2^k are 1, 20, 70, 282, 711, 2376, 3894, 8103, ... [This is 1 + A007547. - _N. J. A. Sloane_, Jan 25 2016] (End)"
			],
			"reference": [
				"D. Olivastro, Ancient Puzzles. Bantam Books, NY, 1993, p. 21.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A007542/b007542.txt\"\u003eTable of n, a(n) for n=1..8103\u003c/a\u003e",
				"J. H. Conway, \u003ca href=\"http://dx.doi.org/10.1007/978-1-4612-4808-8_2\"\u003eFRACTRAN: a simple universal programming language for arithmetic\u003c/a\u003e, in T. M. Cover and Gopinath, eds., Open Problems in Communication and Computation, Springer, NY 1987, pp. 4-26.",
				"Richard K. Guy, \u003ca href=\"http://www.jstor.org/stable/2690263\"\u003eConway's prime producing machine\u003c/a\u003e, Math. Mag. 56 (1983), no. 1, 26-33. [Gives slightly different version of the program which produces different terms, starting from n=132.]",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/FRACTRAN.html\"\u003eFRACTRAN\u003c/a\u003e",
				"OEIS Wiki, \u003ca href=\"https://oeis.org/wiki/Conway\u0026#39;s_PRIMEGAME\"\u003eConway's PRIMEGAME\u003c/a\u003e",
				"Leandro Vendramin, \u003ca href=\"https://www.mun.ca/aac/AACMiniCourses/GAP/problems.pdf\"\u003eMini-couse on GAP - Exercises\u003c/a\u003e, Universidad de Buenos Aires (Argentina, 2020).",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/FRACTRAN\"\u003eFRACTRAN\u003c/a\u003e"
			],
			"formula": [
				"a(n+1) = A203907(a(n)), a(1) = 2. [_Reinhard Zumkeller_, Jan 24 2012]"
			],
			"maple": [
				"l:= [17/91, 78/85, 19/51, 23/38, 29/33, 77/29, 95/23, 77/19, 1/17, 11/13, 13/11, 15/2, 1/7, 55]: a:= proc(n) option remember; global l; local p, k; if n=1 then 2 else p:= a(n-1); for k while not type(p*l[k], integer) do od; p*l[k] fi end: seq(a(n), n=1..50); # _Alois P. Heinz_, Aug 12 2009"
			],
			"mathematica": [
				"conwayFracs := {17/91, 78/85, 19/51, 23/38, 29/33, 77/29, 95/23, 77/19, 1/17, 11/13, 13/11, 15/2, 1/7, 55}; a[1] = 2; A007542[n_] := A007542[n] = (p = A007542[n - 1]; k = 1; While[ ! IntegerQ[p * conwayFracs[[k]]], k++]; p * conwayFracs[[k]]); Table[A007542[n], {n, 42}] (* _Jean-François Alcover_, Jan 23 2012, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(Haskell)",
				"a007542 n = a007542_list !! (n-1)",
				"a007542_list = iterate a203907 2  -- _Reinhard Zumkeller_, Jan 24 2012",
				"(Python)",
				"from fractions import Fraction",
				"nums = [17, 78, 19, 23, 29, 77, 95, 77,  1, 11, 13, 15, 1, 55] # A202138",
				"dens = [91, 85, 51, 38, 33, 29, 23, 19, 17, 13, 11,  2, 7,  1] # A203363",
				"PRIMEGAME = [Fraction(num, den) for num, den in zip(nums, dens)]",
				"def succ(n, program):",
				"  for i in range(len(program)):",
				"    if (n*program[i]).denominator == 1: return (n*program[i]).numerator",
				"def orbit(start, program, steps):",
				"  orb = [start]",
				"  for s in range(1, steps): orb.append(succ(orb[-1], program))",
				"  return orb",
				"print(orbit(2, PRIMEGAME, steps=42)) # _Michael S. Branicky_, Feb 15 2021"
			],
			"xref": [
				"Cf. A007546, A007547, A183132, A202138, A203363."
			],
			"keyword": "easy,nonn,look,nice",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_",
			"references": 13,
			"revision": 71,
			"time": "2021-02-15T16:03:15-05:00",
			"created": "1994-09-19T03:00:00-04:00"
		}
	]
}