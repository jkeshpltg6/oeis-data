{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A073053",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 73053,
			"data": "101,11,101,11,101,11,101,11,101,11,112,22,112,22,112,22,112,22,112,22,202,112,202,112,202,112,202,112,202,112,112,22,112,22,112,22,112,22,112,22,202,112,202,112,202,112,202,112,202,112,112,22,112,22",
			"name": "Apply DENEAT operator (or the Sisyphus function) to n.",
			"comment": [
				"DENEAT(n): concatenate number of even digits in n, number of odd digits and total number of digits. E.g. 25 -\u003e 1.1.2 = 112 (Digits: Even, Not Even, And Total). Leading zeros are then omitted.",
				"This is also known as the Sisyphus function. - _N. J. A. Sloane_, Jun 25 2018",
				"Repeated application of the DENEAT operator reduces all numbers to 123. This is easy to prove. Compare A073054, A100961. - _N. J. A. Sloane_ Jun 18 2005"
			],
			"reference": [
				"M. E. Coppenbarger, Iterations of a modified Sisyphus function, Fib. Q., 56 (No. 2, 2018), 130-141.",
				"M. Ecker, Caution: Black Holes at Work, New Scientist (Dec. 1992)",
				"M. J. Halm, Blackholing, Mpossibilities 69, (Jan 01 1999), p. 2.",
				"J. Schram, The Sisyphus string, J. Rec. Math., 19:1 (1987), 43-44.",
				"M. Zeger, Fatal attraction, Mathematics and Computer Education, 27:2 (1993), 118-123."
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/A073053/b073053.txt\"\u003eTable of n, a(n) for n = 0..20000\u003c/a\u003e"
			],
			"example": [
				"a(1) = 0.1.1 -\u003e 11.",
				"a(10000000000) = 10111 because 10000000000 has 10 even digits, 1 odd digit and 11 total digits",
				"DENEAT(1356)-\u003e \"1\" even (6), \"3\" odd (1,3,5), \"4\" total digits -\u003e134.",
				"DENEAT(1490)-\u003e \"2\" even (0 is considered even,4), \"2\" odd (1,9), \"4\" total digits -\u003e 224. - _Paolo P. Lava_, Mar 05 2010"
			],
			"maple": [
				"read(\"transforms\") :",
				"A073053 := proc(n)",
				"    local e,o,L ;",
				"    if n = 0 then",
				"        0 ;",
				"    else",
				"        e := A196563(n) ;",
				"        o := A196564(n) ;",
				"        L := [e,o,e+o] ;",
				"        digcatL(L) ;",
				"    end if;",
				"end proc: # _R. J. Mathar_, Jul 13 2012",
				"# Maple code based on R. J. Mathar's code for A171797, added by _N. J. A. Sloane_, May 12 2019 (Start)",
				"nevenDgs := proc(n) local a, d; a := 0 ; for d in convert(n, base, 10) do if type(d, 'even') then a :=a +1 ; end if; end do; a ; end proc:",
				"cat2 := proc(a, b) local ndigsb; ndigsb := max(ilog10(b)+1, 1) ; a*10^ndigsb+b ; end:",
				"catL := proc(L) local a, i; a := op(1, L) ; for i from 2 to nops(L) do a := cat2(a, op(i, L)) ; end do; a; end proc:",
				"A055642 := proc(n) max(1, ilog10(n)+1) ; end proc:",
				"A171797 := proc(n) local n1, n2 ; n1 := A055642(n) ; n2 := nevenDgs(n) ; catL([n1, n2, n1-n2]) ; end proc:",
				"A073053 := proc(n) local n1, n2 ; n1 := A055642(n) ; n2 := nevenDgs(n) ; catL([n2, n1-n2, n1]) ; end proc:",
				"seq(A073053(n), n=1..80) ; (End)",
				"L:=proc(n) if n=0 then 1 else floor(evalf(log(n)/log(10)))+1; fi; end;",
				"S:=proc(n) local Le,Ld,Lt,t1,e,d,t; global L;",
				"t1:=convert(n,base,10); e:=0; d:=0; t:=nops(t1);",
				"for i from 1 to t do if (t1[i] mod 2) = 0 then e:=e+1; else d:=d+1; fi; od:",
				"Le:=L(e); Ld:=L(d); Lt:=L(t);",
				"if e=0 then 10^Lt*d+t",
				"elif d=0 then 10^(Ld+Lt)*e+10^Lt*d+t",
				"else 10^(Ld+Lt)*e+10^Lt*d+t; fi;",
				"end;",
				"[seq(S(n),n=1..200)]; # _N. J. A. Sloane_, Jun 25 2018"
			],
			"mathematica": [
				"f[n_] := Block[{id = IntegerDigits[n]}, FromDigits[ Join[ IntegerDigits[ Length[ Select[id, EvenQ[ # ] \u0026]]], IntegerDigits[ Length[ Select[id, OddQ[ # ] \u0026]]], IntegerDigits[ Length[ id]] ]]]; Table[ f[n], {n, 0, 55}] (* _Robert G. Wilson v_, Jun 09 2005 *)",
				"s={};Do[id=IntegerDigits[n];ev=Select[id, EvenQ];ne=Select[id, OddQ];fd=FromDigits[{Length[ev], Length[ne], Length[id]}]; s=Append[s, fd], {n, 81}];SameQ[newA073053-s] (* _Zak Seidov_ *)",
				" deneat[n_]:=Module[{idn=IntegerDigits[n]},FromDigits[Flatten[ IntegerDigits/@ {Count[ idn,_?EvenQ],Count[ idn,_?OddQ],Length[ idn]}]]] Array[ deneat,60,0]// Flatten (* _Harvey P. Dale_, Aug 13 2021 *)"
			],
			"xref": [
				"Cf. A008577, A072420, A073054, A100961, A171797.",
				"For records see A305395, A004643, A308004."
			],
			"keyword": "easy,nonn,base",
			"offset": "0,1",
			"author": "_Michael Joseph Halm_, Aug 16 2002",
			"ext": [
				"Edited and corrected by _Jason Earls_ and _Robert G. Wilson v_, Jun 03 2005",
				"a(0) added by _N. J. A. Sloane_, May 12 2019"
			],
			"references": 14,
			"revision": 37,
			"time": "2021-08-13T11:35:45-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}