{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A123352",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 123352,
			"data": "1,1,1,1,2,1,1,3,5,1,1,4,14,14,1,1,5,30,84,42,1,1,6,55,330,594,132,1,1,7,91,1001,4719,4719,429,1,1,8,140,2548,26026,81796,40898,1430,1,1,9,204,5712,111384,884884,1643356,379236,4862,1",
			"name": "Triangle read by rows, giving Kekulé numbers for certain benzenoids (see the Cyvin-Gutman book for details).",
			"comment": [
				"There is another version in A078920. - _Philippe Deléham_, Apr 12 2007 [In other words, T(n,k) = A078920(n,n-k). - _Petros Hadjicostas_, Oct 19 2019]"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A123352/b123352.txt\"\u003eRows n = 0..50 of the triangle, flattened\u003c/a\u003e",
				"Paul Barry, \u003ca href=\"https://arxiv.org/abs/2011.10827\"\u003eNotes on the Hankel transform of linear combinations of consecutive pairs of Catalan numbers\u003c/a\u003e, arXiv:2011.10827 [math.CO], 2020.",
				"S. J. Cyvin and I. Gutman, \u003ca href=\"https://link.springer.com/book/10.1007/978-3-662-00892-8\"\u003eKekulé structures in benzenoid hydrocarbons\u003c/a\u003e, Lecture Notes in Chemistry, No. 46, Springer, New York, 1988 (see p. 183).",
				"M. de Sainte-Catherine and G. Viennot, \u003ca href=\"https://doi.org/10.1007/BFb0072509\"\u003eEnumeration of certain Young tableaux with bounded height\u003c/a\u003e, in: G. Labelle and P. Leroux (eds), \u003ca href=\"https://doi.org/10.1007/BFb0072503\"\u003eCombinatoire énumérative\u003c/a\u003e, Lecture Notes in Mathematics, vol. 1234, Springer, Berlin, Heidelberg, 1986, pp. 58-67."
			],
			"formula": [
				"T(n, n-1) = A000108(n).",
				"T(n, n-2) = A005700(n-1).",
				"T(n, n-3) = A006149(n-2).",
				"T(n, n-4) = A006150(n-3).",
				"T(n, n-5) = A006151(n-4).",
				"Triangle T(n,k) = (-1)^C(k+1,2) * Product{1 \u003c= i \u003c= j \u003c= k} (-2*(n+1)+i+j)/(i+j). - _Paul Barry_, Jan 22 2009",
				"From _G. C. Greubel_, Dec 17 2021: (Start)",
				"T(n, k) = Product_{j=0..n-k-1} binomial(2*n-2*j, n-j)/binomial(n+j+1, n-j).",
				"T(n, k) = ((n+1)!/(k+1)!)*Product_{j=0..n-k-1} Catalan(n-j)/binomial(n+j+1, n-j). (End)"
			],
			"example": [
				"Triangle T(n,k) (with rows n \u003e= 0 and columns k \u003e= 0) begins as follows:",
				"  1;",
				"  1, 1;",
				"  1, 2,  1;",
				"  1, 3,  5,    1;",
				"  1, 4, 14,   14,    1;",
				"  1, 5, 30,   84,   42,    1;",
				"  1, 6, 55,  330,  594,  132,   1;",
				"  1, 7, 91, 1001, 4719, 4719, 429, 1;",
				"  ..."
			],
			"mathematica": [
				"A123352[n_, k_]:= Product[Binomial[2*n-2*j, n-j]/Binomial[n+j+1, n-j], {j, 0, n-k-1}];",
				"Table[A123352[n, k], {n,0,10}, {k,0,n}]//Flatten (* _G. C. Greubel_, Dec 17 2021 *)"
			],
			"program": [
				"(Sage)",
				"def A123352(n,k): return product( binomial(2*n-2*j, n-j)/binomial(n+j+1, n-j) for j in (0..n-k-1) )",
				"flatten([[A123352(n,k) for k in (0..n)] for n in (0..10)]) # _G. C. Greubel_, Dec 17 2021"
			],
			"xref": [
				"Diagonals give A000108, A005700, A006149, A006150, A006151, etc.",
				"Columns include (truncated versions of) A000012 (k=0), A000027 (k=1), A000330 (k=2), A006858 (k=3), and A091962 (k=4).",
				"Cf. A078920."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_, Oct 14 2006",
			"ext": [
				"More terms from _Philippe Deléham_, Apr 12 2007"
			],
			"references": 6,
			"revision": 34,
			"time": "2021-12-17T03:12:21-05:00",
			"created": "2006-12-06T03:00:00-05:00"
		}
	]
}