{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A189923",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 189923,
			"data": "1,-31,-242,-31,-3124,7502,-16806,-31,-242,96844,-161050,7502,-371292,520986,756008,-31,-1419856,7502,-2476098,96844,4067052,4992550,-6436342,7502,-3124,11510052,-242,520986,-20511148,-23436248",
			"name": "Jordan function J_{-5}(n) multiplied by n^5.",
			"comment": [
				"For the Jordan function J_k see the Comtet and Apostol references."
			],
			"reference": [
				"T. M. Apostol, Introduction to Analytic Number Theory, Springer, 1986.",
				"L. Comtet, Advanced Combinatorics, Reidel, 1974, p. 199, #3."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A189923/b189923.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (terms 1..200 from Indranil Ghosh)"
			],
			"formula": [
				"a(n) = J_{-5}(n)*n^5 = Product_{p prime |n} (1-p^5), for n\u003e=2, a(1)=1.",
				"a(n) = Sum_{d|n} mu(d)*d^5 with the Moebius function mu = A008683.",
				"Dirichlet g.f.: zeta(s)/zeta(s-5).",
				"Sum identity: Sum_{d|n} a(n)*(n/d)^5 = 1 for all n\u003e=1.",
				"a(n) = a(rad(n)) with rad(n) = A007947(n), the squarefree kernel of n.",
				"G.f.: Sum_{k\u003e=1} mu(k)*k^5*x^k/(1 - x^k). - _Ilya Gutkovskiy_, Jan 15 2017"
			],
			"example": [
				"a(2) = a(4) = a(8) = ... = 1 - 2^5 = -31.",
				"a(4) = mu(1)*1^5 + mu(2)*2^5 + mu(4)*4^5 = 1 - 32 + 0 = -31.",
				"Sum identity for n=4: a(1)*(4/1)^5 + a(2)*(4/2)^5 + a(4)*(4/4)^5 = 1024 - 31*32 - 31 = 1."
			],
			"mathematica": [
				"a[n_] := Sum[ MoebiusMu[d]*d^5, {d, Divisors[n]}]; Table[a[n], {n, 1, 30}] (* _Jean-François Alcover_, Sep 03 2012 *)",
				"f[p_, e_] := (1-p^5); a[1] = 1; a[n_] := Times @@ (f @@@ FactorInteger[n]); Array[a, 100] (* _Amiram Eldar_, Dec 08 2020 *)"
			],
			"program": [
				"(PARI) for(n=1, 200, print1(sumdiv(n, d, moebius(d) * d^5),\", \")) \\\\ _Indranil Ghosh_, Mar 11 2017",
				"(PARI) a(n) = sumdiv(n, d, moebius(d) * d^5); \\\\ _Michel Marcus_, Jan 14 2018"
			],
			"xref": [
				"Cf. A023900, A046970, A063453, A189922, for k=-1..-4."
			],
			"keyword": "sign,easy,mult",
			"offset": "1,2",
			"author": "_Wolfdieter Lang_, Jun 16 2011",
			"references": 3,
			"revision": 39,
			"time": "2020-12-08T03:49:21-05:00",
			"created": "2011-06-17T08:33:26-04:00"
		}
	]
}