{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A279612",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 279612,
			"data": "1,1,1,2,3,1,1,1,3,5,2,1,3,4,1,1,3,5,5,4,3,2,3,2,4,5,1,3,4,4,1,1,5,7,7,2,3,7,3,2,4,3,4,2,8,5,1,1,6,8,3,6,7,8,2,3,3,6,8,4,6,5,2,2,9,7,7,7,7,12,3,1,9,10,7,1,10,10,2,3",
			"name": "Number of ways to write n = x^2 + y^2 + z^2 + w^2 with x + 2*y - 2*z a power of 4 (including 4^0 = 1), where x,y,z,w are nonnegative integers.",
			"comment": [
				"Conjecture: (i) a(n) \u003e 0 for all n \u003e 0, and a(n) = 1 only for n = 16^k*q (k = 0,1,2,... and q = 1, 2, 3, 6, 7, 8, 12, 15, 27, 31, 47, 72, 76, 92, 111, 127).",
				"(ii) Let a and b be positive integers with gcd(a,b) odd. Then any positive integer can be written as x^2 + y^2 + z^2 + w^2 with x,y,z,w nonnegative integers and a*x - b*y a power of two (including 2^0 = 1) if and only if (a,b) = (1,1), (2,1), (2,3).",
				"(iii) Let a,b,c be positive integers with a \u003c= b and gcd(a,b,c) odd. Then any positive integer can be written as x^2 + y^2 + z^2 + w^2 with x,y,z,w nonnegative integers and a*x + b*y - c*z a power of two if and only if (a,b,c) is among the triples (1,1,1), (1,1,2), (1,2,1), (1,2,2), (1,3,1), (1,3,2), (1,3,3), (1,3,4), (1,3,5), (1,4,1), (1,4,2), (1,4,3), (1,4,4), (1,5,1), (1,5,2), (1,5,4), (1,5,5,), (1,6,3), (1,7,4), (1,7,7), (1,8,1), (1,9,2), (2,3,1), (2,3,3), (2,3,4), (2,5,1), (2,5,3), (2,5,4), (2,5,5), (2,7,1), (2,7,3), (2,7,7), (2,9,3), (2,11,5), (3,4,3), (7,8,7).",
				"(iv) Let a,b,c be positive integers with b \u003c= c and gcd(a,b,c) odd. Then any positive integer can be written as x^2 + y^2 + z^2 + w^2 with x,y,z,w nonnegative integers and a*x - b*y - c*z a power of two if and only if (a,b,c) is among the triples (2,2,1), (4,2,1), (4,3,1), (4,4,3).",
				"(v) Let a,b,c be positive integers with a \u003c= b, c \u003c= d, and gcd(a,b,c,d) odd. Then any positive integer can be written as x^2 + y^2 + z^2 + w^2 with x,y,z,w nonnegative integers and a*x + b*y - c*z -d*w a power of two if and only if (a,b,c,d) is among the quadruples (1,2,1,1), (1,2,1,2), (1,2,1,3), (1,3,1,2), (1,3,2,3), (1,3,2,4), (1,4,1,2), (1,7,2,6), (1,9,1,4), (2,2,2,3), (2,3,1,2), (2,3,1,3), (2,3,2,3), (2,3,6,1), (2,4,1,2), (2,5,1,2), (2,5,2,3), (2,5,3,4), (3,4,1,2), (3,4,1,3), (3,4,1,5), (3,4,2,5), (3,4,3,4), (3,8,1,10), (3,8,2,3), (4,5,1,5).",
				"(vi) Let a,b,c be positive integers with a \u003c= b \u003c= c and gcd(a,b,c,d) odd. Then any positive integer can be written as x^2 + y^2 + z^2 + w^2 with x,y,z,w nonnegative integers and a*x + b*y + c*z -d*w a power of two if and only if (a,b,c,d) is among the quadruples (1,1,2,2), (1,1,2,3), (1,1,2,4), (1,2,2,3), (1,2,3,4), (1,2,4,3), (1,2,6,7), (1,3,4,4), (1,4,6,5), (2,3,5,4).",
				"(vii) For any positive integers a,b,c,d, not all positive integers can be written as x^2 + y^2 + z^2 + w^2 with x,y,z,w nonnegative integers and a*x - b*y - c*z -d*w a power of two.",
				"(viii) Let a and b be positive integers, and c and d be nonnegative integers. Then, not all positive integers can be written as x^2 + y^2 + z^2 + w^2 with x,y,z,w nonnegative integers and a*x + b*y + c*z + d*w a power of two.",
				"We have verified a(n) \u003e 0 for all n = 1..2*10^7. The conjecture that a(n) \u003e 0 for all n \u003e 0 appeared in arXiv:1701.05868."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A279612/b279612.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2016.11.008\"\u003eRefining Lagrange's four-square theorem\u003c/a\u003e, J. Number Theory 175(2017), 167-190. Also available from \u003ca href=\"http://arxiv.org/abs/1604.06723\"\u003earXiv:1604.06723 [math.NT]\u003c/a\u003e.",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1701.05868\"\u003eRestricted sums of four squares\u003c/a\u003e, arXiv:1701.05868 [math.NT], 2017."
			],
			"example": [
				"a(12) = 1 since 12 = 1^2 + 1^2 + 1^2 + 3^2 with 1 + 2*1 - 2*1 = 4^0.",
				"a(15) = 1 since 15 = 3^2 + 1^2 + 2^2 + 1^2 with 3 + 2*1 - 2*2 = 4^0.",
				"a(27) = 1 since 27 = 4^2 + 1^2 + 1^2 + 3^2 with 4 + 2*1 - 2*1 = 4.",
				"a(31) = 1 since 31 = 3^2 + 2^2 + 3^2 + 3^2 with 3 + 2*2 - 2*3 = 4^0.",
				"a(47) = 1 since 47 = 3^2 + 2^2 + 3^2 + 5^2 with 3 + 2*2 - 2*3 = 4^0.",
				"a(72) = 1 since 72 = 8^2 + 0^2 + 2^2 + 2^2 with 8 + 2*0 - 2*2 = 4.",
				"a(76) = 1 since 76 = 1^2 + 5^2 + 5^2 + 5^2 with 1 + 2*5 - 2*5 = 4^0.",
				"a(92) = 1 since 92 = 4^2 + 6^2 + 6^2 + 2^2 with 4 + 2*6 - 2*6 = 4.",
				"a(111) = 1 since 111 = 9^2 + 1^2 + 5^2 + 2^2 with 9 + 2*1 - 2*5 = 4^0.",
				"a(127) = 1 since 127 = 7^2 + 2^2 + 5^2 + 7^2 with 7 + 2*2 - 2*5 = 4^0."
			],
			"mathematica": [
				"SQ[n_]:=SQ[n]=IntegerQ[Sqrt[n]];",
				"FP[n_]:=FP[n]=n\u003e0\u0026\u0026IntegerQ[Log[4,n]];",
				"Do[r=0;Do[If[SQ[n-x^2-y^2-z^2]\u0026\u0026FP[x+2y-2z],r=r+1],{x,0,Sqrt[n]},{y,0,Sqrt[n-x^2]},{z,0,Sqrt[n-x^2-y^2]}];Print[n,\" \",r];Continue,{n,1,80}]"
			],
			"xref": [
				"Cf. A000079, A000118, A000290, A271518, A275656, A275675, A275676, A275738, A278560, A279616."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Zhi-Wei Sun_, Dec 15 2016",
			"references": 17,
			"revision": 12,
			"time": "2017-01-24T17:31:09-05:00",
			"created": "2016-12-15T23:42:25-05:00"
		}
	]
}