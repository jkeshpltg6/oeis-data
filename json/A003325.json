{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003325",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3325,
			"data": "2,9,16,28,35,54,65,72,91,126,128,133,152,189,217,224,243,250,280,341,344,351,370,407,432,468,513,520,539,559,576,637,686,728,730,737,756,793,854,855,945,1001,1008,1024,1027,1064,1072,1125,1216,1241,1332,1339,1343",
			"name": "Numbers that are the sum of 2 positive cubes.",
			"comment": [
				"It is conjectured that this sequence and A052276 have infinitely many numbers in common, although only one example (128) is known. [Any further examples are greater than 5 million. - _Charles R Greathouse IV_, Apr 12 2020] [Any further example is greater than 10^12. - _M. F. Hasler_, Jan 10 2021]",
				"A113958 is a subsequence; if m is a term then m+k^3 is a term of A003072 for all k \u003e 0. - _Reinhard Zumkeller_, Jun 03 2006",
				"From _James R. Buddenhagen_, Oct 16 2008: (Start)",
				"(i) N and N+1 are both the sum of two positive cubes if N=2*(2*n^2 + 4*n + 1)*(4*n^4 + 16*n^3 + 23*n^2 + 14*n + 4), n=1,2,....",
				"(ii) For n \u003e= 2, let N = 16*n^6 - 12*n^4 + 6*n^2 - 2, so N+1 = 16*n^6 - 12*n^4 + 6*n^2 - 1.",
				"Then the identities 16*n^6 - 12*n^4 + 6*n^2 - 2 = (2*n^2 - n - 1)^3 + (2*n^2 + n - 1)^3 16*n^6 - 12*n^4 + 6*n^2 - 1 = (2*n^2)^3 + (2*n^2 - 1)^3 show that N, N+1 are in the sequence. (End)",
				"If n is a term then n*m^3 (m \u003e= 2) is also a term, e.g., 2m^3, 9m^3, 28m^3, and 35m^3 are all terms of the sequence. \"Primitive\" terms (not of the form n*m^3 with n = some previous term of the sequence and m \u003e= 2) are 2, 9, 28, 35, 65, 91, 126, etc. - _Zak Seidov_, Oct 12 2011",
				"This is an infinite sequence in which the first term is prime but thereafter all terms are composite. - _Ant King_, May 09 2013",
				"By Fermat's Last Theorem (the special case for exponent 3, proved by Euler, is sufficient), this sequence contains no cubes. - _Charles R Greathouse IV_, Apr 03 2021"
			],
			"reference": [
				"C. G. J. Jacobi, Gesammelte Werke, vol. 6, 1969, Chelsea, NY, p. 354."
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/A003325/b003325.txt\"\u003eTable of n, a(n) for n = 1..20000\u003c/a\u003e [First 1000 terms from T. D. Noe]",
				"F. Beukers, \u003ca href=\"http://dx.doi.org/10.1215/S0012-7094-98-09105-0\"\u003eThe Diophantine equation Ax^p+By^q=Cz^r\u003c/a\u003e, Duke Math. J. 91 (1998), 61-88.",
				"Kevin A. Broughan, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL6/Broughan/broughan25.html\"\u003eCharacterizing the sum of two cubes\u003c/a\u003e, J. Integer Seqs., Vol. 6, 2003.",
				"Nils Bruin, \u003ca href=\"http://dx.doi.org/10.1007/10722028_9\"\u003eOn powers as sums of two cubes\u003c/a\u003e, in Algorithmic number theory (Leiden, 2000), 169-184, Lecture Notes in Comput. Sci., 1838, Springer, Berlin, 2000.",
				"C. G. J. Jacobi, \u003ca href=\"http://www.hti.umich.edu/cgi/t/text/text-idx?c=umhistmath;idno=ABR8803\"\u003eGesammelte Werke\u003c/a\u003e.",
				"Michael Penn, \u003ca href=\"https://www.youtube.com/watch?v=RK1DewTkKT8\"\u003e1674 is not a perfect cube\u003c/a\u003e, 2020 video",
				"N. J. A. Sloane, \u003ca href=\"/A003325/a003325.txt\"\u003eTable of n, a(n) for n = 1..59562\u003c/a\u003e",
				"D. Tournes, \u003ca href=\"http://www.reunion.iufm.fr/dep/mathematiques/Seminaires/ActesPDF/Tournes53.pdf\"\u003eA Glance on Indian Mathematician Srinivasa Ramanujan(1887-1920). [Text in French]\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CubicNumber.html\"\u003eCubic Number\u003c/a\u003e",
				"\u003ca href=\"/index/Su#ssq\"\u003eIndex entries for sequences related to sums of cubes\u003c/a\u003e"
			],
			"mathematica": [
				"nn = 2*20^3; Union[Flatten[Table[x^3 + y^3, {x, nn^(1/3)}, {y, x, (nn - x^3)^(1/3)}]]] (* _T. D. Noe_, Oct 12 2011 *)",
				"With[{upto=2000},Select[Total/@Tuples[Range[Ceiling[Surd[upto,3]]]^3,2],#\u003c=upto\u0026]]//Union (* _Harvey P. Dale_, Jun 11 2016 *)"
			],
			"program": [
				"(PARI) cubes=sum(n=1, 11, x^(n^3), O(x^1400)); v = select(x-\u003ex, Vec(cubes^2), 1); vector(#v, k, v[k]+1) \\\\ edited by _Michel Marcus_, May 08 2017",
				"(PARI) isA003325(n) = for(k=1,sqrtnint(n\\2,3), ispower(n-k^3,3) \u0026\u0026 return(1)) \\\\ _M. F. Hasler_, Oct 17 2008, improved upon suggestion of _Altug Alkan_ and _Michel Marcus_, Feb 16 2016",
				"(PARI) T=thueinit('z^3+1); is(n)=#select(v-\u003emin(v[1],v[2])\u003e0, thue(T,n))\u003e0 \\\\ _Charles R Greathouse IV_, Nov 29 2014",
				"(PARI) list(lim)=my(v=List()); lim\\=1; for(x=1,sqrtnint(lim-1,3), my(x3=x^3); for(y=1,min(sqrtnint(lim-x3,3),x), listput(v, x3+y^3))); Set(v) \\\\ _Charles R Greathouse IV_, Jan 11 2022",
				"(Haskell)",
				"a003325 n = a003325_list !! (n-1)",
				"a003325_list = filter c2 [1..] where",
				"   c2 x = any (== 1) $ map (a010057 . fromInteger) $",
				"                       takeWhile (\u003e 0) $ map (x -) $ tail a000578_list",
				"-- _Reinhard Zumkeller_, Mar 24 2012",
				"(Python)",
				"from sympy import integer_nthroot",
				"def aupto(lim):",
				"  cubes = [i*i*i for i in range(1, integer_nthroot(lim-1, 3)[0] + 1)]",
				"  sum_cubes = sorted([a+b for i, a in enumerate(cubes) for b in cubes[i:]])",
				"  return [s for s in sum_cubes if s \u003c= lim]",
				"print(aupto(1343)) # _Michael S. Branicky_, Feb 09 2021"
			],
			"xref": [
				"Subsequence of A045980; supersequence of A202679.",
				"Cf. A024670 (2 distinct cubes), A003072, A001235, A011541, A003826, A010057, A000578, A027750, A010052, A004999, A085323 (n such that a(n+1)=a(n)+1)."
			],
			"keyword": "nonn,easy,nice,changed",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Error in formula line corrected by _Zak Seidov_, Jul 23 2009"
			],
			"references": 135,
			"revision": 93,
			"time": "2022-01-11T22:26:27-05:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}