{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A055133",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 55133,
			"data": "1,-1,1,3,-4,1,-19,27,-9,1,211,-304,108,-16,1,-3651,5275,-1900,300,-25,1,90921,-131436,47475,-7600,675,-36,1,-3081513,4455129,-1610091,258475,-23275,1323,-49,1,136407699,-197216832,71282064,-11449536,1033900,-59584,2352,-64,1",
			"name": "Matrix inverse of A008459 (squares of entries of Pascal's triangle).",
			"comment": [
				"Let E(y) = Sum_{n \u003e= 0} y^n/n!^2 = BesselJ(0,2*sqrt(-y)). Then this triangle is the generalized Riordan array (1/E(y), y) with respect to the sequence n!^2 as defined in Wang and Wang. - _Peter Bala_, Jul 24 2013"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A055133/b055133.txt\"\u003eRows n = 0..99, flattened\u003c/a\u003e",
				"J. Riordan, \u003ca href=\"http://www.jstor.org/stable/2312584\"\u003eInverse relations and combinatorial identities\u003c/a\u003e, Amer. Math. Monthly, 71 (1964), 485-498; see p. 493 with beta_{n,k} = |T(n,k)|.",
				"W. Wang and T. Wang, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2007.12.037\"\u003eGeneralized Riordan array\u003c/a\u003e, Discrete Mathematics, 308(24) (2008), 6466-6500."
			],
			"formula": [
				"T(n, k) = (-1)^(n+k)*A000275(n-k)*C(n, k)^2.",
				"From _Peter Bala_, Jul 24 2013: (Start)",
				"Let E(y) = Sum_{n \u003e= 0} y^n/n!^2 = BesselJ(0,2*sqrt(-y)). Generating function: E(x*y)/E(y) = 1 + (-1 + x)*y + (3 - 4*x + x^2)*y^2/2!^2 + (-19 + 27*x - 9*x^2 + x^3)*y^3/3!^2 + ....",
				"The n-th power of this array has a generating function E(x*y)/E(y)^n. In particular, the matrix inverse A008459 has a generating function E(y)*E(x*y).",
				"Recurrence equation for the row polynomials: R(n,x) = x^n - Sum_{k = 0..n-1} binomial(n,k)^2*R(k,x) with initial value R(0,x) = 1.",
				"There appears to be a connection between the zeros of the Bessel function E(x) and the real zeros of the row polynomials R(n,x). Let alpha denote the root of E(x) = 0 that is smallest in absolute magnitude. Numerically, alpha = -1.44579 64907 ... ( = -(A115365/2)^2). It appears that the real zeros of R(n,x) approach zeros of E(alpha*x) as n increases. A numerical example is given below. Indeed, it may be the case that lim_{n -\u003e inf} R(n,x)/R(n,0) = E(alpha*x) for arbitrary complex x. (End)"
			],
			"example": [
				"Table T(n,k) (with rows n \u003e= 0 and columns k \u003e= 0) begins as follows:",
				"      1;",
				"     -1,       1;",
				"      3,      -4,     1;",
				"    -19,      27,    -9,     1;",
				"    211,    -304,   108,   -16,   1;",
				"  -3651,    5275, -1900,   300, -25,   1;",
				"  90921, -131436, 47475, -7600, 675, -36, 1;",
				"  ... [edited by _Petros Hadjicostas_, Aug 24 2019]",
				"From _Peter Bala_, Jul 24 2013: (Start)",
				"Function   |        Real zeros to 5 decimal places",
				"= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =",
				"R(5,x)     | 1, 5.40649,  7.23983",
				"R(10,x)    | 1, 5.26894, 12.97405, 18.53109",
				"R(15,x)    | 1, 5.26894, 12.94909, 24.04769, 33.87883",
				"R(20,x)    | 1, 5.26894, 12.94909, 24.04216, 38.54959, 53.32419",
				"= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =",
				"E(alpha*x) | 1, 5.26894, 12.94909, 24.04216, 38.54835, 56.46772, ...",
				"where alpha = -1.44579 64907 ... ( = -(A115365/2)^2).",
				"Note: The n-th zero of E(alpha*x) may be calculated in Maple 17 using the instruction evalf( (BesselJZeros(0,n)/BesselJZeros(0,1))^2 ). (End)"
			],
			"maple": [
				"T:= proc(n) local M;",
				"       M:= Matrix(n+1, (i, j)-\u003e binomial(i-1, j-1)^2)^(-1);",
				"       seq(M[n+1, i], i=1..n+1)",
				"    end:",
				"seq(T(n), n=0..10);  # _Alois P. Heinz_, Mar 14 2013"
			],
			"mathematica": [
				"T[n_] := Module[{M}, M = Table[Binomial[i-1, j-1]^2, {i, 1, n+1}, {j, 1, n+1}] // Inverse; Table[M[[n+1, i]], {i, 1, n+1}]]; Table[T[n], {n, 0, 10}] // Flatten (* _Jean-François Alcover_, Nov 28 2015, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A000275, A008459 (matrix inverse), A115365."
			],
			"keyword": "sign,tabl",
			"offset": "0,4",
			"author": "_Christian G. Bower_, Apr 25 2000",
			"references": 7,
			"revision": 37,
			"time": "2019-08-25T03:09:59-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}