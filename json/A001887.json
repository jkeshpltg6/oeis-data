{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001887",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1887,
			"id": "M3970 N1640",
			"data": "1,0,0,0,1,5,33,236,1918,17440,175649,1942171,23396353,305055960,4280721564,64330087888,1030831875953,17545848553729,316150872317105,6012076099604308,120330082937778554",
			"name": "Number of permutations p of {1,2,...,n} such that p(i) - i \u003c 0 or p(i) - i \u003e 2 for all i.",
			"comment": [
				"Previous name was: Hit polynomials."
			],
			"reference": [
				"J. Riordan, The enumeration of permutations with three-ply staircase restrictions, unpublished memorandum, Bell Telephone Laboratories, Murray Hill, NJ, Oct 1963. (See A001883)",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A001887/b001887.txt\"\u003eTable of n, a(n) for n = 0..300\u003c/a\u003e",
				"E. R. Canfield, N. C. Wormald, \u003ca href=\"https://doi.org/10.1016/0012-365X(87)90002-1\"\u003eMenage numbers, bijections and P-recursiveness\u003c/a\u003e, Discr. Math. 63 (1987) 117, table Section 7.",
				"P. Flajolet and R. Sedgewick, \u003ca href=\"http://algo.inria.fr/flajolet/Publications/books.html\"\u003eAnalytic Combinatorics\u003c/a\u003e, 2009; see page 373",
				"V. Kotesovec, \u003ca href=\"https://oeis.org/wiki/User:Vaclav_Kotesovec\"\u003eNon-attacking chess pieces\u003c/a\u003e, 6ed, 2013, p. 224.",
				"N. J. A. Sloane, \u003ca href=\"/A001883/a001883_21.pdf\"\u003eAnnotated copy of Riordan's Three-Ply Staircase paper\u003c/a\u003e (unpublished memorandum, Bell Telephone Laboratories, Murray Hill, NJ, Oct 1963)",
				"D. Zeilberger, \u003ca href=\"http://arxiv.org/abs/1401.1089\"\u003eAutomatic Enumeration of Generalized Menage Numbers\u003c/a\u003e, arXiv preprint arXiv:1401.1089 [math.CO], 2014."
			],
			"formula": [
				"G.f.: (1/(x^2-1))*(x-Sum_{n\u003e=0} n!*(x*(x-1)/(x^3-2*x-1))^n). - _Vladeta Jovovic_, Jun 30 2007",
				"D-finite with recurrence (P. Flajolet, 1997): a(n) = (n-1)*a(n-1) + (n+2)*a(n-2) - (3*n-13)*a(n-3) - (2*n-8)*a(n-4) + (3*n-15)*a(n-5) + (n-4)*a(n-6) - (n-7)*a(n-7) - a(n-8), n\u003e8.",
				"a(n) ~ exp(-3) * n!. - _Vaclav Kotesovec_, Sep 10 2014"
			],
			"mathematica": [
				"nmax = 21;",
				"gf = 1/(x^2-1)(x-Sum[n! (x(x-1)/(x^3-2x-1))^n + O[x]^nmax, {n, 0, nmax}]);",
				"CoefficientList[gf, x] (* _Jean-François Alcover_, Aug 19 2018 *)"
			],
			"xref": [
				"Cf. A000255, A000271, A001883-A001891, A075851, A075852.",
				"First column of A080061."
			],
			"keyword": "nonn",
			"offset": "0,6",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Vladimir Baltic_ and _Vladeta Jovovic_, Jan 05 2003",
				"New name from _Vaclav Kotesovec_ using a former comment by _Vladimir Baltic_ and _Vladeta Jovovic_, Sep 16 2014"
			],
			"references": 3,
			"revision": 55,
			"time": "2020-01-31T15:17:57-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}