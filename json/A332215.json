{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A332215",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 332215,
			"data": "0,1,3,2,15,6,7,4,5,30,63,12,255,14,29,8,511,10,1023,60,13,126,2047,24,23,510,9,28,4095,58,31,16,125,1022,27,20,16383,2046,509,120,32767,26,65535,252,57,4094,262143,48,11,46,1021,1020,1048575,18,119,56,2045,8190,2097151,116,4194303,62,25,32,503,250,8388607,2044,4093,54,16777215,40",
			"name": "Mersenne-prime fixing variant of A243071: a(n) = A243071(A332213(n)).",
			"comment": [
				"Any Mersenne prime (A000668) times any power of 2 (i.e., 2^k, for k\u003e=0) is fixed by this sequence, including also all even perfect numbers.",
				"From _Antti Karttunen_, Jul 10 2020: (Start)",
				"This is a \"tuned variant\" of A243071, and has many of the same properties.",
				"For example, for n \u003e 1, A007814(a(n)) = A007814(n) - A209229(n), that is, this map  preserves the 2-adic valuation of n, except when n is a power of two, in which cases that value is decremented by one, and in particular, a(2^k * n) = 2^k * a(n) for all n \u003e 1. Also, like A243071, this bijection maps primes to the terms of A000225 (binary repunits). However, the \"tuning\" (A332213) has a specific effect that each Mersenne prime (A000668) is mapped to itself. Therefore the terms of A335431 are fixed by this map. Furthermore, I conjecture that there are no other fixed points. For the starters, see the proof in A335879, which shows that at least none of the terms of A335882 are fixed.",
				"(End)"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A332215/b332215.txt\"\u003eTable of n, a(n) for n = 1..8192\u003c/a\u003e",
				"\u003ca href=\"/index/Pri#prime_indices\"\u003eIndex entries for sequences computed from indices in prime factorization\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A243071(A332213(n)).",
				"For all n \u003e= 1, a(A335431(n)) = A335431(n), a(A335882(n)) = A335879(n). - _Antti Karttunen_, Jul 10 2020"
			],
			"program": [
				"(PARI) A332215(n) = A243071(A332213(n));"
			],
			"xref": [
				"Cf. A243071, A332210, A332213, A332214 (inverse permutation), A335431 (conjectured to be all the fixed points), A335879.",
				"Cf. A000043, A000668, A000396, A324200."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Antti Karttunen_, Feb 09 2020",
			"references": 8,
			"revision": 26,
			"time": "2020-07-10T22:06:53-04:00",
			"created": "2020-02-09T20:13:12-05:00"
		}
	]
}