{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A348691",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 348691,
			"data": "0,0,1,1,2,0,1,-1,2,-2,-1,-1,0,-2,-1,1,0,-4,-3,1,-2,0,1,3,-2,-2,-1,3,0,2,3,1,-4,-4,-3,5,-2,4,5,3,-2,2,3,3,4,2,3,-3,-4,0,1,5,2,4,5,-1,2,2,3,-1,4,-2,-1,-3,-8,0,1,9,2,8,9,-1,2,6,7,-1,8,-2",
			"name": "For any nonnegative number n with binary expansion Sum_{k \u003e= 0} b_k * 2^k, a(n) is the imaginary part of f(n) = Sum_{k \u003e= 0} b_k * (i^Sum_{j = 0..k-1} b_j) * (1+i)^k (where i denotes the imaginary unit); sequence A348690 gives the real part.",
			"comment": [
				"The function f defines a bijection from the nonnegative integers to the Gaussian integers.",
				"The function f has similarities with A065620; here the nonzero digits in base 1+i cycle through powers of i, there nonzero digits in base 2 cycle through powers of -1.",
				"If we replace 1's in binary expansions by powers of i from left to right (rather than right to left as here), then we obtain the Lévy C curve (A332251, A332252)."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A348691/b348691.txt\"\u003eTable of n, a(n) for n = 0..8191\u003c/a\u003e",
				"Chandler Davis and Donald Knuth, Number representations and Dragon Curves I, Journal of Recreational Mathematics, volume 3, number 2 (April 1970), pages 66-81. Reprinted in Donald E. Knuth, \u003ca href=\"http://www-cs-faculty.stanford.edu/~uno/fg.html\"\u003eSelected Papers on Fun and Games\u003c/a\u003e, CSLI Publications, 2011, pages 571-614.",
				"Rémy Sigrist, \u003ca href=\"/A348690/a348690.png\"\u003eColored representation of f(n) for n \u003c 2^18 in the complex plane\u003c/a\u003e (the hue is function of n)",
				"Rémy Sigrist, \u003ca href=\"/A348690/a348690_1.png\"\u003eColored representation of f(n) for n \u003c 2^18 in the complex plane\u003c/a\u003e (the color is function of A000120(n) mod 4)",
				"Rémy Sigrist, \u003ca href=\"/A348691/a348691.png\"\u003eColored representation of f(n) for n \u003c 2^18 in the complex plane\u003c/a\u003e (the color is function of the binary length of n, A070939(n))"
			],
			"formula": [
				"a(2^k) = A009545(k) for any k \u003e= 0."
			],
			"program": [
				"(PARI) a(n) = { my (v=0, o=0, x); while (n, n-=2^x=valuation(n, 2); v+=I^o * (1+I)^x; o++); imag(v) }"
			],
			"xref": [
				"See A332251, A332252 for a similar sequence.",
				"Cf. A000120, A009545, A065620, A070939, A348690."
			],
			"keyword": "sign,look,base",
			"offset": "0,5",
			"author": "_Rémy Sigrist_, Oct 29 2021",
			"references": 3,
			"revision": 22,
			"time": "2021-11-01T00:37:35-04:00",
			"created": "2021-10-30T13:42:38-04:00"
		}
	]
}