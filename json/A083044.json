{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A083044",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 83044,
			"data": "1,2,4,3,6,7,5,9,11,10,8,14,17,15,13,12,21,26,23,20,16,18,32,39,35,30,24,19,27,48,59,53,45,36,29,22,41,72,89,80,68,54,44,33,25,62,108,134,120,102,81,66,50,38,28,93,162,201,180,153,122,99,75,57,42,31,140,243",
			"name": "Square table read by antidiagonals forms a permutation of the natural numbers: T(n,0) = floor(n*x/(x-1))+1, T(n,k+1) = ceiling(x*T(n,k)), where x=3/2, n \u003e= 0, k \u003e= 0.",
			"comment": [
				"First row is A061419, first column is T(n,0) = A016777(n) = 3n+1 (namely the numbers not of the form ceiling(3*k/2) for any natural number k, in increasing order), main diagonal is A083045, antidiagonal sums give A083046. [further detail on first column added by _Glen Whitney_, Aug 03 2018]",
				"A083044 is the dispersion of the sequence A007494 of positive integers congruent to (0 or 2) mod 3; see A191655. - _Clark Kimberling_, Jun 10 2011",
				"If T(n+1,k) - T(n,k) = 2m, then T(n+1,k+1) - T(n,k+1) = ceiling(3T(n+1,k)/2) - ceiling(3T(n,k)/2) = ceiling(3T(n,k)/2 + 3m) - ceiling(3T(n,k)/2) = 3m. Similarly, if T(n+1,k) - T(n,k) = 2m+1, then T(n+1,k+1) - T(n,k+1) = ceiling(3T(n,k)/2 + 3m + 3/2) - ceiling(3T(n,k)/2) = {3m+1 or 3m+2, according to whether T(n,k) is even or odd}. The first differences of the first column T(n,0) are periodic: (3)*. The parities of the first column T(n,0) are periodic: (odd,even)*. Hence by induction using the prior two observations, the first differences and parities of every column will be periodic; e.g., for the second column T(n,2): the first differences are (4,5)* and the parities are (even,even,odd,odd)*; for the third column T(n,3): (6,8,6,7)* and (odd,odd,odd,odd,even,even,even,even)*; for the fourth column T(n,4): (9,12,9,10,9,12,9,11)* and (o,e,e,o,o,e,e,o,e,o,o,e,e,o,o,e)*. Is the period length of the first differences of column k always 2^{k-1}? And is the period length of parities always 2^k? Does every integer \u003e 2 occur as T(n+1,k) - T(n,k) for some n and k? Is the smallest first difference in column k always A061418(k+1)? And is the largest first difference in column k always A061419(k+2)? - _Glen Whitney_, Aug 03 2018",
				"Consider the following two-player game: Start with two nonempty piles of counters. Players alternate taking turns consisting of first discarding one of the piles and then dividing the remaining pile into two nonempty piles. The smaller pile may always be discarded; the larger pile may only be discarded if the smaller pile is at least half as large. The player who cannot move (because the configuration has reached two piles of one counter each) loses. Then the numbers c for which two piles of size c is a losing configuration (for the player whose turn it is) are exactly T(4,k) for k \u003e 1, together with 1,3,5, and 9. - _Glen Whitney_, Aug 03 2018"
			],
			"formula": [
				"T(A163491(n)-1, A087088(n)-1) = n. - _Peter Munn_, Jul 16 2020 [corrected _Peter Munn_, Aug 02 2020]"
			],
			"example": [
				"Table begins:",
				"   1  2  3   5   8  12  18  27  41   62   93  140 ...",
				"   4  6  9  14  21  32  48  72 108  162  243  365 ...",
				"   7 11 17  26  39  59  89 134 201  302  453  680 ...",
				"  10 15 23  35  53  80 120 180 270  405  608  912 ...",
				"  13 20 30  45  68 102 153 230 345  518  777 1166 ...",
				"  16 24 36  54  81 122 183 275 413  620  930 1395 ...",
				"  19 29 44  66  99 149 224 336 504  756 1134 1701 ...",
				"  22 33 50  75 113 170 255 383 575  863 1295 1943 ...",
				"  25 38 57  86 129 194 291 437 656  984 1476 2214 ...",
				"  28 42 63  95 143 215 323 485 728 1092 1638 2457 ...",
				"  31 47 71 107 161 242 363 545 818 1227 1841 2762 ..."
			],
			"xref": [
				"Cf. A061419, A083045, A083046, A083047, A083050.",
				"Row in which a number occurs: A163491.",
				"Column in which a number occurs: A087088."
			],
			"keyword": "nonn,tabl",
			"offset": "0,2",
			"author": "_Paul D. Hanna_, Apr 18 2003",
			"references": 19,
			"revision": 33,
			"time": "2020-08-07T01:46:04-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}