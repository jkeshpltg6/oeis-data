{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A290993",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 290993,
			"data": "0,0,0,0,0,1,6,21,56,126,252,463,804,1365,2366,4368,8736,18565,40410,87381,184604,379050,758100,1486675,2884776,5592405,10919090,21572460,43144920,87087001,176565486,357913941,723002336,1453179126,2906358252,5791193143",
			"name": "p-INVERT of (1,1,1,1,1,...), where p(S) = 1 - S^6.",
			"comment": [
				"Suppose s = (c(0), c(1), c(2), ...) is a sequence and p(S) is a polynomial. Let S(x) = c(0)*x + c(1)*x^2 + c(2)*x^3 + ... and T(x) = (-p(0) + 1/p(S(x)))/x. The p-INVERT of s is the sequence t(s) of coefficients in the Maclaurin series for T(x). Taking p(S) = 1 - S gives the \"INVERT\" transform of s, so that p-INVERT is a generalization of the \"INVERT\" transform (e.g., A033453).",
				"See A291000 for a guide to related sequences."
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A290993/b290993.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (6,-15,20,-15,6)."
			],
			"formula": [
				"a(n) = 6*a(n-1) - 15*a(n-2) + 20*a(n-3) - 15*a(n-4) + 6*a(n-5) for n\u003e5. Corrected by _Colin Barker_, Aug 24 2017",
				"G.f.: x^5 / ((1 - 2*x)*(1 - x + x^2)*(1 - 3*x + 3*x^2)). - _Colin Barker_, Aug 24 2017",
				"a(n) = A192080(n-5) for n \u003e 5. - _Georg Fischer_, Oct 23 2018"
			],
			"maple": [
				"seq(coeff(series(x^5/((1-2*x)*(1-x+x^2)*(1-3*x+3*x^2)),x,n+1), x, n), n = 0 .. 35); # _Muniru A Asiru_, Oct 23 2018"
			],
			"mathematica": [
				"z = 60; s = x/(1 - x); p = 1 - s^6;",
				"Drop[CoefficientList[Series[s, {x, 0, z}], x], 1]  (* A000012 *)",
				"Drop[CoefficientList[Series[1/p, {x, 0, z}], x], 1]  (* A290993 *)"
			],
			"program": [
				"(PARI) concat(vector(5), Vec(x^5 / ((1 - 2*x)*(1 - x + x^2)*(1 - 3*x + 3*x^2)) + O(x^50))) \\\\ _Colin Barker_, Aug 24 2017",
				"(GAP) a:=[0,0,0,0,1];;  for n in [6..35] do a[n]:=6*a[n-1]-15*a[n-2]+20*a[n-3]-15*a[n-4]+6*a[n-5]; od; Concatenation([0],a); # _Muniru A Asiru_, Oct 23 2018"
			],
			"xref": [
				"Cf. A000012, A192080, A289780, A291000."
			],
			"keyword": "nonn,easy",
			"offset": "0,7",
			"author": "_Clark Kimberling_, Aug 21 2017",
			"references": 2,
			"revision": 21,
			"time": "2018-10-23T10:33:15-04:00",
			"created": "2017-08-21T22:08:45-04:00"
		}
	]
}