{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A048298",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 48298,
			"data": "0,1,2,0,4,0,0,0,8,0,0,0,0,0,0,0,16,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,32,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0",
			"name": "a(n) = n if n=2^i with i=0,1,2,3,...; else a(n) = 0.",
			"comment": [
				"Expand x/(x-1) = Sum_{n \u003e= 0} 1/x^n as Sum a(n) / (1+x^n).",
				"Nim-binomial transform of the natural numbers. If {t(n)} is the Nim-binomial transform of {a(n)}, then t(n)=(S^n)a(0), where Sf(n) denotes the Nim-sum of f(n) and f(n+1); and S^n=S(S^(n-1)). - _John W. Layman_, Mar 06 2001"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A048298/b048298.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"J.-P. Allouche and J. Shallit, \u003ca href=\"http://www.math.jussieu.fr/~allouche/kreg2.ps\"\u003eThe Ring of k-regular Sequences, II\u003c/a\u003e.",
				"J.-P. Allouche and J. Shallit, \u003ca href=\"http://dx.doi.org/10.1016/S0304-3975(03)00090-2\"\u003eThe ring of k-regular sequences, II\u003c/a\u003e, Theoret. Computer Sci., 307 (2003), 3-29.",
				"Daniele A. Gewurz and Francesca Merola, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL6/Gewurz/gewurz5.html\"\u003eSequences realized as Parker vectors of oligomorphic permutation groups\u003c/a\u003e, J. Integer Seqs., Vol. 6, 2003."
			],
			"formula": [
				"Multiplicative with a(2^e)=2^e and a(p^e)=0 for p \u003e 2. - _Vladeta Jovovic_, Jan 27 2002",
				"Inverse mod 2 binomial transform of n. a(n) = sum{k=0..n, (-1)^A010060(n-k)*mod(C(n, k), 2)*k}. - _Paul Barry_, Jan 03 2005",
				"If n=1 we have a(n)=1; if n=p is prime, then (-1)^(p+1)+a(p)=1, thus a(2)=2, and a(p)=0, if p\u003e2. - _Vladimir Shevelev_, Jun 09 2009",
				"Dirichlet g.f.: 2^s/(2^s-2). - _Ralf Stephan_, Jun 17 2007",
				"Dirichlet g.f.: zeta(s)/eta(s). - _Ralf Stephan_, Mar 25 2015",
				"For n\u003e=1, we have a recursion Sum_{d|n}(-1)^(1+(n/d))a(d)=1. - _Vladimir Shevelev_, Jun 09 2009",
				"For n\u003e=1, there is the recurrence n=Sum_{k=1..n} a(k)*g(n/k) where g(x) = floor(x) - 2*floor(x/2). - _Benoit Cloitre_, Nov 11 2010",
				"a(n) = A209229(n)*n. - _Reinhard Zumkeller_, Oct 17 2015"
			],
			"maple": [
				"0, seq(op([2^n,0$(2^n-1)]), n=0..10); # _Robert Israel_, Mar 25 2015"
			],
			"mathematica": [
				"Table[n*Boole[Or[n == 1, First /@ FactorInteger@ n == {2}]], {n, 0, 120}] (* _Michael De Vlieger_, Mar 25 2015 *)"
			],
			"program": [
				"(PARI) a(n)=direuler(p=1,n,if(p==2,1/(1-2*X),1))[n] /* _Ralf Stephan_, Mar 27 2015 */",
				"(MAGMA) [n eq 2^Valuation(n,2) select n else 0: n in [0..120]]; // _Vincenzo Librandi_, improved by _Bruno Berselli_, Mar 27 2015",
				"(Haskell)",
				"a048298 n = a209229 n * n  -- _Reinhard Zumkeller_, Oct 17 2015"
			],
			"xref": [
				"A kind of inverse to A048272. Cf. A060147.",
				"This is Guy Steele's sequence GS(5, 1) (see A135416).",
				"Cf. A209229 (characteristic function of powers of 2)."
			],
			"keyword": "easy,nonn,mult",
			"offset": "0,3",
			"author": "_Adam Kertesz_",
			"ext": [
				"More terms from Keiko L. Noble (s1180624(AT)cedarville.edu)"
			],
			"references": 18,
			"revision": 60,
			"time": "2017-06-21T04:00:35-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}