{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A338146",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 338146,
			"data": "1,1,4,9,6,1,216,22164,613804,6901425,39713430,131754420,267165360,336798000,257796000,109771200,19958400,1,90052,1471369998,1460163153852,303126054092610,22838390261305920,831533453035309605",
			"name": "Triangle read by rows: T(n,k) is the number of oriented colorings of the edges of a regular n-D orthoplex (or ridges of a regular n-D orthotope) using exactly k colors. Row 1 has 1 column; row n\u003e1 has 2*n*(n-1) columns.",
			"comment": [
				"Each chiral pair is counted as two when enumerating oriented arrangements. A ridge is an (n-2)-face of an n-D polytope. For n=1, the figure is a line segment with one edge. For n=2, the figure is a square with 4 edges (vertices). For n=3, the figure is an octahedron (cube) with 12 edges. For n\u003e1, the number of edges (ridges) is 2*n*(n-1). The Schläfli symbols for the n-D orthotope (hypercube) and the n-D orthoplex (hyperoctahedron, cross polytope) are {4,3,...,3,3} and {3,3,...,3,4} respectively, with n-2 3's in each case. The figures are mutually dual.",
				"The algorithm used in the Mathematica program below assigns each permutation of the axes to a partition of n and then considers separate conjugacy classes for axis reversals. It uses the formulas in Balasubramanian's paper. If the value of m is increased, one can enumerate colorings of higher-dimensional elements beginning with T(m,1)."
			],
			"link": [
				"K. Balasubramanian, \u003ca href=\"https://doi.org/10.33187/jmsm.471940\"\u003eComputational enumeration of colorings of hyperplanes of hypercubes for all irreducible representations and applications\u003c/a\u003e, J. Math. Sci. \u0026 Mod. 1 (2018), 158-180."
			],
			"formula": [
				"For n\u003e1, A337411(n,k) = Sum_{j=1..2*n*(n-1)} T(n,j) * binomial(k,j).",
				"T(n,k) = A338147(n,k) + A338148(n,k) = 2*A338147(n,k) - A338149(n,k) = 2*A338148(n,k) + A338149(n,k).",
				"T(2,k) = A338142(2,k) = A325016(2,k) = A325008(2,k); T(3,k) = A338142(3,k)."
			],
			"example": [
				"Triangle begins with T(1,1):",
				"  1",
				"  1   4     9      6",
				"  1 216 22164 613804 6901425 39713430 131754420 267165360 336798000",
				"  ...",
				"For T(2,3)=9, the 3 achiral colorings are ABAC, ABCB, and ACBC. The three chiral pairs are AABC-AACB, ABBC-ACBB, and ABCC-ACCB."
			],
			"mathematica": [
				"m=1; (* dimension of color element, here an edge *)",
				"Fi1[p1_] := Module[{g, h}, Coefficient[Product[g = GCD[k1, p1]; h = GCD[2 k1, p1]; (1 + 2 x^(k1/g))^(r1[[k1]] g) If[Divisible[k1, h], 1, (1+2x^(2 k1/h))^(r2[[k1]] h/2)], {k1, Flatten[Position[cs, n1_ /; n1 \u003e 0]]}], x, m+1]];",
				"FiSum[] := (Do[Fi2[k2] = Fi1[k2], {k2, Divisors[per]}]; DivisorSum[per, DivisorSum[d1 = #, MoebiusMu[d1/#] Fi2[#] \u0026]/# \u0026]);",
				"CCPol[r_List] := (r1 = r; r2 = cs - r1; If[EvenQ[Sum[If[EvenQ[j3], r1[[j3]], r2[[j3]]], {j3, n}]], (per = LCM @@ Table[If[cs[[j2]] == r1[[j2]], If[0 == cs[[j2]], 1, j2], 2j2], {j2, n}]; Times @@ Binomial[cs, r1] 2^(n-Total[cs]) b^FiSum[]), 0]);",
				"PartPol[p_List] := (cs = Count[p, #]\u0026/@ Range[n]; Total[CCPol[#]\u0026/@ Tuples[Range[0, cs]]]);",
				"pc[p_List] := Module[{ci, mb}, mb = DeleteDuplicates[p]; ci = Count[p, #]\u0026/@ mb; n!/(Times@@(ci!) Times@@(mb^ci))] (*partition count*)",
				"row[m]=b; row[n_Integer] := row[n] = Factor[(Total[(PartPol[#] pc[#])\u0026/@ IntegerPartitions[n]])/(n! 2^(n-1))]",
				"array[n_, k_] := row[n] /. b -\u003e k",
				"Join[{{1}},Table[LinearSolve[Table[Binomial[i,j],{i,2^(m+1)Binomial[n,m+1]},{j,2^(m+1)Binomial[n,m+1]}], Table[array[n,k],{k,2^(m+1)Binomial[n,m+1]}]], {n,m+1,m+4}]] // Flatten"
			],
			"xref": [
				"Cf. A338147 (unoriented), A338148 (chiral), A338149 (achiral), A337411 (k or fewer colors), A325008 (orthoplex vertices, orthotope facets).",
				"Cf. A327087 (simplex), A338142 (orthotope edges, orthoplex ridges)."
			],
			"keyword": "nonn,tabf",
			"offset": "1,3",
			"author": "_Robert A. Russell_, Oct 12 2020",
			"references": 5,
			"revision": 8,
			"time": "2020-10-14T10:50:10-04:00",
			"created": "2020-10-14T10:50:10-04:00"
		}
	]
}