{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A129134",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 129134,
			"data": "1,1,-2,-1,0,2,0,-1,3,0,-2,-2,0,0,0,-1,2,3,-2,0,0,2,0,-2,1,0,-4,0,0,0,0,-1,4,2,0,-3,0,2,0,0,2,0,-2,-2,0,0,0,-2,1,1,-4,0,0,4,0,0,4,0,-2,0,0,0,0,-1,0,4,-2,-2,0,0,0,-3,2,0,-2,-2,0,0,0,0,5,2,-2,0,0,2,0,-2,2,0,0,0,0,0,0,-2,2,1,-6,-1,0,4,0,0,0",
			"name": "Expansion of (1 - phi(-q) * phi(-q^2)) / 2 in powers of q where phi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"For n nonzero, a(n) is nonzero if and only if n is in A002479."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A129134/b129134.txt\"\u003eTable of n, a(n) for n = 1..5000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of (1 - eta(q)^2 * eta(q^2) / eta(q^4)) / 2 in powers of q.",
				"G.f.: (1 - Product_{k\u003e0} (1 - x^k)^2 / (1 + x^(2*k)) )/2.",
				"a(n) = A002325(n) * (-1)^floor((n-1)/2}. A082564(n) = -2 * a(n) unless n=0.",
				"a(3*n + 1) = A258747(n). a(3*n + 2) = A258764(n). - _Michael Somos_, Jun 09 2015"
			],
			"example": [
				"G.f. = q + q^2 - 2*q^3 - q^4 + 2*q^6 - q^8 + 3*q^9 - 2*q^11 - 2*q^12 - q^16 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 1, 0, (-1)^Quotient[ n - 1, 2] DivisorSum[n, KroneckerSymbol[-2, #] \u0026]]; (* _Michael Somos_, Jun 09 2015 *)",
				"a[ n_] := SeriesCoefficient[ (1 - EllipticTheta[ 4, 0, q] EllipticTheta[ 4, 0, q^2]) / 2 , {q, 0, n}]; (* _Michael Somos_, Jun 09 2015 *)",
				"a[ n_] := SeriesCoefficient[ (1 - QPochhammer[ q]^2 QPochhammer[ q^2] / QPochhammer[ q^4]) / 2 , {q, 0, n}]; (* _Michael Somos_, Jun 09 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, 0, (-1)^((n-1)\\2) * sumdiv(n, d, kronecker( -2, d)))};",
				"(PARI) {a(n) = local(A); if( n\u003c1, 0, A = x * O(x^n); polcoeff( (1 - eta(x + A)^2 * eta(x^2 + A) / eta(x^4 + A)) / 2, n))};"
			],
			"xref": [
				"Cf. A002325, A002479, A082564, A258747, A258764."
			],
			"keyword": "sign",
			"offset": "1,3",
			"author": "_Michael Somos_, Mar 30 2007",
			"references": 4,
			"revision": 19,
			"time": "2021-03-12T22:24:44-05:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}