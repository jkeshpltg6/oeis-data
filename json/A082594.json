{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A082594",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 82594,
			"data": "2,1,2,3,6,15,38,91,206,443,900,1701,2914,4303,4748,1081,-14000,-55335,-150394,-346163,-716966,-1369429,-2432788,-4002993,-5964748,-7525017,-6123026,4900093,40900520,134308945,348584680,798958751,1678213244,3277458981,5972923998,10110994307",
			"name": "Constant term when a polynomial of degree n-1 is fitted to the first n primes.",
			"comment": [
				"The polynomial is to pass through the points (k, prime(k)), k=1..n.",
				"The constant term is always an integer because it is the same as f(0), which can be computed from the difference table of the sequence of primes. See Conway and Guy. In fact, the interpolating polynomial is integral for all integer arguments.",
				"A plot of the first 1000 terms shows that the sequence grows exponentially and changes signs occasionally. The Mathematica lines show two ways of computing the sequence. The second, which uses the difference table, is much faster.",
				"The dual sequence (in the sense of Sun, q.v.) of the primes. - _Charles R Greathouse IV_, Oct 03 2013"
			],
			"reference": [
				"J. H. Conway and R. K. Guy, The Book of Numbers, Copernicus Press, NY, 1996, p. 80"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A082594/b082594.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Author?, \u003ca href=\"http://groups.msn.com/BC2LCC/page.msnw?fc_p=%2FSicurv%20%2D%20Simul%20Equ%20and%20Curve%20Fitting\u0026amp;fc_a=0\"\u003eSicurvqf\u003c/a\u003e",
				"T. D. Noe, \u003ca href=\"http://www.sspectra.com/math/A082594.gif\"\u003ePlot of A082594\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://maths.nju.edu.cn/~zwsun/60e.pdf\"\u003eCombinatorial identities in dual sequences\u003c/a\u003e, European J. Combin. 24:6 (2003), pp. 709-718."
			],
			"formula": [
				"a(n) = sum{k=1, .., n} (-1)^(k+1) A007442(k)"
			],
			"example": [
				"For n=4, we fit a cubic through the 4 points (1,2),(2,3),(3,5),(4,7) to obtain a(4) = 3."
			],
			"mathematica": [
				"Table[Coefficient[Expand[InterpolatingPolynomial[Prime[Range[n]], x]], x, 0], {n, 50}]",
				"Diff[lst_List] := Table[lst[[i+1]]-lst[[i]], {i, Length[lst]-1}]; n=50; dt=Table[{}, {n}]; dt[[1]]=Prime[Range[n]]; Do[dt[[i]]=Diff[dt[[i-1]]], {i, 2, n}]; Table[s=dt[[i, 1]]; Do[s=dt[[i-j, 1]]-s, {j, i-1}]; s, {i, n}]"
			],
			"program": [
				"(PARI) dual(v:vec)=vector(#v,i,-sum(j=0,i-1,binomial(i-1,j)*(-1)^j*v[j+1]))",
				"dual(concat(0,primes(100)))[2..101] \\\\ _Charles R Greathouse IV_, Oct 03 2013",
				"(PARI) {a(n) = sum(k=0, n-1, sum(i=0, k, binomial(k, i) * (-1)^i * prime(i+1)))}; /* _Michael Somos_, Dec 02 2020 */"
			],
			"xref": [
				"Cf. A007442, A140119."
			],
			"keyword": "sign",
			"offset": "1,1",
			"author": "_Cino Hilliard_, May 08 2003",
			"ext": [
				"Edited by _T. D. Noe_, May 08 2003"
			],
			"references": 6,
			"revision": 23,
			"time": "2020-12-02T16:43:27-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}