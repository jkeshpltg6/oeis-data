{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A056242",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 56242,
			"data": "1,1,2,1,5,4,1,9,16,8,1,14,41,44,16,1,20,85,146,112,32,1,27,155,377,456,272,64,1,35,259,833,1408,1312,640,128,1,44,406,1652,3649,4712,3568,1472,256,1,54,606,3024,8361,14002,14608,9312,3328,512,1,65,870,5202",
			"name": "Triangle read by rows: T(n,k) = number of k-part order-consecutive partition of {1,2,...,n} (1 \u003c= k \u003c= n).",
			"comment": [
				"Generalized Riordan array (1/(1-x), x/(1-x) + x*dif(x/1-x),x)). - _Paul Barry_, Dec 26 2007",
				"Reversal of A117317. - _Philippe Deléham_, Feb 11 2012",
				"Essentially given by (1, 0, 1/2, 1/2, 0, 0, 0, 0, 0, 0, 0, ...) DELTA (0, 2, 0, 0, 0, 0, 0, 0, 0, ...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, Feb 11 2012",
				"This sequence is given in the Strehl presentation with the o.g.f. (1-z)/[1-2(1+t)z+(1+t)z^2], with offset 0, along with a recursion relation, a combinatorial interpretation, and relations to Hermite and Laguerre polynomials. Note that the o.g.f. is related to that of A049310. - _Tom Copeland_, Jan 08 2017",
				"From _Gus Wiseman_, Mar 06 2020: (Start)",
				"T(n,k) is also the number of unimodal length-n sequences covering an initial interval of positive integers with maximum part k, where a sequence of integers is unimodal if it is the concatenation of a weakly increasing and a weakly decreasing sequence. For example, the sequences counted by row n = 4 are:",
				"  (1111)  (1112)  (1123)  (1234)",
				"          (1121)  (1132)  (1243)",
				"          (1122)  (1223)  (1342)",
				"          (1211)  (1231)  (1432)",
				"          (1221)  (1232)  (2341)",
				"          (1222)  (1233)  (2431)",
				"          (2111)  (1321)  (3421)",
				"          (2211)  (1322)  (4321)",
				"          (2221)  (1332)",
				"                  (2231)",
				"                  (2311)",
				"                  (2321)",
				"                  (2331)",
				"                  (3211)",
				"                  (3221)",
				"                  (3321)",
				"(End)"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A056242/b056242.txt\"\u003eRows n = 1..125 of table, flattened\u003c/a\u003e",
				"Tyler Clark and Tom Richmond, \u003ca href=\"http://people.wku.edu/tom.richmond/Papers/CountConvexTopsFTOsets.pdf\"\u003eThe Number of Convex Topologies on a Finite Totally Ordered Set\u003c/a\u003e, 2013, Involve, Vol. 8 (2015), No. 1, 25-32.",
				"F. K. Hwang and C. L. Mallows, \u003ca href=\"http://dx.doi.org/10.1016/0097-3165(95)90097-7\"\u003eEnumerating nested and consecutive partitions\u003c/a\u003e, J. Combin. Theory Ser. A 70 (1995), no. 2, 323-333.",
				"V. Strehl, \u003ca href=\"http://www.emis.de/journals/SLC/wpapers/s71vortrag/strehl.pdf\"\u003eCombinatoire rétrospective et créative\u003c/a\u003e, an on-line presentation, slide 36, SLC 71, Bertinoro,, September 18, 2013.",
				"Volker Strehl, \u003ca href=\"http://www.mat.univie.ac.at/~slc/wpapers/s76strehl.html\"\u003eLacunary Laguerre Series from a Combinatorial Perspective\u003c/a\u003e, Séminaire Lotharingien de Combinatoire, B76c (2017)."
			],
			"formula": [
				"The Hwang and Mallows reference gives explicit formulas.",
				"T(n,k) = Sum_{j=0..k-1} (-1)^(k-1-j)*binomial(k-1, j)*binomial(n+2j-1, 2j) (1\u003c=k\u003c=n); this is formula (11) in the Huang and Mallows reference.",
				"T(n,k) = 2*T(n-1,k) + 2*T(n-1,k-1) - T(n-2,k) - T(n-2,k-1), T(1,1) = 1, T(2,1) = 1, T(2,2) = 2. - _Philippe Deléham_, Feb 11 2012",
				"G.f.: -(-1+x)*x*y/(1-2*x-2*x*y+x^2*y+x^2). - _R. J. Mathar_, Aug 11 2015"
			],
			"example": [
				"Triangle begins:",
				"  1;",
				"  1,    2;",
				"  1,    5,    4;",
				"  1,    9,   16,    8;",
				"  1,   14,   41,   44,   16;",
				"  1,   20,   85,  146,  112,   32;",
				"  1,   27,  155,  377,  456,  272,   64;",
				"  1,   35,  259,  833, 1408, 1312,  640,  128;",
				"  1,   44,  406, 1652, 3649, 4712, 3568, 1472,  256;",
				"T(3,2)=5 because we have {1}{23}, {23}{1}, {12}{3}, {3]{12} and {2}{13}.",
				"Triangle (1, 0, 1/2, 1/2, 0, 0, 0, ...) DELTA (0, 2, 0, 0, 0, ...) begins:",
				"  1;",
				"  1,   0;",
				"  1,   2,   0;",
				"  1,   5,   4,   0;",
				"  1,   9,  16,   8,   0;",
				"  1,  14,  41,  44,  16,   0;",
				"  1,  20,  85, 146, 112,  32,   0;",
				"  1,  27, 155, 377, 456, 272,  64,   0;"
			],
			"maple": [
				"T:=proc(n,k) if k=1 then 1 elif k\u003c=n then sum((-1)^(k-1-j)*binomial(k-1,j)*binomial(n+2*j-1,2*j),j=0..k-1) else 0 fi end: seq(seq(T(n,k),k=1..n),n=1..12);"
			],
			"mathematica": [
				"rows = 11; t[n_, k_] := (-1)^(k+1)*HypergeometricPFQ[{1-k, (n+1)/2, n/2}, {1/2, 1}, 1]; Flatten[ Table[ t[n, k], {n, 1, rows}, {k, 1, n}]](* _Jean-François Alcover_, Nov 17 2011 *)"
			],
			"program": [
				"(Haskell)",
				"a056242 n k = a056242_tabl !! (n-1)!! (k-1)",
				"a056242_row n = a056242_tabl !! (n-1)",
				"a056242_tabl = [1] : [1,2] : f [1] [1,2] where",
				"   f us vs = ws : f vs ws where",
				"     ws = zipWith (-) (map (* 2) $ zipWith (+) ([0] ++ vs) (vs ++ [0]))",
				"                      (zipWith (+) ([0] ++ us ++ [0]) (us ++ [0,0]))",
				"-- _Reinhard Zumkeller_, May 08 2014"
			],
			"xref": [
				"Row sums are A007052.",
				"Column k = n - 1 is A053220.",
				"Ordered set-partitions are A000670.",
				"Cf. A001523, A049310, A072704, A097805, A227038, A328509, A332294, A332673, A332724, A332872."
			],
			"keyword": "nonn,tabl,easy,nice",
			"offset": "1,3",
			"author": "_Colin Mallows_, Aug 23 2000",
			"references": 13,
			"revision": 58,
			"time": "2020-05-07T12:40:45-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}