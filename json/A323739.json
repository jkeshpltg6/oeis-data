{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A323739",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 323739,
			"data": "2,1,1,2,6,30,180,1440,12960,142560,1995840,29937600,538876800,10777536000,226328256000,5205549888000,135344297088000,3924984615552000,117749538466560000,3885734769396480000,136000716928876800000,4896025809439564800000,190945006568143027200000",
			"name": "a(n) is the number of residues modulo (4*primorial(n)) of the squares of primes greater than or equal to prime(n+1).",
			"comment": [
				"Here, \"primorial(n)\" is A002110(n) = Product_{k=1..n} prime(k).",
				"For n \u003e= 1, a(n) is the number of coprime squares modulo 4*primorial(n). Note that 4*primorial(n) = A102476(n+1) is the smallest k such that rank((Z/kZ)*) = n+1 for n \u003e= 1. (The rank of a finitely generated group rank(G) is defined to be the size of the minimal generating sets of G. In particular, rank((Z/kZ)*) = 0 if k \u003c= 2 and A046072(k) otherwise.) - _Jianing Song_, Oct 18 2021"
			],
			"link": [
				"Jianing Song, \u003ca href=\"/A323739/b323739.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"Jianing Song, \u003ca href=\"/A323739/a323739.txt\"\u003eFurther terms: table of n, a(n) for n = 0..1000\u003c/a\u003e"
			],
			"formula": [
				"Conjecture: a(n) = 2^(1-n)*Product_{j=1..n} (prime(j)-1) for n \u003e= 0, so a(n) = a(n-1)*(prime(n)-1)/2 for n \u003e= 1.",
				"From _Charlie Neder_, Feb 28 2019: (Start)",
				"Conjecture is true. Since there exists a prime congruent to r modulo 4*primorial(n) for any r coprime to primorial(n), this set is precisely the set of coprime quadratic residues of 4*primorial(n). If n \u003e= 1, each residue can be broken down into congruences modulo 8 and the first n-1 odd primes, each odd prime p has (p-1)/2 residue classes, and every combination eventually occurs, giving the formula. (End)"
			],
			"example": [
				"a(3) = 2 because, for every prime p \u003e= prime(3+1) = 7, p^2 mod (4*2*3*5 = 120) is one of the 2 values {1, 49}:",
				"   7^2 mod 120 =  49 mod 120 = 49",
				"  11^2 mod 120 = 121 mod 120 =  1",
				"  13^2 mod 120 = 169 mod 120 = 49",
				"  17^2 mod 120 = 289 mod 120 = 49",
				"  19^2 mod 120 = 361 mod 120 =  1",
				"  23^2 mod 120 = 529 mod 120 = 49",
				"  29^2 mod 120 = 841 mod 120 =  1",
				"  ...",
				".",
				"   q=(n+1)st        b =          residues p^2 mod b",
				"n    prime    4*primorial(n)         for p \u003e= q         a(n)",
				"=  =========  ===============  =======================  ====",
				"0      2      4         =   4           {0,1}             2",
				"1      3      4*2       =   8            {1}              1",
				"2      5      4*2*3     =  24            {1}              1",
				"3      7      4*2*3*5   = 120           {1,49}            2",
				"4     11      4*2*3*5*7 = 840  {1,121,169,289,361,529}    6"
			],
			"program": [
				"(PARI) a(n) = if(n==0, 2, my(t=1); forprime(p=3, , t*=(p-1)/2; if(n--\u003c2, return(t)))) \\\\ _Jianing Song_, Oct 18 2021, following _Charles R Greathouse IV_'s program for A078586"
			],
			"xref": [
				"Cf. A002110, A005867, A240775, A046072, A102476."
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "_Jon E. Schoenfield_, Feb 20 2019",
			"ext": [
				"More terms from _Jianing Song_, Oct 18 2021"
			],
			"references": 3,
			"revision": 32,
			"time": "2021-11-13T13:54:51-05:00",
			"created": "2019-02-26T05:05:51-05:00"
		}
	]
}