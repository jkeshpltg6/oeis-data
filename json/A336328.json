{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A336328",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 336328,
			"data": "57,65,73,73,88,95,43,147,152,127,168,205,97,185,208,111,221,280,49,285,296,95,312,343,296,315,361,152,343,387,323,392,407,147,377,437,285,464,469,255,343,473,247,408,485,469,589,624,403,725,728,871,901,931",
			"name": "Primitive triples for integer-sided triangles with A \u003c B \u003c C \u003c 2*Pi/3 and such that FA + FB + FC is an integer where F is the Fermat point of the triangle.",
			"comment": [
				"Inspired by Project Euler, Problem 143 (see link).",
				"The triples are displayed in increasing order of largest side, and if largest sides coincide then by increasing order of the middle side; also, each triple (a, b, c) is in increasing order.",
				"If one angle of the triangle, for example C, is \u003e= 2*Pi/3 then the Fermat point F is this vertex C, so, FA + FB + FC becomes CA + CB, while when all angles are \u003c 2*Pi/3, then the Fermat point is inside the triangle (see link Fermat points), this last condition means that c^2 \u003c a^2 + a*b + b^2.",
				"For the terms of the data, every FA, FB, FC is a fraction but FA + FB + FC is an integer (see example).",
				"If FA + FB + FC = d, then we have this \"beautifully symmetric equation\" between a, b, c and d: 3*(a^4 + b^4 + c^4 + d^4) = (a^2 + b^2 + c^2 + d^2)^2 (see Martin Gardner).",
				"Equivalently: if a point M is inside an equilateral triangle A'B'C' and integer distances to vertices are MA' = a = A072054(n), MB' = b = A072053(n), MC' = c = A072052(n), then the side of this equilateral triangle A'B'C' is equal to d = FA + FB + FC = A061281(n) where F is the Fermat point of the triangle ABC with sides (a,b,c) (see Martin Gardner)."
			],
			"reference": [
				"Martin Gardner, Mathematical Circus, Elegant triangles, First Vintage Books Edition, 1979, p. 65."
			],
			"link": [
				"Project Euler, \u003ca href=\"https://projecteuler.net/problem=143\"\u003eProblem 143 - Investigating the Torricelli point of a triangle\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"https://mathworld.wolfram.com/FermatPoints.html\"\u003eFermat points\u003c/a\u003e."
			],
			"formula": [
				"If FA + FB + FC = d, then",
				"d = sqrt(((a^2 + b^2 + c^2)/2) + (1/2) * sqrt(6*(a^2*b^2 + b^2*c^2 + c^2*a^2) - 3*(a^4 + b^4 + c^4))), or,",
				"d^2 = (1/2) * (a^2 + b^2 + c^2) + 2 * S * sqrt(3) where S = area of triangle ABC."
			],
			"example": [
				"For first triple (57, 65, 73) and corresponding d = FA + FB + FC = 264/7 + 195/7 + 325/7 = 112, relation gives: 3*(57^4 + 65^4 + 73^4 + 112^4) = (57^2 + 65^2 + 73^2 + 112^2)^2 = 642470409."
			],
			"xref": [
				"Cf. A336329 (FA + FB + FC), A336330 (smallest side), A336331 (middle side), A336332 (largest side), A336333 (perimeter).",
				"Cf. A333391 (with isogonic center).",
				"Cf. A061281, A072052, A072053, A072054."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Bernard Schott_, Jul 17 2020",
			"references": 5,
			"revision": 20,
			"time": "2020-07-20T11:53:18-04:00",
			"created": "2020-07-20T11:53:18-04:00"
		}
	]
}