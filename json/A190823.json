{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A190823",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 190823,
			"data": "1,0,0,1,10,99,1146,15422,237135,4106680,79154927,1681383864,39034539488,983466451011,26728184505750,779476074425297,24281301468714902,804688068731837874,28269541494090294129,1049450257149017422000,41050171013933837206545",
			"name": "Number of permutations of 2 copies of 1..n introduced in order 1..n with no element equal to another within a distance of 2.",
			"comment": [
				"From _Gus Wiseman_, Feb 27 2019: (Start)",
				"Also the number of 2-uniform set partitions of {1..2n} such that no block has its two vertices differing by less than 3. For example, the a(4) = 10 set partitions are:",
				"  {{1,4},{2,6},{3,7},{5,8}}",
				"  {{1,4},{2,7},{3,6},{5,8}}",
				"  {{1,5},{2,6},{3,7},{4,8}}",
				"  {{1,5},{2,6},{3,8},{4,7}}",
				"  {{1,5},{2,7},{3,6},{4,8}}",
				"  {{1,5},{2,8},{3,6},{4,7}}",
				"  {{1,6},{2,5},{3,7},{4,8}}",
				"  {{1,6},{2,5},{3,8},{4,7}}",
				"  {{1,7},{2,5},{3,6},{4,8}}",
				"  {{1,8},{2,5},{3,6},{4,7}}",
				"(End)"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A190823/b190823.txt\"\u003eTable of n, a(n) for n = 0..404\u003c/a\u003e",
				"Dmitry Efimov, \u003ca href=\"https://arxiv.org/abs/2101.09722\"\u003eHafnian of two-parameter matrices\u003c/a\u003e, arXiv:2101.09722 [math.CO], 2021.",
				"Everett Sullivan, \u003ca href=\"https://arxiv.org/abs/1611.02771\"\u003eLinear chord diagrams with long chords\u003c/a\u003e, arXiv preprint arXiv:1611.02771 [math.CO], 2016."
			],
			"formula": [
				"a(n) = (2n+2)a(n-1) - (6n-10)a(n-2) + (6n-16)a(n-3) - (2n-8)a(n-4) - a(n-5) (proved). - _Everett Sullivan_, Mar 16 2017",
				"a(n) ~ 2^(n+1/2) * n^n / exp(n+2), based on Sullivan's formula. - _Vaclav Kotesovec_, Mar 21 2017"
			],
			"example": [
				"All solutions for n=4 (read downwards):",
				"  1    1    1    1    1    1    1    1    1    1",
				"  2    2    2    2    2    2    2    2    2    2",
				"  3    3    3    3    3    3    3    3    3    3",
				"  4    4    4    4    1    4    4    1    4    4",
				"  1    1    2    1    4    2    1    4    2    2",
				"  3    3    1    2    2    3    2    3    1    3",
				"  2    4    4    4    3    4    3    2    3    1",
				"  4    2    3    3    4    1    4    4    4    4"
			],
			"mathematica": [
				"a[1]=0; a[2]=0; a[3]=1; a[4]=10; a[5]=99; a[n_] := a[n] = (2*n+2) a[n-1] - (6*n-10) a[n-2] + (6*n-16) a[n-3] - (2*n-8) a[n-4] - a[n-5]; Array[a, 20] (* based on Sullivan's formula, _Giovanni Resta_, Mar 20 2017 *)",
				"dtui[{}]:={{}};dtui[set:{i_,___}]:=Join@@Function[s,Prepend[#,s]\u0026/@dtui[Complement[set,s]]]/@Table[{i,j},{j,Select[set,#\u003ei+2\u0026]}];",
				"Table[Length[dtui[Range[n]]],{n,0,12,2}] (* _Gus Wiseman_, Feb 27 2019 *)"
			],
			"xref": [
				"Distance of 1 instead of 2 gives |A000806|.",
				"Column k=3 of A293157.",
				"Cf. A000699, A001147 (2-uniform set partitions), A003436, A005493, A011968, A170941, A278990 (distance 2+ version), A306386 (cyclical version)."
			],
			"keyword": "nonn,easy",
			"offset": "0,5",
			"author": "_R. H. Hardin_, May 21 2011",
			"ext": [
				"a(16)-a(20) (using _Everett Sullivan_'s formula) from _Giovanni Resta_, Mar 20 2017",
				"a(0)=1 prepended by _Alois P. Heinz_, Oct 17 2017"
			],
			"references": 6,
			"revision": 43,
			"time": "2021-05-06T21:22:18-04:00",
			"created": "2011-05-21T11:52:45-04:00"
		}
	]
}