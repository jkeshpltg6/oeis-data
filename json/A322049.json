{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A322049",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 322049,
			"data": "1,7,6,30,8,48,17,81,9,50,29,145,27,145,37,189,8,45,34,166,45,252,73,342,37,179,89,425,74,374,86,412,8,49,33,165,46,270,91,436,50,277,149,734,122,630,144,723,38,179,101,488,130,753,209,990,90,450,210,991",
			"name": "When A322050 is displayed as a triangle the rows converge to this sequence.",
			"comment": [
				"It would be nice to have a formula or recurrence. There is certainly a lot of structure.",
				"Indices of records of a(n)/n are (1, 3, 7, 11, 23, 27, 43, 55, 87, 91, 119, 171, 183, 343, 347, 363, 367, 375, 439, 695, 731, 887, 1367, 1371, 1391, 1399, 1451, 1463, 2743, 2923, 2927, 2935, 3511, ...). The ratio a(n)/n increases roughly by 1 at each of these. We conjecture that this ratio is unbounded. We note that the record ratios occur in \"clusters\" at indices twice as large as the preceding cluster: 87, 91; 171, 183; 343..375; 695..731; 1367..1463; 2743..2935; ... This is compatible with the self-similar structure of the graph of this sequence, which starts over at a(2^k) = 8 for all k \u003e= 4. (But note also the distinctive sub-structure repeating with period 2^10, cf. the \"logarithmic plot\" link.) - _M. F. Hasler_, Dec 18 2018"
			],
			"link": [
				"Hugo Pfoertner, \u003ca href=\"/A322049/b322049.txt\"\u003eTable of n, a(n) for n = 0..5461\u003c/a\u003e",
				"Hugo Pfoertner, \u003ca href=\"/A322049/a322049.pdf\"\u003eLogarithmic plot of 5462 terms\u003c/a\u003e, use zoom to see details."
			],
			"formula": [
				"From _M. F. Hasler_, Dec 18 2018: (Start)",
				"Experimental data suggests the following properties:",
				"Sporadic values occurring only a finite number of times, with no regular pattern:",
				"  a(n) | 1 | 6 | 7 | 9 |   37   | 48 |  50   | 53 | ...",
				"  -----+---+---+---+---+--------+----+-------+----+-----",
				"    n  | 0 | 2 | 1 | 8 | 14, 24 |  5 | 9, 40 | 80 | ...",
				"Values occurring in regular patterns:",
				"a(n) = 8 iff n = 2^k, k = 2 or k \u003e= 4; a(n) \u003e 8 for all other n \u003e 2.",
				"a(n) = 33 iff n = 2^(2k+1) + 2, k \u003e= 2; a(n) \u003e 33 for all other n \u003e 12 unless n = 2^k \u003c=\u003e a(n) = 8.",
				"a(n) = 34 iff n = 4^k + 2, k \u003e= 2.",
				"a(n) = 38 iff n = 3*2^k, k = 4, 5, 6, 8, 10, ...",
				"a(n) = 27*2^m if n = 3*2^k with k = 2 (m = 0) or k = 7, 9, ... (m = 1, 2, ...)",
				"a(n) = 45 iff n = 20 or n = 4^k + 1, k \u003e= 2.",
				"a(n) = 46 iff n = 2^(2k+1) + 4, k \u003e= 2.",
				"a(n) = 49 iff n = 2^(2k+1) + 1, k \u003e= 2, or n = 4^k + 4, k \u003e= 3.",
				"a(n) \u003e 50 for all n \u003e 10 not mentioned above. (End)"
			],
			"xref": [
				"Cf. A319018, A319019, A322048, A322050, A322051."
			],
			"keyword": "nonn,look",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Dec 15 2018",
			"references": 7,
			"revision": 34,
			"time": "2018-12-18T15:46:32-05:00",
			"created": "2018-12-15T22:50:06-05:00"
		}
	]
}