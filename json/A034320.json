{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A034320",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 34320,
			"data": "1,1,1,2,2,3,4,5,6,8,10,12,15,18,22,27,32,38,46,54,64,76,89,104,122,141,164,191,220,254,293,336,385,442,504,575,656,745,846,960,1086,1228,1388,1564,1762,1984,2228,2501,2806,3142,3516,3932,4390,4898,5462,6082",
			"name": "Coefficients of completely replicable function 50a with a(0) = 1.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"reference": [
				"F. Calegari, Review of \"A first Course in modular forms\" by F. Diamond and J. Shurman, Bull. Amer. Math. Soc., 43 (No. 3, 2006), 415-421. See p. 418"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A034320/b034320.txt\"\u003eTable of n, a(n) for n = -1..1000\u003c/a\u003e",
				"D. Alexander, C. Cummins, J. McKay and C. Simons, \u003ca href=\"http://oeis.org/A007242/a007242_1.pdf\"\u003eCompletely Replicable Functions\u003c/a\u003e, LMS Lecture Notes, 165, ed. Liebeck and Saxl (1992), 87-98, annotated and scanned copy.",
				"I. Chen and N. Yui, \u003ca href=\"http://www.math.sfu.ca/~ichen/pub.html\"\u003eSingular values of Thompson series\u003c/a\u003e. In Groups, difference sets and the Monster (Columbus, OH, 1993), pp. 255-326, Ohio State University Mathematics Research Institute Publications, 4, de Gruyter, Berlin, 1996.",
				"D. Ford, J. McKay and S. P. Norton, \u003ca href=\"http://dx.doi.org/10.1080/00927879408825127\"\u003eMore on replicable functions\u003c/a\u003e, Commun. Algebra 22, No. 13, 5175-5193 (1994).",
				"H. D. Nguyen, D. Taggart, \u003ca href=\"http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.391.2522\u0026amp;rep=rep1\u0026amp;type=pdf\"\u003eMining the OEIS: Ten Experimental Conjectures\u003c/a\u003e, 2013; Mentions this sequence.",
				"H. D. Nguyen, D. Taggart, \u003ca href=\"http://users.rowan.edu/~nguyen/papers/Mining%20the%20Online%20Encyclopedia%20of%20Integer%20Sequences.pdf\"\u003eMining the OEIS: Ten Experimental Conjectures\u003c/a\u003e, 2013",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e",
				"\u003ca href=\"/index/Mat#McKay_Thompson\"\u003eIndex entries for McKay-Thompson series for Monster simple group\u003c/a\u003e"
			],
			"formula": [
				"Expansion of Hauptmodul for Gamma_0(50)+50 in powers of q.",
				"Expansion of q^(-1) * chi(-q^25) / chi(-q) in powers of q where chi() is a Ramanujan theta function. - _Michael Somos_, Jun 09 2007",
				"Expansion of (eta(q^2) * eta(q^25)) / (eta(q) * eta(q^50)) in powers of q. - _Michael Somos_, Sep 20 2004",
				"Euler transform of period 50 sequence [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, ...]. - _Michael Somos_, Sep 20 2004",
				"G.f. is Fourier series of a weight 0 level 50 modular form. f(-1 / (50 t)) = f(t) where q = exp(2 Pi i t). - _Michael Somos_, Jun 09 2007",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2), A(x^4)) where f(u, v, w) = u^2*v + 2*u*w + 2*u*v^2*w + v*w^2 - v^2 - u^2*w^2. - _Michael Somos_, Jun 09 2007",
				"G.f.: 1/x * (Product_{k\u003e0} (1 + x^k) / (1 + x^(25*k))).",
				"a(n) = A058703(n) unless n=0.",
				"a(n) ~ exp(2*Pi*sqrt(2*n)/5) / (2^(3/4) * sqrt(5) * n^(3/4)). - _Vaclav Kotesovec_, Sep 06 2015"
			],
			"example": [
				"G.f. = q^-1 + 1 + q + 2*q^2 + 2*q^3 + 3*q^4 + 4*q^5 + 5*q^6 + 6*q^7 + 8*q^8 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ q^-1 QPochhammer[q^25, q^50] / QPochhammer[q, q^2], {q, 0, n}]; (* _Michael Somos_, Jul 11 2011 *)",
				"a[ n_] := SeriesCoefficient[ q^-1 Product[1 + q^k, {k, n + 1}] / Product[1 + q^k, {k, 25, n + 1, 25}], {q, 0, n}]; (* _Michael Somos_, Jul 11 2011 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c-1, 0, n++; A = 1 + x * O(x^n); polcoeff( prod( k=1, n, 1 + x^k, A) / prod( k=1, n\\25, 1 + x^(25*k), A),n))}; /* _Michael Somos_, Sep 20 2004 */",
				"(PARI) {a(n) = my(A); if( n\u003c-1, 0, n++; A = x * O(x^n); polcoeff( eta(x^2 + A) * eta(x^25 + A) / (eta(x + A) * eta(x^50 + A)), n))}; /* _Michael Somos_, Sep 20 2004 */",
				"(PARI) N=66; q='q+O('q^N); Vec( (eta(q^2)*eta(q^25))/(eta(q)*eta(q^50))/q ) \\\\ _Joerg Arndt_, Apr 09 2016"
			],
			"xref": [
				"Cf. A034321, A058703."
			],
			"keyword": "nonn",
			"offset": "-1,4",
			"author": "_N. J. A. Sloane_",
			"references": 4,
			"revision": 53,
			"time": "2021-03-12T22:24:42-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}