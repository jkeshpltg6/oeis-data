{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A249814",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 249814,
			"data": "1,2,3,4,5,8,7,6,9,14,15,24,13,20,11,10,17,26,27,34,29,44,47,48,25,38,39,54,21,32,19,12,33,50,51,64,53,80,67,76,57,86,87,114,93,140,95,120,49,74,75,94,77,116,107,90,41,62,63,84,37,56,23,16,65,98,99,124,101,152,127,118,105,158,159,204,133,200,151,142",
			"name": "\"Mountains of Eratosthenes\" permutation: a(1) = 1, a(n) = A249741(A001511(n), a(A003602(n))).",
			"comment": [
				"This sequence is a \"recursed variant\" of A249811.",
				"From _Antti Karttunen_, Jan 18 2015: (Start)",
				"This can be viewed as an entanglement or encoding permutation where the complementary pairs of sequences to be interwoven together are even and odd numbers (A005843/A005408) which are entangled with another complementary pair: even numbers in the order they appear in A253886 and odd numbers in their usual order: (A253886/A005408).",
				"From the above follows also that this sequence can be represented as a binary tree. Each child to the left is obtained by doubling the parent and subtracting one, and each child to the right is obtained by applying A253886 to the parent:",
				"                                    1",
				"                                    |",
				"                 ...................2...................",
				"                3                                       4",
				"      5......../ \\........8                   7......../ \\........6",
				"     / \\                 / \\                 / \\                 / \\",
				"    /   \\               /   \\               /   \\               /   \\",
				"   /     \\             /     \\             /     \\             /     \\",
				"  9       14         15       24         13       20         11       10",
				"17 26   27  34     29  44   47  48     25  38   39  54     21  32   19  12",
				"(End)",
				"For listening I recommend some (mostly) percussive MIDI-instrument and the pitch offset set to at least 29 and the tempo (rate) to about 60. - _Antti Karttunen_, Feb 17 2015"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A249814/b249814.txt\"\u003eTable of n, a(n) for n = 1..8191\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"In the following formulas, A083221 and A249741 are interpreted as bivariate functions:",
				"a(1) = 1, for n\u003e1: a(n) = A083221(A001511(n), a(A003602(n))) - 1 = A249741(A001511(n), a(A003602(n))).",
				"a(1) = 1, a(2n) = A253886(a(n)), a(2n+1) = (2*a(n+1))-1. - _Antti Karttunen_, Jan 18 2015",
				"As a composition of other permutations:",
				"a(n) = A250244(A246684(n)).",
				"Other identities. For all n \u003e= 1, the following holds:",
				"a(n) = (1+a((2*n)-1))/2. [The odd bisection from a(1) onward with one added and then halved gives the sequence back.]",
				"a(A000079(n-1)) = A006093(n)."
			],
			"program": [
				"(Scheme, with memoizing macro definec from _Antti Karttunen_'s IntSeq-library, two different implementations)",
				"(definec (A249814 n) (cond ((\u003c= n 1) n) (else (+ -1 (A083221bi (A001511 n) (A249814 (A003602 n))))))) ;; Code for A083221bi given in A083221.",
				"(definec (A249814 n) (cond ((\u003c= n 1) n) ((even? n) (A253886 (A249814 (/ n 2)))) (else (+ -1 (* 2 (A249814 (/ (+ n 1) 2)))))))"
			],
			"xref": [
				"Inverse: A249813.",
				"Similar or related permutations: A246684, A249811, A250244, A252755.",
				"Cf. A000079, A000265, A001511, A003602, A006093, A083140, A083221, A249741, A250469, A253886.",
				"Compare also the scatterplot of this sequence to the graphs of A252755 and A246684.",
				"Differs from A246684 for the first time at n=14, where a(14) = 20, while A246684(14) = 26."
			],
			"keyword": "nonn,tabf,look,hear",
			"offset": "1,2",
			"author": "_Antti Karttunen_, Nov 06 2014",
			"references": 11,
			"revision": 46,
			"time": "2015-12-13T07:47:37-05:00",
			"created": "2014-11-15T16:49:43-05:00"
		}
	]
}