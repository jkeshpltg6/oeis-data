{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A052541",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 52541,
			"data": "1,3,9,28,87,270,838,2601,8073,25057,77772,241389,749224,2325444,7217721,22402387,69532605,215815536,669848995,2079079590,6453054306,20029011913,62166115329,192951400293,598883212792,1858815753705,5769398661408,17907079197016",
			"name": "Expansion of 1/(1-3*x-x^3).",
			"comment": [
				"A transform of A000244 under the mapping mapping g(x)-\u003e(1/(1-x^3))g(x/(1-x^3)). - _Paul Barry_, Oct 20 2004",
				"a(n) equals the number of n-length words on {0,1,2,3} such that 0 appears only in a run which length is a multiple of 3. - _Milan Janjic_, Feb 17 2015"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A052541/b052541.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=475\"\u003eEncyclopedia of Combinatorial Structures 475\u003c/a\u003e",
				"Milan Janjic, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL19/Janjic/janjic73.html\"\u003eBinomial Coefficients and Enumeration of Restricted Words\u003c/a\u003e, Journal of Integer Sequences, 2016, Vol 19, #16.7.3",
				"José L. Ramírez, Víctor F. Sirvent, \u003ca href=\"http://ami.ektf.hu/uploads/papers/finalpdf/AMI_45_from91to105.pdf\"\u003eA note on the k-Narayana sequence\u003c/a\u003e, Annales Mathematicae et Informaticae, 45 (2015) pp. 91-105.",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,0,1)."
			],
			"formula": [
				"G.f.: 1/(1 - 3*x - x^3).",
				"a(n) = 3*a(n-1) + a(n-3), with a(0)=1, a(1)=3.",
				"a(n) = Sum_{alpha = RootOf(-1+3*x+x^3)} (1/15)*(4 + alpha + 2*alpha^2) * alpha^(-n-1).",
				"a(n) = Sum_{k=0..floor(n/3)} binomial(n-2*k, k) * 3^(n-3*k). - _Paul Barry_, Oct 20 2004"
			],
			"maple": [
				"spec := [S,{S=Sequence(Union(Z,Z,Z,Prod(Z,Z,Z)))},unlabeled]: seq(combstruct[count](spec,size=n), n=0..30);"
			],
			"mathematica": [
				"CoefficientList[Series[x/(1-3*x-x^3), {x, 0, 30}], x] (* _Zerinvary Lajos_, Mar 29 2007 *)",
				"LinearRecurrence[{3,0,1},{1,3,9},30] (* _Vladimir Joseph Stephan Orlovsky_, Jan 28 2012 *)"
			],
			"program": [
				"(PARI) my(x='x+O('x^30)); Vec(x/(1-3*x-x^3)) \\\\ _G. C. Greubel_, May 09 2019",
				"(MAGMA) R\u003cx\u003e:=PowerSeriesRing(Integers(), 30); Coefficients(R!( x/(1-3*x-x^3) )); // _G. C. Greubel_, May 09 2019",
				"(Sage) (x/(1-3*x-x^3)).series(x, 30).coefficients(x, sparse=False) # _G. C. Greubel_, May 09 2019",
				"(GAP) a:=[1,3,9];; for n in [4..30] do a[n]:=3*a[n-1]+a[n-3]; od; a; # _G. C. Greubel_, May 09 2019"
			],
			"xref": [
				"Cf. A076264."
			],
			"keyword": "easy,nonn",
			"offset": "0,2",
			"author": "encyclopedia(AT)pommard.inria.fr, Jan 25 2000",
			"ext": [
				"More terms from _James A. Sellers_, Jun 06 2000"
			],
			"references": 7,
			"revision": 40,
			"time": "2020-07-03T14:55:05-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}