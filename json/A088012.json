{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A088012",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 88012,
			"data": "1155,8925,32445,442365,159030135,815634435,2586415095",
			"name": "Odd solutions to abs(sigma(x)-2x) \u003c= log(x). Numbers n whose abundance-radius does not exceed log(n).",
			"comment": [
				"This sequence should include odd perfect numbers too, if they exist.",
				"There are no other terms below 2*10^9.",
				"     Abundancy(n)             n            2n       sigma(n)  abundance",
				"   1.99480519480519         1155          2310          2304      -6",
				"   2.00067226890756         8925         17850         17856       6",
				"   2.00018492834027        32445         64890         64896       6",
				"   2.00001356346004       442365        884730        884736       6",
				"   2.00000011318610    159030135     318060270     318060288      18",
				"   1.99999999264376    815634435    1631268870    1631268864      -6",
				"   2.00000000695943   2586415095    5172830190    5172830208      18",
				"As it happens, abundance of these is -6, 6 or 18. This is not necessarily true for larger terms.",
				"a(8) \u003e 10^13. - _Giovanni Resta_, Mar 31 2013",
				"See also A171929 and A188597 and A188263 for sequences of numbers (any / deficient / abundant) whose relative abundancy tends to 2. - _M. F. Hasler_, Feb 19 2017",
				"From _Alexander Violette_, Nov 05 2020: (Start)",
				"a(8) \u003c= 221753180448460815.",
				"a(9) \u003c= 3278298202600507814120339275775985.",
				"221753180448460815 and 3278298202600507814120339275775985 are also terms of this sequence and their abundancies are -30 and 30 respectively. In fact, 3278298202600507814120339275775985 and 815634435 are the only odd terms known where abs(sigma(x)-2x) \u003c= log_10(x). (End)"
			],
			"example": [
				"1155 is in the sequence because sigma(1155) = 2304, giving 2*1155 - 2304 = 6, while natural log of 1155 is about 7.05.",
				"From _M. F. Hasler_, Jul 18 2016: (Start)",
				"We have the following factorizations:",
				"1155 = 3 * 5 * 7 * 11,",
				"8925 = 3 * 5^2 * 7 * 17,",
				"32445 = 3^2 * 5 * 7 * 103,",
				"442365 = 3 * 5 * 7 * 11 * 383,",
				"159030135 = 3^5 * 5 * 11 * 73 * 163,",
				"815634435 = 3 * 5 * 7 * 11 * 547 * 1291,",
				"2586415095 = 3^2 * 5 * 11 * 31 * 41 * 4111.",
				"The sequence appears to be a subsequence of A171929. (End)"
			],
			"mathematica": [
				"abu[x_] := Abs[DivisorSigma[1, x]-2*x] Do[If[ !Greater[abu[n], Log[n]//N]\u0026\u0026OddQ[n], Print[n]], {n, 1, 100000}]"
			],
			"program": [
				"(PARI) is(n)=n%2 \u0026\u0026 abs(sigma(n)-2*n)\u003c=log(n) \\\\ _Charles R Greathouse IV_, Feb 21 2017"
			],
			"xref": [
				"Cf. A088007-A088011, A077374, A005100, A005101, A000079, A087167, A000396."
			],
			"keyword": "hard,nonn,more",
			"offset": "1,1",
			"author": "_Labos Elemer_ and _Farideh Firoozbakht_, Oct 20 2003",
			"ext": [
				"Additional comments from _Walter Nissen_, Dec 15 2005",
				"a(7) from _Donovan Johnson_, Dec 21 2008"
			],
			"references": 17,
			"revision": 47,
			"time": "2020-11-05T22:58:41-05:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}