{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A039963",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 39963,
			"data": "1,1,0,0,1,1,1,1,1,1,0,0,1,1,0,0,1,1,0,0,1,1,1,1,1,1,0,0,1,1,1,1,1,1,0,0,1,1,1,1,1,1,0,0,1,1,0,0,1,1,0,0,1,1,1,1,1,1,0,0,1,1,0,0,1,1,0,0,1,1,1,1,1,1,0,0,1,1,0,0,1,1,0,0,1,1,1,1,1,1,0,0,1,1,1,1,1,1,0,0,1,1,1,1,1",
			"name": "The period-doubling sequence A035263 repeated.",
			"comment": [
				"An example of a d-perfect sequence.",
				"Motzkin numbers mod 2. - _Benoit Cloitre_, Mar 23 2004",
				"Let {a, b, c, c, a, b, a, b, a, b, c, c, a, b, ...} be the fixed point of the morphism: a -\u003e ab, b -\u003e cc, c -\u003e ab, starting from a; then the sequence is obtained by taking a = 1, b = 1, c = 0. - _Philippe Deléham_, Mar 28 2004",
				"The asymptotic mean of this sequence is 2/3 (Rowland and Yassawi, 2015; Burns, 2016). - _Amiram Eldar_, Jan 30 2021"
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A039963/b039963.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Rob Burns, \u003ca href=\"https://arxiv.org/abs/1611.04910\"\u003eAsymptotic density of Motzkin numbers modulo small primes\u003c/a\u003e, arXiv:1611.04910 [math.NT], 2016.",
				"David Kohel, San Ling and Chaoping Xing, \u003ca href=\"https://doi.org/10.1007/978-1-4471-0551-0_23\"\u003eExplicit Sequence Expansions\u003c/a\u003e, in: C. Ding, T. Helleseth and H. Niederreiter (eds.), Sequences and their Applications, Proceedings of SETA'98 (Singapore, 1998), Discrete Mathematics and Theoretical Computer Science, 1999, pp. 308-317; \u003ca href=\"http://www.maths.usyd.edu.au/u/kohel/doc/perfect.ps\"\u003ealternative link\u003c/a\u003e.",
				"Eric Rowland and Reem Yassawi, \u003ca href=\"http://www.numdam.org/item/JTNB_2015__27_1_245_0/\"\u003eAutomatic congruences for diagonals of rational functions\u003c/a\u003e, Journal de Théorie des Nombres de Bordeaux, Vol. 27, No. 1 (2015), pp. 245-288; \u003ca href=\"http://arxiv.org/abs/1310.8635\"\u003earXiv preprint\u003c/a\u003e, arXiv:1310.8635 [math.NT], 2013-2014.",
				"T. Amdeberhan and M. Alekseyev, \u003ca href=\"https://mathoverflow.net/q/406773\"\u003eA moment sequence and Motzkin numbers. Modular coincidence?\u003c/a\u003e, MathOverflow, 2021."
			],
			"formula": [
				"a(n) = A035263(1+floor(n/2)). - _Benoit Cloitre_, Mar 23 2004",
				"a(n) = A040039(n) mod 2 = A002212(n+1) mod 2. a(0) = a(1) = 1, for n\u003e=2: a(n) = ( a(n) + Sum_{k=0..n-2} a(k)*a(n-2-k)) mod 2. - _Philippe Deléham_, Mar 26 2004",
				"a(n) = (A(n+2) - A(n)) mod 2, for A = A019300, A001285, A010060, A010059, A000069, A001969. - _Philippe Deléham_, Mar 28 2004",
				"a(n) = A001006(n) mod 2. - _Christian G. Bower_, Jun 12 2005",
				"a(n) = (-1)^n*(A096268(n+1) - A096268(n)). - _Johannes W. Meijer_, Feb 02 2013",
				"a(n) = 1 - A007814(floor(n/2)+1) mod 2 = A005802(n) mod 2. - _Max Alekseyev_, Oct 23 2021"
			],
			"mathematica": [
				"Flatten[ Nest[ Function[l, {Flatten[(l /. {a -\u003e {a, b}, b -\u003e {c, c}, c -\u003e {a, b}})]}], {a}, 7] /. {a -\u003e {1}, b -\u003e {1}, c -\u003e {0}}] (* _Robert G. Wilson v_, Feb 26 2005 *)"
			],
			"program": [
				"(PARI) A039963(n) = 1 - valuation(n\\2+1,2)%2; \\\\ _Max Alekseyev_, Oct 23 2021"
			],
			"xref": [
				"Cf. A005802, A081706.",
				"Motzkin numbers A001006 read mod 2,3,4,5,6,7,8,11: A039963, A039964, A299919, A258712, A299920, A258711, A299918, A258710."
			],
			"keyword": "nonn",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Christian G. Bower_, Jun 12 2005",
				"Edited by _N. J. A. Sloane_ at the suggestion of _Andrew S. Plewe_ and _Ralf Stephan_, Jul 13 2007"
			],
			"references": 11,
			"revision": 46,
			"time": "2021-10-23T13:25:00-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}