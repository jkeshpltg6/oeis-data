{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A258087",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 258087,
			"data": "0,0,1,3,6,9,14,18,25,30,39,45,56,63,76,84,99,108,125,135,154,165,186,198,221,234,259,273,300,315,344,360,391,408,441,459,494,513,550,570,609,630,671,693,736,759,804,828,875,900,949,975,1026,1053,1106,1134",
			"name": "Start with all terms set to 0. Then add n to the next n+2 terms for n=0,1,2,... .",
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A258087/b258087.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,2,-2,-1,1)."
			],
			"formula": [
				"a(n) = (6*n^2+2*n-11+(2*n-5)*(-1)^n)/16+0^n.",
				"a(n) = Sum_{i=1..n-1} (3*i+2)/4+(2-i)*(-1)^i/4.",
				"From _Robert Israel_, May 19 2015: (Start)",
				"G.f.: x^2*(x^3-x^2-2*x-1)/((x+1)^2*(x-1)^3).",
				"E.g.f.: 1 + exp(x)*(6*x^2+8*x-11)/16 - exp(-x)*(2*x+5)/16.",
				"a(n) = a(n-1)+2*a(n-2)-2*a(n-3)-a(n-4)+a(n-5) for n \u003e= 6. (End)",
				"From _Bruno Berselli_, May 20 2015: (Start)",
				"a(n) = a(-n) for n odd, a(n) = a(-n)+n/2 otherwise.",
				"a(n) = (floor(n/2)+1)*(floor(n/2)+2*floor((n-1)/2))/2 for n\u003e0. Therefore, after 3, all terms of the sequence are composite. (End)",
				"a(n) = Sum_{i=floor((n-1)/2)..n-1} i, for n\u003e0. - _Wesley Ivan Hurt_, Apr 11 2016"
			],
			"example": [
				"n  | 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10, ...",
				"__________________________________________",
				"     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ...",
				"+       0, 0",
				"+          1, 1, 1",
				"+             2, 2, 2, 2",
				"+                3, 3, 3, 3, 3",
				"+                   4, 4, 4, 4, 4, 4",
				"+                      5, 5, 5, 5, 5, 5, 5",
				"+                         6, 6, 6, 6, 6, 6, 6, 6",
				"+                            7, 7, 7, 7, 7, 7, 7, 7, 7, 7",
				"+                               8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8",
				"+                                  9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9",
				"+                                      ...",
				"__________________________________________",
				"a(n)|0, 0, 1, 3, 6, 9,14,18,25,30,39,  ..."
			],
			"maple": [
				"A258087:=n-\u003e(6*n^2+2*n-11+(2*n-5)*(-1)^n)/16+0^n: seq(A258087(n), n=0..100);"
			],
			"mathematica": [
				"Join[{0}, Table[(6 n^2 + 2 n - 11 + (2 n - 5) (-1)^n)/16, {n, 100}]]",
				"Table[Total@ Range[Floor[(n - 1)/2], n - 1], {n, 55}] (* _Michael De Vlieger_, Apr 11 2016 *)"
			],
			"program": [
				"(MAGMA) [(6*n^2+2*n-11+(2*n-5)*(-1)^n)/16+0^n: n in [0..60]]; // _Vincenzo Librandi_, May 20 2015",
				"(Sage) [(6*n^2+2*n-11+(2*n-5)*(-1)^n)/16+0^n for n in (0..60)] # _Bruno Berselli_, May 20 2015",
				"(Haskell)",
				"a258087 n = a258087_list !! n",
				"a258087_list = f 0 [0] $",
				"   map (\\i -\u003e take (i + 1) (repeat 0) ++ replicate (i + 2) i) [0..] where",
				"   f i ys@(y:_) (xs:xss) = (ys !! i) :",
				"                           f (i + 1) (zipWith (+) (ys ++ repeat 0) xs) xss",
				"-- _Reinhard Zumkeller_, May 21 2015",
				"(PARI) a(n) = if (n==0, 0, sum(k = (n-1)\\2, n-1, k)); \\\\ _Michel Marcus_, Apr 11 2016",
				"(PARI) x='x+O('x^99); concat([0, 0], Vec(x^2*(x^3-x^2-2*x-1)/((x+1)^2*(x-1)^3))) \\\\ _Altug Alkan_, Apr 11 2016"
			],
			"xref": [
				"Cf. A272058."
			],
			"keyword": "nonn,easy",
			"offset": "0,4",
			"author": "_Wesley Ivan Hurt_, May 19 2015",
			"references": 4,
			"revision": 50,
			"time": "2017-05-06T03:34:05-04:00",
			"created": "2015-05-21T04:11:58-04:00"
		}
	]
}