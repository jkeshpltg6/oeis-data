{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A233583",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 233583,
			"data": "2,2,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56",
			"name": "Coefficients of the generalized continued fraction expansion e = a(1) +a(1)/(a(2) +a(2)/(a(3) +a(3)/(a(4) +a(4)/....))).",
			"comment": [
				"For more details on Blazys' expansion, see A233582.",
				"This sequence matches that of natural numbers (A000027), offset by 1, with two different starting terms."
			],
			"link": [
				"Robert G. Wilson v, \u003ca href=\"/A233583/b233583.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Augustine O. Munagi, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL21/Munagi/munagi10.html\"\u003eInteger Compositions and Higher-Order Conjugation\u003c/a\u003e, J. Int. Seq., Vol. 21 (2018), Article 18.8.5.",
				"Franck Ramaharo, \u003ca href=\"https://arxiv.org/abs/1902.08989\"\u003eA generating polynomial for the two-bridge knot with Conway's notation C(n,r)\u003c/a\u003e, arXiv:1902.08989 [math.CO], 2019.",
				"S. Sykora, \u003ca href=\"http://dx.doi.org/10.3247/sl4math13.001\"\u003eBlazys' Expansions and Continued Fractions\u003c/a\u003e, Stans Library, Vol.IV, 2013, DOI 10.3247/sl4math13.001",
				"S. Sykora, \u003ca href=\"http://oeis.org/wiki/File:BlazysExpansions.txt\"\u003ePARI/GP scripts for Blazys expansions and fractions\u003c/a\u003e, OEIS Wiki"
			],
			"formula": [
				"e = 2+2/(2+2/(2+2/(3+3/(4+4/(5+...)))))."
			],
			"mathematica": [
				"BlazysExpansion[n_, mx_] := Block[{k = 1, x = n, lmt = mx + 1, s, lst = {}}, While[k \u003c lmt, s = Floor[x]; x = 1/(x/s - 1); AppendTo[lst, s]; k++]; lst]; BlazysExpansion[E, 80] (* _Robert G. Wilson v_, May 22 2014 *)"
			],
			"program": [
				"(PARI) bx(x,nmax)={local(c,v,k); // Blazys expansion function",
				"v = vector(nmax);c = x;for(k=1,nmax,v[k] = floor(c);c = v[k]/(c-v[k]););return (v);}",
				"bx(exp(1),100) // Execution; use high real precision"
			],
			"xref": [
				"Cf. A000027 (natural numbers), A001113 (number e).",
				"Cf. Blazys' expansions: A233582 (Pi), A233584, A233585, A233586, A233587 and Blazys' continued fractions: A233588, A233589, A233590, A233591."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_Stanislav Sykora_, Jan 06 2014",
			"references": 12,
			"revision": 31,
			"time": "2019-04-10T21:53:59-04:00",
			"created": "2014-01-18T13:53:47-05:00"
		}
	]
}