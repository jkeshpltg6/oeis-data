{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A118383",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 118383,
			"data": "1,1,1,1,2,1,1,1,1,1,2,2,1,2,1,2,1,2,3,1,1,1,1,1,1,1,1,1,1,2,3,3,1,2,2,1,2,2,1,2,1,2,4,1,1,2,1,2,1,2,1,2,1,1,1,3,2,2,3,1,2,3,1,1,2,3,1,1,2,3,4,3,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,4,1,4,2,1,2,3,3,1,2,3",
			"name": "Unrefined Orloj clock sequences; row n sums to n.",
			"comment": [
				"An Orloj clock sequence is a finite sequence of positive integers that, when iterated, can be grouped so that the groups sum to successive natural numbers. There is one unrefined sequence whose values sum to each n; all other Orloj clock sequences summing to n can be obtained by refining this one. Refining means splitting one or more terms into values summing to that term. (The unrefined sequence for n = 2^k*(2m-1) is the sequence for 2m-1 repeated 2^k times, but any single refinement - possible unless m = 1 - will produce an aperiodic sequence summing to n.) The Orloj clock sequence is the one summing to 15: 1,2,3,4,3,2, with a beautiful up and down pattern."
			],
			"link": [
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Prague_astronomical_clock\"\u003ePrague astronomical clock\u003c/a\u003e"
			],
			"formula": [
				"Let b(i),0\u003c=i\u003ck be all the residues of triangular numbers modulo n in order, with b(k)=n. The differences b(i+1)-b(i) are the sequence for n. The sequence for 2n is the sequence for n repeated."
			],
			"example": [
				"For a sum of 5, we have 1,2,2, which groups as 1, 2, 2+1, 2+2, 1+2+2, 1+2+2+1, .... This could be refined by splitting the second 2, to give the sequence 1,2,1,1; note that when this is grouped, the two 1's from the refinement always wind up in the same sum.",
				"The array starts:",
				"1;",
				"1, 1;",
				"1, 2;",
				"1, 1, 1, 1;",
				"1, 2, 2;",
				"1, 2, 1, 2;",
				"1, 2, 3, 1."
			],
			"program": [
				"(PARI) {Orloj(n) = my(found,tri,i,last,r); found = vector(n,i,0); found[n] = 1; tri = 0; for(i = 1, if(n%2==0,n-1,n\\2), tri += i; if(tri \u003e n, tri -= n); found[tri] = 1); last = 0; r = []; for(i = 1, n, if(found[i], r = concat(r, [i-last]); last = i)); r}",
				"for (n=1,10,print(Orloj(n)))"
			],
			"xref": [
				"Cf. A028355, A118382.",
				"Length of row n is A117484(n)."
			],
			"keyword": "nonn,tabf",
			"offset": "1,5",
			"author": "_Franklin T. Adams-Watters_, Apr 26 2006",
			"references": 4,
			"revision": 8,
			"time": "2018-02-03T09:14:32-05:00",
			"created": "2006-05-19T03:00:00-04:00"
		}
	]
}