{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A304584",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 304584,
			"data": "0,1,2,2,5,10,3,9,17,27,4,14,26,40,56,5,20,37,56,77,100,6,27,50,75,102,131,162,7,35,65,97,131,167,205,245,8,44,82,122,164,208,254,302,352,9,54,101,150,201,254,309,366,425,486,10,65,122,181,242,305,370,437,506,577,650,11",
			"name": "A linear mapping a(n) = x + d*n of pairs of nonnegative integers (x,d), where the pairs are enumerated by antidiagonals.",
			"comment": [
				"The sequence solves the following riddle, which has been communicated by Klaus Nagel: A flea starts to jump on the nonnegative integers at time = 0 at an unknown location x \u003e= 0 making jumps of unknown, but constant distance d \u003e= 0 at every subsequent time step. By which strategy can the flea be captured with 100% certainty in a finite number of trials? The solution is to hit a(n) at time = n. This works for all enumerations of pairs (x,d) of integers, because eventually any combination of starting location x and jump width d will be addressed."
			],
			"link": [
				"Rainer Rosenthal, \u003ca href=\"/A304584/b304584.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e"
			],
			"example": [
				"  d:",
				"  5 |  20",
				"  4 |  14  19",
				"  3 |   9  13  18",
				"  2 |   5   8  12  17",
				"  1 |   2   4   7  11  16",
				"  0 |   0   1   3   6  10  15",
				"    |________________________",
				"  x:    0   1   2   3   4   5",
				".",
				"a(13) = 1 + 13*3 = 40 because the 13th position in the enumeration corresponds to x=1 and d=3."
			],
			"maple": [
				"pos2pair:=proc(n) local w,k,e;w:=floor(sqrt(2*n));if w*(w+1)\u003e2*n then k:=w-1;else k:=w;fi;e:=n-k*(k+1)/2;return [k-e,e];end:WhereFlea:=proc(n) local x,d,pair; pair:=pos2pair(n);x:=pair[1];d:=pair[2];return x+d*n;end:",
				"seq(WhereFlea(n),n=0..66);# _Rainer Rosenthal_, May 23 2018"
			],
			"xref": [
				"Cf. A304585, A304586, A304587, A305260."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Hugo Pfoertner_, May 15 2018",
			"references": 5,
			"revision": 22,
			"time": "2018-08-12T17:28:33-04:00",
			"created": "2018-05-20T18:29:22-04:00"
		}
	]
}