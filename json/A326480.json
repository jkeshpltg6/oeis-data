{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A326480",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 326480,
			"data": "1,-2,2,2,-8,4,4,12,-24,8,-16,32,48,-64,16,-32,-160,160,160,-160,32,272,-384,-960,640,480,-384,64,544,3808,-2688,-4480,2240,1344,-896,128,-7936,8704,30464,-14336,-17920,7168,3584,-2048,256",
			"name": "T(n, k) = 2^n * n! * [x^k] [z^n] (4*exp(x*z))/(exp(z) + 1)^2, triangle read by rows, for 0 \u003c= k \u003c= n. Coefficients of Euler polynomials of order 2.",
			"comment": [
				"T(m, n, k) = 2^n * n! * [x^k] [z^n] (2^m*exp(x*z))/(exp(z) + 1)^m are the coefficients of the generalized Euler polynomials (or Euler polynomials of higher order).",
				"The classical case (m=1) is in A004174, this sequence is case m=2. A different normalization for m=1 is given in A058940 and for m=2 in A326485.",
				"Generalized Euler numbers are 2^n*Sum_{k=0..n} T(m, n, k)*(1/2)^k. The classical Euler numbers are in A122045 and for m=2 in A326483."
			],
			"link": [
				"NIST Digital Library of Mathematical Functions, §24.16(i), \u003ca href=\"https://dlmf.nist.gov/24.16#i\"\u003eHigher-Order Analogs (of Bernoulli and Euler Polynomials)\u003c/a\u003e, Release 1.0.23 of 2019-06-15."
			],
			"example": [
				"Triangle starts:",
				"[0] [     1]",
				"[1] [    -2,       2]",
				"[2] [     2,      -8,     4]",
				"[3] [     4,      12,   -24,      8]",
				"[4] [   -16,      32,    48,    -64,     16]",
				"[5] [   -32,    -160,   160,    160,   -160,     32]",
				"[6] [   272,    -384,  -960,    640,    480,   -384,    64]",
				"[7] [   544,    3808, -2688,  -4480,   2240,   1344,  -896,   128]",
				"[8] [ -7936,    8704, 30464, -14336, -17920,   7168,  3584, -2048,   256]",
				"[9] [-15872, -142848, 78336, 182784, -64512, -64512, 21504,  9216, -4608, 512]"
			],
			"maple": [
				"E2 := proc(n) (4*exp(x*z))/(exp(z) + 1)^2;",
				"series(%, z, 48); 2^n*n!*coeff(%, z, n) end:",
				"ListTools:-Flatten([seq(PolynomialTools:-CoefficientList(E2(n), x), n=0..9)]);"
			],
			"mathematica": [
				"T[n_, k_] := 2^n n! SeriesCoefficient[4 Exp[x z]/(Exp[z]+1)^2, {z, 0, n}, {x, 0, k}];",
				"Table[T[n, k], {n, 0, 8}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Jul 15 2019 *)"
			],
			"xref": [
				"Let E2_{n}(x) = Sum_{k=0..n} T(n,k) x^k. Then E2_{n}(1) = A155585(n+1),",
				"E2_{n}(0) = A326481(n), E2_{n}(-1) = A326482(n), 2^n*E2_{n}(1/2) = A326483(n),",
				"2^n*E2_{n}(-1/2) = A326484(n), [x^n] E2_{n}(x) = A000079(n).",
				"Cf. A004174, A058940, A326485, A122045, A081733."
			],
			"keyword": "sign,tabl",
			"offset": "0,2",
			"author": "_Peter Luschny_, Jul 11 2019",
			"references": 6,
			"revision": 21,
			"time": "2019-07-15T02:44:33-04:00",
			"created": "2019-07-12T21:09:34-04:00"
		}
	]
}