{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A124765",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 124765,
			"data": "0,1,1,1,1,1,2,1,1,1,1,1,2,2,2,1,1,1,1,1,2,1,2,1,2,2,2,2,2,2,2,1,1,1,1,1,1,1,2,1,2,2,1,1,2,2,2,1,2,2,2,2,3,2,3,2,2,2,2,2,2,2,2,1,1,1,1,1,1,1,2,1,2,1,1,1,2,2,2,1,2,2,2,2,2,1,2,1,2,2,2,2,2,2,2,1,2,2,2,2,2,2,3,2,3",
			"name": "Number of monotonically decreasing runs for compositions in standard order.",
			"comment": [
				"The standard order of compositions is given by A066099.",
				"A composition of n is a finite sequence of positive integers summing to n. The k-th composition in standard order (row k of A066099) is obtained by taking the set of positions of 1's in the reversed binary expansion of k, prepending 0, taking first differences, and reversing again. a(n) is the number of maximal weakly decreasing runs in this composition. Alternatively, a(n) is one plus the number of strict ascents in the same composition. For example, the weakly decreasing runs of the 1234567th composition are ((3,2,1),(2,2,1),(2),(5,1,1,1)), so a(1234567) = 4. The 3 strict ascents together with the weak descents are: 3 \u003e= 2 \u003e= 1 \u003c 2 \u003e= 2 \u003e= 1 \u003c 2 \u003c 5 \u003e= 1 \u003e= 1 \u003e= 1. - _Gus Wiseman_, Apr 08 2020"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A124765/b124765.txt\"\u003eTable of n, a(n) for n = 0..16383\u003c/a\u003e"
			],
			"formula": [
				"a(0) = 0, a(n) = A124760(n) + 1 for n \u003e 0."
			],
			"example": [
				"Composition number 11 is 2,1,1; the decreasing runs are 2,1,1; so a(11) = 1.",
				"The table starts:",
				"  0",
				"  1",
				"  1 1",
				"  1 1 2 1",
				"  1 1 1 1 2 2 2 1",
				"  1 1 1 1 2 1 2 1 2 2 2 2 2 2 2 1",
				"  1 1 1 1 1 1 2 1 2 2 1 1 2 2 2 1 2 2 2 2 3 2 3 2 2 2 2 2 2 2 2 1"
			],
			"mathematica": [
				"stc[n_]:=Differences[Prepend[Join@@Position[Reverse[IntegerDigits[n,2]],1],0]]//Reverse;",
				"Table[Length[Split[stc[n],GreaterEqual]],{n,0,100}] (* _Gus Wiseman_, Apr 08 2020 *)"
			],
			"xref": [
				"Cf. A066099, A124760, A011782 (row lengths).",
				"Compositions of n with k strict ascents are A238343.",
				"All of the following pertain to compositions in standard order (A066099):",
				"- Length is A000120.",
				"- Sum is A070939.",
				"- Weakly decreasing compositions are A114994.",
				"- Weakly decreasing runs are counted by A124765.",
				"- Weakly increasing runs are counted by A124766.",
				"- Equal runs are counted by A124767.",
				"- Strictly increasing runs are counted by A124768.",
				"- Strictly decreasing runs are counted by A124769.",
				"- Weakly increasing compositions are A225620.",
				"- Reverse is A228351 (triangle).",
				"- Strict compositions are A233564.",
				"- Constant compositions are A272919.",
				"- Normal compositions are A333217.",
				"- Strictly decreasing compositions are A333255.",
				"- Strictly increasing compositions are A333256.",
				"- Anti-runs are counted by A333381.",
				"- Adjacent unequal pairs are counted by A333382.",
				"- Anti-runs are A333489.",
				"- Runs-resistance is A333628.",
				"Cf. A124761, A124763, A124764, A233249, A333213, A333380."
			],
			"keyword": "easy,nonn,tabf",
			"offset": "0,7",
			"author": "_Franklin T. Adams-Watters_, Nov 06 2006",
			"references": 22,
			"revision": 10,
			"time": "2020-04-09T00:55:27-04:00",
			"created": "2006-12-06T03:00:00-05:00"
		}
	]
}