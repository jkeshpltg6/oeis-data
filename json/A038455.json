{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A038455",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 38455,
			"data": "1,3,1,20,9,1,210,107,18,1,3024,1650,335,30,1,55440,31594,7155,805,45,1,1235520,725592,176554,22785,1645,63,1,32432400,19471500,4985316,705649,59640,3010,84,1,980179200,598482000,159168428,24083892,2267769",
			"name": "A Jabotinsky-triangle related to A006963.",
			"comment": [
				"i) This triangle gives the nonvanishing entries of the Jabotinsky matrix for F(z)= c(z) with c(z) the g.f. for the Catalan numbers A000108. (Notation of F(z) as in Knuth's paper).",
				"ii) E(n,x) := sum(a(n,m)*x^m,m=1..n), E(0,x)=1, are exponential convolution polynomials: E(n,x+y) = sum(binomial(n,k)*E(k,x)*E(n-k,y),k=0..n) (cf. Knuth's paper with E(n,x)= n!*F(n,x).)",
				"iii) Explicit formula: see Knuth's paper for f(n,m) formula with f(k)= A006963(k+1).",
				"Bell polynomial of second kind for log(A000108(x). - _Vladimir Kruchinin_, Mar 26 2013",
				"Also the Bell transform of A006963(n+2). For the definition of the Bell transform see A264428. - _Peter Luschny_, Jan 28 2016"
			],
			"link": [
				"D. E. Knuth, \u003ca href=\"http://arxiv.org/abs/math/9207221\"\u003eConvolution polynomials\u003c/a\u003e, Mathematica J. 2.1 (1992), no. 4, 67-78.",
				"J.-C. Novelli and J.-Y. Thibon, \u003ca href=\"https://arxiv.org/abs/math/0512570\"\u003eNoncommutative Symmetric Functions and Lagrange Inversion\u003c/a\u003e, arXiv:math/0512570 [math.CO], 2005-2006."
			],
			"formula": [
				"a(n, 1) = A006963(n+1)=(2*n-1)!/n!, n \u003e= 1; a(n, m) = sum(binomial(n-1, j-1)*A006963(j+1)*a(n-j, m-1), j=1..n-m+1), n \u003e= m \u003e= 2.",
				"E.g.f.: ((1-sqrt(1-4*x))/x/2)^y. - _Vladeta Jovovic_, May 02 2003",
				"a(n,m) = (n-1)!*(sum_{k=m..n} stirling1(k,m)*binomial(2*n,n-k)/(k-1)!). - _Vladimir Kruchinin_, Mar 26 2013"
			],
			"maple": [
				"# The function BellMatrix is defined in A264428.",
				"# Adds (1,0,0,0, ..) as column 0.",
				"BellMatrix(n -\u003e (2*n+1)!/(n+1)!, 9); # _Peter Luschny_, Jan 28 2016"
			],
			"mathematica": [
				"BellMatrix[f_Function, len_] := With[{t = Array[f, len, 0]}, Table[BellY[n, k, t], {n, 0, len - 1}, {k, 0, len - 1}]];",
				"rows = 11;",
				"M = BellMatrix[(2#+1)!/(#+1)!\u0026, rows];",
				"Table[M[[n, k]], {n, 2, rows}, {k, 2, n}] // Flatten (* _Jean-François Alcover_, Jun 24 2018, after _Peter Luschny_ *)"
			],
			"program": [
				"(Maxima)",
				"a(n,m):=(n-1)!*(sum((stirling1(k,m)*binomial(2*n,n-k))/(k-1)!,k,m,n)); /* _Vladimir Kruchinin_, Mar 26 2013 */"
			],
			"xref": [
				"Cf. A006963, A000108, A001761, A039619, A039646."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Wolfdieter Lang_",
			"references": 5,
			"revision": 28,
			"time": "2018-06-24T06:39:13-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}