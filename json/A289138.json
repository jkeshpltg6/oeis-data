{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A289138",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 289138,
			"data": "1,36,7,6,5,6,3,56,9,0,1,16,7,6,5,6,3,76,9,0,1,96,7,6,5,6,3,96,9,0,1,76,7,6,5,6,3,16,9,0,1,56,7,6,5,6,3,36,9,0,1,36,7,6,5,6,3,56,9,0,1,16,7,6,5,6,3,76,9,0,1,96,7,6,5,6,3,96,9,0,1,76,7,6,5,6,3,16,9,0,1,56,7,6,5,6,3,36,9,0",
			"name": "a(n) = smallest expomorphic number in base n: least integer k such that n^k ends in k, or 0 if no such k exists.",
			"comment": [
				"Definition: For positive integers b (the base) and n, the positive integer (allowing initial zeros) a(n) is expomorphic relative to base b if a(n) has exactly n decimal digits and if b^a(n) == a(n) (mod 10^n) or, equivalently, b^a(n) ends in a(n). [See Crux Mathematicorum link.]",
				"The only twelve values a(n) can take are 0, 1, 3, 5, 6, 7, 9, 16, 36, 56, 76 and 96;",
				"and the percentages of the time these occur are 10, 10, 10, 10, 20, 10, 10, 4, 4, 4, 4 and 4, respectively.",
				"The bases, n, for which k is:",
				"0: n == 0 (mod 10)",
				"1: n == 1 (mod 10)",
				"3: n == 7 (mod 10)",
				"5: n == 5 (mod 10)",
				"6: n == 4 or 6 (mod 10)",
				"7: n == 3 (mod 10)",
				"9: n == 9 (mod 10)",
				"16: n == +/- 12 (mod 50)",
				"36: n == +/-  2 (mod 50)",
				"56: n == +/-  8 (mod 50)",
				"76: n == +/- 18 (mod 50)",
				"96: n == +/- 22 (mod 50).",
				"Periodicity is 50."
			],
			"link": [
				"Charles W. Trigg, \u003ca href=\"https://cms.math.ca/crux/backfile/Crux_v7n06_Jun.pdf\"\u003e Problem 559\u003c/a\u003e, Crux Mathematicorum, page 192, Vol. 7, Jun. 1981.",
				"\u003ca href=\"/index/Rec#order_50\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1)."
			],
			"example": [
				"a(4) is 6 since 4^6 = 4096 which ends in 6."
			],
			"maple": [
				"f:= proc(n) local k;",
				"   if n mod 10 = 0 then return 0 fi;",
				"   for k from 1 do if n^k - k mod 10^(1+ilog10(k)) = 0 then return k fi od",
				"end proc:",
				"map(f, [$1..100]); # _Robert Israel_, Jul 07 2017"
			],
			"mathematica": [
				"f[n_] := If[ Mod[n, 10] \u003e 0, Block[{k = 1}, While[ PowerMod[n, k, 10^IntegerLength[k]] != k, k++]; k], 0]; Array[f, 88]"
			],
			"program": [
				"(Python)",
				"def a(n):",
				"    if n%10==0: return 0",
				"    k=1",
				"    while pow(n, k, 10**len(str(k)))!=k: k+=1",
				"    return k",
				"print([a(n) for n in range(1, 101)]) # _Indranil Ghosh_, Jun 29 2017"
			],
			"xref": [
				"Cf. A288845."
			],
			"keyword": "easy,nonn,base",
			"offset": "1,2",
			"author": "_Bernard Schott_ and _Robert G. Wilson v_, Jun 28 2017",
			"references": 3,
			"revision": 50,
			"time": "2021-05-08T23:06:36-04:00",
			"created": "2017-06-29T01:46:20-04:00"
		}
	]
}