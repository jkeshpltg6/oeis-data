{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A180002",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 180002,
			"data": "4,8,24,43,179,783,1504,6668,29604,56983,253079,1124043,2163724,9610208,42683904,82164403,364934699,1620864183,3120083464,13857908228,61550154924,118481007103,526235577839,2337285022803,4499158186324,19983094049528",
			"name": "a(n) red and b(n) blue balls in an urn; draw 5 balls without replacement; Probability(5 red balls) = Probability(3 red and 2 blue balls).",
			"comment": [
				"This is equivalent to the Pell equation A(n)^2 - 10*B(n)^2 = -9 with a(n) = (A(n)+7)/2, b(n) = (B(n)+1)/2, and the 3 fundamental solutions (1,1), (9,3), (41,13), and the solution (19,6) for the unit form."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A180002/b180002.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_07\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,0,38,-38,0,-1,1)."
			],
			"formula": [
				"G.f.: x*(4 +4*x +16*x^2 -133*x^3 -16*x^4 -4*x^5 +3*x^6)/((1-x)*(1 -38*x^3 +x^6)).",
				"a(n+9) = 39*a(n+6) - 39*a(n+3) + a(n).",
				"Let r = sqrt(10) then:",
				"a(3*n+1) = (14 + (1+r)*(19+6*r)^n + (1-r)*(19-6*r)^n)/4.",
				"a(3*n+2) = (14 + 3*(3+r)*(19+6*r)^n + 3*(3-r)*(19-6*r)^n)/4.",
				"a(3*n+3) = (14 + (41+13*r)*(19+6*r)^n + (41-13*r)*(19-6*r)^n)/4.",
				"a(n) = a(n-1) + 38*a(n-3) - 38*a(n-4) - a(n-6) + a(n-7). - _G. C. Greubel_, Mar 20 2019"
			],
			"example": [
				"For n=3: a(3)=24 and b(3)=7 since binomial(24,5) = binomial(24,3)*binomial(7,2) = 42504."
			],
			"mathematica": [
				"Rest[CoefficientList[Series[x*(4+4*x+16*x^2-133*x^3-16*x^4-4*x^5 +3*x^6 )/((1-x)*(1-38*x^3+x^6)), {x,0,30}], x]] (* _G. C. Greubel_, Mar 20 2019 *)"
			],
			"program": [
				"(PARI) my(x='x+O('x^30)); Vec(x*(4+4*x+16*x^2-133*x^3-16*x^4-4*x^5+3*x^6) /((1-x)*(1-38*x^3+x^6))) \\\\ _G. C. Greubel_, Mar 20 2019",
				"(MAGMA) R\u003cx\u003e:=PowerSeriesRing(Integers(), 30); Coefficients(R!( x*(4+4*x+ 16*x^2-133*x^3-16*x^4-4*x^5+3*x^6)/((1-x)*(1-38*x^3+x^6)) )); // _G. C. Greubel_, Mar 20 2019",
				"(Sage) a=(x*(4+4*x+16*x^2-133*x^3-16*x^4-4*x^5+3*x^6)/((1-x)*(1-38*x^3 +x^6))).series(x, 30).coefficients(x, sparse=False); a[1:] # _G. C. Greubel_, Mar 20 2019"
			],
			"xref": [
				"Cf. A180003 (b(n))."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Paul Weisenhorn_, Aug 05 2010",
			"references": 3,
			"revision": 15,
			"time": "2019-04-19T11:09:49-04:00",
			"created": "2010-08-27T03:00:00-04:00"
		}
	]
}