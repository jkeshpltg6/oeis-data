{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A333240",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 333240,
			"data": "1,4,1,4,0,6,4,3,9,0,8,9,2,1,4,7,6,3,7,5,6,5,5,0,1,8,1,9,0,7,9,8,2,9,3,7,9,9,0,7,6,9,5,0,6,9,3,9,3,1,6,2,1,7,5,0,3,9,9,2,4,9,6,2,4,2,3,9,2,8,1,0,6,9,9,2,0,8,8,4,9,9,4,5,3,7,5,4,8,5,8,5,0,2,4,7,5,1,1,4,2,0,0,2",
			"name": "Decimal expansion of Product_{primes p == 2 (mod 3)} 1/(1 - 1/p^2).",
			"comment": [
				"The range of product are the primes of the form 3*k - 1 (A003627).",
				"See a comment of _R. J. Mathar_ in A175646."
			],
			"link": [
				"Peter Luschny, \u003ca href=\"/A333240/b333240.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Thomas Dence and Carl Pomerance, \u003ca href=\"http://dx.doi.org/10.1023/A:1009753405498\"\u003eEuler's Function in Residue Classes\u003c/a\u003e, Raman. J., Vol. 2 (1998) pp. 7-20, c_3 in formula (1.8) and (5.6), \u003ca href=\"https://math.dartmouth.edu/~carlp/PDF/paper116.pdf\"\u003ealternative link\u003c/a\u003e.",
				"R. J. Mathar, \u003ca href=\"https://arxiv.org/abs/1008.2547\"\u003eTable of Dirichlet L-series and prime zeta modulo functions for small moduli\u003c/a\u003e, arXiv:1008.2547 [math.NT], 2010-2015, Zeta_{3,2}(2) in section 3.2."
			],
			"formula": [
				"A333240 * A175646 = (4*Pi^2)/27 = A214549.",
				"A301429 = sqrt(A333240) / 12^(1/4).",
				"Equals Sum_{k\u003e=1} 1/A004612(k)^2. - _Amiram Eldar_, Sep 27 2020"
			],
			"example": [
				"1.414064390892147637565501819079829379907695069393162175039924962423928106992..."
			],
			"maple": [
				"z := n -\u003e Zeta(n)/Im(polylog(n, (-1)^(2/3))):",
				"x := n -\u003e (z(2^n)*(3^(2^n)-1)*sqrt(3)/2)^(1/2^n)/3:",
				"evalf(mul(x(n), n=1..8), 105); # _Peter Luschny_, Jan 17 2021"
			],
			"mathematica": [
				"digits = 104; precision = digits + 10;",
				"prodeuler[p_, a_, b_, expr_] := Product[If[a \u003c= p \u003c= b, expr, 1], {p, Prime[Range[PrimePi[a], PrimePi[b]]]}];",
				"Lv3[s_] := prodeuler[p, 1, 2^(precision/s), 1/(1 - KroneckerSymbol[-3, p]*p^-s)] // N[#, precision]\u0026;",
				"Lv4[s_] := 2*Im[PolyLog[s, Exp[2*I*Pi/3]]]/Sqrt[3];",
				"Lv[s_] := If[s \u003e= 10000, Lv3[s], Lv4[s]];",
				"gv[s_] := (1 - 3^(-s))*Zeta[s]/Lv[s];",
				"pgv = Product[gv[2^n*2]^(2^-(n + 1)), {n, 0, 11}] // N[#, precision]\u0026;",
				"RealDigits[pgv, 10, digits][[1]]",
				"(* _Jean-François Alcover_, Jan 12 2021, after PARI code due to _Artur Jasinski_ *)",
				"z[n_] := Zeta[n]/Im[PolyLog[n, (-1)^(2/3)]];",
				"x[n_] := (z[2^n] (3^(2^n) - 1) Sqrt[3]/2)^(1/2^n)/3;",
				"N[Product[x[n], {n, 8}], 105] (* _Peter Luschny_, Jan 17 2021 *)"
			],
			"xref": [
				"Cf. A003627, A004612, A175646, A214549, A301429, A333239."
			],
			"keyword": "nonn,cons,hard",
			"offset": "1,2",
			"author": "_Peter Luschny_, May 13 2020",
			"ext": [
				"Last 5 digits corrected by _Jean-François Alcover_, Jan 12 2021",
				"Better name by _Peter Luschny_, Jan 17 2021"
			],
			"references": 17,
			"revision": 46,
			"time": "2021-09-24T01:26:45-04:00",
			"created": "2020-05-13T07:33:41-04:00"
		}
	]
}