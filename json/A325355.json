{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A325355",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 325355,
			"data": "1,1,1,1,1,1,1,1,2,1,1,1,1,1,3,1,1,2,1,1,4,1,1,1,2,1,2,1,1,3,1,1,5,1,4,2,1,1,6,1,1,4,1,1,3,1,1,1,2,2,7,1,1,2,3,1,8,1,1,3,1,1,4,1,5,5,1,1,9,4,1,2,1,1,3,1,5,6,1,1,2,1,1,4,4,1,10,1,1,3,5,1,11,1,6,1,1,2,5,2,1,7,1,1,3",
			"name": "One plus the number of steps applying A325351 (Heinz number of augmented differences of reversed prime indices) to reach a fixed point.",
			"comment": [
				"The Heinz number of an integer partition (y_1,...,y_k) is prime(y_1)*...*prime(y_k).",
				"The augmented differences aug(y) of an integer partition y of length k are given by aug(y)_i = y_i - y_{i + 1} + 1 if i \u003c k and aug(y)_k = y_k. For example, aug(6,5,5,3,3,3) = (2,1,3,1,1,3).",
				"The fixed points of A325351 are the Heinz numbers of hooks A093641."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A325355/b325355.txt\"\u003eTable of n, a(n) for n = 1..65537\u003c/a\u003e",
				"\u003ca href=\"/index/Pri#prime_indices\"\u003eIndex entries for sequences computed from indices in prime factorization\u003c/a\u003e",
				"\u003ca href=\"/index/He#Heinz\"\u003eIndex entries for sequences related to Heinz numbers\u003c/a\u003e"
			],
			"example": [
				"Repeatedly applying A325351 starting with 78 gives 78 -\u003e 66 -\u003e 42 -\u003e 30 -\u003e 18 -\u003e 12, and 12 is a fixed point, so a(78) = 6."
			],
			"mathematica": [
				"primeptn[n_]:=If[n==1,{},Reverse[Flatten[Cases[FactorInteger[n],{p_,k_}:\u003eTable[PrimePi[p],{k}]]]]];",
				"aug[y_]:=Table[If[i\u003cLength[y],y[[i]]-y[[i+1]]+1,y[[i]]],{i,Length[y]}];",
				"Table[Length[FixedPointList[Times@@Prime/@aug[primeptn[#]]\u0026,n]]-1,{n,50}]"
			],
			"program": [
				"(PARI)",
				"augdiffs(n) = { my(diffs=List([]), f=factor(n), prevpi, pi=0, i=#f~); while(i, prevpi=pi; pi = primepi(f[i, 1]); if(prevpi, listput(diffs, 1+(prevpi-pi))); if(f[i, 2]\u003e1, f[i, 2]--, i--)); if(pi, listput(diffs,pi)); Vec(diffs); };",
				"A325351(n) = factorback(apply(prime,augdiffs(n)));",
				"A325355(n) = { my(u=A325351(n)); if(u==n,1,1+A325355(u)); }; \\\\ _Antti Karttunen_, Nov 16 2019"
			],
			"xref": [
				"Positions of 2's are A325359.",
				"Cf. A056239, A093641, A112798, A130091, A289509, A325351, A325352, A325366, A325389, A325394, A325395, A325396."
			],
			"keyword": "nonn",
			"offset": "1,9",
			"author": "_Gus Wiseman_, Apr 23 2019",
			"ext": [
				"More terms from _Antti Karttunen_, Nov 16 2019"
			],
			"references": 7,
			"revision": 13,
			"time": "2019-11-16T20:05:26-05:00",
			"created": "2019-04-24T19:48:09-04:00"
		}
	]
}