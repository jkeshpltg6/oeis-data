{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A257540",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 257540,
			"data": "0,1,2,1,1,2,1,2,3,1,1,1,2,2,1,2,2,1,1,2,3,1,3,2,2,1,1,1,1,2,1,2,2,4,1,1,2,2,3,1,2,3,1,1,1,2,2,2,1,3,2,2,2,1,1,3,3,1,2,2,2,1,1,1,1,1,2,2,1,2,2,3,1,1,2,2,4,1,4,2,3,1,1",
			"name": "Irregular triangle read by rows: row n (n\u003e=2) contains the degrees of the level 1 vertices of the rooted tree having Matula-Goebel number n; row 1: 0.",
			"comment": [
				"The Matula (or Matula-Goebel) number of a rooted tree can be defined in the following recursive manner: to the one-vertex tree there corresponds the number 1; to a tree T with root degree 1 there corresponds the t-th prime number, where t is the Matula-Goebel number of the tree obtained from T by deleting the edge emanating from the root; to a tree T with root degree m\u003e=2 there corresponds the product of the Matula-Goebel numbers of the m branches of T.",
				"Number of entries in row n is the number of prime divisors of n counted with multiplicity.",
				"Sum of entries in row n = A196052(n)."
			],
			"link": [
				"E. Deutsch, \u003ca href=\"http://arxiv.org/abs/1111.4288\"\u003eRooted tree statistics from Matula numbers\u003c/a\u003e, arXiv:1111.4288 [math.CO], 2011.",
				"E. Deutsch, \u003ca href=\"http://dx.doi.org/10.1016/j.dam.2012.05.012\"\u003eRooted tree statistics from Matula numbers\u003c/a\u003e, Discrete Appl. Math., 160, 2012, 2314-2322.",
				"F. Goebel, \u003ca href=\"http://dx.doi.org/10.1016/0095-8956(80)90049-0\"\u003eOn a 1-1-correspondence between rooted trees and natural numbers\u003c/a\u003e, J. Combin. Theory, B 29 (1980), 141-143.",
				"I. Gutman and A. Ivic, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(95)00182-V\"\u003eOn Matula numbers\u003c/a\u003e, Discrete Math., 150, 1996, 131-142.",
				"I. Gutman and Yeong-Nan Yeh, \u003ca href=\"http://www.emis.de/journals/PIMB/067/3.html\"\u003eDeducing properties of trees from their Matula numbers\u003c/a\u003e, Publ. Inst. Math., 53 (67), 1993, 17-22.",
				"D. W. Matula, \u003ca href=\"http://www.jstor.org/stable/2027327\"\u003eA natural rooted tree enumeration by prime factorization\u003c/a\u003e, SIAM Rev. 10 (1968) 273."
			],
			"formula": [
				"Denoting by DL(n) the multiset of the degrees of the level 1 vertices of  the rooted tree with Matula number n, we  have DL(1)=[0], DL[2]=[1], DL(i-th prime) = [1+bigomega(i)], DL(rs) = DL(r) union DL(s), where bigomega(i) is the number of prime divisors of i, counted with multiplicity (A001222) and \"union\" is \"multiset union\". The Maple program is based on these recurrence equations."
			],
			"example": [
				"Row 8 is 1,1,1. Indeed, the rooted tree with Matula number 8 is the star tree \\|/; vertices at level 1 have degrees 1,1,1.",
				"Triangle starts:",
				"0;",
				"1;",
				"2;",
				"1,1;",
				"2;",
				"1,2;",
				"3;",
				"1,1,1;"
			],
			"maple": [
				"with(numtheory): DL := proc (n) if n = 2 then [1] elif bigomega(n) = 1 then [1+bigomega(pi(n))] else [op(DL(op(1, factorset(n)))), op(DL(n/op(1, factorset(n))))] end if end proc: with(numtheory): DL := proc (n) if n = 1 then [0] elif n = 2 then [1] elif bigomega(n) = 1 then [1+bigomega(pi(n))] else [op(DL(op(1, factorset(n)))), op(DL(n/op(1, factorset(n))))] end if end proc: seq(op(DL(n)), n = 1 .. 100);"
			],
			"xref": [
				"Cf. A196052, A001222."
			],
			"keyword": "nonn,tabf",
			"offset": "1,3",
			"author": "_Emeric Deutsch_, May 04 2015",
			"references": 0,
			"revision": 17,
			"time": "2015-05-07T09:22:16-04:00",
			"created": "2015-05-07T09:22:16-04:00"
		}
	]
}