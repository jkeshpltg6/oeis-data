{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A336519",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 336519,
			"data": "3,2,53,7,58979,161923,2643383,1746893,6971,5,17,1499,11,1555077581737,297707,13,37,126541,2130276389911155737,1429,71971,383,61,1559,29,193,12073,698543,157,20289606809,23687,1249,59,2393,251,101,15827173,82351,661",
			"name": "Primes in Pi (variant of A336520): a(n) is the smallest prime factor of A090897(n) that does not appear in earlier terms of a, or 1, if no such factor exists.",
			"comment": [
				"Inspired by a comment of _Mario Cortés_ in A090897, who suggests that 1 might not appear in this sequence."
			],
			"link": [
				"Peter Luschny, \u003ca href=\"/A336519/a336519.txt\"\u003ePrime factorization for n = 1..100\u003c/a\u003e."
			],
			"example": [
				"[ 1] 3,          {3}                  -\u003e 3;",
				"[ 2] 14,         {2, 7}               -\u003e 2;",
				"[ 3] 159,        {3, 53}              -\u003e 53;",
				"[ 4] 2653,       {7, 379}             -\u003e 7;",
				"[ 5] 58979,      {58979}              -\u003e 58979;",
				"[ 6] 323846,     {2, 161923}          -\u003e 161923;",
				"[ 7] 2643383,    {2643383}            -\u003e 2643383;",
				"[ 8] 27950288,   {2, 1746893}         -\u003e 1746893;",
				"[ 9] 419716939,  {6971, 60209}        -\u003e 6971;",
				"[10] 9375105820, {2, 5, 1163, 403057} -\u003e 5."
			],
			"maple": [
				"aList := proc(len) local p, R, spl; R := [];",
				"spl := L -\u003e [seq([seq(L[i], i=1 + n*(n+1)/2..(n+1)*(n+2)/2)], n=0..len)]:",
				"ListTools:-Reverse(convert(floor(Pi*10^((len+1)*(len+2)/2)), base, 10)):",
				"map(`@`(parse,cat,op), spl(%)); map(NumberTheory:-PrimeFactors, %);",
				"for p in % do ListTools:-SelectFirst(p -\u003e evalb(not p in R), p);",
				"R := [op(R), `if`(%=NULL, 1, %)] od end: aList(30);"
			],
			"mathematica": [
				"Block[{nn = 38, s}, s = RealDigits[Pi, 10, (# + 1) (# + 2)/2 \u0026@ nn][[1]]; Nest[Function[{a, n}, Append[a, SelectFirst[FactorInteger[FromDigits@ s[[1 + n (n + 1)/2 ;; (n + 1) (n + 2)/2 ]]][[All, 1]], FreeQ[a, #] \u0026] /. k_ /; MissingQ@ k -\u003e 1]] @@ {#, Length@ #} \u0026, {}, nn + 1]] (* _Michael De Vlieger_, Aug 21 2020 *)"
			],
			"program": [
				"(SageMath)",
				"def Select(item, Selected):",
				"    return next((x for x in item if not (x in Selected)), 1)",
				"def PiPart(n):",
				"    return floor(pi * 10^(n * (n + 1) // 2 - 1)) % 10^n",
				"def A336519List(len):",
				"    prev = []",
				"    for n in range(1, len + 1):",
				"        p = prime_factors(PiPart(n))",
				"        prev.append(Select(p, prev))",
				"    return prev",
				"print(A336519List(39))"
			],
			"xref": [
				"Cf. A090897, A336520."
			],
			"keyword": "nonn,base",
			"offset": "1,1",
			"author": "_Peter Luschny_, Aug 21 2020",
			"references": 2,
			"revision": 22,
			"time": "2020-08-22T18:41:33-04:00",
			"created": "2020-08-22T18:41:33-04:00"
		}
	]
}