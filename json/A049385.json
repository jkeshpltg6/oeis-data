{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A049385",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 49385,
			"data": "1,6,1,66,18,1,1056,372,36,1,22176,9240,1200,60,1,576576,271656,42840,2940,90,1,17873856,9269568,1685376,142800,6090,126,1,643458816,360847872,73313856,7254576,386400,11256,168,1,26381811456,15799069440",
			"name": "Triangle of numbers related to triangle A049375; generalization of Stirling numbers of second kind A008277, Lah-numbers A008297...",
			"comment": [
				"a(n,m) := S2(6; n,m) is the sixth triangle of numbers in the sequence S2(k; n,m), k=1..6: A008277 (unsigned Stirling 2nd kind), A008297 (unsigned Lah), A035342, A035469, A049029, respectively. a(n,1)= A008548(n).",
				"a(n,m) enumerates unordered n-vertex m-forests composed of m plane increasing 6-ary trees. Proof based on the a(n,m) recurrence. See also the F. Bergeron et al. reference, especially Table 1, first row and Example 1 for the e.g.f. for m=1. - _Wolfdieter Lang_, Sep 14 2007"
			],
			"link": [
				"F. Bergeron, Ph. Flajolet and B. Salvy, \u003ca href=\"http://algo.inria.fr/flajolet/Publications/BeFlSa92.pdf\"\u003eVarieties of Increasing Trees\u003c/a\u003e, Lecture Notes in Computer Science vol. 581, ed. J.-C. Raoult, Springer 1992, pp. 24-48. Added Mar 01 2014.",
				"F. Bergeron, Philippe Flajolet and Bruno Salvy, \u003ca href=\"http://hal.inria.fr/inria-00074977\"\u003eVarieties of increasing trees\u003c/a\u003e, HAL, Rapport De Recherche Inria. Added Mar 01 2014.",
				"P. Blasiak, K. A. Penson and A. I. Solomon, \u003ca href=\"https://arxiv.org/abs/quant-ph/0402027\"\u003eThe general boson normal ordering problem\u003c/a\u003e, arXiv:quant-ph/0402027, 2004.",
				"M. Janjic, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL12/Janjic/janjic22.html\"\u003eSome classes of numbers and derivatives\u003c/a\u003e, JIS 12 (2009) 09.8.3",
				"W. Lang, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL3/LANG/lang.html\"\u003eOn generalizations of Stirling number triangles\u003c/a\u003e, J. Integer Seqs., Vol. 3 (2000), #00.2.4.",
				"W. Lang, \u003ca href=\"/A049385/a049385.txt\"\u003eFirst 10 rows\u003c/a\u003e.",
				"Shi-Mei Ma, \u003ca href=\"http://arxiv.org/abs/1208.3104\"\u003eSome combinatorial sequences associated with context-free grammars\u003c/a\u003e, arXiv:1208.3104v2 [math.CO]. - From _N. J. A. Sloane_, Aug 21 2012",
				"E. Neuwirth, \u003ca href=\"http://dx.doi.org/10.1016/S0012-365X(00)00373-3\"\u003eRecursively defined combinatorial Functions: Extending Galton's board\u003c/a\u003e, Discr. Maths. 239 (2001) 33-51."
			],
			"formula": [
				"a(n, m) = n!*A049375(n, m)/(m!*5^(n-m)); a(n+1, m) = (5*n+m)*a(n, m)+ a(n, m-1), n \u003e= m \u003e= 1; a(n, m) := 0, n\u003cm; a(n, 0) := 0, a(1, 1)=1; E.g.f. of m-th column: ((-1+(1-5*x)^(-1/5))^m)/m!.",
				"a(n, m) = sum(|A051150(n, j)|*S2(j, m), j=m..n) (matrix product), with S2(j, m) := A008277(j, m) (Stirling2 triangle). Priv. comm. to _Wolfdieter Lang_ by E. Neuwirth, Feb 15 2001; see also the 2001 Neuwirth reference. See the general comment on products of Jabotinsky matrices given under A035342."
			],
			"example": [
				"Triangle begins:",
				"  {1};",
				"  {6,1};",
				"  {66,18,1};",
				"  {1056,372,36,1};",
				"  ..."
			],
			"maple": [
				"# The function BellMatrix is defined in A264428.",
				"# Adds (1,0,0,0, ..) as column 0.",
				"BellMatrix(n -\u003e mul(5*k+1, k=0..n), 9); # _Peter Luschny_, Jan 28 2016"
			],
			"mathematica": [
				"a[n_, m_] := n!*Coefficient[Series[((-1 + (1 - 5*x)^(-1/5))^m)/m!, {x, 0, n}], x^n];",
				"Flatten[Table[a[n, m], {n, 1, 9}, {m, 1, n}]][[1 ;; 38]]",
				"(* _Jean-François Alcover_, Jun 21 2011, after e.g.f. *)",
				"rows = 9;",
				"t = Table[Product[5k+1, {k, 0, n}], {n, 0, rows}];",
				"T[n_, k_] := BellY[n, k, t];",
				"Table[T[n, k], {n, 1, rows}, {k, 1, n}] // Flatten (* _Jean-François Alcover_, Jun 22 2018, after _Peter Luschny_ *)"
			],
			"xref": [
				"Cf. A049412."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "1,2",
			"author": "_Wolfdieter Lang_",
			"references": 33,
			"revision": 40,
			"time": "2019-08-28T15:38:12-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}