{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A245962",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 245962,
			"data": "1,1,3,2,4,3,7,8,2,11,15,5,18,30,15,2,29,56,35,7,47,104,80,24,2,76,189,171,66,9,123,340,355,170,35,2,199,605,715,407,110,11,322,1068,1410,932,315,48,2,521,1872,2730,2054,832,169,13,843,3262,5208,4396,2079,532,63,2",
			"name": "Triangle read by rows: T(n,k) is the number of induced subgraphs of the Lucas cube Lambda(n) that are isomorphic to the hypercube Q(k).",
			"comment": [
				"Number of entries in row n is 1 + floor(n/2).",
				"The entries in row n are the coefficients of the cube polynomial of the Lucas cube Lambda(n).",
				"For n \u003e= 1, sum of entries in row n = A014551(n) = 2^n + (-1)^n (the Jacobsthal-Lucas numbers).",
				"Sum_{k \u003e= 0} k*T(n,k) = A099429(n).",
				"T(n,0) = A000032(n) (n \u003e= 1; the Lucas numbers); T(n,1) = A099920(n-1); T(n,2) = A245961(n).",
				"As communicated by the authors, Theorem 5.2 and Corollary 5.3 of the Klavzar et al. paper contains a typo: 2nd binomial should be binomial(n - a - 1, a) resp. binomial(n - i - 1, i)."
			],
			"link": [
				"Sandi Klavzar, Michel Mollard, \u003ca href=\"http://www-fourier.ujf-grenoble.fr/~mollard/accepte/CubePolyRevised.pdf\"\u003eCube polynomial of Fibonacci and Lucas cubes\u003c/a\u003e, preprint.",
				"Sandi Klavzar, Michel Mollard, \u003ca href=\"http://dx.doi.org/10.1007/s10440-011-9652-4\"\u003eCube polynomial of Fibonacci and Lucas cubes\u003c/a\u003e, Acta Appl. Math. 117, 2012, 93-105."
			],
			"formula": [
				"T(n,k) = Sum_{i = k..floor(n/2)} (2*binomial(n - i, i) - binomial(n - i - 1, i))*binomial(i, k).",
				"G.f.: (1+(1+t)*z^2)/(1-z-(1+t)*z^2).",
				"The generating polynomial of row n (i.e., the cube polynomial of Lambda(n)) is Sum_{i = 0..floor(n/2)} (2*binomial(n - i, i) - binomial(n - i - 1))(1+x)^i.",
				"The generating polynomial of row n (i.e., the cube polynomial of Lambda(n)) is ((1+w)/2)^n + ((1-w)/2)^n, where w = sqrt(5 + 4x).",
				"The generating function of column k (k \u003e= 1) is z^(2k)(2-z)/(1-z-z^2)^(k+1)."
			],
			"example": [
				"Row 4 is 7, 8, 2. Indeed, the Lucas cube Lambda(4) is the graph \u003c\u003e\u003c\u003e obtained by identifying a vertex of a square with a vertex of another square; it has 7 vertices (i.e., Q(0)s), 8 edges (i.e., Q(1)s), and 2 squares (i.e., Q(2)s).",
				"Triangle starts:",
				"   1;",
				"   1;",
				"   3,  2;",
				"   4,  3;",
				"   7,  8,  2;",
				"  11, 15,  5;"
			],
			"maple": [
				"T := proc (n, k) options operator, arrow: add((2*binomial(n-i, i)-binomial(n-i-1, i))*binomial(i, k), i = k .. floor((1/2)*n)) end proc: for n from 0 to 20 do seq(T(n, k), k = 0 .. (1/2)*n) end do; # yields sequence in triangular form"
			],
			"xref": [
				"Cf. A000032, A014551, A099429, A099920, A245961."
			],
			"keyword": "nonn,tabf",
			"offset": "0,3",
			"author": "_Emeric Deutsch_, Aug 14 2014",
			"references": 0,
			"revision": 23,
			"time": "2019-07-19T20:25:28-04:00",
			"created": "2014-08-16T10:02:07-04:00"
		}
	]
}