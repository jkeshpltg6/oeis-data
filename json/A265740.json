{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A265740",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 265740,
			"data": "1,6,13,10,14,17,7,8,19,23,21,29,34,31,3,38,28,46,47,35,39,49,43,51,42,41,48,53,26,12,57,58,59,2,61,24,68,11,52,63,22,69,62,71,56,65,76,81,44,67,64,83,85,78,77,79,72,70,80,87,84,86,89,9,91,92,73",
			"name": "a(1)=1; a(n+1) is the smallest positive integer not yet used such that all the digits of a(n) and a(n+1) are present in the decimal expansion (including any leading and trailing zeros) of a(n)/a(n+1).",
			"comment": [
				"Conjecture: a(n) is a permutation of the natural numbers.",
				"The following table shows:",
				"C = number of terms calculated",
				"F = first term that is missing",
				"C           F      F/C",
				"1000        5    0.005",
				"2000       50    0.025",
				"5000     1650    0.330",
				"10000    1650    0.165",
				"20000    2475    0.124",
				"50000   24750    0.495",
				"100000 100000    1.000",
				"200000 199800    0.999",
				"500000 499500    0.999",
				"which seems to support the conjecture."
			],
			"link": [
				"Lars Blomberg, \u003ca href=\"/A265740/b265740.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"   1/6  = 0.1666... (1 and 6 are visible on the right-hand side)",
				"   6/13 = 0.461538461538... (6, 1 and 3 are visible)",
				"  13/10 = 1.30 (trailing zeros are included)",
				"  10/14 = 0.7142857142... (1, 0 and 4)",
				"  14/17 = 0.8235294117... (1, 4 and 7)",
				"  17/7  = 2.4285714285... (1 and 7)",
				"   7/8  = 0.875 (7 and 8)",
				"   ..."
			],
			"mathematica": [
				"f[n_] := Block[{a = {1}, k}, Do[k = If[MissingQ@ #, Max@ a, #] \u0026@ SelectFirst[Range@ Max@ a, ! MemberQ[a, #] \u0026]; While[Or[! AllTrue[Join[IntegerDigits@ a[[i - 1]], IntegerDigits@ k], MemberQ[Union@ Flatten@ Prepend[First@ #, If[Last@ # \u003c= 0, 0, Nothing]] \u0026@ If[Depth@ First@ # \u003c 3, Insert[#, 0, {1, 1}], #] \u0026@ RealDigits[a[[i - 1]]/k], #] \u0026], MemberQ[a, k]], k++]; AppendTo[a, k], {i, 2, n}]; a]; f@ 67 (* Version 10.2 *)",
				"f[n_] := Block[{a = {1}, k}, Do[k = 1; While[Or[If[# == 1, False, True] \u0026[Times @@ Boole[MemberQ[Union@ Flatten@ Prepend[First@ #, If[Last@ # \u003c= 0, 0]] \u0026@ If[Depth@ First@ # \u003c 3, Insert[#, 0, {1, 1}], #] \u0026@ RealDigits[a[[i - 1]]/k], #] \u0026 /@ Join[IntegerDigits@ a[[i - 1]], IntegerDigits@ k]]], MemberQ[a, k]], k++]; AppendTo[a, k], {i, 2, n}]; a]; f@ 67 (* _Michael De Vlieger_, Dec 16 2015, Version 6 *)"
			],
			"xref": [
				"See A265756 for another version.",
				"See also A257664."
			],
			"keyword": "nonn,base",
			"offset": "1,2",
			"author": "_Eric Angelini_, submitted by _Lars Blomberg_, Dec 15 2015",
			"ext": [
				"Corrected values for n\u003e=58 by _Lars Blomberg_, Dec 16 2015"
			],
			"references": 3,
			"revision": 29,
			"time": "2015-12-23T14:13:35-05:00",
			"created": "2015-12-15T13:58:55-05:00"
		}
	]
}