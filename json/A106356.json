{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A106356",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 106356,
			"data": "1,1,1,3,0,1,4,3,0,1,7,6,2,0,1,14,7,8,2,0,1,23,20,10,8,2,0,1,39,42,22,13,9,2,0,1,71,72,58,28,14,10,2,0,1,124,141,112,72,33,16,11,2,0,1,214,280,219,150,92,36,18,12,2,0,1,378,516,466,311,189,112,40,20,13,2,0,1",
			"name": "Triangle T(n,k) 0\u003c=k\u003cn : Number of compositions of n with k adjacent equal parts.",
			"comment": [
				"For n \u003e 0, also the number of compositions of n with k + 1 maximal anti-runs (sequences without adjacent equal terms). - _Gus Wiseman_, Mar 23 2020"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A106356/b106356.txt\"\u003eRows n = 1..141, flattened\u003c/a\u003e",
				"A. Knopfmacher and H. Prodinger, \u003ca href=\"http://web.wits.ac.za/Academic/Science/Maths/Staff/Knopfmacher/Publications.htm\"\u003eOn Carlitz compositions\u003c/a\u003e, European Journal of Combinatorics, Vol. 19, 1998, pp. 579-589."
			],
			"example": [
				"T(4,1) = 3 because the compositions of 4 with 1 adjacent equal part are 1+1+2, 2+1+1, 2+2.",
				"Triangle begins:",
				"1;",
				"1,   1;",
				"3,   0,  1;",
				"4,   3,  0, 1;",
				"7,   6,  2, 0, 1;",
				"14,  7,  8, 2, 0, 1;",
				"23, 20, 10, 8, 2, 0, 1;",
				"From _Gus Wiseman_, Mar 23 2020 (Start)",
				"Row n = 6 counts the following compositions (empty column shown by dot):",
				"  (6)     (33)    (222)    (11112)  .  (111111)",
				"  (15)    (114)   (1113)   (21111)",
				"  (24)    (411)   (1122)",
				"  (42)    (1131)  (2211)",
				"  (51)    (1221)  (3111)",
				"  (123)   (1311)  (11121)",
				"  (132)   (2112)  (11211)",
				"  (141)           (12111)",
				"  (213)",
				"  (231)",
				"  (312)",
				"  (321)",
				"  (1212)",
				"  (2121)",
				"(End)"
			],
			"maple": [
				"b:= proc(n, h, t) option remember;",
				"      if n=0 then `if`(t=0, 1, 0)",
				"    elif t\u003c0 then 0",
				"    else add(b(n-j, j, `if`(j=h, t-1, t)), j=1..n)",
				"      fi",
				"    end:",
				"T:= (n, k)-\u003e b(n, -1, k):",
				"seq(seq(T(n, k), k=0..n-1), n=1..15); # _Alois P. Heinz_, Oct 23 2011"
			],
			"mathematica": [
				"b[n_, h_, t_] := b[n, h, t] = If[n == 0, If[t == 0, 1, 0], If[t\u003c0, 0, Sum[b[n-j, j, If [j == h, t-1, t]], {j, 1, n}]]]; T[n_, k_] := b[n, -1, k]; Table[Table[T[n, k], {k, 0, n-1}], {n, 1, 15}] // Flatten (* _Jean-François Alcover_, Feb 20 2015, after _Alois P. Heinz_ *)",
				"Table[Length[Select[Join@@Permutations/@IntegerPartitions[n],n==0||Length[Split[#,#1!=#2\u0026]]==k+1\u0026]],{n,0,12},{k,0,n}] (* _Gus Wiseman_, Mar 23 2020 *)"
			],
			"xref": [
				"Row sums: 2^(n-1)=A000079(n-1). Columns 0-4: A003242, A106357-A106360.",
				"The version counting adjacent unequal parts is A238279.",
				"The k-th composition in standard-order has A124762(k) adjacent equal parts and A333382(k) adjacent unequal parts.",
				"The k-th composition in standard-order has A124767(k) maximal runs and A333381(k) maximal anti-runs.",
				"The version for ascents/descents is A238343.",
				"The version for weak ascents/descents is A333213.",
				"Cf. A064113, A066099, A233564, A333214, A333216."
			],
			"keyword": "nonn,tabl",
			"offset": "1,4",
			"author": "_Christian G. Bower_, Apr 29 2005",
			"references": 69,
			"revision": 20,
			"time": "2020-03-25T22:53:11-04:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}