{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A094649",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 94649,
			"data": "4,1,7,4,19,16,58,64,187,247,622,925,2110,3394,7252,12289,25147,44116,87727,157492,307294,560200,1079371,1987891,3798310,7043041,13382818,24927430,47191492,88165105,166501903,311686804,587670811,1101562312",
			"name": "An accelerator sequence for Catalan's constant.",
			"comment": [
				"From _L. Edson Jeffery_, Apr 03 2011: (Start)",
				"Let U be the unit-primitive matrix (see [Jeffery])",
				"U = U_(9,1) =",
				"(0 1 0 0)",
				"(1 0 1 0)",
				"(0 1 0 1)",
				"(0 0 1 1).",
				"Then a(n) = Trace(U^n). (End)",
				"a(n)==1 (mod 3), a(3*n+1)==1 (mod 9). - _Roman Witula_, Sep 14 2012"
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A094649/b094649.txt\"\u003eTable of n, a(n) for n = 0..3649\u003c/a\u003e",
				"A. Akbary and Q. Wang, \u003ca href=\"http://dx.doi.org/10.1155/IJMMS.2005.2631\"\u003eOn some permutation polynomials over finite fields\u003c/a\u003e, International Journal of Mathematics and Mathematical Sciences, 2005:16 (2005) 2631-2640.",
				"A. Akbary and Q. Wang, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9939-05-08220-1\"\u003eA generalized Lucas sequence and permutation binomials\u003c/a\u003e, Proceeding of the American Mathematical Society, 134 (1) (2006), 15-22, sequence a(n) with l=9.",
				"David M. Bradley, \u003ca href=\"http://dx.doi.org/10.1023/A:1006945407723\"\u003eA Class of Series Acceleration Formulae for Catalan's Constant\u003c/a\u003e, The Ramanujan Journal, Vol. 3, Issue 2, 1999, pp. 159-173.",
				"David M. Bradley, \u003ca href=\"http://arxiv.org/abs/0706.0356\"\u003eA Class of Series Acceleration Formulae for Catalan's Constant\u003c/a\u003e, arXiv:0706.0356 [math.CA], 2007.",
				"L. E. Jeffery, \u003ca href=\"/wiki/User:L._Edson_Jeffery/Unit-Primitive_Matrices\"\u003eUnit-primitive matrices\u003c/a\u003e",
				"Genki Shibukawa, \u003ca href=\"https://arxiv.org/abs/1907.00334\"\u003eNew identities for some symmetric polynomials and their applications\u003c/a\u003e, arXiv:1907.00334 [math.CA], 2019.",
				"Q. Wang, \u003ca href=\"https://www.semanticscholar.org/paper/On-generalized-Lucas-sequences-Wang-Akbari/7e33b3b79703dc6790fca133e8c92cc0cafcfe4a\"\u003eOn generalized Lucas sequences\u003c/a\u003e, Contemp. Math. 531 (2010) 127-141, Table 2 (k=4)",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,3,-2,-1)."
			],
			"formula": [
				"G.f.: ( 4-3*x-6*x^2+2*x^3 ) / ( (x-1)*(x^3+3*x^2-1) )",
				"a(n) = 1+(2*cos(Pi/9))^n+(-2*sin(Pi/18))^n+(-2*cos(2*Pi/9))^n.",
				"a(n) = 2^n*Sum_{k=1..4} cos((2*k-1)*Pi/9)^n. - _L. Edson Jeffery_, Apr 03 2011",
				"a(n) = 1 + (-1)^n*A215664(n), which is compatible with the last two formulas above. - _Roman Witula_, Sep 14 2012",
				"a(n) = 3*a(n-2) + a(n-3) - 3, with a(0)=4, a(1)=1, and a(2)=7. - _Roman Witula_, Sep 14 2012"
			],
			"example": [
				"We have a(0)+a(3)=a(1)+a(2)=8, a(3)+a(4)=a(2)+a(5)=23, and a(7)+a(8)=a(9)+a(3)=247. - _Roman Witula_, Sep 14 2012"
			],
			"mathematica": [
				"LinearRecurrence[{1, 3, -2, -1}, {4, 1, 7, 4}, 34] (* _Jean-François Alcover_, Sep 21 2017 *)"
			],
			"program": [
				"(PARI) Vec((4-3*x-6*x^2+2*x^3)/(1-x-3*x^2+2*x^3+x^4)+O(x^66)) /* _Joerg Arndt_, Apr 08 2011 */"
			],
			"xref": [
				"Cf. A000032, A094648, A094650."
			],
			"keyword": "easy,nonn",
			"offset": "0,1",
			"author": "_Paul Barry_, May 18 2004",
			"references": 5,
			"revision": 56,
			"time": "2019-08-21T16:17:33-04:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}