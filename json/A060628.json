{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A060628",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 60628,
			"data": "1,1,1,1,14,1,1,135,135,1,1,1228,5478,1228,1,1,11069,165826,165826,11069,1,1,99642,4494351,13180268,4494351,99642,1,1,896803,116294673,834687179,834687179,116294673,896803,1,1,8071256,2949965020",
			"name": "Triangle of coefficients in expansion of elliptic function sn(u) in powers of u and k.",
			"reference": [
				"CRC Standard Mathematical Tables and Formulae, 30th ed. 1996, p. 526.",
				"I. P. Goulden and D. M. Jackson, Combinatorial Enumeration, Wiley, N.Y., 1983, (5.2.24)."
			],
			"link": [
				"M. Abramowitz and I. A. Stegun, eds.,\u003ca href=\"http://people.math.sfu.ca/~cbm/aands/toc.htm\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972, p. 575, 16.22.1.",
				"A. Cayley, \u003ca href=\"http://cdl.library.cornell.edu/Hunter/hunter.pl?handle=cornell.library.math/Cayl005\u0026amp;id=7\"\u003eAn Elementary Treatise on Elliptic Functions\u003c/a\u003e (page images), G. Bell and Sons, London, 1895, p. 56.",
				"F. Clarke, \u003ca href=\"http://www-maths.swan.ac.uk/staff/fwc/Gregynog-slides.pdf\"\u003eThe Taylor Series Coefficients of the Jacobi Elliptic Functions\u003c/a\u003e, slides.",
				"D. Dominici, \u003ca href=\"http://arxiv.org/abs/math/0501052\"\u003eNested derivatives: A simple method for computing series expansions of inverse functions.\u003c/a\u003e arXiv:math/0501052v2 [math.CA], 2005.",
				"A. Fransen, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1981-0628708-X\"\u003eConjectures on the Taylor series expansion coefficients of the Jacobian elliptic function sn(n,k)\u003c/a\u003e, Math. Comp., 37 (1981), 475-497.",
				"R. Fricke, \u003ca href=\"http://dx.doi.org/10.1007/978-3-642-19557-0_8\"\u003eDie elliptischen Funktionen und ihre Anwendungen, Erster Teil\u003c/a\u003e, p. 399 with p. 397.",
				"C. L. Mallows, \u003ca href=\"/A004004/a004004.pdf\"\u003eLetter to N. J. A. Sloane, May 16 1973\u003c/a\u003e",
				"J. Tannery and J. Molk, \u003ca href=\"http://gallica.bnf.fr/ark:/12148/bpt6k99586x/f104.image\"\u003eEléments de la Théorie des Fonctions Elliptiques (Vol. 4)\u003c/a\u003e, Gauthier-Villars, Paris, 1902, p. 92.",
				"G. Viennot, \u003ca href=\"http://dx.doi.org/10.1016/0097-3165(80)90001-1\"\u003eUne interprétation combinatoire des coefficients des développements en série entière des fonctions elliptiques de Jacobi\u003c/a\u003e, J. Combin. Theory, A 29 (1980), 121-133.",
				"Eric W. Weisstein, \u003ca href=\"http://mathworld.wolfram.com/JacobiEllipticFunctions.html\"\u003e Jacobi Elliptic Functions\u003c/a\u003e"
			],
			"formula": [
				"Sum_{n\u003e=0} Sum_{k=0..n} (-1)^n*T(n, k)*y^(2*k)*x^(2*n+1)/(2*n+1)! = JacobiSN(x, y).",
				"JacobiSN(x, y) = 1*x + (-1/6 - (1/6)*y^2)*x^3 + (1/120 + (7/60)*y^2 + (1/120)*y^4)*x^5 + (-1/5040 - (3/112)*y^4 - (3/112)*y^2 - (1/5040)*y^6)*x^7 + (1/362880 + (307/90720)*y^6 + (913/60480)*y^4 + (307/90720)*y^2 + (1/362880)*y^8)*x^9 + O(x^11).",
				"From _Peter Bala_, Aug 23 2011: (Start)",
				"Let f(x) = sqrt((1-x^2)*(1-k^2*x^2)).",
				"Define the nested derivative D^n[f](x) by means of the recursion D^0[f](x) = 1 and D^(n+1)[f](x) = d/dx(f(x)*D^n[f](x)) for n \u003e= 0.",
				"Then the coefficient polynomial R(2*n+1,k) of u^(2*n+1)/(2*n+1)! is given by R(2*n+1,k) = D^(2*n)[f](0) - apply [Dominici, Theorem 4.1].",
				"See A145271 for the coefficients in the expansion of D^n[f](x) in powers of f(x).",
				"(End)",
				"sn(u|k^2) = Sum_{n\u003e=0} a_n(k^2)*u^(2*n+1)/(2*n+1)!. For the recurrence of the row polynomials a_n(k^2) = Sum_{m=0..n} (-1)^n*T(n, m)*k^(2*m), see the Fricke reference. - _Wolfdieter Lang_, Jul 05 2016"
			],
			"example": [
				"sn u = u - (1 + k^2)*u^3/3! + (1 + 14*k^2 + k^4)*u^5/5! - (1 + 135*k^2 + 135*k^4 + k^6)*u^7/7! + ...",
				"The triangle T(n, m) begins:",
				"n\\m 0      1         2         3         4         5      6  7",
				"0:  1",
				"2:  1      1",
				"3:  1     14         1",
				"4:  1    135       135         1",
				"5:  1   1228      5478      1228         1",
				"6:  1  11069    165826    165826     11069         1",
				"7:  1  99642   4494351  13180268   4494351     99642      1",
				"8:  1 896803 116294673 834687179 834687179 116294673 896803  1",
				"... reformatted. - _Wolfdieter Lang_, Jul 05 2016"
			],
			"maple": [
				"Maple program from Rostislav Kollman (kollman(AT)dynasig.cz), Nov 05 2009 (Start) The program generates an \"all in one\" triangle of Taylor coefficients of the Jacobi SN,CN,DN functions.",
				"\"SN \", 1 \"CN \", 1 \"DN \", 1",
				"\"SN \", 1, 1 \"CN \", 1, 4 \"DN \", 4, 1",
				"\"SN \", 1, 14, 1 \"CN \", 1, 44, 16 \"DN \", 16, 44, 1",
				"\"SN \", 1, 135, 135, 1 \"CN \", 1, 408, 912, 64 \"DN \", 64, 912, 408, 1",
				"\"SN \", 1, 1228, 5478, 1228, 1 \"CN \", 1, 3688, 30768, 15808, 256 \"DN \", 256, 15808, 30768, 3688, 1",
				"\"SN \", 1, 11069, 165826, 165826, 11069, 1 \"CN \", 1, 33212, 870640, 1538560, 259328, 1024 \"DN \", 1024, 259328, 1538560, 870640, 33212, 1",
				"#----------------------------------------------------------------",
				"# Taylor series coefficients of Jacobi SN,CN,DN",
				"#----------------------------------------------------------------",
				"n := 6: g := x: for i from 1 to 2*n do g := simplify(y*z*diff(g,x) + x*z*diff(g,y) + x*y*diff(g,z)); if(type(i,odd))then SN := simplify(sort(subs(z = k,subs(y = 1,subs(x = 0,g)))) / k);",
				"# lprint(\"SN \",SN); lprint(\"SN \",seq(coeff(SN, k, j),j=0..i-1,2)); else CN := simplify(sort(subs(z = 1,subs(y = 0,subs(x = k,g)))) / k); DN := simplify(sort(subs(z = 0,subs(y = k,subs(x = 1,g)))));",
				"# lprint(\"CN \",CN); # lprint(\"DN \",DN); lprint(\"CN \",seq(coeff(CN, k, j),j=0..i-2,2)); lprint(\"DN \",seq(coeff(DN, k, j),j=2..i,2)); end; end: (End)",
				"A060628 := proc(n,m) JacobiSN(z,k) ; coeftayl(%,z=0,2*n+1) ; (-1)^n*coeftayl(%,k=0,2*m)*(2*n+1)! ; end proc: # alternative program, _R. J. Mathar_, Jan 30 2011"
			],
			"mathematica": [
				"maxn = 8; se = Series[ JacobiSN[u, m], {u, 0, 2*maxn + 1 }]; cc = Partition[ CoefficientList[se, u], 2][[All, 2]]; Flatten[ (CoefficientList[#, m] \u0026 /@ cc)* Table[(-1)^n*(2*n + 1)!, {n, 0, maxn}]] (* _Jean-François Alcover_, Sep 21 2011 *)"
			],
			"xref": [
				"Row sums give A000182. Diagonals: A004004, A004005, A002753."
			],
			"keyword": "easy,nonn,tabl,nice",
			"offset": "0,5",
			"author": "_Vladeta Jovovic_, Apr 13 2001",
			"references": 14,
			"revision": 41,
			"time": "2019-05-10T22:17:18-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}