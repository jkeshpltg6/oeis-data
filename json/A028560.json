{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A028560",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 28560,
			"data": "0,7,16,27,40,55,72,91,112,135,160,187,216,247,280,315,352,391,432,475,520,567,616,667,720,775,832,891,952,1015,1080,1147,1216,1287,1360,1435,1512,1591,1672,1755,1840,1927,2016,2107,2200,2295,2392",
			"name": "a(n) = n*(n + 6), also numbers j such that 9*(9 + j) is a perfect square.",
			"comment": [
				"Nonnegative X values of solutions to the equation X + (X + 3)^2 + (X + 6)^3 = Y^2. To prove that X = n^2 + 6n: Y^2 = X + (X + 3)^2 + (X + 6)^3 = X^3 + 19*X^2 + 115X + 225 = (X + 9)*(X^2 + 10X + 25) = (X + 9)*(X + 5)^2 it means: (X + 9) must be a perfect square, so X = k^2 - 9 with k\u003e=3. we can put: k = n + 3, which gives: X = n^2 + 6n and Y = (n + 3)*(n^2 + 6n + 5). - _Mohamed Bouhamida_, Nov 12 2007",
				"Number of units of a(n) belongs to a periodic sequence: 0, 7, 6, 7, 0, 5, 2, 1, 2, 5. - _Mohamed Bouhamida_, Sep 04 2009",
				"a(n) = A028884(n) - 1. - _Reinhard Zumkeller_, Apr 07 2013",
				"a(m) where m is a positive integer are the only positive integer values of t for which the Binet-de Moivre Formula of the recurrence b(n)=6*b(n-1)+t*b(n-2) with b(0)=0 and b(1)=1 has a root which is a square. In particular, sqrt(6^2+4*t) is an integer since 6^2+4*t=6^2+4*a(m)=(2*m+6)^2. Thus, the charcteristic roots are k1=6+m and k2=-m. - _Felix P. Muga II_, Mar 27 2014"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A028560/b028560.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Patrick De Geest, \u003ca href=\"http://www.worldofnumbers.com/consemor.htm\"\u003ePalindromic Quasipronics of the form n(n+x)\u003c/a\u003e",
				"M. Janjic and B. Petkovic, \u003ca href=\"http://arxiv.org/abs/1301.4550\"\u003eA Counting Function\u003c/a\u003e, arXiv 1301.4550 [math.CO], 2013.",
				"F. P. Muga II, \u003ca href=\"https://www.researchgate.net/publication/267327689_Extending_the_Golden_Ratio_and_the_Binet-de_Moivre_Formula\"\u003eExtending the Golden Ratio and the Binet-de Moivre Formula\u003c/a\u003e, March 2014; Preprint on ResearchGate.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Hydrogen_spectral_series\"\u003eHydrogen spectral series\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-3,1)."
			],
			"formula": [
				"a(n) = (n+3)^2 - 3^2 = n*(n+6).",
				"G.f.: x*(7-5*x)/(1-x)^3.",
				"a(n) = 2*n + a(n-1) + 5. - _Vincenzo Librandi_, Aug 05 2010",
				"Sum_{n\u003e=1} 1/a(n) = 49/120 = 0.4083333... - _R. J. Mathar_, Mar 22 2011",
				"E.g.f.: x*(x+7)*exp(x). - _G. C. Greubel_, Aug 19 2017",
				"Sum_{n\u003e=1} (-1)^(n+1)/a(n) = 37/360. - _Amiram Eldar_, Nov 04 2020"
			],
			"maple": [
				"A028560:=n-\u003en*(n + 6); seq(A028560(n), n=0..100); # _Wesley Ivan Hurt_, Mar 27 2014"
			],
			"mathematica": [
				"Table[n(n + 6), {n, 0, 65}] (* or *) Select[ Range[0, 5000], IntegerQ[ Sqrt[9(9 + #)]]\u0026 ]"
			],
			"program": [
				"(Sage) [lucas_number2(2,n,4-n) for n in range(2,49)] # _Zerinvary Lajos_, Mar 19 2009",
				"(Haskell)",
				"a028560 n = n * (n + 6)  -- _Reinhard Zumkeller_, Apr 07 2013",
				"(PARI) a(n)=n*(n+6) \\\\ _Charles R Greathouse IV_, Sep 24 2015"
			],
			"xref": [
				"a(n-3), n\u003e=4, third column (used for the Paschen series of the hydrogen atom) of triangle A120070.",
				"Cf. A005563."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Patrick De Geest_",
			"ext": [
				"Edited by _Robert G. Wilson v_, Feb 06 2002"
			],
			"references": 37,
			"revision": 78,
			"time": "2021-07-10T20:33:56-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}