{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001018",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1018,
			"id": "M4555 N1937",
			"data": "1,8,64,512,4096,32768,262144,2097152,16777216,134217728,1073741824,8589934592,68719476736,549755813888,4398046511104,35184372088832,281474976710656,2251799813685248,18014398509481984,144115188075855872,1152921504606846976,9223372036854775808,73786976294838206464,590295810358705651712,4722366482869645213696",
			"name": "Powers of 8: a(n) = 8^n.",
			"comment": [
				"Same as Pisot sequences E(1,8), L(1,8), P(1,8), T(1,8). See A008776 for definitions of Pisot sequences.",
				"If X_1, X_2, ..., X_n is a partition of the set {1..2n} into blocks of size 2 then, for n\u003e=1, a(n) is equal to the number of functions f : {1..2n} -\u003e {1,2,3} such that for fixed y_1,y_2,...,y_n in {1,2,3} we have f(X_i)\u003c\u003e{y_i}, (i=1..n). - _Milan Janjic_, May 24 2007",
				"This is the auto-convolution (convolution square) of A059304. - _R. J. Mathar_, May 25 2009",
				"The compositions of n in which each natural number is colored by one of p different colors are called p-colored compositions of n. For n\u003e=1, a(n) equals the number of 8-colored compositions of n such that no adjacent parts have the same color. - _Milan Janjic_, Nov 17 2011",
				"a(n) is equal to the determinant of a 3 X 3 matrix with rows 2^(n+2), 2^(n+1), 2^n; 2^(n+3), 2^(n+4), 2(n+3); 2^n, 2^(n+1), 2^(n+2) when it is divided by 144. - _J. M. Bergot_, May 07 2014"
			],
			"reference": [
				"K. H. Rosen et al., eds., Handbook of Discrete and Combinatorial Mathematics, CRC Press, 2017; p. 15.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A001018/b001018.txt\"\u003eTable of n, a(n) for n = 0..100\u003c/a\u003e",
				"P. J. Cameron, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL3/groups.html\"\u003eSequences realized by oligomorphic permutation groups\u003c/a\u003e, J. Integ. Seqs. Vol. 3 (2000), #00.1.5.",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=273\"\u003eEncyclopedia of Combinatorial Structures 273\u003c/a\u003e",
				"Milan Janjic, \u003ca href=\"https://pmf.unibl.org/wp-content/uploads/2017/10/enumfor.pdf\"\u003eEnumerative Formulas for Some Functions on Finite Sets\u003c/a\u003e",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"Caroline Nunn, \u003ca href=\"https://scholar.rose-hulman.edu/rhumj/vol22/iss2/3\"\u003eA Proof of a Generalization of Niven's Theorem Using Algebraic Number Theory\u003c/a\u003e, Rose-Hulman Undergraduate Mathematics Journal: Vol. 22, Iss. 2, Article 3 (2021). See table at p. 9.",
				"Y. Puri and T. Ward, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL4/WARD/short.html\"\u003eArithmetic and growth of periodic orbits\u003c/a\u003e, J. Integer Seqs., Vol. 4 (2001), #01.2.1.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/SierpinskiCarpet.html\"\u003eSierpiński Carpet\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_01\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (8)."
			],
			"formula": [
				"a(n) = 8^n.",
				"a(0) = 1; a(n) = 8*a(n-1) for n \u003e 0.",
				"G.f.: 1/(1-8*x).",
				"E.g.f.: exp(8*x).",
				"Sum_{n\u003e=0} 1/a(n) = 8/7. - _Gary W. Adamson_, Aug 29 2008",
				"a(n) = A157176(A008588(n)); a(n+1) = A157176(A016969(n)). - _Reinhard Zumkeller_, Feb 24 2009",
				"From _Stefano Spezia_, Dec 28 2021: (Start)",
				"a(n) = (-1)^n*(1 + sqrt(-3))^(3*n) (see Nunn, p. 9).",
				"a(n) = (-1)^n*Sum_{k=0..floor(3*n/2)} (-3)^k*binomial(3*n, 2*k) (see Nunn, p. 9). (End)"
			],
			"maple": [
				"seq(8^n, n=0..23); # _Nathaniel Johnston_, Jun 26 2011",
				"A001018 := n -\u003e 8^n; # _M. F. Hasler_, Apr 19 2015"
			],
			"mathematica": [
				"Table[8^n, {n,0,50}] (* _Vladimir Joseph Stephan Orlovsky_, Feb 15 2011 *)"
			],
			"program": [
				"(Maxima) makelist(8^n,n,0,20); /* _Martin Ettl_, Nov 12 2012 */",
				"(PARI) a(n)=8^n \\\\ _Charles R Greathouse IV_, May 10 2014",
				"(Haskell)",
				"a001018 = (8 ^)",
				"a001018_list = iterate (* 8) 1  -- _Reinhard Zumkeller_, Apr 29 2015",
				"(MAGMA) [8^n : n in [0..30]]; // _Wesley Ivan Hurt_, Sep 27 2016",
				"(Python)",
				"print([8**n for n in range(25)]) # _Michael S. Branicky_, Dec 29 2021"
			],
			"xref": [
				"Cf. A013730, A103333, A013731, A067417, A083233, A055274.",
				"Cf. A008588, A016969, A157176.",
				"Cf. A000079 (powers of 2), A000244 (powers of 3), A000302 (powers of 4), A000351 (powers of 5), A000400 (powers of 6), A000420 (powers of 7), A001019 (powers of 9), ..., A001029 (powers of 19), A009964 (powers of 20), ..., A009992 (powers of 48), A087752 (powers of 49), A165800 (powers of 50), A159991 (powers of 60).",
				"Cf. A032766 (floor(3*n/2))."
			],
			"keyword": "nonn,easy,changed",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 92,
			"revision": 104,
			"time": "2022-01-11T22:04:21-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}