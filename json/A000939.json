{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000939",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 939,
			"id": "M1280 N0491",
			"data": "1,1,1,2,4,14,54,332,2246,18264,164950,1664354,18423144,222406776,2905943328,40865005494,615376173184,9880209206458,168483518571798,3041127561315224,57926238289970076,1161157777643184900,24434798429947993054,538583682082245127336",
			"name": "Number of inequivalent n-gons.",
			"comment": [
				"Here two n-gons are said to be equivalent if they differ in starting point, orientation, or by a rotation (but not by a reflection - for that see A000940).",
				"Number of cycle necklaces on n vertices, defined as equivalence classes of (labeled, undirected) Hamiltonian cycles under rotation of the vertices. The path version is A275527. - _Gus Wiseman_, Mar 02 2019"
			],
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Georg Fischer, \u003ca href=\"/A000939/b000939.txt\"\u003eTable of n, a(n) for n = 1..450\u003c/a\u003e (first 100 terms by T. D. Noe)",
				"S. W. Golomb and L. R. Welch, \u003ca href=\"http://www.jstor.org/stable/2308978\"\u003eOn the enumeration of polygons\u003c/a\u003e, Amer. Math. Monthly, 67 (1960), 349-353.",
				"S. W. Golomb and L. R. Welch, \u003ca href=\"/A000939/a000939.pdf\"\u003eOn the enumeration of polygons\u003c/a\u003e, Amer. Math. Monthly, 67 (1960), 349-353. [Annotated scanned copy]",
				"Samuel Herman, Eirini Poimenidou, \u003ca href=\"https://arxiv.org/abs/1905.04785\"\u003eOrbits of Hamiltonian Paths and Cycles in Complete Graphs\u003c/a\u003e, arXiv:1905.04785 [math.CO], 2019.",
				"A. Stoimenow, \u003ca href=\"https://doi.org/10.1142/S0218216598000073\"\u003eEnumeration of chord diagrams and an upper bound for Vassiliev invariants\u003c/a\u003e, J. Knot Theory Ramifications, 7 (1998), no. 1, 93-114.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/HamiltonianCycle.html\"\u003eHamiltonian Cycle\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Hamiltonian_path\"\u003eHamiltonian path\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Polygon\"\u003ePolygon\u003c/a\u003e.",
				"Gus Wiseman, \u003ca href=\"/A000939/a000939.png\"\u003eInequivalent representatives of the  a(6) = 14 cycle necklaces\u003c/a\u003e.",
				"Gus Wiseman, \u003ca href=\"/A000939/a000939_1.png\"\u003eInequivalent representatives of the  a(7) = 54 n-gons\u003c/a\u003e."
			],
			"formula": [
				"For formula see Maple lines.",
				"a(2*n + 1) =  A002619(2*n + 1)/2 for n \u003e 0; a(2*n) = (A002619(2*n) + A002866(n-1))/2 for n \u003e 1. - _Andrew Howroyd_, Aug 17 2019"
			],
			"example": [
				"Possibilities for n-gons without distinguished vertex can be encoded as permutation classes of vertices, two permutations being equivalent if they can be obtained from each other by circular rotation, translation mod n or complement to n+1.",
				"n=3: 123.",
				"n=4: 1234, 1243.",
				"n=5: 12345, 12354, 12453, 13524.",
				"n=6: 123456, 123465, 123564, 123645, 123654, 124365, 124635, 124653, 125364, 125463, 125634, 126435, 126453, 135264."
			],
			"maple": [
				"with(numtheory):",
				"# for n odd:",
				"Ed:= proc(n) local t1, d; t1:=0; for d from 1 to n do",
				"       if n mod d = 0 then t1:= t1+phi(n/d)^2*d!*(n/d)^d fi od:",
				"       t1/(2*n^2)",
				"     end:",
				"# for n even:",
				"Ee:= proc(n) local t1, d; t1:= 2^(n/2)*(n/2)*(n/2)!; for d",
				"       from 1 to n do if n mod d = 0 then t1:= t1+",
				"       phi(n/d)^2*d!*(n/d)^d; fi od: t1/(2*n^2)",
				"     end:",
				"A000939:= n-\u003e if n mod 2 = 0 then ceil(Ee(n)) else ceil(Ed(n)); fi:",
				"seq(A000939(n), n=1..25);"
			],
			"mathematica": [
				"a[n_] := (t = If[OddQ[n], 0, 2^(n/2)*(n/2)*(n/2)!]; Do[If[Mod[n, d]==0, t = t+EulerPhi[n/d]^2*d!*(n/d)^d], {d, 1, n}]; t/(2*n^2)); a[1] := 1; a[2] := 1; Print[a /@ Range[1, 450]] (* _Jean-François Alcover_, May 19 2011, after Maple prog. *)",
				"rotgra[g_,m_]:=Sort[Sort/@(g/.k_Integer:\u003eIf[k==m,1,k+1])];",
				"Table[Length[Select[Union[Sort[Sort/@Partition[#,2,1,1]]\u0026/@Permutations[Range[n]]],#==First[Sort[Table[Nest[rotgra[#,n]\u0026,#,j],{j,n}]]]\u0026]],{n,8}] (* _Gus Wiseman_, Mar 02 2019 *)"
			],
			"program": [
				"(PARI) a(n)={if(n\u003c3, n\u003e=0, (if(n%2, 0, (n/2-1)!*2^(n/2-2)) + sumdiv(n, d, eulerphi(n/d)^2 * d! * (n/d)^d)/n^2)/2)} \\\\ _Andrew Howroyd_, Aug 17 2019"
			],
			"xref": [
				"Cf. A000940. Bisections give A094154, A094155.",
				"For star polygons see A231091.",
				"Cf. A000031, A002619, A002866, A006125, A008965, A059966, A060223, A192332, A275527, A323858, A323870, A324461."
			],
			"keyword": "nonn,nice,easy",
			"offset": "1,4",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from Pab Ter (pabrlos(AT)yahoo.com), May 05 2004",
				"Added a(1) = 1 and a(2) = 1 by _Gus Wiseman_, Mar 02 2019"
			],
			"references": 16,
			"revision": 52,
			"time": "2019-08-19T16:44:20-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}