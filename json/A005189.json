{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005189",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5189,
			"id": "M2976",
			"data": "1,1,1,3,14,85,626,5387,52882,582149,7094234,94730611,1374650042,21529197077,361809517954,6492232196699,123852300381986,2502521367966277,53379537613065002,1198434678728086019,28245547605034208074,697186985180529270101",
			"name": "Number of n-term 2-sided generalized Fibonacci sequences.",
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/A005189/b005189.txt\"\u003eTable of n, a(n) for n = 0..250\u003c/a\u003e",
				"C. Banderier, H.-K. Hwang, V. Ravelomanana and V. Zacharovas, \u003ca href=\"http://lipn.univ-paris13.fr/~banderier/Papers/mis-n-to-the-logn.pdf\"\u003eAnalysis of an exhaustive search algorithm in random graphs and the n^{c logn}-asymptotics\u003c/a\u003e, SIAM J. Discrete Math., 28(1), 342-371. (30 pages), DOI:10.1137/130916357. - From _N. J. A. Sloane_, Dec 23 2012",
				"Peter C. Fishburn, Peter C. Marcus-Roberts, Fred S. Roberts, \u003ca href=\"http://dx.doi.org/10.1137/0401034\"\u003eUnique finite difference measurement\u003c/a\u003e, SIAM J. Discrete Math. 1 (1988), no. 3, 334-354.",
				"P. C. Fishburn, A. M. Odlyzko and F. S. Roberts, \u003ca href=\"http://www.fq.math.ca/Scanned/27-4/fishburn.pdf\"\u003e2-sided generalized Fibonacci sequences\u003c/a\u003e, Fib. Quart., 27 (1989), 352-361.",
				"Rui-Li Liu, Feng-Zhen Zhao, \u003ca href=\"https://www.emis.de/journals/JIS/VOL21/Liu/liu19.html\"\u003eNew Sufficient Conditions for Log-Balancedness, With Applications to Combinatorial Sequences\u003c/a\u003e, J. Int. Seq., Vol. 21 (2018), Article 18.5.7."
			],
			"formula": [
				"If n \u003c= 2 then a(n) = 1 otherwise a(n) = 2*(n-1)*a(n-1)-(n-2)^2*a(n-2).",
				"E.g.f.: (e*Ei(1/(x-1)) - e*Ei(-1)-1)/(e^(x/(x-1))*(x-1)), where Ei is the exponential integral function. - _Jean-François Alcover_, Sep 05 2015, after Fishburn et al.",
				"0 = a(n)*(-24*a(n+2) + 99*a(n+3) - 78*a(n+4) + 17*a(n+5) - a(n+6)) + a(n+1)*(-15*a(n+2) + 84*a(n+3) - 51*a(n+4) + 6*a(n+5)) + a(n+2)*(-6*a(n+2) + 34*a(n+3) - 15*a(n+4)) + a(n+3)*(+10*a(n+3)) for all n in Z. - _Michael Somos_, Dec 02 2016"
			],
			"example": [
				"G.f. = 1 + x + x^2 + 3*x^3 + 14*x^4 + 85*x^5 + 626*x^6 + 5387*x^7 + ..."
			],
			"maple": [
				"f:=proc(n) option remember;",
				"if n \u003c= 2 then 1 else 2*(n-1)*f(n-1)-(n-2)^2*f(n-2); fi; end;",
				"[seq(f(n),n=0..20)]; # _N. J. A. Sloane_, Jul 10 2015"
			],
			"mathematica": [
				"$Assumptions = Element[x, Reals]; F[x_] := (E*ExpIntegralEi[1/(x-1)] - E*ExpIntegralEi[-1]-1)/(E^(x/(x-1))*(x-1)); Join[{1}, CoefficientList[ Normal[Series[F[x], {x, 0, 18}]], x]*Range[0, 18]!] (* _Jean-François Alcover_, Sep 05 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = if(n\u003c3, n\u003e=0, 2*(n-1)*a(n-1) - (n-2)^2*a(n-2))}; /* _Michael Somos_, Dec 02 2016 */"
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_, _Simon Plouffe_",
			"ext": [
				"More terms from _Vladeta Jovovic_, Sep 05 2005"
			],
			"references": 1,
			"revision": 38,
			"time": "2018-10-11T05:24:59-04:00",
			"created": "1991-05-20T03:00:00-04:00"
		}
	]
}