{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A339856",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 339856,
			"data": "4,6,9,9,12,16,16,20,25,25,30,36,25,35,49,25,40,64,36,42,49,49,56,64,49,63,81,49,70,100,49,77,121,64,72,81,64,88,121,81,90,100,81,99,121,81,117,169,81,126,196,100,110,121,100,130,169,121,132,144,121,143,169",
			"name": "Primitive triples for integer-sided triangles whose sides a \u003c b \u003c c form a geometric progression.",
			"comment": [
				"These triangles are called \"geometric triangles\" in Project Euler problem 370 (see link).",
				"The triples are displayed in increasing lexicographic order (a, b, c).",
				"Equivalently: triples of integer-sided triangles such that b^2 = a*c with a \u003c c and gcd(a, c) = 1.",
				"When a \u003c b \u003c c are in geometric progression with b = a*q, c = b*q, q is the constant, then 1 \u003c q \u003c (1+sqrt(5))/2 = phi = A001622 = 1.6180... (this bound is used in Maple code).",
				"For each triple (a, b, c), there exists (r, s), 0 \u003c r \u003c s such that a = r^2, b = r*s, c = s^2, q = s/r.",
				"Angle C \u003c 90° if 1 \u003c q \u003c sqrt(phi) and angle C \u003e 90° if sqrt(phi) \u003c q \u003c phi with sqrt(phi) = A139339 = 1.2720...",
				"For k \u003e= 2, each triple (a, b, c) of the form (k^2, k*(k+1), (k+1)^2) is (A008133(3k+1), A008133(3k+2), A008133(3k+3)).",
				"Three geometrical properties about these triangles:",
				"  1) The sinus satisfy sin^2(B) = sin(A) * sin(C) with sin(A) \u003c sin(B) \u003c sin(C) that form a geometric progression.",
				"  2) The heights satisfy h_b^2 = h_a * h_c with h_c \u003c h_b \u003c h_a that form a geometric progression.",
				"  3) b^2 = 2 * R * h_b, with R = circumradius of the triangle ABC."
			],
			"link": [
				"David A. Corneth, \u003ca href=\"/A339856/b339856.txt\"\u003eTable of n, a(n) for n = 1..10002\u003c/a\u003e",
				"Project Euler, \u003ca href=\"https://projecteuler.net/problem=370\"\u003eProblem 370: Geometric Triangles\u003c/a\u003e."
			],
			"example": [
				"The smallest such triangle is (4, 6, 9) with 4*9 = 6^2.",
				"There exist four triangles with small side = 49 corresponding to triples (49, 56, 64), (49, 63, 81), (49, 70, 100) and (49, 77, 121).",
				"The table begins:",
				"   4,  6,  9;",
				"   9, 12, 16;",
				"  16, 20, 25;",
				"  25, 30, 36;",
				"  25, 35, 49;",
				"  25, 40, 64;",
				"  36, 42, 49;",
				"  ..."
			],
			"maple": [
				"for a from 1 to 300 do",
				"for b from a+1 to floor((1+sqrt(5))/2 * a) do",
				"for c from b+1 to floor((1+sqrt(5))/2 * b) do",
				"k:=a*c;",
				"if k=b^2 and igcd(a,b,c)=1 then print(a,b,c); end if;",
				"end do;",
				"end do;",
				"end do;"
			],
			"program": [
				"(PARI) lista(nn) = {my(phi = (1+sqrt(5))/2); for (a=1, nn, for (b=a+1, floor(a*phi), for (c=b+1, floor(b*phi), if ((a*c == b^2) \u0026\u0026 (gcd([a,b,c])==1), print([a,b,c])););););} \\\\ _Michel Marcus_, Dec 25 2020",
				"(PARI) upto(n) = my(res=List(), phi = (sqrt(5)+1) / 2); for(i = 2, sqrtint(n), for(j = i+1, (i*phi)\\1, if(gcd(i, j)==1, listput(res, [i^2, i*j, j^2])))); concat(Vec(res)) \\\\ _David A. Corneth_, Dec 25 2020"
			],
			"xref": [
				"Cf. A339857 (smallest side), A339858 (middle side), A339859 (largest side), A339860 (perimeter).",
				"Cf. A336755 (similar for sides in arithmetic progression).",
				"Cf. A335893 (similar for angles in arithmetic progression).",
				"Cf. A001622 (phi), A139339 (sqrt(phi)), A008133."
			],
			"keyword": "nonn,easy,tabf",
			"offset": "1,1",
			"author": "_Bernard Schott_, Dec 19 2020",
			"ext": [
				"Data corrected by _David A. Corneth_, Dec 25 2020"
			],
			"references": 5,
			"revision": 44,
			"time": "2021-02-21T05:37:56-05:00",
			"created": "2020-12-25T10:56:38-05:00"
		}
	]
}