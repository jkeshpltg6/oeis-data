{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A075834",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 75834,
			"data": "1,1,1,2,7,34,206,1476,12123,111866,1143554,12816572,156217782,2057246164,29111150620,440565923336,7101696260883,121489909224618,2198572792193786,41966290373704332,842706170872913634",
			"name": "Coefficients of power series A(x) such that n-th term of A(x)^n = n! x^(n-1) for n \u003e 0.",
			"comment": [
				"Also, number of stablized-interval-free permutations on [n] (see Callan link).",
				"Coefficients in the series reversal of the asymptotic expansion of exp(-x)*Ei(x) for x -\u003e inf, where Ei(x) is the exponential integral. - _Vladimir Reshetnikov_, Apr 24 2016"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A075834/b075834.txt\"\u003eTable of n, a(n) for n = 0..100\u003c/a\u003e",
				"F. Ardila, F. Rincón and L. Williams, \u003ca href=\"http://arxiv.org/abs/1308.2698\"\u003ePositroids and non-crossing partitions\u003c/a\u003e, arXiv preprint arXiv:1308.2698 [math.CO], 2013.",
				"Daniel Birmajer, Juan B. Gil and Michael D. Weiner, \u003ca href=\"https://arxiv.org/abs/1803.07727\"\u003eA family of Bell transformations\u003c/a\u003e, arXiv:1803.07727 [math.CO], 2018.",
				"David Callan, \u003ca href=\"http://arXiv.org/abs/math.CO/0310157\"\u003eCounting stabilized-interval-free permutations\u003c/a\u003e, arXiv:math/0310157 [math.CO], 2003.",
				"David Callan, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL7/Callan/callan91.html\"\u003eCounting Stabilized-Interval-Free Permutations\u003c/a\u003e, Journal of Integer Sequences, Vol. 7 (2004), Article 04.1.8.",
				"Colin Defant and Nathan Williams, \u003ca href=\"https://arxiv.org/abs/2106.05471\"\u003eCoxeter Pop-Tsack Torsing\u003c/a\u003e, arXiv:2106.05471 [math.CO], 2021.",
				"Jesse Elliott, \u003ca href=\"https://arxiv.org/abs/1809.06633\"\u003eAsymptotic expansions of the prime counting function\u003c/a\u003e, arXiv:1809.06633 [math.NT], 2018."
			],
			"formula": [
				"a(0)=a(1)=1, a(n) = (n-1)*a(n-1) + Sum_{j=2..n-2}(j-1)*a(j)*a(n-j), n \u003e= 2. - _David Callan_",
				"G.f.: A(x) = x/series_reversion(x*G(x)); G(x) = A(x*G(x)); A(x) = G(x/A(x)); where G(x) is the g.f. of the factorials (A000142). - _Paul D. Hanna_, Jul 09 2006",
				"G.f.: A(x) = 1 + x/(1 - x*A'(x)/A(x)) = 1 + x/(1-x - x^2*d/dx[(A(x) - 1)/x)]).",
				"G.f.: A(x) = 1 + x*F(x) where F(x) satisfies F(x) = 1 + x*F(x) + x^2*F(x)*F'(x) and F'(x) = d/dx F(x). - _Paul D. Hanna_, Sep 02 2008",
				"a(n) ~ exp(-1) * n! * (1 - 1/n - 5/(2*n^2) - 32/(3*n^3) - 1643/(24*n^4) - 23017/(40*n^5) - 4215719/(720*n^6)). - _Vaclav Kotesovec_, Feb 22 2014",
				"A003319(n+1) = coefficient of x^n in A(x)^n. - _Michael Somos_, Feb 23 2014"
			],
			"example": [
				"At n=7, the 7th term of A(x)^7 is 7! x^6, as demonstrated by A(x)^7 = 1 + 7 x + 28 x^2 + 91 x^3 + 294 x^4 + 1092 x^5 + 5040 x^6 + 29093 x^7 + 203651 x^8 + ... .",
				"A(x) = 1 + x + x^2 + 2*x^3 + 7*x^4 + 34*x^5 + 206*x^6 + ... = x/series_reversion(x + x^2 + 2*x^3 + 6*x^4 + 24*x^5 + 120*x^6 + ...).",
				"Related expansions:",
				"log(A(x)) = x + x^2/2 + 4*x^3/3 + 21*x^4/4 + 136*x^5/5 + 1030*x^6/6 + ...;",
				"1 - x/(A(x) - 1) = x + x^2 + 4*x^3 + 21*x^4 + 136*x^5 + 1030*x^6 +...;",
				"(d/dx)((A(x) - 1)/x) = 1 + 4*x + 21*x^2 + 136*x^3 + 1030*x^4 + ... ."
			],
			"mathematica": [
				"a = ConstantArray[0,20]; a[[1]]=1; a[[2]]=1; a[[3]]=2; Do[a[[n]] = (n-1)*a[[n-1]] + Sum[(j-1)*a[[j]]*a[[n-j]],{j,2,n-2}],{n,4,20}]; Flatten[{1,a}] (* _Vaclav Kotesovec_ after _David Callan_, Feb 22 2014 *)",
				"InverseSeries[Series[Exp[-x] ExpIntegralEi[x], {x, Infinity, 20}]][[3]] (* _Vladimir Reshetnikov_, Apr 24 2016 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c0,0,if(n\u003c=1,1,(n-1)*a(n-1)+sum(j=2,n-2,(j-1)*a(j)*a(n-j));))",
				"(PARI) a(n)=Vec(x/serreverse(x*Ser(vector(n+1,k,(k-1)!))))[n+1] \\\\ _Paul D. Hanna_, Jul 09 2006",
				"(PARI) {a(n)=local(A=1+x+x*O(x^n));for(i=1,n,A=1+x/(1-x*deriv(A)/A));polcoeff(A,n)}",
				"(PARI) {a(n)=local(F=1+x*O(x^n)); for(i=0,n,F=1+x*F+x^2*F*deriv(F)+x*O(x^n));polcoeff(1+x*F,n)} \\\\ _Paul D. Hanna_, Sep 02 2008"
			],
			"xref": [
				"Cf. A209881, A091063, A084938.",
				"Cf. A003319."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_Paul D. Hanna_, Oct 14 2002, Jul 30 2008",
			"ext": [
				"More terms from _David Wasserman_, Jan 26 2005",
				"Minor edits by _Vaclav Kotesovec_, Aug 01 2015"
			],
			"references": 19,
			"revision": 55,
			"time": "2021-09-28T19:57:20-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}