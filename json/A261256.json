{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A261256",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 261256,
			"data": "4,24,72,160,432,896,2592,5632,12800,26624,61440,124416,278528,622592,1376256,2949120,5971968,12058624,25690112,60817408,130023424",
			"name": "Let S_k denote the sequence of numbers j such that A001222(j) - A001221(j) = k. Then a(n) is the n-th term of S_n.",
			"comment": [
				"S_0 would correspond to the squarefree numbers (A005117), that is, numbers j such that A001222(j) = A001221(j). Note that S_0 is excluded from the scheme. - _Michel Marcus_, Sep 21 2015",
				"a(n) = A257851(n,n-1). - _Reinhard Zumkeller_, Nov 29 2015"
			],
			"link": [
				"Charlie Neder, \u003ca href=\"/A261256/b261256.txt\"\u003eTable of n, a(n) for n = 1..500\u003c/a\u003e"
			],
			"formula": [
				"a(n+1) \u003e 2*a(n).",
				"a(n) \u003e= 2^prime(n) for n \u003c 5.",
				"a(n) = b(n)*2^(n+1), where b(n) consists of the values of k/2^excess(k) over odd k, sorted in ascending order. In particular, a(n) \u003c= prime(n)*2^(n+1), with equality only when n = 2. - _Charlie Neder_, Jan 31 2019"
			],
			"example": [
				"For n = 1, S_1 = {4, 9, 12, 18, 20, 25, ...}, so a(1) = S_1(1) = 4.",
				"For n = 2, S_2 = {8, 24, 27, 36, 40, 54, ...}, so a(2) = S_2(2) = 24.",
				"For n = 3, S_3 = {16, 48, 72, 80, 81, 108, ...}, so a(3) = S_3(3) = 72.",
				"For n = 4, S_4 = {32, 96, 144, 160, 216, 224, ...}, so a(4) = S_4(4) = 160.",
				"For n = 5, S_5 = {64, 192, 288, 320, 432, 448, ...}, so a(5) = S_5(5) = 432."
			],
			"mathematica": [
				"OutSeq = {}; For[i = 1, i \u003c= 16, i++, l = Select[Range[10^2*2^i], PrimeOmega[#] - PrimeNu[#] == i \u0026]; AppendTo[OutSeq, l[[i]]]]; OutSeq"
			],
			"program": [
				"(PARI) a(n) = {ik = 1; nbk = 0; while (nbk != n, ik++; if (bigomega(ik) == omega(ik) + n, nbk++);); ik;} \\\\ _Michel Marcus_, Oct 06 2015",
				"(Haskell)",
				"a261256 n = a257851 n (n - 1)  -- _Reinhard Zumkeller_, Nov 29 2015"
			],
			"xref": [
				"Cf. A001221, A001222.",
				"Cf. A060687, A195086, A195087, A195088, A195089, A195090, A195091, A195092, A195093.",
				"Cf. A046660, A257851, A264959."
			],
			"keyword": "nonn,more",
			"offset": "1,1",
			"author": "_Carlos Eduardo Olivieri_, Aug 12 2015",
			"ext": [
				"a(17)-a(21) from _Jon E. Schoenfield_, Sep 12 2015"
			],
			"references": 13,
			"revision": 33,
			"time": "2019-02-01T01:34:22-05:00",
			"created": "2015-11-27T21:04:08-05:00"
		}
	]
}