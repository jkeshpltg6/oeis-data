{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A060739",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 60739,
			"data": "1,1,-2,-36,7200,17640000,-560105280000,-239102222768640000,1408147589778024775680000,116620600756651855983415296000000,-137839975629646325813680872620851200000000,-2352568589682795058651211199786427114330521600000000",
			"name": "a(n) = (-1)^(n(n-1)/2) * Product_{k=0,...,n-1} (n+k-1)!/((k!)^2 * (n-1-k)!).",
			"comment": [
				"Let A_n be the matrix of size n X n defined by: A_n[i,j] = 1/(binomial coefficient i+j-2 over i-1) = 1/C(i+j-2,i-1) where 1 \u003c= i,j \u003c= n. The diagonals of this matrix are the reciprocals of the entries in the Pascal triangle. Then a(n) = 1/det(A_n) = det((A_n)^(-1)).",
				"From the formula for a(n) it follows that the determinant of (A_n)^(-1) is an integer. By inspecting the values of (A_n)^(-1) for small values of n it looks like (A_n)^(-1) is actually a matrix of integers but I do not have a proof of this fact.",
				"Let M_n be the n X n matrix with M_n(i,j)=i/(i+j); then |a(n-1)|=1/det(M_n). - _Benoit Cloitre_, Apr 21 2002",
				"Also related to the multinomial coefficients (i+j)!/i!/j! : abs(a(n))=(1/detQ_n-1) where Q_n is the n X n matrix q(i,j)=i!j!/(i+j)! - _Benoit Cloitre_, May 30 2002",
				"From _Alexander Adamchuk_, Nov 14 2009: (Start)",
				"Also a(n) = (-1)^(n(n-1)/2) * Product[ Binomial[2k,k]^2/2, {k,1,n-1} ].",
				"It is simpler definition of a(n).",
				"It follows from the observation that Sqrt[ Abs[ a(n+1)/a(n)/2 ] ] = {1, 3, 10, 35, 126, 462, ...} = C(2n+1, n+1) = A001700. (End)"
			],
			"link": [
				"Harry J. Smith, \u003ca href=\"/A060739/b060739.txt\"\u003eTable of n, a(n) for n=0,...,43\u003c/a\u003e",
				"T. M. Richardson, \u003ca href=\"http://arxiv.org/abs/1405.6315\"\u003eThe Reciprocal Pascal Matrix\u003c/a\u003e, arXiv preprint arXiv:1405.6315 [math.CO], 2014.",
				"Doron Zeilberger, \u003ca href=\"https://arxiv.org/abs/1405.6315\"\u003eReverend Charles to the aid of Major Percy and Fields-Medalist Enrico\u003c/a\u003e, arXiv:1405.6315 [math.CO], 2014.",
				"Doron Zeilberger, \u003ca href=\"http://www.jstor.org/stable/2974719\"\u003eReverend Charles to the aid of Major Percy and Fields-Medalist Enrico\u003c/a\u003e, Amer. Math. Monthly 103 (1996), 501-502."
			],
			"formula": [
				"If Multinomial[a, b, c] denotes the multinomial coefficient (a+b+c)! / (a! * b! * c!) (which is an integer) then : a(n) = (-1)^(n(n-1)/2) * Product k=0, ..., n-1 Multinomial[k, k, n-1-k] = (-1)^(n(n-1)/2) * product k=0, ..., n-1 (n+k-1)!/((k!)^2 * (n-1-k)!)",
				"a(n) = (-1)^(n(n-1)/2) * Product[ Binomial[2k,k]^2/2, {k,1,n-1} ]. [_Alexander Adamchuk_, Nov 14 2009]",
				"|a(n)| = A163085(2*(n-1))/(n-1)! for n \u003e 0. - _Peter Luschny_, Sep 18 2012",
				"|a(n)| ~ A^3 * 2^(2*n^2 - 3*n + 5/12) * exp(n - 1/4) / (Pi^n * n^(n - 1/4)), where A is the Glaisher-Kinkelin constant A074962. - _Vaclav Kotesovec_, May 19 2020"
			],
			"example": [
				"Here is the matrix A_4 for n=4: [1, 1, 1, 1; 1, 1/2, 1/3, 1/4; 1, 1/3, 1/6, 1/10; 1, 1/4, 1/10, 1/20]; a(4) = 7200 because det(A_4) = 1/7200"
			],
			"maple": [
				"A060739 := n-\u003e(-1)^(n*(n-1)/2) * mul( (n+k-1)!/((k!)^2 * (n-1-k)!), k=0..n-1);"
			],
			"mathematica": [
				"a[n_] := (-1)^(n (n - 1)/2)*Product[ Multinomial[k, k, n - 1 - k], {k, 0, n - 1}]; Table[a[n], {n, 0, 11}] (* _Jean-François Alcover_, Dec 08 2011, after first formula *)"
			],
			"program": [
				"(PARI) for(n=1,15,print1(1/matdet(matrix(n,n,i,j,i/(j+i))),\",\")) \\\\ See Cloitre's comment",
				"(PARI) { for (n=0, 43, if (n\u003c2, a=1, a=(-1)^(n\\2)/matdet(matrix(n-1, n-1, i, j, i/(j+i)))); write(\"b060739.txt\", n, \" \", a); ) } \\\\ _Harry J. Smith_, Jul 10 2009",
				"(Sage)",
				"def A060739(n): return (-1)^(n//2)*A163085(2*(n-1))/factorial(n-1) if n \u003e 0 else 1",
				"[A060739(i) for i in (0..11)] # _Peter Luschny_, Sep 18 2012"
			],
			"xref": [
				"Cf. A005249, A067689.",
				"Cf. A001700. [_Alexander Adamchuk_, Nov 14 2009]"
			],
			"keyword": "easy,sign,nice",
			"offset": "0,3",
			"author": "Noam Katz (noamkj(AT)hotmail.com), Apr 25 2001",
			"references": 4,
			"revision": 29,
			"time": "2020-05-19T03:50:23-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}