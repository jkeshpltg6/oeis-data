{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A036499",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 36499,
			"data": "1,2,12,15,35,40,70,77,117,126,176,187,247,260,330,345,425,442,532,551,651,672,782,805,925,950,1080,1107,1247,1276,1426,1457,1617,1650,1820,1855,2035,2072,2262,2301,2501,2542,2752,2795,3015,3060,3290,3337,3577,3626",
			"name": "Numbers of the form n*(n+1)/6 for n = 2 or 3 modulo 6.",
			"comment": [
				"Numbers with an odd number of partitions with an extra odd partition; coefficient of z^p in Product_{n \u003e= 1}(1-z^n) has coefficient (-1).",
				"n such that the number of partitions of n into distinct parts with an odd number of parts exceed by 1 the number of partitions of n into distinct parts with an even number of parts. [Euler's 1754/55 pentagonal number Theorem, see, e.g. the Freitag-Busam reference (in German). This reference from - _Wolfdieter Lang_, Jan 18 2016]",
				"In formal power series, A010815=(product(1-x^k),k\u003e0), ranks of coefficients -1. (A001318=ranks of nonzero (1 or -1) in A010815=ranks of odds terms in A000009).",
				"Quasipolynomial of order 2. [_Charles R Greathouse IV_, Dec 08 2011]",
				"Union of A033568 and A033570. - _Ray Chandler_, Dec 09 2011"
			],
			"reference": [
				"E. Freitag and R. Busam, Funktionentheorie 1, Springer, Vierte Auflage, 2006, p. 410."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A036499/b036499.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,2,-2,-1,1)."
			],
			"formula": [
				"a(n) = (3*n*n-5*n+2)/2 + (2*n-1)*(n mod 2). - _Frank Ellermann_, Mar 16 2002",
				"G.f.: (1+x+8*x^2+x^3+x^4)/((1-x)^3*(1+x)^2). - _Ray Chandler_, Dec 09 2011",
				"Bisection: a(2*k+1) = A001318(1+4*k) = (2*k+1)*(3*k+1) = A033570(k), a(2*(k+1)) = A001318(2+4*k) = (2*k+1)*(3*k+2) = A033568(k+1), k \u003e= 0. - _Wolfdieter Lang_, Jan 18 2016",
				"a(n) = a(n-1)+2*a(n-2)-2*a(n-3)-a(n-4)+a(n-5) for n\u003e5. - _Wesley Ivan Hurt_, Jan 18 2016"
			],
			"maple": [
				"seq(seq((6*k+i)*(6*k+i+1)/6,i=2..3),k=0..50); # _Robert Israel_, Jan 18 2016"
			],
			"mathematica": [
				"Table[ 1/8*(3 + (-1)^k - 6*k)*(1 + (-1)^k - 2*k), {k, 64} ]",
				"LinearRecurrence[{1,2,-2,-1,1},{1,2,12,15,35},50] (* or *)",
				"CoefficientList[Series[(1+x+8x^2+x^3+x^4)/((1-x)^3(1+x)^2),{x,0,100}],x] (* or *)",
				"Table[(2n+1)(3n+{1,2}),{n,0,24}]//Flatten (* _Ray Chandler_, Dec 09 2011 *)"
			],
			"program": [
				"(PARI) a(n)=n*(3*n-5)/2+1+n%2*(2*n-1) \\\\ _Charles R Greathouse IV_, Dec 08 2011",
				"(MAGMA) [(3*n*n-5*n+2)/2+(2*n-1)*(n mod 2): n in [1..50]]; // _Vincenzo Librandi_, Jan 19 2016"
			],
			"xref": [
				"Cf. A000009, A001318, A010815, A033568, A033570, A036498."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Wouter Meeussen_",
			"ext": [
				"Better description from Claude Lenormand (claude.lenormand(AT)free.fr), Feb 12 2001",
				"Edited by _Ray Chandler_, Dec 09 2011"
			],
			"references": 6,
			"revision": 39,
			"time": "2016-01-29T17:11:20-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}