{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A238343",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 238343,
			"data": "1,1,0,2,0,0,3,1,0,0,5,3,0,0,0,7,9,0,0,0,0,11,19,2,0,0,0,0,15,41,8,0,0,0,0,0,22,77,29,0,0,0,0,0,0,30,142,81,3,0,0,0,0,0,0,42,247,205,18,0,0,0,0,0,0,0,56,421,469,78,0,0,0,0,0,0,0,0,77,689,1013,264,5,0,0,0,0,0,0,0,0,101,1113,2059,786,37,0,0,0,0,0,0,0,0,0",
			"name": "Triangle T(n,k) read by rows: T(n,k) is the number of compositions of n with k descents, n\u003e=0, 0\u003c=k\u003c=n.",
			"comment": [
				"Counting ascents gives the same triangle.",
				"For n \u003e 0, also the number of compositions of n with k + 1 maximal weakly increasing runs. - _Gus Wiseman_, Mar 23 2020"
			],
			"link": [
				"Joerg Arndt and Alois P. Heinz, \u003ca href=\"/A238343/b238343.txt\"\u003eRows n = 0..140, flattened\u003c/a\u003e"
			],
			"example": [
				"Triangle starts:",
				"00:    1;",
				"01:    1,    0;",
				"02:    2,    0,    0;",
				"03:    3,    1,    0,    0;",
				"04:    5,    3,    0,    0,   0;",
				"05:    7,    9,    0,    0,   0, 0;",
				"06:   11,   19,    2,    0,   0, 0, 0;",
				"07:   15,   41,    8,    0,   0, 0, 0, 0;",
				"08:   22,   77,   29,    0,   0, 0, 0, 0, 0;",
				"09:   30,  142,   81,    3,   0, 0, 0, 0, 0, 0;",
				"10:   42,  247,  205,   18,   0, 0, 0, 0, 0, 0, 0;",
				"11:   56,  421,  469,   78,   0, 0, 0, 0, 0, 0, 0, 0;",
				"12:   77,  689, 1013,  264,   5, 0, 0, 0, 0, 0, 0, 0, 0;",
				"13:  101, 1113, 2059,  786,  37, 0, 0, 0, 0, 0, 0, 0, 0, 0;",
				"14:  135, 1750, 4021, 2097, 189, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0;",
				"15:  176, 2712, 7558, 5179, 751, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0;",
				"...",
				"From _Gus Wiseman_, Mar 23 2020: (Start)",
				"Row n = 5 counts the following compositions:",
				"  (5)          (3,2)",
				"  (1,4)        (4,1)",
				"  (2,3)        (1,3,1)",
				"  (1,1,3)      (2,1,2)",
				"  (1,2,2)      (2,2,1)",
				"  (1,1,1,2)    (3,1,1)",
				"  (1,1,1,1,1)  (1,1,2,1)",
				"               (1,2,1,1)",
				"               (2,1,1,1)",
				"(End)"
			],
			"maple": [
				"b:= proc(n, i) option remember; `if`(n=0, 1, expand(",
				"       add(b(n-j, j)*`if`(j\u003ci, x, 1), j=1..n)))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=0..n))(b(n, 0)):",
				"seq(T(n), n=0..20);"
			],
			"mathematica": [
				"b[n_, i_] := b[n, i] = If[n == 0, 1, Sum[b[n-j, j]*If[j\u003ci, x, 1], {j, 1, n}]]; T[n_] := Function[{p}, Table[Coefficient[p, x, i], {i, 0, n}]][b[n, 0]]; Table[T[n], {n, 0, 20}] // Flatten (* _Jean-François Alcover_, Jan 08 2015, translated from Maple *)",
				"Table[Length[Select[Join@@Permutations/@IntegerPartitions[n],n==0||Length[Split[#,LessEqual]]==k+1\u0026]],{n,0,9},{k,0,n}] (* _Gus Wiseman_, Mar 23 2020 *)"
			],
			"xref": [
				"Columns k=0-10 give: A000041, A241626, A241627, A241628, A241629, A241630, A241631, A241632, A241633, A241634, A241635.",
				"T(3n,n) gives A000045(n+1).",
				"T(3n+1,n) = A136376(n+1).",
				"Row sums are A011782.",
				"Compositions by length are A007318.",
				"The version for co-runs or levels is A106356.",
				"The case of partitions (instead of compositions) is A133121.",
				"The version for runs is A238279.",
				"The version without zeros is A238344.",
				"The version for weak ascents is A333213.",
				"Cf. A008284, A124765, A124766, A332875, A333215."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_Joerg Arndt_ and _Alois P. Heinz_, Feb 25 2014",
			"references": 28,
			"revision": 22,
			"time": "2020-03-25T06:54:03-04:00",
			"created": "2014-02-26T08:38:58-05:00"
		}
	]
}