{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A243148",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 243148,
			"data": "1,0,1,0,0,1,0,0,0,1,0,1,0,0,1,0,0,1,0,0,1,0,0,0,1,0,0,1,0,0,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,1,0,1,0,0,1,0,0,1,0,0,1,0,1,0,0,1,0,0,1,0,0,0,1,0,1,0,0,1,0,0,1,0,0,0,1,1,0,1,0,0,1,0,0,1,0,0,1,0,1,1,0,1,0,0,1,0,0,1,0,0,0,1,0,1,1,0,1,0,0,1,0,0,1",
			"name": "Triangle read by rows: T(n,k) = number of partitions of n into k nonzero squares; n \u003e= 0, 0 \u003c= k \u003c= n.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A243148/b243148.txt\"\u003eRows n = 0..200, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = [x^n y^k] 1/Product_{j\u003e=1} (1-y*x^A000290(j)).",
				"Sum_{k=1..n} k * T(n,k) = A281541(n).",
				"Sum_{k=1..n} n * T(n,k) = A276559(n)."
			],
			"example": [
				"T(20,5) = 2 = #{ (16,1,1,1,1), (4,4,4,4,4) } since 20 = 4^2 + 4 * 1^2 = 5 * 2^2.",
				"Triangle T(n,k) begins:",
				"  1;",
				"  0, 1;",
				"  0, 0, 1;",
				"  0, 0, 0, 1;",
				"  0, 1, 0, 0, 1;",
				"  0, 0, 1, 0, 0, 1;",
				"  0, 0, 0, 1, 0, 0, 1;",
				"  0, 0, 0, 0, 1, 0, 0, 1;",
				"  0, 0, 1, 0, 0, 1, 0, 0, 1;",
				"  0, 1, 0, 1, 0, 0, 1, 0, 0, 1;",
				"  0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 1;",
				"  0, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 1;",
				"  0, 0, 0, 1, 1, 0, 1, 0, 0, 1, 0, 0, 1;",
				"  (...)"
			],
			"maple": [
				"b:= proc(n, i, t) option remember; `if`(n=0, `if`(t=0, 1, 0),",
				"      `if`(i\u003c1 or t\u003c1, 0, b(n, i-1, t)+",
				"      `if`(i^2\u003en, 0, b(n-i^2, i, t-1))))",
				"    end:",
				"T:= (n, k)-\u003e b(n, isqrt(n), k):",
				"seq(seq(T(n, k), k=0..n), n=0..14);",
				"# second Maple program:",
				"b:= proc(n, i) option remember; `if`(n=0, 1, `if`(i\u003c1, 0,",
				"      b(n, i-1)+(s-\u003e `if`(s\u003en, 0, expand(x*b(n-s, i))))(i^2)))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=0..n))(b(n, isqrt(n))):",
				"seq(T(n), n=0..14);  # _Alois P. Heinz_, Oct 30 2021"
			],
			"mathematica": [
				"b[n_, i_, k_, t_] := b[n, i, k, t] = If[n == 0, If[t == 0, 1, 0], If[i \u003c 1 || t \u003c 1, 0, b[n, i-1, k, t] + If[i^2 \u003e n, 0, b[n-i^2, i, k, t-1]]]]; T[n_, k_] := b[n, Sqrt[n] // Floor, k, k]; Table[Table[T[n, k], {k, 0, n}], {n, 0, 14}] // Flatten (* _Jean-François Alcover_, Jun 06 2014, after _Alois P. Heinz_ *)",
				"T[n_, k_] := Count[PowersRepresentations[n, k, 2], r_ /; FreeQ[r, 0]]; T[0, 0] = 1; Table[T[n, k], {n, 0, 14}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Feb 19 2016 *)"
			],
			"program": [
				"(PARI) T(n,k,L=n)=if(n\u003ek*L^2, 0, k\u003en-3, k==n, k\u003c2, issquare(n,\u0026n) \u0026\u0026 n\u003c=L*k, k\u003en-6, n-k==3, L=min(L,sqrtint(n-k+1)); sum(r=0,min(n\\L^2,k-1),T(n-r*L^2,k-r,L-1), n==k*L^2)) \\\\ _M. F. Hasler_, Aug 03 2020"
			],
			"xref": [
				"Columns k = 0..10 give: A000007, A010052 (for n\u003e0), A025426, A025427, A025428, A025429, A025430, A025431, A025432, A025433, A025434.",
				"Row sums give A001156.",
				"T(2n,n) gives A111178.",
				"T(n^2,n) gives A319435.",
				"Cf. A000290, A276559, A281541, A341040.",
				"T(n,k) = 1 for n in A025284, A025321, A025357, A294675, A295670, A295797 (for k = 2..7, respectively)."
			],
			"keyword": "nonn,tabl",
			"offset": "0",
			"author": "_Alois P. Heinz_, May 30 2014",
			"references": 26,
			"revision": 51,
			"time": "2021-10-30T17:57:05-04:00",
			"created": "2014-05-30T20:18:35-04:00"
		}
	]
}