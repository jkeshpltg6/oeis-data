{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A235124",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 235124,
			"data": "1,1,1,2,2,2,2,2,2,2,2,2,2,3,2,2,3,2,3,3,3,3,2,3,3,3,2,3,3,3,2,3,3,3,3,2,2,3,3,3,3,2,3,4,3,3,2,3,4,3,3,2,3,4,2,3,3,3,3,3,3,3,2,4,4,2,3,4,3,3,3,3,3,4,2,4,3,3,3,4,3,3,3,4,3,4,3,2,4,3,3,4,4,3,2,4,3,4,4",
			"name": "The rounded harmonic index of the rooted tree with Matula number n (n\u003e=2).",
			"comment": [
				"The harmonic index of a graph is defined as the summation of 2/(d(u)+d(v)) over all edges uv of G, where d(w) denotes the degree of the vertex w.",
				"The Matula number of a rooted tree can be defined in the following recursive manner: to the one-vertex tree there corresponds the number 1; to a tree T with root degree 1 there corresponds the t-th prime number, where t is the Matula  number of the tree obtained from T by deleting the edge emanating from the root; to a tree T with root degree m\u003e=2 there corresponds the product of the Matula numbers of the m branches of T."
			],
			"reference": [
				"L. Zhong, The harmonic index for graphs, Appl. Math. Letters, 25, 2012, 561-566.",
				"F. Goebel, On a 1-1 correspondence between rooted trees and natural numbers, J. Combin. Theory, B 29 (1980), 141-143.",
				"I. Gutman and A. Ivic, On Matula numbers, Discrete Math., 150, 1996, 131-142.",
				"I. Gutman and Y-N. Yeh, Deducing properties of trees from their Matula numbers, Publ. Inst. Math., 53 (67), 1993, 17-22.",
				"D. W. Matula, A natural rooted tree enumeration by prime factorization, SIAM Review, 10, 1968, 273.",
				"E. Deutsch, Rooted tree statistics from Matula numbers, Discrete Applied Math., 160, 2012, 2314-2322."
			],
			"link": [
				"E. Deutsch, \u003ca href=\"http://arxiv.org/abs/1111.4288\"\u003eRooted tree statistics from Matula numbers\u003c/a\u003e, arXiv1111.4288.",
				"A. Ilic, \u003ca href=\"http://arxiv.org/abs/1204.3313\"\u003eNote on the harmonic index of a graph\u003c/a\u003e, arXiv1204.3313.",
				"\u003ca href=\"/index/Mat#matula\"\u003eIndex entries for sequences related to Matula-Goebel numbers\u003c/a\u003e"
			],
			"formula": [
				"There are recurrence relations that give the harmonic index of an \"elevated\" rooted tree (attach a new vertex to the root which becomes the root of the new tree) and of the merge of two rooted trees (identify the two roots). They make use of the sequence of the degrees of the level-1 vertices (denoted by DL in the Maple program).",
				"In the Maple program, F(n) gives the actual (not rounded) harmonic index of the rooted tree with Matula number n. For example, F(987654321) = 807/70; the corresponding tree is the 29-vertex tree given in Fig. 2 of the Deutsch reference."
			],
			"example": [
				"a(5)=2; indeed the rooted tree with Matula number 5 is the path PQRS (rooted at P). The edges PQ and RS have endpoints of degrees 1 and 2 and the edge QR has endpoints  of degrees 2 and 2; consequently, the contributions of these 3 edges to the harmonic index are 2/3, 2/3, and 1/2, respectively; the harmonic index is 4/3 + 1/2 = 11/6 = 1.8333.",
				"G.f. = x^2 + x^3 + x^4 + 2*x^5 + 2*x^6 + 2*x^7 + 2*x^8 + 2*x^9 + 2*x^10 + ..."
			],
			"maple": [
				"f := proc (x, y) options operator, arrow: 2/(x+y) end proc: c := 1: with(numtheory): F := proc (n) local DL, r, s: DL := proc (n) if n = 2 then [1] elif bigomega(n) = 1 then [1+bigomega(pi(n))] else [op(DL(op(1, factorset(n)))), op(DL(n/op(1, factorset(n))))] end if end proc; r := proc (n) options operator, arrow: op(1, factorset(n)) end proc: s := proc (n) options operator, arrow: n/r(n) end proc: if n = 2 then c elif bigomega(n) = 1 then F(pi(n))-(sum(f(DL(pi(n))[j], bigomega(pi(n))), j = 1 .. bigomega(pi(n))))+sum(f(DL(pi(n))[j], 1+bigomega(pi(n))), j = 1 .. bigomega(pi(n)))+f(1, 1+bigomega(pi(n))) else F(r(n))+F(s(n))-(sum(f(DL(r(n))[j], bigomega(r(n))), j = 1 .. bigomega(r(n))))-(sum(f(DL(s(n))[j], bigomega(s(n))), j = 1 .. bigomega(s(n))))+sum(f(DL(r(n))[j], bigomega(n)), j = 1 .. bigomega(r(n)))+sum(f(DL(s(n))[j], bigomega(n)), j = 1 .. bigomega(s(n))) end if end proc: seq(round(F(n)), n = 2 .. 100);"
			],
			"keyword": "nonn",
			"offset": "2,4",
			"author": "_Emeric Deutsch_, Feb 26 2014",
			"references": 0,
			"revision": 12,
			"time": "2017-03-07T11:23:43-05:00",
			"created": "2014-02-26T12:45:37-05:00"
		}
	]
}