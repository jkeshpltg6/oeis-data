{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A059590",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 59590,
			"data": "0,1,2,3,6,7,8,9,24,25,26,27,30,31,32,33,120,121,122,123,126,127,128,129,144,145,146,147,150,151,152,153,720,721,722,723,726,727,728,729,744,745,746,747,750,751,752,753,840,841,842,843,846,847,848,849,864,865",
			"name": "Numbers obtained by reinterpreting base-2 representation of n in the factorial base: a(n) = Sum_{k\u003e=0} A030308(n,k)*A000142(k+1).",
			"comment": [
				"Numbers that are sums of distinct factorials (0! and 1! not treated as distinct).",
				"Complement of A115945; A115944(a(n)) \u003e 0; A115647 is a subsequence. - _Reinhard Zumkeller_, Feb 02 2006",
				"A115944(a(n)) = 1. - _Reinhard Zumkeller_, Dec 04 2011",
				"From _Tilman Piesk_, Jun 04 2012: (Start)",
				"The inversion vector (compare A007623) of finite permutation a(n) (compare A055089, A195663) has only zeros and ones. Interpreted as a binary number it is 2*n (or n when the inversion vector is defined without the leading 0).",
				"The inversion set of finite permutation a(n) interpreted as a binary number (compare A211362) is A211364(n).",
				"(End)"
			],
			"link": [
				"Reinhard Zumkeller (terms 0..500) \u0026 Antti Karttunen, \u003ca href=\"/A059590/b059590.txt\"\u003eTable of n, a(n) for n = 0..8191\u003c/a\u003e",
				"\u003ca href=\"/index/Fa#facbase\"\u003eIndex entries for sequences related to factorial base representation\u003c/a\u003e",
				"\u003ca href=\"/index/Fa#factorial\"\u003eIndex entries for sequences related to factorial numbers\u003c/a\u003e"
			],
			"formula": [
				"G.f. 1/(1-x) * Sum_{k\u003e=0} (k+1)!*x^2^k/(1+x^2^k). - _Ralf Stephan_, Jun 24 2003",
				"a(n) = Sum_{k\u003e=0} A030308(n,k)*A000142(k+1). - _Philippe Deléham_, Oct 15 2011",
				"From _Antti Karttunen_, Aug 19 2016: (Start)",
				"a(0) = 0, a(2n) = A153880(a(n)), a(2n+1) = 1+A153880(a(n)).",
				"a(n) = A225901(A276091(n)).",
				"a(n) = A276075(A019565(n)).",
				"a(A275727(n)) = A276008(n).",
				"A275736(a(n)) = n.",
				"A276076(a(n)) = A019565(n).",
				"A007623(a(n)) = A007088(n).",
				"(End)",
				"a(n) = a(n - mbs(n)) + (1 + floor(log(n) / log(2)))!. - _David A. Corneth_, Aug 21 2016"
			],
			"example": [
				"128 is in the sequence since 5! + 3! + 2! = 128.",
				"a(22) = 128. a(22) = a(6) + (1 + floor(log(16) / log(2)))! = 8 + 5! = 128. Also, 22 = 10110_2. Therefore, a(22) = 1 * 5! + 0 * 4! + 1 * 3! + 1 + 2! + 0 * 0! = 128. - _David A. Corneth_, Aug 21 2016"
			],
			"maple": [
				"[seq(bin2facbase(j),j=0..64)]; bin2facbase := proc(n) local i; add((floor(n/(2^i)) mod 2)*((i+1)!),i=0..floor_log_2(n)); end;",
				"floor_log_2 := proc(n) local nn,i; nn := n; for i from -1 to n do if(0 = nn) then RETURN(i); fi; nn := floor(nn/2); od; end;"
			],
			"mathematica": [
				"a[n_] :=  Reverse[id = IntegerDigits[n, 2]].Range[Length[id]]!; Table[a[n], {n, 0, 60}] (* _Jean-François Alcover_, Jun 19 2012, after _Philippe Deléham_ *)"
			],
			"program": [
				"(Haskell)",
				"import Data.List (elemIndices)",
				"a059590 n = a059590_list !! n",
				"a059590_list = elemIndices 1 $ map a115944 [0..]",
				"-- _Reinhard Zumkeller_, Dec 04 2011",
				"(PARI) a(n) = if(n\u003e0, a(n-msb(n)) + (1+logint(n,2))!, 0)",
				"msb(n) = 2^#binary(n)\u003e\u003e1",
				"{my(b = binary(n)); sum(i=1,#b,b[i]*(#b+1-i)!)} \\\\ _David A. Corneth_, Aug 21 2016"
			],
			"xref": [
				"Cf. A007088, A007623, A014597, A051760, A051761, A059589.",
				"Indices of zeros in A257684.",
				"Cf. A275736 (left inverse).",
				"Cf. A025494, A060112 (subsequences).",
				"Cf. A153880, A225901.",
				"Subsequence of A060132, A256450 and A275804.",
				"Other sequences that are built by replacing 2^k in the binary representation with other numbers: A029931 (naturals), A089625 (primes), A022290 (Fibonacci), A197433 (Catalans), A276091 (n*n!), A275959 ((2n)!/2). Cf. also A276082 \u0026 A276083."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Henry Bottomley_, Jan 24 2001",
			"ext": [
				"Name changed (to emphasize the functional nature of the sequence) with the old definition moved to the comments by _Antti Karttunen_, Aug 21 2016"
			],
			"references": 44,
			"revision": 56,
			"time": "2017-10-08T05:55:17-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}