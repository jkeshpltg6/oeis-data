{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A084509",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 84509,
			"data": "1,1,2,6,24,96,384,1536,6144,24576,98304,393216,1572864,6291456,25165824,100663296,402653184,1610612736,6442450944,25769803776,103079215104,412316860416,1649267441664,6597069766656,26388279066624,105553116266496,422212465065984",
			"name": "Number of ground-state 3-ball juggling sequences of period n.",
			"comment": [
				"This sequence counts the length n asynchronic site swaps given in A084501/A084502.",
				"Equals row sums of triangle A145463. - _Gary W. Adamson_, Oct 11 2008",
				"a(n) is the number of permutations of length n+1 avoiding the partially ordered pattern (POP) {1\u003e2, 1\u003e3, 1\u003e4, 1\u003e5} of length 5. That is, the number of length n+1 permutations having no subsequences of length 5 in which the first element is the largest. - _Sergey Kitaev_, Dec 11 2020"
			],
			"reference": [
				"B. Polster, The Mathematics of Juggling, Springer-Verlag, 2003, p. 48."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A084509/b084509.txt\"\u003eTable of n, a(n) for n = 0..1662\u003c/a\u003e",
				"Fan Chung and R. L. Graham, \u003ca href=\"http://www.jstor.org/stable/27642443\"\u003ePrimitive juggling sequences\u003c/a\u003e, Amer. Math. Monthly 115(3) (2008), 185-19.",
				"Alice L. L. Gao and Sergey Kitaev, \u003ca href=\"https://arxiv.org/abs/1903.08946\"\u003eOn partially ordered patterns of length 4 and 5 in permutations\u003c/a\u003e, arXiv:1903.08946 [math.CO], 2019.",
				"Alice L. L. Gao, Sergey Kitaev, \u003ca href=\"https://doi.org/10.37236/8605\"\u003eOn partially ordered patterns of length 4 and 5 in permutations\u003c/a\u003e, The Electronic Journal of Combinatorics 26(3) (2019), P3.26.",
				"Kai Ting Keshia Yap, David Wehlau, and Imed Zaguia, \u003ca href=\"https://arxiv.org/abs/2101.12061\"\u003ePermutations Avoiding Certain Partially-ordered Patterns\u003c/a\u003e, arXiv:2101.12061 [math.CO], 2021.",
				"\u003ca href=\"/index/J#Juggling\"\u003eIndex entries for sequences related to juggling\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_01\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4)."
			],
			"formula": [
				"a(n) = n! for n \u003c= 4, a(n) = 6*4^(n-3) = A002023(n-3) for n \u003e= 3.",
				"G.f.: 1 + x*(1 - 2*x - 2*x^2)/(1 - 4*x). - _Philippe Deléham_, Aug 16 2005"
			],
			"maple": [
				"A084509 := n -\u003e `if`((n\u003c4),n!,6*(4^(n-3)));",
				"INVERT([seq(A084519(n),n=1..12)]);"
			],
			"mathematica": [
				"LinearRecurrence[{4},{1,2,6},30] (* _Harvey P. Dale_, Aug 23 2018 *)"
			],
			"xref": [
				"First differences of A084508.",
				"INVERT transform of A084519.",
				"Cf. A002023, A084501, A084502, A084529, A145463."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Antti Karttunen_, Jun 02 2003",
			"ext": [
				"a(0)=1 prepended by _Alois P. Heinz_, Dec 11 2020"
			],
			"references": 10,
			"revision": 48,
			"time": "2021-05-07T19:48:23-04:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}