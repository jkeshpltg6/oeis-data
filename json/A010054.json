{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A010054",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 10054,
			"data": "1,1,0,1,0,0,1,0,0,0,1,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0",
			"name": "a(n) = 1 if n is a triangular number, otherwise 0.",
			"comment": [
				"This is essentially the q-expansion of the Jacobi theta function theta_2(q). (In theta_2 one has to ignore the initial factor of 2*q^(1/4) and then replace q by q^(1/2). See also A005369.) - _N. J. A. Sloane_, Aug 03 2014",
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Ramanujan's theta function f(a, b) = Sum_{n=-inf..inf} a^(n*(n+1)/2) * b^(n*(n-1)/2).",
				"This sequence is the concatenation of the base-b digits in the sequence b^n, for any base b \u003e= 2. - Davis Herring (herring(AT)lanl.gov), Nov 16 2004",
				"Number of partitions of n into distinct parts such that the greatest part equals the number of all parts, see also A047993; a(n)=A117195(n,0) for n \u003e 0; a(n) = 1-A117195(n,1) for n \u003e 1. - _Reinhard Zumkeller_, Mar 03 2006",
				"Triangle T(n,k), 0 \u003c= k \u003c= n, read by rows, given by A000007 DELTA A000004 where DELTA is the operator defined in A084938. - _Philippe Deléham_, Jan 03 2009",
				"Convolved with A000041 = A022567, the convolution square of A000009. - _Gary W. Adamson_, Jun 11 2009",
				"A008441(n) = Sum_{k=0..n} a(k)*a(n-k). - _Reinhard Zumkeller_, Nov 03 2009",
				"Polcoeff inverse with alternate signs = A006950: (1, 1, 1, 2, 3, 4, 5, 7, ...). - _Gary W. Adamson_, Mar 15 2010",
				"This sequence is related to Ramanujan's two-variable theta functions because this sequence is also the characteristic function of generalized hexagonal numbers. - _Omar E. Pol_, Jun 08 2012",
				"Number 3 of the 14 primitive eta-products which are holomorphic modular forms of weight 1/2 listed by D. Zagier on page 30 of \"The 1-2-3 of Modular Forms\". - _Michael Somos_, May 04 2016",
				"Number of partitions of n into consecutive parts that contain 1 as a part, n \u003e= 1. - _Omar E. Pol_, Nov 27 2020"
			],
			"reference": [
				"J. H. Conway and N. J. A. Sloane, \"Sphere Packings, Lattices and Groups\", Springer-Verlag, p. 103.",
				"M. D. Hirschhorn, The Power of q, Springer, 2017. See Psi page 9.",
				"J. Tannery and J. Molk, Eléments de la Théorie des Fonctions Elliptiques, Vol. 2, Gauthier-Villars, Paris, 1902; Chelsea, NY, 1972, see p. 27.",
				"E. T. Whittaker and G. N. Watson, A Course of Modern Analysis, Cambridge Univ. Press, 4th ed., 1963, p. 464."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A010054/b010054.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"S. Cooper and M. D. Hirschhorn, \u003ca href=\"http://dx.doi.org/10.1016/S0012-365X(03)00079-7\"\u003eResults of Hurwitz type for three squares\u003c/a\u003e, Discrete Math. 274 (2004), no. 1-3, 9-24. See psi(q).",
				"Shishuo Fu and Yaling Wang, \u003ca href=\"https://arxiv.org/abs/1908.03912\"\u003eBijective recurrences concerning two Schröder triangles\u003c/a\u003e, arXiv:1908.03912 [math.CO], 2019.",
				"M. D. Hirschhorn and J. A. Sellers, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Sellers/sellers32.html\"\u003eA Congruence Modulo 3 for Partitions into Distinct Non-Multiples of Four\u003c/a\u003e, Article 14.9.6, Journal of Integer Sequences, Vol. 17 (2014).",
				"K. Ono, S. Robins and P. T. Wahl, \u003ca href=\"http://www.mathcs.emory.edu/~ono/publications-cv/pdfs/006.pdf\"\u003eOn the representation of integers as sums of triangular numbers\u003c/a\u003e, Aequationes mathematicae, August 1995, Volume 50, Issue 1-2, pp 73-94, Proposition 1.",
				"Franck Ramaharo, \u003ca href=\"https://arxiv.org/abs/1805.10680\"\u003eA generating polynomial for the pretzel knot\u003c/a\u003e, arXiv:1805.10680 [math.CO], 2018.",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e.",
				"Michael Somos, \u003ca href=\"/A108483/a108483.pdf\"\u003eA Multisection of q-Series\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e",
				"\u003ca href=\"/index/Ch#char_fns\"\u003eIndex entries for characteristic functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of f(x, x^3) in powers of x where f(, ) is Ramanujan's general theta function.",
				"Expansion of q^(-1) * (phi(q) - phi(q^4)) / 2 in powers of q^8. - _Michael Somos_, Jul 01 2014",
				"Expansion of q^(-1/8) * eta(q^2)^2 / eta(q) in powers of q. - _Michael Somos_, Apr 13 2005",
				"Euler transform of period 2 sequence [ 1, -1, ...]. - _Michael Somos_, Mar 24 2003",
				"Given g.f. A(x), then B(q) = q * A(q^8) satisfies 0 = f(B(q), B(q^2), B(q^3), B(q^6)) where f(u1, u2, u3, u6) = u1*u6^3 + u2*u3^3 - u1*u2^2*u6. - _Michael Somos_, Apr 13 2005",
				"a(n) = b(8*n + 1) where b()=A098108() is multiplicative with b(2^e) = 0^e, b(p^e) = (1 + (-1)^e) / 2 if p \u003e 2. - _Michael Somos_, Jun 06 2005",
				"a(n) = A005369(2*n). - _Michael Somos_, Apr 29 2003",
				"G.f.: theta_2(sqrt(q)) / (2 * q^(1/8)).",
				"G.f.: 1 / (1 - x / (1 + x / (1 + x^1 / (1 - x / (1 + x / (1 + x^2 / (1 - x / (1 + x / (1 + x^3 / ...))))))))). - _Michael Somos_, May 11 2012",
				"G.f.: Product_{k\u003e0} (1-x^(2*k))/(1-x^(2*k-1)). - _Vladeta Jovovic_, May 02 2002",
				"a(0)=1; for n\u003e0, a(n) = A002024(n+1)-A002024(n). - _Benoit Cloitre_, Jan 05 2004",
				"G.f.: Sum_{j\u003e=0} Product_{k=0..j} x^j. - _Jon Perry_, Mar 30 2004",
				"a(n) = floor((1-cos(Pi*sqrt(8*n+1)))/2). - _Carl R. White_, Mar 18 2006",
				"a(n) = round(sqrt(2n+1)) - round(sqrt(2n)). - _Hieronymus Fischer_, Aug 06 2007",
				"a(n) = ceiling(2*sqrt(2n+1)) - floor(2*sqrt(2n)) - 1. - _Hieronymus Fischer_, Aug 06 2007",
				"a(n) = f(n,0) with f(x,y) = if x \u003e 0 then f(x-y,y+1), otherwise 0^(-x). - _Reinhard Zumkeller_, Sep 27 2008",
				"a(n) = A035214(n) - 1.",
				"From _Mikael Aaltonen_, Jan 22 2015: (Start)",
				"Since the characteristic function of s-gonal numbers is given by floor(sqrt(2n/(s-2) + ((s-4)/(2s-4))^2) + (s-4)/(2s-4)) - floor(sqrt(2(n-1)/(s-2) + ((s-4)/(2s-4))^2) + (s-4)/(2s-4)), by setting s = 3 we get the following: For n \u003e 0, a(n) = floor(sqrt(2*n+1/4)-1/2) - floor(sqrt(2*(n-1)+1/4)-1/2).",
				"(End)",
				"a(n) = (-1)^n * A106459(n). - _Michael Somos_, May 04 2016",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (16 t)) = 2^(-1/2) (t/i)^(1/2) g(t) where q = exp(2 Pi i t) and g() is the g.f. for A002448. - _Michael Somos_, May 05 2016",
				"G.f.: Sum_{n \u003e= 0} x^(n*(n+1)/2) = Product_{n \u003e= 1} (1 - x^n)*(1 + x^n)^2 = Product_{n \u003e= 1} (1 - x^(2*n))*(1 + x^n) = Product_{n \u003e= 1} (1 - x^(2*n))/(1 - x^(2*n-1)). From the sum and product representations of theta_2(0, sqrt(q))/(2*q^(1/8))) function. The last product, given by _Vladeta Jovovic_ above, is obtained from the second to last one by an Euler identity, proved via f(x) := Product_{n \u003e= 1} (1 - x^(2*n-1))*Product_{n \u003e= 1} (1 + x^n) = f(x^2), by moving the odd-indexed factors of the second product to the first product. This leads to f(x) = f(0) = 1. - _Wolfdieter Lang_, Jul 05 2016",
				"a(0) = 1, a(n) = (1/n)*Sum_{k=1..n} A002129(k)*a(n-k) for n \u003e 0. - _Seiichi Manyama_, Apr 08 2017"
			],
			"example": [
				"G.f. = 1 + x + x^3 + x^6 + x^10 + x^15 + x^21 + x^28 + x^36 + x^45 + x^55 + x^66 + ...",
				"G.f. for B(q) = q * A(q^8): q + q^9 + q^25 + q^49 + q^81 + q^121 + q^169 + q^225 + q^289 + q^361 + ...",
				"From _Philippe Deléham_, Jan 04 2008: (Start)",
				"As a triangle this begins:",
				"  1;",
				"  1, 0;",
				"  1, 0, 0;",
				"  1, 0, 0, 0;",
				"  1, 0, 0, 0, 0;",
				"  1, 0, 0, 0, 0, 0;",
				"  ...  (End)"
			],
			"maple": [
				"A010054 := proc(n)",
				"    if issqr(1+8*n) then",
				"        1;",
				"    else",
				"        0;",
				"    end if;",
				"end proc:",
				"seq(A010054(n),n=0..80) ; # _R. J. Mathar_, Feb 22 2021"
			],
			"mathematica": [
				"a[ n_] := SquaresR[ 1, 8 n + 1] / 2; (* _Michael Somos_, Nov 15 2011 *)",
				"a[ n_] := If[ n \u003c 0, 0, SeriesCoefficient[ (Series[ EllipticTheta[ 3, Log[y] / (2 I), x^2], {x, 0, n + Floor @ Sqrt[n]}] // Normal // TrigToExp) /. {y -\u003e x}, {x, 0, n}]]; (* _Michael Somos_, Nov 15 2011 *)",
				"Table[If[IntegerQ[(Sqrt[8n+1]-1)/2],1,0],{n,0,110}] (* _Harvey P. Dale_, Oct 29 2012 *)",
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 2, 0, q^(1/2)] / (2 q^(1/8)), {q, 0, n}]; (* _Michael Somos_, Jul 01 2014 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A)^2 / eta(x + A), n))}; /* _Michael Somos_, Mar 14 2011 */",
				"(PARI) {a(n) = issquare( 8*n + 1)}; /* _Michael Somos_, Apr 27 2000 */",
				"(PARI) a(n) = ispolygonal(n, 3); \\\\ _Michel Marcus_, Jan 22 2015",
				"(Haskell)",
				"a010054 = a010052 . (+ 1) . (* 8)",
				"a010054_list = concatMap (\\x -\u003e 1 : replicate x 0) [0..]",
				"-- _Reinhard Zumkeller_, Feb 12 2012, Oct 22 2011, Apr 02 2011",
				"(MAGMA) Basis( ModularForms( Gamma0(16), 1/2), 362) [2] ; /* _Michael Somos_, Jun 10 2014 */"
			],
			"xref": [
				"Cf. A000217, A002448, A005369, A023531, A035214, A022567, A052343, A006950, A106459, A127648.",
				"Number of ways of writing n as a sum of k triangular numbers, for k=1,...: A010054, A008441, A008443, A008438, A008439, A008440, A226252, A007331, A226253, A226254, A226255, A014787, A014809.",
				"Cf. A106507 (reciprocal series)."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Additional comments from _Michael Somos_, Apr 27 2000"
			],
			"references": 1532,
			"revision": 186,
			"time": "2021-02-22T10:50:21-05:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}