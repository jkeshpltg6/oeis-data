{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A320034",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 320034,
			"data": "1,1,1,2,3,3,5,6,7,7,6,9,11,8,8,9,11,9,11,9,12,9,8,9,14,6,9,5,11,6,11,4,12,5,8,4,14,5,9,3,11,5,11,3,12,5,8,3,14,5,9,3,11,5,11,3,12,5,8,3,14,5,9,3,11,5,11,3,12,5,8,3,14,5,9,3,11,5,11,3,12,5,8,3,14,5,9,3,11,5,11,3,12,5,8,3",
			"name": "a(n) is the number of integer partitions of n with largest part \u003c= 6 for which the index of the seaweed algebra formed by the integer partition paired with its weight is 0.",
			"comment": [
				"The index of a Lie algebra, g, is an invariant of the Lie algebra defined by min(dim(Ker(B_f)) where the min is taken over all linear functionals f on g and B_f denotes the bilinear form f([_,_]), where [,] denotes the bracket multiplication on g.",
				"For seaweed subalgebras of sl(n), which are Lie subalgebras of sl(n) whose matrix representations are parametrized by an ordered pair of compositions of n, the index can be determined from a corresponding graph called a meander.",
				"a(n) is periodic with period 12 for n \u003e 36."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A320034/b320034.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"V. Coll, A. Mayers, N. Mayers, \u003ca href=\"https://arxiv.org/abs/1809.09271\"\u003eStatistics on integer partitions arising from seaweed algebras\u003c/a\u003e, arXiv preprint arXiv:1809.09271 [math.CO], 2018.",
				"V. Dergachev, A. Kirillov, \u003ca href=\"https://www.emis.de/journals/JLT/vol.10_no.2/6.html\"\u003eIndex of Lie algebras of seaweed type\u003c/a\u003e, J. Lie Theory 10 (2) (2000) 331-343.",
				"\u003ca href=\"/index/Rec#order_08\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,-1,0,0,0,1,0,1)."
			],
			"formula": [
				"For n \u003e 36: a(n)=14 if 1 == n (mod 12), a(n)=5 if 2,6,10 == n (mod 12), a(n)=9 if 3 == n (mod 12), a(n)=3 if 0,4,8 == n (mod 12), a(n)=11 if 5,7 == n (mod 12), a(n)=12 if 9 == n (mod 12), a(n)=8 if 11 == n (mod 12).",
				"G.f.: x*(1 + x + 2*x^2 + 3*x^3 + 4*x^4 + 5*x^5 + 7*x^6 + 8*x^7 + 10*x^8 + 10*x^9 + 9*x^10 + 11*x^11 + 9*x^12 + 8*x^13 + 7*x^14 + 4*x^15 + 6*x^16 + 2*x^17 + 5*x^18 + x^19 + 4*x^20 + x^21 + x^22 - 3*x^25 - 7*x^27 - 7*x^29 - 5*x^31 - 2*x^33 - 2*x^35 - x^37 - x^39 - x^41 - x^43) / ((1 - x)*(1 + x)*(1 - x + x^2)*(1 + x^2)*(1 + x + x^2)). - _Colin Barker_, Apr 21 2019"
			],
			"mathematica": [
				"a[n_] := If[n \u003c= 36, {1, 1, 1, 2, 3, 3, 5, 6, 7, 7, 6, 9, 11, 8, 8, 9, 11, 9, 11, 9, 12, 9, 8, 9, 14, 6, 9, 5, 11, 6, 11, 4, 12, 5, 8, 4}[[n]], Switch[ Mod[n, 12], 1, 14, 2|6|10, 5, 3, 9, 0|4|8, 3, 5|7, 11, 9, 12, 11, 8]]; Array[a, 100] (* _Jean-François Alcover_, Dec 08 2018 *)"
			],
			"program": [
				"(PARI) Vec(x*(1 + x + 2*x^2 + 3*x^3 + 4*x^4 + 5*x^5 + 7*x^6 + 8*x^7 + 10*x^8 + 10*x^9 + 9*x^10 + 11*x^11 + 9*x^12 + 8*x^13 + 7*x^14 + 4*x^15 + 6*x^16 + 2*x^17 + 5*x^18 + x^19 + 4*x^20 + x^21 + x^22 - 3*x^25 - 7*x^27 - 7*x^29 - 5*x^31 - 2*x^33 - 2*x^35 - x^37 - x^39 - x^41 - x^43) / ((1 - x)*(1 + x)*(1 - x + x^2)*(1 + x^2)*(1 + x + x^2)) + O(x^100)) \\\\ _Colin Barker_, Apr 21 2019"
			],
			"xref": [
				"Cf. A319981, A319982, A320033, A320036."
			],
			"keyword": "nonn,easy",
			"offset": "1,4",
			"author": "_Nick Mayers_, Oct 03 2018",
			"ext": [
				"Data corrected by _Jean-François Alcover_, Dec 08 2018"
			],
			"references": 5,
			"revision": 20,
			"time": "2019-04-22T18:10:36-04:00",
			"created": "2018-11-04T23:55:17-05:00"
		}
	]
}