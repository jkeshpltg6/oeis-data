{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A334300",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 334300,
			"data": "0,1,1,2,1,3,3,3,1,3,2,5,3,6,5,4,1,3,3,5,3,5,6,7,3,6,5,9,5,9,7,5,1,3,3,5,2,7,7,7,3,7,3,8,7,11,10,9,3,6,7,9,7,10,11,12,5,9,8,13,7,12,9,6,1,3,3,5,3,7,7,7,3,5,5,11,6,13,11,9,3,7,6",
			"name": "Number of distinct nonempty subsequences (not necessarily contiguous) in the n-th composition in standard order (A066099).",
			"comment": [
				"Looking only at contiguous subsequences, or restrictions to a subinterval, gives A124770.",
				"The k-th composition in standard order (graded reverse-lexicographic, A066099) is obtained by taking the set of positions of 1's in the reversed binary expansion of k, prepending 0, taking first differences, and reversing again. This gives a bijective correspondence between nonnegative integers and integer compositions."
			],
			"formula": [
				"a(n) = A334299(n) - 1."
			],
			"example": [
				"Triangle begins:",
				"  1",
				"  1 2",
				"  1 3 3 3",
				"  1 3 2 5 3 6 5 4",
				"  1 3 3 5 3 5 6 7 3 6 5 9 5 9 7 5",
				"If the k-th composition in standard order is c, then we say that the STC-number of c is k. The n-th column below lists the STC-numbers of the nonempty subsequences of the composition with STC-number n:",
				"  1  2  3  4  5  6  7  8  9  10  11  12  13  14  15",
				"        1     2  2  3     4   2   5   4   6   6   7",
				"              1  1  1     1       3   1   5   3   3",
				"                                  2       3   2   1",
				"                                  1       2   1",
				"                                          1"
			],
			"mathematica": [
				"stc[n_]:=Differences[Prepend[Join@@Position[Reverse[IntegerDigits[n,2]],1],0]]//Reverse;",
				"Table[Length[Union[Rest[Subsets[stc[n]]]]],{n,0,100}]"
			],
			"xref": [
				"Row lengths are A011782.",
				"Looking only at contiguous subsequences gives A124770.",
				"The contiguous case with empty subsequences allowed is A124771.",
				"Allowing empty subsequences gives A334299.",
				"Compositions where every subinterval has a different sum are A333222.",
				"Knapsack compositions are A333223.",
				"Contiguous positive subsequence-sums are counted by A333224.",
				"Contiguous subsequence-sums are counted by A333257.",
				"Subsequence-sums are counted by A334968.",
				"Cf. A000120, A029931, A048793, A066099, A070939, A108917, A325676, A334967."
			],
			"keyword": "nonn,tabf",
			"offset": "0,4",
			"author": "_Gus Wiseman_, Jun 01 2020",
			"references": 3,
			"revision": 6,
			"time": "2020-06-04T06:39:50-04:00",
			"created": "2020-06-04T06:39:50-04:00"
		}
	]
}