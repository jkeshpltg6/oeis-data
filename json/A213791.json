{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A213791",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 213791,
			"data": "1,-6,15,-26,45,-66,82,-120,156,-170,231,-276,290,-390,435,-438,561,-630,651,-780,861,-842,1020,-1170,1095,-1326,1431,-1370,1716,-1740,1682,-2016,2145,-2132,2415,-2550,2353,-2850,3120,-2810,3321,-3486,3285,-3906,4005",
			"name": "Expansion of psi(-x)^6 in powers of x where psi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"reference": [
				"J. W. L. Glaisher, Identities, Messenger of Mathematics, 5 (1876), pp. 111-112. see Eq. X",
				"J. W. L. Glaisher, Notes on Certain Formulae in Jacobi's Fundamenta Nova, Messenger of Mathematics, 5 (1876), pp. 174-179. see p.176"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A213791/b213791.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(-3/4) * ( eta(q) * eta(q^4) / eta(q^2) )^6 in powers of q.",
				"Expansion of -1/(8 * r) * ( 1^2 * r^1 / (1 + q) - 3^2 * q^(3/4) / (1 + q^3) - 5^2 * r^5 / (1 + q^5) + 7^2 * q^(7/4) / (1 + q^7) + 9^2 * r^9 / (1 + q^9) - ...) in powers of q where r = q^(3/4) [Glaisher 1876].",
				"Expansion of q^(-1/4) * ( sqrt(k * k') * K / Pi )^3 in powers of q where k, k', K are Jacobi elliptic functions. [Jacobi 1828, p. 108 quoted in Glaisher 1876, p. 176].",
				"Euler transform of period 4 sequence [ -6, 0, -6, -6, ...].",
				"G.f.: (Sum_{k\u003e0} (-x)^((k^2 - k)/2))^6.",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (64 t)) = 64^(3/2) (t/i)^3 f(t) where q = exp(2 Pi i t).",
				"a(n) = (-1)^n * A008440(n). Convolution cube of A134343."
			],
			"example": [
				"G.f. = 1 - 6*x + 15*x^2 - 26*x^3 + 45*x^4 - 66*x^5 + 82*x^6 - 120*x^7 + ...",
				"G.f. = q^3 - 6*q^7 + 15*q^11 - 26*q^15 + 45*q^19 - 66*q^23 + 82*q^27 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ (QPochhammer[ x] / QPochhammer[ x^2, x^4])^6, {x, 0, n}]; (* _Michael Somos_, Jun 10 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( ( eta(x + A) * eta(x^4 + A) / eta(x^2 + A) )^6, n))};"
			],
			"xref": [
				"Cf. A008440, A134343."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Jun 20 2012",
			"references": 2,
			"revision": 17,
			"time": "2021-03-12T22:24:46-05:00",
			"created": "2012-06-21T06:20:11-04:00"
		}
	]
}