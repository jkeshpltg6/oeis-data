{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A279044",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 279044,
			"data": "1,1,1,1,1,1,1,2,1,1,2,1,2,1,1,2,2,3,2,1,1,3,4,1,3,2,1,1,3,5,2,5,3,2,1,1,5,7,5,1,5,3,2,1,1,5,9,7,2,7,5,3,2,1,1,7,12,12,5,1,7,5,3,2,1,1,7,14,16,8,2,11,7,5,3,2,1,1,11,20,20,14,5",
			"name": "Irregular triangular array: T(n,i) = number of partitions of n having crossover part k; see Comments.",
			"comment": [
				"Suppose that P = [p(1),p(2),...,p(k)] is a partition of n, where p(1) \u003e= p(2) \u003e= ... \u003e= p(k). The crossover index of P is the least h such that p(1) + ... + p(h) \u003e = n/2. Equivalently for k \u003e 1, p(1) + ... + p(h) \u003e= p(h+1) + ... + p(k). The crossover part of P is p(h). The n-th row sum is the number of partitions of n, A000041. The bisections of column 1 are also given by A000041. The limit of the reversal of row n is given by A000041."
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A279044/b279044.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e"
			],
			"example": [
				"First 14 rows (indexed by column 1):",
				"1...       1",
				"2...       1    1",
				"3...       1    1    1",
				"4...       1    2    1    1",
				"5...       2    1    2    1    1",
				"6...       2    2    3    2    1    1",
				"7...       3    4    1    3    2    1    1",
				"8...       3    5    2    5    3    2    1    1",
				"9...       5    7    5    1    5    3    2    1    1",
				"10...      5    9    7    2    7    5    3    2    1   1",
				"11...      7    12   12   5    1    7    5    4    2   1   1",
				"12...      7    14   15   8    2    11   7    5    3   2   1   1",
				"13...      11   20   20   14   5    1    11   7    5   3   2   1   1",
				"14...      11   24   25   20   8    2    15   11   7   5   3   2   1   1"
			],
			"mathematica": [
				"p[n_] := p[n] = IntegerPartitions[n]; t[n_, k_] := t[n, k] = p[n][[k]];",
				"q[n_, k_] := q[n, k] = Select[Range[50], Sum[t[n, k][[i]], {i, 1, #}] \u003e= n/2 \u0026, 1];",
				"u[n_] := u[n] = Flatten[Table[p[n][[k]][[q[n, k]]], {k, 1, Length[p[n]]}]];",
				"c[n_, k_] := c[n, k] = Count[u[n], k];",
				"v = Table[c[n, k], {n, 1, 25}, {k, 1, n}];",
				"TableForm[v] (*A279044 array*)",
				"Flatten[v]   (*A279044 sequence*)"
			],
			"xref": [
				"Cf. A000041, A276468 (crossover index array)."
			],
			"keyword": "nonn,easy,tabf",
			"offset": "1,8",
			"author": "_Clark Kimberling_, Dec 04 2016",
			"references": 2,
			"revision": 8,
			"time": "2016-12-07T10:20:58-05:00",
			"created": "2016-12-07T10:20:58-05:00"
		}
	]
}