{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A248222",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 248222,
			"data": "1,1,2,3,3,2,3,4,3,3,4,5,5,3,5,7,4,3,5,7,6,4,5,8,3,5,3,7,4,5,5,8,6,4,5,9,5,5,6,11,6,6,6,8,6,5,5,12,4,3,6,8,7,3,8,9,7,4,6,11,7,5,9,8,9,6,7,13,7,5,7,12,5,5,7,8,11,6,7,15,3,6,8,12,13,6,11,16,7,6",
			"name": "Maximal gap between quadratic residues mod n.",
			"comment": [
				"\"Maximal gap between squares mod n\" would be a less ambiguous definition.",
				"The definition of quadratic residue modulo a nonprime varies from author to author. Sometimes, even when n is a prime, 0 is not counted as a quadratic residue. In this entry, an integer q is called a quadratic residue modulo n if it is congruent to a perfect square modulo n.",
				"See A248376 for the variant with the additional restriction that the residue be coprime to the modulus. - _M. F. Hasler_, Oct 08 2014"
			],
			"reference": [
				"K. Ireland and M. Rosen, A Classical Introduction to Modern Number Theory, Springer, 1982, p. 194. [Requires gcd(q,n)=1 for q to be a quadratic residue mod n.]",
				"F. J. MacWilliams and N. J. A. Sloane, The Theory of Error-Correcting Codes, Elsevier-North Holland, 1978, p. 45.",
				"G. B. Mathews, Theory of Numbers, 2nd edition. Chelsea, NY, p. 32. [Does not require gcd(q,n)=1.]",
				"Ivan Niven and Herbert S. Zuckerman, An Introduction to the Theory of Numbers, New York: John Wiley, 2nd ed., 1966, p. 69. [Requires gcd(q,n)=1 for q to be a quadratic residue mod n.]",
				"J. V. Uspensky and M. A. Heaslet, Elementary Number Theory, McGraw-Hill, NY, 1939, p. 270. [Does not require gcd(q,n)=1.]"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A248222/b248222.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/QuadraticResidue.html\"\u003eQuadratic Residue\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Quadratic_residue\"\u003eQuadratic residue\u003c/a\u003e"
			],
			"example": [
				"For n=7, the quadratic residues are all numbers congruent to 0, 1, 2, or 4 (mod 7), so the largest gap of 3 occurs for example between 4 = 2^2 (mod 7) and 7 = 0^2 (mod 7).",
				"For n=16, the quadratic residues are the numbers congruent to 0, 1, 4 or 9 (mod 16), so the largest gap occurs between, e.g., 9 = 3^2 (mod 16) and 16 = 0^2 (mod 16)."
			],
			"program": [
				"(PARI) (DD(v)=vecextract(v,\"^1\")-vecextract(v,\"^-1\")); a(n)=vecmax(DD(select(f-\u003eissquare(Mod(f,n)),vector(n*2,i,i))))"
			],
			"xref": [
				"Cf. A063987, A130290, A088190, A088191, A088192."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_David W. Wilson_ and _M. F. Hasler_, Oct 04 2014",
			"ext": [
				"Comments and references added by _N. J. A. Sloane_, Oct 04 2014"
			],
			"references": 2,
			"revision": 45,
			"time": "2019-10-10T10:21:29-04:00",
			"created": "2014-10-04T21:50:28-04:00"
		}
	]
}