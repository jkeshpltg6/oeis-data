{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A054767",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 54767,
			"data": "1,3,13,12,781,39,137257,24,39,2343,28531167061,156,25239592216021,411771,10153,48,51702516367896047761,39,109912203092239643840221,9372,1784341,85593501183,949112181811268728834319677753,312,3905,75718776648063,117,1647084",
			"name": "Period of the sequence of Bell numbers A000110 (mod n).",
			"comment": [
				"For p prime, a(p) divides (p^p-1)/(p-1) = A023037(p), with equality at least for p up to 19.",
				"Wagstaff shows that N(p) = (p^p-1)/(p-1) is the period for all primes p \u003c 102 and for primes p = 113, 163, 167 and 173. For p = 7547, N(p) is a probable prime, which means that this p may have the maximum possible period N(p) also. See A088790. - _T. D. Noe_, Dec 17 2008"
			],
			"link": [
				"J. Levine and R. E. Dalton, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1962-0148604-2\"\u003eMinimum Periods, Modulo p, of First Order Bell Exponential Integrals\u003c/a\u003e, Mathematics of Computation, 16 (1962), 416-423.",
				"W. F. Lunnon et al., \u003ca href=\"http://matwbn.icm.edu.pl/ksiazki/aa/aa35/aa3511.pdf\"\u003eArithmetic properties of Bell numbers to a composite modulus I\u003c/a\u003e, Acta Arithmetica 35 (1979), pp. 1-16.",
				"Samuel S. Wagstaff Jr., \u003ca href=\"http://www.ams.org/mcom/1996-65-213/S0025-5718-96-00683-7/home.html\"\u003eAurifeuillian factorizations and the period of the Bell numbers modulo a prime\u003c/a\u003e, Math. Comp. 65 (1996), 383-391.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/BellNumber.html\"\u003eBell Number\u003c/a\u003e"
			],
			"formula": [
				"If gcd(n,m) = 1, a(n*m) = lcm(a(n), a(m)). But the sequence is not in general multiplicative; e.g. a(2) = 3, a(9) = 39 and a(18) = 39. - _Franklin T. Adams-Watters_, Jun 06 2006"
			],
			"mathematica": [
				"(* n.b. this program might be incorrect beyond a(26) *) BellMod[k_, m_] := Mod[ Sum[ Mod[ StirlingS2[k, j], m], {j, 1, k}], m]; BellMod[k_, 1] := BellB[k]; period[nn_] := Module[{lgmin = 2, lgmax = 5, nn1}, lg = If[Length[nn] \u003c= lgmax, lgmin, lgmax]; nn1 = nn[[1 ;; lg]]; km = Length[nn] - lg; Catch[ Do[ If[ nn1 == nn[[k ;; k + lg - 1]], Throw[k - 1]]; If[k == km, Throw[0]], {k, 2, km}]]]; a[1] = 1; a[p_?PrimeQ] := (p^p - 1)/(p - 1); a[n_ /; MemberQ[ FactorInteger[n][[All, 2]], 1]] := a[n] = With[{pp = Select[ FactorInteger[n], #1[[2]] == 1 \u0026 ][[All, 1]]}, a[n/Times @@ pp]*Times @@ a /@ pp]; a[n_ /; n \u003e 4 \u0026\u0026 GCD @@ FactorInteger[n][[All, 2]] \u003e 1] := a[n] = With[{g = GCD @@ FactorInteger[n][[All, 2]]}, n^(1/g)*a[n^(1 - 1/g)]]; a[n_] := period[ Table[ BellMod[k, n], {k, 1, 18}]]; Table[a[n], {n, 1, 26}] (* _Jean-François Alcover_, Jul 31 2012 *)"
			],
			"xref": [
				"Cf. A000110, A023037, A214810."
			],
			"keyword": "nonn,hard,nice",
			"offset": "1,2",
			"author": "_Eric W. Weisstein_, Feb 09 2002",
			"ext": [
				"More information from _Phil Carmody_, Dec 22 2002",
				"Extended by _T. D. Noe_, Dec 18 2008",
				"a(26) corrected by _Jean-François Alcover_, Jul 31 2012",
				"a(18) corrected by _Charles R Greathouse IV_, Jul 31 2012",
				"a(27)-a(28) from _Charles R Greathouse IV_, Sep 07 2016"
			],
			"references": 10,
			"revision": 29,
			"time": "2016-09-07T11:10:19-04:00",
			"created": "2000-06-15T03:00:00-04:00"
		}
	]
}