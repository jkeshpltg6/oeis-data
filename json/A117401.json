{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A117401",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 117401,
			"data": "1,1,1,1,2,1,1,4,4,1,1,8,16,8,1,1,16,64,64,16,1,1,32,256,512,256,32,1,1,64,1024,4096,4096,1024,64,1,1,128,4096,32768,65536,32768,4096,128,1,1,256,16384,262144,1048576,1048576,262144,16384,256,1",
			"name": "Triangle T(n,k) = 2^(k*(n-k)), read by rows.",
			"comment": [
				"Matrix power T^m satisfies: [T^m](n,k) = [T^m](n-k,0)*T(n,k) for all m and so the triangle has an invariant character."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A117401/b117401.txt\"\u003eRows n = 0..50 of the triangle, flattened\u003c/a\u003e",
				"Paul Barry, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL9/Barry/barry91.html\"\u003eOn Integer-Sequence-Based Constructions of Generalized Pascal Triangles\u003c/a\u003e, Journal of Integer Sequences, Vol. 9 (2006), Article 06.2.4."
			],
			"formula": [
				"G.f.: A(x,y) = Sum_{n\u003e=0} x^n/(1 - 2^n*x*y).",
				"G.f. satisfies: A(x,y) = 1/(1 - x*y) + x*A(x,2*y).",
				"Equals ConvOffsStoT transform of the 2^n series: (1, 2, 4, 8,...); e.g., ConvOffs transform of (1, 2, 4, 8) = (1, 8, 16, 8, 1). - _Gary W. Adamson_, Apr 21 2008",
				"T(n,k) = (1/n)*( 2^(n-k)*k*T(n-1,k-1) + 2^k*(n-k)*T(n-1,k) ), where T(i,j)=0 if j\u003ei. - _Tom Edgar_, Feb 20 2014",
				"Let E(x) = Sum_{n\u003e=0} x^n/2^C(n,2).  Then E(x)*E(y*x) = Sum_{n\u003e=0} Sum_{k=0..n} T(n,k)*y^k*x^n/2^C(n,2). - _Geoffrey Critzer_, May 31 2020",
				"T(n, k, m) = (m+2)^(k*(n-k)) with m = 0. - _G. C. Greubel_, Jun 28 2021"
			],
			"example": [
				"A(x,y) = 1/(1-xy) + x/(1-2xy) + x^2/(1-4xy) + x^3/(1-8xy) + ...",
				"Triangle begins:",
				"  1;",
				"  1,   1;",
				"  1,   2,     1;",
				"  1,   4,     4,      1;",
				"  1,   8,    16,      8,       1;",
				"  1,  16,    64,     64,      16,       1;",
				"  1,  32,   256,    512,     256,      32,      1;",
				"  1,  64,  1024,   4096,    4096,    1024,     64,     1;",
				"  1, 128,  4096,  32768,   65536,   32768,   4096,   128,   1;",
				"  1, 256, 16384, 262144, 1048576, 1048576, 262144, 16384, 256, 1;"
			],
			"mathematica": [
				"Table[2^((n-k)k),{n,0,10},{k,0,n}]//Flatten (* _Harvey P. Dale_, Jan 09 2017 *)"
			],
			"program": [
				"(PARI) T(n,k)=if(n\u003ck || k\u003c0,0,2^((n-k)*k))",
				"(MAGMA)",
				"A117401:= func\u003c n, k, m | (m+2)^(k*(n-k)) \u003e;",
				"[A117401(n, k, 0): k in [0..n], n in [0..12]]; // _G. C. Greubel_, Jun 28 2021",
				"(Sage)",
				"def A117401(n, k, m): return (m+2)^(k*(n-k))",
				"flatten([[A117401(n, k, 0) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, Jun 28 2021"
			],
			"xref": [
				"Cf. A117402 (row sums), A117403 (antidiagonal sums), A002416 (central terms).",
				"Cf. this sequence (m=0), A118180 (m=1), A118185 (m=2), A118190 (m=3), A158116 (m=4), A176642 (m=6), A158117 (m=8), A176627 (m=10), A176639 (m=13), A156581 (m=15)."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Paul D. Hanna_, Mar 12 2006",
			"references": 25,
			"revision": 34,
			"time": "2021-06-29T02:30:59-04:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}