{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A181048",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 181048,
			"data": "8,6,6,9,7,2,9,8,7,3,3,9,9,1,1,0,3,7,5,7,3,9,9,5,1,6,3,8,8,2,8,7,0,7,1,3,6,5,2,1,7,5,3,6,7,3,4,5,2,4,4,9,0,4,3,3,5,0,3,1,8,3,8,9,1,7,6,3,9,3,5,1,4,1,0,9,4,1,3,2,9,0,5,5,7,5,0,4,0,3,4,6,3,4,0,8,9,6,8,7,0,5,2,1,8",
			"name": "Decimal expansion of (log(1+sqrt(2))+Pi/2)/(2*sqrt(2)) = Sum_{k\u003e=0} (-1)^k/(4*k+1).",
			"reference": [
				"Jolley, Summation of Series, Dover (1961) eq 82 page 16.",
				"Murray R. Spiegel, Seymour Lipschutz, John Liu. Mathematical Handbook of Formulas and Tables, 3rd Ed. Schaum's Outline Series. New York: McGraw-Hill (2009): p. 135, equation 21.17"
			],
			"link": [
				"Ivan Panchenko, \u003ca href=\"/A181048/b181048.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"J. M. Borwein, P. B. Borwein, and K. Dilcher, \u003ca href=\"http://www.jstor.org/stable/2324715\"\u003ePi, Euler numbers and asymptotic expansions\u003c/a\u003e, Amer. Math. Monthly, 96 (1989), 681-687.",
				"Eric W. Weisstein, \u003ca href=\"https://mathworld.wolfram.com/EulersSeriesTransformation.html\"\u003eEuler's Series Transformation\u003c/a\u003e.",
				"("
			],
			"formula": [
				"Equals (A093954 + A091648/sqrt(2))/2.",
				"Integral_{x = 0..1} 1/(1+x^4) = Sum_(k \u003e= 0} (-1)^k/(4*k+1) = (log(1+sqrt(2)) + Pi/2)/(2*sqrt(2)).",
				"1 - 1/5 + 1/9 - 1/13 + 1/17 - ... = (Pi*sqrt(2))/8 + (sqrt(2)*log(1 + sqrt(2)))/4 = (Pi + 2*log(1 + sqrt(2)))/(4 sqrt(2)). The first two are the formulas as given in Spiegel et al., the third is how Mathematica rewrites the infinite sum. - _Alonso del Arte_, Aug 11 2011",
				"Let N be a positive integer divisible by 4. We have the asymptotic expansion 2*( (log(1 + sqrt(2)) + Pi/2)/(2*sqrt(2)) - Sum_{k = 0..N/4 - 1} (-1)^k/(4*k + 1) ) ~ 1/N + 1/N^2 - 3/N^3 - 11/N^4 + 57/N^5 + 361/N^6 - - ..., where the sequence of coefficients [1, 1, -3, -11, 57, 361, ...] is A188458. This follows from Borwein et al., Lemma 2 with f(x) = 1/x and then set x = N/4 and h = 1/4. An example is given below. Cf. A181049. - _Peter Bala_, Sep 23 2016",
				"Equals Sum_{n \u003e= 0} 2^(n-1)*n!/(Product_{k = 0..n} 4*k + 1) = Sum_{n \u003e= 0} 2^(n-1)*n!/A007696(n+1) (apply Euler's series transformation to Sum_{k \u003e= 0} (-1)^k/(4*k + 1)). - _Peter Bala_, Dec 01 2021"
			],
			"example": [
				"0.86697298733991103757399516388287071365217536734524490433....",
				"At N = 100000 the truncated series Sum_{k = 0..N/4 - 1} (-1)^k/(4*k + 1) ) = 1.7339(3)5974(5)7982(5)075(25)79(846)27(404)7... to 32 digits The bracketed numbers show where this decimal expansion differs from that of 2*A181048. The numbers 1, 1, -3, -11, 57, 361 must be added to the bracketed numbers to give the correct decimal expansion to 32 digits: 2*( (log(1 + sqrt(2)) + Pi/2)/(2*sqrt(2)) ) = 1.7339(4)5974(6)7982(2)075(14) 79(903)27(765)7.... - _Peter Bala_, Sep 23 2016"
			],
			"mathematica": [
				"RealDigits[(Pi Sqrt[2])/8 + (Sqrt[2] Log[1 + Sqrt[2]])/4, 10, 100][[1]] (* _Alonso del Arte_, Aug 11 2011 *)"
			],
			"program": [
				"(PARI) (log(1+sqrt(2))+Pi/2)/(2*sqrt(2)) \\\\ _G. C. Greubel_, Jul 05 2017",
				"(PARI) (asinh(1)+Pi/2)/sqrt(8) \\\\ _Charles R Greathouse IV_, Jul 06 2017"
			],
			"xref": [
				"Cf. A001586, A003881, A091648, A093954, A113476, A181049, A181122, A188458."
			],
			"keyword": "cons,nonn",
			"offset": "0,1",
			"author": "_Jonathan D. B. Hodgson_, Oct 01 2010, Oct 06 2010",
			"references": 9,
			"revision": 38,
			"time": "2021-12-18T23:40:30-05:00",
			"created": "2010-10-02T03:00:00-04:00"
		}
	]
}