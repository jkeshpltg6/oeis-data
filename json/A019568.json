{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A019568",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 19568,
			"data": "2,3,7,12,16,24,31,39,47,44,60,71,79,79,87",
			"name": "a(n) = smallest k \u003e= 1 such that {1^n, 2^n, 3^n, ..., k^n} can be partitioned into two sets with equal sum.",
			"comment": [
				"a(n) is least integer k such that at least one signed sum of the first k n-th powers equals zero.",
				"a(n) \u003c 2^(n+1). The partition of the set {k: 0 \u003c= k \u003c 2^(n+1)} into two sets A,B according to the parity of the number of 1s in the binary expansion of k, has the property that Sum_{k in A} p(k) = Sum_{k in B} p(k) for any polynomial p of degree \u003c= n. Equivalently, if e(k) is the Thue-Morse sequence A106400, then Sum_{0 \u003c= k \u003c 2^m} e(k)p(k) = 0 for any polynomial p with deg(p) \u003c m. - _Pietro Majer_, Mar 14 2009"
			],
			"reference": [
				"Posting to sci.math Nov 11 1996 by fredh(AT)ix.netcom.com (Fred W. Helenius)."
			],
			"link": [
				"Sean A. Irvine, \u003ca href=\"https://github.com/archmageirvine/joeis/blob/master/src/irvine/oeis/a019/A019568.java\"\u003eJava program\u003c/a\u003e (github)",
				"Pietro Majer, \u003ca href=\"http://mathoverflow.net/questions/36995\"\u003eMathOverflow: Asymptotic growth of a certain integer sequence\u003c/a\u003e"
			],
			"formula": [
				"a(n) == 0 or 3 (mod 4) for n \u003e= 1 - _David W. Wilson_, Oct 20 2005"
			],
			"example": [
				"For n=1 and 2 we have: 1+2-3 = 0 (so a(1)=3), 1+4-9+16-25-36+49 = 0 (so a(2)=7).",
				"The sum of the ninth powers of 3 5 9 10 14 19 20 21 25 26 28 31 35 36 37 38 40 41 42 is half the sum of the ninth powers of 1..44, so a(9)=44. - _Don Reble_, Oct 21 2005",
				"Example: the signs (+--+-++--++-+--+) in (+0)-1-8+27-64+125+216-...+3375=0 are those of the expansion of Q(x):=(1-x)(1-x^2)(1-x^4)(1-x^8) = +1-x-x^2+x^3-..+x^15. Since (1-x)^4 divides Q(x), if S is the shift operator on sequences, the operator Q(S) has the fourth discrete difference (I-S)^4 as factor, hence annihilates the sequence of cubes. - _Pietro Majer_, Mar 14 2009"
			],
			"mathematica": [
				"Table[k = 1; found = False; While[s = Range[k]^n; sm = Total[s]; If[EvenQ[sm], sm = sm/2; found = MemberQ[Total /@ Subsets[s], sm]]; ! found, k++]; k, {n, 0, 4}] (* _T. D. Noe_, Apr 01 2014 *)"
			],
			"xref": [
				"Cf. A240070 (partitioned into 3 sets)."
			],
			"keyword": "nonn,more",
			"offset": "0,1",
			"author": "_Robert G. Wilson v_",
			"ext": [
				"More terms from _Don Reble_, Oct 21 2005",
				"Definition simplified by _Pietro Majer_, Mar 15 2009",
				"a(13)-a(14) from _Sean A. Irvine_, Mar 27 2019"
			],
			"references": 12,
			"revision": 29,
			"time": "2019-03-28T11:24:30-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}