{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001949",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1949,
			"id": "M1127 N0430",
			"data": "0,0,0,0,0,1,2,4,8,16,32,63,124,244,480,944,1856,3649,7174,14104,27728,54512,107168,210687,414200,814296,1600864,3147216,6187264,12163841,23913482,47012668,92424472,181701728,357216192,702268543,1380623604,2714234540",
			"name": "Solutions of a fifth-order probability difference equation.",
			"comment": [
				"This sequence is the case r = 5 in the solution to an r-th order probability difference equation that can be found in Eqs. (4) and (3) on p. 356 of Dunkel (1925). (Equation (3) follows equation (4) in the paper!) For r = 2, we get a shifted version of A000071. For r = 3, we get a shifted version of A008937. For r = 4, we get a shifted version of A107066. For r = 6, we get a shifted version of A172316. See also the table in A172119. - _Petros Hadjicostas_, Jun 15 2019"
			],
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A001949/b001949.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"O. Dunkel, \u003ca href=\"http://www.jstor.org/stable/2298801\"\u003eSolutions of a probability difference equation\u003c/a\u003e, Amer. Math. Monthly, 32 (1925), 354-370; see pp. 356 and 369.",
				"T. Langley, J. Liese, and J. Remmel, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL14/Langley/langley2.html\"\u003eGenerating Functions for Wilf Equivalence Under Generalized Factor Order\u003c/a\u003e, J. Int. Seq. 14 (2011), Article #11.4.2.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,0,0,0,0,-1)"
			],
			"formula": [
				"For n \u003e= 6, a(n+1) = 2*a(n) - a(n-5).",
				"G.f.: x^5 / ( (x-1)*(x^5 + x^4 + x^3 + x^2 + x - 1) ).",
				"a(n) = Sum_{k=1..n-4} Sum_{j=0..floor((n-k-4)/5)} (-1)^j*binomial(n-5*j-5, k-1)*binomial(n-k-5*j-4, j). - _Vladimir Kruchinin_, Oct 19 2011",
				"4*a(n) = A000322(n+1) - 1. - _R. J. Mathar_, Aug 16 2017",
				"From _Petros Hadjicostas_, Jun 15 2019: (Start)",
				"a(n) = 1 + a(n-1) + a(n-2) + a(n-3) + a(n-4) + a(n-5) for n \u003e= 5. (See Eq. (4) and the Theorem with r = 5 on p. 356 of Dunkel (1925).)",
				"a(n) = T(n - 5, 5) for n \u003e= 5, where T(n, k) = Sum_{j = 0..floor(n/(k+1))} (-1)^j * binomial(n - k*j, n - (k+1)*j) * 2^(n - (k+1)*j) for 0 \u003c= k \u003c= n. This is _Richard Choulet_'s formula in A172119.",
				"(End)"
			],
			"maple": [
				"A001949:=1/(z-1)/(z**5+z**4+z**3+z**2+z-1); # _Simon Plouffe_ in his 1992 dissertation"
			],
			"mathematica": [
				"t={0,0,0,0,0};Do[AppendTo[t,t[[-5]]+t[[-4]]+t[[-3]]+t[[-2]]+t[[-1]]+1],{n,40}];t (* _Vladimir Joseph Stephan Orlovsky_, Jan 21 2012 *)",
				"LinearRecurrence[{2,0,0,0,0,-1},{0,0,0,0,0,1},40] (* _Harvey P. Dale_, Jan 17 2015 *)"
			],
			"program": [
				"(Maxima)",
				"a(n):=sum(sum((-1)^j*binomial(n-5*j-5,k-1)*binomial(n-k-5*j-4,j),j,0,(n-k-4)/5),k,1,n-4); /* _Vladimir Kruchinin_, Oct 19 2011 */",
				"(PARI) x='x+O('x^99); concat(vector(5), Vec(x^5/((x-1)*(x^5+x^4+x^3+x^2+x-1)))) \\\\ _Altug Alkan_, Oct 04 2017"
			],
			"xref": [
				"Column k = 1 of A141020 (with a different offset) and second main diagonal of A141021 (with no zeros).",
				"Column k = 5 of A172119."
			],
			"keyword": "nonn,easy",
			"offset": "0,7",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Name edited by _Petros Hadjicostas_, Jun 15 2019"
			],
			"references": 11,
			"revision": 82,
			"time": "2021-03-12T22:32:35-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}