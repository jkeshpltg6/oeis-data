{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A113404",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 113404,
			"data": "6,90,630,660,1170,2190,3780,6420,8940,9030,13260,16470,24150,28800,29610,39990,56580,56910,71610,83460,94530,114450,157830,159060,171180,177360,190500,197910,268050,315840,395520,435240,440910,513570,536010,539310,557340,635130",
			"name": "Record gaps between prime quadruplets.",
			"comment": [
				"Prime quadruplets (p, p+2, p+6, p+8) are densest permissible constellations of 4 primes (A007530). By the Hardy-Littlewood k-tuple conjecture, average gaps between prime k-tuples are O(log^k(p)), with k=4 for quadruplets. If a gap is larger than all preceding gaps, we call it a maximal gap, or a record gap. Maximal gaps may be significantly larger than average gaps. This sequence suggests that maximal gaps between prime quadruplets are O(log^5(p)). - _Alexei Kourbatov_, Jan 04 2012"
			],
			"link": [
				"Alexei Kourbatov, \u003ca href=\"/A113404/b113404.txt\"\u003eTable of n, a(n) for n = 1..71\u003c/a\u003e",
				"T. Forbes, Norman Luhn \u003ca href=\"http://www.pzktupel.de/ktuplets\"\u003ePrime k-tuplets\u003c/a\u003e",
				"G. H. Hardy and J. E. Littlewood, \u003ca href=\"https://doi.org/10.1007/BF02403921\"\u003eSome problems of 'Partitio numerorum'; III: On the expression of a number as a sum of primes\u003c/a\u003e, Acta Math., Vol. 44, No. 1 (1923), pp. 1-70.",
				"Alexei Kourbatov, \u003ca href=\"http://www.javascripter.net/math/primes/maximalgapsbetweenktuples.htm#4tuples\"\u003eMaximal gaps between prime k-tuples\u003c/a\u003e (graphs, more terms)",
				"A. Kourbatov, \u003ca href=\"http://arxiv.org/abs/1301.2242\"\u003eMaximal gaps between prime k-tuples: a statistical approach\u003c/a\u003e, arXiv preprint arXiv:1301.2242 [math.NT], 2013. - From _N. J. A. Sloane_, Feb 09 2013",
				"Alexei Kourbatov, \u003ca href=\"http://arxiv.org/abs/1309.4053\"\u003eTables of record gaps between prime constellations\u003c/a\u003e, arXiv preprint arXiv:1309.4053 [math.NT], 2013.",
				"Alexei Kourbatov, \u003ca href=\"http://arxiv.org/abs/1401.6959\"\u003eThe distribution of maximal prime gaps in Cramer's probabilistic model of primes\u003c/a\u003e, arXiv preprint arXiv:1401.6959 [math.NT], 2014.",
				"Alexei Kourbatov and Marek Wolf, \u003ca href=\"http://arxiv.org/abs/1901.03785\"\u003ePredicting maximal gaps in sets of primes\u003c/a\u003e, arXiv preprint arXiv:1901.03785 [math.NT], 2019.",
				"Eric Weisstein's World of Mathematics, , \u003ca href=\"http://mathworld.wolfram.com/k-TupleConjecture.html\"\u003ek-Tuple Conjecture\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, , \u003ca href=\"http://mathworld.wolfram.com/Hardy-LittlewoodConstants.html\"\u003eHardy-Littlewood Constants\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, , \u003ca href=\"http://mathworld.wolfram.com/PrimeConstellation.html\"\u003ePrime Constellation\u003c/a\u003e"
			],
			"formula": [
				"From _Alexei Kourbatov_, Jan 04 2012: (Start)",
				"(1) Upper bound: gaps between prime quadruplets (p, p+2, p+6, p+8) are smaller than 0.241*(log p)^5, where p is the prime at the end of the gap.",
				"(2) Estimate for the actual size of the maximal gap that ends at p: maximal gap = a*(log(p/a)-0.55), where a = 0.241*(log p)^4 is the average gap between quadruplets near p, as predicted by the Hardy-Littlewood k-tuple conjecture.",
				"Formulas (1) and (2) are asymptotically equal as p tends to infinity. However, (1) yields values greater than all known gaps, while (2) yields \"good guesses\" that may be either above or below the actual size of known maximal gaps.",
				"Both formulas (1) and (2) are derived from the Hardy-Littlewood k-tuple conjecture via probability-based heuristics relating the expected maximal gap size to the average gap. Neither of the formulas has a rigorous proof (the k-tuple conjecture itself has no formal proof either). In both formulas, the constant ~0.241 is reciprocal to the Hardy-Littlewood 4-tuple constant 4.15118... (End)"
			],
			"example": [
				"The first prime quadruplets are (5,7,11,13) and (11,13,17,19), so a(1)=11-5=6. The next quadruplet is (101,103,107,109), so a(2)=101-11=90. The following quadruplet is 191,193,197,199 so 90 remains the record and no terms are added."
			],
			"xref": [
				"A229907 lists initial primes in quadruplets preceding the maximal gaps. A113403 lists the corresponding primes at the end of the maximal gaps. Cf. A008407, A007530."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Bernardo Boncompagni_, Oct 28 2005",
			"ext": [
				"Terms 159060 to 635130 added by _Alexei Kourbatov_, Jan 04 2012"
			],
			"references": 12,
			"revision": 38,
			"time": "2021-09-11T01:07:05-04:00",
			"created": "2006-01-24T03:00:00-05:00"
		}
	]
}