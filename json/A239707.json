{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A239707",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 239707,
			"data": "0,1,1,2,1,3,1,3,2,3,1,5,1,3,3,4,1,5,1,4,2,3,1,7,2,3,3,5,1,7,1,5,3,3,2,8,1,3,3,7,1,6,1,5,5,3,1,9,2,4,3,5,1,6,2,7,3,3,1,10,1,3,5,6,3,7,1,5,2,7,1,11,1,3,5,5,2,6,1,9,3,3,1,9,3,3,2,7,1,11,3,4,2,3,3,11,1,5,5,7",
			"name": "Number of bases b for which the base-b alternate digital sum of n is 0.",
			"comment": [
				"For the definition of the alternate digital sum, see A055017 or A225693.",
				"For reference: we write altDigitSum_b(x) for the base-b alternate digital sum of x according to A055017 (with a general base b).",
				"The number of counted bases includes the special base 1. The base-1 expansion of a natural number is defined as 1=1_1, 2=11_1, 3=111_1 and so on. As a result, the base-1 alternate digital sum of n is 0, if n is even, and is 1, if n is odd. If we exclude the base b = 1, the resulting sequence is 0, 0, 1, 1, 1, 2, 1, 2, 2, 2, 1, 4, 1, 2, ... . The properties of this sequence are very similar, but the relation to the prime numbers is less strict.",
				"For b := n - 1, we get altDigitSum_b(n) = 0, thus a(n) \u003e= 1 for all n \u003e 1.",
				"For even n \u003e 2, we get altDigitSum_1(n) = 0, thus a(n) \u003e= 2.",
				"For bases b which satisfy floor(n/2) \u003c b \u003c n - 1, we have altDigitSum_b(n)\u003e 0, thus floor((n+2)/2) is a upper bound for a(n).",
				"If b is a base such that the base-b alternate digital sum of n is 0, then b + 1 is a divisor of n, thus the number of such bases is limited by the number of divisors of n (see Formula section).",
				"If b + 1 is a divisor of n which satisfy b + 1 \u003e= sqrt(n), then altDigitSum_b(n) = 0. This leads to a lower bound for a(n) (see Formula section).",
				"If b + 1 is a divisor of n, then b is not necessarily a base such that the base-b alternate digital sum of n is 0. Example: 4, 5 and 8 are divisors of 200, but altDigitSum_3(200) = 4, altDigitSum_4(200) = -5, altDigitSum_7(200) = 8.",
				"The first n with a(n) = 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, ... are n = 2, 4, 6, 16, 12, 42, 24, 36, 48, 60, ... ."
			],
			"link": [
				"Hieronymus Fischer, \u003ca href=\"/A239707/b239707.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 0, if and only if n=1.",
				"a(n) = 1, if and only if n is a prime number.",
				"a(n) \u003e 1, if and only if n is a composite number.",
				"a(n) = 2, if and only if n is the product of two primes (including squares of primes).",
				"a(n) \u003c= sigma_0(n) - 1, equality holds at least for primes and squares of primes.",
				"a(n) \u003e= floor((sigma_0(n) + 1)/2), for n \u003e 1."
			],
			"example": [
				"a(1) = 0, since altDigitSum_b(1) \u003e 0 for all b \u003e 0.",
				"a(2) = 1, since altDigitSum_1(2) = 0 (because of 2 = 11_1), and altDigitSum_2(2) = -1 (because of 2 = 10_2), and altDigitSum_b(2) = 2 for all b \u003e 2.",
				"a(3) = 1, since altDigitSum_1(3) = 1 (because of 3 = 111_1), and altDigitSum_2(3) = 0 (because of 3 = 11_2), and altDigitSum_3(3) = -1 (because of 3 = 10_3), and altDigitSum_b(3) = 3 for all b \u003e 3.",
				"a(4) = 2, since altDigitSum_1(4) = 0 (because of 3 = 1111_1), and altDigitSum_2(4) = 1 (because of 4 = 100_2), and altDigitSum_3(4) = 0 (because of 4 = 11_3), and altDigitSum_4(4) = -1 (because of 4 = 10_4), and altDigitSum_b(4) = 3 for all b \u003e 4."
			],
			"program": [
				"(Smalltalk)",
				"\"\u003e Version 1: simple calculation for small numbers.",
				"  Answer the number of bases b for which the alternate digital sum of n in base b is 0.",
				"  Valid for bases b \u003e 0.",
				"  Using altDigitalSumRight from A055017.",
				"  Usage: n numOfBasesWithAltDigitalSumEQ0",
				"  Answer: a(n)\"",
				"numOfBasesWithAltDigitalSumEQ0",
				"  | b q numBases |",
				"  self \u003c 2 ifTrue: [^0].",
				"  numBases := 1.",
				"  q := self // 2.",
				"  b := 1.",
				"  [b \u003c q] whileTrue:[",
				"    (self altDigitalSumRight: b) = 0",
				"    ifTrue: [numBases := numBases + 1].",
				"    b := b + 1].",
				"  ^numBases",
				"-----------",
				"\"\u003e Version 2: accelerated calculation for large numbers.",
				"  Answer the number of bases b for which the alternate digital sum of n in base b is 0.",
				"  Valid for bases b \u003e 0.",
				"  Using altDigitalSumRight from A055017.",
				"  Usage: n numOfBasesWithAltDigitalSumEQ0",
				"  Answer: a(n)\"",
				"numOfBasesWithAltDigitalSumEQ0",
				"  | numBases div b |",
				"  div := self divisors.",
				"  numBases := 0.",
				"  2 to: div size do: [ :i | b := (div at: i) - 1.",
				"    (self altDigitalSumRight: b) = 0",
				"    ifTrue: [numBases := numBases + 1]].",
				"  ^numBases"
			],
			"xref": [
				"Cf. A055017, A225693, A187813.",
				"Cf. A239703, A239704, A239705, A239706.",
				"Cf. A000040, A000005 (definition of sigma_0(n))."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Hieronymus Fischer_, Mar 31 2014",
			"references": 6,
			"revision": 30,
			"time": "2018-09-20T00:30:51-04:00",
			"created": "2014-04-15T02:51:29-04:00"
		}
	]
}