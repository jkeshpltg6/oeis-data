{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A319864",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 319864,
			"data": "1,1,2,1,1,1,3,2,1,1,1,1,1,2,4,1,2,1,1,3,1,1,1,1,1,3,1,1,2,1,5,1,1,2,2,1,1,1,1,1,3,1,1,1,1,2,1,2,1,1,1,1,3,1,1,1,1,1,2,2,1,1,6,1,1,1,1,1,2,1,2,2,1,2,1,1,1,1,1,1,1,1,3,3,1,2,1,1,1,1,1,4,2,1,1,1,2,4,1,1,1,1,1,2,1,3,3,1,1",
			"name": "Exponents of the final nontrivial entry of the iterated Stern sequence; a(n) = log_2 min{s^k(n) : k \u003e 0, s^k(n) \u003e 1}, where s(n) = A002487(n).",
			"comment": [
				"Let s(n) = A002487(n). Since s(n) \u003c n for n \u003e 1, iterating A002487 from any starting point eventually yields the fixed point 1 = s(1). Since s^-1(1) consists of the powers of 2, min{s^k(n) : k \u003e 0, s^k(n) \u003e 1} is a nontrivial power of 2 for any n \u003e 1. Hence, the entries of this sequence are integers.",
				"Since a(2^m) = m, every positive integer appears in this sequence.",
				"Question: What is the asymptotic density of the number 1 in this sequence? Of the first 10^6 entries, more than 74% are 1."
			],
			"example": [
				"Letting s(m) = A002487(m), we have s(7) = 3, s(3) = 2, and s(2) = 1. Hence, a(7) = log_2(2) = 1."
			],
			"mathematica": [
				"s[n_] := If[n\u003c2, n, If[EvenQ[n], s[n/2], s[(n-1)/2] + s[(n+1)/2]]];  a[n_] := Module[{nn = s[n]}, If[nn==1, Log2[n], a[nn]]]; Array[a, 100, 2] (* _Amiram Eldar_, Nov 22 2018 *)"
			],
			"program": [
				"(Python)",
				"from math import log",
				"def s(n): return n if n\u003c2 else s(n//2) if n%2==0 else s((n-1)//2)+s((n+1)//2)",
				"def a(n): nn = s(n); return int(log(n,2)) if nn==1 else a(nn)",
				"print([a(n) for n in range(2, 100)])",
				"(PARI) s(n) = if( n\u003c2, n\u003e0, s(n\\2) + if( n%2, s(n\\2 + 1))); \\\\ A002487",
				"a(n) = while((nn = s(n)) != 1, n = nn); valuation(n, 2); \\\\ _Michel Marcus_, Nov 23 2018"
			],
			"xref": [
				"Cf. A002487."
			],
			"keyword": "nonn",
			"offset": "2,3",
			"author": "_Oliver Pechenik_, Sep 29 2018",
			"references": 0,
			"revision": 26,
			"time": "2019-01-14T10:04:27-05:00",
			"created": "2019-01-14T10:04:27-05:00"
		}
	]
}