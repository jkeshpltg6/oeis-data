{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003472",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3472,
			"id": "M4718",
			"data": "1,10,60,280,1120,4032,13440,42240,126720,366080,1025024,2795520,7454720,19496960,50135040,127008768,317521920,784465920,1917583360,4642570240,11142168576,26528972800,62704844800,147220070400",
			"name": "a(n) = 2^(n-4)*C(n,4).",
			"comment": [
				"Number of 4D hypercubes in n-dimensional hypercube. - _Henry Bottomley_, Apr 14 2000",
				"With four leading zeros, binomial transform of C(n,4). - _Paul Barry_, Apr 10 2003",
				"If X_1, X_2, ..., X_n is a partition of a 2n-set X into 2-blocks, then, for n\u003e3, a(n) is equal to the number of (n+4)-subsets of X intersecting each X_i (i=1,2,...,n). - _Milan Janjic_, Jul 21 2007"
			],
			"reference": [
				"M. Abramowitz and I. A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, 1964 (and various reprintings), p. 796.",
				"Clifford A. Pickover, The Math Book, From Pythagoras to the 57th Dimension, 250 Milestones in the History of Mathematics, Sterling Publ., NY, 2009, page 282.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A003472/b003472.txt\"\u003eTable of n, a(n) for n = 4..400\u003c/a\u003e",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"H. J. Brothers, \u003ca href=\"http://www.brotherstechnology.com/math/pascals-prism.html\"\u003ePascal's Prism: Supplementary Material\u003c/a\u003e.",
				"Herbert Izbicki, \u003ca href=\"http://resolver.sub.uni-goettingen.de/purl?GDZPPN00247686X\"\u003eÜber Unterbaeume eines Baumes\u003c/a\u003e, Monatshefte für Mathematik,  Vol. 74 (1970), pp. 56-62.",
				"Herbert Izbicki, \u003ca href=\"http://dx.doi.org/10.1007/BF01298302\"\u003eÜber Unterbaeume eines Baumes\u003c/a\u003e, Monatshefte für Mathematik, Vol. 74 (1970), pp. 56-62.",
				"Milan Janjic, \u003ca href=\"https://pmf.unibl.org/wp-content/uploads/2017/10/enumfor.pdf\"\u003eTwo Enumerative Functions\u003c/a\u003e.",
				"Milan Janjic and Boris Petkovic, \u003ca href=\"http://arxiv.org/abs/1301.4550\"\u003eA Counting Function\u003c/a\u003e, arXiv 1301.4550 [math.CO], 2013.",
				"C. W. Jones, J. C. P. Miller, J. F. C. Conn and R. C. Pankhurst, \u003ca href=\"http://dx.doi.org/10.1017/S0080454100006579\"\u003eTables of Chebyshev polynomials\u003c/a\u003e Proc. Roy. Soc. Edinburgh. Sect. A., Vol. 62, No. 2 (1946), pp. 187-203.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992, arXiv:0911.4975 [math.NT], 2009.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992.",
				"John Riordan and N. J. A. Sloane, \u003ca href=\"/A003471/a003471_1.pdf\"\u003eCorrespondence, 1974\u003c/a\u003e.",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials\u003c/a\u003e.",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (10,-40,80,-80,32)."
			],
			"formula": [
				"a(n) = 2*a(n-1) + A001789(n-1).",
				"From _Paul Barry_, Apr 10 2003: (Start)",
				"O.g.f.: x^4/(1-2*x)^5.",
				"E.g.f.: exp(2*x)(x^4/4!) (with 4 leading zeros). (End)",
				"a(n) = Sum_{i=4..n} binomial(i,4)*binomial(n,i). Example: for n=7, a(7) = 1*35 + 5*21 + 15*7 + 35*1 = 280. - _Bruno Berselli_, Mar 23 2018",
				"From _Amiram Eldar_, Jan 06 2022: (Start)",
				"Sum_{n\u003e=4} 1/a(n) = 20/3 - 8*log(2).",
				"Sum_{n\u003e=4} (-1)^n/a(n) = 216*log(3/2) - 260/3. (End)"
			],
			"maple": [
				"A003472:=-1/(2*z-1)**5; # conjectured by _Simon Plouffe_ in his 1992 dissertation",
				"seq(binomial(n,4)*2^(n-4),n=4..24); # _Zerinvary Lajos_, Jun 12 2008"
			],
			"mathematica": [
				"Table[2^(n-4) Binomial[n,4],{n,4,50}] (* or *) LinearRecurrence[{10,-40,80,-80,32},{1,10,60,280,1120},50] (* _Harvey P. Dale_, May 27 2017 *)"
			],
			"program": [
				"(MAGMA) [2^(n-4)*Binomial(n, 4): n in [4..30]]; // _Vincenzo Librandi_, Oct 16 2011",
				"(PARI) a(n)=binomial(n,4)\u003c\u003c(n-4) \\\\ _Charles R Greathouse IV_, May 18 2015",
				"(Sage) [2^(n-4)*binomial(n,4) for n in (4..30)] # _G. C. Greubel_, Aug 27 2019",
				"(GAP) List([4..30], n-\u003e 2^(n-4)*Binomial(n,4)); # _G. C. Greubel_, Aug 27 2019"
			],
			"xref": [
				"Cf. A001787, A001788, A001789.",
				"a(n) = A038207(n,4)."
			],
			"keyword": "nonn,easy,nice,changed",
			"offset": "4,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _James A. Sellers_, Apr 15 2000"
			],
			"references": 26,
			"revision": 82,
			"time": "2022-01-06T03:40:30-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}