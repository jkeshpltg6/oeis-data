{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A257231",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 257231,
			"data": "1,1,4,1,4,1,5,9,4,1,4,1,16,9,4,1,4,1,16,9,4,1,7,25,16,9,4,1,4,1,36,25,16,9,4,1,16,9,4,1,4,1,16,9,4,1,36,25,16,9,4,1,36,25,16,9,4,1,4,1,36,25,16,9,4,1,16,9,4,1,4,1,36,25,16,9,4,1,16,9,4,1,36,25,16,9,4",
			"name": "a(n) = n^2 mod p where p is the least prime greater than n.",
			"comment": [
				"Conjecture: a(n) is always a positive square, except for the terms 5, 7, 69, 42 and 17 given by n = 7, 23, 113, 114, and 115 respectively. It is easy to show that nonsquare terms are in [p, q) iff p and q are consecutive primes and q-p \u003e sqrt(q). There are no gaps between consecutive primes greater than sqrt(q) for 127 \u003c q \u003c 4*10^18 (see Nicely's table of maximal prime gaps)."
			],
			"link": [
				"Chris Boyd, \u003ca href=\"/A257231/b257231.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Thomas R. Nicely, \u003ca href=\"https://faculty.lynchburg.edu/~nicely/gaps/gaplist.html\"\u003eFirst occurrence prime gaps\u003c/a\u003e [For local copy see A000101]",
				"Tomás Oliveira e Silva, \u003ca href=\"http://sweet.ua.pt/tos/gaps.html\"\u003eGaps between consecutive primes\u003c/a\u003e"
			],
			"example": [
				"a(23) = 7 because 23^2 mod 29 = 7.",
				"a(24) = 25 because 24^2 mod 29 = 25."
			],
			"mathematica": [
				"Table[Mod[n^2, NextPrime@ n], {n, 87}] (* _Michael De Vlieger_, Apr 19 2015 *)",
				"Table[PowerMod[n,2,NextPrime[n]],{n,90}] (* _Harvey P. Dale_, May 24 2015 *)"
			],
			"program": [
				"(PARI) a(n)=n^2%nextprime(n+1)",
				"(MAGMA) [n^2 mod NextPrime(n): n in [1..80]]; // _Vincenzo Librandi_, Apr 19 2015"
			],
			"xref": [
				"Cf. A257230."
			],
			"keyword": "nonn,easy",
			"offset": "1,3",
			"author": "_Chris Boyd_, Apr 19 2015",
			"references": 2,
			"revision": 24,
			"time": "2021-10-28T12:37:08-04:00",
			"created": "2015-04-22T16:24:23-04:00"
		}
	]
}