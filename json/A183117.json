{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A183117",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 183117,
			"data": "0,1,3,7,19,53,153,451,1339,3997,11961,35835,107435,322197,966425,2899027,8696699,26089517,78267673,234801675,704402987,2113205861,6339612857,19018831395,57056483259,171169433149,513508274169,1540524784027,4621574293547,13864722791605,41594168239321",
			"name": "Magnetic Tower of Hanoi, number of moves of disk number k, optimally solving the [NEUTRAL ; NEUTRAL ; NEUTRAL] pre-colored puzzle.",
			"comment": [
				"The Magnetic Tower of Hanoi puzzle is described in link 1 listed below. The Magnetic Tower is pre-colored. Pre-coloring is [NEUTRAL ; NEUTRAL ; NEUTRAL],  given in [Source ; Intermediate ; Destination] order. Thus, the tower in this case is \"natural\" or \"free\". The solution algorithm producing the sequence is optimal (the sequence presented gives the minimum number of moves to solve the puzzle for the given pre-coloring configuration). Optimal solutions are discussed and their optimality is proved in link 2 listed below.",
				"Disk numbering is from largest disk (k = 1) to smallest disk (k = N).",
				"The above-listed \"original\" sequence generates a \"partial-sums\" sequence - describing the total number of moves required to solve the puzzle.",
				"Number of moves of disk k, for large k, is close to (20/33)*3^(k-1) ~ 0.606*3^(k-1). Series designation: P606(k)."
			],
			"reference": [
				"Uri Levy, The Magnetic Tower of Hanoi, Journal of Recreational Mathematics, Volume 35 Number 3 (2006), 2010, pp. 173."
			],
			"link": [
				"Uri Levy, \u003ca href=\"http://arxiv.org/abs/1003.0225\"\u003eThe Magnetic Tower of Hanoi\u003c/a\u003e, arXiv:1003.0225 [math.CO], 2010.",
				"Uri Levy, \u003ca href=\"http://arxiv.org/abs/1011.3843\"\u003eMagnetic Towers of Hanoi and their Optimal Solutions\u003c/a\u003e, arXiv:1011.3843 [math.CO], 2010.",
				"Web applet \u003ca href=\"http://www.weizmann.ac.il/zemed/davidson_online/mtoh/MTOHeng.html\"\u003eto play The Magnetic Tower of Hanoi\u003c/a\u003e [Broken link]"
			],
			"formula": [
				"G.f. appears to be -x*(1+x)*(2*x^3+2*x^2+x-1)/((3*x-1)*(2*x^3+x^2-1)) with a(n) = 3*a(n-1) + a(n-2) - a(n-3) - 6*a(n-4) for n \u003e 5. - _Joerg Arndt_, Jan 03 2011",
				"Recurrence Relations (a(n)=P606(n) as in referenced paper):",
				"P606(n) = P636(n-1) + P636(n-2) + P909(n-2) + 2*3^(n-3) ; n \u003e= 3.",
				"Note:  P636(n) and P909(n) refer to the integer sequences described by A183115 and A183111 respectively.",
				"Closed-Form Expression:",
				"Define:",
				"λ1 = (1+sqrt(26/27))^(1/3) + (1-sqrt(26/27))^(1/3)",
				"λ2 = -0.5*λ1 + 0.5*i*((sqrt(27) + sqrt(26))^(1/3) - (sqrt(27) - sqrt(26))^(1/3))",
				"λ3 = -0.5*λ1 - 0.5*i*((sqrt(27) + sqrt(26))^(1/3) - (sqrt(27) - sqrt(26))^(1/3))",
				"AP = ((1/11)*λ2*λ3 - (3/11)*(λ2 + λ3) + (9/11))/((λ2 - λ1)*(λ3 - λ1))",
				"BP = ((1/11)*λ1*λ3 - (3/11)*(λ1 + λ3) + (9/11))/((λ1 - λ2)*(λ3 - λ2))",
				"CP = ((1/11)*λ1*λ2 - (3/11)*(λ1 + λ2) + (9/11))/((λ2 - λ3)*(λ1 - λ3))",
				"For n \u003e 1:",
				"P606(n) = (20/33)*3^(n-1) + 0.5*AP*((λ1+1)^2)*λ1^(n-1) + 0.5*BP*((λ2+1)^2)*λ2^(n-1) + 0.5*CP*(λ3+1)^2)*λ3^(n-1)."
			],
			"mathematica": [
				"L1 = Root[-2 - # + #^3\u0026, 1];",
				"L2 = Root[-2 - # + #^3\u0026, 3];",
				"L3 = Root[-2 - # + #^3\u0026, 2];",
				"AP = Root[-2 - 9# - 52 #^2 + 572 #^3\u0026, 1];",
				"BP = Root[-2 - 9# - 52 #^2 + 572 #^3\u0026, 3];",
				"CP = Root[-2 - 9# - 52 #^2 + 572 #^3\u0026, 2];",
				"a[0] = 0;",
				"a[n_] := (1/2) AP (L1+1)^2 L1^(n-1) + (1/2) BP (L2+1)^2 L2^(n-1) + (1/2) CP (L3+1)^2 L3^(n-1) + (20 3^(n-1))/33;",
				"Table[a[n] // Round, {n, 0, 30}] (* _Jean-François Alcover_, Dec 03 2018 *)"
			],
			"xref": [
				"A000244 \"Powers of 3\" is the sequence (also) describing the number of moves of the k-th disk solving [RED ; BLUE ; BLUE] or [RED ; RED ; BLUE] pre-colored Magnetic Tower of Hanoi puzzle.",
				"A183111 through A183125 are related sequences, all associated with various solutions of the pre-coloring variations of the Magnetic Tower of Hanoi."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Uri Levy_, Dec 31 2010",
			"references": 5,
			"revision": 34,
			"time": "2018-12-04T04:09:10-05:00",
			"created": "2010-12-25T08:06:53-05:00"
		}
	]
}