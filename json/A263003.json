{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A263003",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 263003,
			"data": "1,1,2,2,6,3,6,24,8,12,8,24,120,30,24,20,24,30,120,720,144,80,144,72,45,144,72,80,144,720,5040,840,360,360,336,144,240,240,252,144,360,336,360,840,5040,40320,5760,2016,1440,2880,1920,630,576,720,960,1152,448,720,576,2880,1152,630,1440,1920,2016,5760,40320,362880,45360,13440,7560,8640,12960,3456,2240,4320,3024,2160,8640,6480,1920,1680,1680,2160,4320,5184,1920,3024,2240,8640,6480,3456,7560,12960,13440,45360,362880",
			"name": "Partition array for the products of the hook lengths of Ferrers (Young) diagrams corresponding to the partitions of n, written in Abramowitz-Stegun order.",
			"comment": [
				"The sequence of row lengths is A000041: [1, 1, 2, 3, 5, 7, 11, 15, 22, 30, 42, ...] (partition numbers p(n)).",
				"For the ordering of this tabf array a(n,k) see Abramowitz-Stegun (A-St) ref. pp. 831-2.",
				"This is the array n!/A117506(n,k).",
				"For rows 1..15 of this irregular triangle see the W. Lang link.",
				"The row sums give A263004.",
				"The formula given below is the one obtained from the version given, e.g., in Wybourne's book for A117506(n, k). See also the Glass-Ng reference, Theorem 1, p. 701, which gives the same formula, after rewriting using also a Vandermonde determinant.",
				"In A. Young's third paper (Q.S.A. III, see A117506), Theorem V on p. 266, CP p. 363, f/n! (the present 1/a(n,k)) appears in the decomposition of 1 for each n, that is Sum_{k = 1..p(n)} 1/a(n,k) Sum_{j=1..d(n,k)} Y'(n,k,j) = 1, with d(n,k) = A117506(n,k), and the Young operators Y' for the standard tableaux for the k-th partition of n in A-St order.",
				"a(n,k) also appears as normalization to obtain the idempotents NP/a(n,k). See A. Young, Q.S.A. II, p. 366, CP p. 97: NP = (1/a(n,k)) (NP)^2 for each Young tableau of the shape given by the k-th partition of n in A-St order."
			],
			"reference": [
				"M. Abramowitz and I. A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, Tenth Printing, 1972, pp. 831-2.",
				"B. Wybourne, Symmetry principles and atomic spectroscopy, Wiley, New York, 1970, p. 9."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A263003/b263003.txt\"\u003eRows n = 0..30, flattened\u003c/a\u003e",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"Kenneth Glass and Chi-Keung Ng, \u003ca href=\"http://www.jstor.org/stable/4145043?seq=1#page_scan_tab_contents\"\u003eA Simple Proof of the Hook Length Formula\u003c/a\u003e, Am. Math. Monthly 111 (2004) 700 - 704.",
				"Wolfdieter Lang, \u003ca href=\"/A263003/a263003.pdf\"\u003eRows 1..15.\u003c/a\u003e"
			],
			"formula": [
				"a(n,k) = Product_{i=1..m(n,k)} (x_i)!/Det(x_i^(m(n,k) - j)) with the Vandermonde determinant for the variables x_i := lambda(n,k)_i + m(n,k) - i, for i, j = 1..m(n,k), where m(n,k) is the number of parts of the k-th partition of n denoted by lambda(n,k), in the A-St order (see above). Lambda(n,k)_i stands for the i-th part of the partition lambda(n,k), sorted in nonincreasing order (this is the reverse of the A-St notation for a partition)."
			],
			"example": [
				"The first rows of this irregular triangle are:",
				"n\\k   1    2    3    4    5   6    7   8   9   10   11",
				"0:    1",
				"1:    1",
				"2:    2    2",
				"3:    6    3    6",
				"4:   24    8   12    8   24",
				"5:  120   30   24   20   24  30  120",
				"6:  720  144   80  144   72  45  144  72  80  144  720",
				"...",
				"Note that the rows are in general not symmetric.",
				"See the W. Lang link for rows n = 1..15.",
				"a(6,6) is related to the (self-conjugate) partition (1, 2, 3) of n = 6, taken in reverse order (3, 2, 1) with the Ferrers (or Young) diagram",
				"   _ _ _",
				"  |_|_|_| and the hook length numbers   5  3  1 ...",
				"  |_|_|                                 3  1",
				"  |_|                                   1",
				"The product gives 5*3*1*3*1*1 = 45 = a(6,6)."
			],
			"maple": [
				"h:= l-\u003e (n-\u003e mul(mul(1+l[i]-j+add(`if`(l[k]\u003e=j, 1, 0),",
				"             k=i+1..n), j=1..l[i]), i=1..n))(nops(l)):",
				"g:= (n, i, l)-\u003e`if`(n=0 or i=1, [h([l[], 1$n])],",
				"               `if`(i\u003c1, [], [g(n, i-1, l)[],",
				"               `if`(i\u003en, [], g(n-i, i, [l[], i]))[]])):",
				"T:= n-\u003e g(n$2, [])[]:",
				"seq(T(n), n=0..10);  # _Alois P. Heinz_, Nov 05 2015"
			],
			"xref": [
				"Cf. A117506, A263004."
			],
			"keyword": "nonn,tabf,look",
			"offset": "0,3",
			"author": "_Wolfdieter Lang_, Oct 09 2015",
			"ext": [
				"Row n=0 prepended by _Alois P. Heinz_, Nov 05 2015"
			],
			"references": 3,
			"revision": 36,
			"time": "2018-05-08T15:11:56-04:00",
			"created": "2015-10-25T17:40:28-04:00"
		}
	]
}