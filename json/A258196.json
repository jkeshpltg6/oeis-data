{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A258196",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 258196,
			"data": "1,0,-1,-2,-1,2,0,2,0,0,1,0,2,-2,-1,0,-2,-2,0,0,0,0,2,0,-1,0,2,0,0,2,-1,2,0,2,0,0,-2,-2,0,0,0,-2,-2,0,1,0,0,-2,2,0,-2,2,-1,0,0,-2,2,2,2,0,0,0,2,0,2,0,0,0,0,0,-1,-2,-2,2,0,-2,0,2,-2",
			"name": "Expansion of f(-x^2) * phi(-x^3) in powers of x where phi(), f() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A258196/b258196.txt\"\u003eTable of n, a(n) for n = 0..2500\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(-1/12) * eta(q^2) * eta(q^3)^2 / eta(q^6) in powers of q.",
				"Euler transform of period 6 sequence [ 0, -1, -2, -1, 0, -2, ...].",
				"G.f.: Product_{k\u003e0} (1 - x^(2*k)) * (1 - x^(3*k)) / (1 + x^(3*k)).",
				"a(49*n + 18) = a(49*n + 25) = a(49*n + 32) = a(49*n + 39) = a(49*n + 46) = 0."
			],
			"example": [
				"G.f. = 1 - x^2 - 2*x^3 - x^4 + 2*x^5 + 2*x^7 + x^10 + 2*x^12 - 2*x^13 + ...",
				"G.f. = q - q^25 - 2*q^37 - q^49 + 2*q^61 + 2*q^85 + q^121 + 2*q^145 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ x^2] EllipticTheta[ 4, 0, x^3], {x, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A) * eta(x^3 + A)^2 / eta(x^6 + A), n))};",
				"(PARI) {a(n) = my(A, p, e, x, w=quadgen(-8)); if( n\u003c0, 0, n = 12*n + 1; A = factor(n); simplify( prod( k=1, matsize(A)[1], [p, e] = A[k,]; if( p\u003c5, 0, p%12 == 11, !(e%2), p%12 == 5, forstep(y = 3, sqrtint(2*p), 6, if( issquare(2*p - y^2, \u0026x), if( x%6==5, x=-x); x = (x-1)/6; break)); (-1)^(e*x) * [1, w, -1, 0, -1, -w, 1, 0][e%8+1], x=0; forstep(y = 0, sqrtint(p), 6, if( issquare(p - y^2, \u0026x), if( x%6==5, x=-x); x = [x-1, y]/6; break)); if( x==0, (-1)^(e\\2) * !(e%2), (-1)^(e*(x[1] + x[2])) * (e+1))))))};",
				"(PARI) q='q+O('q^99); Vec(eta(q^2)*eta(q^3)^2/eta(q^6)) \\\\ _Altug Alkan_, Aug 02 2018"
			],
			"xref": [
				"Cf. A204770."
			],
			"keyword": "sign",
			"offset": "0,4",
			"author": "_Michael Somos_, May 23 2015",
			"references": 2,
			"revision": 15,
			"time": "2021-03-12T22:24:48-05:00",
			"created": "2015-05-23T13:56:33-04:00"
		}
	]
}