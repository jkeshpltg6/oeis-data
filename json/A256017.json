{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A256017",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 256017,
			"data": "2,4,9,8,25,18,49,16,27,50,121,156,169,98,75,32,289,54,361,100,147,242,529,696,125,338,81,196,841,930,961,64,363,578,245,1332,1369,722,507,1640,1681,294,1849,484,2115,1058,2209,2544,343,250,867,676,2809,162",
			"name": "Least integer k \u003e n such that all divisors of n are exactly the first divisors of k in increasing order.",
			"comment": [
				"Gcd(n,k) = n and lcm(n,k) = k; a(n)/n is a prime number.",
				"a(n) is alternatively even and odd for n \u003e= 2. A001248 (squares of primes) is a subsequence."
			],
			"link": [
				"Michel Lagneau and Giovanni Resta, \u003ca href=\"/A256017/b256017.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (first 600 terms from Michel Lagneau)"
			],
			"formula": [
				"For p prime, a(p) = p^2. - _Michel Marcus_, Jun 02 2015"
			],
			"example": [
				"a(6)=18 because the divisors of 6 = {1,2,3,6} are the first divisors of 18 = {1,2,3,6,9,18}."
			],
			"maple": [
				"with(numtheory):nn:=100:",
				"for n from 1 to nn do:",
				"lst0:={}:x:=divisors(n):n0:=nops(x):ii:=0:",
				"  for k from 1 to 10^8 while (ii=0) do:",
				"   y:=divisors(k):n1:=nops(y):lst1:={}:",
				"    if n\u003c\u003ek and n1\u003e=n0 then",
				"    for i from 1 to n0 do",
				"    lst0:=lst0 union {x[i]}:",
				"    od:",
				"     for j from 1 to n0 do",
				"     lst1:=lst1 union {y[j]}:",
				"     od:",
				"     if lst0=lst1",
				"     then",
				"     printf(`%d, `,k):ii:=1:",
				"     else fi:fi:",
				"     od:",
				"    od:"
			],
			"mathematica": [
				"a[n_]:=If[n==1, 2, Block[{k= 2*n,f,d,m}, f = FactorInteger @n; If[1 == Length@f, f[[1,1]]^(1 + f[[1,2]]), d = Divisors@ n; m = Length@ d; While[ Take[ Divisors@ k, m] != d, k += n]; k]]]; Array[a, 1000] (* _Giovanni Resta_, Jun 01 2015 *)",
				"adn[n_]:=Module[{d=Divisors[n],k=n+1,len},len=Length[d];While[Length[ Divisors[ k]]\u003clen||Take[Divisors[k],len]!=d,k++];k]; Array[adn,60] (* _Harvey P. Dale_, Mar 05 2020 *)"
			],
			"program": [
				"(PARI) okd(k, d) = {my(dk = divisors(k)); (#dk \u003e= #d) \u0026\u0026 (d == vector(#d, i, dk[i]));}",
				"a(n) = {my(k = n+1); my(d = divisors(n)); while (! okd(k, d), k++); k;} \\\\ _Michel Marcus_, Jun 02 2015"
			],
			"xref": [
				"Cf. A001248."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Michel Lagneau_, Jun 01 2015",
			"references": 2,
			"revision": 32,
			"time": "2020-03-05T16:14:45-05:00",
			"created": "2015-06-04T17:00:54-04:00"
		}
	]
}