{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A210246",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 210246,
			"data": "1,-1,-2,2,40,104,-1232,-13168,16000,1483904,9695488,-151161088,-3287997440,146760704,866038110208,10263094740992,-169941494497280,-6324725967978496,-15215847186563072,2895126258819203072,54295929047166484480",
			"name": "Polylogarithm li(-n,-1/3) multiplied by (4^(n+1))/3.",
			"comment": [
				"Given n, consider the series s(n) = li(-n,-1/3) = SUM((-1)^k (k^n)/3^k) for k=0,1,2,... . Then a(n)=s(n)*(4^(n+1))/3. For more details, see A212846."
			],
			"link": [
				"Stanislav Sykora, \u003ca href=\"/A210246/b210246.txt\"\u003eTable of n, a(n) for n = 0..100\u003c/a\u003e",
				"OEIS-Wiki, \u003ca href=\"http://oeis.org/wiki/Eulerian_polynomials\"\u003eEulerian polynomials\u003c/a\u003e",
				"S. Sykora, \u003ca href=\"http://dx.doi.org/10.3247/SL1Math06.002\"\u003eFinite and Infinite Sums of the Power Series (k^p)(x^k)\u003c/a\u003e, Stan's Library Vol. I, April 2006, updated March 2012. See Eq.(29).",
				"Eric W. Weisstein, \u003ca href=\"http://mathworld.wolfram.com/Polylogarithm.html\"\u003e MathWorld: Polylogarithm\u003c/a\u003e"
			],
			"formula": [
				"Recurrence: s(n+1)=(-1/4)*SUM(C(n+1,i)*s(i)), where i=0,1,2,...,n, C(n,m) are binomial coefficients, and the starting value is s(0)=SUM((-1/3)^k)=3/4.",
				"From _Peter Bala_, Mar 12 2013: (Start)",
				"E.g.f.: A(x) = 4/(3 + exp(4*x)) = 1 - x - 2*x^2/2! + 2*x^3/3! + 40*x^4/4! + ....",
				"The compositional inverse (A(-x) - 1)^(-1) = x + 2*x^2/2 + 7*x^3/3 + 20*x^4/4 + 61*x^5/5 + ... is the logarithmic generating function for A015518.",
				"Recurrence equation: a(n+1) = -4*a(n) + 3*sum {k = 0..n} binomial(n,k)*a(k)*a(n-k), with a(0) = 1.",
				"(End)",
				"G.f.: 1 + x/Q(0), where Q(k) = 2*x*(k+1) - 1 + 3*x^2*(k+1)*(k+2)/Q(k+1) ; (continued fraction). - _Sergei N. Gladkovskii_, Sep 22 2013",
				"G.f.: 1/Q(0), where Q(k) = 1 + x*(k+1)/( 1 - 3*x*(k+1)/Q(k+1) ); (continued fraction). - _Sergei N. Gladkovskii_, Dec 17 2013",
				"E.g.f.: 2 - W(0), where W(k) = 1 + x/( 4*k+1 - x/( 1 + 4*x/( 4*k+3 - 4*x/W(k+1) ))); (continued fraction). - _Sergei N. Gladkovskii_, Oct 22 2014"
			],
			"example": [
				"a(5) = polylog(-5,-1/3)*4^6/3 = 104."
			],
			"maple": [
				"seq(add((-1)^(n-k)*combinat[eulerian1](n,k)*3^k,k=0..n),n=0..20); # _Peter Luschny_, Apr 21 2013"
			],
			"mathematica": [
				"Table[PolyLog[-n, -1/3] (4^(n+1))/3, {n, 30}] (* _T. D. Noe_, Mar 23 2012 *)",
				"a[ n_] := If[ n \u003c 1, Boole[n == 0], PolyLog[ -n, -1/3] 4^(n + 1) / 3]; (* _Michael Somos_, Nov 01 2014 *)"
			],
			"program": [
				"(PARI) /* See in A212846, run limnpq(nmax,1,3) */",
				"(PARI) x='x+O('x^66); Vec(serlaplace( 4/(3+exp(4*x)) )) \\\\ _Joerg Arndt_, Apr 21 2013"
			],
			"xref": [
				"Similar to A210244. Cf. A210247 (sign changes).",
				"Cf. A212846 (li(-n,-1/2)), A212847 (li(-n,-2/3)).",
				"CF. A213127 through A213157."
			],
			"keyword": "sign",
			"offset": "0,3",
			"author": "_Stanislav Sykora_, Mar 19 2012",
			"references": 35,
			"revision": 67,
			"time": "2015-01-11T00:02:54-05:00",
			"created": "2012-03-23T12:16:30-04:00"
		}
	]
}