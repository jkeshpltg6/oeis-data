{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A216118",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 216118,
			"data": "0,1,1,10,15,5,90,165,90,15,840,1750,1225,350,35,8400,19180,15750,5950,1050,70,90720,222264,204624,92610,22050,2646,126,1058400,2744280,2757720,1421490,411600,67620,5880,210,13305600,36162720,38980920,22203720,7408170,1496880,180180,11880,330",
			"name": "Triangle read by rows: T(n,k) is the number of stretching pairs in all permutations in S_{n,k} (=set of permutations in S_n with k cycles) (n \u003e= 3; 1 \u003c= k \u003c= n-2).",
			"comment": [
				"A stretching pair of a permutation p in S_n is a pair (i,j) (1 \u003c= i \u003c j \u003c= n) satisfying p(i) \u003c i \u003c j \u003c p(j). For example, for the permutation 31254 in S_5 the pair (2,4) is stretching because 1\u003c p(2) \u003c 2 \u003c 4 \u003c p(4) = 5.",
				"Number of entries in row n (n \u003e= 3) is n - 2.",
				"Sum of entries in row n is A216119(n).",
				"T(n,1) = A061206(n-3)."
			],
			"reference": [
				"E. Lundberg and B. Nagle, A permutation statistic arising in dynamics of internal maps. (submitted)"
			],
			"link": [
				"Muniru A Asiru, \u003ca href=\"/A216118/b216118.txt\"\u003eRows n=3..100 of triangle, flattened\u003c/a\u003e",
				"E. Clark and R. Ehrenborg, \u003ca href=\"http://dx.doi.org/10.1016/j.ejc.2008.11.014\"\u003eExplicit expressions for the extremal excedance statistic\u003c/a\u003e, European J. Combinatorics, 31, 2010, 270-279.",
				"J. Cooper, E. Lundberg, and B. Nagle, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v20i1p28\"\u003eGeneralized pattern frequency in large permutations\u003c/a\u003e, Electron. J. Combin. 20, 2013, #P28."
			],
			"formula": [
				"T(n,k) = binomial(n,4)*abs(Stirling1(n-2,k)).",
				"T(n,k) = binomial(n,4)*(-1)^(n-k)*Stirling1(n-2,k)."
			],
			"example": [
				"T(4,1) = 1, T(4,2) = 1 because 22 permutations in S_4 have no stretching pairs, the 1-cycle 3142 has the stretching pair (2,3) and the 2-cycle 2143 has the stretching pair (2,3).",
				"Triangle starts:",
				"    0;",
				"    1,    1;",
				"   10,   15,    5;",
				"   90,  165,   90,  15;",
				"  840, 1750, 1225, 350, 35;",
				"  ..."
			],
			"maple": [
				"with(combinat): T := proc (n, k) options operator, arrow: binomial(n, 4)*abs(stirling1(n-2, k)) end proc: for n from 3 to 12 do seq(T(n, k), k = 1 .. n-2) end do; # yields sequence in triangular form"
			],
			"mathematica": [
				"T[n_, k_] := Binomial[n, 4] * Abs[StirlingS1[n-2, k]]; Table[T[n, k], {n, 3, 12}, {k, 1, n-2}] // Flatten (* _Amiram Eldar_, Dec 13 2018 *)"
			],
			"program": [
				"(GAP) List([3..12],n-\u003eList([1..n-2],k-\u003eBinomial(n,4)*Stirling1(n-2,k))); # _Muniru A Asiru_, Dec 13 2018",
				"(PARI) {T(n,k) = (-1)^(n-k)*binomial(n,4)*stirling(n-2,k,1)};",
				"for(n=3, 10, for(k=1,n-2, print1(T(n,k), \", \"))) \\\\ _G. C. Greubel_, Dec 13 2018",
				"(MAGMA) [[(-1)^(n-k)*Binomial(n,4)*StirlingFirst(n-2,k): k in [1..n-2]]: n in [3..12]]; // _G. C. Greubel_, Dec 13 2018",
				"(Sage) [[binomial(n,4)*stirling_number1(n-2,k) for k in (1..n-2)] for n in (3..12)] # _G. C. Greubel_, Dec 13 2018"
			],
			"xref": [
				"Cf. A216119, A061206."
			],
			"keyword": "nonn,tabl",
			"offset": "3,4",
			"author": "_Emeric Deutsch_, Feb 26 2013",
			"references": 2,
			"revision": 27,
			"time": "2018-12-14T04:27:16-05:00",
			"created": "2013-02-26T18:48:32-05:00"
		}
	]
}