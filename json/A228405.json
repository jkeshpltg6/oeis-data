{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A228405",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 228405,
			"data": "0,1,1,0,1,2,2,2,3,5,0,2,4,7,12,4,4,6,10,17,29,0,4,8,14,24,41,70,8,8,12,20,34,58,99,169,0,8,16,28,48,82,140,239,408,16,16,24,40,68,116,198,338,577,985,0,16,32,56,96,164,280,478,816,1393,2378",
			"name": "Pellian Array, A(n, k) with numbers m such that 2*m^2 +- 2^k is a square, and their corresponding square roots, read downward by diagonals.",
			"comment": [
				"The left column, A(n,0), is A000129(n), Pell Numbers.",
				"The top row, A(0,k), is A077957(k) plus an initial 0, which is the inverse binomial transform of A000129.",
				"These may be considered initializing values, or results, depending the perspective taken, since there are several ways to generate the array. See Formula section for details.",
				"The columns of the array hold all values, in sequential order, of numbers m such that 2m^2 + 2^k or  2m^2 - 2^k are squares, and their corresponding square roots in the next column, which then form the \"next round\" of m values for k+1.",
				"For example A(n,0) are numbers such that 2m^2 +- 1 are squares, the integer square roots of each are in A(n,1), which are then numbers m such that 2m^2 +- 2 are squares, with those square roots in A(n,2), etc.",
				"A(n, k)/A(n,k-2) = 2; A(n,k)/A(n,k-1) converges to sqrt(2) for large n.",
				"A(n,k)/A(n-1,k) converges to 1 + sqrt(2) for large n.",
				"The other columns of this array hold current OEIS sequences as follows:",
				"  A(n,1) = A001333(n);   A(n,2) = A163271(n);  A(n,3) = A002203(n);",
				"Bisections of these column-oriented sequences also appear in the OEIS, corresponding to the even and odd rows of the array, which in turn correspond to the two different recursive square root equations in the formula section below.",
				"Farey fraction denominators interleave columns 0 and 1, and the corresponding numerators interleave columns 1 and 2, for approximating sqrt(2). See A002965 and A119016, respectively.",
				"The other rows of this array hold current OEIS sequences as follows:",
				"A(1,k) = A016116(k);   A(2,k) = A029744(k) less the initial 1;",
				"A(3,k) = A070875(k);    A(4,k) = A091523(k) less the initial 8.",
				"The Pell Numbers (A000219) are the only initializing set of numbers where the two recursive square root equations (see below) produce exclusively integer values, for all iterations of k.  For any other initial values only even iterations (at k = 2, 4, ...) produce integers.",
				"The numbers in this array, especially the first three columns, are also integer square roots of these expressions: floor(m^2/2), floor(m^2/2 + 1), floor (m^2/2 - 1). See A227972 for specific arrangements and relationships. Also: ceiling(m^2/2), ceiling(m^2/2 + 1), ceiling (m^2/2 -1), m^2+1, m^2-1, m^2*(m^2-1)/2, m^2*(m^2-1)/2, in various different arrangements. Many of these involve: A000129(2n)/2 = A001109(n).",
				"A001109 also appears when multiplying adjacent columns:   A(n,k) * A(n,k+1) = (k+1) * A001109(n), for all k."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A228405/b228405.txt\"\u003eAntidiagonals n = 0..139, flattened\u003c/a\u003e",
				"MacTutor, \u003ca href=\"https://mathshistory.st-andrews.ac.uk/Extras/Thompson_irrationals/\"\u003eD'Arcy Thompson on Greek irrationals\u003c/a\u003e",
				"D'Arcy Thompson, \u003ca href=\"https://www.jstor.org/stable/2249223\"\u003eExcess and Defect: Or the Little More and the Little Less\u003c/a\u003e, Mind, New Series, Vol. 38, No. 149 (Jan., 1929), pp. 43-55 (13 pages). See page 50."
			],
			"formula": [
				"If using the left column and top row to initialize: A(n,k) = A(n,k-1) + A(n-1,k-1).",
				"If using only the top row to initialize, then each column for k = i is the binomial transform of A(0,k) restricted to k=\u003e i as input to the transform with an appropriate down shift of index. The inverse binomial transform with a similar condition can produce each row from A000129.",
				"If using only the first two rows to initialize then the Pell equation produces each column, as: A(n,k) = 2*A(n-1, k) + A(n-2, k).",
				"If using only the left column (A000219(n) = Pell Numbers) to initialize then the following two equations will produce each row:",
				"     A(n,k) = sqrt(2*A(n,k-1) + (-2)^(k-1)) for even rows",
				"     A(n,k) = sqrt(2*A(n,k-1) - (-2)^(k-1)) for odd rows.",
				"Interestingly, any portion of the array can also be filled \"backwards\" given the top row and any column k, using only:  A(n,k-1) = A(n-1,k-1) + A(n-1, k), or if given any column and its column number by rearranging the sqrt recursions above."
			],
			"example": [
				"With row # as n. and column # as k, and n, k =\u003e0, the array begins:",
				"0,     1,     0,     2,     0,     4,     0,     8, ...",
				"1,     1,     2,     2,     4,     4,     8,     8, ...",
				"2,     3,     4,     6,     8,    12,    16,    24, ...",
				"5,     7,    10,    14,    20,    28,    40,    56, ...",
				"12,   17,    24,    34,    48,    68,    96,   136, ...",
				"29,   41,    58,    82,   116,   164,   232,   328, ...",
				"70,   99,   140,   198,   280,   396,   560,   792, ...",
				"169,  239,  338,   478,   676,   956,  1352,  1912, ...",
				"408,  577,  816,  1154,  1632,  2308,  3264,  4616, ..."
			],
			"xref": [
				"Cf. A000219, A077957, A001333, A163271, A002203, A001109, A227972.",
				"Main diagonal is A007070."
			],
			"keyword": "nonn,tabl,look",
			"offset": "0,6",
			"author": "_Richard R. Forberg_, Aug 21 2013",
			"references": 13,
			"revision": 30,
			"time": "2021-10-29T23:16:43-04:00",
			"created": "2013-09-02T05:05:13-04:00"
		}
	]
}