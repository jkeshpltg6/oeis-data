{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A180000",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 180000,
			"data": "1,1,1,1,2,2,3,3,12,4,10,10,30,30,105,7,56,56,252,252,1260,60,330,330,1980,396,2574,286,2002,2002,15015,15015,240240,7280,61880,1768,15912,15912,151164,3876,38760,38760,406980,406980,4476780,99484,1144066",
			"name": "a(n) = lcm{1,2,...,n} / swinging_factorial(n) = A003418(n) / A056040(n).",
			"comment": [
				"Characterization: Let e_{p}(m) denote the exponent of the prime p in the prime factorization of m and [.] denote the Iverson bracket, then",
				"e_{p}(a(n)) = Sum_{k\u003e=1} [floor(n/p^k) is even].",
				"This implies, among other things, that no prime \u003e floor(n/2) can divide a(n). The prime exponents e_{2}(a(2n)) give Guy Steele's sequence GS(5,3) A080100.",
				"Asymptotics: log a(n) ~ n(1 - log 2). It is conjectured that log a(n) ~ n(1 - log 2) + O(n^{1/2+eps}) for all eps \u003e 0.",
				"Bounds: A056040(floor(n/3)) \u003c= a(n) \u003c= A056040(floor(n/2)) if n \u003e= 285."
			],
			"link": [
				"Peter Luschny, \u003ca href=\"/A180000/b180000.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Peter Luschny, \u003ca href=\"/A180000/a180000.pdf\"\u003eDie schwingende Fakultät und Orbitalsysteme\u003c/a\u003e, August 2011."
			],
			"formula": [
				"a(n) = 2^(-n)*Product_{1\u003c=k\u003c=n} A014963(k)*(k/2)^((-1)^k)."
			],
			"maple": [
				"a := proc(n) local A014963, k;",
				"A014963 := proc(n) if n \u003c 2 then 1 else numtheory[factorset](n);",
				"if 1 \u003c nops(%) then 1 else op(%) fi fi end;",
				"mul(A014963(k)*(k/2)^((-1)^k), k=1..n)/2^n end;",
				"# Also:",
				"A180000 := proc(n) local lcm, sf;",
				"lcm := ilcm(seq(i,i=1..n));",
				"sf := n!/iquo(n,2)!^2;",
				"lcm/sf end;"
			],
			"mathematica": [
				"a[0] = 1; a[n_] := LCM @@ Range[n] / (n! / Floor[n/2]!^2); Table[a[n], {n, 0, 46}] (* _Jean-François Alcover_, Jul 23 2013 *)"
			],
			"program": [
				"(PARI) L=1; X(n)={ ispower(n, , \u0026n);if(isprime(n),n,1); }",
				"Y(n)={ a=X(n); b=if(bitand(1,n),a,a*(n/2)^2); L=(b*L)/n; }",
				"A180000_list(n)={ L=1; vector(n,m,Y(m)); }  \\\\ for n\u003e0",
				"(Sage)",
				"def Exp(m,n) :",
				"    s = 0; p = m; q = n//p",
				"    while q \u003e 0 :",
				"        if is_even(q) :",
				"            s = s + 1",
				"        p = p * m",
				"        q = n//p",
				"    return s",
				"def A180000(n) :",
				"    A = [1,1,1,1,2,2,3,3,12]",
				"    if n \u003c 9 : return A[n]",
				"    R = []; r = isqrt(n)",
				"    P = Primes(); p = P.first()",
				"    while p \u003c= n//2 :",
				"        if p \u003c= r : R.append(p^Exp(p,n))",
				"        elif p \u003c= n//3 :",
				"            if is_even(n//p) : R.append(p)",
				"        else : R.append(p)",
				"        p = P.next(p)",
				"    return mul(x for x in R)"
			],
			"xref": [
				"Cf. A003418, A014963, A056040."
			],
			"keyword": "nonn",
			"offset": "0,5",
			"author": "_Peter Luschny_, Aug 17 2010",
			"references": 49,
			"revision": 22,
			"time": "2020-05-08T16:58:54-04:00",
			"created": "2010-08-27T03:00:00-04:00"
		}
	]
}