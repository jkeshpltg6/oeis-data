{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A108084",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 108084,
			"data": "1,2,1,8,6,1,64,56,14,1,1024,960,280,30,1,32768,31744,9920,1240,62,1,2097152,2064384,666624,89280,5208,126,1,268435456,266338304,87392256,12094464,755904,21336,254,1,68719476736,68451041280,22638755840,3183575040,205605888,6217920,86360,510,1",
			"name": "Triangle, read by rows, where T(0,0) = 1, T(n,k) = 2^n*T(n-1,k) + T(n-1,k-1).",
			"comment": [
				"Triangle T(n,k), 0 \u003c= k \u003c= n, read by rows given by [2, 2, 8, 12, 32, 56, 128, 240, 512, ...] DELTA [[1, 0, 2, 0, 4, 0, 8, 0, 16, 0, 32, 0, ...] = A014236 (first zero omitted) DELTA A077957 where DELTA is the operator defined in A084938. - _Philippe Deléham_, Aug 23 2006"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A108084/b108084.txt\"\u003eRows n = 0..50 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"Sum_{k=0..n} T(n, k) = A028362(n).",
				"T(n,0) = 2^(n*(n+1)/2) = A006125(n+1). - _Philippe Deléham_, Nov 05 2006",
				"T(n,k) = 2^binomial(n+1-k,2) * A022166(n,k) for 0 \u003c= k \u003c= n. - _Werner Schulte_, Mar 25 2019"
			],
			"example": [
				"Triangle begins:",
				"      1;",
				"      2,     1;",
				"      8,     6,    1;",
				"     64,    56,   14,    1;",
				"   1024,   960,  280,   30,  1;",
				"  32768, 31744, 9920, 1240, 62, 1;"
			],
			"mathematica": [
				"T[n_, k_, q_]:= T[n,k,q]= If[k\u003c0 || k\u003en, 0, If[k==n, 1, q^n*T[n-1,k,q] +T[n-1,k-1,q] ]];",
				"Table[T[n,k,2], {n,0,10}, {k,0,n}]//Flatten (* _G. C. Greubel_, Feb 20 2021 *)"
			],
			"program": [
				"(Sage)",
				"def T(n, k, q):",
				"    if (k\u003c0 or k\u003en): return 0",
				"    elif (k==n): return 1",
				"    else: return q^n*T(n-1,k,q) + T(n-1,k-1,q)",
				"flatten([[T(n,k,2) for k in (0..n)] for n in (0..10)]) # _G. C. Greubel_, Feb 20 2021",
				"(Magma)",
				"function T(n,k,q)",
				"  if k lt 0 or k gt n then return 0;",
				"  elif k eq n then return 1;",
				"  else return q^n*T(n-1,k,q) + T(n-1,k-1,q);",
				"  end if; return T; end function;",
				"[T(n,k,2): k in [0..n], n in [0..10]]; // _G. C. Greubel_, Feb 20 2021"
			],
			"xref": [
				"Cf. A023531 (q=0), A007318 (q=1), this sequence (q=2), A173007 (q=3), A173008 (q=4).",
				"Cf. A006125, A022166, A028362."
			],
			"keyword": "nonn,tabl",
			"offset": "0,2",
			"author": "_Gerald McGarvey_, Jun 05 2005",
			"references": 4,
			"revision": 22,
			"time": "2021-02-20T23:12:01-05:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}