{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A062147",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 62147,
			"data": "1,5,31,229,1961,19081,207775,2501801,32989969,472630861,7307593151,121247816845,2148321709561,40476722545169,807927483311551,17028146983530961,377844723929464865,8803698102396787861,214877019857456672479,5482159931449737760181",
			"name": "Row sums of unsigned triangle A062137 (generalized a=3 Laguerre).",
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A062147/b062147.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"Luis Verde-Star \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL24/Verde/verde4.html\"\u003eA Matrix Approach to Generalized Delannoy and Schröder Arrays\u003c/a\u003e, J. Int. Seq., Vol. 24 (2021), Article 21.4.1.",
				"\u003ca href=\"/index/La#Laguerre\"\u003eIndex entries for sequences related to Laguerre polynomials\u003c/a\u003e"
			],
			"formula": [
				"E.g.f.: exp(x/(1-x))/(1-x)^4.",
				"a(n) = Sum_{m=0..n} n!*binomial(n+3, n-m)/m!.",
				"a(n) = (2*n+3)*a(n-1) - (n-1)*(n+2)*a(n-2). - _Vaclav Kotesovec_, Oct 11 2012",
				"a(n) ~ exp(2*sqrt(n)-n-1/2)*n^(n+7/4)/sqrt(2). - _Vaclav Kotesovec_, Oct 11 2012",
				"a(n) = n!*LaguerreL(n, 3, -1). - _G. C. Greubel_, Mar 10 2021"
			],
			"maple": [
				"A062147 := n -\u003e n!*simplify(LaguerreL(n,3,-1), 'LaguerreL');",
				"seq(A062147(n), n = 0 .. 30); # _G. C. Greubel_, Mar 10 2021"
			],
			"mathematica": [
				"Table[Sum[n!*Binomial[n+3,n-k]/k!,{k,0,n}],{n,0,20}]",
				"(* or *)",
				"Table[n!*SeriesCoefficient[E^(x/(1-x))/(1-x)^4,{x,0,n}],{n,0,20}] (* _Vaclav Kotesovec_, Oct 11 2012 *)"
			],
			"program": [
				"(PARI) my(x='x+O('x^66)); Vec(serlaplace(exp(x/(1-x))/(1-x)^4)) \\\\ _Joerg Arndt_, May 06 2013",
				"(PARI) a(n) = vecsum(apply(abs,Vec(n!*pollaguerre(n, 3)))); \\\\ _Michel Marcus_, Feb 06 2021",
				"(MAGMA) [Factorial(n)*(\u0026+[Binomial(n+3,n-m)/Factorial(m): m in [0..n]]): n in [0..30]]; // _G. C. Greubel_, Feb 06 2018",
				"(Sage) [factorial(n)*gen_laguerre(n, 3, -1) for n in (0..30)] # _G. C. Greubel_, Mar 10 2021"
			],
			"xref": [
				"Cf. A062137, A216294."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Jun 19 2001",
			"references": 7,
			"revision": 33,
			"time": "2021-06-11T18:24:40-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}