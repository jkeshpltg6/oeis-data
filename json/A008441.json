{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A008441",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 8441,
			"data": "1,2,1,2,2,0,3,2,0,2,2,2,1,2,0,2,4,0,2,0,1,4,2,0,2,2,0,2,2,2,1,4,0,0,2,0,4,2,2,2,0,0,3,2,0,2,4,0,2,2,0,4,0,0,0,4,3,2,2,0,2,2,0,0,2,2,4,2,0,2,2,0,3,2,0,0,4,0,2,2,0,6,0,2,2,0,0,2,2,0,1,4,2,2,4,0,0,2,0,2,2,2,2,0,0",
			"name": "Number of ways of writing n as the sum of 2 triangular numbers.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700). The present sequence gives the expansion coefficients of psi(q)^2.",
				"Also the number of positive odd solutions to equation x^2 + y^2 = 8*n + 2. - _Seiichi Manyama_, May 28 2017"
			],
			"reference": [
				"B. C. Berndt, Ramanujan's Notebooks Part III, Springer-Verlag. See p. 139 Example (iv).",
				"J. H. Conway and N. J. A. Sloane, \"Sphere Packings, Lattices and Groups\", Springer-Verlag, p. 102.",
				"R. W. Gosper, Strip Mining in the Abandoned Orefields of Nineteenth Century Mathematics, in Computers in Mathematics (Ed. D. V. Chudnovsky and R. D. Jenks). New York: Dekker, 1990. See p. 279.",
				"R. W. Gosper, Experiments and discoveries in q-trigonometry, in Symbolic Computation, Number Theory, Special Functions, Physics and Combinatorics. Editors: F. G. Garvan and M. E. H. Ismail. Kluwer, Dordrecht, Netherlands, 2001, pp. 79-105. [See Pi_q.]",
				"P. A. MacMahon, Combinatory Analysis, Cambridge Univ. Press, London and New York, Vol. 1, 1915 and Vol. 2, 1916. See vol. 2, p 31, Article 272.",
				"Ivan Niven, Herbert S. Zuckerman and Hugh L. Montgomery, An Introduction to the Theory Of Numbers, Fifth Edition, John Wiley and Sons, Inc., NY 1991, p. 165."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A008441/b008441.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards Applied Math. Series 55, Tenth Printing, 1972, p. 575, 16.23.1 and 16.23.2.",
				"R. P. Agarwal, \u003ca href=\"https://www.ias.ac.in/article/fulltext/pmsc/103/03/0269-0293\"\u003eLambert series and Ramanujan\u003c/a\u003e, Prod. Indian Acad. Sci. (Math. Sci.), v. 103, n. 3, 1993, pp. 269-293 (see p. 285).",
				"N. J. Fine, \u003ca href=\"http://bookstore.ams.org/surv-27/\"\u003eBasic Hypergeometric Series and Applications\u003c/a\u003e, Amer. Math. Soc., 1988; p. 72, Eq (31.2); p. 78, Eq. following (32.25).",
				"J. W. L. Glaisher, \u003ca href=\"http://gdz.sub.uni-goettingen.de/en/dms/loader/img/?PID=PPN600494829_0020%7CLOG_0017\"\u003eOn the function chi(n)\u003c/a\u003e, Quarterly Journal of Pure and Applied Mathematics, 20 (1884), 97-167. See p. 108.",
				"R. W. Gosper, \u003ca href=\"/A274621/a274621.pdf\"\u003eExperiments and discoveries in q-trigonometry\u003c/a\u003e, Preprint.",
				"R. W. Gosper, \u003ca href=\"/A274621/a274621_1.pdf\"\u003eq-Trigonometry: Some Prefatory Afterthoughts\u003c/a\u003e",
				"M. D. Hirschhorn, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2004.08.045\"\u003eThe number of representations of a number by various forms\u003c/a\u003e, Discrete Mathematics 298 (2005), 205-211",
				"N. Heninger, E. M. Rains and N. J. A. Sloane, \u003ca href=\"https://arxiv.org/abs/math/0509316\"\u003eOn the Integrality of n-th Roots of Generating Functions\u003c/a\u003e, arXiv:math/0509316 [math.NT], 2005-2006; J. Combinatorial Theory, Series A, 113 (2006), 1732-1745.",
				"Nicco, \u003ca href=\"https://math.stackexchange.com/questions/1428124/\"\u003eConjectured identity of the product of two theta functions\u003c/a\u003e, Math Stackexchange, Sep 09 2015",
				"K. Ono, S. Robins and P. T. Wahl, \u003ca href=\"http://www.mathcs.emory.edu/~ono/publications-cv/pdfs/006.pdf\"\u003eOn the representation of integers as sums of triangular numbers\u003c/a\u003e, Aequationes mathematicae, August 1995, Volume 50, Issue 1-2, pp. 73-94.",
				"H. Rosengren, \u003ca href=\"https://arxiv.org/abs/math/0504272\"\u003eSums of triangular numbers from the Frobenius determinant\u003c/a\u003e, arXiv:math/0504272 [math.NT], 2005.",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"This sequence is the quadrisection of many sequences. Here are two examples:",
				"a(n) = A002654(4n+1), the difference between the number of divisors of 4*n+1 of form 4*k+1 and the number of form 4*k-1. - _David Broadhurst_, Oct 20 2002",
				"a(n) = b(4*n + 1), where b(n) is multiplicative and b(2^e) = 0^e, b(p^e) = (1 + (-1)^e) / 2 if p == 3 (mod 4), b(p^e) = e+1 if p == 1 (mod 4). - _Michael Somos_, Sep 14 2005",
				"G.f.: (Sum_{k\u003e=0} x^((k^2 + k)/2))^2 = (Sum_{k\u003e=0} x^(k^2 + k)) * (Sum_{k in Z} x^(k^2)).",
				"Expansion of Jacobi theta (theta_2(0,  sqrt(q)))^2 / (4 * q^(1/4)).",
				"Sum[d|(4n+1), (-1)^((d-1)/2) ].",
				"Given g.f. A(x), then B(q) = q * A(q^4) satisfies 0 = f(B(q), B(q^2), B(q^4)) where f(u, v, w) = v^3 + 4 * v * w^2 - u^2 * w. - _Michael Somos_, Sep 14 2005",
				"Given g.f. A(x), then B(q) = q * A(q^4) satisfies 0 = f(B(q), B(q^2), B(q^3), B(q^6)) where f(u1, u2, u3, u6) = u1 * u3 - (u2 - u6) * (u2 + 3*u6). - _Michael Somos_, Sep 14 2005",
				"Expansion of Jacobi k/(4*q^(1/2)) * (2/Pi)* K(k) in powers of q^2. - _Michael Somos_, Sep 14 2005. Convolution of A001938 and A004018. This appears in the denominator of the Jacobi sn and cn formula given in the Abramowitz-Stegun reference, p. 575, 16.23.1 and 16.23.2, where m=k^2. - _Wolfdieter Lang_, Jul 05 2016",
				"G.f.: Sum_{k\u003e=0} a(k) * x^(2*k) = Sum_{k\u003e=0} x^k / (1 + x^(2*k + 1)).",
				"G.f.: Sum_{k in Z} x^k / (1 - x^(4*k + 1)). - _Michael Somos_, Nov 03 2005",
				"Expansion of psi(x)^2 = phi(x) * psi(x^2) in powers of x where phi(), psi() are Ramanujan theta functions.",
				"Moebius transform is period 8 sequence [ 1, -1, -1, 0, 1, 1, -1, 0, ...]. - _Michael Somos_, Jan 25 2008",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (8 t)) = 1/2 (t/i) g(t) where q = exp(2 Pi i t) and g() is the g.f. for A104794.",
				"Euler transform of period 2 sequence [ 2, -2, ...].",
				"G.f.: q^(-1/4) * eta(q^2)^4 / eta(q)^2. See also the Fine reference.",
				"a(n) = Sum_{k=0..n} A010054(k)*A010054(n-k). - _Reinhard Zumkeller_, Nov 03 2009",
				"A004020(n) = 2 * a(n). A005883(n) = 4 * a(n).",
				"Convolution square of A010054.",
				"G.f.: Product_{k\u003e0} (1 - x^(2*k))^2 / (1 - x^(2*k-1))^2.",
				"a(2*n) = A113407(n). a(2*n + 1) = A053692(n). a(3*n) = A002175(n). a(3*n + 1) = 2 * A121444(n). a(9*n + 2) = a(n). a(9*n + 5) = a(9*n + 8) = 0. - _Michael Somos_, Jun 08 2014",
				"G.f.: exp( Sum_{n\u003e=1} 2*(x^n/n) / (1 + x^n) ). - _Paul D. Hanna_, Mar 01 2016",
				"a(n) = A001826(2+8*n) - A001842(2+8*n), the difference between the number of divisors 1 (mod 4) and 3 (mod 4) of 2+8*n. See the Ono et al. link, Corollary 1, or directly the Niven et al. reference, p. 165, Corollary (3.23). - _Wolfdieter Lang_, Jan 11 2017",
				"Expansion of continued fraction 1 / (1 - x^1 + x^1*(1 - x^1)^2 / (1 - x^3 + x^2*(1 - x^2)^2 / (1 - x^5 + x^3*(1 - x^3)^2 / ...))) in powers of x^2. - _Michael Somos_, Apr 20 2017",
				"Given g.f. A(x), and B(x) is the g.f. for A079006, then B(x) = A(x^2) / A(x) and B(x) * B(x^2) * B(x^4) * ... = 1 / A(x). - _Michael Somos_, Apr 20 2017",
				"a(0) = 1, a(n) = (2/n)*Sum_{k=1..n} A002129(k)*a(n-k) for n \u003e 0. - _Seiichi Manyama_, May 06 2017",
				"From _Paul D. Hanna_, Aug 10 2019: (Start)",
				"G.f.: Sum_{n\u003e=0} x^n * Sum_{k=0..n} binomial(n,k) * (x^(2*n+1) - x^(2*k))^(n-k) = Sum_{n\u003e=0} a(n)*x^(2*n).",
				"G.f.: Sum_{n\u003e=0} x^n * Sum_{k=0..n} binomial(n,k) * (x^(2*n+1) + x^(2*k))^(n-k) * (-1)^k = Sum_{n\u003e=0} a(n)*x^(2*n). (End)",
				"From _Peter Bala_, Jan 05 2021: (Start)",
				"G.f.: Sum_{n = -oo..oo} x^(4*n^2+2*n) * (1 + x^(4*n+1))/(1 - x^(4*n+1)). See Agarwal, p. 285, equation 6.20 with i = j = 1 and mu = 4.",
				"For prime p of the form 4*k + 3, a(n*p^2 + (p^2 - 1)/4) = a(n).",
				"If n \u003e 0 and p are coprime then a(n*p + (p^2 - 1)/4) = 0. The proofs are similar to those given for the corresponding results for A115110. Cf. A000729.",
				"For prime p of the form 4*k + 1 and for n not congruent to (p - 1)/4 (mod p) we have a(n*p^2 + (p^2 - 1)/4) = 3*a(n) (since b(n), where b(4*n+1) = a(n), is multiplicative). (End)",
				"From _Peter Bala_, Mar 22 2021: (Start)",
				"G.f. A(q) satisfies:",
				"A(q^2) = Sum_{n = -oo..oo} q^n/(1 - q^(4*n+2)) (set z = q, alpha = q^2, mu = 4 in Agarwal, equation 6.15).",
				"A(q^2) = Sum_{n = -oo..oo} q^(2*n)/(1 - q^(4*n+1)) (set z = q^2, alpha = q, mu = 4 in Agarwal, equation 6.15).",
				"A(q^2) = Sum_{n = -oo..oo} q^n/(1 + q^(2*n+1))^2 = Sum_{n = -oo..oo} q^(3*n+1)/(1 + q^(2*n+1))^2. (End)",
				"G.f.: Sum_{k\u003e=0} a(k) * q^k = Sum_{k\u003e=0} (-1)^k * q^(k*(k+1)) + 2 * Sum_{n\u003e=1, k\u003e=0} (-1)^k * q^(k*(k+2*n+1)+n). - _Mamuka Jibladze_, May 17 2021",
				"G.f.: Sum_{k\u003e=0} a(k) * q^k = Sum_{k\u003e=0} (-1)^k * q^(k*(k+1)) * (1 + q^(2*k+1))/(1 - q^(2*k+1)). - _Mamuka Jibladze_, Jun 06 2021"
			],
			"example": [
				"G.f. = 1 + 2*x + x^2 + 2*x^3 + 2*x^4 + 3*x^6 + 2*x^7 + 2*x^9 + 2*x^10 + 2*x^11 + ...",
				"G.f. for B(q) = q * A(q^4) = q + 2*q^5 + q^9 + 2*q^13 + 2*q^17 + 3*q^25 + 2*q^29 + 2*q^37 + 2*q^41 + ..."
			],
			"maple": [
				"sigmamr := proc(n,m,r) local a,d ; a := 0 ; for d in numtheory[divisors](n) do if modp(d,m) = r then a := a+1 ; end if; end do: a; end proc:",
				"A002654 := proc(n) sigmamr(n,4,1)-sigmamr(n,4,3) ; end proc:",
				"A008441 := proc(n) A002654(4*n+1) ; end proc:",
				"seq(A008441(n),n=0..90) ; # _R. J. Mathar_, Mar 23 2011"
			],
			"mathematica": [
				"Plus@@((-1)^(1/2 (Divisors[4#+1]-1)))\u0026 /@ Range[0, 104] (*Ant King, Dec 02 2010*)",
				"a[ n_] := SeriesCoefficient[ (1/2) EllipticTheta[ 2, 0, q] EllipticTheta[ 3, 0, q], {q, 0, n + 1/4}]; (* _Michael Somos_, Jun 19 2012 *)",
				"a[ n_] := SeriesCoefficient[ (1/4) EllipticTheta[ 2, 0, q]^2, {q, 0, 2 n + 1/2}]; (* _Michael Somos_, Jun 19 2012 *)",
				"a[ n_] := If[ n \u003c 0, 0, DivisorSum[ 4 n + 1, (-1)^Quotient[#, 2] \u0026]];  (* _Michael Somos_, Jun 08 2014 *)",
				"QP = QPochhammer; s = QP[q^2]^4/QP[q]^2 + O[q]^100; CoefficientList[s, q] (* _Jean-François Alcover_, Nov 27 2015, adapted from PARI *)",
				"TriangeQ[n_] := IntegerQ@Sqrt[8n +1]; Table[Count[FrobeniusSolve[{1, 1}, n], {__?TriangeQ}], {n, 0, 104}] (* _Robert G. Wilson v_, Apr 15 2017 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, n==0, polcoeff( sum(k=0, (sqrtint(8*n + 1) - 1)\\2, x^(k * (k+1)/2), x * O(x^n))^2, n) )};",
				"(PARI) {a(n) = if( n\u003c0, 0, n = 4*n + 1; sumdiv(n, d, (-1)^(d\\2)))}; /* _Michael Somos_, Sep 02 2005 */",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A)^4 / eta(x + A)^2, n))};",
				"(PARI) {a(n) = if( n\u003c0, 0, n = 4*n + 1; sumdiv( n, d, (d%4==1) - (d%4==3)))}; /* _Michael Somos_, Sep 14 2005 */",
				"(PARI) { my(q='q+O('q^166)); Vec(eta(q^2)^4 / eta(q)^2) } \\\\ _Joerg Arndt_, Apr 16 2017",
				"(Sage) ModularForms( Gamma1(8), 1, prec=420).1; # _Michael Somos_, Jun 08 2014",
				"(Haskell)",
				"a052343 = (flip div 2) . (+ 1) . a008441",
				"-- _Reinhard Zumkeller_, Jul 25 2014",
				"(MAGMA) A := Basis( ModularForms( Gamma1(8), 1), 420); A[2]; /* _Michael Somos_, Jan 31 2015 */"
			],
			"xref": [
				"A002654, A008442, A035154, A035181, A035184, A112301, A113406, A113414, A113446, A113652 all satisfy A(4n+1)=a(n).",
				"Cf. A004020, A005883, A104794, A052343, A199015 (partial sums).",
				"Number of ways of writing n as a sum of k triangular numbers, for k=1,...: A010054, A008441, A008443, A008438, A008439, A008440, A226252, A007331, A226253, A226254, A226255, A014787, A014809.",
				"Cf. A002175, A053692, A113407, A121444.",
				"Cf. A000217, A001938, A004018.",
				"Cf. A274621 (reciprocal series).",
				"Cf. A001826, A001842.",
				"Cf. A079006."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms and information from _Michael Somos_, Mar 23 2003"
			],
			"references": 64,
			"revision": 174,
			"time": "2021-06-20T23:00:28-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}