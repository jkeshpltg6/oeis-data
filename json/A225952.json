{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A225952",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 225952,
			"data": "4,0,12,8,0,24,0,20,0,40,12,0,0,0,60,0,28,0,56,0,84,16,0,48,0,80,0,112,0,36,0,72,0,0,0,144,20,0,60,0,0,0,140,0,180,0,44,0,88,0,132,0,176,0,220,24,0,0,0,120,0,168,0,0,0,264,0,52,0,104,0,156,0,208,0,260,0,312,28,0,84,0,140,0,0,0,252,0,308,0,364",
			"name": "Triangle read by rows, giving the even legs of primitive Pythagorean triangles, with zero entries for non-primitive triangles.",
			"comment": [
				"For primitive Pythagorean triples (x,y,z) see the Niven et al. reference, Theorem 5.5, p. 232, and the Hardy-Wright reference, Theorem 225, p. 190.",
				"There is a one-to-one correspondence between the values n and m of this number triangle for which a(n,m) does not vanish and primitive solutions of x^2 + y^2 = z^2 with y even, namely x = n^2 - m^2, y = 2*n*m and z = n^2 + m^2. The mirror triangles with x even are not considered here. Therefore a(n,m) = 2*n*m (for these solutions). The number of non-vanishing entries in row n is A055034(n).",
				"The sequence of the main diagonal is 2*n*(n-1) = 4*A000217 (n-1), n \u003e= 2.",
				"If the 0 entries are eliminated and the numbers are ordered nondecreasingly (multiple entries appear) the sequence becomes A120427. All its entries are positive integer multiples of 4, shown in  A008586(n), n \u003e= 1. Note that all even legs \u003c= N are certainly reached if one considers in the triangle rows n = 2, ..., floor(N/2)."
			],
			"reference": [
				"G. H. Hardy and E. M. Wright, An Introduction to the Theory of Numbers, Fifth Edition, Clarendon Press, Oxford, 2003.",
				"Ivan Niven, Herbert S. Zuckerman and Hugh L. Montgomery, An Introduction to the Theory Of Numbers, Fifth Edition, John Wiley and Sons, Inc., NY 1991."
			],
			"link": [
				"Kival Ngaokrajang, \u003ca href=\"/A225952/a225952.pdf\"\u003eIllustration of pattern of zero terms (non-isolated zeros are colored), for n = 1..103.\u003c/a\u003e"
			],
			"formula": [
				"a(n,m) = 2*n*m if n \u003e m \u003e= 1, gcd(n,m) = 1, and n and m are integers of opposite parity (i.e., (-1)^{n+m} = -1), otherwise a(n,m) = 0."
			],
			"example": [
				"The triangle a(n,m) begins:",
				"n\\m   1   2   3   4    5    6    7    8    9    10   11 ...",
				"2:    4",
				"3:    0  12",
				"4:    8   0  24",
				"5:    0  20   0  40",
				"6:   12   0   0   0   60",
				"7:    0  28   0  56    0   84",
				"8:   16   0  48   0   80    0  112",
				"9:    0  36   0  72    0    0    0  144",
				"10:  20   0  60   0    0    0  140    0  180",
				"11:   0  44   0  88    0  132    0  176    0   220",
				"12:  24   0   0   0  120    0  168    0    0     0  264",
				"..."
			],
			"xref": [
				"Cf. A222946 (hypotenuses), A225950 (odd legs), A225949 (leg sums), A225951 (perimeters), A120427 (even legs ordered), A008586 (multiples of 4)."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "2,1",
			"author": "_Wolfdieter Lang_, May 23 2013",
			"ext": [
				"Edited. Refs. added. - _Wolfdieter Lang_, Jul 26 2014"
			],
			"references": 5,
			"revision": 21,
			"time": "2015-12-12T16:49:22-05:00",
			"created": "2013-05-24T04:53:24-04:00"
		}
	]
}