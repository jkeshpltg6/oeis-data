{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A127058",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 127058,
			"data": "1,2,2,10,6,3,74,42,12,4,706,414,108,20,5,8162,5058,1332,220,30,6,110410,72486,19908,3260,390,42,7,1708394,1182762,342252,57700,6750,630,56,8,29752066,21573054,6583788,1159700,138150,12474,952,72,9,576037442",
			"name": "Triangle, read by rows, defined by: T(n,k) = Sum_{j=0..n-k-1) T(j+k,k)*T(n-j,k+1) for n \u003e k \u003e= 0, with T(n,n) = n+1.",
			"comment": [
				"Column 0 is A000698, the number of shellings of an n-cube, divided by 2^n n!.",
				"Column 1 is A115974, the number of Feynman diagrams of the proper self-energy at perturbative order n."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A127058/b127058.txt\"\u003eRows n = 0..15 of triangle, flattened\u003c/a\u003e"
			],
			"example": [
				"Other recurrences exist, as shown by:",
				"column 0 = A000698: T(n,0) = (2n+1)!! - Sum_{k=1..n} (2k-1)!!*T(n-k,0);",
				"column 1 = A115974: T(n,1) = T(n+1,0) - Sum_{k=0..n-1} T(k,1)*T(n-k,0).",
				"Illustrate the recurrence:",
				"T(n,k) = Sum_{j=0..n-k-1) T(j+k,k)*T(n-j,k+1) (n \u003e k \u003e= 0)",
				"at column k=1:",
				"T(2,1) = T(1,1)*T(2,2) = 2*3 = 6;",
				"T(3,1) = T(1,1)*T(3,2) + T(2,1)*T(2,2) = 2*12 + 6*3 = 42;",
				"T(4,1) = T(1,1)*T(4,2) + T(2,1)*T(3,2) + T(3,1)*T(2,2) = 2*108 + 6*12 + 42*3 = 414;",
				"at column k=2:",
				"T(3,2) = T(2,2)*T(3,3) = 3*4 = 12;",
				"T(4,2) = T(2,2)*T(4,3) + T(3,2)*T(3,3) = 3*20 + 12*4 = 108;",
				"T(5,2) = T(2,2)*T(5,3) + T(3,2)*T(4,3) + T(4,2)*T(3,3) = 3*220 + 12*20 + 108*4 = 1332.",
				"Triangle begins:",
				"         1;",
				"         2,        2;",
				"        10,        6,       3;",
				"        74,       42,      12,       4;",
				"       706,      414,     108,      20,      5;",
				"      8162,     5058,    1332,     220,     30,     6;",
				"    110410,    72486,   19908,    3260,    390,    42,   7;",
				"   1708394,  1182762,  342252,   57700,   6750,   630,  56,  8;",
				"  29752066, 21573054, 6583788, 1159700, 138150, 12474, 952, 72, 9; ..."
			],
			"mathematica": [
				"T[n_,k_]:= If[k==n, n+1, Sum[T[j+k,k]*T[n-j,k+1], {j,0,n-k-1}]];",
				"Table[T[n,k], {n,0,10}, {k,0,n}]//Flatten (* _G. C. Greubel_, Jun 03 2019 *)"
			],
			"program": [
				"(PARI) {T(n,k)=if(n==k,n+1,sum(j=0,n-k-1,T(j+k,k)*T(n-j,k+1)))}",
				"(Sage)",
				"def T(n, k):",
				"    if (k==n): return n+1",
				"    else: return sum(T(j+k,k)*T(n-j,k+1) for j in (0..n-k-1))",
				"[[T(n, k) for k in (0..n)] for n in (0..12)] # _G. C. Greubel_, Jun 03 2019"
			],
			"xref": [
				"Columns: A000698, A115974, A127059.",
				"Row sums: A127060.",
				"Cf. A001147 ((2n-1)!!)."
			],
			"keyword": "nonn,tabl",
			"offset": "0,2",
			"author": "_Paul D. Hanna_, Jan 04 2007",
			"references": 3,
			"revision": 11,
			"time": "2019-06-07T22:02:32-04:00",
			"created": "2007-01-12T03:00:00-05:00"
		}
	]
}