{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A033890",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 33890,
			"data": "1,8,55,377,2584,17711,121393,832040,5702887,39088169,267914296,1836311903,12586269025,86267571272,591286729879,4052739537881,27777890035288,190392490709135,1304969544928657,8944394323791464,61305790721611591,420196140727489673",
			"name": "a(n) = Fibonacci(4*n+2).",
			"comment": [
				"(x,y)=(a(n),a(n+1)) are solutions of (x+y)^2/(1+xy)=9, the other solutions are in A033888. - _Floor van Lamoen_, Dec 10 2001",
				"This sequence consists of the odd-indexed terms of A001906 (whose terms are the values of x such that 5*x^2 + 4 is a square). The even-indexed terms of A001906 are in A033888. Lim. n-\u003e Inf. a(n)/a(n-1) = phi^4 = (7 + 3*Sqrt(5))/2. - _Gregory V. Richardson_, Oct 13 2002",
				"a(n) = L(n,-7)*(-1)^n, where L is defined as in A108299; see also A049685 for L(n,+7). - _Reinhard Zumkeller_, Jun 01 2005",
				"General recurrence is a(n)=(a(1)-1)*a(n-1)-a(n-2), a(1)\u003e=4, lim n-\u003einfinity a(n)= x*(k*x+1)^n, k =(a(1)-3), x=(1+sqrt((a(1)+1)/(a(1)-3)))/2. Examples in OEIS: a(1)=4 gives A002878. a(1)=5 gives A001834. a(1)=6 gives A030221. a(1)=7 gives A002315. a(1)=8 gives A033890. a(1)=9 gives A057080. a(1)=10 gives A057081. - _Ctibor O. Zizka_, Sep 02 2008",
				"Indices of square numbers which are also 12-gonal. - _Sture Sjöstedt_, Jun 01 2009",
				"a(n) = A167816(4*n+2). - _Reinhard Zumkeller_, Nov 13 2009",
				"For positive n, a(n) equals the permanent of the (2n) X (2n) tridiagonal matrix with 3's along the main diagonal, and i's along the superdiagonal and the subdiagonal (i is the imaginary unit). - _John M. Campbell_, Jul 08 2011",
				"If we let b(0) = 0 and, for n \u003e=1, b(n) = A033890(n-1), then the sequence b(n) will be F(4n-2) and the first difference is L(4n) or A056854. F(4n-2) is also the ratio of golden spiral length (rounded to the nearest integer) after n rotations. L(4n) is also the pitch length ratio. See illustration in links. - _Kival Ngaokrajang_, Nov 03 2013",
				"The aerated sequence (b(n))n\u003e=1 = [1, 0, 8, 0, 55, 0, 377, 0, ...] is a fourth-order linear divisibility sequence; that is, if n | m then b(n) | b(m). It is the case P1 = 0, P2 = -5, Q = -1 of the 3-parameter family of divisibility sequences found by Williams and Guy. See A100047. - _Peter Bala_, Mar 22 2015",
				"Solutions y of Pell equation x^2 - 5*y^2 = 4; corresponding x values are in A342710 (see A342709). - _Bernard Schott_, Mar 19 2021"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A033890/b033890.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Marco Abrate, Stefano Barbero, Umberto Cerruti, and Nadir Murru, \u003ca href=\"https://www.emis.de/journals/INTEGERS/papers/p38/p38.Abstract.html\"\u003ePolynomial sequences on quadratic curves\u003c/a\u003e, Integers, Vol. 15, 2015, #A38.",
				"Nathan D. Cahill and Darren A. Narayan, \u003ca href=\"http://www.fq.math.ca/Papers1/42-3/quartcahill03_2004.pdf\"\u003eFibonacci and Lucas Numbers as Tridiagonal Matrix Determinants\u003c/a\u003e, Fib. Quart. 42, no. 3, Aug. 2004, pp. 216-221. See p. 219.",
				"Alex Fink, Richard K. Guy, and Mark Krusemeyer, \u003ca href=\"https://doi.org/10.11575/cdm.v3i2.61940\"\u003ePartitions with parts occurring at most thrice\u003c/a\u003e, Contributions to Discrete Mathematics, Vol 3, No 2 (2008), pp. 76-114. See Section 13.",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"Donatella Merlini and Renzo Sprugnoli, \u003ca href=\"https://doi.org/10.1016/j.disc.2016.08.017\"\u003eArithmetic into geometric progressions through Riordan arrays\u003c/a\u003e, Discrete Mathematics 340.2 (2017): 160-174.",
				"Kival Ngaokrajang, \u003ca href=\"/A033890/a033890.pdf\"\u003eIllustration of golden spiral length and pitch ratio\u003c/a\u003e",
				"H. C. Williams and R. K. Guy, \u003ca href=\"http://dx.doi.org/10.1142/S1793042111004587\"\u003eSome fourth-order linear divisibility sequences\u003c/a\u003e, Intl. J. Number Theory 7 (5) (2011) 1255-1277.",
				"H. C. Williams and R. K. Guy, \u003ca href=\"http://www.emis.de/journals/INTEGERS/papers/a17self/a17self.Abstract.html\"\u003eSome Monoapparitic Fourth Order Linear Divisibility Sequences\u003c/a\u003e, Integers, Volume 12A (2012) The John Selfridge Memorial Volume.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (7,-1).",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e"
			],
			"formula": [
				"G.f.: (1+x)/(1-7*x+x^2).",
				"a(n) = 7*a(n-1) - a(n-2), n \u003e 1; a(0)=1, a(1)=8.",
				"a(n) = S(n,7) + S(n-1,7) = S(2*n,sqrt(9) = 3), where S(n,x) = U(n,x/2) are Chebyshev's polynomials of the 2nd kind. Cf. A049310. S(n,7) = A004187(n+1), S(n,3) = A001906(n+1).",
				"a(n) = ((7+3*sqrt(5))^n - (7-3*sqrt(5))^n + 2*((7+3*sqrt(5))^(n-1) - ((7-3*sqrt(5))^(n-1)))) / (3*(2^n)*sqrt(5)). - _Gregory V. Richardson_, Oct 13 2002",
				"Let q(n, x) = Sum_{i=0..n} x^(n-i)*binomial(2*n-i, i); then a(n) = (-1)^n*q(n, -9). - _Benoit Cloitre_, Nov 10 2002",
				"Define f[x,s] = s*x + sqrt((s^2-1)*x^2+1); f[0,s]=0. a(n) = f[a(n-1),7/2] + f[a(n-2),7/2]. - Marcos Carreira, Dec 27 2006",
				"a(n+1) = 8*a(n) - 8*a(n-1) + a(n-2); a(1)=1, a(2)=8, a(3)=55. - _Sture Sjöstedt_, May 27 2009",
				"a(n)=b such that (-1)^n*Integral_{0..Pi/2} (cos((2*n+1)*x))/(3/2-sin(x)) dx = c + b*log(3). - _Francesco Daddi_, Aug 01 2011",
				"a(n) = A000045(A016825(n)). - _Michel Marcus_, Mar 22 2015",
				"a(n) = A001906(2*n+1). - _R. J. Mathar_, Apr 30 2017"
			],
			"maple": [
				"A033890 := proc(n)",
				"    option remember;",
				"    if n \u003c= 1 then",
				"        op(n+1,[1,8]);",
				"    else",
				"        7*procname(n-1)-procname(n-2) ;",
				"    end if;",
				"end proc: # _R. J. Mathar_, Apr 30 2017"
			],
			"mathematica": [
				"Table[Fibonacci[4n + 2], {n, 0, 14}] (* _Vladimir Joseph Stephan Orlovsky_, Jul 21 2008 *)",
				"LinearRecurrence[{7, -1}, {1, 8}, 50] (* _G. C. Greubel_, Jul 13 2017 *)"
			],
			"program": [
				"(PARI) a(n)=fibonacci(4*n+2);",
				"(MAGMA) [Fibonacci(4*n +2): n in [0..100]]; // _Vincenzo Librandi_, Apr 17 2011"
			],
			"xref": [
				"Cf. A000045, A001906, A100047.",
				"Cf. A342709, A342710."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 39,
			"revision": 100,
			"time": "2021-03-23T15:59:43-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}