{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A316773",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 316773,
			"data": "1,1,0,3,1,0,19,10,1,0,193,119,23,1,0,2721,1806,466,46,1,0,49171,34017,10262,1502,87,1,0,1084483,770274,255795,47020,4425,162,1,0,28245729,20429551,7235853,1539939,193699,12525,303,1,0,848456353,621858526,230629024,54314242,8273758,755170,34912,574,1,0",
			"name": "Triangle read by rows: T(n,m) = Sum_{k=m+1..n} (n-1)!/(k-1)!*binomial(2*n-k-1, n-1)*E(k,m) where E(n,m) is Euler's triangle A173018, T(0,0) = 1, n \u003e= m \u003e= 0.",
			"comment": [
				"T(n,m) is the number of labeled binary trees of size n with m ascents on the left branch."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A316773/b316773.txt\"\u003eTable of n, a(n) for n = 0..11475\u003c/a\u003e (rows 0 \u003c= n \u003c= 150, flattened)",
				"Yuriy Shablya, Dmitry Kruchinin, Vladimir Kruchinin, \u003ca href=\"https://doi.org/10.3390/math8060962\"\u003eMethod for Developing Combinatorial Generation Algorithms Based on AND/OR Trees and Its Application\u003c/a\u003e, Mathematics (2020) Vol. 8, No. 6, 962."
			],
			"formula": [
				"E.g.f.: Sum_{n \u003e= m \u003e= 0} T(n, m)/n! * x^n * y^m = E(C(x),y) = (y-1)/(y-exp(C(x)*(y-1))), where E(x,y) is an e.g.f. for Euler's triangle A173018.",
				"T(n,m) = Sum_{k = m+1..n} C(n,k)*E(k,m)*P(n,n-k), T(0,0)=1, where C(n,m) is the transposed Catalan's triangle A033184, E(n,m) is Euler's triangle A173018, and P(n,m) is the number of k-permutations of n A008279."
			],
			"example": [
				"Triangle begins:",
				"--------------------------------------------------------------------------------",
				"  n\\k |         0         1         2        3       4      5     6   7   8   9",
				"------+-------------------------------------------------------------------------",
				"   0  |         1",
				"   1  |         1         0",
				"   2  |         3         1         0",
				"   3  |        19        10         1        0",
				"   4  |       193       119        23        1       0",
				"   5  |      2721      1806       466       46       1      0",
				"   6  |     49171     34017     10262     1502      87      1     0",
				"   7  |   1084483    770274    255795    47020    4425    162     1   0",
				"   8  |  28245729  20429551   7235853  1539939  193699  12525   303   1   0",
				"   9  | 848456353 621858526 230629024 54314242 8273758 755170 34912 574   1   0"
			],
			"maple": [
				"T := (n,m) -\u003e `if`(n=0, 1, add((n-1)!/(k-1)!*binomial(2*n-k-1, n-1)*",
				"combinat:-eulerian1(k, m), k = m+1..n)):",
				"for n from 0 to 6 do seq(T(n, k), k=0..n) od; # _Peter Luschny_, Sep 04 2020"
			],
			"mathematica": [
				"Table[Boole[n == 0] + Sum[(n - 1)!/(k - 1)!*Binomial[2 n - k - 1, n - 1]*Sum[(-1)^j*(m + 1 - j)^k*Binomial[k + 1, j], {j, 0, m}], {k, m + 1, n}], {n, 0, 8}, {m, 0, n}] // Flatten (* _Michael De Vlieger_, Sep 04 2020 *)"
			],
			"program": [
				"(Maxima)",
				"T(n,m):=if m\u003en then 0 else if n=0 then 1 else sum((n-1)!/(k-1)!*binomial(2*n-k-1,n-1)*sum((-1)^j*(m+1-j)^k*binomial(k+1,j),j,0,m),k,m+1,n);"
			],
			"xref": [
				"Cf. A033184, A008279, A173018."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_Yuriy Shablya_, Sep 13 2018",
			"references": 1,
			"revision": 47,
			"time": "2020-09-05T03:43:26-04:00",
			"created": "2018-09-19T06:08:26-04:00"
		}
	]
}