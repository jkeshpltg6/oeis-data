{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A209615",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 209615,
			"data": "1,-1,-1,1,1,1,-1,-1,1,-1,-1,-1,1,1,-1,1,1,-1,-1,1,1,1,-1,1,1,-1,-1,-1,1,1,-1,-1,1,-1,-1,1,1,1,-1,-1,1,-1,-1,-1,1,1,-1,-1,1,-1,-1,1,1,1,-1,1,1,-1,-1,-1,1,1,-1,1,1,-1,-1,1,1,1,-1,-1,1,-1",
			"name": "Completely multiplicative with a(p^e) = 1 if p == 1 (mod 4), a(p^e) = (-1)^e otherwise.",
			"comment": [
				"Turn sequence of the alternate paperfolding curve.  Davis and Knuth define the alternate paperfolding curve by folding a long strip of paper repeatedly in half alternately to the left side or right side, then unfolding it so each crease is 90 degrees (or other angle).  a(n) is their d(n) at equation 4.2.  Their equation 6.2 (varied to d(2) = -1 as described there) is equivalent to the definition here.  The curve is drawn by a unit step forward, turn a(1)*90 degrees left, a unit step forward, turn a(2)*90 degrees left, and so on. - _Kevin Ryde_, Apr 18 2020"
			],
			"reference": [
				"Chandler Davis and Donald E. Knuth, Number Representations and Dragon Curves -- I and II, Journal of Recreational Mathematics, volume 3, number 2, April 1970, pages 66-81, and number 3, July 1970, pages 133-149.  Reprinted in Donald E. Knuth, Selected Papers on Fun and Games, CSLI Publications, 2010, pages 571-614."
			],
			"link": [
				"Jianing Song, \u003ca href=\"/A209615/b209615.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Kevin Ryde, \u003ca href=\"http://user42.tuxfamily.org/alternate/index.html\"\u003eIterations of the Alternate Paperfolding Curve\u003c/a\u003e, section \"Turn\".",
				"\u003ca href=\"/index/Di#divseq\"\u003eIndex to divisibility sequences\u003c/a\u003e"
			],
			"formula": [
				"G.f.: Sum_{k\u003e=0} (-1)^k * x^(2^k) / (1 + x^(2^(k+1))).",
				"G.f. A(x) satisfies A(x) + A(x^2) = x / (1 + x^2).",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2), A(x^4)) where f(u, v, w) = (v + w) - (u + v)^2 * (1 + 2*(v + w)).",
				"If p is prime then a(p) = 1 if and only if p is in A002144.",
				"a(4*n + 1) = 1, a(4*n + 3) = -1. a(2*n) = a(3*n) = a(-n) = -a(n).",
				"a(n) = -(-1)^A106665(n-1) unless n=0.",
				"a(2n) = -a(n), a(2n+1) = (-1)^n.  [Davis and Knuth equation 4.2] - _Kevin Ryde_, Apr 18 2020",
				"From _Jianing Song_, Apr 24 2021: (Start)",
				"a(n) = 1 \u003c=\u003e A003324(n) = 1 or 4, a(n) = -1 \u003c=\u003e A003324(n) = 2 or 3. In other words, a(n) = Legendre(A003324(n), 5) == A003324(n)^2 (mod 5).",
				"a(n) = A034947(n) * (-1)^(v2(n)), where v2(n) = A007814(n) is the 2-adic valuation of n.",
				"Dirichlet g.f.: beta(s)/(1 + 2^(-s)). (End)"
			],
			"example": [
				"x - x^2 - x^3 + x^4 + x^5 + x^6 - x^7 - x^8 + x^9 - x^10 - x^11 - x^12 + ...",
				"From _Kevin Ryde_, Apr 18 2020: (Start)",
				"                   ...              alternate",
				"                    | -1           paperfolding",
				"            -1 ---\u003e\\ \\\u003c--- +1         curve",
				"             ^   -1 |      ^",
				"             |      v      |      turns +1 left",
				"  start --\u003e +1     +1 ---\u003e +1        or -1 right",
				"(End)"
			],
			"program": [
				"(PARI) {a(n) = my(v); if( n==0, 0, v = valuation( n, 2); (-1)^(n/2^v\\2 + v))};",
				"(PARI) {a(n) = if( n!=0, -kronecker( -1, n) * (-1)^if( n!=0, 1 - valuation( n, 2) %2))};",
				"(PARI) {a(n) = my(A, p, e, f); sign(n) * if( n==0, 0, A = factor(abs(n)); prod( k=1, matsize(A)[1], [p, e] = A[k, ]; (-1)^(e * (p%4 != 1))) )};"
			],
			"xref": [
				"Cf. A002144, A003324, A034947, A106665, A292077.",
				"Indices of 1: A338692 = A016813 U A343501; indices of -1: A338691 = A004767 U A343500.",
				"Inverse Moebius transform gives A338690."
			],
			"keyword": "sign,easy,mult",
			"offset": "1,1",
			"author": "_Michael Somos_, Mar 10 2012",
			"references": 11,
			"revision": 43,
			"time": "2021-04-26T08:59:58-04:00",
			"created": "2012-03-12T11:13:29-04:00"
		}
	]
}