{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A298854",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 298854,
			"data": "1,1,1,2,3,2,6,11,11,6,24,50,61,50,24,120,274,379,379,274,120,720,1764,2668,3023,2668,1764,720,5040,13068,21160,26193,26193,21160,13068,5040,40320,109584,187388,248092,270961,248092,187388,109584,40320,362880,1026576,1836396,2565080,2995125,2995125,2565080,1836396,1026576,362880",
			"name": "Characteristic polynomials of Jacobi coordinates. Triangle read by rows, T(n, k) for 0 \u003c= k \u003c= n.",
			"comment": [
				"This is just a different normalization of A223256 and A223257."
			],
			"formula": [
				"P(0)=1 and P(n) = n * (x + 1) * P(n - 1) - (n - 1)^2 * x * P(n - 2)."
			],
			"example": [
				"For n = 3, the polynomial is 6*x^3 + 11*x^2 + 11*x + 6.",
				"The first few polynomials, as a table:",
				"[  1],",
				"[  1,   1],",
				"[  2,   3,   2],",
				"[  6,  11,  11,   6],",
				"[ 24,  50,  61,  50,  24],",
				"[120, 274, 379, 379, 274, 120]"
			],
			"maple": [
				"b:= proc(n) option remember; `if`(n\u003c1, n+1, expand(",
				"      n*(x+1)*b(n-1)-(n-1)^2*x*b(n-2)))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=0..n))(b(n)):",
				"seq(T(n), n=0..10);  # _Alois P. Heinz_, Apr 01 2021"
			],
			"mathematica": [
				"P[0] = 1 ; P[1] = x + 1;",
				"P[n_] := P[n] = n (x + 1) P[n - 1] - (n - 1)^2 x P[n - 2];",
				"Table[CoefficientList[P[n], x], {n, 0, 9}] // Flatten (* _Jean-François Alcover_, Mar 16 2020 *)"
			],
			"program": [
				"(Sage)",
				"@cached_function",
				"def poly(n):",
				"    x = polygen(ZZ, 'x')",
				"    if n \u003c 0:",
				"        return x.parent().zero()",
				"    elif n == 0:",
				"        return x.parent().one()",
				"    else:",
				"        return n * (x + 1) * poly(n - 1) - (n - 1)**2 * x * poly(n - 2)",
				"A298854_row = lambda n: list(poly(n))",
				"for n in (0..7): print(A298854_row(n))"
			],
			"xref": [
				"Closely related to A223256 and A223257.",
				"Row sums are A002720.",
				"Leftmost and rightmost columns are A000142.",
				"Alternating row sums are A177145.",
				"Absolute value of evaluation at x = exp(2*i*Pi/3) is A080171.",
				"Evaluation at x=2 gives A187735."
			],
			"keyword": "tabl,nonn,easy",
			"offset": "0,4",
			"author": "_F. Chapoton_, Jan 27 2018",
			"references": 1,
			"revision": 34,
			"time": "2021-04-01T18:07:00-04:00",
			"created": "2018-01-28T04:24:23-05:00"
		}
	]
}