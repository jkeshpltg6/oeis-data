{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A254051",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 254051,
			"data": "1,3,2,4,8,5,6,11,23,14,7,17,32,68,41,9,20,50,95,203,122,10,26,59,149,284,608,365,12,29,77,176,446,851,1823,1094,13,35,86,230,527,1337,2552,5468,3281,15,38,104,257,689,1580,4010,7655,16403,9842,16,44,113,311,770,2066,4739,12029,22964,49208,29525,18,47",
			"name": "Square array A by downward antidiagonals: A(n,k) = (3 + 3^n*(2*floor(3*k/2) - 1))/6, n,k \u003e= 1; read as A(1,1), A(1,2), A(2,1), A(1,3), A(2,2), A(3,1), ...",
			"comment": [
				"This is transposed dispersion of (3n-1), starting from its complement A032766 as the first row of square array A(row,col). Please see the transposed array A191450 for references and background discussion about dispersions.",
				"For any odd number x = A135765(row,col), the result after one combined Collatz step (3x+1)/2 -\u003e x (A165355) is found in this array at A(row+1,col)."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A254051/b254051.txt\"\u003eTable of n, a(n) for n = 1..10440; the first 144 antidiagonals of array\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e",
				"\u003ca href=\"/index/3#3x1\"\u003eIndex entries for sequences related to 3x+1 (or Collatz) problem\u003c/a\u003e"
			],
			"formula": [
				"In A(n,k)-formulas below, n is the row, and k the column index, both starting from 1:",
				"A(n,k) = (3 + ( A000244(n) * (2*A032766(k) - 1) )) / 6. - _Antti Karttunen_ after _L. Edson Jeffery_'s direct formula for A191450, Jan 24 2015",
				"A(n,k) = A048673(A254053(n,k)). [Alternative formula.]",
				"A(n,k) = (1/2) * (1 + A003961((2^(n-1)) * A254050(k))). [The above expands to this.]",
				"A(n,k) = (1/2) * (1 + (A000244(n-1) * A007310(k))). [Which further reduces to this, equivalent to _L. Edson Jeffery_'s original formula above.]",
				"A(1,k) = A032766(k) and for n \u003e 1: A(n,k) = (3 * A254051(n-1,k)) - 1. [The definition of transposed dispersion of (3n-1).]",
				"A(n,k) = (1+A135765(n,k))/2, or when expressed one-dimensionally, a(n) = (1+A135765(n))/2.",
				"A(n+1,k) = A165355(A135765(n,k)).",
				"As a composition of related permutations. All sequences interpreted as one-dimensional:",
				"a(n) = A048673(A254053(n)). [Proved above.]",
				"a(n) = A191450(A038722(n)). [Transpose of array A191450.]"
			],
			"example": [
				"The top left corner of the array:",
				"   1,   3,   4,   6,   7,   9,  10,  12,   13,   15,   16,   18,   19,   21",
				"   2,   8,  11,  17,  20,  26,  29,  35,   38,   44,   47,   53,   56,   62",
				"   5,  23,  32,  50,  59,  77,  86, 104,  113,  131,  140,  158,  167,  185",
				"  14,  68,  95, 149, 176, 230, 257, 311,  338,  392,  419,  473,  500,  554",
				"  41, 203, 284, 446, 527, 689, 770, 932, 1013, 1175, 1256, 1418, 1499, 1661",
				"..."
			],
			"program": [
				"(Scheme, several versions)",
				"(define (A254051 n) (A254051bi (A002260 n) (A004736 n)))",
				"(define (A254051bi row col) (/ (+ 3 (* (A000244 row) (- (* 2 (A032766 col)) 1))) 6))",
				"(define (A254051 n) (A191450biv2 (A004736 n) (A002260 n))) ;; As transpose of A191450",
				"(define (A254051bi row col) (/ (+ 1 (A003961 (* (A000079 (- row 1)) (+ -1 (* 2 (A249745 col)))))) 2))",
				"(define (A254051bi row col) (/ (+ 1 (A003961 (* (A000079 (- row 1)) (A254050 col)))) 2))",
				"(define (A254051bi row col) (/ (+ 1 (* (A000244 (- row 1)) (A007310 col))) 2))"
			],
			"xref": [
				"Inverse: A254052.",
				"Transpose: A191450.",
				"Row 1: A032766.",
				"Cf. A007051, A057198, A199109, A199113 (columns 1-4).",
				"Cf. A254046 (row index of n in this array, see also A253786), A253887 (column index).",
				"Array A135765(n,k) = 2*A(n,k) - 1.",
				"Other related arrays: A254055, A254101, A254102.",
				"Cf. also A000079, A000244, A003961, A007310, A032766, A002260, A004736, A038722, A165355, A254050.",
				"Related permutations: A048673, A254053, A183209, A249745, A254103, A254104."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_L. Edson Jeffery_ \u0026 _Antti Karttunen_, Jan 24 2015",
			"references": 17,
			"revision": 62,
			"time": "2015-09-03T14:57:36-04:00",
			"created": "2015-02-03T16:10:08-05:00"
		}
	]
}