{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A326988",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 326988,
			"data": "0,0,3,0,5,9,7,0,12,15,11,21,13,21,23,0,17,36,19,35,31,33,23,45,30,39,39,49,29,69,31,0,47,51,47,84,37,57,55,75,41,93,43,77,77,69,47,93,56,90,71,91,53,117,71,105,79,87,59,161,61,93,103,0,83,141,67,119,95,141,71,180,73,111,123,133,95,165,79,155",
			"name": "Sum of nonpowers of 2 dividing n.",
			"comment": [
				"In other words: a(n) is the sum of the divisors of n that are not powers of 2.",
				"a(n) is also the sum of odd divisors greater than 1 of n, multiplied by the sum of the divisors of n that are powers of 2.",
				"a(n) = 0 if and only if n is a power of 2.",
				"a(n) = n if and only if n is an odd prime.",
				"From _Bernard Schott_, Sep 17 2019: (Start)",
				"a(n) = 3*n/2 if and only if n is an even semiprime greater than or equal to 6 (A100484).",
				"a(n) = n + sqrt(n) if and only if n is the square of an odd prime (see A001248 without its first term). (End)"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A326988/b326988.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A000203(n) - A038712(n).",
				"a(n) = (A000593(n) - 1)*A038712(n).",
				"a(n) = A326990(n)*A038712(n).",
				"a(n) = Sum_{d|n, d \u003e 1} d * (1 - [rad(d) = 2]), where rad is the squarefree kernel (A007947) and [] is the Iverson bracket, which gives 1 if the condition is true, 0 if it's false. - _Wesley Ivan Hurt_, Apr 29 2020"
			],
			"example": [
				"For n = 18 the divisors of 18 are [1, 2, 3, 6, 9, 18]. There are four divisors of 18 that are not powers of 2, they are [3, 6, 9, 18]. The sum of them is 3 + 6 + 9 + 18 = 36, so a(18) = 36.",
				"On the other hand, the sum of odd divisors greater than 1 of 18 is 3 + 9 = 12, and the sum of the divisors of 18 that are powers of 2 is 1 + 2 = 3, then we have that 12 * 3 = 36, so a(18) = 36."
			],
			"maple": [
				"f:= n -\u003e numtheory:-sigma(n) - 2^(1+padic:-ordp(n,2))+1:",
				"map(f, [$1..100]); # _Robert Israel_, Apr 29 2020"
			],
			"mathematica": [
				"Table[DivisorSigma[1, n] - Denominator[DivisorSigma[1, 2n]/DivisorSigma[1, n]], {n, 100}] (* _Wesley Ivan Hurt_, Aug 24 2019 *)"
			],
			"program": [
				"(MAGMA) sol:=[];  m:=1;  for n in [1..80] do v:=Set(Divisors(n)) diff {2^k:k in [0..Floor(Log(2,n))]};  sol[m]:=\u0026+v; m:=m+1; end for; sol; // _Marius A. Burtea_, Aug 24 2019",
				"(PARI) ispp2(n) = (n==1) || (isprimepower(n, \u0026p) \u0026\u0026 (p==2));",
				"a(n) = sumdiv(n, d, if (!ispp2(d), d)); \\\\ _Michel Marcus_, Aug 26 2019",
				"(Scala) def divisors(n: Int): IndexedSeq[Int] = (1 to n).filter(n % _ == 0)",
				"(1 to 80).map(divisors(_).filter(n =\u003e n != Integer.highestOneBit(n)).sum) // _Alonso del Arte_, Apr 29 2020"
			],
			"xref": [
				"Row sums of A326989.",
				"Cf. A000079, A000203, A000593, A038712, A057716, A065091, A326987 (number), A326990."
			],
			"keyword": "nonn,easy",
			"offset": "1,3",
			"author": "_Omar E. Pol_, Aug 18 2019",
			"references": 5,
			"revision": 58,
			"time": "2020-05-02T15:22:45-04:00",
			"created": "2019-09-18T17:34:49-04:00"
		}
	]
}