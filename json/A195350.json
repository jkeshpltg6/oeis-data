{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A195350",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 195350,
			"data": "1,1,3,10,37,141,541,2080,8001,30781,118423,455610,1752877,6743881,25945881,99822160,384048001,1477556361,5684635243,21870622810,84143330517,323726495221,1245480100021,4791763116240,18435456144001,70927137880741",
			"name": "Expansion of (1 - 3*x - x^2)/(1 - 4*x + 2*x^3 + x^4).",
			"comment": [
				"Rewrite the Girard-Waring formulae to express the mean powers in terms of the mean symmetric functions of the data values; the results are polynomials in the mean symmetric polynomials, indexed by the power n. Then for 3 data points, the sum of the positive coefficients in the n-th such polynomial is a(n). a(n+1)/a(n) approaches 1/(2^(1/3)-1). See extended comment in A301417. - _Gregory Gerard Wojnar_, Mar 19 2018"
			],
			"link": [
				"Bruno Berselli, \u003ca href=\"/A195350/b195350.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"G. G. Wojnar, D. S. Wojnar, and L. Q. Brin, \u003ca href=\"http://arxiv.org/abs/1706.08381\"\u003eUniversal peculiar linear mean relationships in all polynomials\u003c/a\u003e, arXiv:1706.08381 [math.GM], 2017. See Table GW. n=3 p. 22.",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,0,-2,-1)."
			],
			"formula": [
				"G.f.: (1-3*x-x^2)/((1-x)*(1-3*x-3*x^2-x^3)).",
				"a(n) = 4*a(n-1) - 2*a(n-3) - a(n-4).",
				"a(n) = A301483(n) - A303647(n-2) + A195339(n-4) (conjectured). - _Gregory Gerard Wojnar_, Apr 27 2018"
			],
			"maple": [
				"[seq(coeftayl((1-3*x-x^2)/(1-4*x+2*x^3+x^4), x = 0, k), k=0..25)]; # _Muniru A Asiru_, Mar 20 2018"
			],
			"mathematica": [
				"CoefficientList[Series[(1 - 3 x - x^2)/(1 - 4 x + 2 x^3 + x^4), {x, 0, 25}], x] (* _Vincenzo Librandi_, Mar 26 2013 *)"
			],
			"program": [
				"(PARI) Vec((1-3*x-x^2)/(1-4*x+2*x^3+x^4)+O(x^26))",
				"(MAGMA) m:=26; R\u003cx\u003e:=PowerSeriesRing(Integers(), m); Coefficients(R!((1-3*x-x^2)/(1-4*x+2*x^3+x^4)));",
				"(Maxima) makelist(coeff(taylor((1-3*x-x^2)/(1-4*x+2*x^3+x^4), x, 0, n), x, n), n, 0, 25);"
			],
			"xref": [
				"Cf. A185962 (gives the coefficients of numerator and denominator of the g.f., row 4 and 5 of its triangular array). Sequences likewise related to A185962: A000012 (row 1 and 2), A001333 (row 2 and 3) and A006190 (row 3 and 4).",
				"Cf. also A195339, A301417, A301420, A301421, A301424, A302764, A301483, A303647."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Bruno Berselli_, Sep 16 2011",
			"references": 10,
			"revision": 63,
			"time": "2019-06-30T04:41:28-04:00",
			"created": "2011-09-16T19:56:12-04:00"
		}
	]
}