{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A242252",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 242252,
			"data": "1,0,0,1,0,1,0,1,1,0,1,1,0,1,1,1,0,1,1,0,1,1,1,1,1,0,1,0,1,1,1,1,0,0,0,1,1,1,1,1,0,0,0,1,0,1,1,1,0,1,1,0,0,1,1,1,0,1,1,0,0,1,1,0,1,1,1,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,1,1,1",
			"name": "Start with n-th odd prime, and repeatedly subtract the greatest prime until either 0 or 1 remains.  (The result is the \"primes-greedy residue\" of the n-th odd prime, which is \"primes-greedy summable\" if its residue = 0, as at A242255; see Comments.)",
			"comment": [
				"Suppose that s = (s(1), s(2), ... ) is a sequence of real numbers such that for every real number u, at most finitely many s(i) are \u003c u, and suppose that x \u003e min(s).  We shall apply the greedy algorithm to x, using terms of s.  Specifically, let i(1) be an index i such that s(i) = max{s(j) \u003c x}, and put d(1) = x - s(i(1)).  If d(1) \u003c s(i) for all i, put r = x - s(i(1)).  Otherwise, let i(2) be an index i such that s(i) = max{s(j) \u003c x - s(i(1))}, and put d(2) = x - s(i(1)) - s(i(2)).  If d(2) \u003c s(i) for all i, put r = x - s(i(1)) - s(i(2)).  Otherwise, let i(3) be an index i such that s(i) = max{s(j) \u003c x - s(i(1)) - s(i(2))}, and put d(3) = x - s(i(1)) - s(i(2)) - s(i(3)).  Continue until reaching k such that d(k) \u003c s(i) for every i, and put r = x - s(i(1)) - ... - s(i(k)).  Call r the s-greedy residue of x, and call s(i(1)) + ... + s(i(k)) the s-greedy sum for x.   If r = 0, call x s-greedy summable.  If s(1) = min(s) \u003c s(2), then taking x = s(i) successively for i = 2, 3,... gives a residue r(i) for each i; call (r(i)) the greedy residue sequence for s.  When s is understood from context, the prefix \"s-\" is omitted.  For A242252, s = (2,3,5,7,11, ... ) = A000040."
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A242252/b242252.txt\"\u003eTable of n, a(n) for n = 1..2000\u003c/a\u003e"
			],
			"example": [
				"n ... n-th odd prime ... a(n)",
				"1 ... 3 ................ 1 = 3 - 2",
				"2 ... 5 ................ 0 = 5 - 3 - 2",
				"3 ... 7 ................ 0 = 7 - 5 - 2",
				"4 ... 11 ............... 1 = 11 - 7 - 3",
				"5 ... 13 ............... 0 = 13 - 11 - 2",
				"34 .. 149 .............. 1 = 149 - 139 - 7 - 2"
			],
			"mathematica": [
				"z = 200;  s = Table[Prime[n], {n, 1, z}]; t = Table[{s[[n]], #, Total[#] == s[[n]]} \u0026[   DeleteCases[-Differences[FoldList[If[#1 - #2 \u003e= 0, #1 - #2, #1] \u0026, s[[n]], Reverse[Select[s, # \u003c s[[n]] \u0026]]]], 0]], {n, z}]; r[n_] := s[[n]] - Total[t[[n]][[2]]]; tr =  Table[r[n], {n, 2, z}]  (* A242252 *)",
				"c = Table[Length[t[[n]][[2]]], {n, 2, z}] (* A242253 *)",
				"f = 1 + Flatten[Position[tr, 0]]  (* A242254 *)",
				"Prime[f]  (* A242255 *)",
				"f1 = Prime[Complement[Range[Max[f]], f]] (* A242256 *)",
				"(* _Peter J. C. Moses_, May 06 2014 *)"
			],
			"xref": [
				"Cf. A242253, A242254, A242255, A242256, A241833, A000040."
			],
			"keyword": "nonn,easy",
			"offset": "1",
			"author": "_Clark Kimberling_, May 09 2014",
			"references": 8,
			"revision": 9,
			"time": "2014-05-15T10:13:28-04:00",
			"created": "2014-05-15T10:13:28-04:00"
		}
	]
}