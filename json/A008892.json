{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A008892",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 8892,
			"data": "276,396,696,1104,1872,3770,3790,3050,2716,2772,5964,10164,19628,19684,22876,26404,30044,33796,38780,54628,54684,111300,263676,465668,465724,465780,1026060,2325540,5335260,11738916,23117724,45956820,121129260,266485716",
			"name": "Aliquot sequence starting at 276.",
			"comment": [
				"It is an open question whether this sequence ever reaches 0. See A098007 and the Zimmermann link.",
				"The aliquot sequence starting at 306 joins this sequence after one step.",
				"One can note that the k-tuple abundance of 276 is only 5, since a(6) = 3790 is deficient. On the other hand, the k-tuple abundance of a(8) = 2716 is 164 since a(172) is deficient (see A081705 for definition of k-tuple abundance). - _Michel Marcus_, Dec 31 2013"
			],
			"reference": [
				"Richard K. Guy, Unsolved Problems in Number Theory, B6.",
				"Richard K. Guy and J. L. Selfridge, Interim report on aliquot series, pp. 557-580 of Proceedings Manitoba Conference on Numerical Mathematics. University of Manitoba, Winnipeg, Oct 1971."
			],
			"link": [
				"Jeppe Stig Nielsen, \u003ca href=\"/A008892/b008892.txt\"\u003eTable of n, a(n) for n = 0..2140\u003c/a\u003e (terms n = 0..2127 from Daniel Suteu)",
				"Christophe Clavier, \u003ca href=\"http://christophe.clavier.free.fr/Aliquot/site/Aliquot.html\"\u003eAliquot Sequences\u003c/a\u003e",
				"Christophe Clavier, \u003ca href=\"http://christophe.clavier.free.fr/Aliquot/site/database1/s0276.txt\"\u003eTrajectory of 276 - the first 1576 terms and their factorizations\u003c/a\u003e",
				"Christophe Clavier, \u003ca href=\"/A008892/a008892.txt\"\u003eTrajectory of 276 - the first 1576 terms and their factorizations\u003c/a\u003e [Cached copy]",
				"Wolfgang Creyaufmüller, \u003ca href=\"http://www.aliquot.de/lehmer.htm\"\u003eLehmer Five\u003c/a\u003e",
				"Paul Erdős, Andrew Granville, Carl Pomerance and Claudia Spiro, \u003ca href=\"http://math.dartmouth.edu/~carlp/iterate.pdf\"\u003eOn the normal behavior of the iterates of some arithmetic functions\u003c/a\u003e, Analytic number theory, Birkhäuser Boston, 1990, pp. 165-204.",
				"Paul Erdos, Andrew Granville, Carl Pomerance and Claudia Spiro, \u003ca href=\"/A000010/a000010_1.pdf\"\u003eOn the normal behavior of the iterates of some arithmetic functions\u003c/a\u003e, Analytic number theory, Birkhäuser Boston, 1990, pp. 165-204. [Annotated copy with A-numbers]",
				"factordb.com, \u003ca href=\"http://factordb.com/sequences.php?se=1\u0026amp;aq=276\u0026amp;action=last20\u0026amp;fr=0\u0026amp;to=100\"\u003eSearch result for last 20 terms of 276 sequence\u003c/a\u003e.",
				"N. J. A. Sloane, Three (No, 8) Lovely Problems from the OEIS, Experimental Mathematics Seminar, Rutgers University, Oct 05 2017, \u003ca href=\"https://vimeo.com/237029685\"\u003ePart I\u003c/a\u003e, \u003ca href=\"https://vimeo.com/237030304\"\u003ePart 2\u003c/a\u003e, \u003ca href=\"https://oeis.org/A290447/a290447_slides.pdf\"\u003eSlides.\u003c/a\u003e (Mentions this sequence)",
				"Paul Zimmermann, \u003ca href=\"http://www.loria.fr/~zimmerma/records/aliquot.html\"\u003eLatest information\u003c/a\u003e",
				"\u003ca href=\"/index/Al#ALIQUOT\"\u003eIndex entries for sequences related to aliquot parts\u003c/a\u003e."
			],
			"formula": [
				"a(n+1) = A001065(a(n)). - _R. J. Mathar_, Oct 11 2017"
			],
			"maple": [
				"f := proc(n) option remember; if n = 0 then 276; else sigma(f(n-1))-f(n-1); fi; end:"
			],
			"mathematica": [
				"NestList[DivisorSigma[1, #] - # \u0026, 276, 50] (* _Alonso del Arte_, Feb 24 2018 *)"
			],
			"program": [
				"(PARI) a(n, a=276)={for(i=1,n,a=sigma(a)-a);a} \\\\ _M. F. Hasler_, Feb 24 2018"
			],
			"xref": [
				"Cf. A098007 (length of aliquot sequences).",
				"Cf. A008885 (aliquot sequence starting at 30), ..., A008891 (starting at 180)."
			],
			"keyword": "nonn",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_",
			"references": 22,
			"revision": 48,
			"time": "2020-10-26T22:59:38-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}