{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A181107",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 181107,
			"data": "1,10,6,33,24,24,88,48,72,48,145,120,120,120,120,330,144,240,198,240,144,385,336,336,336,336,336,336,736,384,576,384,672,384,576,384,945,648,648,864,648,648,864,648,648,1450,720,1200,720,1200,870,1200,720,1200,720",
			"name": "Triangle read by rows: T(n,k) is the number of 2 X 2 matrices over Z(n) having determinant congruent to k mod n, 1 \u003c= n, 0 \u003c= k \u003c= n-1.",
			"comment": [
				"The n-th row is {T(n,0),T(n,1),...,T(n,n-1)}.",
				"Let m denote the prime power p^e.",
				"T(m,0) = A020478(m) = (p^(e+1) + p^e-1)*p^(2*e-1).",
				"T(m,1) = A000056(m) = (p^2-1)*p^(3*e-2).",
				"T(prime(n),1) = A127917(n).",
				"Sum_{k=1..n-1} T(n,k) = A005353(n).",
				"T(n,1) = n*A007434(n) for n\u003e=1 because A000056(n) = n*Jordan_Function_J_2(n).",
				"T(2^n,1) = A083233(n) = A164640(2n) for n\u003e=1. Proof: a(n):=T(2^n,1); a(1)=6, a(n)=8*a(n-1); A083233(1)=6 and A083233(n) is a geometric series with ratio 8 (because of its g.f.), too; A164640 = {b(1)=1, b(2)=6, b(n)=8*b(n-2)}.",
				"T(2^n,0) = A165148(n) for n\u003e=0, because  2*T(2^n,0) = (3*2^n-1)*4^n.",
				"T(2^e,2) = A003951(e) for 2 \u003c= e. Proof: T(2^e,2) = 9*8^(e-1) is a series with ratio 8 and initial term 72, as A003951(2...inf) is.",
				"Working with consecutive powers of a prime p, we need a definition (0 \u003c= i \u003c e):",
				"N(p^e,i):=#{k: 0 \u003c k \u003c p^e, gcd(k,p^e) = p^i} = (p-1)*p^(e-1-i). We say that these k's belong to i (respect to p^e). Note that N(p^e,0) = EulerPhi(p^e), and if 0 \u003c k \u003c p^e then gcd(k,p^e) = gcd(k,p^(e+1)). Let T(p^e,[i]) denote the common value of T(p^e,k)'s, where k's belong to i (q.v.PROGRAM); for example, T(p^e,[0]) = T(p^e,1). The number of the 2 X 2 matrices over Z(p^e), T(p^e,0) + Sum_{i=0..e-1} T(p^e,[i])*N(p^e,i) = p^(4e) will be useful.",
				"On the hexagon property: Let prime p be given and let T(p^e,[0]), T(p^e,[1]), T(p^e,[2]), ..., T(p^e,[e-2]), T(p^e,[e-1]) form the e-th row of a Pascal-like triangle, e\u003e=1. Let denote X(r,s) an element of the triangle and its value T(p^r,[s]). Let positive integers a and b given, so that the entries A(m-a,n-b), B(m-a,n), C(m,n+a), D(m+b,n+a), E(m+b,n), F(m,n-b) of the triangle form a hexagon spaced around T(p^m,[n]); if a=b=1 then they surround it. If A*C*E = B*D*F, then we say that the triangle T(.,.) has the \"hexagon property\". (In the case of binomial coefficients X(r,s) = COMB(r,s), the \"hexagon property\" holds (see [Gupta]) and moreover gcd(A,C,E) = gcd(B,D,F) (see [Hitotumatu \u0026 Sato]).)",
				"Corollary 2.2 in [Brent \u0026 McKay] says that, for the d X d matrices over Z(p^e), (mutatis mutandis) T_d(p^e,0) = K*(1-P(d+e-1)/P(e-1)) and T_d(p^e,[i]) = K*(q^e)*((1-q^d)/(1-q))*P(d+i-1)/P(i), where q=1/p, K=(p^e)^(d^2), P(t) = Product_{j=1..t} (1-q^j), P(0):=1. (For the case d=2, we have T(p^e,[i]) = (p+1)*(p^(i+1)-1)*p^(3*e-i-2).) Due to [Brent \u0026 McKay], it can be simply proved that for d X d matrices the \"hexagon property\" is true. The formulation implies an obvious generalization: For the entries A(r,u), B(r,v), C(s,w), D(t,w), E(t,v), F(s,u) of the T_d(.,.)-triangle, a hexagon-like property A*C*E = B*D*F holds. This is false in general for the COMB(.,.)-triangle.",
				"Another (rotated-hexagon-like) property: for the entries A(m-b1,n), B(m-a1,n+c2), C(m+a2,n+c2), D(m+b2,n), E(m+a2,n-c1), F(m-a1,n-c1) of the T_d(.,.)-triangle, the property A*C*E = B*D*F holds, if and only if 2*(a1 + a2) = b1 + b2. This is also in general false for COMB(.,.)-triangle."
			],
			"link": [
				"Erdos Pal, \u003ca href=\"/A181107/b181107.txt\"\u003eRows n=1..100 of triangle, flattened\u003c/a\u003e",
				"Richard P. Brent and Brendan D. McKay, \u003ca href=\"https://doi.org/10.1016/0012-365X(87)90117-8\"\u003eDeterminants and ranks of random matrices over Z_m\u003c/a\u003e, Discrete Mathematics 66 (1987) pp. 35-49.",
				"A. K. Gupta, \u003ca href=\"https://www.fq.math.ca/Scanned/12-1/gupta.pdf\"\u003eGeneralized hidden hexagon squares\u003c/a\u003e, The Fibonacci Quarterly, Vol 12, Number 1, Feb.1974, pp. 45-46.",
				"S. Hitotumatu, D. Sato, \u003ca href=\"https://www.fq.math.ca/Scanned/13-1/hitotumatu.pdf\"\u003eStar of David theorem (I)\u003c/a\u003e, The Fibonacci Quarterly, Vol 13, Number 1, Feb.1975, p. 70."
			],
			"formula": [
				"T(a*b,k) = T(a,(k mod a))*T(b,(k mod b)) if gcd(a,b) = 1.",
				"Sum_{k=1..n-1, gcd(k,n)=1} T(n,k) = A000252(n). - _Andrew Howroyd_, Jul 16 2018"
			],
			"example": [
				"From _Andrew Howroyd_, Jul 16 2018: (Start)",
				"Triangle begins:",
				"    1;",
				"   10,   6;",
				"   33,  24,  24;",
				"   88,  48,  72,  48;",
				"  145, 120, 120, 120, 120;",
				"  330, 144, 240, 198, 240, 144;",
				"  385, 336, 336, 336, 336, 336, 336;",
				"  736, 384, 576, 384, 672, 384, 576, 384;",
				"  945, 648, 648, 864, 648, 648, 864, 648, 648;",
				"  ... (End)"
			],
			"program": [
				"(Other) . (* computing T(p^e,k) ; p=prime, 1\u003c=e, 0\u003c=k\u003cp^e, elementary approach *)",
				". (1) F := (p-1)*p^(e-1)",
				". (2) u := [ u(0),u(1),...,u(p^e-1) ] vector, where",
				". (21) u(0) := p^e + e*F, and",
				". (22) FOR x := 1 TO p^e-1",
				". (22) u(x) := (i+1)*F, where GCD(x,p^e)= p^i",
				". (22) ENDFOR",
				". (3) T(p^e,k):= ScalarProduct( u, kTimesCyclicRightShift(u) )",
				"(PARI)",
				"S(p,e)={my(u=vector(p^e)); my(t=(p-1)*p^(e-1)); u[1] = p^e + e*t; for(j=1, p^e-1, u[j+1] = t*(1+valuation(j, p))); vector(#u, k, sum(j=0, #u-1, u[j + 1]*u[(j+k-1) % #u + 1]))}",
				"T(n)={my(f=factor(n), v=vector(n,i,1)); for(i=1, #f~, my(r=S(f[i,1], f[i,2])); for(j=0, #v-1, v[j + 1] *= r[j % #r + 1])); v}",
				"for(n=1, 10, print(T(n))); \\\\ _Andrew Howroyd_, Jul 16 2018"
			],
			"xref": [
				"Column k=0 is A020478.",
				"Column k=1 is A000056.",
				"Row sums are A005353.",
				"Cf. A000252, A127917."
			],
			"keyword": "mult,nonn,tabl",
			"offset": "1,2",
			"author": "_Erdos Pal_, Oct 03 2010",
			"ext": [
				"Terms a(24)-a(55) from b-file by _Andrew Howroyd_, Jul 16 2018"
			],
			"references": 2,
			"revision": 41,
			"time": "2020-08-13T02:05:51-04:00",
			"created": "2010-10-20T03:00:00-04:00"
		}
	]
}