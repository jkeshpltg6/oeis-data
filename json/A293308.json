{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A293308",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 293308,
			"data": "1,2,3,10,15,56,210,330,1287,2002,8008,31824,50388,203490,319770,1307504,2042975,8436285,34597290,54627300,225792840,354817320,1476337800,6107086800,9669554100,40225345056,63432274896,265182149218,416714805914,1749695026860",
			"name": "Number of permutations of zero-one words with A056576(n)-n zeros and n-1 ones.",
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A293308/b293308.txt\"\u003eTable of n, a(n) for n = 1..2211\u003c/a\u003e",
				"Mike Winkler, \u003ca href=\"https://arxiv.org/abs/1709.03385\"\u003eThe algorithmic structure of the finite stopping time behavior of the 3x + 1 function\u003c/a\u003e, arXiv:1709.03385 [math.GM], Sep 2017. [see (17) on p. 9]"
			],
			"formula": [
				"a(n) = ( A056576(n) - 1 )! / ( ( A056576(n) - n )! * ( n - 1)! )"
			],
			"example": [
				"a(4) = 5! / ( 2! * 3! ) = 5*4/2 = 10.",
				"From _Mike Winkler_, Oct 30 2017: (Start)",
				"The next table shows the output using the PARI function NextPermutation(a), (cf. PROG)",
				"[0, 0, 1, 1, 1] 1",
				"[0, 1, 0, 1, 1] 2",
				"[0, 1, 1, 0, 1] 3",
				"[0, 1, 1, 1, 0] 4",
				"[1, 0, 0, 1, 1] 5",
				"[1, 0, 1, 0, 1] 6",
				"[1, 0, 1, 1, 0] 7",
				"[1, 1, 0, 0, 1] 8",
				"[1, 1, 0, 1, 0] 9",
				"[1, 1, 1, 0, 0] 10",
				"(End)"
			],
			"mathematica": [
				"Table[(# - 1)!/((# - n)!*(n - 1)!) \u0026@ Floor[n Log[2, 3]], {n, 30}] (* _Michael De Vlieger_, Oct 06 2017 *)"
			],
			"program": [
				"(PARI) /* method used in the linked paper arXiv:1709.03385 */",
				"NextPermutation(a) = {i=#a-1; while(!(i\u003c1 || a[i]\u003ca[i+1]), i--); if(i\u003c1, return(0)); k=#a; while(!(a[k]\u003ea[i]), k--); t=a[k]; a[k]=a[i]; a[i]=t; for(k=i+1, (#a+i)/2, t=a[k]; a[k]=a[#a+1+i-k]; a[#a+1+i-k]=t); return(a)}",
				"  /* example for n = 4 */",
				"  {j=1; a=[0, 0, 1, 1, 1]; until(a==0, print(a\" \"j); j++; a=NextPermutation(a))} \\\\ _Mike Winkler_, Oct 30 2017"
			],
			"xref": [
				"Cf. A056576, A100982."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Frank Ellermann_, Oct 05 2017",
			"references": 1,
			"revision": 18,
			"time": "2017-10-30T22:52:01-04:00",
			"created": "2017-10-06T14:15:34-04:00"
		}
	]
}