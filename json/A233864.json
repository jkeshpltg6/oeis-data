{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A233864",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 233864,
			"data": "0,0,0,1,1,2,1,2,3,1,1,3,3,3,3,2,4,5,3,4,4,4,4,4,3,5,4,5,4,5,3,4,7,4,5,6,4,8,8,4,4,4,7,5,6,5,6,8,4,6,8,6,7,6,6,5,5,9,7,9,7,6,8,7,7,8,6,9,9,6,6,12,9,6,10,8,9,12,7,7,11,5,10,9,9,10,7,11,8,9,6,8,14,10,8,8,10,12,9,6",
			"name": "a(n) = |{0 \u003c m \u003c 2*n: m = sigma(k) for some k \u003e 0, and 2*n - 1 - m and 2*n - 1 + m are both prime}|, where sigma(k) is the sum of all (positive) divisors of k.",
			"comment": [
				"Conjecture: (i) a(n) \u003e 0 for all n \u003e 3.",
				"(ii) For any even number 2*n \u003e 0, 2*n + sigma(k) is prime for some 0 \u003c k \u003c 2*n.",
				"See also A233793 for a related conjecture.",
				"Clearly part (i) of the conjecture implies Goldbach's conjecture for even numbers 2*(2*n - 1) with n \u003e 3; we have verified part (i) for n up to 10^8. Concerning part (ii), we remark that 1024 is the unique positive integer k \u003c 1134 with 1134 + sigma(k) prime, and that sigma(1024) = 2047 \u003e 1134."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A233864/b233864.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"a(7) = 1 since sigma(5) = 6, and 2*7 - 1 - 6 = 7 and 2*7 - 1 + 6 = 19 are both prime.",
				"a(10) = 1 since sigma(6) = sigma(11) = 12, and 2*10 - 1 - 12 = 7 and 2*10 - 1 + 12 = 31 are both prime.",
				"a(11) = 1 since sigma(7) = 8, and 2*11 - 1 - 8 = 13 and 2*11 - 1 + 8 = 29 are both prime."
			],
			"mathematica": [
				"f[n_]:=Sum[If[Mod[n,d]==0,d,0],{d,1,n}]",
				"S[n_]:=Union[Table[f[j],{j,1,n}]]",
				"PQ[n_]:=n\u003e0\u0026\u0026PrimeQ[n]",
				"a[n_]:=Sum[If[PQ[2n-1-Part[S[2n-1],i]]\u0026\u0026PQ[2n-1+Part[S[2n-1],i]],1,0],{i,1,Length[S[2n-1]]}]",
				"Table[a[n],{n,1,100}]"
			],
			"xref": [
				"Cf. A000040, A000203, A002372, A002375, A232270, A233544, A233654, A233793."
			],
			"keyword": "nonn",
			"offset": "1,6",
			"author": "_Zhi-Wei Sun_, Dec 16 2013",
			"references": 3,
			"revision": 9,
			"time": "2013-12-17T03:55:47-05:00",
			"created": "2013-12-17T03:55:47-05:00"
		}
	]
}