{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001026",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1026,
			"id": "M5048 N2182",
			"data": "1,17,289,4913,83521,1419857,24137569,410338673,6975757441,118587876497,2015993900449,34271896307633,582622237229761,9904578032905937,168377826559400929,2862423051509815793,48661191875666868481,827240261886336764177,14063084452067724991009,239072435685151324847153,4064231406647572522401601",
			"name": "Powers of 17.",
			"comment": [
				"The compositions of n in which each natural number is colored by one of p different colors are called p-colored compositions of n. For n\u003e=1, a(n) equals the number of 17-colored compositions of n such that no adjacent parts have the same color. - _Milan Janjic_, Nov 17 2011",
				"Numbers n such that sigma(17*n) = 17*n + sigma(n). - _Jahangeer Kholdi_, Nov 23 2013"
			],
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A001026/b001026.txt\"\u003eTable of n, a(n) for n = 0..100\u003c/a\u003e",
				"P. J. Cameron, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL3/groups.html\"\u003eSequences realized by oligomorphic permutation groups\u003c/a\u003e, J. Integ. Seqs. Vol. 3 (2000), #00.1.5.",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=281\"\u003eEncyclopedia of Combinatorial Structures 281\u003c/a\u003e",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"Y. Puri and T. Ward, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL4/WARD/short.html\"\u003eArithmetic and growth of periodic orbits\u003c/a\u003e, J. Integer Seqs., Vol. 4 (2001), #01.2.1.",
				"\u003ca href=\"/index/Rec#order_01\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (17)."
			],
			"formula": [
				"G.f.: 1/(1-17x), e.g.f.: exp(17x).",
				"a(n)=17^n ; a(n)=17*a(n-1) n\u003e0, a(0)=1. - _Vincenzo Librandi_, Nov 21 2010",
				"G.f.: 1 + x*(G(0) - 1)/(x-1) where G(k) = 1 - (4(k+1)^2+1)/(1-x/(x - 1/G(k+1) )); (continued fraction). - _Sergei N. Gladkovskii_, Jan 15 2013"
			],
			"maple": [
				"A001026:=-1/(-1+17*z); [_Simon Plouffe_ in his 1992 dissertation.]"
			],
			"mathematica": [
				"Table[17^n,{n,0,40}] (* _Vladimir Joseph Stephan Orlovsky_, Feb 15 2011 *)"
			],
			"program": [
				"(Sage) [lucas_number1(n,17,0) for n in range(1, 17)] # _Zerinvary Lajos_, Apr 29 2009",
				"(MAGMA)[17^n: n in [0..100]]; // _Vincenzo Librandi_, Nov 21 2010",
				"(Maxima) A001026(n):=17^n$",
				"makelist(A001026(n),n,0,30); /* _Martin Ettl_, Nov 05 2012 */",
				"(PARI) a(n)=17^n \\\\ _Charles R Greathouse IV_, Sep 24 2015"
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _James A. Sellers_, Sep 19 2000"
			],
			"references": 27,
			"revision": 72,
			"time": "2021-03-12T22:32:34-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}