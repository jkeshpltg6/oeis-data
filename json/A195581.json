{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A195581",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 195581,
			"data": "1,0,1,0,0,2,0,0,2,4,0,0,0,16,8,0,0,0,40,64,16,0,0,0,80,400,208,32,0,0,0,80,2240,2048,608,64,0,0,0,0,11360,18816,8352,1664,128,0,0,0,0,55040,168768,104448,30016,4352,256,0,0,0,0,253440,1508032,1277568,479040,99200,11008,512",
			"name": "Number T(n,k) of permutations of {1,2,...,n} that result in a binary search tree of height k; triangle T(n,k), n\u003e=0, 0\u003c=k\u003c=n, read by rows.",
			"comment": [
				"Empty external nodes are counted in determining the height of a search tree."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A195581/b195581.txt\"\u003eRows n = 0..140, flattened\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Binary_search_tree\"\u003eBinary search tree\u003c/a\u003e",
				"\u003ca href=\"/index/Ro#rooted\"\u003eIndex entries for sequences related to rooted trees\u003c/a\u003e",
				"\u003ca href=\"/index/Tra#trees\"\u003eIndex entries for sequences related to trees\u003c/a\u003e"
			],
			"formula": [
				"Sum_{k=0..n} k * T(n,k) = A316944(n).",
				"Sum_{k=n..2^n-1} k * T(k,n) = A317012(n)."
			],
			"example": [
				"T(3,3) = 4, because 4 permutations of {1,2,3} result in a binary search tree of height 3:",
				"  (1,2,3):   1       (1,3,2):   1     (3,1,2):   3   (3,2,1):   3",
				"            / \\                / \\              / \\            / \\",
				"           o   2              o   3            1   o          2   o",
				"              / \\                / \\          / \\            / \\",
				"             o   3              2   o        o   2          1   o",
				"                / \\            / \\              / \\        / \\",
				"               o   o          o   o            o   o      o   o",
				"Triangle T(n,k) begins:",
				"  1;",
				"  0, 1;",
				"  0, 0, 2;",
				"  0, 0, 2,  4;",
				"  0, 0, 0, 16,      8;",
				"  0, 0, 0, 40,     64,      16;",
				"  0, 0, 0, 80,    400,     208,      32;",
				"  0, 0, 0, 80,   2240,    2048,     608,     64;",
				"  0, 0, 0,  0,  11360,   18816,    8352,   1664,   128;",
				"  0, 0, 0,  0,  55040,  168768,  104448,  30016,  4352,   256;",
				"  0, 0, 0,  0, 253440, 1508032, 1277568, 479040, 99200, 11008, 512;"
			],
			"maple": [
				"b:= proc(n, k) option remember; `if`(n\u003c2, `if`(k\u003cn, 0, 1),",
				"      add(binomial(n-1, r)*b(r, k-1)*b(n-1-r, k-1), r=0..n-1))",
				"    end:",
				"T:= (n, k)-\u003e b(n, k)-b(n, k-1):",
				"seq(seq(T(n, k), k=0..n), n=0..10);"
			],
			"mathematica": [
				"b[n_, k_] := b[n, k] = If[n == 0, 1, If[n == 1, If[k \u003e 0, 1, 0], Sum[Binomial[n-1, r-1]*b[r-1, k-1]*b[n-r, k-1], {r, 1, n}] ] ]; t [n_, k_] := b[n, k] - If[k \u003e 0, b[n, k-1], 0]; Table[Table[t[n, k], {k, 0, n}], {n, 0, 10}] // Flatten (* _Jean-François Alcover_, Dec 17 2013, translated from Maple *)"
			],
			"xref": [
				"Row sums give A000142. Column sums give A227822.",
				"Main diagonal gives A011782, lower diagonal gives A076616.",
				"T(n,A000523(n)+1) = A076615(n).",
				"T(2^n-1,n) = A056972(n).",
				"T(2n,n) = A265846(n).",
				"Cf. A195582, A195583, A244108 (the same read by columns), A316944, A317012."
			],
			"keyword": "nonn,tabl",
			"offset": "0,6",
			"author": "_Alois P. Heinz_, Sep 20 2011",
			"references": 20,
			"revision": 54,
			"time": "2020-07-07T20:21:29-04:00",
			"created": "2011-09-21T17:37:29-04:00"
		}
	]
}