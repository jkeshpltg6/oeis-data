{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A178881",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 178881,
			"data": "0,1,3,7,11,20,26,38,50,67,77,105,117,142,172,204,220,265,283,335,379,420,442,518,558,607,661,737,765,870,900,980,1052,1117,1199,1331,1367,1440,1526,1666,1706,1859,1901,2025,2169,2258,2304,2496,2580,2725",
			"name": "Sum of all pairs of greater common divisors for (i,j) where 1 \u003c= i \u003c j \u003c= n.",
			"comment": [
				"You could also be looking for the case where i = j is allowed, which gives A272718(n) = a(n) + n*(n+1)/2."
			],
			"link": [
				"Akshay Bansal, \u003ca href=\"/A178881/b178881.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Akshay Bansal, \u003ca href=\"/A178881/a178881.c.txt\"\u003eC program\u003c/a\u003e",
				"Olivier Bordellès, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL10/Bordelles/bordelles90.html\"\u003eA note on the average order of the gcd-sum function\u003c/a\u003e, Journal of Integer Sequences, vol 10 (2007), article 07.3.3.",
				"Uva Online Judge Algorithm \u003ca href=\"http://uva.onlinejudge.org/external/114/11424.html\"\u003eProblem number 11424\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Sum_{i=1..n-1, j=i+1..n} gcd(i,j).",
				"From _Jianing Song_, Feb 07 2021: (Start)",
				"a(n) = (A018806(n) - n*(n+1)/2) / 2 = (Sum_{k=1..n} phi(k)*(floor(n/k))^2 - n*(n+1)/2) / 2, phi = A000010.",
				"a(n) = A018806(n) - A272718(n).",
				"According to Bordellès (2007), a(n) = (3/Pi^2)*n^2*log(n) + k*n^2 + O(n^(1+theta+epsilon)), where k = (3/Pi^2)*(gamma - 1/2 + log(A^12/(2*Pi)) - 1/2, gamma = A001620, A ~= 1.282427129 is the Glaisher-Kinkelin constant A074962, theta is a certain constant defined in terms of the divisor function and known to lie between 1/4 and 131/416, and epsilon is any positive number. See also A272718. (End)"
			],
			"example": [
				"Denote gcd(i,j) by (i,j), then a(6) = (1,2) + (1,3) + (1,4) + (1,5) + (1,6) + (2,3) + (2,4) + (2,5) + (2,6) + (3,4) + (3,5) + (3,6) + (4,5) + (4,6) + (5,6) = 20. - _Jianing Song_, Feb 07 2021"
			],
			"program": [
				"(C++)",
				"#include \u003ciostream\u003e",
				"#include \u003calgorithm\u003e",
				"#include \u003ccmath\u003e",
				"using namespace std;",
				"int main() {",
				"  int N;",
				"  for (N=1;N\u003c=50;++N) {",
				"    int G=0,i,j;",
				"    for(i=1;i\u003cN;i++)",
				"      for(j=i+1;j\u003c=N;j++) {",
				"        G+=__gcd(i,j);",
				"      }",
				"    cout \u003c\u003cG\u003c\u003c\",\";",
				"  }",
				"  return 0;",
				"}",
				"(PARI) a(n)=sum(k=1, n, eulerphi(k)*(n\\k)^2)/2-n*(n+1)/4 \\\\ _Charles R Greathouse IV_, Apr 11 2016",
				"(PARI) first(n)=my(v=vector(n),t); for(k=1,n, t=eulerphi(k); for(m=k,n, v[m] += t*(m\\k)^2)); v/2-vector(n,k,k*(k+1)/4) \\\\ _Charles R Greathouse IV_, Apr 11 2016"
			],
			"xref": [
				"Cf. A018806, A000010, A001620, A074962, A272718."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "Enric Cusell (cusell(AT)gmail.com), Jun 20 2010",
			"references": 2,
			"revision": 21,
			"time": "2021-02-12T17:51:15-05:00",
			"created": "2010-07-11T03:00:00-04:00"
		}
	]
}