{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A305048",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 305048,
			"data": "0,1,1,0,2,3,2,2,2,4,1,3,5,1,4,3,3,4,2,2,3,2,4,4,2,5,4,4,2,2,2,4,4,6,6,4,3,3,7,6,6,2,4,3,5,3,2,3,8,3,4,4,1,3,5,5,6,5,6,4,3,5,1,1,3,4,4,2,7,2,4,4,2,8,3,7,7,3,5,4,6,1,3,4,4,7,5,4,6,2",
			"name": "Number of ordered pairs (k, m) of nonnegative integers such that 5^k + 10^m is not only a primitive root modulo prime(n) but also smaller than prime(n).",
			"comment": [
				"Conjecture: a(n) \u003e 0 for all n \u003e 4. In other words, any prime p \u003e 7 has a primitive root g \u003c p of the form 5^k + 10^m with k and m nonnegative integers.",
				"We have verified this for any prime p \u003e 7 not exceeding 10^9.",
				"It seems that a(n) = 1 only for n = 2, 3, 11, 14, 53, 63, 64, 82, 99, 101, 111, 129, 344, 369, 391, 795, 1170, 1587, 5629, 5718, 6613, 430516.",
				"See also A305048 for similar conjectures."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A305048/b305048.txt\"\u003eTable of n, a(n) for n = 1..100000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1405.0290\"\u003eNew observations on primitive roots modulo primes\u003c/a\u003e, arXiv:1405.0290 [math.NT], 2014.",
				"Zhi-Wei Sun, \u003ca href=\"http://maths.nju.edu.cn/~zwsun/160p.pdf\"\u003eProblems on combinatorial properties of primes\u003c/a\u003e, in: M. Kaneko, S. Kanemitsu and J. Liu (eds.), Number Theory: Plowing and Starring through High Wave Forms, Proc. 7th China-Japan Seminar (Fukuoka, Oct. 28--Nov. 1, 2013), Ser. Number Theory Appl., Vol. 11, World Sci., Singapore, 2015, pp. 169-187."
			],
			"example": [
				"a(14) = 1 with 5^2 + 10^0 = 26 a primitive root modulo prime(14) = 43.",
				"a(101) = 1 with 5^0 + 10^0 = 2 a primitive root modulo prime(101) = 547.",
				"a(111) = 1 with 5^2 + 10 = 35 a primitive root modulo prime(111) = 607.",
				"a(5718) = 1 with 5^0 + 10^3 = 1001 a primitive root modulo prime(5718) = 56401.",
				"a(6613) = 1 with 5^1 + 10^3 = 1005 a primitive root modulo prime(6613) = 66301.",
				"a(430516) = 1 with 5^5 + 10^1 = 3135 a primitive root modulo prime(430516) = 6276271."
			],
			"mathematica": [
				"p[n_]:=p[n]=Prime[n];",
				"Dv[n_]:=Dv[n]=Divisors[n];",
				"gp[g_,p_]:=gp[g,p]=Mod[g,p]\u003e0\u0026\u0026Sum[Boole[PowerMod[g,Dv[p-1][[k]],p]==1],{k,1,Length[Dv[p-1]]-1}]==0;",
				"tab={};Do[r=0;Do[If[gp[5^a+10^b,p[n]],r=r+1],{a,0,Log[5,p[n]-1]},{b,0,Log[10,p[n]-5^a]}];tab=Append[tab,r],{n,1,90}];Print[tab]"
			],
			"xref": [
				"A000040, A000351, A011557, A303540, A239957, A241476, A241504, A241516, A305030."
			],
			"keyword": "nonn",
			"offset": "1,5",
			"author": "_Zhi-Wei Sun_, May 24 2018",
			"references": 2,
			"revision": 12,
			"time": "2018-05-25T06:02:16-04:00",
			"created": "2018-05-24T09:56:45-04:00"
		}
	]
}