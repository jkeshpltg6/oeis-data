{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A292508",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 292508,
			"data": "1,1,0,1,1,1,1,2,2,1,1,3,4,3,2,1,4,7,7,5,2,1,5,11,14,12,7,4,1,6,16,25,26,19,11,4,1,7,22,41,51,45,30,15,7,1,8,29,63,92,96,75,45,22,8,1,9,37,92,155,188,171,120,67,30,12,1,10,46,129,247,343,359,291,187,97,42,14",
			"name": "Number A(n,k) of partitions of n with k kinds of 1; square array A(n,k), n\u003e=0, k\u003e=0, read by antidiagonals.",
			"comment": [
				"Partial sum operator applied to column k gives column k+1.",
				"A(n,k) is also defined for k \u003c 0. All given formulas and programs can be applied also if k is negative."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A292508/b292508.txt\"\u003eAntidiagonals n = 0..140, flattened\u003c/a\u003e"
			],
			"formula": [
				"G.f. of column k: 1/(1-x)^k * 1/Product_{j\u003e1} (1-x^j).",
				"Column k is Euler transform of k,1,1,1,... .",
				"For fixed k\u003e=0, A(n,k) ~ 2^((k-5)/2) * 3^((k-2)/2) * n^((k-3)/2) * exp(Pi*sqrt(2*n/3)) / Pi^(k-1). - _Vaclav Kotesovec_, Oct 24 2018"
			],
			"example": [
				"Square array A(n,k) begins:",
				"  1,  1,  1,   1,   1,    1,    1,    1,     1, ...",
				"  0,  1,  2,   3,   4,    5,    6,    7,     8, ...",
				"  1,  2,  4,   7,  11,   16,   22,   29,    37, ...",
				"  1,  3,  7,  14,  25,   41,   63,   92,   129, ...",
				"  2,  5, 12,  26,  51,   92,  155,  247,   376, ...",
				"  2,  7, 19,  45,  96,  188,  343,  590,   966, ...",
				"  4, 11, 30,  75, 171,  359,  702, 1292,  2258, ...",
				"  4, 15, 45, 120, 291,  650, 1352, 2644,  4902, ...",
				"  7, 22, 67, 187, 478, 1128, 2480, 5124, 10026, ..."
			],
			"maple": [
				"A:= proc(n, k) option remember; `if`(n=0, 1, add(",
				"      (numtheory[sigma](j)+k-1)*A(n-j, k), j=1..n)/n)",
				"    end:",
				"seq(seq(A(n, d-n), n=0..d), d=0..14);",
				"# second Maple program:",
				"A:= proc(n, k) option remember; `if`(n=0, 1, `if`(k\u003c1,",
				"      A(n, k+1)-A(n-1, k+1), `if`(k=1, combinat[numbpart](n),",
				"      A(n-1, k)+A(n, k-1))))",
				"    end:",
				"seq(seq(A(n, d-n), n=0..d), d=0..14);",
				"# third Maple program:",
				"b:= proc(n, i, k) option remember; `if`(n=0 or i\u003c2,",
				"      binomial(k+n-1, n), add(b(n-i*j, i-1, k), j=0..n/i))",
				"    end:",
				"A:= (n, k)-\u003e b(n$2, k):",
				"seq(seq(A(n, d-n), n=0..d), d=0..14);"
			],
			"mathematica": [
				"b[n_, i_, k_] := b[n, i, k] = If[n == 0 || i \u003c 2, Binomial[k + n - 1, n], Sum[b[n - i*j, i - 1, k], {j, 0, n/i}]];",
				"A[n_, k_] := b[n, n, k];",
				"Table[A[n, d - n], {d, 0, 14}, {n, 0, d}] // Flatten (* _Jean-François Alcover_, May 17 2018, translated from 3rd Maple program *)"
			],
			"xref": [
				"Columns k=0-10 give: A002865, A000041, A000070, A014153, A014160, A014161, A120477, A320753, A320754, A320755, A320756.",
				"Rows n=0-4 give: A000012, A001477, A000124, A004006(k+1), A027927(k+3).",
				"Main diagonal gives A292463.",
				"A(n,n+1) gives A292613.",
				"Cf. A292622, A292741, A292745."
			],
			"keyword": "nonn,tabl",
			"offset": "0,8",
			"author": "_Alois P. Heinz_, Sep 17 2017",
			"references": 20,
			"revision": 40,
			"time": "2021-07-16T20:56:38-04:00",
			"created": "2017-09-18T11:50:20-04:00"
		}
	]
}