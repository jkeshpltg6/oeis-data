{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A022599",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 22599,
			"data": "1,-4,6,-8,17,-28,38,-56,84,-124,172,-232,325,-448,594,-784,1049,-1388,1796,-2320,3005,-3864,4912,-6216,7877,-9940,12430,-15488,19309,-23972,29580,-36408,44766,-54876,66978,-81536,99150,-120272,145374,-175344,211242",
			"name": "Expansion of Product_{m\u003e=1} (1+q^m)^(-4).",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"McKay-Thompson series of class 12J for the Monster group."
			],
			"reference": [
				"T. J. I'a. Bromwich, Introduction to the Theory of Infinite Series, Macmillan, 2nd. ed. 1949, p. 116, q_2^4."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A022599/b022599.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e (terms 0..1000 from Alois P. Heinz)",
				"J. H. Conway and S. P. Norton, \u003ca href=\"http://blms.oxfordjournals.org/content/11/3/308.extract\"\u003eMonstrous Moonshine\u003c/a\u003e, Bull. Lond. Math. Soc. 11 (1979) 308-339.",
				"D. Ford, J. McKay and S. P. Norton, \u003ca href=\"http://dx.doi.org/10.1080/00927879408825127\"\u003eMore on replicable functions\u003c/a\u003e, Commun. Algebra 22, No. 13, 5175-5193 (1994).",
				"Vaclav Kotesovec, \u003ca href=\"http://arxiv.org/abs/1509.08708\"\u003eA method of finding the asymptotics of q-series based on the convolution of generating functions\u003c/a\u003e, arXiv:1509.08708 [math.CO], Sep 30 2015, p. 13.",
				"J. McKay and H. Strauss, \u003ca href=\"http://dx.doi.org/10.1080/00927879008823911\"\u003eThe q-series of monstrous moonshine and the decomposition of the head characters\u003c/a\u003e, Comm. Algebra 18 (1990), no. 1, 253-278.",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e",
				"\u003ca href=\"/index/Mat#McKay_Thompson\"\u003eIndex entries for McKay-Thompson series for Monster simple group\u003c/a\u003e"
			],
			"formula": [
				"Expansion of chi(-x)^4 in powers of x where chi() is a Ramanujan theta function.",
				"Expansion of q^(1/6) * (eta(q) / eta(q^2))^4 in powers of q.",
				"Euler transform of period 2 sequence [ -4, 0, ...]. - _Michael Somos_, Apr 26 2008",
				"Given G.f. A(x) then B(q) = (A(q^6) / q)^2 satisfies 0 = f(B(q), B(q^2)) where f(u, v) = u * (16 + u * v) - v^2. - _Michael Somos_, Apr 26 2008",
				"Given G.f. A(x) then B(q) = A(q^6) / q satisfies 0 = f(B(q), B(q^2), B(q^4)) where f(u, v, w) = 4 * v * (v + u^2) - w^2 * (v - u^2). - _Michael Somos_, Apr 26 2008",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (72 t)) = 4 g(t) where q = exp(2 Pi i t) and g() is the g.f. of A022569.",
				"Convolution inverse is A022569. Convolution square of A022597. Convolution square is A007259.",
				"a(n) ~ (-1)^n * exp(Pi*sqrt(2*n/3)) / (2 * 6^(1/4) * n^(3/4)). - _Vaclav Kotesovec_, Aug 27 2015",
				"a(0) = 1, a(n) = -(4/n)*Sum_{k=1..n} A000593(k)*a(n-k) for n \u003e 0. - _Seiichi Manyama_, Apr 03 2017",
				"G.f.: exp(-4*Sum_{k\u003e=1} (-1)^(k+1)*x^k/(k*(1 - x^k))). - _Ilya Gutkovskiy_, Feb 06 2018"
			],
			"example": [
				"G.f. = 1 - 4*x + 6*x^2 - 8*x^3 + 17*x^4 - 28*x^5 + 38*x^6 - 56*x^7 + ...",
				"T12J = 1/q - 4*q^5 + 6*q^11 - 8*q^17 + 17*q^23 - 28*q^29 + 38*q^35 + ..."
			],
			"maple": [
				"with(numtheory):",
				"a:= proc(n) option remember; `if`(n=0, 1, add(add(",
				"      -4*irem(d, 2)*d, d=divisors(j))*a(n-j), j=1..n)/n)",
				"    end:",
				"seq(a(n), n=0..50);  # _Alois P. Heinz_, May 02 2014"
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ (QPochhammer[ x] / QPochhammer[x^2])^4, {x, 0, n}]; (* _Michael Somos_, Jul 05 2014 *)",
				"nmax = 50; CoefficientList[Series[Product[1/(1 + x^k)^4, {k, 1, nmax}], {x, 0, nmax}], x] (* _Vaclav Kotesovec_, Aug 27 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( (eta(x + A) / eta(x^2 + A))^4, n))};"
			],
			"xref": [
				"Cf. A007259, A022597, A022569.",
				"Column k=4 of A286352."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 5,
			"revision": 40,
			"time": "2021-03-12T22:24:41-05:00",
			"created": "1998-06-14T03:00:00-04:00"
		}
	]
}