{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A334339",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 334339,
			"data": "1,51,34,291,22,17,1,1347,597,11,10,97,892,51,46,1758,6,3540,343,1649,34,5,30,449,2928,446,199,291,472,23,34,879,235,3,22,1770,8661,356,3007,1593,884,17,241,298,1416,15,22,586,133,1464,2,223,3,1180,2,1347,711,236,232,1062,1200,17,597,96771,586,265,577,485,10,11",
			"name": "Least positive integer m such that sigma(m * n) is a cube, where sigma(k) is the sum of the divisors of k.",
			"comment": [
				"Conjecture: a(n) exists for any n \u003e 0. In other words, for any positive integer n, there is a positive integer m with sigma(m * n) equal to a cube.",
				"The author's conjecture in A259915 implies that for each n = 1, 2, 3, ... there is a positive integer m with sigma(m * n) equal to a square.",
				"See also A334337 for a similar conjecture."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A334339/b334339.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"https://doi.org/10.1007/978-3-319-68032-3_20\"\u003eConjectures on representations involving primes\u003c/a\u003e, in: M. Nathanson (ed.), Combinatorial and Additive Number Theory II, Springer Proc. in Math. \u0026 Stat., Vol. 220, Springer, Cham, 2017, pp. 279-310. See also \u003ca href=\"http://arxiv.org/abs/1211.1588\"\u003earXiv\u003c/a\u003e, arXiv:1211.1588 [math.NT], 2012-2017. (Cf. Conjecture 4.5.)"
			],
			"example": [
				"a(2) = 51 with sigma(2*51) = 216 = 6^3.",
				"a(4) = 291 with sigma(4*291) = 2744 = 14^3.",
				"a(578) = 34312749 with sigma(578*34312749) = 42144192000 = 3480^3.",
				"a(673) = 49061802 with sigma(673*49061802) = 66135317184 = 4044^3."
			],
			"mathematica": [
				"cubeQ[n_] := cubeQ[n] = IntegerQ[n^(1/3)];",
				"sigma[n_] := sigma[n] = DivisorSigma[1, n];",
				"tab = {}; Do[m = 0; Label[aa]; m = m + 1; If[cubeQ[sigma[m * n]], tab = Append[tab, m], Goto[aa]], {n, 70}]; tab",
				"lpi[n_]:=Module[{k=1},While[!IntegerQ[Surd[DivisorSigma[1,n*k],3]],k++]; k]; Array[lpi,70] (* _Harvey P. Dale_, Nov 05 2020 *)"
			],
			"program": [
				"(PARI) a(n) = my(m=1); while (!ispower(sigma(n*m), 3), m++); m; \\\\ _Michel Marcus_, Apr 23 2020"
			],
			"xref": [
				"Cf. A000203, A000578, A020477, A259915, A334337."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Zhi-Wei Sun_, Apr 23 2020",
			"ext": [
				"Corrected and extended by _Harvey P. Dale_, Nov 05 2020"
			],
			"references": 3,
			"revision": 24,
			"time": "2020-11-05T19:45:10-05:00",
			"created": "2020-04-23T22:25:06-04:00"
		}
	]
}