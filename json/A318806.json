{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A318806",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 318806,
			"data": "1,1,2,1,2,3,1,2,3,4,1,2,4,5,6,1,2,4,6,7,8,1,2,4,7,9,10,11,1,2,4,7,10,12,13,14,1,2,4,8,12,15,17,18,19,1,2,4,8,13,17,20,22,23,24,1,2,4,8,14,20,24,27,29,30,31,1,2,4,8,15,22,28,32,35,37,38,39,1,2,4,8,15,24,32,38,42,45,47",
			"name": "Triangular array read by rows, where T(n,k) is the number of almost distinct partitions of n in which every part is \u003c= k for 1 \u003c= k \u003c= n.",
			"comment": [
				"An almost distinct partition of n with parts bounded by k is a decreasing sequence of positive integers (a(1), a(2), ..., a(k)) such that n = a(1) + a(2) +...+ a(k), any a(i) \u003e 1 is distinct from all other values, and all a(i) \u003c= k."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A318806/b318806.txt\"\u003eTable of n, a(n) for n = 1..3240\u003c/a\u003e (rows 1 \u003c= n \u003c= 80, flattened).",
				"Sara Billey, Matjaž Konvalinka, and Joshua P. Swanson, \u003ca href=\"http://arxiv.org/abs/1809.07386\"\u003eTableaux posets and the fake degrees of coinvariant algebras\u003c/a\u003e, arXiv:1809.07386 [math.CO], 2018."
			],
			"example": [
				"There are T(5,6) = 7 almost distinct partitions of 6 in which every part is \u003c= 5: [5,1], [4,2], [4,1,1], [3,2,1], [3,1,1,1], [2,1,1,1,1], [1,1,1,1,1,1].",
				"Triangle starts:",
				"1;",
				"1, 2;",
				"1, 2, 3;",
				"1, 2, 3, 4;",
				"1, 2, 4, 5,  6;",
				"1, 2, 4, 6,  7,  8;",
				"1, 2, 4, 7,  9, 10, 11;",
				"1, 2, 4, 7, 10, 12, 13, 14;",
				"1, 2, 4, 8, 12, 15, 17, 18, 19;",
				"1, 2, 4, 8, 13, 17, 20, 22, 23, 24;",
				"..."
			],
			"mathematica": [
				"Array[Table[Count[#, _?(# \u003c= k \u0026)], {k, Max@ #}] \u0026@ DeleteCases[Map[Boole[Flatten@ MapAt[Union, TakeDrop[#, LengthWhile[#, # == 1 \u0026]], -1] == # \u0026@ Reverse@ #] Max@ # \u0026, Reverse@ IntegerPartitions[#]], 0] \u0026, 13] // Flatten (* _Michael De Vlieger_, Dec 12 2018 *)"
			],
			"xref": [
				"Cf. A000009, A026820, A008302."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Sara Billey_, Sep 04 2018",
			"references": 1,
			"revision": 25,
			"time": "2018-12-12T14:24:26-05:00",
			"created": "2018-10-03T03:34:09-04:00"
		}
	]
}