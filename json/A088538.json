{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A088538",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 88538,
			"data": "1,2,7,3,2,3,9,5,4,4,7,3,5,1,6,2,6,8,6,1,5,1,0,7,0,1,0,6,9,8,0,1,1,4,8,9,6,2,7,5,6,7,7,1,6,5,9,2,3,6,5,1,5,8,9,9,8,1,3,3,8,7,5,2,4,7,1,1,7,4,3,8,1,0,7,3,8,1,2,2,8,0,7,2,0,9,1,0,4,2,2,1,3,0,0,2,4,6,8,7,6,4,8,5,8",
			"name": "Decimal expansion of 4/Pi.",
			"comment": [
				"Average length of chord formed from two randomly chosen points on the circumference of a unit circle (see Weisstein/MathWorld link). - _Rick L. Shepherd_, Jun 19 2006",
				"Suppose u(0) = 1 + i where i^2 = -1 and u(n+1) = (1/2)*(u(n) + |u(n)|). Conjecture: limit(Real(u(n)), n = +infinity) = 4/Pi. - _Yalcin Aktar_, Jul 18 2007",
				"Ratio of the arc length of the cycloid for one period to the circumference of the corresponding circle rolling on a line. Thus, for any integral number n of revolutions of a circle of radius r, a point on the circle travels 4/Pi*2Pi*r*n = 8rn (while the center of the circle moves only 2Pi*rn). This ratio varies for partial revolutions and depends upon the initial position of the point with points nearest the line moving the slowest (see Dudeney, who explains how the tops of bicycle wheels move faster than the parts nearest the ground). - _Rick L. Shepherd_, May 05 2014",
				"Average distance traveled in two steps of length 1 for a random walk in the plane starting at the origin. - _Jean-François Alcover_, Aug 04 2014",
				"Ratio of the circle area to the area of a square having equal perimeters. - _Iaroslav V. Blagouchine_, May 06 2016",
				"This is also the value of a special case (n=1) of an n-family of series considered by Hardy (see A278145): 1 + (1/2)*(1/2)^2  + (1/3)*(1*3/(2*4))^2 + (1/4)*((1*3*5) / (2*4*6))^2 + ... = Sum_{k\u003e=0} (1/(k+1))*((2*k-1)!!/(2*k)!!)^2. - _Wolfdieter Lang_, Nov 14 2016",
				"Minimum ratio of the area of a rectangle to one of its inscribed ellipses, or only existing ratio if the rectangle is a square and then the only inscribed ellipse is a circle. This ellipse has its semiaxes parallel to the sides of the rectangle. If a rectangle has sides of length 2a and 2b, its area is 4*a*b, while the ellipse inscribed has a and b as semiaxes, therefore its area is a*b*Pi. Thus the ratio is (4*a*b)/(a*b*Pi) = 4/Pi. - _Giovanni Zedda_, Jun 20 2019",
				"The diameter of the conventional spherical earth is (4/Pi)*10000 km = 12732.395... km. - _Jean-François Alcover_, Oct 30 2021"
			],
			"reference": [
				"H. E. Dudeney, 536 Puzzles \u0026 Curious Problems, Charles Scribner's Sons, New York, 1967, pp. 99, 300-301, #294.",
				"S. R. Finch, Mathematical Constants, Encyclopedia of Mathematics and its Applications, vol. 94, Cambridge University Press, p. 86",
				"G. H. Hardy, Ramanujan, AMS Chelsea Publ., Providence, RI, 2002, p. 105, eq. (7.5.1) for n=1.",
				"L. B. W. Jolley, Summation of Series, Dover (1961)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A088538/b088538.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"J.-P. Allouche, \u003ca href=\"http://arxiv.org/abs/1307.3906\"\u003eOn a formula of T. Rivoal\u003c/a\u003e, arXiv:1307.3906 [math.NT], 2013.",
				"Friedrich L. Bauer, \u003ca href=\"https://doi.org/10.1007/s00287-008-0254-0\"\u003e Historische Notizen / Wallis-artige Kettenprodukte\u003c/a\u003e, Informatik Spektrum 31,4 (2008) 348-352.",
				"J. M. Borwein, A. Straub, J. Wan, and W. Zudilin, \u003ca href=\"http://arxiv.org/abs/1103.2995\"\u003eDensities of short uniform random walks\u003c/a\u003e, arXiv:1103.2995 [math.CA], 2011.",
				"R. J. Mathar, \u003ca href=\"https://arxiv.org/abs/math/0403344\"\u003eChebyshev Series Expansion of Inverse Polynomials\u003c/a\u003e, arXiv:0403344 [math.CA], 2004-2005.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CircleLinePicking.html\"\u003eCircle Line Picking\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Cycloid.html\"\u003eCycloid\u003c/a\u003e.",
				"\u003ca href=\"/index/Tra#transcendental\"\u003eIndex entries for transcendental numbers\u003c/a\u003e"
			],
			"formula": [
				"4/Pi = Product_(1-(-1)^((p-1)/2)/p) where p runs through the odd primes.",
				"Arcsin x = (4/Pi) Sum_{n = 1, 3, 5, 7, ...} T_n(x)/n^2 (Chebyshev series of arcsin; App C of math.CA/0403344). - _R. J. Mathar_, Jun 26 2006",
				"Equals 1 + Sum_{n \u003e= 1} ((2n-3)!!/(2n)!!)^2. [Jolley eq 274]. - _R. J. Mathar_, Nov 03 2011",
				"Equals binomial(1,1/2). - _Bruno Berselli_, May 17 2016",
				"2*A060294 (twice Buffon's constant) = 1/Gamma(3/2)^2. - _Wolfdieter Lang_, Nov 14 2016",
				"Equals 1 + Sum_{n\u003e=0} (Catalan(n)/2^(2*n+1))^2 , with Catalan(n) = A000108(n). This is the rewritten Jolley (274) series. See the above R. J. Mathar entry with (-1)!! := 1. - _Ralf Steiner_, Sep 18 2018",
				"4/Pi = 1 + (1/4)*hypergeometric([1, 1/2, 1/2], [2, 2], 1) = hypergeometric([-1/2, -1/2], [1], 1). From the g.f. of Catalan^2 given in A001246. - _Wolfdieter Lang_, Sep 18 2018",
				"Equals Product_{k\u003e=1} (1 + 1/(4*k*(k+1))). - _Amiram Eldar_, Aug 05 2020"
			],
			"example": [
				"4/Pi = 1.2732395.... = 1/0.78539816..."
			],
			"mathematica": [
				"RealDigits[N[4/Pi, 6!]][[1]] (* _Vladimir Joseph Stephan Orlovsky_, Jun 18 2009 *)"
			],
			"program": [
				"(PARI) 4/Pi \\\\ _Charles R Greathouse IV_, Jun 21 2013"
			],
			"xref": [
				"Cf. A079097 for terms of a generalized continued fraction for 4/Pi. Inverse of A003881. A060294, A278145, A049541 (1/Pi)."
			],
			"keyword": "cons,nonn",
			"offset": "1,2",
			"author": "_Benoit Cloitre_, Nov 16 2003",
			"references": 38,
			"revision": 123,
			"time": "2021-10-31T01:41:55-04:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}