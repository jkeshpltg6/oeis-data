{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A049782",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 49782,
			"data": "0,0,1,2,4,4,6,2,1,4,1,10,10,6,4,10,13,10,9,14,13,12,21,10,14,10,10,6,17,4,2,26,1,30,34,10,5,28,10,34,4,34,16,34,19,44,18,10,48,14,13,10,13,10,34,34,28,46,28,34,22,2,55,26,49,34,65,30,67,34,68,10,55,42,64,66,34",
			"name": "a(n) = (0! + 1! + ... + (n-1)!) mod n.",
			"comment": [
				"Kurepa's conjecture is that gcd(!n,n!)=2, n\u003e1. It is easy to prove that this is equivalent to showing that gcd(p,!p)=1 for all odd primes p. In Guy, 2nd edition, it is stated that Mijajlovic has tested up to p=10^6. Subsequently Gallot tested up to 2^26. I have continued up to just above p=2^27, in fact to p\u003c144000000. There were no examples found where gcd(p,!p)\u003e1. - Paul Jobling, Dec 02 2004",
				"According to Kellner, the conjecture has been proved by Barsky and Benzaghou. - _T. D. Noe_, Dec 02 2004",
				"Barsky and Benzaghou withdrew their proof in 2011. I've extended the search up to 10^9, counterexample wasn't found. - _Milos Tatarevic_, Feb 01 2013"
			],
			"reference": [
				"R. K. Guy, Unsolved Problems in Number Theory, B44: is a(n)\u003e0 for n\u003e2?"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A049782/b049782.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Romeo Mestrovic, \u003ca href=\"https://doi.org/10.2298/FIL1510207M\"\u003eThe Kurepa-Vandermonde matrices arising from Kurepa's left factorial hypothesis\u003c/a\u003e, Filomat 29:10 (2015), 2207-2215. DOI:10.2298/FIL1510207M",
				"Vladica Andrejic, Milos Tatarevic, \u003ca href=\"http://arxiv.org/abs/1409.0800\"\u003eSearching for a counterexample of Kurepa's Conjecture\u003c/a\u003e, arXiv:1409.0800 [math.NT], 2014.",
				"D. Barsky and B. Benzaghou, \u003ca href=\"http://dx.doi.org/10.5802/jtnb.432\"\u003eNombres de Bell et somme de factorielles\u003c/a\u003e, Journal de Théorie des Nombres de Bordeaux, 16:1, No. 17, 2004.",
				"D. Barsky and B. Benzaghou, \u003ca href=\"http://dx.doi.org/10.5802/jtnb.775\"\u003eErratum à l'article \"Nombres de Bell et somme de factorielles\"\u003c/a\u003e, Journal de Théorie des Nombres de Bordeaux, 23:2 (2011), p. 527-527.",
				"Y. Gallot, \u003ca href=\"http://yves.gallot.pagesperso-orange.fr/papers/lfact.pdf\"\u003eMore information\u003c/a\u003e",
				"Bernd C. Kellner, \u003ca href=\"https://arxiv.org/abs/math/0410477\"\u003eSome remarks on Kurepa's left factorial\u003c/a\u003e, arXiv:math/0410477 [math.NT], 2004.",
				"Romeo Mestrovic, \u003ca href=\"http://arxiv.org/abs/1312.7037\"\u003eVariations of Kurepa's left factorial hypothesis\u003c/a\u003e, arXiv preprint arXiv:1312.7037 [math.NT], 2013-2014.",
				"Stephen A. Silver, \u003ca href=\"https://web.archive.org/web/20150315105524/http://www.argentum.freeserve.co.uk/maths/a049782.c\"\u003eC program to generate this sequence\u003c/a\u003e",
				"M. Tatarevic, \u003ca href=\"http://mtatar.wordpress.com/2011/07/30/kurepa/\"\u003eSearching for a counterexample to the Kurepa's left factorial hypothesis (p \u003c 10^9)\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A003422(n) mod n = !n mod n. - _G. C. Greubel_, Dec 11 2019"
			],
			"maple": [
				"a:= proc(n) local c, i, t; c, t:=1, 1;",
				"      for i to n-1 do t:= (t*i) mod n; c:= c+t od; c mod n",
				"    end:",
				"seq(a(n), n=1..100);  # _Alois P. Heinz_, Feb 16 2013"
			],
			"mathematica": [
				"Table[Mod[Sum[ i!, {i, 0, n-1}], n], {n, 80}]",
				"nn=80; With[{fcts=Accumulate[Range[0,nn]!]},Flatten[Table[Mod[Take[fcts,{n}], n], {n,nn}]]] (* _Harvey P. Dale_, Sep 22 2011 *)"
			],
			"program": [
				"(Haskell)",
				"a049782 :: Int -\u003e Integer",
				"a049782 n = (sum $ take n a000142_list) `mod` (fromIntegral n)",
				"-- _Reinhard Zumkeller_, Nov 02 2011",
				"(PARI) a(n)=my(s=1,f=1); for(k=1,n, f=f*k%n; s+=f); s%n \\\\ _Charles R Greathouse IV_, Feb 07 2017",
				"(MAGMA) [\u0026+[Factorial(k-1): k in [1..n]] mod (n): n in [1..80]]; // _Vincenzo Librandi_, May 31 2019",
				"(Sage) [mod(sum(factorial(k) for k in (0..n-1)), n) for n in (1..80)] # _G. C. Greubel_, Dec 11 2019",
				"(GAP) List([1..80], n-\u003e Sum([0..n-1], k-\u003e Factorial(k)) mod n ); # _G. C. Greubel_, Dec 11 2019"
			],
			"xref": [
				"Cf. A000142, A057245.",
				"Note that in the context of this sequence, !n is the left factorial A003422 not the subfactorial A000166."
			],
			"keyword": "nonn,easy,nice",
			"offset": "1,4",
			"author": "_Clark Kimberling_",
			"ext": [
				"More terms from _Erich Friedman_, who observes that the first 500 terms are nonzero. Independently extended by _Stephen A. Silver_."
			],
			"references": 10,
			"revision": 85,
			"time": "2020-08-21T09:13:40-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}