{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A279369",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 279369,
			"data": "1,12,18,58,13,74,57,19,5,72,174,178,182,429,217,138,8,225,247,272,162,825,81,83,85,849,89,999,255,1047,23,110,484,103,1122,288,1383,139,114,143,1407,32,149,1425,1518,408,711,176,1677,165,727,184,1701,188,450,906,910,914",
			"name": "A mapping of rationals a/b (lowest form) to prime rationals p/q such that a/b = (p+1)/(q+1), where n (the sequence index) selects the rationals a/b from the triangle array A226314(n)/A054531(n) and a(n) selects the prime rationals p/q from the same array.",
			"comment": [
				"Rationals a/b (lowest form) can be mapped 1-to-1 to a positive integer n where a/b is the n-th term of the triangular array A226314(n)/A054531(n). Consider two function of x, f_1 = ax-1 and f_2 = bx-1. Then by Schinzel's Hypothesis H there are infinite values of x such that f_1 and f_2 are simultaneously prime allowing a/b to be expressed using two primes p and q as a/b=(p+1)/(q+1).",
				"By choosing the least x for generating p=f_1 and q=f_2 (see A278635) it is possible to find a unique prime rational p/q that maps to rational a/b. If n is the sequence index that selects the rational a/b from the triangular array A226314(n)/A054531(n), then a(n) selects the prime rationals p/q from the same array."
			],
			"link": [
				"Lance Fortnow, \u003ca href=\"http://blog.computationalcomplexity.org/2004/03/counting-rationals-quickly.html\"\u003eCounting the Rationals Quickly\u003c/a\u003e, Computational Complexity Weblog, Monday, March 01, 2004.",
				"A. Schinzel, W. Sierpiński \u003ca href=\"http://matwbn.icm.edu.pl/ksiazki/aa/aa4/aa432.pdf\"\u003eSur certaines hypothèses concernant les nombres premiers\u003c/a\u003e, Acta Arithmetica IV., 1957.",
				"Wikipedia \u003ca href=\"http://en.wikipedia.org/wiki/Schinzel%27s_hypothesis_H\"\u003eSchinzel's hypothesis H\u003c/a\u003e."
			],
			"example": [
				"a(7)=57 because A226314(7)/A054531(7)=1/4 and with least x=3 we have p=f_1=x-1=2 and q=f_2=4x-1=11. Therefore (p+1)/(q+1)=3/12=1/4. Also A226314(57)/A054531(57)=p/q=2/11."
			],
			"mathematica": [
				"func[{i_, j_}] := {j(j-1)/2+i-\u003e{j+(i-j)/GCD[i, j], j/GCD[i, j]}}; rfunc[{i_, j_}] := {{j+(i-j)/GCD[i, j], j/GCD[i, j]}-\u003ej(j-1)/2+i}; getx[{a_, b_}] := Module[{f1, f2, x}, If[a==b, {1, 1}, (f1=a*x-1; f2=b*x-1; x=1; While[(!PrimeQ[f1]||!PrimeQ[f2])\u0026\u0026x\u003c10^5, x++]; If[x==10^5, Abort[], {f1, f2}])]]; assoc=Association@Flatten[Table[func[{a, b}], {b, 1000}, {a, b}], 1]; rassoc=Association@Flatten[Table[rfunc[{a, b}], {b, 1000}, {a, b}], 1]; Table[rassoc[getx[assoc[n]]], {n, 1, 100}]"
			],
			"xref": [
				"Cf. A054531, A226314, A278635."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Frank M Jackson_ and Michael B Rees, Dec 10 2016",
			"references": 0,
			"revision": 5,
			"time": "2016-12-12T09:20:02-05:00",
			"created": "2016-12-12T09:20:02-05:00"
		}
	]
}