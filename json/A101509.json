{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A101509",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 101509,
			"data": "1,3,7,16,35,75,159,334,696,1442,2976,6123,12562,25706,52492,107014,217877,443061,899957,1826078,3701783,7498261,15178255,30706320,62085915,125465715,253415981,511608490,1032427637,2082680887,4199956101,8467124805,17064784905,34382825363,69256687719,139465867773",
			"name": "Binomial transform of tau(n) (see A000005).",
			"comment": [
				"Row sums of A101508.",
				"Also: Number of matrices with positive integer coefficients such that the sum of all entries equals n+1, cf. link \"Partitions and A101509\". - _M. F. Hasler_, Jan 14 2009"
			],
			"link": [
				"M. F. Hasler, \u003ca href=\"/A101509/b101509.txt\"\u003eTable of n, a(n) for n = 0..500\u003c/a\u003e",
				"L. Manor, M. F. Hasler, \u003ca href=\"http://list.seqfan.eu/pipermail/seqfan/2009-January/000522.html\"\u003ePartitions and A101509. SeqFan list, Jan 14 2009\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/transforms.txt\"\u003eTransforms\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Sum_{k=0..n, Sum_{i=0..n, if(mod(i+1, k+1)=0, binomial(n, i), 0)}}.",
				"G.f.: 1/x * Sum_{n\u003e=1} z^n/(1-z^n) (Lambert series) where z=x/(1-x). - _Joerg Arndt_, Jan 30 2011",
				"a(n) ~ 2^n * (log(n/2) + 2*gamma), where gamma is the Euler-Mascheroni constant A001620. - _Vaclav Kotesovec_, Mar 07 2020"
			],
			"example": [
				"From _Gus Wiseman_, Jan 16 2019: (Start)",
				"The a(3) = 16 ways to arrange the parts of an integer partition of 4 into a matrix:",
				"  [4] [1 3] [3 1] [2 2] [1 1 2] [1 2 1] [2 1 1] [1 1 1 1]",
				".",
				"  [1] [3] [2] [1 1]",
				"  [3] [1] [2] [1 1]",
				".",
				"  [1] [1] [2]",
				"  [1] [2] [1]",
				"  [2] [1] [1]",
				".",
				"  [1]",
				"  [1]",
				"  [1]",
				"  [1]",
				"(End)"
			],
			"maple": [
				"bintr:= proc(p) proc(n) add(p(k) *binomial(n, k), k=0..n) end end:",
				"a:= bintr(n-\u003e numtheory[tau](n+1)):",
				"seq(a(n), n=0..40);  # _Alois P. Heinz_, Jan 30 2011"
			],
			"mathematica": [
				"a[n_] := Sum[DivisorSigma[0, k+1]*Binomial[n, k], {k, 0, n}]; Table[a[n], {n, 0, 40}] (* _Jean-François Alcover_, Feb 18 2017 *)"
			],
			"program": [
				"(PARI) A101509(n) = sum( k=0,n, numdiv(k+1)*binomial(n,k)) [_M. F. Hasler_, Jan 14 2009]"
			],
			"xref": [
				"Cf. A000005 (tau), A101508, A160399.",
				"Cf. A000219, A047966, A053529, A319066, A323300, A323301, A323307, A323429."
			],
			"keyword": "easy,nonn",
			"offset": "0,2",
			"author": "_Paul Barry_, Dec 05 2004",
			"references": 20,
			"revision": 41,
			"time": "2020-03-07T09:01:51-05:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}