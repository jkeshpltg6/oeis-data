{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007305",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7305,
			"id": "M0113",
			"data": "0,1,1,1,2,1,2,3,3,1,2,3,3,4,5,5,4,1,2,3,3,4,5,5,4,5,7,8,7,7,8,7,5,1,2,3,3,4,5,5,4,5,7,8,7,7,8,7,5,6,9,11,10,11,13,12,9,9,12,13,11,10,11,9,6,1,2,3,3,4,5,5,4,5,7,8,7,7,8,7,5,6,9,11,10,11,13,12,9,9,12,13,11",
			"name": "Numerators of Farey (or Stern-Brocot) tree fractions.",
			"comment": [
				"From _Reinhard Zumkeller_, Dec 22 2008: (Start)",
				"For n\u003e1: a(n+2) = if A025480(n-1) != 0 and A025480(n) != 0 then a(A025480(n-1)+2) + a(A025480(n)+2) else if A025480(n)=0 then a(A025480(n-1)+2)+1 else 0+a(A025480(n-1)+2);",
				"a(A054429(n)+2) = A047679(n) and a(n+2) = A047679(A054429(n));",
				"A153036(n+1) = floor(a(n+2)/A047679(n)). (End)",
				"From _Yosu Yurramendi_, Jun 25 2014: (Start)",
				"If the terms (n\u003e0) are written as an array (left-aligned fashion) with rows of length 2^m, m = 0,1,2,3,...",
				"1,",
				"1,2,",
				"1,2,3,3,",
				"1,2,3,3,4,5,5,4,",
				"1,2,3,3,4,5,5,4,5,7,8,7,7,8,7,5,",
				"1,2,3,3,4,5,5,4,5,7,8,7,7,8,7,5,6,9,11,10,11,13,12,9,9,12,13,11,10,11,9,6,",
				"then the sum of the m-th row is 3^m (m = 0,1,2,), each column k is constant, and the constants are from A007306, denominators of Farey (or Stern-Brocot) tree fractions (see formula).",
				"If the rows are written in a right-aligned fashion:",
				"                                                                          1,",
				"                                                                        1,2,",
				"                                                                   1, 2,3,3,",
				"                                                       1, 2, 3, 3, 4, 5,5,4,",
				"                                  1,2, 3, 3, 4, 5, 5,4,5, 7, 8, 7, 7, 8,7,5,",
				"  1,2,3,3,4,5,5,4,5,7,8,7,7,8,7,5,6,9,11,10,11,13,12,9,9,12,13,11,10,11,9,6,",
				"then each column is an arithmetic sequence. The differences of the arithmetic sequences also give the sequence A007306 (see formula). The first terms of columns are from A007305 itself (a(A004761(n+1)) = a(n), n\u003e0), and the second ones from A049448 (a(A004761(n+1)+2^A070941(n)) = A049448(n), n\u003e0). (End)",
				"If the sequence is considered in blocks of length 2^m, m = 0,1,2,..., the blocks are the reverse of the blocks of A047679: (a(2^m+1+k) = A047679(2^(m+1)-2-k), m = 0,1,2,..., k = 0,1,2,...,2^m-1). - _Yosu Yurramendi_, Jun 30 2014"
			],
			"reference": [
				"R. L. Graham, D. E. Knuth and O. Patashnik, Concrete Mathematics. Addison-Wesley, Reading, MA, 1990, p. 117.",
				"G. H. Hardy and E. M. Wright, An Introduction to the Theory of Numbers. 3rd ed., Oxford Univ. Press, 1954, p. 23.",
				"J. C. Lagarias, Number Theory and Dynamical Systems, pp. 35-72 of S. A. Burr, ed., The Unreasonable Effectiveness of Number Theory, Proc. Sympos. Appl. Math., 46 (1992). Amer. Math. Soc.",
				"W. J. LeVeque, Topics in Number Theory. Addison-Wesley, Reading, MA, 2 vols., 1956, Vol. 1, p. 154.",
				"I. Niven and H. S. Zuckerman, An Introduction to the Theory of Numbers. 2nd ed., Wiley, NY, 1966, p. 141.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A007305/b007305.txt\"\u003eTable of n, a(n) for n=0..4096\u003c/a\u003e",
				"A. Bogomolny, \u003ca href=\"http://www.cut-the-knot.org/blue/Stern.shtml\"\u003eStern-Brocot Tree\u003c/a\u003e",
				"A. Bogomolny, \u003ca href=\"http://www.cut-the-knot.org/blue/SB_props.shtml\"\u003eInspiration for Maple code\u003c/a\u003e",
				"A. Brocot, \u003ca href=\"http://gallica.bnf.fr/ark:/12148/bpt6k1661912\"\u003eCalcul des rouages par approximation, nouvelle méthode\u003c/a\u003e, Revue Chonométrique 3, 186-194, 1861.",
				"G. A. Jones, \u003ca href=\"http://www.mat.univie.ac.at/~slc/opapers/s18jones.html\"\u003eThe Farey graph\u003c/a\u003e, Séminaire Lotharingien de Combinatoire, B18e (1987), 2 pp.",
				"Shin-ichi Katayama, \u003ca href=\"http://www-math.ias.tokushima-u.ac.jp/journal/2013/Katayama47.pdf\"\u003eModified farey trees and pythagorean triples\u003c/a\u003e Journal of mathematics, the University of Tokushima, 47, 2013.",
				"G. Melancon, \u003ca href=\"https://doi.org/10.1016/S0012-365X(99)00123-5\"\u003eLyndon factorization of sturmian words\u003c/a\u003e, Discr. Math., 210 (2000), 137-149.",
				"Hugo Pfoertner, \u003ca href=\"https://oeis.org/plot2a?name1=A007305\u0026amp;name2=A007306\u0026amp;tform1=untransformed\u0026amp;tform2=untransformed\u0026amp;shift=0\u0026amp;radiop1=ratio\u0026amp;drawpoints=true\"\u003eRatio A007305(n)/A007306(n) vs n\u003c/a\u003e, using Plot 2.",
				"N. J. A. Sloane, \u003ca href=\"/stern_brocot.html\"\u003eStern-Brocot or Farey Tree\u003c/a\u003e",
				"Noam Zimhoni, \u003ca href=\"https://arxiv.org/abs/1904.11782\"\u003eA forest of Eisensteinian triplets\u003c/a\u003e, arXiv:1904.11782 [math.NT], 2019.",
				"\u003ca href=\"/index/Fo#fraction_trees\"\u003eIndex entries for fraction trees\u003c/a\u003e",
				"\u003ca href=\"/index/St#Stern\"\u003eIndex entries for sequences related to Stern's sequences\u003c/a\u003e"
			],
			"formula": [
				"a(n) = SternBrocotTreeNum(n-1) # n starting from 2 gives the sequence from 1, 1, 2, 1, 2, 3, 3, 1, 2, 3, 3, 4, 5, 5, 4, 1, ...",
				"From _Yosu Yurramendi_, Jun 25 2014: (Start)",
				"For m = 1,2,3,..., and k = 0,1,2,...,2^(m-1)-1, with a(1)=1:",
				"a(2^m+k) = a(2^(m-1)+k);",
				"a(2^m+2^(m-1)+k) = a(2^(m-1)+k) + a(2^m-k-1). (End)",
				"a((2^(m+2)-k) = A007306(2^(m+1)-k), m=0,1,2,..., k=0,1,2,...,2^m-1. - _Yosu Yurramendi_, Jul 04 2014",
				"a(2^(m+1)+2^m+k) - a(2^m+k) = A007306(2^m-k+1), m=1,2,..., k=1,2,...,2^(m-1). - _Yosu Yurramendi_, Jul 05 2014",
				"From _Yosu Yurramendi_, Jan 01 2015: (Start)",
				"a(2^m+2^q-1) = q+1, q = 0, 1, 2,..., m = q, q+1, q+2,...",
				"a(2^m+2^q)   = q+2, q = 0, 1, 2,..., m = q+1, q+2, q+3,... (End)",
				"a(2^m+k) = A007306(k+1), m \u003e= 0, 0 \u003c= k \u003c 2*m.  - _Yosu Yurramendi_, May 20 2019",
				"a(n) = A002487(A059893(n)), n \u003e 0. - _Yosu Yurramendi_, Sep 29 2021"
			],
			"example": [
				"A007305/A007306 = [ 0/1; 1/1; ] 1/2; 1/3, 2/3; 1/4, 2/5, 3/5, 3/4; 1/5, 2/7, 3/8, 3/7, 4/7, 5/8, 5/7, 4/5, ...",
				"Another version of Stern-Brocot is A007305/A047679 = 1, 2, 1/2, 3, 1/3, 3/2, 2/3, 4, 1/4, 4/3, 3/4, 5/2, 2/5, 5/3, 3/5, 5, 1/5, 5/4, 4/5, ..."
			],
			"maple": [
				"SternBrocotTreeNum := proc(n) option remember; local msb,r; if(n \u003c 2) then RETURN(n); fi; msb := floor_log_2(n); r := n - (2^msb); if(floor_log_2(r) = (msb-1)) then RETURN(SternBrocotTreeNum(r) + SternBrocotTreeNum(((3*(2^(msb-1)))-r)-1)); else RETURN(SternBrocotTreeNum((2^(msb-1))+r)); fi; end; # _Antti Karttunen_, Mar 19 2000 [Broken program - _N. J. A. Sloane_, Aug 05 2020]"
			],
			"mathematica": [
				"sbt[n_] := Module[{R,L,Y}, R={{1,0},{1,1}}; L={{1,1},{0,1}}; Y={{1,0},{0,1}}; w[b_] := Fold[ #1.If[ #2 == 0,L,R] \u0026,Y,b]; u[a_] := {a[[2,1]]+a[[2,2]],a[[1,1]]+a[[1,2]]}; Map[u,Map[w,Tuples[{0,1},n]]]]",
				"A007305(n) = Flatten[Append[{0,1},Table[Map[First,sbt[i]],{i,0,5}]]]",
				"A047679(n) = Flatten[Table[Map[Last,sbt[i]],{i,0,5}]]",
				"(* _Peter Luschny_, Apr 27 2009 *)"
			],
			"program": [
				"(R)",
				"a \u003c- 1",
				"for(m in 1:6) for(k in 0:(2^(m-1)-1)) {",
				"  a[2^m+        k] \u003c- a[2^(m-1)+k]",
				"  a[2^m+2^(m-1)+k] \u003c- a[2^(m-1)+k] + a[2^m-k-1]",
				"}",
				"a",
				"# _Yosu Yurramendi_, Jun 25 2014"
			],
			"xref": [
				"Cf. A007306, A006842, A006843, A047679, A054424, A057114, A152975."
			],
			"keyword": "nonn,frac,tabf,nice,look",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_",
			"references": 68,
			"revision": 95,
			"time": "2021-09-29T11:20:01-04:00",
			"created": "1994-04-28T03:00:00-04:00"
		}
	]
}