{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A276076",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 276076,
			"data": "1,2,3,6,9,18,5,10,15,30,45,90,25,50,75,150,225,450,125,250,375,750,1125,2250,7,14,21,42,63,126,35,70,105,210,315,630,175,350,525,1050,1575,3150,875,1750,2625,5250,7875,15750,49,98,147,294,441,882,245,490,735,1470,2205,4410,1225,2450,3675,7350,11025,22050,6125,12250,18375,36750,55125,110250,343",
			"name": "Prime-factorization representations of \"factorial base digit polynomials\": a(0) = 1, for n \u003e= 1, a(n) = A275733(n) * a(A276009(n)).",
			"comment": [
				"These are prime-factorization representations of single-variable polynomials where the coefficient of term x^(k-1) (encoded as the exponent of prime(k) in the factorization of n) is equal to the digit in one-based position k of the factorial base representation of n. See the examples."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A276076/b276076.txt\"\u003eTable of n, a(n) for n = 0..5040\u003c/a\u003e",
				"Indranil Ghosh, \u003ca href=\"/A276076/a276076_1.txt\"\u003ePython program for computing this sequence\u003c/a\u003e",
				"\u003ca href=\"/index/Fa#facbase\"\u003eIndex entries for sequences related to factorial base representation\u003c/a\u003e"
			],
			"formula": [
				"a(0) = 1, for n \u003e= 1, a(n) = A275733(n) * a(A276009(n)).",
				"Or: for n \u003e= 1, a(n) = a(A257687(n)) * A000040(A084558(n))^A099563(n).",
				"Other identities.",
				"For all n \u003e= 0:",
				"A276075(a(n)) = n.",
				"A001221(a(n)) = A060130(n).",
				"A001222(a(n)) = A034968(n).",
				"A051903(a(n)) = A246359(n).",
				"A048675(a(n)) = A276073(n).",
				"A248663(a(n)) = A276074(n).",
				"a(A007489(n)) = A002110(n).",
				"a(A059590(n)) = A019565(n).",
				"For all n \u003e= 1:",
				"a(A000142(n)) = A000040(n).",
				"a(A033312(n)) = A076954(n-1)."
			],
			"example": [
				"   n  A007623   polynomial     encoded as             a(n)",
				"   -------------------------------------------------------",
				"   0       0    0-polynomial   (empty product)        = 1",
				"   1       1    1*x^0          prime(1)^1             = 2",
				"   2      10    1*x^1          prime(2)^1             = 3",
				"   3      11    1*x^1 + 1*x^0  prime(2) * prime(1)    = 6",
				"   4      20    2*x^1          prime(2)^2             = 9",
				"   5      21    2*x^1 + 1*x^0  prime(2)^2 * prime(1)  = 18",
				"   6     100    1*x^2          prime(3)^1             = 5",
				"   7     101    1*x^2 + 1*x^0  prime(3) * prime(1)    = 10",
				"and:",
				"  23     321  3*x^2 + 2*x + 1  prime(3)^3 * prime(2)^2 * prime(1)",
				"                                      = 5^3 * 3^2 * 2 = 2250."
			],
			"program": [
				"(Scheme, two versions)",
				"(define (A276076 n) (if (zero? n) 1 (* (expt (A000040 (A084558 n)) (A099563 n)) (A276076 (A257687 n)))))",
				"(define (A276076 n) (if (zero? n) 1 (* (A275733 n) (A276076 (A276009 n)))))"
			],
			"xref": [
				"Cf. A000040, A007623, A084558, A099563, A257687, A276009.",
				"Cf. A276075 (a left inverse).",
				"Cf. A276078 (same terms in ascending order).",
				"Cf. also A000142, A001221, A001222, A002110, A007489, A019565, A033312, A034968, A048675, A051903, A059590, A060130, A076954, A246359, A248663, A276073, A276074.",
				"Cf. also A275733, A275734, A275735, A275725 for other such encodings of factorial base related polynomials."
			],
			"keyword": "nonn,base",
			"offset": "0,2",
			"author": "_Antti Karttunen_, Aug 18 2016",
			"references": 20,
			"revision": 26,
			"time": "2017-06-21T04:36:10-04:00",
			"created": "2016-08-21T11:24:40-04:00"
		}
	]
}