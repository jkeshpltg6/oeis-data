{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A058091",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 58091,
			"data": "1,5,-7,3,15,-32,9,58,-96,22,149,-253,68,372,-599,140,826,-1317,317,1768,-2735,632,3526,-5434,1259,6854,-10364,2346,12765,-19188,4345,23224,-34524,7693,41049,-60654,13487,71176,-104303,22962,120718,-176050,38622,201539",
			"name": "McKay-Thompson series of class 9B for the Monster group.",
			"comment": [
				"Cubic AGM theta functions: a(q) (see A004016), b(q) (A005928), c(q) (A005882)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A058091/b058091.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"D. Ford, J. McKay and S. P. Norton, \u003ca href=\"http://dx.doi.org/10.1080/00927879408825127\"\u003eMore on replicable functions\u003c/a\u003e, Commun. Algebra 22, No. 13, 5175-5193 (1994).",
				"J. McKay and A. Sebbar, \u003ca href=\"http://dx.doi.org/10.1007/s002080000116\"\u003eFuchsian groups, automorphic functions and Schwarzians\u003c/a\u003e, Math. Ann., 318 (2000), 255-275.",
				"\u003ca href=\"/index/Gre#groups\"\u003eIndex entries for sequences related to groups\u003c/a\u003e",
				"\u003ca href=\"/index/Mat#McKay_Thompson\"\u003eIndex entries for McKay-Thompson series for Monster simple group\u003c/a\u003e"
			],
			"formula": [
				"Expansion of 3 * q^(1/3) * a(q) / c(q) in powers of q where a(), c() are cubic AGM theta functions. - _Michael Somos_, Aug 09 2006",
				"Given g.f. A(x), then B(q) = A(q^3) / q satisfies 0 = f(B(q), B(q^2)) where f(u, v) = u^3 + v^3 - u^2*v^2 + 9*u*v - 54. - _Michael Somos_, Aug 09 2006",
				"Given g.f. A(x), then B(q) = A(q^3) / q satisfies 0 = f(B(q), B(q^2)) where f(u, v) = (u^3 + 1) * (v^3 + 1) - (u*v + 5) * (u^2*v^2 - 4*u*v + 11). - _Michael Somos_, Aug 20 2014",
				"G.f.: Sum_{k\u003e=0} a(k) * x^(3*k) = 3*x + (Product_{k\u003e0} (1 - x^k) / (1 - x^(9*k)))^3. - _Michael Somos_, Aug 09 2006",
				"a(n) = A131986(3*n - 1). - _Michael Somos_, Aug 20 2014"
			],
			"example": [
				"G.f. = 1 + 5*x - 7*x^2 + 3*x^3 + 15*x^4 - 32*x^5 + 9*x^6 + 58*x^7 - 96*x^8 + ...",
				"T9B = 1/q + 5*q^2 - 7*q^5 + 3*q^8 + 15*q^11 - 32*q^14 + 9*q^17 + 58*q^20 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ 1/q (QPochhammer[ q] / QPochhammer[ q^9])^3 + 3, {q, 0, 3 n - 1}]; (* _Michael Somos_, Aug 20 2014 *)"
			],
			"program": [
				"(PARI) {a(n) = local(A); if( n\u003c0, 0, n*=3; A = x * O(x^n); polcoeff( (eta(x + A) / eta(x^9 + A))^3, n))}; /* _Michael Somos_, Aug 09 2006 */",
				"(PARI) {a(n) = local(A); if( n\u003c0, 0, A = x * O(x^n); A = eta(x + A) / eta(x^2 + A); A = subst(A + x * O(x^(n\\3)), x, x^3)^3 / A; polcoeff( A + 4*x / A^2, n))}; /* _Michael Somos_, Aug 09 2006 */"
			],
			"xref": [
				"Cf. A007248, A000521, A007240, A014708, A007241, A007267, A045478, etc.",
				"Cf. A131986."
			],
			"keyword": "sign,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Nov 27 2000",
			"references": 12,
			"revision": 22,
			"time": "2018-01-25T02:55:18-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}