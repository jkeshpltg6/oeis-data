{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A248897",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 248897,
			"data": "1,2,0,9,1,9,9,5,7,6,1,5,6,1,4,5,2,3,3,7,2,9,3,8,5,5,0,5,0,9,4,7,7,0,4,8,8,1,8,9,3,7,7,4,9,8,7,2,8,4,9,3,7,1,7,0,4,6,5,8,9,9,5,6,9,2,5,4,1,5,4,5,4,0,8,4,2,3,5,9,2,2,4,5,6,0,8",
			"name": "Decimal expansion of Sum_{i \u003e= 0} (i!)^2/(2*i+1)!.",
			"comment": [
				"Value of the Borwein-Borwein function I_3(a,b) for a = b = 1. - _Stanislav Sykora_, Apr 16 2015",
				"The area of a circle circumscribing a unit-area regular hexagon. - _Amiram Eldar_, Nov 05 2020"
			],
			"reference": [
				"George Boros and Victor H. Moll, Irresistible integrals, Cambridge University Press (2006), pp. 120-121."
			],
			"link": [
				"Xavier Gourdon and Pascal Sebah, \u003ca href=\"http://numbers.computation.free.fr/Constants/Pi/piSeries.html\"\u003eCollection of series for Pi\u003c/a\u003e (see paragraph 7).",
				"Richard Kershner, \u003ca href=\"http://www.jstor.org/stable/2371320\"\u003eThe Number of Circles Covering a Set\u003c/a\u003e, American Journal of Mathematics, 61(3), 665-671.",
				"László Fejes Tóth, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9904-1948-08969-8\"\u003eAn Inequality concerning polyhedra\u003c/a\u003e, Bull. Amer. Math. Soc. 54 (1948), 139-146. See (9) p. 146.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Arithmetic-GeometricMean.html\"\u003eArithmetic-Geometric Mean\u003c/a\u003e, equations 26-32."
			],
			"formula": [
				"Equals 2*sqrt(3)*Pi/9 = 1 + 1/6 + 1/30 + 1/140 + 1/630 + 1/2772 + 1/12012 + ...",
				"Equals m*I_3(m,m) = m*Integral_{x\u003e=0} (x/(m^3+x^3)), for any m\u003e0. - _Stanislav Sykora_, Apr 16 2015",
				"Equals Integral_{x\u003e=0} (1/(1+x^3)) dx. - _Robert FERREOL_, Dec 23 2016",
				"From _Peter Bala_, Oct 27 2019: (Start)",
				"Equals 3/4*Sum_{n \u003e= 0} (n+1)!*(n+2)!/(2*n+3)!.",
				"Equals Sum_{n \u003e= 1} 3^(n-1)/(n*binomial(2*n,n)).",
				"Equals 2*Sum_{n \u003e= 1} 1/(n*binomial(2*n,n)). See Boros and Moll, pp. 120-121.",
				"Equals Integral_{x = 0..1} 1/(1 - x^3)^(1/3) dx = Sum_{n \u003e= 0} (-1)^n*binomial(-1/3,n) /(3*n + 1).",
				"Equals 2*Sum_{n \u003e= 1} 1/((3*n-1)*(3*n-2)) = 2*(1 - 1/2 + 1/4 - 1/5 + 1/7 - 1/8 + ...) (added Oct 30 2019). (End)",
				"Equals Product_{k\u003e=1} 9*k^2/(9*k^2 - 1). - _Amiram Eldar_, Aug 04 2020",
				"From _Peter Bala_, Dec 13 2021: (Start)",
				"Equals (2/3)*A093602.",
				"Conjecture: for k \u003e= 0, 2*sqrt(3)*Pi/9 = (3/2)^k * k!*Sum_{n = -oo..oo} (-1)^n/ Product_{j = 0..k} (3*n + 3*j + 1). (End)"
			],
			"example": [
				"1.2091995761561452337293855050947704881893774987284937170465899569254..."
			],
			"mathematica": [
				"RealDigits[2 Sqrt[3] Pi/9, 10, 100][[1]]"
			],
			"program": [
				"(PARI) a = 2*Pi/(3*sqrt(3)) \\\\ _Stanislav Sykora_, Apr 16 2015"
			],
			"xref": [
				"Cf. A000796, A002194, A093602, A248181, A257096, A257097, A186706.",
				"Cf. A091682 (Sum_{i \u003e= 0} (i!)^2/(2*i)!)."
			],
			"keyword": "nonn,cons,changed",
			"offset": "1,2",
			"author": "_Bruno Berselli_, Mar 06 2015",
			"references": 9,
			"revision": 70,
			"time": "2022-01-05T13:54:12-05:00",
			"created": "2015-03-06T18:22:54-05:00"
		}
	]
}