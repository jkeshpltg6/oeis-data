{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A098012",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 98012,
			"data": "2,3,6,5,15,30,7,35,105,210,11,77,385,1155,2310,13,143,1001,5005,15015,30030,17,221,2431,17017,85085,255255,510510,19,323,4199,46189,323323,1616615,4849845,9699690,23,437,7429,96577,1062347,7436429,37182145,111546435,223092870",
			"name": "Triangle read by rows in which the k-th term in row n (n \u003e= 1, k = 1..n) is Product_{i=0..k-1} prime(n-i).",
			"comment": [
				"Also, square array A(m,n) in which row m lists all products of m consecutive primes (read by falling antidiagonals). See also A248164. - _M. F. Hasler_, May 03 2017"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A098012/b098012.txt\"\u003eRows n = 1..125 of triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"n-th row = partial products of row n in A104887. - _Reinhard Zumkeller_, Oct 02 2014"
			],
			"example": [
				"2",
				"3 3*2",
				"5 5*3 5*3*2",
				"7 7*5 7*5*3 7*5*3*2",
				"Or, as an infinite square array:",
				"     2     3     5     7  ... : row 1 = A000040,",
				"     6    15    35    77  ... : row 2 = A006094,",
				"    30   105   385  1001  ... : row 3 = A046301,",
				"   210  1155  5005 17017  ... : row 4 = A046302,",
				"   ..., with col.1 = A002110, col.2 = A070826, col.3 = A059865\\{1}. - _M. F. Hasler_, May 03 2017"
			],
			"maple": [
				"T:=(n,k)-\u003emul(ithprime(n-i),i=0..k-1): seq(seq(T(n,k),k=1..n),n=1..9); # _Muniru A Asiru_, Mar 16 2019"
			],
			"mathematica": [
				"Flatten[ Table[ Product[ Prime[i], {i, n, j, -1}], {n, 9}, {j, n, 1, -1}]] (* _Robert G. Wilson v_, Sep 21 2004 *)"
			],
			"program": [
				"(Haskell)",
				"a098012 n k = a098012_tabl !! (n-1) !! (k-1)",
				"a098012_row n = a098012_tabl !! (n-1)",
				"a098012_tabl = map (scanl1 (*)) a104887_tabl",
				"-- _Reinhard Zumkeller_, Oct 02 2014",
				"(PARI) T098012(n,k)=prod(i=0,k-1,prime(n-i)) \\\\ \"Triangle\" variant",
				"A098012(m,n)=prod(i=0,m-1,prime(n+i)) \\\\ \"Square array\" variant. - _M. F. Hasler_, May 03 2017",
				"(GAP) P:=Filtered([1..200],IsPrime);;",
				"T:=Flat(List([1..9],n-\u003eList([1..n],k-\u003eProduct([0..k-1],i-\u003eP[n-i])))); # _Muniru A Asiru_, Mar 16 2019"
			],
			"xref": [
				"Cf. A000040, A002110, A006094, A046301, A046302, A046303.",
				"Cf. A060381 (central terms), A104887, A248147."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "1,1",
			"author": "_Alford Arnold_, Sep 09 2004",
			"ext": [
				"More terms from _Robert G. Wilson v_, Sep 21 2004"
			],
			"references": 4,
			"revision": 22,
			"time": "2019-03-16T12:09:46-04:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}