{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A268512",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 268512,
			"data": "1,2,1,12,9,2,60,54,20,3,840,840,400,105,12,2520,2700,1500,525,108,10,27720,31185,19250,8085,2268,385,30,360360,420420,280280,133770,45864,10780,1560,105,720720,864864,611520,321048,127008,36960,7488,945,56,12252240,15036840,11138400,6297480,2776032,942480",
			"name": "Triangle of coefficients c(n,i), 1\u003c=i\u003c=n, such that for each n\u003e=2, c(n,i) are setwise coprime; and for all primes p\u003e2n-1, the sum of (-1)^i*c(n,i)*binomial(i*p,p) is divisible by p^(2n-1).",
			"link": [
				"R. R. Aidagulov, M. A. Alekseyev. On p-adic approximation of sums of binomial coefficients. Journal of Mathematical Sciences 233:5 (2018), 626-634. doi:\u003ca href=\"http://doi.org/10.1007/s10958-018-3948-0\"\u003e10.1007/s10958-018-3948-0\u003c/a\u003e; also \u003ca href=\"http://arxiv.org/abs/1602.02632\"\u003earXiv\u003c/a\u003e, arXiv:1602.02632 [math.NT], 2016-2018."
			],
			"formula": [
				"c(n,i) = A003418(2*(n-1))*binomial(2*n-1,n-i)*(2*i-1)/i/binomial(2*n-1,n)."
			],
			"example": [
				"n=1: 1",
				"n=2: 2, 1",
				"n=3: 12, 9, 2",
				"n=4: 60, 54, 20, 3",
				"n=5: 840, 840, 400, 105, 12",
				"...",
				"For all primes p\u003e3, p^3 divides 2 - binomial(2*p,p) (cf. A087754).",
				"For all primes p\u003e5, p^5 divides 12 - 9*binomial(2*p,p) + 2*binomial(3*p,p) (cf. A268589).",
				"For all primes p\u003e7, p^7 divides 60 - 54*binomial(2*p,p) + 20*binomial(3*p,p) - 3*binomial(4*p,p) (cf. A268590)."
			],
			"mathematica": [
				"a3418[n_] := LCM @@ Range[n];",
				"c[1, 1] = 1; c[n_, i_] := a3418[2(n-1)] Binomial[2n-1, n-i] ((2i-1)/i/ Binomial[2n-1, n]);",
				"Table[c[n, i], {n, 1, 10}, {i, 1, n}] // Flatten (* _Jean-François Alcover_, Dec 04 2018 *)"
			],
			"program": [
				"(PARI) { A268512(n,i) = lcm(vector(2*(n-1),i,i)) * binomial(2*n-1,n-i) * (2*i-1) / i / binomial(2*n-1,n) }"
			],
			"xref": [
				"Cf. A099996 (first column), A068550 (diagonal), A087754, A268589, A268590, A254593."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Max Alekseyev_, Feb 06 2016",
			"references": 5,
			"revision": 24,
			"time": "2018-12-04T07:43:23-05:00",
			"created": "2016-02-07T17:36:42-05:00"
		}
	]
}