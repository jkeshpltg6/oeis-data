{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A277109",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 277109,
			"data": "1,1,1,1,1,1,3,1,3,1,3,1,1,1,3,1,3,7,15,30,1,1,3,7,15,26,26,1,1,1,3,1,3,1,3,1,1,1,3,1,3,7,15,1,1,1,3,1,3,1,3,1,1,1,3,1,3,7,15,1,1,3,7,15,26,1,3,7,15,31,63,26,30,26,26,26,26,30,46,26,26,26,26,1,1,1,3,7,15,1,3,1",
			"name": "Starting from 2^n+1, the length of the longest sequence of consecutive numbers which all take the same number of steps to reach 1 in the Collatz (or '3x+1') problem.",
			"comment": [
				"a(500) was found by Guo-Gang Gao (see links).",
				"Interestingly, this sequence has many sets of consecutive terms that are increasing powers of 2 minus 1. For example: a(291) to a(307), a(447) to a(467), and a(603) to a(625). It is not clear why this is the case.",
				"The largest value known in this sequence is a(1812) = 2^26-1 = 67108863.",
				"Conjecture: If a(n) = 2^k - 1 for some k \u003e 1, then a(n-1) = 2^(k-1) - 1. Conjecture holds for n \u003c= 1812.",
				"From _Hartmut F. W. Hoft_, Aug 16 2018: (Start)",
				"The conjecture is true. Let the lengths of the Collatz runs equal q for all numbers 2^n + 1, 2^n + 2, 2^n + 3, 2^n + 4, ..., 2^n + 2^k - 2, 2^n + 2^k - 1. Then dividing the 2^(k-1) - 1 even numbers by two gives rise to the sequence 2^(n-1) + 1, 2^(n-1) + 2, ..., 2^(n-1) + 2^(k-1) - 1 of numbers for which the lengths of the Collatz runs equals q-1. Furthermore, let the length of the Collatz run of 2^n + 2^k be r != q then the length of the Collatz run of 2^(n-1) + 2^(k-1) is r-1 != q-1, i.e., a(n-1) = 2^(k-1) - 1.",
				"Conjecture: Let a(k), ..., a(k+m), m \u003e= 0, be a subsequence of this sequence such that a(k)=a(k+m+1)=1 and a(k+i) \u003e 1, 1 \u003c= i \u003c= m. Then the lengths of the Collatz runs of a(k+i), 0 \u003c= i \u003c= m, increase by 1. In addition, there is an initial segment of increasing numbers a(k), ..., a(k+j), for some 0 \u003c= j \u003c= m, in each such subsequence having the form 2^i - 1, 0 \u003c i \u003c= j. (End)"
			],
			"link": [
				"Dmitry Kamenetsky, \u003ca href=\"/A277109/b277109.txt\"\u003eTable of n, a(n) for n = 0..1812\u003c/a\u003e",
				"Dmitry Kamenetsky, \u003ca href=\"/A277109/a277109_1.c.txt\"\u003eC program to compute the sequence\u003c/a\u003e",
				"Guo-Gang Gao, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(93)90240-T\"\u003eOn consecutive numbers of the same height in the Collatz problem\u003c/a\u003e, Discrete Mathematics, Volume 112, pages 261-267, 1993.",
				"Pureferret, \u003ca href=\"https://math.stackexchange.com/questions/470782/longest-known-sequence-of-identical-consecutive-collatz-sequence-lengths/\"\u003eLongest known sequence of identical consecutive Collatz sequence lengths\u003c/a\u003e, Mathematics StackExchange, 2013.",
				"Carlos Rivera, \u003ca href=\"http://primepuzzles.net/puzzles/puzz_847.htm\"\u003ePuzzle 847: Consecutive primes with the same Collatz length\u003c/a\u003e, The Prime Puzzles and Problems Connection.",
				"Carlos Rivera, \u003ca href=\"http://primepuzzles.net/puzzles/puzz_851.htm\"\u003ePuzzle 851: Puzzle 847 revisited\u003c/a\u003e, The Prime Puzzles and Problems Connection."
			],
			"example": [
				"a(6) = 3, because 2^6+1, 2^6+2 and 2^6+3 all take 27 steps to reach 1.",
				"From _Hartmut F. W. Hoft_, Aug 16 2018: (Start)",
				"Two examples for the conjecture (L(n) denotes the length of the Collatz run):",
				"n      a(n)    L(n)          n      a(n)    L(n)",
				"64     26      483           20      1       72",
				"------------------           ------------------",
				"65      1      559           21      1      166",
				"66      3      560           22      3      167",
				"67      7      561           23      7      168",
				"68     15      562           24     15      169",
				"69     31      563           ------------------",
				"70     63      564           25     26      170",
				"------------------           26     26      171",
				"71     26      565           ------------------",
				"72     30      566           27      1      247",
				"73     26      567",
				"74     26      568",
				"75     26      569",
				"76     26      570",
				"77     30      571",
				"78     46      572",
				"79     26      573",
				"80     26      574",
				"81     26      575",
				"82     26      576",
				"------------------",
				"83      1      626",
				"The \"power of 2 minus 1\" initial section of any such subsequence of a(n) is always increasing. However, there is no apparent ordering in the second section when that is present. (End)"
			],
			"mathematica": [
				"f[n_] := Length[NestWhileList[If[EvenQ@ #, #/2, 3 # + 1] \u0026, n, # != 1 \u0026]] - 1; Table[k = 1; While[f[2^n + k] == f[2^n + k + 1], k++]; k, {n, 120}] (* _Michael De Vlieger_, Oct 03 2016 *)"
			],
			"program": [
				"nbsteps(n)= s=n; c=0; while(s\u003e1, s=if(s%2, 3*s+1, s/2); c++); c;",
				"a(n) = {my(ns = 2^n+1); my(nbs = nbsteps(ns)); while(nbsteps(ns+1) == nbs, ns++); ns - 2^n;} \\\\ _Michel Marcus_, Oct 30 2016"
			],
			"xref": [
				"Cf. A006577, A078441, A179118, A277684."
			],
			"keyword": "nonn",
			"offset": "0,7",
			"author": "_Dmitry Kamenetsky_, Sep 30 2016",
			"references": 8,
			"revision": 65,
			"time": "2020-07-30T02:13:25-04:00",
			"created": "2016-09-30T13:06:25-04:00"
		}
	]
}