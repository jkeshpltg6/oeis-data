{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A276088",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 276088,
			"data": "0,1,1,1,2,1,1,1,1,1,2,1,2,1,1,1,2,1,3,1,1,1,2,1,4,1,1,1,2,1,1,1,1,1,2,1,1,1,1,1,2,1,2,1,1,1,2,1,3,1,1,1,2,1,4,1,1,1,2,1,2,1,1,1,2,1,1,1,1,1,2,1,2,1,1,1,2,1,3,1,1,1,2,1,4,1,1,1,2,1,3,1,1,1,2,1,1,1,1,1,2,1,2,1,1,1,2,1,3,1,1,1,2,1,4,1,1,1,2,1,4",
			"name": "The least significant nonzero digit in primorial base representation of n: a(n) = A276094(n) / A002110(A276084(n)) (with a(0) = 0).",
			"comment": [
				"For any n \u003e= 1, start from k = n and repeatedly try to divide as many successive primes as possible out of k, iterating as k/2 -\u003e k, k/3 -\u003e k, k/5 -\u003e k, until a nonzero remainder is encountered, which is then the value of a(n). (See the last example).",
				"Note that the sequence has been defined so that it will eventually include also \"digits\" (actually: value holders) \u003e 9 that occur as the least significant nonzero digits in primorial base representation. Thus any eventual decimal corruption of A049345 will not affect these values."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A276088/b276088.txt\"\u003eTable of n, a(n) for n = 0..2310\u003c/a\u003e",
				"\u003ca href=\"/index/Pri#primorialbase\"\u003eIndex entries for sequences related to primorial base\u003c/a\u003e"
			],
			"formula": [
				"a(0) = 0, and for n \u003e= 1, a(n) = A276094(n) / A002110(A276084(n)).",
				"From _Antti Karttunen_, Oct 29 2019: (Start)",
				"a(n) = A067029(A276086(n)).",
				"a(A276086(n)) = A328569(n).",
				"(End)."
			],
			"example": [
				"   n   A049345  the rightmost nonzero = a(n)",
				"---------------------------------------------------------",
				"   0       0             0",
				"   1       1             1",
				"   2      10             1",
				"   3      11             1",
				"   4      20             2",
				"   5      21             1",
				"   6     100             1",
				"   7     101             1",
				"   8     110             1",
				"   9     111             1",
				"  10     120             2",
				"  11     121             1",
				"  12     200             2",
				"  13     201             1",
				"  14     210             1",
				"  15     211             1",
				"  16     220             2",
				".",
				"For n=48 according to the iteration interpretation, we obtain first 48/2 = 24, and the remainder is zero, so we continue: 24/3 = 8 and here the remainder is zero as well, so we try next 8/5, but this gives the nonzero remainder 3, thus a(48)=3.",
				"For n=2100, which could be written \"A0000\" in primorial base (where A stands for digit \"ten\", as 2100 = 10*A002110(4)), the least significant nonzero value holder (also the most significant) is thus 10 and a(2100) = 10. (The first point where this sequence attains a value larger than 9)."
			],
			"mathematica": [
				"nn = 120; b = MixedRadix[Reverse@ Prime@ Range@ PrimePi[nn + 1]]; Table[Last[IntegerDigits[n, b] /. 0 -\u003e Nothing, 0], {n, 0, nn}] (* Version 11, or *)",
				"f[n_] := Block[{a = {{0, n}}}, Do[AppendTo[a, {First@ #, Last@ #} \u0026@ QuotientRemainder[a[[-1, -1]], Times @@ Prime@ Range[# - i]]], {i, 0, #}] \u0026@ NestWhile[# + 1 \u0026, 0, Times @@ Prime@ Range[# + 1] \u003c= n \u0026]; Rest[a][[All, 1]]]; {0}~Join~Table[Last@ DeleteCases[f@ n, d_ /; d == 0], {n, 120}] (* _Michael De Vlieger_, Aug 30 2016 *)"
			],
			"program": [
				"(PARI) A276088(n) = { my(e=0, p=2); while(n \u0026\u0026 !(e=(n%p)), n = n/p; p = nextprime(1+p)); (e); }; \\\\ _Antti Karttunen_, Oct 29 2019",
				"(Scheme, two versions)",
				"(define (A276088 n) (if (zero? n) n (let loop ((n n) (i 1)) (let* ((p (A000040 i)) (d (modulo n p))) (if (not (zero? d)) d (loop (/ (- n d) p) (+ 1 i)))))))",
				"(define (A276088 n) (if (zero? n) n (/ (A276094 n) (A002110 (A276084 n)))))",
				"(Python)",
				"from sympy import nextprime, primepi, primorial",
				"def a053669(n):",
				"    p = 2",
				"    while True:",
				"        if n%p!=0: return p",
				"        else: p=nextprime(p)",
				"def a257993(n): return primepi(a053669(n))",
				"def a002110(n): return 1 if n\u003c1 else primorial(n)",
				"def a276094(n): return 0 if n==0 else n%a002110(a257993(n))",
				"def a(n): return 0 if n==0 else a276094(n)//a002110(a257993(n) - 1)",
				"print([a(n) for n in range(101)]) # _Indranil Ghosh_, Jun 22 2017"
			],
			"xref": [
				"Cf. A000040, A002110, A049345, A067029, A276084, A276086, A276094, A328569."
			],
			"keyword": "nonn,base",
			"offset": "0,5",
			"author": "_Antti Karttunen_, Aug 22 2016",
			"references": 12,
			"revision": 28,
			"time": "2021-03-21T13:00:24-04:00",
			"created": "2016-08-23T12:40:16-04:00"
		}
	]
}