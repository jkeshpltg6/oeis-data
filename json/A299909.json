{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A299909",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 299909,
			"data": "1,6,12,18,24,24,30,42,48,48,54,66,66,66,78,90,90,90,102,108,108,114,126,132,132,138,144,150,156,162,168,174,180,180,186,198,204,204,210,222,222,222,234,246,246,246,258,264,264,270,282,288,288,294,300,306",
			"name": "Coordination sequence of node of type 3^6 in 3-uniform tiling #3.54 in the Galebach listing.",
			"comment": [
				"This tiling has three kinds of nodes. So far the other two types (A299910, A299911) have nor been analyzed."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A299909/b299909.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Brian Galebach, \u003ca href=\"http://probabilitysports.com/tilings.html?u=0\u0026amp;n=3\u0026amp;t=54\"\u003eTiling 3.54\u003c/a\u003e",
				"Brian Galebach, \u003ca href=\"/A299909/a299909.png\"\u003eTiling 3.54\u003c/a\u003e [Annotated figure showing the 3 kinds of points mentioned in A299909, A299910, A299911]",
				"Brian Galebach, \u003ca href=\"/A250120/a250120.html\"\u003ek-uniform tilings (k \u003c= 6) and their A-numbers\u003c/a\u003e",
				"Chaim Goodman-Strauss and N. J. A. Sloane, \u003ca href=\"https://doi.org/10.1107/S2053273318014481\"\u003eA Coloring Book Approach to Finding Coordination Sequences\u003c/a\u003e, Acta Cryst. A75 (2019), 121-134, also \u003ca href=\"http://NeilSloane.com/doc/Cairo_final.pdf\"\u003eon NJAS's home page\u003c/a\u003e. Also \u003ca href=\"http://arxiv.org/abs/1803.08530\"\u003earXiv:1803.08530\u003c/a\u003e.",
				"Printable Paper Web Site, \u003ca href=\"https://www.printablepaper.net/preview/3-3-3-3-3-3_3-3-4-3-4_Tessellation_Paper-Small\"\u003ePrintable 3.3.3.3.3.3,3.3.4.3.4 Tessellation Small\u003c/a\u003e [Shows this tiling]",
				"N. J. A. Sloane, \u003ca href=\"/A299909/a299909_1.png\"\u003eLabeling of nodes in one sector used to determine the coordination sequence\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_10\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,-1,1,0,0,0,1,-1,1,-1)."
			],
			"formula": [
				"G.f.: (x^10+5*x^9+7*x^8+11*x^7+12*x^6+6*x^5+12*x^4+11*x^3+7*x^2+5*x+1) / ((1-x)^2*(1+x^2)*(x^6+x^5+x^4+x^3+x^2+x+1)).",
				"The denominator can also be written as (1-x)*(1+x^2)*(1-x^7).",
				"Recurrence: (-n^2-5*n)*a(n)-n*a(n+1)+",
				"(-n^2-6*n)*a(n+2)-2*n*a(n+3)-2*n*a(n+4)-2*n*a(n+5)-",
				"2*n*a(n+6)+(n^2+3*n)*a(n+7)-n*a(n+8)+(n^2+4*n)*a(n+9) = 0,",
				"with a(0) = 1, a(1) = 6, a(2) = 12, a(3) = 18, a(4) = 24, a(5) = 24, a(6) = 30, a(7) = 42, a(8) = 48, a(9) = 48.",
				"a(n) = a(n-1) - a(n-2) + a(n-3) + a(n-7) - a(n-8) + a(n-9) - a(n-10) for n\u003e10. - _Colin Barker_, Mar 11 2018",
				"Details of the calculation of the generations function. (Start)",
				"The following lines are written in Maple notation, but should be intelligible as plain text. The colors refer to the labeling of one sector shown in the link.",
				"This analysis did not directly use the \"trunks and branches\" method described in the Goodman-Strauss \u0026 Sloane paper, but was influenced by it.",
				"# The generating function for one of the six sectors:",
				"G:=1+2*x+2*x^2+2*x^3; # green sausages",
				"QG:=G/((1-x^4)*(1-x^7)); # the lattice of green sausages",
				"R:=2+2*x+2*x^2+x^3; # red sausages",
				"QR:=R*(1/(1-x^3))*(x^4/(1-x^4)-x^7/(1-x^7)); # lattice of red sausages",
				"XA:=-x^2/(1-x); # correction for \"X-axis\"",
				"# red vertical lines of type a",
				"RVLa := x^2/((1-x)*(1-x^4))+x^5*(1/(1-x^3))*(1/(1-x^4)-1/(1-x^7));",
				"# red vertical lines of type b",
				"RVLb:= x^3/((1-x^4)*(1-x^7)) + x^7/((1-x^3)*(1-x^4)) - x^10/((1-x^3)*(1-x^7));",
				"# red vertical lines of type c (twigs to right of vertical sausages)",
				"RVLc:= x^4/((1-x^4)*(1-x^7)) + x^8/((1-x^3)*(1-x^4)) - x^11/((1-x^3)*(1-x^7));",
				"# Total for one sector",
				"T:=QG+QR+XA+RVLa+RVLb+RVLc;",
				"# Grand total, after correcting for overcounting where sectors meet:",
				"U:=6*T-5-6*x;",
				"series(U,x,30);",
				"# After simplification, grand total is:",
				"(x^10+5*x^9+7*x^8+11*x^7+12*x^6+6*x^5+12*x^4+11*x^3+7*x^2+5*x+1) / ((1-x)^2*(1+x^2)*(x^6+x^5+x^4+x^3+x^2+x+1));",
				"(End) (These details added by _N. J. A. Sloane_, Apr 10 2018)"
			],
			"mathematica": [
				"Join[{1}, LinearRecurrence[{1, -1, 1, 0, 0, 0, 1, -1, 1, -1}, {6, 12, 18, 24, 24, 30, 42, 48, 48, 54}, 60]] (* _Jean-François Alcover_, Jan 09 2019 *)"
			],
			"program": [
				"(PARI) Vec((x^10+5*x^9+7*x^8+11*x^7+12*x^6+6*x^5+12*x^4+11*x^3+7*x^2+5*x+1) / ((1-x)^2*(1+x^2)*(x^6+x^5+x^4+x^3+x^2+x+1)) + O(x^60)) \\\\ _Colin Barker_, Mar 11 2018"
			],
			"xref": [
				"See A299910, A299911 for the other two kinds of nodes."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Mar 07 2018",
			"references": 3,
			"revision": 39,
			"time": "2020-01-19T18:13:17-05:00",
			"created": "2018-03-07T18:10:07-05:00"
		}
	]
}