{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A324405",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 324405,
			"data": "3003,3315,5187,7395,8463,14763,19803,26733,31755,47523,50963,58035,62403,88023,105339,106113,123123,139971,152643,157899,166611,178923,183183,191919",
			"name": "Squarefree integers m \u003e 1 such that if prime p divides m, then s_p(m) \u003e= p and s_p(m) == 3 (mod p-1), where s_p(m) is the sum of the base p digits of m.",
			"comment": [
				"For d \u003e= 1 define S_d = (terms m in A324315 such that s_p(m) == d (mod p-1) if prime p divides m). Then S_1 is precisely the Carmichael numbers (A002997), S_2 is A324404, S_3 is A324405, and the union of all S_d for d \u003e= 1 is A324315.",
				"Subsequence of the 3-Knödel numbers (A033553). Generally, for d \u003e 1 the terms of S_d that are greater than d form a subsequence of the d-Knödel numbers.",
				"See Kellner and Sondow 2019."
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A324405/b324405.txt\"\u003eTable of n, a(n) for n = 1..2000\u003c/a\u003e",
				"Bernd C. Kellner and Jonathan Sondow, \u003ca href=\"https://doi.org/10.4169/amer.math.monthly.124.8.695\"\u003ePower-Sum Denominators\u003c/a\u003e, Amer. Math. Monthly, 124 (2017), 695-709; \u003ca href=\"https://arxiv.org/abs/1705.03857\"\u003earXiv version\u003c/a\u003e, arXiv:1705.03857 [math.NT], 2017.",
				"Bernd C. Kellner and Jonathan Sondow, \u003ca href=\"https://arxiv.org/abs/1902.10672\"\u003eOn Carmichael and polygonal numbers, Bernoulli polynomials, and sums of base-p digits\u003c/a\u003e, arXiv:1902.10672 [math.NT] 2019."
			],
			"example": [
				"3003 = 3*7*11*13 is squarefree and equals 11010020_3, 11520_7, 2290_11, and 14a0_13 in base p = 3, 7, 11, and 13. Then s_3(3003) = 1+1+1+2 = 5 \u003e= 3, s_7(3003) = 1+1+5+2 = 9 \u003e= 7, s_11(3003) = 2+2+9 = 13 \u003e= 11, and s_13(3003) = 1+4+a = 1+4+10 = 15 \u003e= 13. Also, s_3(3003) = 5 == 3 (mod 2), s_7(3003) = 9 == 3 (mod 6), s_11(3003) = 13 == 3 (mod 10), and s_13(3003) = 15 == 3 (mod 12), so 3003 is a member."
			],
			"mathematica": [
				"SD[n_, p_] := If[n \u003c 1 || p \u003c 2, 0, Plus @@ IntegerDigits[n, p]];",
				"LP[n_] := Transpose[FactorInteger[n]][[1]];",
				"TestSd[n_, d_] := (n \u003e 1) \u0026\u0026 (d \u003e 0) \u0026\u0026 SquareFreeQ[n] \u0026\u0026 VectorQ[LP[n], SD[n, #] \u003e= # \u0026\u0026 Mod[SD[n, #] - d, # - 1] == 0 \u0026];",
				"Select[Range[200000], TestSd[#, 3] \u0026]"
			],
			"xref": [
				"Cf. A002997, A033553, A324315, A324316, A324317, A324318, A324319, A324320, A324369, A324370, A324371, A324404."
			],
			"keyword": "nonn,base",
			"offset": "1,1",
			"author": "_Bernd C. Kellner_ and _Jonathan Sondow_, Feb 26 2019",
			"references": 11,
			"revision": 19,
			"time": "2020-12-14T05:30:29-05:00",
			"created": "2019-03-01T04:40:40-05:00"
		}
	]
}