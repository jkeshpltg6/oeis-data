{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A077947",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 77947,
			"data": "1,1,2,5,9,18,37,73,146,293,585,1170,2341,4681,9362,18725,37449,74898,149797,299593,599186,1198373,2396745,4793490,9586981,19173961,38347922,76695845,153391689,306783378,613566757,1227133513,2454267026,4908534053,9817068105",
			"name": "Expansion of 1/(1 - x - x^2 - 2*x^3).",
			"comment": [
				"Number of sequences of codewords of total length n from the code C={0,10,110,111}. E.g., a(3)=5 corresponds to the sequences 000, 010, 100, 110 and 111. - _Paul Barry_, Jan 23 2004",
				"In other words: number of compositions of n into 1 kind of 1's and 2's and two kinds of 3's. - _Joerg Arndt_, Jun 25 2011",
				"Diagonal sums of number Pascal-(1,2,1) triangle A081577. - _Paul Barry_, Jan 24 2005",
				"For n\u003e0: a(n) = A173593(2*n+1) - A173593(2*n); a(n+1) = A173593(2*n) - A173593(2*n-1). - _Reinhard Zumkeller_, Feb 22 2010",
				"Sums of 3 successive terms are powers of 2. - _Mark Dols_, Aug 20 2010",
				"For n \u003e 2, a(n) is the number of quaternary sequences of length n (i) starting with q(0)=0; (ii) ending with q(n-1)=0 or 3 and (iii) in which all triples (q(i), q(i+1), q(i+2)) contain digits 0 and 3; cf. A294627. - _Wojciech Florek_, Jul 30 2018"
			],
			"reference": [
				"S. Roman, Introduction to Coding and Information Theory, Springer-Verlag, 1996, p. 42"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A077947/b077947.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"G. Cerda-Morales, \u003ca href=\"https://arxiv.org/abs/1905.00725\"\u003eA note on modified third-order Jacobsthal Numbers\u003c/a\u003e, arxiv:1905.00725 [math.CO], 2019.",
				"M. H. Cilasun, \u003ca href=\"http://arxiv.org/abs/1412.3265\"\u003eAn Analytical Approach to Exponent-Restricted Multiple Counting Sequences\u003c/a\u003e, arXiv preprint arXiv:1412.3265 [math.NT], 2014.",
				"M. H. Cilasun, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL19/Cilasun/cila5.html\"\u003eGeneralized Multiple Counting Jacobsthal Sequences of Fermat Pseudoprimes\u003c/a\u003e, Journal of Integer Sequences, Vol. 19, 2016, #16.2.3.",
				"Charles K. Cook and Michael R. Bacon, \u003ca href=\"http://ami.ektf.hu/uploads/papers/finalpdf/AMI_41_from27to39.pdf\"\u003eSome identities for Jacobsthal and Jacobsthal-Lucas numbers satisfying higher order recurrence relations\u003c/a\u003e, Annales Mathematicae et Informaticae, 41 (2013) pp. 27-39.",
				"W. Florek, \u003ca href=\"http://doi.org/10.1016/j.amc.2018.06.014\"\u003eA class of generalized Tribonacci sequences applied to counting problems\u003c/a\u003e, Appl. Math. Comput., 338 (2018), 809-821.",
				"S. Kitaev, J. Remmel and M. Tiefenbruck, \u003ca href=\"http://arxiv.org/abs/1201.6243\"\u003eMarked mesh patterns in 132-avoiding permutations I\u003c/a\u003e, arXiv preprint arXiv:1201.6243 [math.CO], 2012. - From _N. J. A. Sloane_, May 09 2012",
				"Sergey Kitaev, Jeffrey Remmel and Mark Tiefenbruck, \u003ca href=\"http://www.emis.de/journals/INTEGERS/papers/p16/p16.Abstract.html\"\u003eQuadrant Marked Mesh Patterns in 132-Avoiding Permutations II\u003c/a\u003e, Electronic Journal of Combinatorial Number Theory, Volume 15 #A16; see also \u003ca href=\"http://arxiv.org/abs/1302.2274\"\u003earXiv preprint\u003c/a\u003e, arXiv:1302.2274 [math.CO], 2013.",
				"Vladimir Victorovich Kruchinin, \u003ca href=\"http://arxiv.org/abs/1009.2565\"\u003eComposition of ordinary generating functions\u003c/a\u003e, arXiv:1009.2565, arXiv:1009.2565 [math.CO], 2010.",
				"Evren Eyican Polatlı and Yüksel Soykan, \u003ca href=\"https://doi.org/10.9734/ARJOM/2021/v17i230270\"\u003eOn generalized third-order Jacobsthal numbers\u003c/a\u003e, Asian Res. J. of Math. (2021) Vol. 17, No. 2, 1-19, Article No. ARJOM.66022.",
				"Yüksel Soykan, \u003ca href=\"https://arxiv.org/abs/1910.03490\"\u003eSumming Formulas For Generalized Tribonacci Numbers\u003c/a\u003e, arXiv:1910.03490 [math.GM], 2019.",
				"Anthony Zaleski and Doron Zeilberger, \u003ca href=\"https://arxiv.org/abs/1712.10072\"\u003eOn the Intriguing Problem of Counting (n+1,n+2)-Core Partitions into Odd Parts\u003c/a\u003e, arXiv:1712.10072 [math.CO], 2017.",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,1,2)."
			],
			"formula": [
				"G.f.: 1/((1-2*x)*(1+x+x^2)).",
				"a(n) = a(n-1)+a(n-2)+2*a(n-3). - _Paul Curtz_, May 23 2008",
				"a(n) = round(2^(n+2)/7). - _Mircea Merca_, Dec 28 2010",
				"a(n) = 4*2^n/7 + 3*cos(2*Pi*n/3)/7 + sqrt(3)*sin(2*Pi*n/3)/21. - _Paul Barry_, Jan 23 2004",
				"Convolution of A000079 and A049347. a(n) = Sum_{k=0..n} 2^k*2*sqrt(3)*cos(2*Pi(n-k)/3+Pi/6)/3. - _Paul Barry_, May 19 2004",
				"a(n) = sum(sum(binomial(k,j)*binomial(j,n-3*k+2*j)*2^(k-j),j,0,k),k,1,n), n\u003e0. - _Vladimir Kruchinin_, Sep 07 2010",
				"Partial sums of A078010 starting (1, 0, 1, 3, 4, 9, ...). - _Gary W. Adamson_, May 13 2013",
				"a(n) = 1/14*(2^(n + 3) + (-1)^n*((-1)^floor(n/3) + 4*(-1)^floor((n + 1)/3) + 2*(-1)^floor((n + 2)/3) + (-1)^floor((n + 4)/3))). - _John M. Campbell_, Dec 23 2016",
				"a(n) = 1/63*(9*2^(2 + n) + (-1)^n*(2 + 9*floor(n/6) - 32*floor((n + 5)/6) + 24*floor((n + 7)/6) + 20*floor((n + 8)/6) - 10*floor((n + 9)/6) - 27*floor((n + 10)/6) + 14*floor((n + 11)/6) + 3*floor((n + 13)/6) - 2*floor((n + 14)/6) + floor((n + 15)/6))). - _John M. Campbell_, Dec 23 2016",
				"7*a(n) = 2^(n+2) + A167373(n+1). - _R. J. Mathar_, Feb 06 2020"
			],
			"example": [
				"It is shown in A294627 that there are 42 quaternary sequences (i.e. build from four digits 0, 1, 2, 3) and having both 0 and 3 in every (consecutive) triple. Only a(4) = 9 of them start with 0 and end with 0 or 3: 0030, 0033, 0130, 0230, 0300, 0303, 0310, 0320, 0330. - _Wojciech Florek_, Jul 30 2018"
			],
			"maple": [
				"seq(round(2^(n+2)/7),n=0..25); # _Mircea Merca_, Dec 28 2010"
			],
			"mathematica": [
				"CoefficientList[Series[1/(1 - x - x^2 - 2*x^3), {x, 0, 100}], x] (* or *) LinearRecurrence[{1, 1, 2}, {1, 1, 2}, 70] (* _Vladimir Joseph Stephan Orlovsky_, Jun 28 2011 *)"
			],
			"program": [
				"(Maxima) a(n):=sum(sum(binomial(k,j)*binomial(j,n-3*k+2*j)*2^(k-j),j,0,k),k,1,n); /* _Vladimir Kruchinin_, Sep 07 2010 */",
				"(MAGMA) [Round(2^(n+2)/7): n in [0..40]]; // _Vincenzo Librandi_, Jun 25 2011",
				"(PARI) Vec(1/(1-x-x^2-2*x^3) + O(x^100)) \\\\ _Altug Alkan_, Oct 31 2015"
			],
			"xref": [
				"Apart from signs, same as A077972.",
				"Cf. A139217 and A139218.",
				"Cf. A078010.",
				"Cf. A294627."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_, Nov 17 2002",
			"ext": [
				"Deleted certain dangerous or potentially dangerous links. - _N. J. A. Sloane_, Jan 30 2021"
			],
			"references": 20,
			"revision": 132,
			"time": "2021-10-29T09:03:53-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}