{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A212822",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 212822,
			"data": "1,2,-1,1,3,-1,2,6,-8,3,2,10,10,-10,3,4,20,10,-50,46,-15,17,119,245,35,-217,161,-45,34,238,406,-350,-644,1372,-1056,315,62,558,1722,1638,-1092,-1008,1828,-1188,315,124,1116,3138,1134,-5838,1134,9452,-14724,10134,-2835",
			"name": "Triangle of coefficients of polynomials concerning Newman-like phenomenon of multiples of b+1 in even base b in interval [0,b^n) (see comment).",
			"comment": [
				"In 1969, D. J. Newman (see the reference) proved that difference between numbers of multiples of 3 with even and odd binary digit sums in interval [0,x] is always positive. This fact now is known as Newman phenomenon.",
				"Consider difference between numbers of multiples of b+1 with even and odd digit sums in even base b in interval [0, b^n). It is a polynomial in b P_n(b) of degree n-1 and multiple of b, if n is even, and n-2, if n is odd, such that all polynomials Q_n(b):=A156769(n/2)*P_n(b)/b, if n is even, and Q_n(b):=A156769((n-1)/2)*P_n(b), if n is odd, presumably have integer coefficients and are of degree n-2. The sequence is triangle of coefficients of polynomials Q_n(b).",
				"The r-th row contains r-1 entries.",
				"Since, evidently, P_n(1)=1, then the row sums form sequence A156769 repeated."
			],
			"link": [
				"D. J. Newman, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9939-1969-0244149-8\"\u003eOn the number of binary digits in a multiple of three\u003c/a\u003e, Proc. Amer. Math. Soc. 21 (1969) 719-721.",
				"Vladimir Shevelev, \u003ca href=\"http://arxiv.org/abs/0710.3177\"\u003eOn monotonic strengthening of Newman-like phenomenon on (2m+1)-multiples in base 2m\u003c/a\u003e, arXiv:0710.3177v2 [math.NT], 2007."
			],
			"formula": [
				"If n\u003e=2 is even, then P_(n+1)(b) = (-1)^((n-2)/2)*(C(b+1,n)-C(b-1,n))-sum{i=2,4,...,n-2}(-1)^((n+i)/2)*C(b+1, n-i)*P_(i+1)(b), where P_n(b)=b*Q_n(b)/A156769(n/2);",
				"if n\u003e=3 is odd, then P_(n+1)(b) = (-1)^((n-1)/2)*(C(b,n)-b*C(b+1,n-1))+sum{i=3,5,...,n-2}(-1)^((n+i)/2)*C(b+1, n-i)*P_(i+1)(b), where",
				"P_n(b) = Q_n(b)/A156769((n-1)/2).",
				"P_n(b) = 2/(b+1)*Sum_{j=1..b/2}(tan(j*Pi/(b+1)))^n, if n is even, and",
				"P_n(b) = 2/(b+1)*Sum_{j=1..b/2}(tan(j*Pi/(b+1)))^n*sin(j*Pi/(b+1)), if n is odd."
			],
			"example": [
				"Triangle begins (r is the number of row or the number of polynomial; coefficients of b^k, k=r-2-i, i=0,1,..., r-2)",
				"r/i.|..0......1......2.....3.....4......5......6.....7",
				"======================================================",
				".2..|..1",
				".3..|..2.....-1",
				".4..|..1......3.....-1",
				".5..|..2......6.....-8.....3",
				".6..|..2.....10.....10...-10.....3",
				".7..|..4.....20.....10...-50....46....-15",
				".8..|.17....119....245....35..-217....161....-45",
				".9..|.34....238....406..-350..-644...1372..-1056....315",
				"For example, if r=4, the polynomial",
				"P_4(b)=b*(b^2+3*b-1)/A156769(4/2)=b/3*(b^2+3*b-1) (b==0 mod 2)",
				"gives difference between multiples of b+1 with even and odd digit sums in  base b in interval [0, b^4). Note also that P_2(b)=b. Therefore, setting in the formula n=r=3, again for P_4(b) we have P_4(b)=b*C(b+1,2)-C(b,3)=b/3*(b^2+3*b-1)."
			],
			"mathematica": [
				"A156769[n_] := Denominator[(2^(2*n-2)/Factorial[2*n-1])]; poly[1, b_] := 1; poly[2, b_] := b; poly[n_, b_] :=  poly[n, b] = If[OddQ[n], (-1)^((n - 1)/2) (FunctionExpand[Binomial[b - 1, n - 1]] - Sum[(-1)^(k/2) FunctionExpand[Binomial[b + 1, n - k - 1]] poly[k + 1, b], {k, 0, n - 2, 2}]), (-1)^((n - 2)/2) (FunctionExpand[Binomial[b, n - 1]] - Sum[(-1)^((k - 1)/2) FunctionExpand[Binomial[b + 1, n - k - 1]] poly[k + 1, b], {k, 1, n - 2, 2}])]; Table[If[EvenQ[z], Most[Reverse[CoefficientList[poly[z, b] A156769[z/2], b]]], Reverse[CoefficientList[poly[z, b] A156769[(z - 1)/2], b]]], {z, 2, 12}]"
			],
			"xref": [
				"Cf. A156769, A038754, A084990, A091042, A212500, A212592, A212593, A212594, A212668, A212669, A212670, A212705, A212706."
			],
			"keyword": "sign,base,tabl",
			"offset": "2,2",
			"author": "_Vladimir Shevelev_ and _Peter J. C. Moses_, May 28 2012",
			"references": 0,
			"revision": 35,
			"time": "2016-11-09T02:35:26-05:00",
			"created": "2012-06-13T17:25:59-04:00"
		}
	]
}