{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A300360",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 300360,
			"data": "0,1,2,1,1,1,2,1,2,1,2,3,1,1,3,1,3,4,2,2,4,1,2,1,1,2,4,3,1,1,2,1,6,2,2,2,5,1,4,1,2,6,3,3,3,1,2,3,4,3,3,2,4,2,2,1,7,3,1,4,1,2,8,1,3,7,3,4,6,3,4,4,6,4,3,2,4,3,1,2",
			"name": "Number of ways to write n^2 as x^2 + y^2 + z^2 + w^2 with x,y,z,w nonnegative integers and z \u003c= w such that x or y is a power of 2 (including 1) and x + 63*y = 2^(2k+1) for some k = 0,1,2,....",
			"comment": [
				"Conjecture: a(n) \u003e 0 for all n \u003e 1, and a(n) = 1 only for n = 5, 13, 25, 29, 59, 61, 79, 91, 95, 101, 103, 1315, 2^k (k = 1,2,3,...), 2^(2k+1)*m (k = 0,1,2,... and m = 3, 5, 7, 11, 15, 19, 23).",
				"Note the difference between the current sequence and A300356.",
				"In the comments of A300219, the author conjectured that a positive square n^2 can be written as x^2 + y^2 + z^2 + w^2 with x,y,z,w nonnegative integers such that both x and x + 3*y are powers of 4 unless n has the form 4^k*81503 with k a nonnegative integer. Since 81503^2 = 208^2 + 16^2 + 51167^2 + 63440^2 with 16 = 4^2 and 208 + 3*16 = 4^4, this implies that any positive square can be written as x^2 + y^2 + z^2 + w^2 with x,y,z,w nonnegative integers such that x or y is a power of 4 and x + 3y is also a power of 4. We also conjecture that for any positive integer n not of the form 4^k*m (k =0,1,... and m = 2, 7) we can write n^2 as x^2 + y^2 + z^2 + w^2 with x,y,z,w nonnegative integers such that x or y is a power of 4 and x + 2*y is also a power of 4."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A300360/b300360.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2016.11.008\"\u003eRefining Lagrange's four-square theorem\u003c/a\u003e, J. Number Theory 175(2017), 167-190.",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1701.05868\"\u003eRestricted sums of four squares\u003c/a\u003e, arXiv:1701.05868 [math.NT], 2017-2018."
			],
			"example": [
				"a(38) = 1 since 38^2 = 2^2 + 0^2 + 12^2 + 36^2 with 2 = 2^1 and 2 + 63*0 = 2^1.",
				"a(86) = 2 since 86 = 65^2 + 1^2 + 19^2 + 53^2 = 65^2 + 1^2 + 31^2 + 47^2 with 1 = 2^0 and 65 + 63*1 = 2^7.",
				"a(535) = 3 since 535^2 = 2^2 + 130^2 + 64^2 + 515^2 = 2^2 + 130^2 + 139^2 + 500^2 = 8^2 + 520^2 + 40^2 + 119^2 with 2 = 2^1, 8 = 2^3, 2 + 63*130 = 2^13 and 8 + 63*520 = 2^15.",
				"a(1315) = 1 since 1315^2 = 512^2 + 512^2 + 61^2 + 1096^2 with 512 = 2^9 and 512 + 63*512 = 2^15."
			],
			"mathematica": [
				"SQ[n_]:=SQ[n]=IntegerQ[Sqrt[n]];",
				"Pow[a_,n_]:=Pow[a,n]=IntegerQ[Log[a,n]];",
				"tab={};Do[r=0;Do[If[SQ[n^2-x^2-y^2-z^2]\u0026\u0026(Pow[2,x]||Pow[2,y])\u0026\u0026Pow[4,(x+63y)/2],r=r+1],{x,0,n},{y,0,Sqrt[n^2-x^2]},{z,0,Sqrt[(n^2-x^2-y^2)/2]}];tab=Append[tab,r],{n,1,80}];Print[tab]"
			],
			"xref": [
				"Cf. A000079, A000118, A000290, A271518, A279612, A281976, A299924, A300219, A300356, A300362."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Zhi-Wei Sun_, Mar 03 2018",
			"references": 6,
			"revision": 16,
			"time": "2018-03-04T10:32:53-05:00",
			"created": "2018-03-03T22:50:50-05:00"
		}
	]
}