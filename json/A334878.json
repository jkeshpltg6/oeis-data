{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A334878",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 334878,
			"data": "0,1,2,2,4,3,8,3,6,5,16,5,32,9,6,4,64,7,128,11,10,17,256,7,18,33,12,29,512,7,1024,5,18,65,12,8,2048,129,34,19,4096,11,8192,83,15,257,16384,9,54,19,66,245,32768,13,20,67,130,513,65536,14,131072,1025",
			"name": "For any n \u003e 0 with prime factorization Product_{k \u003e 0} prime(k)^e_k (where prime(k) denotes the k-th prime number), let b_k = 1 + max_{k \u003e 0} e_k; a(n) = Sum_{k \u003e 0} e_k * b_k^(k-1).",
			"comment": [
				"In other words, a(n) encodes the prime factorization of n in base 1 + A051903(n).",
				"Every nonnegative integer appears finitely many times in this sequence."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A334878/b334878.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"a(2^e) = e for any e \u003e= 0.",
				"a(prime(k)) = 2^(k-1) for any k \u003e 0.",
				"a(prime(k)^e) = e*(e+1)^(k-1) for any k \u003e 0 and e \u003e= 0.",
				"a(n) = A087207(n) for any squarefree number n."
			],
			"example": [
				"For n = 84:",
				"- 84 = 7 * 3 * 2^2 = prime(4) * prime(2) * prime(1)^2,",
				"- b_84 = 1 + 2 = 3,",
				"- so a(84) = 1*3^(4-1) + 1*3^(2-1) + 2*3^(1-1) = 32."
			],
			"program": [
				"(PARI) a(n) = { if (n==1, 0, my (f=factor(n), b=1+vecmax(f[,2]~)); sum(k=1, #f~, f[k,2]*b^(primepi(f[k,1])-1))) }"
			],
			"xref": [
				"Cf. A051903, A087207."
			],
			"keyword": "nonn,base",
			"offset": "1,3",
			"author": "_Rémy Sigrist_, May 14 2020",
			"references": 1,
			"revision": 11,
			"time": "2020-05-17T15:39:59-04:00",
			"created": "2020-05-16T14:27:45-04:00"
		}
	]
}