{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A295423",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 295423,
			"data": "1,3,7,12,15,3,28,8,31,39,42,60,5,56,72,63,12,91,90,96,32,124,7,15,120,120,168,16,127,144,36,195,18,186,9,224,180,234,20,252,21,20,217,210,280,48,248,22,360,11,312,255,54,336,104,336,24,403,372,26,54,392,378,363,26,480,13,66",
			"name": "a(n) = size of the part containing the diagonal of the symmetric representation of sigma(k) when k is the n-th number with an odd number of parts.",
			"comment": [
				"a(n) = middle element of row A071562(n) in A237270.",
				"Every odd prime number p occurs in the sequence since the symmetric representation of p^2 has the three parts ( (p^2 + 1)/2 , p, (p^2 + 1)/2 ).",
				"Let n = 2^m * q, m\u003e=0 and q odd, and let r = floor( (sqrt(8n+1) - 1)/2 ). The symmetric representation of sigma(n) has an odd number of parts precisely when there exists an odd divisor d of q such that d \u003c= r and d*2^(m+1) \u003e r (see the link in A071562).",
				"Conjecture: Every even number, except 2, 4, 6, 10, and 14, occurs as the size of the middle part of the symmetric representation with an odd number of parts.",
				"By symmetry, an even middle part must have an even width at the diagonal constraining the geometry for small areas bounded by adjacent Dyck paths.",
				"Size 2 clearly is impossible with a pair of Dyck paths as boundary.",
				"Size 4 is impossible since the two bounding Dyck paths must end in lengths of 2. However, the rightmost entry in a row of A237591 equals 2 only in rows k*(k+1)/2 - 1, for k\u003e1, so no 2's exist at the diagonal in adjacent Dyck paths.",
				"Size 6 is impossible if Conjecture (2) in A285356 is true since the middle area requires legs of length 3 in the Dyck path at the diagonal; and so are sizes 10 and 14 unless the middle part touches the previous part at a point.",
				"Sizes 6, 10, and 14 do not occur as the middle part for any number n \u003c= 35000 that has an odd number of parts in its symmetric representation of sigma.",
				"For more information see A071562 and A237270."
			],
			"example": [
				"a(4)  = 12 since A071562(4)  =  6 and row  6 of A237270 is (12).",
				"a(8)  =  8 since A071562(8)  = 15 and row 15 of A237270 is (8, 8, 8).",
				"a(23) =  7 since A071562(23) = 49 and row 49 of A237270 is (25, 7, 25).",
				"a(16) = 63 since A071562(16) = 32 and row 32 of A237270 is (63).",
				"Possible layouts for the middle of sizes 4, 6, 10 and 14 with possible Dyck path continuations:",
				"  area:  4          6             10               14",
				"                                              _|_ _",
				"                 _|_          _|_ _            |   |_ _",
				"     _|_ _        | |_         |   |_          |_      |",
				"      |   |       |   |_       |_    |_          |_    |_",
				"      |_ _|_      |_ _ _|_       |_    |           |_    |",
				"          |             |          |_ _|_            |_ _|_",
				"                                       |                 |"
			],
			"mathematica": [
				"(* function a237270[] is defined in A237270 and the property *)",
				"(* on odd divisors is used to minimize computations with a237270 *)",
				"row[n_] := Floor[(Sqrt[8n+1]-1)/2]",
				"evenExp[n_] := First[NestWhile[{#[[1]]+1, #[[2]]/2}\u0026, {0,n}, EvenQ[Last[#]]\u0026]]",
				"oddSRQ[n_] := Module[{e=2^evenExp[n], r=row[n]}, Select[Divisors[n/e], #\u003c=r\u0026\u00262 e #\u003er\u0026]!={}]",
				"middle[list_] := list[[Ceiling[Length[list]/2]]]",
				"a295423[m_, n_] := Map[middle[a237270[#]]\u0026, Select[Range[m, n], oddSRQ]]",
				"a295423[1, 170] (* data *)"
			],
			"xref": [
				"Subsequence of A295422.",
				"Cf. A071562, A237270, A237271, A237591, A237593, A285356."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Hartmut F. W. Hoft_, Feb 12 2018",
			"references": 1,
			"revision": 13,
			"time": "2018-04-12T04:56:39-04:00",
			"created": "2018-04-12T04:56:39-04:00"
		}
	]
}