{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A221671",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 221671,
			"data": "1,2,3,3,4,4,4,5,5,5,5,5,6,6,6,7,7,7,7,7,7,7,8,8,8,8,9,9,9,9,9,9,9,9,9,10,10,10,10,10,11,11,11,11,11,11,11,11,11,11,11,12",
			"name": "Maximum number of squares in a non-constant arithmetic progression (AP) of length n.",
			"comment": [
				"Let s(n;d,i) denote the number of squares in AP i, i+d, i+2d, ..., i+(n-1)d. Then a(n) is the maximum of s(n;d,i) over all such APs with d \u003e 0.",
				"González-Jiménez and Xarles (2013) compute a(n) up to a(52) = 12 using elliptic curves (see Table 2, where their Q(N) = a(N)). They do not seem to have noticed that a(n) = A193832(n) for n != 5 in the range where they computed a(n). I conjecture that this formula holds for all n != 5."
			],
			"link": [
				"Enrique González-Jiménez and Xavier Xarles, \u003ca href=\"http://arxiv.org/abs/1301.5122v1\"\u003eOn a conjecture of Rudin on squares in Arithmetic Progressions\u003c/a\u003e, arXiv 2013."
			],
			"formula": [
				"a(n) = A193832(n) for n \u003c 53 except for n = 5.",
				"a(n) \u003e= A193832(n) for all n. (Proof. A193832 equals the partial sums of A080995 (characteristic function of generalized pentagonal numbers A001318) and a term in the AP 1+24*k is a square if and only if k = A001318(x) = x*(3*x-1)/2 for some x. See González-Jiménez and Xarles (2013) Lemma 2.)",
				"a(A221672(n)) = n."
			],
			"example": [
				"The AP 1, 25, 49 = 1^2, 5^2, 7^2 shows that a(3) = 3. By Fermat and Euler, no four squares are in AP, so a(4) = 3 (see A216869). Then the AP 49, 169, 289, 409, 529 = 7^2, 13^2, 17^2, 409, 23^2 shows that a(5) = 4 (see A216870)."
			],
			"mathematica": [
				"(* note that an extension to more than 52 terms may not be correct *) row[n_] := Join[Table[2*n-1, {2*n-1}], Table[2*n, {n}]]; row[2] = {3, 3, 4, 4, 4}; Flatten[Table[row[n], {n, 1, 6}]][[1 ;; 52]] (* _Jean-François Alcover_, Jan 25 2013, from formula *)"
			],
			"xref": [
				"Cf. A001318, A080995, A193832, A216869, A216870, A221672."
			],
			"keyword": "nonn,hard,more",
			"offset": "1,2",
			"author": "_Jonathan Sondow_, Jan 24 2013",
			"references": 5,
			"revision": 27,
			"time": "2013-02-08T18:18:34-05:00",
			"created": "2013-01-25T18:02:43-05:00"
		}
	]
}