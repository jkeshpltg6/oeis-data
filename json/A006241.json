{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006241",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6241,
			"id": "M0133",
			"data": "1,1,1,2,1,3,1,6,2,3,1,20,1,3,3,54,1,34,1,44,3,3,1,764,2,3,10,140,1,283,1,4470,3,3,3,10416,1,3,3,10820,1,2227,1,2060,62,3,1,958476,2,250,3,8204,1,59154,3,316004,3,3,1,3457904,1,3,158,30229110,3",
			"name": "Number of minimal plane trees with n terminal nodes.",
			"comment": [
				"In equation (4.4) Lew says a(p^3) = 3+3^p, but this is incorrect, it should be a(p^3) = 2+2^p. - _Sean A. Irvine_, Feb 07 2017",
				"From _Gus Wiseman_, Jan 15 2017: (Start)",
				"Number of same-trees of weight n with all leaves equal to 1. A same-tree is either: (case 1) a positive integer, or (case 2) a finite sequence of two or more same-trees all having the same weight, where the weight in case 2 is the sum of weights.",
				"For n\u003e1, a(n) is also equal to the number of same-trees of weight n with all leaves greater than 1 (see example). (End)"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A006241/b006241.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"J. S. Lew, \u003ca href=\"http://dx.doi.org/10.1007/BF01776577\"\u003ePolynomial enumeration of multidimensional lattices\u003c/a\u003e, Math. Systems Theory, 12 (1978), 253-270.",
				"Gus Wiseman, \u003ca href=\"/A006241/a006241.png\"\u003eSame trees with all leaves equal to one n=1..15\u003c/a\u003e",
				"\u003ca href=\"/index/Tra#trees\"\u003eIndex entries for sequences related to trees\u003c/a\u003e"
			],
			"formula": [
				"a(1)=a(2)=a(3)=a(5)=a(7)=1, a(4)=2, a(6)=3, a(n) = Sum_{1 != d | n} a(n / d)^d [From Lew]. - _Sean A. Irvine_, Feb 07 2017 [typo corrected by _Ilya Gutkovskiy_, Apr 24 2019]"
			],
			"example": [
				"The a(12)=20 same-trees with all leaves greater than 1 are:",
				"12, (3333), (222222), ((33)(33)), ((33)(222)), ((33)6), ((222)(33)), ((222)(222)), ((222)6), (6(33)), (6(222)), (66), ((22)(22)(22)), ((22)(22)4), ((22)4(22)), ((22)44), (4(22)(22)), (4(22)4), (44(22)), (444). - _Gus Wiseman_, Jan 15 2017"
			],
			"maple": [
				"a:= proc(n) option remember; `if`(n=1, 1, add(",
				"      a(n/d)^d, d=numtheory[divisors](n) minus {1}))",
				"    end:",
				"seq(a(n), n=1..70);  # _Alois P. Heinz_, Feb 21 2017"
			],
			"mathematica": [
				"Array[If[#1===1,1,Sum[#0[#1/d]^d,{d,Rest[Divisors[#1]]}]]\u0026,200] (* _Gus Wiseman_, Jan 15 2017 *)"
			],
			"xref": [
				"Cf. A001003, A281145."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"a(8), a(27), and a(50) corrected by _Sean A. Irvine_, Feb 07 2017"
			],
			"references": 11,
			"revision": 35,
			"time": "2021-12-26T21:31:59-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}