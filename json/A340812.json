{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A340812",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 340812,
			"data": "1,1,1,1,1,1,1,1,1,2,1,1,1,2,3,1,1,1,3,7,6,1,1,1,3,11,18,11,1,1,1,4,17,49,68,23,1,1,1,4,25,96,252,251,47,1,1,1,5,33,177,687,1406,1020,106,1,1,1,5,43,285,1537,5087,8405,4258,235,1,1,1,6,55,442,3014,14310,40546,52348,18580,551",
			"name": "Array read by antidiagonals: T(n,k) is the number of unlabeled oriented k-gonal 2-trees with n oriented polygons, n \u003e= 0, k \u003e= 2.",
			"comment": [
				"See section 3 of the Labelle reference."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A340812/b340812.txt\"\u003eTable of n, a(n) for n = 0..1325\u003c/a\u003e",
				"G. Labelle, C. Lamathe and P. Leroux, \u003ca href=\"http://arXiv.org/abs/math.CO/0312424\"\u003eLabeled and unlabeled enumeration of k-gonal 2-trees\u003c/a\u003e, arXiv:math/0312424 [math.CO], Dec 23 2003."
			],
			"formula": [
				"G.f. of column k: B(x) - x*B(x)^k + x*(Sum_{d|k} phi(d)*B(x^d)^(k/d))/k, where B(x) if the g.f. of column k of A340814."
			],
			"example": [
				"Array begins:",
				"=========================================================",
				"n\\k |  2    3    4     5      6      7      8       9",
				"----+----------------------------------------------------",
				"  0 |  1    1    1     1      1      1      1       1 ...",
				"  1 |  1    1    1     1      1      1      1       1 ...",
				"  2 |  1    1    1     1      1      1      1       1 ...",
				"  3 |  2    2    3     3      4      4      5       5 ...",
				"  4 |  3    7   11    17     25     33     43      55 ...",
				"  5 |  6   18   49    96    177    285    442     635 ...",
				"  6 | 11   68  252   687   1537   3014   5370    8901 ...",
				"  7 | 23  251 1406  5087  14310  33632  70000  132533 ...",
				"  8 | 47 1020 8405 40546 141582 399065 966254 2089103 ...",
				"  ..."
			],
			"program": [
				"(PARI) \\\\ here B(n,k) gives column k of A340814.",
				"EulerT(v)={Vec(exp(x*Ser(dirmul(v, vector(#v, n, 1/n))))-1, -#v)}",
				"B(n, k)={my(p=1+O(x)); for(n=1, n, p=1+x*Ser(EulerT(Vec(p^(k-1))))); p}",
				"C(n, k)={my(p=B(n,k)); Vec(p - x*p^k + x*sumdiv(k, d, eulerphi(d)*subst(p + O(x*x^(n\\d)), x, x^d)^(k/d))/k)}",
				"{ Mat(vector(7, k, C(7, k+1)~)) }"
			],
			"xref": [
				"Columns 2..4 are A000055, A303742, A340813.",
				"Cf. A340811 (unoriented case), A340814 (edge-rooted case)."
			],
			"keyword": "nonn,tabl",
			"offset": "0,10",
			"author": "_Andrew Howroyd_, Feb 02 2021",
			"references": 5,
			"revision": 11,
			"time": "2021-02-03T21:51:47-05:00",
			"created": "2021-02-02T18:24:19-05:00"
		}
	]
}