{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A164057",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 164057,
			"data": "1,1,0,1,0,0,1,1,0,0,1,0,0,1,1,1,0,0,1,0,0,1,1,0,0,0,1,1,0,1,1,1,0,0,1,0,0,1,1,0,0,0,1,1,0,1,1",
			"name": "Complement to A164056, change A164056 bits (0-\u003e1; 1-\u003e0). Provides a coding template for Petoukhov matrices, relating to DNA codons.",
			"comment": [
				"Sequences by rows can be used as mapping tools for generating Gray code maps.",
				"Jay Kappraff alerted me to the connection between the multiplication version (below) and the 2*3 multiplication table of A036561, in that the terms of the multiplication table (below): (27, 18, 12, 8) are seen as a diagonal in:",
				"1...3,...9,...27,...",
				"2,..6,..18,.........",
				"4..12...............",
				"8...................",
				".",
				"We may recreate the top row (below): (27, 18, 12, 18, 12, 8, 12, 18), by starting at \"27\" in the above array, then given the code (1,0,0,1,0,0,1,1), and (8, 12, 18, 27), we mark down the term to the left if the code = 0, (1 otherwise), giving \"27\" then L,L,R,L,L,R,R or: (27, 18, 12, 18, 12, 8, 12, 18).",
				"Such operations preserve the harmonic character of the isomorphic array in terms of multiplication or division by (2/3) or (3/2) linked to the 2*3 multiplication table. The Gray code map preserves the \"one operation\" procedure as well as a binomial distribution as to frequency.",
				"The 8*8 array below with top row [27, 18, 12, 18, 12, 8, 12, 18]... has been investigated extensively by Petoukhov, relating to the 64 DNA codons (Cf. A164091, A147995). Petoukhov has made the remarkable discovery that such (Petoukhov matrices) can be generated as squares of matrices with irrational terms, in this case phi, 1.618..."
			],
			"reference": [
				"Sergei Petoukhov and Matthew He, \"Symmetrical Analysis Techniques for Genetic Systems and Bioinformatics - Advanced Patterns and Applications\", IGI Global, (978-1-60566-127-9); October, 2009, Chapters (2, 4, and 6) Clifford Pickover, \"The Zen of Magic Squares, Circles, and Stars\", Princeton University Press, 2002, pages 285-289."
			],
			"formula": [
				"By rows, change bits of A164056: (0-\u003e1); (1-\u003e0). Note that A164056 can be derived from 2^n strings of Gray code terms by recording the number of 1's in the Gray code terms for n, followed by the rule \"1\" is recorded if next term is greater than current; 0 otherwise."
			],
			"example": [
				"First few rows of the triangle in 2^n term strings:",
				"1;",
				"1, 0;",
				"1, 0, 0, 1;",
				"1, 0, 0, 1, 0, 0, 1, 1;",
				"1, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 1, 1;",
				"...",
				"Given the 16 bit Gray code string (0,...-\u003e15): 0000, 0001, 0011, 0010, 0110, 0111, 0101, 0100, 1100, 1101, 1111, 1110, 1010, 1011, 1001, 1000; the number f of 1's per term = (0, 1, 2, 1, 2, 3, 2, 1, 2, 3, 4, 3, 2, 3, 2, 1). Then using the increase/decrease rule, we get row 5 of A164056",
				".",
				"0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0 = row 5 of A164056.",
				"Change to",
				"1, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 1, 1 = row 5 of A164057.",
				".",
				"We may use row 3 to generate arrays that make use of the terms by addition or multiplication: By addition: we recreate an array of the number of hydrogen bonds per codon/anti-codon (Cf. A147995, the 64 codons mapped on a Gray code format). Beginning with \"9\" and using row 4: (1,0,0,1,0,0,1,1); we increase by 1 starting from left if we encounter a 1, and decrease by 1 if the next term = 0. We get: (9, 8, 7, 8, 7, 6, 7, 8) = A. Next, the same sequence A along the left border and 9's as the diagonal. Given upper left term = (1,1), for odd numbered columns (n), begin at position (n,n) and circulate A downward. For even numbered columns, circulate A upward.",
				"This gets us:",
				".",
				"9, 8, 7, 8, 7, 6, 7, 8",
				"8, 9, 8, 7, 6, 7, 8, 7",
				"7, 8, 9, 8, 7, 8, 7, 6",
				"8, 7, 8, 9, 8, 7, 8, 7",
				"7, 6, 7, 8, 9, 8, 7, 8",
				"6, 7, 8, 7, 8, 9, 6, 7",
				"7, 8, 7, 6, 7, 8, 9, 8",
				"8, 7, 6, 7, 8, 7, 8, 9",
				".",
				"As shown, (for example), column 4 begins at (4,4), then circulates upwards with sequence A. Last, we superimpose the hydrogen bond array on the DNA array as shown in A147995. Mapping the terms according to the Gray code rules preserves the \"1\" rule in any Knights's move direction including wrap-arounds: Every neighbor differs from any entry by \"1\" by addition or subtraction.",
				"Note that in the previous array, (6, 7, 8, 9) may be obtained by the appropriate addition of terms (2 or 3). In the next example, we use the rows to generate A164091, (which I name Petoukhov matrices) as follows:",
				".",
				"Again, we refer to row 5: (1, 0, 0, 1, 0, 0, 1, 1) and given the upper left term of an 8x8 array = (1,1), we begin with \"27\" (= 3*3*3 rather than 3+3+3 = 9 as in the addition case. Then, when encountering an 0, multiply current term by (2/3). If the next term = 1, multiply current term by (3/2). Then use the identical circulate rule using \"B\" = (27, 18, 12, 18, 12, 8, 12, 18) since given (1, 0, 0, 1, 0, 0, 1, 1) and \"27\", the next term (an 18) = (2/3) * 27, followed by 12 = (2/3)*18, etc; getting: (Cf. A164091):",
				".",
				"27, 18, 12, 18, 12, 08, 12, 18",
				"18, 27, 18, 12, 08, 12, 18, 12",
				"12, 18, 27, 18, 12, 18, 12, 08",
				"18, 12, 18, 27, 18, 12, 08, 12",
				"12, 08, 12, 18, 27, 18, 12, 18",
				"08, 12, 18, 12, 18, 27, 18, 12",
				"12, 18, 12, 08, 12, 18, 27, 18",
				"18, 12, 08, 12, 18, 12, 18, 27",
				".",
				"Both the addition case and the multiplication case have a binomial frequency of terms by rows and columns: (one 9, three 7's, three 8's and one 6); while the multiplication case has (one 27, three 18's three 12's and one 8). Both versions preserve the Gray code \"one operation\" rule in any Knight's move including wrap arounds, since given the second case, any neighbor may be obtained by multiplication of (2/3) or (3/2)."
			],
			"xref": [
				"A036561, A147995, A164056, A164091"
			],
			"keyword": "nonn,tabl",
			"offset": "0,1",
			"author": "_Gary W. Adamson_, Aug 09 2009",
			"references": 5,
			"revision": 5,
			"time": "2013-01-07T12:01:56-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}