{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A096727",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 96727,
			"data": "1,-8,24,-32,24,-48,96,-64,24,-104,144,-96,96,-112,192,-192,24,-144,312,-160,144,-256,288,-192,96,-248,336,-320,192,-240,576,-256,24,-384,432,-384,312,-304,480,-448,144,-336,768,-352,288,-624,576,-384,96,-456,744,-576,336,-432,960,-576,192",
			"name": "Expansion of eta(q)^8 / eta(q^2)^4 in powers of q.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A096727/b096727.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e",
				"K. S. Williams, \u003ca href=\"http://www.jstor.org/stable/10.4169/amer.math.monthly.120.04.329\"\u003eThe parents of Jacobi's four squares theorem are unique\u003c/a\u003e, Amer. Math. Monthly, 120 (2013), 329-345."
			],
			"formula": [
				"a(n) =  -8*sigma(n) + 48*sigma(n/2) - 64*sigma(n/4) for n\u003e0, where sigma(n) = A000203(n) if n is an integer, otherwise 0.",
				"Euler transform of period 2 sequence [ -8, -4, ...].",
				"G.f.: Prod_{k\u003e0} (1 - x^k)^8 / (1 - x^(2k))^4 = 1 + Sum_{k\u003e0} k * (-8 * x^k / (1 - x^k) + 48 * x^(2*k)  /(1 - x^(2*k)) - 64 * x^(4*k)/(1 - x^(4*k))).",
				"G.f. theta_4(q)^4 = (Sum_{k} (-q)^(k^2))^4.",
				"Expansion of phi(-q)^4 in powers of q where phi() is a Ramanujan theta function. - _Michael Somos_, Nov 01 2006",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^3), A(x^9)) where f(u, v, w) = v^4 - 30*u*v^2*w + 12*u*v*w * (u + 9*w) - u*w * (u^2 + 9*w*u + 81*w^2).",
				"a(n) = (-1)^n * A000118(n). a(n) = 8 * A109506(n) unless n=0. a(2*n) = A004011(n). a(2*n + 1) = -A005879(n).",
				"a(0) = 1, a(n) = -(8/n)*Sum_{k=1..n} A002131(k)*a(n-k) for n \u003e 0. - _Seiichi Manyama_, May 02 2017"
			],
			"example": [
				"G.f. = 1 - 8*q + 24*q^2 - 32*q^3 + 24*q^4 - 48*q^5 + 96*q^6 - 64*q^7 + 24*q^8 - ..."
			],
			"mathematica": [
				"CoefficientList[ Series[1 + Sum[k(-8x^k/(1 - x^k) + 48x^(2k)/(1 - x^(2k)) - 64x^(4k)/(1 - x^(4k))), {k, 1, 60}], {x, 0, 60}], x] (* _Robert G. Wilson v_, Jul 14 2004 *)",
				"a[ n_] := With[{m = InverseEllipticNomeQ @ q}, SeriesCoefficient[ q Dt[ Log @ m, q], {q, 0, n}]]; (* _Michael Somos_, Sep 06 2012 *)",
				"a[ n_] := (-1)^n SquaresR[ 4, n]; (* _Michael Somos_, Jun 12 2014 *)",
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 4, 0, q]^4, {q, 0, n}]; (* _Michael Somos_, Jun 12 2014 *)",
				"QP = QPochhammer; s = QP[q]^8/QP[q^2]^4 + O[q]^60; CoefficientList[s, q] (* _Jean-François Alcover_, Nov 23 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, n==0, 8 * (-1)^n * sumdiv( n, d, if( d%4, d)))};",
				"(PARI) {a(n) = local(A); if( n\u003c0, 0, A = x *O (x^n); polcoeff( eta(x + A)^8 / eta(x^2 + A)^4, n))};",
				"(Sage) A = ModularForms( Gamma0(4), 2, prec=57) . basis(); A[0] - 8*A[1]; # _Michael Somos_, Jun 12 2014",
				"(MAGMA) A := Basis( ModularForms( Gamma0(4), 2), 57); A[1] - 8*A[2]; /* _Michael Somos_, Aug 21 2014 */",
				"(Julia) # JacobiTheta4 is defined in A002448.",
				"A096727List(len) = JacobiTheta4(len, 4)",
				"A096727List(57) |\u003e println # _Peter Luschny_, Mar 12 2018"
			],
			"xref": [
				"Cf. A000118, A002131, A004011, A005879, A109506."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Jul 06 2004",
			"references": 17,
			"revision": 37,
			"time": "2021-03-12T22:24:43-05:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}