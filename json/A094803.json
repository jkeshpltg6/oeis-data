{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A094803",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 94803,
			"data": "1,3,9,28,90,296,988,3328,11272,38304,130416,444544,1516320,5174144,17659840,60282880,205795456,702583296,2398676736,8189409280,27960021504,95460743168,325921881088,1112763940864,3799207806976",
			"name": "Number of (s(0), s(1), ..., s(2n)) such that 0 \u003c s(i) \u003c 8 and |s(i) - s(i-1)| = 1 for i = 1,2,...,2n, s(0) = 1, s(2n) = 3.",
			"comment": [
				"In general, a(n) = (2/m)*Sum_{r=1..m-1} sin(r*j*Pi/m)*sin(r*k*Pi/m)*(2*cos(r*Pi/m))^(2n)) counts (s(0), s(1), ..., s(2n)) such that 0 \u003c s(i) \u003c m and |s(i) - s(i-1)| = 1 for i = 1,2,...,2n, s(0) = j, s(2n) = k.",
				"Counts all paths of length (2*n+1), n \u003e= 0, starting and ending at the initial node and ending at the nodes 1, 2, 3, 4 and 5 on the path graph P_7, see the Maple program. - _Johannes W. Meijer_, May 29 2010"
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A094803/b094803.txt\"\u003eTable of n, a(n) for n = 1..1876\u003c/a\u003e",
				"László Németh and László Szalay, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL24/Nemeth/nemeth8.html\"\u003eSequences Involving Square Zig-Zag Shapes\u003c/a\u003e, J. Int. Seq., Vol. 24 (2021), Article 21.5.2.",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (6,-10,4)."
			],
			"formula": [
				"a(n) = (1/4)*Sum_{k=1..7} sin(Pi*k/8)*sin(3*Pi*k/8)*(2*cos(Pi*k/8))^(2n).",
				"a(n) = 6*a(n-1) - 10*a(n-2) + 4*a(n-3).",
				"G.f.: -x*(1-3*x+x^2) / ( (2*x-1)*(2*x^2-4*x+1) )"
			],
			"maple": [
				"with(GraphTheory): G:=PathGraph(7): A:= AdjacencyMatrix(G): nmax:=25; n2:=2*nmax: for n from 0 to n2 do B(n):=A^n; a(n):=add(B(n)[1,k],k=1..5); od: seq(a(2*n+1),n=0..nmax-1); # _Johannes W. Meijer_, May 29 2010"
			],
			"mathematica": [
				"f[n_] := FullSimplify[ TrigToExp[(1/4)Sum[ Sin[Pi*k/8]Sin[3Pi*k/8](2Cos[Pi*k/8])^(2n), {k, 1, 7}]]]; Table[ f[n], {n, 25}] (* _Robert G. Wilson v_, Jun 18 2004 *)",
				"Rest@ CoefficientList[Series[-x (1 - 3 x + x^2)/((2 x - 1)*(2 x^2 - 4 x + 1)), {x, 0, 25}], x] (* _Michael De Vlieger_, Aug 04 2021 *)"
			],
			"xref": [
				"Cf. A006012, A030436 and A024175."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Herbert Kociemba_, Jun 11 2004",
			"references": 6,
			"revision": 21,
			"time": "2021-08-04T21:18:32-04:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}