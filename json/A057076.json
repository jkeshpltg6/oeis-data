{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A057076",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 57076,
			"data": "2,11,119,1298,14159,154451,1684802,18378371,200477279,2186871698,23855111399,260219353691,2838557779202,30963916217531,337764520613639,3684445810532498,40191139395243839,438418087537149731",
			"name": "A Chebyshev or generalized Fibonacci sequence.",
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A057076/b057076.txt\"\u003eTable of n, a(n) for n = 0..963\u003c/a\u003e",
				"Hacène Belbachir, Soumeya Merwa Tebtoub, and László Németh, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL23/Nemeth/nemeth7.html\"\u003eEllipse Chains and Associated Sequences\u003c/a\u003e, J. Int. Seq., Vol. 23 (2020), Article 20.8.5.",
				"P. Bhadouria, D. Jhala, and B. Singh, \u003ca href=\"http://dx.doi.org/10.22436/jmcs.08.01.07\"\u003eBinomial Transforms of the k-Lucas Sequences and its Properties\u003c/a\u003e, The Journal of Mathematics and Computer Science (JMCS), Volume 8, Issue 1, Pages 81-92; sequence R_3.",
				"S. Falcon, \u003ca href=\"http://dx.doi.org/10.4236/am.2014.515216\"\u003eRelationships between Some k-Fibonacci Sequences\u003c/a\u003e, Applied Mathematics 5 (2014), 2226-2234.",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Rea#recur1\"\u003eIndex entries for recurrences a(n) = k*a(n - 1) +/- a(n - 2)\u003c/a\u003e",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (11,-1)."
			],
			"formula": [
				"a(n) = S(n, 11) - S(n-2, 11) = 2*T(n, 11/2) with S(n, x) := U(n, x/2), S(-1, x) := 0, S(-2, x) := -1. S(n, 11)=A004190(n). U-, resp. T-, are Chebyshev's polynomials of the second, resp. first, case. See A049310 and A053120.",
				"G.f.: (2-11x)/(1-11x+x^2).",
				"a(n) = a(-n). - _Michael Somos_, Apr 25 2003",
				"a(n) = ap^n + am^n, with ap := (11+sqrt(117))/2 and am := (11-sqrt(117))/2."
			],
			"example": [
				"G.f. = 2 + 11*x +119*x^2 + 1298*x^3 + 14159*x^4 + 154451*x^5 + ..."
			],
			"mathematica": [
				"a[0] = 2; a[1] = 11; a[n_] := 11a[n - 1] - a[n - 2]; Table[ a[n], {n, 0, 17}] (* _Robert G. Wilson v_, Jan 30 2004 *)",
				"a[ n_] := 2 ChebyshevT[ n, 11/2]; (* _Michael Somos_, May 28 2014 *)"
			],
			"program": [
				"(PARI) {a(n) = subst( poltchebi(n), x, 11/2) * 2};",
				"(PARI) {a(n) = 2 * poltchebyshev(n, 1, 11/2)}; /* _Michael Somos_, May 28 2014 */",
				"(PARI) Vec((2-11*x)/(1-11*x+x^2) + O(x^40)) \\\\ _Michel Marcus_, Feb 18 2016",
				"(Sage) [lucas_number2(n,11,1) for n in range(27)] # _Zerinvary Lajos_, Jun 25 2008"
			],
			"xref": [
				"a(n) = sqrt(4+117*A004190(n-1)^2), n\u003e=1."
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "_Wolfdieter Lang_, Oct 31 2002",
			"references": 8,
			"revision": 41,
			"time": "2020-12-29T20:39:11-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}