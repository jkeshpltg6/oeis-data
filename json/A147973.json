{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A147973",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 147973,
			"data": "-4,2,4,2,-4,-14,-28,-46,-68,-94,-124,-158,-196,-238,-284,-334,-388,-446,-508,-574,-644,-718,-796,-878,-964,-1054,-1148,-1246,-1348,-1454,-1564,-1678,-1796,-1918,-2044,-2174,-2308,-2446,-2588,-2734,-2884",
			"name": "a(n) = -2*n^2 + 12*n - 14.",
			"comment": [
				"-a(n+3) = 2*n^2 - 4, n \u003e= 0, [-4,-2, 4, 14, ...] appears as the first member of the quartet for the square of [n, n+1, n+2, n+3], for n \u003e= 0, in the Clifford algebra Cl_2. The other members are given in A046092(n), A054000(n+1) and A139570(n). The basis of Cl_2 is \u003c1, s1, s2, s12\u003e with s1.s1 = s2.s2 = 1, s12.s12 = -1, s1.s2 = -s2.s1 = s12. See e.g., pp. 5-6, eqs. (2.4)-(2.13) of the S. Gull et al. reference. - _Wolfdieter Lang_, Oct 15 2014",
				"Related to the previous comment: if one uses the exterior (Grassmann) product with s1.s1 = s2.s2 = = s12.s12 = 0 and s1.s2 = -s2.s1 = s12, then the four components of the square of [n, n+1, n+2, n+3] are [A000290(n), A046092(n), A054000(n+1), A139570(n)], n \u003e= 0. - _Wolfdieter Lang_, Nov 13 2014",
				"2 - a(n)/2 is a square. - _Bruno Berselli_, Apr 10 2018"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A147973/b147973.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"S. Gull, A. Lasenby and C. Doran, \u003ca href=\"http://geometry.mrao.cam.ac.uk/1993/01/imaginary-numbers-are-not-real-the-geometric-algebra-of-spacetime/\"\u003eImaginary Numbers are not Real - the Geometric Algebra of Spacetime\u003c/a\u003e, Found. Phys. 23(9), 1175-1201 (1993).",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-3,1)."
			],
			"formula": [
				"a(n) = 3*a(n-1) - 3*a(n-2) + a(n-3). - _Vincenzo Librandi_, Jul 10 2012",
				"a(n) = -2*A008865(n-3). - _J. M. Bergot_, Jun 25 2018",
				"G.f.: -2*x*(2 - 7*x + 7*x^2) / (1 - x)^3. - _Colin Barker_, Feb 12 2019"
			],
			"maple": [
				"[-2*n^2+12*n-14$n=1..50]; # _Muniru A Asiru_, Feb 12 2019"
			],
			"mathematica": [
				"lst={};Do[k=n^2-((n-1)^2+(n-2)^2+(n-3)^2);AppendTo[lst,k],{n,5!}];lst",
				"Table[-2n^2+12n-14,{n,1,50}] (* _Vincenzo Librandi_, Jul 10 2012 *)",
				"LinearRecurrence[{3,-3,1},{-4,2,4},50] (* _Harvey P. Dale_, Mar 02 2020 *)"
			],
			"program": [
				"(MAGMA) [-2*n^2+12*n-14: n in [1..50]]; // _Vincenzo Librandi_, Jul 10 2012",
				"(PARI) a(n)=-2*n^2+12*n-14 \\\\ _Charles R Greathouse IV_, Sep 24 2015",
				"(PARI) Vec(-2*x*(2 - 7*x + 7*x^2) / (1 - x)^3 + O(x^40)) \\\\ _Colin Barker_, Feb 12 2019"
			],
			"xref": [
				"Cf. A008865."
			],
			"keyword": "sign,easy",
			"offset": "1,1",
			"author": "_Vladimir Joseph Stephan Orlovsky_, Nov 18 2008",
			"references": 23,
			"revision": 55,
			"time": "2020-03-02T19:41:17-05:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}