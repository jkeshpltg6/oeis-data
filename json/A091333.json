{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A091333",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 91333,
			"data": "1,2,3,4,5,5,6,6,6,7,8,7,8,8,8,8,9,8,9,9,9,10,10,9,10,10,9,10,11,10,11,10,11,11,11,10,11,11,11,11,12,11,12,12,11,12,12,11,12,12,12,12,12,11,12,12,12,13,13,12,13,13,12,12,13,13,14,13,13,13,13,12,13,13,13,13,14",
			"name": "Number of 1's required to build n using +, -, *, and parentheses.",
			"comment": [
				"Consider an alternate complexity measure b(n) which gives the minimum number of 1's necessary to build n using +, -, *, and / (where this additional operation is strict integer division, defined only for n/d where d|n). It turns out that b(n) coincides with a(n) for all n up to 50221174, see A348069. - _Glen Whitney_, Sep 23 2021"
			],
			"link": [
				"Janis Iraids, \u003ca href=\"/A091333/b091333.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Juris Cernenoks, Janis Iraids, Martins Opmanis, Rihards Opmanis and Karlis Podnieks, \u003ca href=\"http://arxiv.org/abs/1409.0446\"\u003eInteger Complexity: Experimental and Analytical Results II\u003c/a\u003e, arXiv:1409.0446 [math.NT], 2014.",
				"Janis Iraids, \u003ca href=\"/A091333/a091333.c.txt\"\u003eA C program to compute the sequence\u003c/a\u003e",
				"J. Iraids, K. Balodis, J. Cernenoks, M. Opmanis, R. Opmanis and K. Podnieks, \u003ca href=\"http://arxiv.org/abs/1203.6462\"\u003eInteger Complexity: Experimental and Analytical Results\u003c/a\u003e. arXiv preprint arXiv:1203.6462 [math.NT], 2012. - From _N. J. A. Sloane_, Sep 22 2012",
				"\u003ca href=\"/index/Com#complexity\"\u003eIndex to sequences related to the complexity of n\u003c/a\u003e"
			],
			"example": [
				"A091333(23) = 10 because 23 = (1+1+1+1) * (1+1+1) * (1+1) - 1. (Note that 23 is also the smallest index at which A091333 differs from A005245.)"
			],
			"program": [
				"(Python)",
				"from functools import cache",
				"@cache",
				"def f(m):",
				"    if m == 0: return set()",
				"    if m == 1: return {1}",
				"    out = set()",
				"    for j in range(1, m//2+1):",
				"        for x in f(j):",
				"            for y in f(m-j):",
				"                out.update([x + y, x * y])",
				"                if x != y: out.add(abs(x-y))",
				"    return out",
				"def aupton(terms):",
				"    tocover, alst, n = set(range(1, terms+1)), [0 for i in range(terms)], 1",
				"    while len(tocover) \u003e 0:",
				"        for k in f(n) - f(n-1):",
				"            if k \u003c= terms:",
				"                alst[k-1] = n",
				"                tocover.discard(k)",
				"        n += 1",
				"    return alst",
				"print(aupton(77)) # _Michael S. Branicky_, Sep 28 2021"
			],
			"xref": [
				"Cf. A005245, A025280, A091334."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Jens Voß_, Dec 30 2003",
			"references": 10,
			"revision": 44,
			"time": "2021-09-28T15:45:25-04:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}