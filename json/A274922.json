{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A274922",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 274922,
			"data": "1,-1,2,-3,4,-5,6,-7,8,-9,10,-11,12,-13,14,-15,16,-17,18,-19,20,-21,22,-23,24,-25,26,-27,28,-29,30,-31,32,-33,34,-35,36,-37,38,-39,40,-41,42,-43,44,-45,46,-47,48,-49,50,-51,52,-53,54,-55,56,-57,58,-59",
			"name": "a(n) = (-1)^n * n if n\u003e0, a(0) = 1.",
			"comment": [
				"This is a divisibility sequence."
			],
			"link": [
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (-2,-1)."
			],
			"formula": [
				"Euler transform of length 3 sequence [-1, 2, -1].",
				"a(n) = -b(n) where b() is multiplicative with b(2^e) = -(2^e) if e\u003e0, b(p^e) = p^e otherwise.",
				"E.g.f.: 1 - x * exp(-x).",
				"G.f.: (1 + x + x^2) / (1 + 2*x + x^2).",
				"G.f.: (1 - x) * (1 - x^3) / (1 - x^2)^2.",
				"G.f.: 1 / (1 + x / (1 + x / (1 - x / (1 + x)))).",
				"G.f.: 1 - x / (1 + x)^2 = 1 - x /(1 - x)^2 + 4*x^2 / (1 - x^2)^2.",
				"a(n) = (-1)^n * A028310(n). a(2*n) = A004277(n). a(2*n + 1) = - A005408(n).",
				"Convolution inverse of A106510.",
				"A060576(n) = Sum_{k=0..n} binomial(n, k) * a(k).",
				"A028310(n) = Sum_{k=0..n} binomial(n+1, k+1) * a(k).",
				"a(n) = A038608(n), n\u003e0. - _R. J. Mathar_, May 25 2020"
			],
			"example": [
				"G.f. = 1 - x + 2*x^2 - 3*x^3 + 4*x^4 - 5*x^5 + 6*x^6 - 7*x^7 + 8*x^8 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 1, Boole[n == 0], (-1)^n n];",
				"a[ n_] := SeriesCoefficient[ (1 + x + x^2) / (1 + 2*x + x^2), {x, 0, n}];",
				"LinearRecurrence[{-2,-1},{1,-1,2},60] (* _Harvey P. Dale_, Mar 30 2019 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, n==0, (-1)^n * n)};",
				"(PARI) {a(n) = if( n\u003c0, 0, polcoeff( (1 + x + x^2) / (1 + 2*x + x^2) + x * O(x^n), n))};",
				"(MAGMA) m:=50; R\u003cx\u003e:=PowerSeriesRing(Integers(), m); Coefficients(R!((1 + x+x^2)/(1+2*x+x^2))); // _G. C. Greubel_, Jul 29 2018"
			],
			"xref": [
				"Cf. A004277, A005408, A028310, A060576, A106510."
			],
			"keyword": "sign,easy",
			"offset": "0,3",
			"author": "_Michael Somos_, Dec 28 2016",
			"references": 1,
			"revision": 62,
			"time": "2020-05-25T06:08:34-04:00",
			"created": "2016-12-28T20:21:47-05:00"
		}
	]
}