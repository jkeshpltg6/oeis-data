{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A198329",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 198329,
			"data": "1,1,1,1,2,2,1,1,4,3,3,2,2,2,6,1,2,4,1,3,4,5,4,2,9,3,8,2,3,6,5,1,10,3,6,4,2,2,6,3,3,4,2,5,12,7,6,2,4,9,6,3,1,8,15,2,4,5,3,6,4,11,8,1,9,10,2,3,14,6,3,4,4,3,18,2,10,6,5,3,16,5,7",
			"name": "The Matula-Goebel number of the rooted tree obtained from the rooted tree with Matula-Goebel number n after removing the vertices of degree one, together with their incident edges.",
			"comment": [
				"This is the pruning operation mentioned, for example, in the Balaban reference (p. 360) and in the Todeschini - Consonni reference (p. 42)."
			],
			"reference": [
				"A. T. Balaban, Chemical graphs, Theoret. Chim. Acta (Berl.) 53, 355-375, 1979.",
				"R. Todeschini and V. Consonni, Handbook of Molecular Descriptors, Wiley-VCH, 2000."
			],
			"link": [
				"F. Goebel, \u003ca href=\"http://dx.doi.org/10.1016/0095-8956(80)90049-0\"\u003eOn a 1-1-correspondence between rooted trees and natural numbers\u003c/a\u003e, J. Combin. Theory, B 29 (1980), 141-143.",
				"I. Gutman and A. Ivic, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(95)00182-V\"\u003eOn Matula numbers\u003c/a\u003e, Discrete Math., 150, 1996, 131-142.",
				"I. Gutman and Yeong-Nan Yeh, \u003ca href=\"http://www.emis.de/journals/PIMB/067/3.html\"\u003eDeducing properties of trees from their Matula numbers\u003c/a\u003e, Publ. Inst. Math., 53 (67), 1993, 17-22.",
				"D. Matula, \u003ca href=\"http://www.jstor.org/stable/2027327\"\u003eA natural rooted tree enumeration by prime factorization\u003c/a\u003e, SIAM Rev. 10 (1968) 273.",
				"\u003ca href=\"/index/Mat#matula\"\u003eIndex entries for sequences related to Matula-Goebel numbers\u003c/a\u003e"
			],
			"formula": [
				"Let b(n)=A198328(n) (= the Matula-Goebel number of the tree obtained by removing the leaves together with their incident edges from the rooted tree with Matula-Goebel number n). a(1)=1; if n = p(t) (=the t-th prime), then a(n)=b(t); if n=rs (r,s\u003e=2), then a(n)=b(r)b(s). The Maple program is based on this recursive formula."
			],
			"example": [
				"a(7)=1 because the rooted tree with Matula-Goebel number 7 is Y; after deleting the vertices of degree one and their incident edges we obtain the 1-vertex tree having Matula-Goebel number 1."
			],
			"maple": [
				"with(numtheory): a := proc (n) local r, s, b: r := proc (n) options operator, arrow: op(1, factorset(n)) end proc: s := proc (n) options operator, arrow: n/r(n) end proc: b := proc (n) if n = 1 then 1 elif n = 2 then 1 elif bigomega(n) = 1 then ithprime(b(pi(n))) else b(r(n))*b(s(n)) end if end proc: if n = 1 then 1 elif bigomega(n) = 1 then b(pi(n)) else b(r(n))*b(s(n)) end if end proc: seq(a(n), n = 1 .. 120);"
			],
			"mathematica": [
				"b[1] = b[2] = 1; b[n_] := b[n] = If[PrimeQ[n], Prime[b[PrimePi[n]]], Times @@ (b[#[[1]]]^#[[2]]\u0026 /@ FactorInteger[n])];",
				"a[1] = 1; a[n_] := a[n] = If[PrimeQ[n], b[PrimePi[n]], Times @@ (b[#[[1]] ]^#[[2]]\u0026 /@ FactorInteger[n])];",
				"Array[a, 100] (* _Jean-François Alcover_, Dec 18 2017 *)"
			],
			"xref": [
				"Cf. A198328."
			],
			"keyword": "nonn",
			"offset": "1,5",
			"author": "_Emeric Deutsch_, Nov 24 2011",
			"references": 5,
			"revision": 16,
			"time": "2017-12-18T08:30:09-05:00",
			"created": "2011-11-25T17:11:09-05:00"
		}
	]
}