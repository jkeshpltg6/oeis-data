{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A291897",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 291897,
			"data": "1,9,125,32977,971919,358472059,47622059953,137818710619425,8141400285401267,9740358918723188381,3597069206174040366021,12859671622917809034800123,3419734700063005545155284375,8538628250545609672426471056711,6181704419438256867205044161777369",
			"name": "Numerator of E(2*n-1,n), where E(n,x) is the Euler polynomial.",
			"comment": [
				"Conjecture: a(n) is divisible by (2*n-1)^2.",
				"_Robert G. Wilson v_ verified this conjecture up to 5000.",
				"Note that sometimes a(n) is divisible by (2n-1)^3, for example, for n = 1,3,7,9,... when 2*n-1 = 1,5,13,17,... ."
			],
			"reference": [
				"M. Abramowitz and I. A. Stegun, Handbook of Mathematical Functions, 1972, Ch. 23."
			],
			"link": [
				"Robert G. Wilson v, \u003ca href=\"/A291897/b291897.txt\"\u003eTable of n, a(n) for n = 1..215\u003c/a\u003e",
				"Vladimir Shevelev, \u003ca href=\"https://arxiv.org/abs/1708.08096\"\u003eOn a Luschny question\u003c/a\u003e, arXiv:1708.08096 [math.NT], 2017."
			],
			"formula": [
				"a(n) = (E(2*n-1,n) + (-1)^(n-1)*E(2*n-1,0))*A006519(2*n) + A002425(n).",
				"a(n) = 2*(-1)^n*A292706(n)*A006519(2*n) + A002425(n).",
				"a(n) = E(2*n-1, n)*2^A007814(2*n). - _Peter Luschny_, Sep 22 2017"
			],
			"maple": [
				"A291897 := n -\u003e euler(2*n-1, n)*2^(padic[ordp](2*n, 2)):",
				"seq(A291897(n), n=1..15); # _Peter Luschny_, Sep 22 2017"
			],
			"mathematica": [
				"f[n_] := Numerator@ EulerE[2 n - 1, n]; Array[f, 15] (* _Robert G. Wilson v_, Sep 22 2017 *)",
				"Table[2^IntegerExponent[2n, 2] EulerE[2 n-1, n], {n,1,15}] (* _Peter Luschny_, Sep 22 2017 *)"
			],
			"program": [
				"(PARI) a(n) = numerator(subst(eulerpol(2*n-1, 'x), 'x, n)); \\\\ _Michel Marcus_, Sep 21 2021"
			],
			"xref": [
				"Cf. A002425, A006519, A007814, A157805, A292706."
			],
			"keyword": "nonn,frac",
			"offset": "1,2",
			"author": "_Vladimir Shevelev_, Sep 22 2017",
			"ext": [
				"More terms from _Peter J. C. Moses_, Sep 22 2017"
			],
			"references": 7,
			"revision": 44,
			"time": "2021-09-21T06:35:07-04:00",
			"created": "2017-09-23T03:20:03-04:00"
		}
	]
}