{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A074323",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 74323,
			"data": "1,1,3,2,6,4,12,8,24,16,48,32,96,64,192,128,384,256,768,512,1536,1024,3072,2048,6144,4096,12288,8192,24576,16384,49152,32768,98304,65536,196608,131072,393216,262144,786432,524288,1572864,1048576",
			"name": "Coefficient of the highest power of q in the expansion of nu(0)=1, nu(1)=b and for n\u003e=2, nu(n)=b*nu(n-1)+lambda*(n-1)_q*nu(n-2) with (b,lambda)=(1,2), where (n)_q=(1+q+...+q^(n-1)) and q is a root of unity.",
			"comment": [
				"Instead of listing the coefficients of the highest power of q in each nu(n), if we list the coefficients of the smallest power of q (i.e., constant terms), we get a weighted Fibonacci numbers described by f(0)=1, f(1)=1, for n\u003e=2, f(n)=f(n-1)+2f(n-2).",
				"The highest powers are given by the quarter-squares A002620(n-1). - _Paul Barry_, Mar 11 2007"
			],
			"link": [
				"M. Beattie, S. Dăscălescu and S. Raianu, \u003ca href=\"https://arxiv.org/abs/math/0204075\"\u003eLifting of Nichols Algebras of Type B_2\u003c/a\u003e, arXiv:math/0204075 [math.QA], 2002.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,2)."
			],
			"formula": [
				"For given b and lambda, the recurrence relation is given by; t(0)=1, t(1)=b, t(2)=b^2+lambda and for n\u003e=3, t(n)=lambda*t(n-2).",
				"G.f.: (1+x+x^2)/(1-2*x^2); a(n)=2^floor(n/2)+2^((n-2)/2)*(1+(-1)^n)/2-0^n/2. - _Paul Barry_, Mar 11 2007",
				"a(0)=0, a(2n+1) = A000079, a(2n+2) = 3a(2n+1). a(2n)-a(2n+1) = A131577. - _Paul Curtz_, Mar 05 2008",
				"a(2n+1) = 2^n = A000079(n), a(2n+2) = 3*A000079(n). Also a(2n)-a(2n+1) = A131577. a(2n+1)-a(2n)=2^n for n\u003e0. - _Paul Curtz_, Apr 09 2008",
				"a(n+1) = A010684(n)*A016116(n). - _R. J. Mathar_, Jul 08 2009"
			],
			"example": [
				"nu(0)=1;",
				"nu(1)=1;",
				"nu(2)=3;",
				"nu(3)=5+2q;",
				"nu(4)=11+8q+6q^2;",
				"nu(5)=21+22q+20q^2+14q^3+4q^4;",
				"nu(6)=43+60q+70q^2+64q^3+54q^4+28q^5+12q^6;",
				"by listing the coefficients of the highest power in each nu(n), we get 1,1,3,2,6,4,12,..."
			],
			"mathematica": [
				"Join[{1}, LinearRecurrence[{0, 2}, {1, 3}, 41]] (* _Jean-François Alcover_, Sep 22 2017 *)"
			],
			"xref": [
				"Cf. A001045."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "Y. Kelly Itakura (yitkr(AT)mta.ca), Aug 21 2002",
			"ext": [
				"More terms from _Paul Barry_, Mar 11 2007"
			],
			"references": 7,
			"revision": 30,
			"time": "2017-09-22T12:31:51-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}