{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A156995",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 156995,
			"data": "2,1,2,2,4,2,6,12,9,2,24,48,40,16,2,120,240,210,100,25,2,720,1440,1296,672,210,36,2,5040,10080,9240,5040,1764,392,49,2,40320,80640,74880,42240,15840,4032,672,64,2,362880,725760,680400,393120,154440,42768",
			"name": "Triangle T(n, k) = 2*n*binomial(2*n-k, k)*(n-k)!/(2*n-k), with T(0, 0) = 2, read by rows.",
			"comment": [
				"For n\u003e=1, o.g.f. of n-th row is a polynomial p(x,n) = Sum_{k=0..n} ( 2*n*(n-k)! * binomial(2*n-k, k)/(2*n-k)) * x^k. These polynomials are hit polynomials for the reduced ménage problem (Riordan 1958)."
			],
			"reference": [
				"J. Riordan, An Introduction to Combinatorial Analysis, Wiley, 1958, pp. 197-199"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A156995/b156995.txt\"\u003eRows n = 0..50 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = 2*n*binomial(2*n-k, k)*(n-k)!/(2*n-k), with T(0, 0) = 2."
			],
			"example": [
				"Triangle starts with:",
				"  n=0:     2;",
				"  n=1:     1,     2;",
				"  n=2:     2,     4,     2;",
				"  n=3:     6,    12,     9,     2;",
				"  n=4:    24,    48,    40,    16,     2;",
				"  n=5:   120,   240,   210,   100,    25,    2;",
				"  n=6:   720,  1440,  1296,   672,   210,   36,   2;",
				"  n=7:  5040, 10080,  9240,  5040,  1764,  392,  49,  2;",
				"  n=8: 40320, 80640, 74880, 42240, 15840, 4032, 672, 64, 2;",
				"  ..."
			],
			"mathematica": [
				"T[n_, k_]:= If[n==0, 2, 2*n*Binomial[2*n-k, k]*(n-k)!/(2*n-k)];",
				"Table[T[n, k], {n,0,10}, {k,0,n}]//Flatten (* modified by _G. C. Greubel_, May 14 2021 *)"
			],
			"program": [
				"(MAGMA)",
				"A156995:= func\u003c n,k | n eq 0 select 2 else 2*n*Factorial(n-k)*Binomial(2*n-k, k)/(2*n-k) \u003e;",
				"[A156995(n,k): k in [0..n], n in [0..12]]; // _G. C. Greubel_, May 14 2021",
				"(Sage)",
				"def A156995(n,k): return 2 if (k==n) else 2*n*factorial(n-k)*binomial(2*n-k,k)/(2*n-k)",
				"flatten([[A156995(n,k) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, May 14 2021"
			],
			"xref": [
				"Row sums are A300484."
			],
			"keyword": "nonn,tabl",
			"offset": "0,1",
			"author": "_Roger L. Bagula_, Feb 20 2009",
			"ext": [
				"Edited and changed T(0,0) = 2 (to make formula continuous and constant along the diagonal k = n) by _Max Alekseyev_, Mar 06 2018"
			],
			"references": 5,
			"revision": 13,
			"time": "2021-05-15T01:22:39-04:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}