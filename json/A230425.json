{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A230425",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 230425,
			"data": "0,0,0,1,1,0,2,0,1,1,0,3,0,1,1,2,2,0,1,1,1,1,1,0,2,0,1,1,0,3,0,1,1,2,3,0,1,1,2,2,0,1,1,1,1,1,0,3,0,1,1,2,0,3,1,1,3,0,2,1,1,2,3,0,1,1,1,1,1,2,0,3,1,1,0,5,2,1,1,0,4,2,1,1,2,0,3",
			"name": "a(n)=0 if n is in the infinite trunk of factorial beanstalk (in A219666), otherwise 1 + number of steps to reach the farthest leaf in that finite branch of the beanstalk.",
			"comment": [
				"This sequence relates to the factorial base representation (A007623) in the same way as A213725 relates to the binary system."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A230425/b230425.txt\"\u003eTable of n, a(n) for n = 0..10080\u003c/a\u003e"
			],
			"formula": [
				"If A230412(n)=0, a(n)=1; otherwise, if n is in A219666, a(n)=0; otherwise a(n) = 1+max(a(A230423(n)), a(A230424(n)))."
			],
			"example": [
				"From 11 sprouts the following finite side-tree of \"factorial beanstalk\":",
				"    18  19",
				"     \\  /",
				"  14  15",
				"   \\  /",
				"    11",
				"Its leaves are the numbers 14, 18 and 19 (which all occur in A219658), whose factorial base representations (see A007623) are '210', '300' and '301' respectively. The corresponding parent nodes are obtained by subtracting the sum of factorial base digits, thus we get 18-3 = 15 and also 19-4 = 15, thus 15 ('211' in factorial base) is the parent of 18 and 19. For 14 and 15 we get 14-3 = 15-4 = 11, thus 11 is the parent of both 14 and 15, and the common ancestor of all the numbers 11, 14, 15, 18 and 19.",
				"For numbers not occurring in A219666 this sequence gives one more than the maximum number of steps to reach the most distant leaf in such subtrees. In the above case, there is from 11 only one step to 14, but two steps to both 18 and 19. Thus a(11)=2+1=3. For leaves the result is always 1, for example, a(14) = a(18) = a(19) = 1."
			],
			"program": [
				"(Scheme with memoization-macro definec from _Antti Karttunen_'s IntSeq-library)",
				"(definec (A230425 n) (cond ((zero? (A230412 n)) 1) ((inA219666? n) 0) (else (1+ (max (A230425 (A230423 n)) (A230425 (A230424 n)))))))",
				"(define (inA219666? n) (or (zero? n) (= 1 (- (A230418 (1+ n)) (A230418 n)))))"
			],
			"xref": [
				"Differs from A230426 for the first time at n=34, where a(n)=3, while A230426(34)=4. Cf. also A230427.",
				"A219658 gives the position of ones in this sequence (which are the leaves of the tree)."
			],
			"keyword": "nonn",
			"offset": "0,7",
			"author": "_Antti Karttunen_, Nov 10 2013",
			"references": 6,
			"revision": 11,
			"time": "2013-11-21T05:18:19-05:00",
			"created": "2013-11-21T05:18:19-05:00"
		}
	]
}