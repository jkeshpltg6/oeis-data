{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A046682",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 46682,
			"data": "1,1,1,2,3,4,6,8,12,16,22,29,40,52,69,90,118,151,195,248,317,400,505,632,793,985,1224,1512,1867,2291,2811,3431,4186,5084,6168,7456,9005,10836,13026,15613,18692,22316,26613,31659,37619,44601,52815,62416,73680,86809,102162",
			"name": "Number of cycle types of conjugacy classes of all even permutations of n elements.",
			"comment": [
				"Also number of partitions of n with even number of even parts. There is no restriction on the odd parts.",
				"a(n) = u(n) + v(n), n \u003e= 2, of the Osima reference, p. 383.",
				"Also number of partitions of n with largest part congruent to n modulo 2: a(2*n) = A027187(2*n), a(2*n-1) = A027193(2*n-1); a(n) = A000041(n) - A000701(n). - _Reinhard Zumkeller_, Apr 22 2006",
				"Equivalently, number of partitions of n with number of parts having the same parity as n. - _Olivier Gérard_, Apr 04 2012",
				"Also number of distinct free Young diagrams (Ferrers graphs with n nodes). Free Young diagrams are distinct when none is a rigid transformation (translation, rotation, reflection or glide reflection) of another. - _Jani Melik_, May 08 2016",
				"Let the cycle type of an even permutation be represented by the partition A=(O1,O2,...,Oi,E1,E2,...,E2j), where the Os are parts with odd length and the Es are parts with even lengths, and where j may be zero, using Reinhard Zumkeller's observation that the partition associated with a cycle type of an even permutation has an even number of even parts. The set of even cycle types enumerated here can be considered a monoid under a binary operation *: Let A be as above and B=(o1,o2,...,ok,e1,e2,...,e2m). A*B is the partition (O1o1,O1o2,...,O1ok,O1e1,...,O1e2m,O2o1,...,O2e2m,...,Oio1,...,Oie2m,E1o1,...,E1e2m,...,E2je2m). This product has 2im+2jk+4jm even parts, so it represents the cycle type of an even permutation. - _Richard Locke Peterson_, Aug 20 2018"
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A046682/b046682.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e (terms 0..1000 from T. D. Noe)",
				"George E. Andrews, David Newman, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL23/Andrews/andrews5.html\"\u003eThe Minimal Excludant in Integer Partitions\u003c/a\u003e, J. Int. Seq., Vol. 23 (2020), Article 20.2.3.",
				"J. Huh and B. Kim, \u003ca href=\"https://doi.org/10.1142/S1793042120500475\"\u003eThe number of equivalence classes arising from partition involutions\u003c/a\u003e, Int. J. Number Theory, 16 (2020), 925-939.",
				"M. Osima, \u003ca href=\"http://dx.doi.org/10.4153/CJM-1952-034-x\"\u003eOn the irreducible representations of the symmetric group\u003c/a\u003e, Canad. J. Math., 4 (1952), 381-384.",
				"Sheila Sundaram, \u003ca href=\"https://arxiv.org/abs/1808.01416\"\u003eOn a positivity conjecture in the character table of S_n\u003c/a\u003e, arXiv:1808.01416 [math.CO], 2018."
			],
			"formula": [
				"G.f.: Sum_{n\u003e=0} (-q^2)^(n^2) / Product_{m\u003e=1} (1-q^m ) = ( 1/Product_{m\u003e=1} (1-q^m) + Product_{m\u003e=1} (1+q^(2*m-1) ) ) / 2. - _Mamuka Jibladze_, Sep 07 2003",
				"a(n) = (A000041(n) + A000700(n)) / 2."
			],
			"example": [
				"1 + x + x^2 + 2*x^3 + 3*x^4 + 4*x^5 + 6*x^6 + 8*x^7 + 12*x^8 + 16*x^9 + ...",
				"a(3)=2 since cycle types of even permutations of 3 elements is (.)(.)(.), (...).",
				"a(4)=3 since cycle types of even permutations of 4 elements is (.)(.)(.)(.), (...)(.), (..)(..).",
				"a(5)=4 (free Young diagrams):",
				"  XXXXX XXXX. XXX.. XXX..",
				"  ..... X.... XX... X....",
				"  ..... ..... ..... X....",
				"  ..... ..... ..... .....",
				"  ..... ..... ..... ....."
			],
			"maple": [
				"seq(add((-1)^(n-k)*combinat:-numbpart(n,k),k=0..n),n=0..48); # _Peter Luschny_, Aug 03 2015"
			],
			"mathematica": [
				"max = 48; f[q_] := Sum[(-q^2)^n^2, {n, 0, max}]/Product[1-q^n, {n, 1, max}]; CoefficientList[ Series[f[q], {q, 0, max}], q] (* _Jean-François Alcover_, Oct 18 2011, after g.f. *)"
			],
			"program": [
				"(PARI) list(lim)=my(q='q);Vec(sum(n=0,sqrt(lim),(-q^2)^(n^2))/prod(n=1,lim,1-q^n)+O(q^(lim\\1+1))) \\\\ _Charles R Greathouse IV_, Oct 18 2011",
				"(PARI) {a(n) = if( n\u003c0, 0, (numbpart(n) + polcoeff( 1 / prod( k=1, n, 1 + (-x)^k, 1 + x * O(x^n)), n)) / 2)} /* _Michael Somos_, Jul 24 2012 */"
			],
			"xref": [
				"Cf. A000041, A000700, A000701, A006950, A015128.",
				"For the number of conjugacy classes of the alternating group A_n, n\u003e=2, see A000702.",
				"Cf. A118301."
			],
			"keyword": "nonn,nice",
			"offset": "0,4",
			"author": "_Vladeta Jovovic_",
			"references": 26,
			"revision": 89,
			"time": "2020-07-03T06:23:07-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}