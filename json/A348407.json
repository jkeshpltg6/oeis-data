{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A348407",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 348407,
			"data": "4,9,21,47,105,231,505,1095,2361,5063,10809,22983,48697,102855,216633,455111,953913,1995207,4165177,8679879,18058809,37515719,77827641,161247687,333680185,689729991,1424199225,2937876935,6054710841,12467335623,25650499129,52732654023,108328619577",
			"name": "a(n) = ((n+1)*3*2^(n+1) + 29*2^n + (-1)^n)/9.",
			"comment": [
				"The ratio (count of ones)/(count of zeros) in the binary expansion of a(n) is \u003e 1/2 and \u003c= 5 for all n \u003e 0, this is because the division by 9 adds a repeating pattern 111000... after some binary digits.",
				"This sequence has in its \"partial binomial transform\" (see formula section) no other constants than 2 and 1 despite of its more complicated looking closed form expression. This transform has a deep connection to the Grünwald-Letnikov fractional derivative if we replace the order of the derivative with the variable x: D^x*f(x)."
			],
			"link": [
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,0,-4)."
			],
			"formula": [
				"a(n) = round(((n+1)*3*2^(n+1) + 29*2^n)/9).",
				"a(n) = 2^(n+2) + A113861(n).",
				"a(n) = 2^(n+2) + n*2^n - A045883(n) = 2^(n+2) + n*2^n - round(((3*n+1)*2^n)/9).",
				"a(n+1) - 2*a(n) = A001045(n+2).",
				"a(n) = A034007(n+3) + A045883(n-1) for n \u003e 0.",
				"A partial binomial transform in two parts:",
				"(Partial means a diagonal in a difference table a(0), a(2)-a(1), .. . This is partial because one diagonal alone is no invertible transform.)",
				"A001787(n+2) = Sum_{k=0..n}(-1)^(n-k)*binomial(n, k)*a(2*n-k)",
				"             = (n+2)*2^(n+1).",
				"A052951(n+1) = Sum_{k=0..n}(-1)^(n-k)*binomial(n, k)*(a(1+2*n-k) - a(2*n-k))",
				"             = (n+2)*2^(n+1) + 2^n.",
				"The inverse transform:",
				"a(n+1) = Sum_{k=0..floor(n/2)}*binomial(n-k, k)*(k+2)*2^(k+1)",
				"       + Sum_{k=0..floor((n-1)/2)}*binomial(n-k-1, k)*((k+2)*2^(k+1) + 2^k).",
				"From _Stefano Spezia_, Oct 20 2021: (Start)",
				"G.f.: (4 - 3*x - 6*x^2)/((1 + x)*(1 - 2*x)^2).",
				"a(n) = 3*a(n-1) - 4*a(n-3) for n \u003e 2. (End)"
			],
			"mathematica": [
				"Array[((# + 1)*3*2^(# + 1) + 29*2^# + (-1)^#)/9 \u0026, 33, 0] (* _Michael De Vlieger_, Oct 19 2021 *)"
			],
			"xref": [
				"Cf. A001045, A001787, A034007, A045883, A052951, A113861, A348405."
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "_Paul Curtz_ and _Thomas Scheuerle_, Oct 17 2021",
			"references": 1,
			"revision": 64,
			"time": "2021-12-09T01:24:09-05:00",
			"created": "2021-12-09T01:24:09-05:00"
		}
	]
}