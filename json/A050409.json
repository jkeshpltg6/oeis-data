{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A050409",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 50409,
			"data": "0,5,29,86,190,355,595,924,1356,1905,2585,3410,4394,5551,6895,8440,10200,12189,14421,16910,19670,22715,26059,29716,33700,38025,42705,47754,53186,59015,65255,71920,79024,86581,94605,103110,112110,121619",
			"name": "Truncated square pyramid numbers: a(n) = Sum_{k = n..2*n} k^2.",
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A050409/b050409.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"M. Janjic and B. Petkovic, \u003ca href=\"http://arxiv.org/abs/1301.4550\"\u003eA Counting Function\u003c/a\u003e, arXiv preprint arXiv:1301.4550 [math.CO], 2013. - From _N. J. A. Sloane_, Feb 13 2013",
				"M. Janjic, B. Petkovic, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Janjic/janjic45.html\"\u003eA Counting Function Generalizing Binomial Coefficients and Some Other Classes of Integers\u003c/a\u003e, J. Int. Seq. 17 (2014) # 14.3.5.",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-6,4,-1)."
			],
			"formula": [
				"a(n) = n*(n+1)*(14*n+1)/6.",
				"a(n) = A132121(n,4) for n\u003e3. - _Reinhard Zumkeller_, Aug 12 2007",
				"From _Bruno Berselli_, Feb 11 2011: (Start)",
				"G.f.: x*(5+9*x)/(1-x)^4.",
				"a(n) = A129371(2*n). (End)",
				"a(n) = 4*a(n-1) - 6*a(n-2) + 4*a(n-3) - a(n-4). - _Vincenzo Librandi_, Jun 22 2012",
				"E.g.f.: x*(30 + 57*x + 14*x^2)*exp(x)/6. - _G. C. Greubel_, Oct 30 2019"
			],
			"maple": [
				"seq(add((n+k)^2,k=0..n),n=0..40); # _Zerinvary Lajos_, Dec 01 2006"
			],
			"mathematica": [
				"LinearRecurrence[{4,-6,4,-1},{0,5,29,86},40] (* _Vincenzo Librandi_, Jun 22 2012 *)",
				"Table[(n(n+1)(14n+1))/6,{n,0,40}] (* _Harvey P. Dale_, Mar 08 2020 *)"
			],
			"program": [
				"(MAGMA) [\u0026+[k^2: k in [n..2*n]]: n in [0..40]]; // _Bruno Berselli_, Feb 11 2011",
				"(MAGMA) I:=[0, 5, 29, 86]; [n le 4 select I[n] else 4*Self(n-1)-6*Self(n-2)+4*Self(n-3)-Self(n-4): n in [1..40]]; // _Vincenzo Librandi_, Jun 22 2012",
				"(PARI) a(n)=sum(k=n,n+n,k^2)",
				"(PARI) vector(40, n, n*(n-1)*(14*n-13)/6) \\\\ _G. C. Greubel_, Oct 30 2019",
				"(Sage) [n*(n+1)*(14*n+1)/6 for n in (0..40)] # _G. C. Greubel_, Oct 30 2019",
				"(GAP) List([0..40], n-\u003e n*(n+1)*(14*n+1)/6); # _G. C. Greubel_, Oct 30 2019"
			],
			"xref": [
				"Cf. A000330, A033994, A129371, A132112, A132121, A132124.",
				"Cf. A225144. [_Bruno Berselli_, Jun 06 2013]",
				"Cf. A045943: Sum_{k = n..2*n} k.",
				"Cf. A304993: Sum_{k = n..2*n} k*(k+1)/2."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,2",
			"author": "Klaus Strassburger (strass(AT)ddfi.uni-duesseldorf.de), Dec 22 1999",
			"references": 13,
			"revision": 52,
			"time": "2020-03-08T17:54:59-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}