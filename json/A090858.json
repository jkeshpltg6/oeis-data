{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A090858",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 90858,
			"data": "0,0,1,0,2,2,2,4,6,7,8,13,15,21,25,30,39,50,58,74,89,105,129,156,185,221,264,309,366,433,505,593,696,805,941,1090,1258,1458,1684,1933,2225,2555,2922,3346,3823,4349,4961,5644,6402,7267,8234,9309,10525,11886,13393",
			"name": "Number of partitions of n such that there is exactly one part which occurs twice, while all other parts occur only once.",
			"comment": [
				"Number of solutions (p(1),p(2),...,p(n)), p(i)\u003e=0,i=1..n, to p(1)+2*p(2)+...+n*p(n)=n such that |{i: p(i)\u003c\u003e0}| = p(1)+p(2)+...+p(n)-1.",
				"Also number of partitions of n such that if k is the largest part, then, with exactly one exception, all the integers 1,2,...,k occur as parts. Example: a(7)=4 because we have [4,2,1], [3,3,1], [3,2,2] and [3,1,1,1,1]. - _Emeric Deutsch_, Apr 18 2006"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A090858/b090858.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e"
			],
			"formula": [
				"G.f.: Sum_{k\u003e0} x^(2*k)/(1+x^k) * Product_{k\u003e0} (1+x^k). Convolution of 1-A048272(n) and A000009(n). a(n) = A036469(n) - A015723(n).",
				"G.f.: sum(x^(k(k+1)/2)[(1-x^k)/x^(k-1)/(1-x)-k]/product(1-x^i,i=1..k), k=1..infinity). - _Emeric Deutsch_, Apr 18 2006",
				"a(n) ~ c * exp(Pi*sqrt(n/3)) / n^(1/4), where c = 3^(1/4) * (1 - log(2)) / (2*Pi) = 0.064273294789... - _Vaclav Kotesovec_, May 24 2018"
			],
			"example": [
				"a(7) = 4 because we have 4 such partitions of 7: [1,1,2,3], [1,1,5], [2,2,3], [1,3,3].",
				"From _Gus Wiseman_, Apr 19 2019: (Start)",
				"The a(2) = 1 through a(11) = 13 partitions described in the name are the following (empty columns not shown). The Heinz numbers of these partitions are given by A060687.",
				"  (11)  (22)   (221)  (33)   (322)   (44)    (441)   (55)    (443)",
				"        (211)  (311)  (411)  (331)   (332)   (522)   (433)   (533)",
				"                             (511)   (422)   (711)   (442)   (551)",
				"                             (3211)  (611)   (3321)  (622)   (722)",
				"                                     (3221)  (4221)  (811)   (911)",
				"                                     (4211)  (4311)  (5221)  (4322)",
				"                                             (5211)  (5311)  (4331)",
				"                                                     (6211)  (4421)",
				"                                                             (5411)",
				"                                                             (6221)",
				"                                                             (6311)",
				"                                                             (7211)",
				"                                                             (43211)",
				"The a(2) = 1 through a(10) = 8 partitions described in Emeric Deutsch's comment are the following (empty columns not shown). The Heinz numbers of these partitions are given by A325284.",
				"  (2)  (22)  (32)   (222)   (322)    (332)     (432)      (3322)",
				"       (31)  (311)  (3111)  (331)    (431)     (3222)     (3331)",
				"                            (421)    (2222)    (4221)     (22222)",
				"                            (31111)  (3311)    (4311)     (42211)",
				"                                     (4211)    (33111)    (43111)",
				"                                     (311111)  (42111)    (331111)",
				"                                               (3111111)  (421111)",
				"                                                          (31111111)",
				"(End)"
			],
			"maple": [
				"g:=sum(x^(k*(k+1)/2)*((1-x^k)/x^(k-1)/(1-x)-k)/product(1-x^i,i=1..k),k=1..15): gser:=series(g,x=0,64): seq(coeff(gser,x,n),n=1..54); # _Emeric Deutsch_, Apr 18 2006",
				"# second Maple program:",
				"b:= proc(n, i, t) option remember; `if`(n\u003ei*(i+3-2*t)/2, 0,",
				"     `if`(n=0, t, b(n, i-1, t)+`if`(i\u003en, 0, b(n-i, i-1, t)+",
				"     `if`(t=1 or 2*i\u003en, 0, b(n-2*i, i-1, 1)))))",
				"    end:",
				"a:= n-\u003e b(n$2, 0):",
				"seq(a(n), n=0..100);  # _Alois P. Heinz_, Dec 28 2015"
			],
			"mathematica": [
				"b[n_, i_, t_] := b[n, i, t] = If[n \u003e i*(i + 3 - 2*t)/2, 0, If[n == 0, t, b[n, i - 1, t] + If[i \u003e n, 0,  b[n - i, i - 1, t] + If[t == 1 || 2*i \u003e n, 0, b[n - 2*i, i - 1, 1]]]]]; a[n_] := b[n, n, 0]; Table[a[n], {n, 0, 100} ] (* _Jean-François Alcover_, Jan 20 2016, after _Alois P. Heinz_ *)",
				"Table[Length[Select[IntegerPartitions[n],Length[#]-Length[Union[#]]==1\u0026]],{n,0,30}] (* _Gus Wiseman_, Apr 19 2019 *)"
			],
			"program": [
				"(PARI) alist(n)=concat([0,0],Vec(sum(k=1,n\\2,(x^(2*k)+x*O(x^n))/(1+x^k)*prod(j=1,n-2*k,1+x^j+x*O(x^n))))) \\\\ _Franklin T. Adams-Watters_, Nov 02 2015"
			],
			"xref": [
				"Cf. A047967, A265251.",
				"Column k=2 of A266477.",
				"Cf. A008284, A046660, A060687, A116608, A117571, A127002, A325241, A325244."
			],
			"keyword": "easy,nonn",
			"offset": "0,5",
			"author": "_Vladeta Jovovic_, Feb 12 2004",
			"ext": [
				"More terms from Pab Ter (pabrlos(AT)yahoo.com), May 26 2004",
				"a(0) added by _Franklin T. Adams-Watters_, Nov 02 2015"
			],
			"references": 21,
			"revision": 36,
			"time": "2019-04-21T07:44:57-04:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}