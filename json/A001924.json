{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001924",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1924,
			"id": "M2645 N1053",
			"data": "0,1,3,7,14,26,46,79,133,221,364,596,972,1581,2567,4163,6746,10926,17690,28635,46345,75001,121368,196392,317784,514201,832011,1346239,2178278,3524546,5702854,9227431,14930317,24157781,39088132,63245948,102334116,165580101",
			"name": "Apply partial sum operator twice to Fibonacci numbers.",
			"comment": [
				"Leading coefficients in certain rook polynomials (for n\u003e=2; see p. 18 of the Riordan paper). - _Emeric Deutsch_, Mar 08 2004",
				"A107909(a(n)) = A000225(n) = 2^n - 1. - _Reinhard Zumkeller_, May 28 2005",
				"(1, 3, 7, 14, ...) = row sums of triangle A141289. - _Gary W. Adamson_, Jun 22 2008",
				"a(n) is the number of nonempty subsets of {1,2,...,n} such that the difference of successive elements is at most 2.  See example below.  Generally, the o.g.f. for the number of nonempty subsets of {1,2,...,n} such that the difference of successive elements is \u003c= k is: x/((1-x)*(1-2*x+x^(k+1)). Cf.A000217 the case for k=1, A001477 the case for k=0 (counts singleton subsets). - _Geoffrey Critzer_, Feb 17 2012",
				"-fibonacci(n-2) = p(-1) where p(x) is the unique degree-n polynomial such that p(k) = a(k) for k = 0, 1, ..., n. - _Michael Somos_, Dec 31 2012",
				"a(n) is the number of bit strings of length n+1 with the pattern 00 and without the pattern 011, see example. - _John M. Campbell_, Feb 10 2013",
				"a(n) = A228074(n+1,3) for n \u003e 1. - _Reinhard Zumkeller_, Aug 15 2013"
			],
			"reference": [
				"J. Riordan, Discordant permutations, Scripta Math., 20 (1954), 14-23.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A001924/b001924.txt\"\u003eTable of n, a(n) for n=0..500\u003c/a\u003e",
				"Bader AlBdaiwi, \u003ca href=\"https://arxiv.org/abs/1603.01807\"\u003eOn the Number of Cycles in a Graph\u003c/a\u003e, arXiv preprint arXiv:1603.01807 [cs.DM], 2016.",
				"J.-L. Baril and J.-M. Pallo, \u003ca href=\"http://jl.baril.u-bourgogne.fr/Motzkin.pdf\"\u003eMotzkin subposet and Motzkin geodesics in Tamari lattices\u003c/a\u003e, 2013.",
				"N-N. Cao and F-Z. Zhao, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL13/Cao2/cao5r.html\"\u003eSome Properties of Hyperfibonacci and Hyperlucas Numbers\u003c/a\u003e, J. Int. Seq. 13 (2010) # 10.8.8.",
				"Hung Viet Chu, \u003ca href=\"https://arxiv.org/abs/2005.10081\"\u003eVarious Sequences from Counting Subsets\u003c/a\u003e, arXiv:2005.10081 [math.CO], 2020.",
				"Hung Viet Chu, \u003ca href=\"https://arxiv.org/abs/2106.03659\"\u003ePartial Sums of the Fibonacci Sequence\u003c/a\u003e, arXiv:2106.03659 [math.CO], 2021.",
				"Ligia Loretta Cristea, Ivica Martinjak, and Igor Urbiha, \u003ca href=\"http://arxiv.org/abs/1606.06228\"\u003eHyperfibonacci Sequences and Polytopic Numbers\u003c/a\u003e, arXiv:1606.06228 [math.CO], 2016.",
				"E. Kilic and P. Stanica, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL12/Kilic/kilic6.html\"\u003eGenerating matrices for weighted sums of second order linear recurrences\u003c/a\u003e, JIS 12 (2009) 09.2.7.",
				"Wolfdieter Lang, \u003ca href=\"http://www.fq.math.ca/Scanned/36-4/elementary36-4.pdf\"\u003eProblem B-858\u003c/a\u003e, Fibonacci Quarterly, 36 (1998), 373-374, \u003ca href=\"http://www.fq.math.ca/Scanned/36-4/elementary36-4.pdf\"\u003eSolution\u003c/a\u003e, ibid. 37 (1999) 183-184.",
				"Candice A. Marshall, \u003ca href=\"http://hdl.handle.net/11603/10353\"\u003eConstruction of Pseudo-Involutions in the Riordan Group\u003c/a\u003e, Dissertation, Morgan State University, 2017.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"J. Riordan, \u003ca href=\"/A000211/a000211.pdf\"\u003eDiscordant permutations\u003c/a\u003e, Scripta Math., 20 (1954), 14-23. [Annotated scanned copy]",
				"Stacey Wagner, \u003ca href=\"http://via.library.depaul.edu/depaul-disc/vol2/iss1/2\"\u003eEnumerating Alternating Permutations with One Alternating Descent\u003c/a\u003e, DePaul Discoveries: Vol. 2: Iss. 1, Article 2.",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-2,-1,1)."
			],
			"formula": [
				"From _Wolfdieter Lang_: (Start)",
				"G.f.: x/((1-x-x^2)*(1-x)^2).",
				"Convolution of natural numbers n \u003e= 1 with Fibonacci numbers F(k).",
				"a(n) = Fibonacci(n+4) - (3+n). (End)",
				"From _Henry Bottomley_, Jan 03 2003: (Start)",
				"a(n) = a(n-1) + a(n-2) + n = a(n-1) + A000071(n+2).",
				"a(n) = A001891(n) - a(n-1) = n + A001891(n-1).",
				"a(n) = A065220(n+4) + 1 = A000126(n+1) - 1. (End)",
				"a(n) = Sum_{k=0..n} Sum_{i=0..k} Fibonacci(i). - _Benoit Cloitre_, Jan 26 2003",
				"a(n) = (sqrt(5)/2+1/2)^n*(7*sqrt(5)/10+3/2)+(3/2-7*sqrt(5)/10)*(sqrt(5)/2-1/2)^n*(-1)^n-n-3. - _Paul Barry_, Mar 26 2003",
				"a(n) = Sum_{k=0..n} Fibonacci(k)*(n-k). - _Benoit Cloitre_, Jun 07 2004",
				"a(n) - a(n-1) = A101220(1,1,n). - _Ross La Haye_, May 31 2006",
				"F(n) + a(n-3) = A133640(n). - _Gary W. Adamson_, Sep 19 2007",
				"a(n) = Sum_{k=1..n} C(n-k+2,k+1), with n\u003e=0. - _Paolo P. Lava_, Apr 16 2008",
				"a(n) = A077880(-3-n) = 2*a(n-1) - a(n-3) + 1. - _Michael Somos_, Dec 31 2012",
				"INVERT transform is A122595. PSUM transform is A014162. PSUMSIGN transform is A129696. BINOMIAL transform of A039834 with 0,1 prepended is this sequence. - _Michael Somos_, Dec 31 2012",
				"a(n) = Sum_{k=0..n} Sum_{i=0..n} i * C(n-k,k-i). - _Wesley Ivan Hurt_, Sep 21 2017"
			],
			"example": [
				"a(5) = 26 because there are 31 nonempty subsets of {1,2,3,4,5} but 5 of these have successive elements that differ by 3 or more: {1,4}, {1,5}, {2,5}, {1,2,5}, {1,4,5}. - _Geoffrey Critzer_, Feb 17 2012",
				"From _John M. Campbell_, Feb 10 2013: (Start)",
				"There are a(5) = 26 bit strings with the pattern 00 and without the pattern 011 of length 5+1:",
				"   000000, 000001, 000010, 000100, 000101, 001000,",
				"   001001, 001010, 010000, 010001, 010010, 010100,",
				"   100000, 100001, 100010, 100100, 100101, 101000, 101001,",
				"   110000, 110001, 110010, 110100, 111000, 111001, 111100.",
				"(End)"
			],
			"maple": [
				"A001924:=-1/(z**2+z-1)/(z-1)**2; # Conjectured by _Simon Plouffe_ in his 1992 dissertation.",
				"##",
				"a:= n-\u003e (\u003c\u003c0|1|0|0\u003e, \u003c0|0|1|0\u003e, \u003c0|0|0|1\u003e, \u003c1|-1|-2|3\u003e\u003e^n.",
				"         \u003c\u003c0, 1, 3, 7\u003e\u003e)[1, 1]:",
				"seq(a(n), n=0..40);  # _Alois P. Heinz_, Oct 05 2012"
			],
			"mathematica": [
				"a[n_]:= Fibonacci[n+4] -3-n; Array[a, 40, 0]  (* _Robert G. Wilson v_ *)",
				"LinearRecurrence[{3,-2,-1,1},{0,1,3,7},40] (* _Harvey P. Dale_, Jan 24 2015 *)",
				"Nest[Accumulate,Fibonacci[Range[0,40]],2] (* _Harvey P. Dale_, Jun 15 2016 *)"
			],
			"program": [
				"(PARI) a(n)=fibonacci(n+4)-n-3 \\\\ _Charles R Greathouse IV_, Feb 24 2011",
				"(Haskell)",
				"a001924 n = a001924_list !! n",
				"a001924_list = drop 3 $ zipWith (-) (tail a000045_list) [0..]",
				"-- _Reinhard Zumkeller_, Nov 17 2013",
				"(MAGMA) [Fibonacci(n+4)-(n+3): n in [0..40]]; // _Vincenzo Librandi_, Jun 23 2016",
				"(Sage) [fibonacci(n+4) -n-3 for n in (0..40)] # _G. C. Greubel_, Jul 08 2019",
				"(GAP) List([0..40], n-\u003e Fibonacci(n+4) -n-3) # _G. C. Greubel_, Jul 08 2019"
			],
			"xref": [
				"Cf. A000045, A001891, A133640, A141289.",
				"Right-hand column 4 of triangle A011794.",
				"Cf. A014162, A039834, A077880, A122595, A129696.",
				"Cf. A065220."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Description improved by _N. J. A. Sloane_, Jan 01 1997"
			],
			"references": 63,
			"revision": 167,
			"time": "2021-06-08T07:28:46-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}