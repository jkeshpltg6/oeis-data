{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A109001",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 109001,
			"data": "1,1,1,1,6,1,1,15,23,1,1,28,102,60,1,1,45,290,402,125,1,1,66,655,1596,1167,226,1,1,91,1281,4795,6155,2793,371,1,1,120,2268,12040,23750,18888,5852,568,1,1,153,3732,26628,74574,91118,49380,11124,825,1,1,190,5805,53544,201810,350196,291410,114600,19629,1150,1",
			"name": "Triangle, read by rows, where g.f. of row n equals the product of (1-x)^n and the g.f. of the coordination sequence for root lattice B_n, for n \u003e= 0.",
			"comment": [
				"Compare to triangle A108558, where row n equals the (n+1)-th differences of the crystal ball sequence for D_n lattice."
			],
			"link": [
				"Muniru A Asiru, \u003ca href=\"/A109001/b109001.txt\"\u003eRows n=0..100 of triangle, flattened\u003c/a\u003e",
				"R. Bacher, P. de la Harpe and B. Venkov, \u003ca href=\"http://archive.numdam.org/item/AIF_1999__49_3_727_0\"\u003eSéries de croissance et séries d'Ehrhart associées aux réseaux de racines\u003c/a\u003e, C. R. Acad. Sci. Paris, 325 (Series 1) (1997), 1137-1142."
			],
			"formula": [
				"T(n, k) = C(2*n+1, 2*k) - 2*n*C(n-1, k-1).",
				"Row sums are 2^n*(2^n - n) for n \u003e= 0.",
				"G.f. for coordination sequence of B_n lattice: (Sum_{i=0..n} binomial(2*n+1, 2*i)*z^i) - 2*n*z*(1+z)^(n-1))/(1-z)^n. [Bacher et al.]"
			],
			"example": [
				"G.f.s of initial rows of square array A108998 are:",
				"  (1),",
				"  (1 + x)/(1-x),",
				"  (1 + 6*x + x^2)/(1-x)^2;",
				"  (1 + 15*x + 23*x^2 + x^3)/(1-x)^3;",
				"  (1 + 28*x + 102*x^2 + 60*x^3 + x^4)/(1-x)^4.",
				"Triangle begins:",
				"  1;",
				"  1,   1;",
				"  1,   6,    1;",
				"  1,  15,   23,     1;",
				"  1,  28,  102,    60,     1;",
				"  1,  45,  290,   402,   125,     1;",
				"  1,  66,  655,  1596,  1167,   226,     1;",
				"  1,  91, 1281,  4795,  6155,  2793,   371,     1;",
				"  1, 120, 2268, 12040, 23750, 18888,  5852,   568,   1;",
				"  1, 153, 3732, 26628, 74574, 91118, 49380, 11124, 825, 1;"
			],
			"mathematica": [
				"T[n_, k_] := Binomial[2n+1, 2k] - 2n * Binomial[n-1, k-1]; Table[T[n, k], {n, 0, 10}, {k, 0, n}] // Flatten (* _Amiram Eldar_, Dec 14 2018 *)"
			],
			"program": [
				"(PARI) T(n,k)=binomial(2*n+1,2*k)-2*n*binomial(n-1,k-1)",
				"(GAP) Flat(List([0..10],n-\u003eList([0..n],k-\u003eBinomial(2*n+1,2*k)-2*n*Binomial(n-1,k-1)))); # _Muniru A Asiru_, Dec 14 2018"
			],
			"xref": [
				"Cf. A108998, A108999, A109000, A022144 (row 2), A022145 (row 3), A022146 (row 4), A022147 (row 5), A022148 (row 6), A022149 (row 7), A022150 (row 8), A022151 (row 9), A022152 (row 10), A022153 (row 11), A022154 (row 12)."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Paul D. Hanna_, Jun 17 2005",
			"references": 4,
			"revision": 19,
			"time": "2018-12-25T08:36:29-05:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}