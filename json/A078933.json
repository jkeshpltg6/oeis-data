{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A078933",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 78933,
			"data": "2,5234,8158,93844,367806,421351,720114,939787,28187351,110781386,154319269,384242766,390620082,3790689201,65589428378,952764389446,12438517260105,35495694227489,53197086958290,5853886516781223",
			"name": "Good examples of Hall's conjecture: integers x such that 0 \u003c |x^3 - y^2| \u003c sqrt(x) for some integer y.",
			"comment": [
				"Hall conjectured that the nonzero difference k = x^3 - y^2 cannot be less than C x^(1/2), for a constant C. His original conjecture, probably false, has been reformulated in the following way: For any exponent e \u003c 1/2, a constant K_e \u003e 0 exists such that |x^3 - y^2| \u003e K_e x^e.",
				"Danilov found an infinite family of solutions to |x^3 - y^2| \u003c sqrt(x). For more detail see A200216. [_Artur Jasinski_, Nov 04 2011]"
			],
			"reference": [
				"Noam D. Elkies, Rational points near curves and small nonzero |x^3 - y^2| via lattice reduction. Algorithmic Number Theory. Proceedings of ANTS-IV; W. Bosma, ed.; Springer, 2000; pp. 33-63.",
				"Marshall Hall Jr., The Diophantine equation x^3 - y^2 = k, in Computers in Number Theory; A. O. L. Atkin and B. Birch, eds.; Academic Press, 1971; pp. 173-198."
			],
			"link": [
				"Ismael Jimenez Calvo, \u003ca href=\"https://web.archive.org/web/20191127011622/http://ijcalvo.galeon.com/hall.htm\"\u003eMarshall Hall's conjecture\u003c/a\u003e.",
				"Ismael Jimenez Calvo and G. Saez Moreno, \u003ca href=\"http://dx.doi.org/10.1007/3-540-45439-X_21\"\u003eApproximate Power roots in Z_m\u003c/a\u003e, Proceedings of ISC 2001 (Information Security); G. I. Davida and Y. Frankel, eds.; Springer, 2001; pp. 310-323.",
				"I. Jiminez Calvo, J. Herranz, and G. Saez, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-09-02240-6\"\u003eA new algorithm to search for small nonzero |x^3-y^2| values\u003c/a\u003e, Math. Comp. 76 (268) (2009) 2435-2444.",
				"L. V. Danilov, \u003ca href=\"http://dx.doi.org/10.1007/BF01140190\"\u003eDiophantine equation x^3-y^2-k and Hall's conjecture\u003c/a\u003e, Math. Notes Acad. Sci. USSR 32 (1982), 617-618.",
				"L. V. Danilov, \u003ca href=\"http://www.mathnet.ru/php/archive.phtml?wshow=paper\u0026amp;jrnid=mzm\u0026amp;paperid=5944\u0026amp;option_lang=eng\"\u003eLetter to the editors\u003c/a\u003e, Mat. Zametki, 36:3 (1984), 457-458.",
				"L. V. Danilov, \u003ca href=\"http://dx.doi.org/10.1007/BF01141949\"\u003eLetter to the editor\u003c/a\u003e, Mathem. Notes, 36 (3) (1984), 726.",
				"R. D'Mello, \u003ca href=\"http://arxiv.org/abs/1410.0078\"\u003eMarshall Hall's Conjecture and Gaps Between Integer Points on Mordell Elliptic Curves\u003c/a\u003e, arXiv preprint arXiv:1410.0078 [math.NT], 2014.",
				"Noam D. Elkies, \u003ca href=\"http://www.math.harvard.edu/~elkies/hall.html\"\u003eList of integers x,y with x\u003c10^18, 0 \u003c |x^3-y^2| \u003c x^(1/2).\u003c/a\u003e",
				"J. Gebel, A. Petho and H. G. Zimmer, \u003ca href=\"http://dx.doi.org/10.1023/A:1000281602647\"\u003eOn Mordell's equation\u003c/a\u003e, Compositio Math. 110 (1998), 335-367."
			],
			"example": [
				"|5234^3 - 378661^2| = 17 \u003c sqrt(5234), so 5234 is in the sequence."
			],
			"mathematica": [
				"For[x=1, True, x++, If[Abs[x^3-Round[Sqrt[x^3]]^2] \u003c Sqrt[x] \u0026\u0026 !IntegerQ[Sqrt[x]], Print[x]]]"
			],
			"xref": [
				"Cf. A179108, A179387, A200216."
			],
			"keyword": "nonn,more",
			"offset": "1,1",
			"author": "_Dean Hickerson_ and _Robert G. Wilson v_, Dec 16 2002",
			"references": 39,
			"revision": 43,
			"time": "2021-05-07T00:41:51-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}