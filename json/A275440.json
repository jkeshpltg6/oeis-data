{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A275440",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 275440,
			"data": "1,1,1,2,1,2,3,2,2,6,5,8,3,14,4,8,22,4,5,30,20,13,52,24,8,60,68,8,21,112,92,8,13,116,192,56,34,228,284,64,21,218,484,248,16,55,446,768,312,16,34,402,1132,872,144,89,848,1900,1184,160,55,730",
			"name": "Triangle read by rows: T(n,k) is the number of compositions of n into odd parts, having asymmetry degree equal to k (n\u003e=0; 0\u003c=k\u003c=floor(n/4)).",
			"comment": [
				"The asymmetry degree of a finite sequence of numbers is defined to be the number of pairs of symmetrically positioned distinct entries. Example: the asymmetry degree of (2,7,6,4,5,7,3) is 2, counting the pairs (2,3) and (6,5).",
				"Number of entries in row n is 1 + floor(n/4).",
				"Sum of entries in row n is A000045(n) (Fibonacci).",
				"T(n,0) = A053602(n) (= number of palindromic compositions of n into odd parts).",
				"Sum(k*T(n,k), k\u003e=0) = A275441(n)."
			],
			"reference": [
				"S. Heubach and T. Mansour, Combinatorics of Compositions and Words, CRC Press, 2010."
			],
			"link": [
				"K. Alladi and V. E. Hoggatt, Jr. \u003ca href=\"http://www.fq.math.ca/Scanned/13-3/alladi1.pdf\"\u003eCompositions with Ones and Twos\u003c/a\u003e, Fibonacci Quarterly, 13 (1975), 233-239.",
				"V. E. Hoggatt, Jr., and M. Bicknell, \u003ca href=\"http://www.fq.math.ca/Scanned/13-4/hoggatt1.pdf\"\u003ePalindromic compositions\u003c/a\u003e, Fibonacci Quart., Vol. 13(4), 1975, pp. 350-356."
			],
			"formula": [
				"G.f.: G(t,z) = (1-z^4)(1+z-z^2)/(1-2z^2-2tz^4+z^6). In the more general situation of compositions into a[1]\u003ca[2]\u003ca[3]\u003c..., denoting F(z) = Sum(z^{a[j]},j\u003e=1}, we have G(t,z) =(1 + F(z))/(1 - F(z^2) - t(F(z)^2 - F(z^2))). In particular, for t=0 we obtain Theorem 1.2 of the Hoggatt et al. reference."
			],
			"example": [
				"Row 5 is [3,2] because the compositions of 5 into odd parts are 5, 113, 131, 311, and 11111, having asymmetry degrees 0, 1, 0, 1, 0, respectively.",
				"Triangle starts:",
				"1;",
				"1;",
				"1;",
				"2;",
				"1,2;",
				"3,2;",
				"2,6;",
				"5,8; ."
			],
			"maple": [
				"G := (1-z^4)*(1+z-z^2)/(1-2*z^2-2*t*z^4+z^6): Gser := simplify(series(G, z = 0, 30)): for n from 0 to 25 do P[n] := sort(coeff(Gser, z, n)) end do: for n from 0 to 25 do seq(coeff(P[n], t, j), j = 0 .. degree(P[n])) end do; # yields sequence in triangular form"
			],
			"mathematica": [
				"Table[BinCounts[#, {0, 1 + Floor[n/4], 1}] \u0026@ Map[Total, Map[Map[Boole[# \u003e= 1] \u0026, BitXor[Take[(# - 1)/2, Ceiling[Length[#]/2]], Reverse@ Take[(# - 1)/2, -Ceiling[Length[#]/2]]]] \u0026, Flatten[Map[Permutations, DeleteCases[IntegerPartitions@ n, {___, a_, ___} /; EvenQ@ a]], 1]]], {n, 0, 20}] // Flatten (* _Michael De Vlieger_, Aug 17 2016 *)"
			],
			"xref": [
				"Cf. A000045, A053602, A275441."
			],
			"keyword": "nonn,tabf",
			"offset": "0,4",
			"author": "_Emeric Deutsch_, Aug 16 2016",
			"references": 1,
			"revision": 15,
			"time": "2016-08-17T13:11:43-04:00",
			"created": "2016-08-16T13:23:14-04:00"
		}
	]
}