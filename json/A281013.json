{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A281013",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 281013,
			"data": "1,2,2,1,3,2,1,1,3,1,4,2,1,1,1,2,2,1,3,1,1,3,2,4,1,5,2,1,1,1,1,2,2,1,1,3,1,1,1,3,1,2,3,2,1,4,1,1,4,2,5,1,6,2,1,1,1,1,1,2,1,2,1,1,2,2,1,1,1,2,2,2,1,3,1,1,1,1,3,1,1,2,3,1,2,1,3,2,1,1,3,2,2,3,3,1,4,1,1,1,4,1,2,4,2,1,4,3,5,1,1,5,2,6,1,7",
			"name": "Tetrangle T(n,k,i) = i-th part of k-th prime composition of n.",
			"comment": [
				"The *-product of two or more finite sequences is defined to be the lexicographically minimal sequence obtainable by shuffling them together. Every finite positive integer sequence has a unique *-factorization using prime compositions P = {(1), (2), (21), (3), (211), ...}. See A060223 and A228369 for details.",
				"These are co-Lyndon compositions, ordered first by sum and then lexicographically. - _Gus Wiseman_, Nov 15 2019"
			],
			"formula": [
				"Row lengths are A059966(n) = number of prime compositions of n."
			],
			"example": [
				"The prime factorization of (1, 1, 4, 2, 3, 1, 5, 5) is: (11423155) = (1)*(1)*(5)*(5)*(4231). The prime factorizations of the initial terms of A000002 are:",
				"             (1) = (1)",
				"            (12) = (1)*(2)",
				"           (122) = (1)*(2)*(2)",
				"          (1221) = (1)*(221)",
				"         (12211) = (1)*(2211)",
				"        (122112) = (1)*(2)*(2211)",
				"       (1221121) = (1)*(221121)",
				"      (12211212) = (1)*(2)*(221121)",
				"     (122112122) = (1)*(2)*(2)*(221121)",
				"    (1221121221) = (1)*(221)*(221121)",
				"   (12211212212) = (1)*(2)*(221)*(221121)",
				"  (122112122122) = (1)*(2)*(2)*(221)*(221121).",
				"Read as a sequence:",
				"(1), (2), (21), (3), (211), (31), (4), (2111), (221), (311), (32), (41), (5).",
				"Read as a triangle:",
				"(1)",
				"(2)",
				"(21), (3)",
				"(211), (31), (4)",
				"(2111), (221), (311), (32), (41), (5).",
				"Read as a sequence of triangles:",
				"1    2    2 1    2 1 1    2 1 1 1    2 1 1 1 1    2 1 1 1 1 1",
				"          3      3 1      2 2 1      2 2 1 1      2 1 2 1 1",
				"                 4        3 1 1      3 1 1 1      2 2 1 1 1",
				"                          3 2        3 1 2        2 2 2 1",
				"                          4 1        3 2 1        3 1 1 1 1",
				"                          5          4 1 1        3 1 1 2",
				"                                     4 2          3 1 2 1",
				"                                     5 1          3 2 1 1",
				"                                     6            3 2 2",
				"                                                  3 3 1",
				"                                                  4 1 1 1",
				"                                                  4 1 2",
				"                                                  4 2 1",
				"                                                  4 3",
				"                                                  5 1 1",
				"                                                  5 2",
				"                                                  6 1",
				"                                                  7."
			],
			"mathematica": [
				"colynQ[q_]:=Array[Union[{RotateRight[q,#],q}]=={RotateRight[q,#],q}\u0026,Length[q]-1,1,And];",
				"lexsort[f_,c_]:=OrderedQ[PadRight[{f,c}]];",
				"Table[Sort[Select[Join@@Permutations/@IntegerPartitions[n],colynQ],lexsort],{n,5}] (* _Gus Wiseman_, Nov 15 2019 *)"
			],
			"xref": [
				"Cf. A000740, A215474, A228369, A277427.",
				"The binary version is A329318.",
				"The binary non-\"co\" version is A102659.",
				"A sequence listing all Lyndon compositions is A294859.",
				"Numbers whose binary expansion is co-Lyndon are A328596.",
				"Numbers whose binary expansion is co-Lyndon are A275692.",
				"Binary Lyndon words are A001037.",
				"Lyndon compositions are A059966.",
				"Normal Lyndon words are A060223.",
				"Cf. A211097, A211100, A296372, A296373, A298941, A329131, A329312, A329313, A329314, A329324, A329326."
			],
			"keyword": "nonn,tabf",
			"offset": "1,2",
			"author": "_Gus Wiseman_, Jan 12 2017",
			"references": 16,
			"revision": 13,
			"time": "2019-11-15T21:36:36-05:00",
			"created": "2017-01-14T08:08:34-05:00"
		}
	]
}