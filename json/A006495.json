{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006495",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6495,
			"id": "M2880",
			"data": "1,1,-3,-11,-7,41,117,29,-527,-1199,237,6469,11753,-8839,-76443,-108691,164833,873121,922077,-2521451,-9653287,-6699319,34867797,103232189,32125393,-451910159,-1064447283,130656229,5583548873",
			"name": "Real part of (1 + 2*i)^n, where i is sqrt(-1).",
			"comment": [
				"Row sums of the Euler related triangle A117411. Partial sums are A006495. - _Paul Barry_, Mar 16 2006",
				"Binomial transform of [1, 0, -4, 0, 16, 0, -64, 0, 256, 0, ...], i.e. powers of -4 with interpolated zeros. - _Philippe Deléham_, Dec 02 2008",
				"The absolute values of these numbers are the odd numbers y such that x^2 + y^2 = 5^n with x and y coprime. See A098122. - _T. D. Noe_, Apr 14 2011",
				"Pisano period lengths: 1, 1, 8, 1, 4, 8, 48, 4, 24, 4, 60, 8, 12, 48, 8, 8, 16, 24, 90, 4, ... - _R. J. Mathar_, Aug 10 2012",
				"Multiplied by a signed sequence of 2's we obtain 2, -2, -6, 22, -14, -82, 234, -58, -1054, 2398, 474, -12938, ..., the Lucas V(-2,5) sequence. - _R. J. Mathar_, Jan 08 2013"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A006495/b006495.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e (a(88) onwards corrected by Sean A. Irvine, Apr 29 2019)",
				"Beata Bajorska-Harapińska, Barbara Smoleń, Roman Wituła, \u003ca href=\"https://doi.org/10.1007/s00006-019-0969-9\"\u003eOn Quaternion Equivalents for Quasi-Fibonacci Numbers, Shortly Quaternaccis\u003c/a\u003e, Advances in Applied Clifford Algebras (2019) Vol. 29, 54.",
				"G. Berzsenyi, \u003ca href=\"http://www.fq.math.ca/Scanned/15-3/berzsenyi.pdf\"\u003eGaussian Fibonacci numbers\u003c/a\u003e, Fib. Quart., 15 (1977), 233-236.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Lucas_sequence#Specific_names\"\u003eLucas sequence\u003c/a\u003e",
				"\u003ca href=\"/index/Lu#Lucas\"\u003eIndex entries for Lucas sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Ga#gaussians\"\u003eIndex entries for Gaussian integers and primes\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,-5)."
			],
			"formula": [
				"a(n) = (1/2)*((1+2*i)^n + (1-2*i)^n). - _Benoit Cloitre_, Oct 28 2002",
				"From _Paul Barry_, Mar 16 2006: (Start)",
				"G.f.: (1-x)/(1 - 2*x + 5*x^2);",
				"a(n) = 2*a(n-1) - 5*a(n-2);",
				"a(n) = 5^(n/2)*cos(n*atan(1/3) + Pi*n/4);",
				"a(n) = Sum_{k=0..n} Sum_{j=0..n-k} C(n,k-j)*C(j,n-k)}*(-4)^(n-k). (End)",
				"A000351(n) = a(n)^2 + A006496(n)^2. - Fabrice Baubet (intih(AT)free.fr), May 28 2007",
				"a(n) = upper left and lower right terms of the 2 X 2 matrix [1,-2; 2,1]^n. - _Gary W. Adamson_, Mar 28 2008",
				"a(n) = Sum_{k=0..n} A124182(n,k)*(-5)^(n-k). - _Philippe Deléham_, Nov 01 2008",
				"a(n) = Sum_{k=0..n} A098158(n,k)*(-4)^(n-k). - _Philippe Deléham_, Nov 14 2008",
				"a(n) = (4*n+5)*a(n-1) - 8*Sum_{k=1..n} a(k-1)*a(n-k) if n \u003e 0. - _Michael Somos_, Jul 23 2011",
				"E.g.f.: exp(x)*cos(2*x). - _Sergei N. Gladkovskii_, Jul 22 2012",
				"a(n) = 5^(n/2) * cos(n*arctan(2)). - _Sergei N. Gladkovskii_, Aug 13 2012",
				"G.f.: G(0)/2, where G(k)= 1 + 1/(1 - x*(4*k+1)/(x*(4*k+5) + 1/G(k+1))); (continued fraction). - _Sergei N. Gladkovskii_, May 26 2013",
				"From _Paul D. Hanna_, Mar 09 2019: (Start)",
				"G.f.: Sum_{n\u003e=0} (1 + (-1)^n*i)^n * x^n / (1 - (-1)^n*i*x)^(n+1).",
				"G.f.: Sum_{n\u003e=0} (1 - (-1)^n*i)^n * x^n / (1 + (-1)^n*i*x)^(n+1).",
				"(End)",
				"a(n) = hypergeom([1/2 - n/2, -n/2], [1/2], -4). - _Peter Luschny_, Jul 26 2020"
			],
			"example": [
				"1 + x - 3*x^2 - 11*x^3 - 7*x^4 + 41*x^5 + 117*x^6 + 29*x^7 - 527*x^8 + ..."
			],
			"maple": [
				"a := n -\u003e hypergeom([1/2 - n/2, -n/2], [1/2], -4):",
				"seq(simplify(a(n)), n=0..28); # _Peter Luschny_, Jul 26 2020"
			],
			"mathematica": [
				"Table[Re[(1+2I)^n],{n,0,29}] (* _Giovanni Resta_, Mar 28 2006 *)"
			],
			"program": [
				"(Sage) [lucas_number2(n,2,5)/2 for n in range(0,30)] # _Zerinvary Lajos_, Jul 08 2008",
				"(MAGMA) A006495:=func\u003c n | Integers()!Real((1+2*Sqrt(-1))^n) \u003e; [ A006495(n): n in [0..30] ]; // _Klaus Brockhaus_, Feb 04 2011",
				"(PARI) {a(n) = local(A); n++; if( n\u003c1, 0, A = vector(n); A[1] = 1; for( k=2, n, A[k] = (4*k + 1) * A[k-1] - 8 * sum( j=1, k-1, A[j] * A[k-j])); A[n])} /* _Michael Somos_, Jul 23 2011 */",
				"(PARI) a(n) = real( (1 + 2*I)^n ) \\\\ _Charles R Greathouse IV_, Nov 21 2014",
				"(PARI) {a(n) = my(A=1);",
				"A = sum(m=0, n+1, (1 + (-1)^m*I)^m * x^m / (1 - (-1)^m*I*x +x*O(x^n))^(m+1) ); polcoeff(A, n)} \\\\ _Paul D. Hanna_, Mar 09 2019"
			],
			"xref": [
				"Cf. A006496, A045873 (partial sums)."
			],
			"keyword": "sign,easy",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Signs from _Christian G. Bower_, Nov 15 1998",
				"Corrected by _Giovanni Resta_, Mar 28 2006"
			],
			"references": 22,
			"revision": 91,
			"time": "2020-07-26T04:29:09-04:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}