{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A207505",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 207505,
			"data": "0,0,0,0,0,0,0,0,0,0,0,1,2,3,4,5,6,7,8,0,0,1,0,1,2,3,4,5,0,0,7,0,0,1,0,0,7,6,6,6,3,2,1,0,0,1,0,4,3,2,1,1,0,0,2,1,5,4,2,0,2,1,0,2,0,0,4,2,0,1,1,7,4,1,0,4,3,0,2,0,2,1,0,0,7,1,1,3,0,1,4,3,2,2,5,2,6,1,2,5,0,4,3,2,1,0,7,6,5,5,2,5,5,1,3,3,4,1,3,0,6,0,5,3,8,3,5,4,4,4,3,2",
			"name": "Start with n, successively subtract the next digit of the resulting sequence, stop when reaching zero or less: a(n) is the absolute value of the result.",
			"comment": [
				"These numbers have been named \"miss numbers\" by _Hans Havermann_, and the individual sequences are called \"Digit trails\" by _Eric Angelini_, who asked for those which end in 0 (now listed in A207506).",
				"If we don't stop when reaching a negative number but keep going, it appears that we always do reach 0 eventually -- see A208059."
			],
			"link": [
				"H. Havermann, \u003ca href=\"/A207505/b207505.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"H. Havermann, in reply to E. Angelini, \u003ca href=\"http://list.seqfan.eu/pipermail/seqfan/2012-February/009037.html\"\u003eRe: Subtracting digits, hitting zero\u003c/a\u003e, seqfan mailing list, Feb 16 2012"
			],
			"example": [
				"35 hits 0 when successively subtracting its own \"digit-trail\":",
				"a b c",
				"35-3=32",
				"32-5=27",
				"27-3=24",
				"24-2=22",
				"22-2=20",
				"20-7=13",
				"13-2=11",
				"11-4= 7",
				"7-2= 5",
				"5-2= 3",
				"3-2= 1",
				"1-0= 1",
				"1-1= 0 \u003c- hit",
				"We get column b by reading column a digit-by-digit.",
				"So we have 35 -\u003e 32 -\u003e 27 -\u003e 24 -\u003e 22 -\u003e 20 -\u003e 13 -\u003e 11 -\u003e 7 -\u003e 5 -\u003e 3 -\u003e 1 -\u003e 1 -\u003e 0",
				"However, we may not hit 0 exactly, but reach a negative number instead. For n=11, the digit trail sequence is 11, 10, 9, 8, 8, -1, where we stop, and so a(11)=1."
			],
			"mathematica": [
				"f[n_] := Module[{x = n, l}, l = IntegerDigits[x];",
				"   While[x \u003e 0,  x = x - First[l];",
				"    l = Join[Rest[l], IntegerDigits[x]]; ]; Abs[ x]] ;",
				"Table[f[n], {n, 0, 100}] (* _Robert Price_, Apr 09 2020 *)"
			],
			"program": [
				"(PARI) A207505(n,v=0,a=[])={ v\u0026print1(n); a=Vec(Str(n)); while(n\u003e0, a=concat( vecextract(a,\"^1\"), Vec( Str( n-=eval( a[1] )))); v\u0026print1(\",\"n)); -n}"
			],
			"xref": [
				"Cf. A207506, A208059."
			],
			"keyword": "nonn,base",
			"offset": "0,13",
			"author": "_Hans Havermann_, _Eric Angelini_ and _M. F. Hasler_, Feb 18 2012",
			"ext": [
				"Edited by _N. J. A. Sloane_, Jun 01 2012"
			],
			"references": 3,
			"revision": 31,
			"time": "2020-04-09T22:25:43-04:00",
			"created": "2012-02-18T15:06:57-05:00"
		}
	]
}