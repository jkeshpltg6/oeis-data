{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A060354",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 60354,
			"data": "0,1,2,6,16,35,66,112,176,261,370,506,672,871,1106,1380,1696,2057,2466,2926,3440,4011,4642,5336,6096,6925,7826,8802,9856,10991,12210,13516,14912,16401,17986,19670,21456,23347,25346,27456,29680,32021",
			"name": "The n-th n-gonal number: a(n) = n*(n^2-3*n+4)/2.",
			"comment": [
				"Binomial transform of (0,1,0,3,0,0,0,...). - _Paul Barry_, Sep 14 2006",
				"Also the number of permutations of length n which can be sorted by a single cut-and-paste move (in the sense of Cranston, Sudborough, and West). - _Vincent Vatter_, Aug 21 2013",
				"Main diagonal of A317302. - _Omar E. Pol_, Aug 11 2018"
			],
			"link": [
				"Harry J. Smith, \u003ca href=\"/A060354/b060354.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"D. W. Cranston, I. H. Sudborough, and D. B. West, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2007.01.011\"\u003eShort proofs for cut-and-paste sorting of permutations\u003c/a\u003e, Discrete Math. 307, 22 (2007), 2866-2870.",
				"Cheyne Homberger, \u003ca href=\"http://arxiv.org/abs/1410.2657\"\u003ePatterns in Permutations and Involutions: A Structural and Enumerative Approach\u003c/a\u003e, arXiv preprint 1410.2657 [math.CO], 2014.",
				"Homberger and Vatter, \u003ca href=\"http://www.math.ufl.edu/~vatter/publications/poly-classes/\"\u003eOn the effective and automatic enumeration of polynomial permutation classes\u003c/a\u003e. [Broken link]",
				"C. Homberger, V. Vatter, \u003ca href=\"http://arxiv.org/abs/1308.4946\"\u003eOn the effective and automatic enumeration of polynomial permutation classes\u003c/a\u003e, arXiv preprint arXiv:1308.4946 [math.CO], 2013-2015.",
				"\u003ca href=\"/index/Pol#polygonal_numbers\"\u003eIndex to sequences related to polygonal numbers\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-6,4,-1)."
			],
			"formula": [
				"a(n) = (n*(n-2)^2 + n^2)/2.",
				"E.g.f.: exp(x)*x*(1+x^2/2). - _Paul Barry_, Sep 14 2006",
				"a(n) = Sum_{j=0..n-1} (binomial(0,0*j) + binomial(n-1,2)). - _Zerinvary Lajos_, Sep 04 2006 [corrected by _Jon E. Schoenfield_, Aug 11 2018]",
				"G.f.: x*(1-2*x+4*x^2)/(1-x)^4. - _R. J. Mathar_, Sep 02 2008",
				"a(n) = A057145(n,n). - _R. J. Mathar_, Jul 28 2016",
				"a(n) = A000124(n-2) * n. - _Bruce J. Nicholson_, Jul 13 2018",
				"a(n) = Sum_{i=0..n-1} (i*(n-2) + 1). - _Ivan N. Ianakiev_, Sep 25 2020"
			],
			"maple": [
				"A060354 := proc(n)",
				"    (n*(n-2)^2+n^2)/2 ;",
				"end proc: # _R. J. Mathar_, Jul 28 2016"
			],
			"mathematica": [
				"Table[(n (n-2)^2+n^2)/2,{n,0,50}] (* _Harvey P. Dale_, Aug 05 2011 *)",
				"CoefficientList[Series[x (1 - 2 x + 4 x^2) / (1 - x)^4, {x, 0, 50}], x] (* _Vincenzo Librandi_, Feb 16 2015 *)",
				"Table[PolygonalNumber[n,n],{n,0,50}] (* The program uses the PolygonalNumber function from Mathematica version 10 *) (* _Harvey P. Dale_, Mar 07 2016 *)",
				"LinearRecurrence[{4,-6,4,-1},{0,1,2,6},50] (* _Harvey P. Dale_, Mar 07 2016 *)"
			],
			"program": [
				"(PARI) { for (n=0, 1000, write(\"b060354.txt\", n, \" \", (n*(n - 2)^2 + n^2)/2); ) } \\\\ _Harry J. Smith_, Jul 04 2009",
				"(MAGMA) [(n*(n-2)^2+n^2)/2: n in [0..50]]; // _Vincenzo Librandi_, Feb 16 2015"
			],
			"xref": [
				"First differences of A004255.",
				"Cf. A000124, A100177, A057145."
			],
			"keyword": "easy,nice,nonn",
			"offset": "0,3",
			"author": "Hareendra Yalamanchili (hyalaman(AT)mit.edu), Apr 01 2001",
			"references": 35,
			"revision": 69,
			"time": "2020-10-24T23:19:36-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}