{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003959",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3959,
			"data": "1,3,4,9,6,12,8,27,16,18,12,36,14,24,24,81,18,48,20,54,32,36,24,108,36,42,64,72,30,72,32,243,48,54,48,144,38,60,56,162,42,96,44,108,96,72,48,324,64,108,72,126,54,192,72,216,80,90,60,216,62,96,128,729,84,144,68",
			"name": "If n = Product p(k)^e(k) then a(n) = Product (p(k)+1)^e(k), a(1) = 1.",
			"comment": [
				"Completely multiplicative.",
				"Sum of divisors of n with multiplicity. If n = p^m, the number of ways to make p^k as a divisor of n is C(m,k); and sum(C(m,k)*p^k) = (p+1)^k. The rest follows because the function is multiplicative. - _Franklin T. Adams-Watters_, Jan 25 2010"
			],
			"link": [
				"T. D. Noe and Daniel Forgues, \u003ca href=\"/A003959/b003959.txt\"\u003eTable of n, a(n) for n = 1..100000\u003c/a\u003e (first 1000 terms from T. D. Noe)",
				"\u003ca href=\"/index/Di#divseq\"\u003eIndex to divisibility sequences\u003c/a\u003e"
			],
			"formula": [
				"Multiplicative with a(p^e) = (p+1)^e. - _David W. Wilson_, Aug 01 2001",
				"Sum_{n\u003e0} a(n)/n^s = Product_{p prime} 1/(1-p^(-s)-p^(1-s)) (conjectured). - _Ralf Stephan_, Jul 07 2013",
				"This follows from the absolute convergence of the sum (compare with a(n) = n^2) and the Euler product for completely multiplicative functions. Convergence occurs for at least Re(s)\u003e3. - _Thomas Anton_, Jul 15 2021",
				"Sum_{k=1..n} a(k) ~ c * n^2, where c = A065488/2 = 1/(2*A005596) = 1.3370563627850107544802059152227440187511993141988459926... - _Vaclav Kotesovec_, Jul 17 2021",
				"From _Thomas Scheuerle_, Jul 19 2021: (Start)",
				"a(n) = gcd(A166642(n), A166643(n)).",
				"a(n) = A166642(n)/A061142(n).",
				"a(n) = A166643(n)/A165824(n).",
				"a(n) = A166644(n)/A165825(n).",
				"a(n) = A166645(n)/A165826(n).",
				"a(n) = A166646(n)/A165827(n).",
				"a(n) = A166647(n)/A165828(n).",
				"a(n) = A166649(n)/A165830(n).",
				"a(n) = A166650(n)/A165831(n).",
				"a(n) = A167351(n)/A166590(n). (End)",
				"Dirichlet g.f.: zeta(s-1) * Product_{primes p} (1 + 1/(p^s - p - 1)). - _Vaclav Kotesovec_, Aug 22 2021"
			],
			"maple": [
				"a:= n-\u003e mul((i[1]+1)^i[2], i=ifactors(n)[2]):",
				"seq(a(n), n=1..80);  # _Alois P. Heinz_, Sep 13 2017"
			],
			"mathematica": [
				"a[1] = 1; a[n_] := (fi = FactorInteger[n]; Times @@ ((fi[[All, 1]]+1)^fi[[All, 2]])); a /@ Range[67] (* _Jean-François Alcover_, Apr 22 2011 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c1,0,direuler(p=2,n,1/(1-X-p*X))[n]) /* _Ralf Stephan_ */",
				"(Haskell)",
				"a003959 1 = 1",
				"a003959 n = product $ map (+ 1) $ a027746_row n",
				"-- _Reinhard Zumkeller_, Apr 09 2012"
			],
			"xref": [
				"Apart from initial terms, same as A064478.",
				"Cf. A003958, A027746, A036987, A061142, A163407.",
				"Cf. A063441, A165824, A165825, A165826, A165827.",
				"Cf. A165828, A165829, A165831, A167350, A166590.",
				"Cf. A166642, A166643, A166644, A166645, A166646.",
				"Cf. A166647, A166649, A166650, A166586, A165830.",
				"Cf. A167351, A168065, A168066."
			],
			"keyword": "nonn,easy,nice,mult",
			"offset": "1,2",
			"author": "_Marc LeBrun_",
			"ext": [
				"Definition reedited (with formula) by _Daniel Forgues_, Nov 17 2009"
			],
			"references": 82,
			"revision": 67,
			"time": "2021-09-10T06:51:07-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}