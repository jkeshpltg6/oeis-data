{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A281975",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 281975,
			"data": "1,3,4,3,2,4,4,2,1,4,5,3,1,2,3,2,3,5,7,3,4,5,2,1,2,6,9,6,2,4,6,3,4,6,8,4,6,5,4,3,2,11,10,4,1,7,5,1,3,3,11,9,7,5,6,4,2,7,5,4,2,6,4,3,2,7,15,4,4,6,5,1,2,6,7,7,3,6,4,2,4",
			"name": "Number of ways to write n as x^2 + y^2 + z^2 + w^2 with x,y,z,w nonnegative integers and z \u003c= w such that both x and |x-y| are squares.",
			"comment": [
				"Conjecture: (i) a(n) \u003e 0 for all n = 0,1,2,....",
				"(ii) Each nonnegative integer n can be written as x^2 + y^2 + z^2 + w^2 with x,y,z,w nonnegative integers such that |x-y| and 2*(y-z) (or 2*(z-y)) are both squares.",
				"(iii) For each ordered pair (a,b) = (2,1), (3,1), (9,5), (14,10), any nonnegative integer n can be written as x^2 + y^2 + z^2 + w^2 with x,y,z,w nonnegative integers such that x and |a*x-b*y| are both squares.",
				"The author has proved that each n = 0,1,2,... can be written as x^2 + y^2 + z^2 + w^2 with x,y,z,w nonnegative integers such that x (or x-y, or 2(x-y)) is a square.",
				"See also A281976 and A281977 for similar conjectures."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A281975/b281975.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2016.11.008\"\u003eRefining Lagrange's four-square theorem\u003c/a\u003e, J. Number Theory 175(2017), 167-190."
			],
			"example": [
				"a(8) = 1 since 8 = 0^2 + 0^2 + 2^2 + 2^2 with 0 = 0^2 and |0-0| = 0^2.",
				"a(12) = 1 since 12 = 1^2 + 1^2 + 1^2 + 3^2 with 1 = 1^2 and |1-1| = 0^2.",
				"a(44) = 1 since 44 = 1^2 + 5^2 + 3^2 + 3^2 with 1 = 1^2 and |1-5| = 2^2.",
				"a(47) = 1 since 47 = 1^2 + 1^2 + 3^2 + 6^2 with 1 = 1^2 and |1-1| = 0^2.",
				"a(71) = 1 since 71 = 1^2 + 5^2 + 3^2 + 6^2 with 1 = 1^2 and |1-5| = 2^2.",
				"a(95) = 1 since 95 = 1^2 + 2^2 + 3^2 + 9^2 with 1 = 1^2 and |1-2| = 1^2.",
				"a(140) = 1 since 140 = 9^2 + 5^2 + 3^2 + 5^2 with 9 = 3^2 and |9-5| = 2^2.",
				"a(428) = 1 since 428 = 9^2 + 13^2 + 3^2 + 13^2 with 9 = 3^2 and |9-13| = 2^2.",
				"a(568) = 1 since 568 = 4^2 + 8^2 + 2^2 + 22^2 with 4 = 2^2 and |4-8| = 2^2.",
				"a(632) = 1 since 632 = 16^2 + 12^2 + 6^2 + 14^2 with 16 = 4^2 and |16-12| = 2^2.",
				"a(1144) = 1 since 1144 = 16^2 + 20^2 + 2^2 + 22^2 with 16 = 4^2 and |16-20| = 2^2.",
				"a(1544) = 1 since 1544 = 0^2 + 0^2 + 10^2 + 38^2 with 0 = 0^2 and |0-0| = 0^2."
			],
			"mathematica": [
				"SQ[n_]:=SQ[n]=IntegerQ[Sqrt[n]];",
				"Do[r=0;Do[If[SQ[n-x^4-y^2-z^2]\u0026\u0026SQ[Abs[x^2-y]],r=r+1],{x,0,n^(1/4)},{y,0,Sqrt[n-x^4]},{z,0,Sqrt[(n-x^4-y^2)/2]}];Print[n,\" \",r];Continue,{n,0,80}]"
			],
			"xref": [
				"Cf. A000118, A000290, A270969, A271775, A281939, A281941, A281976, A281977."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Zhi-Wei Sun_, Feb 03 2017",
			"references": 7,
			"revision": 11,
			"time": "2017-02-04T11:23:08-05:00",
			"created": "2017-02-04T11:23:08-05:00"
		}
	]
}