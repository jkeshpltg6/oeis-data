{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A277324",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 277324,
			"data": "2,6,18,30,90,270,450,210,630,6750,20250,9450,15750,47250,22050,2310,6930,330750,3543750,1653750,4961250,53156250,24806250,727650,1212750,57881250,173643750,18191250,8489250,25467750,2668050,30030,90090,40020750,1910081250,891371250,9550406250,455814843750,212713593750",
			"name": "Odd bisection of A260443 (the even terms): a(n) = A260443((2*n)+1).",
			"comment": [
				"From _David A. Corneth_, Oct 22 2016: (Start)",
				"The exponents of the prime factorization of a(n) are first nondecreasing, then nonincreasing.",
				"The exponent of 2 in the prime factorization of a(n) is 1. (End)"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A277324/b277324.txt\"\u003eTable of n, a(n) for n = 0..1024\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A260443((2*n)+1).",
				"a(0) = 2; for n \u003e= 1, a(n) = A260443(n) * A260443(n+1).",
				"Other identities. For all n \u003e= 0:",
				"A007949(a(n)) = A005811(n). [See comments in A125184.]",
				"A156552(a(n)) = A277189(n), a(n) = A005940(1+A277189(n)).",
				"A048675(a(n)) = 2n + 1. - _David A. Corneth_, Oct 22 2016",
				"A001222(a(n)) = A007306(1+n).",
				"A056169(a(n)) = A284267(n).",
				"A275812(a(n)) = A284268(n).",
				"A248663(a(n)) = A283975(n).",
				"A000188(a(n)) = A283484(n).",
				"A247503(a(n)) = A284563(n).",
				"A248101(a(n)) = A284564(n).",
				"A046523(a(n)) = A284573(n).",
				"a(n) = A277198(n) * A284008(n).",
				"a(n) = A284576(n) * A284578(n) = A284577(n) * A000290(A284578(n))."
			],
			"example": [
				"A method to find terms of this sequence, explained by an example to find a(7). To find k = a(7), we find k such that A048675(k) = 2*7+1 = 15. 7 has the binary partitions: {[7, 0, 0], [5, 1, 0], [3, 2, 0], [1, 3, 0], [3, 0, 1], [1, 1, 1]}. To each of those, we prepend a 1. This gives the binary partitions of 15 starting with a 1. For example, for the first we get [1, 7, 0, 0]. We see that only [1, 5, 1, 0], [1, 3, 2, 0] and [1, 1, 1, 1] start nondecreasing, then nonincreasing, so we only check those. These numbers will be the exponents in a prime factorization. [1, 5, 1, 0] corresponds to prime(1)^1 * prime(2)^5 * prime(3)^1 * prime(4)^0 = 2430. We find that [1, 1, 1, 1] gives k = 210 for which A048675(k) = 15 so a(7) = 210. - _David A. Corneth_, Oct 22 2016"
			],
			"mathematica": [
				"a[n_] := a[n] = Which[n \u003c 2, n + 1, EvenQ@ n, Times @@ Map[#1^#2 \u0026 @@ # \u0026, FactorInteger[#] /. {p_, e_} /; e \u003e 0 :\u003e {Prime[PrimePi@ p + 1], e}] - Boole[# == 1] \u0026@ a[n/2], True, a[#] a[# + 1] \u0026[(n - 1)/2]]; Table[a[2 n + 1], {n, 0, 38}] (* _Michael De Vlieger_, Apr 05 2017 *)"
			],
			"program": [
				"(Scheme, two versions)",
				"(define (A277324 n) (A260443 (+ 1 n n)))",
				"(define (A277324 n) (if (zero? n) 2 (* (A260443 n) (A260443 (+ 1 n)))))",
				"(Python)",
				"from sympy import factorint, prime, primepi",
				"from operator import mul",
				"def a003961(n):",
				"    F=factorint(n)",
				"    return 1 if n==1 else reduce(mul, [prime(primepi(i) + 1)**F[i] for i in F])",
				"def a260443(n): return n + 1 if n\u003c2 else a003961(a260443(n//2)) if n%2==0 else a260443((n - 1)//2)*a260443((n + 1)//2)",
				"def a(n): return a260443(2*n + 1)",
				"print([a(n) for n in range(101)]) # _Indranil Ghosh_, Jun 21 2017"
			],
			"xref": [
				"Cf. A005811, A005940, A007306, A007949, A156552, A260443, A277189, A277323, A283484, A283975, A284267, A284268, A284563, A284564, A284573.",
				"Cf. A277200 (same sequence sorted into ascending order)."
			],
			"keyword": "nonn",
			"offset": "0,1",
			"author": "_Antti Karttunen_, Oct 10 2016",
			"ext": [
				"More linking formulas added by _Antti Karttunen_, Apr 16 2017"
			],
			"references": 20,
			"revision": 41,
			"time": "2021-03-12T07:53:17-05:00",
			"created": "2016-10-26T15:17:24-04:00"
		}
	]
}