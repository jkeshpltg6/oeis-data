{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A174541",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 174541,
			"data": "0,1,1,1,2,1,1,1,2,1,1,2,2,2,1,1,2,2,2,2,1,1,2,2,1,2,2,1,1,2,2,2,2,2,2,1,1,2,2,2,2,2,2,2,1,1,2,2,1,2,2,2,2,2,1,1,2,2,2,2,2,2,2,2,2,1,1,2,2,2,2,2,2,2,2,2,2,1,1,2,2,2,2,2,2,2,2,2,2,2,1,1,2,2,2,2,2,2,2,2,2,2,2,2,1",
			"name": "Baron Munchhausen's Sequence.",
			"comment": [
				"Let n coins weighing 1, 2, ..., n grams be given. Suppose Baron Munchhausen knows which coin weighs how much, but his audience does not. Then a(n) is the minimum number of weighings the Baron must conduct on a balance scale, so as to unequivocally demonstrate the weight of at least one of the coins.",
				"After a(1) = 0, a(n) is either 1 or 2 for all n.",
				"a(n) = 1 for n triangular, n triangular-plus-one, T_n a square, and T_n a square-plus-one, where T_n is the n-th triangular number; a(n) = 2 for all other n \u003e 1."
			],
			"link": [
				"M. Brand, \u003ca href=\"https://doi.org/10.1016/j.disc.2011.12.026\"\u003eTightening the bounds on the Baron's Omni-sequence\u003c/a\u003e, Discrete Math., 312 (2012), 1326-1335.",
				"T. Khovanova, K. Knop and A. Radul, \u003ca href=\"https://arxiv.org/abs/1003.3406\"\u003eBaron Munchhausen's Sequence\u003c/a\u003e, arXiv:1003.3406 [math.CO], 2010.",
				"T. Khovanova, K. Knop, A. Radul, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL13/Khovanova/khova4.html\"\u003eBaron Munchhausen's Sequence\u003c/a\u003e, J. Int. Seq. 13 (2010) # 10.8.7.",
				"T. Khovanova, A. Radul, \u003ca href=\"http://blog.tanyakhovanova.com/?p=179\"\u003eAnother Coins Sequence\u003c/a\u003e"
			],
			"example": [
				"a(7) = 1 because the weighing 1 + 2 + 3 \u003c 7 conclusively demonstrates the weight of the seven-gram coin."
			],
			"mathematica": [
				"triangularQ[n_] := IntegerQ[ Sqrt[8n+1]]; a[1] = 0; a[n_ /; triangularQ[n] || triangularQ[n-1] || IntegerQ[ Sqrt[n*(n+1)/2]] || IntegerQ[ Sqrt[n*(n+1)/2 - 1]]] = 1; a[_] = 2; Table[a[n], {n, 1, 105}] (* _Jean-François Alcover_, Jul 30 2012, after comments *)"
			],
			"program": [
				"(Scheme) ;;; The following Scheme program generates terms of Baron",
				";;; Munchhausen's Sequence.",
				"(define (acceptable? n)",
				"..(or (triangle? n)",
				"......(= n 2)",
				"......(triangle? (- n 1))",
				"......(square? (triangle n))",
				"......(square? (- (triangle n) 1))))",
				"(stream-map",
				".(lambda (n)",
				"...(if (= n 1)",
				".......0",
				".......(if (acceptable? n)",
				"...........1",
				"...........2)))",
				".(the-integers))"
			],
			"xref": [
				"Cf. A000217, A000124, A001108, A072221, A186313."
			],
			"keyword": "nonn,nice",
			"offset": "1,5",
			"author": "_Tanya Khovanova_, _Konstantin Knop_, and Alexey Radul, Mar 21 2010",
			"references": 1,
			"revision": 23,
			"time": "2021-12-11T11:49:27-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}