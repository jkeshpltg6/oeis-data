{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A024833",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 24833,
			"data": "5,11,19,29,41,61,79,106,129,163,191,232,265,313,365,407,466,529,579,649,723,781,862,947,1013,1105,1201,1301,1379,1486,1597,1712,1801,1923,2049,2179,2279,2416,2557,2702,2813,2965,3121,3281,3445,3571,3742,3917,4096",
			"name": "a(n) = least m such that if r and s in {1/1, 1/2, 1/3, ..., 1/n} satisfy r \u003c s, then r \u003c k/m \u003c (k+1)/m \u003c s for some integer k.",
			"comment": [
				"For a guide to related sequences, see A001000. - _Peter J. C. Moses_, Aug 08 2012"
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A024833/b024833.txt\"\u003eTable of n, a(n) for n = 2..300\u003c/a\u003e"
			],
			"example": [
				"Using the terminology introduced at A001000, the 2nd separator of the set {1/3, 1/2, 1} is a(3) = 11, since 1/3 \u003c 4/11 \u003c 5/11 \u003c 1/2 \u003c 6/11 \u003c 7/11 \u003c 1 and 11 is the least m for which 1/3, 1/2, 1 are thus separated using numbers k/m. - _Clark Kimberling_, Aug 08 2012"
			],
			"mathematica": [
				"leastSeparatorS[seq_, s_] := Module[{n = 1},",
				"Table[While[Or @@ (Ceiling[n #1[[1]]] \u003c",
				"s + 1 + Floor[n #1[[2]]] \u0026) /@ (Sort[#1, Greater] \u0026) /@",
				"Partition[Take[seq, k], 2, 1], n++]; n, {k, 2, Length[seq]}]];",
				"t = Map[leastSeparatorS[1/Range[50], #] \u0026, Range[5]];",
				"TableForm[t]",
				"t[[2]] (* _Clark Kimberling_, Aug 08 2012 *)"
			],
			"xref": [
				"Cf. A001000, A071111, A024843, A024846."
			],
			"keyword": "nonn",
			"offset": "2,1",
			"author": "_Clark Kimberling_",
			"references": 2,
			"revision": 19,
			"time": "2021-12-14T10:30:54-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}