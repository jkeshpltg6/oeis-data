{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A324338",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 324338,
			"data": "1,1,1,2,1,3,3,2,1,4,5,3,4,3,2,5,1,5,7,4,7,5,3,8,5,4,3,7,2,7,8,5,1,6,9,5,10,7,4,11,9,7,5,12,3,11,13,8,6,5,4,9,3,10,11,7,2,9,12,7,11,8,5,13,1,7,11,6,13,9,5,14,13,10,7,17,4,15,18,11,11,9,7,16,5,17,19,12,3,14,19,11,18,13,8,21,7,6,5,11,4,13,14,9,3,13",
			"name": "a(n) = A002487(1+A006068(n)).",
			"comment": [
				"Like in A324337, a few terms preceding each 2^k-th term (here always 1) seem to consist of a batch of nearby Fibonacci numbers (A000045) in some order. For example, a(65533) = 987, a(65534) = 610 and a(65535) = 1597.",
				"For all n \u003e 0 A324338(n)/A324337(n) constitutes an enumeration system of all positive rationals. For all n \u003e 0 A324338(n) + A324337(n) = A071585(n). - _Yosu Yurramendi_, Oct 22 2019"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A324338/b324338.txt\"\u003eTable of n, a(n) for n = 0..16384\u003c/a\u003e",
				"Antti Karttunen, \u003ca href=\"/A324338/a324338.txt\"\u003eData supplement: n, a(n) computed for n = 0..65537\u003c/a\u003e",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e",
				"\u003ca href=\"/index/St#Stern\"\u003eIndex entries for sequences related to Stern's sequences\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A002487(1+A006068(n)).",
				"a(2^n) = 1 for all n \u003e= 0.",
				"From _Yosu Yurramendi_, Oct 22 2019: (Start)",
				"a(2^m+2^(m-1)+k) = A324337(2^m+        k), m \u003e 0, 0 \u003c= k \u003c 2^(m-1)",
				"a(2^m+        k) = A324337(2^m+2^(m-1)+k), m \u003e 0, 0 \u003c= k \u003c 2^(m-1). (End)",
				"a(n) = A324337(A063946(n)), n \u003e 0. _Yosu Yurramendi_, Nov 04 2019",
				"a(n) = A002487(A233279(n)), n \u003e 0. _Yosu Yurramendi_, Nov 08 2019",
				"From _Yosu Yurramendi_, Nov 28 2019: (Start)",
				"a(2^(m+1)+k) - a(2^m+k) = A324337(k), m \u003e= 0,  0 \u003c= k \u003c 2^m.",
				"a(A059893(2^(m+1)+A000069(k+1))) - a(A059893(2^m+A000069(k+1))) =  A071585(k), m \u003e= 1,  0 \u003c= k \u003c 2^(m-1).",
				"a(A059893(2^m+ A001969(k+1))) = A071585(k),    m \u003e= 0,  0 \u003c= k \u003c 2^(m-1). (End)",
				"From _Yosu Yurramendi_, Nov 29 2019: (Start)",
				"For n \u003e 0:",
				"A324338(n) + A324337(n) = A071585(n).",
				"A324338(2*A001969(n)  )-A324337(2*A001969(n)  ) =  A071585(n-1)",
				"A324338(2*A001969(n)+1)-A324337(2*A001969(n)+1) = -A324337(n-1)",
				"A324338(2*A000069(n)  )-A324337(2*A000069(n)  ) = -A071585(n-1)",
				"A324338(2*A000069(n)+1)-A324337(2*A000069(n)+1) =  A324338(n-1) (End)",
				"a(n) = A002487(A233279(n)). _Yosu Yurramendi_, Dec 27 2019"
			],
			"program": [
				"(PARI)",
				"A006068(n)= { my(s=1, ns); while(1, ns = n \u003e\u003e s; if(0==ns, break()); n = bitxor(n, ns); s \u003c\u003c= 1; ); return (n); } \\\\ From A006068",
				"A002487(n) = { my(s=sign(n), a=1, b=0); n = abs(n); while(n\u003e0, if(bitand(n, 1), b+=a, a+=b); n\u003e\u003e=1); (s*b); };",
				"A324338(n) = A002487(1+A006068(n));",
				"(R)",
				"maxlevel \u003c- 6 # by choice #",
				"b \u003c- 0; A324338 \u003c- 1; A324337 \u003c- 1",
				"for(i in 1:2^maxlevel) {",
				"  b[2*i  ] \u003c-     b[i]",
				"  b[2*i+1] \u003c- 1 - b[i]",
				"  A324338[2*i  ] \u003c- A324338[i]          + A324337[i]*   b[i]",
				"  A324338[2*i+1] \u003c- A324338[i]          + A324337[i]*(1-b[i])",
				"  A324337[2*i  ] \u003c- A324338[i]*(1-b[i]) + A324337[i]",
				"  A324337[2*i+1] \u003c- A324338[i]*   b[i]  + A324337[i]}",
				"#",
				"A324338[1:127]; A324337[1:127]",
				"# _Yosu Yurramendi_, Oct 22 2019"
			],
			"xref": [
				"Cf. A000045, A002487, A006068, A324288, A324337."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_Antti Karttunen_, Feb 23 2019",
			"references": 4,
			"revision": 30,
			"time": "2020-01-12T13:50:18-05:00",
			"created": "2019-02-24T01:59:44-05:00"
		}
	]
}