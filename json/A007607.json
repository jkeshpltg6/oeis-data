{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007607",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7607,
			"id": "M0821",
			"data": "2,3,7,8,9,10,16,17,18,19,20,21,29,30,31,32,33,34,35,36,46,47,48,49,50,51,52,53,54,55,67,68,69,70,71,72,73,74,75,76,77,78,92,93,94,95,96,97,98,99,100,101,102,103,104,105,121,122,123,124,125,126,127,128,129,130",
			"name": "Skip 1, take 2, skip 3, etc.",
			"comment": [
				"a(A000290(n)) = A001105(n). - _Reinhard Zumkeller_, Feb 12 2011",
				"A057211(a(n)) = 0. - _Reinhard Zumkeller_, Dec 30 2011",
				"Numbers k with the property that the smallest Dyck path of the symmetric representation of sigma(k) has a central peak. (Cf. A237593.) - _Omar E. Pol_, Aug 28 2018",
				"Union of A317303 and A014105. - _Omar E. Pol_, Aug 29 2018"
			],
			"reference": [
				"R. Honsberger, Mathematical Gems III, M.A.A., 1985, p. 177.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A007607/b007607.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"G.f.: 1/(1-x) * (1/(1-x) + x*Sum_{k\u003e=1} (2k+1)*x^(k*(k+1))). - _Ralf Stephan_, Mar 03 2004",
				"a(n) = floor(sqrt(n) + 1/2)^2 + n = A053187(n) + n. - _Ridouane Oudra_, May 04 2019"
			],
			"example": [
				"From _Omar E. Pol_, Aug 29 2018: (Start)",
				"Written as an irregular triangle in which the row lengths are the nonzero even numbers the sequence begins:",
				"    2,   3;",
				"    7,   8,   9,  10;",
				"   16,  17,  18,  19,  20,  21;",
				"   29,  30,  31,  32,  33,  34,  35,  36;",
				"   46,  47,  48,  49,  50,  51,  52,  53,  54,  55;",
				"   67,  68,  69,  70,  71,  72,  73,  74,  75,  76,  77,  78;",
				"   92,  93,  94,  95,  96,  97,  98,  99, 100, 101, 102, 103, 104, 105;",
				"  121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136;",
				"...",
				"Row sums give the nonzero terms of A317297.",
				"Column 1 gives A130883, n \u003e= 1.",
				"Right border gives A014105, n \u003e= 1.",
				"(End)"
			],
			"mathematica": [
				"Flatten[ Table[i, {j, 2, 16, 2}, {i, j(j - 1)/2 + 1, j(j + 1)/2}]] (* _Robert G. Wilson v_, Mar 11 2004 *)",
				"With[{t=20},Flatten[Take[TakeList[Range[(t(t+1))/2],Range[t]],{2,-1,2}]]] (* _Harvey P. Dale_, Sep 26 2021 *)"
			],
			"program": [
				"(Haskell)",
				"a007607 n = a007607_list !! (n-1)",
				"a007607_list = skipTake 1 [1..] where",
				"   skipTake k xs = take (k + 1) (drop k xs)",
				"                   ++ skipTake (k + 2) (drop (2*k + 1) xs)",
				"-- _Reinhard Zumkeller_, Feb 12 2011",
				"(PARI) for(m=0,10,for(n=2*m^2+3*m+2,2*m^2+5*m+3,print1(n\", \"))) \\\\ _Charles R Greathouse IV_, Feb 12 2011",
				"(Haskell)",
				"a007607_list' = f $ tail $ scanl (+) 0 [1..] where",
				"   f (t:t':t'':ts) = [t+1..t'] ++ f (t'':ts)",
				"-- _Reinhard Zumkeller_, Feb 12 2011"
			],
			"xref": [
				"Cf. A063656, A004202, A063657, A007606, A064801, A004202.",
				"Complement of A007606.",
				"Cf. A014105, A130883, A317297."
			],
			"keyword": "nonn,easy,tabf",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_, _Robert G. Wilson v_, _Mira Bernstein_",
			"references": 9,
			"revision": 59,
			"time": "2021-09-26T17:00:03-04:00",
			"created": "1994-09-19T03:00:00-04:00"
		}
	]
}