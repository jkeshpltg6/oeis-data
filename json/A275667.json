{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A275667",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 275667,
			"data": "1,3,7,9,7,21,25,27,7,21,49,63,25,75,103,81,7,21,49,63,49,147,175,189,25,75,175,225,103,309,409,243,7,21,49,63,49,147,175,189,49,147,343,441,175,525,721,567,25,75,175,225,175,525,625,675,103,309,721",
			"name": "Number of ON cells after n generations in a 2-dimensional \"Odd-Rule\" cellular automaton on triangular tiling.",
			"comment": [
				"Each triangular tile has 3 neighbors. A cell is ON in a given generation if and only if there was an odd number of ON cells among the three nearest neighbors in the preceding generation.",
				"At the initial moment there is a single ON cell.",
				"Given pattern replicates after a number of generations which is a power of 2 when a(n) = 7.",
				"Number of cells on each even step minus one is divisible by 6.",
				"By analogy with the Ekhad, Sloane, Zeilberger link, one may suppose that using ternary expansion of n, recurrence relations for a(n) can be obtained and proved.",
				"From _Andrey Zabolotskiy_, Aug 04 2016: (Start)",
				"If the first conjecture from the Formula section is true then the fact that the right border of the triangle (see Example) gives A000244 follows directly from it.",
				"If the second conjecture is true then the numbers just before the right border give A102900.",
				"Since the 7 cells which are ON at the beginning of every row are farther and farther away from each other, the n-th term of a row (with offset 0) is a(n)*7 for not very large n.",
				"See also comments to A247666.",
				"(End)"
			],
			"link": [
				"Kovba Alexey, \u003ca href=\"/A275667/a275667.pdf\"\u003eIllustration for n = 0..5\u003c/a\u003e",
				"Shalosh B. Ekhad, N. J. A. Sloane, and Doron Zeilberger, \u003ca href=\"http://arxiv.org/abs/1503.01796\"\u003eA Meta-Algorithm for Creating Fast Algorithms for Counting ON Cells in Odd-Rule Cellular Automata\u003c/a\u003e, arXiv:1503.01796 [math.CO], 2015.",
				"\u003ca href=\"/wiki/Index_to_OEIS:_Section_Ce#cell\"\u003eIndex to sequences in the OEIS related to cellular automata\u003c/a\u003e"
			],
			"formula": [
				"a(0) = 1. Conjecture: a(2*t+1) = 3*a(t).",
				"Conjectures: a(8*t+6) = 3*a(4*t+2) + 4*a(2*t), a(8*t+2) = 3*a(4*t) + 4*a(2*t), a(4*t) = a(2*t). These conjectured formulas together give recurrent relations for a(n) for any n. Also, obviously a(2*n) = A247666(n). - _Andrey Zabolotskiy_, Aug 04 2016"
			],
			"example": [
				"From _Omar E. Pol_, Aug 04 2016: (Start)",
				"Written as an irregular triangle in which the row lengths are the terms of A011782 the sequence begins:",
				"1;",
				"3;",
				"7, 9;",
				"7, 21, 25, 27;",
				"7, 21, 49, 63, 25, 75, 103, 81;",
				"7, 21, 49, 63, 49, 147, 175, 189, 25, 75, 175, 225, 103, 309, 409, 243;",
				"...",
				"It appears that the right border gives A000244.",
				"(End)"
			],
			"xref": [
				"Cf. A160239 (square tiling analog), A247640, A247666 (hexagonal tiling analogs).",
				"Cf. A000244, A011782, A102900."
			],
			"keyword": "nonn,tabf",
			"offset": "0,2",
			"author": "_Kovba Alexey_, Aug 04 2016",
			"references": 2,
			"revision": 45,
			"time": "2016-11-27T21:03:15-05:00",
			"created": "2016-08-04T22:54:03-04:00"
		}
	]
}