{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A194797",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 194797,
			"data": "0,-2,1,-7,3,-21,7,-49,23,-97,57,-195,117,-359,256,-624,498,-1086,909,-1831,1634,-2986,2833,-4847,4728,-7700,7798,-12026,12537,-18633,19745,-28479,30723,-42955,47100,-64284,71136,-95228,106402,-139718,157327,-203495",
			"name": "Imbalance of the sum of parts of all partitions of n.",
			"comment": [
				"Consider the three-dimensional structure of the shell model of partitions, version \"tree\" (see example). Note that only the parts \u003e 1 produce the imbalance. The 1's are located in the central columns therefore they do not produce the imbalance. Note that every column contains exactly the same parts. For more information see A135010."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A194797/b194797.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Sum_{k=1..n} (-1)^(k-1)*k*(p(k)-p(k-1)), where p(k) is the number of partitions of k.",
				"a(n) = b(1)-b(2)+b(3)-b(4)+b(5)-b(6)...+-b(n), where b(n) = A138880(n).",
				"a(n) ~ -(-1)^n * Pi * sqrt(2) * exp(Pi*sqrt(2*n/3)) / (48*sqrt(n)). - _Vaclav Kotesovec_, Oct 09 2018"
			],
			"example": [
				"For n = 6 the illustration of the three views of the shell model with 6 shells shows an imbalance (see below):",
				"------------------------------------------------------",
				"Partitions                Tree             Table 1.0",
				"of 6.                    A194805            A135010",
				"------------------------------------------------------",
				"6                   6                     6 . . . . .",
				"3+3                   3                   3 . . 3 . .",
				"4+2                     4                 4 . . . 2 .",
				"2+2+2                     2               2 . 2 . 2 .",
				"5+1                         1   5         5 . . . . 1",
				"3+2+1                       1 3           3 . . 2 . 1",
				"4+1+1                   4   1             4 . . . 1 1",
				"2+2+1+1                   2 1             2 . 2 . 1 1",
				"3+1+1+1                     1 3           3 . . 1 1 1",
				"2+1+1+1+1                 2 1             2 . 1 1 1 1",
				"1+1+1+1+1+1                 1             1 1 1 1 1 1",
				"------------------------------------------------------",
				".",
				".                   6 3 4 2 1 3 5",
				".     Table 2.0     . . . . 1 . .     Table 2.1",
				".      A182982      . . . 2 1 . .      A182983",
				".                   . 3 . . 1 2 .",
				".                   . . 2 2 1 . .",
				".                   . . . . 1",
				"------------------------------------------------------",
				"The sum of all parts \u003e 1 on the left hand side is 34 and the sum of all parts \u003e 1 on the right hand side is 13, so a(6) = -34 + 13 = -21. On the other hand for n = 6 the first n terms of A138880 are 0, 2, 3, 8, 10, 24 thus a(6) = 0-2+3-8+10-24 = -21."
			],
			"maple": [
				"with(combinat):",
				"a:= proc(n) option remember;",
				"      n *(-1)^n *(numbpart(n-1)-numbpart(n)) +a(n-1)",
				"    end: a(0):=0:",
				"seq(a(n), n=1..50); # _Alois P. Heinz_, Apr 04 2012"
			],
			"mathematica": [
				"a[n_] := Sum[(-1)^(k-1)*k*(PartitionsP[k] - PartitionsP[k-1]), {k, 1, n}]; Array[a, 50] (* _Jean-François Alcover_, Dec 09 2016 *)"
			],
			"xref": [
				"Cf. A000041, A002865, A135010, A138121, A138880, A141285, A182710, A182742, A182743, A182746, A182747, A182982, A182983, A182994, A182995, A194796, A194805."
			],
			"keyword": "sign",
			"offset": "1,2",
			"author": "_Omar E. Pol_, Jan 31 2012",
			"references": 4,
			"revision": 77,
			"time": "2018-10-09T09:46:37-04:00",
			"created": "2012-04-05T08:02:41-04:00"
		}
	]
}