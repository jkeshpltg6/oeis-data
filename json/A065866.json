{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A065866",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 65866,
			"data": "1,2,10,84,1008,15840,308880,7207200,196035840,6094932480,213322636800,8303173401600,355850288640000,16653793508352000,845180020548864000,46236318771202560000,2712530701243883520000,169890080762116915200000,11314679378756986552320000",
			"name": "a(n) = n! * Catalan(n+1).",
			"comment": [
				"From _Noam Zeilberger_, Mar 19 2019: (Start)",
				"a(n) is the number of flags in the associahedron of dimension n. For example, there are a(2) = 10 flags in the associahedron of dimension 2, a pentagon. (In this case a flag corresponds to a triple v:e:f of a mutually incident vertex v, edge e, and face f, with f necessarily the unique face of the pentagon.)",
				"Equivalently, a(n) is the number of maximal sequences of consistent parenthesizations of a string of n + 2 letters, starting with n + 1 pairs of parentheses, then removing one pair, and so on, ending with the trivial (outermost) parenthesization. For example, (a(b(cd))):(ab(cd)):(abcd) and (a(b(cd))):(a(bcd)):(abcd) are two of the a(2) = 10 maximal sequences of consistent parenthesizations of the letters abcd. (End)"
			],
			"reference": [
				"R. L. Graham, D. E. Knuth, O. Patashnik, \"Concrete Mathematics\", Addison-Wesley, 1994, pp. 200-204."
			],
			"link": [
				"Harry J. Smith, \u003ca href=\"/A065866/b065866.txt\"\u003eTable of n, a(n) for n = 0..100\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 2 * (2n+1)!/(n+2)!.",
				"E.g.f.: (1-2*x-sqrt(1-4*x))/(2*x^2) = (O.g.f. for A000108)^2 = B_2(x)^2 (cf. GKP reference).",
				"0 = a(n)*(-7200*a(n+2) + 2700*a(n+3) + 246*a(n+4) - 33*a(n+5)) + a(n+1)*(+36*a(n+2) + 372*a(n+3) + 36*a(n+4) - a(n+5)) + a(n+2)*(-18*a(n+2) + 9*a(n+3) + a(n+4)) for n \u003e= 0. - _Michael Somos_, Apr 14 2015",
				"The e.g.f. A(x) satisfies 0 = -2 + A(x) * (6*x - 2) + A'(x) * (4*x^2 - x). - _Michael Somos_, Apr 14 2015",
				"Conjecture: (n+2)*a(n) - 2*n*(2*n+1)*a(n-1) = 0. - _R. J. Mathar_, Oct 31 2015",
				"a(n) ~ 4^n*exp(-n)*n^(n - 2)*sqrt(2)*(24*n - 61)/6. - _Peter Luschny_, Mar 20 2019"
			],
			"example": [
				"G.f. = 1 + 2*x + 10*x^2 + 84*x^3 + 1008*x^4 + 15840*x^5 + 308880*x^6 + ..."
			],
			"maple": [
				"with(combstruct): ZL:=[T, {T=Union(Z, Prod(Epsilon, Z, T), Prod(T, Z, Epsilon), Prod(T, T, Z))}, labeled]: seq(count(ZL, size=i+1)/(i+1), i=0..18); # _Zerinvary Lajos_, Dec 16 2007",
				"a := n -\u003e (2^(2*n+2)*GAMMA(n+3/2))/(sqrt(Pi)*(n+1)*(n+2)):",
				"seq(simplify(a(n)), n=0..17); # _Peter Luschny_, Mar 20 2019"
			],
			"mathematica": [
				"Table[2*(2n+1)!/(n+2)!, {n,0,20}] (* _G. C. Greubel_, Mar 19 2019 *)"
			],
			"program": [
				"(PARI) { for (n = 0, 100, a = 2 * (2*n + 1)!/(n + 2)!; write(\"b065866.txt\", n, \" \", a) ) } \\\\ _Harry J. Smith_, Nov 02 2009",
				"(MAGMA) [Factorial(n)*Catalan(n+1): n in [0..20]]; // _G. C. Greubel_, Mar 19 2019",
				"(Sage) [factorial(n)*catalan_number(n+1) for n in (0..20)] # _G. C. Greubel_, Mar 19 2019",
				"(GAP) List([0..20], n-\u003e 2*Factorial(2*n+1)/Factorial(n+2)) # _G. C. Greubel_, Mar 19 2019"
			],
			"xref": [
				"Cf. A000108.",
				"Equals 2 * A102693(n+1), n \u003e 0.",
				"Main diagonal of A256116."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Len Smiley_, Dec 06 2001",
			"references": 4,
			"revision": 45,
			"time": "2019-03-20T12:59:48-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}