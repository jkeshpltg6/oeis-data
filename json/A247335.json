{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A247335",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 247335,
			"data": "1,10,361,13690,519841,19740250,749609641,28465426090,1080936581761,41047124680810,1558709801289001,59189925324301210,2247658452522156961,85351831270517663290,3241121929827149048041,123077281502161146162250",
			"name": "The curvature of touching circles inscribed in a special way in the larger segment of circle of radius 10/9 divided by a chord of length 4/3.",
			"comment": [
				"Refer to comment of A240926. Consider a circle C of radius 10/9 (in some length units) with a chord of length 4/3. This has been chosen such that the larger sagitta has length 2. The smaller sagitta has length 2/9. The input, besides the circle C is the circle C_0 with radius R_0 = 1, touching the chord and circle C. The following sequence of circles C_n with radii R_n, n \u003e= 1, is obtained from the condition that C_n touches i) the circle C, ii) the chord and iii) the circle C_(n-1). The circle curvatures C_n = 1/R_n, n \u003e= 0, are conjectured to be a(n). If one considers the curvature of touching circles inscribed in the smaller segment, the sequence would be A247512. See an illustration given in the link.",
				"a(n) also seems to be A078986(i)^2 and 10*A097315(j)^2 interleaved; where i = n/2 for n even, j = n/2 - 1/2 for n odd; as following:",
				"1          = 1^2",
				"10         = 10*1^2",
				"361        = 19^2",
				"13690      = 10*37^2",
				"519841     = 721^2",
				"19740250   = 10*1405^2",
				"749609641  = 27379^2",
				"...",
				"A078986; Chebyshev... polynomial: 1, 19, 721, 27379,...",
				"A097315; Pell equation...       : 1, 37, 1405, 53353,..."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A247335/b247335.txt\"\u003eTable of n, a(n) for n = 0..600\u003c/a\u003e",
				"Kival Ngaokrajang, \u003ca href=\"/A247335/a247335_2.pdf\"\u003eIllustration of initial terms\u003c/a\u003e",
				"Wolfdieter Lang, \u003ca href=\"/A247335/a247335_3.pdf\"\u003eCurvature computation for A247335 and A247512.\u003c/a\u003e",
				"Giovanni Lucca, \u003ca href=\"http://forumgeom.fau.edu/FG2018volume18/FG201808index.html\"\u003eInteger Sequences and Circle Chains Inside a Circular Segment\u003c/a\u003e, Forum Geometricorum, Vol. 18 (2018), 47-55.",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (39,-39,1)."
			],
			"formula": [
				"Conjectures from _Colin Barker_, Sep 18 2014: (Start)",
				"a(n) = 39*a(n-1) - 39*a(n-2) + a(n-3).",
				"G.f.: -(10*x^2-29*x+1) / ((x-1)*(x^2-38*x+1)). (End)",
				"From _Wolfdieter Lang_, Sep 30 2014 (Start)",
				"See the W. Lang link for proofs of the following statements.",
				"One step nonlinear recurrence: a(n) = -9 + 19*a(n-1) + 60*sqrt(a(n-1)*(a(n-1) - 1)/10), n\u003e=1, with a(0) = 1.",
				"a(n) =  (1 + A078986(n))/2  = (2 + S(n, 38) - S(n-2, 38))/4 =",
				"  (1 + S(n, 38) -19*S(n-1, 38))/2 for n\u003e=0, with Chebyshev's S-polynomials (see A049310).  S(n, 38) = A078987(n).",
				"The G.f. conjectured by Colin Barker above follows from the one for Chebyshev's T(n, 19) = A078986(n):  (1/(1-x) + (1-19*x)/(1-38*x+x^2))/2 = (1-29*x+10*x^2)/((1-x)* (1-38*x+x^2)).",
				"The four term recurrence conjectured by Colin Barker above follows from the expanded g.f. denominator: (1-x)* (1-38*x+x^2) = 1- 39*x + 39*x^2 - x^3.",
				"(End)",
				"a(n) = ((19+6*sqrt(10))^(-n)*(1+(19+6*sqrt(10))^n)^2)/4. - _Colin Barker_, Mar 03 2016"
			],
			"mathematica": [
				"LinearRecurrence[{39,-39,1}, {1, 10, 361}, 50] (* or *) Table[Round[((19 + 6*Sqrt[10])^(-n)*(1 + (19 + 6*Sqrt[10])^n)^2)]/4, {n, 0, 30}] (* _G. C. Greubel_, Dec 20 2017 *)"
			],
			"program": [
				"(PARI)",
				"{",
				"r=0.9;print1(1,\", \");r1=r;",
				"for (n=1,50,",
				"if (n\u003c=1,ab=2-r,ab=sqrt(ac^2+r^2));",
				"ac=sqrt(ab^2-r^2);",
				"if (n\u003c=1,z=0,z=(Pi/2)-atan(ac/r)+asin((r1-r)/(r1+r));r1=r);",
				"b=acos(r/ab)-z;",
				"r=r*(1-cos(b))/(1+cos(b));",
				"an=floor(9/(10*r));",
				"print1(if(an\u003e9,an,10),\", \")",
				")",
				"}",
				"(PARI) Vec(-(10*x^2-29*x+1)/((x-1)*(x^2-38*x+1)) + O(x^20)) \\\\ _Colin Barker_, Mar 03 2016",
				"(MAGMA) I:=[39,-39,1]; [n le 3 select I[n] else Self(n-1) - 10*Self(n-2) + 361*Self(n-3): n in [1..30]]; // _G. C. Greubel_, Dec 20 2017"
			],
			"xref": [
				"Cf. A240926, A078986, A097315, A247512."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Kival Ngaokrajang_, Sep 18 2014",
			"references": 7,
			"revision": 52,
			"time": "2018-06-14T11:47:11-04:00",
			"created": "2014-09-25T21:38:14-04:00"
		}
	]
}