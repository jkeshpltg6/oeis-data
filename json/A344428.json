{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A344428",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 344428,
			"data": "6,7,0,3,2,0,0,4,6,0,3,5,6,3,9,3,0,0,7,4,4,4,3,2,9,2,5,1,4,7,8,2,6,0,7,1,9,3,6,9,8,0,9,2,5,2,1,0,8,1,2,1,9,9,8,8,8,9,1,0,3,3,1,6,2,5,8,9,4,1,7,5,1,2,0,3,5,3,7,4,3,8,2,6,3,3,7,5,4,3,9",
			"name": "Decimal expansion of exp(-2/5).",
			"comment": [
				"Let f(s) = zeta(zeta(s+1)) - 1, where zeta(s) is the Riemann zeta function. Then f(s) is a strictly increasing function from (0, +oo) to (0, +oo), lim_{s-\u003e0+} f(s) = 0, lim_{s-\u003e+oo} f(s) = +oo.",
				"Conjecture:",
				"(i) f(s) has a unique fixed point s = A069995 - 1 in (0, +oo);",
				"(ii) Lim_{s-\u003e+oo} f(s)/2^s = 1, lim_{s-\u003e0+} f(s)/2^(-1/s) = exp(-2/5) = A344428.",
				"If these are true, let s_0 be any real number \u003e alpha, s_n = zeta(s_{n-1}) for n \u003e= 1, where alpha = A069995 is the fixed point of zeta(s) in (1, +oo), then {s_{2n}} diverges quickly to +oo, {s_{2n+1}} converges quickly to 1.",
				"This is because the derivative of zeta(zeta(s)) - s at s = alpha is (zeta'(alpha))^2 - 1 = A344427^2 - 1 \u003e 0, so (i) implies that zeta(zeta(s)) \u003e s for s \u003e alpha and zeta(zeta(s)) \u003c s for 1 \u003c s \u003c alpha, hence ... \u003e s_{2n} \u003e s_{2n-2} \u003e ... \u003e s_2 \u003e s_0 \u003e alpha \u003e s_1 \u003e s_3 \u003e ... \u003e s_{2n+1} \u003e ..., and it follows from (i) that lim_{n-\u003e+oo} s_{2n} = +oo, lim_{n-\u003e+oo} s_{2n+1} = 1. By definition s_n - 1 = f(s_{n-2} - 1), n \u003e= 2. For large n, s_{2n} - 1 is approximately equal to 2^(s_{2(n-1)} - 1), and 1/(s_{2n+1} - 1) is approximately equal to exp(2/5) * 2^(1/(s_{2(n-1)+1} - 1))."
			],
			"example": [
				"exp(-2/5) = 0.67032004603563930074... In comparison, (zeta(zeta(0.001+1)) - 1) / 2^(-1/0.001) = 0.67022226725425164463..."
			],
			"mathematica": [
				"RealDigits[Exp[-2/5], 10, 100][[1]] (* _Amiram Eldar_, May 19 2021 *)"
			],
			"program": [
				"(PARI) default(realprecision, 100); exp(-2/5)"
			],
			"xref": [
				"Cf. A069995, A344427."
			],
			"keyword": "nonn,cons",
			"offset": "0,1",
			"author": "_Jianing Song_, May 19 2021",
			"references": 2,
			"revision": 16,
			"time": "2021-05-19T10:24:35-04:00",
			"created": "2021-05-19T10:24:35-04:00"
		}
	]
}