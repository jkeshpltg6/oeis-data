{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A034017",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 34017,
			"data": "0,1,3,7,13,19,21,31,37,39,43,49,57,61,67,73,79,91,93,97,103,109,111,127,129,133,139,147,151,157,163,169,181,183,193,199,201,211,217,219,223,229,237,241,247,259,271,273,277,283,291,301,307,309,313,327,331",
			"name": "Numbers that are primitively represented by x^2 + xy + y^2.",
			"comment": [
				"Gives the location of the nonzero terms of A000086.",
				"Starting at a(3), a(n)^2 is the ordered semiperimeter of primitive integer Soddyian triangles (see A210484). - Frank M Jackson, Feb 04 2013",
				"A000086(a(n)) \u003e 0; a(n) = A004611(k) or a(n) = 3*A004611(k) for n \u003e 3 and an appropriate k. - Reinhard Zumkeller, Jun 23 2013",
				"The number of structure units in an icosahedral virus is 20*a(n)*k^2 for integers n and k, see Stannard link. - Charles R Greathouse IV, Nov 03 2015",
				"From _Wolfdieter Lang_, Apr 09 2021: (Start)",
				"The positive definite binary quadratic form F = [1, 1, 1], that is x^2 + x*y + y^2, has discriminant Disc = -3, and class number 1 (see Buell, Examples, p. 19, first line: Delta = -3, h = 1). This reduced form is equivalent to the form [1,-1, 1], but to no other reduced one (see Buell, Theorem 2.4, p. 15).",
				"This form F represents a positive integer k (= a(n)) properly if and only if A002061(j+1) = 2*T(j) + 1 = j^2 + j + 1 == 0 (mod k), for j from {0, 1, ..., k-1}. This congruence determines the representative parallel primitive forms (rpapfs) of discriminant Disc = -3 and representation of a positive integer number k, given by [k, 2*j+1, c(j)], and c(j) is  determined from Disc =-3 as c(j) = ((2*j+1)^2 + 3)/(4*k) = (j^2 + j + 1)/k. Each rpapf has a first reduced form, so called right neighbor form, namely [1, 1, 1] for k = 1 = a(1) (the already reduced parallel form from j = 0), and [1, -1, 1] for k = a(n), with n \u003e= 2.",
				"Only odd numbers k are eligible for representation, because 2*T(j) + 1, with the triangular numbers T = A000217, is odd. The odd k with at least one solution of the congruence are then the members of the present sequence.",
				"The solutions of the reduced forms F = [1, 1, 1] and F' = [1, -1, 1] representing k are related by type I equivalence because of the first two entries ([a, a, c] == [a, -a, c]), and also by type II equivalence because [a, b, a] == [a, -b, a], for positive b. These transfromation matrices are R_I =  Matrix([1, -1],[0, 1]) and R_{II} = Matrix([0, -1], [1, 0]), respectively, to obtain the forms with negative second entry from the ones with positive second entry. The corresponding solutions (x, y)^t (t for transposed) are related by the inverse of these matrices.",
				"The table with the A341422(n) solutions j of the congruence given above are given in A343232. (End)"
			],
			"reference": [
				"B. C. Berndt and R. A. Rankin, Ramanujan: Letters and Commentary, see p. 184, AMS, Providence, RI, 1995.",
				"D. A. Buell, Binary Quadratic Forms, Springer, 1989, pp. 15, 19."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A034017/b034017.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"N. J. A. Sloane et al., \u003ca href=\"https://oeis.org/wiki/Binary_Quadratic_Forms_and_OEIS\"\u003eBinary Quadratic Forms and OEIS\u003c/a\u003e (Index to related sequences, programs, references)",
				"Linda Stannard, \u003ca href=\"https://web.archive.org/web/20030216063201/http://web.uct.ac.za/depts/mmi/stannard/virarch.html\"\u003ePrinciples of Virus Architecture\u003c/a\u003e (archived version)"
			],
			"formula": [
				"n \u003e= 2: 3^{0 or 1} X product of primes of form 3a+1 (A002476) to any nonnegative power.",
				"The sequence {a(n)}_{n\u003e=2} gives the increasingly sorted positive numbers k such that the  set M(k) := {j = 0, 1, 2, ... , k-1 | j^2 + j + 1 == 0 (mod k)}, has cardinality \u003e= 1. - _Wolfdieter Lang_, Apr 09 2021"
			],
			"maple": [
				"N:= 1000: # to get all terms \u003c= N",
				"P:= select(isprime, [seq(6*n+1, n=1..floor((N-1)/6))]):",
				"A:= {1,3}:",
				"for p in P do",
				"  A:= {seq(seq(a*p^k, k=0..floor(log[p](N/a))),a=A)}:",
				"od:",
				"sort(convert(A,list)); # _Robert Israel_, Nov 04 2015"
			],
			"mathematica": [
				"lst = {0}; maxLen = 331; Do[If[Reduce[m^2 + m*n + n^2 == k \u0026\u0026 m \u003e= n \u003e= 0 \u0026\u0026 GCD[m, n] == 1, {m, n}, Integers] === False, , AppendTo[lst, k]], {k, maxLen}]; lst (* _Frank M Jackson_, Jan 10 2013 *) (* simplified by _T. D. Noe_, Feb 05 2013 *)"
			],
			"program": [
				"(PARI) is(n)=my(f=factor(n));for(i=1,#f[,1],if(f[i,1]%3!=1 \u0026\u0026 (f[i,1]!=3 || f[i,2]\u003e1), return(n==0))); 1 \\\\ _Charles R Greathouse IV_, Jan 10 2013",
				"(Haskell)",
				"a034017 n = a034017_list !! (n-1)",
				"a034017_list = 0 : filter ((\u003e 0) . a000086) [1..]",
				"-- _Reinhard Zumkeller_, Jun 23 2013"
			],
			"xref": [
				"Cf. A000217, A002061, A002476, A003136, A007645 (primes), A045611, A045897, A226946 (complement), A045897 (subsequence), A341422, A343232."
			],
			"keyword": "nonn,easy",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_.",
			"ext": [
				"Extended by _Ray Chandler_, Jan 29 2009"
			],
			"references": 15,
			"revision": 52,
			"time": "2021-05-23T05:22:38-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}