{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A264981",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 264981,
			"data": "1,1,1,1,1,1,1,1,9,1,1,1,1,1,1,1,1,9,1,1,1,1,1,1,1,1,9,1,1,1,1,1,1,1,1,9,1,1,1,1,1,1,1,1,9,1,1,1,1,1,1,1,1,9,1,1,1,1,1,1,1,1,9,1,1,1,1,1,1,1,1,9,1,1,1,1,1,1,1,1,81,1,1,1,1,1,1,1,1,9,1,1,1,1,1,1,1,1,9",
			"name": "Highest power of 9 dividing n.",
			"comment": [
				"The generalized binomial coefficients produced by this sequence provide an analog to Kummer's Theorem using arithmetic in base 9. - _Tom Edgar_, Feb 02 2016"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A264981/b264981.txt\"\u003eTable of n, a(n) for n = 1..6561\u003c/a\u003e",
				"Tyler Ball, Tom Edgar, and Daniel Juda, \u003ca href=\"http://dx.doi.org/10.4169/math.mag.87.2.135\"\u003eDominance Orders, Generalized Binomial Coefficients, and Kummer's Theorem\u003c/a\u003e, Mathematics Magazine, Vol. 87, No. 2, April 2014, pp. 135-143.",
				"Tom Edgar and Michael Z. Spivey, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL19/Edgar/edgar3.html\"\u003eMultiplicative functions, generalized binomial coefficients, and generalized Catalan numbers\u003c/a\u003e, Journal of Integer Sequences, Vol. 19 (2016), Article 16.1.6."
			],
			"formula": [
				"a(n) = 9^valuation(n,9). - _Tom Edgar_, Feb 02 2016",
				"G.f.: x/(1 - x) + 8 * Sum_{k\u003e=1} 9^(k-1)*x^(9^k)/(1 - x^(9^k)). - _Ilya Gutkovskiy_, Jul 10 2019"
			],
			"example": [
				"Since 18 = 9 * 2, a(18) = 9. Likewise, since 9 does not divide 17, a(17) = 1. - _Tom Edgar_, Feb 02 2016"
			],
			"mathematica": [
				"Table[9^Length@ TakeWhile[Reverse@ IntegerDigits[n, 9], # == 0 \u0026], {n, 99}] (* _Michael De Vlieger_, Dec 09 2015 *)",
				"9^Table[IntegerExponent[n, 9], {n, 150}] (* _Vincenzo Librandi_, Feb 03 2016 *)"
			],
			"program": [
				"(Scheme)",
				"(define (A264981 n) (let loop ((k 9)) (if (not (zero? (modulo n k))) (/ k 9) (loop (* 9 k)))))",
				"(PARI) a(n) = 9^valuation(n, 9); \\\\ _Michel Marcus_, Dec 08 2015",
				"(Sage) [9^valuation(i, 9) for i in [1..100]] # _Tom Edgar_, Feb 02 2016"
			],
			"xref": [
				"Cf. A264979.",
				"Similar sequences for other bases: A006519 (2), A038500 (3), A234957 (4), A060904 (5), A234959 (6)."
			],
			"keyword": "nonn,mult",
			"offset": "1,9",
			"author": "_Antti Karttunen_, Dec 07 2015",
			"ext": [
				"Keyword:mult added by _Andrew Howroyd_, Jul 20 2018"
			],
			"references": 2,
			"revision": 25,
			"time": "2019-07-10T21:24:43-04:00",
			"created": "2015-12-13T07:48:26-05:00"
		}
	]
}