{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A257656",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 257656,
			"data": "1,1,-1,1,-1,-4,-1,-6,-1,1,4,12,-1,14,6,-4,-1,-16,-1,-18,4,-6,-12,24,-1,21,-14,1,6,-28,4,-30,-1,12,16,24,-1,38,18,14,4,-40,6,-42,-12,-4,-24,48,-1,43,-21,-16,-14,-52,-1,-48,6,-18,28,60,4,62,30,-6,-1",
			"name": "Expansion of f(x) * f(x^3) * f(-x^4)^2 * chi(-x^6)^2 in powers of x where chi(), f() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A257656/b257656.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of eta(q^2)^3 * eta(q^4) * eta(q^6)^5 / (eta(q) * eta(q^3) * eta(q^12)^3) in powers of q.",
				"Euler transform of period 12 sequence [1, -2, 2, -3, 1, -6, 1, -3, 2, -2, 1, -4, ...].",
				"a(n) = a(3*n) = (-1)^n * A109039(n). a(2*n) = A109039(n).",
				"Multiplicative with a(2^e) = -1, a(p^e) = ((p*Kronecker(12, p))^(e+1) - 1)/(p*Kronecker(12, p) - 1) for odd prime p. - _Andrew Howroyd_, Jul 27 2018"
			],
			"example": [
				"G.f. = 1 + x - x^2 + x^3 - x^4 - 4*x^5 - x^6 - 6*x^7 - x^8 + x^9 + 4*x^10 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ -x] QPochhammer[ -x^3] QPochhammer[ x^4]^2 QPochhammer[ x^6, x^12]^2, {x, 0, n}];",
				"a[ n_] := If[ n\u003c1, Boole[n == 0], Times @@ (If[ # \u003c 5, -(-1)^#, With[ {t = # KroneckerSymbol[ 12, #]}, (t^(#2 + 1) - 1 ) / (t - 1)]]\u0026 @@@ FactorInteger @ n)];"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, n==0, -sumdiv(n, d, d * kronecker( 12, d) * (-1)^(n/d)))};",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A)^3 * eta(x^4 + A) * eta(x^6 + A)^5 / (eta(x + A) * eta(x^3 + A) * eta(x^12 + A)^3), n))};",
				"(PARI) {a(n) = my(A, p, e, t); if( n\u003c1, n==0, A = factor(n); prod(k=1, matsize(A)[1], [p, e] = A[k, ]; if( p\u003c5, -(-1)^p, t = p * kronecker( 12, p); (t^(e+1) - 1) / (t - 1))))};"
			],
			"xref": [
				"Cf. A109039."
			],
			"keyword": "sign,mult",
			"offset": "0,6",
			"author": "_Michael Somos_, Jul 25 2015",
			"references": 1,
			"revision": 19,
			"time": "2021-03-12T22:24:48-05:00",
			"created": "2015-07-25T23:19:47-04:00"
		}
	]
}