{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002981",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2981,
			"id": "M0908",
			"data": "0,1,2,3,11,27,37,41,73,77,116,154,320,340,399,427,872,1477,6380,26951,110059,150209",
			"name": "Numbers k such that k! + 1 is prime.",
			"comment": [
				"If n + 1 is prime then (by Wilson's theorem) n + 1 divides n! + 1. Thus for n \u003e 2 if n + 1 is prime n is not in the sequence. - _Farideh Firoozbakht_, Aug 22 2003",
				"For n \u003e 2, n! + 1 is prime \u003c==\u003e nextprime((n+1)!) \u003e (n+1)nextprime(n!) and we can conjecture that for n \u003e 2 if n! + 1 is prime then (n+1)! + 1 is not prime. - Mohammed Bouayoun (bouyao(AT)wanadoo.fr), Mar 03 2004",
				"The prime members are in A093804 (numbers n such that Sum_{d|n} d! is prime) since Sum_{d|n} d! = n! + 1 if n is prime. - _Jonathan Sondow_",
				"150209 is also in the sequence, cf. the link to Caldwell's prime pages. - _M. F. Hasler_, Nov 04 2011"
			],
			"reference": [
				"J.-M. De Koninck, Ces nombres qui nous fascinent, Entry 116, p. 40, Ellipses, Paris 2008.",
				"Harvey Dubner, Factorial and primorial primes, J. Rec. Math., 19 (No. 3, 1987), 197-203.",
				"Richard K. Guy, Unsolved Problems in Number Theory, Section A2.",
				"F. Le Lionnais, Les Nombres Remarquables, Paris, Hermann, 1983, p. 100.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"A. Borning, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1972-0308018-5 \"\u003eSome results for k!+-1 and 2.3.5...p+-1\u003c/a\u003e, Math. Comp., 26 (1972), 567-570.",
				"Chris K. Caldwell, \u003ca href=\"http://primes.utm.edu/top20/page.php?id=30\"\u003eFactorial Primes\u003c/a\u003e",
				"Chris K. Caldwell, \u003ca href=\"http://primes.utm.edu/primes/page.php?id=100445\"\u003e110059! + 1 on the Prime Pages\u003c/a\u003e",
				"Chris K. Caldwell, \u003ca href=\"http://primes.utm.edu/primes/page.php?id=102627\"\u003e150209! + 1 on the Prime Pages\u003c/a\u003e (Nov 03 2011).",
				"Chris K. Caldwell and Y. Gallot, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-01-01315-1\"\u003eOn the primality of n!+-1 and 2*3*5*...*p+-1\u003c/a\u003e, Math. Comp., 71 (2001), 441-448.",
				"H. Dubner, \u003ca href=\"/A006794/a006794.pdf\"\u003eFactorial and primorial primes\u003c/a\u003e, J. Rec. Math., 19 (No. 3, 1987), 197-203. (Annotated scanned copy)",
				"H. Dubner \u0026 N. J. A. Sloane, \u003ca href=\"/A002981/a002981.pdf\"\u003eCorrespondence, 1991\u003c/a\u003e",
				"R. K. Guy \u0026 N. J. A. Sloane, \u003ca href=\"/A005648/a005648.pdf\"\u003eCorrespondence, 1985\u003c/a\u003e",
				"N. Kuosa, \u003ca href=\"http://www.hut.fi/~nkuosa/primeform/\"\u003eSource for 6380.\u003c/a\u003e",
				"Des MacHale and Joseph Manning, \u003ca href=\"http://dx.doi.org/10.1017/mag.2015.28\"\u003eMaximal runs of strictly composite integers\u003c/a\u003e, The Mathematical Gazette, 99, pp 213-219 (2015).",
				"Romeo Mestrovic, \u003ca href=\"http://arxiv.org/abs/1202.3670\"\u003eEuclid's theorem on the infinitude of primes: a historical survey of its proofs (300 BC--2012) and another new proof\u003c/a\u003e, arXiv preprint arXiv:1202.3670 [math.HO], 2012. - From N. J. A. Sloane, Jun 13 2012",
				"Hisanori Mishima, \u003ca href=\"http://www.asahi-net.or.jp/~KC2H-MSM/mathland/matha1/matha104.htm\"\u003eFactors of N!+1\u003c/a\u003e",
				"Rudolf Ondrejka, \u003ca href=\"http://www.utm.edu/research/primes/lists/top_ten/\"\u003eThe Top Ten: a Catalogue of Primal Configurations\u003c/a\u003e",
				"Titus Piezas III, 2004. \u003ca href=\"http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.90.6646\u0026amp;rep=rep1\u0026amp;type=pdf\"\u003eSolving Solvable Sextics Using Polynomial Decomposition\u003c/a\u003e",
				"Maxie D. Schmidt, \u003ca href=\"https://arxiv.org/abs/1701.04741\"\u003eNew Congruences and Finite Difference Equations for Generalized Factorial Functions\u003c/a\u003e, arXiv:1701.04741 [math.CO], 2017.",
				"Apoloniusz Tyszka, \u003ca href=\"https://hal.archives-ouvertes.fr/hal-01625653/document\"\u003eA conjecture which implies that there are infinitely many primes of the form n!+1\u003c/a\u003e, Preprint, 2017.",
				"Apoloniusz Tyszka, \u003ca href=\"https://hal.archives-ouvertes.fr/hal-01614087v5/document\"\u003eA common approach to the problem of the infinitude of twin primes, primes of the form n! + 1, and primes of the form n! - 1\u003c/a\u003e, 2018.",
				"Apoloniusz Tyszka, \u003ca href=\"https://philarchive.org/rec/TYSDAS\"\u003eOn sets X subset of N for which we know an algorithm that computes a threshold number t(X) in N such that X is infinite if and only if X contains an element greater than t(X)\u003c/a\u003e, 2019.",
				"Apoloniusz Tyszka, \u003ca href=\"https://doi.org/10.13140/RG.2.2.28996.68486\"\u003eOn sets X, subset of N, whose finiteness implies that we know an algorithm which for every n, element of N, decides the inequality max (X) \u003c n\u003c/a\u003e, (2019).",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/FactorialPrime.html\"\u003eFactorial Prime\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/IntegerSequencePrimes.html\"\u003eInteger Sequence Primes\u003c/a\u003e",
				"\u003ca href=\"/index/Fa#factorial\"\u003eIndex entries for sequences related to factorial numbers\u003c/a\u003e"
			],
			"example": [
				"3! + 1 = 7 is prime, so 3 is in the sequence."
			],
			"mathematica": [
				"v = {0, 1, 2}; Do[If[ !PrimeQ[n + 1] \u0026\u0026 PrimeQ[n! + 1], v = Append[v, n]; Print[v]], {n, 3, 29651}]",
				"Select[Range[100], PrimeQ[#! + 1] \u0026] (* _Alonso del Arte_, Jul 24 2014 *)"
			],
			"program": [
				"(PARI) for(n=0,500,if(ispseudoprime(n!+1),print1(n\", \"))) \\\\ _Charles R Greathouse IV_, Jun 16 2011",
				"(MAGMA) [n: n in [0..800] | IsPrime(Factorial(n)+1)]; // _Vincenzo Librandi_, Oct 31 2018",
				"(Python)",
				"from sympy import factorial, isprime",
				"for n in range(0,800):",
				"    if isprime(factorial(n)+1):",
				"        print(n, end=', ') # _Stefano Spezia_, Jan 10 2019"
			],
			"xref": [
				"Cf. A002982 (n!-1 is prime), A064295. A088332 gives the primes.",
				"Equals A090660 - 1.",
				"Cf. A093804."
			],
			"keyword": "hard,more,nonn,nice",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Term 6380 sent in by _Jud McCranie_, May 08 2000",
				"Term 26951 from Ken Davis (kraden(AT)ozemail.com.au), May 24 2002",
				"Term 110059 found by PrimeGrid around Jun 11 2011, submitted by _Eric W. Weisstein_, Jun 13 2011",
				"Term 150209 by _Rene Dohmen_, Jun 09 2012"
			],
			"references": 111,
			"revision": 146,
			"time": "2020-04-17T04:32:52-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}