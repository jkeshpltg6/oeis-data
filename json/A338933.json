{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A338933",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 338933,
			"data": "2,16,128,1458,8192,11664,31250,65536,93312,235298,524288,1062882,2000000,3543122,3906250,5971968,9653618,15059072,22781250,28697814,33554432,47775744,48275138,68024448,80707214,94091762,128000000,171532242,226759808,250000000",
			"name": "Numbers k such that the Diophantine equation x^3 + y^3 + 2*z^3 = k has nontrivial primitive parametric solutions.",
			"comment": [
				"The data are derived from the following formula:",
				"(a^2 - a*t -t^2)^3 + (a^2 + a*t -t^2)^3 + 2*(t^2)^3 = 2*a^6",
				"(a^3 - 6*t^3)^3 + (a^3 + 6*t^3) + (-6*a*t^2)^3 = 2*a^9;",
				"(4*a^3 - 3*t^3)^3 + (4*a^3 + 3*t^3)^3 + (-6*a*t^2)^3 = 128*a^9;",
				"(9*a^3 - 2*t^3)^3 + (9*a^3 + 2*t^3)^3 + (-6*a*t^2)^3 = 1458*a^9;",
				"(36*a^3 - t^3)^3 + (36*a^3 + t^3)^3 + (-6*a*t^2)^3 = 93312*a^9;",
				"(6*a^3*t - 72*t^4)^3 + (72*t^4)^3 + 2*(a^4 - 36*a*t^3)^3 = 2*a^12;",
				"(6*a^3*t - 9*t^4)^3 + (9*t^4)^3 + 2*(2*a^4 - 9*a*t^3)^3 = 16*a^12 = 2*2^3*a^12;",
				"(18*a^3*t - 8*t^4)^3 + (8*t^4)^3 + 2*(9*a^4 - 12*a*t^3)^3 = 1458*a^12 = 2*9^3*a^12;",
				"(18*a^3*t - t^4)^3 + (t^4)^3 + 2*(18*a^4 - 3*a*t^3)^3 = 11664*a^12 = 2*18^3*a^12."
			],
			"reference": [
				"R. K. Guy, Unsolved Problems in Number Theory, D5."
			],
			"link": [
				"Kenji Koyama, \u003ca href=\"https://doi.org/10.1090/S0025-5718-00-01202-3\"\u003eOn searching for solutions of the Diophantine equation x^3 + y^3 + 2z^3 = n\u003c/a\u003e, Math. Comp, 69 (2000), 1735-1742.",
				"J. C. P. Miller \u0026 M. F. C. Woollett, \u003ca href=\"https://mathscinet.ams.org/mathscinet-getitem?mr=67916\"\u003eSolutions of the Diophantine equation x^3 + y^3 + z^3 = k\u003c/a\u003e, J. London Math. Soc. 30(1955), 101-110.",
				"Beniamino Segre, \u003ca href=\"https://mathscinet.ams.org/mathscinet-getitem?mr=46064\"\u003eOn the rational solutions of homogeneous cubic equations in four variables\u003c/a\u003e, Math. Notae, 11 (1951), 1-68."
			],
			"example": [
				"16 is a term, because when t is an integer, (6*(2*t + 1) - 9*(2*t + 1)^4, 9*(2*t + 1)^4, 2 - 9*(2*t + 1)^3) is a nontrivial primitive parametric solution of x^3 + y^3 + 2*z^3 = 16."
			],
			"mathematica": [
				"t1 = 2*Range[23]^6;",
				"t2 = 2*{1, 5, 7, 11}^9;",
				"t3 = 128*{1, 2, 4, 5}^9;",
				"t4 = 1458*{1, 3, 5, 7}^9;",
				"t5 = 93312*{1, 2, 3, 4}^9;",
				"t6 = 2*{1, 5, 7, 11}^12;",
				"t7 = 16*{1, 2, 4, 5}^12;",
				"t8 = 1458*{1, 3, 5, 7}^12;",
				"t9 = 11664*{1, 2, 3, 4}^12;",
				"Take[Union[t1, t2, t3, t4, t5, t6, t7, t8, t9], 30]"
			],
			"xref": [
				"Cf. A113490, A173515, A175365, A224215, A226903, A281224, A327586, A338932."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_XU Pingya_, Nov 16 2020",
			"references": 1,
			"revision": 22,
			"time": "2020-11-26T23:38:41-05:00",
			"created": "2020-11-26T23:38:41-05:00"
		}
	]
}