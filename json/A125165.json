{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A125165",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 125165,
			"data": "1,4,1,9,5,1,16,14,6,1,25,30,20,7,1,36,55,50,27,8,1,49,91,105,77,35,9,1,64,140,196,182,112,44,10,1,81,204,336,378,294,156,54,11,1,100,285,540,714,672,450,210,65,12,1,121,385,825,1254,1386,1122,660,275,77,13,1,144",
			"name": "Triangle read by rows: T(n,k) = C(n,k) + 3*C(n,k+1) + 2*C(n,k+2) (0\u003c=k\u003c=n).",
			"comment": [
				"Binomial transform of the infinite tridiagonal matrix M with main diagonal (1,1,1...), subdiagonal (3,3,3...) and subsubdiagonal (2,2,2...).",
				"Sum of entries in row n = 6*2^n-2n-5 = A050488(n+1).",
				"Riordan array ((1+x)/(1-x)^3, x/(1-x)). - _Philippe Deléham_, Dec 07 2013"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A125165/b125165.txt\"\u003eRows n=0..100 of triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = T(n-1,k) + T(n-1,k-1) for n\u003e=k\u003e=1.",
				"T(n,0) = (n+1)^2 = A000290(n+1).",
				"T(n,k) = 3*T(n-1,k) + T(n-1,k-1)-3*T(n-2,k)-2*T(n-2,k-1)+T(n-3,k)+T(n-2,k-1), T(0,0)=1, T(1,0)=4, T(1,1)=1, T(n,k)=0 if k\u003c0 or if k\u003en. - _Philippe Deléham_, Jan 10 2014",
				"exp(x) * e.g.f. for row n = e.g.f. for diagonal n. For example, for n = 3 we have exp(x)*(16 + 14*x + 6*x^2/2! + x^3/3!) = 16 + 30*x + 50*x^2/2! + 77*x^3/3! + 112*x^4/4! + .... The same property holds more generally for Riordan arrays of the form ( f(x), x/(1 - x) ). Cf. A233295. - _Peter Bala_, Dec 21 2014"
			],
			"example": [
				"Triangle starts:",
				"1;",
				"4, 1;",
				"9, 5, 1;",
				"16, 14, 6, 1;",
				"25, 30, 20, 7, 1;",
				"36, 55, 50, 27, 8, 1;",
				"49, 91, 105, 77, 35, 9, 1;"
			],
			"maple": [
				"T:=(n,k)-\u003ebinomial(n,k)+3*binomial(n,k+1)+2*binomial(n,k+2): for n from 0 to 11 do seq(T(n,k),k=0..n) od; # yields sequence in triangular form"
			],
			"mathematica": [
				"Table[Binomial[n,k]+3Binomial[n,k+1]+2Binomial[n,k+2],{n,0,15},{k,0,n}]//Flatten (* _Harvey P. Dale_, Nov 20 2016 *)"
			],
			"program": [
				"(PARI) for(n=0,15, for(k=0,n, print1(binomial(n,k) + 3*binomial(n,k+1) + 2*binomial(n,k+2), \", \"))) \\\\ _G. C. Greubel_, Oct 23 2018",
				"(MAGMA) [[Binomial(n,k) + 3*Binomial(n,k+1) + 2*Binomial(n,k+2): k in [0..n]]: n in [0..15]]; // _G. C. Greubel_, Oct 23 2018"
			],
			"xref": [
				"Cf. A050488, A000290."
			],
			"keyword": "nonn,tabl",
			"offset": "0,2",
			"author": "_Gary W. Adamson_, Nov 21 2006",
			"ext": [
				"Edited by _N. J. A. Sloane_, Dec 02 2006"
			],
			"references": 6,
			"revision": 26,
			"time": "2018-10-24T03:32:23-04:00",
			"created": "2006-12-06T03:00:00-05:00"
		}
	]
}