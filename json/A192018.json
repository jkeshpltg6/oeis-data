{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A192018",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 192018,
			"data": "1,3,2,1,6,6,5,3,1,11,13,14,12,10,5,1,19,24,30,31,31,28,19,7,1,32,42,56,66,74,78,77,61,32,9,1,53,71,98,124,152,175,196,203,180,118,49,11,1,87,118,166,218,284,349,419,485,525,502,384,207,70,13,1,142,194,276,370,499,645,812,998,1189,1331,1349,1152,749,336,95,15,1",
			"name": "Triangle read by rows: T(n,k) is the number of unordered pairs of nodes at distance k in the binary Fibonacci tree of order n (1\u003c=k\u003c=2n-3; entries in row n are the coefficients of the corresponding Wiener polynomial).",
			"comment": [
				"The binary Fibonacci trees f(k) of order k is a rooted binary tree defined as follows: 1. f(0) has no nodes and f(1) consists of a single node. 2. For k\u003e=2, f(k) is constructed from a root with f(k-1) as its left subtree and f(k-2) as its right subtree. See the Iyer \u0026 Reddy references.",
				"Row n contains 2n-3 entries.",
				"T(n,1) = A001911(n-1) (Fibonacci numbers minus 2).",
				"Sum_{k\u003e=1} k*T(n,k) = A192019(n) (the Wiener indices)."
			],
			"reference": [
				"K. Viswanathan Iyer and K. R. Udaya Kumar Reddy, Wiener index of Binomial trees and Fibonacci trees, Int'l. J. Math. Engin. with Comp., Accepted for publication, Sept. 2009."
			],
			"link": [
				"B. E. Sagan, Y-N. Yeh and P. Zhang, \u003ca href=\"http://dx.doi.org/10.1002/(SICI)1097-461X(1996)60:5\u0026lt;959::AID-QUA2\u0026gt;3.0.CO;2-W\"\u003eThe Wiener Polynomial of a Graph\u003c/a\u003e, Internat. J. of Quantum Chem., 60, 1996, 959-969.",
				"K. Viswanathan Iyer and K. R. Udaya Kumar Reddy, \u003ca href=\"http://arxiv.org/abs/0910.4432\"\u003eWiener index of binomial trees and Fibonacci trees\u003c/a\u003e, arXiv:0910.4432 [cs.DM], 2009."
			],
			"formula": [
				"The Wiener polynomial w(n,t) of the binary Fibonacci tree of order n satisfies the recurrence relation w(n,t) = w(n-1,t) + w(n-2,t) + t*r(n-1,t) + t*r(n-2) + t^2*r(n-1,t)*r(n-2,t), w(1,t)=0, w(2,t)=t, where r(n,t) is the generating polynomial of the nodes of the binary Fibonacci tree f(n) with respect to the level of the nodes (for example, r(2,t) = 1 + t for the tree / ; see A004070 and the Maple program)."
			],
			"example": [
				"Triangle starts:",
				"   1;",
				"   3,  2,  1;",
				"   6,  6,  5,  3,  1;",
				"  11, 13, 14, 12, 10,  5,  1;",
				"  19, 24, 30, 31, 31, 28, 19,  7,  1;"
			],
			"maple": [
				"G := z/((1-z)*(1-t*z-t*z^2)): Gser := simplify(series(G, z = 0, 13)): for n to 10 do r[n] := sort(coeff(Gser, z, n)) end do; w[1] := 0: w[2] := t: for n from 3 to 10 do w[n] := sort(expand(w[n-1]+w[n-2]+t*r[n-1]+t*r[n-2]+t^2*r[n-1]*r[n-2])) end do: for n from 2 to 10 do seq(coeff(w[n], t, k), k = 1 .. 2*n-3) end do; # yields sequence in triangular form"
			],
			"xref": [
				"Cf. A001911, A192019."
			],
			"keyword": "nonn,tabf",
			"offset": "2,2",
			"author": "_Emeric Deutsch_, Jun 21 2011",
			"references": 1,
			"revision": 11,
			"time": "2016-12-10T03:00:16-05:00",
			"created": "2011-06-22T10:01:13-04:00"
		}
	]
}