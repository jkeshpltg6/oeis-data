{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A343805",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 343805,
			"data": "1,1,1,1,4,7,1,9,39,87,1,16,126,608,1553,1,25,310,2470,12985,36145,1,36,645,7560,62595,351252,1037367,1,49,1197,19285,225715,1946259,11481631,35402983,1,64,2044,43232,673190,8011136,71657404,439552864,1400424097",
			"name": "T(n, k) = [x^k] n! [t^n] 1/(exp((V*(2 + V))/(4*t))*sqrt(1 + V)) where V = W(-2*t*x) and W denotes the Lambert function. Table read by rows, T(n, k) for 0 \u003c= k \u003c= n.",
			"comment": [
				"The rows of the triangle give the coefficients of the Ehrhart polynomials of integral Coxeter permutahedra of type B. These polynomials count lattice points in a dilated lattice polytope. For a definition see Ardila et al. (p. 1158), the generating functions of these polynomials for the classical root systems are given in theorem 5.2 (p. 1163)."
			],
			"link": [
				"Federico Ardila, Matthias Beck, and Jodi McWhirter, \u003ca href=\"https://doi.org/10.18257/raccefyn.1189\"\u003eThe arithmetic of Coxeter permutahedra\u003c/a\u003e, Rev. Acad. Colomb. Cienc. Ex. Fis. Nat. 44(173):1152-1166, 2020."
			],
			"example": [
				"Triangle starts:",
				"[0] 1;",
				"[1] 1,  1;",
				"[2] 1,  4,    7;",
				"[3] 1,  9,   39,    87;",
				"[4] 1, 16,  126,   608,   1553;",
				"[5] 1, 25,  310,  2470,  12985,   36145;",
				"[6] 1, 36,  645,  7560,  62595,  351252,  1037367;",
				"[7] 1, 49, 1197, 19285, 225715, 1946259, 11481631,  35402983;",
				"[8] 1, 64, 2044, 43232, 673190, 8011136, 71657404, 439552864, 1400424097;"
			],
			"maple": [
				"alias(W = LambertW):",
				"EhrB := exp(-W(-2*t*x)/(2*t) - W(-2*t*x)^2/(4*t))/sqrt(1+W(-2*t*x)):",
				"ser := series(EhrB, x, 10): cx := n -\u003e n!*coeff(ser, x, n):",
				"T := n -\u003e seq(coeff(cx(n), t, k), k=0..n): seq(T(n), n = 0..9);"
			],
			"mathematica": [
				"P := ProductLog[-2 t x]; gf := 1/(E^((P (2 + P))/(4 t)) Sqrt[1 + P]);",
				"ser := Series[gf, {x, 0, 10}]; cx[n_] := n! Coefficient[ser, x, n];",
				"Table[CoefficientList[cx[n], t], {n, 0, 8}] // Flatten"
			],
			"xref": [
				"Cf. A138464 (type A), this sequence (type B), A343806 (type C), A343807 (type D)."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Peter Luschny_, May 01 2021",
			"references": 3,
			"revision": 14,
			"time": "2021-05-02T06:29:27-04:00",
			"created": "2021-05-01T10:00:29-04:00"
		}
	]
}