{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A320917",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 320917,
			"data": "1,15,70,219,546,1050,2150,3315,5299,8190,13542,15330,26690,32250,38220,51491,79170,79485,124166,119574,150500,203130,268710,232050,330771,400350,419020,470850,684546,573300,895622,811395,947940,1187550,1173900,1160481,1826210,1862490,1868300,1809990",
			"name": "a(n) = sigma_2(n)*sigma_3(n)/sigma(n).",
			"comment": [
				"Multiplicative, because products and quotients of multiplicative functions are always multiplicative. a(p^k) for fixed k is trivially a rational function of p. The proofs below show that a(n) is always an integer, and hence a(p^k) is a polynomial in p. (Note that a(n^k) is not equal to this polynomial when n is composite.)",
				"Proof from _Robert Gerbicz_ that this is always an integer:",
				"First consider r=sigma_2(p^k)/sigma(p^k). Let x=p^k, then",
				"r=(p^(2*k+2)-1)/(p^2-1)*(p-1)/(p^(k+1)-1)=(p^2*x^2-1)/(p^2-1)*(p-1)/(p*x-1)=(p*x+1)/(p+1)",
				"Case a: k is even, then x==1 mod (p+1), so (p*x+1)==-1+1==0 mod (p+1). So here even r is an integer.",
				"Case b: k is odd, then",
				"  sigma_3(p^k)=(p^3*x^3-1)/(p^3-1), and we must multiple this by r, and here",
				"  p^3*x^3-1==0 mod (p+1)",
				"  p^3-1==-2 mod (p+1)",
				"  This means that if p=2 then sigma_3(p^k) is divisible by (p+1), so we have proved the theorem.",
				"  If p is odd, then (p+1)/2 divides sigma_3(p^k), but we have another factor of 2 in (p*x+1), because p and x is odd.",
				"It appears that a stronger result is also true: sigma_3(p^k) is divisible by (p+1) if k is odd.",
				"Proof from _Giovanni Resta_ that this is always an integer:",
				"We have sigma_2(p^k)*sigma_3(p^k)/sigma(p^k) =",
				"((p^(2+2k)-1)/(p^2-1) * (p^(3+3k)-1)/(p^3-1)) / ((p^(1+k)-1)/(p-1)) =",
				"((p^(k+1)+1) (p^(3 k+3)-1)) / ((p+1) (p^3-1)),",
				"because p^(2+2k)-1 is the difference of two squares and we can use x^2-1 = (x+1)(x-1).",
				"We observe that (p^(3 k+3)-1)) is always divisible by (p^3-1) (it is indeed sigma_3(p^k)).",
				"Now, if k is even, k+1 is odd, so p^(k+1)+1 is divisible by p+1 giving 1-p+p^2-p^3... +p^k as result, and we are done.",
				"If k is odd, k+1 is even and in general p^(k+1)+1 is not divisible by p+1 (just as p^2+1 is not divisible by p+1).",
				"However, if k is odd, then 3k+3 is even, so p^(3k+3)-1 beside being divisible by p^3-1 is also divisible by p+1. Since p+1 and p^3-1 have no common factors, then the ratio (p^(3 k+3)-1)) / ((p+1) (p^3-1)) is an integer and we are done."
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A320917/b320917.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = sigma_2(n)*sigma_3(n)/sigma(n)."
			],
			"mathematica": [
				"a[n_] := DivisorSigma[2, n] * DivisorSigma[3, n] / DivisorSigma[1, n]; Array[a, 40] (* _Amiram Eldar_, Aug 01 2019 *)"
			],
			"program": [
				"(PARI) a(n) = sigma(n,2)*sigma(n,3)/sigma(n)"
			],
			"xref": [
				"Cf. A000203, A001157, A001158, A320416."
			],
			"keyword": "nonn,mult",
			"offset": "1,2",
			"author": "_Franklin T. Adams-Watters_, Oct 24 2018",
			"references": 2,
			"revision": 16,
			"time": "2019-08-01T11:33:44-04:00",
			"created": "2018-10-26T16:28:40-04:00"
		}
	]
}