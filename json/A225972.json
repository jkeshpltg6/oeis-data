{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A225972",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 225972,
			"data": "0,0,1,6,14,32,55,94,140,208,285,390,506,656,819,1022,1240,1504,1785,2118,2470,2880,3311,3806,4324,4912,5525,6214,6930,7728,8555,9470,10416,11456,12529,13702,14910,16224,17575,19038,20540,22160,23821,25606,27434,29392",
			"name": "The number of binary pattern classes in the (2,n)-rectangular grid with 3 '1's and (2n-3) '0's: two patterns are in same class if one of them can be obtained by a reflection or 180-degree rotation of the other.",
			"comment": [
				"Also the edge count of the n X n black bishop graph. - _Eric W. Weisstein_, Jun 26 2017"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A225972/b225972.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/BlackBishopGraph.html\"\u003eBlack Bishop Graph\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/EdgeCount.html\"\u003eEdge Count\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,1,-4,1,2,-1)."
			],
			"formula": [
				"a(n) = A000330(n) + A142150(n) = (n-1)*(4*n^2 - 2*n - 3*(-1)^n + 3)/12.",
				"a(n) = 2*a(n-1) + a(n-2) - 4*a(n-3) + a(n-4) + 2*a(n-5) - a(n-6) with n \u003e 5, a(0)=0, a(1)=0, a(2)=1, a(3)=6, a(4)=14, a(5)=32.",
				"a(n) = 4*a(n-1) - 6*a(n-2) + 4*a(n-3) - a(n-4) + 4*(n-4)*(-1)^n with n \u003e 3, a(0)=0, a(1)=0, a(2)=1, a(3)=6.",
				"G.f.: x^2*(1 + 4*x + x^2 + 2*x^3)/((1+x)^2*(1-x)^4). - _Bruno Berselli_, May 29 2013",
				"a(n) = (1/4)*(binomial(2*(n-1),3) + 2*binomial(n-2,1)*(1/2)*(1+(-1)^n)). - _Yosu Yurramendi_ and _María Merino_, Aug 21 2013",
				"a(n) = A005993(n-2) + A199771(n-1), n \u003e= 2. - _Christopher Hunt Gribble_, Mar 02 2014"
			],
			"maple": [
				"A225972:=n-\u003e(n-1)*(4*n^2-2*n-3*(-1)^n+3)/12; seq(A225972(n), n=0..40); # _Wesley Ivan Hurt_, Mar 02 2014"
			],
			"mathematica": [
				"Table[(n - 1)*(4*n^2 - 2*n - 3*(-1)^n + 3)/12, {n, 0, 40}] (* _Bruno Berselli_, May 29 2013 *)",
				"CoefficientList[Series[x^2 (1 + 4 x + x^2 + 2 x^3) / ((1 + x)^2 (1 - x)^4), {x, 0, 50}], x] (* _Vincenzo Librandi_, Sep 04 2013 *)",
				"LinearRecurrence[{2, 1, -4, 1, 2, -1}, {0, 1, 6, 14, 32, 55}, 20] (* _Eric W. Weisstein_, Jun 27 2017 *)"
			],
			"program": [
				"(R) a \u003c- vector()",
				"    for(n in 0:40) a[n] \u003c- (1/4)*(choose(2*(n-1),3) + 2*choose(n-2,1)*(1/2)*(1+(-1)^n))",
				"    a  # _Yosu Yurramendi_ and _María Merino_, Aug 21 2013",
				"(MAGMA) [(1/4)*(Binomial(2*(n-1),3)+2*Binomial(n-2,1)*(1/2)*(1+(-1)^n)): n in [1..50]]; // _Vincenzo Librandi_, Sep 04 2013"
			],
			"xref": [
				"Cf. A226048, A000330.",
				"Cf. A289179 (edge count of white bishop graph)."
			],
			"keyword": "nonn,easy",
			"offset": "0,4",
			"author": "_Yosu Yurramendi_, May 26 2013",
			"ext": [
				"More terms from _Vincenzo Librandi_, Sep 04 2013"
			],
			"references": 4,
			"revision": 62,
			"time": "2019-07-22T14:52:17-04:00",
			"created": "2013-05-30T03:39:24-04:00"
		}
	]
}