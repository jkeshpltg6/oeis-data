{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A347944",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 347944,
			"data": "0,1,63,71,75,83,85,87,89,113,115,117,120,129,133,138,139,141,151,155,156,159,161,162,163,172,179,181,182,184,185,189,190,191,199,201,205,209,212,213,215,216,217,220,221,222,233,235,236,239,242,243,245,247,248,250",
			"name": "Numbers that cannot be written as the sum of two (not necessarily distinct) terms in A000009.",
			"comment": [
				"Most natural numbers are here: this sequence has natural density 1. Proof: Let S be the set of numbers can be written as the sum of two terms in A000009. For A000009(n-1) \u003c N \u003c= A000009(n), at most n^2 numbers among [0,N] are in S (since if N = A000009(m) + A000009(k) then m,k \u003c= n-1), so #(S intersect {0,1,...,N})/#{0,1,...,N} \u003c= n^2/(A000009(n-1)+2) -\u003e 0 as N goes to infinity.",
				"This also means that lim_{n-\u003eoo} a(n)/n = 1. Proof: Since this is a list we have a(n) \u003e= n-1, so liminf_{n-\u003eoo} a(n)/n \u003e= 1. If limsup_{n-\u003eoo} a(n)/n \u003e 1, there exists eps \u003e 0 and n_1 \u003c n_2 \u003c ... \u003c n_i \u003c ... such that a(n_i)/(n_i) \u003e 1+eps for all i, then #({0,1,...,a(n_i)}\\S)/#{0,1,...,a(n_i)} = #{a(1),a(2),...,a(n_i)}/#{0,1,...,a(n_i)} = (n_i)/(a(n_i)+1) \u003c 1/(1+eps) for all i, contradicting with the fact that this sequence has natural density 1. Hence limsup_{n-\u003eoo} a(n)/n \u003c= 1, so lim_{n-\u003eoo} a(n)/n = 1.",
				"Note that although 89 is in A000009, it is not the sum of two terms there."
			],
			"link": [
				"Jianing Song, \u003ca href=\"/A347944/b347944.txt\"\u003eTable of n, a(n) for n = 1..27999\u003c/a\u003e (all terms \u003c= 30000)"
			],
			"example": [
				"63 is a term since it is not the sum of two terms in A000009.",
				"61 is not a term since 61 = 15 + 46.",
				"73 is not a term since 73 = 27 + 46."
			],
			"mathematica": [
				"Select[Range[0,250],!ContainsAny[p,k=1;While[Max[p=PartitionsQ/@Range@k++]\u003c#];#-Union@Most@p]\u0026] (* _Giorgos Kalogeropoulos_, Sep 21 2021 *)"
			],
			"program": [
				"(PARI) leng(n) = for(l=0, oo, if(A000009(l)\u003en-1, return(l))) \\\\ See A000009 for its program; A000009(0), A000009(1), ..., A000009(l-1) \u003c= n-1",
				"v(n) = my(l=leng(n), v=[]); for(i=0, l-1, v=concat(v, vector(l, j, A000009(i)+A000009(j-1)))); v=vecsort(v); v",
				"list_zero(n) = setminus([0..n],Set(v(n)))"
			],
			"xref": [
				"Cf. A000009, A347943."
			],
			"keyword": "nonn,easy",
			"offset": "1,3",
			"author": "_Jianing Song_, Sep 20 2021",
			"references": 2,
			"revision": 25,
			"time": "2021-10-06T03:27:12-04:00",
			"created": "2021-09-20T17:41:41-04:00"
		}
	]
}