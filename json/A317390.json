{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A317390",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 317390,
			"data": "2,1,5,25,3,7,43,29,4,11,211,61,37,6,15,638,261,91,40,8,23,664,848,421,111,41,9,26,1613,1956,921,426,121,49,10,27,2991,3321,2058,969,441,124,51,12,28,7021,3004,3336,2092,1002,484,171,52,13,31,11306,7162,3319,3368,2094,1026,535,184,67,14,33",
			"name": "A(n,k) is the n-th positive integer that has exactly k representations of the form 1 + p1 * (1 + p2* ... * (1 + p_j)...), where [p1, ..., p_j] is a (possibly empty) list of distinct primes; square array A(n,k), n\u003e=1, k\u003e=0, read by antidiagonals.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A317390/b317390.txt\"\u003eAntidiagonals n = 1..34, flattened\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"A317241(A(n,k)) = k."
			],
			"example": [
				"A(6,2) = 49: 1 + 3 * (1 + 5 * (1 + 2)) = 1 + 2 * (1 + 23) = 49.",
				"Square array A(n,k) begins:",
				"   2,  1, 25,  43, 211,  638,  664, 1613, 2991, ...",
				"   5,  3, 29,  61, 261,  848, 1956, 3321, 3004, ...",
				"   7,  4, 37,  91, 421,  921, 2058, 3336, 3319, ...",
				"  11,  6, 40, 111, 426,  969, 2092, 3368, 3554, ...",
				"  15,  8, 41, 121, 441, 1002, 2094, 3741, 3928, ...",
				"  23,  9, 49, 124, 484, 1026, 2283, 3914, 4846, ...",
				"  26, 10, 51, 171, 535, 1106, 2381, 3979, 5552, ...",
				"  27, 12, 52, 184, 540, 1156, 2388, 4082, 5886, ...",
				"  28, 13, 67, 187, 591, 1191, 2432, 4126, 6293, ..."
			],
			"maple": [
				"b:= proc(n, s) option remember; `if`(n=1, 1, add(b((n-1)/p,",
				"      s union {p}) , p=numtheory[factorset](n-1) minus s))",
				"    end:",
				"A:= proc() local h, p, q; p, q:= proc() [] end, 0;",
				"      proc(n, k)",
				"        while nops(p(k))\u003cn do q:= q+1;",
				"          h:= b(q, {});",
				"          p(h):= [p(h)[], q]",
				"        od; p(k)[n]",
				"      end",
				"    end():",
				"seq(seq(A(n, d-n), n=1..d), d=1..10);"
			],
			"mathematica": [
				"b[n_, s_List] := b[n, s] = If[n == 1, 1, Sum[If[p == 1, 0, b[(n - 1)/p, s  ~Union~ {p}]], {p, FactorInteger[n - 1][[All, 1]] ~Complement~ s}]];",
				"A[n_, k_] := Module[{h, p, q = 0}, p[_] = {}; While[Length[p[k]] \u003c n, q = q + 1; h = b[q, {}]; p[h] = Append[p[h], q]]; p[k][[n]]];",
				"Table[Table[A[n, d - n], {n, 1, d}], {d, 1, 11}] // Flatten (* _Jean-François Alcover_, Dec 06 2019, from Maple *)"
			],
			"xref": [
				"Columns k=0-10 give: A317242, A317391, A317392, A317393, A317394, A317395, A317396, A317397, A317398, A317399, A317400.",
				"Row n=1 gives A317385.",
				"A(n,n) gives A317537.",
				"Cf. A317241."
			],
			"keyword": "nonn,tabl",
			"offset": "1,1",
			"author": "_Alois P. Heinz_, Jul 27 2018",
			"references": 14,
			"revision": 22,
			"time": "2019-12-06T08:53:27-05:00",
			"created": "2018-07-27T09:51:43-04:00"
		}
	]
}