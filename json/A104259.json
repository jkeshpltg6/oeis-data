{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A104259",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 104259,
			"data": "1,2,1,5,4,1,15,14,6,1,51,50,27,8,1,188,187,113,44,10,1,731,730,468,212,65,12,1,2950,2949,1956,970,355,90,14,1,12235,12234,8291,4356,1785,550,119,16,1,51822,51821,35643,19474,8612,3021,805,152,18,1",
			"name": "Triangle T read by rows: matrix product of Pascal and Catalan triangle.",
			"comment": [
				"Also, Riordan array (G,G), G(t)=(1 - ((1-5*t)/(1-t))^(1/2))/(2*t).",
				"From _Emanuele Munarini_, May 18 2011: (Start)",
				"Row sums = A002212.",
				"Diagonal sums = A190737.",
				"Central coefficients = A190738. (End)"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A104259/b104259.txt\"\u003eTable of n, a(n) for n = 0..5049\u003c/a\u003e",
				"D. Merlini, R. Sprugnoli and M. C. Verri, \u003ca href=\"https://doi.org/10.1007/978-3-0348-8405-1_11\"\u003eAn algebra for proper generating trees\u003c/a\u003e, Mathematics and Computer Science, Part of the series Trends in Mathematics pp 127-139, 2000. [\u003ca href=\"https://www.researchgate.net/publication/2386023_An_Algebra_for_Proper_Generating_Trees\"\u003ealternative link\u003c/a\u003e]",
				"D. Merlini, R. Sprugnoli and M. C. Verri, \u003ca href=\"http://local.disia.unifi.it/merlini/papers/Lucidi.ps\"\u003eAn algebra for proper generating trees\u003c/a\u003e, Colloquium on Mathematics and Computer Science, Versailles, September 2000."
			],
			"formula": [
				"T(n,k) = sum(binomial(n,i)*binomial(2*i-k,i-k)*(k+1)/(i+1),i=k..n).",
				"T(n+1,k+2) = T(n+1,k+1) + T(n,k+2) - T(n,k+1) - T(n,k). - _Emanuele Munarini_, May 18 2011",
				"T(n,k) = T(n-1,k-1) + 2*T(n-1,k) + Sum_{i, i\u003e=0} T(n-1,k+1+i). - _Philippe Deléham_, Feb 23 2012",
				"T(n,k) = C(n,k)*hypergeom([k/2+1/2,k/2+1,k-n],[k+1,k+2],-4). - _Peter Luschny_, Sep 23 2014"
			],
			"example": [
				"Triangle begins:",
				"1",
				"2, 1",
				"5, 4, 1",
				"15, 14, 6, 1",
				"51, 50, 27, 8, 1",
				"188, 187, 113, 44, 10, 1",
				"731, 730, 468, 212, 65, 12, 1",
				"2950, 2949, 1956, 970, 355, 90, 14, 1",
				"12235, 12234, 8291, 4356, 1785, 550, 119, 16, 1",
				"Production matrix begins",
				"2, 1",
				"1, 2, 1",
				"1, 1, 2, 1",
				"1, 1, 1, 2, 1",
				"1, 1, 1, 1, 2, 1",
				"1, 1, 1, 1, 1, 2, 1",
				"1, 1, 1, 1, 1, 1, 2, 1",
				"... - _Philippe Deléham_, Mar 01 2013"
			],
			"maple": [
				"T := (n,k) -\u003e binomial(n,k)*hypergeom([k/2+1/2,k/2+1,k-n],[k+1,k+2],-4); seq(print(seq(round(evalf(T(n,k),99)),k=0..n)),n=0..8); # _Peter Luschny_, Sep 23 2014",
				"# Alternative:",
				"N:= 12:  # to get the first N rows",
				"P:= Matrix(N,N,(i,j) -\u003e binomial(i-1,j-1), shape=triangular[lower]):",
				"C:= Matrix(N,N,(i,j) -\u003e binomial(2*i-j-1,i-j)*j/i, shape=triangular[lower]):",
				"T:= P . C:",
				"for i from 1 to N do",
				"seq(T[i,j],j=1..i)",
				"od;   # _Robert Israel_, Sep 23 2014"
			],
			"mathematica": [
				"Flatten[Table[Sum[Binomial[n,i]Binomial[2i-k,i-k](k+1)/(i+1),{i,k,n}],{n,0,100},{k,0,n}]] (* _Emanuele Munarini_, May 18 2011 *)"
			],
			"program": [
				"(Maxima) create_list(sum(binomial(n,i)*binomial(2*i-k,i-k)*(k+1)/(i+1),i,k,n),n,0,12,k,0,n); /* _Emanuele Munarini_, May 18 2011 */"
			],
			"xref": [
				"T = A007318 * A033184.",
				"Left-hand columns include A007317, A007317 - 1. Row sums are in A002212."
			],
			"keyword": "nonn,tabl",
			"offset": "0,2",
			"author": "_Ralf Stephan_, Mar 17 2005",
			"references": 10,
			"revision": 36,
			"time": "2020-08-13T12:13:15-04:00",
			"created": "2005-04-09T03:00:00-04:00"
		}
	]
}