{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A019988",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 19988,
			"data": "1,2,5,16,55,222,950,4265,19591,91678,434005,2073783,9979772,48315186,235088794,1148891118,5636168859",
			"name": "Number of ways of embedding a connected graph with n edges in the square lattice.",
			"comment": [
				"It is assumed that all edges have length one. - _N. J. A. Sloane_, Apr 17 2019",
				"These are referred to as 'polysticks', 'polyedges' or 'polyforms'. - _Jack W Grahl_, Jul 24 2018",
				"Number of connected subgraphs of the square lattice (or grid) containing n length-one line segments. Configurations differing only a rotation or reflection are not counted as different. The question may also be stated in terms of placing unit toothpicks in a connected arrangement on the square lattice. - _N. J. A. Sloane_, Apr 17 2019",
				"The solution for n=5 features in the card game Digit. - Paweł Rafał Bieliński, Apr 17 2019"
			],
			"reference": [
				"Brian R. Barwell, \"Polysticks,\" Journal of Recreational Mathematics 22 (1990), 165-175."
			],
			"link": [
				"D. Goodger, \u003ca href=\"http://puzzler.sourceforge.net/docs/polysticks-intro.html\"\u003eAn introduction to Polysticks\u003c/a\u003e",
				"M. Keller, \u003ca href=\"http://www.solitairelaboratory.com/polyenum.html\"\u003eCounting polyforms\u003c/a\u003e",
				"D. Knuth, \u003ca href=\"https://arxiv.org/abs/cs/0011047\"\u003eDancing Links\u003c/a\u003e, arXiv:cs/0011047 [cs.DS], 2000. (A discussion of backtracking algorithms which mentions some problems of polystick tiling.)",
				"Ed Pegg, Jr., \u003ca href=\"http://demonstrations.wolfram.com/PolyformExplorer/\"\u003eIllustrations of polyforms\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/A019988/a019988.png\"\u003eIllustration of a(1)-a(4)\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Polyedge.html\"\u003ePolyedge\u003c/a\u003e",
				"Wikicommons, \u003ca href=\"https://commons.wikimedia.org/wiki/File:Polysticks.svg\"\u003ePolysticks\u003c/a\u003e \u003ca href=\"https://commons.wikimedia.org/wiki/File:Free_connected_5-sticks_square_lattice.svg\"\u003e5-sticks\u003c/a\u003e \u003ca href=\"https://commons.wikimedia.org/wiki/File:Free_connected_6-sticks_square_lattice.svg\"\u003e6-sticks\u003c/a\u003e \u003ca href=\"https://commons.wikimedia.org/wiki/File:Free_connected_7-sticks_square_lattice.svg\"\u003e7-sticks\u003c/a\u003e"
			],
			"formula": [
				"A348095(n) + A056841(n+1) = a(n). - _R. J. Mathar_, Sep 30 2021"
			],
			"xref": [
				"If only translations (but not rotations) are factored, consider fixed polyedges (A096267).",
				"If reflections are considered different, we obtain the one-sided polysticks, counted by (A151537). - _Jack W Grahl_, Jul 24 2018",
				"Cf. A001997, A003792, A006372, A059103, A085632, A056841 (tree-like), A348095 (with cycles), A348096 (refined by symmetry).",
				"See A336281 for another version."
			],
			"keyword": "nonn,nice,hard,more",
			"offset": "1,2",
			"author": "_Russ Cox_",
			"ext": [
				"More terms from Brendan Owen (brendan_owen(AT)yahoo.com), Feb 20 2002"
			],
			"references": 11,
			"revision": 54,
			"time": "2021-10-05T03:10:10-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}