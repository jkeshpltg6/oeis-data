{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A091337",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 91337,
			"data": "1,0,-1,0,-1,0,1,0,1,0,-1,0,-1,0,1,0,1,0,-1,0,-1,0,1,0,1,0,-1,0,-1,0,1,0,1,0,-1,0,-1,0,1,0,1,0,-1,0,-1,0,1,0,1,0,-1,0,-1,0,1,0,1,0,-1,0,-1,0,1,0,1,0,-1,0,-1,0,1,0,1,0,-1,0,-1,0,1,0,1,0,-1,0,-1,0,1,0,1,0,-1,0,-1,0,1,0,1,0,-1,0,-1,0,1,0,1",
			"name": "a(n) = (2/n), where (k/n) is the Kronecker symbol.",
			"comment": [
				"Sinh(1) in 'reflected factorial' base is 1.01010101010101010101010101010101010101010101... see A073097 for cosh(1). - _Robert G. Wilson v_, May 04 2005",
				"A non-principal character for the Dirichlet L-series modulo 8, see arXiv:1008.2547 and L-values Sum_{n \u003e= 1} a(n)/n^s in eq (318) by Jolley. - _R. J. Mathar_, Oct 06 2011",
				"Period 8: repeat [1, 0, -1, 0, -1, 0, 1, 0]. - _Wesley Ivan Hurt_, Sep 07 2015",
				"More generally: a(n) = (2^(2i+1)/n), where (k/n) is the Kronecker symbol and i \u003e= 0. - _A.H.M. Smeets_, Jan 23 2018"
			],
			"reference": [
				"L. B. W. Jolley, Summation of series, Dover (1961)."
			],
			"link": [
				"John M. Campbell, \u003ca href=\"http://arxiv.org/abs/1105.3399\"\u003eAn Integral Representation of Kekulé Numbers, and Double Integrals Related to Smarandache Sequences\u003c/a\u003e, arXiv:1105.3399 [math.GM], 2011.",
				"R. J. Mathar, \u003ca href=\"http://arxiv.org/abs/1008.2547\"\u003eTable of Dirichlet L-series...\u003c/a\u003e, arXiv:1008.2547 [math.NT], 2010, 2015, L(m=8,r=2,s).",
				"Michael Somos, \u003ca href=\"http://grail.eecs.csuohio.edu/~somos/rfmc.html\"\u003eRational Function Multiplicative Coefficients\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/KroneckerSymbol.html\"\u003eKronecker Symbol\u003c/a\u003e"
			],
			"formula": [
				"Euler transform of length 8 sequence [0, -1, 0, -1, 0, 0, 0, 1]. - _Michael Somos_, Jul 17 2009",
				"a(n) is multiplicative with a(2^e) = 0^e, a(p^e) = 1 if p == 1, 7 (mod 8), a(p^e) = (-1)^e if p == 3, 5 (mod 8). - _Michael Somos_, Jul 17 2009",
				"G.f.: x*(1 - x^2)/(1 + x^4). a(n) = -a(n + 4) = a(-n) for all n in Z. a(2*n) = 0. a(2*n + 1) = A087960(n). - _Michael Somos_, Apr 10 2011",
				"Transform of Pell numbers A000129 by the Riordan array A102587. - _Paul Barry_, Jul 14 2005",
				"a(n) = -(1/8)*(n mod 8 - (n + 1) mod 8 + (n + 2) mod 8 + (n + 3) mod 8 - (n + 4) mod 8 + (n + 5) mod 8 - (n + 6) mod 8 - (n + 7) mod 8) with n \u003e= 0. - _Paolo P. Lava_, Oct 09 2006",
				"a(n) = (2/n) = (n/2), _Charles R Greathouse IV_ explained. - _Alonso del Arte_, Oct 31 2014",
				"a(n) = (1 - (-1)^n)*(-1)^(n/4 - 1/8 - (-1)^n/8 + (-1)^((2*n + 1 - (-1)^n)/4)/4)/2. - _Wesley Ivan Hurt_, Sep 07 2015",
				"From _Jianing Song_, Nov 14 2018: (Start)",
				"a(n) = sqrt(2)*sin(Pi*n/2)*sin(Pi*n/4).",
				"E.g.f.: sqrt(2)*cos(x/sqrt(2))*sinh(x/sqrt(2)).",
				"Moebius transform of A035185.",
				"a(n) = A101455(n)*A188510(n). (End)",
				"a(n) = Sum_{i=1..n} (-1)^(i + floor((i-3)/4)). - _Wesley Ivan Hurt_, Apr 27 2020"
			],
			"example": [
				"G.f. = x - x^3 - x^5 + x^7 + x^9 - x^11 - x^13 + x^15 + x^17 - x^19 - x^21 + ..."
			],
			"maple": [
				"A091337:= n -\u003e [0, 1, 0, -1, 0, -1, 0, 1][(n mod 8)+1]: seq(A091337(n), n=1..100); # _Wesley Ivan Hurt_, Sep 07 2015"
			],
			"mathematica": [
				"KroneckerSymbol[Range[100], 2] (* _Alonso del Arte_, Oct 30 2014 *)"
			],
			"program": [
				"(PARI) {a(n) = (n%2) * (-1)^((n+1)\\4)}; /* _Michael Somos_, Sep 10 2005 */",
				"(PARI) {a(n) = kronecker( 2, n)}; /* _Michael Somos_, Sep 10 2005 */",
				"(PARI) {a(n) = [0, 1, 0, -1, 0, -1, 0, 1][n%8 + 1]}; /* _Michael Somos_, Jul 17 2009 */",
				"(MAGMA) [(n mod 2) * (-1)^((n+1) div 4)  : n in [1..100]]; // _Vincenzo Librandi_, Oct 31 2014"
			],
			"xref": [
				"Cf. A000129, A035185, A073097, A087960, A101455, A102587, A188510."
			],
			"keyword": "sign,mult,easy",
			"offset": "1,1",
			"author": "_Eric W. Weisstein_, Dec 30 2003",
			"references": 29,
			"revision": 65,
			"time": "2021-03-12T22:24:42-05:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}