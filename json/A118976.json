{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A118976",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 118976,
			"data": "1,2,1,4,4,1,7,12,7,1,11,30,30,11,1,16,65,100,65,16,1,22,126,280,280,126,22,1,29,224,686,980,686,224,29,1,37,372,1512,2940,2940,1512,372,37,1,46,585,3060,7812,10584,7812,3060,585,46,1,56,880,5775,18810,33264,33264,18810,5775,880,56,1",
			"name": "Triangle read by rows: T(n,k)=binomial(n-1,k-1)*binomial(n,k-1)/k + binomial(n-1,k)*binomial(n,k)/(k+1) (1\u003c=k\u003c=n). In other words, to each entry of the Narayana triangle (A001263) add the entry on its right.",
			"comment": [
				"Sum of entries in row n = 2*Cat(n)-1, where Cat(n) are the Catalan numbers (A000108).",
				"Row sums = A131428 starting (1, 3, 9, 27, 83,...). - _Gary W. Adamson_, Aug 31 2007"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A118976/b118976.txt\"\u003eRows n = 1..100 of triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"G.f.: A001263(x, y)*(x + x*y) + x*y. - _Vladimir Kruchinin_, Oct 21 2020"
			],
			"example": [
				"First few rows of the triangle are:",
				"   1;",
				"   2,  1;",
				"   4,  4,   1;",
				"   7, 12,   7,  1;",
				"  11, 30,  30, 11,  1;",
				"  16, 65, 100, 65, 16, 1;",
				"...",
				"Row 4 of the triangle = (7, 12, 7, 1), derived from row 4 of the Narayana triangle, (1, 6, 6, 1): = ((1+6), (6+6), (6+1), (1))."
			],
			"maple": [
				"T:=(n,k)-\u003ebinomial(n-1,k-1)*binomial(n,k-1)/k+binomial(n-1,k) *binomial(n,k)/ (k+1): for n from 1 to 12 do seq(T(n,k),k=1..n) od; # yields sequence in triangular form",
				"# Alternatively:",
				"gf := 1 - ((1/2)*(x + 1)*(sqrt((x*y + y - 1)^2 - 4*y^2*x) + x*y + y - 1))/(y*x):",
				"sery := series(gf, y, 10): coeffy := n -\u003e expand(coeff(sery, y, n)):",
				"seq(print(seq(coeff(coeffy(n), x, k), k=1..n)), n=1..8); # _Peter Luschny_, Oct 21 2020"
			],
			"mathematica": [
				"With[{B=Binomial}, Table[B[n-1,k-1]*B[n,k-1]/k + B[n-1,k]*B[n,k]/(k+1), {n,12}, {k,n}]//Flatten] (* _G. C. Greubel_, Aug 12 2019 *)"
			],
			"program": [
				"(PARI) T(n,k) = b=binomial; b(n-1,k-1)*b(n,k-1)/k + b(n-1,k)*b(n,k)/(k+1);",
				"for(n=1,12, for(k=1,n, print1(T(n,k), \", \"))) \\\\ _G. C. Greubel_, Aug 12 2019",
				"(MAGMA) B:=Binomial; [B(n-1,k-1)*B(n,k-1)/k + B(n-1,k)*B(n,k)/(k+1): k in [1..n], n in [1..12]]; // _G. C. Greubel_, Aug 12 2019",
				"(Sage)",
				"def T(n, k):",
				"    b=binomial",
				"    return b(n-1,k-1)*b(n,k-1)/k + b(n-1,k)*b(n,k)/(k+1)",
				"[[T(n, k) for k in (1..n)] for n in (1..12)] # _G. C. Greubel_, Aug 12 2019",
				"(GAP) B:=Binomial; Flat(List([1..12], n-\u003e List([1..n], k-\u003e B(n-1,k-1)*B(n,k-1)/k + B(n-1,k)*B(n,k)/(k+1) ))); # _G. C. Greubel_, Aug 12 2019"
			],
			"xref": [
				"Cf. A001263, A034856, A000108, A131428."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Gary W. Adamson_, May 07 2006",
			"ext": [
				"Edited by _N. J. A. Sloane_, Nov 29 2006"
			],
			"references": 2,
			"revision": 19,
			"time": "2020-10-22T01:40:03-04:00",
			"created": "2006-05-19T03:00:00-04:00"
		}
	]
}