{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001712",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1712,
			"id": "M4861 N2077",
			"data": "1,12,119,1175,12154,133938,1580508,19978308,270074016,3894932448,59760168192,972751628160,16752851775360,304473528961920,5825460745532160,117070467915075840,2465958106403712000,54336917746726272000,1250216389189281024000",
			"name": "Generalized Stirling numbers.",
			"comment": [
				"The asymptotic expansion of the higher order exponential integral E(x,m=3,n=3) ~ exp(-x)/x^3*(1 - 12/x + 119/x^2 - 1175/x^3 + 12154/x^4 - 133938/x^5 + ...) leads to the sequence given above. See A163931 and A163932 for more information. - _Johannes W. Meijer_, Oct 20 2009",
				"From _Petros Hadjicostas_, Jun 11 2020: (Start)",
				"For nonnegative integers n, m and complex numbers a, b (with b \u003c\u003e 0), the numbers R_n^m(a,b) were introduced by Mitrinovic (1961) using slightly different notation. They were further examined by Mitrinovic and Mitrinovic (1962).",
				"These numbers are defined via the g.f. Product_{r=0..n-1} (x - (a + b*r)) = Sum_{m=0..n} R_n^m(a,b)*x^m for n \u003e= 0.",
				"As a result, R_n^m(a,b) = R_{n-1}^{m-1}(a,b) - (a + b*(n-1))*R_{n-1}^m(a,b) for n \u003e= m \u003e= 1 with R_1^0(a,b) = a, R_1^1(a,b) = 1, and R_n^m(a,b) = 0 for n \u003c m. (Because an empty product is by definition 1, we may let R_0^0(a,b) = 1.)",
				"With a = 0 and b = 1, we get the Stirling numbers of the first kind S1(n,m) = R_n^m(a=0, b=1) = A048994(n,m). (Array A008275 is the same as array A048994 but with no zero row and no zero column.)",
				"We have R_n^m(a,b) = Sum_{k=0}^{n-m} (-1)^k * a^k * b^(n-m-k) * binomial(m+k, k) * S1(n, m+k) for n \u003e= m \u003e= 0.",
				"For the current sequence, a(n) = R_{n+2}^2(a=-3, b=-1) for n \u003e= 0. (End)"
			],
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A001712/b001712.txt\"\u003eTable of n, a(n) for n = 0..100\u003c/a\u003e",
				"Matt Davis, \u003ca href=\"http://arxiv.org/abs/1412.0345\"\u003eQuadrant Marked Mesh Patterns and the r-Stirling Numbers\u003c/a\u003e, arXiv preprint arXiv:1412.0345 [math.CO], 2014.",
				"Matt Davis, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL18/Davis/davis3.html\"\u003eQuadrant Marked Mesh Patterns and the r-Stirling Numbers\u003c/a\u003e, J. Int. Seq. 18 (2015), #15.10.1.",
				"Sergey Kitaev and Jeffrey Remmel, \u003ca href=\"http://arxiv.org/abs/1201.1323\"\u003eSimple marked mesh patterns\u003c/a\u003e, arXiv preprint arXiv:1201.1323 [math.CO], 2012.",
				"Sergey Kitaev and Jeffrey Remmel, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL15/Kitaev/kitaev5.html\"\u003eQuadrant Marked Mesh Patterns\u003c/a\u003e, J. Int. Seq. 15 (2012), #12.4.7.",
				"D. S. Mitrinovic, \u003ca href=\"https://gallica.bnf.fr/ark:/12148/bpt6k762d/f996.image.r=1961%20mitrinovic\"\u003eSur une classe de nombres reliés aux nombres de Stirling\u003c/a\u003e, Comptes rendus de l'Académie des sciences de Paris, t. 252 (1961), 2354-2356. [The numbers R_n^m(a,b) are introduced.]",
				"D. S. Mitrinovic and M. S. Mitrinovic, \u003ca href=\"http://pefmath2.etf.rs/files/47/77.pdf\"\u003eTableaux d'une classe de nombres reliés aux nombres de Stirling\u003c/a\u003e Univ. Beograd. Publ. Elektrotehn. Fak. Ser. Mat. Fiz. No. 77 (1962), 1-77.",
				"D. S. Mitrinovic and R. S. Mitrinovic, \u003ca href=\"https://www.jstor.org/stable/43667130\"\u003eTableaux d'une classe de nombres reliés aux nombres de Stirling\u003c/a\u003e, Univ. Beograd. Publ. Elektrotehn. Fak. Ser. Mat. Fiz., No. 77 (1962), 1-77 [jstor stable version].",
				"Robert E. Moritz, \u003ca href=\"/A001701/a001701.pdf\"\u003eOn the sum of products of n consecutive integers\u003c/a\u003e, Univ. Washington Publications in Math., 1 (No. 3, 1926), 44-49. [Annotated scanned copy]"
			],
			"formula": [
				"a(n) = Sum_{k=0..n} (-1)^(n+k)*binomial(k+2, 2)*3^k*Stirling1(n+2, k+2). - Borislav Crstici (bcrstici(AT)etv.utt.ro), Jan 26 2004",
				"E.g.f.: (1 - 7*log(1 - x) + 6*log(1 - x)^2)/(1 - x)^5. - _Vladeta Jovovic_, Mar 01 2004",
				"If we define f(n,i,a) = Sum_{k=0..n-i} binomial(n,k)*Stirling1(n-k, i)*Product_{j=0..k-1} (-a-j), then a(n-2) = |f(n,2,3)|, for n \u003e= 2. [_Milan Janjic_, Dec 21 2008]",
				"Conjecture: a(n) + 3*(-n-3)*a(n-1) + (3*n^2 + 15*n + 19)*a(n-2) - (n+2)^3*a(n-3)=0. - _R. J. Mathar_, Jun 09 2018",
				"From _Petros Hadjicostas_, Jun 11 2020: (Start)",
				"a(n) = [x^2] Product_{r=0}^{n+1} (x + 3 + r) = (Product_{r=0}^{n+1} (r+3)) * Sum_{0 \u003c= i \u003c j \u003c= n+1} 1/((3+i)*(3+j)).",
				"Since a(n) = R_{n+2}^2(a=-3, b=-1) and A001711(n) = R_{n+1}^1(a=-3,b=-1), the equation R_{n+2}^2(a=-3,b=-1) = R_{n+1}^1(a=-3,b=-1) + (n+4)*R_{n+1}^2(a=-3,b=-1) implies the following:",
				"(i) a(n) = A001711(n) + (n+4)*a(n-1) for n \u003e= 1.",
				"(ii) a(n) = (n+2)!/2 + (2*n+7)*a(n-1) - (n+3)^2*a(n-2) for n \u003e= 2.",
				"(iii) _R. J. Mathar_'s recurrence above. (End)"
			],
			"maple": [
				"A001712 := proc(n)",
				"    add((-1)^(n+k)*binomial(k+2, 2)*3^k*Stirling1(n+2, k+2), k=0..n) ;",
				"end proc:",
				"seq(A001712(n), n=0..10) ; # _R. J. Mathar_, Jun 09 2018"
			],
			"mathematica": [
				"nn = 22; t = Range[0, nn]! CoefficientList[Series[Log[1 - x]^2/(2*(1 - x)^3), {x, 0, nn}], x]; Drop[t, 2] (* _T. D. Noe_, Aug 09 2012 *)"
			],
			"program": [
				"(PARI) a(n) = sum(k=0, n, (-1)^(n+k)*binomial(k+2, 2)*3^k*stirling(n+2, k+2, 1)) \\\\ _Michel Marcus_, Jan 20 2016",
				"(PARI) b(n) = prod(r=0, n+1, r+3);",
				"c(n) = sum(i=0, n+1, sum(j=i+1, n+1, 1/((3+i)*(3+j))));",
				"for(n=0, 18, print1(b(n)*c(n),\",\")) \\\\ _Petros Hadjicostas_, Jun 11 2020"
			],
			"xref": [
				"Cf. A001711, A008275, A048994, A163931, A163932."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from Borislav Crstici (bcrstici(AT)etv.utt.ro), Jan 26 2004"
			],
			"references": 6,
			"revision": 63,
			"time": "2020-06-26T11:13:37-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}