{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A230779",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 230779,
			"data": "5,10,13,17,20,26,29,34,37,40,41,45,52,53,58,61,68,73,74,80,82,89,90,97,101,104,106,109,113,116,117,122,136,137,146,148,149,153,157,160,164,173,178,180,181,193,194,197,202,208,212,218,226,229,232,233,234,241,244,245,257,261,269,272",
			"name": "Numbers which are uniquely decomposable into a sum of two squares, the unique decomposition being with two distinct nonzero squares.",
			"comment": [
				"Numbers with exactly one prime factor of form 4*k+1 with multiplicity one and no prime factor 4*k+3 with odd multiplicity. There is thus no square in the sequence.",
				"These are the primitive elements of A004431, the integers which are the sum of two nonzero distinct squares.",
				"Numbers such that A004018(a(n))= 8.",
				"The square of these numbers is also uniquely decomposable into a sum of two squares, thus this sequence is a subsequence of A084645.",
				"Also a subsequence of A191217: the two sequences are equal up to a(583) = 3121; then A191217(584)= 5^5 = 3125 and a(584) = 3137.",
				"Numbers n such that n^3 is the sum of two nonzero squares in exactly two ways. - _Altug Alkan_, Jul 01 2016"
			],
			"link": [
				"Jean-Christophe Hervé, \u003ca href=\"/A230779/b230779.txt\"\u003eTable of n, a(n) for n = 1..1645\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/SquareNumber.html\"\u003eSquare Number\u003c/a\u003e",
				"Wikipedia (fr), \u003ca href=\"http://fr.wikipedia.org/wiki/Théorème_des_deux_carrés_de_Fermat\"\u003e‎Théorème des deux carrés\u003c/a\u003e",
				"G. Xiao, \u003ca href=\"http://wims.unice.fr/~wims/en_tool~number~twosquares.en.html\"\u003eTwo squares\u003c/a\u003e",
				"\u003ca href=\"/index/Su#ssq\"\u003eIndex entries for sequences related to sums of squares\u003c/a\u003e"
			],
			"formula": [
				"Terms are obtained by the products A125853(k)*A002144(p) for k, p \u003e 0, ordered by increasing values.",
				"A004018(a(n)) = 8."
			],
			"example": [
				"a(1)= 5 = 4+1, a(2)= 10 = 9+1, a(3)= 13 = 9+4. However 2 = 1+1, 4 = 4+0, 8 = 4+4 are excluded because the unique decomposition of these numbers in two squares is not with two distinct nonzero squares; 25, 50, 100 are also excluded because there are two decompositions of these numbers in two squares (including one with equal or zero squares)."
			],
			"program": [
				"(PARI) isok(n) = {f = factor(n); nb1 = 0; for (i=1, #f~, p = f[i, 1]; ep = f[i, 2]; if (p % 4 == 1, nb1 ++; if (ep != 1, return (0))); if (p % 4 == 3, if (ep % 2, return (0)));); return (nb1 == 1);} \\\\ _Michel Marcus_, Nov 17 2013"
			],
			"xref": [
				"Cf. A001481, A004431, A002144, A084645.",
				"Cf. A004018, A022554 (A004018 = 0), A125853 (A004018 = 4)."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Jean-Christophe Hervé_, Nov 16 2013",
			"references": 6,
			"revision": 42,
			"time": "2020-02-20T10:19:35-05:00",
			"created": "2013-11-20T12:55:56-05:00"
		}
	]
}