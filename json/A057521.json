{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A057521",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 57521,
			"data": "1,1,1,4,1,1,1,8,9,1,1,4,1,1,1,16,1,9,1,4,1,1,1,8,25,1,27,4,1,1,1,32,1,1,1,36,1,1,1,8,1,1,1,4,9,1,1,16,49,25,1,4,1,27,1,8,1,1,1,4,1,1,9,64,1,1,1,4,1,1,1,72,1,1,25,4,1,1,1,16,81,1,1,4,1,1,1,8,1,9,1,4,1,1,1,32,1",
			"name": "Powerful (1) part of n: if n = Product_i (pi^ei) then a(n) = Product_{i : ei \u003e 1} (pi^ei); if n=b*c^2*d^3 then a(n)=c^2*d^3 when b is minimized.",
			"link": [
				"Antti Karttunen, \u003ca href=\"/A057521/b057521.txt\"\u003eTable of n, a(n) for n = 1..16383\u003c/a\u003e (first 1000 terms from  T. D. Noe)",
				"Antti Karttunen, \u003ca href=\"/A057521/a057521.txt\"\u003eData supplement: n, a(n) computed for n = 1..65537\u003c/a\u003e",
				"Christian Krause, \u003ca href=\"https://github.com/ckrause/loda\"\u003eLODA, an assembly language, a computational model and a tool for mining integer sequences\u003c/a\u003e",
				"Victor Ufnarovski and Bo Åhlander, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL6/Ufnarovski/ufnarovski.html\"\u003eHow to Differentiate a Number\u003c/a\u003e, J. Integer Seqs., Vol. 6, 2003, #03.3.4.",
				"\u003ca href=\"/index/Pow#powerful\"\u003eIndex entries for sequences related to powerful numbers\u003c/a\u003e"
			],
			"formula": [
				"a(n) = n / A055231(n).",
				"Multiplicative with a(p)=1 and a(p^e)=p^e for e\u003e1. - _Vladeta Jovovic_, Nov 01 2001",
				"From _Antti Karttunen_, Nov 22 2017: (Start)",
				"a(n) = A064549(A003557(n)).",
				"A003557(a(n)) = A003557(n).",
				"(End)",
				"a(n) = gcd(n, A003415(n)^k), for all k \u003e= 2. [This formula was found in the form k=3 by Christian Krause's LODA miner. See Ufnarovski and Åhlander paper, Theorem 5 on p. 4 for why this holds] - _Antti Karttunen_, Mar 09 2021"
			],
			"example": [
				"a(40) = 8 since 40 = 2^3 * 5 so the powerful part is 2^3 = 8."
			],
			"maple": [
				"A057521 := proc(n)",
				"    local a,d,e,p;",
				"    a := 1;",
				"    for d in ifactors(n)[2] do",
				"        e := d[1] ;",
				"        p := d[2] ;",
				"        if e \u003e 1 then",
				"            a := a*p^e ;",
				"        end if;",
				"    end do:",
				"    return a;",
				"end proc: # _R. J. Mathar_, Jun 09 2016"
			],
			"mathematica": [
				"rad[n_] := Times @@ First /@ FactorInteger[n]; a[n_] := n/Denominator[n/rad[n]^2]; Table[a[n], {n, 1, 97}] (* _Jean-François Alcover_, Jun 20 2013 *)",
				"f[p_, e_] := If[e \u003e 1, p^e, 1]; a[1] = 1; a[n_] := Times @@ f @@@ FactorInteger[n]; Array[a, 100] (* _Amiram Eldar_, Sep 21 2020 *)"
			],
			"program": [
				"(PARI) a(n)=my(f=factor(n));prod(i=1,#f~,if(f[i,2]\u003e1,f[i,1]^f[i,2],1)) \\\\ _Charles R Greathouse IV_, Aug 13 2013",
				"(PARI) a(n) = my(f=factor(n)); for (i=1, #f~, if (f[i,2]==1, f[i,1]=1)); factorback(f); \\\\ _Michel Marcus_, Jan 29 2021",
				"(Python)",
				"from sympy import factorint, prod",
				"def a(n): return 1 if n==1 else prod(1 if e==1 else p**e for p, e in factorint(n).items())",
				"print([a(n) for n in range(1, 51)]) # _Indranil Ghosh_, Jul 19 2017"
			],
			"xref": [
				"Cf. A001694, A003415, A003557, A055231, A064549."
			],
			"keyword": "nonn,mult,easy",
			"offset": "1,4",
			"author": "_Henry Bottomley_, Sep 01 2000",
			"references": 49,
			"revision": 55,
			"time": "2021-03-10T12:29:58-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}