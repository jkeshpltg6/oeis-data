{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A088568",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 88568,
			"data": "1,0,-1,0,1,0,1,0,-1,0,-1,-2,-1,0,-1,0,1,0,-1,0,-1,0,1,0,1,0,-1,0,1,0,1,2,1,2,1,0,1,0,-1,0,1,0,1,0,-1,0,-1,0,1,0,1,2,1,0,1,0,-1,0,1,0,1,0,-1,0,-1,-2,-1,0,-1,0,1,0,1,0,-1,0,-1,0,1,0,-1,0,-1,-2,-1,0,-1,0,-1,-2,-1,-2,-3,-2,-1,-2,-1,0,-1,-2,-1,-2,-1,0,-1,0,-1",
			"name": "3*n - 2*(partial sums of Kolakoski sequence A000002).",
			"comment": [
				"It is conjectured that a(n) = o(n).",
				"It is conjectured that the density of 1's and that of 2's in the Kolakoski sequence A000002 are equal to 1/2. The deficit of 2's in the Kolakoski sequence at rank n being defined as n/2 - number of 2's in the Kolakoski word of length n, a(n) is equal to twice the deficit of 2's (or twice the excess of 1's). Equivalently, the number of 2's up to rank n in the Kolakoski sequence is (n - a(n))/2. - _Jean-Christophe Hervé_, Oct 05 2014",
				"The conjecture about the densities of 1's and 2's is equivalent to a(n) = o(n). The graph shows that a(n) seems to oscillate around 0 with a pseudo-periodic and fractal pattern. - _Jean-Christophe Hervé_, Oct 05 2014",
				"It is conjectured that a(n) = O(log(n)) (see PlanetMath link). Note that for a random sequence of 1's and -1's, we would have O(sqrt(n)). - _Daniel Forgues_, Jul 10 2015",
				"The conjecture that a(n) = O(log(n)) seems incorrect as |a(n)| seems to grow as fast as sqrt(n), see A289323 and note that a(2^n) = -A289323(n), so for example a(2^64) = -A289323(64) = -836086974 which is much larger in absolute value than log(2^64), but about 0.19*2^32. - _Richard P. Brent_, Jul 07 2017",
				"For n = 124 to 147, we have the same 24 values as for n = 42 to 65: {0, 1, 0, -1, 0, -1, 0, 1, 0, 1, 2, 1, 0, 1, 0, -1, 0, 1, 0, 1, 0, -1, 0, -1}, and for n = 173 to 200, we have the same 28 values as for n = 11 to 38: {-1, -2, -1, 0, -1, 0, 1, 0, -1, 0, -1, 0, 1, 0, 1, 0, -1, 0, 1, 0, 1, 2, 1, 2, 1, 0, 1, 0}. - _Daniel Forgues_, Jul 11 2015"
			],
			"link": [
				"Jean-Christophe Hervé, \u003ca href=\"/A088568/b088568.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Richard P. Brent, \u003ca href=\"https://maths-people.anu.edu.au/~brent/pd/Kolakoski-UNSW.pdf\"\u003eFast algorithms for the Kolakoski sequence\u003c/a\u003e, Slides from a talk, 2016.",
				"A. Scolnicov, \u003ca href=\"http://planetmath.org/kolakoskisequence\"\u003eKolakoski sequence\u003c/a\u003e, PlanetMath.org."
			],
			"formula": [
				"a(n) = 3*n - 2*A054353(n) by definition. - _Jean-Christophe Hervé_, Oct 05 2014",
				"a(n) = 2*A156077(n) - n. - _Jean-Christophe Hervé_, Oct 05 2014"
			],
			"example": [
				"The sequence A000002 starts 1, 2, 2, 1, 1, 2, ..., so the sixth partial sum is 1 + 2 + 2 + 1 + 1 + 2 = 9, and therefore a(6) = 3*6 - 2*9 = 0. - _Michael B. Porter_, Jul 08 2016"
			],
			"xref": [
				"Cf. A000002 (Kolakoski sequence), A054353 (partial sums of Kolakoski sequence), A156077 (number of 1's in the Kolakoski sequence).",
				"For the discrepancy of the Kolakoski sequence see A294448 (this is simply the negation of the present sequence).",
				"For records see A294449."
			],
			"keyword": "sign,look",
			"offset": "1,12",
			"author": "_Benoit Cloitre_, Nov 17 2003; definition changed Oct 16 2005",
			"references": 23,
			"revision": 67,
			"time": "2020-09-25T11:25:50-04:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}