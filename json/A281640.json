{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A281640",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 281640,
			"data": "1,2,0,0,2,1,2,0,0,4,0,0,0,0,2,0,2,0,0,0,0,2,0,0,0,3,2,0,0,2,2,0,0,0,2,0,2,0,0,0,1,6,0,0,2,0,0,0,0,4,2,0,0,0,2,0,2,0,0,0,0,2,0,0,2,2,0,0,0,2,0,0,0,0,2,0,2,0,0,0,1,4,0,0,2,0,2",
			"name": "Expansion of x * f(x, x) * f(x^5, x^25) in powers of x where f(, ) is Ramanujan's general theta function.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A281640/b281640.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"G.f.: x * (Sum_{k in Z} x^k^2) * (Sum_{k in Z} x^(15*k^2 - 10*k)).",
				"G.f.: x * Product_{k\u003e0} (1 + x^(2*k-1))^2 * (1 - x^(2*k)) * (1 + x^(30*k-25)) * (1 + x^(30*k-5)) * (1 - x^(30*k)).",
				"a(n) = A122855(3*n + 2) = A260649(3*n + 2) = A122856(6*n + 4) = A258276(6*n + 4).",
				"a(n) = - A140727(3*n + 2). 2 * a(n) = A192323(3*n + 2)."
			],
			"example": [
				"G.f. = x + 2*x^2 + 2*x^5 + x^6 + 2*x^7 + 4*x^10 + 2*x^15 + 2*x^17 + 2*x^22 + ...",
				"G.f. = q^5 + 2*q^8 + 2*q^17 + q^20 + 2*q^23 + 4*q^32 + 2*q^47 + 2*q^53 + 2*q^65 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 1, 0, DivisorSum[ 3 n + 2, KroneckerSymbol[ -15, #] (-1)^Boole[Mod[#, 4] == 2] \u0026]];",
				"a[ n_] := SeriesCoefficient[ x EllipticTheta[ 3, 0, x] QPochhammer[ -x^5, x^30] QPochhammer[ -x^25, x^30] QPochhammer[ x^30], {x, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, 0, sumdiv(3*n + 2, d, kronecker(-15, d) * (-1)^(d%4==2) ))};",
				"(PARI) {a(n) = if( n\u003c1, 0, my(m = 3*n + 2, s, x); for(y=1, sqrtint(m\\5), if( y%3 \u0026\u0026 issquare((m - 5*y^2)\\3, \u0026x), s += (x\u003e0) + 1)); s)};",
				"(PARI) {a(n) = if( n\u003c1, 0, my(A); n--; A = x * O(x^n); polcoeff( eta(x^2 + A)^5 * eta(x^10 + A)^2 * eta(x^15 + A) * eta(x^60 + A) / (eta(x + A)^2 * eta(x^4 + A)^2 * eta(x^5 + A) * eta(x^20 + A) * eta(x^30 + A)), n))};"
			],
			"xref": [
				"Cf. A122855, A122856, A140727, A192323, A258276, A260649."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Michael Somos_, Jan 25 2017",
			"references": 1,
			"revision": 11,
			"time": "2021-03-12T22:24:48-05:00",
			"created": "2017-01-25T17:46:33-05:00"
		}
	]
}