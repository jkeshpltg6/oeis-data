{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A340653",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 340653,
			"data": "1,1,1,0,1,0,1,1,0,0,1,2,1,0,0,1,1,2,1,2,0,0,1,1,0,0,1,2,1,3,1,1,0,0,0,2,1,0,0,1,1,3,1,2,2,0,1,2,0,2,0,2,1,1,0,1,0,0,1,2,1,0,2,1,0,3,1,2,0,3,1,3,1,0,2,2,0,3,1,2,1,0,1,2,0,0,0",
			"name": "Number of balanced factorizations of n.",
			"comment": [
				"A factorization into factors \u003e 1 is balanced if it is empty or its length is equal to its maximum Omega (A001222)."
			],
			"example": [
				"The balanced factorizations for n = 120, 144, 192, 288, 432, 768:",
				"  3*5*8    2*8*9    3*8*8      4*8*9      6*8*9      8*8*12",
				"  2*2*30   3*6*8    4*6*8      6*6*8      2*8*27     2*2*8*24",
				"  2*3*20   2*4*18   2*8*12     2*8*18     3*8*18     2*3*8*16",
				"  2*5*12   2*6*12   4*4*12     3*8*12     4*4*27     2*4*4*24",
				"           3*4*12   2*2*2*24   4*4*18     4*6*18     2*4*6*16",
				"                    2*2*3*16   4*6*12     4*9*12     3*4*4*16",
				"                               2*12*12    6*6*12     2*2*12*16",
				"                               2*2*2*36   2*12*18    2*2*2*2*48",
				"                               2*2*3*24   3*12*12    2*2*2*3*32",
				"                               2*3*3*16   2*2*2*54",
				"                                          2*2*3*36",
				"                                          2*3*3*24",
				"                                          3*3*3*16"
			],
			"mathematica": [
				"facs[n_]:=If[n\u003c=1,{{}},Join@@Table[Map[Prepend[#,d]\u0026,Select[facs[n/d],Min@@#\u003e=d\u0026]],{d,Rest[Divisors[n]]}]];",
				"Table[Length[Select[facs[n],#=={}||Length[#]==Max[PrimeOmega/@#]\u0026]],{n,100}]"
			],
			"xref": [
				"Positions of zeros are A001358.",
				"Positions of nonzero terms are A100959.",
				"The co-balanced version is A340596.",
				"Taking maximum factor instead of maximum Omega gives A340599.",
				"The cross-balanced version is A340654.",
				"The twice-balanced version is A340655.",
				"A001055 counts factorizations.",
				"A045778 counts strict factorizations.",
				"A316439 counts factorizations by product and length.",
				"A320655 counts factorizations into semiprimes.",
				"Other balance-related sequences:",
				"- A010054 counts balanced strict partitions.",
				"- A047993 counts balanced partitions.",
				"- A098124 counts balanced compositions.",
				"- A106529 lists Heinz numbers of balanced partitions.",
				"- A340597 have an alt-balanced factorization.",
				"- A340598 counts balanced set partitions.",
				"- A340600 counts unlabeled balanced multiset partitions.",
				"- A340656 have no twice-balanced factorizations.",
				"- A340657 have a twice-balanced factorization.",
				"Cf. A003963, A117409, A303975, A320656, A339846, A339890, A340608."
			],
			"keyword": "nonn",
			"offset": "1,12",
			"author": "_Gus Wiseman_, Jan 15 2021",
			"references": 35,
			"revision": 8,
			"time": "2021-01-19T21:52:12-05:00",
			"created": "2021-01-19T21:52:12-05:00"
		}
	]
}