{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007918",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7918,
			"data": "2,2,2,3,5,5,7,7,11,11,11,11,13,13,17,17,17,17,19,19,23,23,23,23,29,29,29,29,29,29,31,31,37,37,37,37,37,37,41,41,41,41,43,43,47,47,47,47,53,53,53,53,53,53,59,59,59,59,59,59,61,61,67,67,67,67,67,67,71,71,71,71,73,73",
			"name": "Least prime \u003e= n (version 1 of the \"next prime\" function).",
			"comment": [
				"Version 2 of the \"next prime\" function is \"smallest prime \u003e n\". This produces A151800.",
				"Maple uses version 2.",
				"According to the \"k-tuple\" conjecture, a(n) is the initial term of the lexicographically earliest increasing arithmetic progression of n primes; the corresponding common differences are given by A061558. - _David W. Wilson_, Sep 22 2007",
				"It is easy to show that the initial term of an increasing arithmetic progression of n primes cannot be smaller than a(n). - _N. J. A. Sloane_, Oct 18 2007",
				"Also, smallest prime bounded by n and 2n inclusively (in accordance with Bertrand's theorem). Smallest prime \u003en is a(n+1) and is equivalent to smallest prime between n and 2n exclusively. - _Lekraj Beedassy_, Jan 01 2007"
			],
			"reference": [
				"K. Atanassov, On the 37th and 38th Smarandache Problems, Notes on Number Theory and Discrete Mathematics, Sophia, Bulgaria, Vol. 5 (1999), No. 2, 83-85."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A007918/b007918.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Jens Kruse Andersen, \u003ca href=\"http://primerecords.dk/aprecords.htm\"\u003eRecords for primes in arithmetic progressions\u003c/a\u003e",
				"K. Atanassov, \u003ca href=\"http://www.gallup.unm.edu/~smarandache/Atanassov-SomeProblems.pdf\"\u003eOn Some of Smarandache's Problems\u003c/a\u003e",
				"H. Bottomley, \u003ca href=\"http://www.se16.info/js/prime.htm\"\u003ePrime number calculator\u003c/a\u003e",
				"J. Castillo, \u003ca href=\"http://www.gallup.unm.edu/~smarandache/funct2.txt\"\u003eOther Smarandache Type Functions: Inferior/Superior Smarandache f-part of x\u003c/a\u003e, Smarandache Notions Journal, Vol. 10, No. 1-2-3, 1999, 202-204.",
				"Andrew Granville, \u003ca href=\"http://www.dms.umontreal.ca/~andrew/PDF/PrimePattMonthly.pdf\"\u003ePrime Number Patterns\u003c/a\u003e",
				"Hans Gunter, \u003ca href=\"http://primepuzzles.net/puzzles/puzz_145.htm\"\u003ePuzzle 145. The Inferior Smarandache Prime Part and Superior Smarandache Prime Part functions\u003c/a\u003e; Solutions by Jean Marie Charrier, Teresinha DaCosta, Rene Blanch, Richard Kelley and Jim Howell.",
				"J. Sondow and Eric Weisstein, \u003ca href=\"http://mathworld.wolfram.com/BertrandsPostulate.html\"\u003eBertrand's Postulate\u003c/a\u003e, World of Mathematics",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/NextPrime.html\"\u003eNext Prime\u003c/a\u003e, \u003ca href=\"http://mathworld.wolfram.com/k-TupleConjecture.html\"\u003ek-tuple conjecture\u003c/a\u003e",
				"\u003ca href=\"/index/Pri#primes_AP\"\u003eIndex entries for sequences related to primes in arithmetic progressions\u003c/a\u003e"
			],
			"formula": [
				"For n \u003e 1: a(n) = A000040(A049084(A007917(n)) + 1 - A010051(n)). - _Reinhard Zumkeller_, Jul 26 2012",
				"a(n) = A151800(n-1). - _Seiichi Manyama_, Apr 02 2018"
			],
			"maple": [
				"A007918 := n-\u003e nextprime(n-1); # _M. F. Hasler_, Apr 09 2008"
			],
			"mathematica": [
				"NextPrime[Range[-1, 72]] (* _Jean-François Alcover_, Apr 18 2011 *)"
			],
			"program": [
				"(PARI 2.4.3 and later) A007918=nextprime  \\\\ _M. F. Hasler_, Jun 24 2011",
				"(PARI 2.4.2 and earlier) A007918(n)=nextprime(n)  \\\\ _M. F. Hasler_, Jun 24 2011",
				"(PARI) for(x=0,100,print1(nextprime(x)\",\")) \\\\ _Cino Hilliard_, Jan 15 2007",
				"(Haskell)",
				"a007918 n = a007918_list !! n",
				"a007918_list = 2 : 2 : 2 : concat (zipWith",
				"              (\\p q -\u003e (replicate (fromInteger(q - p)) q))",
				"                                   a000040_list $ tail a000040_list)",
				"-- _Reinhard Zumkeller_, Jul 26 2012",
				"(MAGMA) [2] cat [NextPrime(n-1): n in [1..80]]; // _Vincenzo Librandi_, Jan 14 2016"
			],
			"xref": [
				"Cf. A000040, A007917, A008407, A020497, A061558, A151799, A151800, A171400."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,1",
			"author": "R. Muller and Charles T. Le (charlestle(AT)yahoo.com)",
			"references": 103,
			"revision": 64,
			"time": "2018-04-03T17:12:17-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}