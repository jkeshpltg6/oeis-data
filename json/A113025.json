{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A113025",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 113025,
			"data": "1,1,2,1,6,12,1,12,60,120,1,20,180,840,1680,1,30,420,3360,15120,30240,1,42,840,10080,75600,332640,665280,1,56,1512,25200,277200,1995840,8648640,17297280,1,72,2520,55440,831600,8648640,60540480,259459200",
			"name": "Triangle of integer coefficients of polynomials P(n,x) of degree n, and falling powers of x, arising in diagonal Padé approximation of exp(x).",
			"comment": [
				"exp(x) is well approximated by P(n,x)/P(n,-x). (P(n,1)/P(n,-1))_{n\u003e=0} is a sequence of convergents to e: i.e., P(n,1) = A001517(n) and P(n,-1) = abs(A002119(n)).",
				"From _Roger L. Bagula_, Feb 15 2009: (Start)",
				"The row polynomials in rising powers of x are y_n(2*x) = Sum_{k=0..n} binomial(n+k, 2*k)*((2*k)!/k!)*x^k, for n \u003e= 0,  with the Bessel polynomials y_n(x) of Krall and Frink, eq. (3), (see also Grosswald,p. 18, eq. (7) and Riordan, p. 77. For the coefficients see A001498. [Edited by _Wolfdieter Lang_, May 11 2018]",
				"P(n, x) = Sum_{k=0..n} (n+k)!/(k!*(n-k)!)*x^(n-k).",
				"Row sums are A001517. (End)"
			],
			"reference": [
				"J. Riordan, Combinatorial Identities, Wiley, 1968, p.77, 10. [From _Roger L. Bagula_, Feb 15 2009]"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A113025/b113025.txt\"\u003eTable of n, a(n) for the first 50 rows, flattened\u003c/a\u003e",
				"E. Grosswald, \u003ca href=\"http://dx.doi.org/10.1007/BFb0063138\"\u003eBessel Polynomials: Recurrence Relations\u003c/a\u003e, Lecture Notes Math. vol. 698, 1978, p. 18.",
				"H. L. Krall and Orrin Fink, \u003ca href=\"https://doi.org/10.1090/S0002-9947-1949-0028473-1\"\u003eA New Class of Orthogonal Polynomials: The Bessel Polynomials\u003c/a\u003e, Trans. Amer. Math. Soc. 65, 100-115, 1949.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PadeApproximant.html\"\u003ePadé approximants\u003c/a\u003e.",
				"F. Wielonsky, \u003ca href=\"https://doi.org/10.1006/jath.1996.3081\"\u003eAsymptotics of diagonal Hermite-Pade approximants to exp(x)\u003c/a\u003e, J. Approx. Theory 90 (1997) 283-298."
			],
			"formula": [
				"From _Wolfdieter Lang_, May 11 2018: (Start)",
				"T(n, k) = binomial(n+k, 2*k)*(2*k)!/k! = (n+k)!/((n-k)!*k!), n \u003e= 0, k = 0..n. (see the R. L. Baluga comment above).",
				"Recurrence (adapted from A001498, see the Grosswald reference): For n \u003e= 0, k = 0..n: a(n, k) = 0 for n \u003c k (zeros not shown in the triangle), a(n, -1) = 0, a(0, 0) = 1 = a(1, 0) and otherwise a(n, k) = 2*(2*n-1)*a(n-1, k-1) + a(n-2, k).",
				"(End)",
				"T(n, k) = Pochhammer(n+1, k)*binomial(n, k). # _Peter Luschny_, May 11 2018"
			],
			"example": [
				"P(3,x) = x^3 + 12*x^2 + 60*x + 120.",
				"y_3(2*x) = 1 + 12*x + 60*x^2 + 120*x^3. (Bessel with x -\u003e 2*x).",
				"From _Roger L. Bagula_, Feb 15 2009: (Start)",
				"{1},",
				"{1, 2},",
				"{1, 6, 12},",
				"{1, 12, 60, 120},",
				"{1, 20, 180, 840, 1680},",
				"{1, 30, 420, 3360, 15120, 30240},",
				"{1, 42, 840, 10080, 75600, 332640, 665280},",
				"{1, 56, 1512, 25200, 277200, 1995840, 8648640, 17297280},",
				"{1, 72, 2520, 55440, 831600, 8648640, 60540480, 259459200, 518918400},",
				"{1, 90, 3960, 110880, 2162160, 30270240, 302702400, 2075673600, 8821612800, 17643225600},",
				"{1, 110, 5940, 205920, 5045040, 90810720, 1210809600, 11762150400, 79394515200, 335221286400, 670442572800} (End)"
			],
			"maple": [
				"T := (n, k) -\u003e pochhammer(n+1, k)*binomial(n, k):",
				"seq(seq(T(n, k), k=0..n), n=0..9); # _Peter Luschny_, May 11 2018"
			],
			"mathematica": [
				"L[n_, m_] = (n + m)!/((n - m)!*m!);",
				"Table[Table[L[n, m], {m, 0, n}], {n, 0, 10}];",
				"Flatten[%] (* _Roger L. Bagula_, Feb 15 2009 *)",
				"P[x_, n_] := Sum[ (2*n - k)!/(k!*(n - k)!)*x^(k), {k, 0, n}]; Table[Reverse[CoefficientList[P[x, n], x]], {n,0,10}] // Flatten (* _G. C. Greubel_, Aug 15 2017 *)"
			],
			"program": [
				"(PARI) T(n,k)=(n+k)!/k!/(n-k)!"
			],
			"xref": [
				"Cf. A001498, A001517, A303986 (signed version)."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "0,3",
			"author": "_Benoit Cloitre_, Jan 03 2006",
			"references": 6,
			"revision": 30,
			"time": "2018-05-11T14:21:12-04:00",
			"created": "2006-01-24T03:00:00-05:00"
		}
	]
}