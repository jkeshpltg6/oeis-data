{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A335680",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 335680,
			"data": "2,3,3,4,5,4,5,8,8,5,6,12,13,12,6,7,17,21,21,17,7,8,23,30,35,30,23,8,9,30,42,51,51,42,30,9,10,38,55,73,75,73,55,38,10,11,47,71,96,109,109,96,71,47,11,12,57,88,125,143,159,143,125,88,57,12,13,68,108,156,187,209,209,187,156,108,68,13",
			"name": "Array read by antidiagonals: T(m,n) (m\u003e=1, n\u003e=1) = number of vertices in figure formed by taking m equally spaced points on a line and n equally spaced points on a parallel line, and joining each of the m points to each of the n points by a line segment.",
			"comment": [
				"The case m=n (the main diagonal) is dealt with in A331755. A306302 has illustrations for the diagonal case for m = 1 to 15.",
				"Also A335678 has colored illustrations for many values of m and n."
			],
			"link": [
				"M. A. Alekseyev, M. Basova, and N. Yu. Zolotykh. \u003ca href=\"https://doi.org/10.1137/140978090\"\u003eOn the minimal teaching sets of two-dimensional threshold functions\u003c/a\u003e. SIAM Journal on Discrete Mathematics 29:1 (2015), 157-165. doi:10.1137/140978090.",
				"M. Griffiths, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL13/Griffiths2/griffiths.html\"\u003eCounting the regions in a regular drawing of K_{n,n}\u003c/a\u003e, J. Int. Seq. 13 (2010) # 10.8.5.",
				"S. Legendre, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL12/Legendre/legendre2.html\"\u003eThe Number of Crossings in a Regular Drawing of the Complete Bipartite Graph\u003c/a\u003e, J. Integer Seqs., Vol. 12, 2009.",
				"\u003ca href=\"/index/St#Stained\"\u003eIndex entries for sequences related to stained glass windows\u003c/a\u003e"
			],
			"formula": [
				"Comment from _Max Alekseyev_, Jun 28 2020 (Start):",
				"T(m,n) = A114999(m-1,n-1) - A331762(m-1,n-1) + m + n for all m, n \u003e= 1. This follows from the Alekseyev-Basova-Zolotykh (2015) article.",
				"Proof: Here is the appropriate modification of the corresponding comment in A306302: Assuming that K(m,n) has vertices at (i,0) and (j,1), for i=0..m-1 and j=0..n-1, the projective map (x,y) -\u003e ((1-y)/(x+1), y/(x+1)) maps K(m,n) to the partition of the right triangle described by Alekseyev et al. (2015), for which Theorem 13 gives the number of regions, line segments, and intersection points. (End)",
				"_Max Alekseyev_'s formula is an analog of Proposition 9 of Legendre (2009), and gives an explicit formula for this array. - _N. J. A. Sloane_, Jun 30 2020)"
			],
			"example": [
				"The initial rows of the array are:",
				"2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, ...",
				"3, 5, 8, 12, 17, 23, 30, 38, 47, 57, 68, 80, ...",
				"4, 8, 13, 21, 30, 42, 55, 71, 88, 108, 129, 153, ...",
				"5, 12, 21, 35, 51, 73, 96, 125, 156, 192, 230, 274, ...",
				"6, 17, 30, 51, 75, 109, 143, 187, 234, 289, 346, 413, ...",
				"7, 23, 42, 73, 109, 159, 209, 274, 344, 426, 510, 609, ...",
				"8, 30, 55, 96, 143, 209, 275, 362, 455, 564, 674, 805, ...",
				"9, 38, 71, 125, 187, 274, 362, 477, 600, 744, 889, 1062, ...",
				"10, 47, 88, 156, 234, 344, 455, 600, 755, 937, 1119, 1337, ...",
				"11, 57, 108, 192, 289, 426, 564, 744, 937, 1163, 1389, 1660, ...",
				"12, 68, 129, 230, 346, 510, 674, 889, 1119, 1389, 1659, 1984, ...",
				"...",
				"The initial antidiagonals are:",
				"2",
				"3, 3",
				"4, 5, 4",
				"5, 8, 8, 5",
				"6, 12, 13, 12, 6",
				"7, 17, 21, 21, 17, 7",
				"8, 23, 30, 35, 30, 23, 8",
				"9, 30, 42, 51, 51, 42, 30, 9",
				"10, 38, 55, 73, 75, 73, 55, 38, 10",
				"11, 47, 71, 96, 109, 109, 96, 71, 47, 11",
				"12, 57, 88, 125, 143, 159, 143, 125, 88, 57, 12",
				"..."
			],
			"xref": [
				"This is one of a set of five arrays: A335678, A335679, A335680, A335681, A335682.",
				"For the diagonal case see A306302 and A331755.",
				"Cf. A114999, A331762."
			],
			"keyword": "nonn,tabl",
			"offset": "1,1",
			"author": "_Lars Blomberg_, _Scott R. Shannon_, and _N. J. A. Sloane_, Jun 28 2020",
			"references": 5,
			"revision": 22,
			"time": "2020-06-30T20:54:07-04:00",
			"created": "2020-06-28T20:35:24-04:00"
		}
	]
}