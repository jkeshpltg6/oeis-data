{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A282133",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 282133,
			"data": "0,0,0,0,0,0,0,2,0,0,2,2,4,10,12,14,28,38,56,84,124,184,264,374,544,836,1190,1746,2544,3712,5410,7890,11470,16666,24436,35574,51892,75552,110124,160624,234162,341178,497058,725026,1056630,1540158,2244566,3271600",
			"name": "Number of maximal cubefree binary words of length n.",
			"comment": [
				"A word is cubefree if it has no block within it of the form xxx, where x is any nonempty block.  A cubefree word w is maximal if it cannot be extended to the right (i.e., both w0 and w1 end in cubes).",
				"It appears that a(n) ~ A028445(n-11). - _M. F. Hasler_, May 05 2017"
			],
			"link": [
				"Lars Blomberg, \u003ca href=\"/A282133/b282133.txt\"\u003eTable of n, a(n) for n = 1..59\u003c/a\u003e"
			],
			"example": [
				"For n = 8, the two maximal cubefree words of length 8 are 00100100 and its complement 11011011.",
				"The first few maximal cubefree words beginning with 1 are:",
				"[1, 1, 0, 1, 1, 0, 1, 1],",
				"[1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0],",
				"[1, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1],",
				"[1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0],",
				"[1, 1, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1],",
				"[1, 0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0],",
				"[1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0],",
				"[1, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1],",
				"[1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1],",
				"[1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0]].",
				"For those beginning with 0, take the complements. - _N. J. A. Sloane_, May 05 2017"
			],
			"maple": [
				"# Maple code adapted from that in A286262 by _N. J. A. Sloane_, May 05 2017",
				"isCubeFree:=proc(v) local n,L;",
				"for n from 3 to nops(v) do for L to n/3 do",
				"if v[n-L*2+1 .. n] = v[n-L*3+1 .. n-L] then RETURN(false) fi od od; true end;",
				"A282133:=proc(n) local s,m;",
				"s:=0;",
				"for m from 2^(n-1) to 2^n-1 do",
				"if isCubeFree(convert(m,base,2)) then",
				"   if (not isCubeFree(convert(2*m,base,2))) and",
				"   (not isCubeFree(convert(2*m+1,base,2))) then",
				"   s:=s+2; fi;",
				"fi;",
				"od; s; end;",
				"[seq(A282133(n),n=0..18)];"
			],
			"xref": [
				"Cf. A028445, A282317.",
				"For these numbers halved, see A286270."
			],
			"keyword": "nonn",
			"offset": "1,8",
			"author": "_Jeffrey Shallit_, Feb 06 2017",
			"ext": [
				"a(36)-a(48) from _Lars Blomberg_, Feb 09 2019"
			],
			"references": 5,
			"revision": 24,
			"time": "2019-02-09T11:10:25-05:00",
			"created": "2017-02-06T20:18:22-05:00"
		}
	]
}