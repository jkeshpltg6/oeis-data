{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A178153",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 178153,
			"data": "1,0,1,3,0,0,3,3,0,3,0,0,3,5,0,9,0,3,7,0,5,9,0,0,0,5,9,0,0,5,15,0,9,0,7,0,3,11,0,15,0,13,0,0,9,9,7,15,0,0,15,0,21,0,13,0,11,0,0,9,0,9,19,0,0,9,0,15,0,0,19,9,0,9,17,0,0,0,0,27,0,21,0,15,15,0,0,0,7,21,25,7,27,9,21,0",
			"name": "Difference between the numbers of quadratic residues (mod p) less than p/2 and greater than p/2, where p=prime(n).",
			"comment": [
				"When prime(n)=1 (mod 4), then a(n)=0. When prime(n)=3 (mod 4), then a(n)\u003e0. When prime(n)=3 (mod 8) and prime(n)\u003e3, then 3 divides a(n). See Borevich and Shafarevich. The nonzero terms of this sequence are closely related to A002143, the class number of primes p=3 (mod 4).",
				"Same as difference between the numbers of quadratic residues and nonresidues (mod p) less than p/2, where p=prime(n). - Jonathan Sondow, Oct 30 2011"
			],
			"reference": [
				"Z. I. Borevich and I. R. Shafarevich, Number Theory, Academic Press, NY, 1966, p. 346.",
				"H. Davenport, Multiplicative Number Theory, Graduate Texts in Math. 74, 2nd ed., Springer, 1980, p. 51."
			],
			"link": [
				"R. J. Mathar, \u003ca href=\"/A178153/b178153.txt\"\u003eTable of n, a(n) for n = 2..2066\u003c/a\u003e",
				"MathOverflow, \u003ca href=\"http://mathoverflow.net/questions/25263\"\u003eMost squares in the first half-interval\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A178151(n) - A178152(n).",
				"a(n) = sum(j=1..(p-1)/2, (j|p)), where p = prime(n) and (j|p) = +/-1 is the Legendre symbol. - _Jonathan Sondow_, Oct 30 2011"
			],
			"example": [
				"The quadratic residues of 19, the 8th prime, are 1, 4, 5, 6, 7, 9, 11, 16, 17. Hence a(8)=6-3=3."
			],
			"maple": [
				"A178153 := proc(n)",
				"    local r,a,p;",
				"    p := ithprime(n) ;",
				"    a := 0 ;",
				"    for r from 1 to p/2 do",
				"        if numtheory[legendre](r,p) =1 then",
				"            a := a+1 ;",
				"        end if;",
				"    end do:",
				"    for r from ceil(p/2) to p-1 do",
				"        if numtheory[legendre](r,p) =1 then",
				"            a := a-1 ;",
				"        end if;",
				"    end do:",
				"    a;",
				"end proc: # _R. J. Mathar_, Feb 10 2017"
			],
			"mathematica": [
				"Table[p=Prime[n]; Length[Select[Range[(p-1)/2], JacobiSymbol[ #,p]==1\u0026]] - Length[Select[Range[(p+1)/2,p-1], JacobiSymbol[ #,p]==1\u0026]], {n,2,100}]",
				"Table[p = Prime[n]; Sum[ JacobiSymbol[a, p], {a, 1, (p-1)/2}], {n, 2, 100}] (* _Jonathan Sondow_, Oct 30 2011 *)"
			],
			"xref": [
				"Cf. A178154 (without the zero terms)."
			],
			"keyword": "nonn",
			"offset": "2,4",
			"author": "_T. D. Noe_, May 21 2010",
			"references": 4,
			"revision": 12,
			"time": "2017-02-10T09:30:37-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}