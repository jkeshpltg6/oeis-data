{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A100044",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 100044,
			"data": "1,0,9,6,6,2,2,7,1,1,2,3,2,1,5,0,9,5,7,6,4,8,2,7,6,7,7,7,7,6,4,0,1,6,7,9,2,8,1,2,6,3,3,2,6,7,4,7,1,1,9,8,9,5,8,4,9,0,3,7,2,1,5,2,9,1,3,3,3,8,3,1,3,6,0,2,1,3,3,9,1,5,8,8,9,0,8,5,9,3,3,7,4,6,5,0,5,8,0,3,5,3",
			"name": "Decimal expansion of Pi^2/9.",
			"comment": [
				"The Dirichlet L-series for the principal character mod 6 (which is A120325 shifted left) evaluated at 2. - _R. J. Mathar_, Jul 20 2012"
			],
			"reference": [
				"L. B. W. Jolley, Summation of Series, Dover (1961)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A100044/b100044.txt\"\u003eTable of n, a(n) for n = 1..5000\u003c/a\u003e",
				"Jean-Paul Allouche and Jeffrey Shallit, \u003ca href=\"https://doi.org/10.1007/BFb0097122\"\u003eSums of digits and the Hurwitz zeta function\u003c/a\u003e, in: K. Nagasaka and E. Fouvry (eds.), Analytic Number Theory, Lecture Notes in Mathematics, Vol. 1434, Springer, Berlin, Heidelberg, 1990, pp. 19-30.",
				"R. J. Mathar, \u003ca href=\"https://arxiv.org/abs/1008.2547\"\u003eTable of Dirichlet L-series and prime zeta modulo functions for small moduli\u003c/a\u003e, arXiv:1008.2547 [math.NT], 2010-2015, Table 22.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"https://mathworld.wolfram.com/DigitSum.html\"\u003eDigit Sum\u003c/a\u003e"
			],
			"formula": [
				"Equals 1 + (1/2)*(1/3)*(1/2) + (1/3)*(1*2)/(3*5)*(1/2)^2 + (1/4) *(1*2*3)/(3*5*7)*(1/2)^3 + .... [Jolley eq 277]",
				"Equals 1/1^2 + 1/5^2 + 1/7^2 + 1/11^2 + 1/13^2 + 1/17^2 + .... - _R. J. Mathar_, Jul 20 2012",
				"Equals 2*Sum_{n\u003e=1} 1/(6*n*(3*n + (-1)^n - 3) - 3*(-1)^n + 5) = 2*Sum_{n\u003e=1} 1/(2*A104777(n)). - _Alexander R. Povolotsky_, May 18 2014",
				"Equals A019670^2. - _Michel Marcus_, May 19 2014",
				"Equals 2*A086463 = 2*Sum_{n\u003e=1} 1/A091999(n)^2, equivalent to the formula of 2012 above. - _Alexander R. Povolotsky_, May 20 2014",
				"Equals 3F2(1,1,1; 3/2,2 ; 1/4), following from Clausen's formula of J. Reine Angew. Math 3 (1828) for squares of 2F1() as noted in A019670. - _R. J. Mathar_, Oct 16 2015",
				"Equals Product_{n \u003e= 3} prime(n)^2 / (prime(n)^2 - 1), Euler's prime product, excluding first two primes. - _Fred Daniel Kline_, Jun 09 2016",
				"Equals Integral_{x=0..oo} log(x)/(x^6 - 1) dx. - _Amiram Eldar_, Aug 12 2020",
				"Equals Sum_{k\u003e=1} A000120(k) * (2*k+1)/(k^2*(k+1)^2) (Allouche and Shallit, 1990). - _Amiram Eldar_, Jun 01 2021"
			],
			"example": [
				"1.096622711232150957648276777764..."
			],
			"mathematica": [
				"RealDigits[Pi^2/9, 10, 110][[1]] (* _G. C. Greubel_, Feb 17 2017 *)"
			],
			"program": [
				"(PARI) default(realprecision, 110); Pi^2/9 \\\\ _G. C. Greubel_, Feb 17 2017",
				"(Sage) numerical_approx(pi^2/9, digits=120) # _G. C. Greubel_, Jun 02 2021"
			],
			"xref": [
				"Cf. A000120, A019670, A086463, A091999."
			],
			"keyword": "nonn,cons,easy",
			"offset": "1,3",
			"author": "_Eric W. Weisstein_, Oct 31 2004",
			"references": 11,
			"revision": 58,
			"time": "2021-06-02T15:05:45-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}