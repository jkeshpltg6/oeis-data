{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A029847",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 29847,
			"data": "1,1,1,1,1,5,1,1,17,17,1,1,49,146,49,1,1,129,922,922,129,1,1,321,4887,11234,4887,321,1,1,769,23151,106439,106439,23151,769,1,1,1793,101488,856031,1679494,856031,101488,1793,1,1,4097,420512,6137832,21442606,21442606,6137832,420512,4097,1",
			"name": "Gessel-Stanley triangle read by rows: triangle of coefficients of polynomials arising in connection with enumeration of intransitive trees by number of nodes and number of right nodes.",
			"comment": [
				"For precise definition see Knuth (1997).",
				"Named after the American mathematicians Ira Martin Gessel (b. 1951) and Richard Peter Stanley (b. 1944). - _Amiram Eldar_, Jun 11 2021"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A029847/b029847.txt\"\u003eRows n = 0..141, flattened\u003c/a\u003e",
				"Donald E. Knuth, \u003ca href=\"/A323841/a323841.pdf\"\u003eLetter to Daniel Ullman and others\u003c/a\u003e, Apr 29 1997. [Annotated scanned copy, with permission]",
				"Alexander Postnikov, \u003ca href=\"https://doi.org/10.1006/jcta.1996.2735\"\u003eIntransitive Trees\u003c/a\u003e, J. Combin. Theory Ser. A, Vol. 79, No. 2 (1997), pp. 360-366."
			],
			"example": [
				"Triangle begins:",
				"  1;",
				"  .   1;",
				"  .   1,   1;",
				"  .   1,   5,   1;",
				"  .   1,  17,  17,   1;",
				"  .   1,  49, 146,  49,   1;",
				"  .   1, 129, 922, 922, 129, 1;",
				"  .   ..."
			],
			"maple": [
				"f:= proc(n,k) option remember; `if`(k\u003c0, 0, `if`(n=0",
				"      and k=0, 1, f(n-1,k-1)+add(add(binomial(n-1, l)",
				"      *s*f(l,s)*f(n-l-1,k-s), s=1..l), l=1..n-1)))",
				"    end:",
				"seq(seq(f(n, k), k=min(n, 1)..n), n=0..10); # _Alois P. Heinz_, Sep 24 2019"
			],
			"mathematica": [
				"f[n_, k_] := f[n, k] = If[k\u003c0, 0, If[n==0 \u0026\u0026 k==0, 1, f[n-1, k-1]+Sum[Sum[ Binomial[n-1, l]*s*f[l, s]*f[n-l-1, k-s], {s, 1, l}], {l, 1, n-1}]]];",
				"Table[Table[f[n, k], {k, Min[n, 1], n}], {n, 0, 10}] // Flatten (* _Jean-François Alcover_, Feb 14 2021, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Row sums give A007889."
			],
			"keyword": "nonn,tabf,easy",
			"offset": "0,6",
			"author": "_N. J. A. Sloane_.",
			"ext": [
				"More terms from Antonio G. Astudillo (afg_astudillo(AT)lycos.com), Mar 23 2003"
			],
			"references": 2,
			"revision": 28,
			"time": "2021-06-11T05:12:17-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}