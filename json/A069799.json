{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A069799",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 69799,
			"data": "1,2,3,4,5,6,7,8,9,10,11,18,13,14,15,16,17,12,19,50,21,22,23,54,25,26,27,98,29,30,31,32,33,34,35,36,37,38,39,250,41,42,43,242,75,46,47,162,49,20,51,338,53,24,55,686,57,58,59,150,61,62,147,64,65",
			"name": "The number obtained by reversing the sequence of nonzero exponents in the prime factorization of n with respect to distinct primes present, as ordered by their indices.",
			"comment": [
				"Equivalent description nearer to the old name: a(n) is a number obtained by reversing the indices of the primes present in the prime factorization of n, from the smallest to the largest, while keeping the nonzero exponents of those same primes at their old positions.",
				"This self-inverse permutation of natural numbers fixes the numbers in whose prime factorization the sequence of nonzero exponents form a palindrome: A242414.",
				"Integers which are changed are A242416.",
				"Considered as a function on partitions encoded by the indices of primes in the prime factorization of n (as in table A112798), this implements an operation which reverses the order of vertical line segments of the \"steps\" in Young (or Ferrers) diagram of a partition, but keeps the order of horizontal line segments intact. Please see the last example in the example section."
			],
			"link": [
				"Alois P. Heinz (first 1000 terms) \u0026 Antti Karttunen, \u003ca href=\"/A069799/b069799.txt\"\u003eTable of n, a(n) for n = 1..8192\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Young_diagram\"\u003eYoung diagram\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"If n = p_a^e_a * p_b^e_b * ... * p_j^e_j * p_k^e_k, where p_a \u003c ... \u003c p_k are distinct primes of the prime factorization of n (sorted into ascending order), and e_a, ..., e_k are their nonzero exponents, then a(n) = p_a^e_k * p_b^e_j * ... * p_j^e_b * p_k^e_a.",
				"a(n) = product(A027748(o(n)+1-k)^A124010(k): k=1..o(n)) = product(A027748(k)^A124010(o(n)+1-k): k=1..o(n)), where o(n) = A001221(n). - _Reinhard Zumkeller_, Apr 27 2013",
				"From _Antti Karttunen_, Jun 01 2014: (Start)",
				"Can be obtained also by composing/conjugating related permutations:",
				"a(n) = A242415(A242419(n)) = A242419(A242415(n)).",
				"a(n) = A122111(A242415(A122111(n))) = A153212(A242415(A153212(n))).",
				"(End)"
			],
			"example": [
				"a(24) = 54 as 24 = p_1^3 * p_2^1 = 2^3 * 3^1 and 54 = p_1^1 * p_2^3 = 2 * 3^3.",
				"For n = 2200, we see that it encodes the partition (1,1,1,3,3,5) in A112798 as 2200 = p_1 * p_1 * p_1 * p_3 * p_3 * p_5 = 2^3 * 5^2 * 11. This in turn corresponds to the following Young diagram in French notation:",
				"   _",
				"  | |",
				"  | |",
				"  | |_ _",
				"  |     |",
				"  |     |_ _",
				"  |_ _ _ _ _|",
				"Reversing the order of vertical line segment lengths (3,2,1)  to (1,2,3), but keeping the order of horizontal line segment lengths as (1,2,2), we get a new Young diagram",
				"   _",
				"  | |_ _",
				"  |     |",
				"  |     |_ _",
				"  |         |",
				"  |         |",
				"  |_ _ _ _ _|",
				"which represents the partition (1,3,3,5,5,5), encoded in A112798 by p_1 * p_3^2 * p_5^3 = 2 * 5^2 * 11^3 = 66550, thus a(2200) = 66550."
			],
			"maple": [
				"A069799 := proc(n) local e,j; e := ifactors(n)[2]:",
				"mul (e[j][1]^e[nops(e)-j+1][2], j=1..nops(e)) end:",
				"seq (A069799(i), i=1..40);",
				"# _Peter Luschny_, Jan 17 2011"
			],
			"mathematica": [
				"f[n_] := Block[{a = Transpose[ FactorInteger[n]], m = n}, If[ Length[a] == 2, Apply[ Times, a[[1]]^Reverse[a[[2]] ]], m]]; Table[ f[n], {n, 1, 65}]"
			],
			"program": [
				"(Haskell)",
				"a069799 n = product $",
				"            zipWith (^) (a027748_row n) (reverse $ a124010_row n)",
				"-- _Reinhard Zumkeller_, Apr 27 2013",
				"(MIT/GNU Scheme, with Aubrey Jaffer's SLIB Scheme library)",
				"(require 'factor)",
				"(define (A069799 n) (let ((pf (ifactor n))) (apply * (map expt (uniq pf) (reverse (multiplicities pf))))))",
				"(define (ifactor n) (cond ((\u003c n 2) (list)) (else (sort (factor n) \u003c))))",
				"(define (uniq lista) (let loop ((lista lista) (z (list))) (cond ((null? lista) (reverse! z)) ((and (pair? z) (equal? (car z) (car lista))) (loop (cdr lista) z)) (else (loop (cdr lista) (cons (car lista) z))))))",
				"(define (multiplicities lista) (let loop ((mults (list)) (lista lista) (prev #f)) (cond ((not (pair? lista)) (reverse! mults)) ((equal? (car lista) prev) (set-car! mults (+ 1 (car mults))) (loop mults (cdr lista) prev)) (else (loop (cons 1 mults) (cdr lista) (car lista))))))",
				";; _Antti Karttunen_, May 24 2014",
				"(PARI) a(n) = {my(f = factor(n)); my(g = f); my(nbf = #f~); for (i=1, nbf, g[i, 1] = f[nbf-i+1, 1];); factorback(g);} \\\\ _Michel Marcus_, Jul 02 2015"
			],
			"xref": [
				"A242414 gives the fixed points and A242416 is their complement.",
				"Cf. A112798, A059404, A122111, A153212, A137502, A241916, A242418.",
				"{A000027, A069799, A242415, A242419} form a 4-group.",
				"The set of permutations {A069799, A105119, A225891} generates an infinite dihedral group."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Amarnath Murthy_, Apr 13 2002",
			"ext": [
				"Edited, corrected and extended by _Robert G. Wilson v_ and _Vladeta Jovovic_, Apr 15 2002",
				"Definition corrected by _Reinhard Zumkeller_, Apr 27 2013",
				"Definition again reworded, Comments section edited and Young diagram examples added by _Antti Karttunen_, May 30 2014"
			],
			"references": 18,
			"revision": 51,
			"time": "2017-09-09T19:44:00-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}