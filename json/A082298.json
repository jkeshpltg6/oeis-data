{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A082298",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 82298,
			"data": "1,4,20,116,740,5028,35700,261780,1967300,15072836,117297620,924612532,7367204260,59240277988,480118631220,3917880562644,32163325863300,265446382860420,2201136740855700,18329850024033012,153225552507991140",
			"name": "G.f.: (1-3*x-sqrt(9*x^2-10*x+1))/(2*x).",
			"comment": [
				"More generally coefficients of (1-m*x-sqrt(m^2*x^2-(2*m+4)*x+1))/(2*x) are given by a(0)=1 and, for n\u003e0, a(n) = (1/n)*Sum_{k=0..n}(m+1)^k*binomial(n,k)*binomial(n,k-1).",
				"a(n) = number of lattice paths from (0,0) to (n+1,n+1) that consist of steps (i,0) and (0,j) with i,j\u003e=1 and that stay strictly below the diagonal line y=x except at the endpoints. (See Coker reference.) Equivalently, a(n) = number of marked Dyck (n+1)-paths where the vertices in the middle of each UU and each DD are available to be marked (or not): consider the original path as a Dyck path with a mark at each vertex where two horizontal (or two vertical) steps abut. If only the UU vertices are available for marking, then the counting sequence is the little Schroeder number A001003. - _David Callan_, Jun 07 2006",
				"Hankel transform is 4^C(n+1,2). - _Philippe Deléham_, Feb 11 2009",
				"a(n) is the number of Schroder paths of semilength n in which the (2,0)-steps come in 3 colors. Example: a(2)=20 because, denoting U=(1,1), H=(2,0), D=(1,-1), we have 3^2=9 paths of shape HH, 3 paths of shape HUD, 3 paths of shape UDH, 3 paths of shape UHD, and 1 path of each of the shapes UDUD, UUDD. - _Emeric Deutsch_, May 02 2011",
				"(1 + 4x + 20x^2 + 116x^3 + ...) = (1 + 5x + 29x^2 + 185x^3 + ...) * 1/(1 + x + 5x^2 + 29x^3 +185x^4 + ...); where A059231 = (1, 5, 29, 185, 1257, ...) - _Gary W. Adamson_, Nov 17 2011",
				"The first differences between the row sums of the triangle A226392. - _J. M. Bergot_, Jun 21 2013"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A082298/b082298.txt\"\u003eTable of n, a(n) for n = 0..300\u003c/a\u003e",
				"Paul Barry, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL9/Barry/barry91.html\"\u003eOn Integer-Sequence-Based Constructions of Generalized Pascal Triangles\u003c/a\u003e, Journal of Integer Sequences, Vol. 9 (2006), Article 06.2.4.",
				"Paul Barry, \u003ca href=\"https://www.emis.de/journals/JIS/VOL22/Barry3/barry422.html\"\u003eGeneralized Catalan Numbers Associated with a Family of Pascal-like Triangles\u003c/a\u003e, J. Int. Seq., Vol. 22 (2019), Article 19.5.8.",
				"P. Barry, A. Hennessy, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL14/Barry2/barry126.html\"\u003eA Note on Narayana Triangles and Related Polynomials, Riordan Arrays, and MIMO Capacity Calculations \u003c/a\u003e, J. Int. Seq. 14 (2011) # 11.3.8.",
				"Z. Chen, H. Pan, \u003ca href=\"http://arxiv.org/abs/1608.02448\"\u003eIdentities involving weighted Catalan-Schroder and Motzkin Paths\u003c/a\u003e, arXiv:1608.02448 [math.CO] (2016), eq. (1.13), a=4, b=1.",
				"C. Coker, \u003ca href=\"http://dx.doi.org/10.1016/S0012-365X(03)00037-2\"\u003eEnumerating a class of lattice paths\u003c/a\u003e, Discrete Math., 271 (2003), 13-28.",
				"Aoife Hennessy, \u003ca href=\"http://repository.wit.ie/1693/1/AoifeThesis.pdf\"\u003eA Study of Riordan Arrays with Applications to Continued Fractions, Orthogonal Polynomials and Lattice Paths\u003c/a\u003e, Ph. D. Thesis, Waterford Institute of Technology, Oct. 2011."
			],
			"formula": [
				"a(0)=1, n\u003e0 a(n) = (1/n)*Sum_{k=0..n} 4^k*binomial(n, k)*binomial(n, k-1).",
				"a(1)=1, a(n) = 3*a(n-1) + Sum_{i=1..n-1} a(i)*a(n-i). - _Benoit Cloitre_, Mar 16 2004",
				"a(n) = Sum_{k=0..n} 1/(n+1) Binomial(n+1,k)Binomial(2n-k,n-k)3^k. - _David Callan_, Jun 07 2006",
				"From _Paul Barry_, Feb 01 2009: (Start)",
				"G.f.: 1/(1-3x-x/(1-3x-x/(1-3x-x/(1-... (continued fraction);",
				"a(n) = Sum_{k=0..n} binomial(n+k,2k)*3^(n-k)*A000108(k). (End)",
				"a(n) = Sum_{k=0..n} A060693(n,k)*3^k. - _Philippe Deléham_, Feb 11 2009",
				"D-finite with recurrence: (n+1)*a(n) = 5*(2n-1)*a(n-1)-9*(n-2)*a(n-2). - _Paul Barry_, Oct 22 2009",
				"G.f.: 1/(1- 4x/(1-x/(1-4x/(1-x/(1-4x/(1-... (continued fraction). - Aoife Hennessy (aoife.hennessy(AT)gmail.com), Dec 02 2009",
				"G.f.: (1-3*x-sqrt(9*x^2-10*x+1))/(2*x) = (1-G(0))/x; G(k) = 1+x*3-x*4/G(k+1); (continued fraction, 1-step). - _Sergei N. Gladkovskii_, Jan 05 2012",
				"a(n) ~ 3^(2*n+1)/(sqrt(2*Pi)*n^(3/2)). - _Vaclav Kotesovec_, Oct 14 2012",
				"a(n) = 4*hypergeom([1 - n, -n], [2], 4)) for n\u003e0. - _Peter Luschny_, May 22 2017",
				"G.f. A(x) satisfies: A(x) = (1 + x*A(x)^2) / (1 - 3*x). - _Ilya Gutkovskiy_, Jun 30 2020",
				"G.f.: (1+2*x*F(x))^2, where F(x) is the g.f. for A099250. - _Alexander Burstein_, May 11 2021"
			],
			"maple": [
				"A082298_list := proc(n) local j, a, w; a := array(0..n); a[0] := 1;",
				"for w from 1 to n do a[w] := 4*a[w-1]+add(a[j]*a[w-j-1], j=1..w-1) od;convert(a,list)end: A082298_list(20); # _Peter Luschny_, May 19 2011",
				"a := n -\u003e `if`(n=0, 1, 4*hypergeom([1 - n, -n], [2], 4)):",
				"seq(simplify(a(n)), n=0..20); # _Peter Luschny_, May 22 2017"
			],
			"mathematica": [
				"gf[x_] = (1 - 3*x - Sqrt[(9*x^2 - 10*x + 1)])/(2*x); CoefficientList[Series[gf[x], {x, 0, 20}], x] (* _Jean-François Alcover_, Jun 01 2011 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c1,1,sum(k=0,n,4^k*binomial(n,k)*binomial(n,k-1))/n)",
				"(MAGMA) Q:=Rationals(); R\u003cx\u003e:=PowerSeriesRing(Q, 40); Coefficients(R!((1-3*x-Sqrt(9*x^2-10*x+1))/(2*x))) // _G. C. Greubel_, Feb 10 2018"
			],
			"xref": [
				"Cf. A006318, A047891, A059231, A099250."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Benoit Cloitre_, May 10 2003",
			"references": 9,
			"revision": 77,
			"time": "2021-05-12T03:05:07-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}