{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A279636",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 279636,
			"data": "1,1,1,1,1,2,1,1,3,5,1,1,5,10,15,1,1,9,22,41,52,1,1,17,52,125,196,203,1,1,33,130,413,836,1057,877,1,1,65,340,1445,3916,6277,6322,4140,1,1,129,922,5261,19676,41077,52396,41393,21147,1,1,257,2572,19685,104116,288517,481384,479593,293608,115975",
			"name": "Square array A(n,k), n\u003e=0, k\u003e=0, read by antidiagonals, where column k is the exponential transform of the k-th powers.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A279636/b279636.txt\"\u003eAntidiagonals n = 0..140, flattened\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Kronecker_delta\"\u003eKronecker delta\u003c/a\u003e"
			],
			"formula": [
				"E.g.f. of column k: exp(exp(x)*(Sum_{j=0..k} Stirling2(n,j)*x^j) - delta_{0,k})."
			],
			"example": [
				"Square array A(n,k) begins:",
				":   1,    1,    1,     1,      1,       1,        1, ...",
				":   1,    1,    1,     1,      1,       1,        1, ...",
				":   2,    3,    5,     9,     17,      33,       65, ...",
				":   5,   10,   22,    52,    130,     340,      922, ...",
				":  15,   41,  125,   413,   1445,    5261,    19685, ...",
				":  52,  196,  836,  3916,  19676,  104116,   572036, ...",
				": 203, 1057, 6277, 41077, 288517, 2133397, 16379797, ..."
			],
			"maple": [
				"egf:= k-\u003e exp(exp(x)*add(Stirling2(k, j)*x^j, j=0..k)-`if`(k=0, 1, 0)):",
				"A:= (n, k)-\u003e n!*coeff(series(egf(k), x, n+1), x, n):",
				"seq(seq(A(n, d-n), n=0..d), d=0..12);",
				"# second Maple program:",
				"A:= proc(n, k) option remember; `if`(n=0, 1,",
				"      add(binomial(n-1, j-1)*j^k*A(n-j, k), j=1..n))",
				"    end:",
				"seq(seq(A(n, d-n), n=0..d), d=0..12);"
			],
			"mathematica": [
				"A[n_, k_] := A[n, k] = If[n==0, 1, Sum[Binomial[n-1, j-1]*j^k*A[n-j, k], {j, 1, n}]]; Table[A[n, d-n], {d, 0, 12}, {n, 0, d}] // Flatten (* _Jean-François Alcover_, Feb 19 2017, translated from Maple *)"
			],
			"xref": [
				"Columns k=0-10 give: A000110, A000248, A033462, A279358, A279637, A279638, A279639, A279640, A279641, A279642, A279643.",
				"Rows n=0+1,2 give: A000012, A000051.",
				"Main diagonal gives A279644.",
				"Cf. A145460."
			],
			"keyword": "nonn,tabl",
			"offset": "0,6",
			"author": "_Alois P. Heinz_, Dec 16 2016",
			"references": 12,
			"revision": 16,
			"time": "2017-02-19T05:28:07-05:00",
			"created": "2016-12-16T10:04:41-05:00"
		}
	]
}