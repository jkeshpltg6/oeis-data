{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A055551",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 55551,
			"data": "0,0,1,12,36,114,375,1071,2939,7706,20417,53332,139597,364217,957111,2526795,6725234,18069359,48961462",
			"name": "Number of base-2 Euler-Jacobi pseudoprimes (A047713) less than 10^n.",
			"comment": [
				"Pomerance et al. gave the terms a(3)-a(10). Pinch gave the terms a(4)-a(13), but a(13)=124882 was wrong. He later calculated the correct value, which appears in Guy's book. - _Amiram Eldar_, Nov 08 2019"
			],
			"reference": [
				"Richard K. Guy, Unsolved Problems in Number Theory, 3rd Edition, Springer, 2004, section A12, p. 44."
			],
			"link": [
				"Jan Feitsma and William F. Galway, \u003ca href=\"http://www.cecm.sfu.ca/Pseudoprimes/index-2-to-64.html\"\u003eTables of pseudoprimes and related data\u003c/a\u003e.",
				"Richard G.E. Pinch, \u003ca href=\"https://doi.org/10.1007/10722028_30\"\u003eThe pseudoprimes up to 10^13\u003c/a\u003e, Algorithmic Number Theory, 4th International Symposium, ANTS-IV, Leiden, The Netherlands, July 2-7, 2000, Proceedings, Springer, Berlin, Heidelberg, 2000, pp. 459-473, \u003ca href=\"https://www.researchgate.net/publication/239577634_Algorithmic_Number_Theory_4th_International_Symposium_ANTS-IV_Leiden_The_Netherlands_July_2-7_2000_Proceedings\"\u003ealternative link\u003c/a\u003e.",
				"Carl Pomerance, John L. Selfridge, and Samuel S. Wagstaff, \u003ca href=\"https://doi.org/10.1090/S0025-5718-1980-0572872-7\"\u003eThe pseudoprimes to 25*10^9\u003c/a\u003e, Mathematics of Computation, Vol. 35, No. 151 (1980), pp. 1003-1026.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Euler-JacobiPseudoprime.html\"\u003eEuler-Jacobi Pseudoprime.\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Pseudoprime.html\"\u003ePseudoprime.\u003c/a\u003e"
			],
			"example": [
				"Below 10^3 there is only one Euler-Jacobi pseudoprime, 561. Therefore a(3) = 1."
			],
			"mathematica": [
				"ejpspQ[n_] := CompositeQ[n] \u0026\u0026 PowerMod[2, (n - 1)/2, n] == Mod[JacobiSymbol[2, n], n]; s = {}; c = 0; p = 10; n = 1; Do[If[ejpspQ[n], c++]; If[n \u003e p, AppendTo[s, c]; p *= 10], {n, 1, 1000001, 2}]; s (* _Amiram Eldar_, Nov 08 2019 *)"
			],
			"xref": [
				"Cf. A047713, A055550, A055552."
			],
			"keyword": "nonn,more",
			"offset": "1,4",
			"author": "_Eric W. Weisstein_",
			"ext": [
				"a(13) corrected and a(14)-a(19) added by _Amiram Eldar_, Nov 08 2019 (calculated from Feitsma \u0026 Galway's tables)"
			],
			"references": 1,
			"revision": 12,
			"time": "2019-11-08T18:17:43-05:00",
			"created": "2000-07-22T03:00:00-04:00"
		}
	]
}