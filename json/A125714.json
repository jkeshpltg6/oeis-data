{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A125714",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 125714,
			"data": "1,2,3,6,11,6,24,50,35,10,120,274,225,85,15,720,1764,1624,735,175,21,5040,13068,13132,6769,1960,322,28,40320,109584,118124,67284,22449,4536,546,36,362880,1026576,1172700,723680,269325,63273,9450,870,45,3628800",
			"name": "Alfred Moessner's factorial triangle.",
			"comment": [
				"Successive numbers arising from the Moessner construction of the factorial numbers. - _N. J. A. Sloane_, Jul 27 2021",
				"Row sums of the triangle = 1, 5, 23, 119, 719, ...(matching the terms 0, 0, 1, 5, 23, 119, 719, ...; of A033312).",
				"The name of the triangle derives from the fact that A125714(A000124(n)) = A000142(n) for n \u003e 0. Moessner's method uses only additions to compute the factorial n!. - _Peter Luschny_, Jan 27 2009",
				"If n = (m^2+m+2)/2 then a(n) = (m+1)!. For example, taking m = 3, n = 7, and indeed a(7) = 4! = 24. - _N. J. A. Sloane_, Jul 27 2021"
			],
			"reference": [
				"J. H. Conway and R. K. Guy, \"The Book of Numbers\", Springer-Verlag, 1996. Sequence can be seen by reading the successive circled numbers in the \"factorial\" section on page 64 (based on the work of Alfred Moessner)."
			],
			"link": [
				"Joshua Zucker, \u003ca href=\"/A125714/b125714.txt\"\u003eTable of n, a(n) for n = 1..66\u003c/a\u003e",
				"G. S. Kazandzidis, \u003ca href=\"http://www.hms.gr/apothema/?s=sap\u0026amp;i=20\"\u003eOn a conjecture of Moessner and a general problem\u003c/a\u003e, Bull. Soc. Math. Grèce (N.S.) 2 (1961), 23-30.",
				"Dexter Kozen and Alexandra Silva, \u003ca href=\"https://www.jstor.org/stable/10.4169/amer.math.monthly.120.02.131\"\u003eOn Moessner's theorem\u003c/a\u003e, Amer. Math. Monthly 120(2) (2013), 131-139.",
				"R. Krebbers, L. Parlant, and A. Silva, \u003ca href=\"https://doi.org/10.1007/978-3-319-30734-3_21\"\u003eMoessner's theorem: an exercise in coinductive reasoning in Coq\u003c/a\u003e,  Theory and practice of formal methods, 309-324, Lecture Notes in Comput. Sci., 9660, Springer, 2016.",
				"Calvin T. Long, \u003ca href=\"https://doi.org/10.2307/3615513\"\u003eStrike it out--add it up\u003c/a\u003e, Math. Gaz. 66 (438) (1982), 273-277.",
				"Alfred Moessner, \u003ca href=\"https://www.zobodat.at/pdf/Sitz-Ber-Akad-Muenchen-math-Kl_1951_0029.pdf\"\u003eEine Bemerkung über die Potenzen der natürlichen Zahlen\u003c/a\u003e, S.-B. Math.-Nat. Kl. Bayer. Akad. Wiss., 29, 1951.",
				"Ivan Paasche, \u003ca href=\"https://www.zobodat.at/pdf/Sitz-Ber-Akad-Muenchen-math-Kl_1952_0001-0005.pdf\"\u003eEin neuer Beweis des Moessnerschen Satzes\u003c/a\u003e S.-B. Math.-Nat. Kl. Bayer. Akad. Wiss. 1952 (1952), 1-5 (1953). [Two years are listed at the beginning of the journal issue.]",
				"Ivan Paasche, \u003ca href=\"https://doi.org/10.1007/BF01900739\"\u003eBeweis des Moessnerschen Satzes mittels linearer Transformationen\u003c/a\u003e, Arch. Math. (Basel) 6 (1955), 194-199.",
				"Ivan Paasche, \u003ca href=\"http://www.numdam.org/article/CM_1954-1956__12__263_0.pdf\"\u003eEine Verallgemeinerung des Moessnerschen Satzes\u003c/a\u003e, Compositio Math. 12 (1956), 263-270.",
				"Hans Salié, \u003ca href=\"https://www.zobodat.at/pdf/Sitz-Ber-Akad-Muenchen-math-Kl_1952_0007-0011.pdf\"\u003eBemerkung zu einem Satz von A. Moessner\u003c/a\u003e, S.-B. Math.-Nat. Kl. Bayer. Akad. Wiss. 1952 (1952), 7-11 (1953). [Two years are listed at the beginning of the journal issue.]",
				"Oskar Perron, \u003ca href=\"https://www.zobodat.at/pdf/Sitz-Ber-Akad-Muenchen-math-Kl_1951_0031-0034.pdf\"\u003eBeweis des Moessnerschen Satzes\u003c/a\u003e, S.-B. Math.-Nat. Kl. Bayer. Akad. Wiss., 31-34, 1951."
			],
			"formula": [
				"Starting with the natural numbers, circle each triangular number. Underneath, take partial sums of the uncircled terms and circle the terms in this row which are offset one place to the left of the circled 1, 3, 6, 10, ... in the first row. Repeat with analogous operations in succeeding rows. The circled terms in the infinite set become the triangle.",
				"Given n, let j = A003056(n-1)+1 and set t = j*(j+1)/2. Then, for 0 \u003c= i \u003c t, if n == -i (mod t), a(n) = abs(Stirling_1(j+1,j-i)). - _N. J. A. Sloane_, Jul 27 2021"
			],
			"example": [
				"An \"x\" prefaced before each term will indicate the term following the x being circled.",
				"x1 2 x3 4 5 x6 7 8 9 x10 11 12 13 14 x15 ...",
				"__x2 6 x11 18 26 x35 46 58 71 x85 ...",
				"_____________x6 24 x50 96 154 x225 ...",
				"_________________________x24 120 x274 ...",
				"___________________________________________x120 ...",
				"...",
				"i.e., circle the triangular terms in row 1. In row 2, take partial sums of the uncircled terms and circle the terms offset one place to the left of the triangular terms in row 1. Continue in subsequent rows with analogous operations. The triangle consists of the infinite set of terms prefaced with the x (circled on page 64 of \"The Book of Numbers\")."
			],
			"maple": [
				"a := proc(n) local s,m,k,i; s := array(0..n); s[0] := 1;",
				"for m from 1 to n do s[m] := 0; for k from m by - 1 to 1 do",
				"for i from 1 to k do s[i] := s[i] + s[i - 1] od; lprint(s[k]);",
				"if k = n then return(s[n]) fi od; lprint(\"-\") od end: # _Peter Luschny_, Jan 27 2009",
				"with(combinat);",
				"s:=stirling1;",
				"A003056 := proc(n) floor((sqrt(1+8*n)-1)/2) ; end proc:",
				"T:=n-\u003en*(n+1)/2; # A000217",
				"g:=proc(n) local i,j,t; global T,A003056;",
				"j:=A003056(n-1)+1;",
				"t:=T(j);",
				"for i from 0 to t-1 do",
				"if ((n+i) mod t) = 0 then return(abs(s(j+1,j-i))); fi;",
				"od;",
				"end;",
				"[seq(g(n),n=1..80)]; # _N. J. A. Sloane_, Jul 27 2021"
			],
			"mathematica": [
				"n = 10; A125714 = Reap[ ClearAll[s]; s[0] = 1; For[m = 1, m \u003c= n, m++, s[m] = 0; For[k = m, k \u003e= 1, k--, For[i = 1, i \u003c= k, i++, s[i] = s[i] + s[i-1]]; Sow[s[k]]; If[k == n, Print[n, \"! = \", s[n]]; Break[]]]]][[2, 1]] (* _Jean-François Alcover_, Jun 29 2012, after _Peter Luschny_ *)"
			],
			"program": [
				"(PARI) T(n, k)={ my( s=vector(n)); for( m=1, n, forstep( j=m,1,-1, s[1]++; for( i=2, j, s[i] += s[i-1]));",
				"k\u003c0 \u0026\u0026 print(vecextract(s,Str(m\"..1\"))));",
				"if( k\u003e0,s[n+1-k],vecextract(s,\"-1..1\"))} /* returns T[n,k], or the whole n-th row if k is not given, prints row 1...n of the triangle if k\u003c0 */ \\\\ _M. F. Hasler_, Dec 03 2010"
			],
			"xref": [
				"Cf. A000217, A003056, A008275, A033312, A346004-A346007, A346595."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Gary W. Adamson_, Dec 01 2006",
			"ext": [
				"More terms from _Joshua Zucker_, Jun 17 2007"
			],
			"references": 17,
			"revision": 46,
			"time": "2021-07-27T06:30:38-04:00",
			"created": "2006-12-06T03:00:00-05:00"
		}
	]
}