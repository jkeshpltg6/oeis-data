{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A219509",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 219509,
			"data": "1,5,24,49,200,4801,19208,46099201,184396808,4250272665676801,17001090662707208,36129635465198759610694779187201,144518541860795038442779116748808,2610701117696295981568349760414651575095962187244375364404428801",
			"name": "Pierce expansion of 40 - 16*sqrt(6).",
			"comment": [
				"Paradis et al. have determined the Pierce expansion of the quadratic irrationality 2*(p - 1)*(p - sqrt(p^2 - 1)), p a positive integer greater than or equal to 3. This is the case p = 5. For other cases see A219508 (p = 3), A219510 (p = 7) and A219511 (p = 9)"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A219509/b219509.txt\"\u003eTable of n, a(n) for n = 0..20\u003c/a\u003e",
				"J. Paradis, P. Viader, L. Bibiloni \u003ca href=\"http://www.fq.math.ca/Scanned/36-2/paradis.pdf\"\u003eApproximation to quadratic irrationals and their Pierce expansions\u003c/a\u003e, The Fibonacci Quarterly, Vol.36 No. 2 (1998) 146-153.",
				"T. A. Pierce, \u003ca href=\"http://www.jstor.org/stable/2299963\"\u003eOn an algorithm and its use in approximating roots of algebraic equations\u003c/a\u003e, Amer. Math. Monthly, Vol. 36 No. 10, (1929) p.523-525.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PierceExpansion.html\"\u003ePierce Expansion\u003c/a\u003e"
			],
			"formula": [
				"a(2*n+2) = 2*{(5 + 2*sqrt(6))^(2^n) + (5 - 2*sqrt(6))^(2^n) + 2} for n \u003e= 0.",
				"a(2*n+1) = 1/2*{(5 + 2*sqrt(6))^(2^n) + (5 - 2*sqrt(6))^(2^n)} for n \u003e= 0.",
				"Recurrence equations: a(0) = 1, a(1) = 5 and for n \u003e= 1, a(2*n) = 4*(a(2*n-1) + 1) and a(2*n+1) = 2*(a(2*n-1))^2 - 1.",
				"40 - 16*sqrt(6) = sum {n \u003e= 0} 1/product {k = 0..n} a(k) = 1 - 1/5 + 1/(5*24) - 1/(5*24*49) + 1/(5*24*49*200) - ....",
				"a(2*n) = 8*A084765(n-1)^2 for n \u003e= 2.",
				"a(2*n+1) = A084765(n+1) for n \u003e= 0."
			],
			"mathematica": [
				"PierceExp[A_, n_] := Join[Array[1 \u0026, Floor[A]], First@Transpose@ NestList[{Floor[1/Expand[1 - #[[1]] #[[2]]]], Expand[1 - #[[1]] #[[2]]]} \u0026, {Floor[1/(A - Floor[A])], A - Floor[A]}, n - 1]]; PierceExp[ N[4*(10 - 4*Sqrt[6]) , 7!], 10] (* _G. C. Greubel_, Nov 14 2016 *)"
			],
			"program": [
				"(PARI) r=(5 + 2*sqrt(6))/8; for(n=1, 10, print(floor(r), \", \"); r=r/(r-floor(r))) \\\\ _G. C. Greubel_, Nov 15 2016"
			],
			"xref": [
				"Cf. A084765, A219508 (p = 3), A219510 (p = 7), A219511 (p = 9)."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Peter Bala_, Nov 23 2012",
			"references": 4,
			"revision": 19,
			"time": "2016-11-15T02:40:47-05:00",
			"created": "2012-11-23T16:54:22-05:00"
		}
	]
}