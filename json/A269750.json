{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A269750",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 269750,
			"data": "1,1,0,1,1,1,1,2,4,1,1,3,8,9,2,1,4,13,23,25,3,1,5,19,44,72,69,5,1,6,26,73,152,222,203,8,1,7,34,111,275,511,703,623,13,1,8,43,159,452,997,1725,2272,1990,21,1,9,53,218,695,1754,3572,5854,7510,6559,34,1,10,64,289,1017,2870,6645,12717,20065,25325,22161,55",
			"name": "Triangle read by rows: row n gives coefficients of Schur polynomial Omega(n) in order of decreasing powers of x.",
			"comment": [
				"Row n contains n+1 terms."
			],
			"link": [
				"Gheorghe Coserea, \u003ca href=\"/A269750/b269750.txt\"\u003eRows n = 0..200, flattened\u003c/a\u003e",
				"Andrew Misseldine, \u003ca href=\"http://arxiv.org/abs/1508.03757\"\u003eCounting Schur Rings over Cyclic Groups\u003c/a\u003e, arXiv preprint arXiv:1508.03757 [math.RA], 2015."
			],
			"formula": [
				"G.f. A(x) = Sum_{n\u003e=0} P_n(t)*x^n = 2*(1-x)/(-2*x^2 + (t-2)*(x-1) + t*(1-x)*sqrt(1-4*x)), where P_n(t) = Sum_{k=0..n} T(n,k)*t^(n-k) (see Misseldine link); equivalently, the g.f. can be rewritten as y^2*(y^2 - y + 1)/(y^4 - y^3 + 2*y - 1 - t*y*(y - 1)*(y^2 - y + 1)), where y=A000108(x). - _Gheorghe Coserea_, Sep 10 2018"
			],
			"example": [
				"A(x) = 1 + t*x + (t^2 + t + 1)*x^2 + (t^3 + 2*t^2 + 4*t + 1)*x^3 + ...",
				"Triangle begins:",
				"n\\k [0]    [1]    [2]    [3]    [4]    [5]    [6]    [7]    [8]    [9]",
				"[0] 1;",
				"[1] 1,     0;",
				"[2] 1,     1,     1;",
				"[3] 1,     2,     4,     1;",
				"[4] 1,     3,     8,     9,     2;",
				"[5] 1,     4,     13,    23,    25,    3;",
				"[6] 1,     5,     19,    44,    72,    69,    5;",
				"[7] 1,     6,     26,    73,    152,   222,   203,   8;",
				"[8] 1,     7,     34,    111,   275,   511,   703,   623,   13;",
				"[9] 1,     8,     43,    159,   452,   997,   1725,  2272,  1990,  21;",
				"[10]..."
			],
			"mathematica": [
				"c[k_] := Binomial[2k, k]/(k+1);",
				"om[0] = 1; om[1] = x; om[n_] := om[n] = x om[n-1] + Sum[(c[k-1] x + 1) om[n - k], {k, 2, n}];",
				"row[n_] := CoefficientList[om[n], x] // Reverse;",
				"Table[row[n], {n, 0, 11}] // Flatten (* _Jean-François Alcover_, Sep 06 2018 *)"
			],
			"program": [
				"(PARI)",
				"seq(N, t='t) = {",
				"  my(a=vector(N), c(k)=binomial(2*k, k)/(k+1)); a[1]=1; a[2]=t;",
				"  for (n = 2, N-1,",
				"    a[n+1] = t*a[n] + sum(k = 2, n, (c(k-1)*t+1)*a[n+1-k]));",
				"  return(a);",
				"};",
				"concat(apply(Vec, seq(12)))",
				"(PARI)",
				"N=12; x='x + O('x^N); t='t;",
				"concat(apply(Vec, Vec(2*(1-x)/(-2*x^2 + (t-2)*(x-1) + t*(1-x)*sqrt(1-4*x)))))",
				"\\\\ _Gheorghe Coserea_, Sep 10 2018"
			],
			"xref": [
				"Cf. A000040, A000045(n-1)=P_n(0), A000108, A270789.",
				"For odd prime p, evaluating the polynomial P_n(t) at t=A000005(p-1) gives the number of Schur rings over Z_{p^n}. For p=3,5,7 we have t=2,3,4 and the associated sequences A270785(n) = P_n(2), A270786(n) = P_n(3), A270787(n) = P_n(4)."
			],
			"keyword": "nonn,tabl",
			"offset": "0,8",
			"author": "_N. J. A. Sloane_, Mar 22 2016",
			"ext": [
				"More terms from _Gheorghe Coserea_, Mar 24 2016"
			],
			"references": 6,
			"revision": 72,
			"time": "2018-09-12T11:01:25-04:00",
			"created": "2016-03-22T20:58:37-04:00"
		}
	]
}