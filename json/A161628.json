{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A161628",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 161628,
			"data": "1,0,-1,0,-2,3,0,-3,18,-16,0,-4,72,-192,125,0,-5,240,-1440,2500,-1296,0,-6,720,-8640,30000,-38880,16807,0,-7,2016,-45360,280000,-680400,705894,-262144,0,-8,5376,-217728,2240000,-9072000,16941456,-14680064,4782969",
			"name": "E.g.f.: A(x,y) = LambertW(x*y*exp(x))/(x*y*exp(x)), as a triangle of coefficients T(n,k) = [x^n*y^k/n! ] A(x,y), read by rows.",
			"comment": [
				"The sum of row r of the triangle is (-1)^r (see A244119). - _Stanislav Sykora_, Jun 21 2014"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A161628/b161628.txt\"\u003eTable of n, a(n) for the first 50 rows, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = (-1)^k*C(n,k)*(k+1)^(k-1)*k^(n-k).",
				"E.g.f. satisfies: A(x,y) = exp(-x*y*exp(x)*A(x,y)).",
				"E.g.f.: A(x,y) = Sum_{n\u003e=0} (n+1)^(n-1) * (-x)^n*y^n*exp(n*x)/n!.",
				"E.g.f.: A(x,y) = (1/x)*Series_Reversion[x*G(x,y)] where G(x,y) = exp(x*y*exp(x*G(x,y))) is the e.g.f. of A161552.",
				"More generally, if G(x,y) = exp(p*x*y*exp(q*x)*G(x,y)),",
				"where G(x,y)^m = Sum_{n\u003e=0} g(n,m)*x^n/n!,",
				"then g(n,m) = C(n,k)*p^k*q^(n-k) * m*(k+m)^(k-1) * k^(n-k)",
				"and G(x,y) = LambertW(-p*x*y*exp(q*x))/(-p*x*y*exp(q*x))."
			],
			"example": [
				"Triangle begins:",
				"1;",
				"0, -1;",
				"0, -2, 3;",
				"0, -3, 18, -16;",
				"0, -4, 72, -192, 125;",
				"0, -5, 240, -1440, 2500, -1296;",
				"0, -6, 720, -8640, 30000, -38880, 16807;",
				"0, -7, 2016, -45360, 280000, -680400, 705894, -262144;",
				"0, -8, 5376, -217728, 2240000, -9072000, 16941456, -14680064, 4782969;",
				"0, -9, 13824, -979776, 16128000, -102060000, 304946208, -462422016, 344373768, -100000000; ..."
			],
			"mathematica": [
				"Join[{1}, Table[(-1)^k*Binomial[n, k]*(k + 1)^(k - 1)*k^(n - k), {n, 1, 10}, {k, 0, n}]] // Flatten (* _G. C. Greubel_, Nov 09 2017 *)"
			],
			"program": [
				"(PARI) {T(n,k)=(-1)^k*binomial(n,k)*(k+1)^(k-1)*k^(n-k)}",
				"(PARI) {T(n,k)=local(A,LW=serreverse(x*exp(x+x*O(x^n))));A=subst(LW/x,x,x*y*exp(x));n!*polcoeff(polcoeff(A,n,x),k,y)}",
				"(PARI) {T(n,k)=local(G=1+x);for(i=0,n,G=exp(x*y*exp(x*G+O(x^n))));n!*polcoeff(polcoeff(serreverse(x*G)/x,n,x),k,y)}"
			],
			"xref": [
				"Cf. A161552, A244119."
			],
			"keyword": "sign,tabl",
			"offset": "0,5",
			"author": "_Paul D. Hanna_, Jun 15 2009, Jun 16 2009, Jun 17 2009",
			"references": 2,
			"revision": 12,
			"time": "2017-11-10T09:54:15-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}