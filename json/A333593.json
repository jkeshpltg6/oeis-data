{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A333593",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 333593,
			"data": "1,0,6,72,910,12000,163086,2266544,32043726,459167040,6651400756,97214919648,1431514320886,21213380196736,316072831033350,4731683468079072,71128104013487310,1073134004384407680,16243463355081280080,246585461357885877920",
			"name": "a(n) = Sum_{k = 0..n} (-1)^(n + k)*binomial(n + k - 1, k)^2.",
			"comment": [
				"It is known that Sum_{k = 0..2*n} (-1)^(n+k)*C(2*n,k)^2 = C(2*n,n). Here, we consider by analogy Sum_{k = 0..n} (-1)^(n+k)*C(-n,k)^2, where C(-n,k) = (-1)^k * C(n+k-1,k) for integer n and nonnegative integer k.",
				"The sequence b(n) = C(2*n,n) of central binomial coefficients satisfies the congruences b(n*p^k) = b(n*p^(k-1)) ( mod p^(3*k) ) for all prime p \u003e= 5 and any positive integers n and k -  see Mestrovic. We conjecture that the present sequence also satisfies these congruences. Some examples of the congruences are given below. For a proof of the particular case of these congruences, a(p) == 0 ( mod p^3 ) for prime p \u003e= 5, see the Bala link. The sequence a(p)/(2*p^3) for prime p \u003e= 5 begins [48, 304, 36519504, 4827806144, 109213719151680, 17975321574419440, ...]. Cf. A034602.",
				"More generally, calculation suggests that for positive integer A and integer B, the sequence a(A,B;n) := Sum_{k = 0..A*n} (-1)^(n+k)* C(-B*n,k)^2 = Sum_{k = 0..A*n} (-1)^(n+k)*C(B*n+k-1,k)^2 may also satisfy the above congruences for all prime p \u003e= 5."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A333593/b333593.txt\"\u003eTable of n, a(n) for n = 0..500\u003c/a\u003e",
				"P. Bala, \u003ca href=\"/A333593/a333593.pdf\"\u003eNotes on A333593\u003c/a\u003e",
				"R. Mestrovic, \u003ca href=\"http://arxiv.org/abs/1111.3057\"\u003eWolstenholme's theorem: Its Generalizations and Extensions in the last hundred and fifty years (1862-2011)\u003c/a\u003e, arXiv:1111.3057 [math.NT], 2011."
			],
			"formula": [
				"a(n) ~ 2^(4*n) / (5*Pi*n). - _Vaclav Kotesovec_, Mar 28 2020"
			],
			"example": [
				"Examples of congruences:",
				"a(11)  = 97214919648 = (2^5)*3*(7^2)*(11^3)*15527 == 0 ( mod 11^3 ).",
				"a(2*7) - a(2) = 316072831033350 - 6 = (2^13)*3*(7^3)*11*691*4933 == 0 ( mod 7^3 ).",
				"a(5^2) - a(5) = 3164395891098711251676512000 - 12000 = (2^5)*(5^6)*29* 124891891*1747384859327 == 0 ( mod 5^6 )."
			],
			"maple": [
				"seq( add( (-1)^(n+k)*binomial(n+k-1,k)^2, k = 0..n ), n = 0..25);"
			],
			"mathematica": [
				"Table[Binomial[2*n-1, n]^2 * HypergeometricPFQ[{1, -n, -n}, {1 - 2 n, 1 - 2 n}, -1], {n, 0, 20}] (* _Vaclav Kotesovec_, Mar 28 2020 *)"
			],
			"program": [
				"(PARI) a(n) = sum(k=0, n, (-1)^(n+k)*binomial(n+k-1, k)^2); \\\\ _Michel Marcus_, Mar 29 2020"
			],
			"xref": [
				"Cf. A000984, A034602, A333592."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Peter Bala_, Mar 27 2020",
			"references": 2,
			"revision": 20,
			"time": "2021-10-06T14:35:58-04:00",
			"created": "2020-03-30T08:45:13-04:00"
		}
	]
}