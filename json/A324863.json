{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A324863",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 324863,
			"data": "0,1,2,2,3,3,4,3,3,4,5,4,6,5,4,4,7,4,8,5,5,6,9,5,5,7,4,6,10,5,11,5,6,8,5,5,12,9,7,6,13,6,14,7,5,10,15,6,6,5,8,8,16,5,6,7,9,11,17,6,18,12,6,6,7,7,19,9,10,6,20,6,21,13,5,10,6,8,22,7,6,14,23,7,8,15,11,8,24,6,7,11,12,16,9,7,25,6,7,6,26,9,27,9,6",
			"name": "Binary length of A324866(n), where A324866(n) = A156552(n) OR (A323243(n) - A156552(n)).",
			"comment": [
				"Differs from A324861 [binary length of A324876(n)] for the first time at n=50.",
				"Provided that the maximal value that A324861(d) attains among divisors d of n is attained an odd number of times, then a(n) gives that maximal value. It is conjectured that this always holds. Among n = 1..10000, there are only two such cases, where the maximal value occurs more than once among the divisors: 3675 and 7623, where it occurs three times in both (see the examples)."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A324863/b324863.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (based on Hans Havermann's factorization of A156552)",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e",
				"\u003ca href=\"/index/Pri#prime_indices\"\u003eIndex entries for sequences computed from indices in prime factorization\u003c/a\u003e",
				"\u003ca href=\"/index/Si#SIGMAN\"\u003eIndex entries for sequences related to sigma(n)\u003c/a\u003e"
			],
			"formula": [
				"a(1) = 0; for n \u003e 1, a(n) = A070939(A324866(n)) = 1 + A000523(A324866(n)).",
				"a(A000040(n)) = n.",
				"a(n) = Max_{d|n} A324861(d) [conjectured]."
			],
			"example": [
				"For n = 50, we have A156552(50) = 25 and A323243(50) = 31. Taking bitwise-OR (A003986) of 25 and 31-25 = 6, we get 31, in binary \"11111\", with length 5, thus a(50) = 5.",
				"The rest of examples pertain to the conjectured interpretation of this sequence:",
				"Divisors of 8 are [1, 2, 4, 8]. A324861 applied to these gives values [0, 1, 2, 3], of which the largest is 3, thus a(8) = 3.",
				"Divisors of 25 are [1, 5, 25]. A324861 applied to these gives values [0, 3, 5], of which the largest is 5, thus a(25) = 5.",
				"Divisors of 50 are [1, 2, 5, 10, 25, 50]. A324861 applied to these gives values [0, 1, 3, 4, 5, 4], of which the largest is 5, thus a(50) = 5.",
				"Divisors of 88 are [1, 2, 4, 8, 11, 22, 44, 88]. A324861 applied to these gives values [0, 1, 2, 3, 5, 6, 7, 8], of which the largest is 8, thus a(88) = 8.",
				"Divisors of 3675 are [1, 3, 5, 7, 15, 21, 25, 35, 49, 75, 105, 147, 175, 245, 525, 735, 1225, 3675]. A324861 applied to these gives values [0, 2, 3, 4, 4, 5, 5, 5, 6, 4, 6, 5, 6, 5, 8, 7, 8, 8], of which the largest is 8 (occurs three times), thus a(3675) = 8.",
				"Divisors of 7623 are [1, 3, 7, 9, 11, 21, 33, 63, 77, 99, 121, 231, 363, 693, 847, 1089, 2541, 7623]. A324861 applied to these gives values [0, 2, 4, 3, 5, 5, 6, 6, 6, 7, 7, 7, 6, 8, 6, 9, 9, 9], of which the largest is 9 (occurs three times), thus a(7623) = 9."
			],
			"program": [
				"(PARI)",
				"A156552(n) = {my(f = factor(n), p2 = 1, res = 0); for(i = 1, #f~, p = 1 \u003c\u003c (primepi(f[i, 1]) - 1); res += (p * p2 * (2^(f[i, 2]) - 1)); p2 \u003c\u003c= f[i, 2]); res}; \\\\ From A156552 by _David A. Corneth_",
				"A324866(n) = { my(k=A156552(n)); bitor(k,(A323243(n)-k)); }; \\\\ Needs also code from A323243.",
				"A324863(n) = #binary(A324866(n));",
				"(PARI)",
				"A324876(n) = { my(v=0); fordiv(n, d, if(issquarefree(n/d), v=bitxor(v, A324866(d)))); (v); };",
				"A324861(n) = #binary(A324876(n));",
				"A324863(n) = { my(m=0,w,c=0); fordiv(n,d,w=A324861(d); if(w\u003e=m,if(w==m,c++,c=1;m=w))); (m); };"
			],
			"xref": [
				"Cf. A000523, A003986, A070939, A156552, A252464, A323243, A324861, A324864, A324866, A324874, A324879.",
				"Differs from A252464 for the first time at n=25, A324870 gives the differences."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Antti Karttunen_, Mar 21 2019",
			"references": 7,
			"revision": 21,
			"time": "2019-03-27T18:56:17-04:00",
			"created": "2019-03-27T18:56:17-04:00"
		}
	]
}