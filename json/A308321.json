{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A308321",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 308321,
			"data": "2,1,0,2,2,4,1,0,3,8,1,3,4,2,8,6,3,5,7,5,7,7,8,1,3,6,9,0,5,8,3,0,3,7,2,3,7,6,0,0,0,8,5,6,5,5,8,9,1,9,6,1,2,7,7,0,3,3,0,6,5,2,1,4,9,3,7,3,1,1,8,8,7,3,8,4,7,5,5,5,9,9,5,3,5,8,1,0",
			"name": "Decimal expansion of 2^(-9/4); exact width of the A4 paper size measured in meters according to the ISO 216 standard.",
			"comment": [
				"Also exact length of the A5 paper size measured in meters.",
				"According to the ISO 216 standard, the A0 paper size is defined to have an area of 1 square meter where the ratio of the length to the width is sqrt(2), so the length is 2^(1/4) m and the width is 2^(-1/4) m. For each n \u003e= 0, the length of the size A(n+1) is equal to the width of the size A(n) and the width of the size A(n+1) is equal to half of the length of the size A(n), so the area of the size A(n+1) is half of that of A(n). Equivalently, the length of the A(n) size is 2^(-n/2 + 1/4) m and the width is 2^(-n/2 - 1/4) m. For the A4 size, the exact length and width are 2^(-7/4) m = 297.3 mm (A308320) and 2^(-9/4) m = 210.2 mm, and the actual length and width are 297 mm and 210 mm."
			],
			"link": [
				"Prepressure, \u003ca href=\"https://www.prepressure.com/library/paper-size/din-a4\"\u003eA4\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/ISO-216\"\u003eISO 216\u003c/a\u003e"
			],
			"example": [
				"The exact lengths, widths and areas of the A-series is given by:",
				"  size |       exact length      |        exact width      | exact area (mm^2)",
				"   A0  | 2^(  1/4) m = 1189.2 mm | 2^(- 1/4) m =  840.8 mm |  1000000",
				"   A1  | 2^(- 1/4) m =  840.8 mm | 2^(- 3/4) m =  594.6 mm |   500000",
				"   A2  | 2^(- 3/4) m =  594.6 mm | 2^(- 5/4) m =  420.4 mm |   250000",
				"   A3  | 2^(- 5/4) m =  420.4 mm | 2^(- 7/4) m =  297.3 mm |   125000",
				"   A4  | 2^(- 7/4) m =  297.3 mm | 2^(- 9/4) m =  210.2 mm |    62500",
				"   A5  | 2^(- 9/4) m =  210.2 mm | 2^(-11/4) m =  148.7 mm |    31250",
				"   A6  | 2^(-11/4) m =  148.7 mm | 2^(-13/4) m =  105.1 mm |    15625",
				"   A7  | 2^(-13/4) m =  105.1 mm | 2^(-15/4) m =   74.3 mm |     7812.5",
				"   A8  | 2^(-15/4) m =   74.3 mm | 2^(-17/4) m =   52.6 mm |     3906.25",
				"   A9  | 2^(-17/4) m =   52.6 mm | 2^(-19/4) m =   37.2 mm |     1953.125",
				"   A10 | 2^(-19/4) m =   37.2 mm | 2^(-21/4) m =   26.3 mm |      976.5625",
				"And the actual lengths, widths and areas is given by (note that the actual areas are always smaller than the exact areas):",
				"  size | actual length (mm) | actual width (mm) | actual area (mm^2)",
				"   A0  |        1189        |        841        |  999949 (99.9949%)",
				"   A1  |         841        |        594        |  499554 (99.9108%)",
				"   A2  |         594        |        420        |  249480 (99.7920%)",
				"   A3  |         420        |        297        |  124740 (99.7920%)",
				"   A4  |         297        |        210        |   62370 (99.7920%)",
				"   A5  |         210        |        148        |   31080 (99.4560%)",
				"   A6  |         148        |        105        |   15540 (99.4560%)",
				"   A7  |         105        |         74        |    7770 (99.4560%)",
				"   A8  |          74        |         52        |    3848 (98.5088%)",
				"   A9  |          52        |         37        |    1924 (98.5088%)",
				"   A10 |          37        |         26        |     962 (98.5088%)"
			],
			"program": [
				"(PARI) default(realprecision, 100); 2^(-9/4)"
			],
			"xref": [
				"Cf. A010767 (2^(1/4)), A228497 (2^(-1/4)), A308320 (2^(-7/4))."
			],
			"keyword": "nonn,cons",
			"offset": "0,1",
			"author": "_Jianing Song_, May 20 2019",
			"references": 1,
			"revision": 10,
			"time": "2019-06-05T15:00:36-04:00",
			"created": "2019-06-05T15:00:36-04:00"
		}
	]
}