{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A342858",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 342858,
			"data": "13530,136,35,5,4510,10,100,45,51,1404",
			"name": "a(n) is the least integer h such that there exists a Pythagorean triple (x, y, h) that satisfies f(x)+f(y)+f(h)=n where f(m)=A176774(m) is the smallest polygonality of m; a(n) = 0 if no such h exists.",
			"comment": [
				"a(19) \u003e 10^9 if it exists.",
				"It appears that the triples whose sum is 10 (as in the 2nd example below) have legs n^6 = A001014(n), (n^8-n^4)/2 = A218131(n+1)/2 and (n^8 + n^4)/2 = A071231(n) for n\u003e=2; they consist of 2 triangular numbers and 1 square number. - _Michel Marcus_, Apr 12 2021"
			],
			"link": [
				"Michel Marcus, \u003ca href=\"/A342858/a342858.txt\"\u003eTable of n, a(n) for n=9..10000, with 0 for a(19)\u003c/a\u003e"
			],
			"example": [
				"a(9)  = 13530 with A176774([8778, 10296, 13530]) = [3,3,3].",
				"a(10) = 136   with A176774([64, 120, 136])       = [4,3,3].",
				"a(11) = 35    with A176774([21, 28, 35])         = [3,3,5].",
				"a(12) = 5     with A176774([3, 4, 5])            = [3,4,5].",
				"a(13) = 4510  with A176774([2926, 3432, 4510])   = [3,5,5].",
				"a(14) = 10    with A176774([6, 8, 10])           = [3,8,3].",
				"a(15) = 100   with A176774([28, 96, 100])        = [3,8,4].",
				"a(16) = 45    with A176774([27, 36, 45])         = [10,3,3].",
				"a(17) = 51    with A176774([45, 24, 51])         = [3,9,5].",
				"a(18) = 1404  with A176774([540, 1296, 1404])    = [7,4,7]."
			],
			"program": [
				"(PARI) tp(n) = if (n\u003c3, [n], my(v=List()); fordiv(2*n, k, if(k\u003c2, next); if(k==n, break); my(s=(2*n/k-4+2*k)/(k-1)); if(denominator(s)==1, listput(v, s))); v = Vec(v); v[#v]); \\\\ A176774",
				"vsum(v) = vecsum(apply(tp, v));",
				"lista(limp, lim) = {my(vr = vector(limp)); for(u = 2, sqrtint(lim), for(v = 1, u, if (u*u+v*v \u003e lim, break); if ((gcd(u,v) == 1) \u0026\u0026 (0 != (u-v)%2), for (i = 1, lim, if (i*(u*u+v*v) \u003e lim, break); my(w = [i*(u*u - v*v), i*2*u*v, i*(u*u+v*v)]); my(h = i*(u*u+v*v)); my(sw = vsum(w)); if (sw \u003c= limp, if (vr[sw] == 0, vr[sw] = h, if (h \u003c vr[sw], vr[sw] = h))););););); vector(#vr - 8, k, vr[k+8]);}",
				"lista(80, 15000) \\\\ _Michel Marcus_, Apr 16 2021"
			],
			"xref": [
				"Cf. A009000, A046083, A046084, A176774.",
				"Cf. A213188 (see 2nd comment).",
				"Cf. A245646, A245647, A245648, A342491.",
				"Cf. A001014, A071231, A218131."
			],
			"keyword": "nonn,hard,more",
			"offset": "9,1",
			"author": "_Michel Marcus_, Mar 26 2021",
			"references": 1,
			"revision": 43,
			"time": "2021-05-09T12:26:09-04:00",
			"created": "2021-03-28T00:11:55-04:00"
		}
	]
}