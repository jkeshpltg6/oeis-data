{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A206958",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 206958,
			"data": "1,-1,1,0,0,1,0,-1,0,0,0,0,-1,0,0,1,0,0,0,0,0,0,-1,0,0,0,-1,0,0,0,0,0,0,0,0,1,0,0,0,0,-1,0,0,0,0,0,0,0,0,0,0,-1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,-1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0",
			"name": "Expansion of f(x^5, -x^7) - x * f(-x, x^11) in powers of x where f() is Ramanujan's two-variable theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"This is an example of the quintuple product identity in the form f(a*b^4, a^2/b) - (a/b) * f(a^4*b, b^2/a) = f(-a*b, -a^2*b^2) * f(-a/b, -b^2) / f(a, b) where a = -x^3, b = x."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A206958/b206958.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/QuintupleProductIdentity.html\"\u003eQuintuple Product Identity\u003c/a\u003e"
			],
			"formula": [
				"Expansion of f(x^4, -x^8) * f(-x^8,-x^8) / f(x,-x^3) in powers of x where f() is Ramanujan't two-variable theta function.",
				"Euler transform of period 16 sequence [ -1, 1, 1, 0, 1, 0, -1, -2, -1, 0, 1, 0, 1, 1, -1, -1, ...].",
				"G.f.: Sum_{k in Z} (-1)^floor(k/2) * x^(k*(6*k + 2)) * (x^(-3*k) - x^(3*k + 1)).",
				"G.f.: Product_{k\u003e0} (1 - (-1)^k * x^(4*k-1)) * (1 + (-1)^k * x^(4*k-3)) * (1 - (-1)^k * x^(4*k)) * (1 + x^(8*k-6)) * (1 + x^(8*k-2)).",
				"a(5*n + 3) = a(5*n + 4) = 0. |a(n)| = A080995(n).",
				"a(n) = (-1)^n * A206959(n). - _Michael Somos_, Apr 01 2015"
			],
			"example": [
				"G.f. = 1 - x + x^2 + x^5 - x^7 - x^12 + x^15 - x^22 - x^26 + x^35 - x^40 - x^51 + ...",
				"G.f. = q - q^25 + q^49 + q^121 - q^169 - q^289 + q^361 - q^529 - q^625 + q^841 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ -x^12] (QPochhammer[ -x^5, -x^12] QPochhammer[ x^7, -x^12] - x QPochhammer[ x, -x^12] QPochhammer[ -x^11, -x^12]), {x, 0, n}]; (* _Michael Somos_, Apr 01 2015 *)",
				"a[ n_] := SeriesCoefficient[ Product[(1 - x^k)^{1, -1, -1, 0, -1, 0, 1, 2, 1, 0, -1, 0, -1, -1, 1, 1}[[Mod[k, 16, 1]]], {k, n}], {x, 0, n}]; (* _Michael Somos_, Apr 01 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = my(m); if( issquare( 24*n + 1, \u0026m), if( m%6 != 1, m = -m); m \\= 6; (-1)^(m \\ 4 + (m %4 == 2)), 0)};"
			],
			"xref": [
				"Cf. A080995, A206959."
			],
			"keyword": "sign",
			"offset": "0,1",
			"author": "_Michael Somos_, Feb 14 2012",
			"references": 2,
			"revision": 15,
			"time": "2021-03-12T22:24:46-05:00",
			"created": "2012-02-14T03:30:39-05:00"
		}
	]
}