{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A305720",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 305720,
			"data": "1,1,1,1,2,1,1,1,1,1,1,4,3,4,1,1,1,1,1,1,1,1,2,1,16,1,2,1,1,1,3,1,1,3,1,1,1,8,1,4,5,4,1,8,1,1,1,1,1,1,1,1,1,1,1,1,2,9,64,1,6,1,64,9,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,4,1,4,1,8,7,8",
			"name": "Square array T(n, k) read by antidiagonals, n \u003e 0 and k \u003e 0; for any prime number p, the p-adic valuation of T(n, k) is the product of the p-adic valuations of n and of k.",
			"comment": [
				"The array T is completely multiplicative in both parameters.",
				"For any n \u003e 0 and prime number p, T(n, p) is the highest power of p dividing n.",
				"For any function f associating a nonnegative value to any pair of nonnegative values and such that f(0, 0) = 0, we can build an analog of this sequence, say P_f, such that for any prime number p and any n and k \u003e 0 with p-adic valuations i and j, the p-adic valuation of P_f(n, k) equals f(i, j):",
				"   f(i, j)    P_f",
				"   -------    ---",
				"   i * j      T (this sequence)",
				"   i + j      A003991 (product)",
				"   abs(i-j)   A089913",
				"   min(i, j)  A003989 (GCD)",
				"   max(i, j)  A003990 (LCM)",
				"   i AND j    A059895",
				"   i OR j     A059896",
				"   i XOR j    A059897",
				"If log(N) denotes the set {log(n) : n is in N, the set of the positive integers}, one can define a binary operation on log(N): with prime factorizations n = Product p_i^e_i and k = Product p_i^f_i, set log(n) o log(k) = Sum_{i} (e_i*f_i) * log(p_i). o has the premises of a scalar product even if log(N) isn't a vector space. T(n, k) can be viewed as exp(log(n) o log(k)). - _Luc Rousseau_, Oct 11 2020"
			],
			"formula": [
				"T(n, k) = T(k, n) (T is commutative).",
				"T(m, T(n, k)) = T(T(m, n), k) (T is associative).",
				"T(n, k) = 1 iff gcd(n, k) = 1.",
				"T(n, n) = A054496(n).",
				"T(n, A007947(n)) = n.",
				"T(n, 1) = 1.",
				"T(n, 2) = A006519(n).",
				"T(n, 3) = A038500(n).",
				"T(n, 4) = A006519(n)^2.",
				"T(n, 5) = A060904(n).",
				"T(n, 6) = A065331(n).",
				"T(n, 7) = A268354(n).",
				"T(n, 8) = A006519(n)^3.",
				"T(n, 9) = A038500(n)^2.",
				"T(n, 10) = A132741(n).",
				"T(n, 11) = A268357(n)."
			],
			"example": [
				"Array T(n, k) begins:",
				"  n\\k|    1    2    3    4    5    6    7    8    9   10",
				"  ---+--------------------------------------------------",
				"    1|    1    1    1    1    1    1    1    1    1    1",
				"    2|    1    2    1    4    1    2    1    8    1    2  -\u003e A006519",
				"    3|    1    1    3    1    1    3    1    1    9    1  -\u003e A038500",
				"    4|    1    4    1   16    1    4    1   64    1    4",
				"    5|    1    1    1    1    5    1    1    1    1    5  -\u003e A060904",
				"    6|    1    2    3    4    1    6    1    8    9    2  -\u003e A065331",
				"    7|    1    1    1    1    1    1    7    1    1    1  -\u003e A268354",
				"    8|    1    8    1   64    1    8    1  512    1    8",
				"    9|    1    1    9    1    1    9    1    1   81    1",
				"   10|    1    2    1    4    5    2    1    8    1   10  -\u003e A132741"
			],
			"mathematica": [
				"T[n_, k_] := With[{p = FactorInteger[GCD[n, k]][[All, 1]]}, If[p == {1}, 1, Times @@ (p^(IntegerExponent[n, p] * IntegerExponent[k, p]))]];",
				"Table[T[n-k+1, k], {n, 1, 15}, {k, 1, n}] // Flatten (* _Jean-François Alcover_, Jun 11 2018 *)"
			],
			"program": [
				"(PARI) T(n, k) = my (p=factor(gcd(n, k))[,1]); prod(i=1, #p, p[i]^(valuation(n, p[i]) * valuation(k, p[i])))"
			],
			"xref": [
				"Cf. A003989, A003990, A003991, A006519, A007947, A038500, A054496, A059895, A059896, A059897, A060904, A065331, A089913, A132741, A268354, A268357."
			],
			"keyword": "nonn,tabl,mult",
			"offset": "1,5",
			"author": "_Rémy Sigrist_, Jun 09 2018",
			"references": 0,
			"revision": 22,
			"time": "2020-10-21T23:05:23-04:00",
			"created": "2018-06-11T08:48:46-04:00"
		}
	]
}