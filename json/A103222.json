{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A103222",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 103222,
			"data": "1,1,2,2,2,2,6,4,6,0,10,4,8,6,4,8,12,6,18,0,12,10,22,8,10,4,18,12,22,0,30,16,20,8,12,12,30,18,16,0,32,12,42,20,12,22,46,16,42,0,24,8,44,18,20,24,36,16,58,0,50,30,36,32,8,20,66,16,44,0,70,24,62,24,20,36,60,8,78,0",
			"name": "Real part of the totient function phi(n) for Gaussian integers. See A103223 for the imaginary part and A103224 for the norm.",
			"comment": [
				"This definition of the totient function for Gaussian integers preserves many of the properties of the usual totient function: (1) it is multiplicative: if gcd(z1,z2)=1, then phi(z1*z2)=phi(z1)*phi(z2), (2) phi(z^2)=z*phi(z), (3) z=Sum_{d|z} phi(d) for properly selected divisors d and (4) the congruence z=1 (mod phi(z)) appears to be true only for Gaussian primes. The first negative term occurs for n=130=2*5*13, the product of the first three primes which are not Gaussian primes."
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A103222/b103222.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (terms 1..1000 from T. D. Noe)",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/TotientFunction.html\"\u003eTotient Function\u003c/a\u003e"
			],
			"formula": [
				"Let a nonzero Gaussian integer z have the factorization u p1^e1...pn^en, where u is a unit (1, i, -1, -i), the pk are Gaussian primes in the first quadrant and the ek positive integers. Then we define phi(z) = u*product_{k=1..n} (pk-1) pk^(ek-1)."
			],
			"mathematica": [
				"phi[z_] := Module[{f, k, prod}, If[Abs[z]==1, z, f=FactorInteger[z, GaussianIntegers-\u003eTrue]; If[Abs[f[[1, 1]]]==1, k=2; prod=f[[1, 1]], k=1; prod=1]; Do[prod=prod*(f[[i, 1]]-1)f[[i, 1]]^(f[[i, 2]]-1), {i, k, Length[f]}]; prod]]; Re[Table[phi[n], {n, 100}]]"
			],
			"xref": [
				"Cf. A103223, A103224."
			],
			"keyword": "nice,sign",
			"offset": "1,3",
			"author": "_T. D. Noe_, Jan 26 2005",
			"references": 6,
			"revision": 16,
			"time": "2020-02-21T07:07:32-05:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}