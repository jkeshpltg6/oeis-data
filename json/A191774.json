{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A191774",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 191774,
			"data": "1,2,1,1,2,1,1,1,2,2,1,1,1,1,2,1,2,1,1,1,1,2,1,2,2,1,1,2,1,1,1,1,1,2,1,2,2,1,1,1,2,1,1,1,1,1,2,1,2,1,1,2,2,2,1,1,1,1,1,2,1,1,1,1,1,1,2,1,1,2,1,1,2,2,2,2,1,1,1,1,1,1,2,1,2,1",
			"name": "Lim f(f(...f(n)...)) where f(n) is the Farey fractal sequence, A131967.",
			"comment": [
				"Suppose that f(1), f(2), f(3),... is a fractal sequence (a sequence which contains itself as a proper subsequence, such as 1, 2, 1, 2, 3, 1, 2, 3, 4, 1, 2, 3, 4, 5, 1, 2, 3, 4, 5, 6, ...; if the first occurrence of each n is deleted, the remaining sequence is identical to the original; see the Wikipedia article for a rigorous definition).  Then for each n\u003e=1, the limit L(n) of composites f(f(f...f(n)...)) exists and is one of the numbers in the set {k : f(k)=k}.  Thus, if f(2)\u003e2, then L(n)=1 for all n; if f(2)=2 and f(3)\u003e3, then L(n) is 1 or 2 for all n.  Examples:  A020903, A191770, A191774"
			],
			"link": [
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Fractal_sequence\"\u003eFractal sequence\u003c/a\u003e"
			],
			"example": [
				"Write the counting numbers and A131967 like this:",
				"1..2..3..4..5..6..7..8..9..10..11..12..13..14..15..",
				"1..2..1..3..2..1..4..3..5..2...1...6...4...3...5...",
				"It is then easy to check composites:",
				"1-\u003e1, 2-\u003e2, 3-\u003e1, 4-\u003e3-\u003e1, 5-\u003e2, 6-\u003e1, 7-\u003e4-\u003e3-\u003e1,..."
			],
			"mathematica": [
				"Farey[n_] := Select[Union@Flatten@Outer[Divide, Range[n + 1] - 1, Range[n]], # \u003c= 1 \u0026];",
				"newpos[n_] := Module[{length = Total@Array[EulerPhi, n] + 1, f1 = Farey[n], f2 = Farey[n - 1], to},",
				"   to = Complement[Range[length], Flatten[Position[f1, #] \u0026 /@ f2]];",
				"   ReplacePart[Array[0 \u0026, length],",
				"    Inner[Rule, to, Range[length - Length[to] + 1, length], List]]];",
				"a[n_] := Flatten@Table[Fold[ReplacePart[Array[newpos, i][[#2 + 1]], Inner[Rule, Flatten@Position[Array[newpos, i][[#2 + 1]], 0], #1, List]] \u0026, Array[newpos, i][[1]], Range[i - 1]], {i, n}];",
				"t = a[12]; f[n_] := Part[t, n];",
				"Table[f[n], {n, 1, 100}]          (* A131967 *)",
				"h[n_] := Nest[f, n, 50]",
				"t = Table[h[n], {n, 1, 200}]      (* A191774 *)",
				"s = Flatten[Position[t, 1]]       (* A191775 *)",
				"s = Flatten[Position[t, 2]]       (* A191776 *)"
			],
			"xref": [
				"Cf. A020903, A191770, A191775, A191776."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Jun 16 2011",
			"references": 5,
			"revision": 5,
			"time": "2012-03-30T18:57:33-04:00",
			"created": "2011-06-18T14:20:54-04:00"
		}
	]
}