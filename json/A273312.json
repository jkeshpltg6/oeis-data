{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A273312",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 273312,
			"data": "3,13,23,33,39,49,55,65,71,81,87,97,103,113,119,129,135,145,151,161,167,177,183,193,199,209,215,225,231,241,247,257,263,273,279,289,295,305,311,321,327,337,343,353,359,369,375,385,391,401,407,417,423,433",
			"name": "First differences of number of active (ON,black) cells in n-th stage of growth of two-dimensional cellular automaton defined by \"Rule 641\", based on the 5-celled von Neumann neighborhood.",
			"comment": [
				"Initialized with a single black (ON) cell at stage zero.",
				"Appears to be essentially the same as A273407."
			],
			"reference": [
				"S. Wolfram, A New Kind of Science, Wolfram Media, 2002; p. 170."
			],
			"link": [
				"Robert Price, \u003ca href=\"/A273312/b273312.txt\"\u003eTable of n, a(n) for n = 0..127\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"http://arxiv.org/abs/1503.01168\"\u003eOn the Number of ON Cells in Cellular Automata\u003c/a\u003e, arXiv:1503.01168 [math.CO], 2015",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ElementaryCellularAutomaton.html\"\u003eElementary Cellular Automaton\u003c/a\u003e",
				"S. Wolfram, \u003ca href=\"http://wolframscience.com/\"\u003eA New Kind of Science\u003c/a\u003e",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_2D_5-Neighbor_Cellular_Automata\"\u003eIndex to 2D 5-Neighbor Cellular Automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_Elementary_Cellular_Automata\"\u003eIndex to Elementary Cellular Automata\u003c/a\u003e"
			],
			"formula": [
				"Conjectures from _Colin Barker_, May 19 2016: (Start)",
				"a(n) = 8-(-1)^n+8*n for n\u003e1.",
				"a(n) = 8*n+7 for n\u003e1 and even.",
				"a(n) = 8*n+9 for n\u003e1 and odd.",
				"a(n) = a(n-1)+a(n-2)-a(n-3) for n\u003e2.",
				"G.f.: (3+10*x+7*x^2-4*x^4) / ((1-x)^2*(1+x)).",
				"(End)"
			],
			"mathematica": [
				"CAStep[rule_,a_]:=Map[rule[[10-#]]\u0026,ListConvolve[{{0,2,0},{2,1,2},{0,2,0}},a,2],{2}];",
				"code=641; stages=128;",
				"rule=IntegerDigits[code,2,10];",
				"g=2*stages+1; (* Maximum size of grid *)",
				"a=PadLeft[{{1}},{g,g},0,Floor[{g,g}/2]]; (* Initial ON cell on grid *)",
				"ca=a;",
				"ca=Table[ca=CAStep[rule,ca],{n,1,stages+1}];",
				"PrependTo[ca,a];",
				"(* Trim full grid to reflect growth by one cell at each stage *)",
				"k=(Length[ca[[1]]]+1)/2;",
				"ca=Table[Table[Part[ca[[n]][[j]],Range[k+1-n,k-1+n]],{j,k+1-n,k-1+n}],{n,1,k}];",
				"on=Map[Function[Apply[Plus,Flatten[#1]]],ca] (* Count ON cells at each stage *)",
				"Table[on[[i+1]]-on[[i]],{i,1,Length[on]-1}] (* Difference at each stage *)"
			],
			"xref": [
				"Cf. A273309, A273407."
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "_Robert Price_, May 19 2016",
			"references": 2,
			"revision": 12,
			"time": "2016-05-23T17:06:57-04:00",
			"created": "2016-05-20T02:28:33-04:00"
		}
	]
}