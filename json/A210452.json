{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A210452",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 210452,
			"data": "0,0,0,0,1,1,2,2,2,2,1,2,2,3,3,3,1,3,2,4,4,4,2,4,4,4,4,4,2,4,2,5,5,4,5,5,2,4,5,5,1,5,2,6,6,5,2,6,6,6,6,6,2,6,6,6,6,5,2,6,3,5,7,7,7,7,3,7,7,7,3,7,4,6,8,8,8,8,3,8,8,6,3,8,8,6,8,8,3,8,8,8,7,6,8,8,3,8,8,8",
			"name": "Number of integers k\u003cn with k-1 and k+1 both prime, and k and k*n both practical.",
			"comment": [
				"Conjecture: a(n)\u003e0 for all n\u003e4.",
				"This implies the twin prime conjecture since k*p is not practical for any prime p\u003esigma(k)+1.",
				"Zhi-Wei Sun also made the following conjectures:",
				"(1) For each integer n\u003e197, there is a practical number k\u003cn with k-2, k+2, k*n all practical.",
				"(2) For every n=9,10,... there is a practical number k\u003cn with k-4, k+4, k*n all practical.",
				"(3) For any integer n\u003e26863, the interval [1,n] contains five consecutive integers m-2, m-1, m, m+1, m+2 with m-1 and m+1 both prime, and m-2, m, m+2, m*n all practical."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A210452/b210452.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"G. Melfi, \u003ca href=\"http://dx.doi.org/10.1006/jnth.1996.0012\"\u003eOn two conjectures about practical numbers\u003c/a\u003e, J. Number Theory 56 (1996) 205-210 [\u003ca href=\"http://www.ams.org/mathscinet-getitem?mr=1370203\"\u003eMR96i:11106\u003c/a\u003e].",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1211.1588\"\u003eConjectures involving primes and quadratic forms\u003c/a\u003e, arXiv:1211.1588 [math.NT], 2012-2017.",
				"Zhi-Wei Sun, \u003ca href=\"http://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;20e70044.1301\"\u003eSandwiches with primes and practical numbers\u003c/a\u003e, a message to Number Theory List, Jan. 13, 2013."
			],
			"example": [
				"a(11)=1 since 5 and 7 are twin primes, and 6 and 6*11 are both practical."
			],
			"mathematica": [
				"f[n_]:=f[n]=FactorInteger[n]",
				"Pow[n_, i_]:=Pow[n, i]=Part[Part[f[n], i], 1]^(Part[Part[f[n], i], 2])",
				"Con[n_]:=Con[n]=Sum[If[Part[Part[f[n], s+1], 1]\u003c=DivisorSigma[1, Product[Pow[n, i], {i, 1, s}]]+1, 0, 1], {s, 1, Length[f[n]]-1}]",
				"pr[n_]:=pr[n]=n\u003e0\u0026\u0026(n\u003c3||Mod[n, 2]+Con[n]==0)",
				"a[n_]:=a[n]=Sum[If[PrimeQ[k-1]==True\u0026\u0026PrimeQ[k+1]==True\u0026\u0026pr[k]==True\u0026\u0026pr[k*n]==True,1,0],{k,1,n-1}]",
				"Do[Print[n,\" \",a[n]],{n,1,100}]"
			],
			"xref": [
				"Cf. A005153, A210444, A210445, A071558, A208243, A208244, A208246, A208249, A219185, A209253, A209254, A219312, A219315, A219320."
			],
			"keyword": "nonn",
			"offset": "1,7",
			"author": "_Zhi-Wei Sun_, Jan 20 2013",
			"references": 2,
			"revision": 16,
			"time": "2018-12-08T11:21:26-05:00",
			"created": "2013-01-20T22:49:00-05:00"
		}
	]
}