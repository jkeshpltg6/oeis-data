{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A033493",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 33493,
			"data": "1,3,49,7,36,55,288,15,339,46,259,67,119,302,694,31,214,357,519,66,148,281,633,91,658,145,101440,330,442,724,101104,63,841,248,540,393,535,557,2344,106,101331,190,1338,325,497,679,100979,139,806,708,1130,197",
			"name": "Sum of the numbers in the trajectory of n for the 3x+1 problem.",
			"comment": [
				"Given a power of two, the value in this sequence is the next higher Mersenne number, or a(2^m) = 2^(m + 1) - 1. - _Alonso del Arte_, Apr 10 2009",
				"a(n) = sum (A070165(k): 1 \u003c= k \u003c= A006577(n)). - _Reinhard Zumkeller_, Oct 08 2011"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A033493/b033493.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CollatzProblem.html\"\u003eCollatz Problem\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Collatz_conjecture\"\u003eCollatz conjecture\u003c/a\u003e",
				"\u003ca href=\"/index/3#3x1\"\u003eIndex entries for sequences related to 3x+1 (or Collatz) problem\u003c/a\u003e"
			],
			"example": [
				"a(5) = 36 because the Ulam's conjecture trajectory sequence starting on 5 runs 5, 16, 8, 4, 2, 1 and therefore 5 + 16 + 8 + 4 + 2 + 1 = 36. - _Alonso del Arte_, Apr 10 2009"
			],
			"maple": [
				"a:= proc(n) option remember; n+`if`(n=1, 0,",
				"      a(`if`(n::even, n/2, 3*n+1)))",
				"    end:",
				"seq(a(n), n=1..55);  # _Alois P. Heinz_, Jan 29 2021"
			],
			"mathematica": [
				"collatz[1] = 1; collatz[n_Integer?OddQ] := 3n + 1; collatz[n_Integer?EvenQ] := n/2; Table[-1 + Plus @@ FixedPointList[collatz, n], {n, 60}] (* _Alonso del Arte_, Apr 10 2009 *)"
			],
			"program": [
				"(Haskell)",
				"a033493 = sum . a070165_row  -- _Reinhard Zumkeller_, Oct 08 2011",
				"(Python)",
				"def a(n):",
				"    if n==1: return 1",
				"    l=[n, ]",
				"    while True:",
				"        if n%2==0: n/=2",
				"        else: n = 3*n + 1",
				"        l+=[n, ]",
				"        if n\u003c2: break",
				"    return sum(l)",
				"print([a(n) for n in range(1, 101)])  # _Indranil Ghosh_, Apr 14 2017"
			],
			"xref": [
				"Apart from initial term, exactly the same as A049074. - _Alonso del Arte_, Apr 10 2009",
				"Cf. A006370."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Jeff Burch_",
			"ext": [
				"Corrected a(16) to 31 to match other powers of 2; removed duplicate value of a(48) = 139 because a(49) = 806 and not 139. - _Alonso del Arte_, Apr 10 2009"
			],
			"references": 17,
			"revision": 39,
			"time": "2021-01-29T08:29:53-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}