{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A132870",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 132870,
			"data": "1,1,1,6,4,1,132,45,9,1,7156,1432,168,16,1,729895,101725,7550,450,25,1,119636226,12938076,697590,27420,990,36,1,28619359629,2559100705,110137692,3226895,78890,1911,49,1,9374688646296,721024536688,26208036736,624158528,11572400,193312,3360,64,1",
			"name": "Triangle T, read by rows, where the g.f. of row n of T^n = (n^2 + y)^n for 0 \u003c= n \u003c= 29, where T^n denotes the n-th power of T considered as (lower-left) matrix.",
			"comment": [
				"Pascal's triangle, C, obeys: g.f. of row n of C^n = (n + y)^n for n \u003e= 0.",
				"Starting from row 30 on, the terms computed by the given formulae are not integers any more (as noticed by _Alois P. Heinz_). - _M. F. Hasler_, Nov 19 2017"
			],
			"link": [
				"M. F. Hasler, \u003ca href=\"/A132870/b132870.txt\"\u003eTable of n, a(n) for n = 0..464\u003c/a\u003e (Rows 0..29; from row 30 on the table would contain fractions.)"
			],
			"formula": [
				"Diagonals: T(n,n) = 1 for n \u003e= 0. T(n,n-1) = n^2 for n \u003e= 1. T(n, n-2) = (n-1)^2*n*(2n-1)/2. - _M. F. Hasler_, Nov 19 2017"
			],
			"example": [
				"Triangle T begins:",
				"1;",
				"1, 1;",
				"6, 4, 1;",
				"132, 45, 9, 1;",
				"7156, 1432, 168, 16, 1;",
				"729895, 101725, 7550, 450, 25, 1;",
				"119636226, 12938076, 697590, 27420, 990, 36, 1;",
				"28619359629, 2559100705, 110137692, 3226895, 78890, 1911, 49, 1;",
				"9374688646296, 721024536688, 26208036736, 624158528, 11572400, 193312, 3360, 64, 1;",
				"  ...",
				"Matrix square T^2 (padded with 0's to the right of the diagonal) begins:",
				"   1;",
				"   2, 1;",
				"   16, 8, 1;     \u003c== g.f. of row 2: (2^2 + y)^2",
				"   363, 126, 18, 1;",
				"   18864, 4256, 480, 32, 1;",
				"   1845115, 289700, 23350, 1300, 50, 1; ...",
				"Matrix cube T^3 begins:",
				"   1;",
				"   3, 1;",
				"   30, 12, 1;",
				"   729, 243, 27, 1;     \u003c== g.f. of row 3: (3^2 + y)^3",
				"   37380, 9048, 936, 48, 1;",
				"   3534210, 614925, 51000, 2550, 75, 1; ...",
				"Matrix 4th power T^4 begins:",
				"   1;",
				"   4, 1;",
				"   48, 16, 1;",
				"   1266, 396, 36, 1;",
				"   65536, 16384, 1536, 64, 1;     \u003c== g.f. of row 4: (4^2 + y)^4",
				"   6058330, 1142800, 94100, 4200, 100, 1; ...",
				"Matrix 5th power T^5 begins:",
				"   1;",
				"   5, 1;",
				"   70, 20, 1;",
				"   2010, 585, 45, 1;",
				"   106740, 26840, 2280, 80, 1;",
				"   9765625, 1953125, 156250, 6250, 125, 1;     \u003c== g.f. of row 5: (5^2 + y)^5",
				"   1431275130, 222982380, 15380550, 601500, 13950, 180, 1; ...",
				"etc."
			],
			"mathematica": [
				"pt = {{1}}; Table[rhs = CoefficientList[(k^2 + x)^k, x];",
				"qt = Join[pt, {vars = Array[Subscript[a, #] \u0026, k + 1]}];",
				"b = MatrixPower[PadRight[qt], k] ;",
				"{out} = vars /. Solve[Thread[Reverse[Last[b]] == Reverse[rhs]], vars];",
				"pt = Join[pt, {out}]; out, {k, 20}] (* _Wouter Meeussen_, Nov 18 2017 *)"
			],
			"program": [
				"(PARI) T(n,k=-1/*omit 2nd arg to get the whole table up to row n*/,M=Mat(1))={ for(m=#M,n, M=matid(m+1)-matconcat([M^m;Vecrev(('x+m^2)^m)]); M=sum(i=1,m+1,-M^i/i)/m;M=sum(i=0,m+1,M^i/i!));if(k\u003e=0,M[n+1,k+1],M)}\\\\ Rewritten by _M. F. Hasler_, Nov 19 2017",
				"for(n=0,10, for(k=0,n, print1(T(n,k),\", \"));print(\"\"))",
				"A=T(29); c=-1; for(n=0,29, for(k=0,n, write(\"/tmp/b132870.txt\",c++,\" \",A[n+1,k+1]))) \\\\ _M. F. Hasler_, Nov 18 2017"
			],
			"xref": [
				"Cf. A132871 (row sums), columns: A132872, A132873, A132874.",
				"Cf. variants: A177390, A132875."
			],
			"keyword": "nonn,tabl,fini,full",
			"offset": "0,4",
			"author": "_Paul D. Hanna_, Sep 29 2007",
			"ext": [
				"Edits made for clarification by _Paul D. Hanna_, May 25 2010",
				"Further edits by _M. F. Hasler_, Nov 19 2017"
			],
			"references": 11,
			"revision": 51,
			"time": "2019-10-18T06:55:15-04:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}