{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A036036",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 36036,
			"data": "1,2,1,1,3,1,2,1,1,1,4,1,3,2,2,1,1,2,1,1,1,1,5,1,4,2,3,1,1,3,1,2,2,1,1,1,2,1,1,1,1,1,6,1,5,2,4,3,3,1,1,4,1,2,3,2,2,2,1,1,1,3,1,1,2,2,1,1,1,1,2,1,1,1,1,1,1,7,1,6,2,5,3,4,1,1,5,1,2,4,1,3,3,2,2,3,1,1,1",
			"name": "Triangle read by rows in which row n lists all the parts of all reversed partitions of n, sorted first by length and then lexicographically.",
			"comment": [
				"First differs from A334442 for reversed partitions of 9. Namely, this sequence has (1,4,4) before (2,2,5), while A334442 has (2,2,5) before (1,4,4). - _Gus Wiseman_, May 07 2020",
				"This is the \"Abramowitz and Stegun\" ordering of the partitions, referenced in numerous other sequences. The partitions are in reverse order of the conjugates of the partitions in Mathematica order (A080577). Each partition is the conjugate of the corresponding partition in Maple order (A080576). - _Franklin T. Adams-Watters_, Oct 18 2006",
				"The \"Abramowitz and Stegun\" ordering of the partitions is the graded reflected colexicographic ordering of the partitions. - _Daniel Forgues_, Jan 19 2011",
				"The \"Abramowitz and Stegun\" ordering of partitions has been traced back to C. F. Hindenburg, 1779, in the Knuth reference, p. 38. See the Hindenburg link, pp. 77-5 with the listing of the partitions for n=10.  This is also mentioned in the P. Luschny link. - _Wolfdieter Lang_, Apr 04 2011",
				"The \"Abramowitz and Stegun\" order used here means that the partitions of a given number are listed by increasing number of (nonzero) parts, then by increasing lexicographical order with parts in (weakly) indecreasing order. This differs from n=9 on from A334442 which considers reverse lexicographic order of parts in (weakly) decreasing order. - _M. F. Hasler_, Jul 12 2015, corrected thanks to _Gus Wiseman_, May 14 2020",
				"This is the Abramowitz-Stegun ordering of reversed partitions (finite weakly increasing sequences of positive integers). The same ordering of non-reversed partitions is A334301. - _Gus Wiseman_, May 07 2020"
			],
			"reference": [
				"Abramowitz and Stegun, Handbook, p. 831, column labeled \"pi\".",
				"D. Knuth, The Art of Computer Programming, Vol. 4, fascicle 3, 7.2.1.4,  Addison-Wesley, 2005."
			],
			"link": [
				"Franklin T. Adams-Watters, \u003ca href=\"/A036036/b036036.txt\"\u003eFirst 20 rows, flattened\u003c/a\u003e.",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy]. (uses Flash)",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e.",
				"C. F. Hindenburg, \u003ca href=\"http://books.google.com/books?id=oIM_AAAAcAAJ\"\u003eInfinitinomii Dignitatum Exponentis Indeterminati\u003c/a\u003e, Goettingen 1779.",
				"Peter Luschny, \u003ca href=\"http://www.luschny.de/math/seq/CountingWithPartitions.html\"\u003eCounting with partitions\u003c/a\u003e.",
				"OEIS Wiki, \u003ca href=\"http://oeis.org/wiki/Orderings of partitions#A comparison\"\u003eOrderings of partitions (a comparison)\u003c/a\u003e.",
				"Wikiversity, \u003ca href=\"https://en.wikiversity.org/wiki/Lexicographic_and_colexicographic_order\"\u003eLexicographic and colexicographic order\u003c/a\u003e"
			],
			"example": [
				"1",
				"2; 1,1",
				"3; 1,2; 1,1,1",
				"4; 1,3; 2,2; 1,1,2; 1,1,1,1",
				"5; 1,4; 2,3; 1,1,3; 1,2,2; 1,1,1,2; 1,1,1,1,1;",
				"6; 1,5; 2,4; 3,3; 1,1,4; 1,2,3; 2,2,2; 1,1,1,3; 1,1,2,2; 1,1,1,1,2; 1,1,1,1,1,1;",
				"..."
			],
			"mathematica": [
				"Join@@Table[Sort[Reverse/@IntegerPartitions[n]],{n,0,8}] (* _Gus Wiseman_, May 07 2020 *)",
				"- or -",
				"colen[f_,c_]:=OrderedQ[{Reverse[f],Reverse[c]}];",
				"Reverse/@Join@@Table[Sort[IntegerPartitions[n],colen],{n,0,8}] (* _Gus Wiseman_, May 07 2020 *)"
			],
			"program": [
				"(PARI) T036036(n,k)=k\u0026\u0026return(T036036(n)[k]);concat(partitions(n))",
				"\\\\ If 2nd arg \"k\" is not given, return the n-th row as a vector. Assumes PARI version \u003e= 2.7.1. See A193073 for \"hand made\" code.",
				"concat(vector(8,n,T036036(n))) \\\\ to get the \"flattened\" sequence",
				"\\\\ _M. F. Hasler_, Jul 12 2015"
			],
			"xref": [
				"Cf. A036037-A036040.",
				"See A036037 for the graded colexicographic ordering.",
				"See A080576 for the Maple (graded reflected lexicographic) ordering.",
				"See A080577 for the Mathematica (graded reverse lexicographic) ordering.",
				"See A193073 for the graded lexicographic ordering.",
				"See A228100 for the Fenner-Loizou (binary tree) ordering.",
				"The version ignoring length is A026791.",
				"Same as A036037 with partitions reversed.",
				"The lengths of these partitions are A036043.",
				"The number of distinct parts is A103921.",
				"The corresponding ordering of compositions is A124734.",
				"Showing partitions as Heinz numbers gives A185974.",
				"The version for non-reversed partitions is A334301.",
				"Lexicographically ordered reversed partitions are A026791.",
				"Sorting reversed partitions by Heinz number gives A112798.",
				"The version for revlex instead of lex is A334302.",
				"The version for revlex instead of colex is A334442.",
				"Cf. A000041, A211992, A228531, A296774, A334433, A334435, A334436, A334437, A334438, A334439, A334440, A334441."
			],
			"keyword": "nonn,easy,nice,tabf,look",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Edited by _Daniel Forgues_, Jan 21 2011",
				"Edited by _M. F. Hasler_, Jul 12 2015",
				"Name corrected by _Gus Wiseman_, May 12 2020"
			],
			"references": 108,
			"revision": 80,
			"time": "2020-05-16T01:52:13-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}