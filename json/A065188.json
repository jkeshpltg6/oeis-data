{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A065188",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 65188,
			"data": "1,3,5,2,4,9,11,13,15,6,8,19,7,22,10,25,27,29,31,12,14,35,37,39,41,16,18,45,17,48,20,51,53,21,56,58,60,23,63,24,66,28,26,70,72,74,76,78,30,32,82,84,86,33,89,34,92,38,36,96,98,100,102,40,105,107,42,110,43,113",
			"name": "\"Greedy Queens\" permutation of the positive integers.",
			"comment": [
				"This permutation is produced by a simple greedy algorithm: starting from the top left corner, walk along each successive antidiagonal of an infinite chessboard and place a queen in the first available position where it is not threatened by any of the existing queens. In other words, this permutation satisfies the condition that p(i+d) \u003c\u003e p(i)+-d for all i and d \u003e= 1.",
				"p(n) = k means that a queen appears in column n in row k. - _N. J. A. Sloane_, Aug 18 2016",
				"That this is a permutation follows from the proof in A269526 that every row and every column in that array is a permutation of the positive integers. In particular, every row and every column contains a 1 (which translates to a queen in the present sequence). - _N. J. A. Sloane_, Dec 10 2017",
				"The graph of this sequence shows two straight lines of respective slope equal to the Golden Ratio A001622, Phi = 1+phi = (sqrt(5)+1)/2 and phi = 1/Phi = (sqrt(5)-1)/2. - _M. F. Hasler_, Jan 13 2018",
				"One has a(42) = 28 and a(43) = 26. Such irregularities make it difficult to get an explicit formula. They would not occur if the squares on the antidiagonals had been checked for possible positions starting from the opposite end, so as to ensure that the subsequences corresponding to the points on either line would both be increasing. Then one would have that a(n-1) is either round(n*phi)+1 or round(n/phi)+1. (The +-1's could all be avoided if the origin were taken as a(0) = 0 instead of a(1) = 1.) Presently most values are such that either round(n*phi) or round(n/phi) does not differ by more than 1 from a(n-1)-1, except for very few exceptions of the above form (a(42) being the first of these). - _M. F. Hasler_, Jan 15 2018",
				"Equivalently, a(n) is the least positive integer not occurring earlier and so that |a(n)-a(k)| \u003c\u003e |n-k| for all k \u003c n; i.e., fill the first quadrant column by column with lowest possible peaceful queens. - _M. F. Hasler_, Jan 11 2022"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A065188/b065188.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"F. Michel Dekking, Jeffrey Shallit, and N. J. A. Sloane, \u003ca href=\"https://doi.org/10.37236/8905\"\u003eQueens in exile: non-attacking queens on infinite chess boards\u003c/a\u003e, Electronic J. Combin., 27:1 (2020), #P1.52.",
				"Matteo Fischetti and Domenico Salvagnin, \u003ca href=\"https://www.researchgate.net/profile/Matteo_Fischetti/publication/322508723_Chasing_First_Queens_by_Integer_Programming/links/5a5cf3620f7e9b4f78396d0b/Chasing-First-Queens-by-Integer-Programming.pdf\"\u003eChasing First Queens by Integer Programming\u003c/a\u003e, 2018.",
				"Matteo Fischetti and Domenico Salvagnin, \u003ca href=\"https://arxiv.org/abs/1907.08246\"\u003eFinding First and Most-Beautiful Queens by Integer Programming\u003c/a\u003e, arXiv:1907.08246 [cs.DS], 2019.",
				"N. J. A. Sloane, \u003ca href=\"/A065188/a065188.pdf\"\u003eScatterplot of first 100 terms\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/A065188/a065188.txt\"\u003eTable of n, a(n) for n = 1..50000\u003c/a\u003e [Obtained using the Maple program of _Alois P. Heinz_]",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"It would be nice to have a formula! - _N. J. A. Sloane_, Jun 30 2016",
				"a(n) = A275895(n-1)-1. - _M. F. Hasler_, Jan 11 2022"
			],
			"example": [
				"The top left corner of the board is:",
				"  +------------------------",
				"  | Q x x x x x x x x x ...",
				"  | x x x Q x x x x x x ...",
				"  | x Q x x x x x x x x ...",
				"  | x x x x Q x x x x x ...",
				"  | x x Q x x x x x x x ...",
				"  | x x x x x x x x x Q ...",
				"  | x x x x x x x x x x ...",
				"  | x x x x x x x x x x ...",
				"  | x x x x x Q x x x x ...",
				"  | ...",
				"which illustrates p(1)=1, p(2)=3, p(3)=5, p(4)=2, etc. - _N. J. A. Sloane_, Aug 18 2016, corrected Aug 21 2016"
			],
			"maple": [
				"SquareThreatened := proc(a,i,j,upto_n,senw,nesw) local k; for k from 1 to i do if a[k,j] \u003e 0 then RETURN(1); fi; od; for k from 1 to j do if a[i,k] \u003e 0 then RETURN(1); fi; od; if 1 = i and 1 = j then RETURN(0); fi; for k from 1 to `if`((-1 = senw),min(i,j)-1,senw) do if a[i-k,j-k] \u003e 0 then RETURN(1); fi; od; for k from 1 to `if`((-1 = nesw),i-1,nesw) do if a[i-k,j+k] \u003e 0 then RETURN(1); fi; od; for k from 1 to `if`((-1 = nesw),j-1,nesw) do if a[i+k,j-k] \u003e 0 then RETURN(1); fi; od; RETURN(0); end;",
				"GreedyNonThreateningPermutation := proc(upto_n,senw,nesw) local a,i,j; a := array(1..upto_n,1..upto_n); for i from 1 to upto_n do for j from 1 to upto_n do a[i,j] := 0; od; od; for j from 1 to upto_n do for i from 1 to j do if 0 = SquareThreatened(a,i,(j-i+1),upto_n,senw,nesw) then a[i,j-i+1] := 1; fi; od; od; RETURN(eval(a)); end;",
				"PM2PL := proc(a,upto_n) local b,i,j; b := []; for i from 1 to upto_n do for j from 1 to upto_n do if a[i,j] \u003e 0 then break; fi; od; b := [op(b),`if`((j \u003e upto_n),0,j)]; od; RETURN(b); end;",
				"GreedyQueens := upto_n -\u003e PM2PL(GreedyNonThreateningPermutation(upto_n,-1,-1),upto_n);GreedyQueens(256);",
				"# From _Alois P. Heinz_, Aug 19 2016: (Start)",
				"max_diagonal:= 3 * 100: # make this about 3*max number of terms",
				"h:= proc() true end:   # horizontal line free?",
				"v:= proc() true end:   # vertical   line free?",
				"u:= proc() true end:   # up     diagonal free?",
				"d:= proc() true end:   # down   diagonal free?",
				"a:= proc() 0 end:      # for A065188",
				"b:= proc() 0 end:      # for A065189",
				"for t from 2 to max_diagonal do",
				"   if u(t) then",
				"      for j to t-1 do",
				"        i:= t-j;",
				"        if v(j) and h(i) and d(i-j) then",
				"          v(j),h(i),d(i-j),u(i+j):= false$4;",
				"          a(j):= i;",
				"          b(i):= j;",
				"          break",
				"        fi",
				"      od",
				"   fi",
				"od:",
				"seq(a(n), n=1..100); # this is A065188",
				"seq(b(n), n=1..100); # this is A065189 # (End)"
			],
			"mathematica": [
				"Fold[Function[{a, n}, Append[a, 2 + LengthWhile[Differences@ Union@ Apply[Join, MapIndexed[Select[#2 + #1 {-1, 0, 1}, # \u003e 0 \u0026] \u0026 @@ {n - First@ #2, #1} \u0026, a]], # == 1 \u0026]]], {1}, Range[2, 70]] (* _Michael De Vlieger_, Jan 14 2018 *)"
			],
			"program": [
				"(PARI) A065188_first(N, a=List(), u=[0])={for(n=1,N, for(x=u[1]+1,oo, setsearch(u,x) \u0026\u0026 next; for(i=1,n-1, abs(x-a[i])==n-i \u0026\u0026 next(2)); u=setunion(u,[x]); while(#u\u003e1 \u0026\u0026 u[2]==u[1]+1, u=u[^1]); listput(a,x); break));a} \\\\ _M. F. Hasler_, Jan 11 2022"
			],
			"xref": [
				"A065185 gives the associated p(i)-i delta sequence. A065186 gives the corresponding permutation for \"promoted rooks\" used in Shogi, A065257 gives \"Quintal Queens\" permutation.",
				"A065189 gives inverse permutation.",
				"See A199134, A275884, A275890, A275891, A275892 for information about the split of points below and above the diagonal.",
				"Cf. A269526.",
				"If we subtract 1 and change the offset to 0 we get A275895, A275896, A275893, A275894.",
				"Tracking at which squares along the successive antidiagonals the queens appear gives A275897 and A275898.",
				"Antidiagonal and diagonal indices give A276324 and A276325."
			],
			"keyword": "nonn,changed",
			"offset": "1,2",
			"author": "_Antti Karttunen_, Oct 19 2001",
			"references": 29,
			"revision": 92,
			"time": "2022-01-11T12:35:20-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}