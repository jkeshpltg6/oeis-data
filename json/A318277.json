{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A318277",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 318277,
			"data": "1,1,1,1,1,1,1,2,0,1,1,1,1,0,1,1,2,1,1,0,1,1,1,1,0,1,0,1,1,2,1,1,1,1,0,1,1,3,0,3,0,0,0,0,1,1,1,1,0,1,0,1,0,0,1,1,2,2,1,0,2,0,0,0,0,1,1,2,1,1,1,1,1,1,0,0,0,1,1,3,1,3,0,2,0,0,1,0,0,0,1,1,1,1,0,1,0,1",
			"name": "Triangle read by rows; T(n, k) is the number of divisors of A025487(n) having the same prime signature as A025487(k) where 1 \u003c= k \u003c= n.",
			"comment": [
				"If A025487(k) doesn't divide A025487(n) then T(n, k) = 0.",
				"Adapted from _Clark Kimberling_ at A074206: \"With different offset: A074206(A025487(n)) = sum of all A074206(A025487(k)) such that A025487(k) divides A025487(n) and A025487(k) \u003c A025487(n).\"",
				"By looking at the number of divisors of A025487(n) that have the same prime signature as A025487(n) can help in computing A074206, especially if A025487(n) has a lot of divisors."
			],
			"link": [
				"David A. Corneth, \u003ca href=\"/A318277/b318277.txt\"\u003eTable of n, a(n) for n = 1..10011\u003c/a\u003e (first 141 rows, flattened)"
			],
			"formula": [
				"Row sums are A000005(A025487(n))."
			],
			"example": [
				"A025487(9) = 30 and A025487(4) = 6 and have prime signatures (1, 1, 1) and (1, 1) respectively. There are three divisors of 30 with the prime signature (1, 1), being 6, 10 and 15. Therefore, T(9, 4) = 3.",
				"Triangle with rows n and columns k starts:",
				"1,",
				"1, 1,",
				"1, 1, 1,",
				"1, 2, 0, 1,",
				"1, 1, 1, 0, 1,",
				"1, 2, 1, 1, 0, 1,",
				"1, 1, 1, 0, 1, 0, 1,",
				"1, 2, 1, 1, 1, 1, 0, 1,",
				"1, 3, 0, 3, 0, 0, 0, 0, 1,",
				"1, 1, 1, 0, 1, 0, 1, 0, 0, 1,",
				"1, 2, 2, 1, 0, 2, 0, 0, 0, 0, 1,",
				"1, 2, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1,",
				"1, 3, 1, 3, 0, 2, 0, 0, 1, 0, 0, 0, 1,"
			],
			"mathematica": [
				"f[n_] := Block[{lim, ww}, Set[{lim, ww}, {Product[Prime@ i, {i, n}], NestList[Append[#, 1] \u0026, {1}, n - 1]} ]; {{{0}}}~Join~Map[Block[{w = #, k = 1}, Sort@ Apply[Join, {{ConstantArray[1, Length@ w]}, If[Length@ # == 0, #, #[[1]]] }] \u0026@ Reap[Do[If[# \u003c= lim, Sow[w]; k = 1, If[k \u003e= Length@ w, Break[], k++]] \u0026@ Apply[Times, MapIndexed[Prime[First@ #2]^#1 \u0026, #]] \u0026@ Set[w, If[k == 1, MapAt[# + 1 \u0026, w, k], PadLeft[#, Length@ w, First@ #] \u0026@ Drop[MapAt[# + Boole[i \u003e 1] \u0026, w, k], k - 1] ]], {i, Infinity}]][[-1]]] \u0026, ww]]; With[{s = Sort@ Map[{Times @@ Flatten@ MapIndexed[Prime[#2]^#1 \u0026, #], #} \u0026, Join @@ f@ 4]}, Table[DivisorSum[s[[n, 1]], 1 \u0026, If[Length@ # == 1, #, TakeWhile[#, # \u003e 0 \u0026]] \u0026@ Sort[If[# == 1, {0}, Function[f, ReplacePart[Table[0, {PrimePi[f[[-1, 1]]]}], #] \u0026@ Map[PrimePi@ First@ # -\u003e Last@ # \u0026, f]]@ FactorInteger@ #] \u0026@ #, Greater] == s[[k, -1]] \u0026], {n, Length@ s}, {k, n}]] // Flatten (* _Michael De Vlieger_, Oct 10 2018 *)"
			],
			"program": [
				"(PARI) ps(y) = factor(y)[,2];",
				"tabl(nn) = {v = al(nn); for (n=1, nn, d = divisors(v[n]); for (k=1, n, f = ps(v[k]); nb = #select(x-\u003e(ps(x) == f), d); print1(nb, \", \");); print;);} \\\\ _Michel Marcus_, Oct 11 2018; where al(n) is defined in A025487"
			],
			"xref": [
				"Cf. A025487, A074206."
			],
			"keyword": "nonn,tabl",
			"offset": "1,8",
			"author": "_David A. Corneth_, Aug 24 2018",
			"references": 2,
			"revision": 31,
			"time": "2018-12-31T06:10:11-05:00",
			"created": "2018-10-26T10:24:27-04:00"
		}
	]
}