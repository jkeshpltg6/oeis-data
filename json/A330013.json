{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A330013",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 330013,
			"data": "3,3,0,3,3,0,3,6,6,3,3,0,3,3,0,6,3,6,3,6,0,3,3,0,3,3,9,12,3,0,3,6,0,3,9,6,3,3,0,6,3,0,3,6,6,3,3,0,9,3,0,6,3,12,3,12,0,3,3,0,3,3,6,9,9,0,3,6,0,9,3,12,3,3,0,6,9,0,3,6,12,3,3,0,3",
			"name": "a(n) is the number of solutions with nonnegative (x,y,z) to the cubic Diophantine equation x^3+y^3+z^3 - 3*x*y*z = n.",
			"comment": [
				"Some results coming from the Alarcon and Duval reference.",
				"For n = 0, there are infinitely many solutions because every triple (k,k,k) with k \u003e= 0 satisfies the equation.",
				"a(n) = 0 iff 3 divides n and 9 doesn't divide n (equivalent to n is in A016051).",
				"When n belongs to A074232 (complement of A016051), a(n) is always a multiple of 3 because",
				"1) if (a,a,b) [resp. (a,b,b)] with a \u003c b is a primitive solution, then these triples generate 3 solutions with the permutations (a,a,b), (a,b,a), (b,a,a), [resp. (a,b,b), (b,b,a), (b,a,b)] and,",
				"2) if (a,b,c) with a \u003c b \u003c c is a primitive solution, then this triple generates 6 solutions with the permutations (a,b,c), (b,c,a), (c,a,b), (a,c,b), (c,b,a), (b,a,c).",
				"For prime p \u003c\u003e 3, a(p) = a(2*p) = 3.",
				"An inequality: (n/4)^(1/3) \u003c= max(x, y, z) \u003c= (n+2)/3.",
				"This sequence is unbounded.",
				"A261029 gives the number of triples without counting the permutations and, in link, a list of primitive triples up to n = 2000."
			],
			"reference": [
				"Guy Alarcon and Yves Duval, TS: Préparation au Concours Général, RMS, Collection Excellence, Paris, 2010, chapitre 9, Problème: étude d'une équation diophantienne cubique, pages 137-138 and 147-152."
			],
			"link": [
				"Vladimir Shevelev, \u003ca href=\"https://arxiv.org/abs/1508.05748\"\u003eRepresentation of positive integers by the form x^3+y^3+z^3-3xyz\u003c/a\u003e, arXiv:1508.05748 [math.NT], 2015."
			],
			"formula": [
				"If n = 3*k + 1, then (k, k, k+1) is a solution for k \u003e= 0.",
				"If n = 3*k - 1, then (k, k, k-1) is a solution for k \u003e= 1.",
				"If n = 9*k, then (k-1, k, k+1) is a solution for k \u003e= 1.",
				"If n = k^3, then (k, 0, 0) is a solution for k \u003e= 0.",
				"If n = 2*k^3, then (k, k, 0) is a solution for k \u003e= 0."
			],
			"example": [
				"3^3+2^3+2^3-3*2*2*3 = 7 so (3,2,2), (2,2,3) and (2,3,2) are solutions and a(7) = 3.",
				"When n=35, (0,1,3) is a primitive solution that generates 6 solutions and (9,9,10) is another primitive solution that generates 3 solutions, so a(35)=6+3=9 (see comments)."
			],
			"mathematica": [
				"a[n_] := Length@ Solve[x^3 + y^3 + z^3 - 3 x y z == n \u0026\u0026 x \u003e= 0 \u0026\u0026 y \u003e= 0 \u0026\u0026 z \u003e= 0, {x, y, z}, Integers]; Array[a, 85] (* _Giovanni Resta_, Nov 28 2019 *)"
			],
			"xref": [
				"Cf. A016051, A074232.",
				"Cf. A261029 (primitive triples without the permutations).",
				"Cf. A050787, A050791, A212420 (other cubic Diophantine equations)."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Bernard Schott_, Nov 27 2019",
			"ext": [
				"More terms from _Giovanni Resta_, Nov 28 2019"
			],
			"references": 1,
			"revision": 43,
			"time": "2020-01-09T05:47:29-05:00",
			"created": "2020-01-09T05:47:29-05:00"
		}
	]
}