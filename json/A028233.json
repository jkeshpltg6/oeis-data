{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A028233",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 28233,
			"data": "1,2,3,4,5,2,7,8,9,2,11,4,13,2,3,16,17,2,19,4,3,2,23,8,25,2,27,4,29,2,31,32,3,2,5,4,37,2,3,8,41,2,43,4,9,2,47,16,49,2,3,4,53,2,5,8,3,2,59,4,61,2,9,64,5,2,67,4,3,2,71,8,73,2,3,4,7,2,79,16,81,2,83,4,5,2",
			"name": "If n = p_1^e_1 * ... * p_k^e_k, p_1 \u003c ... \u003c p_k primes, then a(n) = p_1^e_1, with a(1) = 1.",
			"comment": [
				"Highest power of smallest prime dividing n. - _Reinhard Zumkeller_, Apr 09 2015"
			],
			"link": [
				"T. D. Noe and Reinhard Zumkeller, \u003ca href=\"/A028233/b028233.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e, first 1000 terms from T. D. Noe"
			],
			"formula": [
				"a(n) = A020639(n)^A067029(n). - _Reinhard Zumkeller_, May 13 2006",
				"a(n) = A141809(n,1). - _Reinhard Zumkeller_, Jun 04 2012",
				"a(n) = n / A028234(n). - _Antti Karttunen_, May 29 2017"
			],
			"example": [
				"From _Muniru A Asiru_, Jan 27 2018: (Start)",
				"If n=10, then a(10) = 2 since 10 = 2^1*5^1.",
				"If n=16, then a(16) = 16 since 16 = 2^4.",
				"If n=29, then a(29) = 29 since 29 = 29^1.",
				"(End)"
			],
			"maple": [
				"A028233 := proc(n)",
				"    local spf,pf;",
				"    if n = 1 then",
				"        return 1 ;",
				"    end if;",
				"    spf := A020639(n) ;",
				"    for pf in ifactors(n)[2] do",
				"        if pf[1] = spf then",
				"            return pf[1]^pf[2] ;",
				"        end if;",
				"    end do:",
				"end proc: # _R. J. Mathar_, Jul 09 2016",
				"# second Maple program:",
				"a:= n-\u003e `if`(n=1, 1, (i-\u003ei[1]^i[2])(sort(ifactors(n)[2])[1])):",
				"seq(a(n), n=1..100);  # _Alois P. Heinz_, Jan 29 2018"
			],
			"mathematica": [
				"a[n_] := Power @@ First[ FactorInteger[n]]; Table[a[n], {n, 1, 86}] (* _Jean-François Alcover_, Dec 01 2011 *)"
			],
			"program": [
				"(Haskell)",
				"a028233 = head . a141809_row",
				"-- _Reinhard Zumkeller_, Jun 04 2012, Aug 17 2011",
				"(PARI) a(n)=if(n\u003e1,n=factor(n);n[1,1]^n[1,2],1) \\\\ _Charles R Greathouse IV_, Apr 26 2012",
				"(Python)",
				"from sympy import factorint",
				"def a(n):",
				"    f = factorint(n)",
				"    return 1 if n==1 else min(f)**f[min(f)] # _Indranil Ghosh_, May 12 2017",
				"(Scheme)",
				";; Naive implementation of A020639 is given under that entry. All of these functions could be also defined with definec to make them faster on the later calls. See http://oeis.org/wiki/Memoization#Scheme",
				"(define (A028233 n) (if (\u003c n 2) n (let ((lpf (A020639 n))) (let loop ((m lpf) (n (/ n lpf))) (cond ((not (zero? (modulo n lpf))) m) (else (loop (* m lpf) (/ n lpf)))))))) ;; _Antti Karttunen_, May 29 2017",
				"(GAP) List(List(List(List([1..10^3], Factors), Collected), i -\u003e i[1]), j -\u003e j[1]^j[2]); # _Muniru A Asiru_, Jan 27 2018"
			],
			"xref": [
				"Cf. A020639, A006530, A034684, A034699, A053585.",
				"See also A028234.",
				"Cf. A008475.",
				"Cf. A141809."
			],
			"keyword": "nonn,nice,easy",
			"offset": "1,2",
			"author": "_Marc LeBrun_",
			"ext": [
				"Edited name to include a(1) = 1 by _Franklin T. Adams-Watters_, Jan 27 2018"
			],
			"references": 26,
			"revision": 52,
			"time": "2018-01-29T09:36:25-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}