{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A179080",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 179080,
			"data": "1,1,1,2,1,3,2,4,2,6,4,7,5,9,8,12,10,14,15,17,19,22,26,26,32,32,42,40,52,48,66,59,79,73,98,89,118,108,143,133,170,160,204,194,241,236,286,283,336,339,396,407,464,483,544,575,634,681,740,803,862,944,1001,1110,1162,1296,1348,1512,1561,1760,1805",
			"name": "Number of partitions of n into distinct parts where all differences between consecutive parts are odd.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A179080/b179080.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e"
			],
			"formula": [
				"G.f.: sum(n\u003e=0, x^(n*(n+1)/2) / prod(k=1..n+1, 1-x^(2*k) ) ). - _Joerg Arndt_, Jan 29 2011",
				"a(n) = A179049(n) + A218355(n). - _Joerg Arndt_, Oct 27 2012"
			],
			"example": [
				"From _Joerg Arndt_, Oct 27 2012:  (Start)",
				"The a(18) = 15 such partitions of 18 are:",
				"[ 1]  1 2 3 12",
				"[ 2]  1 2 5 10",
				"[ 3]  1 2 7 8",
				"[ 4]  1 2 15",
				"[ 5]  1 4 5 8",
				"[ 6]  1 4 13",
				"[ 7]  1 6 11",
				"[ 8]  1 8 9",
				"[ 9]  2 3 4 9",
				"[10]  2 3 6 7",
				"[11]  3 4 5 6",
				"[12]  3 4 11",
				"[13]  3 6 9",
				"[14]  5 6 7",
				"[15]  18",
				"(End)"
			],
			"maple": [
				"b:= proc(n, i) option remember; `if`(n=0, 1,",
				"      `if`(i\u003en, 0, b(n, i+2)+b(n-i, i+1)))",
				"    end:",
				"a:= n-\u003e `if`(n=0, 1, b(n, 1)+b(n, 2)):",
				"seq(a(n), n=0..100);  # _Alois P. Heinz_, Nov 08 2012; revised Feb 24 2020"
			],
			"mathematica": [
				"b[n_, i_, t_] := b[n, i, t] = If[n==0, 1, If[i\u003c1, 0, b[n, i-1, t] + If[i \u003c= n \u0026\u0026 Mod[i, 2] != t, b[n-i, i-1, Mod[i, 2]], 0]]]; a[n_] := If[n==0, 1, Sum[b[n-i, i-1, Mod[i, 2]], {i, 1, n}]]; Table[a[n], {n, 0, 100}] (* _Jean-François Alcover_, Mar 24 2015, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(Sage)",
				"def A179080(n):",
				"    odd_diffs = lambda x: all(abs(d) % 2 == 1 for d in differences(x))",
				"    satisfies = lambda p: not p or odd_diffs(p)",
				"    def count(pred, iter): return sum(1 for item in iter if pred(item))",
				"    return count(satisfies, Partitions(n, max_slope=-1))",
				"print([A179080(n) for n in range(0, 20)]) # show first terms",
				"(Sage) # Alternative after _Alois P. Heinz_:",
				"def A179080(n):",
				"    @cached_function",
				"    def h(n, k):",
				"        if n == 0: return 1",
				"        if k  \u003e n: return 0",
				"        return h(n, k+2) + h(n-k, k+1)",
				"    return h(n, 1) + h(n, 2) if n \u003e 0 else 1",
				"print([A179080(n) for n in range(71)]) # _Peter Luschny_, Feb 25 2020",
				"(PARI) N=66; x='x+O('x^N); gf = sum(n=0,N, x^(n*(n+1)/2) / prod(k=1,n+1, 1-x^(2*k) ) ); Vec( gf ) /* _Joerg Arndt_, Jan 29 2011 */"
			],
			"xref": [
				"Cf. A179049 (odd differences and odd minimal part).",
				"Cf. A189357 (even differences, distinct parts), A096441 (even differences).",
				"Cf. A000009 (partitions of 2*n with even differences and even minimal part)."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_Joerg Arndt_, Jan 04 2011",
			"references": 3,
			"revision": 48,
			"time": "2020-03-06T03:59:55-05:00",
			"created": "2010-11-12T14:28:32-05:00"
		}
	]
}