{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A215669",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 215669,
			"data": "0,4,0,18,32,0,42,44,48,24,42,12,40,8,50,368,16,100,410,118,0,12,442,584,546,1104,482,148,2786,536,398",
			"name": "Number of decimal digits of the smallest solution for the reverse-and-subtract problem for cycle length n.",
			"comment": [
				"Solution x for a given cycle length n for the reverse-and-subtract problem is defined as x = f^n(x), x \u003c\u003e f^j(x) for j \u003c n, where f: k -\u003e |k - reverse(k)|. For some cycle lengths (at least for 1, 3, 6 and 21) no solutions exist, these are marked as 0 in above sequence.",
				"Zero cannot be considered a solution for cycle length 1 as there are nontrivial solutions for other numeral systems, such as 13 (one-three) in base 5 numeral system.",
				"This is an excerpt which shows the smallest solutions with up to 50 digits only:",
				".n..#digits.....................................smallest.solution......ref",
				".2........4..................................................2178..A072141",
				".4.......18....................................169140971830859028..A292634",
				".5.......32......................10591266563195008940873343680499..A292635",
				".7.......42............142710354353443018141857289645646556981858..A292856",
				".8.......44..........16914079504181797053273763831171860502859028..A292857",
				".9.......48......111603518721165960373027269626940447783074704878..A292858",
				"10.......24..............................101451293600894707746789..A292859",
				"11.......42............166425621223026859056339052269787863565428..A292846",
				"12.......12..........................................118722683079..A072718",
				"13.......40..............1195005230033599502088049947699664004979..A292992",
				"14........8..............................................11436678..A072142",
				"15.......50....10695314508256806604321090888649339244708568530399..A292993",
				"17.......16......................................1186781188132188..A072719",
				"22.......12..........................................108811891188..A072143",
				"Solutions for all cycle lengths up to 31 can be found below in the links section. Remember that a zero means there exists no solution for this specific cycle length.",
				"There are two ways to find such solutions, first you can search in a given range of numbers e.g. from 10000000 to 99999999 and apply reverse-and-subtract to each number until you fall below the smallest number in this range (here: 10000000) or you find a cycle. Obviously, this works well only on small numbers up to 18-20 digits.",
				"The second way is to construct a cycle with a given length n from the outside in until the innermost 2 digits of each number match the conditions for a valid cycle. This way it is possible to get the above results within seconds up to some hours depending on the specific cycle length even on an outdated PC."
			],
			"link": [
				"J. H. E. Cohn, \u003ca href=\"http://www.fq.math.ca/Scanned/28-2/cohn.pdf\"\u003ePalindromic Differences\u003c/a\u003e, Fibonacci Quart. (1990):113-120.",
				"Thorsten Ehlers, \u003ca href=\"/A215669/a215669.txt\"\u003esmallest solutions for the reverse-and-subtract problem up to cycle length 31\u003c/a\u003e"
			],
			"example": [
				"a(4) = 169140971830859028 as the smallest cycle with length 4 is 169140971830859028 -\u003e 651817066348182933 -\u003e 312535222687464777 -\u003e 464929563535070436 ( -\u003e 169140971830859028 )."
			],
			"xref": [
				"Cf. A072141, A072142, A072143, A072718, A072719, A292634, A292635, A292846, A292856, A292857, A292858, A292859, A292992, A292993."
			],
			"keyword": "base,nonn,more",
			"offset": "1,2",
			"author": "_Thorsten Ehlers_, Aug 20 2012",
			"ext": [
				"Added a reference, formatted and added one more example in comments. - _Thorsten Ehlers_, Oct 06 2012",
				"Sequences added to comments and crossrefs by _Ray Chandler_, Sep 27 2017"
			],
			"references": 10,
			"revision": 38,
			"time": "2017-09-28T15:48:38-04:00",
			"created": "2012-08-25T00:03:41-04:00"
		}
	]
}