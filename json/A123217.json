{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A123217",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 123217,
			"data": "1,1,1,1,-1,1,2,3,-5,1,3,20,-32,9,1,4,58,-82,5,15,1,5,125,-108,-161,170,-31,1,6,229,17,-797,603,7,-65,1,7,378,532,-2210,664,1468,-968,129,1,8,580,1820,-4226,-2846,8788,-4388,9,255,1,9,843,4440,-5262",
			"name": "Triangle read by rows: the n-th row consists of the coefficients in the expansion of Sum_{j=0..n} A123162(n,j)*x^j*(1 - x)^(n - j).",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A123217/b123217.txt\"\u003eRows n = 0..50 of the irregular triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"From _Franck Maminirina Ramaharo_, Oct 10 2018: (Start)",
				"Row n = coefficients in the expansion of (1-x)^n + x*((1 - 2*sqrt((1-x)*x))^n*(1 - x + sqrt((1-x)*x)) - (1-x - sqrt((1-x)*x))*(1 + 2*sqrt((1-x)*x))^n)/(2*sqrt((1 - x)*x)*(2*x-1)).",
				"G.f.: (1 - (2 - x)*y + (1 - 4*x + 3*x^2)*y^2 - (x - 3*x^2 + 2*x^3)*y^3)/(1 - (3 - x)*y + (3 - 6*x + 4*x^2)*y^2 - (1 - 5*x + 8*x^2 - 4*x^3)*y^3).",
				"E.g.f.: exp((1 - x)*y) + x*((1 - x + sqrt((1 - x)*x))*exp((1 - 2*sqrt((1 - x)*x))*y) - (1 - x - sqrt((1 - x)*x))*exp((1 + 2*sqrt((1 - x)*x))*y))/(2*(2*x - 1)*sqrt((1 - x)*x)) - (1 - 3*x)/(1 - 2*x) + 1. (End)"
			],
			"example": [
				"Triangle begins:",
				"  1;",
				"  1;",
				"  1, 1,  -1;",
				"  1, 2,   3,   -5;",
				"  1, 3,  20,  -32,     9;",
				"  1, 4,  58,  -82,     5,    15;",
				"  1, 6, 229,   17,  -797,   603,    7,   -65;",
				"  1, 7, 378,  532, -2210,   664, 1468,  -968, 129;",
				"  1, 8, 580, 1820, -4226, -2846, 8788, -4388,   9, 255;",
				"  ... reformatted and extended. - _Franck Maminirina Ramaharo_, Oct 10 2018"
			],
			"mathematica": [
				"t[n_, k_]= If[k==0, 1, Binomial[2*n-1, 2*k-1]];",
				"p[n_,x_]:= p[n,x]= Sum[t[n,j]*x^j*(1-x)^(n-j), {j,0,n}];",
				"Table[CoefficientList[p[n,x], x], {n, 0, 10}]//Flatten"
			],
			"program": [
				"(Maxima) A123162(n, k) := if n = 0 and k = 0 or k = 0 then 1 else binomial(2*n - 1, 2*k - 1)$",
				"P(x, n) := expand(sum(A123162(n, j)*x^j*(1 - x)^(n - j), j, 0, n))$",
				"T(n, k) := ratcoef(P(x, n), x, k)$",
				"tabf(nn) := for n:0 thru nn do print(makelist(T(n, k), k, 0, hipow(P(x, n), x)))$ /* _Franck Maminirina Ramaharo_, Oct 10 2018 */",
				"(Sage)",
				"def b(n,k): return 1 if (k==0) else binomial(2*n-1, 2*k-1)",
				"def p(n,x): return sum( b(n,j)*x^j*(1-x)^(n-j) for j in (0..n) )",
				"def T(n): return ( p(n,x) ).full_simplify().coefficients(sparse=False)",
				"[T(n) for n in (0..12)] # _G. C. Greubel_, Jul 15 2021"
			],
			"xref": [
				"Cf. A122753, A123018, A123019, A123021, A123027, A123199, A123202, A123221."
			],
			"keyword": "tabf,sign",
			"offset": "0,7",
			"author": "_Roger L. Bagula_, Oct 04 2006",
			"ext": [
				"Edited, new name, and offset corrected by _Franck Maminirina Ramaharo_, Oct 11 2018"
			],
			"references": 13,
			"revision": 19,
			"time": "2021-07-15T04:37:49-04:00",
			"created": "2006-10-09T03:00:00-04:00"
		}
	]
}