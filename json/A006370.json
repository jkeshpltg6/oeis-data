{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006370",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6370,
			"id": "M3198",
			"data": "0,4,1,10,2,16,3,22,4,28,5,34,6,40,7,46,8,52,9,58,10,64,11,70,12,76,13,82,14,88,15,94,16,100,17,106,18,112,19,118,20,124,21,130,22,136,23,142,24,148,25,154,26,160,27,166,28,172,29,178,30,184,31,190,32,196,33",
			"name": "The Collatz or 3x+1 map: a(n) = n/2 if n is even, 3n + 1 if n is odd.",
			"comment": [
				"The 3x+1 or Collatz problem is as follows: start with any number n. If n is even, divide it by 2, otherwise multiply it by 3 and add 1. Do we always reach 1? This is an unsolved problem. It is conjectured that the answer is yes.",
				"The Krasikov-Lagarias paper shows that at least N^0.84 of the positive numbers \u003c N fall into the 4-2-1 cycle of the 3x+1 problem. This is far short of what we think is true, that all positive numbers fall into this cycle, but it is a step. - Richard C. Schroeppel, May 01 2002",
				"Also A001477 and A016957 interleaved. - _Omar E. Pol_, Jan 16 2014, updated Nov 07 2017",
				"a(n) is the image of a(2*n) under the 3*x+1 map. - _L. Edson Jeffery_, Aug 17 2014",
				"The positions of powers of 2 in this sequence are given in A160967. - _Federico Provvedi_, Oct 06 2021",
				"If displayed as a rectangular array with six columns, the columns are A008585, A350521, A016777, A082286, A016789, A350522 (see example). - _Omar E. Pol_, Jan 03 2022"
			],
			"reference": [
				"R. K. Guy, Unsolved Problems in Number Theory, E16.",
				"J. C. Lagarias, ed., The Ultimate Challenge: The 3x+1 Problem, Amer. Math. Soc., 2010.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A006370/b006370.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Darrell Cox, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL15/Cox/cox10.html\"\u003eThe 3n + 1 Problem: A Probabilistic Approach\u003c/a\u003e, Journal of Integer Sequences, Vol. 15 (2012), #12.5.2.",
				"David Eisenbud and Brady Haran, \u003ca href=\"https://www.youtube.com/watch?v=5mFpVDpKX70\"\u003eUNCRACKABLE? The Collatz Conjecture\u003c/a\u003e, Numberphile Video, 2016.",
				"I. Krasikov and J. C. Lagarias, \u003ca href=\"https://arxiv.org/abs/math/0205002\"\u003eBounds for the 3x+1 Problem using Difference Inequalities\u003c/a\u003e, arXiv:math/0205002 [math.NT], 2002.",
				"J. C. Lagarias, \u003ca href=\"http://www.cecm.sfu.ca/organics/papers/lagarias/paper/html/paper.html\"\u003eThe 3x+1 problem and its generalizations\u003c/a\u003e, Amer. Math. Monthly, 92 (1985), 3-23.",
				"J. C. Lagarias, \u003ca href=\"http://matwbn.icm.edu.pl/ksiazki/aa/aa56/aa5614.pdf\"\u003eThe set of rational cycles for the 3x+1 problem\u003c/a\u003e, Acta Arithmetica, LVI (1990), pp. 33-53.",
				"J. C. Lagarias, \u003ca href=\"https://arxiv.org/abs/math/0309224\"\u003eThe 3x+1 Problem: An Annotated Bibliography (1963-2000)\u003c/a\u003e, arXiv:math/0309224 [math.NT], 2003-2011.",
				"J. C. Lagarias, \u003ca href=\"https://arxiv.org/abs/math/0608208\"\u003eThe 3x+1 Problem: an annotated bibliography, II (2000-2009)\u003c/a\u003e, arXiv:math/0608208 [math.NT], 2006-2012.",
				"Jeffrey C. Lagarias, \u003ca href=\"https://arxiv.org/abs/2111.02635\"\u003eThe 3x+1 Problem: An Overview\u003c/a\u003e, arXiv:2111.02635 [math.NT], 2021.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992; arXiv:0911.4975 [math.NT], 2009.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"E. Roosendaal, \u003ca href=\"http://www.ericr.nl/wondrous/index.html\"\u003eOn the 3x+1 problem\u003c/a\u003e",
				"S. Schreiber \u0026 N. J. A. Sloane, \u003ca href=\"/A006368/a006368.pdf\"\u003eCorrespondence, 1980\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CollatzProblem.html\"\u003eCollatz Problem\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Collatz_conjecture\"\u003eCollatz conjecture\u003c/a\u003e",
				"\u003ca href=\"/index/3#3x1\"\u003eIndex entries for sequences related to 3x+1 (or Collatz) problem\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,2,0,-1)."
			],
			"formula": [
				"G.f.: (4*x+x^2+2*x^3) / (1-x^2)^2.",
				"a(n) = (1/4)*(7*n+2-(-1)^n*(5*n+2)). - _Benoit Cloitre_, May 12 2002",
				"a(n) = ((n mod 2)*2 + 1)*n/(2 - (n mod 2)) + (n mod 2). - _Reinhard Zumkeller_, Sep 12 2002",
				"a(n) = A014682(n+1) * A000034(n). - _R. J. Mathar_, Mar 09 2009",
				"a(n) = a(a(2*n)) = -A001281(-n) for all n in Z. - _Michael Somos_, Nov 10 2016",
				"E.g.f.: (2 + x)*sinh(x)/2 + 3*x*cosh(x). - _Ilya Gutkovskiy_, Dec 20 2016",
				"From _Federico Provvedi_, Aug 17 2021: (Start)",
				"Dirichlet g.f.: (1-2^(-s))*zeta(s) + (3-5*2^(-s))*zeta(s-1).",
				"a(n) = ( a(n+2k) + a(n-2k) ) / 2, for every integer k. (End)"
			],
			"example": [
				"G.f. = 4*x + x^2 + 10*x^3 + 2*x^4 + 16*x^5 + 3*x^6 + 22*x^7 + 4*x^8 + 28*x^9 + ...",
				"From _Omar E. Pol_, Jan 03 2022: (Start)",
				"Written as a rectangular array with six columns read by rows the sequence begins:",
				"   0,   4,  1,  10,  2,  16;",
				"   3,  22,  4,  28,  5,  34;",
				"   6,  40,  7,  46,  8,  52;",
				"   9,  58, 10,  64, 11,  70;",
				"  12,  76, 13,  82, 14,  88;",
				"  15,  94, 16, 100, 17, 106;",
				"  18, 112, 19, 118, 20, 124;",
				"  21, 130, 22, 136, 23, 142;",
				"  24, 148, 25, 154, 26, 160;",
				"  27, 166, 28, 172, 29, 178;",
				"  30, 184, 31, 190, 32, 196;",
				"...",
				"(End)"
			],
			"maple": [
				"f := n-\u003e if n mod 2 = 0 then n/2 else 3*n+1; fi;",
				"A006370:=(4+z+2*z**2)/(z-1)**2/(1+z)**2; # _Simon Plouffe_ in his 1992 dissertation; uses offset 0"
			],
			"mathematica": [
				"f[n_]:=If[EvenQ[n],n/2,3n+1];Table[f[n],{n,50}] (* _Geoffrey Critzer_, Jun 29 2013 *)",
				"LinearRecurrence[{0,2,0,-1},{4,1,10,2},70] (* _Harvey P. Dale_, Jul 19 2016 *)"
			],
			"program": [
				"(PARI) for(n=1,100,print1((1/4)*(7*n+2-(-1)^n*(5*n+2)),\",\"))",
				"(PARI) A006370(n)=if(n%2,3*n+1,n/2) \\\\ _Michael B. Porter_, May 29 2010",
				"(Haskell)",
				"a006370 n | m /= 0    = 3 * n + 1",
				"          | otherwise = n' where (n',m) = divMod n 2",
				"-- _Reinhard Zumkeller_, Oct 07 2011",
				"(Python)",
				"def A006370(n):",
				"    q, r = divmod(n, 2)",
				"    return 3*n+1 if r else q # _Chai Wah Wu_, Jan 04 2015",
				"(MAGMA) [(1/4)*(7*n+2-(-1)^n*(5*n+2)): n in [1..70]]; // _Vincenzo Librandi_, Dec 20 2016"
			],
			"xref": [
				"Cf. A139391, A016945, A005408, A016825, A082286, A070165.",
				"A006577 gives number of steps to reach 1.",
				"Cf. A001281.",
				"Column k=1 of A347270, n \u003e= 1."
			],
			"keyword": "nonn,nice,easy,changed",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from Larry Reeves (larryr(AT)acm.org), Apr 27 2001",
				"Zero prepended and new Name from _N. J. A. Sloane_ at suggestion of _M. F. Hasler_, Nov 06 2017"
			],
			"references": 184,
			"revision": 218,
			"time": "2022-01-03T17:14:56-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}