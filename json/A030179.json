{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A030179",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 30179,
			"data": "0,0,1,4,16,36,81,144,256,400,625,900,1296,1764,2401,3136,4096,5184,6561,8100,10000,12100,14641,17424,20736,24336,28561,33124,38416,44100,50625,57600,65536,73984,83521,93636,104976,116964",
			"name": "Quarter-squares squared: A002620^2.",
			"comment": [
				"Conjectured to be crossing number of complete bipartite graph K_{n,n}. Known to be true for n \u003c= 7.",
				"If the Zarankiewicz conjecture is true, then a(n) is also the rectilinear crossing number of K_{n,n}. - _Eric W. Weisstein_, Apr 24 2017",
				"a(n+1) is the number of 4-tuples (w,x,y,z) with all terms in {0,...,n}, and w,x,y+1,z+1 all even. - _Clark Kimberling_, May 29 2012"
			],
			"reference": [
				"C. Thomassen, Embeddings and minors, pp. 301-349 of R. L. Graham et al., eds., Handbook of Combinatorics, MIT Press."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A030179/b030179.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"G. Xiao, \u003ca href=\"http://wims.unice.fr/~wims/en_tool~number~contfrac.en.html\"\u003eContfrac\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CompleteBipartiteGraph.html\"\u003eComplete Bipartite Graph\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/GraphCrossingNumber.html\"\u003eGraph Crossing Number\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RectilinearCrossingNumber.html\"\u003eRectilinear Crossing Number\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ZarankiewiczsConjecture.html\"\u003eZarankiewicz's Conjecture\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_08\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,2,-6,0,6,-2,-2,1)."
			],
			"formula": [
				"a(n) = floor(n^2/4)^2.",
				"From _R. J. Mathar_, Jul 08 2010: (Start)",
				"G.f.: x^2*(1+2*x+6*x^2+2*x^3+x^4) / ( (1+x)^3*(1-x)^5 ).",
				"a(n) = 2*a(n-1) +2*a(n-2) -6*a(n-3) +6*a(n-5) -2*a(n-6) -2*a(n-7) +a(n-8). (End)",
				"a(n) = (2*n^4 -2*n^2 +1 +(-1)^n*(2*n^2 -1))/32. - _Luce ETIENNE_, Aug 11 2014"
			],
			"maple": [
				"seq( (2*n^4 -2*n^2 +1 +(-1)^n*(2*n^2 -1))/32, n=0..40); # _G. C. Greubel_, Dec 28 2019"
			],
			"mathematica": [
				"f[n_]:=Floor[n^2/2]; Table[Nest[f,n,2],{n,5!}]/2 (* _Vladimir Joseph Stephan Orlovsky_, Mar 10 2010 *)",
				"LinearRecurrence[{2,2,-6,0,6,-2,-2,1}, {0,0,1,4,16,36,81,144}, 40] (* _Harvey P. Dale_, Apr 26 2011 *)",
				"Floor[Range[0, 30]^2/4]^2 (* _Eric W. Weisstein_, Apr 24 2017 *)"
			],
			"program": [
				"(PARI) a(n) = (n^2\\4)^2 \\\\ _Charles R Greathouse IV_, Jun 11 2015",
				"(MAGMA) [(Floor(n^2/4))^2: n in [0..40]]; // _G. C. Greubel_, Dec 28 2019",
				"(Sage) [floor(n^2/4)^2 for n in (0..40)] # _G. C. Greubel_, Dec 28 2019",
				"(GAP) List([0..40], n-\u003e (2*n^4 -2*n^2 +1 +(-1)^n*(2*n^2 -1))/32); # _G. C. Greubel_, Dec 28 2019"
			],
			"xref": [
				"Cf. A000241, A002620, A014540."
			],
			"keyword": "nonn,easy",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_, Jan 10 2002",
			"references": 19,
			"revision": 45,
			"time": "2019-12-29T08:40:57-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}