{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A317408",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 317408,
			"data": "0,1,6,24,84,275,864,2639,7896,23256,67650,194821,556416,1578109,4449354,12480600,34852944,96949079,268746336,742675211,2046683100,5626200216,15430992126,42235173769,115380647424,314656725625,856733282574,2329224424344,6323840144076",
			"name": "a(n) = n * Fibonacci(2n).",
			"comment": [
				"Derivative of Morgan-Voyce Lucas-type evaluated at 1."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A317408/b317408.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Rigoberto Flórez, Robinson Higuita, and Alexander Ramírez, \u003ca href=\"https://arxiv.org/abs/1808.01264\"\u003e The resultant, the discriminant, and the derivative of generalized Fibonacci polynomials\u003c/a\u003e, arXiv:1808.01264 [math.NT], 2018.",
				"Rigoberto Flórez, Robinson Higuita, and Antara Mukherjee, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL21/Florez2/florez8.html\"\u003eStar of David and other patterns in the Hosoya-like polynomials triangles\u003c/a\u003e, Journal of Integer Sequences, Vol. 21 (2018), Article 18.4.6.",
				"R. Flórez, N. McAnally, and A. Mukherjees, \u003ca href=\"http://math.colgate.edu/~integers/s18b2/s18b2.Abstract.html\"\u003eIdentities for the generalized Fibonacci polynomial\u003c/a\u003e, Integers, 18B (2018), Paper No. A2.",
				"R. Flórez, R. Higuita and A. Mukherjees, \u003ca href=\"http://math.colgate.edu/~integers/s14/s14.Abstract.html\"\u003eCharacterization of the strong divisibility property for generalized Fibonacci polynomials\u003c/a\u003e, Integers, 18 (2018), Paper No. A14.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Morgan-VoycePolynomials.html\"\u003eMorgan-Voyce Polynomials\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (6,-11,6,-1)."
			],
			"formula": [
				"G.f.: -(x-1)*(x+1)*x/(x^2-3*x+1)^2. - _Alois P. Heinz_, Jul 27 2018",
				"a(n) = 6*a(n-1) - 11*a(n-2) + 6*a(n-3) - a(n-4) for n \u003e 4. - _Andrew Howroyd_, Jul 27 2018",
				"a(n) = (2^(-n)*((-(3-sqrt(5))^n + (3+sqrt(5))^n)*n))/sqrt(5). - _Colin Barker_, Jul 28 2018",
				"a(n) = n*A001906(n). - _Omar E. Pol_, Jul 29 2018"
			],
			"maple": [
				"a:= n-\u003e (\u003c\u003c0|1|0|0\u003e, \u003c0|0|1|0\u003e, \u003c0|0|0|1\u003e, \u003c-1|6|-11|6\u003e\u003e^n. \u003c\u003c0, 1, 6, 24\u003e\u003e)[1$2]:",
				"seq(a(n), n=1..35);  # _Alois P. Heinz_, Jul 27 2018"
			],
			"mathematica": [
				"CoefficientList[Series[-(x - 1) (x + 1) x/(x^2 - 3 x + 1)^2, {x, 0, 28}], x] (* or *)",
				"LinearRecurrence[{6, -11, 6, -1}, {0, 1, 6, 24}, 29] (* or *)",
				"Array[# Fibonacci[2 #] \u0026, 29, 0] (* _Michael De Vlieger_, Jul 27 2018 *)"
			],
			"program": [
				"(PARI) a(n)=n*fibonacci(2*n) \\\\ _Andrew Howroyd_, Jul 27 2018",
				"(PARI) Vec(-(x-1)*(x+1)*x/(x^2-3*x+1)^2 + O(x^30)) \\\\ _Andrew Howroyd_, Jul 27 2018"
			],
			"xref": [
				"Cf. A000045, A001906, A045925."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Rigoberto Florez_, Jul 27 2018",
			"references": 6,
			"revision": 48,
			"time": "2019-04-25T13:30:07-04:00",
			"created": "2018-08-26T11:26:15-04:00"
		}
	]
}