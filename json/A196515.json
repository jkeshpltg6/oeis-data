{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A196515",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 196515,
			"data": "8,5,2,6,0,5,5,0,2,0,1,3,7,2,5,4,9,1,3,4,6,4,7,2,4,1,4,6,9,5,3,1,7,4,6,6,8,9,8,4,5,3,3,0,0,1,5,1,4,0,3,5,0,8,7,7,2,1,0,7,3,9,4,6,5,2,5,1,5,0,6,5,6,7,4,2,6,3,0,4,4,8,9,6,5,7,7,3,7,8,3,5,0,2,4,9,4,8,4,7",
			"name": "Decimal expansion of the number x satisfying x*e^x = 2.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A196515/b196515.txt\"\u003eTable of n, a(n) for n = 0..5000\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/LambertW-Function.html\"\u003eLambert W-Function\u003c/a\u003e",
				"\u003ca href=\"/index/Tra#transcendental\"\u003eIndex entries for transcendental numbers\u003c/a\u003e"
			],
			"formula": [
				"From _A.H.M. Smeets_, Nov 19 2018: (Start)",
				"Equals LambertW(2).",
				"Consider LambertW(z), where z is a complex number: let x(0) be an arbitrary complex number; x(n+1) = z*exp(-x(n)); if lim_{n -\u003e inf) x(n) exists (which is the case for z = 2), then LambertW(z) = lim_{n -\u003e inf) x(n). The region in the complex plane for which this seems to work is as follows: let z = x+iy, then -1/e \u003c x \u003c e for y = 0 and -c \u003c y \u003c c, c = 1.9612... for x = 0. It is not known if the area is open or closed. (End)"
			],
			"example": [
				"0.852605502013725491346472414695317466898..."
			],
			"mathematica": [
				"Plot[{E^x, 1/x, 2/x, 3/x, 4/x}, {x, 0, 2}]",
				"t = x /. FindRoot[E^x == 1/x, {x, 0.5, 1}, WorkingPrecision -\u003e 100]",
				"RealDigits[t]  (* A030178 *)",
				"t = x /. FindRoot[E^x == 2/x, {x, 0.5, 1}, WorkingPrecision -\u003e 100]",
				"RealDigits[t]  (* A196515 *)",
				"t = x /. FindRoot[E^x == 3/x, {x, 0.5, 2}, WorkingPrecision -\u003e 100]",
				"RealDigits[t]  (* A196516 *)",
				"t = x /. FindRoot[E^x == 4/x, {x, 0.5, 2}, WorkingPrecision -\u003e 100]",
				"RealDigits[t]  (* A196517 *)",
				"t = x /. FindRoot[E^x == 5/x, {x, 0.5, 2}, WorkingPrecision -\u003e 100]",
				"RealDigits[t]  (* A196518 *)",
				"t = x /. FindRoot[E^x == 6/x, {x, 0.5, 2}, WorkingPrecision -\u003e 100]",
				"RealDigits[t]  (* A196519 *)",
				"RealDigits[ ProductLog[2], 10, 100] // First (* _Jean-François Alcover_, Feb 26 2013 *)",
				"(* A good approximation (the first 30 digits) is given by this power series evaluated at z=2, expanded at log(z): *)",
				"Clear[x, a, nn, b, z]",
				"z = 2;",
				"nn = 100;",
				"a = Series[Exp[-x], {x, N[Log[z], 50], nn}];",
				"b = Normal[InverseSeries[Series[x/a, {x, 0, nn}]]];",
				"x = z;",
				"N[b, 30]",
				"N[LambertW[z], 30] (* _Mats Granvik_, Nov 29 2013 *)",
				"RealDigits[LambertW[2], 10, 50][[1]] (* _G. C. Greubel_, Nov 16 2017 *)"
			],
			"program": [
				"(PARI) lambertw(2) \\\\ _G. C. Greubel_, Nov 16 2017"
			],
			"keyword": "nonn,cons",
			"offset": "0,1",
			"author": "_Clark Kimberling_, Oct 03 2011",
			"references": 6,
			"revision": 43,
			"time": "2021-08-31T02:39:06-04:00",
			"created": "2011-10-03T18:37:33-04:00"
		}
	]
}