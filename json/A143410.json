{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A143410",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 143410,
			"data": "1,1,1,5,3,1,29,17,5,1,233,131,37,7,1,2329,1281,353,65,9,1,27949,15139,4105,743,101,11,1,391285,209617,56189,10049,1349,145,13,1,6260561,3325923,883885,156679,20841,2219,197,15,1,112690097,59475329,15700313",
			"name": "Form the difference table of the sequence {2^k*k!}, then divide k-th column entries by 2^k*k!. Array read by ascending antidiagonals, T(n, k) for n, k \u003e= 0.",
			"comment": [
				"This table is closely connected to the constant sqrt(e). The row, column and diagonal entries of this table occur in series acceleration formulas for sqrt(e). For a similar table based on the Euler-Seidel matrix of the sequence {2^k*k!} and related to the constant 1/sqrt(e), see A143411. For other arrays similarly related to constants see A086764 (for e), A143409 (for 1/e), A008288 (for log(2)), A108625 (for zeta(2)) and A143007 (for zeta(3))."
			],
			"link": [
				"Eric Weisstein's World of Mathematics \u003ca href=\"http://mathworld.wolfram.com/Poisson-CharlierPolynomial.html\"\u003ePoisson-Charlier polynomial\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = (-1)^n/k!*sum {j = 0..n} (-2)^j*C(n,j)*(k+j)!.",
				"Relation with Poisson-Charlier polynomials c_n(x,a): T(n,k) = c_n(-(k+1),-1/2).",
				"Recurrence relations: T(n,k) = 2*n*T(n-1,k) + T(n,k-1); T(n,k) = 2*(n+k)*T(n-1,k) - T(n-1,k-1); T(n,k) = 2*(k+1)*T(n-1,k+1) - T(n-1,k);",
				"Recurrence for row n entries: 2*k*T(n,k) = (2*n+2*k+1)*T(n,k-1) - T(n,k-2).",
				"E.g.f. for column k: exp(-y)/(1-2*y)^(k+1).",
				"E.g.f. for array: exp(-y)/(1-x-2*y) = (1 + x + x^2 + ...) + (1 + 3*x + 5*x^2 + ...)*y + (5 + 17*x + 37*x^2 + ...)*y^2/2! + ... .",
				"Series acceleration formulas for sqrt(e):",
				"Row n: sqrt(e) = 2^n*n!*(1/T(n,0) + (-1)^n*[1/(2*1!*T(n,0)*T(n,1)) + 1/(2^2*2!*T(n,1)*T(n,2)) + 1/(2^3*3!*T(n,2)*T(n,3)) + ...]). For example, row 3 gives sqrt(e) = 48*(1/29 - 1/(2*29*131) - 1/(8*131*353) - 1/(48*353*743) - ...).",
				"Column k: sqrt(e) = (1+(1/2)/1!+(1/2)^2/2!+...+(1/2)^k/k!) + 1/(2^k*k!) * sum {n = 0..inf}((-2)^n *n!/(T(n,k)*T(n+1,k))). For example, column 3 gives sqrt(e) = 79/48 + 1/48*[1/(1*7) - 2/(7*65) + 8/(65*743) - 48/(743*10049) + ...].",
				"Main diagonal: sqrt(e) = 1 + 2*[1/(1*3) - 1/(3*37) + 1/(37*743) - ...]. See A143412.",
				"T(n, k) = (-1)^n*(-1/2)^(k + 1)*KummerU(k + 1, k + n + 2, -1/2). - _Peter Luschny_, Jan 02 2020"
			],
			"example": [
				"Table of differences of {2^k*k!}",
				"=====================================================",
				"Column................0.....1.....2.....3.....4.....5",
				"=====================================================",
				"Sequence 2^k*k! ......1.....2.....8....48...384..3840",
				"First differences.....1.....6....40...336..3456",
				"Second differences....5....34...296..3120",
				"Third differences....29...262..2824",
				"Fourth differences..233..2562",
				"...",
				"Remove the common factor 2^k*k! from k-th column entries:",
				"====================================",
				"n\\k|...0......1......2......3......4",
				"====================================",
				"0..|...1......1......1......1......1",
				"1..|...1......3......5......7......9",
				"2..|...5.....17.....37.....65....101",
				"3..|..29....131....353....743...1349",
				"4..|.233...1281...4105..10049..20841",
				"..."
			],
			"maple": [
				"T := (n, k) -\u003e (-1)^n/k!*add((-2)^j*binomial(n,j)*(k+j)!, j = 0..n):",
				"for n from 0 to 9 do seq(T(n, k), k = 0..9) end do;"
			],
			"xref": [
				"Cf. A008288, A076571, A086764, A108625, A143007, A143409, A143411, A143412."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "0,4",
			"author": "_Peter Bala_, Aug 19 2008",
			"references": 4,
			"revision": 14,
			"time": "2020-01-02T13:57:33-05:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}