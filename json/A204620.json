{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A204620",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 204620,
			"data": "41,209,157169,213321,303093,382449,2145353,2478785",
			"name": "Numbers k such that 3*2^k + 1 is a prime factor of a Fermat number 2^(2^m) + 1 for some m.",
			"comment": [
				"Terms are odd: by Morehead's theorem, 3*2^(2*n) + 1 can never divide a Fermat number.",
				"No other terms below 7516000.",
				"Is this sequence the same as \"Numbers k such that 3*2^k + 1 is a factor of a Fermat number 2^(2^m) + 1 for some m\"? - _Arkadiusz Wesolowski_, Nov 13 2018",
				"The last sentence of Morehead's paper is: \"It is easy to show that _composite_ numbers of the forms 2^kappa * 3 + 1, 2^kappa * 5 + 1 can not be factors of Fermat's numbers.\" [a proof is needed]. - _Jeppe Stig Nielsen_, Jul 23 2019",
				"Any factor of a Fermat number 2^(2^m) + 1 of the form 3*2^k + 1 is prime if k \u003c 2*m + 6. - _Arkadiusz Wesolowski_, Jun 12 2021",
				"If, for any m \u003e= 0, F(m) = 2^(2^m) + 1 has a prime factor p of the form 3*2^k + 1, then F(m)/p is congruent to 11 mod 30. - _Arkadiusz Wesolowski_, Jun 13 2021",
				"A number k belongs to this sequence if and only if the order of 2 modulo p is not divisible by 3, where p is a prime of the form 3*2^k + 1 (see Golomb paper). - _Arkadiusz Wesolowski_, Jun 14 2021"
			],
			"link": [
				"Solomon W. Golomb, \u003ca href=\"https://doi.org/10.1090/S0025-5718-1976-0404129-8\"\u003eProperties of the sequence 3.2^n+1\u003c/a\u003e, Math. Comp., 30 (1976), 657-663.",
				"Wilfrid Keller, \u003ca href=\"http://www.prothsearch.com/fermat.html\"\u003eFermat factoring status\u003c/a\u003e",
				"J. C. Morehead, \u003ca href=\"https://doi.org/10.1090/S0002-9904-1906-01371-4\"\u003eNote on the factors of Fermat's numbers\u003c/a\u003e, Bull. Amer. Math. Soc., Volume 12, Number 9 (1906), pp. 449-451.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/FermatNumber.html\"\u003eFermat Number\u003c/a\u003e"
			],
			"mathematica": [
				"lst = {}; Do[p = 3*2^n + 1; If[PrimeQ[p] \u0026\u0026 IntegerQ@Log[2, MultiplicativeOrder[2, p]], AppendTo[lst, n]], {n, 7, 209, 2}]; lst"
			],
			"program": [
				"(PARI) isok(n) = my(p = 3*2^n + 1, z = znorder(Mod(2, p))); isprime(p) \u0026\u0026 ((z \u003e\u003e valuation(z, 2)) == 1); \\\\ _Michel Marcus_, Nov 10 2018"
			],
			"xref": [
				"Subsequence of A002253.",
				"Cf. A000215, A039687, A057775, A057778, A201364, A226366."
			],
			"keyword": "nonn,hard,more",
			"offset": "1,1",
			"author": "_Arkadiusz Wesolowski_, Jan 17 2012",
			"references": 16,
			"revision": 63,
			"time": "2021-09-16T02:22:51-04:00",
			"created": "2012-01-17T14:51:55-05:00"
		}
	]
}