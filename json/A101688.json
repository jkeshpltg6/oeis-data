{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A101688",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 101688,
			"data": "1,0,1,0,1,1,0,0,1,1,0,0,1,1,1,0,0,0,1,1,1,0,0,0,1,1,1,1,0,0,0,0,1,1,1,1,0,0,0,0,1,1,1,1,1,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,1,1,1,1,1,1,1,0,0,0,0,0,0,0,1,1,1,1",
			"name": "Once 1, once 0, repeat, twice 1, twice 0, repeat, thrice 1, thrice 0... and so on.",
			"comment": [
				"The definition is that of a linear sequence. Equivalently, define a (0,1) infinite lower triangular matrix T(n,k) (0 \u003c= k \u003c= n) by T(n,k) = 1 if k \u003e= n/2, 0 otherwise, and read it by rows. The triangle T begins:",
				"  1",
				"  0 1",
				"  0 1 1",
				"  0 0 1 1",
				"  0 0 1 1 1",
				"  0 0 0 1 1 1",
				"...  The matrix T is used in A168508. [Comment revised by _N. J. A. Sloane_, Dec 05 2020]",
				"Also, square array A read by antidiagonals upwards: A(n,k) = 1 if k \u003e= n, 0 otherwise.",
				"For n \u003e= 1, T(n,k) = number of partitions of n into k parts of sizes 1 or 2. - _Nicolae Boicu_, Aug 23 2018"
			],
			"link": [
				"Boris Putievskiy, \u003ca href=\"https://arxiv.org/abs/1212.2732\"\u003eTransformations (of) Integer Sequences And Pairing Functions\u003c/a\u003e, arXiv:1212.2732 [math.CO], 2012."
			],
			"formula": [
				"G.f.: 1/[(1-xy)(1-y)]. k-th row of array: x^(k-1)/(1-x).",
				"T(n, k) = if(binomial(k, n-k)\u003e0, 1, 0). - _Paul Barry_, Aug 23 2005",
				"From _Boris Putievskiy_, Jan 09 2013: (Start)",
				"a(n) = floor((2*A002260(n)+1)/A003056(n)+3).",
				"a(n) = floor((2*n-t*(t+1)+1)/(t+3)), where",
				"t = floor((-1+sqrt(8*n-7))/2). (End)",
				"a(n) = floor(sqrt(2*n+1)) - floor(sqrt(2*n+2) - 1/2). - _Ridouane Oudra_, Jul 16 2020"
			],
			"example": [
				"The array A (on the left) and the triangle T of its antidiagonals (on the right):",
				".1 1 1 1 1 1 1 1 1 ......... 1",
				".0 1 1 1 1 1 1 1 1 ........ 0 1",
				".0 0 1 1 1 1 1 1 1 ....... 0 1 1",
				".0 0 0 1 1 1 1 1 1 ...... 0 0 1 1",
				".0 0 0 0 1 1 1 1 1 ..... 0 0 1 1 1",
				".0 0 0 0 0 1 1 1 1 .... 0 0 0 1 1 1",
				".0 0 0 0 0 0 1 1 1 ... 0 0 0 1 1 1 1",
				".0 0 0 0 0 0 0 1 1 .. 0 0 0 0 1 1 1 1",
				".0 0 0 0 0 0 0 0 1 . 0 0 0 0 1 1 1 1 1"
			],
			"mathematica": [
				"rows = 15; A = Array[If[#1 \u003c= #2, 1, 0]\u0026, {rows, rows}]; Table[A[[i-j+1, j]], {i, 1, rows}, {j, 1, i}] // Flatten (* _Jean-François Alcover_, May 04 2017 *)"
			],
			"xref": [
				"Row sums of T (and antidiagonal sums of A) are A008619.",
				"Cf. A079813, A168508."
			],
			"keyword": "nonn,tabl",
			"offset": "0,1",
			"author": "_Ralf Stephan_, Dec 19 2004",
			"ext": [
				"Edited by _N. J. A. Sloane_, Dec 05 2020"
			],
			"references": 16,
			"revision": 64,
			"time": "2020-12-06T13:25:36-05:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}