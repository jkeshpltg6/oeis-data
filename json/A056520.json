{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A056520",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 56520,
			"data": "1,2,6,15,31,56,92,141,205,286,386,507,651,820,1016,1241,1497,1786,2110,2471,2871,3312,3796,4325,4901,5526,6202,6931,7715,8556,9456,10417,11441,12530,13686,14911,16207,17576,19020,20541,22141,23822",
			"name": "a(n) = (n + 2)*(2*n^2 - n + 3)/6.",
			"comment": [
				"Hankel transform of A030238. - _Paul Barry_, Oct 16 2007",
				"Equals (1, 2, 3, 4, 5,...) convolved with (1, 0, 3, 5, 7, 9,...). - _Gary W. Adamson_, Jul 31 2010",
				"a(n) equals n!^2 times the determinant of the n X n matrix whose (i,j)-entry is 1 + KroneckerDelta[i, j] (-1 + (1 + i^2)/i^2). - _John M. Campbell_, May 20 2011",
				"Positions of ones in A253903. - _Harvey P. Dale_, Mar 05 2015"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A056520/b056520.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Guo-Niu Han, \u003ca href=\"/A196265/a196265.pdf\"\u003eEnumeration of Standard Puzzles\u003c/a\u003e, 2011. [Cached copy]",
				"Guo-Niu Han, \u003ca href=\"https://arxiv.org/abs/2006.14070\"\u003eEnumeration of Standard Puzzles\u003c/a\u003e, arXiv:2006.14070 [math.CO], 2020.",
				"Amit Kumar Singh, Akash Kumar and Thambipillai Srikanthan, \u003ca href=\"http://www.ece.nus.edu.sg/stfpage/eleak/pdf/akumar_todaes_2012.pdf\"\u003eAccelerating Throughput-aware Run-time Mapping for Heterogeneous MPSoCs\u003c/a\u003e, ACM Transactions on Design Automation of Electronic Systems, 2012. - From _N. J. A. Sloane_, Dec 25 2012"
			],
			"formula": [
				"a(n) = a(n-1) + n^2.",
				"a(n) = A000330(n) + 1.",
				"G.f.: (1 - 2*x + 4*x^2 - x^3)/(1 - x)^4. - _Paul Barry_, Apr 14 2010",
				"Let b(0) = b(1) = 1, b(n) = max(b(n-1) + (n - 1)^2, b(n-2) + (n - 2)^2) for n \u003e= 2; then a(n) = b(n+1). - _Yalcin Aktar_, Jul 28 2011"
			],
			"mathematica": [
				"a[n_] := (n+2)*(2*n^2-n+3)/6; Table[a[n], {n, 0, 100}] (* _Vladimir Joseph Stephan Orlovsky_, Dec 17 2008 *)",
				"s = 1; lst = {s}; Do[s += n^2; AppendTo[lst, s], {n, 1, 41, 1}]; lst (* _Zerinvary Lajos_, Jul 12 2009 *)",
				"Table[n!^2*Det[Array[KroneckerDelta[#1,#2](((#1^2+1)/(#1^2))-1)+1\u0026,{n,n}]],{n,1,20}] (* _John M. Campbell_, May 20 2011 *)",
				"FoldList[#1 + #2^2 \u0026, 1, Range@ 40] (* _Robert G. Wilson v_, Oct 28 2011 *)"
			],
			"program": [
				"(MAGMA)[(n+2)*(2*n^2-n+3)/6: n in [0..40]]; // _Vincenzo Librandi_, May 24 2011",
				"(PARI) a(n)=(n+2)*(2*n^2-n+3)/6 \\\\ _Charles R Greathouse IV_, Jul 02 2013"
			],
			"xref": [
				"Cf. A000330, A030238, A253903."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Laura Kasavan (maui12129(AT)cswebmail.com), Aug 26 2000",
			"references": 20,
			"revision": 56,
			"time": "2020-07-28T11:10:37-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}