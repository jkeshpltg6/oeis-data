{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A327087",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 327087,
			"data": "1,1,2,2,1,10,54,136,150,60,1,38,1080,14040,85500,274104,493920,504000,272160,60480,1,182,42111,2848060,70361815,841626366,5722670282,24262499520,67665563280,127639512000,164044520640,141664723200,78702624000,25427001600,3632428800",
			"name": "Triangle read by rows: T(n,k) is the number of oriented colorings of the edges of a regular n-dimensional simplex using exactly k colors. Row n has (n+1)*n/2 columns.",
			"comment": [
				"An n-dimensional simplex has n+1 vertices and (n+1)*n/2 edges. For n=1, the figure is a line segment with one edge. For n=2, the figure is a triangle with three edges. For n=3, the figure is a tetrahedron with six edges. The Schläfli symbol {3,...,3}, of the regular n-dimensional simplex consists of n-1 threes. Two oriented colorings are the same if one is a rotation of the other; chiral pairs are counted as two.",
				"T(n,k) is also the number of oriented colorings of (n-2)-dimensional regular simplices in an n-dimensional simplex using exactly k colors. Thus, T(2,k) is also the number of oriented colorings of the vertices (0-dimensional simplices) of an equilateral triangle."
			],
			"link": [
				"Robert A. Russell, \u003ca href=\"/A327087/b327087.txt\"\u003eTable of n, a(n) for n = 1..220\u003c/a\u003e First 10 rows.",
				"E. M. Palmer and R. W. Robinson, \u003ca href=\"https://projecteuclid.org/euclid.acta/1485889789\"\u003eEnumeration under two representations of the wreath product\u003c/a\u003e, Acta Math., 131 (1973), 123-143."
			],
			"formula": [
				"The algorithm used in the Mathematica program below assigns each permutation of the vertices to a partition of n+1. It then determines the number of permutations for each partition and the cycle index for each partition.",
				"A327083(n,k) = Sum_{j=1..(n+1)*n/2} T(n,j) * binomial(k,j).",
				"T(n,k) = A327088(n,k) + A327089(n,k) = 2*A327088(n,k) - A327090(n,k) = 2*A327089(n,k) + A327090(n,k)."
			],
			"example": [
				"Triangle begins with T(1,1):",
				"  1",
				"  1  2    2",
				"  1 10   54   136   150     60",
				"  1 38 1080 14040 85500 274104 493920 504000 272160 60480",
				"  ...",
				"For T(2,1)=1, all edges of the triangle are the same color. For T(2,2)=2, the edges are AAB and ABB. For T(2,3)=2, the chiral pair is ABC-ACB."
			],
			"mathematica": [
				"CycleX[{2}] = {{1,1}}; (* cycle index for permutation with given cycle structure *)",
				"CycleX[{n_Integer}] := CycleX[n] = If[EvenQ[n], {{n/2,1}, {n,(n-2)/2}}, {{n,(n-1)/2}}]",
				"compress[x : {{_, _} ...}] := (s = Sort[x]; For[i = Length[s], i\u003e1, i-=1, If[s[[i,1]] == s[[i-1,1]], s[[i-1,2]] += s[[i,2]]; s = Delete[s,i], Null]]; s)",
				"CycleX[p_List] := CycleX[p] = compress[Join[CycleX[Drop[p,-1]], If[Last[p] \u003e 1, CycleX[{Last[p]}], ## \u0026[]], If[# == Last[p], {#, Last[p]}, {LCM[#, Last[p]], GCD[#, Last[p]]}] \u0026 /@ Drop[p,-1]]]",
				"pc[p_List] := Module[{ci, mb}, mb = DeleteDuplicates[p]; ci = Count[p, #] \u0026 /@ mb; Total[p]!/(Times @@ (ci!) Times @@ (mb^ci))] (*partition count*)",
				"row[n_Integer] := row[n] = Factor[Total[If[EvenQ[Total[1-Mod[#,2]]], pc[#] j^Total[CycleX[#]][[2]], 0] \u0026 /@ IntegerPartitions[n+1]]/((n+1)!/2)]",
				"array[n_, k_] := row[n] /. j -\u003e k",
				"Table[LinearSolve[Table[Binomial[i,j], {i,1,(n+1)n/2}, {j,1,(n+1)n/2}], Table[array[n,k], {k,1,(n+1)n/2}]], {n,1,6}] // Flatten"
			],
			"xref": [
				"Cf. A327088 (unoriented), A327089 (chiral), A327090 (achiral), A327083 (up to k colors), A325002 (vertices and facets), A338113 (faces and peaks), A338142 (orthotope edges, orthoplex ridges), A338146 (orthoplex edges, orthotope ridges)."
			],
			"keyword": "nonn,tabf",
			"offset": "1,3",
			"author": "_Robert A. Russell_, Aug 19 2019",
			"references": 8,
			"revision": 18,
			"time": "2021-06-09T02:34:42-04:00",
			"created": "2019-08-19T16:57:30-04:00"
		}
	]
}