{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A065620",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 65620,
			"data": "0,1,2,-1,4,-3,-2,3,8,-7,-6,7,-4,5,6,-5,16,-15,-14,15,-12,13,14,-13,-8,9,10,-9,12,-11,-10,11,32,-31,-30,31,-28,29,30,-29,-24,25,26,-25,28,-27,-26,27,-16,17,18,-17,20,-19,-18,19,24,-23,-22,23,-20,21,22,-21,64,-63,-62,63,-60,61,62,-61,-56",
			"name": "a(0)=0; thereafter a(2n) = 2a(n), a(2n+1) = -2a(n) + 1.",
			"comment": [
				"Reversing binary value of n: convert sum of powers of 2 in binary representation of n to alternating sum.",
				"The alternation is applied only to the nonzero bits and does not depend on the exponent of 2. All integers have a unique reversing binary representation (see cited Knuth exercise for proof)."
			],
			"reference": [
				"D. E. Knuth, The Art of Computer Programming. Addison-Wesley, Reading, MA, 1969, Vol. 2, p. 178, (exercise 4.1. Nr. 27)"
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/A065620/b065620.txt\"\u003eTable of n, a(n) for n = 0..20000\u003c/a\u003e (First 8192 terms from Reinhard Zumkeller)"
			],
			"formula": [
				"a(n) = if n\u003c2 then n else b+2*(1-2*b)*a((n-b)/2) where b is the least significant bit in n.",
				"From _Antti Karttunen_, Aug 18 2014: (Start)",
				"a(n) = A246160(n) - A246159(n).",
				"a(A065621(n)) = n.",
				"a(A048724(n)) = -n.",
				"(End)",
				"a(n) = -A104895(n). - _M. F. Hasler_, Apr 16 2018"
			],
			"example": [
				"11 = 1 + 2 + 8 -\u003e 1 - 2 + 8 = 7 = a(11)"
			],
			"maple": [
				"f:=proc(n) option remember; if n=0 then 0 elif (n mod 2) = 0 then 2*f(n/2) else 1-2*f((n-1)/2); fi; end;",
				"[seq(f(n),n=0..100)]; # _N. J. A. Sloane_, Apr 25 2018"
			],
			"program": [
				"(Haskell)",
				"import Data.List (transpose)",
				"a065620 n = a065620_list !! n",
				"a065620_list = 1 : concat (transpose [zs, map ((+ 1) . negate) zs])",
				"               where zs = map (* 2) a065620_list",
				"-- _Reinhard Zumkeller_, Mar 26 2014",
				"(Scheme)",
				"(definec (A065620 n) (cond ((zero? n) n) ((even? n) (* 2 (A065620 (/ n 2)))) (else (+ 1 (* -2 (A065620 (/ (- n 1) 2)))))))",
				";; using memoization-macro definec from IntSeq-library of _Antti Karttunen_, Aug 18 2014",
				"(Python)",
				"def a(n): return n if n\u003c3 else 2*a(n/2) if n%2==0 else -2*a((n - 1)/2) + 1 # _Indranil Ghosh_, Jun 07 2017",
				"(PARI) A065620(n,c=1)=sum(i=0,logint(n+!n,2),if(bittest(n,i),(-1)^c++\u003c\u003ci)) \\\\ _M. F. Hasler_, Apr 16 2018"
			],
			"xref": [
				"Cf. A065621, A048724, A246159, A246160.",
				"The negative of entry A104895."
			],
			"keyword": "base,easy,sign,look",
			"offset": "0,3",
			"author": "_Marc LeBrun_, Nov 07 2001",
			"ext": [
				"Formula fixed by _Reinhard Zumkeller_, Mar 26 2014",
				"Extended to a(0) = 0 by _M. F. Hasler_, Apr 16 2018",
				"Edited by _N. J. A. Sloane_, Apr 25 2018"
			],
			"references": 21,
			"revision": 34,
			"time": "2018-04-25T09:44:35-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}