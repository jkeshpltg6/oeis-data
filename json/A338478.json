{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A338478",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 338478,
			"data": "0,1,2,3,4,13,12,11,8,9,10,7,6,5,32,33,34,37,36,35,38,39,40,31,30,29,26,27,28,25,24,23,14,15,16,19,18,17,20,21,22,103,102,101,98,99,100,97,96,95,104,105,106,109,108,107,110,111,112,121,120,119,116",
			"name": "Let b be an odd function such that b(0) = 0, b(1) = 1, and for any n \u003e 1 such that 3^x \u003c 2*n \u003c 3^(x+1) for some x \u003e 0, b(n) = b(3^x-n) - 3^x; a(n) = abs(b(n)) for any n \u003e= 0.",
			"comment": [
				"This sequence is a self-inverse permutation of the nonnegative integers.",
				"It is possible to build a continuous injective complex-valued function of a real-variable, say f, such that Im(f(r)) = 0 iff r is an integer and for any n in Z, f(n) = b(n) (see illustration in Links section)."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A338478/b338478.txt\"\u003eTable of n, a(n) for n = 0..6560\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A338478/a338478.png\"\u003eRepresentation in the complex plane of a function f as described in Comments section\u003c/a\u003e (the corresponding curve divides the complex plane into two simply connected regions rendered with different colors)",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"a(n) = n iff abs(n - 3^x) \u003c= 1 for some x \u003e= 0."
			],
			"example": [
				"For n = 3:",
				"- we have 3^1 \u003c 2*3 \u003c 3^(1+1),",
				"- so b(3) = b(3 - 3) - 3 = 0 - 3 = -3,",
				"- a(3) = abs(b(3)) = 3."
			],
			"program": [
				"(PARI) b(n) = { if (n\u003c0,  return (-b(-n)), n==0, return (0), n==1, return (1), for (x=1, oo, my (w=3^x, h=w\\2); if (w\u003c2*n \u0026\u0026 2*n\u003c3*w, return (b(w-n)-w)))) }",
				"a(n) = abs(b(n))"
			],
			"xref": [
				"Cf. A000244, A003462, A024023, A034472, A105209."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Rémy Sigrist_, Oct 29 2020",
			"references": 1,
			"revision": 16,
			"time": "2021-08-22T13:18:38-04:00",
			"created": "2020-11-02T20:40:04-05:00"
		}
	]
}