{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A117664",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 117664,
			"data": "1,3,10,105,252,2310,25740,9009,136136,11639628,10581480,223092870,1029659400,2868336900,11090902680,644658718275,606737617200,4011209802600,140603459396400,133573286426580,5215718803323600",
			"name": "Denominator of the sum of all elements in the n X n Hilbert matrix M(i,j) = 1/(i+j-1), where i,j = 1..n.",
			"comment": [
				"Sum_{j=1..n} Sum_{i=1..n} 1/(i+j-1) = A117731(n) / A117664(n) = 2n * H'(2n) = 2n * A058313(2n) / A058312(2n), where H'(2n) is 2n-th alternating sign Harmonic Number. H'(2n) = H(2n) - H(n), where H(n) is n-th Harmonic Number. - _Alexander Adamchuk_, Apr 23 2006"
			],
			"link": [
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/HilbertMatrix.html\"\u003eHilbert Matrix\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/HarmonicNumber.html\"\u003eHarmonic Number\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A111876(n-1)/n.",
				"a(n) = denominator( Sum_{j=1..n} Sum_{i=1..n} 1/(i+j-1) ). Numerator is A117731(n). - _Alexander Adamchuk_, Apr 23 2006",
				"a(n) = denominator( Sum_{k=1..n} (2*k)/(n+k) ). - _Peter Bala_, Oct 10 2021"
			],
			"example": [
				"For n=2, the 2 X 2 Hilbert matrix is [1, 1/2; 1/2, 1/3], so a(2) = denominator(1 + 1/2 + 1/2 + 1/3) = denominator(7/3) = 3.",
				"The n X n Hilbert matrix begins:",
				"    1 1/2 1/3 1/4  1/5  1/6  1/7  1/8 ...",
				"  1/2 1/3 1/4 1/5  1/6  1/7  1/8  1/9 ...",
				"  1/3 1/4 1/5 1/6  1/7  1/8  1/9 1/10 ...",
				"  1/4 1/5 1/6 1/7  1/8  1/9 1/10 1/11 ...",
				"  1/5 1/6 1/7 1/8  1/9 1/10 1/11 1/12 ...",
				"  1/6 1/7 1/8 1/9 1/10 1/11 1/12 1/13 ...",
				"  ..."
			],
			"mathematica": [
				"Table[Denominator[Sum[1/(i + j - 1), {i, n}, {j, n}]], {n, 30}]"
			],
			"xref": [
				"Cf. A091342, A098118, A111876, A082687, A086881, A005249, A001008, A002805.",
				"Numerator is A117731(n)."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Alexander Adamchuk_, Apr 11 2006",
			"references": 4,
			"revision": 21,
			"time": "2021-10-11T18:58:56-04:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}