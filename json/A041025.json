{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A041025",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 41025,
			"data": "1,8,65,528,4289,34840,283009,2298912,18674305,151693352,1232221121,10009462320,81307919681,660472819768,5365090477825,43581196642368,354014663616769,2875698505576520,23359602708228929,189752520171407952,1541379764079492545",
			"name": "Denominators of continued fraction convergents to sqrt(17).",
			"comment": [
				"a(2*n+1) with b(2*n+1) := A041024(2*n+1), n\u003e=0, give all (positive integer) solutions to Pell equation b^2 - 17*a^2 = +1, a(2*n) with b(2*n) := A041024(2*n), n\u003e=0, give all (positive integer) solutions to Pell equation b^2 - 17*a^2 = -1 (cf. Emerson reference).",
				"Bisection: a(2*n)= T(2*n+1,sqrt(17))/sqrt(17)= A078988(n), n\u003e=0 and a(2*n+1)=8*S(n-1,66),n\u003e=0, with T(n,x), resp. S(n,x), Chebyshev's polynomials of the first, resp. second kind. S(-1,x)=0. See A053120, resp. A049310. - _Wolfdieter Lang_, Jan 10 2003",
				"Sqrt(17) = 8/2 + 8/65 + 8/(65*4289) + 8/(4289*283009) + ... . - _Gary W. Adamson_, Dec 26 2007",
				"a(p) == ((p-1)/2)) mod p for odd primes p. - _Gary W. Adamson_, Feb 22 2009",
				"For positive n, a(n) equals the permanent of the n X n tridiagonal matrix with 8's along the main diagonal and 1's along the superdiagonal and the subdiagonal. - _John M. Campbell_, Jul 08 2011",
				"De Moivre's formula: a(n) = (r^n-s^n)/(r-s), for r\u003es, gives sequences with integers if r and s are conjugates. With r=4+sqrt(17) and s=4-sqrt(17), a(n+1)/a(n) converges to r=4+sqrt(17). - _Sture Sjöstedt_, Nov 11 2011",
				"a(n) equals the number of words of length n on alphabet {0,1,...,8} avoiding runs of zeros of odd lengths. - _Milan Janjic_, Jan 28 2015"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A041025/b041025.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"D. Birmajer, J. B. Gil, M. D. Weiner, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL19/Gil/gil6.html\"\u003en the Enumeration of Restricted Words over a Finite Alphabet \u003c/a\u003e, J. Int. Seq. 19 (2016) # 16.1.3, Example 8",
				"E. I. Emerson, \u003ca href=\"http://www.fq.math.ca/Scanned/7-3/emerson.pdf\"\u003eRecurrent Sequences in the Equation DQ^2=R^2+N\u003c/a\u003e, Fib. Quart., 7 (1969), pp. 231-242, Thm. 1, p. 233.",
				"Sergio Falcón and Ángel Plaza, \u003ca href=\"http://dx.doi.org/10.1016/j.chaos.2006.10.022\"\u003eThe k-Fibonacci sequence and the Pascal 2-triangle\u003c/a\u003e, Chaos, Solitons \u0026 Fractals 2007; 33(1): 38-49.",
				"S. Falcón \u0026 Á. Plaza, \u003ca href=\"http://dx.doi.org/10.1016/j.chaos.2007.03.007\"\u003eOn k-Fibonacci sequences and polynomials and their derivatives\u003c/a\u003e, Chaos, Solitons \u0026 Fractals (2007).",
				"M. Janjic, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL18/Janjic/janjic63.html\"\u003eOn Linear Recurrence Equations Arising from Compositions of Positive Integers\u003c/a\u003e, Journal of Integer Sequences, Vol. 18 (2015), Article 15.4.7.",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"Kai Wang, \u003ca href=\"https://www.researchgate.net/publication/339487198_On_k-Fibonacci_Sequences_And_Infinite_Series_List_of_Results_and_Examples\"\u003eOn k-Fibonacci Sequences And Infinite Series List of Results and Examples\u003c/a\u003e, 2020.",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials\u003c/a\u003e.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (8,1)."
			],
			"formula": [
				"G.f.: 1/(1 - 8*x - x^2).",
				"a(n) = ((-i)^n)*S(n, 8*i), with S(n, x) := U(n, x/2) Chebyshev's polynomials of the second kind and i^2 = -1. See A049310.",
				"a(n) = F(n, 8), the n-th Fibonacci polynomial evaluated at x=8. - _T. D. Noe_, Jan 19 2006",
				"a(n) = ((4 + sqrt(17))^n - (4 - sqrt(17))^n)/(2*sqrt(17)); a(n) = sum_{i=0..floor((n-1)/2)} binomial(n-1-i,i)*8^(n-1-2i). - _Sergio Falcon_, Sep 24 2007",
				"Let T = the 2 X 2 matrix [0, 1; 1, 8]. Then T^n * [1, 0] = [a(n-2), a(n-1)]. - _Gary W. Adamson_, Dec 26 2007",
				"a(n) = 8*a(n-1) + a(n-2), n\u003e1; a(0)=1, a(1)=8. - _Philippe Deléham_, Nov 20 2008",
				"sum_{n\u003e=0} (-1)^n/(a(n)*a(n+1)) = sqrt(17)-4. - _Vladimir Shevelev_, Feb 23 2013"
			],
			"mathematica": [
				"a=0;lst={};s=0;Do[a=s-(a-1);AppendTo[lst,a];s+=a*8,{n,3*4!}];lst (* _Vladimir Joseph Stephan Orlovsky_, Oct 27 2009 *)",
				"CoefficientList[Series[1/(-z^2 - 8 z + 1), {z, 0, 200}], z] (* _Vladimir Joseph Stephan Orlovsky_, Jun 23 2011 *)",
				"Denominator[Convergents[Sqrt[17],30]] (* _Harvey P. Dale_, Aug 15 2011 *)",
				"LinearRecurrence[{8,1}, {1,8}, 50] (* _Sture Sjöstedt_, Nov 11 2011 *)"
			],
			"program": [
				"(Sage) [lucas_number1(n,8,-1) for n in range(1, 20)] # _Zerinvary Lajos_, Apr 25 2009",
				"(MAGMA) I:=[1, 8]; [n le 2 select I[n] else 8*Self(n-1)+Self(n-2): n in [1..25]]; // _Vincenzo Librandi_, Feb 23 2013",
				"(PARI) Vec(1/(1-8*x-x^2)+O(x^99)) \\\\ _Charles R Greathouse IV_, Dec 09 2014"
			],
			"xref": [
				"Cf. A041024, A000045, A000129, A006190, A001076, A052918, A005668, A054413, A243399."
			],
			"keyword": "nonn,cofr,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 23,
			"revision": 90,
			"time": "2020-06-15T22:57:25-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}