{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A143685",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 143685,
			"data": "1,1,1,1,11,1,1,21,21,1,1,31,141,31,1,1,41,361,361,41,1,1,51,681,1991,681,51,1,1,61,1101,5921,5921,1101,61,1,1,71,1621,13151,29761,13151,1621,71,1,1,81,2241,24681,96201,96201,24681,2241,81,1,1,91,2961,41511,239241,460251,239241,41511,2961,91,1",
			"name": "Pascal-(1,9,1) array.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A143685/b143685.txt\"\u003eAntidiagonal rows n = 0..50, flattened\u003c/a\u003e"
			],
			"formula": [
				"Square array: T(n, k) = T(n, k-1) + 9*T(n-1, k-1) + T(n-1, k) with T(n, 0) = T(0, k) = 1.",
				"Number triangle: T(n,k) = Sum_{j=0..n-k} binomial(n-k,j)*binomial(k,j)*10^j.",
				"Riordan array (1/(1-x), x*(1+9*x)/(1-x)).",
				"T(n, k) = Hypergeometric2F1([-k, k-n], [1], 10). - _Jean-François Alcover_, May 24 2013",
				"Sum_{k=0..n} T(n, k) = A002534(n+1). - _G. C. Greubel_, May 29 2021"
			],
			"example": [
				"Square array begins as:",
				"  1,  1,    1,     1,      1,       1,        1, ... A000012;",
				"  1, 11,   21,    31,     41,      51,       61, ... A017281;",
				"  1, 21,  141,   361,    681,    1101,     1621, ...",
				"  1, 31,  361,  1991,   5921,   13151,    24681, ...",
				"  1, 41,  681,  5921,  29761,   96201,   239241, ...",
				"  1, 51, 1101, 13151,  96201,  460251,  1565301, ...",
				"  1, 61, 1621, 24681, 239241, 1565301,  7272861, ...",
				"Antidiagonal triangle begins as:",
				"  1;",
				"  1,  1;",
				"  1, 11,    1;",
				"  1, 21,   21,     1;",
				"  1, 31,  141,    31,     1;",
				"  1, 41,  361,   361,    41,     1;",
				"  1, 51,  681,  1991,   681,    51,    1;",
				"  1, 61, 1101,  5921,  5921,  1101,   61,  1;",
				"  1, 71, 1621, 13151, 29761, 13151, 1621, 71, 1;"
			],
			"mathematica": [
				"Table[Hypergeometric2F1[-k, k-n, 1, 10], {n,0,12}, {k,0,n}]//Flatten (* _Jean-François Alcover_, May 24 2013 *)"
			],
			"program": [
				"(MAGMA)",
				"A143685:= func\u003c n,k,q | (\u0026+[Binomial(k, j)*Binomial(n-j, k)*q^j: j in [0..n-k]]) \u003e;",
				"[A143685(n,k,9): k in [0..n], n in [0..12]]; // _G. C. Greubel_, May 29 2021",
				"(Sage) flatten([[hypergeometric([-k, k-n], [1], 10).simplify() for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, May 29 2021"
			],
			"xref": [
				"Cf. A002534, A143680, A143682.",
				"Pascal (1,m,1) array: A123562 (m = -3), A098593 (m = -2), A000012 (m = -1), A007318 (m = 0), A008288 (m = 1), A081577 (m = 2), A081578 (m = 3), A081579 (m = 4), A081580 (m = 5), A081581 (m = 6), A081582 (m = 7), A143683 (m = 8), this sequence (m = 9)."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "0,5",
			"author": "_Paul Barry_, Aug 28 2008",
			"references": 1,
			"revision": 9,
			"time": "2021-05-30T13:01:18-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}