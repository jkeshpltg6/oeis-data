{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A207815",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 207815,
			"data": "1,-3,1,8,-6,1,-21,25,-9,1,55,-90,51,-12,1,-144,300,-234,86,-15,1,377,-954,951,-480,130,-18,1,-987,2939,-3573,2305,-855,183,-21,1,2584,-8850,12707,-10008,4740,-1386,245,-24,1,-6765,26195,-43398,40426,-23373,8715",
			"name": "Triangle of coefficients of Chebyshev's S(n,x-3) polynomials (exponents of x in increasing order).",
			"comment": [
				"Riordan array (1/(1+3*x+x^2), x/(1+3*x+x^2)).",
				"Subtriangle of the triangle given by (0, -3, 1/3, -1/3, 0, 0, 0, 0, 0, 0, 0, ...) DELTA (1, 0, 0, 0, 0, 0, 0, 0, 0, 0, ...) where DELTA is the operator defined in A084938.",
				"Diagonal sums are (-3)^n.",
				"Inverse array is A091965."
			],
			"formula": [
				"T(n,k) = (-1)^(n-k)*A125662(n,k).",
				"Recurrence: T(n,k) = (-3)*T(n-1,k) + T(n-1,k-1) - T(n-2,k).",
				"G.f.: 1/(1+3*x+x^2-y*x)."
			],
			"example": [
				"Triangle begins:",
				"      1;",
				"     -3,     1;",
				"      8,    -6,      1;",
				"    -21,    25,     -9,      1;",
				"     55,   -90,     51,    -12,      1;",
				"   -144,   300,   -234,     86,    -15,     1;",
				"    377,  -954,    951,   -480,    130,   -18,     1;",
				"   -987,  2939,  -3573,   2305,   -855,   183,   -21,   1;",
				"   2584, -8850,  12707, -10008,   4740, -1386,   245, -24,   1;",
				"  -6765, 26195, -43398,  40426, -23373,  8715, -2100, 316, -27, 1;",
				"Triangle (0, -3, 1/3, -1/3, 0, 0, ...) DELTA (1, 0, 0, 0, ...) begins:",
				"  1;",
				"  0,    1;",
				"  0,   -3,   1;",
				"  0,    8,  -6,    1;",
				"  0,  -21,  25,   -9,   1;",
				"  0,   55, -90,   51, -12,   1;",
				"  0, -144, 300, -234,  86, -15, 1;",
				"  ..."
			],
			"mathematica": [
				"T[_?Negative, _] = 0; T[0, 0] = 1; T[0, _] = 0; T[n_, n_] = 1; T[n_, k_] := T[n, k] = T[n - 1, k - 1] - T[n - 2, k] - 3 T[n - 1, k];",
				"Table[T[n, k], {n, 0, 9}, {k, 0, n}] (* _Jean-François Alcover_, Jun 22 2018 *)"
			],
			"program": [
				"(Sage)",
				"@CachedFunction",
				"def A207815(n,k):",
				"    if n\u003c 0: return 0",
				"    if n==0: return 1 if k == 0 else 0",
				"    return A207815(n-1,k-1)-A207815(n-2,k)-3*A207815(n-1,k)",
				"for n in (0..9): [A207815(n,k) for k in (0..n)] # _Peter Luschny_, Nov 20 2012",
				"(PARI) row(n) = Vecrev(subst(polchebyshev(n,2,x/2), x, x-3))",
				"tabf(nn) = for (n=0, nn, print(row(n))); \\\\ _Michel Marcus_, Jun 22 2018"
			],
			"xref": [
				"Cf. Chebyshev's S(n,x+k) polynomials: A207824 (k = 5), A207823 (k = 4), A125662 (k = 3), A078812 (k = 2), A101950 (k = 1), A049310 (k = 0), A104562 (k = -1), A053122 (k = -2), A207815 (k = -3), A159764 (k = -4), A123967 (k = -5)."
			],
			"keyword": "easy,sign,tabl",
			"offset": "0,2",
			"author": "_Philippe Deléham_, Feb 20 2012",
			"ext": [
				"T(8,0) corrected by _Jean-François Alcover_, Jun 22 2018"
			],
			"references": 8,
			"revision": 23,
			"time": "2018-06-22T23:26:31-04:00",
			"created": "2012-02-21T11:26:42-05:00"
		}
	]
}