{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003132",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3132,
			"id": "M3355",
			"data": "0,1,4,9,16,25,36,49,64,81,1,2,5,10,17,26,37,50,65,82,4,5,8,13,20,29,40,53,68,85,9,10,13,18,25,34,45,58,73,90,16,17,20,25,32,41,52,65,80,97,25,26,29,34,41,50,61,74,89,106,36,37,40,45,52,61,72,85,100,117,49",
			"name": "Sum of squares of digits of n.",
			"comment": [
				"It is easy to show that a(n) \u003c 81*(log(n)+1) [log = base 10]. - _Stefan Steinerberger_, Mar 25 2006",
				"It is known that a(0)=0 and a(1)=1 are the only fixed points of this map. For more information about iterations of this map, see A007770, A099645 and A000216 ff. - _M. F. Hasler_, May 24 2009"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence).",
				"Hugo Steinhaus, One Hundred Problems in Elementary Mathematics, Dover New York, 1979, republication of English translation of Sto Zada´n, Basic Books, New York, 1964. Chapter I.2, An interesting property of numbers, pp. 11-12 (available on Google Books)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A003132/b003132.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"J.-P. Allouche and J. Shallit, \u003ca href=\"http://dx.doi.org/10.1016/S0304-3975(03)00090-2\"\u003eThe ring of k-regular sequences, II\u003c/a\u003e, Theoret. Computer Sci., 307 (2003), 3-29; \u003ca href=\"http://www.math.jussieu.fr/~allouche/kreg2.ps\"\u003e[preprint from author's web page, PostScript format]\u003c/a\u003e.",
				"Arthur Porges, \u003ca href=\"http://www.jstor.org/stable/2304639\"\u003eA set of eight numbers\u003c/a\u003e, Amer. Math. Monthly 52 (1945), 379-382.",
				"B. M. Stewart, \u003ca href=\"http://dx.doi.org/10.4153/CJM-1960-032-9\"\u003eSums of functions of digits\u003c/a\u003e, Canad. J. Math., 12 (1960), 374-389.",
				"Robert Walker, \u003ca href=\"http://robertinventor.com/ftswiki/Self_Similar_Sloth_Canon_Number_Sequences\"\u003eSelf Similar Sloth Canon Number Sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Coi#Colombian\"\u003eIndex entries for Colombian or self numbers and related sequences\u003c/a\u003e"
			],
			"formula": [
				"a(n) = n^2-20*n*floor(n/10)+81*sum{k\u003e0,floor(n/10^k)^2}+20*sum{k\u003e0,floor(n/10^k)*(floor(n/10^k)-floor(n/10^(k+1)))}. - _Hieronymus Fischer_, Jun 17 2007",
				"a(10n+k) = a(n)+k^2, 0 \u003c= k \u003c 10. - _Hieronymus Fischer_, Jun 17 2007",
				"a(n) = A007953(A048377(n)) - A007953(n). - _Reinhard Zumkeller_, Jul 10 2011"
			],
			"maple": [
				"A003132 := proc(n) local d; add(d^2,d=convert(n,base,10)) ; end proc: # _R. J. Mathar_, Oct 16 2010"
			],
			"mathematica": [
				"Table[Sum[DigitCount[n][[i]]*i^2, {i, 1, 9}], {n, 0, 40}] (* _Stefan Steinerberger_, Mar 25 2006 *)",
				"Total/@(IntegerDigits[Range[0,80]]^2) (* _Harvey P. Dale_, Jun 20 2011 *)"
			],
			"program": [
				"(PARI) A003132(n)=norml2(digits(n)) \\\\ _M. F. Hasler_, May 24 2009, updated Apr 12 2015",
				"(Haskell)",
				"a003132 0 = 0",
				"a003132 x = d ^ 2 + a003132 x' where (x', d) = divMod x 10",
				"-- _Reinhard Zumkeller_, May 10 2015, Aug 07 2012, Jul 10 2011",
				"(MAGMA) [0] cat [\u0026+[d^2: d in Intseq(n)]: n in [1..80]]; // _Bruno Berselli_, Feb 01 2013",
				"(Python)",
				"def A003132(n): return sum(int(d)**2 for d in str(n)) # _Chai Wah Wu_, Apr 02 2021"
			],
			"xref": [
				"Cf. A052034, A052035.",
				"Cf. A007953, A055017, A076313, A076314.",
				"Concerning iterations of this map, see A003621, A039943, A099645, A031176, A007770, A000216 (starting with 2), A000218 (starting with 3), A080709 (starting with 4, this is the only nontrivial limit cycle), A000221 (starting with 5), A008460 (starting with 6), A008462 (starting with 8), A008463 (starting with 9), A139566 (starting with 15), A122065 (starting with 74169). - _M. F. Hasler_, May 24 2009",
				"Cf. A080151, A051885 (record values and where they occur).",
				"Cf. A257588, A332919."
			],
			"keyword": "nonn,easy,look,base,nice,changed",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Stefan Steinerberger_, Mar 25 2006",
				"Terms checked using the given PARI code, _M. F. Hasler_, May 24 2009",
				"Replaced the Maple program with a version which works also for arguments with \u003e2 digits, _R. J. Mathar_, Oct 16 2010",
				"Added ref to Porges. Steinhaus also treated iterations of this function in his Polish book Sto zada´n, but I don't have access to it. - _Don Knuth_, Sep 07 2015"
			],
			"references": 104,
			"revision": 78,
			"time": "2022-01-03T23:24:17-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}