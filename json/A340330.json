{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A340330",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 340330,
			"data": "1,1,-23,3393,-1917839,4084439425,-33891092958375,1115079034134766465,-146344823494402837552287,76757909901662702029167652609,-160993058320324275979629230976271287,1350558570374627603824657269225692538358977,-45317735199913991775812306272864857418386056626479",
			"name": "G.f. A(x) = Sum_{n\u003e=0} a(n)*x^(2*n) satisfies: 1 = Sum_{n\u003e=0} x^n/(1 + 2^n*x*A(x)^n).",
			"comment": [
				"Note that the g.f. is an even function, so only the coefficients of even powers of x are shown.",
				"The g.f. A(x) of this sequence is motivated by the following identity:",
				"Sum_{n\u003e=0} p^n/(1 - q*r^n) = Sum_{n\u003e=0} q^n/(1 - p*r^n) = Sum_{n\u003e=0} p^n*q^n*r^(n^2)*(1 - p*q*r^(2*n))/((1 - p*r^n)*(1 - q*r^n)) ;",
				"here, p = x, q = -x, and r = 2*A(x), where A(x) causes the sum to equal unity."
			],
			"formula": [
				"The g.f. A(x) = Sum_{n\u003e=0} a(n)*x^(2*n) satisfies the following relations.",
				"(1) 1 = Sum_{n\u003e=0} x^n/(1 + 2^n*x*A(x)^n).",
				"(2) 1 = Sum_{n\u003e=0} (-x)^n/(1 - 2^n*x*A(x)^n).",
				"(3) 1 = Sum_{n\u003e=0} (-1)^n * x^(2*n) * 2^(n^2) * A(x)^(n^2) * (1 + x^2*4^n*A(x)^(2*n)) / (1 - x^2*4^n*A(x)^(2*n))."
			],
			"example": [
				"G.f.: A(x) = 1 + x^2 - 23*x^4 + 3393*x^6 - 1917839*x^8 + 4084439425*x^10 - 33891092958375*x^12 + 1115079034134766465*x^14 - 146344823494402837552287*x^16 + ...",
				"where",
				"1 = 1/(1 + x) + x/(1 + 2*x*A(x)) + x^2/(1 + 2^2*x*A(x)^2) + x^3/(1 + 2^3*x*A(x)^3) + x^4/(1 + 2^4*x*A(x)^4) + x^5/(1 + 2^5*x*A(x)^5) + ...",
				"also",
				"1 = 1/(1 - x) - x/(1 - 2*x*A(x)) + x^2/(1 - 2^2*x*A(x)^2) - x^3/(1 - 2^3*x*A(x)^3) + x^4/(1 - 2^4*x*A(x)^4) - x^5/(1 - 2^5*x*A(x)^5) + ..."
			],
			"program": [
				"(PARI) {a(n) = my(A=1,V=[1]); for(i=0,2*n, V=concat(V,0); A = Ser(concat(V,0)); V[#V] = polcoeff( sum(m=0,#V+1, x^m/(1 + 2^m*x*A^m) ),#V+1)/2 ); polcoeff(A,2*n)}",
				"for(n=0,20,print1(a(n),\", \"))"
			],
			"xref": [
				"Cf. A340331, A340356."
			],
			"keyword": "sign",
			"offset": "0,3",
			"author": "_Paul D. Hanna_, Jan 08 2021",
			"references": 1,
			"revision": 8,
			"time": "2021-02-19T11:58:25-05:00",
			"created": "2021-01-10T11:50:34-05:00"
		}
	]
}