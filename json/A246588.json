{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A246588",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 246588,
			"data": "1,1,1,1,1,1,1,2,1,1,1,1,1,1,2,1,1,1,1,1,1,1,1,2,1,1,1,1,2,2,1,2,1,1,1,1,1,1,1,2,1,1,1,1,1,1,2,1,1,1,1,1,1,1,1,2,2,2,2,2,1,1,2,2,1,1,1,1,1,1,1,2,1,1,1,1,1,1,2,1,1,1,1,1,1,1,1,2,1,1,1,1,2,2,1,2,1,1,1,1,1",
			"name": "Run Length Transform of S(n) = wt(n) = 0,1,1,2,1,2,2,3,1,... (cf. A000120).",
			"comment": [
				"The Run Length Transform of a sequence {S(n), n\u003e=0} is defined to be the sequence {T(n), n\u003e=0} given by T(n) = Product_i S(i), where i runs through the lengths of runs of 1's in the binary expansion of n. E.g. 19 is 10011 in binary, which has two runs of 1's, of lengths 1 and 2. So T(19) = S(1)*S(2). T(0)=1 (the empty product)."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A246588/b246588.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e"
			],
			"maple": [
				"A000120 := proc(n) local w, m, i; w := 0; m :=n; while m \u003e 0 do i := m mod 2; w := w+i; m := (m-i)/2; od; w; end: wt := A000120;",
				"ans:=[];",
				"for n from 0 to 100 do lis:=[]; t1:=convert(n, base, 2); L1:=nops(t1); out1:=1; c:=0;",
				"for i from 1 to L1 do",
				"   if out1 = 1 and t1[i] = 1 then out1:=0; c:=c+1;",
				"   elif out1 = 0 and t1[i] = 1 then c:=c+1;",
				"   elif out1 = 1 and t1[i] = 0 then c:=c;",
				"   elif out1 = 0 and t1[i] = 0 then lis:=[c, op(lis)]; out1:=1; c:=0;",
				"   fi;",
				"   if i = L1 and c\u003e0 then lis:=[c, op(lis)]; fi;",
				"                   od:",
				"a:=mul(wt(i), i in lis);",
				"ans:=[op(ans), a];",
				"od:",
				"ans;"
			],
			"mathematica": [
				"f[n_] := DigitCount[n, 2, 1]; Table[Times @@ (f[Length[#]]\u0026)  /@ Select[ Split[ IntegerDigits[n, 2]], #[[1]] == 1\u0026], {n, 0, 100}] (* _Jean-François Alcover_, Jul 11 2017 *)"
			],
			"program": [
				"(Haskell)",
				"import Data.List (group)",
				"a246588 = product . map (a000120 . length) .",
				"          filter ((== 1) . head) . group . a030308_row",
				"-- _Reinhard Zumkeller_, Feb 13 2015, Sep 05 2014",
				"(Python)",
				"from operator import mul",
				"from functools import reduce",
				"from re import split",
				"def A246588(n):",
				"    return reduce(mul,(bin(len(d)).count('1') for d in split('0+',bin(n)[2:]) if d)) if n \u003e 0 else 1 # _Chai Wah Wu_, Sep 07 2014",
				"(Sage) # uses[RLT from A246660]",
				"A246588_list = lambda len: RLT(lambda n: sum(Integer(n).digits(2)), len)",
				"A246588_list(88) # _Peter Luschny_, Sep 07 2014"
			],
			"xref": [
				"Cf. A000120.",
				"Cf. A167489, A030308.",
				"Run Length Transforms of other sequences: A071053, A227349, A246595, A246596, A246660, A246661, A246674."
			],
			"keyword": "nonn",
			"offset": "0,8",
			"author": "_N. J. A. Sloane_, Sep 05 2014",
			"references": 11,
			"revision": 42,
			"time": "2020-04-06T05:38:06-04:00",
			"created": "2014-09-05T18:16:42-04:00"
		}
	]
}