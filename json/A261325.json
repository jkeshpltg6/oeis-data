{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A261325",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 261325,
			"data": "1,-3,8,-18,38,-75,140,-252,439,-744,1232,-1998,3182,-4986,7700,-11736,17673,-26322,38808,-56682,82070,-117867,167996,-237744,334202,-466836,648224,-895014,1229148,-1679436,2283568,-3090672,4164578,-5587941,7467464,-9940482",
			"name": "Expansion of f(x^3, x^3) * f(x, x^5) / f(x, x)^2 in powers of x where f(,) is Ramanujan's general theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A261325/b261325.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of f(-x^2) * f(x^3) * f(-x^6) / f(x)^3 in powers of x where f() is a Ramanujan theta function.",
				"Expansion of q^(-1/3) * eta(q)^3 * eta(q^4)^3 *  eta(q^6)^4 / (eta(q^2)^8 * eta(q^3) * eta(q^12)) in powers of q.",
				"Euler transform of period 12 sequence [ -3, 5, -2, 2, -3, 2, -3, 2, -2, 5, -3, 0, ...].",
				"a(n) = A187153(3*n + 1) = A213265(3*n + 1) = A233670(3*n + 1) = A233672(3*n + 1).",
				"2 * a(n) = A233673(3*n + 1) = - A260215(3*n + 1). a(2*n + 1) = -3 * A233698(n).",
				"a(n) ~ (-1)^n * exp(2*Pi*sqrt(n/3)) / (4*3^(5/4)*n^(3/4)). - _Vaclav Kotesovec_, Nov 16 2017"
			],
			"example": [
				"G.f. = 1 - 3*x + 8*x^2 - 18*x^3 + 38*x^4 - 75*x^5 + 140*x^6 - 252*x^7 + ...",
				"G.f. = q - 3*q^4 + 8*q^7 - 18*q^10 + 38*q^13 - 75*q^16 + 140*q^19 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ x^2] QPochhammer[ -x^3] QPochhammer[ x^6] / QPochhammer[ -x]^3, {x, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A)^3 * eta(x^4 + A)^3 *  eta(x^6 + A)^4 / (eta(x^2 + A)^8 * eta(x^3 + A) * eta(x^12 + A)), n))};"
			],
			"xref": [
				"Cf. A187153, A213265, A233670, A233672, A233673, A233698, A260215."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Aug 14 2015",
			"references": 6,
			"revision": 10,
			"time": "2021-03-12T22:24:48-05:00",
			"created": "2015-08-14T22:37:38-04:00"
		}
	]
}