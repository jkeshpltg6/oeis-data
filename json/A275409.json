{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A275409",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 275409,
			"data": "1,2,1,0,2,2,2,1,1,1,0,3,1,2,1,1,3,2,5,3,4,3,1,1,1,1,2,2,2,4,2,2,4,2,7,3,1,6,2,1,2,3,4,5,1,1,3,5,3,3,4,3,7,3,2,4,3,4,4,3,1,4,5,3,6,4,4,4,5,7,7,3,6,5,5,4,3,11,2,2,4",
			"name": "Number of ordered ways to write n as 2*w^2 + x^2 + y^2 + z^2 with w + x + 2*y + 4*z a square, where w,x,y,z are nonnegative integers.",
			"comment": [
				"Conjecture: (i) a(n) \u003e 0 except for n = 3, 10, and a(n) = 1 only for n = 0, 2, 7, 8, 9, 12, 14, 15, 22, 23, 24, 25, 36, 39, 44, 45, 60, 87, 98, 106, 110, 111, 183.",
				"(ii) Any natural number can be written as x^2 + y^2 + z^2 + 2*w^2 with x,y,z,w nonnegative integers such that x + 2*y + 3*z - 3*w is a square.",
				"(iii) For each triple (a,b,c) = (1,2,1), (1,2,3), (1,3,1), (2,4,1), (2,4,2), (2,4,3), (2,4,4), (2,4,8), (8,9,5), any natural number can be written as x^2 + y^2 + z^2 + 2*w^2 with x,y,z,w nonnegative integers such that a*x + b*y - c*z is a square.",
				"(iv) Any natural number can be written as x^2 + y^2 + z^2 + 2*w^2 with x,y,z,w nonnegative integers such that x + 2*y - 2*z is twice a nonnegative cube. Also, each natural number can be written as x^2 + y^2 + z^2 + 2*w^3 with x,y,z,w nonnegative integers such that x + 3*y - z is a square.",
				"See also A275344 and A275301 for related conjectures. We are able to show that each natural number can be written as x^2 + y^2 + z^2 + 2*w^2 with x,y,z,w integers such that x + y + z = t^2 for some t = 0, 1, 2."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A275409/b275409.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1604.06723\"\u003eRefining Lagrange's four-square theorem\u003c/a\u003e, arXiv:1604.06723 [math.GM], 2016."
			],
			"example": [
				"a(2) = 1 since 2 = 2*1^2 + 0^2 + 0^2 + 0^2 with 1 + 0 + 2*0 + 4*0 = 1^2.",
				"a(7) = 1 since 7 = 2*1^2 + 0^2 + 2^2 + 1^2 with 1 + 0 + 2*2 + 4*1 = 3^2.",
				"a(8) = 1 since 8 = 2*1^2 + 2^2 + 1^2 + 1^2 with 1 + 2 + 2*1 + 4*1 = 3^2.",
				"a(9) = 1 since 9 = 2*2^2 + 0^2 + 1^2 + 0^2 with 2 + 0 + 2*1 + 4*0 = 2^2.",
				"a(12) = 1 since 12 = 2*2^2 + 2^2 + 0^2 + 0^2 with 2 + 2 + 2*0 + 4*0 = 2^2.",
				"a(14) = 1 since 14 = 2*0^2 + 2^2 + 1^2 + 3^2 with 0 + 2 + 2*1 + 4*3 = 4^2.",
				"a(15) = 1 since 15 = 2*1^2 + 2^2 + 3^2 + 0^2 with 1 + 2 + 2*3 + 4*0 = 3^2.",
				"a(22) = 1 since 22 = 2*1^2 + 4^2 + 2^2 + 0^2 with 1 + 4 + 2*2 + 4*0 = 3^2.",
				"a(23) = 1 since 23 = 2*3^2 + 2^2 + 0^2 + 1^2 with 3 + 2 + 2*0 + 4*1 = 3^2.",
				"a(24) = 1 since 24 = 2*0^2 + 4^2 + 2^2 + 2^2 with 0 + 4 + 2*2 + 4*2 = 4^2.",
				"a(25) = 1 since 25 = 2*0^2 + 4^2 + 0^2 + 3^2 with 0 + 4 + 2*0 + 4*3 = 4^2.",
				"a(36) = 1 since 36 = 2*3^2 + 1^2 + 4^2 + 1^2 with 3 + 1 + 2*4 + 4*1 = 4^2.",
				"a(39) = 1 since 39 = 2*1^2 + 6^2 + 1^2 + 0^2 with 1 + 6 + 2*1 + 4*0 = 3^2.",
				"a(44) = 1 since 44 = 2*3^2 + 0^2 + 1^2 + 5^2 with 3 + 0 + 2*1 + 4*5 = 5^2.",
				"a(45) = 1 since 45 = 2*0^2 + 5^2 + 2^2 + 4^2 with 0 + 5 + 2*2 + 4*4 = 5^2.",
				"a(60) = 1 since 60 = 2*2^2 + 6^2 + 4^2 + 0^2 with 2 + 6 + 2*4 + 4*0 = 4^2.",
				"a(87) = 1 since 87 = 2*3^2 + 2^2 + 8^2 + 1^2 with 3 + 2 + 2*8 + 4*1 = 5^2.",
				"a(98) = 1 since 98 = 2*4^2 + 1^2 + 8^2 + 1^2 with 4 + 1 + 2*8 + 4*1 = 5^2.",
				"a(106) = 1 since 106 = 2*2^2 + 8^2 + 3^2 + 5^2 with 2 + 8 + 2*3 + 4*5 = 6^2.",
				"a(110) = 1 since 110 = 2*6^2 + 5^2 + 3^2 + 2^2 with 6 + 5 + 2*3 + 4*2 = 5^2.",
				"a(111) = 1 since 111 = 2*5^2 + 3^2 + 6^2 + 4^2 with 5 + 3 + 2*6 + 4*4 = 6^2.",
				"a(183) = 1 since 183 = 2*3^2 + 10^2 + 4^2 + 7^2 with 3 + 10 + 2*4 + 4*7 = 7^2."
			],
			"mathematica": [
				"SQ[n_]:=SQ[n]=IntegerQ[Sqrt[n]]",
				"Do[r=0;Do[If[SQ[n-2*w^2-x^2-y^2]\u0026\u0026SQ[w+x+2y+4*Sqrt[n-2*w^2-x^2-y^2]],r=r+1],{w,0,Sqrt[n/2]},{x,0,Sqrt[n-2*w^2]},{y,0,Sqrt[n-2*w^2-x^2]}];Print[n,\" \",r];Continue,{n,0,80}]"
			],
			"xref": [
				"Cf. A000290, A271518, A275297, A275301, A275344."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Zhi-Wei Sun_, Jul 26 2016",
			"references": 1,
			"revision": 10,
			"time": "2016-07-27T03:13:16-04:00",
			"created": "2016-07-27T03:13:16-04:00"
		}
	]
}