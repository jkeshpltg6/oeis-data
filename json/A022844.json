{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A022844",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 22844,
			"data": "0,3,6,9,12,15,18,21,25,28,31,34,37,40,43,47,50,53,56,59,62,65,69,72,75,78,81,84,87,91,94,97,100,103,106,109,113,116,119,122,125,128,131,135,138,141,144,147,150,153,157,160,163,166,169,172,175,179,182,185,188,191,194",
			"name": "a(n) = floor(n*Pi).",
			"comment": [
				"Beatty sequence for Pi.",
				"Differs from A127451 first at a(57). - _L. Edson Jeffery_, Dec 01 2013",
				"These are the nonnegative integers m satisfying sin(m)*sin(m+1) \u003c= 0.  In general, the Beatty sequence of an irrational number r \u003e 1 consists of the numbers m satisfying sin(m*x)*sin((m+1)*x) \u003c= 0, where x = Pi/r. Thus the numbers m satisfying sin(m*x)*sin((m+1)*x) \u003e 0 form the Beatty sequence of r/(1-r).  - Clark Kimberling, Aug 21 2014",
				"This can also be stated in terms of the tangent function. These are the nonnegative integers m such that tan(m/2)*tan(m/2+1/2) \u003c= 0.  In general, the Beatty sequence of an irrational number r \u003e 1 consists of the numbers m satisfying tan(m*x/2)*tan((m+1)*x/2) \u003c= 0, where x = Pi/r.  Thus the numbers m satisfying tan(m*x/2)*tan((m+1)*x/2) \u003e 0 form the Beatty sequence of r/(1-r). - _Clark Kimberling_, Aug 22 2014"
			],
			"link": [
				"Ivan Panchenko, \u003ca href=\"/A022844/b022844.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/BeattySequence.html\"\u003eBeatty Sequence.\u003c/a\u003e",
				"\u003ca href=\"/index/Be#Beatty\"\u003eIndex entries for sequences related to Beatty sequences\u003c/a\u003e"
			],
			"formula": [
				"a(n)/n converges to Pi because |a(n)/n - Pi| = |a(n) - n*Pi|/n \u003c 1/n. - _Hieronymus Fischer_, Jan 22 2006"
			],
			"example": [
				"a(7)=21 because 7*Pi=21.9911... and a(8)=25 because 8*Pi=25.1327.... a(100000)=314159 because Pi=3.141592..."
			],
			"maple": [
				"a:=n-\u003efloor(n*Pi): seq(a(n),n=0..70); # _Muniru A Asiru_, Sep 28 2018"
			],
			"mathematica": [
				"a[n_]:=Floor[Pi*n]; (* _Vladimir Joseph Stephan Orlovsky_, Dec 12 2008 *)"
			],
			"program": [
				"(PARI) vector(80, n, n--; floor(n*Pi)) \\\\ _G. C. Greubel_, Sep 28 2018",
				"(MAGMA) R:=RieldField(10); [Floor(n*Pi(R)): n in [0..80]]; // _G. C. Greubel_, Sep 28 2018"
			],
			"xref": [
				"Cf. A000796, A054386, A038130, A108591, A127451, A140758.",
				"First differences give A063438."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Clark Kimberling_",
			"references": 38,
			"revision": 64,
			"time": "2019-09-05T10:33:15-04:00",
			"created": "1998-06-14T03:00:00-04:00"
		}
	]
}