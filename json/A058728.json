{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A058728",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 58728,
			"data": "1,0,-1,1,0,0,0,-1,1,1,-1,-1,1,0,-1,2,0,-2,2,-1,0,2,-4,0,5,-1,-4,2,1,-2,3,-3,-2,7,-5,-2,8,-6,-5,8,1,-5,2,-2,-1,12,-11,-10,21,-6,-10,13,-7,-4,11,-7,-4,14,-13,-10,33,-14,-28,32,-3,-12,18,-24,1,36,-27,-22,44,-13,-35,50,-13,-36,46,-26,-6,56,-63,-22,89,-30",
			"name": "McKay-Thompson series of class 60D for the Monster group.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A058728/b058728.txt\"\u003eTable of n, a(n) for n = -1..10000\u003c/a\u003e",
				"D. Ford, J. McKay and S. P. Norton, \u003ca href=\"http://dx.doi.org/10.1080/00927879408825127\"\u003eMore on replicable functions\u003c/a\u003e, Commun. Algebra 22, No. 13, 5175-5193 (1994).",
				"Michael Somos, \u003ca href=\"http://grail.eecs.csuohio.edu/~somos/retaprod.html\"\u003eA Remarkable eta-product Identity\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e",
				"\u003ca href=\"/index/Mat#McKay_Thompson\"\u003eIndex entries for McKay-Thompson series for Monster simple group\u003c/a\u003e"
			],
			"formula": [
				"Expansion of 1/q * (chi(-q^2) * chi(-q^30)) / (chi(-q^3) * chi(-q^5)) in powers of q where chi() is a Ramanujan theta function. - _Michael Somos_, Feb 13 2017",
				"Euler transform of a period 60 sequence. - _Michael Somos_, Feb 13 2017",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (60 t)) = g(t) where q = exp(2 Pi i t) and g() is the g.f. for A145933. - _Michael Somos_, Feb 13 2017",
				"G.f.: 1/x * Product_{k\u003e0} (1 + x^(3*k)) * (1 + x^(5*k)) / ((1 + x^(2*k)) * (1 + x^(30*k))). - _Michael Somos_, Feb 13 2017",
				"Convolution inverse of A132967. - _Michael Somos_, Feb 13 2017",
				"Expansion of eta(q^2)*eta(q^6)*eta(q^10)*eta(q^30)/(eta(q^3)*eta(q^4)* eta(q^5)*eta(q^60)) in powers of q. - _G. C. Greubel_, Jun 06 2018"
			],
			"example": [
				"T60D = 1/q - q + q^2 - q^6 + q^7 + q^8 - q^9 - q^10 + q^11 - q^13 + 2*q^14 - ..."
			],
			"mathematica": [
				"QP = QPochhammer; s = q + QP[q]*QP[q^12]*QP[q^15]*(QP[q^20]/(QP[q^3]* QP[q^4]*QP[q^5]*QP[q^60])) + O[q]^90; CoefficientList[s, q] (* _Jean-François Alcover_, Nov 15 2015, adapted from A143751 *)",
				"a[ n_] := SeriesCoefficient[ 1/q QPochhammer[ q^2, q^4] QPochhammer[ q^30, q^60] / (QPochhammer[ q^3, q^46] QPochhammer[ q^5, q^10]), {q, 0, n}]; (* _Michael Somos_, Feb 13 2017 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); n++; if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A) * eta(x^6 + A) * eta(x^10 + A) * eta(x^30 + A) / (eta(x^3 + A) * eta(x^4 + A) * eta(x^5 + A) * eta(x^60 + A)), n))}; /* _Michael Somos_, Feb 13 2017 */"
			],
			"xref": [
				"Cf. A143751, A000521, A007240, A014708, A007241, A007267, A045478, etc.",
				"Cf. A132967, A145933."
			],
			"keyword": "sign",
			"offset": "-1,16",
			"author": "_N. J. A. Sloane_, Nov 27 2000",
			"ext": [
				"More terms from _N. J. A. Sloane_, Sep 23 2009"
			],
			"references": 3,
			"revision": 37,
			"time": "2021-03-12T22:24:42-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}