{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005589",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5589,
			"id": "M2277",
			"data": "4,3,3,5,4,4,3,5,5,4,3,6,6,8,8,7,7,9,8,8,6,9,9,11,10,10,9,11,11,10,6,9,9,11,10,10,9,11,11,10,5,8,8,10,9,9,8,10,10,9,5,8,8,10,9,9,8,10,10,9,5,8,8,10,9,9,8,10,10,9,7,10,10,12,11,11,10,12,12,11,6,9,9,11",
			"name": "Number of letters in the US English name of n, excluding spaces and hyphens.",
			"comment": [
				"Diane Karloff observes (Nov 27 2007) that repeatedly applying the map k-\u003eA005589(k) to any starting value n always leads to 4 (cf. A016037, A133418).",
				"For terms beyond a(100), this sequence uses the US English style, \"one hundred one\" (not \"one hundred and one\"), and the short scale (a billion = 10^9, not 10^12). - _M. F. Hasler_, Nov 03 2013",
				"Explanation of Diane Karloff's observation above: In many languages there exists a number N, after which all numbers are written with fewer letters than the number itself. N is 4 in English, German and Bulgarian, and 11 in Russian. If in the interval [1,N] there are numbers equal to the number of their letters, then they are attractors. In English and German the only attractor is 4, in Bulgarian 3, in Russian there are two, 3 and 11. In the interval [1,N] there may also exist loops of numbers, for instance 4 and 6 in Bulgarian (6 and 4 letters respectively) or 4,5 and 6 in Russian (6,4 and 5 letters respectively). There are no loops in English, therefore the above observation is true. - _Ivan N. Ianakiev_, Sep 20 2014"
			],
			"reference": [
				"Problems Drive, Eureka, 37 (1974), 8-11 and 33.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Ely Golden, \u003ca href=\"/A005589/b005589.txt\"\u003eTable of n, a(n) for n = 0..11159\u003c/a\u003e",
				"Eureka, \u003ca href=\"/A005589/a005589.pdf\"\u003eProblems Drive\u003c/a\u003e, Eureka, 37 (1974), 8-11, 32-33, 24-27. (Annotated scanned copy)",
				"Ely Golden, \u003ca href=\"/A005589/a005589_2.java.txt\"\u003eArbitrary precision number naming program in Java\u003c/a\u003e",
				"Mathematica Stack Exchange, \u003ca href=\"http://mathematica.stackexchange.com/questions/1065/how-to-express-an-integer-number-in-english-words/1067#1067\"\u003e How to express an integer number in English words?\u003c/a\u003e",
				"Landon Curt Noll, \u003ca href=\"http://www.isthe.com/chongo/tech/math/number/number.html\"\u003eThe English Name of a Number\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Number.html\"\u003eNumber\u003c/a\u003e",
				"Robert G. Wilson v, \u003ca href=\"/A000027/a000027.txt\"\u003eEnglish names for the numbers from 0 to 11159 without spaces or hyphens\u003c/a\u003e.",
				"\u003ca href=\"/index/Lc#letters\"\u003eIndex entries for sequences related to number of letters in n\u003c/a\u003e"
			],
			"example": [
				"Note that A052360(373373) = 64 whereas a(373373) = 56."
			],
			"mathematica": [
				"inWords[n_] := Module[{r,",
				"numNames = {\"\", \"one\", \"two\", \"three\", \"four\", \"five\", \"six\", \"seven\", \"eight\", \"nine\"},",
				"teenNames = {\"ten\", \"eleven\", \"twelve\", \"thirteen\", \"fourteen\", \"fifteen\", \"sixteen\", \"seventeen\", \"eighteen\", \"nineteen\"},",
				"tensNames = {\"\", \"ten\", \"twenty\", \"thirty\", \"forty\", \"fifty\", \"sixty\", \"seventy\", \"eighty\", \"ninety\"},",
				"decimals = {\"\", \"thousand\", \"million\", \"billion\", \"trillion\", \"quadrillion\", \"quintillion\", \"sextillion\", \"septillion\", \"octillion\", \"nonillion\", \"decillion\", \"undecillion\", \"duodecillion\", \"tredecillion\", \"quattuordecillion\", \"quindecillion\", \"sexdecillion\", \"septendecillion\", \"octodecillion\", \"novemdecillion\", \"vigintillion\", \"unvigintillion\", \"duovigintillion\", \"trevigintillion\", \"quattuorvigintillion\", \"quinvigintillion\", \"sexvigintillion\", \"septenvigintillion\", \"octovigintillion\", \"novemvigintillion\", \"trigintillion\", \"untrigintillion\", \"duotrigintillion\"}},",
				"r = If[# != 0, numNames[[# + 1]] \u003c\u003e \"hundred\"",
				"(* \u003c\u003e If[#2 != 0||#3 != 0,\" and\",\"\"] *),",
				"\"\"] \u003c\u003e Switch[#2, 0, numNames[[#3 + 1]], 1, teenNames[[#3 + 1]], _, tensNames[[#2 + 1]] \u003c\u003e numNames[[#3 + 1]]] \u0026 @@@",
				"(PadLeft[ FromDigits /@ Characters@ StringReverse@#, 3] \u0026 /@ StringCases[ StringReverse@ IntegerString@ n, RegularExpression[\"\\\\d{1,3}\"]]);",
				"StringJoin@ Reverse@ MapThread[ If[# != \"\", StringJoin[##], \"\"] \u0026, {r, Take[decimals, Length@ r]} ]]; (* modified for this sequence from what is presented in the link and good to 10^102 -1 *)",
				"f[n_] := StringLength@ inWords@ n; f[0] = 4; Array[f, 84, 0]",
				"(* _Robert G. Wilson v_, Nov 04 2007 and revised Mar 31 2015, small revision by _Ivan Panchenko_, Nov 10 2019 *)",
				"a[n_] := StringLength[ StringReplace[ IntegerName[n, \"Words\"], \",\" | \" \" | \"\\[Hyphen]\" -\u003e \"\"]]; a /@ Range[0, 83] (* Mma version \u003e= 10, _Giovanni Resta_, Apr 10 2017 *)"
			],
			"program": [
				"(PARI) A005589(n, t=[10^12, #\"trillion\", 10^9, #\"billion\", 10^6, #\"million\", 1000, #\"thousand\", 100, #\"hundred\"])={ n\u003e99 \u0026\u0026 forstep( i=1, #t, 2, n\u003ct[i] \u0026\u0026 next; n=divrem(n, t[i]); n[1]\u003e999 \u0026\u0026 error(\"n \u003e= 10^\",valuation(t[1],10)+3,\" requires extended 2nd argument\"); return( A005589(n[1])+t[i+1]+if( n[2], A005589( n[2] )))); if( n\u003c20, #([\"zero\", \"one\", \"two\", \"three\", \"four\", \"five\", \"six\", \"seven\", \"eight\", \"nine\", \"ten\", \"eleven\", \"twelve\", \"thirteen\", \"fourteen\", \"fifteen\", \"sixteen\", \"seventeen\", \"eighteen\", \"nineteen\"][n+1]), #([ \"twenty\", \"thirty\", \"forty\", \"fifty\", \"sixty\", \"seventy\", \"eighty\", \"ninety\" ][n\\10-1])+if( n%10, A005589(n%10)))}  \\\\ Extension of 2nd arg to larger numbers is easy using the names listed in Mathematica section above. Only the string lengths are required, so it's easy to extend this list further without additional knowledge and without writing out the names. - _M. F. Hasler_, Jul 26 2011, minor edit on Jun 15 2021",
				"(Python)",
				"from num2words import num2words",
				"def a(n):",
				"    x = num2words(n).replace(' and ', '')",
				"    l = [chr(i) for i in range(97, 123)]",
				"    return sum(1 for i in x if i in l)",
				"print([a(n) for n in range(101)]) # _Indranil Ghosh_, Jul 05 2017"
			],
			"xref": [
				"Cf. A006944 (ordinals), A052360, A052362, A052363, A134629, A133418, A016037."
			],
			"keyword": "nonn,word,nice,easy",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Corrected and extended by Larry Reeves (larryr(AT)acm.org) and _Allan C. Wechsler_, Mar 20 2000",
				"Erroneous b-file deleted by _N. J. A. Sloane_, Sep 25 2008"
			],
			"references": 86,
			"revision": 98,
			"time": "2021-06-17T09:14:11-04:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}