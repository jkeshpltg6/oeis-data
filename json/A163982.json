{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A163982",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 163982,
			"data": "-2,-1,1,2,-5,-16,61,272,-1385,-7936,50521,353792,-2702765,-22368256,199360981,1903757312,-19391512145,-209865342976,2404879675441,29088885112832,-370371188237525,-4951498053124096,69348874393137901",
			"name": "Real part of the coefficient [x^n] of the expansion of (1+i)/(1-i*exp(x)) - 1 multiplied by 2*n!, where i is the imaginary unit.",
			"comment": [
				"The sequence is a signed variant of A163747 and starts with a two instead of a zero.",
				"From _Paul Curtz_, Mar 20 2013: (Start)",
				"-a(n) and successive differences are:",
				"    2,    1,    -1,     -2,      5,     16,    -61, -272;",
				"   -1,   -2,    -1,      7,     11,    -77,   -211, 1657, ...",
				"   -1,    1,     8,      4,    -88,   -134,   1868, 4894, ...",
				"    2,    7,    -4,    -92,    -46,    -46,   2002, 3026, ...",
				"    5,  -11,   -88,     46,   2048,   1024, -72928, ...",
				"  -16,  -77,   134,   2002,  -1024, -73952, -36976, ...",
				"  -61,  211,  1868,  -3026, -72928, ...",
				"  272, 1657, -4894, -69902, ...",
				"This is an autosequence: The inverse binomial transform (left column of the array of differences) is the signed sequence. The main diagonal 2, -2, 8, -92, ... doubles the entries of the first upper diagonal 1, -1, 4, -46, ... = A099023(n).",
				"Sum of the antidiagonals: 2, 0, -4, 0, 32, ... = 2*A155585(n+1). (End)"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A163982/b163982.txt\"\u003eTable of n, a(n) for n = 0..480\u003c/a\u003e",
				"Toufik Mansour, Howard Skogman, Rebecca Smith, \u003ca href=\"https://arxiv.org/abs/1808.04199\"\u003ePassing through a stack k times with reversals\u003c/a\u003e, arXiv:1808.04199 [math.CO], 2018."
			],
			"formula": [
				"Let ((1 + i)/(1 - i*exp(t)) - 1) = a(n) + I*b(n); abs(a(n)) = abs(b(n)).",
				"a(n) = -2^n*(E_{n}(1/2) + E_{n}(1)), E_{n}(x) Euler polynomial. - _Peter Luschny_, Nov 25 2010",
				"E.g.f.: -(1/cosh(x) + tanh(x)) - 1. - _Sergei N. Gladkovskii_, Dec 11 2013",
				"G.f.: -2 - x/W(0), where W(k) = 1 + x + (4*k+3)*(k+1)*x^2 /( 1 + (4*k+5)*(k+1)*x^2 /W(k+1) ); (continued fraction). - _Sergei N. Gladkovskii_, Jan 22 2015",
				"E.g.f.: (-2)*exp(x/2)*cosh(x/2)/cosh(x). - _G. C. Greubel_, Aug 24 2017"
			],
			"maple": [
				"A163982 := n -\u003e -2^n*(euler(n,1/2)+euler(n,1)): # _Peter Luschny_, Nov 25 2010",
				"A163982 := proc(n)",
				"    (1+I)/(1-I*exp(x))-1 ;",
				"    coeftayl(%,x=0,n) ;",
				"    Re(%*2*n!) ;",
				"end proc; # _R. J. Mathar_, Mar 26 2013"
			],
			"mathematica": [
				"f[t_] = (1 + I)/(1 - I*Exp[t]) - 1; Table[Re[2*n!*SeriesCoefficient[Series[f[t], {t, 0, 30}], n]], {n, 0, 30}]",
				"max = 20; Clear[g]; g[max + 2] = 1; g[k_] := g[k] = 1 + x + (4*k+3)*(k+1)*x^2 /( 1 + (4*k+5)*(k+1)*x^2 / g[k+1]); gf = -2 - x/g[0]; CoefficientList[Series[gf, {x, 0, max}], x] (* _Vaclav Kotesovec_, Jan 22 2015, after _Sergei N. Gladkovskii_ *)",
				"With[{nn = 50}, CoefficientList[Series[(-2)*Exp[t/2]*Cosh[t/2]/Cosh[t], {t, 0, nn}], t]*Range[0, nn]!] (* _G. C. Greubel_, Aug 24 2017 *)"
			],
			"program": [
				"(PARI) t='t+O('t^10); Vec(serlaplace((-2)*exp(x/2)*cosh(x/2)/cosh(x))) \\\\ _G. C. Greubel_, Aug 24 2017"
			],
			"xref": [
				"Cf. A163747."
			],
			"keyword": "sign",
			"offset": "0,1",
			"author": "_Roger L. Bagula_, Aug 07 2009",
			"references": 7,
			"revision": 34,
			"time": "2018-11-07T14:44:15-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}