{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000001",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1,
			"id": "M0098 N0035",
			"data": "0,1,1,1,2,1,2,1,5,2,2,1,5,1,2,1,14,1,5,1,5,2,2,1,15,2,2,5,4,1,4,1,51,1,2,1,14,1,2,2,14,1,6,1,4,2,2,1,52,2,5,1,5,1,15,2,13,2,2,1,13,1,2,4,267,1,4,1,5,1,4,1,50,1,2,3,4,1,6,1,52,15,2,1,15,1,2,1,12,1,10,1,4,2",
			"name": "Number of groups of order n.",
			"comment": [
				"Also, number of nonisomorphic subgroups of order n in symmetric group S_n. - _Lekraj Beedassy_, Dec 16 2004",
				"Also, number of nonisomorphic primitives of the combinatorial species Lin[n-1]. - _Nicolae Boicu_, Apr 29 2011",
				"In (J. H. Conway, Heiko Dietrich and E. A. O'Brien, 2008), a(n) is called the \"group number of n\", denoted by gnu(n), and the first occurrence of k is called the \"minimal order attaining k\", denoted by moa(k) (see A046057). - _Daniel Forgues_, Feb 15 2017",
				"It is conjectured in (J. H. Conway, Heiko Dietrich and E. A. O'Brien, 2008) that the sequence n -\u003e a(n) -\u003e a(a(n)) = a^2(n) -\u003e a(a(a(n))) = a^3(n) -\u003e ... -\u003e consists ultimately of 1s, where a(n), denoted by gnu(n), is called the \"group number of n\". - _Muniru A Asiru_, Nov 19 2017",
				"MacHale (2020) shows that there are infinitely many values of n for which there are more groups than rings of that order (cf. A027623). He gives n = 36355 as an example. It would be nice to have enough values of n to create an OEIS entry for them. - _N. J. A. Sloane_, Jan 02 2021"
			],
			"reference": [
				"S. R. Blackburn, P. M. Neumann, and G. Venkataraman, Enumeration of Finite Groups, Cambridge, 2007.",
				"L. Comtet, Advanced Combinatorics, Reidel, 1974, p. 302, #35.",
				"J. H. Conway et al., The Symmetries of Things, Peters, 2008, p. 209.",
				"H. S. M. Coxeter and W. O. J. Moser, Generators and Relations for Discrete Groups, 4th ed., Springer-Verlag, NY, reprinted 1984, p. 134.",
				"CRC Standard Mathematical Tables and Formulae, 30th ed. 1996, p. 150.",
				"R. L. Graham, D. E. Knuth and O. Patashnik, Concrete Mathematics, A Foundation for Computer Science, Addison-Wesley Publ. Co., Reading, MA, 1989, Section 6.6 'Fibonacci Numbers' pp. 281-283.",
				"M. Hall, Jr. and J. K. Senior, The Groups of Order 2^n (n \u003c= 6). Macmillan, NY, 1964.",
				"D. Joyner, 'Adventures in Group Theory', Johns Hopkins Press. Pp. 169-172 has table of groups of orders \u003c 26.",
				"D. S. Mitrinovic et al., Handbook of Number Theory, Kluwer, Section XIII.24, p. 481.",
				"M. F. Newman and E. A. O'Brien, A CAYLEY library for the groups of order dividing 128. Group theory (Singapore, 1987), 437-442, de Gruyter, Berlin-New York, 1989.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"H.-U. Besche and Ivan Panchenko, \u003ca href=\"/A000001/b000001.txt\"\u003eTable of n, a(n) for n = 0..2047\u003c/a\u003e [Terms 1 through 2015 copied from Small Groups Library mentioned below. Terms 2016 - 2047 added by _Ivan Panchenko_, Aug 29 2009. 0 prepended by _Ray Chandler_, Sep 16 2015. a(1024) corrected by _Benjamin Przybocki_, Jan 06 2022]",
				"H. A. Bender, \u003ca href=\"http://www.jstor.org/stable/1967981\"\u003eA determination of the groups of order p^5\u003c/a\u003e, Ann. of Math. (2) 29, pp. 61-72 (1927).",
				"Hans Ulrich Besche and Bettina Eick, \u003ca href=\"http://dx.doi.org/10.1006/jsco.1998.0258\"\u003eConstruction of finite groups\u003c/a\u003e, Journal of Symbolic Computation, Vol. 27, No. 4, Apr 15 1999, pp. 387-404.",
				"Hans Ulrich Besche and Bettina Eick, \u003ca href=\"http://dx.doi.org/10.1006/jsco.1998.0259\"\u003eThe groups of order at most 1000 except 512 and 768\u003c/a\u003e, Journal of Symbolic Computation, Vol. 27, No. 4, Apr 15 1999, pp. 405-413.",
				"H. U. Besche, B. Eick and E. A. O'Brien, \u003ca href=\"http://www.ams.org/era/2001-07-01/S1079-6762-01-00087-7/home.html\"\u003eThe groups of order at most 2000\u003c/a\u003e, Electron. Res. Announc. Amer. Math. Soc. 7 (2001), 1-4.",
				"H. U. Besche, B. Eick and E. A. O'Brien, \u003ca href=\"http://www.icm.tu-bs.de/ag_algebra/software/small/\"\u003eThe Small Groups Library\u003c/a\u003e",
				"H. U. Besche, B. Eick and E. A. O'Brien, \u003ca href=\"http://www.icm.tu-bs.de/ag_algebra/software/small/number.html\"\u003eNumber of isomorphism types of finite groups of given order\u003c/a\u003e",
				"H.-U. Besche, B. Eick and E. A. O'Brien, \u003ca href=\"http://dx.doi.org/10.1142/S0218196702001115\"\u003eA Millennium Project: Constructing Small Groups\u003c/a\u003e, Internat. J. Algebra and Computation, 12 (2002), 623-644.",
				"Henry Bottomley, \u003ca href=\"/A000001/a000001.gif\"\u003eIllustration of initial terms\u003c/a\u003e",
				"D. Burrell, \u003ca href=\"https://people.clas.ufl.edu/davidburrell/files/NoteGroups1024.pdf\"\u003eOn the number of groups of order 1024\u003c/a\u003e, Communications in Algebra, 2021, 1-3.",
				"J. H. Conway, Heiko Dietrich and E. A. O'Brien, \u003ca href=\"http://www.math.auckland.ac.nz/~obrien/research/gnu.pdf\"\u003eCounting groups: gnus, moas and other exotica\u003c/a\u003e, Math. Intell., Vol. 30, No. 2, Spring 2008.",
				"Yang-Hui He and Minhyong Kim, \u003ca href=\"https://arxiv.org/abs/1905.02263\"\u003eLearning Algebraic Structures: Preliminary Investigations\u003c/a\u003e, arXiv:1905.02263 [cs.LG], 2019.",
				"Otto Hölder, \u003ca href=\"http://dx.doi.org/10.1007/BF01443651\"\u003eDie Gruppen der Ordnungen p^3, pq^2, pqr, p^4\u003c/a\u003e, Math. Ann. 43 pp. 301-412 (1893).",
				"Rodney James, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1980-0559207-0\"\u003eThe groups of order p^6 (p an odd prime)\u003c/a\u003e, Math. Comp. 34 (1980), 613-637.",
				"Rodney James and John Cannon, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1969-0238953-8\"\u003eComputation of isomorphism classes of p-groups\u003c/a\u003e, Mathematics of Computation 23.105 (1969): 135-140.",
				"Desmond MacHale, \u003ca href=\"https://doi.org/10.1080/00029890.2020.1820790\"\u003eAre There More Finite Rings than Finite Groups?\u003c/a\u003e, Amer. Math. Monthly (2020) Vol. 127, Issue 10, 936-938.",
				"G. A. Miller, \u003ca href=\"http://www.jstor.org/stable/2370630\"\u003eDetermination of all the groups of order 64\u003c/a\u003e, Amer. J. Math., 52 (1930), 617-634.",
				"László Németh and László Szalay, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL24/Nemeth/nemeth8.html\"\u003eSequences Involving Square Zig-Zag Shapes\u003c/a\u003e, J. Int. Seq., Vol. 24 (2021), Article 21.5.2.",
				"Ed Pegg, Jr., \u003ca href=\"http://www.mathpuzzle.com/MAA/07-Sequence%20Pictures/mathgames_12_08_03.html\"\u003eSequence Pictures\u003c/a\u003e, Math Games column, Dec 08 2003.",
				"Ed Pegg, Jr., \u003ca href=\"/A000043/a000043_2.pdf\"\u003eSequence Pictures\u003c/a\u003e, Math Games column, Dec 08 2003 [Cached copy, with permission (pdf only)]",
				"D. S. Rajan, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(93)90061-W\"\u003eThe equations D^kY=X^n in combinatorial species\u003c/a\u003e, Discrete Mathematics 118 (1993) 197-206 North-Holland.",
				"E. Rodemich, \u003ca href=\"http://dx.doi.org/10.1016/0021-8693(90)90244-I\"\u003eThe groups of order 128\u003c/a\u003e, J. Algebra 67 (1980), no. 1, 129-142.",
				"Gordon Royle, \u003ca href=\"http://staffhome.ecm.uwa.edu.au/~00013890/data.html\"\u003eCombinatorial Catalogues\u003c/a\u003e. See subpage \"Generators of small groups\" for explicit generators for most groups of even order \u003c 1000.",
				"D. Rusin, \u003ca href=\"/A000001/a000001.txt\"\u003eAsymptotics\u003c/a\u003e [Cached copy of lost web page]",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/FiniteGroup.html\"\u003eFinite Group\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Finite_group\"\u003eFinite group\u003c/a\u003e",
				"M. Wild, \u003ca href=\"http://www.jstor.org/stable/30037381\"\u003eThe groups of order sixteen made easy\u003c/a\u003e, Amer. Math. Monthly, 112 (No. 1, 2005), 20-31.",
				"Gang Xiao, \u003ca href=\"http://wims.unice.fr/~wims/wims.cgi?module=tool/algebra/smallgroup\"\u003eSmallGroup\u003c/a\u003e",
				"\u003ca href=\"/index/Gre#groups\"\u003eIndex entries for sequences related to groups\u003c/a\u003e",
				"\u003ca href=\"/index/Cor#core\"\u003eIndex entries for \"core\" sequences\u003c/a\u003e"
			],
			"formula": [
				"From _Mitch Harris_, Oct 25 2006: (Start)",
				"For p, q, r primes:",
				"a(p) = 1, a(p^2) = 2, a(p^3) = 5, a(p^4) = 14, if p = 2, otherwise 15.",
				"a(p^5) = 61 + 2*p + 2*gcd(p-1,3) + gcd(p-1,4), p \u003e= 5, a(2^5)=51, a(3^5)=67.",
				"a(p^e) ~ p^((2/27)e^3 + O(e^(8/3))).",
				"a(p*q) = 1 if gcd(p,q-1) = 1, 2 if gcd(p,q-1) = p. (p \u003c q)",
				"a(p*q^2) is one of the following:",
				"  5, p=2, q odd,",
				"  (p+9)/2, q == 1 (mod p), p odd,",
				"  5, p=3, q=2,",
				"  3, q == -1 (mod p), p and q odd,",
				"  4, p == 1 (mod q), p \u003e 3, p !== 1 (mod q^2),",
				"  5, p == 1 (mod q^2),",
				"  2, q !== +-1 (mod p) and p !== 1 (mod q).",
				"a(p*q*r) (p \u003c q \u003c r) is one of the following:",
				"  q == 1 (mod p)  r == 1 (mod p)  r == 1 (mod q)  a(p*q*r)",
				"  --------------  --------------  --------------  --------",
				"        No              No              No            1",
				"        No              No              Yes           2",
				"        No              Yes             No            2",
				"        No              Yes             Yes           4",
				"        Yes             No              No            2",
				"        Yes             No              Yes           3",
				"        Yes             Yes             No           p+2",
				"        Yes             Yes             Yes          p+4",
				"[table from Derek Holt].",
				"(End)",
				"a(n) = A000688(n) + A060689(n). - _R. J. Mathar_, Mar 14 2015"
			],
			"example": [
				"Groups of orders 1 through 10 (C_n = cyclic, D_n = dihedral of order n, Q_8 = quaternion, S_n = symmetric):",
				"1: C_1",
				"2: C_2",
				"3: C_3",
				"4: C_4, C_2 X C_2",
				"5: C_5",
				"6: C_6, S_3=D_6",
				"7: C_7",
				"8: C_8, C_4 X C_2, C_2 X C_2 X C_2, D_8, Q_8",
				"9: C_9, C_3 X C_3",
				"10: C_10, D_10"
			],
			"maple": [
				"GroupTheory:-NumGroups(n); # with(GroupTheory); loads this command - _N. J. A. Sloane_, Dec 28 2017"
			],
			"mathematica": [
				"FiniteGroupCount[Range[100]] (* _Harvey P. Dale_, Jan 29 2013 *)",
				"a[ n_] := If[ n \u003c 1, 0, FiniteGroupCount @ n]; (* _Michael Somos_, May 28 2014 *)"
			],
			"program": [
				"(MAGMA) D:=SmallGroupDatabase(); [ NumberOfSmallGroups(D, n) : n in [1..1000] ]; // _John Cannon_, Dec 23 2006",
				"(GAP) A000001 := Concatenation([0], List([1..500], n -\u003e NumberSmallGroups(n))); # _Muniru A Asiru_, Oct 15 2017"
			],
			"xref": [
				"The main sequences concerned with group theory are A000001 (this one), A000679, A001034, A001228, A005180, A000019, A000637, A000638, A002106, A005432, A000688, A060689, A051532.",
				"Cf. A046058, A046059, A023675, A023676.",
				"A003277 gives n for which A000001(n) = 1, A063756 (partial sums).",
				"A046057 gives first occurrence of each k.",
				"A027623 gives the number of rings of order n."
			],
			"keyword": "nonn,core,nice,hard,changed",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Michael Somos_",
				"Typo in b-file description fixed by _David Applegate_, Sep 05 2009"
			],
			"references": 159,
			"revision": 210,
			"time": "2022-01-06T05:04:14-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}