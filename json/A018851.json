{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A018851",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 18851,
			"data": "0,1,5,6,2,23,8,27,9,3,10,34,11,37,12,39,4,42,43,14,45,46,15,48,49,5,51,52,17,54,55,56,18,58,59,188,6,61,62,63,20,203,65,66,21,213,68,69,22,7,71,72,23,73,74,235,75,24,241,77,78,247,25,251,8,81,257,26,83,263,84,267,27",
			"name": "a(n)^2 is smallest square beginning with n.",
			"comment": [
				"The following discussion is based on comments from _David A. Corneth_, _Robert Israel_, _N. J. A. Sloane_, and _Chai Wah Wu_. (Start)",
				"As the graph shows, the points belong to various \"curves\". For each n there is a value d = d(n) such that n*10^d \u003c= a(n)^2 \u003c (n+1)*10^d, and so on this curve, a(n) ~ sqrt(n)*10^(d/2). The values of d(n) are given in A272677.",
				"For a given n, d can range from 0 (if n is a square) to d_max, where it appears that d_max approx. equals 3 + floor( log_10(n/25) ). The successive points where d_max increases are given in A272678, and that entry contains more precise conjectures about the values.",
				"For example, in the range 2600 = A272678(5) \u003c= n \u003c 25317 = A272678(6), d_max is 5. This is the upper curve in the graph that is seen if the \"graph\" button is clicked, and on this curve a(n) is about sqrt(n)*10^(5/2). (End)"
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/A018851/b018851.txt\"\u003eTable of n, a(n) for n = 0..20000\u003c/a\u003e",
				"Zak Seidov, \u003ca href=\"http://zak08.livejournal.com/9621.html\"\u003eBlog entry\u003c/a\u003e",
				"Zak Seidov, \u003ca href=\"/A018851/a018851.pdf\"\u003eBlog entry\u003c/a\u003e [Cached copy, pdf format, with permission]"
			],
			"formula": [
				"a(n) \u003e= sqrt(n), for all n \u003e= 0 with equality when n is a square. - _Derek Orr_, Jul 26 2015"
			],
			"maple": [
				"f:= proc(n) local d,m;",
				"  for d from 0 do",
				"    m:= ceil(sqrt(10^d*n));",
				"    if m^2 \u003c 10^d*(n+1) then return m fi",
				"  od",
				"end proc:",
				"map(f, [$1..100]); # _Robert Israel_, Jul 26 2015"
			],
			"program": [
				"(PARI) a(n)=k=1; while(k,d=digits(k^2); D=digits(n); if(#D\u003c=#d,c=1; for(i=1,#D,if(D[i]!=d[i],c=0;break));if(c,return(k)));k++)",
				"vector(100,n,a(n)) \\\\ _Derek Orr_, Jul 26 2015"
			],
			"xref": [
				"Cf. A018796 (the squares), A272677, A272678.",
				"A265432 is a more complicated sequence in the same spirit."
			],
			"keyword": "nonn,base,look",
			"offset": "0,3",
			"author": "_David W. Wilson_",
			"ext": [
				"Added initial 0. - _N. J. A. Sloane_, May 21 2016",
				"Comments revised by _N. J. A. Sloane_, Jul 17 2016"
			],
			"references": 21,
			"revision": 53,
			"time": "2016-07-17T11:10:09-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}