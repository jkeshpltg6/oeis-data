{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A101164",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 101164,
			"data": "0,0,0,0,1,0,0,2,2,0,0,3,7,3,0,0,4,15,15,4,0,0,5,26,43,26,5,0,0,6,40,94,94,40,6,0,0,7,57,175,251,175,57,7,0,0,8,77,293,555,555,293,77,8,0,0,9,100,455,1079,1431,1079,455,100,9,0,0,10,126,668,1911,3191,3191,1911,668,126,10,0",
			"name": "Triangle read by rows: Delannoy numbers minus binomial coefficients.",
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A101164/b101164.txt\"\u003eRows n = 0..100 of table, flattened\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/DelannoyNumber.html\"\u003eDelannoy Number\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/BinomialCoefficient.html\"\u003eBinomial Coefficient\u003c/a\u003e",
				"\u003ca href=\"/index/Pas#Pascal\"\u003eIndex entries for triangles and arrays related to Pascal's triangle\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = TD(n, k) - binomial(n, k), 0\u003c=k\u003c=n, where binomial=A007318 and TD = A008288.",
				"T(n,2) = A005449(n-2) for n\u003e1;",
				"T(n,3) = A101165(n-3) for n\u003e2;",
				"T(n,4) = A101166(n-4) for n\u003e3;",
				"Sum_{k=0..n} T(n, k) = A094706(n).",
				"From _G. C. Greubel_, Sep 17 2021: (Start)",
				"T(n, k) = A008288(n-k, k) - binomial(n, k).",
				"T(n, k) = Sum_{j=0..n-k} binomial(n-k, j)*binomial(k, j)*2^j - binomial(n,k).",
				"T(n, 1) = n-1, n \u003e 0. (End)"
			],
			"example": [
				"Triangle begins as:",
				"  0",
				"  0, 0;",
				"  0, 1,  0;",
				"  0, 2,  2,   0;",
				"  0, 3,  7,   3,   0;",
				"  0, 4, 15,  15,   4,   0;",
				"  0, 5, 26,  43,  26,   5,  0;",
				"  0, 6, 40,  94,  94,  40,  6, 0;",
				"  0, 7, 57, 175, 251, 175, 57, 7, 0;"
			],
			"mathematica": [
				"T[n_, k_]:= Hypergeometric2F1[-k, k-n, 1, 2] - Binomial[n, k];",
				"Table[T[n, k], {n,0,12}, {k,0,n}]//Flatten (* _G. C. Greubel_, Sep 17 2021 *)"
			],
			"program": [
				"(Haskell)",
				"a101164 n k = a101164_tabl !! n !! k",
				"a101164_row n = a101164_tabl !! n",
				"a101164_tabl = zipWith (zipWith (-)) a008288_tabl a007318_tabl",
				"-- _Reinhard Zumkeller_, Jul 30 2013",
				"(MAGMA)",
				"A101164:= func\u003c n,k | (\u0026+[Binomial(n-k,j)*Binomial(k,j)*2^j: j in [0..n-k]]) - Binomial(n,k) \u003e;",
				"[A101164(n,k): k in [0..n], n in [0..12]]; // _G. C. Greubel_, Sep 17 2021",
				"(Sage)",
				"def T(n,k): return simplify(hypergeometric([-n+k, -k], [1], 2)) - binomial(n,k)",
				"flatten([[T(n,k) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, Sep 17 2021"
			],
			"xref": [
				"Cf. A001477, A005449, A008288, A094706, A101165, A101166, A225413."
			],
			"keyword": "nonn,tabl",
			"offset": "0,8",
			"author": "_Reinhard Zumkeller_, Dec 03 2004",
			"references": 8,
			"revision": 32,
			"time": "2021-09-23T02:26:04-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}