{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A066771",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 66771,
			"data": "1,3,-7,-117,-527,-237,11753,76443,164833,-922077,-9653287,-34867797,32125393,1064447283,5583548873,6890111163,-98248054847,-761741108157,-2114245277767,6358056037323,91004468168113,387075408075603,47340744250793,-9392840736385317",
			"name": "5^n cos(2n arctan(1/2)) or denominator of tan(2n arctan(1/2)).",
			"comment": [
				"Let A =",
				"[ -(3/5)-(2/5)i,-(2/5)i,-(2/5)i,-(2/5)i ]",
				"[ -(2/5)i,-(3/5)+(2/5)i,-(2/5)i,(2/5)i ]",
				"[ -(2/5)i,-(2/5)i,-(3/5)+(2/5)i,(2/5)i ]",
				"[ -(2/5)i,(2/5)i,(2/5)i,-(3/5)-(2/5)i ]",
				"be the Cayley transform of the matrix iH, where H =",
				"[1,1,1,1]",
				"[1,-1,1,-1]",
				"[1,1,-1,-1]",
				"[1,-1,-1,1]",
				"is an Hadamard matrix of order 4 and i is the imaginary unit. Any diagonal entry of the matrix A^n is one of the four complex numbers (+ or -)(X/5^n)(+ or -)(Y/(5^n)i). Then a(n) is the X in [A^n]_(j,j), j=1,2,3,4. - _Simone Severini_, Apr 28 2004",
				"Related to the (3,4,5) Pythagorean triple. Each unsigned term represents a leg in a Pythagorean triple in which the hypotenuse = 5^n. E.g. (3 + 4i)^3 = (-117 + 44i), considered as two legs of a triangle, hypotenuse = 125 = 5^3. - _Gary W. Adamson_, Aug 06 2006"
			],
			"reference": [
				"Steven R. Finch, Mathematical Constants, Cambridge, 2003, pp. 430-433."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A066771/b066771.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"J. M. Borwein and R. Girgensohn, \u003ca href=\"http://dx.doi.org/10.4153/CJM-1995-013-4\"\u003eAddition theorems and binary expansions\u003c/a\u003e, Canadian J. Math. 47 (1995) 262-273.",
				"E. Eckert, \u003ca href=\"http://www.jstor.org/stable/2690291\"\u003eThe group of primitive Pythagorean triangles\u003c/a\u003e, Mathematics Magazine 57 (1984) 22-27.",
				"Steven R. Finch, \u003ca href=\"http://www.people.fas.harvard.edu/~sfinch/constant/plff/plff.html\"\u003ePlouffe's Constant\u003c/a\u003e [Broken link]",
				"Steven R. Finch, \u003ca href=\"http://web.archive.org/web/20010624104257/http://www.mathsoft.com/asolve/constant/plff/plff.html\"\u003ePlouffe's Constant\u003c/a\u003e [From the Wayback machine]",
				"Simon Plouffe, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/compass.html\"\u003eThe Computation of Certain Numbers Using a Ruler and Compass\u003c/a\u003e, J. Integer Seqs. Vol. 1 (1998), #98.1.3.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (6,-25)."
			],
			"formula": [
				"G.f.: ( 1-3*x ) / ( 1-6*x+25*x^2 ).",
				"A recursive formula for T(n) = tan(2n arctan(1/2)) is T(n+1)=(4/3+T(n))/(1-4/3*T(n)). Unsigned A(n) is the absolute value of the denominator of T(n).",
				"a(n) is the real part of (2+I)^(2n) = sum(k=0, n, 4^(n-k)*(-1)^k*C(2n, 2k) ). - _Benoit Cloitre_, Aug 03 2002",
				"a(n) = real part of (3 + 4i)^n. - _Gary W. Adamson_, Aug 06 2006",
				"a(n) = 6*a(n-1)-25*a(n-2). - _Gary Detlefs_, Jun 10 2010",
				"a(n) = 5^n*cos(n*arccos(3/5)). - _Gary Detlefs_, Dec 11 2010"
			],
			"maple": [
				"a[1] := 4/3; for n from 1 to 40 do a[n+1] := (4/3+a[n])/(1-4/3*a[n]):od: seq(abs(denom(a[n])), n=1..40);# a[n]=tan(2n arctan(1/2))"
			],
			"mathematica": [
				"CoefficientList[Series[(1-3x)/(1-6x+25x^2),{x,0,30}],x] (* or *) LinearRecurrence[{6,-25},{1,3},30] (* _Harvey P. Dale_, Jul 16 2011 *)"
			],
			"program": [
				"(PARI) a(n)=real((2+I)^(2*n))"
			],
			"xref": [
				"Cf. A066770 5^n sin(2n arctan(1/2)), A000351 powers of 5 and also hypotenuse of right triangle with legs given by A066770 and A066771.",
				"Note that A066770, A066771 and A000351 are primitive Pythagorean triples with hypotenuse 5^n. The offset of A000351 is 0, but the offset is 1 for A066770, A066771.",
				"Cf. A093378.",
				"Cf. A193410, A121622."
			],
			"keyword": "sign,easy,frac",
			"offset": "0,2",
			"author": "Barbara Haas Margolius, (b.margolius(AT)csuohio.edu), Jan 17 2002",
			"references": 12,
			"revision": 49,
			"time": "2020-09-05T03:13:23-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}