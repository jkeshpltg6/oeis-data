{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A219727",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 219727,
			"data": "1,1,1,1,1,1,1,2,2,1,1,5,9,3,1,1,15,66,31,5,1,1,52,712,686,109,7,1,1,203,10457,27036,6721,339,11,1,1,877,198091,1688360,911838,58616,1043,15,1,1,4140,4659138,154703688,231575143,26908756,476781,2998,22,1",
			"name": "Number A(n,k) of k-partite partitions of {n}^k into k-tuples; square array A(n,k), n\u003e=0, k\u003e=0, read by antidiagonals.",
			"comment": [
				"A(n,k) is the number of factorizations of m^n where m is a product of k distinct primes.  A(2,2) = 9: (2*3)^2 = 36 has 9 factorizations: 36, 3*12, 4*9, 3*3*4, 2*18, 6*6, 2*3*6, 2*2*9, 2*2*3*3.",
				"A(n,k) is the number of (n*k) X k matrices with nonnegative integer entries and column sums n up to permutation of rows. - _Andrew Howroyd_, Dec 10 2018"
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A219727/b219727.txt\"\u003eTable of n, a(n) for n = 0..209\u003c/a\u003e"
			],
			"example": [
				"A(1,3) = 5: [(1,1,1)], [(1,1,0),(0,0,1)], [(1,0,1),(0,1,0)], [(1,0,0),(0,1,0),(0,0,1)], [(0,1,1),(1,0,0)].",
				"A(2,2) = 9: [(2,2)], [(2,1),(0,1)], [(2,0),(0,2)], [(2,0),(0,1),(0,1)], [(1,2),(1,0)], [(1,1),(1,1)], [(1,1),(1,0),(0,1)], [(1,0),(1,0),(0,2)], [(1,0),(1,0),(0,1),(0,1)].",
				"Square array A(n,k) begins:",
				"  1,   1,    1,      1,        1,         1,         1,       1, ...",
				"  1,   1,    2,      5,       15,        52,       203,     877, ...",
				"  1,   2,    9,     66,      712,     10457,    198091, 4659138, ...",
				"  1,   3,   31,    686,    27036,   1688360, 154703688, ...",
				"  1,   5,  109,   6721,   911838, 231575143, ...",
				"  1,   7,  339,  58616, 26908756, ...",
				"  1,  11, 1043, 476781, ...",
				"  1,  15, 2998, ..."
			],
			"program": [
				"(PARI)",
				"EulerT(v)={Vec(exp(x*Ser(dirmul(v,vector(#v,n,1/n))))-1, -#v)}",
				"D(p, n, k)={my(v=vector(n)); for(i=1, #p, v[p[i]]++); EulerT(v)[n]^k/prod(i=1, #v, i^v[i]*v[i]!)}",
				"T(n, k)={my(m=n*k, q=Vec(exp(O(x*x^m) + intformal((x^n-1)/(1-x)))/(1-x))); if(n==0, 1, sum(j=0, m, my(s=0); forpart(p=j, s+=D(p,n,k), [1,n]); s*q[#q-j]))} \\\\ _Andrew Howroyd_, Dec 11 2018"
			],
			"xref": [
				"Columns k=0..3 give: A000012, A000041, A002774, A219678.",
				"Rows n=0..3 give: A000012, A000110, A020555, A322487.",
				"Main diagonal gives A322488.",
				"Cf. A188392, A219585 (partitions of {n}^k into distinct k-tuples), A256384, A318284, A318951."
			],
			"keyword": "nonn,tabl",
			"offset": "0,8",
			"author": "_Alois P. Heinz_, Nov 26 2012",
			"references": 18,
			"revision": 29,
			"time": "2018-12-11T20:15:53-05:00",
			"created": "2012-11-26T10:58:47-05:00"
		}
	]
}