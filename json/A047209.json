{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A047209",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 47209,
			"data": "1,4,6,9,11,14,16,19,21,24,26,29,31,34,36,39,41,44,46,49,51,54,56,59,61,64,66,69,71,74,76,79,81,84,86,89,91,94,96,99,101,104,106,109,111,114,116,119,121,124,126,129,131,134,136,139,141,144,146,149,151,154",
			"name": "Numbers that are congruent to {1, 4} mod 5.",
			"comment": [
				"Apart from initial term(s), dimension of the space of weight 2n cuspidal newforms for Gamma_0( 72 ).",
				"Cf. property described by _Gary Detlefs_ in A113801: more generally, these numbers are of the form (2*h*n+(h-4)*(-1)^n-h)/4 (h, n natural numbers), therefore (2*h*n + (h-4)*(-1)^n - h)/4)^2 - 1 == 0 (mod h); in our case, a(n)^2 - 1 == 0 (mod 5). - _Bruno Berselli_, Nov 17 2010",
				"The sum of the alternating series (-1)^(n+1)/a(n) from n=1 to infinity is (Pi/5)*cot(Pi/5), that is (1/5)*sqrt(1 + 2/sqrt(5))*Pi. - _Jean-François Alcover_, May 03 2013",
				"These numbers appear in the product of a Rogers-Ramanujan identity. See A003114 also for references. - _Wolfdieter Lang_, Oct 29 2016",
				"Let m be a product of any number of terms of this sequence. Then m - 1 or m + 1 is divisible by 5. Closed under multiplication. - _David A. Corneth_, May 11 2018"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A047209/b047209.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"William A. Stein, \u003ca href=\"http://wstein.org/Tables/\"\u003eThe modular forms database\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/DeterminedbySpectrum.html\"\u003eDetermined by Spectrum\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,1,-1)."
			],
			"formula": [
				"G.f.: (1+3x+x^2)/((1-x)(1-x^2)).",
				"a(n) = floor((5n-2)/2). [corrected by _Reinhard Zumkeller_, Jul 19 2013]",
				"a(1) = 1, a(n) = 5(n-1) - a(n-1). - _Benoit Cloitre_, Apr 12 2003",
				"From _Bruno Berselli_, Nov 17 2010: (Start)",
				"a(n) = (10*n + (-1)^n - 5)/4.",
				"a(n) - a(n-1) - a(n-2) + a(n-3) = 0 for n \u003e 3.",
				"a(n) = a(n-2) + 5 for n \u003e 2.",
				"a(n) = 5*A000217(n-1) + 1 - 2*Sum_{i=1..n-1} a(i) for n \u003e 1.",
				"a(n)^2 = 5*A036666(n) + 1 (cf. also Comments). (End)",
				"a(n) = 5*floor(n/2) + (-1)^(n+1). - _Gary Detlefs_, Dec 29 2011"
			],
			"maple": [
				"seq(floor(5*k/2)-1, k=1..100); # _Wesley Ivan Hurt_, Sep 27 2013"
			],
			"mathematica": [
				"Select[Range[0, 200], MemberQ[{1, 4}, Mod[#, 5]] \u0026] (* _Vladimir Joseph Stephan Orlovsky_, Feb 12 2012 *)"
			],
			"program": [
				"(Haskell)",
				"a047209 = (flip div 2) . (subtract 2) . (* 5)",
				"a047209_list = 1 : 4 : (map (+ 5) a047209_list)",
				"-- _Reinhard Zumkeller_, Jul 19 2013, Jan 05 2011",
				"(PARI) a(n)=(10*n+(-1)^n-5)/4 \\\\ _Charles R Greathouse IV_, Sep 24 2015"
			],
			"xref": [
				"Cf. A000566, A036666, A003114, A203776, A047336, A047522, A056020, A090771, A175885, A091998, A175886, A175887.",
				"Cf. A005408 (n=1 or 3 mod 4), A007310 (n=1 or 5 mod 6).",
				"Cf. A045468 (primes), A032527 (partial sums)."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Edited by _Michael Somos_, Sep 22 2002"
			],
			"references": 51,
			"revision": 100,
			"time": "2020-03-07T07:53:57-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}