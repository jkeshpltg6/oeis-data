{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A282715",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 282715,
			"data": "1,2,2,3,3,4,3,4,3,4,5,6,5,4,6,7,7,6,4,6,5,7,6,7,5,6,4,5,7,8,8,7,10,10,11,9,7,8,10,7,5,8,11,10,9,10,13,12,13,10,12,11,11,8,5,8,7,10,9,11,8,10,7,10,12,13,11,8,11,13,12,10,7,10,8,11,9,10",
			"name": "Number of nonzero entries in row n of the base-3 generalized Pascal triangle P_3.",
			"comment": [
				"It would be nice to have an entry for the triangle P_3 itself (compare A282714 which gives the base-2 triangle P_2)."
			],
			"link": [
				"Lars Blomberg, \u003ca href=\"/A282715/b282715.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Julien Leroy, Michel Rigo, Manon Stipulanti, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2017.01.003\"\u003eCounting the number of non-zero coefficients in rows of generalized Pascal triangles\u003c/a\u003e, Discrete Mathematics 340 (2017), 862-881, Section 7.",
				"Julien Leroy, Michel Rigo, Manon Stipulanti, \u003ca href=\"https://arxiv.org/abs/1705.10065\"\u003eCounting Subwords Occurrences in Base-b Expansions\u003c/a\u003e, arXiv:1705.10065 [math.CO], 2017.",
				"Julien Leroy, Michel Rigo, Manon Stipulanti, \u003ca href=\"http://math.colgate.edu/~integers/sjs13/sjs13.Abstract.html\"\u003eCounting Subwords Occurrences in Base-b Expansions\u003c/a\u003e, Integers, Electronic Journal of Combinatorial Number Theory 18A (2018), #A13.",
				"Manon Stipulanti, \u003ca href=\"https://arxiv.org/abs/1801.03287\"\u003eConvergence of Pascal-Like Triangles in Parry-Bertrand Numeration Systems\u003c/a\u003e, arXiv:1801.03287 [math.CO], 2018."
			],
			"formula": [
				"Leroy et al. (2017) state some conjectured recurrences."
			],
			"example": [
				"The number of nonzero entries in the n-th row of the following triangle:",
				"1",
				"1 1",
				"1 0 1",
				"1 1 0 1",
				"1 2 0 0 1",
				"1 1 1 0 0 1",
				"1 0 1 0 0 0 1",
				"1 1 1 0 0 0 0 1",
				"1 0 2 0 0 0 0 0 1",
				"1 1 0 2 0 0 0 0 0 1",
				"1 2 0 1 1 0 0 0 0 0 1",
				"1 1 1 1 0 1 0 0 0 0 0 1",
				"1 2 0 2 1 0 0 0 0 0 0 0 1",
				"1 3 0 0 3 0 0 0 0 0 0 0 0 1"
			],
			"maple": [
				"# reuses code snippets of A282714",
				"A282715 := proc(n)",
				"    add(min(P(n,k,3),1),k=0..n) ;",
				"end proc:",
				"seq(A282715(n),n=0..100) ; # _R. J. Mathar_, Mar 03 2017"
			],
			"mathematica": [
				"row[n_] := Module[{bb, ss}, bb = Table[IntegerDigits[k, 3], {k, 0, n}]; ss = Subsets[Last[bb]]; Prepend[Count[ss, #]\u0026 /@ bb // Rest, 1]];",
				"a[n_] := Count[row[n], _?Positive];",
				"Table[a[n], {n, 0, 100}] (* _Jean-François Alcover_, Dec 28 2017 *)"
			],
			"xref": [
				"Cf. A007306, A282714."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Mar 02 2017",
			"ext": [
				"More terms from _Lars Blomberg_, Mar 03 2017"
			],
			"references": 3,
			"revision": 29,
			"time": "2019-04-12T13:48:51-04:00",
			"created": "2017-03-02T22:53:19-05:00"
		}
	]
}