{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A276058",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 276058,
			"data": "1,1,1,1,2,1,2,2,2,2,4,3,6,3,10,5,14,4,24,7,30,4,6,46,8,10,58,20,9,84,36,15,106,68,13,152,112,22,188,196,19,264,304,8,32,324,492,24,28,446,732,72,47,546,1120,160,41,744,1616,344",
			"name": "Triangle read by rows: T(n,k) is the number of compositions of n with parts in {3,4,5,6,...} and having asymmetry degree equal to k (n\u003e=0; 0\u003c=k\u003c=floor(n/7)).",
			"comment": [
				"The asymmetry degree of a finite sequence of numbers is defined to be the number of pairs of symmetrically positioned distinct entries. Example: the asymmetry degree of (2,7,6,4,5,7,3) is 2, counting the pairs (2,3) and (6,5).",
				"A sequence is palindromic if and only if its asymmetry degree i 0.",
				"Number of entries in row n is 1 + floor(n/7).",
				"Sum of entries in row n is A078012(n).",
				"T(n,0) = A226916(n+4)",
				"Sum(k*T(n,k), k\u003e=0)=A276059(n)."
			],
			"reference": [
				"S. Heubach and T. Mansour, Combinatorics of Compositions and Words, CRC Press, 2010."
			],
			"link": [
				"Krithnaswami Alladi and V. E. Hoggatt, Jr. \u003ca href=\"http://www.fq.math.ca/Scanned/13-3/alladi1.pdf\"\u003eCompositions with Ones and Twos\u003c/a\u003e, Fibonacci Quarterly, 13 (1975), 233-239.",
				"V. E. Hoggatt, Jr., and Marjorie Bicknell, \u003ca href=\"http://www.fq.math.ca/Scanned/13-4/hoggatt1.pdf\"\u003ePalindromic compositions\u003c/a\u003e, Fibonacci Quart., Vol. 13(4), 1975, pp. 350-356."
			],
			"formula": [
				"G.f.: G(t,z) = (1-z^2)(1-z +z^3)/(1-z-z^2+z^3-z^6+z^7-2tz^7).  In the more general situation of compositions into a[1]\u003ca[2]\u003ca[3]\u003c..., denoting F(z) = Sum(z^{a[j]},j\u003e=1}, we have G(t,z) =(1 + F(z))/(1 - F(z^2) - t(F(z)^2 - F(z^2))). In particular, for t=0 we obtain Theorem 1.2 of the Hoggatt et al. reference."
			],
			"example": [
				"Row 7 is [1,2] because the compositions of 7 with parts in {3,4,5,...} are  7, 34, and 43, having asymmetry degrees 0, 1, and 1, respectively.",
				"Triangle starts:",
				"1;",
				"0;",
				"0;",
				"1;",
				"1;",
				"1;",
				"2;",
				"1,2,"
			],
			"maple": [
				"G := (1-z^2)*(1-z+z^3)/(1-z-z^2+z^3-z^6+z^7-2*t*z^7): Gser := simplify(series(G, z = 0, 30)): for n from 0 to 25 do P[n] := sort(coeff(Gser, z, n)) end do: for n from 0 to 25 do seq(coeff(P[n], t, j), j = 0 .. degree(P[n])) end do; # yields sequence in triangular form"
			],
			"xref": [
				"Cf. A078012, A226916, A276059."
			],
			"keyword": "nonn,tabf",
			"offset": "0,5",
			"author": "_Emeric Deutsch_, Aug 22 2016",
			"references": 1,
			"revision": 7,
			"time": "2016-08-22T11:13:46-04:00",
			"created": "2016-08-22T11:13:46-04:00"
		}
	]
}