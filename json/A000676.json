{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000676",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 676,
			"id": "M0831 N0316",
			"data": "1,1,0,1,1,2,3,7,12,27,55,127,284,682,1618,3979,9823,24722,62651,160744,415146,1081107,2831730,7462542,19764010,52599053,140580206,377244482,1016022191,2745783463,7443742141,20239038700,55178647926",
			"name": "Number of centered trees with n nodes.",
			"comment": [
				"A tree has either a center or a bicenter and either a centroid or a bicentroid. (These terms were introduced by Jordan.)",
				"If the number of edges in a longest path in the tree is 2m, then the middle node in the path is the unique center, otherwise the two middle nodes in the path are the unique bicenters.",
				"On the bottom of first page 266 of article Cayley (1881) is a table of A000676 and A000677 for n = 1..13. - _Michael Somos_, Aug 20 2018"
			],
			"reference": [
				"N. L. Biggs et al., Graph Theory 1736-1936, Oxford, 1976, p. 49.",
				"F. Harary, Graph Theory, Addison-Wesley, Reading, MA, 1994; pp. 35, 36.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/A000676/b000676.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"A. Cayley, \u003ca href=\"http://dx.doi.org/10.1017/CBO9780511703751.056\"\u003eOn the analytical forms called trees, with application to the theory of chemical combinations\u003c/a\u003e, Reports British Assoc. Advance. Sci. 45 (1875), 257-305 = Math. Papers, Vol. 9, 427-460 (see p. 438).",
				"A. Cayley, \u003ca href=\"http://www.jstor.org/stable/2369158\"\u003eOn the analytical forms called trees\u003c/a\u003e, Amer. J. Math., 4 (1881), 266-268.",
				"C. Jordan, \u003ca href=\"http://www.digizeitschriften.de/dms/resolveppn/?PID=GDZPPN002153998\"\u003eSur les assemblages des lignes\u003c/a\u003e, J. Reine angew. Math., 70 (1869), 185-190.",
				"E. M. Rains and N. J. A. Sloane, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/cayley.html\"\u003eOn Cayley's Enumeration of Alkanes (or 4-Valent Trees)\u003c/a\u003e, J. Integer Sequences, Vol. 2 (1999), Article 99.1.1. [This articles states incorrectly that A000676 and A000677 give the numbers of trees with respectively a centroid and bicentroid.]",
				"Peter Steinbach, \u003ca href=\"/A000088/a000088_17.pdf\"\u003eField Guide to Simple Graphs, Volume 1\u003c/a\u003e, Part 17 [but beware errors] (For Volumes 1, 2, 3, 4 of this book see A000088, A008406, A000055, A000664, respectively.)",
				"Peter Steinbach, \u003ca href=\"/A000055/a000055_12.pdf\"\u003eField Guide to Simple Graphs, Volume 3\u003c/a\u003e, Part 12 [but beware errors] (For Volumes 1, 2, 3, 4 of this book see A000088, A008406, A000055, A000664, respectively.)",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CenteredTree.html\"\u003eCentered Tree\u003c/a\u003e.",
				"\u003ca href=\"/index/Tra#trees\"\u003eIndex entries for sequences related to trees\u003c/a\u003e."
			],
			"formula": [
				"a(n) + A000677(n) = A000055(n)."
			],
			"example": [
				"G.f. = 1 + x + x^3 + x^4 + 2*x^5 + 3*x^6 + 7*x^7 + 12*x^8 + 27*x^9 + 55*x^10 + ... - _Michael Somos_, Aug 20 2018"
			],
			"xref": [
				"Cf. A102911 (trees with a bicentroid), A027416 (trees with a centroid), A000677 (trees with a bicenter), A000055 (trees), A000081 (rooted trees)."
			],
			"keyword": "nonn,nice,easy",
			"offset": "0,6",
			"author": "_N. J. A. Sloane_",
			"references": 11,
			"revision": 38,
			"time": "2020-12-26T11:10:00-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}