{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A317533",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 317533,
			"data": "1,2,2,3,4,3,5,14,9,5,7,28,33,16,7,11,69,104,74,29,11,15,134,294,263,142,47,15,22,285,801,948,599,263,77,22,30,536,2081,3058,2425,1214,453,118,30,42,1050,5212,9769,9276,5552,2322,761,181,42,56,1918,12645,29538,34172,23770,11545,4179,1223,267,56",
			"name": "Regular triangle read rows: T(n,k) = number of non-isomorphic multiset partitions of size n and length k.",
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A317533/b317533.txt\"\u003eTable of n, a(n) for n = 1..1275\u003c/a\u003e (first 50 rows)"
			],
			"example": [
				"Non-isomorphic representatives of the T(3,2) = 4 multiset partitions:",
				"  {{1},{1,1}}",
				"  {{1},{1,2}}",
				"  {{1},{2,2}}",
				"  {{1},{2,3}}",
				"Triangle begins:",
				"    1",
				"    2    2",
				"    3    4    3",
				"    5   14    9    5",
				"    7   28   33   16    7",
				"   11   69  104   74   29   11",
				"   15  134  294  263  142   47   15"
			],
			"mathematica": [
				"permcount[v_List] := Module[{m = 1, s = 0, k = 0, t}, For[i = 1, i \u003c= Length[v], i++, t = v[[i]]; k = If[i \u003e 1 \u0026\u0026 t == v[[i - 1]], k + 1, 1]; m *= t*k; s += t]; s!/m];",
				"c[p_List, q_List, k_] := SeriesCoefficient[1/Product[(1 - x^LCM[p[[i]], q[[j]]])^GCD[p[[i]], q[[j]]], {j, 1, Length[q]}, {i, 1, Length[p]}], {x, 0, k}];",
				"M[m_, n_, k_] := Module[{s = 0}, Do[Do[s += permcount[p]*permcount[q]*c[p, q, k], {q, IntegerPartitions[n]}], {p, IntegerPartitions[m]}]; s/(m!*n!)];",
				"T[n_, k_] := M[k, n, n] - M[k - 1, n, n];",
				"Table[T[n, k], {n, 1, 11}, {k, 1, n}] // Flatten (* _Jean-François Alcover_, Feb 08 2020, after _Andrew Howroyd_ *)"
			],
			"program": [
				"(PARI) \\\\ See A318795 for definition of M.",
				"T(n,k)={M(k, n, n) - M(k-1, n, n)}",
				"for(n=1, 10, for(k=1, n, print1(T(n,k),\", \"));print) \\\\ _Andrew Howroyd_, Dec 28 2019",
				"(PARI) \\\\ Faster version.",
				"permcount(v) = {my(m=1, s=0, k=0, t); for(i=1, #v, t=v[i]; k=if(i\u003e1\u0026\u0026t==v[i-1], k+1, 1); m*=t*k; s+=t); s!/m}",
				"K(q, t, n)={1/prod(j=1, #q, (1-x^lcm(t, q[j]) + O(x*x^n))^gcd(t, q[j]))}",
				"G(m,n)={my(s=0); forpart(q=m, s+=permcount(q)*exp(sum(t=1, n, (K(q, t, n)-1)/t) + O(x*x^n))); s/m!}",
				"A(n,m=n)={my(p=sum(k=0, m, G(k,n)*y^k)*(1-y)); matrix(n, m, n, k, polcoef(polcoef(p, n, x), k, y))}",
				"{ my(T=A(10)); for(n=1, #T, print(T[n,1..n])) } \\\\ _Andrew Howroyd_, Aug 30 2020"
			],
			"xref": [
				"Row sums are A007716. First and last columns are both A000041.",
				"Cf. A034691, A255397, A255903, A255906, A317532, A334550."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Gus Wiseman_, Jul 30 2018",
			"ext": [
				"Terms a(29) and beyond from _Andrew Howroyd_, Dec 28 2019"
			],
			"references": 60,
			"revision": 25,
			"time": "2020-08-31T03:32:10-04:00",
			"created": "2018-07-30T22:16:45-04:00"
		}
	]
}