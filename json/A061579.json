{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A061579",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 61579,
			"data": "0,2,1,5,4,3,9,8,7,6,14,13,12,11,10,20,19,18,17,16,15,27,26,25,24,23,22,21,35,34,33,32,31,30,29,28,44,43,42,41,40,39,38,37,36,54,53,52,51,50,49,48,47,46,45,65,64,63,62,61,60,59,58,57,56,55,77,76,75,74,73,72",
			"name": "Reverse one number (0), then two numbers (2,1), then three (5,4,3), then four (9,8,7,6), etc.",
			"comment": [
				"A self-inverse permutation of the nonnegative numbers.",
				"a(n) is the smallest nonnegative integer not yet in the sequence such that n + a(n) is one less than a square. [_Franklin T. Adams-Watters_, Apr 06 2009]",
				"From _Michel Marcus_, Mar 01 2021: (Start)",
				"Array T(n,k) = (n+k)^2/2 + (n+3*k)/2 for n,k \u003e=0 read by descending antidiagonals.",
				"Array T(n,k) = (n+k)^2/2 + (3*n+k)/2 for n,k \u003e=0 read by ascending antidiagonals. (End)"
			],
			"link": [
				"Harry J. Smith, \u003ca href=\"/A061579/b061579.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Madeline Brandt and Kåre Schou Gjaldbæk, \u003ca href=\"https://arxiv.org/abs/2102.13578\"\u003eClassification of Quadratic Packing Polynomials on Sectors of R^2\u003c/a\u003e, arXiv:2102.13578 [math.NT], 2021. See Figure 9 p. 17.",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"a(n) = floor(sqrt(2n+1)-1/2)*floor(sqrt(2n+1)+3/2) - n = A005563(A003056(n)) - n.",
				"Row (or antidiagonal) n = 0, 1, 2, ... contains the integers from A000217(n) to A000217(n+1)-1 in reverse order (for diagonals, \"reversed\" with respect to the canonical \"falling\" order, cf. A001477/table). - _M. F. Hasler_, Nov 09 2021"
			],
			"example": [
				"Read as a triangle, the sequence is:",
				"    0",
				"    2   1",
				"    5   4   3",
				"    9   8   7   6",
				"   14  13  12  11  10",
				"  (...)",
				"As an infinite square matrix (cf. the \"table\" link, 2nd paragraph) it reads:",
				"    0    2    5    9   14   20   ...",
				"    1    4    8   13   19   22   ...",
				"    3    7   12   18   23   30   ...",
				"    6   11   17   24   31   39   ...",
				"  (...)"
			],
			"mathematica": [
				"Module[{nn=20},Reverse/@TakeList[Range[0,(nn(nn+1))/2],Range[nn]]]// Flatten (* Requires Mathematica version 11 or later *) (* _Harvey P. Dale_, Jul 06 2018 *)"
			],
			"program": [
				"(PARI) A061579_row(n)=vector(n+=1, j, n*(n+1)\\2-j)",
				"A061579_upto(n)=concat([A061579_row(r)|r\u003c-[0..sqrtint(2*n)]]) \\\\ yields approximately n terms: actual number differs by less than +- sqrt(n). - _M. F. Hasler_, Nov 09 2021"
			],
			"xref": [
				"Fixed points are A046092.",
				"Each reversal involves the numbers from A000217 through to A000096.",
				"Cf. A038722. Transpose of A001477."
			],
			"keyword": "nonn,tabl",
			"offset": "0,2",
			"author": "_Henry Bottomley_, May 21 2001",
			"references": 18,
			"revision": 31,
			"time": "2021-11-10T10:18:24-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}