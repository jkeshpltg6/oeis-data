{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A152743",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 152743,
			"data": "0,6,30,72,132,210,306,420,552,702,870,1056,1260,1482,1722,1980,2256,2550,2862,3192,3540,3906,4290,4692,5112,5550,6006,6480,6972,7482,8010,8556,9120,9702,10302,10920,11556,12210,12882,13572,14280",
			"name": "6 times pentagonal numbers: a(n) = 3*n*(3*n-1).",
			"comment": [
				"a(n) is also the Wiener index of the windmill graph D(4,n). The windmill graph D(m,n) is the graph obtained by taking n copies of the complete graph K_m with a vertex in common (i.e. a bouquet of n pieces of K_m graphs). The Wiener index of a connected graph is the sum of distances between all unordered pairs of vertices in the graph. The Wiener index of D(m,n) is (1/2)n(m-1)[(m-1)(2n-1)+1]. For the Wiener indices of D(3,n), D(5,n), and D(6,n) see A033991, A028994, and A180577, respectively. - _Emeric Deutsch_, Sep 21 2010",
				"a(n+1) gives the number of edges in a hexagon-like honeycomb built from A003215(n) congruent regular hexagons (see link). Example: a hexagon-like honeycomb consisting of 7 congruent regular hexagons has 1 core hexagon inside a perimeter of six hexagons. The perimeter consists of 18 external edges. There are 6 edges shared by the perimeter hexagons. The core hexagon has 6 edges. a(2) is the total number of edges, i.e. 18 + 6 + 6 = 30. - _Ivan N. Ianakiev_, Mar 10 2015"
			],
			"link": [
				"Ivan Panchenko, \u003ca href=\"/A152743/b152743.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Ivan N. Ianakiev, \u003ca href=\"http://mislandia.weebly.com/blog/hexagon-like-honeycomb-built-from-regular-congruent-hexagons\"\u003eHexagon-like honeycomb built from regular congruent hexagons\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/WindmillGraph.html\"\u003eWindmill Graph\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3, -3, 1)."
			],
			"formula": [
				"a(n) = 9n^2 - 3n = A000326(n)*6.",
				"a(n) = A049450(n)*3 = A062741(n)*2. - _Omar E. Pol_, Dec 15 2008",
				"a(n) = a(n-1)+18*n-12 (with a(0)=0). - _Vincenzo Librandi_, Nov 26 2010",
				"G.f.: -((6*x*(2*x+1))/(x-1)^3). - _Harvey P. Dale_, Jun 30 2011",
				"E.g.f.: 3*x*(2+3*x)*exp(x). - _G. C. Greubel_, Sep 01 2018"
			],
			"maple": [
				"A152743:=n-\u003e3*n*(3*n-1); seq(A152743(n), n=0..50); # _Wesley Ivan Hurt_, Jun 09 2014"
			],
			"mathematica": [
				"Table[3n(3n-1),{n,0,40}] (* or *) LinearRecurrence[{3,-3,1},{0,6,30},40] (* _Harvey P. Dale_, Jun 30 2011 *)",
				"CoefficientList[Series[-6x (2x+1)/(x-1)^3,{x,0,40}],x] (* _Robert G. Wilson v_, Mar 10 2015 *)"
			],
			"program": [
				"(MAGMA) [ 3*n*(3*n-1) : n in [0..50] ]; // _Wesley Ivan Hurt_, Jun 09 2014",
				"(PARI) a(n)=3*n*(3*n-1) \\\\ _Charles R Greathouse IV_, Oct 07 2015"
			],
			"xref": [
				"Cf. A000326, A152734, A152744, A049450, A062741, A033991, A028994, A180577."
			],
			"keyword": "easy,nonn",
			"offset": "0,2",
			"author": "_Omar E. Pol_, Dec 12 2008",
			"ext": [
				"Converted reference to link by _Omar E. Pol_, Oct 07 2010"
			],
			"references": 12,
			"revision": 47,
			"time": "2018-09-01T21:37:37-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}