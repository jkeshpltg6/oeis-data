{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A020921",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 20921,
			"data": "1,1,1,0,1,1,0,2,3,1,0,2,5,4,1,0,4,10,10,5,1,0,2,11,19,15,6,1,0,6,21,35,35,21,7,1,0,4,22,52,69,56,28,8,1,0,6,33,83,126,126,84,36,9,1,0,4,34,110,205,251,210,120,45,10,1,0,10,55,165,330,462,462,330,165,55,11",
			"name": "Triangle read by rows: T(m,n) = number of solutions to 1 \u003c= a(1) \u003c a(2) \u003c ... \u003c a(m) \u003c= n, where gcd( a(1), a(2), ..., a(m), n) = 1.",
			"link": [
				"Temba Shonhiwa, \u003ca href=\"http://www.fq.math.ca/Scanned/37-1/shonhiwa.pdf\"\u003eA Generalization of the Euler and Jordan Totient Functions\u003c/a\u003e, Fib. Quart., 37 (1999), 67-76."
			],
			"example": [
				"From _R. J. Mathar_, Feb 12 2007: (Start)",
				"Triangle begins",
				"  1",
				"  1 1",
				"  0 1  1",
				"  0 2  3   1",
				"  0 2  5   4   1",
				"  0 4 10  10   5   1",
				"  0 2 11  19  15   6   1",
				"  0 6 21  35  35  21   7   1",
				"  0 4 22  52  69  56  28   8  1",
				"  0 6 33  83 126 126  84  36  9  1",
				"  0 4 34 110 205 251 210 120 45 10 1",
				"The inverse of the triangle is",
				"   1",
				"  -1    1",
				"   1   -1    1",
				"  -1    1   -3    1",
				"   1   -1    7   -4    1",
				"  -1    1  -15   10   -5    1",
				"   1   -1   31  -19   15   -6    1",
				"  -1    1  -63   28  -35   21   -7    1",
				"   1   -1  127  -28   71  -56   28   -8    1",
				"  -1    1 -255    1 -135  126  -84   36   -9    1",
				"   1   -1  511   80  255 -251  210 -120   45  -10    1",
				"with row sums 1,0,1,-2,4,-9,22,-55,135,-319,721,...(cf. A038200).",
				"(End)"
			],
			"maple": [
				"A020921 := proc(n,k) option remember ; local divs ; if n \u003c= 0 then 1 ; elif k \u003e n then 0 ; else divs := numtheory[divisors](n) ; add(numtheory[mobius](op(i,divs))*binomial(n/op(i,divs),k),i=1..nops(divs)) ; fi ; end: nmax := 10 ; for row from 0 to nmax do for col from 0 to row do printf(\"%d,\",A020921(row,col)) ; od ; od ; # _R. J. Mathar_, Feb 12 2007"
			],
			"mathematica": [
				"nmax = 11; t[n_, k_] := Total[ MoebiusMu[#]*Binomial[n/#, k] \u0026 /@ Divisors[n]]; t[0, 0] = 1; Flatten[ Table[t[n, k], {n, 0, nmax}, {k, 0, n}]] (* _Jean-François Alcover_, Oct 20 2011, after PARI *)"
			],
			"program": [
				"(PARI) {T(n, k) = if( n\u003c=0, k==0 \u0026\u0026 n==0, sumdiv(n, d, moebius(d) * binomial(n/d, k)))}",
				"(Sage) # uses[DivisorTriangle from A327029]",
				"DivisorTriangle(moebius, binomial, 13) # _Peter Luschny_, Aug 24 2019"
			],
			"xref": [
				"(Left-hand) columns include A000010, A102309. Row sums are essentially A027375.",
				"Cf. A327029."
			],
			"keyword": "nonn,tabl,nice,easy",
			"offset": "0,8",
			"author": "_Michael Somos_, Nov 17 2002",
			"references": 8,
			"revision": 34,
			"time": "2020-03-24T12:37:41-04:00",
			"created": "1998-06-14T03:00:00-04:00"
		}
	]
}