{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A294516",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 294516,
			"data": "1,17,67,2087,40577,315967,8627249,539432053,543008461,7096662277,306487877071,14457409539227,246534893826499,49437672710843,14617658229054773,29294219493288391,1966205309547985477,139821581165897995307,700098935135639210887,55378426713778630607653,4601722042202662057443599,12144567347216934480292961",
			"name": "Numerators of the partial sums of the reciprocals of (k+1)*(4*k+3) = A033991(k+1), for k \u003e= 0.",
			"comment": [
				"The corresponding numerators are given in A294517.",
				"For the general case V(m,r;n) = Sum_{k=0..n} 1/((k + 1)*(m*k + r)) = (1/(m - r))*Sum_{k=0..n} (m/(m*k + r) - 1/(k+1)), for r = 1, ..., m-1 and m = 2, 3, ..., and their limits see a comment in A294512. Here [m,r] = [4,3].",
				"The limit of the series is V(4,3) = lim_{n -\u003e oo} V(4,3;n) = 3*log(2) - Pi/2 = 0.50864521488493930902... given in A294518."
			],
			"reference": [
				"Max Koecher, Klassische elementare Analysis, Birkhäuser, Basel, Boston, 1987, pp. 189 - 193."
			],
			"link": [
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/DigammaFunction.html\"\u003eDigamma Function\u003c/a\u003e"
			],
			"formula": [
				"a(n) = numerator(V(4,3;n)) with V(4,3;n) = Sum_{k=0..n} 1/((k + 1)*(4*k + 3)) = Sum_{k=0..n} 1/A033991(k+1) = Sum_{k=0..n} (4/(4*k + 3) - 1/(k+1)).",
				"V(4,3;n) = 3*log(2) - Pi/2 + Psi(n+7/4) - Psi(n+2) with the digamma function Psi. Note that  Psi(1) - Psi(3/4) = 3*log(2) - Pi/2. - _Wolfdieter Lang_, Nov 15 2017"
			],
			"example": [
				"The rationals V(4,3;n), n \u003e= 0, begin: 1/3, 17/42, 67/154, 2087/4620, 40577/87780, 315967/672980, 8627249/18170460, 539432053/1126568520, 543008461/1126568520, 7096662277/14645390760, 306487877071/629751802680, ...",
				"V(4,3;10^4) = 0.508620219 (Maple, 10 digits) to be compared with 0.508645215 from V(4,3) given in A294518."
			],
			"program": [
				"(PARI) a(n) = numerator(sum(k=0, n, 1/((k + 1)*(4*k + 3)))); \\\\ _Michel Marcus_, Nov 15 2017"
			],
			"xref": [
				"Cf. A294512, A250551(n+1)/A294515(n) (V(4,1;n)), A294517, A294518."
			],
			"keyword": "nonn,frac,easy",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Nov 07 2017",
			"references": 3,
			"revision": 13,
			"time": "2017-11-16T02:39:24-05:00",
			"created": "2017-11-12T14:45:38-05:00"
		}
	]
}