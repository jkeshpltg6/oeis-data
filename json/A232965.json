{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A232965",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 232965,
			"data": "1,3,1,7,11,27,29,47,64,123,199,343,521,843,1331,2207,3571,5832,9349,15127,24389,39603,64079,103823,167761,271443,438976,710647,1149851,1860867,3010349,4870847,7880599,12752043,20633239,33386248,54018521",
			"name": "Number of circular n-bit strings that, when circularly shifted by 3 bits, do not have coincident 1's in any position.",
			"comment": [
				"a(n) = L[n/gcd(n,3)]^gcd(n,3) where L[n] is the Lucas sequence (A000032).",
				"K[n;s] = L[n/gcd(n,s)]^gcd(n,s) counts circular n-bit strings that, when circularly shifted by s bits, do not have coincident 1's in any position. K[n,s] = #{x|((x\u003c\u003c\u003cs)\u0026x) = (0,...,0)}, where \u003c\u003c\u003cs denotes a left circular shift by s bits and \u0026 is the bitwise AND function.",
				"K[n;1] = L[n] is the Lucas sequence; K[n;2] is the Fielder sequence A001638; K[n;3] is this sequence."
			],
			"link": [
				"Rick L. Shepherd, \u003ca href=\"/A232965/b232965.txt\"\u003eTable of n, a(n) for n = 1..4750\u003c/a\u003e"
			],
			"formula": [
				"K[n;3] satisfies the (empirical) linear recurrence a(n) = a(n-1) + a(n-2) - a(n-3) + a(n-4) +a(n-5) + a(n-6) - a(n-7) - a(n-8), n \u003e 8, derived from the denominator polynomial (1+phi^(-1)*x)*(1-phi*x)*(1-phi^(-1)*x^3)*(1+phi*x^3) of the generating function, where phi = (1+sqrt(5)/2), the golden ratio.",
				"Empirical g.f.: -x*(x-1)*(8*x^6+15*x^5+9*x^4+4*x^3+3*x+1) / ((x^2+x-1)*(x^6-x^3-1)). - _Colin Barker_, Oct 10 2015"
			],
			"example": [
				"K[1;3] = L[1] = 1; K[2;3] = L[2] = 3; K[3;3] = L[1] = 1; K[4;3] = L[4] = 7; K[5;3] = L[5] = 11; K[6;3] = L[2]^3 = 27; K[7;3] = L[7] = 29; K[8;3] = L[8] = 47."
			],
			"program": [
				"(C)",
				"int gcd(int n, int s)//Return the gcd of n and s",
				"int raiseToPower(int n, int d)//Return n^d",
				"#define N 40",
				"#define S 3",
				"int Lucas[N+1]  = {2,1,3,4,7,1,18,....}",
				"main()",
				"{",
				"int n;",
				"for(n = 1; n \u003c N; n++)",
				"printf(\"%i: %i\\n\",n,raiseToPower(Lucas[n/gcd(n,S)],gcd(n,S));",
				"return;",
				"}",
				"(PARI)",
				"L(n) = fibonacci(n-1) + fibonacci(n+1);",
				"a(n) = L(n/gcd(n,3))^gcd(n,3) \\\\ _Rick L. Shepherd_, Jan 23 2014"
			],
			"xref": [
				"Cf. A000032 (Lucas sequence), A001638 (Fielder sequence)."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Gideon J. Kuhn_, Dec 02 2013",
			"ext": [
				"More terms from _Rick L. Shepherd_, Jan 23 2014"
			],
			"references": 1,
			"revision": 24,
			"time": "2015-10-10T09:51:31-04:00",
			"created": "2013-12-07T05:17:09-05:00"
		}
	]
}