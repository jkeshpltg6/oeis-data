{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002721",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2721,
			"id": "M4864 N2080",
			"data": "1,12,132,847,3921,14506,45402,124707,308407,699766,1477686,2936517,5540107,9993192,17333536,29048541,47220357,74703832,115341952,174223731,257989821,375191422,536708382,756232687,1050823851,1441543026,1954172962",
			"name": "Number of 3 X 3 X 3 arrays M_ijk (1 \u003c= i,j,k \u003c= 3) with entries satisfying 0 \u003c= M_ijk \u003c= n and all line sums equal to n.",
			"comment": [
				"Number of 3 X 3 X 3 arrays M_ijk (1 \u003c= i,j,k \u003c= 3) satisfying Sum_i M_ijk = n (all j,k), Sum_j M_ijk = n (all i,k), Sum_k M_ijk = n (all i,j) and 0 \u003c= M_ijk \u003c= n.",
				"The constraints imply that Sum_{i,j,k} M_ijk = 9n.",
				"This is a \"magic cube\" in Stanley's notation (see Stanley references). - _N. J. A. Sloane_, Jul 07 2014"
			],
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence).",
				"R. P. Stanley, Enumerative Combinatorics, Wadsworth, Vol. 1, Second Edition, Section 4.6.1."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A002721/b002721.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"A. G. Bell, \u003ca href=\"http://dx.doi.org/10.1093/comjnl/13.3.278\"\u003ePartitioning integers in n dimensions\u003c/a\u003e, The Computer Journal, 13 (1970), 278-283.",
				"R. P. Stanley, \u003ca href=\"/A002721/a002721.pdf\"\u003eExamples of Magic Labelings\u003c/a\u003e, Unpublished Notes, 1973 [Cached copy, with permission]",
				"\u003ca href=\"/index/Rec#order_09\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (9,-36,84,-126,126,-84,36,-9,1)."
			],
			"formula": [
				"a(n) = (1/4032) * m * (m * (m * (31 * m + 1004) + 6820) + 4272) + 1, where m = n*(n+1) (from the Bell reference). -  _Sean A. Irvine_, Jul 01 2014",
				"G.f.: -(x^8+3*x^7+60*x^6+7*x^5+168*x^4+7*x^3+60*x^2+3*x+1) / (x-1)^9. - _Colin Barker_, Jul 01 2014"
			],
			"example": [
				"Comment from _N. J. A. Sloane_, Jul 06 2014. Here are four of the twelve arrays showing that a(1) = 12 (each row shows top face, middle face, bottom face):",
				"-----------",
				"100 010 001",
				"010 001 100",
				"001 100 010",
				"-----------",
				"100 001 010",
				"010 100 001",
				"001 010 100",
				"-----------",
				"001 010 100",
				"010 100 001",
				"100 001 010",
				"-----------",
				"001 100 010",
				"010 001 100",
				"100 010 001",
				"-----------",
				"Each face must show one of the six 3 X 3 permutation matrices. There are 6 choices for the top face, and for each of these there are two choices for the second face and the third face is then determined, for a total of a(1)=6*2*1=12."
			],
			"maple": [
				"A002721:=n-\u003e(1/4032)*n*(n+1)*(n*(n+1)*(n*(n+1)*(31*n*(n+1)+1004)+6820)+ 4272)+1: seq(A002721(n), n=0..30); # _Wesley Ivan Hurt_, Jul 01 2014"
			],
			"mathematica": [
				"CoefficientList[Series[-(x^8 + 3*x^7 + 60*x^6 + 7*x^5 + 168*x^4 + 7*x^3 + 60*x^2 + 3*x + 1)/(x - 1)^9, {x, 0, 30}], x] (* _Wesley Ivan Hurt_, Jul 01 2014 *)"
			],
			"program": [
				"(PARI) Vec(-(x^8+3*x^7+60*x^6+7*x^5+168*x^4+7*x^3+60*x^2+3*x+1)/(x-1)^9 + O(x^100)) \\\\ _Colin Barker_, Jul 01 2014",
				"(MAGMA) [(1/4032)*n*(n+1)*(n*(n+1)*(n*(n+1)*(31*n*(n+1)+1004)+6820)+4272)+1 : n in [0..30] ]; // _Wesley Ivan Hurt_, Jul 01 2014"
			],
			"xref": [
				"See A001496 for the two-dimensional 4 X 4 analog. Cf. also A002817."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_.",
			"ext": [
				"More terms from _Sean A. Irvine_, Jul 01 2014",
				"Edited by _N. J. A. Sloane_, Jul 06 2014"
			],
			"references": 40,
			"revision": 62,
			"time": "2017-06-18T02:17:12-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}