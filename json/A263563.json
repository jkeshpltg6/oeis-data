{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A263563",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 263563,
			"data": "1,2,3,4,5,6,7,8,9,10,11,110,100,12,120,1200,20,13,130,1300,30,14,140,1400,40,15,150,1500,50,16,160,1600,60,17,170,1700,70,18,180,1800,80,19,190,1900,90,21,210,2100,1000,22,220,2200,22000,200,23,230,2300",
			"name": "A self-describing sequence: when the sequence is read as a string of decimal digits, a(n) can be read from position n (ignoring leading zeros). This sequence is the lexicographically earliest sequence of distinct terms with this property.",
			"comment": [
				"Leading zeros that may appear while reading a(n) arise from non-leading zeros in some previous term, and are ignored.",
				"The table in the Example section makes the definition clearer.",
				"This sequence is conjectured to be a permutation of natural numbers, with putative inverse A263564."
			],
			"link": [
				"Paul Tek, \u003ca href=\"/A263563/b263563.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Paul Tek, \u003ca href=\"/A263563/a263563.pl.txt\"\u003ePERL program for this sequence\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"example": [
				"The following table depicts the first few terms:",
				"+----+--------+-----------------------------------+",
				"| n  | a(n)   | a(n) in situation with leading 0s |",
				"+----+--------+-----------------------------------+",
				"|  1 |      1 | 1                                 |",
				"|  2 |      2 |  2                                |",
				"|  3 |      3 |   3                               |",
				"|  4 |      4 |    4                              |",
				"|  5 |      5 |     5                             |",
				"|  6 |      6 |      6                            |",
				"|  7 |      7 |       7                           |",
				"|  8 |      8 |        8                          |",
				"|  9 |      9 |         9                         |",
				"| 10 |     10 |          10                       |",
				"| 11 |     11 |           011                     |",
				"| 12 |    110 |            110                    |",
				"| 13 |    100 |             100                   |",
				"| 14 |     12 |              0012                 |",
				"| 15 |    120 |               0120                |",
				"| 16 |   1200 |                1200               |",
				"| 17 |     20 |                 200               |",
				"| 18 |     13 |                  0013             |",
				"| 19 |    130 |                   0130            |",
				"| 20 |   1300 |                    1300           |",
				"| 21 |     30 |                     300           |",
				"| 22 |     14 |                      0014         |",
				"| 23 |    140 |                       0140        |",
				"| 24 |   1400 |                        1400       |",
				"| 25 |     40 |                         400       |",
				"| 26 |     15 |                          0015     |",
				"| 27 |    150 |                           0150    |",
				"| 28 |   1500 |                            1500   |",
				"| 29 |     50 |                             500   |",
				"| 30 |     16 |                              0016 |",
				"+----+--------+-----------------------------------+",
				"Comments from _N. J. A. Sloane_, Jan 18 2016 (Start): After a(9)=9, the smallest possible choice for a(10) is the first number that has not yet appeared, which is 10. There is no contradiction, so we take a(10)=10.",
				"Now the smallest number that has not yet appeared is 11, and we can achieve a(11)=11 by making the string of digits starting at the 11th place read 011.",
				"Now the string of digits starting at the 12th pace is 11..., and the smallest candidate of that form is 110, which gives a(12)=110.",
				"And so on. (End)"
			],
			"program": [
				"(Perl) See Links section."
			],
			"xref": [
				"Cf. A263443, A263564."
			],
			"keyword": "nonn,base,look,nice",
			"offset": "1,2",
			"author": "_Paul Tek_, Oct 21 2015",
			"references": 3,
			"revision": 22,
			"time": "2016-04-25T12:00:17-04:00",
			"created": "2015-10-27T21:24:59-04:00"
		}
	]
}