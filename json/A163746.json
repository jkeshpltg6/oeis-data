{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A163746",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 163746,
			"data": "1,1,3,1,2,3,0,1,1,2,0,3,2,0,6,1,2,1,0,2,0,0,0,3,3,2,3,0,2,6,0,1,0,2,0,1,2,0,6,2,2,0,0,0,2,0,0,3,1,3,6,2,2,3,0,0,0,2,0,6,2,0,0,1,4,0,0,2,0,0,0,1,2,2,9,0,0,6,0,2,1,2,0,0,4,0,6,0,2,2,0,0,0,0,0,3,2,1,0,3,2,6,0,2,0",
			"name": "Expansion of (theta_3(q)^2 + 3 * theta_3(q^3)^2) / 4 - 1 in powers of q.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"reference": [
				"N. J. Fine, Basic Hypergeometric Series and Applications, Amer. Math. Soc., 1988; p. 82, Eq. (32.53)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A163746/b163746.txt\"\u003eTable of n, a(n) for n = 1..5000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of psi(q) * psi(q^2) * chi(q^3) * chi(-q^6) - 1 in powers of q where psi(), chi() are Ramanujan theta functions.",
				"Expansion of eta(q^2) * eta(q^4)^2 * eta(q^6)^3 / (eta(q) * eta(q^3) * eta(q^12)^2) - 1 in powers of q.",
				"Moebius transform is period 12 sequence [ 1, 0, 2, 0, 1, 0, -1, 0, -2, 0, -1, 0, ...].",
				"a(n) is multiplicative with a(2^e) = 1, a(3^e) = 2-(-1)^e, a(p^e) = e+1 if p == 1 (mod 4), a(p^e) == (1-(-1)^e)/2 if p == 3 (mod 4).",
				"G.f.: Sum_{k\u003e0} (x^k + x^(3*k)) / (1 - x^(2*k) + x^(4*k)).",
				"a(n) = A125061(n) unless n=0. a(12*n + 7) = a(12*n + 11) = 0.",
				"a(2*n) = a(n). a(2*n + 1) = A138741(n). a(3*n + 1) = A122865(n). a(3*n + 2) = A122856(n). - _Michael Somos_, Sep 02 2015"
			],
			"example": [
				"G.f. = q + q^2 + 3*q^3 + q^4 + 2*q^5 + 3*q^6 + q^8 + q^9 + 2*q^10 + 3*q^12 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 1, 0, DivisorSum[ n, (-1)^Quotient[#, 6] {1, 0, 2, 0, 1, 0}[[Mod[#, 6, 1]]] \u0026]]; (* _Michael Somos_, Sep 02 2015 *)",
				"a[ n_] := If[ n \u003c 1, 0, Times @@ (Which[# \u003c 3, 1, # == 3, Mod[#2, 2] 2 + 1, Mod[#, 4] == 1, #2 + 1, True, (1 + (-1)^#2) / 2] \u0026 @@@ FactorInteger @ n)]; (* _Michael Somos_, Sep 02 2015 *)",
				"a[ n_] := SeriesCoefficient[ (EllipticTheta[ 3, 0, q]^2 + 3 EllipticTheta[ 3, 0, q^3]^2) / 4 - 1, {q, 0, n}]; (* _Michael Somos_, Sep 02 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, 0, sumdiv(n, d, ((d%2) * ((d%3==0) + 1)) * (-1)^(d\\6)))};",
				"(PARI) {a(n) = my(A, p, e); if( n\u003c1, 0, A = factor(n); prod( k=1, matsize(A)[1], [p, e] = A[k, ]; if(p==2, 1, p==3, e%2*2 + 1, p%4==1, e+1, 1-e%2)))};",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A) * eta(x^4 + A)^2 * eta(x^6 + A)^3 / (eta(x + A) * eta(x^3 + A) * eta(x^12 + A)^2) - 1, n))};"
			],
			"xref": [
				"Cf. A122856, A122865, A125061, A138741."
			],
			"keyword": "nonn,mult",
			"offset": "1,3",
			"author": "_Michael Somos_, Aug 03 2009",
			"references": 6,
			"revision": 12,
			"time": "2021-03-12T22:24:46-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}