{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A301303",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 301303,
			"data": "1,1,1,1,3,3,1,1,3,3,2,2,2,4,2,1,3,5,3,5,5,4,3,3,3,5,3,3,4,6,2,1,4,3,4,5,3,4,2,3,6,5,2,4,6,4,2,2,3,6,3,3,4,6,4,4,5,4,4,5,2,4,4,1,7,8,2,7,8,5,3,5,5,7,4,5,6,5,2,5",
			"name": "Number of ways to write n as x^2 + y^2 + z^2 + w^2 with x \u003e= y \u003e= 0 \u003c= z \u003c= w such that x^2 + 23*y^2 = 2^k*m^3 for some k = 0,1,2 and m = 1,2,3,....",
			"comment": [
				"Conjecture: a(n) \u003e 0 for all n \u003e 0, and a(n) = 1 only for n = 2^k (k = 0,1,2,...), 3, 7, 115, 151, 219, 267, 1151, 1367.",
				"We have verified a(n) \u003e 0 for all n = 1..10^8.",
				"See also A301304 and A301314 for similar conjectures."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A301303/b301303.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2016.11.008\"\u003eRefining Lagrange's four-square theorem\u003c/a\u003e, J. Number Theory 175(2017), 167-190.",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1701.05868\"\u003eRestricted sums of four squares\u003c/a\u003e, arXiv:1701.05868 [math.NT], 2017-2018."
			],
			"example": [
				"a(2) = 1 since 2 = 1^2 + 0^2 + 0^2 + 1^2 with 1 + 23*0 = 1^3.",
				"a(4) = 1 since 4 = 2^2 + 0^2 + 0^2 + 0^2 with 2^2 + 23*0 = 2^2*1^3.",
				"a(7) = 1 since 7 = 2^2 + 1^2 + 1^2 + 1^2 with 2^2 + 23*1^2 = 3^3.",
				"a(48) = 2 since 48 = 4^2 + 0^2 + 4^2 + 4^2 = 6^2 + 2^2 + 2^2 + 2^2 with 4^2 + 23*0^2 = 2*2^3 and 6^2 + 23*2^2 = 2*4^3.",
				"a(115) = 1 since 115 = 3^2 + 3^2 + 4^2 + 9^2 with 3^2 + 23*3^2 = 6^3.",
				"a(267) = 1 since 267 = 3^2 + 1^2 + 1^2 + 16^2 with 3^2 + 23*1^2 = 2^2*2^3.",
				"a(1151) = 1 since 1151 = 7^2 + 3^2 + 2^2 + 33^2 with 7^2 + 23*3^2 = 2^2*4^3.",
				"a(1367) = 1 since 1367 = 17^2 + 5^2 + 18^2 + 27^2 with 17^2 + 23*5^2 = 2^2*6^3."
			],
			"mathematica": [
				"SQ[n_]:=SQ[n]=IntegerQ[Sqrt[n]];",
				"CQ[n_]:=CQ[n]=IntegerQ[n^(1/3)];",
				"QQ[n_]:=n\u003e0\u0026\u0026(CQ[n]||CQ[n/2]||CQ[n/4]);",
				"tab={};Do[r=0;Do[If[QQ[x^2+23y^2],Do[If[SQ[n-x^2-y^2-z^2],r=r+1],{z,0,Sqrt[(n-x^2-y^2)/2]}]],{y,0,Sqrt[n/2]},{x,y,Sqrt[n-y^2]}];tab=Append[tab,r],{n,1,80}];Print[tab]"
			],
			"xref": [
				"Cf. A000118, A000290, A271510, A271513, A271518, A281976, A300666, A300667, A300708, A300712, A300751, A300752, A300791, A300792, A300844, A300908, A301304, A301314."
			],
			"keyword": "nonn",
			"offset": "1,5",
			"author": "_Zhi-Wei Sun_, Mar 17 2018",
			"references": 4,
			"revision": 12,
			"time": "2018-03-19T04:20:43-04:00",
			"created": "2018-03-19T04:20:43-04:00"
		}
	]
}