{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A225478",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 225478,
			"data": "1,3,4,21,40,16,231,524,336,64,3465,8784,7136,2304,256,65835,180756,170720,72320,14080,1024,1514205,4420728,4649584,2346240,613120,79872,4096,40883535,125416476,143221680,81946816,25939200,4609024,430080,16384,1267389585,4051444896,4941537984,3113238016,1131902464,246636544,31768576,2228224,65536",
			"name": "Triangle read by rows, 4^k*s_4(n, k) where s_m(n, k) are the Stirling-Frobenius cycle numbers of order m; n \u003e= 0, k \u003e= 0.",
			"comment": [
				"Triangle T(n,k), read by rows, given by (3, 4, 7, 8, 11, 12, 15, 16, ... (A014601)) DELTA (4, 0, 4, 0, 4, 0, 4, 0, ...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, May 14 2015."
			],
			"link": [
				"Peter Luschny, \u003ca href=\"http://www.luschny.de/math/euler/GeneralizedEulerianPolynomials.html\"\u003eGeneralized Eulerian polynomials.\u003c/a\u003e",
				"Peter Luschny, \u003ca href=\"http://www.luschny.de/math/euler/StirlingFrobeniusNumbers.html\"\u003eThe Stirling-Frobenius numbers.\u003c/a\u003e"
			],
			"formula": [
				"For a recurrence see the Sage program.",
				"T(n,k) = 4^k * A225471(n,k). - _Philippe Deléham_, May 14 2015."
			],
			"example": [
				"[n\\k][    0,       1,       2,       3,      4,     5,    6 ]",
				"[0]       1,",
				"[1]       3,       4,",
				"[2]      21,      40,      16,",
				"[3]     231,     524,     336,      64,",
				"[4]    3465,    8784,    7136,    2304,    256,",
				"[5]   65835,  180756,  170720,   72320,  14080,  1024,",
				"[6] 1514205, 4420728, 4649584, 2346240, 613120, 79872, 4096."
			],
			"mathematica": [
				"s[_][0, 0] = 1; s[m_][n_, k_] /; (k \u003e n || k \u003c 0) = 0; s[m_][n_, k_] := s[m][n, k] = s[m][n - 1, k - 1] + (m*n - 1)*s[m][n - 1, k];",
				"T[n_, k_] := 4^k*s[4][n, k];",
				"Table[T[n, k], {n, 0, 8}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Aug 02 2019 *)"
			],
			"program": [
				"(Sage)",
				"@CachedFunction",
				"def SF_CS(n, k, m):",
				"    if k \u003e n or k \u003c 0 : return 0",
				"    if n == 0 and k == 0: return 1",
				"    return m*SF_CS(n-1, k-1, m) + (m*n-1)*SF_CS(n-1, k, m)",
				"for n in (0..8): [SF_CS(n, k, 4) for k in (0..n)]"
			],
			"xref": [
				"T(n, 0) ~ A008545; T(n, n) ~ A000302; T(n, n-1) ~ A002700.",
				"row sums ~ A034176; alternating row sums ~ A008545.",
				"Cf. A225471, A132393 (m=1), A028338 (m=2), A225477 (m=3)."
			],
			"keyword": "nonn,tabl",
			"offset": "0,2",
			"author": "_Peter Luschny_, May 17 2013",
			"references": 1,
			"revision": 17,
			"time": "2019-08-02T13:45:33-04:00",
			"created": "2013-05-21T14:08:04-04:00"
		}
	]
}