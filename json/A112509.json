{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A112509",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 112509,
			"data": "1,3,5,7,10,13,17,22,27,33,40,47,55,64,73,83,94,106,118,131,145,160,176,192,209,227,246,265,285,306,328",
			"name": "Maximum number of numbers represented by substrings of an n-bit number's binary representation.",
			"comment": [
				"Substrings must be contiguous, they are treated as stand-alone binary representations and the reversal of substrings is not permitted."
			],
			"link": [
				"\u003ca href=\"http://www.bmoc.maths.org/home/bmo2-2009.pdf\"\u003e2008/9 British Mathematical Olympiad Round 2: Jan 29 2009\u003c/a\u003e, Problem 4 [From _Joseph Myers_, Feb 01 2009]"
			],
			"example": [
				"To see why a(4)=7 (and A112510(4)=12 and A112511(4)=14), consider all numbers whose binary representations require exactly 4 bits: 1000, 1001, 1010, 1011, 1100, 1101, 1110 and 1111. For each of these binary representations in turn, we find the nonnegative integers represented by all of its contiguous substrings. We count these distinct integer values (putting the count in {}s):",
				"1000: any 0, either 00, or 000 -\u003e 0, 1 -\u003e 1, 10 -\u003e 2, 100 -\u003e 4, 1000 -\u003e 8 {5};",
				"1001: either 0, or 00 -\u003e 0, either 1, 01, or 001 -\u003e 1, 10 -\u003e 2, 100 -\u003e 4, 1001 -\u003e 9 {5};",
				"(For brevity, binary substrings are shown below only if they produce values not shown yet.)",
				"1010: 0, 1, 2, 101 -\u003e 5, 1010 -\u003e 10 {5};",
				"1011: 0, 1, 2, 11 -\u003e 3, 5, 1011 -\u003e 11 {6};",
				"1100: 0, 1, 2, 3, 4, 110 -\u003e 6, 1100 -\u003e 12 {7};",
				"1101: 0, 1, 2, 3, 5, 6, 1101 -\u003e 13 {7};",
				"1110: 0, 1, 2, 3, 6, 111 -\u003e 7, 1110 -\u003e 14 {7};",
				"1111: 1, 3, 7, 1111 -\u003e 15 {4}.",
				"Because the maximum number of distinct integer values {in brackets} is 7, a(4)=7. The smallest 4-bit number for which 7 distinct values are found is 12, so A112510(4)=12. The largest 4-bit number for which 7 are found is 14, so A112511(4)=14. (For n=4 the count is a(n)=7 also for all values (only one, 13, here) between A112510(n) and A112511(n). This is not the case in general.)."
			],
			"xref": [
				"Cf. A112510 (least n-bit number for which this maximum occurs), A112511 (greatest n-bit number for which this maximum occurs).",
				"A078822, A122953, A156022, A156023, A156024, A156025. Equals A156022(n)+1 for n \u003e= 2. [From _Joseph Myers_, Feb 01 2009]"
			],
			"keyword": "base,nonn",
			"offset": "1,2",
			"author": "_Rick L. Shepherd_, Sep 09 2005",
			"ext": [
				"a(21) to a(31) from _Joseph Myers_, Feb 01 2009"
			],
			"references": 6,
			"revision": 5,
			"time": "2012-03-30T17:36:43-04:00",
			"created": "2005-09-21T03:00:00-04:00"
		}
	]
}