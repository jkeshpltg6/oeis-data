{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A327597",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 327597,
			"data": "1,2,2,5,8,7,16,3,20,6,9,16,13,30,22,53,76,65,142,104,19,124,36,161,198,20,219,80,15,32,3,12,4,17,22,20,43,64,27,92,30,41,72,19,92,28,121,150,136,287,424,89,514,302,817,1120,969,110,108,73,182,128,311",
			"name": "a(n) = numerator((a(n-1) + a(n-2) + 1)/a(n-1)), with a(1)=1, a(2)=2.",
			"comment": [
				"As n goes to infinity, 32 \u003c (a(n+1) + a(n))/(a(n+1) - a(n)) \u003c 32.35 (conjectured).",
				"This sequence is the continued fraction expansion of ~ 1.4072426692639398147... (calculated to 19 digits).",
				"The sum of the reciprocals of this sequence is ~ 4.97804721273463... (calculated to 14 digits).",
				"The average reduction 0.621 \u003c a(n)/(a(n-1)+a(n-2)+1) \u003c 0.622 (conjectured); the average reduction is the average of the individual reductions (The change from the numerator to the numerator in the simplest form):",
				"a(3)/(a(2)+a(1)+1) -\u003e 2/(2+1+1) -\u003e 0.5",
				"a(4)/(a(3)+a(2)+1) -\u003e 5/(2+2+1) -\u003e 1",
				"a(5)/(a(4)+a(3)+1) -\u003e 8/(5+2+1) -\u003e 1",
				"a(6)/(a(5)+a(4)+1) -\u003e 7/(8+5+1) -\u003e 0.5",
				"a(7)/(a(6)+a(5)+1) -\u003e 16/(7+8+1) -\u003e 1",
				"a(8)/(a(7)+a(6)+1) -\u003e 3/(16+7+1) -\u003e 0.125",
				"a(9)/(a(8)+a(7)+1) -\u003e 20/(3+16+1) -\u003e 1",
				"a(10)/(a(9)+a(8)+1) -\u003e 6/(20+3+1) -\u003e 0.25",
				"That number is the average of these to ~ 100000 terms (There was some fluctuation to take into account)."
			],
			"link": [
				"Vimal Vinod, \u003ca href=\"/A327597/b327597.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"It appears that this sequence's growth can be approximated by a(n) ~ (1 + 1/c)^n where 17.8 \u003c c \u003c 18.5."
			],
			"example": [
				"a(1) = 1.",
				"a(2) = 2.",
				"a(3) = numerator((1 + 2 + 1)/2) -\u003e numerator(2/1) = 2.",
				"a(4) = numerator((2 + 2 + 1)/2) -\u003e numerator(5/2) = 5.",
				"a(5) = numerator((2 + 5 + 1)/5) -\u003e numerator(8/5) = 8.",
				"a(6) = numerator((5 + 8 + 1)/8) -\u003e numerator(7/4) = 7."
			],
			"mathematica": [
				"Nest[Append[#, Numerator[(#2 + #1 + 1)/#2] \u0026 @@ #[[-2 ;; -1]]] \u0026, {1, 2}, 61] (* _Michael De Vlieger_, Sep 30 2019 *)"
			],
			"program": [
				"(Python)",
				"from fractions import Fraction",
				"num_terms = 100",
				"S = [1, 2]",
				"for n in range(num_terms-2):",
				"    s = Fraction((S[n]+S[n+1]+1),S[n+1]).numerator",
				"    S.append(s)",
				"print(S) # Should print the sequence to the length specified.",
				"(PARI) lista(nn) = {my(xa = 1, ya = 2, za); print1(xa, \", \", ya, \", \"); for (n=3, nn, za = numerator((ya + xa + 1)/ya); print1(za, \", \"); xa = ya; ya = za;);} \\\\ _Michel Marcus_, Sep 24 2019",
				"(MAGMA) a:=[1,2]; [n le 2 select a[n] else Numerator((Self(n-1) + Self(n-2) + 1)/Self(n-1)):n in [1..64]]; // _Marius A. Burtea_, Sep 27 2019"
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Vimal Vinod_, Sep 18 2019",
			"references": 1,
			"revision": 61,
			"time": "2019-09-30T21:56:33-04:00",
			"created": "2019-09-23T13:51:28-04:00"
		}
	]
}