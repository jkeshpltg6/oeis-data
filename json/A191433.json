{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A191433",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 191433,
			"data": "1,3,2,8,5,4,21,13,10,6,55,34,26,16,7,144,89,68,42,18,9,377,233,178,110,47,24,11,987,610,466,288,123,63,29,12,2584,1597,1220,754,322,165,76,31,14,6765,4181,3194,1974,843,432,199,81,37,15,17711,10946",
			"name": "Dispersion of ([nx+n+1/2]), where x=(golden ratio) and [ ]=floor, by antidiagonals.",
			"comment": [
				"Background discussion:  Suppose that s is an increasing sequence of positive integers, that the complement t of s is infinite, and that t(1)=1.  The dispersion of s is the array D whose n-th row is (t(n), s(t(n)), s(s(t(n)), s(s(s(t(n)))), ...).  Every positive integer occurs exactly once in D, so that, as a sequence, D is a permutation of the positive integers.  The sequence u given by u(n)=(number of the row of D that contains n) is a fractal sequence.  Examples:",
				"(1) s=A000040 (the primes), D=A114537, u=A114538.",
				"(2) s=A022343 (without initial 0), D=A035513 (Wythoff array), u=A003603.",
				"(3) s=A007067, D=A035506 (Stolarsky array), u=A133299.",
				"More recent examples of dispersions: A191426-A191455."
			],
			"example": [
				"Northwest corner:",
				"1....3....8....21...55...144",
				"2....5....13...34...89...233",
				"4....10...26...68...178..466",
				"6....16...42...110..288..754",
				"7....18...47...123..322..843"
			],
			"mathematica": [
				"(* Program generates the dispersion array T of increasing sequence f[n] *)",
				"r = 40; r1 = 12;  (* r=# rows of T, r1=# rows to show *)",
				"c = 40; c1 = 12;  (* c=# cols of T, c1=# cols to show *)",
				"x = 1 + GoldenRatio;",
				"f[n_] := Floor[n*x + 1/2] (* f(n) is complement of column 1 *)",
				"mex[list_] := NestWhile[#1 + 1 \u0026, 1, Union[list][[#1]] \u003c= #1 \u0026, 1,",
				"  Length[Union[list]]]",
				"rows = {NestList[f, 1, c]};",
				"Do[rows = Append[rows, NestList[f, mex[Flatten[rows]], r]], {r}];",
				"t[i_, j_] := rows[[i, j]];",
				"TableForm[Table[t[i, j], {i, 1, 10}, {j, 1, 10}]] (* A191433 array *)",
				"Flatten[Table[t[k, n - k + 1], {n, 1, c1}, {k, 1, n}]] (* A191433 sequence *)",
				"(* Program by _Peter J. C. Moses_, Jun 01 2011 *)"
			],
			"xref": [
				"Cf. A114537, A035513, A035506."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Jun 03 2011",
			"references": 1,
			"revision": 10,
			"time": "2014-02-14T00:28:54-05:00",
			"created": "2011-06-05T06:29:51-04:00"
		}
	]
}