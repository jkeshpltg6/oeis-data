{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A242593",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 242593,
			"data": "1,2,4,6,2,10,4,2,16,10,4,2,26,20,12,4,2,42,40,26,14,4,2,68,76,58,32,16,4,2,110,142,120,78,38,18,4,2,178,260,244,172,100,44,20,4,2,288,470,482,374,232,124,50,22,4,2,466,840,936,784,534,300,150,56,24,4,2,754,1488,1788,1612,1176,726,376,178,62,26,4,2",
			"name": "Triangular array read by rows: T(n,k) is the number of length n words on {B,G} that contain exactly k occurrences of the contiguous substrings BGB or GBG.  The substrings are allowed to overlap; n\u003e=0, 0\u003c=k\u003c=max(n-2,0).",
			"comment": [
				"Equivalently, T(n,k) is the number of ways to arrange n children in a line so that exactly k children are in between two children of opposite gender than their own. Children on the ends of the line cannot be counted as \"in between\".",
				"Row sums = 2^n.",
				"Column k=0 is A128588."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A242593/b242593.txt\"\u003eRows n = 0..150, flattened\u003c/a\u003e"
			],
			"formula": [
				"G.f.: 1/(1 - 2*x - 2*(y-1)*x^3/(1 - (y-1)*x - (y-1)*x^2) )."
			],
			"example": [
				"Triangle T(n,k) begins:",
				"    1;",
				"    2;",
				"    4;",
				"    6,   2;",
				"   10,   4,   2;",
				"   16,  10,   4,   2;",
				"   26,  20,  12,   4,   2;",
				"   42,  40,  26,  14,   4,  2;",
				"   68,  76,  58,  32,  16,  4,  2;",
				"  110, 142, 120,  78,  38, 18,  4, 2,",
				"  178, 260, 244, 172, 100, 44, 20, 4, 2;",
				"T(4,1) = 4 because we have: BBGB, BGBB, GBGG, GGBG.",
				"T(4,2) = 2 because we have: BGBG, GBGB."
			],
			"maple": [
				"b:= proc(n, t) option remember; `if`(n=0, 1, expand(",
				"      b(n-1, [4, 3, 4, 4, 3][t])*`if`(t=5, x, 1)+",
				"      b(n-1, [2, 2, 5, 5, 2][t])*`if`(t=3, x, 1)))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p,x,i), i=0..degree(p)))(b(n, 1)):",
				"seq(T(n), n=0..16);  # _Alois P. Heinz_, May 18 2014"
			],
			"mathematica": [
				"nn=10;sol=Solve[{A==va(z^3+z^2A+z B),B==va(z^3+z^2 B + z A)},{A,B}]; Fz[z_,y_]:=Simplify[1/(1-2z-A-B)/.sol/.va-\u003ey-1]; Map[Select[#,#\u003e0\u0026]\u0026, Level[CoefficientList[Series[Fz[z,y],{z,0,nn}],{z,y}],{2}]]//Grid"
			],
			"xref": [
				"Cf. A128588."
			],
			"keyword": "nonn,tabf",
			"offset": "0,2",
			"author": "_Geoffrey Critzer_, May 18 2014",
			"references": 2,
			"revision": 19,
			"time": "2018-04-28T17:23:46-04:00",
			"created": "2014-05-18T18:24:07-04:00"
		}
	]
}