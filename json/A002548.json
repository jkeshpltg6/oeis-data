{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002548",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2548,
			"id": "M4822 N2063",
			"data": "1,1,12,6,180,10,560,1260,12600,1260,166320,13860,2522520,2702700,2882880,360360,110270160,2042040,775975200,162954792,56904848,2586584,1427794368,892371480,116008292400,120470149800,1124388064800",
			"name": "Denominators of coefficients for numerical differentiation.",
			"comment": [
				"Denominator of 1 - 2*HarmonicNumber(n-1)/n. - _Eric W. Weisstein_, Apr 15 2004",
				"Denominator of u(n) = sum( k=1, n-1, 1/(k(n-k)) ) (u(n) is asymptotic to 2*log(n)/n). - _Benoit Cloitre_, Apr 12 2003; corrected by _Istvan Mezo_, Oct 29 2012",
				"Expected area of the convex hull of n points picked at random inside a triangle with unit area. - _Eric W. Weisstein_, Apr 15 2004"
			],
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A002548/b002548.txt\"\u003eTable of n, a(n) for n = 2..250\u003c/a\u003e",
				"W. G. Bickley and J. C. P. Miller, \u003ca href=\"http://dx.doi.org/10.1080/14786444208521334\"\u003eNumerical differentiation near the limits of a difference table\u003c/a\u003e, Phil. Mag., 33 (1942), 1-12 (plus tables).",
				"W. G. Bickley and J. C. P. Miller, \u003ca href=\"/A002551/a002551.pdf\"\u003eNumerical differentiation near the limits of a difference table\u003c/a\u003e, Phil. Mag., 33 (1942), 1-12 (plus tables) [Annotated scanned copy]",
				"A. N. Lowan, H. E. Salzer and A. Hillman, \u003ca href=\"http://projecteuclid.org/euclid.bams/1183504875\"\u003eA table of coefficients for numerical differentiation\u003c/a\u003e, Bull. Amer. Math. Soc., 48 (1942), 920-924.",
				"A. N. Lowan, H. E. Salzer and A. Hillman, \u003ca href=\"/A002545/a002545.pdf\"\u003eA table of coefficients for numerical differentiation\u003c/a\u003e, Bull. Amer. Math. Soc., 48 (1942), 920-924. [Annotated scanned copy]",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/TrianglePointPicking.html\"\u003eTriangle Point Picking\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/SimplexSimplexPicking.html\"\u003eSimplex Simplex Picking\u003c/a\u003e",
				"\u003ca href=\"/index/J#Josephus\"\u003eIndex entries for sequences related to the Josephus Problem\u003c/a\u003e"
			],
			"formula": [
				"G.f.: (-log(1-x))^2 (for fractions A002547(n)/A002548(n)).",
				"A002547(n)/a(n) = 2*Stirling_1(n+2, 2)(-1)^n/(n+2)!."
			],
			"example": [
				"0, 0, 1/12, 1/6, 43/180, 3/10, 197/560, 499/1260, 5471/12600, ..."
			],
			"maple": [
				"seq(denom(Stirling1(j+2,2)/(j+2)!*2!*(-1)^j), j=0..50);"
			],
			"mathematica": [
				"Table[Denominator[1 - 2*HarmonicNumber[n - 1]/n], {n, 2, 30}] (* _Wesley Ivan Hurt_, Mar 24 2014 *)"
			],
			"xref": [
				"Cf. A002547, A093762."
			],
			"keyword": "nonn,frac",
			"offset": "2,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms, GF, formula, Maple code from Barbara Margolius (b.margolius(AT)math.csuohio.edu), Jan 19 2002",
				"Edited by _N. J. A. Sloane_ at the suggestion of _Andrew S. Plewe_, Jun 16 2007"
			],
			"references": 9,
			"revision": 56,
			"time": "2020-06-23T18:43:36-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}