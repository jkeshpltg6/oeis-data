{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A276544",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 276544,
			"data": "1,0,1,0,2,1,0,4,4,1,0,9,15,6,1,0,16,49,37,9,1,0,35,160,183,76,12,1,0,66,498,876,542,142,16,1,0,133,1544,3930,3523,1346,242,20,1,0,261,4715,17179,21392,11511,2980,390,25,1",
			"name": "Triangle read by rows: T(n,k) = number of primitive (aperiodic) reversible string structures with n beads using exactly k different colors.",
			"comment": [
				"A string and its reverse are considered to be equivalent. Permuting the colors will not change the structure."
			],
			"reference": [
				"M. R. Nester (1999). Mathematical investigations of some plant interaction designs. PhD Thesis. University of Queensland, Brisbane, Australia. [See A056391 for pdf file of Chap. 2]"
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A276544/b276544.txt\"\u003eTable of n, a(n) for n = 1..1275\u003c/a\u003e (first 50 rows)"
			],
			"formula": [
				"T(n, k) = Sum_{d|n} mu(n/d) * A284949(d, k)."
			],
			"example": [
				"Triangle starts",
				"1",
				"0   1",
				"0   2    1",
				"0   4    4     1",
				"0   9   15     6     1",
				"0  16   49    37     9     1",
				"0  35  160   183    76    12    1",
				"0  66  498   876   542   142   16   1",
				"0 133 1544  3930  3523  1346  242  20  1",
				"0 261 4715 17179 21392 11511 2980 390 25 1",
				"...",
				"Primitive reversible word structures are:",
				"n=1: a =\u003e 1",
				"n=2: ab =\u003e 1",
				"n=3: aab, aba; abc =\u003e 2 + 1",
				"n=4: aaab, aaba, aabb, abba =\u003e 4 (k=2)",
				"     aabc, abac, abbc, abca =\u003e 4 (k=3)"
			],
			"mathematica": [
				"Ach[n_, k_] := Ach[n, k] = Switch[k, 0, If[n == 0, 1, 0], 1, If[n \u003e 0, 1, 0], _, If[OddQ[n], Sum[Binomial[(n - 1)/2, i] Ach[n - 1 - 2 i, k - 1], {i, 0, (n - 1)/2}], Sum[Binomial[n/2 - 1, i] (Ach[n - 2 - 2 i, k - 1] + 2^i Ach[n - 2 - 2 i, k - 2]), {i, 0, n/2 - 1}]]]",
				"T[n_, k_] := DivisorSum[n, MoebiusMu[n/#] (StirlingS2[#, k] + Ach[#, k])/2\u0026 ];",
				"Table[T[n, k], {n, 1, 10}, {k, 1, n}] // Flatten (* _Jean-François Alcover_, Jun 29 2018, after _Robert A. Russell_ and _Andrew Howroyd_ *)"
			],
			"program": [
				"(PARI) \\\\ here Ach is A304972 as matrix.",
				"Ach(n,m=n)={my(M=matrix(n, m, i, k, i\u003e=k)); for(i=3, n, for(k=2, m, M[i, k]=k*M[i-2, k] + M[i-2, k-1] + if(k\u003e2, M[i-2, k-2]))); M}",
				"T(n,m=n)={my(M=matrix(n, m, i, k, stirling(i, k, 2)) + Ach(n,m)); matrix(n, m, i, k, sumdiv(i, d, moebius(i/d)*M[d,k]))/2}",
				"{ my(A=T(10)); for(n=1, #A, print(A[n, 1..n])) } \\\\ _Andrew Howroyd_, Jan 09 2020"
			],
			"xref": [
				"Columns 2-6 are A056336, A056337, A056338, A056339, A056340.",
				"Partial row sums include A056331, A056332, A056333, A056334, A056335.",
				"Row sums are A276549.",
				"Cf. A284871, A284949, A277504, A137651, A107424, A276543."
			],
			"keyword": "nonn,tabl",
			"offset": "1,5",
			"author": "_Andrew Howroyd_, Apr 09 2017",
			"references": 10,
			"revision": 20,
			"time": "2020-01-10T05:31:34-05:00",
			"created": "2017-04-09T22:33:03-04:00"
		}
	]
}