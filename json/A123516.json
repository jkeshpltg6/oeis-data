{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A123516",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 123516,
			"data": "1,2,-1,8,-8,3,48,-72,54,-15,384,-768,864,-480,105,3840,-9600,14400,-12000,5250,-945,46080,-138240,259200,-288000,189000,-68040,10395,645120,-2257920,5080320,-7056000,6174000,-3333960,1018710,-135135,10321920,-41287680,108380160,-180633600,197568000",
			"name": "Triangle read by rows: T(n,k) = (-1)^k * n! * 2^(n-2*k) * binomial(n,k) * binomial(2*k,k) (0\u003c=k\u003c=n).",
			"comment": [
				"Row sums yield the double factorial numbers (A001147)."
			],
			"reference": [
				"B. T. Gill, Math. Magazine, vol. 79, No. 4, 2006, p. 313, problem 1729."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A123516/b123516.txt\"\u003eTable of n, a(n) for the first 50 rows, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n,0) = 2^n * n! = A000165(n).",
				"T(n,n) = (-1)^n*A001147(n)."
			],
			"example": [
				"Triangle begins:",
				"1;",
				"2,     -1;",
				"8,     -8,      3;",
				"48,    -72,     54,     -15;",
				"384,   -768,    864,    -480,    105;",
				"3840,  -9600,   14400,  -12000,  5250,   -945;",
				"46080, -138240, 259200, -288000, 189000, -68040, 10395;",
				"..."
			],
			"maple": [
				"T:=(n,k)-\u003e(-1)^k*n!*2^(n-2*k)*binomial(n,k)*binomial(2*k,k): for n from 0 to 8 do seq(T(n,k),k=0..n) od; # yields sequence in triangular form"
			],
			"mathematica": [
				"Table[(-1)^k*n! 2^(n - 2 k)*Binomial[n, k]*Binomial[2*k, k], {n, 0, 10}, {k, 0, n}] // Flatten (* _G. C. Greubel_, Oct 14 2017 *)"
			],
			"program": [
				"(PARI) for(n=0,10, for(k=0,n, print1((-1)^k*n!*2^(n-2*k)*binomial(n,k)* binomial(2*k,k), \", \"))) \\\\ _G. C. Greubel_, Oct 14 2017",
				"(MAGMA) /* As triangle * / [[(-1)^k*Factorial(n)*2^(n-2*k)* Binomial(n,k)*Binomial(2*k,k): k in [0..n]]: n in [0.. 15]]; // _Vincenzo Librandi_, Oct 15 2017"
			],
			"xref": [
				"Cf. A001147, A000165."
			],
			"keyword": "sign,tabl",
			"offset": "0,2",
			"author": "_Emeric Deutsch_, Oct 14 2006",
			"references": 1,
			"revision": 12,
			"time": "2017-10-15T01:33:31-04:00",
			"created": "2006-12-06T03:00:00-05:00"
		}
	]
}