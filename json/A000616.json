{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000616",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 616,
			"id": "M0819 N0310 N1026",
			"data": "1,2,3,6,22,402,1228158,400507806843728,527471432057653004017274030725792,11218076601767519586965281984173341005925142853855481024470471657123840",
			"name": "a(-1)=1 by convention; for n \u003e= 0, a(n) = number of irreducible Boolean functions of n variables.",
			"comment": [
				"Number of NP-equivalence classes of switching functions of n or fewer variables.",
				"Number of inequivalent binary nonlinear codes of length n (and all sizes).",
				"a(n+1) = number of NPN-equivalence classes of canalizing functions (see A102449) with n variables. NPN-equivalence allows complementing the function value as well as the individual variables. E.g., the 6 inequivalent canalizing functions when n=3 are 0, x, x AND y, x AND y AND z, x AND (y OR z), x AND (y XOR z). - _Don Knuth_, Aug 24 2005, Aug 06 2006"
			],
			"reference": [
				"F. Harary and E. M. Palmer, Graphical Enumeration, Academic Press, NY, 1973, p. 112.",
				"M. A. Harrison, The number of transitivity sets of Boolean functions, J. Soc. Indust. Appl. Math., 11 (1963), 806-828.",
				"M. A. Harrison, Introduction to Switching and Automata Theory. McGraw Hill, NY, 1965, p. 149.",
				"D. E. Knuth, The Art of Computer Programming, Vol. 4A, Section 7.1.1, p. 79.",
				"S. Muroga, Threshold Logic and Its Applications. Wiley, NY, 1971, p. 38, Table 2.3.2. - Row 11.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence).",
				"I. Tomescu, Introducere in Combinatorica. Editura Tehnica, Bucharest, 1972, p. 129."
			],
			"link": [
				"Marcus Ritt, \u003ca href=\"/A000616/b000616.txt\"\u003eTable of n, a(n) for n = -1..10\u003c/a\u003e",
				"B. Elspas, \u003ca href=\"/A000610/a000610.pdf\"\u003eSelf-complementary symmetry types of Boolean functions\u003c/a\u003e,  IEEE Transactions on Electronic Computers 2, no. EC-9 (1960): 264-266. [Annotated scanned copy]",
				"Steven R. Finch, \u003ca href=\"https://doi.org/10.1017/9781316997741\"\u003eMathematical Constants II\u003c/a\u003e, Encyclopedia of Mathematics and Its Applications, Cambridge University Press, Cambridge, 2018.",
				"R. K. Guy, \u003ca href=\"/A003320/a003320.pdf\"\u003eLetter to N. J. A. Sloane, Mar 1974\u003c/a\u003e",
				"S. Muroga, \u003ca href=\"/A000371/a000371.pdf\"\u003eThreshold Logic and Its Applications\u003c/a\u003e, Wiley, NY, 1971 [Annotated scans of a few pages]",
				"S. Muroga, T. Tsuboi and C. R. Baugh, \u003ca href=\"/A002077/a002077.pdf\"\u003eEnumeration of threshold functions of eight variables\u003c/a\u003e, IEEE Trans. Computers, 19 (1970), 818-825. [Annotated scanned copy]",
				"J. Sklansky, \u003ca href=\"https://doi.org/10.1109/PGEC.1963.263627\"\u003eGeneral synthesis of tributary switching networks\u003c/a\u003e, IEEE Trans. Elect. Computers, 12 (1963), 464-469.",
				"I. Toda, \u003ca href=\"https://doi.org/10.1109/TEC.1962.5219361\"\u003eOn the number of types of self-dual logical functions\u003c/a\u003e, IEEE Trans. Electron. Comput., 11 (1962), 282-284.",
				"I. Toda, \u003ca href=\"/A001531/a001531.pdf\"\u003eOn the number of types of self-dual logical functions\u003c/a\u003e (annotated scanned copy)",
				"\u003ca href=\"/index/Bo#Boolean\"\u003eIndex entries for sequences related to Boolean functions\u003c/a\u003e"
			],
			"formula": [
				"Harrison gives a simple formula in terms of the cycle index of the appropriate group."
			],
			"xref": [
				"Row sums of A039754.",
				"Cf. A102449, A109460, A109462."
			],
			"keyword": "nonn,easy,nice",
			"offset": "-1,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Vladeta Jovovic_",
				"Entry revised by _N. J. A. Sloane_, Aug 07 2006",
				"Terms a(9) and a(10) (given in b-file) from _Marcus Ritt_, Aug 13 2013"
			],
			"references": 23,
			"revision": 60,
			"time": "2019-01-21T11:39:47-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}