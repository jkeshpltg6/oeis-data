{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A060821",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 60821,
			"data": "1,0,2,-2,0,4,0,-12,0,8,12,0,-48,0,16,0,120,0,-160,0,32,-120,0,720,0,-480,0,64,0,-1680,0,3360,0,-1344,0,128,1680,0,-13440,0,13440,0,-3584,0,256,0,30240,0,-80640,0,48384,0,-9216,0,512,-30240,0,302400,0,-403200,0,161280,0,-23040,0,1024",
			"name": "Triangle T(n,k) read by rows giving coefficients of Hermite polynomial of order n (n \u003e= 0, 0 \u003c= k \u003c= n).",
			"comment": [
				"Exponential Riordan array [exp(-x^2), 2x]. - _Paul Barry_, Jan 22 2009"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A060821/b060821.txt\"\u003eRows n=0..100 of triangle, flattened\u003c/a\u003e",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972, p. 801.",
				"Taekyun Kim and Dae San Kim, \u003ca href=\"http://arxiv.org/abs/1602.04096\"\u003eA note on Hermite polynomials\u003c/a\u003e, arXiv:1602.04096 [math.NT], 2016.",
				"Alexander Minakov, \u003ca href=\"https://arxiv.org/abs/1911.03942\"\u003eQuestion about integral of product of four Hermite polynomials integrated with squared weight\u003c/a\u003e, arXiv:1911.03942 [math.CO], 2019.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Hermite_polynomials\"\u003eHermite polynomials\u003c/a\u003e",
				"\u003ca href=\"/index/He#Hermite\"\u003eIndex entries for sequences related to Hermite polynomials\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = ((-1)^((n-k)/2))*(2^k)*n!/(k!*((n-k)/2)!) if n-k is even and \u003e= 0, else 0.",
				"E.g.f.: exp(-y^2 + 2*y*x).",
				"From _Paul Barry_, Aug 28 2005: (Start)",
				"T(n, k) = n!/(k!*2^((n-k)/2)((n-k)/2)!)2^((n+k)/2)cos(Pi*(n-k)/2)(1 + (-1)^(n+k))/2;",
				"T(n, k) = A001498((n+k)/2, (n-k)/2)*cos(Pi*(n-k)/2)2^((n+k)/2)(1 + (-1)^(n+k))/2.",
				"(End)",
				"Row sums: A062267. - _Derek Orr_, Mar 12 2015",
				"a(n*(n+3)/2) = a(A000096(n)) = 2^n. - _Derek Orr_, Mar 12 2015",
				"Recurrence for fixed n: T(n, k) = -(k+2)*(k+1)/(2*(n-k)) * T(n, k+2), starting with T(n, n) = 2^n. - _Ralf Stephan_, Mar 26 2016",
				"The m-th row consecutive nonzero entries in increasing order are (-1)^(c/2)*(c+b)!/(c/2)!b!*2^b with c = m, m-2, ..., 0 and b = m-c if m is even and with c = m-1, m-3, ..., 0 with b = m-c if m is odd. For the 10th row starting at a(55) the 6 consecutive nonzero entries in order are -30240,302400,-403200,161280,-23040,1024 given by c = 10,8,6,4,2,0 and b = 0,2,4,6,8,10. - _Richard Turk_, Aug 20 2017"
			],
			"example": [
				"[1], [0, 2], [ -2, 0, 4], [0, -12, 0, 8], [12, 0, -48, 0, 16], [0, 120, 0, -160, 0, 32], ... .",
				"Thus H_0(x) = 1, H_1(x) = 2*x, H_2(x) = -2 + 4*x^2, H_3(x) = -12*x + 8*x^3, H_4(x) = 12 - 48*x^2 + 16*x^4, ...",
				"Triangle starts:",
				"     1;",
				"     0,     2;",
				"    -2,     0,      4;",
				"     0,   -12,      0,      8;",
				"    12,     0,    -48,      0,      16;",
				"     0,   120,      0,   -160,       0,    32;",
				"  -120,     0,    720,      0,    -480,     0,     64;",
				"     0, -1680,      0,   3360,       0, -1344,      0,   128;",
				"  1680,     0, -13440,      0,   13440,     0,  -3584,     0,    256;",
				"     0, 30240,      0, -80640,       0, 48384,      0, -9216,      0, 512;",
				"-30240,     0, 302400,      0, -403200,     0, 161280,     0, -23040,   0, 1024;"
			],
			"maple": [
				"with(orthopoly):for n from 0 to 10 do H(n,x):od;",
				"T := proc(n,m) if n-m \u003e= 0 and n-m mod 2 = 0 then ((-1)^((n-m)/2))*(2^m)*n!/(m!*((n-m)/2)!) else 0 fi; end;"
			],
			"mathematica": [
				"Flatten[ Table[ CoefficientList[ HermiteH[n, x], x], {n, 0, 10}]] (* _Jean-François Alcover_, Jan 18 2012 *)"
			],
			"program": [
				"(PARI) for(n=0,9,v=Vec(polhermite(n));forstep(i=n+1,1,-1,print1(v[i]\", \"))) \\\\ _Charles R Greathouse IV_, Jun 20 2012",
				"(Python)",
				"from sympy import hermite, Poly, symbols",
				"x = symbols('x')",
				"def a(n): return Poly(hermite(n, x), x).all_coeffs()[::-1]",
				"for n in range(21): print(a(n)) # _Indranil Ghosh_, May 26 2017"
			],
			"xref": [
				"Cf. A001814, A001816, A000321, A062267 (row sums).",
				"Without initial zeros, same as A059343."
			],
			"keyword": "sign,tabl,nice",
			"offset": "0,3",
			"author": "_Vladeta Jovovic_, Apr 30 2001",
			"references": 30,
			"revision": 67,
			"time": "2021-03-10T17:29:01-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}