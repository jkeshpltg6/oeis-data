{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A004525",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 4525,
			"data": "0,1,1,1,2,3,3,3,4,5,5,5,6,7,7,7,8,9,9,9,10,11,11,11,12,13,13,13,14,15,15,15,16,17,17,17,18,19,19,19,20,21,21,21,22,23,23,23,24,25,25,25,26,27,27,27,28,29,29,29,30,31,31,31,32,33,33,33,34,35,35,35,36,37,37,37",
			"name": "One even followed by three odd.",
			"comment": [
				"a(n+1) is the composition length of the n-th symmetric power of the natural representation of a finite subgroup of SL(2,C) of type E_6 (binary tetrahedral group). - _Paul Boddington_, Oct 23 2003",
				"(1 + x + x^2 + x^3 + x^4 + x^5) / ( (1-x^3)*(1- x^4)) is the Poincaré series [or Poincare series] (or Molien series) for H^*(GL_2(F_3)). - _N. J. A. Sloane_, Jun 12 2004",
				"The Fi1 and Fi2 sums, see A180662 for the definition of these sums, of triangle A101950 equal the terms of this sequence without the first term. - _Johannes W. Meijer_, Aug 06 2011",
				"Also the domination number of the n X n black bishop graph. - _Eric W. Weisstein_, Jun 26 2017",
				"Also the domination number of the (n-1)-Moebius laddder. - _Eric W. Weisstein_, Jun 30 2017",
				"Also the rook domination number of the hexagonal hexagon board B_n [Harborth and Nienborg] - _N. J. A. Sloane_, Aug 31 2021"
			],
			"reference": [
				"A. Adem and R. J. Milgram, Cohomology of Finite Groups, Springer-Verlag, 2nd. ed., 2004; p. 247.",
				"Y. Ito, I. Nakamura, Hilbert schemes and simple singularities, New trends in algebraic geometry (Warwick, 1996), 151-233, Cambridge University Press, 1999."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A004525/b004525.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Heiko Harborth and Hauke Nienborg, \u003ca href=\"http://math.colgate.edu/~integers/graham14/graham14.Abstract.html\"\u003eRook domination on hexagonal hexagon boards\u003c/a\u003e, INTEGERS 21A (2021), #A14.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/BlackBishopGraph.html\"\u003eBlack Bishop Graph\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/DominationNumber.html\"\u003eDomination Number\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/MoebiusLadder.html\"\u003eMoebius Ladder\u003c/a\u003e",
				"\u003ca href=\"/index/Tu#2wis\"\u003eIndex entries for two-way infinite sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,-2,2,-1)."
			],
			"formula": [
				"a(n) = a(n-1) - a(n-2) + a(n-3) + 1 = n - A004524(n+1). - _Henry Bottomley_, Mar 08 2000",
				"G.f.: x*(1-x+x^2)/((1-x)^2*(1+x^2)) = x*(1-x^6)/((1-x)*(1-x^3)*(1-x^4)). - _Michael Somos_, Jul 19 2003",
				"a(n) = -a(-n) for all n in Z. - _Michael Somos_, Jul 19 2003",
				"a(n) = floor(n/4) + ceiling(n/4). See also A004396, one even followed by two odd and A002620, quarter-squares: floor(n/2)*ceiling(n/2). - _Jonathan Vos Post_, Mar 19 2006",
				"a(n) = Sum_{k=0..n-1} (1 + (-1)^binomial(k+1, 2))/2. - _Paul Barry_, Mar 31 2008",
				"E.g.f: A(x) = (x*exp(x) + sin(x))/2. - _Vladimir Kruchinin_, Feb 20 2011",
				"a(n) = (1/4)*(2*n - (1 - (-1)^n)*(-1)^(n*(n+1)/2)). - _Bruno Berselli_, Mar 13 2012",
				"a(n) = (n - floor(cos(Pi*(n+1)/2)))/2. - _Wesley Ivan Hurt_, Oct 22 2013",
				"Euler transform of length 6 sequence [1, 0, 1, 1, 0, -1]. - _Michael Somos_, Apr 03 2017",
				"a(n) = (n + sin(n*Pi/2))/2. - _Wesley Ivan Hurt_, Oct 02 2017",
				"a(n) = n-1-a(n-2) for n \u003e= 2. - _Kritsada Moomuang_, 29 Oct 2019"
			],
			"example": [
				"G.f. = x + x^2 + x^3 + 2*x^4 + 3*x^5 + 3*x^6 + 3*x^7 + 4*x^8 + 5*x^9 + ..."
			],
			"maple": [
				"A004525 := proc(n): floor(n/4) + ceil(n/4) end: seq(A004525(n), n=0..75); # _Johannes W. Meijer_, Aug 06 2011"
			],
			"mathematica": [
				"Table[Floor[n/4] + Ceiling[n/4], {n, 0, 100}] (* _Wesley Ivan Hurt_, Oct 22 2013 *)",
				"Table[(n + Sin[n Pi/2])/2, {n, 0, 30}] (* _Eric W. Weisstein_, Jun 30 2017 *)",
				"LinearRecurrence[{2, -2, 2, -1}, {1, 1, 1, 2}, {0, 20}] (* _Eric W. Weisstein_, Jun 30 2017 *)",
				"Table[{n-1,n,n,n},{n,1,41,2}]//Flatten (* _Harvey P. Dale_, Oct 18 2019 *)"
			],
			"program": [
				"(PARI) {a(n) = n\\4 + (n+3)\\4}; /* _Michael Somos_, Jul 19 2003 */",
				"(MAGMA) [Floor(n/4) + Ceiling(n/4): n in [0..70]]; // _Vincenzo Librandi_, Aug 07 2011",
				"(Maxima) makelist((1/4)*(2*n-(1-(-1)^n)*(-1)^(n*(n+1)/2)), n, 0, 75); /* _Bruno Berselli_, Mar 13 2012 */",
				"(Haskell)",
				"a004525 n = a004525_list !! n",
				"a004525_list = 0 : 1 : 1 : zipWith3 (\\x y z -\u003e x - y + z + 1)",
				"               a004525_list (tail a004525_list) (drop 2 a004525_list)",
				"-- _Reinhard Zumkeller_, Jul 14 2012"
			],
			"xref": [
				"Cf. A002620, A004396, A004524."
			],
			"keyword": "nonn,easy",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_",
			"references": 24,
			"revision": 88,
			"time": "2021-10-30T10:44:58-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}