{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A280499",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 280499,
			"data": "1,2,1,3,0,1,4,2,0,1,5,0,3,0,1,6,3,2,0,0,1,7,0,0,0,0,0,1,8,4,0,2,0,0,0,1,9,0,7,0,0,0,3,0,1,10,5,6,0,2,3,0,0,0,1,11,0,0,0,0,0,0,0,0,0,1,12,6,4,3,0,2,0,0,0,0,0,1,13,0,0,0,0,0,0,0,0,0,0,0,1,14,7,0,0,0,0,2,0,0,0,0,0,0,1,15,0,5,0,3,0,0,0,0,0,0,0,0,0,1",
			"name": "Triangular table for division in ring GF(2)[X]: T(n,k) = n/k, or 0 if k is not a divisor of n, where the binary expansion of each number defines the corresponding (0,1)-polynomial.",
			"comment": [
				"This is GF(2)[X] analog of A126988, using \"carryless division in base-2\" instead of ordinary division.",
				"The triangular table T(n,k), n=1.., k=1..n is read by rows: T(1,1), T(2,1), T(2,2), T(3,1), T(3,2), T(3,3), etc."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A280499/b280499.txt\"\u003eTable of n, a(n) for n = 1..32896; the first 256 rows of triangle\u003c/a\u003e",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e",
				"\u003ca href=\"/index/Ge#GF2X\"\u003eIndex entries for sequences operating on polynomials in ring GF(2)[X]\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = the unique d such that A048720(d,k) = n, provided that such d exists, otherwise zero."
			],
			"example": [
				"The first 17 rows of the triangle:",
				"   1",
				"   2 1",
				"   3 0 1",
				"   4 2 0 1",
				"   5 0 3 0 1",
				"   6 3 2 0 0 1",
				"   7 0 0 0 0 0 1",
				"   8 4 0 2 0 0 0 1",
				"   9 0 7 0 0 0 3 0 1",
				"  10 5 6 0 2 3 0 0 0 1",
				"  11 0 0 0 0 0 0 0 0 0 1",
				"  12 6 4 3 0 2 0 0 0 0 0 1",
				"  13 0 0 0 0 0 0 0 0 0 0 0 1",
				"  14 7 0 0 0 0 2 0 0 0 0 0 0 1",
				"  15 0 5 0 3 0 0 0 0 0 0 0 0 0 1",
				"  16 8 0 4 0 0 0 2 0 0 0 0 0 0 0 1",
				"  17 0 15 0 5 0 0 0 0 0 0 0 0 0 3 0 1",
				"  -----------------------------------",
				"7 (\"111\" in binary) encodes polynomial X^2 + X + 1, which is irreducible over GF(2) (7 is in A014580), so it is divisible only by itself and 1, and thus T(7,1) = 7, T(7,k) = 0 for k=2..6 and T(7,7) = 1.",
				"9 (\"1001\" in binary) encodes polynomial X^3 + 1, which is factored over GF(2) as (X+1)(X^2 + X + 1), and thus T(9,3) = 7 and T(9,7) = 3 because the polynomial X + 1 is encoded by 3 (\"11\" in binary)."
			],
			"program": [
				"(Scheme) (define (A280499 n) (A280500bi (A002024 n) (A002260 n))) ;; Code for A280500bi given in A280500."
			],
			"xref": [
				"Lower triangular region of square array A280500.",
				"Transpose: A280494.",
				"Cf. A014580, A048720, A126988, A178908, A280500, A280493 (the row sums)."
			],
			"keyword": "nonn,base,tabl,look",
			"offset": "1,2",
			"author": "_Antti Karttunen_, Jan 09 2017",
			"references": 5,
			"revision": 28,
			"time": "2017-01-11T09:06:54-05:00",
			"created": "2017-01-10T19:13:40-05:00"
		}
	]
}