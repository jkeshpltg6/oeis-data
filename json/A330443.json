{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A330443",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 330443,
			"data": "2,0,1,-1,1,2,1,2,5,7,1,2,1,3,1,7,2,2,10,2,1,10,1,10,1,2,1,7,5,3,13,2,1,19,1,3,1,2,5,7,2,2,1,10,2,7,1,2,10,2,1,7,1,3,1,2,2,7,5,2,1,3,2,10,1,2,1,2,5,10,1,10,10,15,1,22,1,2,10",
			"name": "Least m \u003e= 0 such that (n+m)(n+m+1)/2 - n(n-3)/2 is prime, or -1 if no such m exists.",
			"comment": [
				"a(n) + 1 is the number of steps to reach a prime in the game described by Peter Luschny on the SeqFan list (cf. link): Start with n, then add n, n+1, n+2, ..., n+m until a prime is reached.",
				"See A330501 for the resulting prime, A329946 for the primes never reached.",
				"Among the first 200 terms a(0..199), there are 50 '1's, 49 '2's, 19 '3's and 19 '10's, and 17 '7's. Is there an explanation for the frequency of, e.g., 10?"
			],
			"link": [
				"Peter Luschny, \u003ca href=\"http://list.seqfan.eu/pipermail/seqfan/2019-December/\"\u003eHopping for primes\u003c/a\u003e, SeqFan list, Dec 13 2019."
			],
			"formula": [
				"a(n) = A330502(n) - n."
			],
			"example": [
				"Starting with n = 0, add 0: sum = 0, not prime, then add 1: sum = 1, not prime, then add 2: sum = 3, a prime, so a(0) = 2.",
				"Starting with n = 1, add 1: sum = 2, a prime, so a(1) = 2 - 2 = 0.",
				"Starting with n = 2, add 2: sum = 4, not prime, then add 3: sum = 7, a prime, so a(2) = 3 - 2 = 1.",
				"Starting with n = 3 = T(2) = 2(2+1)/2 (triangular number, cf. A000217), add 3 to get T(2) + 3 = T(3) = 6, then add 4 to get T(3) + 4 = T(4) = 10, and so on. A triangular number T(n) = n(n+1)/2 \u003e 3 is never prime, since either product of n and (n+1)/2, or product of n/2 and n+1. So a(3) = -1."
			],
			"mathematica": [
				"Array[If[# == 3, 0, Block[{m = #}, While[! PrimeQ[m (m + 1)/2 - # (# - 3)/2], m++]; m-#]] \u0026, 72, 0] (* following code from _Michael De Vlieger_ in A330502 *)"
			],
			"program": [
				"(PARI) apply( {A330443(n)=max(A330502(n)-n,-1)}, [0..199])"
			],
			"xref": [
				"Cf. A000217 (triangular numbers n(n+1)/2), A000096 (n(n+3)/2), A330501 (the final prime reached), A330502 (a(n)+n), A329946 (primes never reached)."
			],
			"keyword": "sign",
			"offset": "0,1",
			"author": "_M. F. Hasler_, following an idea of _Peter Luschny_, Dec 16 2019",
			"references": 1,
			"revision": 36,
			"time": "2019-12-27T16:31:41-05:00",
			"created": "2019-12-27T16:31:41-05:00"
		}
	]
}