{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A014945",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 14945,
			"data": "1,3,9,21,27,63,81,147,171,189,243,441,513,567,657,729,903,1029,1197,1323,1539,1701,1971,2187,2667,2709,3087,3249,3591,3969,4599,4617,5103,5913,6321,6561,7077,7203,8001,8127,8379,9261,9747,10773,11907,12483",
			"name": "Numbers k such that k divides 4^k - 1.",
			"comment": [
				"This sequence is closed under multiplication. - _Charles R Greathouse IV_, Nov 03 2016",
				"Conjecture: if k divides 4^k - 1, then (4^k - 1)/k is squarefree. - _Thomas Ordowski_, Dec 24 2018",
				"Following Greathouse's comment, see A323203 for the primitive terms. - _Bernard Schott_, Jan 03 2019",
				"All terms except 1 are divisible by 3.  Proof: suppose n\u003e1 is in the sequence, and let p be its smallest prime factor.  Of course p is odd.  Since 4^n-1 is divisible by p, n is divisible by the multiplicative order of 4 mod p, which is less than p.  But since n has no prime factors \u003c p, that multiplicative order can only be 1, which means p=3. - _Robert Israel_, Jan 24 2019"
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A014945/b014945.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (terms 1..872 from Muniru A Asiru, terms 873..2000 from Alois P. Heinz)"
			],
			"formula": [
				"a(n) = A014741(n+1)/2."
			],
			"maple": [
				"select(n-\u003emodp(4^n-1,n)=0,[$1..13000]); # _Muniru A Asiru_, Dec 28 2018"
			],
			"mathematica": [
				"Select[Range[12500],Divisible[4^#-1,#]\u0026]  (* _Harvey P. Dale_, Mar 23 2011 *)"
			],
			"program": [
				"(PARI) is(n)=Mod(4,n)^n==1 \\\\ _Charles R Greathouse IV_, Nov 03 2016",
				"(GAP) a:=Filtered([1..13000],n-\u003e(4^n-1) mod n=0);; Print(a); # _Muniru A Asiru_, Dec 28 2018",
				"(MAGMA) [n: n in [1..12500] | (4^n-1) mod n eq 0 ]; // _Vincenzo Librandi_, Dec 29 2018",
				"(Python)",
				"for n in range(1,1000):",
				"    if (4**n-1) % n ==0:",
				"        print(n, end=', ') # _Stefano Spezia_, Jan 05 2019"
			],
			"xref": [
				"Cf. A014741, A323203."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Olivier Gérard_",
			"ext": [
				"More terms and better description from _Benoit Cloitre_, Mar 05 2002"
			],
			"references": 26,
			"revision": 50,
			"time": "2019-09-11T08:45:11-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}