{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A009964",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 9964,
			"data": "1,20,400,8000,160000,3200000,64000000,1280000000,25600000000,512000000000,10240000000000,204800000000000,4096000000000000,81920000000000000,1638400000000000000,32768000000000000000",
			"name": "Powers of 20.",
			"comment": [
				"The compositions of n in which each natural number is colored by one of p different colors are called p-colored compositions of n. For n \u003e= 1, a(n) equals the number of 20-colored compositions of n such that no adjacent parts have the same color. - _Milan Janjic_, Nov 17 2011",
				"a(n) gives the number of small cubes in the n-th iteration of the Menger sponge fractal. - _Felix Fröhlich_, Jul 09 2016",
				"Equivalently, the number of vertices in the n-Menger sponge graph."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A009964/b009964.txt\"\u003eTable of n, a(n) for n = 0..100\u003c/a\u003e",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/MengerSponge.html\"\u003eMenger Sponge\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/MengerSpongeGraph.html\"\u003eMenger Sponge Graph\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/VertexCount.html\"\u003eVertex Count\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Menger_sponge\"\u003eMenger sponge\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_01\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (20)."
			],
			"formula": [
				"G.f.: 1/(1-20*x).",
				"E.g.f.: exp(20*x).",
				"a(n) = A159991(n)/A000244(n). - _Reinhard Zumkeller_, May 02 2009",
				"From _Vincenzo Librandi_, Nov 21 2010: (Start)",
				"a(n) = 20^n.",
				"a(n) = 20*a(n-1) for n \u003e 0, a(0) = 1. (End)",
				"a(n) = A000079(n)*A011557(n) = A000302(n)*A000351(n). - _Felix Fröhlich_, Jul 09 2016"
			],
			"maple": [
				"[20^n$n=0..20]; # _Muniru A Asiru_, Nov 21 2018"
			],
			"mathematica": [
				"20^Range[0, 10] (* or *) LinearRecurrence[{20}, {1}, 20] (* _Eric W. Weisstein_, Aug 17 2017 *)"
			],
			"program": [
				"(Sage) [20^n for n in range(21)] # _Zerinvary Lajos_, Apr 29 2009",
				"(MAGMA) [20^n: n in [0..100]] // _Vincenzo Librandi_, Nov 21 2010",
				"(Maxima) makelist(20^n,n,0,30); /* _Martin Ettl_, Nov 05 2012 */",
				"(PARI) a(n)=20^n \\\\ _Charles R Greathouse IV_, Jun 19 2015",
				"(PARI) powers(20,12) \\\\ _Charles R Greathouse IV_, Jun 19 2015",
				"(GAP) List([0..20],n-\u003e20^n); # _Muniru A Asiru_, Nov 21 2018",
				"(Python) [20**n for n in range(21)] # _Stefano Spezia_, Nov 21 2018"
			],
			"xref": [
				"Cf. A291066 (edge count).",
				"Cf. A000079, A011557; A000302, A000351; A000244, A159991."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 26,
			"revision": 69,
			"time": "2020-04-11T06:07:38-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}