{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A215366",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 215366,
			"data": "1,2,3,4,5,6,8,7,9,10,12,16,11,14,15,18,20,24,32,13,21,22,25,27,28,30,36,40,48,64,17,26,33,35,42,44,45,50,54,56,60,72,80,96,128,19,34,39,49,52,55,63,66,70,75,81,84,88,90,100,108,112,120,144,160,192,256",
			"name": "Triangle T(n,k) read by rows in which n-th row lists in increasing order all partitions lambda of n encoded as Product_{i in lambda} prime(i); n\u003e=0, 1\u003c=k\u003c=A000041(n).",
			"comment": [
				"The concatenation of all rows (with offset 1) gives a permutation of the natural numbers A000027 with fixed points 1-6, 9, 10, 14, 15, 21, 22, 33, 49, 1095199, ... and inverse permutation A215501.",
				"Number m is positioned in row n = A056239(m).  The number of different values m, such that both m and m+1 occur in row n is A088850(n).  A215369 lists all values m, such that both m and m+1 are in the same row.",
				"The power prime(i)^j of the i-th prime is in row i*j for j in {0,1,2, ... }.",
				"Column k=2 contains the even semiprimes A100484, where 10 and 22 are replaced by the odd semiprimes 9 and 21, respectively.",
				"This triangle is related to the triangle A145518, see in both triangles the first column, the right border, the second right border and the row sums. - _Omar E. Pol_, May 18 2015"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A215366/b215366.txt\"\u003eRows n = 0..26, flattened\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e",
				"\u003ca href=\"/index/Pri#prime_indices\"\u003eIndex entries for sequences computed from indices in prime factorization\u003c/a\u003e"
			],
			"formula": [
				"Recurrence relation, explained for the set S(4) of entries in row 4: multiply the entries of S(3) by 2 (= 1st prime), multiply the entries of S(2) by 3 (= 2nd prime), multiply the entries of S(1) by 5 (= 3rd prime), multiply the entries of S(0) by 7 (= 4th prime); take the union of all the obtained products. The 3rd Maple program is based on this recurrence relation. - _Emeric Deutsch_, Jan 23 2016"
			],
			"example": [
				"The partitions of n=3 are {[3], [2,1], [1,1,1]}, encodings give {prime(3), prime(2)*prime(1), prime(1)^3} = {5, 3*2, 2^3} =\u003e row 3 = [5, 6, 8].",
				"For n=0 the empty partition [] gives the empty product 1.",
				"Triangle T(n,k) begins:",
				"   1;",
				"   2;",
				"   3,  4;",
				"   5,  6,  8;",
				"   7,  9, 10, 12, 16;",
				"  11, 14, 15, 18, 20, 24, 32;",
				"  13, 21, 22, 25, 27, 28, 30, 36, 40, 48, 64;",
				"  17, 26, 33, 35, 42, 44, 45, 50, 54, 56, 60, 72, 80, 96, 128;",
				"  ...",
				"Corresponding triangle of integer partitions begins:",
				"  ();",
				"  1;",
				"  2, 11;",
				"  3, 21, 111;",
				"  4, 22, 31, 211, 1111;",
				"  5, 41, 32, 221, 311, 2111, 11111;",
				"  6, 42, 51, 33, 222, 411, 321, 2211, 3111, 21111, 111111;",
				"  7, 61, 52, 43, 421, 511, 322, 331, 2221, 4111, 3211, 22111, 31111, 211111, 1111111;  - _Gus Wiseman_, Dec 12 2016"
			],
			"maple": [
				"b:= proc(n, i) option remember; `if`(n=0 or i\u003c2, [2^n],",
				"       [seq(map(p-\u003ep*ithprime(i)^j, b(n-i*j, i-1))[], j=0..n/i)])",
				"    end:",
				"T:= n-\u003e sort(b(n, n))[]:",
				"seq(T(n), n=0..10);",
				"# (2nd Maple program)",
				"with(combinat): A := proc (n) local P, A, i: P := partition(n): A := {}; for i to nops(P) do A := `union`(A, {mul(ithprime(P[i][j]), j = 1 .. nops(P[i]))}) end do: A end proc; # the command A(m) yields row m. # _Emeric Deutsch_, Jan 23 2016",
				"# (3rd Maple program)",
				"q:= 7: S[0] := {1}: for m to q do S[m] := `union`(seq(map(proc (f) options operator, arrow: ithprime(j)*f end proc, S[m-j]), j = 1 .. m)) end do; # for a given positive integer q, the program yields rows 0, 1, 2,...,q. # _Emeric Deutsch_, Jan 23 2016"
			],
			"mathematica": [
				"b[n_, i_] := b[n, i] = If[n == 0 || i\u003c2, {2^n}, Table[Function[#*Prime[i]^j] /@ b[n - i*j, i-1], {j, 0, n/i}] // Flatten]; T[n_] := Sort[b[n, n]]; Table[T[n], {n, 0, 10}] // Flatten (* _Jean-François Alcover_, Mar 12 2015, after _Alois P. Heinz_ *)",
				"nn=7;HeinzPartition[n_]:=If[n===1,{},Flatten[Cases[FactorInteger[n],{p_,k_}:\u003eTable[PrimePi[p],{k}]]]//Reverse];",
				"Take[GatherBy[Range[2^nn],Composition[Total,HeinzPartition]],nn+1] (* _Gus Wiseman_, Dec 12 2016 *)",
				"Table[Map[Times @@ Prime@ # \u0026, IntegerPartitions[n]], {n, 0, 8}] // Flatten (* _Michael De Vlieger_, Jul 12 2017 *)"
			],
			"program": [
				"(PARI) \\\\ From _M. F. Hasler_, Dec 06 2016 (Start)",
				"A215366_row(n)=apply(P-\u003eprod(i=1,#P,prime(P[i])),partitions(n))",
				"A215366_vec(N)=concat(apply(A215366_row,[0..N])) \\\\ \"flattened\" rows 0..N (End)"
			],
			"xref": [
				"Column k=1 gives: A008578(n+1).",
				"Last elements of rows give: A000079.",
				"Second to last elements of rows give: A007283(n-2) for n\u003e1.",
				"Row sums give: A145519.",
				"Row lengths are: A000041.",
				"Cf. A129129 (with row elements using order of A080577).",
				"LCM of terms in row n gives A138534(n).",
				"Cf. A000027, A000040, A056239, A063008, A088850, A100484, A215501.",
				"Cf. A112798, A246867 (the same for partitions into distinct parts).",
				"Cf. A324939."
			],
			"keyword": "nonn,look,tabf",
			"offset": "0,2",
			"author": "_Alois P. Heinz_, Aug 08 2012",
			"references": 154,
			"revision": 100,
			"time": "2020-02-14T17:55:49-05:00",
			"created": "2012-08-09T09:01:46-04:00"
		}
	]
}