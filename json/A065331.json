{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A065331",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 65331,
			"data": "1,2,3,4,1,6,1,8,9,2,1,12,1,2,3,16,1,18,1,4,3,2,1,24,1,2,27,4,1,6,1,32,3,2,1,36,1,2,3,8,1,6,1,4,9,2,1,48,1,2,3,4,1,54,1,8,3,2,1,12,1,2,9,64,1,6,1,4,3,2,1,72,1,2,3,4,1,6,1,16,81,2,1,12,1,2,3,8,1,18,1,4,3,2,1,96",
			"name": "Largest 3-smooth divisor of n.",
			"comment": [
				"Bennett, Filaseta, \u0026 Trifonov show that if n \u003e 8 then a(n^2 + n) \u003c n^0.715. - _Charles R Greathouse IV_, May 21 2014"
			],
			"link": [
				"Charles R Greathouse IV, \u003ca href=\"/A065331/b065331.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"M. A. Bennett, M. Filaseta, and O. Trifonov, \u003ca href=\"http://www.math.ubc.ca/~bennett/BFTpaper0207.pdf\"\u003eOn the factorization of consecutive integers\u003c/a\u003e, J. Reine Angew. Math. 629 (2009), pp. 171-200."
			],
			"formula": [
				"a(n) = n / A065330(n).",
				"a(n) = A006519(n) * A038500(n).",
				"a(n) = (2^A007814 (n)) * (3^A007949(n)).",
				"Multiplicative with a(2^e)=2^e, a(3^e)=3^e, a(p^e)=1, p\u003e3. - _Vladeta Jovovic_, Nov 05 2001",
				"Dirichlet g.f.: zeta(s)*(1-2^(-s))*(1-3^(-s))/ ( (1-2^(1-s))*(1-3^(1-s)) ). - _R. J. Mathar_, Jul 04 2011",
				"a(n) = gcd(n,6^n). - _Stanislav Sykora_, Feb 08 2016",
				"a(A225546(n)) = A225546(A053165(n)). - _Peter Munn_, Jan 17 2020",
				"Sum_{k=1..n} a(k) ~ n*(log(n)^2 + (2*gamma + 3*log(2) + 2*log(3) - 2)*log(n) + (2 + log(2)^2/6 + 3*log(2)*(log(3) - 1) - 2*log(3) + log(3)^2/6 + gamma*(3*log(2) + 2*log(3) - 2) - 2*sg1)) / (6*log(2)*log(3)), where gamma is the Euler-Mascheroni constant A001620 and sg1 is the first Stieltjes constant (see A082633). - _Vaclav Kotesovec_, Sep 19 2020"
			],
			"maple": [
				"A065331 := proc(n) n/A065330(n) ; end: # _R. J. Mathar_, Jun 24 2009",
				"seq(2^padic:-ordp(n,2)*3^padic:-ordp(n,3), n=1..100); # _Robert Israel_, Feb 08 2016"
			],
			"mathematica": [
				"Table[GCD[n, 6^n], {n, 100}] (* _Vincenzo Librandi_, Feb 09 2016 *)",
				"a[n_] := Times @@ ({2, 3}^IntegerExponent[n, {2, 3}]); Array[a, 100] (* _Amiram Eldar_, Sep 19 2020 *)"
			],
			"program": [
				"(PARI) a(n)=3^valuation(n,3)\u003c\u003cvaluation(n,2) \\\\ _Charles R Greathouse IV_, Aug 21 2011",
				"(PARI) a(n)=gcd(n,6^n) \\\\ Not very efficient, but simple. _Stanislav Sykora_, Feb 08 2016",
				"(Haskell)",
				"a065331 = f 2 1 where",
				"   f p y x | r == 0    = f p (y * p) x'",
				"           | otherwise = if p == 2 then f 3 y x else y",
				"           where (x', r) = divMod x p",
				"-- _Reinhard Zumkeller_, Nov 19 2015",
				"(MAGMA) [Gcd(n,6^n): n in [1..100]]; // _Vincenzo Librandi_, Feb 09 2016"
			],
			"xref": [
				"Cf. A065330, A006519, A038500, A007814, A007949, A007310, A047229.",
				"Related to A053165 via A225546."
			],
			"keyword": "mult,nonn,easy",
			"offset": "1,2",
			"author": "_Reinhard Zumkeller_, Oct 29 2001",
			"references": 21,
			"revision": 45,
			"time": "2020-09-19T10:32:05-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}