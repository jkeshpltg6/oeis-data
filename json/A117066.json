{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A117066",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 117066,
			"data": "1,11,48,140,325,651,1176,1968,3105,4675,6776,9516,13013,17395,22800,29376,37281,46683,57760,70700,85701,102971,122728,145200,170625,199251,231336,267148,306965,351075,399776,453376,512193,576555,646800,723276,806341",
			"name": "Partial sums of cupolar numbers (1/3)*(n+1)*(5*n^2+7*n+3) (A096000).",
			"comment": [
				"Consider the partitions of 2n into two parts (p,q) where p \u003c= q. Then a(n) is the total volume of the family of rectangular prisms with dimensions p, p and q. - _Wesley Ivan Hurt_, Apr 15 2018"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A117066/b117066.txt\"\u003eTable of n, a(n) for n = 1..5000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (5,-10,10,-5,1)."
			],
			"formula": [
				"a(n) = Sum_{i=1..n} A096000(i).",
				"a(n) = Sum_{i=1..n} (1/3)*(i+1)*(5*i^2+7*i+3).",
				"a(n) = Sum_{i=1..n} (1/2)*(Q(i) + 3*i^2 + 3*i + 1), where Q(i) are the cuboctahedral numbers (A005902).",
				"a(n) = Sum_{i=0..n} A073254(n,i)*i. - _Peter Luschny_, Oct 29 2011",
				"G.f.: x*(1+6*x+3*x^2) / (1-x)^5. - _Colin Barker_, May 08 2013",
				"9*a(n) = Sum_{i=0..n} (n+i)^3, see Maple code by _Zerinvary Lajos_. - _Bruno Berselli_, Apr 01 2014",
				"a(n) = n^2*(n+1)*(5*n+1)/12. - _Vaclav Kotesovec_, Jan 03 2017",
				"E.g.f.: (x/12)*(12 + 54*x + 36*x^2 + 5*x^3)*exp(x). - _G. C. Greubel_, Jul 19 2017"
			],
			"example": [
				"For n=6, 9*a(6) = 6^3 + 7^3 + 8^3 + 9^3 + 10^3 + 11^3 +12^3 = 9*651. - _Bruno Berselli_, Apr 01 2014"
			],
			"maple": [
				"a:=n-\u003esum ((n+j)^3, j=0..n): seq(a(n)/9, n=1..40);# _Zerinvary Lajos_, Dec 17 2008"
			],
			"mathematica": [
				"Table[Sum[n i (n + i), {i, 0, n}]/2, {n, 40}] (* _Vladimir Joseph Stephan Orlovsky_, Jun 03 2011 *)"
			],
			"program": [
				"(PARI) my(x='x+O('x^40)); Vec(x*(3*x^2+6*x+1)/(1-x)^5) \\\\ _G. C. Greubel_, Jul 19 2017",
				"(PARI) a(n) = n^2*(n+1)*(5*n+1)/12; \\\\ _Altug Alkan_, Apr 16 2018",
				"(MAGMA) [n^2*(n+1)*(5*n+1)/12: n in [1..40]]; // _Vincenzo Librandi_, Apr 16 2018",
				"(Sage) [n^2*(n+1)*(5*n+1)/12 for n in (1..40)] # _G. C. Greubel_, Jul 05 2019",
				"(GAP) List([1..40], n-\u003e n^2*(n+1)*(5*n+1)/12) # _G. C. Greubel_, Jul 05 2019"
			],
			"xref": [
				"Cf. A005902, A096000."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Jonathan Vos Post_, Apr 17 2006",
			"ext": [
				"Terms corrected by _Colin Barker_, May 08 2013"
			],
			"references": 5,
			"revision": 42,
			"time": "2019-07-05T21:02:27-04:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}