{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A078538",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 78538,
			"data": "12,22,12,249,12,22,12,19689,12,22,12,249,12,22,12",
			"name": "Smallest k \u003e 6 such that sigma_n(k)/phi(k) is an integer.",
			"comment": [
				"For n = 16, 48, 64, and 80 the solutions are hard to find, exceed 10^6 or even 10^7.",
				"If a(16) exists, it is greater than 2^32. Terms a(17) to a(47) are 12, 22, 12, 249, 12, 22, 12, 9897, 12, 22, 12, 249, 12, 22, 12, 2566, 12, 22, 12, 249, 12, 22, 12, 19689, 12, 22, 12, 249, 12, 22, 12. - _T. D. Noe_, Dec 08 2013"
			],
			"example": [
				"These terms appear as 5th entries in A020492, A015759-A015774. k = {1, 2, 3, 6} are solutions to Min{k : Mod[sigma[n, k], phi[k]]=0}. First nontrivial solutions are larger: for odd n, k = 12 is solution; for even powers larger numbers arise like 22, 249, 9897, 19689, etc. Certain power-sums of divisors proved to be hard to find."
			],
			"mathematica": [
				"f[k_, x_] := DivisorSigma[k, x]/EulerPhi[x]; Table[fl=1; Do[s=f[k, n]; If[IntegerQ[s]\u0026\u0026Greater[n, 6], Print[{n, k}; fl=0], {n, 100000}, {k, 1, 100}]"
			],
			"program": [
				"(PARI) ok(n,k)=my(f=factor(n), r=sigma(f,k)/eulerphi(f)); r\u003e=7 \u0026\u0026 denominator(r)==1",
				"a(n)=my(k=7); while(!ok(k, n), k++); k \\\\ _Charles R Greathouse IV_, Nov 27 2013",
				"(Python)",
				"from sympy import divisors, totient as phi",
				"def a(n):",
				"    k, pk = 7, phi(7)",
				"    while sum(pow(d, n, pk) for d in divisors(k, generator=True))%pk != 0:",
				"        k += 1",
				"        pk = phi(k)",
				"    return k",
				"print([a(n) for n in range(1, 16)]) # _Michael S. Branicky_, Dec 22 2021"
			],
			"xref": [
				"Cf. A000203, A001157, A001158, A000010, A015759-A015774, A020492."
			],
			"keyword": "hard,more,nonn",
			"offset": "1,1",
			"author": "_Labos Elemer_, Nov 29 2002",
			"references": 4,
			"revision": 21,
			"time": "2021-12-22T07:03:16-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}