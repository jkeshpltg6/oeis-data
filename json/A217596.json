{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A217596",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 217596,
			"data": "1,-1,-2,-5,-17,-64,-259,-1098,-4815,-21659,-99385,-463385,-2189070,-10455340,-50402858,-244929608,-1198504743,-5900360016,-29204546125,-145244328630,-725451444795,-3637422742470,-18301949731665,-92380935149100,-467659449093330",
			"name": "G.f.: x / reversion(x - x^2 - x^3).",
			"reference": [
				"H. S. Wilf, Generatingfunctionology, Academic Press, NY, 1990."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A217596/b217596.txt\"\u003eTable of n, a(n) for n = 0..600\u003c/a\u003e"
			],
			"formula": [
				"a(n) = -Sum_{i=ceiling(n/2)..n} binomial(i,n-i)*binomial(n+i-2,n-2)/(n -1), n\u003e1, a(0)=1, a(1)=-1.",
				"a(n) = -Sum_{i=1..n} A001002(n-i)*a(i), a(0)=1.",
				"From _Paul D. Hanna_, Mar 19 2013: (Start)",
				"G.f. satisfies:",
				"(1) A(x) = 1 - x/A(x) - x^2/A(x)^2.",
				"(2) A(x - x^2 - x^3) = 1 - x - x^2.",
				"(3) [x^n] A(x)^n = -A000204(n), where A000204 is the Lucas numbers.",
				"(4) [x^n] A(x)^(n+1) = 0 for n\u003e2. (End)",
				"Conjecture: 25*n*(n-1)*a(n) - 5*(n-1)*(25*n-42)*a(n-1) + 3*(-23*n^2 + 59*n + 4)*a(n-2) + 9*(3*n-10)*(3*n-11)*a(n-3)=0. - _R. J. Mathar_, May 23 2014",
				"Maple's sumrecursion command gives the second-order recurrence equation: 5*n*(n - 1)*(4*n - 9)*a(n) = 2*(n - 1)*(44*n^2 - 165*n + 150)*a(n-1) + 3*(4*n - 5)(3*n - 7)(3*n - 8)*a(n-2) with initial conditions a(1) = -1 and a(2) = -2. Mathar's conjectured third-order recurrence follows from this. - _Peter Bala_, Feb 15 2015"
			],
			"example": [
				"G.f.: A(x) = 1 - x - 2*x^2 - 5*x^3 - 17*x^4 - 64*x^5 - 259*x^6 - 1098*x^7 - ..."
			],
			"mathematica": [
				"CoefficientList[x/InverseSeries[Series[x-x^2-x^3, {x, 0, 20}], x],x] (* _Vaclav Kotesovec_, Feb 15 2015 *)"
			],
			"program": [
				"(Maxima)",
				"a(n):=if n=0 then 1 else if n=1 then -1 else -sum(binomial(i,n-i)*binomial(n+i-2,n-2),i,ceiling(n/2),n)/(n-1);",
				"(PARI) {a(n)=polcoeff(x/serreverse(x-x^2-x^3+x^2*O(x^n)),n)} \\\\ _Paul D. Hanna_, Mar 19 2013",
				"(PARI) /* Using Vladimir Kruchinin's binomial sum: */",
				"{a(n)=if(n==0,1,if(n==1,-1,-sum(i=n\\2,n,binomial(i,n-i)*binomial(n+i-2,n-2))/(n-1)))} \\\\ _Paul D. Hanna_, Mar 19 2013",
				"(PARI) {a(n)=local(A=1);for(i=1,n,A=1-x/A-x^2/A^2+x*O(x^n));polcoeff(A,n)} \\\\ _Paul D. Hanna_, Mar 19 2013"
			],
			"xref": [
				"Cf. A001002, A007440."
			],
			"keyword": "sign,easy",
			"offset": "0,3",
			"author": "_Vladimir Kruchinin_, Mar 19 2013",
			"references": 1,
			"revision": 34,
			"time": "2018-08-31T15:57:41-04:00",
			"created": "2013-03-19T23:39:44-04:00"
		}
	]
}