{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003575",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3575,
			"data": "1,2,7,35,214,1523,12349,112052,1120849,12219767,143942992,1819256321,24526654381,350974470746,5308470041299,84554039118383,1413794176669942,24745966692370607,452277149756692105,8612255652371171012,170517319084490074405",
			"name": "Dowling numbers: e.g.f.: exp(x + (exp(b*x) - 1)/b) with b=3.",
			"comment": [
				"Named after the American mathematician Thomas Allan Dowling (b. 1941). - _Amiram Eldar_, Jun 06 2021"
			],
			"link": [
				"Muniru A Asiru, \u003ca href=\"/A003575/b003575.txt\"\u003eTable of n, a(n) for n = 0..210\u003c/a\u003e",
				"Moussa Benoumhani, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(95)00095-E\"\u003eOn Whitney numbers of Dowling lattices\u003c/a\u003e, Discrete Math., Vol. 159, No. 1-3 (1996), pp. 13-33.",
				"Thomas A. Dowling, \u003ca href=\"https://doi.org/10.1016/S0095-8956(73)80007-3\"\u003eA class of geometric lattices based on finite groups\u003c/a\u003e, Journal of Combinatorial Theory, Series B, Vol. 14, No. 1 (1973), pp. 61-86.",
				"Mahid M. Mangontarum and Jacob Katriel, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL18/Mangontarum/mango2.html\"\u003eOn q-Boson Operators and q-Analogues of the r-Whitney and r-Dowling Numbers\u003c/a\u003e, J. Int. Seq., Vol. 18 (2015), Article 15.9.8."
			],
			"formula": [
				"E.g.f.: exp(x + (exp(3*x) - 1)/3).",
				"G.f.: 1/(1-x*Q(0)), where Q(k) = 1 + x/(1 - x + 3*x*(k+1)/(x - 1/Q(k+1))); (continued fraction). - _Sergei N. Gladkovskii_, May 19 2013",
				"a(n) = exp(-1/3) * Sum_{k\u003e=0} (3*k + 1)^n / (3^k * k!). - _Ilya Gutkovskiy_, Apr 16 2020"
			],
			"maple": [
				"seq(coeff(series(n!*exp(z+(1/3)*exp(3*z)-(1/3)),z,n+1), z, n), n=0..30); # _Muniru A Asiru_, Feb 19 2019"
			],
			"mathematica": [
				"With[{nn=20},CoefficientList[Series[Exp[x+Exp[3x]/3-1/3],{x,0,nn}],x] Range[0,nn]!] (* _Harvey P. Dale_, Jan 04 2019 *)",
				"Table[Sum[Binomial[n, k] * 3^k * BellB[k, 1/3], {k, 0, n}], {n, 0, 20}] (* _Vaclav Kotesovec_, Apr 17 2020 *)"
			],
			"program": [
				"(PARI) my(x = 'x + O('x^30)); Vec(serlaplace(exp(x + exp(3*x)/3 - 1/3))) \\\\ _Michel Marcus_, Feb 09 2018",
				"(MAGMA) m:=30; c:=3; R\u003cx\u003e:=PowerSeriesRing(Rationals(), m); b:=Coefficients(R!( Exp(x+(Exp(c*x)-1)/c) )); [Factorial(n-1)*b[n]: n in [1..m]]; // _G. C. Greubel_, Feb 20 2019",
				"(Sage)",
				"b=3;",
				"def A003575_list(prec):",
				"    P.\u003cx\u003e = PowerSeriesRing(QQ, prec)",
				"    return P( exp(x +(exp(b*x)-1)/b) ).egf_to_ogf().list()",
				"A003575_list(30) # _G. C. Greubel_, Feb 20 2019"
			],
			"xref": [
				"Cf. A000110 (b=1), A007405 (b=2), this sequence (b=3), A003576 (b=4), A003577 (b=5), A003578 (b=6), A003579 (b=7), A003580 (b=8), A003581 (b=9), A003582 (b=10)."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Name clarified by _G. C. Greubel_, Feb 20 2019"
			],
			"references": 13,
			"revision": 48,
			"time": "2021-06-06T07:36:35-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}