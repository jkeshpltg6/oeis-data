{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A327570",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 327570,
			"data": "1,2,12,16,80,24,252,128,324,160,1100,192,1872,504,960,1024,4352,648,6156,1280,3024,2200,11132,1536,10000,3744,8748,4032,22736,1920,27900,8192,13200,8704,20160,5184,47952,12312,22464,10240,65600,6048,75852,17600,25920,22264,99452,12288",
			"name": "a(n) = n*phi(n)^2, phi = A000010.",
			"comment": [
				"a(n) is the order of the group consisting of all upper-triangular (or equivalently, lower-triangular) matrices in GL(2, Z_n). That is to say, a(n) = |G_n|, where G_n = {{{a, b}, {0, d}} : gcd(a, n) = gcd(d, n) = 1}. The group G_n is well-defined because the product of two upper-triangular matrices is again an upper-triangular matrix. For example,{{a, b}, {0, d}} * {{x, y}, {0, z}} = {{a*x, a*y+b*z}, {0, d*z}}.",
				"The exponent of G_n (i.e., the least positive integer k such that x^k = e for all x in G_n) is A174824(n). (Note that {{1, 1}, {0, 1}} is an element with order n and there exists some r such that {{r, 0}, {0, r}} is an element with order psi(n), psi = A002322. It is easy to show that x^lcm(n, psi(n)) = Id = {{1, 0}, {0, 1}} for all x in G_n.)",
				"If only upper-triangular matrices in SL(2, Z_n) are wanted, we get a group of order n*phi(n) = A002618(n) and exponent A174824(n)."
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A327570/b327570.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"Multiplicative with a(p^e) = (p-1)^2*p^(3e-2).",
				"a(n) = A000010(n)*A002618(n).",
				"a(p) = A011379(p-1) for p prime. - _Peter Luschny_, Sep 17 2019",
				"Sum_{k\u003e=1} 1/a(k) = Product_{primes p} (1 + p^2/((p-1)^3 * (p^2 + p + 1))) = 1.7394747912949637836019917301710010334604379331855033150372654868327481539... - _Vaclav Kotesovec_, Sep 20 2020"
			],
			"example": [
				"G_3 = {{{1, 0}, {0, 1}}, {{1, 1}, {0, 1}}, {{1, 2}, {0, 1}}, {{1, 0}, {0, 2}}, {{1, 1}, {0, 2}}, {{1, 2}, {0, 2}}, {{2, 0}, {0, 1}}, {{2, 1}, {0, 1}}, {{2, 2}, {0, 1}}, {{2, 0}, {0, 2}}, {{2, 1}, {0, 2}}, {{2, 2}, {0, 2}}} with order 12, so a(3) = 12."
			],
			"mathematica": [
				"Table[n * EulerPhi[n]^2, {n, 1, 100}] (* _Amiram Eldar_, Sep 19 2020 *)"
			],
			"program": [
				"(PARI) a(n) = n*eulerphi(n)^2"
			],
			"xref": [
				"Cf. A000010, A000252, A002618, A174824, A011379."
			],
			"keyword": "nonn,easy,mult",
			"offset": "1,2",
			"author": "_Jianing Song_, Sep 17 2019",
			"references": 1,
			"revision": 23,
			"time": "2020-09-20T05:03:39-04:00",
			"created": "2019-09-17T08:57:51-04:00"
		}
	]
}