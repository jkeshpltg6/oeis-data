{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A299018",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 299018,
			"data": "1,2,2,6,11,6,24,60,60,24,120,366,501,366,120,720,2532,4242,4242,2532,720,5040,19764,38268,46863,38268,19764,5040,40320,172512,373104,528336,528336,373104,172512,40320,362880,1668528,3942108,6237828,7213761,6237828,3942108,1668528,362880",
			"name": "Triangle read by rows: T(n,k) is the coefficient of x^k in the polynomial P(n) = n*(x + 1)*P(n - 1) - (n - 2)^2*x*P(n - 2).",
			"formula": [
				"P(0) = 0, P(1) = 1 and P(n) = n * (x + 1) * P(n - 1) - (n - 2)^2 * x * P(n - 2)."
			],
			"example": [
				"For n = 3, the polynomial is 6*x^2 + 11*x + 6.",
				"The first few polynomials, as a table:",
				"[1],",
				"[2,    2],",
				"[6,    11,    6],",
				"[24,   60,    60,  24],",
				"[120,  366,   501, 366, 120]"
			],
			"maple": [
				"P:= proc(n) option remember; expand(`if`(n\u003c2, n,",
				"      n*(x+1)*P(n-1)-(n-2)^2*x*P(n-2)))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=0..n-1))(P(n)):",
				"seq(T(n), n=1..12);  # _Alois P. Heinz_, Jan 31 2018",
				"A := proc(n,k) ## n \u003e= 0 and k = 0 .. n",
				"    option remember;",
				"    if n = 0 and k = 0 then",
				"        1",
				"    elif n \u003e 0 and k \u003e= 0 and k \u003c= n then",
				"        (n+1)*(A(n-1,k)+A(n-1,k-1))-(n-1)^2*A(n-2,k-1)",
				"    else",
				"        0",
				"    end if;",
				"end proc: # _Yu-Sheng Chang_, Apr 14 2020"
			],
			"mathematica": [
				"P[n_] := P[n] = Expand[If[n \u003c 2, n, n (x+1) P[n-1] - (n-2)^2 x P[n-2]]];",
				"row[n_] := CoefficientList[P[n], x];",
				"row /@ Range[12] // Flatten (* _Jean-François Alcover_, Dec 10 2019 *)"
			],
			"program": [
				"(Sage)",
				"@cached_function",
				"def poly(n):",
				"    x = polygen(ZZ, 'x')",
				"    if n \u003c 1:",
				"        return x.parent().zero()",
				"    elif n == 1:",
				"        return x.parent().one()",
				"    else:",
				"        return n * (x + 1) * poly(n - 1) - (n - 2)**2 * x * poly(n - 2)"
			],
			"xref": [
				"Very similar to A298854.",
				"Row sums are A277382(n-1) for n\u003e0.",
				"Leftmost and rightmost columns are A000142.",
				"Alternating row sums are A177145.",
				"Alternating row sum of row 2*n+1 is A001818(n)."
			],
			"keyword": "tabl,nonn,easy",
			"offset": "1,2",
			"author": "_F. Chapoton_, Jan 31 2018",
			"references": 0,
			"revision": 20,
			"time": "2020-05-24T04:24:32-04:00",
			"created": "2018-01-31T16:01:11-05:00"
		}
	]
}