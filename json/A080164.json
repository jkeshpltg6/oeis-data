{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A080164",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 80164,
			"data": "1,2,3,5,7,4,13,18,10,6,34,47,26,15,8,89,123,68,39,20,9,233,322,178,102,52,23,11,610,843,466,267,136,60,28,12,1597,2207,1220,699,356,157,73,31,14,4181,5778,3194,1830,932,411,191,81,36,16,10946,15127,8362,4791,2440",
			"name": "Wythoff difference array, D={d(i,j)}, by antidiagonals.",
			"comment": [
				"D is an interspersion formed by differences between Wythoff pairs in the Wythoff array W={w(i,j)}=A035513 (indexed so that i and j start at 1): d(i,j)=w(i,2j)-w(i,2j-1).",
				"The difference between adjacent column terms is a Fibonacci number: d(i+1,j)-d(i,j) is F(2j) or F(2j+1).",
				"Every term in column 1 of W is in column 1 of D; moreover, in row i of D, every term except the first is in row i of W.",
				"Let W' be the array remaining when all the odd-numbered columns of W are removed from W. The rank array of W' (obtained by replacing each w'(i,j) by its rank when all the numbers w'(h,k) are arranged in increasing order) is D.",
				"Let W\" be the array remaining when all the even-numbered columns of W are removed from W; the rank array of W\" is D.",
				"Let D' be the array remaining when column 1 of D is removed; the rank array of D' is D.",
				"Let E be the array {e(i,j)} given by e(i,j)=d(i,2j)-d(i,2j-1); the rank array of E is D.",
				"D is the dispersion of the sequence u given by u(n)=n+floor(n*x), where x=(golden ratio); that is, D is the dispersion of the upper Wythoff sequence, A001950.  For a discussion of dispersions, see A191426.",
				"In column 1, F(2n) is in position F(2n-1)  - _Clark Kimberling_, Jul 15 2016"
			],
			"reference": [
				"Clark Kimberling, The Wythoff difference array, in Applications of Fibonacci Numbers, vol.10, Proceedings of the Eleventh International Conference on Fibonacci Numbers and Their Applications, William Webb, editor, Congressus Numerantium, Winnipeg, Manitoba 194 (2009) 153-158."
			],
			"link": [
				"Eric Duchêne, Aviezri S. Fraenkel, Vladimir Gurvich, Nhan Bao Ho, Clark Kimberling, and Urban Larsson, \u003ca href=\"https://books.google.com/books?hl=en\u0026amp;lr=lang_en\u0026amp;id=i7WQDwAAQBAJ\u0026amp;oi=fnd\u0026amp;pg=PA65\"\u003eWythoff Visions\u003c/a\u003e, Games of No Chance 5 (2017) Vol. 70, See p. 65.",
				"Clark Kimberling, \u003ca href=\"http://faculty.evansville.edu/ck6/integer/intersp.html\"\u003eInterspersions\u003c/a\u003e",
				"Clark Kimberling, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL10/Kimberling/kimberling26.html\"\u003eComplementary Equations\u003c/a\u003e, Journal of Integer Sequences, Vol. 10 (2007), Article 07.1.4.",
				"Clark Kimberling, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL23/Kimberling/kimber12.html\"\u003eLucas Representations of Positive Integers\u003c/a\u003e, J. Int. Seq., Vol. 23 (2020), Article 20.9.5.",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"d(i, j)=[i*tau]F(2j-1)+(i-1)F(2j-2), where F=A000045 (Fibonacci numbers). d(i, j)=[tau*d(i, j-1)]+d(i, j-1) for i\u003e=2. d(i, j)=3d(i, j-1)-d(i, j-2) for i\u003e=3."
			],
			"example": [
				"Northwest corner:",
				"1   2   5   13   34   89",
				"3   7   18  47   123  322",
				"4   10  26  68   178  466",
				"6   15  39  102  267  699",
				"8   20  52  136  356  932",
				"9   23  60  157  411  1076"
			],
			"mathematica": [
				"(* program generates the dispersion array T of the complement of increasing sequence f[n] *)",
				"r = 40; r1 = 12; (* r=# rows of T, r1=# rows to show *)",
				"c = 40; c1 = 12; (* c=# cols of T, c1=# cols to show *)",
				"x = 1 + GoldenRatio; f[n_] := Floor[n*x]",
				"(* f(n) is complement of column 1 *)",
				"mex[list_] := NestWhile[#1 + 1 \u0026, 1, Union[list][[#1]] \u003c= #1 \u0026, 1, Length[Union[list]]]",
				"rows = {NestList[f, 1, c]};",
				"Do[rows = Append[rows, NestList[f, mex[Flatten[rows]], r]], {r}];",
				"t[i_, j_] := rows[[i, j]];",
				"TableForm[Table[t[i, j], {i, 1, 10}, {j, 1, 10}]]",
				"(* A080164 as an array *)",
				"Flatten[Table[t[k, n - k + 1], {n, 1, c1}, {k, 1, n}]]",
				"(* A080164 as a sequence *)",
				"(* Program by _Peter J. C. Moses_, Jun 01 2011, added here by _Clark Kimberling_, Jun 03 2011 *)"
			],
			"xref": [
				"Cf. A035513, A000201, A001950, A000045."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Feb 08 2003",
			"references": 6,
			"revision": 29,
			"time": "2021-02-05T22:27:34-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}