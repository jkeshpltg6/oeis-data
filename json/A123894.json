{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A123894",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 123894,
			"data": "1,5,25,115,525,2405,11025,50525,231525,1060979,4862065,22280875,102104045,467901065,2144199765,9825991725,45028504525,206347239279,945605099381,4333321849875,19857843661645,91000384646845,417017585036185,1911021221349165",
			"name": "Expansion of g.f.: (1+x^2)*(1+2*x^2)*(1+3*x^2)*(1+4*x^2)/(1-5*x+10*x^2 -40*x^3+35*x^4-105*x^5 +50*x^6-100*x^7+24*x^8-24*x^9).",
			"comment": [
				"Number of words of length n over (0,1,2,3,4} which have no factor iji with i\u003ej. - _N. J. A. Sloane_, May 21 2013"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A123894/b123894.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"A. Burstein and T. Mansour, \u003ca href=\"https://arxiv.org/abs/math/0112281\"\u003eWords restricted by 3-letter generalized multipermutation patterns\u003c/a\u003e, arXiv:math/0112281 [math.CO], 2001; Annals. Combin., 7 (2003), 1-14.",
				"\u003ca href=\"/index/Rec#order_09\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (5,-10,40,-35,105,-50,100,-24,24)."
			],
			"formula": [
				"G.f. may be written more symmetrically as 1/(1-x*(1 +1/(1+x^2) +1/(1+2*x^2) +1/(1+3*x^2) +1/(1+4*x^2))). - _N. J. A. Sloane_, May 21 2013"
			],
			"maple": [
				"seq(coeff(series((1+x^2)*(1+2*x^2)*(1+3*x^2)*(1+4*x^2)/(1 -5*x +10*x^2 -40*x^3+35*x^4-105*x^5+50*x^6-100*x^7+24*x^8-24*x^9), x, n+1), x, n), n = 0 .. 30); # _G. C. Greubel_, Aug 06 2019"
			],
			"mathematica": [
				"CoefficientList[1/(1 - x(1 + 1/(1+x^2) + 1/(1+2x^2) + 1/(1+3x^2) + 1/(1+4x^2))) + O[x]^30, x] (* _Jean-François Alcover_, Jan 09 2019 *)"
			],
			"program": [
				"(PARI) my(x='x+O('x^30)); Vec((1+x^2)*(1+2*x^2)*(1+3*x^2)*(1+4*x^2)/(1 -5*x +10*x^2 -40*x^3+35*x^4-105*x^5+50*x^6-100*x^7+24*x^8-24*x^9)) \\\\ _G. C. Greubel_, Aug 06 2019",
				"(MAGMA) R\u003cx\u003e:=PowerSeriesRing(Integers(), 30); Coefficients(R!( (1+x^2)*(1+2*x^2)*(1+3*x^2)*(1+4*x^2)/(1-5*x+10*x^2 -40*x^3+35*x^4-105*x^5 +50*x^6-100*x^7+24*x^8-24*x^9) )); // _G. C. Greubel_, Aug 06 2019",
				"(Sage) ((1+x^2)*(1+2*x^2)*(1+3*x^2)*(1+4*x^2)/(1-5*x+10*x^2-40*x^3 +35*x^4-105*x^5 +50*x^6-100*x^7+24*x^8-24*x^9)).series(x, 30).coefficients(x, sparse=False) # _G. C. Greubel_, Aug 06 2019",
				"(GAP) a:=[1, 5, 25, 115, 525, 2405, 11025, 50525, 231525];; for n in [10..30] do a[n]:=5*a[n-1]-10*a[n-2] +40*a[n-3]-35*a[n-4]+105*a[n-5] -50*a[n-6]+100*a[n-7]-24*a[n-8]+24*a[n-9]; od; a; # _G. C. Greubel_, Aug 06 2019"
			],
			"xref": [
				"Cf. A005251, A123892, A123893, A225685."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Nov 20 2006",
			"references": 4,
			"revision": 23,
			"time": "2019-08-06T19:05:16-04:00",
			"created": "2006-12-06T03:00:00-05:00"
		}
	]
}