{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A125806",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 125806,
			"data": "1,1,1,1,2,1,1,3,3,1,1,4,8,4,1,1,5,16,16,5,1,1,6,29,48,29,6,1,1,7,47,119,119,47,7,1,1,8,72,256,390,256,72,8,1,1,9,104,500,1070,1070,500,104,9,1,1,10,145,900,2592,3656,2592,900,145,10,1,1,11,195,1525,5674,10762",
			"name": "Triangle of the sum of squared coefficients of q in the q-binomial coefficients, read by rows.",
			"comment": [
				"Central terms equal A063075 (number of partitions of 2n^2 whose Ferrers-plot fits within a 2n X 2n box and cover an n X n box).",
				"The matrix inverse starts",
				"1;",
				"-1,1;",
				"1,-2,1;",
				"-1,3,-3,1;",
				"-1,0,4,-4,1;",
				"9,-21,12,4,-5,1;",
				"-1,34,-73,44,1,-6,1;",
				"-219,479,-219,-139,109,-5,-7,1;",
				"101,-1536,3072,-1776,-54,216,-16,-8,1; - _R. J. Mathar_, Mar 22 2013"
			],
			"link": [
				"Paul D. Hanna, \u003ca href=\"/A125806/b125806.txt\"\u003eRows n = 0..45, listed in flattened form as n, a(n), for n = 0..1080.\u003c/a\u003e"
			],
			"example": [
				"The triangle of q-binomial coefficients:",
				"C_q(n,k) = [Product_{i=n-k+1..n}(1-q^i)]/[Product_{j=1..k}(1-q^j)]",
				"begins:",
				"1;",
				"1, 1;",
				"1, 1+q, 1;",
				"1, 1+q+q^2, 1+q+q^2, 1;",
				"1, 1+q+q^2+q^3, 1+q+2*q^2+q^3+q^4, 1+q+q^2+q^3, 1; ...",
				"recurrence: C_q(n+1,k) = C_q(n,k-1) + q^k * C_q(n,k).",
				"Element T(n,k) of this triangle equals the sum of the squares",
				"of the coefficients of q in q-binomial coefficient C_q(n,k).",
				"This triangle begins:",
				"1;",
				"1, 1;",
				"1, 2, 1;",
				"1, 3, 3, 1;",
				"1, 4, 8, 4, 1;",
				"1, 5, 16, 16, 5, 1;",
				"1, 6, 29, 48, 29, 6, 1;",
				"1, 7, 47, 119, 119, 47, 7, 1;",
				"1, 8, 72, 256, 390, 256, 72, 8, 1;",
				"1, 9, 104, 500, 1070, 1070, 500, 104, 9, 1;",
				"1, 10, 145, 900, 2592, 3656, 2592, 900, 145, 10, 1;",
				"1, 11, 195, 1525, 5674, 10762, 10762, 5674, 1525, 195, 11, 1;",
				"1, 12, 256, 2456, 11483, 28160, 37834, 28160, 11483, 2456, 256, 12, 1;",
				"The central terms equal A063075:",
				"1, 2, 8, 48, 390, 3656, 37834, 417540, 4836452, 58130756, ..."
			],
			"maple": [
				"C := proc(q,n,k)",
				"    local i,j ;",
				"    mul(1-q^i,i=n-k+1..n)/mul(1-q^j,j=1..k) ;",
				"    expand(factor(%)) ;",
				"end proc:",
				"A125806 := proc(n,k)",
				"    local qbin ,q;",
				"    qbin := [coeffs(C(q,n,k),q)] ;",
				"    add( e^2,e=qbin) ;",
				"end proc: # _R. J. Mathar_, Mar 22 2013"
			],
			"program": [
				"(PARI) T(n,k)=local(C_q=if(n==0 || k==0,1,prod(j=n-k+1,n,1-q^j)/prod(j=1,k,1-q^j))); sum(i=0,(n-k)*k,polcoeff(C_q,i)^2)",
				"for(n=0,12,for(k=0,n,print1(T(n,k),\", \"));print(\"\"))"
			],
			"xref": [
				"Cf. A063075 (central terms); A125807, A125808, A125809 (row sums)."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Paul D. Hanna_, Dec 11 2006",
			"references": 4,
			"revision": 11,
			"time": "2017-06-14T00:16:10-04:00",
			"created": "2007-01-12T03:00:00-05:00"
		}
	]
}