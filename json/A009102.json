{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A009102",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 9102,
			"data": "1,-1,1,-3,13,-65,389,-2723,21785,-196065,1960649,-21567139,258805669,-3364473697,47102631757,-706539476355,11304631621681,-192178737568577,3459217276234385,-65725128248453315,1314502564969066301",
			"name": "Expansion of e.g.f. cos(x)/(1+x).",
			"comment": [
				"The absolute value of a(n) equals the real part of the permanent of the n X n matrix with (1+i)'s along the main diagonal, and 1's everywhere else. - _John M. Campbell_, Jul 10 2011"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A009102/b009102.txt\"\u003eTable of n, a(n) for n = 0..438\u003c/a\u003e",
				"Eric Weisstein's MathWorld, \u003ca href=\"http://mathworld.wolfram.com/IncompleteGammaFunction.html\"\u003eIncomplete Gamma Function\u003c/a\u003e."
			],
			"formula": [
				"a(n) = (-1)^n*round(n!*cos(1)). - _Vladeta Jovovic_, Aug 11 2002",
				"a(n) = (-1)^n * n! * Sum_{k=0..floor(n/2} (-1)^k/(2k)!. Unsigned sequence satisfies e.g.f. cos(x)/(1-x). - _Ralf Stephan_, Apr 16 2004",
				"E.g.f.: cos(x)/(1+x) = U(0)/(1-x^2) where U(k)= 1 - x/(1 - x/(x + (2*k+1)*(2*k+2)/U(k+1)) ; (continued fraction, 3-step). - _Sergei N. Gladkovskii_, Oct 17 2012",
				"From _Vladimir Reshetnikov_, Oct 27 2015: (Start)",
				"a(n) = Re((-i)^n*hypergeom([1,-n], [], i)).",
				"a(n) = (-1)^n*(cos(1)*(n+2)!+cos(Pi*n/2)*hypergeom([1], [n/2+2,(n+3)/2], -1/4)+sin(Pi*n/2)*(n+2)*hypergeom([1], [n/2+1,(n+3)/2], -1/4))/(n^2+3*n+2).",
				"a(n) = (-1)^n*Re(Gamma(n+1, i)*exp(i)) = (-1)^n*(Gamma(n+1, i)*exp(i)+Gamma(n+1, -i)*exp(-i))/2, where Gamma(a, x) is the upper incomplete Gamma function, i=sqrt(-1).",
				"Gamma(n+1, i) = exp(-i)*((-1)^n*a(n) + A009551(n)*i).",
				"Recurrence: a(0) = 1, a(1) = -1, a(2) = 1, a(n+3) = -(n+3)*a(n+2)-a(n+1)-(n+1)*a(n). (End)"
			],
			"maple": [
				"G(x):=cos(x)/(1+x): f[0]:=G(x): for n from 1 to 20 do f[n]:=diff(f[n-1],x) od: x:=0: seq(f[n],n=0..20); # _Zerinvary Lajos_, Apr 03 2009",
				"g:= gfun:-rectoproc({a(0) = 1, a(1) = -1, a(2) = 1, a(n+3) = -(n+3)*a(n+2)-a(n+1)-(n+1)*a(n)},a(n),remember):",
				"seq(g(n),n=0..30); # _Robert Israel_, Oct 27 2015"
			],
			"mathematica": [
				"Table[SeriesCoefficient[Cos[x]/(1+x), {x, 0, n}] n!, {n, 0, 20}]",
				"Round@Table[(-1)^n Re[Gamma[n+1, I] E^I], {n, 0, 20}] (* _Vladimir Reshetnikov_, Oct 27 2015 *)"
			],
			"program": [
				"(PARI) x='x+O('x^30); Vec(serlaplace(cos(x)/(1+x))) \\\\ _G. C. Greubel_, Jul 26 2018",
				"(MAGMA) m:=30; R\u003cx\u003e:=PowerSeriesRing(Rationals(), m); b:=Coefficients(R!(Cos(x)/(1+x))); [Factorial(n-1)*b[n]: n in [1..m]]; // _G. C. Greubel_, Jul 26 2018"
			],
			"xref": [
				"Cf. A009551, A000142, A000166, A000522, A000023, A053486, A010844 (incomplete Gamma function values at other points)."
			],
			"keyword": "sign,easy",
			"offset": "0,4",
			"author": "_R. H. Hardin_",
			"ext": [
				"Extended with signs by _Olivier Gérard_, Mar 15 1997"
			],
			"references": 6,
			"revision": 51,
			"time": "2018-07-26T08:04:25-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}