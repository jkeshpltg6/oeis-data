{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007429",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7429,
			"id": "M3249",
			"data": "1,4,5,11,7,20,9,26,18,28,13,55,15,36,35,57,19,72,21,77,45,52,25,130,38,60,58,99,31,140,33,120,65,76,63,198,39,84,75,182,43,180,45,143,126,100,49,285,66,152,95,165,55,232,91,234,105,124,61,385,63,132,162,247,105,260",
			"name": "Inverse Moebius transform applied twice to natural numbers.",
			"reference": [
				"D. M. Burton, Elementary Number Theory, Allyn and Bacon Inc., Boston, MA, 1976, p. 120.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Travis Scholl, \u003ca href=\"/A007429/b007429.txt\"\u003eTable of n, a(n) for n = 1..100000\u003c/a\u003e (terms 1 through 1000 were by T. D. Noe)",
				"O. Bordelles, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL10/Bordelles2/bordelles61.html\"\u003eMean values of generalized gcd-sum and lcm-sum functions\u003c/a\u003e, JIS 10 (2007) 07.9.2, series g_4 (with an apparently wrong D.g.f. after equation 3).",
				"N. J. A. Sloane, \u003ca href=\"/transforms.txt\"\u003eTransforms\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Sum_{d|n} sigma(d), Dirichlet convolution of A000203 and A000012. - _Jason Earls_, Jul 07 2001",
				"a(n) = Sum_{d|n} d*tau(n/d). - _Vladeta Jovovic_, Jul 31 2002",
				"Multiplicative with a(p^e) = (p*(p^(e+1)-1)-(p-1)*(e+1))/(p-1)^2. - _Vladeta Jovovic_, Dec 25 2001",
				"G.f.: Sum_{k\u003e=1} sigma(k)*x^k/(1-x^k). - _Benoit Cloitre_, Apr 21 2003",
				"Moebius transform of A007430. - _Benoit Cloitre_, Mar 03 2004",
				"Dirichlet g.f.: zeta(s-1)*zeta^2(s).",
				"Equals A051731^2 * [1, 2, 3, ...]. Equals row sums of triangle A134577. - _Gary W. Adamson_, Nov 02 2007",
				"Row sums of triangle A134699. - _Gary W. Adamson_, Nov 06 2007",
				"a(n) = n * (Sum_{d|n} tau(d)/d) = n * (A276736(n) / A276737(n)). - _Jaroslav Krizek_, Sep 24 2016",
				"L.g.f.: -log(Product_{k\u003e=1} (1 - x^k)^(sigma(k)/k)) = Sum_{n\u003e=1} a(n)*x^n/n. - _Ilya Gutkovskiy_, May 26 2018"
			],
			"maple": [
				"A007429 := proc(n)",
				"    add(numtheory[sigma](d),d=numtheory[divisors](n)) ;",
				"end proc:",
				"seq(A007429(n),n=1..100) ; # _R. J. Mathar_, Aug 28 2015"
			],
			"mathematica": [
				"f[n_] := Plus @@ DivisorSigma[1, Divisors@n]; Array[f, 52] (* _Robert G. Wilson v_, May 05 2010 *)"
			],
			"program": [
				"(PARI) j=[]; for(n=1,200,j=concat(j,sumdiv(n,d, sigma(d)))); j",
				"(PARI) a(n)=if(n\u003c1,0,direuler(p=2,n,1/(1-X)^2/(1-p*X))[n]) \\\\ _Ralf Stephan_",
				"(PARI)",
				"N=17; default(seriesprecision,N); x=z+O(z^(N+1))",
				"c=sum(j=1,N,j*x^j);",
				"t=1/prod(j=1,N, eta(x^(j))^(1/j))",
				"t=log(t)",
				"t=serconvol(t,c)",
				"Vec(t)",
				"/* _Joerg Arndt_, May 03 2008 */",
				"(PARI) a(n)=sumdiv(n,d, sumdiv(d,t, t ) );  /* _Joerg Arndt_, Oct 07 2012 */",
				"(Sage) def A(n): return sum(sigma(d) for d in n.divisors()) # _Travis Scholl_, Apr 14 2016",
				"(MAGMA) [\u0026+[SumOfDivisors(d): d in Divisors(n)]: n in [1..100]] // _Jaroslav Krizek_, Sep 24 2016"
			],
			"xref": [
				"Cf. A000203, A007430, A134699, A280077."
			],
			"keyword": "nonn,easy,nice,mult",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"references": 57,
			"revision": 75,
			"time": "2021-01-03T15:12:20-05:00",
			"created": "1994-05-24T03:00:00-04:00"
		}
	]
}