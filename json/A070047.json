{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A070047",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 70047,
			"data": "1,1,2,1,3,3,5,5,8,8,12,12,19,19,27,28,39,41,55,58,77,82,106,113,145,156,196,210,262,283,348,376,459,497,600,651,781,849,1009,1097,1298,1413,1660,1807,2113,2302,2676,2916,3377,3681,4242,4623,5309,5787,6619",
			"name": "Number of partitions of n in which no part appears more than twice and no two parts differ by 1.",
			"comment": [
				"Coefficients in expansion of permanent of infinite tridiagonal matrix: matrix([[1, x, 0, 0, 0, ...], [1+x, 1, x^2, 0, 0, ...], [0, 1+x^2, 1, x^3, 0, ...], [0, 0, 1+x^3, 1, x^4, ...], ...]). - _Vladeta Jovovic_, Jul 18 2004",
				"Number of partitions of n into non-multiples of 3 in which no two parts differ by 1 (see the Andrews-Lewis reference). Example: a(6)=5 because we have 51,42,411,222,111111. - _Emeric Deutsch_, May 19 2008",
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"reference": [
				"D. M. Bressoud, Analytic and combinatorial generalizations of the Rogers-Ramanujan identities, Mem. Amer. Math. Soc. 24 (1980), no. 227, 54 pp."
			],
			"link": [
				"Reinhard Zumkeller and Alois P. Heinz, \u003ca href=\"/A070047/b070047.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e (terms 0..120 from Reinhard Zumkeller)",
				"G. E. Andrews and R. P. Lewis, \u003ca href=\"http://dx.doi.org/10.1016/S0012-365X(00)00295-8\"\u003eAn algebraic identity of F. H. Jackson and its implications for partitions\u003c/a\u003e, Discrete Math., 232 (2001), 77-83. see equations (3.1) and (3.2)",
				"Bin Lan and James A. Sellers, \u003ca href=\"http://www.emis.de/journals/INTEGERS/papers/p23/p23.pdf\"\u003eProperties of a Restricted Binary Partition Function a la Andrews and Lewis\u003c/a\u003e, Electronic Journal of Combinatorial Number Theory, Volume 15 #A23.",
				"Andrew Sills, \u003ca href=\"https://works.bepress.com/andrew_sills/40/\"\u003eRademacher-Type Formulas for Restricted Partition and Overpartition Functions\u003c/a\u003e, Ramanujan Journal, 23 (1-3): 253-264, 2010.",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Bailey_pair\"\u003eBailey pair\u003c/a\u003e",
				"Mingjia Yang, Doron Zeilberger, \u003ca href=\"https://arxiv.org/abs/1910.08989\"\u003eSystematic Counting of Restricted Partitions\u003c/a\u003e, arXiv:1910.08989 [math.CO], 2019."
			],
			"formula": [
				"Expansion of phi(-x^3) / f(-x) in powers of x where phi(), f() are Ramanujan theta functions. - _Michael Somos_, Jun 02 2011",
				"Expansion of q^(1/24) * eta(q^3)^2 / (eta(q) * eta(q^6)) in powers of q. - _Michael Somos_, Dec 04 2002",
				"Euler transform of period 6 sequence [ 1, 1, -1, 1, 1, 0, ...]. - _Michael Somos_, Dec 04 2002",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (1152 t)) = (2/3)^(1/2) g(t) where q = exp(2 Pi i t) and g is the g.f. of A233006.",
				"G.f.: Prod_{k\u003e0} (1 - x^(6*k - 3))^2 * (1 - x^(6*k)) / (1 - x^k).",
				"G.f.: Prod_{n\u003e0}[(1-q^(6n-3))/[(1-q^(3n-2))(1-q^(3n-1))]]. - _Emeric Deutsch_, May 19 2008",
				"a(n) ~ 2*Pi * BesselI(1, Pi/6 * sqrt((24*n-1)/2)) / sqrt(3*(24*n-1)) ~ exp(Pi*sqrt(n/3)) / (2*3^(3/4)*n^(3/4)) * (1 - (3*sqrt(3)/(8*Pi) + Pi/(48*sqrt(3)))/sqrt(n) + (Pi^2/13824 - 45/(128*Pi^2) + 5/128)/n). - _Vaclav Kotesovec_, Sep 02 2015, extended Jan 11 2017"
			],
			"example": [
				"G.f. = 1 + x + 2*x^2 + x^3 + 3*x^4 + 3*x^5 + 5*x^6 + 5*x^7 + 8*x^8 + 8*x^9 + 12*x^10 + ...",
				"G.f. = 1/q + q^23 + 2*q^47 + q^71 + 3*q^95 + 3*q^119 + 5*q^143 + 5*q^167 + 8*q^191 + ...",
				"a(6)=5 because we have 6,51,42,411,33."
			],
			"maple": [
				"b:= proc(n, i) option remember; `if`(n=0, 1, `if`(i\u003c1, 0,",
				"       b(n, i-1) +add(b(n-i*j, i-2), j=1..min(n/i, 2))))",
				"    end:",
				"a:= n-\u003e b(n, n):",
				"seq(a(n), n=0..60);  # _Alois P. Heinz_, Jan 18 2013"
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 0, 0, SeriesCoefficient[ Product[ (1 - x^(6 k - 3))^2 (1 - x^(6 k)), {k, Ceiling[ n/6]}] / Product[ 1 - x^k, {k, n}], {x, 0, n}]]; (* _Michael Somos_, Jun 02 2011 *)",
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 4, 0, x^3] / QPochhammer[ x], {x, 0, n}]; (* _Michael Somos_, Dec 03 2013 *)",
				"b[n_, i_] := b[n, i] = If[n == 0, 1, If[i\u003c1, 0, b[n, i-1] + Sum[b[n-i*j, i-2], {j, 1, Min[n/i, 2]}]]]; a[n_] := b[n, n]; Table[a[n], {n, 0, 60}] (* _Jean-François Alcover_, Mar 04 2015, after _Alois P. Heinz_ *)",
				"nmax = 100; CoefficientList[Series[Product[1 / ( (1-x^(3*k-2)) * (1-x^(3*k-1)) * (1 + x^(3*k)) ), {k, 1, nmax}], {x, 0, nmax}], x] (* _Vaclav Kotesovec_, Aug 30 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^3 + A)^2 / (eta(x + A) * eta(x^6 + A)), n))}; /* _Michael Somos_, Jun 02 2011 */",
				"(Haskell)",
				"a070047 n = p 1 n where",
				"   p k m | m == 0 = 1 | m \u003c k = 0 | otherwise = q k (m-k) + p (k+1) m",
				"   q k m | m == 0 = 1 | m \u003c k = 0 | otherwise = p (k+2) (m-k) + p (k+2) m",
				"-- _Reinhard Zumkeller_, Nov 12 2011"
			],
			"xref": [
				"Cf. A233006, A000700, A108961, A108962, A271661, A280937, A280938."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_, May 09 2002",
			"ext": [
				"Additional comments from _Michael Somos_, Dec 04 2002"
			],
			"references": 9,
			"revision": 54,
			"time": "2021-03-12T22:24:42-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}