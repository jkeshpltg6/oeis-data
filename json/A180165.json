{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A180165",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 180165,
			"data": "1,1,2,1,3,3,1,4,8,5,1,5,15,22,8,1,6,24,57,60,13,1,7,35,116,216,164,21,1,8,48,205,560,819,448,34,1,9,63,330,1200,2704,3105,1224,55,1,10,80,497,2268,7025,13056,11772,3344,89,1,11,99,712,3920,15588,41125,63040,44631,9136,144",
			"name": "Triangle read by rows, derived from an array of sequences generated from (1 + x)/ (1 - r*x - r*x^2).",
			"comment": [
				"Row sums = A180166: (1, 3, 7, 18, 51, 161, 560, 2163, ...).",
				"Rows of the array, with other offsets: (row 1 = A000045 starting with offset 2: (1, 2, 3, 5, 8, 13, ...); and for rows \u003e 1, the entries: A028859, A125145, A086347, and A180033 start with offset 0; with the offset in the present array = 1."
			],
			"link": [
				"Robert P. P. McKone, \u003ca href=\"/A180165/b180165.txt\"\u003eAntidiagonals n = 0..199, flattened\u003c/a\u003e"
			],
			"formula": [
				"Triangle read by rows, generated from an array of sequences generated from (1 + x)/(1 - r*x - r*x^2); r \u003e 0.",
				"Alternatively, given the array with offset 1, the sequence r-th sequence is generated from a(k) = r*a(k-1) + r*(k-2); a(1) = 1, a(2) = r+1.",
				"With a matrix method, the array can be generated from a 2 X 2 matrix of the form [0,1; r,r] = M, such that M^n * [1,r+1] = [r,n+1; r,n+2].",
				"Also, for r \u003e 1, the (r+1)-th row of the array is the INVERT transform of the r-th row."
			],
			"example": [
				"First few rows of the triangle:",
				"  1;",
				"  1, 2;",
				"  1, 3, 3;",
				"  1, 4, 8, 5;",
				"  1, 5, 15, 22, 8;",
				"  1, 6, 24, 57, 60, 13;",
				"  1, 7, 35, 116, 216, 164, 21;",
				"  1, 8, 48, 205, 560, 819, 448, 34;",
				"  1, 9, 63, 330, 1200, 2704, 3105, 1224, 55;",
				"  1, 10, 80, 497, 2268, 7025, 13056, 11772, 3344, 89;",
				"  1, 11, 99, 712, 3920, 15588, 41125, 63040, 44631, 9136, 144;",
				"  1, 12, 120, 981, 6336, 30919, 107136, 240750, 304384, 169209, 24960, 233;",
				"  ...",
				"As an array A(r,k) by upwards antidiagonals:",
				"        k=1  k=2  k=3   k=4    k=5",
				"  r=1:   1,   2,    3,    5,     8, ...",
				"  r=2:   1,   3,    8,   22,    60, ...",
				"  r=3:   1,   4,   15,   57,   216, ...",
				"  r=4:   1,   5,   24,  116,   560, ...",
				"  r=5:   1,   6,   35,  205,  1200, ...",
				"Row r=5 = A180033 = (1, 6, 35, 205,...) and is generated from (1+x)/(1-5*x-5*x^2); is the INVERT transform of row r=4; and the array term A(5,4) = 205 = 5*35 + 5*6.",
				"Terms A(2,4) and A(2,5) = [22,60] = [0,1; 2,2]^3 * [1,3]."
			],
			"mathematica": [
				"A180165[a_] := Reverse[Table[Table[CoefficientList[Series[(1 + x)/(1 - r*x - r*x^2), {x, 0, a - 2}], x], {r, 1, a + 1}][[k, n - k]], {n, 1, a}, {k, 1, n - 1}], 2] // Flatten;",
				"A180165[12] (* _Robert P. P. McKone_, Jan 19 2021 *)"
			],
			"xref": [
				"Cf. A180166, A000045, A028859, A125145, A086347, A180033.",
				"Cf. A340156."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Gary W. Adamson_, Aug 14 2010",
			"ext": [
				"a(35) corrected by _Robert P. P. McKone_, Dec 31 2020"
			],
			"references": 5,
			"revision": 45,
			"time": "2021-02-01T00:14:34-05:00",
			"created": "2010-08-27T03:00:00-04:00"
		}
	]
}