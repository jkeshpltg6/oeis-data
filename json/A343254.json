{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A343254",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 343254,
			"data": "1,1,2,1,5,2,15,5,3,52,15,8,203,52,25,16,877,203,89,53,4140,877,354,197,131,21147,4140,1551,810,512,115975,21147,7403,3643,2193,1496,678570,115975,38154,17759,10201,6697,4213597,678570,210803,93130,51146,32345,22482",
			"name": "Triangle read by rows: T(n,k) is the number of 2-balanced partitions of a set of n elements in which the first and the second subsets have cardinality k, for n \u003e= 0, k = 0..floor(n/2).",
			"comment": [
				"A 2-balanced partition is a partition of a set which is the union of three subsets, with the property that the cardinality of the first two subsets are equal (possibly zero), and each block contains the same number (possibly zero) of elements from the first and from the second subset. The rows add to A344775.",
				"T(n,0) are the Bell numbers. T(2k,k) are the numbers of 2-balanced partitions in the particular case in which the third set is empty. T(2k,k) are the generalized Bell numbers given in A023998."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A343254/b343254.txt\"\u003eRows n = 0..200, flattened\u003c/a\u003e",
				"Francesca Aicardi, \u003ca href=\"https://www.researchgate.net/publication/352438417_BALANCED_PARTITIONS\"\u003eBalanced partitions\u003c/a\u003e, preprint on researchgate, 2021.",
				"Francesca Aicardi, Diego Arcis, and Jesús Juyumaya, \u003ca href=\"https://arxiv.org/abs/2107.04170\"\u003eBrauer and Jones tied monoids\u003c/a\u003e, arXiv:2107.04170 [math.RT], 2021."
			],
			"formula": [
				"T(n,k) = Sum_{j=1..n-k} C(n,k,j). C(n,k,j) is defined for n\u003e=2k, j\u003c=n-k, and obtained by the recursion: C(n,k,j) = C(n-1,k,j-1) + j*C(n-1,k,j), with initial conditions C(2k,k,j) = triangle A061691(k,j) of generalized Stirling numbers."
			],
			"example": [
				"T(4,1) = 5, number of 2-balanced partitions of a set A of 4 elements with 1 element in the first subset and 1 element in the second subset: A={a} U {b} U {c,d}. The five partitions are: ((a,b),(c),(d)), ((a,b),(c,d)), ((a,b,c),(d)), ((a,b,d),(c)), ((a,b,c,d)). Note that if a block contains a, then it must contain b. Thus, T(n,1) = T(n-1,0).",
				"Triangle T(n,k) begins:",
				"        1;",
				"        1;",
				"        2,      1;",
				"        5,      2;",
				"       15,      5,      3;",
				"       52,     15,      8;",
				"      203,     52,     25,    16;",
				"      877,    203,     89,    53;",
				"     4140,    877,    354,   197,   131;",
				"    21147,   4140,   1551,   810,   512;",
				"   115975,  21147,   7403,  3643,  2193,  1496;",
				"   678570, 115975,  38154, 17759, 10201,  6697;",
				"  4213597, 678570, 210803, 93130, 51146, 32345, 22482;",
				"  ..."
			],
			"xref": [
				"Cf. A000110 (Bell numbers), A023998, A061691 (generalized Stirling numbers), A344775 (row sums)."
			],
			"keyword": "nonn,tabf",
			"offset": "0,3",
			"author": "_Francesca Aicardi_, Jun 04 2021",
			"references": 2,
			"revision": 36,
			"time": "2021-11-11T20:45:35-05:00",
			"created": "2021-06-22T12:43:25-04:00"
		}
	]
}