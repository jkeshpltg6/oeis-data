{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A292612",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 292612,
			"data": "4,-3,5,0,13,21,68,165,445,1152,3029,7917,20740,54285,142133,372096,974173,2550405,6677060,17480757,45765229,119814912,313679525,821223645,2149991428,5628750621,14736260453,38580030720,101003831725,264431464437,692290561604,1812440220357",
			"name": "a(n) = F(n)^2 + 4*(-1)^n = F(n+3)*F(n-3), where F = A000045.",
			"comment": [
				"This is the case k=3 of the identity F(n)^2 - F(k)^2*(-1)^(n+k) = F(n+k)*F(n-k), known also as Catalan's identity."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A292612/b292612.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CatalansIdentity.html\"\u003eCatalan's Identity\u003c/a\u003e.",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,2,-1)."
			],
			"formula": [
				"G.f.: (4 - 11*x + 3*x^2)/((1 + x)*(1 - 3*x + x^2)).",
				"a(n) = a(-n) = 2*a(n-1) + 2*a(n-2) - a(n-3).",
				"a(n) = 4*A001654(n+1) - 11*A001654(n) + 3*A001654(n-1) with A001654(-1)=0.",
				"5*a(n) = Lucas(2*n) + 18*(-1)^n. Note that Lucas(2*n) + r*(-1)^n is divisible by 5 for r = -2, 3, -7, 8, -12, 13, -17, 18, -22, 23, -27, ... = (-1/4)*(3 + 5*(2*m+1)*(-1)^m) = (-1)^m*A047221(m). On the other hand, a(n) is divisible by 5 when n is a member of A047221.",
				"a(n) = (1/5)*(18*(-1)^n + ((3-sqrt(5))/2)^n + ((3+sqrt(5))/2)^n). - _Colin Barker_, Sep 20 2017",
				"Sum_{n\u003e=4} 1/a(n) = 143/960. - _Amiram Eldar_, Oct 05 2020",
				"Sum_{n\u003e=4} (-1)^n/a(n) = 3/(4*phi) - 407/960, where phi is the golden ratio (A001622). - _Amiram Eldar_, Oct 06 2020"
			],
			"maple": [
				"with(combinat,fibonacci):  A292612:=seq(fibonacci(n)^2+4*(-1)^n, n=0..10^2); # _Muniru A Asiru_, Sep 26 2017"
			],
			"mathematica": [
				"Table[Fibonacci[n]^2 + 4 (-1)^n, {n, 0, 40}]"
			],
			"program": [
				"(PARI) for(n=0, 40, print1(fibonacci(n)^2+4*(-1)^n\", \"));",
				"(PARI) Vec((4-11*x+3*x^2)/((1+x)*(1-3*x+x^2))+O(x^30)) \\\\ _Colin Barker_, Sep 20 2017",
				"(Sage) [fibonacci(n)^2+4*(-1)^n for n in range(40)]",
				"(MAGMA) [Fibonacci(n)^2+4*(-1)^n: n in [0..40]];",
				"(GAP)",
				"List([0..10^2],n -\u003eFibonacci(n)^2+4*(-1)^n); # _Muniru A Asiru_, Sep 26 2017"
			],
			"xref": [
				"Cf. A000045, A001622, A005248: Lucas(2*n), A001654: F(n)*F(n+1).",
				"Cf. A007598 (k=0), A059929 (k=1, without initial 1), A192883 (k=2, without initial -1), this sequence (k=3)."
			],
			"keyword": "sign,easy",
			"offset": "0,1",
			"author": "_Bruno Berselli_, Sep 20 2017",
			"references": 3,
			"revision": 43,
			"time": "2020-10-06T04:01:09-04:00",
			"created": "2017-09-20T10:45:36-04:00"
		}
	]
}