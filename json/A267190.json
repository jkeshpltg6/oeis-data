{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A267190",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 267190,
			"data": "0,1,5,9,13,25,29,41,53,65,85,97,117,145,149,161,173,193,221,241,277,313,357,401,437,489,541,553,581,609,645,689,733,801,869,945,1021,1081,1149,1217,1277,1345,1397,1433,1501,1569,1653,1753,1829,1905,1997,2057,2141,2225,2317,2449,2549,2681,2797,2889,2965,3041,3149,3289",
			"name": "Number of ON cells after n generations of the cellular automaton on the square grid that is described in the Comments.",
			"comment": [
				"The cells are the squares of the standard square grid.",
				"Cells are either OFF or ON, once they are ON they stay ON, and we begin in generation 1 with 1 ON cell.",
				"Each cell has 4 neighbors, those that it shares an edge with. Cells that are ON at generation n all try simultaneously to turn ON all their neighbors that are OFF. They can only do this at this point in time; afterwards they go to sleep (but stay ON).",
				"A square Q is turned ON at generation n+1 if:",
				"a) Q shares an edge with one and only one square P (say) that was turned ON at generation n (in which case the two squares which intersect Q only in a vertex not on that edge are called Q's “outer squares”), and",
				"b) Q's outer squares were not turned ON in any previous generation, and",
				"c) Q's outer squares are not prospective squares of the (n+1)st generation satisfying a).",
				"A151895, A151906, and A170896 are closely related cellular automata.",
				"The key difference between this and A170896 is that if we have two squares Q1 and Q2, both satisfying a), and that are each an outer square of the other, where Q1 satisfies b), but Q2 does not, then for A170896 Q1 is accepted, but for this sequence Q1 is eliminated.  This first happens at n=14, when, for example, A170896 turns (8,3) ON but A267190 doesn't (because (9,2) fails to satisfy b) because (8,1) is ON). - _David Applegate_, Jan 30 2016",
				"A151895 and A267190 first differ at n=17, when A267190 turns (12,2) ON even though its outer square (11,1) was considered (not turned ON) in a previous generation. - _David Applegate_, Jan 30 2016"
			],
			"reference": [
				"D. Applegate, Omar E. Pol and N. J. A. Sloane, The Toothpick Sequence and Other Sequences from Cellular Automata, Congressus Numerantium, Vol. 206 (2010), 157-191"
			],
			"link": [
				"David Applegate, \u003ca href=\"/A267190/b267190.txt\"\u003eTable of n, a(n) for n = 0..260\u003c/a\u003e",
				"David Applegate, \u003ca href=\"/A139250/a139250.anim.html\"\u003eThe movie version\u003c/a\u003e",
				"David Applegate, \u003ca href=\"/A267190/a267190_2.pdf\"\u003eAfter 16 generations, illustrating a(16)=173 (with the A267191(16)=12 newly created cells shown in blue)\u003c/a\u003e",
				"David Applegate, \u003ca href=\"/A267190/a267190_3.pdf\"\u003eAfter 36 generations, illustrating a(36)=1021 (with the A267191(36)=76 newly created cells shown in blue)\u003c/a\u003e David Applegate, Omar E. Pol and N. J. A. Sloane, \u003ca href=\"/A000695/a000695_1.pdf\"\u003eThe Toothpick Sequence and Other Sequences from Cellular Automata\u003c/a\u003e, Congressus Numerantium, Vol. 206 (2010), 157-191. [There is a typo in Theorem 6: (13) should read u(n) = 4.3^(wt(n-1)-1) for n \u003e= 2.], which is also available at \u003ca href=\"http://arxiv.org/abs/1004.3036\"\u003earXiv:1004.3036v2\u003c/a\u003e",
				"S. M. Ulam, \u003ca href=\"/A002858/a002858.pdf\"\u003eOn some mathematical problems connected with patterns of growth of figures\u003c/a\u003e, pp. 215-224 of R. E. Bellman, ed., Mathematical Problems in the Biological Sciences, Proc. Sympos. Applied Math., Vol. 14, Amer. Math. Soc., 1962 [Annotated scanned copy]",
				"\u003ca href=\"/index/To#toothpick\"\u003eIndex entries for sequences related to toothpick sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e"
			],
			"formula": [
				"We do not know of a recurrence or generating function."
			],
			"xref": [
				"Cf. A267191 (first differences), A151895, A151906, A170896.",
				"See also A139250."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_David Applegate_ and _N. J. A. Sloane_, Jan 21 2016",
			"ext": [
				"Corrected by _David Applegate_, Jan 30 2016"
			],
			"references": 5,
			"revision": 51,
			"time": "2021-02-24T02:48:19-05:00",
			"created": "2016-01-21T12:14:16-05:00"
		}
	]
}