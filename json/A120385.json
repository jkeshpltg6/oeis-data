{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A120385",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 120385,
			"data": "1,2,1,3,1,4,2,1,5,2,1,6,3,1,7,3,1,8,4,2,1,9,4,2,1,10,5,2,1,11,5,2,1,12,6,3,1,13,6,3,1,14,7,3,1,15,7,3,1,16,8,4,2,1,17,8,4,2,1,18,9,4,2,1,19,9,4,2,1,20,10,5,2,1,21,10,5,2,1,22,11,5,2,1,23,11,5,2,1,24,12,6,3,1,25",
			"name": "If a(n-1) = 1 then largest value so far + 1, otherwise floor(a(n-1)/2); or table T(n,k) with T(n,0) = n, T(n,k+1) = floor(T(n,k)/2).",
			"comment": [
				"Although not strictly a fractal sequence as defined in the Kimberling link, this sequence has many fractal properties. If the first instance of each value is removed, the result is the original sequence with each row repeated twice. Removing all odd-indexed instances of each value does give the original sequence."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A120385/b120385.txt\"\u003eRows n = 1..2048, flattened\u003c/a\u003e",
				"C. Kimberling, \u003ca href=\"http://faculty.evansville.edu/ck6/integer/fractals.html\"\u003eFractal sequences\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = floor(n/2^(k-1)).",
				"From _Peter Bala_, Feb 02 2013: (Start)",
				"The n-th row polynomial R(n,t) = Sum_{k\u003e=0} t^k*floor(n/2^k) and satisfies the recurrence equation R(n,t) = t*R(floor(n/2),t) + n, with R(1,t) = 1.",
				"O.g.f. Sum_{n\u003e=1} R(n,t)*x^n = 1/(1-x)*Sum_{n\u003e=0} t^n*x^(2^n)/(1 - x^(2^n)).",
				"Product_{n\u003e=1} ( 1 + x^((t^n - 2^n)/(t-2)) ) = 1 + Sum_{n\u003e=1} x^R(n,t) = 1 + x + x^(2 + t) + x^(3 + t) + x^(4 + 2*t + t^2) + .... For related sequences see A050292 (t = -1), A001477(t = 0), A005187 (t = 1) and A080277 (t = 2).",
				"(End)"
			],
			"example": [
				"The table starts:",
				"  1;",
				"  2, 1;",
				"  3, 1;",
				"  4, 2, 1;",
				"  5, 2, 1;",
				"  6, 3, 1;",
				"  7, 3, 1;",
				"  8, 4, 2, 1;"
			],
			"maple": [
				"T:= proc(n) T(n):= `if`(n=1, 1, [n, T(iquo(n, 2))][]) end:",
				"seq(T(n), n=1..30);  # _Alois P. Heinz_, Feb 12 2019"
			],
			"mathematica": [
				"Flatten[Function[n,NestWhile[Append[#, Floor[Last[#]/2]] \u0026, {n}, Last[#] != 1 \u0026]][#] \u0026 /@ Range[50]] (* _Birkas Gyorgy_, Apr 14 2011 *)"
			],
			"xref": [
				"Cf. A029837 (row lengths), A083652 (position of first n).",
				"Cf. A005187 (row sums). A001477, A050292, A080277.",
				"Cf. A000027, A004526, A002265, A132292."
			],
			"keyword": "nonn,tabf",
			"offset": "1,2",
			"author": "_Franklin T. Adams-Watters_, Jun 29 2006",
			"references": 6,
			"revision": 25,
			"time": "2019-12-11T23:43:35-05:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}