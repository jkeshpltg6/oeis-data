{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A281352",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 281352,
			"data": "1,6,12,14,30,48,36,48,84,86,48,96,86,96,96,144,126,192,108,96,192,240,96,288,252,150,144,158,192,432,240,144,372,288,96,384,446,192,288,480,336,384,288,288,528,432,192,480,374,294,300,576,384,720,324,384",
			"name": "Number of ways to write a nonnegative rational integer n as a sum of three squares in the ring of integers of Q(sqrt(3)).",
			"comment": [
				"a(n) is the number of solutions to the equation n = x^2 + y^2 + z^2 with x, y, z in the ring of integers Z[sqrt(3)] of Q(sqrt(3)).",
				"This is the same as solving the system of equations",
				"  n = (a^2 + b^2 + c^2) + 3*(d^2 + e^2 + f^2)",
				"  ad + be + cf = 0",
				"in rational integers.",
				"According to Cohn (1961), the class number of Q(sqrt(3), sqrt{-n}) always divides a(n).",
				"Let O=Z[sqrt(3)] denote the ring of integers of Q(sqrt(3)). Note that the equation 7=x^2+y^2+z^2 has no solutions in integers, but has 48 solutions in O. For example, 7=1^2+(sqrt(3))^2+(sqrt(3))^2.",
				"Let theta_3(q)=1+2q+2q^4+... be the 3rd Jacobi theta function. It is widely known that theta_3(q)^3 is the generating function for the number of rational integer solutions r_3(n) to n=x^2+y^2+z^2.",
				"Is there a generating function for a(n)?",
				"For which n is it true that r_3(n) divides a(n)?"
			],
			"link": [
				"Anton Mosunov, \u003ca href=\"/A281352/b281352.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"H. Cohn, \u003ca href=\"http://www.jstor.org/stable/2372719\"\u003eCalculation of class numbers by decomposition into 3 integral squares in the fields of 2^{1/2} and 3^{1/2}\u003c/a\u003e, American Journal of Mathematics 83 (1), pp. 33-56, 1961.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Ring_of_integers\"\u003eRing of integers\u003c/a\u003e."
			],
			"example": [
				"a(0)=1, because the equation 0 = x^2 + y^2 + z^2 has a single solution (x,y,z)=(0,0,0);",
				"a(1)=6, because the only solutions are (x,y,z)=(+-1,0,0),(0,+-1,0),(0,0,+-1);",
				"a(2)=12, because the only solutions are (x,y,z)=(+-1,+-1,0),(0,+-1,+-1),(+-1,0,+-1);",
				"a(3)=14, because the only solutions are (x,y,z)=(+-1,+-1,+-1),(+-sqrt(3),0,0),(0,+-sqrt(3),0),(0,0,+-sqrt(3));",
				"a(4)=30, etc."
			],
			"xref": [
				"Cf. A005875 (similar in Z), A280802 (similar in Z[sqrt(2)])."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Anton Mosunov_, Jan 22 2017",
			"references": 2,
			"revision": 35,
			"time": "2020-04-10T02:14:04-04:00",
			"created": "2017-01-22T15:25:40-05:00"
		}
	]
}