{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A096636",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 96636,
			"data": "5,7,19,79,331,751,1171,7459,10651,18379,90931,78439,399499,644869,2631511,1427911,4355311,5715319,49196359,43030381,163384621,249623581,452980999,1272463669,505313251",
			"name": "Smallest prime p \u003e prime(n+2) such that p is a quadratic residue mod the first n odd primes 3, 5, 7, 11, ..., prime(n+1), and p is a quadratic non-residue mod prime(n+2).",
			"comment": [
				"Same as smallest prime p with property that the Legendre symbol (p|q) = 1 for the first n odd primes q = prime(k+1), k = 1, 2, ..., n, and (p|q) = -1 for q = prime(n+2). - _T. D. Noe_, Mar 06 2013"
			],
			"example": [
				"Let f(p) = list of Legendre(p|q) for q = 3,5,7,11,13,...",
				"Then f(3), f(5), f(7), f(11), ... are:",
				"p=3: 0, -1, -1, 1, 1, -1, -1, 1, -1, -1, 1, -1, -1, 1, -1, 1, 1, -1, 1, 1, -1, 1, -1, ...",
				"p=5: -1, 0, -1, 1, -1, -1, 1, -1, 1, 1, -1, 1, -1, -1, -1, 1, 1, -1, 1, -1, 1, -1, 1, ...",
				"p=7: 1, -1, 0, -1, -1, -1, 1, -1, 1, 1, 1, -1, -1, 1, 1, 1, -1, -1, -1, -1, -1, 1, -1, ...",
				"p=11: -1, 1, 1, 0, -1, -1, 1, -1, -1, -1, 1, -1, 1, -1, 1, -1, -1, -1, -1, -1, 1, 1, 1, ...",
				"p=13: 1, -1, -1, -1, 0, 1, -1, 1, 1, -1, -1, -1, 1, -1, 1, -1, 1, -1, -1, -1, 1, -1, -1, ...",
				"p=17: -1, -1, -1, -1, 1, 0, 1, -1, -1, -1, -1, -1, 1, 1, 1, 1, -1, 1, -1, -1, -1, 1, 1, ...",
				"p=19: 1, 1, -1, -1, -1, 1, 0, -1, -1, 1, -1, -1, -1, -1, -1, 1, 1, 1, 1, 1, 1, -1, -1, ...",
				"p=5 is the first list that begins with -1, so a(0) = 5,",
				"p=7 is the first list that begins 1, -1, so a(1) = 7,",
				"p=19 is the first list that begins 1, 1, -1, so a(2) = 19."
			],
			"mathematica": [
				"f[n_] := Block[{k = 2}, While[ JacobiSymbol[n, Prime[k]] == 1, k++ ]; Prime[k]]; t = Table[0, {50}]; Do[p = Prime[n]; a = f[p]; If[ t[[ PrimePi[a]]] == 0, t[[ PrimePi[a]]] = p; Print[ PrimePi[a], \" = \", p]], {n, 10^9}]"
			],
			"xref": [
				"Cf. A094929, A222756 (p and q switched).",
				"See also A096637, A096638, A096639, A096640. - _Jonathan Sondow_, Mar 07 2013"
			],
			"keyword": "nonn",
			"offset": "0,1",
			"author": "_Robert G. Wilson v_, Jun 24 2004",
			"ext": [
				"Better definition from _T. D. Noe_, Mar 06 2013",
				"Entry revised by _N. J. A. Sloane_, Mar 06 2013",
				"Simpler definition from _Jonathan Sondow_, Mar 06 2013"
			],
			"references": 7,
			"revision": 36,
			"time": "2013-03-07T20:33:16-05:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}