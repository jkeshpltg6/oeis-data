{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A319087",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 319087,
			"data": "1,5,23,55,155,227,521,777,1263,1663,2873,3449,5477,6653,8453,10501,15125,17069,23567,26767,32059,36899,48537,53145,65645,73757,86879,96287,119835,127035,155865,172249,194029,212525,241925,257477,306761,332753,369257",
			"name": "a(n) = Sum_{k=1..n} k^2*phi(k), where phi is the Euler totient function A000010.",
			"comment": [
				"Comment from _N. J. A. Sloane_, Mar 22 2020 (Start)",
				"Theorem: Sum_{ 1\u003c=i\u003c=n, 1\u003c=j\u003c=n, gcd(i,j)=1 } i*j = a(n).",
				"Proof: From the Apostol reference we know that:",
				"Sum_{ 1\u003c=i\u003c=n, gcd(i,n)=1 } i = n*phi(n)/2 (see A023896).",
				"We use induction on n. The result is true for n=1.",
				"Then a(n) - a(n-1) = 2*Sum_{ i=1..n-1, gcd(i,n)=1 } n*i = n^2*phi(n). QED (End)"
			],
			"reference": [
				"T. M. Apostol, Introduction to Analytic Number Theory, Springer-Verlag, 1976, page 48, problem 16, the function phi_1(n)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A319087/b319087.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) ~ 3*n^4 / (2*Pi^2)."
			],
			"mathematica": [
				"Accumulate[Table[k^2*EulerPhi[k], {k, 1, 50}]]"
			],
			"program": [
				"(PARI) a(n) = sum(k=1, n, k^2*eulerphi(k)); \\\\ _Michel Marcus_, Sep 12 2018"
			],
			"xref": [
				"Cf. A000010, A002088, A011755, A023896, A053191."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Vaclav Kotesovec_, Sep 10 2018",
			"references": 8,
			"revision": 16,
			"time": "2020-03-22T21:15:57-04:00",
			"created": "2018-09-10T09:19:19-04:00"
		}
	]
}