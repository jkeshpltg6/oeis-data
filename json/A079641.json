{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A079641",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 79641,
			"data": "1,2,1,6,6,1,26,36,12,1,150,250,120,20,1,1082,2040,1230,300,30,1,9366,19334,13650,4270,630,42,1,94586,209580,166376,62160,11900,1176,56,1,1091670,2562354,2229444,952728,220500,28476,2016,72,1,14174522",
			"name": "Matrix product of Stirling2-triangle A008277(n,k) and unsigned Stirling1-triangle |A008275(n,k)|.",
			"comment": [
				"Triangle T(n,k), 1\u003c=k\u003c=n, read by rows, given by (0, 2, 1, 4, 2, 6, 3, 8, 4, 10, 5, ...) DELTA (1, 0, 1, 0, 1, 0, 1, 0, 1, 0, ...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, Dec 22 2011",
				"Subtriangle of triangle in A129062. - _Philippe Deléham_, Feb 17 2013",
				"Also the Bell transform of A000629. For the definition of the Bell transform see A264428. - _Peter Luschny_, Jan 26 2016"
			],
			"link": [
				"Nick Early, \u003ca href=\"https://arxiv.org/abs/1712.08520\"\u003eCanonical Bases for Permutohedral Plates\u003c/a\u003e, arXiv:1712.08520 [math.CO], 2017.",
				"Nick Early, \u003ca href=\"https://arxiv.org/abs/1810.03246\"\u003eHoneycomb tessellations and canonical bases for permutohedral blades\u003c/a\u003e, arXiv:1810.03246 [math.CO], 2018.",
				"D. E. Knuth, \u003ca href=\"http://arxiv.org/abs/math/9207221\"\u003eConvolution polynomials\u003c/a\u003e, arXiv:math/9207221 [math.CA], 1992; The Mathematica J., 2 (1992), 67-78."
			],
			"formula": [
				"T(n, k) = Sum_{i=k..n} A008277(n, i) * |A008275(i, k)|.",
				"E.g.f.: (2-exp(x))^(-y). - _Vladeta Jovovic_, Nov 22 2003",
				"From _Peter Bala_, Sep 12 2011: (Start)",
				"The row generating polynomials R(n,x) begin R(1,x) = x, R(2,x) = 2*x+x^2, R(3,x) = 6*x+6*x^2+x^3 and satisfy the recurrence R(n+1,x) = x*(2*R(n,x+1) - R(n,x)). They form a sequence of binomial type polynomials. In particular, denoting R(n,x) by x^[n] to emphasize the analogies with the monomial polynomials x^n, we have the binomial expansion (x+y)^[n] = sum {k = 0..n} binomial(n,k)*x^[n-k]*y^[k].",
				"There is a Dobinski-type formula exp(-x)*sum {k = 0..inf} (-k)^[n]*x^k/k! = Bell(n,-x). The alternating n-th row entries (-1)^k*T(n,k) are the connection coefficients expressing the polynomial Bell(n,-x) as a linear combination of Bell(k,x), 1\u003c=k\u003c=n. For example, the list of coefficients of R(4,x) is [26,36,12,1] and we have Bell(4,-x) = -26*Bell(1,x) + 36*Bell(2,x) - 12*Bell(3,x) + Bell(4,x).",
				"The row polynomials also satisfy an analog of the Bernoulli's summation formula for powers of integers sum {k = 1..n} k^[p] = 1/(p+1) sum {k = 0..p} binomial(p+1,k) * B_k * n^[p+1-k], where B_k denotes the Bernoulli numbers. Compare with A195204 and A195205.",
				"(End)",
				"Let D be the forward difference operator D(f(x)) = f(x+1) - f(x). Then the n-th row polynomial R(n,x) = 1/f(x)*(x*D)^n(f(x)) with f(x) = 2^x. Cf. A209849. Also cf. A008277 where the row polynomials are given by 1/f(x)*(x*d/dx)^n(f(x))), where now f(x) = exp(x). - _Peter Bala_, Mar 16 2012"
			],
			"example": [
				"Triangle begins:",
				"  1;",
				"  2,1;",
				"  6,6,1;",
				"  26,36,12,1;",
				"  150,250,120,20,1;",
				"  1082,2040,1230,300,30,1;",
				"  ...",
				"Triangle (0,2,1,4,2,6,3,8,4,...) DELTA (1,0,1,0,1,0,1,0,1,...) begins:",
				"  1",
				"  0, 1",
				"  0, 2, 1",
				"  0, 6, 6, 1",
				"  0, 26, 36, 12, 1",
				"  0, 150, 250, 120, 20, 1",
				"  0, 1082, 2040, 1230, 300, 30, 1. - _Philippe Deléham_, Dec 22 2011"
			],
			"maple": [
				"# The function BellMatrix is defined in A264428.",
				"# Adds (1, 0, 0, 0, ..) as column 0.",
				"BellMatrix(n -\u003e add((-1)^(n-k)*2^k*k!*combinat:-stirling2(n, k), k=0..n), 9); # _Peter Luschny_, Jan 26 2016"
			],
			"mathematica": [
				"rows = 10;",
				"t = Table[Sum[(-1)^(n-k)*2^k*k!*StirlingS2[n, k], {k,0,n}], {n, 0, rows}];",
				"T[n_, k_] := BellY[n, k, t];",
				"Table[T[n, k], {n, 1, rows}, {k, 1, n}] // Flatten (* _Jean-François Alcover_, Jun 22 2018 *)"
			],
			"xref": [
				"Cf. A000670 (row sums), A000629 (first column), A195204, A195205. A209849, A129062"
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Vladeta Jovovic_, Jan 30 2003",
			"references": 7,
			"revision": 45,
			"time": "2018-10-09T03:01:58-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}