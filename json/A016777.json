{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A016777",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 16777,
			"data": "1,4,7,10,13,16,19,22,25,28,31,34,37,40,43,46,49,52,55,58,61,64,67,70,73,76,79,82,85,88,91,94,97,100,103,106,109,112,115,118,121,124,127,130,133,136,139,142,145,148,151,154,157,160,163,166,169,172,175,178,181,184,187",
			"name": "a(n) = 3*n + 1.",
			"comment": [
				"Numbers k such that the concatenation of the first k natural numbers is not divisible by 3. E.g., 16 is in the sequence because we have 123456789101111213141516 == 1 (mod 3).",
				"Ignoring the first term, this sequence represents the number of bonds in a hydrocarbon: a(#of carbon atoms) = number of bonds. - Nathan Savir (thoobik(AT)yahoo.com), Jul 03 2003",
				"n such that Sum_{k=0..n} (binomial(n+k,n-k) mod 2) is even (cf. A007306). - _Benoit Cloitre_, May 09 2004",
				"Hilbert series for twisted cubic curve. - _Paul Barry_, Aug 11 2006",
				"If Y is a 3-subset of an n-set X then, for n \u003e= 3, a(n-3) is the number of 3-subsets of X having at least two elements in common with Y. - _Milan Janjic_, Nov 23 2007",
				"a(n) = A144390 (1, 9, 23, 43, 69, ...) - A045944 (0, 5, 16, 33, 56, ...). From successive spectra of hydrogen atom. - _Paul Curtz_, Oct 05 2008",
				"Number of monomials in the n-th power of polynomial x^3+x^2+x+1. - _Artur Jasinski_, Oct 06 2008",
				"A145389(a(n)) = 1. - _Reinhard Zumkeller_, Oct 10 2008",
				"Union of A035504, A165333 and A165336. - _Reinhard Zumkeller_, Sep 17 2009",
				"Hankel transform of A076025. - _Paul Barry_, Sep 23 2009",
				"From _Jaroslav Krizek_, May 28 2010: (Start)",
				"a(n) = numbers k such that the antiharmonic mean of the first k positive integers is an integer.",
				"A169609(a(n-1)) = 1. See A146535 and A169609. Complement of A007494.",
				"See A005408 (odd positive integers) for corresponding values A146535(a(n)). (End)",
				"Apart from the initial term, A180080 is a subsequence; cf. A180076. - _Reinhard Zumkeller_, Aug 14 2010",
				"Also the maximum number of triangles that n + 2 noncoplanar points can determine in 3D space. - _Carmine Suriano_, Oct 08 2010",
				"A089911(4*a(n)) = 3. - _Reinhard Zumkeller_, Jul 05 2013",
				"The number of partitions of 6*n into at most 2 parts. - _Colin Barker_, Mar 31 2015",
				"For n \u003e= 1, a(n)/2 is the proportion of oxygen for the stoichiometric combustion reaction of hydrocarbon CnH2n+2, e.g., one part propane (C3H8) requires 5 parts oxygen to complete its combustion. - _Kival Ngaokrajang_, Jul 21 2015",
				"Exponents n \u003e 0 for which 1 + x^2 + x^n is reducible. - _Ron Knott_, Oct 13 2016",
				"Also the number of independent vertex sets in the n-cocktail party graph. - _Eric W. Weisstein_, Sep 21 2017",
				"Also the number of (not necessarily maximum) cliques in the n-ladder rung graph. - _Eric W. Weisstein_, Nov 29 2017",
				"Also the number of maximal and maximum cliques in the n-book graph. - _Eric W. Weisstein_, Dec 01 2017",
				"For n\u003e=1, a(n) is the size of any snake-polyomino with n cells. - _Christian Barrientos_ and _Sarah Minion_, Feb 27 2018",
				"The sum of two distinct terms of this sequence is never a square. See Lagarias et al. p. 167. - _Michel Marcus_, May 20 2018",
				"It seems that, for any n \u003e= 1, there exists no positive integer z such that digit_sum(a(n)*z) = digit_sum(a(n)+z). - _Max Lacoma_, Sep 18 2019",
				"For n \u003e 2, a(n-2) is the number of distinct values of the magic constant in a normal magic triangle of order n (see formula 5 in Trotter). - _Stefano Spezia_, Feb 18 2021"
			],
			"reference": [
				"W. Decker, C. Lossen, Computing in Algebraic Geometry, Springer, 2006, p. 22",
				"L. B. W. Jolley, Summation of Series, Dover Publications, 2nd rev. ed., 1961, pp. 16, 38.",
				"Konrad Knopp, Theory and Application of Infinite Series, Dover, p. 269."
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/A016777/b016777.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Hacène Belbachir, Toufik Djellal, and Jean-Gabriel Luque, \u003ca href=\"https://arxiv.org/abs/1703.00323\"\u003eOn the self-convolution of generalized Fibonacci numbers\u003c/a\u003e, arXiv:1703.00323 [math.CO], 2017.",
				"L. Euler, \u003ca href=\"http://math.dartmouth.edu/~euler/pages/E243.html\"\u003eObservatio de summis divisorum\u003c/a\u003e p. 9.",
				"L. Euler, \u003ca href=\"https://arxiv.org/abs/math/0411587\"\u003eAn observation on the sums of divisors\u003c/a\u003e, arXiv:math/0411587 [math.HO], 2004-2009, see p. 9.",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"Konrad Knopp, \u003ca href=\"http://www.hti.umich.edu/cgi/t/text/text-idx?sid=b88432273f115fb346725f1a42422e19;c=umhistmath;idno=ACM1954.0001.001\"\u003eTheorie und Anwendung der unendlichen Reihen\u003c/a\u003e, Berlin, J. Springer, 1922. (Original German edition of \"Theory and Application of Infinite Series\")",
				"J. C. Lagarias, A. M. Odlyzko, and J. B. Shearer, \u003ca href=\"http://dx.doi.org/10.1016/0097-3165(82)90005-X\"\u003eOn the density of sequences of integers the sum of no two of which is a square. I. Arithmetic progressions\u003c/a\u003e, Journal of Combinatorial Theory. Series A, 33 (1982), pp. 167-185.",
				"T. Mansour, \u003ca href=\"https://arxiv.org/abs/math/9909019\"\u003ePermutations avoiding a set of patterns from S_3 and a pattern from S_4\u003c/a\u003e, arXiv:math/9909019 [math.CO], 1999.",
				"Luis Manuel Rivera, \u003ca href=\"http://arxiv.org/abs/1406.3081\"\u003eInteger sequences and k-commuting permutations\u003c/a\u003e, arXiv preprint arXiv:1406.3081 [math.CO], 2014.",
				"Terrel Trotter, \u003ca href=\"http://www.teherba.org/trottermath.net/simpleops/magictri.html\"\u003eNormal Magic Triangles of Order n\u003c/a\u003e, Journal of Recreational Mathematics Vol. 5, No. 1, 1972, pp. 28-32.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/BookGraph.html\"\u003eBook Graph\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Clique.html\"\u003eClique\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CocktailPartyGraph.html\"\u003eCocktail Party Graph\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/IndependentVertexSet.html\"\u003eIndependent Vertex Set\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/LadderRungGraph.html\"\u003eLadder Rung Graph\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/MaximalClique.html\"\u003eMaximal Clique\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/MaximumClique.html\"\u003eMaximum Clique\u003c/a\u003e",
				"Chengcheng Yang, \u003ca href=\"https://arxiv.org/abs/2011.15010\"\u003eA Problem of Erdös Concerning Lattice Cubes\u003c/a\u003e, arXiv:2011.15010 [math.CO], 2020. See Table p. 27.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,-1)."
			],
			"formula": [
				"G.f.: (1+2*x)/(1-x)^2.",
				"a(n) = 3 + a(n-1).",
				"Sum_{n\u003e=1} (-1)^n/a(n) = (1/3)*(Pi/sqrt(3) + log(2)). [Jolley, p. 16, (79)] - _Benoit Cloitre_, Apr 05 2002",
				"(1 + 4*x + 7*x^2 + 10*x^3 + ...) = (1 + 2*x + 3*x^2 + ...)/(1 - 2*x + 4*x^2 - 8*x^3 +  ...). - _Gary W. Adamson_, Jul 03 2003",
				"E.g.f.: exp(x)*(1+3*x). - _Paul Barry_, Jul 23 2003",
				"a(n) = 2*a(n-1) - a(n-2); a(0)=1, a(1)=4. - _Philippe Deléham_, Nov 03 2008",
				"a(n) = 6*n - a(n-1) - 1 (with a(0) = 1). - _Vincenzo Librandi_, Nov 20 2010",
				"Sum_{n\u003e=0} 1/a(n)^2 = A214550. - _R. J. Mathar_, Jul 21 2012",
				"E.g.f.: E(0), where E(k) = 1 + 3*x/(1 - 2*x/(2*x + 6*x*(k+1)/E(k+1) )); (continued fraction). - _Sergei N. Gladkovskii_, Jul 05 2013",
				"G.f.: 1 + 4*x/(G(0) - 4*x), where G(k) = 1 + 4*x + 3*k*(x+1) -  x*(3*k+1)*(3*k+7)/G(k+1); (cont. fraction). - _Sergei N. Gladkovskii_, Jul 05 2013",
				"a(n) = A238731(n+1,n) = (-1)^n*Sum_{k = 0..n} A238731(n,k)*(-5)^k. - _Philippe Deléham_, Mar 05 2014",
				"Sum_{i=0..n} (a(i)-i) = A000290(n+1). - _Ivan N. Ianakiev_, Sep 24 2014",
				"From _Wolfdieter Lang_, Mar 09 2018: (Start)",
				"a(n) = denominator(Sum_{k=0..n-1} 1/(a(k)*a(k+1)), with the numerator n = A001477(n), where the sum is set to 0 for n = 0. [Jolley, p. 38, (208)]",
				"G.f. for {n/(1 + 3*n)}_{n \u003e= 0} is (1/3)*(1-hypergeom([1, 1], [4/3], -x/(1-x)))/(1-x). (End)",
				"a(n) = -A016789(-1-n) for all n in Z. - _Michael Somos_, May 27 2019"
			],
			"example": [
				"G.f.  = 1 + 4*x + 7*x^2 + 10*x^3 + 13*x^4 + 16*x^5 + 19*x^6 + 22*x^7 + ... - _Michael Somos_, May 27 2019"
			],
			"mathematica": [
				"Range[1, 199, 3] (* _Vladimir Joseph Stephan Orlovsky_, May 26 2011 *)",
				"(* Start from _Eric W. Weisstein_, Sep 21 2017 *)",
				"3 Range[0, 20] + 1",
				"Table[3 n + 1, {n, 0, 20}]",
				"LinearRecurrence[{2, -1}, {1, 4}, 20]",
				"CoefficientList[Series[(1 + 2 x)/(-1 + x)^2, {x, 0, 20}], x]",
				"(* End *)"
			],
			"program": [
				"(MAGMA) [3*n+1 : n in [1..30]]; // Sergei Haller (sergei(AT)sergei-haller.de), Dec 21 2006",
				"(Haskell)",
				"a016777 = (+ 1) . (* 3)",
				"a016777_list = [1, 4 ..]  -- _Reinhard Zumkeller_, Feb 28 2013, Feb 10 2012",
				"(Maxima) A016777(n):=3*n+1$",
				"makelist(A016777(n),n,0,30); /* _Martin Ettl_, Oct 31 2012 */",
				"(PARI) a(n)=3*n+1 \\\\ _Charles R Greathouse IV_, Jul 28 2015"
			],
			"xref": [
				"A016789(n) = 1 + a(n).",
				"First differences of A000326.",
				"Cf. A000290, A016933, A017569, A058183.",
				"Row sums of A131033.",
				"Complement of A007494. - _Reinhard Zumkeller_, Oct 10 2008",
				"Cf. A051536 (lcm).",
				"Cf. A007559 (partial products).",
				"Some subsequences: A002476 (primes), A291745 (nonprimes), A135556 (squares), A016779 (cubes)."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Dec 11 1996",
			"ext": [
				"Better description from _T. D. Noe_, Aug 15 2002",
				"Partially edited by _Joerg Arndt_, Mar 11 2010"
			],
			"references": 241,
			"revision": 264,
			"time": "2021-04-09T09:27:08-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}