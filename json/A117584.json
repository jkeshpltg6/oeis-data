{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A117584",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 117584,
			"data": "1,1,2,1,3,5,1,4,7,12,1,5,9,17,29,1,6,11,22,41,70,1,7,13,27,53,99,169,1,8,15,32,65,128,239,408,1,9,17,37,77,157,309,577,985,1,10,19,42,89,186,379,746,1393,2378",
			"name": "Generalized Pellian triangle.",
			"comment": [
				"Diagonals of the triangle are composed of the infinite set of Pellian sequences. Right border = A000129. Next diagonal going to the left = A001333 starting (1, 3, 7, 17, ...). A048654 = (1, 4, 9, ...). A048655 = (1, 5, 11, ...). A048693 = (1, 6, 13, ...); and so on."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A117584/b117584.txt\"\u003eAntidiagonal rows n = 0..100, flattened\u003c/a\u003e"
			],
			"formula": [
				"Antidiagonals of the generalized Pellian array. First row of the array = A000129: (1, 2, 5, 12, ...). n-th row of the array starts (1, n+1, ...); as a Pellian sequence.",
				"From _G. C. Greubel_, Jul 05 2021: (Start)",
				"T(n, k) = P(k) + (n-1)*P(k-1), where P(n) = A000129(n) (square array).",
				"Sum_{k=1..n} T(n-k+1, k) = A117185(n). (End)"
			],
			"example": [
				"First few rows of the triangle are:",
				"  1;",
				"  1, 2;",
				"  1, 3,  5;",
				"  1, 4,  7, 12;",
				"  1, 5,  9, 17, 29;",
				"  1, 6, 11, 22, 41, 70;",
				"  1, 7, 13, 27, 53, 99, 169;",
				"  ...",
				"The triangle rows are antidiagonals of the generalized Pellian array:",
				"  1, 2,  5, 12, 29, ...",
				"  1, 3,  7, 17, 41, ...",
				"  1, 4,  9, 22, 53, ...",
				"  1, 5, 11, 27, 65, ...",
				"  ...",
				"For example, in the row (1, 5, 11, 27, 65, ...), 65 = 2*27 + 11."
			],
			"mathematica": [
				"T[n_, k_]:= Fibonacci[k, 2] + (n-1)*Fibonacci[k-1, 2];",
				"Table[T[n-k+1, k], {n,12}, {k,n}]//Flatten (* _G. C. Greubel_, Jul 05 2021 *)"
			],
			"program": [
				"(MAGMA)",
				"P:= func\u003c n | Round( ((1+Sqrt(2))^n - (1-Sqrt(2))^n)/(2*Sqrt(2)) ) \u003e;",
				"T:= func\u003c n,k | P(k) + (n-1)*P(k-1) \u003e;",
				"[T(n-k+1, k): k in [1..n], n in [1..12]]; // _G. C. Greubel_, Jul 05 2021",
				"(Sage)",
				"def T(n,k): return lucas_number1(k,2,-1) + (n-1)*lucas_number1(k-1,2,-1)",
				"flatten([[T(n-k+1, k) for k in (1..n)] for n in (1..12)]) # _G. C. Greubel_, Jul 05 2021"
			],
			"xref": [
				"Cf. A000129, A001333, A048654, A048655, A048693, A117185."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Gary W. Adamson_, Mar 29 2006",
			"references": 3,
			"revision": 12,
			"time": "2021-07-06T01:53:05-04:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}