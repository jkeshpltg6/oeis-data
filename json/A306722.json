{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A306722",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 306722,
			"data": "1,1,2,1,1,2,1,1,1,3,0,3,1,1,1,1,0,3,0,3,1,1,0,3,1,1,4,3,0,3,0,1,4,0,1,3,1,0,0,3,0,3,0,1,4,0,1,3,0,1,0,1,0,2,1,2,0,2,0,5,0,1,4,0,1,4,1,0,0,4,0,6,1,1,4,0,0,5,0,4,1",
			"name": "Number of pairs of primes (p,q), p \u003c q, which are a solution of the Diophantine equation (p-1)*(q-1) = (2n)^2.",
			"comment": [
				"a(n) is also the number of semiprimes p*q whose totient is a square (A247129) and equal to (2*n)^2.",
				"From _Robert G. Wilson v_, Mar 30 2019, Mar 30 2019: (Start)",
				"First occurrence of k=1,2,3,...: 1, 3, 10, 27, 60, 72, 120, 180, 270, 480, 252, 1155, 720, 792, 1260, 630, ..., . = A307245.",
				"Start of table:",
				"a(k_i) = n:",
				"\\i  1   2   3   4   5   6    7    8    9   10   11   12   13   14   15  ...",
				"n\\",
				"0  11  17  19  23  29  31   34   38   39   41   43   46   49   51   53  ...",
				"1   1   2   4   5   7   8    9   13   14   15   16   21   22   25   26  ...",
				"2   3   6  54  56  58  87  100  115  116  123  138  148  160  170  176  ...",
				"3  10  12  18  20  24  28   30   36   40   42   48   84   88   99  144  ...",
				"4  27  33  45  63  66  70   75   80  112  126  135  153  156  162  165  ...",
				"5  60  78  90 102 140 168  200  260  264  285  288  315  378  408  432  ...",
				"6  72 105 108 130 150 306  348  357  450  495  528  560  672  696  708  ...",
				"7 120 132 240 297 312 330  390  588  750  882  980 1140 1176 1190 1215  ...",
				"8 180 198 210 280 396 468  540  612  648  700  810  910  945  960 1020  ...",
				"9 270 420 660 858 918 990 1248 1620 1782 1920 2088 2184 2352 2376 2688  ...",
				"... (End).",
				"If n is a prime \u003c\u003e 3, then a(n) = 1 if n is in A052291 and 0 otherwise, and a(n^2) = 1 if 2*n+1 and 2*n^3+1 are primes and 0 otherwise. - _Robert Israel_, Apr 04 2019"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A306722/b306722.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"a(2) = 1 because (2*2)^2 = (2-1) * (17-1), also, phi(2*17) = 4^2.",
				"a(3) = 2 because (2*3)^2 = (2-1) * (37-1) = (3-1) * (19-1), also, phi(2*37) = phi(3*19) = 6^2.",
				"a(11) = 0  because (2*11)^2 can't be written as (p-1)*(q-1) with p \u003c q."
			],
			"maple": [
				"f:= proc(n) local w;",
				"  w:= (2*n)^2;",
				"  nops(select(t -\u003e t \u003c 2*n and isprime(t+1) and isprime(w/t + 1),  numtheory:-divisors(w)))",
				"end proc:",
				"map(f, [$1..100]); # _Robert Israel_, Apr 04 2019"
			],
			"mathematica": [
				"f[n_] := Length@ Select[ Divisors[ 4n^2], # \u003c 2n \u0026\u0026 PrimeQ[# + 1] \u0026\u0026 PrimeQ[ 4n^2/# + 1] \u0026]; Array[f, 81] (* _Robert G. Wilson v_, Mar 30 2019 *)"
			],
			"program": [
				"(PARI) a(n) = {my(nb = 0, nn = 4*n^2); fordiv(nn, d, if (d == 2*n, break); if (isprime(d+1) \u0026\u0026 isprime(nn/d+1), nb++);); nb;} \\\\ _Michel Marcus_, Mar 06 2019"
			],
			"xref": [
				"Cf. A039770, A052291, A062732, A221284, A221285, A247129, A306440, A307245."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Bernard Schott_, Mar 06 2019",
			"references": 3,
			"revision": 42,
			"time": "2019-08-16T16:24:53-04:00",
			"created": "2019-03-15T22:58:49-04:00"
		}
	]
}