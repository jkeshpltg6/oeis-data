{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A248162",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 248162,
			"data": "0,2,0,6,2,0,12,6,4,2,0,20,12,8,6,4,2,0,30,20,14,12,12,8,6,6,4,2,0,42,30,22,18,20,14,12,10,12,8,6,6,4,2,0,56,42,32,26,24,30,22,18,16,14,20,14,12,10,8,12,8,6,6,4,2,0,72,56,44,36,32,42,32,26,24,24,20,18,30,22,18,16,14,12,20,14,12,10,8,12,8,6,6,4,2,0",
			"name": "Partition number array: sum of part*(part - 1) for each partition of n in Abramowitz-Stegun order for n \u003e= 1.",
			"comment": [
				"The row length sequence is A000041.",
				"The partitions of n are sorted by increasing number of parts m and lexicographically within the same part number (Abramowitz-Stegun or Hindenburg order; see the Apr 04 2011 _Wolfdieter Lang_ comment at A036036). The j-th partition of n with m parts is denoted by p(n,m,j) with j = 1, ..., A008284(n,m). In the tabf version used here the k-th partition of n in the mentioned order is p(n,k) with k from 1 to A000041(n). A partition of n with m parts is denoted by (n_1, n_2, ..., n_m) with nondecreasing entries.",
				"The present partition numbers a(n,m,j) appear in the problem of finding the probability P(n,m,j) of having a pair of one color after picking two balls from a box of n balls coming in m distinct colors with n_l balls of color type c_l for l = 1, 2, ..., m. This probability is P(n,m,j) = a(n,m,j)/(n*(n-1)).",
				"This entry is motivated by the Maslanka puzzle Nr. 4, in The Guardian Weekly, Vol. 191, No 21, date 31.10.14, p. 44, solution p. 47. There the example with partition (3,4,5) and a(12,3,11) = a(12,18) = 3*2 + 4*3 + 5*4 = 38 with P(12,18) = 38/(12*11) = 19/66 has been considered."
			],
			"link": [
				"Wolfdieter Lang, \u003ca href=\"/A248162/a248162.pdf\"\u003eMore rows for the triangles.\u003c/a\u003e"
			],
			"formula": [
				"a(n,k) = Sum_{l=1..m} n_l(k)*(n_l(k)-1) if the k-th partition of n in Abramowitz-Stegun order is (n_1(k), n_2(k), ..., n_m(k)), with n \u003e= 1 and k = 1, 2, ..., A000041(n)."
			],
			"example": [
				"The first rows of this irregular triangle are given as lists belonging to increasing number of parts:",
				"n\\k  1   2   3  4   5   6   7  8  9  10  11 ...",
				"1:  [0]",
				"2:  [2] [0]",
				"3:  [6] [2] [0]",
				"4: [12] [6,  4][2] [0]",
				"5: [20][12,  8][6,  4] [2] [0]",
				"6: [30][20, 14,12][12,  8,  6][6, 4] [2] [0]",
				"...",
				"For more rows see the link.",
				"Row n=10 with 42 entries is: [90] [72, 58, 48, 42, 40]",
				"[56, 44, 36, 32, 34, 28, 26, 24] [42, 32, 26, 24, 24, 20, 18, 18, 16]",
				"[30, 22, 18, 16, 14, 12, 10] [20, 14, 12, 10, 8] [12, 8, 6] [6, 4] [2] [0]",
				"The corresponding (rational) probabilities are (in lowest terms):",
				"n\\k 1    2    3      4     5     6      7  ...",
				"1: [0]",
				"2: [1]  [0]",
				"3: [1] [1/3] [0]",
				"4: [1] [1/2, 1/3]  [1/6]  [0]",
				"5: [1] [3/5, 2/5]  [3/10, 1/5] [1/10]  [0]",
				"...",
				"For more rows see the link.",
				"Row n=9 with 30 entries is: [1] [7/9, 11/18, 1/2, 4/9]",
				"[7/12, 4/9, 13/36, 1/3, 1/3, 5/18, 1/4] [5/12, 11/36, 1/4, 2/9, 7/36, 1/6] [5/18, 7/36, 1/6, 5/36, 1/9] [1/6, 1/9, 1/12] [1/12, 1/18] [1/36] [0].",
				"Row n=10 with 42 entries is: [1], [4/5, 29/45, 8/15, 7/15, 4/9], [28/45, 22/45, 2/5, 16/45, 17/45, 14/45, 13/45, 4/15],",
				"[7/15, 16/45, 13/45, 4/15, 4/15, 2/9, 1/5, 1/5, 8/45],",
				"[1/3, 11/45, 1/5, 8/45, 7/45, 2/15, 1/9], [2/9, 7/45, 2/15, 1/9, 4/45], [2/15, 4/45, 1/15], [1/15, 2/45], [1/45], [0].",
				"n=4, k=3, partition (2,2), colors c_1, c_1, c_2, c_2, abbreviated as 1122. Possible pickings 11.., 1.2., 1..2, .12., .1.2, ..22; that is binomial(4,2) = 6 outcomes and two of them with a pair of equal color. P(4,3) = 2/6 = 1/3 = a(4,3)/(4*3) = 4/(4*3) = 1/3. a(4,3) = 2*1 + 2*1 = 4.",
				"n=5, k=5, partition (1,2,2) with a(5,5) = 1*0 + 2*(2*1) = 4 and color pattern 12233. From the binomial(5,2) = 10 outcomes two have one color, hence P(5,5) = 2/10 = 1/5 = a(5,5)/(5*4) = 4/20 = 1/5."
			],
			"xref": [
				"Cf. A000041, A008284."
			],
			"keyword": "nonn,tabf",
			"offset": "1,2",
			"author": "_Wolfdieter Lang_, Nov 02 2014",
			"references": 1,
			"revision": 13,
			"time": "2018-09-01T17:38:18-04:00",
			"created": "2014-11-04T23:04:24-05:00"
		}
	]
}