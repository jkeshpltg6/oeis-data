{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A328288",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 328288,
			"data": "0,986409,0,438404,304,0,572175,153,157,0,219202,197,124,97,0,109601,221,156,69,171,0,255752,73,88,142,68,69,0,140515,129,73,81,86,62,46,0,109601,189,88,40,67,48,51,24,0,432645,89,80,77,31,63,68,41,20,0,0,0,132,80,90,58,32,63,99,37,0",
			"name": "Triangle T(m,n) = # { k | concat(mk,nk) has no digit twice or more }, m \u003e= n \u003e= 0.",
			"comment": [
				"This is an extended version of A328277 which is restricted to m \u003e n \u003e= 1.",
				"One may consider T(m,n) defined for all m, n \u003e= 0, which would yield a symmetric, infinite square array T(m,n), see formula.",
				"For m and/or n = 0, see A328287(n) = T(0,n) = T(n,0), n \u003e= 0.",
				"The table is finite in the sense that T(m,n) = 0 for m \u003e 987654321 (even if the multiple isn't pandigital, (mk, nk) cannot have more than 9+1 distinct digits), but also whenever the total number of digits of m and n exceeds 10."
			],
			"link": [
				"M. F. Hasler, in reply to E. Angelini, \u003ca href=\"http://list.seqfan.eu/pipermail/seqfan/2019-October\"\u003eFractions with no repeated digits\u003c/a\u003e, SeqFan list, Oct. 10, 2020."
			],
			"formula": [
				"T(m,n) = 0 whenever m == n (mod 10).",
				"T(m,n) = T(n,m) for all m, n \u003e= 0, if the condition m \u003e n is dropped."
			],
			"example": [
				"The table reads :",
				"       0,    (m=0)",
				"  986409, 0,      (m=1)",
				"  438404, 304,   0,    (m=2)",
				"  572175, 153, 157,   0,    (m=3)",
				"  219202, 197, 124,  97,   0,   (m=4)",
				"  109601, 221, 156,  69, 171,  0,   (m=5)",
				"  255752,  73,  88, 142,  68, 69,  0,   (m=6)",
				"  140515, 129,  73,  81,  86, 62, 46,  0,   (m=7)",
				"  109601, 189,  88,  40,  67, 48, 51, 24,  0,   (m=8)",
				"  432645,  89,  80,  77,  31, 63, 68, 41, 20,  0,   (m=9)",
				"       0,   0, 132,  80,  90, 58, 32, 63, 99, 37,  0,   (m=10)",
				"   90212,   0, 106,  69,  79, 50, 30, 45, 30, 38,  0,  0,    (m=11)",
				"  127163,  76,   0,  96,  31, 62, 54, 27, 31, 49, 41, 27,   0,   (m=12)",
				"   75768,  84,  72,   0,  31, 58, 47, 26, 23, 34, 43, 25, 20,   0,  (m=13)",
				"   62436, 100,  64,  52,   0, 51, 44, 51, 42, 22, 38, 27, 18, 20   0,  (m=14)",
				"  ...",
				"The terms corresponding to T(2,1) = 304 and T(3,1) = 153 are given in Eric Angelini's post to the SeqFan list.",
				"Column 0 is A328287 (number of multiples of m that have only distinct and nonzero digits."
			],
			"program": [
				"(PARI) T(m,n)=if(min(m,n), A328277(m,n), A328287(max(m,n))"
			],
			"keyword": "nonn,base,fini",
			"offset": "0,2",
			"author": "_M. F. Hasler_, Oct 10 2019",
			"references": 3,
			"revision": 6,
			"time": "2019-10-11T11:49:44-04:00",
			"created": "2019-10-11T11:49:44-04:00"
		}
	]
}