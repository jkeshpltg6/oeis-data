{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A130539",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 130539,
			"data": "1,4,-13,0,-1,16,11,0,25,-52,-46,0,47,0,-22,0,120,-4,0,0,-121,64,-109,0,-97,44,131,0,0,0,13,0,167,100,-37,0,-214,-208,0,0,121,-184,146,0,-143,0,251,0,0,188,59,0,-118,0,299,0,-168,-88,-325,0,-313",
			"name": "Expansion of q^(-1/3) * a(q) * b(q) * c(q) / 3 in powers of q where a(), b(), c() are cubic AGM theta functions.",
			"comment": [
				"Cubic AGM theta functions: a(q) (see A004016), b(q) (A005928), c(q) (A005882).",
				"Denoted by g_3(q) in Cynk and Hulek in Remark 3.4 on page 12 as the unique level 27 form of weight 3.",
				"This is a member of an infinite family of integer weight modular forms. g_1 = A033687, g_2 = A030206, g_3 = A130539, g_4 = A000731.",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (27 t)) = 27^(3/2) (t/i)^3 f(t) where q = exp(2 Pi i t)."
			],
			"link": [
				"S. Cynk and K. Hulek, \u003ca href=\"http://arXiv.org/abs/math/0509424\"\u003eConstruction and examples of higher-dimensional modular Calabi-Yau manifolds\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(-1/3) * ( eta(q)^5 * eta(q^3) + 9 * eta(q)^2 * eta(q^3) * eta(q^9)^3 ) in powers of q.",
				"a(n) = b(3*n + 1) where b() is multiplicative and b(3^e) = 0^e, b(p^e) = (1 + (-1)^e)/2 * p^e if p == 2 (mod 3), b(p^e) = b(p) * b(p^(e-1)) - p^2 * b(p^(e-2)) if p == 1 (mod 3) where b(p) = x^2 - 2*p, 4*p = x^2 + 3*y^2, |x|\u003c|y| and x == 2 (mod 3).",
				"G.f.: Sum_{k\u003e=0} a(k) * x^(3*k + 1) = (1/2) * Sum_{u, v in Z} (u*u - 7*v*v) * x^(u*u + u*v + 7*v*v). - _Michael Somos_, Jun 14 2007",
				"a(4*n + 1) = 4*a(n). a(4*n + 3) = 0. - _Michael Somos_, Oct 20 2015"
			],
			"example": [
				"G.f. = 1 + 4*x - 13*x^2 - x^4 + 16*x^5 + 11*x^6 + 25*x^8 - 52*x^9 - 46*x^10 + ...",
				"G.f. = q + 4*q^4 - 13*q^7 - q^13 + 16*q^16 + 11*q^19 + 25*q^25 - 52*q^28 - ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ x]^2 QPochhammer[ x^3] (QPochhammer[ x]^3 + 9 x QPochhammer[ x^9]^3), {x, 0, n}]; (* _Michael Somos_, Oct 20 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A, p, e, x, y, a0, a1); n = 3*n + 1; if( n\u003c1, 0, A=factor(n); prod( k=1, matsize(A)[1], [p, e] = A[k, ]; if( p==3, 0, if( p%3==2, if( e%2, 0, p^e), for( x=1, sqrtint(4*p\\27), if( issquare(4*p - 27*x^2, \u0026y), break)); y = y^2 - p*2; a0=1; a1=y; for( i=2, e, x=y*a1 - p^2*a0; a0=a1; a1=x); a1))))};",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A)^2 * eta(x^3 + A) * (eta(x + A)^3 + 9 * x * eta(x^9 + A)^3), n))};"
			],
			"xref": [
				"Cf. A000731, A030206, A033687."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Jun 03 2007",
			"references": 5,
			"revision": 14,
			"time": "2019-11-20T14:22:43-05:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}