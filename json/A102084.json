{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A102084",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 102084,
			"data": "0,4,9,15,25,35,49,55,77,91,121,143,169,187,221,247,289,323,361,391,437,403,529,551,589,667,713,703,841,899,961,943,1073,1147,1189,1271,1369,1363,1517,1591,1681,1763,1849,1927,2021,1891,2209,2279,2257,2491",
			"name": "a(1) = 0; for n\u003e0, write 2n=p+q (p, q prime), p*q maximal; then a(n)=p*q (see A073046).",
			"comment": [
				"For n\u003e1, largest semiprime whose sum of prime factors = 2n. Assumes the Goldbach conjecture is true. Also the largest semiprime \u003c= n^2.",
				"Also the greatest integer x such that x' = 2*n, or 0 if there is no such x, where x' is the arithmetic derivative (A003415). Bisection of A099303. The only even number without an anti-derivative is 2. All terms are \u003c= n^2, with equality only when n is prime. In fact a(n) = n^2 - k^2, where k is the least number such that both n-k and n+k are prime; k = A047160(n). It appears that the anti-derivatives of even numbers are overwhelmingly semiprimes of the form n^2 - k^2. For example, 1000 has 28 anti-derivatives, all of this form. Sequence A189763 lists the even numbers that have anti-derivatives not of this form. - _T. D. Noe_, Apr 27 2011"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A102084/b102084.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = n^2 - A047160(n)^2. - _Jason Kimberley_, Jun 26 2012"
			],
			"example": [
				"n=13: 2n = 26; 26 = 23 + 3 = 19 + 7 = 13 + 13; 13*13 = maximal =\u003e p*q = 13*13 = 169."
			],
			"mathematica": [
				"f[n_] := Block[{pf = FactorInteger[n]}, If[Plus @@ Last /@ pf == 2, If[ Length[pf] == 2, Plus @@ First /@ pf, 2pf[[1, 1]]], 0]]; t = Table[0, {51}]; Do[a = f[n]; If[ EvenQ[a] \u0026\u0026 0 \u003c a \u003c 104, t[[a/2]] = n], {n, 2540}]; t (* _Robert G. Wilson v_, Jun 14 2005 *)",
				"Table[k = 0; While[k \u003c n \u0026\u0026 (! PrimeQ[n - k] || ! PrimeQ[n + k]), k++]; If[k == n, 0, (n - k)*(n + k)], {n, 100}] (* _T. D. Noe_, Apr 27 2011 *)"
			],
			"xref": [
				"Cf. A073046, A003415, A047160, A099303, A189762."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Michael Taktikos_, Feb 16 2005",
			"ext": [
				"Edited by _N. J. A. Sloane_, Aug 29 2008 at the suggestion of _R. J. Mathar_"
			],
			"references": 7,
			"revision": 23,
			"time": "2015-07-30T21:49:42-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}