{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A255140",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 255140,
			"data": "1,4,2,7,13,20,10,19,29,40,4,17,31,46,23,40,5,24,4,25,5,28,14,39,13,40,20,49,7,38,19,52,13,48,24,61,99,138,69,23,65,108,18,63,109,156,78,127,177,228,114,38,19,74,37,94,47,106,53,114,19,82,41,106",
			"name": "a(1) = 1, a(n+1) = a(n)/gcd(a(n),n) if this gcd is \u003e 1, else a(n+1) = a(n) + n + 2.",
			"comment": [
				"A variant of A133058, less trivial than A255051: The sequence looks irregular up to index n = 82, where it enters a 4-periodic pattern (1, x, 2x, x), cf. formula. Sequence A255051 starts right from the beginning with the pattern (1, x, 2x, 2), whereas sequence A133058 enters such a pattern only at index n = 641."
			],
			"link": [
				"Harvey P. Dale, \u003ca href=\"/A255140/b255140.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e"
			],
			"formula": [
				"For k \u003e 20, a(4k) = 8k + 2 = 2*a(4k +- 1), a(4k - 2) = 1; equivalently:",
				"a(n) = 2n + 2, n, 1 or n+2 when n = 4k+r \u003e 81 with r = 0, 1, 2 or 3, respectively."
			],
			"example": [
				"a(2) = a(1) + 3 = 4, a(3) = a(2)/2 = 2, a(4) = a(3) + 5 = 7, a(5) = a(4) + 6 = 13, ..."
			],
			"mathematica": [
				"nxt[{n_,a_}]:=Module[{g=GCD[a,n]},{n+1,If[g\u003e1,a/g,a+n+2]}]; NestList[nxt,{1,1},70][[All,2]] (* _Harvey P. Dale_, Oct 12 2019 *)"
			],
			"program": [
				"(PARI) A255140_vec(N)=vector(N, n, if(gcd(N,n-1)\u003e1||n==1, N/=gcd(N, n-1), N+=n+1)) \\\\ Original code simplified by _M. F. Hasler_, Jan 11 2020",
				"(PARI) A255140(n)=if(n \u003c 82, A255140_upto(n)[n], [2*n+2,n,1,n+2][n%4+1]) \\\\ _M. F. Hasler_, Jan 17 2020",
				"(MAGMA) a:=[1]; for n in [2..65] do if Gcd(a[n-1],n-1) gt 1 then Append(~a, a[n-1] div Gcd(a[n-1],n-1)); else Append(~a, a[n-1] +n+1); end if; end for; a; // _Marius A. Burtea_, Jan 11 2020"
			],
			"xref": [
				"Cf. A133058, A255051."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_M. F. Hasler_, Feb 15 2015",
			"ext": [
				"Edited by _M. F. Hasler_, Jan 11 2020"
			],
			"references": 5,
			"revision": 19,
			"time": "2020-01-17T14:12:45-05:00",
			"created": "2015-02-16T14:24:03-05:00"
		}
	]
}