{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A346517",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 346517,
			"data": "1,1,1,2,1,2,5,3,3,5,15,9,5,9,15,52,31,18,18,31,52,203,120,70,40,70,120,203,877,514,299,172,172,299,514,877,4140,2407,1393,801,457,801,1393,2407,4140,21147,12205,7023,4025,2295,2295,4025,7023,12205,21147",
			"name": "Number A(n,k) of partitions of the (n+k)-multiset {1,2,...,n,1,2,...,k} into distinct multisets; square array A(n,k), n\u003e=0, k\u003e=0, read by antidiagonals.",
			"comment": [
				"Also number A(n,k) of factorizations of Product_{i=1..n} prime(i) * Product_{i=1..k} prime(i) into distinct factors; A(2,2) = 5: 2*3*6, 4*9, 3*12, 2*18, 36."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A346517/b346517.txt\"\u003eAntidiagonals n = 0..140, flattened\u003c/a\u003e"
			],
			"formula": [
				"A(n,k) = A045778(A002110(n)*A002110(k)).",
				"A(n,k) = A(k,n).",
				"A(n,k) = A322770(abs(n-k),min(n,k))."
			],
			"example": [
				"A(2,2) = 5: 1122, 11|22, 1|122, 112|2, 1|12|2.",
				"Square array A(n,k) begins:",
				"    1,    1,    2,     5,    15,     52,    203,     877, ...",
				"    1,    1,    3,     9,    31,    120,    514,    2407, ...",
				"    2,    3,    5,    18,    70,    299,   1393,    7023, ...",
				"    5,    9,   18,    40,   172,    801,   4025,   21709, ...",
				"   15,   31,   70,   172,   457,   2295,  12347,   70843, ...",
				"   52,  120,  299,   801,  2295,   6995,  40043,  243235, ...",
				"  203,  514, 1393,  4025, 12347,  40043, 136771,  875936, ...",
				"  877, 2407, 7023, 21709, 70843, 243235, 875936, 3299218, ...",
				"  ..."
			],
			"maple": [
				"g:= proc(n, k) option remember; uses numtheory; `if`(n\u003ek, 0, 1)+",
				"     `if`(isprime(n), 0, add(`if`(d\u003ek or max(factorset(n/d))\u003ed, 0,",
				"        g(n/d, d-1)), d=divisors(n) minus {1, n}))",
				"    end:",
				"p:= proc(n) option remember; `if`(n=0, 1, p(n-1)*ithprime(n)) end:",
				"A:= (n, k)-\u003e g(p(n)*p(k)$2):",
				"seq(seq(A(n, d-n), n=0..d), d=0..10);",
				"# second Maple program:",
				"b:= proc(n) option remember; `if`(n=0, 1,",
				"      add(b(n-j)*binomial(n-1, j-1), j=1..n))",
				"    end:",
				"A:= proc(n, k) option remember; `if`(n\u003ck, A(k, n),",
				"     `if`(k=0, b(n), (A(n+1, k-1)-add(A(n-k+j, j)",
				"      *binomial(k-1, j), j=0..k-1)+A(n, k-1))/2))",
				"    end:",
				"seq(seq(A(n, d-n), n=0..d), d=0..10);"
			],
			"mathematica": [
				"(* Q is A322770 *)",
				"Q[m_, n_] := Q[m, n] = If[n == 0, BellB[m], (1/2)(Q[m+2, n-1] + Q[m+1, n-1] - Sum[Binomial[n-1, k] Q[m, k], {k, 0, n-1}])];",
				"A[n_, k_] := Q[Abs[n-k], Min[n, k]];",
				"Table[A[n, d-n], {d, 0, 10}, {n, 0, d}] // Flatten (* _Jean-François Alcover_, Aug 19 2021 *)"
			],
			"xref": [
				"Columns (or rows) k=0-10 give: A000110, A087648, A322773, A322774, A346897, A346898, A346899, A346900, A346901, A346902, A346903.",
				"Main diagonal gives A094574.",
				"First upper (or lower) diagonal gives A322771.",
				"Second upper (or lower) diagonal gives A322772.",
				"Antidiagonal sums give A346518.",
				"Cf. A002110, A045778, A322770, A346500."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_Alois P. Heinz_, Jul 21 2021",
			"references": 15,
			"revision": 24,
			"time": "2021-08-19T05:01:56-04:00",
			"created": "2021-07-21T17:38:29-04:00"
		}
	]
}