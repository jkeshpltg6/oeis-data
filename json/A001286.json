{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001286",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1286,
			"id": "M4225 N1766",
			"data": "1,6,36,240,1800,15120,141120,1451520,16329600,199584000,2634508800,37362124800,566658892800,9153720576000,156920924160000,2845499424768000,54420176498688000,1094805903679488000,23112569077678080000",
			"name": "Lah numbers: (n-1)*n!/2.",
			"comment": [
				"Sum_{i=0..n} (-1)^i * i^(n+1) * binomial(n,i) = (-1)^n * n * (n+1)! /2. - Yong Kong (ykong(AT)curagen.com), Dec 26 2000",
				"Number of surjections from {1,...,n} to {1,...,n-1}. - _Benoit Cloitre_, Dec 05 2003",
				"a(n+1)=(-1)^(n+1)*det(M_n) where M_n is the n X n matrix M_(i,j)=max(i*(i+1)/2,j*(j+1)/2). - _Benoit Cloitre_, Apr 03 2004",
				"First Eulerian transform of 0,1,2,3,4,... . - _Ross La Haye_, Mar 05 2005",
				"With offset 0 : determinant of the n X n matrix m(i,j)=(i+j+1)!/i!/j!. - _Benoit Cloitre_, Apr 11 2005",
				"These numbers arise when expressing n(n+1)(n+2)...(n+k)[n+(n+1)+(n+2)+...+(n+k)] as sums of squares: n(n+1)[n+(n+1)] = 6(1+4+9+16+ ... + n^2), n(n+1)(n+2)(n+(n+1)+(n+2)) = 36(1+(1+4)+(1+4+9)+...+(1+4+9+16+ ... + n^2)), n(n+1)(n+2)(n+3)(n+(n+1)+(n+2)+(n+3)) = 240(...), ... . - _Alexander R. Povolotsky_, Oct 16 2006",
				"a(n) = number of edges in the Hasse diagram for the weak Bruhat order on the symmetric group S_n. For permutations p,q in S_n, q covers p in the weak Bruhat order if p,q differ by an adjacent transposition and q has one more inversion than p. Thus 23514 covers 23154 due to the transposition that interchanges the third and fourth entries. Cf. A002538 for the strong Bruhat order. - _David Callan_, Nov 29 2007",
				"a(n) is also the number of excedances in all permutations of {1,2,...,n} (an excedance of a permutation p is a value j such p(j)\u003ej). Proof: j is exceeded (n-1)! times by each of the numbers j+1, j+2, ..., n; now, Sum_{j=1..n} (n-j)(n-1)! = n!(n-1)/2. Example: a(3)=6 because the number of excedances of the permutations 123, 132, 312, 213, 231, 321 are 0, 1, 1, 1, 2, 1, respectively. - _Emeric Deutsch_, Dec 15 2008",
				"(-1)^(n+1)*a(n) is the determinant of the n X n matrix whose (i,j)-th element is 0 for i = j, is j-1 for j\u003ei, and j for j \u003c i. - _Michel Lagneau_, May 04 2010",
				"Row sums of the triangle in A030298. - _Reinhard Zumkeller_, Mar 29 2012",
				"a(n) is the total number of ascents (descents) over all n-permutations. a(n) = Sum_{k=1..n} A008292(n,k)*k. - _Geoffrey Critzer_, Jan 06 2013",
				"For m\u003e=4, a(m-2) is the number of Hamiltonian cycles in a simple graph with m vertices which is complete, except for one edge. Proof: think of distinct round-table seatings of m persons such that persons \"1\" and \"2\" may not be neighbors; the count is (m-3)(m-2)!/2. See also A001710. - _Stanislav Sykora_, Jun 17 2014",
				"Popularity of left (right) children in treeshelves. Treeshelves are ordered binary (0-1-2) increasing trees where every child is connected to its parent by a left or a right link. Popularity is the sum of a certain statistic (number of left children, in this case) over all objects of size n. See A278677, A278678 or A278679 for more definitions and examples. See A008292 for the distribution of the left (right) children in treeshelves. - _Sergey Kirgizov_, Dec 24 2016"
			],
			"reference": [
				"A. T. Benjamin and J. J. Quinn, Proofs that really count: the art of combinatorial proof, M.A.A. 2003, p. 90, ex. 4.",
				"L. Comtet, Advanced Combinatorics, Reidel, 1974, p. 156.",
				"A. P. Prudnikov, Yu. A. Brychkov and O.I. Marichev, \"Integrals and Series\", Volume 1: \"Elementary Functions\", Chapter 4: \"Finite Sums\", New York, Gordon and Breach Science Publishers, 1986-1992.",
				"J. Riordan, An Introduction to Combinatorial Analysis, Wiley, 1958, p. 44.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A001286/b001286.txt\"\u003eTable of n, a(n) for n = 2..100\u003c/a\u003e",
				"Jean-Luc Baril, Sergey Kirgizov, Vincent Vajnovszki, \u003ca href=\"https://arxiv.org/abs/1611.07793\"\u003ePatterns in treeshelves\u003c/a\u003e, arXiv:1611.07793 [cs.DM], 2016.",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=399\"\u003eEncyclopedia of Combinatorial Structures 399\u003c/a\u003e",
				"Milan Janjic, \u003ca href=\"http://www.pmfbl.org/janjic/\"\u003eEnumerative Formulas for Some Functions on Finite Sets\u003c/a\u003e",
				"Sandi Klavžar, Uroš Milutinović and Ciril Petr, \u003ca href=\"http://dx.doi.org/10.1016/j.exmath.2005.05.003\"\u003eHanoi graphs and some classical numbers\u003c/a\u003e, Expo. Math. 23 (2005), no. 4, 371-378.",
				"S. Lehr, J. Shallit and J. Tromp, \u003ca href=\"http://dx.doi.org/10.1016/0304-3975(95)00234-0\"\u003eOn the vector space of the automatic reals\u003c/a\u003e, Theoret. Comput. Sci. 163 (1996), no. 1-2, 193-210.",
				"P. A. Piza, \u003ca href=\"http://www.jstor.org/stable/3029339\"\u003eKummer numbers\u003c/a\u003e, Mathematics Magazine, 21 (1947/1948), 257-260.",
				"P. A. Piza, \u003ca href=\"/A001117/a001117.pdf\"\u003eKummer numbers\u003c/a\u003e, Mathematics Magazine, 21 (1947/1948), 257-260. [Annotated scanned copy]",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/BruhatGraph.html\"\u003eBruhat Graph\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/EdgeCount.html\"\u003eEdge Count\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PermutationAscent.html\"\u003ePermutation Ascent\u003c/a\u003e"
			],
			"formula": [
				"E.g.f.: x^2/[2(1-x)^2]. - _Ralf Stephan_, Apr 02 2004",
				"Row sums of table A051683. - _Alford Arnold_, Sep 29 2006",
				"5th binomial transform of A135218: (1, 1, 1, 25, 25, 745, 3145, ...). - _Gary W. Adamson_, Nov 23 2007",
				"If we define f(n,i,x) = Sum_{k=i..n} Sum_{j=i..k} binomial(k,j)*Stirling1(n,k)*Stirling2(j,i)*x^(k-j) then a(n)=(-1)^n*f(n,2,-2), (n\u003e=2). - _Milan Janjic_, Mar 01 2009",
				"a(n) = A000217(n-1)*A000142(n-1). - _Reinhard Zumkeller_, May 15 2010",
				"a(n) = (n+1)!*Sum_{k=1..n-1} 1/(k^2+3*k+2). - _Gary Detlefs_, Sep 14 2011",
				"Sum_{n\u003e=2} 1/a(n) = 2*(2 - exp(1) - gamma + Ei(1)) = 1.19924064599..., where gamma = A001620 and Ei(1) = A091725. - _Ilya Gutkovskiy_, Nov 24 2016",
				"a(n+1) = a(n)*n*(n+1)/(n-1). - _Chai Wah Wu_, Apr 11 2018"
			],
			"example": [
				"G.f. = x^2 + 6*x^3 + 36*x^4 + 240*x^5 + 1800*x^6 + 15120*x^7 + 141120*x^8 + ...",
				"a(10) = (1+2+3+4+5+6+7+8+9)*(1*2*3*4*5*6*7*8*9) = 16329600. - _Reinhard Zumkeller_, May 15 2010"
			],
			"maple": [
				"a:=n-\u003esum(j*n!,j=0..n): seq(a(n), n=0..19); # _Zerinvary Lajos_, Dec 01 2006",
				"seq(sum(mul(j,j=3..n), k=2..n), n=2..21); # _Zerinvary Lajos_, Jun 01 2007",
				"a:=n-\u003esum(k*mul(k, k=1..n),k=1..n):seq(a(n), n=1...19); # _Zerinvary Lajos_, Jun 11 2008",
				"G(x):=x^2/(1-x)^2: f[0]:=G(x): for n from 1 to 20 do f[n]:=diff(f[n-1],x) od: x:=0: seq(f[n]/2,n=2..20); # _Zerinvary Lajos_, Apr 01 2009",
				"with(combinat):seq(n/2*numbperm(n+1,n), n=1..19); # _Zerinvary Lajos_, Apr 17 2009"
			],
			"mathematica": [
				"lst={};s=0;Do[s=s+n;AppendTo[lst, n!*s], {n, 30}];lst (* _Vladimir Joseph Stephan Orlovsky_, Sep 07 2008 *)",
				"Table[Sum[n!, {i, 2, n}]/2, {n, 2, 20}] (* _Zerinvary Lajos_, Jul 12 2009 *)",
				"nn=20;With[{a=Accumulate[Range[nn]],t=Range[nn]!},Times@@@Thread[{a,t}]] (* _Harvey P. Dale_, Jan 26 2013 *)",
				"Table[(n - 1) n! / 2, {n, 2, 30}] (* _Vincenzo Librandi_, Sep 09 2016 *)"
			],
			"program": [
				"(Sage) [(n-1)*factorial(n)/2 for n in range(2, 21)] # _Zerinvary Lajos_, May 16 2009",
				"(Haskell)",
				"a001286 n = sum[1..n-1] * product [1..n-1]",
				"-- _Reinhard Zumkeler_, Aug 01 2011",
				"(Maxima) A001286(n):=(n-1)*n!/2$",
				"makelist(A001286(n),n,1,30); /* _Martin Ettl_, Nov 03 2012 */",
				"(PARI) a(n)=(n-1)*n!/2 \\\\ _Charles R Greathouse IV_, Nov 20 2012",
				"(MAGMA) [(n-1)*Factorial(n)/2: n in [2..25]]; // _Vincenzo Librandi_, Sep 09 2016",
				"(Python)",
				"from __future__ import division",
				"A001286_list = [1]",
				"for n in range(2,100):",
				"    A001286_list.append(A001286_list[-1]*n*(n+1)//(n-1)) # _Chai Wah Wu_, Apr 11 2018"
			],
			"xref": [
				"Cf. A001710, A052609, A062119, A075181, A060638, A060608, A060570, A060612, A135218, A019538, A053495, A051683, A213168, A278677, A278678, A278679, A008292.",
				"A002868 is an essentially identical sequence.",
				"Column 2 of |A008297|.",
				"Third column (m=2) of triangle |A111596(n, m)|: matrix product of |S1|.S2 Stirling number matrices.",
				"Cf. also A000110, A000111."
			],
			"keyword": "nonn,easy,nice",
			"offset": "2,2",
			"author": "_N. J. A. Sloane_",
			"references": 70,
			"revision": 131,
			"time": "2021-08-20T22:09:34-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}