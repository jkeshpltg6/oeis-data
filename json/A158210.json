{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A158210",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 158210,
			"data": "0,-1,-1,1,-1,-2,-1,1,1,-2,-1,2,-1,-2,-2,1,-1,2,-1,2,-2,-2,-1,2,1,-2,1,2,-1,-3,-1,1,-2,-2,-2,2,-1,-2,-2,2,-1,-3,-1,2,2,-2,-1,2,1,2,-2,2,-1,2,-2,2,-2,-2,-1,3,-1,-2,2,1,-2,-3,-1,2,-2,-3,-1,2,-1,-2,2,2,-2,-3,-1,2,1",
			"name": "Number omega(n) of distinct primes dividing n multiplied by -1 when n is squarefree (thus Omega(n) = omega(n)).",
			"comment": [
				"This sequence reveals, among the positive integers, which are the unit, the primes, the prime powers, the squarefree (quadratfrei) composites and finally the nonsquarefree composites per the following:",
				"a(n) \u003c -1: squarefree composites",
				"a(n) = -1: primes (squarefree prime components)",
				"a(n) = 0: unit (squarefree since 1 has no squares of primes as factors)",
				"a(n) = +1: prime powers (nonsquarefree prime components)",
				"a(n) \u003e +1: nonsquarefree composites",
				"The nonsquarefree numbers are misleadingly referred to as squareful numbers (squaresome (?) would have been more precise, but this term is not in use)."
			],
			"link": [
				"Daniel Forgues, \u003ca href=\"/A158210/b158210.txt\"\u003eTable of n, a(n) for n=1..10000\u003c/a\u003e",
				"Weisstein, Eric W., \u003ca href=\"http://mathworld.wolfram.com/Squarefree.html\"\u003eSquarefree\u003c/a\u003e.",
				"Weisstein, Eric W., \u003ca href=\"http://mathworld.wolfram.com/Squareful.html\"\u003eSquareful\u003c/a\u003e."
			],
			"formula": [
				"a(n) = omega(n) * (-1)^mu(n), where mu is the Moebius function.",
				"a(n) = A001221(n) * (-1)^A008683(n).",
				"While omega(n) is additive (i.e., omega(mn) = omega(m) + omega(n), gcd(m,n) = 1), this sequence, while not additive, has the following rule:",
				"a(mn) = [|a(m)| + |a(n)|] * max(sign[a(n)], sign[a(m)]), gcd(m,n) = 1, m \u003e 1, n \u003e 1."
			],
			"mathematica": [
				"Table[(-1)^MoebiusMu[n]*PrimeNu[n], {n, 81}] (* _L. Edson Jeffery_, Dec 08 2014 *)"
			],
			"xref": [
				"Cf. A001221 Number of distinct primes dividing n (also called omega(n)).",
				"Cf. A001222 Number of prime divisors of n (counted with multiplicity) (also called Omega(n)).",
				"Cf. A008683 Moebius (or Mobius) function mu(n).",
				"Cf. A005117 Squarefree numbers.",
				"Cf. A013929 Nonsquarefree numbers.",
				"Cf. A000040 The prime numbers.",
				"Cf. A025475 Powers of a prime but not prime."
			],
			"keyword": "sign",
			"offset": "1,6",
			"author": "_Daniel Forgues_, Mar 14 2009",
			"references": 2,
			"revision": 13,
			"time": "2016-12-18T12:50:52-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}