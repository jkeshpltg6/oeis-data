{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A123741",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 123741,
			"data": "1,2,24,630,52800,11381760,6738443712,10487895163200,43294107630090240,469590163875486482400,13388418681612808458240000,1001088091286168023193223168000,196239953628635168336022309340569600",
			"name": "A second version of Fibonacci factorials besides A003266.",
			"comment": [
				"The formula below is a generalization of n! = Product_{j=1..n} ((n+1) - j) with numbers k replaced by Fibonacci numbers F(k+1):=A000045(k+1), k\u003e=1.",
				"These numbers come up in Vandermonde determinants involving Fibonacci numbers [F(2),...,F(n+1)]. See A123742."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A123741/b123741.txt\"\u003eTable of n, a(n) for n = 1..68\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Product_{j=1..n} (F(n+2) - F(j+1)), n\u003e=1.",
				"a(n) ~ c * phi^(n*(n+2)) / 5^(n/2), where c = A276987 = QPochhammer(1/phi) and phi = A001622 = (1+sqrt(5))/2 is the golden ratio. - _Vaclav Kotesovec_, Mar 31 2021"
			],
			"example": [
				"n=3: (5-1)*(5-2)*(5-3) = 4*3*2 = 24;",
				"n=4: (8-1)*(8-2)*(8-3)*(8-5) = 7*6*5*3 = 630."
			],
			"maple": [
				"with(combinat): seq(mul(fibonacci(n+2)-fibonacci(j+1), j = 1..n), n = 1 .. 20); # _G. C. Greubel_, Aug 10 2019"
			],
			"mathematica": [
				"With[{F=Fibonacci}, Table[Product[F[n+2]-F[j+1],{j,n}], {n,20}]] (* _G. C. Greubel_, Aug 10 2019 *)"
			],
			"program": [
				"(PARI) vector(20, n, f=fibonacci; prod(j=1,n, f(n+2)-f(j+1))) \\\\ _G. C. Greubel_, Aug 10 2019",
				"(MAGMA) F:=Fibonacci; [(\u0026*[F(n+2)-F(j+1): j in [1..n]]): n in [1..20]] // _G. C. Greubel_, Aug 10 2019",
				"(Sage) f=fibonacci; [prod(f(n+2)-f(j+1) for j in (1..n)) for n in (1..20)] # _G. C. Greubel_, Aug 10 2019",
				"(GAP) F:=Fibonacci;; List([1..20], n-\u003e Product([1..n], j-\u003e F(n+2) - F(j+1))); # _G. C. Greubel_, Aug 10 2019"
			],
			"xref": [
				"Cf. A003266 (the usual Fibonacci factorials), A123742."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Wolfdieter Lang_, Oct 13 2006",
			"references": 5,
			"revision": 15,
			"time": "2021-03-31T07:15:28-04:00",
			"created": "2006-12-06T03:00:00-05:00"
		}
	]
}