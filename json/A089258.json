{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A089258",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 89258,
			"data": "1,1,0,1,1,1,1,2,2,2,1,3,5,6,9,1,4,10,16,24,44,1,5,17,38,65,120,265,1,6,26,78,168,326,720,1854,1,7,37,142,393,872,1957,5040,14833,1,8,50,236,824,2208,5296,13700,40320,133496,1,9,65,366,1569,5144,13977,37200,109601,362880,1334961",
			"name": "Transposed version of A080955: T(n,k) = A080955(k,n), n\u003e=0, k\u003e=-1.",
			"comment": [
				"Can be extended to columns with negative indices k\u003c0 via T(n,k) = A292977(n,-k). - _Max Alekseyev_, Mar 06 2018"
			],
			"formula": [
				"For n \u003e 0, k \u003e= -1, T(n,k) is the permanent of the n X n matrix with k+1 on the diagonal and 1 elsewhere.",
				"T(0,k) = 1.",
				"T(n,k) = Sum_{j\u003e=0} A008290(n,j) * (k+1)^j.",
				"T(n,k) = n*T(n-1, k) + k^n .",
				"T(n,k) = n! * Sum_{j=0..n} k^j/j!.",
				"E.g.f. for k-th column: exp(k*x)/(1-x).",
				"Assuming n \u003e= 0, k \u003e= 0: T(n, k) = exp(k-1)*Gamma(n+1, k-1). - _Peter Luschny_, Dec 24 2021"
			],
			"example": [
				"n\\k -1   0   1    2    3    4     5     6  ...",
				"----------------------------------------------",
				"0  | 1,  1,  1,   1,   1,   1,    1,    1, ...",
				"1  | 0,  1,  2,   3,   4,   5,    6,    7, ...",
				"2  | 1,  2,  5,  10,  17,  26,   37,   50, ...",
				"3  | 2,  6, 16,  38,  78, 152,  236,  366, ...",
				"4  | 9, 24, 65, 168, 393, 824, 1569, 2760, ...",
				"..."
			],
			"mathematica": [
				"(* Assuming offset (0, 0): *)",
				"T[n_, k_] := Exp[k - 1] Gamma[n + 1, k - 1];",
				"Table[T[k, n - k], {n, 0, 10}, {k, 0, n}] // Flatten  (* _Peter Luschny_, Dec 24 2021 *)"
			],
			"xref": [
				"Columns: A000166, A000142, A000522, A010842, A053486, A053487, A080954.",
				"Main diagonal gives A217701.",
				"Cf. A080955, A008290, A292977."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "0,8",
			"author": "_Philippe Deléham_, Dec 12 2003",
			"ext": [
				"Edited and changed offset for k to -1 by _Max Alekseyev_, Mar 08 2018"
			],
			"references": 7,
			"revision": 26,
			"time": "2021-12-24T13:09:37-05:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}