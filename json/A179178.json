{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A179178",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 179178,
			"data": "1,3,7,14,25,40,57,77,100,126,155,187,222,260,301,345,392,442,495,551,610,672,737,805,876,950,1027,1107,1190,1276,1365,1457,1552,1650,1751,1855,1962,2072,2185,2301,2420,2542,2667,2795,2926,3060,3197,3337,3480",
			"name": "The number of equal-sized equilateral triangles visible (when viewed from above) in successive Genealodrons formed from 2^n -1 same size equilateral triangles.",
			"comment": [
				"A Genealodron represents a rooted binary tree and is composed of equal sized equilateral triangles. One edge of each triangle is attached to its parent and the other two to its child trees. The first triangle, which is the root of the tree, has a designated unattached parent edge. Triangles may overlap as needed.",
				"The first Genealodron consists of one equilateral triangle.",
				"The second Genealodron is formed by joining another same size equilateral triangle to the left edge and to the right edge of the first so that the second Genealodron is made up of three triangles.",
				"The third Genealodron is formed by joining same size equilateral triangles to the left and right edges of both the second and third triangle of the second Genealodron so that the third Genealodron is made up of seven triangles.",
				"The fourth Genealodron is formed by joining same size equilateral triangles to the left and right edges of the fourth, fifth, sixth and seventh triangles of the third Genealodron so that the fourth Genealodron has fifteen triangles. The fourth Genealodron has the first overlap so although it contains 15 triangles only 14 are seen when it is viewed from above.",
				"The fifth Genealodron is formed by adding 16 more triangles to the edges of last eight triangles added to the fourth Genealodron so the fifth Genealodron has 31 triangles, only 25 of which are seen when it is viewed from above because of the increasing number of overlaps.",
				"The sixth Genealodron has 63 triangles only 40 of which are visible.",
				"Gradually within the Genealodron spirals (which are hexagonal in cross-section) are building counterclockwise on the left hand edge of every triangle and clockwise on the right hand edge of every triangle. Because of the way the triangles form into hexagonal stacks although the total number of triangles in successive Genealodrons is 2^n - 1 the rate at which the number of visible triangles increases becomes arithmetic with a common difference of 3.",
				"A Genealodron formed from an infinite number of same size equilateral triangles creates a hyperbolic plane.",
				"Also, the crystal ball sequence for the honeycomb point lattice with a single edge removed at the origin. Without this removal the sequence would be A005448. The sixth Genealodron when viewed from above has the shape of a hexagon (see illustration of initial terms). All subsequent generations will retain this shape and so the sequence becomes A005449, the second pentagonal numbers. - _Andrew Howroyd_, Mar 24 2016"
			],
			"reference": [
				"Mohammad K. Azarian, A Trigonometric Characterization of Equilateral Triangle, Problem 336, Mathematics and Computer Education, Vol. 31, No. 1, Winter 1997, p. 96.  Solution published in Vol. 32, No. 1, Winter 1998, pp. 84-85.",
				"Mohammad K. Azarian, Equating Distances and Altitude in an Equilateral Triangle, Problem 316, Mathematics and Computer Education, Vol. 28, No. 3, Fall 1994, p. 337.  Solution published in Vol. 29, No. 3, Fall 1995, pp. 324-325."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A179178/b179178.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Kival Ngaokrajang, \u003ca href=\"/A179178/a179178.pdf\"\u003eIllustration of initial terms\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-3,1)."
			],
			"formula": [
				"a(n) = A005449(n-1) for n\u003e=6. - _Andrew Howroyd_, Mar 24 2016",
				"From _Colin Barker_, Mar 26 2016: (Start)",
				"a(n) = 3*a(n-1)-3*a(n-2)+a(n-3) for n\u003e5.",
				"a(n) = (2-5*n+3*n^2)/2 for n\u003e5.",
				"G.f.: x*(1+x^2+x^3+x^4-2*x^6+x^7) / (1-x)^3.",
				"(End)"
			],
			"mathematica": [
				"CoefficientList[Series[x (1 + x^2 + x^3 + x^4 - 2 x^6 + x^7)/(1 - x)^3, {x, 0, 49}], x] (* _Michael De Vlieger_, Mar 26 2016 *)"
			],
			"program": [
				"(PARI) Vec(x*(1+x^2+x^3+x^4-2*x^6+x^7)/(1-x)^3 + O(x^50)) \\\\ _Colin Barker_, Mar 26 2016"
			],
			"xref": [
				"Cf. A179316."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "Elizabeth Hignell (elizabethhignell(AT)hotmail.com), Jun 30 2010",
			"ext": [
				"a(12)-a(40) from _Andrew Howroyd_, Mar 24 2016",
				"Definition improved by _Andrew Howroyd_, Apr 16 2020"
			],
			"references": 8,
			"revision": 35,
			"time": "2020-04-16T20:21:01-04:00",
			"created": "2010-07-11T03:00:00-04:00"
		}
	]
}