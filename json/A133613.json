{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A133613",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 133613,
			"data": "7,8,3,5,9,1,4,6,4,2,6,2,7,2,6,5,7,5,4,0,1,9,5,0,9,3,4,6,8,1,5,8,4,8,1,0,7,6,9,3,2,7,8,4,3,2,2,2,3,0,0,8,3,6,6,9,4,5,0,9,7,6,9,3,9,9,8,1,6,9,9,3,6,9,7,5,3,5,2,6,5,1,5,8,3,9,1,8,1,0,5,6,2,8,4,2,4,0,4,9,8,0,5,1,6",
			"name": "Decimal digits such that for all k\u003e=1, the number A(k) := Sum_{n = 0..k-1 } a(n)*10^n satisfies the congruence 3^A(k) == A(k) (mod 10^k).",
			"comment": [
				"10-adic expansion of the iterated exponential 3^^n for sufficiently large n (where c^^n denotes a tower of c's of height n). E.g., for n\u003e9, 3^^n == 4195387 (mod 10^7).",
				"This sequence also gives many final digits of Graham's number ...399618993967905496638003222348723967018485186439059104575627262464195387. - _Paul Muljadi_, Sep 08 2008 and J. Luis A. Yebra, Dec 22 2008",
				"Graham's number can be represented as G(64):=3^^3^^...^^3 [see M. Gardner and Wikipedia], in which case its G(63) lowermost digits are guaranteed to match this sequence (i.e., the convergence speed of the base 3 is unitary - see A317905). To avoid such confusion, it would be best to interpret this sequence as a real-valued constant 0.783591464..., corresponding to 3^^k in the limit of k-\u003einfinity, and call it Graham's constant G(3). Generalizations to G(n) and G(n,base) are obvious. - _Stanislav Sykora_, Nov 07 2015"
			],
			"reference": [
				"M. Gardner, Mathematical Games, Scientific American 237, 18 - 28 (1977).",
				"M. Ripà, La strana coda della serie n^n^...^n, Trento, UNI Service, Nov 2011, p. 11-12, 69-78. ISBN 978-88-6178-789-6.",
				"Ilan Vardi, \"Computational Recreations in Mathematica,\" Addison-Wesley Publishing Co., Redwood City, CA, 1991, pages 226-229."
			],
			"link": [
				"Robert G. Wilson v, \u003ca href=\"/A133613/b133613.txt\"\u003eTable of n, a(n) for n = 0..10039\u003c/a\u003e",
				"J. Jimenez Urroz and J. Luis A. Yebra, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL12/Yebra/yebra4.html\"\u003eOn the equation a^x == x (mod b^n)\u003c/a\u003e, J. Int. Seq. 12 (2009) #09.8.8.",
				"Robert P. Munafo, \u003ca href=\"http://www.mrob.com/pub/math/largenum-4.html#graham\"\u003eLarge Numbers\u003c/a\u003e [From _Robert G. Wilson v_, May 07 2010]",
				"Reddit user atticdoor, \u003ca href=\"https://www.reddit.com/r/OEIS/comments/5pylei/spotted_an_error_in_the_comments_of_sequence/\"\u003eSpotted an error in the comments of sequence A133613.\u003c/a\u003e",
				"Marco Ripà, \u003ca href=\"https://doi.org/10.7546/nntdm.2020.26.3.245-260\"\u003eOn the constant congruence speed of tetration\u003c/a\u003e, Notes on Number Theory and Discrete Mathematics, Volume 26, 2020, Number 3, Pages 245—260.",
				"Marco Ripà, \u003ca href=\"https://doi.org/10.7546/nntdm.2021.27.4.43-61\"\u003eThe congruence speed formula\u003c/a\u003e, Notes on Number Theory and Discrete Mathematics, 2021, 27(4), 43-61.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Graham\u0026#39;s_number\"\u003eGraham's number\u003c/a\u003e"
			],
			"formula": [
				"a(n) = floor( A183613(n+1) / 10^n )."
			],
			"example": [
				"783591464262726575401950934681584810769327843222300836694509769399816993697535...",
				"Consider the sequence 3^^n: 1, 3, 27, 7625597484987, ... From 3^^3 = 7625597484987 onwards, all terms end with the digits 87. This follows from Euler's generalization of Fermat's little theorem."
			],
			"mathematica": [
				"(* Import Mmca coding for \"SuperPowerMod\" and \"LogStar\" from text file in A133612 and then *) $RecursionLimit = 2^14; f[n_] := SuperPowerMod[3, n + 1, 10^n]; Reverse@ IntegerDigits@ f@ 105 (* _Robert G. Wilson v_, Mar 06 2014 *)"
			],
			"xref": [
				"Cf. A133612, A133614, A133615, A133616, A133617, A133618, A133619, A144539, A144540, A144541, A144542, A144543, A144544, A317905, A318478."
			],
			"keyword": "nonn,base",
			"offset": "0,1",
			"author": "Daniel Geisler (daniel(AT)danielgeisler.com), Dec 18 2007",
			"ext": [
				"More terms from J. Luis A. Yebra, Dec 12 2008",
				"Edited by _N. J. A. Sloane_, Dec 22 2008",
				"More terms from _Robert G. Wilson v_, May 07 2010"
			],
			"references": 17,
			"revision": 75,
			"time": "2021-12-16T04:15:38-05:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}