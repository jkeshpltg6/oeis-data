{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A257942",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 257942,
			"data": "1,3,12,20,15,21,56,72,45,55,132,156,91,105,240,272,153,171,380,420,231,253,552,600,325,351,756,812,435,465,992,1056,561,595,1260,1332,703,741,1560,1640,861,903,1892,1980,1035,1081,2256,2352,1225,1275,2652",
			"name": "a(n) = (n+1)*(n+2)/A014695(n+1), where A014695 is repeat (1, 2, 2, 1).",
			"comment": [
				"Consider, for n \u003e= 0, a sequence s(n). A useful transform is wi(n) = s(0), s(2), s(3), ..., i.e., s(n) without s(1).",
				"For s(n) = 1/(n+1), wi(n)= 1, 1/3, 1/4, 1/5, ..., whose inverse binomial transform is f(n) = 1, -2/3, 7/12, -11/20, 8/15, -11/21, 29/56, -37/72, 23/45, -28/55, 67/132, -79/156, 46/91, -53/105, 121/240, -137/272, ...",
				"The denominator of f(n) is a(n), for n \u003e= 0.",
				"If the numerator of f(n) is b(n), then it can be seen that b(n+1) = -(-1)^n* A226089(n).",
				"Alternating a(n) - b(n) with a(n) + b(n) yields 0, 1, 5, 9, ... = A160050(n+1).",
				"a(4n+1) is linked to the Rydberg-Ritz spectra of hydrogen.",
				"h(n) = 0, 0, 1, 1, 4, 4, 3, 3, 8, 8, 5, 5, ... = duplicated A022998(n).",
				"A022998(n) is linked to the Balmer series (see A246943(n)).",
				"With an initial 0 and offset=0, a(-n) = a(n). Then (a(n+10) - a(n-10))/10 = 1, 6, 10, 7, 9, 22, 26, ... = g(n). a(n) mod 9 is of period 20."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A257942/b257942.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_09\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-6,10,-12,12,-10,6,-3,1)."
			],
			"formula": [
				"a(4n)    = (2*n+1)*(4*n+1).",
				"a(4n+1)  = (2*n+1)*(4*n+3).",
				"a(4n+2)  = (4*n+3)*(4*n+4).",
				"a(4n+3)  = (4*n+4)*(4*n+5).",
				"a(n) = A064038(n+2) * (period 4: repeat 1, 1, 4, 4).",
				"From _Colin Barker_, Jul 14 2015: (Start)",
				"a(n) = (-1/8+i/8)*(((-3-i*3)+i*(-i)^n+i^n)*(2+3*n+n^2)) where i=sqrt(-1).",
				"G.f.: -(x^6+9*x^4-8*x^3+9*x^2+1) / ((x-1)^3*(x^2+1)^3). (End)",
				"a(n) = h(n+2) * A109613(n+1).",
				"a(n) = (n+1)*(n+2) * period 4:repeat (1, 1, 2, 2) /2.",
				"From _Wesley Ivan Hurt_, Jul 18 2015: (Start)",
				"a(n) = (n+1)*(n+2)/(3/2+(-1)^((2*n+7+(-1)^n)/4)/2).",
				"a(n) = 3*a(n-1)-6*a(n-2)+10*a(n-3)-12*a(n-4)+12*a(n-5)-10*a(n-6)+6*a(n-7)-3*a(n-8)+a(n-9), n\u003e9. (End)"
			],
			"maple": [
				"A257942:=n-\u003e(n+1)*(n+2)/(3/2+(-1)^((2*n+7+(-1)^n)/4)/2): seq(A257942(n), n=0..100); # _Wesley Ivan Hurt_, Jul 18 2015"
			],
			"mathematica": [
				"CoefficientList[Series[-(x^6 + 9 x^4 - 8 x^3 + 9 x^2 + 1)/((x - 1)^3 (x^2 + 1)^3), {x, 0, 50}], x] (* _Michael De Vlieger_, Jul 14 2015 *)",
				"(* Using inverse binomial transform *) s[0]=1; s[n_] := 1/(n+2); f[n_] := Sum[(-1)^(n-k)*Binomial[n, k]*s[k], {k, 0, n}]; Table[f[n]//Denominator, {n, 0, 50}] (* _Jean-François Alcover_, Jul 14 2015 *)",
				"LinearRecurrence[{3, -6, 10, -12, 12, -10, 6, -3, 1}, {1, 3, 12, 20, 15, 21, 56, 72, 45}, 55] (* _Vincenzo Librandi_, Jul 15 2015 *)"
			],
			"program": [
				"(PARI) Vec(-(x^6+9*x^4-8*x^3+9*x^2+1)/((x-1)^3*(x^2+1)^3) + O(x^100)) \\\\ _Colin Barker_, Jul 14 2015",
				"(PARI) a(n)=(n+1)*(n+2)/if(n%4\u003c2,2,1) \\\\ _Charles R Greathouse IV_, Jul 14 2015"
			],
			"xref": [
				"Cf. A002378(n+1), A014695(n+1)=A130658(n+2), A014634, A033567(n+1), A104188(n+1), 4*A007742(n+1), A160050 (in A226089), A022998, A109613, A000217(n+1), A246943."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Paul Curtz_, Jul 14 2015",
			"references": 1,
			"revision": 48,
			"time": "2017-06-27T14:58:03-04:00",
			"created": "2015-08-14T15:29:31-04:00"
		}
	]
}