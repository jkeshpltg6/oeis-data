{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A333850",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 333850,
			"data": "38,26,95,71,59,103,67,224,176,175,151,115,232,184,303,219,254,170,146,264,204,180,144,405,309,321,261,428,368,284,296,571,511,475,379,600,612,444,538,466,406,1254,1050,763,727,732,516,996,1080,840,952,772,688,724,844,712,556,1488,1392,1336,1144",
			"name": "Irregular triangle read by rows: T(n, k) gives the sums of the members of the primitive period of the unsigned Schick sequences for the odd numbers from A333855.",
			"comment": [
				"For Schick's sequences see comments in A332439. In A333848 the sum for members of the primitive periods of the unsigned Schick sequences SBB(N, q0 = 1) (BB for Brändli and Beyne) for the odd numbers N from A333854 are given. (In Schick's book p is used instead of odd N \u003e= 3, and in A333848 his B(p) = 1).",
				"The length of row n is A135303(A333855(n)) (the B numbers for A333855(n)).",
				"The corresponding gcd(T(n,k), 2*A333855(n)) values are given in A333851. They are used for the formula of the length of the Euler tours ET(A333855(n), q0_k), for k = 1, 2, ..., B(A333855(n)) based on the unsigned Schick sequences."
			],
			"reference": [
				"Carl Schick, Trigonometrie und unterhaltsame Zahlentheorie, Bokos Druck, Zürich, 2003 (ISBN 3-9522917-0-6). Tables 3.1 to 3.10, for odd p = 3..113 (with gaps), pp. 158-166."
			],
			"link": [
				"Gerold Brändli and Tim Beyne, \u003ca href=\"https://arxiv.org/abs/1504.02757\"\u003eModified Congruence Modulo n with Half the Amount of Residues\u003c/a\u003e, arXiv:1504.02757 [math.NT], 2016.",
				"Wolfdieter Lang, \u003ca href=\"https://arxiv.org/abs/2008.04300\"\u003eOn the Equivalence of Three Complete Cyclic Systems of Integers\u003c/a\u003e, arXiv:2008.04300 [math.NT], 2020."
			],
			"formula": [
				"T(n, k) = Sum_{j=1..A003558(A333855(n))} SBB(A333855(n), q0_k)_j, with the unsigned Schick sequence SBB(N, q0) for all used initial values q0 = q0_k for k = 1, 2, ..., A135303(A333855(n)) (B numbers \u003e= 2)."
			],
			"example": [
				"The irregular triangle T(n, k) begins (here A(n) = A333855(n)):",
				"n,  A(n) \\ k   1     2    3    4    5    6    7   8   9 ...",
				"-------------------------------------------------------------",
				"1,   17:      38    26",
				"2,   31:      95    71   59",
				"3,   33:     103    67",
				"4,   41:     224   176",
				"5,   43:     175   151  115",
				"6,   51:     232   184",
				"7,   57:     303   219",
				"8,   63:     254   170  146",
				"9,   65:     264   204  180  144",
				"10,  73:     405   309  321  261",
				"11,  85:     428   368  284  296",
				"12,  89:     571   511  475  379",
				"13,  91:     600   612  444",
				"14,  93:     538   466  406",
				"15,  97:    1254  1050",
				"16,  99:     763   727",
				"17, 105:     732   516",
				"18, 109:     996  1080  840",
				"19, 113:     952   772  688  724",
				"20, 117:     844   712  556",
				"21, 119:    1488  1392",
				"22, 123:    1336  1144",
				"23, 127:     637   517  457  469  433  385  385 361 325",
				"24, 129:     649   469  469  385  397  361",
				"25, 133:    1374  1218 1026",
				"28, 137:    2456  2168",
				"...",
				"--------------------------------------------------------------------------",
				"n = 1, N = 17, B(17) = A135303((17-1)/2) = 2. In cycle notation:",
				"SBB(17, q0_1) = (1, 15, 13, 9) and SBB(17, q0_2) = (3, 11, 5, 7), with sums",
				"T(1, 1) = 1 + 15 + 13 + 9 = 38 and T(1, 2) = 26. (38 + 26 = 64 = A333848(8) .)"
			],
			"program": [
				"(PARI) RRS(n) = select(x-\u003e(((x%2)==1) \u0026\u0026 (gcd(n, x)==1)), [1..n]);",
				"isok8(m, n) = my(md = Mod(2, 2*n+1)^m); (md==1) || (md==-1);",
				"A003558(n) = my(m=1); while(!isok8(m, n) , m++); m;",
				"B(n) = eulerphi(n)/(2*A003558((n-1)/2));",
				"fmiss(rrs, qs) = {for (i=1, #rrs, if (! setsearch(qs, rrs[i]), return (rrs[i])););}",
				"listb(nn) = {my(v=List()); forstep (n=3, nn, 2, my(bn = B(n)); if (bn \u003e= 2, listput(v, n););); Vec(v);}",
				"persum(n) = {my(bn = B(n)); if (bn \u003e= 2, my(vn = vector(bn)); my(q=1, qt = List()); my(p = A003558((n-1)/2)); my(rrs = RRS(n)); for (k=1, bn, my(qp = List()); q = fmiss(rrs, Set(qt)); listput(qp, q); listput(qt, q); for (i=1, p-1, q = abs(n-2*q); listput(qp, q); listput(qt, q);); vn[k] = vecsum(Vec(qp));); return (vn););}",
				"listas(nn) = {my(v = listb(nn)); vector(#v, k, persum(v[k]));} \\\\ _Michel Marcus_, Jun 13 2020"
			],
			"xref": [
				"Cf. A332439, A333848, A333854, A333851, A333855."
			],
			"keyword": "nonn,tabf",
			"offset": "1,1",
			"author": "_Wolfdieter Lang_, Jun 08 2020",
			"ext": [
				"Some terms were corrected by _Michel Marcus_, Jun 11 2010"
			],
			"references": 3,
			"revision": 23,
			"time": "2020-10-15T16:36:37-04:00",
			"created": "2020-06-19T04:54:10-04:00"
		}
	]
}