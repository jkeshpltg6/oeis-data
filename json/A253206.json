{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A253206",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 253206,
			"data": "1,-1,-1,0,-1,1,-1,0,0,1,-1,0,-1,1,1,0,-1,0,-1,0,1,1,-1,0,0,1,0,0,-1,-1,-1,1,1,1,1,0,-1,1,1,0,-1,-1,-1,0,0,1,-1,0,0,0,1,0,-1,0,1,0,1,1,-1,0,-1,1,0,-1,1,-1,-1,0,1,-1,-1,0,-1,1,0,0,1,-1,-1,0,0,1,-1,0,1,1,1,0,-1,0,1,0,1,1,1,-1,-1",
			"name": "Coefficients of the Dirichlet series for zeta(5s)/zeta(s).",
			"comment": [
				"First formula works also for zeta(k*s)/zeta(s), with k\u003e5, thus generating a whole class of sequences.",
				"Multiplicative with a(p^d) = 1 if d == 0 mod 5, -1 if d == 1 mod 5, 0 otherwise. - _Robert Israel_, Mar 27 2015",
				"Inverse Mobius transform gives the characteristic function of 5th powers. - _R. J. Mathar_, Jun 05 2020"
			],
			"link": [
				"Wolfgang Hintze, \u003ca href=\"/A253206/b253206.txt\"\u003eTable of n, a(n) for n = 1..500\u003c/a\u003e",
				"StackExchange, \u003ca href=\"http://mathematica.stackexchange.com/questions/77711/what-are-the-terms-of-the-sequence-generated-by-zeta3s-zetas/77731\"\u003eQuestion 77711\u003c/a\u003e"
			],
			"formula": [
				"a(k,n) = Sum_{d^k divides n} MoebiusMu[n/d^k], k = 5 gives this sequence. - _Wolfgang Hintze_, Mar 27 2015",
				"Dirichlet G.f.: defining relation: zeta(5s)/zeta(s) = Sum_{n\u003e=1} a(n)/n^s.",
				"G.f.: Sum_{n\u003e=1} a(n)*x^n/(1 - x^n) = Sum_{n\u003e=1} x^(n^5)."
			],
			"maple": [
				"g:= [1,-1,0,0,0]:",
				"[seq(mul(g[x[2] mod 5 + 1], x = ifactors(n)[2]),n=1..100)]; # _Robert Israel_, Mar 27 2015"
			],
			"mathematica": [
				"(* Number theoretical approach *)",
				"a[k_,n_]:=Plus@@(MoebiusMu[n/#^k]\u0026/@Select[(Divisors[n])^(1/k),IntegerQ[#]\u0026])",
				"nn=200; Table[a[5,n],{n,1,nn} (* _Wolfgang Hintze_, Mar 27 2015 *)",
				"(* Comparison of \"Dirichlet\"-powers n^-x *)",
				"fqZeta[k_, nn_] := Module[{z, d, x, g, eqs, sol, t, p},",
				"z[x_, p_] := Sum[1/n^x, {n, 1, p}]; d[x_, p_] := Sum[f[n]/n^x, {n, 1, p}];",
				"g[k] = Expand[z[k*x, nn] - z[x, nn]*d[x, nn]] /. (a_)^((c_)*(b_)) -\u003e",
				"Simplify[a^b]^c; eqs[k] =",
				"Table[Simplify[0 == (1/m^(-x))*Plus @@ Cases[g[k], (_.)/m^x]], {m, 2, nn}];",
				"sol[k] = Solve[Join[{f[1] == 1}, eqs[k]]][[1]];",
				"t[k] = Table[f[n], {n, 1, nn}] /. sol[k]]",
				"fqZeta[5,200]",
				"(* Using G.f. (beyond nn = 73 numerical problems appear) *)",
				"nn = 73; f[x_] := Sum[a[n]*(x^n/(1 - x^n)), {n, 1, nn}];",
				"sol = SolveAlways[0 == Series[f[x] - Sum[x^n^5, {n, 1, nn}], {x, 0, nn}], x];",
				"Flatten[Table[a[n], {n, 1, nn}] /. sol]"
			],
			"program": [
				"(PARI) a(n) = sumdiv(n, d, if (ispower(d, 5), moebius(n/d), 0)); \\\\ _Michel Marcus_, Mar 27 2015",
				"(PARI) for(n=1, 100, print1(direuler(p=2, n, (1-X)/(1-X^5))[n], \", \")) \\\\ _Vaclav Kotesovec_, Jun 14 2020"
			],
			"xref": [
				"Cf. A219009 (k=4), A210826 (k=3), A008836 (k=2)."
			],
			"keyword": "sign,mult",
			"offset": "1",
			"author": "_Wolfgang Hintze_, Mar 25 2015",
			"references": 2,
			"revision": 59,
			"time": "2020-06-14T08:10:36-04:00",
			"created": "2015-03-25T18:50:36-04:00"
		}
	]
}