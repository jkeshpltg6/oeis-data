{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A227343",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 227343,
			"data": "1,1,1,3,3,1,13,13,6,1,75,75,37,10,1,541,541,270,85,15,1,4683,4683,2341,770,170,21,1,47293,47293,23646,7861,1890,308,28,1,545835,545835,272917,90930,22491,4158,518,36,1,7087261,7087261,3543630,1181125,294525,57351,8400,822,45,1",
			"name": "Matrix inverse of triangle A227342.",
			"comment": [
				"The e.g.f. has the form A(t)*exp(x*B(t)), where A(t) = 1/(2 - exp(t)) and B(t) = exp(t) - 1. Thus the row polynomials of this triangle form a Sheffer sequence for the pair (1 - t, log(1 + t)) (see Roman, p.17).",
				"Let x_(k) := x*(x-1)*...*(x-k+1) denote the k-th falling factorial polynomial. Define a sequence x_[n] of basis polynomials for the polynomial algebra C[x] by setting x_[0] = 1, and setting x_[n] = x_(n-1)*(x - 2*n + 1) for n \u003e= 1. The sequence begins [1, x-1, x*(x-3), x*(x-1)*(x-5), x*(x-1)*(x-2)*(x-7), ...]. Then this is the triangle of connection constants for expressing the monomial polynomials x^n as a linear combination of the basis x_[k], that is, x^n = sum {k = 0..n} T(n,k)*x_[k]. An example is given below."
			],
			"reference": [
				"S. Roman, The umbral calculus, Pure and Applied Mathematics 111, Academic Press Inc., New York, 1984. Reprinted by Dover in 2005."
			],
			"link": [
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ShefferSequence.html\"\u003eSheffer Sequence\u003c/a\u003e"
			],
			"formula": [
				"E.g.f.: 1/(2 - exp(t))*exp(x*(exp(t) - 1)) = 1 + (1 + x)*t + (3 + 3*x + x^2)*t^2/2! + (13 + 13*x + 6*x^2 + x^3)*t^3/3! + ....",
				"Recurrence equation: T(n,0) = A000670(n), and for k \u003e= 1, T(n,k) = 1/k*sum {i = 1..n} binomial(n,i)*T(n-i,k-1).",
				"The row polynomials R(n,x) satisfy the Sheffer identity R(n,x + y) = sum {k = 0..n} binomial(n,k)*Bell(k,y)*R(n-k,x), where Bell(k,y) is the Bell or exponential polynomial (row polynomials of A048993).",
				"The row polynomials also satisfy d/dx(R(n,x)) = sum {k = 0..n-1} binomial(n,k)*R(k,x).",
				"Row sums A059099. Column 1 and column 2 = A000670. 1 + 2*column 3 = A000670 (apart from the first two terms).",
				"From _Emanuele Munarini_, Dec 21 2016: (Start)",
				"T(n,k) = (n!/k!)*[t^n](1/(2-exp(t))(exp(t)-1)^k.",
				"T(n,k) = (n!/k!)*[t^(n-k)](t/log(1+t))^(n+1)/(1-t^2). (End)"
			],
			"example": [
				"Triangle begins",
				"n\\k|   0    1    2    3    4    5",
				"= = = = = = = = = = = = = = = = =",
				"0 |   1",
				"1 |   1    1",
				"2 |   3    3    1",
				"3 |  13   13    6    1",
				"4 |  75   75   37   10    1",
				"5 | 541  541  270   85   15    1",
				"...",
				"Connection constants. Row 4 = [75,75,37,10,1]: Thus",
				"75 + 75*(x - 1) + 37*x*(x - 3) + 10*x*(x - 1)*(x - 5)+ x*(x - 1)*(x - 2)*(x - 7) = x^4."
			],
			"mathematica": [
				"T[n_, k_] := n!/k! SeriesCoefficient[Series[1/(2 - Exp[t]) (Exp[t] - 1)^k, {t, 0, n}], n]",
				"Flatten[Table[T[n, k], {n, 0, 12}, {k, 0, n}]]",
				"U[n_, k_] := n!/k! SeriesCoefficient[Series[1/(1 - t^2) (t/Log[1 + t])^(n + 1), {t, 0, n - k}], n - k]",
				"Flatten[Table[U[n, k], {n, 0, 8}, {k, 0, n}]] (* _Emanuele Munarini_, Dec 21 2016 *)"
			],
			"xref": [
				"A000670 (columns 1 and 2), A048993, A059099 (row sums), A105794, A227342 (matrix inverse)."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "0,4",
			"author": "_Peter Bala_, Jul 11 2013",
			"references": 2,
			"revision": 10,
			"time": "2016-12-21T07:51:09-05:00",
			"created": "2013-07-11T04:30:08-04:00"
		}
	]
}