{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002945",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2945,
			"id": "M2220",
			"data": "1,3,1,5,1,1,4,1,1,8,1,14,1,10,2,1,4,12,2,3,2,1,3,4,1,1,2,14,3,12,1,15,3,1,4,534,1,1,5,1,1,121,1,2,2,4,10,3,2,2,41,1,1,1,3,7,2,2,9,4,1,3,7,6",
			"name": "Continued fraction for cube root of 2.",
			"reference": [
				"H. P. Robinson, Letter to N. J. A. Sloane, Nov 13 1973.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Harry J. Smith, \u003ca href=\"/A002945/b002945.txt\"\u003eTable of n, a(n) for n = 1..20000\u003c/a\u003e",
				"BCMATH, \u003ca href=\"http://www.numbertheory.org/php/cfrac_nthroot.html\"\u003eContinued fraction expansion of the n-th root of a positive rational\u003c/a\u003e",
				"E. Bombieri and A. J. van der Poorten, \u003ca href=\"https://doi.org/10.1007/978-94-017-1108-1_10\"\u003eContinued fractions of algebraic numbers\u003c/a\u003e, In: Bosma W., van der Poorten A. (eds) Computational Algebra and Number Theory. Mathematics and Its Applications, vol 325.",
				"Ashok Kumar Gupta and Ashok Kumar Mittal, \u003ca href=\"http://arxiv.org/abs/math/0002227\"\u003eBifurcating continued fractions\u003c/a\u003e, arXiv:math/0002227 [math.GM] (2000).",
				"S. Lang and H. Trotter, \u003ca href=\"http://dx.doi.org/10.1515/crll.1972.255.112\"\u003eContinued fractions for some algebraic numbers\u003c/a\u003e, J. Reine Angew. Math. 255 (1972), 112-134.",
				"S. Lang and H. Trotter, \u003ca href=\"/A002945/a002945.pdf\"\u003eContinued fractions for some algebraic numbers\u003c/a\u003e, J. Reine Angew. Math. 255 (1972), 112-134. [Annotated scanned copy]",
				"Herman P. Robinson, \u003ca href=\"/A003116/a003116.pdf\"\u003eLetter to N. J. A. Sloane, Nov 13 1973\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/DelianConstant.html\"\u003eDelian Constant\u003c/a\u003e",
				"G. Xiao, \u003ca href=\"http://wims.unice.fr/~wims/en_tool~number~contfrac.en.html\"\u003eContfrac\u003c/a\u003e",
				"\u003ca href=\"/index/Con#confC\"\u003eIndex entries for continued fractions for constants\u003c/a\u003e"
			],
			"formula": [
				"Bombieri/van der Poorten give a complicated formula:",
				"a(n) = floor((-1)^(n+1)*3*p(n)^2/(q(n)*(p(n)^3-2*q(n)^3)) - q(n-1)/q(n)),",
				"p(n+1) = a(n)*p(n) + p(n-1),",
				"q(n+1) = a(n)*q(n) + q(n-1),",
				"with a(1) = 1, p(1) = 1, q(1) = 0, p(2) = 1, q(2) = 1. - _Robert Israel_, Jul 30 2014"
			],
			"example": [
				"2^(1/3) = 1.25992104989487316... = 1 + 1/(3 + 1/(1 + 1/(5 + 1/(1 + ...))))."
			],
			"maple": [
				"N:= 100: # to get a(1) to a(N)",
				"a[1] := 1: p[1] := 1: q[1] := 0: p[2] := 1: q[2] := 1:",
				"for n from 2 to N do",
				"  a[n] := floor((-1)^(n+1)*3*p[n]^2/(q[n]*(p[n]^3-2*q[n]^3)) - q[n-1]/q[n]);",
				"  p[n+1] := a[n]*p[n] + p[n-1];",
				"  q[n+1] := a[n]*q[n] + q[n-1];",
				"od:",
				"seq(a[i],i=1..N); # _Robert Israel_, Jul 30 2014"
			],
			"mathematica": [
				"ContinuedFraction[Power[2, (3)^-1],70] (* _Harvey P. Dale_, Sep 29 2011 *)"
			],
			"program": [
				"(PARI) allocatemem(932245000); default(realprecision, 21000); x=contfrac(2^(1/3)); for (n=1, 20000, write(\"b002945.txt\", n, \" \", x[n])); \\\\ _Harry J. Smith_, May 08 2009",
				"(MAGMA) ContinuedFraction(2^(1/3)); // _Vincenzo Librandi_, Oct 08 2017"
			],
			"xref": [
				"Cf. A002946, A002947, A002948, A002949, A002580 (decimal expansion)."
			],
			"keyword": "cofr,nonn",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_.",
			"ext": [
				"BCMATH link from Keith R Matthews (keithmatt(AT)gmail.com), Jun 04 2006"
			],
			"references": 19,
			"revision": 62,
			"time": "2017-10-08T02:33:26-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}