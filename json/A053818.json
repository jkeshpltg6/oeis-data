{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A053818",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 53818,
			"data": "1,1,5,10,30,26,91,84,159,140,385,196,650,406,620,680,1496,654,2109,1080,1806,1650,3795,1544,4150,2756,4365,3164,7714,2360,9455,5456,7370,6256,9940,5196,16206,8778,12324,8560,22140,6972,25585",
			"name": "a(n) = Sum_{k=1..n, gcd(n,k) = 1} k^2.",
			"comment": [
				"Equals row sums of triangle A143612. - _Gary W. Adamson_, Aug 27 2008",
				"a(n) = A175505(n) * A023896(n) / A175506(n). For number n \u003e= 1 holds B(n) = a(n) / A023896(n) = A175505(n) / A175506(n), where B(n) = antiharmonic mean of numbers k such that GCD(k, n) = 1 for k \u003c n. - _Jaroslav Krizek_, Aug 01 2010",
				"n does not divide a(n) iff n is a term in A316860, that is, either n is a power of 2 or n is a multiple of 3 and no prime factor of n is congruent to 1 mod 3. - _Jianing Song_, Jul 16 2018"
			],
			"reference": [
				"T. M. Apostol, Introduction to Analytic Number Theory, Springer-Verlag, 1976, page 48, problem 15, the function phi_2(n).",
				"L. Comtet, Advanced Combinatorics, Reidel, 1974, p. 199, #2."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A053818/b053818.txt\"\u003eTable of n, a(n) for n = 1..2500\u003c/a\u003e",
				"John D. Baum, \u003ca href=\"https://www.jstor.org/stable/2690056\"\u003eA Number-Theoretic Sum\u003c/a\u003e, Mathematics Magazine 55.2 (1982): 111-113.",
				"P. G. Brown, \u003ca href=\"http://www.jstor.org/stable/3621931\"\u003eSome comments on inverse arithmetic functions\u003c/a\u003e, Math. Gaz. 89 (516) (2005) 403-408.",
				"Constantin M. Petridi, \u003ca href=\"https://arxiv.org/abs/1612.07632\"\u003eThe Sums of the k-powers of the Euler set and their connection with Artin's conjecture for primitive roots\u003c/a\u003e, arXiv:1612.07632 [math.NT], 2016."
			],
			"formula": [
				"If n = p_1^e_1 * ... *p_r^e_r then a(n) = n^2*phi(n)/3 + (-1)^r*p_1*..._p_r*phi(n)/6.",
				"a(n) = n^2*A000010(n)/3 + n*A023900(n)/6, n\u003e1. [Brown]",
				"a(n) = A000010(n)/3 * (n^2 + (-1)^A001221(n)*A007947(n)/2)) for n\u003e=2. - _Jaroslav Krizek_, Aug 24 2010",
				"G.f. A(x) satisfies: A(x) = x*(1 + x)/(1 - x)^4 - Sum_{k\u003e=2} k^2 * A(x^k). - _Ilya Gutkovskiy_, Mar 29 2020"
			],
			"maple": [
				"A053818 := proc(n)",
				"    local a,k;",
				"    a := 0 ;",
				"    for k from 1 to n do",
				"        if igcd(k,n) = 1 then",
				"            a := a+k^2 ;",
				"        end if;",
				"    end do:",
				"    a ;",
				"end proc: # _R. J. Mathar_, Sep 26 2013"
			],
			"mathematica": [
				"a[n_] := Plus @@ (Select[ Range@n, GCD[ #, n] == 1 \u0026]^2); Array[a, 43] (* _Robert G. Wilson v_, Jul 01 2010 *)"
			],
			"program": [
				"(PARI) a(n) = sum(k=1, n, k^2*(gcd(n,k) == 1)); \\\\ _Michel Marcus_, Jan 30 2016"
			],
			"xref": [
				"Cf. A023896, A053819, A053820.",
				"Cf. A143612. - _Gary W. Adamson_, Aug 27 2008",
				"Cf. A179871-A179880, A179882-A179887, A179890, A179891, A007645, A003627, A034934. - _Jaroslav Krizek_, Aug 01 2010",
				"Column k=2 of A308477."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_, Apr 07 2000",
			"references": 28,
			"revision": 55,
			"time": "2020-03-29T09:30:15-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}