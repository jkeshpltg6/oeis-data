{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A211270",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 211270,
			"data": "0,1,1,1,1,2,1,2,2,2,1,3,1,2,3,2,1,4,1,3,3,2,1,4,2,2,3,3,1,5,1,3,3,2,3,5,1,2,3,4,1,5,1,3,5,2,1,5,2,4,3,3,1,5,3,4,3,2,1,7,1,2,5,3,3,5,1,3,3,5,1,7,1,2,5,3,3,5,1,5,4,2,1,7,3,2,3,4,1,8,3,3,3,2,3,6,1,4,5",
			"name": "Number of integer pairs (x,y) such that 0\u003cx\u003c=y\u003c=n and x*y=2n.",
			"comment": [
				"For a guide to related sequences, see A211266."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A211270/b211270.txt\"\u003eTable of n, a(n) for n = 1..65537\u003c/a\u003e"
			],
			"formula": [
				"a(n) = floor((A000005(2n)-1)/2). - _Robert Israel_, Feb 25 2019"
			],
			"example": [
				"a(12) counts these pairs: (2,12), (3,8), (4,6).",
				"For n = 2, only the pair (2,2) satisfies the condition, thus a(2) = 1. - _Antti Karttunen_, Sep 30 2018"
			],
			"maple": [
				"seq(floor((numtheory:-tau(2*n)-1)/2),n=1..100); # _Robert Israel_, Feb 25 2019"
			],
			"mathematica": [
				"a = 1; b = n; z1 = 120;",
				"t[n_] :=  t[n] = Flatten[Table[x*y, {x, a, b - 1},",
				"{y, x, b}]]",
				"c[n_, k_] := c[n, k] = Count[t[n], k]",
				"Table[c[n, n], {n, 1, z1}]           (* A038548 *)",
				"Table[c[n, n + 1], {n, 1, z1}]       (* A072670 *)",
				"Table[c[n, 2*n], {n, 1, z1}]         (* A211270 *)",
				"Table[c[n, 3*n], {n, 1, z1}]         (* A211271 *)",
				"Table[c[n, Floor[n/2]], {n, 1, z1}]  (* A211272 *)",
				"c1[n_, m_] := c1[n, m] = Sum[c[n, k], {k, a, m}]",
				"Print",
				"Table[c1[n, n], {n, 1, z1}]          (* A094820 *)",
				"Table[c1[n, n + 1], {n, 1, z1}]      (* A091627 *)",
				"Table[c1[n, 2*n], {n, 1, z1}]        (* A211273 *)",
				"Table[c1[n, 3*n], {n, 1, z1}]        (* A211274 *)",
				"Table[c1[n, Floor[n/2]], {n, 1, z1}] (* A211275 *)"
			],
			"program": [
				"(PARI) A211270(n) = sumdiv(2*n,y,(((2*n/y)\u003c=y)\u0026\u0026(y\u003c=n))); \\\\ _Antti Karttunen_, Sep 30 2018"
			],
			"xref": [
				"Cf. A000005, A211266, A211261."
			],
			"keyword": "nonn",
			"offset": "1,6",
			"author": "_Clark Kimberling_, Apr 07 2012",
			"ext": [
				"Term a(2) corrected by _Antti Karttunen_, Sep 30 2018"
			],
			"references": 8,
			"revision": 15,
			"time": "2019-02-25T10:46:01-05:00",
			"created": "2012-04-11T18:50:17-04:00"
		}
	]
}