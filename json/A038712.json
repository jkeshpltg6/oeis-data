{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A038712",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 38712,
			"data": "1,3,1,7,1,3,1,15,1,3,1,7,1,3,1,31,1,3,1,7,1,3,1,15,1,3,1,7,1,3,1,63,1,3,1,7,1,3,1,15,1,3,1,7,1,3,1,31,1,3,1,7,1,3,1,15,1,3,1,7,1,3,1,127,1,3,1,7,1,3,1,15,1,3,1,7,1,3,1,31,1,3,1,7,1,3,1,15,1,3,1,7,1,3,1,63,1,3",
			"name": "Let k be the exponent of highest power of 2 dividing n (A007814); a(n) = 2^(k+1)-1.",
			"comment": [
				"n XOR n-1, i.e., nim-sum of a pair of consecutive numbers.",
				"Denominator of quotient sigma(2*n)/sigma(n). - _Labos Elemer_, Nov 04 2003",
				"a(n) = the Towers of Hanoi disc moved at the n-th move, using standard moves with discs labeled (1, 3, 7, 15, ...) starting from top (smallest = 1). - _Gary W. Adamson_, Oct 26 2009",
				"Equals row sums of triangle A168312. - _Gary W. Adamson_, Nov 22 2009",
				"In the binary expansion of n, delete everything left of the rightmost 1 bit, and set all bits to the right of it. - _Ralf Stephan_, Aug 22 2013",
				"Every finite sequence of positive integers summing to n may be termwise dominated by a subsequence of the first n values in this sequence [see Bannister et al., 2013]. - _David Eppstein_, Aug 31 2013",
				"Sum of powers of 2 dividing n. - _Omar E. Pol_, Aug 18 2019",
				"Given the binary expansion of (n-1) as {b[k-1], b[k-2], ..., b[2], b[1], b[0]}, then the binary expansion of a(n) is {bitand(b[k-1], b[k-2], ..., b[2], b[1], b[0]), bitand(b[k-2], ..., b[2], b[1], b[0]), ..., bitand(b[2], b[1], b[0]), bitand(b[1], b[0]), b[0], 1}. Recursively stated - 0th bit (L.S.B) of a(n), a(n)[0] = 1, a(n)[i] = bitand(a(n)[i-1], (n-1)[i-1]), where n[i] = i-th bit in the binary expansion of n. - _Chinmaya Dash_, Jun 27 2020"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A038712/b038712.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"M. J. Bannister, Z. Cheng, W. E. Devanny, and D. Eppstein, \u003ca href=\"http://arxiv.org/abs/1308.0403\"\u003eSuperpatterns and universal point sets\u003c/a\u003e, 21st Int. Symp. Graph Drawing, 2013, arXiv:1308.0403 [cs.CG], 2013.",
				"Klaus Brockhaus, \u003ca href=\"/A038712/a038712.gif\"\u003eIllustration of A038712 and A080277\u003c/a\u003e",
				"D. Eppstein, \u003ca href=\"https://11011110.github.io/blog/2013/08/04/1317131-and-majorization.html\"\u003e1317131 and majorization by subsequences\u003c/a\u003e",
				"Fabrizio Frati, M. Patrignani, V. Roselli, \u003ca href=\"https://arxiv.org/abs/1610.02841\"\u003eLR-Drawings of Ordered Rooted Binary Trees and Near-Linear Area Drawings of Outerplanar Graphs\u003c/a\u003e, arXiv preprint arXiv:1610.02841 [cs.CG], 2016.",
				"Malgorzata J. Krawczyk, Paweł Oświęcimka, Krzysztof Kułakowski, \u003ca href=\"https://doi.org/10.3390/e21100968\"\u003eOrdered Avalanches on the Bethe Lattice\u003c/a\u003e, Entropy (2019) Vol. 21, 968.",
				"Ralf Stephan, \u003ca href=\"/somedcgf.html\"\u003eSome divide-and-conquer sequences ...\u003c/a\u003e",
				"Ralf Stephan, \u003ca href=\"/A079944/a079944.ps\"\u003eTable of generating functions\u003c/a\u003e",
				"Ralf Stephan, \u003ca href=\"https://arxiv.org/abs/math/0307027\"\u003eDivide-and-conquer generating functions. I. Elementary sequences\u003c/a\u003e, arXiv:math/0307027 [math.CO], 2003.",
				"\u003ca href=\"/index/Ni#Nimsums\"\u003eIndex entries for sequences related to Nim-sums\u003c/a\u003e",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A110654(n-1) XOR A008619(n). - _Reinhard Zumkeller_, Feb 05 2007",
				"a(n) = 2^A001511(n) - 1 = 2*A006519(n) - 1 = 2^(A007814(n)+1) - 1.",
				"Multiplicative with a(2^e) = 2^(e+1)-1, a(p^e) = 1, p \u003e 2. - _Vladeta Jovovic_, Nov 06 2001; corrected by _Jianing Song_, Aug 03 2018",
				"Sum_{n\u003e0} a(n)*x^n/(1+x^n) = Sum_{n\u003e0} x^n/(1-x^n). Inverse Moebius transform of A048298. - _Vladeta Jovovic_, Jan 02 2003",
				"From _Ralf Stephan_, Jun 15 2003: (Start)",
				"G.f.: Sum_{k\u003e=0} 2^k*x^2^k/(1 - x^2^k).",
				"a(2*n+1) = 1, a(2*n) = 2*a(n)+1. (End)",
				"Equals A130093 * [1, 2, 3, ...]. - _Gary W. Adamson_, May 13 2007",
				"Sum_{i=1..n} (-1)^A000120(n-i)*a(i) = (-1)^(A000120(n)-1)*n. - _Vladimir Shevelev_, Mar 17 2009",
				"Dirichlet g.f.: zeta(s)/(1 - 2^(1-s)). - _R. J. Mathar_, Mar 10 2011",
				"a(n) = A086799(2*n) - 2*n. - _Reinhard Zumkeller_, Aug 07 2011",
				"a((2*n-1)*2^p) = 2^(p+1)-1, p \u003e= 0. - _Johannes W. Meijer_, Feb 01 2013",
				"a(n) = A000225(A001511(n)). - _Omar E. Pol_, Aug 31 2013",
				"a(n) = A000203(n)/A000593(n). - _Ivan N. Ianakiev_ and _Omar E. Pol_, Dec 14 2017",
				"L.g.f.: -log(Product_{k\u003e=0} (1 - x^(2^k))) = Sum_{n\u003e=1} a(n)*x^n/n. - _Ilya Gutkovskiy_, Mar 15 2018",
				"a(n) = 2^(1 + (A183063(n)/A001227(n))) - 1. - _Omar E. Pol_, Nov 06 2018"
			],
			"example": [
				"a(6) = 3 because 110 XOR 101 = 11 base 2 = 3.",
				"From _Omar E. Pol_, Aug 18 2019: (Start)",
				"Illustration of initial terms:",
				"a(n) is also the area of the n-th region of an infinite diagram of compositions (ordered partitions) of the positive integers, where the length of the n-th horizontal line segment is equal to A001511(n) and the length of the n-th vertical line segment is equal to A006519(n), as shown below (first eight regions):",
				"-----------------------------",
				"n    a(n)    Diagram",
				"-----------------------------",
				".            _ _ _ _",
				"1     1     |_| | | |",
				"2     3     |_ _| | |",
				"3     1     |_|   | |",
				"4     7     |_ _ _| |",
				"5     1     |_| |   |",
				"6     3     |_ _|   |",
				"7     1     |_|     |",
				"8    15     |_ _ _ _|",
				".",
				"The above diagram represents the eight compositions of 4: [1,1,1,1],[2,1,1],[1,2,1],[3,1],[1,1,2],[2,2],[1,3],[4].",
				"(End)"
			],
			"maple": [
				"nmax:=98: for p from 0 to ceil(simplify(log[2](nmax))) do for n from 1 to ceil(nmax/(p+2)) do a((2*n-1)*2^p) := 2^(p+1)-1 od: od: seq(a(n), n=1..nmax); # _Johannes W. Meijer_, Feb 01 2013"
			],
			"mathematica": [
				"Table[Denominator[DivisorSigma[1, 2*n]/DivisorSigma[1, n]], {n, 1, 128}]",
				"Table[BitXor[(n + 1), n], {n, 0, 100}] (* _Vladimir Joseph Stephan Orlovsky_, Jul 19 2011 *)"
			],
			"program": [
				"(C) int a(int n) { return n ^ (n-1); } // _Russ Cox_, May 15 2007",
				"(Haskell)",
				"import Data.Bits (xor)",
				"a038712 n = n `xor` (n - 1) :: Integer  -- _Reinhard Zumkeller_, Apr 23 2012",
				"(PARI) vector(66,n,bitxor(n-1,n)) \\\\ _Joerg Arndt_, Sep 01 2013; corrected by _Michel Marcus_, Aug 02 2018"
			],
			"xref": [
				"A038713 translated from binary, diagonals of A003987 on either side of main diagonal.",
				"Cf. A062383. Partial sums give A080277.",
				"Bisection of A089312. Cf. A088837.",
				"a(n)-1 is exponent of 2 in A089893(n).",
				"Cf. A130093.",
				"This is Guy Steele's sequence GS(6, 2) (see A135416).",
				"Cf. A168312, A220466."
			],
			"keyword": "easy,nonn,mult",
			"offset": "1,2",
			"author": "_Henry Bottomley_, May 02 2000",
			"ext": [
				"Definition corrected by _N. J. A. Sloane_, Sep 07 2015 at the suggestion of _Marc LeBrun_",
				"Name corrected by _Wolfdieter Lang_, Aug 30 2016"
			],
			"references": 61,
			"revision": 133,
			"time": "2020-07-26T02:40:14-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}