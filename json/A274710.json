{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A274710",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 274710,
			"data": "1,1,0,2,0,0,6,0,2,2,2,0,0,6,12,12,0,2,4,8,4,2,0,0,6,24,52,40,18,0,2,6,18,18,18,6,2,0,0,6,36,120,180,180,84,24,0,2,8,32,48,72,48,32,8,2,0,0,6,48,216,480,744,672,432,144,30,0,2,10,50,100,200,200,200,100,50,10,2",
			"name": "A statistic on orbital systems over n sectors: the number of orbitals which make k turns.",
			"comment": [
				"The definition of an orbital system is given in A232500 (see also the illustration there). The number of orbitals over n sectors is counted by the swinging factorial A056040.",
				"A 'turn' of an orbital w takes place where signum(w[i]) is not equal to signum(w[i+1]).",
				"A152659 is a subtriangle."
			],
			"link": [
				"Peter Luschny, \u003ca href=\"https://oeis.org/wiki/User:Peter_Luschny/Orbitals\"\u003eOrbitals\u003c/a\u003e"
			],
			"formula": [
				"For even n\u003e0: T(n,k) = 2*C(n/2-1,(k-1+mod(k-1,2))/2)*C(n/2-1,(k-1-mod(k-1,2))/2) for k=0..n-1 (from A152659)."
			],
			"example": [
				"Triangle read by rows, n\u003e=0. The length of row n is n for n\u003e=1.",
				"[n] [k=0,1,2,...]                      [row sum]",
				"[0] [1]                                    1",
				"[1] [1]                                    1",
				"[2] [0, 2]                                 2",
				"[3] [0, 0, 6]                              6",
				"[4] [0, 2, 2,  2]                          6",
				"[5] [0, 0, 6, 12,  12]                    30",
				"[6] [0, 2, 4,  8,   4,   2]               20",
				"[7] [0, 0, 6, 24,  52,  40,  18]         140",
				"[8] [0, 2, 6, 18,  18,  18,   6,  2]      70",
				"[9] [0, 0, 6, 36, 120, 180, 180, 84, 24] 630",
				"T(5,2) = 6 because the six orbitals [-1, -1, 0, 1, 1], [-1, -1, 1, 1, 0], [0, -1, -1, 1, 1], [0, 1, 1, -1, -1], [1, 1, -1, -1, 0], [1, 1, 0, -1, -1] make 2 turns."
			],
			"program": [
				"(Sage) # uses[unit_orbitals from A274709]",
				"# Brute force counting",
				"def orbital_turns(n):",
				"    if n == 0: return [1]",
				"    S = [0]*(n)",
				"    for u in unit_orbitals(n):",
				"        L = sum(0 if sgn(u[i]) == sgn(u[i+1]) else 1 for i in (0..n-2))",
				"        S[L] += 1",
				"    return S",
				"for n in (0..12): print(orbital_turns(n))"
			],
			"xref": [
				"Cf. A056040 (row sum), A152659, A232500.",
				"Other orbital statistics: A241477 (first zero crossing), A274706 (absolute integral), A274708 (number of peaks), A274709 (max. height), A274878 (span), A274879 (returns), A274880 (restarts), A274881 (ascent)."
			],
			"keyword": "nonn,tabf",
			"offset": "0,4",
			"author": "_Peter Luschny_, Jul 10 2016",
			"references": 10,
			"revision": 26,
			"time": "2020-03-27T06:57:42-04:00",
			"created": "2016-07-18T05:32:03-04:00"
		}
	]
}