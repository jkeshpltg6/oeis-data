{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A280547",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 280547,
			"data": "4,7,3,14,1,23,3,7,2,2,1,75,3,7,3,36,1,2476,1,1,2,165,1,14,4,7,3,149,1,2972,3,2,4,14,1,977,4,5,1,34,1,135,2,7,4,136,1,23,2,7,2,11,1,2,3,2,4",
			"name": "Smallest number k such that (k+1)^n - k^n is divisible by a square greater than 1.",
			"comment": [
				"a(31) \u003e 2882.",
				"a(31) \u003c= 2972 (since (2972+1)^31 - 2972^31 is divisible by 1489^2). - _Jon E. Schoenfield_, Jan 20 2017",
				"From _Jon E. Schoenfield_, Jan 22 2017: (Start)",
				"Observation: let f(n,k) = (k+1)^n - k^n; then, for each n \u003c= 100, there exists a value of k \u003c= 3735 and a prime p \u003c= 1489 such that p^2 | f(n,k), and the smallest value of k (for a given n) at which any prime-squared divisor p^2 of f(n,k) is found establishes an upper bound on a(n). For n \u003c= 100, there is no prime p in [1490..10^6] whose square divides f(n,k) at a smaller value of k than the upper bound described above; values of that upper bound, for n = 31..100, are 2972, 3, 2, 4, 14, 1, 977, 4, 5, 1, 34, 1, 135, 2, 7, 4, 136, 1, 23, 2, 7, 2, 11, 1, 2, 3, 2, 4, 1155, 1, 3735, 4, 1, 3, 14, 1, 1068, 3, 7, 2, 715, 1, 415, 4, 7, 3, 2, 1, 533, 1, 7, 4, 509, 1, 14, 4, 7, 2, 23, 1, 23, 3, 7, 4, 14, 1, 1550, 3, 2, 1.",
				"Conjecture: for n \u003c= 100, there is no square greater than (10^6)^2 = 10^12 that divides (k+1)^n - k^n at any value of k lower than the upper bound described above; i.e., the upper bound described above is equal to a(n) for each n \u003c= 100. (End)",
				"Confirmed Jon E. Schoenfield's conjecture through a(58). - _Robert Price_, Feb 04 2017",
				"If p is a prime that divides (k+1)^n - k^n for some k but does not divide n, then by Hensel's lemma there is some k for which p^2 divides (k+1)^n - k^n.  In particular, all terms exist. - _Robert Israel_, Feb 08 2017"
			],
			"example": [
				"a(2) = 4 because (4+1)^2 - 4^2 = 9 is a square."
			],
			"mathematica": [
				"A280547 = {};",
				"For[n = 2, n \u003c 11, n++,",
				"    k = 0;",
				"    While[SquareFreeQ[(k + 1)^n - k^n], k++];",
				"    AppendTo[A280547, k]];",
				"A280547 (* _Robert Price_, Feb 04 2017 *)"
			],
			"program": [
				"(PARI) a(n) = {my(k = 1); while (issquarefree((k+1)^n - k^n), k++); k;} \\\\ _Michel Marcus_, Jan 14 2017"
			],
			"xref": [
				"Cf. A280302, A281996, A282174.",
				"Cf. A289629, A289985."
			],
			"keyword": "nonn,more",
			"offset": "2,1",
			"author": "_Juri-Stepan Gerasimov_, Jan 06 2017",
			"ext": [
				"a(19)-a(30) from _Lars Blomberg_, Jan 14 2017",
				"a(31)-a(58) from _Robert Price_, Feb 04 2017"
			],
			"references": 8,
			"revision": 56,
			"time": "2021-12-07T11:10:15-05:00",
			"created": "2017-01-08T03:46:58-05:00"
		}
	]
}