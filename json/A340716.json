{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A340716",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 340716,
			"data": "1,2,2,3,2,3,4,2,3,5,4,2,3,5,6,4,2,7,3,5,6,4,8,2,7,9,3,5,6,4,10,8,2,7,9,3,5,11,6,4,12,10,8,2,7,9,3,13,5,11,6,4,14,12,10,15,8,2,7,9,16,3,13,5,11,6,4,17,14,12,10,15,8,2,18,7,9,19,16,3",
			"name": "Lexicographically earliest sequence of positive integers with as many distinct values as possible such that for any n \u003e 0, a(n + pi(n)) = a(n) (where pi(n) = A000720(n) corresponds to the number of prime numbers \u003c= n).",
			"comment": [
				"The condition \"with as many distinct values as possible\" means here that for any distinct m and n, provided the orbits of m and n under the map x -\u003e x + pi(x) do not merge, then a(m) \u003c\u003e a(n).",
				"This sequence has similarities with A003602 (A003602(2*n) = A003602(n)) and with A163491 (A163491(n+ceiling(n/2)) = A163491(n))."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A340716/b340716.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 2 iff n belongs to A061535.",
				"a(A095116(n)) = n + 1."
			],
			"example": [
				"The first terms, alongside n + pi(n), are:",
				"  n   a(n)  n + pi(n)",
				"  --  ----  ---------",
				"   1     1          1",
				"   2     2          3",
				"   3     2          5",
				"   4     3          6",
				"   5     2          8",
				"   6     3          9",
				"   7     4         11",
				"   8     2         12",
				"   9     3         13",
				"  10     5         14",
				"  11     4         16",
				"  12     2         17"
			],
			"program": [
				"(PARI) u=0; for (n=1, #a=vector(80), if (a[n]==0, a[n]=u++); print1 (a[n]\", \"); m=n+primepi(n); if (m\u003c=#a, a[m]=a[n]))"
			],
			"xref": [
				"See A003602, A163491 and A340717 for similar sequences.",
				"Cf. A000720, A095117."
			],
			"keyword": "nonn,look",
			"offset": "1,2",
			"author": "_Rémy Sigrist_, Jan 17 2021",
			"references": 2,
			"revision": 11,
			"time": "2021-01-22T14:17:29-05:00",
			"created": "2021-01-21T22:22:21-05:00"
		}
	]
}