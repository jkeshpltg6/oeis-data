{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A017904",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 17904,
			"data": "1,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,2,3,4,5,6,7,8,9,10,11,13,16,20,25,31,38,46,55,65,76,89,105,125,150,181,219,265,320,385,461,550,655,780,930,1111,1330,1595,1915,2300,2761,3311,3966,4746,5676",
			"name": "Expansion of 1/(1 - x^10 - x^11 - ...).",
			"comment": [
				"A Lamé sequence of higher order.",
				"a(n) = number of compositions of n in which each part is \u003e=10. - _Milan Janjic_, Jun 28 2010",
				"a(n+19) equals the number of binary words of length n having at least 9 zeros between every two successive ones. - _Milan Janjic_, Feb 09 2015"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A017904/b017904.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"J. Hermes, \u003ca href=\"http://dx.doi.org/10.1007/BF01446684\"\u003eAnzahl der Zerlegungen einer ganzen rationalen Zahl in Summanden\u003c/a\u003e, Math. Ann., 45 (1894), 371-380.",
				"Augustine O. Munagi, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL21/Munagi/munagi10.html\"\u003eInteger Compositions and Higher-Order Conjugation\u003c/a\u003e, J. Int. Seq., Vol. 21 (2018), Article 18.8.5.",
				"\u003ca href=\"/index/Rec#order_10\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1, 0, 0, 0, 0, 0, 0, 0, 0, 1)."
			],
			"formula": [
				"G.f.: (x-1)/(x-1+x^10). - _Alois P. Heinz_, Aug 04 2008",
				"For positive integers n and k such that k \u003c= n \u003c= 10*k, and 9 divides n-k, define c(n,k) = binomial(k,(n-k)/9), and c(n,k) = 0, otherwise. Then, for n\u003e= 1, a(n+10) = sum(c(n,k), k=1..n). - _Milan Janjic_, Dec 09 2011"
			],
			"maple": [
				"f := proc(r) local t1,i; t1 := []; for i from 1 to r do t1 := [op(t1),0]; od: for i from 1 to r+1 do t1 := [op(t1),1]; od: for i from 2*r+2 to 50 do t1 := [op(t1),t1[i-1]+t1[i-1-r]]; od: t1; end; # set r = order",
				"a:= n-\u003e (Matrix(10, (i,j)-\u003e if (i=j-1) then 1 elif j=1 then [1, 0$8, 1][i] else 0 fi)^n)[10,10]: seq(a(n), n=0..80); # _Alois P. Heinz_, Aug 04 2008"
			],
			"mathematica": [
				"LinearRecurrence[{1, 0, 0, 0, 0, 0, 0, 0, 0, 1}, {1, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 80] (* _Vladimir Joseph Stephan Orlovsky_, Feb 17 2012 *)"
			],
			"program": [
				"(PARI) a(n)=([0,1,0,0,0,0,0,0,0,0; 0,0,1,0,0,0,0,0,0,0; 0,0,0,1,0,0,0,0,0,0; 0,0,0,0,1,0,0,0,0,0; 0,0,0,0,0,1,0,0,0,0; 0,0,0,0,0,0,1,0,0,0; 0,0,0,0,0,0,0,1,0,0; 0,0,0,0,0,0,0,0,1,0; 0,0,0,0,0,0,0,0,0,1; 1,0,0,0,0,0,0,0,0,1]^n)[1,1] \\\\ _Charles R Greathouse IV_, Oct 03 2016"
			],
			"xref": [
				"For Lamé sequences of orders 1 through 9 see A000045, A000930, A017898-A017903, and this one."
			],
			"keyword": "nonn,easy",
			"offset": "0,21",
			"author": "_N. J. A. Sloane_",
			"references": 12,
			"revision": 47,
			"time": "2019-01-23T19:24:19-05:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}