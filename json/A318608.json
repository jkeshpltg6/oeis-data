{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A318608",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 318608,
			"data": "1,0,-1,0,1,0,-1,0,0,0,-1,0,1,0,-1,0,1,0,-1,0,1,0,-1,0,0,0,0,0,1,0,-1,0,1,0,-1,0,1,0,-1,0,1,0,-1,0,0,0,-1,0,0,0,-1,0,1,0,-1,0,1,0,-1,0,1,0,0,0,1,0,-1,0,1,0,-1,0,1,0,0,0,1,0,-1,0,0,0,-1,0,1,0,-1,0,1,0,-1,0,1,0,-1,0,1,0,0,0,1,0,-1,0,1,0,-1,0",
			"name": "Moebius function mu(n) defined for the Gaussian integers.",
			"comment": [
				"Just like the original Moebius function over the integers, a(n) = 0 if n has a squared Gaussian prime factor, otherwise (-1)^t if n is a product of a Gaussian unit and t distinct Gaussian prime factors.",
				"a(n) = 0 for even n since 2 = -i*(1 + i)^2 contains a squared factor. For rational primes p == 1 (mod 4), p is always factored as (x + y*i)(x - y*i), x + y*i and x - y*i are not associated so a(p) = (-1)*(-1) = 1.",
				"Interestingly, a(n) and A091069(n) have the same absolute value (= |A087003(n)|), since the discriminants of the quadratic fields Q[i] and Q[sqrt(2)] are -4 and 8 respectively, resulting in Q[i] and Q[sqrt(2)] being two of the three quadratic fields with discriminant a power of 2 or negated (the other one being Q[sqrt(-2)] with discriminant -8)."
			],
			"link": [
				"Jianing Song, \u003ca href=\"/A318608/b318608.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Gaussian_integer\"\u003eGaussian integer\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 0 if n even or has a square prime factor, otherwise Product_{p divides n} (2 - (p mod 4)) where the product is taken over the primes.",
				"Multiplicative with a(p^e) = 0 if p = 2 or e \u003e 1, a(p) = 1 if p == 1 (mod 4) and -1 if p == 3 (mod 4).",
				"a(n) = 0 if A078458(n) != A086275(n), otherwise (-1)^A086275(n).",
				"a(n) = A103226(n,0) = A103226(0,n).",
				"For squarefree n, a(n) = Kronecker symbol (-4, n) = A101455(n). Also for these n, a(n) = A091069(n) if n even or n == 1 (mod 8), otherwise -A091069(n)."
			],
			"example": [
				"a(15) = -1 because 15 is factored as 3*(2 + i)*(2 - i) with three distinct Gaussian prime factors.",
				"a(21) = (-1)*(-1) = 1 because 21 = 3*7 where 3 and 7 are congruent to 3 mod 4 (thus being Gaussian primes)."
			],
			"mathematica": [
				"f[p_, e_] := If[p == 2 || e \u003e 1, 0, Switch[Mod[p, 4], 1, 1, 3, -1]]; a[n_] := Times @@ f @@@ FactorInteger[n]; Array[a, 100] (* _Amiram Eldar_, Feb 10 2020 *)"
			],
			"program": [
				"(PARI)",
				"a(n)=",
				"{",
				"    my(r=1, f=factor(n));",
				"    for(j=1, #f[, 1], my(p=f[j, 1], e=f[j, 2]);",
				"        if(p==2||e\u003e=2, r=0);",
				"        if(Mod(p,4)==3\u0026e==1, r*=-1);",
				"    );",
				"    return(r);",
				"}"
			],
			"xref": [
				"Absolute values are the same as those of A087003.",
				"First row and column of A103226.",
				"Cf. A008683 (original Moebius function over the integers), A091069 (Moebius function over Z[sqrt(2)]).",
				"Cf. A101455.",
				"Equivalent of arithmetic functions in the ring of Gaussian integers (the corresponding functions in the ring of integers are in the parentheses): A062327 (\"d\", A000005), A317797 (\"sigma\", A000203), A079458 (\"phi\", A000010), A227334 (\"psi\", A002322), A086275 (\"omega\", A001221), A078458 (\"Omega\", A001222), this sequence (\"mu\", A008683)."
			],
			"keyword": "sign,easy,mult",
			"offset": "1,1",
			"author": "_Jianing Song_, Aug 30 2018",
			"references": 6,
			"revision": 37,
			"time": "2020-02-10T06:14:47-05:00",
			"created": "2018-09-02T12:32:44-04:00"
		}
	]
}