{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A275666",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 275666,
			"data": "2,3,6,2,5,5,10,3,5,5,6,10,2,4,6,12,3,3,4,12,4,5,5,6,10,12,2,7,7,7,14,3,6,7,7,7,14,4,6,7,7,7,12,14,5,5,7,7,7,10,14,2,3,10,15,2,4,10,12,15,2,5,6,15,15,3,3,5,15,15,3,3,6,10,15,3,5,5,5,15,3,4",
			"name": "Multisets of numbers such that the sum of reciprocals is 1 and each element e occurs at most lpf(e) - 1 times.",
			"comment": [
				"lpf(n) gives the least prime factor of n (A020639(n)).",
				"The multisets are ordered primarily by largest element, then secondly by length, then thirdly by smallest distinct element.",
				"Sets are placed in nondecreasing order. The separation between two sets is where an element a(n) \u003e a(n + 1).",
				"Let M be the largest element of a multiset. No primes \u003e= (M + 1)/2 are in that multiset.",
				"This sequence may be used to find multisets that have the sum of reciprocals an integer, along with the following operations.",
				"In a set we can replace a number by twice its double. For example, in (2, 3, 6), we can replace the 6 by two twelves, giving {2, 3, 12, 12}. The sum of reciprocals is still 1. However 12 occurs twice in the set, more than lpf(12) - 1 = 1.",
				"{3, 4, 4, 6} isn't included as we could replace two fours by one 2 to get {2, 3, 6}. To exclude such possibilities, each element e is in a multiset at most lpf(e) - 1.",
				"We can also take the union of two sets, for example the sets {2, 3, 6} and {2, 5, 5, 10} giving (2, 2, 3, 5, 5, 6, 10} which has sum of reciprocals 1 + 1 = 2.",
				"We can put 1 in a set, so from {2, 3, 6} we can find {1, 2, 3, 6}.",
				"Using any such operations, we get out of the sequence but more terms of A034708 can be found and A020473 may be investigated further."
			],
			"example": [
				"{2, 3, 6}",
				"{2, 5, 5, 10}",
				"{3, 5, 5, 6, 10}",
				"{2, 4, 6, 12}",
				"{3, 3, 4, 12}",
				"{4, 5, 5, 6, 10, 12}",
				"{2, 7, 7, 7, 14}",
				"{3, 6, 7, 7, 7, 14}",
				"{4, 6, 7, 7, 7, 12, 14}",
				"{5, 5, 7, 7, 7, 10, 14}",
				"{2, 3, 10, 15}",
				"{2, 4, 10, 12, 15}",
				"{2, 5, 6, 15, 15}",
				"{3, 3, 5, 15, 15}",
				"{3, 3, 6, 10, 15}",
				"{3, 5, 5, 5, 15}",
				"...",
				"{3, 3, 4, 12} comes before {2, 7, 7, 7, 14} because the largest element of the first is less than the one from the second.",
				"{2, 5, 5, 10} comes before {3, 5, 5, 6, 10} because they both have the largest element 10 but the latter has more elements.",
				"{2, 4, 10, 12, 15} comes before {2, 5, 6, 15, 15} because they both have the largest element 15 and the same number of elements but the first smallest different element, 4 resp. 5, is less for the first."
			],
			"xref": [
				"Cf. A020473, A020639, A034708, A091784."
			],
			"keyword": "nonn,base,tabf",
			"offset": "1,1",
			"author": "_David A. Corneth_, Aug 23 2016",
			"references": 0,
			"revision": 34,
			"time": "2020-09-02T14:41:43-04:00",
			"created": "2016-10-09T19:31:04-04:00"
		}
	]
}