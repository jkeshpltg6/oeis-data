{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A172364",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 172364,
			"data": "1,1,1,1,1,1,1,1,1,1,1,3,3,3,1,1,10,30,30,10,1,1,31,310,930,310,31,1,1,94,2914,29140,29140,2914,94,1,1,285,26790,830490,2768300,830490,26790,285,1,1,865,246525,23173350,239457950,239457950,23173350,246525,865,1",
			"name": "Triangle read by rows: T(n,k) = round(c(n)/(c(k)*c(n-k))) where c are partial products of a sequence defined in comments.",
			"comment": [
				"Let f be the sequence 0, 1, 1, 1, 3, 10, 31, 94, 285, 865, 2626, 7972, 24201.., f(n) = 3*f(n-1)+f(n-4), and c the partial products of f: c(n) = 1, 1, 1, 1, 3, 30, 930, 87420, 24914700, 21551215500, ... . Then T(n,k) = round(c(n)/(c(k)*c(n-k)))."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A172364/b172364.txt\"\u003eRows n = 0..50 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k, q) = round( c(n,q)/(c(k,q)*c(n-k,q)) ), where c(n, q) = Product_{j=1..n} f(j, q), f(n, q) = q*f(n-1, q) + f(n-4, q), f(0, q) = 0, f(1, q) = f(2, q) = f(3, q) = 1, and q = 3. - _G. C. Greubel_, May 08 2021"
			],
			"example": [
				"Triangle begins as:",
				"  1;",
				"  1,   1;",
				"  1,   1,      1;",
				"  1,   1,      1,        1;",
				"  1,   3,      3,        3,         1;",
				"  1,  10,     30,       30,        10,         1;",
				"  1,  31,    310,      930,       310,        31,        1;",
				"  1,  94,   2914,    29140,     29140,      2914,       94,      1;",
				"  1, 285,  26790,   830490,   2768300,    830490,    26790,    285,   1;",
				"  1, 865, 246525, 23173350, 239457950, 239457950, 23173350, 246525, 865, 1;"
			],
			"mathematica": [
				"f[n_, q_]:= f[n, q]= If[n==0,0,If[n\u003c4, 1, q*f[n-1, q] + f[n-4, q]]];",
				"c[n_, q_]:= Product[f[j, q], {j,n}];",
				"T[n_, k_, q_]:= Round[c[n, q]/(c[k, q]*c[n-k, q])];",
				"Table[T[n, k, 3], {n,0,12}, {k,0,n}]//Flatten (* modified by _G. C. Greubel_, May 08 2021 *)"
			],
			"program": [
				"(Sage)",
				"@CachedFunction",
				"def f(n,q): return 0 if (n==0) else 1 if (n\u003c4) else q*f(n-1, q) + f(n-4, q)",
				"def c(n,q): return product( f(j,q) for j in (1..n) )",
				"def T(n,k,q): return round(c(n, q)/(c(k, q)*c(n-k, q)))",
				"flatten([[T(n,k,3) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, May 08 2021"
			],
			"xref": [
				"Cf. A172363 (q=1), this sequence (q=3)."
			],
			"keyword": "nonn,tabl,less",
			"offset": "0,12",
			"author": "_Roger L. Bagula_, Feb 01 2010",
			"ext": [
				"Definition corrected to give integral terms, _G. C. Greubel_, May 08 2021"
			],
			"references": 2,
			"revision": 20,
			"time": "2021-05-09T09:51:27-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}