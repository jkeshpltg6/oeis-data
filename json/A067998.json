{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A067998",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 67998,
			"data": "0,-1,0,3,8,15,24,35,48,63,80,99,120,143,168,195,224,255,288,323,360,399,440,483,528,575,624,675,728,783,840,899,960,1023,1088,1155,1224,1295,1368,1443,1520,1599,1680,1763,1848,1935,2024,2115,2208,2303,2400,2499,2600,2703,2808,2915,3024,3135,3248,3363",
			"name": "a(n) = n^2 - 2*n.",
			"comment": [
				"a(n) is essentially the case 0 of the polygonal numbers. The polygonal numbers are defined as P_k(n) = Sum_{i=1..n}((k-2)*i-(k-3). Thus P_0(n) = 2*n - n^2 and a(n) = -P_0(n). - _Peter Luschny_, Jul 08 2011",
				"For n \u003e= 3, the denominator of the probability of winning the prize by switching from the initial choice of doors in a generalized Monty Hall problem with n doors: After a prize has been placed behind exactly one of the n doors at random, a contestant chooses a door. Then the host, who knows where the prize is, deliberately opens exactly one unchosen door that does not hide the prize (picked at random by the host among such doors when there is a choice) and then gives the contestant an opportunity to switch to any other door not yet opened. The numerator of this probability is n-1 (incidentally, gcd(n-1, n*(n-2)) = 1). The probability of winning by switching minus the probability of winning by not switching is thus (n-1)/(n*(n-2)) - 1/n = 1/a(n), which approaches zero as n approaches infinity, but nevertheless makes the switching strategy better for every finite n \u003e= 3. The winning probability is 2/3 from switching in the classic 3-door Monty Hall problem; we have 3/8 and 4/15, respectively, in the 4- and 5-door generalizations. (The above analysis was independent but is consistent with the even more general \"N-doors\" section of the Wikipedia article, other parts of which make clear the historical importance of wording this problem as carefully as possible. See also A122774.) - _Rick L. Shepherd_, May 31 2014, clarified Oct 29 2015",
				"For n \u003e 1, a(n) is the largest integer k such that k + n^2 is a multiple of k + n. - _Derek Orr_, Sep 04 2014"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A067998/b067998.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://www.wikipedia.org/wiki/Monty_Hall_problem\"\u003eMonty Hall problem\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-3,1)."
			],
			"formula": [
				"a(n) = A005563(n-2) = A005563(-n) = A000290(n-1)-1.",
				"G.f.: x*(3*x-1)/(1-x)^3. - _Paul Barry_, Mar 27 2007",
				"E.g.f.: exp(x)*(x^2-x). - _Paul Barry_, Mar 27 2007",
				"a(n) = 2*n + a(n-1) - 3 (with a(0)=0). - _Vincenzo Librandi_, Aug 08 2010"
			],
			"maple": [
				"A067998:=n-\u003en^2-2*n: seq(A067998(n), n=0..50); # _Wesley Ivan Hurt_, Sep 04 2014"
			],
			"mathematica": [
				"Table[ n^2 - 2*n, {n, 0, 60} ] (* George E. Antoniou *)",
				"LinearRecurrence[{3, -3, 1}, {0, -1, 0}, 80] (* _Vladimir Joseph Stephan Orlovsky_, Feb 23 2012 *)"
			],
			"program": [
				"(PARI) a(n)=n^2-2*n;",
				"(Haskell)",
				"a067998 n = n * (n - 2)",
				"a067998_list = scanl (+) 0 [-1, 1 ..]",
				"-- _Reinhard Zumkeller_, Aug 26 2013",
				"(MAGMA) [n^2-2*n : n in [0..50]]; // _Wesley Ivan Hurt_, Sep 04 2014",
				"(PARI) concat(0, Vec(x*(3*x-1)/(1-x)^3 + O(x^100))) \\\\  _Altug Alkan_, Oct 30 2015"
			],
			"xref": [
				"Essentially the same as A005563.",
				"Cf. A060747 (first differences).",
				"Cf. A000290."
			],
			"keyword": "easy,sign",
			"offset": "0,4",
			"author": "_George E. Antoniou_, Feb 06 2002",
			"ext": [
				"Edited and extended by _Robert G. Wilson v_, Feb 08 2002"
			],
			"references": 30,
			"revision": 89,
			"time": "2020-02-29T03:04:47-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}