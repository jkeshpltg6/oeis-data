{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007219",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7219,
			"id": "M5204",
			"data": "1,28,2108,227322,30276740,4541771016,739092675672,127674038970623,23085759901610016,4327973308197103600,835531767841066680300,165266721954751746697155,33364181616540879268092840",
			"name": "Number of golygons of order 8n (or serial isogons of order 8n).",
			"comment": [
				"A golygon of order N is a closed path along the streets of the Manhattan grid with successive edge lengths of 1,2,3,...,N (returning to the starting point after the edge of length N), and which makes a 90-degree turn (left or right) after each edge.",
				"It is known that the order N must be a multiple of 8."
			],
			"reference": [
				"A. K. Dewdeney, An odd journey along even roads leads to home in Golygon City, Mathematical Recreations Column, Scientific American, July 1990, pp. 118-121.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence).",
				"I. Vardi, Computational Recreations in Mathematica. Addison-Wesley, Redwood City, CA, 1991, p. 92."
			],
			"link": [
				"Vaclav Kotesovec, \u003ca href=\"/A007219/b007219.txt\"\u003eTable of n, a(n) for n = 1..100\u003c/a\u003e",
				"A. K. Dewdeney, \u003ca href=\"/A007219/a007219.png\"\u003eIllustration of the unique golygon of order 8\u003c/a\u003e, from the article \"An odd journey along even roads leads to home in Golygon City\", Mathematical Recreations Column, Scientific American, July 1990, pp. 118-121.",
				"A. K. Dewdeney, \u003ca href=\"/A007219/a007219_1.png\"\u003eIllustration of the 28 golygons of order 16\u003c/a\u003e, from the article \"An odd journey along even roads leads to home in Golygon City\", Mathematical Recreations Column, Scientific American, July 1990, pp. 118-121.",
				"Adam P. Goucher, \u003ca href=\"http://cp4space.wordpress.com/2014/04/30/golygons-and-golyhedra/\"\u003eGolygons and golyhedra\u003c/a\u003e",
				"L. Sallows, M. Gardner, R. K. Guy and D. E. Knuth, \u003ca href=\"http://www.jstor.org/stable/2690648\"\u003eSerial isogons of 90 degrees\u003c/a\u003e, Math. Mag. 64 (1991), 315-324.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Golygon.html\"\u003eGolygon\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A006718(n)/4. - _Charles R Greathouse IV_, Apr 29 2012",
				"a(n) ~ 3*2^(8*n-6)/(Pi*n^2*(4*n+1)). - _Vaclav Kotesovec_, Dec 09 2013"
			],
			"mathematica": [
				"p1[n_] := Product[x^k + 1, {k, 1, n - 1, 2}] // Expand; p2[n_] := Product[x^k + 1, {k, 1, n/2}] // Expand; c[n_] := Coefficient[p1[n], x, n^2/8] * Coefficient[p2[n], x, n (n/2 + 1)/8]; a[n_] := c[8*n]/4; Table[a[n], {n, 1, 13}] (* _Jean-François Alcover_, Jul 24 2013, after _Eric W. Weisstein_ *)"
			],
			"xref": [
				"Cf. A060005, A107350.",
				"See also A006718."
			],
			"keyword": "nonn,easy,nice",
			"offset": "1,2",
			"author": "_Simon Plouffe_",
			"ext": [
				"Two more terms from _N. J. A. Sloane_ (from the reference), May 23 2005"
			],
			"references": 6,
			"revision": 56,
			"time": "2017-12-11T16:55:17-05:00",
			"created": "1994-04-28T03:00:00-04:00"
		}
	]
}