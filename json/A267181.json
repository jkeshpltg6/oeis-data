{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A267181",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 267181,
			"data": "0,1,0,1,0,0,1,2,1,0,1,3,0,2,0,1,4,4,3,3,0,1,5,2,0,1,4,0,1,6,5,5,4,4,5,0,1,7,3,6,0,5,2,6,0,1,8,6,2,6,5,1,5,7,0,1,9,4,6,4,0,3,5,3,8,0,1,10,7,7,7,7,6,6,6,6,9,0,1,11,5,3,2,7,0,6,1,2,4,10,0",
			"name": "Array read by antidiagonals: T(i,j) (i\u003e=0, j\u003e=0) = number of steps to reach either top row or main diagonal using the steps (i,j)-\u003e(j,i) or (i,j)-\u003e(i,j-i).",
			"comment": [
				"We start at (i,j) and apply either (i,j) -\u003e (j,i) if i\u003ej or (i,j) -\u003e (i,j-i) if j\u003ei.  T(i,j) is the minimal number of steps to reach either (0,k) or (k,k) for some k.",
				"Somewhat analogous to the array in A072030 except that here the offset is different and we pay for transposition steps as well as subtraction steps."
			],
			"formula": [
				"Recurrence: T(0,k)=TR(k,k)=0; if i\u003ej then T(i,j)=T(j,i)+1; if j\u003ei then T(i,j)=T(i,j-i)+1.",
				"For a \u003e 1 and b,k \u003e 0, T(ak,k) = a, T(ak+b,k) = T(b,k) + a + 2, T(k,ak) = a - 1, T(k,ak+b) = T(k,b) + a. - _Charlie Neder_, Feb 08 2019"
			],
			"example": [
				"Array begins:",
				"0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ...",
				"1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, ...",
				"1, 2, 0, 3, 1, 4, 2, 5, 3, 6, 4, 7, 5, ...",
				"1, 3, 4, 0, 4, 5, 1, 5, 6, 2, 6, 7, 3, ...",
				"1, 4, 2, 5, 0, 5, 3, 6, 1, 6, 4, 7, 2, ...",
				"1, 5, 5, 6, 6, 0, 6, 6, 7, 7, 1, 7, 7, ...",
				"1, 6, 3, 2, 4, 7, 0, 7, 4, 3, 5, 8, 1, ...",
				"1, 7, 6, 6, 7, 7, 8, 0, 8, 7, 7, 8, 8, ...",
				"1, 8, 4, 7, 2, 8, 5, 9, 0, 9, 5, 8, 3, ...",
				"1, 9, 7, 3, 7, 8, 4, 8, 10, 0, 10, 8, 4, ...",
				"1, 10, 5, 7, 5, 2, 6, 8, 6, 11, 0, 11, 6, ...",
				"1, 11, 8, 8, 8, 8, 9, 9, 9, 9, 12, 0, 12, ...",
				"1, 12, 6, 4, 3, 8, 2, 9, 4, 5, 7, 13, 0, ...",
				"...",
				"The first few antidiagonals are:",
				"0,",
				"1, 0,",
				"1, 0, 0,",
				"1, 2, 1, 0,",
				"1, 3, 0, 2, 0,",
				"1, 4, 4, 3, 3, 0,",
				"1, 5, 2, 0, 1, 4, 0,",
				"1, 6, 5, 5, 4, 4, 5, 0,",
				"1, 7, 3, 6, 0, 5, 2, 6, 0,",
				"1, 8, 6, 2, 6, 5, 1, 5, 7, 0,",
				"1, 9, 4, 6, 4, 0, 3, 5, 3, 8, 0,",
				"..."
			],
			"maple": [
				"M:=12;",
				"A:=Array(0..M, 0..M, 0);",
				"for k from 0 to M do A[0,k]:=0; A[k,k]:=0; od:",
				"# border number k",
				"# col k, row n",
				"for k from 1 to M do",
				"for n from 1 to k-1 do A[n,k]:=A[n,k-n]+1; od:",
				"# row k, col i",
				"for i from k-1 by -1 to 0 do A[k,i]:=A[i,k]+1; od:",
				"od:",
				"for n from 0 to M do lprint([seq(A[n,k],k=0..M)]); od: # square array",
				"for n from 0 to M do lprint([seq(A[n-j,j],j=0..n)]); od: # antidiagonals"
			],
			"xref": [
				"Cf. A072030.",
				"For initial rows and columns see A267182-A267187.",
				"For the array read mod 2, see A267188."
			],
			"keyword": "nonn,tabl",
			"offset": "0,8",
			"author": "_N. J. A. Sloane_, Jan 16 2016",
			"references": 8,
			"revision": 20,
			"time": "2019-02-08T12:44:02-05:00",
			"created": "2016-01-16T14:21:30-05:00"
		}
	]
}