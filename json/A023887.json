{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A023887",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 23887,
			"data": "1,5,28,273,3126,47450,823544,16843009,387440173,10009766650,285311670612,8918294543346,302875106592254,11112685048647250,437893920912786408,18447025552981295105,827240261886336764178,39346558271492178925595,1978419655660313589123980",
			"name": "a(n) = sigma_n(n): sum of n-th powers of divisors of n.",
			"comment": [
				"Logarithmic derivative of A023881.",
				"Compare to A217872(n) = sigma(n)^n."
			],
			"reference": [
				"Tom M. Apostol, Introduction to Analytic Number Theory, Springer-Verlag, 1976, page 38."
			],
			"link": [
				"Nick Hobson, \u003ca href=\"/A023887/b023887.txt\"\u003eTable of n, a(n) for n = 1..100\u003c/a\u003e"
			],
			"formula": [
				"G.f.: Sum_{n\u003e0} (n*x)^n/(1-(n*x)^n). - _Vladeta Jovovic_, Oct 27 2002",
				"If the canonical prime factorization of n \u003e 1 is the product of p^e(p) then sigma_n(n) = Product_p ((p^(n*(e(p)+1)))-1)/(p^n-1). - Nick Hobson, Nov 25 2006",
				"sigma_n(n) is odd if and only if n is a square or twice a square. - Nick Hobson, Nov 25 2006",
				"Conjecture: sigma_m(n) = sigma(n^m * rad(n)^(m-1))/sigma(rad(n)^(m-1)) for n \u003e 0 and m \u003e 0, where sigma = A000203 and rad = A007947. - _Velin Yanev_, Aug 24 2017",
				"a(n) ~ n^n. - _Vaclav Kotesovec_, Nov 02 2018",
				"Sum_{n\u003e=1} 1/a(n) = A199858. - _Amiram Eldar_, Nov 19 2020"
			],
			"example": [
				"The divisors of 6 are 1, 2, 3 and 6, so a(6) = 1^6 + 2^6 + 3^6 + 6^6 = 47450."
			],
			"mathematica": [
				"Table[DivisorSigma[n,n],{n,1,50}] (* _Vladimir Joseph Stephan Orlovsky_, Feb 26 2009 *)"
			],
			"program": [
				"(PARI) a(n) = sigma(n,n); \\\\ Nick Hobson, Nov 25 2006",
				"(Maxima) makelist(divsum(n,n),n,1,20); \\\\ _Emanuele Munarini_, Mar 26 2011"
			],
			"xref": [
				"Cf. A000203, A001157-A001160, A013954-A013972, A023881, A199858."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Olivier Gérard_",
			"ext": [
				"Edited by _N. J. A. Sloane_, Nov 25 2006"
			],
			"references": 38,
			"revision": 35,
			"time": "2020-11-19T04:43:24-05:00",
			"created": "1998-06-14T03:00:00-04:00"
		}
	]
}