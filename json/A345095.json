{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A345095",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 345095,
			"data": "0,1,1,2,3,5,8,13,21,3,24,6,30,3,33,6,39,12,51,6,57,12,69,15,84,12,96,15,111,2,113,4,117,8,125,7,132,5,137,10,147,11,158,13,171,8,179,16,195,14,209,9,218,9,227,9,236,9,245,9,254,9,263,9,272,9,281,9,290,9,299,18,317,8,325,7,332,5,337,10,347,11,358,13,371,8,379,16,395,14,409,9,418,9,427,9,436,9,445,9",
			"name": "a(n) is the sum of the two preceding terms if n is even, or of the two preceding digits if n is odd, with a(0) = 0, a(1) = 1.",
			"comment": [
				"Considering the terms modulo 100, the sequence becomes periodic with period 40 after the first 32 terms. [Observation by _Hans Havermann_.] See more precise formula: a(n+40) = a(n) [+ 200 if n even], n \u003e= 32, where 200 is the sum of every other term [i.e., the odd-indexed terms] of the repeating part.",
				"The repeating part (mod 100) is exactly the same as for the sister sequence A345097, where it starts already after 9 terms.",
				"Differs from the Fibonacci sequence A000045 from a(9) = 3 on.",
				"After a(6) = 8, every other term a(2k), computed as the sum of the two preceding terms, has at least two digits, so the subsequent term a(2k+1) is always equal to the sum of the last two digits of the preceding term a(2k).",
				"In the same way, the graph of this sequence has two components: odd-indexed terms repeating the pattern [8, 7, 5, 10, 11, 13, 8, 16, 14, 9, ..., 9, 18] of length 20, and even-indexed terms evolving around the straight line y(n) = 5n - 47.25 with first differences equal to the odd-indexed terms."
			],
			"link": [
				"Eric Angelini, \u003ca href=\"https://mailman.xmission.com/hyperkitty/list/math-fun@mailman.xmission.com/thread/ERBI7PWVRAJJ5ZTGSKX2C7F4PTAVTEOR/\"\u003eFibonacci alternated\u003c/a\u003e, math-fun discussion list on xmission.com, Jul 04 2021",
				"\u003ca href=\"/index/Rec#order_42\"\u003eIndex to entries for linear recurrences with constant coefficients, order 42\u003c/a\u003e, signature (0, 1, 0, ..., 0, 1, 0, -1)"
			],
			"formula": [
				"a(n+1) = a(n) + a(n-1) if n is odd, = a(n)%10 + floor(a(n)/10)%10 if n is even, where % is the binary modulo (or remainder) operator.",
				"a(n+40) = a(n) for odd n \u003e 32, a(n+40) = a(n) + 200 for even n \u003e= 32, whence:",
				"a(n) = a((n-32)%40 + 32) + [floor((n-32)/40)*200 if n even], n \u003e= 32, giving any a(n) explicitly in terms of a(0..71).",
				"a(n) = a(n-2) + a(n-40) - a(n-42) for n \u003e= 74.",
				"O.g.f.: x*(1 + x - x^2)*(Sum_{k=0..35} c_k x^2k)/(1 - x^2 - x^40 + x^42), where c = (1, 2, 5, 13, 3, 6, 3, 6, 12, 6, 12, 15, 12, 15, 2, 4, 8, 7, 5, 10, 10, 11, 3, 3, 11, 3, 6, 3, -3, 3, -3, -6, -3, -6, 7, 14). - _M. F. Hasler_, Jun 10 2021"
			],
			"example": [
				"Up to a(6) = 8, the terms have only one digit and therefore the sequence coincides with the Fibonacci sequence A000045 up to a(7) = 13.",
				"a(8) = 21 = 8 + 13 is the sum of the two preceding terms.",
				"a(9) = 3 = 2 + 1 is the sum of the two preceding digits.",
				"a(10) = 24 = 21 + 3 is the sum of the two preceding terms.",
				"a(11) = 6 = 2 + 4 is the sum of the two preceding digits.",
				"and so on."
			],
			"mathematica": [
				"a[0]=0;a[1]=1;a[n_]:=a[n]=If[EvenQ@n,a[n-1]+a[n-2],Total[Flatten[IntegerDigits/@Array[a,n-1]][[-2;;]]]];Array[a,100,0] (* _Giorgos Kalogeropoulos_, Jun 08 2021 *)"
			],
			"program": [
				"(PARI) A345095_vec(N=99,a=List([0,1]))={ for(n=2,N, listput(a, if(n%2==0 || a[n]\u003c10, a[n-1]+a[n], sumdigits(a[n]%100))));Vec(a)} \\\\ Compute the vector a(0..N)",
				"{M345095=A345095_vec(72); A345095(n)=if(n\u003c32, M345095[n+1], n=divrem(n-32, 40); M345095[n[2]+33]+!(n[2]%2)*n[1]*200)} \\\\ _M. F. Hasler_, Jun 10 2021"
			],
			"xref": [
				"Cf. A000045, A345097 (same with rule for odd/even indexed terms exchanged)."
			],
			"keyword": "nonn,base",
			"offset": "0,4",
			"author": "_M. F. Hasler_ and _Eric Angelini_, Jun 07 2021",
			"references": 2,
			"revision": 36,
			"time": "2021-06-12T14:07:41-04:00",
			"created": "2021-06-12T14:07:41-04:00"
		}
	]
}