{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A326616",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 326616,
			"data": "1,1,2,2,5,1,9,13,9,44,42,10,96,225,150,9,152,680,1098,576,3,155,1350,4155,5201,2266,124,2180,11730,26642,26904,9966,140,3751,30300,106281,182000,149832,47466,160,6050,69042,348061,896392,1229760,855240,237019",
			"name": "Number T(n,k) of colored integer partitions of n using all colors of a k-set such that each block of part i with multiplicity j has a pattern of i*j distinct colors in increasing order; triangle T(n,k), n\u003e=0, A185283(n)\u003c=k\u003c=n, read by rows.",
			"comment": [
				"T(n,k) is defined for all n\u003e=0 and k\u003e=0.  The triangle displays only positive terms.  All other terms are zero."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A326616/b326616.txt\"\u003eRows n = 0..200, flattened\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Partition_(number_theory)\"\u003ePartition (number theory)\u003c/a\u003e"
			],
			"formula": [
				"Sum_{k=A185283(n)..n} k * T(n,k) = A326649(n).",
				"Sum_{n=k..A024916(k)} n * T(n,k) = A326651(k)."
			],
			"example": [
				"T(3,2) = 2: 2a1b, 2b1a.",
				"T(3,3) = 5: 3abc, 2ab1c, 2ac1b, 2bc1a, 111abc",
				"Triangle T(n,k) begins:",
				"  1;",
				"     1;",
				"        2;",
				"        2,  5;",
				"        1,  9,  13;",
				"            9,  44,   42;",
				"           10,  96,  225,   150;",
				"            9, 152,  680,  1098,    576;",
				"            3, 155, 1350,  4155,   5201,   2266;",
				"               124, 2180, 11730,  26642,  26904,   9966;",
				"               140, 3751, 30300, 106281, 182000, 149832, 47466;",
				"               ..."
			],
			"maple": [
				"g:= proc(n) option remember; `if`(n=0, 0, numtheory[sigma](n)+g(n-1)) end:",
				"h:= proc(n) option remember; local k; for k from",
				"      `if`(n=0, 0, h(n-1)) do if g(k)\u003e=n then return k fi od",
				"    end:",
				"b:= proc(n, i, k) option remember; `if`(n=0, 1, `if`(i\u003c1, 0, add((t-\u003e",
				"      b(n-t, min(n-t, i-1), k)*binomial(k, t))(i*j), j=0..n/i)))",
				"    end:",
				"T:= (n, k)-\u003e add(b(n$2, k-i)*(-1)^i*binomial(k, i), i=0..k):",
				"seq(seq(T(n, k), k=h(n)..n), n=0..12);"
			],
			"mathematica": [
				"g[n_] := g[n] = If[n == 0, 0, DivisorSigma[1, n] + g[n - 1]];",
				"h[n_] := h[n] = Module[{k}, For[k = If[n == 0, 0, h[n - 1]], True, k++,  If[g[k] \u003e= n, Return[k]]]];",
				"b[n_, i_, k_] := b[n, i, k] = If[n == 0, 1, If[i \u003c 1, 0, Sum[Function[t,   b[n - t, Min[n - t, i - 1], k]*Binomial[k, t]][i*j], {j, 0, n/i}]]];",
				"T[n_, k_] := Sum[b[n, n, k - i]*(-1)^i*Binomial[k, i], {i, 0, k}];",
				"Table[Table[T[n, k], {k, h[n], n}], {n, 0, 12}]  // Flatten (* _Jean-François Alcover_, Feb 27 2021, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Main diagonal gives A178682.",
				"Row sums give A326648.",
				"Column sums give A326650.",
				"Cf. A000203, A185283, A326617 (this triangle read by columns), A326649, A326651."
			],
			"keyword": "nonn,tabf",
			"offset": "0,3",
			"author": "_Alois P. Heinz_, Sep 12 2019",
			"references": 7,
			"revision": 72,
			"time": "2021-02-27T15:06:13-05:00",
			"created": "2019-09-15T18:22:41-04:00"
		}
	]
}