{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A122070",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 122070,
			"data": "1,1,2,2,6,5,3,15,24,13,5,32,78,84,34,8,65,210,340,275,89,13,126,510,1100,1335,864,233,21,238,1155,3115,5040,4893,2639,610,34,440,2492,8064,16310,21112,17080,7896,1597,55,801,5184,19572,47502,76860,82908,57492,23256,4181",
			"name": "Triangle given by T(n,k) = Fibonacci(n+k+1)*binomial(n,k) for 0\u003c=k\u003c=n.",
			"comment": [
				"Subtriangle of (0, 1, 1, -1, 0, 0, 0, 0, 0, 0, 0, ...) DELTA (1, 1, 1, 0, 0, 0, 0, 0, 0, 0, ...) where DELTA is the operator defined in A084938.",
				"Mirror image of the triangle in A185384."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A122070/b122070.txt\"\u003eRows n = 0..100 of triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = A000045(n+k+1)*A007318(n,k) .",
				"T(n,n) = Fibonacci(2*n+1) = A001519(n+1) .",
				"Sum_{k=0..n} T(n,k) = Fibonacci(3*n+1) = A033887(n) .",
				"Sum_{k=0..n}(-1)^k*T(n,k) = (-1)^n = A033999(n) .",
				"Sum_{k=0..floor(n/2)} T(n-k,k) = (Fibonacci(n+1))^2 = A007598(n+1).",
				"Sum_{k=0..n} T(n,k)*2^k = Fibonacci(4*n+1) = A033889(n).",
				"Sum_{k=0..n} T(n,k)^2 = A208588(n).",
				"G.f.: (1-y*x)/(1-(1+3y)*x-(1+y-y^2)*x^2).",
				"T(n,k) = T(n-1,k) + 3*T(n-1,k-1) + T(n-2,k) + T(n-2,k-1) - T(n-2,k-2), T(0,0) = T(1,0) = 1, T(1,1) = 2, T(n,k) = 0 if k\u003c0 or if k\u003en.",
				"T(n,k) = A185384(n,n-k).",
				"T(2n,n) = binomial(2n,n)*Fibonacci(3*n+1) = A208473(n)."
			],
			"example": [
				"Triangle begins:",
				"   1;",
				"   1,   2;",
				"   2,   6,   5;",
				"   3,  15,  24,   13;",
				"   5,  32,  78,   84,   34;",
				"   8,  65, 210,  340,  275,  89;",
				"  13, 126, 510, 1100, 1335, 864, 233;",
				"(0, 1, 1, -1, 0, 0, ...) DELTA (1, 1, 1, 0, 0, ...) begins :",
				"  1;",
				"  0,  1;",
				"  0,  1,   2;",
				"  0,  2,   6,   5;",
				"  0,  3,  15,  24,   13;",
				"  0,  5,  32,  78,   84,   34;",
				"  0,  8,  65, 210,  340,  275,  89;",
				"  0, 13, 126, 510, 1100, 1335, 864, 233;"
			],
			"maple": [
				"with(combinat): seq(seq(binomial(n,k)*fibonacci(n+k+1), k=0..n), n=0..10); # _G. C. Greubel_, Oct 02 2019"
			],
			"mathematica": [
				"Table[Fibonacci[n+k+1]*Binomial[n,k], {n,0,10}, {k,0,n}]//Flatten (* _G. C. Greubel_, Oct 02 2019 *)"
			],
			"program": [
				"(PARI) T(n,k) = binomial(n,k)*fibonacci(n+k+1);",
				"for(n=0,10, for(k=0,n, print1(T(n,k), \", \"))) \\\\ _G. C. Greubel_, Oct 02 2019",
				"(MAGMA) [Binomial(n,k)*Fibonacci(n+k+1): k in [0..n], n in [0..10]]; // _G. C. Greubel_, Oct 02 2019",
				"(Sage) [[binomial(n,k)*fibonacci(n+k+1) for k in (0..n)] for n in (0..10)] # _G. C. Greubel_, Oct 02 2019",
				"(GAP) Flat(List([0..10], n-\u003e List([0..n], k-\u003e Binomial(n,k)*Fibonacci(n+ k+1) ))); # _G. C. Greubel_, Oct 02 2019"
			],
			"xref": [
				"Cf. A000045, A001519, A033887, A033889, A185384."
			],
			"keyword": "nonn,tabl",
			"offset": "0,3",
			"author": "_Philippe Deléham_, Oct 15 2006, Mar 13 2012",
			"ext": [
				"Corrected and extended by _Philippe Deléham_, Mar 13 2012",
				"Term a(50) corrected by _G. C. Greubel_, Oct 02 2019"
			],
			"references": 4,
			"revision": 17,
			"time": "2019-10-02T20:41:09-04:00",
			"created": "2006-12-06T03:00:00-05:00"
		}
	]
}