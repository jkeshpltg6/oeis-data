{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A089582",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 89582,
			"data": "2,0,2,2,2,2,2,2,0,0,0,0,0,0,2,2,0,2,2,0,0,2,2,2,0,0,0,2,2,0,2,0,0,0,2,2,0,0,0,0,0,0,2,2,0,2,2,0,2,0,0,2,0,2,2,2,2,0,0,0,0,0,0,2,0,0,2,2,0,0,2,2,0,2,0,0,0,0,0,2,0,2,2,2,2,2,0,0,2,2,0,0,2,2,0,0,0,0,2,2,2,2,0,0,0",
			"name": "From Gilbreath's conjecture.",
			"comment": [
				"Let d_0(n) = p_n, the n-th prime, for n = 1 and let d_k+1 (n) = | d_k(n) - d_k(n+1) | for k = 0, n = 1. A well known conjecture, usually ascribed to Gilbreath but actually due to Proth in the 19th century, says that d_k(1) = 1 for all k \u003e= 1. This sequence gives d_k(2) for all k \u003e1 and for the conjecture to be true, this sequence must contain only 0's and 2's. Although not necessary to the conjecture's validity, the 0's and 2's are of roughly equal count.",
				"The paper cited below by A. M. Odlyzko reports on a computation that verified this conjecture for k = p(10^13) ~ 3 * 10^11. It also discusses the evidence and the heuristics about this conjecture. It is very likely that similar conjectures are also valid for many other integer sequences.",
				"Number of zeros in the first 10^n terms: 3, 53, 520, 4995, 49737, 500177, ... - _Robert G. Wilson v_, Sep 29 2014"
			],
			"reference": [
				"R. K. Guy, Unsolved Problems in Number Theory, 2nd Ed., Springer-Verlag, NY, Berlin, 1994, A10.",
				"Clifford A. Pickover, The Math Book, From Pythagoras to the 57th Dimension, 250 Milestones in the History of Mathematics, Sterling Publ., NY, 2009, page 410.",
				"P. Ribenboim, The new book of prime number records, 3rd edition, Springer-Verlag, New York, NY, pp. xxiv+541, ISBN 0-387-94457-5. 1995. MR 96k:11112"
			],
			"link": [
				"Chris Caldwell, \u003ca href=\"http://primes.utm.edu/glossary/page.php?sort=GilbreathsConjecture\"\u003eThe Prime Glossary, Goldbach's conjecture\u003c/a\u003e.",
				"Andrew M. Odlyzko, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1993-1182247-7\"\u003eIterated Absolute Values of Differences of Consecutive Primes\u003c/a\u003e, Math. Comp. 61 (1993), 373-380.",
				"N. J. A. Sloane, \u003ca href=\"http://neilsloane.com/doc/sg.txt\"\u003eMy favorite integer sequences\u003c/a\u003e, in Sequences and their Applications (Proceedings of SETA '98).",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/GilbreathsConjecture.html\"\u003eGilbreath's Conjecture.\u003c/a\u003e"
			],
			"example": [
				"See the triangle in A036262."
			],
			"mathematica": [
				"mx = 105; lst = {}; t = Array[ Prime, mx+2]; Do[t = Abs@ Differences@ t; AppendTo[lst, t[[2]]], {n, mx}]; lst"
			],
			"xref": [
				"See A036262 for an abbreviated table of absolute differences."
			],
			"keyword": "easy,nonn",
			"offset": "1,1",
			"author": "_Robert G. Wilson v_ and _R. K. Guy_, Nov 08 2003",
			"references": 2,
			"revision": 31,
			"time": "2019-11-29T21:28:34-05:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}