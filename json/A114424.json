{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A114424",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 114424,
			"data": "1,0,1,1,1,1,1,4,2,4,2,9,5,9,10,17,17,17,26,29,50,34,65,61,102,72,146,130,201,170,266,289,387,388,491,611,677,811,899,1260,1225,1630,1619,2355,2270,3086,2970,4361,4147,5524,5555,7625,7609,9681,10202,13085",
			"name": "Number of partitions of n such that the size of the tail below the Durfee square is equal to the size of the tail to the right of the Durfee square.",
			"reference": [
				"G. E. Andrews, The Theory of Partitions, Addison-Wesley, 1976 (pp. 27-28).",
				"G. E. Andrews and K. Eriksson, Integer Partitions, Cambridge Univ. Press, 2004 (pp. 75-78)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A114424/b114424.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = coefficient of (t^0)(z^n) in G(t,1/t,z), where G(t,s,z)=sum(z^(k^2)/product((1-(tz)^j)(1-(sz)^j),j=1..k),k=1..infinity) is the trivariate g.f. according to partition size (z), size of the tail below the Durfee square (t) and size of the tail to the right of the Durfee square (s)."
			],
			"example": [
				"a(9) = 2 because we have [5,1,1,1,1] with both tails of size 4 and [3,3,3] with both tails of size 0."
			],
			"maple": [
				"g:=sum(z^(k^2)/product((1-(t*z)^j)*(1-(z/t)^j), j=1..k), k=1..10): gser:=simplify(series(g,z=0,65)): 1,seq(coeff(numer(coeff(gser,z^n)), t^(n-1)), n=2..60);",
				"# second Maple program:",
				"b:= proc(n, i) option remember;",
				"      `if`(n=0, 1, `if`(i\u003c1, 0, b(n, i-1)+`if`(i\u003en, 0, b(n-i, i))))",
				"    end:",
				"a:= proc(n) local r; add(`if`(irem(n-d^2, 2, 'r')=1, 0,",
				"                          b(r, d)^2), d=1..floor(sqrt(n)))",
				"    end:",
				"seq(a(n), n=1..70);  # _Alois P. Heinz_, Apr 09 2012"
			],
			"mathematica": [
				"b[n_, i_] := b[n, i] = If[n==0, 1, If[i\u003c1, 0, b[n, i-1] + If[i\u003en, 0, b[n-i, i]]]]; a[n_] := Sum[If[{q, r} = QuotientRemainder[n-d^2, 2]; r==1, 0, b[q, d]^2], {d, 1, Floor[Sqrt[n]]}]; Table[a[n], {n, 1, 70}] (* _Jean-François Alcover_, Mar 31 2015, after _Alois P. Heinz_ *) *)"
			],
			"keyword": "nonn",
			"offset": "1,8",
			"author": "_Emeric Deutsch_, Feb 12 2006",
			"references": 1,
			"revision": 10,
			"time": "2015-03-31T08:54:37-04:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}