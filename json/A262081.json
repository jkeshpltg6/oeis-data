{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A262081",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 262081,
			"data": "6,10,210,34,399,58,651,82,903,795,1281,142,1533,2739,2037,214,6213,1965,2919,298,5285,3423,2595,3801,382,12545,7385,4683,3495,5061,8785,3945,5691,562,5943,9669,6447,10461,12909,694,7329,5385,12845,7959,5835,13895",
			"name": "a(1) = 6; for n \u003e 1, a(n) = the least squarefree composite number whose sum of prime factors is prime and whose greatest prime factor is the sum of prime factors of a(n-1).",
			"comment": [
				"a(n) is a composite squarefree integer in order to avoid the trivial sequences 3, 6, 5, 5, 5, ... or 6, 10, 49, 49, 49, ... if a(1)=3 or a(1)=6.",
				"The greatest prime factors of a(n) are 3, 5, 7, 17, 19, 29, 31, 41, 43, 53, 61, 71, 73, 83, 97, 107, 109, 131, 139, 149, 151, 163, 173, 181, 191, 193, 211, 223, 233, 241, ...",
				"The definition expressed with A-numbers: a(1) = 6; for n \u003e 1, a(n) = the least term k of A120944 such that A001414(k) is prime and A006530(k) = A001414(a(n-1)). Also A008472 could be used instead of A001414 because all terms are squarefree, thus all prime factors of any single term are distinct. - _Antti Karttunen_, Sep 28 2015"
			],
			"link": [
				"Michel Lagneau, \u003ca href=\"/A262081/b262081.txt\"\u003eTable of n, a(n) for n = 1..500\u003c/a\u003e"
			],
			"example": [
				"{a(4), a(5), a(6)} = {34, 399, 58} because 34 = 2*17 =\u003e 2+17 = 19 is the greatest prime factor of 399 = 3*7*19, and 3+7+19 = 29 is the greatest prime divisor of 58 = 2*29 where 29+2 = 31 is prime."
			],
			"maple": [
				"with(numtheory):p0:=3:nn:=10^7:",
				"  for n from 1 to 50 do:",
				"   ii:=0:",
				"     for k from 4 to nn while(ii=0) do:",
				"      x:=factorset(k):n0:=nops(x):",
				"      s:=sum('x[i]', 'i'=1..n0):",
				"       if isprime(s) and x[n0]=p0 and issqrfree(k)and type(k,prime)=false",
				"        then",
				"        ii:=1: printf(`%d, `,k):p0:=s:k0:=k:",
				"        else",
				"        fi:",
				"       od:",
				"      od:"
			],
			"program": [
				"(PARI) gpf(k) = my(fk=factor(k)); fk[#fk~,1];",
				"sc(k) = forcomposite(c = k,, if (issquarefree(c), return (c)));",
				"sdpf(k)= my(fk=factor(k)); vecsum(fk[,1]);",
				"scok(a) = {my(s = sdpf(a)); forcomposite(c = 1,,if (issquarefree(c) \u0026\u0026 (gpf(c)==s) \u0026\u0026 isprime(sdpf(c)), return (c)););}",
				"lista(nn) = {print1(a=sc(1), \", \");for (n=2, nn, na = scok(a); print1(na, \", \"); a = na;);} \\\\ _Michel Marcus_, Sep 27 2015"
			],
			"xref": [
				"Cf. A001414, A008472, A006530, A120944."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Michel Lagneau_, Sep 10 2015",
			"references": 1,
			"revision": 31,
			"time": "2015-10-02T16:45:29-04:00",
			"created": "2015-10-02T16:45:29-04:00"
		}
	]
}