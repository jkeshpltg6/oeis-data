{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A126331",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 126331,
			"data": "1,4,1,17,9,1,77,63,14,1,371,406,134,19,1,1890,2535,1095,230,24,1,10095,15660,8240,2269,351,29,1,56040,96635,59129,19936,4053,497,34,1,320795,598344,412216,162862,40698,6572,668,39,1",
			"name": "Triangle T(n,k), 0 \u003c= k \u003c= n, read by rows defined by: T(0,0)=1, T(n,k)=0 if k \u003c 0 or if k \u003e n, T(n,0) = 4*T(n-1,0) + T(n-1,1), T(n,k) = T(n-1,k-1) + 5*T(n-1,k) + T(n-1,k+1) for k \u003e= 1.",
			"comment": [
				"This triangle belongs to the family of triangles defined by: T(0,0)=1, T(n,k)=0 if k \u003c 0 or if k \u003e n, T(n,0) = x*T(n-1,0) + T(n-1,1), T(n,k) = T(n-1,k-1) + y*T(n-1,k) + T(n-1,k+1) for k \u003e= 1. Other triangles arise from choosing different values for (x,y): (0,0) -\u003e A053121; (0,1) -\u003e A089942; (0,2) -\u003e A126093; (0,3) -\u003e A126970; (1,0)-\u003e A061554; (1,1) -\u003e A064189; (1,2) -\u003e A039599; (1,3) -\u003e A110877; (1,4) -\u003e A124576; (2,0) -\u003e A126075; (2,1) -\u003e A038622; (2,2) -\u003e A039598; (2,3) -\u003e A124733; (2,4) -\u003e A124575; (3,0) -\u003e A126953; (3,1) -\u003e A126954; (3,2) -\u003e A111418; (3,3) -\u003e A091965; (3,4) -\u003e A124574; (4,3) -\u003e A126791; (4,4) -\u003e A052179; (4,5) -\u003e A126331; (5,5) -\u003e A125906. - _Philippe Deléham_, Sep 25 2007",
				"7^n = (n-th row terms) dot (first n+1 odd integers). Example: 7^3 = 343 = (77, 63, 14, 1) dot (1, 3, 5, 7) = (77 + 189 + 70 + 7) = 243. - _Gary W. Adamson_, Jun 15 2011"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A126331/b126331.txt\"\u003eTable of n, a(n) for the first 50 rows, flattened\u003c/a\u003e"
			],
			"formula": [
				"Sum_{k=0..n} T(n,k) = A098409(n).",
				"Sum_{k\u003e=0} T(m,k)*T(n,k) = T(m+n,0) = A104455(m+n).",
				"Sum_{k=0..n} T(n,k)*(2*k+1) = 7^n. - _Philippe Deléham_, Mar 26 2007"
			],
			"example": [
				"Triangle begins:",
				"      1;",
				"      4,     1;",
				"     17,     9,    1;",
				"     77,    63,   14,    1;",
				"    371,   406,  134,   19,   1;",
				"   1890,  2535, 1095,  230,  24,  1;",
				"  10095, 15660, 8240, 2269, 351, 29, 1;",
				"From _Philippe Deléham_, Nov 07 2011: (Start)",
				"Production matrix begins:",
				"  4, 1",
				"  1, 5, 1",
				"  0, 1, 5, 1",
				"  0, 0, 1, 5, 1",
				"  0, 0, 0, 1, 5, 1,",
				"  0, 0, 0, 0, 1, 5, 1",
				"  0, 0, 0, 0, 0, 1, 5, 1",
				"  0, 0, 0, 0, 0, 0, 1, 5, 1",
				"  0, 0, 0, 0, 0, 0, 0, 1, 5, 1 (End)"
			],
			"mathematica": [
				"T[0, 0, x_, y_] := 1; T[n_, 0, x_, y_] := x*T[n - 1, 0, x, y] + T[n - 1, 1, x, y]; T[n_, k_, x_, y_] := T[n, k, x, y] = If[k \u003c 0 || k \u003e n, 0,",
				"T[n - 1, k - 1, x, y] + y*T[n - 1, k, x, y] + T[n - 1, k + 1, x, y]];",
				"Table[T[n, k, 4, 5], {n, 0, 10}, {k, 0, n}] // Flatten (* _G. C. Greubel_, May 22 2017 *)"
			],
			"keyword": "nonn,tabl",
			"offset": "0,2",
			"author": "_Philippe Deléham_, Mar 10 2007",
			"references": 25,
			"revision": 23,
			"time": "2020-01-20T21:41:38-05:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}