{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A028416",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 28416,
			"data": "7,11,13,17,19,23,29,47,59,61,73,89,97,101,103,109,113,127,131,137,139,149,157,167,179,181,193,197,211,223,229,233,241,251,257,263,269,281,293,313,331,337,349,353,367,373,379,383,389,401,409,419,421,433",
			"name": "Primes p such that the decimal expansion of 1/p has a periodic part of even length.",
			"comment": [
				"Primes whose reciprocals have even period length.",
				"Primes p such that the order of 10 mod p is even. [_Joerg Arndt_, Mar 04 2014]",
				"A002371(A049084(a(n))) mod 2 == 0.",
				"Not the same as A040121: a(33)=241 is not in A040121.",
				"Let (d(i): 1\u003c=i\u003c=2*K) be the period of decimal expansion of 1/a(n), K=A002371(A049084(a(n)))/2, then d(i) + d(i+K) = 9 for i with 1\u003c=i\u003c=K, or, equivalently: u + v = 10^K - 1 with u = SUM(d(i)*10^(K-i):1\u003c=i\u003c=K) and v = SUM(d(i+K)*10^(K-i):1\u003c=i\u003c=K). - _Reinhard Zumkeller_, Oct 05 2008"
			],
			"reference": [
				"H. Rademacher and O. Toeplitz, Von Zahlen und Figuren (Springer 1930, reprinted 1968), ch. 19, 'Die periodischen Dezimalbrueche'. [_Reinhard Zumkeller_, Oct 05 2008]"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A028416/b028416.txt\"\u003eTable of n, a(n) for n=1..1000\u003c/a\u003e",
				"\u003ca href=\"/index/1#1overn\"\u003eIndex entries for sequences related to decimal expansion of 1/n.\u003c/a\u003e"
			],
			"example": [
				"From _Reinhard Zumkeller_, Oct 05 2008: (Start)",
				"(0,5,8,8,2,3,5,2,9,4,1,1,7,6,4,7) is the period of 1/17 (see A007450),",
				"K = A002371(A049084(17))/2 = A002371(7)/2 = 16/2 = 8,",
				"u = 5882352, v = 94117647: u + v = 99999999 = 10^8 - 1. (End)"
			],
			"maple": [
				"A028416 := proc(n) local st:",
				"st := ithprime(n):",
				"if (modp(numtheory[order](10,st),2) = 0) then",
				"   RETURN(st)",
				"fi: end:  seq(A028416(n), n=1..100); # _Jani Melik_, Feb 24 2011"
			],
			"mathematica": [
				"Select[Prime[Range[4,100]],EvenQ[Length[RealDigits[1/#][[1,1]]]]\u0026] (* _Harvey P. Dale_, Jul 07 2011 *)"
			],
			"program": [
				"(PARI) forprime(p=7,1e3,if(znorder(Mod(10,p))%2==0,print1(p\", \"))) \\\\ _Charles R Greathouse IV_, Feb 24 2011"
			],
			"xref": [
				"Cf. A087000, A186635."
			],
			"keyword": "nonn,base",
			"offset": "1,1",
			"author": "Mario Velucchi (mathchess(AT)velucchi.it)",
			"ext": [
				"More terms from _Reinhard Zumkeller_, Jul 29 2003"
			],
			"references": 10,
			"revision": 39,
			"time": "2021-12-30T10:49:09-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}