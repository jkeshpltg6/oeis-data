{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A079910",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 79910,
			"data": "1,6,46,212,738,2104,5150,11196,22162,40688,70254,115300,181346,275112,404638,579404,810450,1110496,1494062,1977588,2579554,3320600,4223646,5314012,6619538,8170704,10000750,12145796,14644962,17540488,20877854",
			"name": "Solution to the Dancing School Problem with 5 girls and n+5 boys: f(5,n).",
			"comment": [
				"f(g,h) = per(B), the permanent of the (0,1)-matrix B of size g X (g+h) with b(i,j)=1 if and only if i \u003c= j \u003c= i+h. See A079908 for more information.",
				"For fixed g, f(g,n) is polynomial in n for n \u003e= g-2. See reference."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A079910/b079910.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Jaap Spies, \u003ca href=\"http://www.jaapspies.nl/mathfiles/dancingschool.pdf\"\u003eDancing School Problems, Permanent solutions of Problem 29\u003c/a\u003e, Nieuw Archief voor Wiskunde 5/7 nr. 4, Dec 2006, pp. 283-285.",
				"Jaap Spies, \u003ca href=\"http://www.jaapspies.nl/oeis/a079910.sage\"\u003eSage program for computing A079910\u003c/a\u003e.",
				"Jaap Spies, \u003ca href=\"http://www.jaapspies.nl/mathfiles/dancing.sage\"\u003eSage program for computing the polynomial a(n)\u003c/a\u003e.",
				"Jaap Spies, \u003ca href=\"http://www.jaapspies.nl/bookb5.pdf\"\u003eA Bit of Math, The Art of Problem Solving\u003c/a\u003e, Jaap Spies Publishers (2019).",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (6,-15,20,-15,6,-1)."
			],
			"formula": [
				"a(0)=1, a(1)=6, a(2)=46, a(n) = n^5 - 5*n^4 + 25*n^3 - 55*n^2 + 80*n - 46.",
				"G.f.: (6*x^7 + 11*x^6 + 20*x^5 + 51*x^4 + 6*x^3 + 25*x^2 + 1) / (x-1)^6. - _Colin Barker_, Jan 04 2015",
				"E.g.f.: 47 + 6*x + exp(x)*(-46 + 46*x + 20*x^3 + 5*x^4 + x^5). - _Stefano Spezia_, Dec 18 2019"
			],
			"mathematica": [
				"CoefficientList[Series[(6 x^7 + 11 x^6 + 20 x^5 + 51 x^4 + 6 x^3 + 25 x^2 + 1) / (x - 1)^6, {x, 0, 30}], x] (* _Vincenzo Librandi_, Feb 17 2015 *)"
			],
			"program": [
				"(PARI) Vec((6*x^7+11*x^6+20*x^5+51*x^4+6*x^3+25*x^2+1)/(x-1)^6 + O(x^100)) \\\\ _Colin Barker_, Jan 04 2015",
				"(MAGMA) [1,6] cat [n^5-5*n^4+25*n^3-55*n^2+80*n-46: n in [2..30]]; // _Vincenzo Librandi_, Feb 17 2015"
			],
			"xref": [
				"Cf. A079908-A079928."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Jaap Spies_, Jan 28 2003",
			"ext": [
				"More terms from _Benoit Cloitre_, Jan 29 2003"
			],
			"references": 2,
			"revision": 29,
			"time": "2019-12-18T21:50:44-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}