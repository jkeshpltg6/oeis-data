{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A346012",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 346012,
			"data": "0,0,0,1,0,0,0,1,1,0,0,1,0,0,0,3,0,1,0,1,0,0,0,1,1,0,1,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,1,0,0,3,1,1,0,1,0,1,0,1,0,0,0,1,0,0,1,5,0,0,0,1,0,0,0,5,0,0,1,1,0,0,0,3,3,0,0,1,0,0,0",
			"name": "a(n) is the numerator of A346009(n)/A346010(n) - A001221(n)/2.",
			"comment": [
				"a(n)/A346013 is the difference between two functions, the average number of distinct prime factors of the divisors of n and half the number of distinct prime factors of n.",
				"Duncan (1961) proved that these two functions have the same average order, log(log(n))/2, and that their difference has a constant average order, or an asymptotic mean, c (A346011; see the Formula section).",
				"The nonzero values occur exactly at the nonsquarefree numbers (A013929) and their asymptotic mean is c/(1-6/Pi^2) = 0.2439041253..."
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A346012/b346012.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"R. L. Duncan, \u003ca href=\"https://www.jstor.org/stable/2311587\"\u003eNote on the divisors of a number\u003c/a\u003e, The American Mathematical Monthly, Vol. 68, No. 4 (1961), pp. 356-359.",
				"Sébastien Gaboury, \u003ca href=\"http://hdl.handle.net/20.500.11794/18899\"\u003eSur les convolutions de fonctions arithmétiques\u003c/a\u003e, M.Sc. thesis, Laval University, Quebec, 2007.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Average_order_of_an_arithmetic_function\"\u003eAverage order of an arithmetic function\u003c/a\u003e."
			],
			"formula": [
				"Let f(n) = a(n)/A346013(n) be the sequence of fractions. Then:",
				"f(n) depends only on the prime signature of n: If n = Product_{i} p_i^e_i, then f(n) = Sum_{i} (e_i - 1)/(2*(e_i + 1)).",
				"f(n) = 0 if and only if n is squarefree (A005117), and f(n) \u003e 0 otherwise.",
				"f(n) = (Sum_{p prime, p^2|n} d(n/p^2))/(2*d(n)), where d(n) is the number of divisors of n (A000005).",
				"Asymptotic mean: lim_{n-\u003eoo} (1/n) * Sum_{k=1..n} f(n) = 0.095628... (A346011)."
			],
			"example": [
				"The fractions begin with 0, 0, 0, 1/6, 0, 0, 0, 1/4, 1/6, 0, 0, 1/6, ...."
			],
			"mathematica": [
				"f[p_, e_] := e/(e + 1); a[1] = 0; a[n_] := Numerator[Plus @@ f @@@ (fct = FactorInteger[n]) - Length[fct]/2]; Array[a, 100]"
			],
			"xref": [
				"Cf. A000005, A001221, A005117, A013929, A346009, A346010, A346011, A346013 (denominators)."
			],
			"keyword": "nonn,frac",
			"offset": "1,16",
			"author": "_Amiram Eldar_, Jul 01 2021",
			"references": 3,
			"revision": 10,
			"time": "2021-07-02T03:48:56-04:00",
			"created": "2021-07-01T16:12:32-04:00"
		}
	]
}