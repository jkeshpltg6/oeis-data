{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A288776",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 288776,
			"data": "1,0,0,512,11232,145920,1055616,5618688,25330128,89127936,295067136",
			"name": "Theta series of the 24-dimensional lattice of hyper-roots E_5(SU(3))",
			"comment": [
				"This lattice is associated with the exceptional module-category E_5(SU(3)) over the fusion (monoidal) category A_5(SU(3)).",
				"The Grothendieck group of the former, a finite abelian category, is a Z+ - module over the Grothendieck ring of the latter, with a basis given by isomorphism classes of simple objects.",
				"Simple objects of A_k(SU(3)) are irreducible integrable representations of the affine Lie algebra of SU(3) at level k.",
				"The classification of module-categories over A_k(SU(3)) was done, using another terminology, by P. Di Francesco and J.-B Zuber, and by A. Ocneanu (see refs below): it contains several infinite families that exist for all values of the positive integer k (among others one finds the A_k(SU(3)) themselves and the orbifold series D_k(SU(3))), and several exceptional cases for special values of k.",
				"To every such module-category one can associate a set of hyper-roots (see refs below) and consider the corresponding lattice, denoted by the same symbol.",
				"E_k(SU(3)), with k=5, is one of the exceptional cases; other exceptional cases exist for k=9 and k=21. It is also special because it has self-fusion (it is flat, in operator algebra parlance).",
				"E_5(SU(3)) has r=12 simple objects. The rank of the lattice is 2r=24. Det =2^30. This lattice, with k=5, is defined by 2 * r * (k+3)^2/3=512 hyper-roots of norm 6. They are also the vectors of shortest length (so, vectors of shortest length and hyper-roots coincide, like for lattices of type A_k(SU(3))). Minimal norm is 6.",
				"The lattice is rescaled (q --\u003e q^2): its theta function starts as 1 + 512*q^6 + 11232*q^8 +... See example."
			],
			"reference": [
				"P. Di Francesco and J.-B. Zuber, SU(N) lattice integrable models associated with graphs, Nucl. Phys., B 338, pp 602--646, (1990)."
			],
			"link": [
				"R. Coquereaux, \u003ca href=\"https://arxiv.org/abs/1708.00560\"\u003eTheta functions for lattices of SU(3) hyper-roots\u003c/a\u003e, arXiv:1708.00560 [math.QA], 2017.",
				"A. Ocneanu, \u003ca href=\"https://cel.archives-ouvertes.fr/cel-00374414\"\u003eThe Classification of subgroups of quantum SU(N)\u003c/a\u003e, in \"Quantum symmetries in theoretical physics and mathematics\", Bariloche 2000, Eds. R. Coquereaux, A. Garcia. and R. Trinchero, AMS Contemporary Mathematics, 294, pp. 133-160, (2000). End of Sec 2.5."
			],
			"formula": [
				"G.f. = 1 + 512*x^3 + 11232*x^4 + 145920*x^5 + ...",
				"G.f. = 1 + 512*q^6 + 11232*q^8 + 145920*q^10 + ..."
			],
			"xref": [
				"Cf. A008434. {D_6}^{+} lattice is rescaled A_1(SU(3)).",
				"Cf. A290654 is A_2(SU(3)). Cf. A290655 is A_3(SU(3)). Cf. A287329 is A_4(SU(3). Cf. A287944 is A_5(SU(3)).",
				"Cf. A288488, A288489, A288779, A288909."
			],
			"keyword": "nonn,more",
			"offset": "0,4",
			"author": "_Robert Coquereaux_, Sep 01 2017",
			"references": 9,
			"revision": 12,
			"time": "2017-09-03T13:13:09-04:00",
			"created": "2017-09-01T23:48:56-04:00"
		}
	]
}