{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A093729",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 93729,
			"data": "1,0,1,0,1,1,0,2,2,1,0,7,7,3,1,0,41,41,15,4,1,0,397,397,123,26,5,1,0,6377,6377,1656,274,40,6,1,0,171886,171886,36987,4721,515,57,7,1,0,7892642,7892642,1391106,134899,10810,867,77,8,1",
			"name": "Square table T, read by antidiagonals, where T(n,k) gives the number of n-th generation descendents of a node labeled (k) in the tree of tournament sequences.",
			"comment": [
				"Column 1 forms A008934 (number of tournament sequences). Row sums form A093730.",
				"A tournament sequence is an increasing sequence of positive integers (t_1,t_2,...) such that t_1 = 1 and t_{i+1} \u003c= 2*t_i, where integer k\u003e1."
			],
			"link": [
				"M. Cook and M. Kleber, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v7i1r44\"\u003eTournament sequences and Meeussen sequences\u003c/a\u003e, Electronic J. Comb. 7 (2000), #R44.",
				"Michael Somos, \u003ca href=\"https://math.stackexchange.com/q/477910\"\u003eA functional power series equation\u003c/a\u003e, Mathematics StackExchange answer."
			],
			"formula": [
				"T(0, k)=1 for k\u003e=0, T(n, 0)=0 for n\u003e=1; else T(n, k)=T(n, k-1)-T(n-1, k)+T(n-1, 2*k-1)+T(n-1, 2*k) for k\u003c=n; else T(n, k)=Sum_{j=1..n+1} (-1)^(j-1)*C(n+1, j)*T(n, k-j) for k\u003en (Cook-Kleber).",
				"Column k of T equals column 0 of the matrix k-th power of triangle A097710, which satisfies the matrix recurrence: A097710(n, k) = [A097710^2](n-1, k-1) + [A097710^2](n-1, k) for n\u003ek\u003e=0."
			],
			"example": [
				"Array begins:",
				"[1,1,1,1,1,1,1,1,1,1,1,...],",
				"[0,1,2,3,4,5,6,7,8,9,10,...],",
				"[0,2,7,15,26,40,57,77,100,...],",
				"[0,7,41,123,274,515,867,1351,1988,...],",
				"[0,41,397,1656,4721,10810,21456,38507,64126,...],",
				"[0,397,6377,36987,134899,376175,880032,1818607,3426722,...],",
				"[0,6377,171886,1391106,6501536,...],",
				"[0,171886,7892642,...],",
				"[0,7892642,627340987,...],..."
			],
			"mathematica": [
				"t[n_?Negative, _] = 0; t[0, _] = 1; t[n_, k_] /; k \u003c= n := t[n, k] = t[n, k - 1] - t[n-1, k] + t[n - 1, 2 k - 1] + t[n - 1, 2 k]; t[n_, k_] := t[n, k] = Sum[(-1)^(j - 1)*Binomial[n + 1, j]*t[n, k - j], {j, 1, n + 1}]; Flatten[Table[t[i - k, k - 1], {i, 10}, {k, i}]] (* _Jean-François Alcover_, May 31 2011, after PARI prog. *)"
			],
			"program": [
				"(PARI) {T(n,k)=if(n\u003c0,0,if(n==0,1,if(k==0,0, if(k\u003c=n,T(n,k-1)-T(n-1,k)+T(n-1,2*k-1)+T(n-1,2*k), sum(j=1,n+1, (-1)^(j-1)*binomial(n+1,j)*T(n,k-j))))))}",
				"(PARI) {a(n, m) = my(A=1); for(k=1, n, A = (A - q^k * r * subst( subst(A, q, q^2), r, r^2)) / (1-q); subst(subst(A, r, q^(m-1)), q, 1)}; /* _Michael Somos_, Jun 19 2017 */"
			],
			"xref": [
				"Cf. A008934, A093730.",
				"Cf. A113080, A113081, A113092, A113103."
			],
			"keyword": "nonn,tabl",
			"offset": "0,8",
			"author": "_Paul D. Hanna_, Apr 14 2004; revised Oct 14 2005",
			"references": 9,
			"revision": 19,
			"time": "2021-03-12T22:24:42-05:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}