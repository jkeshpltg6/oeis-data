{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A327639",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 327639,
			"data": "1,1,1,1,1,2,1,1,4,6,3,1,6,15,16,6,1,10,45,88,76,24,1,14,93,282,420,302,84,1,21,223,1052,2489,3112,1970,498,1,29,444,2950,9865,18123,18618,10046,2220,1,41,944,9030,42787,112669,173338,155160,74938,15108",
			"name": "Number T(n,k) of proper k-times partitions of n; triangle T(n,k), n \u003e= 0, 0 \u003c= k \u003c= max(0,n-1), read by rows.",
			"comment": [
				"In each step at least one part is replaced by the partition of itself into smaller parts. The parts are not resorted.",
				"T(n,k) is defined for all n\u003e=0 and k\u003e=0.  The triangle displays only positive terms.  All other terms are zero.",
				"Row n is the inverse binomial transform of the n-th row of array A323718."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A327639/b327639.txt\"\u003eRows n = 0..170, flattened\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Iverson_bracket\"\u003eIverson bracket\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Partition_(number_theory)\"\u003ePartition (number theory)\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = Sum_{i=0..k} (-1)^(k-i) * binomial(k,i) * A323718(n,i).",
				"T(n,n-1) = A327631(n,n-1)/n = A327643(n) for n \u003e= 1.",
				"Sum_{k=1..n-1} k * T(n,k) = A327646(n).",
				"Sum_{k=0..max(0,n-1)} (-1)^k * T(n,k) = [n\u003c2], where [] is an Iverson bracket."
			],
			"example": [
				"T(4,0) = 1:  4",
				"T(4,1) = 4:     T(4,2) = 6:          T(4,3) = 3:",
				"  4-\u003e 31          4-\u003e 31 -\u003e 211        4-\u003e 31 -\u003e 211 -\u003e 1111",
				"  4-\u003e 22          4-\u003e 31 -\u003e 1111       4-\u003e 22 -\u003e 112 -\u003e 1111",
				"  4-\u003e 211         4-\u003e 22 -\u003e 112        4-\u003e 22 -\u003e 211 -\u003e 1111",
				"  4-\u003e 1111        4-\u003e 22 -\u003e 211",
				"                  4-\u003e 22 -\u003e 1111",
				"                  4-\u003e 211-\u003e 1111",
				"Triangle T(n,k) begins:",
				"  1;",
				"  1;",
				"  1,  1;",
				"  1,  2,   1;",
				"  1,  4,   6,    3;",
				"  1,  6,  15,   16,     6;",
				"  1, 10,  45,   88,    76,     24;",
				"  1, 14,  93,  282,   420,    302,     84;",
				"  1, 21, 223, 1052,  2489,   3112,   1970,    498;",
				"  1, 29, 444, 2950,  9865,  18123,  18618,  10046,  2220;",
				"  1, 41, 944, 9030, 42787, 112669, 173338, 155160, 74938, 15108;",
				"  ..."
			],
			"maple": [
				"b:= proc(n, i, k) option remember; `if`(n=0 or k=0, 1, `if`(i\u003e1,",
				"      b(n, i-1, k), 0) +b(i$2, k-1)*b(n-i, min(n-i, i), k))",
				"    end:",
				"T:= (n, k)-\u003e add(b(n$2, i)*(-1)^(k-i)*binomial(k, i), i=0..k):",
				"seq(seq(T(n, k), k=0..max(0, n-1)), n=0..12);"
			],
			"mathematica": [
				"b[n_, i_, k_] := b[n, i, k] = If[n == 0 || k == 0, 1, If[i \u003e 1, b[n, i - 1, k], 0] + b[i, i, k - 1] b[n - i, Min[n - i, i], k]];",
				"T[n_, k_] := Sum[b[n, n, i] (-1)^(k - i) Binomial[k, i], {i, 0, k}];",
				"Table[T[n, k], {n, 0, 12}, {k, 0, Max[0, n - 1] }] // Flatten (* _Jean-François Alcover_, Dec 09 2020, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Columns k=0-2 give A000012, A000065, A327769.",
				"Row sums give A327644.",
				"T(2n,n) gives A327645.",
				"Cf. A323718, A327631, A327643, A327646."
			],
			"keyword": "nonn,tabf",
			"offset": "0,6",
			"author": "_Alois P. Heinz_, Sep 20 2019",
			"references": 8,
			"revision": 41,
			"time": "2020-12-09T15:12:15-05:00",
			"created": "2019-09-20T15:24:56-04:00"
		}
	]
}