{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A341512",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 341512,
			"data": "0,1,2,11,2,36,4,85,46,58,2,324,4,120,120,575,2,693,4,566,248,172,6,2340,94,270,788,1176,2,1800,6,3661,348,358,336,5967,4,492,548,4210,2,3744,4,1820,2490,744,6,15372,380,2271,720,2826,6,11304,392,8760,992,946,2,15480,6,1232,5164,22631,636,5904,4,3866",
			"name": "a(n) = A341529(n) - A341528(n) = (sigma(n)*A003961(n)) - (n*sigma(A003961(n))).",
			"link": [
				"Antti Karttunen, \u003ca href=\"/A341512/b341512.txt\"\u003eTable of n, a(n) for n = 1..8191\u003c/a\u003e",
				"Antti Karttunen, \u003ca href=\"/A341512/a341512.txt\"\u003eData supplement: n, a(n) computed for n = 1..65537\u003c/a\u003e",
				"\u003ca href=\"/index/Pri#gaps\"\u003eIndex entries for primes, gaps between\u003c/a\u003e",
				"\u003ca href=\"/index/Pri#prime_indices\"\u003eIndex entries for sequences computed from indices in prime factorization\u003c/a\u003e",
				"\u003ca href=\"/index/Si#SIGMAN\"\u003eIndex entries for sequences related to sigma(n)\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A341529(n) - A341528(n) = (sigma(n)*A003961(n)) - (n*sigma(A003961(n))).",
				"For all primes p, a(p) = (q*(p+1)) - (p*(q+1)) = (pq + q) - (pq + p) = q - p = A001223(A000720(p)), where q = nextprime(p) = A003961(p).",
				"And in general, a(p^e) = (q^e * (p^(e+1)-1)/(p-1)) - ((p^e) * (q^(e+1)-1)/(q-1)), where q = A003961(p).",
				"Thus, a(p^2) = (p + 1)*q^2 - p^2*q - p^2,",
				"      a(p^3) = (p^2 + p + 1)*q^3 - p^3*q^2 - p^3*q - p^3,",
				"      a(p^4) = (p^3 + p^2 + p + 1)*q^4 - p^4*q^3 - p^4*q^2 - p^4*q - p^4,",
				"      etc."
			],
			"mathematica": [
				"Array[#2 DivisorSigma[1, #1] - #1 DivisorSigma[1, #2] \u0026 @@ {#, Times @@ Map[#1^#2 \u0026 @@ # \u0026, FactorInteger[#] /. {p_, e_} /; e \u003e 0 :\u003e {Prime[PrimePi@ p + 1], e}] - Boole[# == 1]} \u0026, 68] (* _Michael De Vlieger_, Feb 22 2021 *)"
			],
			"program": [
				"(PARI)",
				"A003961(n) = { my(f=factor(n)); for (i=1, #f~, f[i, 1] = nextprime(f[i, 1]+1)); factorback(f); }; \\\\ From A003961",
				"A341528(n) = (n*sigma(A003961(n)));",
				"A341529(n) = (sigma(n)*A003961(n));",
				"A341512(n) = (A341529(n)-A341528(n));"
			],
			"xref": [
				"Cf. A000203, A000720, A001223, A003961, A003973, A286385, A341528, A341529, A341530.",
				"Cf. Sequences A001359, A029710, A031924 give the positions of 2's, 4's and 6's in this sequence, or at least subsets of such positions."
			],
			"keyword": "nonn,look",
			"offset": "1,3",
			"author": "_Antti Karttunen_, Feb 22 2021",
			"references": 11,
			"revision": 23,
			"time": "2021-03-23T16:16:48-04:00",
			"created": "2021-02-25T16:57:17-05:00"
		}
	]
}