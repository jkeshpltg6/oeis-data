{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A081120",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 81120,
			"data": "1,2,0,4,0,0,4,1,0,0,4,0,2,0,2,0,0,2,2,2,0,0,2,0,2,4,1,6,0,0,0,0,0,0,2,0,0,0,6,2,0,0,0,2,2,0,6,4,2,0,0,0,4,2,4,2,0,0,0,4,2,0,4,1,0,0,2,0,0,0,2,2,0,2,0,4,0,0,2,0,2,0,2,0,0,0,2,0,2,0,0,0,0,0,2,0,0,0,0,6",
			"name": "Number of integral solutions to Mordell's equation y^2 = x^3 - n.",
			"comment": [
				"Mordell's equation has a finite number of integral solutions for all nonzero n.",
				"Gebel, Pethö, and Zimmer (1998) computed the solutions for |n| \u003c= 10^4. Bennett and Ghadermarzi (2015) extended this bound to |n| \u003c= 10^7.",
				"Sequence A081121 gives n for which there are no integral solutions. See A081119 for the number of integral solutions to y^2 = x^3 + n."
			],
			"reference": [
				"T. M. Apostol, Introduction to Analytic Number Theory, Springer-Verlag, page 191."
			],
			"link": [
				"Jean-François Alcover, \u003ca href=\"/A081120/b081120.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e [There were errors in the previous b-file, which had 10000 terms contributed by T. D. Noe and based on the work of J. Gebel.]",
				"M. A. Bennett and A. Ghadermarzi, \u003ca href=\"http://www.math.ubc.ca/~bennett/BeGh-LMSJCM-2015.pdf\"\u003eMordell's equation : a classical approach\u003c/a\u003e. LMS J. Compute. Math. 18 (2015): 633-646. \u003ca href=\"https://doi.org/10.1112%2FS1461157015000182\"\u003edoi:10.1112/S1461157015000182\u003c/a\u003e \u003ca href=\"https://arxiv.org/abs/1311.7077\"\u003earXiv:1311.7077\u003c/a\u003e",
				"J. Gebel, A. Pethö, and H. G. Zimmer, \u003ca href=\"https://doi.org/10.1023%2FA%3A1000281602647\"\u003eOn Mordell's equation\u003c/a\u003e, Compositio Mathematica. 110:3 (1998): 335-367.",
				"J. Gebel, \u003ca href=\"/A001014/a001014.txt\"\u003eInteger points on Mordell curves\u003c/a\u003e [Cached copy, after the original web site tnt.math.se.tmu.ac.jp was shut down in 2017]",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/MordellCurve.html\"\u003eMordell Curve\u003c/a\u003e"
			],
			"mathematica": [
				"(* This naive approach gives correct results up to n=1000 *) xmax[_] = 10^4; Do[ xmax[n] = 10^5, {n, {366, 775, 999}}]; Do[ xmax[n] = 10^6, {n, {207, 307, 847}}]; f[n_] := (x = Floor[n^(1/3)] - 1; s = {}; While[ x \u003c= xmax[n], x++; y2 = x^3 - n; If[y2 \u003e= 0, y = Sqrt[y2]; If[ IntegerQ[y], AppendTo[s, y]]]]; s); a[n_] := (fn = f[n]; If[fn == {}, 0, 2 Length[fn] - If[ First[fn] == 0, 1, 0]]); Table[ an = a[n]; Print[\"a[\", n, \"] = \", an]; an, {n, 1, 100}] (* _Jean-François Alcover_, Mar 06 2012 *)"
			],
			"xref": [
				"Cf. A081119, A081121."
			],
			"keyword": "nice,nonn",
			"offset": "1,2",
			"author": "_T. D. Noe_, Mar 06 2003",
			"ext": [
				"Edited by _Max Alekseyev_, Feb 06 2021"
			],
			"references": 24,
			"revision": 35,
			"time": "2021-07-17T04:29:53-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}