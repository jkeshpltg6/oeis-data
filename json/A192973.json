{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A192973",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 192973,
			"data": "1,3,10,23,47,88,157,271,458,763,1259,2064,3369,5483,8906,14447,23415,37928,61413,99415,160906,260403,421395,681888,1103377,1785363,2888842,4674311,7563263,12237688,19801069,32038879,51840074,83879083",
			"name": "Constant term of the reduction by x^2 -\u003e x+1 of the polynomial p(n,x) defined at Comments.",
			"comment": [
				"The titular polynomials are defined recursively: p(n,x) = x*p(n-1,x) + 1 +2*n^2, with p(0,x)=1. For an introduction to reductions of polynomials by substitutions such as x^2 -\u003e x+1, see A192232 and A192744."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A192973/b192973.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-2,-1,1)."
			],
			"formula": [
				"a(n) = 3*a(n-1) - 2*a(n-2) - a(n-3) + a(n-4).",
				"G.f.: x*(1+3*x^2)/((1-x-x^2)*(1-x)^2). - _R. J. Mathar_, May 11 2014",
				"a(n) = Lucas(n+4) - Fibonacci(n-1) - 2*(2*n+3). - _Ehren Metcalfe_, Jul 13 2019"
			],
			"mathematica": [
				"(* First program *)",
				"q = x^2; s = x + 1; z = 40;",
				"p[0, x]:= 1;",
				"p[n_, x_]:= x*p[n-1, x] + 2*n^2 +1;",
				"Table[Expand[p[n, x]], {n, 0, 7}]",
				"reduce[{p1_, q_, s_, x_}]:= FixedPoint[(s PolynomialQuotient @@ #1 + PolynomialRemainder @@ #1 \u0026)[{#1, q, x}] \u0026, p1]",
				"t = Table[reduce[{p[n, x], q, s, x}], {n, 0, z}];",
				"u1 = Table[Coefficient[Part[t, n], x, 0], {n, 1, z}] (* A192973 *)",
				"u2 = Table[Coefficient[Part[t, n], x, 1], {n, 1, z}] (* A192974 *)",
				"(* Additional programs *)",
				"LinearRecurrence[{3, -2, -1, 1}, {1, 3, 10, 23}, 50] (* _Vincenzo Librandi_, Jul 14 2019 *)",
				"With[{F = Fibonacci}, Table[F[n+4]+3*F[n+2] -2*(2*n+3), {n,40}]] (* _G. C. Greubel_, Jul 24 2019 *)"
			],
			"program": [
				"(MAGMA) [Lucas(n+4)-Fibonacci(n-1)-2*(2*n+3): n in [1..40]]; // _Vincenzo Librandi_, Jul 14 2019",
				"(PARI) vector(40, n, f=fibonacci; f(n+4)+3*f(n+2) -2*(2*n+3)) \\\\ _G. C. Greubel_, Jul 24 2019",
				"(Sage) f=fibonacci; [f(n+4)+3*f(n+2) -2*(2*n+3) for n in (1..40)] # _G. C. Greubel_, Jul 24 2019",
				"(GAP) F:=Fibonacci;; List([1..40], n-\u003e F(n+4)+3*F(n+2) -2*(2*n+3)); # _G. C. Greubel_, Jul 24 2019"
			],
			"xref": [
				"Cf. A000032, A000045, A192232, A192744, A192951, A192974."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Jul 13 2011",
			"references": 3,
			"revision": 25,
			"time": "2019-08-21T01:22:27-04:00",
			"created": "2011-07-13T20:16:38-04:00"
		}
	]
}