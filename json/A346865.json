{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A346865",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 346865,
			"data": "1,12,24,56,78,144,112,360,234,360,384,672,434,960,720,992,864,1872,760,2352,1344,1584,1872,2880,1767,3024,2160,4032,2400,4320,1984,6552,4032,3672,4608,6552,2812,7440,5376,7200,5082,8064,4752,10080,7020,8064,6144",
			"name": "Sum of divisors of the n-th hexagonal number.",
			"comment": [
				"The characteristic shape of the symmetric representation of a(n) consists in that in the main diagonal of the diagram the smallest Dyck path has a valley and the largest Dyck path has a peak.",
				"So knowing this characteristic shape we can know if a number is an hexagonal number (or not) just by looking at the diagram, even ignoring the concept of hexagonal number.",
				"Therefore we can see a geometric pattern of the distribution of the hexagonal numbers in the stepped pyramid described in A245092."
			],
			"formula": [
				"a(n) = A000203(A000384(n)).",
				"Sum_{k=1..n} a(k) ~ 4*n^3/3. - _Vaclav Kotesovec_, Aug 18 2021"
			],
			"example": [
				"a(3) = 24 because the sum of divisors of the third hexagonal number (i.e., 15) is 1 + 3 + 5 + 15 = 24.",
				"On the other hand we can see that in the main diagonal of every diagram the smallest Dyck path has a valley and the largest Dyck path has a peak as shown below.",
				"Illustration of initial terms:",
				"-------------------------------------------------------------------------",
				"  n  H(n)  a(n)  Diagram",
				"-------------------------------------------------------------------------",
				"                 _         _                 _                         _",
				"  1    1    1   |_|       | |               | |                       | |",
				"                          | |               | |                       | |",
				"                       _ _| |               | |                       | |",
				"                      |    _|               | |                       | |",
				"                 _ _ _|  _|                 | |                       | |",
				"  2    6   12   |_ _ _ _|                   | |                       | |",
				"                                            | |                       | |",
				"                                       _ _ _|_|                       | |",
				"                                   _ _| |                             | |",
				"                                  |    _|                             | |",
				"                                 _|  _|                               | |",
				"                                |_ _|                                 | |",
				"                                |                                     | |",
				"                 _ _ _ _ _ _ _ _|                            _ _ _ _ _| |",
				"  3   15   24   |_ _ _ _ _ _ _ _|                           |  _ _ _ _ _|",
				"                                                            | |",
				"                                                         _ _| |",
				"                                                     _ _|  _ _|",
				"                                                    |    _|",
				"                                                   _|  _|",
				"                                                  |  _|",
				"                                             _ _ _| |",
				"                                            |  _ _ _|",
				"                                            | |",
				"                                            | |",
				"                                            | |",
				"                 _ _ _ _ _ _ _ _ _ _ _ _ _ _| |",
				"  4   28   56   |_ _ _ _ _ _ _ _ _ _ _ _ _ _ _|",
				".",
				"Column H gives the nonzero hexagonal numbers (A000384).",
				"a(n) is also the area (and the number of cells) of the n-th diagram.",
				"For n = 3 the sum of the regions (or parts) of the third diagram is 8 + 8 + 8 = 24, so a(3) = 24.",
				"For more information see A237593."
			],
			"mathematica": [
				"a[n_] := DivisorSigma[1, n*(2*n - 1)]; Array[a, 50] (* _Amiram Eldar_, Aug 18 2021 *)"
			],
			"program": [
				"(PARI) a(n) = sigma(n*(2*n-1)); \\\\ _Michel Marcus_, Aug 18 2021",
				"(Python)",
				"from sympy import divisors",
				"def a(n): return sum(divisors(n*(2*n - 1)))",
				"print([a(n) for n in range(1, 48)]) # _Michael S. Branicky_, Aug 20 2021"
			],
			"xref": [
				"Bisection of A074285.",
				"Cf. A000203, A000384, A237591, A237593, A245092, A262626, A346864.",
				"Some sequences that gives sum of divisors: A000225 (of powers of 2), A008864 (of prime numbers), A065764 (of squares), A073255 (of composites), A074285 (of triangular numbers, also of generalized hexagonal numbers), A139256 (of perfect numbers), A175926 (of cubes), A224613 (of multiples of 6), A346866 (of second hexagonal numbers), A346867 (of numbers with middle divisors), A346868 (of numbers with no middle divisors)."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Omar E. Pol_, Aug 17 2021",
			"references": 5,
			"revision": 49,
			"time": "2021-12-10T11:18:53-05:00",
			"created": "2021-08-17T19:37:06-04:00"
		}
	]
}