{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A145895",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 145895,
			"data": "1,1,1,0,1,1,2,1,0,1,2,4,2,4,1,0,1,4,8,10,8,4,6,1,0,1,8,20,26,24,25,12,7,8,1,0,1,17,48,70,84,70,54,47,16,11,10,1,0,1,37,116,197,244,241,224,141,104,76,20,16,12,1,0,1,82,286,535,728,816,734,609,472,246,180,112,24,22",
			"name": "Triangle read by rows: T(n,k) is the number of Dyck paths of semilength n and having k UDU and DUD's (here U=(1,1), D=(1,-1); 0 \u003c= k \u003c= 2n-2).",
			"comment": [
				"Row n contains 2n-1 entries (n\u003e=1).",
				"Row sums are the Catalan numbers (A000108).",
				"T(n,0) = A004148(n-1) (the secondary structure numbers).",
				"Sum_{k=0..2n-2} k*T(n,k) = 2*binomial(2n-2, n-2) = 2*A001791(n-1)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A145895/b145895.txt\"\u003eRows n = 0..100, flattened\u003c/a\u003e",
				"Andrei Asinowski, Cyril Banderier, \u003ca href=\"https://doi.org/10.4230/LIPIcs.AofA.2020.1\"\u003eOn Lattice Paths with Marked Patterns: Generating Functions and Multivariate Gaussian Distribution\u003c/a\u003e, 31st International Conference on Probabilistic, Combinatorial and Asymptotic Methods for the Analysis of Algorithms (AofA 2020) Leibniz International Proceedings in Informatics (LIPIcs) Vol. 159, 1:1-1:16."
			],
			"formula": [
				"G.f.: c(z/(1+(1-t^2)z+(1-t)^2*z^2)), where c(z) = (1-sqrt(1-4z))*(2z) is the g.f. of the Catalan numbers (A000108).",
				"The trivariate g.f., with z marking semilength, t marking number of UDU's and s marking number of DUD's is c(z/(1+(1-ts)*z + (1-t)(1-s)z^2)), where c(z) = (1-sqrt(1-4z))*(2z) is the g.f. of the Catalan numbers (A000108)."
			],
			"example": [
				"T(4,3) = 4 because we have UDUDUUDD, UUDDUDUD, UDUUDUDD and UUDUDDUD.",
				"Triangle starts:",
				"  1;",
				"  1;",
				"  1,  0,  1;",
				"  1,  2,  1,  0,  1;",
				"  2,  4,  2,  4,  1,  0,  1;",
				"  4,  8, 10,  8,  4,  6,  1,  0,  1;"
			],
			"maple": [
				"c := proc (z) options operator, arrow: (1/2-(1/2)*sqrt(1-4*z))/z end proc: G := c(z/(1+(1-t^2)*z+(1-t)^2*z^2)): Gser := simplify(series(G, z = 0, 12)): for n from 0 to 9 do P[n] := sort(coeff(Gser, z, n)) end do: 1; for n to 9 do seq(coeff(P[n], t, k), k = 0 .. 2*n-2) end do; # yields sequence in triangular form",
				"# second Maple program:",
				"b:= proc(x, y, t) option remember; `if`(y\u003c0 or y\u003ex, 0,",
				"     `if`(x=0, 1, expand(b(x-1, y+1, [2, 2, 4, 2, 4][t])*",
				"     `if`(t=3, z, 1)+b(x-1, y-1, [5, 3, 5, 3, 5][t])*`if`(t=4, z, 1))))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, z, i), i=0..degree(p)))(b(2*n, 0, 1)):",
				"seq(T(n), n=1..12);  # _Alois P. Heinz_, Jun 04 2014"
			],
			"mathematica": [
				"b[x_, y_, t_] := b[x, y, t] = If[y\u003c0 || y\u003ex, 0, If[x == 0, 1, Expand[b[x-1, y+1, {2, 2, 4, 2, 4}[[t]]]*If[t == 3, z, 1] + b[x-1, y-1, {5, 3, 5, 3, 5}[[t]]]*If[t == 4, z, 1]]]]; T[n_] := Function[{p}, Table[Coefficient[p, z, i], {i, 0, Exponent[p, z]}]][b[2*n, 0, 1]]; Table[T[n], {n, 0, 12}] // Flatten (* _Jean-François Alcover_, May 27 2015, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A000108, A004148, A001791."
			],
			"keyword": "nonn,tabf",
			"offset": "0,7",
			"author": "_Emeric Deutsch_, Dec 10 2008",
			"references": 1,
			"revision": 22,
			"time": "2020-08-27T19:34:08-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}