{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A246375",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 246375,
			"data": "1,2,3,4,5,6,9,8,7,10,15,12,11,18,21,16,25,14,27,20,13,30,81,24,17,22,45,36,23,42,39,32,19,50,51,28,35,54,99,40,55,26,33,60,37,162,129,48,49,34,75,44,29,90,87,72,41,46,135,84,47,78,189,64,65,38,63,100,95,102,153,56,31,70",
			"name": "Permutation of natural numbers: a(1) = 1, a(2n) = 2*a(n), a(2n+1) = A003961(1+a(n)). [Where A003961(n) shifts the prime factorization of n one step towards larger primes].",
			"comment": [
				"This can be viewed as yet another \"entanglement permutation\" where the two complementary pairs to be interwoven together are even and odd numbers (A005843/A005408) which are entangled with the complementary pair even numbers (taken straight) and odd numbers in the order they appear in A003961: (A005843/A003961). Sequence A163511 has almost the same definition, but its domain starts from 0, which results a different permutation."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A246375/b246375.txt\"\u003eTable of n, a(n) for n = 1..16384\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"a(1) = 1, a(2n) = 2*a(n), a(2n+1) = A003961(1+a(n)). [Where A003961(n) shifts the prime factorization of n one step towards larger primes].",
				"As a composition of related permutations:",
				"a(n) = A246379(A246378(n)).",
				"Other identities. For all n \u003e= 1 the following holds:",
				"A000035(a(n)) = A000035(n). [Like A005940 \u0026 A005941, this also preserves the parity]."
			],
			"program": [
				"(PARI)",
				"default(primelimit, (2^31)+(2^30));",
				"A003961(n) = my(f = factor(n)); for (i=1, #f~, f[i, 1] = nextprime(f[i, 1]+1)); factorback(f); \\\\ Using code of _Michel Marcus_",
				"A246375(n) = if(1==n, 1, if(!(n%2), 2*A246375(n/2), A003961(1+A246375((n-1)/2))));",
				"for(n=1, 16384, write(\"b246375.txt\", n, \" \", A246375(n)));",
				"(Scheme, with memoizing definec-macro)",
				"(definec (A246375 n) (cond ((\u003c= n 1) n) ((even? n) (* 2 (A246375 (/ n 2)))) (else (A003961 (+ 1 (A246375 (/ (- n 1) 2)))))))"
			],
			"xref": [
				"Inverse: A246376.",
				"Similar or related permutations: A005940, A005941, A163511, A245606, A246378, A246379.",
				"Cf. A000035, A003961, A005408, A005843."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Antti Karttunen_, Aug 27 2014",
			"references": 8,
			"revision": 13,
			"time": "2014-09-01T11:46:54-04:00",
			"created": "2014-09-01T11:46:54-04:00"
		}
	]
}