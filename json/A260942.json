{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A260942",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 260942,
			"data": "0,1,-2,0,1,0,0,1,0,0,0,-2,0,1,-2,0,2,0,0,1,0,0,1,-2,0,0,0,0,1,-2,0,0,0,0,1,0,0,2,-2,0,1,-2,0,1,0,0,1,-2,0,1,0,0,1,0,0,0,-2,0,2,0,0,0,0,0,0,-2,0,1,-2,0,0,0,0,2,-2,0,1,0,0,3,0,0,1",
			"name": "Expansion of x * phi(-x) * psi(x^12) / chi(-x^3) in powers of x where phi(), psi(), chi() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A260942/b260942.txt\"\u003eTable of n, a(n) for n = 0..2500\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(-5/8) * eta(q)^2 * eta(q^6) * eta(q^24)^2 / (eta(q^2) * eta(q^3) * eta(q^12)) in powers of q.",
				"Euler transform of period 24 sequence [ -2, -1, -1, -1, -2, -1, -2, -1, -1, -1, -2, 0, -2, -1, -1, -1, -2, -1, -2, -1, -1, -1, -2, -2, ...].",
				"a(3*n) = 0. a(3*n + 1) = A131963(n). a(3*n + 2) = -2 * A112609(n)."
			],
			"example": [
				"G.f. = x - 2*x^2 + x^4 + x^7 - 2*x^11 + x^13 - 2*x^14 + 2*x^16 + x^19 + ...",
				"G.f. = q^13 - 2*q^21 + q^37 + q^61 - 2*q^93 + q^109 - 2*q^117 + 2*q^133 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 4, 0, x] EllipticTheta[ 2, 0, x^6] QPochhammer[ -x^3, x^3] / (2 x^(1/2)), {x, 0, n}];",
				"eta[q_] := q^(1/24)*QPochhammer[q]; CoefficientList[Series[ q^(-5/8)* eta[q]^2*eta[q^6]*eta[q^24]^2/(eta[q^2]*eta[q^3]*eta[q^12]), {q, 0, 50}], q] (* _G. C. Greubel_, Aug 01 2018 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c1, 0, n--; A = x * O(x^n); polcoeff( eta(x + A)^2 * eta(x^6 + A) * eta(x^24 + A)^2 / (eta(x^2 + A) * eta(x^3 + A) * eta(x^12 + A)), n))};"
			],
			"xref": [
				"Cf. A112609, A131963."
			],
			"keyword": "sign",
			"offset": "0,3",
			"author": "_Michael Somos_, Aug 04 2015",
			"references": 2,
			"revision": 8,
			"time": "2021-03-12T22:24:48-05:00",
			"created": "2015-08-04T18:38:29-04:00"
		}
	]
}