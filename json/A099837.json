{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A099837",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 99837,
			"data": "1,-1,-1,2,-1,-1,2,-1,-1,2,-1,-1,2,-1,-1,2,-1,-1,2,-1,-1,2,-1,-1,2,-1,-1,2,-1,-1,2,-1,-1,2,-1,-1,2,-1,-1,2,-1,-1,2,-1,-1,2,-1,-1,2,-1,-1,2,-1,-1,2,-1,-1,2,-1,-1,2,-1,-1,2,-1,-1,2,-1,-1,2,-1,-1",
			"name": "Expansion of (1 - x^2) / (1 + x + x^2) in powers of x.",
			"comment": [
				"A transform of (-1)^n.",
				"Row sums of Riordan array ((1-x)/(1+x), x/(1+x)^2), A110162.",
				"Let b(n) = Sum_{k=0..floor(n/2)} binomial(n-k,k)(-1)^(n-2k). Then a(n) = b(n) - b(n-2) = A049347(n) - A049347(n-2) (n \u003e 0). The g.f. 1/(1+x) of (-1)^n is transformed to (1-x^2)/(1+x+x^2) under the mapping G(x)-\u003e((1-x^2)/(1+x^2))G(x/(1+x^2)). Partial sums of A099838.",
				"A(n) = a(n+3) (or a(n) if a(0) is replaced by 2) appears, together with B(n) = A049347(n) in the formula 2*exp(2*Pi*n*i/3) = A(n) + B(n)*sqrt(3)*i, n \u003e= 0, with i = sqrt(-1). See A164116 for the case N=5. - _Wolfdieter Lang_, Feb 27 2014"
			],
			"link": [
				"Michael Somos, \u003ca href=\"http://grail.eecs.csuohio.edu/~somos/rfmc.txt\"\u003eRational Function Multiplicative Coefficients\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (-1,-1)."
			],
			"formula": [
				"G.f.: (1-x^2)/(1+x+x^2).",
				"Euler transform of length 3 sequence [-1, -1, 1]. - _Michael Somos_, Mar 21 2011",
				"Moebius transform is length 3 sequence [-1, 0, 3]. - _Michael Somos_, Mar 22 2011",
				"a(n) = -b(n) where b(n) = A061347(n) is multiplicative with b(3^e) = -2 if e \u003e 0, b(p^e) = 1 otherwise. - _Michael Somos_, Jan 19 2012",
				"a(n) = a(-n). a(n) = c_3(n) if n \u003e 1 where c_k(n) is Ramanujan's sum. - _Michael Somos_, Mar 21 2011",
				"G.f.: (1 - x) * (1 - x^2) / (1 - x^3). a(n) = -a(n-1) - a(n-2) unless n = 0, 1, 2. - _Michael Somos_, Jan 19 2012",
				"Dirichlet g.f.: Sum_{n\u003e=1} a(n)/n^s = zeta(s)*(3^(1-s)-1). - _R. J. Mathar_, Apr 11 2011",
				"a(n+3) = R(n,-1) for n \u003e= 0, with the monic Chebyshev T-polynomials R with coefficient table A127672. - _Wolfdieter Lang_, Feb 27 2014",
				"For n \u003e 0, a(n) = 2*cos(n*Pi/3)*cos(n*Pi). - _Wesley Ivan Hurt_, Sep 25 2017"
			],
			"example": [
				"G.f. = 1 - x - x^2 + 2*x^3 - x^4 - x^5 + 2*x^6 - x^7 - x^8 + 2*x^9 - x^10 + ..."
			],
			"mathematica": [
				"a[0] = 1; a[n_] := Mod[n+2, 3] - Mod[n, 3]; A099837 = Table[a[n], {n, 0, 71}](* _Jean-François Alcover_, Feb 15 2012, after _Michael Somos_ *)",
				"LinearRecurrence[{-1, -1}, {1, -1, -1}, 50] (* _G. C. Greubel_, Aug 08 2017 *)"
			],
			"program": [
				"(PARI) {a(n) = [2, -1, -1][n%3 + 1] - (n == 0)}; /* _Michael Somos_, Jan 19 2012 */",
				"(Maxima) A099837(n) := block(",
				"        if n = 0 then 1 else [2,-1,-1][1+mod(n,3)]",
				")$ /* _R. J. Mathar_, Mar 19 2012 */",
				"(PARI) Vec((1-x^2)/(1+x+x^2) + O(x^20)) \\\\ _Felix Fröhlich_, Aug 08 2017"
			],
			"xref": [
				"Cf. A061347, A100051, A100063, A098554."
			],
			"keyword": "easy,sign",
			"offset": "0,4",
			"author": "_Paul Barry_, Oct 27 2004",
			"references": 44,
			"revision": 58,
			"time": "2021-03-12T22:24:43-05:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}