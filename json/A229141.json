{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A229141",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 229141,
			"data": "1,0,0,2,0,1,0,5,35,0",
			"name": "Number of circular permutations i_1, ..., i_n of 1, ..., n such that all the n sums i_1^2+i_2, ..., i_{n-1}^2+i_n, i_n^2+i_1 are among those integers m with the Jacobi symbol (m/(2n+1)) equal to 1.",
			"comment": [
				"Conjecture: a(n) \u003e 0 if 2*n+1 is a prime greater than 11.",
				"Zhi-Wei Sun also made the following conjectures:",
				"(1) For any prime p = 2*n+1 \u003e 11, there is a circular permutation i_1, ..., i_n of 1, ..., n such that all the n numbers i_1^2-i_2, i_2^2-i_3, ..., i_{n-1}^2-i_n, i_n^2-i_1 are quadratic residues modulo p.",
				"(2)  Let p = 2*n+1 be an odd prime. If p \u003e 13 (resp., p \u003e 11), then there is a circular permutation i_1, ..., i_n of 1, ..., n such that all the n numbers i_1^2+i_2, i_2^2+i_3, ..., i_{n-1}^2+i_n, i_n^2+i_1 (resp., the n numbers i_1^2-i_2, i_2^2-i_3, ..., i_{n-1}^2-i_n, i_n^2-i_1) are primitive roots modulo p.",
				"(3)  Let p = 2*n+1 be an odd prime. If p \u003e 19 (resp. p \u003e 13), then there is a circular permutation i_1, ..., i_n of 1, ..., n such that all the n numbers i_1^2+i_2^2, i_2^2+i_3^2, ..., i_{n-1}^2+i_n^2, i_n^2+i_1^2 (resp., the n numbers i_1^2-i_2^2, i_2^2-i_3^2, ..., i_{n-1}^2-i_n^2, i_n^2-i_1^2) are primitive roots modulo p.",
				"See also the linked arXiv paper of Sun for more conjectures involving primitive roots modulo primes."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1309.1679\"\u003eSome new problems in additive combinatorics\u003c/a\u003e, preprint, arXiv:1309.1679 [math.NT], 2013-2014."
			],
			"example": [
				"a(4) = 2 due to the permutations (1,3,2,4) and (1,4,3,2).",
				"a(6) = 1 due to the permutation (1,3,5,2,6,4).",
				"a(8) = 5 due to the permutations",
				"   (1,3,4,2,5,8,6,7), (1,8,3,6,2,4,5,7), (1,8,3,6,7,4,2,5),",
				"   (1,8,3,7,6,2,4,5), (1,8,6,7,3,4,2,5).",
				"a(9) \u003e 0 due to the permutation (1,3,7,6,8,4,9,2,5)."
			],
			"mathematica": [
				"(* A program to compute the desired circular permutations for n = 8. *)",
				"f[i_,j_,p_]:=f[i,j,p]=JacobiSymbol[i^2+j,p]==1",
				"V[i_]:=Part[Permutations[{2,3,4,5,6,7,8}],i]",
				"m=0",
				"Do[Do[If[f[If[j==0,1,Part[V[i],j]],If[j\u003c7,Part[V[i],j+1],1],17]==False,Goto[aa]],{j,0,7}];",
				"m=m+1;Print[m,\":\",\" \",1,\" \",Part[V[i],1],\" \",Part[V[i],2],\" \",Part[V[i],3],\" \",Part[V[i],4],\" \",Part[V[i],5],\" \",Part[V[i],6],\" \",Part[V[i],7]];Label[aa];Continue,{i,1,7!}]"
			],
			"xref": [
				"Cf. A229038, A229082, A229130, A229005."
			],
			"keyword": "nonn,more,hard",
			"offset": "1,4",
			"author": "_Zhi-Wei Sun_, Sep 15 2013",
			"ext": [
				"a(10) = 0 from  _R. J. Mathar_, Sep 15 2013"
			],
			"references": 2,
			"revision": 14,
			"time": "2019-08-04T09:55:39-04:00",
			"created": "2013-09-16T18:56:54-04:00"
		}
	]
}