{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A133089",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 133089,
			"data": "1,3,0,-5,0,0,-7,0,0,0,9,0,0,0,0,11,0,0,0,0,0,-13,0,0,0,0,0,0,-15,0,0,0,0,0,0,0,17,0,0,0,0,0,0,0,0,19,0,0,0,0,0,0,0,0,0,-21,0,0,0,0,0,0,0,0,0,0,-23,0,0,0,0,0,0,0,0,0,0,0,25,0,0,0,0,0,0,0,0,0,0,0,0,27,0,0,0,0,0,0,0",
			"name": "Expansion of f(x)^3 in powers of x where f() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"reference": [
				"S. Ramanujan, Notebooks, Tata Institute of Fundamental Research, Bombay 1957 Vol. 1, see page 266.  MR0099904 (20 #6340)"
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A133089/b133089.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(-1/8) * (eta(q^2)^3 / (eta(q) * eta(q^4)))^3 in powers of q.",
				"Euler transform of period 4 sequence [ 3, -6, 3, -3, ...].",
				"a(n) = b(8*n+1) where b(n) is multiplicative and b(2^e) = 0^e, b(p^e) = (1 + (-1)^e)/2 * p^(e/2) if p == 1, 3 (mod 8), b(p^e) = (1 + (-1)^e)/2 * (-p)^(e/2) if p == 5, 7 (mod 8).",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (256 t)) = 64 (t/i)^(3/2) f(t) where q = exp(2 Pi i t).",
				"a(3*n + 2) = a(5*n + 2) = a(5*n + 4) = a(9*n + 4) = a(9*n + 7) = 0. a(9*n + 1) = 3 * a(n). a(25*n + 3) = -5 * a(n).",
				"G.f.: Sum_{k\u003e=0} (-1)^floor(k/2) * (2*k + 1) * x^(k*(k + 1))/2.",
				"G.f.: ( Product_{k\u003e0} (1 - x^k) * (1 + x^k)^2 / (1 + x^(2*k)) )^3.",
				"a(n) = -(-1)^n * A010816(n). a(3*n) = A133079(n)."
			],
			"example": [
				"G.f. = 1 + 3*x - 5*x^3 - 7*x^6 + 9*x^10 + 11*x^15 - 13*x^21 - 15*x^28 + ...",
				"G.f. = q + 3*q^9 - 5*q^25 - 7*q^49 + 9*q^81 + 11*q^121 - 13*q^169 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ -x]^3, {x, 0, n}]; (* _Michael Somos_, Jun 19 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, 0, if( issquare( 8*n+1, \u0026n), (-1)^( (n-1) \\ 4) * n))};",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( (eta(x^2 + A)^3 / (eta(x + A) * eta(x^4 + A)))^3, n))};"
			],
			"xref": [
				"Cf. A010816, A133079."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Sep 09 2007",
			"references": 5,
			"revision": 12,
			"time": "2021-03-12T22:24:44-05:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}