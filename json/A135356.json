{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A135356",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 135356,
			"data": "2,2,0,3,-3,2,4,-6,4,0,5,-10,10,-5,2,6,-15,20,-15,6,0,7,-21,35,-35,21,-7,2,8,-28,56,-70,56,-28,8,0,9,-36,84,-126,126,-84,36,-9,2,10,-45,120,-210,252,-210,120,-45,10,0,11,-55,165,-330,462,-462,330,-165,55,-11,2",
			"name": "Triangle T(p,s) read by rows: coefficients in the recurrence of sequences which equal their p-th differences.",
			"comment": [
				"Sequences which equal their p-th differences obey recurrences a(n)=sum(s=1..p) T(p,s)*a(n-s).",
				"This defines T(p,s) as essentially a signed version of a chopped Pascal triangle A014410, see A130785.",
				"For cases like p=2, 4, 6, 8, 10, 12, 14, the denominator of the rational generating function of a(n) contains a factor 1-x; depending on the first terms in the sequences a(n), additional, simpler recurrences may exist if this cancels with a factor in the numerator. - _R. J. Mathar_, Jun 10 2008",
				"Row sums are 2."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A135356/b135356.txt\"\u003eRows n = 1..141, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(p,s) = (-1)^(s+1)*A007318(p,s), 1\u003c=s\u003cp. T(p,p) = 0 if p even. T(p,p) = 2 if p odd."
			],
			"example": [
				"Triangle begins with row p=1:",
				"2;",
				"2, 0;",
				"3, -3, 2;",
				"4, -6, 4, 0;",
				"5, -10, 10, -5, 2;",
				"Examples of p=1: A000079, of p=2: A131577, of p=3: A131708, A130785, A131562, A057079, of p=4: A000749, A038503, A009545, A038505, of p=5: A133476, of p=6: A140343, of p=7: A140342."
			],
			"maple": [
				"T:= (p, s)-\u003e  `if`(p=s, 2*irem(p, 2), (-1)^(s+1) *binomial(p, s)):",
				"seq(seq(T(p, s), s=1..p), p=1..11);  # _Alois P. Heinz_, Aug 26 2011"
			],
			"mathematica": [
				"T[p_, s_] := If[p == s, 2*Mod[s, 2], (-1)^(s+1)*Binomial[p, s]]; Table[T[p, s], {p, 1, 11}, {s, 1, p}] // Flatten (* _Jean-François Alcover_, Feb 19 2015, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A130785."
			],
			"keyword": "sign,tabl",
			"offset": "1,1",
			"author": "_Paul Curtz_, Dec 08 2007, Mar 25 2008, Apr 28 2008",
			"ext": [
				"Edited by _R. J. Mathar_, Jun 10 2008"
			],
			"references": 13,
			"revision": 20,
			"time": "2015-02-23T20:14:50-05:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}