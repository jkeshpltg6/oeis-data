{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A038548",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 38548,
			"data": "1,1,1,2,1,2,1,2,2,2,1,3,1,2,2,3,1,3,1,3,2,2,1,4,2,2,2,3,1,4,1,3,2,2,2,5,1,2,2,4,1,4,1,3,3,2,1,5,2,3,2,3,1,4,2,4,2,2,1,6,1,2,3,4,2,4,1,3,2,4,1,6,1,2,3,3,2,4,1,5,3,2,1,6,2,2,2,4,1,6,2,3,2,2,2,6,1,3,3,5,1,4,1,4,4",
			"name": "Number of divisors of n that are at most sqrt(n).",
			"comment": [
				"Number of ways to arrange n identical objects in a rectangle, modulo rotation.",
				"Number of unordered solutions of x*y = n. - _Colin Mallows_, Jan 26 2002",
				"Number of ways to write n-1 as n-1 = x*y + x + y, 0\u003c=x\u003c=y\u003c=n. - _Benoit Cloitre_, Jun 23 2002",
				"Also number of values for x where x+2n and x-2n are both squares (e.g., if n=9, then 18+18 and 18-18 are both squares, as are 82+18 and 82-18 so a(9)=2); this is because a(n) is the number of solutions to n=k(k+r) in which case if x=r^2+2n then x+2n=(r+2k)^2 and x-2n=r^2 (cf. A061408). - _Henry Bottomley_, May 03 2001",
				"Also number of sums of sequences of consecutive odd numbers or consecutive even numbers including sequences of length 1 (e.g., 12 = 5+7 or 2+4+6 or 12 so a(12)=3). - _Naohiro Nomoto_, Feb 26 2002",
				"Number of partitions whose consecutive parts differ by exactly two.",
				"a(n) depends only on prime signature of n (cf. A025487). So a(24) = a(375) since 24=2^3*3 and 375=3*5^3 both have prime signature (3,1). - _Christian G. Bower_, Jun 06 2005",
				"Also number of partitions of n such that if k is the largest part, then each of the parts 1,2,...,k-1 occurs exactly twice. Example: a(12)=3 because we have [3,3,2,2,1,1],[2,2,2,2,2,1,1] and [1,1,1,1,1,1,1,1,1,1,1,1]. - _Emeric Deutsch_, Mar 07 2006",
				"a(n) is also the number of nonnegative integer solutions of the Diophantine equation 4 x^2-y^2=16 n. For example, a(24)=4 because there are 4 solutions: (x,y)=(10,4),(11,10),(14,20),(25,46). - _N-E. Fahssi_, Feb 27 2008",
				"a(n) is the number of even divisors of 2*n that are \u003c=sqrt(2*n). - _Joerg Arndt_, Mar 04 2010",
				"First differences of A094820. - _John W. Layman_, Feb 21 2012",
				"a(n) = #{k: A027750(n,k) \u003c= A000196(n)}; a(A008578(n)) = 1; a(A002808(n)) \u003e 1. - _Reinhard Zumkeller_, Dec 26 2012",
				"Row lengths of the tables in A161906 and A161908. - _Reinhard Zumkeller_, Mar 08 2013",
				"Number of positive integers in the sequence defined by x_0 = n, x_(k+1) = (k+1)*(x_k-2)/(k+2) or equivalently by x_k = n/(k+1) - k. - _Luc Rousseau_, Mar 03 2018",
				"Expanding the first comment: Number of rectangles with area n and integer side lengths, modulo rotation. Also number of 2D grids of n congruent squares, in a rectangle, modulo rotation (cf. A000005 for rectangles instead of squares; cf. A034836 for the 3D case). - _Manfred Boergens_, Jun 08 2021"
			],
			"reference": [
				"G. E. Andrews, K. Eriksson, Integer Partitions, Cambridge Univ. Press, 2004. page 18, Exer. 21, 22."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A038548/b038548.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Cristina Ballantine and Mircea Merca, \u003ca href=\"https://doi.org/10.1016/j.jnt.2016.06.007\"\u003eNew convolutions for the number of divisors\u003c/a\u003e, Journal of Number Theory, 2016, vol. 170, pp. 17-34.",
				"Christopher Briggs, Y. Hirano, and H. Tsutsui, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL19/Briggs/briggs3.html\"\u003ePositive Solutions to Some Systems of Diophantine Equations\u003c/a\u003e, Journal of Integer Sequences, 2016 Vol 19 #16.8.4.",
				"S.-H. Cha, E. G. DuCasse, and L. V. Quintas, \u003ca href=\"http://arxiv.org/abs/1405.5283\"\u003eGraph invariants based on the divides relation and ordered by prime signatures\u003c/a\u003e, arXiv:1405.5283 [math.NT], 2014, (2.27).",
				"Madeline Locus Dawsey, Matthew Just and Robert Schneider, \u003ca href=\"https://arxiv.org/abs/2107.14284\"\u003eA \"supernormal\" partition statistic\u003c/a\u003e, arXiv:2107.14284 [math.NT], 2021. See Table 2 p. 21.",
				"T. Verhoeff, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/trapzoid.html\"\u003eRectangular and Trapezoidal Arrangements\u003c/a\u003e, J. Integer Sequences, Vol. 2, 1999, #99.1.6."
			],
			"formula": [
				"a(n) = ceiling(d(n)/2), where d(n) = number of divisors of n (A000005).",
				"a(2k) = A034178(2k)+A001227(k). a(2k+1) = A034178(2k+1). - _Naohiro Nomoto_, Feb 26 2002",
				"G.f.: sum(k\u003e=1, x^(k^2)/(1-x^k)). - _Jon Perry_, Sep 10 2004",
				"Dirichlet g.f.: (zeta(s)^2 + zeta(2*s))/2. - _Christian G. Bower_, Jun 06 2005 [corrected by _Vaclav Kotesovec_, Aug 19 2019]",
				"a(n) = (A000005(n) + A010052(n))/2. - _Omar E. Pol_, Jun 23 2009",
				"a(n) = A034178(4*n). - _Michael Somos_, May 11 2011",
				"2*a(n) = A161841(n). - _R. J. Mathar_, Mar 07 2021"
			],
			"example": [
				"a(4) = 2 since 4 = 2 * 2 = 4 * 1. Also A034178(4*4) = 2 since 16 = 4^2 - 0^2 = 5^2 - 3^2. - _Michael Somos_, May 11 2011",
				"x + x^2 + x^3 + 2*x^4 + x^5 + 2*x^6 + x^7 + 2*x^8 + 2*x^9 + 2*x^10 + x^11 + ..."
			],
			"maple": [
				"with(numtheory): A038548 := n-\u003eceil(sigma[0](n)/2);"
			],
			"mathematica": [
				"Table[ Floor[ (DivisorSigma[0, n] + 1)/2], {n, 105}] (* _Robert G. Wilson v_, Mar 02 2009 *)",
				"Table[Count[Divisors[n],_?(#\u003c=Sqrt[n]\u0026)],{n,110}] (* _Harvey P. Dale_, Jul 10 2021 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, 0, sumdiv(n, d, d*d \u003c= n))} /* _Michael Somos_, Jan 25 2005 */",
				"(PARI) a(n)=ceil(numdiv(n)/2) \\\\ _Charles R Greathouse IV_, Sep 28 2012",
				"(Haskell)",
				"a038548 n = length $ takeWhile (\u003c= a000196 n) $ a027750_row n",
				"-- _Reinhard Zumkeller_, Dec 26 2012"
			],
			"xref": [
				"Different from A068108. Records give A038549, A004778, A086921.",
				"Cf. A000005, A072670, A094820, A161841, A108504.",
				"Cf. A066839, A033676, row sums of A303300."
			],
			"keyword": "nonn,easy,nice",
			"offset": "1,4",
			"author": "_Tom Verhoeff_, _N. J. A. Sloane_",
			"references": 179,
			"revision": 112,
			"time": "2021-08-02T05:02:47-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}