{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A075053",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 75053,
			"data": "0,0,1,1,0,1,0,1,0,0,0,1,1,3,1,1,1,3,0,1,1,1,2,3,1,2,1,2,1,2,1,3,3,2,2,3,1,4,2,1,0,1,1,2,0,1,0,2,0,0,1,1,2,3,1,2,1,2,1,2,0,1,1,1,0,1,0,2,0,0,1,3,2,4,2,2,2,2,1,3,0,0,1,2,0,1,0,1,0,1,0,1,2,1,0,2,0,3,1,0,0,2,1,4,2",
			"name": "Number of primes (counted with repetition) that can be formed by rearranging some or all of the digits of n.",
			"comment": [
				"\"Counted with repetition\" means that if the same prime can be obtained using different digits, then it is counted several times (e.g., 13 obtained from 113 using the 1st and 3rd digit or the 2nd and 3rd digit), but not so if it is obtained as different permutations of the same digits (e.g., a(11) = 1 because the identical permutation and the transposition (2,1) of the digits [1,1] both yield 11, but this does not count twice since the same digits are used). - _M. F. Hasler_, Mar 12 2014",
				"See A039993 for the variant which counts only distinct primes, e.g., A039993(22) = 1, A039993(113) = 7. See A039999 for the variant which requires all digits to be used. - _M. F. Hasler_, Oct 14 2019"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A075053/b075053.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"M. F. Hasler, \u003ca href=\"https://oeis.org/wiki/Primeval_numbers\"\u003ePrimeval numbers\u003c/a\u003e, OEIS wiki, 2014, updated 2019."
			],
			"formula": [
				"a(n) \u003e= A039993(n) with equality iff n has no duplicate digit. -  _M. F. Hasler_, Oct 14 2019",
				"a(n) = Sum_{k in S(n)} A039999(k), if n has not more than one digit 0, where S(n) are the numbers whose nonzero digits are a subsequence of those of n, and which contain the digit 0 if n does, taken with multiplicity (for numbers using some but not all digits that are repeated in n). - _M. F. Hasler_, Oct 15 2019"
			],
			"example": [
				"From 13 we can obtain 3, 13 and 31 so a(13) = 3. In the same way, a(17) = 3.",
				"From 22 we obtain 2 in two ways, so a(22) = 2.",
				"a(101) = 2 because from the digits of 101 one can form the primes 11 (using digits 1 \u0026 3) and 101 (using all digits). The prime 11 = 011 formed using all 3 digits does not count separately because it has a leading zero.",
				"a(111) = 3 because one can form the prime 11 using digits 1 \u0026 2, 1 \u0026 3 or 2 \u0026 3."
			],
			"mathematica": [
				"f[n_] := Length@ Select[ Union[ FromDigits@# \u0026 /@ Flatten[ Subsets@# \u0026 /@ Permutations@ IntegerDigits@ n, 1]], PrimeQ@# \u0026]; Array[f, 105, 0] (* _Robert G. Wilson v_, Mar 12 2014 *)"
			],
			"program": [
				"(PARI) A075053(n,D=vecsort(digits(n)),S=0)=for(b=1,2^#D-1,forperm(vecextract(D,b),p,p[1]\u0026\u0026isprime(fromdigits(Vec(p)))\u0026\u0026S++));S \\\\ Second optional arg allows to give the digits if they are already known. - _M. F. Hasler_, Oct 14 2019, replacing earlier code from 2014."
			],
			"xref": [
				"Different from A039993 (counts only distinct primes). Cf. A072857, A076449.",
				"Cf. A039999 (use all digits, count only distinct primes, allow leading zeros), A046810 (similar but don't allow leading zeros).",
				"Cf. A134597 (maximum over all n-digit numbers), A076730 (maximum of A039993 for all n-digit numbers).",
				"Cf. A239196 and A239197 for record indices and values."
			],
			"keyword": "nonn,base,easy",
			"offset": "0,14",
			"author": "_N. J. A. Sloane_, Oct 12 2002",
			"ext": [
				"Corrected and extended by _John W. Layman_, Oct 15 2002",
				"Examples \u0026 crossrefs edited by _M. F. Hasler_, Oct 14 2019"
			],
			"references": 13,
			"revision": 42,
			"time": "2019-10-22T21:31:17-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}