{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A178746",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 178746,
			"data": "0,1,3,6,6,7,13,12,12,13,15,26,26,27,25,24,24,25,27,30,30,31,53,52,52,53,55,50,50,51,49,48,48,49,51,54,54,55,61,60,60,61,63,106,106,107,105,104",
			"name": "Binary counter with intermittent bits. Starting at zero the counter attempts to increment by 1 at each step but each bit in the counter alternately accepts and rejects requests to toggle.",
			"comment": [
				"A simple scatter plot reveals a self-similar structure that resembles flying geese.",
				"Ignoring the initial zero term, split the sequence into rows of increasing binary magnitude such that the terms in row m satisfy 2^m \u003c= a(n) \u003c 2^(m+1).",
				"0: 1,",
				"1: 3,",
				"2: 6,6,7,",
				"3: 13,12,12,13,15,",
				"4: 26,26,27,25,24,24,25,27,30,30,31,",
				"5: 53,52,52,53,55,50,50,51,49,48,48,49,51,54,54,55,61,60,60,61,63,",
				"Then,",
				"Row m starts at n = A005578(m+1) in the original sequence",
				"The first term in row m is A081254(m)",
				"The last term in row m is 2^(m+1)-1",
				"The number of terms in row m is A001045(m+1)",
				"The number of distinct terms in row m is A005578(m)",
				"The number of ascending runs in row m is A005578(m)",
				"The number of non-ascending runs in row m is A005578(m)",
				"The number of descending runs in row m is A052950(m)",
				"The number of non-descending runs in row m is A005578(m-1)",
				"The sum of terms in row m is A178747(m)",
				"The total number of '1' bits in the terms of row n is A178748(m)"
			],
			"link": [
				"D. Scambler, \u003ca href=\"/A178746/b178746.txt\"\u003eTable of n, a(n) for n = 0..1024\u003c/a\u003e"
			],
			"formula": [
				"If n is a power of 2, a(n) = n*3/2. Lim(a(n)/n) = 3/2."
			],
			"example": [
				"0 -\u003e low bit toggles -\u003e 1 -\u003e should be 2 but low bit does not toggle -\u003e 3 -\u003e should be 4 but 2nd-lowest bit does not toggle -\u003e 6 -\u003e should be 7 but low bit does not toggle -\u003e 6 -\u003e low bit toggles -\u003e 7"
			],
			"program": [
				"(PARI) seq(n)={my(a=vector(n+1), f=0, p=0); for(i=2, #a, my(b=bitxor(p+1,p)); f=bitxor(f,b); p=bitxor(p, bitand(b,f)); a[i]=p); a} \\\\ _Andrew Howroyd_, Mar 03 2020"
			],
			"xref": [
				"Cf. A178747 sum of terms in rows of a(n), A178748 total number of '1' bits in the terms of rows of a(n)."
			],
			"keyword": "nonn,look",
			"offset": "0,3",
			"author": "_David Scambler_, Jun 08 2010",
			"references": 3,
			"revision": 11,
			"time": "2020-03-04T03:25:16-05:00",
			"created": "2010-07-11T03:00:00-04:00"
		}
	]
}