{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A069128",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 69128,
			"data": "1,16,46,91,151,226,316,421,541,676,826,991,1171,1366,1576,1801,2041,2296,2566,2851,3151,3466,3796,4141,4501,4876,5266,5671,6091,6526,6976,7441,7921,8416,8926,9451,9991,10546,11116,11701,12301,12916,13546,14191,14851,15526",
			"name": "Centered 15-gonal numbers: a(n) = (15*n^2 - 15*n + 2)/2.",
			"comment": [
				"Centered pentadecagonal numbers or centered quindecagonal numbers or centered pentakaidecagonal numbers. - _Omar E. Pol_, Oct 03 2011"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A069128/b069128.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"E. Weisstein, \u003ca href=\"http://mathworld.wolfram.com/CenteredPolygonalNumber.html\"\u003eCentered Polygonal Numbers\u003c/a\u003e",
				"\u003ca href=\"/index/Ce#CENTRALCUBE\"\u003eIndex entries for sequences related to centered polygonal numbers\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-3,1)"
			],
			"formula": [
				"a(n) = (15*n^2 - 15*n + 2)/2.",
				"a(n) = 15*n+a(n-1)-15 (with a(1)=1). - _Vincenzo Librandi_, Aug 08 2010",
				"G.f.: -x*(1+13*x+x^2) / (x-1)^3. - _R. J. Mathar_, Feb 04 2011",
				"Binomial transform of [1, 15, 15, 0, 0, 0, ...] and Narayana transform (A001263) of [1, 15, 0, 0, 0, ...]. - _Gary W. Adamson_, Jul 28 2011",
				"a(n) = A194715(n-1) + 1. - _Omar E. Pol_, Oct 03 2011",
				"From _Amiram Eldar_, Jun 21 2020: (Start)",
				"Sum_{n\u003e=1} 1/a(n) = 2*Pi*tan(sqrt(7/15)*Pi/2)/sqrt(105).",
				"Sum_{n\u003e=1} a(n)/n! = 17*e/2 - 1.",
				"Sum_{n\u003e=1} (-1)^n * a(n)/n! = 17/(2*e) - 1. (End)"
			],
			"example": [
				"a(5) = 151 because (15*5^2 - 15*5 + 2)/2 = 151."
			],
			"maple": [
				"A069128:=n-\u003e(15*n^2 - 15*n + 2)/2: seq(A069128(n), n=1..50); # _Wesley Ivan Hurt_, Nov 14 2014"
			],
			"mathematica": [
				"FoldList[#1 + #2 \u0026, 1, 15 Range@ 45] (* _Robert G. Wilson v_, Feb 02 2011 *)",
				"LinearRecurrence[{3,-3,1},{1,16,46},50] (* _Harvey P. Dale_, Oct 22 2013 *)"
			],
			"program": [
				"(MAGMA) [(15*n^2 - 15*n + 2)/2 : n in [1..50]]; // _Wesley Ivan Hurt_, Nov 14 2014",
				"(PARI) a(n)=15*n*(n-1)/2+1 \\\\ _Charles R Greathouse IV_, Nov 15 2014"
			],
			"xref": [
				"Cf. A005448, A001844, A005891, A003215, A069099."
			],
			"keyword": "nonn,easy,nice",
			"offset": "1,2",
			"author": "_Terrel Trotter, Jr._, Apr 07 2002",
			"references": 6,
			"revision": 51,
			"time": "2020-06-21T05:36:13-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}