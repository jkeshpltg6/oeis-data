{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A321449",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 321449,
			"data": "1,0,1,0,1,2,0,1,2,3,0,1,4,5,5,0,1,4,8,8,7,0,1,6,13,19,16,11,0,1,6,17,27,32,24,15,0,1,8,24,47,61,62,41,22,0,1,8,30,63,99,111,100,61,30,0,1,10,38,94,158,209,210,170,95,42,0,1,10,45,119,229,328,382,348,259,136,56",
			"name": "Regular triangle read by rows where T(n,k) is the number of twice-partitions of n with a combined total of k parts.",
			"comment": [
				"A twice partition of n (A063834) is a choice of an integer partition of each part in an integer partition of n."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A321449/b321449.txt\"\u003eRows n = 0..200, flattened\u003c/a\u003e"
			],
			"formula": [
				"O.g.f.: Product_{n \u003e= 0} 1/(1 - x^n * (Sum_{0 \u003c= k \u003c= n} A008284(n,k) * t^k))."
			],
			"example": [
				"Triangle begins:",
				"   1",
				"   0   1",
				"   0   1   2",
				"   0   1   2   3",
				"   0   1   4   5   5",
				"   0   1   4   8   8   7",
				"   0   1   6  13  19  16  11",
				"   0   1   6  17  27  32  24  15",
				"   0   1   8  24  47  61  62  41  22",
				"   0   1   8  30  63  99 111 100  61  30",
				"The sixth row {0, 1, 6, 13, 19, 16, 11} counts the following twice-partitions:",
				"  (6)  (33)    (222)      (2211)        (21111)          (111111)",
				"       (42)    (321)      (3111)        (1111)(2)        (111)(111)",
				"       (51)    (411)      (111)(3)      (111)(21)        (1111)(11)",
				"       (3)(3)  (21)(3)    (211)(2)      (21)(111)        (11111)(1)",
				"       (4)(2)  (22)(2)    (21)(21)      (211)(11)        (11)(11)(11)",
				"       (5)(1)  (31)(2)    (22)(11)      (2111)(1)        (111)(11)(1)",
				"               (3)(21)    (221)(1)      (11)(11)(2)      (1111)(1)(1)",
				"               (32)(1)    (3)(111)      (111)(2)(1)      (11)(11)(1)(1)",
				"               (4)(11)    (31)(11)      (11)(2)(11)      (111)(1)(1)(1)",
				"               (41)(1)    (311)(1)      (2)(11)(11)      (11)(1)(1)(1)(1)",
				"               (2)(2)(2)  (11)(2)(2)    (21)(11)(1)      (1)(1)(1)(1)(1)(1)",
				"               (3)(2)(1)  (2)(11)(2)    (211)(1)(1)",
				"               (4)(1)(1)  (21)(2)(1)    (11)(2)(1)(1)",
				"                          (2)(2)(11)    (2)(11)(1)(1)",
				"                          (22)(1)(1)    (21)(1)(1)(1)",
				"                          (3)(11)(1)    (2)(1)(1)(1)(1)",
				"                          (31)(1)(1)",
				"                          (2)(2)(1)(1)",
				"                          (3)(1)(1)(1)"
			],
			"maple": [
				"g:= proc(n, i) option remember; `if`(n=0 or i=1, x^n,",
				"      g(n, i-1)+ `if`(i\u003en, 0, expand(g(n-i, i)*x)))",
				"    end:",
				"b:= proc(n, i) option remember; `if`(n=0 or i=1, x^n,",
				"      b(n, i-1)+ `if`(i\u003en, 0, expand(b(n-i, i)*g(i$2))))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=0..degree(p)))(b(n$2)):",
				"seq(T(n), n=0..12);  # _Alois P. Heinz_, Nov 11 2018"
			],
			"mathematica": [
				"Table[Length[Join@@Table[Select[Tuples[IntegerPartitions/@ptn],Length[Join@@#]==k\u0026],{ptn,IntegerPartitions[n]}]],{n,0,10},{k,0,n}]",
				"(* Second program: *)",
				"g[n_, i_] := g[n, i] = If[n == 0 || i == 1, x^n,",
				"     g[n, i - 1] + If[i \u003e n, 0, Expand[g[n - i, i]*x]]];",
				"b[n_, i_] := b[n, i] = If[n == 0 || i == 1, x^n,",
				"     b[n, i - 1] + If[i \u003e n, 0, Expand[b[n - i, i]*g[i, i]]]];",
				"T[n_] := CoefficientList[b[n, n], x];",
				"T /@ Range[0, 12] // Flatten (* _Jean-François Alcover_, May 20 2021, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Row sums are A063834. Last column is A000041.",
				"Cf. A000219, A001970, A007716, A008284, A055884, A289501, A317449, A317532, A317533, A320801, A320808."
			],
			"keyword": "nonn,tabl",
			"offset": "0,6",
			"author": "_Gus Wiseman_, Nov 10 2018",
			"references": 2,
			"revision": 18,
			"time": "2021-05-21T04:16:24-04:00",
			"created": "2018-11-11T18:22:18-05:00"
		}
	]
}