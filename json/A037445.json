{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A037445",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 37445,
			"data": "1,2,2,2,2,4,2,4,2,4,2,4,2,4,4,2,2,4,2,4,4,4,2,8,2,4,4,4,2,8,2,4,4,4,4,4,2,4,4,8,2,8,2,4,4,4,2,4,2,4,4,4,2,8,4,8,4,4,2,8,2,4,4,4,4,8,2,4,4,8,2,8,2,4,4,4,4,8,2,4,2,4,2,8,4,4,4,8,2,8,4,4,4,4,4,8,2,4,4,4,2,8,2,8,8",
			"name": "Number of infinitary divisors (or i-divisors) of n.",
			"comment": [
				"A divisor of n is called infinitary if it is a product of divisors of the form p^{y_a 2^a}, where p^y is a prime power dividing n and sum_a y_a 2^a is the binary representation of y."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A037445/b037445.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Steven R. Finch, \u003ca href=\"/A007947/a007947.pdf\"\u003eUnitarism and Infinitarism\u003c/a\u003e, February 25, 2004. [Cached copy, with permission of the author]",
				"J. O. M. Pedersen, \u003ca href=\"http://amicable.homepage.dk/tables.htm\"\u003eTables of Aliquot Cycles\u003c/a\u003e [Broken link]",
				"J. O. M. Pedersen, \u003ca href=\"http://web.archive.org/web/20140502102524/http://amicable.homepage.dk/tables.htm\"\u003eTables of Aliquot Cycles\u003c/a\u003e [Via Internet Archive Wayback-Machine]",
				"J. O. M. Pedersen, \u003ca href=\"/A063990/a063990.pdf\"\u003eTables of Aliquot Cycles\u003c/a\u003e [Cached copy, pdf file only]",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/InfinitaryDivisor.html\"\u003eInfinitary Divisor\u003c/a\u003e",
				"\u003ca href=\"/index/Eu#epf\"\u003eIndex entries for sequences computed from exponents in factorization of n\u003c/a\u003e"
			],
			"formula": [
				"Multiplicative with a(p^e) = 2^A000120(e). - _David W. Wilson_, Sep 01 2001",
				"Let n = q_1*...*q_k, where q_1,...,q_k are different terms of A050376. Then a(n) = 2^k (the number of subsets of a set with k elements is 2^k). - _Vladimir Shevelev_, Feb 19 2011.",
				"a(n) = product(A000079(A000120(A124010(n,k))): k=1..A001221(n)). - _Reinhard Zumkeller_, Mar 19 2013",
				"From _Antti Karttunen_, May 28 2017: (Start)",
				"a(n) = A286575(A156552(n)). [Because multiplicative with a(p^e) = A001316(e).]",
				"a(n) = 2^A064547(n). (End)"
			],
			"example": [
				"For n = 8, n = 2^3 = 2^\"11\" (writing 3 in binary) so the infinitary divisors are 2^\"00\" = 1, 2^\"01\" = 2, 2^\"10\" = 4 and 2^\"11\" = 8, so a(8) = 4.",
				"For n = 90, n = 2*5*9 where 2,5,9 are in A050376, so a(90) = 2^3 = 8."
			],
			"maple": [
				"A037445 := proc(n)",
				"    local a,p;",
				"    a := 1 ;",
				"    for p in ifactors(n)[2] do",
				"        a := a*2^wt(p[2]) ;",
				"    end do:",
				"    a ;",
				"end proc: # _R. J. Mathar_, May 16 2016"
			],
			"mathematica": [
				"Table[Length@((Times @@ (First[it]^(#1 /. z -\u003e List)) \u0026 ) /@",
				"Flatten[Outer[z, Sequence @@ bitty /@",
				"Last[it = Transpose[FactorInteger[k]]], 1]]), {k, 2, 240}]",
				"bitty[k_] := Union[Flatten[Outer[Plus, Sequence @@ ({0, #1} \u0026 ) /@ Union[2^Range[0, Floor[Log[2, k]]]*Reverse[IntegerDigits[k, 2]]]]]]",
				"y[n_] := Select[Range[0, n], BitOr[n, # ] == n \u0026 ] divisors[Infinity][1] := {1} divisors[Infinity][n_] := Sort[Flatten[Outer[Times, Sequence @@ (FactorInteger[n] /. {p_, m_Integer} :\u003e p^y[m])]]] Length /@ divisors[Infinity] /@ Range[105] - Paul Abbott (paul(AT)physics.uwa.edu.au), Apr 29 2005",
				"a[1] = 1; a[n_] := Times @@ Flatten[ 2^DigitCount[#, 2, 1]\u0026  /@ FactorInteger[n][[All, 2]] ]; Table[a[n], {n, 1, 105}] (* _Jean-François Alcover_, Aug 19 2013, after _Reinhard Zumkeller_ *)"
			],
			"program": [
				"(PARI) A037445(n) = factorback(apply(a -\u003e 2^hammingweight(a), factorint(n)[,2])) \\\\ _Andrew Lelechenko_, May 10 2014",
				"(Haskell)",
				"a037445 = product . map (a000079 . a000120) . a124010_row",
				"-- _Reinhard Zumkeller_, Mar 19 2013",
				"(Scheme) (define (A037445 n) (if (= 1 n) n (* (A001316 (A067029 n)) (A037445 (A028234 n))))) ;; _Antti Karttunen_, May 28 2017",
				"(Python)",
				"from sympy import factorint",
				"def wt(n): return bin(n).count(\"1\")",
				"def a(n):",
				"    f=factorint(n)",
				"    return 2**sum([wt(f[i]) for i in f]) # _Indranil Ghosh_, May 30 2017"
			],
			"xref": [
				"Cf. A000120, A001316, A004607, A007358, A007357, A038148, A049417, A064547, A074848, A124010, A156552, A286575."
			],
			"keyword": "nonn,nice,easy,mult",
			"offset": "1,2",
			"author": "_Yasutoshi Kohmoto_",
			"ext": [
				"Corrected and extended by _Naohiro Nomoto_, Jun 21 2001"
			],
			"references": 71,
			"revision": 69,
			"time": "2018-07-30T02:26:31-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}