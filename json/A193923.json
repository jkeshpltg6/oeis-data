{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A193923",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 193923,
			"data": "1,1,1,1,2,3,1,3,5,8,1,4,8,13,21,1,5,12,21,34,55,1,6,17,33,55,89,144,1,7,23,50,88,144,233,377,1,8,30,73,138,232,377,610,987,1,9,38,103,211,370,609,987,1597,2584,1,10,47,141,314,581,979,1596,2584,4181,6765",
			"name": "Triangular array:  the fusion of (p(n,x)) by (q(n,x)), where p(n,x)=(x+1)^n and q(n,x)=Sum_{k=0..n}F(k+1)*x^(n-k), where F=A000045 (Fibonacci numbers).",
			"comment": [
				"See A193722 for the definition of fusion of two sequences of polynomials or triangular arrays.",
				"The row sums equal A079289(2*n). - _Johannes W. Meijer_, Aug 12 2013"
			],
			"link": [
				"Michel Marcus, \u003ca href=\"/A193923/b193923.txt\"\u003eRows n=0..100 of triangle, flattened\u003c/a\u003e",
				"T. G. Lavers, \u003ca href=\"https://ajc.maths.uq.edu.au/pdf/10/ocr-ajc-v10-p147.pdf\"\u003eFibonacci numbers, ordered partitions, and transformations of a finite set\u003c/a\u003e, Australasian Journal of Combinatorics, Volume 10(1994), pp. 147-151. See triangle p. 151 (with rows reversed and initial term 0)."
			],
			"formula": [
				"T(n, k) = Sum_{p=0..k} binomial(n+k-p-1, p). - _Johannes W. Meijer_, Aug 12 2013",
				"T(n, n) = Fibonacci(2*n) for n\u003e=1. - _Michel Marcus_, Nov 03 2020"
			],
			"example": [
				"First six rows:",
				"1",
				"1...1",
				"1...2...3",
				"1...3...5....8",
				"1...4...8....13...21",
				"1...5...12...21...34...55"
			],
			"maple": [
				"T := proc(n, k) option remember: if k = 0 then return(1) fi: if k = n then return(combinat[fibonacci](2*n)) fi: T(n, k) := T(n-1, k-1) + T(n-1, k) end: seq(seq(T(n, k), k=0..n), n=0..9); # _Johannes W. Meijer_, Aug 12 2013"
			],
			"mathematica": [
				"p[n_, x_] := (x + 1)^n;",
				"q[n_, x_] := Sum[Fibonacci[k + 1]*x^(n - k), {k, 0, n}];",
				"t[n_, k_] := Coefficient[p[n, x], x^k]; t[n_, 0] := p[n, x] /. x -\u003e 0;",
				"w[n_, x_] := Sum[t[n, k]*q[n + 1 - k, x], {k, 0, n}]; w[-1, x_] := 1",
				"g[n_] := CoefficientList[w[n, x], {x}]",
				"TableForm[Table[Reverse[g[n]], {n, -1, z}]]",
				"Flatten[Table[Reverse[g[n]], {n, -1, z}]]  (* A193923 *)",
				"TableForm[Table[g[n], {n, -1, z}]]",
				"Flatten[Table[g[n], {n, -1, z}]]  (* A193924 *)"
			],
			"xref": [
				"Cf. A193722, A193924.",
				"Cf. A001906 (Fibonacci(2*n))."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Clark Kimberling_, Aug 09 2011",
			"references": 2,
			"revision": 20,
			"time": "2020-11-03T11:46:37-05:00",
			"created": "2011-08-09T21:49:30-04:00"
		}
	]
}