{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003973",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3973,
			"data": "1,4,6,13,8,24,12,40,31,32,14,78,18,48,48,121,20,124,24,104,72,56,30,240,57,72,156,156,32,192,38,364,84,80,96,403,42,96,108,320,44,288,48,182,248,120,54,726,133,228,120,234,60,624,112,480,144,128,62,624,68",
			"name": "Inverse Möbius transform of A003961; a(n) = sigma(A003961(n)), where A003961 shifts the prime factorization of n one step towards the larger primes.",
			"comment": [
				"Sum of the divisors of the prime shifted n, or equally, sum of the prime shifted divisors of n. - _Antti Karttunen_, Aug 17 2020"
			],
			"link": [
				"Charles R Greathouse IV, \u003ca href=\"/A003973/b003973.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"\u003ca href=\"/index/Pri#prime_indices\"\u003eIndex entries for sequences computed from indices in prime factorization\u003c/a\u003e",
				"\u003ca href=\"/index/Si#SIGMAN\"\u003eIndex entries for sequences related to sigma(n)\u003c/a\u003e"
			],
			"formula": [
				"Multiplicative with a(p^e) = (q^(e+1)-1)/(q-1) where q = nextPrime(p). - _David W. Wilson_, Sep 01 2001",
				"From _Antti Karttunen_, Aug 06-12 2020: (Start)",
				"a(n) = Sum_{d|n} A003961(d) = Sum_{d|A003961(n)} d.",
				"a(n) = A000203(A003961(n)) = A000593(A003961(n)).",
				"a(n) = 2*A336840(n) - A000005(n) = 2*Sum_{d|n} (A048673(d) - (1/2)).",
				"a(n) = A008438(A108228(n)) = A008438(A048673(n)-1).",
				"a(n) = A336838(n) * A336856(n).",
				"a(n) is odd if and only if n is a square.",
				"(End)"
			],
			"mathematica": [
				"b[1] = 1; b[p_?PrimeQ] := b[p] = Prime[ PrimePi[p] + 1]; b[n_] := b[n] = Times @@ (b[First[#]]^Last[#] \u0026) /@ FactorInteger[n]; a[n_] := Sum[ b[d], {d, Divisors[n]}]; Table[a[n], {n, 1, 70}]  (* _Jean-François Alcover_, Jul 18 2013 *)"
			],
			"program": [
				"(PARI) aPrime(p,e)=my(q=nextprime(p+1));(q^(e+1)-1)/(q-1)",
				"a(n)=my(f=factor(n));prod(i=1,#f~,aPrime(f[i,1],f[i,2])) \\\\ _Charles R Greathouse IV_, Jul 18 2013",
				"(PARI) A003973(n) = { my(f = factor(n)); for(i=1, #f~, f[i, 1] = nextprime(f[i, 1]+1)); sigma(factorback(f)); }; \\\\ _Antti Karttunen_, Aug 06 2020"
			],
			"xref": [
				"Cf. A000203, A000290 (positions of odd terms), A003961, A007814, A048673, A108228, A295664, A336840.",
				"Permutation of A008438.",
				"Used in the definitions of the following sequences: A326042, A336838, A336841, A336844, A336846, A336847, A336848, A336849, A336850, A336851, A336852, A336856, A336931, A336932.",
				"Cf. also A003972."
			],
			"keyword": "nonn,easy,mult",
			"offset": "1,2",
			"author": "_Marc LeBrun_",
			"ext": [
				"More terms from _David W. Wilson_, Aug 29 2001",
				"Secondary name added by _Antti Karttunen_, Aug 06 2020"
			],
			"references": 65,
			"revision": 52,
			"time": "2020-08-20T21:20:34-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}