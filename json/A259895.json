{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A259895",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 259895,
			"data": "1,0,1,1,0,1,1,0,0,2,0,1,1,0,0,2,0,0,1,0,2,1,0,1,1,0,0,0,0,1,3,0,1,1,0,0,1,0,1,1,0,0,2,0,0,2,0,1,1,0,1,2,0,0,0,0,1,1,0,1,2,0,0,1,0,3,0,0,0,1,0,0,2,0,1,3,0,0,0,0,0,1,0,1,1,0,2",
			"name": "Expansion of psi(x^2) * psi(x^3) in powers of x where psi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A259895/b259895.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(-5/8) * eta(q^4)^2 * eta(q^6)^2 / (eta(q^2) * eta(q^3)) in powers of q.",
				"Euler transform of period 12 sequence [ 0, 1, 1, -1, 0, 0, 0, -1, 1, 1, 0, -2, ...].",
				"a(n) = A259896(3*n + 1). a(3*n) = A128583(n). a(3*n + 1) = a(9*n + 8) = 0.",
				"2 * a(n) = A129402(4*n + 2) = A190615(4*n + 2) = A000377(8*n + 5) = A192013(8*n + 5). - _Michael Somos_, Jul 22 2015",
				"-2 * a(n) = A259668(2*n + 1) = A128580(4*n + 2) = A134177(4*n + 2) = A257921(6*n + 3). - _Michael Somos_, Jul 22 2015",
				"a(3*n + 2) = A259896(n). - _Michael Somos_, Jul 22 2015"
			],
			"example": [
				"G.f. = 1 + x^2 + x^3 + x^5 + x^6 + 2*x^9 + x^11 + x^12 + 2*x^15 + x^18 + ...",
				"G.f. = q^5 + q^21 + q^29 + q^45 + q^53 + 2*q^77 + q^93 + q^101 + 2*q^125 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 2, 0, x] EllipticTheta[ 2, 0, x^(3/2)] / (4 q^(5/8)), {x, 0, n}];",
				"a[ n_] := If[ n \u003c 0, 0, 1/2 Sum[ KroneckerSymbol[ -6, d], {d, Divisors[8 n + 5]}]]; (* _Michael Somos_, Jul 22 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^4 + A)^2 * eta(x^6 + A)^2 / (eta(x^2 + A) * eta(x^3 + A)), n))};",
				"(PARI) {a(n) = if( n\u003c0, 0, 1/2 * sumdiv( 8*n + 5, d, kronecker( -6, d)))};"
			],
			"xref": [
				"Cf. A000377, A128580, A128583, A129402, A134177, A190615, A192013, A257921, A259668, A259896."
			],
			"keyword": "nonn",
			"offset": "0,10",
			"author": "_Michael Somos_, Jul 07 2015",
			"references": 6,
			"revision": 13,
			"time": "2021-03-12T22:24:48-05:00",
			"created": "2015-07-07T11:52:37-04:00"
		}
	]
}