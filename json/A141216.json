{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A141216",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 141216,
			"data": "30,320,224,240,72,360,728,0,672,216,1320,0,0,16,5060,60,126,10560,216,0,3360,2574,150,5040,2808,3600,3600,232,400,420,22,2700,2784,224,96,70,1640,240,9200,3600,2760,58344,616,504,102,5600,8064,264,11880,1440,7488,252",
			"name": "a(n) = A137576((N-1)/2) - N, where N = A001567(n).",
			"comment": [
				"The zero terms are of a special interest. Indeed, since for any odd prime p, A137576((p-1)/2)=p, then it is natural to call \"overpseudoprimes\" those Poulet pseudoprimes A001567(n) for which a(n)=0.",
				"Theorem. A squarefree composite number m = p_1*p_2*...*p_k is an overpseudoprime if and only if A002326((p_1-1)/2) = A002326((p_2-1)/2) = ... = A002326((p_k-1)/2). Moreover, every overpseudoprime is in A001262.",
				"Note that in A001262 there exist terms which are not squarefree. The first is A001262(52) = 1194649 = 1093^2.",
				"It can be shown that if an overpseudoprime is not a multiple of the square of a Wieferich prime (see A001220) then it is squarefree. Also all squares of Wieferich primes are overpseudoprimes."
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A141216/b141216.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"V. Shevelev, \u003ca href=\"http://arxiv.org/abs/0806.3412\"\u003eOverpseudoprimes, Mersenne Numbers and Wieferich Primes\u003c/a\u003e, arxiv:0806.3412 [math.NT], 2008-2012."
			],
			"mathematica": [
				"fppQ[n_]:=PowerMod[2,n,n]==2;f[n_] := (t = MultiplicativeOrder[2, 2n+1])*DivisorSum[2n+1, EulerPhi[#] / MultiplicativeOrder[2, #]\u0026]-t+1; s={}; Do[If[fppQ[n] \u0026\u0026 CompositeQ[n],AppendTo[s,f[(n-1)/2 ]-n]],{n,1,10000}]; s (* _Amiram Eldar_, Dec 09 2018 after _Jean-François Alcover_ at A137576 *)"
			],
			"program": [
				"(PARI) f(n) = my(t); sumdiv(2*n+1, d, eulerphi(d)/(t=znorder(Mod(2, d))))*t-t+1; \\\\ A137576",
				"isfpp(n) = {Mod(2, n)^n==2 \u0026 !isprime(n) \u0026 n\u003e1}; \\\\ A001567",
				"lista(nn) = {for (n=1, nn, if (isfpp(n), print1(f((n-1)/2) - n, \", \");););} \\\\ _Michel Marcus_, Dec 09 2018"
			],
			"xref": [
				"Cf. A137576, A001567, A001262, A002326, A006694."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Vladimir Shevelev_, Jun 14 2008, Jul 13 2008",
			"ext": [
				"More terms via b137576.txt from _R. J. Mathar_, Jun 12 2010",
				"More terms from _Michel Marcus_, Dec 09 2018"
			],
			"references": 3,
			"revision": 23,
			"time": "2019-06-28T14:40:33-04:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}