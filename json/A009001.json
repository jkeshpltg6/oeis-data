{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A009001",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 9001,
			"data": "1,1,-1,-3,1,5,-1,-7,1,9,-1,-11,1,13,-1,-15,1,17,-1,-19,1,21,-1,-23,1,25,-1,-27,1,29,-1,-31,1,33,-1,-35,1,37,-1,-39,1,41,-1,-43,1,45,-1,-47,1,49,-1,-51,1,53,-1,-55,1,57,-1,-59,1,61,-1,-63,1,65,-1,-67,1,69,-1,-71,1,73,-1,-75,1,77,-1,-79,1,81,-1,-83,1,85",
			"name": "Expansion of e.g.f: (1+x)*cos(x).",
			"comment": [
				"If signs are ignored, continued fraction for tan(1) (cf. A093178)."
			],
			"link": [
				"Harry J. Smith, \u003ca href=\"/A009001/b009001.txt\"\u003eTable of n, a(n) for n = 0..20000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,-2,0,-1)."
			],
			"formula": [
				"a(n) = (-1)^(n/2) if n even, n*(-1)^((n-1)/2) if n odd.",
				"a(n) = -a(n-2) if n even, 2*a(n-1) - a(n-2) if n odd. - _Michael Somos_, Jan 26 2014",
				"a(n) =(n^n mod (n+1))*(-1)^floor(n/2) for n \u003e 0 = (-1)^n*(a(n-2) - a(n-1)) - a(n-3) for n \u003e 2. - _Henry Bottomley_, Oct 19 2001",
				"G.f.: (1+x+x^2-x^3)/(1+x^2)^2. E.g.f: (1+x)*cos(x).",
				"E.g.f.: (1+x)*cos(x) = U(0) where U(k) = 1 + x - x^2/((2*k+1)*(2*k+2)) * U(k+1). - _Sergei N. Gladkovskii_, Oct 17 2012 [Edited by _Michael Somos_, Jan 26 2014]"
			],
			"example": [
				"tan(1) = 1.557407724654902230... = 1 + 1/(1 + 1/(1 + 1/(3 + 1/(1 + ...)))). - _Harry J. Smith_, Jun 15 2009",
				"G.f. = 1 + x - x^2 - 3*x^3 + x^4 + 5*x^5 - x^6 - 7*x^7 + x^8 + 9*x^9 - x^10 + ..."
			],
			"maple": [
				"seq(coeff(series(factorial(n)*(1+x)*cos(x), x,n+1),x,n),n=0..90); # _Muniru A Asiru_, Jul 21 2018"
			],
			"mathematica": [
				"With[{nn=90},CoefficientList[Series[(1+x)Cos[x],{x,0,nn}],x]Range[0,nn]!] (* _Harvey P. Dale_, Jul 15 2012 *)",
				"LinearRecurrence[{0, -2, 0, -1}, {1, 1, -1, -3}, 100] (* _Jean-François Alcover_, Feb 21 2020 *)"
			],
			"program": [
				"(PARI) {a(n) = (-1)^(n\\2) * if( n%2, n, 1)} /* _Michael Somos_, Oct 16 2006 */",
				"(PARI) { allocatemem(932245000); default(realprecision, 79000); x=contfrac(tan(1)); for (n=0, 20000, write(\"b009001.txt\", n, \" \", (-1)^(n\\2)*x[n+1])); } \\\\ _Harry J. Smith_, Jun 15 2009",
				"(MAGMA) m:=50; R\u003cx\u003e:=PowerSeriesRing(Rationals(), m); b:=Coefficients(R!((1+x)*Cos(x))); [Factorial(n-1)*b[n]: n in [1..m]]; // _G. C. Greubel_, Jul 21 2018"
			],
			"xref": [
				"Cf. A009531, A049471 (decimal expansion of tan(1))."
			],
			"keyword": "sign,easy,nice",
			"offset": "0,4",
			"author": "_R. H. Hardin_, _N. J. A. Sloane_, _Simon Plouffe_, _David W. Wilson_",
			"ext": [
				"Formula corrected by _Olivier Gérard_, Mar 15 1997",
				"Definition clarified by _Harvey P. Dale_, Jul 15 2012"
			],
			"references": 6,
			"revision": 50,
			"time": "2020-02-21T09:21:18-05:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}