{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A332742",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 332742,
			"data": "0,0,0,0,0,1,0,2,3,2,0,8,0,3,7,16,0,24,0,16,12,4,0,52,16,5,81,26,0,54,0,104,18,6,31,168,0,7,25,112,0,99,0,38,201,8,0,344,65,132,33,52,0,612,52,202,42,9,0,408,0,10,411,688,80,162,0,68,52,272",
			"name": "Number of non-unimodal negated permutations of a multiset whose multiplicities are the prime indices of n.",
			"comment": [
				"This multiset is generally not the same as the multiset of prime indices of n. For example, the prime indices of 12 are {1,1,2}, while a multiset whose multiplicities are {1,1,2} is {1,1,2,3}.",
				"A sequence of integers is unimodal if it is the concatenation of a weakly increasing and a weakly decreasing sequence."
			],
			"link": [
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/UnimodalSequence.html\"\u003eUnimodal Sequence\u003c/a\u003e"
			],
			"formula": [
				"a(n) + A332741(n) = A318762(n)."
			],
			"example": [
				"The a(n) permutations for n = 6, 8, 9, 10, 12, 14, 15, 16:",
				"  121  132  1212  1121  1132  11121  11212  1243",
				"       231  1221  1211  1213  11211  11221  1324",
				"            2121        1231  12111  12112  1342",
				"                        1312         12121  1423",
				"                        1321         12211  1432",
				"                        2131         21121  2143",
				"                        2311         21211  2314",
				"                        3121                2341",
				"                                            2413",
				"                                            2431",
				"                                            3142",
				"                                            3241",
				"                                            3412",
				"                                            3421",
				"                                            4132",
				"                                            4231"
			],
			"mathematica": [
				"nrmptn[n_]:=Join@@MapIndexed[Table[#2[[1]],{#1}]\u0026,If[n==1,{},Flatten[Cases[FactorInteger[n]//Reverse,{p_,k_}:\u003eTable[PrimePi[p],{k}]]]]];",
				"unimodQ[q_]:=Or[Length[q]\u003c=1,If[q[[1]]\u003c=q[[2]],unimodQ[Rest[q]],OrderedQ[Reverse[q]]]];",
				"Table[Length[Select[Permutations[nrmptn[n]],!unimodQ[#]\u0026]],{n,30}]"
			],
			"xref": [
				"Dominated by A318762.",
				"The complement of the non-negated version is counted by A332294.",
				"The non-negated version is A332672.",
				"The complement is counted by A332741.",
				"A less interesting version is A333146.",
				"Unimodal compositions are A001523.",
				"Unimodal normal sequences are A007052.",
				"Non-unimodal normal sequences are A328509.",
				"Partitions with non-unimodal 0-appended first differences are A332284.",
				"Compositions whose negation is unimodal are A332578.",
				"Partitions with non-unimodal negated run-lengths are A332639.",
				"Numbers whose negated prime signature is not unimodal are A332642.",
				"Cf. A056239, A112798, A115981, A124010, A181819, A181821, A304660, A332280, A332283, A332288, A332638, A332669, A333145."
			],
			"keyword": "nonn",
			"offset": "1,8",
			"author": "_Gus Wiseman_, Mar 09 2020",
			"references": 12,
			"revision": 5,
			"time": "2020-03-09T18:26:39-04:00",
			"created": "2020-03-09T18:26:39-04:00"
		}
	]
}