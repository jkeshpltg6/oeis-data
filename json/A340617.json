{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A340617",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 340617,
			"data": "7,2,1,0,9,7,9,7,8,2,4,0,7,5,2,4,1,5,8,3,2,4,3,1,1,7,7,5,0,3,5,0,6,4,1,9,3,3,2,3,8,0,0,9,4,8,8,2,2,7,0,9,0,4,4,8,6,4,2,7,7,4,6,9,5,1,2,7,0,9,1,2,6,0,3,6,6,0,3,9,4,7,1,7,2,0,6,5,0,1,7,3,7,9,8,4,9,3,6,2,2,8,8,7,6,5",
			"name": "Decimal expansion of Product_{p prime, p == 3 (mod 4)} (1 - 2/p^2).",
			"link": [
				"X. Gourdon and P. Sebah, \u003ca href=\"http://numbers.computation.free.fr/Constants/Miscellaneous/constantsNumTheory.html\"\u003eSome Constants from Number theory\u003c/a\u003e",
				"R. J. Mathar, \u003ca href=\"http://arxiv.org/abs/1008.2547\"\u003eTable of Dirichlet L-series and Prime Zeta Modulo functions for small moduli\u003c/a\u003e, arXiv:1008.2547 [math.NT], 2010-2015, page 38 (case 4 3 2)."
			],
			"formula": [
				"Equals 2*A065474/A335963."
			],
			"example": [
				"0.7210979782407524158324311775035064193323800948822709044864277469512..."
			],
			"maple": [
				"Digits := 150;",
				"with(NumberTheory);",
				"DirichletBeta := proc(s) (Zeta(0, s, 1/4) - Zeta(0, s, 3/4))/4^s; end proc;",
				"alfa := proc(s) DirichletBeta(s)*Zeta(s)/((1 + 1/2^s)*Zeta(2*s)); end proc;",
				"beta := proc(s) (1 - 1/2^s)*Zeta(s)/DirichletBeta(s); end proc;",
				"pzetamod43 := proc(s, terms) 1/2*Sum(Moebius(2*j + 1)*log(beta((2*j + 1)*s))/(2*j + 1), j = 0..terms); end proc;",
				"evalf(exp(-Sum(2^t*pzetamod43(2*t, 70)/t, t = 1..200)));"
			],
			"mathematica": [
				"S[m_, n_, s_] := (t = 1; sums = 0; difs = 1; While[Abs[difs] \u003e 10^(-digits - 5) || difs == 0, difs = (MoebiusMu[t]/t) * Log[If[s*t == 1, DirichletL[m, n, s*t], Sum[Zeta[s*t, j/m]*DirichletCharacter[m, n, j]^t, {j, 1, m}]/m^(s*t)]]; sums = sums + difs; t++]; sums);",
				"P[m_, n_, s_] := 1/EulerPhi[m] * Sum[Conjugate[DirichletCharacter[m, r, n]] * S[m, r, s], {r, 1, EulerPhi[m]}] + Sum[If[GCD[p, m] \u003e 1 \u0026\u0026 Mod[p, m] == n, 1/p^s, 0], {p, 1, m}];",
				"Z2[m_, n_, s_] := (w = 1; sumz = 0; difz = 1; While[Abs[difz] \u003e 10^(-digits - 5), difz = 2^w * P[m, n, s*w]/w; sumz = sumz + difz; w++]; Exp[-sumz]);",
				"$MaxExtraPrecision = 1000; digits = 121; RealDigits[Chop[N[Z2[4, 3, 2], digits]], 10, digits-1][[1]]"
			],
			"xref": [
				"Cf. A002145, A065474, A085991, A243379, A335963."
			],
			"keyword": "nonn,cons",
			"offset": "0,1",
			"author": "_Vaclav Kotesovec_, Jan 13 2021",
			"references": 1,
			"revision": 21,
			"time": "2021-08-25T15:47:10-04:00",
			"created": "2021-01-13T15:39:24-05:00"
		}
	]
}