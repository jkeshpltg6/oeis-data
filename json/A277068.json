{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A277068",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 277068,
			"data": "1,1,1,1,6,1,18,1,3,2,1,1,1,2,2,1,2,21,1,6,2,1,3,1,2,1,2,6,2,4,2,1,1,4,2,3,1,1,2,2,3,2,2,1,1,1,1,1,2,12,2,1,1,2,2,2,1,4,3,4,2,2,2,1,5,1,1,4,2,2,2,3,1,7,2,1,1,2,2,6,7,1,1,2,2,8",
			"name": "a(n) = gcd(s1, s2), where s1 is the sum of the odd numbers and s2 is the sum of the even numbers in the Collatz (3x+1)trajectory of n.",
			"comment": [
				"Statistics of a(n) for the first 10^6 terms:",
				"+------+-----------------+------------+",
				"|      | number of terms |            |",
				"|      |    such that    |            |",
				"|   n  | gcd(s1, s2) = n | percentage |",
				"+------+-----------------+------------+",
				"|    1 |     401614      |   40.16%   |",
				"|    2 |     305471      |   30.54%   |",
				"|    3 |      44381      |    4.44%   |",
				"|    4 |      76228      |    7.62%   |",
				"|    5 |      15966      |    1.60%   |",
				"|    6 |      34514      |    3.45%   |",
				"|    7 |       8969      |    0.90%   |",
				"|    8 |      19156      |    1.92%   |",
				"|    9 |       4941      |    0.49%   |",
				"|   10 |      12212      |    1.22%   |",
				"|   11 |       3316      |    0.33%   |",
				"|   12 |       8234      |    0.82%   |",
				"| \u003e 12 |      64998      |    6.50%   |",
				"+------+-----------------+------------+",
				"It seems that the values of the third column oscillate infinitely when n tend towards infinity.",
				"Records: 1, 6, 18, 21, 23, 93, 187, 560, 1730, 5098, 10552, 11060, 11657, 31072, 32468, 306770, 793906, 1956888, 3107101, 12210181, etc.; they appear at 1, 5, 7, 18, 133, 147, 186, 270, 839, 5090, 5244, 5488, 23255, 62132, 113624, 153341, 793842, 6849034, 9321240, 12210146, etc. - _Robert G. Wilson v_, Oct 03 2016"
			],
			"link": [
				"Michel Lagneau, \u003ca href=\"/A277068/b277068.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Robert G. Wilson v, \u003ca href=\"/A277068/a277068.txt\"\u003eThe first occurrence of a(n)\u003c/a\u003e"
			],
			"example": [
				"a(5)=6 because the Collatz trajectory of 5 is 5 -\u003e 16 -\u003e 8 -\u003e 4 -\u003e 2 -\u003e 1 =\u003e s1 = 5+1 = 6, s2 = 16+8+4+2 = 30, and gcd(6, 30) = 6."
			],
			"maple": [
				"nn:=10^7:",
				"for n from 1 to 100 do:",
				"  m:=n:s1:=0:s2:=0:",
				"   for i from 1 to nn while(m\u003c\u003e1) do:",
				"    if irem(m,2)=0",
				"     then",
				"     s2:=s2+m:m:=m/2:",
				"     else",
				"     s1:=s1+m:m:=3*m+1:",
				"    fi:",
				"   od:",
				"     x:=gcd(s1+1,s2): printf(`%d, `,x):",
				"  od:"
			],
			"mathematica": [
				"Collatz[n_] := NestWhileList[ If[ OddQ[#], 3#+1, #/2] \u0026, n, # \u003e 1 \u0026]; f[n_] := Block[{c = Collatz@ n}, GCD[Plus @@ Select[c, OddQ], Plus @@ Select[c, EvenQ]]]; Array[f, 86] (* _Robert G. Wilson v_, Oct 03 2016 *)"
			],
			"program": [
				"(PARI) a(n) = {my(se = 0); my(so = 0); while (n!=1, if (n % 2, so+=n; n = 3*n+1, se +=n; n = n/2);); gcd(se, so+1);} \\\\ _Michel Marcus_, Oct 03 2016"
			],
			"xref": [
				"Cf. A213909, A213916, A271973."
			],
			"keyword": "nonn",
			"offset": "1,5",
			"author": "_Michel Lagneau_, Sep 28 2016",
			"references": 3,
			"revision": 40,
			"time": "2017-09-15T23:29:25-04:00",
			"created": "2016-10-07T00:25:51-04:00"
		}
	]
}