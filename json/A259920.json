{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A259920",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 259920,
			"data": "1,1,1,1,2,0,1,1,2,1,2,1,3,2,2,2,3,1,3,2,5,3,5,2,6,3,6,3,7,4,7,5,9,5,9,5,11,6,11,7,14,7,15,9,17,9,17,9,21,11,21,12,25,13,25,15,29,16,31,17,35,19,37,21,42,22,44,25,49,27,52,29,58,32,61",
			"name": "Expansion of phi(-x^5) * f(-x^5) / f(-x, -x^4) in powers of x where phi() and f() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Rogers-Ramanujan functions: G(q) (see A003114), H(q) (A003106)."
			],
			"reference": [
				"Srinivasa Ramanujan, The Lost Notebook and Other Unpublished Papers, Narosa Publishing House, New Delhi, 1988, p. 23, 8th equation."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A259920/b259920.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of f(-x^5)^3 / (f(-x^10) * f(-x^2, -x^3)) in powers of x where f(,) is the Ramanujan general theta function.",
				"Expansion of phi(-x^5) * G(x) in powers of x where f(,) is the Ramanujan general theta function and G() is a Rogers-Ramanujan function. - _Michael Somos_, Jul 09 2015",
				"Euler transform of period 10 sequence [ 1, 0, 0, 1, -2, 1, 0, 0, 1, -1, ...].",
				"G.f.: (Sum_{k in Z} (-1)^k * x^(5*k^2)) / (Product_{k in Z} 1 - x^abs(5*k + 1))."
			],
			"example": [
				"G.f. = 1 + x + x^2 + x^3 + 2*x^4 + x^6 + x^7 + 2*x^8 + x^9 + 2*x^10 + x^11 + ...",
				"G.f. = q^-1 + q^59 + q^119 + q^179 + 2*q^239 + q^359 + q^419 + 2*q^479 + q^539 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 4, 0, x^5] / (QPochhammer[ x, x^5] QPochhammer[ x^4, x^5]), {x, 0, n}];",
				"a[ n_] := SeriesCoefficient[ Product[ (1 - x^k)^{ -1, 0, 0, -1, 2, -1, 0, 0, -1, 1}[[Mod[k, 10, 1]]], {k, n}], {x, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, 0, polcoeff( prod(k=1, n, (1 - x^k + x * O(x^n))^ [1, -1, 0, 0, -1, 2, -1, 0, 0, -1][k%10+1]), n))};"
			],
			"xref": [
				"Cf. A053256, A053266."
			],
			"keyword": "nonn",
			"offset": "0,5",
			"author": "_Michael Somos_, Jul 08 2015",
			"references": 1,
			"revision": 12,
			"time": "2021-03-12T22:24:48-05:00",
			"created": "2015-07-08T22:07:33-04:00"
		}
	]
}