{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A061602",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 61602,
			"data": "1,1,2,6,24,120,720,5040,40320,362880,2,2,3,7,25,121,721,5041,40321,362881,3,3,4,8,26,122,722,5042,40322,362882,7,7,8,12,30,126,726,5046,40326,362886,25,25,26,30,48,144,744,5064,40344,362904,121,121,122,126",
			"name": "Sum of factorials of the digits of n.",
			"comment": [
				"Numbers n such that a(n) = n are known as factorions. It is known that there are exactly four of these [in base 10]: 1, 2, 145, 40585. - _Amarnath Murthy_",
				"The sum of factorials of the digits is the same for 0, 1, 2 in any base. - _Alonso del Arte_, Oct 21 2012"
			],
			"link": [
				"Harry J. Smith and Indranil Ghosh, \u003ca href=\"/A061602/b061602.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e (first 1001 terms from Harry J. Smith)",
				"Project Euler, \u003ca href=\"https://projecteuler.net/problem=74\"\u003eProblem 74: Digit factorial chains\u003c/a\u003e",
				"H. J. J. te Riele, \u003ca href=\"https://ir.cwi.nl/pub/6662\"\u003eIteration of number-theoretic functions\u003c/a\u003e, Nieuw Archief v. Wiskunde, (4) 1 (1983), 345-360. See Example I.1.b.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Factorion.html\"\u003eFactorion.\u003c/a\u003e"
			],
			"example": [
				"a(24) = (2!) + (4!) = 2 + 24 = 26.",
				"a(153) = 127 because 1! + 5! + 3! = 1 + 120 + 6 = 127."
			],
			"maple": [
				"A061602 := proc(n)",
				"        add(factorial(d),d=convert(n,base,10)) ;",
				"end proc: # _R. J. Mathar_, Dec 18 2011"
			],
			"mathematica": [
				"a[n_] := Total[IntegerDigits[n]! ]; Table[a[n], {n, 1, 53}] (* Saif Hakim (saif7463(AT)gmail.com), Apr 23 2006 *)"
			],
			"program": [
				"(PARI) { for (n=0, 1000, a=0; x=n; until (x==0, a+=(x - 10*(x\\10))!; x=x\\10); write(\"b061602.txt\", n, \" \", a) ) } \\\\ _Harry J. Smith_, Jul 25 2009",
				"(MAGMA) a061602:=func\u003c n | n eq 0 select 1 else \u0026+[ Factorial(d): d in Intseq(n) ] \u003e; [ a061602(n): n in [0..60] ]; // _Klaus Brockhaus_, Nov 23 2010",
				"(Python)",
				"import math",
				"def A061602(n):",
				"    s=0",
				"    for i in str(n):",
				"        s+=math.factorial(int(i))",
				"    return s # _Indranil Ghosh_, Jan 11 2017",
				"(R)",
				"i=0",
				"values \u003c- c()",
				"while (i\u003c1000) {",
				"  values[i+1] \u003c- A061602(i)",
				"  i=i+1",
				"}",
				"plot(values)",
				"A061602 \u003c- function(n) {",
				"  sum=0;",
				"  numberstring \u003c- paste0(i)",
				"  numberstring_split \u003c- strsplit(numberstring, \"\")[[1]]",
				"  for (number in numberstring_split) {",
				"    sum = sum+factorial(as.numeric(number))",
				"  }",
				"  return(sum)",
				"}",
				"# _Raphaël Deknop_, Nov 08 2021"
			],
			"xref": [
				"Cf. A061603, A108911, A193163.",
				"See also A014080, A214285, A254499, A306955."
			],
			"keyword": "nonn,base,easy",
			"offset": "0,3",
			"author": "_Amarnath Murthy_, May 19 2001",
			"ext": [
				"Corrected and extended by _Vladeta Jovovic_, May 19 2001",
				"Link and amended comment by Mark Hudson (mrmarkhudson(AT)hotmail.com), Nov 12 2004"
			],
			"references": 40,
			"revision": 44,
			"time": "2021-11-24T09:14:02-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}