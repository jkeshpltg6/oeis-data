{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A035929",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 35929,
			"data": "0,1,1,1,2,6,19,61,200,670,2286,7918,27770,98424,351983,1268541,4602752,16799894,61642078,227239086,841230292,3126039364,11656497518,43601626146,163561902392,615183356156,2319423532024,8764535189296,33187922345210,125912855167740",
			"name": "Number of Dyck n-paths starting U^mD^m (an m-pyramid), followed by a pyramid-free Dyck path.",
			"comment": [
				"Hankel transform is -A128834. - _Paul Barry_, Jul 04 2009"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A035929/b035929.txt\"\u003eTable of n, a(n) for n = 0..500\u003c/a\u003e",
				"J.-L. Baril, S. Kirgizov, \u003ca href=\"http://jl.baril.u-bourgogne.fr/Stirling.pdf\"\u003eThe pure descent statistic on permutations\u003c/a\u003e, Preprint, 2016.",
				"Paul Barry, \u003ca href=\"https://arxiv.org/abs/1912.11845\"\u003eChebyshev moments and Riordan involutions\u003c/a\u003e, arXiv:1912.11845 [math.CO], 2019.",
				"W. Kuszmaul, \u003ca href=\"http://arxiv.org/abs/1509.08216\"\u003eFast Algorithms for Finding Pattern Avoiders and Counting Pattern Occurrences in Permutations\u003c/a\u003e, arXiv:1509.08216 [cs.DM], 2015.",
				"Murray Tannock, \u003ca href=\"https://skemman.is/bitstream/1946/25589/1/msc-tannock-2016.pdf\"\u003eEquivalence classes of mesh patterns with a dominating pattern\u003c/a\u003e, MSc Thesis, Reykjavik Univ., May 2016."
			],
			"formula": [
				"G.f.: A(x) satisfies A^2*(x^2-2*x+2) - A*(x+1) + x = 0.",
				"The generating function can be written as x/(1-x) times that of A082989.",
				"G.f.: (2*x)/(1+x+(1-x)*sqrt(1-4*x)) = 1/(1-x(1-x)/(1-x/(1-x/(1-x/(1-x/(1-x/(1-... (continued fraction). - _Paul Barry_, Jul 04 2009",
				"From _Gary W. Adamson_, Jul 14 2011: (Start)",
				"a(n), n\u003e0; is the upper left term in M^(n-1), where M is the infinite square production matrix:",
				"  1, 1, 0, 0, 0, 0, ...",
				"  0, 1, 1, 0, 0, 0, ...",
				"  1, 1, 1, 1, 0, 0, ...",
				"  1, 1, 1, 1, 1, 0, ...",
				"  1, 1, 1, 1, 1, 1, ...",
				"  ... (End)",
				"D-finite with recurrence: 2*n*a(n) +4*(-3*n+4)*a(n-1) +(19*n-44)*a(n-2) + (-13*n + 36)*a(n-3) +2*(2*n-7)*a(n-4)=0. - _R. J. Mathar_, Nov 24 2012",
				"a(n) ~ 3 * 4^n / (25 * sqrt(Pi) * n^(3/2)). - _Vaclav Kotesovec_, Feb 12 2014",
				"From _Alexander Burstein_, Aug 05 2017: (Start)",
				"G.f: A = x/(1-(1-x)*x*C) = x*C/(1+x^2*C^2) = x*C^3/(1+2*x*C^3), where C is the g.f. of A000108.",
				"A/x composed with x*C = g.f. of A165543, where A and C are as above. (End)"
			],
			"example": [
				"The a(5) = 6 cases are UUUUUDDDDD, UDUUUDUDDD, UDUUUDDUDD, UDUUDUUDDDD, UDUUDUDUDUDD and UUDDUUDUDD."
			],
			"maple": [
				"A:= proc(n) option remember; if n=0 then 0 else convert (series ((A(n-1)^2 *(x^2-2*x+2) +x)/ (x+1), x,n+1), polynom) fi end: a:= n-\u003e coeff (A(n), x,n): seq (a(n), n=0..25); # _Alois P. Heinz_, Aug 23 2008"
			],
			"mathematica": [
				"CoefficientList[Series[2*x/(1+x+(1-x)*Sqrt[1-4*x]), {x, 0, 20}], x] (* _Vaclav Kotesovec_, Feb 12 2014 *)"
			],
			"program": [
				"(PARI) x='x+O('x^30); concat([0], Vec(2*x/(1+x+(1-x)*sqrt(1-4*x)))) \\\\ _G. C. Greubel_, Jan 15 2018",
				"(MAGMA) /* Expansion */ Q:=Rationals(); R\u003cx\u003e:=PowerSeriesRing(Q,30); R!(2*x/(1+x+(1-x)*Sqrt(1-4*x))); // _G. C. Greubel_, Jan 15 2018"
			],
			"xref": [
				"Cf. A082989."
			],
			"keyword": "nonn",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Edited by _Louis Shapiro_, Feb 16 2005",
				"Wrong g.f. removed by _Vaclav Kotesovec_, Feb 12 2014"
			],
			"references": 4,
			"revision": 63,
			"time": "2020-03-15T02:27:50-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}