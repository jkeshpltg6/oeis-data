{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A227401",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 227401,
			"data": "1,1,1,1,1,2,4,5,5,5,6,9,12,15,16,17,20,26,34,40,44,48,55,68,84,98,108,118,135,161,192,221,244,268,303,354,414,470,519,571,641,737,847,954,1052,1156,1291,1465,1664,1861,2048,2248,2496,2807,3158,3511,3855",
			"name": "Expansion of chi(x^6) / (chi(-x) * chi(x^3)) in powers of x which chi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A227401/b227401.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Vaclav Kotesovec, \u003ca href=\"https://arxiv.org/abs/1509.08708\"\u003eA method of finding the asymptotics of q-series based on the convolution of generating functions\u003c/a\u003e, arXiv:1509.08708 [math.CO], 2015-2016.",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of f(x^6) / f(-x^1, -x^5) in powers of x where f(,) is a Ramanujan theta function.",
				"Expansion of q^(1/12) * eta(q^2) * eta(q^3) * eta(q^12)^3 / (eta(q) * eta(q^6)^3 * eta(q^24)) in powers of q.",
				"Euler transform of period 24 sequence [ 1, 0, 0, 0, 1, 2, 1, 0, 0, 0, 1, -1, 1, 0, 0, 0, 1, 2, 1, 0, 0, 0, 1, 0, ...].",
				"a(n) ~ 11^(1/4) * exp(Pi*sqrt(11*n)/6) / (4*sqrt(6)*n^(3/4)). - _Vaclav Kotesovec_, Jul 11 2016"
			],
			"example": [
				"G.f. = 1 + x + x^2 + x^3 + x^4 + 2*x^5 + 4*x^6 + 5*x^7 + 5*x^8 + 5*x^9 + ...",
				"G.f. = 1/q + q^11 + q^23 + q^35 + q^47 + 2*q^59 + 4*q^71 + 5*q^83 + 5*q^95 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ -x^6] / (QPochhammer[ x, x^6] QPochhammer[ x^5, x^6] QPochhammer[ x^6]), {x, 0, n}];",
				"a[ n_] := SeriesCoefficient[ QPochhammer[ -x^6, x^12] QPochhammer[ -x, x] QPochhammer[ x^3, -x^3], {x, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A) * eta(x^3 + A) * eta(x^12 + A)^3 / (eta(x + A) * eta(x^6 + A)^3 * eta(x^24 + A)), n))};"
			],
			"keyword": "nonn",
			"offset": "0,6",
			"author": "_Michael Somos_, Sep 20 2013",
			"references": 1,
			"revision": 21,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2013-09-21T03:00:10-04:00"
		}
	]
}