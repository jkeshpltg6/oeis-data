{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A080992",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 80992,
			"data": "16,3,2,13,5,10,11,8,9,6,7,12,4,15,14,1",
			"name": "Entries in Durer's magic square.",
			"comment": [
				"4 X 4 magic square included in Albrecht Durer's 1514 engraving \"Melancolia\". 15 and 14 appear in the bottom row, giving the date.",
				"A006003(4) = 34 is the magic constant, occurring 23 times as sum of exactly 4 distinct numbers 1..16 with regular patterns in the 4 X 4 square(see also link): - _Reinhard Zumkeller_, Jun 20 2013",
				"sum(T(k,i): i = 1..4) = sum(T(i,k): i = 1..4) = 34, for k = 1..4;",
				"sum(T(k,k): k = 1..4) = sum(T(k,5-k): k = 1..4) = 34;",
				"T(1,1) + T(1,2) + T(2,1) + T(2,2) = 16 + 3 + 5 + 10 = 34;",
				"T(1,3) + T(1,4) + T(2,3) + T(2,4) = 2 + 13 + 11 + 8 = 34;",
				"T(3,1) + T(3,2) + T(4,1) + T(4,2) = 9 + 6 + 4 + 15 = 34;",
				"T(3,3) + T(3,4) + T(4,3) + T(4,4) = 7 + 12 + 14 + 1 = 34;",
				"T(1,1) + T(1,4) + T(4,1) + T(4,4) = 16 + 13 + 4 + 1 = 34;",
				"T(2,2) + T(2,3) + T(3,2) + T(3,3) = 10 + 11 + 6 + 7 = 34;",
				"T(1,2) + T(2,4) + T(4,3) + T(3,1) = 3 + 8 + 14 + 9 = 34;",
				"T(1,3) + T(3,4) + T(4,2) + T(2,1) = 2 + 12 + 15 + 5 = 34;",
				"T(1,2) + T(2,3) + T(4,2) + T(2,1) = 3 + 11 + 15 + 5 = 34;",
				"T(1,3) + T(2,4) + T(4,3) + T(2,2) = 2 + 8 + 14 + 10 = 34;",
				"T(1,2) + T(3,3) + T(4,2) + T(3,1) = 3 + 7 + 15 + 9 = 34;",
				"T(1,3) + T(3,4) + T(4,3) + T(3,2) = 2 + 12 + 14 + 6 = 34;",
				"T(1,2) + T(1,3) + T(4,2) + T(4,3) = 3 + 2 + 15 + 14 = 34;",
				"T(4,2)*100 + T(4,3) = 1514, the year of the engraving and the pair (T(4,4),T(4,1)) = (1,4) corresponds to Albrecht Dürer's coded initials.",
				"The square has its magic constant (34) equal to one of its eigenvalues (34, 8, -8, 0) like any other normal magic square of order n \u003e 2. - _Michal Paulovic_, Mar 14 2021"
			],
			"reference": [
				"Hossin Behforooz, \"Permutation-free magic squares\", J. Recreational Mathematics, vol. 33, (2004-2005), pp. 103-106."
			],
			"link": [
				"History 291, Princeton University, \u003ca href=\"http://www.princeton.edu/~his291/Durer_Melancolia.html\"\u003eDurer's Melancolia\u003c/a\u003e",
				"Laurence Eaves and Brady Haran, \u003ca href=\"http://www.youtube.com/watch?v=gGvyeuDT2Do\"\u003eMagic square - Sixty Symbols\u003c/a\u003e",
				"A. Skalli, \u003ca href=\"https://sites.google.com/site/aliskalligvaen/home-page/-magic-cube-with-duerer-s-square\"\u003eMagic cube with Dürer's square\u003c/a\u003e",
				"Torsten \"Kermit\", \u003ca href=\"https://web.archive.org/web/20150912040302/http://www.heim2.tu-clausthal.de/~kermit/faust-duerer.shtml\"\u003eDie Rolle Dürers in Thomas Manns Doktor Faustus\u003c/a\u003e (in German).",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/DuerersMagicSquare.html\"\u003eDürer' Magic Square\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/GnomonMagicSquare.html\"\u003eGnomon Magic Square\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Magic_square#Albrecht_D.C3.BCrer.27s_magic_square\"\u003eAlbrecht Dürer's magic square\u003c/a\u003e",
				"Reinhard Zumkeller, \u003ca href=\"/A080992/a080992.txt\"\u003eThe 23 sums in Albrecht Dürer's magic square\u003c/a\u003e",
				"\u003ca href=\"/index/Mag#magic\"\u003eIndex entries for sequences related to magic squares\u003c/a\u003e"
			],
			"example": [
				".          1    2    3    4",
				".       +----+----+----+----+",
				".    1  | 16 |  3 |  2 | 13 |",
				".       +----+----+----+----+",
				".    2  |  5 | 10 | 11 |  8 |",
				".       +----+----+----+----+",
				".    3  |  9 |  6 |  7 | 12 |",
				".       +----+----+----+----+",
				".    4  |  4 | 15 | 14 |  1 |",
				".       +----+----+----+----+",
				".          D   ^^   ^^    A"
			],
			"keyword": "fini,full,nonn",
			"offset": "1,1",
			"author": "_David W. Wilson_, Feb 26 2003",
			"ext": [
				"Extended by _Reinhard Zumkeller_, Jun 20 2013"
			],
			"references": 4,
			"revision": 24,
			"time": "2021-04-09T18:10:36-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}