{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A027642",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 27642,
			"data": "1,2,6,1,30,1,42,1,30,1,66,1,2730,1,6,1,510,1,798,1,330,1,138,1,2730,1,6,1,870,1,14322,1,510,1,6,1,1919190,1,6,1,13530,1,1806,1,690,1,282,1,46410,1,66,1,1590,1,798,1,870,1,354,1,56786730,1",
			"name": "Denominator of Bernoulli number B_n.",
			"comment": [
				"Row products of A138243. - _Mats Granvik_, Mar 08 2008",
				"Equals row products of triangle A143343 and for a(n) \u003e 1, row products of triangle A080092. - _Gary W. Adamson_, Aug 09 2008",
				"Julius Worpitzky's 1883 algorithm for generating Bernoulli numbers is described in A028246. - _Gary W. Adamson_, Aug 09 2008",
				"The sequence of denominators of B_n is defined here by convention, not by necessity. The convention amounts to mapping 0 to the rational number 0/1. It might be more appropriate to regard numerators and denominators of the Bernoulli numbers as independent sequences N_n and D_n which combine to B_n = N_n / D_n. This is suggested by the theorem of Clausen which describes the denominators as the sequence D_n = 1, 2, 6, 2, 30, 2, 42, ... which combines with N_n = 1, -1, 1, 0, -1, 0, ... to the sequence of Bernoulli numbers. (Cf. A141056 and A027760.) - _Peter Luschny_, Apr 29 2009"
			],
			"reference": [
				"M. Abramowitz and I. A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, 1964 (and various reprintings), p. 810.",
				"Jacob Bernoulli, Ars Conjectandi, Basel: Thurneysen Brothers, 1713. See page 97.",
				"Thomas Clausen, \"Lehrsatz aus einer Abhandlung Über die Bernoullischen Zahlen\", Astr. Nachr. 17 (1840), 351-352 (see P. Luschny link).",
				"L. Comtet, Advanced Combinatorics, Reidel, 1974, p. 49.",
				"H. T. Davis, Tables of the Mathematical Functions. Vols. 1 and 2, 2nd ed., 1963, Vol. 3 (with V. J. Fisher), 1962; Principia Press of Trinity Univ., San Antonio, TX, Vol. 2, p. 230.",
				"L. M. Milne-Thompson, Calculus of Finite Differences, 1951, p. 137.",
				"Roger Plymen, The Great Prime Number Race, AMS, 2020. See pp. 8-10."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A027642/b027642.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"Beáta Bényi and Péter Hajnal, \u003ca href=\"https://arxiv.org/abs/1804.01868\"\u003ePoly-Bernoulli Numbers and Eulerian Numbers\u003c/a\u003e, arXiv:1804.01868 [math.CO], 2018.",
				"K.-W. Chen, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL4/CHEN/AlgBE2.html\"\u003eAlgorithms for Bernoulli numbers and Euler numbers\u003c/a\u003e, J. Integer Sequences, 4 (2001), #01.1.6.",
				"Ghislain R. Franssens, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL9/Franssens/franssens13.html\"\u003eOn a Number Pyramid Related to the Binomial, Deleham, Eulerian, MacMahon and Stirling number triangles\u003c/a\u003e, Journal of Integer Sequences, Vol. 9 (2006), Article 06.4.1.",
				"H. W. Gould and J. Quaintance, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Quaintance/quain3.html\"\u003eBernoulli Numbers and a New Binomial Transform Identity\u003c/a\u003e, J. Int. Seq. 17 (2014) # 14.2.2",
				"A. Iványi, \u003ca href=\"http://www.emis.de/journals/AUSM/C5-1/math51-5.pdf\"\u003eLeader election in synchronous networks\u003c/a\u003e, Acta Univ. Sapientiae, Mathematica, 5, 2 (2013) 54-82.",
				"M. Kaneko, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL3/KANEKO/AT-kaneko.html\"\u003eThe Akiyama-Tanigawa algorithm for Bernoulli numbers\u003c/a\u003e, J. Integer Sequences, 3 (2000), #00.2.9.",
				"Guo-Dong Liu, H. M. Srivastava, and Hai-Quing Wang, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Srivastava/sriva3.html\"\u003eSome Formulas for a Family of Numbers Analogous to the Higher-Order Bernoulli Numbers\u003c/a\u003e, J. Int. Seq. 17 (2014) # 14.4.6",
				"Peter Luschny, \u003ca href=\"http://www.luschny.de/math/zeta/ClausenNumbers.htm\"\u003eGeneralized Clausen numbers: definition and application\u003c/a\u003e.",
				"R. Mestrovic, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Mestrovic/mes4.html\"\u003eOn a Congruence Modulo n^3 Involving Two Consecutive Sums of Powers\u003c/a\u003e, Journal of Integer Sequences, Vol. 17 (2014), 14.8.4.",
				"Hisanori Mishima, \u003ca href=\"http://www.asahi-net.or.jp/~KC2H-MSM/mathland/matha1/matha103.htm\"\u003eFactorizations of many number sequences\u003c/a\u003e",
				"Hisanori Mishima, \u003ca href=\"http://www.asahi-net.or.jp/~KC2H-MSM/mathland/matha1/matha134.htm\"\u003eFactorizations of many number sequences\u003c/a\u003e",
				"Hisanori Mishima, \u003ca href=\"http://www.asahi-net.or.jp/~KC2H-MSM/mathland/matha1/matha1341.htm\"\u003eFactorizations of many number sequences\u003c/a\u003e",
				"A. F. Neto, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL18/Neto/neto7.html\"\u003eCarlitz's Identity for the Bernoulli Numbers and Zeon Algebra\u003c/a\u003e, J. Int. Seq. 18 (2015) # 15.5.6.",
				"Carl Pomerance and Samuel S. Wagstaff Jr, \u003ca href=\"https://arxiv.org/abs/2105.13252\"\u003eThe denominators of the Bernoulli numbers\u003c/a\u003e, arXiv:2105.13252 [math.NT], 2021.",
				"J. Sondow and E. Tsukerman, \u003ca href=\"https://arxiv.org/abs/1401.0322\"\u003eThe p-adic order of power sums, the Erdos-Moser equation, and Bernoulli numbers\u003c/a\u003e, arXiv:1401.0322 [math.NT], 2014; see section 5.",
				"Matthew Roughan, \u003ca href=\"https://arxiv.org/abs/2010.09860\"\u003eThe Polylogarithm Function in Julia\u003c/a\u003e, arXiv:2010.09860 [math.NA], 2020.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Bernoulli_number\"\u003eBernoulli number\u003c/a\u003e",
				"\u003ca href=\"/index/Be#Bernoulli\"\u003eIndex entries for sequences related to Bernoulli numbers.\u003c/a\u003e",
				"\u003ca href=\"/index/Cor#core\"\u003eIndex entries for \"core\" sequences\u003c/a\u003e"
			],
			"formula": [
				"E.g.f: x/(exp(x) - 1); take denominators.",
				"Let E(x) be the e.g.f., then E(x) = U(0), where U(k) =  2*k + 1 - x*(2*k+1)/(x + (2*k+2)/(1 + x/U(k+1))); (continued fraction, 3-step). - _Sergei N. Gladkovskii_, Jun 25 2012",
				"E.g.f.: x/(exp(x)-1) = E(0) where E(k) = 2*k+1 - x/(2 + x/E(k+1) ); (continued fraction). - _Sergei N. Gladkovskii_, Mar 16 2013",
				"E.g.f.: x/(exp(x)-1) = 2*E(0) - 2*x, where E(k)= x + (k+1)/(1 + 1/(1 - x/E(k+1) )); (continued fraction). - _Sergei N. Gladkovskii_, Jul 10 2013",
				"E.g.f.: x/(exp(x)-1) = (1-x)/E(0), where E(k) = 1 - x*(k+1)/(x*(k+1) + (k+2-x)*(k+1-x)/E(k+1) ); (continued fraction). - _Sergei N. Gladkovskii_, Oct 21 2013",
				"E.g.f.: conjecture: x/(exp(x)-1) = T(0)/2 - x, where T(k) = 8*k+2 + x/( 1 - x/( 8*k+6 + x/( 1 - x/T(k+1) ))); (continued fraction). - _Sergei N. Gladkovskii_, Nov 24 2013",
				"a(2*n) = 2*A001897(n) = A002445(n) = 3*A277087(n) for n \u003e= 1. _Jonathan Sondow_, Dec 14 2016"
			],
			"example": [
				"B_n sequence begins 1, -1/2, 1/6, 0, -1/30, 0, 1/42, 0, -1/30, 0, 5/66, 0, -691/2730, 0, 7/6, 0, -3617/510, ..."
			],
			"maple": [
				"(-1)^n*sum( (-1)^'m'*'m'!*stirling2(n,'m')/('m'+1),'m'=0..n);",
				"A027642 := proc(n) denom(bernoulli(n)) ; end: # _Zerinvary Lajos_, Apr 08 2009"
			],
			"mathematica": [
				"Table[ Denominator[ BernoulliB[n]], {n, 0, 68}] (* _Robert G. Wilson v_, Oct 11 2004 *)",
				"Denominator[ Range[0, 68]! CoefficientList[ Series[x/(E^x - 1), {x, 0, 68}], x]]",
				"(* Alternative code using Clausen Theorem: *)",
				"A027642[k_Integer]:=If[EvenQ[k],Times@@Table[Max[1,Prime[i]*Boole[Divisible[k,Prime[i]-1]]],{i,1,PrimePi[2k]}],1+KroneckerDelta[k,1]]; (* _Enrique Pérez Herrero_, Jul 15 2010 *)",
				"a[0] = 1; a[n_] := Times @@ Select[Divisors[n] + 1, PrimeQ]; Table[a[n], {n, 0, 100}] (* _Jean-François Alcover_, Mar 12 2012, after Ilan Vardi, when direct computation for large n is unfeasible *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c0, 0, denominator(bernfrac(n)))",
				"(MAGMA) [Denominator(Bernoulli(n)): n in [0..150]]; // _Vincenzo Librandi_, Mar 29 2011",
				"(Haskell)",
				"a027642 n = a027642_list !! n",
				"a027642_list = 1 : map (denominator . sum) (zipWith (zipWith (%))",
				"   (zipWith (map . (*)) (tail a000142_list) a242179_tabf) a106831_tabf)",
				"-- _Reinhard Zumkeller_, Jul 04 2014",
				"(Sage)",
				"def A027642_list(len):",
				"    f, R, C = 1, [1], [1]+[0]*(len-1)",
				"    for n in (1..len-1):",
				"        f *= n",
				"        for k in range(n, 0, -1):",
				"            C[k] = C[k-1] / (k+1)",
				"        C[0] = -sum(C[k] for k in (1..n))",
				"        R.append((C[0]*f).denominator())",
				"    return R",
				"A027642_list(62) # _Peter Luschny_, Feb 20 2016",
				"(Python)",
				"from sympy import bernoulli",
				"[bernoulli(i).denominator() for i in range(51)] # _Indranil Ghosh_, Mar 18 2017"
			],
			"xref": [
				"See A027641 (numerators) for full list of references, links, formulas, etc.",
				"Cf. A002882, A003245, A127187, A127188, A138243, A028246, A143343, A080092, A141056, A027760.",
				"Cf. A242179, A106831, A000142.",
				"Cf. A001897, A002445, A277087."
			],
			"keyword": "nonn,frac,easy,core,nice",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 328,
			"revision": 137,
			"time": "2021-10-26T14:21:23-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}