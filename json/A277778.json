{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A277778",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 277778,
			"data": "1,1,0,1,1,1,-1,0,0,1,0,1,2,1,-1,-1,0,0,-1,0,1,1,-1,0,-1,1,1,2,2,1,0,-1,-1,-1,0,0,1,0,-1,-1,-1,0,0,1,2,1,-1,-1,1,0,-2,-1,0,1,-1,1,0,2,1,2,1,1,1,0,0,-1,0,-1,-1,-1,0,0,-1,0,1,1,1,0,0",
			"name": "If n is even, a(n) = a(n/2-1), and if n is odd, a(n) = a((n-1)/2) - a((n+1)/2), with a(1) = a(2) = 1.",
			"comment": [
				"This sequence grows very slowly, in both positive and negative directions. The first 3 in the list is a(221), the first 12 is a(122333), and the first -13 is a(980851).",
				"First occurrences: a(1) = 1, a(3) = 0, a(7) = -1, a(13) = 2, a(51) = -2, a(221) = 3, a(477) = 4, a(845) = -3, a(1907) = -4, a(3549) = 5, a(7389) = 6, a(7645) = 7, a(13533) = -5, a(27101) = -6, a(30579) = -7, a(56797) = 8, a(61157) = 9, a(117981) = 10, a(122333) = 12, a(216541) = -9, a(216805) = -8, a(236509) = 11, a(245213) = 13, a(433629) = -11, a(471923) = -10, a(489331) = -12, a(978533) = 14, a(978661) = 15, a(980851) = -13, a(1818077) = 16, a(1887709) = 17, a(1957341) = 20, a(3464669) = -15, a(3469029) = -14, a(3755485) = -16, a(3775453) = 18, a(3914701) = 19, a(3914717) = 21, a(3915229) = 22, a(3923421) = 23, a(6938077) = -19, a(7511517) = -17, a(7773661) = -18, a(7829363) = -20, a(15658725) = 25, a(15658867) = -21, a(15660915) = -22, a(15693683) = -23, a(28949981) = 24, a(29089245) = 28, a(29220317) = 27, a(30199005) = 26, a(30203357) = 29, a(31313117) = 30, a(31317469) = 33. - _Charles R Greathouse IV_, Oct 30 2016",
				"From _Robert Israel_, Nov 10 2016: (Start)",
				"a(2^k-2) = 1.",
				"a(2^k-1) = A010892(k).",
				"a(2^k) = A010892(k-1).",
				"a(13*(16^k-1)/15) = A000045(k+2) for k \u003e= 1.",
				"Using this, there is c\u003e0 such that a(n) \u003e c n^d for infinitely many n, where d = log_16((1+sqrt(5))/2) = 0.1735604784...",
				"(End)"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A277778/b277778.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Robert G. Wilson v, \u003ca href=\"/A277778/a277778.txt\"\u003e First occurrence of k.\u003c/a\u003e"
			],
			"formula": [
				"G.f. satisfies A(x) = (x^2 + x - 1/x) * A(x^2) + 2*x + x^2. - _Andrey Zabolotskiy_, Oct 30 2016",
				"|a(n)| \u003c\u003c n^0.71. - _Charles R Greathouse IV_, Nov 01 2016"
			],
			"example": [
				"The first two terms are a(1) = a(2) = 1. To get the next two terms, subtract the second from the first to get a(3) = a(1) - a(2) = 0 and copy the first term as a(4) = a(1) = 1.",
				"To find a(5) and a(6), start over using a(2) and a(3); then for a(7) and a(8), use a(3) and a(4); and so on."
			],
			"maple": [
				"N:= 200: # to get a(1) .. a(N)",
				"A[1]:= 1: A[2]:= 1:",
				"for j from 1 to (N-1)/2 do",
				"  A[2*j+1]:= A[j] - A[j+1];",
				"  A[2*j+2]:= A[j];",
				"od:",
				"seq(A[i],i=1..N); # _Robert Israel_, Nov 10 2016"
			],
			"mathematica": [
				"a[n_] := a[n] = If[ OddQ[n], a[(n - 1)/2] - a[(n + 1)/2], a[n/2 - 1]]; a[1] = a[2] = 1; Array[a, 100] (* _Robert G. Wilson v_, Nov 11 2016 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c7, return(n!=3)); if(n%2, a(n\\2) - a(n\\2+1), a(n/2-1)) \\\\ _Charles R Greathouse IV_, Oct 30 2016"
			],
			"xref": [
				"Cf. A000045, A005590, A010892, A190610."
			],
			"keyword": "sign",
			"offset": "1,13",
			"author": "_Tristan Cam_, Oct 30 2016",
			"references": 2,
			"revision": 38,
			"time": "2016-11-11T03:31:40-05:00",
			"created": "2016-11-02T12:53:17-04:00"
		}
	]
}