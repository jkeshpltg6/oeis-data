{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A306547",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 306547,
			"data": "1,1,-2,1,-11,4,1,-55,35,-8,1,-274,210,-91,16,1,-1368,986,-637,219,-32,1,-6837,3180,-3473,1752,-507,64,1,-34181,-1431,-17951,10543,-4563,1147,-128,1,-170900,-145310,-129950,48442,-30524,11470,-2555,256,1,-854494,-1726360,-1490890,-2314,-177832,84176,-28105,5627,-512",
			"name": "Triangle read by rows, defined by Riordan's general Eulerian recursion: T(n, k) = (k+3)*T(n-1, k) + (n-k-2) * T(n-1, k-1) with T(n,1) = 1, T(n,n) = (-2)^(n-1).",
			"comment": [
				"Row sums are {1, -1, -6, -27, -138, -831, -5820, -46563, -419070, -4190703, ...}.",
				"The Mathematica code for e(n,k,m) gives eleven sequences of which the first few are in the OEIS (see Crossrefs section)."
			],
			"reference": [
				"J. Riordan, An Introduction to Combinatorial Analysis, Wiley, 1958, pp. 214-215."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A306547/b306547.txt\"\u003eRows n = 1..100 of triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = (k+3)*T(n-1, k) + (n-k-2)*T(n-1, k-1) with T(n,1) = 1, T(n,n) = (-2)^(n-1).",
				"e(n,k,m)= (k+m)*e(n-1, k, m) + (n-k+1-m)*e(n-1, k-1, m) with m=3."
			],
			"example": [
				"Triangle begins with:",
				"1.",
				"1,      -2.",
				"1,     -11,       4.",
				"1,     -55,      35,      -8.",
				"1,    -274,     210,     -91,    16.",
				"1,   -1368,     986,    -637,   219,    -32.",
				"1,   -6837,    3180,   -3473,  1752,   -507,    64.",
				"1,  -34181,   -1431,  -17951, 10543,  -4563,  1147,  -128.",
				"1, -170900, -145310, -129950, 48442, -30524, 11470, -2555, 256."
			],
			"mathematica": [
				"e[n_, 0, m_]:= 1; (* Example for m=3 *)",
				"e[n_, k_, m_]:= 0 /; k \u003e= n;",
				"e[n_, k_, m_]:= (k+m)*e[n-1, k, m] + (n-k+1-m)*e[n-1, k-1, m];",
				"Table[Flatten[Table[Table[e[n, k, m], {k,0,n-1}], {n,1,10}]], {m,0,10}]",
				"T[n_, 1]:= 1; T[n_, n_]:= (-2)^(n-1); T[n_, k_]:= T[n, k] = (k+3)*T[n-1, k] + (n-k-2)*T[n-1, k-1]; Table[T[n, k], {n, 1, 12}, {k, 1, n}]//Flatten"
			],
			"program": [
				"(PARI) {T(n, k) = if(k==1, 1, if(k==n, (-2)^(n-1), (k+3)*T(n-1, k) + (n-k-2)* T(n-1, k-1)))};",
				"for(n=1, 12, for(k=1, n, print1(T(n, k), \", \")))",
				"(Sage)",
				"def T(n, k):",
				"    if (k==1): return 1",
				"    elif (k==n): return (-2)^(n-1)",
				"    else: return (k+3)*T(n-1, k) + (n-k-2)* T(n-1, k-1)",
				"[[T(n, k) for k in (1..n)] for n in (1..12)]"
			],
			"xref": [
				"Cf. A157011 (m=0), A008292 (m=1), A157012 (m=2), A157013 (m=3), this sequence."
			],
			"keyword": "sign,tabl",
			"offset": "1,3",
			"author": "_G. C. Greubel_, Feb 22 2019",
			"references": 1,
			"revision": 15,
			"time": "2019-04-19T11:02:57-04:00",
			"created": "2019-04-19T11:02:57-04:00"
		}
	]
}