{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A305161",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 305161,
			"data": "1,1,0,1,1,0,1,1,1,0,1,1,3,1,0,1,1,3,7,1,0,1,1,3,10,19,1,0,1,1,3,10,31,51,1,0,1,1,3,10,35,101,141,1,0,1,1,3,10,35,121,336,393,1,0,1,1,3,10,35,126,426,1128,1107,1,0,1,1,3,10,35,126,456,1520,3823,3139,1,0",
			"name": "Number A(n,k) of compositions of n into exactly n nonnegative parts \u003c= k; square array A(n,k), n\u003e=0, k\u003e=0, read by antidiagonals.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A305161/b305161.txt\"\u003eAntidiagonals n = 0..200\u003c/a\u003e"
			],
			"formula": [
				"A(n,k) = [x^n] ((x^(k+1)-1)/(x-1))^n.",
				"A(n,k) - A(n,k-1) = A180281(n,k) for n,k \u003e 0.",
				"A(n,k) = A(n,n) for all k \u003e= n."
			],
			"example": [
				"A(3,1) = 1: 111.",
				"A(3,2) = 7: 012, 021, 102, 111, 120, 201, 210.",
				"A(3,3) = 10: 003, 012, 021, 030, 102, 111, 120, 201, 210, 300.",
				"A(4,2) = 19: 0022, 0112, 0121, 0202, 0211, 0220, 1012, 1021, 1102, 1111, 1120, 1201, 1210, 2002, 2011, 2020, 2101, 2110, 2200.",
				"A(4,3) = 31: 0013, 0022, 0031, 0103, 0112, 0121, 0130, 0202, 0211, 0220, 0301, 0310, 1003, 1012, 1021, 1030, 1102, 1111, 1120, 1201, 1210, 1300, 2002, 2011, 2020, 2101, 2110, 2200, 3001, 3010, 3100.",
				"Square array A(n,k) begins:",
				"  1, 1,    1,    1,    1,    1,    1,    1,    1, ...",
				"  0, 1,    1,    1,    1,    1,    1,    1,    1, ...",
				"  0, 1,    3,    3,    3,    3,    3,    3,    3, ...",
				"  0, 1,    7,   10,   10,   10,   10,   10,   10, ...",
				"  0, 1,   19,   31,   35,   35,   35,   35,   35, ...",
				"  0, 1,   51,  101,  121,  126,  126,  126,  126, ...",
				"  0, 1,  141,  336,  426,  456,  462,  462,  462, ...",
				"  0, 1,  393, 1128, 1520, 1667, 1709, 1716, 1716, ...",
				"  0, 1, 1107, 3823, 5475, 6147, 6371, 6427, 6435, ..."
			],
			"maple": [
				"A:= (n, k)-\u003e coeff(series(((x^(k+1)-1)/(x-1))^n, x, n+1), x, n):",
				"seq(seq(A(n, d-n), n=0..d), d=0..12);",
				"# second Maple program:",
				"b:= proc(n, i, k) option remember; `if`(n=0, 1,",
				"      `if`(i=0, 0, add(b(n-j, i-1, k), j=0..min(n, k))))",
				"    end:",
				"A:= (n, k)-\u003e b(n$2, k):",
				"seq(seq(A(n, d-n), n=0..d), d=0..12);"
			],
			"mathematica": [
				"b[n_, i_, k_] := b[n, i, k] = If[n == 0, 1, If[i == 0, 0, Sum[b[n - j, i - 1, k], {j, 0, Min[n, k]}]]];",
				"A[n_, k_] := b[n, n, k];",
				"Table[A[n, d - n], {d, 0, 12}, {n, 0, d}] // Flatten (* _Jean-François Alcover_, May 05 2019, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Columns k=0-10 give: A000007, A000012, A002426, A005725, A187925, A318113, A318114, A318115, A318116, A167403, A318117.",
				"Rows n=0-1 give: A000012, A057427.",
				"Main diagonal gives A088218 or A001700(n-1) for n\u003e0.",
				"A(n+1,n) gives A048775.",
				"Cf. A180281."
			],
			"keyword": "nonn,tabl",
			"offset": "0,13",
			"author": "_Alois P. Heinz_, Aug 17 2018",
			"references": 15,
			"revision": 25,
			"time": "2019-05-05T16:05:58-04:00",
			"created": "2018-08-18T19:56:40-04:00"
		}
	]
}