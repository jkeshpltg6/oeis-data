{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A349629",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 349629,
			"data": "1,-3,-4,1,-6,2,-8,0,1,9,-12,-2,-14,12,8,0,-18,-1,-20,-3,32,18,-24,0,1,21,0,-4,-30,-12,-32,0,16,27,48,1,-38,30,56,0,-42,-16,-44,-6,-2,36,-48,0,1,-3,24,-7,-54,0,72,0,80,45,-60,4,-62,48,-8,0,84,-24,-68,-9,32,-72,-72,0,-74,57,-4,-10,96,-28",
			"name": "Numerators of the Dirichlet inverse of the abundancy index, sigma(n)/n.",
			"comment": [
				"Because the ratio A000203(n)/n [known as the abundancy index of n] is multiplicative, so is also its Dirichlet inverse. This sequence gives the numerator of that ratio when presented in its lowest terms, while A349630 gives the denominators. See the examples."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A349629/b349629.txt\"\u003eTable of n, a(n) for n = 1..20000\u003c/a\u003e"
			],
			"example": [
				"The ratio a(n)/A349630(n) for n = 1..15: 1/1, -3/2, -4/3, 1/2, -6/5, 2/1, -8/7, 0/1, 1/3, 9/5, -12/11, -2/3, -14/13, 12/7, 8/5."
			],
			"mathematica": [
				"f[1] = 1; f[n_] := f[n] = -DivisorSum[n, f[#] * DivisorSigma[1, n/#] * #/n \u0026, # \u003c n \u0026]; Numerator @ Array[f, 100] (* _Amiram Eldar_, Nov 28 2021 *)"
			],
			"program": [
				"(PARI)",
				"up_to = 16384;",
				"DirInverseCorrect(v) = { my(u=vector(#v)); u[1] = (1/v[1]); for(n=2, #v, u[n] = (-u[1]*sumdiv(n, d, if(d\u003cn, v[n/d]*u[d], 0)))); (u) }; \\\\ Compute the Dirichlet inverse of the sequence given in input vector v.",
				"Abi(n) = (sigma(n)/n);",
				"vDirInv_of_Abi = DirInverseCorrect(vector(up_to,n,Abi(n)));",
				"A349629(n) = numerator(vDirInv_of_Abi[n]);"
			],
			"xref": [
				"Cf. A000203, A017665, A017666.",
				"Cf. A349630 (denominators)."
			],
			"keyword": "sign,frac",
			"offset": "1,2",
			"author": "_Antti Karttunen_, Nov 27 2021",
			"references": 2,
			"revision": 14,
			"time": "2021-12-01T08:44:09-05:00",
			"created": "2021-11-27T11:15:41-05:00"
		}
	]
}