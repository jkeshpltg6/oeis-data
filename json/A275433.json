{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A275433",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 275433,
			"data": "1,1,2,2,2,4,4,4,12,8,20,4,8,44,12,16,68,44,16,132,100,8,32,196,252,32,32,356,500,136,64,516,1068,384,16,64,900,1956,1096,80,128,1284,3804,2592,384,128,2180,6612,6152,1280,32,256,3076,12108,13056,4080,192,256,5124,20292,27784,11056,1024,512,7172,35644,54816,28960,3904,64",
			"name": "Triangle read by rows: T(n,k) is the number of compositions of n having degree of asymmetry equal to k (n\u003e=0; 0\u003c=k\u003c=n/3).",
			"comment": [
				"The degree of asymmetry of a finite sequence of numbers is defined to be the number of pairs of symmetrically positioned distinct entries. Example: the degree of asymmetry of (2,7,6,4,5,7,3) is 2, counting the pairs (2,3) and (6,5).",
				"A sequence is palindromic if and only if its degree of asymmetry is 0.",
				"Sum(k*T(n,k), k\u003e=0) = A275434(n)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A275433/b275433.txt\"\u003eRows n = 0..250, flattened\u003c/a\u003e",
				"V. E. Hoggatt, Jr., and Marjorie Bicknell, \u003ca href=\"http://www.fq.math.ca/Scanned/13-4/hoggatt1.pdf\"\u003ePalindromic compositions\u003c/a\u003e, Fibonacci Quart., Vol. 13(4), 1975, pp. 350-356."
			],
			"formula": [
				"G.f.: G(t,z) = (1-z^2)/((1-z)*(1-2*z^2) - 2tz^3). In the more general situation of compositions into a[1]\u003ca[2]\u003ca[3]\u003c..., denoting F(z) = Sum(z^{a[j]},j\u003e=1}, we have G(t,z) =(1 + F(z))/(1 - F(z^2) - t(F(z)^2 - F(z^2))). In particular, for t=0 we obtain Theorem 1.2 of the Hoggatt et al. reference."
			],
			"example": [
				"T(4,0) = 4 because we have 4, 22, 121, and 1111.",
				"T(4,1) = 4 because we have 13, 31, 112, and 211.",
				"Triangle starts:",
				"1;",
				"1;",
				"2;",
				"2,2;",
				"4,4;",
				"4,12;",
				"8,20,4."
			],
			"maple": [
				"G := (1-z^2)/((1-z)*(1-2*z^2)-2*t*z^3): Gser := simplify(series(G, z = 0, 24)): for n from 0 to 20 do P[n] := sort(coeff(Gser, z, n)) end do: for n from 0 to 20 do seq(coeff(P[n], t, j), j = 0 .. degree(P[n])) end do; # yields sequence in triangular form",
				"# second Maple program:",
				"b:= proc(n, i) option remember; expand(`if`(n=0, 1, add(b(n-j,",
				"      `if`(i=0, j, 0))*`if`(i\u003e0 and i\u003c\u003ej, x, 1), j=1..n)))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=0..degree(p)))(b(n, 0)):",
				"seq(T(n), n=0..20);  # _Alois P. Heinz_, Jul 29 2016"
			],
			"mathematica": [
				"b[n_, i_] := b[n, i] = Expand[If[n == 0, 1, Sum[b[n - j, If[i == 0, j, 0]]*If[i \u003e 0 \u0026\u0026 i != j, x, 1], {j, 1, n}]]]; T[n_] := Function[p, Table[Coefficient[p, x, i], {i, 0, Exponent[p, x]}]][b[n, 0]]; Table[T[n], {n, 0, 20}] // Flatten (* _Jean-François Alcover_, Dec 22 2016, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A275434.",
				"Row sums give A011782.",
				"Column k=0 gives A016116."
			],
			"keyword": "nonn,tabf",
			"offset": "0,3",
			"author": "_Emeric Deutsch_, Jul 29 2016",
			"references": 2,
			"revision": 20,
			"time": "2016-12-22T17:50:29-05:00",
			"created": "2016-07-29T19:20:01-04:00"
		}
	]
}