{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A096713",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 96713,
			"data": "1,1,-1,1,-3,1,3,-6,1,15,-10,1,-15,45,-15,1,-105,105,-21,1,105,-420,210,-28,1,945,-1260,378,-36,1,-945,4725,-3150,630,-45,1,-10395,17325,-6930,990,-55,1,10395,-62370,51975,-13860,1485,-66,1,135135,-270270,135135,-25740,2145,-78,1",
			"name": "Irregular triangle T(n,k) of nonzero coefficients of the modified Hermite polynomials (n \u003e= 0 and 0 \u003c= k \u003c= floor(n/2)).",
			"comment": [
				"Triangle of nonzero coefficients of matching polynomial of complete graph of order n.",
				"Row sums of absolute values produce A000085 (number of involutions). - _Wouter Meeussen_, Mar 12 2008",
				"Row n has floor(n/2) + 1 nonzero coefficients. - _Robert Israel_, Dec 23 2015",
				"Also the nonzero terms of the Bell matrix generated by the sequence [-1,1,0,0,0, ...] read by rows (see second Sage program). For the definition of the Bell matrix see A264428. - _Peter Luschny_, Jan 20 2016",
				"From _Petros Hadjicostas_, Oct 28 2019: (Start)",
				"The formulas about the p.d.f. of the standard normal distribution were proved, for example, by Charlier (1905, pp. 13-15), but they were well-known for many years before him. Charlier (1905) has generalized these results to other measures whose n-th moment (around 0) exists for each integer n \u003e= 0.",
				"Different forms (with or without signs) of these coefficients T(n,k) appear in other arrays as well; e.g., see A049403, A104556, A122848, A130757 (odd rows only), etc.",
				"(End)"
			],
			"reference": [
				"C. D. Godsil, Algebraic Combinatorics, Chapman \u0026 Hall, New York, 1993."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A096713/b096713.txt\"\u003eTable of n, a(n) for n = 0..10099\u003c/a\u003e (rows 0 to 199, flattened)",
				"Carl V. L. Charlier, \u003ca href=\"https://babel.hathitrust.org/cgi/pt?id=chi.73230316\u0026amp;view=1up\u0026amp;seq=103\"\u003eÜber die Darstellung willkürlicher Funktionen\u003c/a\u003e, Arkiv För Matematik, Astronomi Och Fysik, Band 2, No. 20 (Meddelande från Lunds Astronomiska Observatorium, Series I, No. 27), 1905, 1-35. [Accessible only in the USA via the \u003ca href=\"https://www.hathitrust.org/accessibility\"\u003eHathiTrust Digital Library\u003c/a\u003e.]",
				"Tom Halverson and Theodore N. Jacobson, \u003ca href=\"https://arxiv.org/abs/1808.08118\"\u003eSet-partition tableaux and representations of diagram algebras\u003c/a\u003e, arXiv:1808.08118 [math.RT], 2018.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/HermitePolynomial.html\"\u003eHermite Polynomial\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/MatchingPolynomial.html\"\u003eMatching Polynomial\u003c/a\u003e. - _Eric W. Weisstein_, Sep 27 2008"
			],
			"formula": [
				"G.f.: HermiteH(n,x/sqrt(2))/2^(n/2). - _Wouter Meeussen_, Mar 12 2008",
				"From _Robert Israel_, Dec 23 2015: (Start)",
				"T(2*m, k) = (-1)^(m+k)*(2*m)!*2^(k-m)/((m-k)!*(2*k)!), k = 0..m.",
				"T(2*m+1, k) = (-1)^(m+k)*(2*m+1)!*2^(k-m)/((m-k)!*(2*k+1)!), k = 0..m. (End)",
				"From _Petros Hadjicostas_, Oct 28 2019: (Start)",
				"Let He_n(x) be the n-th modified Hermite polynomial (see the references above); i.e., let He_n(x) = Sum_{k = 0..m} T(2*m, k)*x^(2*k) when n = 2*m and He_n(x) = Sum_{k = 0..m} T(2*m+1, k)*x^(2*k+1) when n = 2*m+1.",
				"Let phi(x) = (1/sqrt(2*Pi)) * exp(-x^2/2) be the p.d.f. of a standard normal distribution. Then He_n(x) = (-1)^n * (1/phi(x)) * d^n(phi(x))/dx^n for n \u003e= 0.",
				"We have He_n(x) = x*He_{n-1}(x) - (n-1)*He_{n-2}(x) for n \u003e= 2. (End)"
			],
			"example": [
				"Triangle T(n,k) (with rows n \u003e= 0 and columns k \u003e= 0) begins as follows:",
				"    1;",
				"    1;",
				"   -1,     1;",
				"   -3,     1;",
				"    3,    -6,    1;",
				"   15,    -10,   1;",
				"  -15,     45, -15,   1;",
				"  -105,   105, -21,   1;",
				"   105,  -420, 210, -28, 1;",
				"   945, -1260, 378, -36, 1;",
				"   ...",
				"The corresponding modified Hermite polynomials are as follows",
				"He_0(x) = 1, He_1(x) = x,",
				"He_2(x) = -1 + x^2, He_3(x) = -3*x + x^3,",
				"He_4(x) = 3 - 6*x^2 + x^4, He_5(x) = 15*x - 10*x^3 + x^5, ...",
				"[Modified by _Petros Hadjicostas_, Oct 28 2019]"
			],
			"maple": [
				"A:= NULL:",
				"for n from 0 to 20 do",
				"  HH:= expand(orthopoly[H](n,x/sqrt(2))/2^(n/2));",
				"  C:= subs(0=NULL, [seq(coeff(HH,x,j),j=0..n)]);",
				"  A:= A, op(C);",
				"od:",
				"A; #  _Robert Israel_, Dec 23 2015",
				"# Alternatively:",
				"A096713 := (n, k) -\u003e `if`(2*k\u003cn, NULL, (-1/2)^(n-k)*n!/((2*k-n)!*(n-k)!)):",
				"seq(seq(A096713(n, k), k=0..n), n=0..13); # _Peter Luschny_, Dec 24 2015"
			],
			"mathematica": [
				"Table[CoefficientList[HermiteH[n,x/Sqrt[2] ]/2^(n/2),x],{n,0,25}] (* _Wouter Meeussen_, Mar 12 2008 *)"
			],
			"program": [
				"(PARI) T(n,k)=if(k\u003c0||2*k\u003en, 0, (-1)^(n\\2-k)*n!/(n\\2-k)!/(n%2+2*k)!/2^(n\\2-k)) /* _Michael Somos_, Jun 04 2005 */",
				"(Sage)",
				"from sage.functions.hypergeometric import closed_form",
				"def A096713_row(n):",
				"    R.\u003cz\u003e = ZZ[]",
				"    h = hypergeometric([-n/2,(1-n)/2], [], -2*z)",
				"    T = R(closed_form(h)).coefficients()",
				"    return T[::-1]",
				"for n in range(13): A096713_row(n) # _Peter Luschny_, Aug 21 2014",
				"(Sage) # uses[bell_transform from A264428]",
				"def bell_zero_filter(generator, dim):",
				"    G = [generator(k) for k in srange(dim)]",
				"    row = lambda n: bell_transform(n, G)",
				"    F = [filter(lambda r: r != 0, R) for R in [row(n) for n in srange(dim)]]",
				"    return [i for f in F for i in f]",
				"print(bell_zero_filter(lambda n: [1,-1][n] if n \u003c 2 else 0, 14)) # _Peter Luschny_, Jan 20 2016",
				"(Python)",
				"from sympy import hermite, Poly, sqrt",
				"def a(n): return Poly(hermite(n, x/sqrt(2))/2**(n/2), x).coeffs()[::-1]",
				"for n in range(21): print(a(n)) # _Indranil Ghosh_, May 26 2017"
			],
			"xref": [
				"Cf. A000085, A049403, A104556, A122848, A130757, A264428."
			],
			"keyword": "sign,tabf",
			"offset": "0,5",
			"author": "_Eric W. Weisstein_, Jul 04 2004",
			"references": 12,
			"revision": 72,
			"time": "2020-04-17T02:53:53-04:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}