{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A087711",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 87711,
			"data": "2,4,5,8,7,8,11,10,11,14,13,18,17,16,17,22,21,20,23,22,23,26,25,30,29,28,33,32,31,32,37,36,35,38,37,38,43,42,41,44,43,48,47,46,57,52,51,50,53,52,53,56,55,56,59,58,75,70,69,72,67,66,65,68,67,72,71,70,71,80,81,78",
			"name": "a(n) = smallest number k such that both k-n and k+n are primes.",
			"comment": [
				"Let b(n), c(n) and d(n) be respectively, smallest number m such that phi(m-n) + sigma(m+n) = 2n, smallest number m such that phi(m+n) + sigma(m-n) = 2n and smallest number m such that phi(m-n) + sigma(m+n) = phi(m+n) + sigma(m-n), we conjecture that for each positive integer n, a(n)=b(n)=c(n)=d(n). Namely we conjecture that for each positive integer n, a(n) \u003c A244446(n), a(n) \u003c A244447(n) and a(n) \u003c A244448(n). - _Jahangeer Kholdi_ and _Farideh Firoozbakht_, Sep 05 2014"
			],
			"link": [
				"Zak Seidov, \u003ca href=\"/A087711/b087711.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A020483(n)+n for n \u003e= 1. - _Robert Israel_, Sep 08 2014"
			],
			"example": [
				"n=10: k=13 because 13-10 and 13+10 are both prime and 13 is the smallest k such that k +/- 10 are both prime",
				"4-1=3, prime, 4+1=5, prime; 5-2=3, 5+2=7; 8-3=5, 8+3=11; 9-4=5, 9+4=13, ..."
			],
			"maple": [
				"Primes:= select(isprime,{seq(2*i+1,i=1..10^3)}):",
				"a[0]:= 2:",
				"for n from 1 do",
				"  Q:= Primes intersect map(t -\u003e t-2*n,Primes);",
				"  if nops(Q) = 0 then break fi;",
				"  a[n]:= min(Q) + n;",
				"od:",
				"seq(a[i],i=0..n-1); # _Robert Israel_, Sep 08 2014"
			],
			"mathematica": [
				"s = \"\"; k = 0; For[i = 3, i \u003c 22^2, If[PrimeQ[i - k] \u0026\u0026 PrimeQ[i + k], s = s \u003c\u003e ToString[i] \u003c\u003e \",\"; k++ ]; i++ ]; Print[s] (* _Vladimir Joseph Stephan Orlovsky_, Apr 03 2008 *)",
				"snk[n_]:=Module[{k=n+1},While[!PrimeQ[k+n]||!PrimeQ[k-n],k++];k]; Array[ snk,80,0] (* _Harvey P. Dale_, Dec 13 2020 *)"
			],
			"program": [
				"(MAGMA) distance:=function(n); k:=n+2; while not IsPrime(k-n) or not IsPrime(k+n) do k:=k+1; end while; return k; end function; [ distance(n): n in [1..71] ]; /* _Klaus Brockhaus_, Apr 08 2007 */",
				"(PARI) a(n)=my(k);while(!isprime(k-n) || !isprime(k+n),k++);return(k) \\\\ _Edward Jiang_, Sep 05 2014"
			],
			"xref": [
				"Cf. A087695, A087696, A087697, A087678, A087679, A087680, A087681, A087682, A087683.",
				"Cf. A082467. See A137169 for another version.",
				"Cf. A244446, A244447, A244448.",
				"Cf. A020483."
			],
			"keyword": "easy,nonn",
			"offset": "0,1",
			"author": "_Zak Seidov_, Sep 28 2003",
			"ext": [
				"Entries checked by _Klaus Brockhaus_, Apr 08 2007"
			],
			"references": 10,
			"revision": 34,
			"time": "2020-12-13T12:50:11-05:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}