{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A347981",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 347981,
			"data": "1,1,1,2,1,1,1,2,1,2,2,1,1,1,1,3,2,2,2,1,1,2,2,2,2,3,1,1,1,1,1,2,1,3,2,1,2,2,4,2,2,2,1,1,1,2,3,2,2,4,1,2,2,2,1,3,2,1,1,1,1,1,1,4,2,2,3,1,1,3,2,2,2,4,1,1,2,2,2,1,4,2,2,2,2,3,2,2,2,1,1,1,1,2,3,3,3",
			"name": "Irregular triangle T(n, k) read by rows in which row n lists the number of parts in the symmetric representation of sigma for n = 2^m * q, 2^(m-1) * q, ... , q, with m \u003e= 0, q odd, 1 \u003c= k \u003c= m + 1.",
			"comment": [
				"The length of row n = 2^m * q, m\u003e=0, q odd, in the triangle is m+1, i.e., the exponent of the even part of n plus 1. The rightmost number in row n gives the number of regions in the symmetric representation of sigma, for short \"#rsrs\", of the odd part q of n, its index in this sequence is A005187(n).",
				"The numbers in each row of the triangle are nondecreasing.",
				"The lengths of at least the first 21 rows are given by A001511. - _Omar E. Pol_, Sep 22 2021",
				"It appears that row lengths give A001511, the columns are A237271, and row 2^i, i \u003e= 0, lists 1 + i ones. _Omar E. Pol_, Oct 01 2021"
			],
			"formula": [
				"T(n, k) = A237271(n/2^(k-1)) for n=2^m*q, m\u003e=0, q odd, 1\u003c=k\u003c=m+1, and",
				"T(n, m+1) = a(A005187(n)) = a(A011371(n) + n), n \u003e= 1."
			],
			"example": [
				"Row 15, a(26) = (3), consists of a single number since 15 is odd.",
				"Row 48, a(90..94) = (1, 1, 1, 1, 2), is the sequence of #rsrs for the numbers 48, 24, 12, 6, 3.",
				"Row 228, a(450..452) = (1, 2, 4), is the sequence of #rsrs for the numbers 228, 114, 57.",
				"First 21 rows of the triangle; columns indicate division of n by powers of 2:",
				"  n       1  2  4  8  16",
				"-------------------------------------",
				"  1:      1;",
				"  2:      1, 1;",
				"  3:      2;",
				"  4:      1, 1, 1;",
				"  5:      2;",
				"  6:      1, 2;",
				"  7:      2;",
				"  8:      1, 1, 1, 1;",
				"  9:      3;",
				"  10:     2, 2;",
				"  11:     2;",
				"  12:     1, 1, 2;",
				"  13:     2;",
				"  14:     2, 2;",
				"  15:     3;",
				"  16:     1, 1, 1, 1, 1;",
				"  17:     2;",
				"  18:     1, 3;",
				"  19:     2;",
				"  20:     1, 2, 2;",
				"  21:     4;"
			],
			"mathematica": [
				"(* function a237270[ ] is defined in A237270 *)",
				"a237271[n_] := Length[a237270[n]]",
				"a347981[n_] := Last[Transpose[NestWhileList[{First[#]/2, a237271[First[#]/2]}\u0026, {n, a237271[n]}, IntegerExponent[First[#], 2]\u003e0\u0026]]]",
				"Flatten[Map[a347981, Range[50]]]"
			],
			"xref": [
				"Column 1 gives A237271.",
				"Cf. A001511, A005187, A011371, A235791, A237270, A237591, A237593, A279387."
			],
			"keyword": "nonn,tabf",
			"offset": "1,4",
			"author": "_Hartmut F. W. Hoft_, Sep 22 2021",
			"references": 1,
			"revision": 21,
			"time": "2021-10-01T21:54:46-04:00",
			"created": "2021-09-23T12:04:47-04:00"
		}
	]
}