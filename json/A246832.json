{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A246832",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 246832,
			"data": "1,1,3,4,2,5,2,3,7,5,5,6,5,3,10,6,3,7,7,4,11,9,3,14,8,8,5,4,7,10,13,7,8,10,7,15,8,4,17,10,8,11,10,5,16,11,4,11,15,8,14,10,7,22,8,9,14,8,13,21,12,5,13,15,6,21,15,9,13,8,11,9,12,15,20,21",
			"name": "Expansion of psi(x) * psi(x^2) * phi(x^2) in powers of x where phi(), psi() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A246832/b246832.txt\"\u003eTable of n, a(n) for n = 0..2500\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of psi(x) * psi(x^2)^3 / psi(x^4) in powers of x where psi() is a Ramanujan theta function.",
				"Expansion of q^(-3/8) * eta(q^4)^7 / (eta(q) * eta(q^2) * eta(q^8)^2) in powers of q.",
				"Euler transform of period 8 sequence [1, 2, 1, -5, 1, 2, 1, -3, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (64 t)) = 2 (t/i)^(3/2) g(t) where q = exp(2 Pi i t) and g() is the g.f. for A246816."
			],
			"example": [
				"G.f. = 1 + x + 3*x^2 + 4*x^3 + 2*x^4 + 5*x^5 + 2*x^6 + 3*x^7 + 7*x^8 + ...",
				"G.f. = q^3 + q^11 + 3*q^19 + 4*q^27 + 2*q^35 + 5*q^43 + 2*q^51 + 3*q^59 + ..."
			],
			"mathematica": [
				"eta[q_] := q^(1/24)*QPochhammer[q]; a:= CoefficientList[Series[q^(-3/8)* eta[q^4]^7/(eta[q]*eta[q^2]*eta[q^8]^2), {q, 0, 60}], q]]; Table[a[[n]], {n, 1, 50}] (* _G. C. Greubel_, Aug 05 2018 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^4 + A)^7 / (eta(x + A) * eta(x^2 + A) * eta(x^8 + A)^2), n))};"
			],
			"xref": [
				"Cf. A246816."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Michael Somos_, Sep 04 2014",
			"references": 3,
			"revision": 9,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2014-09-04T13:37:21-04:00"
		}
	]
}