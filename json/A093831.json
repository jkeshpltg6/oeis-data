{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A093831",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 93831,
			"data": "1,4,10,24,51,104,206,384,697,1228,2112,3568,5898,9592,15358,24256,37850,58340,88980,134344,200972,298112,438538,640256,928041,1336104,1911436,2717776,3842110,5401784,7555012,10514176,14562432,20077672",
			"name": "Expansion of q * (chi(-q) * chi(-q^5))^-4 in powers of q where chi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A093831/b093831.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of (eta(q^2) * eta(q^10) / (eta(q) * eta(q^5)))^4 in powers of q.",
				"Euler transform of period 10 sequence [ 4, 0, 4, 0, 8, 0, 4, 0, 4, 0, ...].",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2)) where f(u, v) = u^2 - v*(1 + 8*u + 16*u*v).",
				"G.f.: x * (Product_{k\u003e0} (1 - x^(10*k - 5)) * (1 - x^(2*k - 1)))^-4.",
				"Convolution inverse of A132040. - _Michael Somos_, Apr 26 2015",
				"a(n) ~ exp(2*Pi*sqrt(2*n/5)) / (16 * 2^(3/4) * 5^(1/4) * n^(3/4)). - _Vaclav Kotesovec_, Sep 07 2015"
			],
			"example": [
				"G.f. = q + 4*q^2 + 10*q^3 + 24*q^4 + 51*q^5 + 104*q^6 + 206*q^7 + 384*q^8 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ q (QPochhammer[ -q, q] QPochhammer[ -q^5, q^5] )^4, {q, 0, n}]; (* _Michael Somos_, Apr 26 2015 *)",
				"nmax = 40; Rest[CoefficientList[Series[x * Product[1/((1 - x^(10*k - 5)) * (1 - x^(2*k - 1)))^4, {k, 1, nmax}], {x, 0, nmax}], x]] (* _Vaclav Kotesovec_, Sep 07 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, 0, n--; polcoeff( (1 / prod(k=1, (n+5)\\10, 1 - x^(10*k - 5), 1 + x * O(x^n)) / prod(k=1, (n+1)\\2, 1 - x^(2*k - 1), 1 + x * O(x^n)))^4, n))};",
				"(PARI) {a(n) = my(A); if( n\u003c1, 0, n--; A = x * O(x^n); polcoeff( (eta(x^2 + A) * eta(x^10 + A) / (eta(x + A) * eta(x^5 + A)))^4, n))};"
			],
			"xref": [
				"Cf. A132040."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Michael Somos_, Apr 17 2004, Oct 04 2004",
			"ext": [
				"Edited by _N. J. A. Sloane_ at the suggestion of _Andrew S. Plewe_, Jun 05 2007"
			],
			"references": 3,
			"revision": 19,
			"time": "2021-03-12T22:24:42-05:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}