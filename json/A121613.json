{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A121613",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 121613,
			"data": "1,-4,6,-8,13,-12,14,-24,18,-20,32,-24,31,-40,30,-32,48,-48,38,-56,42,-44,78,-48,57,-72,54,-72,80,-60,62,-104,84,-68,96,-72,74,-124,96,-80,121,-84,108,-120,90,-112,128,-120,98,-156,102,-104,192,-108,110",
			"name": "Expansion of psi(-x)^4 in powers of x where psi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Number 33 of the 74 eta-quotients listed in Table I of Martin (1996)."
			],
			"reference": [
				"J. W. L. Glaisher, Notes on Certain Formulae in Jacobi's Fundamenta Nova, Messenger of Mathematics, 5 (1876), pp. 174-179. see p.179",
				"Hardy, et al., Collected Papers of Srinivasa Ramanujan, p. 326, Question 359."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A121613/b121613.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Y. Martin, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9947-96-01743-6\"\u003eMultiplicative eta-quotients\u003c/a\u003e, Trans. Amer. Math. Soc. 348 (1996), no. 12, 4825-4856, see page 4852 Table I.",
				"Michael Somos, \u003ca href=\"/A030203/a030203.txt\"\u003eIndex to Yves Martin's list of 74 multiplicative eta-quotients and their A-numbers\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(-1/2) * (eta(q) * eta(q^4) / eta(q^2))^4 in powers of q.",
				"Expansion of q^(-1/2)/4 * k * k' * (K / (Pi/2))^2 in powers of q where k, k', K are Jacobi elliptic functions. - _Michael Somos_, Jun 22 2012",
				"Euler transform of period 4 sequence [ -4, 0, -4, -4, ...].",
				"a(n) = b(2*n + 1) where b(n) is multiplicative with b(2^e) = 0^n, b(p^e) = (p^(e+1) - 1) / (p - 1) if p == 1 (mod 4), b(p^e) = (-1)^e * (p^(e+1) - 1) / (p - 1) if p == 3 (mod 4).",
				"Given g.f. A(x), then B(x) = 4 * Integral_{0..x} A(x^2) dx = arcsin(4 * x * A001938(x^2)) satisfies 0 = f(B(x), B(x^3)) where f(u, v) = sin(u + v) / 2 - sin((u - v) / 2). - _Michael Somos_, Oct 14 2013",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (16 t)) = (t/i)^2 f(t) where q = exp(2 Pi i t). - _Michael Somos_, Jun 27 2013",
				"G.f.: (Product_{k\u003e0} (1 - x^k) / (1 - x^(4*k - 2)))^4.",
				"G.f.: Sum_{k\u003e0} -(-1)^k * (2*k - 1) * x^(k - 1) / (1 + x^(2*k - 1)).",
				"G.f.: (Product_{k\u003e0} (1 - x^(2*k - 1)) * (1 - x^(4*k)))^4.",
				"G.f.: (Sum_{k\u003e0} (-1)^floor(k/2) * x^((k^2 - k)/2))^4.",
				"G.f.: Sum_{k\u003e0} (-1)^k * (2*k - 1) * x^(2*k - 1) / (1 + x^(4*k - 2)).",
				"a(n) = (-1)^n * A008438(n). a(2*n) = A112610(n). a(2*n + 1) = -4 * A097723(n).",
				"Convolution square of A134343. - _Michael Somos_, Jun 20 2012",
				"a(3*n + 2) = 6 * A258831(n). a(4*n + 3) = -8 * A258835(n). - _Michael Somos_, Jun 11 2015"
			],
			"example": [
				"G.f. = 1 - 4*x + 6*x^2 - 8*x^3 + 13*x^4 - 12*x^5 + 14*x^6 - 24*x^7 + ...",
				"G.f. = q - 4*q^3 + 6*q^5 - 8*q^7 + 13*q^9 - 12*q^11 + 14*q^13 - 24*q^15 + ..."
			],
			"mathematica": [
				"a[ n_] := With[ {m = InverseEllipticNomeQ @ q}, SeriesCoefficient[ Sqrt[(1 - m) m ] (EllipticK[m] 2/Pi)^2 / (4 q^(1/2)), {q, 0, n}]]; (* _Michael Somos_, Jun 22 2012 *)",
				"a[ n_] := SeriesCoefficient[ (QPochhammer[ q] QPochhammer[ q^4] / QPochhammer[ q^2])^4, {q, 0, n}]; (* _Michael Somos_, Oct 14 2013 *)",
				"a[ n_] := If[ n \u003c 0, 0, (-1)^n DivisorSigma[1, 2 n + 1]]; (* _Michael Somos_, Jun 15 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, 0, (-1)^n * sigma(2*n + 1))};",
				"(Sage) A = ModularForms( Gamma0(16), 2, prec=110).basis(); A[1] - 4*A[3]; # _Michael Somos_, Jun 27 2013",
				"(MAGMA) A := Basis( ModularForms( Gamma0(16), 2), 110); A[2] - 4*A[4]; /* _Michael Somos_, Jun 10 2015 */"
			],
			"xref": [
				"Cf. A001938, A008438, A097723, A112610, A134343, A258831, A258835."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Aug 10 2006",
			"references": 6,
			"revision": 42,
			"time": "2021-03-12T22:24:44-05:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}