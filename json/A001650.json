{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001650",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1650,
			"data": "1,3,3,3,5,5,5,5,5,7,7,7,7,7,7,7,9,9,9,9,9,9,9,9,9,11,11,11,11,11,11,11,11,11,11,11,13,13,13,13,13,13,13,13,13,13,13,13,13,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,17,17,17,17,17,17,17,17,17,17,17,17,17",
			"name": "n appears n times (n odd).",
			"comment": [
				"For n \u003e= 0, a(n+1) is the number of integers x with |x| \u003c= sqrt(n), or equivalently the number of point in the Z^1 lattice of norm \u003c= n+1. - _David W. Wilson_, Oct 22 2006",
				"The burning number of a connected graph of order n is at most a(n). See Bessy et al. - _Michel Marcus_, Jun 18 2018"
			],
			"reference": [
				"J. H. Conway and N. J. A. Sloane, \"Sphere Packings, Lattices and Groups\", Springer-Verlag, p. 106."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A001650/b001650.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Stéphane Bessy, Anthony Bonato, Jeannette Janssen and Dieter Rautenbach, \u003ca href=\"https://arxiv.org/abs/1511.06023\"\u003eBounds on the Burning Number\u003c/a\u003e, arXiv:1511.06023 [math.CO], 2015-2016.",
				"Abraham Isgur, Vitaly Kuznetsov, and Stephen Tanny, \u003ca href=\"https://arxiv.org/abs/1202.0276\"\u003eA combinatorial approach for solving certain nested recursions with non-slow solutions\u003c/a\u003e, arXiv preprint arXiv:1202.0276 [math.CO], 2012."
			],
			"formula": [
				"a(n) = 1 + 2*floor(sqrt(n-1)), n \u003e 0. - Antonio Esposito (antonio.b.esposito(AT)italtel.it), Jan 21 2002",
				"G.f.: theta_3(x)*x/(1-x). a(n+1)=a(n)+A000122(n). - _Michael Somos_, Apr 29 2003.",
				"a(1) = 1, a(2) = 3, a(3) = 3, a(n) = a(n-a(n-2))+2. - _Branko Curgus_, May 07 2010",
				"a(n) = 2*ceiling(sqrt(n))-1. - _Branko Curgus_, May 07 2010",
				"Seen as a triangle read by rows: T(n,k) = 2*(n-1), k=1..n. - _Reinhard Zumkeller_, Nov 14 2015"
			],
			"mathematica": [
				"a[1]=1,a[2]=3,a[3]=3,a[n_]:=a[n]=a[n-a[n-2]]+2 (* _Branko Curgus_, May 07 2010 *)",
				"Flatten[Table[Table[n,{n}],{n,1,17,2}]] (* _Harvey P. Dale_, Mar 31 2013 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c1,0,1+2*sqrtint(n-1))",
				"(Haskell)",
				"a001650 n k = a001650_tabf !! (n-1) !! (k-1)",
				"a001650_row n = a001650_tabf !! (n-1)",
				"a001650_tabf = iterate (\\xs@(x:_) -\u003e map (+ 2) (x:x:xs)) [1]",
				"a001650_list = concat a001650_tabf",
				"-- _Reinhard Zumkeller_, Nov 14 2015"
			],
			"xref": [
				"Cf. A001670. Partial sums of A000122.",
				"Cf. A111650, A131507, A193832."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_.",
			"ext": [
				"More terms from _Michael Somos_, Apr 29 2003"
			],
			"references": 18,
			"revision": 28,
			"time": "2020-01-28T12:09:56-05:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}