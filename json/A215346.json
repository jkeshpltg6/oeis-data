{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A215346",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 215346,
			"data": "1,-4,8,-16,34,-64,112,-192,319,-512,808,-1248,1886,-2816,4144,-6016,8643,-12288,17296,-24144,33442,-45952,62720,-85056,114620,-153600,204728,-271456,358204,-470528,615344,-801408,1039621,-1343488,1729920,-2219808,2838920",
			"name": "Expansion of (1/q) * phi(-q) * phi(q^4) / (phi(q) * psi(q^8)) in powers of q where phi(), psi() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"A058516, A176143, A214035, A215346 are all essentially the same sequence. - _N. J. A. Sloane_, Aug 08 2012"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A215346/b215346.txt\"\u003eTable of n, a(n) for n = -1..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of ( eta(q)^2 * eta(q^8)^3 / (eta(q^2)^3 * eta(q^16)^2))^2 in powers of q.",
				"Euler transform of period 16 sequence [ -4, 2, -4, 2, -4, 2, -4, -4, -4, 2, -4, 2, -4, 2, -4, 0, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (16 t)) = 4 * g(t) where q = exp(2 Pi i t) and g() is g.f. for A215348.",
				"a(n) =  (-1)^n * A214035(n). a(2*n) = -4 * A131126(n). Convolution inverse of A215348."
			],
			"example": [
				"1/q - 4 + 8*q - 16*q^2 + 34*q^3 - 64*q^4 + 112*q^5 - 192*q^6 + 319*q^7 + ..."
			],
			"mathematica": [
				"QP = QPochhammer; s = (QP[q]^2*(QP[q^8]^3/(QP[q^2]^3*QP[q^16]^2)))^2 + O[q]^40; CoefficientList[s, q] (* _Jean-François Alcover_, Nov 27 2015, adapted from PARI *)"
			],
			"program": [
				"(PARI) {a(n) = local(A); if( n\u003c-1, 0, n++; A = x * O(x^n); polcoeff( ( eta(x + A)^2 * eta(x^8 + A)^3 / (eta(x^2 + A)^3 * eta(x^16 + A)^2))^2, n))}"
			],
			"xref": [
				"Cf. A000122, A000700, A010054, A121373, A131126, A214035, A215348.",
				"Cf. A058516, A176143, A214035, A215346."
			],
			"keyword": "sign",
			"offset": "-1,2",
			"author": "_Michael Somos_, Aug 08 2012",
			"references": 6,
			"revision": 21,
			"time": "2021-03-12T22:24:46-05:00",
			"created": "2012-08-08T13:54:04-04:00"
		}
	]
}