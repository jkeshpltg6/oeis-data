{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A129235",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 129235,
			"data": "1,4,6,11,10,20,14,26,23,32,22,50,26,44,44,57,34,72,38,78,60,68,46,112,59,80,76,106,58,136,62,120,92,104,92,173,74,116,108,172,82,184,86,162,150,140,94,238,111,180,140,190,106,232,140,232,156,176,118,324,122,188",
			"name": "a(n) = 2*sigma(n) - tau(n), where tau(n) is the number of divisors of n (A000005) and sigma(n) is the sum of divisors of n (A000203).",
			"comment": [
				"Row sums of A129234. - _Emeric Deutsch_, Apr 17 2007",
				"Equals row sums of A130307. - _Gary W. Adamson_, May 20 2007",
				"Equals row sums of triangle A143315. - _Gary W. Adamson_, Aug 06 2008",
				"Equals A051731 * (1, 3, 5, 7, ...); i.e., the inverse Mobius transform of the odd numbers. Example: a(4) = 11 = (1, 1, 0, 1) * (1, 3, 5, 7) = (1 + 3 + 0 + 7), where (1, 1, 0, 1) = row 4 of A051731. - _Gary W. Adamson_, Aug 17 2008",
				"Equals row sums of triangle A143594. - _Gary W. Adamson_, Aug 26 2008"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A129235/b129235.txt\"\u003eTable of n, a(n) for n = 1..5000\u003c/a\u003e"
			],
			"formula": [
				"G.f.: Sum_{k\u003e=1} z^k*(k-(k-1)*z^k)/(1-z^k)^2. - _Emeric Deutsch_, Apr 17 2007",
				"G.f.: Sum_{n\u003e=1} x^n*(1+x^n)/(1-x^n)^2. - _Joerg Arndt_, May 25 2011",
				"L.g.f.: -log(Product_{k\u003e=1} (1 - x^k)^(2-1/k)) = Sum_{n\u003e=1} a(n)*x^n/n. - _Ilya Gutkovskiy_, Mar 18 2018",
				"a(n) = A222548(n) - A222548(n-1). - _Ridouane Oudra_, Jul 11 2020"
			],
			"example": [
				"a(4) = 2*sigma(4) - tau(4) = 2*7 - 3 = 11."
			],
			"maple": [
				"with(numtheory): seq(2*sigma(n)-tau(n),n=1..75); # _Emeric Deutsch_, Apr 17 2007",
				"G:=sum(z^k*(k-(k-1)*z^k)/(1-z^k)^2,k=1..100): Gser:=series(G,z=0,80): seq(coeff(Gser,z,n),n=1..75); # _Emeric Deutsch_, Apr 17 2007"
			],
			"mathematica": [
				"a[n_] := DivisorSum[2n, If[EvenQ[#], #-1, 0]\u0026]; Array[a, 70] (* _Jean-François Alcover_, Dec 06 2015, adapted from PARI *)"
			],
			"program": [
				"(PARI) a(n)=sumdiv(2*n,d, if(d%2==0, d-1, 0 ) ); /* _Joerg Arndt_, Oct 07 2012 */",
				"(PARI) a(n) = 2*sigma(n)-numdiv(n); \\\\ _Altug Alkan_, Mar 18 2018"
			],
			"xref": [
				"Cf. A129234, A129236, A129237.",
				"Cf. A000005, A000203.",
				"Cf. A130307.",
				"Cf. A051731, A143315, A143594."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Gary W. Adamson_, Apr 05 2007",
			"ext": [
				"Edited by _Emeric Deutsch_, Apr 17 2007"
			],
			"references": 11,
			"revision": 33,
			"time": "2020-07-12T14:56:50-04:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}