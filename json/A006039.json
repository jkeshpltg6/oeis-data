{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006039",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6039,
			"id": "M4132",
			"data": "6,20,28,70,88,104,272,304,368,464,496,550,572,650,748,836,945,1184,1312,1376,1430,1504,1575,1696,1870,1888,1952,2002,2090,2205,2210,2470,2530,2584,2990,3128,3190,3230,3410,3465,3496,3770,3944,4030",
			"name": "Primitive non-deficient numbers.",
			"comment": [
				"A number n is non-deficient (A023196) iff it is abundant or perfect, that is iff A001065(n) is \u003e= n. Since any multiple of a non-deficient number is itself non-deficient, we call a non-deficient number primitive iff all its proper divisors are deficient. - _Jeppe Stig Nielsen_, Nov 23 2003",
				"Numbers whose proper multiples are all abundant, and whose proper divisors are all deficient. - _Peter Munn_, Sep 08 2020"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A006039/b006039.txt\"\u003eTable of n, a(n) for n = 1..8671\u003c/a\u003e",
				"L. E. Dickson, \u003ca href=\"http://www.jstor.org/stable/2370405\"\u003eFiniteness of the Odd Perfect and Primitive Abundant Numbers with n Distinct Prime Factors\u003c/a\u003e, Amer. J. Math., 35 (1913), 413-426.",
				"R. K. Guy, \u003ca href=\"/A001599/a001599_1.pdf\"\u003eLetter to N. J. A. Sloane with attachment, Jun. 1991\u003c/a\u003e",
				"Jared Duker Lichtman, \u003ca href=\"https://doi.org/10.1016/j.jnt.2018.03.021\"\u003eThe reciprocal sum of primitive nondeficient numbers\u003c/a\u003e, Journal of Number Theory, Vol. 191 (2018), pp. 104-118."
			],
			"formula": [
				"Union of A000396 (perfect numbers) and A071395 (primitive abundant numbers). - _M. F. Hasler_, Jul 30 2016",
				"Sum_{n\u003e=1} 1/a(n) is in the interval (0.34842, 0.37937) (Lichtman, 2018). - _Amiram Eldar_, Jul 15 2020"
			],
			"mathematica": [
				"k = 1; lst = {}; While[k \u003c 4050, If[DivisorSigma[1, k] \u003e= 2 k \u0026\u0026 Min@Mod[k, lst] \u003e 0, AppendTo[lst, k]]; k++]; lst (* _Robert G. Wilson v_, Mar 09 2017 *)"
			],
			"xref": [
				"Cf. A001065 (aliquot function), A023196 (non-deficient), A005101 (abundant), A000396 (perfect), A005231 (odd abundant), A071395 (primitive abundant), A006038 (odd primitive abundant), A091191."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_, _R. K. Guy_",
			"references": 18,
			"revision": 42,
			"time": "2020-09-14T05:44:20-04:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}