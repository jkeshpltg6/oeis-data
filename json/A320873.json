{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A320873",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 320873,
			"data": "1480028141,1480028189,1480028183,1480028213,1480028171,1480028129,1480028159,1480028153,1480028201,1850590069,1850590117,1850590111,1850590141,1850590099,1850590057,1850590087,1850590081,1850590129,5196185959,5196186007,5196186001,5196186031,5196185989,5196185947,5196185977,5196185971,5196186019",
			"name": "List of 3 X 3 magic squares made of consecutive primes, in order of increasing magic sum. Only the lexicographically smallest variant of equivalent squares (modulo D4 symmetries) is listed, as a row containing the 3 rows of the square.",
			"comment": [
				"The first row is the lexicographically first 3 X 3 magic square of consecutive primes with the smallest possible magic constant 4440084513 = A270305(1) = A073520(3).",
				"The same 9 terms are also given in increasing order in sequence A073519. But this is equivalent of giving just the smallest of the terms (cf. A256891) or the central element (cf. A166113) or the magic constant itself (cf. A270305), which uniquely determines the sequence of primes since they have to be consecutive and their sum is equal to 3 times the magic constant.",
				"In the case of 3 X 3 magic squares, however, the lexicographically smallest representative has its elements in a well-defined order, see comment in A320872. This allows the reconstruction of the square from the set of primes which can be computed from the central elements A166113 or magic constants A270305, cf. PROGRAM in A073519."
			],
			"reference": [
				"Allan W. Johnson, Jr., Journal of Recreational Mathematics, vol. 23:3, 1991, pp. 190-191.",
				"Clifford A. Pickover, The Zen of Magic Squares, Circles and Stars: An Exhibition of Surprising Structures across Dimensions, Princeton University Press, 2002."
			],
			"link": [
				"Harvey Heinz, \u003ca href=\"http://www.magic-squares.net/primesqr.htm\"\u003ePrime Magic Squares\u003c/a\u003e",
				"\u003ca href=\"/index/Mag#magic\"\u003eIndex entries for sequences related to magic squares\u003c/a\u003e"
			],
			"formula": [
				"a(9n-4) = A166113(n) = A270305(n)/3 for all n \u003e= 1."
			],
			"example": [
				"The first row of 9 terms, (1480028141, 1480028189, 1480028183, 1480028213, 1480028171, 1480028129, 1480028159, 1480028153, 1480028201), corresponds to the following smallest 3 X 3 magic square of consecutive primes:",
				"    [1480028141  1480028189  1480028183]",
				"    [1480028213  1480028171  1480028129] .",
				"    [1480028159  1480028153  1480028201]",
				"The eleventh row yields the first example where the second term is smaller than the third one:",
				"    [23813359643  23813359721  23813359727]",
				"    [23813359781  23813359697  23813359613] .",
				"    [23813359667  23813359673  23813359751]"
			],
			"program": [
				"(PARI) A320873_row(n)=vecextract(n=MagicPrimes(3*A166113[n],3),[2,6+n=n[2]*2==n[1]+n[3],7-n,9,5,1,3+n,4-n,8]) \\\\ For MagicPrimes() see A073519 (the set of primes of the first row).",
				"/* the following allows to produce all 8 variants of a magic square that are equivalent modulo reflection on any of the 4 symmetry axes of the square */",
				"REV(M)=matconcat(Vecrev(M)) \\\\ reverse the order of columns of M",
				"FLIP(M)=matconcat(Colrev(M)) \\\\ reverse the order of rows of M",
				"ALL(M,C(f,L)=concat(apply(f,L),L))=Set(C(REV,C(FLIP,[M,M~]))) \\\\ PARI orders the set according to the (first) columns of the matrices, so one must take the transpose to get them ordered according to elements of the first row."
			],
			"xref": [
				"Cf. A073519, A073520, A073521, A073522.",
				"Cf. A073520 (smallest magic sum for a n X n magic square made from consecutive primes).",
				"Cf. A104157 (smallest of n^2 consecutive primes forming a magic square).",
				"Cf. A166113 (center element of 3 X 3 magic squares of consecutive primes).",
				"Cf. A256891 (smallest entry of 3 X 3 magic squares of consecutive primes) = A151799^4(A166113).",
				"Cf. A270305 (magic sums of 3 X 3 magic squares of consecutive primes) = 3*A166113."
			],
			"keyword": "nonn,tabf,changed",
			"offset": "1,1",
			"author": "_M. F. Hasler_, Oct 22 2018",
			"references": 11,
			"revision": 25,
			"time": "2022-01-07T19:34:28-05:00",
			"created": "2018-10-23T08:22:40-04:00"
		}
	]
}