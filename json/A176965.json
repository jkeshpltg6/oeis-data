{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A176965",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 176965,
			"data": "1,0,6,2,26,10,106,42,426,170,1706,682,6826,2730,27306,10922,109226,43690,436906,174762,1747626,699050,6990506,2796202,27962026,11184810,111848106,44739242,447392426,178956970,1789569706,715827882,7158278826",
			"name": "a(n) = 2^(n-1) - (2^n*(-1)^n + 2)/3.",
			"comment": [
				"The ratio a(n+1)/a(n) approaches 10 for even n and 2/5 for odd n as n-\u003einfinity."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A176965/b176965.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,4,-4)."
			],
			"formula": [
				"From _R. J. Mathar_, Apr 30 2010: (Start)",
				"a(n) = a(n-1) + 4*a(n-2) - 4*a(n-3).",
				"G.f.: x*(1 - x + 2*x^2)/( (1-x)*(1+2*x)*(1-2*x) ). (End)",
				"a(n) = A087231(n), n \u003e 2. - _R. J. Mathar_, May 03 2010",
				"a(2n-1) = A061547(2n), a(2n) = A061547(2n-1), n \u003e 0. - _Yosu Yurramendi_, Dec 23 2016",
				"a(n+1) = 2*A096773(n), n \u003e 0. - _Yosu Yurramendi_, Dec 30 2016",
				"a(2n-1) = A020989(n-1), a(2n) = A020988(n-1), n \u003e 0. - _Yosu Yurramendi_, Jan 03 2017",
				"a(2n-1) = (A083597(n-1) + A000302(n-1))/2, a(2n) = (A083597(n-1) - A000302(n-1))/2, n \u003e 0. - _Yosu Yurramendi_, Mar 04 2017",
				"a(n+2) = 4*a(n) + 2, a(1) = 1, a(2) = 0, n \u003e 0. - _Yosu Yurramendi_, Mar 07 2017",
				"a(n) = (-16 + (9 - (-1)^n) * 2^(n - (-1)^n))/24. - _Loren M. Pearson_, Dec 28 2019",
				"E.g.f.: (3*exp(2*x) - 4*exp(x) + 3 - 2*exp(-2*x))/6. - _G. C. Greubel_, Dec 28 2019",
				"a(n) = (2^n*5^(n mod 2) - 4)/6. - _Heinz Ebert_, Jun 29 2021"
			],
			"maple": [
				"seq( (3*2^(n-1) -(-2)^n -2)/3, n=1..30); # _G. C. Greubel_, Dec 28 2019"
			],
			"mathematica": [
				"a[n_]:= a[n]= 2^(n-1)*If[n==1, 1, a[n-1]/2 +(-1)^(n-1)*Sqrt[(5 +4*(-1)^(n-1) )]/2]; Table[a[n], {n,30}]",
				"LinearRecurrence[{1,4,-4}, {1,0,6}, 30] (* _G. C. Greubel_, Dec 28 2019 *)"
			],
			"program": [
				"(PARI) vector(30, n, (3*2^(n-1) -(-2)^n -2)/3 ) \\\\ _G. C. Greubel_, Dec 28 2019",
				"(MAGMA) [(3*2^(n-1) -(-2)^n -2)/3: n in [1..30]]; // _G. C. Greubel_, Dec 28 2019",
				"(Sage) [(3*2^(n-1) -(-2)^n -2)/3 for n in (1..30)] # _G. C. Greubel_, Dec 28 2019",
				"(GAP) List([1..30], n-\u003e (3*2^(n-1) -(-2)^n -2)/3); # _G. C. Greubel_, Dec 28 2019"
			],
			"xref": [
				"Merger of A020988 (even n) and A020989 (odd n)."
			],
			"keyword": "nonn,easy",
			"offset": "1,3",
			"author": "_Roger L. Bagula_, Apr 29 2010",
			"references": 3,
			"revision": 60,
			"time": "2021-06-29T16:10:46-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}