{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A138177",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 138177,
			"data": "1,1,2,1,4,4,1,7,15,10,1,10,36,52,26,1,14,74,176,190,76,1,18,132,460,810,696,232,1,23,222,1060,2705,3756,2674,764,1,28,347,2180,7565,15106,17262,10480,2620,1,34,525,4204,19013,51162,83440,80816,42732,9496,1,40",
			"name": "Triangle T(n,k) read by rows: number of k X k symmetric matrices with nonnegative integer entries and without zero rows or columns such that sum of all entries is equal to n, n\u003e=1, 1\u003c=k\u003c=n.",
			"comment": [
				"See the Brualdi/Ma reference for the connection to A161126. - _Joerg Arndt_, Nov 02 2014",
				"T(n,k) is also the number of semistandard Young tableaux of size n whose entries span the interval 1..k. See also _Gus Wiseman_'s comment in A138178. The T(4,2) = 7 semi-standard Young tableaux of size 4 spanning the interval 1..2 are:",
				"   11  122  112  111  1222  1122  1112",
				"   22  2    2    2                      . - _Jacob Post_, Jun 15 2018"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A138177/b138177.txt\"\u003eRows n = 1..141, flattened\u003c/a\u003e",
				"Richard A. Brualdi, Shi-Mei Ma, \u003ca href=\"http://dx.doi.org/10.1016/j.ejc.2014.08.026\"\u003eEnumeration of involutions by descents and symmetric matrices\u003c/a\u003e, European Journal of Combinatorics, vol.43, pp.220-228, (January 2015).",
				"FindStat - Combinatorial Statistic Finder, \u003ca href=\"http://www.findstat.org/SemistandardTableaux\"\u003eSemistandard Young tableaux\u003c/a\u003e",
				"Samantha Dahlberg, \u003ca href=\"http://arxiv.org/abs/1410.7356\"\u003eCombinatorial Proofs of Identities Involving Symmetric Matrices\u003c/a\u003e, arXiv:1410.7356 [math.CO], (27-October-2014)"
			],
			"formula": [
				"T(n,k) = Sum_{i=0..k} (-1)^i * binomial(k,i) * A210391(n,k-i). - _Alois P. Heinz_, Apr 06 2015"
			],
			"example": [
				"Triangle T(n,k) begins:",
				"  1;",
				"  1,  2;",
				"  1,  4,   4;",
				"  1,  7,  15,   10;",
				"  1, 10,  36,   52,   26;",
				"  1, 14,  74,  176,  190,   76;",
				"  1, 18, 132,  460,  810,  696,  232;",
				"  1, 23, 222, 1060, 2705, 3756, 2674, 764;",
				"  ..."
			],
			"maple": [
				"gf:= k-\u003e 1/((1-x)^k*(1-x^2)^(k*(k-1)/2)):",
				"A:= (n, k)-\u003e coeff(series(gf(k), x, n+1), x, n):",
				"T:= (n, k)-\u003e add(A(n, k-i)*(-1)^i*binomial(k, i), i=0..k):",
				"seq(seq(T(n, k), k=1..n), n=1..12);  # _Alois P. Heinz_, Apr 06 2015"
			],
			"mathematica": [
				"gf[k_] := 1/((1-x)^k*(1-x^2)^(k*(k-1)/2)); A[n_, k_] := Coefficient[ Series [gf[k], {x, 0, n+1}], x, n]; T[n_, k_] := Sum[(-1)^j*Binomial[k, j]*A[n, k-j], {j, 0, k}]; Table[T[n, k], {n, 1, 12}, {k, 1, n}] // Flatten (* _Jean-François Alcover_, Jan 31 2016, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. (row sums) A138178, A135589, A135588, A161126, A210391.",
				"Main diagonal gives A000085. - _Alois P. Heinz_, Apr 06 2015",
				"T(2n,n) gives A266305.",
				"T(n^2,n) gives A268309."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Vladeta Jovovic_, Mar 03 2008",
			"references": 6,
			"revision": 43,
			"time": "2018-11-17T16:20:46-05:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}