{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A178472",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 178472,
			"data": "0,1,1,2,1,5,1,8,4,17,1,38,1,65,19,128,1,284,1,518,67,1025,1,2168,16,4097,256,8198,1,16907,1,32768,1027,65537,79,133088,1,262145,4099,524408,1,1056731,1,2097158,16636,4194305,1,8421248,64,16777712,65539",
			"name": "Number of compositions (ordered partitions) of n where the gcd of the part sizes is not 1.",
			"comment": [
				"Of course, all part sizes must be greater than 1; that condition alone gives the Fibonacci numbers, which is thus an upper bound.",
				"Also the number of periodic compositions of n, where a sequence is periodic if its cyclic rotations are not all different. Also compositions with non-relatively prime run-lengths. - _Gus Wiseman_, Nov 10 2019"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A178472/b178472.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Hunki Baek, Sejeong Bang, Dongseok Kim, Jaeun Lee, \u003ca href=\"http://arxiv.org/abs/1412.2426\"\u003eA bijection between aperiodic palindromes and connected circulant graphs\u003c/a\u003e, arXiv:1412.2426 [math.CO], 2014. See Table 2."
			],
			"formula": [
				"a(n) = Sum_{d|n \u0026 d\u003cn} 2^(d-1) * (-mu(n/d)).",
				"a(n) = 2^(n-1) - A000740(n).",
				"a(n) = A152061(n)/2. - _George Beck_, Jan 20 2018"
			],
			"example": [
				"For n=6, we have 5 compositions: \u003c6\u003e, \u003c4,2\u003e, \u003c2,4\u003e, \u003c2,2,2\u003e, and \u003c3,3\u003e.",
				"From _Gus Wiseman_, Nov 10 2019: (Start)",
				"The a(2) = 1 through a(9) = 4 non-relatively prime compositions:",
				"  (2)  (3)  (4)    (5)  (6)      (7)  (8)        (9)",
				"            (2,2)       (2,4)         (2,6)      (3,6)",
				"                        (3,3)         (4,4)      (6,3)",
				"                        (4,2)         (6,2)      (3,3,3)",
				"                        (2,2,2)       (2,2,4)",
				"                                      (2,4,2)",
				"                                      (4,2,2)",
				"                                      (2,2,2,2)",
				"The a(2) = 1 through a(9) = 4 periodic compositions:",
				"  11  111  22    11111  33      1111111  44        333",
				"           1111         222              1313      121212",
				"                        1212             2222      212121",
				"                        2121             3131      111111111",
				"                        111111           112112",
				"                                         121121",
				"                                         211211",
				"                                         11111111",
				"The a(2) = 1 through a(9) = 4 compositions with non-relatively prime run-lengths:",
				"  11  111  22    11111  33      1111111  44        333",
				"           1111         222              1133      111222",
				"                        1122             2222      222111",
				"                        2211             3311      111111111",
				"                        111111           111122",
				"                                         112211",
				"                                         221111",
				"                                         11111111",
				"(End)"
			],
			"maple": [
				"A178472 := n -\u003e (2^n - add(mobius(n/d)*2^d, d in divisors(n)))/2:",
				"seq(A178472(n), n=1..51); # _Peter Luschny_, Jan 21 2018"
			],
			"mathematica": [
				"Table[2^(n - 1) - DivisorSum[n, MoebiusMu[n/#]*2^(# - 1) \u0026], {n, 51}] (* _Michael De Vlieger_, Jan 20 2018 *)"
			],
			"program": [
				"(PARI) vector(60,n,2^(n-1)-sumdiv(n,d,2^(d-1)*moebius(n/d)))"
			],
			"xref": [
				"Cf. A000045, A008683, A011782, A178470.",
				"Periodic binary words are A152061.",
				"Cf. A000740, A027375, A059966, A121016, A329140, A329145."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Franklin T. Adams-Watters_, May 28 2010",
			"ext": [
				"Ambiguous term a(0) removed by _Max Alekseyev_, Jan 02 2012"
			],
			"references": 39,
			"revision": 34,
			"time": "2019-11-14T12:56:32-05:00",
			"created": "2010-07-11T03:00:00-04:00"
		}
	]
}