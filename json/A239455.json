{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A239455",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 239455,
			"data": "0,1,2,2,4,5,7,10,13,16,21,28,33,45,55,65,83,105,121,155,180,217,259,318,362,445,512,614,707,850,958,1155,1309,1543,1754,2079,2327,2740,3085,3592,4042,4699,5253,6093,6815,7839,8751,10069,11208,12832,14266,16270",
			"name": "Number of Look-and-Say partitions of n; see Comments.",
			"comment": [
				"Suppose that p = x(1) \u003e= x(2) \u003e= … \u003e= x(k) is a partition of n.  Let y(1) \u003e y(2) \u003e … \u003e y(h) be the distinct parts of p, and let m(i) be the multiplicity of y(i) for 1 \u003c= i \u003c= h.  Then we can \"look\" at p as \"m(1) y(1)'s and m(2) y(2)'s and … m(h) y(h)'s\". Reversing the m's and y's, we can then \"say\" the Look-and-Say partition of p, denoted by LS(p).  The name \"Look-and-Say\" follows the example of Look-and-Say integer sequences (e.g., A005150).  As p ranges through the partitions of n, LS(p) ranges through all the Look-and-Say partitions of n. The number of these is A239455(n).",
				"The Look-and-Say array is distinct from the Wilf array, described at A098859; for example, the number of Look-and-Say partitions of 9 is A239455(9) = 16, whereas the number of Wilf partitions of 9 is A098859(9) = 15.  The Look-and-Say partition of 9 which is not a Wilf partition of 9 is [2,2,2,1,1,1]."
			],
			"example": [
				"The 11 partitions of 6 generate 7 Look-and-Say partitions as follows:",
				"6 -\u003e 111111",
				"51 -\u003e 111111",
				"42 -\u003e 111111",
				"411 -\u003e 21111",
				"33 -\u003e 222",
				"321 -\u003e 111111",
				"3111 -\u003e 3111",
				"222 -\u003e 33",
				"2211 -\u003e 222",
				"21111 -\u003e 411",
				"111111 -\u003e 6,",
				"so that a(6) counts these 7 partitions:  111111, 21111, 222, 3111, 33, 411, 6."
			],
			"mathematica": [
				"LS[part_List] := Reverse[Sort[Flatten[Map[Table[#[[2]], {#[[1]]}] \u0026, Tally[part]]]]]; LS[n_Integer] := #[[Reverse[Ordering[PadRight[#]]]]] \u0026[DeleteDuplicates[Map[LS, IntegerPartitions[n]]]]; TableForm[t = Map[LS[#] \u0026, Range[10]]](*A239454,array*)",
				"Flatten[t](*A239454,sequence*)",
				"Map[Length[LS[#]] \u0026, Range[25]](*A239455*)",
				"(* Peter J. C. Moses, Mar 18 2014 *)"
			],
			"xref": [
				"Cf. A239454."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Clark Kimberling_ and _Peter J. C. Moses_, Mar 19 2014",
			"references": 2,
			"revision": 21,
			"time": "2019-05-19T20:34:04-04:00",
			"created": "2014-03-24T04:30:29-04:00"
		}
	]
}