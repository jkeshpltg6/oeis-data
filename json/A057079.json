{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A057079",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 57079,
			"data": "1,2,1,-1,-2,-1,1,2,1,-1,-2,-1,1,2,1,-1,-2,-1,1,2,1,-1,-2,-1,1,2,1,-1,-2,-1,1,2,1,-1,-2,-1,1,2,1,-1,-2,-1,1,2,1,-1,-2,-1,1,2,1,-1,-2,-1,1,2,1,-1,-2,-1,1,2,1,-1,-2,-1,1,2,1,-1,-2,-1,1,2,1,-1,-2,-1,1,2,1,-1,-2,-1,1,2,1,-1,-2,-1",
			"name": "Periodic sequence: repeat [1,2,1,-1,-2,-1]; expansion of (1+x)/(1-x+x^2).",
			"comment": [
				"Inverse binomial transform of A057083. Binomial transform of A061347. The sums of consecutive pairs of elements give A084103. - _Paul Barry_, May 15 2003",
				"Hexaperiodic sequence identical to its third differences. - _Paul Curtz_, Dec 13 2007",
				"a(n+1) is the Hankel transform of A001700(n+1)-A001700(n). - _Paul Barry_, Apr 21 2009",
				"Non-simple continued fraction expansion of 1 = 1+1/(2+1/(1+1/(-1+...))). - _R. J. Mathar_, Mar 08 2012",
				"Pisano period lengths: 1, 3, 2, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, ... - _R. J. Mathar_, Aug 10 2012",
				"Alternating row sums of Riordan triangle A111125. - _Wolfdieter Lang_, Oct 18 2012",
				"Periodic sequences of this type can be also calculated by a(n) = c + floor(q/(p^m-1)*p^n) mod p, where c is a constant, q is the number representing the periodic digit pattern and m is the period length. c, p and q can be calculated as follows: Let D be the array representing the number pattern to be repeated, m = size of D, max = maximum value of elements in D, min = minimum value of elements in D. Than c := min, p := max - min + 1 and q := p^m*sum_{i=1..m} (D(i)-min)/p^i. Example: D = (1, 2, 1, -1, -2, -1), c = -2, m = 6, p = 5 and q = 12276 for this sequence. - _Hieronymus Fischer_, Jan 04 2013"
			],
			"link": [
				"Alex Fink, Richard K. Guy, and Mark Krusemeyer, \u003ca href=\"https://doi.org/10.11575/cdm.v3i2.61940\"\u003ePartitions with parts occurring at most thrice\u003c/a\u003e, Contributions to Discrete Mathematics, Vol 3, No 2 (2008), pp. 76-114. See Section 13.",
				"T.-X. He and L. W. Shapiro, \u003ca href=\"http://dx.doi.org/10.1016/j.laa.2017.06.025\"\u003eFuss-Catalan matrices, their weighted sums, and stabilizer subgroups of the Riordan group\u003c/a\u003e, Lin. Alg. Applic. 532 (2017) 25-41, theorem 2.5, k=3.",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,-1).",
				"\u003ca href=\"/index/Rea#recur1\"\u003eIndex entries for recurrences a(n) = k*a(n - 1) +/- a(n - 2)\u003c/a\u003e",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e"
			],
			"formula": [
				"a(n) = S(n, 1) + S(n-1, 1) = S(2*n, sqrt(3)); S(n, x) := U(n, x/2), Chebyshev polynomials of 2nd kind, A049310. S(n, 1) = A010892(n).",
				"a(n) = 2*cos((n-1)*Pi/3) = a(n-1) - a(n-2) = -a(n-3) = a(n-6) = (A022003(n+1)+1)*(-1)^floor(n/3). Unsigned a(n) = 4 - a(n-1) - a(n-2). - _Henry Bottomley_, Mar 29 2001",
				"a(n) = (-1)^floor(n/3) + ((-1)^floor((n-1)/3) + (-1)^floor((n+1)/3))/2. - Mario Catalani (mario.catalani(AT)unito.it), Jan 07 2003",
				"a(n) = (1/2 - sqrt(3)*i/2)^(n-1) + (1/2 + sqrt(3)*i/2)^(n-1) = cos(Pi*n/3) + sqrt(3)*sin(Pi*n/3). - _Paul Barry_, Mar 15 2004",
				"The period 3 sequence (2, -1, -1, ...) has a(n) = 2*cos(2*Pi*n/3) = (-1/2 - sqrt(3)*i/2)^n + (-1/2 + sqrt(3)*i/2)^n. - _Paul Barry_, Mar 15 2004",
				"Euler transform of length 6 sequence [2, -2, -1, 0, 0, 1]. - _Michael Somos_, Jul 14 2006",
				"G.f.: (1 + x) / (1 - x + x^2) = (1 - x^2)^2 * (1 - x^3) / ((1 - x)^2 * (1 - x^6)). a(n) = a(2-n) for all n in Z. - _Michael Somos_, Jul 14 2006",
				"a(n) = -(1/6)*(2*(n mod 6) + ((n+1) mod 6) - ((n+2) mod 6) - 2*((n+3) mod 6) - ((n+4) mod 6) + ((n+5) mod 6)). - _Paolo P. Lava_, Nov 20 2006",
				"a(n) = A033999(A002264(n))*(A000035(A010872(n))+1). - _Hieronymus Fischer_, Jun 20 2007",
				"a(n) = (3*A033999(A002264(n)) - A033999(n))/2. - _Hieronymus Fischer_, Jun 20 2007",
				"a(n) = (-1)^floor(n/3)*((n mod 3) mod 2 + 1). - _Hieronymus Fischer_, Jun 20 2007",
				"a(n) = (3*(-1)^floor(n/3) - (-1)^n)/2. - _Hieronymus Fischer_, Jun 20 2007",
				"a(n) = (-1)^((n-1)/3) + (-1)^((1-n)/3). - _Jaume Oliver Lafont_, May 13 2010",
				"E.g.f.: E(x) = S(0), S(k) = 1 + 2*x/(6*k+1 - x*(6*k+1)/(4*(3*k+1) + x + 4*x*(3*k+1)/(6*k + 3 - x - x*(6*k+3)/(3*k + 2 + x - x*(3*k+2)/(12*k + 10 + x - x*(12*k+10)/(x - (6*k+6)/S(k+1))))))); (continued fraction). - _Sergei N. Gladkovskii_, Dec 14 2011",
				"a(n) = -2 + floor((281/819)*10^(n+1)) mod 10. - _Hieronymus Fischer_, Jan 04 2013",
				"a(n) = -2 + floor((11/14)*5^(n+1)) mod 5. - _Hieronymus Fischer_, Jan 04 2013",
				"a(n) = A010892(n) + A010892(n-1).",
				"a(n) = ( (1+i*sqrt(3))^(n-1) + (1-i*sqrt(3))^(n-1) )/2^(n-1), where i=sqrt(-1). - _Bruno Berselli_, Dec 01 2014",
				"a(n) = 2*sin((2n+1)*Pi/6). - _Wesley Ivan Hurt_, Apr 04 2015",
				"a(n) = hypergeom([-n/2-2, -n/2-5/2], [-n-4], 4). - _Peter Luschny_, Dec 17 2016",
				"G.f.: 1 / (1 - 2*x / (1 + 3*x / (2 - x))). - _Michael Somos_, Dec 29 2016",
				"a(n) = (2*n+1)*(Sum_{k=0..n} ((-1)^k/(2*k+1))*binomial(n+k,2*k)) for n \u003e= 0. - _Werner Schulte_, Jul 10 2017",
				"Sum_{n\u003e=0} (a(n)/(2*n+1))*x^(2*n+1) = arctan(x/(1-x^2)) for -1 \u003c x \u003c 1. - _Werner Schulte_, Jul 10 2017"
			],
			"example": [
				"G.f. = 1 + 2*x + x^2 - x^3 - 2*x^4 - x^5 + x^6 + 2*x^7 + x^8 - x^9 - 2*x^10 + x^11 + ..."
			],
			"maple": [
				"A057079:=n-\u003e[1, 2, 1, -1, -2, -1][(n mod 6)+1]: seq(A057079(n), n=0..100); # _Wesley Ivan Hurt_, Mar 10 2015"
			],
			"mathematica": [
				"a[n_] := {1, 2, 1, -1, -2, -1}[[Mod[n, 6] + 1]]; Array[a, 100, 0] (* _Jean-François Alcover_, Jul 05 2013 *)",
				"CoefficientList[Series[(1 + x)/(1 - x + x^2), {x, 0, 71}], x] (* _Michael De Vlieger_, Jul 10 2017 *)"
			],
			"program": [
				"(PARI) {a(n) = [1, 2, 1, -1, -2, -1][n%6 + 1]}; /* _Michael Somos_, Jul 14 2006 */",
				"(PARI) {a(n) = if( n\u003c0, n = 2-n); polcoeff( (1 + x) / (1 - x + x^2) + x * O(x^n), n)}; /* _Michael Somos_, Jul 14 2006 */",
				"(PARI) a(n)=2^(n%3%2)*(-1)^(n\\3) \\\\ _Tani Akinari_, Aug 15 2013"
			],
			"xref": [
				"Cf. A049310. Apart from signs, same as A061347.",
				"Cf. A002264, A010872."
			],
			"keyword": "easy,sign",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Aug 04 2000",
			"references": 61,
			"revision": 98,
			"time": "2021-06-12T02:56:57-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}