{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A045623",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 45623,
			"id": "M1412",
			"data": "1,2,5,12,28,64,144,320,704,1536,3328,7168,15360,32768,69632,147456,311296,655360,1376256,2883584,6029312,12582912,26214400,54525952,113246208,234881024,486539264,1006632960,2080374784,4294967296",
			"name": "Number of 1's in all compositions of n+1.",
			"comment": [
				"Let M_n be the n X n matrix m_(i,j) = 2 + abs(i-j) then det(M_n) = (-1)^(n-1)*a(n-1). - _Benoit Cloitre_, May 28 2002",
				"a(n) is the number of triangulations of a regular (n+3)-gon in which every triangle shares at least one side with the polygon itself. - _David Callan_, Mar 25 2004",
				"Number of compositions of j+n, j\u003en and j the maximum part. E.g. a(4) is derived from the number of compositions of, for example: 54(2), 531(6), 522(3), 5211(12) and 51111(5) giving 2+6+3+12+5=28. - _Jon Perry_, Sep 13 2005",
				"If X_1,X_2,...,X_n are 2-blocks of a (2n+2)-set X then, for n\u003e=1, a(n+1) is the number of (n+1)-subsets of X intersecting each X_i, (i=1,2,...,n). - _Milan Janjic_, Nov 18 2007",
				"Generated from iterates of M * [1,1,1,...], where M = an infinite triadiagonal matrix with (1,1,1,...) in the main and superdiagonals and (1,0,0,0,...) in the subdiagonal. - _Gary W. Adamson_, Jan 04 2009",
				"a(n) is the number of weak compositions of n with exactly 1 part equal to 0. - _Milan Janjic_, Jun 27 2010",
				"An elephant sequence, see A175654. For the corner squares 16 A[5] vectors, with decimal values between 19 and 400, lead to this sequence. For the central square these vectors lead to the companion sequence A045891 (without the first leading 1). - _Johannes W. Meijer_, Aug 15 2010",
				"Equals first finite difference row of A001792: (1, 3, 8, 20, 48, 112, ...). - _Gary W. Adamson_, Oct 26 2010",
				"With alternating signs the g.f. is: (1 + x)^2/(1 + 2*x)^2.",
				"Number of 132-avoiding permutations of [n+2] containing exactly one 213 pattern. - _David Scambler_, Nov 07 2011",
				"a(n) is the number of 1's in all compositions of n+1 = the number of 2's in all compositions of n+2 = the number of 3's in all compositions of n+3 = ...  So the partial sums = A001792. - _Geoffrey Critzer_, Feb 12 2012",
				"Also number of compositions of n into 2 sorts of parts where all parts of the first sort precede all parts of the second sort; see example. - _Joerg Arndt_, Apr 28 2013",
				"a(n) is also the difference of the total number of parts between all compositions of n+1 and all compositions of n. The equivalent sequence for partitions is A138137. - _Omar E. Pol_, Aug 28 2013",
				"Except for an initial 1, this is the p-INVERT of (1,1,1,1,1,...) for p(S) = (1 - S)^2; see A291000. - _Clark Kimberling_, Aug 24 2017",
				"For a composition of n, the total number of runs of parts of size k is a(n-k) - a(n-2k). - _Gregory L. Simay_, Feb 17 2018",
				"a(n) is the number of binary trees on n+1 nodes that are isomorphic to a path graph. The ratio of a(n)/A000108(n+1) gives the probability that a random Catalan tree on n+1 nodes is isomorphic to a path graph. - _Marcel K. Goh_, May 09 2020",
				"a(n) is the number of words of length n over the alphabet {0,1,2} such that the first letter is not 2 and the last 1 occurs before the first 0. - _Henri Mühle_, Mar 08 2021"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A045623/b045623.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Marco Abrate, Stefano Barbero, Umberto Cerruti and Nadir Murru, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2014.06.026\"\u003eColored compositions, Invert operator and elegant compositions with the \"black tie\"\u003c/a\u003e, Discrete Math., Vol. 335 (2014), pp. 1-7. MR3248794.",
				"Marco Abrate, Stefano Barbero, Umberto Cerruti and Nadir Murru, \u003ca href=\"http://arxiv.org/abs/1409.6454\"\u003eColored compositions, Invert operator and elegant compositions with the \"black tie\"\u003c/a\u003e, arXiv:1409.6454 [math.NT], 2014.",
				"Ron M. Adin and Yuval Roichman, \u003ca href=\"http://arxiv.org/abs/1301.1675\"\u003eMatrices, Characters and Descents\u003c/a\u003e, arXiv:1301.1675 [math.CO], 2013-2014; see p.10.",
				"Freddy Cachazo and Nick Early, \u003ca href=\"https://arxiv.org/abs/2010.09708\"\u003ePlanar Kinematics: Cyclic Fixed Points, Mirror Superpotential, k-Dimensional Catalan Numbers, and Root Polytopes\u003c/a\u003e, arXiv:2010.09708 [math.CO], 2020.",
				"Camille Combe, \u003ca href=\"https://arxiv.org/abs/2007.00048\"\u003eA geometric and combinatorial exploration of Hochschild lattices\u003c/a\u003e, arXiv:2007.00048 [math.CO], 2020.",
				"Éva Czabarka, Rigoberto Flórez, Leandro Junes and José L. Ramírez, \u003ca href=\"https://doi.org/10.1016/j.disc.2018.06.032\"\u003eEnumerations of peaks and valleys on non-decreasing Dyck paths\u003c/a\u003e, Discrete Math., Vol. 341, No. 10 (2018), pp. 2789-2807. See p. 2798.",
				"Michael Dairyko, Lara Pudwell, Samantha Tyner and Casey Wynn, \u003ca href=\"https://doi.org/10.37236/2099\"\u003eNon-contiguous pattern avoidance in binary trees\u003c/a\u003e. Electron. J. Combin., Vol. 19, No. 3 (2012), Paper 22, 21 pp. MR2967227. - From _N. J. A. Sloane_, Feb 01 2013",
				"Robert Davis and Greg Simay, \u003ca href=\"https://arxiv.org/abs/2001.11089\"\u003eFurther Combinatorics and Applications of Two-Toned Tilings\u003c/a\u003e, arXiv:2001.11089 [math.CO], 2020.",
				"Frank Ellermann, \u003ca href=\"/A001792/a001792.txt\"\u003eIllustration of binomial transforms\u003c/a\u003e.",
				"Nickolas Hein and Jia Huang, \u003ca href=\"https://arxiv.org/abs/1807.04623\"\u003eVariations of the Catalan numbers from some nonassociative binary operations\u003c/a\u003e, arXiv:1807.04623 [math.CO], 2018.",
				"V. E. Hoggatt, Jr., \u003ca href=\"/A001628/a001628.pdf\"\u003eLetters to N. J. A. Sloane, 1974-1975\u003c/a\u003e.",
				"Milan Janjic, \u003ca href=\"https://pmf.unibl.org/wp-content/uploads/2017/10/enumfor.pdf\"\u003eTwo Enumerative Functions\u003c/a\u003e.",
				"Milan Janjic and Boris Petkovic, \u003ca href=\"http://arxiv.org/abs/1301.4550\"\u003eA Counting Function\u003c/a\u003e, arXiv 1301.4550 [math.CO], 2013.",
				"Milan Janjic and Boris Petkovic, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Janjic/janjic45.html\"\u003eA Counting Function Generalizing Binomial Coefficients and Some Other Classes of Integers\u003c/a\u003e, J. Int. Seq., Vol. 17 (2014), Article 14.3.5.",
				"Sergey Kitaev, Jeffrey Remmel and Mark Tiefenbruck, \u003ca href=\"https://arxiv.org/abs/1302.2274\"\u003eQuadrant Marked Mesh Patterns in 132-Avoiding Permutations II\u003c/a\u003e, arXiv:1302.2274 [math.CO], 2013.",
				"Sergey Kitaev, Jeffrey Remmel and Mark Tiefenbruck, \u003ca href=\"https://www.emis.de/journals/INTEGERS/papers/p16/p16.Abstract.html\"\u003eQuadrant Marked Mesh Patterns in 132-Avoiding Permutations II\u003c/a\u003e, Electronic Journal of Combinatorial Number Theory, Vol. 15 (2015), Article A16.",
				"Henri Mühle \u003ca href=\"https://arxiv.org/abs/2008.13247\"\u003eHochschild lattices and shuffle lattices\u003c/a\u003e, arXiv:2008.13247 [math.CO], 2020.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-4)."
			],
			"formula": [
				"Sum_{k = 0..n} (k+2)*binomial(n,k) gives the sequence but with a different offset: 2, 5, 12, 28, 64, 144, 320, 704, 1536, ... - _N. J. A. Sloane_, Jan 30 2008 - formula corrected by _Robert G. Wilson v_, Feb 26 2018",
				"Binomial transform of 1,1,2,2,3,3,... . - _Paul Barry_, Mar 06 2003",
				"a(0)=1, a(n) = (n+3)*2^(n-2), n \u003e= 1.",
				"a(n+1) = 2*a(n) + 2^(n-1), n\u003e0.",
				"G.f.: (1-x)^2/(1-2*x)^2. - Detlef Pauly (dettodet(AT)yahoo.de), Mar 03 2003",
				"G.f.: 1/(1-x-x^2-x^3-...)^2. - _Jon Perry_, Jul 04 2004",
				"a(n) = Sum_{0 \u003c= j \u003c= k \u003c= n} binomial(n, j+k). - _Benoit Cloitre_, Oct 14 2004",
				"a(n) = Sum_{k=0..n} C(n, k)*floor((k+2)/2). - _Paul Barry_, Mar 06 2003",
				"a(n+1) - 2*a(n) = A131577(n). - _Paul Curtz_, May 18 2008",
				"G.f.: 1/(1-x) + Q(0)*x/(1-x)^3, where Q(k)= 1 + (k+1)*x/(1 - x - x*(1-x)/(x + (k+1)*(1-x)/Q(k+1))); (continued fraction). - _Sergei N. Gladkovskii_, Apr 25 2013",
				"a(n) = Sum_{k=0..n} (k+1)*C(n-1,n-k). - _Peter Luschny_, Apr 20 2015",
				"a(n) = Sum_{k=0..n-1} a(k) + 2^(n-1) = A001787(n-1) + 2^n, a(0)=1. - _Yuchun Ji_, May 22 2020",
				"a(n) = Sum_{m=0..n}((2*m+2)*n-2*m^2+1)*C(2*n+2,2*m+1)/((4*n+2)*2^n). - _Vladimir Kruchinin_, Nov 01 2020",
				"E.g.f.: (1 + exp(2*x)*(3 + 2*x))/4. - _Stefano Spezia_, Dec 19 2021",
				"From _Amiram Eldar_, Jan 05 2022: (Start)",
				"Sum_{n\u003e=0} 1/a(n) = 32*log(2) - 61/3.",
				"Sum_{n\u003e=0} (-1)^n/a(n) = 32*log(3/2) - 37/3. (End)"
			],
			"example": [
				"E.g. a(2)=5 because in the compositions of 3, namely 3,2+1,1+2,1+1+1, we have five 1's altogether.",
				"There are a(3)=12 compositions of 3 into 2 sorts of parts where all parts of the first sort precede all parts of the second sort. Here p:s stands for \"part p of sort s\":",
				"01:  [ 1:0  1:0  1:0  ]",
				"02:  [ 1:0  1:0  1:1  ]",
				"03:  [ 1:0  1:1  1:1  ]",
				"04:  [ 1:0  2:0  ]",
				"05:  [ 1:0  2:1  ]",
				"06:  [ 1:1  1:1  1:1  ]",
				"07:  [ 1:1  2:1  ]",
				"08:  [ 2:0  1:0  ]",
				"09:  [ 2:0  1:1  ]",
				"10:  [ 2:1  1:1  ]",
				"11:  [ 3:0  ]",
				"12:  [ 3:1  ]",
				"- _Joerg Arndt_, Apr 28 2013",
				"For the compositions of 6, the total number of runs of parts of size 2 is a(6-2) - a(6-2*2) = 28 - 5 = 23, enumerated as follows (with the runs of 2 enclosed in []): 4,[2]; [2],4; [2],3,1; [2],1,3; 3,[2],1; 1,[2],3; 3,1,[2]; 1,3,[2]; [2,2,2]; [2,2],1,1; 1,[2,2],1; 1,1,[2,2]; [2],1,[2],1; 1,[2],1,[2]; [2],1,1,[2]; [2],1,1,1,1; 1,[2],1,1,1; 1,1,[2],1,1; 1,1,1,[2],1; and 1,1,1,1[2]. - _Gregory L. Simay_, Feb 17 2018",
				"There are a(3)=12 triwords of length 3: (0,0,0), (0,0,2), (0,2,0), (0,2,2), (1,0,0), (1,0,2), (1,1,0), (1,1,1), (1,1,2), (1,2,0), (1,2,1), (1,2,2). - _Henri Mühle_, Mar 08 2021"
			],
			"maple": [
				"seq(ceil(1/4*2^n*(n+3)),n=0..50);"
			],
			"mathematica": [
				"Table[If[n==0, 1, 2^(n-2)(n+3)], {n, 0, 29}] (* _Robert G. Wilson v_, Jun 27 2005 *)",
				"CoefficientList[Series[(1 -2x +x^2)/(1-2x)^2, {x, 0, 30}], x] (* or *)",
				"LinearRecurrence[{4, -4}, {1, 2, 5}, 31] (* _Robert G. Wilson v_, Feb 18 2018 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c1,n==0,(n+3)*2^(n-2))",
				"(Haskell)",
				"a045623 n = a045623_list !! n",
				"a045623_list = tail $ f a011782_list [] where",
				"   f (u:us) vs = sum (zipWith (*) vs $ reverse ws) : f us ws",
				"     where ws = u : vs",
				"-- _Reinhard Zumkeller_, Jul 21 2013",
				"(GAP) a:=[2,5];; for n in [3..40] do a[n]:=4*a[n-1]-4*a[n-2]; od; Concatenation([1],a); # _Muniru A Asiru_, Oct 16 2018",
				"(Maxima)",
				"a(n):=sum(((2*m+2)*n-2*m^2+1)*binomial(2*n+2,2*m+1),m,0,n)/((4*n+2)*2^n); /* _Vladimir Kruchinin_, Nov 01 2020 */"
			],
			"xref": [
				"Convolution of A011782.",
				"Row sums of A103450, A128254, A152195, A177992, A198069.",
				"Cf. A001792."
			],
			"keyword": "easy,nonn,changed",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_",
			"references": 78,
			"revision": 184,
			"time": "2022-01-05T13:55:49-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}