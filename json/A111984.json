{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A111984",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 111984,
			"data": "1,8,-64,960,-15360,274432,-5223936,103604224,-2115993600,44188631040,-939004514304,20235083907072,-441102457372672,9708551114588160,-215436164616683520,4814308934865944576,-108243166106365722624,2446764246113433157632",
			"name": "Expansion of g.f.: A(x) = ( Sum_{n\u003e=0} (2*n+1)*8^n*x^(n*(n+1)/2) )^(1/3).",
			"comment": [
				"Define F(x,q) = Sum_{n\u003e=0} (2*n+1)*q^n*x^(n*(n+1)/2); then F(x,q)^(1/3) is an integer series in x when q = -1 (mod 9) or when q = 0 (mod 3). At q=-1 we have the famous result of Jacobi: F(x,-1)^(1/3) = (1 - 3*x + 5*x^3 - 7*x^6 + 9*x^10 +...)^(1/3) = 1 + Sum_{n\u003e=1} (-1)^n * (x^(n*(3*n-1)/2) + x^(n*(3*n+1)/2) )."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A111984/b111984.txt\"\u003eTable of n, a(n) for n = 0..715\u003c/a\u003e"
			],
			"formula": [
				"Conjecture: a(5*n+4) == 0 (mod 5) (checked up to n = 200). - _Peter Bala_, Feb 26 2021"
			],
			"example": [
				"G.f.: A(x) = 1 + 8*x - 64*x^2 + 960*x^3 - 15360*x^4 + 274432*x^5 - 5223936*x^6 +...",
				"where",
				"A(x)^3 = 1 + 3*8*x + 5*8^2*x^3 + 7*8^3*x^6 + 9*8^4*x^10 + 11*8^5*x^15 + 13*8^6*x^21 + 15*8^7*x^28 + 17*8^8*x^36 + 19*8^9*x^45 + 21*8^10*x^55 +..."
			],
			"maple": [
				"seq(coeff(series( ( add((2*n+1)*8^n*x^(n*(n+1)/2), n=0..40) )^(1/3), x, n+1), x, n), n = 0..30); # _G. C. Greubel_, Nov 05 2019"
			],
			"mathematica": [
				"With[{nn=20},CoefficientList[Series[Surd[Sum[(2n+1)8^n x^((n(n+1))/2),{n,0,nn}],3],{x,0,nn}],x]] (* _Harvey P. Dale_, Jul 04 2013 *)"
			],
			"program": [
				"(PARI) a(n)=polcoeff(sum(k=0,sqrtint(2*n+1),(2*k+1)*8^k*x^(k*(k+1)/2)+x*O(x^n))^(1/3),n)",
				"(Sage) [( (sum((2*n+1)*8^n*x^(n*(n+1)/2) for n in (0..40)) )^(1/3) ).series(x,n+1).list()[n] for n in (0..30)] # _G. C. Greubel_, Nov 05 2019"
			],
			"xref": [
				"Cf. A111983 (g.f. A(x)^3), A111985 (g.f. A(x)^(1/4))."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Paul D. Hanna_, Aug 25 2005",
			"references": 8,
			"revision": 17,
			"time": "2021-02-27T21:29:45-05:00",
			"created": "2005-09-21T03:00:00-04:00"
		}
	]
}