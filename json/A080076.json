{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A080076",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 80076,
			"data": "3,5,13,17,41,97,113,193,241,257,353,449,577,641,673,769,929,1153,1217,1409,1601,2113,2689,2753,3137,3329,3457,4481,4993,6529,7297,7681,7937,9473,9601,9857,10369,10753,11393,11777,12161,12289,13313",
			"name": "Proth primes: primes of the form k*2^m + 1 with odd k \u003c 2^m, m \u003e= 1.",
			"comment": [
				"Conjecture: a(n) ~ (n log n)^2 / 2. - _Thomas Ordowski_, Oct 19 2014",
				"Named after the French farmer and self-taught mathematician François Proth (1852-1879). - _Amiram Eldar_, Jun 05 2021"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A080076/b080076.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Chris K. Caldwell's The Top Twenty, \u003ca href=\"http://primes.utm.edu/top20/page.php?id=66\"\u003eProth\u003c/a\u003e.",
				"James Grime and Brady Haran, \u003ca href=\"https://www.youtube.com/watch?v=fcVjitaM3LY\"\u003e78557 and Proth Primes\u003c/a\u003e, Numberphile video, 2017.",
				"Max Lewis and Victor Scharaschkin, \u003ca href=\"https://www.emis.de/journals/INTEGERS/papers/q80/q80.Abstract.html\"\u003ek-Lehmer and k-Carmichael Numbers\u003c/a\u003e, Integers, Vol. 16 (2016), #A80.",
				"Rogério Paludo and Leonel Sousa, \u003ca href=\"https://doi.org/10.1109/ASAP52443.2021.00031\"\u003eNumber Theoretic Transform Architecture suitable to Lattice-based Fully-Homomorphic Encryption\u003c/a\u003e, 2021 IEEE 32nd Int'l Conf. Appl.-specific Sys., Architectures and Processors (ASAP) 163-170.",
				"François Proth, \u003ca href=\"https://fr.wikisource.org/wiki/Page:Comptes_rendus_hebdomadaires_des_s%C3%A9ances_de_l%E2%80%99Acad%C3%A9mie_des_sciences,_tome_087,_1878.djvu/932\"\u003eThéorèmes sur les nombres premiers\u003c/a\u003e, Comptes rendus de l'Académie des Sciences de Paris, Vol. 87 (1878), p. 926.",
				"Tsz-Wo Sze, \u003ca href=\"https://arxiv.org/abs/0812.2596\"\u003eDeterministic Primality Proving on Proth Numbers\u003c/a\u003e, arXiv:0812.2596 [math.NT], 2009.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ProthPrime.html\"\u003eProth Prime\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Proth_prime\"\u003eProth prime\u003c/a\u003e."
			],
			"maple": [
				"N:= 20000: # to get all terms \u003c= N",
				"S:= select(isprime, {seq(seq(k*2^m+1, k = 1 .. min(2^m, (N-1)/2^m), 2), m=1..ilog2(N-1))}):",
				"sort(convert(S,list)); # _Robert Israel_, Feb 02 2016"
			],
			"mathematica": [
				"r[p_, n_] := Reduce[p == (2*m + 1)*2^n + 1 \u0026\u0026 2^n \u003e 2*m + 1 \u0026\u0026 n \u003e 0 \u0026\u0026 m \u003e= 0, {a, m}, Integers]; r[p_] := Catch[ Do[ If[ r[p, n] =!= False, Throw[True]], {n, 1, Floor[Log[2, p]]}]]; A080076 = Reap[ Do[ p = Prime[k]; If[ r[p] === True, Sow[p]], {k, 1, 2000}]][[2, 1]] (* _Jean-François Alcover_, Apr 06 2012 *)",
				"nn = 13; Union[Flatten[Table[Select[1 + 2^n Range[1, 2^Min[n, nn - n + 1], 2], # \u003c 2^(nn + 1) \u0026\u0026 PrimeQ[#] \u0026], {n, nn}]]] (* _T. D. Noe_, Apr 06 2012 *)"
			],
			"program": [
				"(PARI) is_A080076(N)=isproth(N)\u0026\u0026isprime(N) \\\\ see A080075 for isproth(). - _M. F. Hasler_, Oct 18 2014"
			],
			"xref": [
				"Cf. A080075.",
				"Cf. A134876 (number of Proth primes), A214120, A239234.",
				"Cf. A248972."
			],
			"keyword": "nonn,changed",
			"offset": "1,1",
			"author": "_Eric W. Weisstein_, Jan 24 2003",
			"references": 18,
			"revision": 65,
			"time": "2022-01-13T12:40:05-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}