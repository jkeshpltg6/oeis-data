{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A291230",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 291230,
			"data": "6,25,96,351,1242,4304,14706,49761,167232,559303,1864110,6197472,20567262,68166713,225713280,746866143,2470077378,8166190192,26990599050,89190984033,294691499808,973574384231,3216160413654,10623856065984,35092075282998,115910575744921",
			"name": "p-INVERT of (0,1,0,1,0,1,...), where p(S) = (1 - S)(1 - 2 S)(1 - 3 S).",
			"comment": [
				"Suppose s = (c(0), c(1), c(2),...) is a sequence and p(S) is a polynomial. Let S(x) = c(0)*x + c(1)*x^2 + c(2)*x^3 + ... and T(x) = (-p(0) + 1/p(S(x)))/x. The p-INVERT of s is the sequence t(s) of coefficients in the Maclaurin series for T(x).  Taking p(S) = 1 - S gives the \"INVERT\" transform of s, so that p-INVERT is a generalization of the \"INVERT\" transform (e.g., A033453).",
				"See A291219 for a guide to related sequences."
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A291230/b291230.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (6, -8, -6, 8, 6, 1)"
			],
			"formula": [
				"G.f.: (-6 + 11 x + 6 x^2 - 11 x^3 - 6 x^4)/(-1 + 6 x - 8 x^2 - 6 x^3 + 8 x^4 + 6 x^5 + x^6).",
				"a(n) = 6*a(n-1) - 8*a(n-2) - 6*a(n-3) + 8*a(n-4) + 6*a(n-5) + a(n-6)  for n \u003e= 7."
			],
			"mathematica": [
				"z = 60; s = x/(1 - x^2); p = (1 - s)(1 - 2 s)(1 - 3 s);",
				"Drop[CoefficientList[Series[s, {x, 0, z}], x], 1]  (* A000035 *)",
				"Drop[CoefficientList[Series[1/p, {x, 0, z}], x], 1]  (* A291230 *)"
			],
			"xref": [
				"Cf. A000035, A291219."
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "_Clark Kimberling_, Aug 25 2017",
			"references": 2,
			"revision": 4,
			"time": "2017-08-25T23:40:04-04:00",
			"created": "2017-08-25T23:40:04-04:00"
		}
	]
}