{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A057145",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 57145,
			"data": "1,1,2,1,3,3,1,4,6,4,1,5,9,10,5,1,6,12,16,15,6,1,7,15,22,25,21,7,1,8,18,28,35,36,28,8,1,9,21,34,45,51,49,36,9,1,10,24,40,55,66,70,64,45,10,1,11,27,46,65,81,91,92,81,55,11,1,12,30,52,75,96,112",
			"name": "Square array of polygonal numbers T(n,k) = ((n-2)*k^2 - (n-4)*k)/2, n \u003e= 2, k \u003e= 1, read by antidiagonals upwards.",
			"comment": [
				"The set of the \"nontrivial\" entries T(n\u003e=3,k\u003e=3) is in A090466. - _R. J. Mathar_, Jul 28 2016",
				"T(n,k) is the smallest number that can be expressed as the sum of k consecutive positive integers that differ by n - 2. In other words: T(n,k) is the sum of k terms of the arithmetic progression with common difference n - 2 and 1st term 1, (see the example). - _Omar E. Pol_, Apr 29 2020"
			],
			"reference": [
				"A. H. Beiler, Recreations in the Theory of Numbers. New York: Dover, p. 189, 1966.",
				"J. H. Conway and R. K. Guy, The Book of Numbers, Springer-Verlag (Copernicus), p. 38, 1996."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A057145/b057145.txt\"\u003eRows n = 2..100, flattened\u003c/a\u003e",
				"Lukas Andritsch, \u003ca href=\"https://arxiv.org/abs/1804.07243\"\u003eBoundary algebra of a GL_m-dimer\u003c/a\u003e, arXiv:1804.07243 [math.RT], 2018.",
				"\u003ca href=\"/index/Pol#polygonal_numbers\"\u003eIndex to sequences related to polygonal numbers\u003c/a\u003e"
			],
			"formula": [
				"T(2n+4,n) = n^3. - Stuart M. Ellerstein (ellerstein(AT)aol.com), Aug 28 2000",
				"T(n, k) = T(n-1, k) + k*(k-1)/2 [with T(2, k)=k] = T(n, k-1) + 1 + (n-2)*(k-1) [with T(n, 0)=0] = k + (n-2)k(k-1)/2 = k + A063212(n-2, k-1). - _Henry Bottomley_, Jul 11 2001",
				"G.f. for row n: x*(1+(n-3)*x)/(1-x)^3, n\u003e=2. - _Paul Barry_, Feb 21 2003",
				"From _Wolfdieter Lang_, Nov 05 2014: (Start)",
				"The triangle is a(n, m) = T(n-m+1, m) = (1/2)*m*(n*(m-1) + 3 - m^2) for n \u003e= 2, m = 1, 2, ..., n-1 and zero elsewhere.",
				"O.g.f. for column m (without leading zeros): (x*binomial(m,2) + (1+2*m-m^2)*(m/2)*(1-x))/(x^(m-1)*(1-x)^2). (End)",
				"T(n,k) = A139600(n-2,k) = A086270(n-2,k). - _R. J. Mathar_, Jul 28 2016",
				"Row sums of A077028: T(n+2,k+1) = Sum_{j=0..k} A077028(n,j), where A077028(n,k) = 1+n*k is the square array interpretation of A077028 (the 1D polygonal numbers). - _R. J. Mathar_, Jul 30 2016"
			],
			"example": [
				"Array T(n k) (n \u003e= 2, k \u003e= 1) begins:",
				"1,  2,  3,  4,   5,   6,   7,   8,   9,  10,  11, ...",
				"1,  3,  6, 10,  15,  21,  28,  36,  45,  55,  66, ...",
				"1,  4,  9, 16,  25,  36,  49,  64,  81, 100, 121, ...",
				"1,  5, 12, 22,  35,  51,  70,  92, 117, 145, 176, ...",
				"1,  6, 15, 28,  45,  66,  91, 120, 153, 190, 231, ...",
				"1,  7, 18, 34,  55,  81, 112, 148, 189, 235, 286, ...",
				"1,  8, 21, 40,  65,  96, 133, 176, 225, 280, 341, ...",
				"1,  9, 24, 46,  75, 111, 154, 204, 261, 325, 396, ...",
				"1, 10, 27, 52,  85, 126, 175, 232, 297, 370, 451, ...",
				"1, 11, 30, 58,  95, 141, 196, 260, 333, 415, 506, ...",
				"1, 12, 33, 64, 105, 156, 217, 288, 369, 460, 561, ...",
				"1, 13, 36, 70, 115, 171, 238, 316, 405, 505, 616, ...",
				"1, 14, 39, 76, 125, 186, 259, 344, 441, 550, 671, ...",
				"-------------------------------------------------------",
				"From _Wolfdieter Lang_, Nov 04 2014: (Start)",
				"The triangle a(k, m) begins:",
				"k\\m 1  2  3  4  5   6   7   8   9  10  11  12 13 14 ...",
				"2:  1",
				"3:  1  2",
				"4:  1  3  3",
				"5:  1  4  6  4",
				"6:  1  5  9 10  5",
				"7:  1  6 12 16 15   6",
				"8:  1  7 15 22 25  21   7",
				"9:  1  8 18 28 35  36  28   8",
				"10: 1  9 21 34 45  51  49  36   9",
				"11: 1 10 24 40 55  66  70  64  45  10",
				"12: 1 11 27 46 65  81  91  92  81  55  11",
				"13: 1 12 30 52 75  96 112 120 117 100  66  12",
				"14: 1 13 33 58 85 111 133 148 153 145 121  78 13",
				"15: 1 14 36 64 95 126 154 176 189 190 176 144 91 14",
				"...",
				"-------------------------------------------------------",
				"a(2,1) = T(2,1), a(6, 3) = T(4, 3). (End)",
				".",
				"From _Omar E. Pol_, May 03 2020: (Start)",
				"Illustration of the corner of the square array:",
				".",
				"  1       2         3           4",
				"  O     O O     O O O     O O O O",
				".",
				"  1       3         6          10",
				"  O     O O     O O O     O O O O",
				"          O       O O       O O O",
				"                    O         O O",
				"                                O",
				".",
				"  1       4         9          16",
				"  O     O O     O O O     O O O O",
				"          O       O O       O O O",
				"          O       O O       O O O",
				"                    O         O O",
				"                    O         O O",
				"                                O",
				"                                O",
				".",
				"  1       5        12          22",
				"  O     O O     O O O     O O O O",
				"          O       O O       O O O",
				"          O       O O       O O O",
				"          O       O O       O O O",
				"                    O         O O",
				"                    O         O O",
				"                    O         O O",
				"                                O",
				"                                O",
				"                                O",
				"(End)"
			],
			"maple": [
				"A057145 := proc(n,k)",
				"    ((n-2)*k^2-(n-4)*k)/2 ;",
				"end proc:",
				"seq(seq(A057145(d-k,k),k=1..d-2),d=3..12); # _R. J. Mathar_, Jul 28 2016"
			],
			"mathematica": [
				"nn = 12; Flatten[Table[k (3 - k^2 - n + k*n)/2, {n, 2, nn}, {k, n - 1}]] (* _T. D. Noe_, Oct 10 2012 *)"
			],
			"program": [
				"(MAGMA) /* As square array: */ t:=func\u003cn,s | (n^2*(s-2)-n*(s-4))/2\u003e; [[t(s,n): s in [1..11]]: n in [2..14]]; // _Bruno Berselli_, Jun 24 2013"
			],
			"xref": [
				"Many rows and columns of this array are in the database.",
				"Cf. A055795 (antidiagonal sums), A064808 (main diagonal)."
			],
			"keyword": "nonn,nice,tabl,easy",
			"offset": "2,3",
			"author": "_N. J. A. Sloane_, Sep 12 2000",
			"ext": [
				"a(50)=49 corrected to a(50)=40 by _Jean-François Alcover_, Jul 22 2011",
				"Edited: Name shortened, offset in Paul Barry's g.f. corrected and Conway-Guy reference added. - _Wolfdieter Lang_, Nov 04 2014"
			],
			"references": 62,
			"revision": 101,
			"time": "2020-05-05T05:31:13-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}