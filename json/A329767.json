{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A329767",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 329767,
			"data": "1,2,0,0,2,2,0,2,2,4,0,2,4,6,4,0,2,2,12,12,4,0,2,6,30,18,8,0,0,2,2,44,44,32,4,0,0,2,6,82,76,74,16,0,0,0,2,4,144,138,172,52,0,0,0,0,2,6,258,248,350,156,4,0,0,0,0,2,2,426,452,734,404,28,0,0,0,0",
			"name": "Triangle read by rows where T(n,k) is the number of binary words of length n \u003e= 0 with runs-resistance k, 0 \u003c= k \u003c= n.",
			"comment": [
				"A composition of n is a finite sequence of positive integers with sum n.",
				"For the operation of taking the sequence of run-lengths of a finite sequence, runs-resistance is defined as the number of applications required to reach a singleton.",
				"Except for the k = 0 column and the n = 0 and n = 1 rows, this is the triangle appearing on page 3 of Lenormand, which is A319411. Unlike A318928, we do not here require that a(n) \u003e= 1.",
				"The n = 0 row is chosen to ensure that the row-sums are A000079, although the empty word arguably has indeterminate runs-resistance."
			],
			"link": [
				"Claude Lenormand, \u003ca href=\"/A318921/a318921.pdf\"\u003eDeux transformations sur les mots\u003c/a\u003e, Preprint, 5 pages, Nov 17 2003."
			],
			"example": [
				"Triangle begins:",
				"   1",
				"   2   0",
				"   0   2   2",
				"   0   2   2   4",
				"   0   2   4   6   4",
				"   0   2   2  12  12   4",
				"   0   2   6  30  18   8   0",
				"   0   2   2  44  44  32   4   0",
				"   0   2   6  82  76  74  16   0   0",
				"   0   2   4 144 138 172  52   0   0   0",
				"   0   2   6 258 248 350 156   4   0   0   0",
				"   0   2   2 426 452 734 404  28   0   0   0   0",
				"For example, row n = 4 counts the following words:",
				"  0000  0011  0001  0010",
				"  1111  0101  0110  0100",
				"        1010  0111  1011",
				"        1100  1000  1101",
				"              1001",
				"              1110"
			],
			"mathematica": [
				"runsres[q_]:=If[Length[q]==1,0,Length[NestWhileList[Length/@Split[#]\u0026,q,Length[#]\u003e1\u0026]]-1];",
				"Table[Length[Select[Tuples[{0,1},n],runsres[#]==k\u0026]],{n,0,10},{k,0,n}]"
			],
			"xref": [
				"Row sums are A000079.",
				"Column k = 2 is A319410.",
				"Column k = 3 is 2 * A329745.",
				"The version for compositions is A329744.",
				"The version for partitions is A329746.",
				"The number of nonzero entries in row n \u003e 0 is A319412(n).",
				"The runs-resistance of the binary expansion of n is A318928.",
				"Cf. A001037, A096365, A225485, A245563, A319411, A325280, A329747, A329750."
			],
			"keyword": "nonn,tabl",
			"offset": "0,2",
			"author": "_Gus Wiseman_, Nov 21 2019",
			"references": 18,
			"revision": 9,
			"time": "2019-11-30T09:10:15-05:00",
			"created": "2019-11-22T16:13:28-05:00"
		}
	]
}