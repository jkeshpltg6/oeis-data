{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A256447",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 256447,
			"data": "2,3,3,7,5,9,6,13,23,9,28,22,12,24,39,37,17,44,32,16,53,37,53,76,46,23,43,20,49,161,48,82,23,142,27,91,90,66,103,97,41,181,41,74,39,228,228,86,45,86,130,44,217,134,141,138,46,148,106,47,261,355,116,53,109,387,166,284,65,119,181,243,198,195,122,190,268,125,265,330,78",
			"name": "Number of integers in range (prime(n)^2)+1 .. (prime(n)*prime(n+1)) whose smallest prime factor is at least prime(n): a(n) = A250477(n) - A250474(n).",
			"comment": [
				"a(n) = number of integers in range [(prime(n)^2)+1, (prime(n) * prime(n+1))] whose smallest prime factor is at least prime(n).",
				"All the terms are strictly positive, because at least for the last number in the range we have A020639(prime(n)*prime(n+1)) = prime(n).",
				"See the conjectures in A256448."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A256447/b256447.txt\"\u003eTable of n, a(n) for n = 1..564\u003c/a\u003e",
				"A. Karttunen, \u003ca href=\"https://oeis.org/plot2a?name1=A256447\u0026amp;name2=A256448\u0026amp;tform1=untransformed\u0026amp;tform2=untransformed\u0026amp;shift=0\u0026amp;radiop1=ratio\u0026amp;drawlines=true\"\u003eRatio a(n)/A256448(n) plotted with OEIS Plot2-script\u003c/a\u003e",
				"A. Karttunen, \u003ca href=\"https://oeis.org/plot2a?name1=A256447\u0026amp;name2=A251723\u0026amp;tform1=untransformed\u0026amp;tform2=untransformed\u0026amp;shift=0\u0026amp;radiop1=ratio\u0026amp;drawlines=true\"\u003eRatio a(n)/A251723(n) plotted with OEIS Plot2-script\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A250477(n) - A250474(n).",
				"a(n) = A251723(n) - A256448(n).",
				"a(n) = A256448(n) + A256449(n).",
				"a(n) = A256468(n) + 1.",
				"Other identities. For all n \u003e= 1:",
				"a(n+1) = A256446(n) - A256448(n)."
			],
			"example": [
				"For n=1, we have in range [(prime(1)^2)+1, (prime(1) * prime(2))], that is, in range [5,6], two numbers, 5 and 6, whose smallest prime factor (A020639) is at least 2, thus a(1) = 2.",
				"For n=2, we have in range [10, 15] three numbers, {11, 13, 15}, whose smallest prime factor is at least 3, thus a(2) = 3.",
				"For n=3, we have in range [26, 35] three numbers, {29, 31, 35}, whose smallest prime factor is at least prime(3) = 5, thus a(3) = 3."
			],
			"mathematica": [
				"f[n_] := Count[Range[Prime[n]^2 + 1, Prime[n] Prime[n + 1]],",
				"  x_ /; Min[First /@ FactorInteger[x]] \u003e=",
				"Prime@n]; Array[f, 81] (* _Michael De Vlieger_, Mar 30 2015 *)"
			],
			"program": [
				"(Scheme) (define (A256447 n) (- (A250477 n) (A250474 n)))"
			],
			"xref": [
				"One more than A256468.",
				"Cf. A020639, A250474, A250477, A251723, A256446, A256448, A256449."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Antti Karttunen_, Mar 29 2015",
			"references": 7,
			"revision": 24,
			"time": "2015-03-30T21:41:04-04:00",
			"created": "2015-03-29T22:58:52-04:00"
		}
	]
}