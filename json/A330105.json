{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A330105",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 330105,
			"data": "1,2,3,4,3,6,7,8,9,6,3,12,13,14,15,16,3,18,19,12,21,6,7,24,9,26,27,28,13,30,3,32,15,6,69,36,37,38,39,24,3,42,13,12,45,14,13,48,49,18,15,52,53,54,15,56,57,26,3,60,37,6,63,64,39,30,3,12,69,138",
			"name": "MM-number of the brute-force normalization of the multiset of multisets with MM-number n.",
			"comment": [
				"We define the brute-force normalization of a multiset of multisets to be obtained by first normalizing so that the vertices cover an initial interval of positive integers, then applying all permutations to the vertex set, and finally taking the least representative, where the ordering of multisets is first by length and then lexicographically.",
				"For example, 15301 is the MM-number of {{3},{1,2},{1,1,4}}, which has the following normalizations together with their MM-numbers:",
				"  Brute-force:    43287: {{1},{2,3},{2,2,4}}",
				"  Lexicographic:  43143: {{1},{2,4},{2,2,3}}",
				"  VDD:            15515: {{2},{1,3},{1,1,4}}",
				"  MM:             15265: {{2},{1,4},{1,1,3}}",
				"A prime index of n is a number m such that prime(m) divides n. The multiset of prime indices of n is row n of A112798. The multiset of multisets with MM-number n is formed by taking the multiset of prime indices of each part of the multiset of prime indices of n. For example, the prime indices of 78 are {1,2,6}, so the multiset of multisets with MM-number 78 is {{},{1},{1,2}}."
			],
			"mathematica": [
				"brute[m_]:=If[Union@@m!={}\u0026\u0026Union@@m!=Range[Max@@Flatten[m]],brute[m/.Rule@@@Table[{(Union@@m)[[i]],i},{i,Length[Union@@m]}]],First[Sort[brute[m,1]]]];",
				"brute[m_,1]:=Table[Sort[Sort/@(m/.Rule@@@Table[{i,p[[i]]},{i,Length[p]}])],{p,Permutations[Union@@m]}];",
				"Table[Map[Times@@Prime/@#\u0026,brute[primeMS/@primeMS[n]],{0,1}],{n,100}]"
			],
			"xref": [
				"This sequence is idempotent and its image/fixed points are A330104.",
				"Non-isomorphic multiset partitions are A007716.",
				"Cf. A000612, A055621, A283877, A316983, A317533, A330098, A330103.",
				"Other normalizations: A330061 (VDD MM), A330101 (brute-force BII), A330102 (VDD BII), A330105 (brute-force MM).",
				"Other fixed points:",
				"- Brute-force: A330104 (multisets of multisets), A330107 (multiset partitions), A330099 (set-systems).",
				"- Lexicographic: A330120 (multisets of multisets), A330121 (multiset partitions), A330110 (set-systems).",
				"- VDD: A330060 (multisets of multisets), A330097 (multiset partitions), A330100 (set-systems).",
				"- MM: A330108 (multisets of multisets), A330122 (multiset partitions), A330123 (set-systems).",
				"- BII: A330109 (set-systems)."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Gus Wiseman_, Dec 02 2019",
			"references": 13,
			"revision": 5,
			"time": "2019-12-03T21:12:53-05:00",
			"created": "2019-12-03T21:12:53-05:00"
		}
	]
}