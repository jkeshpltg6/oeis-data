{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A290575",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 290575,
			"data": "1,4,40,544,8536,145504,2618176,48943360,941244376,18502137184,370091343040,7508629231360,154145664817600,3196100636757760,66834662101834240,1407913577733228544,29849617614785770456,636440695668355742560,13638210075999240396736,293565508750164008207104,6344596821114216520841536",
			"name": "Apéry-like numbers Sum_{k=0..n} (C(n,k) * C(2*k,n))^2.",
			"comment": [
				"Sequence epsilon in Almkvist, Straten, Zudilin article."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A290575/b290575.txt\"\u003eTable of n, a(n) for n = 0..500\u003c/a\u003e",
				"G. Almkvist, D. van Straten, and W. Zudilin, \u003ca href=\"https://doi.org/10.1017/S0013091509000959\"\u003eGeneralizations of Clausen’s formula and algebraic transformations of Calabi-Yau differential equations\u003c/a\u003e, Proc. Edinburgh Math. Soc.54 (2) (2011), 273-295.",
				"Ofir Gorodetsky, \u003ca href=\"https://arxiv.org/abs/2102.11839\"\u003eNew representations for all sporadic Apéry-like sequences, with applications to congruences\u003c/a\u003e, arXiv:2102.11839 [math.NT], 2021. See epsilon p. 3.",
				"Amita Malik and Armin Straub, \u003ca href=\"https://doi.org/10.1007/s40993-016-0036-8\"\u003eDivisibility properties of sporadic Apéry-like numbers\u003c/a\u003e, Research in Number Theory, 2016, 2:5",
				"Zhi-Hong Sun, \u003ca href=\"https://arxiv.org/abs/1803.10051\"\u003eCongruences for Apéry-like numbers\u003c/a\u003e, arXiv:1803.10051 [math.NT], 2018.",
				"Zhi-Hong Sun, \u003ca href=\"https://arxiv.org/abs/2004.07172\"\u003eNew congruences involving Apéry-like numbers\u003c/a\u003e, arXiv:2004.07172 [math.NT], 2020."
			],
			"formula": [
				"a(-1)=0, a(0)=1, a(n+1) = ((2*n+1)*(12*n^2+12*n+4)*a(n)-16*n^3)*a(n-1))/(n+1)^3.",
				"a(n) = Sum_{k=ceiling(n/2)..n} binomial(n,k)^2*binomial(2*k,n)^2. [Gorodetsky] - _Michel Marcus_, Feb 25 2021",
				"a(n) ~ 2^(2*n - 3/4) * (1 + sqrt(2))^(2*n+1) / (Pi*n)^(3/2). - _Vaclav Kotesovec_, Jul 10 2021"
			],
			"mathematica": [
				"Table[Sum[(Binomial[n, k]*Binomial[2*k, n])^2, {k, 0, n}], {n, 0, 25}] (* _G. C. Greubel_, Oct 23 2017 *)"
			],
			"program": [
				"(PARI) C=binomial; a(n) = sum (k=0, n, C(n,k)^2 * C(k+k,n)^2);"
			],
			"xref": [
				"The Apéry-like numbers [or Apéry-like sequences, Apery-like numbers, Apery-like sequences] include A000172, A000984, A002893, A002895, A005258, A005259, A005260, A006077, A036917, A063007, A081085, A093388, A125143 (apart from signs), A143003, A143007, A143413, A143414, A143415, A143583, A183204, A214262, A219692, A226535, A227216, A227454, A229111 (apart from signs), A260667, A260832, A262177, A264541, A264542, A279619, A290575, A290576. (The term \"Apery-like\" is not well-defined.)",
				"Other Apery-like sequences are A000172, A002893, A002895, A005258, A005259, A005260, A006077, A081085, A093388, A125143, A183204, A219692, A229111, A290576.",
				"For primes that do not divide the terms of the sequences A000172, A005258, A002893, A081085, A006077, A093388, A125143, A229111, A002895, A290575, A290576, A005259 see A260793, A291275-A291284 and A133370 respectively."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Hugo Pfoertner_, Aug 06 2017",
			"references": 46,
			"revision": 39,
			"time": "2021-07-10T17:20:45-04:00",
			"created": "2017-08-06T20:26:45-04:00"
		}
	]
}