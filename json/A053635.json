{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A053635",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 53635,
			"data": "0,2,6,12,24,40,84,140,288,540,1080,2068,4224,8216,16548,32880,65856,131104,262836,524324,1049760,2097480,4196412,8388652,16782048,33554600,67117128,134218836,268452240,536870968,1073777040,2147483708,4295033472,8589938808",
			"name": "a(n) = Sum_{d|n} phi(d)*2^(n/d).",
			"comment": [
				"Dirichlet convolution of phi(n) and 2^n. - _Richard L. Ollerton_, May 06 2021"
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A053635/b053635.txt\"\u003eTable of n, a(n) for n = 0..3321\u003c/a\u003e",
				"James East and Ron Niles, \u003ca href=\"https://arxiv.org/abs/1710.11245\"\u003eInteger polygons of given perimeter\u003c/a\u003e, arXiv:1710.11245 [math.CO], 2017.",
				"James East and Ron Niles, \u003ca href=\"https://doi.org/10.1017/S0004972718001612\"\u003eInteger polygons of given perimeter\u003c/a\u003e, Bull. Aust. Math. Soc. 100(1) (2019), 131-147.",
				"T. Pisanski, D. Schattschneider, and B. Servatius, \u003ca href=\"http://www.jstor.org/stable/27642932\"\u003eApplying Burnside's lemma to a one-dimensional Escher problem\u003c/a\u003e, Math. Mag., 79 (2006), 167-180. See v(n)."
			],
			"formula": [
				"a(n) = n * A000031(n).",
				"a(n) = Sum_{k=1..n} 2^gcd(n,k). - _Ilya Gutkovskiy_, Apr 16 2021",
				"a(n) = Sum_{k=1..n} 2^(n/gcd(n,k))*phi(gcd(n,k))/phi(n/gcd(n,k)). - _Richard L. Ollerton_, May 06 2021"
			],
			"maple": [
				"with(numtheory); A053685:=n-\u003eadd( phi(n/d)*2^d, d in divisors(n)); # _N. J. A. Sloane_, Nov 21 2009"
			],
			"mathematica": [
				"a[0] = 0; a[n_] := Sum[EulerPhi[d] 2^(n/d), {d, Divisors[n]}];",
				"Table[a[n], {n, 0, 31}] (* _Jean-François Alcover_, Aug 30 2018 *)"
			],
			"program": [
				"(PARI) a(n) = if (n, sumdiv(n, d, eulerphi(d)*2^(n/d)), 0); \\\\ _Michel Marcus_, Sep 20 2017",
				"(MAGMA) [0] cat  [\u0026+[EulerPhi(d)*2^(n div d): d in Divisors(n)]: n in [1..40]]; // _Vincenzo Librandi_, Jul 20 2019"
			],
			"xref": [
				"Cf. A000031, A053634, A053636. Twice A034738.",
				"Column k=2 of A185651."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Mar 23 2000",
			"references": 16,
			"revision": 43,
			"time": "2021-05-07T00:54:48-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}