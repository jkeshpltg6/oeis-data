{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A156348",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 156348,
			"data": "1,1,1,1,0,1,1,2,0,1,1,0,0,0,1,1,3,3,0,0,1,1,0,0,0,0,0,1,1,4,0,4,0,0,0,1,1,0,6,0,0,0,0,0,1,1,5,0,0,5,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,1,1,6,10,10,0,6,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,1,1,7,0,0,0,0,7,0,0,0,0,0,0",
			"name": "Triangle T(n,k) read by rows. Column of Pascal's triangle interleaved with k-1 zeros.",
			"comment": [
				"The rows of the Pascal triangle are here found as square root parabolas like in the plots at www.divisorplot.com. Central binomial coefficients are found at the square root boundary.",
				"A156348 * A000010 = A156834: (1, 2, 3, 5, 5, 12, 7, 17, 19, 30, 11, ...). - _Gary W. Adamson_, Feb 16 2009",
				"Row sums give A157019."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A156348/b156348.txt\"\u003eRows n = 1..125 of triangle, flattened\u003c/a\u003e",
				"el Houcein el Abdalaoui, Mohamed Dahmoune and Djelloul Ziadi, \u003ca href=\"http://arxiv.org/abs/1301.3751\"\u003eOn the transition reduction problem for finite automata\u003c/a\u003e, arXiv preprint arXiv:1301.3751 [cs.FL], 2013. - From _N. J. A. Sloane_, Feb 12 2013",
				"Jeff Ventrella, \u003ca href=\"http://www.divisorplot.com\"\u003eDivisor Plot\u003c/a\u003e",
				"\u003ca href=\"/index/Pas#Pascal\"\u003eIndex entries for triangles and arrays related to Pascal's triangle\u003c/a\u003e"
			],
			"example": [
				"Table begins:",
				"1",
				"1  1",
				"1  0  1",
				"1  2  0  1",
				"1  0  0  0  1",
				"1  3  3  0  0  1",
				"1  0  0  0  0  0  1",
				"1  4  0  4  0  0  0  1",
				"1  0  6  0  0  0  0  0  1",
				"1  5  0  0  5  0  0  0  0  1",
				"1  0  0  0  0  0  0  0  0  0  1",
				"1  6 10 10  0  6  0  0  0  0  0  1",
				"1  0  0  0  0  0  0  0  0  0  0  0  1",
				"1  7  0  0  0  0  7  0  0  0  0  0  0  1",
				"1  0 15  0 15  0  0  0  0  0  0  0  0  0  1",
				"1  8  0 20  0  0  0  8  0  0  0  0  0  0  0  1",
				"1  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  1",
				"1  9 21  0  0 21  0  0  9  0  0  0  0  0  0  0  0  1",
				"1  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  1",
				"1 10  0 35 35  0  0  0  0 10  0  0  0  0  0  0  0  0  0  1"
			],
			"maple": [
				"A156348 := proc(n,k)",
				"    if k \u003c 1 or k \u003e n then",
				"        return 0 ;",
				"    elif n mod k = 0 then",
				"        binomial(n/k-2+k,k-1) ;",
				"    else",
				"        0 ;",
				"    end if;",
				"end proc: # _R. J. Mathar_, Mar 03 2013"
			],
			"mathematica": [
				"T[n_, k_] := Which[k \u003c 1 || k \u003e n, 0, Mod[n, k] == 0, Binomial[n/k - 2 + k, k - 1], True, 0];",
				"Table[T[n, k], {n, 1, 14}, {k, 1, n}] // Flatten (* _Jean-François Alcover_, Nov 16 2017 *)"
			],
			"program": [
				"(Haskell)  Following Mathar's Maple program.",
				"a156348 n k = a156348_tabl !! (n-1) !! (k-1)",
				"a156348_tabl = map a156348_row [1..]",
				"a156348_row n = map (f n) [1..n] where",
				"   f n k = if r == 0 then a007318 (n' - 2 + k) (k - 1) else 0",
				"           where (n', r) = divMod n k",
				"-- _Reinhard Zumkeller_, Jan 31 2014"
			],
			"xref": [
				"Cf. A007318, A051731,A156834."
			],
			"keyword": "nonn,tabl,easy,look",
			"offset": "1,8",
			"author": "_Mats Granvik_, Feb 08 2009",
			"references": 10,
			"revision": 25,
			"time": "2017-11-16T07:20:41-05:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}