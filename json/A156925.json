{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A156925",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 156925,
			"data": "1,1,1,1,8,-11,-6,1,38,-108,-242,839,-444,-180,1,144,-425,-7382,48451,-96764,-2559,257002,-312444,88344,30240,1,487,720,-130472,1277794,-4193514,-6504496",
			"name": "FP2 polynomials related to the generating functions of the left hand columns of the A156920 triangle.",
			"comment": [
				"The FP2 polynomials appear in the numerators of the GF2 o.g.f.s. of the left hand columns of A156920. The FP2 can be calculated with the formula of the LHC sequence, see A156920, and the formula for the general structure of the generating function GF2, see below.",
				"An appropriate name for the FP2 polynomials seems to be the flower polynomials of the second kind because the zero patterns of these polynomials look like flowers. The zero patterns of the FP2 and the FP1, see A156921, resemble each other closely.",
				"A Maple program that generates for a left hand column with a certain LHCnr its GF2 and FP2 can be found below. LHCnr stands for left hand column number and starts from 1."
			],
			"formula": [
				"G.f.: GF2(z; LHCnr) = FP2(z; LHCnr)/Product_{m=1..LHCnr} (1-m*z)^(LHCnr-m+1).",
				"Row sum(n+1) = (-1)^(n)*2*(n+1)!*Row sum(n); Row sum(n=0) = 1."
			],
			"example": [
				"The first few rows of the \"triangle\" of the coefficients of the FP2 polynomials.",
				"In the columns the coefficients of the powers of z^m, m=0,1,2,..., appear.",
				"  [1]",
				"  [1, 1]",
				"  [1, 8, -11, -6]",
				"  [1, 38, -108, -242, 839, -444, -180]",
				"  [1, 144, -425, -7382, 48451, -96764, -2559, 257002, -312444, 88344, 30240]",
				"Matrix of the coefficients of the FP2 polynomials. The coefficients in the columns of this matrix are the powers of z^m, m=0,1,2,...",
				"  [1, 0, 0, 0, 0, 0, 0]",
				"  [1, 1, 0, 0, 0, 0, 0]",
				"  [1, 8 , -11, -6, 0, 0, 0]",
				"  [1, 38, -108, -242, 839, -444, -180]",
				"The first few FP2 polynomials are:",
				"  FP2(z; LHCnr = 1) = 1",
				"  FP2(z; LHCnr = 2) = (1+z)",
				"  FP2(z; LHCnr = 3) = 1+8*z-11*z^2-6*z^3",
				"Some GF2(z;LHCnr) are:",
				"  GF2(z; LHCnr = 3) = (1+8*z-11*z^2-6*z^3)/((1-z)^3*(1-2*z)^2*(1-3*z))",
				"  GF2(z; LHCnr = 4) = (1+38*z-108*z^2-242*z^3+839*z^4-444*z^5-180*z^6)/((1-z)^4*(1-2*z)^3*(1-3*z)^2*(1-4*z))"
			],
			"maple": [
				"LHCnr:=5; LHCmax:=(LHCnr)*(LHCnr-1)/2: RHCend:=LHCnr+LHCmax: for k from LHCnr to RHCend do for n from 0 to k do S2[k,n]:=sum((-1)^(n+i)*binomial(n,i)*i^k/n!,i=0..n) end do: G(k,x):= sum(S2[k,p]*((2*p)!/p!)*x^p/(1-4*x)^(p+1),p=0..k)/ (((-1)^(k+1)*2*x)/(-1+4*x)^(k+1)): fx:=simplify(G(k,x)): nmax:=degree(fx); for n from 0 to nmax do d[n]:= coeff(fx,x,n)/2^n end do: LHC[n]:=d[LHCnr-1] end do: a:=n-\u003e LHC[n]: seq(a(n), n=LHCnr..RHCend); for nx from 0 to LHCmax do num:=sort(sum(A[t]*z^t,t=0..LHCmax)): nom:=product((1-u*z)^(LHCnr-u+1),u=1..LHCnr); LHCb:=series(num/nom,z,nx+1); y:=coeff(LHCb,z,nx)-A[nx]; x:=LHC[LHCnr+nx]; A[nx]:=x-y; end do: FP2[LHCnr]:=sort(num,z, ascending); GenFun[LHCnr]:= FP2[LHCnr]/ product((1-m*z)^(LHCnr-m+1), m=1..LHCnr);"
			],
			"xref": [
				"Cf. A156920, A156921, A156927, A156933.",
				"For the first few GF2's see A050488, A142965, A142966 and A142968.",
				"Row sums(n) = A156926(n).",
				"The number of FP2 terms follow the 'Lazy Caterer's sequence' A000124.",
				"For the polynomials in the denominators of the GF2(z;LHCnr) see A157703."
			],
			"keyword": "sign,tabf,more,uned",
			"offset": "0,5",
			"author": "_Johannes W. Meijer_, Feb 20 2009",
			"references": 19,
			"revision": 19,
			"time": "2021-05-19T00:22:59-04:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}