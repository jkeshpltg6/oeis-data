{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A069072",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 69072,
			"data": "6,60,210,504,990,1716,2730,4080,5814,7980,10626,13800,17550,21924,26970,32736,39270,46620,54834,63960,74046,85140,97290,110544,124950,140556,157410,175560,195054,215940,238266,262080,287430,314364,342930",
			"name": "a(n) = (2n+1)*(2n+2)*(2n+3).",
			"comment": [
				"Terms are areas of primitive Pythagorean triangles whose odd sides differ by 2; e.g., the triangle with sides 8,15,17 has area 60. - _Lekraj Beedassy_, Apr 18 2003",
				"Using (n, n+1), (n, n+2), and (n+1, n+2) to generate three unreduced Pythagorean triangles gives a sum of the areas for all three to be (2*n+1)*(2*n+2)*(2*n+3), which are three consecutive numbers. - _J. M. Bergot_, Aug 22 2011"
			],
			"reference": [
				"T. J. I'a. Bromwich, Introduction to the Theory of Infinite Series, Macmillan, 2nd. ed. 1949, p. 190.",
				"Jolley, Summation of Series, Oxford (1961).",
				"Konrad Knopp, Theory and application of infinite series, Dover, p. 269."
			],
			"link": [
				"M. Janjic and B. Petkovic, \u003ca href=\"http://arxiv.org/abs/1301.4550\"\u003eA Counting Function\u003c/a\u003e, arXiv 1301.4550, 2013",
				"Konrad Knopp, \u003ca href=\"http://name.umdl.umich.edu/ACM1954.0001.001\"\u003eTheorie und Anwendung der unendlichen Reihen\u003c/a\u003e, Berlin, J. Springer, 1922. (Original german edition of \"Theory and Application of Infinite Series\")",
				"S. Ramanujan, \u003ca href=\"http://www.imsc.res.in/~rao/ramanujan/NoteBooks/NoteBook2/chapterII/page1.htm\"\u003eNotebook entry\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-6,4,-1)."
			],
			"formula": [
				"log(2) - 1/2 = sum_{n\u003e=0}, 1/a(n); (1/2)*(1-log(2)) = sum_{n\u003e=0} (-1)^n/a(n). [Jolley eq 236 and 237]",
				"sum_{n\u003e=0} x^n/a(n) = ((1+x)/sqrt(x)*log((1+sqrt x)/(1-sqrt x)) + 2*log(1-x)-2)/(4x). [Jolley eq 280 for 0\u003cx\u003c1]",
				"sum_{n\u003e=0} (-x)^n/a(n) = (1-log(1+x) -(1-x)/sqrt(x)*arctan(x))/(2x). [Jolley eq 281 for 0\u003cx\u003c=1]",
				"a(n) = 6*A000447(n+1). - _Lekraj Beedassy_, Apr 18 2003",
				"G.f.: 6*(1 + 6*x + x^2) / (x-1)^4 . - _R. J. Mathar_, Jun 09 2013",
				"a(0)=6, a(1)=60, a(2)=210, a(3)=504, a(n) = 4*a(n-1) - 6*a(n-2) + 4*a(n-3) - a(n-4). - _Harvey P. Dale_, Dec 08 2013",
				"a(n) = 2*A035328(n+1). - _J. M. Bergot_, Jan 02 2015"
			],
			"mathematica": [
				"Array[Times@@(2#+{1,2,3})\u0026,40,0] (* or *) LinearRecurrence[{4,-6,4,-1},{6,60,210,504},40] (* _Harvey P. Dale_, Dec 08 2013 *)"
			],
			"program": [
				"(PARI) a(n)=(2*n+1)*(2*n+2)*(2*n+3) \\\\ _Charles R Greathouse IV_, Oct 07 2015"
			],
			"xref": [
				"Cf. A097321, A069140."
			],
			"keyword": "easy,nonn",
			"offset": "0,1",
			"author": "_Benoit Cloitre_, Apr 05 2002",
			"references": 8,
			"revision": 44,
			"time": "2015-10-07T12:22:10-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}