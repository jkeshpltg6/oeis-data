{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A137251",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 137251,
			"data": "1,1,1,1,3,1,1,6,7,1,1,10,26,15,1,1,15,71,98,31,1,1,21,161,425,342,63,1,1,28,322,1433,2285,1138,127,1,1,36,588,4066,11210,11413,3670,255,1,1,45,1002,10165,44443,79781,54073,11586,511,1,1,55,1617,23056,150546,434638,528690,246409,36038,1023,1",
			"name": "Triangle T(n,k) read by rows: number of k X k triangular matrices with nonnegative integer entries and without zero rows or columns such that sum of all entries is equal to n, n\u003e=1, 1\u003c=k\u003c=n.",
			"comment": [
				"Row sums are A022493.",
				"Number of ascent sequences of length n with k-1 ascents, see example. [_Joerg Arndt_, Nov 03 2012]",
				"Number of interval orders on n elements having exactly k maximal antichains. Also, number of interval orders on n elements having an interval representation with k distinct endpoints, but not with k-1 distinct endpoints. Also, number of interval orders on n elements whose elements define k distinct strict down-sets (a strict down-set defined by an element x of a poset (P,\u003c) is the set {y in P: y\u003cx}). See Fishburn, Chapter 2.3. - _Vít Jelínek_, Sep 06 2014"
			],
			"reference": [
				"Peter C. Fishburn, Interval Orders and Interval Graphs: Study of Partially Ordered Sets, John Wiley \u0026 Sons, 1985."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A137251/b137251.txt\"\u003eRows n = 1..141, flattened\u003c/a\u003e (Rows n = 1..15 from Joerg Arndt)",
				"Mireille Bousquet-Mélou, Anders Claesson, Mark Dukes, Sergey Kitaev, \u003ca href=\"http://arxiv.org/abs/0806.0666\"\u003e(2+2)-free posets, ascent sequences and pattern avoiding permutations\u003c/a\u003e, arXiv:0806.0666 [math.CO]",
				"William Y. C. Chen, Alvin Y.L. Dai, Theodore Dokos, Tim Dwyer and Bruce E. Sagan, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v20i1p76\"\u003eOn 021-Avoiding Ascent Sequences, The Electronic Journal of Combinatorics\u003c/a\u003e Volume 20, Issue 1 (2013), #P76.",
				"M. Dukes, V Jelínek, M. Kubitzke \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v18i1p44\"\u003e Composition Matrices, (2+2)-Free Posets and their Specializations\u003c/a\u003e, Electronic Journal of Combinatorics, Volume 18, Issue 1, 2011, Paper #P44.",
				"Vít Jelínek, \u003ca href=\"http://dx.doi.org/10.1016/j.jcta.2011.11.010\"\u003eCounting general and self-dual interval orders\u003c/a\u003e, Journal of Combinatorial Theory, Series A, Volume 119, Issue 3, April 2012, pp. 599-614; \u003ca href=\"http://arxiv.org/abs/1106.2261\"\u003earXiv preprint\u003c/a\u003e."
			],
			"formula": [
				"G.f.: Sum_{n,k\u003e=1} T(n,k)x^n y^k = Sum_{n\u003e=1} y^n Prod_{i=1..n} (1-(1-x)^i)/(y+(1-x)^i-y*(1-x)^i). See Jelínek's paper, Corollary 2.5. - _Vít Jelínek_, Sep 06 2014"
			],
			"example": [
				"Triangle starts:",
				"01:  1,",
				"02:  1, 1,",
				"03:  1, 3, 1,",
				"04:  1, 6, 7, 1,",
				"05:  1, 10, 26, 15, 1,",
				"06:  1, 15, 71, 98, 31, 1,",
				"07:  1, 21, 161, 425, 342, 63, 1,",
				"08:  1, 28, 322, 1433, 2285, 1138, 127, 1,",
				"09:  1, 36, 588, 4066, 11210, 11413, 3670, 255, 1,",
				"10:  1, 45, 1002, 10165, 44443, 79781, 54073, 11586, 511, 1,",
				"11:  1, 55, 1617, 23056, 150546, 434638, 528690, 246409, 36038, 1023, 1,",
				"12:  1, 66, 2497, 48400, 451515, 1968580, 3895756, 3316193, 1090517, 110930, 2047, 1,",
				"...",
				"From _Joerg Arndt_, Nov 03 2012: (Start)",
				"The 53 ascent sequences of length 5 together with their numbers of ascents are (dots for zeros):",
				"01:  [ . . . . . ]   0      28:  [ . 1 1 . 1 ]   2",
				"02:  [ . . . . 1 ]   1      29:  [ . 1 1 . 2 ]   2",
				"03:  [ . . . 1 . ]   1      30:  [ . 1 1 1 . ]   1",
				"04:  [ . . . 1 1 ]   1      31:  [ . 1 1 1 1 ]   1",
				"05:  [ . . . 1 2 ]   2      32:  [ . 1 1 1 2 ]   2",
				"06:  [ . . 1 . . ]   1      33:  [ . 1 1 2 . ]   2",
				"07:  [ . . 1 . 1 ]   2      34:  [ . 1 1 2 1 ]   2",
				"08:  [ . . 1 . 2 ]   2      35:  [ . 1 1 2 2 ]   2",
				"09:  [ . . 1 1 . ]   1      36:  [ . 1 1 2 3 ]   3",
				"10:  [ . . 1 1 1 ]   1      37:  [ . 1 2 . . ]   2",
				"11:  [ . . 1 1 2 ]   2      38:  [ . 1 2 . 1 ]   3",
				"12:  [ . . 1 2 . ]   2      39:  [ . 1 2 . 2 ]   3",
				"13:  [ . . 1 2 1 ]   2      40:  [ . 1 2 . 3 ]   3",
				"14:  [ . . 1 2 2 ]   2      41:  [ . 1 2 1 . ]   2",
				"15:  [ . . 1 2 3 ]   3      42:  [ . 1 2 1 1 ]   2",
				"16:  [ . 1 . . . ]   1      43:  [ . 1 2 1 2 ]   3",
				"17:  [ . 1 . . 1 ]   2      44:  [ . 1 2 1 3 ]   3",
				"18:  [ . 1 . . 2 ]   2      45:  [ . 1 2 2 . ]   2",
				"19:  [ . 1 . 1 . ]   2      46:  [ . 1 2 2 1 ]   2",
				"20:  [ . 1 . 1 1 ]   2      47:  [ . 1 2 2 2 ]   2",
				"21:  [ . 1 . 1 2 ]   3      48:  [ . 1 2 2 3 ]   3",
				"22:  [ . 1 . 1 3 ]   3      49:  [ . 1 2 3 . ]   3",
				"23:  [ . 1 . 2 . ]   2      50:  [ . 1 2 3 1 ]   3",
				"24:  [ . 1 . 2 1 ]   2      51:  [ . 1 2 3 2 ]   3",
				"25:  [ . 1 . 2 2 ]   2      52:  [ . 1 2 3 3 ]   3",
				"26:  [ . 1 . 2 3 ]   3      53:  [ . 1 2 3 4 ]   4",
				"27:  [ . 1 1 . . ]   1",
				"There is 1 ascent sequence with no ascent, 10 with one ascent, etc., giving the fourth row [1, 10, 26, 15, 1].",
				"(End)"
			],
			"maple": [
				"b:= proc(n, i, t) option remember; local j; if n\u003c1 then [0$t, 1]",
				"      else []; for j from 0 to t+1 do zip((x, y)-\u003ex+y, %,",
				"      b(n-1, j, t+`if`(j\u003ei, 1, 0)), 0) od; % fi",
				"    end:",
				"T:= n-\u003e b(n-1, 0, 0)[]:",
				"seq(T(n), n=1..12);  # _Alois P. Heinz_, May 20 2013"
			],
			"mathematica": [
				"zip[f_, x_List, y_List, z_] := With[{m = Max[Length[x], Length[y]]}, f[PadRight[x, m, z], PadRight[y, m, z]]]; b[n_, i_, t_] := b[n, i, t] = Module[{j, pc}, If[n\u003c1, Append[Array[0\u0026, t], 1], pc = {}; For[j = 0, j \u003c= t+1, j++, pc = zip[Plus, pc, b[n-1, j, t+If[j\u003ei, 1, 0]], 0]]; pc]]; T[n_] := b[n-1, 0, 0]; Table[T[n], {n, 1, 12}] // Flatten (* _Jean-François Alcover_, Jan 29 2014, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A022493 (number of ascent sequences), A218577 (ascent sequences with maximal element k), A175579 (ascent sequences with k zeros)."
			],
			"keyword": "nonn,tabl",
			"offset": "1,5",
			"author": "_Vladeta Jovovic_, Mar 11 2008",
			"references": 12,
			"revision": 47,
			"time": "2017-05-13T15:37:58-04:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}