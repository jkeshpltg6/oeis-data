{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A320874",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 320874,
			"data": "170693941183817,170693941183933,170693941183949,170693941183981,170693941183979,170693941183951,170693941183847,170693941183903,170693941183891,170693941183859,170693941184023,170693941183907,170693941183993,170693941183937,170693941183861,170693941183889",
			"name": "Lexicographically first 4 X 4 pandiagonal magic square made of consecutive primes.",
			"comment": [
				"This is also the 4 X 4 pandiagonal magic square made of consecutive primes which has the smallest possible magic constant (= sum), 682775764735680 = A256234(1). (In the present case there is no other non-equivalent pandiagonal 4 X 4 magic square having the same magic sum, but this could be possible as for rows 7 and 8 of A320872.)",
				"There exist many non-pandiagonal 4 X 4 magic squares composed of consecutive primes with much smaller magic constant, the smallest being A073520(4) = 258.",
				"Pandiagonal means that not only the 2 main diagonals, but also the 6 other \"broken\" diagonals all have the same sum, Sum_{i=1..4} A[i,M4(k +- i)] = 682775764735680 for k = 1, ..., 4 and M4(x) = y in {1, ..., 4} such that y == x (mod 4).",
				"A pandiagonal magic square allows rotations (but not arbitrary cyclic permutations like, e.g., 1 -\u003e 3 -\u003e 4 -\u003e 1) of columns or rows, as well as reflection on the 4 symmetry axes of the square (which also produce rotations of 90 degrees around the center of the square). Among all these variants of this square, there is none with elements coming earlier than (170693941183817, 170693941183933, ...), cf. PROGRAM for explicit verification.",
				"The same 4 X 4 primes are given in increasing order in sequence A245721. But does not give more information than smallest term, the central term, or the magic constant itself (cf. A256234) which uniquely determines the sequence of primes (cf. PARI code) since they have to be consecutive and their sum is equal to 4 times the magic constant. The present sequence gives the full information about the magic square, and the given PARI code allows the production of all \"equivalent\" variants of the square."
			],
			"reference": [
				"Allan W. Johnson, Jr., Journal of Recreational Mathematics, vol. 23:3, 1991, pp. 190-191.",
				"Clifford A. Pickover, The Zen of Magic Squares, Circles and Stars: An Exhibition of Surprising Structures across Dimensions, Princeton University Press, 2002."
			],
			"link": [
				"Harvey Heinz, \u003ca href=\"http://www.magic-squares.net/primesqr.htm\"\u003ePrime Magic Squares\u003c/a\u003e",
				"\u003ca href=\"/index/Mag#magic\"\u003eIndex entries for sequences related to magic squares\u003c/a\u003e"
			],
			"example": [
				"The magic square is",
				"  [ 170693941183817 170693941183933 170693941183949 170693941183981 ]",
				"  [ 170693941183979 170693941183951 170693941183847 170693941183903 ]",
				"  [ 170693941183891 170693941183859 170693941184023 170693941183907 ]",
				"  [ 170693941183993 170693941183937 170693941183861 170693941183889 ]"
			],
			"program": [
				"(PARI) /* the following transformation operators for matrices, together with transposition, allow to produce all (24 for n=4) variants of a (pandiagonal) magic square */",
				"REV(M)=matconcat(Vecrev(M)) \\\\ reverse the order of columns of M",
				"FLIP(M)=matconcat(Colrev(M)) \\\\ reverse the order of rows of M",
				"ROT(M,k=1)=matconcat([M[,k+1..#M],M[,1..k]]) \\\\ rotate left by k (default: 1) columns",
				"ALL(M)=Set(concat(apply(M-\u003evector(#M,k,ROT(M,k)),[M,M~,REV(M),REV(M~),FLIP(M),FLIP(M~)]))) \\\\ PARI orders the set according to the (first) columns of the matrices, so one must take the transpose to get them ordered according to elements of the first row.",
				"\\\\ The set of primes is A245721=MagicPrimes(682775764735680,4), cf. A073519."
			],
			"xref": [
				"Cf. A073519 and A320873, A073521, A073522 (3 X 3, 4 X 4 and 5 X 5 consecutive primes), A073523 and A320876 (6 X 6 consecutive primes, pandiagonal magic square).",
				"Cf. A210710: Minimal index of a Stanley antimagic square of order n consisting of distinct primes.",
				"Cf. A073520: Smallest magic sum for an n^2 magic square made of consecutive primes.",
				"Cf. A104157: Smallest of n X n consecutive primes forming a magic square.",
				"Cf. A256234: Magic sums of 4 X 4 pandiagonal magic squares of consecutive primes."
			],
			"keyword": "nonn,fini,full,changed",
			"offset": "1,1",
			"author": "_M. F. Hasler_, Oct 22 2018",
			"references": 3,
			"revision": 20,
			"time": "2022-01-07T19:34:35-05:00",
			"created": "2018-10-24T09:49:16-04:00"
		}
	]
}