{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A102537",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 102537,
			"data": "1,1,3,1,8,12,1,15,55,55,1,24,156,364,273,1,35,350,1400,2380,1428,1,48,680,4080,11628,15504,7752,1,63,1197,9975,41895,92169,100947,43263,1,80,1960,21560,123970,396704,708400,657800,246675,1,99,3036,42504",
			"name": "Triangle T(n,k) read by rows: 1/n * C(2n+k,k-1) * C(n,k).",
			"comment": [
				"Number of dissections of a convex (2n+2)-gon by k-1 noncrossing diagonals into (2j+2)-gons, 1 \u003c= j \u003c= n-1.",
				"Apparently, a signed, refined version of this array is given on page 65 of the Einziger link, related to the antipode of a Hopf algebra. - _Tom Copeland_, May 19 2015",
				"The f-vectors of the simplicial noncrossing hypertree complexes of McCammond (p. 15). The reduced Euler characteristics are the signed Catalan numbers A000108. - _Tom Copeland_, May 19 2017"
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A102537/b102537.txt\"\u003eTable of n, a(n) for n = 1..11325\u003c/a\u003e (rows 1 \u003c= n \u003c= 150).",
				"H. Einziger, \u003ca href=\"https://search.proquest.com/openview/9a6007300d492143210f01408f4f0e70/1?pq-origsite=gscholar\u0026amp;cbl=18750\u0026amp;diss=y\"\u003eIncidence Hopf algebras: Antipodes, forest formulas, and noncrossing partitions\u003c/a\u003e, Dissertation (2010), George Washington University.",
				"J. McCammond, \u003ca href=\"http://web.math.ucsb.edu/~jon.mccammond/papers/index.html\"\u003eNoncrossing Hypertrees\u003c/a\u003e, 2015.",
				"Jean-Christophe Novelli and Jean-Yves Thibon, \u003ca href=\"http://arxiv.org/abs/1403.5962\"\u003eHopf Algebras of m-permutations,(m+1)-ary trees, and m-parking functions\u003c/a\u003e, arXiv preprint arXiv:1403.5962 [math.CO], 2014.",
				"Jean-Christophe Novelli and Jean-Yves Thibon, \u003ca href=\"https://arxiv.org/abs/2106.08257\"\u003eNoncommutative Symmetric Functions and Lagrange Inversion II: Noncrossing partititions and the Farahat-Higman algebra\u003c/a\u003e, arXiv:2106.08257 [math.CO], 2021.",
				"E. Tzanaki, \u003ca href=\"https://arxiv.org/abs/math/0501100\"\u003ePolygon dissections and some generalizations of cluster complexes\u003c/a\u003e, arXiv:math/0501100 [math.CO], 2005."
			],
			"example": [
				"Triangle begins",
				"  1;",
				"  1,  3;",
				"  1,  8,   12;",
				"  1, 15,   55,    55;",
				"  1, 24,  156,   364,    273;",
				"  1, 35,  350,  1400,   2380,   1428;",
				"  1, 48,  680,  4080,  11628,  15504,   7752;",
				"  1, 63, 1197,  9975,  41895,  92169, 100947,  43263;",
				"  1, 80, 1960, 21560, 123970, 396704, 708400, 657800, 246675;"
			],
			"mathematica": [
				"Table[1/n*Binomial[2 n + k, k - 1] Binomial[n, k], {n, 10}, {k, n}] // Flatten (* _Michael De Vlieger_, May 20 2017 *)"
			],
			"program": [
				"(MAGMA) [[1/n * Binomial(2*n+k,k-1) * Binomial(n,k): k in [1..n]]: n in [1.. 15]]; // _Vincenzo Librandi_, May 20 2015"
			],
			"xref": [
				"Left-hand columns include A005563. Right-hand columns include essentially A001764 and A013698.",
				"Row sums are in A003168.",
				"Cf. A000108.",
				"Cf. A243662 for rows reversed."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Ralf Stephan_, Jan 14 2005",
			"references": 6,
			"revision": 42,
			"time": "2021-10-05T22:13:12-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}