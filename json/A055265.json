{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A055265",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 55265,
			"data": "1,2,3,4,7,6,5,8,9,10,13,16,15,14,17,12,11,18,19,22,21,20,23,24,29,30,31,28,25,34,27,26,33,38,35,32,39,40,43,36,37,42,41,48,49,52,45,44,53,50,47,54,55,46,51,56,57,70,61,66,65,62,69,58,73,64,63,68,59,72,67,60",
			"name": "a(n) is the smallest positive integer not already in the sequence such that a(n)+a(n-1) is prime, starting with a(1)=1.",
			"comment": [
				"The sequence is well-defined (the terms must alternate in parity, and by Dirichlet's theorem a(n+1) always exists). - _N. J. A. Sloane_, Mar 07 2017",
				"Does every positive integer eventually occur? - _Dmitry Kamenetsky_, May 27 2009. Reply from _Robert G. Wilson v_, May 27 2009: The answer is almost certainly yes, on probabilistic grounds.",
				"It appears that this is the limit of the rows of A051237. That those rows do approach a limit seems certain, and given that that limit exists, that this sequence is the limit seems even more likely, but no proof is known for either conjecture. - _Robert G. Wilson v_, Mar 11 2011, edited by _Franklin T. Adams-Watters_, Mar 17 2011",
				"The sequence is also a particular case of \"among the pairwise sums of any M consecutive terms, N are prime\", with M = 2, N = 1. For other M, N see A055266 \u0026 A253074 (M = 2, N = 0), A329333, A329405 - A329416, A329449 - A329456, A329563 - A329581, and the OEIS Wiki page. - _M. F. Hasler_, Feb 11 2020"
			],
			"link": [
				"Zak Seidov, \u003ca href=\"/A055265/b055265.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (First 1000 terms from T. D. Noe)",
				"N. J. A. Sloane, \u003ca href=\"/A055265/a055265.txt\"\u003eTable of n, a(n) for n = 1..100000\u003c/a\u003e (computed using Orlovsky's Mma program)",
				"M. F. Hasler, \u003ca href=\"/wiki/User:M._F._Hasler/Prime_sums_from_neighboring_terms\"\u003ePrime sums from neighboring terms\u003c/a\u003e, OEIS Wiki, Nov. 23, 2019",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"a(2n-1) = A128280(2n-1) - 1, a(2n) = A128280(2n) + 1, for all n \u003e= 1. - _M. F. Hasler_, Feb 11 2020"
			],
			"example": [
				"a(5) = 7 because 1, 2, 3 and 4 have already been used and neither 4 + 5 = 9 nor 4 + 6 = 10 are prime while 4 + 7 = 11 is prime."
			],
			"maple": [
				"A055265 := proc(n)",
				"    local a,i,known ;",
				"    option remember;",
				"    if n =1 then",
				"        1;",
				"    else",
				"        for a from 1 do",
				"            known := false;",
				"            for i from 1 to n-1 do",
				"                if procname(i) = a then",
				"                    known := true;",
				"                    break;",
				"                end if;",
				"            end do:",
				"            if not known and isprime(procname(n-1)+a) then",
				"                return a;",
				"            end if;",
				"        end do:",
				"    end if;",
				"end proc:",
				"seq(A055265(n),n=1..100) ; # _R. J. Mathar_, Feb 25 2017"
			],
			"mathematica": [
				"f[s_List] := Block[{k = 1, a = s[[ -1]]}, While[ MemberQ[s, k] || ! PrimeQ[a + k], k++ ]; Append[s, k]]; Nest[f, {1}, 71] (* _Robert G. Wilson v_, May 27 2009 *)",
				"q=2000; a={1}; z=Range[2,2*q]; While[Length[z]\u003eq-1, k=1; While[!PrimeQ[z[[k]]+Last[a]], k++]; AppendTo[a,z[[k]]]; z=Delete[z,k]]; Print[a] (*200 times faster*) (* _Vladimir Joseph Stephan Orlovsky_, May 03 2011 *)"
			],
			"program": [
				"(HP 50G Calculator) \u003c\u003c DUPDUP + 2 -\u003e N M L \u003c\u003c { 1 } 1 N 1 - FOR i L M FOR j DUP j POS NOT IF THEN j DUP 'L' STO M 'j' STO END NEXT OVER i GET SWAP WHILE DUP2 + DUP ISPRIME? NOT REPEAT DROP DO 1 + 3 PICK OVER POS NOT UNTIL END END ROT DROP2 + NEXT \u003e\u003e \u003e\u003e _Gerald Hillier_, Oct 28 2008",
				"(Haskell)",
				"import Data.List (delete)",
				"a055265 n = a055265_list !! (n-1)",
				"a055265_list = 1 : f 1 [2..] where",
				"   f x vs = g vs where",
				"     g (w:ws) = if a010051 (x + w) == 1",
				"                   then w : f w (delete w vs) else g ws",
				"-- _Reinhard Zumkeller_, Feb 14 2013",
				"(PARI) v=[1];n=1;while(n\u003c50,if(isprime(v[#v]+n)\u0026\u0026!vecsearch(vecsort(v),n), v=concat(v,n);n=0);n++);v \\\\ _Derek Orr_, Jun 01 2015",
				"(PARI) U=-a=1; vector(100,k, k=valuation(1+U+=1\u003c\u003ca, 2); while(bittest(U,k)|| !isprime(a+k), k++); a=k) \\\\ _M. F. Hasler_, Feb 11 2020"
			],
			"xref": [
				"Inverse permutation: A117922; fixed points: A117925; A117923=a(a(n)). - _Reinhard Zumkeller_, Apr 03 2006",
				"Cf. A036440, A051237, A051239, A055266, A088643. A010051.",
				"Cf. A086527 (the primes a(n)+a(n-1)).",
				"Cf. A070942 (n's such that a(1..n) is a permutation of (1..n)). - _Zak Seidov_, Oct 19 2011",
				"See also A076990, A243625.",
				"See A282695 for deviation from identity sequence.",
				"A073659 is a version where the partial sums must be primes."
			],
			"keyword": "easy,nice,nonn",
			"offset": "1,2",
			"author": "_Henry Bottomley_, May 09 2000",
			"ext": [
				"Corrected by _Hans Havermann_, Sep 24 2002"
			],
			"references": 39,
			"revision": 115,
			"time": "2020-03-30T18:37:43-04:00",
			"created": "2000-06-15T03:00:00-04:00"
		}
	]
}