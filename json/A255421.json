{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A255421",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 255421,
			"data": "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,23,19,20,21,25,22,24,26,27,28,29,34,37,30,31,32,36,33,41,35,38,39,43,40,47,42,49,52,53,44,45,46,51,48,61,57,50,54,55,59,67,56,71,64,58,66,70,72,97,60,62,63,77,69,83,65,81",
			"name": "Permutation of natural numbers: a(1) = 1, a(p_n) = ludic(1+a(n)), a(c_n) = nonludic(a(n)), where p_n = n-th prime, c_n = n-th composite number and ludic = A003309, nonludic = A192607.",
			"comment": [
				"This can be viewed as yet another \"entanglement permutation\", where two pairs of complementary subsets of natural numbers are interwoven with each other. In this case a complementary pair ludic/nonludic numbers (A003309/A192607) is intertwined with a complementary pair prime/composite numbers (A000040/A002808)."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A255421/b255421.txt\"\u003eTable of n, a(n) for n = 1..8192\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"a(1) = 1, and for n \u003e 1, if A010051(n) = 1 [i.e. when n is a prime], a(n) = A003309(1+a(A000720(n))), otherwise a(n) = A192607(a(A065855(n))).",
				"As a composition of other permutations:",
				"a(n) = A237126(A246377(n)).",
				"Other identities.",
				"a(A007097(n)) = A255420(n). [Maps iterates of primes to the iterates of Ludic numbers.]"
			],
			"example": [
				"When n = 19 = A000040(8) [the eighth prime], we look for the value of a(8), which is 8 [all terms less than 19 are fixed because the beginnings of A003309 and A008578 coincide up to A003309(8) = A008578(8) = 17], and then take the eighth ludic number larger than 1, which is A003309(1+8) = 23, thus a(19) = 23.",
				"When n = 20 = A002808(11) [the eleventh composite], we look for the value of a(11), which is 11 [all terms less than 19 are fixed, see above], and then take the eleventh nonludic number, which is A192607(11) = 19, thus a(20) = 19.",
				"When n = 30 = A002808(19) [the 19th composite], we look for the value of a(19), which is 23 [see above], and then take the 23rd nonludic number, which is A192607(23) = 34, thus a(30) = 34."
			],
			"program": [
				"(Scheme, with memoization-macro definec)",
				"(definec (A255421 n) (cond ((= 1 n) n) ((= 1 (A010051 n)) (A003309 (+ 1 (A255421 (A000720 n))))) (else (A192607 (A255421 (A065855 n))))))",
				";; Alternatively:",
				"(define (A255421 n) (A237126 (A246377 n)))"
			],
			"xref": [
				"Inverse: A255422.",
				"Cf. A000040, A000720, A002808, A003309, A007097, A008578, A065855, A010051, A192607, A255420, A255324.",
				"Related or similar permutations: A237126, A246377, A245703, A245704, A255407, A255408."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Antti Karttunen_, Feb 23 2015",
			"references": 8,
			"revision": 15,
			"time": "2015-02-23T23:05:24-05:00",
			"created": "2015-02-23T23:05:24-05:00"
		}
	]
}