{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A244540",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 244540,
			"data": "1,3,3,2,3,4,2,0,3,5,4,2,2,4,0,0,3,6,5,2,4,0,2,0,2,7,4,4,0,4,0,0,3,4,6,0,5,4,2,0,4,6,0,2,2,4,0,0,2,3,7,4,4,4,4,0,0,4,4,2,0,4,0,0,3,8,4,2,6,0,0,0,5,6,4,2,2,0,0,0,4,7,6,2,0,8,2",
			"name": "Expansion of phi(q) * (phi(q) + phi(q^2)) / 2 in powers of q where phi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A244540/b244540.txt\"\u003eTable of n, a(n) for n = 0..2500\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of f(-q^3, -q^5)^2 * phi(q) / psi(-q) = f(-q^3, -q^5)^2 * chi(q)^3 in powers of q where phi(), psi(), chi(), f() are Ramanujan theta functions.",
				"Euler transform of period 8 sequence [3, -3, 1, 0, 1, -3, 3, -2, ...].",
				"Moebius transform is period 8 sequence [3, 0, -1, 0, 1, 0, -3, 0, ...].",
				"Convolution product of A244526 and A107635. Convolution product of A000122 and A093709.",
				"a(n) = (A004018(n) + A033715(n)) / 2 = A244543(2*n).",
				"a(2*n) = a(n). a(8*n + 3) = 2*A033761(n). a(8*n + 5) = 4*A053692(n). a(8*n + 7) = 0."
			],
			"example": [
				"G.f. = 1 + 3*q + 3*q^2 + 2*q^3 + 3*q^4 + 4*q^5 + 2*q^6 + 3*q^8 + 5*q^9 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 1, Boole[n == 0], Sum[ {3, 0, -1, 0, 1, 0, -3, 0}[[ Mod[ d, 8, 1] ]], {d, Divisors @ n}]];",
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 3, 0, q] (EllipticTheta[ 3, 0, q] + EllipticTheta[ 3, 0, q^2]) / 2, {q, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, n==0, sumdiv(n, d, [0, 3, 0, -1, 0, 1, 0, -3][d%8 + 1]))};",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = sum(k=1, sqrtint(n), 2 * x^k^2, 1 + x * O(x^n)); polcoeff( A * (A + subst(A, x, x^2)) / 2, n))};",
				"(Sage) A = ModularForms( Gamma1(8), 1, prec=33) . basis(); A[0] + 3*A[1] + 3*A[2];",
				"(MAGMA) A := Basis( ModularForms( Gamma1(8), 1), 33); A[1] + 3*A[2] + 3*A[3];"
			],
			"xref": [
				"Cf. A000122, A004018, A033715, A033761, A053692, A093709, A107635, A244526, A244543."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Michael Somos_, Jun 29 2014",
			"references": 3,
			"revision": 13,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2014-06-29T15:08:16-04:00"
		}
	]
}