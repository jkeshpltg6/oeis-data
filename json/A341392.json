{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A341392",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 341392,
			"data": "1,1,2,1,4,2,3,1,8,4,6,2,9,3,4,1,16,8,12,4,18,6,8,2,27,9,12,3,16,4,5,1,32,16,24,8,36,12,16,4,54,18,24,6,32,8,10,2,81,27,36,9,48,12,15,3,64,16,20,4,25,5,6,1,64,32,48,16,72,24,32,8,108,36,48,12,64,16,20,4,162,54,72,18,96,24,30,6,128",
			"name": "a(n) = A284005(n) / (1 + A000120(n))!.",
			"comment": [
				"From _Antti Karttunen_, Feb 10 2021: (Start)",
				"This sequence can be represented as a binary tree. Each child to the left is obtained by multiplying its parent with (1+{binary weight of its breadth-first-wise index in the tree}), while each child to the right is just a clone of its parent:",
				"                                      1",
				"                                      |",
				"                   ...................1...................",
				"                  2                                       1",
				"        4......../ \\........2                   3......../ \\........1",
				"       / \\                 / \\                 / \\                 / \\",
				"      /   \\               /   \\               /   \\               /   \\",
				"     /     \\             /     \\             /     \\             /     \\",
				"    8       4           6       2           9       3           4       1",
				"  16 8    12 4        18 6     8 2        27 9    12 3        16 4     5 1",
				"etc.",
				"(End)",
				"This sequence and A243499 have the same set of values on intervals from 2^m to 2^(m+1) - 1 for m \u003e= 0. - _Mikhail Kurkov_, Jun 18 2021"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A341392/b341392.txt\"\u003eTable of n, a(n) for n = 0..16384\u003c/a\u003e",
				"Michael De Vlieger, \u003ca href=\"/A341392/a341392.png\"\u003eThis sequence as a binary tree\u003c/a\u003e showing rows 0 \u003c= r \u003c= 5.",
				"Michael De Vlieger, \u003ca href=\"/A341392/a341392_1.png\"\u003eThis sequence as a binary tree\u003c/a\u003e showing rows 0 \u003c= r \u003c= 8."
			],
			"formula": [
				"a(n) = A284005(n) / (1 + A000120(n))! = A284005(n) / A000142(1 + A000120(n)).",
				"a(2n+1) = a(n) for n \u003e= 0.",
				"a(2n) = (1 + A000120(n))*a(n) = A243499(2*A059894(n)) = a(n) + a(2n - 2^A007814(n)) for n \u003e 0 with a(0) = 1.",
				"[2*a(n) - 1 = A329369(n)] = [A036987(A053645(n))]."
			],
			"maple": [
				"a:= proc(n) option remember; `if`(n=0, 1,",
				"      a(iquo(n, 2, 'd'))*`if`(d=1, 1, add(i, i=Bits[Split](n+1))))",
				"    end:",
				"seq(a(n), n=0..120);  # _Alois P. Heinz_, Jun 23 2021"
			],
			"mathematica": [
				"Array[DivisorSigma[0, Apply[Times, Map[#1^#2 \u0026 @@ # \u0026, FactorInteger[#1] /. {p_, e_} /; e == 1 :\u003e {Times @@ Prime@ Range@ PrimePi@ p, e}]]]/#2 \u0026 @@ {Times @@ Prime@ Flatten@ Position[#, 1] \u0026@ Reverse@ #, (1 + Count[#, 1])!} \u0026@ IntegerDigits[#, 2] \u0026, 89, 0] (* _Michael De Vlieger_, Feb 24 2021 *)"
			],
			"program": [
				"(PARI)",
				"A284005(n) = { my(k=if(n, logint(n, 2)), s=1); prod(i=0, k, s+=bittest(n, k-i)); }; \\\\ From A284005",
				"A341392(n) = (A284005(n)/((1 + hammingweight(n))!)); \\\\ _Antti Karttunen_, Feb 10 2021",
				"(PARI) A341392(n) = if(!n,1,if(n%2, A341392((n-1)/2), (1+hammingweight(n))*A341392(n/2))); \\\\ _Antti Karttunen_, Feb 10 2021"
			],
			"xref": [
				"Cf. A000120, A000142, A007814, A036987, A053645, A243499, A284005, A329369 (similar recurrence)."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Mikhail Kurkov_, Feb 10 2021",
			"references": 10,
			"revision": 39,
			"time": "2021-06-23T18:45:50-04:00",
			"created": "2021-02-28T01:54:23-05:00"
		}
	]
}