{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000806",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 806,
			"id": "M3982 N1651",
			"data": "1,0,1,-5,36,-329,3655,-47844,721315,-12310199,234615096,-4939227215,113836841041,-2850860253240,77087063678521,-2238375706930349,69466733978519340,-2294640596998068569,80381887628910919255,-2976424482866702081004,116160936719430292078411",
			"name": "Bessel polynomial y_n(-1).",
			"comment": [
				"a(n) can be seen as a subset of the unordered pairings of the first 2n integers (A001147) with forbidden pairs (i,i+1) for all i in [1,2n-1] (all adjacent integers). The circular version of this constraint is A003436. - _Olivier Gérard_, Feb 08 2011",
				"|a(n)| is the number of perfect matchings in the complement of P_{2n} where P_{2n} is the path graph on 2n vertices. - _Andrew Howroyd_, Mar 15 2016",
				"The unsigned version of these numbers now has its own entry: see A278990. - _N. J. A. Sloane_, Dec 07 2016"
			],
			"reference": [
				"G. Kreweras and Y. Poupard, Sur les partitions en paires d'un ensemble fini totalement ordonné, Publications de l'Institut de Statistique de l'Université de Paris, 23 (1978), 57-74.",
				"J. Riordan, Combinatorial Identities, Wiley, 1968, p. 77.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe and Seiichi Manyama, \u003ca href=\"/A000806/b000806.txt\"\u003eTable of n, a(n) for n = 0..404\u003c/a\u003e (first 101 terms from T. D. Noe)",
				"G. Kreweras and Y. Poupard, \u003ca href=\"/A000806/a000806.pdf\"\u003eSur les partitions en paires d'un ensemble fini totalement ordonné\u003c/a\u003e, Publications de l'Institut de Statistique de l'Université de Paris, 23 (1978), 57-74. (Annotated scanned copy)",
				"R. J. Mathar, \u003ca href=\"http://vixra.org/abs/1511.0015\"\u003eA class of multinomial permutations avoiding object clusters\u003c/a\u003e, vixra:1511.0015 (2015), sequence M_{c,2}/c!.",
				"J. Riordan, \u003ca href=\"/A000806/a000806_1.pdf\"\u003eLetter to N. J. A. Sloane, Aug. 1970\u003c/a\u003e",
				"Everett Sullivan, \u003ca href=\"https://arxiv.org/abs/1611.02771\"\u003eLinear chord diagrams with long chords\u003c/a\u003e, arXiv preprint arXiv:1611.02771 [math.CO], 2016.",
				"J. Touchard, \u003ca href=\"http://dx.doi.org/10.4153/CJM-1956-034-1\"\u003eNombres exponentiels et nombres de Bernoulli\u003c/a\u003e, Canad. J. Math., 8 (1956), 305-320.",
				"Donovan Young, \u003ca href=\"https://arxiv.org/abs/2004.06921\"\u003ePolyomino matchings in generalised games of memory and linear k-chord diagrams\u003c/a\u003e, arXiv:2004.06921 [math.CO], 2020.",
				"\u003ca href=\"/index/Be#Bessel\"\u003eIndex entries for sequences related to Bessel functions or polynomials\u003c/a\u003e"
			],
			"formula": [
				"E.g.f.: exp(sqrt(1 + 2*x) - 1) / sqrt(1 + 2*x). - _Michael Somos_, Feb 16 2002",
				"D-finite with recurrence a(n) = (-2*n+1)*a(n-1) + a(n-2). - _T. D. Noe_, Oct 26 2006",
				"If y = x + Sum_{k\u003e1} A000272(k) * x^k/k!, then y = x + Sum{k\u003e1} a(k-2) * (-y)^k/k!. - _Michael Somos_, Sep 07 2005",
				"a(-1-n) = a(n). - _Michael Somos_, Apr 02 2007",
				"a(n) = sum_{m=0..n} A001498(n,m)*(-1)^m, n\u003e=0 (alternating row sums of Bessel triangle).",
				"E.g.f. for unsigned version: -exp(sqrt(1-2*x)-1). - _Karol A. Penson_, Mar 20 2010 [gives -1, 1, 0, 1, 5, 36, 329, ... ]",
				"E.g.f. for unsigned version: 1/(sqrt(1-2*x))*exp(sqrt(1-2*x)-1). - _Sergei N. Gladkovskii_, Jul 03 2012",
				"G.f.: 1/G(0) where G(k)= 1 - x + x*(2*k+1)/(1 - x + 2*x*(k+1)/G(k+1)); (continued fraction, 2-step). - _Sergei N. Gladkovskii_, Jul 10 2012",
				"G.f.: 1+x/U(0)  where U(k)=   1 - x + x*(k+1)/U(k+1) ; (continued fraction, Euler's 1st kind, 1-step). - _Sergei N. Gladkovskii_, Oct 06 2012",
				"a(n) = BesselK[n+1/2,-1]/BesselK[5/2,-1]. - _Vaclav Kotesovec_, Aug 07 2013",
				"|a(n)| ~ 2^(n+1/2)*n^n/exp(n+1). - _Vaclav Kotesovec_, Aug 07 2013",
				"0 = a(n) * (a(n+2)) + a(n+1) * (-a(n+1) + 2*a(n+2) + a(n+3)) + a(n+2) * (-a(n+2)) for all n in Z. - _Michael Somos_, Jan 27 2014",
				"a(n) = -i*(BesselK[3/2,1]*BesselI[n+3/2,-1] - BesselI[3/2,-1]*BesselK[n+3/2,1]), n\u003e=0 for unsigned version - _G. C. Greubel_ , Apr 19 2015",
				"a(n) = hypergeom( [n+1, -n], [], 1/2). - _Peter Luschny_, Nov 10 2016",
				"From _G. C. Greubel_, Aug 16 2017: (Start)",
				"a(n) = (1/2)_{n} * (-2)^n * hypergeometric1f1(-n; -2*n; -2).",
				"G.f.: (1/(1-t))*hypergeometric2f0(1, 1/2; -; -2*t/(1-t)^2). (End)"
			],
			"example": [
				"For n=3, the a(3) = 5 solutions are (14) (25) (36), (14) (26) (35), (15) (24) (36), (16) (24) (35), (13) (25) (46) excluding 10 other possible pairings.",
				"G.f. = 1 + x^2 - 5*x^3 + 36*x^4 - 329*x^5 + 3655*x^6 - 47844*x^7 + ..."
			],
			"maple": [
				"A000806 := proc(n) option remember; if n\u003c=1 then n else (2*n+1)*A000806(n-1)+A000806(n-2); fi; end; # for unsigned version",
				"a := n -\u003e hypergeom([n+1,-n],[],1/2): seq(simplify(a(n)),n=0..20); # _Peter Luschny_, Nov 10 2016"
			],
			"mathematica": [
				"a[n_] := a[n] = (-2n+1)*a[n-1] + a[n-2]; a[0] = 1; a[1] = 0; Table[a[n], {n, 0, 19}] (* _Jean-François Alcover_, Nov 29 2011, after _T. D. Noe_ *)",
				"Table[Sum[Binomial[n, i]*(2*n-i)!/2^(n-i)*(-1)^(n-i)/n!, {i, 0, n}], {n, 0, 20}] (* _Vaclav Kotesovec_, Aug 07 2013 *)",
				"a[ n_] := With[ {m = If[ n\u003c0, -n-1, n]}, (-1)^m (2 m - 1)!! Hypergeometric1F1[ -m, -2 m, -2] ]; (* _Michael Somos_, Jan 27 2014 *)",
				"a[ n_] := With[ {m = If[ n\u003c0, -n-1, n]}, Sum[ (-1)^(m - i) (2 m - i)! / (2^(m - i) i! (m - i)!), {i, 0, m}] ]; (* _Michael Somos_, Jan 27 2014 *)",
				"a[ n_] := With[ {m = If[ n\u003c0, -n-1, n]}, If[ m\u003c1, 1, (-1)^m Numerator @ FromContinuedFraction[ Table[ (-1)^Quotient[k, 2] If[ OddQ[k], k, 1], {k, 2 m}] ] ] ]; (* _Michael Somos_, Jan 27 2014 *)",
				"Table[(-1)^n (2 n - 1)!! Hypergeometric1F1[-n, -2 n, -2], {n, 0, 20}] (* _Eric W. Weisstein_, Nov 14 2018 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, n = -n-1); sum(k=0, n, (2*n-k)! / (k! * (n-k)!) * (-1/2)^(n-k) )}; /* _Michael Somos_, Apr 02 2007 */",
				"(PARI) {a(n) = local(A); if( n\u003c0, n = -n-1); A = sqrt(1 + 2*x + x * O(x^n)); n! * polcoeff( exp(A-1) / A, n)}; /* _Michael Somos_, Apr 02 2007 */",
				"(PARI) {a(n) = local(A); if( n\u003c0, n = -n-1); n+=2; -(-1)^n * n! * polcoeff( serreverse( sum(k=1, n, k^(k-2)* x^k / k!, x * O(x^n))), n)}; /* _Michael Somos_, Apr 02 2007 */",
				"(PARI) {a(n) = if( n\u003c0, n=-n-1); contfracpnqn( vector( 2*n, k, (-1)^(k\\2) * if( k%2, k, 1))) [1,1] }; /* _Michael Somos_, Jan 27 2014 */",
				"(MAGMA) I:=[0,1]; [1] cat [n le 2 select I[n] else (1-2*n)*Self(n-1)+Self(n-2): n in [1..30]]; // _Vincenzo Librandi_, Apr 19 2015"
			],
			"xref": [
				"Polynomial coefficients are in A001498. Cf. A003436.",
				"Cf. A001515, A101682, A278990."
			],
			"keyword": "sign,easy,nice",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_",
			"references": 26,
			"revision": 114,
			"time": "2020-07-29T23:12:35-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}