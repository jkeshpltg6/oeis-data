{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A016627",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 16627,
			"data": "1,3,8,6,2,9,4,3,6,1,1,1,9,8,9,0,6,1,8,8,3,4,4,6,4,2,4,2,9,1,6,3,5,3,1,3,6,1,5,1,0,0,0,2,6,8,7,2,0,5,1,0,5,0,8,2,4,1,3,6,0,0,1,8,9,8,6,7,8,7,2,4,3,9,3,9,3,8,9,4,3,1,2,1,1,7,2,6,6,5,3,9,9,2,8,3,7,3,7",
			"name": "Decimal expansion of log(4).",
			"comment": [
				"Constant cited in the Percus reference. - _Jonathan Vos Post_, Aug 13 2008",
				"This constant (negated) is also the 1-dimensional analog of Madelung's constant. - _Jean-François Alcover_, May 20 2014",
				"This constant is the sum over the reciprocals of the hexagonal numbers A000384(n), n \u003e= 1. See the Downey et al. link, and the formula by _Robert G. Wilson v_ below. - _Wolfdieter Lang_, Sep 12 2016",
				"log(4) - 1 is the mean ratio between the smaller length and the larger length of the two parts of a stick that is being broken at a point that is uniformly chosen at random (Mosteller, 1965). - _Amiram Eldar_, Jul 25 2020",
				"From _Bernard Schott_, Sep 11 2020: (Start)",
				"This constant was the subject of the problem B5 during the 42nd Putnam competition in 1981 (see formula Sep 11 2020 and Putnam link).",
				"Jeffrey Shallit generalizes this result obtained for base 2 to any base b (see Amer. Math. Month. link): Sum_{k\u003e=1} digsum(k)_b / (k*(k+1)) = (b/(b-1)) * log(b) where digsum(k)_b is the sum of the digits of k when expressed in base b (for base 10 see A334388). (End)"
			],
			"reference": [
				"Milton Abramowitz and Irene A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, 1964 (and various reprintings), p. 2.",
				"Frederick Mosteller, Fifty challenging problems of probability, Dover, New York, 1965. See problem 42, pp. 10 and 63."
			],
			"link": [
				"Harry J. Smith, \u003ca href=\"/A016627/b016627.txt\"\u003eTable of n, a(n) for n = 1..20000\u003c/a\u003e",
				"Milton Abramowitz and Irene A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"Lawrence Downey, Boon W. Ong and James A. Sellers, \u003ca href=\"http://www.personal.psu.edu/jxs23/downey_ong_sellers_cmj_preprint.pdf\"\u003eBeyond the Basel Problem: Sums of Reciprocals of Figurate Numbers\u003c/a\u003e, Coll. Math. J., 39, no. 5 (2008), 391-394.",
				"D. H. Lehmer, \u003ca href=\"http://www.jstor.org/stable/2322496\"\u003eInteresting series involving the Central Binomial Coefficient\u003c/a\u003e, Am. Math. Monthly, Vol. 92, No. 7 (1985) pp. 449-457.",
				"Allon G. Percus, Gabriel Istrate, Bruno Goncalves, Robert Z. Sumi and Stefan Boettcher, \u003ca href=\"http://arxiv.org/abs/0808.1549\"\u003eThe Peculiar Phase Structure of Random Graph Bisection\u003c/a\u003e, arXiv:0808.1549 [cond-mat.stat-mech], 2008.",
				"H.-J. Seiffert, \u003ca href=\"https://fq.math.ca/Scanned/32-4/elementary32-4.pdf\"\u003eProblem B-771\u003c/a\u003e, Elementary Problems and Solutions, The Fibonacci Quarterly, Vol. 32, No. 4 (1994), p. 374; \u003ca href=\"https://www.fq.math.ca/Scanned/33-5/elementary33-5.pdf\"\u003eMore Sums\u003c/a\u003e, Solution to Problem B-771 by Don Redmond, ibid., Vol. 33, No. 5 (1995), pp. 470-471.",
				"J. O. Shallit, \u003ca href=\"https://www.jstor.org/stable/2322523\"\u003eSolutions of Advanced Problems, 6450\u003c/a\u003e, The American Mathematical Monthly, Vol. 92, No. 7, Aug.-Sep., 1985, pp. 513-514; DOI: 10.2307/2322523.",
				"42nd Putnam Competition, \u003ca href=\"https://prase.cz/kalva/putnam/putn81.html\"\u003eProblem B5\u003c/a\u003e, 1981.",
				"\u003ca href=\"/index/O#Olympiads\"\u003eIndex entries for sequences related to Olympiads and other Mathematical competitions\u003c/a\u003e.",
				"\u003ca href=\"/index/Tra#transcendental\"\u003eIndex entries for transcendental numbers\u003c/a\u003e."
			],
			"formula": [
				"log(4) = Sum_{k \u003e= 1} H(k)/2^k where H(k) is the k-th harmonic number. - _Benoit Cloitre_, Jun 15 2003",
				"Equals 1 - Sum_{k \u003e= 1} (-1)^k/A002378(k) = 1 + 2*Sum_{k \u003e= 0} 1/A069072(k) = 5/4 - Sum_{k \u003e= 1} (-1)^k/A007531(k+2). - _R. J. Mathar_, Jan 23 2009",
				"Equals 2*A002162 = Sum_{n \u003e= 1} binomial(2*n, n)/(n*4^n) [D. H. Lehmer, Am. Math. Monthly 92 (1985) 449 and Jolley eq. 262]. - _R. J. Mathar_, Mar 04 2009",
				"log(4) = Sum_{k \u003e= 1} A191907(4, k)/k, (conjecture). - _Mats Granvik_, Jun 19 2011",
				"log(4) = lim_{n -\u003e infinity} A066066(n)/n. - _M. F. Hasler_, Oct 20 2013",
				"Equals Sum_{k \u003e= 1} 1/( 2*k^2 - k ). - _Robert G. Wilson v_, Aug 31 2014",
				"Equals gamma(0, 1/2) - gamma(0, 1) = -(EulerGamma + polygamma(0, 1/2)), where gamma(n,x) denotes the generalized Stieltjes constants. - _Peter Luschny_, May 16 2018",
				"From _Amiram Eldar_, Jul 25 2020: (Start)",
				"Equals Sum_{k\u003e=1} (3/4)^k/k.",
				"Equals Sum_{k\u003e=1} 1/(k*2^(k-1)) = Sum_{k\u003e=1} 1/A001787(k).",
				"Equals Integral_{x=0..1} log(1+1/x) dx. (End)",
				"Equals Sum_{k\u003e=1} A000120(k) / (k*(k+1)). - _Bernard Schott_, Sep 11 2020",
				"Equals 1 + Sum_{k\u003e=1} zeta(2*k+1)/4^k. - _Amiram Eldar_, May 27 2021",
				"Equals Sum_{k\u003e=1} (2*k+1)*Fibonacci(k)/(k*(k+1)*2^k) (Seiffert, 1994). - _Amiram Eldar_, Jan 15 2022"
			],
			"example": [
				"1.38629436111989061883446424291635313615100026872051050824136..."
			],
			"mathematica": [
				"RealDigits[Log@ 4, 10, 111][[1]] (* _Robert G. Wilson v_, Aug 31 2014 *)"
			],
			"program": [
				"(PARI) default(realprecision, 20080); x=log(4); for (n=1, 20000, d=floor(x); x=(x-d)*10; write(\"b016627.txt\", n, \" \", d)); \\\\ _Harry J. Smith_, May 16 2009, corrected May 19 2009",
				"(PARI) A016627_vec(N)=digits(floor(log(precision(4.,N))*10^(N-1))) \\\\ Or: default(realprecision,N);digits(log(4)\\.1^N) \\\\ _M. F. Hasler_, Oct 20 2013"
			],
			"xref": [
				"Cf. A016732 (continued fraction).",
				"Cf. A000384, A001787, A002162, A002378, A007531, A066066, A069072, A191907.",
				"Cf. A000045, A000120, A334388."
			],
			"keyword": "nonn,base,cons,changed",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"references": 28,
			"revision": 94,
			"time": "2022-01-15T09:57:08-05:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}