{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A008671",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 8671,
			"data": "1,0,1,1,1,1,2,2,2,3,3,3,4,4,5,5,6,6,7,7,8,9,9,10,11,11,12,13,14,14,16,16,17,18,19,20,21,22,23,24,25,26,28,28,30,31,32,33,35,36,37,39,40,41,43,44,46,47,49,50,52,53,55,57,58,60,62,63,65,67,69,70,73,74,76,78,80",
			"name": "Expansion of 1/((1-x^2)*(1-x^3)*(1-x^7)).",
			"comment": [
				"Number of partitions of n into parts 2, 3, and 7. - _Joerg Arndt_, Jul 08 2013"
			],
			"reference": [
				"A. Adler, Hirzebruch's curves F_1, F_2, F_4, F_14, F_28 for Q(sqrt 7), pp. 221-285 of S. Levy, ed., The Eightfold Way, Cambridge Univ. Press, 1999 (see p. 262).",
				"L. Smith, Polynomial Invariants of Finite Groups, Peters, 1995, p. 199 (No. 24)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A008671/b008671.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=227\"\u003eEncyclopedia of Combinatorial Structures 227\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_12\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,1,1,0,-1,0,1,0,-1,-1,0,1)."
			],
			"formula": [
				"Euler transform of length 7 sequence [ 0, 1, 1, 0, 0, 0, 1]. - _Michael Somos_, Oct 11 2006",
				"a(n) = a(-12-n), a(n) = a(n-2) + a(n-3) - a(n-5) + a(n-7) - a(n-9) - a(n-10) + a(n-12) for all n in Z. - _Michael Somos_, Oct 11 2006",
				"a(n) = floor((3*n^2+36*n+196)/252 + (-1/9)*(-2)^floor((n+2-3*floor((n+2)/3))/2)). - _Tani Akinari_, Jul 07 2013",
				"a(n) ~ 1/84*n^2. - _Ralf Stephan_, Apr 29 2014",
				"0 = a(n) - a(n+2) - a(n+3) + a(n+5) - (mod(n, 7) == 2) for all n in Z. - _Michael Somos_, Mar 18 2015",
				"a(n) = A008614(2*n). - _Michael Somos_, Mar 18 2015"
			],
			"example": [
				"G.f. = 1 + x^2 + x^3 + x^4 + x^5 + 2*x^6 + 2*x^7 + 2*x^8 + 3*x^9 + 3*x^10 + ..."
			],
			"maple": [
				"seq(coeff(series(1/((1-x^2)*(1-x^3)*(1-x^7)), x, n+1), x, n), n = 0..80); # _G. C. Greubel_, Sep 08 2019"
			],
			"mathematica": [
				"CoefficientList[Series[1/((1-x^2)(1-x^3)(1-x^7)), {x,0,80}], x] (* _Vincenzo Librandi_, Jun 22 2013 *)",
				"a[ n_] := With[{m = If[ n \u003c 0, -12 - n, n]}, SeriesCoefficient[ 1 / ((1 - x^2) (1 - x^3) (1 - x^7)), {x, 0, m}]]; (* _Michael Somos_, Mar 18 2015 *)",
				"a[ n_] := Quotient[ 3 n^2 + 36 n + If[ OddQ[n], 189, 252], 252]; (* _Michael Somos_, Mar 18 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, n = -12-n); polcoeff( 1 / ((1 - x^2) * (1 - x^3) * (1 - x^7)) + x * O(x^n), n)}; /* _Michael Somos_, Oct 11 2006 */",
				"(PARI) {a(n) = (3*n^2 + 36*n + if( n%2, 189, 252)) \\ 252}; /* _Michael Somos_, Mar 18 2015 */",
				"(MAGMA) R\u003cx\u003e:=PowerSeriesRing(Integers(), 80); Coefficients(R!( 1/((1-x^2)*(1-x^3)*(1-x^7)) )); // _G. C. Greubel_, Sep 08 2019",
				"(Sage)",
				"def A008671_list(prec):",
				"    P.\u003cx\u003e = PowerSeriesRing(ZZ, prec)",
				"    return P(1/((1-x^2)*(1-x^3)*(1-x^7))).list()",
				"A008671_list(80) # _G. C. Greubel_, Sep 08 2019",
				"(GAP) a:=[1,0,1,1,1,1,2,2,2,3,3,3];; for n in [13..80] do a[n]:=a[n-2] +a[n-3] -a[n-5] +a[n-7] -a[n-9] -a[n-10] +a[n-12]; od; a; # _G. C. Greubel_, Sep 08 2019"
			],
			"xref": [
				"First differences of A029001.",
				"Cf. A008614."
			],
			"keyword": "nonn,easy",
			"offset": "0,7",
			"author": "_N. J. A. Sloane_",
			"references": 2,
			"revision": 38,
			"time": "2019-09-09T04:32:52-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}