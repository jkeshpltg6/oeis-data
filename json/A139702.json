{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A139702",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 139702,
			"data": "1,-1,4,-24,178,-1512,14152,-142705,1528212,-17211564,202460400,-2474708496,31310415376,-408815254832,5495451727376,-75907303147652,1075685334980240,-15618612118252960,232102241507321384,-3526880759915999016",
			"name": "G.f. satisfies: x = A( x + A(x)^2 ).",
			"comment": [
				"Signed version of A213591."
			],
			"link": [
				"Paul D. Hanna, \u003ca href=\"/A139702/b139702.txt\"\u003eTable of n, a(n), n=1..100.\u003c/a\u003e"
			],
			"formula": [
				"Let G(x) = Series_Reversion( A(x) ) = x + A(x)^2, then G(x) = G(G(x)) - x^2 = g.f. of A138740.",
				"G.f. satisfies: A(x) = x*G(-A(x)^2/x) where G(x) = 1 + x*G(1-1/G(x))^2 is the g.f. of A212411.",
				"G.f.: A(x)/x is the unique solution to variable A in the infinite system of simultaneous equations starting with:",
				"A = 1 - x*B^2;",
				"B = A - x*C^2;",
				"C = B - x*D^2;",
				"D = C - x*E^2;",
				"E = D - x*F^2; ...",
				"G.f. satisfies: A(x) = x*exp( Sum_{n\u003e=0} (-1)^(n+1)*[d^n/dx^n A(x)^(2n+2)/x]/(n+1)! ). [_Paul D. Hanna_, Dec 18 2010]"
			],
			"example": [
				"G.f.: A(x) = x - x^2 + 4*x^3 - 24*x^4 + 178*x^5 - 1512*x^6 +-...",
				"A(x)^2 = x^2 - 2*x^3 + 9*x^4 - 56*x^5 + 420*x^6 - 3572*x^7 +-...",
				"where A(x + A(x)^2) = x.",
				"Let G(x) = Series_Reversion( A(x) ) = x + A(x)^2, then:",
				"G(x) = x + x^2 - 2*x^3 + 9*x^4 - 56*x^5 + 420*x^6 -+... and",
				"G(G(x)) = x + 2*x^2 - 2*x^3 + 9*x^4 - 56*x^5 + 420*x^6 -+...",
				"so that G(x) = G(G(x)) - x^2 = g.f. of A138740.",
				"Logarithmic series:",
				"log(A(x)/x) = -A(x)^2/x + [d/dx A(x)^4/x]/2! - [d^2/dx^2 A(x)^6/x]/3! + [d^3/dx^3 A(x)^8/x]/4! -+..."
			],
			"mathematica": [
				"nmax = 20; sol = {a[1] -\u003e 1}; nmin = Length[sol]+1;",
				"Do[A[x_] = Sum[a[k] x^k, {k, 0, n}] /. sol; eq = CoefficientList[x - A[x + A[x]^2] + O[x]^(n+1), x][[nmin;;]] == 0 /. sol; sol = sol ~Join~ Solve[eq][[1]], {n, nmin, nmax}];",
				"a /@ Range[nmax] /. sol (* _Jean-François Alcover_, Nov 06 2019 *)"
			],
			"program": [
				"(PARI) {a(n)=local(A=x); if(n\u003c1, 0, for(i=1,n, A=serreverse(x + (A+x*O(x^n))^2)); polcoeff(A, n))}",
				"(PARI) /* n-th Derivative: */",
				"{Dx(n,F)=local(D=F);for(i=1,n,D=deriv(D));D}",
				"/* G.f.: [_Paul D. Hanna_, Dec 18 2010] */",
				"{a(n)=local(A=x-x^2+x*O(x^n));for(i=1,n,",
				"A=x*exp(sum(m=0,n,(-1)^(m+1)*Dx(m,A^(2*m+2)/x)/(m+1)!)+x*O(x^n)));polcoeff(A,n)}"
			],
			"xref": [
				"Cf. A138740, A212411, A213591.",
				"Cf. A088714, A088717, A091713, A120971, A140094, A140095.",
				"Cf. A143426, A087949, A143435, A182969."
			],
			"keyword": "sign",
			"offset": "1,3",
			"author": "_Paul D. Hanna_, Apr 30 2008, May 20 2008",
			"references": 13,
			"revision": 23,
			"time": "2019-11-06T04:34:52-05:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}