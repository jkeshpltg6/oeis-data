{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A083219",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 83219,
			"data": "0,1,2,3,2,3,4,5,4,5,6,7,6,7,8,9,8,9,10,11,10,11,12,13,12,13,14,15,14,15,16,17,16,17,18,19,18,19,20,21,20,21,22,23,22,23,24,25,24,25,26,27,26,27,28,29,28,29,30,31,30,31,32,33,32,33,34,35,34,35,36,37,36,37,38",
			"name": "a(n) = n - 2*floor(n/4).",
			"comment": [
				"Conjecture: number of roots of P(x) = x^n - x^(n-1) - x^(n-2) - ... - x - 1 in the left half-plane. - _Michel Lagneau_, Apr 09 2013",
				"a(n) is n+2 with its second least significant bit removed (see A021913(n+2) for that bit). - _Kevin Ryde_, Dec 13 2019"
			],
			"link": [
				"Altug Alkan, \u003ca href=\"/A083219/b083219.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,0,0,1,-1)"
			],
			"formula": [
				"a(n) = A083220(n)/2.",
				"a(n) = a(n-1) + n mod 2 + (n mod 4 - 1)*(1 - n mod 2), a(0) = 0.",
				"G.f.: x*(1+x+x^2-x^3)/((1-x)^2*(1+x)*(1+x^2)). - _R. J. Mathar_, Aug 28 2008",
				"a(n) = 1 + (1/12)*Sum_{k=0..n} (7*(k mod 4) + ((k+1) mod 4) + ((k+2) mod 4) - 5*((k+3) mod 4)), with n \u003e= 0. - _Paolo P. Lava_, Oct 06 2008",
				"a(n) = n - A129756(n). - _Michel Lagneau_, Apr 09 2013",
				"Bisection: a(2*k) = 2*floor((n+2)/4), a(2*k+1) = a(2*k) + 1, k \u003e= 0. - _Wolfdieter Lang_, May 08 2017",
				"a(n) = (2*n + 3 - 2*cos(n*Pi/2) - cos(n*Pi) - 2*sin(n*Pi/2))/4. - _Wesley Ivan Hurt_, Oct 02 2017",
				"a(n) = A162330(n+2) - 1 = A285869(n+3) - 1. - _Kevin Ryde_, Dec 13 2019",
				"E.g.f.: ((1 + x)*cosh(x) - cos(x) + (2 + x)*sinh(x) - sin(x))/2. - _Stefano Spezia_, May 27 2021"
			],
			"maple": [
				"a:= n-\u003e n - 2*floor(n/4):",
				"seq(a(n), n=0..74);  # _Alois P. Heinz_, Jan 24 2021"
			],
			"mathematica": [
				"Array[# - 2 Floor[#/4] \u0026, 75, 0] (* _Michael De Vlieger_, Oct 02 2017 *)",
				"LinearRecurrence[{1,0,0,1,-1},{0,1,2,3,2},80] (* _Harvey P. Dale_, Oct 01 2018 *)"
			],
			"program": [
				"(PARI) a(n) = n - n\\4*2 \\\\ _Charles R Greathouse IV_, Jun 11 2015"
			],
			"xref": [
				"Cf. A083220, A129756, A018837, A162751 (second highest bit removed).",
				"Cf. A021913, A162330, A285869."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Reinhard Zumkeller_, Apr 22 2003",
			"references": 7,
			"revision": 49,
			"time": "2021-05-27T16:36:27-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}