{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A187596",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 187596,
			"data": "1,1,1,1,0,1,1,1,1,1,1,0,2,0,1,1,1,3,3,1,1,1,0,5,0,5,0,1,1,1,8,11,11,8,1,1,1,0,13,0,36,0,13,0,1,1,1,21,41,95,95,41,21,1,1,1,0,34,0,281,0,281,0,34,0,1,1,1,55,153,781,1183,1183,781,153,55,1,1,1,0,89,0,2245,0,6728,0,2245,0,89,0,1,1,1,144,571,6336",
			"name": "Array T(m,n) read by antidiagonals: number of domino tilings of the m X n grid (m\u003e=0, n\u003e=0).",
			"comment": [
				"A099390 supplemented by an initial row and column of 1's.",
				"See A099390 (the main entry for this array) for further information.",
				"If we work with the row index starting at 1 then every row of the array is a divisibility sequence, i.e., the terms satisfy the property that if n divides m then a(n) divide a(m) provided a(n) != 0. Row k satisfies a linear recurrence of order 2^floor(k/2) (Stanley, Ex. 36 p. 273). - _Peter Bala_, Apr 30 2014"
			],
			"reference": [
				"R. P. Stanley, Enumerative Combinatorics, Vol. 1, Cambridge University Press, 1997."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A187596/b187596.txt\"\u003eAntidiagonals n = 0..80, flattened\u003c/a\u003e",
				"J. Propp, \u003ca href=\"http://arxiv.org/abs/math/9904150\"\u003eEnumeration of Matchings: Problems and Progress\u003c/a\u003e, arXiv:math/9904150v2 [math.CO]",
				"E. W. Weisstein, \u003ca href=\"http://mathworld.wolfram.com/ChebyshevPolynomialoftheSecondKind.html\"\u003eChebyshev Polynomial of the second kind\u003c/a\u003e MathWorld",
				"E. W. Weisstein, \u003ca href=\"http://mathworld.wolfram.com/FibonacciPolynomial.html\"\u003eFibonacci Polynomial\u003c/a\u003e MathWorld"
			],
			"formula": [
				"From _Peter Bala_, Apr 30 2014: (Start)",
				"T(n,k)^2 = absolute value of Prod(Prod( 2*cos(a*Pi/(n+1)) + 2*i*cos(b*Pi/(k+1)), a = 1..n), b = 1..k), where i = sqrt(-1). See Propp, Section 5.",
				"Equivalently, working with both the row index n and column index k starting at 1 we have T(n,k)^2 = absolute value of Resultant (F(n,x), U(k-1,x/2)), where U(n,x) is a Chebyshev polynomial of the second kind and F(n,x) is a Fibonacci polynomial defined recursively by F(0,x) = 0, F(1,x) = 1 and F(n,x) = x*F(n-1,x) + F(n-2,x) for n \u003e= 2. The divisibility properties of the array entries mentioned in the Comments are a consequence of this result. (End)"
			],
			"example": [
				"Array begins:",
				"  1,  1,  1,  1,   1,    1,     1,     1,       1,      1,        1, ...",
				"  1,  0,  1,  0,   1,    0,     1,     0,       1,      0,        1, ...",
				"  1,  1,  2,  3,   5,    8,    13,    21,      34,     55,       89, ...",
				"  1,  0,  3,  0,  11,    0,    41,     0,     153,      0,      571, ...",
				"  1,  1,  5, 11,  36,   95,   281,   781,    2245,   6336,    18061, ...",
				"  1,  0,  8,  0,  95,    0,  1183,     0,   14824,      0,   185921, ...",
				"  1,  1, 13, 41, 281, 1183,  6728, 31529,  167089, 817991,  4213133, ...",
				"  1,  0, 21,  0, 781,    0, 31529,     0, 1292697,      0, 53175517, ..."
			],
			"maple": [
				"with(LinearAlgebra):",
				"T:= proc(m,n) option remember; local i, j, t, M;",
				"      if m\u003c=1 or n\u003c=1 then 1 -irem(n*m, 2)",
				"    elif irem(n*m, 2)=1 then 0",
				"    elif m\u003cn then T(n,m)",
				"    else M:= Matrix(n*m, shape=skewsymmetric);",
				"         for i to n do",
				"           for j to m do",
				"             t:= (i-1)*m+j;",
				"             if j\u003cm then M[t, t+1]:= 1 fi;",
				"             if i\u003cn then M[t, t+m]:= 1-2*irem(j, 2) fi",
				"           od",
				"         od;",
				"         sqrt(Determinant(M))",
				"      fi",
				"    end:",
				"seq(seq(T(m, d-m), m=0..d), d=0..14);  # _Alois P. Heinz_, Apr 11 2011"
			],
			"mathematica": [
				"t[m_, n_] := Product[2*(2+Cos[2*j*Pi/(m+1)]+Cos[2*k*Pi/(n+1)]), {k, 1, n/2}, {j, 1, m/2}]; t[_?OddQ, _?OddQ] = 0; Table[t[m-n, n] // FullSimplify, {m, 0, 13}, {n, 0, m}] // Flatten (* _Jean-François Alcover_, Jan 07 2014, after A099390 *)"
			],
			"xref": [
				"Cf. A099390.",
				"See A187616 for a triangular version, and A187617, A187618 for the sub-array T(2m,2n).",
				"See also A049310, A053117."
			],
			"keyword": "nonn,tabl",
			"offset": "0,13",
			"author": "_N. J. A. Sloane_, Mar 11 2011",
			"references": 13,
			"revision": 58,
			"time": "2021-01-11T04:52:46-05:00",
			"created": "2011-03-11T18:17:30-05:00"
		}
	]
}