{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A342723",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 342723,
			"data": "0,0,0,1,1,0,0,1,0,1,0,2,1,1,3,1,2,0,0,4,3,0,0,4,7,2,0,5,2,5,0,1,0,3,4,3,4,0,6,7,3,4,0,3,4,0,0,5,0,9,10,9,3,0,5,8,0,4,0,17,4,0,9,1,19,2,0,6,2,7,0,7,7,7,23,2,8,12,0,10,0,5,0,15,27",
			"name": "a(n) is the number of convex integer quadrilaterals (up to congruence) with integer side lengths a,b,c,d with n=Max(a,b,c,d), integer diagonals e,f and integer area.",
			"comment": [
				"Without loss of generality we assume that a is the largest side length and that the diagonal e divides the convex quadrilateral into two triangles with sides a,b,e and c,d,e. The triangle inequality implies e \u003e a-b and abs(e-c) \u003c d \u003c e+c."
			],
			"example": [
				"a(4)=1 is the smallest possible solution and is a rectangle with a=c=4, b=d=3, e=f=5 and area 12. a(24)=4 includes the smallest possible solution with all sides a,b,c,d different and a=24, b=20, c=15, d=7, e=20, f=25 and area 234. Furthermore there are three rectangles with a=24,b=7, a=24,b=10 and a=24,b=18."
			],
			"mathematica": [
				"an={};",
				"area[a_,b_,c_,d_,e_,f_]:=1/4 Sqrt[4e^2 f^2-(a^2+c^2-b^2-d^2)^2];",
				"he[a_,b_,e_]:=1/(2 e) Sqrt[-(a-b-e) (a+b-e) (a-b+e) (a+b+e)];",
				"paX[e_]:={e,0} (*vertex A coordinate*)",
				"pbX[a_,b_,e_]:={(-a^2+b^2+e^2)/(2 e),he[a,b,e]}(*vertex B coordinate*)",
				"pc={0,0};(*vertex C coordinate*)pdX[c_,d_,e_]:={(c^2-d^2+e^2)/(2 e),-he[c,d,e]}(*vertex D coordinate*)",
				"convexQ[{bx_,by_},{dx_,dy_},e_]:=If[(by-dy) e\u003eby dx-bx dy\u003e0,True,False]",
				"(*define order on tuples*)",
				"gQ[x_,y_]:=Module[{z=x-y,res=False},Do[If[z[[i]]\u003e0,res=True;Break[],If[z[[i]]\u003c0,Break[]]],{i,1,6}];res]",
				"(*check if tuple is canonical*)",
				"canonicalQ[{a_,b_,c_,d_,e_,f_}]:=Module[{x={a,b,c,d,e,f}},If[(gQ[{b,a,d,c,e,f},x]||gQ[{d,c,b,a,e,f},x]||gQ[{c,d,a,b,e,f},x]||gQ[{b,c,d,a,f,e},x]||gQ[{a,d,c,b,f,e},x]||gQ[{c,b,a,d,f,e},x]||gQ[{d,a,b,c,f,e},x]),False,True]]",
				"Do[cnt=0;",
				"Do[pa=paX[e];pb=pbX[a,b,e];pd=pdX[c,d,e];",
				"If[(f=Sqrt[(pb-pd).(pb-pd)];IntegerQ[f])\u0026\u0026(ar=area[a,b,c,d,e,f]; IntegerQ[ar])\u0026\u0026convexQ[pb,pd,e]\u0026\u0026canonicalQ[{a,b,c,d,e,f}],cnt++",
				"(*;Print[{{a,b,c,d,e,f,ar},Graphics[Line[{pa,pb,pc,pd,pa}]]}]*)],{b,1,a},{e,a-b+1,a+b-1},{c,1,a},{d,Abs[e-c]+1,Min[a,e+c-1]}];",
				"AppendTo[an,cnt],{a,1,85}]",
				"an"
			],
			"xref": [
				"Cf. A340858 for trapezoids, A342720 and A342721 for concave integer quadrilaterals, A342722 for convex integer quadrilaterals with arbitrary area."
			],
			"keyword": "nonn",
			"offset": "1,12",
			"author": "_Herbert Kociemba_, Apr 25 2021",
			"references": 3,
			"revision": 10,
			"time": "2021-04-27T02:45:23-04:00",
			"created": "2021-04-27T02:45:23-04:00"
		}
	]
}