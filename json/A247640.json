{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A247640",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 247640,
			"data": "1,6,6,24,6,36,24,96,6,36,36,144,24,144,96,384,6,36,36,144,36,216,144,576,24,144,144,576,96,576,384,1536,6,36,36,144,36,216,144,576,36,216,216,864,144,864,576,2304,24,144,144,576,144,864",
			"name": "Number of ON cells after n generations of \"Odd-Rule\" cellular automaton on hexagonal lattice based on 6-celled neighborhood.",
			"comment": [
				"The neighborhood of a cell consists of the six surrounding cells (but not the cell itself). A cell is ON at generation n iff an odd number of its neighbors were ON at the previous generation. We start with one ON cell.",
				"This is the Run Length Transform of the sequence 1, 6, 24, 96, 384, 1536, 6144, 24576, ... (almost certainly A164908, or 1 followed by A002023).",
				"It appears that this is also the sequence corresponding to the odd-rule cellular automaton defined by OddRule 356 (see Ekhad-Sloane-Zeilberger \"Odd-Rule Cellular Automata on the Square Grid\" link). - _N. J. A. Sloane_, Feb 26 2015"
			],
			"link": [
				"Shalosh B. Ekhad, N. J. A. Sloane, and  Doron Zeilberger, \u003ca href=\"http://arxiv.org/abs/1503.01796\"\u003eA Meta-Algorithm for Creating Fast Algorithms for Counting ON Cells in Odd-Rule Cellular Automata\u003c/a\u003e, arXiv:1503.01796 [math.CO], 2015; see also the \u003ca href=\"http://www.math.rutgers.edu/~zeilberg/mamarim/mamarimhtml/CAcount.html\"\u003eAccompanying Maple Package\u003c/a\u003e.",
				"Shalosh B. Ekhad, N. J. A. Sloane, and  Doron Zeilberger, \u003ca href=\"http://arxiv.org/abs/1503.04249\"\u003eOdd-Rule Cellular Automata on the Square Grid\u003c/a\u003e, arXiv:1503.04249 [math.CO], 2015.",
				"N. J. A. Sloane, On the No. of ON Cells in Cellular Automata, Video of talk in Doron Zeilberger's Experimental Math Seminar at Rutgers University, Feb. 05 2015: \u003ca href=\"https://vimeo.com/119073818\"\u003ePart 1\u003c/a\u003e, \u003ca href=\"https://vimeo.com/119073819\"\u003ePart 2\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"http://arxiv.org/abs/1503.01168\"\u003eOn the Number of ON Cells in Cellular Automata\u003c/a\u003e, arXiv:1503.01168 [math.CO], 2015.",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e"
			],
			"formula": [
				"a(n) = number of terms in expansion of f^n mod 2, where f = 1/x+x+1/y+y+1/(x*y)+x*y (mod 2);"
			],
			"maple": [
				"C := f-\u003e`if`(type(f,`+`),nops(f),1);",
				"f := 1/x+x+1/y+y+1/(x*y)+x*y;",
				"g := n-\u003eexpand(f^n) mod 2;",
				"[seq(C(g(n)),n=0..100)];"
			],
			"mathematica": [
				"A247640[n_] := Total[CellularAutomaton[{42, {2, {{1, 1, 0}, {1, 0, 1}, {0, 1, 1}}}, {1, 1}}, {{{1}}, 0}, {{{n}}}], 2]; Array[A247640, 54, 0] (* _JungHwan Min_, Sep 06 2016 *)",
				"A247640L[n_] := Total[#, 2] \u0026 /@ CellularAutomaton[{42, {2, {{1, 1, 0}, {1, 0, 1}, {0, 1, 1}}}, {1, 1}}, {{{1}}, 0}, n]; A247640L[53] (* _JungHwan Min_, Sep 06 2016 *)"
			],
			"xref": [
				"Cf. A164908, A001023, A071053, A160239, A247666."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Sep 22 2014",
			"references": 6,
			"revision": 22,
			"time": "2016-09-07T00:24:20-04:00",
			"created": "2014-09-22T14:56:18-04:00"
		}
	]
}