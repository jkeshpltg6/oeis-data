{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002413",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2413,
			"id": "M4498 N1904",
			"data": "0,1,8,26,60,115,196,308,456,645,880,1166,1508,1911,2380,2920,3536,4233,5016,5890,6860,7931,9108,10396,11800,13325,14976,16758,18676,20735,22940,25296,27808,30481,33320,36330,39516,42883,46436,50180,54120",
			"name": "Heptagonal (or 7-gonal) pyramidal numbers: a(n) = n*(n+1)*(5*n-2)/6.",
			"comment": [
				"The partial sums of A000566. - _R. J. Mathar_, Mar 19 2008",
				"A002413(n + 1) is the number of 4-tuples (w, x, y, z) having all terms in {0, ..., n} and w = floor((x + y + z)/2).  - _Clark Kimberling_, May 28 2012",
				"From _Ant King_, Oct 25 2012: (Start)",
				"For n \u003e 0, the digital roots of this sequence A010888(A002413(n)) form the purely periodic 27-cycle {1, 8, 8, 6, 7, 7, 2, 6, 6, 7, 5, 5, 3, 4, 4, 8, 3, 3, 4, 2, 2, 9, 1, 1, 5, 9, 9}.",
				"For n \u003e 0, the units' digits of this sequence A010879(A002413(n)) form the purely periodic 20-cycle {1, 8, 6, 0, 5, 6, 8, 6, 5, 0, 6, 8, 1, 0, 0, 6, 3, 6, 0, 0}.",
				"(End)"
			],
			"reference": [
				"A. H. Beiler, Recreations in the Theory of Numbers, Dover, NY, 1964, p. 194.",
				"E. Deza and M. M. Deza, Figurate numbers, World Scientific Publishing (2012), page 93.",
				"L. E. Dickson, History of the Theory of Numbers. Carnegie Institute Public. 256, Washington, DC, Vol. 1, 1919; Vol. 2, 1920; Vol. 3, 1923, see vol. 2, p. 2.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A002413/b002413.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/HeptagonalPyramidalNumber.html\"\u003eHeptagonal Pyramidal Number.\u003c/a\u003e",
				"\u003ca href=\"/index/Ps#pyramidal_numbers\"\u003eIndex to sequences related to pyramidal numbers\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-6,4,-1)."
			],
			"formula": [
				"a(n) = n*(n + 1)*(5*n - 2)/6.",
				"G.f.: x*(1 + 4*x)/(1 - x)^4. [Suggested by _Simon Plouffe_ in his 1992 dissertation.]",
				"From _Ant King_, Oct 25 2012: (Start)",
				"a(n) = a(n - 1) + n*(5*n - 3)/2.",
				"a(n) = 3*a(n - 1) - 3*a(n - 2) + a(n - 3) + 5.",
				"a(n) = 4*a(n - 1) - 6*a(n - 2) + 4*a(n - 3) - a(n - 4)",
				"a(n) = (n + 1)*(2*A000566(n) + n)/6 = (5*n - 2)*A000217(n)/3.",
				"a(n) = A000292(n) + 4*A000292(n - 1)",
				"a(n) = A002412(n) + A000292(n - 1)",
				"a(n) = A000217(n) + 5*A000292(n - 1)",
				"a(n) = binomial(n + 2, 3) + 4*binomial(n + 1, 3) = (5*n - 2) * binomial(n + 1, 2)/3.",
				"Sum_{n \u003e= 1} 1/a(n) = 15*(log(3125) + sqrt(5)*log((3 - sqrt(5))/2) - 2*Pi*sqrt(5*(5 - 2*sqrt(5)))/5 - 8/5)/28 = 1.207293...",
				"(End)",
				"a(n) = Sum_{i=0..n-1} (n-i)*(5*i+1), with a(0)=0. - _Bruno Berselli_, Feb 10 2014",
				"a(n) = A080851(5,n-1). - _R. J. Mathar_, Jul 28 2016",
				"E.g.f.: x*(6 + 18*x + 5*x^2)*exp(x)/6. - _Ilya Gutkovskiy_, May 12 2017"
			],
			"example": [
				"For n=7, a(7) = 7*1 + 6*6 + 5*11 + 4*16 + 3*21 + 2*26 + 1*31 = 308. - _Bruno Berselli_, Feb 10 2014"
			],
			"maple": [
				"A002413:=n-\u003en*(n+1)*(5*n-2)/6: seq(A002413(n), n=0..60); # _Wesley Ivan Hurt_, Apr 14 2017"
			],
			"mathematica": [
				"LinearRecurrence[{4, -6, 4, -1}, {1, 8, 26, 60}, 40] (* _Ant King_, Oct 25 2012 *)",
				"Table[(5n^3 + 3n^2 - 2n)/6, {n, 0, 39}] (* _Alonso del Arte_, Oct 25 2012 *)"
			],
			"program": [
				"(Maxima) A002413(n):=n*(n+1)*(5*n-2)/6$ makelist(A002413(n),n,0,20); /* _Martin Ettl_, Dec 12 2012 */",
				"(PARI) a(n)=n*(n+1)*(5*n-2)/6 \\\\ _Charles R Greathouse IV_, Sep 24 2015",
				"(MAGMA) [n*(n + 1)*(5*n - 2)/6: n in [0..50]]; // _G. C. Greubel_, Nov 04 2017"
			],
			"xref": [
				"Cf. A093562 ((5, 1) Pascal, column m = 3).",
				"Cf. similar sequences listed in A237616."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _James A. Sellers_, Dec 23 1999",
				"a(0)=0 prepended by _Max Alekseyev_, Nov 23 2011"
			],
			"references": 36,
			"revision": 89,
			"time": "2021-03-12T22:32:35-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}