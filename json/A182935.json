{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A182935",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 182935,
			"data": "1,-1,1,1003,-4027,-5128423,168359651,68168266699,-587283555451,-221322134443186643,3253248645450176257,52946591945344238676937,-3276995262387193162157789,-6120218676760621380031990351",
			"name": "Numerators of an asymptotic series for the factorial function (Stirling's formula with half-shift).",
			"comment": [
				"G_n = A182935(n)/A144618(n). These rational numbers provide the coefficients for an asymptotic expansion of the factorial function.",
				"The relationship between these coefficients and the Bernoulli numbers are due to De Moivre, 1730 (see Laurie)."
			],
			"link": [
				"Dirk Laurie, \u003ca href=\"http://dip.sun.ac.za/~laurie/papers/computing_gamma.pdf\"\u003eOld and new ways of computing the gamma function\u003c/a\u003e, page 14, 2005.",
				"Peter Luschny, \u003ca href=\"http://www.luschny.de/math/factorial/approx/SimpleCases.html\"\u003eApproximation Formulas for the Factorial Function.\u003c/a\u003e",
				"W. Wang, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2015.12.016\"\u003eUnified approaches to the approximations of the gamma function\u003c/a\u003e, J. Number Theory (2016)."
			],
			"formula": [
				"z! ~ sqrt(2 Pi) (z+1/2)^(z+1/2) e^(-z-1/2)  Sum_{n\u003e=0} G_n / (z+1/2)^n."
			],
			"example": [
				"G_0 = 1, G_1 = -1/24, G_2 = 1/1152, G_3 = 1003/414720."
			],
			"maple": [
				"G := proc(n) option remember; local j,R;",
				"R := seq(2*j,j=1..iquo(n+1,2));",
				"`if`(n=0,1,add(bernoulli(j,1/2)*G(n-j+1)/(n*j),j=R)) end:",
				"A182935 := n -\u003e numer(G(n)); seq(A182935(i),i=0..15);"
			],
			"mathematica": [
				"a[0] = 1; a[n_] := a[n] = Sum[ BernoulliB[j, 1/2]*a[n-j+1]/(n*j), {j, 2, n+1, 2}]; Table[a[n] // Numerator, {n, 0, 15}] (* _Jean-François Alcover_, Jul 26 2013, after Maple *)"
			],
			"xref": [
				"Cf. A001163, A001164, A144618."
			],
			"keyword": "sign,frac",
			"offset": "0,4",
			"author": "Peter Luschny, Feb 24 2011",
			"references": 3,
			"revision": 11,
			"time": "2016-09-23T12:54:21-04:00",
			"created": "2010-12-13T20:53:31-05:00"
		}
	]
}