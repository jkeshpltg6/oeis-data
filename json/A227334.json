{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A227334",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 227334,
			"data": "1,2,8,4,4,8,48,4,24,4,120,8,12,48,8,8,16,24,360,4,48,120,528,8,20,12,72,48,28,8,960,16,120,16,48,24,36,360,24,4,40,48,1848,120,24,528,2208,8,336,20,16,12,52,72,120,48,360,28,3480,8,60,960,48,32",
			"name": "Exponent of the group of the Gaussian integers in a reduced system modulo n.",
			"comment": [
				"a(n) is the exponent of the multiplicative group of Gaussian integers modulo n, i.e., (Z[i]/nZ[i])* = {a + b*i: a, b in Z/nZ and gcd(a^2 + b^2, n) = 1}. The number of elements in (Z[i]/nZ[i])* is A079458(n).",
				"For n \u003e 2, a(n) is divisible by 4. - _Jianing Song_, Aug 29 2018",
				"From _Jianing Song_, Sep 23 2018: (Start)",
				"Equivalent of psi (A002322) in the ring of Gaussian integers.",
				"a(n) is the smallest positive e such that for any Gaussian integer z coprime to n we have z^e == 1 (mod n).",
				"By definition, A079458(n)/a(n) is always an integer, and is 1 iff (Z[i]/nZ[i])* is cyclic, that is, rank((Z[i]/nZ[i])*) = A316506(n) = 0 or 1, and n has a primitive root in (Z[i]/nZ[i])*. A079458(n)/a(n) = 1 iff n = 1, 2 or a prime congruent to 3 modulo 4. (End)"
			],
			"link": [
				"Jianing Song, \u003ca href=\"/A227334/b227334.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Gaussian_integer\"\u003eGaussian integer\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Torsion_group\"\u003eTorsion group\u003c/a\u003e"
			],
			"formula": [
				"a(2^e) = 2^e if e \u003c= 2 and 2^(e-1) if e \u003e= 3, a(p^e) = (p - 1)*p^(e-1) if p == 1 (mod 4) and (p^2 - 1)*p^(e-1) if p == 3 (mod 4). If gcd(m, n) = 1 then a(mn) = lcm(a(m), a(n)). - _Jianing Song_, Aug 29 2018"
			],
			"example": [
				"Let G = (Z[i]/4Z[i])* = {i, 3i, 1, 1 + 2i, 2 + i, 2 + 3i, 3, 3 + 2i}. The possibilities for the exponent of G are 8, 4, 2 and 1. G^4 = {x^4 mod 4 : x belongs to G} = {1} and i^2 !== 1 (mod 4). Therefore, the exponent of G is greater than 2, accordingly the exponent of G is 4 and a(4) = 4."
			],
			"mathematica": [
				"fa = FactorInteger;lamas[1] = 1;lamas[p_, s_]:= Which[Mod[p, 4]==3,p^(s-1)(p^2 - 1), Mod[p, 4] == 1, p^(s - 1)(p - 1), s ≥ 4, 2^(s - 1), s \u003e 1, 4, s == 1, 2]; lamas[n_] := {aux = 1; Do[aux = LCM[aux, lamas[fa[n][[i, 1]], fa[n][[i, 2]]]], {i, 1, Length@fa[n]}]; aux}[[1]]; Table[lamas[n], {n, 100}]"
			],
			"program": [
				"(PARI) a(n)=",
				"{",
				"    my(r=1, f=factor(n));",
				"    for(j=1, #f[, 1], my(p=f[j, 1], e=f[j, 2]);",
				"        if(p==2\u0026\u0026e\u003c=2, r=lcm(r,2^e));",
				"        if(p==2\u0026\u0026e\u003e=3, r=lcm(r,2^(e-1)));",
				"        if(p%4==1, r=lcm(r,(p-1)*p^(e-1)));",
				"        if(p%4==3, r=lcm(r,(p^2-1)*p^(e-1)));",
				"    );",
				"    return(r);",
				"} \\\\ _Jianing Song_, Aug 29 2018"
			],
			"xref": [
				"Equivalent of arithmetic functions in the ring of Gaussian integers (the corresponding functions in the ring of integers are in the parentheses): A062327 (\"d\", A000005), A317797 (\"sigma\", A000203), A079458 (\"phi\", A000010), this sequence (\"psi\", A002322), A086275 (\"omega\", A001221), A078458 (\"Omega\", A001222), A318608 (\"mu\", A008683)."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_José María Grau Ribas_, Jul 07 2013",
			"references": 6,
			"revision": 40,
			"time": "2018-09-24T02:03:02-04:00",
			"created": "2013-07-11T19:26:34-04:00"
		}
	]
}