{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A336783",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 336783,
			"data": "5,90,188",
			"name": "Integers b where the number of cycles under iteration of sum of cubes of digits in base b is exactly four.",
			"comment": [
				"Let b \u003e 1 be an integer, and write the base b expansion of any nonnegative integer m as m = x_0 + x_1 b + ... +x_d b^d with x_d \u003e 0 and 0 \u003c= x_i \u003c b for i=0,...,d.",
				"Consider the map S_{x^3,b}: N to N, with S_{x^3,b}(m) := x_0^3+ ... + x_d^3.",
				"It is known that the orbit set {m,S_{x^3,b}(m), S_{x^3,b}(S_{x^3,b}(m)), ...} is finite for all m\u003e0. Each orbit contains a finite cycle, and for a given base b, the union of such cycles over all orbit sets is finite. Let us denote by L(x^3,i) the set of bases b such that the set of cycles associated to S_{x^3,b} consists of exactly i elements. In this notation, the sequence is the set of known elements of L(x^3,4).",
				"Meanwhile, the known terms of the sequence L(x^3,1) is {2}, L(x^3,2) is empty, and L(x^3,3) is {3, 26}. It's undetermined whether the complete sequences are finite, if so, whether the above give all terms."
			],
			"link": [
				"H. Hasse and G. Prichett, \u003ca href=\"https://doi.org/10.1515/crll.1978.298.8\"\u003eA conjecture on digital cycles\u003c/a\u003e, J. reine angew. Math. 298 (1978), 8--15. Also on \u003ca href=\"http://gdz.sub.uni-goettingen.de/dms/resolveppn/?PPN=GDZPPN002194481\"\u003eGDZ\u003c/a\u003e.",
				"D. Lorenzini, M. Melistas, A. Suresh, M. Suwama, and H. Wang, \u003ca href=\"http://alpha.math.uga.edu/~lorenz/IntegerDynamics.pdf\"\u003eInteger Dynamics\u003c/a\u003e, preprint."
			],
			"formula": [
				"Integers b where A194025(b) + A194281(b) = 4."
			],
			"example": [
				"For instance, when b=5 the associated four cycles are (1),(28),(118) and (9,65,35)."
			],
			"xref": [
				"Cf. A194025, A194281.",
				"Cf. A336744 and A336762 (sum of squares of digits)."
			],
			"keyword": "nonn,base,hard,more",
			"offset": "1,1",
			"author": "_Haiyang Wang_, Aug 04 2020",
			"references": 2,
			"revision": 21,
			"time": "2021-01-09T02:09:07-05:00",
			"created": "2020-08-16T05:27:44-04:00"
		}
	]
}