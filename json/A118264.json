{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A118264",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 118264,
			"data": "1,0,3,8,24,72,216,648,1944,5832,17496,52488,157464,472392,1417176,4251528,12754584,38263752,114791256,344373768,1033121304,3099363912,9298091736,27894275208,83682825624,251048476872,753145430616",
			"name": "Coefficient of q^n in (1-q)^3/(1-3q); dimensions of the enveloping algebra of the derived free Lie algebra on 3 letters.",
			"comment": [
				"a(n) is the number of generalized compositions of n when there are i^2-1 different types of i, (i=1,2,...). - _Milan Janjic_, Sep 24 2010"
			],
			"reference": [
				"C. Reutenauer, Free Lie algebras. London Mathematical Society Monographs. New Series, 7. Oxford Science Publications. The Clarendon Press, Oxford University Press, New York, 1993. xviii+269 pp."
			],
			"link": [
				"N. Bergeron, C. Reutenauer, M. Rosas and M. Zabrocki, \u003ca href=\"http://arxiv.org/abs/math.CO/0502082\"\u003eInvariants and Coinvariants of the Symmetric Group in Noncommuting Variables\u003c/a\u003e, arXiv:math.CO/0502082 , Canad. J. Math. 60 (2008), no. 2, 266-296.",
				"\u003ca href=\"/index/Rec#order_01\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3)."
			],
			"formula": [
				"G.f.: (1-x)^3/(1-3x).",
				"a(n) = 3^{n-1}-3^{n-3} for n\u003e=3.",
				"a(n) = A080923(n-1), n\u003e1.",
				"If p[i]=i^2-1 and if A is Hessenberg matrix of order n defined by: A[i,j]=p[j-i+1], (i\u003c=j), A[i,j]=-1, (i=j+1), and A[i,j]=0 otherwise. Then, for n\u003e=1, a(n)=det A. - _Milan Janjic_, May 02 2010",
				"For a(n)\u003e=8, a(n+1)=3*a(n). - _Harvey P. Dale_, Jun 28 2011"
			],
			"example": [
				"The enveloping algebra of the derived free Lie algebra is characterized as the intersection of the kernels of all partial derivative operators in the space of non-commutative polynomials, a(0) = 1 since all constants are killed by derivatives, a(1) = 0 since no polys of degree 1 are killed, a(2) = 3 since all Lie brackets [x1,x2], [x1,x3], [x2, x3] are killed by all derivative operators."
			],
			"maple": [
				"f:=n-\u003ecoeftayl((1-q)^3/(1-3*q),q=0,n):seq(f(i),i=0..15);"
			],
			"mathematica": [
				"CoefficientList[Series[(1-q)^3/(1-3q),{q,0,30}],q] (* or *) Join[{1,0,3}, NestList[3#\u0026,8,30]] (* _Harvey P. Dale_, Jun 28 2011 *)",
				"Join[{1, 0, 3}, LinearRecurrence[{3}, {8}, 24]] (* _Jean-François Alcover_, Sep 23 2017 *)"
			],
			"xref": [
				"Cf. A080923, A027376, A118265, A118266."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Mike Zabrocki_, Apr 20 2006",
			"ext": [
				"Formula corrected _Mike Zabrocki_, Jul 22 2010"
			],
			"references": 3,
			"revision": 27,
			"time": "2017-09-23T11:28:58-04:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}