{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A125053",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 125053,
			"data": "1,1,3,1,5,15,21,15,5,61,183,285,327,285,183,61,1385,4155,6681,8475,9129,8475,6681,4155,1385,50521,151563,247065,325947,378105,396363,378105,325947,247065,151563,50521,2702765,8108295,13311741,17908935",
			"name": "Variant of triangle A008301, read by rows of 2*n+1 terms, such that the first column is the secant numbers (A000364).",
			"comment": [
				"Foata and Han refer to this as the triangle of Poupard numbers h_n(k). - _N. J. A. Sloane_, Feb 17 2014",
				"Central terms (A125054) equal the binomial transform of the tangent numbers (A000182)."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A125053/b125053.txt\"\u003eRows n = 0..100 of triangle, flattened\u003c/a\u003e",
				"Dominique Foata and Guo-Niu Han, \u003ca href=\"http://www-irma.u-strasbg.fr/~foata/paper/pub123Seidel.pdf\"\u003eSeidel Triangle Sequences and Bi-Entringer Numbers\u003c/a\u003e, Nov 20 2013;"
			],
			"formula": [
				"Sum_{k=0..2n} C(2n,k)*T(n,k) = 4^n * A000182(n), where A000182 are the tangent numbers.",
				"Sum_{k=0..2n} (-1)^n*C(2n,k)*T(n,k) = (-4)^n."
			],
			"example": [
				"If we write the triangle like this:",
				"......................... ...1;",
				"................... ...1, ...3, ...1;",
				"............. ...5, ..15, ..21, ..15, ...5;",
				"....... ..61, .183, .285, .327, .285, .183, ..61;",
				". 1385, 4155, 6681, 8475, 9129, 8475, 6681, 4155, 1385;",
				"then the first nonzero term is the sum of the previous row:",
				"1385 = 61 + 183 + 285 + 327 + 285 + 183 + 61,",
				"the next term is 3 times the first:",
				"4155 = 3*1385,",
				"and the remaining terms in each row are obtained by the rule illustrated by:",
				"6681 = 2*4155 - 1385 - 4*61;",
				"8475 = 2*6681 - 4155 - 4*183;",
				"9129 = 2*8475 - 6681 - 4*285;",
				"8475 = 2*9129 - 8475 - 4*327;",
				"6681 = 2*8475 - 9129 - 4*285;",
				"4155 = 2*6681 - 8475 - 4*183;",
				"1385 = 2*4155 - 6681 - 4*61.",
				"An alternate recurrence is illustrated by:",
				"4155 = 1385 + 2*(61 + 183 + 285 + 327 + 285 + 183 + 61);",
				"6681 = 4155 + 2*(183 + 285 + 327 + 285 + 183);",
				"8475 = 6681 + 2*(285 + 327 + 285);",
				"9129 = 8475 + 2*(327);",
				"and then for k\u003en, T(n,k) = T(n,2*n-k)."
			],
			"maple": [
				"T := proc(n, k) option remember; local j;",
				"  if n = 1 then 1",
				"elif k = 1 then add(T(n-1, j), j=1..2*n-3)",
				"elif k = 2 then 3*T(n, 1)",
				"elif k \u003e n then T(n, 2*n-k)",
				"else 2*T(n, k-1) - T(n, k-2) - 4*T(n-1, k-2)",
				"  fi end:",
				"seq(print(seq(T(n,k), k=1..2*n-1)), n=1..5); # _Peter Luschny_, May 11 2014"
			],
			"mathematica": [
				"t[n_, k_] := t[n, k] = If[2*n \u003c k || k \u003c 0, 0, If[n == 0 \u0026\u0026 k == 0, 1, If[k == 0, Sum[t[n-1, j], {j, 0, 2*n-2}], If[k \u003c= n, t[n, k-1] + 2*Sum[t[n-1, j], {j, k-1, 2*n-1-k}], t[n, 2*n-k]]]]]; Table[t[n, k], {n, 0, 6}, {k, 0, 2*n}] // Flatten (* _Jean-François Alcover_, Dec 06 2012, translated from Pari *)"
			],
			"program": [
				"(PARI) T(n,k)=if(2*n\u003ck || k\u003c0,0,if(n==0 \u0026\u0026 k==0,1,if(k==0,sum(j=0,2*n-2,T(n-1,j)), if(k\u003c=n,T(n,k-1)+2*sum(j=k-1,2*n-1-k,T(n-1,j)),T(n,2*n-k)))))",
				"(Haskell)",
				"a125053 n k = a125053_tabf !! n !! k",
				"a125053_row n = a125053_tabf !! n",
				"a125053_tabf = iterate f [1] where",
				"f zs = zs' ++ reverse (init zs') where",
				"zs' = (sum zs) : g (map (* 2) zs) (sum zs)",
				"g [x] y = [x + y]",
				"g xs y = y' : g (tail $ init xs) y' where y' = sum xs + y",
				"-- _Reinhard Zumkeller_, Mar 17 2012"
			],
			"xref": [
				"Cf. A008301, A000364 (secant numbers, which are the row sums), A125054 (central terms), A125055, A000182, A008282.",
				"Cf. A210111 (left half)."
			],
			"keyword": "nonn,tabf,nice",
			"offset": "0,3",
			"author": "_Paul D. Hanna_, Nov 21 2006, Dec 20 2006",
			"references": 8,
			"revision": 32,
			"time": "2017-06-14T00:14:04-04:00",
			"created": "2006-12-06T03:00:00-05:00"
		}
	]
}