{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A289192",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 289192,
			"data": "1,1,1,1,2,2,1,3,7,6,1,4,14,34,24,1,5,23,86,209,120,1,6,34,168,648,1546,720,1,7,47,286,1473,5752,13327,5040,1,8,62,446,2840,14988,58576,130922,40320,1,9,79,654,4929,32344,173007,671568,1441729,362880",
			"name": "A(n,k) = n! * Laguerre(n,-k); square array A(n,k), n\u003e=0, k\u003e=0, read by antidiagonals.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A289192/b289192.txt\"\u003eAntidiagonals n = 0..140, flattened\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/LaguerrePolynomial.html\"\u003eLaguerre Polynomial\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Laguerre_polynomials\"\u003eLaguerre polynomials\u003c/a\u003e",
				"\u003ca href=\"/index/La#Laguerre\"\u003eIndex entries for sequences related to Laguerre polynomials\u003c/a\u003e"
			],
			"formula": [
				"A(n,k) = n! * Sum_{i=0..n} k^i/i! * binomial(n,i).",
				"E.g.f. of column k: exp(k*x/(1-x))/(1-x).",
				"A(n, k) = (-1)^n*KummerU(-n, 1, -k). - _Peter Luschny_, Feb 12 2020",
				"A(n, k) = (2*n+k-1)*A(n-1, k) - (n-1)^2*A(n-2, k) for n \u003e 1. - _Seiichi Manyama_, Feb 03 2021"
			],
			"example": [
				"Square array A(n,k) begins:",
				":   1,    1,    1,     1,     1,     1, ...",
				":   1,    2,    3,     4,     5,     6, ...",
				":   2,    7,   14,    23,    34,    47, ...",
				":   6,   34,   86,   168,   286,   446, ...",
				":  24,  209,  648,  1473,  2840,  4929, ...",
				": 120, 1546, 5752, 14988, 32344, 61870, ..."
			],
			"maple": [
				"A:= (n,k)-\u003e n! * add(binomial(n, i)*k^i/i!, i=0..n):",
				"seq(seq(A(n, d-n), n=0..d), d=0..12);"
			],
			"mathematica": [
				"A[n_, k_] := n! * LaguerreL[n, -k];",
				"Table[A[n - k, k], {n, 0, 9}, {k, n, 0, -1}] // Flatten (* _Jean-François Alcover_, May 05 2019 *)"
			],
			"program": [
				"(Python)",
				"from sympy import binomial, factorial as f",
				"def A(n, k): return f(n)*sum(binomial(n, i)*k**i/f(i) for i in range(n + 1))",
				"for n in range(13): print([A(k, n - k) for k in range(n + 1)]) # _Indranil Ghosh_, Jun 28 2017",
				"(PARI) {T(n, k) = if(n\u003c2, k*n+1, (2*n+k-1)*T(n-1, k)-(n-1)^2*T(n-2, k))} \\\\ _Seiichi Manyama_, Feb 03 2021",
				"(PARI) T(n, k) = n!*pollaguerre(n, 0, -k); \\\\ _Michel Marcus_, Feb 05 2021"
			],
			"xref": [
				"Columns k=0-10 give: A000142, A002720, A087912, A277382, A289147, A289211, A289212, A289213, A289214, A289215, A289216.",
				"Rows n=0-2 give: A000012, A000027(k+1), A008865(k+2).",
				"Main diagonal gives A277373.",
				"Cf. A253286, A341014."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Alois P. Heinz_, Jun 28 2017",
			"references": 17,
			"revision": 51,
			"time": "2021-05-07T07:37:45-04:00",
			"created": "2017-06-28T11:05:48-04:00"
		}
	]
}