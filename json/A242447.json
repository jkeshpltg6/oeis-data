{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A242447",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 242447,
			"data": "1,0,1,0,1,1,0,3,0,1,0,3,4,0,1,0,5,6,4,0,1,0,11,10,5,5,0,1,0,13,21,18,5,6,0,1,0,19,40,34,21,6,7,0,1,0,27,87,59,40,27,7,8,0,1,0,57,121,132,100,49,35,8,9,0,1,0,65,219,272,210,131,63,44,9,10,0,1",
			"name": "Number T(n,k) of compositions of n in which the maximal multiplicity of parts equals k; triangle T(n,k), n\u003e=0, 0\u003c=k\u003c=n, read by rows.",
			"comment": [
				"T(0,0) = 1 by convention. T(n,k) counts the compositions of n in which at least one part has multiplicity k and no part has a multiplicity larger than k."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A242447/b242447.txt\"\u003eRows n = 0..140, flattened\u003c/a\u003e"
			],
			"example": [
				"T(6,1) = 11: [1,2,3], [1,3,2], [2,1,3], [2,3,1], [3,1,2], [3,2,1], [2,4], [4,2], [1,5], [5,1], [6].",
				"T(6,2) = 10: [1,1,2,2], [1,2,1,2], [1,2,2,1], [2,1,1,2], [2,1,2,1], [2,2,1,1], [3,3], [1,1,4], [1,4,1], [4,1,1].",
				"T(6,3) = 5: [2,2,2], [1,1,1,3], [1,1,3,1], [1,3,1,1], [3,1,1,1].",
				"T(6,4) = 5: [1,1,1,1,2], [1,1,1,2,1], [1,1,2,1,1], [1,2,1,1,1], [2,1,1,1,1].",
				"T(6,6) = 1: [1,1,1,1,1,1].",
				"Triangle T(n,k) begins:",
				"  1;",
				"  0,  1;",
				"  0,  1,   1;",
				"  0,  3,   0,   1;",
				"  0,  3,   4,   0,   1;",
				"  0,  5,   6,   4,   0,  1;",
				"  0, 11,  10,   5,   5,  0,  1;",
				"  0, 13,  21,  18,   5,  6,  0, 1;",
				"  0, 19,  40,  34,  21,  6,  7, 0, 1;",
				"  0, 27,  87,  59,  40, 27,  7, 8, 0, 1;",
				"  0, 57, 121, 132, 100, 49, 35, 8, 9, 0, 1;"
			],
			"maple": [
				"b:= proc(n, i, p, k) option remember; `if`(n=0, p!, `if`(i\u003c1, 0,",
				"      add(b(n-i*j, i-1, p+j, k)/j!, j=0..min(n/i, k))))",
				"    end:",
				"T:= (n, k)-\u003e b(n$2, 0, k) -`if`(k=0, 0, b(n$2, 0, k-1)):",
				"seq(seq(T(n, k), k=0..n), n=0..14);"
			],
			"mathematica": [
				"b[n_, i_, p_, k_] := b[n, i, p, k] = If[n == 0, p!, If[i\u003c1, 0, Sum[b[n - i*j, i-1, p + j, k]/j!, {j, 0, Min[n/i, k]}]]]; T[n_, k_] := b[n, n, 0, k] - If[k == 0, 0, b[n, n, 0, k-1]]; Table[Table[T[n, k], {k, 0, n}], {n, 0, 14}] // Flatten (* _Jean-François Alcover_, Jan 22 2015, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Columns k=0-10 give: A000007, A032020 (for n\u003e0), A243119, A243120, A243121, A243122, A243123, A243124, A243125, A243126, A243127.",
				"T(2n,n) = A232665(n).",
				"Row sums give A011782.",
				"Cf. A242451 (the same for minimal multiplicity)."
			],
			"keyword": "nonn,tabl",
			"offset": "0,8",
			"author": "_Alois P. Heinz_, May 15 2014",
			"references": 15,
			"revision": 27,
			"time": "2018-10-25T14:45:59-04:00",
			"created": "2014-05-15T09:00:30-04:00"
		}
	]
}