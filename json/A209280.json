{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A209280",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 209280,
			"data": "9,81,18,81,9,702,9,171,27,72,18,693,18,72,27,171,9,702,9,81,18,81,9,5913,9,81,18,81,9,1602,9,261,36,63,27,594,18,162,36,162,18,603,9,171,27,72,18,5814,9,171,27,72,18,603,9,261,36,63,27,1584,27,63,36,261,9",
			"name": "First difference of A050289 = numbers whose digits are a permutation of (1,...,9).",
			"comment": [
				"This sequence is the natural extension of A107346 (and others, see below) from 5!-1 to 9!-1 terms, which is the natural (since maximal) length, given that OEIS sequence data are stored as decimal numbers. On the other hand, it is quite different from A219664 in many aspects, not only for the reason that the other sequence is infinite and therefore differs from this one in all terms beyond n = 9!-1.",
				"The sequence is finite, with 9!-1 terms, and symmetric: a(n)=a(9!-n).",
				"All terms are multiples of 9, cf. formula.",
				"The subsequence of the first n!-1 terms (n=2,...,9) yields the first differences of the sequence of numbers whose digits are a permutation of (1,...,n):",
				"The first 8!-1 terms yield the first differences of A178478: numbers whose digits are a permutation of 12345678.",
				"The first 7!-1 terms yield the first differences of A178477: numbers whose digits are a permutation of 1234567.",
				"The first 6!-1 terms yield the first differences of A178476: numbers whose digits are a permutation of 123456.",
				"The first 5!-1 terms yield A107346, the first differences of A178475: numbers whose digits are a permutation of 12345."
			],
			"formula": [
				"a(n) = A219664(n) = 9*A217626(n) (for n \u003c 9!). - _M. F. Hasler_, Jan 12 2013",
				"a(n) = a(m!-n) for any m \u003c 10 such that n \u003c m!."
			],
			"example": [
				"The same initial terms are obtained for the permutations of any set of the form {1,...,m}, e.g., {1,2,3} or {1,...,9}: In the first case we have P = (123,132,213,231,312,321) and P(4)-P(3) = 231 - 213 = 18 = a(3), and in the latter case P(4)-P(3) = 123456897 - 123456879 = 18, again. - _M. F. Hasler_, Jan 12 2013"
			],
			"mathematica": [
				"Take[Differences[Sort[FromDigits/@Permutations[Range[9]]]],70] (* _Harvey P. Dale_, Mar 31 2018 *)"
			],
			"program": [
				"(PARI) A209280_list(N=5)={my(v=vector(N,i,10^(N-i))~); v=vecsort(vector(N!,k,numtoperm(N,k)*v)); vecextract(v,\"^1\")-vecextract(v,\"^-1\")} \\\\ return the N!-1 first terms as a vector",
				"(PARI) A209280(n)={if(a209280=='a209280 || #a209280\u003cn, a209280=A209280_list(A090529(n+1)));a209280[n]}"
			],
			"xref": [
				"Cf. A030299, A219664."
			],
			"keyword": "easy,nonn,base,fini",
			"offset": "1,1",
			"author": "_M. F. Hasler_, Jan 12 2013",
			"references": 7,
			"revision": 26,
			"time": "2020-02-27T03:03:01-05:00",
			"created": "2013-01-15T23:12:36-05:00"
		}
	]
}