{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A106597",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 106597,
			"data": "1,1,1,1,3,1,1,5,5,1,1,7,14,7,1,1,9,27,27,9,1,1,11,44,72,44,11,1,1,13,65,149,149,65,13,1,1,15,90,266,388,266,90,15,1,1,17,119,431,836,836,431,119,17,1,1,19,152,652,1585,2150,1585,652,152,19,1,1,21,189,937,2743,4753,4753,2743,937,189,21,1",
			"name": "Triangle T(n,k) = T(n-1, k-1) + T(n-1, k) + Sum_{i \u003e= 1} T(n-2*i, k-i), with T(n, 0) = T(n, n) = 1, read by rows.",
			"comment": [
				"Next term is sum of two terms above you in previous row (as in Pascal's triangle A007318) plus sum of terms directly above you on a vertical line.",
				"T(n,n-k) is the number of lattice paths from (0,0) to (n,k) using steps (1,0), (0,1), and (s,s) for s\u003e=1. - _Joerg Arndt_, Jul 01 2011",
				"Row sums gives A118649. - _Emanuele Munarini_, Feb 01 2017"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A106597/b106597.txt\"\u003eRows n = 0..50 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"G.f.: (1-x^2*y)/(1-x-x*y-2*x^2*y+x^3*y+x^3*y^2). - _Emanuele Munarini_, Feb 01 2017"
			],
			"example": [
				"Triangle begins:",
				"  1;",
				"  1,  1;",
				"  1,  3,   1;",
				"  1,  5,   5,   1;",
				"  1,  7,  14,   7,   1;",
				"  1,  9,  27,  27,   9,   1;",
				"  1, 11,  44,  72,  44,  11,   1;",
				"  1, 13,  65, 149, 149,  65,  13,   1;",
				"  1, 15,  90, 266, 388, 266,  90,  15,  1;",
				"  1, 17, 119, 431, 836, 836, 431, 119, 17, 1;"
			],
			"mathematica": [
				"CoefficientList[#, y]\u0026 /@ CoefficientList[(1 -x^2*y)/(1 -x -x*y -2x^2*y +x^3*y + x^3*y^2) + O[x]^12, x]//Flatten (* _Jean-François Alcover_, Oct 30 2018, after _Emanuele Munarini_ *)"
			],
			"program": [
				"(PARI) /* same as in A092566, but last line (output) replaced by the following */",
				"/* show as triangle T(n-k,k): */",
				"{ for(n=0,N-1, for(k=0,n, print1(T(n-k,k),\", \"); ); print(); ); }",
				"/* _Joerg Arndt_, Jul 01 2011 */",
				"(Sage)",
				"@CachedFunction",
				"def T(n, k):",
				"    if (k\u003c0): return 0",
				"    elif (k==0 or k==n): return 1",
				"    else: return + T(n-1, k-1) + T(n-1, k) + sum( T(n-2*j, k-j) for j in (1..min(k, n//2, n-k)))",
				"flatten([[T(n, k) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, Sep 08 2021"
			],
			"xref": [
				"Cf. A007318, A118649.",
				"T(2n,n) gives A118650."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_, May 30 2005",
			"ext": [
				"More terms from _Joshua Zucker_, May 10 2006",
				"Definition corrected by _Emilie Hogan_, Oct 15 2009"
			],
			"references": 3,
			"revision": 37,
			"time": "2021-09-10T02:05:26-04:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}