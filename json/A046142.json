{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A046142",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 46142,
			"data": "1,33,185,553,1233,2321,3913,6105,8993,12673,17241,22793,29425,37233,46313,56761,68673,82145,97273,114153,132881,153553,176265,201113,228193,257601,289433,323785,360753,400433,442921,488313,536705,588193,642873,700841",
			"name": "Haüy rhombic dodecahedral numbers.",
			"comment": [
				"The Haüy rhombic dodecahedral formula is remarkably similar to that of A254473, the 24-hedral numbers: a(n) = (2*n+1)*(8*n^2+14*n+7). Compare with (2*n-1)*(8*n^2-14*n+7); the differences are simple: (1) the first factor of the dodecahedral formula has \"+1\" and the 24-hedral formula has \"-1\"; (2) the second factor of the former has \"-14n\" and the latter has \"+14n\". Note that the rhombic dodecahedron has 24 edges. The difference between these sequences is diff(n) = 72*n^2 + 14. - _Peter M. Chema_, Jan 09 2016",
				"Named after the French priest and mineralogist René Just Haüy (1743-1822). - _Amiram Eldar_, Jun 22 2021"
			],
			"reference": [
				"H. Steinhaus, Mathematical Snapshots, 3rd ed. New York: Dover, pp. 185-186, 1999."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A046142/b046142.txt\"\u003eTable of n, a(n) for n = 1..5000\u003c/a\u003e",
				"R.-J. Haüy, \u003ca href=\"http://gallica.bnf.fr/ark:/12148/bpt6k1060890\"\u003eEssai d'une théorie sur la structure des crystaux appliquée à plusieurs genres de substances crystallisées\u003c/a\u003e, 1784.",
				"Jonathan Vos Post, \u003ca href=\"http://magicdragon.com/poly.html\"\u003eTable of Polytope Numbers, Sorted, Through 1,000,000\u003c/a\u003e which lists Haüy rhombic dodecahedral numbers as \"RhoDod(n).\"",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/HauyConstruction.html\"\u003eHauy Construction\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RhombicDodecahedralNumber.html\"\u003eRhombic Dodecahedral Number\u003c/a\u003e.",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-6,4,-1)."
			],
			"formula": [
				"a(n) = (2*n - 1)*(8*n^2 - 14*n + 7).",
				"G.f.: x*(7*x^3 +59*x^2 +29*x +1)/(1-x)^4. - _Colin Barker_, Oct 26 2012",
				"a(n) = A016755(n) + A069072(n-1). - _Luciano Ancora_, Mar 23 2015",
				"a(n) = A016755(n) + 6*A000447(n-1). - _Luciano Ancora_, Mar 23 2015",
				"a(n) = 4*a(n-1)-6*a(n-2)+4*a(n-3)-a(n-4) for n\u003e4. - _Wesley Ivan Hurt_, Mar 02 2016",
				"E.g.f.: (-7 +8*x +12*x^2 +16*x^3)*exp(x) + 7. - _G. C. Greubel_, Nov 04 2017"
			],
			"maple": [
				"A046142:=n-\u003e(2*n-1)*(8*n^2-14*n+7): seq(A046142(n), n=1..50); # _Wesley Ivan Hurt_, Mar 02 2016"
			],
			"mathematica": [
				"Table[(2 n - 1) (8 n^2 - 14 n + 7), {n, 40}] (* _Vincenzo Librandi_, Mar 29 2015 *)",
				"LinearRecurrence[{4, -6, 4, -1}, {1, 33, 185, 553}, 20] (* _Eric W. Weisstein_, Sep 27 2017 *)",
				"CoefficientList[Series[(1 + 29 x + 59 x^2 + 7 x^3)/(-1 + x)^4, {x, 0, 20}], x] (* _Eric W. Weisstein_, Sep 27 2017 *)"
			],
			"program": [
				"(PARI) Vec(x*(7*x^3+59*x^2+29*x+1)/(x-1)^4 + O(x^50)) \\\\ _Michel Marcus_, Mar 24 2015",
				"(MAGMA) [(2*n-1)*(8*n^2-14*n+7): n in [1..40]]; // _Vincenzo Librandi_, Mar 29 2015"
			],
			"xref": [
				"Cf. A000447, A001845, A016755, A069072, A254473."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Eric W. Weisstein_",
			"references": 5,
			"revision": 69,
			"time": "2021-06-22T12:28:01-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}