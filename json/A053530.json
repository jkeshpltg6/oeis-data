{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A053530",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 53530,
			"data": "1,0,1,3,7,35,171,847,5041,32643,223705,1659581,13182159,110802133,984241363,9212696235,90477239521,929604133343,9969157068273,111329454692485,1291932988047775,15550838026589061,193833398512358011,2498039016973836491",
			"name": "Expansion of e.g.f.: exp(-x - x^2/2 + x*exp(x)).",
			"comment": [
				"The number of simple labeled graphs on n nodes whose connected components are stars. - _Geoffrey Critzer_, Dec 10 2011",
				"Equivalently, the number of minimal edge covers of the complete graph K_n. - _Andrew Howroyd_, Aug 04 2017"
			],
			"reference": [
				"R. P. Stanley, Enumerative Combinatorics, Cambridge, Vol. 2, 1999; see Problem 5.15(b)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A053530/b053530.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"Vaclav Kotesovec, \u003ca href=\"http://oeis.org/A216688/a216688.pdf\"\u003eAsymptotic solution of the equations using the Lambert W-function\u003c/a\u003e",
				"Vladimir Kruchinin, D. V. Kruchinin, \u003ca href=\"http://arxiv.org/abs/1103.2582\"\u003eComposita and their properties \u003c/a\u003e, arXiv:1103.2582 [math.CO], 2011.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CompleteGraph.html\"\u003eComplete Graph\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/MinimalEdgeCover.html\"\u003eMinimal Edge Cover\u003c/a\u003e"
			],
			"formula": [
				"a(n) = n!*Sum_{k=1..n} (1/k!)*( binomial(k, n-k)*2^(k-n)*(-1)^k + Sum_{j=1..k} binomial(k,j)* (Sum_{i=j..n-k+j} (j^(i-j)/(i-j)! * binomial(k-j,n-i-k+j)*(1/2)^(n-i-k+j)*(-1)^(k-j)) ) ), n\u003e0. - _Vladimir Kruchinin_, Sep 10 2010",
				"From _Vaclav Kotesovec_, Aug 06 2014: (Start)",
				"a(n) ~ n^n / (exp(r^2/2 + n*r/(1+r)) * r^n * sqrt(r^2*(1+r)/n + 2+r-1/(1+r))), where r is the root of the equation r*(exp(r)*(1+r)-1-r) = n.",
				"(a(n)/n!)^(1/n) ~ exp(1/(2*LambertW(sqrt(n)/2)))/(2*LambertW(sqrt(n)/2)).",
				"(End)"
			],
			"mathematica": [
				"nn = 30; a = x Exp[x]; Range[0, nn]! CoefficientList[Series[Exp[a - x^2/2! - x], {x, 0, nn}], x] (* _Geoffrey Critzer_, Dec 10 2011 *)",
				"CoefficientList[Series[Exp[-x - x^2/2 + x Exp[x]], {x, 0, 30}], x] Range[0, 30]! (* _Eric W. Weisstein_, Aug 10 2017 *)",
				"Table[n! Sum[1/k! (Binomial[k, n-k] 2^(k-n) (-1)^k + Sum[Binomial[k, j] Sum[j^(i-j)/(i-j)! Binomial[k-j, n-i-k+j] 2^(i-j+k-n) (-1)^(k-j), {i, j, n-k+j}], {j, k}]), {k, n}], {n, 30}] (* _Eric W. Weisstein_, Aug 10 2017 *)"
			],
			"program": [
				"(Maxima) a(n):=n!*sum((binomial(k,n-k)*2^(k-n)*(-1)^k +sum(binomial(k,j) *sum(j^(i-j)/(i-j)!*binomial(k-j,n-i-k+j)*(1/2)^(n-i-k+j)*(-1)^(k-j),i,j,n-k+j),j,1,k))/k!,k,1,n); /* _Vladimir Kruchinin_, Sep 10 2010 */",
				"(PARI) x='x+O('x^30); Vec(serlaplace(exp(-x-1/2*x^2+x*exp(x)))) \\\\ _Altug Alkan_, Aug 10 2017",
				"(MAGMA) m:=30; R\u003cx\u003e:=PowerSeriesRing(Rationals(), m); b:=Coefficients(R!( Exp(-x -x^2/2 +x*Exp(x)) )); [Factorial(n-1)*b[n]: n in [1..m]]; // _G. C. Greubel_, May 15 2019",
				"(Sage) m = 30; T = taylor(exp(-x -x^2/2 +x*exp(x)), x, 0, m); [factorial(n)*T.coefficient(x, n) for n in (0..m)] # _G. C. Greubel_, May 15 2019"
			],
			"xref": [
				"Cf. A000248, A210655."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_, Jan 16 2000",
			"references": 9,
			"revision": 46,
			"time": "2019-05-16T02:08:44-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}