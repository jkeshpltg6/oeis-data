{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A061420",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 61420,
			"data": "0,1,2,3,3,4,4,4,5,5,5,5,6,6,6,6,6,6,7,7,7,7,7,7,7,7,7,8,8,8,8,8,8,8,8,8,8,8,8,8,8,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10",
			"name": "a(n) = a(ceiling((n-1)*2/3)) + 1 with a(0) = 0.",
			"comment": [
				"Least k such that f^(k)(n) = 0 where f(x) = floor(2/3*x) and f^(k+1)(x) = f(f^(k)(x)). - _Benoit Cloitre_, May 26 2007",
				"Number of 3:2 compressor stages in a Wallace tree multiplier starting with (n+2) partial products. - _Chinmaya Dash_, Aug 18 2020"
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A061420/b061420.txt\"\u003eTable of n, a(n) for n = 0..2000\u003c/a\u003e",
				"K. A. C. Bickerstaff, M. Schulte and E. E. Swartzlander, \u003ca href=\"https://doi.ieeecomputersociety.org/10.1109/ASAP.1993.397168\"\u003eReduced area multipliers\u003c/a\u003e, Proceedings of International Conference on Application Specific Array Processors (ASAP '93), Venice, Italy, 1993, pp. 478-489. See Table 1 p. 480.",
				"William J. Gilbert, \u003ca href=\"https://doi.org/10.1016/0022-247X(81)90262-6\"\u003eRadix Representations of Quadratic Fields\u003c/a\u003e, Journal of Mathematical Analysis and Applications 83 (1981) pp 264-274.  Gilbert (page 273) cites Wang and Washburn (below) in connection with the length of the base 3/2 expansion of the even positive integers.",
				"A. M. Odlyzko and H. S. Wilf, \u003ca href=\"https://doi.org/10.1017/S0017089500008272\"\u003eFunctional iteration and the Josephus problem\u003c/a\u003e, Glasgow Math. J. 33, 235-240, 1991.",
				"E. T. H. Wang, Phillip C. Washburn, \u003ca href=\"http://www.jstor.org/stable/2322068\"\u003eProblem E2604\u003c/a\u003e, American Mathematical Monthly 84 (1977) pp. 821-822.",
				"\u003ca href=\"/index/J#Josephus\"\u003eIndex entries for sequences related to the Josephus Problem\u003c/a\u003e"
			],
			"formula": [
				"a(n) = a(n-1) + 1 if n is in A061419; a(n) = a(n-1) otherwise.",
				"From _Clark Kimberling_, Oct 19 2012: (Start)",
				"a(n) = a(floor(2*n/3)) + 1, where a(0) = 0 (alternative definition).",
				"Washburn's solution of Problem E2604 (see References) shows that (for n\u003e0), a(n) = -floor(-L((n+1)/c)), where L is the logarithm with base 3/2 and",
				"  c = lim_{n-\u003einfinity} (2/3)^n*s(n) where s(n) = floor(3*s(n-1)/2) + 1 and s(0)=0.  The editors state that \"It may be interesting to know whether c is irrational or even transcendental\"; c = 1.62227050288476731595695098289932... .",
				"Odlyzko and Wilf also discuss the defining recurrence, and they, after Washburn, give a formula for the sequence using c, as in the third Mathematica program below.",
				"(End)"
			],
			"example": [
				"a(10) = a(ceiling(9*2/3)) + 1 = a(6) + 1 = 4 + 1 = 5."
			],
			"maple": [
				"a:= n-\u003e `if`(n=0, 0, a(ceil((n-1)*2/3))+1):",
				"seq(a(n), n=0..100);  # _Alois P. Heinz_, Oct 29 2012"
			],
			"mathematica": [
				"(* 1st program, using the alternative definition *)",
				"a[0] = 0; a[n_] := a[Floor[2 n/3]] + 1;",
				"Table[a[n], {n, 0, 120}]",
				"(* 2nd program, using Cloitre's recurrence *)",
				"f[x_] := Floor[2 x/3]; g[0, x_] := f[x];",
				"g[k_, x_] := f[g[k - 1, x]];",
				"u[n_] := Flatten[Table[g[k, n], {k, 0, 12}]]",
				"v[n_] := First[Position[u[n], 0]];",
				"Flatten[Table[v[n], {n, 1, 120}]]",
				"(* 3rd program, using the constant c *)",
				"f[n_] := -Floor[-Log[3/2, (n + 1)/1.62227050288476731595695098289932]]",
				"Table[f[n], {n, 1, 120}]",
				"(* _Clark Kimberling_, Oct 23 2012 *)"
			],
			"program": [
				"(PARI) a(n) = if(n\u003c0, 0, s=n; c=0; while(floor(s)\u003e0, s=floor(2/3*s); c++); c) \\\\ _Benoit Cloitre_, May 26 2007",
				"(MAGMA) [IsZero(n) select 0 else Self(Floor(2*n/3)+1)+1: n in [0..90]]; // _Bruno Berselli_, Oct 31 2012"
			],
			"xref": [
				"Cf. A029837, A061419, A083286 (the constant c)."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Henry Bottomley_, May 02 2001",
			"references": 3,
			"revision": 71,
			"time": "2020-09-05T03:40:49-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}