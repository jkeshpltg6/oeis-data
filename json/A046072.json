{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A046072",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 46072,
			"data": "1,1,1,1,1,1,1,2,1,1,1,2,1,1,2,2,1,1,1,2,2,1,1,3,1,1,1,2,1,2,1,2,2,1,2,2,1,1,2,3,1,2,1,2,2,1,1,3,1,1,2,2,1,1,2,3,2,1,1,3,1,1,2,2,2,2,1,2,2,2,1,3,1,1,2,2,2,2,1,3,1,1,1,3,2,1,2,3,1,2,2,2,2,1,2,3,1,1,2,2,1,2",
			"name": "Decompose multiplicative group of integers modulo n as a product of cyclic groups C_{k_1} x C_{k_2} x ... x C_{k_m}, where k_i divides k_j for i \u003c j; then a(n) = m.",
			"comment": [
				"The multiplicative group modulo n can be written as the direct product of a(n) (but not fewer) cyclic groups. - _Joerg Arndt_, Dec 25 2014",
				"a(n) = 1 (that is, the multiplicative group modulo n is cyclic) iff n is in A033948, or equivalently iff A034380(n)=1. - _Max Alekseyev_, Jan 07 2015",
				"This sequence gives the minimal number of generators of the multiplicative group of integers modulo n which is isomorphic to the Galois group Gal(Q(zeta_n)/Q), with zeta_n =exp(2*Pi*I/n). See, e.g., Theorem 9.1.11., p. 235 of the Cox reference. See also the table of the Wikipedia link. - _Wolfdieter Lang_, Feb 28 2017",
				"In this factorization the trivial group C_1 = {1} is allowed as a factor only for n = 0 and 1 (otherwise one could have arbitrarily many leading C_1 factors for n \u003e= 3). - _Wolfdieter Lang_, Mar 07 2017"
			],
			"reference": [
				"Cox, David A., Galois Theory, John Wiley \u0026 Sons, Hoboken, New Jrsey, 2004, 235.",
				"Shanks, D. Solved and Unsolved Problems in Number Theory, 4th ed. New York: Chelsea, pp. 92-93, 1993."
			],
			"link": [
				"Joerg Arndt, \u003ca href=\"/A046072/b046072.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ModuloMultiplicationGroup.html\"\u003eModulo Multiplication Group.\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Multiplicative_group_of_integers_modulo_n\"\u003eMultiplicative group of integers modulo n\u003c/a\u003e. See the table at the end."
			],
			"formula": [
				"a(n) = A001221(n) - 1 if n \u003e 2 is divisible by 2 and not by 4, a(n) = A001221(n) + 1 if n is divisible by 8, a(n) = A001221(n) in other cases. - _Ivan Neretin_, Aug 01 2016"
			],
			"mathematica": [
				"f[n_] := Which[OddQ[n], PrimeNu[n], EvenQ[n] \u0026\u0026 ! IntegerQ[n/4],",
				"  PrimeNu[n] - 1, IntegerQ[n/4] \u0026\u0026 ! IntegerQ[n/8], PrimeNu[n],",
				"  IntegerQ[n/8], PrimeNu[n] + 1]; Join[{1, 1},",
				"Table[f[n], {n, 3, 102}]] (* _Geoffrey Critzer_, Dec 24 2014 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c=2, 1, #znstar(n)[3]); \\\\ _Joerg Arndt_, Aug 26 2014"
			],
			"xref": [
				"Cf. A046073 (number of squares in multiplicative group modulo n), A281855, A282625 (for total factorization).",
				"a(n)=k iff n is in: A033948 (k=1), A272592 (k=2), A272593 (k=3), A272594 (k=4), A272595 (k=5), A272596 (k=6), A272597 (k=7), A272598 (k=8), A272599 (k=9)."
			],
			"keyword": "nonn,nice",
			"offset": "1,8",
			"author": "_Eric W. Weisstein_",
			"references": 34,
			"revision": 80,
			"time": "2021-03-19T10:06:59-04:00",
			"created": "2016-12-17T19:29:37-05:00"
		}
	]
}