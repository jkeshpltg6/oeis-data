{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A245111",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 245111,
			"data": "1,0,1,0,1,3,0,1,12,10,0,1,35,90,35,0,1,90,525,560,126,0,1,217,2520,5460,3150,462,0,1,504,10836,42000,46200,16632,1716,0,1,1143,43470,280665,519750,342342,84084,6435,0,1,2550,166375,1709400,4969965,5297292,2312310,411840,24310",
			"name": "G.f.: A(x,y) = Sum_{n\u003e=0} exp(-y/(1-n*x)) * y^n/(1-n*x)^n / n!.",
			"comment": [
				"Compare g.f. to: 1/(1-x*y) = Sum_{n\u003e=0} exp(-y*(1+n*x)) * y^n*(1+n*x)^n / n!.",
				"Row sums equal A245110.",
				"Antidiagonal sums: A218667.",
				"Main diagonal is: C(2*n-1,n) (A001700).",
				"Secondary diagonal: C(2*n-1,n)*n^2 (A002544)."
			],
			"link": [
				"Paul D. Hanna, \u003ca href=\"/A245111/b245111.txt\"\u003eTable of n, a(n), of flattened triangle for rows 0..32\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = Stirling2(n, k) * binomial(n+k-1, k-1) for k\u003e0, where Stirling2(n,k) = A048993(n,k)."
			],
			"example": [
				"G.f.: A(x,y) = 1 + x*y + x^2*(y + 3*y^2)",
				"+ x^3*(y + 12*y^2 + 10*y^3)",
				"+ x^4*(y + 35*y^2 + 90*y^3 + 35*y^4)",
				"+ x^5*(y + 90*y^2 + 525*y^3 + 560*y^4 + 126*y^5)",
				"+ x^6*(y + 217*y^2 + 2520*y^3 + 5460*y^4 + 3150*y^5 + 462*y^6) +...",
				"where",
				"A(x,y) = exp(-y) + exp(-y/(1-x))*y/(1-x) + (exp(-y/(1-2*x))*y^2/(1-2*x)^2)/2!",
				"+ (exp(-y/(1-3*x))*y^3/(1-3*x)^3)/3! + (exp(-y/(1-4*x))*y^4/(1-4*x)^4)/4!",
				"+ (exp(-y/(1-5*x))*y^5/(1-5*x)^5)/5! + (exp(-y/(1-6*x))*y^6/(1-6*x)^6)/6!",
				"+ (exp(-y/(1-7*x))*y^7/(1-7*x)^7)/7! + (exp(-y/(1-8*x))*y^8/(1-8*x)^8)/8! +...",
				"simplifies to a power series with only integer coefficients of x^n*y^k.",
				"Triangle begins:",
				"1;",
				"0, 1;",
				"0, 1, 3;",
				"0, 1, 12, 10;",
				"0, 1, 35, 90, 35;",
				"0, 1, 90, 525, 560, 126;",
				"0, 1, 217, 2520, 5460, 3150, 462;",
				"0, 1, 504, 10836, 42000, 46200, 16632, 1716;",
				"0, 1, 1143, 43470, 280665, 519750, 342342, 84084, 6435;",
				"0, 1, 2550, 166375, 1709400, 4969965, 5297292, 2312310, 411840, 24310;",
				"0, 1, 5621, 615780, 9754030, 42567525, 68549481, 47087040, 14586000, 1969110, 92378; ...",
				"where T(n,k) = A048993(n,k) * C(n+k-1, k-1) for k\u003e0."
			],
			"program": [
				"(PARI) /* From definition: */",
				"{T(n,k)=local(A=1+x*y); A=sum(k=0, n, 1/(1-k*x+x*O(x^n))^k*y^k/k!*exp(-y/(1-k*x+x*O(x^n))+y*O(y^n))); polcoeff(polcoeff(A, n,x),k,y)}",
				"for(n=0, 10, for(k=0,n, print1(T(n,k),\", \"));print(\"\"))",
				"(PARI) /* From T(n,k) = Stirling2(n, k) * C(n+k-1, k-1) */",
				"{Stirling2(n, k) = sum(j=0, k, (-1)^(k+j) * binomial(k, j) * j^n) / k!}",
				"{T(n,k)=if(k==0,0^n,Stirling2(n, k) * binomial(n+k-1, k-1))}",
				"for(n=0, 10, for(k=0,n, print1(T(n,k),\", \"));print(\"\"))"
			],
			"xref": [
				"Cf. A245110, A218667, A001700, A002544, A048993."
			],
			"keyword": "nonn,tabl",
			"offset": "0,6",
			"author": "_Paul D. Hanna_, Jul 12 2014",
			"references": 3,
			"revision": 17,
			"time": "2014-07-30T16:27:18-04:00",
			"created": "2014-07-12T23:22:43-04:00"
		}
	]
}