{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A244098",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 244098,
			"data": "1,2,2,3,2,5,2,4,3,5,2,9,2,5,5,5,2,9,2,9,5,5,2,14,3,5,4,9,2,16,2,6,5,5,5,19,2,5,5,14,2,16,2,9,9,5,2,20,3,9,5,9,2,14,5,14,5,5,2,35,2,5,9,7,5,16,2,9,5,16,2,34,2,5,9,9,5,16,2,20,5,5",
			"name": "Total number of divisors of all the ordered prime factorizations of an integer.",
			"comment": [
				"a(n) = total number of ordered prime factorizations dividing all possible ordered prime factorizations making up n.",
				"Example: for n = 12; a(12) = 9 because 12 = 2*2*3 = 2*3*2 = 3*2*2 the divisors of which are 1, 2, 3, 2*2, 2*3, 3*2, 2*2*3, 2*3*2, 3*2*2. This makes 9 ordered prime factorizations dividing all those making up 12.",
				"Dirichlet convolution of A008480 with A000012."
			],
			"link": [
				"Pierre-Louis Giscard, \u003ca href=\"/A244098/b244098.txt\"\u003eTable of n, a(n) for n = 1..5000\u003c/a\u003e"
			],
			"formula": [
				"Dirichlet generating function: Zeta(s)/(1-P(s)) with Zeta(s) the Riemann zeta function and P(s) the prime zeta function.",
				"G.f. A(x) satisfies: A(x) = x / (1 - x) + Sum_{k\u003e=1} A(x^prime(k)). - _Ilya Gutkovskiy_, May 30 2020"
			],
			"example": [
				"For n = 6; a(6) = 5 because 6 = 2*3 = 3*2, the divisors of which are 1, 2, 3, 2*3, 3*2. This makes 5 ordered prime factorizations dividing all those making up 6.",
				"For n = 12; a(12) = 9 because 12 = 2*2*3 = 2*3*2 = 3*2*2, the divisors of which are 1, 2, 3, 2*2, 2*3, 3*2, 2*2*3, 2*3*2, 3*2*2. This makes 9 ordered prime factorizations dividing all those making up 12.",
				"For n prime, a(n) = 2 because a prime n has a single ordered prime factorization n with divisors 1 and n. This makes two ordered prime factorizations dividing that making up n."
			],
			"mathematica": [
				"f[s_]=Zeta[s]/(1-PrimeZetaP[s]); (* Dirichlet g.f *)",
				"(* or *)",
				"Clear[a, b];",
				"a = Prepend[",
				"   Array[Multinomial @@ Last[Transpose[FactorInteger[#]]] \u0026, 200, 2],",
				"   1];",
				"b = Table[1, {u, 1, Length[a]}];",
				"Table[Sum[If[IntegerQ[p/n], b[[n]] a[[p/n]], 0], {n, 1, p}], {p, 1,",
				"  Length[a]}]"
			],
			"xref": [
				"Cf. A000012, A008480."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Pierre-Louis Giscard_, Jun 20 2014",
			"references": 1,
			"revision": 30,
			"time": "2020-05-30T19:14:00-04:00",
			"created": "2014-06-25T10:16:39-04:00"
		}
	]
}