{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A055483",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 55483,
			"data": "1,2,3,4,5,6,7,8,9,1,11,3,1,1,3,1,1,9,1,2,3,22,1,6,1,2,9,2,1,3,1,1,33,1,1,9,1,1,3,4,1,6,1,44,9,2,1,12,1,5,3,1,1,9,55,1,3,1,1,6,1,2,9,2,1,66,1,2,3,7,1,9,1,1,3,1,77,3,1,8,9,2,1,12,1,2,3,88,1,9,1,1,3,1,1,3,1,1,99,1,101,3,1,1,3,1,1,9,1,11,111",
			"name": "a(n) = GCD of n and the reverse of n.",
			"comment": [
				"a(A226778(n)) = 1; a(A071249(n)) \u003e 1. - _Reinhard Zumkeller_, Jun 18 2013",
				"a(n) = n iff n \u003e= 1 is a palindrome (n is in A002113). - _Felix Fröhlich_, Oct 28 2014"
			],
			"link": [
				"T. D. Noe and Indranil Ghosh, \u003ca href=\"/A055483/b055483.txt\"\u003eTable of n, a(n) for n = 1..50000\u003c/a\u003e (first 1000 terms from T. D. Noe)"
			],
			"formula": [
				"a(n) = gcd(n, A004086(n)). - _Felix Fröhlich_, Oct 28 2014",
				"3 | a(n) if 3 | n and 9 | a(n) if 9 | n. - _Alonso del Arte_, Aug 31 2021"
			],
			"example": [
				"a(12) = 3 since gcd(12, 21) = 3.",
				"a(13) = 1 since 13 and 31 are coprime.",
				"a(101) = gcd(101, 101) = 101."
			],
			"maple": [
				"R:=proc(q) local x,k; x:=convert(q,base,10);",
				"add(x[k]*10^(nops(x)-k),k=1..nops(x)); end:",
				"seq(gcd(i,R(i)),i=1..111); # _Paolo P. Lava_, Mar 02 2018"
			],
			"mathematica": [
				"gcn[n_] := GCD[n, IntegerReverse[n]]; Array[gcn, 120] (* _Harvey P. Dale_, Jan 23 2012 *)"
			],
			"program": [
				"(Haskell)",
				"a055483 n = gcd n $ a004086 n  -- _Reinhard Zumkeller_, Jun 18 2013",
				"(PARI) a004086(n)=eval(concat(Vecrev(Str(n))))",
				"a(n)=gcd(n, a004086(n)) \\\\ _Felix Fröhlich_, Oct 28 2014",
				"(MAGMA) [Gcd(n, Seqint(Reverse(Intseq(n)))): n in [1..100]]; // _Vincenzo Librandi_, Oct 29 2014",
				"(Scala) def reverseDigits(n: Int): Int = Integer.parseInt(n.toString.reverse)",
				"def euclGCD(a: Int, b: Int): Int = b match { case 0 =\u003e a; case n =\u003e Math.abs(euclGCD(b, a % b)) }",
				"(1 to 120).map(n =\u003e euclGCD(n, reverseDigits(n))) // _Alonso del Arte_, Aug 31 2021",
				"(Python)",
				"from math import gcd",
				"def a(n): return gcd(n, int(str(n)[::-1]))",
				"print([a(n) for n in range(1, 112)]) # _Michael S. Branicky_, Aug 31 2021"
			],
			"xref": [
				"Different from A069652, first differs at a(101), since gcd(101, 110) = 1.",
				"Cf. A002113, A004086, A071249, A226778."
			],
			"keyword": "base,easy,nonn,nice",
			"offset": "1,2",
			"author": "_Erich Friedman_, Jun 27 2000",
			"ext": [
				"Edited by _Robert G. Wilson v_, Apr 10 2002"
			],
			"references": 21,
			"revision": 53,
			"time": "2021-09-03T13:19:54-04:00",
			"created": "2000-07-22T03:00:00-04:00"
		}
	]
}