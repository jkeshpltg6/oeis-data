{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A153648",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 153648,
			"data": "2,5,5,2,46,2,2,123,123,2,2,155,936,155,2,2,187,2936,2936,187,2,2,219,5448,19912,5448,219,2,2,251,8472,69400,69400,8472,251,2,2,283,12008,159592,437480,159592,12008,283,2,2,315,16056,298680,1638072,1638072,298680,16056,315,2",
			"name": "Triangle T(n, k) = T(n-1, k) + T(n-1, k-1) + j*prime(j)*T(n-2, k-1) with j=3, read by rows.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A153648/b153648.txt\"\u003eRows n = 1..50 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = T(n-1, k) + T(n-1, k-1) + j*prime(j)*T(n-2, k-1) with j=3.",
				"From _G. C. Greubel_, Mar 04 2021: (Start)",
				"T(n,k,p,q,j) = T(n-1,k,p,q,j) + T(n-1,k-1,p,q,j) + (p*j+q)*prime(j)*T(n-2,k-1,p,q,j) with T(2,k,p,q,j) = prime(j), T(3,2,p,q,j) = 2*prime(j)^2 -4, T(4,2,p,q,j) = T(4,3,p,q,j) = prime(j)^2 -2, T(n,1,p,q,j) = T(n,n,p,q,j) = 2 and (p,q,j) = (1,0,3).",
				"Sum_{k=0..n} T(n,k,p,q,j) = 2*prime(j)^(n-1), for (p,q,j) = (1,0,3), = 2*A000351(n-1). (End)"
			],
			"example": [
				"Triangle begins as:",
				"  2;",
				"  5,   5;",
				"  2,  46,     2;",
				"  2, 123,   123,      2;",
				"  2, 155,   936,    155,       2;",
				"  2, 187,  2936,   2936,     187,       2;",
				"  2, 219,  5448,  19912,    5448,     219,      2;",
				"  2, 251,  8472,  69400,   69400,    8472,    251,     2;",
				"  2, 283, 12008, 159592,  437480,  159592,  12008,   283,   2;",
				"  2, 315, 16056, 298680, 1638072, 1638072, 298680, 16056, 315, 2;"
			],
			"mathematica": [
				"T[n_, k_, p_, q_, j_]:= T[n,k,p,q,j]= If[n==2, Prime[j], If[n==3 \u0026\u0026 k==2 || n==4 \u0026\u0026 2\u003c=k\u003c=3, ((3-(-1)^n)/2)*Prime[j]^(n-1) -2^((3-(-1)^n)/2), If[k==1 || k==n, 2, T[n-1,k,p,q,j] + T[n-1,k-1,p,q,j] + (p*j+q)*Prime[j]*T[n-2,k-1,p,q,j] ]]];",
				"Table[T[n,k,1,0,3], {n,12}, {k,n}]//Flatten (* modified by _G. C. Greubel_, Mar 04 2021 *)"
			],
			"program": [
				"(Sage)",
				"@CachedFunction",
				"def f(n,j): return ((3-(-1)^n)/2)*nth_prime(j)^(n-1) - 2^((3-(-1)^n)/2)",
				"def T(n,k,p,q,j):",
				"    if (n==2): return nth_prime(j)",
				"    elif (n==3 and k==2 or n==4 and 2\u003c=k\u003c=3): return f(n,j)",
				"    elif (k==1 or k==n): return 2",
				"    else: return T(n-1,k,p,q,j) + T(n-1,k-1,p,q,j) + (p*j+q)*nth_prime(j)*T(n-2,k-1,p,q,j)",
				"flatten([[T(n,k,1,0,3) for k in (1..n)] for n in (1..12)]) # _G. C. Greubel_, Mar 04 2021",
				"(Magma)",
				"f:= func\u003c n,j | Round(((3-(-1)^n)/2)*NthPrime(j)^(n-1) - 2^((3-(-1)^n)/2)) \u003e;",
				"function T(n,k,p,q,j)",
				"  if n eq 2 then return NthPrime(j);",
				"  elif (n eq 3 and k eq 2 or n eq 4 and k eq 2 or n eq 4 and k eq 3) then return f(n,j);",
				"  elif (k eq 1 or k eq n) then return 2;",
				"  else return T(n-1,k,p,q,j) + T(n-1,k-1,p,q,j) + (p*j+q)*NthPrime(j)*T(n-2,k-1,p,q,j);",
				"  end if; return T;",
				"end function;",
				"[T(n,k,1,0,3): k in [1..n], n in [1..12]]; // _G. C. Greubel_, Mar 04 2021"
			],
			"xref": [
				"Sequences with variable (p,q,j): A153516 (0,1,2), A153518 (0,1,3), A153520 (0,1,4), A153521 (0,1,5), this sequence (1,0,3), A153649 (1,1,4), A153650 (1,4,5), A153651 (1,5,6), A153652 (2,1,7), A153653 (2,1,8), A153654 (2,1,9), A153655 (2,1,10), A153656 (2,3,9), A153657 (2,7,10).",
				"Cf. A000351 (powers of 5)."
			],
			"keyword": "nonn,tabl",
			"offset": "1,1",
			"author": "_Roger L. Bagula_, Dec 30 2008",
			"ext": [
				"Edited by _G. C. Greubel_, Mar 04 2021"
			],
			"references": 14,
			"revision": 8,
			"time": "2021-03-05T02:51:30-05:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}