{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A290889",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 290889,
			"data": "1,1,1,1,2,1,5,4,13,10,38,34,118,103,380,346,1262,1153,4277,3965,14745,13746,51541,48396,182182,171835,650095,615966,2338706,2223755,8472697,8082457,30884150,29543309,113189168,108545916,416839177,400623807,1541726967",
			"name": "Number of partitions of the set of odd numbers {1, 3, ..., 2*n-1} into two subsets such that the absolute difference of the sums of the two subsets is minimized.",
			"comment": [
				"Partitioning in equal sums is only possible for n = 4*k-1, k \u003e 1, and the number of such partitions is given by A156700. For the set {1,3} and the other values of n, i.e., for the sets {1,3,5}, {1,3,5,7,9}, {1,3,5,7,9,11,13}, one can use the criterion to split the sets \"as well as possible\" by choosing those partitions for which the absolute value of the difference of the respective sums of the subset members achieves its minimum."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A290889/b290889.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e"
			],
			"formula": [
				"a(n) ~ (3 - (-1)^n) * sqrt(3) * 2^(n - 5/2) / (sqrt(Pi) * n^(3/2)). - _Vaclav Kotesovec_, Sep 18 2017"
			],
			"example": [
				"a(1) = 1: {}U{1} with difference 1.",
				"a(2) = 1: {1}U{3} with difference 2.",
				"a(3) = 1: {1,3}U{5} with difference 1.",
				"a(4) = 1 = A156700(2): {1,7}U{3,5} with difference 0.",
				"a(5) = 2: {1,3,9}U{5,7} and {1,5,7}U{3,9} with |difference|=1.",
				"a(6) = 1 = A156700(3): {1,3,5,9}U{7,11} with difference 0.",
				"a(7) = 5: {1,3,5,7,9}U{11,13}, {1,3,9,11}U{5,7,13}, {1,5,7,11}U{3,9,13},",
				"          {1,11,13}U{3,5,7,9}, {1,3,7,13}U{5,9,11} with |difference|=1."
			],
			"maple": [
				"b:= proc(n, i) option remember; `if`(n\u003ei^2, 0,",
				"      `if`(n=i^2, 1, b(abs(n-2*i+1), i-1)+b(n+2*i-1, i-1)))",
				"    end:",
				"a:= n-\u003e `if`(n\u003c5, 1, (t-\u003e b(t, n)/(2-t))(irem(n, 2))):",
				"seq(a(n), n=1..50);  # _Alois P. Heinz_, Aug 14 2017"
			],
			"mathematica": [
				"b[n_, i_] := b[n, i] = If[n \u003e i^2, 0, If[n == i^2, 1, b[Abs[n - 2i + 1], i - 1] + b[n + 2i - 1, i - 1]]];",
				"a[n_] := If[n \u003c 5, 1, b[#, n]/(2-#)\u0026[Mod[n, 2]]];",
				"Array[a, 50] (* _Jean-François Alcover_, Nov 14 2020, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A005408, A058377, A069918, A156700."
			],
			"keyword": "nonn",
			"offset": "1,5",
			"author": "_Hugo Pfoertner_, Aug 13 2017",
			"references": 2,
			"revision": 35,
			"time": "2020-11-14T08:35:09-05:00",
			"created": "2017-08-14T23:05:48-04:00"
		}
	]
}