{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A338809",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 338809,
			"data": "12,8,120,108,756,704,3384,3340,11880,10032,33800,32312,82440,78656,182172,144540,365712,350600",
			"name": "Number of polyhedra formed when an n-bipyramid, formed from two n-gonal pyraminds joined at the base, is internally cut by all the planes defined by any three of its vertices.",
			"comment": [
				"For a n-bipyramid, formed from two n-gonal pyraminds joined at the base, create all possible internal planes defined by connecting any three of its vertices. For example, in the case of a 3-bipyramid this results in 4 planes. Use all the resulting planes to cut the n-bipyramid into individual smaller polyhedra. The sequence lists the number of resulting polyhedra for bipyramids with n\u003e=3.",
				"See A338825 for the number and images of the k-faced polyhedra in each bipyramid dissection.",
				"The author thanks _Zach J. Shannon_ for assistance in producing the images for this sequence."
			],
			"link": [
				"Hyung Taek Ahn and Mikhail Shashkov, \u003ca href=\"https://cnls.lanl.gov/~shashkov/papers/ahn_geometry.pdf\"\u003eGeometric Algorithms for 3D Interface Reconstruction\u003c/a\u003e.",
				"Scott R. Shannon, \u003ca href=\"/A338809/a338809.png\"\u003e5-bipyramid, showing the 16 plane cuts on the external edges and faces\u003c/a\u003e.",
				"Scott R. Shannon, \u003ca href=\"/A338809/a338809.jpg\"\u003e5-bipyramid showing the 120 polyhedra post-cutting and exploded\u003c/a\u003e. Each piece has been moved away from the origin by a distance proportional to the average distance of its vertices from the origin. All 120 polyhedra have 4 faces, shown in red.",
				"Scott R. Shannon, \u003ca href=\"/A338809/a338809_1.png\"\u003e12-bipyramid, showing the 103 plane cuts on the external edges and faces\u003c/a\u003e.",
				"Scott R. Shannon, \u003ca href=\"/A338809/a338809_1.jpg\"\u003e12-bipyramid, showing the 10032 polyhedra post-cutting\u003c/a\u003e. The 4,5,6,7 faced polyhedra are colored red, orange, yellow, green respectively. The 8-faced polyhedra are not visible on the surface.",
				"Scott R. Shannon, \u003ca href=\"/A338809/a338809_2.jpg\"\u003e12-bipyramid, showing the 10032 polyhedra post-cutting and exploded\u003c/a\u003e.The 8-faced polyhedra colored blue can be seen.",
				"Scott R. Shannon, \u003ca href=\"/A338809/a338809_2.png\"\u003e20-bipyramid, showing the 331 plane cuts on the external edges and faces\u003c/a\u003e.",
				"Scott R. Shannon, \u003ca href=\"/A338809/a338809_3.jpg\"\u003e20-bipyramid, showing the 350600 polyhedra post-cutting\u003c/a\u003e. The 4,5,6,7,8,9,11 faced polyhedra are colored red, orange, yellow, green, blue, indigo, violet respectively. The polyhedra with 10 and 12 faces are not visible on the surface.",
				"Scott R. Shannon, \u003ca href=\"/A338809/a338809_4.jpg\"\u003e20-bipyramid positions vertically, showing the 350600 polyhedra post-cutting\u003c/a\u003e.",
				"Scott R. Shannon, \u003ca href=\"/A338809/a338809_5.jpg\"\u003e20-bipyramid, showing the 350600 polyhedra post-cutting and exploded\u003c/a\u003e. The 10-faced and 12-faced polyhedra, colored black and white, can also be seen.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"https://mathworld.wolfram.com/Dipyramid.html\"\u003eDipyramid\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Bipyramid\"\u003eBipyramid\u003c/a\u003e."
			],
			"example": [
				"a(3) = 12. The 3-bipyramid is cut with 4 internal planes resulting in 12 polyhedra, all 12 pieces having 4 faces.",
				"a(5) = 120. The 5-bipyramid is cut with 16 internal planes resulting in 120 polyhedra, all 120 pieces having 4 faces.",
				"a(7) = 756. The 7-bipyramid is cut with 36 internal planes resulting in 756 polyhedra; 448 with 4 faces, 280 with 5 faces, and 28 with 6 faces.",
				"Note that for a single n-pyramid the number of polyhedra is the same as the number of regions in the dissection of a 2D n-polygon, see A007678, as all planes join two points on the polygon and the single apex, resulting in an equivalent number of regions."
			],
			"xref": [
				"Cf. A338825 (number of k-faced polyhedra), A338571 (Platonic solids), A333539 (n-dimensional cube), A007678 (2D n-polygon)."
			],
			"keyword": "nonn,more",
			"offset": "3,1",
			"author": "_Scott R. Shannon_, Nov 10 2020",
			"references": 3,
			"revision": 23,
			"time": "2020-12-07T01:45:39-05:00",
			"created": "2020-12-07T01:45:39-05:00"
		}
	]
}