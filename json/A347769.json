{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A347769",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 347769,
			"data": "0,1,2,3,4,5,6,7,8,9,10,11,12,16,15,13,14,17,18,21,19,20,22,23,24,36,55,25,26,27,28,29,30,42,54,66,78,90,144,259,45,33,31,32,34,35,37,38,39,40,50,43,41,44,46,47,48,76,64,63,49,51,52,53,56,57,58,59,60,108,172",
			"name": "a(0) = 0; a(1) = 1; for n \u003e 1, a(n) = A001065(a(n-1)) = sigma(a(n-1)) - a(n-1) (the sum of aliquot parts of a(n-1)) if this is not yet in the sequence; otherwise a(n) is the smallest number missing from the sequence.",
			"comment": [
				"This sequence is a permutation of the nonnegative integers iff Catalan's aliquot sequence conjecture (also called Catalan-Dickson conjecture) is true.",
				"a(563) = 276 is the smallest number whose aliquot sequence has not yet been fully determined.",
				"As long as the aliquot sequence of 276 is not known to be finite or eventually periodic, a(563+k) = A008892(k)."
			],
			"link": [
				"Eric Chen, \u003ca href=\"/A347769/b347769.txt\"\u003eTable of n, a(n) for n = 0..2703\u003c/a\u003e (all currently known terms, after the b-file of A008892)",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"https://mathworld.wolfram.com/AliquotSequence.html\"\u003eAliquot sequence\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"https://mathworld.wolfram.com/CatalansAliquotSequenceConjecture.html\"\u003eCatalan's Aliquot Sequence Conjecture\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Aliquot_sequence\"\u003eAliquot sequence\u003c/a\u003e"
			],
			"example": [
				"a(0) = 0, a(1) = 1;",
				"since A001065(a(1)) = 0 has already appeared in this sequence, a(2) = 2;",
				"since A001065(a(2)) = 1 has already appeared in this sequence, a(3) = 3;",
				"...",
				"a(11) = 11;",
				"since A001065(a(11)) = 1 has already appeared in this sequence, a(12) = 12;",
				"since A001065(a(12)) = 16 has not yet appeared in this sequence, a(13) = A001065(a(12)) = 16;",
				"since A001065(a(13)) = 15 has not yet appeared in this sequence, a(14) = A001065(a(13)) = 15;",
				"since A001065(a(14)) = 9 has already appeared in this sequence, a(15) = 13;",
				"..."
			],
			"program": [
				"(PARI) A347769_list(N)=print1(0, \", \"); if(N\u003e0, print1(1, \", \")); v=[0, 1]; b=1; for(n=2, N, if(setsearch(Set(v), sigma(b)-b), k=1; while(k\u003c=n, if(!setsearch(Set(v), k), b=k; k=n+1, k++)), b=sigma(b)-b); print1(b, \", \"); v=concat(v, b))"
			],
			"xref": [
				"Cf. A032451.",
				"Cf. A001065 (sum of aliquot parts).",
				"Cf. A003023, A044050, A098007, A098008: (\"length\" of aliquot sequences, four versions).",
				"Cf. A007906.",
				"Cf. A115060 (maximum term of aliquot sequences).",
				"Cf. A115350 (termination of the aliquot sequences).",
				"Cf. A098009, A098010 (records of \"length\" of aliquot sequences).",
				"Cf. A290141, A290142 (records of maximum term of aliquot sequences).",
				"Cf. A000396, A063990, A122726, A206708, A347770.",
				"Cf. A080907, A063769, A121507, A121508, A131884, A126016.",
				"Aliquot sequences starting at various numbers: A000004 (0), A000007 (1), A033322 (2), A010722 (6), A143090 (12), A143645 (24), A010867 (28), A008885 (30), A143721 (38), A008886 (42), A143722 (48), A143723 (52), A008887 (60), A143733 (62), A143737 (68), A143741 (72), A143754 (75), A143755 (80), A143756 (81), A143757 (82), A143758 (84), A143759 (86), A143767 (87), A143846 (88), A143847 (96), A143919 (100), A008888 (138), A008889 (150), A008890 (168), A008891 (180), A203777 (220), A008892 (276), A014360 (552), A014361 (564), A074907 (570), A014362 (660), A269542 (702), A045477 (840), A014363 (966), A014364 (1074), A014365 (1134), A074906 (1521), A143930 (3630), A072891 (12496), A072890 (14316), A171103 (46758), A072892 (1264460)."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Eric Chen_, Sep 13 2021",
			"references": 1,
			"revision": 43,
			"time": "2021-09-23T08:03:29-04:00",
			"created": "2021-09-14T14:25:20-04:00"
		}
	]
}