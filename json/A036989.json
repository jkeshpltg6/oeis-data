{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A036989",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 36989,
			"data": "1,2,1,3,1,2,2,4,1,2,1,3,1,3,3,5,1,2,1,3,1,2,2,4,1,2,2,4,2,4,4,6,1,2,1,3,1,2,2,4,1,2,1,3,1,3,3,5,1,2,1,3,1,3,3,5,1,3,3,5,3,5,5,7,1,2,1,3,1,2,2,4,1,2,1,3,1,3,3,5,1,2,1,3,1,2,2,4,1,2,2,4,2,4,4,6,1,2,1,3,1,2,2,4,1",
			"name": "Read binary expansion of n from the right; keep track of the excess of 1's over 0's that have been seen so far; sequence gives 1 + maximum(excess of 1's over 0's).",
			"comment": [
				"Associated with A036988 (Remark 4 of the reference)."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A036989/b036989.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"H. Niederreiter and M. Vielhaber, \u003ca href=\"http://dx.doi.org/10.1006/jcom.1996.0014\"\u003eTree complexity and a doubly exponential gap between structured and random sequences\u003c/a\u003e, J. Complexity, 12 (1996), 187-198.",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 1 iff, in the binary expansion of n, reading from right to left, the number of 1's never exceeds the number of 0's: a(A036990(n)) = 1.",
				"a(0) = 1, a(2n) = max(a(n) - 1, 1), a(2n+1) = a(n) + 1. - _Franklin T. Adams-Watters_, Dec 26 2006",
				"Equals inverse Moebius transform (A051731) of A010060, the Thue-Morse sequence starting with \"1\": (1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1,...). - _Gary W. Adamson_, May 13 2007"
			],
			"example": [
				"59 in binary is 111011, excess from right to left is 1,2,1,2,3,4, maximum is 4, so a(59) = 4."
			],
			"mathematica": [
				"a[0] = 1; a[n_?EvenQ] := a[n] = Max[a[n/2] - 1, 1]; a[n_] := a[n] = a[(n-1)/2] + 1; Table[a[n], {n, 0, 104}] (* _Jean-François Alcover_, Nov 05 2013, after _Franklin T. Adams-Watters_ *)"
			],
			"program": [
				"(Haskell)",
				"import Data.List (transpose)",
				"a036989 n = a036989_list !! n",
				"a036989_list = 1 : concat (transpose",
				"   [map (+ 1) a036989_list, map ((max 1) . pred) $ tail a036989_list])",
				"-- _Reinhard Zumkeller_, Jul 31 2013"
			],
			"xref": [
				"Cf. A036988, A036990, A126387.",
				"Cf. A010060."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_.",
			"ext": [
				"Edited by _Joshua Zucker_, May 11 2006"
			],
			"references": 5,
			"revision": 27,
			"time": "2017-03-15T20:24:31-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}