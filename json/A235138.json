{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A235138",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 235138,
			"data": "0,1,2,2,4,1,6,4,6,3,10,2,12,5,7,8,16,3,18,6,11,9,22,4,20,11,18,10,28,29,30,16,19,15,23,6,36,17,23,12,40,1,42,18,21,21,46,8,42,15,31,22,52,9,39,20,35,27,58,58,60,29,33,32,47,5,66,30,43,11,70,12,72,35,35,34,59,7,78,24,54,39,82,2,63,41,55,36,88,87,71,42,59,45,71,16,96,35,57,30",
			"name": "Sum_{k=1..n} k^phi(n) (mod n) where phi(n) = A000010(n).",
			"comment": [
				"a(n) = n-1 if and only if n is prime or is a Giuga number A007850.",
				"a(n) = 1 if (and probably only if) n is a primary pseudoperfect number A054377.",
				"a(2^k*p) = 2^(k-1)*p-2^k if p is an odd prime. - _Robert Israel_, Apr 25 2017"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A235138/b235138.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"J. Sondow and K. MacMillan, \u003ca href=\"http://math.colgate.edu/~integers/l34/l34.Abstract.html\"\u003eReducing the Erdos-Moser equation 1^n + 2^n + . . . + k^n = (k+1)^n modulo k and k^2\u003c/a\u003e, Integers 11 (2011), #A34.",
				"J. Sondow and E. Tsukerman, \u003ca href=\"https://arxiv.org/abs/1401.0322\"\u003eThe p-adic order of power sums, the Erdos-Moser equation, and Bernoulli numbers\u003c/a\u003e, arXiv:1401.0322 [math.NT], 2014; see section 4.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Giuga_number\"\u003eGiuga number\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Primary_pseudoperfect_number\"\u003ePrimary pseudoperfect number\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A235137(n) (mod n)."
			],
			"example": [
				"a(4) = 30 (mod 4) = 2 since 1^(phi(4)) + 2^(phi(4)) + 3^(phi(4)) + 4^(phi(4))= 1^2 + 2^2 + 3^2 + 4^2 = 1 + 4 + 9 + 16 = 30."
			],
			"maple": [
				"f:= proc(n) local q; q:= numtheory:-phi(n);",
				"   add(k\u0026^q, k=1..n) mod n",
				"end proc:",
				"map(f, [$1..100]); # _Robert Israel_, Apr 25 2017"
			],
			"mathematica": [
				"a[n_] := Mod[Sum[PowerMod[i, EulerPhi@n, n], {i, n}], n]"
			],
			"program": [
				"(PARI) a(n)=my(p=eulerphi(n));sum(k=1,n,k^p) \\\\ _Charles R Greathouse IV_, Jan 04 2014"
			],
			"xref": [
				"Cf. A007850, A054377, A235137."
			],
			"keyword": "nonn,look",
			"offset": "1,3",
			"author": "_Jonathan Sondow_ and Emmanuel Tsukerman, Jan 03 2014",
			"references": 4,
			"revision": 37,
			"time": "2017-04-25T17:20:10-04:00",
			"created": "2014-01-04T05:07:26-05:00"
		}
	]
}