{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A208664",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 208664,
			"data": "1,1,-1,0,0,-1,0,-1,0,0,0,1,0,-1,0,1,-1,0,-1,0,0,0,0,-2,1,0,2,1,0,1,0,0,0,1,1,1,0,0,0,0,-1,0,0,0,-1,0,1,0,-1,0,0,-2,0,0,0,-1,-1,-1,0,0,1,0,1,0,0,0,0,1,-1,0,-2,0,0,1,0,0,0,-1,-1,2",
			"name": "Expansion of f(x) * f(x^11) in powers of x where f() is a Ramanujan theta function.",
			"comment": [
				"Number 69 of the 74 eta-quotients listed in Table I of Martin (1996).",
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A208664/b208664.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Y. Martin, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9947-96-01743-6\"\u003eMultiplicative eta-quotients\u003c/a\u003e, Trans. Amer. Math. Soc. 348 (1996), no. 12, 4825-4856, see page 4852 Table I.",
				"Michael Somos, \u003ca href=\"/A030203/a030203.txt\"\u003eIndex to Yves Martin's list of 74 multiplicative eta-quotients and their A-numbers\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(-1/2) * eta(q^2)^3 * eta(q^22)^3 / (eta(q) * eta(q^4) * eta(q^11) * eta(q^44)) in powers of q.",
				"Euler transform of period 44 sequence [ 1, -2, 1, -1, 1, -2, 1, -1, 1, -2, 2, -1, 1, -2, 1, -1, 1, -2, 1, -1, 1, -4, 1, -1, 1, -2, 1, -1, 1, -2, 1, -1, 2, -2, 1, -1, 1, -2, 1, -1, 1, -2, 1, -2, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (176 t)) = 176^(1/2) (t/i) f(t) where q = exp(2 Pi i t).",
				"a(n) = b(2*n + 1) where b(n) is multiplicative with b(2^e) = 0^e, b(11^e) = (-1)^e, b(p^e) = s * ((e-1)%3 - 1) if f=0, b(p^e) = s * (e + 1) if f=3, b(p^e) = s * (1 + (-1)^e) / 2 if f=1 where s = (-1)^(e*(p-1)/2) and f = number of zeros of x^3-x^2-x-1 modulo p.",
				"a(n) = (-1)^n * A030200(n). a(11*n + 3) = a(11*n + 6) = a(11*n + 8) = a(11*n + 9) = a(11*n + 10) = 0. a(11*n + 5) = -a(n)."
			],
			"example": [
				"G.f. = 1 + x - x^2 - x^5 - x^7 + x^11 - x^13 + x^15 - x^16 - x^18 - 2*x^23 + ...",
				"G.f. = q + q^3 - q^5 - q^11 - q^15 + q^23 - q^27 + q^31 - q^33 - q^37 - 2*q^47 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ -x] QPochhammer[ -x^11] , {x, 0, n}]; (* _Michael Somos_, Jun 09 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A, p, e, f); if(n\u003c0, 0, n = 2*n+1; A = factor(n); prod( k=1, matsize(A)[1], [p, e] = A[k,]; if( p==2, 0, p==11, (-1)^e, f = sum( k=0, p-1, (k^3-k^2-k-1)%p == 0); (-1)^(e*(p-1)/2) * if( f==0, (e-1)%3-1, f==1, (1 + (-1)^e) / 2, e+1))))};",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A)^3 * eta(x^22 + A)^3 / (eta(x + A) * eta(x^4 + A) * eta(x^11 + A) * eta(x^44 + A)), n))};"
			],
			"xref": [
				"Cf. A030200."
			],
			"keyword": "sign",
			"offset": "0,24",
			"author": "_Michael Somos_, Mar 05 2012",
			"references": 1,
			"revision": 29,
			"time": "2021-03-12T22:24:46-05:00",
			"created": "2012-03-05T17:36:23-05:00"
		}
	]
}