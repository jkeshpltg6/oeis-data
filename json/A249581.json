{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A249581",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 249581,
			"data": "0,1,1,0,1,2,3,4,5,8,13,20,23,36,59,92,105,164,269,420,479,748,1227,1916,2185,3412,5597,8740,9967,15564,25531,39868,45465,70996,116461,181860,207391,323852,531243,829564,946025,1477268,2423293,3784100",
			"name": "List of quadruples (r,s,t,u): the matrix M = [[9,24,16][3,10,8][1,4,4]] is raised to successive powers, then (r,s,t,u) are the square roots of M[3,1], M[3,3], M[1,1], M[1,3] respectively.",
			"comment": [
				"The general form of these matrices is [[t^2,2tu,u^2][rt,st+ru,su][r^2,2rs,s^2]]. Different symmetries have different properties.",
				"Iff |r * u - s * t| = 1 then terms to the left of a(0) are all integers."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A249581/b249581.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Russell Walsmith, \u003ca href=\"/A249581/a249581.pdf\"\u003eDCL-Chemy III: Hyper-Quadratics\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_08\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,0,0,5,0,0,0,-2)."
			],
			"formula": [
				"a(4n) + a(4n + 1) = a(4n + 2).",
				"a(4n + 1) + a(4n + 2) + a(4n + 3) - a(4n) = a(4n + 5)",
				"4a(4n) = a(4n+3).",
				"a(4n+1) = A147722(n), a(4n+2) = A052984(n).",
				"a(n) = 5*a(n-4)-2*a(n-8). - _Colin Barker_, Nov 04 2014",
				"G.f.: x*(4*x^6-2*x^5-3*x^4+x^3+x+1) / (2*x^8-5*x^4+1). - _Colin Barker_, Nov 04 2014"
			],
			"example": [
				"M^0 = [[1,0,0][0,1,0][0,0,1]]. r = sqrt(M[3,1]) = a(0) = 0; s = sqrt(M[3,3]) = a(1) = 1; t = sqrt(M[1,1]) = a(2) = 1; u = sqrt(M[1,3]) = a(3) = 0.",
				"M^1 = [[9,24,16][3,10,8][1,4,4]]. r = sqrt(M[3,1]) = a(4) = 1; s = sqrt(M[3,3]) = a(5) = 2; t = sqrt(M[1,1]) = a(6) = 3; u = sqrt(M[1,3]) = a(7) = 4."
			],
			"mathematica": [
				"LinearRecurrence[{0,0,0,5,0,0,0,-2},{0,1,1,0,1,2,3,4},50] (* _Harvey P. Dale_, Aug 01 2016 *)"
			],
			"program": [
				"(PARI) concat(0, Vec(x*(4*x^6-2*x^5-3*x^4+x^3+x+1)/(2*x^8-5*x^4+1) + O(x^100))) \\\\ _Colin Barker_, Nov 04 2014"
			],
			"xref": [
				"Cf. A249579, A249580, A147722, A052984."
			],
			"keyword": "nonn,easy,tabf",
			"offset": "0,6",
			"author": "_Russell Walsmith_, Nov 03 2014",
			"references": 2,
			"revision": 20,
			"time": "2016-08-01T14:00:05-04:00",
			"created": "2014-11-05T01:36:42-05:00"
		}
	]
}