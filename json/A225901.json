{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A225901",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 225901,
			"data": "0,1,4,5,2,3,18,19,22,23,20,21,12,13,16,17,14,15,6,7,10,11,8,9,96,97,100,101,98,99,114,115,118,119,116,117,108,109,112,113,110,111,102,103,106,107,104,105,72,73,76,77,74,75,90,91,94,95,92,93,84,85,88,89,86,87,78,79,82,83,80,81,48,49,52,53,50,51,66,67,70,71,68",
			"name": "Write n in factorial base, then replace each nonzero digit d of radix k with k-d.",
			"comment": [
				"Analogous to A004488 or A048647 for the factorial base.",
				"A self-inverse permutation of the natural numbers.",
				"From _Antti Karttunen_, Aug 16-29 2016: (Start)",
				"Consider the following way to view a factorial base representation of nonnegative integer n. For each nonzero digit d_i present in the factorial base representation of n (where i is the radix = 2.. = one more than 1-based position from the right), we place a pebble to the level (height) d_i at the corresponding column i of the triangular diagram like below, while for any zeros the corresponding columns are left empty:",
				".",
				"Level",
				"  6        o",
				"          ─ ─",
				"  5        . .",
				"          ─ ─ ─",
				"  4        . . .",
				"          ─ ─ ─ ─",
				"  3        . . . .",
				"          ─ ─ ─ ─ ─",
				"  2        . . o . .",
				"          ─ ─ ─ ─ ─ ─",
				"  1        . o . . o o",
				"          ─ ─ ─ ─ ─ ─ ─",
				"  Radix:   7 6 5 4 3 2",
				"  Digits:  6 1 2 0 1 1 = A007623(4491)",
				"Instead of levels, we can observe on which \"slope\" each pebble (nonzero digit) is located at. Formally, the slope of nonzero digit d_i with radix i is (i - d_i). Thus in above example, both the most significant digit (6) and the least significant 1 are on slope 1 (called \"maximal slope\", because it contains digits that are maximal allowed in those positions), while the second 1 from the right is on slope 2 (\"submaximal slope\").",
				"This involution (A225901) sends each nonzero digit at level k to the slope k (and vice versa) by flipping such a diagram by the shallow diagonal axis that originates from the bottom right corner. Thus, from above diagram we obtain:",
				"Slope (= digit's radix - digit's value)",
				"   1",
				"   2 .",
				"   3 .  .╲",
				"   4 .  .╲o╲",
				"   5 .  .╲.╲.╲",
				"   6 .  .╲.╲o╲.╲",
				"     .  .╲.╲.╲.╲o╲",
				"        o╲.╲.╲.╲.╲o╲",
				"        -----------------",
				"        1  5  3 0  2  1  = A007623(1397)",
				"and indeed, a(4491) = 1397 and a(1397) = 4491.",
				"Thus this permutation maps between polynomial encodings A275734 \u0026 A275735 and all the respective sequences obtained from them, where the former set of sequences are concerned with the \"slopes\" and the latter set with the \"levels\" of the factorial base representation. See the Crossrefs section.",
				"Sequences A231716 and A275956 are closed with respect to this sequence, in other words, for all n, a(A231716(n)) is a term of A231716 and a(A275956(n)) is a term of A275956.",
				"(End)"
			],
			"link": [
				"Paul Tek, \u003ca href=\"/A225901/b225901.txt\"\u003eTable of n, a(n) for n = 0..5039\u003c/a\u003e",
				"\u003ca href=\"/index/Fa#facbase\"\u003eIndex entries for sequences related to factorial base representation\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"From _Antti Karttunen_, Aug 29 2016: (Start)",
				"a(0) = 0; for n \u003e= 1, a(n) = A276091(A275736(n)) + A153880(a(A257684(n))).",
				"or, for n \u003e= 1, a(n) = A276149(n) + a(A257687(n)).",
				"(End)",
				"Other identities. For n \u003e= 0:",
				"a(n!) = A001563(n).",
				"a(n!-1) = A007489(n-1).",
				"From _Antti Karttunen_, Aug 16 2016: (Start)",
				"A275734(a(n)) = A275735(n) and vice versa, A275735(a(n)) = A275734(n).",
				"A060130(a(n)) = A060130(n). [The flip preserves the number of nonzero digits.]",
				"A153880(n) = a(A255411(a(n))) and A255411(n) = a(A153880(a(n))). [This involution conjugates between the two fundamental factorial base shifts.]",
				"a(n) = A257684(a(A153880(n))) = A266193(a(A255411(n))). [Follows from above.]",
				"A276011(n) = A273662(a(A273670(n))).",
				"A276012(n) = A273663(a(A256450(n))).",
				"(End)"
			],
			"example": [
				"a(1000) = a(1*6! + 2*5! + 1*4! + 2*3! + 2*2!) = (7-1)*6! + (6-2)*5! + (5-1)*4! + (4-2)*3! + (3-2)*2! = 4910.",
				"a(1397) = a(1*6! + 5*5! + 3*4! + 0*3! + 2*2! + 1*1!) = (7-1)*6! + (6-5)*5! + (5-3)*4! + (3-2)*2! + (2-1)*1! = 4491."
			],
			"mathematica": [
				"b = MixedRadix[Reverse@ Range[2, 12]]; Table[FromDigits[Map[Boole[# \u003e 0] \u0026, #] (Reverse@ Range[2, Length@ # + 1] - #), b] \u0026@ IntegerDigits[n, b], {n, 0, 82}] (* Version 10.2, or *)",
				"f[n_] := Block[{a = {{0, n}}}, Do[AppendTo[a, {First@ #, Last@ #} \u0026@ QuotientRemainder[a[[-1, -1]], Times @@ Range[# - i]]], {i, 0, #}] \u0026@ NestWhile[# + 1 \u0026, 0, Times @@ Range[# + 1] \u003c= n \u0026]; Most@ Rest[a][[All, 1]] /. {} -\u003e {0}]; g[w_List] := Total[Times @@@ Transpose@ {Map[Times @@ # \u0026, Range@ Range[0, Length@ w]], Reverse@ Append[w, 0]}]; Table[g[Map[Boole[# \u003e 0] \u0026, #] (Reverse@ Range[2, Length@ # + 1] - #)] \u0026@ f@ n, {n, 0, 82}] (* _Michael De Vlieger_, Aug 29 2016 *)"
			],
			"program": [
				"(PARI) a(n)=my(s=0,d,k=2);while(n,d=n%k;n=n\\k;if(d,s=s+(k-d)*(k-1)!);k=k+1);return(s)",
				"(Scheme)",
				"(define (A225901 n) (let loop ((n n) (z 0) (m 2) (f 1)) (cond ((zero? n) z) (else (loop (quotient n m) (if (zero? (modulo n m)) z (+ z (* f (- m (modulo n m))))) (+ 1 m) (* f m))))))",
				";; One implementing the first recurrence, with memoization-macro definec:",
				"(definec (A225901 n) (if (zero? n) n (+ (A276091 (A275736 n)) (A153880 (A225901 (A257684 n))))))",
				";; _Antti Karttunen_, Aug 29 2016",
				"(Python)",
				"from sympy import factorial as f",
				"def a(n):",
				"    s=0",
				"    k=2",
				"    while(n):",
				"        d=n%k",
				"        n=(n//k)",
				"        if d: s=s+(k - d)*f(k - 1)",
				"        k+=1",
				"    return s",
				"print([a(n) for n in range(101)]) # _Indranil Ghosh_, Jun 19 2017"
			],
			"xref": [
				"Cf. A000142, A007623, A004488, A048647, A001563, A007489, A257684, A257687, A276091, A275736, A276149.",
				"Cf. A275959 (fixed points), A231716, A275956.",
				"Cf. A153880 \u0026 A255411.",
				"Cf. also A275734 \u0026 A275735, A275952 \u0026 A275954.",
				"This involution maps between the following sequences related to \"levels\" and \"slopes\" (see comments): A275806 \u003c--\u003e A060502, A257511 \u003c--\u003e A260736, A264990 \u003c--\u003e A275811, A275729 \u003c--\u003e A275728, A275948 \u003c--\u003e A275946, A275949 \u003c--\u003e A275947, A275964 \u003c--\u003e A275962, A059590 \u003c--\u003e A276091.",
				"Related permutations: A275957, A275958, A275835, A275836, A275837, A275838, A276011, A276012."
			],
			"keyword": "nonn,base",
			"offset": "0,3",
			"author": "_Paul Tek_, May 20 2013",
			"references": 55,
			"revision": 72,
			"time": "2020-04-25T13:07:11-04:00",
			"created": "2013-05-21T00:43:40-04:00"
		}
	]
}