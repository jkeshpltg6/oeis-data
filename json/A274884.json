{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A274884",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 274884,
			"data": "-1,-1,-1,1,-1,0,0,1,-1,1,0,1,1,-1,0,0,1,2,3,2,2,1,-1,1,0,1,1,3,1,2,1,1,-1,0,0,1,2,5,6,9,9,10,9,8,5,4,2,1,-1,1,0,1,1,3,3,5,4,5,5,5,3,3,2,1,1",
			"name": "Triangle read by rows, coefficients of q-polynomials representing the oscillating orbitals over n sectors as A274888(n) - 2*A274886(n), a q-analog of A232500.",
			"comment": [
				"The polynomials are univariate polynomials over the integers with degree floor((n+1)/2)^2 + ((n+1) mod 2). Evaluated at q=1 the polynomials give A232500.",
				"For the combinatorial interpretation see A232500 and the link 'orbitals' (see also the illustrations there)."
			],
			"link": [
				"Peter Luschny, \u003ca href=\"https://oeis.org/wiki/User:Peter_Luschny/Orbitals\"\u003eOrbitals\u003c/a\u003e"
			],
			"example": [
				"The polynomials start:",
				"[0] -1",
				"[1] -1",
				"[2] q - 1",
				"[3] (q - 1) * (q^2 + q + 1)",
				"[4] (q^2 + 1) * (q^2 + q - 1)",
				"[5] (q^2 + 1) * (q^2 + q - 1) * (q^4 + q^3 + q^2 + q + 1)",
				"[6] (q^2 - q + 1) * (q^3 + q^2 + q - 1) * (q^4 + q^3 + q^2 + q + 1)",
				"The table starts:",
				"[n] [k=0,1,2,...] [row sum]",
				"[0] [-1] -1",
				"[1] [-1] -1",
				"[2] [-1, 1] 0",
				"[3] [-1, 0, 0, 1] 0",
				"[4] [-1, 1, 0, 1, 1] 2",
				"[5] [-1, 0, 0, 1, 2, 3, 2, 2, 1] 10",
				"[6] [-1, 1, 0, 1, 1, 3, 1, 2, 1, 1] 10",
				"[7] [-1, 0, 0, 1, 2, 5, 6, 9, 9, 10, 9, 8, 5, 4, 2, 1] 70",
				"[8] [-1, 1, 0, 1, 1, 3, 3, 5, 4, 5, 5, 5, 3, 3, 2, 1, 1] 42"
			],
			"maple": [
				"QOscOrbitals := proc(n) local h, p, P, F, C, S;",
				"P := x -\u003e QDifferenceEquations:-QPochhammer(q,q,x);",
				"F := x -\u003e QDifferenceEquations:-QFactorial(x,q);",
				"h := iquo(n,2): p := `if`(n::even,1-q,1);",
				"C := (p*P(n))/(P(h)*P(h+1)); S := F(n)/F(h)^2;",
				"expand(simplify(expand(S-2*C))); seq(coeff(%,q,j), j=0..degree(%)) end:",
				"seq(QOscOrbitals(n), n=0..8);"
			],
			"program": [
				"(Sage) # uses[q_ext_catalan_number]",
				"# Function q_ext_catalan_number is in A274886.",
				"from sage.combinat.q_analogues import q_multinomial",
				"def q_osc_orbitals(n):",
				"    return q_multinomial([n//2, n%2, n//2]) - 2*q_ext_catalan_number(n)",
				"for n in (0..9): print(q_osc_orbitals(n).list())"
			],
			"xref": [
				"Cf. A232500 (row sums), A274886, A274888."
			],
			"keyword": "sign,tabf",
			"offset": "0,18",
			"author": "_Peter Luschny_, Jul 20 2016",
			"references": 1,
			"revision": 15,
			"time": "2020-03-13T17:44:42-04:00",
			"created": "2016-08-01T05:52:33-04:00"
		}
	]
}