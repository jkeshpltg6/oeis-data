{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A214409",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 214409,
			"data": "1,3,4,7,6,13,8,15,13,21,12,31,14,31,26,31,18,49,20,51,32,45,24,65,31,49,40,57,30,91,32,63,52,63,48,91,38,75,56,93,42,127,44,93,88,93,48,127,57,93,80,105,54,121,72,127,80,105,60,217,62,127,104,127",
			"name": "a(n) is the smallest conjectured m such that the irreducible fraction m/n is a known abundancy index.",
			"comment": [
				"The abundancy index of a number k is sigma(k)/k. When n is prime, (n+1)/n is irreducible and abund(n) = (n+1)/n, so a(n) = n + 1.",
				"A known abundancy index is related to a limit. Terms of the sequence have been built with a limit of 10^30. So when n is composite, the values for a(n) are conjectural. A higher limit could provide smaller values.",
				"If m \u003c k \u003c sigma(m) and k is relatively prime to m, then k/m is an abundancy outlaw. Hence if r/s is an abundancy index with gcd(r, s) = 1, then r \u003e= sigma(s). [Stanton and Holdener page 3]. Since a(n) is coprime to n, this implies that a(n) \u003e= sigma(n).",
				"When a(n)=A214413(n), this means that a(n) is sure to be the least m satisfying the property."
			],
			"link": [
				"Michel Marcus, \u003ca href=\"/A214409/b214409.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Michel Marcus, \u003ca href=\"/A214409/a214409.txt\"\u003eComputations for k such that sigma(k)/k = a(n)/n\u003c/a\u003e",
				"Kate Moore \u003ca href=\"http://biology.kenyon.edu/HHMI/posters_2011/moorek.pdf\"\u003eA Geometric Representation of the Abundancy Index\u003c/a\u003e",
				"W. Nissen \u003ca href=\"http://upforthecount.com/math/black.html\"\u003eAugmentation of Table of Abundancies\u003c/a\u003e",
				"William G. Stanton and Judy A. Holdener, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL10/Holdener/holdener7.html\"\u003eAbundancy \"Outlaws\" of the Form (sigma(N) + t)/N\u003c/a\u003e, Journal of Integer Sequences , Vol 10 (2007) , Article 07.9.6."
			],
			"example": [
				"For n = 5, a(5) = 6 because 6/5 is irreducible, 6/5 is a known abundancy (namely of 5), and no number below 6 can be found with the same properties."
			],
			"xref": [
				"Equal to or greater than A214413."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Michel Marcus_, Jul 16 2012",
			"references": 3,
			"revision": 20,
			"time": "2019-01-27T05:42:57-05:00",
			"created": "2012-07-23T12:28:25-04:00"
		}
	]
}