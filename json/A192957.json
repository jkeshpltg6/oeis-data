{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A192957",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 192957,
			"data": "0,1,1,5,14,34,72,141,261,465,806,1370,2296,3809,6273,10277,16774,27306,44368,71997,116725,189121,306286,495890,802704,1299169,2102497,3402341,5505566,8908690,14415096,23324685,37740741,61066449,98808278",
			"name": "Coefficient of x in the reduction by x^2 -\u003e x+1 of the polynomial p(n,x) defined at Comments.",
			"comment": [
				"The titular polynomials are defined recursively: p(n,x) = x*p(n-1,x) +- 1 + n^2, with p(0,x)=1.  For an introduction to reductions of polynomials by substitutions such as x^2 -\u003e x+1, see A192232 and A192744."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A192957/b192957.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-5,1,2,-1)."
			],
			"formula": [
				"a(n) = 4*a(n-1) - 5*a(n-2) + a(n-3) + 2*a(n-4) - a(n-5).",
				"From _R. J. Mathar_, May 09 2014: (Start)",
				"G.f.: x*(1 -3*x +6*x^2 -2*x^3)/((1-x-x^2)*(1-x)^3).",
				"a(n) - a(n-1) = A192956(n-1). (End)",
				"a(n) = Fibonacci(n+4) + 4*Fibonacci(n+2) - (n^2+4*n+7). - _G. C. Greubel_, Jul 12 2019"
			],
			"mathematica": [
				"(* First program *)",
				"q = x^2; s = x + 1; z = 40;",
				"p[0, x]:= 1;",
				"p[n_, x_]:= x*p[n-1, x] + n^2 - 1;",
				"Table[Expand[p[n, x]], {n, 0, 7}]",
				"reduce[{p1_, q_, s_, x_}]:= FixedPoint[(s PolynomialQuotient @@ #1 + PolynomialRemainder @@ #1 \u0026)[{#1, q, x}] \u0026, p1]",
				"t = Table[reduce[{p[n, x], q, s, x}], {n, 0, z}];",
				"u1 = Table[Coefficient[Part[t, n], x, 0], {n, 1, z}] (* A192956 *)",
				"u2 = Table[Coefficient[Part[t, n], x, 1], {n, 1, z}] (* A192957 *)",
				"(* Second program *)With[{F=Fibonacci}, Table[F[n+4]+4*F[n+2]-(n^2+4*n+7), {n,0,40}]] (* _G. C. Greubel_, Jul 12 2019 *)"
			],
			"program": [
				"(PARI) vector(40, n, n--; f=fibonacci; f(n+4)+4*f(n+2)-(n^2+4*n+7)) \\\\ _G. C. Greubel_, Jul 12 2019",
				"(MAGMA) F:=Fibonacci; [F(n+4)+4*F(n+2)-(n^2+4*n+7): n in [0..40]]; // _G. C. Greubel_, Jul 12 2019",
				"(Sage) f=fibonacci; [f(n+4)+4*f(n+2)-(n^2+4*n+7) for n in (0..40)] # _G. C. Greubel_, Jul 12 2019",
				"(GAP) F:=Fibonacci;; List([0..40], n-\u003e F(n+4)+4*F(n+2)-(n^2+4*n+7)); # _G. C. Greubel_, Jul 12 2019"
			],
			"xref": [
				"Cf. A000045, A192232, A192744, A192951, A192956."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_Clark Kimberling_, Jul 13 2011",
			"references": 3,
			"revision": 17,
			"time": "2019-07-12T20:18:12-04:00",
			"created": "2011-07-13T20:20:20-04:00"
		}
	]
}