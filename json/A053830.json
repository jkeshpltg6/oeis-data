{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A053830",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 53830,
			"data": "0,1,2,3,4,5,6,7,8,1,2,3,4,5,6,7,8,9,2,3,4,5,6,7,8,9,10,3,4,5,6,7,8,9,10,11,4,5,6,7,8,9,10,11,12,5,6,7,8,9,10,11,12,13,6,7,8,9,10,11,12,13,14,7,8,9,10,11,12,13,14,15,8,9,10,11,12,13,14,15,16,1,2,3,4,5,6,7,8,9",
			"name": "Sum of digits of (n written in base 9).",
			"comment": [
				"Also the fixed point of the morphism 0-\u003e{0,1,2,3,4,5,6,7,8}, 1-\u003e{1,2,3,4,5,6,7,8,9}, 2-\u003e{2,3,4,5,6,7,8,9,10}, etc. - _Robert G. Wilson v_, Jul 27 2006"
			],
			"link": [
				"Indranil Ghosh, \u003ca href=\"/A053830/b053830.txt\"\u003eTable of n, a(n) for n = 0..59049\u003c/a\u003e",
				"Jeffrey O. Shallit, \u003ca href=\"http://www.jstor.org/stable/2322179\"\u003eProblem 6450\u003c/a\u003e, Advanced Problems, The American Mathematical Monthly, Vol. 91, No. 1 (1984), pp. 59-60; \u003ca href=\"http://www.jstor.org/stable/2322523\"\u003eTwo series, solution to Problem 6450\u003c/a\u003e, ibid., Vol. 92, No. 7 (1985), pp. 513-514.",
				"Robert Walker, \u003ca href=\"http://robertinventor.com/ftswiki/Self_Similar_Sloth_Canon_Number_Sequences\"\u003eSelf Similar Sloth Canon Number Sequences\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/DigitSum.html\"\u003eDigit Sum\u003c/a\u003e."
			],
			"formula": [
				"From _Benoit Cloitre_, Dec 19 2002: (Start)",
				"a(0) = 0, a(9n+i) = a(n) + i for 0 \u003c= i \u003c= 8;",
				"a(n) = n - 8*Sum_{k\u003e=1} floor(n/9^k) = n - 8*A054898(n). (End)",
				"a(n) = A138530(n,9) for n \u003e 8. - _Reinhard Zumkeller_, Mar 26 2008",
				"a(n) = Sum_{k\u003e=0} A031087(n,k). - _Philippe Deléham_, Oct 21 2011",
				"a(0) = 0; a(n) = a(n - 9^floor(log_9(n))) + 1. - _Ilya Gutkovskiy_, Aug 24 2019",
				"Sum_{n\u003e=1} a(n)/(n*(n+1)) = 9*log(9)/8 (Shallit, 1984). - _Amiram Eldar_, Jun 03 2021"
			],
			"example": [
				"a(20) = 2+2 = 4 because 20 is written as 22 base 9.",
				"From _Omar E. Pol_, Feb 23 2010: (Start)",
				"It appears that this can be written as a triangle (see the conjecture in the entry A000120):",
				"0;",
				"1,2,3,4,5,6,7,8;",
				"1,2,3,4,5,6,7,8,9,2,3,4,5,6,7,8,9,10,3,4,5,6,7,8,9,10,11,4,5,6,7,8,9,10,11,...",
				"where the rows converge to A173529. (End)"
			],
			"mathematica": [
				"Table[Plus @@ IntegerDigits[n, 9], {n, 0, 100}] (* or *)",
				"Nest[ Flatten[ #1 /. a_Integer -\u003e Table[a + i, {i, 0, 8}]] \u0026, {0}, 3] (* _Robert G. Wilson v_, Jul 27 2006 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c1,0,if(n%9,a(n-1)+1,a(n/9)))",
				"(MAGMA) [\u0026+Intseq(n, 9):n in [0..100]]; // _Marius A. Burtea_, Aug 24 2019"
			],
			"xref": [
				"Cf. A000120, A007953, A053735, A053737, A053824, A053828, A231684, A231685, A231686, A231687.",
				"Cf. A173529. - _Omar E. Pol_, Feb 23 2010"
			],
			"keyword": "base,nonn",
			"offset": "0,3",
			"author": "_Henry Bottomley_, Mar 28 2000",
			"references": 23,
			"revision": 43,
			"time": "2021-06-03T03:30:51-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}