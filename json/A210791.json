{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A210791",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 210791,
			"data": "1,1,1,1,2,2,1,3,7,3,1,4,17,14,5,1,5,36,42,30,8,1,6,72,104,111,58,13,1,7,141,233,329,251,111,21,1,8,275,494,862,848,553,206,34,1,9,538,1016,2097,2479,2112,1158,377,55,1,10,1058,2056,4870,6608,6875",
			"name": "Triangle of coefficients of polynomials u(n,x) jointly generated with A210792; see the Formula section.",
			"comment": [
				"Row n starts with 1 and ends with F(n), where F=A000045 (Fibonacci numbers).",
				"Column 2: 1,2,3,4,5,6,7,8,...",
				"Row sums: A007051.",
				"Alternating row sums: A000129.",
				"For a discussion and guide to related arrays, see A208510.",
				"Subtriangle of the triangle given by (1, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, ...) DELTA (0, 1, 1, -1, 0, 0, 0, 0, 0, 0, 0, ...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, Mar 29 2012"
			],
			"formula": [
				"u(n,x) = u(n-1,x) + x*v(n-1,x),",
				"v(n,x) = (x-1)*u(n-1,x) + (x+2)*v(n-1,x),",
				"where u(1,x)=1, v(1,x)=1.",
				"From _Philippe Deléham_, Mar 29 2012: (Start)",
				"As DELTA-triangle T(n,k) with 0 \u003c= k \u003c= n:",
				"G.f.: (1 - 2*x - y*x + 2*y*x^2 - y^2*x^2)/(1 - 3*x - y*x + 2*x^2 + 2*y*x^2 - y^2*x^2).",
				"T(n,k) = 3*T(n-1,k) + T(n-1,k-1) - 2*T(n-2,k) - 2*T(n-2,k-1) + T(n-2,k-2), T(0,0) = T(1,0) = T(2,0) = T(2,1) = 1, T(1,1) = T(2,2) = 0 and T(n,k) = 0 if k \u003c 0 or if k \u003e n. (End)"
			],
			"example": [
				"First five rows:",
				"  1;",
				"  1,  1;",
				"  1,  2,  2;",
				"  1,  3,  7,  3;",
				"  1,  4, 17, 14,  5;",
				"First three polynomials u(n,x):",
				"  1",
				"  1 + x",
				"  1 + 2x + 2x^2.",
				"From _Philippe Deléham_, Mar 29 2012: (Start)",
				"(1, 0, 0, 2, 0, 0, 0, ...) DELTA (0, 1, 1, -1, 0, 0, 0, ...) begins:",
				"  1;",
				"  1,   0;",
				"  1,   1,   0;",
				"  1,   2,   2,   0;",
				"  1,   3,   7,   3,   0;",
				"  1,   4,  17,  14,   5,   0;",
				"  1,   5,  36,  42,  30,   8,   0;",
				"  1,   6,  72, 104, 111,  58,  13,   0; (End)"
			],
			"mathematica": [
				"u[1, x_] := 1; v[1, x_] := 1; z = 16;",
				"u[n_, x_] := u[n - 1, x] + (x + j)*v[n - 1, x] + c;",
				"d[x_] := h + x; e[x_] := p + x;",
				"v[n_, x_] := d[x]*u[n - 1, x] + e[x]*v[n - 1, x] + f;",
				"j = 0; c = 0; h = -1; p = 2; f = 0;",
				"Table[Expand[u[n, x]], {n, 1, z/2}]",
				"Table[Expand[v[n, x]], {n, 1, z/2}]",
				"cu = Table[CoefficientList[u[n, x], x], {n, 1, z}];",
				"TableForm[cu]",
				"Flatten[%]    (* A210791 *)",
				"cv = Table[CoefficientList[v[n, x], x], {n, 1, z}];",
				"TableForm[cv]",
				"Flatten[%]    (* A210792 *)",
				"Table[u[n, x] /. x -\u003e 1, {n, 1, z}]  (* A007051 *)",
				"Table[v[n, x] /. x -\u003e 1, {n, 1, z}]  (* A000244 *)",
				"Table[u[n, x] /. x -\u003e -1, {n, 1, z}] (* A001129 *)",
				"Table[v[n, x] /. x -\u003e -1, {n, 1, z}] (* A001333 *)"
			],
			"xref": [
				"Cf. A210792, A208510."
			],
			"keyword": "nonn,tabl",
			"offset": "1,5",
			"author": "_Clark Kimberling_, Mar 26 2012",
			"references": 3,
			"revision": 13,
			"time": "2020-01-26T20:55:36-05:00",
			"created": "2012-03-28T22:36:28-04:00"
		}
	]
}