{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A296515",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 296515,
			"data": "0,0,1,3,6,9,12,15,18,21,24,27,30,33,36,39,42,45,48,51,54,57,60,63,66,69,72,75,78,81,84,87,90,93,96,99,102,105,108,111,114,117,120,123,126,129,132,135,138",
			"name": "Number of edges in a maximal planar graph with n vertices.",
			"comment": [
				"{a(n)} is a monotonic increasing sequence because a maximal planar graph of order n can be generated on n + 1 nodes. Therefore a(n) \u003c= a(n + 1).",
				"Maximal planar graphs of order n \u003e 5 are not unique.",
				"|E(G_2n)| = (2n - 1) + 2*Sum_{k=0..(floor(log_2(n - 1)))} floor((n - 1)/2^k) where |E(G_2n)| is the size of a minimal planar graph G of order 2n.",
				"Number of edges of a maximal 3-degenerate graph of order n (this class includes 3-trees).  The intersection of this class and maximal planar graphs is the Apollonian networks (planar 3-trees); neither class contains the other. - _Allan Bickle_, Nov 14 2021"
			],
			"link": [
				"A. Bickle, \u003ca href=\"https://doi.org/10.7151/dmgt.1637\"\u003eStructural results on maximal k-degenerate graphs\u003c/a\u003e, Discuss. Math. Graph Theory 32 4 (2012), 659-676.",
				"Dawei He, Yan Wang and Xingxing Yu, \u003ca href=\"https://arxiv.org/abs/1511.05020\"\u003e The Kelmans-Seymour conjecture I\u003c/a\u003e, arXiv:1511.05020 [math.CO], 2015.",
				"D. R. Lick and A. T. White, \u003ca href=\"https://doi.org/10.4153/CJM-1970-125-1\"\u003ek-degenerate graphs\u003c/a\u003e, Canad. J. Math. 22 (1970), 1082-1096.",
				"K. Stephenson, \u003ca href=\"http://www.ams.org/notices/200311/fea-stephenson.pdf\"\u003e Circle Packing: A Mathematical Tale\u003c/a\u003e, Notices Amer. Math. Soc. 50 (2003).",
				"Squid Tamar-Mattis, \u003ca href=\"http://math.uchicago.edu/~may/REU2016/REUPapers/Tamar-Mattis.pdf\"\u003e Planar Graphs and Wagner's and Kuratowski's Theorems\u003c/a\u003e, REU (2016)",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/KuratowskiReductionTheorem.html\"\u003e Kuratowski Reduction Theorem\u003c/a\u003e, \u003ca href=\"http://mathworld.wolfram.com/PolyhedralGraph.html\"\u003e Polyhedral Graph\u003c/a\u003e",
				"David R. Wood, \u003ca href=\"https://arxiv.org/abs/cs/0505047\"\u003e A Simple Proof of the Fary-Wagner Theorem\u003c/a\u003e, arXiv:cs/0505047 [math.CO], 2005.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,-1)."
			],
			"formula": [
				"a(n) = floor(6/2^n) + 3n - 6 (see comments section of A008486).",
				"G.f.: x^2 +3*x^3/(x-1)^2. - _R. J. Mathar_, Apr 14 2018"
			],
			"mathematica": [
				"CoefficientList[Series[(x^2 (1 + x + x^2))/(x - 1)^2, {x, 0, 60}], x] (* or *)LinearRecurrence[{2, -1}, {0, 0, 1, 3}, 60] (* _Robert G. Wilson v_, Mar 04 2018 *)"
			],
			"xref": [
				"Cf. A242088, A008585."
			],
			"keyword": "nonn,easy",
			"offset": "0,4",
			"author": "_Joseph Wheat_, _Jonathan Sondow_, Feb 28 2018",
			"references": 0,
			"revision": 129,
			"time": "2021-12-15T11:06:46-05:00",
			"created": "2018-03-04T23:24:48-05:00"
		}
	]
}