{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A039724",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 39724,
			"data": "0,1,110,111,100,101,11010,11011,11000,11001,11110,11111,11100,11101,10010,10011,10000,10001,10110,10111,10100,10101,1101010,1101011,1101000,1101001,1101110,1101111,1101100,1101101,1100010,1100011,1100000,1100001,1100110,1100111,1100100",
			"name": "Numbers in base -2.",
			"comment": [
				"a(A007583(n)) are the only terms with all 1s digits; the number of digits = 2n + 1. - _Bob Selcoe_, Aug 21 2016"
			],
			"reference": [
				"M. Gardner, Knotted Doughnuts and Other Mathematical Entertainments. Freeman, NY, 1986, p. 101.",
				"D. E. Knuth, The Art of Computer Programming. Addison-Wesley, Reading, MA, 1969, Vol. 2, p. 189."
			],
			"link": [
				"William A. Tedeschi, \u003ca href=\"/A039724/b039724.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Joerg Arndt, \u003ca href=\"http://www.jjj.de/fxt/#fxtbook\"\u003eMatters Computational (The Fxtbook)\u003c/a\u003e, p. 58-59",
				"Roberto Avanzi, Gerhard Frey, Tanja Lange, and Roger Oyono, \u003ca href=\"https://arxiv.org/abs/math/0312060\"\u003eOn using expansions to the base of -2\u003c/a\u003e, International Journal of Computer Mathematics, 81:4 (2004), pp. 403-406. arXiv:math/0312060 [math.NT], 2003.",
				"Jaime Rangel-Mondragon, \u003ca href=\"http://demonstrations.wolfram.com/NegabinaryNumbersToDecimal/\"\u003eNegabinary Numbers to Decimal\u003c/a\u003e",
				"Vladimir Shevelev, \u003ca href=\"http://arxiv.org/abs/1603.04434\"\u003eTwo analogs of Thue-Morse sequence\u003c/a\u003e, arXiv:1603.04434 [math.NT], 2016.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Negabinary.html\"\u003eNegabinary\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Negative_base\"\u003eNegative base\u003c/a\u003e"
			],
			"formula": [
				"G.f. g(x) satisfies g(x) = (x + 10*x^2 + 11*x^3)/(1 - x^4) + 100(1 + x + x^2 + x^3)*g(x^4)/x^2. - _Robert Israel_, Feb 24 2016"
			],
			"example": [
				"2 = 4 + (-2) + 0 = 110_(-2), 3 = 4 + (-2) + 1 = 111_(-2), ..., 6 = 16 + (-8) + 0 + (-2) + 0 = 11010_(-2)."
			],
			"maple": [
				"f:= proc(n) option remember; 10*floor((n mod 4)/2) + (n mod 2) + 100*procname(round(n/4)) end proc:",
				"f(0):= 0:",
				"seq(f(i),i=0..100); # _Robert Israel_, Feb 24 2016"
			],
			"mathematica": [
				"ToNegaBases[ i_Integer, b_Integer ] := FromDigits[ Rest[ Reverse[ Mod[ NestWhileList[ (#1 - Mod[ #1, b ])/-b \u0026, i, #1 != 0 \u0026 ], b ] ] ] ]; Table[ ToNegaBases[ n, 2 ], {n, 0, 31} ]"
			],
			"program": [
				"(Haskell)",
				"a039724 0 = 0",
				"a039724 n = a039724 n' * 10 + m where",
				"   (n', m) = if r \u003c 0 then (q + 1, r + 2) else (q, r)",
				"             where (q, r) = quotRem n (negate 2)",
				"-- _Reinhard Zumkeller_, Jul 07 2012",
				"(Python)",
				"def A039724(n):",
				"    s, q = '', n",
				"    while q \u003e= 2 or q \u003c 0:",
				"        q, r = divmod(q, -2)",
				"        if r \u003c 0:",
				"            q += 1",
				"            r += 2",
				"        s += str(r)",
				"    return int(str(q)+s[::-1]) # _Chai Wah Wu_, Apr 09 2016",
				"(PARI) A039724(n)=if(n,A039724(n\\(-2))*10+bittest(n,0)) \\\\ _M. F. Hasler_, Oct 16 2018"
			],
			"xref": [
				"Nonnegative numbers in negative bases: A039723 (b=-10), this sequence (b=-2), A073785 (b=-3), A007608 (b=-4), A073786 (b=-5), A073787 (b=-6), A073788 (b=-7), A073789 (b=-8), A073790 (b=-9).",
				"Cf. A212529 (negative numbers in base -2).",
				"Cf. A005351, A007583."
			],
			"keyword": "base,nice,nonn,easy",
			"offset": "0,3",
			"author": "Robert Lozyniak (11(AT)onna.com)",
			"ext": [
				"More terms from _Eric W. Weisstein_"
			],
			"references": 47,
			"revision": 65,
			"time": "2020-11-05T15:26:04-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}