{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A121296",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 121296,
			"data": "10,11,13,16,20,28,45,73,133,348,4943,22779,537226,11662285,46524257772,1092759075796059,159271598072111595659,3317896028408943302861454961,594387514787460257685718548861374076357,91930654519343922607883279072515432244874866615525276",
			"name": "Descending dungeons: like A121295 but read subscripts from top downwards.",
			"comment": [
				"A \"dungeon\" of numbers."
			],
			"reference": [
				"David Applegate, Marc LeBrun and N. J. A. Sloane, Descending Dungeons and Iterated Base-Changing, in \"The Mathematics of Preference, Choice and Order: Essays in Honor of Peter Fishburn\", edited by Steven Brams, William V. Gehrlein and Fred S. Roberts, Springer, 2009, pp. 393-402."
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/A121296/b121296.txt\"\u003eTable of n, a(n) for n = 10..35\u003c/a\u003e",
				"David Applegate, Marc LeBrun and N. J. A. Sloane, \u003ca href=\"https://arxiv.org/abs/math/0611293\"\u003eDescending Dungeons and Iterated Base-Changing\u003c/a\u003e, arXiv:math/0611293 [math.NT], 2006-2007.",
				"David Applegate, Marc LeBrun, N. J. A. Sloane, \u003ca href=\"https://www.jstor.org/stable/40391135\"\u003eDescending Dungeons, Problem 11286\u003c/a\u003e, Amer. Math. Monthly, 116 (2009) 466-467.",
				"Brady Haran and Neil Sloane, \u003ca href=\"https://www.youtube.com/watch?v=xNx3JxRhnZE\"\u003eDungeon Numbers\u003c/a\u003e, Numberphile video (2020). \u003ca href=\"https://www.youtube.com/watch?v=HFeKdMf01rQ\"\u003e(extra)\u003c/a\u003e"
			],
			"formula": [
				"If a, b \u003e= 10, then a_b is roughly 10^(log(a)log(b)) (all logs are base 10 and \"roughly\" means it is an upper bound and using floor(log()) gives a lower bound). Equivalently, there exists c \u003e 0 such that for all a, b \u003e= 10, 10^(c log(a)log(b)) \u003c= a_b \u003c= 10^(log(a)log(b)). Thus a_n is roughly 10^product(log(9+i),i=1..n), or equivalently, a_n = 10^10^(n loglog n + O(n))."
			],
			"example": [
				"a(13) = ((13_12)_11)_10 = (15_11)_10 = 16_10 = 16."
			],
			"maple": [
				"asubb := proc(a,b) local t1; t1:=convert(a,base,10); add(t1[j]*b^(j-1),j=1..nops(t1)): end; # asubb(a,b) evaluates a as if it were written in base b",
				"s2:=[10]; for n from 11 to 35 do t1:=n; for i from 1 to n-10 do t1:=asubb(t1,n-i); od: s2:=[op(s2),t1]; od;"
			],
			"xref": [
				"Cf. A121263, A121265, A121295."
			],
			"keyword": "nonn,base",
			"offset": "10,1",
			"author": "_David Applegate_ and _N. J. A. Sloane_, Aug 25 2006",
			"references": 12,
			"revision": 15,
			"time": "2020-08-07T00:34:10-04:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}