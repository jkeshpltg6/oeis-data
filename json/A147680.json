{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A147680",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 147680,
			"data": "1,1,1,1,2,2,2,1,2,2,3,3,4,4,4,4,4,4,3,3,4,5",
			"name": "Number of disk polyominoes of order n (see Comments for definition).",
			"comment": [
				"Any closed disk in the real plane includes a finite set (possibly empty) of points from the square lattice Z^2.",
				"These roughly-circular patches of lattice points are connected by chains of adjacent lattice points (this is an easy theorem) and hence they form a special class of polyominoes, which I call \"disc polyominoes\".",
				"It's quite easy to calculate which lattice points are within a given radius of a given center, but the inverse problem can be a little challenging.",
				"That is, given a polyomino, determine whether it is a disk polyomino.",
				"I have been enumerating small disk polyominoes, to see how many configurations are possible for various numbers of lattice points.",
				"There is one disk polyomino for each of the orders 0, 1, 2 and 3; two for each of the orders 4, 5 and 6; only one for order 7; two each for orders 9 and 10; and three each for orders 11 and 12."
			],
			"example": [
				"The following is a list of the polyominoes that have been shown to be disks.",
				"I use the notation we used to use for small Life patterns, where each row is represented by the value of a binary number whose ones show which points are part of the configuration. These numbers are usually small, and we write the different row-descriptors with no delimiter between them, going up to letters of the alphabet if we run out of digits. We usually pick a scan order that minimizes the maximum description.",
				"For order 0, we of course have only (0), and for order 1 only (1).",
				"Order 2 gives (11), and order 3 gives the L-tromino (13). Order 4 has two examples, the block (33) and the T-tetromino (131). Order 5 gives the P-pentomino (133) and the X-pentomino (272).",
				"Order 6: (273), (333).",
				"Order 7: (373).",
				"Order 8: (377), (2772).",
				"Order 9: (777), (2773).",
				"Order 10: (2777), (3773), (27f6). (That \"f\" means 15, with four adjacent points in a row included in the polyomino.)",
				"Order 11: (3777), (27f7), (67f6).",
				"Order 12: (7777), (2ff7), (27f72), (6ff6).",
				"Order 13: (77f7), (6ff7), (27ff2),(4eve4). (The \"v\" represents a decimal 31, binary 11111, a row of five lattice-points.)",
				"Order 14: (7ff7), (2fff2), (27ff6), (4eve6).",
				"Order 15: (7fff), (2fff6), (4evee), (4evf6)."
			],
			"keyword": "nonn,more",
			"offset": "0,5",
			"author": "_Allan C. Wechsler_, Apr 30 2009",
			"ext": [
				"a(12) added by _Allan C. Wechsler_, May 12 2011, and a(13)-a(14) on Apr 09 2012",
				"a(15) added by _Allan C. Wechsler_, Apr 10 2012",
				"a(16)-a(21) added by _Allan C. Wechsler_, Apr 12 2012",
				"a(20) corrected from 3 to 4 by _Allan C. Wechsler_, Nov 07 2013"
			],
			"references": 0,
			"revision": 33,
			"time": "2016-04-13T17:26:45-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}