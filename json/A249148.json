{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A249148",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 249148,
			"data": "1,2,1,3,1,4,3,2,4,6,7,1,5,1,6,8,11,1,7,2,12,14,15,6,16,20,22,23,1,8,26,27,10,28,30,31,1,9,13,2,32,37,1,10,38,39,14,40,43,1,11,3,15,16,47,1,12,49,7,8,52,54,55,9,22,56,59,1,13,5,10,60,62,63,25,14,64,70,71,1,14,72,75,28,77,15,29,1,15,30,78,79,1,16,83",
			"name": "a(1) = 1, after which, if a(n-1) = 1, a(n) = 1 + the total number of 1's that have occurred in the sequence so far, otherwise a(n) = the total number of times the least prime dividing a(n-1) [i.e., A020639(a(n-1))] occurs as a divisor (counted with multiplicity for each term) in the previous terms from a(1) up to and including a(n-1).",
			"comment": [
				"Inspired by A248034.",
				"After a(1), it is very likely that 1's occur only just after primes, although they do not necessarily occur after every prime. For example, 13 is the first prime whose initial occurrence is not followed by 1."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A249148/b249148.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"a(1) = 1 by definition.",
				"For n = 2, we see that a(1) = 1, which is the only 1 that has occurred in the sequence so far, and thus a(2) = 1+1 = 2.",
				"For n = 3, we see that a(2) = 2, with the least prime dividing it being 2, which has occurred so far only once (namely in a(2)), thus a(3) = 1.",
				"For n = 4, we see that a(3) = 1, and there has occurred two 1's so far (as a(1) and a(3)), thus a(4) = 2+1 = 3.",
				"For n = 5, we see that a(4) = 3, with the least prime dividing it being 3, which has occurred now just once, thus a(5) = 1.",
				"For n = 6, we see that a(5) = 1, and there has occurred three 1's so far (as a(1), a(3) and a(5)), thus a(6) = 3+1 = 4.",
				"For n = 7, we see that a(6) = 4 = 2*2, with its least prime 2 dividing it two times, and also occurring once at a(2), thus a(7) = 3."
			],
			"program": [
				"(PARI)",
				"A049084(n) = if(isprime(n), primepi(n), 0); \\\\ This function from _Charles R Greathouse IV_",
				"A249148_write_bfile(up_to_n) = { my(pfcounts, n, a_n, f, k); pfcounts = vector(up_to_n); a_n = 1; for(n = 1, up_to_n, if((1 == a_n), pfcounts[1]++; a_n = pfcounts[1], f=factor(a_n); for(i=1,#f~,k = A049084(f[i,1])+1; pfcounts[k] += f[i,2]); a_n = pfcounts[A049084(f[1,1])+1]); write(\"b249148.txt\", n, \" \", a_n)); };",
				"A249148_write_bfile(10000);",
				"(MIT/GNU Scheme with memoizing definec-macro from _Antti Karttunen_'s IntSeq-library and factor function from Aubrey Jaffer's SLIB-library)",
				"(definec (A249148 n) (if (= 1 n) 1 (vector-ref (A249148aux_primefactor_counts (- n 1)) (A055396 (A249148 (- n 1))))))",
				"(definec (A249148aux_primefactor_counts n) (cond ((= 1 n) (vector 2)) (else (let* ((a_n (A249148 n)) (copy-of-prevec (vector-copy (A249148aux_primefactor_counts (- n 1)))) (newsize (max (vector-length copy-of-prevec) (+ 1 (A061395 a_n)))) (pf_counts_vec (vector-grow copy-of-prevec newsize))) (let loop ((pf_indices (map A049084 (factor a_n)))) (cond ((null? pf_indices) pf_counts_vec) (else (vector-set! pf_counts_vec (car pf_indices) (+ 1 (or (vector-ref pf_counts_vec (car pf_indices)) 0))) (loop (cdr pf_indices)))))))))"
			],
			"xref": [
				"Cf. A020639, A049084, A055396, A061395, A249147, A248034, A249144, A249069, A249070."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Antti Karttunen_, Oct 24 2014",
			"references": 6,
			"revision": 17,
			"time": "2014-10-25T14:49:29-04:00",
			"created": "2014-10-25T14:49:29-04:00"
		}
	]
}