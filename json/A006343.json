{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006343",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6343,
			"id": "M3400",
			"data": "1,0,1,1,4,10,34,112,398,1443,5387,20482,79177,310102,1228187,4910413,19792582,80343445,328159601,1347699906,5561774999,23052871229,95926831442,400587408251,1678251696379,7051768702245,29710764875014",
			"name": "Arkons: number of elementary maps with n-1 nodes.",
			"reference": [
				"K. Appel and W. Haken, Every planar map is four colorable. With the collaboration of J. Koch. Contemporary Mathematics, 98. American Mathematical Society, Providence, RI, 1989. xvi+741 pp. ISBN: 0-8218-5103-9.",
				"F. R. Bernhart, Topics in Graph Theory Related to the Five Color Conjecture. Ph.D. Dissertation, Kansas State Univ., 1974.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A006343/b006343.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"F. R. Bernhart, \u003ca href=\"http://dx.doi.org/10.1016/S0012-365X(99)00054-0\"\u003eCatalan, Motzkin and Riordan numbers\u003c/a\u003e, Discr. Math., 204 (1999), 73-112.",
				"F. R. Bernhart, \u003ca href=\"/A000296/a000296_1.pdf\"\u003eFundamental chromatic numbers\u003c/a\u003e, Unpublished. (Annotated scanned copy)",
				"F. R. Bernhart \u0026 N. J. A. Sloane, \u003ca href=\"/A001683/a001683.pdf\"\u003eCorrespondence, 1977\u003c/a\u003e",
				"F. R. Bernhart \u0026 N. J. A. Sloane, \u003ca href=\"/A006343/a006343.pdf\"\u003eEmails, April-May 1994\u003c/a\u003e",
				"G. D. Birkhoff and D. C. Lewis, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9947-1946-0018401-4\"\u003eChromatic polynomials\u003c/a\u003e, Trans. Amer. Math. Soc. 60, (1946). 355-451."
			],
			"formula": [
				"a(n-1) = Sum (n-k-1)^(-1)*binomial(n, k)*binomial(2*n-3*k-4, n-2*k-2); k = 0..[ (n-2)/2 ], n \u003e= 3.",
				"From _Peter Bala_, Jun 22 2015: (Start)",
				"O.g.f. A(x) equals 1/x * series reversion ( x/(1 + x^2*C(x) ), where C(x) = (1 - sqrt(1 - 4*x))/(2*x) is the o.g.f. for A000108.",
				"A(x) is an algebraic function satisfying x^3*A^3(x) - (x - 1)*A^2(x) + (x - 2)*A(x) + 1 = 0. (End)",
				"a(n) ~ sqrt(s*(1 - s + 3*r^2*s^2) / (1 - r + 3*r^3*s)) / (2*sqrt(Pi) * n^(3/2) * r^(n - 1/2)), where r = 0.2229935155751761877673240243525445951244491757706... and s = 1.116796494086474135831052534637944909439048671327... are real roots of the system of equations 1 + (r-2)*s + r^3*s^3 = (r-1)*s^2, r + 2*s + 3*r^3*s^2 = 2 + 2*r*s. - _Vaclav Kotesovec_, Nov 27 2017",
				"Conjecture: D-finite with recurrence: -(n+3)*(n-1)*a(n) +(11*n^2-2*n-45)*a(n-1) -(37*n+29)*(n-3)*a(n-2) +(29*n^2-125*n+78)*a(n-3) +(61*n-106)*(n-3)*a(n-4) -155*(n-3)*(n-4)*a(n-5)=0. - _R. J. Mathar_, Feb 20 2020"
			],
			"maple": [
				"A006343:=n-\u003eadd(binomial(n,k)*binomial(2*n-3*k-4,n-2*k-2)/(n-k-1), k=0..(n-2)/2): (1, seq(A006343(n), n=1..30)); # _Wesley Ivan Hurt_, Jun 22 2015"
			],
			"mathematica": [
				"a[n_] := Sum[ Binomial[n, k]*Binomial[2*n-3*k-4, n-2*k-2]/(n-k-1), {k, 0, (n-2)/2}]; a[0] = 1; Table[a[n], {n, 0, 26}] (* _Jean-François Alcover_, Dec 14 2012, from formula *)"
			],
			"program": [
				"(Haskell)",
				"a006343 0 = 1",
				"a006343 n = sum $ zipWith div",
				"   (zipWith (*) (map (a007318 n) ks)",
				"                (map (\\k -\u003e a007318 (2*n - 3*k - 4) (n - 2*k - 2)) ks))",
				"   (map (toInteger . (n - 1 -)) ks)",
				"   where ks = [0 .. (n - 2) `div` 2]",
				"-- _Reinhard Zumkeller_, Aug 23 2012"
			],
			"xref": [
				"Cf. A000108, A000934, A007318."
			],
			"keyword": "easy,nonn,nice",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Erroneously duplicated term 4 removed per Frank Bernhart's report by _Max Alekseyev_, Feb 11 2010"
			],
			"references": 13,
			"revision": 51,
			"time": "2020-02-20T03:38:30-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}