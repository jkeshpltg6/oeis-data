{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A004642",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 4642,
			"data": "1,2,11,22,121,1012,2101,11202,100111,200222,1101221,2210212,12121201,102020102,211110211,1122221122,10022220021,20122210112,111022121001,222122012002,1222021101011,10221112202022,21220002111121,120210012000012,1011120101000101,2100010202000202",
			"name": "Powers of 2 written in base 3.",
			"comment": [
				"When n is odd, a(n) ends in 1, and when n is even, a(n) ends in 2, since 2^n is congruent to 1 mod 3 when n is odd and to 2 mod 3 when n is even. - _Alonso del Arte_ Dec 11 2009",
				"Sloane (1973) conjectured a(n) always has a 0 between the most and least significant digits if n \u003e 15. This has been verified up to n = 10^5 (see A102483). - _Alonso del Arte_, Feb 20 2011",
				"Erdos (1978) conjectured that for n \u003e 8 a(n) has at least one 2 (see link to Terry Tao's blog). - _Dmitry Kamenetsky_, Jan 10 2017"
			],
			"reference": [
				"Sloane, N. J. A. \"The Persistence of a Number.\" J. Recr. Math. 6 (1973): 97 - 98"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A004642/b004642.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Jeffrey C. Lagarias, \u003ca href=\"http://arxiv.org/pdf/math/0512006v4.pdf\"\u003eTernary Expansions of Powers of 2\u003c/a\u003e, ArXiv:math/0512006v4 [math.DS], 2008.",
				"Terry Tao, \u003ca href=\"https://terrytao.wordpress.com/2011/08/25/the-collatz-conjecture-littlewood-offord-theory-and-powers-of-2-and-3/\"\u003eThe Collatz Conjecture\u003c/a\u003e, 2011.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Ternary.html\"\u003eTernary\u003c/a\u003e"
			],
			"mathematica": [
				"Table[FromDigits[IntegerDigits[2^n, 3]], {n, 25}] (* _Alonso del Arte_ Dec 11 2009 *)"
			],
			"program": [
				"(PARI) a(n)=fromdigits(digits(2^n,3)) \\\\ _M. F. Hasler_, Jun 23 2018",
				"(MAGMA) [Seqint(Intseq(2^n, 3)): n in [0..30]]; // _G. C. Greubel_, Sep 10 2018"
			],
			"xref": [
				"Cf. A000079, Powers of 2 written in base 10.",
				"Cf. A004643, ..., A004655: powers of 2 written in base 4, 5, ..., 16",
				"Cf. A004656, A004658, A004659, ... : powers of 3 written in base 2, 4, 5, ..."
			],
			"keyword": "nonn,base,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 17,
			"revision": 35,
			"time": "2018-09-11T03:15:48-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}