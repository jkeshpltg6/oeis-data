{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A182057",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 182057,
			"data": "1,1,0,0,0,1,0,0,-2,-3,0,0,4,1,0,0,-1,5,0,0,-8,-10,0,0,14,4,0,0,-4,17,0,0,-23,-31,0,0,40,9,0,0,-10,46,0,0,-60,-79,0,0,98,21,0,0,-24,112,0,0,-140,-183,0,0,224,46,0,0,-54,249,0,0,-304,-396,0",
			"name": "Expansion of psi(x) * f(x^4) / (psi(x^3) * f(x^6) * chi(-x^24)) in powers of x where psi(), chi(), f() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A182057/b182057.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(-2/3) * eta(q^2)^2 * eta(q^3) * eta(q^8)^3 * eta(q^48) / (eta(q) * eta(q^4) * eta(q^6) * eta(q^12)^3 * eta(q^16)) in powers of q.",
				"Euler transform of period 48 sequence [1, -1, 0, 0, 1, -1, 1, -3, 0, -1, 1, 3, 1, -1, 0, -2, 1, -1, 1, 0, 0, -1, 1, 0, 1, -1, 0, 0, 1, -1, 1, -2, 0, -1, 1, 3, 1, -1, 0, -3, 1, -1, 1, 0, 0, -1, 1, 0, ...].",
				"a(4*n + 2) = a(4*n + 3) = 0. a(4*n) = A182032(12*n + 2). a(4*n + 1) = A182032(12*n + 5).",
				"Expansion of psi(x) * f(x^4) / (f(x^3) * phi(x^6) * chi(x^12)) in powers of x where phi(), psi(), f() are Ramanujan theta functions. - _Michael Somos_, Aug 10 2017"
			],
			"example": [
				"G.f. = 1 + x + x^5 - 2*x^8 - 3*x^9 + 4*x^12 + x^13 - x^16 + 5*x^17 - 8*x^20 + ...",
				"G.f. = q^2 + q^5 + q^17 - 2*q^26 - 3*q^29 + 4*q^38 + q^41 - q^50 + 5*q^53 + ..."
			],
			"mathematica": [
				"eta[x_] := x^(1/24)*QPochhammer[x]; A182057[n_] := SeriesCoefficient[ q^(-2/3)*eta[q^2]^2*eta[q^3]*eta[q^8]^3*eta[q^48]/(eta[q]*eta[q^4]* eta[q^6]*eta[q^12]^3 *eta[q^16]), {q, 0, n}]; Table[A182057[n], {n,0,50}] (* _G. C. Greubel_, Aug 10 2017 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A)^2 * eta(x^3 + A) * eta(x^8 + A)^3 * eta(x^48 + A) / (eta(x + A) * eta(x^4 + A) * eta(x^6 + A) * eta(x^12 + A)^3 * eta(x^16 + A)), n))}"
			],
			"xref": [
				"Cf. A182032."
			],
			"keyword": "sign",
			"offset": "0,9",
			"author": "_Michael Somos_, Apr 08 2012",
			"references": 2,
			"revision": 15,
			"time": "2021-03-12T22:24:46-05:00",
			"created": "2012-04-09T13:55:25-04:00"
		}
	]
}