{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A344845",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 344845,
			"data": "1,2,4,5,8,9,10,12,16,15,18,20,23,24,28,32,30,36,34,38,39,42,49,45,48,52,55,58,56",
			"name": "a(n)/2 is the smallest possible area of a non-obtuse triangle with integer coordinates in the plane and with shortest side of length sqrt(A001481(n+1)).",
			"comment": [
				"Since sqrt(m) is a distance between two points in Z^2 iff m is the sum of two squares, the \"shortest side length\" requirement can only be met for m in A001481\\{0}. Thus this sequence cannot be extended to more arguments.",
				"It has been shown that A344710(n) can be described and computed as min{a(k) | n \u003c= A001481(k+1) \u003c 5n/4}.",
				"For the following comments, let m := A001481(n+1).",
				"The validity of a triangle can be checked via four Diophantine inequalites: three Euclidean norms for the distances, and one derived from the law of cosines 2*max{|AB|,|BC|,|CA|}\u003c=|AB|+|BC|+|CA| for the angles.",
				"It was proved that a valid triangle with a sidelength of sqrt(2m) or larger has at least area m/2, and that a valid triangle with area m/2 always exists.",
				"All valid triangles with an area smaller than m/2 can be found by checking for triangles with no sidelength of sqrt(2m) or longer, and at most one sidelength of sqrt(5m/4) or longer.",
				"These criteria can be used to set A to (0,0), and look for B in the set of points X with |AX| = sqrt(m), and C in the set of points X with sqrt(m) \u003c= |AX| \u003c sqrt(5m/4). Furthermore, B can be assumed to be in the first octant, and C in a different octant but at most 2 octants away. It has been shown that this suffices to find congruent versions of all valid triangles with an area below m/2.",
				"The sequence c(n) := min{a(k) | k \u003e= n} is an upper bound on the sequence b(n) := ceiling(1/x(n)), where x(n) is the supremum on the density of marked points (\"dots\") in the discrete plane Z^2 with increasing pairwise minimum distance. Up to n=10, the two sequences have been shown to contain the same terms.",
				"It has been shown that an alternative interpretation of the problem described in b(n) is the packing of circles with increasing diameter with centers in the discrete plane Z^2.",
				"For [b(n)], it is conjectured that 1/x(n) is always an integer, making the ceiling function omittable for the definition."
			],
			"link": [
				"Jonathan F. Waldmann, \u003ca href=\"https://docs.google.com/spreadsheets/d/19x9Qj8HcmQXVD855IMFH_3MZt70RBwckLld2zYk53L0/edit?usp=sharing\"\u003eA more nuanced upright triangle sequence\u003c/a\u003e",
				"Jonathan F. Waldmann, \u003ca href=\"https://docs.google.com/spreadsheets/d/19ylTMfMzm9XVvOOYr0xYvFAMOHt0wbQn3x-Q1ObSAIs\"\u003eAn algorithm for the upright triangle sequence\u003c/a\u003e",
				"Jonathan F. Waldmann, \u003ca href=\"https://docs.google.com/spreadsheets/d/1cJo_cP683PiU3ZUKEtNWKvSSMfGMhcI4efwyVwFBb4M\"\u003eProofs for the first few terms in the discrete circle packing sequence\u003c/a\u003e",
				"Jonathan F. Waldmann, \u003ca href=\"https://docs.google.com/spreadsheets/d/1bKbl6Qq_PsWtb7zBx_PPmbrlIeVmAMioJ7_gFJRUae0\"\u003eMore proofs for the discrete circle packing sequence\u003c/a\u003e"
			],
			"formula": [
				"Let m := A001481(n+1).",
				"   a(n) = min{m, 2*min{area(ABC) | A, B, C in Z^2;",
				"   A = (0,0);",
				"   B = (a,b) with a \u003e= b \u003e= 0;",
				"   |AB| = sqrt(m);",
				"   C = (c,d) with c \u003e= 0 or d \u003e= c;",
				"   sqrt(m) \u003c= |AC| \u003c sqrt(5m/4);",
				"   sqrt(m) \u003c= |BC| \u003c sqrt(2m) } } (proved in \"A more nuanced upright triangle sequence\").",
				"a(n) \u003c= m.",
				"a(n) \u003e sqrt(3/4)*m (conjectured)."
			],
			"example": [
				"[a(n)]: For n = 3, i.e. A001481(n+1) = 4, a triangle with a shortest sidelength of sqrt(4) and the minimal area of 4/2 = 2 can be placed at A=(0,0), B=(2,0), and C=(0,2). Alternatively, C can be placed at (1,2) or (2,2). -\u003e a(3) = 4.",
				"[b(n)]: For the smallest 3 minimum distances sqrt(1), sqrt(2), and sqrt(4), the following repeating patterns (X for dots, O for empty spaces) achieve the highest possible densities of 1, 1/2, and 1/4 respectively:",
				"   XXXXXX   OXOXOX   OXOXOX",
				"   XXXXXX   XOXOXO   OOOOOO",
				"   XXXXXX   OXOXOX   OXOXOX",
				"   XXXXXX   XOXOXO   OOOOOO"
			],
			"xref": [
				"Cf. A001481, A344710."
			],
			"keyword": "nonn,more",
			"offset": "1,2",
			"author": "_Jonathan F. Waldmann_, May 29 2021",
			"references": 1,
			"revision": 48,
			"time": "2021-07-28T07:27:17-04:00",
			"created": "2021-07-28T07:27:17-04:00"
		}
	]
}