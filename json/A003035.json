{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003035",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3035,
			"id": "M0982",
			"data": "0,0,1,1,2,4,6,7,10,12,16,19,22,26",
			"name": "Maximal number of 3-tree rows in n-tree orchard problem.",
			"comment": [
				"It is known that a(15) is 31 or 32, a(16)=37 and a(17) is 40, 41 or 42. - _N. J. A. Sloane_, Feb 11 2013"
			],
			"reference": [
				"P. Brass et al., Research Problems in Discrete Geometry, Springer, 2005.",
				"S. A. Burr, in The Mathematical Gardner, Ed. D. A. Klarner, p. 94, Wadsworth, 1981.",
				"S. A. Burr, B. Grünbaum and N. J. A. Sloane, The Orchard Problem, Geometriae Dedicata, 2 (1974), 397-424.",
				"Jean-Paul Delahaye, Des points qui s'alignent... ou pas, \"Logique et calcul\" column, \"Pour la science\", June 2021.",
				"H. E. Dudeney, Amusements in Mathematics, Nelson, London, 1917, page 56.",
				"Paul Erdos and George Purdy. Extremal problems in geometry, Chapter 17, pages 809-874 in R. L. Graham et al., eds., Handbook of Combinatorics, 2 vols., MIT Press, 1995. See Section 3.7.",
				"M. Gardner, Time Travel and Other Mathematical Bewilderments. Freeman, NY, 1988, Chap. 22.",
				"B. Grünbaum, Arrangements and Spreads. American Mathematical Society, Providence, RI, 1972, p. 22.",
				"John Jackson, Rational Amusements for Winter Evenings, London, 1821.",
				"F. Levi, Geometrische Konfigurationen, Hirzel, Leipzig, 1929.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"S. A. Burr, B. Grünbaum and N. J. A. Sloane, \u003ca href=\"http://neilsloane.com/doc/ORCHARD/orchard.html\"\u003eThe Orchard Problem\u003c/a\u003e, Geometriae Dedicata, 2 (1974), 397-424.",
				"Zhao Hui Du, \u003ca href=\"http://zdu.spaces.live.com/blog/cns!C95152CB25EF2037!122.entry\"\u003eOrchard Planting Problem\u003c/a\u003e [From _Zhao Hui Du_, Nov 20 2008] [Seems to concentrate on the 4 trees per line version. - _N. J. A. Sloane_, Oct 16 2010]",
				"Noam D. Elkies, \u003ca href=\"https://arxiv.org/abs/math/0612749\"\u003eOn some points-and-lines problems and configurations\u003c/a\u003e, arXiv:math/0612749 [math.MG], 2006; [Concerned with other versions of the problem].",
				"Erich Friedman, \u003ca href=\"https://erich-friedman.github.io/packing/trees/\"\u003eTable of values and bounds for up to 25 trees\u003c/a\u003e",
				"Z. Furedi and I. Palasti, \u003ca href=\"https://doi.org/10.1090/S0002-9939-1984-0760946-2\"\u003eArrangements of lines with a large number of triangles\u003c/a\u003e, Proc. Amer. Math. Soc., 92(4):561-566, 1984.",
				"B. Green, T. Tao, \u003ca href=\"https://arxiv.org/abs/1208.4714\"\u003eOn sets defining few ordinary lines\u003c/a\u003e, arXiv:1208.4714.  (Shows that a(n) = [n(n-3)/6]+1 for all sufficiently large n.)",
				"R. Padmanabhan, Alok Shukla, \u003ca href=\"https://arxiv.org/abs/2003.07172\"\u003eOrchards in elliptic curves over finite fields\u003c/a\u003e, arXiv:2003.07172 [math.NT], 2020.",
				"Ed Pegg, Jr., \u003ca href=\"http://blog.wolfram.com/2018/02/02/cultivating-new-solutions-for-the-orchard-planting-problem/\"\u003eCultivating New Solutions for the Orchard-Planting Problem\u003c/a\u003e",
				"Ed Pegg, Jr., \u003ca href=\"/A003035/a003035_1.jpg\"\u003eIllustration showing that a(15) \u003e= 31\u003c/a\u003e [Another version that uses all 31 triples from -7 to 7 which sum to 0 (mod 15). Coordinates are: {-7, {-1 - Sqrt[3], -1 + 2 Sqrt[3]}}, {-6, {2 (2 + Sqrt[3]), -5}}, {-5, {0, -3}}, {-4, {-2 (2 + Sqrt[3]), -1}}, {-3, {-2, 1}}, {-2, {2, -1}}, {-1, {2 (2 + Sqrt[3]), 1}}, {0, {0, 3}}, {1, {-2 (2 + Sqrt[3]), 5}}, {2, {1 + Sqrt[3], 1 - 2 Sqrt[3]}}, {3, {-2 (2 + Sqrt[3]), -1 - 2 Sqrt[3]}}, {4, {-2 - Sqrt[3], 1}}, {5, {0, 0}}, {6, {2 + Sqrt[3], -1}}, {7, {2 (2 + Sqrt[3]), 1 + 2 Sqrt[3]}}]",
				"Ed Pegg, Jr., \u003ca href=\"/A003035/a003035_3.jpg\"\u003eIllustration showing that a(15) \u003e= 31 and a(16) \u003e= 37\u003c/a\u003e",
				"Ed Pegg, Jr., \u003ca href=\"/A003035/a003035_2.jpg\"\u003eIllustration for a(16) = 37\u003c/a\u003e [Based on a drawing in Burr-Grünbaum-Sloane (1974). The bottom left point is at -(sqrt(3), sqrt(5)). Note that 3 points and one line are at infinity.]",
				"Ed Pegg, Jr., \u003ca href=\"/A003035/a003035_4.jpg\"\u003eIllustrations of constructions for 9 through 28 trees.\u003c/a\u003e",
				"G. B. Purdy and J. W. Smith, \u003ca href=\"https://doi.org/10.1007/s00454-010-9270-3\"\u003eLines, circles, planes and spheres\u003c/a\u003e, Discrete Comput. Geom., 44 (2010), 860-882. [Makes use of A003035 in a formula. - _N. J. A. Sloane_, Oct 19 2017]",
				"N. J. A. Sloane, \u003ca href=\"/A003035/a003035.gif\"\u003eIllustration of initial terms (from Grünbaum-Burr-Sloane paper)\u003c/a\u003e",
				"J. Solymosi and M. Stojakovic, \u003ca href=\"https://doi.org/10.1007/s00454-013-9526-9\"\u003eMany collinear k-tuples with no k + 1 collinear points\u003c/a\u003e, Discrete \u0026 Computational Geometry, October 2013, Volume 50, Issue 3, pp 811-820; also arXiv 1107.0327, 2013.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Orchard-PlantingProblem.html\"\u003eOrchard-Planting Problem.\u003c/a\u003e"
			],
			"xref": [
				"Cf. A006065 (4 trees/row), A008997 (5 trees per row), A058212."
			],
			"keyword": "nonn,nice,hard,more",
			"offset": "1,5",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"13 and 14 trees result from _Zhao Hui Du_, Nov 20 2008",
				"Replaced my old picture with link to my write-up. - _Ed Pegg Jr_, Feb 02 2018"
			],
			"references": 5,
			"revision": 111,
			"time": "2021-05-29T22:08:23-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}