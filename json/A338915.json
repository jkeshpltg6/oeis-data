{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A338915",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 338915,
			"data": "0,0,0,0,1,0,1,1,4,2,6,6,12,12,20,22,38,42,60,73,101,124,164,203,266,319,415,507,649,786,983,1198,1499,1797,2234,2673,3303,3952,4826,5753,6999",
			"name": "Number of integer partitions of n that have an even number of parts and cannot be partitioned into distinct pairs of not necessarily distinct parts.",
			"comment": [
				"The multiplicities of such a partition form a non-loop-graphical partition (A339655, A339657)."
			],
			"link": [
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/GraphicalPartition.html\"\u003eGraphical partition.\u003c/a\u003e"
			],
			"formula": [
				"A027187(n) = a(n) + A338916(n)."
			],
			"example": [
				"The a(7) = 1 through a(12) = 12 partitions:",
				"  211111  2222      411111    222211      222221      3333",
				"          221111    21111111  331111      611111      222222",
				"          311111              511111      22211111    441111",
				"          11111111            22111111    32111111    711111",
				"                              31111111    41111111    22221111",
				"                              1111111111  2111111111  32211111",
				"                                                      33111111",
				"                                                      42111111",
				"                                                      51111111",
				"                                                      2211111111",
				"                                                      3111111111",
				"                                                      111111111111",
				"For example, the partition y = (3,2,2,1,1,1,1,1) can be partitioned into pairs in just three ways:",
				"  {{1,1},{1,1},{1,2},{2,3}}",
				"  {{1,1},{1,1},{1,3},{2,2}}",
				"  {{1,1},{1,2},{1,2},{1,3}}",
				"None of these is strict, so y is counted under a(12)."
			],
			"mathematica": [
				"smcs[n_]:=If[n\u003c=1,{{}},Join@@Table[Map[Prepend[#,d]\u0026,Select[smcs[n/d],Min@@#\u003ed\u0026]],{d,Select[Rest[Divisors[n]],PrimeOmega[#]==2\u0026]}]];",
				"Table[Length[Select[IntegerPartitions[n],EvenQ[Length[#]]\u0026\u0026smcs[Times@@Prime/@#]=={}\u0026]],{n,0,10}]"
			],
			"xref": [
				"The Heinz numbers of these partitions are A320892.",
				"The complement in even-length partitions is A338916.",
				"A000070 counts non-multigraphical partitions of 2n, ranked by A339620.",
				"A000569 counts graphical partitions, ranked by A320922.",
				"A001358 lists semiprimes, with squarefree case A006881.",
				"A058696 counts partitions of even numbers, ranked by A300061.",
				"A209816 counts multigraphical partitions, ranked by A320924.",
				"A320655 counts factorizations into semiprimes.",
				"A322353 counts factorizations into distinct semiprimes.",
				"A339617 counts non-graphical partitions of 2n, ranked by A339618.",
				"A339655 counts non-loop-graphical partitions of 2n, ranked by A339657.",
				"A339656 counts loop-graphical partitions, ranked by A339658.",
				"The following count partitions of even length and give their Heinz numbers:",
				"- A027187 has no additional conditions (A028260).",
				"- A096373 cannot be partitioned into strict pairs (A320891).",
				"- A338914 can be partitioned into strict pairs (A320911).",
				"- A338916 can be partitioned into distinct pairs (A320912).",
				"- A339559 cannot be partitioned into distinct strict pairs (A320894).",
				"- A339560 can be partitioned into distinct strict pairs (A339561).",
				"Cf. A001055, A007717, A025065, A320656, A320732, A320893, A338898, A338902."
			],
			"keyword": "nonn,more",
			"offset": "0,9",
			"author": "_Gus Wiseman_, Dec 10 2020",
			"references": 23,
			"revision": 19,
			"time": "2021-02-12T11:36:20-05:00",
			"created": "2020-12-18T07:58:27-05:00"
		}
	]
}