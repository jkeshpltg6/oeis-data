{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A245925",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 245925,
			"data": "1,-3,25,-243,2601,-29403,344569,-4141875,50737129,-630663003,7930793025,-100681224075,1288236350025,-16592960274075,214939203248025,-2797935722568243,36578032462268649,-480000660000226875,6320012816203363489,-83462977778600141643,1105193229806740453201",
			"name": "G.f.: Sum_{n\u003e=0} x^n*Sum_{k=0..n} (-1)^k * C(n,k)^2 * Sum_{j=0..k} C(k,j)^2 * x^j.",
			"comment": [
				"The g.f.s formed from a(2*n)^(1/2) and (-a(2*n+1)/3)^(1/2) are:",
				"A245926: sqrt( (1-x + sqrt(1-14*x+x^2)) / (2*(1-14*x+x^2)) );",
				"A245927: sqrt( (1-x - sqrt(1-14*x+x^2)) / (6*x*(1-14*x+x^2)) ).",
				"Lim_{n-\u003einfinity} a(n+1)/a(n) = -(7 + 4*sqrt(3))."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A245925/b245925.txt\"\u003eTable of n, a(n) for n = 0..500\u003c/a\u003e",
				"A. Bostan, S. Boukraa, J.-M. Maillard, and J.-A. Weil, \u003ca href=\"http://arxiv.org/abs/1507.03227\"\u003eDiagonals of rational functions and selected differential Galois groups\u003c/a\u003e, arXiv preprint arXiv:1507.03227 [math-ph], 2015."
			],
			"formula": [
				"G.f.: Sum_{n\u003e=0} x^n / (1+x)^(2*n+1) * ( Sum_{k=0..n} C(n,k)^2*(-x)^k )^2.",
				"G.f.: 1 / AGM(1-x^2, sqrt(1+14*x^2+x^4)), where AGM(x,y) = AGM((x+y)/2, sqrt(x*y)) is the arithmetic-geometric mean.",
				"a(2*n) = A245926(n)^2.",
				"a(2*n+1) = (-3)*A245927(n)^2.",
				"a(n) = Sum_{k=0..n} Sum_{j=0..2*n-2*k} (-1)^(j+k) * C(2*n-k,j+k)^2 * C(j+k,k)^2.",
				"D-finite with recurrence: n^2*(2*n-3)*a(n) = -(2*n-1)*(13*n^2 - 26*n + 10)*a(n-1) + (2*n-3)*(13*n^2 - 26*n + 10)*a(n-2) + (n-2)^2*(2*n-1)*a(n-3). - _Vaclav Kotesovec_, Aug 16 2014",
				"a(n) ~ (-1)^n * (2+sqrt(3)) * (7+4*sqrt(3))^n / (4*Pi*n). - _Vaclav Kotesovec_, Aug 16 2014",
				"a(n) = (-1)^n*Sum_{k=0..n} binomial(2*(n-k), n-k)*binomial(2*n-k, k)^2. - _Peter Luschny_, Aug 17 2014",
				"a(n) = (-1)^n*binomial(2*n,n)*hyper4F3([-n,-n,-n,-n+1/2],[1,-2*n,-2*n], 4). - _Peter Luschny_, Aug 17 2014",
				"a(n) = Sum_{k=0..n} (-1)^k * C(2*k, k)^2 * C(n+k, n-k). - _Paul D. Hanna_, Aug 17 2014",
				"a(n) = (-1)^n*hypergeom([-n, -n, n + 1, n + 1], [1/2, 1, 1], 1/4). - _Peter Luschny_, Mar 14 2018",
				"a(n) = Legendre_P(n, sqrt(-3))^2. - _Peter Bala_, Dec 22 2020"
			],
			"example": [
				"G.f.: A(x) = 1 - 3*x^2 + 25*x^4 - 243*x^6 + 2601*x^8 - 29403*x^10 + ...",
				"where the g.f. is given by the binomial series:",
				"A(x) = 1 + x*(1 - (1+x)) + x^2*(1 - 2^2*(1+x) + (1+2^2*x+x^2))",
				"+ x^3*(1 - 3^2*(1+x) + 3^2*(1+2^2*x+x^2) - (1+3^2*x+3^2*x^2+x^3))",
				"+ x^4*(1 - 4^2*(1+x) + 6^2*(1+2^2*x+x^2) - 4^2*(1+3^2*x+3^2*x^2+x^3) + (1+4^2*x+6^2*x^2+4^2*x^3+x^4))",
				"+ x^5*(1 - 5^2*(1+x) + 10^2*(1+2^2*x+x^2) - 10^2*(1+3^2*x+3^2*x^2+x^3) + 5^2*(1+4^2*x+6^2*x^2+4^2*x^3+x^4) - (1+5^2*x+10^2*x^2+10^2*x^3+5^2*x^4+x^5))",
				"+ x^6*(1 - 6^2*(1+x) + 15^2*(1+2^2*x+x^2) - 20^2*(1+3^2*x+3^2*x^2+x^3) + 15^2*(1+4^2*x+6^2*x^2+4^2*x^3+x^4) - 6^2*(1+5^2*x+10^2*x^2+10^2*x^3+5^2*x^4+x^5) + (1+6^2*x+15^2*x^2+20^2*x^3+15^2*x^4+6^2*x^5+x^6)) + ...",
				"in which the coefficients of odd powers of x vanish.",
				"We can also express the g.f. by the binomial series identity:",
				"A(x) = 1/(1+x) + x/(1+x)^3*(1-x)^2 + x^2/(1+x)^5*(1 - 2^2*x + x^2)^2",
				"+ x^3/(1+x)^7*(1 - 3^2*x + 3^2*x^2 - x^3)^2",
				"+ x^4/(1+x)^9*(1 - 4^2*x + 6^2*x^2 - 4^2*x^3 + x^4)^2",
				"+ x^5/(1+x)^11*(1 - 5^2*x + 10^2*x^2 - 10^2*x^3 + 5^2*x^4 - x^5)^2",
				"+ x^6/(1+x)^13*(1 - 6^2*x + 15^2*x^2 - 20^2*x^3 + 15^2*x^4 - 6^2*x^5 + x^6)^2 + ..."
			],
			"maple": [
				"A245925 := n -\u003e (-1)^n*add(binomial(2*(n-k), n-k)*binomial(2*n-k, k)^2, k=0..n); seq(A245925(n), n=0..20); # _Peter Luschny_, Aug 17 2014"
			],
			"mathematica": [
				"Table[Sum[Sum[(-1)^(j+k) * Binomial[2*n - k, j + k]^2 * Binomial[j + k, k]^2, {j, 0, 2*n - 2*k}], {k, 0, n}], {n, 0, 20}] (* _Vaclav Kotesovec_, Aug 16 2014 after _Paul D. Hanna_ *)",
				"a[n_] := (-1)^n*HypergeometricPFQ[{-n, -n, n + 1, n + 1}, {1/2, 1, 1}, 1/4];",
				"Table[a[n], {n, 0, 20}] (* _Peter Luschny_, Mar 14 2018 *)"
			],
			"program": [
				"(PARI) /* By definition: */",
				"{a(n)=polcoeff(sum(m=0, n, x^m*sum(k=0, m, (-1)^k*binomial(m, k)^2*sum(j=0, k, binomial(k, j)^2*x^j)+x*O(x^n))), n)}",
				"for(n=0, 20, print1(a(2*n), \", \"))",
				"(PARI) /* From alternate g.f.: */",
				"{a(n)=local(A=1);A=sum(m=0,n,x^m/(1+x)^(2*m+1)*sum(k=0,m,binomial(m,k)^2*(-x)^k)^2+x*O(x^n));polcoeff(A,n)}",
				"for(n=0,20,print1(a(2*n),\", \"))",
				"(PARI) /* From formula for a(n); printing only nonzero terms: */",
				"{a(n)=sum(k=0, n\\2, sum(j=0, n-2*k, (-1)^(j+k)*binomial(n-k, j+k)^2*binomial(j+k, k)^2))}",
				"for(n=0, 20, print1(a(2*n), \", \"))",
				"(PARI) /* From formula for a(n) (nonzero terms): */",
				"{a(n)=sum(k=0, n, sum(j=0, 2*n-2*k, (-1)^(j+k)*binomial(2*n-k,j+k)^2*binomial(j+k, k)^2))}",
				"for(n=0, 20, print1(a(n), \", \"))",
				"(PARI) /* Formula for a(n), after _Peter Luschny_ and _Robert Israel_: */",
				"{a(n) = (-1)^n * sum(k=0,n, binomial(2*k, k) * binomial(n+k, n-k)^2)}",
				"for(n=0, 20, print1(a(n), \", \"))",
				"(PARI) /* Simpler formula for a(n): */",
				"{a(n) = sum(k=0, n, (-1)^k * binomial(2*k, k)^2 * binomial(n+k, n-k) )}",
				"for(n=0, 20, print1(a(n), \", \"))",
				"(PARI) /* Using AGM: */",
				"{a(n)=polcoeff( 1 / agm(1-x, sqrt(1+14*x+x^2 +x*O(x^n))), n)}",
				"for(n=0,20,print1(a(n),\", \"))",
				"(Sage)",
				"A245925 = lambda n: (-1)^n*sum(binomial(2*(n-k), n-k)*binomial(2*n-k, k)^2 for k in (0..n))",
				"[A245925(n) for n in range(21)] # _Peter Luschny_, Aug 17 2014"
			],
			"xref": [
				"Cf. A245926, A245927, A245929, A243945, A249891."
			],
			"keyword": "sign,easy",
			"offset": "0,2",
			"author": "_Paul D. Hanna_, Aug 15 2014",
			"references": 16,
			"revision": 56,
			"time": "2020-12-23T04:38:10-05:00",
			"created": "2014-08-15T18:22:27-04:00"
		}
	]
}