{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A128701",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 128701,
			"data": "1,3,10,18,20,42,84,90,108,168,300,336,504,540,600,630,660,1008,1200,1560,1620,1980,2100,2340,2400,3024,3120,3240,3780,3960,4200,4680,5880,6120,6240,7920,8400,8820",
			"name": "Highly abundant numbers that are not products of consecutive primes with nonincreasing exponents, i.e., that are not of the form n=2^{e_2} * 3^{e_3} * ...* p^{e_p}, with e_2\u003e=e_3\u003e=...\u003e=e_p.",
			"comment": [
				"This is the subsequence of those highly abundant numbers (A002093) that have a different canonical structure to the superabundant numbers (A004394), the colossally abundant numbers (A004490), the highly composite numbers (A002182) and the superior highly composite numbers (A002201)."
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A128701/b128701.txt\"\u003eTable of n, a(n) for n = 1..8404\u003c/a\u003e",
				"L. Alaoglu and P. Erdős, \u003ca href=\"http://www.renyi.hu/~p_erdos/1944-03.pdf\"\u003eOn highly composite and similar numbers\u003c/a\u003e, Trans. Amer. Math. Soc., 56 (1944), 448-469.",
				"Jeffrey C. Lagarias, \u003ca href=\"https://arxiv.org/abs/math/0008177\"\u003eAn Elementary Problem Equivalent to the Riemann Hypothesis\u003c/a\u003e, arXiv:math/0008177 [math.NT], 2000-2001.",
				"Jeffrey C. Lagarias, \u003ca href=\"http://www.jstor.org/stable/2695443\"\u003eAn elementary problem equivalent to the Riemann hypothesis\u003c/a\u003e, American Mathematical Monthly 109 (2002), pp. 534-543.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Highly_abundant_number\"\u003eHighly Abundant Numbers\u003c/a\u003e."
			],
			"formula": [
				"The highly abundant numbers (A002093) are those values of n for which sigma(n)\u003esigma(m) for all m\u003cn, where sigma(n)= A000203(n)."
			],
			"example": [
				"As 10 is the third highly abundant number that cannot be expressed as a product of consecutive primes with nonincreasing exponents, then a(3)=10."
			],
			"mathematica": [
				"hadata1=FoldList[Max,1,Table[DivisorSigma[1,n],{n,2,10000}]]; data1=Flatten[Position[hadata1,#,1,1]\u0026/@Union[hadata1]];primefactorlist[1]={1};primefactorlist[k_]:=First[Transpose[FactorInteger[k]]];exponentlist[1]={1};exponentlist[k_]:=Last[Transpose[FactorInteger[k]]];g[k_List]:=If[MemberQ[Table[k[[i]]\u003c= k[[i-1]],{i,1,Length[k]}],False],False,True];h[k_]:=If[primefactorlist[k]==(Prime[ # ]\u0026/@Range[Length[primefactorlist[k]]]),True,False];Select[data1,Or[ ! h[ # ],!g[exponentlist[ # ]]]\u0026]",
				"seq = {1}; sm = 0; Do[f = FactorInteger[n]; p = f[[;; , 1]]; e = f[[;; , 2]]; s = Times @@ ((p^(e + 1) - 1)/(p - 1)); If[s \u003e sm, sm = s; m = Length[p]; If[p[[-1]] != Prime[m] || (m \u003e 1 \u0026\u0026 ! AllTrue[Differences[e], # \u003c= 0 \u0026]), AppendTo[seq, n]]], {n, 2, 10^4}]; seq (* _Amiram Eldar_, Jun 18 2019 *)"
			],
			"xref": [
				"Cf. A002093, A004394, A000203, A004490, A002182, A002201, A128699, A128700, A128702."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Ant King_, Mar 28 2007",
			"references": 3,
			"revision": 21,
			"time": "2019-06-18T18:24:05-04:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}