{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A004007",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 4007,
			"id": "M5349",
			"data": "1,72,270,720,936,2160,2214,3600,4590,6552,5184,10800,9360,12240,13500,17712,14760,25920,19710,26064,28080,36000,25920,47520,37638,43272,45900,59040,46800,75600,51840,69264,73710,88560,62208,108000,85176",
			"name": "Theta series of E_6 lattice.",
			"comment": [
				"Cubic AGM theta functions: a(q) (see A004016), b(q) (A005928), c(q) (A005882)."
			],
			"reference": [
				"J. H. Conway and N. J. A. Sloane, \"Sphere Packings, Lattices and Groups\", Springer-Verlag, p. 123.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A004007/b004007.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"N. Heninger, E. M. Rains and N. J. A. Sloane, \u003ca href=\"http://arXiv.org/abs/math.NT/0509316\"\u003eOn the Integrality of n-th Roots of Generating Functions\u003c/a\u003e, J. Combinatorial Theory, Series A, 113 (2006), 1732-1745.",
				"G. Nebe and N. J. A. Sloane, \u003ca href=\"http://www.math.rwth-aachen.de/~Gabriele.Nebe/LATTICES/E6.html\"\u003eHome page for this lattice\u003c/a\u003e"
			],
			"formula": [
				"Expansion of eta(q)^9 / eta(q^3)^3 + 81*q * eta(q^3)^9 / eta(q)^3 in powers of q.",
				"Expansion of a(q)^3 + 2*c(q)^3 in powers of q where a(), c() are cubic AGM theta functions. - _Michael Somos_, Oct 24 2006"
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ q]^9 / QPochhammer[ q^3]^3 + 81 q QPochhammer[ q^3]^9 / QPochhammer[ q]^3, {q, 0, n}]; (* _Michael Somos_, Feb 19 2015 *)",
				"terms = 37; f[q_] = LatticeData[\"E6\", \"ThetaSeriesFunction\"][-I Log[q]/Pi]; s = Series[f[q], {q, 0, 2 terms}] // Normal // Simplify[#, q \u003e 0]\u0026; (List @@ s)[[1 ;; terms]] /. q -\u003e 1 (* _Jean-François Alcover_, Jul 04 2017 *)"
			],
			"program": [
				"(PARI) {a(n) = local(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A)^9 / eta(x^3 + A)^3 + 81 * x * eta(x^3 + A)^9 / eta(x + A)^3, n))}; /* _Michael Somos_, Oct 24 2006 */"
			],
			"xref": [
				"Cf. A005129 (dual lattice)."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 4,
			"revision": 29,
			"time": "2018-01-11T01:09:04-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}