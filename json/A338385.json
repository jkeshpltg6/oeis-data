{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A338385",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 338385,
			"data": "18,27,24,32,56,64,192,224,400,500,360,432,540,648,972,2187,1875,3125,1458,1701,1296,1350,2160,2400,5120,5632,2880,3024,3840,4608,4032,4704,3780,5184,10240,16384,8448,9216,20000,25000,15680,16464,15876,25515,20412,23814",
			"name": "Table read by rows, in which the n-th row lists the primitive solutions (k, q), k\u003cq, such that k*tau(k) = q*tau(q) = A338384(n).",
			"comment": [
				"As the multiplicativity of tau(k) ensures an infinity of solutions to the general equation k*tau(k) = q*tau(q) (see A338382), Richard K. Guy asked if there is an infinity of primitive solutions. A solution (k, q) with m = k*tau(k) = q*tau(q) is  primitive in the sense that (k', q') is not a solution for any k' = k/d, q' = q/d, m' = m/(d*tau(d)), d\u003e1 with m' = k' * tau(k') = q' * tau(q').",
				"Warning, Richard K. Guy asked if \"there is an infinity of primitive solutions (for k*tau(k) = q*tau(q)), in the sense that (k', q') is not a solution for any k' = k/d, q' = q/d, d\u003e1\". It appears that this definition is not enough well defined, because some solutions as (4032, 4704), (20000, 25000), (20412, 23814),... that are primitive are not obtained in this case (see detailed example (20000, 25000) below). The mathematical explanation is that tau satisfies the relation tau(r*s) = tau(r) * tau(s) * (t/tau(t)) where t = gcd(r,s)."
			],
			"reference": [
				"Richard K. Guy, Unsolved Problems in Number Theory, 3rd Edition, Springer, 2004, Section B12, p. 102-103.",
				"D. Wells, The Penguin Dictionary of Curious and Interesting Numbers, Revised Edition, Penguin Books, London, England, 1997, entry 168, page 127."
			],
			"example": [
				"The table begins:",
				"    18,  27;",
				"    24,  32;",
				"    56,  64;",
				"   192, 224;",
				"   400, 500;",
				"   360, 432;",
				"   ...",
				"1st row is (18, 27) because 18 * tau(18) = 27 * tau(27) = 108 = A338384(1).",
				"4th row is (192, 224) because 192 * tau(192) = 224 * tau(24) = 2688 = A338384(4); Note that 168 * tau(168) = 192 * tau(192) = 224 * tau(24) = 2688 = A338382(8) but (168, 192) and (168, 224) are not primitive solutions (see detailed example in A338384).",
				"5th row is (400, 500) because 400 * tau(400) = 500 * tau(500) = 6000.",
				"20th row is (20000, 25000) although (20000/50, 25000/50) = (400, 500) and that (400, 500) is the 5th row. Explanation: A338384(20) = 600000 = 20000*tau(20000) = 25000*tau(25000) and this pair is primitive, because for d = 50, we get 600000/(50*tau(50)) = 2000 \u003c\u003e  (20000/50)*tau(20000/50) = (25000/50)*tau(25000/50) = 6000. To be exhaustive, the two other pairs linked with 600000: (15000, 20000) and (15000, 25000) are not primitive."
			],
			"xref": [
				"Cf. A000005, A038040, A338381, A338382, A338383, A338384.",
				"Cf. A337876 (similar for k*sigma(k))."
			],
			"keyword": "nonn,tabf",
			"offset": "1,1",
			"author": "_Bernard Schott_, Nov 09 2020",
			"references": 3,
			"revision": 45,
			"time": "2020-12-06T08:36:32-05:00",
			"created": "2020-12-06T08:36:32-05:00"
		}
	]
}