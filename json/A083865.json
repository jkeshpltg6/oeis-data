{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A083865",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 83865,
			"data": "6,28,34,120,126,148,154,496,502,524,530,616,622,644,650,672,678,700,706,792,798,820,826,1168,1174,1196,1202,1288,1294,1316,1322,8128,8134,8156,8162,8248,8254,8276,8282,8624,8630,8652,8658,8744,8750,8772,8778",
			"name": "Sums of (one or more distinct) k-perfect numbers.",
			"comment": [
				"Each k-perfect number (A007691\\{1}) appears once, and may also appear at most once in each sum of k-perfect numbers to create other terms in the sequence. [_Harvey P. Dale_, Feb 07 2012]"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A083865/b083865.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"Empirical observation: a(n) = 2*n + Sum_{k \u003e= 1} 4^k*floor(2*n/2^k) for 1 \u003c= n \u003c= 15 and 32 \u003c= n \u003c= 47; a(n) = 2*n - 1344 + Sum_{k \u003e= 1} 4^k*floor(2*n/2^k) for 16 \u003c= n \u003c= 31. Note 1344 = 4^3 + 4^4 + 4^5. Cf. A000695. - _Peter Bala_, Nov 29 2016",
				"If b(n) = 2*n + Sum_{k \u003e= 1} 4^k*floor(2*n/2^k) - a(n), we also have b(n) = 1344 for 48 \u003c= n \u003c= 63, then 2400 for 64 \u003c= n \u003c= 79, 3744 for 80 \u003c= n \u003c= 95, 8008 for 96 \u003c= n \u003c= 111, etc.  The first case where b(n) is not constant on an interval 16*k \u003c= n \u003c= 16*k+15 is k=57214, where b(915431)=2747770287196 but b(915432)=2747770287312. - _Robert Israel_, Nov 29 2016"
			],
			"example": [
				"a(3) = 34 because it is the sum of 6 + 28 both of which are perfect numbers."
			],
			"maple": [
				"N:= 10000: # to get all terms \u003c= N",
				"Kperf:= select(t -\u003e numtheory:-sigma(t) mod t = 0, [$2..N]):",
				"S:= {0}:",
				"for k in Kperf do S:= S union (k +~ S) od:",
				"sort(convert(S minus {0}, list)); # _Robert Israel_, Nov 29 2016"
			],
			"mathematica": [
				"With[{perf=Select[Range[10000],DivisorSigma[1,#]==2#\u0026]},Rest[Union[Total/@ Subsets[perf]]]] (* _Harvey P. Dale_, Feb 07 2012 *)"
			],
			"program": [
				"(PARI) a=[];n=1;until(50\u003c#a=concat(a,vector(#a+1,i,n+if(i\u003e1,a[i-1]))),while(sigma(n++)%n,));a  \\\\ _M. F. Hasler_, Feb 09 2012"
			],
			"xref": [
				"Cf. A065997, A000695."
			],
			"keyword": "easy,nonn",
			"offset": "1,1",
			"author": "Torsten Klar (klar(AT)radbruch.jura.uni-mainz.de), Jun 18 2003",
			"ext": [
				"Corrected by _M. F. Hasler_ and others, Feb 07 2012"
			],
			"references": 3,
			"revision": 26,
			"time": "2021-01-25T10:27:15-05:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}