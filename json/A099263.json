{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A099263",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 99263,
			"data": "1,2,5,15,52,203,877,4140,21146,115929,677359,4189550,27243100,184941915,1301576801,9433737120,69998462014,529007272061,4054799902003,31415584940850,245382167055488,1928337630016767,15222915798289765",
			"name": "a(n) = (1/40320)*8^n + (1/1440)*6^n + (1/360)*5^n + (1/64)*4^n + (11/180)*3^n + (53/288)*2^n + 103/280. Partial sum of Stirling numbers of second kind S(n,i), i=1..8 (i.e., a(n) = Sum_{i=1..8} S(n,i)).",
			"comment": [
				"Density of regular language L over {1,2,3,4,5,6,7,8} (i.e., number of strings of length n in L) described by a regular expression with c = 8: Sum_{i=1..c} (Product_{j=1..i} (j(1+...+j)*), where \"Sum\" stands for union and \"Product\" for concatenation."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A099263/b099263.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Joerg Arndt and N. J. A. Sloane, \u003ca href=\"/A278984/a278984.txt\"\u003eCounting Words that are in \"Standard Order\"\u003c/a\u003e",
				"N. Moreira and R. Reis, \u003ca href=\"http://www.dcc.fc.up.pt/~nam/publica/dcc-2004-07.pdf\"\u003eOn the density of languages representing finite set partitions\u003c/a\u003e, Technical Report DCC-2004-07, August 2004, DCC-FC\u0026 LIACC, Universidade do Porto.",
				"N. Moreira and R. Reis, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL8/Moreira/moreira8.html\"\u003eOn the Density of Languages Representing Finite Set Partitions\u003c/a\u003e, Journal of Integer Sequences, Vol. 8 (2005), Article 05.2.8.",
				"\u003ca href=\"/index/Rec#order_07\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (29,-343,2135,-7504,14756,-14832,5760)."
			],
			"formula": [
				"For c = 8, a(n) = c^n/c! + Sum_{k=1..c-2} k^n/k! * Sum_{j=2..c-k} (-1)^j/j!, or = Sum_{k=1..c} g(k, c)*k^n, where g(1, 1) = 1, g(1, c) = g(1, c-1) + (-1)^(c-1)/(c-1)! for c \u003e 1, and g(k, c) = g(k-1, c-1)/k for c \u003e 1 and 2 \u003c= k \u003c= c.",
				"G.f.: -x*(3641*x^6 - 6583*x^5 + 4566*x^4 - 1579*x^3 + 290*x^2 - 27*x + 1) / ((x-1)*(2*x-1)*(3*x-1)*(4*x-1)*(5*x-1)*(6*x-1)*(8*x-1)). [_Colin Barker_, Dec 05 2012]",
				"a(n) = Sum_{k=0..8} Stirling2(n,k).",
				"G.f.: Sum_{j=0..k} A248925(k,j)*x^j / Product_{j=1..k} (1 - j*x) with k = 8. - _Robert A. Russell_, Apr 25 2018"
			],
			"mathematica": [
				"CoefficientList[Series[-(3641 x^6 - 6583 x^5 + 4566 x^4 - 1579 x^3 + 290 x^2 - 27 x + 1) / ((x - 1) (2 x - 1) (3 x - 1) (4 x - 1) (5 x - 1) (6 x - 1) (8 x - 1)), {x, 0, 30}], x] (* _Vincenzo Librandi_, Jul 27 2017 *)",
				"Table[Sum[StirlingS2[n, k], {k, 0, 8}], {n, 1, 30}] (* _Robert A. Russell_, Apr 25 2018 *)",
				"LinearRecurrence[{29,-343,2135,-7504,14756,-14832,5760},{1,2,5,15,52,203,877},30] (* _Harvey P. Dale_, Aug 27 2019 *)"
			],
			"program": [
				"(MAGMA) [(1/40320)*8^n+(1/1440)*6^n+(1/360)*5^n+(1/64)*4^n +(11/180)*3^n+(53/288)*2^n+103/280: n in [1..30]]; // _Vincenzo Librandi_, Jul 27 2017",
				"(PARI) a(n) = (1/40320)*8^n + (1/1440)*6^n + (1/360)*5^n + (1/64)*4^n + (11/180)*3^n + (53/288)*2^n + 103/280; \\\\ _Altug Alkan_, Apr 25 2018"
			],
			"xref": [
				"Cf. A007051, A007581, A056272, A056273, A099262.",
				"A row of the array in A278984.",
				"Cf. A008277 (Stirling2), A248925."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Nelma Moreira_, Oct 10 2004",
			"references": 8,
			"revision": 53,
			"time": "2021-03-09T10:51:21-05:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}