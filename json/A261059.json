{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A261059",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 261059,
			"data": "1,0,2,1,4,25,47,237,562,1965,7960,24148,85579,307569,1104519,4106381,14710760,52113647,193181449,698356631,2574590311,9600573372,35644252223,131545038705,492346772797,1843993274342,6903884199622,25984680496124,97937400336407",
			"name": "Number of solutions to c(1)*prime(2)+...+c(2n)*prime(2n+1) = -2, where c(i) = +-1 for i \u003e 1, c(1) = 1.",
			"comment": [
				"There cannot be a solution for an odd number of terms on the l.h.s. because all terms are odd but the r.h.s. is even."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A261059/b261059.txt\"\u003eTable of n, a(n) for n = 1..300\u003c/a\u003e"
			],
			"example": [
				"a(1) = 1 because prime(2) - prime(3) = -2.",
				"a(2) = 0 because prime(2) +- prime(3) +- prime(4) +- prime(5) is different from -2 for any choice of the signs.",
				"a(3) = 2 counts the 2 solutions prime(2) - prime(3) + prime(4) - prime(5) - prime(6) + prime(7) = -2 and prime(2) - prime(3) - prime(4) + prime(5) + prime(6) - prime(7) = -2."
			],
			"maple": [
				"s:= proc(n) option remember;",
				"      `if`(n\u003c3, 0, ithprime(n)+s(n-1))",
				"    end:",
				"b:= proc(n, i) option remember; `if`(n\u003es(i), 0, `if`(i=2, 1,",
				"      b(abs(n-ithprime(i)),i-1)+b(n+ithprime(i),i-1)))",
				"    end:",
				"a:= n-\u003e b(5, 2*n+1):",
				"seq(a(n), n=1..30);  # _Alois P. Heinz_, Aug 08 2015"
			],
			"mathematica": [
				"s[n_] := s[n] = If[n\u003c3, 0, Prime[n]+s[n-1]]; b[n_, i_] := b[n, i] = If[n \u003e s[i], 0, If[i == 2, 1, b[Abs[n-Prime[i]], i-1] + b[n+Prime[i], i-1]]];  a[n_] := b[5, 2*n+1]; Table[a[n], {n, 1, 30}] (* _Jean-François Alcover_, Nov 11 2015, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(PARI) A261059(n,rhs=-2,firstprime=2)={rhs-=prime(firstprime);my(p=vector(2*n-2+bittest(rhs,0),i,prime(i+firstprime)));sum(i=1,2^#p-1,sum(j=1,#p,(-1)^bittest(i,j-1)*p[j])==rhs)} \\\\ For illustrative purpose; too slow for n \u003e\u003e 10.",
				"(PARI) a(n,s=-2-3,p=2)=if(n\u003c=s,if(s==p,n==s,a(abs(n-p),s-p,precprime(p-1))+a(n+p,s-p,precprime(p-1))),if(s\u003c=0,a(abs(s),sum(i=p+1,p+2*n-1,prime(i)),prime(p+n*2-1))))"
			],
			"xref": [
				"Cf. A261057 (starting with prime(1)), A261060 (starting with prime(3)), A261045 (starting with prime(4)), A261061 - A261063 and A261044 (r.h.s. = -1), A022894 - A022904, A083309, A022920 (r.h.s. = 0, 1 or 2), ."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_M. F. Hasler_, Aug 08 2015",
			"ext": [
				"a(15)-a(29) from _Alois P. Heinz_, Aug 08 2015"
			],
			"references": 18,
			"revision": 15,
			"time": "2015-11-11T05:13:53-05:00",
			"created": "2015-08-08T17:49:53-04:00"
		}
	]
}