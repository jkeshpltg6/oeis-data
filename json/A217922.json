{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A217922",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 217922,
			"data": "1,1,2,1,6,7,3,24,46,40,15,120,326,430,315,105,720,2556,4536,4900,3150,945,5040,22212,49644,70588,66150,38115,10395,40320,212976,574848,1011500,1235080,1032570,540540,135135",
			"name": "Triangle read by rows: labeled trees counted by improper edges.",
			"comment": [
				"T(n,k) is the number of labeled trees on [n], rooted at 1, with k improper edges, for n \u003e= 1, k \u003e= 0. See Zeng link for definition of improper edge."
			],
			"link": [
				"J. Fernando Barbero G., Jesús Salas, Eduardo J. S. Villaseñor, \u003ca href=\"http://arxiv.org/abs/1307.2010\"\u003eBivariate Generating Functions for a Class of Linear Recurrences. I. General Structure\u003c/a\u003e, arXiv:1307.2010 [math.CO], 2013-2014.",
				"Dominique Dumont, Armand Ramamonjisoa, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v3i2r17\"\u003eGrammaire de Ramanujan et Arbres de Cayley\u003c/a\u003e, Electr. J. Combinatorics, Volume 3, Issue 2 (1996) R17 (see page 17).",
				"M. Josuat-Vergès, \u003ca href=\"http://arxiv.org/abs/1310.7531\"\u003eDerivatives of the tree function\u003c/a\u003e, arXiv preprint arXiv:1310.7531 [math.CO], 2013.",
				"Lucas Randazzo, \u003ca href=\"https://arxiv.org/abs/1905.02083\"\u003eArboretum for a generalization of Ramanujan polynomials\u003c/a\u003e, arXiv:1905.02083 [math.CO], 2019.",
				"Jiang Zeng, \u003ca href=\"http://math.univ-lyon1.fr/homes-www/zeng/public_html/paper/publication.html\"\u003eA Ramanujan sequence that refines the Cayley formula for trees\u003c/a\u003e, Ramanujan Journal 3 (1999) 1, 45-54, \u003ca href=\"http://dx.doi.org/10.1023/A:1009809224933\"\u003e[DOI]\u003c/a\u003e"
			],
			"example": [
				"Table begins",
				"\\ k  0....1....2....3   ...",
				"n",
				"1 |..1",
				"2 |..1",
				"3 |..2....1",
				"4 |..6....7....3",
				"5 |.24...46...40....15",
				"6 |120..326..430...315...105",
				"T(4,2) = 3 because we have 1-\u003e3-\u003e4-\u003e2, 1-\u003e4-\u003e2-\u003e3, 1-\u003e4-\u003e3-\u003e2, in each of which the last 2 edges are improper."
			],
			"mathematica": [
				"T[n_,0]:= (n-1)!;T[n_, k_]:= If[k\u003c0 || k\u003en-2, 0, (n-1)T[n-1, k] +(n+k-3)T[n-1, k-1]];",
				"Join[{1}, Table[T[n,k], {n, 12}, {k, 0, n-2}]//Flatten] (* modified by _G. C. Greubel_, May 07 2019 *)"
			],
			"program": [
				"(Sage)",
				"def T(n, k):",
				"    if k==0: return factorial(n-1)",
				"    elif (k\u003c0 or k \u003e n-2): return 0",
				"    else: return (n-1)*T(n-1, k) + (n+k-3)* T(n-1, k-1)",
				"[1] + [[T(n, k) for k in (0..n-2)] for n in (2..12)] # _G. C. Greubel_, May 07 2019"
			],
			"xref": [
				"Cf. A054589, A075856. Row sums are n^(n-2), A000272."
			],
			"keyword": "nonn,tabf",
			"offset": "1,3",
			"author": "_David Callan_, Oct 14 2012",
			"references": 0,
			"revision": 28,
			"time": "2019-05-07T10:59:34-04:00",
			"created": "2012-10-25T18:52:44-04:00"
		}
	]
}