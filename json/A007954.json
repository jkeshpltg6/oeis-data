{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007954",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7954,
			"data": "0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,2,4,6,8,10,12,14,16,18,0,3,6,9,12,15,18,21,24,27,0,4,8,12,16,20,24,28,32,36,0,5,10,15,20,25,30,35,40,45,0,6,12,18,24,30,36,42,48,54,0,7,14,21,28,35,42,49,56,63,0,8,16,24,32,40,48,56,64,72,0,9,18,27,36,45,54,63,72,81,0,0,0,0,0,0,0,0",
			"name": "Product of decimal digits of n.",
			"comment": [
				"Moebius transform of A093811(n). a(n) = A093811(n) * A008683(n), where operation * denotes Dirichlet convolution, namely b(n) * c(n) = Sum_{d|n} b(d) * c(n/d). Simultaneously holds Dirichlet multiplication: a(n) * A000012(n) = A093811(n). - _Jaroslav Krizek_, Mar 22 2009",
				"Apart from the 0's, all terms are in A002473. Further, for all m in A002473 there is some n such that a(n) = m, see A096867. - _Charles R Greathouse IV_, Sep 29 2013",
				"a(n) = 0 asymptotically almost surely, namely for all n except for the set of numbers without digit '0'; this set is of density zero, since it is less and less probable to have no '0' as the number of digits of n grows. (See also A054054.) - _M. F. Hasler_, Oct 11 2015"
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/A007954/b007954.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Rigoberto Flórez, Robinson A. Higuita and Antara Mukherjee, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Mukherjee/mukh2.html\"\u003eAlternating Sums in the Hosoya Polynomial Triangle\u003c/a\u003e, Article 14.9.5 Journal of Integer Sequences, Vol. 17 (2014).",
				"Florentin Smarandache, \u003ca href=\"http://www.gallup.unm.edu/~smarandache/OPNS.pdf\"\u003eOnly Problems, Not Solutions!\u003c/a\u003e.",
				"\u003ca href=\"/index/Coi#Colombian\"\u003eIndex entries for Colombian or self numbers and related sequences\u003c/a\u003e"
			],
			"formula": [
				"A000035(a(A014261(n))) = 1. - _Reinhard Zumkeller_, Nov 30 2007",
				"a(n) = abs(A187844(n)). - _Reinhard Zumkeller_, Mar 14 2011",
				"a(n) \u003e 0 if and only if A054054(n) \u003e 0. a(n) = d in {1, ..., 9} if n = (10^k - 1)/9 + (d - 1)*10^m = A002275(k) + (d - 1)*A011557(m) for some k \u003e m \u003e= 0. The statement holds with \"if and only if\" for d in {1, 2, 3, 5, 7}. For d = 4, 6, 8 or 9, one has a(n) = d if n = (10^k - 1)/9 + (a - 1)*10^m + (b - 1)*10^p with integers k \u003e m \u003e p \u003e= 0 and a, b \u003e 0 such that d = a*b. - _M. F. Hasler_, Oct 11 2015",
				"From _Robert Israel_, May 17 2016: (Start)",
				"G.f.: Sum_{n \u003e= 0} Product_{j = 0..n} Sum_{k = 1..9} k*x^(k*10^j).",
				"G.f. satisfies A(x) = (x + 2*x^2 + ... + 9*x^9)*(1 + A(x^10)). (End)"
			],
			"maple": [
				"A007954 := proc(n::integer)",
				"    if n = 0 then",
				"        0;",
				"    else",
				"        mul( d,d=convert(n,base,10)) ;",
				"    end if;",
				"end proc: # _R. J. Mathar_, Oct 02 2019"
			],
			"mathematica": [
				"Array[Times @@ IntegerDigits@ # \u0026, 108, 0] (* _Robert G. Wilson v_, Mar 15 2011 *)"
			],
			"program": [
				"(PARI) A007954(n)= { local(resul = n % 10); n \\= 10; while( n \u003e 0, resul *= n %10; n \\= 10; ); return(resul); } \\\\ _R. J. Mathar_, May 23 2006, edited by _M. F. Hasler_, Apr 23 2015",
				"(PARI) A007954(n)=prod(i=1,#n=Vecsmall(Str(n)),n[i]-48) \\\\ (...eval(Vec(...)),n[i]) is about 50% slower; (...digits(n)...) about 6% slower. \\\\ _M. F. Hasler_, Dec 06 2009",
				"(PARI) a(n)=if(n,factorback(digits(n)),0) \\\\ _Charles R Greathouse IV_, Apr 14 2020",
				"(Haskell)",
				"a007954 n | n \u003c 10 = n",
				"          | otherwise = m * a007954 n' where (n', m) = divMod n 10",
				"-- _Reinhard Zumkeller_, Oct 26 2012, Mar 14 2011",
				"(MAGMA) [0] cat [\u0026*Intseq(n): n in [1..110]]; // _Vincenzo Librandi_, Jan 03 2020",
				"(Scala) (0 to 99).map(_.toString.toCharArray.map(_ - 48).scanRight(1)(_ * _).head) // _Alonso del Arte_, Apr 14 2020",
				"(Python)",
				"from math import prod",
				"def a(n): return prod(map(int, str(n)))",
				"print([a(n) for n in range(108)]) # _Michael S. Branicky_, Jan 16 2022"
			],
			"xref": [
				"Cf. A031347 (different from A035930), A007953, A007602, A010888, A093811, A008683, A000012, A061076 (partial sums), A230099.",
				"Cf. A051802 (ignoring zeros)."
			],
			"keyword": "nonn,base,easy,nice,hear,changed",
			"offset": "0,3",
			"author": "R. Muller",
			"ext": [
				"Error in term 25 corrected, Nov 15 1995"
			],
			"references": 263,
			"revision": 107,
			"time": "2022-01-17T02:52:58-05:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}