{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000921",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 921,
			"id": "M4398 N1854",
			"data": "7,31,43,67,73,79,103,127,163,181,223,229,271,277,307,313,337,349,409,421,439,457,463,499,523,577,643,661,673,691,709,727,757,769,811,823,829,853,877,919,967,991,997,1021,1069,1087,1093,1117,1123,1171,1213",
			"name": "Primes p of the form 3k+1 such that Sum_{x=1..p} cos(2*Pi*x^3/p) \u003e  sqrt(p).",
			"comment": [
				"For the first 1000 terms in this sequence (primes up to 44683), the minimum difference between sqrt(p) and the sum is 1.47633.... Hence there does not seem to be a need to compute the sum to high precision. - _T. D. Noe_, Jun 20 2012"
			],
			"reference": [
				"H. Hasse, Vorlesungen über Zahlentheorie. Springer-Verlag, NY, 1964, p. 482.",
				"G. B. Mathews, Theory of Numbers, 2nd edition. Chelsea, NY, p. 228.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A000921/b000921.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"D. R. Heath-Brown, \u003ca href=\"http://eprints.maths.ox.ac.uk/158/01/kummer.pdf\"\u003eKummer's Conjecture for Cubic Gauss Sums\u003c/a\u003e",
				"J. von Neumann and H. H. Goldstine, \u003ca href=\"https://doi.org/10.1090/S0025-5718-1953-0055784-0\"\u003eA numerical study of a conjecture of Kummer\u003c/a\u003e, Math. Comp., 7 (1953), 133-134.",
				"J. von Neumann and H. H. Goldstine, \u003ca href=\"/A000921/a000921.pdf\"\u003e A numerical study of a conjecture of Kummer\u003c/a\u003e, Math. Comp., 7 (1953), 133-134. [Annotated scanned copy]"
			],
			"example": [
				"7 is here because the sum of cos(2*Pi*x^3/7) = 4.7409 \u003e sqrt(7)."
			],
			"program": [
				"(PARI) isok(p) = isprime(p) \u0026\u0026 ((p % 3) == 1) \u0026\u0026 (sum(x=1, p, cos(2*Pi*x^3/p)) \u003e sqrt(p)); \\\\ _Michel Marcus_, Oct 16 2017"
			],
			"xref": [
				"Cf. A000922, A000923, A002476."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Edited by _Don Reble_, May 26 2007"
			],
			"references": 3,
			"revision": 30,
			"time": "2017-10-19T03:13:42-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}