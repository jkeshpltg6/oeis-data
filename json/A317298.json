{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A317298",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 317298,
			"data": "1,3,11,21,37,55,79,105,137,171,211,253,301,351,407,465,529,595,667,741,821,903,991,1081,1177,1275,1379,1485,1597,1711,1831,1953,2081,2211,2347,2485,2629,2775,2927,3081,3241,3403,3571,3741,3917,4095,4279,4465,4657",
			"name": "a(n) = (1/2)*(1 + (-1)^n + 2*n + 4*n^2).",
			"comment": [
				"For n \u003e 0, first differences of A304487.",
				"All the terms of this sequence are odd numbers."
			],
			"link": [
				"Stefano Spezia, \u003ca href=\"/A317298/b317298.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,0,-2,1)."
			],
			"formula": [
				"a(n) = (1/2)*(A033999(n) + A005408(n) + 4*A000290(n)).",
				"a(n) = 2*a(n-1) - 2*a(n-3) + a(n-4) for n \u003e 3.",
				"a(2*n) = A188135(n).",
				"a(2*n-1) = A033567(n), for n \u003e 0.",
				"O.g.f.: -(1 + x + 5*x^2 + x^3)/(-1 + x)^3*(1 + x).",
				"E.g.f.: (1/2)*exp(-x)*(1 + exp(2*x)*(1 + 6*x + 4*x^2)).",
				"Sum_{n\u003e0} 1/a(n) = (1/4)*(Pi - log(4)) + i*(polygamma(0, 1/8 - i*sqrt(7)/8) - polygamma(0, 1/8 + i*sqrt(7)/8))/(2*sqrt(7)) = 1.603596691017309384564895..., where i is the imaginary unit. - _Stefano Spezia_, Feb 10 2019",
				"a(n) = 1 + 2*(n^2 + floor(n/2)). - _Stefano Spezia_, Dec 08 2021"
			],
			"maple": [
				"a:=n-\u003e(1/2)*(1 + (-1)^n + 2*n + 4*n^2): seq(a(n), n=0..50);"
			],
			"mathematica": [
				"a[n_]:=(1/2)*(1 + (-1)^n + 2*n + 4*n^2); Array[a, 50, 0]"
			],
			"program": [
				"(GAP) Flat(List([0..50], n-\u003e(1/2)*(1 + (-1)^n + 2*n + 4*n^2)));",
				"(MAGMA) [(1/2)*(1+(-1)^n+2*n+4*n^2): n in [0..50]];",
				"(Maxima) makelist((1/2)*(1+(-1)^n+2*n+4*n^2), n, 0, 50);",
				"(PARI) a(n) = (1/2)*(1+(-1)^n+2*n+4*n^2);",
				"(Python) [(1+(-1)**n+2*n+4*n**2)/2 for n in range(0,50)]"
			],
			"xref": [
				"Cf. A000290, A004526, A005408, A033567, A033999, A188135, A304487.",
				"Cf. A306362 (prime numbers subsequence)."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Stefano Spezia_, Jan 22 2019",
			"references": 3,
			"revision": 42,
			"time": "2021-12-09T04:45:49-05:00",
			"created": "2019-01-23T09:14:57-05:00"
		}
	]
}