{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A009195",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 9195,
			"data": "1,1,1,2,1,2,1,4,3,2,1,4,1,2,1,8,1,6,1,4,3,2,1,8,5,2,9,4,1,2,1,16,1,2,1,12,1,2,3,8,1,6,1,4,3,2,1,16,7,10,1,4,1,18,5,8,3,2,1,4,1,2,9,32,1,2,1,4,1,2,1,24,1,2,5,4,1,6,1,16,27,2,1,12,1,2,1,8,1,6,1,4,3,2,1,32,1,14,3,20",
			"name": "a(n) = gcd(n, phi(n)).",
			"comment": [
				"The inequality gcd(n, phi(n)) \u003c= 2n exp(-sqrt(log 2 log n)) holds for all squarefree n \u003e= 1 (Erdős, Luca, and Pomerance).",
				"Erdős shows that for almost all n, a(n) ~ log log log log n. - _Charles R Greathouse IV_, Nov 23 2011"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A009195/b009195.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Paul Erdős, \u003ca href=\"http://www.renyi.hu/~p_erdos/1948-11.pdf\"\u003eSome asymptotic formulas in number theory\u003c/a\u003e, J. Indian Math. Soc. (N.S.) 12 (1948), pp. 75-78.",
				"Paul Erdős, Florian Luca, Carl Pomerance, \u003ca href=\"http://www.math.dartmouth.edu/~carlp/PDF/gcdnphin14.pdf\"\u003eOn the proportion of numbers coprime to a given integer\u003c/a\u003e, in Anatomy of Integers, pp. 47-64, J.-M. De Koninck, A. Granville, F. Luca (editors), AMS, 2008."
			],
			"formula": [
				"a(n) = gcd(n, A051953(n)). - _Labos Elemer_",
				"a(n) = n / A109395(n). - _Antti Karttunen_, May 04 2017 (corrected also typo in above formula)."
			],
			"maple": [
				"a009195 := n -\u003e igcd(i,numtheory[phi](n));"
			],
			"mathematica": [
				"Table[GCD[n,EulerPhi[n]],{n,100}] (* _Harvey P. Dale_, Aug 11 2011 *)"
			],
			"program": [
				"(PARI) a(n)=gcd(n,eulerphi(n)) \\\\ _Charles R Greathouse IV_, Nov 23 2011",
				"(Haskell)",
				"a009195 n = n `gcd` a000010 n  -- _Reinhard Zumkeller_, Feb 27 2012",
				"(Python)",
				"def a009195(n):",
				"    from math import gcd",
				"    phi = lambda x: len([i for i in range(x) if gcd(x,i) == 1])",
				"    return gcd(n, phi(n))",
				"# _Edward Minnix III_, Dec 05 2015",
				"(MAGMA) [Gcd(n, EulerPhi(n)): n in [1..100]]; // _Vincenzo Librandi_, Dec 17 2015"
			],
			"xref": [
				"Cf. A000010, A003277, A050399, A051953, A061303, A109395, A285711."
			],
			"keyword": "nonn,easy,nice",
			"offset": "1,4",
			"author": "_David W. Wilson_",
			"references": 55,
			"revision": 45,
			"time": "2017-05-04T17:45:55-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}