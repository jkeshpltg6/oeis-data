{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A290732",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 290732,
			"data": "1,2,3,4,3,6,4,8,9,6,6,12,7,8,9,16,9,18,10,12,12,12,12,24,11,14,27,16,15,18,16,32,18,18,12,36,19,20,21,24,21,24,22,24,27,24,24,48,22,22,27,28,27,54,18,32,30,30,30,36",
			"name": "Number of distinct values of X*(3*X-1)/2 mod n.",
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A290732/b290732.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Andreas Enge, William Hart, Fredrik Johansson, \u003ca href=\"http://arxiv.org/abs/1608.06810\"\u003eShort addition sequences for theta functions\u003c/a\u003e, arXiv:1608.06810 [math.NT], (24-August-2016). See Table 6."
			],
			"formula": [
				"a(3^n) = 3^n. - _Hugo Pfoertner_, Aug 25 2018",
				"a(n) = A317623(n) * A040001(n). - _Andrew Howroyd_, Oct 27 2018",
				"Multiplicative with a(2^e) = 2^e, a(3^e) = 3^e, a(p^e) = 1 + floor( p^(e+1)/(2*p+2) ) for prime p \u003e= 5. - _Andrew Howroyd_, Nov 03 2018"
			],
			"example": [
				"The values taken by (3*X^2-X)/2 mod n for small n are:",
				"   1, [0]",
				"   2, [0, 1]",
				"   3, [0, 1, 2]",
				"   4, [0, 1, 2, 3]",
				"   5, [0, 1, 2]",
				"   6, [0, 1, 2, 3, 4, 5]",
				"   7, [0, 1, 2, 5]",
				"   8, [0, 1, 2, 3, 4, 5, 6, 7]",
				"   9, [0, 1, 2, 3, 4, 5, 6, 7, 8]",
				"  10, [0, 1, 2, 5, 6, 7]",
				"  11, [0, 1, 2, 4, 5, 7]",
				"  12, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]",
				"  ..."
			],
			"maple": [
				"a:=[]; M:=80;",
				"for n from 1 to M do",
				"q1:={};",
				"for i from 0 to 2*n-1 do q1:={op(q1), i*(3*i-1)/2 mod n}; od;",
				"s1:=sort(convert(q1,list));",
				"a:=[op(a),nops(s1)];",
				"od:",
				"a;"
			],
			"mathematica": [
				"a[n_] := Table[PolynomialMod[X(3X-1)/2, n], {X, 0, 2*n-1}]// Union // Length;",
				"Array[a, 60] (* _Jean-François Alcover_, Sep 01 2018 *)"
			],
			"program": [
				"(PARI) a(n)={my(v=vector(n)); for(i=0, 2*n-1, v[i*(3*i-1)/2%n + 1]=1); vecsum(v)} \\\\ _Andrew Howroyd_, Oct 27 2018",
				"(PARI) a(n)={my(f=factor(n)); prod(i=1, #f~, my([p,e]=f[i,]); if(p\u003c=3, p^e, 1 + p^(e+1)\\(2*p+2)))} \\\\ _Andrew Howroyd_, Nov 03 2018"
			],
			"xref": [
				"Cf. A000224 (analog for X^2), A014113, A290729, A290730, A290731, A317623."
			],
			"keyword": "nonn,mult",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, Aug 10 2017",
			"ext": [
				"Even terms corrected by _Andrew Howroyd_, Nov 03 2018"
			],
			"references": 6,
			"revision": 33,
			"time": "2018-11-03T19:45:34-04:00",
			"created": "2017-08-10T12:31:52-04:00"
		}
	]
}