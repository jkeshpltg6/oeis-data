{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A288646",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 288646,
			"data": "1,10,11,1110,101,111110,1101,11111100,10001,1111111010,110001,111111110010,1000001,11111111100010,11101001,1111111111011010,101001001,111111111110000010,1101111001,11111111111101010010,10000101001,1111111111111001010010,111010101001",
			"name": "Binary representation of the diagonal from the corner to the origin of the n-th stage of growth of the two-dimensional cellular automaton defined by \"Rule 489\", based on the 5-celled von Neumann neighborhood.",
			"comment": [
				"Initialized with a single black (ON) cell at stage zero."
			],
			"reference": [
				"S. Wolfram, A New Kind of Science, Wolfram Media, 2002; p. 170."
			],
			"link": [
				"Robert Price, \u003ca href=\"/A288646/b288646.txt\"\u003eTable of n, a(n) for n = 0..126\u003c/a\u003e",
				"Robert Price, \u003ca href=\"/A288646/a288646.tmp.txt\"\u003eDiagrams of first 20 stages\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"http://arxiv.org/abs/1503.01168\"\u003eOn the Number of ON Cells in Cellular Automata\u003c/a\u003e, arXiv:1503.01168 [math.CO], 2015",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ElementaryCellularAutomaton.html\"\u003eElementary Cellular Automaton\u003c/a\u003e",
				"S. Wolfram, \u003ca href=\"http://wolframscience.com/\"\u003eA New Kind of Science\u003c/a\u003e",
				"Wolfram Research, \u003ca href=\"http://atlas.wolfram.com/\"\u003eWolfram Atlas of Simple Programs\u003c/a\u003e",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_2D_5-Neighbor_Cellular_Automata\"\u003eIndex to 2D 5-Neighbor Cellular Automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_Elementary_Cellular_Automata\"\u003eIndex to Elementary Cellular Automata\u003c/a\u003e"
			],
			"mathematica": [
				"CAStep[rule_, a_] := Map[rule[[10 - #]] \u0026, ListConvolve[{{0, 2, 0},{2, 1, 2}, {0, 2, 0}}, a, 2],{2}];",
				"code = 489; stages = 128;",
				"rule = IntegerDigits[code, 2, 10];",
				"g = 2 * stages + 1; (* Maximum size of grid *)",
				"a = PadLeft[{{1}}, {g, g}, 0,Floor[{g, g}/2]]; (* Initial ON cell on grid *)",
				"ca = a;",
				"ca = Table[ca = CAStep[rule, ca], {n, 1, stages + 1}];",
				"PrependTo[ca, a];",
				"(* Trim full grid to reflect growth by one cell at each stage *)",
				"k = (Length[ca[[1]]] + 1)/2;",
				"ca = Table[Table[Part[ca[[n]] [[j]],Range[k + 1 - n, k - 1 + n]], {j, k + 1 - n, k - 1 + n}], {n, 1, k}];",
				"Table[FromDigits[Part[ca[[i]] [[i]], Range[i, 2 * i - 1]], 10], {i, 1, stages - 1}]"
			],
			"xref": [
				"Cf. A288647, A288648, A288649."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Robert Price_, Jun 12 2017",
			"references": 4,
			"revision": 7,
			"time": "2017-06-12T21:52:20-04:00",
			"created": "2017-06-12T21:52:20-04:00"
		}
	]
}