{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A329413",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 329413,
			"data": "1,2,3,7,13,5,8,9,17,16,4,6,11,12,10,14,15,21,18,19,20,30,22,24,29,26,23,25,36,32,33,27,28,37,31,39,35,34,38,42,44,41,40,43,45,46,47,50,52,49,65,53,51,54,55,57,48,60,56,59,61,71,70,67,58,64,62,63,68,66,73,72,69,76,75,74,78,80",
			"name": "Lexicographically earliest sequence of distinct positive numbers such that among the pairwise sums of any five consecutive terms there are exactly two prime sums.",
			"comment": [
				"Conjectured to be a permutation of the positive integers: a(10^6) = 10^6 + 9 and all numbers below 10^6 - 7 are used at that point. - _M. F. Hasler_, Nov 15 2019"
			],
			"link": [
				"Jean-Marc Falcoz, \u003ca href=\"/A329413/b329413.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"a(1) = 1 is the smallest possible choice for the first term.",
				"a(2) = 2 as 2 is the smallest available integer not leading to a contradiction. Note that as 1 + 2 = 3 we already have one prime sum (on the required two) with the quintuplet {1,2,a(3),a(4),a(5)}.",
				"a(3) = 3 as 3 is the smallest available integer not leading to a contradiction. Note that as 2 + 3 = 5 we now have the two prime sums required with the quintuplet {1,2,3,a(4),a(5)}.",
				"a(4) = 7 as a(4) = 4, 5 or 6 would lead to a contradiction: indeed, the quintuplets {1,2,3,4,a(5)}, {1,2,3,5,a(5)} and {1,2,3,6,a(5)} will produce more than the two required prime sums. With a(4) = 7 we have no contradiction as the quintuplet {1,2,3,7,a(5)} has now exactly two prime sums: 1 + 2 = 3 and 2 + 3 = 5.",
				"a(5) = 13 as a(5) = 4, 5, 6, 8, 9, 10, 11 or 12 would again lead to a contradiction (more than 2 prime sums with the quintuplet); in combination with any other term before it, a(5) = 13 will produce only composite sums.",
				"a(6) = 5 as 5 is the smallest available integer not leading to a contradiction: indeed, the quintuplet {2,3,7,13,5} shows exactly the two prime sums we are looking for: 2 + 3 = 5 and 2 + 5 = 7.",
				"And so on."
			],
			"program": [
				"(PARI) A329413(n,show=0,o=1,N=2,M=4,p=[],U,u=o)={for(n=o,n-1, show\u0026\u0026print1(o\",\"); U+=1\u003c\u003c(o-u); U\u003e\u003e=-u+u+=valuation(U+1,2); p=concat(if(#p\u003e=M,p[^1],p),o); my(c=N-sum(i=2,#p,sum(j=1,i-1,isprime(p[i]+p[j])))); if(#p\u003cM \u0026\u0026 sum(i=1,#p,isprime(p[i]+u))\u003c=c, o=u)|| for(k=u,oo, bittest(U,k-u) || sum(i=1,#p,isprime(p[i]+k))!=c || [o=k, break]));o} \\\\ Optional args: show=1: print a(o..n-1); o=0: start with a(0) = 0 (A329453), N, M: produce N primes using M+1 consecutive terms. - _M. F. Hasler_, Nov 15 2019"
			],
			"xref": [
				"Cf. A329333 (3 consecutive terms, exactly 1 prime sum). See also A329450, A329452 onwards."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Eric Angelini_ and _Jean-Marc Falcoz_, Nov 14 2019",
			"references": 3,
			"revision": 30,
			"time": "2019-11-28T11:02:05-05:00",
			"created": "2019-11-15T23:52:44-05:00"
		}
	]
}