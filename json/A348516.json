{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A348516",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 348516,
			"data": "1,0,7,0,16,1,7,1,22,0,16,1,16,6,2,1,8,6,7,1,4,1,66,9,22,3,2,0,15,1,16,2,32,1,6,9,16,2,11,6,19,13,2,13,1,1,10,22,8,2,1,6,1,159,7,1,20,1,3,6,4,2,15,1,11,3,66,6,1,9,1,6,22,2,4,3,1,2,2,2,6",
			"name": "a(n) is the least positive integer k such that the base 3 representation of n^k contains equally many 1's and 2's, or 0 if no k with this property exists.",
			"comment": [
				"a(3*n) = a(n) for any positive integer n because multiplication by 3 does not change the counts of the digits 1 and 2 in the base 3 representation. Hence a(n) reaches any of its values at infinitely many n.",
				"There are infinitely many n with a(n) = 1 that are not divisible by 3, e.g. the numbers of the form (3^m + 2)(3^(m-1) + 3^(m-2) + ... + 3 + 1), m = 1, 2, 3, ...",
				"Of course, a(n^a(n)) = 1 whenever a(n) \u003e 0. More generally, if a(n) = p*q, where p and q are positive integers, then a(n^p) = q (hence any positive divisor of a nonzero term of the sequence is a term too). If a(n) = 0 then a(n^p) = 0 for any positive integer p.",
				"In the absence of a proof that a(n) = 0 only for the numbers n which are powers of 3, it would be desirable to have at least an algorithm whose application to any concrete n answers the question whether a(n) = 0.",
				"Except for the case when the number a(n) is 0, it is the least positive integer k such that n^k is a term of the sequence A039001.",
				"Problem: Are there positive integers not occurring in the sequence a(1),a(2),a(3),...?"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A348516/b348516.txt\"\u003eTable of n, a(n) for n = 0..19683\u003c/a\u003e"
			],
			"example": [
				"a(2) = 7 because the base 3 representations of 2^1, 2^2, 2^3, 2^4, 2^5, 2^6 and 2^7 are 2, 11, 22, 121, 1012, 2101 and 11202 respectively."
			],
			"mathematica": [
				"Array[If[IntegerQ@ Log[3, #], 0, Block[{k = 1}, While[Unequal @@ Most@ DigitCount[#^k, 3], k++]; k]] \u0026, 72] (* _Michael De Vlieger_, Oct 21 2021 *)"
			],
			"program": [
				"(Python)",
				"h=[0,1,-1]",
				"def d(x):",
				"    y,d=x,0",
				"    while y\u003e0: d,y=d+h[y%3],y//3",
				"    return d",
				"def a(n):",
				"    v,a,x=n,0,1",
				"    while v%3==0: v=v//3",
				"    if v\u003e1:",
				"        while d(x)!=0: a,x=a+1,v*x",
				"    return a",
				"(Python)",
				"from gmpy2 import digits",
				"def A348516(n):",
				"    k, s = 1, digits(n,3).rstrip('0')",
				"    if s == '1' or s == '': return 1-len(s)",
				"    m = int(s,3)",
				"    mk = m",
				"    while s.count('1') != s.count('2'): k += 1; mk *= m; s = digits(mk,3)",
				"    return k # _Chai Wah Wu_, Nov 11 2021",
				"(PARI) isp3(n) = my(q); isprimepower(n,\u0026q) \u0026\u0026 (q==3);",
				"isok(k, n) = my(d=digits(n^k, 3)); #select(x-\u003e(x==1), d) == #select(x-\u003e(x==2), d);",
				"a(n) = if ((n==1) || isp3(n), return (0)); my(k=1); while (!isok(k, n), k++); k; \\\\ _Michel Marcus_, Oct 22 2021"
			],
			"xref": [
				"Cf. A039001."
			],
			"keyword": "nonn,base",
			"offset": "0,3",
			"author": "_Dimiter Skordev_, Oct 21 2021",
			"ext": [
				"a(0) from _Michel Marcus_, Nov 11 2021"
			],
			"references": 1,
			"revision": 50,
			"time": "2021-11-11T20:39:35-05:00",
			"created": "2021-11-11T09:28:53-05:00"
		}
	]
}