{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A014552",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 14552,
			"data": "0,0,1,1,0,0,26,150,0,0,17792,108144,0,0,39809640,326721800,0,0,256814891280,2636337861200,0,0,3799455942515488,46845158056515936,0,0,111683611098764903232,1607383260609382393152,0,0",
			"name": "Number of solutions to Langford (or Langford-Skolem) problem (up to reversal of the order).",
			"comment": [
				"These are also called Langford pairings.",
				"2*a(n)=A176127(n) gives the number of ways are of arranging the numbers 1,1,2,2,...,n,n so that there is one number between the two 1's, two numbers between the two 2's, ..., n numbers between the two n's.",
				"a(n) \u003e 0 iff n == 0 or 3 (mod 4)."
			],
			"reference": [
				"Jaromir Abrham, \"Exponential lower bounds for the numbers of Skolem and extremal Langford sequences,\" Ars Combinatoria 22 (1986), 187-198.",
				"Elin Farnell, Puzzle Pedagogy: A Use of Riddles in Mathematics Education, PRIMUS, July 2016, pp. 202-211; http://dx.doi.org/10.1080/10511970.2016.1195465",
				"M. Gardner, Mathematical Magic Show, New York: Vintage, pp. 70 and 77-78, 1978.",
				"M. Gardner, Mathematical Magic Show, Revised edition published by Math. Assoc. Amer. in 1989. Contains a postscript on pp. 283-284 devoted to a discussion of early computations of the number of Langford sequences.",
				"R. K. Guy, The unity of combinatorics, Proc. 25th Iranian Math. Conf, Tehran, (1994), Math. Appl 329 129-159, Kluwer Dordrecht 1995, Math. Rev. 96k:05001.",
				"D. E. Knuth, The Art of Computer Programming, Vol. 4A, Section 7.1.1, p. 2.",
				"M. Krajecki, Christophe Jaillet and Alain Bui, \"Parallel tree search for combinatorial problems: A comparative study between OpenMP and MPI,\" Studia Informatica Universalis 4 (2005), 151-190.",
				"Roselle, David P. Distributions of integers into s-tuples with given differences. Proceedings of the Manitoba Conference on Numerical Mathematics (Univ. Manitoba, Winnipeg, Man., 1971), pp. 31--42.Dept. Comput. Sci., Univ. Manitoba, Winnipeg, Man., 1971. MR0335429 (49 #211). - From _N. J. A. Sloane_, Jun 05 2012"
			],
			"link": [
				"Ali Assarpour, Amotz Bar-Noy, Ou Liuo, \u003ca href=\"http://arxiv.org/abs/1507.00315\"\u003eCounting the Number of Langford Skolem Pairings\u003c/a\u003e, arXiv:1507.00315 [cs.DM], 2015.",
				"Gheorghe Coserea, \u003ca href=\"/A014552/a014552.txt\"\u003eSolutions for n=7\u003c/a\u003e.",
				"Gheorghe Coserea, \u003ca href=\"/A014552/a014552_1.txt\"\u003eSolutions for n=8\u003c/a\u003e.",
				"Gheorghe Coserea, \u003ca href=\"/A014552/a014552_1.mzn.txt\"\u003eMiniZinc model for generating solutions\u003c/a\u003e.",
				"R. O. Davies, \u003ca href=\"http://www.jstor.org/stable/3610650\"\u003eOn Langford's problem II\u003c/a\u003e, Math. Gaz., 1959, vol. 43, 253-255.",
				"M. Krajecki, \u003ca href=\"http://dialectrix.com/langford/krajecki/krajecki-letter.html\"\u003eL(2,23)=3,799,455,942,515,488\u003c/a\u003e.",
				"C. D. Langford, \u003ca href=\"http://www.jstor.org/stable/3610392\"\u003e2781. Parallelograms with Integral Sides and Diagonals\u003c/a\u003e, Math. Gaz., 1958, vol. 42, p. 228.",
				"J. E. Miller, \u003ca href=\"http://dialectrix.com/langford.html\"\u003eLangford's Problem\u003c/a\u003e",
				"G. Nordh, \u003ca href=\"http://arxiv.org/abs/math/0506155\"\u003ePerfect Skolem sequences\u003c/a\u003e, arXiv:math/0506155 [math.CO], 2005.",
				"Zan Pan, \u003ca href=\"https://eprint.panzan.me/articles/langford.pdf\"\u003eConjectures on the number of Langford sequences\u003c/a\u003e, (2021).",
				"C. J. Priday, \u003ca href=\"http://www.jstor.org/stable/3610650\"\u003eOn Langford's Problem I\u003c/a\u003e, Math. Gaz., 1959, vol. 43, 250-255.",
				"W. Schneider, \u003ca href=\"http://web.archive.org/web/2004/www.wschnei.de/digit-related-numbers/langfords-problem.html\"\u003eLangford's Problem\u003c/a\u003e",
				"T. Skolem, \u003ca href=\"http://www.mscand.dk/article/view/10490\"\u003eOn certain distributions of integers in pairs with given differences\u003c/a\u003e, Math. Scand., 1957, vol. 5, 57-68.",
				"T. Saito and S. Hayasaka, \u003ca href=\"http://www.jstor.org/stable/3618042\"\u003eLangford sequences: a progress report\u003c/a\u003e, Math. Gaz., 1979, vol. 63, #426, 261-262.",
				"J. E. Simpson, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(83)90008-0\"\u003eLangford Sequences: perfect and hooked\u003c/a\u003e, Discrete Math., 1983, vol. 44, #1, 97-104.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/LangfordsProblem.html\"\u003eLangford's Problem\u003c/a\u003e."
			],
			"formula": [
				"a(n) = A176127(n)/2."
			],
			"example": [
				"Solutions for n=3 and 4: 312132 and 41312432. Solution for n=16: 16, 14, 12, 10, 13, 5, 6, 4, 15, 11, 9, 5, 4, 6, 10, 12, 14, 16, 13, 8, 9, 11, 7, 1, 15, 1, 2, 3, 8, 2, 7, 3."
			],
			"xref": [
				"See A050998 for further examples of solutions.",
				"If the zeros are omitted we get A192289.",
				"Cf. A059106, A059107, A059108, A125762, A026272."
			],
			"keyword": "nonn,hard,nice,more",
			"offset": "1,7",
			"author": "John E. Miller (john@timehaven.us), _Eric W. Weisstein_, _N. J. A. Sloane_",
			"ext": [
				"a(20) from Ron van Bruchem and Mike Godfrey, Feb 18 2002",
				"a(21)-a(23) sent by John E. Miller (john@timehaven.us) and Pab Ter (pabrlos(AT)yahoo.com), May 26 2004. These values were found by a team at Universite de Reims Champagne-Ardenne, headed by Michael Krajecki, using over 50 processors for 4 days.",
				"a(24)=46845158056515936 was computed circa Apr 15 2005 by the Krajecki team. - _Don Knuth_, Feb 03 2007",
				"Edited by _Max Alekseyev_, May 31 2011",
				"a(27) from the J. E. Miller web page \"Langford's problem\"; thanks to _Eric Desbiaux_ for reporting this. - _N. J. A. Sloane_, May 18 2015. However, it appears that the value was wrong. - _N. J. A. Sloane_, Feb 22 2016",
				"Corrected and extended using results from the Assarpour et al. (2015) paper by _N. J. A. Sloane_, Feb 22 2016 at the suggestion of _William Rex Marshall_."
			],
			"references": 19,
			"revision": 85,
			"time": "2021-06-09T22:40:25-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}