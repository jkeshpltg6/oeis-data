{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A191727",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 191727,
			"data": "1,3,2,8,6,4,21,16,11,5,53,41,28,13,7,133,103,71,33,18,9,333,258,178,83,46,23,10,833,646,446,208,116,58,26,12,2083,1616,1116,521,291,146,66,31,14,5208,4041,2791,1303,728,366,166,78,36,15,13021,10103",
			"name": "Dispersion of A047219, (numbers \u003e1 and congruent to 1 or 3 mod 5), by antidiagonals.",
			"comment": [
				"For a background discussion of dispersions and their fractal sequences, see A191426.  For dispersions of congruence sequences mod 3, mod 4, or mod 5, see A191655, A191663, A191667, A191702.",
				"...",
				"Suppose that {2,3,4,5,6} is partitioned as {x1, x2} and {x3,x4,x5}.  Let S be the increasing sequence of numbers \u003e1 and congruent to x1 or x2 mod 5, and let T be the increasing sequence of numbers \u003e1 and congruent to x3 or x4 or x5 mod 5.  There are 10 sequences in S, each matched by a (nearly) complementary sequence in T.  Each of the 20 sequences generates a dispersion, as listed here:",
				"...",
				"A191722=dispersion of A008851 (0, 1 mod 5 and \u003e1)",
				"A191723=dispersion of A047215 (0, 2 mod 5 and \u003e1)",
				"A191724=dispersion of A047218 (0, 3 mod 5 and \u003e1)",
				"A191725=dispersion of A047208 (0, 4 mod 5 and \u003e1)",
				"A191726=dispersion of A047216 (1, 2 mod 5 and \u003e1)",
				"A191727=dispersion of A047219 (1, 3 mod 5 and \u003e1)",
				"A191728=dispersion of A047209 (1, 4 mod 5 and \u003e1)",
				"A191729=dispersion of A047221 (2, 3 mod 5 and \u003e1)",
				"A191730=dispersion of A047211 (2, 4 mod 5 and \u003e1)",
				"A191731=dispersion of A047204 (3, 4 mod 5 and \u003e1)",
				"...",
				"A191732=dispersion of A047202 (2,3,4 mod 5 and \u003e1)",
				"A191733=dispersion of A047206 (1,3,4 mod 5 and \u003e1)",
				"A191734=dispersion of A032793 (1,2,4 mod 5 and \u003e1)",
				"A191735=dispersion of A047223 (1,2,3 mod 5 and \u003e1)",
				"A191736=dispersion of A047205 (0,3,4 mod 5 and \u003e1)",
				"A191737=dispersion of A047212 (0,2,4 mod 5 and \u003e1)",
				"A191738=dispersion of A047222 (0,2,3 mod 5 and \u003e1)",
				"A191739=dispersion of A008854 (0,1,4 mod 5 and \u003e1)",
				"A191740=dispersion of A047220 (0,1,3 mod 5 and \u003e1)",
				"A191741=dispersion of A047217 (0,1,2 mod 5 and \u003e1)",
				"...",
				"For further information about these 20 dispersions, see A191722.",
				"...",
				"Regarding the dispersions A191722-A191741, there are general formulas for sequences of the type \"(a or b mod m)\" and \"(a or b or c mod m)\" used in the relevant Mathematica programs."
			],
			"link": [
				"Ivan Neretin, \u003ca href=\"/A191727/b191727.txt\"\u003eTable of n, a(n) for n = 1..5050\u003c/a\u003e (first 100 antidiagonals, flattened)"
			],
			"example": [
				"Northwest corner:",
				"1....3....8....21....53",
				"2....6....16...41....103",
				"4....11...28...71....178",
				"5....13...33...83....208",
				"7....18...46...116...291",
				"9....23...58...146...366"
			],
			"mathematica": [
				"(* Program generates the dispersion array t of the increasing sequence f[n] *)",
				"r = 40; r1 = 12;  c = 40; c1 = 12;",
				"a=3; b=6; m[n_]:=If[Mod[n,2]==0,1,0];",
				"f[n_]:=a*m[n+1]+b*m[n]+5*Floor[(n-1)/2]",
				"Table[f[n], {n, 1, 30}]  (* A047219 *)",
				"mex[list_] := NestWhile[#1 + 1 \u0026, 1, Union[list][[#1]] \u003c= #1 \u0026, 1, Length[Union[list]]]",
				"rows = {NestList[f, 1, c]};",
				"Do[rows = Append[rows, NestList[f, mex[Flatten[rows]], r]], {r}];",
				"t[i_, j_] := rows[[i, j]];",
				"TableForm[Table[t[i, j], {i, 1, 10}, {j, 1, 10}]] (* A191727 *)",
				"Flatten[Table[t[k, n - k + 1], {n, 1, c1}, {k, 1, n}]] (* A191727  *)"
			],
			"xref": [
				"Cf. A047212, A047219, A191737, A191722, A191426."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Jun 13 2011",
			"references": 20,
			"revision": 12,
			"time": "2017-10-12T13:06:41-04:00",
			"created": "2011-06-15T17:55:48-04:00"
		}
	]
}