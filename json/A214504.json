{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A214504",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 214504,
			"data": "12,14,32,36,36,48,80,88,86,100,188,210,209,228,204,204,418,470,472,524,479,452,906,1016,1028,1152,1050,1020,1088,980,1943,2170,2219,2472,2250,2222,2333,2200,4137,4610,4754,5260,4811,4738,4929,4784,4920,4924",
			"name": "Irregular array T(n,k) of the numbers of non-extendable (complete) non-self-adjacent simple paths incorporating each of a minimal subset of nodes within a square lattice bounded by rectangles with nodal dimensions n and 3, n \u003e= 2.",
			"comment": [
				"The subset of nodes is contained in the top left-hand quarter of the rectangle and has nodal dimensions floor((n+1)/2) and 2 to capture all geometrically distinct counts.",
				"The quarter-rectangle is read by rows.",
				"The irregular array of numbers is:",
				"....k....1....2....3....4....5....6....7....8....9...10",
				"..n",
				"..2.....12...14",
				"..3.....32...36...36...48",
				"..4.....80...88...86..100",
				"..5....188..210..209..228..204..204",
				"..6....418..470..472..524..479..452",
				"..7....906.1016.1028.1152.1050.1020.1088..980",
				"..8...1943.2170.2219.2472.2250.2222.2333.2200",
				"..9...4137.4610.4754.5260.4811.4738.4929.4784.4920.4924",
				"where k indicates the position of a node in the quarter-rectangle.",
				"For each n, the maximum value of k is 2*floor((n+1)/2).",
				"Reading this array by rows gives the sequence."
			],
			"link": [
				"C. H. Gribble, \u003ca href=\"https://oeis.org/wiki/Complete_non-self-adjacent_paths:Results_for_Square_Lattice\"\u003eComputed characteristics of complete non-self-adjacent paths in a square lattice bounded by various sizes of rectangle.\u003c/a\u003e",
				"C. H. Gribble, \u003ca href=\"https://oeis.org/wiki/Complete non-self-adjacent paths:Program\"\u003eComputes characteristics of complete non-self-adjacent paths in square and cubic lattices bounded by various sizes of rectangle and rectangular cuboid respectively.\u003c/a\u003e"
			],
			"example": [
				"When n = 2, the number of times (NT) each node in the rectangle (N) occurs in a complete non-self-adjacent simple path is",
				"N   0  1  2",
				"    3  4  5",
				"NT 12 14 12",
				"   12 14 12",
				"To limit duplication, only the top left-hand corner 12 and the 14 to its right are stored in the sequence,",
				"  i.e. T(2,1) = 12 and T(2,2) = 14."
			],
			"xref": [
				"Cf. A213106, A213249, A213089, A213954, A214121, A214397, A214399"
			],
			"keyword": "nonn,tabf",
			"offset": "2,1",
			"author": "_Christopher Hunt Gribble_, Jul 19 2012",
			"ext": [
				"Comment corrected by _Christopher Hunt Gribble_, Jul 22 2012"
			],
			"references": 6,
			"revision": 8,
			"time": "2012-07-23T12:47:02-04:00",
			"created": "2012-07-19T14:01:14-04:00"
		}
	]
}