{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A118920",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 118920,
			"data": "2,4,2,10,8,2,28,28,12,2,84,96,54,16,2,264,330,220,88,20,2,858,1144,858,416,130,24,2,2860,4004,3276,1820,700,180,28,2,9724,14144,12376,7616,3400,1088,238,32,2,33592,50388,46512,31008,15504,5814,1596,304,36,2",
			"name": "Triangle read by rows: T(n,k) is the number of Grand Dyck paths of semilength n that cross the x-axis k times (n\u003e=1, k\u003e=0).",
			"comment": [
				"A Grand Dyck path of semilength n is a path in the half-plane x\u003e=0, starting at (0,0), ending at (2n,0) and consisting of steps u=(1,1) and d=(1,-1).",
				"Row sums are the central binomial coefficients (A000984). T(n,0)=2*A000108(n) (the Catalan numbers doubled). T(n,1)=2*A002057(n-2). Sum(k*T(n,k),k\u003e=0)=2*A008549(n-1). For crossings of the x-axis in one direction, see A118919.",
				"This triangle is related to paired pattern P_3 and P_4 defined in the Pan \u0026 Remmel link. - _Ran Pan_, Feb 01 2016"
			],
			"link": [
				"Ran Pan and Jeffrey B. Remmel, \u003ca href=\"http://arxiv.org/abs/1601.07988\"\u003ePaired patterns in lattice paths\u003c/a\u003e, arXiv:1601.07988 [math.CO], 2016."
			],
			"formula": [
				"T(n,k) = 2*(k+1)*binomial(2*n,n-k-1)/n.",
				"G.f.: G(t,z)=2*z*C^2/(1-t*z*C^2), where C=(1-sqrt(1-4*z))/(2*z) is the Catalan function.",
				"More generally, the trivariate g.f. G=G(x,y,z), where x (y) marks number of downward (upward) crossings of the x-axis, is given by G = z*C^2*(2+(x+y)*z*C^2)/(1-x*y*z^2*C^4).",
				"a(n) = 2 * A039598(n-1). - _Georg Fischer_, Oct 27 2021"
			],
			"example": [
				"T(3,1)=8 because we have ud|dudu,ud|dduu,udud|du,uudd|du,du|udud,du|uudd, dudu|ud and dduu|ud (the crossings of the x-axis are shown by |).",
				"Triangle starts:",
				"   2;",
				"   4, 2;",
				"  10, 8, 2;",
				"  28,28,12, 2;",
				"  84,96,54,16,2;"
			],
			"maple": [
				"T:=(n,k)-\u003e2*(k+1)*binomial(2*n,n-k-1)/n: for n from 1 to 11 do seq(T(n,k),k=0..n-1) od; # yields sequence in triangular form"
			],
			"mathematica": [
				"Table[2 (k + 1) Binomial[2 n, n - k - 1]/n, {n, 10}, {k, 0, n - 1}] // Flatten (* _Michael De Vlieger_, Feb 01 2016 *)"
			],
			"program": [
				"(Sage) # Algorithm of L. Seidel (1877)",
				"# Prints the first n rows of the triangle.",
				"def A118920_triangle(n) :",
				"    D = [0]*(n+2); D[1] = 2",
				"    b = True; h = 1",
				"    for i in range(2*n) :",
				"        if b :",
				"            for k in range(h, 0, -1) : D[k] += D[k-1]",
				"            h += 1",
				"        else :",
				"            for k in range(1, h, 1) : D[k] += D[k+1]",
				"        b = not b",
				"        if b : print([D[z] for z in (1..h-1)])",
				"A118920_triangle(10)  # _Peter Luschny_, Oct 19 2012",
				"(PARI) T(n,k) = 2*(k+1)*binomial(2*n,n-k-1)/n \\\\ _Charles R Greathouse IV_, Feb 01 2016"
			],
			"xref": [
				"Cf. A000984, A000108, A002057, A008549, A039598, A118919."
			],
			"keyword": "nonn,tabl",
			"offset": "1,1",
			"author": "_Emeric Deutsch_, May 06 2006",
			"references": 2,
			"revision": 25,
			"time": "2021-10-27T11:26:42-04:00",
			"created": "2006-05-19T03:00:00-04:00"
		}
	]
}