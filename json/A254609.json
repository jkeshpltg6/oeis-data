{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A254609",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 254609,
			"data": "1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,5,5,5,5,1,1,1,5,5,5,1,1,1,1,1,5,5,1,1,1,1,1,1,1,5,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,5,5,5,5,1,5,5,5,5,1,1,1,5,5,5,1,1,5,5,5,1,1,1,1,1,5,5,1,1,1,5,5",
			"name": "Triangle read by rows: T(n,k) = A243757(n)/(A243757(k)*A243757(n-k)).",
			"comment": [
				"These are the generalized binomial coefficients associated with A060904.",
				"The exponent of T(n,k) is the number of 'carries' that occur when adding k and n-k in base 5 using the traditional addition algorithm.",
				"If T(n,k) != 0 mod 5, then n dominates k in base 5.",
				"A194459(n) = number of ones in row n. - _Reinhard Zumkeller_, Feb 04 2015"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A254609/b254609.txt\"\u003eRows n = 0..124 of triangle, flattened\u003c/a\u003e",
				"Tyler Ball, Tom Edgar, and Daniel Juda, \u003ca href=\"http://dx.doi.org/10.4169/math.mag.87.2.135\"\u003eDominance Orders, Generalized Binomial Coefficients, and Kummer's Theorem\u003c/a\u003e, Mathematics Magazine, Vol. 87, No. 2, April 2014, pp. 135-143.",
				"Tyler Ball and Daniel Juda, \u003ca href=\"https://www.rose-hulman.edu/mathjournal/archives/2013/vol14-n2/paper2/v14n2-2pd.pdf\"\u003eDominance over N\u003c/a\u003e, Rose-Hulman Undergraduate Mathematics Journal, Vol. 13, No. 2, Fall 2013.",
				"Tom Edgar and Michael Z. Spivey, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL19/Edgar/edgar3.html\"\u003eMultiplicative functions, generalized binomial coefficients, and generalized Catalan numbers\u003c/a\u003e, Journal of Integer Sequences, Vol. 19 (2016), Article 16.1.6."
			],
			"formula": [
				"T(n,k) = A243757(n)/(A243757(k)*A243757(n-k)).",
				"T(n,k) = Product_{i=1..n} A060904(i)/(Product_{i=1..k} A060904(i)*Product_{i=1..n-k} A060904(i)).",
				"T(n,k) = A060904(n)/n*(k/A060904(k)*T(n-1,k-1)+(n-k)/A060904(n-k)*T(n-1,k))."
			],
			"example": [
				"The first five terms in A060904 are 1, 1, 1, 1, and 5 and so T(4,2) = 1*1*1*1/((1*1)*(1*1))=1 and T(5,3) = 5*1*1*1*1/((1*1*1)*(1*1))=5.",
				"The triangle begins:",
				"1",
				"1, 1",
				"1, 1, 1",
				"1, 1, 1, 1",
				"1, 1, 1, 1, 1",
				"1, 5, 5, 5, 5, 1",
				"1, 1, 5, 5, 5, 1, 1",
				"1, 1, 1, 5, 5, 1, 1, 1",
				"1, 1, 1, 1, 5, 1, 1, 1, 1",
				"1, 1, 1, 1, 1, 1, 1, 1, 1, 1",
				"1, 5, 5, 5, 5, 1, 5, 5, 5, 5, 1",
				"1, 1, 5, 5, 5, 1, 1, 5, 5, 5, 1, 1",
				"1, 1, 1, 5, 5, 1, 1, 1, 5, 5, 1, 1, 1",
				"1, 1, 1, 1, 5, 1, 1, 1, 1, 5, 1, 1, 1, 1",
				"1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1"
			],
			"program": [
				"P=[0]+[5^valuation(i,5) for i in [1..100]]",
				"[m for sublist in [[mul(P[1:n+1])/(mul(P[1:k+1])*mul(P[1:(n-k)+1])) for k in [0..n]] for n in [0..len(P)-1]] for m in sublist]",
				"(Haskell)",
				"import Data.List (inits)",
				"a254609 n k = a254609_tabl !! n !! k",
				"a254609_row n = a254609_tabl !! n",
				"a254609_tabl = zipWith (map . div)",
				"   a243757_list $ zipWith (zipWith (*)) xss $ map reverse xss",
				"   where xss = tail $ inits a243757_list",
				"-- _Reinhard Zumkeller_, Feb 04 2015"
			],
			"xref": [
				"Cf. A060904, A243757, A234957, A242849, A082907.",
				"Cf. A194459."
			],
			"keyword": "nonn,tabl",
			"offset": "0,17",
			"author": "_Tom Edgar_, Feb 02 2015",
			"references": 3,
			"revision": 19,
			"time": "2016-12-24T20:18:25-05:00",
			"created": "2015-02-04T09:01:10-05:00"
		}
	]
}