{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A126760",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 126760,
			"data": "0,1,1,1,1,2,1,3,1,1,2,4,1,5,3,2,1,6,1,7,2,3,4,8,1,9,5,1,3,10,2,11,1,4,6,12,1,13,7,5,2,14,3,15,4,2,8,16,1,17,9,6,5,18,1,19,3,7,10,20,2,21,11,3,1,22,4,23,6,8,12,24,1,25,13,9,7,26,5,27,2,1,14,28,3,29,15,10,4,30,2",
			"name": "a(0) = 0, a(2n) = a(n), a(3n) = a(n), a(6n+1) = 2n + 1, a(6n+5) = 2n + 2.",
			"comment": [
				"For further information see A126759, which provided the original motivation for this sequence.",
				"From _Antti Karttunen_, Jan 28 2015: (Start)",
				"The odd bisection of the sequence gives A253887, and the even bisection gives the sequence itself.",
				"A254048 gives the sequence obtained when this sequence is restricted to A007494 (numbers congruent to 0 or 2 mod 3).",
				"For all odd numbers k present in square array A135765, a(k) = the column index of k in that array. (End)",
				"A322026 and this sequence (without the initial zero) are ordinal transforms of each other. - _Antti Karttunen_, Feb 09 2019"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A126760/b126760.txt\"\u003eTable of n, a(n) for n = 0..19683\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A126759(n)-1. [The original definition.]",
				"From _Antti Karttunen_, Jan 28 2015: (Start)",
				"a(0) = 0, a(2n) = a(n), a(3n) = a(n), a(6n+1) = 2n + 1, a(6n+5) = 2n + 2.",
				"Or with the last clause represented in another way:",
				"a(0) = 0, a(2n) = a(n), a(3n) = a(n), a(6n+1) = 2n + 1, a(6n-1) = 2n.",
				"Other identities. For all n \u003e= 1:",
				"a(n) = A253887(A003602(n)).",
				"a(6n-3) = a(4n-2) = a(2n-1) = A253887(n).",
				"(End)",
				"a(n) = A249746(A003602(A064989(n))). - _Antti Karttunen_, Feb 04 2015"
			],
			"mathematica": [
				"f[n_] := Block[{a}, a[0] = 0; a[1] = a[2] = a[3] = 1; a[x_] := Which[EvenQ@ x, a[x/2], Mod[x, 3] == 0, a[x/3], Mod[x, 6] == 1, 2 (x - 1)/6 + 1, Mod[x, 6] == 5, 2 (x - 5)/6 + 2]; Table[a@ i, {i, 0, n}]] (* _Michael De Vlieger_, Feb 03 2015 *)"
			],
			"program": [
				"(Scheme, with memoizing macro definec)",
				"(definec (A126760 n) (cond ((zero? n) n) ((even? n) (A126760 (/ n 2))) ((zero? (modulo n 3)) (A126760 (/ n 3))) ((= 1 (modulo n 6)) (+ 1 (/ (- n 1) 3))) (else (/ (+ n 1) 3))))",
				";; _Antti Karttunen_, Jan 28 2015",
				"(PARI) A126760(n)={n\u0026\u0026n\\=3^valuation(n,3)\u003c\u003cvaluation(n,2);n%3+n\\6*2} \\\\ _M. F. Hasler_, Jan 19 2016"
			],
			"xref": [
				"One less than A126759.",
				"Cf. A003586, A003602, A007310, A064989, A249746, A253887, A254048, A273669, A322026 (ordinal transform), A322317, A323881 (Dirichlet inverse), A323882.",
				"Related arrays: A135765, A254102."
			],
			"keyword": "nonn",
			"offset": "0,6",
			"author": "_N. J. A. Sloane_, Feb 19 2007",
			"ext": [
				"Name replaced with an independent recurrence and the old description moved to the Formula section - _Antti Karttunen_, Jan 28 2015"
			],
			"references": 26,
			"revision": 37,
			"time": "2019-02-09T23:26:10-05:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}