{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A232398",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 232398,
			"data": "0,0,0,1,2,2,2,2,4,2,2,2,3,2,4,3,4,2,4,4,4,2,3,3,3,4,4,1,3,4,5,3,5,4,5,4,4,1,4,3,5,3,5,4,5,4,5,3,3,4,5,2,3,3,4,4,5,3,3,4,6,4,5,3,7,5,5,3,4,6,6,4,7,4,6,6,7,3,3,4,5,5,6,2,6,5,5,4,5,5,5,5,5,1,4,6,4,2,5,6",
			"name": "Number of ways to write n = p + (2^k - k) + (2^m - m) with p prime and 0 \u003c k \u003c= m.",
			"comment": [
				"Conjecture: a(n) \u003e 0 for all n \u003e 3.",
				"This was motivated by A231201. We have verified the conjecture for n up to 2*10^8. It seems that a(n) = 1 for no odd n.",
				"In contrast, R. Crocker proved that there are infinitely many positive odd numbers not of the form p + 2^k + 2^m with p prime and k, m \u003e 0.",
				"It seems that any integer n \u003e 3 not equal to 1361802 can be written in the form p + (2^k + k) + (2^m + m), where p is a prime, and k and m are nonnegative integers.",
				"On Dec 08 2013, Qing-Hu Hou finished checking the conjecture for n up to 10^10 and found no counterexamples. - _Zhi-Wei Sun_, Dec 08 2013"
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A232398/b232398.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"R. Crocker, \u003ca href=\"http://projecteuclid.org/euclid.pjm/1102971271\"\u003eOn the sum of a prime and two powers of two\u003c/a\u003e, Pacific J. Math. 36 (1971), 103-107.",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1312.1166\"\u003eOn a^n + b*n modulo m\u003c/a\u003e, preprint, arXiv:1312.1166 [math.NT], 2013-2014."
			],
			"example": [
				"a(11) = 2 since 11 = 5 + (2 - 1) + (2^3 - 3) = 7 + (2^2 - 2) + (2^2 - 2) with 5 and 7 prime.",
				"a(28) = 1 since 28 = 11 + (2^3 - 3) + (2^4 - 4) with 11 prime.",
				"a(94) = 1 since 94 = 31 + (2^3 - 3) + (2^6 - 6) with 31 prime."
			],
			"mathematica": [
				"PQ[n_]:=n\u003e0\u0026\u0026PrimeQ[n]",
				"A232398[n_] := Sum[If[2^m - m \u003c n \u0026\u0026 PQ[n - 2^m + m - 2^k + k], 1, 0], {m, Log[2, 2n]}, {k, m}]; Table[A232398[n], {n, 100}]"
			],
			"xref": [
				"Cf. A000040, A000079, A156695, A231201, A231725, A232616."
			],
			"keyword": "nonn",
			"offset": "1,5",
			"author": "_Zhi-Wei Sun_, Nov 23 2013",
			"references": 8,
			"revision": 32,
			"time": "2019-08-06T12:03:24-04:00",
			"created": "2013-11-23T13:40:42-05:00"
		}
	]
}