{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A271724",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 271724,
			"data": "1,3,2,1,4,4,1,3,4,6,4,2,4,7,1,1,10,8,5,6,8,5,1,4,7,10,7,2,11,13,2,3,8,9,8,6,7,13,3,6,15,8,4,4,13,8,1,2,8,15,11,4,14,18,5,7,6,6,12,5,12,17,5,1,16,21,3,11,16,12,1,8,8,18,16,5,16,12,4,6",
			"name": "Number of ordered ways to write n as w^2 + x^2 + y^2 + z^2 with w*(x+2*y+3*z) a square, where w,x,y,z are nonnegative integers with x \u003e 0.",
			"comment": [
				"Conjecture: (i) a(n) \u003e 0 for all n \u003e 0, and a(n) = 1 only for n = 7, 15, 47, 151, 4^k*q (k = 0,1,2,... and q = 1, 23, 71).",
				"(ii) For positive integers a,b,c with gcd(a,b,c) squarefree, any natural number can be written as w^2 + x^2 + y^2 + z^2 with w,x,y,z nonnegative integers and w*(a*x+b*y+c*z) a square, if and only if {a,b,c} is among {1,2,3}, {1,3,6}, {1,6,9}, {5,6,9}, {18,30,114}.",
				"(iii) For each quadruple (a,b,c,d) = (1,1,2,12), (1,2,7,60), (1,3,9,48), (1,4,11,48), (1,5,8,24), (1,8,11,24), (2,6,8,15), (3,5,6,24), (3,6,15,40), (3,6,18,40), (3,12,15,20), (4,4,8,15), (4,8,12,21), (4,8,12,45), (4,8,20,15), (4,8,36,45), (5,10,15,24), (6,9,15,20), (7,14,28,60), (7,21,28,60), (7,21,42,60), (12,36,48,55), (14,21,28,60), (3,9,18,112), (3,21,33,80), (4,5,9,120), (4,12,16,105), any natural number can be written as w^2 + x^2 + y^2 + z^2 with w,x,y,z nonnegative integers such that (a*x+b*y+c*z)^2 + (d*w)^2 is a square.",
				"See also A271510, A271513, A271518, A271644, A271665, A271714 and A271721 for other conjectures refining lagrange's four-square theorem."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A271724/b271724.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1604.06723\"\u003eRefining Lagrange's four-square theorem\u003c/a\u003e, arXiv:1604.06723, 2016."
			],
			"example": [
				"a(1) = 1 since 1 = 0^2 + 1^2 + 0^2 + 0^2 with 1 \u003e 0 and 0*(1+2*0+3*0) = 0^2.",
				"a(3) = 2 since 3 = 1^2 + 1^2 + 0^2 + 1^2 with 1*(1+2*0+3*1) = 2^2, and 3 = 0^2 + 1^2 + 1^2 + 1^2 with 0*(1+2*1+3*1) = 0^2.",
				"a(7) = 1 since 7 = 1^2 + 1^2 + 1^2 + 2^2 with 1*(1+2*1+3*2) = 3^2.",
				"a(15) = 1 since 15 = 2^2 + 3^2 + 1^2 + 1^2 with 2*(3+2*1+3*1) = 4^2.",
				"a(23) = 1 since 23 = 1^2 + 3^2 + 2^2 + 3^2 with 1*(3+2*2+3*3) = 4^2.",
				"a(31) = 2 since 31 = 2^2 + 1^2 + 1^2 + 5^2 with 2*(1+2*1+3*5) = 6^2, and also 31 = 2^2 + 3^2 + 3^2 + 3^2 with 2*(3+2*3+3*3) = 6^2.",
				"a(47) = 1 since 47 = 1^2 + 1^2 + 3^2 + 6^2 with 1*(1+2*3+3*6) = 5^2.",
				"a(71) = 1 since 71 = 1^2 + 6^2 + 5^2 + 3^2 with 1*(6+2*5+3*3) = 5^2.",
				"a(151) = 1 since 151 = 9^2 + 6^2 + 5^2 + 3^2 with 9*(6+2*5+3*3) = 15^2."
			],
			"mathematica": [
				"SQ[n_]:=SQ[n]=IntegerQ[Sqrt[n]]",
				"Do[r=0;Do[If[SQ[n-x^2-y^2-z^2]\u0026\u0026SQ[Sqrt[n-x^2-y^2-z^2](x+2y+3z)],r=r+1],{x,1,Sqrt[n]},{y,0,Sqrt[n-x^2]},{z,0,Sqrt[n-x^2-y^2]}];Print[n,\" \",r];Label[aa];Continue,{n,1,80}]"
			],
			"xref": [
				"Cf. A000118, A000290, A259789, A271510, A271513, A271518, A271608, A271644, A271665, A271714, A271721."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Zhi-Wei Sun_, Apr 13 2016",
			"references": 38,
			"revision": 17,
			"time": "2016-05-01T13:28:18-04:00",
			"created": "2016-04-13T06:50:19-04:00"
		}
	]
}