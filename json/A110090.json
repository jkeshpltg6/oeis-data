{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A110090",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 110090,
			"data": "0,1,2,2,1,4,4,1,6,6,1,8,8,1,10,10,1,12,12,1,14,14,1,16,16,1,18,18,1,20,20,1,22,22,1,24,24,1,26,26,1,28,28,1,30,30,1,32,32,1,34,34,1,36,36,1,38,38,1,40,40,1,42,42,1,44,44,1,46,46,1,48,48,1,50,50,1,52,52,1,54,54",
			"name": "Numerators of sequence of rationals defined by r(n) = n for n\u003c=1 and for n\u003e1: r(n) = (sum of denominators of r(n-1) and r(n-2))/(sum of numerators of r(n-1) and r(n-2)).",
			"comment": [
				"r(n) = a(n) / A110091(n);",
				"a(n) = (A110091(n-1) + A110091(n-2))/GCD(a(n-1) + a(n-2), A110091(n-1) + A110091(n-2)).",
				"r --\u003e 1."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A110090/b110090.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,0,2,0,0,-1)."
			],
			"formula": [
				"a(3*k) = 2*k, a(3*k+1) = 1, a(3*k+2) = 2*(k+1).",
				"a(n) = (1/9)*(4*n + 5 + (4*n-4)*cos((2*n+1)*Pi/3) + sqrt(3)*sin(2*(n-1)*Pi/3) - sqrt(3)*sin(4*(n-1)*Pi/3)). - _Wesley Ivan Hurt_, Sep 25 2017",
				"From _Vincenzo Librandi_, Sep 27 2017: (Start)",
				"G.f.: x*(1 +2*x +2*x^2 -x^3)/((1-x)^2*(1+x+x^2)^2).",
				"a(n) = 2*a(n-3) - a(n-6) for n\u003e8. (End)"
			],
			"example": [
				"First terms of r: 0 1 2 2/3 1 4/3 4/5 1 6/5 6/7 1 8/7 ...:",
				"r(2) = (1+1)/(1+0) = 2,   r(3) = (1+1)/(2+1) = 2/3, r(4) = (3+1)/(2+2) = 1,",
				"r(5) = (1+3)/(1+2) = 4/3, r(6) = (3+1)/(4+1) = 4/5, ..."
			],
			"mathematica": [
				"Join[{0,1}, LinearRecurrence[{0,0,2,0,0,-1}, {2,2,1,4,4,1}, 100]] (* or *) CoefficientList[Series[x*(1+2*x+2*x^2-x^3)/((1-x)^2*(1+x+x^2)^2), {x,0,100}], x] (* _Vincenzo Librandi_, Sep 27 2017 *)"
			],
			"program": [
				"(MAGMA) I:=[0,1,2,2,1,4,4,1]; [n le 8 select I[n] else 2*Self(n-3)-Self(n-6): n in [1..100]]; // _Vincenzo Librandi_, Sep 27 2017",
				"(Sage)",
				"def A110090_list(prec):",
				"    P.\u003cx\u003e = PowerSeriesRing(ZZ, prec)",
				"    return P( x*(1+2*x+2*x^2-x^3)/(1-x^3)^2 ).list()",
				"A110090_list(100) # _G. C. Greubel_, Jun 16 2021"
			],
			"xref": [
				"Cf. A110091 (denominators)."
			],
			"keyword": "nonn,easy,frac",
			"offset": "0,3",
			"author": "_Reinhard Zumkeller_, Jul 14 2005",
			"references": 3,
			"revision": 22,
			"time": "2021-06-17T09:12:28-04:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}