{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006129",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6129,
			"id": "M3678",
			"data": "1,0,1,4,41,768,27449,1887284,252522481,66376424160,34509011894545,35645504882731588,73356937912127722841,301275024444053951967648,2471655539737552842139838345,40527712706903544101000417059892,1328579255614092968399503598175745633",
			"name": "a(0), a(1), a(2), ... satisfy Sum_{k=0..n} a(k)*binomial(n,k) = 2^binomial(n,2), for n \u003e= 0.",
			"comment": [
				"Also labeled graphs on n unisolated nodes (inverse binomial transform of A006125). - _Vladeta Jovovic_, Apr 09 2000",
				"Also the number of edge covers of the complete graph K_n. - _Eric W. Weisstein_, Mar 30 2017"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A006129/b006129.txt\"\u003eTable of n, a(n) for n = 0..50\u003c/a\u003e",
				"A. N. Bhavale, B. N. Waphare, \u003ca href=\"https://ajc.maths.uq.edu.au/pdf/78/ajc_v78_p073.pdf\"\u003eBasic retracts and counting of lattices\u003c/a\u003e, Australasian J. of Combinatorics (2020) Vol. 78, No. 1, 73-99.",
				"C. L. Mallows \u0026 N. J. A. Sloane, \u003ca href=\"/A006123/a006123.pdf\"\u003eEmails, May 1991\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/transforms.txt\"\u003eTransforms\u003c/a\u003e",
				"R. Tauraso, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL11/Tauraso/tauraso18.html\"\u003eEdge cover time for regular graphs\u003c/a\u003e, JIS 11 (2008) 08.4.4.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CompleteGraph.html\"\u003eComplete Graph\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/EdgeCover.html\"\u003eEdge Cover\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Sum_{k=0..n} (-1)^(n-k)*binomial(n, k)*2^binomial(k, 2).",
				"E.g.f.: A(x)/exp(x) where A(x) = Sum_{n\u003e=0} 2^C(n,2) x^n/n!. - _Geoffrey Critzer_, Oct 21 2011",
				"a(n) ~ 2^(n*(n-1)/2). - _Vaclav Kotesovec_, May 04 2015"
			],
			"example": [
				"2^binomial(n,2) = 1 + binomial(n,2) + 4*binomial(n,3) + 41*binomial(n,4) + 768*binomial(n,5) + ..."
			],
			"maple": [
				"a:= proc(n) option remember; `if`(n=0, 1,",
				"      2^binomial(n, 2) - add(a(k)*binomial(n,k), k=0..n-1))",
				"    end:",
				"seq(a(n), n=0..20);  # _Alois P. Heinz_, Oct 26 2012"
			],
			"mathematica": [
				"a = Sum[2^Binomial[n, 2] x^n/n!, {n, 0, 20}]; Range[0, 20]! CoefficientList[Series[a/Exp[x], {x, 0, 20}], x] (* _Geoffrey Critzer_, Oct 21 2011 *)",
				"Table[Sum[(-1)^(n - k) Binomial[n, k] 2^Binomial[k, 2], {k, 0, n}], {n, 0, 20}] (* _Vaclav Kotesovec_, May 04 2015 *)"
			],
			"program": [
				"(PARI) for(n=0,25, print1(sum(k=0,n,(-1)^(n-k)*binomial(n, k)*2^binomial(k, 2)), \", \")) \\\\ _G. C. Greubel_, Mar 30 2017",
				"(Python)",
				"from sympy.core.cache import cacheit",
				"from sympy import binomial",
				"@cacheit",
				"def a(n): return 1 if n==0 else 2**binomial(n, 2) - sum(a(k)*binomial(n, k) for k in range(n))",
				"print([a(n) for n in range(21)]) # _Indranil Ghosh_, Aug 12 2017"
			],
			"xref": [
				"Row sums of A054548."
			],
			"keyword": "nonn,nice,easy",
			"offset": "0,4",
			"author": "_Colin Mallows_",
			"ext": [
				"More terms from _Vladeta Jovovic_, Apr 09 2000"
			],
			"references": 73,
			"revision": 68,
			"time": "2021-04-15T21:38:47-04:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}