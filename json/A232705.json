{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A232705",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 232705,
			"data": "0,0,4,0,8,4,8,8,20,0,16,12,24,8,28,16,28,24,40,12,32,24,44,16,48,28,48,32,52,32,40,44,64,28,72,32,76,40,72,44,56,56,84,56,80,52,88,48,92,56,92,56,96,68,88,72,108,56,104,76,112,64,124,80,104,92,112,92,120,96,116,80,144,84",
			"name": "Number of Gaussian integers z satisfying (n-1)/2 \u003c |z| \u003c n/2.",
			"comment": [
				"Number of integer Cartesian grid points covered by a ring with width 1/2 and outer radius n/2.",
				"For symmetry reasons, a(n) is a multiple of 4.",
				"By bounds on the Gauss circle problem, a(n)/n -\u003e Pi/2 as n -\u003e infinity (see Wikipedia link). - _Robert Israel_ and _Peter Bala_, Mar 26 2020"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A232705/b232705.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Robert Israel, \u003ca href=\"/A232705/a232705.png\"\u003ePlot of a(n)/(n*Pi/2) for n=1..10000\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Gauss_circle_problem\"\u003eGauss circle problem\u003c/a\u003e",
				"\u003ca href=\"/index/Ga#gaussians\"\u003eIndex entries for Gaussian integers and primes\u003c/a\u003e"
			],
			"maple": [
				"N:= 100: # for a(1)..a(N)",
				"V:= Vector(N):",
				"for x from 1 to N/2 do",
				"  for y from 1 to x do",
				"    r:= 2*sqrt(x^2 + y^2);",
				"    if r::integer then next fi;",
				"    n:= ceil(r);",
				"    if n \u003e N then break fi;",
				"    if x = y then V[n]:= V[n]+4 else V[n]:= V[n]+8 fi",
				"od od:",
				"convert(V,list); # _Robert Israel_, Mar 26 2020"
			],
			"program": [
				"(PARI) a(n)=sum(i=-n,n,sum(j=-n,n,d=sqrt(i*i+j*j);if(d\u003e(n-1)/2\u0026\u0026d\u003cn/2,1)))"
			],
			"xref": [
				"Cf. A047077, A036705, A333462."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Ralf Stephan_, Nov 28 2013",
			"references": 5,
			"revision": 26,
			"time": "2020-03-27T06:39:24-04:00",
			"created": "2013-12-04T03:24:54-05:00"
		}
	]
}