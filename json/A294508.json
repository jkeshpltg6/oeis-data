{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A294508",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 294508,
			"data": "0,1,1,2,1,0,2,2,1,2,3,1,0,2,0,3,2,1,3,1,2,4,2,0,1,-1,1,-1,4,2,1,3,0,3,0,2,4,3,1,3,2,4,2,4,6,4,4,2,4,3,5,3,6,8,9,5,3,1,4,1,3,1,3,5,9,5,5,4,1,5,2,5,3,4,8,10,7,9,6,3,0,3,0,3,0,3,6,7,4,6,3,6,3,1,4,1,5,1,5,6,10,6,9,6,8",
			"name": "Regular triangular array read by rows: T(n,m) = pi(n*m) - pi(n)*pi(m) for n \u003e 0 and 0 \u003c m \u003c= n.",
			"comment": [
				"Inspired by A291440.",
				"Mincu and Panaitopol (2008) prove that pi(m*n) \u003e= pi(m)*i(n) for all positive m and n except for m = 5, n = 7; m = 7, n = 5; and m = n = 7.",
				"a(i) = -1 for i = 26 and 28, when n = 7 and m = either 5 or 7.",
				"a(i) = 0 for i = 1, 6, 13, 15, 24, 33, 35, 81, 83, 85, 174, 176, 178; when n=m=1; n=m=3; n=5 and m is either 3 or 5; n=7 and m=3; n=8 and m is either 5 or 7; n=13 and m is either 3, 5, or 7; and n=19 with m being either 3, 5 or 7.",
				"First occurrence of k = -1, 0, 1, 2, .., 20, 21, etc. occurs at i = 26, 1, 2, 4, 11, 22, 51, 45, 77, 54, 55, 76, 115, 120, 130, 187, 168, 135, 171, 136, 169, 274, etc.",
				"Last occurrence of k \u003e= -1 occurs at i = 28, 178, 260, 499, 906, 1179, 2704, 2778, 3406, 6558, 6673, 6789, 7024, 9594, 9733, 10156, 11479, 19704, 19903, 20304, 20709, 20913, etc.",
				"Conjecture: min_{1\u003c=m\u003c=n} T(n,m) \u003c= T(n,M) for all M \u003e n if n \u003c\u003e 5."
			],
			"link": [
				"Gabriel Mincu and Laurentiu Panaitopol, \u003ca href=\"https://www.emis.de/journals/JIPAM/article951.html\"\u003eProperties of some functions connected to prime numbers\u003c/a\u003e, J. Inequal. Pure Appl. Math., 9 No. 1 (2008), Art. 12."
			],
			"formula": [
				"a(n*(n+1)/2) = T(n,n) = A291440(n).",
				"min_{1\u003c=m\u003c=n} a(n*(n-1)/2 + m) = min_{1\u003c=m\u003c=n} T(n,m) = A294509(n)."
			],
			"example": [
				"a(19) = 3 since 19 = 5*6/2 + 4, so the 19th term is T(6,4) = pi(6*4) - pi(6)*pi(4) = 9 - 3*2 = 3.",
				"Triangular array begins:",
				"   n\\ m  1  2  3  4  5  6  7  8  9 10 11 12 13 14",
				"   1  0",
				"   2  1  1",
				"   3  2  1  0",
				"   4  2  2  1  2",
				"   5  3  1  0  2  0",
				"   6  3  2  1  3  1  2",
				"   7  4  2  0  1 -1  1 -1",
				"   8  4  2  1  3  0  3  0  2",
				"   9  4  3  1  3  2  4  2  4  6",
				"  10  4  4  2  4  3  5  3  6  8  9",
				"  11  5  3  1  4  1  3  1  3  5  9  5",
				"  12  5  4  1  5  2  5  3  4  8 10  7  9",
				"  13  6  3  0  3  0  3  0  3  6  7  4  6  3",
				"  14  6  3  1  4  1  5  1  5  6 10  6  9  6  8",
				"  15  6  4  2  5  3  6  3  6  8 11  8 11  8 10 12"
			],
			"mathematica": [
				"t[n_, m_] := PrimePi[n*m] - PrimePi[n]*PrimePi[m]; Table[ t[n, m], {n, 13}, {m, n}] // Flatten"
			],
			"program": [
				"(PARI) T(n,m) = primepi(n*m) - primepi(n)*primepi(m);",
				"tabl(nn) = for (n=1, nn, for (m=1, n, print1(T(n,m), \", \")); print); \\\\ _Michel Marcus_, Nov 08 2017"
			],
			"xref": [
				"Cf. A000720, A291440, A294509."
			],
			"keyword": "sign,tabl",
			"offset": "1,4",
			"author": "_Jonathan Sondow_ and _Robert G. Wilson v_, Nov 06 2017",
			"references": 2,
			"revision": 15,
			"time": "2017-11-08T11:19:32-05:00",
			"created": "2017-11-08T02:27:32-05:00"
		}
	]
}