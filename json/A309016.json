{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A309016",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 309016,
			"data": "1,2,6,12,24,72,144,288,864,1728,5184,10368,20736,62208,124416,373248,746496,1492992,4478976,8957952,26873856,53747712,107495424,322486272,644972544,1289945088,3869835264,7739670528,23219011584,46438023168,92876046336,278628139008,557256278016",
			"name": "Superior 2-highly composite numbers: 3-smooth numbers (A003586) k for which there is a real number e \u003e 0 such that d(k)/k^e \u003e= d(j)/j^e for all 3-smooth numbers j, where d(k) is the number of divisors of k (A000005).",
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A309016/b309016.txt\"\u003eTable of n, a(n) for n = 1..2709\u003c/a\u003e",
				"Gérard Bessi, \u003ca href=\"https://eudml.org/doc/181966\"\u003eEtude des nombres 2-hautement composés\u003c/a\u003e, Séminaire de Théorie des nombres de Bordeaux, Vol. 4 (1975), pp. 1-22.",
				"Michael De Vlieger, \u003ca href=\"/A309016/a309016.txt\"\u003eFactors p analogous to A000705 such that the product of the smallest n terms equals a(n + 1)\u003c/a\u003e (10^5 terms)."
			],
			"example": [
				"From _Michael De Vlieger_, Jul 12 2019: (Start)",
				"We can plot all terms in A003586 with the power range 2^x with x \u003e= 0 and 3^y with y \u003e= 0 on the x and y axis, respectively. Plot of terms m in A309015, with terms also in a(n) placed in brackets:",
				"                                2^x",
				"          0    1     2     3     4     5     6     7     8",
				"        +-----------------------------------------------------",
				"     0  |[1]  [2]    4",
				"     1  |     [6]  [12]  [24]   48",
				"3^y  2  |           36   [72] [144]  [288]   576",
				"     3  |                216   432   [864] [1728] 3456  6912 ...",
				"          ...",
				"Larger scale plot with \".\" representing a term m in A309015, and \"o\" representing a term in A309015 also in a(n) for all m \u003c A002110(20).",
				"                              2^x",
				"        0    5   10   15   20   25   30   35   40   45  ...",
				"        +------------------------------------------------",
				"       0|oo.",
				"        | ooo.",
				"        |  .ooo.",
				"        |   ..oo..",
				"        |    ..ooo..",
				"       5|      ..oo...",
				"        |       ..ooo...",
				"        |         ..oo....",
				"        |          ..ooo....",
				"        |            ..ooo....",
				"      10|             ...oo.....",
				"        |               ..ooo....",
				"        |                ...oo.....",
				"        |                  ..ooo.....",
				"3^y     |                   ...ooo....",
				"      15|                     ...oo.....",
				"        |                      ...ooo.....",
				"        |                        ...oo.....",
				"        |                         ...ooo.....",
				"        |                           ...oo......",
				"      20|                            ...ooo.....",
				"        |                              ...ooo.....",
				"        |                               ....oo......",
				"        |                                 ...ooo.....",
				"        |                                  ....oo......",
				"      25|                                    ...ooo......",
				"        |                                     ....ooo....",
				"        |                                       ....oo.",
				"        |                                        ....o",
				"        |                                          .",
				"     ...",
				"(End)"
			],
			"mathematica": [
				"f[nn_, k_: 2] := Block[{w = {{2, 1}, {3, 0}}, s = {2}, P = 1, q = k - 2, x, i, n, f}, f[w_List] := Log[#1, (#2 + 2)/(#2 + 1)] \u0026 @@ w; x = Array[f[w[[#]] ] \u0026, P + 1]; For[n = 2, n \u003c= nn, n++, i = First@ FirstPosition[x, Max[x]]; AppendTo[s, w[[i, 1]]]; w[[i, 2]]++; If[And[i \u003e P, P \u003c= q], P++; AppendTo[w, {Prime[i + 1], 0}]; AppendTo[x, f[Last@ w]]]; x[[i]] = f@ w[[i]] ]; s]; {1}~Join~FoldList[Times, f[32, 2]] (* _Michael De Vlieger_, Jul 11 2019, after _T. D. Noe_ at A000705 *)"
			],
			"xref": [
				"Subsequence of A003586 and A309015.",
				"Cf. A000005, A002201."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Amiram Eldar_, Jul 06 2019",
			"ext": [
				"More terms from _Michael De Vlieger_, Jul 11 2019"
			],
			"references": 2,
			"revision": 13,
			"time": "2019-07-13T10:14:06-04:00",
			"created": "2019-07-11T19:14:39-04:00"
		}
	]
}