{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A118306",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 118306,
			"data": "1,3,2,9,7,15,5,27,4,21,13,45,11,33,6,81,19,75,17,63,10,39,29,135,49,51,8,99,23,105,37,243,14,57,77,225,31,69,22,189,43,165,41,117,12,87,53,405,25,147,26,153,47,375,91,297,34,93,61,315,59,111,20,729,119,195,71",
			"name": "If n = product(k\u003e=1} p(k)^b(n,k), where p(k) is the k-th prime and where each b(n,k) is a nonnegative integer, then: If n occurs earlier in the sequence, then a(n) = product{k\u003e=2} p(k-1)^b(n,k); If n does not occur earlier in the sequence, then a(n) = product{k\u003e=1} p(k+1)^b(n,k).",
			"comment": [
				"Sequence is a permutation of the positive integers and it is its own inverse permutation.",
				"From _Antti Karttunen_, Nov 05 2016: (Start)",
				"A016945 gives the positions of even terms.",
				"A007310 is closed with respect to this permutation. See A277911 for the permutation induced.",
				"A029744 (without 3) seems to give the positions of records in this sequence (note that it gives the record positions in related A003961 and A048673) which implies that A083658 (without its term 5) would then give the record values.",
				"(End)"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A118306/b118306.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_OEIS:_Section_Pri#prime_indices\"\u003eIndex entries for sequences computed from prime indices\u003c/a\u003e"
			],
			"formula": [
				"From _Antti Karttunen_, Nov 05 2016: (Start)",
				"a(1) = 1; and for n \u003e 1, if n = a(k) for some k = 1 .. n-1, then a(n) = A064989(n), otherwise a(n) = A003961(n). [After the original definition and _R. J. Mathar_'s Maple-code]",
				"a(1) = 1, and for n \u003e 1, if A055396(n) is odd, a(n) = A003961(n), otherwise a(n) = A064989(n). [The above reduces to this.]",
				"a(n) = product{k\u003e=1} prime(k-((-1)^A055396(n)))^e(k) when n = product{k\u003e=1} prime(k)^e(k).",
				"a(2n) = A249734(n) and a(A249734(n)) = 2n.",
				"A126760(a(A007310(n))) = A277911(n).",
				"For n \u003e 1, A055396(a(n)) = A055396(n) - (-1)^A055396(n). [Permutation sends the terms on any odd row of A246278 to the next even row just below, and vice versa.]",
				"A246277(a(n)) = A246277(n). [While keeping them in the same column.]",
				"a(n) = A064989(A064989(a(A003961(A003961(n))))).",
				"(End)"
			],
			"maple": [
				"A064989 := proc(n) local a,ifs,p ; a := 1 ; ifs := ifactors(n)[2] ; for p in ifs do if op(1,p) \u003e 2 then a := a* prevprime(op(1,p))^op(2,p) ; fi ; od; RETURN(a) ; end: A003961 := proc(n) local a,ifs,p ; a := 1 ; ifs := ifactors(n)[2] ; for p in ifs do a := a* nextprime(op(1,p))^op(2,p) ; od; RETURN(a) ; end: A118306 := proc(nmin) local a,anxt,i,n ; a := [1] ; while nops(a) \u003c nmin do n := nops(a)+1 ; if n in a then anxt := A064989(n) ; else anxt := A003961(n) ; fi ; a := [op(a),anxt] ; od; a ; end: A118306(100) ; # _R. J. Mathar_, Sep 06 2007"
			],
			"program": [
				"(PARI)",
				"A118306(n) = { if(1==n, 1, my(f = factor(n)); my(d = (-1)^primepi(f[1, 1])); for(i=1, #f~, f[i, 1] = prime(primepi(f[i, 1])-d)); factorback(f)); }; \\\\ _Antti Karttunen_, Nov 06 2016",
				"for(n=1, 10001, write(\"b118306.txt\", n, \" \", A118306(n)));",
				"(Scheme) (define (A118306 n) (cond ((= 1 n) n) ((odd? (A055396 n)) (A003961 n)) (else (A064989 n)))) ;; _Antti Karttunen_, Nov 05 2016"
			],
			"xref": [
				"Cf. A003961, A064989, A007310, A016945, A029744, A048673, A055396, A083221, A083658, A246277, A246278, A249734, A277911."
			],
			"keyword": "nonn,look",
			"offset": "1,2",
			"author": "_Leroy Quet_, May 14 2006",
			"ext": [
				"More terms from _R. J. Mathar_, Sep 06 2007",
				"A small omission in the definition corrected by _Antti Karttunen_, Nov 05 2016"
			],
			"references": 5,
			"revision": 59,
			"time": "2016-11-07T09:10:03-05:00",
			"created": "2006-05-19T03:00:00-04:00"
		}
	]
}