{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A081405",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 81405,
			"data": "1,1,3,4,15,24,105,192,945,1920,10395,23040,135135,322560,2027025,5160960,34459425,92897280,654729075,1857945600,13749310575,40874803200,316234143225,980995276800,7905853580625,25505877196800,213458046676875",
			"name": "  a(n) = (n+1)*a(n-2) with a(0) = a(1) = 1.",
			"comment": [
				"A001147 and A002866 combined."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A081405/b081405.txt\"\u003eTable of n, a(n) for n = 0..790\u003c/a\u003e"
			],
			"formula": [
				"a(0)=a(1)=1; a(2n) = A001147(2*n-2) odd terms, double factorial numbers; a(2n-1) = A002866(n) = 2^(n-1)*n!",
				"0 = a(n)*(a(n+1) - a(n+3)) + a(n+1)*a(n+2) if n\u003e=0. - _Michael Somos_, Jan 24 2014",
				"a(n) = (n-1)-st term of column 1 of the array at A249159, for n \u003e= 0. - _Clark Kimberling_, Oct 23 2014"
			],
			"example": [
				"G.f. = 1 + x + 3*x^2 + 4*x^3 + 15*x^4 + 24*x^5 + 105*x^6 + 192*x^7 + ..."
			],
			"maple": [
				"a[0]:=1:a[1]:=1:for n from 2 to 50 do a[n]:=(a[n-2]*(n+1)^2) od: seq(sqrt(a[n]), n=0..26); # _Zerinvary Lajos_, Mar 04 2008"
			],
			"mathematica": [
				"f[n_]:= (n+1)*f[n-2]; f[0] = 1; f[1] = 1; Table[f[n], {n, 1, 30}]",
				"a[ n_]:= If[ n \u003c 0, 0, If[OddQ[n], 2^((n-1)/2) ((n+1)/2)!, (n+1)!!]]; (* _Michael Somos_, Jan 24 2014 *)",
				"RecurrenceTable[{a[0]==a[1]==1,a[n]==(n+1)a[n-2]},a,{n,30}] (* _Harvey P. Dale_, Nov 05 2021 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c2, n\u003e=0, (n+1) * a(n-2))}; /* _Michael Somos_, Jan 24 2014 */",
				"(PARI) {a(n) = if( n\u003c0, 0, if( n%2, 2^(n\\2) * (n\\2 + 1)!, (n+1)! / (2^(n\\2) * (n\\2)!)))}; /* _Michael Somos_, Jan 24 2014 */",
				"(MAGMA) [n le 1 select 1 else (n+1)*Self(n-1): n in [0..30]]; // _Vincenzo Librandi_, Oct 26 2014",
				"(Sage)",
				"def a(n):",
				"    if n\u003c2: return 1",
				"    else: return (n+1)*a(n-2)",
				"[a(n) for n in (0..30)] # _G. C. Greubel_, Aug 24 2019",
				"(GAP)",
				"a:= function(n)",
				"    if n\u003c2 then return 1;",
				"    else return (n+1)*a(n-2);",
				"    fi;",
				"  end;",
				"List([0..30], n-\u003e a(n) ); # _G. C. Greubel_, Aug 24 2019"
			],
			"xref": [
				"Cf. A000142, A001147, A002866."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Labos Elemer_, Apr 01 2003",
			"references": 6,
			"revision": 34,
			"time": "2021-11-05T16:00:33-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}