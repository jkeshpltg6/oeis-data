{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001845",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1845,
			"id": "M4384 N1844",
			"data": "1,7,25,63,129,231,377,575,833,1159,1561,2047,2625,3303,4089,4991,6017,7175,8473,9919,11521,13287,15225,17343,19649,22151,24857,27775,30913,34279,37881,41727,45825,50183,54809,59711,64897,70375,76153,82239",
			"name": "Centered octahedral numbers (crystal ball sequence for cubic lattice).",
			"comment": [
				"Number of points in simple cubic lattice at most n steps from origin.",
				"If X is an n-set and Y_i (i=1,2,3) mutually disjoint 2-subsets of X then a(n-6) is equal to the number of 6-subsets of X intersecting each Y_i (i=1,2,3). - _Milan Janjic_, Aug 26 2007",
				"Equals binomial transform of [1, 6, 12, 8, 0, 0, 0, ...] where (1, 6, 12, 8) = row 3 of the Chebyshev triangle A013609. - _Gary W. Adamson_, Jul 19 2008",
				"Let A be the Hessenberg matrix of order n, defined by: A[1,j]=1, A[i,i]:=2,(i\u003e1), A[i,i-1]=-1, and A[i,j]=0 otherwise. Then, for n \u003e= 4, a(n-2) = -coeff(charpoly(A,x),x^(n-3)). - _Milan Janjic_, Jan 26 2010",
				"a(n) = A005408(n) * A097080(n-1) / 3. - _Reinhard Zumkeller_, Dec 15 2013",
				"a(n) = D(3,n) where D are the Delannoy numbers (A008288). As such, a(n) gives the number of grid paths from (0,0) to (3,n) using steps that move one unit north, east, or northeast. - _David Eppstein_, Sep 07 2014"
			],
			"reference": [
				"L. Comtet, Advanced Combinatorics, Reidel, 1974, p. 81.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A001845/b001845.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Luciano Ancora, \u003ca href=\"https://upload.wikimedia.org/wikipedia/commons/9/9c/FigurateN.pdf\"\u003eThe Square Pyramidal Number and other figurate numbers\u003c/a\u003e, ch. 4.",
				"Bela Bajnok, \u003ca href=\"https://arxiv.org/abs/1705.07444\"\u003eAdditive Combinatorics: A Menu of Research Problems\u003c/a\u003e, arXiv:1705.07444 [math.NT], May 2017. See Section 2.3.",
				"D. Bump, K. Choi, P. Kurlberg, and J. Vaaler, \u003ca href=\"http://www.cecm.sfu.ca/~choi/paper/lrh.pdf\"\u003eA local Riemann hypothesis, I\u003c/a\u003e pages 16 and 17",
				"J. H. Conway and N. J. A. Sloane, \u003ca href=\"https://doi.org/10.1098/rspa.1997.0126\"\u003eLow-Dimensional Lattices VII: Coordination Sequences\u003c/a\u003e, Proc. Royal Soc. London, A453 (1997), 2369-2389 (\u003ca href=\"http://neilsloane.com/doc/Me220.pdf\"\u003epdf\u003c/a\u003e).",
				"Milan Janjic, \u003ca href=\"http://www.pmfbl.org/janjic/\"\u003eTwo Enumerative Functions\u003c/a\u003e",
				"M. Janjic, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL13/Janjic/janjic33.html\"\u003eHessenberg Matrices and Integer Sequences \u003c/a\u003e, J. Int. Seq. 13 (2010) # 10.7.8.",
				"Milan Janjić, \u003ca href=\"https://arxiv.org/abs/1905.04465\"\u003eOn Restricted Ternary Words and Insets\u003c/a\u003e, arXiv:1905.04465 [math.CO], 2019.",
				"G. Kreweras, \u003ca href=\"http://www.numdam.org/item?id=BURO_1973__20__3_0\"\u003eSur les hiérarchies de segments\u003c/a\u003e, Cahiers Bureau Universitaire Recherche Opérationnelle, Cahier 20, Inst. Statistiques, Univ. Paris, 1973.",
				"G. Kreweras, \u003ca href=\"/A001844/a001844.pdf\"\u003eSur les hiérarchies de segments\u003c/a\u003e, Cahiers du Bureau Universitaire de Recherche Opérationnelle, Institut de Statistique, Université de Paris, #20 (1973). (Annotated scanned copy)",
				"T. P. Martin, \u003ca href=\"http://dx.doi.org/10.1016/0370-1573(95)00083-6\"\u003eShells of atoms\u003c/a\u003e, Phys. Reports, 273 (1996), 199-241, eq. (10).",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"R. G. Stanton and D. D. Cowan, \u003ca href=\"http://dx.doi.org/10.1137/1012049\"\u003eNote on a \"square\" functional equation\u003c/a\u003e, SIAM Rev., 12 (1970), 277-279.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/HauyConstruction.html\"\u003eHauy Construction\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/OctahedralNumber.html\"\u003eOctahedral Number\u003c/a\u003e",
				"\u003ca href=\"/index/Cor#crystal_ball\"\u003eIndex entries for crystal ball sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-6,4,-1)."
			],
			"formula": [
				"G.f.: (1+x)^3 /(1-x)^4. [conjectured (correctly) by _Simon Plouffe_ in his 1992 dissertation]",
				"a(n) = (2*n+1)*(2*n^2 + 2*n + 3)/3.",
				"First differences of A014820(n). - _Alexander Adamchuk_, May 23 2006",
				"a(n) = a(n-1) + 4*n^2 + 2, a(0)=1. - _Vincenzo Librandi_, Mar 27 2011",
				"a(n) = 4*a(n-1) - 6*a(n-2) + 4*a(n-3) - a(n-4), with a(0)=1, a(1)=7, a(2)=25, a(3)=63. - _Harvey P. Dale_, Jun 05 2013",
				"a(n) = Sum_{k=0..min(3,n)} 2^k * binomial(3,k) * binomial(n,k). See Bump et al. - _Tom Copeland_, Sep 05 2014",
				"From _Luciano Ancora_, Jan 08 2015: (Start)",
				"a(n) = 2 * A000330(n) + A000330(n+1) + A000330(n-1).",
				"a(n) = A005900(n) + A005900(n+1).",
				"a(n) = A005900(n) + A000330(n) + A000330(n+1).",
				"a(n) = A000330(n-1) + A000330(n) + A005900(n+1).",
				"(End)",
				"a(n) = A002412(n+1) + A016061(n-1) for n \u003e 0. - _Bruce J. Nicholson_, Nov 12 2017"
			],
			"mathematica": [
				"Table[(4 n^3 - 6 n^2 + 8 n - 3)/3, {n, 100}] (* _Vladimir Joseph Stephan Orlovsky_, Jan 15 2011 *)",
				"LinearRecurrence[{4, -6, 4, -1}, {1, 7, 25, 63}, 40] (* _Harvey P. Dale_, Jun 05 2013 *)",
				"CoefficientList[Series[(1 + x)^3/(-1 + x)^4, {x, 0, 20}], x] (* _Eric W. Weisstein_, Sep 27 2017 *)"
			],
			"program": [
				"(PARI) a(n)=(2*n+1)*(2*n^2+2*n+3)/3 \\\\ _Charles R Greathouse IV_, Dec 06 2011",
				"(Haskell)",
				"a001845 n = (2 * n + 1) * (2 * n ^ 2 + 2 * n + 3) `div` 3",
				"-- _Reinhard Zumkeller_, Dec 15 2013"
			],
			"xref": [
				"Sums of 2 consecutive terms give A008412.",
				"(1/12)*t*(2*n^3 - 3*n^2 + n) + 2*n - 1 for t = 2, 4, 6, ... gives A049480, A005894, A063488, A001845, A063489, A005898, A063490, A057813, A063491, A005902, A063492, A005917, A063493, A063494, A063495, A063496.",
				"Partial sums of A005899.",
				"Cf. A001846, A001847, A001848, etc., A014820, A013609.",
				"Cf. also A240876, A002412, A016061, A005899.",
				"The 28 uniform 3D tilings: cab: A299266, A299267; crs: A299268, A299269; fcu: A005901, A005902; fee: A299259, A299265; flu-e: A299272, A299273; fst: A299258, A299264; hal: A299274, A299275; hcp: A007899, A007202; hex: A005897, A005898; kag: A299256, A299262; lta: A008137, A299276; pcu: A005899, A001845; pcu-i: A299277, A299278; reo: A299279, A299280; reo-e:  A299281, A299282; rho: A008137, A299276; sod: A005893, A005894; sve: A299255, A299261; svh: A299283, A299284; svj: A299254, A299260; svk: A010001, A063489; tca: A299285, A299286; tcd: A299287, A299288; tfs: A005899, A001845; tsi: A299289, A299290; ttw: A299257, A299263; ubt: A299291, A299292; bnn: A007899, A007202. See the Proserpio link in A299266 for overview."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 87,
			"revision": 144,
			"time": "2021-03-12T22:32:35-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}