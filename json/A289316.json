{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A289316",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 289316,
			"data": "1,1,2,8,37,219,1557,12994,124427,1344506,16178891,214522339,3107144562,48805300668,826268787588,14998055299920,290550119360174,5983278021430064,130512410617529321,3006012061455129053,72900477505718600661",
			"name": "The number of upper-triangular matrices whose nonzero entries are positive odd numbers summing to n and each row contains a nonzero entry.",
			"comment": [
				"A row-Fishburn matrix of size n is defined to be an upper-triangular matrix with nonnegative integer entries which sum to n and each row contains a nonzero entry. See A158691. Here we are considering row-Fishburn matrices where the nonzero entries are all odd.",
				"The g.f. F(x) for primitive row_Fishburn matrices (i.e., row_Fishburn matrices with entries restricted to the set {0,1}), is F(x) = Sum_{n\u003e=0} Product_{k=1..n} ( (1 + x)^k - 1 ). See A179525. Let C(x) = x/(1 - x^2) = x + x^3 + x^5 + x^7 + .... Then appplying Lemma 2.2.22 of Goulden and Jackson gives the g.f. for the present sequence as the composition F(C(x))."
			],
			"reference": [
				"I. P. Goulden and D. M. Jackson, Combinatorial Enumeration, Wiley, N.Y., 1983, p. 42."
			],
			"link": [
				"Hsien-Kuei Hwang, Emma Yu Jin, \u003ca href=\"https://arxiv.org/abs/1911.06690\"\u003eAsymptotics and statistics on Fishburn matrices and their generalizations\u003c/a\u003e, arXiv:1911.06690 [math.CO], 2019."
			],
			"formula": [
				"G.f.: A(x) = Sum_{n \u003e= 0} Product_{k = 1..n} ( (1 + x/(1 - x^2))^k - 1 )."
			],
			"example": [
				"a(3) = 8: The eight row-Fishburn matrices of size 3 with odd nonzero entries are",
				"(3) /1 1\\",
				"    \\0 1/",
				"/1 0 0\\  /0 1 0\\  /0 0 1\\",
				"|0 1 0|  |0 1 0|  |0 1 0|",
				"\\0 0 1/  \\0 0 1/  \\0 0 1/",
				"/1 0 0\\  /0 1 0\\  /0 0 1\\",
				"|0 0 1|  |0 0 1|  |0 0 1|",
				"\\0 0 1/  \\0 0 1/  \\0 0 1/"
			],
			"maple": [
				"C:= x -\u003e x/(1 - x^2):",
				"G:= add(mul( (1 + C(x))^k - 1, k=1..n),n=0..20):",
				"S:= series(G,x,21):",
				"seq(coeff(S,x,j),j=0..20);"
			],
			"xref": [
				"Cf. A158691, A179525, A289312, A289313, A289314, A289315, A289317."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Peter Bala_, Jul 24 2017",
			"references": 1,
			"revision": 9,
			"time": "2020-02-07T13:40:19-05:00",
			"created": "2017-07-25T12:06:03-04:00"
		}
	]
}