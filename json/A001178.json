{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001178",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1178,
			"id": "M3207 N1298",
			"data": "0,4,3,2,3,1,2,2,1,2,3,1,3,2,3,1,2,1,2,2,2,2,2,0,3,3,2,2,3,1,2,2,3,2,2,1,3,2,3,2,3,2,3,2,1,2,3,1,3,2,2,3,3,2,3,2,2,3,4,1,2,2,2,3,3,1,3,2,2",
			"name": "Fibonacci frequency of n.",
			"comment": [
				"a(A235702(n)) = 0. - _Reinhard Zumkeller_, Jan 15 2014",
				"a(n) is the least nonnegative integer k such that the function iterates f: {1, 2, ...} -\u003e {1, 2, ...}, n -\u003e f(n) = A001175(n), satisfy f^[k+1](n) = f^[k](n), where f^[0] is the identity map f^[0](n) = n and f^[k+1] = f o f^[k]. See the Fulton and Morris link, where the function f is called pi and a(n)= omega(n) for n \u003e= 2, and omega(24) should be 0. (see the Zumkeller remark on the Hannon and Morris reference) - _Wolfdieter Lang_, Jan 18 2015"
			],
			"reference": [
				"B. H. Hannon and W. L. Morris, Tables of Arithmetical Functions Related to the Fibonacci Numbers. Report ORNL-4261, Oak Ridge National Laboratory, Oak Ridge, Tennessee, Jun 1968. [There is a typo in the value of a(24) given in the table on the last page.]",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A001178/b001178.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"D. Fulton and W. L. Morris, \u003ca href=\"http://matwbn.icm.edu.pl/ksiazki/aa/aa16/aa1621.pdf\"\u003eOn arithmetical functions related to the Fibonacci numbers\u003c/a\u003e, Acta Arithmetica, 16 (1969), 105-110.",
				"B. H. Hannon and W. L. Morris, \u003ca href=\"/A001175/a001175.pdf\"\u003eTables of Arithmetical Functions Related to the Fibonacci Numbers\u003c/a\u003e [Annotated and scanned copy]",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Pisano_period\"\u003ePisano period\u003c/a\u003e"
			],
			"formula": [
				"See a comment above and the program."
			],
			"mathematica": [
				"pi[1] = 1;",
				"pi[n_] := For[k = 1, True, k++, If[Mod[Fibonacci[k], n] == 0 \u0026\u0026 Mod[ Fibonacci[k+1], n] == 1, Return[k]]];",
				"a[n_] := Length[FixedPointList[pi, n]] - 2;",
				"a /@ Range[100] (* _Jean-François Alcover_, Oct 28 2019 *)"
			],
			"program": [
				"(Haskell)",
				"a001178 = f 0 where",
				"   f j x = if x == y then j else f (j + 1) y  where y = a001175 x",
				"-- _Reinhard Zumkeller_, Jan 15 2014"
			],
			"xref": [
				"Cf. A001175, A235702."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"a(24) corrected by _Reinhard Zumkeller_, Jan 15 2014"
			],
			"references": 4,
			"revision": 33,
			"time": "2021-12-19T09:55:53-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}