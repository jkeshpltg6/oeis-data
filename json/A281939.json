{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A281939",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 281939,
			"data": "1,2,3,2,1,2,2,2,2,3,5,2,1,4,3,3,3,3,6,1,1,4,1,2,2,3,7,5,3,3,3,4,3,4,8,3,2,4,3,4,5,7,10,2,1,7,1,2,5,2,7,4,3,4,2,3,3,3,7,4,4,3,3,6,1,5,12,4,1,4,4,3,4,5,8,4,3,4,4,3,5",
			"name": "Number of ways to write n as x^2 + y^2 + z^2 + w^2 with x - y and 3*z + w both squares, where x,y,z are nonnegative integers and w is an integer.",
			"comment": [
				"Conjecture: (i) a(n) \u003e 0 for all n = 0,1,2,....",
				"(ii) Any nonnegative integer n can be written as x^2 + y^2 + z^2 + w^2 with |2*x-y| and 3*z+2*w both squares, where x,y,z are nonnegative integers and w is an integer.",
				"(iii) Any nonnegative integer n can be written as x^2 + y^2 + z^2 + w^2 with x+2*y a square and z+2*w twice a square, where x,y,z,w are integers.",
				"(iv) For each k = 1,3, every nonnegative integer n can be written as x^2 + y^2 + z^2 + w^2 with x+k*y and z+5*w both squares, where x,y,z,w are integers.",
				"(v) Any nonnegative integer n can be written as x^2 + y^2 + z^2 + w^2 with x+2*y and 6*z+2*w both squares, where x,y,z,w are integers."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A281939/b281939.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2016.11.008\"\u003eRefining Lagrange's four-square theorem\u003c/a\u003e, J. Number Theory 175(2017), 167-190."
			],
			"example": [
				"a(4) = 1 since 4 = 1^2 + 1^2 + 1^2 + 1^2 with 1 - 1 = 0^2 and 3*1 + 1 = 2^2.",
				"a(12) = 1 since 12 = 1^2 + 1^2 + 1^2 + (-3)^2 with 1 - 1 = 0^2 and 3*1 + (-3) = 0^2.",
				"a(19) = 1 since 19 = 3^2 + 3^2 + 0^2 + 1^2 with 3 - 3 = 0^2 and 3*0 + 1 = 1^2.",
				"a(20) = 1 since 20 = 3^2 + 3^2 + 1^2 + 1^2 with 3 - 3 = 0^2 and 3*1 + 1 = 2^2.",
				"a(22) = 1 since 22 = 3^2 + 2^2 + 3^2 + 0^2 with 3 - 2 = 1^2 and 3*3 + 0 = 3^2.",
				"a(44) = 1 since 44 = 3^2 + 3^2 + 5^2 + 1^2 with 3 - 3 = 0^2 and 3*5 + 1 = 4^2.",
				"a(46) = 1 since 46 = 5^2 + 4^2 + 1^2 + (-2)^2 with 5 - 4 = 1^2 and 3*1 + (-2) = 1^2.",
				"a(68) = 1 since 68 = 7^2 + 3^2 + 1^2 + (-3)^2 with 7 - 3 = 2^2 and 3*1 + (-3) = 0^2.",
				"a(212) = 1 since 212 = 5^2 + 5^2 + 9^2 + 9^2 with 5 - 5 = 0^2 and 3*9 + 9 = 6^2.",
				"a(1144) = 1 since 1144 = 20^2 + 16^2 + 22^2 + (-2)^2 with 20 - 16 = 2^2 and 3*22 + (-2) = 8^2."
			],
			"mathematica": [
				"SQ[n_]:=SQ[n]=IntegerQ[Sqrt[n]];",
				"Do[r=0;Do[If[SQ[n-x^2-y^2-z^2]\u0026\u0026SQ[x-y]\u0026\u0026SQ[3z+(-1)^k*Sqrt[n-x^2-y^2-z^2]],r=r+1],{y,0,Sqrt[n/2]},{x,y,Sqrt[n-y^2]},{z,0,Sqrt[n-x^2-y^2]},{k,0,Min[Sqrt[n-x^2-y^2-z^2],1]}]; Print[n,\" \",r];Continue,{n,0,80}]"
			],
			"xref": [
				"Cf. A000118, A000290, A271775, A281941."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Zhi-Wei Sun_, Feb 02 2017",
			"references": 13,
			"revision": 7,
			"time": "2017-02-03T02:57:04-05:00",
			"created": "2017-02-03T02:57:04-05:00"
		}
	]
}