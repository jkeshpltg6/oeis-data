{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A123655",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 123655,
			"data": "1,2,4,8,14,24,40,64,101,156,236,352,518,752,1080,1536,2162,3018,4180,5744,7840,10632,14328,19200,25591,33932,44776,58816,76918,100176,129952,167936,216240,277476,354864,452392,574958,728568,920600,1160064",
			"name": "Expansion of q * psi(q^8) / phi(-q) in powers of q where psi(), phi() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: phi(q) (A000122), psi(q) (A010054).",
				"Number 12 of the 14 eta-quotients listed in Table 2 of Moy 2013. - _Michael Somos_, Sep 19 2013"
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A123655/b123655.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Kevin Acres, David Broadhurst, \u003ca href=\"https://arxiv.org/abs/1810.07478\"\u003eEta quotients and Rademacher sums\u003c/a\u003e, arXiv:1810.07478 [math.NT], 2018. See Table 1 p. 10.",
				"Richard Moy, \u003ca href=\"http://arxiv.org/abs/1309.4320\"\u003eCongruences among power series coefficients of modular forms\u003c/a\u003e, arXiv:1309.4320",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of eta(q^2) * eta(q^16)^2 / (eta(q)^2 * eta(q^8)) in powers of q.",
				"Euler transform of period 16 sequence [ 2, 1, 2, 1, 2, 1, 2, 2, 2, 1, 2, 1, 2, 1, 2, 0, ...].",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2)) where f(u, v) = u^2 - v * (1 + 4*u) * (1 + 2*v).",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (16 t)) = 1/8 * g(t) where q = exp(2 Pi i t) and g() is g.f. for A185338.",
				"a(n) is odd iff n is an odd square. If n\u003e2 is a power of 2 then the highest power of 2 dividing a(n) is (n/2)^3. - _Michael Somos_, Feb 18 2007",
				"4 * a(n) = A007096(n) unless n=0. -(-1)^n * a(n) = A208605(n). Convolution inverse of A185338.",
				"G.f.: x * Product_{k\u003e0} (1 + x^k)^2 * (1 + x^(2*k)) * (1 + x^(4*k)) * (1 + x^(8*k))^2. _Michael Somos_, Sep 19 2013",
				"a(2*n) = 2 * A107035(n). a(2*n + 1) = A093160(n). - _Michael Somos_, Sep 19 2013",
				"a(n) ~ exp(sqrt(n)*Pi) / (2^(9/2) * n^(3/4)). - _Vaclav Kotesovec_, Nov 15 2017"
			],
			"example": [
				"G.f. = q + 2*q^2 + 4*q^3 + 8*q^4 + 14*q^5 + 24*q^6 + 40*q^7 + 64*q^8 + 101*q^9 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 2, 0, q^4] / EllipticTheta[ 4, 0, q] / 2, {q, 0, n}]; (* _Michael Somos_, Sep 19 2013 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c1, 0, n--; A = x * O(x^n); polcoeff( eta(x^2 + A) * eta(x^16 + A)^2 / (eta(x + A)^2 * eta(x^8 + A)), n))};"
			],
			"xref": [
				"Cf. A007096, A185338, A093160, A107035, A185338."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Michael Somos_, Oct 04 2006",
			"references": 6,
			"revision": 32,
			"time": "2021-03-12T22:24:44-05:00",
			"created": "2006-10-09T03:00:00-04:00"
		}
	]
}