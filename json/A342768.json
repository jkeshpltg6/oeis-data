{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A342768",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 342768,
			"data": "1,2,3,8,5,12,7,32,27,20,11,48,13,28,45,128,17,108,19,80,63,44,23,192,125,52,243,112,29,180,31,512,99,68,175,432,37,76,117,320,41,252,43,176,405,92,47,768,343,500,153,208,53,972,275,448,171,116,59,720",
			"name": "a(n) = A342767(n, n).",
			"comment": [
				"This sequence has similarities with A087019.",
				"These are the positions of first appearances of each positive integer in A346701, and also in A346703. - _Gus Wiseman_, Aug 09 2021"
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A342768/b342768.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A342768/a342768.gp.txt\"\u003ePARI program for A342768\u003c/a\u003e",
				"\u003ca href=\"/index/Di#dismal\"\u003eIndex entries for sequences related to dismal (or lunar) arithmetic\u003c/a\u003e"
			],
			"formula": [
				"a(n) = n iff n = 1 or n is a prime number.",
				"a(p^k) = p^(2*k-1) for any k \u003e 0 and any prime number p.",
				"A007947(a(n)) = A007947(n).",
				"A001222(a(n)) = 2*A001222(n) - 1 for any n \u003e 1.",
				"From _Gus Wiseman_, Aug 09 2021: (Start)",
				"A001221(a(n)) = A001221(n).",
				"If g = A006530(n) is the greatest prime factor of n, then a(n) = n^2/g.",
				"a(n) = A129597(n)/2.",
				"(End)"
			],
			"example": [
				"For n = 42:",
				"- 42 = 2 * 3 * 7, so:",
				"          2 3 7",
				"        x 2 3 7",
				"        -------",
				"          2 3 7",
				"        2 3 3",
				"    + 2 2 2",
				"    -----------",
				"      2 2 3 3 7",
				"- hence a(42) = 2 * 2 * 3 * 3 * 7 = 252."
			],
			"mathematica": [
				"Table[n^2/FactorInteger[n][[-1,1]],{n,100}] (* _Gus Wiseman_, Aug 09 2021 *)"
			],
			"program": [
				"(PARI) See Links section."
			],
			"xref": [
				"Cf. A007947, A087019, A342767.",
				"The sum of prime indices of a(n) is 2*A056239(n) - A061395(n).",
				"The version for even indices is A129597(n) = 2*a(n) for n \u003e 1.",
				"The sorted version is A346635.",
				"These are the positions of first appearances in A346701 and in A346703.",
				"A001221 counts distinct prime factors.",
				"A001222 counts prime factors with multiplicity.",
				"A027193 counts partitions of odd length, ranked by A026424.",
				"A209281 adds up the odd bisection of standard compositions (even: A346633).",
				"A346697 adds up the odd bisection of prime indices (reverse: A346699).",
				"Cf. A000290, A006530, A033942, A037143, A344653, A345957, A346698, A346700, A346704."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Rémy Sigrist_, Apr 02 2021",
			"references": 7,
			"revision": 18,
			"time": "2021-09-13T09:19:53-04:00",
			"created": "2021-04-04T00:58:24-04:00"
		}
	]
}