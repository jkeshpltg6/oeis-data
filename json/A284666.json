{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A284666",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 284666,
			"data": "1,1,1,1,25,49,18,25,32,1,841,1681,49,169,289,50,169,288,49,289,529,128,289,450,98,625,1152,289,625,961,800,841,882,162,1681,3200,288,1369,2450,529,1369,2209,1,28561,57121,49,5329,10609,961,1681,2401,289,2809,5329",
			"name": "List of 3-term arithmetic progressions of coprime positive integers whose product is a square.",
			"comment": [
				"This is a 3-column table read by rows a, a+d, a+2*d. Each row has product a square. The rows are ordered by the products. The square roots of the products form A284876, which contains A046176. The pairs a,d form A284874.",
				"Goldbach proved that a product of 3 consecutive positive integers is never a square.",
				"Euler proved that a product of 4 consecutive positive integers is never a square.",
				"Erdos and Selfridge (1975) proved that a product of 2 or more consecutive positive integers is never a square or a higher power.",
				"Saradha (1998) proved that 18, 25, 32 is the only arithmetic progression a, a+d, ..., a+(k-1)*d whose product is a square if a\u003e=1, 1\u003cd\u003c=22, and k\u003e=3 with gcd(a,d)=1. In 1997 she showed that the product is not a square or a higher power if a\u003e=1, 1\u003cd\u003c=6, and k\u003e=3 with gcd(a,d)=1.",
				"(1, 1+d, 1+2*d) is in the table if and only if d is in A078522. - _Robert Israel_, Apr 05 2017 - _Jonathan Sondow_, Apr 06 2017"
			],
			"link": [
				"Giovanni Resta, \u003ca href=\"/A284666/b284666.txt\"\u003eTable of n, a(n) for n = 1..1248\u003c/a\u003e (triples with product \u003c 10^18)",
				"P. Erdős and J.L. Selfridge, \u003ca href=\"https://projecteuclid.org/download/pdf_1/euclid.ijm/1256050816\"\u003eThe product of consecutive integers is never a power\u003c/a\u003e, Illinois J. Math., 19 (1975), 292-301.",
				"N. Saradha, \u003ca href=\"http://matwbn.icm.edu.pl/ksiazki/aa/aa82/aa8224.pdf\"\u003eOn perfect powers in products with terms from arithmetic progressions\u003c/a\u003e, Acta Arith., 82 (1997), 147-172.",
				"N. Saradha, \u003ca href=\"http://matwbn.icm.edu.pl/ksiazki/aa/aa86/aa8613.pdf\"\u003eSquares in products with terms in an arithmetic progression\u003c/a\u003e, Acta Arith., 86 (1998), 27-43."
			],
			"formula": [
				"a(3*k+1) = A284874(2*k+1) and a(3*k+2) = A284874(2*k+1)+A284874(2*k+2) and a(3*k+3) = A284874(2*k+1)+2*A284874(2*k+2) and a(3*k+1)*a(3*k+2)*a(3*k+3) = A284876(k+1)^2 for k\u003e=0."
			],
			"example": [
				"18*(18+7)*(18+2*7) = 18*25*32 = 9*25*64 = (3*5*8)^2 and gcd(18,25,32) = 1, so 18,25,32 is in the sequence."
			],
			"maple": [
				"N:= 10^11: # to get all triples where the product \u003c= N",
				"Res:= [1,0]:",
				"for a from 1 to floor(N^(1/3)) do",
				"  for d from 1 while a*(a+d)*(a+2*d) \u003c= N do",
				"     if igcd(a,d) = 1 and issqr(a*(a+d)*(a+2*d)) then",
				"       Res:= Res, [a,d]",
				"     fi",
				"  od",
				"od:",
				"Res:= sort([Res], (s,t) -\u003e s[1]*(s[1]+s[2])*(s[1]+2*s[2]) \u003c= t[1]*(t[1]+t[2])*(t[1]+2*t[2])):",
				"map(t -\u003e (t[1],t[1]+t[2],t[1]+2*t[2]), Res); # _Robert Israel_, Apr 05 2017"
			],
			"mathematica": [
				"nn = 50000; t = {};",
				"p[a_, b_, c_] := a *b*c; Do[",
				"If[p[a, a + d, a + 2 d] \u003c= 2 nn^2 \u0026\u0026 GCD[a, d] == 1 \u0026\u0026",
				"   IntegerQ[Sqrt[p[a, a + d, a + 2 d]]],",
				"  AppendTo[t, {a, a + d, a + 2 d}]], {a, 1, nn}, {d, 0, nn}];",
				"Sort[t, p[#1[[1]], #1[[2]], #1[[3]]] \u003c",
				"    p[#2[[1]], #2[[2]], #2[[3]]] \u0026] // Flatten"
			],
			"xref": [
				"Cf. A046176, A078522, A284874, A284876."
			],
			"keyword": "nonn,tabf",
			"offset": "1,5",
			"author": "_Jonathan Sondow_, Mar 31 2017",
			"references": 3,
			"revision": 38,
			"time": "2017-04-07T14:31:06-04:00",
			"created": "2017-04-06T21:08:51-04:00"
		}
	]
}