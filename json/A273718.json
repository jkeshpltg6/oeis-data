{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A273718",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 273718,
			"data": "0,0,1,5,20,74,263,914,3134,10655,36023,121331,407610,1366926,4578365,15321750,51245820,171335458,572714527,1914159445,6397373996,21381342737,71465609723,238892470728,798659461590,2670437231049,8930385538663,29869572490093,99922049387230,334324916304050",
			"name": "The number of L-shaped corners in all bargraphs of semiperimeter n.",
			"comment": [
				"The total number of descents in all bargraphs of semiperimeter n\u003e=2. - _Arnold Knopfmacher_, Nov 02 2016"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A273718/b273718.txt\"\u003eTable of n, a(n) for n = 2..500\u003c/a\u003e",
				"A. Blecher, C. Brennan and A. Knopfmacher, \u003ca href=\"http://www.tandfonline.com/doi/abs/10.2989/16073606.2015.1121932\"\u003eCombinatorial parameters in bargraphs\u003c/a\u003e, Quaestiones Mathematicae, 39 (2016), 619-635.",
				"M. Bousquet-Mélou and A. Rechnitzer, \u003ca href=\"http://dx.doi.org/10.1016/S0196-8858(02)00553-5\"\u003eThe site-perimeter of bargraphs\u003c/a\u003e, Adv. Appl. Math., 31, 2003, 86-112.",
				"Emeric Deutsch, S Elizalde, \u003ca href=\"http://arxiv.org/abs/1609.00088\"\u003eStatistics on bargraphs viewed as cornerless Motzkin paths\u003c/a\u003e, arXiv preprint arXiv:1609.00088, 2016"
			],
			"formula": [
				"G.f.:  g(z) = (1 - 4z  + 3z^2 +2Q - Q)/(2zQ), where Q = sqrt(1-4z+2z^2+z^4).",
				"a(n) = Sum(k*A273717(n,k), k\u003e=0).",
				"Conjecture: (n+1)*a(n) +(-7*n+2)*a(n-1) +2*(7*n-12)*a(n-2) +2*(-3*n+10)*a(n-3) +(n+1)*a(n-4) +3*(-n+4)*a(n-5)=0. - _R. J. Mathar_, May 30 2016",
				"Conjecture: -(n+1)*(4*n-15)*a(n) +2*(8*n^2-28*n+15)*a(n-1) -2*(4*n-9)*(n-3)*a(n-2) +4*(n-3)*a(n-3) -(4*n-11)*(n-3)*a(n-4)=0. - _R. J. Mathar_, May 30 2016"
			],
			"example": [
				"a(4)=1 because the 5 (=A082582(4)) bargraphs of semiperimeter 4 correspond to the compositions [1,1,1], [1,2], [2,1], [2,2], [3] of which only [2,1] yields a |_ -shaped corner."
			],
			"maple": [
				"Q := sqrt(1-4*z+2*z^2+z^4): g := ((1-4*z+3*z^2+2*z*Q-Q)*(1/2))/(z*Q): gser := series(g, z = 0,40): seq(coeff(gser, z, n), n = 2 .. 35);"
			],
			"mathematica": [
				"f[x_] := Sqrt[1 - 4*x + 2*x^2 + x^4]; CoefficientList[Series[(1 - 4*x + 3*x^2 + 2*f[x] - f[x])/(2*x*f[x]), {x, 2, 50}], x] (* _G. C. Greubel_, May 29 2016 *)"
			],
			"xref": [
				"Cf. A082582, A273717."
			],
			"keyword": "nonn",
			"offset": "2,4",
			"author": "_Emeric Deutsch_, May 29 2016",
			"references": 2,
			"revision": 26,
			"time": "2017-08-19T23:12:13-04:00",
			"created": "2016-05-30T00:25:08-04:00"
		}
	]
}