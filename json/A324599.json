{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A324599",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 324599,
			"data": "0,0,4,7,9,10,11,18,6,25,13,28,15,40,8,51,26,35,17,54,20,59,19,70,10,85,45,56,21,88,48,73,23,108,12,127,40,105,68,81,55,96,25,130,30,149,27,154,14,177,76,123,95,110,29,180,48,161,65,146",
			"name": "Irregular triangle with the representative solutions of the Diophantine equation x^2 - 5 congruent to 0 modulo N(n), with N(n) = A089270(n), for n \u003e= 1.",
			"comment": [
				"The length of row n is 1 for n = 1 and n = 2, and for n \u003e= 3 it is 2^{r1 + r4} with the number r1 and r4 of distinct primes congruent to 1 and 4 modulo 5, respectively, in the prime number factorization of N(n). E.g., n = 29, N = 209 = 11*19, has r1 = 1 and r4 = 1, with four solutions.",
				"For N(1) = 1 every integer solves this Diophantine equation, and the representative solution is 0.",
				"For N(2) = 5 there is only one representative solution, namely 0.",
				"For n \u003e= 3 the solutions come in a nonnegative power of 2 pairs, each of the type (x1, x2) with x2 = N - x1.",
				"See the link in A089270 to the W. Lang paper, section 3, and Table 7."
			],
			"example": [
				"The irregular triangle T(n, k) begins (pairs (x, N - x) in brackets):",
				"n,    N \\ k   1   2     3   4  ...",
				"----------------------------------",
				"1,    1:      0",
				"2,    5:      0",
				"3,   11:     (4   7)",
				"4,   19:     (9  10)",
				"5,   29:    (11  18)",
				"6,   31:     (6  25)",
				"7,   41:    (13  28)",
				"8,   55:    (15  40)",
				"9,   59:     (8  51)",
				"10,  61:    (26  35)",
				"11,  71:    (17  54)",
				"12,  79:    (20  59)",
				"13,  89:    (19  70)",
				"14,  95:    (10  85)",
				"15, 101:    (45  56)",
				"16, 109:    (21  88)",
				"17, 121:    (48  73)",
				"18, 131:    (23 108)",
				"19, 139:    (12 127)",
				"20, 145:    (40 105)",
				"....",
				"29, 209:    (29 180)  (48 161)",
				"...",
				"41, 319:    (18 301)  (40 279)",
				"...",
				"43, 341:    (37 304) (161 180)",
				"...",
				"59, 451:    (95 356) (136 315)"
			],
			"xref": [
				"Cf. A089270, A324598 (x^2 + x - 1 == 0 (mod N))."
			],
			"keyword": "nonn,tabf",
			"offset": "1,3",
			"author": "_Wolfdieter Lang_, Jul 08 2019",
			"references": 1,
			"revision": 11,
			"time": "2019-07-13T14:16:01-04:00",
			"created": "2019-07-12T19:16:13-04:00"
		}
	]
}