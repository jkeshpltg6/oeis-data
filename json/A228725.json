{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A228725",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 228725,
			"data": "6,3,5,1,8,1,4,2,2,7,3,0,7,3,9,0,8,5,0,1,1,8,7,2,1,0,5,7,7,0,2,8,9,4,9,9,5,5,8,8,2,9,7,3,5,1,5,0,0,8,9,4,2,6,4,6,3,2,2,3,6,2,2,1,8,9,1,3,0,6,7,4,3,7,3,6,7,9,6,9,3,2,7,1",
			"name": "Decimal expansion of the generalized Euler constant gamma(1,2).",
			"comment": [
				"The complement (A239097) is gamma(0,2) = Lim_{x -\u003e infinity} (Sum_{0\u003cn\u003c=x, n even} (1/n - log(x)/2) = (A001620 - A002162)/2 = -0.05796575... - _R. J. Mathar_, Sep 06 2013"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A228725/b228725.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"M. L. Glasser, \u003ca href=\"https://doi.org/10.1080/00029890.2019.1565856\"\u003eA note on Beukers's and related integrals\u003c/a\u003e, Amer. Math. Monthly 126(4) (2019), 361-363.",
				"J. C. Lagarias, \u003ca href=\"http://arxiv.org/abs/1303.1856\"\u003eEuler's constant: Euler's work and modern developments\u003c/a\u003e, arXiv:1303.1856 [math.NT], 2013. See Section 3.8.",
				"D. H. Lehmer, \u003ca href=\"http://matwbn.icm.edu.pl/ksiazki/aa/aa27/aa27121.pdf\"\u003eEuler constants for arithmetic progressions\u003c/a\u003e, Acta Arith. 27 (1975), 125-142."
			],
			"formula": [
				"Equals Lim_{x -\u003e infinity} (Sum_{0\u003cn\u003c=x, n odd} 1/n - log(x)/2).",
				"Equals (A001620 + A002162)/2.",
				"From _Amiram Eldar_, Jun 30 2020: (Start)",
				"Equals -Integral_{x=0..1} log(log(1/x))*x dx.",
				"Equals -Integral_{x=0..oo} exp(-2*x)*log(x) dx. (End)",
				"Equals Integral_{x=0..1, y=0..1} log(-log(x*y))*x*y/log(x*y) dx dy. (Apply Theorem 1 or Theorem 2 of Glasser (2019) to one of _Amiram Eldar_'s integrals.) - _Petros Hadjicostas_, Jun 30 2020"
			],
			"example": [
				".63518142273073908501187210577028949955882973515008942646322..."
			],
			"maple": [
				"(gamma+log(2))/2 ; evalf(%) ;"
			],
			"mathematica": [
				"RealDigits[(EulerGamma+Log[2])/2,10,120][[1]] (* _Harvey P. Dale_, Dec 26 2013 *)"
			],
			"program": [
				"(PARI) (Euler+log(2))/2 \\\\ _Charles R Greathouse IV_, Jul 21 2015",
				"(MAGMA) SetDefaultRealField(RealField(100)); R:= RealField();",
				"(EulerGamma + Log(2))/2; // _G. C. Greubel_, Aug 27 2018"
			],
			"xref": [
				"Cf. A001620, A002162, A239097."
			],
			"keyword": "nonn,cons",
			"offset": "0,1",
			"author": "_R. J. Mathar_, Aug 31 2013",
			"references": 18,
			"revision": 49,
			"time": "2020-07-02T03:33:47-04:00",
			"created": "2013-09-01T01:58:50-04:00"
		}
	]
}