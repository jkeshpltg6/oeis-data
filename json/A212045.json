{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A212045",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 212045,
			"data": "1,3,1,7,3,5,15,7,61,2,31,15,241,25,8,21,31,131,101,137,13,127,21,12,7,2381,343,151,255,127,2105,167,10781,2033,32663,32,511,255,16531,929,42061,9383,84677,2357,83,1023,511,5231,7387,74189,1771,12419",
			"name": "Numerators in the resistance triangle: T(k,n)=b, where b/c is the resistance distance R(k,n) for k resistors in an n-dimensional cube.",
			"comment": [
				"The term \"resistance distance\" for electric circuits was in use years before it was proved to be a metric (on edges of graphs). The historical meaning has been described thus: \"one imagines unit resistors on each edge of a graph G and takes the resistance distance between vertices i and j of G to be the effective resistance between vertices i and j...\"  (from Klein, 2002; see the References).  Let R(k,n) denote the resistance distance for k resistors in an n-dimensional cube (for details, see Example and References).  Then",
				"R(k,n)=A212045(k,n)/A212046(k,n).  Moreover,",
				"A212045(1,n)=A090633(n), A212045(n,n)=A046878(n),",
				"A212046(1,n)=A090634(n), A212046(n,n)=A046879(n)."
			],
			"reference": [
				"F. Nedemeyer and Y. Smorodinsky, Resistances in the multidimensional cube, Quantum 7:1 (1996) 12-15 and 63."
			],
			"link": [
				"D. J. Klein, \u003ca href=\"http://dx.doi.org/10.1007/BF01164627\"\u003eResistance Distance\u003c/a\u003e, Journal of Mathematical Chemistry 12 (1993) 81-95.",
				"D. J. Klein, \u003ca href=\"http://public.carnet.hr/ccacaa/CCA-PDF/cca2002/v75-n2/CCA_75_2002_633_649_KLEIN.pdf\"\u003eResistance-Distance Sum Rules\u003c/a\u003e, Croatia Chemica Acta, 75 (2002), 633-649.",
				"Nicholas Pippenger, \u003ca href=\"http://arxiv.org/abs/0904.1757v1\"\u003e The Hypercube of Resistors, Asymptotic Expansions, and Preferential Arrangements\u003c/a\u003e, arXiv:0904.1757v1 [math.CO], 2009.",
				"N. Pippenger, \u003ca href=\"http://www.jstor.org/stable/10.4169/002557010X529752\"\u003eThe Hypercube of Resistors, Asymptotic Expansions, and Preferential Arrangements\u003c/a\u003e, Mathematics Magazine, 83:5 (2010) 331-346.",
				"David Randall, \u003ca href=\"http://www.radioelectronicschool.net/files/downloads/resistor_cube_problem.pdf\"\u003eThe Resistor Cube Problem\u003c/a\u003e.",
				"D. Singmaster, \u003ca href=\"http://dx.doi.org/10.1137/1022099\"\u003eProblem 79-16, Resistances in an n-Dimensional Cube\u003c/a\u003e, SIAM Review, 22 (1980) 504."
			],
			"formula": [
				"A212045(n)/A212046(n) is the rational number R(k, n) =",
				"[(k-1)*R(k-2,n)-n*R(k-1,n)+2^(1-n)]/(k-n-1), for n\u003e=1, k\u003e=1."
			],
			"example": [
				"First six rows of A212045/A212046:",
				"1",
				"3/4 .... 1",
				"7/12 ... 3/4 .... 5/6",
				"15/32 .. 7/12 ... 61/96 ... 2/3",
				"31/80 .. 15/32 .. 241/480 . 25/48 ... 8/15",
				"21/64 .. 31/80 .. 131/320 . 101/240 . 137/320 . 13/30",
				"The resistance distances for n=3 (the ordinary cube) are 7/12, 3/4, and 5/6, so that row 3 of the triangle of numerators is (7, 3, 5).  For the corresponding electric circuit, suppose X is a vertex of the cube. The resistance across any one of the 3 edges from X is 7/12 ohm; the resistance across any two adjoined edges (i.e., a diagonal of a face of the cubes) is 3/4 ohm; the resistance across and three adjoined edges (a diagonal of the cube) is 5/6 ohm."
			],
			"mathematica": [
				"R[0, n_] := 0; R[1, n_] := (2 - 2^(1 - n))/n;",
				"R[k_, n_] := R[k, n] = ((k - 1) R[k - 2, n] - n R[k - 1, n] + 2^(1 - n))/(k - n - 1)",
				"t = Table[R[k, n], {n, 1, 11}, {k, 1, n}]",
				"Flatten[Numerator[t]]    (* A212045 *)",
				"Flatten[Denominator[t]]  (* A212046 *)",
				"TableForm[Numerator[t]]",
				"TableForm[Denominator[t]]"
			],
			"xref": [
				"Cf. A212046, A046878, A046879, A046825, A090634, A090633."
			],
			"keyword": "nonn,frac,tabl",
			"offset": "1,2",
			"author": "_Peter J. C. Moses_, Apr 28 2012",
			"references": 2,
			"revision": 26,
			"time": "2014-04-28T09:56:12-04:00",
			"created": "2012-04-28T20:49:01-04:00"
		}
	]
}