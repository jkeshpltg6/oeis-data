{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A140882",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 140882,
			"data": "1,2,-1,0,-4,1,0,-8,6,-1,0,-12,19,-8,1,0,-16,44,-34,10,-1,0,-20,85,-104,53,-12,1,0,-24,146,-259,200,-76,14,-1,0,-28,231,-560,606,-340,103,-16,1,0,-32,344,-1092,1572,-1210,532,-134,18,-1,0,-36,489,-1968,3630,-3652,2171,-784,169,-20,1",
			"name": "A set of Cartan-like matrices with the properties that either the rows or columns as sums are zero that give a triangle of coefficients of characteristic polynomials: Example 3 X 3 matrix (row sums zero): {{2, -2, 0}, {-1, 2, -1}, {0, -2, 2}}.",
			"comment": [
				"Row sums are: {1, 1, -3, -3, 0, 3, 3, 0, -3, -3, 0}.",
				"This sequence of matrices was inspired by the Kemeny \"dominant\", \"hybrid\" and \"recessive\" matrix of genetic characteristics:",
				"  {{2, 2, 0},",
				"   {1, 2, 1},",
				"   {0, 2, 2}}/2^2.",
				"That type of matrix has row sums equal to one.",
				"I noticed that it resembled an unsigned Cartan matrix of a D_n or B_n type with rows sums zero."
			],
			"reference": [
				"Kemeny, Snell and Thompson, Introduction to Finite Mathematics, 1966, Prentice-Hall, New Jersey, Section 3, Chapter VII, page 407."
			],
			"link": [
				"Pentti Haukkanen, Jorma Merikoski, Seppo Mustonen, \u003ca href=\"http://www.acta.sapientia.ro/acta-math/C6-2/math62-5.pdf\"\u003eSome polynomials associated with regular polygons\u003c/a\u003e, Acta Univ. Sapientiae, Mathematica, 6, 2 (2014) 178-193."
			],
			"formula": [
				"m(d) = If[ n == m, 2, If[(n == d \u0026\u0026m == d - 1) || (n == 1 \u0026\u0026 m == 2), -2, If[(n == m - 1 || n == m + 1), -1, 0]]; out_n,m=Coefficients(CharacteristicPolynomial(m(n)).",
				"Starting with the third row (0,-4,1), these coefficients appear to occur in the odd row polynomials in inverse powers of u for the o.g.f. (u-4)/(u-ux+x^2) of A267633, which generates a Fibonacci-type running average of adjacent polynomials of the o.g.f. involving these polynomials and others embedded in A228785. - _Tom Copeland_, Jan 16 2016"
			],
			"example": [
				"  1;",
				"  2,  -1;",
				"  0,  -4,   1;",
				"  0,  -8,   6,    -1;",
				"  0, -12,  19,    -8,    1;",
				"  0, -16,  44,   -34,   10,    -1;",
				"  0, -20,  85,  -104,   53,   -12,    1;",
				"  0, -24, 146,  -259,  200,   -76,   14,   -1;",
				"  0, -28, 231,  -560,  606,  -340,  103,  -16,   1;",
				"  0, -32, 344, -1092, 1572, -1210,  532, -134,  18,  -1;",
				"  0, -36, 489, -1968, 3630, -3652, 2171, -784, 169, -20, 1;",
				"  ..."
			],
			"mathematica": [
				"T[n_, m_, d_] := If[ n == m, 2, If[(n == d \u0026\u0026 m == d - 1) || ( n == 1 \u0026\u0026 m == 2), -2, If[(n == m - 1 || n == m + 1), -1, 0]]]; M[d_] := Table[T[n, m, d], {n, 1, d}, {m, 1, d}]; a = Join[{{1}}, Table[CoefficientList[Det[M[ d] - x*IdentityMatrix[d]], x], {d, 1, 10}]]; Flatten[a]"
			],
			"xref": [
				"Cf. A228785, A267633."
			],
			"keyword": "uned,tabl,sign",
			"offset": "1,2",
			"author": "_Roger L. Bagula_ and _Gary W. Adamson_, Jul 22 2008",
			"references": 2,
			"revision": 19,
			"time": "2021-01-30T01:26:06-05:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}