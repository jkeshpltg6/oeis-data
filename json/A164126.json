{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A164126",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 164126,
			"data": "1,2,2,2,2,6,2,4,6,4,2,12,6,12,2,8,12,8,6,8,12,8,2,24,12,24,6,24,12,24,2,16,24,16,12,16,24,16,6,16,24,16,12,16,24,16,2,48,24,48,12,48,24,48,6,48,24,48,12,48,24,48,2,32,48,32,24,32,48,32,12,32,48,32,24,32,48,32,6",
			"name": "First differences of A006995.",
			"comment": [
				"Contribution from _Hieronymus Fischer_, Feb 18 2012: (Start)",
				"From the formula section it follows that a(2^m - 1 + 2^(m-1) - k) = a(2^m - 1 + k) for 0 \u003c= k \u003c= 2^(m-1), as well as a(2^m - 1 + 2^(m-1) - k) = 2 for k=0, 2^(m-1) and a(2^m - 1 + 2^(m-1) - k) = 6 for k=2^(m-2), hence, starting from positions n=2^m-1, the following 2^(m-1) terms form symmetric tuples limited on the left and on the right by a '2' and always having a '6' as the center element.",
				"Example: for n = 15 = 2^4 - 1, we have the (2^3+1)-tuple (2,8,12,8,6,8,12,8,2).",
				"Further on, since a(2^m - 1 + 2^(m-1) + k) = a(2^(m+1) - 1 - k) for 0 \u003c= k \u003c= 2^(m-1) an analogous statement holds true for starting positions n = 2^m + 2^(m-1) - 1.",
				"Example: for n = 23 = 2^4 + 2^3 - 1, we find the (2^3+1)-tuple (2,24,12,24,6,24,12,24,2).",
				"If we group the sequence terms according to the value of m=floor(log_2(n)), writing those terms together in separate lines and opening each new line for n \u003e= 2^m + 2^(m-1), then a kind of a 'logarithmic shaped' cone end will be formed, where both the symmetry and the calculation rules become obvious. The first 63 terms are depicted below:",
				"                          1",
				"                          2",
				"                          2",
				"                        2  2",
				"                        6  2",
				"                      4 6  4  2",
				"                     12 6 12  2",
				"                8 12  8 6  8 12  8 2",
				"               24 12 24 6 24 12 24 2",
				"   16 24 16 12 16 24 16 6 16 24 16 12 16 24 16 2",
				"   48 24 48 12 48 24 48 6 48 24 48 12 48 24 48 2",
				".",
				"(End)",
				"Decremented by 1, also the sequence of run lengths of 0's in A178225. - _Hieronymus Fischer_, Feb 19 2012"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A164126/b164126.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A006995(n+1) - A006995(n).",
				"Contribution from _Hieronymus Fischer_, Feb 17 2012: (Start)",
				"a(4*2^m - 1) = a(6*2^m - 1) = 2;",
				"a(5*2^m - 1) = a(7*2^m - 1) = 6 (for m \u003e 0);",
				"Let m = floor(log_2(n)), then",
				"  Case 1: a(n) = 2, if n+1 = 2^(m+1) or n+1 = 3*2^(m-1);",
				"  Case 2: a(n) = 2^(m-1), if n = 0(mod 2) and n \u003c 3*2^(m-1);",
				"  Case 3: a(n) = 3*2^(m-1), if n = 0(mod 2) and n \u003e= 3*2^(m-1);",
				"  Case 4: a(n) = 3*2^(m-1)/gcd(n+1-2^m, 2^m), otherwise.",
				"Cases 2-4 above can be combined as",
				"  Case 2': a(n) = (2 - (-1)^(n-(n-1)*floor(2*n/(3*2^m))))*2^(m-1)/gcd(n+1-2^m, 2^m).",
				"Recursion formula:",
				"Let m = floor(log_2(n)); then",
				"  Case 1: a(n) = 2*a(n-2^(m-1)), if 2^m \u003c= n \u003c 2^m + 2^(m-2) - 1;",
				"  Case 2: a(n) = 6, if n = 2^m + 2^(m-2) - 1;",
				"  Case 3: a(n) = a(n-2^(m-2)), if 2^m + 2^(m-2) \u003c= n \u003c 2^m + 2^(m-1) - 1;",
				"  Case 4: a(n) = 2, if n = 2^m + 2^(m-1) - 1;",
				"  Case 5: a(n) = (2 + (-1)^n)*a(n-2^(m-1)), otherwise (which means 2^m + 2^(m-1) \u003c= n \u003c 2^(m+1)).",
				"(End)"
			],
			"example": [
				"a(1) = A006995(2) - A006995(1) = 1 - 0 = 1."
			],
			"mathematica": [
				"f[n_]:=FromDigits[RealDigits[n,2][[1]]]==FromDigits[Reverse[RealDigits[n, 2][[1]]]]; a=1;lst={};Do[If[f[n],AppendTo[lst,n-a];a=n],{n,1,8!,1}]; lst"
			],
			"xref": [
				"Cf. A006995, A164124, A029971, A016041, A164125.",
				"See A178225."
			],
			"keyword": "nonn,base,easy",
			"offset": "1,2",
			"author": "_Vladimir Joseph Stephan Orlovsky_, Aug 10 2009",
			"ext": [
				"a(1) changed to 1 and keyword:base added by _R. J. Mathar_, Aug 26 2009"
			],
			"references": 3,
			"revision": 29,
			"time": "2017-10-04T00:17:53-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}