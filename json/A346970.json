{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A346970",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 346970,
			"data": "2,3,5,10,21,55,182,357,1105,2958,16588,51243,106981,608685,3003455,7497910",
			"name": "Smallest c which can be split into positive integers a,b with a+b=c, such that a*b*c is divisible by each of the first n primes.",
			"comment": [
				"From _David A. Corneth_, Aug 10 2021: (Start)",
				"Let k be a candidate value for a(n) and let g = gcd(c, primorial(n)) = gcd(c, A002110(n)). Let p be the largest prime that primorial(n)/g is divisible by. Then at least one of a and b is divisible by p.",
				"So we can check multiples m of p \u003c k and see if m*(k-m)*k is divisible by primorial(n).",
				"If that is the case then k can be written as a + b where 0 \u003c a,b \u003c k and a*b*k is divisible by primorial(n) and a(n) = k when each of 1..k-1 do not have that property. (End)"
			],
			"formula": [
				"A002110(n)^(1/3) \u003c a(n) \u003c= A002110(n). - _David A. Corneth_, Aug 10 2021",
				"a(2k) \u003c= Product_{i=1..k} p_2i, a(2k+1) \u003c= Product_{i=0..k} p_{2i+1} where p_i is the i-th prime. - _Chai Wah Wu_, Oct 14 2021"
			],
			"example": [
				"a(4) = 10 via 3+7=10 and 3*7*10 = 210 which is divisible by each of the first 4 primes, namely 2, 3, 5 and 7.",
				"a(7) = 182 via 17+165=182. 17*165*182 = 510510 which is divisible by each of the first 7 primes, namely 2, 3, ..., 13, 17."
			],
			"mathematica": [
				"Do[Monitor[k=1;While[!Or@@And@@@(IntegerQ/@(#/Prime@Range@n)\u0026/@Times@@@(Join[{k},#]\u0026/@IntegerPartitions[k,{2}])),k++];Print@k,k],{n,20}] (* _Giorgos Kalogeropoulos_, Aug 16 2021 *)"
			],
			"program": [
				"(PARI) a(n) = { if(n \u003c= 3, return(prime(n))); P = vecprod(primes(n)); for(i = sqrtnint(P, 3) + 1, oo, if(iscanforA(n, i), return(i) ) ) }",
				"iscanforA(n, k) = { my(g = gcd(k, P), step); if(k^2*g \u003c P, return(0)); step = hpf(P/g); forstep(i = step, k, step, if((i*(k-i)*k)%P == 0,  return(1) ) ); 0 }",
				"hpf(n) = my(f = factor(n)); f[#f~, 1] \\\\ _David A. Corneth_, Aug 10 2021",
				"(Python)",
				"from sympy import primorial, primefactors, gcd",
				"def A346970(n):",
				"    c, ps = 2, primorial(n)",
				"    while True:",
				"        m = ps//gcd(ps,c)",
				"        if m == 1:",
				"            return c",
				"        p = max(primefactors(m))",
				"        for a in range(p,c,p):",
				"            if a*(c-a) % m == 0:",
				"                return c",
				"        c += 1 # _Chai Wah Wu_, Oct 13 2021"
			],
			"xref": [
				"Cf. A002110, A346971."
			],
			"keyword": "nonn,more",
			"offset": "1,1",
			"author": "_Steven M. Altschuld_, Aug 09 2021",
			"ext": [
				"a(12)-a(16) from _David A. Corneth_, Aug 10 2021"
			],
			"references": 2,
			"revision": 47,
			"time": "2021-10-16T05:09:52-04:00",
			"created": "2021-10-13T10:47:31-04:00"
		}
	]
}