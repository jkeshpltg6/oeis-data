{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000605",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 605,
			"id": "M4406 N1860",
			"data": "1,7,33,123,257,515,925,1419,2109,3071,4169,5575,7153,9171,11513,14147,17077,20479,24405,28671,33401,38911,44473,50883,57777,65267,73525,82519,91965,101943,113081,124487,137065,150555,164517,179579,195269,212095",
			"name": "Number of points of norm \u003c= n in cubic lattice.",
			"reference": [
				"J. H. Conway and N. J. A. Sloane, \"Sphere Packings, Lattices and Groups\", Springer-Verlag, p. 107.",
				"H. Gupta, A Table of Values of N_3(t), Proc. National Institute of Sciences of India, 13 (1947), 35-63.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A000605/b000605.txt\"\u003eTable of n, a(n) for n=0..500\u003c/a\u003e",
				"W. Fraser and C. C. Gotlieb, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1962-0155788-9\"\u003eA calculation of the number of lattice points in the circle and sphere\u003c/a\u003e, Math. Comp., 16 (1962), 282-290.",
				"Z. C. Holden, R. M. Richard, J. M. Herbert, \u003ca href=\"http://dx.doi.org/10.1063/1.4850655\"\u003ePeriodic boundary conditions for QM/MM calculations: Ewald summation for extended Gaussian basis sets\u003c/a\u003e, The Journal of Chemical Physics, J. Chem. Phys. 139, 244108 (2013)."
			],
			"formula": [
				"a(n) = A117609(n^2). - _R. J. Mathar_, Apr 21 2010",
				"a(n) = [x^(n^2)] theta_3(x)^3/(1 - x), where theta_3() is the Jacobi theta function. - _Ilya Gutkovskiy_, Apr 14 2018"
			],
			"mathematica": [
				"Table[Sum[SquaresR[3, k], {k, 0, n^2}], {n, 0, 37}]"
			],
			"program": [
				"(C)",
				"int A000605(int i)",
				"{",
				"    const int ring = i*i;",
				"    int result = 0;",
				"    for (int a = -i; a \u003c= i; a++)",
				"        for (int b = -i; b \u003c= i; b++)",
				"            for (int c = -i; c \u003c= i; c++)",
				"                if ( ring \u003e= a*a+b*b+c*c )  result++;",
				"    return result;",
				"} /* _Oskar Wieland_, Apr 08 2013 */",
				"(PARI)",
				"N=66;  q='q+O('q^(N^2));",
				"t=Vec((eta(q^2)^5/(eta(q)^2*eta(q^4)^2))^3/(1-q));  /* A117609 */",
				"vector(sqrtint(#t),n,t[(n-1)^2+1])",
				"/* _Joerg Arndt_, Apr 08 2013 */"
			],
			"xref": [
				"Column k=3 of A302997.",
				"Cf. A117609 (number of lattice points inside the ball x^2+y^2+z^2 \u003c= n)."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _David W. Wilson_, May 22 2000"
			],
			"references": 11,
			"revision": 47,
			"time": "2019-09-15T13:48:31-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}