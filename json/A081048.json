{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A081048",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 81048,
			"data": "0,1,-3,11,-50,274,-1764,13068,-109584,1026576,-10628640,120543840,-1486442880,19802759040,-283465647360,4339163001600,-70734282393600,1223405590579200,-22376988058521600,431565146817638400,-8752948036761600000,186244810780170240000",
			"name": "Signed Stirling numbers of the first kind.",
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A081048/b081048.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"Vladimir Reshetnikov, \u003ca href=\"/A081048/a081048.txt\"\u003eProof Mathar's formula, Apr 24 2013\u003c/a\u003e"
			],
			"formula": [
				"a(n) = n!*Sum {k=1..n} (-1)^(n+1)*1/k.",
				"E.g.f.: log(1+x)/(1+x).",
				"D-finite with recurrence a(n) = (2*n-1)*a(n-1) + (n-1)^2*a(n-2) = 0. (Proved by Reshetnikov.) - _R. J. Mathar_, Nov 24 2012",
				"a(n) = (-1)^(n-1)*det(S(i+2,j+1), 1 \u003c= i,j \u003c= n-1), where S(n,k) are Stirling numbers of the second kind and n\u003e0. - _Mircea Merca_, Apr 06 2013",
				"a(n) ~ n! * (-1)^(n+1) * (log(n) + gamma), where gamma is the Euler-Mascheroni constant (A001620). - _Vaclav Kotesovec_, Oct 05 2013"
			],
			"example": [
				"a(9): coefficient of p^2 in polynomial p (p - 1) (p - 2) (p - 3) (p - 4) (p - 5) (p - 6) (p - 7) (p - 8) = -1 + 40320 p - 109584 p^2 + 118124 p^3 - 67284 p^4 + 22449 p^5 - 4536 p^6 + 546 p^7 - 36 p^8 + p^9 is equal to -109584. - _Artur Jasinski_, Nov 30 2008"
			],
			"maple": [
				"a:= proc(n) option remember;",
				"      `if`(n\u003c2, n, (1-2*n)*a(n-1) -(n-1)^2*a(n-2))",
				"    end:",
				"seq(a(n), n=0..30);  # _Alois P. Heinz_, Aug 06 2013"
			],
			"mathematica": [
				"aa = {}; Do[AppendTo[aa,Coefficient[Expand[Product[p - n, {n, 0, m}]], p, 2]], {m, 1, 20}]; aa (* _Artur Jasinski_, Nov 30 2008 *)",
				"a[n_] := (-1)^(n+1)*n!*HarmonicNumber[n];",
				"Table[a[n], {n, 0, 30}] (* _Jean-François Alcover_, Mar 29 2017 *)"
			],
			"program": [
				"(PARI) a(n)=stirling(n,2) \\\\ _Charles R Greathouse IV_, May 08 2015",
				"(MAGMA) m:=30; R\u003cx\u003e:=PowerSeriesRing(Rationals(), m); b:=Coefficients(R!(Log(1+x)/(1+x))); [0] cat [Factorial(n)*b[n]: n in [1..m-1]]; // _G. C. Greubel_, Aug 28 2018"
			],
			"xref": [
				"Cf. A000254, A008275."
			],
			"keyword": "sign",
			"offset": "0,3",
			"author": "_Paul Barry_, Mar 05 2003",
			"references": 12,
			"revision": 40,
			"time": "2021-09-15T07:04:10-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}