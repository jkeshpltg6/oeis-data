{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A319411",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 319411,
			"data": "2,2,2,2,2,4,2,4,6,4,2,2,12,12,4,2,6,30,18,8,0,2,2,44,44,32,4,0,2,6,82,76,74,16,0,0,2,4,144,138,172,52,0,0,0,2,6,258,248,350,156,4,0,0,0,2,2,426,452,734,404,28,0,0,0,0,2,10,790,752,1500,938,104,0,0,0,0,0",
			"name": "Triangle read by rows: T(n,k) = number of binary vectors of length n with runs-resistance k (1 \u003c= k \u003c= n).",
			"comment": [
				"\"Runs-resistance\" is defined in A318928.",
				"Row sums are 2,4,8,16,... (the binary vectors may begin with 0 or 1).",
				"This is similar to A329767 but without the k = 0 column and with a different row n = 1. - _Gus Wiseman_, Nov 25 2019"
			],
			"link": [
				"Hiroaki Yamanouchi, \u003ca href=\"/A319411/b319411.txt\"\u003eTable of n, a(n) for n = 1..1830\u003c/a\u003e",
				"Claude Lenormand, \u003ca href=\"/A318921/a318921.pdf\"\u003eDeux transformations sur les mots\u003c/a\u003e, Preprint, 5 pages, Nov 17 2003. Apparently unpublished. This is a scanned copy of the version that the author sent to me in 2003."
			],
			"example": [
				"Triangle begins:",
				"2,",
				"2, 2,",
				"2, 2, 4,",
				"2, 4, 6, 4,",
				"2, 2, 12, 12, 4,",
				"2, 6, 30, 18, 8, 0,",
				"2, 2, 44, 44, 32, 4, 0,",
				"2, 6, 82, 76, 74, 16, 0, 0,",
				"2, 4, 144, 138, 172, 52, 0, 0, 0,",
				"2, 6, 258, 248, 350, 156, 4, 0, 0, 0,",
				"2, 2, 426, 452, 734, 404, 28, 0, 0, 0, 0,",
				"2, 10, 790, 752, 1500, 938, 104, 0, 0, 0, 0, 0,",
				"...",
				"Lenormand gives the first 20 rows.",
				"The calculation of row 4 is as follows.",
				"We may assume the first bit is a 0, and then double the answers.",
				"vector / runs / steps to reach a single number:",
				"0000 / 4 / 1",
				"0001 / 31 -\u003e 11 -\u003e 2 / 3",
				"0010 / 211 -\u003e 12 -\u003e 11 -\u003e 2 / 4",
				"0011 / 22 -\u003e 2 / 2",
				"0100 / 112 -\u003e 21 -\u003e 11 -\u003e 2 / 4",
				"0101 / 1111 -\u003e 4 / 2",
				"0110 / 121 -\u003e 111 -\u003e 3 / 3",
				"0111 / 13 -\u003e 11 -\u003e 2 / 3",
				"and we get 1 (once), 2 (twice), 3 (three times) and 4 (twice).",
				"So row 4 is: 2,4,6,4."
			],
			"mathematica": [
				"runsresist[q_]:=If[Length[q]==1,1,Length[NestWhileList[Length/@Split[#]\u0026,q,Length[#]\u003e1\u0026]]-1];",
				"Table[Length[Select[Tuples[{0,1},n],runsresist[#]==k\u0026]],{n,10},{k,n}] (* _Gus Wiseman_, Nov 25 2019 *)"
			],
			"xref": [
				"Row sums are A000079.",
				"Column k = 2 is 2 * A032741 = A319410.",
				"Column k = 3 is 2 * A329745 (because runs-resistance 2 for compositions corresponds to runs-resistance 3 for binary words).",
				"The version for compositions is A329744.",
				"The version for partitions is A329746.",
				"The number of nonzero entries in row n \u003e 0 is A319412(n).",
				"The runs-resistance of the binary expansion of n is A318928.",
				"Cf. A096365, A225485, A245563, A319413, A319414, A319415, A325280, A329747, A329750, A329767, A329870."
			],
			"keyword": "nonn,tabl,base",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_, Sep 20 2018",
			"references": 21,
			"revision": 18,
			"time": "2019-11-27T08:34:42-05:00",
			"created": "2018-09-20T07:34:42-04:00"
		}
	]
}