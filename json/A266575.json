{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A266575",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 266575,
			"data": "1,2,4,8,8,12,16,16,25,28,28,32,40,40,48,64,48,62,76,64,80,92,80,96,121,100,112,128,120,136,160,128,144,184,152,200,200,164,208,224,192,216,252,224,248,296,224,256,337,262,312,320,280,336,368,320,336,396",
			"name": "Expansion of q * f(-q^4)^6 / phi(-q) in powers of q where phi(), f() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A266575/b266575.txt\"\u003eTable of n, a(n) for n = 1..2500\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q * phi(q) * psi(q^2)^4 in powers of q where phi(), psi() are Ramanujan theta functions.",
				"Expansion of eta(q^2) * eta(q^4)^6 / eta(q)^2 in powers of q.",
				"Euler transform of period 4 sequence [2, 1, 2, -5, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (4 t)) = 2^(-3/2) (t/I)^(5/2) g(t) where q = exp(2 Pi i t) and g() is the g.f. for A245643.",
				"G.f.: x * Product_{k\u003e0} (1 + x^k) * (1 - x^(4*k))^6 / (1 - x^k).",
				"Convolution inverse of A134414."
			],
			"example": [
				"G.f. = x + 2*x^2 + 4*x^3 + 8*x^4 + 8*x^5 + 12*x^6 + 16*x^7 + 16*x^8 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ q QPochhammer[ q^4]^6 / EllipticTheta[ 4, 0, q], {q, 0, n}];",
				"a[ n_] := SeriesCoefficient[ 2^-4 EllipticTheta[ 3, 0, q] EllipticTheta[ 2, 0, q]^4, {q, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c1, 0, n--; A = x * O(x^n); polcoeff( eta(x^2 + A) * eta(x^4 + A)^6 / eta(x + A)^2, n))};",
				"(MAGMA) A := Basis( ModularForms( Gamma1(4), 5/2), 59); A[2];"
			],
			"xref": [
				"Cf. A134414. A245643."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Michael Somos_, Jan 03 2016",
			"references": 2,
			"revision": 18,
			"time": "2021-03-12T22:24:48-05:00",
			"created": "2016-01-03T21:17:38-05:00"
		}
	]
}