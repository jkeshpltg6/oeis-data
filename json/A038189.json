{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A038189",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 38189,
			"data": "0,0,0,1,0,0,1,1,0,0,0,1,1,0,1,1,0,0,0,1,0,0,1,1,1,0,0,1,1,0,1,1,0,0,0,1,0,0,1,1,0,0,0,1,1,0,1,1,1,0,0,1,0,0,1,1,1,0,0,1,1,0,1,1,0,0,0,1,0,0,1,1,0,0,0,1,1,0,1,1,0,0,0,1,0,0,1,1,1,0,0,1,1,0,1,1,1,0,0,1,0,0,1,1,0,0,0,1",
			"name": "Bit to left of least significant 1-bit in binary expansion of n.",
			"comment": [
				"Characteristic function of A091067.",
				"Image, under the coding i -\u003e floor(i/2), of the fixed point, starting with 0, of the morphism 0 -\u003e 01, 1 -\u003e 02, 2 -\u003e 32, 3 -\u003e 31. - _Jeffrey Shallit_, May 15 2016"
			],
			"reference": [
				"Jean-Paul Allouche and Jeffrey O. Shallit, Automatic sequences, Cambridge, 2003, sect. 5.1.6"
			],
			"link": [
				"Ivan Panchenko, \u003ca href=\"/A038189/b038189.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Michael Gilleland, \u003ca href=\"/selfsimilar.html\"\u003eSome Self-Similar Integer Sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Ch#char_fns\"\u003eIndex entries for characteristic functions\u003c/a\u003e",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e",
				"\u003ca href=\"/index/Fo#fold\"\u003eIndex entries for sequences obtained by enumerating foldings\u003c/a\u003e"
			],
			"formula": [
				"a(0) = 0, a(2*n) = a(n) for n\u003e0, a(4*n+1) = 0, a(4*n+3) = 1.",
				"G.f.: Sum_{k\u003e=0} t^3/(1-t^4), where t=x^2^k. Parity of A025480. a(n) = 1/2 * (1 - (-1)^A025480(n)). - _Ralf Stephan_, Jan 04 2004",
				"a(n) = 1 if Kronecker(-n,m)=Kronecker(m,n) for all m, otherwise a(n)=0. - _Michael Somos_, Sep 22 2005",
				"a(n) = 1 iff A164677(n) \u003c 0. - _M. F. Hasler_, Aug 06 2015"
			],
			"example": [
				"a(6) = 1 since 6 = 110 and bit before rightmost 1 is a 1."
			],
			"maple": [
				"A038189 := proc(n)",
				"    option remember;",
				"    if n = 0 then",
				"        0 ;",
				"    elif type(n,'even') then",
				"        procname(n/2) ;",
				"    elif modp(n,4) = 1 then",
				"        0 ;",
				"    else",
				"        1 ;",
				"    end if;",
				"end proc:",
				"seq(A038189(n),n=0..100) ; # _R. J. Mathar_, Mar 30 2018"
			],
			"mathematica": [
				"f[n_] := Block[{id2 = Join[{0}, IntegerDigits[n, 2]]}, While[ id2[[-1]] == 0, id2 = Most@ id2]; id2[[-2]]]; f[0] = 0; Array[f, 105, 0] (* _Robert G. Wilson v_, Apr 14 2009 and fixed Feb 27 2014 *)",
				"f[n_] := f[n] = Switch[Mod[n, 4], 0, f[n/2], 1, 0, 2, f[n/2], 3, 1]; f[0] = 0; Array[f, 105, 0] (* _Robert G. Wilson v_, Apr 14 2009, fixed Feb 27 2014 *)"
			],
			"program": [
				"(C) int a(int n) { return (n \u0026 ((n\u0026-n)\u003c\u003c1)) ? 1 : 0; } /* from _Russ Cox_ */",
				"(PARI) a(n) = if(n\u003c1, 0, ((n/2^valuation(n,2)-1)/2)%2) /* _Michael Somos_, Sep 22 2005 */",
				"(PARI) a(n) = if(n\u003c3, 0, prod(m=1,n, kronecker(-n,m)==kronecker(m,n))) /* _Michael Somos_, Sep 22 2005 */",
				"(PARI) A038189(n)=bittest(n,valuation(n,2)+1) \\\\ _M. F. Hasler_, Aug 06 2015",
				"(PARI) a(n)=my(h=bitand(n,-n));n=bitand(n,h\u003c\u003c1);n!=0; \\\\ _Joerg Arndt_, Apr 09 2021",
				"(Magma)",
				"function a (n)",
				"  if n eq 0 then return 0; // alternatively,  return 1;",
				"  else while IsEven(n) do n := n div 2; end while; end if;",
				"  return n div 2 mod 2; end function;",
				"  nlo := 0; nhi := 32;",
				"  [a(n) : n in [nlo..nhi] ]; //  _Fred Lunnon_, Mar 27 2018",
				"(Python)",
				"def A038189(n):",
				"    s = bin(n)[2:]",
				"    m = len(s)",
				"    i = s[::-1].find('1')",
				"    return int(s[m-i-2]) if m-i-2 \u003e= 0 else 0 # _Chai Wah Wu_, Apr 08 2021"
			],
			"xref": [
				"Cf. A038190.",
				"A014707(n)=a(n+1). A014577(n)=1-a(n+1).",
				"The following are all essentially the same sequence: A014577, A014707, A014709, A014710, A034947, A038189, A082410. - _N. J. A. Sloane_, Jul 27 2012",
				"Related sequences A301848, A301849, A301850. - _Fred Lunnon_, Mar 27 2018"
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "_Fred Lunnon_, Dec 11 1999",
			"ext": [
				"More terms from _David W. Wilson_",
				"Definition corrected by _Russ Cox_ and _Ralf Stephan_, Nov 08 2004"
			],
			"references": 21,
			"revision": 91,
			"time": "2021-10-11T11:45:45-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}