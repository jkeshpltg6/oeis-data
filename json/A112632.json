{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A112632",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 112632,
			"data": "1,1,2,1,2,1,2,1,2,3,2,1,2,1,2,3,4,3,2,3,2,1,2,3,2,3,2,3,2,3,2,3,4,3,4,3,2,1,2,3,4,3,4,3,4,3,2,1,2,1,2,3,2,3,4,5,6,5,4,5,4,5,4,5,4,5,4,3,4,3,4,5,4,3,2,3,4,3,4,3,4,3,4,3,2,3,4,3,4,3,4,5,4,5,4,5,6,7,6,5",
			"name": "Excess of 3k - 1 primes over 3k + 1 primes, beginning with 2.",
			"comment": [
				"Cumulative sums of A134323, negated. The first negative term is a(23338590792) = -1 for the prime 608981813029. See page 4 of the paper by Granville and Martin. - _T. D. Noe_, Jan 23 2008 [Corrected by _Jianing Song_, Nov 24 2018]",
				"In general, assuming the strong form of RH, if 0 \u003c a, b \u003c k, gcd(a, k) = gcd(b, k) = 1, a is a quadratic residue and b is a quadratic nonresidue mod n, then Pi(k,b)(n) \u003e Pi(k,a)(n) occurs more often than not. Pi(a,b)(x) denotes the number of primes in the arithmetic progression a*k + b less than or equal to x. This phenomenon is called \"Chebyshev's bias\". - _Jianing Song_, Nov 24 2018"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A112632/b112632.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"A. Granville and G. Martin, \u003ca href=\"http://www.jstor.org/stable/27641834\"\u003ePrime number races\u003c/a\u003e, Amer. Math. Monthly, 113 (No. 1, 2006), pp. 1-33.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Chebyshev%27s_bias\"\u003eChebyshev's bias\u003c/a\u003e"
			],
			"formula": [
				"a(n) = -Sum_{primes p\u003c=n} Legendre(prime(i),3) = -Sum_{primes p\u003c=n} Kronecker(-3,prime(i)) = -Sum_{i=1..n} A102283(prime(i)). - _Jianing Song_, Nov 24 2018"
			],
			"example": [
				"a(1) = 1 because 2 == -1 (mod 3).",
				"a(2) = 1 because 3 == 0 (mod 3) and does not change the counting.",
				"a(3) = 2 because 5 == -1 (mod 3).",
				"a(4) = 1 because 7 == 1 (mod 3)."
			],
			"mathematica": [
				"a[n_] := a[n] = a[n-1] + If[Mod[Prime[n], 6] == 1, -1, 1]; a[1] = a[2] = 1; Table[a[n], {n, 1, 100}]  (* _Jean-François Alcover_, Jul 24 2012 *)",
				"Accumulate[Which[IntegerQ[(#+1)/3],1,IntegerQ[(#-1)/3],-1,True,0]\u0026 /@ Prime[ Range[100]]] (* _Harvey P. Dale_, Jun 06 2013 *)"
			],
			"program": [
				"(Haskell)",
				"a112632 n = a112632_list !! (n-1)",
				"a112632_list = scanl1 (+) $ map negate a134323_list",
				"-- _Reinhard Zumkeller_, Sep 16 2014",
				"(PARI) a(n) = -sum(i=1, n, kronecker(-3, prime(i))) \\\\ _Jianing Song_, Nov 24 2018"
			],
			"xref": [
				"Cf. A007352, A098044, A102283, A134323.",
				"Let d be a fundamental discriminant.",
				"Sequences of the form \"a(n) = -Sum_{primes p\u003c=n} Kronecker(d,p)\" with |d| \u003c= 12: A321860 (d=-11), A320857 (d=-8), A321859 (d=-7), A066520 (d=-4), A321856 (d=-3), A321857 (d=5), A071838 (d=8), A321858 (d=12).",
				"Sequences of the form \"a(n) = -Sum_{i=1..n} Kronecker(d,prime(i))\" with |d| \u003c= 12: A321865 (d=-11), A320858 (d=-8), A321864 (d=-7), A038698 (d=-4), this sequence (d=-3), A321862 (d=5), A321861 (d=8), A321863 (d=12)."
			],
			"keyword": "sign,nice",
			"offset": "1,3",
			"author": "_Roger Hui_, Dec 22 2005",
			"references": 22,
			"revision": 30,
			"time": "2018-12-22T16:47:41-05:00",
			"created": "2006-01-24T03:00:00-05:00"
		}
	]
}