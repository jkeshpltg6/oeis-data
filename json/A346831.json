{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A346831",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 346831,
			"data": "1,0,1,-1,0,1,2,-1,-2,1,1,0,-6,0,1,4,9,-4,-10,0,1,-1,0,15,0,-15,0,1,14,-1,-46,19,34,-19,-2,1,1,0,-28,0,70,0,-28,0,1,40,81,-88,-196,56,150,-8,-36,0,1,-1,0,45,0,-210,0,210,0,-45,0,1",
			"name": "Table read by rows, coefficients of the characteristic polynomials of the tangent matrices.",
			"comment": [
				"The tangent matrix M(n, k) is an N X N matrix defined with h = floor((N+1)/2) as:",
				"   M[n - k, k + 1] = if n \u003c h then 1 otherwise -1,",
				"   M[N - n + k + 1, N - k] = if n \u003c N - h then -1 otherwise 1,",
				"   for n in [1..N-1] and for k in [0..n-1], and 0 in the main antidiagonal.",
				"The name 'tangent matrix' derives from M(n, k) = signum(tan(Pi*(n + k)/(N + 1))) whenever the right side of this equation is defined."
			],
			"formula": [
				"The rows with even index equal those of A135670.",
				"The determinants of tangent matrices with even dimension are A152011."
			],
			"example": [
				"Table starts:",
				"[0]  1;",
				"[1]  0,  1;",
				"[2] -1,  0,   1;",
				"[3]  2, -1,  -2,    1;",
				"[4]  1,  0,  -6,    0,   1;",
				"[5]  4,  9,  -4,  -10,   0,   1;",
				"[6] -1,  0,  15,    0, -15,   0,   1;",
				"[7] 14, -1, -46,   19,  34, -19,  -2,   1;",
				"[8]  1,  0, -28,    0,  70,   0, -28,   0, 1;",
				"[9] 40, 81, -88, -196,  56, 150,  -8, -36, 0, 1.",
				".",
				"The first few tangent matrices:",
				"1       2          3              4                  5",
				"---------------------------------------------------------------",
				"0;   -1  0;    1  -1  0;    1  -1  -1   0;   1   1  -1  -1   0;",
				"      0  1;   -1   0  1;   -1  -1   0   1;   1  -1  -1   0   1;",
				"               0   1  1;   -1   0   1   1;  -1  -1   0   1   1;",
				"                            0   1   1  -1;  -1   0   1   1   1;",
				"                                             0   1   1   1  -1;"
			],
			"maple": [
				"TangentMatrix := proc(N) local M, H, n, k;",
				"   M := Matrix(N, N); H := iquo(N + 1, 2);",
				"   for n from 1 to N - 1 do for k from 0 to n - 1 do",
				"       M[n - k, k + 1] := `if`(n \u003c H, 1, -1);",
				"       M[N - n + k + 1, N - k] := `if`(n \u003c N - H, -1, 1);",
				"od od; M end:",
				"A346831Row := proc(n) if n = 0 then return 1 fi;",
				"   LinearAlgebra:-CharacteristicPolynomial(TangentMatrix(n), x);",
				"   seq(coeff(%, x, k), k = 0..n) end:",
				"seq(A346831Row(n), n = 0..10);"
			],
			"program": [
				"(Julia)",
				"using AbstractAlgebra",
				"function TangentMatrix(N)",
				"    M = zeros(ZZ, N, N)",
				"    H = div(N + 1, 2)",
				"    for n in 1:N - 1",
				"        for k in 0:n - 1",
				"            M[n - k, k + 1] = n \u003c H ? 1 : -1",
				"            M[N - n + k + 1, N - k] = n \u003c N - H ? -1 : 1",
				"        end",
				"    end",
				"M end",
				"function A346831Row(n)",
				"    n == 0 \u0026\u0026 return [ZZ(1)]",
				"    R, x = PolynomialRing(ZZ, \"x\")",
				"    S = MatrixSpace(ZZ, n, n)",
				"    M = TangentMatrix(n)",
				"    c = charpoly(R, S(M))",
				"    collect(coefficients(c))",
				"end",
				"for n in 0:9 println(A346831Row(n)) end"
			],
			"xref": [
				"Cf. A135670, A152011, A346837 (generalized tangent matrix)."
			],
			"keyword": "sign,tabl",
			"offset": "0,7",
			"author": "_Peter Luschny_, Sep 11 2021",
			"references": 7,
			"revision": 21,
			"time": "2021-09-12T04:24:31-04:00",
			"created": "2021-09-12T04:24:31-04:00"
		}
	]
}