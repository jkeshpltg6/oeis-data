{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A273359",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 273359,
			"data": "9,41,84,225,356,489,624,761,900,1209,1616,2025,2436,2849,3264,3681,4100,4521,4944,5369,5796,6225,6656,7089,7524,7961,8400,8841,9284,9729,10881,12164,13449,14736,16025,17316,18609,19904,21201,22500",
			"name": "Numbers k such that the decimal number concat(4,k) is a square.",
			"comment": [
				"Elements are squares of integers in (sqrt(41), sqrt(50)) * sqrt(10)^k without the leading 4 elements for nonnegative k. - _David A. Corneth_, May 20 2016"
			],
			"link": [
				"Nathan Fox, \u003ca href=\"/A273359/b273359.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"84 is a member because 484 = 22^2 is a square.",
				"0 is not a member because 40 is not a square.",
				"sqrt(410) \u003c 21 AND 22 \u003c sqrt(500) \u003c 23 so 21^2 = 441 and 22^2 = 484 give 41 and 84 respectively.",
				"64 \u003c sqrt(4100) \u003c 65 AND 70 \u003c sqrt(5000) \u003c 71 so 65^2 = 4225, 66^2 = 4356, ..., 70^2 = 4900 give 225, 356, ..., 900 respectively. - _David A. Corneth_, May 20 2016"
			],
			"maple": [
				"t1:=[];",
				"for k from 1 to 30000 do",
				"if issqr(k+4*10^length(k)) then t1:=[op(t1), k]; fi;",
				"od;",
				"t1;"
			],
			"mathematica": [
				"Select[Range[45000], IntegerQ[Sqrt[4 10^IntegerLength[#] + #]] \u0026] (* _Vincenzo Librandi_, Feb 20 2020 *)",
				"DeleteCases[(FromDigits[Drop[IntegerDigits[#], 1]]) \u0026 /@ Select[Range[3, 500]^2, IntegerDigits[#][[1]] == 4 \u0026\u0026 IntegerDigits[#][[2]] != 0 \u0026], 0] (* _Alonso del Arte_, Feb 20 2020 *)"
			],
			"program": [
				"a(n) = {my(k=1,t=0); while(n\u003ek, n-=k; t++; k=floor(sqrt(50)*sqrt(10^t))- ceil(sqrt(41)*sqrt(10^t))+1);(ceil(sqrt(41)*sqrt(10^t))+n-1)^2%(40*10^t)} \\\\ _David A. Corneth_, May 20 2016",
				"(MAGMA) [n: n in [1..50000 ] | IsSquare(Seqint(Intseq(n) cat Intseq(4)))]; // _Vincenzo Librandi_, Feb 20 2020",
				"(Scala) (3 to 500).map(n =\u003e n * n).filter(n =\u003e n.toString.startsWith(\"4\") \u0026\u0026 !n.toString.startsWith(\"40\")).map(n =\u003e Integer.parseInt(n.toString.substring(1))) // _Alonso del Arte_, Feb 20 2020"
			],
			"xref": [
				"Cf. A272671, A273357, A273358, A273360, A273361, A273362, A273363, A273364."
			],
			"keyword": "nonn,base",
			"offset": "1,1",
			"author": "_Nathan Fox_, _Brooke Logan_, and _N. J. A. Sloane_, May 20 2016",
			"references": 8,
			"revision": 19,
			"time": "2020-02-20T14:03:35-05:00",
			"created": "2016-05-20T22:24:23-04:00"
		}
	]
}