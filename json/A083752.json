{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A083752",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 83752,
			"data": "393,786,1179,109,1965,2358,2751,218,3537,3930,4323,327,132,5502,5895,436,6681,7074,7467,545,8253,8646,9039,157,9825,264,10611,763,11397,11790,12183,872,481,13362,13755,981,184,14934,396,1090,16113,16506,16899,1199",
			"name": "Minimal k \u003e n such that (4k+3n)(4n+3k) is a square.",
			"comment": [
				"A problem of elementary geometry lead to the search for squares of the form (4*a^2+3*b^2)(4*b^2+3*a^2). I could not find any such squares except when a=b. See link to ZS.",
				"Letting j := 24k+25n in (4k+3n)(4n+3k)=x^2 yields the Pell-like equation j^2 - 48 x^2 = 49 n^2.  The recurrence relationship for solutions to Pell equations implies that if k,x is a solution for n, then so is k1=18817k+19600n-5432x, x1=18817x-65184k-67900n.  As a result, if there is a solution with 109/4n \u003c k \u003c 393n, then there is also one with n \u003c k \u003c 109/4n, so either n \u003c a(n) \u003c= 109/4n or a(n)=393n. - _David Applegate_, Jan 09 2014"
			],
			"link": [
				"Charles R Greathouse IV, \u003ca href=\"/A083752/b083752.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Zak Seidov, \u003ca href=\"/A083752/a083752.pdf\"\u003eTwo \"triangles\" in a right triangle\u003c/a\u003e [Cached copy, pdf file, with permission]"
			],
			"formula": [
				"(4a(n)+3n)(4n+3a(n)) is a square.",
				"n \u003c a(n) \u003c= 393n. - _Charles R Greathouse IV_, Dec 13 2013"
			],
			"example": [
				"a(24)=157 because (4*157+3*24)(3*157+4*24)= 396900=630*630."
			],
			"maple": [
				"a:= proc(n) local k; for k from n+1",
				"      while not issqr((4*k+3*n)*(4*n+3*k)) do od; k",
				"    end:",
				"seq(a(n), n=1..50);  # _Alois P. Heinz_, Dec 13 2013"
			],
			"mathematica": [
				"a[n_] := For[k = n + 1, True, k++, If[IntegerQ[Sqrt[(4k+3n)(4n+3k)]], Return[k]]]; Table[an = a[n]; Print[an]; an, {n, 1, 50}] (* _Jean-François Alcover_, Oct 31 2016 *)"
			],
			"program": [
				"(PARI) a(n)=my(k=n+1); while(!issquare((4*k+3*n)*(4*n+3*k)), k++); k \\\\ _Charles R Greathouse IV_, Dec 13 2013",
				"(PARI) diff(v)=vector(#v-1,i,v[i+1]-v[i])",
				"a(n)=my(v=select(k-\u003eissquare(12*Mod(k,n)^2),[0..n-1])); forstep(k=n+v[1], 393*n, diff(concat(v,n)), if(issquare((4*k+3*n)*(4*n+3*k)) \u0026\u0026 k\u003en, return(k))) \\\\ _Charles R Greathouse IV_, Dec 13 2013",
				"(PARI) a(n)=for(k=n+1, 109*n\\4, if(issquare((4*k+3*n)*(4*n+3*k)), return(k))); 393*n \\\\ _Charles R Greathouse IV_, Jan 09 2014",
				"(Sage)",
				"def a(n):",
				"    k = n + 1",
				"    while not is_square((4*k+3*n)*(4*n+3*k)):",
				"        k += 1",
				"    return k",
				"[a(n) for n in (1..44)] # _Peter Luschny_, Jun 25 2014",
				"(Haskell)",
				"a083752 n = head [k | k \u003c- [n+1..], a010052 (12*(k+n)^2 + k*n) == 1]",
				"-- _Reinhard Zumkeller_, Apr 06 2015"
			],
			"xref": [
				"Cf. A085018, A085019.",
				"Cf. A010052, A235188."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Zak Seidov_, Jun 17 2003",
			"ext": [
				"a(12) corrected by _Charles R Greathouse IV_, Dec 13 2013"
			],
			"references": 4,
			"revision": 47,
			"time": "2016-10-31T10:21:22-04:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}