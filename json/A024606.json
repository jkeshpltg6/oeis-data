{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A024606",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 24606,
			"data": "7,13,19,21,28,31,37,39,43,49,52,57,61,63,67,73,76,79,84,91,93,97,103,109,111,112,117,124,127,129,133,139,147,148,151,156,157,163,169,171,172,175,181,183,189,193,196,199,201,208,211,217,219,223,228,229,237,241,244,247",
			"name": "Numbers of form x^2 + xy + y^2 with distinct x and y \u003e 0.",
			"comment": [
				"Alternatively, numbers expressible in more than one way as i^2 - ij + j^2, where 1 \u003c= i \u003c= j or 1 \u003c= i \u003c j. The following argument shows that the conditions i \u003c= j or i \u003c j are here equivalent. Note first that i^2 - ij + j^2 = (j - i)^2 - (j - i)*j + j^2, so the only non-duplicated values i^2 - ij + j^2 with 1 \u003c= i \u003c j are when j = 2i, whence i^2 - ij + j^2 = 3i^2. On the other hand, the values with i = j are j^2. There are no integer solutions to 3i^2 = j^2 with i \u003e= 1. - _Franklin T. Adams-Watters_, May 03 2006",
				"Numbers whose prime factorization contains at least one prime congruent to 1 mod 6 and any prime factor congruent to 2 mod 3 has even multiplicity. - _Franklin T. Adams-Watters_, May 03 2006",
				"This is a subsequence of Loeschian numbers A003136, closed under multiplication. Its primitive elements are those with exactly one prime factor of form 6k + 1 with multiplicity one (A232436). - _Jean-Christophe Hervé_, Nov 22 2013",
				"a(1)*a(2)*a(3) = 1729, the Hardy-Ramanujan taxicab number. 1729 is then in the sequence, by the argument of the preceding comment. - _Jean-Christophe Hervé_, Nov 24 2013",
				"1729 is also the least term that can be written in 4 distinct ways in the given form. Sequence A024614 does not include the restriction x != y, it is the disjoint union of this sequence and A033428 (i.e., 3*x^2) (without 0). - _M. F. Hasler_, Mar 05 2018"
			],
			"link": [
				"G. Nebe and N. J. A. Sloane, \u003ca href=\"http://www.math.rwth-aachen.de/~Gabriele.Nebe/LATTICES/A2.html\"\u003eHome page for hexagonal (or triangular) lattice A2\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eA Multisection of q-Series\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e",
				"\u003ca href=\"/index/Aa#A2\"\u003eIndex entries for sequences related to A2 = hexagonal = triangular lattice\u003c/a\u003e"
			],
			"formula": [
				"A004016(a(n)) \u003e= 12. - _Jean-Christophe Hervé_, Nov 24 2013"
			],
			"example": [
				"a(1) = 7 = 1^2 + 2 + 2^2."
			],
			"mathematica": [
				"Take[Union[Flatten[Table[x^2 + x*y + y^2, {x, 15}, {y, x - 1}]]], 60] (* _Robert G. Wilson v_, Nov 24 2013 *)"
			],
			"program": [
				"(PARI) for(k=1,247,my(a088534=sum(x=0,sqrt(k\\3),sum(y=max(x,sqrtint(k-x^2)\\2),sqrtint(k-2*x^2),x^2+x*y+y^2==k)),a004016d6=sumdiv(k,d,(d%3==1)-(d%3==2)));if(a088534!=a004016d6,print1(k,\", \"))) \\\\ _Hugo Pfoertner_, Sep 22 2019"
			],
			"xref": [
				"Cf. A003136, A004016, A024614, A074628, A088534, A118886, A232436."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_Clark Kimberling_",
			"ext": [
				"Definition modified by _Alonso del Arte_ and _Jean-Christophe Hervé_, Nov 25 2013"
			],
			"references": 8,
			"revision": 53,
			"time": "2021-03-06T01:54:17-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}