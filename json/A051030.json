{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A051030",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 51030,
			"data": "2,172,14258,1183258,98196140,8149096378,676276803218,56122825570732,4657518245567522,386517891556533610,32076327480946722092,2661948663027021400042,220909662703761829481378,18332840055749204825554348,1521404814964480238691529490",
			"name": "Ramanujan's c-series: expansion of (2+8*x-10*x^2)/(1-82*x-82*x^2+x^3).",
			"comment": [
				"The \"amazing\" identity of Ramanujan is a(n)^3 + b(n)^3 = c(n)^3 + (-1)^n, where a(n) = A051028(n), b(n) = A051029(n) and c(n) = A051030(n). - _Emeric Deutsch_, Oct 14 2006"
			],
			"link": [
				"Harvey P. Dale, \u003ca href=\"/A051030/b051030.txt\"\u003eTable of n, a(n) for n = 0..500\u003c/a\u003e",
				"Kwang-Wu Chen, \u003ca href=\"https://www.fq.math.ca/Papers1/50-3/Kwang-WuChen.pdf\"\u003eExtensions of an amazing identity of Ramanujan\u003c/a\u003e, Fib. Q., 50 (2012), 227-230.",
				"J. H. Han and M. D. Hirschhorn, \u003ca href=\"http://www.jstor.org/stable/27642956\"\u003eAnother Look at an Amazing Identity of Ramanujan\u003c/a\u003e, Mathematics Magazine, Vol. 79 (2006), pp. 302-304.",
				"Michael D. Hirschhorn, \u003ca href=\"http://www.jstor.org/stable/2691416\"\u003eAn amazing identity of Ramanujan\u003c/a\u003e, Math. Mag. 68 (1995), no. 3, 199--201. MR1335148",
				"Michael D. Hirschhorn, \u003ca href=\"http://www.jstor.org/stable/2690530\"\u003eA Proof in the Spirit of Zeilberger of an Amazing Identity of Ramanujan\u003c/a\u003e, Math. Mag., 69.4 (1996), 267-269.",
				"J. McLaughlin, \u003ca href=\"http://www.fq.math.ca/Papers1/48-1/McLaughlin.pdf\"\u003eAn identity motivated by an amazing identity of Ramanujan\u003c/a\u003e, Fib. Q., 48 (No. 1, 2010), 34-38.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujansSumIdentity.html\"\u003eRamanujan's Sum Identity.\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (82, 82, -1)."
			],
			"formula": [
				"G.f.: (2+8*x-10*x^2)/((1+x)*(1-83*x+x^2)).",
				"X(n+1) = AX(n), where X(n) = transpose(A051028(n), A051029(n), A051030(n)) and A = matrix(3,3,[63,104,-68; 64,104,-67; 80,131,-85)]). - _Emeric Deutsch_, Oct 14 2006",
				"a(0) = 2, a(1) = 172, a(2) = 14258, a(n) = 82*a(n-1)+82*a(n-2)-a(n-3). - _Harvey P. Dale_, Dec 17 2012"
			],
			"maple": [
				"g:=(2+8*x-10*x^2)/(1-82*x-82*x^2+x^3): gser:=series(g,x=0,20): seq(coeff(gser,x,n),n=0..12); # _Emeric Deutsch_, Oct 14 2006"
			],
			"mathematica": [
				"CoefficientList[Series[(2+8x-10x^2)/(1-82x-82x^2+x^3),{x,0,30}],x] (* or *) LinearRecurrence[{82,82,-1},{2,172,14258},20] (* _Harvey P. Dale_, Dec 17 2012 *)"
			],
			"program": [
				"(PARI) Vec((2+8*x-10*x^2)/(1-82*x-82*x^2+x^3) + O(x^30)) \\\\ _Michel Marcus_, Feb 29 2016",
				"(MAGMA) I:=[2,172,14258]; [n le 3 select I[n] else 82*Self(n-1)+82*Self(n-2)-Self(n-3):n in [1..30]]; // _Vincenzo Librandi_, Feb 29 2016"
			],
			"xref": [
				"Cf. A051028, A051029."
			],
			"keyword": "nonn",
			"offset": "0,1",
			"author": "_Eric W. Weisstein_",
			"ext": [
				"Minor edits (g.f. and name) by _M. F. Hasler_, May 08 2016"
			],
			"references": 7,
			"revision": 37,
			"time": "2019-09-30T06:24:05-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}