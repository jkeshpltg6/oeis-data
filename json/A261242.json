{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A261242",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 261242,
			"data": "1,1,2,1,4,1,4,12,18,12,8,6,2,44,56,120,28,88,4,36,0,8",
			"name": "Irregular triangle T(n, k) of number of connected bisymmetric n X n matrices B_n with 0 or 1 entries, B_n[1,1] = 1 = B_n[1,n], and k islands of 0's.",
			"comment": [
				"The row length sequence is 1 for n = 1 and A000982(n-2) + 1 for n \u003e= 2, that is:  1, 1, 2, 3, 6, 9, 14, 19, 26, 33, 42, ... = A261243.",
				"This entry is motivated by A258643.",
				"For bisymmetric matrices see the Wikipedia link.",
				"For the number of independent entries of an n X n bisymmetric matrix B_n see a Jul 07 2015 comment on A002620(n+1), n \u003e= 1. For the binary case (only 0 and 1 entries) see A060656(n+1), and the _Dennis P. Walsh_ comment and link. If B_n[1,1] and B_n[1,n] is given then the four corners are fixed, and, for n \u003e= 3, there are A002620(n+1) - 2 = A014616(n-2) entries free.",
				"If the n X n bisymmetric matrix B_n of 0's and 1's with B_n[1, 1] = 1 = B_n[1, n] is considered as a grid of n^2 squares of length 1 (in some length unit) with the four corners filled with 1's and the other squares with 0 or 1 then a path between the centers of squares with step length 1 can be defined. No diagonal steps (length sqrt(2)) are allowed. B_n is called connected if there exists no path of 0's which dissects the grid into two parts.",
				"An island of 0's (a 0-island) in B_n is defined as a set of 0's for which each pair is connected by a path of 0's, and a 0 entry at the coast of a 0-island has at least one entry 1 one step away. A single square filled with a 0 is a 0-island if all four neighbors 1 step (of length 1) apart are filled with 1's. If k=0 there exists no such 0-island. See the n=4 examples with k \u003e=1 below. The k = 1 matrix has one simply connected 0-island of four squares. The four k = 2 matrices have two 0-islands consisting of one square each.",
				"See the link with the figures by K. N. where red squares stand for 1 and empty squares for 0. Each matrix appears there rotated by 45 degrees in the counterclockwise direction. The mirror operation means row reversion in the matrix B_n. In the figures this is a mirror operation w.r.t. the middle NW-SE diagonal. 0-islands appear in the figures as holes.",
				"For the row sums see A261244."
			],
			"link": [
				"Kival Ngaokrajang, \u003ca href=\"/A261242/a261242.pdf\"\u003eIllustration of T(n,k) for n = 1..5, k \u003e= 0\u003c/a\u003e, \u003ca href=\"/A261242/a261242_1.pdf\"\u003eT(6,0)\u003c/a\u003e, \u003ca href=\"/A261242/a261242_2.pdf\"\u003eT(6,1)\u003c/a\u003e, \u003ca href=\"/A261242/a261242_3.pdf\"\u003eT(6,2)\u003c/a\u003e, \u003ca href=\"/A261242/a261242_4.pdf\"\u003eT(6,4)\u003c/a\u003e, \u003ca href=\"/A261242/a261242_5.pdf\"\u003eT(6,k) for k = 3, 5, 6, 8\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Bisymmetric_matrix\"\u003eBisymmetric Matrix\u003c/a\u003e."
			],
			"example": [
				"The irregular triangle T(n, k) begins:",
				"n\\k   0   1    2   3   4  5   6   7   8  ...",
				"1:    1",
				"2:    1",
				"3:    2   1",
				"4:    4   1    4",
				"5:   12  18   12   8   6  2",
				"6:   44  56  120  28  88  4  36   0   8",
				"...",
				"n=4: k=0:",
				"[[1,1,1,1], [1,1,1,1], [1,1,1,1], [1,1,1,1]],",
				"[[1,0,0,1], [0,1,1,0], [0,1,1,0], [1,0,0,1]],",
				"[[1,1,0,1], [1,1,1,0], [0,1,1,1], [1,0,1,1]],",
				"[[1,0,1,1], [0,1,1,1], [1,1,1,0], [1,1,0,1]];",
				"     k=1:",
				"[[1,1,1,1], [1,0,0,1], [1,0,0,1], [1,1,1,1]];",
				"     k=2:",
				"[[1,1,1,1], [1,0,1,1], [1,1,0,1], [1,1,1,1]],",
				"[[1,1,1,1], [1,1,0,1], [1,0,1,1], [1,1,1,1]],",
				"[[1,1,0,1], [1,0,1,0], [0,1,0,1], [1,0,1,1]],",
				"[[1,0,1,1], [0,1,0,1], [1,0,1,0], [1,1,0,1]]."
			],
			"xref": [
				"Cf. A000982, A002620, A014616, A258643, A261243, A261244."
			],
			"keyword": "nonn,tabf,more",
			"offset": "1,3",
			"author": "_Wolfdieter Lang_ and _Kival Ngaokrajang_, Aug 18 2015",
			"references": 5,
			"revision": 36,
			"time": "2020-04-06T22:23:02-04:00",
			"created": "2015-08-18T12:13:54-04:00"
		}
	]
}