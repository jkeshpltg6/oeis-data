{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A184362",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 184362,
			"data": "1,-2,-3,0,0,6,0,8,0,0,0,0,-13,0,0,-16,0,0,0,0,0,0,23,0,0,0,27,0,0,0,0,0,0,0,0,-36,0,0,0,0,-41,0,0,0,0,0,0,0,0,0,0,52,0,0,0,0,0,58,0,0,0,0,0,0,0,0,0,0,0,0,-71,0,0,0,0,0,0,-78,0,0,0,0,0,0,0,0,0,0,0,0,0,0,93,0,0,0",
			"name": "G.f.: eta(x) + x*eta'(x).",
			"comment": [
				" The formulas specified in this entry use eta(x) to denote Dedekind's eta(q) function without the q^(1/24) factor."
			],
			"formula": [
				"G.f. A(x) satisfies:",
				"(1) [x^n] A(x)/eta(x)^(n+1) = 0 for n\u003e=1.",
				"(2) [x^n] A(x)/eta(x)^n = A109084(n) for n\u003e=0.",
				"(3) [x^n] A(x)/eta(x)^(n+2) = A109085(n) for n\u003e=0.",
				"(4) A(x)/eta(x) = 1 - Sum_{n\u003e=1} sigma(n)*x^n.",
				"(5) A(x) = 1 + Sum_{n\u003e=1} (-1)^n*[n(3n-1)/2+1 + (n(3n+1)/2+1)*x^n)] * x^(n(3n-1)/2).",
				"(6) A(x)*eta(x)^2 = Sum_{n\u003e=0} (-1)^n*(2n+1)*(n^2+n+6)/6*x^(n(n+1)/2)."
			],
			"example": [
				"G.f.: A(x) = 1 - 2*x - 3*x^2 + 6*x^5 + 8*x^7 - 13*x^12 - 16*x^15 + 23*x^22 + 27*x^26 - 36*x^35 - 41*x^40 +...",
				"Illustrate the property: [x^n] A(x)/eta(x)^(n+1) = 0",
				"in the table of coefficients of A(x)/eta(x)^(n+1) for n=0..10:",
				"[1, -1, -3, -4, -7, -6, -12, -8, -15, -13, -18,...,-sigma(n),...];",
				"[1,(0), -2, -6, -15, -28, -55, -90, -154, -240, -378,...];",
				"[1, 1,(0), -5, -20, -54, -130, -275, -555, -1050, -1924,...];",
				"[1, 2, 3,(0), -17, -72, -221, -572, -1350, -2958, -6160,...];",
				"[1, 3, 7, 10,(0), -63, -287, -930, -2580, -6475, -15162,...];",
				"[1, 4, 12, 26, 38,(0), -253, -1196, -4059, -11780, -31027,...];",
				"[1, 5, 18, 49, 105, 153,(0), -1062, -5175, -18140, -54544,...];",
				"[1, 6, 25, 80, 210, 442, 646,(0), -4615, -22990, -82671,...];",
				"[1, 7, 33, 120, 363, 924, 1926, 2816,(0), -20570, -104285,...];",
				"[1, 8, 42, 170, 575, 1668, 4161, 8602, 12585,(0), -93538,...];",
				"[1, 9, 52, 231, 858, 2756, 7766, 19071, 39182, 57343,(0),...]; ...",
				"so that the coefficient of x^n in A(x)/eta(x)^(n+1) is zero for n\u003e=1.",
				"Note: the g.f.s of the diagonals in the above table are powers of G(x),",
				"where G(x) = 1/eta(x*G(x)) is the g.f. of A109085.",
				"The g.f. of A184363 equals:",
				"A(x)*eta(x)^2 = 1 - 4*x + 10*x^3 - 21*x^6 + 39*x^10 - 66*x^15 +...+ (-1)^n*(2n+1)*(n^2+n+6)/6*x^(n(n+1)/2) +..."
			],
			"program": [
				"(PARI) {a(n)=polcoeff(eta(x+x*O(x^n)) + x*deriv(eta(x+x*O(x^n))),n)}"
			],
			"xref": [
				"Cf. A184363, A184365, A109084, A109085, A000203."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Paul D. Hanna_, Jan 18 2011",
			"ext": [
				"Example of g.f. corrected by _Paul D. Hanna_, Jan 18 2011",
				"Name changed slightly by _Paul D. Hanna_, Nov 27 2012"
			],
			"references": 3,
			"revision": 14,
			"time": "2012-11-27T05:52:42-05:00",
			"created": "2011-01-12T12:53:46-05:00"
		}
	]
}