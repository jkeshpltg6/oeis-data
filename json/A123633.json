{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A123633",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 123633,
			"data": "1,-3,3,5,-18,15,24,-75,57,86,-252,183,262,-744,522,725,-1998,1365,1852,-4986,3336,4436,-11736,7719,10103,-26322,17067,22040,-56682,36306,46336,-117867,74700,94378,-237744,149277,186926,-466836,290706,361126,-895014,553224",
			"name": "Expansion of (c(q^2)/c(q))^3 in powers of q where c() is a cubic AGM theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Cubic AGM theta functions: a(q) (see A004016), b(q) (A005928), c(q) (A005882)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A123633/b123633.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q / (chi(-q^3)^3 / chi(-q))^3 in powers of q where chi() is a Ramanujan theta function.",
				"Euler transform of period 6 sequence [ -3, 0, 6, 0, -3, 0, ...].",
				"G.f. A(x) satisfies  0 = f(A(x), A(x^2)) where f(u, v)=  u^2 - v - u*v * (6 + 8*v).",
				"G.f.: x * (Product_{k\u003e0} (1 - x^(2*k - 1)) / (1 - x^(6*k - 3))^3 )^3.",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (6 t)) = (1 / 8) g(t) where q = exp(2 Pi i t) and g() is the g.f. for A128642.",
				"A128636(n) = a(n) unless n = 0. Convolution inverse of A105559.",
				"Convolution cube of A092848."
			],
			"example": [
				"G.f. = q - 3*q^2 + 3*q^3 + 5*q^4 - 18*q^5 + 15*q^6 + 24*q^7 - 75*q^8 + 57*q^9 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ q / (QPochhammer[ q^3, q^6]^3 / QPochhammer[ q, q^2])^3, {q, 0, n}]; (* _Michael Somos_, Feb 19 2015 *)",
				"a[ n_] := SeriesCoefficient[ q (Product[ 1 - q^k, {k, 1, n, 2}] / Product[ 1 - q^k, {k, 3, n, 6}]^3)^3, {q, 0, n}]; (* _Michael Somos_, Feb 19 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = local(A); if( n\u003c1, 0, n--; A = x * O(x^n); polcoeff( (eta(x + A) / eta(x^2 + A))^3 * (eta(x^6 + A) / eta(x^3 + A))^9, n))};"
			],
			"xref": [
				"Cf. A092848, A105559, A128636, A128642."
			],
			"keyword": "sign",
			"offset": "1,2",
			"author": "_Michael Somos_, Oct 03 2006, Jan 21 2009",
			"references": 6,
			"revision": 14,
			"time": "2021-03-12T22:24:44-05:00",
			"created": "2006-10-09T03:00:00-04:00"
		}
	]
}