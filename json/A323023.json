{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A323023",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 323023,
			"data": "1,1,2,1,1,2,2,1,1,3,1,2,1,2,2,1,1,3,2,2,1,1,2,2,1,2,2,1,4,1,1,3,2,2,1,1,3,2,2,1,2,2,1,2,2,1,1,4,2,2,1,2,1,2,2,1,3,1,3,2,2,1,1,3,3,1,1,5,1,2,2,1,2,2,1,2,2,1,4,2,1,1,2,2,1,2,2",
			"name": "Irregular triangle read by rows where row n is the omega-sequence of n.",
			"comment": [
				"We define the omega-sequence of n to have length A323014(n), and the k-th term is Omega(red^{k-1}(n)), where Omega = A001222 and red^{k} is the k-th functional iteration of A181819.",
				"Except for n = 1, all rows end with 1. If n is not prime, the term in row n prior to the last is A304465(n)."
			],
			"example": [
				"The sequence of omega-sequences begins:",
				"   1:            26: 2 2 1      51: 2 2 1        76: 3 2 2 1",
				"   2: 1          27: 3 1        52: 3 2 2 1      77: 2 2 1",
				"   3: 1          28: 3 2 2 1    53: 1            78: 3 3 1",
				"   4: 2 1        29: 1          54: 4 2 2 1      79: 1",
				"   5: 1          30: 3 3 1      55: 2 2 1        80: 5 2 2 1",
				"   6: 2 2 1      31: 1          56: 4 2 2 1      81: 4 1",
				"   7: 1          32: 5 1        57: 2 2 1        82: 2 2 1",
				"   8: 3 1        33: 2 2 1      58: 2 2 1        83: 1",
				"   9: 2 1        34: 2 2 1      59: 1            84: 4 3 2 2 1",
				"  10: 2 2 1      35: 2 2 1      60: 4 3 2 2 1    85: 2 2 1",
				"  11: 1          36: 4 2 1      61: 1            86: 2 2 1",
				"  12: 3 2 2 1    37: 1          62: 2 2 1        87: 2 2 1",
				"  13: 1          38: 2 2 1      63: 3 2 2 1      88: 4 2 2 1",
				"  14: 2 2 1      39: 2 2 1      64: 6 1          89: 1",
				"  15: 2 2 1      40: 4 2 2 1    65: 2 2 1        90: 4 3 2 2 1",
				"  16: 4 1        41: 1          66: 3 3 1        91: 2 2 1",
				"  17: 1          42: 3 3 1      67: 1            92: 3 2 2 1",
				"  18: 3 2 2 1    43: 1          68: 3 2 2 1      93: 2 2 1",
				"  19: 1          44: 3 2 2 1    69: 2 2 1        94: 2 2 1",
				"  20: 3 2 2 1    45: 3 2 2 1    70: 3 3 1        95: 2 2 1",
				"  21: 2 2 1      46: 2 2 1      71: 1            96: 6 2 2 1",
				"  22: 2 2 1      47: 1          72: 5 2 2 1      97: 1",
				"  23: 1          48: 5 2 2 1    73: 1            98: 3 2 2 1",
				"  24: 4 2 2 1    49: 2 1        74: 2 2 1        99: 3 2 2 1",
				"  25: 2 1        50: 3 2 2 1    75: 3 2 2 1     100: 4 2 1"
			],
			"mathematica": [
				"red[n_]:=Times@@Prime/@Last/@If[n==1,{},FactorInteger[n]];",
				"omg[n_,k_]:=If[k==1,PrimeOmega[n],omg[red[n],k-1]];",
				"dep[n_]:=If[n==1,0,If[PrimeQ[n],1,1+dep[Times@@Prime/@Last/@If[n==1,{},FactorInteger[n]]]]];",
				"Table[omg[n,k],{n,100},{k,dep[n]}]"
			],
			"xref": [
				"Row lengths are A323014, or A182850 if we assume A182850(2) = 1.",
				"First column is empty if n = 1 and otherwise A001222(n).",
				"Second column is empty if n is 1 or prime and otherwise A001221(n).",
				"Third column is empty if n is 1, prime, or a power of a prime and otherwise A071625(n).",
				"Cf. A024619, A056239, A067340, A118914, A181819, A181821, A182857, A304464, A304465, A323022."
			],
			"keyword": "nonn,tabf",
			"offset": "1,3",
			"author": "_Gus Wiseman_, Jan 02 2019",
			"references": 62,
			"revision": 4,
			"time": "2019-01-02T23:14:39-05:00",
			"created": "2019-01-02T23:14:39-05:00"
		}
	]
}