{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A054994",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 54994,
			"data": "1,5,25,65,125,325,625,1105,1625,3125,4225,5525,8125,15625,21125,27625,32045,40625,71825,78125,105625,138125,160225,203125,274625,359125,390625,528125,690625,801125,1015625,1185665,1221025,1373125,1795625",
			"name": "Numbers of the form (q1^b1)(q2^b2)(q3^b3)(q4^b4)(q5^b5)... where q1=5, q2=13, q3=17, q4=29, q5=37, ... (A002144) and b1\u003e=b2\u003e=b3\u003e=b4\u003e=b5...",
			"comment": [
				"This sequence is related to Pythagorean triples regarding the number of hypotenuses which are in a particular number of total Pythagorean triples and a particular number of primitive Pythagorean triples.",
				"Least integer \"mod 4 prime signature\" values that are the hypotenuse of at least one primitive Pythagorean triple - _Ray Chandler_, Aug 26 2004",
				"See A097751 for definition of \"mod 4 prime signature\"; terms of A097752 with all prime factors of form 4*k+1."
			],
			"link": [
				"Ray Chandler, \u003ca href=\"/A054994/b054994.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PythagoreanTriple.html\"\u003ePythagorean Triple\u003c/a\u003e"
			],
			"formula": [
				"Sum_{n\u003e=1} 1/a(n) = Product_{n\u003e=1} 1/(1 - 1/A006278(n)) = 1.2707219403... - _Amiram Eldar_, Oct 20 2020"
			],
			"example": [
				"1=5^0, 5=5^1, 25=5^2, 65=(5)(13), 125=5^3, 325=(5^2)(13), 625=5^4, etc."
			],
			"mathematica": [
				"maxTerm = 10^15;(* this limit gives ~ 500 terms *) maxNumberOfExponents = 9;(* this limit has to be increased until the number of reaped terms no longer changes *) bmax = Ceiling[ Log[ maxTerm]/Log[q]]; q = Reap[For[k = 0 ; cnt = 0, cnt \u003c= maxNumberOfExponents, k++, If[PrimeQ[4*k + 1], Sow[4*k + 1]; cnt++]]][[2, 1]]; Clear[b]; b[maxNumberOfExponents + 1] = 0; iter = Sequence @@ Table[{b[k], b[k + 1], bmax[[k]]}, {k, maxNumberOfExponents, 1, -1}]; Reap[ Do[an = Product[q[[k]]^b[k], {k, 1, maxNumberOfExponents}]; If[an \u003c= maxTerm, Print[an]; Sow[an]], Evaluate[iter]]][[2, 1]] // Flatten // Union (* _Jean-François Alcover_, Jan 18 2013 *)"
			],
			"program": [
				"(PARI) list(lim)=",
				"{",
				"  my(u=[1], v=List(), w=v, pr, t=1);",
				"  forprime(p=5,,",
				"    if(p%4\u003e1, next);",
				"    t*=p;",
				"    if(t\u003elim, break);",
				"    listput(w,t)",
				"  );",
				"  for(i=1,#w,",
				"    pr=1;",
				"    for(e=1,logint(lim\\=1,w[i]),",
				"      pr*=w[i];",
				"      for(j=1,#u,",
				"        t=pr*u[j];",
				"        if(t\u003elim, break);",
				"        listput(v,t)",
				"      )",
				"    );",
				"    if(w[i]^2\u003clim, u=Set(concat(Vec(v),u)); v=List());",
				"  );",
				"  Set(concat(Vec(v),u));",
				"} \\\\ _Charles R Greathouse IV_, Dec 11 2016"
			],
			"xref": [
				"Subsequence of A097752.",
				"Cf. A002144, A006278, A071383, A097751, A097752, A097753, A097754, A097755, A097756."
			],
			"keyword": "easy,nonn",
			"offset": "1,2",
			"author": "Bernard Altschuler (Altschuler_B(AT)bls.gov), May 30 2000",
			"ext": [
				"More terms from _Henry Bottomley_, Mar 14 2001"
			],
			"references": 14,
			"revision": 29,
			"time": "2021-09-15T21:36:24-04:00",
			"created": "2000-06-15T03:00:00-04:00"
		}
	]
}