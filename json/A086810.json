{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A086810",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 86810,
			"data": "1,0,1,0,1,2,0,1,5,5,0,1,9,21,14,0,1,14,56,84,42,0,1,20,120,300,330,132,0,1,27,225,825,1485,1287,429,0,1,35,385,1925,5005,7007,5005,1430,0,1,44,616,4004,14014,28028,32032,19448,4862,0,1,54,936,7644,34398,91728",
			"name": "Triangle obtained by adding a leading diagonal 1,0,0,0,... to A033282.",
			"comment": [
				"Mirror image of triangle A133336. - _Philippe Deléham_, Dec 10 2008",
				"From _Tom Copeland_, Oct 09 2011: (Start)",
				"With polynomials",
				"P(0,t) = 0",
				"P(1,t) = 1",
				"P(2,t) = t",
				"P(3,t) = t + 2 t^2",
				"P(4,t) = t + 5 t^2 +  5 t^3",
				"P(5,t) = t + 9 t^2 + 21 t^3 + 14 t^4",
				"The o.g.f. A(x,t) = {1+x-sqrt[(1-x)^2-4xt]}/[2(1+t)] (see Drake et al.).",
				"B(x,t)= x-t x^2/(1-x)= x-t(x^2+x^3+x^4+...) is the comp. inverse in x.",
				"Let h(x,t) = 1/(dB/dx) = (1-x)^2/(1+(1+t)*x*(x-2)) = 1/(1-t(2x+3x^2+4x^3+...)), an o.g.f. in x for row polynomials in t of A181289. Then P(n,t) is given by (1/n!)(h(x,t)*d/dx)^n x, evaluated at x=0, A = exp(x*h(y,t)*d/dy) y, eval. at y=0, and dA/dx = h(A(x,t),t). These results are a special case of A133437 with u(x,t) = B(x,t), i.e., u_1=1 and (u_n)=-t for n \u003e 1. See A001003 for t = 1. (End)",
				"Let U(x,t) = [A(x,t)-x]/t, then U(x,0) = -dB(x,t)/dt and U satisfies dU/dt = UdU/dx, the inviscid Burgers' equation (Wikipedia), also called the Hopf equation (see Buchstaber et al.). Also U(x,t) = U(A(x,t),0) = U(x+tU,0) since U(x,0) = [x-B(x,t)]/t. - _Tom Copeland_, Mar 12 2012",
				"Diagonals of A132081 are essentially rows of this sequence. - _Tom Copeland_, May 08 2012",
				"T(r, s) is the number of [0,r]-covering hierarchies with s segments (see Kreweras). - _Michel Marcus_, Nov 22 2014",
				"From _Yu Hin Au_, Dec 07 2019: (Start)",
				"T(n,k) is the number of small Schröder n-paths (lattice paths from (0,0) to (2n,0) using steps U=(1,1), F=(2,0), D=(1,-1) with no F step on the x-axis) that has exactly k U steps.",
				"T(n,k) is the number of Schröder trees (plane rooted tree where each internal node has at least two children) with exactly n+1 leaves and k internal nodes. (End)"
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A086810/b086810.txt\"\u003eTable of n, a(n) for n = 0..11475\u003c/a\u003e (rows 0 \u003c= n \u003c= 150, flattened.)",
				"Yu Hin (Gary) Au, \u003ca href=\"https://arxiv.org/abs/1912.00555\"\u003eSome Properties and Combinatorial Implications of Weighted Small Schröder Numbers\u003c/a\u003e, arXiv:1912.00555 [math.CO], 2019.",
				"Paul Barry, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL16/Barry3/barry252.html\"\u003eOn the Inverses of a Family of Pascal-Like Matrices Defined by Riordan Arrays\u003c/a\u003e, Journal of Integer Sequences, 16 (2013), #13.5.6.",
				"Paul Barry, \u003ca href=\"https://arxiv.org/abs/1803.06408\"\u003eThree Études on a sequence transformation pipeline\u003c/a\u003e, arXiv:1803.06408 [math.CO], 2018.",
				"Paul Barry, \u003ca href=\"https://www.emis.de/journals/JIS/VOL22/Barry3/barry422.html\"\u003eGeneralized Catalan Numbers Associated with a Family of Pascal-like Triangles\u003c/a\u003e, J. Int. Seq., Vol. 22 (2019), Article 19.5.8.",
				"V. Buchstaber and E. Bunkova,\u003ca href=\"http://arxiv.org/abs/1010.0944\"\u003eElliptic formal group laws, integral Hirzebruch genera and Kirchever genera,\u003c/a\u003e, arXiv:1010.0944 [math-ph], 2010 (see p. 19).",
				"V. Buchstaber and T. Panov,\u003ca href=\"http://arxiv.org/abs/1102.1079\"\u003e Toric Topology. Chapter 1: Geometry and Combinatorics of Polytopes,\u003c/a\u003e, arXiv:1102.1079 [math.CO], 2011-2012 (see p. 41).",
				"G. Chatel, V. Pilaud, \u003ca href=\"http://arxiv.org/abs/1411.3704\"\u003eCambrian Hopf Algebras\u003c/a\u003e, arXiv:1411.3704 [math.CO], 2014-2015.",
				"T. Copeland, \u003ca href=\"http://tcjpn.wordpress.com/2015/12/21/generators-inversion-and-matrix-binomial-and-integral-transforms/\"\u003eGenerators, Inversion, and Matrix, Binomial, and Integral Transforms\u003c/a\u003e, 2015.",
				"T. Copeland, \u003ca href=\"https://tcjpn.wordpress.com/2014/09/17/compositional-inverse-pairs-the-inviscid-burgers-hopf-equation-and-the-stasheff-associahedra/\"\u003e Compositional inverse pairs, the Burgers-Hopf equation, and the Stasheff associahedra\u003c/a\u003e, 2014.",
				"T. Copeland, \u003ca href=\"https://tcjpn.wordpress.com/2011/04/11/lagrange-a-la-lah/\"\u003eLagrange a la Lah\u003c/a\u003e, 2011.",
				"B. Drake, Ira M. Gessel, and Guoce Xin, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL10/Gessel/gessel20.html\"\u003eThree Proofs and a Generalization of the Goulden-Litsyn-Shevelev Conjecture on a Sequence Arising in Algebraic Geometry,\u003c/a\u003e J. of Integer Sequences, Vol. 10 (2007), Article 07.3.7.",
				"G. Kreweras, \u003ca href=\"http://www.numdam.org/item?id=BURO_1973__20__3_0\"\u003eSur les hiérarchies de segments\u003c/a\u003e, Cahiers Bureau Universitaire Recherche Opérationnelle, Cahier 20, Inst. Statistiques, Univ. Paris, 1973, p. 21-22.",
				"G. Kreweras, \u003ca href=\"/A001844/a001844.pdf\"\u003eSur les hiérarchies de segments\u003c/a\u003e, Cahiers du Bureau Universitaire de Recherche Opérationnelle, Institut de Statistique, Université de Paris, #20 (1973). (Annotated scanned copy)",
				"J. Zhou, \u003ca href=\"http://arxiv.org/abs/1405.5296\"\u003eQuantum deformation theory of the Airy curve and the mirror symmetry of a point\u003c/a\u003e, arXiv preprint arXiv:1405.5296 [math.AG], 2014."
			],
			"formula": [
				"Triangle T(n, k) read by rows; given by [0, 1, 0, 1, 0, 1, ...] DELTA [1, 1, 1, 1, 1, 1, 1, 1, 1, ...] where DELTA is Deléham's operator defined in A084938.",
				"For k\u003e0, T(n, k) = binomial(n+k-1, n)*binomial(n+2k, k)/(n+k+1); T(0, 0) = 1 and T(n, 0) = 0 if n \u003e 0.",
				"Sum_{k\u003e=0} T(n, k)*2^k = A107841(n). - _Philippe Deléham_, May 26 2005",
				"Sum_{k\u003e=0} T(n-k, k) = A005043(n). - _Philippe Deléham_, May 30 2005",
				"T(n, k) = A108263(n+k, k). - _Philippe Deléham_, May 30 2005",
				"Sum_{k=0..n} T(n,k)*x^k = A000007(n), A001003(n), A107841(n), A131763(n), A131765(n), A131846(n), A131926(n), A131869(n), A131927(n) for x = 0, 1, 2, 3, 4, 5, 6, 7, 8, respectively. - _Philippe Deléham_, Nov 05 2007",
				"Sum_{k=0..n} T(n,k)*5^k*(-2)^(n-k) = A152601(n). - _Philippe Deléham_, Dec 10 2008",
				"Sum_{k=0..n} T(n,k)*(-1)^k*3^(n-k) = A154825(n). - _Philippe Deléham_, Jan 17 2009",
				"Umbrally, P(n,t) = Lah[n-1,-t*a.]/n! = (1/n)*Sum_{k=1..n-1} binomial(n-2,k-1)a_k t^k/k!, where (a.)^k = a_k = (n-1+k)!/(n-1)!, the rising factorial, and Lah(n,t) = n!*Laguerre(n,-1,t) are the Lah polynomials A008297 related to the Laguerre polynomials of order -1. - _Tom Copeland_, Oct 04 2014",
				"T(n, k) = binomial(n, k)*binomial(n+k, k-1)/n, for k \u003e= 0; T(0, 0) = 1 (see Kreweras, p. 21). - _Michel Marcus_, Nov 22 2014",
				"P(n,t) = Lah[n-1,-:Dt:]/n! t^(n-1) with (:Dt:)^k = (d/dt)^k t^k = k! Laguerre(k,0,-:tD:) with (:tD:)^j = t^j D^j. The normalized Laguerre polynomials of 0 order are given in A021009. - _Tom Copeland_, Aug 22 2016"
			],
			"example": [
				"Triangle starts:",
				"  1;",
				"  0,  1;",
				"  0,  1,  2;",
				"  0,  1,  5,  5;",
				"  0,  1,  9, 21, 14;",
				"  ..."
			],
			"mathematica": [
				"Table[Boole[n == 2] + If[# == -1, 0, Binomial[n - 3, #] Binomial[n + # - 1, #]/(# + 1)] \u0026[k - 1], {n, 2, 12}, {k, 0, n - 2}] // Flatten (* after _Jean-François Alcover_ at A033282, or *)",
				"Table[If[n == 0, 1, Binomial[n, k] Binomial[n + k, k - 1]/n], {n, 0, 10}, {k, 0, n}] // Flatten (* _Michael De Vlieger_, Aug 22 2016 *)"
			],
			"program": [
				"(PARI) t(n, k) = if (n==0, 1, binomial(n, k)*binomial(n+k, k-1)/n);",
				"tabl(nn) = {for (n=0, nn, for (k=0, n, print1(t(n,k), \", \");); print(););} \\\\ _Michel Marcus_, Nov 22 2014"
			],
			"xref": [
				"Diagonals: A000007, A000012, A000096, A033275, A033276, A033277, A033278, A033279, A000108, A002054, A002055, A002056, A007160, A033280, A033281.",
				"The diagonals (except for A000007) are also the diagonals of A033282.",
				"Row sums: A001003 (Schroeder numbers).",
				"Cf. A033282, A084938.",
				"Cf. A001003, A008297, A021009, A132081, A133437, A181289."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "0,6",
			"author": "_Philippe Deléham_, Aug 05 2003",
			"ext": [
				"Typo in a(60) corrected by _Michael De Vlieger_, Nov 21 2019"
			],
			"references": 25,
			"revision": 120,
			"time": "2020-01-10T12:28:34-05:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}