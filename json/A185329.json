{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A185329",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 185329,
			"data": "1,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,2,2,3,3,4,4,5,5,6,7,8,9,11,12,14,16,18,20,24,26,30,34,39,43,50,55,63,71,80,89,102,113,128,143,161,179,203,225,253,282,316,351,395,437,489,544,607,673,752,832,927,1028,1143",
			"name": "Number of partitions of n with parts \u003e= 9.",
			"comment": [
				"a(n) is also the number of not necessarily connected 2-regular graphs on n-vertices with girth at least 9 (all such graphs are simple). The integer i corresponds to the i-cycle; addition of integers corresponds to disconnected union of cycles.",
				"By removing a single part of size 9, an A026802 partition of n becomes an A185329 partition of n - 9. Hence this sequence is essentially the same as A026802.",
				"In general, if g\u003e=1 and g.f. = Product_{m\u003e=g} 1/(1-x^m), then a(n,g) ~ Pi^(g-1) * (g-1)! * exp(Pi*sqrt(2*n/3)) / (2^((g+3)/2) * 3^(g/2) * n^((g+1)/2)) ~ p(n) * Pi^(g-1) * (g-1)! / (6*n)^((g-1)/2), where p(n) is the partition function A000041(n). - _Vaclav Kotesovec_, Jun 02 2018"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A185329/b185329.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Jason Kimberley, \u003ca href=\"/wiki/User:Jason_Kimberley/E_k-reg_girth_ge_g_index\"\u003eIndex of sequences counting not necessarily connected k-regular simple graphs with girth at least g\u003c/a\u003e"
			],
			"formula": [
				"G.f.: Product_{m\u003e=9} 1/(1-x^m).",
				"a(n) = p(n) - p(n-1) - p(n-2) + p(n-5) + p(n-7) + p(n-9) - p(n-11) - 2*p(n-12) - p(n-13) - p(n-15) + p(n-16) + p(n-17) + 2*p(n-18) + p(n-19) + p(n-20) - p(n-21) - p(n-23) - 2*p(n-24) - p(n-25) + p(n-27) + p(n-29) + p(n-31) - p(n-34) - p(n-35) + p(n-36) where p(n)=A000041(n). - _Shanzhen Gao_",
				"This sequence is the Euler transformation of A185119.",
				"a(n) ~ exp(Pi*sqrt(2*n/3)) * 70*Pi^8 / (9*sqrt(3)*n^5). - _Vaclav Kotesovec_, Jun 02 2018",
				"G.f.: Sum_{k\u003e=0} x^(9*k) / Product_{j=1..k} (1 - x^j). - _Ilya Gutkovskiy_, Nov 28 2020"
			],
			"maple": [
				"seq(coeff(series(1/mul(1-x^(m+9), m = 0..80), x, n+1), x, n), n = 0..70); # _G. C. Greubel_, Nov 03 2019"
			],
			"mathematica": [
				"CoefficientList[Series[x^9/QPochhammer[x^9, x], {x,0,75}], x] (* _G. C. Greubel_, Nov 03 2019 *)"
			],
			"program": [
				"(PARI) my(x='x+O('x^70)); Vec(1/prod(m=0,80, 1-x^(m+9))) \\\\ _G. C. Greubel_, Nov 03 2019",
				"(MAGMA) R\u003cx\u003e:=PowerSeriesRing(Integers(), 70); Coefficients(R!( 1/(\u0026*[1-x^(m+9): m in [0..80]]) )); // _G. C. Greubel_, Nov 03 2019",
				"(Sage)",
				"def A185329_list(prec):",
				"    P.\u003cx\u003e = PowerSeriesRing(ZZ, prec)",
				"    return P( 1/product((1-x^(m+9)) for m in (0..80)) ).list()",
				"A185329_list(70) # _G. C. Greubel_, Nov 03 2019"
			],
			"xref": [
				"Not necessarily connected 2-regular graphs with girth at least g [partitions into parts \u003e= g]: A026807 (triangle); chosen g: A000041 (g=1 -- multigraphs with loops allowed), A002865 (g=2 -- multigraphs with loops forbidden), A008483 (g=3), A008484 (g=4), A185325(g=5), A185326 (g=6), A185327 (g=7), A185328 (g=8), this sequence (g=9).",
				"Not necessarily connected 2-regular graphs with girth exactly g [partitions with smallest part g]: A026794 (triangle); chosen g: A002865 (g=2), A026796 (g=3), A026797 (g=4), A026798 (g=5), A026799 (g=6), A026800(g=7), A026801 (g=8), A026802 (g=9), A026803 (g=10)."
			],
			"keyword": "nonn,easy",
			"offset": "0,19",
			"author": "_Jason Kimberley_, Feb 01 2012",
			"references": 20,
			"revision": 28,
			"time": "2020-11-29T05:38:32-05:00",
			"created": "2012-02-02T12:03:40-05:00"
		}
	]
}