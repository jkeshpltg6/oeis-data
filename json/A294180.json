{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A294180",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 294180,
			"data": "1,2,3,1,2,3,1,1,2,3,1,2,3,1,1,2,3,1,2,3,1,2,3,1,1,2,3,1,2,3,1,1,2,3,1,2,3,1,2,3,1,1,2,3,1,2,3,1,1,2,3,1,2,3,1,1,2,3,1,2,3,1,2,3,1,1,2,3,1,2,3,1,1,2,3,1,2,3,1,2,3,1,1,2,3,1,2,3,1,1,2,3,1,2,3,1,1,2,3,1,2,3,1,2,3,1,1,2",
			"name": "The 3-symbol Pell word.",
			"comment": [
				"In the Pell word A171588 = 0, 0, 1, 0, 0, 1, 0, 0, 0, ..., group the letters in overlapping blocks of length two: [0,0],[0,1],[1,0],[0,0],[0,1],[1,0],... Then code [0,0]-\u003e1, [0,1]-\u003e2, [1,0]-\u003e3. This gives (a(n)).",
				"(a(n)) is the unique fixed point of the 3-symbol Pell morphism",
				"    1 -\u003e 123, 2 -\u003e123, 3 -\u003e 1.",
				"The morphism and the fixed point are in standard form.",
				"Modulo a change of alphabet (1-\u003e0, 2-\u003e1, 3-\u003e2), this sequence is equal to A263844.",
				"From _Michel Dekking_, Feb 23 2018: (Start)",
				"The positions of 1 in (a(n)) are given by",
				"  A188376 = 1,4,7,8,11,14,15,18,...",
				"Why is this true? First, the Pell word b is given by",
				"    b(n) = [(n+1)(1-r)]-[n(1-r)], where r =1/sqrt(2).",
				"This can rewritten as",
				"    b(n) = [nr]-[(n+1)r]+1.",
				"Second,",
				"    1 occurs at n in (a(n))  \u003c=\u003e",
				"    00 occurs at n in (b(n)) \u003c=\u003e",
				"    b(n)+b(n+1) = 0          \u003c=\u003e",
				"    [nr]-[(n+2)r]+2 = 0      \u003c=\u003e",
				"    [(n+2)r]-[nr]-1 = 1      \u003c=\u003e",
				"    1 occurs at n in A188374.",
				"The positions of 2 in (a(n)) are given by A001952 - 1 = 2,5,9,12,16,..., since 2 occurs at n in (a(n))) if and only if 3 occurs at n+1 in (a(n)).",
				"The positions of 3 in (a(n)) are given by A001952 = 3,6,10,13,17,..., since 3 occurs at n in (a(n)) if and only if 1 occurs at n in (b(n)).",
				"The sequence of positions of 3 in (a(n)) is equal to the sequence b in Carlitz et al. The sequence of positions of 1 in (a(n)) seems to be equal to the sequence ad' in Carlitz et al. (End)",
				"See the comments of A188376 for a proof of the observation on the positions of 1 in (a(n)). - _Michel Dekking_, Feb 27 2018"
			],
			"link": [
				"Michel Dekking, \u003ca href=\"/A294180/b294180.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"L. Carlitz, R. Scoville and V. E. Hoggatt, Jr.,\u003ca href=\"http://www.fq.math.ca/Scanned/10-5/carlitz1.pdf\"\u003ePellian Representations\u003c/a\u003e, Fib. Quart. Vol. 10, No. 5, (1972), pp. 449-488.",
				"F. Michel Dekking, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL19/Dekking/dekk4.html\"\u003eMorphisms, Symbolic Sequences, and Their Standard Forms\u003c/a\u003e, Journal of Integer Sequences, Vol. 19 (2016), Article 16.1.1."
			],
			"formula": [
				"a(n) = floor((n+2)r)+floor((n+1)r)-2*floor(nr)+1, where r = 1-1/sqrt(2)."
			],
			"maple": [
				"a:=[seq(floor((n+2)*(1-1/sqrt(2)))+floor((n+1)*(1-1/sqrt(2)))-2*floor(n*(1-1/sqrt(2)))+1, n=1..130)];"
			],
			"mathematica": [
				"With[{r = 1 - 1/Sqrt[2]}, Table[Inner[Times, Map[Floor[(n + #) r] \u0026, Range[0, 2]], {-2, 1, 1}, Plus] + 1, {n, 108}]] (* _Michael De Vlieger_, Feb 15 2018 *)"
			],
			"program": [
				"(MAGMA) [Floor((n+2)*r)+Floor((n+1)*r)-2*Floor(n*r)+1 where r is 1-1/Sqrt(2): n in [1..90]]; // _Vincenzo Librandi_, Feb 23 2018"
			],
			"xref": [
				"Cf. A270788, A263844, A188376."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Michel Dekking_, Feb 11 2018",
			"references": 6,
			"revision": 37,
			"time": "2018-03-03T03:41:18-05:00",
			"created": "2018-02-21T05:43:11-05:00"
		}
	]
}