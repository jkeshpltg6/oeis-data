{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A215794",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 215794,
			"data": "-1,-31,-609,-11711,-224833,-4315871,-82846113,-1590286719,-30526618241,-585978870687,-11248256653025,-215917815567167,-4144686996149441,-79560041170858591,-1527208244431770145,-29315784501060168447,-562736106255347592449",
			"name": "a(n) = -7^n*A(2*n+1), where A(n) = A(n-1) + A(n-2) + A(n-3)/7, with A(0)=3, A(1)=1, A(2)=3.",
			"comment": [
				"The Berndt-type sequence number 12 for the argument 2Pi/7",
				"  defined by the relation sqrt(7)*a(n) = t(1)^(2*n+1) + t(2)^(2*n+1) + t(4)^(2*n+1) = (-sqrt(7) + 4*s(1))^(2*n+1) + (-sqrt(7) + 4*s(2))^(2*n+1) + (-sqrt(7) + 4*s(4))^(2*n+1), where t(j) := tan(2*Pi*j/7) and s(j) := sin(2*Pi*j/7) (the respective sum with even powers in A108716 are given, see also A215828). We note that sqrt(7)*a(n) = B(2*n+1), where B(n) is defined in the comments to A215575. From Witula-Slota's (Section 6) and Witula's (Remark 11) papers it follows that B(n) is equal to the product (-sqrt(7))^n by the value of big omega function with index n for the argument 2*i/sqrt(7). The last value is equal to A(n). The respective recurrence relation for A(n) from the following decomposition follow (see Witula-Slota's paper for details): (X-1-2*i*d*s(1))*(X-1-2*i*d*s(2))*(X-1- 2*i*d*s(4)) = X^3 - (3+i*sqrt(7))*X^2 + (3+i*2*sqrt(7)*d)*X - (1+i*sqrt(7)*d + i*sqrt(7)*d^3), since the big omega function with index n for the argument d is equal to the sum: (1 + 2*i*d*s(1))^n + (1 + 2*i*d*s(2))^n + (1 + 2*i*d*s(4))^n and it is equal to 3 for n=0, 3 + i*sqrt(7)*d for n=1, and at last 3 + 2*i*sqrt(7)*d - 7*d^2 for n=2.",
				"The sequence a(n+1)/a(n) is decreasing and convergent to (t(2))^2 = 19,195669... Moreover we have floor(a(n+1)/a(n)) = 19 for every n=1,2,..."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A215794/b215794.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"Roman Witula and Damian Slota, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL10/Slota/witula13.html\"\u003eNew Ramanujan-Type Formulas and Quasi-Fibonacci Numbers of Order 7\u003c/a\u003e, Journal of Integer Sequences, Vol. 10 (2007), Article 07.5.6",
				"Roman Witula, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL12/Witula/witula17.html\"\u003eRamanujan Type Trigonometric Formulas: The General Form for the Argument 2*Pi/7\u003c/a\u003e, Journal of Integer Sequences, Vol. 12 (2009), Article 09.8.5",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (21,-35,7)."
			],
			"formula": [
				"G.f.: -(1+10*x-7*x^2)/(1-21*x+35*x^2-7*x^3). [_Bruno Berselli_, Aug 30 2012]",
				"a(n) = -A275195(2*n-1)/(7^n). - _Kai Wang_, Aug 02 2016"
			],
			"example": [
				"We have -31*sqrt(7) = t(1)^3 + t(2)^3 + t(4)^3."
			],
			"mathematica": [
				"  LinearRecurrence[{21, -35, 7}, {-1, -31, -609}, 17] (* _Bruno Berselli_, Aug 30 2012 *)"
			],
			"program": [
				"(MAGMA) m:=17; R\u003cx\u003e:=PowerSeriesRing(Integers(), m); Coefficients(R!(-(1+10*x-7*x^2)/(1-21*x+35*x^2-7*x^3)));  // _Bruno Berselli_, Aug 30 2012",
				"(MAGMA) I:=[-1, -31, -609]; [n le 3 select I[n] else 21*Self(n-1)-35*Self(n-2)+7*Self(n-3): n in [1..20]]; // _Vincenzo Librandi_, Mar 19 2013"
			],
			"xref": [
				"Cf. A215575, A108716, A215828, A275195."
			],
			"keyword": "sign,easy",
			"offset": "0,2",
			"author": "_Roman Witula_, Aug 23 2012",
			"references": 16,
			"revision": 32,
			"time": "2016-09-01T13:11:17-04:00",
			"created": "2012-08-24T21:14:30-04:00"
		}
	]
}