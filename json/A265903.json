{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A265903",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 265903,
			"data": "1,3,2,5,7,4,6,12,15,8,9,14,27,31,16,10,21,30,58,63,32,11,24,48,62,121,127,64,13,26,54,106,126,248,255,128,17,29,57,116,227,254,503,511,256,18,38,61,120,242,475,510,1014,1023,512,19,42,86,125,247,496,978,1022,2037,2047,1024,20,45,96,192,253,502,1006,1992,2046,4084,4095,2048",
			"name": "Square array read by descending antidiagonals: A(1,k) = A188163(k), and for n \u003e 1, A(n,k) = A087686(1+A(n-1,k)).",
			"comment": [
				"Square array A(n,k) [where n is row and k is column] is read by descending antidiagonals: A(1,1), A(1,2), A(2,1), A(1,3), A(2,2), A(3,1), etc.",
				"For n \u003e= 3, each row n lists the numbers that appear n times in A004001. See also A051135."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A265903/b265903.txt\"\u003eTable of n, a(n) for n = 1..210; the first 20 antidiagonals of array\u003c/a\u003e",
				"\u003ca href=\"/index/Ho#Hofstadter\"\u003eIndex entries for Hofstadter-type sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"For the first row n=1, A(1,k) = A188163(k), for rows n \u003e 1, A(n,k) = A087686(1+A(n-1,k))."
			],
			"example": [
				"The top left corner of the array:",
				"     1,    3,    5,    6,     9,    10,    11,    13,    17,    18,    19",
				"     2,    7,   12,   14,    21,    24,    26,    29,    38,    42,    45",
				"     4,   15,   27,   30,    48,    54,    57,    61,    86,    96,   102",
				"     8,   31,   58,   62,   106,   116,   120,   125,   192,   212,   222",
				"    16,   63,  121,  126,   227,   242,   247,   253,   419,   454,   469",
				"    32,  127,  248,  254,   475,   496,   502,   509,   894,   950,   971",
				"    64,  255,  503,  510,   978,  1006,  1013,  1021,  1872,  1956,  1984",
				"   128,  511, 1014, 1022,  1992,  2028,  2036,  2045,  3864,  3984,  4020",
				"   256, 1023, 2037, 2046,  4029,  4074,  4083,  4093,  7893,  8058,  8103",
				"   512, 2047, 4084, 4094,  8113,  8168,  8178,  8189, 16006, 16226, 16281",
				"  1024, 4095, 8179, 8190, 16292, 16358, 16369, 16381, 32298, 32584, 32650",
				"  ..."
			],
			"program": [
				"(Scheme)",
				"(define (A265903 n) (A265903bi (A002260 n) (A004736 n)))",
				"(define (A265903bi row col) (if (= 1 row) (A188163 col) (A087686 (+ 1 (A265903bi (- row 1) col)))))"
			],
			"xref": [
				"Inverse permutation: A267104.",
				"Transpose: A265901.",
				"Row 1: A188163.",
				"Row 2: A266109.",
				"Row 3: A267103.",
				"For the known and suspected columns, see the rows listed for transposed array A265901.",
				"Cf. A004001, A051135, A087686.",
				"Cf. A265900 (main diagonal), A265909 (submain diagonal).",
				"Cf. A162598 (column index of n in array), A265332 (row index of n in array).",
				"Cf. also permutations A267111, A267112."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Antti Karttunen_, Dec 18 2015",
			"references": 17,
			"revision": 39,
			"time": "2016-09-20T13:24:50-04:00",
			"created": "2016-01-24T23:47:49-05:00"
		}
	]
}