{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A152532",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 152532,
			"data": "4,11,41,69,161,213,353,505,655,1011,1197,1509,1841,2185,2667,3115,3831,4197,4749,5463,5901,6865,7873,8795,9789,10601,11013,11873,13617,14549,17137,17935,20135,20691,23091,24299,25893,27865",
			"name": "a(n) = prime(n) * prime(n+2) - 2 * prime(n+1).",
			"comment": [
				"Before this sequence, a(24) = 8795 was an uninteresting number, see References and Links. For example: 8795 was mentioned in Sloane's Gap paper, pages 4-5: Which numbers do not appear in Sloane's encyclopedia? At the time of an initial calculation conducted in August 2008 by Philippe Guglielmetti, the smallest absent number tracked down was 8795."
			],
			"reference": [
				"Bartolo Luque, La brecha de Sloane: Tras la huella sociológica de las matemáticas, Investigación y Ciencia, Edición española de Scientific American, julio de 2014, p. 90-91."
			],
			"link": [
				"Ingo Althofer, \u003ca href=\"http://www.littlegolem.net/jsp/forum/topic2.jsp?forum=1\u0026amp;topic=1830\"\u003eIs 8795 a boring number?\u003c/a\u003e",
				"Nicolas Gauvrit, Jean-Paul Delahaye, Hector Zenil, \u003ca href=\"http://arxiv.org/abs/1101.4470\"\u003eSloane's Gap: Do Mathematical and Social Factors Explain the Distribution of Numbers in the OEIS?\u003c/a\u003e, arXiv:1101.4470 [math.PR], p. 4-5.",
				"Nicolas Gauvrit, Hector Zenil, Jean-Paul Delahaye, \u003ca href=\"http://www.ehess.fr/revue-msh/pdf/N194R1413.pdf\"\u003eLe fossé de Sloane\u003c/a\u003e, Math. \u0026 Sci. hum. / Mathematics and Social Sciences,1413, n° 194, Summer 2011 (in French).",
				"Charles R Greathouse IV, \u003ca href=\"http://math.crg4.com/uninteresting.html\"\u003eUninteresting numbers\u003c/a\u003e."
			],
			"formula": [
				"a(n) = A000040(n)*A000040(n+2) - 2*A000040(n+1) = A090076(n) - A100484(n+1).",
				"a(n) ~ n^2 log^2 n. - _Charles R Greathouse IV_, Sep 14 2015"
			],
			"example": [
				"For n = 2, prime(2) = 3, prime(2+1) = 5 and prime(2+2) = 7, so a(2) = 3*7 - 2*5 = 21 - 10 = 11.",
				"For n = 24, prime(24) = 89, prime(24+1) = 97 and prime(24+2) = 101, so a(24) = 89*101 - 2*97 = 8989 - 194 = 8795."
			],
			"maple": [
				"seq(ithprime(n)*ithprime(n+2)-2*ithprime(n+1), n=1..1000); # _Robert Israel_, Dec 21 2014"
			],
			"mathematica": [
				"First[#]Last[#]-2#[[2]]\u0026/@Partition[Prime[Range[100]],3,1] (* _Harvey P. Dale_, Jun 16 2011 *)"
			],
			"program": [
				"(PARI) a(n,p=prime(n))=my(q=nextprime(p+1)); p*nextprime(q+1) - 2*q",
				"apply(p-\u003ea(0,p), primes(100)) \\\\ _Charles R Greathouse IV_, Sep 14 2015"
			],
			"xref": [
				"Cf. A000040, A090076, A100484, A111071, A152527, A152528, A152529, A152530, A152531."
			],
			"keyword": "easy,nonn",
			"offset": "1,1",
			"author": "_Omar E. Pol_, Dec 06 2008",
			"references": 5,
			"revision": 50,
			"time": "2015-09-14T14:43:33-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}