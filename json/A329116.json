{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A329116",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 329116,
			"data": "0,1,0,-1,-2,-1,0,1,2,3,2,1,0,-1,-2,-3,-4,-3,-2,-1,0,1,2,3,4,5,4,3,2,1,0,-1,-2,-3,-4,-5,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,6,5,4,3,2,1,0,-1,-2,-3,-4,-5,-6,-7,-8",
			"name": "Successively count to (-1)^(n+1)*n (n = 0, 1, 2, ... ).",
			"comment": [
				"Also x-coordinates of a point moving in counterclockwise triangular spiral (A329972 gives the y-coordinates)."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A329116/b329116.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"\u003ca href=\"/index/Con#coordinates_2D_curves\"\u003eIndex entries for sequences related to coordinates of 2D curves\u003c/a\u003e"
			],
			"formula": [
				"a(n) = (-1)^t * (t^2 - t - n) where t=ceiling(sqrt(n)).",
				"a(n) = (-1)^t * floor(t^2 - sqrt(n) - n) where t=ceiling(sqrt(n)).",
				"A053615(n) = abs(a(n)).",
				"abs(A196199(n)) = abs(a(n)).",
				"A255175(n) = a(n+1)."
			],
			"example": [
				"   y",
				"     |",
				"   4 |                         56",
				"     |                           \\",
				"     |                            \\",
				"     |                             \\",
				"   3 |                         30  55",
				"     |                         / \\   \\",
				"     |                        /   \\   \\",
				"     |                       /     \\   \\",
				"   2 |                     31  12  29  54",
				"     |                     /   / \\   \\   \\",
				"     |                    /   /   \\   \\   \\",
				"     |                   /   /     \\   \\   \\",
				"   1 |                 32  13   2  11  28  53",
				"     |                 /   /   / \\   \\   \\   \\",
				"     |                /   /   /   \\   \\   \\   \\",
				"     |               /   /   /     \\   \\   \\   \\",
				"   0 |             33  14   3   0---1  10  27  52",
				"     |             /   /   /             \\   \\   \\",
				"     |            /   /   /               \\   \\   \\",
				"     |           /   /   /                 \\   \\   \\",
				"  -1 |         34  15   4---5---6---7---8---9  26  51",
				"     |         /   /                             \\   \\",
				"     |        /   /                               \\   \\",
				"     |       /   /                                 \\   \\",
				"  -2 |     35  16--17--18--19--20--21--22--23--24--25  50",
				"     |     /                                             \\",
				"     |    /                                               \\",
				"     |   /                                                 \\",
				"  -3 | 36--37--38--39--40--41--42--43--44--45--46--47--48--49",
				"     |",
				"     +--------------------------------------------------------",
				"   x:  -6  -5  -4  -3  -2  -1   0   1   2   3   4   5   6   7",
				"We count as follows. Start at n=0 with 0.",
				"Next step is to count to 1: so we have 0, 1.",
				"Next step is to count to -2, so we have 0, 1, 0, -1, -2.",
				"Next we have to go to +3, so we have 0, 1, 0, -1, -2, -1, 0, 1, 2, 3.",
				"And so on."
			],
			"mathematica": [
				"a[n_] := Table[(-1)^(# + 1)*(-#^2 + # + k) \u0026[Ceiling@ Sqrt@ k], {k, 0, n}]; a[64]"
			],
			"xref": [
				"Cf. A053615, A196199, A339265 (first differences). Essentially the same as A255175."
			],
			"keyword": "sign,easy,look",
			"offset": "0,5",
			"author": "_Mikk Heidemaa_, Nov 13 2019",
			"references": 6,
			"revision": 80,
			"time": "2021-01-03T01:47:47-05:00",
			"created": "2019-12-05T20:09:52-05:00"
		}
	]
}