{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A329866",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 329866,
			"data": "1,3,16,30,33,48,55,56,59,60,67,68,72,79,95,97,110,112,118,120,121,125,134,135,137,143,145,158,160,195,196,219,220,225,231,241,250,258,270,280,286,291,292,315,316,351,381,382,390,391,393,399,415,416,431,432",
			"name": "Numbers whose binary expansion has its runs-resistance equal to its cuts-resistance minus 1.",
			"comment": [
				"For the operation of taking the sequence of run-lengths of a finite sequence, runs-resistance is defined to be the number of applications required to reach a singleton.",
				"For the operation of shortening all runs by 1, cuts-resistance is defined to be the number of applications required to reach an empty word."
			],
			"link": [
				"Claude Lenormand, \u003ca href=\"/A318921/a318921.pdf\"\u003eDeux transformations sur les mots\u003c/a\u003e, Preprint, 5 pages, Nov 17 2003."
			],
			"example": [
				"The sequence of terms together with their binary expansions begins:",
				"    1:         1",
				"    3:        11",
				"   16:     10000",
				"   30:     11110",
				"   33:    100001",
				"   48:    110000",
				"   55:    110111",
				"   56:    111000",
				"   59:    111011",
				"   60:    111100",
				"   67:   1000011",
				"   68:   1000100",
				"   72:   1001000",
				"   79:   1001111",
				"   95:   1011111",
				"   97:   1100001",
				"  110:   1101110",
				"  112:   1110000",
				"  118:   1110110",
				"  120:   1111000",
				"For example, 79 has runs-resistance 3 because we have (1001111) -\u003e (124) -\u003e (111) -\u003e (3), while the cuts-resistance is 4 because we have (1001111) -\u003e (0111) -\u003e (11) -\u003e (1) -\u003e (), so 79 is in the sequence."
			],
			"mathematica": [
				"runsres[q_]:=Length[NestWhileList[Length/@Split[#]\u0026,q,Length[#]\u003e1\u0026]]-1;",
				"degdep[q_]:=Length[NestWhileList[Join@@Rest/@Split[#]\u0026,q,Length[#]\u003e0\u0026]]-1;",
				"Select[Range[100],runsres[IntegerDigits[#,2]]-degdep[IntegerDigits[#,2]]==-1\u0026]"
			],
			"xref": [
				"Positions of -1's in A329867.",
				"The version for runs-resistance equal to cuts-resistance is A329865.",
				"Compositions with runs-resistance equal to cuts-resistance are A329864.",
				"Compositions with runs-resistance = cuts-resistance minus 1 are A329869.",
				"Runs-resistance of binary expansion is A318928.",
				"Cuts-resistance of binary expansion is A319416.",
				"Compositions counted by runs-resistance are A329744.",
				"Compositions counted by cuts-resistance are A329861.",
				"Binary words counted by runs-resistance are A319411 and A329767.",
				"Binary words counted by cuts-resistance are A319421 and A329860.",
				"Cf. A000975, A003242, A107907, A164707, A329738, A329868."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Gus Wiseman_, Nov 23 2019",
			"references": 5,
			"revision": 7,
			"time": "2019-11-24T10:00:27-05:00",
			"created": "2019-11-24T10:00:27-05:00"
		}
	]
}