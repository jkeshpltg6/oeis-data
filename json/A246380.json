{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A246380",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 246380,
			"data": "1,4,2,9,7,6,23,16,3,14,13,12,43,35,17,26,37,8,101,24,5,22,19,21,53,62,83,51,79,27,233,39,191,54,149,15,103,134,11,36,47,10,151,34,41,30,29,33,73,75,241,86,113,114,89,72,1153,108,443,40,593,296,547,56,167,245,173,76,563,194,1553,25",
			"name": "Permutation of natural numbers: a(1) = 1, a(2n) = nthcomposite(a(n)), a(2n-1) = nthprime(a(A064989(2n-1)-1)), where nthprime = A000040, nthcomposite = A002808, and A064989(n) shifts the prime factorization of n one step towards smaller primes.",
			"comment": [
				"Has an infinite number of infinite cycles. See comments in A246379."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A246380/b246380.txt\"\u003eTable of n, a(n) for n = 1..3098\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"a(1) = 1, a(2n) = nthcomposite(a(n)), a(2n-1) = nthprime(a(A064989(2n-1)-1)), where nthprime = A000040, nthcomposite = A002808, and A064989(n) shifts the prime factorization of n one step towards smaller primes.",
				"As a composition of related permutations:",
				"a(n) = A246378(A246376(n)).",
				"Other identities. For all n \u003e 1 the following holds:",
				"A010051(a(n)) = A000035(n). [Maps odd numbers larger than one to primes, and even numbers to composites, in some order. Permutations A246378 \u0026 A246682 have the same property]."
			],
			"program": [
				"(PARI)",
				"default(primelimit,(2^31)+(2^30));",
				"A002808(n) = { my(k=-1); while( -n + n += -k + k=primepi(n), ); n }; \\\\ This function from _M. F. Hasler_",
				"A064989(n) = {my(f); f = factor(n); if((n\u003e1 \u0026\u0026 f[1,1]==2), f[1,2] = 0); for (i=1, #f~, f[i,1] = precprime(f[i,1]-1)); factorback(f)};",
				"A246380(n) = if(1==n, 1, if(!(n%2), A002808(A246380(n/2)), prime(A246380(A064989(n)-1))));",
				"for(n=1, 3098, write(\"b246380.txt\", n, \" \", A246380(n)));",
				"(Scheme, with memoization-macro definec)",
				"(definec (A246380 n) (cond ((\u003c n 2) n) ((even? n) (A002808 (A246380 (/ n 2)))) (else (A000040 (A246380 (- (A064989 n) 1))))))"
			],
			"xref": [
				"Inverse: A246379.",
				"Similar or related permutations: A246376, A246378, A246363, A246364, A246366, A246368, A064216, A246682.",
				"Cf. A000035, A000040, A002808, A010051, A064989."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Antti Karttunen_, Aug 29 2014",
			"references": 6,
			"revision": 13,
			"time": "2014-09-01T11:47:37-04:00",
			"created": "2014-09-01T11:47:37-04:00"
		}
	]
}