{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A124838",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 124838,
			"data": "1,2,6,4,20,10,70,56,504,420,4620,3960,3432,6006,90090,80080,1361360,408408,369512,67184,470288,1293292,29745716,27457584,228813200,212469400,5736673800,5354228880,155272637520,291136195350,273491577450",
			"name": "Denominators of third-order harmonic numbers (defined by Conway and Guy, 1996).",
			"comment": [
				"Numerators are A124837. All fractions reduced. Thanks to Jonathan Sondow for verifying these calculations. He suggests that the equivalent definition in terms of first order harmonic numbers may be computationally simpler. We are happy with the description of A027612 Numerator of 1/n + 2/(n-1) + 3/(n-2) +...+ (n-1)/2 + n, but baffled by the description of A027611."
			],
			"reference": [
				"J. H. Conway and R. K. Guy, The Book of Numbers, New York: Springer-Verlag, pp. 143 and 258-259, 1996."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A124838/b124838.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/HarmonicNumber.html\"\u003eHarmonic Number\u003c/a\u003e. See equation for third-order harmonic numbers."
			],
			"formula": [
				"A124837(n)/A124838(n) = Sum_{i=1..n} A027612(n)/A027611(n+1).",
				"a(n) = denominator(Sum_{m=1..n} Sum_{L=1..m} Sum_{k=1..L} 1/k).",
				"a(n) = denominator(((n+2)!/(2!*n!)) * Sum_{k=3..n+2} 1/k). - _Alexander Adamchuk_, Nov 11 2006",
				"a(n) = A213999(n+2,n-1). - _Reinhard Zumkeller_, Jul 03 2012"
			],
			"example": [
				"a(1) = 1 = denominator of 1/1.",
				"a(2) = 2 = denominator of 1/1 + 5/2 = 7/2.",
				"a(3) = 6 = denominator of 7/2 + 13/3 = 47/6.",
				"a(4) = 4 = denominator of 47/6 + 77/12 = 57/4.",
				"a(5) = 20 = denominator of 57/4 + 87/10 = 549/20.",
				"a(6) = 10 = denominator of 549/20 + 223/20 = 341/10",
				"a(7) = 70 = denominator of 341/10 + 481/35 = 3349/70.",
				"a(8) = 1260 = denominator of 3349/70 + 4609/280 = 88327/1260.",
				"a(9) = 45 = denominator of 88327/1260 + 4861/252 = 3844/45.",
				"a(10) = 504 = denominator of 3844/45 + 55991/2520 = 54251/504, or, untelescoping:",
				"a(10) = 504 = denominator of 1/1 + 5/2 + 13/3 + 77/12 + 87/10 + 223/20 + 481/35 + 4609/252 + 4861/252 + 55991/2520 = 54251/504."
			],
			"mathematica": [
				"Table[Denominator[(n+2)!/2!/n!*Sum[1/k,{k,3,n+2}]],{n,1,40}] (* _Alexander Adamchuk_, Nov 11 2006 *)"
			],
			"program": [
				"(Haskell)",
				"a124838 n = a213999 (n + 2) (n - 1) -- _Reinhard Zumkeller_, Jul 03 2012"
			],
			"xref": [
				"Cf. A027611, A027612, A124837."
			],
			"keyword": "easy,frac,nonn",
			"offset": "1,2",
			"author": "_Jonathan Vos Post_, Nov 10 2006",
			"ext": [
				"Corrected and extended by _Alexander Adamchuk_, Nov 11 2006"
			],
			"references": 4,
			"revision": 23,
			"time": "2021-01-30T02:32:06-05:00",
			"created": "2006-12-06T03:00:00-05:00"
		}
	]
}