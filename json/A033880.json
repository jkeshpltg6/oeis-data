{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A033880",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 33880,
			"data": "-1,-1,-2,-1,-4,0,-6,-1,-5,-2,-10,4,-12,-4,-6,-1,-16,3,-18,2,-10,-8,-22,12,-19,-10,-14,0,-28,12,-30,-1,-18,-14,-22,19,-36,-16,-22,10,-40,12,-42,-4,-12,-20,-46,28,-41,-7,-30,-6,-52,12,-38,8,-34,-26,-58,48,-60,-28,-22",
			"name": "Abundance of n, or (sum of divisors of n) - 2n.",
			"comment": [
				"For no known n is a(n) = 1. If there is such an n it must be greater than 10^35 and have seven or more distinct prime factors (Hagis and Cohen 1982). - _Jonathan Vos Post_, May 01 2011",
				"a(n) = -1 iff n is a power of 2. a(n) = 1 - n iff n is prime. - _Omar E. Pol_, Jan 30 2014 [If a(n) = -1 then n is called a least deficient number or an almost perfect number. All the powers of 2 are least deficient numbers but it is not known if there exists a least deficient number that is not a power of 2. See A000079. - _Jianing Song_, Oct 13 2019]",
				"According to Deléglise (1998), the abundant numbers have natural density 0.2474 \u003c A(2) \u003c 0.2480. Since the perfect numbers having density 0, the deficient numbers have density 0.7520 \u003c 1 - A(2) \u003c 0.7526. - _Daniel Forgues_, Oct 10 2015",
				"2-abundance of n, a special case of the k-abundance of n, defined as (sum of divisors of n) - k*n, k \u003e= 1. - _Daniel Forgues_, Oct 24 2015",
				"Not to be confused with the abundancy of n, defined as (sum of divisors of n) / n. (Cf. A017665 / A017666.) - _Daniel Forgues_, Oct 25 2015"
			],
			"reference": [
				"Guy, R. K. \"Almost Perfect, Quasi-Perfect, Pseudoperfect, Harmonic, Weird, Multiperfect and Hyperperfect Numbers.\" Section B2 in Unsolved Problems in Number Theory, 2nd ed. New York: Springer-Verlag, pp. 45-53, 1994."
			],
			"link": [
				"J. G. Wurtzel, \u003ca href=\"/A033880/b033880.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e [This replaces an earlier b-file computed by T. D. Noe]",
				"Nichole Davis, Dominic Klyve and Nicole Kraght, \u003ca href=\"http://dx.doi.org/10.2140/involve.2013.6.493\"\u003eOn the difference between an integer and the sum of its proper divisors\u003c/a\u003e, Involve, Vol. 6 (2013), No. 4, 493-504; DOI: 10.2140/involve.2013.6.493",
				"Marc Deléglise, \u003ca href=\"http://projecteuclid.org/euclid.em/1048515661\"\u003eBounds for the density of abundant integers\u003c/a\u003e, Experiment. Math. Volume 7, Issue 2 (1998), 137-143.",
				"P. Hagis and G. L. Cohen, \u003ca href=\"http://dx.doi.org/10.1017/S1446788700018401\"\u003eSome Results Concerning Quasiperfect Numbers\u003c/a\u003e, J. Austral. Math. Soc. Ser. A 33, 275-286, 1982.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Abundance.html\"\u003eAbundance\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Abundancy.html\"\u003eAbundancy\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/QuasiperfectNumber.html\"\u003eQuasiperfect Number\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A000203(n) - A005843(n). - _Omar E. Pol_, Dec 14 2008",
				"a(n) = A001065(n) - n. - _Omar E. Pol_, Dec 27 2013"
			],
			"example": [
				"For n = 10 the divisors of 10 are 1, 2, 5, 10. The sum of proper divisors of 10 minus 10 is 1 + 2 + 5 - 10 = -2, so the abundance of 10 is a(10) = -2. - _Omar E. Pol_, Dec 27 2013"
			],
			"maple": [
				"with(numtheory); n-\u003esigma(n) - 2*n;"
			],
			"mathematica": [
				"Array[Total[Divisors[#]]-2#\u0026,70] (* _Harvey P. Dale_, Sep 16 2011 *)"
			],
			"program": [
				"(PARI) a(n)=sigma(n)-2*n \\\\ _Charles R Greathouse IV_, Nov 20 2012",
				"(MAGMA) [SumOfDivisors(n)-2*n: n in [1..100]]; // _Vincenzo Librandi_, Oct 11 2015"
			],
			"xref": [
				"Equals -A033879.",
				"a(n) = A000203(n) - A005843(n). - _Omar E. Pol_, Dec 14 2008",
				"a(n) = A001065(n) - n. - _Omar E. Pol_, Dec 27 2013",
				"Cf. A005100 (a(n) \u003c 0), A000396 (a(n) = 0) and A005101 (a(n) \u003e 0).",
				"See also A023197."
			],
			"keyword": "sign,nice",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Definition corrected Jul 04 2005"
			],
			"references": 98,
			"revision": 66,
			"time": "2019-10-14T01:24:34-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}