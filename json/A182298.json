{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A182298",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 182298,
			"data": "0,2,4,3,6,5,4,7,7,6,5,10,8,8,7,6,12,11,9,9,8,7,11,13,12,10,10,9,8,15,12,14,13,11,11,10,9,17,16,13,15,14,12,12,11,10",
			"name": "Smallest complementary perimeter, as defined in the comments, among all sets of nonnegative integers whose volume (sum) is n.",
			"comment": [
				"The volume and perimeter of a set S of nonnegative integers are introduced in the reference.  The volume is defined simply as the sum of the elements of S, and the perimeter is defined as the sum of the elements of S whose predecessor and successor are not both in S.  The complementary perimeter (introduced in the link) of S is the perimeter of the complement of S in the set of nonnegative integers."
			],
			"link": [
				"Patrick Devlin, \u003ca href=\"http://arxiv.org/abs/1107.2954\"\u003eSets with High Volume and Low Perimeter\u003c/a\u003e, arXiv:1107.2954 [math.CO], 2011.",
				"Patrick Devlin, \u003ca href=\"http://arxiv.org/abs/1202.1331\"\u003eInteger Subsets with High Volume and Low Perimeter\u003c/a\u003e, arXiv:1202.1331 [math.CO], 2012.",
				"Patrick Devlin, \u003ca href=\"http://www.emis.de/journals/INTEGERS/papers/m32/m32.Abstract.html\"\u003eInteger Subsets with High Volume and Low Perimeter\u003c/a\u003e, INTEGERS, Vol. 12, #A32.",
				"J. Miller, F. Morgan, E. Newkirk, L. Pedersen and D. Seferis, \u003ca href=\"http://www.jstor.org/stable/10.4169/math.mag.84.1.037\"\u003eIsoperimetric Sets of Integers\u003c/a\u003e, Math. Mag. 84 (2011) 37-42."
			],
			"formula": [
				"Following the notation in the link, for n \u003e= 0, write n = (0+1+2+...+f(n)) - g(n), be the representation of n with f(n) and g(n) minimal such that 0 \u003c= g(n) \u003c= f(n).  Then f(n) = A002024(n) = round(sqrt(2n)), and g(n) = A025581(n) = f(n)*(f(n)+1)/2 - n.",
				"Finally, let Q(n):=a(n), and let P(n):=A186053(n).  Then unless n is one of the 177 known counterexamples tabulated in the link, we have P(n) = f(n) + Q(g(n)), and Q(n) = 1 + f(n) + P(g(n))."
			],
			"example": [
				"For n=8, the set S={0,1,3,4} has volume (total sum) 8 and complementary perimeter (the sum of 2 and 5) is 7.  No other set of volume 8 has a smaller complementary perimeter, so a(8)=7.",
				"Similarly, for n=11, the set S={2,4,5} has volume 11=2+4+5 and complementary perimeter 10=1+3+6.  This is the smallest among all sets with volume 11, so a(11)=10."
			],
			"xref": [
				"Cf. A186053."
			],
			"keyword": "nonn,more",
			"offset": "0,2",
			"author": "_Patrick Devlin_, Apr 23 2012",
			"references": 6,
			"revision": 31,
			"time": "2020-02-27T22:46:35-05:00",
			"created": "2012-04-24T14:37:58-04:00"
		}
	]
}