{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A118196",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 118196,
			"data": "1,-1,1,-1,-1,31,-449,4223,377087,-79232513,13509592063,-74458331137,-5113818652864513,11766261105083555839,-22128578595003966668801,-88147548436159218430476289,3787186430286106428653327941631,-103331603080469761480767867413463041",
			"name": "Column 0 of the matrix inverse of triangle A117401(n,k) = (2^k)^(n-k).",
			"comment": [
				"The entire matrix inverse of triangle A117401 is determined by column 0 (this sequence): [A117401^-1](n,k) = a(n-k)*2^(k*(n-k)) for n\u003e=k\u003e=0. Any g.f. of the form: Sum_{k\u003e=0} b(k)*x^k may be expressed as: Sum_{n\u003e=0} c(n)*x^n/(1-2^n*x) by applying the inverse transformation: c(n) = Sum_{k=0..n} a(n-k)*b(k)*2^(k*(n-k))."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A118196/b118196.txt\"\u003eTable of n, a(n) for n = 0..75\u003c/a\u003e"
			],
			"formula": [
				"G.f.: 1 = Sum_{n\u003e=0} a(n)*x^n/(1-2^n*x).",
				"0^n = Sum_{k=0..n} a(k)*2^(k*(n-k)) for n\u003e=0.",
				"a(n) = (-1)*Sum_{j=0..n-1} 2^(j*(n-j))*a(j) with a(0) = 1 and a(1) = -1. - _G. C. Greubel_, Jun 30 2021"
			],
			"example": [
				"Recurrence at n=4:",
				"0 = a(0)*(2^0)^4 +a(1)*(2^1)^3 +a(2)*(2^2)^2 +a(3)*(2^3)^1 +a(4)*(2^4)^0 = 1*(2^0) - 1*(2^3) + 1*(2^4) - 1*(2^3) - 1*(2^0).",
				"The g.f. is illustrated by:",
				"1 = 1/(1-x) -1*x/(1-2*x) +1*x^2/(1-4*x) -1*x^3/(1-8*x) -1*x^4/(1-16*x) +31*x^5/(1-32*x) -449*x^6/(1-64*x) + 4223*x^7/(1-128*x) +..."
			],
			"mathematica": [
				"(* First program *)",
				"m = 17;",
				"M = Table[If[k \u003c= n, 2^((n-k)k), 0], {n, 0, m}, {k, 0, m}];",
				"Inverse[M][[All, 1]] (* _Jean-François Alcover_, Jun 13 2019 *)",
				"(* Second program *)",
				"a[n_]:= a[n]= If[n\u003c2, (-1)^n, -Sum[2^(j*(n-j))*a[j], {j, 0, n-1}]];",
				"Table[a[n], {n, 0, 30}] (* _G. C. Greubel_, Jun 30 2021 *)"
			],
			"program": [
				"(PARI) {a(n) = local(T=matrix(n+1,n+1,r,c,if(r\u003e=c,(2^(c-1))^(r-c)))); return((T^-1)[n+1,1])};",
				"(Sage)",
				"def A118196_list(len):",
				"    R, C = [1], [1]+[0]*(len-1)",
				"    for n in (1..len-1):",
				"        for k in range(n, 0, -1):",
				"            C[k] = C[k-1] / (2^k)",
				"        C[0] = -sum(C[k] for k in (1..n))",
				"        R.append(C[0]*2^(n*(n+1)/2))",
				"    return R",
				"print(A118196_list(18)) # _Peter Luschny_, Feb 20 2016",
				"(Sage)",
				"@CachedFunction",
				"def a(n): return (-1)^n if (n\u003c2) else -sum(2^(j*(n-j))*a(j) for j in (0..n-1))",
				"[a(n) for n in (0..30)] # _G. C. Greubel_, Jun 30 2021"
			],
			"xref": [
				"Cf. A117401, A118197."
			],
			"keyword": "sign",
			"offset": "0,6",
			"author": "_Paul D. Hanna_, Apr 15 2006",
			"references": 2,
			"revision": 20,
			"time": "2021-06-30T04:32:52-04:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}