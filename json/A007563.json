{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007563",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7563,
			"id": "M2751",
			"data": "0,1,1,3,8,25,77,258,871,3049,10834,39207,143609,532193,1990163,7503471,28486071,108809503,417862340,1612440612,6248778642,24309992576,94905791606,371691137827,1459935388202,5749666477454",
			"name": "Number of rooted connected graphs where every block is a complete graph.",
			"reference": [
				"F. Harary and E. M. Palmer, Graphical Enumeration, Academic Press, NY, 1973, p. 71, (3.4.13).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vaclav Kotesovec, \u003ca href=\"/A007563/b007563.txt\"\u003eTable of n, a(n) for n = 0..1600\u003c/a\u003e (first 200 terms from T. D. Noe)",
				"Maryam Bahrani and Jérémie Lumbroso, \u003ca href=\"http://arxiv.org/abs/1608.01465\"\u003eEnumerations, Forbidden Subgraph Characterizations, and the Split-Decomposition\u003c/a\u003e, arXiv:1608.01465 [math.CO], 2016.",
				"M. Bernstein and N. J. A. Sloane, \u003ca href=\"https://arxiv.org/abs/math/0205301\"\u003eSome canonical sequences of integers\u003c/a\u003e, arXiv:math/0205301 [math.CO], 2002; Linear Alg. Applications, 226-228 (1995), 57-72; erratum 320 (2000), 210. [Link to arXiv version]",
				"M. Bernstein and N. J. A. Sloane, \u003ca href=\"/A003633/a003633_1.pdf\"\u003eSome canonical sequences of integers\u003c/a\u003e, Linear Alg. Applications, 226-228 (1995), 57-72; erratum 320 (2000), 210. [Link to Lin. Alg. Applic. version together with omitted figures]",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=167\"\u003eEncyclopedia of Combinatorial Structures 167\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/transforms.txt\"\u003eTransforms\u003c/a\u003e"
			],
			"formula": [
				"Shifts left when Euler transform is applied twice.",
				"a(n) ~ c * d^n / n^(3/2), where d = 4.189610958393826965527036454524044275... (see A245566), c = 0.1977574301782950818433893126632477845870281049591883888... . - _Vaclav Kotesovec_, Jul 26 2014"
			],
			"maple": [
				"with(numtheory): etr:= proc(p) local b; b:= proc(n) option remember; if n=0 then 1 else (add(d*p(d), d=divisors(n)) +add(add(d*p(d), d=divisors(j)) *b(n-j), j=1..n-1))/n fi end end: b:= etr(a): c:= etr(b): a:= n-\u003e if n=0 then 0 else c(n-1) fi: seq(a(n), n=0..25); # _Alois P. Heinz_, Sep 06 2008"
			],
			"mathematica": [
				"etr[p_] := etr[p] = Module[{b}, b[n_] := b[n] = If[n == 0, 1, Sum[ Sum[ d*p[d], {d, Divisors[j]}]*b[n-j], {j, 1, n}]/n]; b]; a[0] = 0; a[n_] := etr[etr[a]][n-1]; Table[a[n], {n, 0, 25}] (* _Jean-François Alcover_, May 28 2013, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(PARI)",
				"EulerT(v)={Vec(exp(x*Ser(dirmul(v,vector(#v,n,1/n))))-1, -#v)}",
				"seq(n)={my(v=[1]); for(i=2, n, v=concat([1], EulerT(EulerT(v)))); concat([0], v)} \\\\ _Andrew Howroyd_, May 20 2018"
			],
			"xref": [
				"Cf. A007549, A030019, A035051, A035052, A035053.",
				"Column k=2 of A144042.",
				"Cf. A245566."
			],
			"keyword": "nonn,nice,eigen",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"New description from _Christian G. Bower_, Oct 15 1998"
			],
			"references": 15,
			"revision": 56,
			"time": "2018-08-05T20:44:50-04:00",
			"created": "1994-09-19T03:00:00-04:00"
		}
	]
}