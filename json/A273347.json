{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A273347",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 273347,
			"data": "1,4,16,60,218,778,2744,9600,33391,115638,399137,1374050,4720272,16187632,55434424,189607406,647872199,2211794804,7545239986,25722676402,87641357150,298456587038,1015914399813,3456670564220,11757143968393,39976448714086,135887220346719",
			"name": "The sum of the areas of the bargraphs of semiperimeter n (n\u003e=2).",
			"reference": [
				"A. Blecher, C. Brennan, and A. Knopfmacher, Combinatorial parameters in bargraphs (preprint)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A273347/b273347.txt\"\u003eTable of n, a(n) for n = 2..1000\u003c/a\u003e",
				"M. Bousquet-Mélou and A. Rechnitzer, \u003ca href=\"http://dx.doi.org/10.1016/S0196-8858(02)00553-5\"\u003eThe site-perimeter of bargraphs\u003c/a\u003e, Adv. in Appl. Math. 31 (2003), 86-112.",
				"M. Bousquet-Mélou and R. Brak, \u003ca href=\"https://hal.archives-ouvertes.fr/hal-00342024\"\u003eExactly solved models of polyominoes and polygons\u003c/a\u003e, Chapter 3 of Polygons, Polyominoes and Polycubes, Lecture Notes in Physics, Vol. 775, 43-78, Springer, Berlin, Heidelberg 2009.",
				"Emeric Deutsch, S Elizalde, \u003ca href=\"http://arxiv.org/abs/1609.00088\"\u003eStatistics on bargraphs viewed as cornerless Motzkin paths\u003c/a\u003e, arXiv preprint arXiv:1609.00088, 2016"
			],
			"formula": [
				"G.f.: g(z) = z^2 (2 - z - z^3 - z q )^2 /(1 - 4z + z^4 + q + z^2 q)^2, where q = sqrt(1 - 4z + 2z^2 + z^4) (see Section 4.3 of the Blecher et al. reference).",
				"a(n) = Sum(k*A273346(n,k), k\u003e=1).",
				"a(n) = ((69-115*n+28*n^2)*a(n-1) -(264-265*n+52*n^2)*a(n-2) +(3*(29-29*n+4*n^2))*a(n-3) -(3*(10-21*n+4*n^2))*a(n-4) +(4*n-9) * (5*n-29)*a(n-5) +(4*n-13)*(n-6)*a(n-6) +(n-7)*(4*n-9)*a(n-7))/ (n*(4*n-13)) for n\u003e=7. - _Alois P. Heinz_, Jun 04 2016"
			],
			"example": [
				"a(4) = 16  because the 5 (=A082582(4)) bargraphs of semiperimeter 4 correspond to the compositions [1,1,1], [1,2], [2,1], [2,2], [3] and, clearly, the sum of their areas is 3 + 3 + 3 + 4 + 3 = 16."
			],
			"maple": [
				"Q := sqrt(1-4*z+2*z^2+z^4): g := z^2*(2-z-z^3-z*Q)^2/(1-4*z+z^4+Q+z^2*(2+Q))^2; gser:= series(g,z = 0,40): seq(coeff(gser, z, m), m = 2 .. 35);",
				"# second Maple program:",
				"a:= proc(n) option remember;",
				"      `if`(n\u003c7, [0$2, 1, 4, 16, 60, 218, 778][n+1],",
				"      ((69-115*n+28*n^2)*a(n-1) -(264-265*n+52*n^2)*a(n-2)",
				"       +(3*(29-29*n+4*n^2))*a(n-3) -(3*(10-21*n+4*n^2))*a(n-4)",
				"       +(4*n-9)*(5*n-29)*a(n-5) +(4*n-13)*(n-6)*a(n-6)",
				"       +(n-7)*(4*n-9)*a(n-7))/(n*(4*n-13)))",
				"    end:",
				"seq(a(n), n=2..30);  # _Alois P. Heinz_, Jun 04 2016"
			],
			"mathematica": [
				"b[n_, y_, t_] := b[n, y, t] = Expand[If[n==0, 1-t, If[t\u003c0, 0, b[n-1, y+1, 1]] + If[t\u003e0 || y\u003c2, 0, b[n, y-1, -1]] + If[y\u003c1, 0, b[n-1, y, 0]*z^y]]]; T[n_] := Function[p, Table[Coefficient[p, z, i], {i, 1, Exponent[p, z]}]][b[n, 0, 0]]; a[n_] := (row = T[n]; row.Range[Length[row]]); Table[a[n], {n, 2, 30}] (* _Jean-François Alcover_, Nov 29 2016 after _Alois P. Heinz_'s Maple code for A273346 *)"
			],
			"xref": [
				"Cf. A273346, A273348."
			],
			"keyword": "nonn",
			"offset": "2,2",
			"author": "_Emeric Deutsch_, Jun 03 2016",
			"references": 4,
			"revision": 25,
			"time": "2017-08-19T23:21:36-04:00",
			"created": "2016-06-04T07:39:08-04:00"
		}
	]
}