{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A335585",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 335585,
			"data": "1,2,3,4,9,8,15,14,5,6,25,12,35,16,7,10,21,20,27,22,39,11,18,77,24,49,34,63,17,28,51,40,33,46,45,23,30,161,26,69,13,36,65,32,55,38,75,19,42,95,44,85,48,115,52,105,62,87,68,29,54,203,60,119,76,153,70,117,50,57,56,81,58,93",
			"name": "The numbers visited on a square spiral, with a(n) = n for 1 \u003c= n \u003c= 3, when stepping to an unvisited number as close as possible to the n = 1 starting position that has at least one common factor with the second last visited number but none with the last visited number. In case of a tie, choose the smallest number.",
			"comment": [
				"This sequence is the square spiral version of the Yellowstone permutation A098550. The same rules for selecting the next number apply except that, instead of choosing the smallest unvisited number for a(n), the number closest to the starting n = 1 position which satisfies the selection rules is chosen. If two or more such numbers exist then the smallest is chosen.",
				"The first term that differs from A098550 is a(23) = 18. See the examples below."
			],
			"link": [
				"Scott R. Shannon, \u003ca href=\"/A335585/b335585.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Scott R. Shannon, \u003ca href=\"/A335585/a335585.png\"\u003eImage of the first 50000 visited numbers on the square spiral\u003c/a\u003e. The colors are graduated across the spectrum from red to violet to indicate the relative visit order of the numbers. The starting 1 position is colored white."
			],
			"example": [
				"The square spiral used is:",
				".",
				"  17--16--15--14--13   .",
				"   |               |   .",
				"  18   5---4---3  12   29",
				"   |   |       |   |   |",
				"  19   6   1---2  11   28",
				"   |   |           |   |",
				"  20   7---8---9--10   27",
				"   |                   |",
				"  21--22--23--24--25--26",
				".",
				"a(7) = 15 as a(5) = 9 = 3*3 and a(8) = 8 = 2*2*2, thus a(7) must contain 3 as a factor but not 2. The closest unvisited number to the starting 1 position that satisfies these conditions is 15.",
				"a(23) = 18 as a(21) = 39 = 3*13 and a(22) = 11, thus a(23) must contain 3 or 13 as a factor but not 11. The smallest unvisited number satisfying these conditions is 13, which is sqrt(8) units from 1. However 18 is unvisited and also satisfies the conditions, and is only sqrt(5) units from 1, thus a(23) = 18. This is the first term that differs from A098550."
			],
			"xref": [
				"Cf. A098550, A335661, A330979, A340783, A336957, A064413."
			],
			"keyword": "nonn,walk",
			"offset": "1,2",
			"author": "_Scott R. Shannon_, Jan 26 2021",
			"references": 1,
			"revision": 21,
			"time": "2021-02-05T00:16:57-05:00",
			"created": "2021-02-01T13:44:20-05:00"
		}
	]
}