{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A033156",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 33156,
			"data": "1,4,8,12,17,22,27,32,38,44,50,56,62,68,74,80,87,94,101,108,115,122,129,136,143,150,157,164,171,178,185,192,200,208,216,224,232,240,248,256,264,272,280,288,296,304,312,320,328,336,344,352,360,368,376,384,392,400,408",
			"name": "a(1) = 1; for m \u003e= 2, a(n) = a(n-1) + floor(a(n-1)/(n-1)) + 2.",
			"link": [
				"Harvey P. Dale, \u003ca href=\"/A033156/b033156.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Hsien-Kuei Hwang, S. Janson, T.-H. Tsai, \u003ca href=\"http://140.109.74.92/hk/wp-content/files/2016/12/aat-hhrr-1.pdf\"\u003eExact and asymptotic solutions of the recurrence f(n) = f(floor(n/2)) + f(ceiling(n/2)) + g(n): theory and applications\u003c/a\u003e, Preprint 2016.",
				"Hsien-Kuei Hwang, S. Janson, T.-H. Tsai, \u003ca href=\"https://doi.org/10.1145/3127585\"\u003eExact and Asymptotic Solutions of a Divide-and-Conquer Recurrence Dividing at Half: Theory and Applications\u003c/a\u003e, ACM Transactions on Algorithms, 13:4 (2017), #47; DOI: 10.1145/3127585.",
				"M. A. Nyblom, \u003ca href=\"http://www.jstor.org/stable/2695446\"\u003eSome curious sequences involving floor and ceiling functions\u003c/a\u003e, Am. Math. Monthly 109 (#6, 200), 559-564, Th. 3.1.",
				"R. Stephan, \u003ca href=\"/somedcgf.html\"\u003eSome divide-and-conquer sequences ...\u003c/a\u003e",
				"R. Stephan, \u003ca href=\"/A079944/a079944.ps\"\u003eTable of generating functions\u003c/a\u003e"
			],
			"formula": [
				"a(n) = n*(floor(log_2 n) + 3) - 2^((floor (log_2 n)) + 1).",
				"a(n) = n + a(floor(n/2)) + a(ceiling(n/2)) = n + min{a(k) + a(n-k):0 \u003c k \u003c n} = n + A003314(n). - _Henry Bottomley_, Jul 03 2002",
				"A001855(n) + 2n-1. a(n) = b(n)+1 with b(0)=0, b(2n) = b(n) + b(n-1) + 2n + 2, b(2n+1) = 2b(n) + 2n + 3. - _Ralf Stephan_, Oct 24 2003",
				"a(n) = A123753(n-1) + n - 1. - _Peter Luschny_, Nov 30 2017"
			],
			"maple": [
				"A033156 := proc(n) option remember; if n=1 then 1 else A033156(n-1)+floor(A033156(n-1)/(n-1))+2; fi; end;"
			],
			"mathematica": [
				"a[n_] := n (2 + IntegerLength[n, 2]) - 2^IntegerLength[n, 2];",
				"Table[a[n], {n, 1, 59}] (* _Peter Luschny_, Dec 02 2017 *)",
				"nxt[{n_,a_}]:={n+1,a+Floor[a/n]+2}; NestList[nxt,{1,1},60][[All,2]] (* _Harvey P. Dale_, Nov 03 2020 *)"
			],
			"program": [
				"(Python)",
				"def A033156(n):",
				"    s, i, z = 2*n-1, n-1, 1",
				"    while 0 \u003c= i: s += i; i -= z; z += z",
				"    return s",
				"print([A033156(n) for n in range(1, 60)]) # _Peter Luschny_, Nov 30 2017"
			],
			"xref": [
				"Cf. A123753."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, Jun 05 2002",
			"references": 6,
			"revision": 43,
			"time": "2020-11-03T19:34:44-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}