{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A284849",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 284849,
			"data": "0,1,2,2,3,3,4,4,5,5,6,6,7,5,6,6,5,5,4,4,4,3,4,4,4,3,4,4,4,3,4,4,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3",
			"name": "Number of zeros strictly inside the unit circle of the Bernoulli polynomial B(n,x).",
			"comment": [
				"The n-th Bernoulli polynomial is defined by the exponential generating function:",
				"t*exp(x*t)/(exp(t)-1) = Sum_{n\u003e=0} bernoulli(n,x)/n!*t^n.",
				"The first few Bernoulli polynomials are:",
				"B(0,x) = 1",
				"B(1,x) = x - 1/2",
				"B(2,x) = x^2 - x + 1/6",
				"B(3,x) = x^3 - 3/2 x^2 + 1/2 x",
				"B(4,x)= x^4 - 2x^3 + x^2 - 1/30",
				"Conjecture: for n \u003e 63, a(n) = 3 for n odd and a(n) = 4 otherwise. - _Charles R Greathouse IV_, May 09 2017"
			],
			"link": [
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/BernoulliPolynomial.html\"\u003eThe World of Mathematics: Bernoulli Polynomial\u003c/a\u003e"
			],
			"formula": [
				"Conjectures from _Colin Barker_, Jan 22 2020: (Start)",
				"G.f.: x*(1 + 2*x + x^2 + x^3 + x^4 + x^5 + x^6 + x^7 + x^8 + x^9 + x^10 + x^11 - x^12 - x^13 + x^14 - x^15 - x^16 - x^17 - x^18 - x^20 + x^22 - x^24 + x^26 - x^28 + x^30 - x^32) / ((1 - x)*(1 + x)).",
				"a(n) = a(n-2) for n\u003e33.",
				"(End)"
			],
			"example": [
				"a(6) = 4 because the zeros of B(6,x) = x^6 - 3x^5 + 5/2 x^4 - 1/2 x^2 + 1/42 are:",
				"x1 = -0.2728865...-0.06497293...*i,",
				"x2 = -0.2728865...+0.06497293...*i,",
				"x3 =  0.2475407...,",
				"x4 =  0.7524592...,",
				"x5 =  1.272886...-.06497293...*i,",
				"x6 =  1.272886...+.06497293...*i",
				"with four roots x1, x2, x3 and x4 in the unit circle."
			],
			"maple": [
				"for n from 0 to 90 do:it:=0:",
				"y:=[fsolve(expand(bernoulli(n,x)),x,complex)]:for m from 1 to nops(y) do:if abs(y[m])\u003c1 then it:=it+1:else fi:od: printf(`%d, `, it):od:"
			],
			"program": [
				"(PARI) a(n)=my(v=polroots(bernpol(n))); sum(i=1,#v,abs(v[i])\u003c1) \\\\ _Charles R Greathouse IV_, May 09 2017"
			],
			"xref": [
				"Cf. A001898, A027641, A027642."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Michel Lagneau_, May 09 2017",
			"references": 1,
			"revision": 21,
			"time": "2020-01-22T08:56:10-05:00",
			"created": "2017-05-12T15:16:02-04:00"
		}
	]
}