{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A178821",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 178821,
			"data": "1,5,5,15,30,15,35,105,105,35,70,280,420,280,70,126,630,1260,1260,630,126,210,1260,3150,4200,3150,1260,210,330,2310,6930,11550,11550,6930,2310,330,495,3960,13860,27720,34650,27720,13860,3960,495,715,6435,25740,60060,90090,90090,60060,25740,6435,715",
			"name": "Triangle read by rows: T(n,k) = binomial(n+4,4) * binomial(n,k), 0 \u003c= k \u003c= n.",
			"comment": [
				"The product of the pentatope numbers (A000332, beginning with fifth term) and Pascal's triangle (A007318). Also level 5 of Pascal's prism (A178819) read by rows: (i+4; 4, i-j, j), i \u003e= 0, 0 \u003c= j \u003c= i."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A178821/b178821.txt\"\u003eRows n=0..100 of triangle, flattened\u003c/a\u003e",
				"H. J. Brothers, \u003ca href=\"https://doi.org/10.1017/S0025557200004447\"\u003ePascal's prism\u003c/a\u003e, The Mathematical Gazette, 96 (July 2012), 213-220.",
				"H. J. Brothers, \u003ca href=\"http://www.brotherstechnology.com/math/pascals-prism.html\"\u003ePascal's Prism: Supplementary Material\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = C(n+4,4) * C(n,k), 0 \u003c= k \u003c= n.",
				"For element a in A178819: a_(5, i, j) = (i+3; 4, i-j, j-1), i \u003e= 1, 1 \u003c= j \u003c= i.",
				"G.f.: 1/(1 - x - x*y)^5. - _Ilya Gutkovskiy_, Mar 20 2020"
			],
			"example": [
				"Triangle begins:",
				"   1;",
				"   5,   5;",
				"  15,  30,  15;",
				"  35, 105, 105,  35;",
				"  70, 280, 420, 280,  70;"
			],
			"maple": [
				"T:=(n,k)-\u003ebinomial(n+4,4)*binomial(n,k): seq(seq(T(n,k),k=0..n),n=0..9); # _Muniru A Asiru_, Jan 22 2019"
			],
			"mathematica": [
				"Table[Multinomial[4, i-j, j], {i, 0, 9}, {j, 0, i}]//Column"
			],
			"program": [
				"(MAGMA) /* As triangle */ [[Binomial(n+4,4)*Binomial(n,k): k in [0..n]]: n in [0.. 10]]; // _Vincenzo Librandi_, Oct 23 2017",
				"(PARI) {T(n,k) = binomial(n+4, 4)*binomial(n, k)}; \\\\ _G. C. Greubel_, Jan 22 2019",
				"(Sage) [[binomial(n+4, 4)*binomial(n, k) for k in (0..n)] for n in (0..10)] # _G. C. Greubel_, Jan 22 2019",
				"(GAP) T:=Flat(List([0..10], n-\u003e List([0..n], k-\u003e Binomial(n+4, 4)* Binomial(n, k) ))); # _G. C. Greubel_, Jan 22 2019"
			],
			"xref": [
				"Cf. A000332, A007318, A178819, A178820, A178822.",
				"Rows sum to A003472, shallow diagonals sum to A001873."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "0,2",
			"author": "_Harlan J. Brothers_, Jun 19 2010",
			"references": 3,
			"revision": 26,
			"time": "2020-03-20T15:10:42-04:00",
			"created": "2010-07-11T03:00:00-04:00"
		}
	]
}