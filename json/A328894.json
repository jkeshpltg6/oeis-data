{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A328894",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 328894,
			"data": "1069,884,995,884,885,988,885,943,549,1070,942,548,881,951,987,886,661,601,1123,1313,1034,1070,1101,1070,1930,943,655,882,1930,943,1471,992,583,884,806,704,1062,1098,1096,1129,1174,723,438,1102,854",
			"name": "a(n) is the number of steps before being trapped for a knight starting on square n on a single-digit square-spiral numbered board and where the knight moves to the smallest numbered unvisited square; the minimum distance from the origin is used if the square numbers are equal; the smallest spiral number ordering is used if the distances are equal.",
			"comment": [
				"This is the number of completed steps before being trapped for a knight starting on a square with square spiral number n for a knight with step rules given in A326918. We use the standard square spiral number of A316667 to define the start square, as opposed to its single-digit board value, as it is a unique value for each square on the board.",
				"Unlike board numbering methods which have a unique smallest value at the origin, which causes the knight to immediately move toward it when starting from any other square, the single-digit numbering method has multiple small values distributed over the board. Therefore when starting from an arbitrary square the knight may move in any direction, toward the smallest valued neighboring square one knight leap away. Only when two or more such squares exist with the same number does the origin start to act as the square of attraction. This means some knight paths will meander well away from the origin and can become trapped before ever approaching it.",
				"For starting squares n from 1 to 10^6 the longest path before being trapped is a(435525) = 2865. The smallest path to being trapped is a(42329) = 109. The path which ends on the square with the largest standard square spiral number is a(31223), which ends on square 47863. The first path which ends on the square with the smallest standard spiral number is a(138), which ends on square 4. This square is adjacent to the origin, but it is curious that the three squares with smaller spiral numbers, 1,2,3, do not act as the end square for any of the starting squares studied."
			],
			"link": [
				"Scott R. Shannon, \u003ca href=\"/A328894/b328894.txt\"\u003eTable of n, a(n) for n = 1..20000\u003c/a\u003e",
				"Eric Angelini, \u003ca href=\"https://cinquantesignes.blogspot.com/2019/05/kneils-knumberphile-knight.html\"\u003eKneil's Knumberphile Knight\u003c/a\u003e, Cinquante signes, May 04 2019.",
				"Scott R. Shannon, \u003ca href=\"/A328894/a328894.png\"\u003ePath for starting square n = 435525\u003c/a\u003e. This is trapped after 2865 steps, the longest found path. In this and other images a green square marks the starting square, an orange square the 0-numbered origin square, a red square the ending square, and blue squares mark the eight blocking squares for the end square.",
				"Scott R. Shannon, \u003ca href=\"/A328894/a328894_4.png\"\u003ePath for starting square n = 42329\u003c/a\u003e. This is trapped after 109 steps, the shortest found path. This is an example of a path starting and being trapped without approaching the origin. Note that the start square also acts as one of the eight blocking squares.",
				"Scott R. Shannon, \u003ca href=\"/A328894/a328894_2.png\"\u003ePath for starting square n = 31223\u003c/a\u003e. This is trapped on the square with standard spiral number 47863, the largest found value. The path also starts and ends without approaching the origin.",
				"Scott R. Shannon, \u003ca href=\"/A328894/a328894_3.png\"\u003ePath for starting square n = 138\u003c/a\u003e. This is trapped on the square with standard spiral number 4, the smallest value found.",
				"N. J. A. Sloane and Brady Haran, \u003ca href=\"https://www.youtube.com/watch?v=RGQe8waGJ4w\"\u003eThe Trapped Knight\u003c/a\u003e, Numberphile video (2019)."
			],
			"example": [
				"a(1) = 1069. See A326918.",
				"The squares are numbered using single digits of the spiral number ordering as:",
				"                                .",
				"                                .",
				"    2---2---2---1---2---0---2   2",
				"    |                       |   |",
				"    3   1---2---1---1---1   9   3",
				"    |   |               |   |   |",
				"    2   3   4---3---2   0   1   1",
				"    |   |   |       |   |   |   |",
				"    4   1   5   0---1   1   8   3",
				"    |   |   |           |   |   |",
				"    2   4   6---7---8---9   1   0",
				"    |   |                   |   |",
				"    5   1---5---1---6---1---7   3",
				"    |                           |",
				"    2---6---2---7---2---8---2---9",
				"If the knight has a choice of two or more squares in this spiral with the same number which also have the same distance from the origin, then the square with the minimum standard spiral number, as shown in A316667, is chosen."
			],
			"xref": [
				"Cf. A316667, A326413, A326916, A174344, A274923, A296030."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Scott R. Shannon_, Oct 29 2019",
			"references": 5,
			"revision": 24,
			"time": "2020-01-05T22:23:03-05:00",
			"created": "2019-10-31T01:01:29-04:00"
		}
	]
}