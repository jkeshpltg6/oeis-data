{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A329588",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 329588,
			"data": "2,2,4,4,4,2,4,4,2,12,2,8,4,4,12,4,4,4,4,2,4,24,4,4,4,2,8,4,4,12,2,4,8,4,24,2,4,8,4,12,12,8,4,4,4,12,24,4,8,8,2,4,8,12,4,4,2,4,8,2,12,12,24,8,4,4,12,4,8,4,4,12,4,2,4,48",
			"name": "Number of representative solutions (a, b) of the complex congruence z^2 == +1 (mod m) with z = a + b*i and nonvanishing a*b.",
			"comment": [
				"This sequence gives one half of the row lengths of the irregular triangle A329587.",
				"For the number of representative solutions of this congruence for all positive moduli m and vanishing a*b see A329586.",
				"The formula for the number of representative solutions a(n), with modulus m = m(n), given in A329587, can be found from the number of all such solutions  for m = m(n) given in A227091 after subtracting the number of solutions with a*b = 0 given in A329586. For odd m = m(n) this is S(m) = 2^(r1(m) +r3(m))*(2^r1(m) - 1) - delta(r3(m), 0)*2^(r1(m)), with r1(m) and r3(m) the number of distinct primes 1 (mod 4) and 3 (mod 4) in the prime number factorization of m respectively, and delta is the Kronecker symbol. For even m this is S(m) = 2^(r1(m) + r3(m))*(2^(1+r1(m)) - 1) - delta(r3(m), 0)*2^(r1(m)) if m/2 is odd (e2 = 1), and otherwise S(m) = 2^(r2(e2(m)) + r1(m) + r3(m))*(2^(1 + r1(m) + r3(m)) - 1), with r2(e2(m)) = 1 or 2 if e2(m) = 2 or \u003e= 3, if m/2^(e2(m)) is odd."
			],
			"formula": [
				"a(n) is the number of solutions (a, b) of the congruence z^2 == +1 (mod m(n)), with z = a + b*i and a*b not equal to 0, for n \u003e= 1. For m = m(n) see A329587: it is the sequence of even numbers \u003e= 4 combined with the odd numbers from A329589, sorted increasingly."
			],
			"example": [
				"n = 1, m = 4: a(1) = S(4) = 2^1 *(2^1 - 1) = 2.",
				"n = 2, m = 6 = 2*3: a(2) = S(6) = 2^(0+1)*(2^1 - 1) - 0 = 2.",
				"n = 3, m = 8 = 2^3: a(3) = S(8) = 2^2*(2^1 - 1) = 4.",
				"n = 4, m = 10 = 2*5: a(4) = S(10) = 2^(1+0)*(2^(1+1) -1) - 2^1 = 2*3 - 2 = 4.",
				"n = 10, m = 20 = 2^2*5: a(10) = S(20) = 2^(1+1+0)*(2^(1+1+0) - 1) = 4*3 = 12.",
				"n = 15, m = 30 = 2*3*5: a(15) = S(30) = 2^(1+1)*(2^(1+1) - 1) - 0 = 4*3 = 12."
			],
			"xref": [
				"Cf. A329585, A329586, A329587, A329589."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Wolfdieter Lang_, Dec 14 2019",
			"references": 2,
			"revision": 15,
			"time": "2020-09-29T17:03:27-04:00",
			"created": "2019-12-31T06:41:53-05:00"
		}
	]
}