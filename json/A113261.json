{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A113261",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 113261,
			"data": "1,1,-5,1,11,-24,-5,50,-53,1,120,-120,11,170,-250,-24,203,-288,-5,362,-264,50,600,-528,-53,601,-850,1,550,-840,120,962,-821,-120,1440,-1200,11,1370,-1810,170,1272,-1680,-250,1850,-1320,-24,2640,-2208,203,2451,-3005,-288,1870,-2808",
			"name": "Expansion of (9*phi(q)*phi(q^3)^5 - phi(q)^5*phi(q^3))/8 in powers of q where phi(q) is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"reference": [
				"B. C. Berndt, Ramanujan's Notebooks Part III, Springer-Verlag, see p. 226 Entry 4(ii)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A113261/b113261.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Euler transform of period 12 sequence [1, -6, 6, -5, 1, -12, 1, -5, 6, -6, 1, -6, ...].",
				"Expansion of eta(q^2)^7 * eta(q^6)^11 / (eta(q) * eta(q^3)^5 * eta(q^4) * eta(q^12)^5) in powers of q.",
				"a(n) is multiplicative and a(3^e) = 1, a(2^e) = ((-4)^(e+1)-1)/(-4-1)-2, a(p^e) = ((-p^2)^(e+1)-1)/(-p-1) if p == 2 (mod 3), a(p^e) = ((p^2)(e+1)-1)/(p-1) if p == 1 (mod 3).",
				"G.f.: 1 + Sum_{k\u003e0} k^2 x^k/(1-(-x)^k) Kronecker(-3, k)."
			],
			"example": [
				"G.f. = 1 + q - 5*q^2 + q^3 + 11*q^4 - 24*q^5 - 5*q^6 + 50*q^7 + ... - _Michael Somos_, Sep 07 2018"
			],
			"mathematica": [
				"a[n_]:= SeriesCoefficient[(9*EllipticTheta[3, 0, q]*EllipticTheta[3, 0, q^3]^5 - EllipticTheta[3, 0, q]^5*EllipticTheta[3, 0, q^3])/8, {q, 0, n}]; Table[a[n], {n,0,50}] (* _G. C. Greubel_, Feb 09 2018 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, n==0, sumdiv(n, d, d^2 * kronecker(-3, d) * (-1)^(n-d)))};",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A)^7 * eta(x^6 + A)^11 / (eta(x + A) * eta(x^3 + A)^5 * eta(x^4 + A) * eta(x^12 + A)^5), n))};",
				"(PARI) {a(n) = my(A, p, e, t); if( n\u003c1, n==0, A = factor(n); prod(k=1, matsize(A)[1], [p, e] = A[k, ]; if( p==3, 1, t=p^2*(-1)^(p%3==2); (t^(e+1) - 1) / (t-1) - 2*(p==2))))};"
			],
			"keyword": "sign,mult",
			"offset": "0,3",
			"author": "_Michael Somos_, Oct 21 2005",
			"references": 3,
			"revision": 21,
			"time": "2021-03-12T22:24:43-05:00",
			"created": "2006-01-24T03:00:00-05:00"
		}
	]
}