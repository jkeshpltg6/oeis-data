{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A114212",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 114212,
			"data": "1,2,3,4,4,4,6,8,6,4,6,8,8,8,12,16,10,4,6,8,8,8,12,16,12,8,12,16,16,16,24,32,18,4,6,8,8,8,12,16,12,8,12,16,16,16,24,32,20,8,12,16,16,16,24,32,24,16,24,32,32,32,48,64,34,4,6,8,8,8,12,16,12,8,12,16,16,16,24,32,20,8",
			"name": "Generalized Gould sequence.",
			"comment": [
				"Row sums of A114213."
			],
			"link": [
				"Jeffrey Shallit and Lukas Spiegelhofer, \u003ca href=\"https://arxiv.org/abs/1710.06203\"\u003eContinuants, run lengths, and Barry's modified Pascal triangle\u003c/a\u003e, arXiv:1710.06203 [math.CO], 2017."
			],
			"formula": [
				"a(n) = Sum_{k=0..n} (Sum_{j=0..n-k} C(k, j)*C(n-k, j)*((1 + (-1)^k)/2) mod 2).",
				"a(n) = A001316(n) + A001316((n-2)/2)*(1 + (-1)^n)/2."
			],
			"example": [
				"From _Omar E. Pol_, Jun 09 2009: (Start)",
				"Triangle begins:",
				"  1;",
				"  2,3;",
				"  4,4,4,6;",
				"  8,6,4,6,8,8,8,12;",
				"  16,10,4,6,8,8,8,12,16,12,8,12,16,16,16,24;",
				"  32,18,4,6,8,8,8,12,16,12,8,12,16,16,16,24,32,20,8,12,16,16,16,24,32,24,...",
				"Also, we can write the initial term followed by a triangle:",
				"  1;",
				"  2;",
				"  3,4;",
				"  4,4,6,8;",
				"  6,4,6,8,8,8,12,16;",
				"  10,4,6,8,8,8,12,16,12,8,12,16,16,16,24,32;",
				"  18,4,6,8,8,8,12,16,12,8,12,16,16,16,24,32,20,8,12,16,16,16,24,32,24,16,...",
				"Also, we can write first two terms followed by a triangle:",
				"  1;",
				"  2;",
				"  3;",
				"  4,4;",
				"  4,6,8,6;",
				"  4,6,8,8,8,12,16,10;",
				"  4,6,8,8,8,12,16,12,8,12,16,16,16,24,32,18;",
				"  4,6,8,8,8,12,16,12,8,12,16,16,16,24,32,20,8,12,16,16,16,24,32,24,16,24,32,...",
				"(End)"
			],
			"program": [
				"(PARI) T(n,k) = sum(j=0, n-k, binomial(k, j)*binomial(n-k, j)*(1+(-1)^j)/2) % 2; \\\\ A114213",
				"a(n) = sum(k=0, n-1, T(n,k)); \\\\ _Michel Marcus_, Jun 06 2021"
			],
			"xref": [
				"Cf. A000079. [_Omar E. Pol_, Jun 09 2009]"
			],
			"keyword": "easy,nonn",
			"offset": "0,2",
			"author": "_Paul Barry_, Nov 17 2005",
			"references": 1,
			"revision": 15,
			"time": "2021-06-06T09:05:14-04:00",
			"created": "2006-01-24T03:00:00-05:00"
		}
	]
}