{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A091602",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 91602,
			"data": "1,1,1,2,0,1,2,2,0,1,3,2,1,0,1,4,3,2,1,0,1,5,4,3,1,1,0,1,6,7,3,3,1,1,0,1,8,8,6,3,2,1,1,0,1,10,12,7,5,3,2,1,1,0,1,12,15,11,6,5,2,2,1,1,0,1,15,21,14,10,5,5,2,2,1,1,0,1,18,26,20,12,9,5,4,2,2,1,1,0,1,22,35,25,18,11,8,5,4,2,2,1,1,0,1",
			"name": "Triangle: T(n,k) = number of partitions of n such that some part is repeated k times and no part is repeated more than k times.",
			"comment": [
				"From _Gary W. Adamson_, Mar 13 2010: (Start)",
				"The triangle by rows = finite differences starting from the top, of an array in which row 1 = p(x)/p(x^2), row 2 = p(x)/p(x^3), ... row k = p(x)/p(x^k); such that p(x) = polcoeff A000041: (1 + x + 2x^2 + 3x^3 + 5x^4 + 7x^5 + ...)",
				"Note that p(x)/p(x^2) = polcoeff A000009: (1 + x + x^2 + 2x^3 + 2x^4 + ...).",
				"Refer to the example. (End)"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A091602/b091602.txt\"\u003eRows n = 1..141, flattened\u003c/a\u003e"
			],
			"formula": [
				"G.f.: G = G(t,x) = sum(k\u003e=1, t^k*(prod(j\u003e=1, (1-x^((k+1)*j))/(1-x^j) ) -prod(j\u003e=1, (1-x^(k*j))/(1-x^j) ) ) ). - _Emeric Deutsch_, Mar 30 2006",
				"Sum_{k=1..n} k * T(n,k) = A264397(n). - _Alois P. Heinz_, Nov 20 2015"
			],
			"example": [
				"Triangle starts:",
				"01:  1,",
				"02:  1, 1,",
				"03:  2, 0, 1,",
				"04:  2, 2, 0, 1,",
				"05:  3, 2, 1, 0, 1,",
				"06:  4, 3, 2, 1, 0, 1,",
				"07:  5, 4, 3, 1, 1, 0, 1,",
				"08:  6, 7, 3, 3, 1, 1, 0, 1,",
				"09:  8, 8, 6, 3, 2, 1, 1, 0, 1,",
				"10: 10, 12, 7, 5, 3, 2, 1, 1, 0, 1,",
				"11: 12, 15, 11, 6, 5, 2, 2, 1, 1, 0, 1,",
				"12: 15, 21, 14, 10, 5, 5, 2, 2, 1, 1, 0, 1,",
				"13: 18, 26, 20, 12, 9, 5, 4, 2, 2, 1, 1, 0, 1,",
				"14: 22, 35, 25, 18, 11, 8, 5, 4, 2, 2, 1, 1, 0, 1,",
				"...",
				"In the partition 5+2+2+2+1+1, 2 is repeated 3 times, no part is repeated more than 3 times.",
				"From _Gary W. Adamson_, Mar 13 2010: (Start)",
				"First few rows of the array =",
				"...",
				"1,..1,..1,..2,..2,..3,...4,...5,...6,...8,...10,...; = p(x)/p(x^2) = A000009",
				"1,..1,..2,..2,..4,..5,...7,...9,..13,..16,...22,...; = p(x)/p(x^3)",
				"1,..1,..2,..3,..4,..6,...9,..12,..16,..22,...29,...; = p(x)/p(x^4)",
				"1,..1,..2,..3,..5,..6,..10,..13,..19,..25,...34,...; = p(x)/p(x^5)",
				"1,..1,..2,..3,..5,..7,..10,..14,..20,..27,...37,...; = p(x)/p(x^6)",
				"...",
				"Finally, taking finite differences from the top and deleting the first \"1\",",
				"we obtain triangle A091602 with row sums = A000041 starting with offset 1:",
				"1;",
				"1, 1;",
				"2, 0, 1;",
				"2, 2, 0, 1;",
				"3, 2, 1, 0, 1;",
				"4, 3, 2, 1, 0, 1;",
				"...",
				"(End)"
			],
			"maple": [
				"g:=sum(t^k*(product((1-x^((k+1)*j))/(1-x^j),j=1..50)-product((1-x^(k*j))/(1-x^j),j=1..50)),k=1..50): gser:=simplify(series(g,x=0,20)): for n from 1 to 13 do P[n]:=coeff(gser,x^n) od: for n from 1 to 13 do seq(coeff(P[n],t^j),j=1..n) od;",
				"# yields sequence in triangular form - _Emeric Deutsch_, Mar 30 2006",
				"b:= proc(n, i, k) option remember; `if`(n=0, 1,",
				"      `if`(i\u003en, 0, add(b(n-i*j, i+1, min(k,",
				"       iquo(n-i*j, i+1))), j=0..min(n/i, k))))",
				"    end:",
				"T:= (n, k)-\u003e b(n, 1, k) -`if`(k=0, 0, b(n, 1, k-1)):",
				"seq(seq(T(n, k), k=1..n), n=1..20);",
				"# _Alois P. Heinz_, Nov 27 2013"
			],
			"mathematica": [
				"b[n_, i_, k_] := b[n, i, k] = If[n == 0, 1, If[i\u003en, 0, Sum[b[n-i*j, i+1, Min[k, Quotient[n-i*j, i+1]]], {j, 0, Min[n/i, k]}]]]; t[n_, k_] := b[n, 1, k] - If[k == 0, 0, b[n, 1, k-1]]; Table[t[n, k], {n, 1, 20}, {k, 1, n}] // Flatten (* _Jean-François Alcover_, Jan 17 2014, after _Alois P. Heinz_'s second Maple program *)"
			],
			"xref": [
				"Row sums: A000041. Inverse: A091603. Square: A091604.",
				"Columns 1-6: A000009, A091605-A091609. Convergent of columns: A002865.",
				"Cf. A000009. - _Gary W. Adamson_, Mar 13 2010",
				"T(2n,n) gives: A232697.",
				"Cf. A213177, A264397."
			],
			"keyword": "nonn,tabl",
			"offset": "1,4",
			"author": "_Christian G. Bower_, Jan 23 2004",
			"references": 15,
			"revision": 25,
			"time": "2018-10-11T18:32:23-04:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}