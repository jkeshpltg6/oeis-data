{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A330797",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 330797,
			"data": "1,1,-1,3,-15,105,-945,10395,-135135,2027025,-34459425,654729075,-13749310575,316234143225,-7905853580625,213458046676875,-6190283353629375,191898783962510625,-6332659870762850625,221643095476699771875,-8200794532637891559375,319830986772877770815625",
			"name": "Evaluation of the Stirling cycle polynomials at -1/2 and normalized with (-2)^n.",
			"formula": [
				"a(n) = (-2)^n*Sum_{k=0..n} |Stirling1(n,k)|*(-1/2)^k.",
				"a(n) = (-2)^(n-1)*RisingFactorial(1/2, n-1).",
				"a(n) = ((-2)^(n-1)*Gamma(n - 1/2))/sqrt(Pi).",
				"a(n) = n!*[x^n] (1+2*x)^(1/2).",
				"D-finite with recurrence a(n) = (3 - 2*n)*a(n-1).",
				"a(n) = (-1)^(n-1)*(2*n-3)!! = (-1)^(n-1)*A001147(n-1).",
				"a(2*n) = -2^(2*n-1)*RisingFactorial(1/2, 2*n-1) = -A103639(n-1).",
				"a(2*n+1) = 4^n*RisingFactorial(1/2, 2*n) = A101485(n).",
				"a(n) ~ -((-2*n)^n/exp(n))/(sqrt(2)*n)."
			],
			"maple": [
				"a := n -\u003e ((-2)^(n-1)*GAMMA(n-1/2))/sqrt(Pi): seq(a(n), n=1..9);",
				"# Alternative:",
				"arec := proc(n) option remember: if n = 0 then 1 else",
				"(3 - 2*n)*arec(n-1) fi end: seq(arec(n), n=0..20);",
				"# Or:",
				"gf := (1+2*x)^(1/2); ser := series(gf, x, 24);",
				"seq(n!*coeff(ser, x, n), n=0..20);"
			],
			"mathematica": [
				"a[n_] := (-2)^n*Sum[Abs[StirlingS1[n, k]]*(-1/2)^k, {k, 0, n}];",
				"Table[a[n], {n, 0, 21}] (* _Jean-François Alcover_, Nov 19 2021 *)"
			],
			"program": [
				"(SageMath)",
				"def A330797(n): return (-2)^(n-1)*rising_factorial(1/2, n-1)",
				"[A330797(n) for n in (0..20)]"
			],
			"xref": [
				"The equivalent for Stirling2 is A009235.",
				"Cf. A130534, A001147, A103639, A101485.",
				"Cf. A330803, A330802, A330800, A330799."
			],
			"keyword": "sign",
			"offset": "0,4",
			"author": "_Peter Luschny_, Jan 06 2020",
			"references": 2,
			"revision": 11,
			"time": "2021-11-19T05:25:49-05:00",
			"created": "2020-01-06T08:14:47-05:00"
		}
	]
}