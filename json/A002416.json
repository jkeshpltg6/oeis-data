{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002416",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2416,
			"data": "1,2,16,512,65536,33554432,68719476736,562949953421312,18446744073709551616,2417851639229258349412352,1267650600228229401496703205376,2658455991569831745807614120560689152,22300745198530623141535718272648361505980416,748288838313422294120286634350736906063837462003712",
			"name": "a(n) = 2^(n^2).",
			"comment": [
				"For n \u003e= 1 a(n) is the number of n X n (0, 1) matrices.",
				"Also number of directed graphs on n labeled nodes allowing self-loops (cf. A053763).",
				"1/2^(n^2) is the Hankel transform of C(n, n/2)*(1 + (-1)^n)/(2*2^n), or C(2n, n)/4^n with interpolated zeros. - _Paul Barry_, Sep 27 2007",
				"Hankel transform of A064062. - _Philippe Deléham_, Nov 19 2007",
				"a(n) is also the order of the semigroup (monoid) of all binary relations on an n-set. - _Abdullahi Umar_, Sep 14 2008",
				"With offset = 1, a(n) is the number of n X n (0, 1) matrices with an even number of 1's in every row and in every column. - _Geoffrey Critzer_, May 23 2013",
				"a(n) is the number of functions from an n-set to its power set (by definition of function including the empty function only when n = 0). - _Rick L. Shepherd_, Dec 27 2014"
			],
			"reference": [
				"John M. Howie, Fundamentals of semigroup theory. Oxford: Clarendon Press, (1995). - _Abdullahi Umar_, Sep 14 2008"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A002416/b002416.txt\"\u003eTable of n, a(n) for n = 0..33\u003c/a\u003e",
				"Peter J. Cameron, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL3/groups.html\"\u003eSequences realized by oligomorphic permutation groups\u003c/a\u003e, J. Integ. Seqs. Vol. 3 (2000), #00.1.5.",
				"Theresia Eisenkölbl, \u003ca href=\"https://arxiv.org/abs/math/0106038\"\u003e2-Enumerations of halved alternating sign matrices\u003c/a\u003e, arXiv:math/0106038 [math.CO], 2001.",
				"Theresia Eisenkölbl, \u003ca href=\"https://www.mat.univie.ac.at/~slc/wpapers/s46eisenko.html\"\u003e2-Enumerations of halved alternating sign matrices\u003c/a\u003e, Séminaire Lotharingien Combin. 46, (2001), Article B46c, 11 pp.",
				"Daniele A. Gewurz and Francesca Merola, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL6/Gewurz/gewurz5.html\"\u003eSequences realized as Parker vectors of oligomorphic permutation groups\u003c/a\u003e, J. Integer Seqs., Vol. 6, 2003.",
				"F. Harary and R. W. Robinson, \u003ca href=\"http://dx.doi.org/10.4153/CJM-1979-007-3\"\u003eLabeled bipartite blocks\u003c/a\u003e, Canad. J. Math., 31 (1979), 60-68.",
				"S. R. Kannan, Rajesh Kumar Mohapatra, \u003ca href=\"https://arxiv.org/abs/1909.13678\"\u003eCounting the Number of Non-Equivalent Classes of Fuzzy Matrices Using Combinatorial Techniques\u003c/a\u003e, arXiv:1909.13678 [math.GM], 2019.",
				"Kent E. Morrison, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL9/Morrison/morrison37.html\"\u003eInteger Sequences and Matrices Over Finite Fields\u003c/a\u003e, Journal of Integer Sequences, Vol. 9 (2006), Article 06.2.1.",
				"Götz Pfeiffer, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL7/Pfeiffer/pfeiffer6.html\"\u003eCounting Transitive Relations\u003c/a\u003e, Journal of Integer Sequences, Vol. 7 (2004), Article 04.3.2.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/01-Matrix.html\"\u003e01-Matrix\u003c/a\u003e.",
				"\u003ca href=\"/index/Di#divseq\"\u003eIndex to divisibility sequences\u003c/a\u003e"
			],
			"formula": [
				"G.f. satisfies: A(x) = 1 + 2*x*A(4x). - _Paul D. Hanna_, Dec 04 2009",
				"a(n) = 2^n * Sum_{i = 0...C(n, 2)} C(C(n, 2), i)*3^i.  The summation conditions on i, 0 \u003c= i \u003c= C(n, 2), the number of 1's above the main diagonal in the matrix representations of the relations on {1, 2, ..., n}. - _Geoffrey Critzer_, Feb 18 2011",
				"G.f.: 1 / (1 - 2^1*x / (1 - 2^1*(2^2-1)*x / (1 - 2^5 * x / (1 - 2^3*(2^4-1)*x / (1 - 2^9*x / (1 - 2^5*(2^6-1)*x / ...)))))). - _Michael Somos_, May 12 2012",
				"a(n) = [x^n] 1/(1 - 2^n*x). - _Ilya Gutkovskiy_, Oct 10 2017",
				"Sum_{n\u003e=0} 1/a(n) = A319015. - _Amiram Eldar_, Oct 14 2020"
			],
			"example": [
				"G.f. = 1 + 2*x + 16*x^2 + 512*x^3 + 65536*x^4 + 33554432*x^5 + ..."
			],
			"mathematica": [
				"Table[2^(n^2), {n,0,15}] (* _Vladimir Joseph Stephan Orlovsky_, Dec 13 2008 *)"
			],
			"program": [
				"(PARI) a(n)=polresultant((x-1)^n,(x+1)^n,x) \\\\ _Ralf Stephan_",
				"(PARI) a(n)=2^n^2 \\\\ _Charles R Greathouse IV_, Jun 23 2021",
				"(MAGMA) [2^(n^2): n in [0..15]]; // _Vincenzo Librandi_, May 13 2011",
				"(Sage) [2^(n^2) for n in (0..15)] # _G. C. Greubel_, Jul 03 2019",
				"(GAP) List([0..15], n-\u003e 2^(n^2) ) # _G. C. Greubel_, Jul 03 2019"
			],
			"xref": [
				"Bisection of A060656.",
				"Cf. A053763, A064062, A064231, A319015."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 96,
			"revision": 97,
			"time": "2021-06-23T16:42:26-04:00",
			"created": "1998-06-14T03:00:00-04:00"
		}
	]
}