{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A014969",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 14969,
			"data": "1,8,32,96,256,624,1408,3008,6144,12072,22976,42528,76800,135728,235264,400704,671744,1109904,1809568,2914272,4640256,7310592,11404416,17626944,27009024,41047992,61905088,92681664",
			"name": "Expansion of (theta_3(q) / theta_4(q))^2 in powers of q.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"reference": [
				"A. Cayley, An Elementary Treatise on Elliptic Functions, 2nd ed, 1895, p. 380, Section 488.",
				"J. H. Conway and N. J. A. Sloane, \"Sphere Packings, Lattices and Groups\", Springer-Verlag, p. 102.",
				"N. J. Fine, Basic Hypergeometric Series and Applications, Amer. Math. Soc., 1988; Eq. (34.3).",
				"R. Fricke, Die elliptischen Funktionen und ihre Anwendungen, Teubner, 1922, Vol. 2, see p. 375. Eqs. (17),(18),(19)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A014969/b014969.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Vaclav Kotesovec, \u003ca href=\"http://arxiv.org/abs/1509.08708\"\u003eA method of finding the asymptotics of q-series based on the convolution of generating functions\u003c/a\u003e, arXiv:1509.08708 [math.CO], Sep 30 2015, p. 11.",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of (phi(q) / phi(-q))^2 = (phi(q) / phi(-q^2))^4 = (phi(-q^2) / phi(-q))^4 = (psi(q) / psi(-q))^4 = (chi(q)^2 / chi(-q^2))^4 = (chi(q) / chi(-q))^4 = (chi(-q^2) / chi(-q)^2)^4 = (f(q) / f(-q))^4 in powers of q where phi(), psi(), chi() are Ramanujan theta functions. - _Michael Somos_, Aug 01 2011",
				"Expansion of Fricke t(omega) = tau(omega) / 2 + 1 in powers of q = exp(2 Pi i omega).",
				"Expansion of elliptic 1 / sqrt(1 - lambda(z)) = 1 / k' in powers of nome q = exp(Pi*i*z).",
				"Euler transform of period 4 sequence [ 8, -4, 8, 0, ...]. - _Michael Somos_, Jul 07 2005",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2)) where f(u, v) = (1 + u)^2 - 4*u*v^2. - _Michael Somos_, Nov 14 2006",
				"G.f.: (theta_3(x) / theta_4(x))^2 = (Sum_{k} x^k^2) / (Sum_{k} (-x)^k^2)^2 = (Product_{k\u003e0} (1 - x^(4*k - 2)) / ((1 - x^(4*k - 1)) * (1 - x^(4*k - 3)))^2)^4.",
				"A139820(n) = (-1)^n * a(n). 8 * A107035(n) = a(n) unless n=0. 2 * A131126(n) = a(n) unless n=0. Convolution inverse of A139820.",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (8 t)) = 1/4 g(t) where q = exp(2 Pi i t) and g() is the g.f. for A029841. - _Michael Somos_, Jun 04 2015",
				"a(n) ~ exp(Pi*sqrt(2*n)) / (8 * 2^(1/4) * n^(3/4)). - _Vaclav Kotesovec_, Aug 28 2015",
				"G.f.: exp(8*Sum_{k\u003e=1} sigma(2*k - 1)*x^(2*k-1)/(2*k - 1)). - _Ilya Gutkovskiy_, Apr 19 2019",
				"Empirical: Sum_{n\u003e=0} a(n)/exp(2*Pi*n) = (1/4)*sqrt(8 + 6*sqrt(2)). - _Simon Plouffe_, Mar 02 2021"
			],
			"example": [
				"G.f. = 1 + 8*q + 32*q^2 + 96*q^3 + 256*q^4 + 624*q^5 + 1408*q^6 + 3008*q^7 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ 1 / Sqrt[1 - InverseEllipticNomeQ  @ q], {q, 0, n}]; (* _Michael Somos_, Aug 01 2011 *)",
				"a[ n_] := SeriesCoefficient[ (EllipticTheta[ 3, 0, q] / EllipticTheta[ 4, 0, q])^2, {q, 0, n}]; (* _Michael Somos_, Aug 01 2011 *)",
				"nmax=60; CoefficientList[Series[Product[((1+x^(2*k+1))/(1-x^(2*k+1)))^4,{k,0,nmax}],{x,0,nmax}],x] (* _Vaclav Kotesovec_, Aug 28 2015 *)",
				"s = (QPochhammer[q^2]^3/(QPochhammer[q]^2*QPochhammer[q^4]))^4+O[q]^30; CoefficientList[s, q] (* _Jean-François Alcover_, Nov 09 2015, adapted from PARI *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( (eta(x^2 + A)^3 / (eta(x + A)^2 * eta(x^4 + A)))^4, n))}; /* _Michael Somos_, Jul 07 2005 */"
			],
			"xref": [
				"Cf. A029841, A107035, A131126, A139820."
			],
			"keyword": "nonn,nice",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 17,
			"revision": 62,
			"time": "2021-03-12T23:54:12-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}