{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A325757",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 325757,
			"data": "1,2,1,1,2,3,1,1,1,2,2,4,1,1,1,3,2,2,2,1,1,1,2,3,5,1,1,1,1,1,2,2,2,6,1,1,1,2,4,1,1,2,2,3,1,1,1,1,4,7,1,1,1,1,2,2,2,2,8,1,1,1,1,1,2,2,3,1,1,2,2,4,1,1,1,2,5,9,1,1,1,1,1,1,2,2,3",
			"name": "Irregular triangle read by rows giving the frequency span of n.",
			"comment": [
				"We define the frequency span of an integer partition to be the partition itself if it has no or only one block, and otherwise it is the multiset union of the partition and the frequency span of its multiplicities. For example, the frequency span of (3,2,2,1) is {1,2,2,3} U {1,1,2} U {1,2} U {1,1} U {2} = {1,1,1,1,1,1,2,2,2,2,2,3}. The frequency span of a positive integer is the frequency span of its prime indices (row n of A296150)."
			],
			"example": [
				"Triangle begins:",
				"   1:",
				"   2: 1",
				"   3: 2",
				"   4: 1 1 2",
				"   5: 3",
				"   6: 1 1 1 2 2",
				"   7: 4",
				"   8: 1 1 1 3",
				"   9: 2 2 2",
				"  10: 1 1 1 2 3",
				"  11: 5",
				"  12: 1 1 1 1 1 2 2 2",
				"  13: 6",
				"  14: 1 1 1 2 4",
				"  15: 1 1 2 2 3",
				"  16: 1 1 1 1 4",
				"  17: 7",
				"  18: 1 1 1 1 2 2 2 2",
				"  19: 8",
				"  20: 1 1 1 1 1 2 2 3",
				"  21: 1 1 2 2 4",
				"  22: 1 1 1 2 5",
				"  23: 9",
				"  24: 1 1 1 1 1 1 2 2 3",
				"  25: 2 3 3",
				"  26: 1 1 1 2 6",
				"  27: 2 2 2 3",
				"  28: 1 1 1 1 1 2 2 4"
			],
			"mathematica": [
				"primeMS[n_]:=If[n==1,{},Flatten[Cases[FactorInteger[n],{p_,k_}:\u003eTable[PrimePi[p],{k}]]]];",
				"freqspan[ptn_]:=If[Length[ptn]\u003c=1,ptn,Sort[Join[ptn,freqspan[Sort[Length/@Split[ptn]]]]]];",
				"Table[freqspan[primeMS[n]],{n,15}]"
			],
			"xref": [
				"Row lengths are A325249.",
				"Run-lengths are A325758.",
				"Number of distinct terms in row n is A325759(n).",
				"Cf. A001221, A001222, A056239, A071625, A112798, A181819, A182857, A290822, A323014, A324843, A325277, A325755, A325760."
			],
			"keyword": "nonn,tabf",
			"offset": "1,2",
			"author": "_Gus Wiseman_, May 19 2019",
			"references": 3,
			"revision": 9,
			"time": "2019-05-19T06:18:15-04:00",
			"created": "2019-05-19T06:18:15-04:00"
		}
	]
}