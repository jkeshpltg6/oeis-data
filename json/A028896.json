{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A028896",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 28896,
			"data": "0,6,18,36,60,90,126,168,216,270,330,396,468,546,630,720,816,918,1026,1140,1260,1386,1518,1656,1800,1950,2106,2268,2436,2610,2790,2976,3168,3366,3570,3780,3996,4218,4446,4680,4920,5166,5418,5676",
			"name": "6 times triangular numbers: a(n) = 3*n*(n+1).",
			"comment": [
				"From _Floor van Lamoen_, Jul 21 2001: (Start)",
				"Write 1,2,3,4,... in a hexagonal spiral around 0; then a(n) is the sequence found by reading the line from 0 in the direction 0, 6, ...",
				"The spiral begins:",
				"                 85--84--83--82--81--80",
				"                 /                     \\",
				"               86  56--55--54--53--52  79",
				"               /   /                 \\   \\",
				"             87  57  33--32--31--30  51  78",
				"             /   /   /             \\   \\   \\",
				"           88  58  34  16--15--14  29  50  77",
				"           /   /   /   /         \\   \\   \\   \\",
				"         89  59  35  17   5---4  13  28  49  76",
				"         /   /   /   /   /     \\   \\   \\   \\   \\",
				"    \u003c==90==60==36==18===6===0   3  12  27  48  75",
				"           /   /   /   /   /   /   /   /   /   /",
				"         61  37  19   7   1---2  11  26  47  74",
				"           \\   \\   \\   \\         /   /   /   /",
				"           62  38  20   8---9--10  25  46  73",
				"             \\   \\   \\             /   /   /",
				"             63  39  21--22--23--24  45  72",
				"               \\   \\                 /   /",
				"               64  40--41--42--43--44  71",
				"                 \\                     /",
				"                 65--66--67--68--69--70",
				"(End)",
				"If Y is a 4-subset of an n-set X then, for n \u003e= 5, a(n-5) is the number of (n-4)-subsets of X having exactly two elements in common with Y. - _Milan Janjic_, Dec 28 2007",
				"a(n) is the maximal number of points of intersection of n+1 distinct triangles drawn in the plane. For example, two triangles can intersect in at most a(1) = 6 points (as illustrated in the Star of David configuration). - Terry Stickels (Terrystickels(AT)aol.com), Jul 12 2008",
				"Also sequence found by reading the line from 0, in the direction 0, 6, ... and the same line from 0, in the direction 0, 18, ..., in the square spiral whose vertices are the generalized octagonal numbers A001082. Axis perpendicular to A195143 in the same spiral. - _Omar E. Pol_, Sep 18 2011",
				"Partial sums of A008588. - _R. J. Mathar_, Aug 28 2014",
				"Also the number of 5-cycles in the (n+5)-triangular honeycomb acute knight graph. - _Eric W. Weisstein_, Jul 27 2017"
			],
			"link": [
				"Ivan Panchenko, \u003ca href=\"/A028896/b028896.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Milan Janjic, \u003ca href=\"http://www.pmfbl.org/janjic/\"\u003eEnumerative Formulas for Some Functions on Finite Sets\u003c/a\u003e",
				"Enrique Navarrete, Daniel Orellana, \u003ca href=\"https://arxiv.org/abs/1907.10023\"\u003eFinding Prime Numbers as Fixed Points of Sequences\u003c/a\u003e, arXiv:1907.10023 [math.NT], 2019.",
				"Luis Manuel Rivera, \u003ca href=\"http://arxiv.org/abs/1406.3081\"\u003eInteger sequences and k-commuting permutations\u003c/a\u003e, arXiv preprint arXiv:1406.3081 [math.CO], 2014.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/GraphCycle.html\"\u003eGraph Cycle\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-3,1)."
			],
			"formula": [
				"O.g.f.: 6*x/(1 - x)^3.",
				"E.g.f.: 3*x*(x + 2)*exp(x). - _G. C. Greubel_, Aug 19 2017",
				"a(n) = 6*A000217(n).",
				"a(n) = polygorial(3, n+1). - Daniel Dockery (peritus(AT)gmail.com), Jun 16 2003",
				"From _Zerinvary Lajos_, Mar 06 2007: (Start)",
				"a(n) = A049598(n)/2.",
				"a(n) = A124080(n) - A046092(n).",
				"a(n) = A033996(n) - A002378(n). (End)",
				"a(n) = A002378(n)*3 = A045943(n)*2. - _Omar E. Pol_, Dec 12 2008",
				"a(n) = a(n-1) + 6*n for n\u003e0, a(0)=0. - _Vincenzo Librandi_, Aug 05 2010",
				"a(n) = A003215(n) - 1. - _Omar E. Pol_, Oct 03 2011",
				"From _Philippe Deléham_, Mar 26 2013: (Start)",
				"a(n) = 3*a(n-1) - 3*a(n-2) + a(n-3) for n\u003e2, a(0)=0, a(1)=6, a(2)=18.",
				"a(n) = A174709(6*n + 5). (End)",
				"a(n) = A049450(n) + 4*n. - _Lear Young_, Apr 24 2014",
				"a(n) = Sum_{i = n..2*n} 2*i. - _Bruno Berselli_, Feb 14 2018",
				"a(n) = A320047(1, n, 1). - _Kolosov Petro_, Oct 04 2018",
				"a(n) = T(3*n) - T(2*n-2) + T(n-2), where T(n) = A000217(n). In general, T(k)*T(n) = Sum_{i=0..k-1} (-1)^i*T((k-i)*(n-i)). - _Charlie Marion_, Dec 04 2020"
			],
			"maple": [
				"[seq(6*binomial(n,2),n=1..44)]; # _Zerinvary Lajos_, Nov 24 2006"
			],
			"mathematica": [
				"6 Accumulate[Range[0, 50]] (* _Harvey P. Dale_, Mar 05 2012 *)",
				"6 PolygonalNumber[Range[0, 20]] (* _Eric W. Weisstein_, Jul 27 2017 *)",
				"LinearRecurrence[{3, -3, 1}, {0, 6, 18}, 20] (* _Eric W. Weisstein_, Jul 27 2017 *)"
			],
			"program": [
				"(MAGMA) [3*n*(n+1): n in [0..50]]; // _Wesley Ivan Hurt_, Jun 09 2014",
				"(PARI) a(n)=3*n*(n+1) \\\\ _Charles R Greathouse IV_, Sep 24 2015",
				"(PARI) first(n) = Vec(6*x/(1 - x)^3 + O(x^n), -n) \\\\ _Iain Fox_, Feb 14 2018",
				"(GAP) List([0..44],n-\u003e3*n*(n+1)); # _Muniru A Asiru_, Mar 15 2019"
			],
			"xref": [
				"Cf. A000217, A000567, A003215, A008588, A024966, A028895, A033996, A046092, A049598, A084939, A084940, A084941, A084942, A084943, A084944, A124080.",
				"Cf. A002378 (3-cycles in triangular honeycomb acute knight graph), A045943 (4-cycles), A152773 (6-cycles)."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "Joe Keane (jgk(AT)jgk.org), Dec 11 1999",
			"references": 37,
			"revision": 120,
			"time": "2020-12-06T08:34:18-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}