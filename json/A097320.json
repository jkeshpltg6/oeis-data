{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A097320",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 97320,
			"data": "12,20,24,28,40,44,45,48,52,56,63,68,72,76,80,88,92,96,99,104,112,116,117,124,135,136,144,148,152,153,160,164,171,172,175,176,184,188,189,192,200,207,208,212,224,232,236,244,248,261,268,272,275,279,284,288",
			"name": "Numbers with more than one prime factor and, in the ordered factorization, the exponent always decreases when read from left to right.",
			"comment": [
				"If n = Product_{k=1..m} p(k)^e(k), then m \u003e 1, e(1) \u003e e(2) \u003e ... \u003e e(m)."
			],
			"link": [
				"Michael S. Branicky, \u003ca href=\"/A097320/b097320.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"80 is 2^4 * 5^1 and 4\u003e1, so 80 is in sequence."
			],
			"maple": [
				"with(numtheory): P:=proc(n) local a,k,ok; a:=ifactors(n)[2];",
				"if nops(a)\u003e1 then ok:=1; for k from 1 to nops(a)-1 do",
				"  if a[k][2]\u003c=a[k+1][2] then ok:=0; break; fi; od; fi;",
				"if ok=1 then ok:=0; n; fi; end: seq(P(i),i=1..3*10^2);",
				"# _Paolo P. Lava_, Jan 18 2018"
			],
			"mathematica": [
				"fQ[n_] := Module[{f = Transpose[FactorInteger[n]][[2]]}, Length[f] \u003e 1 \u0026\u0026 Max[Differences[f]] \u003c 0]; Select[Range[2, 288], fQ] (* _T. D. Noe_, Nov 04 2013 *)"
			],
			"program": [
				"(PARI) for(n=1, 320, F=factor(n); t=0; s=matsize(F)[1]; if(s\u003e1, for(k=1, s-1, if(F[k, 2]\u003c=F[k+1, 2], t=1; break)); if(!t, print1(n\", \"))))",
				"(PARI) is(n) = my(f = factor(n)[,2]); #f \u003e 1 \u0026\u0026 vecsort(f,,12) == f \\\\ _Rick L. Shepherd_, Jan 17 2018",
				"(Python)",
				"from sympy import factorint",
				"def ok(n):",
				"    e = list(factorint(n).values())",
				"    return 1 \u003c len(e) == len(set(e)) and e == sorted(e, reverse=True)",
				"print([k for k in range(289) if ok(k)]) # _Michael S. Branicky_, Dec 20 2021"
			],
			"xref": [
				"Subsequence of A126706, A097318, A112769. Supersequence of A096156.",
				"Cf. A097319, A230766."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Ralf Stephan_, Aug 04 2004",
			"references": 7,
			"revision": 27,
			"time": "2021-12-20T09:42:41-05:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}