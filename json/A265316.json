{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A265316",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 265316,
			"data": "0,2,7,21,23,64,69,71,193,207,209,214,579,581,622,627,629,643,1737,1739,1744,1866,1868,1882,1887,1889,1930,5211,5213,5218,5232,5234,5599,5604,5606,5647,5661,5663,5668,5790,5792,15634,15639,15641,15655,15696,15698",
			"name": "First row of A262057.",
			"comment": [
				"From _Robert Israel_, Feb 03 2016: (Start)",
				"a(n) is the first member of the n-th sequence in the greedy partition of the nonnegative integers into sequences that contain no 3-term arithmetic progression.",
				"As a special case (proved by Roth in 1953) of Szemerédi's theorem, sequences with no 3-term arithmetic progressions must have density 0.  In particular, the nonnegative integers can't be partitioned into finitely many such sequences.  Therefore this sequence is infinite.",
				"a(n+1) \u003e= a(n) + 2.  There seem to be many cases where this is an equality. (End)",
				"It can be deduced from the main result of Gerver, Propp, Simpson (below) that a(3n+1) = 3a(2n+1), a(3n+2) = 2 + 3a(2n+1), and a(3n) = 1 + 3a(2n). This implies infinitely many cases where a(n+1) = a(n) + 2. - _C. Kenneth Fan_, Dec 09 2018"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A265316/b265316.txt\"\u003eTable of n, a(n) for n = 1..140\u003c/a\u003e",
				"Matvey Borodin, Hannah Han, Kaylee Ji, Tanya Khovanova, Alexander Peng, David Sun, Isabel Tu, Jason Yang, William Yang, Kevin Zhang, Kevin Zhao, \u003ca href=\"https://arxiv.org/abs/1901.09818\"\u003eVariants of Base 3 over 2\u003c/a\u003e, arXiv:1901.09818 [math.NT], 2019.",
				"J. Gerver, J. Propp, J. Simpson, \u003ca href=\"https://doi.org/10.1090/S0002-9939-1988-0929018-1\"\u003eGreedily partitioning the natural numbers into sets free of arithmetic progressions\u003c/a\u003e, Proc. of the Amer. Math. Soc. 102 (1988), no. 3, pp. 765-772.",
				"Tanya Khovanova and Kevin Wu, \u003ca href=\"https://arxiv.org/abs/2007.09705\"\u003eBase 3/2 and Greedily Partitioned Sequences\u003c/a\u003e, arXiv:2007.09705 [math.NT], 2020.",
				"K. F. Roth, \u003ca href=\"https://doi.org/10.1112/jlms/s1-28.1.104\"\u003eOn certain sets of integers\u003c/a\u003e, Journal of the London Mathematical Society s1-28 (1953), 104-109.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Szemer%C3%A9di%27s_theorem\"\u003e Szemerédi's theorem\u003c/a\u003e."
			],
			"maple": [
				"M:= 100: # to get a(1) to a(M)",
				"for i from 1 to M do B[i]:= {}: F[i]:= {}: od:",
				"for x from 0 do",
				"  for i from 1 to M do",
				"     if not member(x,F[i]) then",
				"       F[i]:= F[i] union map(y -\u003e 2*x-y, B[i]);",
				"     B[i]:= B[i] union {x};",
				"     if not assigned(A[i]) then A[i]:= x fi;",
				"     break",
				"    fi",
				"  od;",
				"  if i = M+1 then break fi;",
				"od:",
				"seq(A[i],i=1..M); # _Robert Israel_, Feb 03 2016"
			],
			"xref": [
				"Cf. A262057."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Max Barrentine_, Dec 06 2015",
			"references": 4,
			"revision": 31,
			"time": "2020-07-21T02:25:48-04:00",
			"created": "2016-03-17T17:13:52-04:00"
		}
	]
}