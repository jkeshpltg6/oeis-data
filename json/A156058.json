{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A156058",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 156058,
			"data": "1,5,50,625,8750,131250,2062500,33515625,558593750,9496093750,164023437500,2870410156250,50784179687500,906860351562500,16323486328125000,295863189697265625,5395152282714843750,98911125183105468750,1822047042846679687500",
			"name": "a(n) = 5^n * Catalan(n).",
			"comment": [
				"From _Joerg Arndt_, Oct 22 2012: (Start)",
				"Number of strings of length 2*n of five different types of balanced parentheses.",
				"The number of strings of length 2*n of t different types of balanced parentheses is given by t^n * A000108(n): there are n opening parentheses in the strings, giving t^n choices for the type (the closing parentheses are chosen to match). (End)",
				"Number of Dyck paths of length 2n in which the step U=(1,1) come in 5 colors. [_José Luis Ramírez Ramírez_, Jan 31 2013]"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A156058/b156058.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 5^n*A000108(n).",
				"a(n) = upper left term in M^n, M = the infinite square production matrix as follows:",
				"5, 5, 0, 0, 0, 0,...",
				"5, 5, 5, 0, 0, 0,...",
				"5, 5, 5, 5, 0, 0,...",
				"5, 5, 5, 5, 5, 0,...",
				"...",
				"- Gary W. Adamson, Jul 18 2011",
				"E.g.f.: KummerM(1/2, 2, 20*x). - _Peter Luschny_, Aug 26 2012",
				"(n+1)*a(n) -10*(2*n-1)*a(n-1)=0. - _R. J. Mathar_, Oct 06 2012",
				"G.f.: c(5*x) with c(x) the o.g.f. of A000108 (Catalan). - _Philippe Deléham_, Nov 15 2013",
				"a(n)=sum{k=0..n} A085880(n,k)*4^k. - _Philippe Deléham_, Nov 15 2013",
				"G.f.: 1/(1 - 5*x/(1 - 5*x/(1 - 5*x/(1 - ...)))), a continued fraction. - _Ilya Gutkovskiy_, Apr 19 2017",
				"Sum_{n\u003e=0} 1/a(n) = 410/361 + 600*arctan(1/sqrt(19)) / (361*sqrt(19)). - _Vaclav Kotesovec_, Nov 23 2021"
			],
			"maple": [
				"A156058_list := proc(n) local j, a, w; a := array(0..n); a[0] := 1;",
				"for w from 1 to n do a[w] := 5*(a[w-1]+add(a[j]*a[w-j-1],j=1..w-1)) od; convert(a,list)end: A156058_list(16); # _Peter Luschny_, May 19 2011",
				"A156058 := proc(n)",
				"    5^n*A000108(n) ;",
				"end proc: # _R. J. Mathar_, Oct 06 2012"
			],
			"mathematica": [
				"Table[5^n CatalanNumber[n],{n,0,20}]  (* _Harvey P. Dale_, Mar 13 2011 *)"
			],
			"program": [
				"(MAGMA) [5^n*Catalan(n): n in [0..20]]; // _Vincenzo Librandi_, Jul 19 2011"
			],
			"xref": [
				"Cf. A000108, A151374, A005159, A151403."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Philippe Deléham_, Feb 03 2009",
			"references": 9,
			"revision": 40,
			"time": "2021-11-23T11:55:27-05:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}