{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A164617",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 164617,
			"data": "1,1,4,10,20,39,76,140,244,415,696,1140,1820,2861,4448,6816,10292,15372,22756,33356,48408,69683,99600,141312,199036,278557,387608,536230,737632,1009464,1374888,1863764,2514868,3378948,4521672,6027000,8002676",
			"name": "Expansion of (phi^3(q^3) / phi(q)) * (psi(-q^3) / psi^3(-q)) in powers of q where phi(), psi() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A164617/b164617.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Vaclav Kotesovec, \u003ca href=\"http://arxiv.org/abs/1509.08708\"\u003eA method of finding the asymptotics of q-series based on the convolution of generating functions\u003c/a\u003e, arXiv:1509.08708 [math.CO], Sep 30 2015",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of eta(q^6)^14 / (eta(q) * eta(q^2)^2 * eta(q^3)^5 * eta(q^4) * eta(q^12)^5) in powers of q.",
				"Euler transform of period 12 sequence [ 1, 3, 6, 4, 1, -6, 1, 4, 6, 3, 1, 0, ...].",
				"Convolution of A113973 and A132974. a(n) = A164616(3*n).",
				"a(n) ~ exp(2*Pi*sqrt(n/3)) / (2 * 3^(9/4) * n^(3/4)). - _Vaclav Kotesovec_, Oct 13 2015"
			],
			"example": [
				"G.f. = 1 + q + 4*q^2 + 10*q^3 + 20*q^4 + 39*q^5 + 76*q^6 + 140*q^7 + 244*q^8 + ..."
			],
			"mathematica": [
				"nmax=60; CoefficientList[Series[Product[(1-x^(6*k))^14 / ((1-x^k) * (1-x^(2*k))^2 * (1-x^(3*k))^5 * (1-x^(4*k)) * (1-x^(12*k))^5),{k,1,nmax}],{x,0,nmax}],x] (* _Vaclav Kotesovec_, Oct 13 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^6 + A)^14 / (eta(x + A) * eta(x^2 + A)^2 * eta(x^3 + A)^5 * eta(x^4 + A) * eta(x^12 + A)^5), n))};"
			],
			"xref": [
				"Cf. A113973, A132974, A164616."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Michael Somos_, Aug 17 2009",
			"references": 8,
			"revision": 13,
			"time": "2021-03-12T22:24:46-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}