{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A060728",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 60728,
			"data": "3,4,5,7,15",
			"name": "Numbers n such that Ramanujan's equation x^2 + 7 = 2^n has an integer solution.",
			"comment": [
				"See A038198 for corresponding x. - _Lekraj Beedassy_, Sep 07 2004",
				"Also numbers such that 2^(n-3)-1 is in A000217, i.e., a triangular number. - _M. F. Hasler_, Feb 23 2009",
				"With respect to _M. F. Hasler_'s comment above, all terms 2^(n-3) - 1 are known as the Ramanujan-Nagell triangular numbers (A076046). - _Raphie Frank_, Mar 31 2013"
			],
			"reference": [
				"J.-M. De Koninck, Ces nombres qui nous fascinent, Entry 181, p. 56, Ellipses, Paris 2008.",
				"J. Roberts, Lure of the Integers. pp. 90-91, MAA 1992.",
				"Ian Stewart \u0026 David Tall, Algebraic Number Theory and Fermat's Last Theorem, 3rd Ed. Natick, Massachusetts (2002): 96-98."
			],
			"link": [
				"T. Skolem, S. Chowla and D. J. Lewis, \u003ca href=\"http://www.jstor.org/stable/2033452\"\u003eThe Diophantine Equation 2^(n+2)-7=x^2 and Related Problems\u003c/a\u003e. Proc. Amer. Math. Soc. 10 (1959) 663-669. [_M. F. Hasler_, Feb 23 2009]",
				"Anonymous, \u003ca href=\"http://www.biochem.okstate.edu/OAS/OJAS/thiendo.htm\"\u003eDeveloping a general 2nd degree Diophantine Equation x^2 + p = 2^n\u003c/a\u003e",
				"M. Beeler, R. W. Gosper and R. Schroeppel, \u003ca href=\"http://www.inwap.com/pdp10/hbaker/hakmem/number.html#item31\"\u003eHAKMEM: item 31: A Ramanujan Problem (R. Schroeppel)\u003c/a\u003e",
				"Curtis Bright, \u003ca href=\"https://cs.uwaterloo.ca/~cbright/reports/ramanujans-square-equation.pdf\"\u003eSolving Ramanujan's Square Equation Computationally\u003c/a\u003e",
				"Spencer De Chenne, \u003ca href=\"http://buzzard.ups.edu/courses/2013spring/projects/spencer-ant-ups-434-2013.pdf\"\u003eThe Ramanujan-Nagell Theorem: Understanding the Proof\u003c/a\u003e",
				"T. Do, \u003ca href=\"http://ojas.ucok.edu/98/T98/THIENDO.HTM\"\u003eDeveloping A General 2nd Degree Diophantine Equation x^2 + p = 2^n\u003c/a\u003e",
				"A. Engel, \u003ca href=\"https://bayanbox.ir/view/5143526036407318041/problem-solving-strategies-math-cs.blog.ir.pdf\"\u003eProblem-Solving Strategies\u003c/a\u003e. p. 126.",
				"Gerry Myerson, \u003ca href=\"http://www.mathematik.uni-bielefeld.de/~sillke/PUZZLES/ramanujan-nagell\"\u003eBibliography\u003c/a\u003e",
				"T. Nagell, \u003ca href=\"https://projecteuclid.org/euclid.afm/1485893356\"\u003eThe Diophantine equation x^2 + 7 = 2^n\u003c/a\u003e, Ark. Mat. 4 (1961), no. 2-3, 185-187.",
				"S. Ramanujan, Journal of the Indian Mathematical Society, \u003ca href=\"http://www.imsc.res.in/~rao/ramanujan/collectedpapers/question/q464.htm\"\u003eQuestion 464(v,120)\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujansSquareEquation.html\"\u003eRamanujan's Square Equation\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/DiophantineEquation2ndPowers.html\"\u003eDiophantine Equation 2nd Powers\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Carmichael%27s_theorem\"\u003eCarmichael's Theorem\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Diophantine_equation\"\u003eDiophantine equation\u003c/a\u003e"
			],
			"formula": [
				"a(n) = log_2(8*A076046(n) + 8) = log_2(A227078(n) + 7)",
				"Empirically, a(n) = Fibonacci(c + 1) + 2 = ceiling[e^((c - 1)/2)] + 2 where {c} is the complete set of positive solutions to {n in N | 2 cos(2*Pi/n) is in Z}; c is in {1,2,3,4,6} (see A217290)."
			],
			"example": [
				"The fifth and ultimate solution to Ramanujan's equation is obtained for the 15th power of 2, so that we have x^2 + 7 = 2^15 yielding x = 181."
			],
			"mathematica": [
				"ramaNagell[n_] := Reduce[x^2 + 7 == 2^n, x, Integers] =!= False; Select[ Range[100], ramaNagell] (* _Jean-François Alcover_, Sep 21 2011 *)"
			],
			"program": [
				"(MAGMA) [n: n in [0..100] | IsSquare(2^n-7)]; // _Vincenzo Librandi_, Jan 07 2014",
				"(PARI) is(n)=issquare(2^n-7) \\\\ _Anders Hellström_, Dec 12 2015"
			],
			"xref": [
				"Cf. A002249, A038198, A076046, A077020, A077021, A107920, A215795, A227078"
			],
			"keyword": "fini,full,nonn",
			"offset": "1,1",
			"author": "_Lekraj Beedassy_, Apr 25 2001",
			"ext": [
				"Added keyword \"full\", _M. F. Hasler_, Feb 23 2009"
			],
			"references": 16,
			"revision": 91,
			"time": "2018-03-20T18:41:23-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}