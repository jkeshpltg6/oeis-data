{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A323710",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 323710,
			"data": "1,3,2,7,8,6,4,5,128,24,256,14,64,12,16,15,32,384,340282366920938463463374607431768211456,56,16777216,768,115792089237316195423570985008687907853269984665640564039457584007913129639936,10,16384,192,18446744073709551616,28,4096,48",
			"name": "a(n) is the symmetrical of n via transport of structure from binary trees, where the binary tree of n is built as follows: create a root with value n and recursively apply the rule {write node's value as (2^c)*(2*k+1); if c\u003e0, create a left child with value c; if k\u003e0, create a right child with value k}.",
			"comment": [
				"Let f denote the bijection that maps positive integers onto binary trees, defined in the name; let g be its inverse; let r denote the symmetry on binary trees (i.e., starting from the root, r recursively swaps left and right children). By definition a(n) = g(r(f(n))).",
				"If instead of r, one uses s, the operation that swaps the left and right children of the root, without recursion, then one gets g(s(f(n))) = A117303(n).",
				"Better leave a(39) = 2^340282366920938463463374607431768211456 not fully evaluated."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A323710/b323710.txt\"\u003eTable of n, a(n) for n = 1..38\u003c/a\u003e"
			],
			"formula": [
				"a(a(n)) = n.",
				"a(n) = n iff n is in A323752."
			],
			"example": [
				"100 = (2^2)*(2*12+1) and recursively, 2 = (2^1), 12 = (2^2)*(2*1+1). We then have the following binary tree representation of 100:",
				"     100",
				"     / \\",
				"    2  12",
				"   /   / \\",
				"  1   2   1",
				"     /",
				"    1",
				"Erase the numerical values, just keep the tree structure:",
				"      o",
				"     / \\",
				"    o   o",
				"   /   / \\",
				"  o   o   o",
				"     /",
				"    o",
				"Take its symmetrical:",
				"      o",
				"     / \\",
				"    o   o",
				"   / \\   \\",
				"  o   o   o",
				"       \\",
				"        o",
				"Compute back new numerical values from the leafs (value: 1) up:",
				"(2*1+1) = 3; (2^1)*(2*3+1) = 14; (2^14)*(2*3+1) = 114688",
				"   114688",
				"     / \\",
				"   14   3",
				"   / \\   \\",
				"  1   3   1",
				"       \\",
				"        1",
				"So, a(100) = 114688."
			],
			"maple": [
				"a:= proc(n) option remember; `if`(n=0, 0, (j-\u003e",
				"      (2*a(j)+1)*2^a((n/2^j-1)/2))(padic[ordp](n, 2)))",
				"    end:",
				"seq(a(n), n=1..38);  # _Alois P. Heinz_, Jan 24 2019"
			],
			"mathematica": [
				"f[0]:=x",
				"f[n_]:=Module[{c,k},c=IntegerExponent[n,2];k=(n/2^c-1)/2;o[f[c],f[k]]])",
				"g[x]:=0",
				"g[o[C_,K_]]:=(2^g[C])(2g[K]+1)",
				"r[x]:=x",
				"r[o[C_,K_]]:=o[r[K],r[C]]",
				"a[n_]:=g@r@f[n]",
				"Table[a[n], {n, 1, 30}]"
			],
			"xref": [
				"Cf. A117303 (variant where swap left/right is not recursively applied).",
				"Cf. A323665.",
				"Cf. A323752 (fixed points of this sequence)."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Luc Rousseau_, Jan 24 2019",
			"references": 3,
			"revision": 13,
			"time": "2019-01-27T17:56:44-05:00",
			"created": "2019-01-24T19:31:01-05:00"
		}
	]
}