{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A290700",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 290700,
			"data": "1,5,25,49,141,389,1009,2761,7441,19925,53769,144721,389325,1048325,2821665,7594761,20444065,55029413,148124153,398713969,1073231821,2888859781,7776063377,20931130057,56341150641,151655712629,408217654249,1098815597201",
			"name": "Number of minimal edge covers in the n-prism graph.",
			"comment": [
				"The n-prism graph is well defined for n \u003e= 3. Sequence extended to n = 1 using recurrence. - _Andrew Howroyd_, Aug 10 2017"
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A290700/b290700.txt\"\u003eTable of n, a(n) for n = 1..200\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/MinimalEdgeCover.html\"\u003eMinimal Edge Cover\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PrismGraph.html\"\u003ePrism Graph\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_09\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1, 2, 6, 2, 2, -2, -2, -1, 1)."
			],
			"formula": [
				"From _Andrew Howroyd_, Aug 10 2017: (Start)",
				"a(n) = a(n-1) + 2*a(n-2) + 6*a(n-3) + 2*a(n-4) + 2*a(n-5) - 2*a(n-6) - 2*a(n-7) - a(n-8) + a(n-9) for n \u003e 9.",
				"G.f.: x*(1 + 4*x + 18*x^2 + 8*x^3 + 10*x^4 - 12*x^5 - 14*x^6 - 8*x^7 + 9*x^8)/((1 - 2*x - 2*x^2 + x^4)*(1 + x + x^2 - x^3)*(1 + x^2)).",
				"(End)"
			],
			"mathematica": [
				"Table[2 Cos[n Pi/2] + RootSum[-1 + # + #^2 + #^3 \u0026, #^n \u0026] -",
				"  RootSum[1 - 2 #^2 - 2 #^3 + #^4 \u0026, -2 #^(n + 2) - 2 #^(n + 3) + #^(n + 4) \u0026], {n, 20}]",
				"LinearRecurrence[{1, 2, 6, 2, 2, -2, -2, -1, 1}, {1, 5, 25, 49, 141, 389, 1009, 2761, 7441}, 20]",
				"CoefficientList[Series[-( (1 + 4 x + 18 x^2 + 8 x^3 + 10 x^4 - 12 x^5 - 14 x^6 - 8 x^7 + 9 x^8)/((1 + x^2) (-1 - x - x^2 + x^3) (1 - 2 x - 2 x^2 + x^4))), {x, 0, 20}], x]"
			],
			"program": [
				"(PARI)",
				"Vec((1 + 4*x + 18*x^2 + 8*x^3 + 10*x^4 - 12*x^5 - 14*x^6 - 8*x^7 + 9*x^8)/((1 - 2*x - 2*x^2 + x^4)*(1 + x + x^2 - x^3)*(1 + x^2))+O(x^30)) \\\\ _Andrew Howroyd_, Aug 10 2017"
			],
			"xref": [
				"Cf. A123304."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Eric W. Weisstein_, Aug 09 2017",
			"ext": [
				"a(1)-a(2) and terms a(9) and beyond from _Andrew Howroyd_, Aug 10 2017"
			],
			"references": 1,
			"revision": 13,
			"time": "2017-08-11T10:57:41-04:00",
			"created": "2017-08-09T20:56:41-04:00"
		}
	]
}