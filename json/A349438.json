{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A349438",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 349438,
			"data": "1,1,1,1,2,0,2,1,3,0,4,-1,2,0,2,1,4,-1,2,-2,2,0,4,-2,10,0,9,-2,6,-4,2,1,4,0,4,-4,6,0,2,-4,4,-4,2,-4,6,0,4,-3,14,-4,4,-2,6,-6,8,-4,2,0,6,-6,2,0,6,1,4,-8,6,-4,4,-8,4,-6,2,0,10,-2,8,-4,6,-6,27,0,4,-6,8,0,6,-8,6,-16,4,-4,2,0,4",
			"name": "Dirichlet convolution of A000027 with A349348 (Dirichlet inverse of A252463), where A252463 shifts the prime factorization of odd numbers one step towards smaller primes and divides even numbers by two.",
			"comment": [
				"Convolving this sequence with A348045 gives Euler phi, A000010.",
				"It might first seem that A000265(a(p^k)) = p^(k-1) for all odd primes and all exponents k \u003e= 1, but this does not hold for prime 37. However, with p=37, identity A065330(A349438(37^k)) = 37^(k-1) seems to hold for all exponents k \u003e= 1. - _Antti Karttunen_, Nov 20 2021"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A349438/b349438.txt\"\u003eTable of n, a(n) for n = 1..20000\u003c/a\u003e",
				"\u003ca href=\"/index/Pri#prime_indices\"\u003eIndex entries for sequences computed from indices in prime factorization\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Sum_{d|n} d * A349348(n/d)."
			],
			"mathematica": [
				"f[p_, e_] := NextPrime[p, -1]^e; s[1] = 1; s[n_] := If[EvenQ[n], n/2, Times @@ f @@@ FactorInteger[n]]; sinv[1] = 1; sinv[n_] := sinv[n] = -DivisorSum[n, sinv[#] * s[n/#] \u0026, # \u003c n \u0026]; a[n_] := DivisorSum[n, # * sinv[n/#] \u0026]; Array[a, 100] (* _Amiram Eldar_, Nov 18 2021 *)"
			],
			"program": [
				"(PARI)",
				"A064989(n) = {my(f); f = factor(n); if((n\u003e1 \u0026\u0026 f[1,1]==2), f[1,2] = 0); for (i=1, #f~, f[i,1] = precprime(f[i,1]-1)); factorback(f)};",
				"A252463(n) = if(!(n%2),n/2,A064989(n));",
				"memoA349348 = Map();",
				"A349348(n) = if(1==n,1,my(v); if(mapisdefined(memoA349348,n,\u0026v), v, v = -sumdiv(n,d,if(d\u003cn,A252463(n/d)*A349348(d),0)); mapput(memoA349348,n,v); (v)));",
				"A349438(n) = sumdiv(n,d,d*A349348(n/d));"
			],
			"xref": [
				"Cf. A000027, A064989, A252463, A349348, A349437 (Dirichlet inverse), A349439 (sum with it).",
				"Cf.  also A000010, A000265, A065330, A348045."
			],
			"keyword": "sign",
			"offset": "1,5",
			"author": "_Antti Karttunen_, Nov 18 2021",
			"references": 4,
			"revision": 16,
			"time": "2021-11-21T01:19:22-05:00",
			"created": "2021-11-18T09:55:06-05:00"
		}
	]
}