{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A033665",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 33665,
			"data": "0,0,0,0,0,0,0,0,0,0,1,0,1,1,1,1,1,1,1,2,1,1,0,1,1,1,1,1,2,1,1,1,1,0,1,1,1,2,1,2,1,1,1,1,0,1,2,1,2,2,1,1,1,1,1,0,1,2,2,3,1,1,1,1,2,1,0,2,3,4,1,1,1,2,1,2,2,0,4,6,1,1,2,1,2,2,3,4,0,24,1,2,1,2,2,3,4,6,24,0,1,0,1,1",
			"name": "Number of 'Reverse and Add' steps needed to reach a palindrome starting at n, or -1 if n never reaches a palindrome.",
			"comment": [
				"Palindromes themselves are not 'Reverse and Add!'ed, so they yield a zero!",
				"Numbers n that may have a(n) = -1 (i.e., potential Lychrel numbers) appear in A023108. - _Michael De Vlieger_, Jan 11 2018",
				"Record indices and values are given in A065198 and A065199. - _M. F. Hasler_, Feb 16 2020"
			],
			"reference": [
				"D. Wells, The Penguin Dictionary of Curious and Interesting Numbers, pp. 142-3 Penguin Books 1987."
			],
			"link": [
				"Kerry Mitchell, \u003ca href=\"/A033665/b033665.txt\"\u003eTable of n, a(n) for n = 0..195\u003c/a\u003e",
				"P. De Geest, \u003ca href=\"http://www.worldofnumbers.com/weblinks.htm\"\u003eSome thematic websources\u003c/a\u003e",
				"Jason Doucette, \u003ca href=\"http://www.jasondoucette.com/worldrecords.html#Most\"\u003eWorld Records\u003c/a\u003e",
				"S. K. Eddins, \u003ca href=\"https://web.archive.org/web/20091014155324/http://staff.imsa.edu:80/math/journal/volume4/articles/Palindrome.pdf\"\u003eThe Palindromic Order Of A Number\u003c/a\u003e [archived page]",
				"Kerry Mitchell, \u003ca href=\"/A033665/a033665.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e (The -1 entries are only conjectural)",
				"Kerry Mitchell, \u003ca href=\"/A033665/a033665_1.txt\"\u003eTable of n, a(n) for n = 0..100000\u003c/a\u003e (The -1 entries are only conjectural)",
				"I. Peter, \u003ca href=\"https://web.archive.org/web/20110728141429/http://www.floot.demon.co.uk/palindromes.html\"\u003eSearch for the biggest numeric palindrome\u003c/a\u003e [archived page]",
				"T. Trotter, Jr., \u003ca href=\"https://web.archive.org/web/20180129201104/http://www.trottermath.net/recurops/palndrom.html\"\u003ePalindrome Power\u003c/a\u003e [archived page]",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/196-Algorithm.html\"\u003e196-Algorithm\u003c/a\u003e.",
				"\u003ca href=\"/index/Res#RAA\"\u003eIndex entries for sequences related to Reverse and Add!\u003c/a\u003e"
			],
			"example": [
				"19 -\u003e 19+91 = 110 -\u003e 110+011 = 121 = palindrome, took 2 steps, so a(19)=2.",
				"n = 89 needs 24 steps to end up with the palindrome 8813200023188. See A240510. - _Wolfdieter Lang_, Jan 12 2018"
			],
			"mathematica": [
				"rev[n_]:=FromDigits[Reverse[IntegerDigits[n]]];radd[n_]:=n+rev[n];",
				"pal[n_]:=If[n==rev[n],True,False];",
				"raddN[n_]:=Length[NestWhileList[radd[#]\u0026,n,pal[#]==False\u0026]]-1;",
				"raddN/@Range[0,195] (* _Ivan N. Ianakiev_, Aug 31 2015 *)",
				"With[{nn = 10^3}, Array[-1 + Length@ NestWhileList[# + IntegerReverse@ # \u0026, #, !PalindromeQ@ # \u0026, 1, nn] /. k_ /; k == nn -\u003e -1 \u0026, 200]] (* _Michael De Vlieger_, Jan 11 2018 *)"
			],
			"program": [
				"(PARI) rev(n)={d=digits(n);p=\"\";for(i=1,#d,p=concat(Str(d[i]),p));return(eval(p))}",
				"a(n)=if(n==rev(n),return(0));for(k=1,10^3,i=n+rev(n);if(rev(i)==i,return(k));n=i)",
				"n=0;while(n\u003c100,print1(a(n),\", \");n++) \\\\ _Derek Orr_, Jul 28 2014",
				"(PARI) A033665(n,LIM=333)={-!for(i=0,LIM,my(r=A004086(n)); n==r\u0026\u0026return(i); n+=r)} \\\\ with {A004086(n)=fromdigits(Vecrev(digits(n)))}. The second optional arg is a search limit that could be taken smaller up to very large n, e.g., 99 for n \u003c 10^9, 200 for n \u003c 10^14, 250 for n \u003c 10^18: see A065199 for the records and A065198 for the n's. - _M. F. Hasler_, Apr 13 2019, edited Feb 16 2020"
			],
			"xref": [
				"Cf. A002113, A023108, A023109, A033865, A006960, A016016, A063048, A240510.",
				"Equals A030547(n) - 1.",
				"Cf. A065198, A065199 (record indices \u0026 values)."
			],
			"keyword": "nonn,base,easy",
			"offset": "0,20",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Patrick De Geest_, Jun 15 1998",
				"I truncated the b-file at n=195, since the value of a(196) is not presently known (cf. A006960). The old b-files are now a-files. - _N. J. A. Sloane_, May 09 2015"
			],
			"references": 20,
			"revision": 67,
			"time": "2020-02-19T04:06:09-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}