{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A294745",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 294745,
			"data": "1,2,5,11,21,6,27,55,10,46,101,23,89,180,9,114,234,3,139,292,16,206,416,38,291,591,30,355,4,410,845,25,490,986,40,568,1163,35,665,1331,56,759,18,798,1659,63,966,1956,65,1100,19,1195,2420,74,1400,22,1453",
			"name": "Lexicographically first sequence of distinct positive integers in which successive terms differ by distinct triangular numbers.",
			"comment": [
				"a(1) = 1; for n \u003e 1, a(n) is the smallest unused positive integer such that |a(n) - a(n-1)| is a triangular number (A000217) that has not already been used as the absolute difference between two successive terms.",
				"This sequence differs from Recamán's sequence (A005132) in its starting value (1 vs. 0) and the absolute distance between its successive terms |a(n) - a(n-1)| (any unused triangular number vs. n).",
				"Conjecture: this sequence is a permutation of the positive integers.",
				"Terms that are less than all subsequent terms: a(1)=1, a(2)=2, a(18)=3, a(29)=4, a(157)=7, a(216)=8, a(254)=13, a(1220)=20; a(?)=33 (does not appear in the first 40000 terms)."
			],
			"link": [
				"Jon E. Schoenfield, \u003ca href=\"/A294745/b294745.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"a(1) = 1 since all terms are positive integers and the sequence is lexicographically first.",
				"Writing the k-th triangular number A000217(k) as T(k):",
				"a(2) = 2 because 2 is the smallest unused positive number that differs from a(1)=1 by a triangular number: |2 - 1| = 1 = T(1);",
				"a(3) = 5 because 5 is the smallest unused positive number that differs from a(2)=2 by a triangular number other than 1 (already used): |5 - 2| = 3 = T(2).",
				"Similarly,",
				"  a(4) = 11: |11 -  5| =  6 = T(3);",
				"  a(5) = 21: |21 - 11| = 10 = T(4);",
				"  a(6) =  6: | 6 - 21| = 15 = T(5);",
				"  a(7) = 27: |27 -  6| = 21 = T(6);",
				"  a(8) = 55: |55 - 27| = 28 = T(7);",
				"  a(9) = 10: |10 - 55| = 45 = T(9) (T(8) has not yet been used, but 55 - 45 = 10 gives a smaller unused number than 55 - 36 = 19)."
			],
			"program": [
				"(MAGMA)",
				"a:=[1]; TUsed:=[]; for n in [2..57] do tBest:=0; k:=0; while true do k+:=1; T:=(k*(k+1)) div 2; if not (T in TUsed) then t:=a[n-1]-T; if t lt 1 then break; end if; if not (t in a) then tBest:=t; end if; end if; end while; if tBest eq 0 then k:=0; while true do k+:=1; T:=(k*(k+1)) div 2; if not (T in TUsed) then t:=a[n-1]+T; if not (t in a) then tBest:=t; break; end if; end if; end while; end if; a[n]:=tBest; TUsed[#TUsed+1]:=Abs(a[n]-a[n-1]); end for; a;"
			],
			"xref": [
				"Cf. A000217 (triangular numbers), A005132 (Recamán's sequence)."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Jon E. Schoenfield_, Dec 09 2017",
			"references": 1,
			"revision": 22,
			"time": "2017-12-12T00:52:13-05:00",
			"created": "2017-12-09T20:20:19-05:00"
		}
	]
}