{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A154372",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 154372,
			"data": "1,1,1,1,4,1,1,12,9,1,1,32,54,16,1,1,80,270,160,25,1,1,192,1215,1280,375,36,1,1,448,5103,8960,4375,756,49,1,1,1024,20412,57344,43750,12096,1372,64,1",
			"name": "Triangle T(n,k) = (k+1)^(n-k)*binomial(n,k).",
			"comment": [
				"From A152650/A152656,coefficients of other exponential polynomials(*). a(n) is triangle A152818 where terms of each column is divided by the beginning one. See A000004, A001787(n+1), A006043=2*A027472, A006044=6*A038846.",
				"(*) Not factorial as written in A006044. See A000110, Bell-Touchard. Second diagonal is 1,4,9,16,25, denominators of Lyman's spectrum of hydrogen, A000290(n+1) which has homogeneous indices for denominators series of Rydberg-Ritz spectrum of hydrogen.",
				"The matrix inverse starts",
				"      1;",
				"     -1,      1;",
				"      3,     -4,     1;",
				"    -16,     24,    -9,     1;",
				"    125,   -200,    90,   -16,   1;",
				"  -1296,   2160, -1080,   240, -25,   1;",
				"  16807, -28812, 15435, -3920, 525, -36, 1;",
				".. compare with A122525 (row reversed). - _R. J. Mathar_, Mar 22 2013",
				"From _Peter Bala_, Jan 14 2015: (Start)",
				"Exponential Riordan array [exp(z), z*exp(z)]. This triangle is the particular case a = 0, b = 1, c = 1 of the triangle of generalized Stirling numbers of the second kind S(a,b,c) defined in the Bala link. Cf. A059297.",
				"This is the triangle of connection constants when expressing the monomials x^n as a linear combination of the basis polynomials (x - 1)*(x - k - 1)^(k-1), k = 0,1,2,.... For example, from row 3 we have x^3 = 1 + 12*(x - 1) + 9*(x - 1)*(x - 3) + (x - 1)*(x - 4)^2.",
				"Let M be the infinite lower unit triangular array with (n,k)-th entry (k*(n - k + 1) + 1)/(k + 1)*binomial(n,k). M is the row reverse of A145033. For k = 0,1,2,... define M(k) to be the lower unit triangular block array",
				"/I_k 0\\",
				"\\ 0  M/ having the k X k identity matrix I_k as the upper left block; in particular, M(0) = M. The infinite product M(0)*M(1)*M(2)*..., which is clearly well-defined, is equal to the present triangle. See the Example section. (End)",
				"T(n,k) is also the number of idempotent partial transformations of {1,2,...,n} having exactly k fixed points. - _Geoffrey Critzer_, Nov 25 2021"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A154372/b154372.txt\"\u003eTable of n, a(n) for n = 0..1274\u003c/a\u003e",
				"Peter Bala, \u003ca href=\"/A143395/a143395.pdf\"\u003eA 3 parameter family of generalized Stirling numbers\u003c/a\u003e",
				"Emanuele Munarini, \u003ca href=\"https://doi.org/10.2298/AADM180226017M\"\u003eCombinatorial identities involving the central coefficients of a Sheffer matrix\u003c/a\u003e, Applicable Analysis and Discrete Mathematics (2019) Vol. 13, 495-517."
			],
			"formula": [
				"T(n,k) = (k+1)^(n-k)*binomial(n,k). k!*T(n,k) gives the entries for A152818 read as a triangular array.",
				"E.g.f.: exp(x*(1+t*exp(x))) = 1 + (1+t)*x + (1+4*t+t^2)*x^2/2! + (1+12*t+9*t^2+t*3)*x^3/3! + .... O.g.f.: Sum_{k\u003e=1} (t*x)^(k-1)/(1-k*x)^k = 1 + (1+t)*x + (1+4*t+t^2)*x^2 + .... Row sums are A080108. - _Peter Bala_, Oct 09 2011",
				"From _Peter Bala_, Jan 14 2015: (Start)",
				"Recurrence equation: T(n+1,k+1) = T(n,k+1) + Sum_{j = 0..n-k} (j + 1)*binomial(n,j)*T(n-j,k) with T(n,0) = 1 for all n.",
				"Equals the matrix product A007318 * A059297. (End)"
			],
			"example": [
				"With the array M(k) as defined in the Comments section, the infinite product M(0)*M(1)*M(2)*... begins",
				"/1      \\ /1        \\ /1        \\      /1        \\",
				"|1 1     ||0 1       ||0 1      |      |1  1      |",
				"|1 3 1   ||0 1 1     ||0 0 1    |... = |1  4  1   |",
				"|1 6 5 1 ||0 1 3 1   ||0 0 1 1  |      |1 12  9  1|",
				"|...     ||0 1 6 5 1 ||0 0 1 3 1|      |...       |",
				"|...     ||...       ||...      |      |          |",
				"- _Peter Bala_, Jan 13 2015"
			],
			"mathematica": [
				"T[n_, k_] := (k + 1)^(n - k)*Binomial[n, k]; Table[T[n, k], {n, 0, 10}, {k, 0, n}] // Flatten (* _G. C. Greubel_, Sep 15 2016 *)"
			],
			"program": [
				"(MAGMA) /* As triangle */ [[(k+1)^(n-k)*Binomial(n,k) : k in [0..n]]: n in [0.. 15]]; // _Vincenzo Librandi_, Sep 15 2016"
			],
			"xref": [
				"Cf. A080108, A152818, A059297, A145033."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "0,5",
			"author": "_Paul Curtz_, Jan 08 2009",
			"references": 4,
			"revision": 37,
			"time": "2021-12-14T20:34:24-05:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}