{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A122771",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 122771,
			"data": "1,2,-1,-1,-1,1,2,-2,2,-1,-1,-2,4,-3,1,2,-3,6,-7,4,-1,-1,-3,9,-13,11,-5,1,2,-4,12,-22,24,-16,6,-1,-1,-4,16,-34,46,-40,22,-7,1,2,-5,20,-50,80,-86,62,-29,8,-1,-1,-5,25,-70,130,-166,148,-91,37,-9,1,2,-6,30,-95,200,-296,314,-239,128,-46,10,-1,-1,-6,36,-125",
			"name": "Triangle read by rows, 0 \u003c= k \u003c= n: T(n,k) is the coefficient of x^k in the characteristic polynomial of I + A^(-1), where A is the n-step Fibonacci companion matrix and I is the identity matrix.",
			"comment": [
				"Here, the characteristic polynomial of a matrix M is defined as det(M-x*I).",
				"The matrix I + A^(-1) for 2 \u003c= n \u003c= 6:",
				"  2 X 2: {{0, 1}, {1, 1}},",
				"  3 X 3: {{0, -1, 1}, {1, 1, 0}, {0, 1, 1}},",
				"  4 X 4: {{0, -1, -1, 1}, {1, 1, 0, 0}, {0, 1, 1, 0}, {0, 0, 1,1}},",
				"  5 X 5: {{0, -1, -1, -1, 1}, {1, 1, 0, 0, 0}, {0, 1, 1, 0, 0}, {0, 0, 1, 1, 0}, {0, 0, 0, 1,1}},",
				"  6 X 6: {{0, -1, -1, -1, -1, 1}, {1, 1, 0, 0, 0, 0}, {0, 1, 1, 0, 0, 0}, {0, 0, 1, 1, 0, 0}, {0, 0, 0, 1, 1, 0}, {0, 0, 0, 0, 1, 1}}"
			],
			"reference": [
				"Jay Kappraff, Beyond Measure, A Guided Tour Through Nature, Myth and Number, World Scientific, 2002.",
				"Kappraff, J., Blackmore, D. and Adamson, G. \"Phyllotaxis as a Dynamical System: A Study in Number.\" In Symmetry in Plants edited by R. V. Jean and D. Barabe. Singapore: World Scientific. (1996)."
			],
			"link": [
				"P. Steinbach, \u003ca href=\"https://www.jstor.org/stable/2691048\"\u003eGolden fields: a case for the heptagon\u003c/a\u003e, Math. Mag. 70 (1997), no. 1, 22-31."
			],
			"example": [
				"Triangular array:",
				"   1;",
				"   2,  -1;",
				"  -1,  -1,   1;",
				"   2,  -2,   2,  -1;",
				"  -1,  -2,   4,  -3,   1;",
				"   2,  -3,   6,  -7,   4,  -1;",
				"  -1,  -3,   9, -13,  11,  -5,   1;",
				"   2,  -4,  12, -22,  24, -16,   6,  -1;",
				"  -1,  -4,  16, -34,  46, -40,  22,  -7,   1;",
				"   2,  -5,  20, -50,  80, -86,  62, -29,   8,  -1;"
			],
			"mathematica": [
				"An[d_] := Table[If[n == d, 1, If[m == n + 1, 1, 0]], {n, 1, d}, {m, 1, d}];",
				"Join[{{1}}, Table[CoefficientList[CharacteristicPolynomial[IdentityMatrix[d] + MatrixPower[An[d], -1], x], x], {d, 1, 20}]];",
				"Flatten[%]"
			],
			"program": [
				"(Python)",
				"from sympy import Matrix,eye",
				"def A122771_row(n):",
				"  if n==0: return [1]",
				"  A=Matrix(n,n,lambda i,j:int(i==n-1 or i==j-1))",
				"  p=(eye(n)+A.inv()).charpoly()",
				"  return [(-1)**n*c for c in p.all_coeffs()[::-1]] # _Pontus von Brömssen_, May 01 2021"
			],
			"keyword": "tabl,sign",
			"offset": "0,2",
			"author": "_Gary W. Adamson_ and _Roger L. Bagula_, Oct 20 2006",
			"ext": [
				"Edited by _Pontus von Brömssen_, May 01 2021"
			],
			"references": 2,
			"revision": 38,
			"time": "2021-06-12T02:45:47-04:00",
			"created": "2006-12-06T03:00:00-05:00"
		}
	]
}