{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A242419",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 242419,
			"data": "1,2,3,4,5,6,7,8,9,15,11,18,13,35,10,16,17,12,19,75,21,77,23,54,25,143,27,245,29,30,31,32,55,221,14,36,37,323,91,375,41,105,43,847,50,437,47,162,49,45,187,1859,53,24,33,1715,247,667,59,150,61,899,147,64,65",
			"name": "Reverse both the exponents and the deltas of the indices of distinct primes present in the prime factorization of n.",
			"comment": [
				"This self-inverse permutation (involution) of natural numbers preserves both the total number of prime divisors and the (index of) largest prime factor of n, i.e. for all n it holds that A001222(a(n)) = A001222(n) and A006530(a(n)) = A006530(n) [equally: A061395(a(n)) = A061395(n)].",
				"Considered as an operation on partitions encoded by the indices of primes in the prime factorization of n (as in table A112798), this implements a bijection which reverses the order of \"steps\" in Young (or Ferrers) diagram of a partition (but keeps the horizontal line segment of each step horizontal and the vertical line segment vertical). Please see the last example in the example section.",
				"To understand the given recursive formula, it helps to see that in the above context (Young diagrams drawn with French notation), the sequences employed effect the following operations:",
				"  A001222: gives the height of whole diagram,",
				"  A051119: removes the bottommost step from the diagram,",
				"  A241919: gives the length of the horizontal line segment of the bottom step, i.e. its width,",
				"  A071178: gives the length of the vertical line segment of the bottom step, i.e. its height,",
				"  A242378(k,n): increases the width of whole Young diagram encoded by n by adding a rectangular area A001222(n) squares high and k squares wide to its left,",
				"  and finally, multiplying by A000040(a)^b adds a new topmost step whose width is a and height is b. Particularly, multiplying by (A000040(A241919(n))^A071178(n)) transfers the bottommost step to the top."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A242419/b242419.txt\"\u003eTable of n, a(n) for n = 1..8192\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Young_diagram\"\u003eYoung diagram\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"If n = p_a^e_a * p_b^e_b * ... * p_h^e_h * p_i^e_i * p_j^e_j * p_k^e_k, where p_a \u003c ... \u003c p_k are distinct primes (sorted into ascending order) in the prime factorization of n, and e_a .. e_k are their nonzero exponents, then a(n) = p_{k-j}^e_k * p_{k-i}^e_j  * p_{k-h}^e_i * ... * p_{k-a}^e_b * p_k^e_a.",
				"As a recurrence:",
				"a(1) = 1, and for n\u003e1, a(n) = (A000040(A241919(n))^A071178(n)) * A242378(A241919(n), a(A051119(n))).",
				"By composing related permutations:",
				"a(n) = A122111(A153212(n)) = A153212(A122111(n)).",
				"a(n) = A069799(A242415(n)) = A242415(A069799(n)).",
				"a(n) = A105119(A242420(n))."
			],
			"example": [
				"For n = 10 = 2*5 = p_1^1 * p_3^1, we get p_(3-1)^1 * p_3^1 = 3 * 5 = 15, thus a(10) = 15.",
				"For n = 20 = 2*2*5 = p_1^2 * p_3^1, we get p_(3-1)^1 * p_3^2 = 3^1 * 5^2 = 3*25 = 75, thus a(20) = 75.",
				"For n = 84 = 2*2*3*7 = p_1^2 * p_2 * p_4, when we reverse the deltas of indices, and reverse also the order of exponents, we get p_(4-2) * p_(4-1) * p_4^2 = 3 * 5 * 7^2 = 735, thus a(84) = 735.",
				"For n = 2200, we see that it encodes the partition (1,1,1,3,3,5) in A112798 as 2200 = p_1 * p_1 * p_1 * p_3 * p_3 * p_5 = 2^3 * 5^2 * 11. This in turn corresponds to the following Young diagram in French notation:",
				"   _",
				"  | |",
				"  | |",
				"  | |_ _",
				"  |     |",
				"  |     |_ _",
				"  |_ _ _ _ _|",
				"Reversing the order of \"steps\", so that each horizontal and vertical line segment centered around a \"convex corner\" moves as a whole, means that the first stair from the top (one unit wide and three units high) is moved to the last position, the second one (two units wide and two units high) stays in the middle, and the original bottom step (two units wide and one unit high) will be the new topmost step, thus we get the following Young diagram:",
				"   _ _",
				"  |   |_ _",
				"  |       |",
				"  |       |_",
				"  |         |",
				"  |         |",
				"  |_ _ _ _ _|",
				"which represents the partition (2,4,4,5,5,5), encoded in A112798 by p_2 * p_4^2 * p_5^3 = 3 * 7^2 * 11^3 = 195657, thus a(2200) = 195657."
			],
			"program": [
				"(Scheme, with _Antti Karttunen_'s IntSeq-library)",
				"(definec (A242419 n) (cond ((\u003c= n 1) n) (else (* (expt (A000040 (A241919 n)) (A071178 n)) (A242378bi (A241919 n) (A242419 (A051119 n)))))))"
			],
			"xref": [
				"Fixed points: A242417.",
				"Cf. A112798, A122111, A153212, A000040, A241919, A071178, A242378, A051119, A243057, A105119, A242420.",
				"{A000027, A122111, A153212, A242419} form a 4-group.",
				"{A000027, A069799, A242415, A242419} form also a 4-group."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Antti Karttunen_, May 17 2014",
			"references": 10,
			"revision": 37,
			"time": "2014-06-02T22:23:05-04:00",
			"created": "2014-06-02T22:23:04-04:00"
		}
	]
}