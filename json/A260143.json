{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A260143",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 260143,
			"data": "2,3,14,15,21,22,33,34,35,38,39,44,45,57,58,75,76,85,86,87,93,94,95,98,99,116,117,118,119,122,123,133,134,135,136,141,142,143,145,146,147,148,158,159,171,172,177,178,201,202,203,205,206,213,214,215,217,218,219,230,231,244,245",
			"name": "Runs of consecutive integers with same prime signature.",
			"comment": [
				"This sequence is infinite, see A189982 and Theorem 4 in Goldston-Graham-Pintz-Yıldırım. - _Charles R Greathouse IV_, Jul 17 2015"
			],
			"link": [
				"Charles R Greathouse IV, \u003ca href=\"/A260143/b260143.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"D. A. Goldston, S. W. Graham, J. Pintz, and C. Y. Yıldırım, \u003ca href=\"http://arxiv.org/abs/0803.2636\"\u003eSmall gaps between almost primes, the parity problem, and some conjectures of Erdos on consecutive integers\u003c/a\u003e, arXiv:0803.2636 [math.NT], 2008.",
				"MathOverflow, \u003ca href=\"http://mathoverflow.net/questions/32412\"\u003eQuestion on consecutive integers with similar prime factorizations\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PrimeSignature.html\"\u003ePrime Signature\u003c/a\u003e",
				"OEIS Wiki, \u003ca href=\"http://oeis.org/wiki/Prime_signatures\"\u003ePrime signatures\u003c/a\u003e"
			],
			"example": [
				"Runs begin:",
				"(terms)         (prime signature)",
				"{2, 3},         [1]",
				"{14, 15},       [1,1]",
				"{21, 22},       [1,1]",
				"{33, 34, 35},   [1,1]",
				"{38, 39},       [1,1]",
				"{44, 45},       [1,2]",
				"{57, 58},       [1,1]",
				"{75, 76},       [1,2]",
				"{85, 86, 87},   [1,1]",
				"{93, 94, 95},   [1,1]",
				"{98, 99},       [1,2]",
				"..."
			],
			"mathematica": [
				"Split[Range[2,250], Sort[FactorInteger[#1][[All, 2]]] === Sort[FactorInteger[#2][[All, 2]]]\u0026] // Select[#, Length[#] \u003e 1\u0026]\u0026 // Flatten"
			],
			"program": [
				"(PARI) is(n)=my(f=vecsort(factor(n)[,2])); f==vecsort(factor(n-1)[,2]) || f==vecsort(factor(n+1)[,2]) \\\\ _Charles R Greathouse IV_, Jul 17 2015",
				"(Python)",
				"from sympy import factorint",
				"def aupto(limit):",
				"    aset, prevsig = {2}, [1]",
				"    for k in range(3, limit+2):",
				"        sig = sorted(factorint(k).values())",
				"        if sig == prevsig: aset.update([k - 1, k])",
				"        prevsig = sig",
				"    return sorted(aset)",
				"print(aupto(250)) # _Michael S. Branicky_, Sep 20 2021"
			],
			"xref": [
				"Main sequence is A052213."
			],
			"keyword": "nonn,easy,tabf",
			"offset": "1,1",
			"author": "_Jean-François Alcover_, Jul 17 2015",
			"references": 3,
			"revision": 20,
			"time": "2021-09-20T22:14:25-04:00",
			"created": "2015-07-20T01:43:48-04:00"
		}
	]
}