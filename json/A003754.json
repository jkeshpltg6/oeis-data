{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003754",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3754,
			"data": "0,1,2,3,5,6,7,10,11,13,14,15,21,22,23,26,27,29,30,31,42,43,45,46,47,53,54,55,58,59,61,62,63,85,86,87,90,91,93,94,95,106,107,109,110,111,117,118,119,122,123,125,126,127,170,171,173,174,175,181",
			"name": "Numbers with no adjacent 0's in binary expansion.",
			"comment": [
				"Theorem (J.-P. Allouche, J. Shallit, G. Skordev): This sequence = A052499 - 1.",
				"Ahnentafel numbers of ancestors contributing the X-chromosome to a female. A280873 gives the male inheritance. - _Floris Strijbos_, Jan 09 2017 [Equivalence with this sequence pointed out by _John Blythe Dobson_, May 09 2018]",
				"From _Gus Wiseman_, Apr 04 2020: (Start)",
				"The k-th composition in standard order (row k of A066099) is obtained by taking the set of positions of 1's in the reversed binary expansion of k, prepending 0, taking first differences, and reversing again. This gives a bijective correspondence between nonnegative integers and integer compositions. This sequence lists all numbers k such that the k-th composition in standard order has no parts greater than two. For example, the terms together with the corresponding compositions begin:",
				"    0: ()            30: (1,1,1,2)         90: (2,1,2,2)",
				"    1: (1)           31: (1,1,1,1,1)       91: (2,1,2,1,1)",
				"    2: (2)           42: (2,2,2)           93: (2,1,1,2,1)",
				"    3: (1,1)         43: (2,2,1,1)         94: (2,1,1,1,2)",
				"    5: (2,1)         45: (2,1,2,1)         95: (2,1,1,1,1,1)",
				"    6: (1,2)         46: (2,1,1,2)        106: (1,2,2,2)",
				"    7: (1,1,1)       47: (2,1,1,1,1)      107: (1,2,2,1,1)",
				"   10: (2,2)         53: (1,2,2,1)        109: (1,2,1,2,1)",
				"   11: (2,1,1)       54: (1,2,1,2)        110: (1,2,1,1,2)",
				"   13: (1,2,1)       55: (1,2,1,1,1)      111: (1,2,1,1,1,1)",
				"   14: (1,1,2)       58: (1,1,2,2)        117: (1,1,2,2,1)",
				"   15: (1,1,1,1)     59: (1,1,2,1,1)      118: (1,1,2,1,2)",
				"   21: (2,2,1)       61: (1,1,1,2,1)      119: (1,1,2,1,1,1)",
				"   22: (2,1,2)       62: (1,1,1,1,2)      122: (1,1,1,2,2)",
				"   23: (2,1,1,1)     63: (1,1,1,1,1,1)    123: (1,1,1,2,1,1)",
				"   26: (1,2,2)       85: (2,2,2,1)        125: (1,1,1,1,2,1)",
				"   27: (1,2,1,1)     86: (2,2,1,2)        126: (1,1,1,1,1,2)",
				"   29: (1,1,2,1)     87: (2,2,1,1,1)      127: (1,1,1,1,1,1,1)",
				"(End)"
			],
			"link": [
				"Indranil Ghosh, \u003ca href=\"/A003754/b003754.txt\"\u003eTable of n, a(n) for n = 1..50000\u003c/a\u003e (terms 1..1000 from T. D. Noe)",
				"J.-P. Allouche, J. Shallit and G. Skordev, \u003ca href=\"http://www.math.jussieu.fr/~allouche/kimb.ps\"\u003eSelf-generating sets, integers with missing blocks and substitutions\u003c/a\u003e, Discrete Math. 292 (2005) 1-15.",
				"David Garth and Adam Gouge, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL10/Garth/garth14.html\"\u003eAffinely Self-Generating Sets and Morphisms\u003c/a\u003e, Journal of Integer Sequences, Article 07.1.5, 10 (2007) 1-13.",
				"T. Karki, A. Lacroix, M. Rigo, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL13/Rigo/rigo6.html\"\u003eOn the recognizability of self-generating sets\u003c/a\u003e, JIS 13 (2010) #10.2.2.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Ahnentafel\"\u003eAhnentafel\u003c/a\u003e",
				"\u003ca href=\"/index/Ar#2-automatic\"\u003eIndex entries for 2-automatic sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e"
			],
			"example": [
				"21 is in the sequence because 21 = 10101_2. '10101' has no '00' present in it. - _Indranil Ghosh_, Feb 11 2017"
			],
			"maple": [
				"isA003754 := proc(n) local bdgs ; bdgs := convert(n,base,2) ; for i from 2 to nops(bdgs) do if op(i,bdgs)=0 and op(i-1,bdgs)= 0 then return false; end if; end do; return true; end proc:",
				"A003754 := proc(n) option remember; if n= 1 then 0; else for a from procname(n-1)+1 do if isA003754(a) then return a; end if; end do: end if; end proc:",
				"# _R. J. Mathar_, Oct 23 2010"
			],
			"mathematica": [
				"Select[ Range[0, 200], !MatchQ[ IntegerDigits[#, 2], {___, 0, 0, ___}]\u0026] (* _Jean-François Alcover_, Oct 25 2011 *)",
				"Select[Range[0,200],SequenceCount[IntegerDigits[#,2],{0,0}]==0\u0026] (* The program uses the SequenceCount function from Mathematica version 10 *) (* _Harvey P. Dale_, May 21 2015 *)"
			],
			"program": [
				"(Haskell)",
				"a003754 n = a003754_list !! (n-1)",
				"a003754_list = filter f [0..] where",
				"   f x = x == 0 || x `mod` 4 \u003e 0 \u0026\u0026 f (x `div` 2)",
				"-- _Reinhard Zumkeller_, Dec 07 2012, Oct 19 2011",
				"(PARI) is(n)=n=bitor(n,n\u003e\u003e1)+1; n\u003e\u003e=valuation(n,2); n==1 \\\\ _Charles R Greathouse IV_, Feb 06 2017",
				"(Python)",
				"i=0",
				"while i\u003c=500:",
				"    if \"00\" not in bin(i)[2:]:",
				"        print(str(i), end=',')",
				"    i+=1 # _Indranil Ghosh_, Feb 11 2017"
			],
			"xref": [
				"A104326(n) = A007088(a(n)); A023416(a(n)) = A087116(a(n)); A107782(a(n)) = 0; A107345(a(n)) = 1; A107359(n) = a(n+1) - a(n); A104326(n) = A007088(a(n)); a(A001911(n)) = A000225(n); a(A000071(n+2)) = A000975(n). - _Reinhard Zumkeller_, May 25 2005",
				"Cf. A003796 (no 000), A004745 (no 001), A004746 (no 010), A004744 (no 011), A004742 (no 101), A004743 (no 110), A003726 (no 111).",
				"Complement of A004753.",
				"Cf. A023705, A196168, A280873.",
				"All of the following pertain to compositions in standard order (A066099):",
				"- The length is A000120.",
				"- Compositions without ones are ranked by A022340.",
				"- The sum is A070939.",
				"- Compositions with no twos are ranked by A175054.",
				"- The reverse is A228351.",
				"- Constant compositions are ranked by A272919.",
				"- Normal compositions are ranked by A333217.",
				"- Anti-runs are counted by A333381.",
				"- Anti-runs are ranked by A333489.",
				"- Runs-resistance is A333628.",
				"Cf. A124767, A318928, A329745, A329767."
			],
			"keyword": "nonn,easy,base,nice",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Removed \"2\" from the name, because, for example, one could argue that 10001 has 3 adjacent zeros, not 2. - _Gus Wiseman_, Apr 04 2020"
			],
			"references": 39,
			"revision": 71,
			"time": "2020-04-10T15:03:18-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}