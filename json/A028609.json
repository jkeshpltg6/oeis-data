{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A028609",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 28609,
			"data": "1,2,0,4,2,4,0,0,0,6,0,2,4,0,0,8,2,0,0,0,4,0,0,4,0,6,0,8,0,0,0,4,0,4,0,0,6,4,0,0,0,0,0,0,2,12,0,4,4,2,0,0,0,4,0,4,0,0,0,4,8,0,0,0,2,0,0,4,0,8,0,4,0,0,0,12,0,0,0,0,4,10,0,0,0,0,0,0,0,4,0,0,4,8,0,0,0,4,0,6,6,0,0",
			"name": "Expansion of (theta_3(z)*theta_3(11z) + theta_2(z)*theta_2(11z)).",
			"comment": [
				"Theta series of lattice with Gram matrix [2, 1; 1, 6].",
				"Number of integer solutions (x, y) to x^2 + x*y + 3*y^2 = n. - _Michael Somos_, Sep 20 2004",
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"reference": [
				"H. McKean and V. Moll, Elliptic Curves, Cambridge University Press, 1997, page 202. MR1471703 (98g:14032)"
			],
			"link": [
				"John Cannon, \u003ca href=\"/A028609/b028609.txt\"\u003eTable of n, a(n) for n = 0..5000\u003c/a\u003e",
				"N. J. A. Sloane et al., \u003ca href=\"https://oeis.org/wiki/Binary_Quadratic_Forms_and_OEIS\"\u003eBinary Quadratic Forms and OEIS\u003c/a\u003e (Index to related sequences, programs, references)",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of phi(x) * phi(x^11) = 4 * x^3 * psi(x^2) * psi(x^22) in powers of x where phi(), psi() are Ramanujan theta functions. - _Michael Somos_, Apr 21 2015",
				"Moebius transform is period 11 sequence [ 2, -2, 2, 2, 2, -2, -2, -2, 2, -2, 0, ...]. - _Michael Somos_, Jan 29 2007",
				"a(n) = 2 * b(n) and b(n) is multiplicative with b(11^e) = 1, b(p^e) = (1 + (-1)^e) / 2 if p == 2, 6, 7, 8, 10 (mod 11), b(p^e) = e + 1 if p == 1, 3, 4, 5, 9 (mod 11). - _Michael Somos_, Jan 29 2007",
				"G.f.: 1 + 2 * Sum_{k\u003e0} Kronecker( -11, k) * x^k / (1 - x^k). - _Michael Somos_, Jan 29 2007",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (11 t)) = 11^(1/2) (t/i) f(t) where q = exp(2 Pi i t). - _Michael Somos_, Jun 05 2007",
				"Expansion of (F(x)^2 + 4 * F(x^2)^2 + 8 * F(x^4)^2) / F(x^2) in powers of x or expansion of (F(x)^2 + 2 * F(x^2)^2 + 2 * F(x^4)^2) / F(x^2) in powers of x^4 where F(x) = x^(1/2) * f(-x) * f(-x^11) and f() is a Ramanujan theta function. - _Michael Somos_, Mar 01 2010",
				"a(n) = 2 * A035179(n) unless n=0. Convolution square is A028610."
			],
			"example": [
				"G.f. = 1 + 2*x + 4*x^3 + 2*x^4 + 4*x^5 + 6*x^9 + 2*x^11 + 4*x^12 + 8*x^15 + ...",
				"Theta series of lattice with Gram matrix [2, 1; 1, 6] = 1 + 2*q^2 + 4*q^6 + 2*q^8 + 4*q^10 + 6*q^18 + 2*q^22 + 4*q^24 + 8*q^30 + 2*q^32 + 4*q^40 + 4*q^46 + 6*q^50 + 8*q^54 + 4*q^62 + 4*q^66 + 6*q^72 + 4*q^74 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 1, Boole[ n == 0], DivisorSum[ n, KroneckerSymbol[ -11, #] \u0026] 2]; (* _Michael Somos_, Jul 12 2014 *)",
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 3, 0, q] EllipticTheta[ 3, 0, q^11] + EllipticTheta[ 2, 0, q] EllipticTheta[ 2, 0, q^11], {q, 0, n}]; (* _Michael Somos_, Jul 12 2014 *)"
			],
			"program": [
				"(PARI) {a(n) = my(t); if( n\u003c1, n==0, 2 * issquare(n) + 2 * sum( y=1, sqrtint(n * 4\\11), 2 * issquare( t=4*n - 11*y^2) - (t==0)))}; /* _Michael Somos_, Sep 20 2004 */",
				"(PARI) {a(n) = if( n\u003c0, 0, polcoeff( 1 + 2 * x * Ser(qfrep( [ 2, 1; 1, 6], n, 1)), n))}; /* _Michael Somos_, Apr 21 2015 */",
				"(PARI) {a(n) = if( n\u003c1, n==0, direuler( p=2, n, 1 / (1 - X) / (1 - kronecker( -11, p) * X))[n] * 2)}; /* _Michael Somos_, Jun 05 2005 */",
				"(PARI) {a(n) = if( n\u003c1, n==0, 2 * sumdiv( n, d, kronecker( -11, d)))}; /* _Michael Somos_, Jan 29 2007 */",
				"(MAGMA) A := Basis( ModularForms( Gamma1(11), 1), 103); A[1] + 2*A[2] + 4*A[4] + 2*A[5]; /* _Michael Somos_, Jul 12 2014 */"
			],
			"xref": [
				"Cf. A028610, A035179, A028954, A056874.",
				"Number of integer solutions to f(x,y) = n where f(x,y) is the principal binary quadratic form with discriminant d: A004016 (d=-3), A004018 (d=-4), A002652 (d=-7), A033715 (d=-8), this sequence (d=-11), A028641 (d=-19), A138811 (d=-43)."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 15,
			"revision": 39,
			"time": "2021-03-12T22:24:41-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}