{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A065463",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 65463,
			"data": "7,0,4,4,4,2,2,0,0,9,9,9,1,6,5,5,9,2,7,3,6,6,0,3,3,5,0,3,2,6,6,3,7,2,1,0,1,8,8,5,8,6,4,3,1,4,1,7,0,9,8,0,4,9,4,1,4,2,2,6,8,4,2,5,9,1,0,9,7,0,5,6,6,8,2,0,0,6,7,7,8,5,3,6,8,0,8,2,4,4,1,4,5,6,9,3,1,3",
			"name": "Decimal expansion of Product_{p prime} (1 - 1/(p*(p+1))).",
			"comment": [
				"The density of A268335. - _Vladimir Shevelev_, Feb 01 2016",
				"The probability that two numbers are coprime given that one of them is coprime to a randomly chosen third number. - _Luke Palmer_, Apr 27 2019"
			],
			"link": [
				"Olivier Bordellès and Benoit Cloitre, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL16/Bordelles/bord14.html\"\u003eAn Alternating Sum Involving the Reciprocal of Certain Multiplicative Functions\u003c/a\u003e, J. Int. Seq., Vol. 16 (2013), Article 13.6.3.",
				"Eckford Cohen, \u003ca href=\"https://doi.org/10.1007/BF01180473\"\u003eArithmetical functions associated with the unitary divisors of an integer\u003c/a\u003e, Mathematische Zeitschrift, Vol. 74, No. 1 (1960), pp. 66-80.",
				"Steven R. Finch, \u003ca href=\"https://doi.org/10.1017/9781316997741\"\u003eMathematical Constants II\u003c/a\u003e, Encyclopedia of Mathematics and Its Applications, Cambridge University Press, Cambridge, 2018, p. 50.",
				"David Handelman, \u003ca href=\"http://arxiv.org/abs/1309.7417\"\u003eInvariants for critical dimension groups and permutation-Hermite equivalence\u003c/a\u003e, arXiv preprint arXiv:1309.7417 [math.AC], 2013-2017.",
				"R. J. Mathar, \u003ca href=\"http://arxiv.org/abs/0903.2514\"\u003eHardy-Littlewood constants embedded into infinite products over all positive integers\u003c/a\u003e, arxiv:0903.2514 [math.NT] (2009) constant Q_1^(1).",
				"G. Niklasch, \u003ca href=\"http://guests.mpim-bonn.mpg.de/moree/Moree.en.html\"\u003eSome number theoretical constants: 1000-digit values\u003c/a\u003e.",
				"G. Niklasch, \u003ca href=\"/A001692/a001692.html\"\u003eSome number theoretical constants: 1000-digit values\u003c/a\u003e. [Cached copy]",
				"V. Sita Ramaiah and D. Suryanarayana, \u003ca href=\"http://ousar.lib.okayama-u.ac.jp/en/33820\"\u003eSums of reciprocals of some multiplicative functions\u003c/a\u003e, Mathematical Journal of Okayama University, Vol. 21, No. 2 (1979), pp. 155-164.",
				"R. Sitaramachandrarao and D. Suryanarayana, \u003ca href=\"https://doi.org/10.1090/S0002-9939-1973-0319922-9\"\u003eOn Sigma_{n\u003c=x} sigma*(n) and Sigma_{n\u003c=x} phi*(n)\u003c/a\u003e, Proceedings of the American Mathematical Society, Vol. 41, No. 1 (1973), pp. 61-66.",
				"László Tóth, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL20/Toth/toth25.html\"\u003eAlternating sums concerning multiplicative arithmetic functions\u003c/a\u003e, Journal of Integer Sequences, Vol. 20 (2017), Article 17.2.1,\u003ca href=\"https://arxiv.org/abs/1608.00795\"\u003earXiv preprint\u003c/a\u003e, arXiv:1608.00795 [math.NT], 2016.",
				"Deyu Zhang and Wenguang Zhai, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL13/Zhang/zhang10.html\"\u003eMean Values of a Gcd-Sum Function Over Regular Integers Modulo n\u003c/a\u003e, J. Int. Seq., Vol. 13 (2010), Article 10.4.7, eq. (4).",
				"Rimer Zurita \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL23/Zurita/zur3.html\"\u003eGeneralized Alternating Sums of Multiplicative Arithmetic Functions\u003c/a\u003e, J. Int. Seq., Vol. 23 (2020), Article 20.10.4."
			],
			"formula": [
				"From _Amiram Eldar_, Mar 05 2019: (Start)",
				"Equals lim_{m-\u003eoo} (2/m^2)*Sum_{k=1..m} rad(k), where rad(k) = A007947(k) is the squarefree kernel of k (Cohen).",
				"Equals lim_{m-\u003eoo} (2/m^2)*Sum_{k=1..m} uphi(k), where uphi(k) = A047994(k) is the unitary totient function (Sitaramachandrarao and Suryanarayana).",
				"Equals lim_{m-\u003eoo} (1/log(m))*Sum_{k=1..m} 1/psi(k), where psi(k) = A001615(k) is the Dedekind psi function (Sita Ramaiah and Suryanarayana).",
				"(End)",
				"Equals A065473*A013661/A065480. - _Luke Palmer_, Apr 27 2019",
				"Equals Sum_{k\u003e=1} mu(k)/(k*sigma(k)), where mu is the Möbius function (A008683) and sigma(k) is the sum of divisors of k (A000203). - _Amiram Eldar_, Jan 14 2022"
			],
			"example": [
				"0.7044422009991655927366033503..."
			],
			"mathematica": [
				"$MaxExtraPrecision = 1200; digits = 98; terms = 1200; P[n_] := PrimeZetaP[n]; LR = Join[{0, 0}, LinearRecurrence[{-2, 0, 1}, {-2, 3, -6}, terms + 10]]; r[n_Integer] := LR[[n]]; Exp[NSum[r[n]*P[n - 1]/(n - 1), {n, 3, terms}, NSumTerms -\u003e terms, WorkingPrecision -\u003e digits + 10]] // RealDigits[#, 10, digits]\u0026 // First (* _Jean-François Alcover_, Apr 18 2016 *)"
			],
			"program": [
				"(PARI) prodeulerrat(1 - 1/(p*(p+1))) \\\\ _Amiram Eldar_, Mar 14 2021"
			],
			"xref": [
				"Cf. A001615, A007947, A047994, A078082, A268335, A306633.",
				"Cf. A000203, A065473, A065480, A065490, A008683."
			],
			"keyword": "cons,nonn,changed",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_, Nov 19 2001",
			"references": 24,
			"revision": 74,
			"time": "2022-01-14T07:31:51-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}