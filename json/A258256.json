{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A258256",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 258256,
			"data": "1,1,1,0,1,2,0,0,1,4,2,0,0,2,0,0,1,2,4,0,2,0,0,0,0,3,2,0,0,2,0,0,1,0,2,0,4,2,0,0,2,2,0,0,0,8,0,0,0,1,3,0,2,2,0,0,0,0,2,0,0,2,0,0,1,4,0,0,2,0,0,0,4,2,2,0,0,0,0,0,2,4,2,0,0,4,0",
			"name": "Expansion of f(q^3) * psi(-q^3)^3 / (psi(-q) * psi(-q^9)) in powers of q where psi(), f() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A258256/b258256.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of eta(q^2) * eta(q^3)^2 * eta(q^12)^2 * eta(q^18) / (eta(q) * eta(q^4) * eta(q^9) * eta(q^36)) in powers of q.",
				"Euler transform of period 36 sequence [1, 0, -1, 1, 1, -2, 1, 1, 0, 0, 1, -3, 1, 0, -1, 1, 1, -2, 1, 1, -1, 0, 1, -3, 1, 0, 0, 1, 1, -2, 1, 1, -1, 0, 1, -2, ...].",
				"Moebius transform is period 36 sequence [1, 0, -1, 0, 1, 0, -1, 0, 4, 0, -1, 0, 1, 0, -1, 0, 1, 0, -1, 0, 1, 0, -1, 0, 1, 0, -4, 0, 1, 0, -1, 0, 1, 0, -1, 0, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (36 t)) = 6 (t/i) f(t) where q = exp(2 Pi i t).",
				"a(2*n) = a(n). a(3*n + 1) = A122865(n). a(3*n + 2) = A122856(n). a(4*n + 3) = 0. a(12*n + 1) = A002175. a(12*n + 5) = 2 * A121444(n).",
				"a(n) = Sum_{d|n} A258260(d) * (-1)^(n+d) if n\u003e0.",
				"a(n) = (-1)^n * A256282(n). - _Michael Somos_, Jun 06 2015",
				"a(n) is multiplicative with a(0) = 1, a(2^e) = 1, a(3^e) = 2*(1 + (-1)^e), a(p^e) = (1 + (-1)^e) / 2 if p == 3 (mod 4), a(p^e) = e+1 if p == 1 (mod 4). - _Michael Somos_, Jun 06 2015",
				"Expansion of A0(x)^2 + A0(x)*A1(x) + A1(x)^2 in powers of x where A0(x) = phi(x^9), A1(x) = x * f(x^3, x^15) = x * A089801(x^3). - _Michael Somos_, Jun 23 2018"
			],
			"example": [
				"G.f. = 1 + q + q^2 + q^4 + 2*q^5 + q^8 + 4*q^9 + 2*q^10 + 2*q^13 + q^16 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 1, Boole[n == 0], DivisorSum[ n, {1, 2, -1, 0}[[Mod[#, 4, 1]]] If[ Divisible[ #, 9], 4, 1] (-1)^(Boole[Mod[#, 8] == 6] + n + #) \u0026]];",
				"a[ n_] := If[ n \u003c 2, Boole[n \u003e= 0], Times @@ (Which[ # == 2, 1, Mod[#, 4] == 1, #2 + 1, True, If[# == 3, 4, 1] Mod[#2 + 1, 2]] \u0026 @@@ FactorInteger[n])];",
				"a[ n_] := SeriesCoefficient[ q^(1/8) QPochhammer[ -q^3] EllipticTheta[ 2, Pi/4, q^(3/2)]^3 / (Sqrt[2] EllipticTheta[ 2, Pi/4, q^(1/2)] EllipticTheta[ 2, Pi/4, q^(9/2)]), {q, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, n==0, sumdiv(n, d, [0, 1, 2, -1][d%4 + 1] * if(d%9, 1, 4) * (-1)^((d%8==6) + n+d)))};",
				"(PARI) {a(n) = my(A, p, e); if( n\u003c1, n==0, A = factor(n); prod( k=1, matsize(A)[1], [p, e] = A[k,]; if( p==2, 1, p%4==1, e+1, if( p==3, 4, 1) * (1 - e%2) )))};",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A) * eta(x^3 + A)^2 * eta(x^12 + A)^2 * eta(x^18 + A) / (eta(x + A) * eta(x^4 + A) * eta(x^9 + A) * eta(x^36 + A)), n))};",
				"(MAGMA) A := Basis( ModularForms( Gamma1(36), 1), 87); A[1] + A[2] + A[3] + A[5] + 2*A[6] + A[9] + 4*A[10] + 2*A[11] + 2*A[14] + A[17] + 2*A[18] + 4*A[19];"
			],
			"xref": [
				"Cf. A002175, A089801, A121444, A122856, A122856, A256282, A258260."
			],
			"keyword": "nonn,mult",
			"offset": "0,6",
			"author": "_Michael Somos_, May 24 2015",
			"references": 7,
			"revision": 21,
			"time": "2021-03-12T22:24:48-05:00",
			"created": "2015-05-24T13:58:55-04:00"
		}
	]
}