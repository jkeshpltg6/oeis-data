{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A258747",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 258747,
			"data": "1,-1,0,0,0,-1,-2,2,1,0,0,2,0,0,-2,0,1,0,0,0,0,-1,-2,0,2,-2,0,2,0,-2,0,0,2,-1,0,0,0,0,0,2,3,0,0,0,0,-2,-2,0,0,0,0,0,0,0,-2,2,1,-2,0,2,0,0,-4,0,2,-1,0,0,0,0,-2,2,0,0,0,2,0,0,0,0,2",
			"name": "Expansion of chi(-x) * f(x^3) * f(-x^6) in powers of x where chi(), f() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A258747/b258747.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(-1/3) * eta(q) * eta(q^6)^4 / (eta(q^2) * eta(q^3) * eta(q^12)) in powers of q.",
				"Euler transform of period 12 sequence [ -1, 0, 0, 0, -1, -3, -1, 0, 0, 0, -1, -2, ...].",
				"G.f.: Product_{k\u003e0} (1 + x^(3*k)) * (1 - x^(6*k))^2 / ( (1 + x^k) * (1 + x^(6*k)) ).",
				"-2 * a(n) = A082564(3*n + 1). a(n) = A129134(3*n + 1).",
				"a(4*n + 3) = 2 * A257402(n-1). a(8*n) = A257398(n). a(8*n + 2) = a(8*n + 4) = a(16*n + 3) = a(16*n + 15) = 0. a(16*n + 7) = 2 * A255318(n). a(16*n + 11) = 2 * A255319(n)."
			],
			"example": [
				"G.f. = 1 - x - x^5 - 2*x^6 + 2*x^7 + x^8 + 2*x^11 - 2*x^14 + x^16 - x^21 + ...",
				"G.f. = q - q^4 - q^16 - 2*q^19 + 2*q^22 + q^25 + 2*q^34 - 2*q^43 + q^49 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ x, x^2] QPochhammer[ -x^3] QPochhammer[ x^6], {x, 0, n}];",
				"a[ n_] := If[ n \u003c 0, 0, (-1)^Quotient[ 3 n, 2] DivisorSum[ 3 n + 1, KroneckerSymbol[-2, #] \u0026]]];"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A) * eta(x^6 + A)^4 / (eta(x^2 + A) * eta(x^3 + A) * eta(x^12 + A)), n))};",
				"(PARI) {a(n) = if( n\u003c0, 0, (-1)^(3*n\\2) * sumdiv(3*n + 1, d, kronecker( -2, d)))};"
			],
			"xref": [
				"Cf. A082564, A129134, A255318, A255319, A257398."
			],
			"keyword": "sign",
			"offset": "0,7",
			"author": "_Michael Somos_, Jun 09 2015",
			"references": 3,
			"revision": 10,
			"time": "2021-03-12T22:24:48-05:00",
			"created": "2015-06-09T02:55:43-04:00"
		}
	]
}