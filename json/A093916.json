{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A093916",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 93916,
			"data": "2,7,9,24,24,53,47,94,78,147,117,212,164,289,219,378,282,479,353,592,432,717,519,854,614,1003,717,1164,828,1337,947,1522,1074,1719,1209,1928,1352,2149,1503,2382,1662,2627,1829,2884,2004,3153,2187,3434,2378,3727,2577,4032,2784,4349,2999,4678,3222,5019,3453,5372",
			"name": "a(2*k-1) = (2*k-1)^2 + 2 - k, a(2*k) = 6*k^2 + 2 - k: First column of the triangle A093915.",
			"comment": [
				"The sequence was initially defined as the first column of the triangle A093915, constructed by trial and error. It is however easy to prove that the sum of the r-th row of A093915, A093917(r), equals twice A006003(r) when r is odd, and three times A006003(r) when r is even. Given the expression of the row sum A093917(r) in terms of the first element a(r), one obtains the explicit formula for a(r). - _M. F. Hasler_, Apr 04 2009"
			],
			"link": [
				"Harvey P. Dale, \u003ca href=\"/A093916/b093916.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,3,0,-3,0,1)."
			],
			"formula": [
				"a(n) = ((n^2+1)*b(n) - n + 1)/2 where b(n) = 3 - (n mod 2) = 2 if n odd, = 3 if n even. - _M. F. Hasler_, Apr 04 2009",
				"From _Colin Barker_, May 01 2012: (Start)",
				"a(n) = (n*(5*n-2) + (n^2+1)*(-1)^n + 7)/4.",
				"a(n) = 3*a(n-2) - 3*a(n-4) + a(n-6).",
				"G.f.: x*(2+7*x+3*x^2+3*x^3+3*x^4+2*x^5)/((1-x)^3*(1+x)^3). (End)",
				"E.g.f.: (1/4)*( (7 +3*x +5*x^2)*exp(x) - 8 + (1 -x +x^2)*exp(-x) ). - _G. C. Greubel_, Dec 30 2021"
			],
			"mathematica": [
				"LinearRecurrence[{0,3,0,-3,0,1},{2,7,9,24,24,53},80] (* _Harvey P. Dale_, Nov 24 2017 *)"
			],
			"program": [
				"(PARI code by _M. F. Hasler_, Apr 04 2009)",
				"A093916(n)=((n^2+1)*(3-n%2)-n+1)/2",
				"/* or the \"experimental\" version, trying out all allowed values */",
				"A093916(n)={ local( s=(n^3+n)/2, d=(n^2-n)/2, k=ceil((2*s-d)/n)); while( (n*k+d)%s, k++ ); k }",
				"(MAGMA) [(n*(5*n-2) + (-1)^n*(n^2+1) + 7)/4: n in [1..70]]; // _G. C. Greubel_, Dec 30 2021",
				"(Sage) [(5*n^2 -2*n +7 +(-1)^n*(n^2 +1))/4 for n in (1..70)] # _G. C. Greubel_, Dec 30 2021"
			],
			"xref": [
				"Cf. A093915, A093917, A093918."
			],
			"keyword": "nonn,easy,changed",
			"offset": "1,1",
			"author": "_Amarnath Murthy_, Apr 25 2004",
			"ext": [
				"Edited and extended by _M. F. Hasler_, Apr 04 2009"
			],
			"references": 4,
			"revision": 26,
			"time": "2022-01-02T21:19:04-05:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}