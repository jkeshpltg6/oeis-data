{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A082491",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 82491,
			"data": "1,0,2,12,216,5280,190800,9344160,598066560,48443028480,4844306476800,586161043776000,84407190782745600,14264815236056985600,2795903786354347468800,629078351928420506112000,161044058093696572354560000,46541732789077953723039744000",
			"name": "a(n) = n! * d(n), where n! = factorial numbers (A000142), d(n) = subfactorial numbers (A000166).",
			"comment": [
				"a(n) is also the number of pairs of n-permutations p and q such that p(x)\u003c\u003eq(x) for each x in { 1, 2, ..., n }.",
				"Or number of n X n matrices with exactly one 1 and one 2 in each row and column, other entries 0 (cf. A001499). - _Vladimir Shevelev_, Mar 22 2010",
				"a(n) is approximately equal to (n!)^2/e. - _J. M. Bergot_, Jun 09 2018"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A082491/b082491.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"Ira Gessel, \u003ca href=\"http://www.mat.univie.ac.at/~slc/opapers/s17gessel.html\"\u003eEnumerative applications of symmetric functions\u003c/a\u003e, Séminaire Lotharingien de Combinatoire, B17a (1987), 17 pp.",
				"Shawn L. Witte, \u003ca href=\"https://www.math.ucdavis.edu/~tdenena/dissertations/201910_Witte_Dissertation.pdf\"\u003eLink Nomenclature, Random Grid Diagrams, and Markov Chain Methods in Knot Theory\u003c/a\u003e, Ph. D. Dissertation, University of California-Davis (2020)."
			],
			"formula": [
				"a(n) = n! * d(n) where d(n) = A000166(n).",
				"a(n) = Sum_{k=0..n} binomial(n, k)^2 * (-1)^k * (n - k)!^2 * k!.",
				"a(n+2) = (n+2)*(n+1) * ( a(n+1) + (n+1)*a(n) ).",
				"a(n) ~ 2*Pi*n^(2*n+1)*exp(-2*n-1). - _Ilya Gutkovskiy_, Dec 04 2016"
			],
			"maple": [
				"with (combstruct):a:=proc(m) [ZL, {ZL=Set(Cycle(Z, card\u003e=m))}, labeled]; end: ZLL:=a(2):seq(count(ZLL, size=n)*n!, n=0..15); # _Zerinvary Lajos_, Jun 11 2008"
			],
			"mathematica": [
				"Table[Subfactorial[n]*n!, {n, 0, 15}] (* _Zerinvary Lajos_, Jul 10 2009 *)"
			],
			"program": [
				"(Maxima) A000166[0]:1$",
				"A000166[n]:=n*A000166[n-1]+(-1)^n$",
				"  makelist(n!*A000166[n], n, 0, 12); /* _Emanuele Munarini_, Mar 01 2011 */",
				"(PARI)",
				"d(n)=if(n\u003c1, n==0, n*d(n-1)+(-1)^n);",
				"a(n)=d(n)*n!;",
				"vector(33,n,a(n-1))",
				"/* _Joerg Arndt_, May 28 2012 */",
				"(PARI) {a(n) = if( n\u003c2, n==0, n! * round(n! / exp(1)))}; /* _Michael Somos_, Jun 24 2018 */",
				"(Python)",
				"A082491_list, m, x = [], 1, 1",
				"for n in range(10*2):",
				"....x, m = x*n**2 + m, -(n+1)*m",
				"....A082491_list.append(x) # _Chai Wah Wu_, Nov 03 2014",
				"(Scala)",
				"val A082491_pairs: LazyList[BigInt \u0026\u0026 BigInt] =",
				"  (BigInt(0), BigInt(1)) #::",
				"  (BigInt(1), BigInt(0)) #::",
				"  lift2 {",
				"    case ((n, z), (_, y)) =\u003e",
				"      (n+2, (n+2)*(n+1)*((n+1)*z+y))",
				"  } (A082491_pairs, A082491_pairs.tail)",
				"val A082491: LazyList[BigInt] =",
				"  lift1(_._2)(A082491_pairs)",
				"/** _Luc Duponcheel_, Jan 25 2020 */"
			],
			"xref": [
				"Cf. A000142, A000166."
			],
			"keyword": "easy,nonn",
			"offset": "0,3",
			"author": "_Emanuele Munarini_, Apr 28 2003",
			"references": 10,
			"revision": 64,
			"time": "2020-05-09T04:45:29-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}