{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A055210",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 55210,
			"data": "1,1,1,3,1,1,1,3,7,1,1,3,1,1,1,11,1,7,1,3,1,1,1,3,21,1,7,3,1,1,1,11,1,1,1,21,1,1,1,3,1,1,1,3,7,1,1,11,43,21,1,3,1,7,1,3,1,1,1,3,1,1,7,43,1,1,1,3,1,1,1,21,1,1,21,3,1,1,1,11,61,1,1,3,1,1,1,3,1,7,1,3,1,1,1,11,1",
			"name": "Sum of totients of square divisors of n.",
			"link": [
				"Antti Karttunen, \u003ca href=\"/A055210/b055210.txt\"\u003eTable of n, a(n) for n = 1..16384\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Sum_{d is square and divides n} phi(d).",
				"Multiplicative with a(p^e) = (p^(e+1)+1)/(p+1) for even e and a(p^e) = (p^e+1)/(p+1) for odd e. - _Vladeta Jovovic_, Dec 01 2001",
				"a(n) = Sum_{d|n} A010052(d)*A000010(d). - _Antti Karttunen_, Nov 18 2017",
				"Conjecture: a(n) = sigma_2(n/core(n))/sigma_1(n/core(n)) = A001157(A008833(n))/A000203(A008833(n)) for all n \u003e 0. - _Velin Yanev_, Oct 13 2019",
				"G.f.: Sum_{k\u003e=1} k * phi(k) * x^(k^2) / (1 - x^(k^2)). - _Ilya Gutkovskiy_, Aug 20 2021"
			],
			"example": [
				"n = 400: its square divisors are {1, 4, 16, 25, 100, 400}, their totients are {1, 2, 8, 20, 40, 160} and the totient-sum over these divisors is, so a(400) = 231. This value arises at special squarefree multiples of 400 (400 times 2, 3, 5, 6, 7, 10, 11, 13, 15, 17, 19, 21, 22, 23 etc).",
				"a(400) = a(2^4*5^2) = (2^5 + 1)/3*(5^3 + 1)/6 = 231."
			],
			"mathematica": [
				"Array[DivisorSum[#, EulerPhi, IntegerQ@ Sqrt@ # \u0026] \u0026, 97] (* _Michael De Vlieger_, Nov 18 2017 *)",
				"f[p_, e_] := If[EvenQ[e], (p^(e + 1) + 1)/(p + 1), (p^e + 1)/(p + 1)]; a[n_] := Times @@ (f @@@ FactorInteger[n]); Array[a, 100] (* _Amiram Eldar_, Dec 09 2020 *)"
			],
			"program": [
				"(PARI) a(n) = sumdiv(n, d, eulerphi(d)*issquare(d)); \\\\ _Michel Marcus_, Dec 31 2013",
				"(MAGMA) [\u0026+[EulerPhi(d):d in Divisors(n)| IsSquare(d)]: n in [1..100]]; // _Marius A. Burtea_, Oct 14 2019"
			],
			"xref": [
				"Cf. A000010, A010052."
			],
			"keyword": "nonn,mult",
			"offset": "1,4",
			"author": "_Labos Elemer_, Jun 19 2000",
			"references": 2,
			"revision": 32,
			"time": "2021-08-20T09:02:10-04:00",
			"created": "2000-07-22T03:00:00-04:00"
		}
	]
}