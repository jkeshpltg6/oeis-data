{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A178830",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 178830,
			"data": "0,0,1,0,1,1,1,1,1,3,1,2,1,3,3,3,3,1,4,4,3,2,2,4,3,5,3,2,4,5,4,5,6,1,4,2,5,4,7,4,4,3,3,6,14,3,4,10,6,3,6,4,4,4,8,7,6,8,7,10,5,11,8,5,11,4,7,7,5,8,12,5,6,9,8,11,8,5,8,9,8,10,8,12,7,11,8,7,7,8,6,7,8,11,8,16,9,12,13,8",
			"name": "Number of distinct partitions of {1,...,n} into two nonempty sets P and S with the product of the elements of P equal to the sum of elements in S.",
			"comment": [
				"That the terms are nonzero for n\u003e4 is shown by the fact that for n odd, P={1,(n-1)/2,n-1} works, and for n even, P={1,(n-2)/2,n} works.",
				"a(A207852(n)) = n and a(m) \u003c\u003e n for m \u003c A207852(n). - _Reinhard Zumkeller_, Feb 21 2012"
			],
			"reference": [
				"Problem 2826, Crux Mathematicorum, May 2004, 178-179"
			],
			"link": [
				"Reinhard Zumkeller and Alois P. Heinz, \u003ca href=\"/A178830/b178830.txt\"\u003eTable of n, a(n) for n = 1..1800\u003c/a\u003e, (first 300 terms from Reinhard Zumkeller)"
			],
			"example": [
				"{1,2,3} can be partitioned into P={3} and S={1,2} with 3=1+2.  This is the only such partition, so a(3)=1.",
				"{1,2,3,4,5} can be partitioned into P={1,2,4} and S={3,5} with 1*2*4=3+5.  This is the only such partition, so a(5)=1.",
				"{1,...,10} can be partitioned into subsets P={1,2,3,7} and S={4,5,6,8,9,10} since 1*2*3*7=4+5+6+8+9+10. P can also be taken as P={6,7} or P={1,4,10}, and so there are 3 distinct ways to partition {1,...,10}, and so a(10)=3."
			],
			"maple": [
				"b:= proc(n, s, p)",
				"      `if`(s=p, 1, `if`(n\u003c1, 0, b(n-1, s, p)+",
				"      `if`(s-n\u003cp*n, 0, b(n-1, s-n, p*n))))",
				"    end:",
				"a:= n-\u003e `if`(n=1, 0, b(n, n*(n+1)/2, 1)):",
				"seq(a(n), n=1..100);  # _Alois P. Heinz_, Jun 07 2012"
			],
			"mathematica": [
				"b[_, s_, s_] = 1; b[n_ /; n \u003c 1, _, _] = 0; b[n_, s_, p_] := b[n, s, p] = b[n-1, s, p] + If[s-n \u003c p*n, 0, b[n-1, s-n, p*n]]; a[1] = 0; a[n_] := a[n] = b[n, n*(n+1)/2, 1]; Table[Print[a[n]]; a[n], {n, 1, 100}] (* _Jean-François Alcover_, Aug 16 2013, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(PARI) maxprodset(n)=ii=1;while(ii!+ii*(ii+1)/2\u003cn*(n+1)/2,ii=ii+1);return(ii-1);",
				"{for(jj=2,100,",
				"   countK=0;",
				"   S=vector(jj,ii,ii);",
				"   for(subsize=1,maxprodset(jj),",
				"      forvec(v=vector(subsize,k,[1,#S]),",
				"         vs=vecextract(S,v);",
				"         summ=jj*(jj+1)/2-sum(jk=1,#vs,vs[jk]);",
				"         prodd=prod(jk=1,#vs,vs[jk]);",
				"         if(summ==prodd,countK=countK+1),",
				"      2));",
				"   print1(countK,\",\");",
				")",
				"}",
				"(Haskell)",
				"a178830 1 = 0",
				"a178830 n = z [1..n] (n * (n + 1) `div` 2) 1 where",
				"   z []     s p             = fromEnum (s == p)",
				"   z (x:xs) s p | s \u003e p     = z xs (s - x) (p * x) + z xs s p",
				"                | otherwise = fromEnum (s == p)",
				"-- _Reinhard Zumkeller_, Feb 21 2012"
			],
			"keyword": "nonn,nice",
			"offset": "1,10",
			"author": "_Matthew Conroy_, Dec 31 2010",
			"references": 5,
			"revision": 46,
			"time": "2018-01-08T02:37:06-05:00",
			"created": "2010-11-12T14:27:46-05:00"
		}
	]
}