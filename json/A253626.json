{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A253626",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 253626,
			"data": "1,1,3,1,3,0,3,2,3,1,0,0,3,2,6,0,3,0,3,2,0,2,0,0,3,1,6,1,6,0,0,2,3,0,0,0,3,2,6,2,0,0,6,2,0,0,0,0,3,3,3,0,6,0,3,0,6,2,0,0,0,2,6,2,3,0,0,2,0,0,0,0,3,2,6,1,6,0,6,2,0,1,0,0,6,0,6",
			"name": "Expansion of psi(q^2) * f(q, q^2)^2 / f(q, q^5) in powers of q where psi(), f() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Cubic AGM theta functions: a(q) (see A004016), b(q) (A005928), c(q) (A005882)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A253626/b253626.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of psi(q^2)^2 * phi(-q^3)^2 / (psi(-q) * psi(-q^3)) = f(q) * f(q^3) * (chi(-q^3) / chi(-q^2))^4 in powers of q where phi(), psi(), chi(), f() are Ramanujan theta functions.",
				"Expansion of (a(q) + 3*a(q^2) + 2*a(q^4)) / 6 = b(q^4) * (-b(q) + 4*b(q^4)) / (3*b(q^2)) in powers of q where a(), b() are cubic AGM theta functions.",
				"Expansion of eta(q^3)^3 * eta(q^4)^3 / ( eta(q) * eta(q^2) * eta(q^6) * eta(q^12) ) in powers of q.",
				"Euler transform of period 12 sequence [ 1, 2, -2, -1, 1, 0, 1, -1, -2, 2, 1, -2, ...].",
				"Moebius transform is period 12 sequence [ 1, 2, 0, 0, -1, 0, 1, 0, 0, -2, -1, 0, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (12 t)) = 12^(1/2) (t/i) f(t) where q = exp(2 Pi i t).",
				"a(n) is multiplicative with a(0) = 1, a(2^e) = 3 if e\u003e0, a(3^e) = 1, a(p^e) = e+1 if p == 1 (mod 6), a(p^e) == (1 + (-1)^e)/2 if p == 5 (mod 6).",
				"G.f.: 1 + Sum_{k\u003e0} (3 - mod(k,2)*2) * (q^k + q^(3*k)) / (1 + q^(2*k) + q^(4*k)).",
				"G.f.: Product_{k\u003e0} (1 - q^(3*k))^3 * (1 - q^(4*k))^3 / ( (1 - q^k) * (1 - q^(2*k)) * (1 - q^(6*k)) * (1 - q^(12*k)) ).",
				"a(n) = (-1)^n * A253625(n). a(2*n) = A107760(n). a(2*n + 1) = A033762(n). a(3*n) = a(n). a(3*n + 1) = A122861(n). a(4*n + 1) = A112604(n). a(4*n + 2) = 3 * A033762(n). a(4*n + 3) = A112605(n).",
				"a(6*n + 1) = A097195(n). a(6*n + 2) = 3 * A033687(n). a(6*n + 5) = 0. a(12*n + 1) = A123884(n). a(12*n + 7) = 2 * A121361(n). a(12*n + 10) = 0."
			],
			"example": [
				"G.f. = 1 + q + 3*q^2 + q^3 + 3*q^4 + 3*q^6 + 2*q^7 + 3*q^8 + q^9 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 1, Boole[n == 0], Sum[ (-1)^ Quotient[ d, 3] {1, 1, 0}[[ Mod[d, 3, 1] ]] {1, 2}[[ Mod[n/d, 2, 1] ]], {d, Divisors @ n}]];",
				"a[ n_] := SeriesCoefficient[ QPochhammer[ -q] QPochhammer[ -q^3] (QPochhammer[ q^3, q^6] QPochhammer[ -q^2, q^2])^4, {q, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, n==0, sumdiv(n, d, kronecker(-12, d) + if(d%2, 0, 2 * kronecker(-12, d/2))))};",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^3 + A)^3 * eta(x^4 + A)^3 / ( eta(x + A) * eta(x^2 + A) * eta(x^6 + A) * eta(x^12 + A) ), n))};",
				"(PARI) {a(n) = my(A, p, e); if( n\u003c1, n==0, A = factor(n); prod( k=1, matsize(A)[1], if( p=A[k, 1], e=A[k, 2]; if( p==2, 3, if( p==3, 1, if( p%6 == 1, e+1, 1-e%2))))))};",
				"(MAGMA) A := Basis( ModularForms( Gamma1(12), 1), 86); A[1] + A[2] + 3*A[3] + A[4] + 3*A[5];"
			],
			"xref": [
				"Cf. A033687, A033762, A097195, A107760, A112604, A112605, A121361, A122861, A123884, A253625."
			],
			"keyword": "nonn,mult",
			"offset": "0,3",
			"author": "_Michael Somos_, Jan 06 2015",
			"references": 2,
			"revision": 8,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2015-01-06T15:13:56-05:00"
		}
	]
}