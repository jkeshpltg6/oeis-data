{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A153187",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 153187,
			"data": "0,1,3,2,10,80,3,21,231,3465,4,36,504,9576,229824,5,55,935,21505,623645,21827575,6,78,1560,42120,1432080,58715280,2818333440,7,105,2415,74865,2919735,137227545,7547514975,475493443425,8,136,3536,123760,5445440,288608320,17893715840,1270453824640,101636305971200",
			"name": "Triangle sequence: T(n, k) = -Product_{j=0..k+1} ((n+1)*j - 1).",
			"comment": [
				"Row sums are: {0, 4, 92, 3720, 239944, 22473720, 2878524564, 483181183072, 102924947692880, 27128289837188700, ...}."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A153187/b153187.txt\"\u003eRows n = 0..100 of triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = -Product_{j=0..k+1} (j*(n+1) - 1).",
				"T(n, k) = -(n+1)^(k+2) * Pochhammer(-1/(n+1), k+2)."
			],
			"example": [
				"Triangle begins as:",
				"  0;",
				"  1,   3;",
				"  2,  10,   80;",
				"  3,  21,  231,  3465;",
				"  4,  36,  504,  9576,  229824;",
				"  5,  55,  935, 21505,  623645,  21827575;",
				"  6,  78, 1560, 42120, 1432080,  58715280,  2818333440;",
				"  7, 105, 2415, 74865, 2919735, 137227545,  7547514975, 475493443425;"
			],
			"maple": [
				"seq(seq(-mul(j*(n+1)-1, j = 0..k+1), k = 0..n), n = 0..10); # _G. C. Greubel_, Mar 05 2020"
			],
			"mathematica": [
				"T[n_, m_] = -Product[(n+1)*j -1, {j,0,m+1}]; Table[T[n, m], {n,0,10}, {m,0,n}]//Flatten",
				"Table[-(n+1)^(k+2)*Pochhammer[-1/(n+1), k+2], {n,0,10}, {k,0,n}]//Flatten (* _G. C. Greubel_, Mar 05 2020 *)"
			],
			"program": [
				"(PARI) T(n,k) = (-1)*prod(j=0, k+1, j*(n+1)-1);",
				"for(j=0, 10, for(k=0,n, print1(T(n,k), \", \"))) \\\\ _G. C. Greubel_, Mar 05 2020",
				"(MAGMA) [-(\u0026*[j*(n+1)-1: j in [0..k+1]]): k in [0..n], n in [0..10]]; // _G. C. Greubel_, Mar 05 2020",
				"(Sage) [[-(n+1)^(k+2)*rising_factorial(-1/(n+1), k+2) for k in (0..n)] for n in (0..10)] # _G. C. Greubel_, Mar 05 2020",
				"(GAP) Flat(List([0..10], n-\u003e List([0..n], k-\u003e (-1)*Product([0..k+1], j-\u003e j*(n+1) -1) ))); # _G. C. Greubel_, Mar 05 2020"
			],
			"xref": [
				"Cf. A001477, A014105, A033593, A033594, A153273."
			],
			"keyword": "nonn,tabl",
			"offset": "0,3",
			"author": "_Roger L. Bagula_, Dec 20 2008",
			"ext": [
				"Edited by _G. C. Greubel_, Mar 05 2020"
			],
			"references": 2,
			"revision": 7,
			"time": "2020-03-05T04:57:10-05:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}