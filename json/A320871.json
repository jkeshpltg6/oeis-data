{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A320871",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 320871,
			"data": "2,7,6,9,5,1,4,3,8,2,9,7,11,6,1,5,3,10,3,7,8,11,6,1,4,5,9,3,8,7,10,6,2,5,4,9,2,11,8,13,7,1,6,3,12,3,10,8,12,7,2,6,4,11,4,8,9,12,7,2,5,6,10,4,9,8,11,7,3,6,5,10,2,13,9,15,8,1,7,3,14,3,11,10,15,8,1,6,5,13",
			"name": "List of all inequivalent 3 X 3 magic squares made of distinct positive integers, sorted by increasing sum. For each equivalence class modulo symmetries of the square, the lexicographically smallest representative is shown.",
			"comment": [
				"\"Symmetries of the square\" means the symmetry group D4 consisting of reflections on any of the 4 symmetry axes of the square (horizontal H, vertical V, 2 diagonals D \u0026 A), which also generates the rotations around the center by multiples of 90°, R1, R2, R3 (and R0 = id): e.g., H o D = R1, where D means to transpose the 3 X 3 matrix, H means reversal of the rows, etc.",
				"The 8 (\"equivalent\") variants of the first square are listed in A217568."
			],
			"example": [
				"The first five inequivalent magic squares (with magic sums 15, 18, 18, 18, 21) are",
				"   [2 7 6]    [ 2 9  7]    [ 3 7 8]    [ 3 8 7]    [ 2 11  8]",
				"   [9 5 1]    [11 6  1]    [11 6 1]    [10 6 2]    [13  7  1]",
				"   [4 3 8]    [ 5 3 10]    [ 4 5 9]    [ 5 4 9]    [ 6  3 12]",
				"They are listed as rows of the 9 elements of each square, so the first row is:",
				"  [2, 7, 6; 9, 5, 1; 4, 3, 8],",
				"the second row is:",
				"  [2, 9, 7; 11, 6, 1; 5, 3, 10], and so on."
			],
			"program": [
				"(PARI) A320871_row(N=10,show_all=1,c=3)={for(c=c,oo, forstep(d=c-1,2,-1, for(b=max(2*d+1-c,1), d-1, d!=2*b\u0026\u0026 S=[c-d,c+b,c+d-b;c+2*d-b,c,c-2*d+b;c-d+b,c-b,c+d]; !(show_all\u0026\u0026print(S))\u0026\u0026 !N--\u0026\u0026 return(S))))} \\\\ The third (optional) argument allows to start the list with the first square(s) having the central element \u003e= c, i.e., magic sum \u003e= 3c."
			],
			"xref": [
				"Cf. A217568: the 8 equivalent variants of the first row.",
				"Cf. A320872: subsequence of rows that consist only of primes; A268790 lists their magic sums with duplicates removed.",
				"Cf. A320873: the first row that consists of a set of consecutive primes; it has magic sum = 4440084513 = A270305(1) = A073520(3)."
			],
			"keyword": "nonn,tabf",
			"offset": "1,1",
			"author": "_M. F. Hasler_, Oct 28 2018",
			"references": 3,
			"revision": 9,
			"time": "2019-11-11T00:48:38-05:00",
			"created": "2018-11-09T04:10:26-05:00"
		}
	]
}