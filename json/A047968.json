{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A047968",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 47968,
			"data": "1,3,4,8,8,17,16,30,34,52,57,99,102,153,187,261,298,432,491,684,811,1061,1256,1696,1966,2540,3044,3876,4566,5846,6843,8610,10203,12610,14906,18491,21638,26508,31290,38044,44584,54133,63262,76241",
			"name": "a(n) = Sum_{d|n} p(d), where p(d) = A000041 = number of partitions of d.",
			"comment": [
				"Inverse Moebius transform of A000041.",
				"Row sums of triangle A137587. - _Gary W. Adamson_, Jan 27 2008",
				"Row sums of triangle A168021. - _Omar E. Pol_, Nov 20 2009",
				"Row sums of triangle A168017. Row sums of triangle A168018. - _Omar E. Pol_, Nov 25 2009",
				"Sum of the partition numbers of the divisors of n. - _Omar E. Pol_, Feb 25 2014",
				"Conjecture: for n \u003e 6, a(n) is strictly increasing. - _Franklin T. Adams-Watters_, Apr 19 2014",
				"Number of constant multiset partitions of multisets spanning an initial interval of positive integers with multiplicities an integer partition of n. - _Gus Wiseman_, Sep 16 2018"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A047968/b047968.txt\"\u003eTable of n, a(n) for n=1..1000\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/transforms.txt\"\u003eTransforms\u003c/a\u003e"
			],
			"formula": [
				"G.f.: Sum_{k\u003e0} (-1+1/Product_{i\u003e0} (1-z^(k*i))). - _Vladeta Jovovic_, Jun 22 2003",
				"G.f.: sum(n\u003e0,A000041(n)*x^n/(1-x^n)). - _Mircea Merca_, Feb 24 2014.",
				"a(n) = A168111(n) + A000041(n). - _Omar E. Pol_, Feb 26 2014",
				"a(n) = Sum_{y is a partition of n} A000005(GCD(y)). - _Gus Wiseman_, Sep 16 2018"
			],
			"example": [
				"For n = 10 the divisors of 10 are 1, 2, 5, 10, hence the partition numbers of the divisors of 10 are 1, 2, 7, 42, so a(10) = 1 + 2 + 7 + 42 = 52. - _Omar E. Pol_, Feb 26 2014",
				"From _Gus Wiseman_, Sep 16 2018: (Start)",
				"The a(6) = 17 constant multiset partitions:",
				"  (111111)  (111)(111)    (11)(11)(11)  (1)(1)(1)(1)(1)(1)",
				"  (111222)  (12)(12)(12)",
				"  (111122)  (112)(112)",
				"  (112233)  (123)(123)",
				"  (111112)",
				"  (111123)",
				"  (111223)",
				"  (111234)",
				"  (112234)",
				"  (112345)",
				"  (123456)",
				"(End)"
			],
			"maple": [
				"with(combinat): with(numtheory): a := proc(n) c := 0: l := sort(convert(divisors(n), list)): for i from 1 to nops(l) do c := c+numbpart(l[i]) od: RETURN(c): end: for j from 1 to 60 do printf(`%d, `, a(j)) od: # _Zerinvary Lajos_, Apr 14 2007"
			],
			"mathematica": [
				"a[n_] := Sum[ PartitionsP[d], {d, Divisors[n]}]; Table[a[n], {n, 1, 44}] (* _Jean-François Alcover_, Oct 03 2013 *)"
			],
			"xref": [
				"Cf. A000041, A000837, A047966, A055893, A137587, A003606 (Euler transform).",
				"Cf. A002033, A003238, A018783, A034729, A052409, A078392, A100953, A319162."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, Dec 11 1999",
			"references": 56,
			"revision": 43,
			"time": "2018-09-16T21:34:39-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}