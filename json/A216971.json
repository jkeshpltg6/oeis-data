{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A216971",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 216971,
			"data": "1,2,2,6,18,3,24,156,72,4,120,1520,1260,220,5,720,17310,21000,7020,600,6,5040,232932,363720,187320,32970,1554,7,40320,3698744,6794256,4746840,1351840,141288,3920,8,362880,68680656,139241088,121105152,48822480,8625456,573048,9720,9,3628800,1471193370",
			"name": "Triangle read by rows: T(n,k) is the number of functions f:{1,2,...,n}-\u003e{1,2,...,n} that have exactly k nonrecurrent elements mapped to some (one or more) recurrent element. n \u003e= 1, 0 \u003c= k \u003c= n-1.",
			"comment": [
				"x in {1,2,...,n} is a recurrent element if there is some k such that f^k(x) = x where f^k(x) denotes iterated functional composition. In other words, a recurrent element is in a cycle of the functional digraph.",
				"Row sums = n^n.",
				"First column (k = 0) counts the n! bijective functions.",
				"T(n,n-1) counts the n constant functions.",
				"Conjecture: every entry in row n is divisible by n. - _Jon Perry_, Sep 21 2012"
			],
			"link": [
				"Joerg Arndt, \u003ca href=\"/A216971/b216971.txt\"\u003eTable of n, a(n) for n = 1..528\u003c/a\u003e"
			],
			"formula": [
				"E.g.f.: 1/(1-x*exp(y*T(x))) - 1 where T(x) is the e.g.f. for A000169."
			],
			"example": [
				"Triangle starts:",
				"     1,",
				"     2,      2,",
				"     6,     18,      3,",
				"    24,    156,     72,      4,",
				"   120,   1520,   1260,    220,      5,",
				"   720,  17310,  21000,   7020,    600,      6,",
				"  5040, 232932, 363720, 187320,  32970,   1554,      7,",
				"  ..."
			],
			"mathematica": [
				"nn=7;t=Sum[n^(n-1)x^n/n!,{n,1,nn}];f[list_]:=Select[list,#\u003e0\u0026];Drop[Map[f,Range[0,nn]! CoefficientList[Series[1/(1-x Exp[y t]),{x,0,nn}],{x,y}]],1]//Grid"
			],
			"program": [
				"(PARI)",
				"N=15; x='x+O('x^N);",
				"T=serreverse(x*exp(-x));",
				"egf=1/(1-x*exp('y*T)) - 1;",
				"v=Vec(serlaplace(egf));",
				"{ for (n=1, N-1, /* print triangle: */",
				"    row = Pol( v[n], 'y );",
				"    row = polrecip( row );",
				"    print( Vec(row) );",
				"); }",
				"/* _Joerg Arndt_, Sep 21 2012 */"
			],
			"keyword": "nonn,nice,tabl",
			"offset": "1,2",
			"author": "_Geoffrey Critzer_, Sep 21 2012",
			"references": 3,
			"revision": 38,
			"time": "2015-11-30T09:35:44-05:00",
			"created": "2012-09-21T08:37:45-04:00"
		}
	]
}