{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A097310",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 97310,
			"data": "1,14,391,10934,305761,8550374,239104711,6686381534,186979578241,5228741809214,146217791079751,4088869408423814,114342125644787041,3197490648645613334,89415396036432386311,2500433598371461203374,69922725358364481308161,1955335876435834015425134",
			"name": "Chebyshev T-polynomials T(n,14) with Diophantine property.",
			"comment": [
				"a(n)^2 - 195 b(n)^2 = +1 with b(n):=A097311(n) gives all nonnegative solutions of this Pell equation.",
				"a(195+390k)-1 and a(195+390k)+1 are consecutive odd powerful numbers. See A076445. - _T. D. Noe_, May 04 2006",
				"Except for the first term, positive values of x (or y) satisfying x^2 - 28xy + y^2 + 195 = 0. - _Colin Barker_, Feb 23 2014"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A097310/b097310.txt\"\u003eTable of n, a(n) for n = 0..700\u003c/a\u003e",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e",
				"\u003ca href=\"/index/Rec\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (28,-1)."
			],
			"formula": [
				"a(n)=28*a(n-1) - a(n-2), a(-1):= 14, a(0)=1.",
				"a(n)= T(n, 14)= (S(n, 28)-S(n-2, 28))/2 = S(n, 28)-14*S(n-1, 28) with T(n, x), resp. S(n, x), Chebyshev's polynomials of the first, resp.second, kind. See A053120 and A049310. S(n, 28)=A097311(n).",
				"a(n)= (ap^n + am^n)/2 with ap := 14+sqrt(195) and am := 14-sqrt(195).",
				"a(n)= sum(((-1)^k)*(n/(2*(n-k)))*binomial(n-k, k)*(2*14)^(n-2*k), k=0..floor(n/2)), n\u003e=1.",
				"G.f.: (1-14*x)/(1-28*x+x^2).",
				"a(n)=sqrt(1 + 195*A097311(n)^2), n\u003e=0."
			],
			"mathematica": [
				"LinearRecurrence[{28,-1},{1,14},20] (* _Harvey P. Dale_, Jan 29 2014 *)",
				"CoefficientList[Series[(1 - 14 x)/(1 - 28 x + x^2), {x, 0, 40}], x] (* _Vincenzo Librandi_, Feb 24 2014 *)"
			],
			"program": [
				"(Sage) [lucas_number2(n,28,1)/2 for n in range(0,16)] - _Zerinvary Lajos_, Jun 27 2008",
				"(PARI) Vec((1-14*x)/(1-28*x+x^2) + O(x^100)) \\\\ _Colin Barker_, Feb 23 2014"
			],
			"xref": [
				"Cf. A090249, A097311."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Aug 31 2004",
			"ext": [
				"More terms from _Colin Barker_, Feb 23 2014"
			],
			"references": 5,
			"revision": 26,
			"time": "2019-12-07T12:18:24-05:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}