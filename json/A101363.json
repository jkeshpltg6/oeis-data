{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A101363",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 101363,
			"data": "0,1,8,20,60,112,208,216,480,660,864,1196,1568,2250,2464,2992,3924,4332,5160,8148,7040,8096,10560,10600,12064,15552,15288,17052,25320,21080,23360,30360,28288,30940,36288,36852,40128,50076,47120,50840,67620",
			"name": "In the interior of a regular 2n-gon with all diagonals drawn, the number of points where exactly three diagonals intersect.",
			"comment": [
				"When n is odd, there are no intersections in the interior of an n-gon where more than 2 diagonals meet.",
				"When n is not a multiple of 6, there are no intersections in the interior of an n-gon where more than 3 diagonals meet.",
				"When n is not a multiple of 30, there are no intersections in the interior of an n-gon where more than 5 diagonals meet.",
				"I checked the following conjecture up to n=210: \"An n-gon with n=30k has 5n points where 6 or 7 diagonals meet and no points where more than 7 diagonals meet; If k is odd, then 6 diagonals meet in each of 4n points and 7 diagonals meet in each of n points; If k is even, then no groups of exactly 6 diagonals meet in a point, while exactly 7 diagonals meet in each of 5n points.\""
			],
			"link": [
				"Graeme McRae, Feb 23 2008, \u003ca href=\"/A101363/b101363.txt\"\u003eTable of n, a(n) for n = 2..105\u003c/a\u003e",
				"M. F. Hasler, \u003ca href=\"/A006561/a006561.html\"\u003eInteractive illustration of A006561(n)\u003c/a\u003e",
				"B. Poonen and M. Rubinstein, \u003ca href=\"http://arXiv.org/abs/math.MG/9508209\"\u003eThe number of intersection points made by the diagonals of a regular polygon\u003c/a\u003e, arXiv:math/9508209 [math.MG], 1995-2006, which has fewer typos than the SIAM version.",
				"B. Poonen and M. Rubinstein, \u003ca href=\"http://dx.doi.org/10.1137/S0895480195281246\"\u003eNumber of Intersection Points Made by the Diagonals of a Regular Polygon\u003c/a\u003e, SIAM J. Discrete Mathematics, Vol. 11, pp. 135-156 (1998). [Copy on SIAM web site]",
				"B. Poonen and M. Rubinstein, \u003ca href=\"http://math.mit.edu/~poonen/papers/ngon.pdf\"\u003eThe number of intersection points made by the diagonals of a regular polygon\u003c/a\u003e, SIAM J. on Discrete Mathematics, Vol. 11, No. 1, 135-156 (1998). [Copy on B. Poonen's web site]",
				"B. Poonen and M. Rubinstein, \u003ca href=\"http://math.mit.edu/~poonen/papers/ngon.m\"\u003eMathematica programs for A006561 and related sequences\u003c/a\u003e",
				"M. Rubinstein, \u003ca href=\"/A006561/a006561_3.pdf\"\u003eDrawings for n=4,5,6,...\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/A006561/a006561_4.pdf\"\u003eIllustrations of a(8) and a(9)\u003c/a\u003e",
				"R. G. Wilson V, \u003ca href=\"/A006561/a006561_1.pdf\"\u003eIllustration of a(10)\u003c/a\u003e",
				"\u003ca href=\"/index/Pol#Poonen\"\u003eIndex entry for Sequences formed by drawing all diagonals in regular polygon\u003c/a\u003e"
			],
			"example": [
				"a(6)=60 because inside a regular 12-gon there are 60 points (4 on each radius and 1 midway between radii) where exactly three diagonals intersect."
			],
			"xref": [
				"Cf. A006561, A007678, A101364, A101365",
				"A column of A292105.",
				"Cf. A000332: C(n, 4) = number of intersection points of diagonals of convex n-gon.",
				"Cf. A006561: number of intersections of diagonals in the interior of regular n-gon",
				"Cf. A292104: number of 2-way intersections in the interior of a regular n-gon",
				"Cf. A101364: number of 4-way intersections in the interior of a regular n-gon",
				"Cf. A101365: number of 5-way intersections in the interior of a regular n-gon",
				"Cf. A137938: number of 4-way intersections in the interior of a regular 6n-gon",
				"Cf. A137939: number of 5-way intersections in the interior of a regular 6n-gon."
			],
			"keyword": "nonn",
			"offset": "2,3",
			"author": "_Graeme McRae_, Dec 26 2004, revised Feb 23 2008, Feb 26 2008",
			"references": 6,
			"revision": 18,
			"time": "2018-02-16T20:23:57-05:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}