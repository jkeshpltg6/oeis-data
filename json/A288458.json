{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A288458",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 288458,
			"data": "1,-24,288,-2688,-32256,2820096,-95035392,1972076544,-9841803264,-1288894414848,70351960670208,-2164060518875136,36664809432809472,365875642245316608,-55960058736918134784,2436570173137823465472,-64272155689216515244032,664295705652718630600704,35692460661517822602510336",
			"name": "Chebyshev coefficients of density of states of cubic lattice.",
			"comment": [
				"This is the sequence of integers z^n g_n for n=0,2,4,6,... where g_n are the coefficients in the Chebyshev polynomial expansion of the density of states of the simple cubic lattice (z=6), g(w) = 1 / (Pi*sqrt(1-w^2)) * Sum_{n\u003e=0} (2-delta_n) g_n T_n(w). Here |w| \u003c= 1 and delta is the Kronecker delta.",
				"The Chebyshev coefficients, g_n, are related to the number of walks on the lattice that return to the origin, W_n, as g_n = Sum_{k=0..n} a_{nk} z^{-k} W_k, where z is the coordination number of the lattice and a_{nk} are the coefficients of Chebyshev polynomials such that T_n(x) = Sum_{k=0..n} a_{nk} x^k.",
				"The author was unable to obtain a closed form for z^n g_n."
			],
			"link": [
				"Yen Lee Loh, \u003ca href=\"http://arxiv.org/abs/1706.03083\"\u003eA general method for calculating lattice Green functions on the branch cut\u003c/a\u003e, arXiv:1706.03083 [math-ph], 2017."
			],
			"mathematica": [
				"Whon[n_] := If[OddQ[n], 0,",
				"   Sum[Binomial[n/2,j]^2 Binomial[2j,j], {j, 0, n/2}]];",
				"Wcub[n_] := Binomial[n, n/2] Whon[n];",
				"ank[n_, k_] := SeriesCoefficient[ChebyshevT[n, x], {x, 0, k}];",
				"zng[n_] := Sum[ank[n, k]*6^(n-k)*Wcub[k], {k, 0, n}];",
				"Table[zng[n], {n,0,50}]"
			],
			"xref": [
				"Related to numbers of walks returning to origin, W_n, on cubic lattice (A002896).",
				"See also A288454, A288455, A288456, A288457, A288458, A288459, A288460, A288461."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Yen-Lee Loh_, Jun 16 2017",
			"references": 8,
			"revision": 18,
			"time": "2017-06-19T15:15:41-04:00",
			"created": "2017-06-19T15:15:41-04:00"
		}
	]
}