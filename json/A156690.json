{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A156690",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 156690,
			"data": "1,1,1,1,-3,1,1,15,15,1,1,-105,525,-105,1,1,945,33075,33075,945,1,1,-10395,3274425,-22920975,3274425,-10395,1,1,135135,468242775,29499294825,29499294825,468242775,135135,1,1,-2027025,91307341125,-63275987399625,569483886596625,-63275987399625,91307341125,-2027025,1",
			"name": "Triangle T(n, k, m) = t(n,m)/( t(k,m) * t(n-k,m) ) with T(n, 0, m) = T(n, n, m) = 1, where t(n, m) = Product_{j=1..n} Product_{i=1..j-1} ( 1 - (m+1)*(i+1) ) and m = 1, read by rows.",
			"comment": [
				"Row sums are: {1, 2, -1, 32, 317, 68042, -16392913, 59935345472, 443114522425577, 41952026212764267602, -11773681484663891313796273, ...}."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A156690/b156690.txt\"\u003eRows n = 0..30 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k, m) = t(n,m)/( t(k,m) * t(n-k,m) ) with T(n, 0, m) = T(n, n, m) = 1, where t(n, m) = Product_{j=1..n} Product_{i=1..j-1} ( 1 - (m+1)*(i+1) ) and m = 1.",
				"T(n, k, m, p, q) = (-p*(m+1))^(k*(n-k)) * (f(n,m,p,q)/(f(k,m,p,q)*f(n-k,m,p,q))) where Product_{j=1..n} Pochhammer( (q*(m+1) -1)/(p*(m+1)), j) for (m, p, q) = (1, 1, 1). - _G. C. Greubel_, Feb 25 2021"
			],
			"example": [
				"Triangle begins as:",
				"  1;",
				"  1,      1;",
				"  1,     -3,         1;",
				"  1,     15,        15,           1;",
				"  1,   -105,       525,        -105,           1;",
				"  1,    945,     33075,       33075,         945,         1;",
				"  1, -10395,   3274425,   -22920975,     3274425,    -10395,      1;",
				"  1, 135135, 468242775, 29499294825, 29499294825, 468242775, 135135, 1;"
			],
			"mathematica": [
				"(* First program *)",
				"t[n_, k_]:= If[k==0, n!, Product[1 -(i+1)*(k+1), {j,n}, {i,0,j-1}] ];",
				"T[n_, k_, m_]:= If[n==0, 1, t[n, m]/(t[k, m]*t[n-k, m])];",
				"Table[T[n,k,1], {n,0,12}, {k,0,n}]//Flatten (* modified by _G. C. Greubel_, Feb 25 2021 *)",
				"(* Second program *)",
				"f[n_, m_, p_, q_]:= Product[Pochhammer[(q*(m+1) -1)/(p*(m+1)), j], {j,n}];",
				"T[n_, k_, m_, p_, q_]:= (-p*(m+1))^(k*(n-k))*(f[n,m,p,q]/(f[k,m,p,q]*f[n-k,m,p,q]));",
				"Table[T[n,k,1,1,1], {n,0,12}, {k,0,n}]//Flatten (* _G. C. Greubel_, Feb 25 2021 *)"
			],
			"program": [
				"(Sage)",
				"@CachedFunction",
				"def f(n, m, p, q): return product( rising_factorial( (q*(m+1)-1)/(p*(m+1)), j) for j in (1..n))",
				"def T(n,k,m,p,q): return (-p*(m+1))^(k*(n-k))*(f(n,m,p,q)/(f(k,m,p,q)*f(n-k,m,p,q) ))",
				"flatten([[T(n,k,1,1,1) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, Feb 25 2021",
				"(Magma)",
				"f:= func\u003c n,m,p,q | n eq 0 select 1 else m eq 0 select Factorial(n) else (\u0026*[ 1 -(p*i+q)*(m+1): i in [0..j], j in [0..n-1]]) \u003e;",
				"T:= func\u003c n,k,m,p,q | f(n,m,p,q)/(f(k,m,p,q)*f(n-k,m,p,q)) \u003e;",
				"[T(n,k,1,1,1): k in [0..n], n in [0..12]]; // _G. C. Greubel_, Feb 25 2021"
			],
			"xref": [
				"Cf. A007318 (m=0), this sequence (m=1), A156691 (m=2), A156692 (m=3).",
				"Cf. A156693, A156696, A156722."
			],
			"keyword": "sign,tabl",
			"offset": "0,5",
			"author": "_Roger L. Bagula_, Feb 13 2009",
			"ext": [
				"Edited by _G. C. Greubel_, Feb 25 2021"
			],
			"references": 8,
			"revision": 12,
			"time": "2021-02-26T05:48:21-05:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}