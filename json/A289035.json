{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A289035",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 289035,
			"data": "0,0,1,0,0,1,0,0,1,0,0,0,1,0,0,1,0,0,0,1,0,0,1,0,0,1,0,0,0,1,0,0,1,0,0,0,1,0,0,1,0,0,1,0,0,0,1,0,0,1,0,0,0,1,0,0,1,0,0,0,1,0,0,1,0,0,1,0,0,0,1,0,0,1,0,0,0,1,0,0,1,0,0,1,0,0",
			"name": "Fixed point of the mapping 00-\u003e0010, 01-\u003e010, 10-\u003e010, starting with 00.",
			"comment": [
				"Conjecture: the number of letters (0's and 1's) in the n-th iterate of the mapping is given by A289004.",
				"The mapping is applied as in the Mathematica command StringReplace. In particular, for a word of odd length, the final letter is retained, as in 0010010 -\u003e 00100100100. (If the final letter is removed, the iterates are changed, but the limiting fixed point remains unchanged. See A293077.)",
				"More generally, to describe the result of the command StringReplace[w, {u(1)-\u003ev(1), u(2)-\u003ev(2) ... u(n)-\u003cv(n)}], assume that no u(i) is a subword of any other u(j). The word w is then a concatenation of subwords u(i(0)), g(1), u(i(1)), g(2), u(i(2)), ..., such that u(i(0)) is the empty word, and each g(j) is the subword, g, possibly empty, of least length immediately following u(i(j-1)), such that no subword of g is one of the words u(i). The result of the command is then the concatenation of g(1), v(i(1)), g(2), v(i(2)), ...  That is, each u(i) is replaced by v(i), and the words g(i) are left unchanged."
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A289035/b289035.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"First seven iterates of the mapping:",
				"00",
				"0010",
				"0010010",
				"00100100100",
				"001001001000100100",
				"0010010010001001000100100100010",
				"0010010010001001000100100100010010001001001000100100"
			],
			"mathematica": [
				"z = 10; (* number of iterates *)",
				"s = {0, 0}; w[0] = StringJoin[Map[ToString, s]];",
				"w[n_] := StringReplace[w[n - 1], {\"00\" -\u003e \"0010\", \"01\" -\u003e \"010\", \"10\" -\u003e \"010\"}]",
				"TableForm[Table[w[n], {n, 0, 10}]]",
				"st = ToCharacterCode[w[z]] - 48   (* A289035 *)",
				"Flatten[Position[st, 0]]  (* A289036 *)",
				"Flatten[Position[st, 1]]  (* A289037 *)",
				"Table[StringLength[w[n]], {n, 0, 20}] (* A289004 *)"
			],
			"xref": [
				"Cf. A289036, A289037, A289004, A293076."
			],
			"keyword": "nonn,easy",
			"offset": "1",
			"author": "_Clark Kimberling_, Jun 27 2017",
			"ext": [
				"Updated by _Clark Kimberling_, Sep 30 2017"
			],
			"references": 6,
			"revision": 18,
			"time": "2020-04-07T21:23:38-04:00",
			"created": "2017-06-27T21:37:33-04:00"
		}
	]
}