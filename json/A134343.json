{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A134343",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 134343,
			"data": "1,-2,1,-2,2,0,3,-2,0,-2,2,-2,1,-2,0,-2,4,0,2,0,1,-4,2,0,2,-2,0,-2,2,-2,1,-4,0,0,2,0,4,-2,2,-2,0,0,3,-2,0,-2,4,0,2,-2,0,-4,0,0,0,-4,3,-2,2,0,2,-2,0,0,2,-2,4,-2,0,-2,2,0,3,-2,0,0,4,0,2",
			"name": "Expansion of psi(-x)^2 in powers of x where psi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Number 57 of the 74 eta-quotients listed in Table I of Martin (1996)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A134343/b134343.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Y. Martin, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9947-96-01743-6\"\u003eMultiplicative eta-quotients\u003c/a\u003e, Trans. Amer. Math. Soc. 348 (1996), no. 12, 4825-4856, see page 4852 Table I.",
				"Michael Somos, \u003ca href=\"/A030203/a030203.txt\"\u003eIndex to Yves Martin's list of 74 multiplicative eta-quotients and their A-numbers\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(-1/4) * (eta(q) * eta(q^4) / eta(q^2))^2 in powers of q.",
				"Euler transform of period 4 sequence [ -2, 0, -2, -2, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (64 t)) = 8 (t/i) f(t) where q = exp(2 Pi i t).",
				"a(n) = b(4*n + 1) where b() is multiplicative with b(2^e) = 0^e, b(p^e) = (1 + (-1)^e) / 2 if p == 3 (mod 4), b(p^e) = e+1 if p == 1 (mod 8), b(p^e) = (-1)^e * (e+1) if p == 5 (mod 8).",
				"G.f.: (Product_{k\u003e0} (1 - x^k) * (1 + x^(2*k)))^2.",
				"a(9*n + 5) = a(9*n + 8) = 0. a(n) = (-1)^n * A008441(n). a(2*n) = A113407(n). a(2*n + 1) = -2 * A053692(n).",
				"2 * a(n) = A204531(4*n + 1) = - A246950(n). a(4*n) = A246862(n). a(4*n + 2) = A246683(n). - _Michael Somos_, Jun 22 2015",
				"a(4*n + 1) = -2 * A259287(n). a(4*n + 3) = -2 * A259285(n). - _Michael Somos_, Jun 24 2015",
				"Convolution square is A121613. Convolution cube is A213791. Convolution with A000009 is A143379. Convolution with A000143 is A209942. _Michael Somos_, Jun 22 2015",
				"G.f.: Sum_{k\u003e0 odd} (x^k + x^(3*k)) / (1 + x^(4*k)) * (-1)^floor((k+1) / 4). - _Michael Somos_, Jun 22 2015",
				"G.f.: Sum_{k\u003e0 odd} (x^k - x^(3*k)) / (1 + x^(4*k)) * (-1)^floor(k / 4). - _Michael Somos_, Jun 22 2015"
			],
			"example": [
				"G.f. = 1 - 2*x + x^2 - 2*x^3 + 2*x^4 + 3*x^6 - 2*x^7 - 2*x^9 + 2*x^10 + ...",
				"G.f. = q - 2*q^5 + q^9 - 2*q^13 + 2*q^17 + 3*q^25 - 2*q^29 - 2*q^37 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 0, 0, (-1)^n DivisorSum[ 4 n + 1, (-1)^Quotient[#, 2] \u0026]]; (* _Michael Somos_, Jun 22 2015 *)",
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 2, Pi/4, x^(1/2)]^2 / (2 x^(1/4)), {x, 0, n}]; (* _Michael Somos_, Jun 22 2015 *)",
				"a[ n_] := SeriesCoefficient[(QPochhammer[ x] QPochhammer[ x^4] / QPochhammer[ x^2])^2, {x, 0, n}]; (* _Michael Somos_, Jun 22 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, 0, (-1)^n * sumdiv( 4*n + 1, d, (-1)^(d\\2)))};",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( ( eta(x + A) * eta(x^4 + A) / eta(x^2 + A) )^2, n))};",
				"(MAGMA) A := Basis( ModularForms( Gamma1(64), 1), 321); A[2] - 2*A[6] + A[10] - 2*A[14] + 2*A[18] + 3*A[26] - 2*A[30] + 2*A[35] - 2*A[36]; /* _Michael Somos_, Jun 22 2015 */;"
			],
			"xref": [
				"Cf. A008441, A053692, A113407, A121613, A143379, A209942, A213791, A246862, A246683, A246950, A259285, A259287."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Oct 21 2007",
			"references": 11,
			"revision": 23,
			"time": "2021-03-12T22:24:45-05:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}