{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A034356",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 34356,
			"data": "1,2,1,3,3,1,4,6,4,1,5,10,10,5,1,6,16,22,16,6,1,7,23,43,43,23,7,1,8,32,77,106,77,32,8,1,9,43,131,240,240,131,43,9,1,10,56,213,516,705,516,213,56,10,1,11,71,333,1060,1988,1988,1060,333,71,11,1,12,89",
			"name": "Triangle read by rows giving T(n,k) = number of inequivalent linear [n,k] binary codes (n \u003e= 1, 1 \u003c= k \u003c= n).",
			"link": [
				"Harald Fripertinger, \u003ca href=\"http://www.mathe2.uni-bayreuth.de/frib/codes/tables.html\"\u003eIsometry Classes of Codes\u003c/a\u003e.",
				"Harald Fripertinger, \u003ca href=\"http://www.mathe2.uni-bayreuth.de/frib/codes/tables_6.html\"\u003eWnk2: Number of the isometry classes of all binary (n,k)-codes\u003c/a\u003e. [This is a rectangular array whose lower triangle contains T(n,k).]",
				"H. Fripertinger and A. Kerber, \u003ca href=\"https://www.researchgate.net/publication/2550138_Isometry_Classes_of_Indecomposable_Linear_Codes\"\u003eIsometry classes of indecomposable linear codes\u003c/a\u003e, preprint, 1995. [We have T(n,k) = W_{nk2}; see p. 4 of the preprint.]",
				"H. Fripertinger and A. Kerber, \u003ca href=\"https://doi.org/10.1007/3-540-60114-7_15\"\u003eIsometry classes of indecomposable linear codes\u003c/a\u003e. In: G. Cohen, M. Giusti, T. Mora (eds), Applied Algebra, Algebraic Algorithms and Error-Correcting Codes, 11th International Symposium, AAECC 1995, Lect. Notes Comp. Sci. 948 (1995), pp. 194-204. [We have T(n,k) = W_{nk2}; see p. 197.]",
				"Petros Hadjicostas, \u003ca href=\"/A034358/a034358.txt\"\u003eGenerating function for column k=4\u003c/a\u003e.",
				"Petros Hadjicostas, \u003ca href=\"/A034356/a034356.txt\"\u003eGenerating function for column k=5\u003c/a\u003e.",
				"Petros Hadjicostas, \u003ca href=\"/A034356/a034356_1.txt\"\u003eGenerating function for column k=6\u003c/a\u003e.",
				"Kent E. Morrison, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL9/Morrison/morrison37.html\"\u003eInteger Sequences and Matrices Over Finite Fields\u003c/a\u003e, Journal of Integer Sequences, Vol. 9 (2006), Article 06.2.1.",
				"David Slepian, \u003ca href=\"https://archive.org/details/bstj39-5-1219\"\u003eSome further theory of group codes\u003c/a\u003e, Bell System Tech. J. 39(5) (1960), 1219-1252.",
				"David Slepian, \u003ca href=\"https://doi.org/10.1002/j.1538-7305.1960.tb03958.x\"\u003eSome further theory of group codes\u003c/a\u003e, Bell System Tech. J. 39(5) (1960), 1219-1252.",
				"Marcel Wild, \u003ca href=\"http://dx.doi.org/10.1006/eujc.1996.0026\"\u003eConsequences of the Brylawski-Lucas Theorem for binary matroids\u003c/a\u003e, European Journal of Combinatorics 17 (1996), 309-316.",
				"Marcel Wild, \u003ca href=\"http://dx.doi.org/10.1006/ffta.1999.0273\"\u003eThe asymptotic number of inequivalent binary codes and nonisomorphic binary matroids\u003c/a\u003e, Finite Fields and their Applications 6 (2000), 192-202.",
				"Marcel Wild, \u003ca href=\"https://doi.org/10.1137/S0895480104445538\"\u003eThe asymptotic number of binary codes and binary matroids\u003c/a\u003e, SIAM J. Discrete Math. 19(3) (2005), 691-699. [This paper apparently corrects errors in previous papers.]",
				"\u003ca href=\"/index/Coa#codes_binary_linear\"\u003eIndex entries for sequences related to binary linear codes\u003c/a\u003e"
			],
			"formula": [
				"From _Petros Hadjicostas_, Sep 30 2019: (Start)",
				"T(n,k) = Sum_{i = k..n} A034253(i,k) for 1 \u003c= k \u003c= n.",
				"G.f. for column k=1: x/(1-x)^2.",
				"G.f. for column k=2: -(x^3 - x - 1)*x^2/((x^2 + x + 1)*(x + 1)*(x - 1)^4).",
				"G.f. for column k=3: -(x^12 - 2*x^11 + x^10 - x^9 - x^6 + x^4 - x - 1)*x^3/((x^6 + x^5 + x^4 + x^3 + x^2 + x + 1)*(x^2 + x + 1)^2*(x^2 + 1)*(x + 1)^2*(x - 1)^8).",
				"G.f. for column k \u003e= 4: modify the Sage program below (cf. function f). It is too complicated to write it here. For some cases, see also the links above.",
				"(End)"
			],
			"example": [
				"Table T(n,k) (with rows n \u003e= 1 and columns k \u003e= 1) begins as follows:",
				"  1;",
				"  2,  1;",
				"  3,  3,  1;",
				"  4,  6,  4,   1;",
				"  5, 10, 10,   5,  1;",
				"  6, 16, 22,  16,  6,  1;",
				"  7, 23, 43,  43, 23,  7, 1;",
				"  8, 32, 77, 106, 77, 32, 8, 1;",
				"  ..."
			],
			"program": [
				"(Sage) # Fripertinger's method to find the g.f. of column k \u003e= 2 (for small k):",
				"def A034356col(k, length):",
				"    R = PowerSeriesRing(ZZ, 'x', default_prec=length)",
				"    x = R.gen().O(length)",
				"    G1 = PSL(k, GF(2))",
				"    G2 = PSL(k-1, GF(2))",
				"    D1 = G1.cycle_index()",
				"    D2 = G2.cycle_index()",
				"    f1 = sum(i[1]*prod(1/(1-x^j) for j in i[0]) for i in D1)",
				"    f2 = sum(i[1]*prod(1/(1-x^j) for j in i[0]) for i in D2)",
				"    f = (f1 - f2)/(1-x)",
				"    return f.list()",
				"# For instance the Taylor expansion for column k = 4 gives",
				"print(A034356col(4, 30)) # _Petros Hadjicostas_, Oct 07 2019"
			],
			"xref": [
				"This is A076831 with the k=0 column omitted.",
				"Columns include A000027 (k=1), A034198 (k=2), A034357 (k=3), A034358 (k=4), A034359 (k=5), A034360 (k=6), A034361 (k=7), A034362 (k=8).",
				"Cf. A034253, A034254, A034328."
			],
			"keyword": "tabl,nonn",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"references": 23,
			"revision": 35,
			"time": "2020-03-14T17:38:48-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}