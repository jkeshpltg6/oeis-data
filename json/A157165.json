{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A157165",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 157165,
			"data": "1,19,734,16294,557407,81759221,1083213812,3737624804,1221606572113,4687376819963,543445163726882,314646975551939566,4747879086124761619,415213127253949396153,374983405094739446762072,11671625151617599366571432,11713911632041456348356827",
			"name": "Numerators of partial sums of a series related to Lebesgue's constant L(0) = 1.",
			"comment": [
				"For the denominators see A157166.",
				"Lebesgue's constants L(n):= (2/Pi)*int(|sin((2*n+1)*x)|/sin(x),x=0..Pi/2). (Called rho_n in the Szego reference). L(0) = 1.",
				"1 = L(0) = (16/(Pi^2))*sum(Theta(1,k)/(4*k^2-1),k \u003e= 1) with Theta(1,k) = sum(1/(2*j-1),j=1..k) = int(((sin(k*x))^2)/sin(x),x=0..Pi/2) (see Szego reference formula (R), p.165 and the line before this).",
				"The rationals (partial sums) R(0;n) = 3*sum(Theta(1,k)/(4*k^2-1),k=1..n) give (in lowest terms) A157165(n)/A157166(n). The sequence {R(0;n)/3} converges slowly to (Pi^2)/16, approximately 0.6168502752 because L(0)=1 (see the W. Lang link for R(0;10^n)/3 for n=0..4)."
			],
			"link": [
				"Wolfdieter Lang, \u003ca href=\"/A157165/a157165.txt\"\u003eSequences related to Lebesgue's constants.\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/LebesgueConstants.html\"\u003eLebesgue Constants\u003c/a\u003e",
				"G. Szego, \u003ca href=\"http://dx.doi.org/10.1007/BF01667326\"\u003eÜber die Lebesgueschen Konstanten bei den Fourierschen Reihen\u003c/a\u003e, Math. Z. 9 (1921) 163-166."
			],
			"formula": [
				"a(n) = numerator(R(0;n)) = numerator(3*sum(Theta(1,k)/(4*k^2-1),k=1..n)), n\u003e=1, with Theta(1,k) defined above."
			],
			"example": [
				"Rationals R(0;n) = A157165(n)/ A157166(n): [1, 19/15, 734/525, 16294/11025, 557407/363825, 81759221/52026975...]."
			],
			"mathematica": [
				"theta[1, k_] := Sum[1 / (2j-1), {j, 1, k}]; a[n_] := Numerator[ 3*Sum[theta[1, k]/(4k^2 - 1), {k, 1, n}]]; Table[a[n], {n, 1, 17}]  (* _Jean-François Alcover_, Nov 03 2011, after given formula *)"
			],
			"xref": [
				"A157167/A157168 for 45*((Pi^2)/16)*L(1) partial sums."
			],
			"keyword": "nonn,frac,easy",
			"offset": "1,2",
			"author": "_Wolfdieter Lang_, Oct 16 2009, Nov 24 2009",
			"references": 5,
			"revision": 18,
			"time": "2019-08-29T16:01:22-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}