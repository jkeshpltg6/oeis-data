{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A298736",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 298736,
			"data": "6,10,26,90,88,84,82,200,282,280,522,518,516,512,942,936,934,928,924,922,2566,2562,2556,2548,2544,2542,5268,5266,5262,5248,5244,5238,5236,7280,7278,7272,7266,7262,7256,43356,43354,43344,43342,43338,43336,43324,54024",
			"name": "a(n) = s(n) - prime(n+1)+3, where s(n) = smallest even number x \u003e prime(n) such that the difference x-p is composite for all primes p \u003c= prime(n).",
			"comment": [
				"The statement \"a(n) \u003e= 0 for n \u003e= 1\" is equivalent to Goldbach's conjecture (cf. Phong, Dongdong, 2004, Theorem (a)).",
				"Records: 6, 10, 26, 90, 200, 282, 522, 942, 2566, 5268, 7280, 43356, 54024, ..., . - _Robert G. Wilson v_, Feb 28 2018"
			],
			"link": [
				"Robert G. Wilson v, \u003ca href=\"/A298736/b298736.txt\"\u003eTable of n, a(n) for n = 1..1205\u003c/a\u003e",
				"Bui Minh Phong and Li Dongdong, \u003ca href=\"http://emis.matem.unam.mx/journals/AMI/2004.htm\"\u003eElementary problems which are equivalent to the Goldbach's Conjecture\u003c/a\u003e, Acta Academiae Paedagogicae Agriensis, Sectio Mathematicae 31 (2004) 33-37.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Goldbach%27s_conjecture\"\u003eGoldbach's conjecture\u003c/a\u003e",
				"\u003ca href=\"/index/Go#Goldbach\"\u003eIndex entries for sequences related to Goldbach conjecture\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A152522(n)-A000040(n+1)+3 for n \u003e 0."
			],
			"maple": [
				"N:= 100: # to get a(1)..a(N)",
				"P:= [seq(ithprime(i),i=1..N+1)]:",
				"s:= proc(n,k0) local k;",
				"  for k from max(k0,P[n]+1) by 2 do",
				"    if andmap(not(isprime), map(t -\u003e k - t, P[1..n])) then return k",
				"  fi",
				"od",
				"end proc:",
				"K[1]:= 6: A[1]:= 6:",
				"for n from 2 to N do",
				"  K[n]:= s(n,K[n-1]);",
				"  A[n]:= K[n]- P[n+1]+3;",
				"od:",
				"seq(A[n],n=1..N); # _Robert Israel_, Mar 01 2018"
			],
			"mathematica": [
				"f[n_] := Block[{k, x = 2, q = Prime@ Range@ n}, x += Mod[x, 2]; While[k = 1; While[k \u003c n +1 \u0026\u0026 CompositeQ[x - q[[k]]], k++]; k \u003c n +1, z = x += 2]; x - Prime[n +1] +3]; Array[f, 47] (* _Robert G. Wilson v_, Feb 26 2018 *)"
			],
			"program": [
				"(PARI) s(n) = my(p=prime(n), x); if(p==2, x=4, x=p+1); while(1, forprime(q=1, p, if(ispseudoprime(x-q), break, if(q==p, return(x)))); x=x+2)",
				"a(n) = s(n)-prime(n+1)+3"
			],
			"xref": [
				"Cf. A000040, A082467, A152522, A242165."
			],
			"keyword": "look,nonn",
			"offset": "1,1",
			"author": "_Felix Fröhlich_, Jan 25 2018",
			"references": 1,
			"revision": 31,
			"time": "2018-03-04T23:42:36-05:00",
			"created": "2018-03-04T23:42:36-05:00"
		}
	]
}