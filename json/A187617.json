{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A187617",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 187617,
			"data": "1,1,1,1,2,1,1,5,5,1,1,13,36,13,1,1,34,281,281,34,1,1,89,2245,6728,2245,89,1,1,233,18061,167089,167089,18061,233,1,1,610,145601,4213133,12988816,4213133,145601,610,1",
			"name": "Array T(m,n) read by antidiagonals: number of domino tilings of the 2m X 2n grid (m\u003e=0, n\u003e=0).",
			"comment": [
				"A099390 is the main entry for this problem.",
				"The even-indexed rows and columns of the square array in A187596.",
				"Row (and column) 2 is given by A122367. - _Nathaniel Johnston_, Mar 22 2011"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A187617/b187617.txt\"\u003eAntidiagonals n = 0..26, flattened\u003c/a\u003e",
				"N. Allegra, \u003ca href=\"http://arxiv.org/abs/1410.4131\"\u003eExact solution of the 2d dimer model: Corner free energy, correlation functions and combinatorics\u003c/a\u003e, arXiv:1410.4131 [cond-mat.stat-mech], 2014. See Table 1.",
				"Laura Florescu, Daniela Morar, David Perkinson, Nicholas Salter and Tianyuan Xu, \u003ca href=\"https://doi.org/10.37236/4472\"\u003eSandpiles and Dominos\u003c/a\u003e, El. J. Comb., 22 (2015), P1.66. See Theorem 15.",
				"\u003ca href=\"/index/Do#domino\"\u003eIndex entries for sequences related to dominoes\u003c/a\u003e"
			],
			"example": [
				"The array begins:",
				"  1,  1,     1,       1,          1,            1, ...",
				"  1,  2,     5,      13,         34,           89, ...",
				"  1,  5,    36,     281,       2245,        18061, ...",
				"  1, 13,   281,    6728,     167089,      4213133, ...",
				"  1, 34,  2245,  167089,   12988816,   1031151241, ...",
				"  1, 89, 18061, 4213133, 1031151241, 258584046368, ..."
			],
			"maple": [
				"ft:=(m,n)-\u003e",
				"2^(m*n/2)*mul( mul(",
				"(cos(Pi*i/(n+1))^2+cos(Pi*j/(m+1))^2), j=1..m/2), i=1..n/2);",
				"T:=(m,n)-\u003eround(evalf(ft(m,n),300));"
			],
			"mathematica": [
				"T[m_, n_] := Product[2(2 + Cos[(2j Pi)/(2m+1)] + Cos[(2k Pi)/(2n+1)]), {j, 1, m}, {k, 1, n}];",
				"Table[T[m-n, n] // Round, {m, 0, 8}, {n, 0, m}] // Flatten (* _Jean-François Alcover_, Aug 05 2018 *)"
			],
			"program": [
				"(PARI) default(realprecision, 120);",
				"{T(n, k) = round(prod(a=1, n, prod(b=1, k, 4*cos(a*Pi/(2*n+1))^2+4*cos(b*Pi/(2*k+1))^2)))} \\\\ _Seiichi Manyama_, Jan 09 2021"
			],
			"xref": [
				"A187618 is the triangle version.",
				"Cf. A187596, A099390, A348566.",
				"Main diagonal is A004003. Second and third rows give A001519, A188899."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_, Mar 11 2011",
			"ext": [
				"More terms from _Nathaniel Johnston_, Mar 22 2011"
			],
			"references": 11,
			"revision": 48,
			"time": "2021-10-22T23:47:25-04:00",
			"created": "2011-03-11T23:53:14-05:00"
		}
	]
}