{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A215406",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 215406,
			"data": "0,0,1,1,1,1,1,1,1,1,2,2,3,3,3,3,1,1,1,1,2,2,2,2,3,3,3,3,3,3,3,3,2,2,2,2,2,2,3,3,3,3,4,4,5,5,5,5,5,5,6,6,7,7,7,7,8,8,8,8,8,8,8,8,2,2,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,4,4,4",
			"name": "A ranking algorithm for the lexicographic ordering of the Catalan families.",
			"comment": [
				"See Antti Karttunen's code in A057117. Karttunen writes: \"Maple procedure CatalanRank is adapted from the algorithm 3.23 of the CAGES (Kreher and Stinson) book.\"",
				"For all n\u003e0, a(A014486(n)) = n = A080300(A014486(n)). The sequence A080300 differs from this one in that it gives 0 for those n which are not found in A014486. - _Antti Karttunen_, Aug 10 2012"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A215406/b215406.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"A. Karttunen, \u003ca href=\"http://www.iki.fi/~kartturi/matikka/tab9766.htm\"\u003eCatalan's Triangle and ranking of Mountain Ranges\u003c/a\u003e",
				"D. L. Kreher and D. R. Stinson, \u003ca href=\"http://www.math.mtu.edu/~kreher/cages.html\"\u003eCombinatorial Algorithms, Generation, Enumeration and Search\u003c/a\u003e, CRC Press, 1998.",
				"F. Ruskey, \u003ca href=\"http://www.cs.uvic.ca/~ruskey/Publications/Thesis/Thesis.html\"\u003eAlgorithmic Solution of Two Combinatorial Problems\u003c/a\u003e, Thesis, Department of Applied Physics and Information Science, University of Victoria, 1978."
			],
			"maple": [
				"A215406 := proc(n) local m,a,y,t,x,u,v;",
				"m := iquo(A070939(n), 2);",
				"a := A030101(n);",
				"y := 0; t := 1;",
				"for x from 0 to 2*m-2 do",
				"    if irem(a, 2) = 1 then y := y + 1",
				"    else u := 2*m - x;",
				"         v := m-1 - iquo(x+y,2);",
				"         t := t + A037012(u,v);",
				"         y := y - 1 fi;",
				"    a := iquo(a, 2) od;",
				"A014137(m) - t end:",
				"seq(A215406(i),i=0..199); # _Peter Luschny_, Aug 10 2012"
			],
			"mathematica": [
				"A215406[n_] := Module[{m, d, a, y, t, x, u, v}, m = Quotient[Length[d = IntegerDigits[n, 2]], 2]; a = FromDigits[Reverse[d], 2]; y = 0; t = 1; For[x = 0, x \u003c= 2*m - 2, x++, If[Mod[a, 2] == 1, y++, u = 2*m - x; v = m - Quotient[x + y, 2] - 1; t = t - Binomial[u - 1, v - 1] + Binomial[u - 1, v]; y--]; a = Quotient[a, 2]]; (1 - I*Sqrt[3])/2 - 4^(m + 1)*Gamma[m + 3/2]*Hypergeometric2F1[1, m + 3/2, m + 3, 4]/(Sqrt[Pi]*Gamma[m + 3]) - t]; Table[A215406[n] // Simplify, {n, 0, 86}] (* _Jean-François Alcover_, Jul 25 2013, translated and adapted from Peter Luschny's Maple program *)"
			],
			"program": [
				"(Sage)",
				"def A215406(n) : # CatalanRankGlobal(n)",
				"    m = A070939(n)//2",
				"    a = A030101(n)",
				"    y = 0; t = 1",
				"    for x in (1..2*m-1) :",
				"        u = 2*m - x; v = m - (x+y+1)/2",
				"        mn = binomial(u, v) - binomial(u, v-1)",
				"        t += mn*(1 - a%2)",
				"        y -= (-1)^a",
				"        a = a//2",
				"    return A014137(m) - t"
			],
			"xref": [
				"Cf. A213704, A057117, A057164, A057505, A057506, A057501, A057502, A057511, A057512, A057123, A057117, A057509, A057510, A057161, A057162, A072766, A071654, A071652, A075161, A061856, A072635, A072788, A057518, A075168, A080119, A057120, A072773."
			],
			"keyword": "nonn,look",
			"offset": "0,11",
			"author": "_Peter Luschny_, Aug 09 2012",
			"references": 4,
			"revision": 31,
			"time": "2017-02-01T12:31:06-05:00",
			"created": "2012-08-09T22:11:43-04:00"
		}
	]
}