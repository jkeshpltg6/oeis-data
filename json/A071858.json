{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A071858",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 71858,
			"data": "0,1,1,2,1,2,2,0,1,2,2,0,2,0,0,1,1,2,2,0,2,0,0,1,2,0,0,1,0,1,1,2,1,2,2,0,2,0,0,1,2,0,0,1,0,1,1,2,2,0,0,1,0,1,1,2,0,1,1,2,1,2,2,0,1,2,2,0,2,0,0,1,2,0,0,1,0,1,1,2,2,0,0,1,0,1,1,2,0,1,1,2,1,2,2,0,2,0,0,1,0,1,1,2,0",
			"name": "(Number of 1's in binary expansion of n) mod 3.",
			"comment": [
				"This is the generalized Thue-Morse sequence t_3 (Allouche and Shallit, p. 335).",
				"Ternary sequence which is a fixed point of the morphism 0 -\u003e 01, 1 -\u003e 12, 2 -\u003e 20.",
				"Sequence is T^(infty)(0) where T is the operator acting on any word on alphabet {0,1,2} by inserting 1 after 0, 2 after 1 and 0 after 2. For instance T(001)=010112, T(120)=122001. - _Benoit Cloitre_, Mar 02 2009"
			],
			"reference": [
				"J.-P. Allouche and J. Shallit, Automatic Sequences, Cambridge Univ. Press, 2003."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A071858/b071858.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Jin Chen, Zhixiong Wen, Wen Wu, \u003ca href=\"https://arxiv.org/abs/1802.03610\"\u003eOn the additive complexity of a Thue-Morse like sequence\u003c/a\u003e, arXiv:1802.03610 [math.CO], 2018.",
				"\u003ca href=\"/index/Fi#FIXEDPOINTS\"\u003eIndex entries for sequences that are fixed points of mappings\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A010872(A000120(n)).",
				"Recurrence: a(2*n) = a(n), a(2*n+1) = (a(n)+1) mod 3.",
				"a(n) = A000695(n)(mod 3). - _John M. Campbell_, Jul 16 2016"
			],
			"mathematica": [
				"f[n_] := Mod[ Count[ IntegerDigits[n, 2], 1], 3]; Table[ f[n], {n, 0, 104}] (* Or *)",
				"Nest[ Flatten[ # /. {0 -\u003e {0, 1}, 1 -\u003e {1, 2}, 2 -\u003e {2, 0}}] \u0026, {0}, 7] (* _Robert G. Wilson v_ Mar 03 2005, modified May 17 2014 *)",
				"Table[Mod[DigitCount[n,2,1],3],{n,0,110}] (* _Harvey P. Dale_, Jul 01 2015 *)"
			],
			"program": [
				"(PARI) for(n=1,200,print1(sum(i=1,length(binary(n)), component(binary(n),i))%3,\",\"))",
				"(PARI) map(d)=if(d==2,[2,0],if(d==1,[1,2],[0,1]))",
				"{m=53;v=[];w=[0];while(v!=w,v=w;w=[];for(n=1,min(m,length(v)),w=concat(w,map(v[n]))));for(n=1,2*m,print1(v[n],\",\"))} \\\\ _Klaus Brockhaus_, Jun 23 2004"
			],
			"xref": [
				"Cf. A000120, A010872.",
				"Cf. A010060, A001285, A010059, A048707, A096271, A100619, A179868.",
				"See A245555 for another version."
			],
			"keyword": "nonn,easy",
			"offset": "0,4",
			"author": "_Benoit Cloitre_, Jun 09 2002",
			"ext": [
				"Edited by _Ralf Stephan_, Dec 11 2004"
			],
			"references": 7,
			"revision": 39,
			"time": "2019-01-03T03:47:25-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}