{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A322457",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 322457,
			"data": "1,3,4,6,10,12,9,11,15,17,21,27,16,18,22,24,28,34,36,38,40,48,25,27,31,33,37,43,45,47,49,55,57,59,61,75,36,38,42,44,48,54,56,58,60,66,68,70,72,78,80,84,86,90,108,49,51,55,57,61,67,69,71,73,79,81",
			"name": "Irregular triangle: Row n contains numbers k that have recursively symmetrical partitions having Durfee square with side length n.",
			"comment": [
				"For all n, n^2 \u003c= k \u003c= 3*n^2.",
				"For n \u003e 5, some k may have more than 1 recursively self-conjugate partitions in the same row. For example, k = 90 in row 6 has two recursively self-conjugate partitions (RSCPs) with Durfee square of 6: (12,12,12,9,9,9,6,6,6,3,3,3) and (12,11,11,11,11,7,6,5,5,5,5,1). These RSCPs can be defined by dendritically laying out squares in the series {6,3,3} and {6,5,1} respectively."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A322457/b322457.txt\"\u003eTable of n, a(n) for n = 1..10391\u003c/a\u003e (rows 1 \u003c= n \u003c= 36, flattened)",
				"Michael De Vlieger, \u003ca href=\"/A322457/a322457.png\"\u003eIllustration for A322457\u003c/a\u003e",
				"Michael De Vlieger, \u003ca href=\"/A322457/a322457_1.png\"\u003eAnnotated plot of k \u003c= 1200 in rows n \u003c= 34\u003c/a\u003e, vertical exaggeration 12x."
			],
			"formula": [
				"First term of row n = n^2 = A000290(n).",
				"Last term of row n = 3*n^2 = 3*A000290(n)."
			],
			"example": [
				"Triangle begins:",
				"Row 1:   1,  3;",
				"Row 2:   4,  6, 10, 12;",
				"Row 3:   9, 11, 15, 17, 21, 27;",
				"Row 4:  16, 18, 22, 24, 28, 34, 36, 38, 40, 48;",
				"        ...",
				"Row 2 contains the following recursively self-conjugate partitions with Durfee square with side length 2. Below are diagrams that place {2^0, 2^1, 2^2, ... 2^(m-1)} squares of side lengths in S = {k_1, k_2, k_3, ..., k_m}:",
				"(2,2), sum 4, or in terms of squares, {2}:",
				"   11",
				"   11;",
				"(3,2,1), sum 6, or in terms of squares, {2,1}:",
				"   112",
				"   11",
				"   2;",
				"(4,3,2,1), sum 10, or in terms of squares, {2,1,1}:",
				"   1123",
				"   113",
				"   23",
				"   3;",
				"(4,4,2,2), sum 12, or in terms of squares, {2,2}:",
				"   1122",
				"   1122",
				"   22",
				"   22."
			],
			"mathematica": [
				"f[n_] := Block[{w = {n}, c}, c[x_] := Apply[Times, Most@ x - Reverse@ Accumulate@ Reverse@ Rest@ x]; Reap[Do[Which[And[Length@ w == 2, SameQ @@ w], Sow[w]; Break[], Length@ w == 1, Sow[w]; AppendTo[w, 1], c[w] \u003e 0, Sow[w]; AppendTo[w, 1], True, Sow[w]; w = MapAt[1 + # \u0026, Drop[w, -1], -1] ], {i, Infinity}] ][[-1, 1]] ]; Array[Union@ Map[Total@ MapIndexed[#1^2*2^First[#2 - 1] \u0026, #] \u0026, f[#]] \u0026, 7] // Flatten"
			],
			"xref": [
				"Cf.: A190899, A190900, A321223, A322156."
			],
			"keyword": "nonn,tabf,easy",
			"offset": "1,2",
			"author": "_Michael De Vlieger_, Dec 11 2018",
			"references": 4,
			"revision": 10,
			"time": "2018-12-31T02:42:24-05:00",
			"created": "2018-12-26T15:07:16-05:00"
		}
	]
}