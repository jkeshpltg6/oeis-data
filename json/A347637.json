{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A347637",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 347637,
			"data": "7,13,15,9,21,21,15,17,35,27,11,25,25,37,33,17,21,41,33,59,39,13,29,31,45,41,53",
			"name": "Table read by ascending antidiagonals. T(n, k) is the minimum number of pebbles such that any assignment of those pebbles on a complete graph with n vertices is a next-player winning game in the two-player impartial (k+1, k) pebbling game. T(n, k) for n \u003e= 5 and k \u003e= 1.",
			"comment": [
				"A (k+1, k) pebbling move involves removing k + 1 pebbles from a vertex in a simple graph and placing k pebbles on an adjacent vertex.",
				"A two-player impartial (k+1, k) pebbling game involves two players alternating (k+1, k) pebbling moves. The first player unable to make a move loses.",
				"T(3, k) = A016921(k) for k \u003e= 0. The proof will appear in a paper that is currently in preparation.",
				"It is conjectured that T(4, k) for odd k\u003e=3 is infinite, so we start with n = 5.",
				"T(5, k) = A346197(k) for k \u003e= 1.",
				"T(n, 1) = A340631(n) for n \u003e= 3.",
				"T(n, 2) = A346401(n) for n \u003e= 3."
			],
			"reference": [
				"E. R. Berlekamp, J. H. Conway, and R. K. Guy, Winning Ways for Your Mathematical Plays, Vol. 1, CRC Press, 2001."
			],
			"link": [
				"Eugene Fiorini, Max Lind, Andrew Woldar, and Tony W. H. Wong, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL24/Wong/wong31.html\"\u003eCharacterizing Winning Positions in the Impartial Two-Player Pebbling Game on Complete Graphs\u003c/a\u003e, J. Int. Seq., Vol. 24 (2021), Article 21.6.4."
			],
			"example": [
				"The data is organized in a table beginning with row n = 5 and column k = 1. The data is read by ascending antidiagonals. The formula binomial(n + k - 5, 2) + k converts the indices from table form to sequence form.",
				"The table T(n, k) begins:",
				"[n/k]  1   2   3   4   5   6  ...",
				"---------------------------------",
				"[ 5]   7, 15, 21, 27, 33, 39, ...",
				"[ 6]  13, 21, 35, 37, 59, 53, ...",
				"[ 7]   9, 17, 25, 33, 41, 51, ...",
				"[ 8]  15, 25, 41, 45, 61, ...",
				"[ 9]  11, 21, 31, 41, 51, ...",
				"[10]  17, 29, 45, 53, 71, ...",
				"[11]  13, 25, 37, 49, 61, ...",
				"[12]  19, 33, 51, ...",
				"[13]  15, 29, 43, ...",
				"[14]  21, 37, ...",
				"[15]  17, 33, ...",
				"[16]  23, 41, ..."
			],
			"mathematica": [
				"(* m represents number of vertices in the complete graph. Each pebbling move removes k+1 pebbles from a vertex and adds k pebbles to an adjacent vertex. *)",
				"Do[(* Given m and a, list all possible assignments with a pebbles. *)",
				"alltuples[m_, a_] := IntegerPartitions[a + m, {m}] - 1;",
				"(* Given an assignment, list all resultant assignments after one pebbling move; only works for m\u003e=3. *)",
				"pebblemoves[config_] :=",
				"  Block[{m, temp}, m = Length[config];",
				"   temp = Table[config, {i, m (m - 1)}] +",
				"     Permutations[Join[{-(k + 1), k}, Table[0, {i, m - 2}]]];",
				"   temp = Select[temp, Min[#] \u003e= 0 \u0026];",
				"   temp = ReverseSort[DeleteDuplicates[ReverseSort /@ temp]]];",
				"(* Given m and a, list all assignments that are P-games. *)",
				"Plist = {};",
				"plist[m_, a_] :=",
				"  Block[{index, tuples},",
				"   While[Length[Plist] \u003c m, index = Length[Plist];",
				"    AppendTo[Plist, {{Join[{1}, Table[0, {i, index}]]}}]];",
				"   Do[AppendTo[Plist[[m]], {}]; tuples = alltuples[m, i];",
				"    Do[If[",
				"      Not[IntersectingQ[pebblemoves[tuples[[j]]],",
				"        If[i \u003e 2, Plist[[m, i - 1]], {}]]],",
				"      AppendTo[Plist[[m, i]], tuples[[j]]]], {j, Length[tuples]}], {i,",
				"      Length[Plist[[m]]] + 1, a}]; Plist[[m, a]]];",
				"(* Given m, print out the minimum a such that there are no P-games with a pebbles *)",
				"Do[a = 1; While[plist[m, a] != {}, a++];",
				"  Print[\"k=\", k, \" m=\", m, \" a=\", a], {m, 5, 10}], {k, 1, 6}]"
			],
			"xref": [
				"Cf. A340631, A346197, A346401."
			],
			"keyword": "nonn,more,tabl",
			"offset": "5,1",
			"author": "_Kayla Barker_, _Mia DeStefano_, _Eugene Fiorini_, _Michael Gohn_, _Joe Miller_, _Jacob Roeder_, _Wing Hong Tony Wong_, Sep 09 2021",
			"references": 1,
			"revision": 29,
			"time": "2021-09-25T03:47:23-04:00",
			"created": "2021-09-25T03:47:23-04:00"
		}
	]
}