{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A010892",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 10892,
			"data": "1,1,0,-1,-1,0,1,1,0,-1,-1,0,1,1,0,-1,-1,0,1,1,0,-1,-1,0,1,1,0,-1,-1,0,1,1,0,-1,-1,0,1,1,0,-1,-1,0,1,1,0,-1,-1,0,1,1,0,-1,-1,0,1,1,0,-1,-1,0,1,1,0,-1,-1,0,1,1,0,-1,-1,0,1,1,0,-1,-1,0,1,1,0,-1,-1,0,1,1,0,-1,-1,0,1,1,0,-1,-1,0,1,1,0,-1,-1,0",
			"name": "Inverse of 6th cyclotomic polynomial. A period 6 sequence.",
			"comment": [
				"Any sequence b(n) satisfying the recurrence b(n) = b(n-1) - b(n-2) can be written as b(n) = b(0)*a(n) + (b(1)-b(0))*a(n-1).",
				"a(n) is the determinant of the n X n matrix M with m(i,j)=1 if |i-j| \u003c= 1 and 0 otherwise. - Mario Catalani (mario.catalani(AT)unito.it), Jan 25 2003",
				"Also row sums of triangle in A108299; a(n)=L(n-1,1), where L is also defined as in A108299; see A061347 for L(n,-1). - _Reinhard Zumkeller_, Jun 01 2005",
				"Pisano period lengths:  1, 3, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, ... - _R. J. Mathar_, Aug 10 2012",
				"Periodic sequences of this type can also be calculated as a(n) = c + floor(q/(p^m-1)*p^n) mod p, where c is a constant, q is the number representing the periodic digit pattern and m is the period. c, p and q can be calculated as follows: Let D be the array representing the number pattern to be repeated, m = size of D, max = maximum value of elements in D, min = minimum value of elements in D. Then c := min, p := max - min + 1 and q := p^m*Sum_{i=1..m} (D(i)-min)/p^i. Example: D = (1, 1, 0, -1, -1, 0), c = -1, m = 6, p = 3 and q = 676 for this sequence. - _Hieronymus Fischer_, Jan 04 2013",
				"B(n) = a(n+5) = S(n-1, 1) appears, together with a(n) = A057079(n+1), in the formula 2*exp(Pi*n*i/3) = A(n) + B(n)*sqrt(3)*i with i = sqrt(-1). For S(n, x) see A049310. See also a Feb 27 2014 comment on A099837. - _Wolfdieter Lang_, Feb 27 2014",
				"a(n) (for n\u003e=1) is the difference between numbers of even and odd permutations p of 1,2,...,n such that |p(i)-i|\u003c=1 for i=1,2,...,n. - _Dmitry Efimov_, Jan 08 2016",
				"From _Tom Copeland_, Jan 31 2016: (Start)",
				"Specialization of the o.g.f. 1 / ((x - w1)(x-w2)) = (1/(w1-w2)) ((w1-w2) + (w1^2 - w2^2) x + (w1^3-w2^3) x^2 + ...) with w1*w2 = (1/w1) + (1/w2) = 1. Then w1 = q = e^(i*Pi/3) and w2 = 1/q = e^(-i*Pi/3), giving the o.g.f. 1 /(1-x+x^2) for this entry with a(n) = (2/sqrt(3)) sin((n+1)Pi/3). See the Copeland link for more relations.",
				"a(n) = (q^(n+1) - q^(-(n+1))) / (q - q^(-1)), so this entry gives the o.g.f. for an instance of the quantum integers denoted by [m]_q in Morrison et al. and Tingley. (End)"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A010892/b010892.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"S. Barbero, U. Cerruti, N. Murru, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL13/Barbero2/barbero7.html\"\u003eA Generalization of the Binomial Interpolated Operator and its Action on Linear Recurrent Sequences \u003c/a\u003e, J. Int. Seq. 13 (2010) # 10.9.7, eq (3).",
				"Paul Barry, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL8/Barry/barry84.html\"\u003eA Catalan Transform and Related Transformations on Integer Sequences\u003c/a\u003e, J. Integer Sequ., Vol. 8 (2005), Article 05.4.5.",
				"Tom Copeland, \u003ca href=\"http://tcjpn.wordpress.com/2015/10/12/the-elliptic-lie-triad-kdv-and-ricattt-equations-infinigens-and-elliptic-genera/\"\u003eAddendum to Elliptic Lie Triad\u003c/a\u003e",
				"Robert G. Donnelly, Molly W. Dunkum, Sasha V. Malone, and Alexandra Nance, \u003ca href=\"https://arxiv.org/abs/2012.14991\"\u003eSymmetric Fibonaccian distributive lattices and representations of the special linear Lie algebras\u003c/a\u003e, arXiv:2012.14991 [math.CO], 2020.",
				"Ralph E. Griswold, \u003ca href=\"https://www2.cs.arizona.edu/patterns/sequences/\"\u003eShaft Sequences\u003c/a\u003e, 2001.",
				"S. Morrison, E. Peters, N. Snyder, \u003ca href=\"http://arxiv.org/abs/1003.0022\"\u003eKnot polynomial identities and quantum group coincidences\u003c/a\u003e, arXiv preprint arXiv:1003.0022 [math.QA], 2014.",
				"Michael Somos, \u003ca href=\"http://grail.eecs.csuohio.edu/~somos/rfmc.txt\"\u003eRational Function Multiplicative Coefficients\u003c/a\u003e",
				"P. Tingley,  \u003ca href=\"http://arxiv.org/abs/1002.0555\"\u003eA minus sign ... (Two constructions of the Jones polynomial)\u003c/a\u003e, arXiv preprint arXiv:1002.0555v2 [math.GT], 2015.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,-1).",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e",
				"\u003ca href=\"/index/Pol#poly_cyclo_inv\"\u003eIndex to sequences related to inverse of cyclotomic polynomials\u003c/a\u003e"
			],
			"formula": [
				"G.f.: 1 / (1 - x + x^2).",
				"a(n) = a(n-1) - a(n-2), a(0)=1, a(1)=1.",
				"a(n) = ((-1)^floor(n/3) + (-1)^floor((n+1)/3))/2.",
				"a(n) = 0 if n mod 6 = 2 or 5, a(n) = +1 if n mod 6 = 0 or 1, a(n) = -1 otherwise. a(n) = S(n, 1) = U(n, 1/2) (Chebyshev U(n, x) polynomials).",
				"a(n) = sqrt(4/3)*Im((1/2 + i*sqrt(3/4))^(n+1)). - _Henry Bottomley_, Apr 12 2000",
				"Binomial transform of A057078. a(n) = Sum_{k=0..n} C(k, n-k)*(-1)^(n-k). - _Paul Barry_, Sep 13 2003",
				"a(n) = 2*sin(Pi*n/3 + Pi/3)/sqrt(3). - _Paul Barry_, Jan 28 2004",
				"a(n) = Sum_{k=0..floor(n/2)} C(n-k, k)*(-1)^k. - _Paul Barry_, Jul 28 2004",
				"Euler transform of length 6 sequence [1, -1, -1, 0, 0, 1]. - _Michael Somos_, Sep 23 2005",
				"a(n) = a(1 - n) = -a(-2 - n) for all n in Z. - _Michael Somos_, Feb 14 2006",
				"a(n) = -(1/6)*(n mod 6 + (n+1) mod 6 - ((n+3) mod 6) - ((n+4) mod 6)). - _Paolo P. Lava_, Oct 20 2006",
				"a(n) = Sum_{k=0..n} (-2)^(n-k) * A085838(n,k). - _Philippe Deléham_, Oct 26 2006",
				"a(n) = b(n+1) where b(n) is multiplicative with b(2^e) = -(-1)^e if e\u003e0, b(3^e) = 0^e, b(p^e) = 1 if p == 1 (mod 6), b(p^e) = (-1)^e if p == 5 (mod 6). - _Michael Somos_, Oct 29 2006",
				"Given g.f. A(x), then, B(x) = x * A(x) satisfies 0 = f(B(x), B(x^2)) where f(u, v) = u^2 - v - 2*u*v * (1 - u). - _Michael Somos_, Oct 29 2006",
				"a(2*n) = A057078(n), a(2*n+1) = A049347(n).",
				"a(n) = Sum_{k=0..n} A109466(n,k). - _Philippe Deléham_, Nov 14 2006",
				"a(n) = Sum_{k=0..n} A133607(n,k). - _Philippe Deléham_, Dec 30 2007",
				"a(n) = A128834(n+1). - _Jaume Oliver Lafont_, Dec 05 2008",
				"a(n) = Sum_{k=0..n} C(n+k+1,2k+1) * (-1)^k. - _Paul Barry_, Jun 03 2009",
				"a(n) = A101950(n,0) = (-1)^n * A049347(n). - _Philippe Deléham_, Feb 10 2012",
				"a(n) = Product_{k=1..floor(n/2)} 1 - 4*(cos(k*Pi/(n+1)))^2. - _Mircea Merca_, Apr 01 2012",
				"G.f.: 1 / (1 - x / (1 + x / (1 - x))). - _Michael Somos_, Apr 02 2012",
				"a(n) = -1 + floor(181/819*10^(n+1)) mod 10. - _Hieronymus Fischer_, Jan 03 2013",
				"a(n) = -1 + floor(13/14*3^(n+1)) mod 3. - _Hieronymus Fischer_, Jan 04 2013",
				"a(n) = 1/(1+r2)*(1/r1)^n + 1/(1+r1)*(1/r2)^n, with r1=(1-i*sqrt(3))/2 and r2=(1+i*sqrt(3))/2. - _Ralf Stephan_, Jul 19 2013",
				"a(n) = ((n+1)^2 mod 3) * (-1)^floor((n+1)/3). - _Wesley Ivan Hurt_, Mar 15 2015",
				"a(n-1) = n - Sum_{i=1..n-1} i*a(n-i). - _Derek Orr_, Apr 28 2015",
				"a(n) = S(2*n+1, sqrt(3))/sqrt(3) = S(n, 1) with S(n, x) coefficients given in A049310. The S(n, 1) formula appeared already above. S(2*n, sqrt(3)) = A057079(n). See also a Feb 27 2014 comment above. - _Wolfdieter Lang_, Jan 16 2018",
				"E.g.f.: sqrt(exp(x)*4/3) * cos(x*sqrt(3/4) - Pi/6). - _Michael Somos_, Jul 05 2018",
				"a(n) = Determinant(Tri(n)), for n \u003e= 1, with Tri(n) the n X n tridiagonal matrix with entries 1 (a special Toeplitz matrix). - _Wolfdieter Lang_, Sep 20 2019",
				"a(n) = Product_{k=1..n}(1 + 2*cos(k*Pi/(n+1))). - _Peter Luschny_, Nov 28 2019"
			],
			"example": [
				"G.f. = 1 + x - x^3 - x^4 + x^6 + x^7 - x^9 - x^10 + x^12 + x^13 - x^15 + ..."
			],
			"maple": [
				"a:=n-\u003ecoeftayl(1/(x^2-x+1), x=0, n);",
				"a:=n-\u003e2*sin(Pi*(n+1)/3)/sqrt(3);",
				"A010892:=n-\u003e[1,1,0,-1,-1,0][irem(n,6)+1];",
				"A010892:=n-\u003eArray(0..5,[1,1,0,-1,-1,0])[irem(n,6)];",
				"A010892:=n-\u003etable([0=1,1=1,2=0,3=-1,4=-1,5=0])[irem(n,6)];",
				"with(numtheory,cyclotomic); c := series(1/cyclotomic(6,x),x,102): seq(coeff(c,x,n),n=0..101); # _Rainer Rosenthal_, Jan 01 2007"
			],
			"mathematica": [
				"a[n_] := {1, 1, 0, -1, -1, 0}[[Mod[n, 6] + 1]]; Table[a[n], {n, 0, 101}] (* _Jean-François Alcover_, Jul 19 2013 *)",
				"CoefficientList[Series[1/Cyclotomic[6, x], {x, 0, 100}], x] (* _Vincenzo Librandi_, Apr 03 2014 *)",
				"PadRight[{},120,{1,1,0,-1,-1,0}] (* _Harvey P. Dale_, Jul 07 2020 *)"
			],
			"program": [
				"(PARI) {a(n) = (-1)^(n\\3) * sign((n + 1)%3)}; /* _Michael Somos_, Sep 23 2005 */",
				"(PARI) {a(n) = subst( poltchebi(n) + poltchebi(n-1), 'x, 1/2) * 2/3}; /* _Michael Somos_, Sep 23 2005 */",
				"(PARI) {a(n) = [1, 1, 0, -1, -1, 0][n%6 + 1]}; /* _Michael Somos_, Feb 14 2006",
				"(PARI) {a(n) = my(A, p, e); if( n\u003c0, 0, n++; A = factor(n); prod( k=1, matsize(A)[1], [p, e] = A[k, ]; if( p==2, -(-1)^e, p==3, 0, p%6 == 1, 1, (-1)^e)))}; /* _Michael Somos_, Oct 29 2006 */",
				"(Python)",
				"def A010892(n): return [1,1,0,-1,-1,0][n%6] # Alec Mihailovs, Jan 01 2007",
				"(Sage) [lucas_number1(n,1,+1) for n in range(-5, 97)] # _Zerinvary Lajos_, Apr 22 2009",
				"(Sage)",
				"def A010892():",
				"    x, y = -1, -1",
				"    while True:",
				"        yield -x",
				"        x, y = y, -x + y",
				"a = A010892()",
				"[next(a) for i in range(40)]  # _Peter Luschny_, Jul 11 2013",
				"(MAGMA) \u0026cat[[1,1,0,-1,-1,0]: n in [0..20]]; // _Vincenzo Librandi_, Apr 03 2014"
			],
			"xref": [
				"a(n) = row sums of signed triangle A049310.",
				"Cf. A049347, A057078.",
				"Differs only by a shift from A128834.",
				"a(n+1) = row sums of triangle A130777: repeat(1,0,-1,-1,0,1)."
			],
			"keyword": "sign,easy",
			"offset": "0,1",
			"author": "_Simon Plouffe_",
			"ext": [
				"Entry revised by _N. J. A. Sloane_, Jul 16 2004"
			],
			"references": 135,
			"revision": 186,
			"time": "2021-04-13T18:41:05-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}