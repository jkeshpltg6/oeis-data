{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A124422",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 124422,
			"data": "1,1,1,1,3,2,5,8,2,22,25,5,52,101,45,5,283,423,156,15,855,1889,1143,238,15,5451,9726,5002,916,52,19921,48382,35805,10540,1275,52,144074,292223,187515,49155,5400,203,614866,1609551,1379753,512710,89425,7089,203",
			"name": "Triangle read by rows: T(n,k) is the number of partitions of the set {1,2,...,n}, having exactly k blocks consisting only even entries (0\u003c=k\u003c=floor(n/2)).",
			"comment": [
				"Row n has 1+floor(n/2) terms. Row sums are the Bell numbers (A000110). T(2n-1,n-1) = T(2n,n) = A000110(n) (the Bell numbers). T(n,0) = A124423(n)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A124422/b124422.txt\"\u003eRows n = 0..150, flattened\u003c/a\u003e"
			],
			"formula": [
				"The generating polynomial of row n is P[n](s)=Q[n](1,s,1), where the polynomials Q[n]=Q[n](t,s,x) are defined by Q[0]=1; Q[n]=t*dQ[n-1]/dt + x*dQ[n-1]/ds + x*dQ[n-1]/dx + t*Q[n-1] if n is odd and Q[n]=x*dQ[n-1]/dt + s*dQ[n-1]/ds + x*dQ[n-1]/dx + s*Q[n-1] if n is even."
			],
			"example": [
				"T(4,1) = 8 because we have 134|2, 13|24, 14|2|3, 1|24|3, 1|2|34, 123|4, 1|23|4 and 12|3|4.",
				"Triangle starts:",
				"1;",
				"1;",
				"1,    1;",
				"3,    2;",
				"5,    8,  2;",
				"22,  25,  5;",
				"52, 101, 45, 5;"
			],
			"maple": [
				"Q[0]:=1: for n from 1 to 13 do if n mod 2 = 1 then Q[n]:=expand(t*diff(Q[n-1],t)+x*diff(Q[n-1],s)+x*diff(Q[n-1],x)+t*Q[n-1]) else Q[n]:=expand(x*diff(Q[n-1],t)+s*diff(Q[n-1],s)+x*diff(Q[n-1],x)+s*Q[n-1]) fi od: for n from 0 to 13 do P[n]:=sort(subs({t=1,x=1},Q[n])) od: for n from 0 to 13 do seq(coeff(P[n],s,j),j=0..floor(n/2)) od; # yields sequence in triangular form",
				"# second Maple program:",
				"T:= proc(n, k) local g, u; g:= floor(n/2); u:=ceil(n/2);",
				"      add(Stirling2(i, k)*binomial(g, i)*",
				"      add(Stirling2(u, j)*j^(g-i), j=0..u), i=k..g)",
				"    end:",
				"seq(seq(T(n,k), k=0..floor(n/2)), n=0..15); # _Alois P. Heinz_, Oct 23 2013"
			],
			"mathematica": [
				"Unprotect[Power]; 0^0 = 1; T[n_, k_] := Module[{g = Floor[n/2], u = Ceiling[n/2]},  Sum[StirlingS2[i, k]*Binomial[g, i]*Sum[StirlingS2[u, j]*j^(g-i), {j, 0, u}], {i, k, g}]]; Table[Table[T[n, k], {k, 0, Floor[n/2]}], {n, 0, 15}] // Flatten (* _Jean-François Alcover_, May 22 2015, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A000110, A124418, A124419, A124420, A124421, A124423."
			],
			"keyword": "nonn,tabf",
			"offset": "0,5",
			"author": "_Emeric Deutsch_, Oct 31 2006",
			"references": 8,
			"revision": 16,
			"time": "2016-06-27T17:17:07-04:00",
			"created": "2006-12-06T03:00:00-05:00"
		}
	]
}