{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006629",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6629,
			"id": "M3542",
			"data": "1,4,18,88,455,2448,13566,76912,444015,2601300,15426840,92431584,558685348,3402497504,20858916870,128618832864,797168807855,4963511449260,31032552351570,194743066471800,1226232861415695",
			"name": "Self-convolution 4th power of A001764, which enumerates ternary trees.",
			"comment": [
				"Sum of root degrees of all noncrossing trees on nodes on a circle. - _Emeric Deutsch_"
			],
			"reference": [
				"H. M. Finucan, Some decompositions of generalized Catalan numbers, pp. 275-293 of Combinatorial Mathematics IX. Proc. Ninth Australian Conference (Brisbane, August 1981). Ed. E. J. Billington, S. Oates-Williams and A. P. Street. Lecture Notes Math., 952. Springer-Verlag, 1982.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A006629/b006629.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"Emanuele Munarini, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL20/Munarini/muna4.html\"\u003eShifting Property for Riordan, Sheffer and Connection Constants Matrices\u003c/a\u003e, Journal of Integer Sequences, Vol. 20 (2017), Article 17.8.2.",
				"Joris Nieuwveld, \u003ca href=\"https://arxiv.org/abs/2108.11382\"\u003eFractions, Functions and Folding. A Novel Link between Continued Fractions, Mahler Functions and Paper Folding\u003c/a\u003e, Master's Thesis, arXiv:2108.11382 [math.NT], 2021.",
				"C. H. Pah, \u003ca href=\"http://dx.doi.org/10.1007/s10955-010-9989-5\"\u003eSingle polygon counting on Cayley Tree of order 3\u003c/a\u003e, J. Stat. Phys. 140 (2010) 198-207.",
				"\u003ca href=\"/index/Ro#rooted\"\u003eIndex entries for sequences related to rooted trees\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 2*binomial(3*n+3,n)/(n+2). - _Emeric Deutsch_",
				"G.f.: 3_F_2 ( [ 2, 5/3, 4/3 ]; [ 3, 5/2 ]; 27 x / 4 ).",
				"G.f.: A(x) = G(x)^4 where G(x) = 1 + x*G(x)^3 = g.f. of A001764 giving a(n)=C(3n+m-1,n)*m/(2n+m) at power m=4 with offset n=0. - _Paul D. Hanna_, May 10 2008"
			],
			"program": [
				"(PARI) a(n)=my(m=4);binomial(3*n+m-1,n)*m/(2*n+m) /* 4th power of A001764 with offset n=0 */ \\\\ _Paul D. Hanna_, May 10 2008"
			],
			"xref": [
				"Column 2 of A092276."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Simon Plouffe_, _N. J. A. Sloane_",
			"ext": [
				"More precise definition from _Paul D. Hanna_, May 10 2008"
			],
			"references": 22,
			"revision": 41,
			"time": "2021-08-27T03:00:59-04:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}