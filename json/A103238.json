{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A103238",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 103238,
			"data": "1,2,2,8,6,3,52,36,12,4,480,324,96,20,5,5816,3888,1104,200,30,6,87936,58536,16320,2800,360,42,7,1601728,1064016,294048,49200,5940,588,56,8,34251520,22728384,6252288,1032800,120960,11172,896,72,9,843099616",
			"name": "Triangular matrix T, read by rows, that satisfies: T^2 + T = SHIFTUP(T), also T^(n+1) + T^n = SHIFTUP(T^n - D*T^(n-1)) for all n, where D is a diagonal matrix with diagonal(D) = diagonal(T) = {1,2,3,...}.",
			"comment": [
				"Leftmost column is A103239. The operation SHIFTUP(T) shifts each column of T up 1 row, dropping the elements that occupied the diagonal of T."
			],
			"formula": [
				"G.f. for column k: T(k, k) = k+1 = Sum_{n\u003e=k} T(n, k)*x^(n-k)/(1-x)^(n-k) * Product_{j=0..n-k} (1-(j+k+2)*x). Diagonalization: T = P*D*P^-1 where P(r, c) = A103249(r, c)/(r-c)! = (-1)^(r-c)*(c^2+c)^(r-c)/(r-c)! for r\u003e=c\u003e=1 and [P^-1](r, c) = A103244(r, c)/(r-c)! and D is a diagonal matrix = {1, 2, 3, ...}."
			],
			"example": [
				"Rows of T begin:",
				"[1],",
				"[2,2],",
				"[8,6,3],",
				"[52,36,12,4],",
				"[480,324,96,20,5],",
				"[5816,3888,1104,200,30,6],",
				"[87936,58536,16320,2800,360,42,7],",
				"[1601728,1064016,294048,49200,5940,588,56,8],...",
				"Rows of T^2 begin:",
				"[1],",
				"[6,4],",
				"[44,30,9],",
				"[428,288,84,16],",
				"[5336,3564,1008,180,25],...",
				"Then T^2 + T = SHIFTUP(T):",
				"[2],",
				"[8,6],",
				"[52,36,12],",
				"[480,324,96,20],",
				"[5816,3888,1104,200,30],...",
				"G.f. for column 0: 1 = (1-2x) + 2*x/(1-x)*(1-2x)(1-3x) + 8*x^2/(1-x)^2*(1-2x)(1-3x)(1-4x) + 52*x^3/(1-x)^3*(1-2x)(1-3x)(1-4x)(1-5x) + ... + T(n,0)*x^n/(1-x)^n*(1-2x)(1-3x)*..*(1-(n+2)x) + ...",
				"G.f. for column 1: 2 = 2*(1-3x) + 6*x/(1-x)*(1-3x)(1-4x) + 36*x^2/(1-x)^2*(1-3x)(1-4x)(1-5x) + 324*x^3/(1-x)^3*(1-3x)(1-4x)(1-5x)(1-6x) + ... + T(n,1)*x^(n-1)/(1-x)^(n-1)*(1-3x)(1-4x)*..*(1-(n+2)x) + ..."
			],
			"program": [
				"(PARI) /* Using Matrix Diagonalization Formula: */ T(n,k)=local(P,D);D=matrix(n+1,n+1,r,c,if(r==c,r)); P=matrix(n+1,n+1,r,c,if(r\u003e=c,(-1)^(r-c)*(c^2+c)^(r-c)/(r-c)!)); return(if(n\u003ck|k\u003c0,0,(P*D*P^-1)[n+1,k+1]))",
				"(PARI) /* Using Generating Function for Columns: */ T(n,k)=if(n\u003ck,0,if(n==k,k+1,polcoeff( k+1-sum(i=k,n-1,T(i,k)*x^(i-k)/(1-x)^(i-k)* prod(j=0,i-k,1-(j+k+2)*x+x*O(x^n))),n-k)))"
			],
			"xref": [
				"Cf. A103239, A103244, A103249, A103236, A103237."
			],
			"keyword": "nonn,tabl",
			"offset": "0,2",
			"author": "_Paul D. Hanna_, Jan 31 2005",
			"references": 2,
			"revision": 12,
			"time": "2015-06-14T00:55:32-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}