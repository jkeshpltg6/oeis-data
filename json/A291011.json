{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A291011",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 291011,
			"data": "4,15,52,172,552,1736,5384,16536,50440,153112,463176,1397720,4210568,12668568,38083528,114414424,343587336,1031482904,3095956040,9291013848,27879595144,83652416920,250985562312,753015407192,2259167856392,6777755227416,20333785775944",
			"name": "p-INVERT of (1,1,1,1,1,...), where p(S) = (1 - S)^2 (1 - 2 S).",
			"comment": [
				"Suppose s = (c(0), c(1), c(2),...) is a sequence and p(S) is a polynomial. Let S(x) = c(0)*x + c(1)*x^2 + c(2)*x^3 + ... and T(x) = (-p(0) + 1/p(S(x)))/x. The p-INVERT of s is the sequence t(s) of coefficients in the Maclaurin series for T(x).  Taking p(S) = 1 - S gives the \"INVERT\" transform of s, so that p-INVERT is a generalization of the \"INVERT\" transform (e.g., A033453).",
				"See A291000 for a guide to related sequences."
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A291011/b291011.txt\"\u003eTable of n, a(n) for n = 0..999\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (7, -16, 12)"
			],
			"formula": [
				"G.f.: (-4 + 13 x - 11 x^2)/((-1 + 2 x)^2 (-1 + 3 x)).",
				"a(n) = 7*a(n-1) - 16*a(n-2) + 12*a(n-3) for n \u003e= 4.",
				"a(n) = (8*3^n - 2^(n-1)*(8+n)). - _Colin Barker_, Aug 23 2017"
			],
			"mathematica": [
				"z = 60; s = x/(1 - x); p = (1 - s)^2 (1 - 2 s);",
				"Drop[CoefficientList[Series[s, {x, 0, z}], x], 1]  (* A000012 *)",
				"Drop[CoefficientList[Series[1/p, {x, 0, z}], x], 1]  (* A291011 *)",
				"LinearRecurrence[{7,-16,12},{4,15,52},30] (* _Harvey P. Dale_, Sep 23 2017 *)"
			],
			"program": [
				"(PARI) Vec((4 - 13*x + 11*x^2) / ((1 - 2*x)^2*(1 - 3*x)) + O(x^30)) \\\\ _Colin Barker_, Aug 23 2017"
			],
			"xref": [
				"Cf. A000012, A289780, A291000."
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "_Clark Kimberling_, Aug 23 2017",
			"references": 2,
			"revision": 8,
			"time": "2017-09-23T13:55:24-04:00",
			"created": "2017-08-23T16:04:10-04:00"
		}
	]
}