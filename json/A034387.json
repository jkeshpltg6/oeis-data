{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A034387",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 34387,
			"data": "0,2,5,5,10,10,17,17,17,17,28,28,41,41,41,41,58,58,77,77,77,77,100,100,100,100,100,100,129,129,160,160,160,160,160,160,197,197,197,197,238,238,281,281,281,281,328,328,328,328,328,328",
			"name": "Sum of primes \u003c= n.",
			"comment": [
				"Also sum of all prime-factors in n!.",
				"For large n, these numbers can be closely approximated by the number of primes \u003c n^2. For example, the sum of primes \u003c 10^10 = 2220822432581729238. The number of primes \u003c (10^10)^2 or 10^20 = 2220819602560918840. This has a relative error of 0.0000012743... - _Cino Hilliard_, Jun 08 2008",
				"Equals row sums of triangle A143537. - _Gary W. Adamson_, Aug 23 2008",
				"a(n) = A158662(n) - 1. a(p) - a(p-1) = p, for p = primes (A000040), a(c) - a(c-1) = 0, for c = composite numbers (A002808). - _Jaroslav Krizek_, Mar 23 2009",
				"Partial sums of A061397. - _Reinhard Zumkeller_, Mar 21 2014"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A034387/b034387.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Jens Askgaard, \u003ca href=\"https://arxiv.org/abs/1902.06299\"\u003eOn the additive period length of the Sprague-Grundy function of certain Nim-like games\u003c/a\u003e, arXiv:1902.06299 [math.CO], 2019.",
				"Cino Hilliard, \u003ca href=\"http://docs.google.com/Doc?docid=dgpq9w4b_26dtrq634m\u0026amp;hl=en\"\u003e Sum of primes \u003c/a\u003e"
			],
			"formula": [
				"From the prime number theorem a(n) has the asymptotic expression: a(n) ~ n^2 / (2 log n). - Dan Fux (dan.fux(AT)OpenGaia.com), Apr 07 2001",
				"a(n) = n^2/(2 log n) + O(n^2 log log n/log^2 n). - _Vladimir Shevelev_ and _Charles R Greathouse IV_, May 29 2014",
				"Conjecture: G.f.: Sum_{i\u003e0} Sum_{j\u003e=i} Sum_{k\u003e=j|i-j+k is prime} x^k. - _Benedict W. J. Irwin_, Mar 31 2017",
				"a(n) = (n+1)*A000720(n) - A046992(n). - _Ridouane Oudra_, Sep 18 2021"
			],
			"mathematica": [
				"s=0; Table[s=s+n*Boole[PrimeQ[n]],{n,100}] (* _Zak Seidov_, Apr 11 2011 *)",
				"Accumulate[Table[If[PrimeQ[n],n,0],{n,60}]] (* _Harvey P. Dale_, Jul 25 2016 *)"
			],
			"program": [
				"(PARI) a(n)=sum(i=1,primepi(n),prime(i)) \\\\ _Michael B. Porter_, Sep 22 2009",
				"(PARI) a=0;for(k=1,100,print1(a=a+k*isprime(k),\", \")) \\\\ _Zak Seidov_, Apr 11 2011",
				"(Haskell)",
				"a034387 n = a034387_list !! (n-1)",
				"a034387_list = scanl1 (+) a061397_list",
				"-- _Reinhard Zumkeller_, Mar 21 2014",
				"(Python)",
				"from sympy import isprime",
				"from itertools import accumulate",
				"def alist(n): return list(accumulate(k*isprime(k) for k in range(1, n+1)))",
				"print(alist(57)) # _Michael S. Branicky_, Sep 18 2021"
			],
			"xref": [
				"Cf. A007504, A158662, A073837, A066779, A034386, A000720.",
				"This is a lower bound on A287881."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_.",
			"references": 42,
			"revision": 51,
			"time": "2021-10-15T17:29:46-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}