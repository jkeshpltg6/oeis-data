{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A189572",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 189572,
			"data": "0,1,0,0,1,0,1,0,1,0,0,1,0,1,0,0,1,0,1,0,0,1,0,1,0,1,0,0,1,0,1,0,0,1,0,1,0,1,0,0,1,0,1,0,0,1,0,1,0,1,0,0,1,0,1,0,0,1,0,1,0,0,1,0,1,0,1,0,0,1,0,1,0,0,1,0,1,0,1,0,0,1,0,1,0,0,1,0,1,0,0,1,0,1,0,1,0,0,1,0,1,0,0,1,0,1,0,1,0,0,1,0,1,0,0,1",
			"name": "Fixed point of the morphism 0-\u003e01, 1-\u003e001.",
			"comment": [
				"Is this a shifted version of A004641? - _R. J. Mathar_, May 16 2011",
				"The answer is: yes. We have (a(n+1)) = A004641(n), for all n. Let S be the morphism 0-\u003e01, 1-\u003e001, and let T be the morphism 0-\u003e10, 1-\u003e100. By definition A004641 is the unique fixed point of T. The equality above is obviously implied by the following claim:  for all n  it holds that S^n(0)0 = 0T^n(0), and S^n(01)0 = 0T^n(10). Proof: S(0)0 = 010 = 0T(0), and S(01)0 = 010010 = 0T(10). Proceed by induction: S^(n+1)(0)0 = S^n(01)0 = 0T^n(10) = 0T^(n+1)(0) and S^(n+1)(01)0 = S^n(01)S^n(0)S^n(01)0 = 0T^n(10)T^n(0)T^n(10) = 0T^(n+1)(10). - _Michel Dekking_, Feb 07 2017",
				"(a(n)) is the inhomogeneous Sturmian sequence with slope  sqrt(2) - 1 and intercept 1 - sqrt(2)/2. Let psi be the morphism 0-\u003e01, 1-\u003e001. Then the parameters of the Sturmian sequence can be computed from psi = psi_1 psi_4, where psi_1 is the Fibonacci morphism, and psi_4 is the morphism 0-\u003e0, 1-\u003e10. See also Example 2 in Komatsu's paper, which considers the square 0-\u003e01001, 1-\u003e0101001 of psi. - _Michel Dekking_, Nov 03 2018",
				"Proof of Kimberling's conjecture: one can use Lemma 1 in the Bosma, Dekking, Steiner paper, which gives the positions of 1 in a Sturmian sequence as a Beatty sequence. - _Michel Dekking_, Nov 03 2018"
			],
			"link": [
				"Wieb Bosma, Michel Dekking, Wolfgang Steiner, \u003ca href=\"http://math.colgate.edu/~integers/sjs4/sjs4.Abstract.html\"\u003eA remarkable sequence related to Pi and sqrt(2)\u003c/a\u003e, Integers, Electronic Journal of Combinatorial Number Theory 18A (2018), #A4.",
				"Michel Dekking, \u003ca href=\"http://math.colgate.edu/~integers/sjs7/sjs7.Abstract.html\"\u003eSubstitution invariant Sturmian words and binary trees\u003c/a\u003e, Integers, Electronic Journal of Combinatorial Number Theory 18A (2018), #A17.",
				"Takao Komatsu, \u003ca href=\"https://projecteuclid.org/euclid.tjm/1270041624\"\u003eSubstitution invariant inhomogeneous Beatty sequences\u003c/a\u003e, Tokyo J. Math. 22 (1999), 235-243."
			],
			"formula": [
				"a(n) = floor(alpha*n+beta) - floor(alpha*(n-1)+beta), with alpha = sqrt(2)-1, beta = 1-sqrt(2)/2 - _Michel Dekking_, Nov 03 2018"
			],
			"example": [
				"0-\u003e01-\u003e01001-\u003e010010101001-\u003e"
			],
			"mathematica": [
				"t = Nest[Flatten[# /. {0-\u003e{0,1}, 1-\u003e{0, 0, 1}}] \u0026, {0}, 6] (*A189572*)",
				"f[n_] := t[[n]]",
				"Flatten[Position[t, 0]] (*A189573*)",
				"Flatten[Position[t, 1]] (* A080652 conjectured *)"
			],
			"xref": [
				"Cf. A189573, A080762.",
				"See A188068 and the mirror image of A275855 for a similar pair of fixed points of morphisms. - _Michel Dekking_, Feb 07 2017"
			],
			"keyword": "nonn",
			"offset": "1",
			"author": "_Clark Kimberling_, Apr 23 2011",
			"references": 5,
			"revision": 19,
			"time": "2018-11-03T02:10:00-04:00",
			"created": "2011-04-25T00:45:26-04:00"
		}
	]
}