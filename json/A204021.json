{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A204021",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 204021,
			"data": "1,1,-1,2,-4,1,4,-12,9,-1,8,-32,40,-16,1,16,-80,140,-100,25,-1,32,-192,432,-448,210,-36,1,64,-448,1232,-1680,1176,-392,49,-1,128,-1024,3328,-5632,5280,-2688,672,-64,1,256,-2304,8640,-17472,20592",
			"name": "Triangle read by rows: row n shows the coefficients of the characteristic polynomial of the n-th principal submatrix of min(2i-1,2j-1) (A157454).",
			"comment": [
				"Let p(n)=p(n,x) be the characteristic polynomial of the n-th principal submatrix.  The zeros of p(n) are real, and they interlace the zeros of p(n+1).  See A202605 and A204016 for guides to related sequences.",
				"a(0)=1 by convention. - _Philippe Deléham_, Nov 17 2013"
			],
			"reference": [
				"(For references regarding interlacing roots, see A202605.)"
			],
			"link": [
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Morgan-VoycePolynomials.html\"\u003eMorgan-Voyce polynomials\u003c/a\u003e"
			],
			"formula": [
				"From _Peter Bala_, May 01 2012: (Start)",
				"Triangle appears to be a signed version of the row reverse of A211957.",
				"If true, then for 0 \u003c= k \u003c= n-1, T(n,k) = (-1)^k*n/(n-k)*2^(n-k-1)*binomial(2*n-k-1,k) and sum {k = 0..n} T(n,k)*x^(n-k) = 1/2*(-1)^n*(b(2*n,-2*x) + 1)/b(n,-2*x), where b(n,x) := sum {k = 0..n} binomial(n+k,2*k)*x^k are the Morgan-Voyce polynomials of A085478.",
				"Conjectural o.g.f.: t*(1-x-x^2*t)/(1-2*t*(1-x)+t^2*x^2) = (1-x)*t + (2-4*x+x^2)*t^2 + ....",
				"(End)",
				"T(n,k)=2*T(n-1,k)-2*T(n-1,k-1)-T(n-2,k-2), T(0,0)=T(1,0)=1, T(1,1)=-1, T(n,k)=0 of k\u003c0 or if k\u003en. - _Philippe Deléham_, Nov 17 2013"
			],
			"example": [
				"Top of the triangle:",
				"1",
				"1....-1",
				"2....-4.....1",
				"4....-12....9....-1",
				"8....-32....40...-16....1",
				"16...-80....140..-100...25....-1",
				"32...-192...432..-448...210...-36....1",
				"-448=2*(-100)-2*140-(-32). - _Philippe Deléham_, Nov 17 2013"
			],
			"mathematica": [
				"f[i_, j_] := Min[2 i - 1, 2 j - 1];",
				"m[n_] := Table[f[i, j], {i, 1, n}, {j, 1, n}]",
				"TableForm[m[6]] (* 6x6 principal submatrix *)",
				"Flatten[Table[f[i, n + 1 - i],",
				"  {n, 1, 15}, {i, 1, n}]]   (* A157454 *)",
				"p[n_] := CharacteristicPolynomial[m[n], x];",
				"c[n_] := CoefficientList[p[n], x]",
				"TableForm[Flatten[Table[p[n], {n, 1, 10}]]]",
				"Table[c[n], {n, 1, 12}]",
				"Flatten[%]                  (* A204021 *)",
				"TableForm[Table[c[n], {n, 1, 10}]]"
			],
			"xref": [
				"Cf. A157454, A202605, A204016. A085478, A211957."
			],
			"keyword": "tabl,sign",
			"offset": "0,4",
			"author": "_Clark Kimberling_, Jan 11 2012",
			"references": 5,
			"revision": 19,
			"time": "2013-11-17T15:05:08-05:00",
			"created": "2012-01-11T17:32:29-05:00"
		}
	]
}