{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A273171",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 273171,
			"data": "1,3,1,5,5,1,35,7,7,1,63,7,9,9,1,231,55,33,55,11,1,429,429,143,143,13,13,1,6435,5005,3003,195,455,105,15,1,12155,2431,1547,221,595,85,17,17,1,46189,12597,12597,969,323,969,969,57,19,1,88179,146965,20349,14535,2261,20349,5985,133,105,21,1",
			"name": "Triangle for numerators of coefficients for integrated odd powers of cos(x) in terms sin((2*m+1)*x).",
			"comment": [
				"The triangle for the denominators is given in A273172.",
				"Int(cos^(2*n+1)(x), x) = Sum_{m = 0..n} R(n, m)*sin((2*m+1)*x), n \u003e= 0, with the rational triangle a(n, m)/A273172(n, m).",
				"For the rational triangle for the odd powers of cos see A244420/A244421. See also the odd-indexed rows of A273496.",
				"The signed rational triangle S(n, m) = R(n, m)*(-1)^(m+1) appears in the formula",
				"  Int(sin^(2*n+1)(x), x) = Sum_{m = 0..n} S(n, m)*cos((2*m+1)*x), n \u003e= 0,"
			],
			"reference": [
				"I. S. Gradstein and I. M. Ryshik, Tables of series, products , and integrals, Volume 1, Verlag Harri Deutsch, 1981, pp. 168-169, 2.513 1 and 4."
			],
			"formula": [
				"a(n, m) = numerator(R(n, m)) with the rationals R(n, m) = (1/2^(2*n)) * binomial(2*n+1, n-m)/(2*m+1) for m = 0, ..., n, n \u003e= 0. See the Gradstein-Ryshik reference (where the sin arguments are falling)."
			],
			"example": [
				"The triangle a(n, m) begins:",
				"n\\m    0     1     2   3   4   5   6  7  8 9",
				"0:     1",
				"1:     3     1",
				"2:     5     5     1",
				"3:    35     7     7   1",
				"4:    63     7     9   9  1",
				"5:   231    55    33  55  11   1",
				"6:   429   429   143 143  13  13   1",
				"7:  6435  5005  3003 195 455 105  15  1",
				"8: 12155  2431  1547 221 595  85  17 17  1",
				"9: 46189 12597 12597 969 323 969 969 57 19 1",
				"...",
				"row 10: 88179 146965 20349 14535 2261 20349 5985 133 105 21 1,",
				"...",
				"The rational triangle R(n, m) begins:",
				"n\\m   0    1     2    3      4     ...",
				"0:   1/1",
				"1:   3/4  1/12",
				"2:   5/8  5/48 1/80",
				"3:  35/64 7/64 7/320 1/448",
				"4: 63/128 7/64 9/320 9/1792 1/2304",
				"...",
				"row 5: 231/512 55/512 33/1024 55/7168 11/9216 1/11264,",
				"row 6: 429/1024 429/4096 143/4096 143/14336 13/6144 13/45056 1/53248,",
				"row 7: 6435/16384 5005/49152 3003/81920 195/16384 455/147456 105/180224 15/212992 1/245760,",
				"row 8: 12155/32768 2431/24576 1547/40960 221/16384 595/147456 85/90112 17/106496 17/983040 1/1114112,",
				"row 9: 46189/131072 12597/131072 12597/327680 969/65536 323/65536 969/720896 969/3407872 57/1310720 19/4456448 1/4980736,",
				"row 10: 88179/262144 146965/1572864 20349/524288 14535/917504 2261/393216 20349/11534336 5985/13631488 133/1572864 105/8912896 21/19922944 1/22020096.",
				"...",
				"n = 3: Int(cos^7(x), x) = (35/64)*sin(x) + (7/64)*sin(3*x) + (7/320)*sin(5*x) + (1/448)*sin(7*x). Gradstein-Rhyshik, p. 169, 2.513 16.",
				"  Int(sin^7(x), x) = -(35/64)*cos(x) + (7/64)*cos(3*x) - (7/320)*cos(5*x) + (1/448)*cos(7*x). Gradstein-Rhyshik, p. 169, 2.513 10."
			],
			"mathematica": [
				"T[MaxN_] :=   Function[{n}, With[{exp =  Expand[(1/2)^(2 n + 1) (Exp[I x] + Exp[-I x])^(2 n + 1)]},  2/(2 # + 1) Coefficient[exp, Exp[I (2 # + 1) x]] \u0026 /@  Range[0, n]]][#] \u0026 /@ Range[0, MaxN];",
				"T[5] // ColumnForm (* _Bradley Klee_, Jun 14 2016 *)"
			],
			"program": [
				"(PARI) a(n, m) = numerator((1/2^(2*n))*binomial(2*n+1, n-m)/(2*m+1));",
				"tabl(nn) = for (n=0, nn, for (k=0, n, print1(a(n,k), \", \")); print()); \\\\ _Michel Marcus_, Jun 19 2016"
			],
			"xref": [
				"Cf. A273172, A244420, A244421, A273496."
			],
			"keyword": "nonn,tabl,frac,easy",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Jun 13 2016",
			"references": 2,
			"revision": 15,
			"time": "2019-12-11T07:38:10-05:00",
			"created": "2016-06-23T00:03:40-04:00"
		}
	]
}