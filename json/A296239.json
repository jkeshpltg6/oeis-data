{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A296239",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 296239,
			"data": "0,0,0,0,1,0,1,1,0,1,2,2,1,0,1,2,3,4,3,2,1,0,1,2,3,4,5,6,6,5,4,3,2,1,0,1,2,3,4,5,6,7,8,9,10,10,9,8,7,6,5,4,3,2,1,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,16,15,14,13,12,11,10,9",
			"name": "a(n) = distance from n to nearest Fibonacci number.",
			"comment": [
				"The Fibonacci numbers correspond to sequence A000045.",
				"This sequence is analogous to:",
				"- A051699 (distance to nearest prime),",
				"- A053188 (distance to nearest square),",
				"- A053646 (distance to nearest power of 2),",
				"- A053615 (distance to nearest oblong number),",
				"- A053616 (distance to nearest triangular number),",
				"- A061670 (distance to nearest power),",
				"- A074989 (distance to nearest cube),",
				"- A081134 (distance to nearest power of 3),",
				"The local maxima of the sequence correspond to positive terms of A004695.",
				"a(n) = 0 iff n = A000045(k) for some k \u003e= 0.",
				"a(n) = 1 iff n = A061489(k) for some k \u003e 4.",
				"For any n \u003e= 0, abs(a(n+1) - a(n)) \u003c= 1.",
				"For any n \u003e 0, a(n) \u003c n, and a^k(n) = 0 for some k \u003e 0 (where a^k denotes the k-th iterate of a); k equals A105446(n) for n = 1..80 (and possibly more values).",
				"a(n) \u003e max(a(n-1), a(n+1)) iff n = A001076(k) for some k \u003e 1."
			],
			"link": [
				"\u003ca href=\"/index/Di#distance_to_the_nearest\"\u003eIndex entries for sequences related to distance to nearest element of some set\u003c/a\u003e"
			],
			"formula": [
				"a(n) = abs(n - Fibonacci(floor(log(sqrt(20)*n)/log((1 + sqrt(5))/2)-1))). - _Jon E. Schoenfield_, Dec 14 2017"
			],
			"example": [
				"For n = 42:",
				"- A000045(9) = 34 \u003c= 42 \u003c= 55 = A000045(10),",
				"- a(42) = min(42 - 34, 55 - 42) = min(8, 13) = 8."
			],
			"mathematica": [
				"fibPi[n_] := 1 + Floor[ Log[ GoldenRatio, 1 + n*Sqrt@5]]; f[n_] := Block[{m = fibPi@ n}, Min[n - Fibonacci[m -1], Fibonacci[m] - n]]; Array[f, 81, 0] (* _Robert G. Wilson v_, Dec 11 2017 *)"
			],
			"program": [
				"(PARI) a(n) = for (i=1, oo, if (n\u003c=fibonacci(i), return (min(n-fibonacci(i-1), fibonacci(i)-n))))"
			],
			"xref": [
				"Cf. A000045, A001076, A004695, A051699, A053188, A053615, A053616, A053646, A061489, A061670, A074989, A081134, A105446."
			],
			"keyword": "nonn,easy",
			"offset": "0,11",
			"author": "_Rémy Sigrist_, Dec 09 2017",
			"references": 2,
			"revision": 23,
			"time": "2017-12-15T12:24:09-05:00",
			"created": "2017-12-15T12:24:09-05:00"
		}
	]
}