{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A174501",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 174501,
			"data": "1,4,1,32,1,196,1,1152,1,6724,1,39200,1,228484,1,1331712,1,7761796,1,45239072,1,263672644,1,1536796800,1,8957108164,1,52205852192,1,304278004996,1,1773462177792,1,10336495061764,1,60245508192800,1,351136554095044,1",
			"name": "Continued fraction expansion for exp( Sum_{n\u003e=1} 1/(n*A003499(n)) ), where A003499(n) = (3+sqrt(8))^n + (3-sqrt(8))^n.",
			"link": [
				"Colin Barker, \u003ca href=\"/A174501/b174501.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Peter Bala, \u003ca href=\"/A174500/a174500_2.pdf\"\u003eSome simple continued fraction expansions for an infinite product, Part 1\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,7,0,-7,0,1)."
			],
			"formula": [
				"a(2n-1) = 1, a(2n) = A003499(n) - 2, for n\u003e=1 [conjecture].",
				"The above conjectures are correct. See the Bala link for details. - _Peter Bala_, Jan 08 2013",
				"a(n) = 7*a(n-2)-7*a(n-4)+a(n-6). G.f.: -x*(x^4+4*x^3-6*x^2+4*x+1) / ((x-1)*(x+1)*(x^2-2*x-1)*(x^2+2*x-1)). - _Colin Barker_, Jan 20 2013",
				"a(n) = (((-1-sqrt(2))^n+(1-sqrt(2))^n+(sqrt(2)-1)^n+(1+sqrt(2))^n-4))/2 for n even. - _Colin Barker_, May 11 2016"
			],
			"example": [
				"Let L = Sum_{n\u003e=1} 1/(n*A003499(n)) or, more explicitly,",
				"L = 1/6 + 1/(2*34) + 1/(3*198) + 1/(4*1154) + 1/(5*6726) +...",
				"so that L = 0.1833074113563494600094468694966574405706183998044...",
				"then exp(L) = 1.2011836088120841844713993433258934531421726294252...",
				"equals the continued fraction given by this sequence:",
				"exp(L) = [1;4,1,32,1,196,1,1152,1,6724,1,39200,1,...]; i.e.,",
				"exp(L) = 1 + 1/(4 + 1/(1 + 1/(32 + 1/(1 + 1/(196 + 1/(1 +...)))))).",
				"Compare these partial quotients to A003499(n), n=1,2,3,...:",
				"[6,34,198,1154,6726,39202,228486,1331714,7761798,45239074,...]."
			],
			"mathematica": [
				"LinearRecurrence[{0,7,0,-7,0,1},{1,4,1,32,1,196},50] (* _Harvey P. Dale_, Jul 14 2021 *)"
			],
			"program": [
				"(PARI) {a(n)=local(L=sum(m=1,2*n+1000,1./(m*round((3+sqrt(8))^m+(3-sqrt(8))^m))));contfrac(exp(L))[n]}",
				"(PARI) Vec(-x*(x^4+4*x^3-6*x^2+4*x+1)/((x-1)*(x+1)*(x^2-2*x-1)*(x^2+2*x-1)) + O(x^50)) \\\\ _Colin Barker_, May 11 2016"
			],
			"xref": [
				"Cf. A003499, A174500, A174502."
			],
			"keyword": "cofr,nonn,easy",
			"offset": "1,2",
			"author": "_Paul D. Hanna_, Mar 20 2010",
			"references": 5,
			"revision": 24,
			"time": "2021-07-14T17:07:48-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}