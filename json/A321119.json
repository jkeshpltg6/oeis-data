{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A321119",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 321119,
			"data": "4,2,8,10,28,38,104,142,388,530,1448,1978,5404,7382,20168,27550,75268,102818,280904,383722,1048348,1432070,3912488,5344558,14601604,19946162,54493928,74440090,203374108,277814198,759002504,1036816702,2832635908,3869452610",
			"name": "a(n) = ((1 - sqrt(3))^n + (1 + sqrt(3))^n)/2^floor((n - 1)/2); n-th row common denominator of A321118.",
			"reference": [
				"Harold J. Ahlberg, Edwin N. Nilson and Joseph L. Walsh, The Theory of Splines and Their Applications, Academic Press, 1967. See p. 47, Table 2.5.2."
			],
			"link": [
				"Encyclopedia of Mathematics, \u003ca href=\"https://www.encyclopediaofmath.org/index.php/Quadrature_formula\"\u003eQuadrature formula\u003c/a\u003e",
				"John C. Holladay, \u003ca href=\"https://doi.org/10.1090/S0025-5718-1957-0093894-6\"\u003eA smoothest curve approximation\u003c/a\u003e, Math. Comp. Vol. 11 (1957), 233-243.",
				"Peter Köhler, \u003ca href=\"https://doi.org/10.1007/BF02575942\"\u003eOn the weights of Sard's quadrature formulas\u003c/a\u003e, CALCOLO Vol. 25 (1988), 169-186.",
				"Leroy F. Meyers and Arthur Sard, \u003ca href=\"https://doi.org/10.1002/sapm1950291118\"\u003eBest approximate integration formulas\u003c/a\u003e, J. Math. Phys. Vol. 29 (1950), 118-123.",
				"Arthur Sard, \u003ca href=\"https://doi.org/10.2307/2372095\"\u003eBest approximate integration formulas; best approximation formulas\u003c/a\u003e, American Journal of Mathematics Vol. 71 (1949), 80-91.",
				"Isaac J. Schoenberg, \u003ca href=\"https://projecteuclid.org/euclid.bams/1183525790\"\u003eSpline interpolation and best quadrature formulae\u003c/a\u003e, Bull. Amer. Math. Soc. Vol. 70 (1964), 143-148.",
				"Frans Schurer, \u003ca href=\"https://research.tue.nl/en/publications/on-natural-cubic-splines-with-an-application-to-numerical-integra\"\u003eOn natural cubic splines, with an application to numerical integration formulae\u003c/a\u003e, EUT report. WSK, Dept. of Mathematics and Computing Science Vol. 70-WSK-04 (1970), 1-32.",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,4,0,-1)."
			],
			"formula": [
				"a(n) = (((sqrt(2) - sqrt(6))/2)^n + ((sqrt(6) + sqrt(2))/2)^n)*((2 - sqrt(2))*(-1)^n + 2 + sqrt(2))/2.",
				"a(-n) = (-1)^n*a(n).",
				"a(n) = 2*A000034(n+1)*A002531(n).",
				"a(2*n) = 2*A001834(n).",
				"a(2*n+1) = 2*A003500(n).",
				"a(n) = 4*a(n-2) - a(n-4) with a(0) = 4, a(1) = 2, a(2) = 8, a(3) = 10.",
				"a(2*n+3) = a(2*n+1) + a(2*n+2).",
				"a(2*n+2) = a(2*n) + 2*a(2*n+1).",
				"G.f.: 2*(1 - x)*(2 + 3*x - x^2)/(1 - 4*x^2 + x^4).",
				"E.g.f.: (1 + exp(-sqrt(6)*x))*((2 - sqrt(2))*exp(sqrt(2 - sqrt(3))*x) + (2 + sqrt(2))*exp(sqrt(2 + sqrt(3))*x))/2.",
				"Lim_{n-\u003einfinity} a(2*n+1)/a(2*n) = (1 + sqrt(3))/2."
			],
			"example": [
				"a(0) = ((1 - sqrt(3))^0 + (1 + sqrt(3))^0)/2^floor((0 - 1)/2) = 2*(1 + 1) = 4."
			],
			"mathematica": [
				"LinearRecurrence[{0, 4, 0, -1}, {4, 2, 8, 10}, 50]"
			],
			"program": [
				"(Maxima) a(n) := ((1 - sqrt(3))^n + (1 + sqrt(3))^n)/2^floor((n - 1)/2)$",
				"makelist(ratsimp(a(n)), n, 0, 50);"
			],
			"xref": [
				"Cf. A002176 (common denominators of Cotesian numbers).",
				"Cf. A321118, A321120.",
				"Cf. A001834, A002530, A002531, A003500, A005246, A048788, A083336, A125905."
			],
			"keyword": "nonn,easy,frac",
			"offset": "0,1",
			"author": "_Franck Maminirina Ramaharo_, Nov 01 2018",
			"references": 4,
			"revision": 12,
			"time": "2018-12-01T08:59:18-05:00",
			"created": "2018-12-01T08:59:18-05:00"
		}
	]
}