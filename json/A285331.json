{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A285331",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 285331,
			"data": "0,1,2,3,6,4,14,7,5,12,30,9,62,10,8,15,126,19,254,25,24,252,510,39,13,76,11,21,1022,28,2046,31,38,316,18,79,4094",
			"name": "Inverse for A285332: a(1) = 0, a(2) = 1, a(A019565(n)) = 2*a(n), a(A065642(n)) = 1 + 2*a(n).",
			"comment": [
				"Note the indexing: the domain starts from 1, while the range includes also zero.",
				"For the question whether this sequence and A285332 are permutations of natural numbers, see comments in A285332 and the conjecture stated in A019565.",
				"As a practical problem, it seems next-to-impossible to compute even the value of a(38). Even though we know that 38 certainly is not in a finite cycle of A019565, because A048675(38) = 129, A048675(129) = 8194 and A048675(8194) = 4503599627370561 which factorizes as 3^2 * 37 * 71 * 190483425427 (thus is not squarefree and A285320(38) = 3), the value of a(38) is most likely so huge that it will not fit into the data section or even into a b-file. The same problem applies to all numbers that share prime factors with 38, namely 76, 152, 304, 608, 722, ...",
				"Terms a(39) .. a(61) are [632, 51, 8190, 60, 16382, 505, 17, 72057594037927932, 32766, 159, 29, 103, 1016, 153, 65534, 319, 50, 43, 16376, 131014, 131070, 57, 262142].",
				"The name is slightly misleading. The given definition of a(n) is not always very helpful to compute the terms (cf. example of n = 38), it is actually not clear whether the sequence is well defined. - _M. F. Hasler_, Mar 01 2018"
			],
			"link": [
				"Indranil Ghosh, \u003ca href=\"/A285331/a285331.txt\"\u003ePython Program to generate the sequence\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"a(1) = 0, a(2) = 1, and for n \u003e 2, if A008683(n) \u003c\u003e 0 [when n is squarefree], a(n) = 2*a(A048675(n)), otherwise a(n) = 1 + 2*a(A285328(n)).",
				"For all n \u003e= 0, a(A285332(n)) = n."
			],
			"example": [
				"a(1) = 0 and a(2) = 1 by definition.",
				"a(3) = a(prime(2)) = a(A019565(2^1)) = 2*a(2) = 2.",
				"a(4) = a(2^2) = a(A065642(2)) = 1 + 2*a(2) = 3.",
				"a(5) = a(prime(3)) = a(A019565(2^2)) = 2*a(4) = 6.",
				"a(9) = a(3^2) = a(A065642(3)) = 1 + 2*a(3) = 5.",
				"a(10) = a(2*5) = a(prime(1)*prime(3)) = a(A019565(2^0+2^2)) = 2*a(1+4) = 12.",
				"To compute a(38), write 38 = prime(1)*prime(8) = A019565(2^7+2^0), so a(38) = 2*a(129). To compute this, use 129 = prime(2)*prime(14) = A019565(2^13+2^1), so a(129) = 2*a(8194). But 8194 = prime(1)*prime(7)*prime(53) = A019565(2^0+2^6+2^52), so a(8194) = 2*a(4503599627370561)..."
			],
			"program": [
				"(PARI) A285331(n)={ if(n\u003c=2,n-1,if(moebius(n)\u003c\u003e0, 2*A285331(A048675(n)), 1+2*A285331(A285328(n))))} \\\\ See A048675 \u0026 A285328 for respective PARI code. (We avoid content duplication leading to obsolete code.)",
				"(Scheme, with memoization-macro definec)",
				"(definec (A285331 n) (cond ((\u003c= n 2) (- n 1)) ((not (zero? (A008683 n))) (* 2 (A285331 (A048675 n)))) (else (+ 1 (* 2 (A285331 (A285328 n)))))))"
			],
			"xref": [
				"Inverse: A285332.",
				"Cf. A005117, A008683, A019565, A048675, A065642, A087207, A285319, A285320, A285328.",
				"Compare also to permutation A285111."
			],
			"keyword": "nonn,hard,more",
			"offset": "1,3",
			"author": "_Antti Karttunen_, Apr 17 2017, comments edited Apr 19 2017",
			"references": 7,
			"revision": 37,
			"time": "2018-03-03T17:35:29-05:00",
			"created": "2017-04-18T15:44:24-04:00"
		}
	]
}