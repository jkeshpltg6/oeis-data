{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A152770",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 152770,
			"data": "0,0,0,1,0,3,0,4,2,5,0,11,0,7,6,11,0,16,0,17,8,11,0,29,4,13,10,23,0,35,0,26,12,17,10,47,0,19,14,43,0,47,0,35,28,23,0,67,6,38,18,41,0,59,14,57,20,29,0,97,0,31,36,57,16,71,0,53,24,67,0,112,0,37,44,59,16,83,0,97",
			"name": "Sum of proper divisors minus the number of proper divisors of n: a(n) = sigma(n) - n - d(n) + 1.",
			"comment": [
				"Sum of divisors of n, minus the number of divisors of n, minus n, plus 1.",
				"Also, sum of proper divisors of n, minus the number of divisors of n, plus 1.",
				"Note that if a(n)\u003e0 then n is a composite number (A002808), otherwise, n is a noncomposite number (A008578) also called prime number at the beginning of the 20th century.",
				"Also, sum of divisors of n, minus the number of proper divisors of n, minus n.",
				"a(A008578(n)) = 0 for all n\u003e=1. - _Robert G. Wilson v_, Dec 14 2008"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A152770/b152770.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Joerg Arndt, \u003ca href=\"http://arxiv.org/abs/1202.6525\"\u003eOn computing the generalized Lambert series\u003c/a\u003e, arXiv:1202.6525v3 [math.CA], (2012)."
			],
			"formula": [
				"a(n) = A000203(n) - A000005(n) - n + 1 = A001065(n) - A000005(n) + 1 = A000203(n) - A062249(n) + 1 = A065608(n) - n + 1.",
				"a(n) = A000203(n) - A032741(n) - n.",
				"a(n) = A001065(n) - A032741(n).",
				"a(n) = A158901(n) - n. - _Juri-Stepan Gerasimov_, Sep 12 2009",
				"From _Peter Bala_ Jan 22 2021: (Start)",
				"G.f.: A(q) = Sum_{n \u003e= 2} (n-1)*q^(2*n)/(1 - q^n) = Sum_{n \u003e= 2} q^(2*n)/(1 - q^n)^2. Cf. A001065.",
				"Faster converging series: A(q) = Sum_{n \u003e= 1} q^(n*(n+1))*((n-1)*q^(3*n+2) - n*q^(2*n+1) + (2-n)*q^(n+1) + n - 1)/((1 - q^n)*(1 - q^(n+1))^2) - apply the operator t*d/dt to equation 1 in Arndt, then set t = q^2 and x = q. (End)"
			],
			"maple": [
				"A152770 := proc(n)",
				"        numtheory[sigma](n)-n-numtheory[tau](n)+1 ;",
				"end proc: # _R. J. Mathar_, Sep 28 2011"
			],
			"mathematica": [
				"f[n_] := DivisorSigma[1, n] - DivisorSigma[0, n] - n + 1; Array[f, 105] (* _Robert G. Wilson v_, Dec 14 2008 *)"
			],
			"program": [
				"(PARI) a(n)=sigma(n)-n-numdiv(n)+1 \\\\ _Charles R Greathouse IV_, Mar 09 2014"
			],
			"xref": [
				"Cf. A000005, A000040, A000203, A001065, A002808, A008578, A062249, A065608, A032741."
			],
			"keyword": "nonn,easy",
			"offset": "1,6",
			"author": "_Omar E. Pol_, Dec 12 2008",
			"ext": [
				"More terms from _Omar E. Pol_ and _Robert G. Wilson v_, Dec 14 2008",
				"Definition clarified and edited by _Omar E. Pol_, Dec 21 2008"
			],
			"references": 22,
			"revision": 28,
			"time": "2021-01-24T10:17:27-05:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}