{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A082137",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 82137,
			"data": "1,1,1,1,2,2,1,3,6,4,1,4,12,16,8,1,5,20,40,40,16,1,6,30,80,120,96,32,1,7,42,140,280,336,224,64,1,8,56,224,560,896,896,512,128,1,9,72,336,1008,2016,2688,2304,1152,256,1,10,90,480,1680,4032,6720,7680,5760,2560,512",
			"name": "Square array of transforms of binomial coefficients, read by antidiagonals.",
			"comment": [
				"Rows are associated with the expansions of (x^k/k!)exp(x)cosh(x) (leading zeros dropped). Rows include A011782, A057711, A080929, A082138, A080951, A082139, A082140, A082141. Columns are of the form 2^(k-1)C(n+k, k). Diagonals include A069723, A082143, A082144, A082145, A069720.",
				"T(n, k) is also the number of idempotent order-preserving and order-decreasing partial transformations (of an n-chain) of width k (width(alpha)= |Dom(alpha)|). - _Abdullahi Umar_, Oct 02 2008",
				"Read as a triangle this is A119468 with rows reversed. A119468 has e.g.f. exp(z*x)/(1-tanh(x)). - _Peter Luschny_, Aug 01 2012",
				"Read as a triangle this is a subtriangle of A198793. - _Philippe Deléham_, Nov 10 2013"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A082137/b082137.txt\"\u003eTable of n, a(n) for the first 50 rows, flattened\u003c/a\u003e",
				"Laradji, A. and Umar, \u003ca href=\"http://dx.doi.org/10.1016/j.jalgebra.2003.10.023\"\u003eA. Combinatorial results for semigroups of order-preserving partial transformations\u003c/a\u003e, Journal of Algebra 278, (2004), 342-359.",
				"Laradji, A. and Umar, A. \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL7/Umar/um.html\"\u003eCombinatorial results for semigroups of order-decreasing partial transformations\u003c/a\u003e, J. Integer Seq. 7 (2004), 04.3.8."
			],
			"formula": [
				"Square array defined by T(n, k)=(2^(n-1)+0^n/2)C(n + k, n)= Sum{k=0..n, C(n+k, k+j)C(k+j, k)(1+(-1)^j)/2 }.",
				"As an infinite lower triangular matrix, equals A007318 * A134309. - _Gary W. Adamson_, Oct 19 2007",
				"O.g.f. for array read as a triangle: (1-x*(1+t))/((1-x)*(1-x*(1+2*t))) = 1 + x*(1+t) + x^2*(1+2*t+2*t^2) + x^3*(1+3*t+6*t^2+4*t^3) + .... - _Peter Bala_, Apr 26 2012",
				"For array read as a triangle: T(n,k) = 2*T(n-1,k) + 2*T(n-1,k-1) - T(n-2,k) -2*T(n-2,k-1), T(0,0) = T(1,0) = T(1,1) = 1, T(n,k) = 0 if k\u003c0 or if k\u003en. - _Philippe Deléham_, Nov 10 2013"
			],
			"example": [
				"Rows begin",
				"  1 1  2   4   8 ...",
				"  1 2  6  16  40 ...",
				"  1 3 12  40 120 ...",
				"  1 4 20  80 280 ...",
				"  1 5 30 140 560 ...",
				"Read as a triangle, this begins:",
				"  1",
				"  1, 1",
				"  1, 2,  2",
				"  1, 3,  6,  4",
				"  1, 4, 12, 16,   8",
				"  1, 5, 20, 40,  40, 16",
				"  1, 6, 30, 80, 120, 96, 32",
				"  ... - _Philippe Deléham_, Nov 10 2013"
			],
			"maple": [
				"# As a triangular array:",
				"T := (n,k) -\u003e 2^(k+0^k-1)*binomial(n,k):",
				"for n from 0 to 9 do seq(T(n,k), k=0..n) od; # _Peter Luschny_, Nov 10 2017"
			],
			"mathematica": [
				"rows = 11; t[n_, k_] := 2^(n-1)*(n+k)!/(n!*k!); t[0, _] = 1; tkn = Table[ t[n, k], {k, 0, rows}, {n, 0, rows}]; Flatten[ Table[ tkn[[ n-k+1, k ]], {n, 1, rows}, {k, 1, n}]] (* _Jean-François Alcover_, Jan 20 2012 *)"
			],
			"program": [
				"(Sage)",
				"def A082137_row(n) : # as a triangular array",
				"    var('z')",
				"    s = (exp(z*x)/(1-tanh(x))).series(x,n+2)",
				"    t = factorial(n)*s.coefficient(x,n)",
				"    return [t.coefficient(z,n-k) for k in (0..n)]",
				"for n in (0..7) : print(A082137_row(n))  # _Peter Luschny_, Aug 01 2012"
			],
			"xref": [
				"Cf. A119468, A007318, A134309."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "0,5",
			"author": "_Paul Barry_, Apr 06 2003",
			"references": 14,
			"revision": 54,
			"time": "2021-02-15T02:00:54-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}