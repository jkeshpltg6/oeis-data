{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A145515",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 145515,
			"data": "1,1,1,1,1,1,1,2,1,1,1,2,4,1,1,1,2,5,10,1,1,1,2,6,23,36,1,1,1,2,7,46,239,202,1,1,1,2,8,82,1086,5828,1828,1,1,1,2,9,134,3707,79326,342383,27338,1,1,1,2,10,205,10340,642457,18583582,50110484,692004,1,1,1,2,11,298,24901,3649346,446020582,14481808030,18757984046,30251722,1,1",
			"name": "Square array A(n,k), n\u003e=0, k\u003e=0, read by antidiagonals: A(n,k) is the number of partitions of k^n into powers of k.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A145515/b145515.txt\"\u003eAntidiagonals n = 0..40, flattened\u003c/a\u003e"
			],
			"formula": [
				"See program.",
				"For k\u003e1: A(n,k) = [x^(k^n)] 1/Product_{j\u003e=0} (1-x^(k^j))."
			],
			"example": [
				"A(2,3) = 5, because there are 5 partitions of 3^2=9 into powers of 3: [1,1,1,1,1,1,1,1,1], [1,1,1,1,1,1,3], [1,1,1,3,3], [3,3,3], [9].",
				"Square array A(n,k) begins:",
				"  1,  1,   1,    1,     1,      1,  ...",
				"  1,  1,   2,    2,     2,      2,  ...",
				"  1,  1,   4,    5,     6,      7,  ...",
				"  1,  1,  10,   23,    46,     82,  ...",
				"  1,  1,  36,  239,  1086,   3707,  ...",
				"  1,  1, 202, 5828, 79326, 642457,  ..."
			],
			"maple": [
				"b:= proc(n, j, k) local nn;",
				"      nn:= n+1;",
				"      if n\u003c0  then 0",
				"    elif j=0  or n=0 or k\u003c=1 then 1",
				"    elif j=1  then nn",
				"    elif n\u003e=j then (nn-j) *binomial(nn, j) *add(binomial(j, h)",
				"                   /(nn-j+h) *b(j-h-1, j, k) *(-1)^h, h=0..j-1)",
				"              else b(n, j, k):= b(n-1, j, k) +b(k*n, j-1, k)",
				"      fi",
				"    end:",
				"A:= (n, k)-\u003e b(1, n, k):",
				"seq(seq(A(n, d-n), n=0..d), d=0..13);"
			],
			"mathematica": [
				"b[n_, j_, k_] := Module[{nn = n+1}, Which[n \u003c 0, 0, j == 0 || n == 0 || k \u003c= 1, 1, j == 1, nn, n \u003e= j, (nn-j)*Binomial[nn, j]*Sum[Binomial[j, h]/(nn-j+h)* b[j-h-1, j, k]*(-1)^h, {h, 0, j-1}], True, b[n, j, k] = b[n-1, j, k] + b[k*n, j-1, k] ] ]; a[n_, k_] := b[1, n, k]; Table[Table[a[n, d-n], {n, 0, d}], {d, 0, 13}] // Flatten (* _Jean-François Alcover_, Dec 12 2013, translated from Maple *)"
			],
			"xref": [
				"Columns k=0+1, 2-10 give: A000012, A002577, A078125, A078537, A111822, A111827, A111832, A111837, A145512, A145513.",
				"Row n=3 gives: A189890(k+1).",
				"Main diagonal gives: A145514.",
				"Cf. A007318."
			],
			"keyword": "nonn,tabl",
			"offset": "0,8",
			"author": "_Alois P. Heinz_, Oct 11 2008",
			"ext": [
				"Edited by _Alois P. Heinz_, Jan 12 2011"
			],
			"references": 22,
			"revision": 30,
			"time": "2019-03-19T12:47:42-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}