{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A049410",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 49410,
			"data": "1,3,1,6,9,1,6,51,18,1,0,210,195,30,1,0,630,1575,525,45,1,0,1260,10080,6825,1155,63,1,0,1260,51660,71505,21840,2226,84,1,0,0,207900,623700,333585,57456,3906,108,1,0,0,623700,4573800,4293135,1195425,131670",
			"name": "A triangle of numbers related to triangle A049325.",
			"comment": [
				"a(n,1)= A008279(3,n-1). a(n,m)=: S1(-3; n,m), a member of a sequence of lower triangular Jabotinsky matrices, including S1(1; n,m)= A008275 (signed Stirling first kind), S1(2; n,m)= A008297(n,m) (signed Lah numbers). a(n,m) matrix is inverse to signed matrix ((-1)^(n-m))*A000369(n,m).",
				"The monic row polynomials E(n,x) := sum(a(n,m)*x^m,m=1..n), E(0,x) := 1 are exponential convolution polynomials (see A039692 for the definition and a Knuth reference).",
				"Also the inverse Bell transform of the quadruple factorial numbers Product_{k=0..n-1} (4*k+3) (A008545) adding 1,0,0,0,... as column 0. For the definition of the Bell transform see A264428 and for cross-references A265604. - _Peter Luschny_, Dec 31 2015"
			],
			"link": [
				"Wolfdieter Lang, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/index.html\"\u003eOn generalizations of Stirling number triangles\u003c/a\u003e, J. Integer Seqs., Vol. 3 (2000), #00.2.4."
			],
			"formula": [
				"a(n, m) = n!*A049325(n, m)/(m!*4^(n-m)); a(n, m) = (4*m-n+1)*a(n-1, m) + a(n-1, m-1), n \u003e= m \u003e= 1; a(n, m)=0, n\u003cm; a(n, 0) := 0; a(1, 1)=1. E.g.f. for m-th column: (((-1+(1+x)^4)/4)^m)/m!."
			],
			"example": [
				"Triangle begins:",
				"  {1};",
				"  {3,1};",
				"  {6,9,1};",
				"  {6,51,18,1};",
				"  ...",
				"E.g. row polynomial E(3,x)= 6*x+9*x^2+x^3."
			],
			"mathematica": [
				"rows = 10;",
				"t = Table[Product[4k+3, {k, 0, n-1}], {n, 0, rows}];",
				"T[n_, k_] := BellY[n, k, t];",
				"M = Inverse[Array[T, {rows, rows}]] // Abs;",
				"A049325 = Table[M[[n, k]], {n, 1, rows}, {k, 1, n}] // Flatten (* _Jean-François Alcover_, Jun 22 2018, after _Peter Luschny_ *)"
			],
			"program": [
				"(Sage) # uses[inverse_bell_transform from A265605]",
				"# Adds a column 1,0,0,0,... at the left side of the triangle.",
				"multifact_4_3 = lambda n: prod(4*k + 3 for k in (0..n-1))",
				"inverse_bell_matrix(multifact_4_3, 9) # _Peter Luschny_, Dec 31 2015"
			],
			"xref": [
				"Row sums give A049426."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "1,2",
			"author": "_Wolfdieter Lang_",
			"references": 5,
			"revision": 19,
			"time": "2020-03-26T03:56:57-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}