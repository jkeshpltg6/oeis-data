{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A230194",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 230194,
			"data": "0,0,0,0,0,1,1,3,3,3,4,3,6,5,3,3,3,5,4,4,4,2,9,10,9,7,5,12,8,2,8,6,6,7,9,4,3,10,11,2,4,6,10,9,11,9,4,10,17,9,1,4,7,6,6,6,2,5,14,13,7,5,14,6,3,5,4,12,11,14,5,2,16,11,5,9,6,8,11,23,15,3,23,18,17,9,8,20,5,10,14,3,14,15,16,9,8,24,10,7",
			"name": "Number of ways to write n = x + y + z (x, y, z \u003e 0) such that all the 11 integers 6*x-1, 6*x+1, 6*x-5, 6*x+5, 6*y-1, 6*y-5, 6*y+5, 6*(x+y)+5, 6*z-1, 6*z-5 and 6*z+5 are prime.",
			"comment": [
				"Conjecture: a(n) \u003e 0 for all n \u003e 5.",
				"Let S be the set of those primes p with p-4 and p+6 also prime. Since each element of S has the form 6*k-1 with k \u003e 0, the conjecture implies that 6*n-3 with n \u003e 5 can be expressed as a sum of three primes in the set S. If n = x + y + z, then 6*n = (6*(x+y)+5) + (6*z-5). So a(n) \u003e 0 implies Goldbach's conjecture for the even number 6*n.",
				"Let T be the set of those primes p with p+4 and p-6 also prime. Clearly each element of T has the form 6*k+1 with k \u003e 0. We conjecture that 6*n+3 with n \u003e 5 can be expressed as a sum of three primes in the set T."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A230194/b230194.txt\"\u003eTable of n, a(n) for n = 1..5000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1211.1588\"\u003eConjectures involving primes and quadratic forms\u003c/a\u003e, preprint, arXiv:1211.1588 [math.NT], 2012-2017."
			],
			"example": [
				"a(30) = 2 since 30 = 2 + 14 + 14 = 18 + 4 + 8, and 6*2-1 = 11, 6*2+1 = 13, 6*2-5 = 7, 6*2+5 = 17, 6*14-1 = 83, 6*14-5 = 79, 6*14+5 = 89, 6*(2+14)+5 = 101, 6*18-1 = 107, 6*18+1 = 109, 6*18-5 = 103, 6*18+5 = 113, 6*4-1 = 23, 6*4-5 = 19, 6*4+5 = 29, 6*(18+4)+5 = 137, 6*8-1 = 47, 6*8-5 = 43 and 6*8+5 = 53 are all prime."
			],
			"mathematica": [
				"SQ[n_]:=PrimeQ[6n-1]\u0026\u0026PrimeQ[6n-5]\u0026\u0026PrimeQ[6n+5]",
				"a[n_]:=Sum[If[SQ[i]\u0026\u0026PrimeQ[6i+1]\u0026\u0026SQ[j]\u0026\u0026PrimeQ[6(i+j)+5]\u0026\u0026SQ[n-i-j],1,0],{i,1,n-2},{j,1,n-1-i}]",
				"Table[a[n],{n,1,100}]"
			],
			"xref": [
				"Cf. A230140, A230141."
			],
			"keyword": "nonn",
			"offset": "1,8",
			"author": "_Zhi-Wei Sun_, Oct 11 2013",
			"references": 1,
			"revision": 12,
			"time": "2019-08-05T05:32:32-04:00",
			"created": "2013-10-11T09:27:07-04:00"
		}
	]
}