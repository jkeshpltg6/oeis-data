{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A271705",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 271705,
			"data": "1,1,1,1,4,1,1,15,9,1,1,64,66,16,1,1,325,490,190,25,1,1,1956,3915,2120,435,36,1,1,13699,34251,23975,6755,861,49,1,1,109600,328804,283136,101990,17696,1540,64,1,1,986409,3452436,3534636,1554966,342846,40404,2556,81,1",
			"name": "Triangle read by rows, T(n,k) = Sum_{j=0..n} (-1)^(n-j)*C(-j-1,-n-1)*L(j,k), L the unsigned Lah numbers A271703, for n\u003e=0 and 0\u003c=k\u003c=n.",
			"comment": [
				"This is the Sheffer (aka exponential Riordan) matrix T =  P*L = A007318*A271703 = (exp(x), x/(1-x)). Note that P = A007318 is Sheffer (exp(t), t) (of the Appell type). The Sheffer a-sequence is [1,1,repeat(0)] and the z-sequence has e.g.f. (x/(1+x))*(1 - exp(-x/(1+x)) given in A288869 / A000027. Because the column k=0 has only entries 1, the z-sequence gives fractional representations of 1. See A288869. - _Wolfdieter Lang_, Jun 20 2017"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A271705/b271705.txt\"\u003eRows n = 0..50 of the triangle, flattened\u003c/a\u003e",
				"Marin Knežević, Vedran Krčadinac, and Lucija Relić, \u003ca href=\"https://arxiv.org/abs/2012.15307\"\u003eMatrix products of binomial coefficients and unsigned Stirling numbers\u003c/a\u003e, arXiv:2012.15307 [math.CO], 2020."
			],
			"formula": [
				"From _Wolfdieter Lang_, Jun 20 2017: (Start)",
				"T(n, k) = Sum_{m=k..n} A007318(n, m)*A271703(m, k), n \u003e= k \u003e= 0, and 0 for k \u003c m. See also the name.",
				"E.g.f. of column k: exp(x)*(x/(1-x))^k/k! (Sheffer property), k \u003e= 0.",
				"E.g.f. of triangle (or row polynomials in x): exp(z)*exp((x*z/(1-z)).",
				"Recurrence for T(n, k), k \u003e= 1, with T(n, 0) = 1, T(n, k) = 0 if n \u003c k: T(n, k) = (n/k)*T(n-1, k-1) + n*T(n-1, k), n \u003e= 1, k = 1..n. (From the a-sequence with column k=0 as input.) (End)",
				"T(n, k) = Sum_{j=0..n-k} j!*binomial(n, j+k)*binomial(j+k, k)*binomial(j+k-1, k-1) with T(n, 0) = 1. - _G. C. Greubel_, Jan 09 2022"
			],
			"example": [
				"Triangle starts:",
				"  1;",
				"  1,    1;",
				"  1,    4,    1;",
				"  1,   15,    9,    1;",
				"  1,   64,   66,   16,   1;",
				"  1,  325,  490,  190,  25,  1;",
				"  1, 1956, 3915, 2120, 435, 36, 1;",
				"  ...",
				"Recurrence: T(3, 2) = (3/2)*4 + 3*1 = 9. - _Wolfdieter Lang_, Jun 20 2017"
			],
			"maple": [
				"L := (n,k) -\u003e `if`(k\u003c0 or k\u003en,0,(n-k)!*binomial(n,n-k)*binomial(n-1,n-k)):",
				"T := (n,k) -\u003e add(L(j,k)*binomial(-j-1,-n-1)*(-1)^(n-j), j=0..n):",
				"seq(seq(T(n,k), k=0..n), n=0..9);"
			],
			"mathematica": [
				"T[n_, k_]:= If[k==0, 1, Sum[((k*j!)/(j+k))*Binomial[n, j+k]*Binomial[j+k, k]^2, {j,0,n-k}]];",
				"Table[T[n, k], {n,0,12}, {k,0,n}]//Flatten (* _G. C. Greubel_, Jan 09 2022 *)"
			],
			"program": [
				"(MAGMA)",
				"B:=Binomial;",
				"A271705:= func\u003c n,k | k eq 0 select 1 else (\u0026+[B(n, j+k)*B(j+k, k)*B(j+k-1, k-1)*Factorial(j): j in [0..n-k]]) \u003e;",
				"[A271705(n,k): k in [0..n], n in [0..12]]; // _G. C. Greubel_, Jan 09 2022",
				"(Sage)",
				"b=binomial",
				"def A271705(n,k): return 1 if (k==0) else sum(factorial(j-k)*b(n, j)*b(j, k)*b(j-1, k-1) for j in (k..n))",
				"flatten([[A271705(n,k) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, Jan 09 2022"
			],
			"xref": [
				"Cf. A000290 (diag n, n-1), A062392 (diag n, n-2).",
				"Cf. A007526 (col. 1), A134432 (col. 2).",
				"Cf. A052844 (row sums), A059110 (matrix inverse).",
				"Cf. A007318, A271703, A288869."
			],
			"keyword": "nonn,easy,tabl,changed",
			"offset": "0,5",
			"author": "_Peter Luschny_, Apr 14 2016",
			"references": 5,
			"revision": 26,
			"time": "2022-01-09T09:48:47-05:00",
			"created": "2016-04-20T05:34:59-04:00"
		}
	]
}