{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A186492",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 186492,
			"data": "1,0,1,2,0,3,0,14,0,15,28,0,132,0,105,0,5556,0,1500,0,945,1112,0,10668,0,1995,0,10395,0,43784,0,212940,0,304290,0,135135,87568,0,1408992,0,4533480,0,5239080,0,2027025",
			"name": "Recursive triangle for calculating A186491.",
			"comment": [
				"The table entries are defined by a recurrence relation (see below).",
				"This triangle can be used to calculate the entries of A186491: the nonzero entries of the first column of the triangle give A186491.",
				"PRODUCTION MATRIX",
				"The production matrix P for this triangle is the bidiagonal matrix with the sequence [2,4,6,...] on the main subdiagonal, the sequence [1,3,5,...] on the main superdiagonal and 0's elsewhere: the first row of P^n is the n-th row of this triangle."
			],
			"link": [
				"C. V. Sukumar and A. Hodges, \u003ca href=\"https://doi.org/10.1098/rspa.2007.0003\"\u003eQuantum algebras and parity-dependent spectra\u003c/a\u003e, Proc. R. Soc. A (2007) 463, 2415-2427."
			],
			"formula": [
				"Recurrence relation",
				"(1)... T(n,k) = (2*k-1)*T(n-1,k-1)+(2*k+2)*T(n-1,k+1).",
				"GENERATING FUNCTION",
				"E.g.f. (Compare with the e.g.f. of A104035):",
				"(2)... 1/sqrt(cos(2*t)-u*sin(2*t)) = sum {n = 0..inf } R(n,u)*t^n/n! = 1 + u*t + (2+3*u^2)*t^2/2! + (14*u+15*u^3)*t^3/3!+....",
				"ROW POLYNOMIALS",
				"The row polynomials R(n,u) begin",
				"... R(1,u) = u",
				"... R(2,u) = 2+3*u^2",
				"... R(3,u) = 14*u+15*u^3",
				"... R(4,u) = 28+132*u^2+105u^4.",
				"They satisfy the recurrence relation",
				"(3)... R(n+1,u) = 2*(1+u^2)*d/du(R(n,u))+u*R(n,u) with starting value R(0,u) = 1.",
				"Compare with Formula (1) of A104035 for the polynomials Q_n(u).",
				"The polynomials R(n,u) are related to the shifted row polynomials A(n,u) of A142459 via",
				"(4)... R(n,u) = ((u+I)/2)^n*A(n+1,(u-I)/(u+I))",
				"with the inverse identity",
				"(5)... A(n+1,u) = (-I)^n*(1-u)^n*R(n,I*(1+u)/(1-u)),",
				"where {A(n,u)}n\u003e=1 begins [1,1+u,1+10*u+u^2,1+59*u+59*u^2+u^3,...] and I = sqrt(-1)."
			],
			"example": [
				"Table begins",
				"n\\k|.....0.....1......2.....3......4.....5......6",
				"=================================================",
				"0..|.....1",
				"1..|.....0.....1",
				"2..|.....2.....0......3",
				"3..|.....0....14......0....15",
				"4..|....28.....0....132.....0....105",
				"5..|.....0...556......0..1500......0...945",
				"6..|..1112.....0..10668.....0..19950.....0..10395",
				"..",
				"Examples of recurrence relation",
				"T(4,2) = 3*T(3,1) + 6*T(3,3) = 3*14 + 6*15 = 132;",
				"T(6,4) = 7*T(5,3) + 10*T(5,5) = 7*1500 + 10*945 = 19950."
			],
			"mathematica": [
				"R[0][_] = 1; R[1][u_] = u;",
				"R[n_][u_] := R[n][u] = 2(1+u^2) R[n-1]'[u] + u R[n-1][u];",
				"Table[CoefficientList[R[n][u], u], {n, 0, 8}] // Flatten (* _Jean-François Alcover_, Nov 13 2019 *)"
			],
			"xref": [
				"A104035, A142459, A144015 (row sums), A186491."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "0,4",
			"author": "_Peter Bala_, Feb 22 2011",
			"references": 4,
			"revision": 14,
			"time": "2019-11-13T06:58:39-05:00",
			"created": "2011-02-22T10:44:50-05:00"
		}
	]
}