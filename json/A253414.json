{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A253414",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 253414,
			"data": "1,1,-1,0,1,-1,-1,1,1,0,-1,-1,1,0,-1,1,1,0,-1,0,1,-1,-1,0,1,1,-1,-1,1,0,-1,1,1,0,-1,0,1,-1,-1,1,1,0,-1,-1,1,0,-1,0,1,1,-1,0,1,-1,-1,0,1,1,-1,-1,1,0,-1,1,1,0,-1,0,1,-1,-1,1,1,0,-1,-1,1,0,-1,1,1,0,-1,0,1,-1,-1,0,1,1,-1,-1,1,0,-1,0,1,1,-1,0,1",
			"name": "G.f. satisfies (1+x^2)*g(x) = 1 + x*g(x^2).",
			"comment": [
				"From _Peter Bala_, Jan 05 2015: (Start)",
				"The set of all terms is {-1,0,1}.",
				"Proof. It is not difficult to verify that the function g(x) defined by g(x) = (1 - x^2)*( 1/(1 - x^4) + x/(1 - x^8) + x^3/(1 - x^16) + x^7/(1 - x^32) + ...), for |x| \u003c 1, satisfies the functional equation (1 + x^2)*g(x) = 1 + x*g(x^2).",
				"We thus have g(x) = (1 - x^2)*S(x), where S(x) = sum {n\u003e=0} ( x^(4*n) + x^(8*n + 1) + x^(16*n + 3) + x^(32*n + 7) + ...). It is easy to see that the arithmetic progressions {4*n}, {8*n + 1}, {16*n + 3}, {32*n + 7}, ... are disjoint and hence S(x) has coefficients in {0,1}. It follows that g(x) = (1 - x^2)*S(x) has coefficients in {-1,0,1}. (End)"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A253414/b253414.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e"
			],
			"formula": [
				"a(2*k) = (-1)^k.",
				"a(1) = 1.",
				"a(2*k+1)=a(k)-a(2*k-1) for k \u003e= 1.",
				"G.f.: g(x) = 1/(1 + x^2) + x/((1 + x^2)*(1 + x^4)) + x^3/((1 + x^2)*(1 + x^4)*(1 + x^8)) + ... = (1 - x^2)*( 1/(1 - x^4) + x/(1 - x^8) + x^3/(1 - x^16) + ... ). - _Peter Bala_, Jan 05 2015"
			],
			"example": [
				"(1+x^2)*(1 + x - x^2 + x^4 + ...) = 1 + x*(1 + x^2 - x^4 + ...)"
			],
			"maple": [
				"N:= 100: # to get a(0) to a(N)",
				"A:= Array(0..N):",
				"for i from 0 to floor(N/2) do",
				"   A[2*i] := (-1)^i",
				"od:",
				"A[1]:= 1:",
				"for i from 1 to floor((N-1)/2) do",
				"   A[2*i+1]:= A[i] - A[2*i-1]",
				"od:",
				"seq(A[i],i=0..N);"
			],
			"mathematica": [
				"nmax = 100; sol = {a[0] -\u003e 1};",
				"Do[A[x_] = Sum[a[k] x^k, {k, 0, n}] /. sol; eq = CoefficientList[(1 + x^2) A[x] - (1 + x A[x^2]) + O[x]^(n + 1), x] == 0 /. sol; sol = sol ~Join~ Solve[eq][[1]], {n, 1, nmax}];",
				"sol /. Rule -\u003e Set;",
				"a /@ Range[0, nmax] (* _Jean-François Alcover_, Nov 01 2019 *)"
			],
			"keyword": "sign",
			"offset": "0",
			"author": "_Robert Israel_, Dec 31 2014",
			"references": 1,
			"revision": 16,
			"time": "2019-11-01T18:37:08-04:00",
			"created": "2015-01-04T23:02:26-05:00"
		}
	]
}