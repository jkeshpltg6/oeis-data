{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005812",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5812,
			"id": "M0111",
			"data": "0,1,2,1,2,3,2,3,2,1,2,3,2,3,4,3,4,3,2,3,4,3,4,3,2,3,2,1,2,3,2,3,4,3,4,3,2,3,4,3,4,5,4,5,4,3,4,5,4,5,4,3,4,3,2,3,4,3,4,5,4,5,4,3,4,5,4,5,4,3,4,3,2,3,4,3,4,3,2,3,2,1,2,3,2,3,4,3,4,3,2,3,4,3,4,5,4,5,4,3,4,5,4,5,4,3",
			"name": "Weight of balanced ternary representation of n.",
			"comment": [
				"Weight of n means count of nonzero digits of n. - _Daniel Forgues_, Mar 24 2010",
				"a(n) = A134022(n) + A134024(n) = A134021(n) - A134023(n)."
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Daniel Forgues, \u003ca href=\"/A005812/b005812.txt\"\u003eTable of n, a(n) for n = 0..100000\u003c/a\u003e",
				"P. Flajolet and Lyle Ramshaw, \u003ca href=\"http://dx.doi.org/10.1137/0209014\"\u003eA note on Gray code and odd-even merge\u003c/a\u003e, SIAM J. Comput. 9 (1980), 142-158.",
				"Michael Gilleland, \u003ca href=\"/selfsimilar.html\"\u003eSome Self-Similar Integer Sequences\u003c/a\u003e"
			],
			"formula": [
				"a(3n)=a(n), a(3n+1)=a(n)+1, a(9n+2)=a(n)+2, a(9n+5)=a(3n+2)+1, a(9n+8)=a(3n+2).",
				"a(n) = Sum_{k\u003e0} floor(|2*sin(n*Pi/3^k)|). - _Toshitaka Suzuki_, Sep 10 2006"
			],
			"mathematica": [
				"a[n_] := With[{q=Round[n/3]}, Abs[n-3q]+a[q]]; a[0]=0; Table[a[n], {n, 0, 105}](* _Jean-François Alcover_, Nov 25 2011, after Pari *)"
			],
			"program": [
				"(Lisp) (defun btw (n) (if (= n 0) 0 (multiple-value-bind (q r) (round n 3) (+ (abs r) (btw q)))))",
				"(PARI) a(n)=local(q); if(n\u003c=0,0,q=round(n/3); abs(n-3*q)+a(q))",
				"(Python)",
				"def a(n):",
				"    s=0",
				"    x=0",
				"    while n\u003e0:",
				"        x=n%3",
				"        n//=3",
				"        if x==2:",
				"            x=-1",
				"            n+=1",
				"        if x!=0: s+=1",
				"    return s",
				"print([a(n) for n in range(101)]) # _Indranil Ghosh_, Jun 07 2017"
			],
			"keyword": "easy,nonn,nice",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_, _Jeffrey Shallit_",
			"ext": [
				"Additional terms from _Allan C. Wechsler_"
			],
			"references": 7,
			"revision": 41,
			"time": "2021-04-30T06:07:00-04:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}