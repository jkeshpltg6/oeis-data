{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A132191",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 132191,
			"data": "1,1,2,1,4,3,1,6,9,4,1,12,18,16,5,1,12,54,40,25,6,1,40,72,160,75,36,7,1,28,405,280,375,126,49,8,1,96,390,2176,825,756,196,64,9,1,104,1944,2800,8125,2016,1372,288,81,10,1,280,3411,17920,13175,23976,4312,2304,405",
			"name": "Square array a(m,n) read by antidiagonals, defined by A000010(n)*a(m,n) = Sum_{k=1..n, gcd(k,n)=1} m^{ Sum_{d|n} A000010(d)/ (multiplicative order of k modulo d) }.",
			"comment": [
				"From _Andrew Howroyd_, Apr 22 2017: (Start)",
				"Number of step shifted (decimated) sequences of length n using a maximum of m different symbols. See A056371 for an explanation of step shifts. -",
				"Number of mappings with domain {0..n-1} and codomain {1..m} up to equivalence.  Mappings A and B are equivalent if there is a d, prime to n, such that A(i) = B(i*d mod n) for i in {0..n-1}. (End)"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A132191/b132191.txt\"\u003eTable of n, a(n) for n = 1..1830\u003c/a\u003e",
				"Max Alekseyev, \u003ca href=\"/A132191/a132191.txt\"\u003eFirst 20 rows and columns of table\u003c/a\u003e",
				"R. C. Titsworth, \u003ca href=\"http://projecteuclid.org/euclid.ijm/1256059671\"\u003eEquivalence classes of periodic sequences\u003c/a\u003e, Illinois J. Math., 8 (1964), 266-270."
			],
			"example": [
				"Array begins:",
				"1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...",
				"2, 4, 6, 12, 12, 40, 28, 96, 104, 280, 216, 1248, 704, 2800, 4344, 8928, 8232, 44224, 29204, 136032, ...",
				"3, 9, 18, 54, 72, 405, 390, 1944, 3411, 14985, 17802, 139968, 133104, 798525, 1804518, 5454378, 8072532, 64599849, 64573626, 437732424, ...",
				"4, 16, 40, 160, 280, 2176, 2800, 17920, 44224, 263296, 419872, 4280320, 5594000, 44751616, 134391040, 539054080, 1073758360, 11453771776, 15271054960, 137575813120, ...",
				"5, 25, 75, 375, 825, 8125, 13175, 103125, 327125, 2445625, 4884435, 61640625, 101732425, 1017323125, 3816215625, 19104609375, 47683838325, 635787765625, 1059638680675, 11924780390625, ..."
			],
			"mathematica": [
				"a[m_, n_] := (1/EulerPhi[n])*Sum[If[GCD[k, n]==1, m^DivisorSum[n, EulerPhi[#] / MultiplicativeOrder[k, #]\u0026], 0], {k, 1, n}]; Table[a[m-n+1, n], {m, 1, 15}, {n, m, 1, -1}] // Flatten (* _Jean-François Alcover_, Dec 01 2015 *)"
			],
			"program": [
				"(PARI) for(i=1,15,for(m=1,i,n=i-m+1; print1(sum(k=1, n, if(gcd(k,n)==1, m^sumdiv(n,d,eulerphi(d)/znorder(Mod(k,d))),0))/eulerphi(n)\",\"))) \\\\ Herman Jamke (hermanjamke(AT)fastmail.fm), Apr 26 2008"
			],
			"xref": [
				"Row m=2 is A056371",
				"Row m=3 is A056372",
				"Row m=4 is A056373",
				"Row m=5 is A056374",
				"Row m=6 is A056375",
				"Column n=2 is A000290",
				"Column n=3 is A002411",
				"Column n=4 is A019582",
				"Cf. A285522, A285548."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_, Dec 01 2007, based on email from _Max Alekseyev_, Nov 08 2007",
			"ext": [
				"More terms from Herman Jamke (hermanjamke(AT)fastmail.fm), Apr 26 2008",
				"Offset corrected by _Andrew Howroyd_, Apr 20 2017"
			],
			"references": 13,
			"revision": 33,
			"time": "2017-06-13T12:12:10-04:00",
			"created": "2007-12-09T03:00:00-05:00"
		}
	]
}