{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A008314",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 8314,
			"data": "1,1,1,1,1,3,1,4,3,1,5,10,1,6,15,10,1,7,21,35,1,8,28,56,35,1,9,36,84,126,1,10,45,120,210,126,1,11,55,165,330,462,1,12,66,220,495,792,462,1,13,78,286,715,1287,1716,1,14,91,364,1001,2002,3003,1716,1,15,105,455,1365,3003,5005",
			"name": "Irregular triangle read by rows: one half of the coefficients of the expansion of (2*x)^n in terms of Chebyshev T-polynomials.",
			"comment": [
				"The entry a(0,0) should actually be 1/2.",
				"The row lengths of this array are [1,1,2,2,3,3,...] = A004526.",
				"Row k also counts the binary strings of length k that have 0, 2 up to 2*floor(k/2) 'unmatched symbols'. See contributions by Marc van Leeuwen in Math StackExchange link. - _Wouter Meeussen_, Apr 17 2013",
				"For n \u003e= 1, T(n,k) is the coefficient of cos((n-2k)x) in the expression for 2^(n-1)*cos(x)^n as a sum of cosines of multiples of x.  It is binomial(n,k) if k \u003c n/2, while T(n,n/2) = binomial(n,n/2)/2 if n is even. - _Robert Israel_, Jul 25 2016"
			],
			"reference": [
				"M. Abramowitz and I. A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, 1964 (and various reprintings), p. 795.",
				"T. J. Rivlin, Chebyshev polynomials: from approximation theory to algebra and number theory, 2nd ed., Wiley, New York, 1990, pp. 54-55, Ex. 1.5.31."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A008314/b008314.txt\"\u003eTable of n, a(n) for n = 0..10099\u003c/a\u003e (rows 0 to 199, flattened)",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"Suyoung Choi and Hanchul Park, \u003ca href=\"http://arxiv.org/abs/1210.3776\"\u003eA new graph invariant arises in toric topology\u003c/a\u003e, arXiv preprint arXiv:1210.3776 [math.AT], 2012.",
				"Math StackExchange, \u003ca href=\"http://math.stackexchange.com/questions/345529\"\u003eBijection between number of partitions of 2n satisfying certain conditions with number of partitions of n\u003c/a\u003e, April-March 2013.",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e"
			],
			"formula": [
				"a(n,k) are the M_3 multinomial numbers A036040 for the partitions with m = 1 and 2 parts (in Abramowitz-Stegun order). - _Wolfdieter Lang_, Aug 01 2014"
			],
			"example": [
				"[1/2], [1], [1,2/2=1], [1,3], [1,4,6/2=3], [1,5,10], [1,6,15,20/2=10],...",
				"From _Wolfdieter Lang_, Aug 01 2014: (Start)",
				"This irregular triangle begins (even n has falling even T-polynomial indices, odd n has falling odd T-indices):",
				"n\\k  1  2   3   4     5     6     7     8 ...",
				"0: 1/2 (but a(0,1) = 1)",
				"1:   1",
				"2:   1  1",
				"3:   1  3",
				"4:   1  4   3",
				"5:   1  5  10",
				"6:   1  6  15  10",
				"7:   1  7  21  35",
				"8:   1  8  28  56    35",
				"9:   1  9  36  84   126",
				"10:  1 10  45 120   210   126",
				"11:  1 11  55 165   330   462",
				"12:  1 12  66 220   495   792   462",
				"13:  1 13  78 286   715  1287  1716",
				"14:  1 14  91 364  1001  2002  3003  1716",
				"15:  1 15 105 455  1365  3003  5005  6435",
				"...",
				"(2*x)^5 = 2*(1*T_5(x) + 5*T_3(x) + 10*T_1(x)),",
				"(2*x)^6 = 2*(1*T_6(x) + 6*T_4(x) + 15*T_3(x) + 10*T_0(x)).",
				"(End)"
			],
			"maple": [
				"F:= proc(n) local q;",
				"  q:= combine(2^(n-1)*cos(t)^n,trig);",
				"  if n::even then",
				"     seq(coeff(q,cos((n-2*j)*t)),j=0..n/2-1),eval(q,cos=0)",
				"  else",
				"     seq(coeff(q,cos((n-2*j)*t)),j=0..(n-1)/2)",
				"  fi",
				"end proc:",
				"1, seq(F(n),n=1..15); # _Robert Israel_, Jul 25 2016"
			],
			"mathematica": [
				"Table[(c/@ Range[n,0,-2]) /. Flatten[Solve[Thread[CoefficientList[Expand[1/2*(2*x)^n -Sum[c[k] ChebyshevT[k,x],{k,0,n}]],x]==0]]],{n,16}];",
				"(* or with combinatorics *)",
				"match[li:{(1|-1)..}]:= Block[{it=li,rot=0}, While[Length[Union[Join[it,{\"(\",\")\"}]]]\u003e3, rot++; it=RotateRight[it //.{a___,1,b___String,-1,c___} -\u003e{a,\"(\",b,\")\",c}]]; RotateLeft[it,rot] /. {(1|-1)-\u003e0, \"(\"-\u003e1,\")\"-\u003e-1}];",
				"Table[Last/@ Sort@ Tally[Table[Tr[Abs@ match[-1+2*IntegerDigits[n,2]]], {n,2^(k-1), 2^k-1}]], {k,1,16}]; (* _Wouter Meeussen_, Apr 17 2013 *)"
			],
			"xref": [
				"Cf. A007318, A008311.",
				"Bisection triangles: A122366 (odd numbered rows), A127673 (even numbered rows)."
			],
			"keyword": "nonn,tabf",
			"offset": "0,6",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Name reformulated by _Wolfdieter Lang_, Aug 01 2014"
			],
			"references": 5,
			"revision": 50,
			"time": "2019-11-10T20:27:45-05:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}