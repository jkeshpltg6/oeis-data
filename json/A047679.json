{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A047679",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 47679,
			"data": "1,2,1,3,3,2,1,4,5,5,4,3,3,2,1,5,7,8,7,7,8,7,5,4,5,5,4,3,3,2,1,6,9,11,10,11,13,12,9,9,12,13,11,10,11,9,6,5,7,8,7,7,8,7,5,4,5,5,4,3,3,2,1,7,11,14,13,15,18,17,13,14,19,21,18,17,19,16,11,11,16,19,17,18",
			"name": "Denominators in full Stern-Brocot tree.",
			"comment": [
				"Numerators are A007305.",
				"Write n in binary; list run lengths; add 1 to last run length; make into continued fraction. Sequence gives denominator of fraction obtained.",
				"From _Reinhard Zumkeller_, Dec 22 2008: (Start)",
				"  For n \u003e 1: a(n) = if A025480(n-1) != 0 and A025480(n) != 0 then = a(A025480(n-1)) + a(A025480(n)) else if A025480(n)=0 then a(A025480(n-1))+0 else 1+a(A025480(n-1));",
				"  a(n) = A007305(A054429(n)+2) and a(A054429(n)) = A007305(n+2);",
				"  A153036(n+1) = floor(A007305(n+2)/a(n)). (End)",
				"From _Yosu Yurramendi_, Jun 25 2014 and Jun 30 2014: (Start)",
				"If the terms are written as an array a(m, k) = a(2^(m-1)-1+k) with m \u003e= 1 and k = 0, 1, ..., 2^(m-1)-1:",
				"1,",
				"2,1,",
				"3,3, 2, 1,",
				"4,5, 5, 4, 3, 3, 2,1,",
				"5,7, 8, 7, 7, 8, 7,5,4, 5, 5, 4, 3, 3,2,1,",
				"6,9,11,10,11,13,12,9,9,12,13,11,10,11,9,6,5,7,8,7,7,8,7,5,4,5,5,4,3,3,2,1,",
				"then the sum of the m-th row is 3^(m-1), and each column is an arithmetic sequence. The differences of these arithmetic sequences give the sequence A007306(k+1). The first terms of columns are 1 for k = 0 and a(k-1) for k \u003e= 1.",
				"In a row reversed version A(m, k) = a(m, m-(k+1)):",
				"1",
				"1,2",
				"1,2,3,3,",
				"1,2,3,3,4,5,5,4",
				"1,2,3,3,4,5,5,4,5,7,8,7,7,8,7,5",
				"1,2,3,3,4,5,5,4,5,7,8,7,7,8,7,5,6,9,11,10,11,13,12,12,9,9,12,13,11,10,11,9,6",
				"each column k \u003e= 0 is constant, namely A007306(k+1).",
				"This row reversed version coincides with the array for A007305 (see the Jun 25 2014 comment there). (End)",
				"Looking at the plot, the sequence clearly shows a fractal structure. (The repeating pattern oddly resembles the [first completed] facade of the Sagrada Familia!) - _Daniel Forgues_, Nov 15 2019"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A047679/b047679.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/stern_brocot.html\"\u003eStern-Brocot or Farey Tree\u003c/a\u003e",
				"\u003ca href=\"/index/Fo#fraction_trees\"\u003eIndex entries for fraction trees\u003c/a\u003e",
				"\u003ca href=\"/index/St#Stern\"\u003eIndex entries for sequences related to Stern's sequences\u003c/a\u003e"
			],
			"formula": [
				"a(n) = SternBrocotTreeDen(n) # n starting from 1.",
				"From _Yosu Yurramendi_, Jul 02 2014: (Start)",
				"For m \u003e0 and 0 \u003c= k \u003c 2^(m-1), with a(0)=1, a(1)=2:",
				"a(2^m+k-1) = a(2^(m-1)+k-1) + a((2^m-1)-k-1);",
				"a(2^m+2^(m-1)+k-1) = a(2^(m-1)+k-1). (End)",
				"a(2^m-2^q  ) = q+1, q \u003e= 0, m \u003e q",
				"a(2^m-2^q-1) = q+2, q \u003e= 0, m \u003e q+1. - _Yosu Yurramendi_, Jan 01 2015",
				"a(2^(m+1)-1-k) = A007306(k+1), m \u003e= 0, 0 \u003c= k \u003c= 2^m. - _Yosu Yurramendi_, May 20 2019",
				"a(n) = A002487(1+A059893(n)), n \u003e 0. - _Yosu Yurramendi_, Sep 29 2021"
			],
			"example": [
				"E.g., 57-\u003e111001-\u003e[ 3,2,1 ]-\u003e[ 3,2,2 ]-\u003e3 + 1/(2 + 1/(2) ) = 17/2. For n=1,2, ... we get 2, 3/2, 3, 4/3, 5/3, 5/2, 4, 5/4, 7/5, 8/5, ...",
				"1; 2,1; 3,3,2,1; 4,5,5,4,3,3,2,1; ....",
				"Another version of Stern-Brocot is A007305/A047679 = 1, 2, 1/2, 3, 1/3, 3/2, 2/3, 4, 1/4, 4/3, 3/4, 5/2, 2/5, 5/3, 3/5, 5, 1/5, 5/4, 4/5, ..."
			],
			"maple": [
				"SternBrocotTreeDen := n -\u003e SternBrocotTreeNum(((3*(2^floor_log_2(n)))-n)-1); # SternBrocotTreeNum given in A007305 and (((3*(2^floor_log_2(n)))-n)-1) is equal to A054429[n]."
			],
			"mathematica": [
				"CFruns[ n_Integer ] := Fold[ #2+1/#1\u0026, Infinity, Reverse[ MapAt[ #+1\u0026, Length/@Split[ IntegerDigits[ n, 2 ] ], {-1} ] ] ]",
				"(* second program: *)",
				"a[n_] := Module[{LL = Length /@ Split[IntegerDigits[n, 2]]}, LL[[-1]] += 1; FromContinuedFraction[LL] // Denominator]; Table[a[n], {n, 1, 100}] (* _Jean-François Alcover_, Feb 25 2016 *)"
			],
			"program": [
				"(PARI) {a(n) = local(v, w); v = binary(n++); w = [1]; for( n=2, #v, if( v[n] != v[n-1], w = concat(w, 1), w[#w]++)); w[#w]++; contfracpnqn(w)[2, 1]} /* _Michael Somos_, Jul 22 2011 */",
				"(R)",
				"a \u003c- 1",
				"for(m in 1:6) for(k in 0:(2^(m-1)-1)) {",
				"  a[2^m+        k] = a[2^(m-1)+k] + a[2^m-k-1]",
				"  a[2^m+2^(m-1)+k] = a[2^(m-1)+k]",
				"}",
				"a",
				"# _Yosu Yurramendi_, Dec 31 2014"
			],
			"xref": [
				"Cf. A007305, A007306, A054424, A152976."
			],
			"keyword": "nonn,easy,frac,nice,tabf,look",
			"offset": "0,2",
			"author": "_Wouter Meeussen_",
			"ext": [
				"Edited by _Wolfdieter Lang_, Mar 31 2015"
			],
			"references": 40,
			"revision": 89,
			"time": "2021-09-29T11:20:06-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}