{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A259657",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 259657,
			"data": "1,0,0,-2,-3,0,0,6,0,0,0,0,8,0,0,-12,-9,0,0,6,0,0,0,0,12,0,0,-2,-12,0,0,18,0,0,0,0,6,0,0,-24,-12,0,0,6,0,0,0,0,20,0,0,-12,-12,0,0,24,0,0,0,0,24,0,0,-12,-21,0,0,6,0,0,0,0,0,0,0,-14,-24",
			"name": "Expansion of phi(-x^3) * f(-x^4)^3 / f(-x^12) in powers of x where phi(), f() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Cubic AGM theta functions: a(q) (see A004016), b(q) (A005928), c(q) (A005882)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A259657/b259657.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of phi(-x^3) * b(x^4) in powers of x where phi() is a Ramanujan theta function and b() is a cubic AGM theta function.",
				"Expansion of eta(q^3)^2 * eta(q^4)^3 / (eta(q^6) * eta(q^12)) in powers of q.",
				"Euler transform of period 12 sequence [ 0, 0, -2, -3, 0, -1, 0, -3, -2, 0, 0, -3, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (72 t)) = 72^(3/2) (t/i)^(3/2) g(t) where q = exp(2 Pi i t) and g(t) is the g.f. for A259655.",
				"G.f.: Product_{k\u003e0} (1 - x^(4*k))^3 / ((1 + x^(3*k))^2 * (1 + x^(6*k))).",
				"a(3*n + 1) = -3 * A143161(n-1). a(3*n + 2) = a(4*n + 1) = a(4*n + 2) = 0. a(12*n) = A014453(n)."
			],
			"example": [
				"G.f. = 1 - 2*x^3 - 3*x^4 + 6*x^7 + 8*x^12 - 12*x^15 - 9*x^16 + 6*x^19 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 4, 0, x^3] QPochhammer[ x^4]^3 / QPochhammer[ x^12], {x, 0, n}];",
				"a[ n_] := SeriesCoefficient[ QPochhammer[x^4]^3 / (QPochhammer[ -x^3, x^3]^2 QPochhammer[ -x^6, x^6]), {x, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^3 + A)^2 * eta(x^4 + A)^3 / (eta(x^6 + A) * eta(x^12 + A)), n))};"
			],
			"xref": [
				"Cf. A014453, A143161, A259655."
			],
			"keyword": "sign",
			"offset": "0,4",
			"author": "_Michael Somos_, Jul 02 2015",
			"references": 1,
			"revision": 9,
			"time": "2021-03-12T22:24:48-05:00",
			"created": "2015-07-02T16:50:57-04:00"
		}
	]
}