{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A176261",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 176261,
			"data": "1,1,1,1,-2,1,1,-2,-2,1,1,-11,-11,-11,1,1,-20,-29,-29,-20,1,1,-56,-74,-83,-74,-56,1,1,-119,-173,-191,-191,-173,-119,1,1,-290,-407,-461,-470,-461,-407,-290,1,1,-650,-938,-1055,-1100,-1100,-1055,-938,-650,1",
			"name": "Triangle T(n,k) = A006130(k) - A006130(n) + A006130(n-k) read by rows.",
			"comment": [
				"Row sums are s(n) = {1, 2, 0, -2, -31, -96, -341, -964, -2784, -7484, -20041, ...}, obey s(n) = 3*s(n-1) + 3*s(n-2) - 11*s(n-3) - 3*s(n-4) + 9*s(n-5) and have g.f. (1-x+3*x^3-9*x^2)/((1-x)*(1-x-3*x^2)^2)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A176261/b176261.txt\"\u003eRows n = 0..100 of triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = T(n,n-k).",
				"T(n,k) = A006130(k) - A006130(n) + A006130(n-k), where A006130(n) = Sum_{j=0..n} binomial(n-j, j)*3^j. - _G. C. Greubel_, Nov 24 2019"
			],
			"example": [
				"Triangle begins as:",
				"  1;",
				"  1,     1;",
				"  1,    -2,     1;",
				"  1,    -2,    -2,     1;",
				"  1,   -11,   -11,   -11,     1;",
				"  1,   -20,   -29,   -29,   -20,     1;",
				"  1,   -56,   -74,   -83,   -74,   -56,     1;",
				"  1,  -119,  -173,  -191,  -191,  -173,  -119,     1;",
				"  1,  -290,  -407,  -461,  -470,  -461,  -407,  -290,     1;",
				"  1,  -650,  -938, -1055, -1100, -1100, -1055,  -938,  -650,     1;",
				"  1, -1523, -2171, -2459, -2567, -2603, -2567, -2459, -2171, -1523, 1;"
			],
			"maple": [
				"A176261 := proc(n,k)",
				"        A006130(k)-A006130(n)+A006130(n-k) ;",
				"end proc; # _R. J. Mathar_, May 03 2013"
			],
			"mathematica": [
				"A006130[n_]:= Sum[Binomial[n-j,j]*3^j, {j,0,n}]; T[n_,k_]:= A006130[k] - A006130[n] + A006130[n-k]; Table[T[n,k], {n,0,10}, {k,0,n}]//Flatten (* modified by _G. C. Greubel_, Nov 24 2019 *)"
			],
			"program": [
				"(PARI) A006130(n) = sum(j=0,n,binomial(n-j,j)*3^j);",
				"T(n,k) = A006130(k) -A006130(n) +A006130(n-k); \\\\ _G. C. Greubel_, Nov 24 2019",
				"(MAGMA) A006130:= func\u003c n | \u0026+[Binomial(n-j,j)*3^j: j in [0..n]] \u003e;",
				"[A006130(k) -A006130(n) +A006130(n-k): k in [0..n], n in [0..10]]; // _G. C. Greubel_, Nov 24 2019",
				"(Sage)",
				"def A006130(n): return sum(binomial(n-j,j)*3^j for j in (0..n))",
				"[[A006130(k) -A006130(n) +A006130(n-k) for k in (0..n)] for n in (0..10)] # _G. C. Greubel_, Nov 24 2019"
			],
			"xref": [
				"Cf. A006130."
			],
			"keyword": "sign,tabl,easy",
			"offset": "0,5",
			"author": "_Roger L. Bagula_, Apr 13 2010",
			"references": 1,
			"revision": 14,
			"time": "2019-11-24T14:09:15-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}