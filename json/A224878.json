{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A224878",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 224878,
			"data": "1,1,0,1,1,0,1,1,0,1,3,0,1,2,1,0,1,3,2,0,1,5,2,0,1,4,5,0,1,4,6,1,0,1,6,8,1,0,1,7,9,3,0,1,6,13,4,0,1,7,15,7,0,1,7,18,10,0,1,8,20,14,1,0,1,11,23,17,2,0,1,8,28,24,3,0,1,9,31,30,5,0,1",
			"name": "Number T(n,k) of partitions of n into distinct parts with boundary size k (where one part of size 0 is allowed).",
			"comment": [
				"Boundary size of a partition (or set) is the number of parts (elements) having fewer than 2 neighbors.",
				"T(n,k) is also the number of subsets of {0, 1, 2, ...} whose elements sum to n and that have k elements in its boundary."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A224878/b224878.txt\"\u003eRows n = 0..600, flattened\u003c/a\u003e"
			],
			"example": [
				"T(9,1) = 1: [9].",
				"T(9,2) = 6: [0,9], [1,8], [2,7], [3,6], [4,5], [2,3,4].",
				"T(9,3) = 8: [1,2,6], [1,3,5], [0,1,8], [0,2,7], [0,3,6], [0,4,5], [0,2,3,4], [0,1,2,6].",
				"T(9,4) = 1: [0,1,3,5].",
				"Triangle T(n,k) begins:",
				"1, 1; (namely, the empty set and the set {0})",
				"0, 1, 1;",
				"0, 1, 1;",
				"0, 1, 3;",
				"0, 1, 2,  1;",
				"0, 1, 3,  2;",
				"0, 1, 5,  2;",
				"0, 1, 4,  5;",
				"0, 1, 4,  6, 1;",
				"0, 1, 6,  8, 1;",
				"0, 1, 7,  9, 3;",
				"0, 1, 6, 13, 4;",
				"0, 1, 7, 15, 7;"
			],
			"maple": [
				"b:= proc(n, i, t) option remember; `if`(n=0 and i\u003c0, `if`(t\u003e1, x, 1),",
				"      expand(`if`(i\u003c0, 0, `if`(t\u003e1, x, 1)*b(n, i-1, iquo(t, 2))+",
				"      `if`(i\u003en, 0, `if`(t=2, x, 1)*b(n-i, i-1, iquo(t, 2)+2)))))",
				"    end:",
				"T:= n-\u003e (p-\u003eseq(coeff(p, x, i), i=0..degree(p)))(b(n$2, 0)):",
				"seq(T(n), n=0..30);  # _Alois P. Heinz_, Jul 23 2013"
			],
			"mathematica": [
				"b[n_, i_, t_] := b[n, i, t] = If[n==0 \u0026\u0026 i\u003c0, If[t\u003e1, x, 1], Expand[If[i\u003c0, 0, If[t\u003e1, x, 1]*b[n, i-1, Quotient[t, 2]] + If[i\u003en, 0, If[t==2, x, 1] * b[n-i, i-1, Quotient[t, 2]+2]]]]]; T[n_] := Function[p, Table[ Coefficient[ p, x, i], {i, 0, Exponent[p, x]}]][b[n, n, 0]]; Table[T[n], {n, 0, 30}] // Flatten (* _Jean-François Alcover_, Feb 07 2017, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A227551 (no parts of size 0 are allowed).",
				"Row sums are twice A000009."
			],
			"keyword": "nonn,tabf",
			"offset": "0,11",
			"author": "_Patrick Devlin_, Jul 23 2013",
			"references": 2,
			"revision": 15,
			"time": "2017-02-07T06:40:36-05:00",
			"created": "2013-07-23T18:27:06-04:00"
		}
	]
}