{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A260637",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 260637,
			"data": "28,35,56,91,140,203,280,371,476,595,728,875,1036,1211,1400,1603,1820,2051,2296,2555,2828,3115,3416,3731,4060,4403,4760,5131,5516,5915,6328,6755,7196,7651,8120,8603,9100,9611,10136,10675,11228,11795,12376,12971",
			"name": "Sums of seven consecutive squares: a(n) = n^2 + (n+1)^2 + (n+2)^2 + (n+3)^2 + (n+4)^2 + (n+5)^2 + (n+6)^2.",
			"comment": [
				"a(n) is defined for any n in Z and a(-n) = a(n-6).",
				"There are no primes or squares in the sequence because a(n) is a multiple of 7 and 7 is with multiplicity 1: a(n) = 7*((n+3)^2 + 4), and the factor (n+3)^2 + 4 is not a multiple of 7 for any n. A001032 gives the integers k such that the sum of k consecutive squares is a square."
			],
			"link": [
				"Jean-Christophe Hervé, \u003ca href=\"/A260637/b260637.txt\"\u003eTable of n, a(n) for n = -3..1000\u003c/a\u003e",
				"Patrick De Geest, \u003ca href=\"http://www.worldofnumbers.com/index.html\"\u003eWorld!Of Numbers\u003c/a\u003e",
				"\u003ca href=\"/index/Tu#2wis\"\u003eIndex entries for two-way infinite sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-3,1)."
			],
			"formula": [
				"a(n) = 7*n^2 + 42*n + 91 = 7*(n^2 + 6*n + 13) = 7*((n+3)^2 + 4).",
				"a(n) = 3*a(n-1) - 3*a(n-2) + a(n-3) = a(n-1) + 7*(2*n+7).",
				"G.f.: -7*(5*x^2-7*x+4) / (x^3*(x-1)^3). - _Colin Barker_, Nov 12 2015"
			],
			"maple": [
				"A260637:=n-\u003e7*((n+3)^2 + 4): seq(A260637(n), n=-3..50); # _Wesley Ivan Hurt_, Nov 17 2015"
			],
			"mathematica": [
				"Table[Plus@@(Range[n, n + 6]^2), {n, -3, 96}]"
			],
			"program": [
				"(PARI) vector(100, n, n--; n^2+(n+1)^2+(n+2)^2+(n+3)^2+(n+4)^2+(n+5)^2+(n+6)^2).",
				"(PARI) a(n) = 7*n^2 + 42*n + 91;",
				"vector(50, n, a(n-4)) \\\\ _Altug Alkan_, Nov 11 2015",
				"(PARI) Vec(-7*(5*x^2-7*x+4)/(x^3*(x-1)^3) + O(x^100)) \\\\ _Colin Barker_, Nov 12 2015",
				"(MAGMA) [7*((n+3)^2 + 4) : n in [-3..50]]; // _Wesley Ivan Hurt_, Nov 17 2015"
			],
			"xref": [
				"Cf. A000290, A001844, A120328, A027575, A027578, A027865.",
				"Cf. A001032."
			],
			"keyword": "nonn,easy",
			"offset": "-3,1",
			"author": "_Jean-Christophe Hervé_, Nov 11 2015",
			"references": 3,
			"revision": 25,
			"time": "2015-11-25T21:33:00-05:00",
			"created": "2015-11-25T21:33:00-05:00"
		}
	]
}