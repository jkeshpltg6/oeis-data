{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A154227",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 154227,
			"data": "1,1,1,1,8,1,1,19,19,1,1,35,158,35,1,1,57,592,592,57,1,1,86,1629,5608,1629,86,1,1,123,3767,28549,28549,3767,123,1,1,169,7760,105621,309458,105621,7760,169,1,1,225,14694,320566,1985274,1985274,320566,14694,225,1",
			"name": "Triangle T(n, k) = T(n-1, k) + T(n-1, k-1) + ((n+1)*(n+2)/2)*T(n-2, k-1), read by rows.",
			"comment": [
				"Row sums are: {1, 2, 10, 40, 230, 1300, 9040, 64880, 536560, 4641520, ...}.",
				"The row sums of this class of sequences (see Cf section) is given by the following. Let S(n) be the row sum then S(n) = 2*S(n-1) + f(n)*S(n-2) for a given f(n). For this sequence f(n) = binomial(n+2, 2). - _G. C. Greubel_, Mar 02 2021"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A154227/b154227.txt\"\u003eRows n = 0..50 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = T(n-1, k) + T(n-1, k-1) + ((n+1)*(n+2)/2)*T(n-2, k-1) with T(n, 0) = T(n, n) = 1."
			],
			"example": [
				"Triangle begins as:",
				"  1;",
				"  1,   1;",
				"  1,   8,     1;",
				"  1,  19,    19,      1;",
				"  1,  35,   158,     35,       1;",
				"  1,  57,   592,    592,      57,       1;",
				"  1,  86,  1629,   5608,    1629,      86,      1;",
				"  1, 123,  3767,  28549,   28549,    3767,    123,     1;",
				"  1, 169,  7760, 105621,  309458,  105621,   7760,   169,   1;",
				"  1, 225, 14694, 320566, 1985274, 1985274, 320566, 14694, 225, 1;"
			],
			"maple": [
				"T:= proc(n, k) option remember;",
				"      if k=0 or k=n then 1",
				"    else T(n-1, k) + T(n-1, k-1) + binomial(n+2,2)*T(n-2, k-1)",
				"      fi; end:",
				"seq(seq(T(n, k), k=0..n), n=0..12); # _G. C. Greubel_, Mar 02 2021"
			],
			"mathematica": [
				"T[n_, k_]:= T[n,k]= If[k==0 || k==n, 1, T[n-1, k] + T[n-1, k-1] + Binomial[n+2, 2]*T[n-2, k-1] ];",
				"Table[T[n, k], {n,0,12}, {k,0,n}]//Flatten (* modified by _G. C. Greubel_, Mar 02 2021 *)"
			],
			"program": [
				"(Sage)",
				"def f(n): return binomial(n+2,2)",
				"def T(n,k):",
				"    if (k==0 or k==n): return 1",
				"    else: return T(n-1, k) + T(n-1, k-1) + f(n)*T(n-2, k-1)",
				"flatten([[T(n,k) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, Mar 02 2021",
				"(Magma)",
				"f:= func\u003c n | Binomial(n+2,2) \u003e;",
				"function T(n,k)",
				"  if k eq 0 or k eq n then return 1;",
				"  else return T(n-1, k) + T(n-1, k-1) + f(n)*T(n-2, k-1);",
				"  end if; return T;",
				"end function;",
				"[T(n,k): k in [0..n], n in [0..12]]; // _G. C. Greubel_, Mar 02 2021"
			],
			"xref": [
				"Cf. A154228, A154229, A154230, A154231, A154233."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Roger L. Bagula_, Jan 05 2009",
			"ext": [
				"Edited by _G. C. Greubel_, Mar 02 2021"
			],
			"references": 6,
			"revision": 5,
			"time": "2021-03-02T16:18:12-05:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}