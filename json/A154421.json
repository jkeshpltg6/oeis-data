{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A154421",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 154421,
			"data": "0,0,0,0,0,1,1,3,2,5,2,5,2,4,3,4,4,5,2,6,2,7,5,7,3,9,3,9,4,7,3,6,4,9,3,10,3,8,4,6,5,8,6,8,3,9,4,8,6,8",
			"name": "Number of ways to express n as the sum of an odd prime, a positive Fibonacci number and an even Lucas number.",
			"comment": [
				"On Jan 09 2009, _Zhi-Wei Sun_ conjectured that a(n)\u003e0 for all n=6,7,.... ; in other words, any integer n\u003e5 can be written in the form p+F_s+L_{3t} with p an odd prime, s positive and t nonnegative. [Compare this with the conjecture related to the sequence A154290.] Sun verified the above conjecture up to 5*10^6 and Qing-Hu Hou continued the verification up to 2*10^8. If we set v_0=2, v_1=4 and v_{n+1}=4v_n+v_{n-1} for n=1,2,3,..., then L_{3t}=v_t is at least 4^t for every t=0,1,2,.... On Jan 17 2009, _D. S. McNeil_ found that 36930553345551 cannot be written as the sum of a prime, a Fibonacci number and an even Lucas number."
			],
			"reference": [
				"R. Crocker, On a sum of a prime and two powers of two, Pacific J. Math. 36(1971), 103-107."
			],
			"link": [
				"Zhi-Wei SUN, \u003ca href=\"/A154421/b154421.txt\"\u003eTable of n, a(n), n=1..50000.\u003c/a\u003e",
				"D. S. McNeil, \u003ca href=\"https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;bda1acb4.0812\"\u003eSun's strong conjecture\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;6434d742.0812\"\u003eA summary concerning my conjecture n=p+F_s+F_t\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;a9e706e.0812\"\u003eA summary concerning my conjecture n=p+F_s+F_t (II)\u003c/a\u003e",
				"Terence Tao, \u003ca href=\"http://arxiv.org/abs/0802.3361\"\u003eA remark on primality testing and decimal expansions\u003c/a\u003e, Journal of the Australian Mathematical Society 91:3 (2011), pp. 405-413.",
				"K. J. Wu and Z. W. Sun, Covers of the integers with odd moduli and their applications to the forms x^m-2^n and x^2-F_{3n}/2, Math. Comp. 78 (2009) 1853, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-09-02212-1\"\u003e[DOI]\u003c/a\u003e, \u003ca href=\"http://arxiv.org/abs/math/0702382\"\u003earXiv:math.NT/0702382\u003c/a\u003e"
			],
			"formula": [
				"a(n) = |{\u003cp,s,t\u003e: p+F_s+L_{3t}=n with p an odd prime, s\u003e1 and t nonnegative}|."
			],
			"example": [
				"For n=8 the a(8)=3 solutions are 3 + F_4 + L_0, 3 + F_2 + L_3, 5 + F_2 + L_0."
			],
			"mathematica": [
				"PQ[m_]:=m\u003e2\u0026\u0026PrimeQ[m] RN[n_]:=Sum[If[PQ[n-2*Fibonacci[3x+1]+Fibonacci[3x]-Fibonacci[y]],1,0], {x,0,Log[2,n]},{y,2,2*Log[2,Max[2,n-2*Fibonacci[3x+1]+Fibonacci[3x]]]}] Do[Print[n,\" \",RN[n]];Continue,{n,1,50000}]"
			],
			"xref": [
				"Cf. A000040, A000045, A000032, A154257, A154285, A154290, A154364, A154417, A156695."
			],
			"keyword": "nonn",
			"offset": "1,8",
			"author": "_Zhi-Wei Sun_, Jan 09 2009",
			"ext": [
				"McNeil's counterexample added by _Zhi-Wei Sun_, Jan 20 2009"
			],
			"references": 2,
			"revision": 16,
			"time": "2018-09-11T08:48:05-04:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}