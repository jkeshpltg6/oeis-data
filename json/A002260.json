{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002260",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2260,
			"data": "1,1,2,1,2,3,1,2,3,4,1,2,3,4,5,1,2,3,4,5,6,1,2,3,4,5,6,7,1,2,3,4,5,6,7,8,1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9,10,1,2,3,4,5,6,7,8,9,10,11,1,2,3,4,5,6,7,8,9,10,11,12,1,2,3,4,5,6,7,8,9,10,11,12,13,1,2,3,4,5,6,7,8,9,10,11,12,13,14",
			"name": "Triangle read by rows: T(n,k) = k for n \u003e= 1, k = 1..n.",
			"comment": [
				"Old name: integers 1 to k followed by integers 1 to k+1 etc. (a fractal sequence).",
				"Start counting again and again.",
				"This is a \"doubly fractal sequence\" - see the _Franklin T. Adams-Watters_ link.",
				"The PARI functions t1, t2 can be used to read a square array T(n,k) (n \u003e= 1, k \u003e= 1) by antidiagonals downwards: n -\u003e T(t1(n), t2(n)). - _Michael Somos_, Aug 23 2002",
				"Reading this sequence as the antidiagonals of a rectangular array, row n is (n,n,n,...); this is the weight array (Cf. A144112) of the array A127779 (rectangular). - _Clark Kimberling_, Sep 16 2008",
				"The upper trim of an arbitrary fractal sequence s is s, but the lower trim of s, although a fractal sequence, need not be s itself. However, the lower trim of A002260 is A002260. (The upper trim of s is what remains after the first occurrence of each term is deleted; the lower trim of s is what remains after all 0's are deleted from the sequence s-1.) - _Clark Kimberling_, Nov 02 2009",
				"Eigensequence of the triangle = A001710 starting (1, 3, 12, 60, 360, ...). - _Gary W. Adamson_, Aug 02 2010",
				"The triangle sums, see A180662 for their definitions, link this triangle of natural numbers with twenty-three different sequences, see the crossrefs. The mirror image of this triangle is A004736. - _Johannes W. Meijer_, Sep 22 2010",
				"From _Paul Curtz_, Jul 25 2011: (Start)",
				"Akiyama-Tanigawa algorithm from A000027(n) gives",
				"   1,  2,  3,  4,  5,  6,  7,  8,",
				"  -1, -2, -3, -4, -5, -6, -7, -8,",
				"   1,  2,  3,  4,  5,  6,  7,  8,",
				"  -1, -2, -3, -4, -5, -6, -7, -8.",
				"By antidiagonals:",
				"   1,",
				"  -1,  2,",
				"   1, -2,  3,",
				"  -1,  2, -3,  4,",
				"   1, -2,  3, -4,  5,",
				"  -1,  2, -3,  4, -5,  6.",
				"Row sum = A016116. (End)",
				"A002260 is the self-fission of the polynomial sequence (q(n,x)), where q(n,x) = x^n +  x^(n-1) + ... + x + 1. See A193842 for the definition of fission. - _Clark Kimberling_, Aug 07 2011",
				"Sequence B is called a reluctant sequence of sequence A, if B is triangle array read by rows: row number k coincides with first k elements of the sequence A. Sequence A002260 is reluctant sequence of sequence 1,2,3,... (A000027). - _Boris Putievskiy_, Dec 12 2012",
				"This is the maximal sequence of positive integers, such that once an integer k has occurred, the number of k's always exceeds the number of (k+1)'s for the remainder of the sequence, with the first occurrence of the integers being in order. - _Franklin T. Adams-Watters_, Oct 23 2013",
				"A002260 are the k antidiagonal numerators of rationals in Cantor's proof of 1-to-1 correspondence between rationals and naturals; the denominators are k-numerator+1. - _Adriano Caroli_, Mar 24 2015",
				"T(n,k) gives the distance to the largest triangular number \u003c n. - _Ctibor O. Zizka_, Apr 09 2020"
			],
			"reference": [
				"Clark Kimberling, \"Fractal sequences and interspersions,\" Ars Combinatoria 45 (1997) 157-168. (Introduces upper trimming, lower trimming, and signature sequences.)",
				"M. Myers, Smarandache Crescendo Subsequences, R. H. Wilde, An Anthology in Memoriam, Bristol Banner Books, Bristol, 1998, p. 19.",
				"F. Smarandache, Sequences of Numbers Involved in Unsolved Problems, Hexis, Phoenix, 2006."
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/A002260/b002260.txt\"\u003eTable of n, a(n) for n = 1..11325\u003c/a\u003e",
				"Franklin T. Adams-Watters, \u003ca href=\"/A002260/a002260.txt\"\u003eDoubly Fractal Sequences\u003c/a\u003e",
				"Matin Amini and Majid Jahangiri, \u003ca href=\"https://arxiv.org/abs/1612.09481\"\u003eA Novel Proof for Kimberling’s Conjecture on Doubly Fractal Sequences\u003c/a\u003e, arXiv:1612.09481 [math.NT], 2017.",
				"Bruno Berselli, \u003ca href=\"/A002260/a002260.jpg\"\u003eIllustration of the initial terms\u003c/a\u003e",
				"Jerry Brown et al., \u003ca href=\"https://doi.org/10.1111/j.1949-8594.1997.tb17373.x\"\u003eProblem 4619\u003c/a\u003e, School Science and Mathematics (USA), Vol. 97(4), 1997, pp. 221-222.",
				"Glen Joyce C. Dulatre, Jamilah V. Alarcon, Vhenedict M. Florida, and Daisy Ann A. Disu, \u003ca href=\"http://docplayer.net/87034980-Vol-15-no-2-april-2017-dmmmsu-cas-science-monitor.html\"\u003eOn Fractal Sequences\u003c/a\u003e, DMMMSU-CAS Science Monitor (2016-2017) Vol. 15 No. 2, 109-113.",
				"Clark Kimberling, \u003ca href=\"http://faculty.evansville.edu/ck6/integer/fractals.html\"\u003eFractal sequences\u003c/a\u003e",
				"Clark Kimberling, \u003ca href=\"http://matwbn.icm.edu.pl/ksiazki/aa/aa73/aa7321.pdf\"\u003eNumeration systems and fractal sequences\u003c/a\u003e, Acta Arithmetica 73 (1995) 103-117.",
				"Boris Putievskiy, \u003ca href=\"http://arxiv.org/abs/1212.2732\"\u003eTransformations Integer Sequences And Pairing Functions\u003c/a\u003e arXiv:1212.2732 [math.CO], 2012.",
				"F. Smarandache, \u003ca href=\"http://www.gallup.unm.edu/~smarandache/Sequences-book.pdf\"\u003eSequences of Numbers Involved in Unsolved Problems\u003c/a\u003e.",
				"Aaron Snook, \u003ca href=\"http://www.cs.cmu.edu/afs/cs/user/mjs/ftp/thesis-program/2012/theses/snook.pdf\"\u003eAugmented Integer Linear Recurrences\u003c/a\u003e, 2012. - _N. J. A. Sloane_, Dec 19 2012",
				"Michael Somos, \u003ca href=\"/A073189/a073189.txt\"\u003eSequences used for indexing triangular or square arrays\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/SmarandacheSequences.html\"\u003eSmarandache Sequences.\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/UnitFraction.html\"\u003eUnit Fraction.\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 1 + A002262(n).",
				"n-th term is n - m*(m+1)/2 + 1, where m = floor((sqrt(8*n+1) - 1) / 2).",
				"The above formula is for offset 0; for offset 1, use a(n) = n-m*(m+1)/2 where m = floor((-1+sqrt(8*n-7))/2). - _Clark Kimberling_, Jun 14 2011",
				"a(k * (k + 1) / 2 + i) = i for k \u003e= 0 and 0 \u003c i \u003c= k + 1. - _Reinhard Zumkeller_, Aug 14 2001",
				"a(n) = (2*n + round(sqrt(2*n)) - round(sqrt(2*n))^2)/2. - _Brian Tenneson_, Oct 11 2003",
				"a(n) = n - binomial(floor((1+sqrt(8*n))/2), 2). - _Paul Barry_, May 25 2004",
				"T(n,k) = A001511(A118413(n,k)); T(n,k) = A003602(A118416(n,k)). - _Reinhard Zumkeller_, Apr 27 2006",
				"a(A000217(n)) = A000217(n) - A000217(n-1), a(A000217(n-1) + 1) = 1, a(A000217(n) - 1) = A000217(n) - A000217(n-1) - 1. - _Alexander R. Povolotsky_, May 28 2008",
				"a(A169581(n)) = A038566(n). - _Reinhard Zumkeller_, Dec 02 2009",
				"T(n,k) = Sum_{i=1..k} i*binomial(k,i)*binomial(n-k,n-i) (regarded as triangle, see the example). - _Mircea Merca_, Apr 11 2012",
				"T(n,k) = Sum_{i=max(0,n+1-2*k)..n-k+1} (i+k)*binomial(i+k-1,i)*binomial(k,n-i-k+1)*(-1)^(n-i-k+1). - _Vladimir Kruchinin_, Oct 18 2013",
				"G.f.: x*y / ((1 - x) * (1 - x*y)^2) = Sum_{n,k\u003e0} T(n,k) * x^n * y^k. - _Michael Somos_, Sep 17 2014"
			],
			"example": [
				"First six rows:",
				"  1",
				"  1   2",
				"  1   2   3",
				"  1   2   3   4",
				"  1   2   3   4   5",
				"  1   2   3   4   5   6"
			],
			"maple": [
				"at:=0; for n from 1 to 150 do for i from 1 to n do at:=at+1; lprint(at,i); od: od: # _N. J. A. Sloane_, Nov 01 2006",
				"seq(seq(i,i=1..k),k=1..13); # _Peter Luschny_, Jul 06 2009"
			],
			"mathematica": [
				"FoldList[{#1, #2} \u0026, 1, Range[2, 13]] // Flatten (* _Robert G. Wilson v_, May 10 2011 *)",
				"Flatten[Table[Range[n],{n,20}]] (* _Harvey P. Dale_, Jun 20 2013 *)"
			],
			"program": [
				"(PARI) t1(n)=n-binomial(floor(1/2+sqrt(2*n)),2) /* this sequence */",
				"(Haskell)",
				"a002260 n k = k",
				"a002260_row n = [1..n]",
				"a002260_tabl = iterate (\\row -\u003e map (+ 1) (0 : row)) [1]",
				"-- _Reinhard Zumkeller_, Aug 04 2014, Jul 03 2012",
				"(Maxima) T(n,k):=sum((i+k)*binomial(i+k-1,i)*binomial(k,n-i-k+1)*(-1)^(n-i-k+1),i,max(0,n+1-2*k),n-k+1); /* _Vladimir Kruchinin_, Oct 18 2013 */",
				"(PARI) A002260(n)=n-binomial((sqrtint(8*n)+1)\\2,2) \\\\ _M. F. Hasler_, Mar 10 2014"
			],
			"xref": [
				"Cf. A000217, A001710, A002262, A003056, A004736 (ordinal transform), A025581, A056534, A094727, A127779.",
				"Triangle sums (see the comments): A000217 (Row1, Kn11); A004526 (Row2); A000096 (Kn12); A055998 (Kn13); A055999 (Kn14); A056000 (Kn15); A056115 (Kn16); A056119 (Kn17); A056121 (Kn18); A056126 (Kn19); A051942 (Kn110); A101859 (Kn111); A132754 (Kn112); A132755 (Kn113); A132756 (Kn114); A132757 (Kn115); A132758 (Kn116); A002620 (Kn21); A000290 (Kn3); A001840 (Ca2); A000326 (Ca3); A001972 (Gi2); A000384 (Gi3).",
				"Cf. A108872."
			],
			"keyword": "nonn,easy,nice,tabl,look",
			"offset": "1,3",
			"author": "Angele Hamel (amh(AT)maths.soton.ac.uk)",
			"ext": [
				"More terms from _Reinhard Zumkeller_, Apr 27 2006",
				"Incorrect program removed by _Franklin T. Adams-Watters_, Mar 19 2010",
				"New name from _Omar E. Pol_, Jul 15 2012"
			],
			"references": 441,
			"revision": 195,
			"time": "2021-06-04T22:49:30-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}