{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A160638",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 160638,
			"data": "0,128,64,192,32,160,96,224,16,144,80,208,48,176,112,240,8,136,72,200,40,168,104,232,24,152,88,216,56,184,120,248,4,132,68,196,36,164,100,228,20,148,84,212,52,180,116,244,12,140,76,204,44,172,108,236,28,156",
			"name": "Bit-reversed 8-bit binary numbers.",
			"comment": [
				"This sequence is found in computer programs that need to reverse the bits in a byte, typically during data compression or other bit-level encoding. a(n) is its own inverse: a(a(n)) = n.",
				"A permutation of the integers 0-255. - _Jon Perry_, Oct 06 2012",
				"a(n) is even for 0 \u003c= n\u003c 128 and odd for n \u003c= 128 \u003c 256. - _Jon Perry_, Oct 06 2012",
				"a(m) + a(n) = a(m+n) when the binary representations of m and n have no bits in common. - _Jon Perry_, Oct 06 2012"
			],
			"reference": [
				"Henry S. Warren, Hacker's Delight, Addison-Wesley, 2002, pages 101-106."
			],
			"link": [
				"Russ Cox, \u003ca href=\"/A160638/b160638.txt\"\u003eTable of n, a(n) for n = 0 .. 255\u003c/a\u003e (full sequence)",
				"Sean Anderson, \u003ca href=\"http://graphics.stanford.edu/~seander/bithacks.html#BitReverseObvious\"\u003eBit Twiddling Hacks\u003c/a\u003e",
				"M. Beeler, R. W. Gosper, and R. Schroeppel, \u003ca href=\"http://www.inwap.com/pdp10/hbaker/hakmem/hacks.html#item167\"\u003eHAKMEM (MIT AI Memo 239, Feb. 29, 1972)\u003c/a\u003e, Item 167."
			],
			"formula": [
				"a(n) = floor(A030101(n+256)/2). - _Reinhard Zumkeller_, Jan 12 2013"
			],
			"example": [
				"n = 1 = 00000001 binary, so a(1) = 10000000 binary = 128.",
				"n = 29 = 00011101 binary, so a(29) = 10111000 binary = 184."
			],
			"mathematica": [
				"a[n_] := FromDigits[PadLeft[IntegerDigits[n, 2], 8] // Reverse, 2]; Table[a[n], {n, 0, 255}] (* _Jean-François Alcover_, Dec 26 2015 *)"
			],
			"program": [
				"(C) int a = 0; for(int i=0; i\u003c8; i++) if(n \u0026 (1\u003c\u003ci)) a |= 1\u003c\u003c(7 - i);",
				"(PARI) A160638(n)=binary(n+256)*vector(9,n,2^n)~\\4  \\\\ - _M. F. Hasler_, Oct 07 2012",
				"(PARI) A160638(n)=sum(i=0,7,bittest(n,7-i)\u003c\u003ci)  \\\\ - _M. F. Hasler_, Oct 07 2012",
				"(Haskell)",
				"import Data.Bits (testBit, setBit)",
				"import Data.Word (Word8)",
				"a160638 :: Word8 -\u003e Word8",
				"a160638 n = rev 0 0 where",
				"   rev 8 y = y",
				"   rev i y = rev (i + 1) (if testBit n i then setBit y (7 - i) else y)",
				"-- _Reinhard Zumkeller_, Jan 12 2013"
			],
			"xref": [
				"Cf. A217589."
			],
			"keyword": "base,easy,fini,full,nonn,nice",
			"offset": "0,2",
			"author": "_Russ Cox_, May 21 2009",
			"references": 3,
			"revision": 31,
			"time": "2015-12-26T04:37:15-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}