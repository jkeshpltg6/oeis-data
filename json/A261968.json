{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A261968",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 261968,
			"data": "1,-2,4,-8,14,-22,36,-56,84,-126,184,-264,376,-528,732,-1008,1374,-1856,2492,-3320,4394,-5784,7568,-9848,12756,-16442,21096,-26960,34312,-43500,54956,-69184,86804,-108576,135392,-168336,208722,-258096,318320,-391632",
			"name": "Expansion of phi(q^5) / phi(q) in powers of q where phi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A261968/b261968.txt\"\u003eTable of n, a(n) for n = 0..2500\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of eta(q)^2 * eta(q^4)^2 * eta(q^10)^5 / (eta(q^2)^5 * eta(q^5)^2 * eta(q^20)^2) in powers of q.",
				"Euler transform of period 20 sequence [ -2, 3, -2, 1, 0, 3, -2, 1, -2, 0, -2, 1, -2, 3, 0, 1, -2, 3, -2, 0, ...].",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2)) where f(u, v) = (1 - 2*u^2 + 5*u^4) * (1 - 2*v^2 + 5*v^4) - 4*(u^2 + 2*u*v - v^2)^2.",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^3)) where f(u, v) = (v^2 + 3*u*v - u^2) * (u^2 + v^2) - u*v * (1 + 5*u^2*v^2).",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (20 t)) = 5^(-1/2) * g(t) where q = exp(2 Pi i t) and g() is the g.f. for A144377.",
				"G.f.: Product_{k\u003e0} P(10, x^k)^3 * P(5, x^k) / P(20, x^k)^2 where P(n, x) is the n-th cyclotomic polynomial.",
				"a(n) = (-1)^n * A138526(n). Convolution inverse is A144377."
			],
			"example": [
				"G.f. = 1 - 2*q + 4*q^2 - 8*q^3 + 14*q^4 - 22*q^5 + 36*q^6 - 56*q^7 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 3, 0, q^5] / EllipticTheta[ 3, 0, q], {q, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A)^2 * eta(x^4 + A)^2 * eta(x^10 + A)^5 / (eta(x^2 + A)^5 * eta(x^5 + A)^2 * eta(x^20 + A)^2), n))};"
			],
			"xref": [
				"Cf. A138526, A144377."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Sep 06 2015",
			"references": 3,
			"revision": 11,
			"time": "2021-03-12T22:24:48-05:00",
			"created": "2015-09-06T20:06:54-04:00"
		}
	]
}