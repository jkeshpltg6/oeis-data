{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A274261",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 274261,
			"data": "1,2,4,6,11,14,19,37,38,53,50,57,80,81,99,125,131,213,156,330,161,220,173,207,244,225,312,337,234,293,462,471,434,535,349,458,470,489,477,413,527,474,619,539,554,666,743,690,1295,740,627,617,706,911,755,867",
			"name": "Exhaustion numbers for the greedy prime offset sequence A135311.",
			"comment": [
				"The greedy prime offset sequence, A135311, is the close-packed integer sequence, starting with 0, such that for no prime p does the sequence form a complete system of residues modulo p. Instead, at least one residue must be missing for p, this is the (conjectured to be unique) \"forbidden residue\" for p. Every prime, it appears, has a unique forbidden residue. If this is true then every prime has an \"exhaustion number\" which is the number of terms of the greedy sequence needed to exhaust all the other residues and determine which one is forbidden. The uniqueness of the forbidden residue for any individual prime can be verified by calculation.",
				"Note: I discovered the greedy sequence many years ago and did a writeup including discussion of forbidden residues and exhaustion numbers. See Links section."
			],
			"link": [
				"R. Michael Perry, \u003ca href=\"/A274261/a274261.pdf\"\u003eA number sequence relating to the closepacking of primes\u003c/a\u003e"
			],
			"example": [
				"The first few terms of the greedy offset sequence are 0, 2, 6, 8. For n=3, the n-th prime = 5. The residues of the greedy sequence modulo 5 are 0, 2, 1, 3 .... The first four residues exhaust all the possibilities but one, showing that 4 is the forbidden residue for 5 and the exhaustion number is also 4."
			],
			"mathematica": [
				"b[n_] := Module[{set = {}, m = 0, p, q, r}, p = Prime[n];",
				"  While[Length[set] \u003c p - 1, m++; q = Mod[g[m], p];",
				"   If[FreeQ[set, q], set = Append[set, q]]];",
				"  r = Complement[Range[0, p - 1], set][[1]];",
				"  {n, p, r, m}]",
				"(* b[n] returns a 4-element list: {n, Prime[n], forbidden_residue[n], exhaustion_number[n]}. g is the greedy sequence, see A135311 for Mathematica code, where a[n]=g[n].*)"
			],
			"xref": [
				"Cf. A135311, A274260."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_R. Michael Perry_, Jun 16 2016",
			"references": 2,
			"revision": 19,
			"time": "2019-08-25T13:43:14-04:00",
			"created": "2016-06-17T09:29:49-04:00"
		}
	]
}