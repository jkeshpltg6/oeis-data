{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A321466",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 321466,
			"data": "1,-4,12,-20,28,-24,28,-32,60,-68,72,-48,44,-56,96,-120,124,-72,76,-80,168,-160,144,-96,76,-124,168,-212,224,-120,168,-128,252,-240,216,-192,92,-152,240,-280,360,-168,224,-176,336,-408,288,-192,140,-228,372",
			"name": "Expansion of (phi(x^3)^3 / phi(x))^2 in powers of x where phi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Cubic AGM theta functions: a(q) (see A004016), b(q) (A005928), c(q) (A005882).",
				"The g.f. of A113973 is k = k(q) := phi(x^3)^3 / phi(x) given in equation (2.2) page 996 of Williams 2012, and the g.f. of k^2 which is given in equation (2.3) page 997 is this sequence.",
				"Number 54 of the 126 eta-quotients listed in Table 1 of Williams 2012."
			],
			"link": [
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e",
				"K. S. Williams, \u003ca href=\"http://dx.doi.org/10.1142/S1793042112500595\"\u003eFourier series of a class of eta quotients\u003c/a\u003e, Int. J. Number Theory 8 (2012), no. 4, 993-1004."
			],
			"formula": [
				"Expansion of (eta(q)^2 * eta(q^4)^2 * eta(q^6)^15 / (eta(q^2)^5 * eta(q^3)^6 * eta(q^12)^6))^2 in powers of q.",
				"Expansion of ((a(x) - 2*a(x^2) - 2*a(x^4))/3)^2 = ((b(x) + 2*b(x^4))^2 / (9*b(x^2)))^2 in powers of x where a(), b() are cubic AGM theta functions.",
				"Euler transform of period 12 sequence [-4, 6, 8, 2, -4, -12, -4, 2, 8, 6, -4, -4, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (12 t)) = (4/3) (t/i)^2 g(t) where q = exp(2 Pi i t) and g() is the g.f. for A321465.",
				"G.f.: (theta_3(0, x^3)^3 / theta_3(0, x))^2 where theta_3(0, x) is a Jacobi theta function.",
				"G.f.: (Product_{k\u003e0} f(x^k))^2 where f(x) := ((1 - x) * (1 + x^2))^2 * ((1 - x^3) * (1 + x^3)^3)^3 / ((1 - x^2) * (1 + x^6)^2)^3.",
				"a(n) = -4*(s(n) - 6*s(n/2) + s(n/3) + 4*s(n/4) + 2*s(n/6) + 4*s(n/12)) if n\u003e0 where s(x) = sum of divisors of x for integer x else 0.",
				"a(n) = (-1)^n * A227226(n). Convolution square of A113973."
			],
			"example": [
				"G.f. = 1 - 4*x + 12*x^2 - 20*x^3 + 28*x^4 - 24*x^5 + 28*x^6 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 3, 0, x^3]^6 / EllipticTheta[ 3, 0, x]^2, {x, 0, n}];",
				"a[ n_] := With[{s = If[ FractionalPart @ # \u003e 0, 0, DivisorSigma[1, #]] \u0026}, If[ n \u003c 1, Boole[n == 0], -4 (s[n] - 6 s[n/2] + s[n/3] + 4 s[n/4] + 2 s[n/6] + 4 s[n/12])]];"
			],
			"program": [
				"(PARI) {a(n) = my(s = x -\u003e if(frac(x), 0, sigma(x))); if( n\u003c1, n==0, -4*(s(n) - 6*s(n/2) + s(n/3) + 4*s(n/4) + 2*s(n/6) + 4*s(n/12)))};",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( (eta(x + A)^2 * eta(x^4 + A)^2 * eta(x^6 + A)^15 / (eta(x^2 + A)^5 * eta(x^3 + A)^6 * eta(x^12 + A)^6))^2, n))};",
				"(MAGMA) A := Basis( ModularForms( Gamma0(12), 2), 51); A[1] - 4*A[2] + 12*A[3] - 20*A[4] + 28*A[5];"
			],
			"xref": [
				"Cf. A113973,A227226,A321465."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Nov 11 2018",
			"references": 1,
			"revision": 5,
			"time": "2021-03-12T22:24:48-05:00",
			"created": "2018-11-11T19:23:53-05:00"
		}
	]
}