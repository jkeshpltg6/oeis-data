{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A035457",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 35457,
			"data": "1,0,1,0,1,0,2,0,2,0,3,0,4,0,5,0,6,0,8,0,10,0,12,0,15,0,18,0,22,0,27,0,32,0,38,0,46,0,54,0,64,0,76,0,89,0,104,0,122,0,142,0,165,0,192,0,222,0,256,0,296,0,340,0,390,0,448,0,512,0,585,0,668,0,760,0,864,0,982,0",
			"name": "Number of partitions of n into parts of the form 4*k + 2.",
			"comment": [
				"Also number of partitions of n into distinct even parts. Example: a(10)=3 because we have [10],[8,2] and [6,4]. - _Emeric Deutsch_, Feb 22 2006",
				"Also number of partitions of n into odd parts, each part occurring an even number of times. Example: a(10)=3 because we have [5,5], [3,3,1,1,1,1] and [1,1,1,1,1,1,1,1,1,1]. - _Emeric Deutsch_, Apr 08 2006"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A035457/b035457.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e"
			],
			"formula": [
				"G.f.: 1/Product_{n\u003e=0} (1 - x^(4*n+2)).",
				"G.f.: 1/Product_{j\u003e=0} (1 - x^(8*j+2))(1 - x^(8*j+6))).",
				"G.f.: Product_{j\u003e=1} (1 + x^(2*j)). - _Emeric Deutsch_, Feb 22 2006",
				"a(2*n-1) = 0, a(2*n) = A000009(n). a(n) = A116675(n,0). - _Emeric Deutsch_, Feb 22 2006",
				"G.f.: Sum_{n\u003e=1} (x^(n*(n+1)) / Product_{k=1..n} (1 - x^(2*k))). - _Joerg Arndt_, Mar 10 2011",
				"If n is even, a(n) ~ exp(Pi*sqrt(n/6)) / (2^(5/4) * 3^(1/4) * n^(3/4)). - _Vaclav Kotesovec_, Feb 26 2015",
				"a(4*n) = A035294(n) and a(4*n+2) = A078408(n). - _George Beck_, Aug 19 2017"
			],
			"example": [
				"a(10)=3 because we have [10], [6,2,2] and [2,2,2,2,2]."
			],
			"maple": [
				"g:=product(1+x^(2*j),j=1..45): gser:=series(g,x=0,85): seq(coeff(gser,x,n),n=0..79); # _Emeric Deutsch_, Feb 22 2006; a(0) added by _Georg Fischer_, Dec 10 2020"
			],
			"mathematica": [
				"nn=80;CoefficientList[Series[Product[1+ x^(2i),{i,1,nn}],{x,0,nn}],x] (* _Geoffrey Critzer_, Jun 20 2014 *)",
				"nmax = 50; kmax = nmax/4; s = Range[0, kmax]*4 + 2;",
				"Table[Count[IntegerPartitions@n, x_ /; SubsetQ[s, x]], {n, 0, nmax}] (* _Robert Price_, Aug 03 2020 *)"
			],
			"program": [
				"(PARI)",
				"N=166;  S=2+sqrtint(N);  x='x+O('x^N);",
				"gf=sum(n=0, S, x^(n^2+n)/prod(k=1,n, 1-x^(2*k)) );",
				"Vec(gf)",
				"\\\\ _Joerg Arndt_, Feb 18 2014"
			],
			"xref": [
				"Cf. A000726, A116675."
			],
			"keyword": "nonn",
			"offset": "0,7",
			"author": "_Olivier Gérard_",
			"ext": [
				"More terms from _Emeric Deutsch_, Feb 22 2006",
				"Description simplified by _Joerg Arndt_, Jun 24 2009",
				"a(0)=1 added by _Joerg Arndt_, Mar 11 2011"
			],
			"references": 13,
			"revision": 54,
			"time": "2020-12-10T10:33:02-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}