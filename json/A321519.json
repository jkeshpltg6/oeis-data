{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A321519",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 321519,
			"data": "0,0,1,0,1,0,2,1,1,0,1,1,6,0,1,0,6,2,1,0,6,1,6,0,1,0,6,1,1,1,6,2,6,1,1,0,6,2,1,0,2,1,11,1,1,1,25,1,1,1,1,1,6,0,6,0,16,1,1,1,1,1,6,1,1,0,6,3,1,2,1,6,25,0,6,1,6,1,1,1,6,2,25,0,1,1",
			"name": "Let d(n,i), i = 1..k be the k divisors of n^2 + 1 (the number 1 is not counted). a(n) is the number of ordered pairs d(n,i) \u003c d(n,j) such that gcd(d(n,i), d(n,j)) = 1.",
			"comment": [
				"Terms only depends on prime signature of n^2+1. - _David A. Corneth_, Nov 14 2018",
				"We observe an interesting statistic for n \u003c= 10^5: the four values of a(n) = 0, 1, 6, 25 represent more than 82% (see the table below).",
				"a(A005574(n)) = 0, a(A085722(n)) = 1, a(A272078(n)) = 6, a(A316351(n)) = 25.",
				"In the general case, a(k) = m if k^2+1 = p*q^m, m = 1, 2, 3, ... with p, q primes.",
				"+--------------+-----------------------+------------+",
				"|              | number of occurrences |            |",
				"|     a(n)     |     for n \u003c= 10^5     | percentage |",
				"+--------------+-----------------------+------------+",
				"|       0      |          6656         |    6.656%  |",
				"|       1      |         23255         |   23.255%  |",
				"|       6      |         31947         |   31.947%  |",
				"|      25      |         20461         |   20.461%  |",
				"| other values |         17681         |   17.681%  |",
				"+--------------+-----------------------+------------+"
			],
			"formula": [
				"a(n) = A089233(n^2+1). - _Michel Marcus_, Nov 13 2018"
			],
			"example": [
				"a(13) = 6 because the divisors {d(i)} of 13^2 + 1 = 170 (without the number 1)  are  {2, 5, 10, 17, 34, 85, 170}, and gcd(d(i), d(j)) = 1 for the 6 following pairs of elements of {d(i)}: (2, 5), (2, 17), (2, 85), (5, 17), (5, 34) and (10, 17)."
			],
			"maple": [
				"with(numtheory):nn:=10^3:",
				"for n from 1 to nn do:",
				"  it:=0:d:=divisors(n^2+1):n0:=nops(d):",
				"   for k from 2 to n0-1 do:",
				"    for l from k+1 to n0 do:",
				"     if gcd(d[k],d[l])= 1",
				"      then",
				"      it:=it+1",
				"      else",
				"     fi:",
				"   od:",
				"  od:",
				"  printf(`%d, `,it):",
				"od:"
			],
			"mathematica": [
				"f[n_] := (DivisorSigma[0, n^2] - 1)/2 - DivisorSigma[0, n] + 1; Map[f, Range[0,100]^2+1] (* _Amiram Eldar_, Nov 14 2018 after Robert G. Wilson v at A089233 *)"
			],
			"program": [
				"(PARI) a(n) = {my(d=divisors(n^2+1)); sum(k=2, #d, sum(j=2, k-1, gcd(d[k], d[j]) == 1));} \\\\ _Michel Marcus_, Nov 12 2018"
			],
			"xref": [
				"Cf. A005574, A002522, A085722, A089233, A193432, A272078, A316351."
			],
			"keyword": "nonn",
			"offset": "1,7",
			"author": "_Michel Lagneau_, Nov 12 2018",
			"references": 0,
			"revision": 22,
			"time": "2018-12-16T01:13:43-05:00",
			"created": "2018-12-16T01:13:43-05:00"
		}
	]
}