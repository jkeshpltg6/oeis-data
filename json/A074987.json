{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A074987",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 74987,
			"data": "2,1,4,3,8,3,9,5,7,5,22,5,21,7,16,15,32,7,27,15,13,11,46,15,33,13,19,13,58,15,62,17,25,17,39,13,57,19,35,17,55,13,49,25,35,23,94,17,43,25,64,35,106,19,41,35,37,29,118,17,77,31,37,51,104,25,134,51,92,35,142",
			"name": "a(n) is the least m not equal to n such that phi(m) = phi(n).",
			"comment": [
				"In 1922, Carmichael asked if for any given natural number n there exists a natural number m different from n such that phi(m) = phi(n). A. Schlafly and S. Wagon showed in 1994 that if there is an n such that phi(m) != phi(n) for all m distinct from n, then n must be greater than 10^(10^7). [Improved to 10^(10^10) by Kevin Ford. - _Pontus von Brömssen_, May 15 2020]",
				"I conjecture that a(n) \u003c= 2n. I have checked this for all n \u003c= 10^4. (It is not possible to do better than the 2n upper bound since a(11) = 2*11.)",
				"For odd n the conjecture is true because phi(n)=phi(2n). - _T. D. Noe_, Oct 18 2006",
				"From _Robert Israel_, Aug 12 2016: (Start)",
				"If a(n) \u003e n then a(a(n)) = n.",
				"If n is in A138537 then a(n) = 2*n. (End)",
				"From _David A. Corneth_, May 12 2018: (Start)",
				"A210719 has values n such that a(n) \u003e n, so a(A210719(n)) = n.",
				"Its complement, A296214, has values n such that a(n) \u003c n. (End)"
			],
			"reference": [
				"J. Tattersall, \"Elementary Number Theory in Nine Chapters\", Cambridge University Press, 2001, pp. 162-163."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A074987/b074987.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"K. Ford, \u003ca href=\"http://arxiv.org/abs/1104.3264\"\u003eThe distribution of totients\u003c/a\u003e, arXiv:1104.3264 [math.NT], 2011.",
				"A. Schlafly and S. Wagon, \u003ca href=\"https://doi.org/10.1090/S0025-5718-1994-1226815-3\"\u003eCarmichael's conjecture on the Euler function is valid below 10^{10,000,000}\u003c/a\u003e, Mathematics of Computation, 63 No. 207(1994), 415-419.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Carmichael\u0026#39;s_totient_function_conjecture\"\u003eCarmichael's_totient_function_conjecture\u003c/a\u003e"
			],
			"example": [
				"phi(5) = 4 and 8 is the least natural number k different from 5 such phi(k) = 4. Hence phi(5) = 8."
			],
			"maple": [
				"N:= 1000: # to get a(n) for n \u003c= N",
				"todo:= N;",
				"for n from 1 while todo \u003e 0 do",
				"  v:= numtheory:-phi(n);",
				"  if assigned(R[v]) then",
				"    if n \u003c= N then",
				"      A[n]:= R[v]; todo:= todo-1;",
				"    fi;",
				"    if R[v] \u003c= N and not assigned(A[R[v]])  then",
				"      A[R[v]]:= n; todo:= todo-1;",
				"    fi;",
				"  else",
				"    R[v]:= n",
				"  fi",
				"od:",
				"seq(A[n],n=1..N); # _Robert Israel_, Aug 12 2016"
			],
			"mathematica": [
				"l = {}; Do[ e = EulerPhi[n]; i = 1; While[e != EulerPhi[i] || n == i, i++ ]; l = Append[l, i], {n, 1, 100}]; l",
				"Module[{nn=300,lst},lst=Table[{n,EulerPhi[n]},{n,nn}];Take[Table[ SelectFirst[ lst,#[[2]]==lst[[k,2]]\u0026\u0026#[[1]]!=lst[[k,1]]\u0026],{k,nn}],100]][[All,1]] (* Requires Mathematica version 10 or later *) (* _Harvey P. Dale_, Oct 23 2020 *)"
			],
			"program": [
				"(Python)",
				"from sympy import totient",
				"def A074987(n):",
				"  m=1",
				"  while totient(m)!=totient(n) or m==n:",
				"    m+=1",
				"  return m # _Pontus von Brömssen_, May 15 2020",
				"(PARI) a(n) = my(t=eulerphi(n), m=1); while ((eulerphi(m) != t) || (m==n), m++); m; \\\\ _Michel Marcus_, May 15 2020"
			],
			"xref": [
				"Cf. A138537, A210719, A296214."
			],
			"keyword": "easy,nice,nonn",
			"offset": "1,1",
			"author": "_Joseph L. Pe_, Oct 02 2002",
			"references": 2,
			"revision": 49,
			"time": "2021-12-21T23:30:25-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}