{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A253827",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 253827,
			"data": "1,2,4,4,10,4,16,6,10,13,14,16,40,8,26,19,34,21,36,28,18,18,34,27,31,68,16,71,30,23,37,37,67,44,54,55,54,26,65,50,70,68,79,43,60,70,52,51,132,38,60,100,59,111,114,84,77,68,78,105,49,67,124,145,35",
			"name": "a(n) is the number of primes of the form x^2 + x + prime(n) for 0 \u003c= x \u003c=prime(n).",
			"comment": [
				"Equivalently, number of distinct primes of the form x^2 - x + prime(n) for 0 \u003c= x \u003c= prime(n). (The point is that x^2 + x = (x+1)^2 - (x+1), so the two forms give the same numbers. x^2 - x + prime(n) is the same for x=0 and x=1, which is why the \"distinct\" in the comment. - _Robert Israel_, Oct 09 2016)",
				"1 \u003c= a(n) \u003c= prime(n)-1.  a(n) = prime(n)-1 iff n is in A014556.  Are there any n \u003e 1 such that a(n) = 1? - _Robert Israel_, Jan 16 2015"
			],
			"link": [
				"Michel Lagneau, \u003ca href=\"/A253827/b253827.txt\"\u003eTable of n, a(n) for n = 1..4000\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/LuckyNumberofEuler.html\"\u003eLucky Number of Euler\u003c/a\u003e"
			],
			"example": [
				"a(13) = 40 because prime(13) = 41 and x^2 + x + 41 generates 40 prime numbers for x = 0..41."
			],
			"maple": [
				"f:= proc(n)",
				"local p,x;",
				"p:= ithprime(n);",
				"nops(select(isprime, [seq(x^2+x+p,x=0..p)]))",
				"end proc:",
				"seq(f(n), n=1..100); # _Robert Israel_, Jan 16 2015"
			],
			"mathematica": [
				"lst={};Do[p=Prime[n];k=0;Do[If[PrimeQ[x^2+x+p],k=k+1],{x,0,p}];AppendTo[lst,k],{n,1,100}];lst",
				"Table[With[{p=Prime[n]},Count[Table[x^2+x+p,{x,0,p}],_?PrimeQ]],{n,70}] (* _Harvey P. Dale_, May 27 2018 *)"
			],
			"program": [
				"(PARI) a(n) = my(p=prime(n)); sum(k=0, p, isprime(subst(x^2+x+p, x, k))); \\\\ _Michel Marcus_, Jan 16 2015"
			],
			"xref": [
				"Cf. A000040, A005846, A014556, A228123."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Michel Lagneau_, Jan 16 2015",
			"references": 2,
			"revision": 17,
			"time": "2018-05-27T13:26:18-04:00",
			"created": "2015-01-16T19:20:37-05:00"
		}
	]
}