{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A093960",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 93960,
			"data": "1,2,4,11,29,76,199,521,1364,3571,9349,24476,64079,167761,439204,1149851,3010349,7881196,20633239,54018521,141422324,370248451,969323029,2537720636,6643838879,17393796001,45537549124,119218851371,312119004989,817138163596",
			"name": "a(1) = 1, a(2) = 2, a(n+1) = n*a(1) + (n-1)*a(2) + ... + (n-r)*a(r+1) + ... + a(n).",
			"comment": [
				"a(1) = a(2) = 1 gives A088305, i.e., Fibonacci numbers with even indices. This can be called 'fake Fibonacci sequence'. 4 = 3+1, 11 = 8+3, 29 = 21+8, 76 = 55+21, etc. a(n) = F(2n-2) + F(2n-4).",
				"Except for the initial terms, this is the same as the bisection of the Lucas sequence (A002878). - _Franklin T. Adams-Watters_, Jul 17 2006"
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A093960/b093960.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-1)."
			],
			"formula": [
				"a(n) = F(2*n-2) + F(2*n-4), where F(k) is k-th Fibonacci number, n \u003e 2.",
				"a(n) = 3*a(n-1) - a(n-2) for n\u003e4. - _Colin Barker_, Mar 26 2015",
				"G.f.: x*(1-x)^2*(1+x) / (1-3*x+x^2). - _Colin Barker_, Mar 26 2015",
				"a(n) = 2^(2-n)*[n\u003c3] + LucasL(2*n-3). - _G. C. Greubel_, Dec 30 2021"
			],
			"maple": [
				"a[1]:=1: a[2]:=2: for n from 2 to 33 do a[n+1]:=sum((n-r)*a[r+1],r=0..n-1) od: seq(a[n],n=1..33); # _Emeric Deutsch_, Aug 01 2005"
			],
			"mathematica": [
				"Print[1]; Print[2]; Do[Print[Fibonacci[2*n - 2] + Fibonacci[2*n - 4]], {n, 3, 20}] (* _Ryan Propper_, Jun 19 2005 *)",
				"LinearRecurrence[{3,-1},{1,2,4,11},30] (* _Harvey P. Dale_, Nov 17 2018 *)"
			],
			"program": [
				"(PARI) Vec(x*(x-1)^2*(x+1)/(x^2-3*x+1) + O(x^100)) \\\\ _Colin Barker_, Mar 26 2015",
				"(MAGMA) [1,2] cat [Lucas(2*n-3): n in [3..30]]; // _G. C. Greubel_, Dec 30 2021",
				"(Sage) [2^(2-n)*bool(n\u003c3) + lucas_number2(2*n-3, 1, -1) for n in (1..30)] # _G. C. Greubel_, Dec 30 2021"
			],
			"xref": [
				"Cf. A000045, A002878, A088305."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Amarnath Murthy_, May 22 2004",
			"ext": [
				"More terms from _Ryan Propper_, Jun 19 2005",
				"More terms from _Emeric Deutsch_, Aug 01 2005"
			],
			"references": 3,
			"revision": 23,
			"time": "2021-12-31T02:02:28-05:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}