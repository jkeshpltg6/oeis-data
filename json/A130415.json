{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A130415",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 130415,
			"data": "1,0,1,-5,0,6,0,-5,0,8,35,0,-112,0,80,0,7,0,-28,0,24,-21,0,126,0,-216,0,112,0,-15,0,108,0,-216,0,128,165,0,-1584,0,4752,0,-5632,0,2304,0,55,0,-616,0,2112,0,-2816,0,1280,-143,0,2002,0,-9152,0,18304,0,-16640,0,5632,0,-91,0,1456,0,-7488",
			"name": "Coefficient table for polynomials related to the eigenfunctions of a certain Schroedinger problem (Poeschl-Teller I).",
			"comment": [
				"The eigenenergies are e_n = ((n+1)^2-1)/2, n=1,2,... and the normalized eigenfunctions (modulo phases) are Psi_n(q) which are for odd n: sqrt(1/(Pi*e_n))*((1+n*y^2)*U(n,y)/y - (n+1)*U(n-1,y)) with y:=cos(q) and the Chebyshev polynomials U(n,y) (see A049310). For even n: sqrt(1/(Pi*e_n))*tan(q)*(n*U(n+1,y)+ (n+2)*U(n-1,y))/2, with y:=cos(q).",
				"The row polynomials P(n,y):=sum(a(n,m)*y^m,m=0..n-1), n\u003e=1, are related to the eigenfunctions as follows.",
				"For odd n: P(n,y)= sqrt(Pi*e_n)*Psi_n(q)/(gcdrow(n)*y^2) with y=cos(q) and gcdrow(n) the greatest common divisor of the row coefficients factored out. It seems that gcdrow(2*k-1)=2*A006519(2*k-1), k=1,2,3,... (twice the highest power of 2 dividing n).",
				"For even n: P(n,y)= sqrt(Pi*e_n)*Psi_n(q)/(8*gcdrow(n)*sin(q)*cos(q)) with y=cos(q) and the gcdrow(2*k) sequence is [1,1,2,2,1,1,4,4,1,1,2,2,1,1,8,8,1,1,2,2,1,1,...],k=1,2,... This looks like the doubled sequence A006519 (the first 100 terms have been checked).",
				"The one-dimensional, stationary Schroedinger equation with potential V(q) = (2/cos(q)^2-1)/ 2 where |q| \u003c Pi/2 (so-called Poeschl-Teller I potential) can be solved exactly, e.g., via supersymmetry adapted to quantum mechanics (and called D=1+0, N=2 supersymmetry). q:=Pi*x/L, L:=sqrt(m*w/hbar) with the coordinate |x| \u003c= L/2 and m and w are the mass and the frequency of the underlying harmonic Bose oscillator. The reduced Planck constant is hbar = h/(2*Pi). The eigenenergies E_n and the potential are divided by hbar*w to become dimensionless e_n and V(q)."
			],
			"link": [
				"W. Lang, \u003ca href=\"/A130415/a130415.txt\"\u003eFirst 10 lines and more.\u003c/a\u003e"
			],
			"formula": [
				"a(n,m) = [y^m] P(n,y), n=1,2,..., m=0,1,2,..., with the polynomials P(n,y) defined above for even and odd n in terms of Chebyshev polynomials and the eigenenergies e_n."
			],
			"example": [
				"Ground state (n=1): e_1=3/2, Psi_1(q)= sqrt(2/(3*Pi))*((1+cos(q)^2)*2 - 2)= sqrt(2/(3*Pi))*2*cos(q)^2; gcdrow(1)=2, P(1,y)=sqrt(Pi*3/2)*(1/(2*y^2)*Psi_1(q=arccos(y)) =1.",
				"First excited state (n=2): e_2=4, Psi_2(q)= sqrt(1/(4*Pi))*tan(q)*(2*U(3,cos(q))+4*U(1,cos(q)))/2 = (4/sqrt(Pi))*sin(q)*cos(q)^2; gcdrow(2)=1, P(2,y)= sqrt(Pi*4)*Psi_n(q=arccos(y))/(8*1*sqrt(1-y^2)*y) =y.",
				"Triangle begins:",
				"  [1];",
				"  [0,1];",
				"  [-5,0,6];",
				"  [0,-5,0,8];",
				"  [35,0,-112,0,80];",
				"  [0,7,0,-28,0,24];",
				"  ..."
			],
			"keyword": "sign,tabl,easy",
			"offset": "0,4",
			"author": "_Wolfdieter Lang_, Jul 13 2007",
			"references": 1,
			"revision": 15,
			"time": "2019-08-28T10:04:33-04:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}