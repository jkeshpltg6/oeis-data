{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A281119",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 281119,
			"data": "1,1,1,1,1,1,2,1,1,1,3,1,1,1,5,1,3,1,3,1,1,1,9,1,1,2,3,1,4,1,12,1,1,1,12,1,1,1,9,1,4,1,3,3,1,1,29,1,3,1,3,1,9,1,9,1,1,1,17,1,1,3,34,1,4,1,3,1,4,1,44,1,1,3,3,1,4,1,29,5,1,1",
			"name": "Number of complete tree-factorizations of n \u003e= 2.",
			"comment": [
				"A tree-factorization of n\u003e=2 is either (case 1) the number n or (case 2) a sequence of two or more tree-factorizations, one of each part of a weakly increasing factorization of n into factors greater than 1. A complete (or total) tree-factorization is a tree-factorization whose leaves are all prime numbers.",
				"a(n) depends only on the prime signature of n. - _Andrew Howroyd_, Nov 18 2018"
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A281119/b281119.txt\"\u003eTable of n, a(n) for n = 2..10000\u003c/a\u003e",
				"A. Knopfmacher, M. Mays, \u003ca href=\"http://www.mathematica-journal.com/issue/v10i1/contents/Factorizations/Factorizations_3.html\"\u003eOrdered and Unordered Factorizations of Integers\u003c/a\u003e, The Mathematica Journal 10(1), 2006."
			],
			"formula": [
				"a(p^n) = A196545(n) for prime p. - _Andrew Howroyd_, Nov 18 2018"
			],
			"example": [
				"The a(36)=12 complete tree-factorizations of 36 are:",
				"(2(2(33))), (2(3(23))), (2(233)),   (3(2(23))),",
				"(3(3(22))), (3(223)),   ((22)(33)), ((23)(23)),",
				"(22(33)),   (23(23)),   (33(22)),   (2233)."
			],
			"mathematica": [
				"postfacs[n_]:=If[n\u003c=1,{{}},Join@@Table[Map[Prepend[#,d]\u0026,Select[postfacs[n/d],Min@@#\u003e=d\u0026]],{d,Rest[Divisors[n]]}]];",
				"treefacs[n_]:=If[n\u003c=1,{{}},Prepend[Join@@Function[q,Tuples[treefacs/@q]]/@DeleteCases[postfacs[n],{n}],n]];",
				"Table[Length[Select[treefacs[n],FreeQ[#,_Integer?(!PrimeQ[#]\u0026)]\u0026]],{n,2,83}]"
			],
			"program": [
				"(PARI) seq(n)={my(v=vector(n), w=vector(n)); v[1]=1; for(k=2, n, w[k]=v[k]+isprime(k); forstep(j=n\\k*k, k, -k, my(i=j, e=0); while(i%k==0, i/=k; e++; v[j]+=w[k]^e*v[i]))); w[2..n]} \\\\ _Andrew Howroyd_, Nov 18 2018"
			],
			"xref": [
				"Cf. A000311, A001055, A063834, A196545, A262673, A273873, A281113, A281118."
			],
			"keyword": "nonn",
			"offset": "2,7",
			"author": "_Gus Wiseman_, Jan 15 2017",
			"references": 15,
			"revision": 12,
			"time": "2018-11-18T19:25:11-05:00",
			"created": "2017-01-16T14:15:21-05:00"
		}
	]
}