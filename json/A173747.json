{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A173747",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 173747,
			"data": "1,1,1,1,1,1,14,1,1,1,14,76,1,1,1,92,76,234,1,1,1,196,976,234,536,1,1,1,664,5776,4428,536,1030,1,1,1,1912,16576,54756,13376,1030,1764,1,1,1,5552,131776,130248,287296,31900,1764,2786,1,1,1",
			"name": "Square array T(n, k) = v(k, n)((1)), where v(n, q) = M*v(n-1, q), M = {{0, 1, 0}, {0, 0, 1}, {8*q^3, 6*q, 0}}, with v(0, q) = {1, 1, 1}, read by antidiagonals.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A173747/b173747.txt\"\u003eAntidiagonal rows n = 0..50, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = v(k, n)((1)), where v(n, q) = M*v(n-1, q), M = {{0, 1, 0}, {0, 0, 1}, {8*q^3, 6*q, 0}}, with v(0, q) = {1, 1, 1} (square array).",
				"T(n, k) = f(k, n+1), where f(n, q) = 6*q*f(n-2, q) + 8*q^3*f(n-3, q), and f(0,q) = f(1,q) = f(2,q) = 1 (square array). - _G. C. Greubel_, Jul 06 2021"
			],
			"example": [
				"Square array begins as:",
				"  1, 1, 1,   14,   14,    92, ...;",
				"  1, 1, 1,   76,   76,   976, ...;",
				"  1, 1, 1,  234,  234,  4428, ...;",
				"  1, 1, 1,  536,  536, 13376, ...;",
				"  1, 1, 1, 1030, 1030, 31900, ...;",
				"  1, 1, 1, 1764, 1764, 65232, ...;",
				"Antidiagonal triangle begins as:",
				"     1;",
				"     1,      1;",
				"     1,      1,      1;",
				"    14,      1,      1,      1;",
				"    14,     76,      1,      1,     1;",
				"    92,     76,    234,      1,     1,    1;",
				"   196,    976,    234,    536,     1,    1,    1;",
				"   664,   5776,   4428,    536,  1030,    1,    1, 1;",
				"  1912,  16576,  54756,  13376,  1030, 1764,    1, 1, 1;",
				"  5552, 131776, 130248, 287296, 31900, 1764, 2786, 1, 1, 1;"
			],
			"mathematica": [
				"(* First program *)",
				"M = {{0, 1, 0}, {0, 0, 1}, {8*q^3, 6*q, 0}};",
				"v[0, q_] = {1, 1, 1};",
				"v[n_, q_]:= v[n, q]= M.v[n-1, q];",
				"T = Table[v[n, q][[1]], {n,0,20}, {q,1,21}];",
				"Table[T[[n-k+1, k+1]], {n,0,10}, {k,0,n}]//Flatten (* modified by _G. C. Greubel_, Jul 06 2021 *)",
				"(* Second program *)",
				"f[n_, q_]:= f[n, q]= If[n\u003c3, 1, 6*q*f[n-2, q] + 8*q^3*f[n-3, q]];",
				"T[n_, k_]:= f[k, n+1];",
				"Table[T[k, n-k], {n,0,12}, {k,0,n}]//Flatten (* _G. C. Greubel_, Jul 06 2021 *)"
			],
			"program": [
				"(Sage)",
				"@CachedFunction",
				"def f(n,q): return 1 if (n\u003c3) else 6*q*f(n-2, q) + 8*q^3*f(n-3, q)",
				"def T(n,k): return f(k, n+1)",
				"flatten([[T(k, n-k) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, Jul 06 2021"
			],
			"xref": [
				"Cf. A173749, A173778, A173779."
			],
			"keyword": "nonn,tabl",
			"offset": "0,7",
			"author": "_Roger L. Bagula_, Feb 23 2010",
			"ext": [
				"Edited by _G. C. Greubel_, Jul 06 2021"
			],
			"references": 4,
			"revision": 15,
			"time": "2021-07-07T01:59:50-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}