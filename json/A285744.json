{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A285744",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 285744,
			"data": "2310,1155,770,1365,462,385,330,1785,910,231,210,455,420,165,154,1995,390,595,510,273,110,105,546,665,714,255,1190,195,570,77,630,2145,70,285,66,715,660,315,140,357,690,55,780,345,182,399,798,805,858,429,130",
			"name": "Lexicographically earliest sequence of distinct positive terms such that, for any n\u003e0, n*a(n) has at least 5 distinct prime factors.",
			"comment": [
				"If n has at least 5 distinct prime factors, then a(n) is the least unused number; as there are infinitely many numbers with at least 5 distinct prime factors, this sequence is a permutation of the natural numbers.",
				"The inverse of this sequence is the sequence itself.",
				"The first fixed points are: 40755, 42966, 54285, 54740, 55965, 56070, 66045, 66066, 70035, 70350, 73815, 73920 (note that the fixed points have at least 5 distinct prime factors).",
				"Conjecturally, a(n) ~ n.",
				"This sequence has similarities with A285487: here n*a(n) has at least 5 distinct prime factors, there a(n)*a(n+1) has at least 5 distinct prime factors."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A285744/b285744.txt\"\u003eTable of n, a(n) for n = 1..25000\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A285744/a285744.gp.txt\"\u003ePARI program for A285744\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A285744/a285744.png\"\u003eColored scatterplot of the first 25000 terms\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"example": [
				"The first terms, alongside the primes p dividing n*a(n), are:",
				"n       a(n)    p",
				"--      ----    --------------",
				"1       2310    2, 3, 5, 7, 11",
				"2       1155    2, 3, 5, 7, 11",
				"3       770     2, 3, 5, 7, 11",
				"4       1365    2, 3, 5, 7,     13",
				"5       462     2, 3, 5, 7, 11",
				"6       385     2, 3, 5, 7, 11",
				"7       330     2, 3, 5, 7, 11",
				"8       1785    2, 3, 5, 7,         17",
				"9       910     2, 3, 5, 7,     13",
				"10      231     2, 3, 5, 7, 11",
				"11      210     2, 3, 5, 7, 11",
				"12      455     2, 3, 5, 7,     13",
				"13      420     2, 3, 5, 7,     13",
				"14      165     2, 3, 5, 7, 11",
				"15      154     2, 3, 5, 7, 11",
				"16      1995    2, 3, 5, 7,             19",
				"17      390     2, 3, 5,        13, 17",
				"18      595     2, 3, 5, 7,         17",
				"19      510     2, 3, 5,            17, 19",
				"20      273     2, 3, 5, 7,     13"
			],
			"xref": [
				"Cf. A285487."
			],
			"keyword": "nonn,look",
			"offset": "1,1",
			"author": "_Rémy Sigrist_, Apr 25 2017",
			"references": 2,
			"revision": 11,
			"time": "2017-04-26T23:21:21-04:00",
			"created": "2017-04-26T23:21:21-04:00"
		}
	]
}