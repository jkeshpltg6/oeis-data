{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A294168",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 294168,
			"data": "0,1,0,2,0,1,2,0,1,0,4,0,1,0,0,3,2,0,6,0,0,3,0,0,2,3,2,0,0,2,2,0,0,2,0,5,3,1,4,0,10,0,0,2,0,0,1,4,1,2,5,4,8,5,0,12,0,0,1,3,3,3,0,0,1,3,0,0,1,2,3,0,0,1,2,0,2,3,6,8,9,0,9,2,7,0,16",
			"name": "Irregular triangle read by rows in which row n contains significant digits after the radix point for unit fractions 1/n expanded in factorial base.",
			"comment": [
				"See the Wikipedia link for the construction method of 1/n in factorial base. This version eliminates the 1/0! and 1/1! places, which are always 0.",
				"By convention, row n = 1 contains {0}.",
				"Length of row n = A002034(n) - 1.",
				"Length of row p = p - 1 for p prime."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A294168/b294168.txt\"\u003eTable of n, a(n) for n = 1..10384\u003c/a\u003e (rows 1 \u003c= n \u003c= 250).",
				"OEIS Wiki, \u003ca href=\"https://oeis.org/wiki/Factorial_numeral_system\"\u003eFactorial numeral system\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Factorial_number_system#Fractional_values\"\u003eFactorial number system (Fractional values)\u003c/a\u003e",
				"\u003ca href=\"/index/Fa#facbase\"\u003eIndex entries for sequences related to factorial base representation\u003c/a\u003e"
			],
			"example": [
				"Row n = 7 contains {0, 0, 3, 2, 0, 6} since these are the digits that appear after the radix point in the factorial base expansion of 1/7. The length of row 7 = A002034(7) - 1 = 6, the largest number in row 7 is A299020(7) = 6, and the sum of row 7 = A276350(7) = 11.",
				"1/n expanded in factorial base appears below; this sequence includes numbers to the right of the radix point.",
				"   n    1/n in factorial base     A276350(n)  A299020(n)",
				"  --    ----------------------    ----------  ----------",
				"   1    1.0                            1          1",
				"   2    0.1                            1          1",
				"   3    0.0 2                          2          2",
				"   4    0.0 1 2                        3          2",
				"   5    0.0 1 0 4                      5          4",
				"   6    0.0 1                          1          1",
				"   7    0.0 0 3 2 0 6                 11          6",
				"   8    0.0 0 3                        3          3",
				"   9    0.0 0 2 3 2                    7          3",
				"  10    0.0 0 2 2                      4          2",
				"  11    0.0 0 2 0 5 3 1 4 0 10        25         10",
				"  12    0.0 0 2                        2          2",
				"  13    0.0 0 1 4 1 2 5 4 8 5 0 12    42         12",
				"  14    0.0 0 1 3 3 3                 10          3",
				"  15    0.0 0 1 3                      4          3",
				"..."
			],
			"mathematica": [
				"Array[With[{s = NumberDecompose[1/#, 1/Range[#]!]}, Rest@ Drop[s, -LengthWhile[Reverse@ s, # == 0 \u0026]]] - Boole[# == 1] \u0026, 17] /. {} -\u003e {0} // Flatten"
			],
			"xref": [
				"Cf. A002034, A007623, A276350, A299020."
			],
			"keyword": "nonn,base,tabf",
			"offset": "1,4",
			"author": "_Michael De Vlieger_, Feb 10 2018",
			"references": 5,
			"revision": 23,
			"time": "2018-02-23T08:28:20-05:00",
			"created": "2018-02-22T17:01:45-05:00"
		}
	]
}