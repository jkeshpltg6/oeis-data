{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A102698",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 102698,
			"data": "8,80,368,1264,3448,7792,16176,30696,54216,90104,143576,220328,326680,471232,664648,916344,1241856,1655208,2172584,2812664,3598664,4553800,5702776,7075264,8705088,10628928,12880056,15496616,18523472,22003808",
			"name": "Number of equilateral triangles with coordinates (x,y,z) in the set {0, 1,...,n}.",
			"comment": [
				"Inspired by Problem 25 on the 2005 AMC-12A Mathematics Competition, which asked for a(2)."
			],
			"reference": [
				"Mohammad K. Azarian, A Trigonometric Characterization of  Equilateral Triangle, Problem 336, Mathematics and Computer Education, Vol. 31, No. 1, Winter 1997, p. 96.  Solution published in Vol. 32, No. 1, Winter 1998, pp. 84-85.",
				"Mohammad K. Azarian, Equating Distances and Altitude in an Equilateral Triangle, Problem 316, Mathematics and Computer Education, Vol. 28, No. 3, Fall 1994, p. 337.  Solution published in Vol. 29, No. 3, Fall 1995, pp. 324-325."
			],
			"link": [
				"Eugen J. Ionascu and Rodrigo A. Obando, \u003ca href=\"/A102698/b102698.txt\"\u003eTable of n, a(n) for n = 1..100\u003c/a\u003e",
				"Ray Chandler and Eugen J. Ionascu, \u003ca href=\"http://arXiv.org/abs/0710.0708\"\u003eA characterization of all equilateral triangles in Z^3\u003c/a\u003e, arXiv:0710.0708 [math.NT], 2007.",
				"Eugen J. Ionascu, \u003ca href=\"/A102698/a102698.txt\"\u003eMaple program\u003c/a\u003e",
				"Eugen J. Ionascu, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL10/Ionascu/ionascu2.html\"\u003eA parametrization of equilateral triangles having integer coordinates\u003c/a\u003e, J. Integer Seqs., Vol. 10 (2007), #07.6.7.",
				"Eugen J. Ionascu, \u003ca href=\"https://www.emis.de/journals/AMUC/_vol-77/_no_1/_ionascu/ionascu.html\"\u003eCounting all equilateral triangles in {0,1,...,n}^3\u003c/a\u003e, Acta Mathematica Universitatis Comenianae, Vol. LXXVII, 1 (2008) p. 129-140.",
				"Rodrigo A. Obando, \u003ca href=\"/A102698/a102698_b.txt\"\u003eMathematica program\u003c/a\u003e",
				"Burkard Polster, \u003ca href=\"https://youtu.be/sDfzCIWpS7Q?t=799\"\u003eWhat does this prove? Some of the most gorgeous visual \"shrink\" proofs ever invented\u003c/a\u003e, Mathologer video (2020)."
			],
			"formula": [
				"a(n) approximately equals n^4.989; also lim log(a(n))/log(n) exists. - _Eugen J. Ionascu_, Dec 09 2006"
			],
			"example": [
				"a(1) = 8 because in the unit cube, equilateral triangles are formed by cutting off any one of the 8 corners.",
				"a(2) = 80 because there are 8 unit cubes with 8 each, 8 larger triangles (analogous to the 8 in the unit cube, but twice as big) and also 8 triangles of side length sqrt(6)."
			],
			"maple": [
				"See Ionascu link for Maple program."
			],
			"mathematica": [
				"See Obando link for Mathematica program."
			],
			"xref": [
				"Cf. a(n)=8*A103501, A103158 tetrahedra in lattice cube."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Joshua Zucker_, Feb 04 2005",
			"ext": [
				"More terms from _Hugo Pfoertner_, Feb 10 2005",
				"Edited by _Ray Chandler_, Sep 15 2007, Jul 27 2010"
			],
			"references": 10,
			"revision": 30,
			"time": "2020-10-21T02:44:08-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}