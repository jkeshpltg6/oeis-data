{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A080950",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 80950,
			"data": "2,3,6,5,7,8,8,7,9,10,11,10,10,11,10,9,11,12,13,12,13,14,13,12,12,13,14,13,12,13,12,11,13,14,15,14,15,16,15,14,15,16,17,16,15,16,15,14,14,15,16,15,16,17,16,15,14,15,16,15,14,15,14,13,15,16,17,16,17,18,17,16,17",
			"name": "Number of numbers that differ from n in binary representation by exactly one edit-operation: deletion, insertion, or substitution.",
			"comment": [
				"a(n) = #{i: LD-2(n,i)=1}, where LD-2 is the Levenshtein distance on binary strings."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A080950/b080950.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Michael Gilleland, \u003ca href=\"http://www.merriampark.com/ld.htm\"\u003eLevenshtein Distance\u003c/a\u003e. [It has been suggested that this algorithm gives incorrect results sometimes. - _N. J. A. Sloane_]"
			],
			"formula": [
				"From _Robert Israel_, Aug 08 2019: (Start)",
				"a(4*k) = a(2*k)+2 for k \u003e= 2.",
				"a(4*k+1) = a(2*k)+3 for k \u003e= 2.",
				"a(4*k+2) = a(2*k+1)+3.",
				"a(4*k+3) = a(2*k+1)+2.",
				"G.f. g(x) satisfies g(x) = (1+x)*g(x^2) + (x+2*x^2+x^4+x^8)/(1-x+x^2-x^3).",
				"(End)"
			],
			"example": [
				"n=6: binary representation of numbers at Levenshtein distance 1 from 6='110': {10, 11, 100, 111, 1010, 1100, 1101, 1110}, so a(6)=8.",
				"n=42: binary representation of numbers at Levenshtein distance 1 from 42='101010': {10010, 10100, 10101, 10110, 11010, 100010, 101000, 101011, 101110, 111010, 1001010, 1010010, 1010100, 1010101, 1010110, 1011010, 1101010}, therefore a(42)=17."
			],
			"maple": [
				"f:= proc(n) local R,i,m,a,b;",
				"  m:= ilog2(n);",
				"  R:= {n+2^(m+1),n+2^m};",
				"  for i from 0 to m-1 do",
				"    b:= n mod 2^i;",
				"    a:= (n-b)/2^i;",
				"    R:= R union {floor(a/2)*2^i+b, a*2^(i+1)+b,",
				"                 (2*a+1)*2^i+b,n + (1-2*(a mod 2))*2^i}",
				"  od;",
				"nops(R)",
				"end proc:",
				"f(0):= 2: f(1):= 3: f(2):= 6:",
				"map(f, [$0..100]); # _Robert Israel_, Aug 08 2019"
			],
			"mathematica": [
				"a[n_] := a[n] = If[n \u003c 6, {2, 3, 6, 5, 7, 8}[[n+1]], Switch[Mod[n, 4], 0, a[n/2]+2, 1, a[(n-1)/2]+3, 2, a[(n-2)/2+1]+3, 3, a[(n-3)/2+1]+2]];",
				"a /@ Range[0, 100]",
				"(* or: *)",
				"m = 100; g[_] = 1;",
				"Do[g[x_] = (1 + x) g[x^2] + (x + 2 x^2 + x^4 + x^8)/(1 - x + x^2 - x^3) + O[x]^m // Normal, {m}];",
				"1 + CoefficientList[g[x], x] (* _Jean-François Alcover_, Oct 24 2019, after _Robert Israel_ *)"
			],
			"xref": [
				"Cf. A080910."
			],
			"keyword": "nonn,base,look",
			"offset": "0,1",
			"author": "_Reinhard Zumkeller_, Apr 02 2003",
			"references": 2,
			"revision": 15,
			"time": "2019-10-24T10:07:56-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}