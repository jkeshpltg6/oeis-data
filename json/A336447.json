{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A336447",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 336447,
			"data": "1,2,3,5,7,41,37,31,29,521,509,337,109,43,47,83,89,179,173,359,353,349,113,293,307,311,313,317,191,97,101,103,107,691,683,197,193,1429,1427,887,883,661,659,653,463,461,457,181,467,479,1163,1171,331,673,677,1153,1151,487,491,199",
			"name": "Squares visited by a chess rook moving on a square-spiral numbered board where the rook moves to an unvisited square containing the smallest prime number.",
			"comment": [
				"This sequences gives the numbers of the squares visited by a chess rook moving on a square-spiral numbered board where the rook starts on the 1 numbered square and at each step moves to an unvisited square containing the smallest prime number. The movement is restricted to the four directions a rook can move on a standard chess board, and the rook cannot move over a previously visited square. Note that if the rook simply moves to an unvisited square containing the smallest number the sequence will be infinite as the rook will just follow the square spiral path.",
				"The sequence is finite. After 134 steps the square with number 863 is visited, after which all four squares the rook can move to have been visited.",
				"The first term where this sequence differs from A336413, where the rook steps to the closest unvisited prime, is a(7) = 37. See the examples below.",
				"The largest visited square is a(102) = 3739. The largest step distance between visited squares is 24 units, between a(128) = 2179 to a(129) = 2203. The largest prime gap between visited squares is 2646, from a(101) = 1093 to a(102) = 3739. The smallest unvisited prime is 11."
			],
			"link": [
				"Scott R. Shannon, \u003ca href=\"/A336447/a336447.png\"\u003eImage showing the 134 steps of the rook's path\u003c/a\u003e. A green square shows the starting 1 square, a red square shows the final square with number 863, and a thick white line is the path between visited squares. All visited prime numbered squares are shown in yellow, while those unvisited squares containing primes are shown in grey. The three squares which block the rook's movement from the final square are shown with a red border. The square spiral numbering of the board is shown as a thin white line. Click on the image to zoom in to see the prime numbers."
			],
			"example": [
				"The board is numbered with the square spiral:",
				".",
				"  17--16--15--14--13   .",
				"   |               |   .",
				"  18   5---4---3  12   29",
				"   |   |       |   |   |",
				"  19   6   1---2  11   28",
				"   |   |           |   |",
				"  20   7---8---9--10   27",
				"   |                   |",
				"  21--22--23--24--25--26",
				".",
				"a(1) = 1, the starting square for the rook.",
				"a(2) = 2. The four unvisited prime numbered squares around a(1) the rook can move to are numbered 2,61,19,23. Of these 2 is the smallest.",
				"a(7) = 37. The three unvisited prime numbered squares around a(6) = 41 the rook can move to are numbered 37,43,107. Of those 37 is the smallest. Note that 43 is the closest prime, being only 2 units away while 37 is 4 units away.",
				"a(135) = 863. The final square. The three previously visited prime numbered squares around a(135) are numbered 191,859,1709. Note there is no fourth prime as the column of squares directly upward from 863 contains no primes; the values from 871,994,1125,... and beyond fit the quadratic 4n^2+119n+871, which can be factored as (4n+67)*(n+13), and thus contains no primes."
			],
			"xref": [
				"Cf. A336446, A336402, A336413, A330979, A000040, A063826, A214664, A214665, A136626, A115258, A331027."
			],
			"keyword": "nonn,fini,full,walk",
			"offset": "1,2",
			"author": "_Scott R. Shannon_, Jul 22 2020",
			"references": 5,
			"revision": 13,
			"time": "2021-05-16T07:59:35-04:00",
			"created": "2020-07-25T09:51:41-04:00"
		}
	]
}