{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A214960",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 214960,
			"data": "1,-1,1,0,0,0,1,0,0,0,0,-1,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,1,-1,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,-1,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0",
			"name": "Expansion of psi(x^2) - x * psi(x^10) in powers of x where psi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A214960/b214960.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"R. Blecksmith; J. Brillhart; I. Gerst, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1988-0942157-2\"\u003eSome infinite product identities\u003c/a\u003e, Math. Comp. 51 (1988), no. 183, 301-314. see p. 310.",
				"S. Cooper and M. Hirschhorn, \u003ca href=\"http://dx.doi.org/10.1216/rmjm/1008959672\"\u003eOn some infinite product identities\u003c/a\u003e, Rocky Mountain J. Math., 31 (2001) 131-139. see p. 134 Theorem 6.",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of f(-x, -x^4) * f(-x^2, x^3) / f(-x^2, -x^2) = f(-x, x^4) * f(x^2, x^3) / f(-x^10, -x^10) in powers of x where f(,) is Ramanujan's two-variable theta function.",
				"Euler transform of period 20 sequence [ -1, 1, 1, 0, 0, 0, 1, 0, -1, -1, -1, 0, 1, 0, 0, 0, 1, 1, -1, -1, ...].",
				"a(n) = b(4*n + 1) where b(n) is multiplicative and b(2^e) = 0^e, b(5^e) = (-1)^e, else b(p^e) = (1 + (-1)^e) / 2.",
				"G.f.: Sum_{k\u003e0} x^(k*(k - 1)) - x^(5*k*(k - 1) + 1) = Product_{k\u003e0} (1 - x^(10*k)) * (1 - x^(10*k - 1)) * (1 + x^(10*k-2)) * (1 + x^(10*k - 3)) * (1 + x^(10*k - 4)) * (1 + x^(10*k - 6)) * (1 + x^(10*k - 7)) * (1 + x^(10*k - 8)) * (1 - x^(10*k - 9)).",
				"a(9*n + 2) = - a(5*n + 1) = a(n), a(5*n + 3) = a(5*n + 4) = a(6*n + 3) = a(6*n + 4) = a(9*n + 5) = a(9*n + 8) = 0.",
				"a(n) = (-1)^n * A127693(n). a(2*n) = A010054(n). a(3*n) = A089806(n). a(6*n) = A080995(n)."
			],
			"example": [
				"1 - x + x^2 + x^6 - x^11 + x^12 + x^20 + x^30 - x^31 + x^42 + x^56 - x^61 + ...",
				"q - q^5 + q^9 + q^25 - q^45 + q^49 + q^81 + q^121 - q^125 + q^169 + q^225 + ..."
			],
			"mathematica": [
				"f[x_, y_] := QPochhammer[-x, x*y]*QPochhammer[-y, x*y]*QPochhammer[x*y, x*y]; A214960[n_] := SeriesCoefficient[f[-x, -x^4]*f[-x^2, x^3]/f[-x^2, -x^2], {x, 0, n}]; Table[A214960[n], {n,0,50}] (* _G. C. Greubel_, Jun 18 2017 *)"
			],
			"program": [
				"(PARI) {a(n) = issquare(4*n + 1) - issquare(20*n + 5)}",
				"(PARI) {a(n) = local(A, p, e); if( n\u003c0, 0, A = factor(4*n + 1); prod( k=1, matsize(A)[1], if( p=A[k, 1], e=A[k, 2]; if( p==2, 0, if( p==5, (-1)^e, (1 + (-1)^e) / 2)))))}"
			],
			"xref": [
				"Cf. A010054, A080995, A089806, A127693."
			],
			"keyword": "sign",
			"offset": "0,1",
			"author": "_Michael Somos_, Jul 30 2012",
			"references": 2,
			"revision": 13,
			"time": "2021-03-12T22:24:46-05:00",
			"created": "2012-07-31T10:46:50-04:00"
		}
	]
}