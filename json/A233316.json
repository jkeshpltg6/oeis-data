{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A233316",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 233316,
			"data": "0,0,1,3,1,0,-1,0,2,0,-3,0,5,0,-691,0,140,0,-10851,0,219335,0,-1222277,0,1709026,0,-1181820455,0,538845489,0,-23749461029,0,68926730208040,0,-84802531453387,0,270657225128535,0,-26315271553053477373,0,380899208799402670,0,-1827579029475143854357",
			"name": "a(n) = Numerator(binomial(n+2, 2)*Bernoulli(n, 1)) for n \u003e= 0 and 0 for n \u003c 0.",
			"comment": [
				"Numerators of 0, 0, followed by A000217(n)*A164555(n)/A027642(n).",
				"Third fractional autosequence after (1) (Br0(n) = ) A164555/A027642 and (2) Br(n) = A229979/(c(n) = 1,1,1,2,1,6,... = 1 interleaved with A006955 or 1 followed by A050932; thanks to _Jean-François Alcover_). Hence",
				"Br2(n) = 0, 0, 1, 3/2, 1, 0, -1/2, 0, 2/3, 0, -3/2, 0, 5, 0, -691/30, ..., second complementary Bernoulli numbers.",
				"Br2(n) differences table:",
				"    0,     0,    1,    3/2,    1,     0,   -1/2, ...",
				"    0,     1,   1/2,  -1/2,   -1,   -1/2,   1/2, ...",
				"    1,   -1/2,  -1,   -1/2,   1/2,    1,    1/6, ...",
				"  -3/2,  -1/2,  1/2,    1,    1/2,  -5/6,  -3/2, ...",
				"    1,     1,   1/2,  -1/2,  -4/3,  -2/3,    2,  ...",
				"    0,   -1/2,  -1,   -5/6,   2/3,   8/3,   4/3, ...",
				"  -1/2,  -1/2,  1/6,   3/2,    2,   -4/3,   -8,  ... .",
				"The main diagonal is the double of the first upper diagonal. Then, the autosequence (its inverse binomial transform is the signed sequence) is of second kind. Note that Br0(n) is an autosequence of second kind and Br(n) an autosequence of first kind.",
				"First Bernoulli polynomials, i.e., for B(1) = -1/2, A196838/A196839, with 0's instead of the spaces:",
				"    1,     0,    0,    0,    0,   0,   0,   0,  0, ...",
				"  -1/2,    1,    0,    0,    0,   0,   0,   0,  0, ...",
				"   1/6,   -1,    1,    0,    0,   0,   0,   0,  0, ...",
				"    0,    1/2, -3/2,   1,    0,   0,   0,   0,  0, ...",
				"  -1/30,   0,    1,   -2,    1,   0,   0,   0,  0, ...",
				"    0,   -1/6,   0,   5/3, -5/2,  1,   0,   0,  0, ...",
				"   1/42,   0,  -1/2,   0,   5/2, -3,   1,   0,  0, ...",
				"    0,    1/6,   0,  -7/6,   0,   0, -7/2,  1,  0, ...",
				"  -1/30,   0,   2/3,   0,  -7/3,  0, 14/3, -4, 10, ... .",
				"First column: A164555/A027642 with -1/2 instead of 1/2, A027641/A027642.",
				"Second column: A229979/c(n) with -1 instead of 1, first column in A229979.",
				"Third column: Br2(n) with -3/2 instead of 3/2, first column of the first array.",
				"Etc.",
				"Sequences used for Brp(n). For p=1, Br(n) is used.",
				"  1, 1, 1, 1, 1,  1,  1,  1,  1,  1, ...",
				"  0, 1, 2, 3, 4,  5,  6,  7,  8,  9, ... = A001477,",
				"  0, 0, 1, 3, 6, 10, 15, 21, 28, 36, ...",
				"  0, 0, 0, 1, 4, 10, 20, 35, 56, 84, ... . See A052553.",
				"For every sequence, the multiplication by A164555/A027642 begins at 1.",
				"(Br0(n), Br(n), Br2(n), Br3(n), ... lead to A193815.)"
			],
			"example": [
				"a(2) =    1*1 =   1,",
				"a(3) =  3*1/2 =  3/2,",
				"a(4) =    6/6 =   1,",
				"a(5) =   10*0 =   0,",
				"a(6) = -15/30 = -1/2."
			],
			"mathematica": [
				"b[-2] = b[-1] = 0; b[1] = 1/2; b[n_] := BernoulliB[n]; a[n_] := (n+1)*(n+2)/2*b[n] // Numerator; Table[a[n], {n, -2, 40}] (* _Jean-François Alcover_, Dec 09 2013 *)"
			],
			"xref": [
				"Cf. A190339(array). A000012 for Br0(n), A000217 for Br(n)=A229979/c(n), A000217 for Br2(n), A000292 for Br3(n)."
			],
			"keyword": "sign,frac,tabl,uned",
			"offset": "-2,4",
			"author": "_Paul Curtz_, Dec 07 2013",
			"ext": [
				"Corrected and extended by _Jean-François Alcover_, Dec 09 2013"
			],
			"references": 2,
			"revision": 38,
			"time": "2019-08-24T03:20:30-04:00",
			"created": "2013-12-13T04:16:42-05:00"
		}
	]
}