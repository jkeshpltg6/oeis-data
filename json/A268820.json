{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A268820",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 268820,
			"data": "0,1,0,2,1,0,3,3,1,0,4,6,3,1,0,5,2,2,3,1,0,6,12,7,2,3,1,0,7,4,6,6,2,3,1,0,8,7,13,5,6,2,3,1,0,9,5,12,7,7,6,2,3,1,0,10,24,5,15,4,7,6,2,3,1,0,11,8,4,13,5,5,7,6,2,3,1,0,12,11,25,4,14,12,5,7,6,2,3,1,0,13,9,24,12,15,4,4,5,7,6,2,3,1,0,14,13,9,27,12,10,13,4,5,7,6,2,3,1,0",
			"name": "Square array A(r,c): A(0,c) = c, A(r,0) = 0, A(r\u003e=1,c\u003e=1) = A003188(1+A006068(A(r-1,c-1))) = A268717(1+A(r-1,c-1)), read by descending antidiagonals as A(0,0), A(0,1), A(1,0), A(0,2), A(1,1), A(2,0), ...",
			"link": [
				"Antti Karttunen, \u003ca href=\"/A268820/b268820.txt\"\u003eTable of n, a(n) for n = 0..32895; the first 256 antidiagonals of array\u003c/a\u003e"
			],
			"formula": [
				"For row zero: A(0,k) = k, for column zero: A(n,0) = 0, and in other cases: A(n,k) = A003188(1+A006068(A(n-1,k-1)))",
				"Other identities. For all n \u003e= 0:",
				"A(n,n) = A003188(n).",
				"A(A006068(n),A006068(n)) = n."
			],
			"example": [
				"The top left [0 .. 16] x [0 .. 19] section of the array:",
				"0, 1, 2, 3, 4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19",
				"0, 1, 3, 6, 2, 12,  4,  7,  5, 24,  8, 11,  9, 13, 15, 10, 14, 48, 16, 19",
				"0, 1, 3, 2, 7,  6, 13, 12,  5,  4, 25, 24,  9,  8, 15, 14, 11, 10, 49, 48",
				"0, 1, 3, 2, 6,  5,  7, 15, 13,  4, 12, 27, 25,  8, 24, 14, 10,  9, 11, 51",
				"0, 1, 3, 2, 6,  7,  4,  5, 14, 15, 12, 13, 26, 27, 24, 25, 10, 11,  8,  9",
				"0, 1, 3, 2, 6,  7,  5, 12,  4, 10, 14, 13, 15, 30, 26, 25, 27, 11,  9, 24",
				"0, 1, 3, 2, 6,  7,  5,  4, 13, 12, 11, 10, 15, 14, 31, 30, 27, 26,  9,  8",
				"0, 1, 3, 2, 6,  7,  5,  4, 12, 15, 13,  9, 11, 14, 10, 29, 31, 26, 30,  8",
				"0, 1, 3, 2, 6,  7,  5,  4, 12, 13, 14, 15,  8,  9, 10, 11, 28, 29, 30, 31",
				"0, 1, 3, 2, 6,  7,  5,  4, 12, 13, 15, 10, 14, 24,  8, 11,  9, 20, 28, 31",
				"0, 1, 3, 2, 6,  7,  5,  4, 12, 13, 15, 14, 11, 10, 25, 24,  9,  8, 21, 20",
				"0, 1, 3, 2, 6,  7,  5,  4, 12, 13, 15, 14, 10,  9, 11, 27, 25,  8, 24, 23",
				"0, 1, 3, 2, 6,  7,  5,  4, 12, 13, 15, 14, 10, 11,  8,  9, 26, 27, 24, 25",
				"0, 1, 3, 2, 6,  7,  5,  4, 12, 13, 15, 14, 10, 11,  9, 24,  8, 30, 26, 25",
				"0, 1, 3, 2, 6,  7,  5,  4, 12, 13, 15, 14, 10, 11,  9,  8, 25, 24, 31, 30",
				"0, 1, 3, 2, 6,  7,  5,  4, 12, 13, 15, 14, 10, 11,  9,  8, 24, 27, 25, 29",
				"0, 1, 3, 2, 6,  7,  5,  4, 12, 13, 15, 14, 10, 11,  9,  8, 24, 25, 26, 27"
			],
			"mathematica": [
				"A003188[n_]:=BitXor[n, Floor[n/2]]; A006068[n_]:=If[n\u003c2, n, Block[{m=A006068[Floor[n/2]]}, 2m + Mod[Mod[n,2] + Mod[m, 2], 2]]]; a[r_, 0]:= 0; a[0, c_]:=c; a[r_, c_]:= A003188[1 + A006068[a[r - 1, c - 1]]]; Table[a[c, r - c], {r, 0, 15}, {c, 0, r}] //Flatten (* _Indranil Ghosh_, Apr 02 2017 *)"
			],
			"program": [
				"(Scheme)",
				"(define (A268820 n) (A268820bi (A002262 n) (A025581 n)))",
				"(define (A268820bi row col) (cond ((zero? row) col) ((zero? col) 0) (else (A268717 (+ 1 (A268820bi (- row 1) (- col 1)))))))",
				"(define (A268820bi row col) (cond ((zero? row) col) ((zero? col) 0) (else (A003188 (+ 1 (A006068 (A268820bi (- row 1) (- col 1))))))))",
				"(PARI) A003188(n) = bitxor(n, n\\2);",
				"A006068(n) = if(n\u003c2, n, {my(m = A006068(n\\2)); 2*m + (n%2 + m%2)%2});",
				"a(r, c) = if(r==0, c, if(c==0, 0, A003188(1 + A006068(a(r - 1, c - 1)))));",
				"for(r=0, 15, for(c=0, r, print1(a(c, r - c),\", \"); ); print(); ); \\\\ _Indranil Ghosh_, Apr 02 2017",
				"(Python)",
				"def A003188(n): return n^(n//2)",
				"def A006068(n):",
				"    if n\u003c2: return n",
				"    else:",
				"        m=A006068(n//2)",
				"        return 2*m + (n%2 + m%2)%2",
				"def a(r, c): return c if r\u003c1 else 0 if c\u003c1 else A003188(1 + A006068(a(r - 1, c - 1)))",
				"for r in range(16):",
				"    print([a(c, r - c) for c in range(r + 1)]) # _Indranil Ghosh_, Apr 02 2017"
			],
			"xref": [
				"Cf. A003188, A006068.",
				"Inverses of these permutations can be found in table A268830.",
				"Row 0: A001477, Row 1: A268717, Row 2: A268821, Row 3: A268823, Row 4: A268825, Row 5: A268827, Row 6: A268831, Row 7: A268933.",
				"Rows converge towards A003188, which is also the main diagonal.",
				"Cf. array A268715 (can be extracted from this one).",
				"Cf. array A268833 (shows related Hamming distances with regular patterns)."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_Antti Karttunen_, Feb 14 2016",
			"references": 13,
			"revision": 33,
			"time": "2020-04-30T13:36:05-04:00",
			"created": "2016-02-17T17:23:04-05:00"
		}
	]
}