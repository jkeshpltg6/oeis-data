{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A182386",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 182386,
			"data": "1,0,1,-2,9,-44,265,-1854,14833,-133496,1334961,-14684570,176214841,-2290792932,32071101049,-481066515734,7697064251745,-130850092279664,2355301661033953,-44750731559645106,895014631192902121,-18795307255050944540,413496759611120779881",
			"name": "a(0) = 1, a(n) = 1 - n * a(n-1).",
			"comment": [
				"Hankel transform is A055209."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A182386/b182386.txt\"\u003eTable of n, a(n) for n = 0..450\u003c/a\u003e",
				"Shirali Kadyrov, Farukh Mashurov, \u003ca href=\"https://arxiv.org/abs/1912.03214\"\u003eGeneralized continued fraction expansions for Pi and E\u003c/a\u003e, arXiv:1912.03214 [math.NT], 2019."
			],
			"formula": [
				"a(n+2) = p(-1) where p(x) is the unique degree-n polynomial such that p(k) = A001563(k+1) for k = 0, 1, ..., n.",
				"E.g.f.: exp(x) / (1 + x).",
				"a(n) = (-1)^n * A000166(n).",
				"G.f.: 1/U(0) where U(k)=  1 - x + x*(k+1)/(1 + x*(k+1)/U(k+1)); (continued fraction). - _Sergei N. Gladkovskii_, Oct 13 2012",
				"E.g.f.: exp(x) / (1 + x) = 1/(1 - x^2/2 + x^3/(U(0) + 2*x))  where U(k)= k^2 + k*(4-x) - 2*x + 3 + x*(k+1)*(k+3)^2/U(k+1); (continued fraction). - _Sergei N. Gladkovskii_, Oct 16 2012",
				"G.f.: 1/Q(0) where Q(k) =1 + 4*k*x - x^2*(2*k + 1)^2/( 1 + (4*k+2)*x - x^2*(2*k + 2)^2/Q(k+1) ); (continued fraction). - _Sergei N. Gladkovskii_, Mar 10 2013",
				"G.f.: 1/Q(0), where Q(k)= 1 + (k+1)^2*(x) - x/(1 - x*(k+1)^2/Q(k+1)); (continued fraction). - _Sergei N. Gladkovskii_, Apr 18 2013",
				"a(n) = (-1)^n*Gamma(n+1,-1)*exp(-1), where Gamma(a,x) is the incomplete gamma function. - _Ilya Gutkovskiy_, May 05 2016",
				"0 = a(n)*(-a(n+1) +a(n+2) +a(n+3)) +a(n+1)*(+a(n+1) -2*a(n+2) -a(n+3)) +a(n+2)*(+a(n+2)) for all n\u003e=0. - _Michael Somos_, Jun 26 2018"
			],
			"example": [
				"G.f. = 1 + x^2 - 2*x^3 + 9*x^4 - 44*x^5 + 265*x^6 - 1854*x^7 + 14833*x^8 + ..."
			],
			"maple": [
				"a:= proc(n) option remember; `if`(n=0, 1, 1-n*a(n-1)) end:",
				"seq(a(n), n=0..30);  # _Alois P. Heinz_, Jul 19 2015"
			],
			"mathematica": [
				"a[n_] := x D[1/x Exp[x], {x, n}] x^n Exp[-x]",
				"Table[a[n] /. x -\u003e 1, {n, 0, 20}] (* _Gerry Martens_ , May 05 2016 *)",
				"a[0] = 1; a[n_] := a[n] = 1 - n a[n - 1]; Table[a@ n, {n, 0, 22}] (* _Michael De Vlieger_, May 05 2016 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, n==0, 1 - n * a(n-1))};",
				"(Sage)",
				"A182386 = lambda n: hypergeometric([-n, 1], [], 1)",
				"print([simplify(A182386(n)) for n in range(23)]) # _Peter Luschny_, Oct 19 2014",
				"(MAGMA) m:=35; R\u003cx\u003e:=PowerSeriesRing(Rationals(), m); b:=Coefficients(R!(Exp(x)/(1+x))); [Factorial(n-1)*b[n]: n in [1..m]]; // _G. C. Greubel_, Aug 11 2018"
			],
			"xref": [
				"Cf. A000166, A001563, A055209."
			],
			"keyword": "sign",
			"offset": "0,4",
			"author": "_Michael Somos_, Apr 27 2012",
			"references": 8,
			"revision": 55,
			"time": "2020-03-07T08:49:23-05:00",
			"created": "2012-04-27T16:57:59-04:00"
		}
	]
}