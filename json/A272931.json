{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A272931",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 272931,
			"data": "2,1,-7,-11,17,61,-7,-251,-223,781,1673,-1451,-8143,-2339,30233,39589,-81343,-239699,85673,1044469,701777,-3476099,-6283207,7621189,32754017,2269261,-128746807,-137823851,377163377,928458781,-580194727,-4294029851,-1973250943",
			"name": "a(n) = 2^(n+1)*cos(n*arctan(sqrt(15))).",
			"comment": [
				"For n \u003e= 1, |a(n)| is the unique odd positive solution y to 4^(n+1) = 15*x^2 + y^2. The value of x is |A106853(n-1)|. - _Jianing Song_, Jan 22 2019"
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A272931/b272931.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Paul Barry, \u003ca href=\"https://arxiv.org/abs/2004.04577\"\u003eOn a Central Transform of Integer Sequences\u003c/a\u003e, arXiv:2004.04577 [math.CO], 2020.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,-4)."
			],
			"formula": [
				"Let a(x) = x/2 - i*sqrt(15)*x/2 and b(x) = x/2 + i*sqrt(15)*x/2, then:",
				"a(n) = a(1)^n + b(1)^n.",
				"a(n) = n! [x^n] exp(a(x)) + exp(b(x)).",
				"a(n) = [x^n] (2 - x)/(4*x^2 - x + 1).",
				"a(n) = Sum_{k=0..floor(n/2)} (-4)^k*n*(n - k - 1)!/(k!*(n - 2*k)!) for n \u003e= 1.",
				"For n \u003e= 1, 15*a(n)^2 + A106853(n-1)^2 = 4^(n+1). - _Jianing Song_, Jan 22 2019",
				"a(n) = a(n-1) - 4*a(n-2) for n\u003e1. - _Colin Barker_, Jan 22 2019"
			],
			"maple": [
				"seq(simplify(((1-I*sqrt(15))^n + (1+I*sqrt(15))^n)/2^n), n=0..32);"
			],
			"mathematica": [
				"LinearRecurrence[{1, -4}, {2, 1}, 33]"
			],
			"program": [
				"(Sage)",
				"[lucas_number2(i, 1, 4) for i in range(33)]",
				"(PARI) Vec((2 - x) / (1 - x + 4*x^2) + O(x^40)) \\\\ _Colin Barker_, Jan 22 2019"
			],
			"xref": [
				"Cf. A087204, A002249, A106853."
			],
			"keyword": "sign,easy",
			"offset": "0,1",
			"author": "_Peter Luschny_, May 11 2016",
			"references": 2,
			"revision": 25,
			"time": "2020-07-23T19:23:06-04:00",
			"created": "2016-05-11T13:28:07-04:00"
		}
	]
}