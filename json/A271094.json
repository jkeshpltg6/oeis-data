{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A271094",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 271094,
			"data": "4,0,35,-35,107,-107,211,-211,347,-347,515,-515,715,-715,947,-947,1211,-1211,1507,-1507,1835,-1835,2195,-2195,2587,-2587,3011,-3011,3467,-3467,3955,-3955,4475,-4475,5027,-5027,5611,-5611,6227,-6227,6875,-6875,7555",
			"name": "First differences of number of active (ON,black) cells in n-th stage of growth of two-dimensional cellular automaton defined by \"Rule 275\", based on the 5-celled von Neumann neighborhood.",
			"comment": [
				"Initialized with a single black (ON) cell at stage zero."
			],
			"reference": [
				"S. Wolfram, A New Kind of Science, Wolfram Media, 2002; p. 170."
			],
			"link": [
				"Robert Price, \u003ca href=\"/A271094/b271094.txt\"\u003eTable of n, a(n) for n = 0..127\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"http://arxiv.org/abs/1503.01168\"\u003eOn the Number of ON Cells in Cellular Automata\u003c/a\u003e, arXiv:1503.01168 [math.CO], 2015",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ElementaryCellularAutomaton.html\"\u003eElementary Cellular Automaton\u003c/a\u003e",
				"S. Wolfram, \u003ca href=\"http://wolframscience.com/\"\u003eA New Kind of Science\u003c/a\u003e",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_2D_5-Neighbor_Cellular_Automata\"\u003eIndex to 2D 5-Neighbor Cellular Automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_Elementary_Cellular_Automata\"\u003eIndex to Elementary Cellular Automata\u003c/a\u003e"
			],
			"formula": [
				"Conjectures from _Colin Barker_, Mar 30 2016: (Start)",
				"a(n) = 4-9*(-1)^n+(4+8*(-1)^n)*n+4*(-1)^n*n^2 for n\u003e1.",
				"a(n) = 4*n^2+12*n-5 for n\u003e1 and even.",
				"a(n) = -4*n^2-4*n+13 for n\u003e1 and odd.",
				"a(n) = -a(n-1)+2*a(n-2)+2*a(n-3)-a(n-4)-a(n-5) for n\u003e6.",
				"G.f.: (4+4*x+27*x^2-8*x^3+6*x^4+4*x^5-5*x^6) / ((1-x)^2*(1+x)^3).",
				"(End)"
			],
			"mathematica": [
				"CAStep[rule_,a_]:=Map[rule[[10-#]]\u0026,ListConvolve[{{0,2,0},{2,1,2},{0,2,0}},a,2],{2}];",
				"code=275; stages=128;",
				"rule=IntegerDigits[code,2,10];",
				"g=2*stages+1; (* Maximum size of grid *)",
				"a=PadLeft[{{1}},{g,g},0,Floor[{g,g}/2]]; (* Initial ON cell on grid *)",
				"ca=a;",
				"ca=Table[ca=CAStep[rule,ca],{n,1,stages+1}];",
				"PrependTo[ca,a];",
				"(* Trim full grid to reflect growth by one cell at each stage *)",
				"k=(Length[ca[[1]]]+1)/2;",
				"ca=Table[Table[Part[ca[[n]][[j]],Range[k+1-n,k-1+n]],{j,k+1-n,k-1+n}],{n,1,k}];",
				"on=Map[Function[Apply[Plus,Flatten[#1]]],ca] (* Count ON cells at each stage *)",
				"Table[on[[i+1]]-on[[i]],{i,1,Length[on]-1}] (* Difference at each stage *)"
			],
			"xref": [
				"Cf. A271091."
			],
			"keyword": "sign,easy",
			"offset": "0,1",
			"author": "_Robert Price_, Mar 30 2016",
			"references": 1,
			"revision": 11,
			"time": "2016-03-31T02:23:42-04:00",
			"created": "2016-03-30T16:56:00-04:00"
		}
	]
}