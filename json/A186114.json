{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A186114",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 186114,
			"data": "1,1,2,1,1,3,0,0,0,2,1,1,1,2,4,0,0,0,0,0,3,1,1,1,1,1,2,5,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,2,4,0,0,0,0,0,0,0,0,0,3,1,1,1,1,1,1,1,2,2,3,6,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,2,5,0,0,0,0,0,0,0,0,0,0,0,0,0,4,1,1,1,1,1,1,1,1,1,1,1,2,2,3,7",
			"name": "Triangle of regions and partitions of integers (see Comments lines for definition).",
			"comment": [
				"Let r = T(n,k) be a record in the sequence. The consecutive records \"r\" are the natural numbers A000027. Consider the first n rows; the triangle T(n,k) has the property that the columns, without the zeros, from k..1, are also the partitions of r in juxtaposed reverse-lexicographical order, so k is also A000041(r), the number of partitions of r. Note that a record r is always the final term of a row if such row contains 1’s. The number of positive integer a(1)..r is A006128(r). The sums a(1)..r is A066186(r). Here the set of positive integers in every row (from 1 to n) is called a “region” of r. The number of regions of r equals the number of partitions of r. If T(n,1) = 1 then the row n is formed by the smallest parts, in nondecreasing order, of all partitions of T(n,n)."
			],
			"link": [
				"Robert Price, \u003ca href=\"/A186114/b186114.txt\"\u003eTable of n, a(n) for n = 1..196878, rows 1-627.\u003c/a\u003e",
				"Omar E. Pol, \u003ca href=\"http://www.polprimos.com/imagenespub/polpar02.jpg\"\u003eIllustration of the seven regions of 5\u003c/a\u003e"
			],
			"formula": [
				"T(n,1) = A167392(n).",
				"T(n,k) = A141285(n), if k = n."
			],
			"example": [
				"Triangle begins:",
				"1,",
				"1, 2,",
				"1, 1, 3,",
				"0, 0, 0, 2,",
				"1, 1, 1, 2, 4,",
				"0, 0, 0, 0, 0, 3,",
				"1, 1, 1, 1, 1, 2, 5,",
				"0, 0, 0, 0, 0, 0, 0, 2,",
				"0, 0, 0, 0, 0, 0, 0, 2, 4,",
				"0, 0, 0, 0, 0, 0, 0, 0, 0, 3,",
				"1, 1, 1, 1, 1, 1, 1, 2, 2, 3, 6",
				"...",
				"The row n = 11 contains the 6th record in the sequence:  a(66) = T(11,11) = 6, then consider the first 11 rows of triangle. Note that the columns, from k = 11..1, without the zeros, are also the 11 partitions of 6 in juxtaposed reverse-lexicographical order: [6], [3, 3], [4, 2], [2, 2, 2], [5, 1], [3, 2, 1], [4, 1, 1], [2, 2, 1, 1], [3, 1, 1, 1], [2, 1, 1, 1, 1], [1, 1, 1, 1, 1, 1]. See A026792."
			],
			"mathematica": [
				"A206437 = Cases[Import[\"https://oeis.org/A206437/b206437.txt\",",
				"     \"Table\"], {_, _}][[All, 2]];",
				"A194446 = Cases[Import[\"https://oeis.org/A194446/b194446.txt\",",
				"    \"Table\"], {_, _}][[All, 2]];",
				"f[n_] := Module[{v},",
				"   v = Take[A206437, A194446[[n]]];",
				"   A206437 = Drop[A206437, A194446[[n]]];",
				"   Reverse[PadRight[v, n]]];",
				"Table[f[n], {n, PartitionsP[20]}]  // Flatten (* _Robert Price_, Apr 26 2020 *)"
			],
			"xref": [
				"Mirror of triangle A193870. Column 1 gives A167392. Right diagonal gives A141285.",
				"Cf. A000041, A135010, A138121, A183152, A186412, A187219, A194436-A194439, A194446-A194448, A206437."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Omar E. Pol_, Aug 08 2011",
			"references": 52,
			"revision": 27,
			"time": "2020-04-26T21:52:09-04:00",
			"created": "2011-08-12T11:27:40-04:00"
		}
	]
}