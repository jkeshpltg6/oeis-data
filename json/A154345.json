{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A154345",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 154345,
			"data": "1,4,-2,12,-15,3,32,-76,36,0,80,-325,275,0,-30,192,-1266,1710,0,-720,180,448,-4655,9457,0,-10290,5670,-630,1024,-16472,48552,0,-114240,104160,-25200,0",
			"name": "B(n,k) an additive decomposition of (4^n-2^n)*B(n), B(n) the Bernoulli numbers (triangle read by rows).",
			"comment": [
				"The Swiss-Knife polynomials A153641 can be understood as a sum of polynomials. Evaluated at x=1 and multiplied by n this results in a decomposition of (4^n-2^n) times the Bernoulli numbers A027641/A027642 (for n\u003e0 and B_1 = 1/2)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A154345/b154345.txt\"\u003eTable of n, a(n) for the first 50 rows\u003c/a\u003e",
				"Peter Luschny, \u003ca href=\"http://www.luschny.de/math/seq/SwissKnifePolynomials.html\"\u003eThe Swiss-Knife polynomials.\u003c/a\u003e"
			],
			"formula": [
				"Let c(k) = frac{(-1)^{floor(k/4)}{2^{floor(k/2)}} [4 not div k] (Iverson notation).",
				"B(n,k) = Sum(v=0..k,(-1)^(v)*binomial(k,v)*n*c(k)*(v+2)^(n-1));",
				"B(n) = (Sum(k=0..n, B(n,k)) / (4^n-2^n)"
			],
			"example": [
				"1,",
				"4,    -2,",
				"12,   -15,    3,",
				"32,   -76,    36,    0,",
				"80,   -325,   275,   0, -30,",
				"192,  -1266,  1710,  0, -720,    180,",
				"448,  -4655,  9457,  0, -10290,  5670,   -630,",
				"1024, -16472, 48552, 0, -114240, 104160, -25200,  0."
			],
			"maple": [
				"B := proc(n,k) local v,c; c := m -\u003e if irem(m+1,4) = 0 then 0 else 1/((-1)^iquo(m+1,4)*2^iquo(m,2)) fi; add((-1)^(v)*binomial(k,v)*n*c(k)*(v+2)^(n-1),v=0..k) end: seq(print(seq(B(n,k),k=0..(n-1))),n=0..8);"
			],
			"mathematica": [
				"c[m_] := If[Mod[m+1, 4] == 0, 0, 1/((-1)^Quotient[m+1, 4]*2^Quotient[m, 2])]; b[n_, k_] := Sum[(-1)^v*Binomial[k, v]*n*c[k]*(v+2)^(n-1), {v, 0, k}]; Table[b[n, k], {n, 0, 8}, {k, 0, n - 1}] // Flatten (* _Jean-François Alcover_, Jul 30 2013, after Maple *)"
			],
			"xref": [
				"Cf. A153641, A154341, A154342, A154343, A154344."
			],
			"keyword": "easy,sign,tabl",
			"offset": "0,2",
			"author": "_Peter Luschny_, Jan 07 2009",
			"references": 6,
			"revision": 15,
			"time": "2016-09-13T02:54:17-04:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}