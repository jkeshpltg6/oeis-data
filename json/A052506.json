{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A052506",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 52506,
			"data": "1,0,2,3,16,65,336,1897,11824,80145,586000,4588001,38239224,337611001,3144297352,30779387745,315689119456,3383159052833,37790736663456,439036039824193,5294386116882280,66155074120062921,855156188538926296,11418964004032623809",
			"name": "E.g.f.: exp(x*exp(x)-x).",
			"comment": [
				"a(n) is the number of forests of rooted labeled trees with height exactly one.  Equivalently, the number of idempotent mappings from {1,2,...,n} into {1,2,...,n} where each fixed point has at least one (other than itself) element mapped to it.  See the second summation formula provided by Vladeta Jovovic with conditions on k, the number of fixed points. - _Geoffrey Critzer_, Sep 20 2012"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A052506/b052506.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=39\"\u003eEncyclopedia of Combinatorial Structures 39\u003c/a\u003e",
				"Vaclav Kotesovec, \u003ca href=\"/A216688/a216688.pdf\"\u003eAsymptotic solution of the equations using the Lambert W-function\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Sum_{k=0..n} binomial(n, k)*(n-k-1)^k. - _Vladeta Jovovic_, Apr 12 2003",
				"a(n) = Sum_{k=0..floor(n/2)} binomial(n, k)*k!*Stirling2(n-k, k). - _Vladeta Jovovic_, Dec 19 2004",
				"a(n) ~ exp((1-r*(n+r))/(1+r)) * n^(n+1/2) * sqrt(1+r) / (r^n * sqrt((1+r)^3 + n*(1+3*r+r^2))), where r satisfies exp(r)*(1+r) - (1+n)/r = 1. - _Vaclav Kotesovec_, Aug 04 2014",
				"(a(n)/n!)^(1/n) ~ exp(1/(2*LambertW(sqrt(n)/2))) / (2*LambertW(sqrt(n)/2)). - _Vaclav Kotesovec_, Aug 06 2014"
			],
			"maple": [
				"spec := [S,{S=Set(Tree), Tree=Prod(Z,Set(Z,0 \u003c card))},labeled]: seq(combstruct[count](spec, size=n), n=0..20);"
			],
			"mathematica": [
				"nn=20;Range[0,nn]! CoefficientList[Series[Exp[x(Exp[x]-1)], {x,0,nn}], x]  (* _Geoffrey Critzer_, Sep 20 2012 *)"
			],
			"program": [
				"(PARI) my(x='x+O('x^30)); Vec(serlaplace( exp(x*exp(x)-x) )) \\\\ _G. C. Greubel_, Nov 15 2017",
				"(MAGMA) m:=30; R\u003cx\u003e:=PowerSeriesRing(Rationals(), m); b:=Coefficients(R!( Exp(x*Exp(x)-x) )); [Factorial(n-1)*b[n]: n in [1..m-1]]; // _G. C. Greubel_, May 13 2019",
				"(Sage) m = 30; T = taylor(exp(x*exp(x)-x), x, 0, m); [factorial(n)*T.coefficient(x, n) for n in (0..m)] # _G. C. Greubel_, May 13 2019"
			],
			"xref": [
				"Cf. A000248, A240989."
			],
			"keyword": "easy,nonn",
			"offset": "0,3",
			"author": "encyclopedia(AT)pommard.inria.fr, Jan 25 2000",
			"references": 5,
			"revision": 46,
			"time": "2021-08-07T01:26:30-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}