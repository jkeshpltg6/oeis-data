{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A081719",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 81719,
			"data": "1,0,1,0,1,1,0,1,3,1,0,1,5,5,1,0,1,9,14,7,1,0,1,13,32,27,9,1,0,1,20,66,80,44,11,1,0,1,28,123,203,160,65,13,1,0,1,40,222,465,486,280,90,15,1,0,1,54,377,985,1305,990,448,119,17,1,0,1,75,630,1978,3203,3051,1807,672,152,19,1",
			"name": "Triangle T(n,k) read by rows, related to Faà di Bruno's formula (n \u003e= 0 and 0 \u003c= k \u003c= n).",
			"comment": [
				"From _Petros Hadjicostas_, May 30 2020: (Start)",
				"We may prove _Philippe Deléham_'s formula by induction on n. Let P(n,k) = A008284(n,k) and b(n) = A039809(n). For n = 0, Sum_{k=0..0} T(0,k) = 1 = b(1). Let n \u003e= 1, and assume his formula is true for all s \u003c n, i.e., Sum_{k=0..s} T(s,k) = b(s+1).",
				"Then Sum_{k=0..n} T(n, k) = Sum_{k=1..n} T(n,k) = Sum_{k=1..n} Sum_{s=k-1..n-1} P(n+1, s+1)*T(s, k-1) = Sum_{s=0..n-1} P(n+1, s+1) Sum_{k=1..s+1} T(s, k-1) = Sum_{s=0..n-1} P(n+1, s+1) Sum_{m=0..s} T(s,m) = Sum_{s=0..n-1} P(n+1, s+1)*b(s+1) = Sum_{r=1..n} P(n+1, r)*b(r) = b(n+1) (by the definition of b = A039809). (End)"
			],
			"link": [
				"Warren P. Johnson, \u003ca href=\"https://www.maa.org/sites/default/files/pdf/upload_library/22/Ford/Johnson217-234.pdf\"\u003eThe curious history of Faà di Bruno's formula\u003c/a\u003e, American Mathematical Monthly, 109 (2002), 217-234.",
				"Warren P. Johnson, \u003ca href=\"https://www.jstor.org/stable/2695352?seq=1\"\u003eThe curious history of Faà di Bruno's formula\u003c/a\u003e, American Mathematical Monthly, 109 (2002), 217-234.",
				"Winston C. Yang, \u003ca href=\"http://dx.doi.org/10.1016/S0012-365X(99)00412-4\"\u003eDerivatives are essentially integer partitions\u003c/a\u003e, Discrete Mathematics, 222(1-3), July 2000, 235-245."
			],
			"formula": [
				"There is a recurrence involving the partition function A008284.",
				"Sum_{k=0..n} T(n,k) = A039809(n+1). - _Philippe Deléham_, Sep 30 2006",
				"From _Petros Hadjicostas_, May 30 2020: (Start)",
				"T(n, k) = Sum_{s=k-1..n-1} A008284(n+1, s+1)*T(s, k-1) for 1 \u003c= k \u003c= n with T(0,0) = 1 and T(n,0) = 0 for n \u003e= 1.",
				"T(n, k=2) = A007042(n) = A047812(n,2). (End)"
			],
			"example": [
				"Triangle T(n,k) (with rows n \u003e= 0 and columns k = 0..n) begins:",
				"  1;",
				"  0, 1;",
				"  0, 1,  1;",
				"  0, 1,  3,  1;",
				"  0, 1,  5,  5,  1;",
				"  0, 1,  9, 14,  7,  1;",
				"  0, 1, 13, 32, 27,  9,  1;",
				"  0, 1, 20, 66, 80, 44, 11, 1;",
				"  ..."
			],
			"mathematica": [
				"(* b = A008284 *)",
				"b[n_, k_]:= b[n, k]= If[n\u003e0 \u0026\u0026 k\u003e0, b[n-1, k-1] + b[n-k, k], Boole[n==0 \u0026\u0026 k==0]];",
				"T[n_, k_]:= T[n, k]= If[n==0 \u0026\u0026 k==0, 1, If[k==0, 0,  Sum[T[j, k-1]*b[n+1, j+1], {j, k-1, n-1}] ]];",
				"Table[T[n, k], {n, 0, 12}, {k, 0, n}]//Flatten (* _G. C. Greubel_, May 31 2020 *)"
			],
			"program": [
				"(PARI) P(n, k)=#partitions(n-k, k); /* A008284 */",
				"tabl(nn) = {A = matrix(nn, nn, n, k, 0); A[1,1] = 1; for(n=2, nn, for(k=2, n, A[n,k] = sum(s=k-2, n-2, P(n, s+1)*A[s+1,k-1])));",
				"for (n=1, nn, for (k=1, n, print1(A[n, k], \", \"); ); print(); ); }  \\\\ _Petros Hadjicostas_, May 29 2020"
			],
			"xref": [
				"Cf. A007042, A008284, A039809, A047812."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "0,9",
			"author": "_N. J. A. Sloane_, Apr 05 2003",
			"ext": [
				"More terms from _Emeric Deutsch_, Feb 28 2005"
			],
			"references": 3,
			"revision": 31,
			"time": "2020-06-02T01:58:41-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}