{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A165141",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 165141,
			"data": "3,9,1,6,16,36,50,37,66,82,167,121,162,236,226,276,302,446,478,532,457,586,677,521,666,852,976,877,1006,1046,1277,1381,1857,1556,1507,1657,1832,1732,2336,2299,2007,2677,2326,2117,2591,2502,2516,2592,3106,3557",
			"name": "The least positive integer that can be written in exactly n ways as the sum of a square, a pentagonal number and a hexagonal number",
			"comment": [
				"On Sep 04 2009, _Zhi-Wei Sun_ conjectured that the sequence A160324 contains every positive integer, i.e., for each positive integer n there exists a positive integer s which can be written in exactly n ways as the sum of a square, a pentagonal number and a hexagonal number. Based on this conjecture we create the current sequence. It seems that 0.9 \u003c a(n)/n^2 \u003c 1.6 for n \u003e 33. _Zhi-Wei Sun_ conjectured that a(n)/n^2 has a limit c with 1.1 \u003c c \u003c 1.2. On Sun's request, his friend Qing-Hu Hou produced a list of a(n) for n = 1..913 (see the b-file)."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A165141/b165141.txt\"\u003eTable of n, a(n) for n = 1..913\u003c/a\u003e",
				"F. Ge and Z. W. Sun, \u003ca href=\"https://arxiv.org/abs/0906.2450\"\u003eOn some universal sums of generalized polygonal numbers\u003c/a\u003e, preprint, arXiv:0906.2450 [math.NT], 2009-2016.",
				"M. B. Nathanson, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9939-1987-0866422-3\"\u003eA short proof of Cauchy's polygonal number theorem\u003c/a\u003e, Proc. Amer. Math. Soc. 99(1987), 22-24.",
				"Zhi-Wei Sun, \u003ca href=\"https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;53c19cb4.0908\"\u003eA challenging conjecture on sums of polygonal number\u003c/a\u003e (a message to Number Theory List), 2009.",
				"Zhi-Wei Sun, \u003ca href=\"http://math.nju.edu.cn/~zwsun/PolyNumber.pdf\"\u003ePolygonal numbers, primes and ternary quadratic forms\u003c/a\u003e (a talk given at a number theory conference), 2009.",
				"Zhi-Wei Sun, \u003ca href=\"http://math.nju.edu.cn/~zwsun/MSPT.htm\"\u003eMixed Sums of Primes and Other Terms\u003c/a\u003e (a webpage).",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/0905.0635\"\u003eOn universal sums of polygonal numbers, preprint\u003c/a\u003e, arXiv:0905.0635 [math.NT], 2009-2015."
			],
			"formula": [
				"a(n) = min{m\u003e0: m=x^2+(3y^2-y)/2+(2z^2-z) has exactly n solutions with x,y,z=0,1,2,...}."
			],
			"example": [
				"For n=5 the a(5)=16 solutions are 0^2+1+15 = 1^2+0+15 = 2^2+12+0 = 3^2+1+6 = 4^2+0+0 = 16."
			],
			"mathematica": [
				"SQ[x_]:=x\u003e-1\u0026\u0026IntegerPart[Sqrt[x]]^2==x RN[n_]:=Sum[If[SQ[n-(3y^2-y)/2-(2z^2-z)],1,0], {y,0,Sqrt[n]},{z,0,Sqrt[Max[0,n-(3y^2-y)/2]]}] Do[Do[If[RN[m]==n,Print[n,\" \", m];Goto[aa]],{m,1,1000000}];Label[aa];Continue,{n,1,100}]"
			],
			"xref": [
				"Cf. A160324, A000290, A000326, A000384, A160325, A160326."
			],
			"keyword": "nice,nonn",
			"offset": "1,1",
			"author": "_Zhi-Wei Sun_, Sep 05 2009",
			"references": 1,
			"revision": 19,
			"time": "2018-09-11T08:57:55-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}