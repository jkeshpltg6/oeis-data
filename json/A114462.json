{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A114462",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 114462,
			"data": "1,1,1,1,2,3,6,7,1,18,19,5,54,59,18,1,166,191,65,7,522,631,242,34,1,1670,2123,906,154,9,5418,7247,3395,680,55,1,17786,25011,12746,2932,300,11,58974,87071,47931,12414,1540,81,1,197226,305275,180439,51878,7552",
			"name": "Triangle read by rows: T(n,k) is the number of Dyck paths of semilength n having k ascents of length 2 starting at an even level (0\u003c=k\u003c=floor(n/2)).",
			"comment": [
				"Row n has 1+floor(n/2) terms. Row sums are the Catalan numbers (A000108). Sum(kT(n,k), k=0..floor(n/2)) = binomial(2n-3,n-1)-binomial(2n-4,n) = A077587(n-2) (n\u003e=2). Column 0 yields A114464."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A114462/b114462.txt\"\u003eRows n = 0..200, flattened\u003c/a\u003e"
			],
			"formula": [
				"G.f.: G(t,z) satisfies zG^2-(1-z+tz-3tz^2+3z^2-z^3-t^2z^3+2tz^3)G+1-z+z^2+tz-tz^2=0."
			],
			"example": [
				"T(4,1) = 7 because we have (UU)DDUDUD, UD(UU)DDUD, UDUD(UU)DD, (UU)DUDDUD,",
				"UD(UU)DUDD, (UU)DUDUDD and (UU)DUUDDD, where U=(1,1), D=(1,-1) (the ascents of length 2 starting at an even level are shown between parentheses; note that the last path has an ascent of length 2 that starts at an odd level).",
				"Triangle starts:",
				"1;",
				"1;",
				"1,   1;",
				"2,   3;",
				"6,   7,  1;",
				"18, 19,  5;",
				"54, 59, 18, 1;"
			],
			"maple": [
				"G:= 1/2/z*(3*z^2+2*z^3*t+1-z^3*t^2-3*z^2*t-z^3+t*z-z -sqrt(1+20*z^3*t-18*z^5*t^2+15*z^4*t^2+18*z^5*t+6*z^5*t^3-2*z^4*t^3-12*z^2*t -12*z^3 -6*z-24*z^4*t-8*z^3*t^2+z^6-6*z^5+11*z^4 +z^2*t^2+6*z^6*t^2 -4*z^6*t^3 -4*z^6*t+z^6*t^4+2*t*z +11*z^2)): Gser:=simplify(series(G,z=0,17)): P[0]:=1: for n from 1 to 14 do P[n]:=coeff(Gser,z^n) od: for n from 0 to 14 do seq(coeff(t*P[n],t^j),j=1..1+floor(n/2)) od; # yields sequence in triangular form",
				"# second Maple program:",
				"b:= proc(x, y, t) option remember; `if`(y\u003c0 or y\u003ex, 0, `if`(x=0,",
				"      `if`(t=2, z, 1), expand(b(x-1, y-1, min(3, t+1))+",
				"      `if`(t=2 and irem(y, 2)=0, z, 1)*b(x-1, y+1, 0))))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, z, i), i=0..degree(p)))(b(2*n, 0$2)):",
				"seq(T(n), n=0..14);  # _Alois P. Heinz_, Mar 12 2014"
			],
			"mathematica": [
				"b[x_, y_, t_] := b[x, y, t] = If[y\u003c0 || y\u003ex, 0, If[x==0, If[t==2, z, 1], Expand[ b[x-1, y-1, Min[3, t+1]] + If[t==2 \u0026\u0026 Mod[y, 2]==0, z, 1]*b[x-1, y+1, 0]]]]; T[n_] := Function[{p}, Table[Coefficient[p, z, i], {i, 0, Exponent[p, z]}]][b[2*n, 0, 0]]; Table[T[n], {n, 0, 14}] // Flatten (* _Jean-François Alcover_, Mar 31 2015, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A077587, A000108, A114463, A114464, A114465, A102402."
			],
			"keyword": "nonn,tabf",
			"offset": "0,5",
			"author": "_Emeric Deutsch_, Nov 29 2005",
			"references": 5,
			"revision": 11,
			"time": "2015-03-31T03:24:18-04:00",
			"created": "2006-01-24T03:00:00-05:00"
		}
	]
}