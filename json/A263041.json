{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A263041",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 263041,
			"data": "1,-3,4,-5,8,-14,20,-25,37,-54,71,-91,121,-164,210,-264,343,-443,554,-687,863,-1087,1340,-1637,2021,-2489,3027,-3659,4442,-5391,6480,-7755,9306,-11153,13278,-15752,18711,-22203,26214,-30860,36354,-42777,50137,-58628",
			"name": "Expansion of f(-x, -x^5)^2 / f(x, x^3) in powers of x where f(, ) is Ramanujan's general theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A263041/b263041.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of f(-x)^3 * psi(x^3)^2 / f(-x^2)^4 in powers of x where psi(), f() are Ramanujan theta functions.",
				"Expansion of q^(-13/24) * eta(q)^3 * eta(q^6)^4 / (eta(q^2)^4 * eta(q^3)^2) in powers of q.",
				"Euler transform of period 6 sequence [ -3, 1, -1, 1, -3, -1, ...].",
				"a(n) = - A053269(3*n + 2).",
				"a(n) ~ (-1)^n * exp(sqrt(n/2)*Pi) / (6*sqrt(n)). - _Vaclav Kotesovec_, Apr 17 2016"
			],
			"example": [
				"G.f. = 1 - 3*x + 4*x^2 - 5*x^3 + 8*x^4 - 14*x^5 + 20*x^6 - 25*x^7 + 37*x^8 + ...",
				"G.f. = q^13 - 3*q^37 + 4*q^61 - 5*q^85 + 8*q^109 - 14*q^133 + 20*q^157 - 25*q^181 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ x^(-1/2) QPochhammer[ x] (EllipticTheta[ 2, 0, x^(3/2)] / EllipticTheta[ 2, 0, x^(1/2)])^2, {x, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A)^3 * eta(x^6 + A)^4 / (eta(x^2 + A)^4 * eta(x^3 + A)^2), n))};"
			],
			"xref": [
				"Cf. A053269."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Apr 17 2016",
			"references": 3,
			"revision": 19,
			"time": "2021-03-12T22:24:48-05:00",
			"created": "2016-04-17T13:26:34-04:00"
		}
	]
}