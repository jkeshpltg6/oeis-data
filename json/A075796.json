{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A075796",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 75796,
			"data": "2,38,682,12238,219602,3940598,70711162,1268860318,22768774562,408569081798,7331474697802,131557975478638,2360712083917682,42361259535039638,760141959546795802,13640194012307284798,244763350261984330562,4392100110703410665318,78813038642399407645162",
			"name": "Numbers k such that 5*k^2 + 5 is a square.",
			"comment": [
				"Bisection of A001077; a(n) = A001077(2*n-1). - _Greg Dresden_, Jun 08 2021"
			],
			"reference": [
				"A. H. Beiler, \"The Pellian.\" Ch. 22 in Recreations in the Theory of Numbers: The Queen of Mathematics Entertains. Dover, New York, New York, pp. 248-268, 1966.",
				"L. E. Dickson, History of the Theory of Numbers, Vol. II, Diophantine Analysis. AMS Chelsea Publishing, Providence, Rhode Island, 1999, pp. 341-400.",
				"Peter G. L. Dirichlet, Lectures on Number Theory (History of Mathematics Source Series, V. 16); American Mathematical Society, Providence, Rhode Island, 1999, pp. 139-147."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A075796/b075796.txt\"\u003eTable of n, a(n) for n = 1..200\u003c/a\u003e",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"J. J. O'Connor and E. F. Robertson, \u003ca href=\"https://web.archive.org/web/20170425000702/http://www-gap.dcs.st-and.ac.uk/~history/HistTopics/Pell.html\"\u003ePell's Equation\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PellEquation.html\"\u003ePell Equation.\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (18,-1)."
			],
			"formula": [
				"a(n) = (((9 + 4*sqrt(5))^n - (9 - 4*sqrt(5))^n) + ((9 + 4*sqrt(5))^(n-1) - (9 - 4*sqrt(5))^(n-1)))/(4*sqrt(5)).",
				"a(n) = 18*a(n-1) - a(n-2).",
				"a(n) = 2*A049629(n-1).",
				"Lim_{n-\u003einf} a(n)/a(n-1) = 8*phi + 1 = 9 + 4*sqrt(5).",
				"a(n+1) = 9*a(n) + 4*sqrt(5)*sqrt((a(n)^2+1)). - _Richard Choulet_, Aug 30 2007",
				"G.f.: 2*x*(1 + x)/(1 - 18*x + x^2). - _Richard Choulet_, Oct 09 2007",
				"From _Johannes W. Meijer_, Jul 01 2010: (Start)",
				"a(n) = A000045(6*n+3) + A000045(6*n)/2.",
				"a(n) = 2*A167808(6*n+4) - A167808(6*n+6).",
				"Lim_{k-\u003einf} a(n+k)/a(k) = A023039(n)*A060645(n)*sqrt(5).",
				"(End)",
				"5*A007805(n)^2 - 1 = a(n+1)^2. - _Sture Sjöstedt_, Nov 29 2011",
				"From _Peter Bala_, Nov 29 2013: (Start)",
				"a(n) = Lucas(6*n - 3)/2.",
				"Sum_{n \u003e= 1} 1/(a(n) + 5/a(n)) = 1/4. Compare with A002878, A005248, A023039. (End)",
				"Lim_{n-\u003einf} a(n)/A007805(n-1) = sqrt(5). - _A.H.M. Smeets_, May 29 2017",
				"E.g.f.: (exp((9 - 4*sqrt(5))*x)*(- 5 + 2*sqrt(5) + (5 + 2*sqrt(5))*exp(8*sqrt(5)*x)))/(2*sqrt(5)). - _Stefano Spezia_, Feb 13 2019",
				"Sum_{n \u003e 0} 1/a(n) = (1/log(9 - 4*sqrt(5)))*(- 17 - 38/sqrt(5))*sqrt(5*(9 - 4*sqrt(5)))*(- 9 + 4*sqrt(5))*(psi_{9 - 4*sqrt(5)}(1/2) - psi_{9 - 4*sqrt(5)}(1/2 - (I*Pi)/log(9 - 4*sqrt(5)))) approximately equal to 0.527868600269500798938265500122302016..., where psi_q(x) is the q-digamma function. - _Stefano Spezia_, Feb 25 2019"
			],
			"maple": [
				"with(combinat); A075796:=n-\u003efibonacci(6*n+3)+fibonacci(6*n)/2; seq(A075796(n), n=1..50); # _Wesley Ivan Hurt_, Nov 29 2013"
			],
			"mathematica": [
				"LinearRecurrence[{18, -1}, {2, 38}, 50] (* _Sture Sjöstedt_, Nov 29 2011; typo fixed by _Vincenzo Librandi_, Nov 30 2011 *)",
				"LucasL[6*Range[20]-3]/2 (* _G. C. Greubel_, Feb 13 2019 *)",
				"CoefficientList[Series[2*(1+x)/( 1-18*x+x^2 ), {x,0,20}],x] (* _Stefano Spezia_, Mar 02 2019 *)"
			],
			"program": [
				"(MAGMA) I:=[2,38]; [n le 2 select I[n] else 18*Self(n-1)-Self(n-2): n in [1..20]]; // _Vincenzo Librandi_, Nov 30 2011",
				"(MAGMA) [Lucas(6*n-3)/2: n in [1..20]]; // _G. C. Greubel_, Feb 13 2019",
				"(PARI) vector(20, n, (fibonacci(6*n-2) + fibonacci(6*n-4))/2) \\\\ _G. C. Greubel_, Feb 13 2019",
				"(Sage) [(fibonacci(6*n-2) + fibonacci(6*n-4))/2 for n in (1..20)] # _G. C. Greubel_, Feb 13 2019"
			],
			"xref": [
				"Cf. A000290, A306380, A001077."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_Gregory V. Richardson_, Oct 13 2002",
			"references": 11,
			"revision": 92,
			"time": "2021-06-13T03:45:54-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}