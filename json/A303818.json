{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A303818",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 303818,
			"data": "1,3,1,32,1,34,1,32,11,34,1,323,1,34,11,322,1,343,1,324,11,34,1,3232,11,34,11,324,1,3433,1,322,11,34,11,32342,1,34,11,3223,1,3434,1,324,111,34,1,32322,11,343,11,324,1,3434,11,3223,11,34,1,323432,1,34,111",
			"name": "Representation of the divisor set of n based on parities of divisor and complementary divisor.",
			"comment": [
				"The divisors of n counted in A038548(n) are sorted, each divisor is represented by a digit of 1 to 4, and these digits are concatenated to form the decimals of a(n).",
				"The parity digits are 1,2,3,4 and are mapped as follows:",
				"1: odd factor of an odd number",
				"2: even factor of an even number, paired with an even factor",
				"3: odd factor of an even number",
				"4: even factor of an even number, paired with an odd factor",
				"a(n) gives the significant or first half of the parity of n."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A303818/b303818.txt\"\u003eTable of n, a(n) for n = 1..16384\u003c/a\u003e",
				"G. R. Bryant, \u003ca href=\"http://numbergazing.org/divisor-parity-4/\"\u003eDivisor 4 Parity\u003c/a\u003e"
			],
			"formula": [
				"a(odd prime) = 1. - _Michel Marcus_, Jul 05 2018"
			],
			"example": [
				"For n=24, 24 has the following divisors: {1, 2, 3, 4, 6, 8, 12, 24} with the following divisor pairings {{1,24}, {2,12}, {3,8}, {4,6}}.",
				"The first divisor is 1, odd, and paired with an even, so we have: 3;",
				"the second divisor is 2, even, and paired with an even, so we have: 2;",
				"the third divisor is 3, odd, and paired with an even, so we have: 3;",
				"the fourth divisor is 4, even, and paired with an even, so we have: 2.",
				"That gives us the significant portion of the parity as 3232. (The full parity would include the complement and be 32322424.)"
			],
			"mathematica": [
				"Table[FromDigits[Map[Boole[OddQ@ #] \u0026 /@ {#, n/#} \u0026, Take[#, Ceiling[Length[#]/2]] \u0026@ Divisors@ n] /. {{1, 1} -\u003e 1, {0, 0} -\u003e 2, {1, 0} -\u003e 3, {0, 1} -\u003e 4}], {n, 100}] (* _Michael De Vlieger_, May 03 2018 *)"
			],
			"program": [
				"(PARI) par(d, nd) = if (d % 2, if (nd % 2, 1, 3), if (nd % 2, 4, 2));",
				"a(n) = my(s=\"\"); fordiv (n, d, if (d \u003c= n/d, s = concat(s, par(d, n/d)))); eval(s); \\\\ _Michel Marcus_, Jul 05 2018"
			],
			"xref": [
				"Cf. A247795."
			],
			"keyword": "nonn,base",
			"offset": "1,2",
			"author": "_Gregory Bryant_, Apr 30 2018",
			"references": 1,
			"revision": 18,
			"time": "2018-12-23T16:13:46-05:00",
			"created": "2018-08-03T18:10:49-04:00"
		}
	]
}