{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A087256",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 87256,
			"data": "1,1,1,6,1,3,1,3,1,12,1,3,1,3,1,8,1,3,1,3,1,6,1,3,1,3,1,13,1,3,1,3,1,8,1,3,1,3,1,6,1,3,1,3,1,9,1,3,1,3,1,11,1,3,1,3,1,6,1,3,1,3,1,21,1,3,1,3,1,8,1,3,1,3,1,6,1,3,1,3,1,78,1,3,1,3,1,8,1,3,1,3,1,6,1,3,1,3,1,9,1,3,1",
			"name": "Number of different initial values for 3x+1 trajectories in which the largest term appearing in the iteration is 2^n.",
			"comment": [
				"It would be interesting to know whether the ...1,3,1,3,1,x,1,3,1,3,1,... pattern persists. - _John W. Layman_, Jun 09 2004",
				"The observed pattern should persist. Proof: [1] a(odd)=1 because -1+2^odd is not divisible by 3, so in Collatz-algorithm 2^odd is preceded by increasing inverse step. Thus 2^odd is the only suitable initial value; [2] a[2k]\u003e=3 for k\u003e1 because 2^(2k)-1=-1+4^k=3A so {b=2^2k, (b-1)/3 and (2a-2)/3} are three relevant initial values. No more case arises unless condition-[3] (see below) was satisfied; [3] a[6k+4]\u003e=5 for k\u003e=1, ..iv=c=2^(6k+4); here {c, (c-1)/3, 2(c-1)/3, (2c-5)/9, (4c-10)/9} is 5 suitable initial values, iff (2c-5)/9 is integer; e.g. at 6k+4=10, {1024\u003c-341\u003c-682\u003c-227\u003c-454} back-tracking the iteration. - _Labos Elemer_, Jun 17 2004",
				"A105730 gives a(6k+4). - _David Wasserman_, Apr 18 2005",
				"From _Hartmut F. W. Hoft_, Jun 24 2016: (Start)",
				"Except for a(2)=1 the sequence has the 6-element quasiperiod 1, 3, 1, x, 1, 3  where x\u003e=6, but unequal to 7 and 10 (see links below and in A033496). Observe that for n=2^(6k+4)=16*2^(6k), n mod 9 = 7 so that (2n-5)/9 is an integer and a(n)\u003e=6.",
				"Conjecture: All numbers m \u003e 10 occur as values in A087256 (see A233293).",
				"The conjecture has been verified for all 10 \u003c k \u003c 133 for Collatz trajectories with maximum value through 2^(36000*6 + 4). The largest fan of initial values in this range, F(6*1993+4), has maximum 2^11962 and size 3958.",
				"(End)"
			],
			"link": [
				"Hartmut F. W. Hoft, \u003ca href=\"/A087256/a087256.pdf\"\u003eproof of quasi period 6\u003c/a\u003e"
			],
			"example": [
				"n = 10: 2^10 = 1024 = peak for trajectories started with initial value taken from the list: {151, 201, 227, 302, 341, 402, 454, 604, 682, 804, 908, 1024};",
				"a trajectory with peak=1024: {201, 604, 302, 151, 454, 227, 682, 341, 1024, 512, 256, 128, 64, 32, 16, 8, 4, 2, 1}"
			],
			"mathematica": [
				"c[x_]:=c[x]=(1-Mod[x, 2])*(x/2)+Mod[x, 2]*(3*x+1);c[1]=1; fpl[x_]:=FixedPointList[c, x]; {$RecursionLimit=1000;m=0}; Table[Print[{xm-1, m}];m=0; Do[If[Equal[Max[fpl[n]], 2^xm], m=m+1], {n, 1, 2^xm}], {xm, 1, 30}]"
			],
			"program": [
				"(PARI) f(n, m) = 1 + if(2*n \u003c= m, f(2*n, m), 0) + if (n%6 == 4, f(n\\3, m), 0); a(n) = f(2^n, 2^n); \\\\ _David Wasserman_"
			],
			"xref": [
				"Cf. A025586, A087251-A087254, A105730, A233293."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Labos Elemer_, Sep 08 2003",
			"ext": [
				"Terms a(19)-a(21) from _John W. Layman_, Jun 09 2004",
				"More terms from _David Wasserman_, Apr 18 2005"
			],
			"references": 7,
			"revision": 20,
			"time": "2016-07-02T01:29:51-04:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}