{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A325416",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 325416,
			"data": "1,2,0,4,8,6,32,30,12,24,48,96,60,120,240,480,960,1920,3840,2520,5040,10080,20160,40320,80640",
			"name": "Least k such that the omega-sequence of k sums to n, and 0 if none exists.",
			"comment": [
				"We define the omega-sequence of n (row n of A323023) to have length A323014(n) = adjusted frequency depth of n, and the k-th term is Omega(red^{k-1}(n)), where Omega = A001222 and red^{k} is the k-th functional iteration of red = A181819, defined by red(n = p^i*...*q^j) = prime(i)*...*prime(j) = product of primes indexed by the prime exponents of n. For example, we have 180 -\u003e 18 -\u003e 6 -\u003e 4 -\u003e 3, so the omega-sequence of 180 is (5,3,2,2,1) with sum 13."
			],
			"example": [
				"The sequence of terms together with their omega-sequences (n = 2 term not shown) begins:",
				"     1:",
				"     2:  1",
				"     4:  2 1",
				"     8:  3 1",
				"     6:  2 2 1",
				"    32:  5 1",
				"    30:  3 3 1",
				"    12:  3 2 2 1",
				"    24:  4 2 2 1",
				"    48:  5 2 2 1",
				"    96:  6 2 2 1",
				"    60:  4 3 2 2 1",
				"   120:  5 3 2 2 1",
				"   240:  6 3 2 2 1",
				"   480:  7 3 2 2 1",
				"   960:  8 3 2 2 1",
				"  1920:  9 3 2 2 1",
				"  3840: 10 3 2 2 1",
				"  2520:  7 4 3 2 2 1",
				"  5040:  8 4 3 2 2 1"
			],
			"mathematica": [
				"omseq[n_Integer]:=If[n\u003c=1,{},Total/@NestWhileList[Sort[Length/@Split[#]]\u0026,Sort[Last/@FactorInteger[n]],Total[#]\u003e1\u0026]];",
				"da=Table[Total[omseq[n]],{n,10000}];",
				"Table[If[!MemberQ[da,k],0,Position[da,k][[1,1]]],{k,0,Max@@da}]"
			],
			"xref": [
				"Cf. A056239, A181819, A181821, A304465, A307734, A323023, A325238, A325277, A325280, A325413, A325415.",
				"Omega-sequence statistics: A001222 (first omega), A001221 (second omega), A071625 (third omega), A323022 (fourth omega), A304465 (second-to-last omega), A182850 or A323014 (frequency depth), A325248 (Heinz number), A325249 (sum)."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Gus Wiseman_, Apr 25 2019",
			"references": 4,
			"revision": 6,
			"time": "2019-04-25T13:31:03-04:00",
			"created": "2019-04-25T13:31:03-04:00"
		}
	]
}