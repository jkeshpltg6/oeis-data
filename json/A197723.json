{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A197723",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 197723,
			"data": "4,7,1,2,3,8,8,9,8,0,3,8,4,6,8,9,8,5,7,6,9,3,9,6,5,0,7,4,9,1,9,2,5,4,3,2,6,2,9,5,7,5,4,0,9,9,0,6,2,6,5,8,7,3,1,4,6,2,4,1,6,8,8,8,4,6,1,7,2,4,6,0,9,4,2,9,3,1,3,4,9,7,9,4,2,0,5,2,2,3,8,0,1,3,1,7,5,6,0,1,9,7,3,2,2",
			"name": "Decimal expansion of (3/2)*Pi.",
			"comment": [
				"As radians, this is equal to 270 degrees or 300 gradians.",
				"Multiplying a number by -i (with i being the imaginary unit sqrt(-1)) is equivalent to rotating it by this amount on the complex plane.",
				"Named 'Pau' by Randall Munroe, as a humorous compromise between Pi and Tau. - _Orson R. L. Peters_, Jan 08 2017",
				"(3*Pi/2)*a^2 is the area of the cardioid whose polar equation is r = a*(1+cos(t)) and whose Cartesian equation is (x^2+y^2-a*x)^2 = a^2*(x^2+y^2). The length of this cardioid is 8*a. See the curve at the Mathcurve link. - _Bernard Schott_, Jan 29 2020"
			],
			"link": [
				"Ivan Panchenko, \u003ca href=\"/A197723/b197723.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Robert Ferréol, \u003ca href=\"https://www.mathcurve.com/courbes2d.gb/cardioid/cardioid.shtml\"\u003eCardioid\u003c/a\u003e, Mathcurve.",
				"Randall Munroe, \u003ca href=\"http://xkcd.com/1292/\"\u003exkcd: Pi vs. Tau\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Cardioid.html\"\u003eCardioid\u003c/a\u003e",
				"\u003ca href=\"/index/Tra#transcendental\"\u003eIndex entries for transcendental numbers\u003c/a\u003e"
			],
			"formula": [
				"2*Pi - Pi/2 = Pi + Pi/2.",
				"Equals Integral_{t=0..Pi} (1+cos(t))^2 dt. - _Bernard Schott_, Jan 29 2020",
				"Equals -4 + Sum_{k\u003e=1} (k+1)*2^k/binomial(2*k,k). - _Amiram Eldar_, Aug 19 2020"
			],
			"example": [
				"4.712388980384689857693965074919254326296..."
			],
			"maple": [
				"Digits:=100: evalf(3*Pi/2); # _Wesley Ivan Hurt_, Jan 08 2017"
			],
			"mathematica": [
				"RealDigits[3Pi/2, 10, 105][[1]]"
			],
			"program": [
				"(PARI) 3*Pi/2 \\\\ _Charles R Greathouse IV_, Jul 06 2018"
			],
			"xref": [
				"Cf. A019669, A003881, A347152."
			],
			"keyword": "nonn,cons",
			"offset": "1,1",
			"author": "_Alonso del Arte_, Oct 17 2011",
			"references": 12,
			"revision": 54,
			"time": "2021-08-20T05:53:06-04:00",
			"created": "2011-10-18T16:44:01-04:00"
		}
	]
}