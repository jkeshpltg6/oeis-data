{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A099011",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 99011,
			"data": "169,385,741,961,1121,2001,3827,4879,5719,6215,6265,6441,6479,6601,7055,7801,8119,9799,10945,11395,13067,13079,13601,15841,18241,19097,20833,20951,24727,27839,27971,29183,29953,31417,31535,34561,35459,37345",
			"name": "Pell pseudoprimes: odd composite numbers n such that P(n)-Kronecker(2,n) is divisible by n.",
			"comment": [
				"Here P(n) are the Pell numbers (A000129), defined by P(0)=0, P(1)=1, P(x) = 2*P(x-1) + P(x-2) and Kronecker(2,n) is equal to 1 if n is congruent to +-1 mod 8 and equal to -1 if n is congruent to +-3 mod 8."
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A099011/b099011.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (from Dana Jacobsen's site, terms 1..200 from Ralf Stephan)",
				"Antonio J. Di Scala, Nadir Murru, Carlo Sanna, \u003ca href=\"https://arxiv.org/abs/2001.00353\"\u003eLucas pseudoprimes and the Pell conic\u003c/a\u003e, arXiv:2001.00353 [math.NT], 2020.",
				"Dana Jacobsen, \u003ca href=\"http://ntheory.org/pseudoprimes.html\"\u003ePseudoprime Statistics, Tables, and Data\u003c/a\u003e (includes terms to 5e12)"
			],
			"example": [
				"169 is a Pell pseudoprime because P(169)-Kronecker(2,169) is divisible by 169."
			],
			"mathematica": [
				"pell[0] = 0; pell[1] = 1; pell[n_] := pell[n] = 2*pell[n - 1] + pell[n - 2]; pellpspQ[n_] := OddQ[n] \u0026\u0026 CompositeQ[n] \u0026\u0026 Divisible[pell[n] - JacobiSymbol[2, n], n]; Select[Range[40000], pellpspQ] (* _Amiram Eldar_, Nov 22 2019 *)"
			],
			"program": [
				"(Perl)",
				"use Math::Prime::Util qw/:all/;",
				"my($U,$V);",
				"foroddcomposites {",
				"  ($U,$V) = lucas_sequence($_, 2, -1, $_);",
				"  $U = ($U - kronecker(2,$_)) % $_;",
				"  print \"$_\\n\" if $U == 0;",
				"} 1e11;   # _Dana Jacobsen_, Sep 13 2014"
			],
			"xref": [
				"Cf. A000129."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Jack Brennen_, Nov 13 2004",
			"references": 7,
			"revision": 28,
			"time": "2020-03-20T15:08:37-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}