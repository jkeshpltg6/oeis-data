{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A341581",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 341581,
			"data": "0,1,2,5,10,20,37,70,130,243,450,836,1549,2874,5326,9875,18302,33928,62885,116566,216058,400483,742314,1375932,2550365,4727266,8762262,16241395,30104390,55800320,103429237,191712350,355350370,658663363,1220872210,2262960276",
			"name": "Number of steps needed to move the largest disk out from a stack of n disks in the Towers of Hanoi exchanging disks puzzle with 3 pegs.",
			"comment": [
				"Scorer, Grundy and Smith define a variation of the towers of Hanoi puzzle where the smallest disk moves freely and two disks can exchange positions when they differ in size by 1, are on different pegs, and each is top-most on its peg.  The puzzle is to move a stack of n disks from one peg to another.",
				"Stockmeyer et al. determine the shortest solution to the puzzle.  a(n) is their h(n) which is the number of steps to go from n disks on peg X to the largest disk to peg Y and the others remaining on X.  This arises in A341580 to go between subgraph \"connection\" points."
			],
			"link": [
				"Kevin Ryde, \u003ca href=\"/A341581/b341581.txt\"\u003eTable of n, a(n) for n = 0..700\u003c/a\u003e",
				"Paul K. Stockmeyer et al., \u003ca href=\"https://doi.org/10.1080/00207169508804452\"\u003eExchanging Disks in the Tower of Hanoi\u003c/a\u003e, International Journal of Computer Mathematics, volume 59, number 1-2, pages 37-47, 1995.  Also \u003ca href=\"http://www.cs.wm.edu/~pkstoc/gov.pdf\"\u003eauthor's copy\u003c/a\u003e.  a(n) = h(n) in section 3.",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,0,-1,2,-2).",
				"\u003ca href=\"/index/To#Hanoi\"\u003eIndex entries for sequences related to Towers of Hanoi\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A341579(n-2) + A341580(n-1) + 1, for n\u003e=2. [Stockmeyer et al.]",
				"a(n) = 2*a(n-1) - a(n-3) + 2*a(n-4) - 2*a(n-5).",
				"G.f.: x * (1 + x^2 + x^3) /( (1-x) * (1 - x - x^2 - 2*x^4) ).",
				"G.f.: -1/(1-x) + (1 + x + x^3)/(1 - x - x^2 - 2*x^4)."
			],
			"example": [
				"As a graph where each vertex is a configuration of disks on pegs and each edge is a step (as drawn by Scorer et al.),",
				"                A",
				"               / \\",
				"              *---*         n=3 disks",
				"             /     \\         A to D",
				"            *       *        steps",
				"           / \\     / \\      a(3) = 5",
				"          *---B---*---*",
				"             /     \\",
				"        D   /       \\   *",
				"       / \\ /         \\ / \\",
				"      *---C           *---*",
				"     /     \\         /     \\",
				"    *       *-------*       *",
				"   / \\     / \\     / \\     / \\",
				"  *---*---*---*   *---*---*---*",
				"The recurrence using A341579 and A341580 is steps A341580(2)=3 from A to B, +1 from B to C, and A341579(1) = 1 from C to D (the whole puzzle solution in an n-2 subgraph)."
			],
			"program": [
				"(PARI) my(p=Mod('x,'x^4-'x^3-'x^2-2)); a(n) = subst(lift(p^(n+2))\\'x,'x,2)/2 - 1;"
			],
			"xref": [
				"Cf. A341579, A341580."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Kevin Ryde_, Feb 16 2021",
			"references": 2,
			"revision": 13,
			"time": "2021-07-17T21:19:27-04:00",
			"created": "2021-02-20T00:35:45-05:00"
		}
	]
}