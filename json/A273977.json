{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A273977",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 273977,
			"data": "11,111,112,121,122,1111,1112,1121,1122,1123,1211,1212,1213,1221,1222,1223,1231,1232,1233,11111,11112,11121,11122,11123,11211,11212,11213,11221,11222,11223,11231,11232,11233,11234,12111,12112,12113,12121,12122,12123,12131,12132,12133,12134,12211,12212",
			"name": "Words of length n over an alphabet of size 9 that are in standard order with at least one letter is repeated.",
			"comment": [
				"We study words made of letters from an alphabet of size b, where b \u003e= 1. (Here b=9.) We assume the letters are labeled {1,2,3,...,b}. There are b^n possible words of length n.",
				"We say that a word is in \"standard order\" if it has the property that whenever a letter i appears, the letter i-1 has already appeared in the word. This implies that all words begin with the letter 1.",
				"These are the words described in row b=9 of the array in A278986.",
				"This sequence can be potentially expanded by a much more efficient algorithm than the brute-force one presented in the program section."
			],
			"reference": [
				"D. D. Hromada, Integer-based nomenclature for the ecosystem of repetitive expressions in complete works of William Shakespeare, submitted to special issue of Argument and Computation on Rhetorical Figures in Computational Argument Studies, 2016."
			],
			"link": [
				"Daniel Devatman Hromada, \u003ca href=\"/A273977/b273977.txt\"\u003eList of n, a(n) for n = 1..142407\u003c/a\u003e (All words with at most 10 digits.)",
				"Joerg Arndt and N. J. A. Sloane, \u003ca href=\"/A278984/a278984.txt\"\u003eCounting Words that are in \"Standard Order\"\u003c/a\u003e"
			],
			"mathematica": [
				"Select[Range[2*10^4], And[Max[DigitCount@ #] \u003e= 2, Range@ Length@ Union@ # == DeleteDuplicates@ # \u0026@ IntegerDigits@ #] \u0026] (* _Michael De Vlieger_, Nov 10 2016 *)"
			],
			"program": [
				"(PERL)",
				"# script brute-force checking all integers up to infinity",
				"$i=0;",
				"INCREMENT : while ($i) {",
				"$i++;",
				"my %d;",
				"$d{\"0\"}=1;",
				"$r=0;",
				"for $d (split //,$i) {",
				"next INCREMENT if !exists $d{($d-1)};",
				"if ($d{$d}) {",
				"$r=1;",
				"}",
				"$d{$d}=true;",
				"}",
				"print \"$i\\n\" if $r;",
				"}"
			],
			"xref": [
				"Cf. A278987."
			],
			"keyword": "base,easy,nonn",
			"offset": "1,1",
			"author": "_Daniel Devatman Hromada_, Nov 10 2016",
			"ext": [
				"Edited by _N. J. A. Sloane_, Dec 06 2016",
				"Duplicated terms removed from b-file by _Andrew Howroyd_, Feb 27 2018"
			],
			"references": 3,
			"revision": 35,
			"time": "2018-02-28T09:54:07-05:00",
			"created": "2016-12-06T12:07:31-05:00"
		}
	]
}