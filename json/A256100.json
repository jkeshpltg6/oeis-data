{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A256100",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 256100,
			"data": "1,1,1,1,1,1,1,1,1,2,1,3,4,5,2,6,2,7,2,8,2,9,2,10,2,11,2,12,2,3,2,4,13,5,6,7,3,8,3,9,3,10,3,11,3,12,3,13,3,4,3,5,14,6,14,7,8,9,4,10,4,11,4,12,4,13,4,14,4,5,4,6,15,7,15,8,15,9,10,11,5,12,5,13,5,14,5,15,5,6",
			"name": "In S = A007376 (read as a sequence) the digit S(n) appears a(n) times in the sequence S(1), ..., S(n).",
			"comment": [
				"The motivation to consider this sequence came from the proposal A256379 by Anthony Sand.",
				"This sequence can also be read as an irregular triangle (array) in which a(n, k) is the number of appearances of the k-th digit of n in the digits of 1, ... ,n-1 and the first k digits of n. See the example for the head of this array. The row length is A055842(n), n \u003e= 1.",
				"This can also be described as the ordinal transform of A007376. - _Franklin T. Adams-Watters_, Oct 10 2015"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A256100/b256100.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) gives the number of digits A007376(n) in the sequence starting with A007376(1) and ending with A007376(n)."
			],
			"example": [
				"a(10) = 2 because A007376(10) = 1 and that sequence up to n=10 is 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, and 1 appears twice.",
				"a(24) = 10 because A007376(24) = 1 and this is the tenth 1 in A007376 up to, and including, A007376(24).",
				"Read as a tabf array a(n, k) with row length A055842(n) this begins:",
				"   n\\k  1  2  ...",
				"   1:   1",
				"   2:   1",
				"   3:   1",
				"   4:   1",
				"   5:   1",
				"   6:   1",
				"   7:   1",
				"   8:   1",
				"   9:   1",
				"  10:   2  1",
				"  11:   3  4",
				"  12:   5  2",
				"  13:   6  2",
				"  14:   7  2",
				"  15:   8  2",
				"  16:   9  2",
				"  17:  10  2",
				"  18:  11  2",
				"  19:  12  2",
				"  20:   3  2",
				"  ..."
			],
			"mathematica": [
				"lim = 120; s = Flatten[IntegerDigits /@ Range@ lim]; f[n_] := Block[{d = IntegerDigits /@ Take[s, n] // Flatten // FromDigits}, DigitCount[d][[If[ s[[n]] == 0, 10, s[[n]] ]] ] ]; Array[f, lim] (* _Michael De Vlieger_, Apr 08 2015, after _Robert G. Wilson v_ at A007376 *)"
			],
			"program": [
				"(Haskell)",
				"a256100 n = a256100_list !! (n-1)",
				"a256100_list = f a007376_list $ take 10 $ repeat 1 where",
				"   f (d:ds) counts = y : f ds (xs ++ (y + 1) : ys) where",
				"                           (xs, y:ys) = splitAt d counts",
				"-- _Reinhard Zumkeller_, Aug 13 2015"
			],
			"xref": [
				"Cf. A007376, A065648."
			],
			"keyword": "nonn,base,easy,look",
			"offset": "1,10",
			"author": "_Wolfdieter Lang_, Apr 08 2015",
			"references": 6,
			"revision": 27,
			"time": "2017-03-24T00:47:57-04:00",
			"created": "2015-04-09T05:46:49-04:00"
		}
	]
}