{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A022307",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 22307,
			"data": "0,0,0,1,1,1,1,1,2,2,2,1,2,1,2,3,3,1,3,2,4,3,2,1,4,2,2,4,4,1,5,2,4,3,2,3,5,3,3,3,6,2,5,1,5,5,3,1,6,3,5,3,4,2,6,4,6,5,3,2,8,2,3,5,6,3,5,3,5,5,7,2,8,2,4,5,5,4,6,2,9,7,3,1,9,4,3,4,9,2,10,4,6,4,2,6,9,4,5,6",
			"name": "Number of distinct prime factors of n-th Fibonacci number.",
			"comment": [
				"Although every prime divides some Fibonacci number, this is not true for the Lucas numbers. Exactly 1/3 of all primes do not divide any Lucas number. See Lagarias and Moree for more details. - _Jonathan Vos Post_, Dec 06 2006",
				"First occurrence of k: 0, 3, 8, 15, 20, 30, 40, 70, 60, 80, 90, 140, 176, 120, 168, 180, 324, 252, 240, 378, ..., . - _Robert G. Wilson v_, Dec 10 2006 [Other than 0, this is sequence A060320. - _Jon E. Schoenfield_, Dec 30 2016]",
				"Row lengths of table A060442. - _Reinhard Zumkeller_, Aug 30 2014",
				"If k properly divides n then a(n) \u003e= a(k) + 1, except for a(6) = a(3) = 1. - _Robert Israel_, Aug 18 2015"
			],
			"reference": [
				"Alfred Brousseau, Fibonacci and Related Number Theoretic Tables, The Fibonacci Association, 1972, pages 1-8."
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A022307/b022307.txt\"\u003eTable of n, a(n) for n = 0..1408\u003c/a\u003e (terms 0..1000 from T. D. Noe derived from Kelly's data)",
				"Blair Kelly, \u003ca href=\"http://mersennus.net/fibonacci//\"\u003eFibonacci and Lucas Factorizations\u003c/a\u003e",
				"J. C. Lagarias, \u003ca href=\"http://projecteuclid.org/euclid.pjm/1102706452\"\u003eThe set of primes dividing the Lucas numbers has density 2/3\u003c/a\u003e, Pacific J. Math., 118. No. 2, (1985), 449-461.",
				"J. C. Lagarias, \u003ca href=\"http://projecteuclid.org/euclid.pjm/1102622818\"\u003eErrata to: The set of primes dividing the Lucas numbers has density 2/3\u003c/a\u003e, Pacific J. Math., 162, No. 2, (1994), 393-396.",
				"Hisanori Mishima, WIFC (World Integer Factorization Center), \u003ca href=\"http://www.asahi-net.or.jp/~kc2h-msm/mathland/matha1/matha108.htm\"\u003eFibonacci numbers (n = 1 to 100\u003c/a\u003e, \u003ca href=\"http://www.asahi-net.or.jp/~kc2h-msm/mathland/matha1/matha109.htm\"\u003en = 101 to 200\u003c/a\u003e, \u003ca href=\"http://www.asahi-net.or.jp/~kc2h-msm/mathland/matha1/matha110.htm\"\u003en = 201 to 300\u003c/a\u003e, \u003ca href=\"http://www.asahi-net.or.jp/~kc2h-msm/mathland/matha1/matha111.htm\"\u003en = 301 to 400\u003c/a\u003e, \u003ca href=\"http://www.asahi-net.or.jp/~kc2h-msm/mathland/matha1/matha112.htm\"\u003en = 401 to 480)\u003c/a\u003e.",
				"Pieter Moree, \u003ca href=\"http://dx.doi.org/10.2140/pjm.1998.186.267\"\u003eCounting Divisors of Lucas Numbers\u003c/a\u003e, Pacific J. Math, Vol. 186, No. 2, 1998, pp. 267-284.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/FibonacciNumber.html\"\u003eFibonacci Number\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Sum{d|n} A086597(d), Mobius transform of A086597.",
				"a(n) = A001221(A000045(n)) = omega(F(n). - _Jonathan Vos Post_, Dec 06 2006"
			],
			"mathematica": [
				"Table[Length[FactorInteger[Fibonacci[n]]], {n, 150}]"
			],
			"program": [
				"(PARI) a(n)=omega(fibonacci(n)) \\\\ _Charles R Greathouse IV_, Feb 03 2014",
				"(Haskell)",
				"a022307 n = if n == 0 then 0 else a001221 $ a000045 n",
				"-- _Reinhard Zumkeller_, Aug 30 2014",
				"(MAGMA) [0] cat [#PrimeDivisors(Fibonacci(n)): n in [1..100]]; // _Vincenzo Librandi_, Jul 26 2017"
			],
			"xref": [
				"Cf. A038575 (number of prime factors, counting multiplicity), A086597 (number of primitive prime factors).",
				"Cf. A000032, A000040, A000045, A001221, A053028.",
				"Cf. A060442, A086598 (omega(Lucas(n)).",
				"Cf. A060320. - _Jon E. Schoenfield_, Dec 30 2016"
			],
			"keyword": "nonn",
			"offset": "0,9",
			"author": "_Clark Kimberling_",
			"references": 27,
			"revision": 66,
			"time": "2019-10-14T08:54:41-04:00",
			"created": "1998-06-14T03:00:00-04:00"
		}
	]
}