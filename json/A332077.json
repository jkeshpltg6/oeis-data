{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A332077",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 332077,
			"data": "1,2,1,3,2,1,4,7,2,1,5,11,21,2,1,6,21",
			"name": "Square array of sunflower numbers Sun(m,n) = minimal number of distinct sets of cardinality \u003c= m such that there is a sunflower with at least n sets among them, read by falling antidiagonals; m, n \u003e= 1.",
			"comment": [
				"A sunflower S is a collection of sets such that all pairwise intersections of distinct A, B in S are equal. The intersection of all the sets is called the core or kernel of S.",
				"Some authors (e.g., Wikipedia) use \"more than\" instead of \"at least\" in the definition, which corresponds to an index n decreased by 1. We use the same conventions Tao (but following OEIS standards we use m,n instead of k,r). Also, some authors (e.g., Abbott et al. and the Polymath wiki page) use f(k,r) = Sun(k,r) - 1 which is not the minimal number of required sets, but such that any collection of *more than* f(k,r) sets has the given property.",
				"Bell et al. improve Rao's bound [as reproved by Tao] from Sun(m,n) \u003c= O(n log(mn))^m for m, n \u003e= 2 to the slightly cleaner bound Sun(m,n) \u003c= O(n log m)^m for m, n \u003e= 2. [Pers. comm. from L. Warnke.] - _M. F. Hasler_, May 02 2021"
			],
			"link": [
				"Abbott H.L., Hanson D. and Sauer N., \u003ca href=\"https://doi.org/10.1016/0097-3165(72)90103-3\"\u003eIntersection Theorems for Systems of Sets\u003c/a\u003e. J. Comb. Theory A 12 (1972) pp. 381-389. doi:10.1016/0097-3165(72)90103-3",
				"T. Bell, C. Chueluecha and L. Warnke, \u003ca href=\"http://doi.org/10.1016/j.disc.2021.112367\"\u003eNote on Sunflowers\u003c/a\u003e, Discrete Mathematics 344 (2021), 112340; DOI: 10.1016/j.disc.2021.112367; preprint arXiv:2009.09327.",
				"P. Erdös, R. Rado, \u003ca href=\"https://doi.org/10.1112/jlms/s1-35.1.85\"\u003eIntersection Theorems for Systems of Sets\u003c/a\u003e, Journal of the London Mathematical Society, s1-35 (1960) pp. 85-90. doi:10.1112/jlms/s1-35.1.85",
				"Polymath wiki, \u003ca href=\"https://asone.ai/polymath/index.php?title=The_Erdos-Rado_sunflower_lemma\"\u003eThe Erdos-Rado sunflower lemma\u003c/a\u003e, as of Feb 5, 2016",
				"Anup Rao, \u003ca href=\"https://arxiv.org/abs/1909.04774\"\u003eCoding for Sunflowers\u003c/a\u003e, arXiv:1909.04774 [math.CO], Sept. 2019.",
				"Terence Tao, \u003ca href=\"https://terrytao.wordpress.com/2020/07/20/the-sunflower-lemma-via-shannon-entropy\"\u003eThe sunflower lemma via Shannon entropy\u003c/a\u003e, personal blog \"What's new\", Jul 20 2020.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Sunflower_(mathematics)\"\u003eSunflower (mathematics)\u003c/a\u003e."
			],
			"formula": [
				"Sun(m,n) = n for n \u003c= 2 and all m;",
				"Sun(1,n) = n for all n: see Examples for explanation.",
				"Sun(2,n) = n(n-1)+1 if n is odd, (n-1)^2-n/2 if n is even. (Abbott-Hanson-Sauer)",
				"(n-1)^m \u003c= Sun(m,n) \u003c= (n-1)^m*m! + 1. (Erdös \u0026 Rado)",
				"Sun(m,n) \u003c= O(n log(mn))^m for m, n \u003e= 2. (Rao)",
				"Sun(m,n) \u003c= O(n log m)^m for m, n \u003e= 2. (Bell-Chueluecha-Warnke)",
				"Sunflower conjecture: Sun(m,n) \u003c= (n*O(1))^m."
			],
			"example": [
				"The table starts:",
				"   m \\n=1   2   3   4   5   6   7  ...",
				"  ---+-------------------------------",
				"   1 |  1   2   3   4   5   6   7  ...",
				"   2 |  1   2   7  11  21  28  43  ...",
				"   3 |  1   2  21  ...",
				"   4 |  1   2  ...",
				"   5 |  1   2  ...",
				"    ...",
				"Row m=1 has Sun(1,n) = n for all n, because any collection of n sets having at most 1 element (which may or may not include the empty set) makes up an n-petal sunflower S with an empty kernel.",
				"Columns n=1 and n=2 have Sun(m,n) = n for any m, because any single set A makes up a 1-petal sunflower S = {A}, and any two distinct sets A, B make up a 2-petal sunflower S = {A, B} with kernel A ∩ B, necessarily not equal to both A and B since they are distinct; then so the petals with at least one of them nonempty."
			],
			"xref": [
				"Cf. A236397, A266696."
			],
			"keyword": "nonn,hard,more,nice",
			"offset": "1,2",
			"author": "_M. F. Hasler_, Jul 27 2020",
			"references": 0,
			"revision": 21,
			"time": "2021-05-02T22:11:30-04:00",
			"created": "2020-08-01T11:40:22-04:00"
		}
	]
}