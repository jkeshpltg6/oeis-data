{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A299769",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 299769,
			"data": "1,1,4,2,0,9,3,8,0,16,5,4,9,0,25,7,16,18,16,0,36,11,12,18,16,25,0,49,15,32,27,48,25,36,0,64,22,28,54,32,50,36,49,0,81,30,60,54,80,75,72,49,64,0,100,42,60,90,80,100,72,98,64,81,0,121,56,108,126,160,125,180,98,128,81,100,0,144",
			"name": "Triangle read by rows: T(n,k) is the sum of all squares of the parts k in the last section of the set of partitions of n, with n \u003e= 1, 1 \u003c= k \u003c= n.",
			"comment": [
				"The partial sums of the k-th column of this triangle give the k-th column of triangle A299768.",
				"Note that the last section of the set of partitions of n is also the n-th section of the set of partitions of any positive integer \u003e= n."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A299769/b299769.txt\"\u003eRows n = 1..200, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = A299768(n,k) - A299768(n-1,k). - _Alois P. Heinz_, Jul 23 2018"
			],
			"example": [
				"Triangle begins:",
				"   1;",
				"   1,   4;",
				"   2,   0,   9;",
				"   3,   8,   0,  16;",
				"   5,   4,   9,   0,  25;",
				"   7,  16,  18,  16,   0,  36;",
				"  11,  12,  18,  16,  25,   0,  49;",
				"  15,  32,  27,  48,  25,  36,   0,  64;",
				"  22,  28,  54,  32,  50,  36,  49,   0,  81;",
				"  30,  60,  54,  80,  75,  72,  49,  64,   0, 100;",
				"  42,  60,  90,  80, 100,  72,  98,  64,  81,   0, 121;",
				"  56, 108, 126, 160, 125, 180,  98, 128,  81, 100,   0, 144;",
				"  ...",
				"Illustration for the 4th row of triangle:",
				".",
				".                                  Last section of the set",
				".        Partitions of 4.          of the partitions of 4.",
				".       _ _ _ _                              _",
				".      |_| | | |  [1,1,1,1]                 | |  [1]",
				".      |_ _| | |  [2,1,1]                   | |  [1]",
				".      |_ _ _| |  [3,1]                _ _ _| |  [1]",
				".      |_ _|   |  [2,2]               |_ _|   |  [2,2]",
				".      |_ _ _ _|  [4]                 |_ _ _ _|  [4]",
				".",
				"For n = 4 the last section of the set of partitions of 4 is [4], [2, 2], [1], [1], [1], so the squares of the parts are respectively [16], [4, 4], [1], [1], [1]. The sum of the squares of the parts 1 is 1 + 1 + 1 = 3. The sum of the squares of the parts 2 is 4 + 4 = 8. The sum of the squares of the parts 3 is 0 because there are no parts 3. The sum of the squares of the parts 4 is 16. So the fourth row of triangle is [3, 8, 0, 16]."
			],
			"maple": [
				"b:= proc(n, i) option remember; `if`(n=0 or i=1, 1+n*x, b(n, i-1)+",
				"      (p-\u003e p+(coeff(p, x, 0)*i^2)*x^i)(b(n-i, min(n-i, i))))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=1..n))(b(n$2)-b(n-1$2)):",
				"seq(T(n), n=1..14);  # _Alois P. Heinz_, Jul 23 2018"
			],
			"mathematica": [
				"b[n_, i_] := b[n, i] = If[n==0 || i==1, 1 + n*x, b[n, i-1] + Function[p, p + (Coefficient[p, x, 0]*i^2)*x^i][b[n-i, Min[n-i, i]]]];",
				"T[n_] := Function[p, Table[Coefficient[p, x, i], {i, 1, n}]][b[n, n] - b[n-1, n-1]];",
				"T /@ Range[14] // Flatten (* _Jean-François Alcover_, Dec 10 2019, after _Alois P.heinz_ *)"
			],
			"xref": [
				"Column 1 is A000041.",
				"Leading diagonal gives A000290, n \u003e= 1.",
				"Second diagonal gives A000007.",
				"Row sums give A206440.",
				"Cf. A066183, A135010, A206438, A299768."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Omar E. Pol_, Mar 20 2018",
			"references": 1,
			"revision": 36,
			"time": "2019-12-10T12:10:57-05:00",
			"created": "2018-07-23T20:16:46-04:00"
		}
	]
}