{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A182199",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 182199,
			"data": "4,2,3,2,2,2,2,1,2,2,1,1,1,1,2,2,2,1,2,1,1,1,1,1,2,2,1,2,2,2,1,1,2,1,2,1,2,2,2,2,1,1,1,2,1,1,2,2,1,2,2,1,1,1,1,1,2,1,1,1,2,2,2,1,2,2,2,1,1,1,2,1,1,1,1,2,1,2,1,1,1,1,2,1,2,1,2",
			"name": "Largest integer N such that a^(2^k) + b^(2^k) for 1 \u003c= k \u003c= N is prime, where p = a^2 + b^2 is the n-th prime of the form 4m+1.",
			"comment": [
				"a(1) corresponds to the first four Fermat primes. - _Thomas Ordowski_, Apr 22 2012",
				"Schinzel's hypothesis H implies that there are arbitrarily large terms in the sequence. - _Thomas Ordowski_, Apr 26 2012",
				"First value \u003e 4 is a(102416) = 5 (corresponding to p = 2823521). - _Robert Israel_, May 28 2015",
				"First value \u003e 5 is a(4250044) = 6 (corresponding to p = 151062433). - _Giovanni Resta_, Jun 09 2015"
			],
			"link": [
				"Charles R Greathouse IV, \u003ca href=\"/A182199/b182199.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"Let f(p,k) = a^(2^k)+b^(2^k), where f(p,1) = p is a prime of form 4k+1.",
				"f(5,1) = 5, f(5,2) = 17, f(5,3) = 257, f(5,4) = 65537, f(5,5) = 641*6700417. So N = 4. Next prime of form 4k+1 is 13; N = 2. 17; N = 3. etc."
			],
			"maple": [
				"N:= 10^4: # to get values corresponding to primes \u003c= N",
				"Primes:= select(isprime,[4*i+1 $ i=1..floor((N-1)/4)]):",
				"G:= map(p -\u003e [Re,Im](GaussInt:-GIfactors(p)[2][1][1]),Primes):",
				"f:= proc(ab) local j;",
				"  for j from 2 do if not isprime(ab[1]^(2^j)+ab[2]^(2^j)) then return(j-1) fi od",
				"end proc:",
				"map(f,G); # _Robert Israel_, May 28 2015"
			],
			"mathematica": [
				"nn = 35; pr = {}; Do[p = a^2 + b^2; If[p \u003c nn^2 \u0026\u0026 PrimeQ[p], AppendTo[pr, {p, a, b}]], {a, nn}, {b, a}]; pr = Sort[pr]; {jnk, a, b} = Transpose[pr]; Table[i = 1; While[PrimeQ[a[[n]]^2^i + b[[n]]^2^i], i++]; i - 1, {n, 2, Length[pr]}] (* _T. D. Noe_, Apr 24 2012 *)"
			],
			"program": [
				"(PARI) f(p)=my(s=lift(sqrt(Mod(-1, p))), x=p, t); if(s\u003ep/2, s=p-s); while(s^2\u003ep, t=s; s=x%s; x=t); s",
				"forprime(p=5,1e3,if(p%4==1,a=f(p);b=sqrtint(p-a^2);n=1; while(ispseudoprime(a^(2^n)+b^(2^n)),n++);print1(n-1\", \")))",
				"\\\\ _Charles R Greathouse IV_, Apr 24 2012"
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Thomas Ordowski_, Apr 20 2012",
			"references": 3,
			"revision": 51,
			"time": "2015-06-09T05:40:49-04:00",
			"created": "2012-04-25T12:02:24-04:00"
		}
	]
}