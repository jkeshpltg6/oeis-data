{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A346837",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 346837,
			"data": "1,0,1,-1,0,-1,-2,-1,0,-1,1,0,6,0,1,-4,1,12,6,0,1,-1,0,-15,0,-15,0,-1,-14,-17,12,1,-30,-15,0,-1,1,0,28,0,70,0,28,0,1,-40,-63,72,156,40,6,56,28,0,1",
			"name": "Table read by rows, coefficients of the determinant polynomials of the generalized tangent matrices.",
			"comment": [
				"The generalized tangent matrix M(n, k) is an N X N matrix defined for n in [1..N-1] and for k in [0..n-1] with h = floor((N+1)/2) as:",
				"   M[n - k, k + 1] = if n \u003c h then 1 otherwise -1,",
				"   M[N - n + k + 1, N - k] = if n \u003c N - h then -1 otherwise 1,",
				"and the indeterminate x in the main antidiagonal.",
				"The tangent matrix M(n, k) as defined in A346831 is the special case which arises from setting x = 0. The determinant of a generalized tangent matrix M is a polynomial which we call the determinant polynomial of M."
			],
			"example": [
				"Table starts:",
				"[0]   1;",
				"[1]   0,   1;",
				"[2]  -1,   0,  -1;",
				"[3]  -2,  -1,   0,  -1;",
				"[4]   1,   0,   6,   0,   1;",
				"[5]  -4,   1,  12,   6,   0,   1;",
				"[6]  -1,   0, -15,   0, -15,   0, -1;",
				"[7] -14, -17,  12,   1, -30, -15,  0, -1;",
				"[8]   1,   0,  28,   0,  70,   0, 28,  0, 1;",
				"[9] -40, -63,  72, 156,  40,   6, 56, 28, 0, 1.",
				".",
				"The first few generalized tangent matrices:",
				"1       2          3              4                  5",
				"---------------------------------------------------------------",
				"x;   -1  x;    1  -1  x;    1  -1  -1   x;   1   1  -1  -1   x;",
				"      x  1;   -1   x  1;   -1  -1   x   1;   1  -1  -1   x   1;",
				"               x   1  1;   -1   x   1   1;  -1  -1   x   1   1;",
				"                            x   1   1  -1;  -1   x   1   1   1;",
				"                                             x   1   1   1  -1;"
			],
			"maple": [
				"GeneralizedTangentMatrix := proc(N) local M, H, n, k;",
				"   M := Matrix(N, N); H := iquo(N + 1, 2);",
				"   for n from 1 to N - 1 do for k from 0 to n - 1 do",
				"       M[n - k, k + 1] := `if`(n \u003c H, 1, -1);",
				"       M[N - n + k + 1, N - k] := `if`(n \u003c N - H, -1, 1);",
				"od od; for k from 1 to N do M[k, N-k+1] := x od;",
				"M end:",
				"A346837Row := proc(n) if n = 0 then return 1 fi;",
				"   GeneralizedTangentMatrix(n):",
				"   LinearAlgebra:-Determinant(%);",
				"   seq(coeff(%, x, k), k = 0..n) end:",
				"seq(A346837Row(n), n = 0..9);"
			],
			"xref": [
				"Cf. A011782 (row sums modulo sign), A347596 (alternating row sums), A346831."
			],
			"keyword": "sign,tabl",
			"offset": "0,7",
			"author": "_Peter Luschny_, Sep 11 2021",
			"references": 2,
			"revision": 11,
			"time": "2021-09-12T04:24:35-04:00",
			"created": "2021-09-12T04:24:35-04:00"
		}
	]
}