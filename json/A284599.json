{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A284599",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 284599,
			"data": "0,0,3,0,5,3,7,0,3,5,11,3,13,7,8,0,17,3,19,5,10,11,0,3,5,13,3,7,29,8,31,0,14,17,12,3,0,19,16,5,41,10,43,11,8,0,0,3,7,5,20,13,0,3,16,7,22,29,59,8,61,31,10,0,18,14,0,17,3,12,71,3,73,0,8,19,18,16,0,5,3,41,0,10,22,43,32,11,0,8",
			"name": "Sum of twin prime (A001097) divisors of n.",
			"link": [
				"Robert Israel, \u003ca href=\"/A284599/b284599.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/TwinPrimes.html\"\u003eTwin Primes\u003c/a\u003e",
				"\u003ca href=\"/index/Su#sums_of_divisors\"\u003eIndex entries for sequences related to sums of divisors\u003c/a\u003e"
			],
			"formula": [
				"G.f.: Sum_{k\u003e=1} A001097(k)*x^A001097(k)/(1 - x^A001097(k)).",
				"a(n) = Sum_{d|n, d twin prime} d.",
				"a(A062729(n)) = 0.",
				"a(A001097(n)) = A001097(n)."
			],
			"example": [
				"a(15) = 8 because 15 has 4 divisors {1, 3, 5, 15} among which 2 are twin primes {3, 5} therefore 3 + 5 = 8."
			],
			"maple": [
				"N:= 200: # to get a(1)..a(N)",
				"P:= select(isprime, {seq(i,i=3..N+2)}):",
				"TP:= P intersect map(`-`,P,2):",
				"TP:= TP union map(`+`,TP,2):",
				"V:= Vector(N):",
				"for p in TP do",
				"  pm:= [seq(i,i=p..N,p)];",
				"  V[pm]:= map(`+`,V[pm],p);",
				"od:",
				"convert(V,list); # _Robert Israel_, Mar 30 2017"
			],
			"mathematica": [
				"Table[Total[Select[Divisors[n], PrimeQ[#1] \u0026\u0026 (PrimeQ[#1 - 2] || PrimeQ[#1 + 2]) \u0026]], {n, 80}]"
			],
			"program": [
				"(Python)",
				"from sympy import divisors, isprime",
				"def a(n): return sum([i for i in divisors(n) if isprime(i) and (isprime(i - 2) or isprime(i + 2))])",
				"print([a(n) for n in range(1, 91)]) # _Indranil Ghosh_, Mar 30 2017",
				"(PARI) a(n) = sumdiv(n, d, d*(isprime(d) \u0026\u0026 (isprime(d-2) || isprime(d+2)))); \\\\ _Michel Marcus_, Apr 02 2017"
			],
			"xref": [
				"Cf. A001097, A008472, A062729, A284203."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Ilya Gutkovskiy_, Mar 30 2017",
			"references": 2,
			"revision": 19,
			"time": "2021-05-09T11:19:02-04:00",
			"created": "2017-04-02T10:18:58-04:00"
		}
	]
}