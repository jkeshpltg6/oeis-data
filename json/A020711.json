{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A020711",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 20711,
			"data": "5,7,10,14,20,29,42,61,89,130,190,278,407,596,873,1279,1874,2746,4024,5897,8642,12665,18561,27202,39866,58426,85627,125492,183917,269543,395034,578950,848492,1243525,1822474,2670965,3914489,5736962,8407926,12322414,18059375",
			"name": "Pisot sequences E(5,7), P(5,7).",
			"link": [
				"Colin Barker, \u003ca href=\"/A020711/b020711.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Andrei Asinowski, Cyril Banderier, Valerie Roitner, \u003ca href=\"https://lipn.univ-paris13.fr/~banderier/Papers/several_patterns.pdf\"\u003eGenerating functions for lattice paths with several forbidden patterns\u003c/a\u003e, (2019).",
				"Shalosh B. Ekhad, N. J. A. Sloane and Doron Zeilberger, \u003ca href=\"https://arxiv.org/abs/1609.05570\"\u003eAutomated Proof (or Disproof) of Linear Recurrences Satisfied by Pisot Sequences\u003c/a\u003e, arXiv:1609.05570 [math.NT], 2016.",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=911\"\u003eEncyclopedia of Combinatorial Structures 911\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,-1,1,-1)."
			],
			"formula": [
				"a(n) = 2*a(n-1) - a(n-2) + a(n-3) - a(n-4) (holds at least up to n = 1000 but is not known to hold in general).",
				"Empirical g.f.: -(4*x^3-x^2+3*x-5) / ((x-1)*(x^3+x-1)). - _Colin Barker_, Oct 07 2014",
				"Theorem: E(5,7) satisfies a(n) = 3 a(n - 1) + 2 a(n - 2) + a(n - 3) - a(n - 4) for n \u003e= 4. Proved using the PtoRv program of Ekhad-Sloane-Zeilberger, and implies the above conjectures. - _N. J. A. Sloane_, Sep 09 2016",
				"Empirical formula: a(n) = a(n-1) + a(n-3) - 1. - _Greg Dresden_, May 18 2020"
			],
			"mathematica": [
				"PSE[a_,b_,n_] := Join[{x = a, y = b}, Table[z = Floor[y^2/x + 1/2]; x = y; y = z, {n}]]; A020711 = PSE[5,7,50] (* _Vladimir Joseph Stephan Orlovsky_, Mar 26 2011 *)",
				"LinearRecurrence[{2,-1,1,-1},{5,7,10,14},50] (* _Harvey P. Dale_, Jan 20 2017 *)"
			],
			"program": [
				"(PARI) Vec(-(4*x^3-x^2+3*x-5)/((x-1)*(x^3+x-1)) + O(x^40)) \\\\ _Jinyuan Wang_, Mar 10 2020"
			],
			"xref": [
				"See A008776 for definitions of Pisot sequences."
			],
			"keyword": "nonn",
			"offset": "0,1",
			"author": "_David W. Wilson_",
			"references": 3,
			"revision": 53,
			"time": "2020-05-21T10:26:15-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}