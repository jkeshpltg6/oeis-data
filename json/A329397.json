{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A329397",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 329397,
			"data": "1,2,4,7,12,20,33,55,92,156,267,466,822,1473,2668,4886,9021,16786,31413,59101,111654,211722,402697,768025,1468170",
			"name": "Number of compositions of n whose Lyndon factorization is uniform.",
			"comment": [
				"We define the Lyndon product of two or more finite sequences to be the lexicographically maximal sequence obtainable by shuffling the sequences together. For example, the Lyndon product of (231) with (213) is (232131), the product of (221) with (213) is (222131), and the product of (122) with (2121) is (2122121). A Lyndon word is a finite sequence that is prime with respect to the Lyndon product. Equivalently, a Lyndon word is a finite sequence that is lexicographically strictly less than all of its cyclic rotations. Every finite sequence has a unique (orderless) factorization into Lyndon words, and if these factors are arranged in lexicographically decreasing order, their concatenation is equal to their Lyndon product. For example, (1001) has sorted Lyndon factorization (001)(1).",
				"A sequence of words is uniform if they all have the same length."
			],
			"example": [
				"The a(1) = 1 through a(6) = 20 Lyndon factorizations:",
				"  (1)  (2)     (3)        (4)           (5)              (6)",
				"       (1)(1)  (12)       (13)          (14)             (15)",
				"               (2)(1)     (112)         (23)             (24)",
				"               (1)(1)(1)  (2)(2)        (113)            (114)",
				"                          (3)(1)        (122)            (123)",
				"                          (2)(1)(1)     (1112)           (132)",
				"                          (1)(1)(1)(1)  (3)(2)           (1113)",
				"                                        (4)(1)           (1122)",
				"                                        (2)(2)(1)        (3)(3)",
				"                                        (3)(1)(1)        (4)(2)",
				"                                        (2)(1)(1)(1)     (5)(1)",
				"                                        (1)(1)(1)(1)(1)  (11112)",
				"                                                         (12)(12)",
				"                                                         (2)(2)(2)",
				"                                                         (3)(2)(1)",
				"                                                         (4)(1)(1)",
				"                                                         (2)(2)(1)(1)",
				"                                                         (3)(1)(1)(1)",
				"                                                         (2)(1)(1)(1)(1)",
				"                                                         (1)(1)(1)(1)(1)(1)"
			],
			"mathematica": [
				"lynQ[q_]:=Array[Union[{q,RotateRight[q,#]}]=={q,RotateRight[q,#]}\u0026,Length[q]-1,1,And];",
				"lynfac[q_]:=If[Length[q]==0,{},Function[i,Prepend[lynfac[Drop[q,i]],Take[q,i]]][Last[Select[Range[Length[q]],lynQ[Take[q,#]]\u0026]]]];",
				"Table[Length[Select[Join@@Permutations/@IntegerPartitions[n],SameQ@@Length/@lynfac[#]\u0026]],{n,10}]"
			],
			"xref": [
				"Lyndon and co-Lyndon compositions are (both) counted by A059966.",
				"Lyndon compositions that are not weakly increasing are A329141.",
				"Lyndon compositions whose reverse is not co-Lyndon are A329324.",
				"Cf. A000740, A001037, A008965, A060223, A102659, A211100, A275692, A328596, A329312, A329318, A329395, A329396, A329398, A329399."
			],
			"keyword": "nonn,more",
			"offset": "1,2",
			"author": "_Gus Wiseman_, Nov 13 2019",
			"ext": [
				"a(19)-a(25) from _Robert Price_, Jun 20 2021"
			],
			"references": 1,
			"revision": 7,
			"time": "2021-06-21T03:09:04-04:00",
			"created": "2019-11-14T12:58:47-05:00"
		}
	]
}