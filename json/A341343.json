{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A341343",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 341343,
			"data": "1,3,3,6,3,9,3,8,6,9,3,18,3,9,9,9,3,18,3,18,9,9,3,24,6,9,8,18,3,27,3,9,9,9,9,36,3,9,9,24,3,27,3,18,18,9,3,27,6,18,9,18,3,24,9,24,9,9,3,54,3,9,18,9,9,27,3,18,9,27,3,48,3,9,18,18,9,27,3,27,9,9,3,54,9",
			"name": "Dirichlet g.f.: Sum_{n\u003e=1} a(n)/n^s = (zeta(s))^3 / (zeta(3*s))^2.",
			"comment": [
				"There is a family of multiplicative sequences based on trinomial numbers (A027907) and some fixed integer k \u003e= 0. Let a(k,n), k \u003e= 0, n \u003e 0, be multiplicative with a(k,p^e) = Sum_{i=0..e} trinomial(k,i) for prime p and e \u003e= 0, where trinomial(n,k) = 0 if 2*n \u003c k. These sequences have the Dirichlet g.f.: Sum_{n\u003e=1} a(k,n)/n^s = (zeta(s))^(k+1) / (zeta(3*s))^k. For several members of the family see A000012 (k=0), A073184 (k=1), and this sequence (k=2)."
			],
			"link": [
				"Vaclav Kotesovec, \u003ca href=\"/A341343/b341343.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"Multiplicative with a(p^e) = Sum_{i=0..e} trinomial(2,i) for prime p and e \u003e= 0, where trinomial(n,k) = 0 if 2*n \u003c k.",
				"Let b(n), n \u003e 0, be the Dirichlet inverse of a(n); b(n) is multiplicative with b(p^(3*e)) = 1 for e \u003e= 0, and b(p^(3*e-2)) = -3*e and b(p^(3*e-1)) = 3*e for e \u003e 0 and prime p.",
				"Sum_{k=1..n} a(k) ~ n * (log(n)^2/2 + (3*gamma - 6*zeta'(3)/zeta(3) - 1)*log(n) + 1 - 3*gamma + 3*gamma^2 + 6*(1 - 3*gamma)*zeta'(3)/zeta(3) + 27*zeta'(3)^2 / zeta(3)^2 - 9*zeta''(3)/zeta(3) - 3*sg1) / zeta(3)^2, where gamma is the Euler-Mascheroni constant A001620, zeta(3) = A002117, zeta'(3) = -A244115, zeta''(3) = A340442 and sg1 is the first Stieltjes constant (see A082633). - _Vaclav Kotesovec_, Nov 20 2021"
			],
			"program": [
				"(PARI) {T(n,k) = if( n\u003c0, 0, polcoeff( (1 + x + x^2)^n, k))}; \\\\ A027907",
				"a(n)={my(f=factor(n));prod(k=1,#f[,1],sum(i=0,f[k,2],T(2,i)))};",
				"for(j=1,75,print1(a(j),\", \")) \\\\ _Hugo Pfoertner_, Feb 13 2021",
				"(PARI) for(n=1, 100, print1(direuler(p=2, n, (1 - X^3)^2/(1 - X)^3)[n], \", \")) \\\\ _Vaclav Kotesovec_, Nov 20 2021"
			],
			"xref": [
				"Cf. A000012, A027907, A073184."
			],
			"keyword": "nonn,easy,mult",
			"offset": "1,2",
			"author": "_Werner Schulte_, Feb 09 2021",
			"references": 1,
			"revision": 15,
			"time": "2021-11-20T05:56:49-05:00",
			"created": "2021-02-18T12:35:36-05:00"
		}
	]
}