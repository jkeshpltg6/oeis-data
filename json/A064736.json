{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A064736",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 64736,
			"data": "1,2,6,3,12,4,20,5,35,7,56,8,72,9,90,10,110,11,143,13,182,14,210,15,240,16,272,17,306,18,342,19,399,21,462,22,506,23,552,24,600,25,650,26,702,27,756,28,812,29,870,30,930,31,992,32,1056,33,1122,34,1224,36",
			"name": "a(1)=1, a(2)=2; for n\u003e0, a(2*n+2) = smallest number missing from {a(1), ... ,a(2*n)}, and a(2*n+1) = a(2*n)*a(2*n+2).",
			"comment": [
				"Let c be the smallest positive constant such that for all permutations {a_n} of the positive integers, lim inf_{n -\u003e infinity} gcd(a_n, a_{n+1})/n \u003c= c. This sequence shows c \u003e= 1/2.",
				"The definition implies that if a(n) is prime then n is even. - _N. J. A. Sloane_, May 23 2017",
				"a(2n) ~ n+1 ~ n has asymptotic density 1 and a(2n-1) ~ n(n+1) ~ n^2 has asymptotic density zero. - _M. F. Hasler_, May 23 2017"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A064736/b064736.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Ray Chandler, \u003ca href=\"/A064736/a064736.txt\"\u003eTable of n, a(n) for n = 1..200000\u003c/a\u003e (large file, 2.8 MB)",
				"Ray Chandler, \u003ca href=\"/A064736/a064736.gz\"\u003eTable of n, a(n) for n = 1..2000000\u003c/a\u003e (large gzipped file)",
				"P. Erdős, R. Freud, and N. Hegyvári, \u003ca href=\"https://users.renyi.hu/~p_erdos/1983-02.pdf\"\u003eArithmetical properties of permutations of integers\u003c/a\u003e, Acta Mathematica Hungarica 41:1-2 (1983), pp. 169-176.",
				"Dana G. Korssjoen, Biyao Li, Stefan Steinerberger, Raghavendra Tripathi, and Ruimin Zhang, \u003ca href=\"https://arxiv.org/abs/2012.04625\"\u003eFinding structure in sequences of real numbers via graph theory: a problem list\u003c/a\u003e, arXiv:2012.04625, Dec 08, 2020",
				"Pierre Mazet, Eric Saias, \u003ca href=\"https://arxiv.org/abs/1803.10073\"\u003eEtude du graphe divisoriel 4\u003c/a\u003e, arXiv:1803.10073 [math.NT], 2018.",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"mathematica": [
				"A064736 = {a[1]=1, a[2]=2}; a[n_] := a[n] = (an = If[OddQ[n], a[n-1]*a[n+1], First[ Complement[ Range[n], A064736]]]; AppendTo[A064736, an]; an); Table[a[n], {n, 1, 62}] (*_Jean-François Alcover_, Aug 07 2012 *)"
			],
			"program": [
				"(Haskell)",
				"import Data.List (delete)",
				"a064736 n = a064736_list !! (n-1)",
				"a064736_list = 1 : 2 : f 1 2 [3..] where",
				"   f u v (w:ws) = u' : w : f u' w (delete u' ws) where u' = v * w",
				"-- _Reinhard Zumkeller_, Mar 23 2012"
			],
			"xref": [
				"A064745 gives inverse permutation.",
				"Interleaving of A286290 and A286291. See also A286292, A286293.",
				"Cf. A064764, A210770."
			],
			"keyword": "nonn,easy,nice",
			"offset": "1,2",
			"author": "J. C. Lagarias (lagarias(AT)umich.edu), Oct 21 2001",
			"ext": [
				"More terms from _Vladeta Jovovic_, Oct 21 2001",
				"Definition clarified by _N. J. A. Sloane_, May 23 2017"
			],
			"references": 16,
			"revision": 62,
			"time": "2020-12-09T01:46:43-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}