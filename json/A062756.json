{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A062756",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 62756,
			"data": "0,1,0,1,2,1,0,1,0,1,2,1,2,3,2,1,2,1,0,1,0,1,2,1,0,1,0,1,2,1,2,3,2,1,2,1,2,3,2,3,4,3,2,3,2,1,2,1,2,3,2,1,2,1,0,1,0,1,2,1,0,1,0,1,2,1,2,3,2,1,2,1,0,1,0,1,2,1,0,1,0,1,2,1,2,3,2,1,2,1,2,3,2,3,4,3,2,3,2,1,2,1,2,3,2",
			"name": "Number of 1's in ternary (base-3) expansion of n.",
			"comment": [
				"Fixed point of the morphism: 0 -\u003e010; 1 -\u003e121; 2 -\u003e232; ...; n -\u003e n(n+1)n, starting from a(0)=0. - _Philippe Deléham_, Oct 25 2011"
			],
			"link": [
				"R. Zumkeller, \u003ca href=\"/A062756/b062756.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"F. T. Adams-Watters and F. Ruskey, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL12/Ruskey2/ruskey14.html\"\u003eGenerating Functions for the Digital Sum and Other Digit Counting Sequences\u003c/a\u003e, JIS 12 (2009) 09.5.6.",
				"Michael Gilleland, \u003ca href=\"/selfsimilar.html\"\u003eSome Self-Similar Integer Sequences\u003c/a\u003e",
				"S. Northshield, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL18/Northshield/north4.html\"\u003eAn Analogue of Stern's Sequence for Z[sqrt(2)]\u003c/a\u003e, Journal of Integer Sequences, 18 (2015), #15.11.6.",
				"Kevin Ryde, \u003ca href=\"http://user42.tuxfamily.org/terdragon/index.html\"\u003eIterations of the Terdragon Curve\u003c/a\u003e, see index \"dir\".",
				"Robert Walker, \u003ca href=\"http://robertinventor.com/ftswiki/Self_Similar_Sloth_Canon_Number_Sequences\"\u003eSelf Similar Sloth Canon Number Sequences\u003c/a\u003e"
			],
			"formula": [
				"a(0) = 0, a(3n) = a(n), a(3n+1) = a(n)+1, a(3n+2) = a(n). - _Vladeta Jovovic_, Jul 18 2001",
				"G.f.: (Sum_{k\u003e=0} x^(3^k)/(1+x^(3^k)+x^(2*3^k)))/(1-x). In general, the generating function for the number of digits equal to d in the base b representation of n (0 \u003c d \u003c b) is (Sum_{k\u003e=0} x^(d*b^k)/(Sum_{i=0..b-1} x^(i*b^k)))/(1-x). - _Franklin T. Adams-Watters_, Nov 03 2005 [For d=0, use the above formula with d=b: (Sum_{k\u003e=0} x^(b^(k+1))/(Sum_{i=0..b-1} x^(i*b^k)))/(1-x), adding 1 if you consider the representation of 0 to have one zero digit.]",
				"a(n) = a(floor(n/3)) + (n mod 3) mod 2. - _Paul D. Hanna_, Feb 24 2006"
			],
			"mathematica": [
				"Table[Count[IntegerDigits[i, 3], 1], {i, 0, 200}]",
				"Nest[Join[#, # + 1, #] \u0026, {0}, 5] (* _IWABUCHI Yu(u)ki_, Sep 08 2012 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c1,0,a(n\\3)+(n%3)%2) \\\\ _Paul D. Hanna_, Feb 24 2006",
				"(Haskell)",
				"a062756 0 = 0",
				"a062756 n = a062756 n' + m `mod` 2 where (n',m) = divMod n 3",
				"-- _Reinhard Zumkeller_, Feb 21 2013"
			],
			"xref": [
				"Cf. A080846, A343785 (first differences).",
				"Cf. A081606 (indices of !=0).",
				"Indices of terms 0..6: A005823, A023692, A023693, A023694, A023695, A023696, A023697.",
				"Numbers of: A077267 (0's), A081603 (2's), A160384 (1's+2's).",
				"Other bases: A000120, A160381, A268643."
			],
			"keyword": "nonn,base",
			"offset": "0,5",
			"author": "Ahmed Fares (ahmedfares(AT)my-deja.com), Jul 16 2001",
			"ext": [
				"More terms from _Vladeta Jovovic_, Jul 18 2001"
			],
			"references": 42,
			"revision": 53,
			"time": "2021-11-10T16:34:58-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}