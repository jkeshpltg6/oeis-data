{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A016814",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 16814,
			"data": "1,25,81,169,289,441,625,841,1089,1369,1681,2025,2401,2809,3249,3721,4225,4761,5329,5929,6561,7225,7921,8649,9409,10201,11025,11881,12769,13689,14641,15625,16641,17689,18769,19881,21025,22201,23409,24649,25921,27225,28561,29929",
			"name": "a(n) = (4n+1)^2.",
			"comment": [
				"A bisection of A016754. Sequence arises from reading the line from 1, in the direction 1, 25, ..., in the square spiral whose vertices are the squares A000290. - _Omar E. Pol_, May 24 2008"
			],
			"link": [
				"Ivan Panchenko, \u003ca href=\"/A016814/b016814.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-3,1)."
			],
			"formula": [
				"a(n) = a(n-1) + 32 * n - 8, n \u003e 0. - _Vincenzo Librandi_, Dec 15 2010",
				"From _George F. Johnson_, Sep 28 2012: (Start)",
				"G.f.: (1 + 22*x + 9*x^2)/(1 - x)^3.",
				"a(n+1) = a(n) + 16 + 8*sqrt(a(n)).",
				"a(n+1) = 2*a(n) - a(n-1) + 32 = 3*a(n) - 3*a(n-1) + a(n-2).",
				"a(n-1)*a(n+1) = (a(n) - 16)^2 ; a(n+1) - a(n-1) = 16*sqrt(a(n)).",
				"a(n) = A016754(2*n) = (A016813(n))^2.",
				"(End)",
				"a(0) = 1, a(n) = a(n - 1) + 32(n - 1) - 8. - _Alonso del Arte_, Aug 19 2017",
				"Sum_{n\u003e=0} 1/a(n) = G/2 + Pi^2/16, where G is the Catalan constant (A006752). - _Amiram Eldar_, Jun 28 2020",
				"Product_{n\u003e=1} (1 - 1/a(n)) = 2*Gamma(5/4)^2/sqrt(Pi) = 2 * A068467^2 * A087197. - _Amiram Eldar_, Feb 01 2021"
			],
			"example": [
				"a(5) = (4 * 5 + 1)^2 = 21^2 = 441.",
				"a(6) = (4 * 6 + 1)^2 = 25^2 = 625."
			],
			"maple": [
				"A016814:=n-\u003e(4*n+1)^2; seq(A016814(k), k=0..100); # _Wesley Ivan Hurt_, Nov 02 2013"
			],
			"mathematica": [
				"(4 * Range[0, 40] + 1)^2 (* or *) LinearRecurrence[{3, -3, 1}, {1, 25, 81}, 40] (* _Harvey P. Dale_, Nov 20 2012 *)",
				"Accumulate[32Range[0, 47] - 8] + 9 (* _Alonso del Arte_, Aug 19 2017 *)"
			],
			"program": [
				"(PARI) a(n)=(4*n+1)^2 \\\\ _Charles R Greathouse IV_, Oct 07 2015"
			],
			"xref": [
				"Cf. A000290, A001539, A016742, A016754, A016802, A016826, A016838, A068467, A087197."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 13,
			"revision": 47,
			"time": "2021-02-01T02:56:18-05:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}