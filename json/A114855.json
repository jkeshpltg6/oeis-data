{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A114855",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 114855,
			"data": "1,-2,0,0,0,4,0,0,-5,0,0,0,0,0,0,0,7,0,0,0,0,-8,0,0,0,0,0,0,0,0,0,0,0,10,0,0,0,0,0,0,-11,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,13,0,0,0,0,0,0,0,0,-14,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,16,0,0,0,0,0,0,0,0,0,0,-17,0,0,0,0",
			"name": "Expansion of q^(-1/3) * (eta(q) * eta(q^4))^2 / eta(q^2) in powers of q.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"reference": [
				"S. Ramanujan, On Certain Arithmetical Functions. Collected Papers of Srinivasa Ramanujan, p. 147, Ed. G. H. Hardy et al., AMS Chelsea 2000.",
				"S. Ramanujan, Notebooks, Tata Institute of Fundamental Research, Bombay 1957 Vol. 1, see page 266.  MR0099904 (20 #6340)"
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A114855/b114855.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e",
				"M. Josuat-Verges and J. S. Kim, \u003ca href=\"http://arxiv.org/abs/1101.5608\"\u003eTouchard-Riordan formulas, T-fractions, and Jacobi's triple product identity\u003c/a\u003e, p. 28, equation (61), arXiv:1101.5608, 2011"
			],
			"formula": [
				"Expansion of psi(x^2) * f(-x)^2 = phi(-x) * f(-x^4)^2 in powers of x where phi(), psi(), f() are Ramanujan theta functions.",
				"Euler transform of period 4 sequence [ -2, -1, -2, -3, ...].",
				"a(n) = b(3*n + 1) where b(n) is multiplicative and a(p^e) = 0 if e is odd, a(3^e) = 0^e, a(p^e) = p^(e/2) if p == 1 (mod 3), a(p^e) = (-p)^(e/2) if p == 2 (mod 3).",
				"Given g.f. A(x), then B(q) = q * A(q^3) satisfies 0 = f(B(q), B(q^2), B(q^4)) where f(u, v, w) = (u*w * (u + 2*w) * (u + 4*w))^2 - v^6 * (u^2 + 4*u*w + 8*w^2).",
				"G.f.: Sum_{k} (3*k + 1) * x^(3*k^2 + 2*k) = Product_{k\u003e0} (1 - x^k)^2 * (1 + x^(2*k)) * (1 - x^(4*k)).",
				"a(4*n + 2) = a(4*n + 3) = a(8*n + 4) = 0. a(4*n + 1) = -2 * a(n). 2 * a(n) = A113277(4*n + 1) = - A114855(4*n + 1).",
				"(-1)^n * a(n) = A113277(n). a(8*n) = A080332(n).",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (36 t)) = 6^(3/2) (t/i)^(3/2) f(t) where q = exp(2 Pi i t). - _Michael Somos_, Mar 11 2015"
			],
			"example": [
				"G.f. = 1 - 2*x + 4*x^5 - 5*x^8 + 7*x^16 - 8*x^21 + 10*x^33 - 11*x^40 + ...",
				"G.f. = q - 2*q^4 + 4*q^16 - 5*q^25 + 7*q^49 - 8*q^64 + 10*q^100 - 11*q^121 +..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ x^4]^2 EllipticTheta[ 4, 0, x], {x, 0, n}]; (* _Michael Somos_, Mar 11 2015 *)",
				"a[ n_] := SeriesCoefficient[ QPochhammer[ x]^2 EllipticTheta[ 2, 0, x] / (2 x^(1/4)), {x, 0, n}];  (* _Michael Somos_, Mar 11 2015 *)",
				"a[ n_] := With[{m = Sqrt[3 n + 1]}, If[ IntegerQ[ m], -m (-1)^Mod[ m, 3], 0]]; (* _Michael Somos_, Mar 11 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = if( issquare( 3*n + 1, \u0026n), n * -(-1)^(n%3), 0)};",
				"(PARI) {a(n) = local(A, p, e); if( n\u003c0, 0, n = 3*n + 1; A = factor(n); prod( k=1, matsize(A)[1], if( p=A[k,1], e=A[k,2]; if( e%2, 0, (-(-1)^(p%3) * p)^(e/2) )))) };",
				"(PARI) {a(n) = local(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( (eta(x + A) * eta(x^4 + A))^2 / eta(x^2 + A), n))};",
				"(MAGMA) A := Basis( CuspForms( Gamma1(36), 3/2), 300); A[1] - 2*A[4]; /* _Michael Somos_, Mar 11 2015 */"
			],
			"xref": [
				"Cf. A080332, A113277, A114855."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Jan 01 2006",
			"references": 2,
			"revision": 19,
			"time": "2021-03-12T22:24:43-05:00",
			"created": "2006-01-24T03:00:00-05:00"
		}
	]
}