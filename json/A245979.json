{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A245979",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 245979,
			"data": "5,3,5,5,8,8,5,8,13,8,13,8,13,13,8,13,13,21,21,13,21,21,13,21,13,21,21,13,21,34,21,34,21,34,34,21,34,21,34,34,21,34,34,21,34,21,34,34,21,34,34,55,55,34,55,55,34,55,34,55,55,34,55,55,34,55,34",
			"name": "First differences of A245978.",
			"comment": [
				"It appears that every term is a Fibonacci number, as in A000045.  The sequence A245979 arises from A245977 and A245978 in the following manner.",
				"Suppose, as in A245920, that S = (s(0),s(1),s(2),...) is an infinite sequence such that every finite block of consecutive terms occurs infinitely many times in S.  (It is assumed that A014675 is such a sequence.)  Let B = B(m,k) = (s(m-k),s(m-k+1),...,s(m)) be such a block, where m \u003e= 0 and k \u003e= 0.  Let m(1) be the least i \u003e m such that (s(i-k),s(i-k+1),...,s(i)) = B(m,k), and put B(m(1),k+1) = (s(m(1)-k-1),s(m(1)-k),...,s(m(1))).  Let m(2) be the least i \u003e m(1) such that (s(i-k-1),s(i-k),...,s(i)) = B(m(1),k+1), and put B(m(2),k+2) = (s(m(2)-k-2),s(m(2)-k-1),...,s(m(2))).  Continuing in this manner gives a sequence of blocks B(m(n),k+n).  Let B'(n) = reverse(B(m(n),k+n)), so that for n \u003e= 1, B'(n) comes from B'(n-1) by suffixing a single term; thus the limit of B'(n) is defined; we call it the \"limit-reverse of S with initial block B(m,k)\", denoted by S*(m,k), or simply S*.",
				"...",
				"The sequence (m(i)), where m(0) = 0, is the \"index sequence for limit-reversing S with initial block B(m,k)\" or simply the index sequence for S*, as in A245921 and A245978.  Then A245979 is the difference sequence of A245978, as in the Example."
			],
			"example": [
				"S = the infinite Fibonacci word A014675, with B = (s(2),s(3)); that is, (m,k) = (2,3)",
				"S = (2,1,2,2,1,2,1,2,2,1,2,2,1,2,1,2,2,1,2,...)",
				"B'(0) = (2,2)",
				"B'(1) = (2,2,1)",
				"B'(2) = (2,2,1,2)",
				"B'(3) = (2,2,1,2,1)",
				"B'(4) = (2,2,1,2,1,2)",
				"B'(5) = (2,2,1,2,1,2,2)",
				"S* = (2,2,1,2,1,2,2,1,2,2,1,2,1,2,2,1,2,1,...), with index sequence (3,8,11,16,21,29,...), of which the difference sequence is (5,3,5,5,8,8,5,8,13,8,...)"
			],
			"mathematica": [
				"z = 140; seqPosition2[list_, seqtofind_] := Last[Last[Position[Partition[list, Length[#], 1], Flatten[{___, #, ___}], 1, 2]]] \u0026[seqtofind]; x = GoldenRatio; s = Differences[Table[Floor[n*x], {n, 1, z^2}]]; ans = Join[{s[[p[0] = pos = seqPosition2[s, #] - 1]]}, #] \u0026[{s[[3]], s[[4]]}];  (* Initial block is (s(3),s(4))) [OR (s(2),s(3) if",
				"using offset 0] *) cfs = Table[s = Drop[s, pos - 1]; ans = Join[{s[[p[n] = pos = seqPosition2[s, #] - 1]]}, #] \u0026[ans], {n, z}]; q = Join[{3}, Rest[Accumulate[Join[{1}, Table[p[n], {n, 0, z}]]]]] (* A245978 *)",
				"q1 = Differences[q]  (* A245979 *)"
			],
			"xref": [
				"Cf. A245978, A245977, A245979, A245921, A014675, A000045."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Clark Kimberling_ and _Peter J. C. Moses_, Aug 10 2014",
			"references": 3,
			"revision": 8,
			"time": "2016-09-26T21:42:38-04:00",
			"created": "2014-08-22T10:17:49-04:00"
		}
	]
}