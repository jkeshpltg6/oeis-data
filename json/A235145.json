{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A235145",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 235145,
			"data": "0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,1,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,0,2",
			"name": "a(n) = Number of steps to reach a fixed point or 2-cycle, when iterating A235027 starting from value n.",
			"comment": [
				"Equally, a(n) = minimum number of steps needed to repeat k = A235027(k)(starting from k = n) until A001222(A235027(k)) = A001222(k).",
				"Or in other words, how many times are needed to repeatedly factorize the number, to reverse the bits of each odd prime factor (with A056539) and factorize and bit-reverse the reversed factors again, until the number of prime divisors no more grows, meaning that we have found either a fixed point or entered a cycle of two."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A235145/b235145.txt\"\u003eTable of n, a(n) for n = 0..10001\u003c/a\u003e"
			],
			"formula": [
				"If A235027(A235027(n)) = n, a(n)=0, otherwise 1+a(A235027(n)).",
				"Equally, if A001222(A235027(n)) = A001222(n), a(n)=0, otherwise 1+a(A235027(n)).",
				"a(2n) = a(n), and in general, for composite values a(u * v) = max(a(u),a(v)).",
				"For composite n, a(n) = Max_{p|n} a(p). [The above reduces to this: select the maximal value from all values a(p) computed for primes p dividing n]",
				"For prime p, a(p) = 0 if A056539(p) is also prime (p is 2 or in A074832), otherwise a(p) = 1+a(A056539(p))."
			],
			"example": [
				"19, '10011' in binary, when reversed, yields '11001' = 25, when factored, yields 5 * 5, ('101' * '101' in binary), which divisors stay same when reversed, thus it took one iteration step to reach a point where the number of prime divisors no more grows. Thus a(19)=1."
			],
			"program": [
				"(Scheme, two alternative definitions using memoizing definec-macro from _Antti Karttunen_'s IntSeq-library)",
				"(definec (A235145 n) (cond ((= (A235027 (A235027 n)) n) 0) (else (+ 1 (A235145 (A235027 n))))))",
				"(definec (A235145 n) (cond ((= (A001222 (A235027 n)) (A001222 n)) 0) (else (+ 1 (A235145 (A235027 n))))))",
				"(PARI) revbits(n) = fromdigits(Vecrev(binary(n)), 2);",
				"a235027(n) = {f = factor(n); for (k=1, #f~, if (f[k,1] != 2, f[k,1] = revbits(f[k,1]););); factorback(f);}",
				"find(v, newn) = {for (k=1, #v, if (v[#v -k + 1] == newn, return (k));); return (0);}",
				"a(n) = {ok = 0; v = [n]; while (! ok, newn = a235027(n); ind = find(v, newn); if (ind, ok = 1, v = concat(v, newn); n = newn);); #v - ind;} \\\\ _Michel Marcus_, Aug 06 2017"
			],
			"xref": [
				"A235146 gives the positions of records. Cf. A001222, A056539, A074832, A235027."
			],
			"keyword": "nonn,base",
			"offset": "0,140",
			"author": "_Antti Karttunen_, Jan 03 2014",
			"references": 8,
			"revision": 20,
			"time": "2017-08-06T03:20:04-04:00",
			"created": "2014-01-15T13:51:25-05:00"
		}
	]
}