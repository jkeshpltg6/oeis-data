{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002373",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2373,
			"id": "M2273 N0899",
			"data": "3,3,3,5,3,3,5,3,3,5,3,5,7,3,3,5,7,3,5,3,3,5,3,5,7,3,5,7,3,3,5,7,3,5,3,3,5,7,3,5,3,5,7,3,5,7,19,3,5,3,3,5,3,3,5,3,5,7,13,11,13,19,3,5,3,5,7,3,3,5,7,11,11,3,3,5,7,3,5,7,3,5,3,5,7,3,5,7,3,3,5,7,11,11,3,3,5,3",
			"name": "Smallest prime in decomposition of 2n into sum of two odd primes.",
			"comment": [
				"See A020481 for another version.",
				"a(A208662(n)) = A065091(n) and a(m) \u003c\u003e A065091(n) for m \u003c A208662(n). - _Reinhard Zumkeller_, Feb 29 2012",
				"Records are in A025019, their indices in A051610. - _Ralf Stephan_, Dec 29 2013",
				"Note that these primes do not all belong to a twin prime pair. The first instance is a(110) = 23. - _Michel Marcus_, Aug 17 2020 from a suggestion by _Pierre CAMI_"
			],
			"reference": [
				"D. H. Lehmer, Guide to Tables in the Theory of Numbers. Bulletin No. 105, National Research Council, Washington, DC, 1941, p. 80.",
				"N. Pipping, Neue Tafeln für das Goldbachsche Gesetz nebst Berichtigungen zu den Haussnerschen Tafeln, Finska Vetenskaps-Societeten, Comment. Physico Math. 4 (No. 4, 1927), pp. 1-27.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A002373/b002373.txt\"\u003eTable of n, a(n) for n = 3..10000\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/GoldbachPartition.html\"\u003eGoldbach Partition\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Goldbach%27s_conjecture\"\u003eGoldbach's conjecture\u003c/a\u003e",
				"\u003ca href=\"/index/Go#Goldbach\"\u003eIndex entries for sequences related to Goldbach conjecture\u003c/a\u003e"
			],
			"mathematica": [
				"Table[k = 2; While[q = Prime[k]; ! PrimeQ[2*n - q], k++]; q, {n, 3, 100}] (* _Jean-François Alcover_, Apr 26 2011 *)",
				"Table[Min[Flatten[Select[IntegerPartitions[2*n,{2}],AllTrue[ #,OddQ] \u0026\u0026 AllTrue[#,PrimeQ]\u0026]]],{n,3,100}] (* Requires Mathematica version 10 or later *) (* _Harvey P. Dale_, Aug 31 2020 *)"
			],
			"program": [
				"(Haskell) a002373 n = head $ dropWhile ((== 0) . a010051 . (2*n -)) a065091_list -- _Reinhard Zumkeller_, Feb 29 2012",
				"(PARI) a(n)=forprime(p=3,n,if(isprime(2*n-p), return(p))) \\\\ _Charles R Greathouse IV_, May 18 2015"
			],
			"xref": [
				"Cf. A002372, A002374, A014092, A065091, A010051."
			],
			"keyword": "nonn,nice,easy",
			"offset": "3,1",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Ray Chandler_, Sep 19 2003"
			],
			"references": 31,
			"revision": 52,
			"time": "2020-08-31T19:04:13-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}