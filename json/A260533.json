{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A260533",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 260533,
			"data": "1,1,1,1,2,1,1,3,1,2,1,1,4,3,3,2,2,1,1,5,6,4,1,6,3,1,2,2,1,1,6,10,5,4,12,4,3,3,6,3,2,2,2,1,1,7,15,6,10,20,5,1,12,6,12,4,3,3,6,6,3,1,2,2,2,1",
			"name": "Table of partition coefficients read by rows. The coefficient of a partition p is Product_{j=1..length(p)-1} C(p[j], p[j+1]). Row n lists the coefficients of the partitions of n in the ordering A080577, for n\u003e=1.",
			"comment": [
				"The triangle is a refinement of Pascal's triangle A007318."
			],
			"formula": [
				"Let P = Partitions(n, k) denote the set of partitions p of n with largest part k. Then Sum_{p in P} PartitionCoefficient(p) = binomial(n-1,k-1) for n\u003e=0 and k\u003e=0 (assuming binomial(-1,-1) = 1)."
			],
			"example": [
				"The signed version of the triangle starts:",
				"[1]",
				"[-1, 1]",
				"[1, -2, 1]",
				"[-1, 3, -1, -2, 1]",
				"[1, -4, 3, 3, -2, -2, 1]",
				"[-1, 5, -6, -4, 1, 6, 3, -1, -2, -2, 1]",
				"Adding adjacent coefficients with equal sign reduces the triangle to the matrix inverse of Pascal's triangle (A130595)."
			],
			"maple": [
				"with(combstruct): with(ListTools):",
				"PartitionCoefficients := proc(n) local L, iter, p;",
				"iter := iterstructs(Partition(n)): L := []:",
				"while not finished(iter) do",
				"   p := Reverse(nextstruct(iter)):",
				"   L := [mul(binomial(p[j], p[j+1]), j=1..nops(p)-1), op(L)]",
				"od end:",
				"for n from 1 to 6 do PartitionCoefficients(n) od;"
			],
			"program": [
				"(Sage)",
				"PartitionCoeff = lambda p: mul(binomial(p[j], p[j+1]) for j in range(len(p)-1))",
				"PartitionCoefficients = lambda n: [PartitionCoeff(p) for p in Partitions(n)]",
				"for n in (1..7): print(PartitionCoefficients(n))"
			],
			"xref": [
				"Cf. A007318, A080577, A130595."
			],
			"keyword": "nonn,tabf",
			"offset": "1,5",
			"author": "_Peter Luschny_, Jul 28 2015",
			"references": 0,
			"revision": 20,
			"time": "2020-03-14T10:31:21-04:00",
			"created": "2015-08-01T13:06:33-04:00"
		}
	]
}