{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A323939",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 323939,
			"data": "0,2,5,10,22,28,65,60,172,110,461,182,1314,280,3977,408,12504,570,40021,770,128814,1012,414481,1300,1330052,1638,4253341,2030,13553978,2480,43049433,2992,136317872,3570,430471077,4218,1355976262,4940,4261630689,5740,13366013020,6622",
			"name": "Number of unbranched tri-4-catafusenes under the symmetry point group C_{2v} as a function of the number of hexagons (see Cyvin et al. (1996) for precise definition).",
			"comment": [
				"From _Petros Hadjicostas_, May 25 2019: (Start)",
				"According to the Schoenflies notation used in Chemistry, the point group C_{2v} is the cyclic group C_2 \"with the addition of\" 2 \"mirror planes containing the axis of rotation (vertical planes)\" (see the Wikipedia article about the Schoenflies notation).",
				"According to the Wikipedia article about Molecular symmetry, a simple description of typical geometry of this group is \"angular (H2O) or see-saw (SF4)\" (where SF4 = sulfur tetrafluoride).",
				"According to Cyvin et al. (1996, p. 496), a catafusene is a \"simply connected catacondensed polyhex\". \"It is a system consisting of congruent regular hexagons, where any two hexagons either share exactly one edge or are disjointed\". \"A tri-4-catafusene is generated by contraction of exactly three of its hexagons to tetragons\" (see p. 499).",
				"In general (see p. 496), \"any alpha-q-catafusene (q = 3-5) can be generated from a catafusene on converting alpha of its hexagons to q-gons by contraction\". Here, obviously, alpha = 3 and q = 4.",
				"Here, a(n) is the number of (non-equivalent) unbranched tri-4-catafusenes under the symmetry point group C_{2v}, where n = number of polygons (also known as \"rings\") in the unbranched tri-4-catafusenes. (Hence, n - 3 is the total number of the remaining hexagons since 3 of the original hexagons became tetragons.) In the paper, the letter r is used to denote the number of polygons rather than the letter n (see p. 496).",
				"The numbers (a(n): n \u003e= 3) = (a(r): r \u003e= 3) appear in Table 1 on p. 500 of Cyvin et al. (1996) under the point group C_{2v}.",
				"In the paper, a(n) is denoted by M_r (p. 500) and is called the total number of \"mirror-symmetrical (C_{2v}) systems\" (here r = n). It is decomposed into the sum C_r + L_r, where C_r is the number of \"centrosymmetrical (C_{2h}) systems\" (p. 499) and L_r = (1/2)*binomial(r, 3) - (1/8)*(1-(-1)^r)*(r-1) (see Eq. (15) on p. 499). The numbers L_r count some kind of symmetry described on p. 499 of the paper, but it is unclear what they exactly count.",
				"The point group C_{2h} mentioned above (with the centrosymmetrical systems) is the cyclic group C_2 \"with the addition of a mirror (reflection) plane perpendicular to the axis of rotation (horizontal plane)\" (see the Wikipedia articles about Molecular symmetry and the Schoenflies notation).",
				"Unfortunately, the letter C is used for two different purposes in the paper: to denote various point groups in three dimensions and to denote the number of non-equivalent systems of contracted catafusenes under some kind of symmetry.",
				"The numbers (C_r: r \u003e= 0), which apparently appear in Table 1 (p. 500) under the point group C_{2h} (see above), satisfy C_{2*i} = 0 and C_{2*i+3} = (1/2)*Sum_{1 \u003c= j \u003c= i} j*a_{i+1,j} (see Eq. (17), p. 500), where the double array a_{i,j} appears on p. 498 (Section 5) of the paper. We have a_{i,j} = binomial(i-1, j-1)*2^(i-j) = A038207(i-1, j-1) for i, j \u003e= 1 (see Eq. (32), p. 502 in the paper).",
				"Putting the above information together, we can easily prove the formulas for a(n) that appear below in the FORMULA section.",
				"Note that the notation for the current sequence a(n) should not be confused with the double array a_{i,j} defined on p. 498 of the paper.",
				"(End)"
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A323939/b323939.txt\"\u003eTable of n, a(n) for n = 3..1000\u003c/a\u003e",
				"S. J. Cyvin, B. N. Cyvin and J. Brunvoll, \u003ca href=\"https://dx.doi.org/10.1016/0022-2860(95)09039-8\"\u003eIsomer enumeration of some polygonal systems representing polycyclic conjugated hydrocarbons\u003c/a\u003e, Journal of Molecular Structure 376 (Issues 1-3) (1996), 495-505. See Table 1 (p. 500).",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Fusene.html\"\u003eFusene\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Molecular_symmetry\"\u003eMolecular symmetry\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Point_groups_in_three_dimensions\"\u003ePoint groups in three dimensions\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Polyhex_(mathematics)\"\u003ePolyhex (mathematics)\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Schoenflies_notation\"\u003eSchoenflies notation\u003c/a\u003e.",
				"\u003ca href=\"/index/Rec#order_10\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,7,-16,-14,44,2,-48,15,18,-9)."
			],
			"formula": [
				"From _Petros Hadjicostas_, May 26 2019: (Start)",
				"a(n) = L(n) + C(n) for n \u003e= 3, where L(n) = (1/2)*binomial(n, 3) - (1/8)*(1-(-1)^n)*(n-1) and C(n) = (1/24)*(1 - (-1)^n)*((n + 3)*3^((n-3)/2) - 3*(n-1)).",
				"a(2*m) = binomial(2*m, 3)/2 = A006331(m - 1) for m \u003e= 2, and a(2*m + 1) = binomial(2*m + 1, 3)/2 + (m + 2)*3^m/18 - m for m \u003e= 1.",
				"(End)",
				"From _Colin Barker_, May 28 2019: (Start)",
				"G.f.: x^4*(2 + x - 14*x^2 - x^3 + 22*x^4 - 3*x^5 + 2*x^6 - x^7) / ((1 - x)^4*(1 + x)^2*(1 - 3*x^2)^2).",
				"a(n) = 2*a(n-1) + 7*a(n-2) - 16*a(n-3) - 14*a(n-4) + 44*a(n-5) + 2*a(n-6) - 48*a(n-7) + 15*a(n-8) + 18*a(n-9) - 9*a(n-10) for n\u003e12.",
				"(End)"
			],
			"mathematica": [
				"a[n_] := (1/24) (3((-1)^n - 1) (n - 1) + 2 n (n - 1) (n - 2)+ ((-1)^(n + 1) + 1) (-3n + 3^((n - 3)/2) (n + 3) + 3));",
				"a /@ Range[3, 44] (* _Jean-François Alcover_, Oct 24 2019 *)"
			],
			"program": [
				"(PARI) concat(0, Vec(x^4*(2 + x - 14*x^2 - x^3 + 22*x^4 - 3*x^5 + 2*x^6 - x^7) / ((1 - x)^4*(1 + x)^2*(1 - 3*x^2)^2) + O(x^40))) \\\\ _Colin Barker_, May 30 2019"
			],
			"xref": [
				"Cf. A006331, A038207, A323940, A323941."
			],
			"keyword": "nonn,easy",
			"offset": "3,2",
			"author": "_N. J. A. Sloane_, Feb 09 2019",
			"ext": [
				"Name edited by _Petros Hadjicostas_, May 26 2019",
				"More terms from _Petros Hadjicostas_, May 26 2019"
			],
			"references": 4,
			"revision": 87,
			"time": "2019-10-24T11:34:30-04:00",
			"created": "2019-02-09T22:22:20-05:00"
		}
	]
}