{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A104496",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 104496,
			"data": "1,0,0,-1,5,-19,67,-232,804,-2806,9878,-35072,125512,-452388,1641028,-5986993,21954973,-80884423,299233543,-1111219333,4140813373,-15478839553,58028869153,-218123355523,821908275547,-3104046382351,11747506651599,-44546351423299,169227201341651",
			"name": "Expansion of 2*(2*x+1)/((x+1)*(sqrt(4*x+1)+1)).",
			"comment": [
				"Previous name was: Row sums of triangle A104495. A104495 equals the matrix inverse of triangle A099602, where row n of A099602 equals the inverse Binomial transform of column n of the triangle of trinomial coefficients (A027907).",
				"Absolute row sums of triangle A104495 forms A014137 (partial sums of Catalan numbers)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A104496/b104496.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e"
			],
			"formula": [
				"G.f.: A(x) = (1 + 2*x)/(1+x)/(1+x - x^2*Catalan(-x)^2), where Catalan(x)=(1-(1-4*x)^(1/2))/(2*x) (cf. A000108).",
				"a(n) ~ (-1)^n * 2^(2*n+1) / (3*sqrt(Pi)*n^(3/2)). - _Vaclav Kotesovec_, Mar 06 2014",
				"D-finite with recurrence: (n+1)*a(n) +(7*n-3)*a(n-1) +2*(7*n-12)*a(n-2) +4*(2*n-5)*a(n-3)=0. - _R. J. Mathar_, Jan 23 2020"
			],
			"maple": [
				"gf := (2*(2*x+1))/((x+1)*(sqrt(4*x+1)+1)): ser := series(gf,x,30):",
				"seq(coeff(ser,x,n),n=0..28); # _Peter Luschny_, Apr 25 2016"
			],
			"mathematica": [
				"CoefficientList[Series[(1+2*x)/(1+x)/(1+x - (1-(1+4*x)^(1/2))^2/4), {x, 0, 20}], x] (* _Vaclav Kotesovec_, Mar 06 2014 *)"
			],
			"program": [
				"(PARI) {a(n)=local(X=x+x*O(x^n));polcoeff( (1+2*X)/(1+X)/(1+X-(1-(1+4*X)^(1/2))^2/4),n,x)}",
				"(Python)",
				"from itertools import accumulate",
				"def A104496_list(size):",
				"    if size \u003c 1: return []",
				"    L, accu = [1], [1]",
				"    for n in range(size-1):",
				"        accu = list(accumulate(accu + [-accu[0]]))",
				"        L.append(-(-1)**n*accu[-1])",
				"    return L",
				"print(A104496_list(29)) # _Peter Luschny_, Apr 25 2016"
			],
			"xref": [
				"Cf. A104495, A099602, A027907, A000108."
			],
			"keyword": "sign,easy",
			"offset": "0,5",
			"author": "_Paul D. Hanna_, Mar 11 2005",
			"ext": [
				"New name using the g.f. of the author by _Peter Luschny_, Apr 25 2016"
			],
			"references": 3,
			"revision": 20,
			"time": "2020-01-30T21:29:15-05:00",
			"created": "2005-04-09T03:00:00-04:00"
		}
	]
}