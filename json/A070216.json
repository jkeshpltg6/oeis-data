{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A070216",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 70216,
			"data": "2,5,8,10,13,18,17,20,25,32,26,29,34,41,50,37,40,45,52,61,72,50,53,58,65,74,85,98,65,68,73,80,89,100,113,128,82,85,90,97,106,117,130,145,162,101,104,109,116,125,136,149,164,181,200,122,125,130,137,146,157",
			"name": "Triangle T(n, k) = n^2 + k^2, 1 \u003c= k \u003c= n, read by rows.",
			"comment": [
				"The formula yields squares of hypotenuses of right triangles having integer side lengths (A000404), but with duplicates (cf. A024508) and not in increasing order. - _M. F. Hasler_, Apr 05 2016"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A070216/b070216.txt\"\u003eRows n = 1..120 of triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"a(n, k) = n^2 + k^2, 1 \u003c= k \u003c= n.",
				"T(n,k) = (A215630(n,k) + A215631(n,k)) / 2, 1 \u003c= k \u003c=n. - _Reinhard Zumkeller_, Nov 11 2012",
				"T(n,k) = A002024(n,k)^2 + A002260(n,k)^2. - _David Rabahy_, Mar 24 2016"
			],
			"example": [
				"a(3,2)=13 because 3^2+2^2=13.",
				"Triangle begins:",
				"2;",
				"5, 8;",
				"10, 13, 18;",
				"17, 20, 25, 32;",
				"26, 29, 34, 41, 50;",
				"37, 40, 45, 52, 61, 72;",
				"50, 53, 58, 65, 74, 85, 98;",
				"65, 68, 73, 80, 89, 100, 113, 128;",
				"82, 85, 90, 97, 106, 117, 130, 145, 162;",
				"101, 104, 109, 116, 125, 136, 149, 164, 181, 200; ...",
				"- _Vincenzo Librandi_, Apr 30 2014"
			],
			"mathematica": [
				"t[n_,k_]:=n^2 + k^2; Table[t[n, k], {n, 11}, {k, n}]//Flatten (* _Vincenzo Librandi_, Apr 30 2014 *)"
			],
			"program": [
				"(Haskell)",
				"a070216 n k = a070216_tabl !! (n-1) !! (k-1)",
				"a070216_row n = a070216_tabl !! (n-1)",
				"a070216_tabl = zipWith (zipWith (\\u v -\u003e (u + v) `div` 2))",
				"                       a215630_tabl a215631_tabl",
				"-- _Reinhard Zumkeller_, Nov 11 2012",
				"(MAGMA) [n^2+k^2: k in [1..n], n in [1..15]]; // _Vincenzo Librandi_, Apr 30 2014",
				"(PARI) T(n, k) = n^2+k^2;",
				"for (n=1, 10, for(k=1, n, print1(T(n, k), \", \"))) \\\\ _Altug Alkan_, Mar 24 2016"
			],
			"xref": [
				"Not a permutation of sequence A000404 (which has no duplicates).",
				"Cf. A002522 (left edge), A001105 (right edge), A219054 (row sums)."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "1,1",
			"author": "Charles Northup (cnorthup(AT)esc6.net), May 07 2002",
			"ext": [
				"More terms from Larry Reeves (larryr(AT)acm.org), Sep 25 2002",
				"Edited and corrected by _M. F. Hasler_, Apr 05 2016"
			],
			"references": 10,
			"revision": 39,
			"time": "2016-04-05T18:46:20-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}