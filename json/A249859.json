{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A249859",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 249859,
			"data": "3,0,5,6,21,8,45,30,77,24,117,70,165,48,221,126,285,80,357,198,437,120,525,286,621,168,725,390,837,224,957,510,1085,288,1221,646,1365,360,1517,798,1677,440,1845,966,2021,528,2205,1150,2397,624,2597,1350,2805",
			"name": "Least common multiple of n + 2 and n - 2.",
			"comment": [
				"The recurrence for the general case lcm(n+k, n-k) is a(n) = 3*a(n-2*k) - 3*a(n-4*k) + a(n-6*k) for n \u003e 6*k."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A249859/b249859.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_12\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,0,0,3,0,0,0,-3,0,0,0,1)."
			],
			"formula": [
				"a(n) = lcm(n - 2, n + 2).",
				"a(n) = 3*a(n-4) - 3*a(n-8) + a(n-12) for n \u003e 12.",
				"G.f.: x*(-6*x^12 - 2*x^11 - 3*x^10 + 23*x^8 + 12*x^7 + 30*x^6 + 8*x^5 + 12*x^4 + 6*x^3 + 5*x^2 + 3) / (-x^12 + 3*x^8 - 3*x^4 + 1).",
				"From _Peter Bala_, Feb 15 2019; (Start)",
				"For n \u003e= 2, a(n) = (n^2 - 4)/b(n), where b(n), n \u003e= 1, is the periodic sequence [1, 4, 1, 2, 1, 4, 1, 2, ...] of period 4. a(n) is thus a quasi-polynomial in n.",
				"For n \u003e= 3, a(n) = (n + 2)*A060819(n-2). (End)"
			],
			"example": [
				"a(8) = 30 because lcm(8 + 2, 8 - 2) = lcm(6, 10) = 30."
			],
			"maple": [
				"A249859:=n-\u003e ilcm(n+2,n-2): seq(A249859(n), n=1..100); # _Wesley Ivan Hurt_, Jul 09 2017"
			],
			"mathematica": [
				"Table[LCM[n - 2, n + 2], {n, 50}] (* _Alonso del Arte_, Nov 07 2014 *)",
				"CoefficientList[Series[(-6 x^12 - 2 x^11 - 3 x^10 + 23 x^8 + 12 x^7 + 30 x^6 + 8 x^5 + 12 x^4 + 6 x^3 + 5 x^2 + 3) / (-x^12 + 3 x^8 - 3 x^4 + 1), {x, 0, 40}], x] (* _Vincenzo Librandi_, Nov 10 2014 *)",
				"LinearRecurrence[{0,0,0,3,0,0,0,-3,0,0,0,1},{3,0,5,6,21,8,45,30,77,24,117,70,165},60] (* _Harvey P. Dale_, Jul 11 2017 *)"
			],
			"program": [
				"(PARI) a(n) = lcm(n+2, n-2)",
				"(PARI) Vec(x*(-6*x^12 -2*x^11 -3*x^10 +23*x^8 +12*x^7 +30*x^6 +8*x^5 +12*x^4 +6*x^3 +5*x^2 +3) / (-x^12 +3*x^8 -3*x^4 +1) + O(x^100))",
				"(MAGMA) [Lcm(n-2, n+2): n in [1..60]]; // _Vincenzo Librandi_, Nov 10 2014"
			],
			"xref": [
				"Cf. A066830 (k=1), A249860 (k=3), A060819."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_Colin Barker_, Nov 07 2014",
			"references": 4,
			"revision": 30,
			"time": "2019-02-16T07:24:41-05:00",
			"created": "2014-11-14T06:00:36-05:00"
		}
	]
}