{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A092082",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 92082,
			"data": "1,7,1,91,21,1,1729,511,42,1,43225,15015,1645,70,1,1339975,523705,69300,4025,105,1,49579075,21240765,3226405,230300,8330,147,1,2131900225,984172735,166428990,13820205,621810,15386,196,1,104463111025",
			"name": "Triangle of numbers related to triangle A092083; generalization of Stirling numbers of second kind A008277, Lah-numbers A008297, ...",
			"comment": [
				"a(n,m) := S2(7; n,m) is the seventh triangle of numbers in the sequence S2(k;n,m), k=1..6: A008277 (unsigned Stirling 2nd kind), A008297 (unsigned Lah), A035342, A035469, A049029, A049385, respectively. a(n,1)=A008542(n), n\u003e=1.",
				"a(n,m) enumerates unordered n-vertex m-forests composed of m plane increasing 7-ary trees. Proof based on the a(n,m) recurrence. See also the F. Bergeron et al. reference, especially Table 1, first row and Example 1 for the e.g.f. for m=1. - _Wolfdieter Lang_, Sep 14 2007",
				"Also the Bell transform of A008542(n+1). For the definition of the Bell transform see A264428. - _Peter Luschny_, Jan 26 2016"
			],
			"link": [
				"F. Bergeron, Ph. Flajolet and B. Salvy, \u003ca href=\"http://dx.doi.org/10.1007/3-540-55251-0_2\"\u003eVarieties of Increasing Trees\u003c/a\u003e, in Lecture Notes in Computer Science vol. 581, (1992), pp. 24-48.",
				"P. Blasiak, K. A. Penson and A. I. Solomon, \u003ca href=\"http://www.arXiv.org/abs/quant-ph/0402027\"\u003eThe general boson normal ordering problem.\u003c/a\u003e, arXiv:quant-phys/0402027, 2004.",
				"P. Blasiak, K. A. Penson and A. I. Solomon, \u003ca href=\"http://dx.doi.org/10.1016/S0375-9601(03)00194-4\"\u003eThe general boson normal ordering problem\u003c/a\u003e, Phys. Lett. A 309 (2003) 198-205.",
				"M. Janjic, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL12/Janjic/janjic22.html\"\u003eSome classes of numbers and derivatives\u003c/a\u003e, JIS 12 (2009) 09.8.3",
				"W. Lang, \u003ca href=\"/A092082/a092082.txt\"\u003eFirst 10 rows\u003c/a\u003e.",
				"W. Lang, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL3/LANG/lang.html\"\u003eOn generalizations of Stirling number triangles\u003c/a\u003e, J. Integer Seqs., Vol. 3 (2000), #00.2.4.",
				"Shi-Mei Ma, \u003ca href=\"http://arxiv.org/abs/1208.3104\"\u003eSome combinatorial sequences associated with context-free grammars\u003c/a\u003e, arXiv:1208.3104v2 [math.CO], 2012. - From N. J. A. Sloane, Aug 21 2012"
			],
			"formula": [
				"a(n, m) = sum(|A051151(n, j)|*S2(j, m), j=m..n) (matrix product), with S2(j, m) := A008277(j, m) (Stirling2 triangle). Priv. comm. with _Wolfdieter Lang_ by E. Neuwirth, Feb 15 2001; see also the 2001 Neuwirth reference. See the general comment on products of Jabotinsky matrices given under A035342.",
				"a(n, m) = n!*A092083(n, m)/(m!*6^(n-m)); a(n+1, m) = (6*n+m)*a(n, m)+ a(n, m-1), n \u003e= m \u003e= 1; a(n, m) := 0, n\u003cm; a(n, 0) := 0, a(1, 1)=1.",
				"E.g.f. for m-th column: ((-1+(1-6*x)^(-1/6))^m)/m!."
			],
			"example": [
				"{1}; {7,1}; {91,21,1}; {1729,511,42,1}; ..."
			],
			"maple": [
				"# The function BellMatrix is defined in A264428.",
				"# Adds (1, 0, 0, 0, ..) as column 0.",
				"BellMatrix(n -\u003e mul(6*k+1, k=0..n), 9); # _Peter Luschny_, Jan 26 2016"
			],
			"mathematica": [
				"mmax = 9; a[n_, m_] := n!*Coefficient[Series[((-1 + (1 - 6*x)^(-1/6))^m)/m!, {x, 0, mmax}], x^n];",
				"Flatten[Table[a[n, m], {n, 1, mmax}, {m, 1, n}]][[1 ;; 37]] (* _Jean-François Alcover_, Jun 22 2011, after e.g.f. *)",
				"rows = 9;",
				"t = Table[Product[6k+1, {k, 0, n}], {n, 0, rows}];",
				"T[n_, k_] := BellY[n, k, t];",
				"Table[T[n, k], {n, 1, rows}, {k, 1, n}] // Flatten (* _Jean-François Alcover_, Jun 22 2018, after _Peter Luschny_ *)"
			],
			"xref": [
				"Cf. A092084 (row sums), A092085 (alternating row sums)."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "1,2",
			"author": "_Wolfdieter Lang_, Mar 19 2004",
			"references": 30,
			"revision": 37,
			"time": "2019-08-28T15:41:32-04:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}