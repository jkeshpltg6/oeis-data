{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A192001",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 192001,
			"data": "1,2,1,3,3,1,4,6,5,1,5,10,14,9,1,6,15,30,36,17,1,7,21,55,100,98,33,1,8,28,91,225,354,276,65,1,9,36,140,441,979,1300,794,129,1,10,45,204,784,2275,4425,4890,2316,257,1",
			"name": "Triangle with sums of nonnegative integer powers of positive first n integers in the columns.",
			"comment": [
				"This is the Abramowitz-Stegun table on p. 813, call it s(m,n), with an extra column n=0 with values n added, and read by antidiagonals. a(n,m) = s(n-m,m), n+1\u003e= m\u003e=0.",
				"O.g.f. for column no. m\u003e=0:",
				"(x^(m+1)/(1-x)^(m+2))*E(m;x) with the row polynomials E(m;x)=sum(A173018(m,p)*x^p,p=0..m) of the Eulerian number triangle (proof via the Worpitzky identity). See the Graham et al. reference p. 253-8 for Eulerian numbers, and the Worpitzky identity (6.37) on p. 255.",
				"E.g.f. for diagonals (starting with k=0 for the main diagonal): g(k,x)=exp(x)*(exp((k+1)*x)-1)/(1-exp(x)).",
				"  Compare with (7.77) on p. 353 of the Graham et al. reference.",
				"O.g.f. for diagonals (starting with k=0 for the main diagonal): G(k,z)=(Psi(1/z+1)-Psi(1/z-k-1))/z - 1.",
				"  with the digamma function Psi(z):=(log(GAMMA(z)))'.",
				"  Compare with Graham et al., p. 352, eq.(7.76), where H_z=Psi(z+1)+gamma, with the Euler-Mascheroni constant gamma.",
				"The diagonal sequences are, for k=0..9: A000012, A000051, A001550-A001557.",
				"The negative k-diagonal, -a(k+m+1,m), yields the Sheffer z-sequence Shz(k+1;m) for the Sheffer arrays |S1|(k+1) defined in a comment to A094646.",
				"See also A196837 with a W. Lang link, where the o.g.f.s for the diagonals, numbered with k\u003e=1, are given as G(k,x)=sum((k-m)*S1(k+1,k+1-m)*x^m,m=0..k)/ product(1-j*x,j=1..k), with S1 the Stirling numbers of the first kind, A048994. - _Wolfdieter Lang_, Nov 01 2011"
			],
			"reference": [
				"M. Abramowitz and I. A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, 1964. Tenth printing, Wiley, 2002 (also electronically available, see the link), p. 813.",
				"Mohammad K. Azarian, Problem 1218, Pi Mu Epsilon Journal, Vol. 13, No. 2, Spring 2010, p. 116.  Solution published in Vol. 13, No. 3, Fall 2010, pp. 183-185.",
				"Ronald L. Graham, Donald E. Knuth and Oren Patashnik, Concrete Mathematics, Addison-Wesley, 1991."
			],
			"link": [
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [scanned copy], p. 813."
			],
			"formula": [
				"a(n,m) = s(n-m,m), n-1 \u003e= m \u003e= 0, n \u003e= 1, else 0, with s(n,m):=sum(k^m,k=1..n).",
				"O.g.f. column no. m: see a comment above.",
				"O.g.f.s and e.g.f.s for diagonals k\u003e=0: see a comment above.",
				"Recurrence known by Ibn al-Haytham (see a comment and link under A000537, and comments by _Bruno Berselli_ under the A-numbers of the first column sequences):",
				"  a(n,m) = (n-m)*a(n-1,m-1) - sum(a(l,m-1),l=m..n-2), n \u003e=1, n-1 \u003e= m \u003e= 1. a(n,0) = n. - _Wolfdieter Lang_, Jan 12 2013"
			],
			"example": [
				"The triangle a(n,m) begins:",
				"n\\m    0   1   2    3    4     5     6     7    8   9  10 ...",
				"n=1:   1",
				"n=2:   2   1",
				"n=3:   3   3   1",
				"n=4:   4   6   5    1",
				"n=5:   5  10  14    9    1",
				"n=6:   6  15  30   36   17     1",
				"n=7:   7  21  55  100   98    33     1",
				"n=8:   8  28  91  225  354   276    65     1",
				"n=9:   9  36 140  441  979  1300   794   129    1",
				"n=10: 10  45 204  784 2275  4425  4890  2316  257   1",
				"n=11: 11  55 285 1296 4676 12201 20515 18700 6818 513   1",
				"...  Reformatted and extended by _Wolfdieter Lang_, Jan 12 2013",
				"a(4,2)= 5 = s(2,2) = 1^2 + 2^2.",
				"Recurrence: 55 = a(7,2) = (7-2)*a(6,1) - (a(2,1) + a(3,1) + a(4,1) + a(5,1)) = 5*15 - (1 + 3 + 6 + 10) = 55. - _Wolfdieter Lang_, Jan 12 2013",
				"The first column, m=0 holds the integers 1,2,3,..., equal to the sums of 0th powers of the n first integers. The second column is 1, 1+2, 1+2+3,... = A000217. The third column are the sums of squares, 1^2, 1^2+2^2, 1^2+2^2+3^3,... = A000330, etc. - _M. F. Hasler_, Jan 13 2013"
			],
			"mathematica": [
				"Flatten[ Table[ HarmonicNumber[-m + n, -m], {n, 1, 10}, {m, 0, n - 1}]] (* _Jean-François Alcover_, Sep 26 2011 *)"
			],
			"program": [
				"(PARI) A192001(n,m) = sum(k=1,n-m,k^m) \\\\ - _M. F. Hasler_, Jan 13 2013"
			],
			"xref": [
				"A103438 (omitting the first column of zeros, reversed)."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "1,2",
			"author": "_Wolfdieter Lang_, Jun 25 2011",
			"references": 2,
			"revision": 38,
			"time": "2018-05-08T15:11:56-04:00",
			"created": "2011-06-26T08:39:15-04:00"
		}
	]
}