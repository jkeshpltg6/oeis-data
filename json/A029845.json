{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A029845",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 29845,
			"data": "1,8,20,0,-62,0,216,0,-641,0,1636,0,-3778,0,8248,0,-17277,0,34664,0,-66878,0,125312,0,-229252,0,409676,0,-716420,0,1230328,0,-2079227,0,3460416,0,-5677816,0,9198424,0,-14729608,0,23328520,0,-36567242,0,56774712,0",
			"name": "Expansion of 16/lambda(z) in powers of nome q = exp(Pi*i*z).",
			"comment": [
				"Ramanujan theta functions: f(q) := Product_{k\u003e=1} (1-(-q)^k) (see A121373), phi(q) := theta_3(q) := Sum_{k=-oo..oo} q^(k^2) (A000122), psi(q) := Sum_{k=0..oo} q^(k*(k+1)/2) (A010054), chi(q) := Product_{k\u003e=0} (1+q^(2k+1)) (A000700)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A029845/b029845.txt\"\u003eTable of n, a(n) for n = -1..10000\u003c/a\u003e (terms -1..1000 from Alois P. Heinz)",
				"J. H. Conway and S. P. Norton, \u003ca href=\"http://blms.oxfordjournals.org/content/11/3/308.extract\"\u003eMonstrous Moonshine\u003c/a\u003e, Bull. Lond. Math. Soc. 11 (1979) 308-339.",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/EllipticLambdaFunction.html\"\u003eElliptic Lambda Function\u003c/a\u003e"
			],
			"formula": [
				"Expansion of (eta(q^2)^3/(eta(q)*eta(q^4)^2))^8 in powers of q. - _Michael Somos_, Nov 14 2006",
				"Expansion of (chi(q)*chi(-q^2))^8/q in powers of q where chi() is a Ramanujan theta function.",
				"Euler transform of period 4 sequence [ 8, -16, 8, 0, ...]. - _Michael Somos_, Nov 14 2006",
				"G.f. A(x) satisfies: 0=f(A(x), A(x^2)) where f(u, v) = 256 - v*(32-16*u+u^2) + v^2. - _Michael Somos_, Nov 14 2006",
				"G.f.: 1/q*(Product_{k\u003e0} (1+q^(2k-1))/(1+q^(2k)))^8."
			],
			"example": [
				"1/q + 8 + 20*q - 62*q^3 + 216*q^5 - 641*q^7 + 1636*q^9 - 3778*q^11 + ..."
			],
			"mathematica": [
				"QP = QPochhammer; s = 16*q + (QP[q]/QP[q^4])^8 + O[q]^50; CoefficientList[s, q] (* _Jean-François Alcover_, Nov 27 2015, adapted from PARI *)"
			],
			"program": [
				"(PARI) {a(n)=local(A); if(n\u003c-1, 0, n++; A=x*O(x^n); polcoeff( 16*x+(eta(x+A)/eta(x^4+A))^8, n))} /* _Michael Somos_, Nov 14 2006 */"
			],
			"xref": [
				"Cf. A007248(n) = a(2n-1). A124972(n) = a(n) except at n=0.",
				"Cf. A000122, A000700, A010054, A121373."
			],
			"keyword": "sign,easy",
			"offset": "-1,2",
			"author": "_N. J. A. Sloane_",
			"references": 10,
			"revision": 35,
			"time": "2021-03-12T22:24:41-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}