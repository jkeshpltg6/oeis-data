{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A272610",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 272610,
			"data": "5,9,4,6,5,5,18,4,5,10,10,18,4,10,15,10,27,4,15,15,10,36,4,15,20,15,36,4,20,25,15,45,4,25,25,20,45,4,25,35,15,54,4,35,25,20,72,4,25,40,25,54,4,40,40,20,72,4,40,35,25,81,4,35,50,25,81,4,50,40,25,117",
			"name": "a(1)=5, a(2)=9, a(3)=4, a(4)=6; thereafter a(n) = a(n-a(n-1)) + a(n-a(n-2)).",
			"comment": [
				"In calculating terms of this sequence, use the convention that a(n)=0 for n\u003c=0.",
				"Similar to Hofstadter's Q-sequence A005185 but with different starting values.",
				"This sequence exists as long as A272611 and A272612 exist.",
				"No other term of this sequence changes if a(4) is replaced by a number greater than 6.",
				"If a(2) is replaced by a number N greater than 9, then every other term of the form a(5n+2) is replaced by a(5n+2)*N/9."
			],
			"link": [
				"Nathan Fox, \u003ca href=\"/A272610/b272610.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"a(1)=5, a(2)=9, a(3)=4, a(4)=6; thereafter a(5n)=5*A272611(n), a(5n+1)=5*A272612(n), a(5n+2)=9*A272613(n), a(5n+3)=4, a(5n+4)=5*A272611(n)."
			],
			"maple": [
				"A272610:=proc(n) option remember:",
				"    if n \u003c= 0 then",
				"        return 0:",
				"    elif n = 1 then",
				"        return 5:",
				"    elif n = 2 then",
				"        return 9:",
				"    elif n = 3 then",
				"        return 4:",
				"    elif n = 4 then",
				"        return 6:",
				"    else",
				"        return A272610(n-A272610(n-1))+A272610(n-A272610(n-2)):",
				"    fi:",
				"end:"
			],
			"program": [
				"(Python)",
				"from functools import cache",
				"@cache",
				"def a(n):",
				"    if n \u003c 0: return 0",
				"    if n \u003c 5: return [0, 5, 9, 4, 6][n]",
				"    return a(n - a(n-1)) + a(n - a(n-2))",
				"print([a(n) for n in range(1, 73)]) # _Michael S. Branicky_, Sep 20 2021"
			],
			"xref": [
				"Cf. A005185, A272611, A272612, A272613."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Nathan Fox_, May 03 2016",
			"references": 9,
			"revision": 12,
			"time": "2021-09-20T09:04:07-04:00",
			"created": "2016-05-03T17:03:59-04:00"
		}
	]
}