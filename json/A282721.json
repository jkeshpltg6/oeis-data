{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A282721",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 282721,
			"data": "1,13,32,137,306,314,555,876,1400,1416,1742,2450,3099,3788,4816,5430,6351,7344,8393,9546,12858,13373,15265,17277,16311,18403,19521,22344,21805,23590,25495,26805,30767,30863,31570,35980,40678,43946,45640,49124,50055,52776,58418,66210,71521,71665,83666,81628",
			"name": "Let p = n-th prime == 3 mod 8; a(n) = sum of quadratic residues mod p that are \u003c p/2.",
			"link": [
				"Robert Israel, \u003ca href=\"/A282721/b282721.txt\"\u003eTable of n, a(n) for n = 1..4000\u003c/a\u003e",
				"Christian Aebi and Grant Cairns, \u003ca href=\"http://arxiv.org/abs/1512.00896\"\u003eSums of Quadratic residues and nonresidues\u003c/a\u003e, arXiv:1512.00896 [math.NT], 2015."
			],
			"maple": [
				"with(numtheory):",
				"Ql:=[]; Qu:=[]; Q:=[]; Nl:=[]; Nu:=[]; N:=[]; Th:=[];",
				"for i1 from 1 to 300 do",
				"p:=ithprime(i1);",
				"if (p mod 8) = 3 then",
				"ql:=0; qu:=0; q:=0; nl:=0; nu:=0; n:=0;",
				"for j from 1 to p-1 do",
				"if legendre(j,p)=1 then",
				"  q:=q+j;",
				"  if j\u003cp/2 then ql:=ql+j; else qu:=qu+j; fi;",
				"else",
				"  n:=n+j;",
				"  if j\u003cp/2 then nl:=nl+j; else nu:=nu+j; fi;",
				"fi;",
				"                    od;",
				"Ql:=[op(Ql),ql];",
				"Qu:=[op(Qu),qu];",
				"Q:=[op(Q),q];",
				"Nl:=[op(Nl),nl];",
				"Nu:=[op(Nu),nu];",
				"N:=[op(N),n];",
				"Th:=[op(Th),q+ql];",
				"fi;",
				"od:",
				"Ql; Qu; Q; Nl; Nu; N; Th; # A282721 - A282727",
				"# Alternative",
				"f:= proc(p) local q,r,t,j;",
				"  r:= (p-1)/2; t:= 0;",
				"  for j from 1 to r do",
				"    q:= j^2 mod p;",
				"    if q \u003c= r then t:= t+q fi;",
				"od:",
				"t",
				"end proc:",
				"map(f, select(isprime, [seq(i,i=3..10000,8)])); # _Robert Israel_, Mar 27 2017"
			],
			"mathematica": [
				"s[p_] := Total[Select[Range[Floor[p/2]], JacobiSymbol[#, p] == 1\u0026]];",
				"s /@ Select[Range[3, 2000, 8], PrimeQ] (* _Jean-François Alcover_, Nov 17 2017 *)"
			],
			"program": [
				"(Python)",
				"from sympy import isprime",
				"def a(p):",
				"    r=(p - 1)//2",
				"    t=0",
				"    for j in range(1, r + 1):",
				"        q=(j**2)%p",
				"        if q\u003c=r:t+=q",
				"    return t",
				"print([a(p) for p in range(3, 2001, 8) if isprime(p)]) # _Indranil Ghosh_, Mar 27 2017, translated from Maple code"
			],
			"xref": [
				"Cf. A282035-A282043 and A282722-A282727."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, Feb 20 2017",
			"references": 12,
			"revision": 32,
			"time": "2021-03-24T09:57:46-04:00",
			"created": "2017-02-20T21:59:25-05:00"
		}
	]
}