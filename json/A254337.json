{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A254337",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 254337,
			"data": "0,1,8,6,10,14,12,4,20,16,24,18,22,28,26,34,30,32,36,40,42,46,38,44,52,48,54,50,58,56,62,64,60,66,68,72,70,74,80,76,78,86,82,84,90,92,94,88,98,96,104,100,102,108,110,112,114,106,116,122,118,120,124,126,130,132,134,128,138,136,142,140,144,146,148,150,154,152,156,158",
			"name": "Lexicographically earliest sequence of distinct numbers such that no sum of consecutive terms is prime.",
			"comment": [
				"In other words, no sum a(i)+a(i+1)+a(i+2)+...+a(n) may be prime. In particular, the sequence may not contain any primes.",
				"I conjecture that the sequence contains all even numbers \u003e 2 and no odd number beyond 1. If so, we must simply ensure that the sum a(1)+...+a(n) is not prime, which is always possible for one of the three consecutive even numbers {2n, 2n+2, 2n+4}. As a consequence, it would follow that a(n) ~ 2n.",
				"Is there even a proof that the smallest odd composite number, 9, does not appear?",
				"The variant A254341 has the additional restriction of alternating parity, which avoids excluding the odd numbers.",
				"The least odd composite number a'(n+1) that could occur as the next term after a(n) and such that sum(a(i),i=k...n)+a'(n+1) is composite for all k \u003c= n is (for n = 0, 1, 2,...): 9, 9, 25, 21, 39, 25, 69, 65, 45, 119, 95, 77, 55, 27, 595, 561, 531, 865, 1519, 1479, 1437, 1391, 1353, 1309, 1257, 1209, 1155, 1105, 1047, 2317, 2255, 2191, 3565, 5719, 13067, 12995, 12925, 12851, 12771, 12695, 12617, 12531, 12449, 12365, 12275, ... The growth of this sequence shows how it is increasingly unlikely that an odd number could occur, since the next possible even term is only about 2n."
			],
			"link": [
				"Robert G. Wilson v and M. F. Hasler, \u003ca href=\"/A254337/b254337.txt\"\u003eTable of n, a(n) for n = 0..5000\u003c/a\u003e (terms 0..999 from M. F. Hasler)"
			],
			"formula": [
				"It appears that a(n) ~ 2n."
			],
			"example": [
				"To explain the beginning of the sequence, observe that starting with the smallest possible terms 0, 1 does not appear to lead to a contradiction (and in fact never does), so we start there.",
				"The next composite would be 4 but 1+4=5 is prime, as is 1+6, but 1+8=9 is not, so we take a(2) = 8 to be the next term.",
				"4 is impossible for a(3) since 1+8+4=13 is prime, but neither 1+8+6=15 nor 8+6 is prime, so a(3)=6."
			],
			"mathematica": [
				"f[lst_List] := Block[{k = 1}, While[ PrimeQ@ k || MemberQ[lst, k] || Union@ PrimeQ@ Accumulate@ Reverse@ Join[lst, {k}] != {False}, k++]; Append[lst, k]]; Nest[f, {0}, 70] (* _Robert G. Wilson v_, Jan 31 2015 *)"
			],
			"program": [
				"(PARI) a=[];u=0; for(i=1,99, a=concat(a,0); until( ! isprime(s) || ! a[i]++, while( isprime(a[i]) || bittest(u,a[i]), a[i]++); s=a[k=i]; while( k\u003e1 \u0026\u0026 ! isprime( s+=a[k--]),)); u+=2^a[i]; print1(a[i]\",\"))"
			],
			"xref": [
				"Cf. A254341, A153136, A254211, A002808, A253073, A253074, A054408, A084834.",
				"Cf. A025044 (no pairwise sum is prime), A025043 (no pairwise difference is prime)."
			],
			"keyword": "nonn,nice",
			"offset": "0,3",
			"author": "_M. F. Hasler_, Jan 28 2015",
			"references": 13,
			"revision": 94,
			"time": "2020-01-05T22:22:20-05:00",
			"created": "2015-01-29T03:59:40-05:00"
		}
	]
}