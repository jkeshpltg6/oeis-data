{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A124927",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 124927,
			"data": "1,1,2,1,4,2,1,6,6,2,1,8,12,8,2,1,10,20,20,10,2,1,12,30,40,30,12,2,1,14,42,70,70,42,14,2,1,16,56,112,140,112,56,16,2,1,18,72,168,252,252,168,72,18,2,1,20,90,240,420,504,420,240,90,20,2,1,22,110,330,660,924,924,660,330,110,22,2",
			"name": "Triangle read by rows: T(n,0)=1, T(n,k)=2*binomial(n,k) if k\u003e0 (0\u003c=k\u003c=n).",
			"comment": [
				"Pascal triangle with all entries doubled except for the first entry in each row. A028326 with first column replaced by 1's. Row sums are 2^(n+1)-1.",
				"From _Paul Barry_, Sep 19 2008: (Start)",
				"Reversal of A129994. Diagonal sums are A001595. T(2n,n) is A100320.",
				"Binomial transform of matrix with 1,2,2,2,... on main diagonal, zero elsewhere. (End)",
				"This sequence is jointly generated with A210042 as an array of coefficients of polynomials v(n,x):  initially, u(1,x)=v(1,x)=1; for n\u003e1, u(n,x)=u(n-1,x)+v(n-1,x) +1 and v(n,x)=x*u(n-1,x)+x*v(n-1,x).  See the Mathematica section. - _Clark Kimberling_, Mar 09 2012",
				"Subtriangle of the triangle given by (1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, ...) DELTA (0, 2, -1, 0, 0, 0, 0, 0, 0, 0, ...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, Mar 25 2012"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A124927/b124927.txt\"\u003eRows n=0..150 of triangle, flattened\u003c/a\u003e",
				"\u003ca href=\"/index/Pas#Pascal\"\u003eIndex entries for triangles and arrays related to Pascal's triangle\u003c/a\u003e"
			],
			"formula": [
				"T(n,0) = 1; for n\u003e0: T(n,n) = 2, T(n,k) = T(n-1,k) + T(n-1,n-k), 1\u003ck\u003cn. - _Reinhard Zumkeller_, Mar 04 2012",
				"T(n,k) = 2*T(n-1,k) + T(n-1,k-1) - T(n-2,k) - T(n-2,k-1), T(0,0) = T(1,0) = 1, T(1,1) = 2, T(n,k) = 0 if k\u003c0 or if k\u003en. - _Philippe Deléham_, Mar 25 2012",
				"G.f.: (1-x+x*y)/((-1+x)*(x*y+x-1)). - _R. J. Mathar_, Aug 11 2015"
			],
			"example": [
				"Triangle starts:",
				"  1;",
				"  1,  2;",
				"  1,  4,  2;",
				"  1,  6,  6,  2;",
				"  1,  8, 12,  8,  2;",
				"  1, 10, 20, 20, 10, 2;",
				"(1, 0, 0, 1, 0, 0, ...) DELTA (0, 2, -1, 0, 0, ...) begins:",
				"  1;",
				"  1,  0;",
				"  1,  2,  0;",
				"  1,  4,  2,  0;",
				"  1,  6,  6,  2,  0;",
				"  1,  8, 12,  8,  2, 0;",
				"  1, 10, 20, 20, 10, 2, 0. - _Philippe Deléham_, Mar 25 2012"
			],
			"maple": [
				"T:=proc(n,k) if k=0 then 1 else 2*binomial(n,k) fi end: for n from 0 to 12 do seq(T(n,k),k=0..n) od; # yields sequence in triangular form"
			],
			"mathematica": [
				"(* First program *)",
				"u[1, x_] := 1; v[1, x_] := 1; z = 16;",
				"u[n_, x_] := u[n - 1, x] + v[n - 1, x] + 1;",
				"v[n_, x_] := x*u[n - 1, x] + x*v[n - 1, x] + 1;",
				"Table[Expand[u[n, x]], {n, 1, z/2}]",
				"Table[Expand[v[n, x]], {n, 1, z/2}]",
				"cu = Table[CoefficientList[u[n, x], x], {n, 1, z}];",
				"TableForm[cu]",
				"Flatten[%]    (* A210042 *)",
				"Table[Expand[v[n, x]], {n, 1, z}]",
				"cv = Table[CoefficientList[v[n, x], x], {n, 1, z}];",
				"TableForm[cv]",
				"Flatten[%]    (* A124927 *) (* _Clark Kimberling_, Mar 17 2012 *)",
				"(* Second program *)",
				"Table[If[k==0, 1, 2*Binomial[n, k]], {n,0,12}, {k,0,n}]//Flatten (* _G. C. Greubel_, Jul 10 2019 *)"
			],
			"program": [
				"(Haskell)",
				"a124927 n k = a124927_tabl !! n !! k",
				"a124927_row n = a124927_tabl !! n",
				"a124927_tabl = iterate",
				"   (\\row -\u003e zipWith (+) ([0] ++ reverse row) (row ++ [1])) [1]",
				"-- _Reinhard Zumkeller_, Mar 04 2012",
				"(PARI) T(n,k) = if(k==0,1, 2*binomial(n,k)); \\\\ _G. C. Greubel_, Jul 10 2019",
				"(MAGMA) [k eq 0 select 1 else 2*Binomial(n,k): k in [0..n], n in [0..12]]; // _G. C. Greubel_, Jul 10 2019",
				"(Sage)",
				"def T(n, k):",
				"    if (k==0): return 1",
				"    else: return 2*binomial(n,k)",
				"[[T(n, k) for k in (0..n)] for n in (0..12)] # _G. C. Greubel_, Jul 10 2019"
			],
			"xref": [
				"Cf. A000225.",
				"Cf. A074909."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "0,3",
			"author": "_Gary W. Adamson_, Nov 12 2006",
			"ext": [
				"Edited by _N. J. A. Sloane_, Nov 24 2006"
			],
			"references": 8,
			"revision": 35,
			"time": "2019-07-11T10:09:06-04:00",
			"created": "2006-12-06T03:00:00-05:00"
		}
	]
}