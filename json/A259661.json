{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A259661",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 259661,
			"data": "1,11,110,1100,11001,110011,1100110,11001100,110011001,1100110011,11001100110,110011001100,1100110011001,11001100110011,110011001100110,1100110011001100,11001100110011001,110011001100110011,1100110011001100110,11001100110011001100",
			"name": "Binary representation of the middle column of the \"Rule 54\" elementary cellular automaton starting with a single ON cell.",
			"link": [
				"Robert Price, \u003ca href=\"/A259661/b259661.txt\"\u003eTable of n, a(n) for n = 0..999\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Rule54.html\"\u003eRule 54\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ElementaryCellularAutomaton.html\"\u003eElementary Cellular Automaton\u003c/a\u003e",
				"Stephen Wolfram, \u003ca href=\"http://wolframscience.com/\"\u003eA New Kind of Science\u003c/a\u003e, Wolfram Media, 2002; p. 55.",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_Elementary_Cellular_Automata\"\u003eIndex to Elementary Cellular Automata\u003c/a\u003e"
			],
			"formula": [
				"Conjectures from _Colin Barker_, Dec 08 2015 and Apr 17 2019: (Start)",
				"a(n) = 11*a(n-1) - 11*a(n-2) + 11*a(n-3) - 10*a(n-4) for n\u003e3.",
				"G.f.: 1 / ((1-x)*(1-10*x)*(1+x^2)).",
				"(End)",
				"The above conjectures are correct. Also a(n) = floor(10^(n+3)/909). - _Karl V. Keller, Jr._, Sep 26 2021"
			],
			"example": [
				"From _Michael De Vlieger_, Dec 09 2015: (Start)",
				"First 8 rows at left, ignoring \"0\" outside of range of 1's, the center column values in parentheses, and at right the value of center column cells up to that row:",
				"                (1)                 -\u003e 1",
				"              1 (1) 1               -\u003e 11",
				"            1 0 (0) 0 1             -\u003e 110",
				"          1 1 1 (0) 1 1 1           -\u003e 1100",
				"        1 0 0 0 (1) 0 0 0 1         -\u003e 11001",
				"      1 1 1 0 1 (1) 1 0 1 1 1       -\u003e 110011",
				"    1 0 0 0 1 0 (0) 0 1 0 0 0 1     -\u003e 1100110",
				"  1 1 1 0 1 1 1 (0) 1 1 1 0 1 1 1   -\u003e 11001100",
				"1 0 0 0 1 0 0 0 (1) 0 0 0 1 0 0 0 1 -\u003e 110011001",
				"(End)"
			],
			"mathematica": [
				"lim = 20; Take[Last@ Take[#, Ceiling[Length[#]/2]] \u0026 /@ CellularAutomaton[54, {{1}, 0}, lim], #] \u0026 /@ Range@ lim (* _Michael De Vlieger_, Dec 09 2015 *)"
			],
			"program": [
				"(Python) print([10**(n+3)//909 for n in range(50)]) # _Karl V. Keller, Jr._, Sep 26 2021"
			],
			"xref": [
				"Cf. A071030, A118109, A118108, A133872, A077854."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Robert Price_, Dec 05 2015",
			"references": 2,
			"revision": 49,
			"time": "2021-09-27T20:56:45-04:00",
			"created": "2015-12-05T22:07:00-05:00"
		}
	]
}