{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A322809",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 322809,
			"data": "1,2,3,4,3,5,3,6,6,7,3,8,3,9,9,10,3,11,3,12,12,13,3,14,14,15,15,16,3,17,3,18,18,19,19,20,3,21,21,22,3,23,3,24,24,25,3,26,26,27,27,28,3,29,29,30,30,31,3,32,3,33,33,34,34,35,3,36,36,37,3,38,3,39,39,40,40,41,3,42,42,43,3,44,44,45,45,46,3,47,47,48,48,49,49,50,3,51,51,52,3,53,3,54,54",
			"name": "Lexicographically earliest such sequence a that a(i) = a(j) =\u003e f(i) = f(j) for all i, j, where f(n) = -1 if n is an odd prime, and f(n) = floor(n/2) for all other numbers.",
			"comment": [
				"This sequence is a restricted growth sequence transform of a function f which is defined as f(n) = A004526(n), unless n is an odd prime, in which case f(n) = -1, which is a constant not in range of A004526. See the Crossrefs section for a list of similar sequences.",
				"For all i, j:",
				"  A305801(i) = A305801(j) =\u003e a(i) = a(j),",
				"  a(i) = a(j) =\u003e A039636(i) = A039636(j).",
				"For all i, j: a(i) = a(j) \u003c=\u003e A323161(i+1) = A323161(j+1).",
				"The shifted version of this filter, A323161, has a remarkable ability to find many sequences related to primes and prime chains. - _Antti Karttunen_, Jan 06 2019"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A322809/b322809.txt\"\u003eTable of n, a(n) for n = 1..65537\u003c/a\u003e",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A323161(n+1) - 1."
			],
			"program": [
				"(PARI)",
				"up_to = 65537;",
				"rgs_transform(invec) = { my(om = Map(), outvec = vector(length(invec)), u=1); for(i=1, length(invec), if(mapisdefined(om,invec[i]), my(pp = mapget(om, invec[i])); outvec[i] = outvec[pp] , mapput(om,invec[i],i); outvec[i] = u; u++ )); outvec; };",
				"A322809aux(n) = if((n\u003e2)\u0026\u0026isprime(n),-1,(n\u003e\u003e1));",
				"v322809 = rgs_transform(vector(up_to,n,A322809aux(n)));",
				"A322809(n) = v322809[n];"
			],
			"xref": [
				"Cf. A004526, A039636, A305801, A323161.",
				"A list of few similarly constructed sequences follows, where each sequence is an rgs-transform of such function f, for which the value of f(n) is the n-th term of the sequence whose A-number follows after a parenthesis, unless n is of the form ..., in which case f(n) is given a constant value outside of the range of that sequence:",
				"  A322809 (A004526, unless an odd prime) [This sequence],",
				"  A322589 (A007913, unless an odd prime),",
				"  A322591 (A007947, unless an odd prime),",
				"  A322805 (A252463, unless an odd prime),",
				"  A323082 (A300840, unless an odd prime),",
				"  A322822 (A300840, unless n \u003e 2 and a Fermi-Dirac prime, A050376),",
				"  A322988 (A322990, unless a prime power \u003e 2),",
				"  A323078 (A097246, unless an odd prime),",
				"  A322808 (A097246, unless a squarefree number \u003e 2),",
				"  A322816 (A048675, unless an odd prime),",
				"  A322807 (A285330, unless an odd prime),",
				"  A322814 (A286621, unless an odd prime),",
				"  A322824 (A242424, unless an odd prime),",
				"  A322973 (A006370, unless an odd prime),",
				"  A322974 (A049820, unless n \u003e 1 and n is in A046642),",
				"  A323079 (A060681, unless an odd prime),",
				"  A322587 (A295887, unless an odd prime),",
				"  A322588 (A291751, unless an odd prime),",
				"  A322592 (A289625, unless an odd prime),",
				"  A323369 (A323368, unless an odd prime),",
				"  A323371 (A295886, unless an odd prime),",
				"  A323374 (A323373, unless an odd prime),",
				"  A323401 (A323372, unless an odd prime),",
				"  A323405 (A323404, unless an odd prime)."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Antti Karttunen_, Dec 26 2018",
			"references": 6,
			"revision": 45,
			"time": "2019-02-09T23:27:42-05:00",
			"created": "2018-12-26T21:36:20-05:00"
		}
	]
}