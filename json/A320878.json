{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A320878",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 320878,
			"data": "286330897,286330943,388098901,955201943,1776186851,1854778853,2559495863,2647782901,3517793911,3628857863,3866728909,3974453911,4167637819,4269837799,5083007887,5362197829,5642510933,6034811933,8180784851,8214319903",
			"name": "Primes such that iteration of A062028 (n + its digit sum) yields 6 primes in a row.",
			"comment": [
				"In contrast to A048523, ..., A048527, this definition uses \"at least\" for the number of successive primes. This allows easier computation of subsequences of terms which yield even more primes in a row.",
				"One can nonetheless compute the terms of this sequence by considering possible pre-images under A062028 of terms of A048527. This gives the terms which yield exactly 6 primes in a row (i.e., A320878 \\ A320879), and one has to take the union with further iterates of this procedure (which successively yields A320879 \\ A320880, etc)."
			],
			"link": [
				"Lars Blomberg, \u003ca href=\"/A320878/b320878.txt\"\u003eTable of n, a(n) for n = 1..7626\u003c/a\u003e (Terms \u003c 10^14. The first 200 from M. F. Hasler)",
				"Carlos Rivera, \u003ca href=\"http://www.primepuzzles.net/puzzles/puzz_163.htm\"\u003ePuzzle 163. P+SOD(P)\u003c/a\u003e"
			],
			"formula": [
				"Numbers n in A048519 for which A062028(n) is in A048527, form the subset A320878 \\ A320879."
			],
			"program": [
				"(PARI) is_A320878(n,p=n)={for(i=1,6, isprime(p=A062028(p))||return);isprime(n)}",
				"forprime(p=286e6,,is_A320878(p)\u0026\u0026 print1(p\",\"))",
				"/* much faster, using the precomputed array A048527, as follows: */",
				"PP(n)=select(p-\u003ep+sumdigits(p)==n,primes([n-9*#digits(n),n-2])) \\\\ Returns list of prime predecessors for A062028. (PP(n) nonempty \u003c=\u003e n in A320881.)",
				"A320878=[]; my(S=A048527); while(#S=Set(concat(apply(PP,S))), A320878=setunion(A320878,S)) \\\\ Yields 211 terms from A048527[1..3000]"
			],
			"xref": [
				"Cf. A062028 (n + digit sum of n), A047791 (A062028(n) is prime), A048519 (primes among these).",
				"Cf. A048523 .. A048527, A320879.",
				"a(1) = A090009(7) = start of first chain of 7 primes under iteration of A062028.",
				"Cf. A320881, A048520.",
				"Cf. A230093 (number of m s.th. m + (sum of digits of m) = n) and references there."
			],
			"keyword": "nonn,base",
			"offset": "1,1",
			"author": "_Zak Seidov_ and _M. F. Hasler_, Nov 08 2018",
			"references": 7,
			"revision": 20,
			"time": "2019-02-10T05:18:59-05:00",
			"created": "2018-11-09T08:00:24-05:00"
		}
	]
}