{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003295",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3295,
			"id": "M3872",
			"data": "1,-5,17,46,116,252,533,1034,1961,3540,6253,10654,17897,29284,47265,74868,117158,180608,275562,415300,620210,916860,1344251,1953974,2819664,4038300,5746031,8122072,11413112,15943576,22153909,30620666",
			"name": "McKay-Thompson series of class 11A for the Monster group with a(0) = -5.",
			"comment": [
				"Coefficients of a modular function denoted by B(tau) in Atkin (1967)."
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A003295/b003295.txt\"\u003eTable of n, a(n) for n = -1..1000\u003c/a\u003e",
				"A. O. L. Atkin, \u003ca href=\"https://doi.org/10.1017/S0017089500000045\"\u003eProof of a conjecture of Ramanujan\u003c/a\u003e, Glasgow Math. J., 8 (1967), 14-32.",
				"A. O. L. Atkin, \u003ca href=\"/A003295/a003295.pdf\"\u003eProof of a conjecture of Ramanujan\u003c/a\u003e, Glasgow Math. J., 8 (1967), 14-32. (Annotated scanned copy, poor quality)",
				"J. H. Conway and S. P. Norton, \u003ca href=\"http://blms.oxfordjournals.org/content/11/3/308.extract\"\u003eMonstrous Moonshine\u003c/a\u003e, Bull. Lond. Math. Soc. 11 (1979) 308-339.",
				"N. D. Elkies, \u003ca href=\"http://www.math.harvard.edu/~elkies/modular.pdf\"\u003eElliptic and modular curves over finite fields and related computational issues\u003c/a\u003e, in AMS/IP Studies in Advanced Math., 7 (1998), 21-76, see p. 42.",
				"D. Ford, J. McKay and S. P. Norton, \u003ca href=\"http://dx.doi.org/10.1080/00927879408825127\"\u003eMore on replicable functions\u003c/a\u003e, Commun. Algebra 22, No. 13, 5175-5193 (1994).",
				"J. McKay and H. Strauss, \u003ca href=\"http://dx.doi.org/10.1080/00927879008823911\"\u003eThe q-series of monstrous moonshine and the decomposition of the head characters\u003c/a\u003e, Comm. Algebra 18 (1990), no. 1, 253-278.",
				"\u003ca href=\"/index/Mat#McKay_Thompson\"\u003eIndex entries for McKay-Thompson series for Monster simple group\u003c/a\u003e"
			],
			"formula": [
				"From _Michael Somos_, Aug 31 2012: (Start)",
				"Expansion of -11 + (1 + 3*F)^2 * (1/F + 1 + 3*F) where F = eta(q^3) * eta(q^33) / (eta(q) * eta(q^11)) (= g.f. of A128663) in powers of q.",
				"G.f. is Fourier series of a level 11 modular function. f(-1 / (11 t)) = f(t) where q = exp(2 Pi i t).",
				"A000521(n) = a(n) + 11 * a(11*n) unless n=0. [Atkin (1967) p. 22]",
				"a(n) = A003295(n) = A058205(n) = A128525(n) = A134784(n) unless n=0. (End)",
				"a(n) ~ exp(4*Pi*sqrt(n/11)) / (sqrt(2)*11^(1/4)*n^(3/4)). - _Vaclav Kotesovec_, Sep 07 2017"
			],
			"example": [
				"G.f. = 1/q - 5 + 17*q + 46*q^2 + 116*q^3 + 252*q^4 + 533*q^5 + 1034*q^6 + ..."
			],
			"mathematica": [
				"QP = QPochhammer; F = q*QP[q^3]*(QP[q^33]/(QP[q]*QP[q^11])); s = q*(-11 + (1 + 3*F)^2*(1/F + 1 + 3*F)) + O[q]^40; CoefficientList[s, q] (* _Jean-François Alcover_, Nov 13 2015, from 1st formula *)"
			],
			"program": [
				"(PARI) q='q+O('q^50); F =q*eta(q^3)*eta(q^33)/(eta(q)*eta(q^11));  Vec(-11 + (1 + 3*F)^2*(3*F + 1 + 1/F)) \\\\ _G. C. Greubel_, May 10 2018"
			],
			"xref": [
				"Cf. A000521, A007240, A014708, A007241, A007267, A045478, etc.",
				"Cf. A003295, A058205, A128525, A128663, A134784."
			],
			"keyword": "sign,nice,easy",
			"offset": "-1,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from Kok Seng Chua (chuaks(AT)ihpc.nus.edu.sg), Jul 05 2000"
			],
			"references": 5,
			"revision": 59,
			"time": "2018-05-12T18:50:28-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}