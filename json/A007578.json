{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007578",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7578,
			"id": "M1219",
			"data": "1,1,2,4,10,26,76,232,763,2611,9415,35135,136335,544623,2242618,9463508,40917803,180620411,813405580,3728248990,17377551032,82232982872,394742985974,1919885633178,9453682648281,47086636037601,237071351741426,1205689994416252",
			"name": "Number of Young tableaux of height \u003c= 7.",
			"comment": [
				"Also the number of n-length words w over 7-ary alphabet {a1,a2,...,a7} such that for every prefix z of w we have #(z,a1) \u003e= #(z,a2) \u003e= ... \u003e= #(z,a7), where #(z,x) counts the letters x in word z. - _Alois P. Heinz_, May 30 2012"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A007578/b007578.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Andrei Asinowski, Axel Bacher, Cyril Banderier, Bernhard Gittenberger, \u003ca href=\"https://lipn.univ-paris13.fr/~banderier/Papers/patterns2019.pdf\"\u003eAnalytic combinatorics of lattice paths with forbidden patterns, the vectorial kernel method, and generating functions for pushdown automata\u003c/a\u003e, Laboratoire d'Informatique de Paris Nord (LIPN 2019).",
				"F. Bergeron, L. Favreau and D. Krob, \u003ca href=\"/A007578/a007578.pdf\"\u003eConjectures on the enumeration of tableaux of bounded height\u003c/a\u003e, Preprint. (Annotated scanned copy)",
				"F. Bergeron, L. Favreau and D. Krob, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(94)00148-C\"\u003eConjectures on the enumeration of tableaux of bounded height\u003c/a\u003e, Discrete Math, vol. 139, no. 1-3 (1995), 463-468.",
				"Juan B. Gil, Peter R. W. McNamara, Jordan O. Tirrell, Michael D. Weiner, \u003ca href=\"https://arxiv.org/abs/1708.00513\"\u003eFrom Dyck paths to standard Young tableaux\u003c/a\u003e, arXiv:1708.00513 [math.CO], 2017.",
				"\u003ca href=\"/index/Y#Young\"\u003eIndex entries for sequences related to Young tableaux.\u003c/a\u003e"
			],
			"formula": [
				"a(n) ~ 45/32 * 7^(n+21/2)/(Pi^(3/2)*n^(21/2)). - _Vaclav Kotesovec_, Sep 11 2013"
			],
			"maple": [
				"h:= proc(l) local n; n:=nops(l); add(i, i=l)! /mul(mul(1+l[i]-j+",
				"      add(`if`(l[k]\u003e=j, 1, 0), k=i+1..n), j=1..l[i]), i=1..n)",
				"    end:",
				"g:= proc(n, i, l) option remember;",
				"      `if`(n=0, h(l), `if`(i=1, h([l[], 1$n]), `if`(i\u003c1, 0,",
				"        g(n, i-1, l) +`if`(i\u003en, 0, g(n-i, i, [l[], i])))))",
				"    end:",
				"a:= n-\u003e g(n, 7, []):",
				"seq(a(n), n=0..30); # _Alois P. Heinz_, Apr 10 2012",
				"# second Maple program",
				"a:= proc(n) option remember;",
				"      `if`(n\u003c4, [1, 1, 2, 4][n+1],",
				"       ((4*n^3+78*n^2+424*n+495)*a(n-1)",
				"       +(n-1)*(34*n^2+280*n+305)*a(n-2)",
				"       -2*(n-1)*(n-2)*(38*n+145)*a(n-3)",
				"       -105*(n-1)*(n-2)*(n-3)*a(n-4)) /",
				"       ((n+6)*(n+10)*(n+12)))",
				"    end:",
				"seq(a(n), n=0..30);  # _Alois P. Heinz_, Oct 12 2012"
			],
			"mathematica": [
				"RecurrenceTable[{105 (-3+n) (-2+n) (-1+n) a[-4+n]+2 (-2+n) (-1+n) (145+38 n) a[-3+n]-(-1+n) (305+280 n+34 n^2) a[-2+n]+(-495-424 n-78 n^2-4 n^3) a[-1+n]+(6+n) (10+n) (12+n) a[n]==0,a[1]==1,a[2]==2,a[3]==4,a[4]==10}, a, {n, 20}] (* _Vaclav Kotesovec_, Sep 11 2013 *)"
			],
			"xref": [
				"Column k=7 of A182172. - _Alois P. Heinz_, May 30 2012"
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Simon Plouffe_",
			"ext": [
				"More terms from _Alois P. Heinz_, Apr 10 2012"
			],
			"references": 15,
			"revision": 51,
			"time": "2020-02-25T02:09:38-05:00",
			"created": "1994-09-19T03:00:00-04:00"
		}
	]
}