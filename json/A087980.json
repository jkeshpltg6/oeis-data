{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A087980",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 87980,
			"data": "1,2,4,8,12,16,24,32,48,64,72,96,128,144,192,256,288,360,384,432,512,576,720,768,864,1024,1152,1440,1536,1728,2048,2160,2304,2592,2880,3072,3456,4096,4320,4608,5184,5760,6144,6912,8192,8640,9216,10368,10800",
			"name": "Numbers with strictly decreasing prime exponents.",
			"comment": [
				"This representation provides a natural ordering between strictly decreasing sequences of natural numbers. Let f and g be such sequences with f(1) \u003e f(2) \u003e ... \u003e f(m) and g(1) \u003e g(2) \u003e ... \u003e g(n). Define f \u003c g iff p^f \u003c p^g, where p^f is short for Product(i=1..m) p_i^f(i) and p^g is defined likewise as Product(i=1..n) p_i^g(i).",
				"Note that \"strictly decreasing sequences of natural numbers\" is another way to say \"partitions into distinct parts\".",
				"Also products of primorial numbers p_1#^k_1 * p_2#^k_2 * ... * p_n#^k_n where all k_i \u003e 0.",
				"A124010(a(n),k+1) \u003c A124010(a(n),k), 1 \u003c= k \u003c A001221(a(n)). - _Reinhard Zumkeller_, Apr 13 2015"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A087980/b087980.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e"
			],
			"formula": [
				"The numbers of the form Product(i=1..n) p_i^k_i where p_i = A000040(i) is the i-th prime and k_1 \u003e k_2 \u003e ... \u003e k_n are positive natural numbers.",
				"Compute x = 2^k_1 * 3^k_2 * 5^k_3 * 7^k_4 * 11^k_5 for k_1 \u003e ... \u003e k_5 allowing k_i = 0 for i \u003e 1 and k_i = k_(i+1) in that case. Discard all x \u003e 174636000 = 2^5*3^4*5^3*7^2*11 and enumerate those below. For more members take higher primes into account."
			],
			"example": [
				"The sequence starts with a(1)=1, a(2)=2, a(3)=4 and a(4)=8. The next term is a(5)=12 = 2^2*3^1 = p_1^k_1 * p_2^k_2 with k_1=2 \u003e k_2=1."
			],
			"mathematica": [
				"selQ[k_] := Module[{n = k, e = IntegerExponent[k, 2], t}, n /= 2^e; For[p = 3, True, p = NextPrime[p], t = IntegerExponent[n, p]; If[t == 0, Return[n == 1]]; If[t \u003e= e, Return[False]]; e = t; n /= p^e]];",
				"Select[Range[12000], selQ] (* _Jean-François Alcover_, Mar 27 2020, after first PARI program *)"
			],
			"program": [
				"(Haskell)",
				"import Data.List (isPrefixOf)",
				"a087980 n = a087980_list !! (n-1)",
				"a087980_list = 1 : filter f [2..] where",
				"   f x = isPrefixOf ps a000040_list \u0026\u0026 all (\u003c 0) (zipWith (-) (tail es) es)",
				"         where ps = a027748_row x; es = a124010_row x",
				"-- _Reinhard Zumkeller_, Apr 13 2015",
				"(PARI) is(n)=my(e=valuation(n,2),t); n\u003e\u003e=e; forprime(p=3,, t=valuation(n,p); if(t==0, return(n==1)); if(t\u003e=e, return(0)); e=t; n/=p^e) \\\\ _Charles R Greathouse IV_, Jun 25 2017",
				"(PARI) list(lim)=my(v=[],u=powers(2,logint(lim\\=1,2)),w,p=2,t); forprime(q=3,, w=List(); for(i=1,#u, t=u[i]; for(e=1,valuation(u[i],p)-1, t*=q; if(t\u003elim, break); listput(w,t))); v=concat(v,Vec(u)); if(#w==0, break); u=w; p=q); Set(v) \\\\ _Charles R Greathouse IV_, Jun 25 2017"
			],
			"xref": [
				"Cf. A000040, A025487, A002110, A000009, A027748, A124010, A133808, A133813."
			],
			"keyword": "easy,nice,nonn",
			"offset": "1,2",
			"author": "_Rainer Rosenthal_, Oct 27 2003",
			"ext": [
				"Edited by _Franklin T. Adams-Watters_, Apr 25 2006",
				"Offset change to 1 by _T. D. Noe_, May 24 2010"
			],
			"references": 16,
			"revision": 18,
			"time": "2020-03-27T06:56:48-04:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}