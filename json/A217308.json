{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A217308",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 217308,
			"data": "1,2,11,19,83,107,157,669,751,1259,4957,6879,6011,14303,47071,48093,65371,188143,327515,440287,384751,1029883,2604783,2948955,3602299,6946651,20304733,23846747,23937003,23723867,57278299,167689071,175479547,191496027,233824091",
			"name": "Minimal natural number (in decimal representation) with n prime substrings in base-8 representation (substrings with leading zeros are considered to be nonprime).",
			"comment": [
				"The sequence is well-defined in that for each n the set of numbers with n prime substrings is not empty. Proof: Define m(0):=1, m(1):=2 and m(n+1):=8*m(n)+2 for n\u003e0. This results in m(n)=2*sum_{j=0..n-1} 8^j = 2*(8^n - 1)/7 or m(n)=1, 2, 22, 222, 2222, 22222, …, (in base-8) for n=0,1,2,3,…. Evidently, for n\u003e0 m(n) has n 2’s and these are the only prime substrings in base-8 representation. This is why every substring of m(n) with more than one digit is a product of two integers \u003e 1 (by definition) and can therefore not be prime number.",
				"No term is divisible by 8."
			],
			"link": [
				"Hieronymus Fischer, \u003ca href=\"/A217308/b217308.txt\"\u003eTable of n, a(n) for n = 0..45\u003c/a\u003e"
			],
			"formula": [
				"a(n) \u003e 8^floor(sqrt(8*n-7)-1)/2), for n\u003e0.",
				"a(n) \u003c= 2*(8^n - 1)/7, n\u003e0.",
				"a(n+1) \u003c= 8*a(n)+2."
			],
			"example": [
				"a(1) = 2 = 2_8, since 2 is the least number with 1 prime substring in base-8 representation.",
				"a(2) = 11 = 13_8, since 11 is the least number with 2 prime substrings in base-8 representation (3_8 and 13_8).",
				"a(3) = 19 = 23_8, since 19 is the least number with 3 prime substrings in base-8 representation (2_8, 3_8, and 23_8).",
				"a(4) = 83 = 123_8, since 83 is the least number with 4 prime substrings in base-8 representation (2_8, 3_8, 23_8=19, and 123_8=83).",
				"a(8) = 751 = 1357_8, since 751 is the least number with 8 prime substrings in base-8 representation (3_8, 5_8, 7_8, 13_8=11, 35_8=29, 57_8=47, 357_8=239, and 1357_8=751)."
			],
			"xref": [
				"Cf. A019546, A035232, A039996, A046034, A069489, A085823, A211681, A211682, A211684, A211685.",
				"Cf. A035244, A079397, A213300-A213321.",
				"Cf. A217302-A217309."
			],
			"keyword": "nonn,base",
			"offset": "0,2",
			"author": "_Hieronymus Fischer_, Nov 22 2012",
			"references": 3,
			"revision": 12,
			"time": "2014-10-24T04:15:26-04:00",
			"created": "2012-12-05T17:13:54-05:00"
		}
	]
}