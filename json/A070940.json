{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A070940",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 70940,
			"data": "1,1,2,1,3,2,3,1,4,3,4,2,4,3,4,1,5,4,5,3,5,4,5,2,5,4,5,3,5,4,5,1,6,5,6,4,6,5,6,3,6,5,6,4,6,5,6,2,6,5,6,4,6,5,6,3,6,5,6,4,6,5,6,1,7,6,7,5,7,6,7,4,7,6,7,5,7,6,7,3,7,6,7,5,7,6,7,4,7,6,7,5,7",
			"name": "Number of digits that must be counted from left to right to reach the last 1 in the binary representation of n.",
			"comment": [
				"Length of longest carry sequence when adding numbers \u003c= n to n in binary representation: a(n)=T(n, A080079(n)) and T(n,k) \u003c= a(n) for 1 \u003c= k \u003c= n, with T defined as in A080080. - _Reinhard Zumkeller_, Jan 26 2003",
				"a(n+1) is the number of distinct values of gcd(2^n, binomial(n,j)) (or, equivalently, A007814(binomial(n,j))) arising for j=0,...,n-1. Proof using Kummer's Theorem given by Marc Schwartz. - _Labos Elemer_, Apr 23 2003",
				"E.g., n=10: 10th row of Pascal's triangle = {1,10,45,120,210,252,210,120,45,10,1}, largest powers of 2 dividing binomial coefficients is: {1,2,1,8,2,4,2,8,1,2,1}; including distinct powers of 2, thus a(10)=4. If m=-1+2^k, i.e., m=0,1,3,7,15,31,.. then a(m)=1. This corresponds to \"odd rows\" of Pascal triangle. - _Labos Elemer_",
				"Smallest x \u003e 0 for which a(x)=n equals 2^n. - _Labos Elemer_",
				"a(n) \u003c= A070939(n), a(n) = A070939(n) iff n is odd, where A070939(n) = floor(log_2(n)) + 1. - _Reinhard Zumkeller_, Jan 26 2003",
				"Can be regarded as a table with row n having 2^(n-1) columns, with odd columns repeating the previous row, and even columns containing the row number. - _Franklin T. Adams-Watters_, Nov 08 2011",
				"It appears that a(n) is the greatest number in a periodicity equivalence class defined at A269570; e.g., the 5 classes for n = 35 are (1, 1, 2, 2, 6), (1, 1, 1, 1, 4, 2, 2), (3), (1, 3), (1, 2); in these the greatest number is 6, so that a(35) = 6. - _Clark Kimberling_, Mar 01 2016",
				"Number of binary digits of the largest odd factor of n. - _Andres Cicuttin_, May 18 2017"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A070940/b070940.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e"
			],
			"formula": [
				"a(n) = floor(log_2(n)) - A007814(n) = A070939(n) - A007814(n).",
				"a(n) = f(n, 1), f(n, k) = if n=1 then k else f(floor(n/2), k+(if k\u003e1 then 1 else n mod 2)). - _Reinhard Zumkeller_, Feb 01 2003",
				"G.f.: sum(k\u003e=0, t/(1-t^2) * [1 + sum(l\u003e=1, t^2^l)], t=x^2^k). - _Ralf Stephan_, Mar 15 2004",
				"a(n) = A070939(A000265(n)). - _Andres Cicuttin_, May 19 2017",
				"a(1) = 1 and for m \u003e= 0, 0 \u003c= k \u003c 2^m, a(2^(m+1)+2*k) =  a(2^m+k), a(2^(m+1)+2*k+1) = m+2. - _Yosu Yurramendi_, Aug 08 2019"
			],
			"example": [
				"a(10)=3 is the number of digits that must be counted from left to right to reach the last 1 in 1010, the binary representation of 10.",
				"The table starts:",
				"1",
				"1 2",
				"1 3 2 3",
				"1 4 3 4 2 4 3 4"
			],
			"maple": [
				"A070940 := n -\u003e if n mod 2 = 0 then A070939(n)-A001511(n/2) else A070939(n); fi;"
			],
			"mathematica": [
				"Table[Length[Union[Table[GCD[2^n, Binomial[n, j]], {j, 0, n}]]], {n, 0, 256}]",
				"f[n_] := Position[ IntegerDigits[n, 2], 1][[ -1, 1]]; Table[ f[n], {n, 105}] (* _Robert G. Wilson v_, Dec 01 2004 *)",
				"(* By exploiting the \"positional\" regularity of the sequence *)",
				"b = {}; a = {1, 1};",
				"Do[a = Riffle[a, j];",
				"  b = AppendTo[b, a[[1 ;; Floor[Length[a]/2]]]] // Flatten, {j, 1, 10}];",
				"Print[b[[1 ;; 100]]] (* _Andres Cicuttin_, May 18 2017 *)",
				"(* By following the alternative definition \"Number of binary digits of the largest integer odd factor of n\" *)",
				"c = Table[IntegerDigits[n/(2^IntegerExponent[n, 2]), 2] // Length , {n,",
				"    2^10 - 1}];",
				"Print[c[[1 ;; 100]]] (* _Andres Cicuttin_, May 18 2017 *)",
				"lidn[n_]:=Module[{idn=IntegerDigits[n,2]},idn=If[Last[idn]==0,Flatten[ Most[ Split[ idn]]],idn];Length[idn]]; Array[lidn,100] (* _Harvey P. Dale_, Oct 18 2020 *)"
			],
			"program": [
				"(Haskell)",
				"a070940 = maximum . a080080_row  -- _Reinhard Zumkeller_, Apr 22 2013",
				"(R)",
				"blocklevel \u003c- 7  # by choice",
				"a \u003c- 1",
				"for(m in 0:blocklevel)",
				"  for(k in 0:(2^m-1)){",
				"    a[2^(m+1)+2*k  ] \u003c-  a[2^m+k]",
				"    a[2^(m+1)+2*k+1] \u003c-  m + 2",
				"}",
				"a",
				"# _Yosu Yurramendi_, Aug 08 2019",
				"(Python)",
				"def A070940(n):",
				"    while n%2 == 0:",
				"        n = n//2",
				"    a = 0",
				"    while n != 0:",
				"        n, a = n//2, a+1",
				"    return a",
				"n = 0",
				"while n \u003c 100:",
				"    n = n+1",
				"    print(n,A070940(n)) # _A.H.M. Smeets_, Aug 19 2019"
			],
			"xref": [
				"Cf. A070939, A001511. Differs from A002487 around 11th term.",
				"Cf. A000005, A007318, A000079, A082907, A082908.",
				"Bisections give A070941 and this sequence (again).",
				"Cf. A002064 (row sums), A199570."
			],
			"keyword": "nonn,nice,easy,tabf",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_, May 18 2002",
			"ext": [
				"Entry revised by _Ralf Stephan_, Nov 29 2004"
			],
			"references": 11,
			"revision": 56,
			"time": "2020-10-18T10:48:38-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}