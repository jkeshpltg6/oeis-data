{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A344686",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 344686,
			"data": "0,1,-1,4,-1,-4,9,1,-5,-9,16,5,-4,-11,-16,25,11,-1,-11,-19,-25,36,19,4,-9,-20,-29,-36,49,29,11,-5,-19,-31,-41,-49,64,41,20,1,-16,-31,-44,-55,-64,81,55,31,9,-11,-29,-45,-59,-71,-81,100,71,44,19,-4,-25,-44,-61,-76,-89,-100",
			"name": "Triangle T(n, k) obtained from the array N2(a, b) = a^2 - a*b - b^2, for a \u003e= 0 and b \u003e= 0, read by upwards antidiagonals.",
			"comment": [
				"The general array N(a, b) gives the norms of the integers alpha = a*1 + b*phi, for rational integers a and b, with phi = (1 + sqrt(5))/2 = A001622, in the real quadratic number field Q(phi), also called Q(sqrt(5)). N(a, b) := alpha*alpha' = a^2 + a*b - b^2, with alpha' = (a+b)*1 - b*phi. (phi' = (1 - sqrt(5))/2 = 1 - phi.)",
				"The present array is N2(a, b) = N(a,-b) = N(-a, b), for a \u003e= 0 and b \u003e= 0. The companion array N1(a, b) = N(a, b), for a \u003e= 0 and b \u003e= 0, is given (as triangle) in A281385.",
				"For units and primes in Q(phi), and for references, see A344685."
			],
			"reference": [
				"F. W. Dodd, Number theory in the quadratic field with golden section unit, Polygonal Publishing House, Passaic, NJ.",
				"G. H. Hardy and E. M. Wright, An Introduction to the Theory of Numbers, fifth edition, Clarendon Press Oxford, 2003."
			],
			"formula": [
				"Array N2(a, b) = a^2 - a*b - b^2, for a \u003e= 0 and b \u003e= 0.",
				"Triangle T(n, k) = N2(n-k, k) = N(n-k, -k) = n^2 - 3*n*k + k^2, for n \u003e= 0 and k = 0, 1, ..., n.",
				"G.f. for row polynomials R(n, y) = Sum_{k=0..n} T(n, k)*y^k, i.e. of the triangle: G(x, y) = x*(1 - y + (1 -y - 1*y^2)*x - y*(2 - 4*y)*x^2  - y^2*x^3)/((1 - x*y)^3*(1 - x)^3) (compare with the g.f.s in A281385 and A344685)."
			],
			"example": [
				"The array N2(a, b) begins:",
				"a \\ b   0  1  2   3   4   5   6   7   8    9   10 ...",
				"-----------------------------------------------------",
				"O:      0 -1 -4  -9 -16 -25 -36 -49 -64  -81 -100 ...",
				"1:      1 -1 -5 -11 -19 -29 -41 -55 -71  -89 -109 ...",
				"2:      4  1 -4 -11 -20 -31 -44 -59 -76  -95 -116 ...",
				"3:      9  5 -1  -9 -19 -31 -45 -61 -79  -99 -121 ..",
				"4:     16 11  4  -5 -16 -29 -44 -61 -80 -101 -124 ...",
				"5:     25 19 11   1 -11 -25 -41 -59 -79 -101 -125 ...",
				"6:     36 29 20   9  -4 -19 -36 -55 -76  -99 -124 ...",
				"7:     49 41 31  19   5 -11 -29 -49 -71  -95 -121 ...",
				"8:     64 55 44  31  16  -1 -20 -41 -64  -89 -116 ...",
				"9:     81 71 59  45  29  11  -9 -31 -55  -81 -109 ...",
				"10:   100 89 76  61  44  25   4 -19 -44  -71 -100 ...",
				"...",
				"------------------------------------------------------",
				"The Triangle T(n, k) begins:",
				"n \\ k  0   1   2   3   4   5   6   7   8   9   10 ...",
				"-----------------------------------------------------",
				"O:     0",
				"1:     1  -1",
				"2:     4  -1  -4",
				"3:     9   1  -5  -9",
				"4:    16   5  -4 -11 -16",
				"5:    25  11  -1 -11 -19 -25",
				"6:    36  19   4  -9 -20 -29 -36",
				"7:    49  29  11  -5 -19 -31 -41 -49",
				"8:    64  41  20   1 -16 -31 -44 -55 -64",
				"9:    81  55  31   9 -11 -29 -45 -59 -71 -81",
				"10:  100  71  44  19  -4 -25 -44 -61 -76 -89 -100",
				"...",
				"------------------------------------------------------",
				"Units from norm N(a, -b) = N2(a, b) = +1 or -1, for a \u003e= 0 and b \u003e= 0: +(a, b) or -(a, b), with (a, b) = (0, 1), (1, 0), (1, 1), (2, 1), (3, 2), (5, 3), (8, 5), ...; cases + or -  phi^n, n \u003e= 0. Fibonacci neighbors.",
				"Some primes im Q(phi) from  |N(a, -b)| = q, with q a prime in Q:",
				"a = 1:  (1, 2), (1, 3), (1, 4), (1, 5), (1, 6), (1, 8), (1, 9), (1, 10), ...",
				"a = 2:  (2, 3), (2, 5), (2, 7), ...",
				"a = 3:  (3, 1), (3, 4), (3, 5), (3, 7), (3, 8), ...",
				"a = 4:  (4, 1), (4, 3), (4, 5), (4, 7), (4, 9), ...",
				"a = 5:  (5, 1), (5, 2), (5, 4), (5, 6), (5, 7), (5, 8), (5, 9), ...",
				"a = 6:  (6, 1), (6, 5), ...",
				"a = 7:  (7, 1), (7, 2), (7, 3), (7, 4), (7, 5), (7, 6), (7, 8), ...",
				"a = 8:  (8, 3), (8, 7), (8, 9), ...",
				"a = 9:  (9, 1), (9, 2), (9, 4), (9, 5), (9, 7), (9, 10), ...",
				"a = 10: (10, 1), (10, 3), (10, 7), (10, 9), ..."
			],
			"xref": [
				"Cf. A281385, A344685."
			],
			"keyword": "sign,tabl,easy",
			"offset": "0,4",
			"author": "_Wolfdieter Lang_, Jun 17 2021",
			"references": 1,
			"revision": 17,
			"time": "2021-08-08T01:41:54-04:00",
			"created": "2021-08-08T01:41:54-04:00"
		}
	]
}