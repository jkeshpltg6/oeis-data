{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A258245",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 258245,
			"data": "0,3,1,12,6,4,40,2,15,21,13,128,5,7,9,43,50,69,41,405,25,31,14,16,18,22,131,138,160,219,129,1275,8,10,53,59,72,81,100,42,44,47,51,70,408,414,436,505,691,406,4008,34,17,19,23,26,28,32,141,150,163,169",
			"name": "Irregular triangle (Beatty tree for Pi) as determined in Comments; a permutation of the nonnegative integers.",
			"comment": [
				"The Beatty tree for an irrational number r \u003e 1 (such as r = Pi), is formed as follows.  To begin, let s = r/(r-1), so that the sequences defined u and v defined by u(n) = floor(r*n) and v(n) = floor(s*n), for n \u003e=1 are the Beatty sequences of r and s, and u and v partition the positive integers.",
				"The tree T has root 0 with an edge to 3, and all other edges are determined as follows:  if x is in u(v), then there is an edge from x to floor(r + r*x) and an edge from x to ceiling(x/r); otherwise there is an edge from x to floor(r + r*x).  (Thus, the only branchpoints are the numbers in u(v).)",
				"Another way to form T is by \"backtracking\" to the root 0.  Let b(x) = floor[x/r] if x is in (u(n)), and b(x) = floor[r*x] if x is in (v(n)).  Starting at any vertex x, repeated applications of b eventually reach 0.  The number of steps to reach 0 is the number of the generation of T that contains x.  (See Example for x = 8).",
				"See A258212 for a guide to Beatty trees for various choices of r."
			],
			"example": [
				"Rows (or generations, or levels) of T:",
				"0",
				"3",
				"1   12",
				"6   4   40",
				"2   21  15   13   128",
				"9   7   69   5    50   43   42   405",
				"31  25  22   219  18   16   160  14   138   131   129   1275",
				"Generations 0 to 7 of the tree are drawn by the Mathematica program.  In T, the path from 0 to 8 is (0,3,1,6,21,7,25,8).  The path obtained by backtracking (i.e., successive applications of the mapping b in Comments) is (8,25,7,21,6,1,3,0)."
			],
			"mathematica": [
				"r = Pi; k = 2000; w = Map[Floor[r #] \u0026, Range[k]];",
				"f[x_] := f[x] = If[MemberQ[w, x], Floor[x/r], Floor[r*x]];",
				"b := NestWhileList[f, #, ! # == 0 \u0026] \u0026;",
				"bs = Map[Reverse, Table[b[n], {n, 0, k}]];",
				"generations = Table[DeleteDuplicates[Map[#[[n]] \u0026, Select[bs, Length[#] \u003e n - 1 \u0026]]], {n, 8}]",
				"paths = Sort[Map[Reverse[b[#]] \u0026, Last[generations]]]",
				"graph = DeleteDuplicates[Flatten[Map[Thread[Most[#] -\u003e Rest[#]] \u0026, paths]]]",
				"TreePlot[graph, Top, 0, VertexLabeling -\u003e True, ImageSize -\u003e 800]",
				"Map[DeleteDuplicates, Transpose[paths]] (* _Peter J. C. Moses_,May 21 2015 *)"
			],
			"xref": [
				"Cf. A022844, A258244 (path-length, 0 to n), A258212."
			],
			"keyword": "nonn,tabf,easy",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Jun 08 2015",
			"references": 2,
			"revision": 4,
			"time": "2015-06-11T10:36:14-04:00",
			"created": "2015-06-11T10:36:14-04:00"
		}
	]
}