{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007579",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7579,
			"id": "M1217",
			"data": "1,1,2,4,10,26,76,231,756,2556,9096,33231,126060,488488,1948232,7907185,32831370,138321690,593610420,2579109780,11377862340,50726936820,229078351992,1043999256966,4810194384348,22340617618860,104742353862360,494547143860035",
			"name": "Number of Young tableaux of height \u003c= 6.",
			"comment": [
				"Also the number of n-length words w over 6-ary alphabet {a1,a2,...,a6} such that for every prefix z of w we have #(z,a1) \u003e= #(z,a2) \u003e= ... \u003e= #(z,a6), where #(z,x) counts the letters x in word z. - _Alois P. Heinz_, May 30 2012"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A007579/b007579.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"F. Bergeron, L. Favreau and D. Krob, \u003ca href=\"/A007578/a007578.pdf\"\u003eConjectures on the enumeration of tableaux of bounded height\u003c/a\u003e, Preprint. (Annotated scanned copy)",
				"F. Bergeron, L. Favreau and D. Krob, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(94)00148-C\"\u003eConjectures on the enumeration of tableaux of bounded height\u003c/a\u003e, Discrete Math, vol. 139, no. 1-3 (1995), 463-468.",
				"Alon Regev, Amitai Regev, Doron Zeilberger, \u003ca href=\"http://arxiv.org/abs/1507.03499\"\u003eIdentities in character tables of S_n\u003c/a\u003e, arXiv preprint arXiv:1507.03499 [math.CO], 2015.",
				"\u003ca href=\"/index/Y#Young\"\u003eIndex entries for sequences related to Young tableaux.\u003c/a\u003e"
			],
			"formula": [
				"a(n) ~ 3/4 * 6^(n+15/2)/(Pi^(3/2)*n^(15/2)). - _Vaclav Kotesovec_, Sep 11 2013",
				"D-finite with recurrence +(n+5)*(n+9)*(n+8)*a(n) +4*(-5*n^2-46*n-84)*a(n-1) -4*(n-1)*(10*n^2+58*n+33)*a(n-2) +144*(n-1)*(n-2)*a(n-3) +144*(n-1)*(n-2)*(n-3)*a(n-4)=0. - _R. J. Mathar_, Sep 23 2021"
			],
			"maple": [
				"h:= proc(l) local n; n:=nops(l); add(i, i=l)! /mul(mul(1+l[i]-j",
				"      +add(`if`(l[k]\u003e=j, 1, 0), k=i+1..n), j=1..l[i]), i=1..n)",
				"    end:",
				"g:= proc(n, i, l) option remember;",
				"      `if`(n=0, h(l), `if`(i=1, h([l[], 1$n]), `if`(i\u003c1, 0,",
				"        g(n, i-1, l) +`if`(i\u003en, 0, g(n-i, i, [l[], i])))))",
				"    end:",
				"a:= n-\u003e g(n, 6, []):",
				"seq(a(n), n=0..30); # _Alois P. Heinz_, Apr 18 2012",
				"# second Maple program:",
				"a:= proc(n) option remember;",
				"      `if`(n\u003c4, [1, 1, 2, 4][n+1], ((20*n^2+184*n+336)*a(n-1)",
				"       +4*(n-1)*(10*n^2+58*n+33)*a(n-2) -144*(n-1)*(n-2)*a(n-3)",
				"       -144*(n-1)*(n-2)*(n-3)*a(n-4))/ ((n+5)*(n+8)*(n+9)))",
				"    end:",
				"seq(a(n), n=0..30);  # _Alois P. Heinz_, Oct 12 2012"
			],
			"mathematica": [
				"RecurrenceTable[{144 (-3+n) (-2+n) (-1+n) a[-4+n]+144 (-2+n) (-1+n) a[-3+n]-4 (-1+n) (33+58 n+10 n^2) a[-2+n]-4 (84+46 n+5 n^2) a[-1+n]+(5+n) (8+n) (9+n) a[n]==0,a[1]==1,a[2]==2,a[3]==4,a[4]==10}, a, {n, 20}] (* _Vaclav Kotesovec_, Sep 11 2013 *)"
			],
			"xref": [
				"Column k=6 of A182172. - _Alois P. Heinz_, May 30 2012"
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Simon Plouffe_",
			"ext": [
				"More terms from _Alois P. Heinz_, Apr 10 2012"
			],
			"references": 14,
			"revision": 61,
			"time": "2021-09-23T05:47:30-04:00",
			"created": "1994-09-19T03:00:00-04:00"
		}
	]
}