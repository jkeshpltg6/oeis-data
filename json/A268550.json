{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A268550",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 268550,
			"data": "1,12,366,13800,574650,25335072,1159174464,54435558672,2606102756730,126634810078920,6226427808402516,309095505195676992,15466884541698962736,779158533743408851200,39476348002042199114400,2010009672816216740255520",
			"name": "Diagonal of 1/(1 - x - y - z - x y - x z - y z).",
			"comment": [
				"Annihilating differential operator: x*(x+2)*(x+1)*(27*x^2+54*x-1)*Dx^2 + (81*x^4+324*x^3+431*x^2+214*x-2)*Dx + 24*x^3+72*x^2+72*x+24. - _Gheorghe Coserea_, Jul 03 2016"
			],
			"link": [
				"Gheorghe Coserea, \u003ca href=\"/A268550/b268550.txt\"\u003eTable of n, a(n) for n = 0..310\u003c/a\u003e, First 201 terms from Vaclav Kotesovec.",
				"A. Bostan, S. Boukraa, J.-M. Maillard, J.-A. Weil, \u003ca href=\"http://arxiv.org/abs/1507.03227\"\u003eDiagonals of rational functions and selected differential Galois groups\u003c/a\u003e, arXiv preprint arXiv:1507.03227 [math-ph], 2015.",
				"Jacques-Arthur Weil, \u003ca href=\"http://www.unilim.fr/pages_perso/jacques-arthur.weil/diagonals/\"\u003eSupplementary Material for the Paper \"Diagonals of rational functions and selected differential Galois groups\"\u003c/a\u003e"
			],
			"formula": [
				"Conjecture: 2*n^2*(n-1)*a(n) -(n-1)*(107*n^2-107*n+24)*a(n-1) +12*(-9*n^3+27*n^2-22*n+2)*a(n-2) -3*n*(3*n-5)*(3*n-7)*a(n-3)=0. - _R. J. Mathar_, Mar 10 2016",
				"a(n) ~ sqrt(3) * (27 + 6*sqrt(21))^n / (2*Pi*n). - _Vaclav Kotesovec_, Jul 01 2016",
				"G.f.: hypergeom([1/12, 5/12], [1], 1728*x^3*(x+2)^3*(27*x^2+54*x-1)/(-1+48*x+24*x^2)^3)/(1-48*x-24*x^2)^(1/4). - _Gheorghe Coserea_, Jul 01 2016",
				"0 = x*(x+2)*(x+1)*(27*x^2+54*x-1)*y'' + (81*x^4+324*x^3+431*x^2+214*x-2)*y' + (24*x^3+72*x^2+72*x+24)*y, where y is g.f. - _Gheorghe Coserea_, Jul 03 2016"
			],
			"maple": [
				"A268550 := proc(n)",
				"    1/(1-x-y-z-x*y-x*z-y*z) ;",
				"    coeftayl(%,x=0,n) ;",
				"    coeftayl(%,y=0,n) ;",
				"    coeftayl(%,z=0,n) ;",
				"end proc:",
				"seq(A268550(n),n=0..20) ; # _R. J. Mathar_, Mar 10 2016"
			],
			"mathematica": [
				"gf = Hypergeometric2F1[1/12, 5/12, 1, 1728*x^3*(x + 2)^3*(27*x^2 + 54*x - 1)/(-1 + 48*x + 24*x^2)^3]/(1 - 48*x - 24*x^2)^(1/4);",
				"CoefficientList[gf + O[x]^20, x] (* _Jean-François Alcover_, Dec 03 2017, after _Gheorghe Coserea_ *)"
			],
			"program": [
				"(PARI)",
				"my(x='x, y='y, z='z);",
				"R =  1/(1 - x - y - z - x*y - x*z - y*z);",
				"diag(n, expr, var) = {",
				"  my(a = vector(n));",
				"  for (i = 1, #var, expr = taylor(expr, var[#var - i + 1], n));",
				"  for (k = 1, n, a[k] = expr;",
				"       for (i = 1, #var, a[k] = polcoeff(a[k], k-1)));",
				"  return(a);",
				"};",
				"diag(10, R, [x,y,z])",
				"(PARI) \\\\ system(\"wget http://www.jjj.de/pari/hypergeom.gpi\");",
				"read(\"hypergeom.gpi\");",
				"N = 16; x = 'x + O('x^N);",
				"Vec(hypergeom([1/12, 5/12], [1], 1728*x^3*(x+2)^3*(27*x^2+54*x-1)/(-1+48*x+24*x^2)^3, N)/(1-48*x-24*x^2)^(1/4)) \\\\ _Gheorghe Coserea_, Jul 03 2016"
			],
			"xref": [
				"Cf. A268545-A268555."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Feb 29 2016",
			"references": 2,
			"revision": 28,
			"time": "2017-12-03T02:09:14-05:00",
			"created": "2016-02-29T10:28:24-05:00"
		}
	]
}