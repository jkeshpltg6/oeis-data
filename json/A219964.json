{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A219964",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 219964,
			"data": "1,1,2,3,2,5,3,7,4,1,5,11,9,13,7,1,16,17,1,19,25,1,11,23,81,1,13,1,49,29,1,31,256,1,17,1,1,37,19,1,625,41,1,43,121,1,23,47,6561,1,1,1,169,53,1,1,2401,1,29,59,1,61,31,1,65536,1,1,67,289,1,1,71",
			"name": "a(n) = product(i \u003e= 0, (P(n, i)/P(n-1, i))^(2^i)) where P(n, i) = product(p prime, n/2^(i+1) \u003c p \u003c= n/2^i).",
			"comment": [
				"a(n) is 1 or a prime or an even power of a prime (A084400, A050376).",
				"If n \u003e 0 then a(n) = 1 if and only if n is an element of A110473."
			],
			"link": [
				"Peter Luschny, \u003ca href=\"/A219964/b219964.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A220027(n) / A220027(n-1)."
			],
			"example": [
				"a(20) = (7/(5*7))^2*((3*5)/3)^4 = 25.",
				"a(22) = ((13*17*19)/(11*13*17*19))*((7*11)/7)^2 = 11."
			],
			"maple": [
				"A219964 := proc(n) local l, m, z;",
				"if isprime(n) then RETURN(n) fi;",
				"z := 1; l := n - 1; m := n;",
				"do l := iquo(l, 2); m := iquo(m, 2);",
				"   if l = 0 then break fi;",
				"   if l \u003c m then if isprime(l+1) then RETURN((l+1)^z) fi fi;",
				"   z := z + z;",
				"od; 1 end:  seq(A219964(k), k=0..71);"
			],
			"mathematica": [
				"a[n_] := Module[{l, m, z}, If[PrimeQ[n] , Return[n] ]; z = 1; l = Max[0, n - 1]; m = n; While[True, l = Quotient[l, 2]; m = Quotient[m, 2]; If[l == 0 , Break[]]; If[l \u003c m , If[ PrimeQ[l+1], Return[(l+1)^z]]]; z = z+z]; 1]; Table[a[k], {k, 0, 71}] (* _Jean-François Alcover_, Jan 15 2014, after Maple *)"
			],
			"program": [
				"(J)",
				"genSeq=: 3 :0",
				"p=. x: i.\u0026.(_1\u0026p:) y1=.y+1",
				"i=.(#~y1\u003e])\u0026.\u003e \u003c:@((i.@\u003e.\u0026.(2\u0026^.)y1)*])\u0026.\u003e p",
				"y{.(;p(^2x^0,i.@\u003c:@#)\u0026.\u003ei) (;i) } y1$1",
				")",
				"(Sage)",
				"def A219964(n):",
				"    if is_prime(n): return n",
				"    z = 1; l = max(0,n-1); m = n",
				"    while true:",
				"        l = l // 2",
				"        m = m // 2",
				"        if l == 0: break",
				"        if l \u003c m:",
				"            if is_prime(l+1): return (l+1)^z",
				"        z = z + z",
				"    return 1",
				"[A219964(n) for n in (0..71)]"
			],
			"xref": [
				"Cf. A220027, the partial products of a(n)."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Peter Luschny_ and _Arie Groeneveld_, Mar 30 2013",
			"references": 2,
			"revision": 22,
			"time": "2018-07-08T02:08:37-04:00",
			"created": "2013-04-09T14:03:47-04:00"
		}
	]
}