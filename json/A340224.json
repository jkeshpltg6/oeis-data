{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A340224",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 340224,
			"data": "0,1,2,2,3,2,4,1,2,2,3,2,4,5,3,3,5,5,2,6,4,4,4,6,3,5,4,5,6,6,4,4,6,6,3,5,6,5,6,7,3,6,5,7,5,4,8,3,6,7,7,7,5,6,7,7,4,8,7,6,6,5,8,4,9,7,6,8,8,4,5,7,8,6,8,9,8,7,5,9,8,6,7,6,9,7,10,7,7,9,7,5,7,8,8",
			"name": "a(n+1) = a(n-a(n)*(a(n)-1)/2) + 1, starting with a(1) = 0.",
			"comment": [
				"To obtain the next term, compute the binomial(a,2) with the current term, then count back its value and add 1.",
				"The sequence cannot repeat. Proof: Assume a finite period. Label an arbitrary term in the period x. Because of the back-referencing definition it follows that x-1 has to be in the period, and by the same argument so does x-2 and x-3, x-4, ... until 0. But it is not possible to obtain new 0s since each new term is larger than one already existing term.",
				"Every positive integer appears in the sequence.",
				"First occurrence of n: 1, 2, 3, 5, 7, 14, 20, 40, 47, 65, 87, 124, 170, 210, 258, 310, 389, 469, 548, 640, 758, 864, ...",
				"The sequence appears to grow with the cube root of n, which is expected since f(x) = (6*x)^(1/3) satisfies the definition for large x, i.e., lim_{x-\u003eoo} f(x+1)-(f(x-f(x)*(f(x)-1)/2)+1) = 0.",
				"Each term is determined by referencing an earlier term. Following the chain of these references we can uniquely trace back every term to the initial zero, e.g., a(40) -\u003e a(24) -\u003e a(17) -\u003e a(13) -\u003e a(11) -\u003e a(9) -\u003e a(8) -\u003e a(1). These progressions form a tree, with the zero being at its root (for visualization see the Links section). The average branching factor B (number of links divided by the number of non-leaf nodes) is numerically evaluated to B = 1.380... (for n = 10^8). Considering the asymptotic behavior of the sequence a(n) ~ (6*n)^(1/3), we can expect that the number of links within a range (n,m) is asymptotically equal to m-n and therefore B is the inverse of the proportion of non-leaf terms (B = 1.380... then implies that roughly 27.5% of all terms never get referenced)."
			],
			"link": [
				"Rok Cestnik, \u003ca href=\"/A340224/a340224.pdf\"\u003eTerm-referencing tree for 500 terms\u003c/a\u003e",
				"Rok Cestnik, \u003ca href=\"/A340224/a340224.py.txt\"\u003eProgram for plotting the term-referencing tree\u003c/a\u003e"
			],
			"formula": [
				"a(n) ~ (6*n)^(1/3) (conjectured)."
			],
			"example": [
				"a(2) = a(1-a(1)*(a(1)-1)/2)+1 = a(1)+1 = 1.",
				"a(3) = a(2-a(2)*(a(2)-1)/2)+1 = a(2)+1 = 2.",
				"a(4) = a(3-a(3)*(a(3)-1)/2)+1 = a(2)+1 = 2.",
				"a(5) = a(4-a(4)*(a(4)-1)/2)+1 = a(3)+1 = 3."
			],
			"program": [
				"(Python)",
				"a = [0]",
				"for n in range(1000):",
				"    a.append(a[int(n-a[n]*(a[n]-1)/2)]+1)"
			],
			"xref": [
				"Related to A339929.",
				"Cf. A330772, A340134, A005206, A002516."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Rok Cestnik_, Jan 01 2021",
			"references": 1,
			"revision": 6,
			"time": "2021-01-10T11:15:57-05:00",
			"created": "2021-01-10T11:15:57-05:00"
		}
	]
}