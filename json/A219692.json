{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A219692",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 219692,
			"data": "2,6,54,564,6390,76356,948276,12132504,158984694,2124923460,28877309604,398046897144,5554209125556,78328566695736,1114923122685720,15999482238880464,231253045986317814,3363838379489630916",
			"name": "a(n) = Sum_{j=0..floor(n/3)} (-1)^j C(n,j) * C(2j,j) * C(2n-2j,n-j) * (C(2n-3j-1,n) + C(2n-3j,n)).",
			"comment": [
				"This sequence is s_18 in Cooper's paper.",
				"This is one of the Apery-like sequences - see Cross-references. - _Hugo Pfoertner_, Aug 06 2017",
				"Every prime eventually divides some term of this sequence. - _Amita Malik_, Aug 20 2017"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A219692/b219692.txt\"\u003eTable of n, a(n) for n = 0..830\u003c/a\u003e (terms 0..254 from Jason Kimberley)",
				"S. Cooper, \u003ca href=\"http://dx.doi.org/10.1007/s11139-011-9357-3\"\u003eSporadic sequences, modular forms and new series for 1/pi\u003c/a\u003e, Ramanujan J. (2012).",
				"Ofir Gorodetsky, \u003ca href=\"https://arxiv.org/abs/2102.11839\"\u003eNew representations for all sporadic Apéry-like sequences, with applications to congruences\u003c/a\u003e, arXiv:2102.11839 [math.NT], 2021. See s18 p. 3.",
				"Amita Malik and Armin Straub, \u003ca href=\"https://doi.org/10.1007/s40993-016-0036-8\"\u003eDivisibility properties of sporadic Apéry-like numbers\u003c/a\u003e, Research in Number Theory, 2016, 2:5."
			],
			"formula": [
				"1/Pi",
				"= 2*3^(-5/2) Sum {k\u003e=0} (n a(n)/18^n) [Cooper, equation (42)]",
				"= 2*3^(-5/2) Sum {k\u003e=0} (n a(n)/A001027(n)).",
				"G.f.: 1+hypergeom([1/8, 3/8],[1],256*x^3/(1-12*x)^2)^2/sqrt(1-12*x). - _Mark van Hoeij_, May 07 2013",
				"Conjecture D-finite with recurrence: n^3*a(n) -2*(2*n-1)*(7*n^2-7*n+3)*a(n-1) +12*(4*n-5)*(n-1)* (4*n-3)*a(n-2)=0. - _R. J. Mathar_, Jun 14 2016"
			],
			"mathematica": [
				"Table[Sum[(-1)^j*Binomial[n,j]*Binomial[2j,j]*Binomial[2n-2j, n-j]* (Binomial[2n-3j-1,n] +Binomial[2n-3j,n]), {j,0,Floor[n/3]}], {n,0,20}] (* _G. C. Greubel_, Oct 24 2017 *)"
			],
			"program": [
				"(MAGMA) s_18 := func\u003ck|\u0026+[(-1)^j*C(k,j)*C(2*j,j)*C(2*k-2*j,k-j)*(C(2*k-3*j-1,k)+C(2*k-3*j,k)):j in[0..k div 3]]\u003e where C is Binomial;",
				"(PARI) {a(n) = sum(j=0,floor(n/3), (-1)^j*binomial(n,j)*binomial(2*j,j)* binomial(2*n-2*j,n-j)*(binomial(2*n-3*j-1,n) +binomial(2*n-3*j,n)))}; \\\\ _G. C. Greubel_, Apr 02 2019",
				"(Sage) [sum((-1)^j*binomial(n,j)*binomial(2*j,j)*binomial(2*n-2*j,n-j)* (binomial(2*n-3*j-1,n)+binomial(2*n-3*j,n)) for j in (0..floor(n/3))) for n in (0..20)] # _G. C. Greubel_, Apr 02 2019"
			],
			"xref": [
				"The Apéry-like numbers [or Apéry-like sequences, Apery-like numbers, Apery-like sequences] include A000172, A000984, A002893, A002895, A005258, A005259, A005260, A006077, A036917, A063007, A081085, A093388, A125143 (apart from signs), A143003, A143007, A143413, A143414, A143415, A143583, A183204, A214262, A219692,A226535, A227216, A227454, A229111 (apart from signs), A260667, A260832, A262177, A264541, A264542, A279619, A290575, A290576. (The term \"Apery-like\" is not well-defined.)"
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "_Jason Kimberley_, Nov 25 2012",
			"references": 34,
			"revision": 40,
			"time": "2021-02-25T07:29:05-05:00",
			"created": "2012-11-26T12:32:02-05:00"
		}
	]
}