{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A279618",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 279618,
			"data": "1,-9,30,-15,-240,978,-1463,-2361,18201,-42800,15624,227742,-809028,1088367,1593120,-11383551,25003158,-8589729,-119069358,403991280,-521730930,-736063496,5088063696,-10843708302,3624181875,48991048836,-162420646812,205328313785,284014016994",
			"name": "Expansion of w_7/(1 + 13*w_7 + 49*w_7^2) in powers of q, where w_7 = (eta(7*q)/eta(q))^4.",
			"comment": [
				"G.f. is  y_7 in Cooper's paper.",
				"See Equation (3.15) and Theorem 3.10 in O'Brien's thesis.",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (7 t)) = f(t) where q = exp(2 Pi i t). - _Michael Somos_, Sep 07 2018"
			],
			"reference": [
				"S. Cooper, (2012). Sporadic sequences, modular forms and new series for 1/pi. The Ramanujan Journal, 29(1-3), 163-183.",
				"L. O'Brien, Modular forms and two new integer sequences at level 7, Massey University, 2016."
			],
			"link": [
				"Lynette O'Brien, \u003ca href=\"https://www.researchgate.net/profile/Lynette_Obrien\"\u003eModular forms and two new integer sequences at level 7\u003c/a\u003e",
				"L. O'Brien, \u003ca href=\"https://doi.org/10.13140/RG.2.2.33912.03843\"\u003eModular forms and two new integer sequences at level 7\u003c/a\u003e, Massey University, 2016."
			],
			"formula": [
				"G.f. is w_7/(1 + 13*w_7 + 49*w_7^2) = (eta(q)*eta(7q)/z_7)^3 where w_7 = (eta(7*q)/eta(q))^4 and z_7 = 1 + 2*Sum_{k\u003e0} Kronecker(-7,k)*q^k/(1-q^k).",
				"G.f. is also (eta(q)*eta(7*q)/z_7)^3, where z_7 = 1 + 2*Sum_{k\u003e0} Kronecker(-7,k)*q^k/(1-q^k). See A002652."
			],
			"example": [
				"G.f. = q - 9*q^2 + 30*q^3 - 15*q^4 - 240*q^5 + 978*q^6 - 1463*q^7 + ..."
			],
			"mathematica": [
				"a[ n_] := With[{u1 = QPochhammer[ x]^4, u7 = QPochhammer[ x^7]^4}, SeriesCoefficient[ x u1 u7 / (u1^2 + 13 x u1 u7 + 49 x^2 u7^2) , {x, 0, n}]]; (* _Michael Somos_, Sep 07 2018 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c1, 0, A = x * O(x^n); A = x * (eta(x^7 + A) / eta(x + A))^4; polcoeff( 1 / (1/A + 13 + 49*A), n))}; /* _Michael Somos_, Sep 07 2018 */"
			],
			"xref": [
				"Cf. A002652, A121593, A279613, A279619."
			],
			"keyword": "sign",
			"offset": "1,2",
			"author": "_Lynette O'Brien_, Dec 15 2016",
			"references": 2,
			"revision": 28,
			"time": "2018-09-07T12:30:41-04:00",
			"created": "2016-12-24T09:27:07-05:00"
		}
	]
}