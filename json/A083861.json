{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A083861",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 83861,
			"data": "0,0,1,0,1,5,0,1,5,19,0,1,5,20,65,0,1,5,21,75,211,0,1,5,22,85,275,665,0,1,5,23,95,341,1000,2059,0,1,5,24,105,409,1365,3625,6305,0,1,5,25,115,479,1760,5461,13125,19171,0,1,5,26,125,551,2185,7573,21845,47500,58025",
			"name": "Square array T(n,k) of second binomial transforms of generalized Fibonacci numbers, read by ascending antidiagonals, with n, k \u003e= 0.",
			"comment": [
				"Row n \u003e= 0 of the array gives the solution to the recurrence b(k) = 5*b(k-1) + (n - 6)*b(k-2) for k \u003e= 2 with b(0) = 0 and b(1) = 1. The rows are the binomial transforms of the rows of array A083857. The rows are the second binomial transforms of the generalized Fibonacci numbers in array A083856."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A083861/b083861.txt\"\u003eAntidiagonals n = 0..100, flattened\u003c/a\u003e",
				"OEIS, \u003ca href=\"https://oeis.org/transforms.html\"\u003eTransformations of integer sequences\u003c/a\u003e."
			],
			"formula": [
				"T(n, k) = (((5 + sqrt(4*n + 1))/2)^k - ((5 - sqrt(4*n + 1))/2)^k)/sqrt(4*n + 1).",
				"O.g.f. for row n \u003e= 0: -x/(-1 + 5*x + (n-6)*x^2) . - _R. J. Mathar_, Dec 02 2007",
				"From _Petros Hadjicostas_, Dec 25 2019: (Start)",
				"T(n,k) = 5*T(n,k-1) + (n - 6)*T(n,k-2) for k \u003e= 2 with T(n,0) = 0 and T(n,1) = 1 for all n \u003e= 0.",
				"T(n,k) = Sum_{i = 0..k} binomial(k,i) * A083857(n,i).",
				"T(n,k) = Sum_{i = 0..k} Sum_{j = 0..i} binomial(k,i) * binomial(i,j) * A083856(n,j). (End)"
			],
			"example": [
				"Array T(n,k) (with rows n \u003e= 0 and columns k \u003e= 0) begins as follows:",
				"  0, 1, 5, 19,  65, 211,  665,  2059,  6305,  19171, ...",
				"  0, 1, 5, 20,  75, 275, 1000,  3625, 13125,  47500, ...",
				"  0, 1, 5, 21,  85, 341, 1365,  5461, 21845,  87381, ...",
				"  0, 1, 5, 22,  95, 409, 1760,  7573, 32585, 140206, ...",
				"  0, 1, 5, 23, 105, 479, 2185,  9967, 45465, 207391, ...",
				"  0, 1, 5, 24, 115, 551, 2640, 12649, 60605, 290376, ...",
				"  0, 1, 5, 25, 125, 625, 3125, 15625, 78125, 390625, ...",
				"  ..."
			],
			"maple": [
				"seq(seq(round( (((5+sqrt(4*(n-k)+1))/2)^k - ((5-sqrt(4*(n-k)+1))/2)^k)/sqrt(4*(n-k)+1) ), k=0..n), n=0..10); # _G. C. Greubel_, Dec 27 2019"
			],
			"mathematica": [
				"T[n_, k_]:= Round[(((5 +Sqrt[4*n+1])/2)^k - ((5 -Sqrt[4*n+1])/2)^k)/Sqrt[4*n+1]]; Table[T[n-k, k], {n, 0, 10}, {k, 0, n}]//Flatten (* _G. C. Greubel_, Dec 27 2019 *)"
			],
			"program": [
				"(PARI) T(n, k) = round( (((5+sqrt(4*n+1))/2)^k - ((5-sqrt(4*n+1))/2)^k)/sqrt(4*n + 1) );",
				"for(n=0,10, for(k=0,n, print1(T(n-k,k), \", \"))) \\\\ _G. C. Greubel_, Dec 27 2019",
				"(MAGMA)",
				"T:= func\u003c n,k | Round( (((5+Sqrt(4*n+1))/2)^k - ((5-Sqrt(4*n+1))/2)^k)/Sqrt(4*n + 1) ) \u003e;",
				"[T(n-k,k): k in [0..n], n in [0..10]]; // _G. C. Greubel_, Dec 27 2019",
				"(Sage) [[round( (((5+sqrt(4*(n-k)+1))/2)^k - ((5-sqrt(4*(n-k)+1))/2)^k)/sqrt(4*(n-k)+1) ) for k in (0..n)] for n in (0..10)] # _G. C. Greubel_, Dec 27 2019"
			],
			"xref": [
				"Rows include A001047 (n=0), A093131 (n=1), A002450 (n=2), A004254 (n=5), A000351 (n=6), A052918 (n=7), A015535 (n=8), A015536 (n=9), A015537 (n=10).",
				"Cf. A083856 (second inverse binomial transform), A083856 (first inverse binomial transform), A082297 (main diagonal)."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "0,6",
			"author": "_Paul Barry_, May 06 2003",
			"ext": [
				"Name and various sections edited by _Petros Hadjicostas_, Dec 25 2019"
			],
			"references": 4,
			"revision": 43,
			"time": "2019-12-30T18:56:43-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}