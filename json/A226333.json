{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A226333",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 226333,
			"data": "1,9,28,73,125,252,344,585,757,1125,1332,2044,2198,3096,3500,4681,4914,6813,6860,9125,9632,11988,12168,16380,15625,19782,20440,25112,24390,31500,29792,37449,37296,44226,43000,55261,50654,61740,61544,73125,68922,86688",
			"name": "Expansion of (E_4(q) - E_4(q^5)) / 240 in powers of q where E_4 is an Eisenstein series.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A226333/b226333.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q * (f(-q) * f(-q^5))^4 + 13 * q^2 * (f(-q^5)^5 / f(-q))^2 in powers of q where f() is a Ramanujan theta function.",
				"a(n) is multiplicative with a(p^e) = p^(3*e) if p=5, else a(p^e) = (p^(3*(e+1)) - 1) / (p^3 - 1).",
				"G.f.: Sum_{k\u003e0} k^3 * x^k * (1 - x^(4*k)) / ((1 - x^k) * (1 - x^(5*k))).",
				"a(n) = A004009(n) if n is not divisible by 5, else a(n) = 5^3 * a(n/5)."
			],
			"example": [
				"q + 9*q^2 + 28*q^3 + 73*q^4 + 125*q^5 + 252*q^6 + 344*q^7 + 585*q^8 + 757*q^9 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 1, 0, DivisorSigma[ 3, n] - If[ Divisible[ n, 5], DivisorSigma[ 3, n/5], 0]]",
				"a[ n_] := SeriesCoefficient[ q (QPochhammer[ q] QPochhammer[q^5])^4 + 13 q^2 ( QPochhammer[q^5]^5 / QPochhammer[ q])^2, {q, 0, n}]"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, 0, sigma( n, 3) - if( n%5, 0, sigma( n/5, 3)))}",
				"(PARI) {a(n) = local(A); if( n\u003c1, 0, n--; A = x * O(x^n); polcoeff( (eta(x + A) * eta(x^5 + A))^4 + 13 * x * (eta(x^5 + A)^5 / eta(x + A))^2, n))}",
				"(PARI) {a(n) = local(A, p, e); if( n\u003c1, 0, A = factor(n); prod( k=1, matsize(A)[1], if( p=A[k, 1], e=A[k, 2]; if( p==5, p^(3*e), (p^(3*e+3) - 1) / (p^3 - 1)))))}"
			],
			"xref": [
				"Cf. A004009"
			],
			"keyword": "nonn,mult",
			"offset": "1,2",
			"author": "_Michael Somos_, Jun 04 2013",
			"references": 1,
			"revision": 13,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2013-06-04T13:58:36-04:00"
		}
	]
}