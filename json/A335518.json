{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A335518",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 335518,
			"data": "1,1,1,3,3,3,13,13,25,13,75,75,185,213,75,541,541,1471,2719,2053,541,4683,4683,13265,32973,40367,22313,4683,47293,47293,136711,408265,713277,625295,271609,47293",
			"name": "Number of matching pairs of patterns, the first of length n and the second of length k.",
			"comment": [
				"We define a pattern to be a finite sequence covering an initial interval of positive integers. Patterns are counted by A000670 and ranked by A333217. A sequence S is said to match a pattern P if there is a not necessarily contiguous subsequence of S whose parts have the same relative order as P. For example, (3,1,1,3) matches (1,1,2), (2,1,1), and (2,1,2), but avoids (1,2,1), (1,2,2), and (2,2,1)."
			],
			"link": [
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Permutation_pattern\"\u003ePermutation pattern\u003c/a\u003e",
				"Gus Wiseman, \u003ca href=\"/A102726/a102726.txt\"\u003eSequences counting and ranking compositions by the patterns they match or avoid.\u003c/a\u003e"
			],
			"example": [
				"Triangle begins:",
				"     1",
				"     1     1",
				"     3     3     3",
				"    13    13    25    13",
				"    75    75   185   213    75",
				"   541   541  1471  2719  2053   541",
				"  4683  4683 13265 32973 40367 22313  4683",
				"Row n =2 counts the following pairs:",
				"  ()\u003c=(1,1)  (1)\u003c=(1,1)  (1,1)\u003c=(1,1)",
				"  ()\u003c=(1,2)  (1)\u003c=(1,2)  (1,2)\u003c=(1,2)",
				"  ()\u003c=(2,1)  (1)\u003c=(2,1)  (2,1)\u003c=(2,1)"
			],
			"mathematica": [
				"mstype[q_]:=q/.Table[Union[q][[i]]-\u003ei,{i,Length[Union[q]]}];",
				"allnorm[n_]:=If[n\u003c=0,{{}},Function[s,Array[Count[s,y_/;y\u003c=#]+1\u0026,n]]/@Subsets[Range[n-1]+1]];",
				"Table[Sum[Length[Union[mstype/@Subsets[y,{k}]]],{y,Join@@Permutations/@allnorm[n]}],{n,0,5},{k,0,n}]"
			],
			"xref": [
				"Columns k = 0 and k = 1 are both A000670.",
				"Row sums are A335517.",
				"Patterns are ranked by A333217.",
				"Patterns matched by a standard composition are counted by A335454.",
				"Patterns contiguously matched by compositions are counted by A335457.",
				"Minimal patterns avoided by a standard composition are counted by A335465.",
				"Patterns matched by prime indices are counted by A335549.",
				"Cf. A011782, A034691, A056986, A124771, A269134, A329744, A333257, A334299."
			],
			"keyword": "nonn,tabl,more",
			"offset": "0,4",
			"author": "_Gus Wiseman_, Jun 23 2020",
			"references": 1,
			"revision": 4,
			"time": "2020-06-24T07:22:13-04:00",
			"created": "2020-06-24T07:22:13-04:00"
		}
	]
}