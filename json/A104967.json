{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A104967",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 104967,
			"data": "1,-1,1,-1,-2,1,-1,-1,-3,1,-1,0,0,-4,1,-1,1,2,2,-5,1,-1,2,3,4,5,-6,1,-1,3,3,3,5,9,-7,1,-1,4,2,0,0,4,14,-8,1,-1,5,0,-4,-6,-6,0,20,-9,1,-1,6,-3,-8,-10,-12,-14,-8,27,-10,1,-1,7,-7,-11,-10,-10,-14,-22,-21,35,-11,1,-1,8,-12,-12,-5,0,0,-8,-27,-40,44,-12,1",
			"name": "Matrix inverse of triangle A104219, read by rows, where A104219(n,k) equals the number of Schroeder paths of length 2n having k peaks at height 1.",
			"comment": [
				"Row sums equal A090132 with odd-indexed terms negated. Absolute row sums form A104968. Row sums of squared terms gives A104969.",
				"Riordan array ((1-2*x)/(1-x), x(1-2*x)/(1-x)). - _Philippe Deléham_, Dec 05 2015"
			],
			"link": [
				"Paul D. Hanna, \u003ca href=\"/A104967/b104967.txt\"\u003eTable of n, a(n) for n = 0..1080\u003c/a\u003e"
			],
			"formula": [
				"G.f.: A(x, y) = (1-2*x)/(1-x - x*y*(1-2*x)).",
				"Sum_{k=0..n} T(n, k) = (-1)^n*A090132(n).",
				"Sum_{k=0..n} abs(T(n, k)) = A104968(n).",
				"Sum_{k=0..n} T(n, k)^2 = A104969(n).",
				"T(n,k) = Sum_{i=0..n-k} (-2)^i*binomial(k+1,i)*binomial(n-i,k). - _Vladimir Kruchinin_, Nov 02 2011",
				"Sum_{k=0..floor(n/2)} T(n-k, k) = A078011(n+2). - _G. C. Greubel_, Jun 09 2021"
			],
			"example": [
				"Triangle begins:",
				"   1;",
				"  -1,  1;",
				"  -1, -2,  1;",
				"  -1, -1, -3,  1;",
				"  -1,  0,  0, -4,  1;",
				"  -1,  1,  2,  2, -5,  1;",
				"  -1,  2,  3,  4,  5, -6,  1;",
				"  -1,  3,  3,  3,  5,  9, -7,  1;",
				"  -1,  4,  2,  0,  0,  4, 14, -8,  1;",
				"  -1,  5,  0, -4, -6, -6,  0, 20, -9, 1; ..."
			],
			"maple": [
				"A104967:= (n,k)-\u003e add( (-2)^j*binomial(k+1, j)*binomial(n-j, k), j=0..n-k);",
				"seq(seq( A104967(n,k), k=0..n), n=0..12); # _G. C. Greubel_, Jun 09 2021"
			],
			"mathematica": [
				"T[n_, k_]:= T[n, k]= Which[k==n, 1, k==0, 0, True, T[n-1, k-1] - Sum[T[n-i, k-1], {i, 2, n-k+1}]];",
				"Table[T[n, k], {n, 13}, {k, n}]//Flatten (* _Jean-François Alcover_, Jun 11 2019, after _Peter Luschny_ *)"
			],
			"program": [
				"(PARI) {T(n,k)=local(X=x+x*O(x^n),Y=y+y*O(y^k)); polcoeff(polcoeff((1-2*X)/(1-X-X*Y*(1-2*X)),n,x),k,y)}",
				"for(n=0, 16, for(k=0, n, print1(T(n, k), \", \")); print(\"\"))",
				"(Maxima) T(n,k):=sum((-2)^i*binomial(k+1,i)*binomial(n-i,k),i,0,n-k); \\\\ _Vladimir Kruchinin_, Nov 02 2011",
				"(Sage)",
				"def A104967_row(n):",
				"    @cached_function",
				"    def prec(n, k):",
				"        if k==n: return 1",
				"        if k==0: return 0",
				"        return prec(n-1,k-1)-sum(prec(n-i,k-1) for i in (2..n-k+1))",
				"    return [prec(n, k) for k in (1..n)]",
				"for n in (1..10): print(A104967_row(n)) # _Peter Luschny_, Mar 16 2016",
				"(MAGMA)",
				"A104967:= func\u003c n,k | (\u0026+[(-2)^j*Binomial(k+1, j)*Binomial(n-j, k): j in [0..n-k]]) \u003e;",
				"[A104967(n,k): k in [0..n], n in [0..12]]; // _G. C. Greubel_, Jun 09 2021"
			],
			"xref": [
				"Cf. A078011, A090132, A104968, A104969, A134824, A153881.",
				"Cf. A347171 (rows reversed, up to signs)."
			],
			"keyword": "sign,tabl",
			"offset": "0,5",
			"author": "_Paul D. Hanna_, Mar 30 2005",
			"references": 6,
			"revision": 38,
			"time": "2021-08-28T12:46:18-04:00",
			"created": "2005-04-09T03:00:00-04:00"
		}
	]
}