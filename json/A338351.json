{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A338351",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 338351,
			"data": "1,3,15,35,77,33,39,65,55,99,21,91,143,165,51,119,133,57,45,85,187,209,95,75,63,161,115,135,87,203,175,155,93,69,253,275,105,111,407,319,145,185,259,147,117,221,323,171,123,205,215,129,141,235,245,189,153,391,299,195,159,371",
			"name": "Lexicographically earliest infinite sequence {a(n)} of distinct odd positive numbers such that, for n\u003e2, a(n) has a common factor with a(n-1) but not with a(n-2).",
			"comment": [
				"A version of A336957 defined just on the odd numbers.",
				"Let Ker(k), the kernel of k, denote the set of primes dividing k. Thus Ker(36) = {2,3}, Ker(1) = {}. Then Product_{p in Ker(k)} p = A000265(k), which is denoted by ker(k).",
				"Theorem 1: For n\u003e2, a(n) is the smallest odd number m not yet in the sequence such that",
				"(i) Ker(m) intersect Ker(a(n-1)) is nonempty,",
				"(ii) Ker(m) intersect Ker(a(n-2)) is empty, and",
				"(iii) The set Ker(m) \\ Ker(a(n-1)) is nonempty."
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/A338351/b338351.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"maple": [
				"with(numtheory);",
				"N:= 10^3: # to get a(1) to a(n) where a(n+1) is the first term \u003e N",
				"B:= Vector(N, datatype=integer[1]):",
				"A[1]:=1; A[2]:=3;",
				"for n from 3 do",
				"  for k from 5 to N by 2 do",
				"    if B[k] = 0 and igcd(k, A[n-1]) \u003e 1 and igcd(k, A[n-2]) = 1 then",
				"          if nops(factorset(k) minus factorset(A[n-1])) \u003e 0 then",
				"       A[n]:= k;",
				"       B[k]:= 1;",
				"       break;",
				"          fi;",
				"    fi;",
				"  od:",
				"  if k \u003e N then break; fi;",
				"od:",
				"s1:=[seq(A[i], i=1..n-1)];"
			],
			"program": [
				"(PARI) a338351(upto)={my(v=[1,3]);for(n=1,upto,forstep(k=5,oo,2,if(!vecsearch(vecsort(v),k),if(gcd(k,v[#v])\u003e1\u0026\u0026gcd(k,v[#v-1])==1,if(#setminus(Set(factor(k)[,1]),Set(factor(v[#v])[,1]))\u003e0,v=concat(v,[k]);break)))));v};",
				"a338351(60) \\\\ _Hugo Pfoertner_, Oct 30 2020"
			],
			"xref": [
				"Cf. A000265, A336957."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, Oct 30 2020",
			"references": 2,
			"revision": 20,
			"time": "2021-01-03T04:47:48-05:00",
			"created": "2020-10-30T08:08:14-04:00"
		}
	]
}