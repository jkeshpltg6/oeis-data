{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003775",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3775,
			"data": "1,8,95,1183,14824,185921,2332097,29253160,366944287,4602858719,57737128904,724240365697,9084693297025,113956161827912,1429438110270431,17930520634652959,224916047725262248,2821291671062267585,35389589910135145793,443918325373278904936",
			"name": "Number of perfect matchings (or domino tilings) in P_5 X P_2n.",
			"reference": [
				"F. Faase, On the number of specific spanning subgraphs of the graphs G X P_n, Ars Combin. 49 (1998), 129-154.",
				"R. P. Stanley, Enumerative Combinatorics I, p. 292."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A003775/b003775.txt\"\u003eTable of n, a(n) for n = 0..300\u003c/a\u003e",
				"F. Faase, \u003ca href=\"http://www.iwriteiam.nl/Cpaper.pdf\"\u003eOn the number of specific spanning subgraphs of the graphs G X P_n\u003c/a\u003e, Preliminary version of paper that appeared in Ars Combin. 49 (1998), 129-154.",
				"F. Faase, \u003ca href=\"http://www.iwriteiam.nl/counting.html\"\u003eCounting Hamiltonian cycles in product graphs\u003c/a\u003e",
				"F. Faase, \u003ca href=\"http://www.iwriteiam.nl/Cresults.html\"\u003eResults from the counting program\u003c/a\u003e",
				"David Klarner and Jordan Pollack, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(80)90098-9\"\u003eDomino tilings of rectangles with fixed width\u003c/a\u003e, Disc. Math. 32 (1980) 45-52.",
				"R. J. Mathar, \u003ca href=\"http://arxiv.org/abs/1311.6135\"\u003ePaving rectangular regions with rectangular tiles: tatami and non-tatami tilings\u003c/a\u003e, arXiv:1311.6135 [math.CO], 2013, Table 4.",
				"James A. Sellers, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL5/Sellers/sellers4.html\"\u003eDomino Tilings and Products of Fibonacci and Pell Numbers\u003c/a\u003e, Journal of Integer Sequences, Vol. 5 (2002), Article 02.1.2.",
				"Thotsaporn ”Aek” Thanatipanonda, \u003ca href=\"https://www.fq.math.ca/Papers1/57-5/thanatipanonda.pdf\"\u003eStatistics of Domino Tilings on a Rectangular Board\u003c/a\u003e, Fibonacci Quart. 57 (2019), no. 5, 145-153. See p. 151.",
				"\u003ca href=\"/index/Do#domino\"\u003eIndex entries for sequences related to dominoes\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (15,-32,15,-1)."
			],
			"formula": [
				"If b(n) denotes the number of perfect matchings (or domino tilings) in P_5 X P_n we have:",
				"b(1) = 0,",
				"b(2) = 8,",
				"b(3) = 0,",
				"b(4) = 95,",
				"b(5) = 0,",
				"b(6) = 1183,",
				"b(7) = 0,",
				"b(8) = 14824 and",
				"b(n) = 15b(n-2) - 32b(n-4) + 15b(n-6) - b(n-8).",
				"G.f.: (1-x)*(1 - 6*x + x^2)/(1 - 15*x + 32*x^2 - 15*x^3 + x^4).",
				"Let M be the 4 X 4 matrix |1 0 2 8 | 0 1 0 2 | 2 1 5 21| 1 1 1 8 |; then a(n) = M^n(4, 4). - _Philippe Deléham_, Aug 08 2003",
				"Limit_{n -\u003e infinity} a(n)/a(n-1) = (3 + sqrt(5))*(5 + sqrt(21))/4 = 12.54375443458... - _Philippe Deléham_, Jun 13 2005",
				"a(n) = ((35 + 7*sqrt(5) + 5*sqrt(21) + sqrt(105))*((3+sqrt(5))*(5+sqrt(21))/4)^n + (35 - 7*sqrt(5) + 5*sqrt(21) - sqrt(105))*((3-sqrt(5))*(5+sqrt(21))/4)^n + (35 + 7*sqrt(5) - 5*sqrt(21) - sqrt(105))*((3+sqrt(5))*(5-sqrt(21))/4)^n + (35 - 7*sqrt(5) - 5*sqrt(21) + sqrt(105))*((3-sqrt(5))*(5-sqrt(21))/4)^n)/140. - _Tim Monahan_, Aug 13 2011"
			],
			"maple": [
				"a:= n-\u003e (\u003c\u003c15|-32|15|-1\u003e, \u003c1|0|0|0\u003e, \u003c0|1|0|0\u003e, \u003c0|0|1|0\u003e\u003e^n. \u003c\u003c8, 1, 1, 8\u003e\u003e)[2, 1]: seq(a(n), n=0..20);  # _Alois P. Heinz_, Sep 24 2011"
			],
			"mathematica": [
				"a = 3; b = 5; c = 7; d = a*c; e = b*c; g = a*b*c; f[n_] := Simplify[((e + c*Sqrt[b] + b*Sqrt[d] + Sqrt[g])*((a + Sqrt[b])*(b + Sqrt[d])/4)^n + (e - c*Sqrt[b] + b*Sqrt[d] - Sqrt[g])*((a - Sqrt[b])*(b + Sqrt[d])/4)^n + (e + c*Sqrt[b] - b*Sqrt[d] - Sqrt[g])*((a + Sqrt[b])*(b - Sqrt[d])/4)^n + (e - c*Sqrt[b] - 5*Sqrt[d] + Sqrt[g])*((a - Sqrt[b])*(b - Sqrt[d])/4)^n)/ 140]; Array[f, 17, 0] (* _Robert G. Wilson v_, Aug 13 2011 *)",
				"a[n_] := (MatrixPower[{{15, -32, 15, -1}, {1, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 1, 0}}, n].{8, 1, 1, 8})[[2]]; Table[a[n], {n, 0, 20}] (* _Jean-François Alcover_, Jan 31 2016, after _Alois P. Heinz_ *)",
				"a[0] = 1; a[n_] := Product[2(2+Cos[j Pi/3]+Cos[2k Pi/(2n+1)]), {k, 1, n}, {j, 1, 2}] // Round;",
				"Table[a[n], {n, 0, 19}] (* _Jean-François Alcover_, Aug 20 2018 *)",
				"LinearRecurrence[{15, -32, 15, -1}, {1, 8, 95, 1183}, 20] (* _Vincenzo Librandi_, Aug 20 2018 *)"
			],
			"program": [
				"(MAGMA) I:=[1,8,95,1183]; [n le 4 select I[n] else 15*Self(n-1)-32*Self(n-2)+15*Self(n-3)-Self(n-4): n in [1..30]]; // _Vincenzo Librandi_, Aug 20 2018"
			],
			"xref": [
				"Row 5 of array A099390.",
				"Bisection of A189003."
			],
			"keyword": "nonn,changed",
			"offset": "0,2",
			"author": "_Frans J. Faase_",
			"ext": [
				"Recurrence from Faase's web page added by _N. J. A. Sloane_, Feb 03 2009"
			],
			"references": 8,
			"revision": 80,
			"time": "2022-01-15T09:49:42-05:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}