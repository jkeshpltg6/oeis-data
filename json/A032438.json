{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A032438",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 32438,
			"data": "0,0,3,5,12,16,27,33,48,56,75,85,108,120,147,161,192,208,243,261,300,320,363,385,432,456,507,533,588,616,675,705,768,800,867,901,972,1008,1083,1121,1200,1240,1323,1365,1452,1496,1587,1633,1728,1776,1875,1925",
			"name": "a(n) = n^2 - floor((n+1)/2)^2.",
			"comment": [
				"The answer to a question from Mike and Laurie Crain (2crains(AT)concentric.net): how many even numbers are there in an n X n multiplication table starting at 1 X 1?",
				"a(n+1) is the number of pairs (x,y) with x and y in {0,...,n}, x and y of the same parity, and x+y \u003e= n. - _Clark Kimberling_, Jul 02 2012",
				"From _J. M. Bergot_, Aug 08 2013: (Start)",
				"Define a triangle to have T(1,1)=0 and T(n,c) = n^2 - c^2. The difference of the sum of the terms in antidiagonal(n+1) and those in antidiagonal(n)=a(n).",
				"Column 0 is vertical and T(n,n)=0. The first few rows are 0; 3,0; 8,5,0; 15,12,7,0; 24,21,16,9,0; 35,32,27,20,11,0; the first few antidiagonals are 0; 3; 8,0; 15,5; 24,12,0; 35,21,7; 48,32,16,0; the first few sum of terms in the antidiagonals are 0, 3, 8, 20, 36, 63, 96, 144, 200, 275, 360, 468, 588, 735, 896, 1088, 1296, 1539. (End)",
				"Sum of the largest parts in the partitions of 2n into two distinct odd parts. For example, a(5) = 16; the partitions of 2(5) = 10 into two distinct odd parts are (9,1) and (7,3). The sum of the largest parts is then 9+7 = 16. - _Wesley Ivan Hurt_, Nov 27 2017"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A032438/b032438.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,2,-2,-1,1)."
			],
			"formula": [
				"a(n) = n^2 - A008794(n+1).",
				"G.f.: x^2*(x^2 + 2*x + 3)/(1-x^2)^2/(1-x). - _Ralf Stephan_, Jun 10 2003",
				"a(n) = (1/8)*(2*n*(3*n-1)+(2*n+1)*(-1)^n-1). a(-n-1) = A014255(n). - _Bruno Berselli_, Sep 27 2011",
				"a(n) = a(n-1) + 2*a(n-2) - 2*a(n-3) - a(n-4) + a(n-5), n \u003e 4. - _Harvey P. Dale_, Nov 24 2011"
			],
			"maple": [
				"A032438:=n-\u003en^2-floor((n+1)/2)^2; seq(A032438(n), n=0..100) # _Wesley Ivan Hurt_, Nov 25 2013"
			],
			"mathematica": [
				"Table[n^2-Floor[((n+1)/2)]^2,{n,0,50}] (* or *) LinearRecurrence[ {1,2,-2,-1,1},{0,0,3,5,12},51]"
			],
			"program": [
				"(MAGMA) [n^2-Floor( (n+1)/2 )^2 : n in [0..60]]; // _Vincenzo Librandi_, Sep 27 2011",
				"(PARI) a(n)=n^2 - ((n+1)\\2)^2 \\\\ _Charles R Greathouse IV_, Feb 19 2017"
			],
			"xref": [
				"First differences are in A059029, partial sums in A143785.",
				"Cf. A008794, A014255."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"references": 4,
			"revision": 53,
			"time": "2017-12-05T04:03:51-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}