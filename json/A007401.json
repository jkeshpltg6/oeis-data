{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007401",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7401,
			"id": "M2316",
			"data": "1,3,4,6,7,8,10,11,12,13,15,16,17,18,19,21,22,23,24,25,26,28,29,30,31,32,33,34,36,37,38,39,40,41,42,43,45,46,47,48,49,50,51,52,53,55,56,57,58,59,60,61,62,63,64,66,67,68,69,70,71,72,73,74,75,76",
			"name": "Add n-1 to n-th term of 'n appears n times' sequence (A002024).",
			"comment": [
				"Complement of A000096 = increasing sequence of positive integers excluding n*(n+3)/2. - _Jonathan Vos Post_, Aug 13 2005",
				"As a triangle: (1; 3,4; 6,7,8; 10,11,12,13; ...), Row sums = A127736: (1, 7, 21, 46, 85, 141, 217, ...). - _Gary W. Adamson_, Oct 25 2007",
				"Odd primes are a subsequence except 5, cf. A004139. - _Reinhard Zumkeller_, Jul 18 2011",
				"A023532(a(n)) = 1. - _Reinhard Zumkeller_, Dec 04 2012",
				"T(n,k) = ((n+k)^2+n-k)/2 - 1, n,k \u003e 0, read by antidiagonals. - _Boris Putievskiy_, Jan 14 2013",
				"A023531(a(n)) = 0. - _Reinhard Zumkeller_, Feb 14 2015"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A007401/b007401.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Boris Putievskiy, \u003ca href=\"http://arxiv.org/abs/1212.2732\"\u003eTransformations [of] Integer Sequences And Pairing Functions\u003c/a\u003e arXiv:1212.2732 [math.CO], 2012.",
				"J. Riordan, \u003ca href=\"/A007401/a007401_8.pdf\"\u003eThe enumeration of trees by height and diameter\u003c/a\u003e, IBM Journal 4 (1960), 473-478. (Annotated scanned copy)",
				"T. R. S. Walsh \u0026 N. J. A. Sloane, \u003ca href=\"/A007401/a007401_5.pdf\"\u003eCorrespondence, 1991\u003c/a\u003e",
				"T. R. S. Walsh, \u003ca href=\"/A007401/a007401_2.pdf\"\u003eData (Preprint 1, Part 1)\u003c/a\u003e",
				"T. R. S. Walsh, \u003ca href=\"/A007401/a007401_3.pdf\"\u003eData (Preprint 1, Part 2)\u003c/a\u003e",
				"T. R. S. Walsh, \u003ca href=\"/A007401/a007401_4.pdf\"\u003eData (Preprint 1, Part 3)\u003c/a\u003e",
				"T. R. S. Walsh, \u003ca href=\"/A007401/a007401_1.pdf\"\u003eNotes\u003c/a\u003e",
				"T. R. S. Walsh, \u003ca href=\"/A007401/a007401.pdf\"\u003eNumber of sensed planar maps with n edges and m vertices\u003c/a\u003e",
				"N. C. Wormald, \u003ca href=\"/A007401/a007401_6.pdf\"\u003eOn the number of planar maps\u003c/a\u003e, Can. J. Math. 33.1 (1981), 1-11. (Annotated scanned copy)"
			],
			"formula": [
				"From _Boris Putievskiy_, Jan 14 2013: (Start)",
				"a(n) = A014132(n) - 1.",
				"a(n) = A003057(n)^2 - A114327(n) - 1.",
				"a(n) = ((t+2)^2 + i - j)/2-1, where",
				"i = n-t*(t+1)/2,",
				"j = (t*t+3*t+4)/2-n,",
				"t = floor((-1+sqrt(8*n-7))/2). (End)"
			],
			"example": [
				"From _Boris Putievskiy_, Jan 14 2013: (Start)",
				"The start of the sequence as table:",
				"   1,  3,  6, 10, 15, 21, 28, ...",
				"   4,  7, 11, 16, 22, 29, 37, ...",
				"   8, 12, 17, 23, 30, 38, 47, ...",
				"  13, 18, 24, 31, 39, 48, 58, ...",
				"  19, 25, 32, 40, 49, 59, 70, ...",
				"  26, 33, 41, 50, 60, 71, 83, ...",
				"  34, 42, 51, 61, 72, 84, 97, ...",
				"  ...",
				"The start of the sequence as triangle array read by rows:",
				"   1;",
				"   3,  4;",
				"   6,  7,  8;",
				"  10, 11, 12, 13;",
				"  15, 16, 17, 18, 19;",
				"  21, 22, 23, 24, 25, 26;",
				"  28, 29, 30, 31, 32, 33, 34;",
				"  ...",
				"Row number r contains r numbers r*(r+1)/2, r*(r+1)/2+1, ..., r*(r+1)/2+r-1. (End)"
			],
			"mathematica": [
				"f[n_] := n + Floor[ Sqrt[2n] - 1/2]; Array[f, 66]; (* _Robert G. Wilson v_, Feb 13 2011 *)"
			],
			"program": [
				"(PARI) a(n)=n+floor(sqrt(n+n)-1/2) \\\\ _Charles R Greathouse IV_, Feb 13 2011",
				"(PARI) for(m=1,9, for(n=m*(m+1)/2,(m^2+3*m-2)/2, print1(n\", \"))) \\\\ _Charles R Greathouse IV_, Feb 13 2011",
				"(Haskell)",
				"a007401 n = a007401_list !! n",
				"a007701_list = [x | x \u003c- [0..], a023531 x == 0]",
				"-- _Reinhard Zumkeller_, Feb 14 2015, Dec 04 2012"
			],
			"xref": [
				"Cf. A002024, A000096, A127736, A014132, A002260, A004736, A003057, A114327, A023531."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, _Mira Bernstein_",
			"references": 41,
			"revision": 95,
			"time": "2018-02-09T21:46:01-05:00",
			"created": "1994-05-12T03:00:00-04:00"
		}
	]
}