{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A094202",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 94202,
			"data": "0,1,4,6,9,12,14,22,27,33,35,51,56,64,74,80,88,90,116,127,145,158,174,184,197,203,216,232,234,276,294,326,368,378,399,425,441,462,472,493,519,525,546,572,588,609,611,679,708,760,828,847,915,944,988,1022,1064,1090",
			"name": "Integers k whose Zeckendorf representation A014417(k) is palindromic.",
			"reference": [
				"C. G. Lekkerkerker, Voorstelling van natuurlijke getallen door een som van getallen van Fibonacci, Simon Stevin vol. 29, 1952, pages 190-195.",
				"E. Zeckendorf, Représentation des nombres naturels par une somme de nombres de Fibonacci ou de nombres de Lucas, Bulletin de la Société Royale des Sciences de Liège vol. 41 (1972) pages 179-182."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A094202/b094202.txt\"\u003eTable of n, a(n) for n = 1..20000\u003c/a\u003e (first 129 terms from Indranil Ghosh)",
				"Ron Knott, \u003ca href=\"http://www.maths.surrey.ac.uk/hosted-sites/R.Knott/Fibonacci/fibrep.html\"\u003eFibonacci Bases\u003c/a\u003e."
			],
			"example": [
				"Fibonacci base columns are ...,8,5,3,2,1 with column entries 0 or 1 and no two consecutive ones (the Zeckendorf representation) so that each n has a unique representation.",
				"12 is in the sequence because 12 = 8 + 3 + 1 = 10101 base Fib; 14 = 13 + 1 = 100001 base Fib."
			],
			"mathematica": [
				"zeck[n_Integer] := Block[{k = Ceiling[ Log[ GoldenRatio, n*Sqrt[5]]], t = n, fr = {}}, While[k \u003e 1, If[t \u003e= Fibonacci[k], AppendTo[ fr, 1]; t = t - Fibonacci[k], AppendTo[fr, 0]]; k-- ]; FromDigits[fr]]; a = {}; Do[z = zeck[n]; If[ FromDigits[ Reverse[ IntegerDigits[z]]] == z, AppendTo[a, n]], {n, 1123}]; a (* _Robert G. Wilson v_, May 29 2004 *)",
				"mirror[dig_, s_] := Join[dig, s, Reverse[dig]]; select[v_, mid_] := Select[v, Length[#] == 0 || Last[#] != mid \u0026]; fib[dig_] := Plus @@ (dig * Fibonacci[Range[2, Length[dig] + 1]]); pals = Rest[IntegerDigits /@ FromDigits /@ Select[Tuples[{0, 1}, 7], SequenceCount[#, {1, 1}] == 0 \u0026]]; Union@Join[{0, 1}, fib /@ Join[mirror[#, {}] \u0026 /@ (select[pals, 1]), mirror[#, {1}] \u0026 /@ (select[pals, 1]), mirror[#, {0}] \u0026 /@ pals]] (* _Amiram Eldar_, Jan 11 2020 *)"
			],
			"program": [
				"(Python)",
				"from sympy import fibonacci",
				"def a(n):",
				"    k=0",
				"    x=0",
				"    while n\u003e0:",
				"        k=0",
				"        while fibonacci(k)\u003c=n: k+=1",
				"        x+=10**(k - 3)",
				"        n-=fibonacci(k - 1)",
				"    return x",
				"def ok(n):",
				"    x=str(a(n))",
				"    return x==x[::-1]",
				"print([n for n in range(1101) if ok(n)]) # _Indranil Ghosh_, Jun 07 2017"
			],
			"xref": [
				"Cf. A014417, A035517.",
				"Gives the positions of zeros in A095734. Subsets: A095730, A048757. A006995 gives the integers whose binary expansion is palindromic."
			],
			"keyword": "nonn,base",
			"offset": "1,3",
			"author": "_Ron Knott_, May 25 2004",
			"ext": [
				"More terms from _Robert G. Wilson v_, May 28 2004",
				"Offset changed to 1 by _Alois P. Heinz_, Aug 02 2017"
			],
			"references": 16,
			"revision": 39,
			"time": "2021-03-22T03:41:49-04:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}