{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A262057",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 262057,
			"data": "0,2,1,7,5,3,21,8,6,4,23,22,16,11,9,64,26,24,17,14,10,69,65,50,25,19,15,12,71,70,67,53,48,20,18,13,193,80,78,68,59,49,34,29,27,207,194,152,79,73,62,51,35,32,28,209,208,196,161,150,74,63,52,43,33,30",
			"name": "Array based on the Stanley sequence S(0), A005836, by antidiagonals.",
			"comment": [
				"This array is similar to a dispersion in that the first column is the minimal nonnegative sequence that contains no 3-term arithmetic progression, and each next column is the minimal sequence consisting of the numbers rejected from the previous column that contains no 3-term arithmetic progression.",
				"A100480(n) describes which column n is sorted into.",
				"The columns of the array form the greedy partition of the nonnegative integers into sequences that contain no 3-term arithmetic progression. - _Robert Israel_, Feb 03 2016"
			],
			"link": [
				"Max Barrentine and Robert Israel, \u003ca href=\"/A262057/b262057.txt\"\u003eTable of n, a(n) for n = 1..10011\u003c/a\u003e (first 141 antidiagonals, flattened; n=1..77 from Max Barrentine)"
			],
			"example": [
				"From the top-left corner, this array starts:",
				"   0   2   7  21  23  64",
				"   1   5   8  22  26  65",
				"   3   6  16  24  50  67",
				"   4  11  17  25  53  68",
				"   9  14  19  48  59  73",
				"  10  15  20  49  62  74"
			],
			"maple": [
				"M:= 20: # to get the first M antidiagonals",
				"for i from 1 to M do B[i]:= {}: F[i]:= {}: od:",
				"countdowns:= Vector(M,j-\u003eM+1-j):",
				"for x from 0 while max(countdowns) \u003e 0 do",
				"  for i from 1 do",
				"     if not member(x, F[i]) then",
				"       F[i]:= F[i] union map(y -\u003e 2*x-y, B[i]);",
				"       B[i]:= B[i] union {x};",
				"       countdowns[i]:= countdowns[i] - 1;",
				"     break",
				"    fi",
				"  od;",
				"od:",
				"seq(seq(B[n+1-i][i], i=1..n),n=1..M); # _Robert Israel_, Feb 03 2016"
			],
			"program": [
				"(MATLAB)",
				"function  A = A262057( M, N )",
				"% to get first M antidiagonals using x up to N",
				"B = cell(1,M);",
				"F = zeros(M,N+1);",
				"countdowns = [M:-1:1];",
				"for x=0:N",
				"    if max(countdowns) == 0",
				"        break",
				"    end",
				"    for i=1:M",
				"        if F(i,x+1) == 0",
				"            newforb = 2*x - B{i};",
				"            newforb = newforb(newforb \u003c= N \u0026 newforb \u003e= 1);",
				"            F(i,newforb+1) = 1;",
				"            B{i}(end+1) = x;",
				"            countdowns(i) = countdowns(i)-1;",
				"            break",
				"        end",
				"    end",
				"end",
				"if max(countdowns) \u003e 0",
				"    [~,jmax] = max(countdowns);",
				"    jmax = jmax(1);",
				"    error ('Need larger N: B{%d} has only %d elements',jmax,numel(B{jmax}));",
				"end",
				"A = zeros(1,M*(M+1)/2);",
				"k = 0;",
				"for n=1:M",
				"    for i=1:n",
				"        k=k+1;",
				"        A(k) = B{n+1-i}(i);",
				"    end",
				"end",
				"end % _Robert Israel_, Feb 03 2016"
			],
			"xref": [
				"First column is A005836.",
				"First row is A265316.",
				"Cf. A074940, A100480."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Max Barrentine_, Nov 29 2015",
			"references": 3,
			"revision": 51,
			"time": "2016-04-25T12:05:03-04:00",
			"created": "2015-12-07T11:13:50-05:00"
		}
	]
}