{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A324251",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 324251,
			"data": "-2,2,-1,2,-4,4,-2,4,-1,1,-1,4,-1,4,-6,6,-3,6,-2,6,-1,1,-1,1,-6,1,-1,1,-1,6,-1,2,-1,6,-1,6,-8,8,-4,8,-2,1,-3,1,-2,8,-2,8,-1,1,-2,1,-1,8,-1,2,-4,2,-1,8,-1,3,-1,8,-1,8,-10,10,-5,10,-3,2,-3,10,-2,1,-1,2,-10,2,-1,1,-2,10,-2,10",
			"name": "Irregular triangle read by rows: parameters of the principal cycle of discriminant 4*D(n), with D(n) = A000037(n).",
			"comment": [
				"The row length of this irregular triangle is 2*A307372(n).",
				"The indefinite binary quadratic Pell form is F = [1, 0, -D(n)], with D(n) = A000037(n) (D not a square). This form is not reduced (see the Buell or Scholz-Schoeneberg references, and the W. Lang link in A225953 for the definition).",
				"The first reduced form, obtained after two equivalence transformations, is FR(n) = [1, 2*s(n), -(D(n) - s(n)^2)] where s(n) = A000194(n) = D(n) - n, for n \u003e= 1. Hence FR(n) =  [1, 2*s(n), -(n - s(n)*(s(n)-1))]. For the two transformations invoving R(t) = matrix([0, -1], [1, t]), first with t = 0 then with t = s(n) see a comment in A000194, and the proposition in the W. Lang link given below. FR(n) is the principal form F_p(n) of discriminant 4*D(n).",
				"Each reduced form FR(n) leads to a cycle of reduced forms with (primitive) period P(n) = 2*p(n) = 2*A307372(n). The sequence of R-transformations is given by the parameter tuple (t_1(n), ..., t_{2*p(n)}(n)) with alternating signs which give the row entries T(n, k) =  t_k(n). See also Table 2 of the W. Lang link.",
				"The automorphic transformation is obtained by the matrix Auto(n) = R(t_1(n))*R(t_2(n))*...*R(t_{2*p(n)}(n)). Together with the matrix B(n) := R(0)*R(s(n)) = Matrix([-1, s(n)], [0, -1]) one finds all solutions of the Pell equation x^2 - D(n)*y^2 = +1. For each n \u003e= 1 there is one family (also called class) of proper solutions. The general solution is (x(n;j), y(n;j))^T  = B(n)*(Auto(n))^j*(1,0)^T, for integer j (T for transposed). One can always choose x \u003e= 1 by an overall sign flip in x and y.",
				"For the general Pell equation x^2 - D(n)*y^2 = N, for integer N, the parallel forms equivalent to FR(n) become important. For details see the W. Lang link given below, section 3."
			],
			"reference": [
				"D. A. Buell, Binary Quadratic Forms. Springer-Verlag, NY, 1989, p. 21.",
				"A. Scholz and B. Schoeneberg, Einführung in die Zahlentheorie, 5. Aufl., de Gruyter, Berlin, New York, 1973, p. 112."
			],
			"link": [
				"Wolfdieter Lang, \u003ca href=\"/A324251/a324251_2.pdf\"\u003eCycles of reduced Pell forms, general Pell equations and Pell graphs\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = t_k(n), the k-th enry of the t-tuple for the R-transformations of the principal cycle for discriminant 4*D(n), with D(n) = A000037(n). See the comments above."
			],
			"example": [
				"The irregular triangle T(n, k) begins:",
				"n,  D(n) \\k   1   2   3   4   5   6   7   8   9  10 ...   2*A324252(n)",
				"----------------------------------------------------------------------",
				"1,   2:      -2   2                                           2",
				"2,   3:      -1   2                                           2",
				"3,   5:      -4   4                                           2",
				"4,   6:      -2   4                                           2",
				"5,   7:      -1   1  -1   4                                   4",
				"6,   8:      -1   4                                           2",
				"7,  10:      -6   6                                           2",
				"8,  11:      -3   6                                           2",
				"9,  12:      -2   6                                           2",
				"10, 13:      -1   1  -1   1  -6   1  -1   1  -1   6          10",
				"11, 14:      -1   2  -1   6                                   4",
				"12, 15:      -1   6                                           2",
				"13, 17:      -8   8                                           2",
				"14, 18:      -4   8                                           2",
				"15, 19:      -2   1  -3   1  -2   8                           6",
				"16, 20:      -2   8                                           2",
				"17, 21:      -1   1  -2   1  -1   8                           6",
				"18, 22:      -1   2  -4   2  -1   8                           6",
				"19, 23:      -1   3  -1   8                                   4",
				"20, 24:      -1   8                                           2",
				"...",
				"--------------------------------------------------------------------",
				"The  forms for the cycle CR(5) for D(5) = 7 (discriminant 28) are:",
				"FR(5) = [1, 4, -3], the transformation wth  R(-1) produces FR1(5) = [-3, 2, 2], from this R(1) leads to FR2(5) = [2, 2, -3], then with R(-1) to FR3(5) = [-3, 4, 1], and with R(4) back to FR(5)."
			],
			"xref": [
				"Cf. A000037, A000194, A225953, A307372."
			],
			"keyword": "sign,tabf",
			"offset": "1,1",
			"author": "_Wolfdieter Lang_, Apr 19 2019",
			"references": 12,
			"revision": 13,
			"time": "2020-12-16T15:39:55-05:00",
			"created": "2019-04-19T10:16:07-04:00"
		}
	]
}