{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A238016",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 238016,
			"data": "0,1,1,1,1,1,1,1,2,1,1,1,3,3,1,1,1,5,12,5,1,1,1,9,75,64,7,1,1,1,17,588,2280,377,11,1,1,1,33,5043,123464,106852,2432,15,1,1,1,65,44652,7566280,55567352,6889527,16475,22,1",
			"name": "Number A(n,k) of partitions of n^k into parts that are at most n; square array A(n,k), n\u003e=0, k\u003e=0, read by antidiagonals.",
			"comment": [
				"In general, for k\u003e3, is column k asymptotic to exp(2*n) * n^((k-2)*n-k) / (2*Pi). For k=1 see A000041, for k=2 see A206226 and for k=3 see A238608. - _Vaclav Kotesovec_, May 25 2015",
				"Conjecture: If f(n) \u003e= O(n^4) then \"number of partitions of f(n) into parts that are at most n\" is asymptotic to f(n)^(n-1) / (n!*(n-1)!). See also A237998, A238000, A236810 or A258668-A258672. - _Vaclav Kotesovec_, Jun 07 2015"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A238016/b238016.txt\"\u003eAntidiagonals n = 0..54, flattened\u003c/a\u003e",
				"A. V. Sills and D. Zeilberger, \u003ca href=\"https://arxiv.org/abs/1108.4391\"\u003eFormulae for the number of partitions of n into at most m parts (using the quasi-polynomial ansatz)\u003c/a\u003e arXiv:1108.4391 [math.CO], 2011."
			],
			"formula": [
				"A(n,k) = [x^(n^k)] Product_{j=1..n} 1/(1-x^j)."
			],
			"example": [
				"A(3,1) = 3: 3, 21, 111.",
				"A(3,2) = 12: 333, 3222, 3321, 22221, 32211, 33111, 222111, 321111, 2211111, 3111111, 21111111, 111111111.",
				"A(2,3) = 5: 2222, 22211, 221111, 2111111, 11111111.",
				"A(2,4) = 9: 22222222, 222222211, 2222221111, 22222111111, 222211111111, 2221111111111, 22111111111111, 211111111111111, 1111111111111111.",
				"Square array A(n,k) begins:",
				"  0, 1,   1,      1,        1,           1, ...",
				"  1, 1,   1,      1,        1,           1, ...",
				"  1, 2,   3,      5,        9,          17, ...",
				"  1, 3,  12,     75,      588,        5043, ...",
				"  1, 5,  64,   2280,   123464,     7566280, ...",
				"  1, 7, 377, 106852, 55567352, 33432635477, ..."
			],
			"mathematica": [
				"A[n_, k_] := SeriesCoefficient[Product[1/(1-x^j), {j, 1, n}], {x, 0, n^k}]; A[0, 0] = 0; Table[A[n-k, k], {n, 0, 10}, {k, n, 0, -1}] // Flatten (* _Jean-François Alcover_, Oct 11 2015 *)"
			],
			"xref": [
				"Columns k=0-10 give: A057427, A000041, A206226, A238608, A238609, A238610, A238611, A238612, A238613, A238614, A238615.",
				"Rows n=0-10 give: A057427, A000012, A094373, A238630, A238631, A238632, A238633, A238634, A238635, A238636, A238637.",
				"Main diagonal gives A238000.",
				"Cf. A238010."
			],
			"keyword": "nonn,tabl",
			"offset": "0,9",
			"author": "_Alois P. Heinz_, Feb 17 2014",
			"references": 27,
			"revision": 35,
			"time": "2018-08-29T21:13:13-04:00",
			"created": "2014-02-17T07:04:59-05:00"
		}
	]
}