{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A324989",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 324989,
			"data": "1,2,3,4,5,7,11,22,101,111,121,131,151,181,191,202,313,353,373,383,727,757,787,797,919,929,1001,1111,10001,10201,10301,10501,10601,11111,11311,11411,12421,12721,12821,13331,13831,13931,14341,14741,15451,15551,16061",
			"name": "Palindromes whose product of divisors is palindromic.",
			"comment": [
				"Numbers m such that m and A007955(m) = pod(m) are both in A002113.",
				"Of 48025 terms \u003c 10^11, all but 30 are prime. - _Robert Israel_, Apr 23 2019"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A324989/b324989.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"Product of divisors of palindrome number 22 with divisors 1, 2, 11 and 22 is 484 (palindrome number)."
			],
			"maple": [
				"revdigs:= proc(n)",
				"local L, nL, i;",
				"L:= convert(n, base, 10);",
				"nL:= nops(L);",
				"add(L[i]*10^(nL-i), i=1..nL);",
				"end:",
				"pals:= proc(d) local x, y;",
				"  if d::even then [seq(x*10^(d/2)+revdigs(x), x=10^(d/2-1)..10^(d/2)-1)]",
				"  else [seq(seq(x*10^((d+1)/2)+y*10^((d-1)/2)+revdigs(x), y=0..9), x=10^((d-1)/2-1)..10^((d-1)/2)-1)]",
				"  fi",
				"end proc:",
				"pals(1):= [$1..9]:",
				"filter:= proc(n) local v;",
				"  v:= convert(numtheory:-divisors(n),`*`);",
				"  revdigs(v)=v",
				"end proc:",
				"seq(op(select(filter, pals(d))),d=1..5); # _Robert Israel_, Apr 23 2019"
			],
			"mathematica": [
				"Select[Range[10^5], And[PalindromeQ@ #, PalindromeQ[Times @@ Divisors@ #]] \u0026] (* _Michael De Vlieger_, Mar 24 2019 *)",
				"Select[Range[17000],AllTrue[{#,Times@@Divisors[#]},PalindromeQ]\u0026] (* _Harvey P. Dale_, Oct 13 2021 *)"
			],
			"program": [
				"(MAGMA)[n: n in [1..100000] | Intseq(n, 10) eq Reverse(Intseq(n, 10)) and Intseq(\u0026*[d: d in Divisors(n)], 10) eq Reverse(Intseq(\u0026*[d: d in Divisors(n)], 10))]",
				"(PARI) ispal(n) = my(d=digits(n)); Vecrev(d) == d;",
				"isok(n) = ispal(n) \u0026\u0026 ispal(vecprod(divisors(n))); \\\\ _Michel Marcus_, Mar 23 2019"
			],
			"xref": [
				"Cf. A007955, A002113, A069747.",
				"Includes A002385.",
				"Similar sequences for functions sigma(m) and tau(m): A028986, A324988."
			],
			"keyword": "nonn,base",
			"offset": "1,2",
			"author": "_Jaroslav Krizek_, Mar 23 2019",
			"references": 3,
			"revision": 21,
			"time": "2021-10-13T12:18:50-04:00",
			"created": "2019-04-22T15:33:03-04:00"
		}
	]
}