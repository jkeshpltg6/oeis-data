{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A025426",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 25426,
			"data": "0,0,1,0,0,1,0,0,1,0,1,0,0,1,0,0,0,1,1,0,1,0,0,0,0,1,1,0,0,1,0,0,1,0,1,0,0,1,0,0,1,1,0,0,0,1,0,0,0,0,2,0,1,1,0,0,0,0,1,0,0,1,0,0,0,2,0,0,1,0,0,0,1,1,1,0,0,0,0,0,1,0,1,0,0,2,0,0,0,1,1,0,0,0,0,0,0,1,1,0,1,1,0,0,1,0,1,0",
			"name": "Number of partitions of n into 2 nonzero squares.",
			"comment": [
				"For records see A007511, A048610, A016032. - _R. J. Mathar_, Feb 26 2008"
			],
			"link": [
				"R. Zumkeller, \u003ca href=\"/A025426/b025426.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"\u003ca href=\"/index/Su#ssq\"\u003eIndex entries for sequences related to sums of squares\u003c/a\u003e"
			],
			"formula": [
				"Let m = A004018(n)/4. If m is even then a(n) = m/2, otherwise a(n) = (m - (-1)^A007814(n))/2. - _Max Alekseyev_, Mar 09 2009, Mar 14 2009",
				"a(A018825(n)) = 0; a(A000404(n)) \u003e 0; a(A025284(n)) = 1; a(A007692(n)) \u003e 1. - _Reinhard Zumkeller_, Aug 16 2011",
				"a(A000578(n)) = A084888(n). - _Reinhard Zumkeller_, Jul 18 2012",
				"a(n) = Sum_{i=1..floor(n/2)} A010052(i) * A010052(n-i). - _Wesley Ivan Hurt_, Apr 19 2019",
				"a(n) = [x^n y^2] Product_{k\u003e=1} 1/(1 - y*x^(k^2)). - _Ilya Gutkovskiy_, Apr 19 2019"
			],
			"maple": [
				"A025426 := proc(n)",
				"    local a,x;",
				"    a := 0 ;",
				"    for x from 1 do",
				"        if 2*x^2 \u003e n then",
				"            return a;",
				"        end if;",
				"        if issqr(n-x^2) then",
				"            a := a+1 ;",
				"        end if;",
				"    end do:",
				"end proc: # _R. J. Mathar_, Sep 15 2015"
			],
			"mathematica": [
				"m[n_] := m[n] = SquaresR[2, n]/4; a[0] = 0; a[n_] := If[ EvenQ[ m[n] ], m[n]/2, (m[n] - (-1)^IntegerExponent[n, 2])/2]; Table[ a[n], {n, 0, 107}] (* _Jean-François Alcover_, Jan 31 2012, after _Max Alekseyev_ *)",
				"nmax = 107; sq = Range[Sqrt[nmax]]^2;",
				"Table[Length[Select[IntegerPartitions[n, All, sq], Length[#] == 2 \u0026]], {n, 0, nmax}] (* _Robert Price_, Aug 17 2020 *)"
			],
			"program": [
				"(Haskell)",
				"a025426 n = sum $ map (a010052 . (n -)) $",
				"                      takeWhile (\u003c= n `div` 2) $ tail a000290_list",
				"a025426_list = map a025426 [0..]",
				"-- _Reinhard Zumkeller_, Aug 16 2011",
				"(PARI) a(n)={my(v=valuation(n,2),f=factor(n\u003e\u003ev),t=1);for(i=1,#f[,1],if(f[i,1]%4==1,t*=f[i,2]+1,if(f[i,2]%2,return(0))));if(t%2,t-(-1)^v,t)/2;} \\\\ _Charles R Greathouse IV_, Jan 31 2012"
			],
			"xref": [
				"Cf. A000161 (2 nonnegative squares), A063725 (order matters), A025427 (3 nonzero squares).",
				"Cf. A172151, A004526. - _Reinhard Zumkeller_, Jan 26 2010",
				"Column k=2 of A243148."
			],
			"keyword": "nonn,easy",
			"offset": "0,51",
			"author": "_David W. Wilson_",
			"references": 45,
			"revision": 48,
			"time": "2020-08-18T15:59:09-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}