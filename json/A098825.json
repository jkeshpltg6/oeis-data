{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A098825",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 98825,
			"data": "1,1,0,1,0,1,1,0,3,2,1,0,6,8,9,1,0,10,20,45,44,1,0,15,40,135,264,265,1,0,21,70,315,924,1855,1854,1,0,28,112,630,2464,7420,14832,14833,1,0,36,168,1134,5544,22260,66744,133497,133496,1,0,45,240,1890,11088,55650,222480,667485,1334960,1334961",
			"name": "Triangle read by rows: T(n,k) = number of partial derangements, that is, the number of permutations of n distinct, ordered items in which exactly k of the items are in their natural ordered positions, for n \u003e= 0, k = n, n-1, ..., 1, 0.",
			"comment": [
				"In other words, T(n,k) is the number of permutations of n letters that are at Hammimg distance k from the identity permutation (Cf. Diaconis, p. 112). - _N. J. A. Sloane_, Mar 02 2007",
				"The sequence d(n) = 1, 0, 1, 2, 9, 44, 265, 1854, 14833, ... (A000166) is the number of derangements, that is, the number of permutations of n distinct, ordered items in which none of the items is in its natural ordered position."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A098825/b098825.txt\"\u003eRows n = 0..125 of triangle, flattened\u003c/a\u003e",
				"P. Diaconis, \u003ca href=\"https://www.jstor.org/stable/4355560\"\u003eGroup Representations in Probability and Statistics\u003c/a\u003e, IMS, 1988; see p. 112.",
				"Chris D. H. Evans, John Hughes and Julia Houston, \u003ca href=\"https://doi.org/10.1348/000711002760554525\"\u003eSignificance-testing the validity of idiographic methods: A little derangement goes a long way\u003c/a\u003e, British Journal of Mathematical and Statistical Psychology, 1 November 2002, Vol. 55, pp. 385-390.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PartialDerangement.html\"\u003ePartial Derangement\u003c/a\u003e"
			],
			"formula": [
				"T(0, 0) = 1; d(0) = T(0, 0); for k = n, n-1, ..., 1, T(n, k) = c(n, k) d(n-k) where c(n, k) = n! / [(k!) (n-k)! ]; T(n, 0) = n! - [ T(n, n) + T(n, n-1) + ... + T(n, 1) ]; d(n) = T(n, 0).",
				"T(n,k) = A008290(n,n-k). - _Vladeta Jovovic_, Sep 04 2006",
				"Assuming a uniform distribution on S_n, the mean Hamming distance is n-1 and the variance is 1 (Diaconis, p. 117). - _N. J. A. Sloane_, Mar 02 2007"
			],
			"example": [
				"Assume d(0), d(1), d(2) are given. Then",
				"T(3, 3) = c(3, 3) d(0) = (1) (1) = 1",
				"T(3, 2) = c(3, 2) d(1) = (3) (0) = 0",
				"T(3, 1) = c(3, 1) d(2) = (3) (1) = 3",
				"T(3, 0) = 3! - [ 1 + 0 + 3 ] = 6 - 4 = 2",
				"d(3) = T(3, 0).",
				"Triangle begins:",
				"  1;",
				"  1, 0;",
				"  1, 0,  1;",
				"  1, 0,  3,  2;",
				"  1, 0,  6,  8,   9;",
				"  1, 0, 10, 20,  45,  44;",
				"  1, 0, 15, 40, 135, 264,  265;",
				"  1, 0, 21, 70, 315, 924, 1855, 1854;",
				"  ..."
			],
			"mathematica": [
				"t[0, 0] = 1; t[n_, k_] := Binomial[n, k]*k!*Sum[(-1)^j/j!, {j, 0, k}]; Flatten[ Table[ t[n, k], {n, 0, 10}, {k, 0, n}]] (* _Robert G. Wilson v_, Nov 04 2004 *)",
				"T[n_, k_] := Binomial[n, n-k] Subfactorial[k];",
				"Table[T[n, k], {n, 0, 10}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Sep 01 2021 *)"
			],
			"program": [
				"(Haskell)",
				"a098825 n k = a098825_tabl !! n !! k",
				"a098825_row n = a098825_tabl !! n",
				"a098825_tabl = map (zipWith (*) a000166_list) a007318_tabl",
				"-- _Reinhard Zumkeller_, Dec 16 2013"
			],
			"xref": [
				"Cf. A000166, A007318, A008290.",
				"T(2n,n) gives A281262."
			],
			"keyword": "nonn,tabl",
			"offset": "0,9",
			"author": "_Gerald P. Del Fiacco_, Nov 02 2004",
			"ext": [
				"More terms from _Robert G. Wilson v_, Nov 04 2004"
			],
			"references": 7,
			"revision": 33,
			"time": "2021-09-01T03:40:25-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}