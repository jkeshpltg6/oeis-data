{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A329431",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 329431,
			"data": "1,2,1,6,4,1,38,48,28,8,1,1446,3648,4432,3296,1628,544,120,16,1,2090918,10550016,26125248,41867904,48398416,42666880,29610272,16475584,7419740,2711424,800992,189248,35064,4928,496,32,1,4371938082726,44118436709376",
			"name": "Irregular triangular array, read by rows: row n shows the coefficients of the polynomial p(n,x) defined in Comments.",
			"comment": [
				"Let f(x) = x^2 + 2, u(0,x) = 1, u(n,x) = f(u(n-1),x), and p(n,x) = u(n,sqrt(x)).",
				"Then the sequence (p(n,0)) = (1,2,6,38,1446, ... ) is a strong divisibility sequence, as implied by Dickson's record of a statement by J. J. Sylvester proved by W. S. Foster in 1889.  p(n,0)) = A072191(n) for n \u003e= 1."
			],
			"reference": [
				"L. E. Dickson, History of the Theory of Numbers, vol. 1, Chelsea, New York, 1952, p. 403."
			],
			"example": [
				"Rows 0..4:",
				"  1;",
				"  2, 1;",
				"  6, 4, 1;",
				"  38, 48, 28, 8, 1;",
				"  1446, 3648, 4432, 3296, 1628, 544, 120, 16, 1.",
				"Rows 0..4, the polynomials u(n,x):",
				"  1;",
				"  2 + x^2;",
				"  6 + 4 x^2 + x^4;",
				"  38 + 48 x^2 + 28 x^4 + 8 x^6 + x^8;",
				"  1446 + 3648 x^2 + 4432 x^4 + 3296 x^6 + 1628 x^8 + 544 x^10 + 120 x^12 + 16 x^14 + x^16."
			],
			"mathematica": [
				"f[x_] := x^2 + 2;  u[0, x_] := 1;",
				"u[1, x_] := f[x]; u[n_, x_] := f[u[n - 1, x]]",
				"Column[Table [Expand[u[n, x]], {n, 0, 5}]] (* A329431 polynomials u(n,x) *)",
				"Table[CoefficientList[u[n, Sqrt[x], x], {n, 0, 5}]  (* A329431 array *)"
			],
			"xref": [
				"Cf. A329429, A329430, A329432, A329433."
			],
			"keyword": "nonn,tabf",
			"offset": "0,2",
			"author": "_Clark Kimberling_, Nov 23 2019",
			"references": 7,
			"revision": 4,
			"time": "2019-12-27T16:40:18-05:00",
			"created": "2019-12-27T16:40:18-05:00"
		}
	]
}