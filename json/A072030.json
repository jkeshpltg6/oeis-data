{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A072030",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 72030,
			"data": "1,2,2,3,1,3,4,3,3,4,5,2,1,2,5,6,4,4,4,4,6,7,3,4,1,4,3,7,8,5,2,5,5,2,5,8,9,4,5,3,1,3,5,4,9,10,6,5,5,6,6,5,5,6,10,11,5,3,2,5,1,5,2,3,5,11,12,7,6,6,5,7,7,5,6,6,7,12,13,6,6,4,6,4,1,4,6,4,6,6,13,14,8,4,6,2,3,8",
			"name": "Array read by antidiagonals: T(n,k) = number of steps in simple Euclidean algorithm for gcd(n,k) where n \u003e= 1, k \u003e= 1.",
			"comment": [
				"The old definition was: Triangle T(a,b) read by rows giving number of steps in simple Euclidean algorithm for gcd(a,b) (a \u003e b \u003e= 1). [For this, see A049834.]",
				"For example \u003c11,3\u003e -\u003e \u003c8,3\u003e -\u003e \u003c5,3\u003e -\u003e \u003c3,2\u003e -\u003e \u003c2,1\u003e -\u003e \u003c1,1\u003e -\u003e \u003c1,0\u003e takes 6 steps.",
				"The number of steps function can be defined inductively by T(a,b) = T(b,a), T(a,0) = 0, and T(a+b,b) = T(a,b)+1.",
				"The simple Euclidean algorithm is the Euclidean algorithm without divisions. Given a pair \u003ca,b\u003e of positive integers with a\u003e=b, let \u003ca^(1),b^(1)\u003e = \u003cmax(a-b,b),min(a-b,b)\u003e. This is iterated until a^(m)=0. Then T(a,b) is the number of steps m.",
				"Note that row n starts at k = 1; the number of steps to compute gcd(n,0) or gcd(0,n) is not shown. - _T. D. Noe_, Oct 29 2007"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A072030/b072030.txt\"\u003eAntidiagonals 1..49, flattened\u003c/a\u003e",
				"James C. Alexander, \u003ca href=\"http://web.archive.org/web/20030625141534/http://www.cwru.edu/artsci/math/alexander/pdf/fib.pdf\"\u003eThe Entropy of the Fibonacci Code\u003c/a\u003e (1989)."
			],
			"example": [
				"The array begins:",
				"   1,  2,  3,  4,  5,  6,  7,  8,  9, 10, ...",
				"   2,  1,  3,  2,  4,  3,  5,  4,  6,  5, ...",
				"   3,  3,  1,  4,  4,  2,  5,  5,  3,  6, ...",
				"   4,  2,  4,  1,  5,  3,  5,  2,  6,  4, ...",
				"   5,  4,  4,  5,  1,  6,  5,  5,  6,  2, ...",
				"   6,  3,  2,  3,  6,  1,  7,  4,  3,  4, ...",
				"   7,  5,  5,  5,  5,  7,  1,  8,  6,  6, ...",
				"   8,  4,  5,  2,  5,  4,  8,  1,  9,  5, ...",
				"   9,  6,  3,  6,  6,  3,  6,  9,  1, 10, ...",
				"  10,  5,  6,  4,  2,  4,  6,  5, 10,  1, ...",
				"  ...",
				"The first few antidiagonals are:",
				"   1;",
				"   2,  2;",
				"   3,  1,  3;",
				"   4,  3,  3,  4;",
				"   5,  2,  1,  2,  5;",
				"   6,  4,  4,  4,  4,  6;",
				"   7,  3,  4,  1,  4,  3,  7;",
				"   8,  5,  2,  5,  5,  2,  5,  8;",
				"   9,  4,  5,  3,  1,  3,  5,  4,  9;",
				"  10,  6,  5,  5,  6,  6,  5,  5,  6, 10;",
				"  ..."
			],
			"maple": [
				"A072030 := proc(n,k)",
				"    option remember;",
				"    if n \u003c 1 or k \u003c 1 then",
				"        0;",
				"    elif n = k then",
				"        1 ;",
				"    elif n \u003c k then",
				"        procname(k,n) ;",
				"    else",
				"        1+procname(k,n-k) ;",
				"    end if;",
				"end proc:",
				"seq(seq(A072030(d-k,k),k=1..d-1),d=2..12) ; # _R. J. Mathar_, May 07 2016"
			],
			"mathematica": [
				"T[n_, k_] := T[n, k] = Which[n\u003c1 || k\u003c1, 0, n==k, 1, n\u003ck, T[k, n], True, 1 + T[k, n-k]]; Table[T[n-k, k], {n, 2, 15}, {k, 1, n-1}] // Flatten (* _Jean-François Alcover_, Nov 21 2016, adapted from PARI *)"
			],
			"program": [
				"(PARI) T(n, k) = if( n\u003c1 || k\u003c1, 0, if( n==k, 1, if( n\u003ck, T(k,n), 1 + T(k, n-k))))"
			],
			"xref": [
				"Row sums are A072031. Cf. A049834 (the lower left triangle), A003989, A050873.",
				"See also A267177, A267178, A267181."
			],
			"keyword": "nonn,tabl,easy,nice",
			"offset": "1,2",
			"author": "_Michael Somos_, Jun 07 2002",
			"ext": [
				"Definition and Comments revised by _N. J. A. Sloane_, Jan 14 2016"
			],
			"references": 14,
			"revision": 52,
			"time": "2017-06-13T21:43:31-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}