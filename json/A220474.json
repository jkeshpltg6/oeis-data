{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A220474",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 220474,
			"data": "223,227,269,349,359,569,587,593,739,809,857,991,1009,1019,1259,1481,1483,1487,1489,1861,1867,1993,1997,2003,2027,2267,2269,2657,2671,2687,2689,2699,3181,3187,3307,3313,3319,3323,3457,3461,3491,3527,3529,3581,3623,3769,4049,4201,4391,4481",
			"name": "Chebyshev numbers C_v(n) for v=10/9: a(n) is the smallest number such that if x\u003e=a(n), then theta(x)-theta(9*x/10)\u003e=n*log(x), where theta(x)=sum_{prime p\u003c=x}log p.",
			"comment": [
				"All terms are primes.",
				"Up to a(99)=9029, all terms are (10/9)-Ramanujan numbers as in Shevelev's link; up to 9029, the only missing (10/9)-Ramanujan number is 127."
			],
			"link": [
				"N. Amersi, O. Beckwith, S. J. Miller, R. Ronan, J. Sondow, \u003ca href=\"http://arxiv.org/abs/1108.0475\"\u003eGeneralized Ramanujan primes\u003c/a\u003e, arXiv 2011.",
				"N. Amersi, O. Beckwith, S. J. Miller, R. Ronan, J. Sondow, \u003ca href=\"http://link.springer.com/chapter/10.1007/978-1-4939-1601-6_1\"\u003eGeneralized Ramanujan primes\u003c/a\u003e, Combinatorial and Additive Number Theory, Springer Proc. in Math. \u0026 Stat., CANT 2011 and 2012, Vol. 101 (2014), 1-13",
				"V. Shevelev, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL15/Shevelev/shevelev19.html\"\u003eRamanujan and Labos primes, their generalizations, and classifications of primes\u003c/a\u003e, J. Integer Seq. 15 (2012) Article 12.5.4",
				"Vladimir Shevelev, Charles R. Greathouse IV, Peter J. C. Moses, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL16/Moses/moses1.html\"\u003eOn intervals (kn, (k+1)n) containing a prime for all n\u003e1\u003c/a\u003e, Journal of Integer Sequences, Vol. 16 (2013), Article 13.7.3. \u003ca href=\"http://arxiv.org/abs/1212.2785\"\u003earXiv:1212.2785\u003c/a\u003e"
			],
			"formula": [
				"a(n)\u003c=prime(31*(n+1))."
			],
			"mathematica": [
				"k=9; xs=Table[{m,Ceiling[x/.FindRoot[(x (-1300+Log[x]^4))/Log[x]^5==(k+1) m,{x,f[(k+1) m]-1},AccuracyGoal-\u003eInfinity,PrecisionGoal-\u003e20,WorkingPrecision-\u003e100]]},{m,1,101}]; Table[{m,1+NestWhile[#-1\u0026,xs[[m]][[2]],(1/Log[#1]Plus@@Log[Select[Range[Floor[(k #1)/(k+1)]+1,#1],PrimeQ]]\u0026)[#]\u003em\u0026]},{m,1,100}] (* _Peter J. C. Moses_, Dec 20 2012 *)",
				"(* Assuming range of x is from a(n) to 2*a(n) *) Clear[a, theta]; theta[x_] := theta[x] = Sum[Log[p], {p, Table[Prime[k], {k, 1, PrimePi[x]}]}] // N; a[0] = 211(* just to speed-up computation *); a[n_] := a[ n] = (t = Table[an = Prime[pi]; Table[{an, x \u003e= an \u0026\u0026 theta[x] - theta[9*x/10] \u003e= n*Log[x]}, {x, an, 2*an}], {pi, PrimePi[a[n-1]], 31*(n+1)}]; sp = t // Flatten[#, 1]\u0026 // Sort // Split[#, #1[[1]] == #2[[1]]\u0026 ]\u0026; Select[sp, And @@ (#[[All, 2]]) \u0026] // First // First // First); Table[Print[a[n]]; a[n], {n, 1, 50}] (* _Jean-François Alcover_, Feb 11 2013 *)"
			],
			"xref": [
				"Cf. A220293, A220462, A220463."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Vladimir Shevelev_, _Charles R Greathouse IV_ and _Peter J. C. Moses_, Dec 15 2012",
			"ext": [
				"More terms from _Jean-François Alcover_, Feb 11 2013"
			],
			"references": 1,
			"revision": 39,
			"time": "2016-12-04T19:46:30-05:00",
			"created": "2012-12-22T00:26:06-05:00"
		}
	]
}