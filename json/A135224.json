{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A135224",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 135224,
			"data": "1,3,1,5,3,1,9,7,4,1,17,15,11,5,1,33,31,26,16,6,1,65,63,57,42,22,7,1,129,127,120,99,64,29,8,1,257,255,247,219,163,93,37,9,1,513,511,502,466,382,256,130,46,10,1",
			"name": "Triangle A103451 * A007318 * A000012, read by rows. T(n, k) for 0 \u003c= k \u003c= n.",
			"comment": [
				"Row sums = A132750: (1, 4, 9, 21, 49, 113,...).",
				"Left border = A083318: (1, 3, 5, 9, 17, 33,...)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A135224/b135224.txt\"\u003eRows n = 0..100 of triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = A103451(n,k) * A007318(n,k) * A000012(n,k) as infinite lower triangular matrices.",
				"T(n, k) = Sum_{j=0..n} binomial(n, k+j), with T(0,0) = 1 and T(n,0) = 2^n + 1. - _G. C. Greubel_, Nov 20 2019",
				"T(n, k) = binomial(n, k)*hypergeom([1, k-n], [k+1], -1) - binomial(n, k+n+1)* hypergeom([1, k+1], [k+n+2], -1) + 0^k - 0^n. - _Peter Luschny_, Nov 20 2019"
			],
			"example": [
				"First few rows of the triangle are:",
				"   1;",
				"   3,  1;",
				"   5,  3,  1;",
				"   9,  7,  4,  1;",
				"  17, 15, 11,  5,  1;",
				"  33, 31, 26, 16,  6, 1;",
				"  65, 63, 57, 42, 22, 7, 1;",
				"..."
			],
			"maple": [
				"T:= proc(n, k) option remember;",
				"      if k=0 and n=0 then 1",
				"    elif k=0 then 2^n +1",
				"    else add(binomial(n, k+j), j=0..n)",
				"      fi; end:",
				"seq(seq(T(n, k), k=0..n), n=0..10); # _G. C. Greubel_, Nov 20 2019"
			],
			"mathematica": [
				"T[n_, k_]:= T[n, k] = If[k==n==0, 1, If[k==0, 2^n +1, Sum[Binomial[n, k + j], {j, 0, n}]]]; Table[T[n, k], {n, 0, 10}, {k, 0, n}]//Flatten (* _G. C. Greubel_, Nov 20 2019 *)"
			],
			"program": [
				"(PARI) T(n,k) = if(k==0 \u0026\u0026 n==0, 1, if(k==0, 2^n +1, sum(j=0, n, binomial(n, k+j)) )); \\\\ _G. C. Greubel_, Nov 20 2019",
				"(MAGMA)",
				"function T(n,k)",
				"  if k eq 0 and n eq 0 then return 1;",
				"  elif k eq 0 then return 2^n +1;",
				"  else return (\u0026+[Binomial(n, k+j): j in [0..n]]);",
				"  end if; return T; end function;",
				"[T(n,k): k in [0..n], n in [0..10]]; // _G. C. Greubel_, Nov 20 2019",
				"(Sage)",
				"def T(n, k):",
				"    if (k==0 and n==0): return 1",
				"    elif (k==0): return 2^n + 1",
				"    else: return sum(binomial(n, k+j) for j in (0..n))",
				"[[T(n, k) for k in (0..n)] for n in (0..10)] # _G. C. Greubel_, Nov 20 2019"
			],
			"xref": [
				"Cf. A083318, A103451, A132750."
			],
			"keyword": "nonn,tabl",
			"offset": "0,2",
			"author": "_Gary W. Adamson_, Nov 23 2007",
			"references": 2,
			"revision": 11,
			"time": "2019-11-20T07:13:36-05:00",
			"created": "2007-12-09T03:00:00-05:00"
		}
	]
}