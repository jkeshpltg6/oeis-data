{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A321029",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 321029,
			"data": "1,1,1,2,1,1,2,4,3,1,6,2,8,2,1,8,12,3,14,2,2,6,18,4,5,8,9,4,24,1,26,16,6,12,2,6,32,14,8,4,36,2,38,12,3,18,42,8,14,5,12,16,48,9,6,8,14,24,54,2,56,26,6,32,8,6,62,24,18,2,66,12,68,32,5,28,12,8,74,8",
			"name": "Number of integers x such that 1 \u003c= x \u003c= n and gcd(x,n) = gcd(x+4,n) = gcd(x+6,n) = gcd(x+10,n) = gcd(x+12,n) = 1.",
			"comment": [
				"Equivalently, a(n) is the number of \"admissible\" residue classes modulo n which are allowed (by divisibility considerations) to contain infinitely many initial primes p in prime 5-tuples (p, p+4, p+6, p+10, p+12). This sequence also gives the number of \"admissible\" residue classes (mod n) for initial primes p in the other type of prime 5-tuples: (p, p+2, p+6, p+8, p+12). This sequence is a generalization of Euler's totient function (A000010(n), the number of residue classes modulo n containing infinitely many primes).",
				"If n is prime, a(n) = max(1,n-5)."
			],
			"reference": [
				"V. A. Golubev, Sur certaines fonctions multiplicatives et le problème des jumeaux. Mathesis 67 (1958), 11-20.",
				"J. Sándor, B. Crstici, Handbook of Number Theory, vol. II. Kluwer, 2004, p. 289."
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A321029/b321029.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"V. A. Golubev, \u003ca href=\"http://dml.cz/dmlcz/117061\"\u003eA generalization of the functions phi(n) and pi(x)\u003c/a\u003e. Časopis pro pěstování matematiky 78 (1953), 47-48.",
				"V. A. Golubev, \u003ca href=\"http://dml.cz/dmlcz/117442\"\u003eExact formulas for the number of twin primes and other generalizations of the function pi(x)\u003c/a\u003e. Časopis pro pěstování matematiky 87 (1962), 296-305.",
				"Alexei Kourbatov and Marek Wolf, \u003ca href=\"https://arxiv.org/abs/1901.03785\"\u003ePredicting maximal gaps in sets of primes\u003c/a\u003e, arXiv preprint arXiv:1901.03785 [math.NT], 2019."
			],
			"formula": [
				"Multiplicative with a(p^e) = p^(e-1) if p \u003c= 5; (p-5)*p^(e-1) if p \u003e= 7."
			],
			"example": [
				"All initial primes p in prime 5-tuples (p, p+4, p+6, p+10, p+12) are congruent to 7 mod 10; that is, there is only one \"admissible\" residue class mod 10; therefore a(10) = 1."
			],
			"mathematica": [
				"Table[Count[Range@ n, x_ /; Equal @@ Append[Map[GCD[# + x, n] \u0026, {0, 4, 6, 10, 12}], 1]], {n, 80}] (* _Michael De Vlieger_, Nov 13 2018 *)",
				"f[p_, e_] := If[p \u003c 7, p^(e-1), (p-5)*p^(e-1)]; a[1] = 1; a[n_] := Times @@ f @@@ FactorInteger[n]; Array[a, 100] (* _Amiram Eldar_, Jan 22 2020 *)"
			],
			"program": [
				"(PARI) phi5(n) = sum(x=1, n, (gcd(n, x)==1) \u0026\u0026 (gcd(n, x+4)==1) \u0026\u0026 (gcd(n, x+6)==1) \u0026\u0026 (gcd(n, x+10)==1) \u0026\u0026 (gcd(n, x+12)==1));",
				"for(n=1, 80, print1(phi5(n)\", \"))"
			],
			"xref": [
				"Cf. similar generalizations of totient for k-tuples: A002472 (k=2), A319534 (k=3), A319516 (k=4), A321030 (k=6)."
			],
			"keyword": "nonn,mult",
			"offset": "1,4",
			"author": "_Alexei Kourbatov_, Oct 26 2018",
			"references": 5,
			"revision": 28,
			"time": "2020-01-22T06:14:14-05:00",
			"created": "2019-01-05T01:38:18-05:00"
		}
	]
}