{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A308075",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 308075,
			"data": "1,2,3,5,8,12,14,11,7,9,20,16,18,79,25,27,88,34,36,97,43,45,101,29,49,110,38,54,200,47,58,699,56,67,789,65,76,798,74,85,879,83,94,888,92,139,897,119,148,969,128,157,978,137,166,987,146,175,996,155,184,1001,69,89,1010,78,98,1100,87,179,2000,96,188,5999,159,197,6899",
			"name": "Lexicographically earliest sequence of distinct positive integers such that the sum of the digits of a(n) is either the sum of all the digits of a(n-1) and a(n+1), or is the absolute difference |((sum of digits of a(n-1)) - ((sum of digits of a(n+1))|, for n \u003e 1.",
			"comment": [
				"To get the \"New sequence\" below, replace all terms by the sum of their digits:",
				"This seq = 1,2,3,5,8,12,14,11,7,9,20,16,18,79,25,27,88,...",
				"New  seq = 1,2,3,5,8, 3, 5, 2,7,9, 2, 7, 9,16, 7, 9,16,...",
				"We see that every term of the \"New sequence\" is either the sum of its two adjacent terms, or their absolute difference.",
				"This sequence may not be a permutation of the positive integers as the number 10 does not appear among the first 1000000 terms (according to Rémy Sigrist)."
			],
			"link": [
				"Jean-Marc Falcoz, \u003ca href=\"/A308075/b308075.txt\"\u003eTable of n, a(n) for n = 1..1502\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A308075/a308075.png\"\u003eLogarithmic scatterplot of the first 1000000 terms\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A308075/a308075_1.png\"\u003eScatterplot of the digital sums of the first 1000000 terms\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A308075/a308075.gp.txt\"\u003ePARI program for A308075\u003c/a\u003e"
			],
			"example": [
				"a(2) = 2 is |1-3|;",
				"a(3) = 3 is |2-5|;",
				"a(4) = 5 is |3-8|;",
				"a(5) = 8 is (5+1+2);",
				"a(6) = 12 because 12 gives (1+2) = 3 and this 3 is (8-1-4);",
				"a(7) = 14 because 14 gives (1+4) = 5 and this 5 is (1+2+1+1);",
				"a(8) = 11 because 11 gives (1+1) = 2 and this 2 is |1+4-7|;",
				"etc."
			],
			"program": [
				"(PARI) See Links section."
			],
			"xref": [
				"Cf. A007953 (Digital sum (i.e., sum of digits) of n; also called digsum(n)), A307638 (digital sums of this sequence)."
			],
			"keyword": "base,nonn",
			"offset": "1,2",
			"author": "_Eric Angelini_ and _Jean-Marc Falcoz_, May 11 2019",
			"references": 2,
			"revision": 19,
			"time": "2019-05-11T20:05:27-04:00",
			"created": "2019-05-11T10:37:39-04:00"
		}
	]
}