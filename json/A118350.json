{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A118350",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 118350,
			"data": "1,1,0,1,1,0,1,2,1,0,1,3,6,1,0,1,4,13,7,1,0,1,5,21,42,8,1,0,1,6,30,96,54,9,1,0,1,7,40,163,325,67,10,1,0,1,8,51,244,770,445,81,11,1,0,1,9,63,340,1353,2688,583,96,12,1,0,1,10,76,452,2093,6530,3842,740,112,13,1,0",
			"name": "Pendular triangle, read by rows, where row n is formed from row n-1 by the recurrence: if n \u003e 2k, T(n,k) = T(n,n-k) + T(n-1,k), else T(n,k) = T(n,n-1-k) + 3*T(n-1,k), for n\u003e=k\u003e=0, with T(n,0)=1 and T(n,n)=0^n.",
			"comment": [
				"See definition of pendular triangle and pendular sums at A118340."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A118350/b118350.txt\"\u003eRows n = 0..100 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(2*n+m,n) = [A118351^(m+1)](n), i.e., the m-th lower semi-diagonal forms the self-convolution (m+1)-power of the central terms A118351."
			],
			"example": [
				"Row 6 equals the pendular sums of row 5,",
				"  [1,  4, 13,  7,  1,  0], where the sums proceed as follows:",
				"  [1, __, __, __, __, __]: T(6,0) = T(5,0) = 1;",
				"  [1, __, __, __, __,  1]: T(6,5) = T(6,0) + 3*T(5,5) = 1 + 3*0 = 1;",
				"  [1,  5, __, __, __,  1]: T(6,1) = T(6,5) + T(5,1) = 1 + 4 = 5;",
				"  [1,  5, __, __,  8,  1]: T(6,4) = T(6,1) + 3*T(5,4) = 5 + 3*1 = 8;",
				"  [1,  5, 21, __,  8,  1]: T(6,2) = T(6,4) + T(5,2) = 8 + 13 = 21;",
				"  [1,  5, 21, 42,  8,  1]: T(6,3) = T(6,2) + 3*T(5,3) = 21 + 3*7 = 42;",
				"  [1,  5, 21, 42,  8,  1, 0] finally, append a zero to obtain row 6.",
				"Triangle begins:",
				"  1;",
				"  1,  0;",
				"  1,  1,  0;",
				"  1,  2,  1,   0;",
				"  1,  3,  6,   1,    0;",
				"  1,  4, 13,   7,    1,     0;",
				"  1,  5, 21,  42,    8,     1,     0;",
				"  1,  6, 30,  96,   54,     9,     1,    0;",
				"  1,  7, 40, 163,  325,    67,    10,    1,   0;",
				"  1,  8, 51, 244,  770,   445,    81,   11,   1,   0;",
				"  1,  9, 63, 340, 1353,  2688,   583,   96,  12,   1,  0;",
				"  1, 10, 76, 452, 2093,  6530,  3842,  740, 112,  13,  1, 0;",
				"  1, 11, 90, 581, 3010, 11760, 23286, 5230, 917, 129, 14, 1, 0; ...",
				"Central terms are T(2*n,n) = A118351(n);",
				"semi-diagonals form successive self-convolutions of the central terms:",
				"T(2*n+1,n) = A118352(n) = [A118351^2](n),",
				"T(2*n+2,n) = A118353(n) = [A118351^3](n)."
			],
			"mathematica": [
				"T[n_, k_, p_]:= T[n,k,p] = If[n\u003ck || k\u003c0, 0, If[k==0, 1, If[k==n, 0, If[n\u003c=2*k, T[n,n-k-1,p] + p*T[n-1,k,p], T[n,n-k,p] + T[n-1,k,p] ]]]];",
				"Table[T[n,k,3], {n,0,12}, {k,0,n}]//Flatten (* _G. C. Greubel_, Feb 17 2021 *)"
			],
			"program": [
				"(PARI) T(n,k)=if(n\u003ck || k\u003c0,0,if(k==0,1,if(n==k,0, if(n\u003e2*k,T(n,n-k)+T(n-1,k),T(n,n-1-k)+3*T(n-1,k)))))",
				"(Sage)",
				"@CachedFunction",
				"def T(n, k, p):",
				"    if (k\u003c0 or n\u003ck): return 0",
				"    elif (k==0): return 1",
				"    elif (k==n): return 0",
				"    elif (n\u003e2*k): return T(n,n-k,p) + T(n-1,k,p)",
				"    else: return T(n, n-k-1, p) + p*T(n-1, k, p)",
				"flatten([[T(n,k,3) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, Feb 17 2021",
				"(Magma)",
				"function T(n,k,p)",
				"  if k lt 0 or n lt k then return 0;",
				"  elif k eq 0 then return 1;",
				"  elif k eq n then return 0;",
				"  elif n gt 2*k then return T(n,n-k,p) + T(n-1,k,p);",
				"  else return T(n,n-k-1,p) + p*T(n-1,k,p);",
				"  end if;",
				"  return T;",
				"end function;",
				"[T(n,k,3): k in [0..n], n in [0..12]]; // _G. C. Greubel_, Feb 17 2021"
			],
			"xref": [
				"Cf. A118351, A118352, A118353, A118354.",
				"Cf. A167763 (p=0), A118340 (p=1), A118345 (p=2), this sequence (p=3)."
			],
			"keyword": "nonn,tabl",
			"offset": "0,8",
			"author": "_Paul D. Hanna_, Apr 26 2006",
			"references": 10,
			"revision": 9,
			"time": "2021-02-18T00:29:06-05:00",
			"created": "2006-05-19T03:00:00-04:00"
		}
	]
}