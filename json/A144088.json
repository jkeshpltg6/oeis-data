{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A144088",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 144088,
			"data": "1,1,1,4,2,1,18,12,3,1,108,72,24,4,1,780,540,180,40,5,1,6600,4680,1620,360,60,6,1,63840,46200,16380,3780,630,84,7,1,693840,510720,184800,43680,7560,1008,112,8,1,8361360,6244560,2298240,554400,98280,13608,1512,144,9,1",
			"name": "T(n,k) is the number of partial bijections (or subpermutations) of an n-element set with exactly k fixed points.",
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A144088/b144088.txt\"\u003eTable of n, a(n) for n = 0..1325\u003c/a\u003e (rows n=0..50)",
				"A. Laradji and A. Umar, \u003ca href=\"http://dx.doi.org/10.1007/s00233-007-0732-8\"\u003eCombinatorial results for the symmetric inverse semigroup\u003c/a\u003e, Semigroup Forum 75, (2007), 221-236.",
				"A. Umar, \u003ca href=\"http://www.mathnet.ru/eng/adm33\"\u003eSome combinatorial problems in the theory of symmetric ...\u003c/a\u003e, Algebra Disc. Math. 9 (2010) 115-126."
			],
			"formula": [
				"T(n,k) = C(n,k)*(n-k)! * Sum_{m=0..n-k} (-1^m/m!)*Sum_{j=0..n-m} C(n-m,j)/j!.",
				"(n-k)*T(n,k) = n*(2n-2k-1)*T(n-1,k) - n*(n-1)*(n-k-3)*T(n-2,k) - n*(n-1)*(n-2)*T(n-3,k), T(k,k)=1 and T(n,k)=0 if n \u003c k.",
				"E.g.f.: exp(log(1/(1-x)) - x + y*x)*exp(x/(1-x)). - _Geoffrey Critzer_, Nov 29 2021"
			],
			"example": [
				"Triangle begins:",
				"      1;",
				"      1,     1;",
				"      4,     2,     1;",
				"     18,    12,     3,    1;",
				"    108,    72,    24,    4,   1;",
				"    780,   540,   180,   40,   5,  1;",
				"   6600,  4680,  1620,  360,  60,  6, 1;",
				"  63840, 46200, 16380, 3780, 630, 84, 7, 1;",
				"  ...",
				"T(3,1) = 12 because there are exactly 12 partial bijections (on a 3-element set) with exactly 1 fixed point, namely: (1)-\u003e(1), (2)-\u003e(2), (3)-\u003e(3), (1,2)-\u003e(1,3), (1,2)-\u003e(3,2), (1,3)-\u003e(1,2), (1,3)-\u003e(2,3), (2,3)-\u003e(2,1), (2,3)-\u003e(1,3), (1,2,3)-\u003e(1,3,2), (1,2,3)-\u003e(3,2,1), (1,2,3)-\u003e(2,1,3) - the mappings are coordinate-wise."
			],
			"mathematica": [
				"max = 7; f[x_, k_] := (x^k/k!)*(Exp[x^2/(1-x)]/(1-x)); t[n_, k_] := n!*SeriesCoefficient[ Series[ f[x, k], {x, 0, max}], n]; Flatten[ Table[ t[n, k], {n, 0, max}, {k, 0, n}]](* _Jean-François Alcover_, Mar 12 2012, from e.g.f. by _Joerg Arndt_ *)"
			],
			"program": [
				"(PARI)",
				"T(n) = {my(egf=exp(log(1/(1-x) + O(x*x^n)) - x + y*x + x/(1-x))); Vec([Vecrev(p) | p\u003c-Vec(serlaplace(egf))])}",
				"{ my(A=T(10)); for(n=1, #A, print(A[n])) } \\\\ _Andrew Howroyd_, Nov 29 2021"
			],
			"xref": [
				"T(n, 0) = A144085, T(n, 1) = A144086, T(n, 2) = A144087.",
				"Row sums give A002720."
			],
			"keyword": "nice,nonn,tabl",
			"offset": "0,4",
			"author": "_Abdullahi Umar_, Sep 11 2008, Sep 16 2008",
			"ext": [
				"Terms a(36) and beyond from _Andrew Howroyd_, Nov 29 2021"
			],
			"references": 4,
			"revision": 28,
			"time": "2021-12-14T20:33:32-05:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}