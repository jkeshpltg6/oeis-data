{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A060899",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 60899,
			"data": "1,2,8,24,96,320,1280,4480,17920,64512,258048,946176,3784704,14057472,56229888,210862080,843448320,3186360320,12745441280,48432676864,193730707456,739699064832,2958796259328,11342052327424",
			"name": "Number of walks of length n on square lattice, starting at origin, staying on points with x+y \u003e= 0.",
			"comment": [
				"The number of lattice paths consisting of 2*n steps either (1,1) or (1,-1) that return to the x-axis only at times that are a multiple of 4. - _Peter Bala_, Jan 02 2020"
			],
			"link": [
				"Harry J. Smith, \u003ca href=\"/A060899/b060899.txt\"\u003eTable of n, a(n) for n=0,...,200\u003c/a\u003e",
				"Paul Barry, \u003ca href=\"https://www.emis.de/journals/JIS/VOL22/Barry1/barry411.html\"\u003eThe Central Coefficients of a Family of Pascal-like Triangles and Colored Lattice Paths\u003c/a\u003e, J. Int. Seq., Vol. 22 (2019), Article 19.1.3.",
				"A. Bostan, \u003ca href=\"https://www-apr.lip6.fr/sem-comb-slides/IHP-bostan.pdf\"\u003eComputer Algebra for Lattice Path Combinatorics\u003c/a\u003e, Séminaire de Combinatoire Ph. Flajolet, March 28 2013.",
				"Alin Bostan, Andrew Elvey Price, Anthony John Guttmann, Jean-Marie Maillard, \u003ca href=\"https://arxiv.org/abs/2001.00393\"\u003eStieltjes moment sequences for pattern-avoiding permutations\u003c/a\u003e, arXiv:2001.00393 [math.CO], 2020.",
				"Math Overflow, \u003ca href=\"https://mathoverflow.net/questions/86924/combinatorial-proof-for-the-number-of-lattice-paths-that-return-to-the-axis-only\"\u003eCombinatorial proof for the number of lattice paths that return to the axis only at times that are a multiple of 4\u003c/a\u003e, 2012"
			],
			"formula": [
				"a(n) = 2^n*binomial(n, [n/2]);",
				"G.f.: (sqrt((1+4*x)/(1-4*x))-1)/4/x. - _Vladeta Jovovic_, Apr 28 2003",
				"E.g.f.: BesselI(0, 4*x)+BesselI(1, 4*x). - _Vladeta Jovovic_, Apr 28 2003",
				"a(n) = 4^n*sum{k=0..n, C(n,k)C(k)/(-2)^k}, with C(n)=A000108(n). - _Paul Barry_, Dec 28 2006",
				"(n+1)*a(n) -4*a(n-1) +16*(-n+1)*a(n-2)=0. - _R. J. Mathar_, Nov 24 2012",
				"a(n) = (-4)^n*hypergeom([3/2,-n],[2],2). - _Peter Luschny_, Apr 26 2016"
			],
			"mathematica": [
				"Table[2^n Binomial[n,Floor[n/2]],{n,0,30}] (* _Harvey P. Dale_, Oct 15 2017 *)"
			],
			"program": [
				"(PARI) { for (n=0, 200, write(\"b060899.txt\", n, \" \", 2^n*binomial(n, n\\2)); ) } \\\\ _Harry J. Smith_, Jul 14 2009"
			],
			"xref": [
				"Cf. A005566, A001700, A060897, A060898, A060900.",
				"Cf. A001405."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_David W. Wilson_, May 05 2001",
			"references": 3,
			"revision": 38,
			"time": "2020-03-23T21:23:38-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}