{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A108958",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 108958,
			"data": "0,1,6,27,110,430,1652,6307,24054,91866,351692,1350030,5196204,20050108,77542376,300507427,1166737574,4537436578,17672369756,68922740122,269127888644,1052047384708,4116711169496,16123793452942,63205286441660,247959232919620",
			"name": "Number of unordered pairs of distinct length-n binary words having the same number of 1's.",
			"comment": [
				"Equals row sums of triangle A143418, starting with a(2). - _Gary W. Adamson_, Aug 14 2008",
				"In coupled systems of n spin 1/2 particles (magnetic resonance) where the spin state of the i-th particle can be coded as 0 (Sz_i=-1/2) or 1 (Sz_i=+1/2), number of distinct (v\u003cw) nontrivial (v!=w) zero-quantum transitions (v-\u003ew). - _Stanislav Sykora_, Jun 07 2012",
				"a(n) is the number of lattice paths from (0,0) to (n,n) using E(1,0) and N(0,1) as steps that horizontally cross the diagonal y = x with odd many times. For example, a(2) = 1 because there is only one path that horizontally crosses the diagonal with odd many times, namely, NEEN. - _Ran Pan_, Feb 01 2016"
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A108958/b108958.txt\"\u003eTable of n, a(n) for n = 1..1664\u003c/a\u003e",
				"Mircea Merca, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL15/Merca2/merca7.html\"\u003e A Special Case of the Generalized Girard-Waring Formula\u003c/a\u003e J. Integer Sequences, Vol. 15 (2012), Article 12.5.7.",
				"Ran Pan and Jeffrey B. Remmel, \u003ca href=\"http://arxiv.org/abs/1601.07988\"\u003ePaired patterns in lattice paths\u003c/a\u003e, arXiv:1601.07988 [math.CO], 2016.",
				"Stanislav Sýkora, \u003ca href=\"http://www.ebyte.it/stan/blog12to14.html#14Dec31\"\u003eMagnetic Resonance on OEIS\u003c/a\u003e, Stan's NMR Blog (Dec 31, 2014), Retrieved Nov 12, 2019."
			],
			"formula": [
				"a(n) = sum(binomial(binomial(n, k), 2), k=0..n);",
				"a(n) = binomial(2*n-1, n-1)-2^(n-1) = A088218(n)-A011782(n). E.g.f.: exp(2*x)*(BesselI(0, 2*x)-1)/2. - _Vladeta Jovovic_, Jul 24 2005",
				"a(n) = (1/2)*sum(i+j\u003en,0\u003c=i,j\u003c=n,binomial(i+j,i)). - _Benoit Cloitre_, May 26 2006",
				"Conjecture: n*(n-2)*a(n) +2*(-3*n^2+7*n-3)*a(n-1) +4*(n-1)*(2*n-3) *a(n-2)=0. - _R. J. Mathar_, Apr 04 2012",
				"a(n) = sum_{0\u003ci\u003c=k\u003cn} (-1)^(i+1)*binomial(n,k+i)*binomial(n,k-i). - _Mircea Merca_, Apr 05 2012",
				"a(n) = binomial(2*n,n) - A005317(n), - _Ran Pan_, Feb 01 2016"
			],
			"example": [
				"a(3) = 6 because the pairs are {001,010}, {001,100}, {010,100}, {011,101}, {011,110}, {101,110}."
			],
			"maple": [
				"with(combinat) a:= proc(n) add(binomial(binomial(n,k), 2), k=0..n) end;"
			],
			"mathematica": [
				"Table[Binomial[2 n, n] - (2^n + Binomial[2 n, n])/2, {n, 30}] (* _Vincenzo Librandi_, Feb 01 2016 *)"
			],
			"program": [
				"(MAGMA) [Binomial(2*n,n)-(2^n+Binomial(2*n,n))/2: n in [1..30]]; // _Vincenzo Librandi_, Feb 01 2016",
				"(PARI) a(n)=binomial(2*n-1,n-1)-2^(n-1) \\\\ _Charles R Greathouse IV_, Feb 01 2016"
			],
			"xref": [
				"Cf. A143418, A005317."
			],
			"keyword": "easy,nonn",
			"offset": "1,3",
			"author": "_Jeffrey Shallit_, Jul 22 2005",
			"references": 3,
			"revision": 52,
			"time": "2019-11-18T22:05:38-05:00",
			"created": "2005-09-21T03:00:00-04:00"
		}
	]
}