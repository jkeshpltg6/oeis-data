{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001235",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1235,
			"data": "1729,4104,13832,20683,32832,39312,40033,46683,64232,65728,110656,110808,134379,149389,165464,171288,195841,216027,216125,262656,314496,320264,327763,373464,402597,439101,443889,513000,513856,515375,525824,558441,593047,684019,704977",
			"name": "Taxi-cab numbers: sums of 2 cubes in more than 1 way.",
			"comment": [
				"From Wikipedia: \"1729 is known as the Hardy-Ramanujan number after a famous anecdote of the British mathematician G. H. Hardy regarding a hospital visit to the Indian mathematician Srinivasa Ramanujan. In Hardy's words: 'I remember once going to see him when he was ill at Putney. I had ridden in taxi cab number 1729 and remarked that the number seemed to me rather a dull one, and that I hoped it was not an unfavorable omen. \"No,\" he replied, \"it is a very interesting number; it is the smallest number expressible as the sum of two cubes in two different ways.\"'\"",
				"A011541 gives another version of \"taxicab numbers\".",
				"If n is in this sequence, then n*k^3 is also in this sequence for all k \u003e 0. So this sequence is obviously infinite. - _Altug Alkan_, May 09 2016"
			],
			"reference": [
				"R. K. Guy, Unsolved Problems in Number Theory, Section D1.",
				"G. H. Hardy, Ramanujan, Cambridge Univ. Press, 1940, p. 12.",
				"Ya. I. Perelman, Algebra can be fun, pp. 142-143.",
				"H. W. Richmond, On integers which satisfy the equation t^3 +- x^3 +- y^3 +- z^3, Trans. Camb. Phil. Soc., 22 (1920), 389-403, see p. 402.",
				"D. Wells, The Penguin Dictionary of Curious and Interesting Numbers. Penguin Books, NY, 1986, 165."
			],
			"link": [
				"Shahar Amitai, \u003ca href=\"/A001235/b001235.txt\"\u003eTable of n, a(n) for n = 1..30000\u003c/a\u003e (terms a(1)-a(4724) from T. D. Noe, terms a(4725)-a(10000) from Zak Seidov).",
				"Shahar Amitai, \u003ca href=\"/A001235/a001235_1.txt\"\u003ePython code to generate all taxicab numbers up to N.\u003c/a\u003e",
				"J. Charles-É, Recreomath, \u003ca href=\"http://translate.google.com/translate?hl=en\u0026amp;sl=fr\u0026amp;u=http://www.recreomath.qc.ca/dict_ramanujan_n.htm\"\u003eRamanujan's Number\u003c/a\u003e",
				"A. Grinstein, \u003ca href=\"https://web.archive.org/web/20040320144821/http://zadok.org/mattandloraine/1729.html\"\u003eRamanujan and 1729\u003c/a\u003e, University of Melbourne Dept. of Math and Statistics Newsletter: Issue 3, 1998.",
				"Istanbul Bilgi University, \u003ca href=\"https://web.archive.org/web/20110712051436/http://cs.bilgi.edu.tr/pages/curiosity_corner/challenges/ramanujans_number.html\"\u003eRamanujan and Hardy's Taxi\u003c/a\u003e",
				"Christopher Lane, The First ten Ta(2) and their double distinct cubic sums representations, \u003ca href=\"http://cdl.best.vwh.net/JavaScript/Cubes.html\"\u003eFind Ramanujan's Taxi Number using JavaScript\u003c/a\u003e",
				"J. Leech, \u003ca href=\"http://dx.doi.org/10.1017/S0305004100032850\"\u003eSome solutions of Diophantine equations\u003c/a\u003e, Proc. Camb. Phil. Soc., 53 (1957), 778-780.",
				"J. Loy, \u003ca href=\"https://web.archive.org/web/20130823014303/http://www.jimloy.com/number/hardy.htm\"\u003eThe Hardy-Ramanujan Number\u003c/a\u003e.",
				"Ken Ono, Sarah Trebat-Leder, \u003ca href=\"http://arxiv.org/abs/1510.00735\"\u003eThe 1729 K3 surface\u003c/a\u003e, arXiv:1510.00735 [math.NT], 2015.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CubicNumber.html\"\u003eCubic Number\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/DiophantineEquation3rdPowers.html\"\u003eDiophantine Equation 3rd Powers\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/TaxicabNumber.html\"\u003eTaxicab Number\u003c/a\u003e",
				"D. W. Wilson, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/wilson10.html\"\u003eThe Fifth Taxicab Number is 48988659276962496\u003c/a\u003e, J. Integer Sequences, Vol. 2, 1999, #99.1.9."
			],
			"example": [
				"4104 belongs to the sequence as 4104 = 2^3 + 16^3 = 9^3 + 15^3."
			],
			"mathematica": [
				"Select[Range[750000],Length[PowersRepresentations[#,2,3]]\u003e1\u0026] (* _Harvey P. Dale_, Nov 25 2014, with correction by _Zak Seidov_, Jul 13 2015 *)"
			],
			"program": [
				"(PARI) is(n)=my(t);for(k=ceil((n/2)^(1/3)),(n-.4)^(1/3),if(ispower(n-k^3,3),if(t,return(1),t=1)));0 \\\\ _Charles R Greathouse IV_, Jul 15 2011",
				"(PARI) T=thueinit(x^3+1,1);",
				"is(n)=my(v=thue(T,n)); sum(i=1,#v,v[i][1]\u003e=0 \u0026\u0026 v[i][2]\u003e=v[i][1])\u003e1 \\\\ _Charles R Greathouse IV_, May 09 2016"
			],
			"xref": [
				"Cf. A003325, A003826, A007692, A008917, A011541, A018786, A018787, A018850, A023050, A023051, A343708."
			],
			"keyword": "nonn,nice",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_",
			"references": 87,
			"revision": 101,
			"time": "2021-08-11T14:42:12-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}