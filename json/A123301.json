{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A123301",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 123301,
			"data": "1,0,0,0,1,0,0,1,1,0,0,1,34,1,0,0,1,199,199,1,0,0,1,916,7037,916,1,0,0,1,3889,117071,117071,3889,1,0,0,1,15982,1535601,6317926,1535601,15982,1,0,0,1,64747,18271947,228842801,228842801,18271947",
			"name": "Triangle read by rows: T(n,k) is the number of specially labeled bicolored nonseparable graphs with k points in one color class and n-k points in the other class. \"Special\" means there are separate labels 1,2,...,k and 1,2,...,n-k for the two color classes (n \u003e= 2, k = 1,...,n-1).",
			"reference": [
				"R. W. Robinson, Numerical implementation of graph counting algorithms, AGRC Grant, Math. Dept., Univ. Newcastle, Australia, 1977."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A123301/b123301.txt\"\u003eTable of n, a(n) for n = 2..1276\u003c/a\u003e (first 50 rows; first 24 rows from R. W. Robinson)",
				"F. Harary and R. W. Robinson, \u003ca href=\"http://dx.doi.org/10.4153/CJM-1979-007-3\"\u003eLabeled bipartite blocks\u003c/a\u003e, Canad. J. Math., 31 (1979), 60-68."
			],
			"formula": [
				"A004100(n) = (1/2) * Sum_{k=1..n-1} binomial(n,k)*T(n,k). - _Andrew Howroyd_, Jan 03 2021"
			],
			"example": [
				"Triangle begins:",
				"  1;",
				"  0, 0;",
				"  0, 1,    0;",
				"  0, 1,    1,      0;",
				"  0, 1,   34,      1,      0;",
				"  0, 1,  199,    199,      1,    0;",
				"  0, 1,  916,   7037,    916,    1, 0;",
				"  0, 1, 3889, 117071, 117071, 3889, 1, 0;",
				"  ...",
				"Formatted as an array:",
				"=================================================",
				"k/j | 1 2    3       4         5           6",
				"--- +-------------------------------------------",
				"  1 | 1 0    0       0         0           0 ...",
				"  2 | 0 1    1       1         1           1 ...",
				"  3 | 0 1   34     199       916        3889 ...",
				"  4 | 0 1  199    7037    117071     1535601 ...",
				"  5 | 0 1  916  117071   6317926   228842801 ...",
				"  6 | 0 1 3889 1535601 228842801 21073662977 ...",
				"  ..."
			],
			"program": [
				"(PARI)",
				"G(n)={sum(i=0, n, x^i*(sum(j=0, n, y^j*2^(i*j)/(i!*j!)) + O(y*y^n))) + O(x*x^n)}",
				"\\\\ this switches x/y halfway through because PARI only does serreverse in x.",
				"B(n)={my(p=log(G(n))); p=subst(deriv(p,y), x, serreverse(x*deriv(p,x))); p=substvec(p, [x,y], [y,x]); intformal(log(x/serreverse(x*p)))}",
				"M(n)={my(p=B(n)); matrix(n,n,i,j,polcoef(polcoef(p,j),i)*i!*j!)}",
				"{ my(A=M(6)); for(n=1, #A~, print(A[n,])) } \\\\ _Andrew Howroyd_, Jan 04 2021"
			],
			"xref": [
				"Central coefficients are A005334.",
				"Cf. A004100, A123474, A262307."
			],
			"keyword": "nonn,tabl",
			"offset": "2,13",
			"author": "_N. J. A. Sloane_, Nov 12 2006",
			"ext": [
				"Offset corrected by _Andrew Howroyd_, Jan 04 2021"
			],
			"references": 3,
			"revision": 24,
			"time": "2021-05-22T21:01:52-04:00",
			"created": "2006-12-06T03:00:00-05:00"
		}
	]
}