{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A137852",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 137852,
			"data": "1,1,-2,9,-24,130,-720,8505,-35840,412776,-3628800,42030450,-479001600,7019298000,-82614884352,1886805545625,-20922789888000,374426276224000,-6402373705728000,134987215801622184,-2379913632645120000,55685679780013920000",
			"name": "G.f.: Product_{n\u003e=1} (1 + a(n)*x^n/n!) = exp(x).",
			"comment": [
				"Equals signed A006973 (except for initial term), where A006973 lists the dimensions of representations by Witt vectors."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A137852/b137852.txt\"\u003eTable of n, a(n) for n = 1..170\u003c/a\u003e",
				"Gottfried Helms, \u003ca href=\"http://go.helms-net.de/math/musings/dreamofasequence.pdf\"\u003eA dream of a (number-) sequence\u003c/a\u003e, 2007-2009."
			],
			"formula": [
				"a(n) = (n-1)!*[(-1)^n + Sum_{d divides n, 1\u003cd\u003cn} d*( -a(d)/d! )^(n/d) ] for n\u003e1 with a(1)=1.",
				"Another recurrence. With FP(n,m) the set of partitions of n with m distinct parts (which could be called fermionic partitions (fp)) and the multinomial numbers M1(fp(n,m)) (given as array in A036038) for any fp(n,m) from FP(n,m): a(n)= 1 - sum( sum(M1(fp)*product(a(k[j]),j=1..m),fp from FP(n,m)),m=2..maxm(n)), with maxm(n):=A003056(n) and the distinct parts k[j], j=1,...,m, of the partition fp(n,m). Inputs a(1)=1, a(2)=1. See also array A008289(n,m) for the cardinality of the set FP(n,m). - _Wolfdieter Lang_, Feb 20 2009"
			],
			"example": [
				"exp(x) = (1+x)*(1+x^2/2!)*(1-2*x^3/3!)*(1+9*x^4/4!)*(1-24*x^5/5!)* (1+130*x^6/6!)*(1-720*x^7/7!)*(1+8505*x^8/8!)*(1-35840*x^9/9!)*(1+412776*x^10/10!)*(1-3628800*x^11/11!)*...*(1+a(n)*x^n/n!)*...",
				"Another recurrence: n=6; m=1,2,3=maxm(6)=A003056(6); fp(6,2) from {(1,5),(2,4)}, fp(6,3)=(1,2,3); a(6)= 1 - ( 6*a(1)*a(5) + 15*a(2)*a(4) + 60*a(1)*a(2)*a(3)). Check: 1 - (6*1*(-24) + 15*1*9 +60*1*1*(-2)) = 130 = a(6). - _Wolfdieter Lang_, Feb 20 2009"
			],
			"maple": [
				"with(numtheory):",
				"a:= proc(n) option remember; `if`(n=1, 1, (n-1)!*((-1)^n+",
				"       add(d*(-a(d)/d!)^(n/d), d=divisors(n) minus {1, n})))",
				"    end:",
				"seq(a(n), n=1..30);  # _Alois P. Heinz_, Aug 14 2012"
			],
			"mathematica": [
				"max = 22; f[x_] := Product[1 + a[n] x^n/n!, {n, 1, max}]; coes = CoefficientList[ Series[f[x] - Exp[x], {x, 0, max}], x]; sol = Solve[ Thread[coes == 0]][[1]]; Table[a[n] /. sol, {n, 1, max}] (* _Jean-François Alcover_, Nov 28 2011 *)",
				"a[1] = 1; a[n_] := a[n] = (n-1)!*((-1)^n + Sum[d*(-a[d]/d!)^(n/d), {d, Divisors[n] ~Complement~ {1, n}}]);",
				"Array[a, 30] (* _Jean-François Alcover_, Jan 11 2018 *)"
			],
			"program": [
				"(PARI) {a(n)=if(n\u003c1,0,if(n==1,1,(n-1)!*((-1)^n + sumdiv(n,d, if(d\u003cn\u0026d\u003e1, d*(-a(d)/d!)^(n/d))))))}",
				"for(n=1,30,print1(a(n),\", \"))",
				"(PARI) /* As coefficients in product g.f.: */",
				"{a(n)=if(n\u003c1,0,n!*polcoeff(exp(x +x*O(x^n))/prod(k=0,n-1,1+a(k)*x^k/k! +x*O(x^n)),n))}",
				"for(n=1,30,print1(a(n),\", \"))"
			],
			"xref": [
				"Cf. A006973."
			],
			"keyword": "nice,sign",
			"offset": "1,3",
			"author": "_Paul D. Hanna_, Feb 14 2008",
			"references": 10,
			"revision": 28,
			"time": "2020-06-12T12:22:48-04:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}