{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A090841",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 90841,
			"data": "11,7,11177,1777,71777,1777717,1177717771,77777177,7177717777,1777777777,71777777777,1717777777777,7177777777777,17777777777777,17177777777777717,7717777777777777,1177777777177777777,1777777777777777177,7777177777777777777",
			"name": "Smallest prime whose product of digits is 7^n.",
			"link": [
				"Michael S. Branicky, \u003ca href=\"/A090841/b090841.txt\"\u003eTable of n, a(n) for n = 0..999\u003c/a\u003e"
			],
			"example": [
				"a(6) = 1177717771 because its digital product is 7^6, and it is prime."
			],
			"maple": [
				"a:= proc(n) local k, t; for k from 0 do t:= min(select(isprime,",
				"      map(x-\u003e parse(cat(x[])), combinat[permute]([1$k, 7$n]))));",
				"      if t\u003cinfinity then return t fi od",
				"    end:",
				"seq(a(n), n=0..18);  # _Alois P. Heinz_, Nov 07 2021"
			],
			"mathematica": [
				"NextPrim[n_] := Block[{k = n + 1}, While[ !PrimeQ[k], k++ ]; k]; a = Table[0, {18}]; p = 2; Do[q = Log[7, Times @@ IntegerDigits[p]]; If[q != 0 \u0026\u0026 IntegerQ[q] \u0026\u0026 a[[q]] == 0, a[[q]] = p; Print[q, \" = \", p]]; p = NextPrim[p], {n, 1, 10^9}]",
				"For a(8); a = Map[ FromDigits, Permutations[{1, 1, 7, 7, 7, 7, 7, 7, 7, 7}]]; Min[ Select[a, PrimeQ[ # ] \u0026]]"
			],
			"program": [
				"(Python)",
				"from sympy import isprime",
				"from sympy.utilities.iterables import multiset_permutations as mp",
				"def a(n):",
				"    if n \u003c 2: return [11, 7][n]",
				"    digits = n",
				"    while True:",
				"        for p in mp(\"1\"*(digits-n) + \"7\"*n, digits):",
				"            t = int(\"\".join(p))",
				"            if isprime(t): return t",
				"        digits += 1",
				"print([a(n) for n in range(19)]) # _Michael S. Branicky_, Nov 07 2021"
			],
			"xref": [
				"Cf. A089365, A088653, A090840, A091465, A089298."
			],
			"keyword": "base,nonn",
			"offset": "0,1",
			"author": "_Robert G. Wilson v_, Dec 09 2003",
			"ext": [
				"a(17) and beyond from _Michael S. Branicky_, Nov 07 2021"
			],
			"references": 6,
			"revision": 7,
			"time": "2021-11-07T09:46:29-05:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}