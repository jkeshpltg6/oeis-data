{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A283938",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 283938,
			"data": "1,4,2,10,6,3,18,13,8,5,29,22,16,11,7,43,34,26,20,14,9,59,49,39,31,24,17,12,78,66,55,45,36,28,21,15,99,86,73,62,51,41,33,25,19,123,108,94,81,69,57,47,38,30,23,150,133,117,103,89,76,64,53,44,35",
			"name": "Interspersion of the signature sequence of tau^2, where tau = (1 + sqrt(5))/2 = golden ratio.",
			"comment": [
				"Row n is the ordered sequence of numbers k such that A118276(k) = n. As a sequence, A283938 is a permutation of the positive integers. As an array, A283938 is the joint-rank array (defined at A182801) of the numbers {i+j*r}, for i\u003e=1, j\u003e=1, where r = tau^2 = (3 + sqrt(5))/2. This is a transposable interspersion; i.e., every row intersperses all other rows, and every column intersperses all other columns."
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A283938/b283938.txt\"\u003eAntidiagonals n = 1..60, flattened\u003c/a\u003e",
				"Clark Kimberling and John E. Brown, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL7/Kimberling/kimber67.html\"\u003ePartial Complements and Transposable Dispersions\u003c/a\u003e, J. Integer Seqs., Vol. 7, 2004."
			],
			"example": [
				"Northwest corner:",
				"1   4  10   18  29  43  59   78  99   123",
				"2   6  13   22  34  49  66   86  108  133",
				"3   8  16   26  39  55  73   94  117  143",
				"5  11  20   31  45  62  81  103  127  154",
				"7  14  24   36  51  69  89  112  137  165",
				"9  17  28   41  57  76  97  121  147  176",
				"From _Indranil Ghosh_, Mar 19 2017: (Start)",
				"Triangle formed when the array is read by antidiagonals:",
				"    1;",
				"    4,   2;",
				"   10,   6,  3;",
				"   18,  13,  8,  5;",
				"   29,  22, 16, 11,  7;",
				"   43,  34, 26, 20, 14,  9;",
				"   59,  49, 39, 31, 24, 17, 12;",
				"   78,  66, 55, 45, 36, 28, 21, 15;",
				"   99,  86, 73, 62, 51, 41, 33, 25, 19;",
				"  123, 108, 94, 81, 69, 57, 47, 38, 30, 23;",
				"  ...",
				"(End)"
			],
			"mathematica": [
				"r = GoldenRatio^2; z = 100;",
				"s[0] = 1; s[n_] := s[n] = s[n - 1] + 1 + Floor[n*r];",
				"u = Table[n + 1 + Sum[Floor[(n - k)/r], {k, 0, n}], {n, 0, z}] (* A283968, row 1 of A283938 *)",
				"v = Table[s[n], {n, 0, z}] (* A283969, col 1 of A283938 *)",
				"w[i_, j_] := v[[i]] + u[[j]] + (i - 1)*(j - 1) - 1;",
				"Grid[Table[w[i, j], {i, 1, 10}, {j, 1, 10}]] (* A283938, array *)",
				"Flatten[Table[w[k, n - k + 1], {n, 1, 20}, {k, 1, n}]] (* A283938, sequence *)"
			],
			"program": [
				"(PARI)",
				"\\\\ This code produces the triangle mentioned in the example section",
				"r = (3 +sqrt(5))/2;",
				"z = 100;",
				"s(n) = if(n\u003c1, 1, s(n - 1) + 1 + floor(n*r));",
				"p(n) = n + 1 + sum(k=0, n, floor((n - k)/r));",
				"u = v = vector(z + 1);",
				"for(n=1, 101, (v[n] = s(n - 1)));",
				"for(n=1, 101, (u[n] = p(n - 1)));",
				"w(i,j) = v[i] + u[j] + (i - 1) * (j - 1) - 1;",
				"tabl(nn) = {for(n=1, nn, for(k=1, n, print1(w(n - k + 1, k),\", \");); print(););};",
				"tabl(10) \\\\ _Indranil Ghosh_, Mar 19 2017"
			],
			"xref": [
				"Cf. A118276, A283961, A283968, A283969."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Mar 18 2017",
			"references": 3,
			"revision": 24,
			"time": "2017-03-19T13:18:00-04:00",
			"created": "2017-03-19T01:15:31-04:00"
		}
	]
}