{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A266046",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 266046,
			"data": "1,2,2,-4,-28,-88,-184,-208,272,2336,7712,16832,21056,-16768,-193408,-673024,-1531648,-2088448,836096,15875072,58483712,138684416,203835392,-16764928,-1290072064,-5059698688,-12498362368,-19635257344,-3550855168,103608123392",
			"name": "Real part of Q^n, where Q is the quaternion 2 + j + k.",
			"comment": [
				"In general, given a quaternion Q = r+u*i+v*j+w*k with integer coefficients [r,u,v,w], its powers Q^n = R(n)+U(n)*i+V(n)*j+W(n)*k define four integer sequences R(n),U(n),V(n),W(n). The process can be also transcribed as a four-term, first order recurrence for the elements of the four sequences. Since |Q^n| = |Q|^n, we have, for any n, R(n)^2+U(n)^2+V(n)^2+W(n)^2 = (L^2)^n, where L^2 = r^2+u^2+v^2+w^2 is a constant. The normalized sequence Q^n/L^n describes a unitary quaternion undergoing stepwise rotations by the angle phi = arctan(sqrt(u^2+v^2+w^2)/r). Consequently, the four sequences exhibit sign changes with the mean period of P = 2*Pi/phi steps.",
				"When Q has a symmetry with respect to permutations and/or inversions of the imaginary axes, the four sequences become even more interdependent.",
				"In this particular case Q = 2+j+k, and Q^n = a(n)+b(n)*(j+k), where b(n) is the sequence A190965. The first-order recurrence reduces to two-terms, namely a(n+1)=2*a(n)-2*b(n), b(n+1)=2*b(n)+a(n). This implies further a single-term, second order recurrence a(n+2)=4*a(n+1)-6*a(n), shared by both a(n) and b(n), but with different starting terms. The mean period of sign changes is P = 10.208598624... steps.",
				"The following OEIS sequences can be also cast as quaternion powers:",
				"Q = 1+i+j+k: Q^n = A128018(n)+A088138(n)*(i+j+k), P = 6.000,",
				"Q = 1+j+k  : Q^n = A087455(n)+A088137(n)*(j+k),   P = 6.577071086...,",
				"Q = 2+i+j+k: Q^n = A213421(n)+A168175(n)*(i+j+k), P = 8.803377735..."
			],
			"link": [
				"Stanislav Sykora, \u003ca href=\"/A266046/b266046.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Beata Bajorska-Harapińska, Barbara Smoleń, Roman Wituła, \u003ca href=\"https://doi.org/10.1007/s00006-019-0969-9\"\u003eOn Quaternion Equivalents for Quasi-Fibonacci Numbers, Shortly Quaternaccis\u003c/a\u003e, Advances in Applied Clifford Algebras Vol. 29, No. 3 (2019), Article 54.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-6)."
			],
			"formula": [
				"a(n)^2 + 2*A190965(n)^2 = 6^n.",
				"From _Colin Barker_, Dec 21 2015: (Start)",
				"a(n) = ((2-i*sqrt(2))^n+(2+i*sqrt(2))^n)/2, where i=sqrt(-1).",
				"a(n) = 4*a(n-1) - 6*a(n-2) for n\u003e1.",
				"G.f.: (1-2*x) / (1-4*x+6*x^2). (End)"
			],
			"mathematica": [
				"LinearRecurrence[{4, -6}, {1, 2}, 30] (* _Bruno Berselli_, Dec 22 2015 *)"
			],
			"program": [
				"(PARI) \\\\ A simple function to generate quaternion powers:",
				"QuaternionToN(r, u, v, w, nmax) = {local (M); M = matrix(nmax+1, 4); M[1, 1]=1; for(n=2, nmax+1, M[n, 1]=r*M[n-1, 1]-u*M[n-1, 2]-v*M[n-1, 3]-w*M[n-1, 4]; M[n, 2]=u*M[n-1, 1]+r*M[n-1, 2]+w*M[n-1, 3]-v*M[n-1, 4]; M[n, 3]=v*M[n-1, 1]-w*M[n-1, 2]+r*M[n-1, 3]+u*M[n-1, 4]; M[n, 4]=w*M[n-1, 1]+v*M[n-1, 2]-u*M[n-1, 3]+r*M[n-1, 4]; ); return (M); }",
				"a=QuaternionToN(2, 0, 1, 1, 1000)[,1]; \\\\ Select the real parts",
				"(PARI) Vec((1-2*x)/(1-4*x+6*x^2) + O(x^40)) \\\\ _Colin Barker_, Dec 21 2015",
				"(MAGMA) [n le 2 select n else  4*Self(n-1)-6*Self(n-2): n in [1..40]]; // _Vincenzo Librandi_, Dec 22 2015"
			],
			"xref": [
				"Cf. A087455, A088137, A088138, A128018, A168175, A190965, A213421."
			],
			"keyword": "sign,easy",
			"offset": "0,2",
			"author": "_Stanislav Sykora_, Dec 20 2015",
			"references": 2,
			"revision": 26,
			"time": "2019-07-17T17:14:20-04:00",
			"created": "2015-12-24T13:46:21-05:00"
		}
	]
}