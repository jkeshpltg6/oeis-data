{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A139365",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 139365,
			"data": "0,0,0,1,0,1,1,2,2,3,0,1,1,2,2,3,1,2,2,3,3,4,2,3,3,4,4,5,3,4,4,5,5,6,0,1,1,2,2,3,1,2,2,3,3,4,2,3,3,4,4,5,3,4,4,5,5,6,1,2,2,3,3,4,2,3,3,4,4,5,3,4,4,5,5,6,4,5,5,6,6,7,2,3,3,4,4,5,3,4,4,5,5,6,4,5,5,6,6,7,5,6,6,7,7",
			"name": "Array of digit sums of factorial representation of numbers 0,1,...,n!-1 for n \u003e= 1.",
			"comment": [
				"The row lengths sequence is A000142 (factorials).",
				"When the factorial representation is read as (D. N.) Lehmer code for permutations of n objects then the digit sums in row n count the inversions of the permutations arranged in lexicographic order.",
				"Row n is the first n! terms of A034968. - _Franklin T. Adams-Watters_, May 13 2009"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A139365/b139365.txt\"\u003eRows n = 0..8, flattened\u003c/a\u003e",
				"FindStat - Combinatorial Statistic Finder, \u003ca href=\"http://www.findstat.org/St000018\"\u003eThe number of inversions of a permutation \u003c/a\u003e",
				"A. Kohnert, \u003ca href=\"ftp://ftp.mathe2.uni-bayreuth.de/axel/combalg/combalg.ps\"\u003e Kombinatorische Algorithmen in C, Skript, Uni Bayreuth, 1997, pp. 5-7 \u003c/a\u003e [Broken link]",
				"W. Lang, \u003ca href=\"/A139365/a139365.txt\"\u003eFirst 6 rows. Factorial representations or Lehmer code for permutations\u003c/a\u003e.",
				"D. N. Lehmer, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9904-1906-01419-7\"\u003eOn the orderly listing of substitutions\u003c/a\u003e, Bull. AMS 12 (1906), 81-84.",
				"\u003ca href=\"/index/Fa#facbase\"\u003eIndex entries for sequences related to factorial base representation\u003c/a\u003e"
			],
			"formula": [
				"Row n \u003e= 1: sum(facrep(n,m)[n-j],j=1..n), m=0,1,...,n!-1, with the factorial representation facrep(n,m) of m for given n."
			],
			"example": [
				"n=3: The Lehmer codes for the permutations of {1,2,3} are [0,0,0], [0,1,0], [1,0,0], [1,1,0], [2,0,0] and [2,1,0]. These are the factorial representations for 0,1,...,5=3!-1. Therefore row n=3 has the digit sums 0,1,1,2,2,3, the number of inversions of the permutations [1,2,3], [1,3,2], [2,1,3], [2,3,1], [3,1,2] and [3,2,1] (lexicographic order)."
			],
			"mathematica": [
				"nn = 5; m = 1; While[Factorial@ m \u003c nn! - 1, m++]; m; Table[Total@ IntegerDigits[k, MixedRadix[Reverse@ Range[2, m]]], {n, 0, 5}, {k, 0, n! - 1}] // Flatten (* Version 10.2, or *)",
				"f[n_] := Block[{a = {{0, n}}}, Do[AppendTo[a, {First@ #, Last@ #} \u0026@ QuotientRemainder[a[[-1, -1]], Times @@ Range[# - i]]], {i, 0, #}] \u0026@ NestWhile[# + 1 \u0026, 0, Times @@ Range[# + 1] \u003c= n \u0026]; Most@ Rest[a][[All, 1]]]; Table[Total@ f@ k, {n, 0, 5}, {k, 0, n! - 1}] // Flatten (* _Michael De Vlieger_, Aug 29 2016 *)"
			],
			"xref": [
				"Cf. A034968. - _Franklin T. Adams-Watters_, May 13 2009",
				"Cf. A008302."
			],
			"keyword": "nonn,base,easy,tabf",
			"offset": "0,8",
			"author": "_Wolfdieter Lang_ May 21 2008",
			"ext": [
				"In %H '.' -\u003e 'or','with' -\u003e 'for' In %D changed http link address. - _Wolfdieter Lang_, Sep 09 2008",
				"Zero term added by _Franklin T. Adams-Watters_, May 13 2009"
			],
			"references": 3,
			"revision": 35,
			"time": "2019-08-29T17:49:37-04:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}