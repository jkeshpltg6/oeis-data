{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A320647",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 320647,
			"data": "0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,2,0,0,0,1,12,17,4,0,0,0,2,44,84,51,9,0,0,0,7,137,388,339,125,15,0,0,0,12,408,1586,2010,1054,258,24,0,0,0,31,1190,6405,10900,7928,2761,490,35,0,0,0,58,3416,24927,56700,54383,25680,6392,859,51,0,0,0,126,9730,96404,286888,356594,218246,72284,13472,1420,68,0,0",
			"name": "Triangle read by rows: T(n,k) is the number of chiral pairs of cycles of length n (1) with a color pattern of exactly k colors or equivalently (2) partitioned into k nonempty subsets.",
			"comment": [
				"Two color patterns are the same if the colors are permuted. A chiral cycle is different from its reverse.",
				"Adnk[d,n,k] in Mathematica program is coefficient of x^k in A(d,n)(x) in Gilbert and Riordan reference."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A320647/b320647.txt\"\u003eTable of n, a(n) for n = 1..1275\u003c/a\u003e",
				"E. N. Gilbert and J. Riordan, \u003ca href=\"http://projecteuclid.org/euclid.ijm/1255631587\"\u003eSymmetry types of periodic sequences\u003c/a\u003e, Illinois J. Math., 5 (1961), 657-665."
			],
			"formula": [
				"T(n,k) = (A152175(n,k) - A304972(n,k)) / 2 = A152175(n,k) - A152176(n,k) = A152176(n,k) - A304972(n,k).",
				"T(n,k) = -Ach(n,k)/2 + (1/2n)*Sum_{d|n} phi(d)*A(d,n/d,k), where Ach(n,k) = [n\u003e=0 \u0026 n\u003c2 \u0026 n==k] + [n\u003e1]*(k*Ach(n-2,k)+Ach(n-2,k-1)+Ach(n-2,k-2)) and A(d,n,k) = [n==0 \u0026 k==0] + [n\u003e0 \u0026 k\u003e0]*(k*A(d,n-1,k) + Sum_{j|d} A(d,n-1,k-j))."
			],
			"example": [
				"The triangle begins with T(1,1):",
				"  0;",
				"  0,   0;",
				"  0,   0,    0;",
				"  0,   0,    0,     0;",
				"  0,   0,    0,     0,      0;",
				"  0,   0,    4,     2,      0,      0;",
				"  0,   1,   12,    17,      4,      0,      0;",
				"  0,   2,   44,    84,     51,      9,      0,     0;",
				"  0,   7,  137,   388,    339,    125,     15,     0,     0;",
				"  0,  12,  408,  1586,   2010,   1054,    258,    24,     0,    0;",
				"  0,  31, 1190,  6405,  10900,   7928,   2761,   490,    35,    0,  0;",
				"  0,  58, 3416, 24927,  56700,  54383,  25680,  6392,   859,   51,  0, 0;",
				"  0, 126, 9730, 96404, 286888, 356594, 218246, 72284, 13472, 1420, 68, 0, 0;",
				"  ...",
				"For T(6,3)=4, the chiral pairs are AAABBC-AAABCC, AABABC-AABCAC, AABACB-AABCAB, and AABACC-AABBAC.",
				"For T(6,4)=2, the chiral pairs are AABACD-AABCAD and AABCBD-AABCDC."
			],
			"mathematica": [
				"Ach[n_, k_] := Ach[n, k] = If[n\u003c2, Boole[n==k \u0026\u0026 n\u003e=0], k Ach[n-2,k] + Ach[n-2,k-1] + Ach[n-2,k-2]] (* A304972 *)",
				"Adnk[d_,n_,k_] := Adnk[d,n,k] = If[n\u003e0 \u0026\u0026 k\u003e0, Adnk[d,n-1,k]k + DivisorSum[d,Adnk[d,n-1,k-#] \u0026], Boole[n==0 \u0026\u0026 k==0]]",
				"Table[DivisorSum[n,EulerPhi[#]Adnk[#,n/#,k]\u0026]/(2n)-Ach[n,k]/2,{n,12},{k,n}] // Flatten"
			],
			"program": [
				"(PARI) \\\\ Ach is A304972 and R is A152175 as square matrices.",
				"Ach(n)={my(M=matrix(n, n, i, k, i\u003e=k)); for(i=3, n, for(k=2, n, M[i, k]=k*M[i-2, k] + M[i-2, k-1] + if(k\u003e2, M[i-2, k-2]))); M}",
				"R(n)={Mat(Col([Vecrev(p/y, n) | p\u003c-Vec(intformal(sum(m=1, n, eulerphi(m) * subst(serlaplace(-1 + exp(sumdiv(m, d, y^d*(exp(d*x + O(x*x^(n\\m)))-1)/d))), x, x^m))/x))]))}",
				"T(n)={(R(n) - Ach(n))/2}",
				"{ my(A=T(12)); for(n=1, #A, print(A[n, 1..n])) } \\\\ _Andrew Howroyd_, Sep 20 2019"
			],
			"xref": [
				"Columns 1-6 are A000004, A059053, A320643, A320644, A320645, A320646.",
				"Row sums are A320749.",
				"Cf. A152175 (oriented), A152176 (unoriented), A304972 (achiral)."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "1,18",
			"author": "_Robert A. Russell_, Oct 18 2018",
			"references": 10,
			"revision": 21,
			"time": "2019-11-04T02:17:30-05:00",
			"created": "2018-10-22T13:50:34-04:00"
		}
	]
}