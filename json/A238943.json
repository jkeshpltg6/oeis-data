{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A238943",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 238943,
			"data": "1,2,2,3,2,3,4,3,2,3,4,5,4,3,3,3,4,5,6,5,4,4,3,3,4,3,4,5,6,7,6,5,5,4,4,4,3,3,4,5,4,5,6,7,8,7,6,6,5,5,5,4,4,4,4,5,3,4,4,5,6,4,5,6,7,8,9,8,7,7,6,6,6,5,5,5,5,5,4,4,4,4,5,6,3,4",
			"name": "Triangular array read by rows: t(n,k) = size of the Ferrers matrix of p(n,k).",
			"comment": [
				"Suppose that p is a partition of n, and let m = max{greatest part of p, number of parts of p}.  Write the Ferrers graph of p with 1's as nodes, and pad the graph with 0's to form an m X m square matrix, which is introduced at A237981 as the Ferrers matrix of p, denoted by f(p).  The size of f(p) is m."
			],
			"formula": [
				"t(n,k) = max{max(p(n,k)), length(p(n,k)}, where p(n,k) is the k-th partition of n in Mathematica order."
			],
			"example": [
				"First 8 rows:",
				"1",
				"2 2 2",
				"3 2 3",
				"4 3 2 3 4",
				"5 4 3 3 3 4 5",
				"6 5 4 4 3 3 4 3 4 5 6",
				"7 6 5 5 4 4 4 3 3 4 5 4 5 6 7",
				"8 7 6 6 5 5 5 4 4 4 4 5 3 4 4 5 6 4 5 6 7 8",
				"For n = 3, the three partitions are [3], [2,1], [1,1,1].  Their respective Ferrers matrices derive from Ferrers graphs as follows:",
				"The partition [3] has Ferrers graph 1 1 1, with Ferrers matrix of size 3:",
				"1 1 1",
				"0 0 0",
				"0 0 0",
				"The partition [2,1] has Ferrers graph",
				"11",
				"1",
				"with Ferrers matrix of size 2:",
				"1 1",
				"1 0",
				"The partition [1,1,1] has Ferrers graph",
				"1",
				"1",
				"1",
				"with Ferrers matrix of size 3",
				"1 0 0",
				"1 0 0",
				"1 0 0",
				"Thus row 3 is (3,2,3)."
			],
			"mathematica": [
				"p[n_, k_] := p[n, k] = IntegerPartitions[n][[k]]; a[t_] := Max[Max[t], Length[t]]; t = Table[a[p[n, k]], {n, 1, 10}, {k, 1, PartitionsP[n]}]",
				"u = TableForm[t]  (* A238943 array *)",
				"v = Flatten[t]    (* A238943 sequence *)"
			],
			"xref": [
				"Cf. A238944, A238945, A237981, A000041."
			],
			"keyword": "nonn,tabf,easy",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Mar 07 2014",
			"references": 3,
			"revision": 16,
			"time": "2020-04-06T22:15:13-04:00",
			"created": "2014-03-14T00:07:33-04:00"
		}
	]
}