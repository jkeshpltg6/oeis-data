{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A278823",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 278823,
			"data": "1,4,29,32,93,84,189,188,321,316,489,460,693,676,933,916,1205,1180,1505,1496,1849,1836,2229,2188,2645,2616,3097,3060,3577,3536,4089,4064,4645,4604,5237,5176,5857,5808,6513,6472,7201,7160,7933,7900,8693,8648,9497",
			"name": "4-Portolan numbers: number of regions formed by n-secting the angles of a square.",
			"comment": [
				"m-Portolan numbers for m\u003e3 (especially m even) are more difficult than m=3 (A277402) because Ceva's theorem doesn't immediately give us a condition for redundant intersections. The values for n \u003c= 23 were found by brute force in Mathematica, as follows:",
				"1. Solve for the coordinates of all intersections between lines within the square, recording multiplicity.",
				"2. Use an elementary Euler's-formula method as in Poonen and Rubinstein 1998 to turn the intersection-count into a region-count."
			],
			"link": [
				"Lars Blomberg, \u003ca href=\"/A278823/b278823.txt\"\u003eTable of n, a(n) for n = 1..500\u003c/a\u003e",
				"Ethan Beihl, \u003ca href=\"http://ethan.beihl.org/erb/creative-work/mathematical-visualization/\"\u003ePictures for some small n\u003c/a\u003e",
				"Lars Blomberg, \u003ca href=\"/A278823/a278823.png\"\u003eColoured illustration for n=4\u003c/a\u003e",
				"Lars Blomberg, \u003ca href=\"/A278823/a278823_1.png\"\u003eColoured illustration for n=5\u003c/a\u003e",
				"Lars Blomberg, \u003ca href=\"/A278823/a278823_2.png\"\u003eColoured illustration for n=64\u003c/a\u003e",
				"Lars Blomberg, \u003ca href=\"/A278823/a278823_3.png\"\u003eColoured illustration for n=65\u003c/a\u003e",
				"B. Poonen and M. Rubinstein (1998) \u003ca href=\"http://math.mit.edu/~poonen/papers/ngon.pdf\"\u003eThe Number of Intersection Points Made by the Diagonals of a Regular Polygon\u003c/a\u003e, SIAM J. Discrete Mathematics 11(1), pp. 135-156, doi:\u003ca href=\"http://dx.doi.org/10.1137/S0895480195281246\"\u003e10.1137/S0895480195281246\u003c/a\u003e, arXiv:\u003ca href=\"http://arXiv.org/abs/math.MG/9508209\"\u003emath.MG/9508209\u003c/a\u003e (typos corrected)"
			],
			"formula": [
				"For n = 2k - 1, a(n) is close to 18k^2 - 26k + 9. For n = 2k, a(n) is close to 18k^2 - 26k + 12. The residuals are related to the structure of redundant intersections in the figure."
			],
			"example": [
				"For n=3, the 4*(3-1) = 8 lines intersect to make 12 triangles, 8 kites, 8 irregular quadrilaterals, and an octagon in the middle. The total number of regions a(3) is therefore 12+8+8+1 = 29."
			],
			"xref": [
				"3-Portolan numbers (equilateral triangle): A277402.",
				"n-sected sides (not angles): A108914.",
				"Cf. A277402, A335526 (vertices), A335527 (edges), A335528 (ngons)."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Ethan Beihl_, Nov 28 2016",
			"ext": [
				"a(24) and beyond from _Lars Blomberg_, Jun 12 2020"
			],
			"references": 4,
			"revision": 16,
			"time": "2020-06-12T06:17:27-04:00",
			"created": "2016-11-29T22:48:38-05:00"
		}
	]
}