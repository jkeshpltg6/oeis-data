{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A123620",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 123620,
			"data": "1,4,16,60,228,864,3276,12420,47088,178524,676836,2566080,9728748,36884484,139839696,530172540,2010036708,7620627744,28891993356,109537863300,415289569968,1574482299804,5969315609316,22631393727360,85802128010028,325300565212164",
			"name": "Expansion of (1 + x + x^2) / (1 - 3*x - 3*x^2).",
			"comment": [
				"From _Johannes W. Meijer_, Aug 14 2010: (Start)",
				"A berserker sequence, see A180141. For the corner squares 16 A[5] vectors with decimal values between 3 and 384 lead to this sequence. These vectors lead for the side squares to A180142 and for the central square to A155116.",
				"This sequence belongs to a family of sequences with GF(x) = (1+x+k*x^2)/(1-3*x+(k-4)*x^2). Berserker sequences that are members of this family are 4*A055099(n) (k=2; with leading 1 added), A123620 (k=1; this sequence), A000302 (k=0), 4*A179606 (k=-1; with leading 1 added) and A180141 (k=-2). Some other members of this family are 4*A003688 (k=3; with leading 1 added), 4*A003946 (k=4; with leading 1 added), 4*A002878 (k=5; with leading 1 added) and 4*A033484 (k=6; with leading 1 added).",
				"(End)",
				"a(n) is the number of length n sequences on an alphabet of 4 letters that do not contain more than 2 consecutive equal letters. For example, a(3)=60 because we count all 4^3=64 words except: aaa, bbb, ccc, ddd. - _Geoffrey Critzer_, Mar 12 2014"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A123620/b123620.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"A. Burstein and T. Mansour, \u003ca href=\"https://arxiv.org/abs/math/0112281\"\u003eWords restricted by 3-letter generalized multipermutation patterns\u003c/a\u003e, arXiv:math/0112281 [math.CO], 2001.",
				"A. Burstein and T. Mansour, \u003ca href=\"http://dx.doi.org/10.1007/s000260300000\"\u003eWords restricted by 3-letter generalized multipermutation patterns\u003c/a\u003e, Annals. Combin., 7 (2003), 1-14.",
				"Martin Burtscher, Igor Szczyrba, Rafał Szczyrba, \u003ca href=\"http://www.emis.de/journals/JIS/VOL18/Szczyrba/sz3.html\"\u003eAnalytic Representations of the n-anacci Constants and Generalizations Thereof\u003c/a\u003e, Journal of Integer Sequences, Vol. 18 (2015), Article 15.4.5.",
				"P. Flajolet and R. Sedgewick, \u003ca href=\"http://algo.inria.fr/flajolet/Publications/books.html\"\u003eAnalytic Combinatorics\u003c/a\u003e, 2009; see page 205",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,3)."
			],
			"formula": [
				"a(0)=1, a(1)=4, a(2)=16, a(n)=3*a(n-1)+3*a(n-2) for n\u003e2. - _Philippe Deléham_, Sep 18 2009",
				"a(n) = ((2^(1-n)*(-(3-sqrt(21))^(1+n) + (3+sqrt(21))^(1+n)))) / (3*sqrt(21)) for n\u003e0. - _Colin Barker_, Oct 17 2017"
			],
			"mathematica": [
				"nn=25;CoefficientList[Series[(1-z^(m+1))/(1-r z +(r-1)z^(m+1))/.{r-\u003e4,m-\u003e2},{z,0,nn}],z] (* _Geoffrey Critzer_, Mar 12 2014 *)",
				"CoefficientList[Series[(1 + x + x^2)/(1 - 3 x - 3 x^2), {x, 0, 40}], x] (* _Vincenzo Librandi_, Mar 14 2014 *)"
			],
			"program": [
				"(PARI) x='x+O('x^50); Vec((1+x+x^2)/(1-3*x-3*x^2)) \\\\ _G. C. Greubel_, Oct 16 2017 *)",
				"(MAGMA) [1] cat [Round(((2^(1-n)*(-(3-Sqrt(21))^(1+n) + (3+Sqrt(21))^(1+n))))/(3*Sqrt(21))): n in [1..50]]; // _G. C. Greubel_, Oct 26 2017"
			],
			"xref": [
				"Column 4 in A265584."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Nov 20 2006",
			"references": 5,
			"revision": 49,
			"time": "2017-10-28T10:46:52-04:00",
			"created": "2006-12-06T03:00:00-05:00"
		}
	]
}