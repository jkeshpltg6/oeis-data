{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A092921",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 92921,
			"data": "0,1,0,1,1,0,1,1,1,0,1,2,1,1,0,1,3,2,1,1,0,1,5,4,2,1,1,0,1,8,7,4,2,1,1,0,1,13,13,8,4,2,1,1,0,1,21,24,15,8,4,2,1,1,0,1,34,44,29,16,8,4,2,1,1,0,1,55,81,56,31,16,8,4,2,1,1,0,1,89,149,108,61,32,16,8,4,2,1,1,0",
			"name": "Array F(k, n) read by descending antidiagonals: k-generalized Fibonacci numbers in row k \u003e= 1, starting (0, 1, 1, ...), for column n \u003e= 0.",
			"comment": [
				"For all k \u003e= 1, the k-generalized Fibonacci number F(k,n) satisfies the recurrence obtained by adding more terms to the recurrence of the Fibonacci numbers.",
				"The number of tilings of an 1 X n rectangle with tiles of size 1 X 1, 1 X 2, ..., 1 X k is F(k,n).",
				"T(k,n) is the number of 0-balanced ordered trees with n edges and height k (height is the number of edges from root to a leaf). - _Emeric Deutsch_, Jan 19 2007",
				"Brlek et al. (2006) call this table \"number of psp-polyominoes with flat bottom\". - _N. J. A. Sloane_, Oct 30 2018"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A092921/b092921.txt\"\u003eAntidiagonals n = 0..140, flattened\u003c/a\u003e",
				"Srecko Brlek, Andrea Frosini, Simone Rinaldi, and Laurent Vuillon, \u003ca href=\"https://doi.org/10.37236/1041\"\u003eTilings by translation: enumeration by a rational language approach\u003c/a\u003e, The Electronic Journal of Combinatorics, vol. 13, (2006). Table 1 is essentially this array. - _N. J. A. Sloane_, Jul 20 2014",
				"E. S. Egge, \u003ca href=\"https://arxiv.org/abs/math/0109219\"\u003eRestricted permutations related to Fibonacci numbers and k-generalized Fibonacci numbers\u003c/a\u003e, arXiv:math/0109219 [math.CO], 2001.",
				"E. S. Egge, \u003ca href=\"https://arxiv.org/abs/math/0307050\"\u003eRestricted 3412-Avoiding Involutions\u003c/a\u003e, arXiv:math/0307050 [math.CO], 2003.",
				"E. S. Egge and T. Mansour, \u003ca href=\"https://arxiv.org/abs/math/0203226\"\u003eRestricted permutations, Fibonacci numbers and k-generalized Fibonacci numbers\u003c/a\u003e, arXiv:math/0203226 [math.CO], 2002.",
				"E. S. Egge and T. Mansour, \u003ca href=\"https://arxiv.org/abs/math/0209255\"\u003e231-avoiding involutions and Fibonacci numbers\u003c/a\u003e, arXiv:math/0209255 [math.CO], 2002.",
				"Nathaniel D. Emerson, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL9/Emerson/emerson6.html\"\u003eA Family of Meta-Fibonacci Sequences Defined by Variable-Order Recursions\u003c/a\u003e, Journal of Integer Sequences, Vol. 9 (2006), Article 06.1.8.",
				"Abraham Flaxman, Aram W. Harrow, and Gregory B. Sorkin, \u003ca href=\"https://doi.org/10.37236/1761\"\u003eStrings with Maximally Many Distinct Subsequences and Substrings\u003c/a\u003e, Electronic J. Combinatorics 11 (1) (2004), Paper R8.",
				"I. Flores, \u003ca href=\"http://www.fq.math.ca/Scanned/5-3/flores.pdf\"\u003ek-Generalized Fibonacci numbers\u003c/a\u003e, Fib. Quart., 5 (1967), 258-266.",
				"H. Gabai, \u003ca href=\"http://www.fq.math.ca/Scanned/8-1/gabai.pdf\"\u003eGeneralized Fibonacci k-sequences\u003c/a\u003e, Fib. Quart., 8 (1970), 31-38.",
				"R. Kemp, \u003ca href=\"http://dx.doi.org/10.1002/rsa.3240050111\"\u003eBalanced ordered trees\u003c/a\u003e, Random Structures and Alg., 5 (1994), pp. 99-121.",
				"E. P. Miles jr., \u003ca href=\"http://www.jstor.org/stable/2308649\"\u003eGeneralized Fibonacci numbers and associated matrices\u003c/a\u003e, The Amer. Math. Monthly, 67 (1960) 745-752.",
				"M. D. Miller, \u003ca href=\"http://www.jstor.org/stable/2316316\"\u003eOn generalized Fibonacci numbers\u003c/a\u003e, The Amer. Math. Monthly, 78 (1971) 1108-1109."
			],
			"formula": [
				"F(k,n) = F(k,n-1) + F(k,n-2) + ... + F(k,n-k); F(k,1) = 1 and F(k,n) = 0 for n \u003c= 0.",
				"G.f.: x/(1-Sum_{i=1..k} x^i).",
				"F(k,n) = 2^(n-2) for 1 \u003c n \u003c= k+1. - _M. F. Hasler_, Apr 20 2018"
			],
			"example": [
				"From _Peter Luschny_, Apr 03 2021: (Start)",
				"Array begins:",
				"              n = 0  1  2  3  4  5   6   7   8    9   10",
				"-------------------------------------------------------------",
				"[k=1, mononacci ] 0, 1, 1, 1, 1, 1,  1,  1,  1,   1,   1, ...",
				"[k=2, Fibonacci ] 0, 1, 1, 2, 3, 5,  8, 13, 21,  34,  55, ...",
				"[k=3, tribonacci] 0, 1, 1, 2, 4, 7, 13, 24, 44,  81, 149, ...",
				"[k=4, tetranacci] 0, 1, 1, 2, 4, 8, 15, 29, 56, 108, 208, ...",
				"[k=5, pentanacci] 0, 1, 1, 2, 4, 8, 16, 31, 61, 120, 236, ...",
				"[k=6]             0, 1, 1, 2, 4, 8, 16, 32, 63, 125, 248, ...",
				"[k=7]             0, 1, 1, 2, 4, 8, 16, 32, 64, 127, 253, ...",
				"[k=8]             0, 1, 1, 2, 4, 8, 16, 32, 64, 128, 255, ...",
				"[k=9]             0, 1, 1, 2, 4, 8, 16, 32, 64, 128, 256, ...",
				"Note that the first parameter in F(k, n) refers to rows, and the second parameter refers to columns. This is always the case. Only the usual naming convention for the indices is not adhered to because it is common to call the row sequences k-bonacci numbers. (End)",
				".",
				"From _Peter Luschny_, Aug 12 2015: (Start)",
				"As a triangle counting compositions of n with largest part k:",
				"n\\k]| [0][1] [2] [3] [4][5][6][7][8][9]",
				"[0] | [0]",
				"[1] | [0, 1]",
				"[2] | [0, 1,  1]",
				"[3] | [0, 1,  1,  1]",
				"[4] | [0, 1,  2,  1,  1]",
				"[5] | [0, 1,  3,  2,  1, 1]",
				"[6] | [0, 1,  5,  4,  2, 1, 1]",
				"[7] | [0, 1,  8,  7,  4, 2, 1, 1]",
				"[8] | [0, 1, 13, 13,  8, 4, 2, 1, 1]",
				"[9] | [0, 1, 21, 24, 15, 8, 4, 2, 1, 1]",
				"For example for n=7 and k=3 we have the 7 compositions [3, 3, 1], [3, 2, 2], [3, 2, 1, 1], [3, 1, 3], [3, 1, 2, 1], [3, 1, 1, 2], [3, 1, 1, 1, 1].",
				"(End)"
			],
			"maple": [
				"F:= proc(k, n) option remember; `if`(n\u003c2, n,",
				"      add(F(k, n-j), j=1..min(k,n)))",
				"    end:",
				"seq(seq(F(k, d+1-k), k=1..d+1), d=0..12);  # _Alois P. Heinz_, Nov 02 2016",
				"# Based on the above function:",
				"Arow := (k, len) -\u003e seq(F(k, j), j = 0..len):",
				"seq(lprint(Arow(k, 14)), k = 1..10); # _Peter Luschny_, Apr 03 2021"
			],
			"mathematica": [
				"F[k_, n_] := F[k, n] = If[n\u003c2, n, Sum[F[k, n-j], {j, 1, Min[k, n]}]];",
				"Table[F[k, d+1-k], {d, 0, 12}, {k, 1, d+1}] // Flatten (* _Jean-François Alcover_, Jan 11 2017, translated from Maple *)"
			],
			"program": [
				"(PARI) F(k,n)=if(n\u003c2,if(n\u003c1,0,1),sum(i=1,k,F(k,n-i)))",
				"(PARI) T(m,n)=!!n*(matrix(m,m,i,j,j==i+1||i==m)^(n+m-2))[1,m] \\\\ _M. F. Hasler_, Apr 20 2018",
				"(PARI) F(k,n) = if(n==0,0, polcoeff(lift(Mod('x, Pol(vector(k+1,i, if(i==1,1,-1))))^(n+k-2)), k-1)); \\\\ _Kevin Ryde_, Jun 05 2020",
				"(Sage)",
				"# As a triangle of compositions of n with largest part k.",
				"C = lambda n,k: Compositions(n, max_part=k, inner=[k]).cardinality()",
				"for n in (0..9): [C(n,k) for k in (0..n)] # _Peter Luschny_, Aug 12 2015"
			],
			"xref": [
				"Columns converge to A166444: each column n converges to A166444(n) = 2^(n-2).",
				"Rows 1-8 are (shifted) A057427, A000045, A000073, A000078, A001591, A001592, A066178, A079262.",
				"Essentially a reflected version of A048887.",
				"See A048004 and A126198 for closely related arrays.",
				"Cf. A066099."
			],
			"keyword": "nonn,tabl",
			"offset": "0,12",
			"author": "_Ralf Stephan_, Apr 17 2004",
			"references": 19,
			"revision": 85,
			"time": "2021-12-04T17:32:04-05:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}