{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A336624",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 336624,
			"data": "0,15,66,17391,76245,20069280,87986745,23159931810,101536627566,26726541239541,117173180224500,30842405430498585,135217748442445515,35592109140254127630,156041164529401899891,41073263105447832786516,180071368649181350028780,47398510031577658781511915",
			"name": "Triangular numbers that are one-eighth of other triangular numbers; T(t) such that 8*T(t)=T(u) for some u where T(k) is the k-th triangular number.",
			"comment": [
				"The triangular numbers T(t) that are one-eighth of other triangular numbers T(u) : T(t)=T(u)/8. The t's are in A336623, the T(u)'s are in A336626 and the u's are in A336625.",
				"Can be defined for negative n by setting a(n) = a(-1-n) for all n in Z."
			],
			"link": [
				"Vladimir Pletser, \u003ca href=\"/A336624/b336624.txt\"\u003eTable of n, a(n) for n = 0..650\u003c/a\u003e",
				"Vladimir Pletser, \u003ca href=\"https://arxiv.org/abs/2101.00998\"\u003eRecurrent Relations for Multiple of Triangular Numbers being Triangular Numbers\u003c/a\u003e, arXiv:2101.00998 [math.NT], 2021.",
				"Vladimir Pletser, \u003ca href=\"https://arxiv.org/abs/2102.12392\"\u003eClosed Form Equations for Triangular Numbers Multiple of Other Triangular Numbers\u003c/a\u003e, arXiv:2102.12392 [math.GM], 2021.",
				"Vladimir Pletser, \u003ca href=\"https://arxiv.org/abs/2102.13494\"\u003eTriangular Numbers Multiple of Triangular Numbers and Solutions of Pell Equations\u003c/a\u003e, arXiv:2102.13494 [math.NT], 2021.",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,1154,-1154,-1,1)."
			],
			"formula": [
				"a(n) = 1154*a(n-2) - a(n-4) + 81, for n\u003e=2 with a(1)=15, a(0)=0, a(-1)=0, a(-2)=15.",
				"a(n) = a(n-1) + 1154*a(n-2) - 1154*a(n-3) - a(n-4) + a(n-5), for n\u003e=3 with a(2)=66, a(1)=15, a(0)=0, a(-1)=0, a(-2)=15.",
				"a(n) = b(n)*(b(n)+1)/2 where b(n) is A336623(n).",
				"G.f.: 3*x*(5 + 17*x + 5*x^2) / ((1 - x)*(1 - 34*x + x^2)*(1 + 34*x + x^2)). - _Colin Barker_, Aug 08 2020",
				"a(n) = ((sqrt(2) + 1)^(4*n + 2) * (11 - 6*(-1)^n*sqrt(2)) + (sqrt(2) - 1)^(4*n + 2) * (11 + 6*(-1)^n*sqrt(2)) - 18)/256. - _Vaclav Kotesovec_, Sep 08 2020",
				"From _Vladimir Pletser_, Feb 21 2021: (Start)",
				"a(n) = ((11 - 6*sqrt(2))*(1 + sqrt(2))^(4n + 2) + (11 + 6*sqrt(2))*(1 - sqrt(2) )^(4n + 2) - 18) / 256 for even n.",
				"a(n) = ((11 + 6*sqrt(2))*(1 + sqrt(2) )^(4n + 2) + (11 - 6*sqrt(2))*(1 - sqrt(2) )^(4n + 2) - 18) / 256 for odd n. (End)"
			],
			"example": [
				"a(1)= 15 is a term because it is triangular and 8*15 = 120 is also triangular.",
				"a(2) = 1154*a(0) - a(-2) + 81 = 0 - 15 + 81 = 66;",
				"a(3) = 1154*a(1) - a(-1) + 81 = 1154*15 - 0 + 81 = 17391, etc."
			],
			"maple": [
				"f := gfun:-rectoproc({a(n) = 1154*a(n - 2) - a(n - 4) + 81, a(1) = 15, a(0) = 0, a(-1) = 0, a(-2) = 15}, a(n), remember): map(f, [$ (0 .. 40)])[]; #"
			],
			"mathematica": [
				"LinearRecurrence[{1, 1154, -1154, -1, 1}, {0, 15, 66, 17391, 76245}, 18] (* _Amiram Eldar_, Aug 08 2020 *)",
				"FullSimplify[Table[((Sqrt[2] + 1)^(4*n + 2)*(11 - 6*(-1)^n*Sqrt[2]) + (Sqrt[2] - 1)^(4*n + 2)*(11 + 6*(-1)^n*Sqrt[2]) - 18)/256, {n, 0, 17}]] (* _Vaclav Kotesovec_, Sep 08 2020 *)"
			],
			"program": [
				"(PARI) concat(0, Vec(3*x*(5 + 17*x + 5*x^2) / ((1 - x)*(1 - 34*x + x^2)*(1 + 34*x + x^2)) + O(x^40))) \\\\ _Colin Barker_, Aug 08 2020"
			],
			"xref": [
				"Subsequence of A000217.",
				"Cf. A336623, A336626, A336625.",
				"Cf. A053141, A001652, A075528, A029549, A061278, A001571, A076139, A076140, A077259, A077262, A077260, A077261, A077288, A077291, A077289, A077290, A077398, A077401, A077399, A077400."
			],
			"keyword": "easy,nonn",
			"offset": "0,2",
			"author": "_Vladimir Pletser_, Aug 07 2020",
			"references": 8,
			"revision": 49,
			"time": "2021-05-13T01:10:39-04:00",
			"created": "2020-10-08T01:32:03-04:00"
		}
	]
}