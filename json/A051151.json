{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A051151",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 51151,
			"data": "1,-6,1,72,-18,1,-1296,396,-36,1,31104,-10800,1260,-60,1,-933120,355104,-48600,3060,-90,1,33592320,-13716864,2104704,-158760,6300,-126,1,-1410877440,609700608,-102114432,8772624,-423360,11592,-168",
			"name": "Generalized Stirling number triangle of first kind.",
			"comment": [
				"a(n,m) = R_n^m(a=0, b=6) in the notation of the given 1961 and 1962 references.",
				"a(n,m) is a Jabotinsky matrix, i.e., the monic row polynomials E(n,x) := Sum_{m=1..n} a(n,m)*x^m = Product_{j=0..n-1} (x-6*j), n \u003e= 1, and E(0,x) := 1 are exponential convolution polynomials (see A039692 for the definition and a Knuth reference).",
				"This is the signed Stirling1 triangle A008275 with diagonal d \u003e= 0 (main diagonal d = 0) scaled with 6^d."
			],
			"link": [
				"Wolfdieter Lang, \u003ca href=\"/A051151/a051151.txt\"\u003eFirst 10 rows\u003c/a\u003e.",
				"D. S. Mitrinovic, \u003ca href=\"https://gallica.bnf.fr/ark:/12148/bpt6k762d/f996.image.r=1961%20mitrinovic\"\u003eSur une classe de nombres reliés aux nombres de Stirling\u003c/a\u003e, Comptes rendus de l'Académie des sciences de Paris, t. 252 (1961), 2354-2356. [The numbers R_n^m(a,b) are first introduced.]",
				"D. S. Mitrinovic and R. S. Mitrinovic, \u003ca href=\"https://www.jstor.org/stable/43667130\"\u003eTableaux d'une classe de nombres reliés aux nombres de Stirling\u003c/a\u003e, Univ. Beograd. Publ. Elektrotehn. Fak. Ser. Mat. Fiz., No. 77 (1962), 1-77. [Special cases of the numbers R_n^m(a,b) are tabulated.]"
			],
			"formula": [
				"a(n, m) = a(n-1, m-1) - 6*(n-1)*a(n-1, m), n \u003e= m \u003e= 1; a(n, m) := 0, n \u003c m; a(n, 0) := 0 for n \u003e= 1; a(0, 0) = 1.",
				"E.g.f. for the m-th column of the signed triangle: (log(1 + 6*x)/6)^m)/m!.",
				"a(n, m) = S1(n, m)*6^(n-m), with S1(n, m) := A008275(n, m) (signed Stirling1 triangle)."
			],
			"example": [
				"Triangle a(n,m) (with rows n \u003e= 1 and columns m = 1..n) begins:",
				"        1;",
				"       -6,      1;",
				"       72,    -18,      1;",
				"    -1296,    396,    -36,    1;",
				"    31104, -10800,   1260,  -60,   1;",
				"  -933120, 355104, -48600, 3060, -90, 1;",
				"   ...",
				"3rd row o.g.f.: E(3,x) = 72*x - 18*x^2 + x^3."
			],
			"xref": [
				"First (m=1) column sequence is: A047058(n-1).",
				"Row sums (signed triangle): A008543(n-1)*(-1)^(n-1).",
				"Row sums (unsigned triangle): A008542(n).",
				"Cf. A008275 (Stirling1 triangle, b=1), A039683 (b=2), A051141 (b=3), A051142 (b=4), A051150 (b=5)."
			],
			"keyword": "sign,easy,tabl",
			"offset": "1,2",
			"author": "_Wolfdieter Lang_",
			"ext": [
				"Various sections edited by _Petros Hadjicostas_, Jun 08 2020"
			],
			"references": 7,
			"revision": 23,
			"time": "2020-06-08T17:03:40-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}