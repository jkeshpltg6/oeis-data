{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A347285",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 347285,
			"data": "0,1,2,1,3,1,4,2,1,5,3,2,1,6,3,2,1,7,4,2,1,8,5,3,2,1,9,5,3,2,1,10,6,4,3,2,1,11,6,4,3,2,1,12,7,4,3,2,1,13,8,5,4,3,2,1,14,8,5,4,3,2,1,15,9,6,4,3,2,1,16,10,6,4,3,2,1,17,10,6,4,3,2,1",
			"name": "Irregular triangle T(n,k) starting with n followed by e_k = floor(log_p_k(p_(k-1)^e_(k-1))) such that e_k \u003e 0.",
			"comment": [
				"Irregular triangle T(n,k) starting with n followed by e_k corresponding to the largest 1 \u003c p_k^e_k \u003c p_(k-1)^e_(k-1).",
				"T(0,1) = 0 by convention; 0 is not allowed for n \u003e 0.",
				"T(n,k) \u003e T(n,k+1). The least first difference among row n is 1.",
				"Conjecture: let S be the sum of the absolute values of the first differences of terms in row n. For all n \u003e 0, n - S = 1. - _Michael De Vlieger_, Aug 27 2021"
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A347285/b347285.txt\"\u003eTable of n, a(n) for n = 0..10367\u003c/a\u003e (rows 0 \u003c= n \u003c= 300, flattened)"
			],
			"formula": [
				"T(n,1) = n; T(n,k) = floor(log_p_k(p_(k-1)^T(n,k-1))).",
				"A000217(A089576(n)) \u003c= sum of terms in row n."
			],
			"example": [
				"Row 0 contains {0} by convention.",
				"Row 1 contains {1} since we can find no nonzero exponent e such that 3^e \u003c 2^1.",
				"Row 2 contains {2,1} since 3^1 \u003c 2^2 yet 3^2 \u003e 2^2. (We assume hereinafter that the powers listed are the largest possible smaller than the immediately previous term.)",
				"Row 3 contains {3,1} since 2^3 \u003e 3^1.",
				"Row 4 contains {4,2,1} since 2^4 \u003e 3^2 \u003e 5^1, etc.",
				"Triangle begins:",
				"   0",
				"   1",
				"   2   1",
				"   3   1",
				"   4   2  1",
				"   5   3  2  1",
				"   6   3  2  1",
				"   7   4  2  1",
				"   8   5  3  2  1",
				"   9   5  3  2  1",
				"  10   6  4  3  2  1",
				"  11   6  4  3  2  1",
				"  12   7  4  3  2  1",
				"  13   8  5  4  3  2  1",
				"  14   8  5  4  3  2  1",
				"  15   9  6  4  3  2  1",
				"  16  10  6  4  3  2  1",
				"  ..."
			],
			"mathematica": [
				"Array[NestWhile[Block[{p = Prime[#2]}, Append[#1, {p^#, #} \u0026@ Floor@ Log[p, #1[[-1, 1]]]]] \u0026 @@ {#, Length@ # + 1} \u0026, {{2^#, #}}, #[[-1, -1]] \u003e 1 \u0026][[All, -1]] \u0026, 18, 0] // Flatten"
			],
			"xref": [
				"Cf. A000217, A000961, A089576 (row lengths)."
			],
			"keyword": "nonn,tabf,easy",
			"offset": "0,3",
			"author": "_Michael De Vlieger_, Aug 26 2021",
			"references": 7,
			"revision": 17,
			"time": "2021-09-08T09:52:43-04:00",
			"created": "2021-08-27T13:04:44-04:00"
		}
	]
}