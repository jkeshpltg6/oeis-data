{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A269910",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 269910,
			"data": "1,5,1,45,1,117,1,221,1,357,1,525,1,725,1,957,1,1221,1,1517,1,1845,1,2205,1,2597,1,3021,1,3477,1,3965,1,4485,1,5037,1,5621,1,6237,1,6885,1,7565,1,8277,1,9021,1,9797,1,10605,1,11445,1,12317,1,13221,1",
			"name": "Number of active (ON,black) cells in n-th stage of growth of two-dimensional cellular automaton defined by \"Rule 3\", based on the 5-celled von Neumann neighborhood.",
			"comment": [
				"Initialized with a single black (ON) cell at stage zero."
			],
			"reference": [
				"S. Wolfram, A New Kind of Science, Wolfram Media, 2002; p. 170."
			],
			"link": [
				"Robert Price, \u003ca href=\"/A269910/b269910.txt\"\u003eTable of n, a(n) for n = 0..128\u003c/a\u003e",
				"Robert Price, \u003ca href=\"/A269910/a269910.tmp.txt\"\u003eDiagrams of the first 20 stages.\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"http://arxiv.org/abs/1503.01168\"\u003eOn the Number of ON Cells in Cellular Automata\u003c/a\u003e, arXiv:1503.01168 [math.CO], 2015",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ElementaryCellularAutomaton.html\"\u003eElementary Cellular Automaton\u003c/a\u003e",
				"S. Wolfram, \u003ca href=\"http://wolframscience.com/\"\u003eA New Kind of Science\u003c/a\u003e",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_2D_5-Neighbor_Cellular_Automata\"\u003eIndex to 2D 5-Neighbor Cellular Automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_Elementary_Cellular_Automata\"\u003eIndex to Elementary Cellular Automata\u003c/a\u003e"
			],
			"formula": [
				"Conjectures from _Colin Barker_, Mar 08 2016: (Start)",
				"a(n) = (-1+2*(-1)^n-2*(-1+(-1)^n)*n-2*(-1+(-1)^n)*n^2).",
				"a(n) = 1 for n even.",
				"a(n) = 4*n^2+4*n-3 for n odd.",
				"a(n) = 3*a(n-2)-3*a(n-4)+a(n-6) for n\u003e5.",
				"G.f.: (1+5*x-2*x^2+30*x^3+x^4-3*x^5) / ((1-x)^3*(1+x)^3).",
				"(End)"
			],
			"mathematica": [
				"CAStep[rule_,a_]:=Map[rule[[10-#]]\u0026,ListConvolve[{{0,2,0},{2,1,2},{0,2,0}},a,2],{2}];",
				"code=3; stages=128;",
				"rule=IntegerDigits[code,2,10];",
				"g=2*stages+1; (* Maximum size of grid *)",
				"a=PadLeft[{{1}},{g,g},0,Floor[{g,g}/2]]; (* Initial ON cell on grid *)",
				"ca=a;",
				"ca=Table[ca=CAStep[rule,ca],{n,1,stages+1}];",
				"PrependTo[ca,a];",
				"(* Trim full grid to reflect growth by one cell at each stage *)",
				"k=(Length[ca[[1]]]+1)/2;",
				"ca=Table[Table[Part[ca[[n]][[j]],Range[k+1-n,k-1+n]],{j,k+1-n,k-1+n}],{n,1,k}];",
				"Map[Function[Apply[Plus,Flatten[#1]]],ca] (* Count ON cells at each stage *)"
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Robert Price_, Mar 07 2016",
			"references": 4,
			"revision": 14,
			"time": "2016-03-08T10:41:21-05:00",
			"created": "2016-03-08T06:34:56-05:00"
		}
	]
}