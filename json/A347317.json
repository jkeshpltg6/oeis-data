{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A347317",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 347317,
			"data": "0,1,1,0,2,2,2,0,3,2,4,1,1,0,4,4,4,1,4,0,5,5,4,1,6,2,1,0,6,7,5,1,6,3,3,1,0,7,9,5,3,6,4,4,2,0,1,0,9,10,6,4,9,4,5,2,0,3,1,0,11,11,7,5,10,6,6,3,0,3,2,2,0",
			"name": "An alternative version of the inventory sequence A342585: now a row only ends when a 0 is reached that would not be followed by any further terms.",
			"comment": [
				"This sequence has offset 0, which seems more appropriate than the offset 1 that A342585 has.",
				"In both A342585 and the present sequence, a row records the numbers of 0's, 1's, 2's, etc., in the sequence so far.",
				"The difference is that in A342585 a row ends when the first 0 is reached. The row beginning 7, for example, is 7,  9,  5,  3,  6,  4,  4,  2,  0, and ends, because there is no 8 in the sequence up to this point. However, there is a 9, so we can say that the row ended prematurely.",
				"In the present sequence we continue the row until we reach a 0 which is 1 more than the highest term in the sequence up to that point, and then the row ends.",
				"So the row beginning 7 is now 7,  9,  5,  3,  6,  4,  4,  2,  0,  1,  0.",
				"From this point on the two sequences differ.",
				"Unfortunately, this version has the drawback that most of the entries are zero!"
			],
			"link": [
				"Michael S. Branicky, \u003ca href=\"/A347317/b347317.txt\"\u003eTable of n, a(n) for n = 0..25000\u003c/a\u003e"
			],
			"example": [
				"The triangle begins:",
				"   0;",
				"   1,  1,  0;",
				"   2,  2,  2,  0;",
				"   3,  2,  4,  1,  1,  0;",
				"   4,  4,  4,  1,  4,  0;",
				"   5,  5,  4,  1,  6,  2,  1,  0;",
				"   6,  7,  5,  1,  6,  3,  3,  1,  0;",
				"   7,  9,  5,  3,  6,  4,  4,  2,  0,  1,  0;",
				"   9, 10,  6,  4,  9,  4,  5,  2,  0,  3,  1,  0;",
				"  11, 11,  7,  5, 10,  6,  6,  3,  0,  3,  2,  2,  0;",
				"..."
			],
			"mathematica": [
				"Block[{c, k, m, r = 0}, c[0] = 1; {0}~Join~Reap[Do[k = 0; While[k \u003c= r, If[IntegerQ@ c[k], Set[m, c[k]], Set[c[k], 0]; Set[m, 0]]; If[m \u003e r, r = m]; Sow[m]; If[IntegerQ@ c[m], c[m]++, c[m] = 1]; k++]; Sow[0]; c[0]++, 9]][[-1, -1]]] (* _Michael De Vlieger_, Oct 12 2021 *)"
			],
			"program": [
				"(Python)",
				"from collections import Counter",
				"def aupton(terms):",
				"    num, alst, inventory = 0, [0], Counter([0])",
				"    for n in range(2, terms+1):",
				"        c = inventory[num]",
				"        if c == 0 and num \u003e max(inventory):",
				"            num = 0",
				"        else:",
				"            num += 1",
				"        alst.append(c); inventory.update([c])",
				"    return alst",
				"print(aupton(73)) # _Michael S. Branicky_, Sep 09 2021"
			],
			"xref": [
				"Cf. A342585. See A347318 for row lengths."
			],
			"keyword": "nonn,tabf",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_, Sep 09 2021",
			"references": 5,
			"revision": 31,
			"time": "2021-10-13T10:26:39-04:00",
			"created": "2021-09-09T19:11:25-04:00"
		}
	]
}