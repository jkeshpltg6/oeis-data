{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A080358",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 80358,
			"data": "1,1,6,240,414720,4379443200,2648687247360000,11619303595714805760000,4047756373260469165621248000000,311107430628520522709128328175943680000000,152539657943794787580793302587123569672794931200000000",
			"name": "Value of Vandermonde determinant for the first n prime numbers: V[p(1),...,p(n)].",
			"comment": [
				"Value of Vandermonde determinant is unchanged if the numbers are shifted by an arbitrary c constant, i.e., v[p(1), ..., p(n)] = v[p(1)-c, ..., p(n)-c].",
				"For a guide to related sequences, see A093883. - _Clark Kimberling_, Jan 03 2012"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A080358/b080358.txt\"\u003eTable of n, a(n) for n = 1..36\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Product[p(i)-p(j)]; i, j, i\u003ej. for n, it is product of C(n, 2) prime differences of not necessarily consecutive primes."
			],
			"example": [
				"a(1)=1 corresponds to 1 X 1 V-matrix, while a(2)=1 computed from 2 X 2 matrix.",
				"n = 2: a(2) = p(2) - p(1) = 3-2 = 1;",
				"n = 3: a(3) = (5-3)*(5-2)*(3-2) = 2*3*1 = 6; ...",
				"n = 6: a(6) = (13-11)*(13-7)*(13-5)*(13-3)*(13-2)*(11-7)*(11-5)*(11-3)*(11-2)*(7-5)*(7-3)*(7-2)*(5-3)*(5-2)*(3-2) = 2*6*8*10*11*4*6*8*9*2*4*5*2*3*1 = 10560*1728*40*6*1 = 4379443200."
			],
			"maple": [
				"with(LinearAlgebra):",
				"a:= n-\u003e Determinant(Matrix(n, (i,j)-\u003e ithprime(i)^(j-1))):",
				"seq(a(n), n=1..15);  # _Alois P. Heinz_, Jul 22 2017"
			],
			"mathematica": [
				"b[x_] := Prime[x] d[x_] := b[x+1]-b[x] t[m_] := b[m+1]-Table[b[x], {x, 1, m}] pt[x_] := Apply[Times, t[x]] va[x_] := Apply[Times, Table[pt[w], {w, 1, x}]] Table[va[j], {j, 1, 10}]"
			],
			"xref": [
				"Cf. A000040, A001359, A203521, A290179."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Labos Elemer_, Feb 19 2003",
			"references": 6,
			"revision": 21,
			"time": "2018-07-07T19:21:30-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}