{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A227971",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 227971,
			"data": "-1,2,8,32,96,-1024,512,2048,40960,32768,1572864,-33554432,2097152,8388608,234881024,536870912,20937965568,8589934592,34359738368,-73392401154048,549755813888,2199023255552,-8796093022208000,-1577385769486516224,11258999068426240",
			"name": "Determinant of the (p_n+1)/2 X (p_n+1)/2 matrix with (i,j)-entry (i,j=0,...,(p_n-1)/2) being the Legendre symbol((i+j)/p_n), where p_n is the n-th prime.",
			"comment": [
				"Conjecture: If p_n == 1 (mod 4), then a(n) == ((p_n-1)/2)! (mod p_n). If p_n == 3 (mod 4), then a(n) == (2/p_n) (mod p_n).",
				"Zhi-Wei Sun also made the following general conjecture:",
				"Let p be any odd prime. For each integer d let R(d,p) be the determinant of the (p+1)/2-by-(p+1)/2 matrix whose (i,j)-entry (i,j = 0,...,(p-1)/2) is the Legendre symbol ((i+d*j)/p). When p == 3 (mod 4), we have R(d,p) == (2/p) (mod p) if (d/p) = 1, and R(d,p) == 1 (mod p) if (d/p) = -1. In the case p == 1 (mod 4), we have R(c^2*d,p) == (c/p)*R(d,p) (mod p) for any integer c, and R(d,p) == 1 or -1 (mod p) if (d/p) = -1.",
				"The author could prove that for any odd prime p and integer d not divisible by p, the determinant of the (p-1)-by-(p-1) matrix with (i,j)-entry (i,j=1,...,p-1) being the Legendre symbol ((i+dj)/p) has the exact value (-d/p)*p^{(p-3)/2}.",
				"On August 19 2013, Zhi-Wei Sun found a formula for a(n). Namely, he made the following conjecture: If p_n == 1 (mod 4) and e(p_n)^{h(p_n)} = (a_n + b_n*sqrt(p_n))/2 with a_n and b_n integers of the same parity (where e(p_n) and h(p_n) are the fundamental unit and the class number of the quadratic field Q(sqrt(p_n)) respectively), then a(n) = - (2/p_n)*2^{(p_n-3)/2}*a_n. If p_n \u003e 3 and p_n == 3 (mod 4), then  a(n) = 2^{(p_n-1)/2}.",
				"On August 19 2013, Zhi-Wei Sun proved all the conjectured congruences mentioned above by using the identity D(c,d,n) = (-d)^{n*(n+1)/2}*(n!)^{n+1}, where D(c,d,n) is the (n+1) X (n+1) determinant with (i,j)-entry equal to (i+d*j+c)^n for all i,j = 0,...,n. For any prime p == 1 (mod 4) he showed that R(d,p) == (d*(d/p))^{(p-1)/4}*((p-1)/2)! (mod p). Note also that the formula for a(n) found by Sun on August 9, 2013 is actually equivalent to Chapman's result on the evaluation of the determinant |((i+j-1)/p)|_{i,j=1,...,(p+1)/2}."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A227971/b227971.txt\"\u003eTable of n, a(n) for n = 2..100\u003c/a\u003e",
				"L. Carlitz, \u003ca href=\"http://matwbn.icm.edu.pl/ksiazki/aa/aa5/aa536.pdf\"\u003eSome cyclotomic matrices\u003c/a\u003e, Acta Arith. 5(1959), 293-308.",
				"Robin Chapman, \u003ca href=\"http://dx.doi.org/10.4064/aa115-3-4\"\u003eDeterminants of Legendre symbol matrices\u003c/a\u003e, Acta Arith. 115 (2004), 231-244.",
				"Zhi-Wei Sun, \u003ca href=\"http://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;d4d29df6.1307\"\u003eA conjecture on Legendre symbol determinants\u003c/a\u003e, a message to Number Theory List, July 17, 2013.",
				"Zhi-Wei Sun, \u003ca href=\"https://arxiv.org/abs/1308.2900\"\u003eOn some determinants with Legendre symbol entries\u003c/a\u003e, arXiv:1308.2900 [math.NT], 2010-2013; Finite Fields Appl. 56(2019), 285-307,",
				"M. Vseminov, \u003ca href=\"http://dx.doi.org/10.1016/j.laa.2011.08.039\"\u003eOn the evaluation of R. Chapman's \"evil determinant\"\u003c/a\u003e, Linear Algebra Appl. 436(2012), 4101-4106.",
				"M. Vseminov, \u003ca href=\"http://dx.doi.org/10.4064/aa159-4-3\"\u003eOn R. Chapman's \"evil determinant\": case p == 1 (mod 4)\u003c/a\u003e, Acta Arith. 159 (2013), 331-344."
			],
			"example": [
				"a(2) = -1 since the determinant |((i+j)/3)|_{i=0,1; j=0,1}| equals -1."
			],
			"mathematica": [
				"a[n_] := Det[Table[JacobiSymbol[i+j, Prime[n]], {i, 0, (Prime[n]-1)/2}, {j, 0, (Prime[n]-1)/2}]]; Table[a[n], {n, 2, 30}]"
			],
			"program": [
				"(PARI) a(n) = my(p=prime(n)); matdet(matrix((p+1)/2, (p+1)/2, i, j, i--; j--; kronecker(i+j, p))); \\\\ _Michel Marcus_, Aug 25 2021"
			],
			"xref": [
				"Cf. A176113, A179071, A179072, A226163, A227609, A227968, A228005, A228077."
			],
			"keyword": "sign",
			"offset": "2,2",
			"author": "_Zhi-Wei Sun_, Aug 01 2013",
			"references": 8,
			"revision": 55,
			"time": "2021-08-25T06:56:38-04:00",
			"created": "2013-08-02T03:23:55-04:00"
		}
	]
}