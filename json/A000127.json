{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000127",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 127,
			"id": "M1119 N0427",
			"data": "1,2,4,8,16,31,57,99,163,256,386,562,794,1093,1471,1941,2517,3214,4048,5036,6196,7547,9109,10903,12951,15276,17902,20854,24158,27841,31931,36457,41449,46938,52956,59536,66712,74519,82993,92171,102091,112792,124314,136698",
			"name": "Maximal number of regions obtained by joining n points around a circle by straight lines. Also number of regions in 4-space formed by n-1 hyperplanes.",
			"comment": [
				"a(n) is the sum of the first five terms in the n-th row of Pascal's triangle. - _Geoffrey Critzer_, Jan 18 2009",
				"{a(k): 1 \u003c= k \u003c= 5} = divisors of 16. - _Reinhard Zumkeller_, Jun 17 2009",
				"Equals binomial transform of [1, 1, 1, 1, 1, 0, 0, 0, ...]. - _Gary W. Adamson_, Mar 02 2010",
				"From _Bernard Schott_, Apr 05 2021: (Start)",
				"As a(n) = 2^(n-1) for n = 1..5, it is misleading to believe that a(n) = 2^(n-1) for n \u003e 5 (see Patrick Popescu-Pampu link); other curiosities: a(6) = 2^5 - 1 and a(10) = 2^8.",
				"The sequence of the first differences is A000125, the sequence of the second differences is A000124, the sequence of the third differences is A000027 and the sequence of the fourth differences is the all 1's sequence A000012 (see J. H. Conway and R. K. Guy reference, p. 80). (End)"
			],
			"reference": [
				"R. B. Banks, Slicing Pizzas, Racing Turtles and Further Adventures in Applied Mathematics, Princeton Univ. Press, 1999. See p. 28.",
				"L. Comtet, Advanced Combinatorics, Reidel, 1974, p. 72, Problem 2.",
				"J. H. Conway and R. K. Guy, The Book of Numbers, Copernicus Press, NY, 1996, Chap. 3.",
				"J. H. Conway and R. K. Guy, Le Livre des Nombres, Eyrolles, 1998, p. 80.",
				"J.-M. De Koninck \u0026 A. Mercier, 1001 Problèmes en Théorie Classique Des Nombres, Problem 33 pp. 18; 128 Ellipses Paris 2004.",
				"A. Deledicq and D. Missenard, A La Recherche des Régions Perdues, Math. \u0026 Malices, No. 22 Summer 1995 issue pp. 22-3 ACL-Editions Paris.",
				"M. Gardner, Mathematical Circus, pp. 177; 180-1 Alfred A. Knopf NY 1979.",
				"M. Gardner, The Colossal Book of Mathematics, 2001, p. 561.",
				"James Gleick, Faster, Vintage Books, NY, 2000 (see pp. 259-261).",
				"M. de Guzman, Aventures Mathématiques, Prob. B pp. 115-120 PPUR Lausanne 1990.",
				"Ross Honsberger; Mathematical Gems I, Chap. 9.",
				"Ross Honsberger; Mathematical Morsels, Chap. 3.",
				"Jeux Mathématiques et Logiques, Vol. 3 pp. 12; 51 Prob. 14 FFJM-SERMAP Paris 1988.",
				"J. N. Kapur, Reflections of a Mathematician, Chap.36, pp. 337-343, Arya Book Depot, New Delhi 1996.",
				"C. D. Miller, V. E. Heeren, J. Hornsby, M. L. Morrow and J. Van Newenhizen, Mathematical Ideas, Tenth Edition, Pearson, Addison-Wesley, Boston, 2003, Cptr 1, 'The Art of Problem Solving, page 6.",
				"I. Niven, Mathematics of Choice, pp. 158; 195 Prob. 40 NML 15 MAA 1965.",
				"C. S. Ogilvy, Tomorrow's Math, pp. 144-6 OUP 1972.",
				"Alfred S. Posamentier \u0026 Ingmar Lehmann, The (Fabulous) Fibonacci Numbers, Prometheus Books, NY, 2007, page 81-87.",
				"A. M. Robert, A Course in p-adic Analysis, Springer-Verlag, 2000; p. 213.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A000127/b000127.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Alan Calvitti, \u003ca href=\"/A000127/a000127.jpg\"\u003eIllustration of initial terms\u003c/a\u003e",
				"M. L. Cornelius, \u003ca href=\"/A006261/a006261_1.pdf\"\u003eVariations on a geometric progression\u003c/a\u003e, Mathematics in School, 4 (No. 3, May 1975), p. 32. (Annotated scanned copy)",
				"Colin Defant, \u003ca href=\"https://arxiv.org/abs/2104.03890\"\u003eMeeting Covered Elements in nu-Tamari Lattices\u003c/a\u003e, arXiv:2104.03890 [math.CO], 2021.",
				"M. Griffiths, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL14/Griffiths2/griffiths17.html\"\u003eRemodified Bessel Functions via Coincidences and Near Coincidences\u003c/a\u003e, Journal of Integer Sequences, Vol. 14 (2011), Article 11.7.1.",
				"R. K. Guy, \u003ca href=\"/A005347/a005347.pdf\"\u003eThe Second Strong Law of Small Numbers\u003c/a\u003e, Math. Mag, 63 (1990), no. 1, 3-20. [Annotated scanned copy]",
				"R. K. Guy, \u003ca href=\"/A005165/a005165.pdf\"\u003eThe strong law of small numbers\u003c/a\u003e. Amer. Math. Monthly 95 (1988), no. 8, 697-712. [Annotated scanned copy]",
				"R. K. Guy, \u003ca href=\"/A000346/a000346.pdf\"\u003eLetter to N. J. A. Sloane\u003c/a\u003e",
				"D. A. Lind, \u003ca href=\"http://www.fq.math.ca/Scanned/3-4/lind.pdf\"\u003eOn a class of nonlinear binomial sums\u003c/a\u003e, Fib. Quart., 3 (1965), 292-298.",
				"Math Forum, \u003ca href=\"http://mathforum.org/library/drmath/view/55262.html\"\u003eRegions of a circle Cut by Chords to n points\u003c/a\u003e.",
				"R. J. Mathar, \u003ca href=\"/A247158/a247158.pdf\"\u003eThe number of binary nxm matrices with at most k 1's in each row or column\u003c/a\u003e, (2014) Table 4 column 1.",
				"Ângela Mestre and José Agapito, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL22/Mestre/mestre2.html\"\u003eSquare Matrices Generated by Sequences of Riordan Arrays\u003c/a\u003e, J. Int. Seq., Vol. 22 (2019), Article 19.8.4.",
				"Leo Moser and W. Bruce Ross, \u003ca href=\"http://www.jstor.org/stable/3219224\"\u003eMathematical Miscellany, On the Danger of Induction\u003c/a\u003e, Mathematics Magazine, Vol. 23, No. 2 (Nov. - Dec., 1949), pp. 109-114.",
				"M. Noy, \u003ca href=\"http://www.maa.org/programs/faculty-and-departments/classroom-capsules-and-notes/a-short-solution-of-a-problem-in-combinatorial-geometry\"\u003eA Short Solution of a Problem in Combinatorial Geometry\u003c/a\u003e, Mathematics Magazine, pp. 52-3 69(1) 1996 MAA.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"Patrick Popescu-Pampu, \u003ca href=\"http://images.math.cnrs.fr/Demarrage-trompeur.html\"\u003eDémarrage trompeur\u003c/a\u003e, Images des Mathématiques, CNRS, 2017, rediffusion 2021 (in French).",
				"D. J. Price, \u003ca href=\"http://www.jstor.org/stable/3609091\"\u003eSome unusual series occurring in n-dimensional geometry\u003c/a\u003e, Math. Gaz., 30 (1946), 149-150.",
				"H. P. Robinson, \u003ca href=\"/A002664/a002664.pdf\"\u003eLetter to N. J. A. Sloane, Mar 21 1985\u003c/a\u003e",
				"Grant Sanderson, \u003ca href=\"https://www.youtube.com/watch?v=K8P8uFahAgc\"\u003eCircle division solution\u003c/a\u003e, 3Blue1Brown video (2015).",
				"K. Uhland, \u003ca href=\"http://uhlandkf.homestead.com/files/PuzzlePage/198507Sol.htm\"\u003eA Blase of Glory\u003c/a\u003e [Broken link?]",
				"K. Uhland, \u003ca href=\"http://uhlandkf.homestead.com/files/PuzzlePage/199909sol.htm\"\u003eMoser's Problem\u003c/a\u003e [Broken link?]",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CircleDivisionbyChords.html\"\u003eCircle Division by Chords\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/StrongLawofSmallNumbers.html\"\u003eStrong Law of Small Numbers\u003c/a\u003e",
				"Reinhart Zumkeller, \u003ca href=\"/A161700/a161700.txt\"\u003eEnumerations of Divisors\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (5,-10,10,-5,1)."
			],
			"formula": [
				"a(n) = C(n-1, 4) + C(n-1, 3) + ... + C(n-1, 0) = A055795(n) + 1 = C(n, 4) + C(n-1, 2) + n.",
				"a(n) = Sum_{k=0..2} C(n, 2k). - Joel Sanderi (sanderi(AT)itstud.chalmers.se), Sep 08 2004",
				"a(n) = (n^4 - 6*n^3 + 23*n^2 - 18*n + 24)/24.",
				"G.f.: (1 - 3*x + 4*x^2 - 2*x^3 + x^4)/(1-x)^5. (for offset 0) - _Simon Plouffe_ in his 1992 dissertation",
				"E.g.f.: (1 + x + x^2/2 + x^3/6 + x^4/24)*exp(x) (for offset 0). [Typos corrected by _Juan M. Marquez_, Jan 24 2011]",
				"a(n) = 5*a(n-1) - 10*a(n-2) + 10*a(n-3) - 5*a(n-4) + a(n-5), n \u003e 4. - _Harvey P. Dale_, Aug 24 2011",
				"a(n) = A000124(A000217(n-1)) - n*A000217(n-2) - A034827(n), n \u003e 1. - _Melvin Peralta_, Feb 15 2016",
				"a(n) = A223718(-n). - _Michael Somos_, Dec 23 2017",
				"For n \u003e 2, a(n) = n + 1 + sum_{i=2..(n-2)}sum_{j=1..(n-i)}(1+(i-1)(j-1)). - _Alec Jones_, Nov 17 2019"
			],
			"example": [
				"a(7)=99 because the first five terms in the 7th row of Pascal's triangle are 1 + 7 + 21 + 35 + 35 = 99. - _Geoffrey Critzer_, Jan 18 2009",
				"G.f. = x + 2*x^2 + 4*x^3 + 8*x^4 + 16*x^5 + 31*x^6 + 57*x^7 + 99*x^8 + 163*x^9 + ..."
			],
			"maple": [
				"A000127 := n-\u003e(n^4 - 6*n^3 + 23*n^2 - 18*n + 24)/24;",
				"with (combstruct):ZL:=[S, {S=Sequence(U, card\u003cr), U=Set(Z, card\u003e=1)}, unlabeled]: seq(count(subs(r=6, ZL), size=m), m=0..41); # _Zerinvary Lajos_, Mar 08 2008"
			],
			"mathematica": [
				"f[n_] := Sum[Binomial[n, i], {i, 0, 4}]; Table[f@n, {n, 0, 40}] (* _Robert G. Wilson v_, Jun 29 2007 *)",
				"Total/@Table[Binomial[n-1,k],{n,50},{k,0,4}] (* or *) LinearRecurrence[ {5,-10,10,-5,1},{1,2,4,8,16},50] (* _Harvey P. Dale_, Aug 24 2011 *)",
				"Table[(n^4 - 6 n^3 + 23 n^2 - 18 n + 24) / 24, {n, 100}] (* _Vincenzo Librandi_, Feb 16 2015 *)",
				"a[ n_] := Binomial[n, 4] + Binomial[n, 2] + 1; (* _Michael Somos_, Dec 23 2017 *)"
			],
			"program": [
				"(Haskell)",
				"a000127 = sum . take 5 . a007318_row  -- _Reinhard Zumkeller_, Nov 24 2012",
				"(MAGMA) [(n^4-6*n^3+23*n^2-18*n+24)/24: n in [1..50]]; // _Vincenzo Librandi_, Feb 16 2015",
				"(PARI) a(n)=(n^4-6*n^3+23*n^2-18*n+24)/24 \\\\ _Charles R Greathouse IV_, Mar 22 2016",
				"(PARI) {a(n) = binomial(n, 4) + binomial(n, 2) + 1}; /* _Michael Somos_, Dec 23 2017 */",
				"(Python)",
				"def A000127(n): return n*(n*(n*(n - 6) + 23) - 18)//24 + 1 # _Chai Wah Wu_, Sep 18 2021"
			],
			"xref": [
				"Cf. A000012, A000027, A000124, A000125, A002522, A005408, A016813, A086514, A058331, A161701, A161702, A161703, A161704, A161706, A161707, A161708, A161710, A080856, A161711, A161712, A161713, A161715, A006261, A007318, A008859-A008863, A219531, A223718."
			],
			"keyword": "nonn,easy,nice",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Formula corrected and additional references from torsten.sillke(AT)lhsystems.com",
				"Additional correction from Jonas Paulson (jonasso(AT)sdf.lonestar.org), Oct 30 2003"
			],
			"references": 48,
			"revision": 193,
			"time": "2021-09-18T21:50:37-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}