{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A258835",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 258835,
			"data": "1,3,3,4,7,6,9,13,9,10,15,15,13,19,18,16,30,21,19,27,21,31,31,24,25,39,33,28,48,30,35,54,33,34,52,42,45,51,39,45,55,51,50,70,45,46,78,48,54,80,57,63,78,54,55,75,84,58,79,60,61,117,63,74,87,72,81",
			"name": "Expansion of psi(x)^3 * psi(x^4) in powers of x where psi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel and Muniru A Asiru, \u003ca href=\"/A258835/b258835.txt\"\u003eTable of n, a(n) for n = 0..20000\u003c/a\u003e(first 1000 terms from G. C. Greubel)",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(-7/8) * eta(q^2)^6 * eta(q^8)^2 / (eta(q)^3 * eta(q^4)) in powers of q.",
				"Euler transform of period 8 sequence [ 3, -3, 3, -2, 3, -3, 3, -4, ...].",
				"G.f.: Product_{k\u003e0} (1 - x^(2*k))^4 * (1 + x^k)^3 * (1 + x^(2*k)) * (1 + x^(4*k))^2.",
				"-8 * a(n) = A121613(4*n + 3). a(n) = sigma(8*n + 7) / 8."
			],
			"example": [
				"G.f. = 1 + 3*x + 3*x^2 + 4*x^3 + 7*x^4 + 6*x^5 + 9*x^6 + 13*x^7 + 9*x^8 + ...",
				"G.f. = q^7 + 3*q^15 + 3*q^23 + 4*q^31 + 7*q^39 + 6*q^47 + 9*q^55 + 13*q^63 + ..."
			],
			"maple": [
				"with(numtheory): seq(sigma(8*n-1)/8, n=1..1000); # _Muniru A Asiru_, Dec 31 2017"
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 0, 0, DivisorSigma[ 1, 8 n + 7] / 8];",
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 2, 0, x]^3 EllipticTheta[ 2, 0, x^4] / (16 x^(7/4)), {x, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, 0, sigma(8*n + 7) / 8)};",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A)^6 * eta(x^8 + A)^2 / (eta(x + A)^3 * eta(x^4 + A)), n))};",
				"(GAP) sequence := List([1..10^5],n-\u003eSigma(8*n-1)/8); # _Muniru A Asiru_, Dec 31 2017"
			],
			"xref": [
				"Cf. A121613."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Michael Somos_, Jun 11 2015",
			"references": 3,
			"revision": 21,
			"time": "2021-03-12T22:24:48-05:00",
			"created": "2015-06-11T21:34:12-04:00"
		}
	]
}