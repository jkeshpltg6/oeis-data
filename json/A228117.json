{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A228117",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 228117,
			"data": "1,1,2,2,3,4,4,6,7,9,10,16,14,23,24,33,33,50,50,71,75,101,103,146,151,201,211,280,292,389,409,519,573,707,765,960,1043,1276,1393,1704,1870,2258,2483,2970,3281,3920,4290,5101,5659,6640,7318,8628,9506,11081",
			"name": "Number of partitions of n that have hookset {1,2,...,k} for some k.",
			"comment": [
				"It appears to be the case that the difference between entry a(2n-1) and a(2n) is substantially less than the difference between a(2n) and a(2n+1), after a few initial exceptions."
			],
			"example": [
				"a(7) = 6, counting the partitions (7), (43), (331), (322), (2221), and (111111).  The hooklengths of (7) are {1,2,3,4,5,6,7}, and the hooklengths of (322) are {1,1,2,2,3,4,5}."
			],
			"maple": [
				"h:= proc(l) local n, s; n:=nops(l); s:= {seq(seq(1+l[i]-j",
				"       +add(`if`(l[k]\u003e=j, 1, 0), k=i+1..n), j=1..l[i]), i=1..n)};",
				"       `if`(s={$1..max(s[], 0)}, 1, 0)",
				"    end:",
				"g:= (n, i, l)-\u003e `if`(n=0 or i=1, h([l[], 1$n]), `if`(i\u003c1, 0,",
				"             g(n, i-1, l)+`if`(i\u003en, 0, g(n-i, i, [l[], i])))):",
				"a:= n-\u003e g(n$2, []):",
				"seq(a(n), n=0..30);  # _Alois P. Heinz_, Aug 12 2013"
			],
			"mathematica": [
				"\u003c\u003c \"Combinatorica`\"",
				"HookSet[Lambda_] := Module[{i, j, k, HookHolder},",
				"  HookHolder = {};",
				"  HS = {};",
				"  For[i = 1, i \u003c Length[Lambda] + 1, i++,",
				"   For[j = 1, j \u003c Lambda[[i]] + 1, j++,",
				"    CurrentHook =",
				"     Lambda[[i]] - j + TransposePartition[Lambda][[j]] - i + 1;",
				"    If[! MemberQ[HS, CurrentHook],",
				"     HookHolder = Append[HS, CurrentHook]; HS = HookHolder]",
				"    ]",
				"   ];",
				"  HookHolder = Sort[HS];",
				"  HS = HookHolder;",
				"  Return[HS]]",
				"For[i = 1, i \u003c 31, i++,",
				"For[j = 1, j \u003c PartitionsP[i] + 1, j++,",
				"  CurrSet=HookSet[Partitions[i][[j]]];",
				"  If[CurrSet == Table[i,{i,1,Length[CurrSet]}],",
				"   SGFHolder = SegGenFn + q^i;",
				"   SegGenFn = SGFHolder]",
				"  ]",
				"]",
				"(* second program: *)",
				"h[l_] := Module[{n, s}, n = Length[l]; s = Table[Table[1 + l[[i]] - j + Sum[If[l[[k]] \u003e= j, 1, 0], {k, i+1, n}], {j, 1, l[[i]]}], {i, 1, n}] // Flatten // Union; If[s == Range[Max[Append[s, 0]]], 1, 0]]; g[n_, i_, l_] := g[n, i, l] = If[n == 0 || i == 1, h[Join[l, Array[1\u0026, n]]], If[i\u003c1, 0, g[n, i-1, l] + If[i\u003en, 0, g[n-i, i, Append[l, i]]]]]; a[n_] := g[n, n, {}]; Table[an = a[n]; Print[\"a(\", n, \") = \", an]; an, {n, 0, 60}] (* _Jean-François Alcover_, Jan 22 2016, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A158291, the number of partitions which have hookset {1,2,...,n}, not counting multiplicities."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_William J. Keith_, Aug 10 2013",
			"ext": [
				"a(31)-a(53) from _Alois P. Heinz_, Aug 12 2013"
			],
			"references": 0,
			"revision": 17,
			"time": "2016-01-22T09:47:04-05:00",
			"created": "2013-08-13T04:03:59-04:00"
		}
	]
}