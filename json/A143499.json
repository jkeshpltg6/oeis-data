{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A143499",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 143499,
			"data": "1,8,1,72,18,1,720,270,30,1,7920,3960,660,44,1,95040,59400,13200,1320,60,1,1235520,926640,257400,34320,2340,78,1,17297280,15135120,5045040,840840,76440,3822,98,1,259459200,259459200,100900800,20180160,2293200",
			"name": "Triangle of unsigned 4-Lah numbers.",
			"comment": [
				"This is the case r = 4 of the unsigned r-Lah numbers L(r;n,k). The unsigned 4-Lah numbers count the partitions of the set {1,2,...,n} into k ordered lists with the restriction that the elements 1, 2, 3 and 4 belong to different lists. For other cases see A105278 (r = 1), A143497 (r = 2 and comments on the general case) and A143498 (r = 3).",
				"The unsigned 4-Lah numbers are related to the 4-Stirling numbers: the lower triangular array of unsigned 4-Lah numbers may be expressed as the matrix product St1(4) * St2(4), where St1(4) = A143493 and St2(4) = A143496 are the arrays of 4-restricted Stirling numbers of the first and second kind respectively. An alternative factorization for the array is as St1 * P^6 * St2, where P denotes Pascal's triangle, A007318, St1 is the triangle of unsigned Stirling numbers of the first kind, abs(A008275) and St2 denotes the triangle of Stirling numbers of the second kind, A008277."
			],
			"link": [
				"Erich Neuwirth, \u003ca href=\"http://homepage.univie.ac.at/erich.neuwirth/papers/TechRep99-05.pdf\"\u003eRecursively defined combinatorial functions: Extending Galton's board\u003c/a\u003e, Discrete Math. 239 No. 1-3, 33-51 (2001).",
				"G. Nyul, G. Rácz, \u003ca href=\"https://doi.org/10.1016/j.disc.2014.03.029\"\u003eThe r-Lah numbers\u003c/a\u003e, Discrete Mathematics, 338 (2015), 1660-1666.",
				"Michael J. Schlosser and Meesue Yoo, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v24i1p31\"\u003eElliptic Rook and File Numbers\u003c/a\u003e, Electronic Journal of Combinatorics, 24(1) (2017), #P1.31.",
				"M. Shattuck, \u003ca href=\"https://arxiv.org/abs/1412.8721\"\u003eGeneralized r-Lah numbers\u003c/a\u003e, arXiv:1412.8721 [math.CO], 2014"
			],
			"formula": [
				"T(n,k) = (n-4)!/(k-4)!*binomial(n+3,k+3), n,k \u003e= 4.",
				"Recurrence: T(n,k) = (n+k-1)*T(n-1,k) + T(n-1,k-1) for n,k \u003e= 4, with the boundary conditions: T(n,k) = 0 if n \u003c 4 or k \u003c 4; T(4,4) = 1.",
				"E.g.f. for column k: Sum_{n \u003e= k} T(n,k)*t^n/(n-4)! = 1/(k-4)!*t^k/(1-t)^(k+4) for k \u003e= 4.",
				"E.g.f: Sum_{n = 4..inf} Sum_{k = 4..n} T(n,k)*x^k*t^n/(n-4)! = (x*t)^4/(1-t)^8*exp(x*t/(1-t)) = (x*t)^4*(1 + (8+x)*t +(72+18*x+x^2)*t^2/2! + ...).",
				"Generalized Lah identity: (x+7)*(x+8)*...*(x+n+2) = Sum_{k = 4..n} T(n,k)*(x-1)*(x-2)*...*(x-k+4).",
				"The polynomials 1/n!*Sum_{k = 4..n+4} T(n+4,k)*(-x)^(k-4) for n \u003e= 0 are the generalized Laguerre polynomials Laguerre(n,7,x).",
				"Array = A132493* A143496 = abs(A008275) * ( A007318 )^6 * A008277 (apply Theorem 10 of [Neuwirth]). Array equals exp(D), where D is the array with the quadratic sequence (8,18,30,44, ... ) on the main subdiagonal and zeros everywhere else."
			],
			"example": [
				"Triangle begins",
				"n\\k|......4......5......6......7......8......9",
				"==============================================",
				"4..|......1",
				"5..|......8......1",
				"6..|.....72.....18......1",
				"7..|....720....270.....30......1",
				"8..|...7920...3960....660.....44......1",
				"9..|..95040..59400..13200...1320.....60......1",
				"...",
				"T(5,4) = 8. The partitions of {1,2,3,4,5} into 4 ordered lists, such that the elements 1, 2, 3 and 4 lie in different lists, are: {1}{2}{3}{4,5} and {1}{2}{3}{5,4}, {1}{2}{4}{3,5} and {1}{2}{4}{5,3}, {1}{3}{4}{2,5} and {1}{3}{4}{5,2}, {2}{3}{4}{1,5} and {2}{3}{4}{5,1}."
			],
			"maple": [
				"with combinat: T := (n, k) -\u003e (n-4)!/(k-4)!*binomial(n+3,k+3): for n from 4 to 13 do seq(T(n, k), k = 4..n) end do;"
			],
			"mathematica": [
				"T[n_, k_] := (n-4)!/(k-4)!*Binomial[n+3, k+3]; Table[T[n, k], {n, 4, 10}, {k, 4, n}] // Flatten (* _Amiram Eldar_, Nov 26 2018 *)"
			],
			"xref": [
				"Cf. A007318, A008275, A008277, A049388 (column 4), A105278 (unsigned Lah numbers), A143493, A143496, A143497, A143498."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "4,2",
			"author": "_Peter Bala_, Aug 25 2008",
			"references": 5,
			"revision": 23,
			"time": "2018-11-26T17:12:43-05:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}