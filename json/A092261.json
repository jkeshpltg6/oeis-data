{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A092261",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 92261,
			"data": "1,3,4,1,6,12,8,1,1,18,12,4,14,24,24,1,18,3,20,6,32,36,24,4,1,42,1,8,30,72,32,1,48,54,48,1,38,60,56,6,42,96,44,12,6,72,48,4,1,3,72,14,54,3,72,8,80,90,60,24,62,96,8,1,84,144,68,18,96,144,72,1,74,114,4,20,96,168,80",
			"name": "Sum of unitary, squarefree divisors of n, including 1.",
			"comment": [
				"Unitary convolution of the sequence of n*mu^2(n) (absolute values of A055615) and A000012. - _R. J. Mathar_, May 30 2011"
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A092261/b092261.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Eckford Cohen, \u003ca href=\"http://dx.doi.org/10.1007/BF01180473\"\u003eArithmetical functions associated with the unitary divisors of an integer\u003c/a\u003e, Math. Zeitschr. 74 (1960) 66-80, sequence sigma'(n).",
				"Steven R. Finch, \u003ca href=\"/A007947/a007947.pdf\"\u003eUnitarism and Infinitarism\u003c/a\u003e, February 25, 2004. [Cached copy, with permission of the author]"
			],
			"formula": [
				"Multiplicative with a(p) = p+1 and a(p^e) = 1 for e \u003e 1. - _Vladeta Jovovic_, Feb 22 2004",
				"From _Álvar Ibeas_, Mar 06 2015: (Start)",
				"a(n) = a(A055231(n)) = A000203(A055231(n)).",
				"Dirichlet g.f.: zeta(s) * Product_{p prime} (1 + p^(1-s) - p^(1-2s)).",
				"(End)",
				"From _Antti Karttunen_, Nov 25 2017: (Start)",
				"a(n) = A048250(A055231(n)).",
				"a(n) = A000203(n) / A295294(n).",
				"a(n) = A048250(n) / A295295(n) = A048250(n) / A048250(A057521(n)), where A057521(n) = A064549(A003557(n)).",
				"(End)",
				"Lim_{n-\u003eoo} (1/n) * Sum_{k=1..n} a(k)/k = Product_{p prime}(1 - 1/(p^2*(p+1))) = 0.881513... (A065465). - _Amiram Eldar_, Jun 10 2020",
				"Dirichlet g.f.: zeta(s) * zeta(s-1) * Product_{p prime} (1 + p^(2-3*s) - p^(1-2*s) - p^(2-2*s)). - _Vaclav Kotesovec_, Aug 20 2021"
			],
			"mathematica": [
				"Table[Plus @@ Select[Divisors@ n, Max @@ Last /@ FactorInteger@ # == 1 \u0026\u0026 GCD[#, n/#] == 1 \u0026], {n, 1, 79}] (* _Michael De Vlieger_, Mar 08 2015 *)",
				"f[p_, e_] := If[e==1, p+1, 1]; a[1]=1; a[n_] := Times @@ f @@@ FactorInteger[n]; Array[a, 79] (* _Amiram Eldar_, Mar 01 2019 *)"
			],
			"program": [
				"(PARI) a(n) = sumdiv(n, d, d*issquarefree(d)*(gcd(d, n/d) == 1)); \\\\ _Michel Marcus_, Mar 06 2015",
				"(Scheme)",
				";; This implementation utilizes the memoization-macro definec for which an implementation is available at http://oeis.org/wiki/Memoization#Scheme",
				";; The other functions, A020639, A067029 and A028234 can be found under the respective entries, and should likewise defined with definec:",
				"(definec (A092261 n) (if (= 1 n) 1 (* (+ 1 (if (\u003e (A067029 n) 1) 0 (A020639 n))) (A092261 (A028234 n))))) ;; _Antti Karttunen_, Nov 25 2017",
				"(PARI) for(n=1, 100, print1(direuler(p=2, n, (1 + p^2*X^3 - p*X^2 - p^2*X^2)/(1-X)/(1-p*X))[n], \", \")) \\\\ _Vaclav Kotesovec_, Aug 20 2021"
			],
			"xref": [
				"Cf. A000203, A003557, A007947, A048250, A055231, A056671, A057521, A065465, A295294, A295295, A329728."
			],
			"keyword": "nonn,mult",
			"offset": "1,2",
			"author": "_Steven Finch_, Feb 20 2004",
			"references": 11,
			"revision": 47,
			"time": "2021-11-20T07:31:10-05:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}