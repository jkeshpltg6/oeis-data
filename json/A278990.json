{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A278990",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 278990,
			"data": "1,0,1,5,36,329,3655,47844,721315,12310199,234615096,4939227215,113836841041,2850860253240,77087063678521,2238375706930349,69466733978519340,2294640596998068569,80381887628910919255,2976424482866702081004,116160936719430292078411",
			"name": "Number of loopless linear chord diagrams with n chords.",
			"comment": [
				"See the signed version of these numbers, A000806, for much more information about these numbers.",
				"From _Gus Wiseman_, Feb 27 2019: (Start)",
				"Also the number of 2-uniform set partitions of {1..2n} containing no two successive vertices in the same block. For example, the a(3) = 5 set partitions are:",
				"  {{1,3},{2,5},{4,6}}",
				"  {{1,4},{2,5},{3,6}}",
				"  {{1,4},{2,6},{3,5}}",
				"  {{1,5},{2,4},{3,6}}",
				"  {{1,6},{2,4},{3,5}}",
				"(End)",
				"From _Gus Wiseman_, Jul 05 2020: (Start)",
				"Also the number of permutations of the multiset {1,1,2,2,...,n,n} with no two consecutive terms equal and where the first i appears before the first j for i \u003c j. For example, the a(3) = 5 permutations are the following.",
				"  (1,2,3,1,2,3)",
				"  (1,2,3,1,3,2)",
				"  (1,2,3,2,1,3)",
				"  (1,2,3,2,3,1)",
				"  (1,2,1,3,2,3)",
				"(End)"
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A278990/b278990.txt\"\u003eTable of n, a(n) for n = 0..404\u003c/a\u003e (terms 0..200 from Gheorghe Coserea)",
				"Dmitry Efimov, \u003ca href=\"https://arxiv.org/abs/1904.08651\"\u003eThe hafnian of Toeplitz matrices of a special type, perfect matchings and Bessel polynomials\u003c/a\u003e, arXiv:1904.08651 [math.CO], 2019.",
				"H. Eriksson, A. Martin, \u003ca href=\"https://arxiv.org/abs/1702.04177\"\u003eEnumeration of Carlitz multipermutations\u003c/a\u003e, arXiv:1702.04177 [math.CO], 2017.",
				"E. Krasko, I. Labutin, A. Omelchenko, \u003ca href=\"https://arxiv.org/abs/1709.03218\"\u003eEnumeration of labelled and unlabelled Hamiltonian Cycles in complete k-partite graphs\u003c/a\u003e, arXiv:1709.03218 [math.CO], 2017, Table 1.",
				"E. Krasko, A. Omelchenko, \u003ca href=\"https://arxiv.org/abs/1601.05073\"\u003eEnumeration of Chord Diagrams without Loops and Parallel Chords\u003c/a\u003e, arXiv:1601.05073 [math.CO], 2016.",
				"E. Krasko, A. Omelchenko, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v24i3p43\"\u003eEnumeration of Chord Diagrams without Loops and Parallel Chords\u003c/a\u003e, The Electronic Journal of Combinatorics, 24(3) (2017), #P3.43.",
				"Gus Wiseman, \u003ca href=\"/A278990/a278990.png\"\u003eThe a(4) = 36 loopless linear chord diagrams\u003c/a\u003e."
			],
			"formula": [
				"From _Gheorghe Coserea_, Dec 09 2016: (Start)",
				"D-finite with recurrence a(n) = (2*n-1)*a(n-1) + a(n-2), with a(0) = 1, a(1) = 0.",
				"E.g.f. y satisfies: 0 = (1-2*x)*y'' - 3*y' - y.",
				"a(n) - a(n-1) = A003436(n) for all n \u003e= 2.",
				"(End)",
				"From _Vaclav Kotesovec_, Sep 15 2017: (Start)",
				"a(n) = sqrt(2)*exp(-1)*(BesselK(1/2 + n, 1)/sqrt(Pi) - i*sqrt(Pi)*BesselI(1/2 + n, -1)), where i is the imaginary unit.",
				"a(n) ~ 2^(n+1/2) * n^n  / exp(n+1).",
				"(End)",
				"a(n) = A114938(n)/n! - _Gus Wiseman_, Jul 05 2020 (from _Alexander Burstein_'s formula at A114938)."
			],
			"mathematica": [
				"RecurrenceTable[{a[n] == (2n-1)a[n-1] +a[n-2], a[0] == 1, a[1] == 0}, a, {n, 0, 20}] (* _Vaclav Kotesovec_, Sep 15 2017 *)",
				"FullSimplify[Table[-I*(BesselI[1/2 + n, -1] BesselK[3/2, 1] - BesselI[3/2, -1] BesselK[1/2 + n, 1]), {n, 0, 18}]] (* _Vaclav Kotesovec_, Sep 15 2017 *)",
				"Table[(2 n - 1)!! Hypergeometric1F1[-n, -2 n, -2], {n, 0, 20}] (* _Eric W. Weisstein_, Nov 14 2018 *)",
				"Table[Sqrt[2/Pi]/E ((-1)^n Pi BesselI[1/2 + n, 1] + BesselK[1/2 + n, 1]), {n, 0, 20}] // FunctionExpand // FullSimplify (* _Eric W. Weisstein_, Nov 14 2018 *)",
				"twouniflin[{}]:={{}};twouniflin[set:{i_,___}]:=Join@@Function[s,Prepend[#,s]\u0026/@twouniflin[Complement[set,s]]]/@Table[{i,j},{j,Select[set,#\u003ei+1\u0026]}];",
				"Table[Length[twouniflin[Range[n]]],{n,0,14,2}] (* _Gus Wiseman_, Feb 27 2019 *)"
			],
			"program": [
				"(PARI) seq(N) = {",
				"  my(a = vector(N)); a[1] = 0; a[2] = 1;",
				"  for (n = 3, N, a[n] = (2*n-1)*a[n-1] + a[n-2]);",
				"  concat(1, a);",
				"};",
				"seq(20) \\\\ _Gheorghe Coserea_, Dec 09 2016"
			],
			"xref": [
				"Column k=2 of A293157.",
				"Row n=2 of A322013.",
				"Cf. A000110, A000699 (topologically connected 2-uniform), A000806, A001147 (2-uniform), A003436 (cyclical version), A005493, A170941, A190823 (distance 3+ version), A322402, A324011, A324172.",
				"Anti-run compositions are A003242.",
				"Separable partitions are A325534.",
				"Cf. A007716, A292884, A333489.",
				"Other sequences involving the multiset {1,1,2,2,...,n,n}: A001147, A007717, A020555, A094574, A316972."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_, Dec 07 2016",
			"ext": [
				"a(0)=1 prepended by _Gheorghe Coserea_, Dec 09 2016"
			],
			"references": 23,
			"revision": 84,
			"time": "2020-07-07T06:34:50-04:00",
			"created": "2016-12-07T03:19:22-05:00"
		}
	]
}