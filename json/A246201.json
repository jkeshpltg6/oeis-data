{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A246201",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 246201,
			"data": "1,3,7,2,6,14,15,4,12,28,5,30,13,8,24,56,10,60,29,26,16,48,112,20,31,120,58,52,32,96,9,224,40,62,240,116,25,104,64,192,57,18,448,80,124,480,11,232,50,208,128,384,114,36,61,896,160,248,27,960,17,22,464,100,416,256,49,768,228,72,122,1792,113,320,496,54,1920,34,44",
			"name": "Permutation of natural numbers: a(1) = 1, a(A014580(n)) = (2*a(n))+1, a(A091242(n)) = 2*a(n), where A014580(n) = binary code for n-th irreducible polynomial over GF(2), A091242(n) = binary code for n-th reducible polynomial over GF(2).",
			"comment": [
				"Because 2 is the only even term in A014580, it implies that, apart from a(2)=3, odd numbers occur in odd positions only (along with many even numbers that also occur in odd positions).",
				"Note that for any value k in A246156, \"Odd reducible polynomials over GF(2)\": 5, 9, 15, 17, 21, 23, ..., a(k) will be even, and apart from 2, all other even numbers are mapped to some even number, so all those terms reside in infinite cycles. Furthermore, apart from 5 and 15, all of them reside in separate cycles. The infinite cycle containing 5 and 15 goes as: ..., 47, 11, 5, 6, 14, 8, 4, 2, 3, 7, 15, 24, 20, 26, 120, 7680, ... and it is only because a(2) = 3, that it can temporarily switch back from even terms to odd terms, until after a(15) = 24 it is finally doomed to the eternal evenness.",
				"(Compare also to the comments given at A246161)."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A246201/b246201.txt\"\u003eTable of n, a(n) for n = 1..10001\u003c/a\u003e",
				"\u003ca href=\"/index/Ge#GF2X\"\u003eIndex entries for sequences operating on GF(2)[X]-polynomials\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"a(1) = 1, and for n \u003e 1, if A091225(n) = 1 [i.e. when n is in A014580], a(n) = 1 + (2*a(A091226(n))), otherwise a(n) = 2*a(A091245(n)).",
				"As a composition of related permutations:",
				"a(n) = A054429(A245701(n)).",
				"a(n) = A006068(A246161(n)).",
				"a(n) = A193231(A246163(n)).",
				"a(n) = A246203(A193231(n)).",
				"Other identities:",
				"For all n \u003e 1, A000035(a(n)) = A091225(n). [After 1 maps binary representations of reducible GF(2) polynomials to even numbers and the corresponding representations of irreducible polynomials to odd numbers, in some order. A246203 has the same property]."
			],
			"program": [
				"(Scheme, with memoization-macro definec)",
				"(definec (A246201 n) (cond ((= 1 n) n) ((= 1 (A091225 n)) (+ 1 (* 2 (A246201 (A091226 n))))) (else (* 2 (A246201 (A091245 n))))))"
			],
			"xref": [
				"Inverse: A246202.",
				"Similar or related permutations: A245701, A246161, A006068, A054429, A193231, A246163, A246203, A237427.",
				"Cf. A091225, A091226, A091245."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Antti Karttunen_, Aug 19 2014",
			"references": 9,
			"revision": 16,
			"time": "2014-08-20T23:59:48-04:00",
			"created": "2014-08-20T23:59:48-04:00"
		}
	]
}