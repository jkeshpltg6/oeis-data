{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A118376",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 118376,
			"data": "1,2,6,24,112,568,3032,16768,95200,551616,3248704,19389824,117021824,712934784,4378663296,27081760768,168530142720,1054464293888,6629484729344,41860283723776,265346078982144,1687918305128448,10771600724946944,68941213290561536",
			"name": "Number of all trees of weight n, where nodes have positive integer weights and the sum of the weights of the children of a node is equal to the weight of the node.",
			"comment": [
				"The number of trees with leaf nodes equal to 1 is counted by the sequence A001003 of super-Catalan numbers. The number of binary trees is counted by the sequence A007317 and the number of binary trees with leaf nodes equal to 1 is counted by the sequence A000108 of Catalan numbers.",
				"Also the number of series-reduced enriched plane trees of weight n. A series-reduced enriched plane tree of weight n is either the number n itself or a finite sequence of at least two series-reduced enriched plane trees, one of each part of an integer composition of n. For example, the a(3) = 6 trees are: 3, (21), (12), (111), ((11)1), (1(11)). - _Gus Wiseman_, Sep 11 2018",
				"Conjectured to be the number of permutations of length n avoiding the partially ordered pattern (POP) {1\u003e2, 1\u003e3, 3\u003e4, 3\u003e5} of length 5. That is, conjectured to be the number of length n permutations having no subsequences of length 5 in which the first element is the largest, and the third element is larger than the fourth and fifth elements. - _Sergey Kitaev_, Dec 13 2020"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A118376/b118376.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Paul Barry, \u003ca href=\"https://arxiv.org/abs/1803.10297\"\u003eGeneralized Eulerian Triangles and Some Special Production Matrices\u003c/a\u003e, arXiv:1803.10297 [math.CO], 2018.",
				"H. Cambazard, N. Catusse, \u003ca href=\"http://arxiv.org/abs/1512.06649\"\u003eFixed-Parameter Algorithms for Rectilinear Steiner tree and Rectilinear Traveling Salesman Problem in the Plane\u003c/a\u003e, arXiv preprint arXiv:1512.06649 [cs.DS], 2015.",
				"S. B. Ekhad, M. Yang, \u003ca href=\"http://sites.math.rutgers.edu/~zeilberg/tokhniot/oMathar1maple12.txt\"\u003e Proofs of Linear Recurrences of Coefficients of Certain Algebraic Formal Power Series Conjectured in the On-Line Encyclopedia Of Integer Sequences\u003c/a\u003e, (2017)",
				"Alice L. L. Gao, Sergey Kitaev, \u003ca href=\"https://arxiv.org/abs/1903.08946\"\u003eOn partially ordered patterns of length 4 and 5 in permutations\u003c/a\u003e, arXiv:1903.08946 [math.CO], 2019.",
				"Alice L. L. Gao, Sergey Kitaev, \u003ca href=\"https://doi.org/10.37236/8605\"\u003eOn partially ordered patterns of length 4 and 5 in permutations\u003c/a\u003e, The Electronic Journal of Combinatorics 26(3) (2019), P3.26.",
				"Pawel Hitczenko, Jeremy R. Johnson, Hung-Jen Huang, \u003ca href=\"http://dx.doi.org/10.1016/j.tcs.2005.09.074\"\u003eDistribution of a class of divide and conquer recurrences arising from the computation of the Walsh-Hadamard transform\u003c/a\u003e, Theoretical Computer Science, Vol. 352, 2006, pp. 8-30.",
				"J. R. Johnson, M. Püschel, \u003ca href=\"http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.117.1233\"\u003eIn search for the optimal Walsh-Hadamard transform\u003c/a\u003e, Proc. ICASSP, Vol. 4, 2000, pp. 3347-3350.",
				"Vladimir Kruchinin, D. V. Kruchinin, \u003ca href=\"http://arxiv.org/abs/1103.2582\"\u003eComposita and their properties \u003c/a\u003e, arXiv:1103.2582 [math.CO], 2011-2013."
			],
			"formula": [
				"Recurrence: T(1) = 1; For n \u003e 1, T(n) = 1 + Sum_{n=n1+...+nt} T(n1)*...*T(nt).",
				"G.f.: (-1+(1-8*z+8*z^2)^(1/2))/(-4+4*z).",
				"From _Vladimir Kruchinin_, Sep 03 2010: (Start)",
				"O.g.f.: A(x) = A001003(x/(1-x)).",
				"a(n) = Sum_{k=1..n} binomial(n-1,k-1)*A001003(k), n\u003e0. (End)",
				"D-finite with recurrence: n*a(n) + 3*(-3*n+4)*a(n-1) + 4*(4*n-9)*a(n-2) + 8*(-n+3)*a(n-3) = 0. - _R. J. Mathar_, Sep 27 2013",
				"a(n) ~ sqrt(sqrt(2)-1) * 2^(n-1/2) * (2+sqrt(2))^(n-1) / (sqrt(Pi) * n^(3/2)). - _Vaclav Kotesovec_, Feb 03 2014",
				"From _Peter Bala_, Jun 17 2015: (Start)",
				"With offset 0, binomial transform of A001003.",
				"O.g.f. A(x) = series reversion of x*(2*x - 1)/(2*x^2 - 1); 2*(1 - x)*A^2(x) - A(x) + x = 0.",
				"A(x) satisfies the differential equation (x - 9*x^2 + 16*x^3 - 8*x^4)*A'(x) + x*(3 - 4*x)*A(x) + x*(2*x - 1) = 0. Extracting coefficients gives Mathar's recurrence above. (End)",
				"a(n) = Sum_{j=0..(n-1)/2} (-1)^j*2^(n-j-1)*C(n,j)*C(2*n-2*j-2,n-2*j-1)/n. - _Vladimir Kruchinin_, Sep 29 2020"
			],
			"example": [
				"T(3) = 6 because there are six trees",
				"  3    3      3     3     3       3",
				"      2 1    2 1   1 2   1 2    1 1 1",
				"            1 1           1 1",
				"From _Gus Wiseman_, Sep 11 2018: (Start)",
				"The a(4) = 24 series-reduced enriched plane trees:",
				"  4,",
				"  (31), (13), (22), (211), (121), (112), (1111),",
				"  ((21)1), ((12)1), (1(21)), (1(12)), (2(11)), ((11)2),",
				"  ((111)1), (1(111)), ((11)(11)), ((11)11), (1(11)1), (11(11)),",
				"  (((11)1)1), ((1(11))1), (1((11)1)), (1(1(11))).",
				"(End)"
			],
			"maple": [
				"T := proc(n) option remember; local C, s, p, tp, k, i; if n = 1 then return 1; else s := 1; for k from 2 to n do C := combinat[composition](n,k); for p in C do tp := map(T,p); s := s + mul(tp[i],i=1..nops(tp)); end do; end do; end if; return s; end;"
			],
			"mathematica": [
				"Rest[CoefficientList[Series[(Sqrt[1-8*x+8*x^2]-1)/(4*x-4), {x, 0, 20}], x]] (* _Vaclav Kotesovec_, Feb 03 2014 *)",
				"a[n_] := 1+Sum[Binomial[n-1, k-1]*Hypergeometric2F1[2-k, k+1, 2, -1], {k, 2, n}]; Table[a[n], {n, 1, 20}] (* _Jean-François Alcover_, Apr 03 2015, after _Vladimir Kruchinin_ *)",
				"urp[n_]:=Prepend[Join@@Table[Tuples[urp/@ptn],{ptn,Join@@Permutations/@Select[IntegerPartitions[n],Length[#]\u003e1\u0026]}],n];",
				"Table[Length[urp[n]],{n,7}] (* _Gus Wiseman_, Sep 11 2018 *)"
			],
			"program": [
				"(PARI) x='x+O('x^25); Vec((sqrt(1-8*x+8*x^2) - 1)/(4*x-4)) \\\\ _G. C. Greubel_, Feb 08 2017",
				"(Maxima) a(n):=sum((-1)^j*2^(n-j-1)*binomial(n,j)*binomial(2*n-2*j-2,n-2*j-1),j,0,(n-1)/2)/n /* _Vladimir Kruchinin_, Sep 29 2020 */"
			],
			"xref": [
				"Cf. A000108, A000311, A001003, A005804, A007317, A007853, A032171, A032200, A143363, A289501, A317852."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "Jeremy Johnson (jjohnson(AT)cs.drexel.edu), May 15 2006",
			"references": 17,
			"revision": 62,
			"time": "2020-12-14T05:21:25-05:00",
			"created": "2006-05-19T03:00:00-04:00"
		}
	]
}