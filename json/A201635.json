{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A201635",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 201635,
			"data": "1,1,0,1,1,2,1,2,4,6,1,3,7,13,22,1,4,11,24,46,80,1,5,16,40,86,166,296,1,6,22,62,148,314,610,1106,1,7,29,91,239,553,1163,2269,4166,1,8,37,128,367,920,2083,4352,8518,15792,1,9,46,174,541,1461,3544,7896",
			"name": "Triangle formed by T(n,n) = (-1)^n*Sum_{j=0..n} C(-n,j), T(n,k) = Sum_{j=0..k} T(n-1,j) for k=0..n-1, and n\u003e=0, read by rows.",
			"comment": [
				"Notation: If a sequence id is starred then the offset and/or some terms are different. Starred terms indicate the variance.",
				"Row sums: [A026641 ] [1,  1,  4,  13,  46,  166,   610]",
				"--",
				"T(j+2, 2) [A000124*] [1*, 2 , 4,   7,  11,  16,     22]",
				"T(j+3, 3) [A003600*] [1*, 2*, 6,  13,  24,  40,     62]",
				"--",
				"T(j  , j) [A072547 ] [1,  0,  2,   6,  22,  80,    296]",
				"T(j+1, j) [A026641 ] [1,  1,  4,  13,  46,  166,   610]",
				"T(j+2, j) [A014300 ] [1,  2,  7,  24,  86,  314,  1163]",
				"T(j+3, j) [A014301*] [1,  3, 11,  40, 148,  553,  2083]",
				"T(j+4, j) [A172025 ] [1,  4, 16,  62, 239,  920,  3544]",
				"T(j+5, j) [A172061 ] [1,  5, 22,  91, 367, 1461,  5776]",
				"T(j+6, j) [A172062 ] [1,  6, 29, 128, 541, 2232,  9076]",
				"T(j+7, j) [A172063 ] [1,  7, 37, 174, 771, 3300, 13820]",
				"--",
				"T(2j  ,j) [Central ] [1,  1,  7,  40, 239, 1461,  9076]",
				"T(2j+1,j) [A183160 ] [1,  2, 11,  62, 367, 2232, 13820]",
				"T(2j+2,j) [        ] [1,  3, 16,  91, 541, 3300, 20476]",
				"T(2j+3,j) [A199033*] [1,  4, 22, 128, 771, 4744, 29618]"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A201635/b201635.txt\"\u003eRows n=0..100 of triangle, flattened\u003c/a\u003e"
			],
			"example": [
				"Triangle begins as:",
				"[n]|k-\u003e",
				"[0] 1",
				"[1] 1, 0",
				"[2] 1, 1,  2",
				"[3] 1, 2,  4,  6",
				"[4] 1, 3,  7, 13,  22",
				"[5] 1, 4, 11, 24,  46,  80",
				"[6] 1, 5, 16, 40,  86, 166, 296",
				"[7] 1, 6, 22, 62, 148, 314, 610, 1106."
			],
			"maple": [
				"A201635 := proc(n,k) option remember; local j;",
				"if n=k then (-1)^n*add(binomial(-n,j), j=0..n)",
				"else add(A201635(n-1,j), j=0..k) fi end:",
				"for n from 0 to 7 do seq(A(n,k), k=0..n) od;"
			],
			"mathematica": [
				"T[n_, k_]:= T[n, k]= If[k==n, (-1)^n*Sum[Binomial[-n, j], {j, 0, n}], Sum[T[n-1, j], {j, 0, k}]]; Table[T[n, k], {n,0,10}, {k,0,n}]//Flatten (* _G. C. Greubel_, Feb 27 2019 *)"
			],
			"program": [
				"(Sage)",
				"@CachedFunction",
				"def A201635(n, k):",
				"    if n==k: return (-1)^n*add(binomial(-n, j) for j in (0..n))",
				"    return add(A201635(n-1, j) for j in (0..k))",
				"for n in (0..7) : [A201635(n, k) for k in (0..n)]",
				"(PARI)",
				"{T(n,k) = if(k==n, (-1)^n*sum(j=0,n, binomial(-n,j)), sum(j=0,k, T(n-1,j)))};",
				"for(n=0,10, for(k=0,n, print1(T(n,k), \", \"))) \\\\ _G. C. Greubel_, Feb 27 2019"
			],
			"keyword": "nonn,tabl",
			"offset": "0,6",
			"author": "_Peter Luschny_, Nov 14 2012",
			"references": 1,
			"revision": 10,
			"time": "2019-03-01T08:04:19-05:00",
			"created": "2012-11-15T11:29:11-05:00"
		}
	]
}