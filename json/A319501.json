{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A319501",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 319501,
			"data": "1,0,1,0,1,3,0,2,12,13,0,2,38,105,73,0,3,110,588,976,501,0,4,302,2811,8416,9945,4051,0,5,806,12354,59488,121710,111396,37633,0,6,2109,51543,375698,1185360,1830822,1366057,394353,0,8,5450,207846,2209276,10096795,23420022,28969248,18235680,4596553",
			"name": "Number T(n,k) of sets of nonempty words with a total of n letters over k-ary alphabet such that all k letters occur at least once in the set; triangle T(n,k), n\u003e=0, 0\u003c=k\u003c=n, read by rows.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A319501/b319501.txt\"\u003eRows n = 0..140, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = Sum_{i=0..k} (-1)^i * C(k,i) * A292804(n,k-i)."
			],
			"example": [
				"T(2,2) = 3: {ab}, {ba}, {a,b}.",
				"T(3,2) = 12: {aab}, {aba}, {abb}, {baa}, {bab}, {bba}, {a,ab}, {a,ba}, {a,bb}, {aa,b}, {ab,b}, {b,ba}.",
				"T(4,2) = 38: {aaab}, {aaba}, {aabb}, {abaa}, {abab}, {abba}, {abbb}, {baaa}, {baab}, {baba}, {babb}, {bbaa}, {bbab}, {bbba}, {a,aab}, {a,aba}, {a,abb}, {a,baa}, {a,bab}, {a,bba}, {a,bbb}, {aa,ab}, {aa,ba}, {aa,bb}, {aaa,b}, {aab,b}, {ab,ba}, {ab,bb}, {aba,b}, {abb,b}, {b,baa}, {b,bab}, {b,bba}, {ba,bb}, {a,aa,b}, {a,ab,b}, {a,b,ba}, {a,b,bb}.",
				"Triangle T(n,k) begins:",
				"  1;",
				"  0, 1;",
				"  0, 1,    3;",
				"  0, 2,   12,    13;",
				"  0, 2,   38,   105,     73;",
				"  0, 3,  110,   588,    976,     501;",
				"  0, 4,  302,  2811,   8416,    9945,    4051;",
				"  0, 5,  806, 12354,  59488,  121710,  111396,   37633;",
				"  0, 6, 2109, 51543, 375698, 1185360, 1830822, 1366057, 394353;"
			],
			"maple": [
				"h:= proc(n, i, k) option remember; `if`(n=0, 1, `if`(i\u003c1, 0,",
				"      add(h(n-i*j, i-1, k)*binomial(k^i, j), j=0..n/i)))",
				"    end:",
				"T:= (n, k)-\u003e add((-1)^i*binomial(k, i)*h(n$2, k-i), i=0..k):",
				"seq(seq(T(n, k), k=0..n), n=0..12);"
			],
			"mathematica": [
				"h[n_, i_, k_] := h[n, i, k] = If[n==0, 1, If[i\u003c1, 0, Sum[h[n-i*j, i-1, k]* Binomial[k^i, j], {j, 0, n/i}]]];",
				"T[n_, k_] := Sum[(-1)^i Binomial[k, i] h[n, n, k-i], {i, 0, k}];",
				"Table[T[n, k], {n, 0, 12}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Jan 05 2020, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Columns k=0-10 give: A000007, A000009 (for n\u003e0), A320203, A320204, A320205, A320206, A320207, A320208, A320209, A320210, A320211.",
				"Main diagonal gives A000262.",
				"Row sums give A319518.",
				"T(2n,n) gives A319519.",
				"Cf. A257740, A292804."
			],
			"keyword": "nonn,tabl",
			"offset": "0,6",
			"author": "_Alois P. Heinz_, Sep 20 2018",
			"references": 16,
			"revision": 23,
			"time": "2020-01-05T05:36:05-05:00",
			"created": "2018-09-21T17:23:32-04:00"
		}
	]
}