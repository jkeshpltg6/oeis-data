{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A098256",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 98256,
			"data": "1,442,195805,86741173,38426143834,17022694977289,7541015448795193,3340652821121293210,1479901658741284096837,655593094169567733605581,290426260815459764703175546,128658177948154506195773161297,56995282404771630784962807279025,25248781447135884283232327851446778",
			"name": "First differences of Chebyshev polynomials S(n,443)=A098254(n) with Diophantine property.",
			"comment": [
				"(21*b(n))^2 - 445*a(n)^2 = -4 with b(n)=A098255(n) give all positive solutions of this Pell equation."
			],
			"link": [
				"Indranil Ghosh, \u003ca href=\"/A098256/b098256.txt\"\u003eTable of n, a(n) for n = 0..377\u003c/a\u003e",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"Giovanni Lucca, \u003ca href=\"http://forumgeom.fau.edu/FG2019volume19/FG201902index.html\"\u003eInteger Sequences and Circle Chains Inside a Hyperbola\u003c/a\u003e, Forum Geometricorum (2019) Vol. 19, 11-16.",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (443, -1)."
			],
			"formula": [
				"G.f.: (1 - x)/(1 - 443*x + x^2).",
				"a(n) = ((-1)^n)*S(2*n, 21*i) with the imaginary unit i and the S(n, x)=U(n, x/2) Chebyshev polynomials.",
				"a(n) = S(n, 443) - S(n-1, 443) = T(2*n+1, sqrt(445)/2)/(sqrt(445)/2), with S(n, x)=U(n, x/2) Chebyshev's polynomials of the second kind, A049310. S(-1, x)= 0 = U(-1, x) and T(n, x) Chebyshev's polynomials of the first kind, A053120.",
				"a(n) = 443*a(n-1) - a(n-2) for n\u003e1, a(0)=1, a(1)=442. - _Philippe Deléham_, Nov 18 2008"
			],
			"example": [
				"All positive solutions of Pell equation x^2 - 445*y^2 = -4 are (21=21*1,1), (9324=21*444,442), (4130511=21*196691,195805), (1829807049=21*87133669,86741173), ..."
			],
			"mathematica": [
				"LinearRecurrence[{443,-1}, {1,442}, 20] (* _G. C. Greubel_, Aug 01 2019 *)"
			],
			"program": [
				"(PARI) my(x='x+O('x^20)); Vec((1-x)/(1-443*x+x^2)) \\\\ _G. C. Greubel_, Aug 01 2019",
				"(MAGMA) I:=[1,442]; [n le 2 select I[n] else 443*Self(n-1) - Self(n-2): n in [1..20]]; // _G. C. Greubel_, Aug 01 2019",
				"(Sage) ((1-x)/(1-443*x+x^2)).series(x, 20).coefficients(x, sparse=False) # _G. C. Greubel_, Aug 01 2019",
				"(GAP) a:=[1,442];; for n in [3..20] do a[n]:=443*a[n-1]-a[n-2]; od; a; # _G. C. Greubel_, Aug 01 2019"
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Sep 10 2004",
			"references": 4,
			"revision": 26,
			"time": "2019-08-01T20:24:26-04:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}