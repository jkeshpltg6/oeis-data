{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A193010",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 193010,
			"data": "1,7,8,3,9,2,2,9,9,6,3,1,2,8,7,8,7,6,7,8,4,6,2,3,6,9,1,6,0,9,0,1,7,0,9,7,2,5,1,0,2,9,8,6,0,6,3,3,8,4,1,2,1,7,8,7,0,7,0,0,0,7,3,6,6,8,9,5,2,5,9,7,4,0,0,2,0,3,0,2,5,3,5,4,8,2,6,1,5,6,5,0,5,6,7,1,9,4,5,2",
			"name": "Decimal expansion of the constant term of the reduction of e^x by x^2-\u003ex+1.",
			"comment": [
				"Suppose that q and s are polynomials and degree(q)\u003edegree(s).  The reduction of a polynomial p by q-\u003es is introduced at A192232.  If p is replaced by a function f having power series",
				"...",
				"c(0) + c(1)*x + c(2)*x^2 + ... ,",
				"...",
				"then the reduction, R(f), of f by q-\u003es is here introduced as the limit, if it exists, of the reduction of p(n,x) by q-\u003es, where p(n,x) is the n-th partial sum of f(x):",
				"...",
				"R(f(x)) = c(0)*R(1) + c(1)*R(x) + c(2)*R(x^2) + ...  If q(x)=x^2 and s(x)=x+1, then",
				"...",
				"R(f(x)) = c(0) + c(1)*x + c(2)*(x+1) + c(3)*(2x+1) + c(4)(3x+2) +  ..., so that",
				"...",
				"R(f(x)) = sum{c(n)*(F(n)*x+F(n-1)}: n\u003e=0}, where F=A000045 (Fibonacci sequence), so that",
				"...",
				"R(f(x)) =  u0 + x*u1 where u0=sum{c(n)F(n-1): n\u003e=0}, u1=sum(c(n)F(n): n\u003e=0); the numbers u0 and u1 are given by A193010 and A098689.",
				"Following is a list of reductions by x^2-\u003ex+1 of selected functions.  Each sequence A-number refers to the constant represented by the sequence.  Adjustments for offsets are needed in some cases.",
				"e^x......... A193010 + x*A098689",
				"e^(-x)...... A193026 + x*A099935",
				"e^(2x)...... A193027 + x*A193028",
				"e^(x/2)..... A193029 + x*A193030",
				"sin x....... A193011 + x*A193012",
				"cos x....... A193013 + x*A193014",
				"sinh x...... A193015 + x*A193016",
				"cosh x...... A193017 + x*A193025",
				"2^x......... A193031 + x*A193032",
				"2^(-x)...... A193009 + x*A193035",
				"3^x......... A193083 + x*A193084",
				"t^x......... A193075 + x*A193076, t=(1+sqrt(5))/2",
				"t^(-x)...... A193077 + x*A193078, t=(1+sqrt(5))/2",
				"sinh(2x).... A193079 + x*A193080",
				"cosh(2x).... A193081 + x*A193082",
				"(e^x)cos x.. A193083 + x*A193084",
				"(e^x)sin x.. A193085 + x*A193086",
				"(cos x)^2... A193087 + x*A193088",
				"(sin x)^2... A193089 + x*A193088"
			],
			"example": [
				"Equals 1.783922996312878767846236916090170972510..."
			],
			"mathematica": [
				"f[x_] := Exp[x]; r[n_] := Fibonacci[n];",
				"c[n_] := SeriesCoefficient[Series[f[x], {x, 0, n}], n]",
				"u0 = N[Sum[c[n]*r[n - 1], {n, 0, 200}], 100]",
				"RealDigits[u0, 10]  (* A193010 *)",
				"u1 = N[Sum[c[n]*r[n], {n, 0, 200}], 100]",
				"RealDigits[u1, 10]  (* A098689 *)"
			],
			"xref": [
				"Cf. A192232."
			],
			"keyword": "nonn,cons",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Jul 14 2011",
			"references": 36,
			"revision": 11,
			"time": "2012-03-30T18:57:37-04:00",
			"created": "2011-07-14T19:47:52-04:00"
		}
	]
}