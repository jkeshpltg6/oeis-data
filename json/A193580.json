{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A193580",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 193580,
			"data": "1,1,1,1,4,1,9,16,8,1,1,16,78,140,79,1,25,228,964,1987,1974,978,242,27,1,1,36,520,3920,16834,42368,62266,51504,21792,3600,1,49,1020,11860,85275,397014,1220298,2484382,3324193,2882737,1601292,569818,129657,18389,1520,64,1",
			"name": "Triangle read by rows: T(n,k) = number of ways to place k nonattacking kings on an n X n board.",
			"comment": [
				"Rows 2n and 2n-1 both contain 1 + n^2 entries. Cf. A008794.",
				"Row n sums to A063443(n+1).",
				"Number of walks of length n-1 on a graph in which each node represents a 11-avoiding n-bit binary sequence B and adjacency of B and B' is determined by B'\u0026(B|(B\u003c\u003c1)|(B\u003e\u003e1))=0 and the total number of nonzero bits in the walk is k.",
				"Row n gives the coefficients of the independence polynomial of the n X n king graph. - _Eric W. Weisstein_, Jun 20 2017"
			],
			"reference": [
				"Norman Biggs, Algebraic Graph Theory, Cambridge University Press, New York, NY, second edition, 1993."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A193580/b193580.txt\"\u003eRows n = 0..21, flattened\u003c/a\u003e (Rows n = 0..20 from Andrew Woods)",
				"R. J. Mathar, \u003ca href=\"http://arxiv.org/abs/1609.03964\"\u003eTiling n x m rectangles with 1 x 1 and s x s squares\u003c/a\u003e arXiv:1609.03964 [math.CO], 2016, Section 4.1.",
				"J. Nilsson, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL20/Nilsson/nilsson15.html\"\u003eOn Counting the Number of Tilings of a Rectangle with Squares of Size 1 and 2\u003c/a\u003e, Journal of Integer Sequences, Vol. 20 (2017), Article 17.2.2.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/IndependencePolynomial.html\"\u003eIndependence Polynomial\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/KingGraph.html\"\u003eKing Graph\u003c/a\u003e"
			],
			"formula": [
				"T(n, 0) = 1;",
				"T(n, 1) = n^2;",
				"T(2n-1, n^2-1) = n^3;",
				"T(2n-1, n^2) = 1."
			],
			"example": [
				"The table begins with T(0,0):",
				"  1;",
				"  1,   1;",
				"  1,   4;",
				"  1,   9,  16,   8,   1;",
				"  1,  16,  78, 140,  79;",
				"  ...",
				"T(4,3) = 140 because there are 140 ways to place 3 kings on a 4 X 4 chessboard so that no king threatens any other."
			],
			"xref": [
				"Columns 2 to 10: A061995, A061996, A061997, A061998, A172158, A194788, A201369, A201771, A220467.",
				"Diagonal: A201513.",
				"Cf. A179403, etc., for extension to toroidal boards.",
				"Cf. A166540, etc., for extension into three dimensions.",
				"Cf. A098487 for a clipped version.",
				"Row n sums to A063443(n+1)."
			],
			"keyword": "nonn,tabf",
			"offset": "0,5",
			"author": "_Andrew Woods_, Aug 27 2011",
			"references": 13,
			"revision": 51,
			"time": "2017-12-03T12:07:00-05:00",
			"created": "2011-08-27T12:15:12-04:00"
		}
	]
}