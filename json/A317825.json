{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A317825",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 317825,
			"data": "1,3,0,9,-4,0,7,27,-18,-12,23,0,13,21,-6,81,-64,-54,73,-36,57,69,-46,0,25,39,-12,63,-34,-18,49,243,-210,-192,227,-162,199,219,-180,-108,149,171,-128,207,-162,-138,185,0,49,75,-24,117,-64,-36,91,189,-132,-102,161,-54,115,147,-84,729,-664,-630,697,-576",
			"name": "a(1) = 1, a(n) = 3*a(n/2) if n is even, a(n) = n - a(n-1) if n is odd.",
			"comment": [
				"Sequence has an elegant fractal-like scatter plot, situated (approximately) symmetrically over X-axis.",
				"This sequence can also be generalized with some modifications. Let f_k(1) = 1. f_k(n) = floor(k*a(n/2)) if n is even, f_k(n) = n - f_k(n-1) if n is odd. This sequence is a(n) = f_k(n) where k = 3. For example, if k is e (A001113), then recurrence also provides a curious fractal-like structure that has some similarities with a(n). See Links section for their plots.",
				"A scatterplot of (Sum_{i = 1..2*n} a(i)) - n^2 gives a similar plot as for a(n). - _A.H.M. Smeets_, Sep 01 2018"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A317825/b317825.txt\"\u003eTable of n, a(n) for n = 1..16383\u003c/a\u003e",
				"Altug Alkan, \u003ca href=\"/A317825/a317825.png\"\u003eA scatterplot of a(n) for n \u003c= 2^15-1\u003c/a\u003e",
				"Altug Alkan, \u003ca href=\"/A317825/a317825_1.png\"\u003eA scatterplot of f_e(n) for n \u003c= 2^15-1\u003c/a\u003e",
				"Altug Alkan, \u003ca href=\"/A317825/a317825_2.png\"\u003eA scatterplot of (A317825(n), abs(A318303(n)))\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A317825/a317825_3.png\"\u003eA colored scatterplot of (A317825(n), abs(A318303(n))) for n = 1..2^20-1 \u003c/a\u003e (where the color is function of n)"
			],
			"formula": [
				"From _A.H.M. Smeets_, Sep 01 2018: (Start)",
				"Sum_{i = 1..2*n-1} a(i) = n^2 for n \u003e= 0.",
				"Sum_{i = 1..2*n} a(i) = 3*a(n) + n^2 for n \u003e= 0, a(0) = 0.",
				"Sum_{i = 1..36*2^n} a(i) = 162*A085350(n) for n \u003e= 0.",
				"Lim_{n -\u003e infinity} a(n)/n^2 = 0.",
				"Lim_{n -\u003e infinity} (Sum_{i = 1..n} a(i))/n^2 = 1/4. (End)"
			],
			"mathematica": [
				"Nest[Append[#1, If[EvenQ[#2], 3 #1[[#2/2]], #2 - #1[[-1]] ]] \u0026 @@ {#, Length@ # + 1} \u0026, {1}, 67] (* _Michael De Vlieger_, Aug 22 2018 *)"
			],
			"program": [
				"(PARI) A317825(n) = if(1==n,n,if(!(n%2),3*A317825(n/2),n-A317825(n-1)));",
				"(Python)",
				"aa = [0]",
				"a,n = 0,0",
				"while n \u003c 16383:",
				"....n = n+1",
				"....if n%2 == 0:",
				"........a = 3*aa[n//2]",
				"....else:",
				"........a = n-a",
				"....aa = aa+[a]",
				"....print(n,a) # _A.H.M. Smeets_, Sep 01 2018",
				"(MAGMA) [n eq 1 select 1 else IsEven(n) select 3*Self(n div 2) else n- Self(n-1): n in [1..80]]; // _Vincenzo Librandi_, Sep 03 2018"
			],
			"xref": [
				"Cf. A318265, A318303."
			],
			"keyword": "sign,look",
			"offset": "1,2",
			"author": "_Altug Alkan_ and _Antti Karttunen_, Aug 22 2018",
			"references": 5,
			"revision": 48,
			"time": "2018-09-08T04:42:05-04:00",
			"created": "2018-08-25T02:22:06-04:00"
		}
	]
}