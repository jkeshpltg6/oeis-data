{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A309380",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 309380,
			"data": "180,240,1380,4200,15420,52080,177780,595320,1978860,6515520,21298980,69168840,223369500,717772560,2296480980,7319252760,23247851340,73615135200,232462779780,732245695080,2301319648380,7217727595440,22594530691380,70607719663800",
			"name": "Number of unordered pairs of 5-colorings of an n-wheel that differ in the coloring of exactly one vertex.",
			"comment": [
				"The n-wheel graph is defined for n \u003e= 4. The value of a(3) was computed using the complete graph on 3 vertices."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A309380/b309380.txt\"\u003eTable of n, a(n) for n = 3..200\u003c/a\u003e",
				"Prateek Bhakta, Benjamin Brett Buckner, Lauren Farquhar, Vikram Kamat, Sara Krehbiel, Heather M. Russell, \u003ca href=\"https://doi.org/10.1007/s00373-018-1985-6\"\u003eCut-Colorings in Coloring Graphs\u003c/a\u003e, Graphs and Combinatorics, (2019) 35(1), 239-248.",
				"Luis Cereceda, Janvan den Heuvel, Matthew Johnson, \u003ca href=\"https://doi.org/10.1016/j.disc.2007.07.028\"\u003eConnectedness of the graph of vertex-colourings\u003c/a\u003e, Discrete Mathematics, (2008) 308(5-6), 913-919.",
				"Aalok Sathe, \u003ca href=\"https://github.com/aalok-sathe/coloring-graphs\"\u003eColoring Graphs Library\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Wheel_graph\"\u003eWheel graph\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (6,-6,-16,15,18)."
			],
			"formula": [
				"From _Andrew Howroyd_, Sep 10 2019: (Start)",
				"a(n) = 10*(2^(n-1) - 2*(-1)^n + (n-1)*(3^(n-2) - 3*(-1)^n)).",
				"a(n) = 10*A092297(n-1) + 5*A326347(n-1).",
				"a(n) = binomial(k, 2)*A106512(n-1, k-2) + k*(n-1)*(binomial(k-2, 2)*A106512(n-3, k-1) + binomial(k-3, 2)*A106512(n-2, k-1)) where k = 5.",
				"a(n) = 6*a(n-1) - 6*a(n-2) - 16*a(n-3) + 15*a(n-4) + 18*a(n-5) for n \u003e 7.",
				"G.f.: 60*x^3*(3 - 14*x + 17*x^2 + 4*x^3 - 6*x^4)/((1 + x)^2*(1 - 2*x)*(1 - 3*x)^2).",
				"(End)"
			],
			"program": [
				"(PARI) a(n) = {10*(2^(n-1) - 2*(-1)^n + (n-1)*(3^(n-2) - 3*(-1)^n))} \\\\ _Andrew Howroyd_, Sep 10 2019",
				"(PARI) Vec(60*(3 - 14*x + 17*x^2 + 4*x^3 - 6*x^4)/((1 + x)^2*(1 - 2*x)*(1 - 3*x)^2) + O(x^30)) \\\\ _Andrew Howroyd_, Sep 10 2019"
			],
			"xref": [
				"Cf. A092297, A106512, A309379 (similar sequence with 4 colors), A090860 (4-colorings), A309315 (5-colorings), A326347 (on n-cycle)."
			],
			"keyword": "nonn",
			"offset": "3,1",
			"author": "_Aalok Sathe_, Jul 26 2019",
			"ext": [
				"Terms a(12) and beyond from _Andrew Howroyd_, Sep 10 2019"
			],
			"references": 3,
			"revision": 29,
			"time": "2019-09-11T19:42:27-04:00",
			"created": "2019-08-28T12:10:54-04:00"
		}
	]
}