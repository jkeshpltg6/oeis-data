{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A277710",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 277710,
			"data": "1,5,2,13,10,3,29,26,39,4,41,58,75,20,9,61,82,147,52,21,6,85,122,207,116,45,78,7,125,170,291,164,93,150,11,8,173,250,411,244,189,294,19,40,81,209,346,579,340,381,414,35,104,105,18,253,418,819,500,657,582,67,232,165,42,23,281,506,927,692,765,822,131,328,213,90,43,12",
			"name": "Square array A(r,c), where each row r lists all numbers k for which A264977(k) = r, read by downwards antidiagonals: A(1,1), A(1,2), A(2,1), A(1,3), A(2,2), A(3,1), etc.",
			"comment": [
				"Alternative description: Each row r lists the positions of A019565(r) in A277330.",
				"Odd terms occur only on rows with odd index, and even terms only on rows with even index. Specifically: all terms k on row r are equal to r modulo 4, thus the first differences of each row are all multiples of 4.",
				"All the terms on any particular row are either all multiples of two (or respectively: three, or six), or none of them are."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A277710/b277710.txt\"\u003eTable of n, a(n) for n = 1..1225; the first 49 antidiagonals of array\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"A(r,1) = A277711(r); for c \u003e 1, A(r,c) = A277816(A(r,c-1)).",
				"Other identities. For all r\u003e=1, c\u003e=1:",
				"A(2*r,c) = 2*A(r,c).",
				"A(r,c) modulo 4 = r modulo 4."
			],
			"example": [
				"The top left 12 x 12 corner of the array:",
				"   1,   5,  13,  29,  41,   61,   85,  125,  173,  209,  253,  281",
				"   2,  10,  26,  58,  82,  122,  170,  250,  346,  418,  506,  562",
				"   3,  39,  75, 147, 207,  291,  411,  579,  819,  927, 1155, 1635",
				"   4,  20,  52, 116, 164,  244,  340,  500,  692,  836, 1012, 1124",
				"   9,  21,  45,  93, 189,  381,  657,  765,  873, 1317, 1533, 1749",
				"   6,  78, 150, 294, 414,  582,  822, 1158, 1638, 1854, 2310, 3270",
				"   7,  11,  19,  35,  67,  131,  259,  311,  359,  515,  619,  655",
				"   8,  40, 104, 232, 328,  488,  680, 1000, 1384, 1672, 2024, 2248",
				"  81, 105, 165, 213, 333,  429,  669,  861, 1341, 1725, 2685, 2721",
				"  18,  42,  90, 186, 378,  762, 1314, 1530, 1746, 2634, 3066, 3498",
				"  23,  43,  79,  83, 103,  155,  163,  203,  307,  323,  403,  611",
				"  12, 156, 300, 588, 828, 1164, 1644, 2316, 3276, 3708, 4620, 6540"
			],
			"program": [
				"(Scheme, with Antti Karttunen's IntSeq-library)",
				"(define (A277710 n) (A277710bi (A002260 n) (A004736 n)))",
				"(define (A277710bi row col) (if (= 1 col) (A277711 row) (A277816 (A277710bi row (- col 1)))))",
				";; Alternatively:",
				"(define (A277710bi row col) ((rowfun-for-A277710 row) col))",
				"(definec (rowfun-for-A277710 n) (MATCHING-POS 1 0 (lambda (k) (= n (A264977 k)))))"
			],
			"xref": [
				"Transpose: A277709.",
				"Column 1: A277711, sorted into ascending order: A277817.",
				"Row 1: A277701, Row 2: A277712 (= 2*A277701), Row 3: A277713, Row 4: 4*A277701, Row 5: A277715, Row 6: 2*A277713. Row 8: 8*A277701, Row 10: 2*A277715.",
				"Cf. A277824 (the index of the column where n is located in this array).",
				"Cf. A019565, A264977, A277330, A277816 and permutation pair A277695 \u0026 A277696."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Antti Karttunen_, Oct 29 2016",
			"ext": [
				"The dispersion-style formula added by _Antti Karttunen_, Nov 06 2016"
			],
			"references": 13,
			"revision": 27,
			"time": "2016-11-07T09:10:42-05:00",
			"created": "2016-10-30T13:46:27-04:00"
		}
	]
}