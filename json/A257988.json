{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A257988",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 257988,
			"data": "0,0,1,0,1,1,1,0,2,1,1,0,1,1,2,0,1,2,1,1,2,1,1,0,2,1,3,1,1,2,1,0,2,1,2,0,1,1,2,0,1,2,1,1,3,1,1,0,2,2,2,1,1,3,2,1,2,1,1,1,1,1,3,0,2,2,1,1,2,2,1,0,1,1,3,1,2,2,1,0,4,1,1,1,2,1,2,1,1,3,2,1,2,1,2,0,1,2,3,2,1,2,1",
			"name": "Number of parts that are larger than the number of 1's in the partition having Heinz number n.",
			"comment": [
				"We define the Heinz number of a partition p = [p_1, p_2, ..., p_r] to be Product(p_j-th prime, j=1...r) (a concept used by _Alois P. Heinz_ in A215366 as an \"encoding\" of a partition). For example, for the partition [1, 1, 2, 4, 10] we get 2*2*3*7*29 = 2436.",
				"In the Maple program the subprogram b yields the number of 1's in the partition having Heinz number n and the subprogram B yields the partition having Heinz number n.",
				"The considered partition statistic is needed in the partition statistic \"crank\" (A257989)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A257988/b257988.txt\"\u003eTable of n, a(n) for n = 1..20000\u003c/a\u003e",
				"G. E. Andrews and F. G. Garvan, \u003ca href=\"http://dx.doi.org/10.1090/S0273-0979-1988-15637-6\"\u003eDyson's crank of a partition\u003c/a\u003e, Bull. Amer. Math. Soc., 18 (1988), 167-171."
			],
			"example": [
				"a(252) = 1 because the partition having Heinz number 252 = 2^2 * 3^2 * 7 is [1,1,2,2,4] and exactly one part, namely 4, is larger than 2 (the number of 1's).",
				"a(945) = 5 because the partition having Heinz number 945 = 3^3 * 5 * 7 is [2,2,2,3,4] and all parts are larger than 0 (the number of 1's)."
			],
			"maple": [
				"with(numtheory): a := proc (n) local b, B, c, i: b := proc (n) if `mod`(n, 2) = 1 then 0 else 1+b((1/2)*n) end if end proc: B := proc (n) local nn, j, m: nn := op(2, ifactors(n)): for j to nops(nn) do m[j] := op(j, nn) end do: [seq(seq(pi(op(1, m[i])), q = 1 .. op(2, m[i])), i = 1 .. nops(nn))] end proc: c := 0: for i to bigomega(n) do if b(n) \u003c B(n)[i] then c := c+1 else  end if end do: c end proc: seq(a(n), n = 1 .. 150);",
				"# second Maple program:",
				"a:= n-\u003e(l-\u003enops(select(x-\u003ex\u003eadd(`if`(i=1, 1, 0), i=l), l)))(",
				"       [seq(numtheory[pi](i[1])$i[2], i=ifactors(n)[2])]):",
				"seq(a(n), n=1..100);  # _Alois P. Heinz_, May 10 2016"
			],
			"mathematica": [
				"a[n_] := Function[l, Length[Select[l, # \u003e Sum[If[i == 1, 1, 0], {i, l}]\u0026]]][Flatten @ Table[Array[PrimePi[i[[1]]]\u0026, i[[2]]], {i, FactorInteger[n]}] ]; Table[a[n], {n, 1, 100}] (* _Jean-François Alcover_, Dec 10 2016 after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A257989, A215366."
			],
			"keyword": "nonn",
			"offset": "1,9",
			"author": "_Emeric Deutsch_, May 16 2015",
			"references": 2,
			"revision": 19,
			"time": "2016-12-10T06:23:58-05:00",
			"created": "2015-05-17T22:34:27-04:00"
		}
	]
}