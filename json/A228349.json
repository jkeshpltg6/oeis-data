{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A228349",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 228349,
			"data": "1,1,2,1,1,1,2,3,1,1,2,1,1,1,1,1,2,2,3,4,1,1,2,1,1,1,2,3,1,1,2,1,1,1,1,1,1,1,1,1,2,2,2,2,3,3,4,5,1,1,2,1,1,1,2,3,1,1,2,1,1,1,1,1,2,2,3,4,1,1,2,1,1,1,2,3,1,1,2,1,1,1,1,1,1,1",
			"name": "Triangle read by rows: T(j,k) is the k-th part in nondecreasing order of the j-th region of the set of compositions (ordered partitions) of n in colexicographic order, if 1\u003c=j\u003c=2^(n-1) and 1\u003c=k\u003c=A006519(j).",
			"comment": [
				"Triangle read by rows in which row n lists the A006519(n) elements of the row A001511(n) of triangle A090996, n \u003e= 1.",
				"The equivalent sequence for partitions is A220482."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A228349/b228349.txt\"\u003eTable of n, a(n) for n = 1..13312\u003c/a\u003e (rows 1 \u003c= n \u003c= 2^11 = 2048)."
			],
			"example": [
				"----------------------------------------------------------",
				".             Diagram                Triangle",
				"Compositions    of            of compositions (rows)",
				"of 5          regions          and regions (columns)",
				"----------------------------------------------------------",
				".            _ _ _ _ _",
				"5           |_        |                                 5",
				"1+4         |_|_      |                               1 4",
				"2+3         |_  |     |                             2   3",
				"1+1+3       |_|_|_    |                           1 1   3",
				"3+2         |_    |   |                         3       2",
				"1+2+2       |_|_  |   |                       1 2       2",
				"2+1+2       |_  | |   |                     2   1       2",
				"1+1+1+2     |_|_|_|_  |                   1 1   1       2",
				"4+1         |_      | |                 4               1",
				"1+3+1       |_|_    | |               1 3               1",
				"2+2+1       |_  |   | |             2   2               1",
				"1+1+2+1     |_|_|_  | |           1 1   2               1",
				"3+1+1       |_    | | |         3       1               1",
				"1+2+1+1     |_|_  | | |       1 2       1               1",
				"2+1+1+1     |_  | | | |     2   1       1               1",
				"1+1+1+1+1   |_|_|_|_|_|   1 1   1       1               1",
				".",
				"Written as an irregular triangle in which row n lists the parts of the n-th region the sequence begins:",
				"1;",
				"1,2;",
				"1;",
				"1,1,2,3;",
				"1;",
				"1,2;",
				"1;",
				"1,1,1,1,2,2,3,4;",
				"1;",
				"1,2;",
				"1;",
				"1,1,2,3;",
				"1;",
				"1,2;",
				"1;",
				"1,1,1,1,1,1,1,1,2,2,2,2,3,3,4,5;",
				"...",
				"Alternative interpretation of this sequence:",
				"Triangle read by rows in which row r lists the regions of the last section of the set of compositions of r:",
				"[1];",
				"[1,2];",
				"[1],[1,1,2,3];",
				"[1],[1,2],[1],[1,1,1,1,2,2,3,4];",
				"[1],[1,2],[1],[1,1,2,3],[1],[1,2],[1],[1,1,1,1,1,1,1,1,2,2,2,2,3,3,4,5];"
			],
			"mathematica": [
				"Table[Map[Length@ TakeWhile[IntegerDigits[#, 2], # == 1 \u0026] \u0026, Range[2^(# - 1), 2^# - 1]] \u0026@ IntegerExponent[2 n, 2], {n, 32}] // Flatten (* _Michael De Vlieger_, May 23 2017 *)"
			],
			"xref": [
				"Main triangle: Right border gives A001511. Row j has length A006519(j). Row sums give A038712.",
				"Cf. A001787, A001792, A011782, A029837, A045623, A065120, A070939, A090996, A186114, A187816, A187818, A206437, A220482, A228347, A228348, A228350, A228351, A228366, A228367, A228370, A228371, A228525, A228526."
			],
			"keyword": "nonn,tabf",
			"offset": "1,3",
			"author": "_Omar E. Pol_, Aug 26 2013",
			"references": 4,
			"revision": 19,
			"time": "2017-05-24T02:39:54-04:00",
			"created": "2013-10-22T12:38:36-04:00"
		}
	]
}