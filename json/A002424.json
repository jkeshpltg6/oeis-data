{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002424",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2424,
			"id": "M5058 N2188",
			"data": "1,-18,126,-420,630,-252,-84,-72,-90,-140,-252,-504,-1092,-2520,-6120,-15504,-40698,-110124,-305900,-869400,-2521260,-7443720,-22331160,-67964400,-209556900,-653817528,-2062039896,-6567978928,-21111360840",
			"name": "Expansion of (1-4*x)^(9/2).",
			"reference": [
				"A. Fletcher, J. C. P. Miller, L. Rosenhead and L. J. Comrie, An Index of Mathematical Tables. Vols. 1 and 2, 2nd ed., Blackwell, Oxford and Addison-Wesley, Reading, MA, 1962, Vol. 1, p. 55.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence).",
				"T. N. Thiele, Interpolationsrechnung. Teubner, Leipzig, 1909, p. 164."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A002424/b002424.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Alexander Barg, \u003ca href=\"https://arxiv.org/abs/2005.12995\"\u003eStolarsky's invariance principle for finite metric spaces\u003c/a\u003e, arXiv:2005.12995 [math.CO], 2020.",
				"N. J. A. Sloane, \u003ca href=\"/A000984/a000984.pdf\"\u003eNotes on A984 and A2420-A2424\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Sum_{m=0..n} binomial(n, m) * K_m(10), where K_m(x) = K_m(n, 2, x) is a Krawtchouk polynomial. - Alexander Barg, abarg(AT)research.bell-labs.com.",
				"a(n) = -(945/32)*4^n*Gamma(-9/2+n)/(sqrt(Pi)*Gamma(1+n)). - _Peter Luschny_, Dec 14 2015",
				"a(n) = (-4)^n*binomial(9/2, n). - _G. C. Greubel_, Jul 03 2019",
				"D-finite with recurrence: n*a(n) +2*(-2*n+11)*a(n-1)=0. - _R. J. Mathar_, Jan 16 2020"
			],
			"maple": [
				"A002424 := n -\u003e -(945/32)*4^n*GAMMA(-9/2+n)/(sqrt(Pi)*GAMMA(1+n)):",
				"seq(A002424(n),n=0..28); # _Peter Luschny_, Dec 14 2015"
			],
			"mathematica": [
				"CoefficientList[Series[(1-4x)^(9/2),{x,0,30}],x] (* _Harvey P. Dale_, Dec 27 2011 *)"
			],
			"program": [
				"(PARI) x='x+O('x^30); Vec((1-4*x)^(9/2)) \\\\ _Altug Alkan_, Dec 14 2015",
				"(PARI) vector(30, n, n--; (-4)^n*binomial(9/2, n)) \\\\ _G. C. Greubel_, Jul 03 2019",
				"(MAGMA) R\u003cx\u003e:=PowerSeriesRing(Rationals(), 30); Coefficients(R!( (1-4*x)^(9/2) )); // _G. C. Greubel_, Jul 03 2019",
				"(Sage) [(-4)^n*binomial(9/2, n) for n in (0..30)] # _G. C. Greubel_, Jul 03 2019"
			],
			"xref": [
				"Cf. A002420, A002421, A002422, A002423, A004001, A007054, A007272."
			],
			"keyword": "sign,easy,nice",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 5,
			"revision": 41,
			"time": "2020-08-17T20:50:20-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}