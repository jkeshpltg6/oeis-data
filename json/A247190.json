{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A247190",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 247190,
			"data": "0,3,2,4,3,3,4,4,2,6,5,8,4,2,2,8,5,7,4,9,3,8,15,2,14,2,9,2,7,10,3,4,10,13,13,11,2,10,17,12,3,27,3,7,31,19,10,2,21,14,8,2,14,11,7,15,17,14,8,17,3,14,4,10,20,7,2,24,2,2,10,5,18,44,21,36",
			"name": "Calculate the smallest positive residues of 2!,3!,...,(prime(n)-1)! modulo prime(n) and stop the calculation at the moment m when for the first time there appear two equal numbers. If m!==k!, then a(n)=k; but a(n)=0 if no such m exists.",
			"comment": [
				"Erdős asked whether there are any primes p \u003e 5 for which the numbers 2!, 3!,..., (p-1)! are all distinct mod p.",
				"According to [Trudgian], for n in the interval [4, pi(10^9)], we have a(n)\u003e0.",
				"There is a conjecture that answer to the Erdős question is no, which is equivalent to having a(n)\u003e0 for n\u003e=4."
			],
			"link": [
				"Peter J. C. Moses and Chai Wah Wu, \u003ca href=\"/A247190/b247190.txt\"\u003eTable of n, a(n) for n = 3..10002\u003c/a\u003e First 1000 terms from Peter J. C. Moses.",
				"Vladica Andrejic, Alin Bostan, Milos Tatarevic, \u003ca href=\"https://arxiv.org/abs/1904.09196\"\u003eImproved algorithms for left factorial residues\u003c/a\u003e, arXiv:1904.09196 [math.NT], 2019.",
				"B. Rokowska and A. Schinzel, \u003ca href=\"http://retro.seals.ch/digbib/view?pid=elemat-001:1960:15::144\"\u003eSur un problème de M. Erdős\u003c/a\u003e, Elem. Math., 15:84-85, 1960, MR117188 (22 #7970).",
				"T. Trudgian, \u003ca href=\"http://arxiv.org/abs/1310.6403\"\u003eThere are no socialist primes less than 10^9\u003c/a\u003e, arXiv:1310.6403 [math.NT], 2013.",
				"T. Trudgian, \u003ca href=\"http://www.emis.de/journals/INTEGERS/papers/o63/o63.Abstract.html\"\u003eThere are no socialist primes less than 10^9\u003c/a\u003e, INTEGERS, 14 (2014), A63."
			],
			"formula": [
				"If, for n\u003e=4, a(n)=0, then prime(n)==5 (mod 8) and (5/prime(n))=1, (-23/prime(n))=1 [Rokowska and Schinzel]; (1957/prime(n))=1 or (1957/prime(n))=-1 \u0026 ((4*y+25)/prime(n))=-1 for all y satisfying y*(y+4)*(y+6)==1 (mod prime(n)[Trudgian]."
			],
			"example": [
				"Let n=5, prime(n)=11. Then modulo 11 we have 2!==2, 3!==6, 4!==2. Since 4!==2!, then, by the definition, m=4,k=2. So a(5)=2.",
				"Let n=7, prime(n)=17. Then modulo 17 we have 2!==2, 3!==6, 4!==7, 5!==1, 6!==6. Since 6!==3!, then, by the definition, m=6,k=3. So a(7)=3."
			],
			"mathematica": [
				"Table[A247190={};",
				"NestWhile[#+1\u0026,2,(AppendTo[A247190,Mod[#!,Prime[n]]];Max[Last[Transpose[Tally[A247190]]]]\u003c=1)\u0026]-1;",
				"If[#\u003en,-1,#]\u0026[Position[A247190,Last[A247190],1,1][[1]][[1]]]+1,{n,3,100}] (* _Peter J. C. Moses_, Nov 23 2014 *)"
			],
			"program": [
				"(Python)",
				"from sympy import prime",
				"def A247190(n):",
				"....p, f, fv = prime(n), 1, {}",
				"....for i in range(2,p):",
				"........f = (f*i) % p",
				"........if f in fv:",
				"............return fv[f]",
				"........else:",
				"............fv[f] = i",
				"....else:",
				"........return 0 # _Chai Wah Wu_, Nov 25 2014"
			],
			"xref": [
				"Cf. A000040."
			],
			"keyword": "nonn",
			"offset": "3,2",
			"author": "_Vladimir Shevelev_, Nov 23 2014",
			"ext": [
				"More terms from _Peter J. C. Moses_, Nov 23 2014"
			],
			"references": 5,
			"revision": 47,
			"time": "2019-04-22T13:52:08-04:00",
			"created": "2014-11-24T20:38:40-05:00"
		}
	]
}