{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A036428",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 36428,
			"data": "1,225,43681,8473921,1643897025,318907548961,61866420601441,12001766689130625,2328280871270739841,451674487259834398561,87622522247536602581025,16998317641534841066320321,3297585999935511630263561281,639714685669847721430064568225",
			"name": "Square octagonal numbers.",
			"comment": [
				"Also, numbers simultaneously octagonal and centered octagonal. - _Steven Schlicker_, Apr 24 2007",
				"As n increases, this sequence is approximately geometric with common ratio r = lim(n -\u003e Infinity, a(n)/a(n-1)) = ( 2 + sqrt(3))^4 = 97 + 56 * sqrt(3). - _Ant King_, Nov 15 2011",
				"Each m-th octagonal number (or m-th term of A000567) is also a square number, when q=sqrt(m), q is integer and q=A079935(n) or n-th term of A079935. - _Sergey Pavlov_, Oct 18 2015"
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A036428/b036428.txt\"\u003eTable of n, a(n) for n = 1..437\u003c/a\u003e",
				"C. Gill, solution to question no. 8, \u003ca href=\"https://archive.org/details/mathematicalmis00unkngoog\"\u003eMathematical Miscellany\u003c/a\u003e, 1 (1836), pp. 220-225, at p. 223.",
				"S. C. Schlicker, \u003ca href=\"http://www.jstor.org/stable/10.4169/math.mag.84.5.339\"\u003eNumbers Simultaneously Polygonal and Centered Polygonal\u003c/a\u003e, Mathematics Magazine, Vol. 84, No. 5, December 2011, pp. 339-350.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/OctagonalSquareNumber.html\"\u003eOctagonal Square Number.\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (195,-195,1)."
			],
			"formula": [
				"Let x(n) + y(n)*sqrt(48) = (8+sqrt(48))*(7+sqrt(48))^n, s(n) = (y(n)+1)/2; then a(n) = (1/2)*(2+8*(s(n)^2-s(n))). - _Steven Schlicker_, Apr 24 2007",
				"a(n+2) = 194*a(n+1)-a(n)+32 and also a(n+1) = 97*a(n) + 56*sqrt(3*a(n)^2+a(n)). - _Richard Choulet_, Sep 26 2007",
				"G.f.: x(x^2+30x+1)/((1-x)(1-194x+x^2)).",
				"a(n) = -(1/6)+(7/12)*{[97-56*sqrt(3)]^n+[97+56*sqrt(3)]^n}-(1/3)*sqrt(3)*{[97-56*sqrt(3)]^n -[97+56*sqrt(3)]^n}, with n\u003e=0. - _Paolo P. Lava_, Nov 25 2008",
				"From _Ant King_, Nov 15 2011: (Start)",
				"a(n) = 1/12 * ((2 + sqrt(3))^(4n-2) + (2 - sqrt(3))^(4n-2) - 2).",
				"a(n) = floor (1/12 * (2 + sqrt(3))^(4n-2)).",
				"a(n) = 1/12 * ( (tan(5*Pi/12))^(4n-2) + (tan(Pi/12))^(4n-2) - 2).",
				"a(n) = floor (1/12 * tan(5*Pi/12)^(4n-2)).",
				"(End)"
			],
			"maple": [
				"A036428 := proc(n)",
				"        option remember;",
				"        if n \u003c 4 then",
				"                op(n,[1,225,43681]) ;",
				"        else",
				"                195*(procname(n-1)-procname(n-2))+procname(n-3) ;",
				"        end if;",
				"end proc: # _R. J. Mathar_, Nov 11 2011"
			],
			"mathematica": [
				"LinearRecurrence[{195,-195,1}, {1,225,43681}, 12] (* _Ant King_, Nov 15 2011 *)"
			],
			"program": [
				"(PARI) Vec(-x*(x^2+30*x+1)/((x-1)*(x^2-194*x+1)) + O(x^20)) \\\\ _Colin Barker_, Jun 24 2015",
				"(PARI) vector(15, n, floor((2+sqrt(3))^(4*n-2)/12)) \\\\ _Altug Alkan_, Oct 19 2015",
				"(MAGMA) [Floor(1/12*(2+Sqrt(3))^(4*n-2)): n in [1..20]]; // _Vincenzo Librandi_, Dec 04 2015"
			],
			"xref": [
				"Cf. A000567, A006051, A006060, A016754, A028230, A046184."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "Jean-Francois Chariot (jean-francois.chariot(AT)afoc.alcatel.fr)",
			"ext": [
				"More terms from _Eric W. Weisstein_",
				"Edited by _N. J. A. Sloane_, Oct 02 2007"
			],
			"references": 6,
			"revision": 57,
			"time": "2021-01-19T14:31:42-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}