{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A075844",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 75844,
			"data": "0,6,120,2394,47760,952806,19008360,379214394,7565279520,150926376006,3010962240600,60068318435994,1198355406479280,23907039811149606,476942440816512840,9514941776519107194,189821893089565631040",
			"name": "Numbers n such that 11*n^2 + 4 is a square.",
			"comment": [
				"Lim. n-\u003e Inf. a(n)/a(n-1) = 10 + 3*sqrt(11)."
			],
			"reference": [
				"A. H. Beiler, \"The Pellian\", ch. 22 in Recreations in the Theory of Numbers: The Queen of Mathematics Entertains. Dover, New York, New York, pp. 248-268, 1966.",
				"L. E. Dickson, History of the Theory of Numbers, Vol. II, Diophantine Analysis. AMS Chelsea Publishing, Providence, Rhode Island, 1999, pp. 341-400.",
				"Peter G. L. Dirichlet, Lectures on Number Theory (History of Mathematics Source Series, V. 16); American Mathematical Society, Providence, Rhode Island, 1999, pp. 139-147."
			],
			"link": [
				"Harvey P. Dale, \u003ca href=\"/A075844/b075844.txt\"\u003eTable of n, a(n) for n = 0..750\u003c/a\u003e",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"J. J. O'Connor and E. F. Robertson, \u003ca href=\"http://www-gap.dcs.st-and.ac.uk/~history/HistTopics/Pell.html\"\u003ePell's Equation\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PellEquation.html\"\u003ePell Equation.\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (20,-1)."
			],
			"formula": [
				"a(n) = ((10+3*sqrt(11))^n - (10-3*sqrt(11))^n) / sqrt(11).",
				"a(n) = 20*a(n-1) - a(n-2).",
				"G.f.: 6*x/(1 - 20*x + x^2).",
				"a(n) = (1/3)*(A075839(n+1) - A075839(n)), n\u003e=1. - _N. J. A. Sloane_, Sep 22 2004",
				"a(n) = 6*A075843(n). - _R. J. Mathar_, Jul 03 2011"
			],
			"maple": [
				"seq(coeff(series(6*x/(1-20*x+x^2), x, n+1), x, n), n = 0..20); # _G. C. Greubel_, Dec 06 2019"
			],
			"mathematica": [
				"LinearRecurrence[{20,-1},{0,6},20] (* _Harvey P. Dale_, May 28 2012 *)"
			],
			"program": [
				"(PARI) my(x='x+O('x^20)); concat([0], Vec(6*x/(1-20*x+x^2))) \\\\ _G. C. Greubel_, Dec 06 2019",
				"(MAGMA) R\u003cx\u003e:=PowerSeriesRing(Integers(), 20); [0] cat Coefficients(R!( 6*x/(1 - 20*x + x^2) )); // _G. C. Greubel_, Dec 06 2019",
				"(Sage)",
				"def A075844_list(prec):",
				"    P.\u003cx\u003e = PowerSeriesRing(ZZ, prec)",
				"    return P( 6*x/(1-20*x+x^2) ).list()",
				"A075844_list(20) # _G. C. Greubel_, Dec 06 2019",
				"(GAP) a:=[0,6];; for n in [3..20] do a[n]:=20*a[n-1]-a[n-2]; od; a; # _G. C. Greubel_, Dec 06 2019"
			],
			"xref": [
				"Cf. A221762."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Gregory V. Richardson_, Oct 14 2002",
			"references": 3,
			"revision": 32,
			"time": "2019-12-06T16:07:27-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}