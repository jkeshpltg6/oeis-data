{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A081659",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 81659,
			"data": "1,2,4,6,9,13,19,28,42,64,99,155,245,390,624,1002,1613,2601,4199,6784,10966,17732,28679,46391,75049,121418,196444,317838,514257,832069,1346299,2178340,3524610,5702920,9227499,14930387,24157853,39088206",
			"name": "a(n) = n + Fibonacci(n+1).",
			"comment": [
				"Row sums of triangle A135222. - _Gary W. Adamson_, Nov 23 2007",
				"a(n) is the F(n+1)-th highest positive integer not equal to any a(k), 1 \u003c= k \u003c= n-1, where F(n) = Fibonacci numbers = A000045(n). - _Jaroslav Krizek_, Oct 28 2009"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A081659/b081659.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-2,-1,1)."
			],
			"formula": [
				"a(n) = (sqrt(5)*(1+sqrt(5))^(n+1) - sqrt(5)*(1-sqrt(5))^(n+1))/(10*2^n) + n.",
				"G.f.: (1-x-x^3)/((1-x-x^2)*(1-x)^2).",
				"From _Jaroslav Krizek_, Oct 28 2009: (Start)",
				"a(0) = 1, a(n) = a(n-1) + A000045(n-1) + 1 for n \u003e= 1.",
				"a(0) = 1, a(n) = a(n-1) + A000045(n+1) - A000045(n) + 1 for n \u003e= 1.",
				"a(0) = 1, a(1) = 2, a(2) = 4, a(n) = a(n-1) + a(n-2) - (n-3) n \u003e= 3. (End)",
				"E.g.f.: (1/10)*exp(-2*x/(1+sqrt(5)))*(5 - sqrt(5) + (5 + sqrt(5))*exp(sqrt(5)*x) + 10*exp((1/2)*(1+sqrt(5))*x)*x). - _Stefano Spezia_, Nov 20 2019"
			],
			"maple": [
				"with(combinat); seq(n + fibonacci(n+1), n=0..40); # _G. C. Greubel_, Nov 20 2019"
			],
			"mathematica": [
				"Table[ Fibonacci[n+1]+n, {n, 0, 38}] (* _Vladimir Joseph Stephan Orlovsky_, Apr 03 2011 *)",
				"CoefficientList[Series[(x^3+x-1)/((x-1)^2 (x^2+x-1)), {x, 0, 40}], x] (* _Vincenzo Librandi_, Aug 10 2013 *)",
				"LinearRecurrence[{3,-2,-1,1},{1,2,4,6},40] (* _Harvey P. Dale_, Mar 02 2016 *)"
			],
			"program": [
				"(MuPAD) numlib::fibonacci(n)+n-1 $ n = 1..48; // _Zerinvary Lajos_, May 08 2008",
				"(MAGMA) [n+Fibonacci(n+1): n in [0..40]]; // _Vincenzo Librandi_, Aug 10 2013",
				"(PARI) a(n)=n+fibonacci(n) \\\\ _Charles R Greathouse IV_, Oct 07 2015",
				"(Sage) [n+fibonacci(n+1) for n in range(40)] # _G. C. Greubel_, Feb 12 2019",
				"(GAP) List([0..40], n-\u003e n + Fibonacci(n+1) ); # _G. C. Greubel_, Nov 20 2019"
			],
			"xref": [
				"Cf. A000045, A001611 (first differences), A002062, A135222."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Paul Barry_, Mar 26 2003",
			"references": 10,
			"revision": 49,
			"time": "2019-11-20T06:15:49-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}