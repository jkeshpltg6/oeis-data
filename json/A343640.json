{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A343640",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 343640,
			"data": "0,0,0,0,0,1,1,0,1,1,1,1,0,1,1,-1,1,1,-1,0,1,-1,-1,1,0,-1,1,1,-1,1,1,0,0,1,1,0,0,1,0,-1,1,0,-1,0,0,-1,-1,0,0,-1,0,1,-1,0,1,0,-1,1,1,-1,0,1,-1,-1,1,-1,-1,0,-1,-1,-1,-1,0,-1,-1,1,-1,-1,0,0,-1,0,0,-2,1,0,-2,1,1,-2,0,1,-2,-1,1,-2,-1,0,-2,-1,-1,-2,0,-1,-2,1,-1,-2",
			"name": "Coordinate triples (x(n), y(n), z(n); n \u003e= 0) of the 3D square spiral filling space with shells of increasing radius for the sup-norm, in turn filled by squares extending from one pole to the opposite one.",
			"comment": [
				"This is a 3D generalization of the 2D square spiral and could be used to produce a 3D variant of Ulam's prime spiral.",
				"See A343630 for an analog using the Euclidean or 2-norm instead of the sup- or oo-norm used here, so points are partitioned in spheres and circles instead of squares and cubes here.",
				"The integer lattice points, Z^3, are listed in order of increasing sup norm R = max(|x|, |y|, |z|). Each \"sphere\" or shell of given radius R is filled starting at the North or South pole using concentric squares on the top and bottom face and squares of fixed size (2R+1) X (2R+1) at intermediate z-coordinates. Each square (circle for the sup-norm) is filled in the sense of increasing longitude, where the positive x axis corresponds to longitude 0, i.e., the points (r,0,z), (0,r,z), (-r,0,z) and (0,-r,z) are visited in this order. The z-values are alternatively increasing and decreasing (so over a period of two shells they follow the same rectangle-wave shape as the x-values do over the period of each square).",
				"The sequence can be seen as a table with row length of 3, where each row corresponds to the (x,y,z)-coordinates of one point (then the three columns are A343641, A343642 and A343643), or as a table with row lengths 3*A010014, where A010014(r) is the number of points with sup-norm r.",
				"There are (2n+1)^3 integer lattice points with sup norm \u003c= n. Therefore, the point number n (where 0 is the origin) is in the shell r = round(n^(1/3)/2) = floor(...+1/2). Within shell r, which starts with the point number (2r-1)^3 (except for r=0), the first and last (2r+1)^2 points are on square spirals on the top and bottom faces, and the other points are on 2r-1 squares forming \"belts\" of 8r points each, on the side faces of the cube."
			],
			"link": [
				"Hugo Pfoertner, \u003ca href=\"/A343640/a343640.png\"\u003eVisualization of shells 0, 1, 2\u003c/a\u003e.",
				"Hugo Pfoertner, \u003ca href=\"/A343640/a343640_1.txt\"\u003eList of terms up to r = 10 (n = 9260)\u003c/a\u003e, [n, x, y, z]."
			],
			"example": [
				"Shell r = 0 is the origin, {(0,0,0)}.",
				"Shell r = 1 contains the 3*3 + 4*2 + 3*3 = 26 points with oo-norm 1, i.e., all points with coordinates within {-1, 0, 1} except for the origin. They are listed in a square spiral starting at the North Pole: (0,0,1), (1,0,1), (1,1,1), (0,1,1), (-1,1,1), (-1,0,1), (-1,-1,1), (0,-1,1), (1,-1,1); then on the equator:  (1,0,0), (1,1,0), (0,1,0), (-1,1,0), (-1,0,0), (-1,-1,0), (0,-1,0), (1,-1,0), and then on the South face using an inward spiral: (1,0,-1), (1,1,-1), (0,1,-1), (-1,1,-1), (-1,0,-1), (-1,-1,-1), (0,-1,-1), (1,-1,-1), (0,0,-1).",
				"Since there are no empty shells, the z-coordinate is always increasing for even r and decreasing for odd r."
			],
			"program": [
				"(PARI) A343640_row(n)={local(L=List(), a(r, z, d=I)= if(r, for(i=1,8*r, listput(L,[real(r),imag(r),z]); r+=d; abs(real(r))==abs(imag(r)) \u0026\u0026 d*=I), listput(L,[0,0,z])), s=(-1)^n /* flip South \u003c-\u003e North for odd n */); /* main prog: (1) square spiral on South face from center to board */ for(d=!n,n, a(d,-s*n)); /* (2) \"equatorial(?) bands\" from South to North */ for(z=1-n,n-1, a(n,s*z)); /* (3) square spiral on North face ending in pole */ for(d=0,n, a(n-d,s*n)); Vec(L)} \\\\ row n of the table = list of points (x,y,z) in the shell n, i.e., with sup norm n. [Missing \"s*\" in a(n,s*z) added on May 27 2021]",
				"A343640_vec=concat([A343640_row(r) | r\u003c-[0..2]]) \\\\ From r=0 up to n there are (2n+1)^3 points with 3 coordinates each!"
			],
			"xref": [
				"Cf. A343641, A343642, A343643 (list of x, y resp. z-coordinates only).",
				"Cf. A343631, A343632, A343633 (variant using the Euclidean norm =\u003e circle shaped spirals), A342561, A343632, A342563 (another variant).",
				"Cf. A010014 (number of points on a shell with given radius), A016755.",
				"Cf. A174344, A268038, A274923 (2-dimensional square spiral)."
			],
			"keyword": "sign,tabf",
			"offset": "0,84",
			"author": "_M. F. Hasler_, Apr 28 2021",
			"references": 10,
			"revision": 44,
			"time": "2021-06-16T20:58:14-04:00",
			"created": "2021-05-01T10:04:42-04:00"
		}
	]
}