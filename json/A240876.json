{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A240876",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 240876,
			"data": "1,23,265,2047,11969,56695,227305,795455,2485825,7059735,18474633,45046719,103274625,224298231,464387817,921406335,1759885185,3248227095,5812626185,10113604735,17152640321,28418229623,46082942185,73265596607,114375683009",
			"name": "Expansion of (1 + x)^11 / (1 - x)^12.",
			"comment": [
				"Also 11-dimensional centered hyperoctahedron numbers (see Deza in References) or Crystal ball sequence for 11-dimensional cubic lattice.",
				"Sum_{n \u003e= 0} 1/a(n) = 1.047847848425287358769594801715758965260..."
			],
			"reference": [
				"E. Deza and M. M. Deza, Figurate numbers, World Scientific Publishing (2012), page 230 (paragraph 3.6.6)."
			],
			"link": [
				"Bruno Berselli, \u003ca href=\"/A240876/b240876.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"D. Bump, K. Choi, P. Kurlberg, and J. Vaaler, \u003ca href=\"http://www.cecm.sfu.ca/~choi/paper/lrh.pdf\"\u003eA local Riemann hypothesis, I\u003c/a\u003e pages 16 and 17.",
				"OEIS Wiki, \u003ca href=\"http://oeis.org/wiki/Centered_orthoplicial_polytopic_numbers#Table_of_sequences\"\u003eCentered orthoplex numbers\u003c/a\u003e, see Table of formulas and values (row 11).",
				"\u003ca href=\"/index/Rec#order_12\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (12,-66,220,-495,792,-924,792,-495,220,-66,12,-1)."
			],
			"formula": [
				"G.f.: (1 + x)^11 / (1 - x)^12.",
				"a(n) = 12*a(n-1) - 66*a(n-2) + 220*a(n-3) - 495*a(n-4) + 792*a(n-5) - 924*a(n-6) + 792*a(n-7) - 495*a(n-8) + 220*a(n-9) - 66*a(n-10) + 12*a(n-11) - a(n-12), with initial values as shown.",
				"a(n) = (2*n + 1)*(2*n*(n + 1)*(n^2 + n + 5)*(2*n^2 + 2*n + 51)*(n^4 + 2*n^3 + 68*n^2 + 67*n + 537)/155925 + 1).",
				"a(n) = A008421(n) + 2*Sum_{i=0..n-1} A008421(i) for n \u003e 0, a(0) = 1.",
				"a(n) = Sum_{k = 0..min(11,n)} 2^k*binomial(11,k)*binomial(n,k). See Bump et al. - _Tom Copeland_, Sep 05 2014"
			],
			"mathematica": [
				"CoefficientList[Series[(1 + x)^11/(1 - x)^12, {x, 0, 30}], x]",
				"LinearRecurrence[{12,-66,220,-495,792,-924,792,-495,220,-66,12,-1},{1,23,265,2047,11969,56695,227305,795455,2485825,7059735,18474633,45046719},30] (* _Harvey P. Dale_, Apr 15 2018 *)"
			],
			"program": [
				"(PARI) Vec((1+x)^11/(1-x)^12+O(x^30))",
				"(MAGMA) m:=30; R\u003cx\u003e:=PowerSeriesRing(Integers(),m); Coefficients(R!((1+x)^11/(1-x)^12));",
				"(Maxima) makelist(coeff(taylor((1+x)^11/(1-x)^12, x, 0, n), x, n), n, 0, 30);",
				"(Sage)",
				"m = 30; L.\u003cx\u003e = PowerSeriesRing(ZZ, m)",
				"f = (1+x)^11/(1-x)^12",
				"print(f.coefficients())"
			],
			"xref": [
				"Cf. similar sequences with g.f. (1+x)^m/(1-x)^(m+1): A005408 (m=1), A001844 .. A001849 (m=2..7), A008417 (m=8), A008419 (m=9), A008421 (m=10), this sequence (m=11), A053805 (m=12).",
				"Subsequence of the odd numbers, A005408."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Bruno Berselli_, Apr 16 2014",
			"ext": [
				"Edited by _M. F. Hasler_, May 07 2018"
			],
			"references": 11,
			"revision": 53,
			"time": "2020-03-01T04:29:53-05:00",
			"created": "2014-04-16T18:34:31-04:00"
		}
	]
}