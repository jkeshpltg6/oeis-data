{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A056671",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 56671,
			"data": "1,2,2,1,2,4,2,1,1,4,2,2,2,4,4,1,2,2,2,2,4,4,2,2,1,4,1,2,2,8,2,1,4,4,4,1,2,4,4,2,2,8,2,2,2,4,2,2,1,2,4,2,2,2,4,2,4,4,2,4,2,4,2,1,4,8,2,2,4,8,2,1,2,4,2,2,4,8,2,2,1,4,2,4,4,4,4,2,2,4,4,2,4,4,4,2,2,2,2,1,2,8,2,2,8",
			"name": "1 + the number of unitary and squarefree divisors of n = number of divisors of reduced squarefree part of n.",
			"comment": [
				"Note that 1 is regarded as free of squares of primes and is also a square number and a unitary divisor."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A056671/b056671.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Steven R. Finch, \u003ca href=\"/A007947/a007947.pdf\"\u003eUnitarism and Infinitarism\u003c/a\u003e, February 25, 2004. [Cached copy, with permission of the author]",
				"\u003ca href=\"/index/Eu#epf\"\u003eIndex entries for sequences computed from exponents in factorization of n\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A000005(A055231(n)) = A000005(A007913(n)/A055229(n)).",
				"Multiplicative with a(p) = 2 and a(p^e) = 1 for e \u003e 1. a(n) = 2^A056169(n). - _Vladeta Jovovic_, Nov 01 2001",
				"a(n) = A034444(n) - A056674(n). - _Antti Karttunen_, Jul 19 2017"
			],
			"example": [
				"n = 252 = 2*2*3*3*7 has 18 divisors, 8 unitary and 8 squarefree divisors of which 2 are unitary and squarefree, divisors {1,7};",
				"n = 2520 = 2*2*2*3*3*5*7 has 48 divisors, 16 unitary and 16 squarefree divisors of which {1,5,7,35} are both, thus a(2520) = 4.",
				"a(2520) = a(2^3*3^2*5*7) = a(2^3)*a(3^2)*a(5)*a(7) = 1*1*2*2 = 4."
			],
			"mathematica": [
				"Array[DivisorSigma[0, #] \u0026@ Denominator[#/Apply[Times, FactorInteger[#][[All, 1]]]^2] \u0026, 105] (* or *)",
				"Table[DivisorSum[n, 1 \u0026, And[SquareFreeQ@ #, CoprimeQ[#, n/#]] \u0026], {n, 105}] (* _Michael De Vlieger_, Jul 19 2017 *)",
				"f[p_,e_] := If[e==1, 2, 1]; a[1] = 1; a[n_] := Times @@ (f @@@ FactorInteger[n]); Array[a, 100] (* _Amiram Eldar_, May 14 2019 *)"
			],
			"program": [
				"(PARI)",
				"A057521(n) = { my(f=factor(n)); prod(i=1, #f~, if(f[i, 2]\u003e1, f[i, 1]^f[i, 2], 1)); } \\\\ _Charles R Greathouse IV_, Aug 13 2013",
				"A055231(n) = n/A057521(n);",
				"A056671(n) = numdiv(A055231(n));",
				"\\\\ Or:",
				"A055229(n) = { my(c=core(n)); gcd(c, n/c); }; \\\\ This function from _Charles R Greathouse IV_, Nov 20 2012",
				"A056671(n) = numdiv(core(n)/A055229(n)); \\\\ _Antti Karttunen_, Jul 19 2017",
				"(Scheme) (define (A056671 n) (if (= 1 n) n (* (if (= 1 (A067029 n)) 2 1) (A056671 (A028234 n))))) ;; (After the given multiplicative formula) - _Antti Karttunen_, Jul 19 2017",
				"(Python)",
				"from sympy import factorint, prod",
				"def a(n): return 1 if n==1 else prod([2 if e==1 else 1 for p, e in factorint(n).items()])",
				"print([a(n) for n in range(1, 51)]) # _Indranil Ghosh_, Jul 19 2017"
			],
			"xref": [
				"Cf. A000005, A007913, A034444, A055229, A055231, A056169, A056674."
			],
			"keyword": "mult,nonn",
			"offset": "1,2",
			"author": "_Labos Elemer_, Aug 10 2000",
			"references": 7,
			"revision": 41,
			"time": "2020-05-04T15:00:56-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}