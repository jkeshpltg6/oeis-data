{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A037227",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 37227,
			"data": "1,3,1,5,1,3,1,7,1,3,1,5,1,3,1,9,1,3,1,5,1,3,1,7,1,3,1,5,1,3,1,11,1,3,1,5,1,3,1,7,1,3,1,5,1,3,1,9,1,3,1,5,1,3,1,7,1,3,1,5,1,3,1,13,1,3,1,5,1,3,1,7,1,3,1,5,1,3,1,9,1,3,1,5,1,3,1,7,1,3,1,5,1,3,1,11,1,3,1,5,1,3",
			"name": "If n = 2^m*k, k odd, then a(n) = 2*m+1.",
			"comment": [
				"a(A005408(n)) = 1; a(A016825(n)) = 3; A017113(a(n)) = 5; A051062(a(n)) = 7. - _Reinhard Zumkeller_, Jun 30 2012",
				"Take the number of rightmost zeros in the binary expansion of n, double it, and increment it by 1. - _Ralf Stephan_, Aug 22 2013",
				"Gives the maximum possible number of n X n complex Hermitian matrices with the property that all of their nonzero real linear combinations are nonsingular (see Adams et al. reference). - _Nathaniel Johnston_, Dec 11 2013"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A037227/b037227.txt\"\u003eTable of n, a(n) for n=1..1024\u003c/a\u003e",
				"J. F. Adams, P. D. Lax, and R. S. Phillips, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9939-1965-0179183-6\"\u003eOn matrices whose real linear combinations are nonsingular\u003c/a\u003e, Proceedings of the American Mathematical Society, 16:318-322, 1965.",
				"D. B. Shapiro, \u003ca href=\"http://www.jstor.org/stable/2589421\"\u003eProblem 10456: Anticommuting Matrices\u003c/a\u003e, Amer. Math. Monthly, 105 (1998), 565-566.",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Sum_{d divides n} (-1)^(d+1)*mu(d)*tau(n/d). Multiplicative with a(p^e) = 2*e+1 if p = 2; 1 if p \u003e 2. - _Vladeta Jovovic_, Apr 27 2003",
				"a(n) = a(n-1)+(-1)^n*(a(floor(n/2))+1). - _Vladeta Jovovic_, Apr 27 2003",
				"a(2*n) = a(n) + 2, a(2*n+1) = 1. a(n) = 2*A007814(n) + 1. - _Ralf Stephan_, Oct 07 2003",
				"a((2*n-1)*2^p)  = 2*p+1, p  \u003e= 0 and n \u003e= 1. - _Johannes W. Meijer_, Feb 07 2013",
				"From _Peter Bala, Feb 07 2016: (Start)",
				"a(n) = ( A002487(n-1) + A002487(n+1) )/A002487(n).",
				"a(n*2^(k+1) + 2^k) = 2*k + 1 for n,k \u003e= 0; thus a(2*n+1) = 1, a(4*n+2) = 3, a(8*n+4) = 5, a(16*n+8) = 7 and so on. Note the square array ( n*2^(k+1) + 2^k - 1 )n, k\u003e=0 is the transpose of A075300.",
				"G.f.: Sum_{n \u003e= 0} (2*n + 1)*x^(2^n)/(1 - x^(2^(n+1))). (End)",
				"a(n) = 2*floor(A002487(n-1)/A002487(n))+1 for n \u003e 1. - _I. V. Serov_, Jun 15 2017"
			],
			"maple": [
				"nmax:=102: for p from 0 to ceil(simplify(log[2](nmax))) do for n from 1 to ceil(nmax/(p+2)) do a((2*n-1)*2^p):= 2*p+1: od: od: seq(a(n), n=1..nmax);  # _Johannes W. Meijer_, Feb 07 2013"
			],
			"mathematica": [
				"a[n_] := Sum[(-1)^(d+1)*MoebiusMu[d]*DivisorSigma[0, n/d], {d, Divisors[n]}]; Table[a[n], {n, 1, 102}] (* _Jean-François Alcover_, Dec 31 2012, after _Vladeta Jovovic_ *)",
				"f[n_]:=Module[{z=Last[Split[IntegerDigits[n,2]]]},If[Union[z]={0},2* Length[ z]+1,1]]; Array[f,110] (* _Harvey P. Dale_, Jun 16 2019, after Ralf Stephan *)",
				"Table[2 IntegerExponent[n, 2] + 1, {n, 120}] (* _Vincenzo Librandi_, Jun 19 2019 *)"
			],
			"program": [
				"(Haskell)",
				"a037227 = (+ 1) . (* 2) . a007814  -- _Reinhard Zumkeller_, Jun 30 2012",
				"(R)",
				"maxrow \u003c- 6 # by choice",
				"a \u003c- 1",
				"for(m in 0:maxrow){",
				"for(k in 0:(2^m-1)) {",
				"   a[2^(m+1)    +k] \u003c- a[2^m+k]",
				"   a[2^(m+1)+2^m+k] \u003c- a[2^m+k]",
				"}",
				"   a[2^(m+1)      ] \u003c- a[2^(m+1)] + 2",
				"}",
				"a",
				"# _Yosu Yurramendi_, May 21 2015",
				"(PARI) a(n)=2*valuation(n,2)+1 \\\\ _Charles R Greathouse IV_, May 21 2015",
				"(MAGMA) [2*Valuation(n, 2)+1: n in [1..120]]; // _Vincenzo Librandi_, Jun 19 2019"
			],
			"xref": [
				"Cf. A001511, A002487, A007814, A075300, A220466."
			],
			"keyword": "nonn,easy,nice,mult",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Erich Friedman_"
			],
			"references": 14,
			"revision": 55,
			"time": "2019-06-19T02:45:53-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}