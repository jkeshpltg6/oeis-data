{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A299768",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 299768,
			"data": "1,2,4,4,4,9,7,12,9,16,12,16,18,16,25,19,32,36,32,25,36,30,44,54,48,50,36,49,45,76,81,96,75,72,49,64,67,104,135,128,125,108,98,64,81,97,164,189,208,200,180,147,128,81,100,139,224,279,288,300,252,245,192,162,100,121",
			"name": "Triangle read by rows: T(n,k) = sum of all squares of the parts k in all partitions of n, with n \u003e= 1, 1 \u003c= k \u003c= n.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A299768/b299768.txt\"\u003eRows n = 1..200, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = (k^2)*A066633(n,k) = k*A138785(n,k). - _Omar E. Pol_, Jun 07 2018"
			],
			"example": [
				"Triangle begins:",
				"   1;",
				"   2,  4;",
				"   4,  4,  9;",
				"   7, 12,  9, 16;",
				"  12, 16, 18, 16, 25,",
				"  19, 32, 36, 32, 25, 36;",
				"  30, 44, 54, 48, 50, 36, 49;",
				"...",
				"For n = 4 the partitions of 4 are [4], [2, 2], [3, 1], [2, 1, 1], [1, 1, 1, 1], so the squares of the parts are respectively [16], [4, 4], [9, 1], [4, 1, 1], [1, 1, 1, 1]. The sum of the squares of the parts 1 is 1 + 1 + 1 + 1 + 1 + 1 + 1 = 7. The sum of the squares of the parts 2 is 4 + 4 + 4 = 12. The sum of the squares of the parts 3 is 9. The sum of the squares of the parts 4 is 16. So the fourth row of triangle is [7, 12, 9, 16]."
			],
			"maple": [
				"b:= proc(n, i) option remember; `if`(n=0 or i=1, 1+n*x, b(n, i-1)+",
				"      (p-\u003e p+(coeff(p, x, 0)*i^2)*x^i)(b(n-i, min(n-i, i))))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=1..n))(b(n$2)):",
				"seq(T(n), n=1..14);  # _Alois P. Heinz_, Mar 20 2018"
			],
			"mathematica": [
				"b[n_, i_] := b[n, i] = If[n == 0 || i == 1, 1 + n*x, b[n, i - 1] + # + (Coefficient[#, x, 0]*i^2*x^i)\u0026[b[n - i, Min[n - i, i]]]];",
				"T[n_] := Table[Coefficient[#, x, i], {i, 1, n}]\u0026[b[n, n]];",
				"Table[T[n], {n, 1, 14}] // Flatten (* _Jean-François Alcover_, May 22 2018, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(PARI) row(n) = {v = vector(n); forpart(p=n, for(k=1, #p, v[p[k]] += p[k]^2;);); v;} \\\\ _Michel Marcus_, Mar 20 2018"
			],
			"xref": [
				"Column 1 is A000070.",
				"Leading diagonal is A000290, n \u003e= 1.",
				"Row sums give A066183.",
				"Both A180681 and A206561 have the same row sums as this triangle.",
				"Cf. A066633, A138785."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Omar E. Pol_, Mar 19 2018",
			"ext": [
				"More terms from _Michel Marcus_, Mar 20 2018"
			],
			"references": 5,
			"revision": 47,
			"time": "2018-06-07T21:58:44-04:00",
			"created": "2018-03-26T18:24:57-04:00"
		}
	]
}