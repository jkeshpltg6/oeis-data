{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A031173",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 31173,
			"data": "240,275,693,720,792,1155,1584,2340,2640,2992,3120,5984,6325,6336,6688,6732,8160,9120,9405,10725,11220,12075,13860,14560,16800,17472,17748,18560,19305,21476,23760,23760,24684,25704,26649,29920,30780",
			"name": "Longest edge a of smallest (measured by the longest edge) primitive Euler bricks (a, b, c, sqrt(a^2 + b^2), sqrt(b^2 + c^2), sqrt(a^2 + c^2) are integers).",
			"comment": [
				"Primitive means that gcd(a,b,c) = 1.",
				"The trirectangular tetrahedron (0, a=a(n), b=A031174(n), c=A031175(n)) has three right triangles with area divisible by 6 = 2*3 each and a volume divisible by 15840 = 2^5*3^2*5*11. The biquadratic term b^2*c^2 + a^2*(b^2 + c^2) is divisible by 144 = 2^4*3^2. Also gcd(b + c, c + a, a + b) = 1. - _Ralf Steiner_, Nov 22 2017",
				"There are some longest edges a which occur multiple times, such as a(31) = a(32)) = 23760. - _Ralf Steiner_, Jan 07 2018",
				"A trirectangular tetrahedron is never a perfect body (in the sense of Wyss) because it always has an irrational area of the base (a,b,c) whose value is half of the length of the space-diagonal of the related cuboid (b*c, c*a, a*b). The trirectangular bipyramid (6 faces, 9 edges, 5 vertices) built from these trirectangular tetrahedrons and the related left-handed ones connected on their bases have rational numbers for volume, face areas and edge lengths, but again an irrational value for the length of the space-diagonal which is a rational part of the length of the space-diagonal of the related cuboid (b*c, c*a, a*b). - _Ralf Steiner_, Jan 14 2018"
			],
			"reference": [
				"Calculated by F. Helenius (fredh(AT)ix.netcom.com)."
			],
			"link": [
				"Giovanni Resta, \u003ca href=\"/A031173/b031173.txt\"\u003eTable of n, a(n) for n = 1..3556\u003c/a\u003e",
				"R. R. Gallyamov, I. R. Kadyrov, D. D. Kashelevskiy, \u003ca href=\"https://arxiv.org/abs/1601.00636\"\u003eA fast modulo primes algorithm for searching perfect cuboids and its implementation\u003c/a\u003e, arXiv preprint arXiv:1601.00636 [math.NT], 2016.",
				"A. A. Masharov and R. A. Sharipov, \u003ca href=\"http://arxiv.org/abs/1504.07161\"\u003eA strategy of numeric search for perfect cuboids in the case of the second cuboid conjecture\u003c/a\u003e, arXiv preprint arXiv:1504.07161 [math.NT], 2015.",
				"Dustin Moody, Mohammad Sadek, Arman Shamsi Zargar, \u003ca href=\"https://doi.org/10.1216/RMJ-2019-49-7-2253\"\u003eFamilies of elliptic curves of rank \u003e= 5 over Q(t)\u003c/a\u003e, Rocky Mountain Journal of Mathematics (2019) Vol. 49, No. 7, 2253-2266.",
				"Giovanni Resta, \u003ca href=\"/A031173/a031173.txt\"\u003eThe 3556 primitive bricks with c \u003c b \u003c a \u003c 5*10^8\u003c/a\u003e",
				"J. Ramsden and H. Sharipov, \u003ca href=\"http://arxiv.org/abs/1207.6764\"\u003eInverse problems associated with perfect cuboids\u003c/a\u003e, arXiv preprint arXiv:1207.6764 [math.NT], 2012.",
				"J. Ramsden and H. Sharipov, \u003ca href=\"http://arxiv.org/abs/1208.1859\"\u003eOn singularities of the inverse problems associated with perfect cuboids\u003c/a\u003e, arXiv preprint arXiv:1208.1859 [math.NT], 2012.",
				"Ruslan Sharipov, \u003ca href=\"http://arxiv.org/abs/1108.5348\"\u003ePerfect cuboids and irreducible polynomials\u003c/a\u003e, arXiv:1108.5348 [math.NT], 2011.",
				"Ruslan Sharipov, \u003ca href=\"http://arxiv.org/abs/1109.2534\"\u003eA note on the first cuboid conjecture\u003c/a\u003e, arXiv:1109.2534 [math.NT], 2011.",
				"Ruslan Sharipov, \u003ca href=\"http://arxiv.org/abs/1201.1229\"\u003eA note on the second cuboid conjecture. Part I\u003c/a\u003e, arXiv:1201.1229 [math.NT], 2012.",
				"Ruslan Sharipov, \u003ca href=\"http://arxiv.org/abs/1205.3135\"\u003ePerfect cuboids and multisymmetric polynomials\u003c/a\u003e, arXiv preprint arXiv:1205.3135 [math.NT], 2012.",
				"Ruslan Sharipov, \u003ca href=\"http://arxiv.org/abs/1206.6769\"\u003eOn an ideal of multisymmetric polynomials associated with perfect cuboids\u003c/a\u003e, arXiv preprint arXiv:1206.6769 [math.NT], 2012.",
				"Ruslan Sharipov, \u003ca href=\"http://arxiv.org/abs/1207.2102\"\u003eOn the equivalence of cuboid equations and their factor equations\u003c/a\u003e, arXiv preprint arXiv:1207.2102 [math.NT], 2012.",
				"Ruslan Sharipov, \u003ca href=\"http://arxiv.org/abs/1207.4081\"\u003eA biquadratic Diophantine equation associated with perfect cuboids\u003c/a\u003e, arXiv preprint arXiv:1207.4081 [math.NT], 2012.",
				"Ruslan Sharipov, \u003ca href=\"http://arxiv.org/abs/1208.0308\"\u003eOn a pair of cubic equations associated with perfect cuboids\u003c/a\u003e, arXiv preprint arXiv:1208.0308 [math.NT], 2012.",
				"Ruslan Sharipov, \u003ca href=\"http://arxiv.org/abs/1208.1227\"\u003eOn two elliptic curves associated with perfect cuboids\u003c/a\u003e, arXiv preprint arXiv:1208.1227 [math.NT], 2012.",
				"Ruslan Sharipov, \u003ca href=\"http://arxiv.org/abs/1505.02745\"\u003eAsymptotic estimates for roots of the cuboid characteristic equation in the linear region\u003c/a\u003e, arXiv preprint arXiv:1505.02745 [math.NT], 2015.",
				"Ruslan Sharipov, \u003ca href=\"http://arxiv.org/abs/1505.00724\"\u003eReverse asymptotic estimates for roots of the cuboid characteristic equation in the case of the second cuboid conjecture\u003c/a\u003e, arXiv preprint arXiv:1505.00724 [math.NT], 2015.",
				"Ruslan Sharipov, \u003ca href=\"http://arxiv.org/abs/1507.01861\"\u003eA note on invertible quadratic transformations of the real plane\u003c/a\u003e, arXiv preprint arXiv:1507.01861 [math.AG], 2015.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/EulerBrick.html\"\u003eEuler Brick\u003c/a\u003e",
				"Walter Wyss, \u003ca href=\"https://arxiv.org/abs/1506.02215\"\u003eNo Perfect Cuboid\u003c/a\u003e, arXiv:1506.02215 [math.NT], 2015-2017.",
				"\u003ca href=\"/index/Br#bricks\"\u003eIndex entries for sequences related to bricks\u003c/a\u003e"
			],
			"xref": [
				"Cf. A031174, A031175."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Eric W. Weisstein_",
			"references": 20,
			"revision": 101,
			"time": "2020-02-25T21:26:27-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}