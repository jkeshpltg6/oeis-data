{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A327259",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 327259,
			"data": "1,2,2,3,8,3,4,10,10,4,5,16,13,16,5,6,18,20,20,18,6,7,24,23,32,23,24,7,8,26,30,36,36,30,26,8,9,32,33,48,41,48,33,32,9,10,34,40,52,54,54,52,40,34,10,11,40,43,64,59,72,59,64,43,40,11,12,42,50,68,72,78,78,72,68,50,42,12",
			"name": "Array T(n,k) = 2*n*k - A319929(n,k), n \u003e= 1, k \u003e= 1, read by antidiagonals.",
			"comment": [
				"Associative multiplication-like table whose values depend on whether n and k are odd or even.",
				"Associativity is proved by checking the formula with eight cases of three odd and even arguments. T(n,k) is distributive as long as partitioning an even number into two odd numbers is not allowed.",
				"T(n,k) has the same group structure as A319929, A322630 and A322744. For those arrays position (3,3) is 5, 7 and 11 respectively. T(3,3) = 13. If we didn't have the formula for these arrays, their entries could be computed knowing one position and applying the arithmetic rules."
			],
			"link": [
				"David Lovler, \u003ca href=\"/A327259/b327259.txt\"\u003eTable of n, a(n) for n = 1..465\u003c/a\u003e [restored by _Georg Fischer_, Oct 14 2019]"
			],
			"formula": [
				"T(n,k) = 2*n*k - n - k + 1, if n is odd and k is odd;",
				"T(n,k) = 2*n*k - n, if n is even and k is odd;",
				"T(n,k) = 2*n*k - k, if n is odd and k is even;",
				"T(n,k) = 2*n*k, if n is even and k is even.",
				"T(n,k) = 8*floor(n/2)*floor(k/2) + A319929(n,k)."
			],
			"example": [
				"Array T(n,k) begins:",
				"   1   2   3   4   5   6   7   8   9  10",
				"   2   8  10  16  18  24  26  32  34  40",
				"   3  10  13  20  23  30  33  40  43  50",
				"   4  16  20  32  36  48  52  64  68  80",
				"   5  18  23  36  41  54  59  72  77  90",
				"   6  24  30  48  54  72  78  96 102 120",
				"   7  26  33  52  59  78  85 104 111 130",
				"   8  32  40  64  72  96 104 128 136 160",
				"   9  34  43  68  77 102 111 136 145 170",
				"  10  40  50  80  90 120 130 160 170 200"
			],
			"mathematica": [
				"T[n_,k_]:=2n*k-If[Mod[n,2]==1,If[Mod[k,2]==1,n+k-1,k],If[Mod[k,2]==1,n,0]]; MatrixForm[Table[T[n,k],{n,1,10},{k,1,10}]] (* _Stefano Spezia_, Sep 05 2019 *)"
			],
			"program": [
				"(PARI) T(n,k) = 2*n*k - if (n%2, if (k%2, n+k-1, k), if (k%2, n, 0));",
				"matrix(8, 8, n, k, T(n,k)) \\\\ _Michel Marcus_, Sep 04 2019"
			],
			"xref": [
				"Equals A322744 + A322630 - A319929.",
				"Equals 4*A322630 - 3*A319929.",
				"Cf. A327260, A327261, A327263."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_David Lovler_, Aug 27 2019",
			"references": 9,
			"revision": 36,
			"time": "2021-03-11T02:52:40-05:00",
			"created": "2019-10-13T23:58:03-04:00"
		}
	]
}