{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A053610",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 53610,
			"data": "1,2,3,1,2,3,4,2,1,2,3,4,2,3,4,1,2,3,4,2,3,4,5,3,1,2,3,4,2,3,4,5,3,2,3,1,2,3,4,2,3,4,5,3,2,3,4,5,1,2,3,4,2,3,4,5,3,2,3,4,5,3,4,1,2,3,4,2,3,4,5,3,2,3,4,5,3,4,5,2,1,2,3,4,2,3,4,5,3,2,3,4,5,3,4,5,2,3,4,1,2,3,4",
			"name": "Number of positive squares needed to sum to n using the greedy algorithm.",
			"comment": [
				"Define f(n) = n - x2 where (x+1)^2 \u003e n \u003e= x^2. a(n) = number of iterations in f(...f(f(n))...) to reach 0.",
				"a(n) = 1 iff n is a perfect square.",
				"Also sum of digits when writing n in base where place values are squares, cf. A007961. [_Reinhard Zumkeller_, May 08 2011]",
				"The sequence could have started with a(0)=0. - _Thomas Ordowski_, Jul 12 2014",
				"The sequence is not bounded, see A006892. - _Thomas Ordowski_, Jul 13 2014"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A053610/b053610.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A007953(A007961(n)). - _Henry Bottomley_, Jun 01 2000",
				"a(n) = a(n-(int(sqrt(n)))^2)+1 = a(A053186(n))+1 [with a(0) = 0]. - _Henry Bottomley_, May 16 2000",
				"A053610 = A002828 + A062535. [_M. F. Hasler_, Dec 04 2008]"
			],
			"example": [
				"7=4+1+1+1, so 7 requires 4 squares using the greedy algorithm, so a(7)=4."
			],
			"maple": [
				"A053610 := proc(n)",
				"    local a,x;",
				"    a := 0 ;",
				"    x := n ;",
				"    while x \u003e 0 do",
				"        x := x-A048760(x) ;",
				"        a := a+1 ;",
				"    end do:",
				"    a ;",
				"end proc: # _R. J. Mathar_, May 13 2016"
			],
			"mathematica": [
				"f[n_] := (n - Floor[Sqrt[n]]^2); g[n_] := (m = n; c = 1; While[a = f[m]; a != 0, c++; m = a]; c); Table[ g[n], {n, 1, 105}]"
			],
			"program": [
				"(PARI) A053610(n,c=1)=while(n-=sqrtint(n)^2,c++);c \\\\ _M. F. Hasler_, Dec 04 2008",
				"(Haskell)",
				"a053610 n = s n $ reverse $ takeWhile (\u003c= n) $ tail a000290_list where",
				"  s _ []                 = 0",
				"  s m (x:xs) | x \u003e m     = s m xs",
				"             | otherwise = m' + s r xs where (m',r) = divMod m x",
				"-- _Reinhard Zumkeller_, May 08 2011"
			],
			"xref": [
				"Cf. A006892 (positions of records), A055401, A007961.",
				"Cf. A000196, A000290, A057945 (summing triangular numbers)."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Jud McCranie_, Mar 19 2000",
			"references": 26,
			"revision": 44,
			"time": "2016-05-14T12:16:57-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}