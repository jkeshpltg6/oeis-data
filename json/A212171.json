{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A212171",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 212171,
			"data": "1,1,2,1,1,1,1,3,2,1,1,1,2,1,1,1,1,1,1,4,1,2,1,1,2,1,1,1,1,1,1,3,1,2,1,1,3,2,1,1,1,1,1,1,5,1,1,1,1,1,1,2,2,1,1,1,1,1,3,1,1,1,1,1,1,2,1,2,1,1,1,1,4,1,2,2,1,1,1,2,1,1,3,1,1,1,3,1",
			"name": "Prime signature of n (nonincreasing version): row n of table lists positive exponents in canonical prime factorization of n, in nonincreasing order.",
			"comment": [
				"Length of row n equals A001221(n).",
				"The multiset of positive exponents in n's prime factorization completely determines a(n) for a host of OEIS sequences, including several \"core\" sequences.  Of those not cross-referenced here or in A212172, many can be found by searching the database for A025487.",
				"(Note: Differing opinions may exist about whether the prime signature of n should be defined as this multiset itself, or as a symbol or collection of symbols that identify or \"signify\" this multiset. The definition of this sequence is designed to be compatible with either view, as are the original comments. When n \u003e= 2, the customary ways to signify the multiset of exponents in n's prime factorization are to list the constituent exponents in either nonincreasing or nondecreasing order; this table gives the nonincreasing version.)",
				"Table lists exponents in the order in which they appear in the prime factorization of a member of A025487.  This ordering is common in database comments (e.g., A008966).",
				"Each possible multiset of an integer's positive prime factorization exponents corresponds to a unique partition that contains the same elements (cf. A000041). This includes the multiset of 1's positive exponents, { } (the empty multiset), which corresponds to the partition of 0."
			],
			"link": [
				"Jason Kimberley, \u003ca href=\"/A212171/b212171.txt\"\u003eTable of i, a(i) for i = 2..24301 (n = 2..10000)\u003c/a\u003e"
			],
			"formula": [
				"Row n of A118914, reversed.",
				"Row n of A124010 for n \u003e 1, with exponents sorted in nonincreasing order. Equivalently, row A046523(n) of A124010 for n \u003e 1."
			],
			"example": [
				"First rows of table read: 1; 1; 2; 1; 1,1; 1; 3; 2; 1,1; 1; 2,1;...",
				"The multiset of positive exponents in the prime factorization of 6 = 2*3 is {1,1} (1s are often left implicit as exponents). The prime signature of 6 is therefore {1,1}.",
				"12 = 2^2*3 has positive exponents 2 and 1 in its prime factorization, as does 18 = 2*3^2. Rows 12 and 18 of the table both read {2,1}."
			],
			"program": [
				"(MAGMA) \u0026cat[Reverse(Sort([pe[2]:pe in Factorisation(n)])):n in[1..76]]; // _Jason Kimberley_, Jun 13 2012"
			],
			"xref": [
				"Cf. A025487, A001221 (row lengths), A001222 (row sums). A118914 gives the nondecreasing version. A124010 lists exponents in n's prime factorization in natural order, with A124010(1) = 0.",
				"A212172 cross-references over 20 sequences that depend solely on n's prime exponents \u003e= 2, including the \"core\" sequence A000688.  Other sequences determined by the exponents in the prime factorization of n include:",
				"Multiplicative: A000005, A007425, A008683, A008836, A034444, A037445, A181819.",
				"Additive: A001221, A001222, A056169.",
				"Other: A001055, A008480, A010553, A038548, A050320, A051707, A071625, A074206, A076078, A085082, A088873.",
				"A highly incomplete selection of sequences, each definable by the set of prime signatures possessed by its members: A000040, A000290, A000578, A000583, A000961, A001248, A001358, A001597, A001694, A002808, A004709, A005117, A006881, A013929, A030059, A030229, A052486."
			],
			"keyword": "nonn,easy,tabf",
			"offset": "2,3",
			"author": "_Matthew Vandermast_, Jun 03 2012",
			"references": 14,
			"revision": 19,
			"time": "2012-06-16T15:12:27-04:00",
			"created": "2012-06-16T15:12:27-04:00"
		}
	]
}