{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A324592",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 324592,
			"data": "1,1,1,1,2,1,1,3,3,1,1,4,4,4,1,1,5,9,9,5,1,1,6,11,16,11,6,1,1,7,12,25,25,12,7,1,1,8,17,36,8,36,17,8,1,1,9,27,49,55,55,49,27,9,1,1,10,16,64,31,72,31,64,16,10,1,1,11,33,81,125,119,119,125",
			"name": "Square array T(n, k) read by diagonals, n \u003e 0, k \u003e 0; for any number m \u003e 0 with prime factorization Product_{i \u003e 0} prime(i)^e(i), let f(m) = Sum_{i \u003e 0) e(i) * sqrt(A005117(i)); f establishes a bijection between the positive numbers and the finite sums of square roots of squarefree numbers; let g be the inverse of f; T(n, k) = g(f(n) * f(k)).",
			"comment": [
				"The set of square roots of squarefree numbers, { sqrt(A005117(i)), i \u003e 0 }, is Q-linearly independent. The set of finite sums of square roots of squarefree numbers is closed under multiplication, hence the sequence is well defined.",
				"The function f can be naturally extended to the set of positive rational numbers: if r = u/v (not necessarily in reduced form), then f(r) = f(u) - f(v).",
				"This sequence has similarities with A297845."
			],
			"link": [
				"Eric Jaffe, \u003ca href=\"http://www.math.uchicago.edu/~may/VIGRE/VIGRE2007/REUPapers/FINALAPP/Jaffe.pdf\"\u003eLinearly Independent Integer Roots over the Scalar Field Q\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A324592/a324592.gp.txt\"\u003ePARI program for A324592\u003c/a\u003e"
			],
			"formula": [
				"For any m \u003e 0, n \u003e 0 and k \u003e 0:",
				"- T(n, k) = T(k, n) (T is commutative),",
				"- T(m, T(n, k)) = T(T(m, n), k) (T is associative),",
				"- T(m, n*k) = T(m, n) * T(m, k) and T(n*k, m) = T(n, m) * T(k, m) (T is completely multiplicative in both parameters),",
				"- T(n, 1) = 1 (1 is an absorbing element for T),",
				"- T(n, 2) = n (2 is an identity element for T),",
				"- T(n, 2^i) = n^i for any i \u003e= 0,",
				"- A001221(T(n, k)) \u003c= A001221(n) * A001221(k),",
				"- T(prime(n), prime(n)) = 2^A005117(n) (where prime(n) denotes the n-th prime number)."
			],
			"example": [
				"Array T(n, k) begins:",
				"  n\\k|  1   2   3    4    5    6    7     8     9    10",
				"  ---+-------------------------------------------------",
				"    1|  1   1   1    1    1    1    1     1     1     1",
				"    2|  1   2   3    4    5    6    7     8     9    10",
				"    3|  1   3   4    9   11   12   17    27    16    33",
				"    4|  1   4   9   16   25   36   49    64    81   100",
				"    5|  1   5  11   25    8   55   31   125   121    40",
				"    6|  1   6  12   36   55   72  119   216   144   330",
				"    7|  1   7  17   49   31  119   32   343   289   217",
				"    8|  1   8  27   64  125  216  343   512   729  1000",
				"    9|  1   9  16   81  121  144  289   729   256  1089",
				"   10|  1  10  33  100   40  330  217  1000  1089   400",
				"For n = 3 and k = 5:",
				"- f(3) = f(prime(2)) = sqrt(A005117(2)) = sqrt(2),",
				"- f(5) = f(prime(3)) = sqrt(A005117(3)) = sqrt(3),",
				"- f(3) * f(5) = sqrt(6) = sqrt(A005117(5)),",
				"- hence T(3, 5) = prime(5) = 11."
			],
			"program": [
				"(PARI) See Links section."
			],
			"xref": [
				"Cf. A001221, A005117, A297845."
			],
			"keyword": "nonn,tabl",
			"offset": "1,5",
			"author": "_Rémy Sigrist_, Sep 03 2019",
			"references": 1,
			"revision": 12,
			"time": "2019-09-10T19:58:22-04:00",
			"created": "2019-09-10T19:58:22-04:00"
		}
	]
}