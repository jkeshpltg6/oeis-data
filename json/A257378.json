{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A257378",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 257378,
			"data": "1,5,3,3,13,3,3,9,5,13,9,3,3,5,9,7,3,3,3,5,3,7,19,5,5,33,3,7,7,9,5,15,3,21,15,7,35,89,25,15,25,49,53,45,13,15,21,31,27,3,9,33,37,23,41,41,19,9,111,7,3,89,13,39,31,17,11,101,17,37,7,51,75",
			"name": "Smallest odd number k such that k*n*2^n+1 is a prime number.",
			"comment": [
				"Conjecture: a(n) exists for every n.",
				"The conjecture is a corollary of Dirichlet's theorem on primes in arithmetic progressions. - _Robert Israel_, Jan 05 2016",
				"As N increases sum {k, n=1 to N} / sum {n, n=1 to N} tends to 0.818.",
				"If k=1 then n*2^n+1 is a Cullen prime.",
				"Generalized Cullen primes have the form n*b^n+1, I propose to name the primes k*n*2^n-1 generalized Cullen primes of the second type."
			],
			"link": [
				"Pierre CAMI, \u003ca href=\"/A257378/b257378.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"1*1*2^1+1=3 prime so a(1)=1.",
				"1*2*2^2+1=9 composite, 3*2*2^2+1=25 composite, 5*2*2^2+1=41 prime so a(2)=5.",
				"1*3*2^3+1=25 composite, 3*3*2^3=73 prime so a(3)=3."
			],
			"maple": [
				"Q:= proc(m) local k;",
				"  for k from 1 by 2 do if isprime(k*m+1) then return k fi od",
				"end proc: seq(Q(n*2^n), n=1..100); # _Robert Israel_, Jan 05 2016"
			],
			"mathematica": [
				"Table[k = 1; While[!PrimeQ[k*n*2^n + 1], k += 2]; k, {n, 73}] (* _Michael De Vlieger_, Apr 21 2015 *)"
			],
			"program": [
				"(PFGW \u0026 SCRIPT)",
				"SCRIPT",
				"DIM n,0",
				"DIM k",
				"DIMS t",
				"OPENFILEOUT myf,a(n).txt",
				"LABEL loop1",
				"SET n,n+1",
				"IF n\u003e3000 THEN END",
				"SET k,-1",
				"LABEL loop2",
				"SET k,k+2",
				"SETS t,%d,%d\\,;n;k",
				"PRP k*n*2^n+1,t",
				"IF ISPRP THEN GOTO a",
				"GOTO loop2",
				"LABEL a",
				"WRITE myf,t",
				"GOTO loop1",
				"(PARI) a(n) = k=1; while(!isprime(k*n*2^n+1), k+=2); k \\\\ _Colin Barker_, Apr 21 2015",
				"(PFGW) ABC2 $b*$a*2^$a+1 // {number_primes,$b,1}",
				"a: from 1 to 10000",
				"b: from 1 to 100000 step 2",
				"_Charles R Greathouse IV_, Apr 24 2015"
			],
			"xref": [
				"Cf. A256787, A257379, A266909."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Pierre CAMI_, Apr 21 2015",
			"references": 4,
			"revision": 27,
			"time": "2016-01-06T15:52:56-05:00",
			"created": "2015-06-03T01:55:50-04:00"
		}
	]
}