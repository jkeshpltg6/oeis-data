{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A076336",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 76336,
			"data": "78557,271129,271577,322523,327739,482719,575041,603713,903983,934909,965431,1259779,1290677,1518781,1624097,1639459,1777613,2131043,2131099,2191531,2510177,2541601,2576089,2931767,2931991,3083723,3098059,3555593,3608251",
			"name": "(Provable) Sierpiński numbers: odd numbers n such that for all k \u003e= 1 the numbers n*2^k + 1 are composite.",
			"comment": [
				"It is only a conjecture that this sequence is complete up to 3000000 - there may be missing terms.",
				"It is conjectured that 78557 is the smallest Sierpiński number. - _T. D. Noe_, Oct 31 2003",
				"Sierpiński numbers are proved by exhibiting a periodic sequence p of prime divisors with p(k) | n*2^k+1 and disproved by finding prime n*2^k+1. It is conjectured that numbers that cannot be proved Sierpiński in this way are non-Sierpiński. However, some numbers resist both proof and disproof. - _David W. Wilson_, Jan 17 2005",
				"Sierpiński showed that this sequence is infinite.",
				"There are 4 related sequences that arise in this context:",
				"S1: Numbers n such that n*2^k + 1 is composite for all k (this sequence)",
				"S2: Odd numbers n such that 2^k + n is composite for all k (apparently it is conjectured that S1 and S2 are the same sequence)",
				"S3: Numbers n such that n*2^k + 1 is prime for all k (empty)",
				"S4: Numbers n such that 2^k + n is prime for all k (empty)",
				"The following argument, due to Michael Reid, attempts to show that S3 and S4 are empty: If p is a prime divisor of n + 1, then for k = p - 1, the term (either n*2^k + 1 or 2^k + n) is a multiple of p (and also \u003e p, so not prime). [However, David McAfferty points that for the case S3, this argument fails if p is of the form 2^m-1. So it may only be a conjecture that the set S3 is empty. - _N. J. A. Sloane_, Jun 27 2021]",
				"a(1) = 78557 is also the smallest odd n for which either n^p*2^k + 1 or n^p + 2^k is composite for every k \u003e 0 and every prime p greater than 3. - _Arkadiusz Wesolowski_, Oct 12 2015",
				"n = 4008735125781478102999926000625 = (A213353(1))^4 is in this sequence but is thought not to satisfy the conjecture mentioned by _David W. Wilson_ above. For this multiplier, all n*2^(4m + 2) + 1 are composite by an Aurifeuillean factorization. Only the remaining cases, n*2^k + 1 where k is not 2 modulo 4, are covered by a finite set of primes (namely {3, 17, 97, 241, 257, 673}). See Izotov link for details (although with another prime set). - _Jeppe Stig Nielsen_, Apr 14 2018"
			],
			"reference": [
				"C. A. Pickover, The Math Book, Sterling, NY, 2009; see p. 420.",
				"P. Ribenboim, The Book of Prime Number Records, 2nd. ed., 1989, p. 282."
			],
			"link": [
				"T. D. Noe and Arkadiusz Wesolowski, \u003ca href=\"/A076336/b076336.txt\"\u003eTable of n, a(n) for n = 1..15000\u003c/a\u003e (T. D. Noe supplied 13394 terms which came from McLean. a(1064), a(7053), and a(13397)-a(15000) from Arkadiusz Wesolowski.)",
				"Chris Caldwell, \u003ca href=\"http://primes.utm.edu/glossary/page.php?sort=RieselNumber\"\u003eRiesel number\u003c/a\u003e",
				"Chris Caldwell, \u003ca href=\"http://primes.utm.edu/glossary/page.php?sort=SierpinskiNumber\"\u003eSierpinski number\u003c/a\u003e",
				"Yves Gallot, \u003ca href=\"http://yves.gallot.pagesperso-orange.fr/papers/smallbrier.pdf\"\u003eA search for some small Brier numbers\u003c/a\u003e, 2000.",
				"Dan Ismailescu and Peter Seho Park, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL16/Ismailescu/ismailescu3.html\"\u003eOn Pairwise Intersections of the Fibonacci, Sierpiński, and Riesel Sequences\u003c/a\u003e, Journal of Integer Sequences, 16 (2013), #13.9.8.",
				"Anatoly S. Izotov, \u003ca href=\"https://www.fq.math.ca/Scanned/33-3/izotov.pdf\"\u003eA Note on Sierpinski Numbers\u003c/a\u003e, Fibonacci Quarterly (1995), pp. 206-207.",
				"G. Jaeschke, \u003ca href=\"http://www.jstor.org/stable/2007382\"\u003eOn the Smallest k Such that All k*2^N + 1 are Composite\u003c/a\u003e, Mathematics of Computation, Vol. 40, No. 161 (Jan., 1983), pp. 381-384.",
				"J. McLean, \u003ca href=\"/A076336/a076336a.html\"\u003eSearching for large Sierpinski numbers\u003c/a\u003e [Cached copy]",
				"J. McLean, \u003ca href=\"/A076336/a076336b.html\"\u003eBrier Numbers\u003c/a\u003e [Cached copy]",
				"C. Rivera, \u003ca href=\"http://www.primepuzzles.net/problems/prob_029.htm\"\u003eBrier numbers\u003c/a\u003e",
				"Payam Samidoost, \u003ca href=\"http://sierpinski.insider.com/dual\"\u003eDual Sierpinski problem search page\u003c/a\u003e [Broken link?]",
				"Payam Samidoost, \u003ca href=\"/A076336/a076336c.html\"\u003eDual Sierpinski problem search page\u003c/a\u003e [Cached copy]",
				"Payam Samidoost, \u003ca href=\"http://sierpinski.insider.com/4787\"\u003e4847\u003c/a\u003e [Broken link?]",
				"Payam Samidoost, \u003ca href=\"/A076336/a076336d.html\"\u003e4847\u003c/a\u003e [Cached copy]",
				"W. Sierpiński, \u003ca href=\"http://dx.doi.org/10.5169/seals-20713\"\u003eSur un problème concernant les nombres k * 2^n + 1\u003c/a\u003e, Elem. Math., 15 (1960), pp. 73-74.",
				"Seventeen or Bust, \u003ca href=\"http://www.seventeenorbust.com/\"\u003eA Distributed Attack on the Sierpinski Problem\u003c/a\u003e",
				"Jeremiah T. Southwick, \u003ca href=\"https://scholarcommons.sc.edu/etd/5879/\"\u003eTwo Inquiries Related to the Digits of Prime Numbers\u003c/a\u003e, Ph. D. Dissertation, University of South Carolina (2020).",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/SierpinskiNumberoftheSecondKind.html\"\u003eSierpiński Number of the Second Kind\u003c/a\u003e"
			],
			"xref": [
				"Cf. A003261, A052333, A076335, A076337, A101036, A137715, A263169, A305473, A306151."
			],
			"keyword": "nonn,hard,nice",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_, Nov 07 2002",
			"references": 65,
			"revision": 96,
			"time": "2021-06-27T10:35:49-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}