{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A157103",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 157103,
			"data": "1,1,1,1,1,1,1,2,2,1,1,3,5,3,1,1,5,12,10,4,1,1,8,29,33,17,5,1,1,13,70,109,72,26,6,1,1,21,169,360,305,135,37,7,1,1,34,408,1189,1292,701,228,50,8,1,1,55,985,3927,5473,3640,1405,357,65,9,1",
			"name": "Array A(n, k) = Fibonacc(n+1, k), with A(n, 0) = A(n, n) = 1, read by antidiagonals.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A157103/b157103.txt\"\u003eAntidiagonals n = 0..50, flattened\u003c/a\u003e",
				"Michelle Rudolph-Lilith, \u003ca href=\"http://arxiv.org/abs/1508.07894\"\u003eOn the Product Representation of Number Sequences, with Application to the Fibonacci Family\u003c/a\u003e, arXiv preprint arXiv:1508.07894, 2015. See Table 3."
			],
			"formula": [
				"A(n, k) = Fibonacc(n+1, k), with A(n, 0) = A(n, n) = 1 (array).",
				"A(n, 1) = A000045(n+1).",
				"T(n, k) = k*T(n-1, k) + T(n-2, k) with T(n, 0) = T(n, n) = 1 (triangle).",
				"From _G. C. Greubel_, Jan 11 2022: (Start)",
				"T(n, k) = Fibonacci(n-k+1, k), with T(n, 0) = T(n, n) = 1.",
				"T(2*n, n) = A084845(n) for n \u003e= 1, with T(0, 0) = 1.",
				"T(2*n+1, n+1) = A084844(n). (End)"
			],
			"example": [
				"Array begins as:",
				"  1,  1,   1,    1,     1,     1,      1,      1 ... A000012;",
				"  1,  1,   2,    3,     4,     5,      6,      7 ... A000027;",
				"  1,  2,   5,   10,    17,    26,     37,     50 ... A002522;",
				"  1,  3,  12,   33,    72,   135,    228,    357 ...;",
				"  1,  5,  29,  109,   305,   701,   1405,   2549 ...;",
				"  1,  8,  70,  360,  1292,  3640,   8658,  18200 ...;",
				"  1, 13, 169, 1189,  5473, 18901,  53353, 129949 ...;",
				"  1, 21, 408, 3927, 23184, 98145, 328776, 927843 ...;",
				"First few rows of the triangle =",
				"  1;",
				"  1,   1;",
				"  1,   1,    1;",
				"  1,   2,    2,     1;",
				"  1,   3,    5,     3,     1;",
				"  1,   5,   12,    10,     4,     1;",
				"  1,   8,   29,    33,    17,     5,     1;",
				"  1,  13,   70,   109,    72,    26,     6,     1;",
				"  1,  21,  169,   360,   305,   135,    37,     7,    1;",
				"  1,  34,  408,  1189,  1292,   701,   228,    50,    8,   1;",
				"  1,  55,  985,  3927,  5473,  3640,  1405,   357,   65,   9,   1;",
				"  1,  89, 2378, 12970, 23184, 18901,  8658,  2549,  528,  82,  10,  1;",
				"  1, 144, 5741, 42837, 98209, 98145, 53353, 18200, 4289, 747, 101, 11, 1;",
				"Example: Column 3 = (1, 3, 10, 33, 109, 360,...) = A006190."
			],
			"mathematica": [
				"(* First program *)",
				"T[_, 0]=1; T[n_, n_]=1; T[_, _]=0;",
				"T[n_, k_] /; 0 \u003c= k \u003c= n := k T[n-1, k] + T[n-2, k];",
				"Table[T[n, k], {n, 0, 10}, {k, 0, n}]//Flatten (* _Jean-François Alcover_, Aug 07 2018 *)",
				"(* Second program *)",
				"T[n_, k_]:= If[k==0 || k==n, 1, Fibonacci[n-k+1, k]];",
				"Table[T[n, k], {n,0,15}, {k,0,n}]//Flatten (* _G. C. Greubel_, Jan 11 2022 *)"
			],
			"program": [
				"(MAGMA)",
				"A157103:= func\u003c n,k | k eq 0 or k eq n select 1 else Evaluate(DicksonSecond(n, -1), k) \u003e;",
				"[A157103(n-k, k): k in [0..n], n in [0..15]]; // _G. C. Greubel_, Jan 11 2022",
				"(Sage)",
				"def A157103(n,k): return 1 if (k==0 or k==n) else lucas_number1(n+1, k, -1)",
				"flatten([[A157103(n-k, k) for k in (0..n)] for n in (0..10)]) # _G. C. Greubel_, Jan 11 2022"
			],
			"xref": [
				"Cf. A000045, A000129, A001076, A005668, A006190, A041025, A052918, A054413, A084844, A084845, A099371."
			],
			"keyword": "nonn,tabl,changed",
			"offset": "0,8",
			"author": "_Gary W. Adamson_, Feb 22 2009",
			"ext": [
				"Edited by _G. C. Greubel_, Jan 11 2022"
			],
			"references": 3,
			"revision": 10,
			"time": "2022-01-11T22:00:33-05:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}