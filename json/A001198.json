{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001198",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1198,
			"id": "M4601 N1962",
			"data": "9,14,21,27,34,43,50,61,70,81,93",
			"name": "Zarankiewicz's problem k_3(n).",
			"comment": [
				"Guy denotes k_{a,b}(m,n) the least k such that any m X n matrix with k '1's and '0's elsewhere has an a X b submatrix with all '1's, and omits b (resp. n) when b = a (resp. n = m). With this notation, a(n) = k_3(n). Sierpiński (1951) found a(4..6), a(7) is due to Brzeziński and a(8) due to Čulík (1956). - _M. F. Hasler_, Sep 28 2021"
			],
			"reference": [
				"L. Comtet, Advanced Combinatorics, Reidel, 1974, p. 291.",
				"R. K. Guy, A problem of Zarankiewicz, in P. Erdős and G. Katona, editors, Theory of Graphs (Proceedings of the Colloquium, Tihany, Hungary), Academic Press, NY, 1968, pp. 119-150.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"R. K. Guy, \u003ca href=\"/A001197/a001197.pdf\"\u003eA problem of Zarankiewicz\u003c/a\u003e, Research Paper No. 12, Dept. of Math., Univ. Calgary, Jan. 1967. [Annotated and scanned copy, with permission]",
				"R. K. Guy, \u003ca href=\"http://dx.doi.org/10.1007/BFb0060112\"\u003eA many-facetted problem of Zarankiewicz\u003c/a\u003e, Lect. Notes Math. 110 (1969), 129-148."
			],
			"formula": [
				"a(n) = A350304(n) + 1 = n^2 - A347473(n) = n^2 - A350237(n) + 1. - _Andrew Howroyd_, Dec 26 2021"
			],
			"example": [
				"From _M. F. Hasler_, Sep 28 2021: (Start)",
				"For n = 3 it is clearly necessary and sufficient that there be 3 X 3 = 9 ones in the n X n matrix in order to have an all-ones 3 X 3 submatrix.",
				"For n = 4 there may be at most 2 zeros in the 4 X 4 matrix in order to be guaranteed to have a 3 X 3 submatrix with all '1's, whence a(4) = 16 - 2 = 14: If 3 zeros are placed on a diagonal, it is no more possible to find a 3 X 3 all-ones submatrix, but if there are at most 2 zeros, one always has such a submatrix, as one can see from the following two diagrams:",
				"                                       0 1 1 1        0 1 1 1      no 3 X 3",
				"     Here one can delete, e.g.,   -\u003e   1 0 1 1        1 0 1 1  \u003c-  all-ones",
				"     row 1 and column 2 to get         1 1 1 1        1 1 0 1      submatrix",
				"     an all-ones 3 X 3 matrix.         1 1 1 1        1 1 1 1        (End)"
			],
			"xref": [
				"Cf. A001197 (k_2(n)), A006613 (k_{2,3}(n)), ..., A006626 (k_4(n,n+1)).",
				"Cf. A339635, A347473, A350237, A350304."
			],
			"keyword": "nonn,more,hard",
			"offset": "3,1",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"a(11)-a(13) from _Andrew Howroyd_, Dec 26 2021"
			],
			"references": 8,
			"revision": 32,
			"time": "2021-12-26T14:09:36-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}