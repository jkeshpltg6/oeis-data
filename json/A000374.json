{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000374",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 374,
			"data": "1,1,2,1,2,2,3,1,3,2,2,2,2,3,5,1,3,3,2,2,6,2,3,2,3,2,4,3,2,5,7,1,5,3,6,3,2,2,5,2,3,6,4,2,8,3,3,2,5,3,8,2,2,4,5,3,5,2,2,5,2,7,13,1,7,5,2,3,6,6,3,3,9,2,8,2,6,5,3,2,5,3,2,6,12,4,5,2,9,8,10,3,14,3,5,2,3,5,8,3",
			"name": "Number of cycles (mod n) under doubling map.",
			"comment": [
				"Number of cycles of the function f(x) = 2x mod n. Number of irreducible factors in the factorization of the polynomial x^n-1 over the integers mod 2. - _T. D. Noe_, Apr 16 2003"
			],
			"reference": [
				"R. Lidl and H. Niederreiter, Finite Fields, Addison-Wesley, 1983, p. 65."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A000374/b000374.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Jarkko Peltomäki and Aleksi Saarela, \u003ca href=\"https://doi.org/10.1016/j.jcta.2020.105340\"\u003eStandard words and solutions of the word equation X_1^2 ... X_n^2 = (X_1 ... X_n)^2\u003c/a\u003e, Journal of Combinatorial Theory, Series A (2021) Vol. 178, 105340. See also \u003ca href=\"https://arxiv.org/abs/2004.14657\"\u003earXiv:2004.14657\u003c/a\u003e [cs.FL], 2020."
			],
			"formula": [
				"a(n) = Sum_{d|m} phi(d)/ord(2, d), where m is n with all factors of 2 removed. - _T. D. Noe_, Apr 19 2003",
				"a(n) = (1/ord(2,m))*Sum_{j = 0..ord(2,m)-1} gcd(2^j - 1, m), where m is n with all factors of 2 removed. - _Nihar Prakash Gargava_, Nov 12 2018"
			],
			"example": [
				"a(14) = 3 because (1) the function 2x mod 14 has the three cycles (0),(2,4,8),(6,12,10) and (2) the factorization of x^14-1 over integers mod 2 is (1+x)^2 (1+x+x^3)^2 (1+x^2+x^3)^2, which has three unique factors. Note that the length of the cycles is the same as the degree of the factors."
			],
			"mathematica": [
				"Table[Length[FactorList[x^n - 1, Modulus -\u003e 2]] - 1, {n, 100}]",
				"CountFactors[p_, n_] := Module[{sum=0, m=n, d, f, i}, While[Mod[m, p]==0, m/=p]; d=Divisors[m]; Do[f=d[[i]]; sum+=EulerPhi[f]/MultiplicativeOrder[p, f], {i, Length[d]}]; sum]; Table[CountFactors[2, n], {n, 100}]"
			],
			"program": [
				"(PARI) a(n)={sumdiv(n \u003e\u003e valuation(n,2), d, eulerphi(d)/znorder(Mod(2,d)))} \\\\ _Andrew Howroyd_, Nov 12 2018"
			],
			"xref": [
				"Cf. A000005, A023135-A023142.",
				"Cf. A081844 (number of irreducible factors of x^(2n+1) - 1 over GF(2)).",
				"Cf. A037226 (number of primitive irreducible factors of x^(2n+1) - 1 over integers mod 2)."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Shel Kaphan_",
			"references": 19,
			"revision": 33,
			"time": "2021-01-19T21:00:11-05:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}