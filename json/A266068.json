{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A266068",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 266068,
			"data": "1,100,10,1111001,100,11111110011,1000,111111111100111,10000,1111111111111001111,100000,11111111111111110011111,1000000,111111111111111111100111111,10000000,1111111111111111111111001111111,100000000,11111111111111111111111110011111111",
			"name": "Binary representation of the n-th iteration of the \"Rule 3\" elementary cellular automaton starting with a single ON (black) cell.",
			"comment": [
				"Rule 35 also generates this sequence."
			],
			"link": [
				"Robert Price, \u003ca href=\"/A266068/b266068.txt\"\u003eTable of n, a(n) for n = 0..999\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ElementaryCellularAutomaton.html\"\u003eElementary Cellular Automaton\u003c/a\u003e",
				"Stephen Wolfram, \u003ca href=\"http://wolframscience.com/\"\u003eA New Kind of Science\u003c/a\u003e, Wolfram Media, 2002; p. 55.",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_Elementary_Cellular_Automata\"\u003eIndex to Elementary Cellular Automata\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,10011,0,-110010,0,100000)."
			],
			"formula": [
				"G.f.: (1+100*x-10001*x^2+109901*x^3+10000*x^4-120000*x^5) / ((1-x)*(1+x)*(1-100*x)*(1+100*x)*(1-10*x^2)). - _Colin Barker_, Dec 21 2015",
				"a(n) = (10*100^n - 99*10^((n-1)/2) - 1)/9 for odd n; a(n) = 10^(n/2) for even n. - _Karl V. Keller, Jr._, Aug 26 2021"
			],
			"example": [
				"From _Michael De Vlieger_, Dec 21 2015: (Start)",
				"First 8 rows, replacing leading zeros with \".\", the row converted to its binary equivalent at right:",
				"              1                =               1",
				"            1 0 0              =             100",
				"          . . . 1 0            =              10",
				"        1 1 1 1 0 0 1          =         1111001",
				"      . . . . . . 1 0 0        =             100",
				"    1 1 1 1 1 1 1 0 0 1 1      =     11111110011",
				"  . . . . . . . . . 1 0 0 0    =            1000",
				"1 1 1 1 1 1 1 1 1 1 0 0 1 1 1  = 111111111100111",
				"(End)"
			],
			"mathematica": [
				"rule = 3; rows = 20; Table[FromDigits[Table[Take[CellularAutomaton[rule,{{1},0}, rows-1, {All,All}][[k]], {rows-k+1, rows+k-1}], {k,1,rows}][[k]]], {k,1,rows}]"
			],
			"program": [
				"(Python) print([(10*100**n - 99*10**((n-1)//2) - 1)//9 if n%2 else 10**(n//2) for n in range(30)]) # _Karl V. Keller, Jr._, Aug 26 2021"
			],
			"xref": [
				"Cf. A263428, A266069."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Robert Price_, Dec 20 2015",
			"references": 3,
			"revision": 35,
			"time": "2021-08-31T20:03:11-04:00",
			"created": "2015-12-24T11:26:05-05:00"
		}
	]
}