{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A247692",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 247692,
			"data": "15544,268040,1062708,27629107",
			"name": "Minimal absolute discriminants a(n) of complex quadratic fields with 3-class group of type (3,3), 3-principalization type E.6 (1122), and second 3-class group G of odd nilpotency class cl(G)=2(n+2)+1.",
			"comment": [
				"The 3-principalization type (transfer kernel type, TKT) E.6 (1122) is not a permutation and has a single fixed point.",
				"The nilpotency condition cl(G)=2n+5 for the second 3-class group is equivalent to a transfer target type, TTT (called IPAD by Boston, Bush and Hajir) of the shape [(3^{n+2},3^{n+3}),(3,3,3),(3,9)^2].",
				"The second 3-class group G is a vertex of depth 1 on the coclass tree with root SmallGroup(243,6) contained in the coclass graph G(3,2).",
				"All these fields possess a Hilbert 3-class field tower of exact length 3.",
				"A247692 is an extremely sparse subsequence of A242878 and it is exceedingly hard to compute a(n) for n\u003e0."
			],
			"link": [
				"N. Boston, M. R. Bush, F. Hajir, \u003ca href=\"http://arxiv.org/abs/1111.4679\"\u003eHeuristics for p-class towers of imaginary quadratic fields\u003c/a\u003e, Math. Ann. (2013), Preprint: arXiv:1111.4679v1 [math.NT], 2011.",
				"M. R. Bush and D. C. Mayer, \u003ca href=\"http://arxiv.org/abs/1312.0251\"\u003e3-class field towers of exact length 3\u003c/a\u003e, J. Number Theory (2014), Preprint: arXiv:1312.0251v1 [math.NT], 2013.",
				"D. C. Mayer, \u003ca href=\"https://arxiv.org/abs/1403.3899\"\u003eThe second p-class group of a number field\u003c/a\u003e, arXiv:1403.3899 [math.NT], 2014; Int. J. Number Theory 8 (2012), no. 2, 471-505.",
				"D. C. Mayer, \u003ca href=\"https://arxiv.org/abs/1403.3896\"\u003eTransfers of metabelian p-groups\u003c/a\u003e, arXiv:1403.3896 [math.GR], 2014; Monatsh. Math. 166 (3-4) (2012), 467-495.",
				"D. C. Mayer, \u003ca href=\"https://arxiv.org/abs/1403.3833\"\u003eThe distribution of second p-class groups on coclass graphs\u003c/a\u003e, arXiv:1403.3833 [math.NT], 2014; J. Théor. Nombres Bordeaux 25 (2) (2013), 401-456.",
				"D. C. Mayer, \u003ca href=\"http://arxiv.org/abs/1403.3839\"\u003ePrincipalization algorithm via class group structure\u003c/a\u003e, J. Théor. Nombres Bordeaux (2014), Preprint: arXiv:1403.3839v1 [math.NT], 2014.",
				"Daniel C. Mayer, \u003ca href=\"http://arxiv.org/abs/1504.00851\"\u003ePeriodic sequences of p-class tower groups\u003c/a\u003e, arXiv:1504.00851, 2015.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Artin_transfer_(group_theory)#Example\"\u003eArtin transfer (group theory), Table 2\u003c/a\u003e"
			],
			"example": [
				"For a(0)=15544, we have the ground state of TKT E.6 with TTT [(9,27),(3,3,3),(3,9)^2] and cl(G)=5.",
				"For a(1)=268040, we have the first excited state of TKT E.6 with TTT [(27,81),(3,3,3),(3,9)^2] and cl(G)=7.",
				"a(0) and a(1) are due to D. C. Mayer (2012).",
				"a(2) and a(3) are due to N. Boston, M. R. Bush and F. Hajir (2013)."
			],
			"xref": [
				"Cf. A242862, A242863, A242878 (supersequences), A247693, A247694, A247695, A247696, A247697 (disjoint sequences)."
			],
			"keyword": "hard,more,nonn",
			"offset": "0,1",
			"author": "_Daniel Constantin Mayer_, Sep 28 2014",
			"references": 6,
			"revision": 21,
			"time": "2018-12-08T03:02:30-05:00",
			"created": "2014-10-01T04:35:58-04:00"
		}
	]
}