{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A015305",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 15305,
			"data": "1,-21,903,-25585,875007,-27125217,882215391,-28005209505,899790907743,-28735427761313,920460637644639,-29439916001972385,942314556807454559,-30150270336284213409,964869381941043396447,-30874848551033891160225",
			"name": "Gaussian binomial coefficient [ n,5 ] for q = -2.",
			"reference": [
				"J. Goldman and G.-C. Rota, The number of subspaces of a vector space, pp. 75-83 of W. T. Tutte, editor, Recent Progress in Combinatorics. Academic Press, NY, 1969.",
				"I. P. Goulden and D. M. Jackson, Combinatorial Enumeration. Wiley, NY, 1983, p. 99.",
				"M. Sved, Gaussians and binomials, Ars. Combinatoria, 17A (1984), 325-351."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A015305/b015305.txt\"\u003eTable of n, a(n) for n = 5..200\u003c/a\u003e",
				"\u003ca href=\"/index/Ga#Gaussian_binomial_coefficients\"\u003eIndex entries related to Gaussian binomial coefficients\u003c/a\u003e.",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (-21,462,3080,-14784,-21504,32768)."
			],
			"formula": [
				"A015305(n) = T[n,5], where T is the triangular array A015109. - _M. F. Hasler_, Nov 04 2012",
				"G.f.: x^5/((1-x)*(1+2*x)*(1-4*x)*(1+8*x)*(1-16*x)*(1+32*x)). - _R. J. Mathar_, Aug 03 2016",
				"From _G. C. Greubel_, Sep 21 2019: (Start)",
				"a(n) = (1 -11*(-2)^(n-4) +55*(-2)^(2*n-7) -55*(-2)^(3*n-9) +11*(-2)^(4*n- 10) -(-2)^(5*n-10))/40095.",
				"E.g.f.: (11*exp(16*x) - 440 + 1024*exp(x) - 704*exp(-2*x) + 110*exp(-8*x) - exp(-32*x))/41057280. (End)"
			],
			"maple": [
				"seq((1 -11*(-2)^(n-4) +55*(-2)^(2*n-7) -55*(-2)^(3*n-9) +11*(-2)^(4*n- 10) -(-2)^(5*n-10))/40095, n=5..25); # _G. C. Greubel_, Sep 21 2019"
			],
			"mathematica": [
				"Table[QBinomial[n, 5, -2], {n, 5, 20}] (* _Vincenzo Librandi_, Oct 29 2012 *)"
			],
			"program": [
				"(Sage) [gaussian_binomial(n,5,-2) for n in range(5,21)] # _Zerinvary Lajos_, May 27 2009",
				"(PARI) a(n) = (1 -11*(-2)^(n-4) +55*(-2)^(2*n-7) -55*(-2)^(3*n-9) +11*(-2)^(4*n- 10) -(-2)^(5*n-10))/40095 \\\\ _G. C. Greubel_, Sep 21 2019",
				"(MAGMA) [(1 -11*(-2)^(n-4) +55*(-2)^(2*n-7) -55*(-2)^(3*n-9) +11*(-2)^(4*n-10) -(-2)^(5*n-10))/40095: n in [5..25]]; // _G. C. Greubel_, Sep 21 2019",
				"(GAP) List([5..25], n-\u003e (1 -11*(-2)^(n-4) +55*(-2)^(2*n-7) -55*(-2)^(3*n-9) +11*(-2)^(4*n- 10) -(-2)^(5*n-10))/40095 ); # _G. C. Greubel_, Sep 21 2019"
			],
			"xref": [
				"Diagonal k=5 of the triangular array A015109. See there for further references and programs. - _M. F. Hasler_, Nov 04 2012"
			],
			"keyword": "sign,easy",
			"offset": "5,2",
			"author": "_Olivier Gérard_, Dec 11 1999",
			"references": 5,
			"revision": 27,
			"time": "2019-12-07T12:18:18-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}