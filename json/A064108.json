{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A064108",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 64108,
			"data": "0,1,21,421,8421,168421,3368421,67368421,1347368421,26947368421,538947368421,10778947368421,215578947368421,4311578947368421,86231578947368421,1724631578947368421,34492631578947368421",
			"name": "a(n) = (20^n-1)/19.",
			"comment": [
				"Partial sums of powers of 20 (A009964), q-integers for q=20: diagonal k=1 in triangle A022184.",
				"Partial sums are in A014904  Also, the sequence is related to A014937 by A014937 n) = n*a(n)-sum_{i=0..n-1} a(i), for n\u003e0. - _Bruno Berselli_, Nov 06 2012",
				"For n \u003e= 1, a(n) is the total number of holes in a certain box fractal (start with 20 boxes, 1 hole) after n iterations. See illustration in links. - _Kival Ngaokrajang_, Jan 28 2015"
			],
			"link": [
				"M. F. Hasler, \u003ca href=\"/A064108/b064108.txt\"\u003eTable of n, a(n) for n = 0..100\u003c/a\u003e",
				"Kival Ngaokrajang, \u003ca href=\"/A064108/a064108.pdf\"\u003eIllustration of initial terms\u003c/a\u003e",
				"\u003ca href=\"/index/Par#partial\"\u003eIndex entries related to partial sums\u003c/a\u003e",
				"\u003ca href=\"/index/Q#q-numbers\"\u003eIndex entries related to q-numbers\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (21,-20)."
			],
			"formula": [
				"a(n) = 20*a(n-1) + 1, with a(0)=0. - _Vincenzo Librandi_, Aug 07 2010",
				"a(0)=0, a(1)=1, a(n) = 21*a(n-1) - 20*a(n-2). - _Harvey P. Dale_, Oct 04 2012",
				"a(n) = floor(20^n/19). - _M. F. Hasler_, Nov 04 2012",
				"G.f.: x/((1-x)*(1-20*x)). - _Bruno Berselli_, Nov 06 2012"
			],
			"example": [
				"From _N. J. A. Sloane_, Nov 04 2014: Can also be obtained by writing powers of 2 in a staggered array and adding them (cf. A249604). For example, a(9) is:",
				"..........1",
				".........2",
				"........4",
				".......8",
				".....16",
				"....32",
				"...64",
				".128",
				"256",
				"-----------",
				"26947368421"
			],
			"maple": [
				"a:=n-\u003esum(20^(n-j), j=0..n): seq(a(n), n=0..15); # _Zerinvary Lajos_, Feb 11 2007"
			],
			"mathematica": [
				"(20^Range[20]-1)/19 (* or *) NestList[20#+1\u0026,1,20] (* _Harvey P. Dale_, Oct 04 2012 *)"
			],
			"program": [
				"(Sage) [gaussian_binomial(n,1,20) for n in range(1,17)] # _Zerinvary Lajos_, May 29 2009",
				"(PARI) for (n=0, 100, write(\"b064108.txt\", n, \" \", (20^n - 1)/19))  \\\\ _Harry J. Smith_, Sep 07 2009",
				"(PARI) A064108(n)=20^n\\19  \\\\ _M. F. Hasler_, Nov 04 2012",
				"(Maxima) A064108(n):=(20^n-1)/19$ makelist(A064108(n),n,1,30); /* _Martin Ettl_, Nov 05 2012 */"
			],
			"xref": [
				"Cf. A000225, A003462, A002450, A003463, A003464, A023000, A023001, A002452, A002275, A016123, A016125, A091030, A135519, A135518, A131865, A091045, A218722, A064108, A218724, ..., A218733, ..., A218743, ..., A218752, A094028.",
				"Cf. also A249604."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Jason Earls_, Sep 17 2001",
			"ext": [
				"Edited and extended to offset 0 by _M. F. Hasler_, Nov 04 2012"
			],
			"references": 39,
			"revision": 37,
			"time": "2019-12-07T12:18:22-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}