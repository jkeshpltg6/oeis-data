{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A185325",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 185325,
			"data": "1,0,0,0,0,1,1,1,1,1,2,2,3,3,4,5,6,7,9,10,13,15,18,21,26,30,36,42,50,58,70,80,95,110,129,150,176,202,236,272,317,364,423,484,560,643,740,847,975,1112,1277,1456,1666,1897,2168,2464,2809,3189,3627,4112,4673",
			"name": "Number of partitions of n into parts \u003e= 5.",
			"comment": [
				"a(n) is also the number of not necessarily connected 2-regular graphs on n-vertices with girth at least 5 (all such graphs are simple). The integer i corresponds to the i-cycle; addition of integers corresponds to disconnected union of cycles.",
				"By removing a single part of size 5, an A026798 partition of n becomes an A185325 partition of n - 5. Hence this sequence is essentially the same as A026798.",
				"a(n) = number of partitions of n+4 such that 4*(number of parts) is a part. - _Clark Kimberling_, Feb 27 2014"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A185325/b185325.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Jason Kimberley, \u003ca href=\"/wiki/User:Jason_Kimberley/E_k-reg_girth_ge_g_index\"\u003eIndex of sequences counting not necessarily connected k-regular simple graphs with girth at least g\u003c/a\u003e"
			],
			"formula": [
				"G.f.: Product_{m\u003e=5} 1/(1-x^m).",
				"Given by p(n) -p(n-1) -p(n-2) +2*p(n-5) -p(n-8) -p(n-9) +p(n-10), where p(n) = A000041(n). - _Shanzhen Gao_, Oct 28 2010 [sign of 10 corrected from + to -, and moved from A026798 to this sequence by _Jason Kimberley_].",
				"This sequence is the Euler transformation of A185115.",
				"a(n) ~ exp(Pi*sqrt(2*n/3)) * Pi^4 / (6*sqrt(3)*n^3). - _Vaclav Kotesovec_, Jun 02 2018",
				"G.f.: exp(Sum_{k\u003e=1} x^(5*k)/(k*(1 - x^k))). - _Ilya Gutkovskiy_, Aug 21 2018"
			],
			"maple": [
				"seq(coeff(series(1/mul(1-x^(m+5), m = 0..80), x, n+1), x, n), n = 0..70); # _G. C. Greubel_, Nov 03 2019"
			],
			"mathematica": [
				"Drop[Table[Count[IntegerPartitions[n], p_ /; MemberQ[p, 4*Length[p]]], {n, 40}], 3]  (* _Clark Kimberling_, Feb 27 2014 *)",
				"CoefficientList[Series[1/QPochhammer[x^5, x], {x, 0, 70}], x] (* _G. C. Greubel_, Nov 03 2019 *)"
			],
			"program": [
				"(MAGMA)",
				"p :=  func\u003c n | n lt 0 select 0 else NumberOfPartitions(n) \u003e;",
				"A185325 := func\u003cn | p(n)-p(n-1)-p(n-2)+2*p(n-5)-p(n-8)-p(n-9)+p(n-10)\u003e;",
				"[A185325(n):n in[0..60]];",
				"(MAGMA) R\u003cx\u003e:=PowerSeriesRing(Integers(), 70); Coefficients(R!( 1/(\u0026*[1-x^(m+5): m in [0..80]]) )); // _G. C. Greubel_, Nov 03 2019",
				"(PARI) my(x='x+O('x^70)); Vec(1/prod(m=0,80, 1-x^(m+5))) \\\\ _G. C. Greubel_, Nov 03 2019",
				"(Sage)",
				"def A185325_list(prec):",
				"    P.\u003cx\u003e = PowerSeriesRing(ZZ, prec)",
				"    return P( 1/product((1-x^(m+5)) for m in (0..80)) ).list()",
				"A185325_list(70) # _G. C. Greubel_, Nov 03 2019"
			],
			"xref": [
				"2-regular simple graphs with girth at least 5: A185115 (connected), A185225 (disconnected), this sequence (not necessarily connected).",
				"Not necessarily connected 2-regular graphs with girth at least g [partitions into parts \u003e= g]: A026807 (triangle); chosen g: A000041 (g=1 -- multigraphs with loops allowed), A002865 (g=2 -- multigraphs with loops forbidden), A008483 (g=3), A008484 (g=4), this sequence (g=5), A185326 (g=6), A185327 (g=7), A185328 (g=8), A185329 (g=9).",
				"Not necessarily connected 2-regular graphs with girth exactly g [partitions with smallest part g]: A026794 (triangle); chosen g: A002865 (g=2), A026796 (g=3), A026797 (g=4), A026798 (g=5), A026799 (g=6), A026800(g=7), A026801 (g=8), A026802 (g=9), A026803 (g=10).",
				"Not necessarily connected k-regular simple graphs with girth at least 5: A185315 (any k), A185305 (triangle); specified degree k: this sequence (k=2), A185335 (k=3).",
				"Cf. A008484, A008483."
			],
			"keyword": "nonn,easy",
			"offset": "0,11",
			"author": "_Jason Kimberley_, Nov 11 2011",
			"references": 23,
			"revision": 47,
			"time": "2019-11-04T09:21:31-05:00",
			"created": "2011-11-10T11:35:33-05:00"
		}
	]
}