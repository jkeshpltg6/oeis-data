{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A159797",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 159797,
			"data": "0,1,1,2,3,4,3,5,7,9,4,7,10,13,16,5,9,13,17,21,25,6,11,16,21,26,31,36,7,13,19,25,31,37,43,49,8,15,22,29,36,43,50,57,64,9,17,25,33,41,49,57,65,73,81,10,19,28,37,46,55,64,73,82,91,100,11,21,31,41,51,61,71,81,91,101",
			"name": "Triangle read by rows in which row n lists n+1 terms, starting with n, such that the difference between successive terms is equal to n-1.",
			"comment": [
				"Note that the last term of the n-th row is the n-th square A000290(n).",
				"See also A162611, A162614 and A162622.",
				"The triangle sums, see A180662 for their definitions, link the triangle A159797 with eleven sequences, see the crossrefs. - _Johannes W. Meijer_, May 20 2011",
				"T(n,k) is the number of distinct sums in the direct sum of {1, 2, ... n} with itself k times for 1 \u003c= k \u003c= n+1, e.g., T(5,3) = the number of distinct sums in the direct sum {1,2,3,4,5} + {1,2,3,4,5} + {1,2,3,4,5}. The sums range from 1+1+1=3 to 5+5+5=15. So there are 13 distinct sums. - _Derek Orr_, Nov 26 2014"
			],
			"link": [
				"Harvey P. Dale, \u003ca href=\"/A159797/b159797.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"David Applegate, Omar E. Pol and N. J. A. Sloane, \u003ca href=\"/A000695/a000695_1.pdf\"\u003eThe Toothpick Sequence and Other Sequences from Cellular Automata\u003c/a\u003e, Congressus Numerantium, Vol. 206 (2010), 157-191. [There is a typo in Theorem 6: (13) should read u(n) = 4.3^(wt(n-1)-1) for n \u003e= 2.]",
				"N. J. A. Sloane, \u003ca href=\"/wiki/Catalog_of_Toothpick_and_CA_Sequences_in_OEIS\"\u003eCatalog of Toothpick and Cellular Automata Sequences in the OEIS\u003c/a\u003e"
			],
			"formula": [
				"Given m = floor( (sqrt(8*n+1)-1)/2 ), then a(n) = m + (n - m*(m+1)/2)*(m-1). - _Carl R. White_, Jul 24 2010"
			],
			"example": [
				"Triangle begins:",
				"0;",
				"1, 1;",
				"2, 3, 4;",
				"3, 5, 7, 9;",
				"4, 7,10,13,16;",
				"5, 9,13,17,21,25;",
				"6,11,16,21,26,31,36;"
			],
			"maple": [
				"A159797:=proc(n) local m: m := floor( (sqrt(8*n+1)-1)/2 ): A159797(n):= m + (n - m*(m+1)/2)*(m-1) end: seq(A159797(n),n=0..75); # _Johannes W. Meijer_, May 20 2011"
			],
			"mathematica": [
				"Flatten[Table[NestList[#+n-1\u0026,n,n],{n,0,12}]] (* _Harvey P. Dale_, Aug 04 2014 *)"
			],
			"program": [
				"(GNU bc) scale=0;for(n=0;n\u003c76;n++){m=(sqrt(8*n+1)-1)/2;print m+(n-m*(m+1)/2)*(m-1),\",\"};print\"\\n\" /* _Carl R. White_, Jul 24 2010 */"
			],
			"xref": [
				"Cf. A000290, A001477, A081493, A159798, A162609, A162610, A162611, A162614, A162622.",
				"Cf.: A006002 (row sums). - _R. J. Mathar_, Jul 17 2009",
				"Cf. A163282, A163283, A163284, A163285. - _Omar E. Pol_, Nov 18 2009",
				"From _Johannes W. Meijer_, May 20 2011: (Start)",
				"Triangle sums (see the comments): A006002 (Row1), A050187 (Row2), A058187 (Related to Kn11, Kn12, Kn13, Fi1 and Ze1), A006918 (Related to Kn21, Kn22, Kn23, Fi2 and Ze2), A000330 (Kn3), A016061 (Kn4), A190717 (Related to Ca1 and Ze3), A144677 (Related to Ca2 and Ze4), A000292 (Related to Ca3, Ca4, Gi3 and Gi4) A190718 (Related to Gi1) and A144678 (Related to Gi2). (End)"
			],
			"keyword": "easy,nonn,tabl",
			"offset": "0,4",
			"author": "_Omar E. Pol_, Jul 09 2009",
			"ext": [
				"Edited by _Omar E. Pol_, Jul 18 2009",
				"More terms from _Omar E. Pol_, Nov 18 2009",
				"More terms from _Carl R. White_, Jul 24 2010"
			],
			"references": 33,
			"revision": 26,
			"time": "2021-02-24T02:48:18-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}