{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A097840",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 97840,
			"data": "1,84,6971,578509,48009276,3984191399,330639876841,27439125586404,2277116783794691,188973253929372949,15682502959354160076,1301458772372465913359,108005395603955316648721",
			"name": "Chebyshev polynomials S(n,83) + S(n-1,83) with Diophantine property.",
			"comment": [
				"(9*a(n))^2 - 85*b(n)^2 = -4 with b(n)=A097841(n) give all positive solutions of this Pell equation."
			],
			"link": [
				"Indranil Ghosh, \u003ca href=\"/A097840/b097840.txt\"\u003eTable of n, a(n) for n = 0..520\u003c/a\u003e",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"Giovanni Lucca, \u003ca href=\"http://forumgeom.fau.edu/FG2019volume19/FG201902index.html\"\u003eInteger Sequences and Circle Chains Inside a Hyperbola\u003c/a\u003e, Forum Geometricorum (2019) Vol. 19, 11-16.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (83, -1).",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e"
			],
			"formula": [
				"a(n) = S(n, 83) + S(n-1, 83) = S(2*n, sqrt(85)), with S(n, x) = U(n, x/2) Chebyshev's polynomials of the second kind, A049310. S(-1, x) = 0 = U(-1, x). S(n, 83) = A097839(n).",
				"a(n) = (-2/9)*i*((-1)^n)*T(2*n+1, 9*i/2) with the imaginary unit i and Chebyshev's polynomials of the first kind. See the T-triangle A053120.",
				"G.f.: (1+x)/(1 - 83*x + x^2).",
				"a(n) = 83*a(n-1) - a(n-2) for n \u003e 1; a(0)=1, a(1)=84. - _Philippe Deléham_, Nov 18 2008"
			],
			"example": [
				"All positive solutions of Pell equation x^2 - 85*y^2 = -4 are (9=9*1,1), (756=9*84,82), (62739=9*6971,6805), (5206581=9*578509,564733), ..."
			],
			"mathematica": [
				"CoefficientList[Series[(1+x)/(1-83x+x^2), {x, 0, 20}], x] (* _Michael De Vlieger_, Feb 08 2017 *)"
			],
			"program": [
				"(PARI) my(x='x+O('x^20)); Vec((1+x)/(1-83*x+x^2)) \\\\ _G. C. Greubel_, Jan 13 2019",
				"(MAGMA) m:=20; R\u003cx\u003e:=PowerSeriesRing(Integers(), m); Coefficients(R!( (1+x)/(1-83*x+x^2) )); // _G. C. Greubel_, Jan 13 2019",
				"(Sage) ((1+x)/(1-83*x+x^2)).series(x, 20).coefficients(x, sparse=False) # _G. C. Greubel_, Jan 13 2019",
				"(GAP) a:=[1,84];; for n in [3..20] do a[n]:=83*a[n-1]-a[n-2]; od; a; # _G. C. Greubel_, Jan 13 2019"
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Sep 10 2004",
			"references": 4,
			"revision": 38,
			"time": "2019-04-19T13:40:40-04:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}