{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A188999",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 188999,
			"data": "1,3,4,5,6,12,8,15,10,18,12,20,14,24,24,27,18,30,20,30,32,36,24,60,26,42,40,40,30,72,32,63,48,54,48,50,38,60,56,90,42,96,44,60,60,72,48,108,50,78,72,70,54,120,72,120,80,90,60,120,62,96,80,119,84,144,68,90,96,144,72,150,74,114,104,100",
			"name": "Bi-unitary sigma: sum of the bi-unitary divisors of n.",
			"comment": [
				"The sequence of bi-unitary perfect numbers obeying a(n) = 2*n consists of only 6, 60, 90 [Wall].",
				"Row sum of row n of the irregular table of the bi-unitary divisors, A222266."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A188999/b188999.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"K. Alladi, \u003ca href=\"http://dx.doi.org/10.1017/S1446788700017304\"\u003eOn arithmetic functions and divisors of higher order\u003c/a\u003e, J. Austral. Math. Soc. 23 (series A) (1977) 9-27.",
				"J. Sandor, B. Crstici, \u003ca href=\"http://dx.doi.org/10.1007/1-4020-2547-5_1\"\u003ePerfect numbers: Old and new issues; perspectives\u003c/a\u003e, in Handbook of number theory, II, p 45.",
				"L. Toth, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL12/Toth2/toth5.html\"\u003eOn the bi-unitary analogues of Euler's arithmetical function and the gcd-sum function\u003c/a\u003e J. Int. Seq. 12 (2009) # 09.5.2",
				"C. R. Wall, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9939-1972-0289403-9\"\u003eBi-unitary perfect numbers\u003c/a\u003e, Proc. Am. Math. Soc. 33 (1) (1972) 39-42.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/BiunitaryDivisor.html\"\u003eBiunitary Divisor\u003c/a\u003e",
				"Tomohiro Yamada, \u003ca href=\"https://arxiv.org/abs/1705.00189\"\u003e2 and 9 are the only biunitary superperfect numbers\u003c/a\u003e, arXiv:1705.00189 [math.NT], 2017."
			],
			"formula": [
				"Multiplicative with a(p^e) = (p^(e+1)-1)/(p-1) if e is odd, a(p^e) = (p^(e+1)-1)/(p-1) -p^(e/2) if e is even.",
				"a(n) = A000203(n) - A319072(n). - _Omar E. Pol_, Sep 29 2018"
			],
			"example": [
				"The divisors of n=16 are d=1, 2, 4, 8 and 16. The greatest common unitary divisor of (1,16) is 1, of (2,8) is 1, of (4,4) is 4, of (8,2) is 1, of (16,1) is 1 (see A165430). So 1, 2, 8 and 16 are bi-unitary divisors of 16, which sum to a(16) = 1 + 2 + 8 + 16 = 27."
			],
			"maple": [
				"A188999 := proc(n) local a,e,p,f; a :=1 ; for f in ifactors(n)[2] do e := op(2,f) ; p := op(1,f) ; if type(e,'odd') then a := a*(p^(e+1)-1)/(p-1) ; else a := a*((p^(e+1)-1)/(p-1)-p^(e/2)) ; end if; end do: a ; end proc:",
				"seq( A188999(n),n=1..80) ;"
			],
			"mathematica": [
				"f[n_] := Select[Divisors[n], Function[d, CoprimeQ[d, n/d]]]; Table[DivisorSum[n, # \u0026, Last@ Intersection[f@ #, f[n/#]] == 1 \u0026], {n, 76}] (* _Michael De Vlieger_, May 07 2017 *)",
				"a[n_] := If[n==1, 1, Product[{p, e} = pe; If[OddQ[e], (p^(e+1)-1)/(p-1), ((p^(e+1)-1)/(p-1)-p^(e/2))], {pe, FactorInteger[n]}]]; Array[a, 80] (* _Jean-François Alcover_, Sep 22 2018 *)"
			],
			"program": [
				"(Haskell)",
				"a188999 n = product $ zipWith f (a027748_row n) (a124010_row n) where",
				"   f p e = (p ^ (e + 1) - 1) `div` (p - 1) - (1 - m) * p ^ e' where",
				"           (e', m) = divMod e 2",
				"-- _Reinhard Zumkeller_, Mar 04 2013",
				"(PARI) udivs(n) = {my(d = divisors(n)); select(x-\u003e(gcd(x, n/x)==1), d); }",
				"gcud(n, m) = vecmax(setintersect(udivs(n), udivs(m)));",
				"biudivs(n) = select(x-\u003e(gcud(x, n/x)==1), divisors(n));",
				"a(n) = vecsum(biudivs(n)); \\\\ _Michel Marcus_, May 07 2017",
				"(PARI) a(n) = {f = factor(n); for (i=1, #f~, p = f[i,1]; e = f[i,2]; f[i,1] = if (e % 2, (p^(e+1)-1)/(p-1), (p^(e+1)-1)/(p-1) -p^(e/2)); f[i,2] = 1;); factorback(f);} \\\\ _Michel Marcus_, Nov 09 2017"
			],
			"xref": [
				"Cf. A222266, A027748, A124010, A034448, A319072."
			],
			"keyword": "mult,nonn",
			"offset": "1,2",
			"author": "_R. J. Mathar_, Apr 15 2011",
			"references": 59,
			"revision": 37,
			"time": "2018-10-16T15:20:05-04:00",
			"created": "2011-04-15T14:31:32-04:00"
		}
	]
}