{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001643",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1643,
			"id": "M2368 N0938",
			"data": "1,3,4,11,21,42,71,131,238,443,815,1502,2757,5071,9324,17155,31553,58038,106743,196331,361106,664183,1221623,2246918,4132721,7601259,13980892,25714875,47297029,86992802,160004703,294294531,541292030,995591267,1831177831",
			"name": "A Fielder sequence.",
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A001643/b001643.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Daniel C. Fielder, \u003ca href=\"https://www.fq.math.ca/Scanned/6-3/fielder.pdf\"\u003eSpecial integer sequences controlled by three parameters\u003c/a\u003e, Fibonacci Quarterly 6, 1968, 64-70.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"Y. Puri and T. Ward, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/index.html\"\u003eArithmetic and growth of periodic orbits\u003c/a\u003e, J. Integer Seqs., Vol. 4 (2001), #01.2.1.",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1, 1, 0, 1, 1, 1)."
			],
			"formula": [
				"G.f.: x*(1+2*x+4*x^3+5*x^4+6*x^5)/(1-x-x^2-x^4-x^5-x^6).",
				"From _Greg Dresden_, Jul 07 2021: (Start)",
				"a(3*n+1) = A001644(3*n+1).",
				"a(3*n+2) = A001644(3*n+2).",
				"a(3*n+3) = A001644(3*n+3) - 3*(-1)^n. (End)"
			],
			"maple": [
				"A001643:=-(1+2*z+4*z**3+5*z**4+6*z**5)/(z+1)/(z**3+z**2+z-1)/(z**2-z+1); [Conjectured by _Simon Plouffe_ in his 1992 dissertation.]"
			],
			"mathematica": [
				"LinearRecurrence[{1, 1, 0, 1, 1, 1}, {1, 3, 4, 11, 21, 42}, 50] (* _T. D. Noe_, Aug 09 2012 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c0,0,polcoeff(x*(1+2*x+4*x^3+5*x^4+6*x^5)/(1-x-x^2-x^4-x^5-x^6)+x*O(x^n),n))",
				"(MAGMA) I:=[1,3,4,11,21,42]; [n le 6 select I[n] else Self(n-1) + Self(n-2) + Self(n-4) + Self(n-5) + Self(n-6): n in [1..30]]; // _G. C. Greubel_, Jan 09 2018"
			],
			"xref": [
				"Cf. A001644."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"references": 2,
			"revision": 42,
			"time": "2021-07-07T11:25:08-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}