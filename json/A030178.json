{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A030178",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 30178,
			"data": "5,6,7,1,4,3,2,9,0,4,0,9,7,8,3,8,7,2,9,9,9,9,6,8,6,6,2,2,1,0,3,5,5,5,4,9,7,5,3,8,1,5,7,8,7,1,8,6,5,1,2,5,0,8,1,3,5,1,3,1,0,7,9,2,2,3,0,4,5,7,9,3,0,8,6,6,8,4,5,6,6,6,9,3,2,1,9,4,4,6,9,6,1,7,5,2,2,9,4,5,5,7,6,3,8",
			"name": "Decimal expansion of LambertW(1): the solution to x*exp(x) = 1.",
			"comment": [
				"Sometimes called the Omega constant.",
				"LambertW(n)/n, n=1,2,3,4,5,..., can be calculated with the same recurrence as for the numerators in Dirichlet series for logarithms of n using tetration. Convergence is slow for large numbers. See Mathematica program for recurrence. Tetration appears to work also for LambertW(n*(complex number))/n, n=1,2,3,4,5,... See link to mathematics stackexchange. (Conjecture.) - _Mats Granvik_, Oct 19 2013",
				"Infinite power tower for c = 1/E, i.e., c^c^c^..., where c = 1/A068985. - _Stanislav Sykora_, Nov 03 2013",
				"Notice the narrow interval exp(-gamma) \u003c w(1) \u003c gamma, with gamma = A001620. - _Jean-François Alcover_, Dec 18 2013",
				"Also the solution to x = -log(x). - _Robert G. Wilson v_, Feb 22 2014"
			],
			"link": [
				"G. C. Greubel and Stanislav Sykora, \u003ca href=\"/A030178/b030178.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e (terms 0..1999 from Stanislav Sykora)",
				"blackpenredpen, \u003ca href=\"https://www.youtube.com/watch?v=EjUp_5X6io4\"\u003eFinding Omega, featuring Newton's method\u003c/a\u003e, video (2018).",
				"Daniel Cummerow, \u003ca href=\"https://web.archive.org/web/20091027122939/http://www.geocities.com/Vienna/9349/constants.html\"\u003eSound of Mathematics\u003c/a\u003e, Constants.",
				"Mats Granvik, \u003ca href=\"http://math.stackexchange.com/questions/531276/lambertwk-k-by-tetration-for-natural-numbers\"\u003eLambertW(k)/k by tetration for natural numbers\u003c/a\u003e.",
				"István Mező, \u003ca href=\"https://arxiv.org/abs/2012.02480\"\u003eAn integral representation for the Lambert W function\u003c/a\u003e, arXiv:2012.02480 [math.CA], 2020.",
				"Simon Plouffe, \u003ca href=\"http://www.plouffe.fr/simon/constants/omega.txt\"\u003eLambert W(1, 0)\u003c/a\u003e.",
				"Simon Plouffe, \u003ca href=\"http://www.worldwideschool.org/library/books/sci/math/MiscellaneousMathematicalConstants/chap70.html\"\u003eThe omega constant or W(1)\u003c/a\u003e.",
				"Stanislav Sykora, \u003ca href=\"https://doi.org/10.3247/SL6Math16.002\"\u003eFixed points of the mappings exp(z) and -exp(z) in C\u003c/a\u003e, Stan's Library, Vol. VI, 2016.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/OmegaConstant.html\"\u003eOmega Constant\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/LambertW-Function.html\"\u003eLambert W-Function\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Omega_constant\"\u003eOmega constant\u003c/a\u003e.",
				"Wadim Zudilin, \u003ca href=\"https://arxiv.org/abs/2004.11029\"\u003eDiophantine problems related to the Omega constant\u003c/a\u003e, arXiv:2004.11029 [math.NT], 2020.",
				"\u003ca href=\"/index/Tra#transcendental\"\u003eIndex entries for transcendental numbers\u003c/a\u003e"
			],
			"formula": [
				"Equals 1/A030797.",
				"Equals (1/Pi) * Integral_{x=0..Pi} log(1 + sin(x)*exp(x*cot(x))/x) dx (Mező, 2020). - _Amiram Eldar_, Jul 04 2021"
			],
			"example": [
				"0.5671432904097838729999686622103555497538157871865125081351310792230457930866..."
			],
			"maple": [
				"evalf(LambertW(1));"
			],
			"mathematica": [
				"RealDigits[ ProductLog[1], 10, 111][[1]] (* _Robert G. Wilson v_, May 19 2004 *)"
			],
			"program": [
				"(PARI) solve(x=0,1,x*exp(x)-1) \\\\ _Charles R Greathouse IV_, Mar 20 2012",
				"(PARI) lambertw(1) \\\\ _Stanislav Sykora_, Nov 03 2013"
			],
			"xref": [
				"Cf. A019474, A059526, A059527, A238274.",
				"Cf. A276759 (another fixed point of -exp(z))."
			],
			"keyword": "nonn,cons",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_",
			"references": 43,
			"revision": 121,
			"time": "2021-07-04T03:22:33-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}