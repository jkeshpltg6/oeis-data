{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A301636",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 301636,
			"data": "0,0,1,1,1,0,1,2,1,1,0,2,4,2,4,1,1,4,2,4,9,4,2,4,1,1,5,4,4,5,5,2,0,2,5,1,1,5,8,5,1,1,5,2,0,0,2,8,10,4,2,4,5,1,1,1,1,5,10,8,5,5,9,4,2,4,4,2,4,9,5,5,8,10,9,5,5,9,9,5,5,9,4,2,4,10",
			"name": "Square array T(n, k) read by antidiagonals upwards, n \u003e= 0 and k \u003e= 0: T(n, k) = square of the distance from n + k*i to nearest square of a Gaussian integer (where i denotes the root of -1 with positive imaginary part).",
			"comment": [
				"The distance between two Gaussian integers is not necessarily integer, hence the use of the square of the distance.",
				"This sequence is a complex variant of A053188.",
				"See A301626 for the square array dealing with cubes of Gaussian integers."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A301636/a301636.png\"\u003eColored scatterplot for abs(x) \u003c= 500 and abs(y) \u003c= 500\u003c/a\u003e (where the hue is function of sqrt(T(abs(x), abs(y))))",
				"Rémy Sigrist, \u003ca href=\"/A301636/a301636_1.png\"\u003eVoronoi diagram of the squares of Gaussian integers for abs(x) \u003c= 500 and abs(y) \u003c= 500\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A301636/a301636_2.png\"\u003eScatterplot of (x, y) such that T(abs(x), abs(y)) is a square and abs(x) \u003c= 500 and abs(y) \u003c= 500\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A301636/a301636.gp.txt\"\u003ePARI program for A301636\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Gaussian_integer\"\u003eGaussian integer\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Voronoi_diagram\"\u003eVoronoi diagram\u003c/a\u003e",
				"\u003ca href=\"/index/Di#distance_to_the_nearest\"\u003eIndex entries for sequences related to distance to nearest element of some set\u003c/a\u003e"
			],
			"formula": [
				"T(n, 0) \u003c= A053188(n)^2.",
				"T(n, 0) = 0 iff n is a square (A000290).",
				"T(0, k) = 0 iff k is twice a square (A001105).",
				"T(n, k) = 0 iff n + k*i = z^2 for some Gaussian integer z."
			],
			"example": [
				"Square array begins:",
				"  n\\k|    0    1    2    3    4    5    6    7    8    9   10",
				"  ---+-------------------------------------------------------",
				"    0|    0    1    0    1    4    9    4    1    0    1    4",
				"    1|    0    1    1    2    4    5    5    2    1    2    5",
				"    2|    1    2    4    2    1    2    5    5    4    5    8",
				"    3|    1    2    4    1    0    1    4    9    9   10    8",
				"    4|    0    1    4    2    1    2    5   10   16   10    5",
				"    5|    1    2    5    5    4    5    8   10   13    9    4",
				"    6|    4    5    8   10    8    5    4    5    8   10    5",
				"    7|    4    5    8   10    5    2    1    2    5   10    8",
				"    8|    1    2    5    9    4    1    0    1    4    9   13",
				"    9|    0    1    4    9    5    2    1    2    5   10   17",
				"   10|    1    2    5   10    8    5    4    5    8   13   20"
			],
			"program": [
				"(PARI) See Links section."
			],
			"xref": [
				"Cf. A000290, A001105, A053188, A301626."
			],
			"keyword": "nonn,tabl",
			"offset": "0,8",
			"author": "_Rémy Sigrist_, Mar 25 2018",
			"references": 2,
			"revision": 11,
			"time": "2018-03-26T20:04:48-04:00",
			"created": "2018-03-26T20:04:48-04:00"
		}
	]
}