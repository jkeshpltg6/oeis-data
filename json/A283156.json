{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A283156",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 283156,
			"data": "0,1,2,2,1,1,2,2,1,2,2,1,1,1,1,2,1,1,1,3,2,2,2,1,2,0,2,1,1,1,2,3,1,1,1,1,3,3,1,1,1,1,1,0,2,2,1,0,1,2,1,2,4,2,2,1,2,1,1,0,1,0,1,1,2,1,3,2,1,3,1,1,0,2,2,2,3,2,1,1,0,1,2,1,2,1,1",
			"name": "Number of preimages of even integers under the sum-of-proper-divisors function.",
			"comment": [
				"Let sigma(n) denote the sum of divisors function, and s(n):=sigma(n)-n. The k-th element a(k) corresponds to the number of solutions to 2k=s(m) in positive integers, where m is a variable. In 2016, C. Pomerance proved that, for every e \u003e 0, the number of solutions is O_e((2k)^{2/3+e}).",
				"Note that for odd numbers n the problem of solving n=s(m) is quite different from the case when n is even. According to a slightly stronger version of Goldbach's conjecture, for every odd number n there exist primes p and q such that n = s(pq) = p + q + 1. This conjecture was verified computationally by Oliveira e Silva to 10^18. Thus the problem is (almost) equivalent to counting the solutions to n=p+q+1 in primes."
			],
			"link": [
				"Anton Mosunov, \u003ca href=\"/A283156/b283156.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"R. K. Guy, J. L. Selfridge, \u003ca href=\"https://doi.org/10.1090/S0025-5718-1975-0384669-X\"\u003eWhat drives an aliquot sequence?\u003c/a\u003e, Math. Comp. 29 (129), 1975, 101-107.",
				"P. Pollack, C. Pomerance, \u003ca href=\"https://doi.org/10.1090/btran/10\"\u003eSome problems of Erdos on the sum-of-divisors function\u003c/a\u003e, Trans. Amer. Math. Soc., Ser. B, 3 (2016), 1-26.",
				"C. Pomerance, \u003ca href=\"https://math.dartmouth.edu/~carlp/aliquot.pdf\"\u003eThe first function and its iterates\u003c/a\u003e, A Celebration of the Work of R. L. Graham, S. Butler, J. Cooper, and G. Hurlbert, eds., Cambridge U. Press, to appear.",
				"C. Pomerance, H.-S. Yang, \u003ca href=\"https://doi.org/10.1090/S0025-5718-2013-02775-5\"\u003eVariant of a theorem of Erdos on the sum-of-proper-divisors function\u003c/a\u003e, Math. Comp., 83 (2014), 1903-1913.T. Oliveira e Silva, \u003ca href=\"http://sweet.ua.pt/tos/goldbach.html\"\u003eGoldbach conjecture verification\u003c/a\u003e, 2015."
			],
			"formula": [
				"a(n) = A048138(2*n). - _Michel Marcus_, Mar 04 2017"
			],
			"example": [
				"a(1)=0, because 2*1=s(m) has no solutions;",
				"a(2)=1, because 2*2=s(9);",
				"a(3)=2, because 2*3=s(6)=s(25);",
				"a(4)=2, because 2*4=s(10)=s(49);",
				"a(5)=1, because 2*5=s(14)."
			],
			"program": [
				"(PARI) a(n) =  sum(k=1, (2*n-1)^2, (sigma(k) - k) == 2*n); \\\\ _Michel Marcus_, Mar 04 2017"
			],
			"xref": [
				"Cf. A048138, A283152."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Anton Mosunov_, Mar 01 2017",
			"references": 8,
			"revision": 21,
			"time": "2018-06-03T02:04:06-04:00",
			"created": "2017-03-04T18:55:21-05:00"
		}
	]
}