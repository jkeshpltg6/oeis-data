{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A335919",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 335919,
			"data": "1,1,2,1,4,6,8,6,20,16,4,40,56,32,1,68,152,144,64,94,376,480,352,128,114,844,1440,1376,832,256,116,1744,4056,4736,3712,1920,512,94,3340,10856,15248,14272,9600,4352,1024,60,5976,27672,47104,50784,40576,24064",
			"name": "Number T(n,k) of binary search trees of height k having n internal nodes; triangle T(n,k), n\u003e=0, max(0,floor(log_2(n))+1)\u003c=k\u003c=n, read by rows.",
			"comment": [
				"Empty external nodes are counted in determining the height of a search tree.",
				"T(n,k) is defined for n,k \u003e= 0. The triangle contains only the positive terms. Terms not shown are zero."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A335919/b335919.txt\"\u003eRows n = 0..200, flattened\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Binary_search_tree\"\u003eBinary search tree\u003c/a\u003e",
				"\u003ca href=\"/index/Ro#rooted\"\u003eIndex entries for sequences related to rooted trees\u003c/a\u003e",
				"\u003ca href=\"/index/Tra#trees\"\u003eIndex entries for sequences related to trees\u003c/a\u003e"
			],
			"formula": [
				"Sum_{k=0..n} k * T(n,k) = A335921(n).",
				"Sum_{n=k..2^k-1} n * T(n,k) = A335922(k)."
			],
			"example": [
				"Triangle T(n,k) begins:",
				"  1;",
				"     1;",
				"        2;",
				"        1, 4;",
				"           6,   8;",
				"           6,  20,   16;",
				"           4,  40,   56,   32;",
				"           1,  68,  152,  144,   64;",
				"               94,  376,  480,  352,  128;",
				"              114,  844, 1440, 1376,  832,  256;",
				"              116, 1744, 4056, 4736, 3712, 1920, 512;",
				"  ..."
			],
			"maple": [
				"g:= n-\u003e `if`(n=0, 0, ilog2(n)+1):",
				"b:= proc(n, h) option remember; `if`(n=0, 1, `if`(n\u003c2^h,",
				"      add(b(j-1, h-1)*b(n-j, h-1), j=1..n), 0))",
				"    end:",
				"T:= (n, k)-\u003e b(n, k)-`if`(k\u003e0, b(n, k-1), 0):",
				"seq(seq(T(n, k), k=g(n)..n), n=0..12);"
			],
			"mathematica": [
				"g[n_] := If[n == 0, 0, Floor@Log[2, n]+1];",
				"b[n_, h_] := b[n, h] = If[n == 0, 1, If[n \u003c 2^h,",
				"     Sum[b[j - 1, h - 1]*b[n - j, h - 1], {j, 1, n}], 0]];",
				"T[n_, k_] := b[n, k] - If[k \u003e 0, b[n, k - 1], 0];",
				"Table[Table[T[n, k], {k, g[n], n}], {n, 0, 12}] // Flatten (* _Jean-François Alcover_, Feb 08 2021, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Row sums give A000108.",
				"Column sums give A001699.",
				"Main diagonal gives A011782.",
				"T(n+3,n+2) gives A014480.",
				"T(n,max(0,A000523(n)+1)) = A328349(n).",
				"Cf. A073345, A073429 (another version with 0's), A076615, A195581, A244108, A335920 (the same read by columns), A335921, A335922."
			],
			"keyword": "nonn,tabf",
			"offset": "0,3",
			"author": "_Alois P. Heinz_, Jun 29 2020",
			"references": 6,
			"revision": 26,
			"time": "2021-02-08T17:41:08-05:00",
			"created": "2020-06-29T17:52:45-04:00"
		}
	]
}