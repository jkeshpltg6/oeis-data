{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A306440",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 306440,
			"data": "0,1,1,1,1,1,2,0,1,1,1,1,2,0,1,1,1,0,2,0,2,1,1,1,1,0,1,0,1,1,3,0,1,1,0,1,4,0,0,1,1,1,1,0,2,0,1,0,2,0,1,1,1,1,2,0,2,0,1,0,3,0,0,1,0,1,2,0,1,1,1,0,2,0,1,1,0,0,2,0,2,1,1,1,2,0,1,0,1,1,3,0,1,0,0,1",
			"name": "Number of different ways of expressing 2*n as (p - 1)*(q - 1), where p \u003c q are primes.",
			"comment": [
				"If 2*n-1 is an odd prime then a(n) \u003e 0.",
				"If a(n) \u003e 0 then 2n+1 is in A323550.",
				"Given any distinct odd primes p_1, ..., p_m, Dickson's conjecture implies that there are infinitely many m such that m/(p_i-1) + 1 is prime for all i.  Thus the sequence should be unbounded. - _Robert Israel_, Mar 29 2019"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A306440/b306440.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e"
			],
			"formula": [
				"a(6k+1) = 0 for k \u003e 0 because 12k+2 can't be written as (p-1)(q-1) except for k = 0 with p = 2, q = 3: If q \u003e 3, then q-1 is congruent to 0 or 4 (mod 6), and no p = 2, p = 3 (=\u003e q-1 = 6k+1) or p \u003e 3 is possible. - _M. F. Hasler_, Feb 25 2019"
			],
			"example": [
				"a(2) = 1 because 2*2 = 4 can only be expressed as (p - 1)*(q - 1) with primes p = 2 and q = 5.",
				"a(6) = 2 because for 2*6 = 12, there are only two possible ordered pairs of distinct primes (p,q), (2,13) and (3,7), such that 12 = (p - 1)*(q - 1)."
			],
			"maple": [
				"f:= proc(n) local t;",
				"    nops(select(t -\u003e t^2\u003c2*n and isprime(t+1) and isprime(2*n/t+1), numtheory:-divisors(2*n)))",
				"end proc:",
				"map(f, [$0..200]); # _Robert Israel_, Mar 18 2019"
			],
			"mathematica": [
				"a[n_]:=Module[{k=0},Do[Do[If[2n==(Prime[i]-1)*(Prime[j]-1),k++],{i,1,j-1}],{j,2,PrimePi[2n]+1}];Return[k]];",
				"Table[a[j],{j,0,128}]"
			],
			"program": [
				"(PARI) A306440(n,d,c)={forprime(p=2,sqrtint(-(n\u003e0)+n*=2)+1,n%(p-1)==0 \u0026\u0026 isprime(n/(p-1)+1) \u0026\u0026 c++ \u0026\u0026 d \u0026\u0026 printf(\"%d-1=(%d-1)*(%d-1) [%d], \",n+1,p,n/(p-1)+1,c));c} \\\\ Give 1 as 2nd optional arg (d=debug) to get a list of all decompositions. - _M. F. Hasler_, Feb 25 2019",
				"(PARI) a(n) = if(n==0, return(0)); my(d=divisors(n\u003c\u003c1)); d+=vector(#d, i, 1); sum(i=1, #d\\2, isprime(d[i]) \u0026\u0026 isprime(d[#d-i+1])) \\\\ for finding lots of terms or a(n) for large n. \\\\ _David A. Corneth_, Mar 18 2019"
			],
			"xref": [
				"Cf. A323550."
			],
			"keyword": "nonn",
			"offset": "0,7",
			"author": "_Andres Cicuttin_, Feb 15 2019",
			"references": 3,
			"revision": 41,
			"time": "2019-03-29T21:06:32-04:00",
			"created": "2019-03-28T11:59:36-04:00"
		}
	]
}