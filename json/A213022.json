{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A213022",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 213022,
			"data": "1,5,8,5,8,16,9,8,16,8,17,24,8,16,16,13,24,16,16,24,32,13,8,32,8,24,40,16,25,24,24,24,32,16,16,40,17,32,32,16,40,48,16,16,32,21,48,32,16,24,40,32,24,56,24,45,40,16,32,24,32,40,48,16,32,64,25,24",
			"name": "Expansion of phi(x)^2 * psi(x) in powers of x where phi(), psi() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"The body-centered cubic (b.c.c. also known as D3*) lattice is the set of all triples [a, b, c] where the entries are all integers or all one half an odd integer."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A213022/b213022.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(-1/8) * eta(q^2)^12 / (eta(q)^5 * eta(q^4)^4) in powers of q.",
				"Expansion of q^(-1/16) times theta series of b.c.c. lattice with respect to point [0, 0, 1/4] in powers of q^(1/2).",
				"Euler transform of period 4 sequence [ 5, -7, 5, -3, ...].",
				"6 * a(n) = A005875(8*n + 1)."
			],
			"example": [
				"a(0) = 1 since the norm squared of point [0, 0, 0] with respect to [0, 0, 1/4] is 1/16 = 1/16 + 1/2*0.",
				"a(1) = 5 since the norm squared of points [-1/2, -1/2, -1/2], [-1/2, 1/2, -1/2], [0, 0, -1], [1/2, -1/2, -1/2], [1/2, 1/2, -1/2] with respect to [0, 0, 1/4] is 9/16 = 1/16 + 1/2*1.",
				"1 + 5*x + 8*x^2 + 5*x^3 + 8*x^4 + 16*x^5 + 9*x^6 + 8*x^7 + 16*x^8 + 8*x^9 + ...",
				"q + 5*q^9 + 8*q^17 + 5*q^25 + 8*q^33 + 16*q^41 + 9*q^49 + 8*q^57 + 16*q^65 + ..."
			],
			"mathematica": [
				"CoefficientList[QPochhammer[q^2]^12/(QPochhammer[q]^5*QPochhammer[q^4]^4) + O[q]^70, q] (* _Jean-François Alcover_, Nov 05 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = local(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A)^12 / (eta(x + A)^5 * eta(x^4 + A)^4), n))}"
			],
			"xref": [
				"Cf. A005875."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Michael Somos_, Jun 03 2012",
			"references": 8,
			"revision": 15,
			"time": "2021-03-12T22:24:46-05:00",
			"created": "2012-06-04T03:01:03-04:00"
		}
	]
}