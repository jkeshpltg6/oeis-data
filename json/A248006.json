{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A248006",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 248006,
			"data": "3,4,3,6,5,8,9,6,9,4,11,7,5,16,7,9,5,12,7,18,21,8,15,13,27,14,11,10,14,32,7,14,5,12,35,10,13,24,7,14,13,11,9,42,45,16,11,30,13,12,19,27,33,8,15,22,28,4,35,28,18,64,7,14,21,28,19,10",
			"name": "Least positive integer m such that m + n divides phi(m*n), where phi(.) is Euler's totient function.",
			"comment": [
				"Conjecture: For any n \u003e 2, a(n) exists and a(n) \u003c= n.",
				"See also A248007 and A248008 for similar conjectures. - _Zhi-Wei Sun_, Sep 29 2014",
				"The conjecture is true: One can show that 2*n divides phi(n^2) for all n \u003e 2. So, a(n) is at most n. - _Derek Orr_, Sep 29 2014",
				"a(n) \u003e= 3 for all n. - _Robert Israel_, Sep 29 2014"
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A248006/b248006.txt\"\u003eTable of n, a(n) for n = 3..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1409.5685\"\u003eA new theorem on the prime-counting function\u003c/a\u003e, arXiv:1409.5685, 2014."
			],
			"example": [
				"a(5) = 3 since 3 + 5 divides phi(3*5) = 8."
			],
			"maple": [
				"f:= proc(n)",
				"local m;",
				"for m from 3 do",
				"  if numtheory:-phi(m*n) mod (m+n) = 0 then return m fi",
				"od",
				"end proc;",
				"seq(f(n),n=3..100); # _Robert Israel_, Sep 29 2014"
			],
			"mathematica": [
				"Do[m=1;Label[aa];If[Mod[EulerPhi[m*n],m+n]==0,Print[n,\" \",m];Goto[bb]];m=m+1;Goto[aa];Label[bb];Continue,{n,3,70}]"
			],
			"program": [
				"(PARI)",
				"a(n)=m=1;while(eulerphi(m*n)%(m+n),m++);m",
				"vector(100,n,a(n+2)) \\\\ _Derek Orr_, Sep 29 2014"
			],
			"xref": [
				"Cf. A000010, A248004, A248007, A248008."
			],
			"keyword": "nonn",
			"offset": "3,1",
			"author": "_Zhi-Wei Sun_, Sep 29 2014",
			"references": 3,
			"revision": 12,
			"time": "2014-09-29T11:19:14-04:00",
			"created": "2014-09-29T11:19:14-04:00"
		}
	]
}