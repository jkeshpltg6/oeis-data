{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A255397",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 255397,
			"data": "1,1,4,18,92,528,3356,23344,175984,1426520,12352600,113645488,1105760224,11333738336,121957021744,1373618201360,16151326356192,197796234588800,2517603785738752,33242912468993312,454583512625280256,6427749935432143072,93847133530055987840",
			"name": "Number of multimin-partitions of normal multisets of weight n.",
			"comment": [
				"A multiset is normal if its entries span an initial interval of positive integers. A multimin-partition is any sequence of multisets whose minima are weakly increasing. In a suitable category (see example) multimin-partitions m=(m_1,...,m_k) are morphisms m : U(m_1,...,m_k) -\u003e {min(m_1),...,min(m_k)} where U denotes multiset union and min denotes minimum."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A255397/b255397.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"Gus Wiseman, \u003ca href=\"https://docs.google.com/document/d/1m0s6DGTBkDW9gvMuFmJHvy6oLGRAbQ7okAZcOPZawp0/pub\"\u003eComcategories and Multiorders\u003c/a\u003e"
			],
			"example": [
				"For a(3) = 18",
				"[[1][2][3]]:[123]-\u003e[123]",
				"[[1][23]]:[123]-\u003e[12]",
				"[[13][2]]:[123]-\u003e[12]",
				"[[12][3]]:[123]-\u003e[13]",
				"[[123]]:[123]-\u003e[1]",
				"[[1][2][2]]:[122]-\u003e[122]",
				"[[1][22]]:[122]-\u003e[12]",
				"[[12][2]]:[122]-\u003e[12]",
				"[[122]]:[122]-\u003e[1]",
				"[[1][1][2]]:[112]-\u003e[112]",
				"[[1][12]]:[112]-\u003e[11]",
				"[[12][1]]:[112]-\u003e[11]",
				"[[11][2]]:[112]-\u003e[12]",
				"[[112]]:[112]-\u003e[1]",
				"[[1][1][1]]:[111]-\u003e[111]",
				"[[1][11]]:[111]-\u003e[11]",
				"[[11][1]]:[111]-\u003e[11]",
				"[[111]]:[111]-\u003e[1]"
			],
			"mathematica": [
				"mmcount[m_List] := mmcount[m] = If[Length[m] === 0, 0, 1 + Plus @@ mmcount /@ Union[Subsets[Rest[m]]]];",
				"mmallnorm[n_Integer] := Function[s, Array[Count[s, y_ /; y \u003c= #] + 1 \u0026, n]] /@ Subsets[Range[n - 1] + 1];",
				"Array[Plus @@ mmcount /@ mmallnorm[#] \u0026, 13]"
			],
			"program": [
				"(PARI)",
				"R(n,k)=Vec(prod(j=1, k, 1/(1 - x/(1-x + O(x^n))^j)) + O(x*x^n))",
				"seq(n)={sum(k=0, n, R(n, k)*sum(r=k, n, binomial(r, k)*(-1)^(r-k)) )} \\\\ _Andrew Howroyd_, Feb 04 2021"
			],
			"xref": [
				"Cf. A262671."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Gus Wiseman_, Feb 22 2015",
			"ext": [
				"a(14)-a(15) from _Vaclav Kotesovec_, Feb 22 2015",
				"a(0)=1 prepended and terms a(16) and beyond from _Andrew Howroyd_, Feb 04 2021"
			],
			"references": 17,
			"revision": 14,
			"time": "2021-02-04T14:16:21-05:00",
			"created": "2015-03-11T04:43:34-04:00"
		}
	]
}