{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A158973",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 158973,
			"data": "1,2,3,4,4,6,5,7,6,7,6,11,7,9,9,10,8,12,9,13,11,11,10,17,11,12,12,14,11,18,12,16,14,14,14,20,13,15,15,20,14,20,15,19,20,17,16,25,17,20,18,20,17,23,19,24,19,19,18,29,19,21,24,24,21,25,20,24,22,27,21,32,22,24,26",
			"name": "a(n) = count of numbers k \u003c= n such that all proper divisors of k are divisors of n.",
			"comment": [
				"For primes p, a(p) = A036234(p) = A000720(p) + 1."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A158973/b158973.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A000005(n) + A004788(n-1). - _Vladeta Jovovic_, Apr 07 2009 (Corrected by _Altug Alkan_, Nov 25 2015)"
			],
			"example": [
				"For n = 8 we have the following proper divisors for k \u003c= n: {1}, {1}, {1}, {1, 2}, {1}, {1, 2, 3}, {1}, {1, 2, 4}. Only k = 6 has a proper divisor that is not a divisor of 8, viz. 3. Hence a(8) = 7."
			],
			"maple": [
				"N:= 1000: # to get a(1) to a(N)",
				"A:= Vector(N, numtheory:-tau):",
				"for p in select(isprime,[2,seq(i,i=3..N,2)]) do",
				"for d from 0 to floor(log[p](N))-1 do",
				"  R:= [seq(seq(p^d*(i+p*j), j=1..floor((N/p^d - i)/p)), i=1..p-1)];",
				"  A[R]:= map(`+`,A[R],1);",
				"od",
				"od:",
				"convert(A,list); # _Robert Israel_, Nov 24 2015"
			],
			"mathematica": [
				"f[n_] := Block[{d = Most@ Divisors@ n}, Select[Range@ n, Union[Most@ Divisors@ #, d] == d \u0026]]; Array[Length@ f@ # \u0026, {75}] (* _Michael De Vlieger_, Nov 25 2015 *)"
			],
			"program": [
				"(MAGMA) [ #[ k: k in [1..n] | forall(t){ d: d in Divisors(k) | d eq k or d in Divisors(n) } ]: n in [1..75] ];",
				"(PARI) a004788(n) = {sfp = Set(); for (k=1, n-1, sfp = setunion(sfp, Set(factor(binomial(n, k))[, 1]))); return (length(sfp));}",
				"a(n) = numdiv(n) + a004788(n-1); \\\\ _Altug Alkan_, Nov 25 2015"
			],
			"xref": [
				"Cf. A000040, A000720, A036234."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Jaroslav Krizek_, Apr 01 2009",
			"ext": [
				"Edited and extended by _Klaus Brockhaus_, Apr 06 2009"
			],
			"references": 4,
			"revision": 18,
			"time": "2015-11-25T20:51:09-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}