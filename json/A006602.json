{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006602",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6602,
			"id": "M1532",
			"data": "2,1,2,5,20,180,16143,489996795",
			"name": "a(n) is the number of hierarchical models on n unlabeled factors or variables with linear terms forced.",
			"comment": [
				"Also number of pure (= irreducible) group-testing histories of n items - A. Boneh, Mar 31 2000",
				"Also number of antichain covers of an unlabeled n-set, so a(n) equals first differences of A003182. - _Vladeta Jovovic_, Goran Kilibarda, Aug 18 2000",
				"Also number of inequivalent (under permutation of variables) nondegenerate monotone Boolean functions of n variables. We say h and g (functions of n variables) are equivalent if there exists a permutation p of S_n such that hp=g. E.g., a(3)=5 because xyz, xy+xz+yz, x+yz+xyz, xy+xz+xyz, x+y+z+xy+xz+yz+xyz are 5 inequivalent nondegenerate monotone Boolean functions that generate (by permutation of variables) the other 4. For example, y+xz+xyz can be obtained from x+yz+xyz by exchanging x and y. - Alan Veliz-Cuba (alanavc(AT)vt.edu), Jun 16 2006",
				"The non-spanning/covering case is A003182. The labeled case is A006126. - _Gus Wiseman_, Feb 20 2019"
			],
			"reference": [
				"Y. M. M. Bishop, S. E. Fienberg and P. W. Holland, Discrete Multivariate Analysis. MIT Press, 1975, p. 34. [In part (e), the Hierarchy Principle for log-linear models is defined. It essentially says that if a higher-order parameter term is included in the log-linear model, then all the lower-order parameter terms should also be included. - _Petros Hadjicostas_, Apr 10 2020]",
				"V. Jovovic and G. Kilibarda, On enumeration of the class of all monotone Boolean functions, in preparation.",
				"A. A. Mcintosh, personal communication.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"V. Jovovic and G. Kilibarda, \u003ca href=\"http://mi.mathnet.ru/eng/dm/v11/i4/p127\"\u003eOn the number of Boolean functions in the Post classes F^{mu}_8\u003c/a\u003e, Diskretnaya Matematika, 11(4) (1999), 127-138 (\u003ca href=\"https://doi.org/10.1515/dma.1999.9.6.593\"\u003etranslated\u003c/a\u003e in Discrete Mathematics and Applications, 9(6) (1999), 593-605).",
				"C. Lienkaemper, \u003ca href=\"http://www.math.tamu.edu/REU/results/REU_2015/lienreport.pdf\"\u003eWhen do neural codes come from convex or good covers?\u003c/a\u003e, 2015.",
				"C. L. Mallows, \u003ca href=\"/A000372/a000372_5.pdf\"\u003eEmails to N. J. A. Sloane, Jun-Jul 1991\u003c/a\u003e",
				"Gus Wiseman, \u003ca href=\"/A048143/a048143_4.txt\"\u003eSequences enumerating clutters, antichains, hypertrees, and hyperforests, organized by labeling, spanning, and allowance of singletons\u003c/a\u003e."
			],
			"formula": [
				"a(n) = A007411(n) + 1.",
				"First differences of A003182. - _Gus Wiseman_, Feb 23 2019"
			],
			"example": [
				"From _Gus Wiseman_, Feb 20 2019: (Start)",
				"Non-isomorphic representatives of the a(0) = 2 through a(4) = 20 antichains:",
				"  {}    {{1}}  {{12}}    {{123}}         {{1234}}",
				"  {{}}         {{1}{2}}  {{1}{23}}       {{1}{234}}",
				"                         {{13}{23}}      {{12}{34}}",
				"                         {{1}{2}{3}}     {{14}{234}}",
				"                         {{12}{13}{23}}  {{1}{2}{34}}",
				"                                         {{134}{234}}",
				"                                         {{1}{24}{34}}",
				"                                         {{1}{2}{3}{4}}",
				"                                         {{13}{24}{34}}",
				"                                         {{14}{24}{34}}",
				"                                         {{13}{14}{234}}",
				"                                         {{12}{134}{234}}",
				"                                         {{1}{23}{24}{34}}",
				"                                         {{124}{134}{234}}",
				"                                         {{12}{13}{24}{34}}",
				"                                         {{14}{23}{24}{34}}",
				"                                         {{12}{13}{14}{234}}",
				"                                         {{123}{124}{134}{234}}",
				"                                         {{13}{14}{23}{24}{34}}",
				"                                         {{12}{13}{14}{23}{24}{34}}",
				"(End)"
			],
			"xref": [
				"Cf. A000372, A003182, A006126 (labeled case), A007411, A014466, A261005, A293993, A304997, A304998, A304999, A305001, A305855, A306505, A320449, A321679."
			],
			"keyword": "nonn,nice,hard",
			"offset": "0,1",
			"author": "_Colin Mallows_",
			"ext": [
				"a(6) from A. Boneh, 32 Hantkeh St., Haifa 34608, Israel, Mar 31 2000",
				"Entry revised by _N. J. A. Sloane_, Jul 23 2006",
				"a(7) from A007411 and A003182. - _N. J. A. Sloane_, Aug 13 2015",
				"Named edited by _Petros Hadjicostas_, Apr 08 2020"
			],
			"references": 27,
			"revision": 60,
			"time": "2020-04-11T04:13:38-04:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}