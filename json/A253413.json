{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A253413",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 253413,
			"data": "1,2,3,6,5,5,14,14,21,27,44,57,78,114,158,222,306,437,608,851,1193,1674,2346,3281,4605,6450,9039,12662,17748,24870,34844,48830,68423,95882,134349,188265,263810,369666,518001,725859,1017128,1425261,1997178,2798582",
			"name": "Number of n-bit legal circular binary words with maximal set of 1's.",
			"comment": [
				"An n-bit circular binary word is legal if every 1 has an adjacent 0.",
				"In other words, a(n) is the number of minimal dominating sets in the n-cycle graph C_n. - _Eric W. Weisstein_, Jul 24 2017"
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A253413/b253413.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"M. L. Gargano, A. Weisenseel, J. Malerba and M. Lewinter, \u003ca href=\"http://www.csis.pace.edu/~ctappert/srd2005/b3.pdf\"\u003eDiscrete Renyi parking constants\u003c/a\u003e, 36th Southeastern Conf. on Combinatorics, Graph Theory, and Computing, Boca Raton, 2005, Congr. Numer. 176 (2005) 43-48.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CycleGraph.html\"\u003eCycle Graph\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/MinimalDominatingSet.html\"\u003eMinimal Dominating Set\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,1,1,1,0,-1)."
			],
			"formula": [
				"a(n) = a(n-2) + a(n-3) + a(n-4) - a(n-6) for n\u003e7. - _Chai Wah Wu_, Jan 02 2015",
				"G.f.: x*(1 + 2*x + 2*x^2 + 3*x^3 - x^4 - 6*x^5 + x^6) / (1 - x^2 - x^3 - x^4 + x^6). - _Paul D. Hanna_, Jan 02 2015"
			],
			"example": [
				"The only legal circular words with maximal set of 1's are",
				"0 if n = 1; 01 \u0026 10 if n = 2; 011, 101 \u0026 110 if n = 3;",
				"0011, 0101, 0110, 1001, 1010 \u0026 1100 if n = 4;",
				"01011, 01101, 10101, 10110 \u0026 11010 if n = 5; and",
				"010101, 011011, 101010, 101101 \u0026 110110 if n = 6.",
				"From _Eric W. Weisstein_, Jul 24 2017 (Start)",
				"Minimal dominating sets of cycle graph C_n:",
				"C_1: {1}",
				"C_2: {{1}, {2}}",
				"C_3: {{1}, {2}, {3}}",
				"C_4: {{1, 2}, {1, 3}, {1, 4}, {2, 3}, {2, 4}, {3, 4}}",
				"C_5: {{1, 3}, {1, 4}, {2, 4}}, {2, 5}, {3, 5}}",
				"C_6: {{1, 4}, {2, 5}, {3, 6}, {1, 3, 5}, {2, 4, 6}} (End)"
			],
			"mathematica": [
				"Join[{1}, Table[RootSum[1 - #^2 - #^3 - #^4 + #^6 \u0026, #^n \u0026], {n, 2, 20}]]  (* _Eric W. Weisstein_, Jul 24 2017 *)",
				"Join[{1}, LinearRecurrence[{0, 1, 1, 1, 0, -1}, {0, 2, 3, 6, 5, 5}, {2, 20}]] (* _Eric W. Weisstein_, Jul 24 2017 *)",
				"CoefficientList[Series[(1 + 2 x + 2 x^2 + 3 x^3 - x^4 - 6 x^5 + x^6)/(1 - x^2 - x^3 - x^4 + x^6), {x, 0, 20}], x] (* _Eric W. Weisstein_, Jul 24 2017 *)"
			],
			"program": [
				"(Python)",
				"def A253413(n):",
				"....if n \u003e 1:",
				"........c, fs = 0, '0'+str(n)+'b'",
				"........for i in range(2**n):",
				"............s = format(i,fs)",
				"............s = s[-2:]+s+s[:2]",
				"............for j in range(n):",
				"................if s[j:j+4] == '0100' or s[j+1:j+5] == '0010' or s[j+1:j+4] == '000' or s[j+1:j+4] == '111':",
				"....................break",
				"............else:",
				"................c += 1",
				"........return c",
				"....else:",
				"........return 1 # _Chai Wah Wu_, Jan 02 2015",
				"(PARI) Vec(x*(1 + 2*x + 2*x^2 + 3*x^3 - x^4 - 6*x^5 + x^6) / (1 - x^2 - x^3 - x^4 + x^6) + O(x^100)) \\\\ _Colin Barker_, Jul 26 2017"
			],
			"xref": [
				"Asymmetric analog of A001608 (no consecutive 1s but maximal).",
				"Circular analog of A253412."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Steven Finch_, Dec 31 2014",
			"ext": [
				"a(21)-a(28) from _Chai Wah Wu_, Jan 02 2015",
				"More terms from _Colin Barker_, Jul 26 2017"
			],
			"references": 6,
			"revision": 60,
			"time": "2021-09-09T16:20:22-04:00",
			"created": "2015-01-01T18:49:14-05:00"
		}
	]
}