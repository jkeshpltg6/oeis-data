{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A061808",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 61808,
			"data": "1,3,5,7,9,11,13,15,17,19,315,115,75,135,319,31,33,35,37,39,533,559,135,517,539,51,53,55,57,59,793,315,195,335,759,71,73,75,77,79,1377,913,595,957,979,91,93,95,97,99,1111,515,315,535,1199,111,113,115,117",
			"name": "a(n) is the smallest number with all digits odd that is divisible by 2n-1.",
			"comment": [
				"From _Yang Haoran_, Dec 02 2017: (Start)",
				"Record value for a(n) = (2n-1) * A296009(n).",
				"   (1, 3, 5, ..., 19) * 1 = (1, 3, 5, ..., 19)",
				"   21 *  15 =    315",
				"   29 *  11 =    319",
				"   41 *  13 =    533",
				"   43 *  13 =    559",
				"   61 *  13 =    793",
				"   81 *  17 =   1377",
				"  127 *  11 =   1397",
				"  129 *  15 =   1935",
				"  149 *  13 =   1937",
				"  167 *  19 =   3173",
				"  169 *  33 =   5577",
				"  201 * 155 =  31155",
				"  299 * 105 =  31395",
				"  401 * 133 =  53333",
				"  601 * 119 =  71519",
				"  633 * 283 = 179139",
				"(the list above is complete)",
				"...",
				"990001 * 12121113 = 11999913991113 (the first A296009(n) \u003e 2n-1).",
				"(End)"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A061808/b061808.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Mathematical Excalibur, \u003ca href=\"https://www.math.ust.hk/excalibur/v13_n1.pdf\"\u003e Problem 300\u003c/a\u003e, Vol. 1 No. 3 p. 3, 2008."
			],
			"maple": [
				"Ad[1]:= [1,3,5,7,9]:",
				"for n from 2 to 9 do Ad[n]:= map(t -\u003e seq(10*t+j,j=[1,3,5,7,9]), Ad[n-1]) od:",
				"Aod:= [seq(op(Ad[i]),i=1..9)]:",
				"f:= proc(n) local k;",
				"   for k from 1 to nops(Aod) do",
				"       if Aod[k] mod (2*n-1) = 0 then return(Aod[k]) fi",
				"     od;",
				"     NotFound",
				"end proc:",
				"map(f, [$1..100]); # _Robert Israel_, Feb 15 2017"
			],
			"mathematica": [
				"Table[Block[{k = 2 n - 1}, While[Nand[AllTrue[IntegerDigits@ k, OddQ], Divisible[k, 2 n - 1]], k += 2]; k], {n, 59}] (* _Michael De Vlieger_, Dec 02 2017 *)"
			],
			"program": [
				"(MAGMA) a:=[]; for n in [1..120 by 2] do k:=1; while not Set(Intseq(n*k)) subset {1,3,5,7,9} do k:=k+2; end while; Append(~a,k*n); end for; a; // _Marius A. Burtea_, Sep 20 2019",
				"(PARI) isoddd(n) = #select(x-\u003e((x%2) == 0), digits(n)) == 0;",
				"a(n) = {my(m = 2*n-1, k = 1); while(!isoddd(k*m), k++); k*m;} \\\\ _Michel Marcus_, Sep 20 2019"
			],
			"xref": [
				"Cf. A061807, A014261.",
				"Equals A296009 * (2n-1)."
			],
			"keyword": "base,nonn,look",
			"offset": "1,2",
			"author": "_Amarnath Murthy_, May 28 2001",
			"ext": [
				"Corrected and extended by Larry Reeves (larryr(AT)acm.org), May 30 2001"
			],
			"references": 3,
			"revision": 49,
			"time": "2019-10-08T22:23:44-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}