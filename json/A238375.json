{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A238375",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 238375,
			"data": "1,2,4,6,11,16,28,40,69,98,168,238,407,576,984,1392,2377,3362,5740,8118,13859,19600,33460,47320,80781,114242,195024,275806,470831,665856,1136688,1607520,2744209,3880898,6625108,9369318,15994427,22619536,38613964,54608392",
			"name": "Row sums of triangle in A152719.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A238375/b238375.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,2,-2,1,-1)."
			],
			"formula": [
				"a(n) = Sum_{k=0..n} A152719(n,k).",
				"G.f.: (1+x)/((1-2*x^2-x^4)*(1-x)).",
				"a(2*n) = A005409(n+2).",
				"a(2*n+1) = 2*A048739(n).",
				"a(n) = (-4 + 2*(1+(-1)^n)*Pell((n+4)/2) + (1-(-1)^n)*Q((n+3)/2))/4, where Pell(n) = A000129(n) and Q(n) = A002203(n). - _G. C. Greubel_, May 21 2021",
				"a(n) = a(n-1)+2*a(n-2)-2*a(n-3)+a(n-4)-a(n-5). - _Wesley Ivan Hurt_, May 22 2021"
			],
			"example": [
				"Triangle A152719 and row sums:",
				"  1;  ............................. sum =  1",
				"  1, 1;  .......................... sum =  2",
				"  1, 2, 1;  ....................... sum =  4",
				"  1, 2, 2,  1;  ................... sum =  6",
				"  1, 2, 5,  2,  1;  ............... sum = 11",
				"  1, 2, 5,  5,  2, 1;  ............ sum = 16",
				"  1, 2, 5, 12,  5, 2, 1;  ......... sum = 28",
				"  1, 2, 5, 12, 12, 5, 2, 1;  ...... sum = 40"
			],
			"mathematica": [
				"Table[Sum[Fibonacci[1+Min[k, n-k], 2], {k,0,n}], {n,0,45}] (* _G. C. Greubel_, May 21 2021 *)"
			],
			"program": [
				"(Sage)",
				"def Pell(n): return n if (n\u003c2) else 2*Pell(n-1) + Pell(n-2)",
				"def a(n): return sum(Pell(1+min(k, n-k)) for k  in (0..n))",
				"[a(n) for n in (0..45)] # _G. C. Greubel_, May 21 2021",
				"(PARI) my(x='x+O('x^44)); Vec((1+x)/((1-2*x^2-x^4)*(1-x))) \\\\ _Joerg Arndt_, May 22 2021"
			],
			"xref": [
				"Cf. A000129, A002203, A005409, A048739, A135153 (first differences), A152719."
			],
			"keyword": "easy,nonn",
			"offset": "0,2",
			"author": "_Philippe Deléham_, Feb 25 2014",
			"references": 2,
			"revision": 16,
			"time": "2021-05-23T02:53:04-04:00",
			"created": "2014-02-25T21:18:17-05:00"
		}
	]
}