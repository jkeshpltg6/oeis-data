{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A331556",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 331556,
			"data": "5,9,14,99,52,89,100,407,268,10769,10890,99,99,4400,8900,9890,10000,97625,1089,3584,99,629882,1099890,10989,926,890000,8491505,10890099,8229644,9999989,69923062,10890000,99099000,43337905,99990089,962943454,109890,454649691",
			"name": "The lower (or left) offset of a 196-iterate (A006960) from the largest palindrome less than the iterate.",
			"comment": [
				"When normalized over (0,1) by their respective palindrome-free interval about a 196-iterate, it has been empirically observed that the frequency distribution of this sequence appears to be quite symmetric about 0.5, as well as fractal when plotting the distribution over decreasing bin sizes.",
				"The 196-iterates referred to here come from the reverse-and-add process generating A006960."
			],
			"formula": [
				"a(n) = A331560(n) - A331557(n)."
			],
			"example": [
				"The first term is 5 since 196-191 = 5",
				"The second term is 9 since 887-878 = 9, etc."
			],
			"mathematica": [
				"Map[Block[{k = # - 1}, While[k != IntegerReverse@ k, k--]; # - k] \u0026, NestList[# + IntegerReverse[#] \u0026, 196, 25]] (* brute force, or *)",
				"Map[# - Block[{n = #, w, len, ww}, w = IntegerDigits[n]; len = Length@ w; ww = Take[w, Ceiling[len/2] ]; If[# \u003c n, #, FromDigits@ Flatten@{#, If[OddQ@ len, Reverse@ Most@ #, Reverse@ #]} \u0026@ If[Last@ ww == 0, MapAt[# - 1 \u0026, Most@ ww, -1]~Join~{9}, MapAt[# - 1 \u0026, ww, -1]]] \u0026@ FromDigits@ Flatten@ {ww, If[OddQ@ len, Reverse@ Most@ ww, Reverse@ ww]}] \u0026, NestList[# + IntegerReverse[#] \u0026, 196, 37]] (* _Michael De Vlieger_, Jan 22 2020 *)"
			],
			"program": [
				"(Python)",
				"# Slow Brute-force",
				"n = 196",
				"while n \u003c 10**15:",
				"m = n",
				"while m != int(str(m)[::-1]): m+=-1",
				"print(n-m)",
				"n = n + int(str(n)[::-1])"
			],
			"xref": [
				"Cf. A006960, A331557, A331560."
			],
			"keyword": "nonn,base",
			"offset": "1,1",
			"author": "_James D. Klein_, Jan 20 2020",
			"ext": [
				"More terms from _Michael De Vlieger_, Jan 22 2020"
			],
			"references": 2,
			"revision": 28,
			"time": "2020-01-24T15:11:01-05:00",
			"created": "2020-01-22T12:59:38-05:00"
		}
	]
}