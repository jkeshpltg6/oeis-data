{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A144331",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 144331,
			"data": "1,0,1,1,0,0,1,3,3,0,0,0,1,6,15,15,0,0,0,0,1,10,45,105,105,0,0,0,0,0,1,15,105,420,945,945,0,0,0,0,0,0,1,21,210,1260,4725,10395,10395,0,0,0,0,0,0,0,1,28,378,3150,17325,62370,135135,135135,0,0,0,0,0,0",
			"name": "Triangle b(n,k) read by rows (n \u003e= 0, 0 \u003c= k \u003c= 2n). See A144299 for definition and properties.",
			"comment": [
				"Although this entry is the last of the versions of the underlying triangle to be added to the OEIS, for some applications it is the most important.",
				"Row n has 2n+1 entries.",
				"A001498 has a b-file."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A144331/b144331.txt\"\u003eRows n = 0..100 of triangle, flattened\u003c/a\u003e",
				"Moa Apagodu, David Applegate, N. J. A. Sloane, and Doron Zeilberger, \u003ca href=\"http://arxiv.org/abs/1701.08394\"\u003eAnalysis of the Gift Exchange Problem\u003c/a\u003e, arXiv:1701.08394, 2017.",
				"David Applegate and N. J. A. Sloane, \u003ca href=\"http://arxiv.org/abs/0907.0513\"\u003eThe Gift Exchange Problem\u003c/a\u003e (arXiv:0907.0513, 2009)"
			],
			"formula": [
				"E.g.f.: Sum_{n \u003e= 0} Sum_{k = 0..2n} b(n,k) y^n x^k/k! = exp(y(x+x^2/2)).",
				"b(n,k) = n!/(2^(n-k)*(2*n-k)!*(k-n)!)."
			],
			"example": [
				"Triangle begins:",
				"  1",
				"  0 1 1",
				"  0 0 1 3 3",
				"  0 0 0 1 6 15 15",
				"  0 0 0 0 1 10 45 105 105",
				"  0 0 0 0 0  1 15 105 420  945  945",
				"  0 0 0 0 0  0  1  21 210 1260 4725 10395 10395",
				"  ..."
			],
			"mathematica": [
				"Flatten[ Table[ PadLeft[ Table[(n+k)!/(2^k*k!*(n-k)!), {k, 0, n}], 2*n+1, 0], {n, 0, 8}]] (* _Jean-François Alcover_, Oct 14 2011 *)"
			],
			"program": [
				"(Haskell)",
				"a144331 n k = a144331_tabf !! n !! k",
				"a144331_row n = a144331_tabf !! n",
				"a144331_tabf = iterate (\\xs -\u003e",
				"  zipWith (+) ([0] ++ xs ++ [0]) $ zipWith (*) (0:[0..]) ([0,0] ++ xs)) [1]",
				"-- _Reinhard Zumkeller_, Nov 24 2014"
			],
			"xref": [
				"Cf. A144299. Row sums give A001515, column sums A000085.",
				"Other versions of this triangle are given in A001497, A001498, A111924 and A100861.",
				"See A144385 for a generalization."
			],
			"keyword": "nonn,tabf,nice",
			"offset": "0,8",
			"author": "_David Applegate_ and _N. J. A. Sloane_, Dec 07 2008",
			"references": 7,
			"revision": 18,
			"time": "2017-01-30T21:19:27-05:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}