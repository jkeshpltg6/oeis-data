{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A026797",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 26797,
			"data": "0,0,0,1,0,0,0,1,1,1,1,2,2,3,3,5,5,7,8,11,12,16,18,24,27,34,39,50,57,70,81,100,115,140,161,195,225,269,311,371,427,505,583,688,791,928,1067,1248,1434,1668,1914,2223,2546,2945,3370,3889",
			"name": "Number of partitions of n in which the least part is 4.",
			"comment": [
				"a(n) is also the number of, not necessarily connected, 2-regular simple graphs girth exactly 4. - _Jason Kimberley_, Feb 22 2013"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A026797/b026797.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Jason Kimberley, \u003ca href=\"/wiki/User:Jason_Kimberley/E_k-reg_girth_eq_g_index\"\u003eIndex of sequences counting not necessarily connected k-regular simple graphs with girth exactly g\u003c/a\u003e"
			],
			"formula": [
				"G.f.: x^4 * Product_{m\u003e=4} 1/(1-x^m).",
				"a(n) ~ exp(Pi*sqrt(2*n/3)) * Pi^3 / (12*sqrt(2)*n^(5/2)). - _Vaclav Kotesovec_, Jun 02 2018",
				"G.f.: Sum_{k\u003e=1} x^(4*k) / Product_{j=1..k-1} (1 - x^j). - _Ilya Gutkovskiy_, Nov 25 2020"
			],
			"maple": [
				"seq(coeff(series(x^4/mul(1-x^(m+4), m=0..65), x, n+1), x, n), n = 1..60); # _G. C. Greubel_, Nov 03 2019"
			],
			"mathematica": [
				"Table[Count[IntegerPartitions[n],_?(Min[#]==4\u0026)],{n,60}] (* _Harvey P. Dale_, May 13 2012 *)",
				"Rest@CoefficientList[Series[x^4/QPochhammer[x^4, x], {x,0,60}], x] (* _G. C. Greubel_, Nov 03 2019 *)"
			],
			"program": [
				"(PARI) my(x='x+O('x^60)); concat([0,0,0], Vec(x^4/prod(m=0,70, 1-x^(m+4)))) \\\\ _G. C. Greubel_, Nov 03 2019",
				"(MAGMA) R\u003cx\u003e:=PowerSeriesRing(Integers(), 60); [0,0,0] cat Coefficients(R!( x^4/(\u0026*[1-x^(m+4): m in [0..70]]) )); // _G. C. Greubel_, Nov 03 2019",
				"(Sage)",
				"def A026797_list(prec):",
				"    P.\u003cx\u003e = PowerSeriesRing(ZZ, prec)",
				"    return P( x^4/product((1-x^(m+4)) for m in (0..60)) ).list()",
				"a=A026797_list(60); a[1:] # _G. C. Greubel_, Nov 03 2019"
			],
			"xref": [
				"Essentially the same as A008484.",
				"Not necessarily connected 2-regular graphs with girth at least g [partitions into parts \u003e= g]: A026807 (triangle); chosen g: A000041 (g=1 -- multigraphs with loops allowed), A002865 (g=2 -- multigraphs with loops forbidden), A008483 (g=3), A008484 (g=4), A185325(g=5), A185326 (g=6), A185327 (g=7), A185328 (g=8), A185329 (g=9).",
				"Not necessarily connected 2-regular graphs with girth exactly g [partitions with smallest part g]: A026794 (triangle); chosen g: A002865 (g=2 -- multigraphs with at least one pair of parallel edges, but loops forbidden), A026796 (g=3), this sequence (g=4), A026798 (g=5), A026799 (g=6), A026800 (g=7), A026801 (g=8), A026802 (g=9), A026803 (g=10).",
				"Not necessarily connected k-regular simple graphs girth exactly 4: A198314 (any k), A185644 (triangle); fixed k: this sequence (k=2), A185134 (k=3), A185144 (k=4)."
			],
			"keyword": "nonn,easy",
			"offset": "1,12",
			"author": "_Clark Kimberling_",
			"references": 23,
			"revision": 35,
			"time": "2020-11-25T08:29:18-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}