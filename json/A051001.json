{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A051001",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 51001,
			"data": "1,1,82,1,626,82,2402,1,6643,626,14642,82,28562,2402,51332,1,83522,6643,130322,626,196964,14642,279842,82,391251,28562,538084,2402,707282,51332,923522,1,1200644,83522,1503652,6643,1874162,130322,2342084,626,2825762,196964",
			"name": "Sum of 4th powers of odd divisors of n.",
			"link": [
				"Robert Israel, \u003ca href=\"/A051001/b051001.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"J. W. L. Glaisher, \u003ca href=\"https://books.google.com/books?id=bLs9AQAAMAAJ\u0026amp;pg=RA1-PA1\"\u003eOn the representations of a number as the sum of two, four, six, eight, ten, and twelve squares\u003c/a\u003e, Quart. J. Math. 38 (1907), 1-62 (see p. 4 and p. 8).",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/OddDivisorFunction.html\"\u003eOdd Divisor Function\u003c/a\u003e.",
				"\u003ca href=\"/index/Ge#Glaisher\"\u003eIndex entries for sequences mentioned by Glaisher\u003c/a\u003e"
			],
			"formula": [
				"Dirichlet g.f. (1-2^(4-s))*zeta(s)*zeta(s-4). - _R. J. Mathar_, Apr 06 2011",
				"G.f.: Sum_{k\u003e=1} (2*k - 1)^4*x^(2*k-1)/(1 - x^(2*k-1)). - _Ilya Gutkovskiy_, Jan 04 2017",
				"a(n) = A001159(A000265(n)). - _Robert Israel_, Jan 05 2017",
				"Multiplicative with a(2^e) = 1 and a(p^e) = (p^(4*e+4)-1)/(p^4-1) for p \u003e 2. - _Amiram Eldar_, Sep 14 2020",
				"Sum_{k=1..n} a(k) ~ zeta(5)*n^5/10. - _Vaclav Kotesovec_, Sep 24 2020",
				"G.f.: Sum_{n \u003e= 1} x^n*(1 + 76*x^(2*n) + 230*x^(4*n) + 76*x^(6*n) + x^(8*n))/(1 - x^(2*n))^5. See row 5 of A060187. - _Peter Bala_, Dec 20 2021"
			],
			"maple": [
				"f:= proc(n) add(x^4, x = numtheory:-divisors(n/2^padic:-ordp(n,2))) end proc:",
				"map(f, [$1..100]); # _Robert Israel_, Jan 05 2017"
			],
			"mathematica": [
				"Table[Total[Select[Divisors[n],OddQ]^4],{n,40}] (* _Harvey P. Dale_, Oct 02 2014 *)",
				"f[2, e_] := 1; f[p_, e_] := (p^(4*e + 4) - 1)/(p^4 - 1); a[1] = 1; a[n_] := Times @@ f @@@ FactorInteger[n]; Array[a, 100] (* _Amiram Eldar_, Sep 14 2020 *)"
			],
			"program": [
				"(PARI) a(n) = sumdiv(n , d, (d%2)*d^4); \\\\ _Michel Marcus_, Jan 14 2014"
			],
			"xref": [
				"Cf. A000265, A000593, A001227, A001159, A050999, A051000, A051002."
			],
			"keyword": "nonn,mult,look",
			"offset": "1,3",
			"author": "_Eric W. Weisstein_",
			"references": 18,
			"revision": 37,
			"time": "2021-12-21T07:41:12-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}