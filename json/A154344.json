{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A154344",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 154344,
			"data": "1,0,-2,0,-3,3,0,-4,12,0,0,-5,35,0,-30,0,-6,90,0,-360,180,0,-7,217,0,-2730,3150,-630,0,-8,504,0,-16800,33600,-15120,0,0,-9,1143,0,-91854,283500,-215460,0,22680",
			"name": "G(n,k) an additive decomposition of 2^n*G(n), G(n) the Genocchi numbers (triangle read by rows).",
			"comment": [
				"The Swiss-Knife polynomials A153641 can be understood as a sum of polynomials. Evaluated at x=-1 multiplied by n+1 this results in a decomposition of 2^n times the Genocchi numbers A036968."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A154344/b154344.txt\"\u003eTable of n, a(n) for n = 0..1274\u003c/a\u003e",
				"Peter Luschny, \u003ca href=\"http://www.luschny.de/math/seq/SwissKnifePolynomials.html\"\u003eThe Swiss-Knife polynomials.\u003c/a\u003e"
			],
			"formula": [
				"Let c(k) = frac{(-1)^{floor(k/4)}{2^{floor(k/2)}} [4 not div k] (Iverson notation).",
				"G(n,k) = Sum_{v=0,..,k} ( (-1)^(v)*binomial(k,v)*(n+1)*c(k)*v^n );",
				"G(n) = (1/2^n)*Sum_{k=0,..,n} G(n,k)."
			],
			"example": [
				"1,",
				"0, -2,",
				"0, -3,    3,",
				"0, -4,   12, 0,",
				"0, -5,   35, 0,    -30,",
				"0, -6,   90, 0,   -360,    180,",
				"0, -7,  217, 0,  -2730,   3150,    -630,",
				"0, -8,  504, 0, -16800,  33600,  -15120, 0,",
				"0, -9, 1143, 0, -91854, 283500, -215460, 0, 22680."
			],
			"maple": [
				"G := proc(n,k) local v,c,pow; pow := (a,b) -\u003e if a = 0 and b = 0 then 1 else a^b fi; c := m -\u003e if irem(m+1,4) = 0 then 0 else 1/((-1)^iquo(m+1,4)*2^iquo(m,2)) fi; add((-1)^(v)*binomial(k,v)*(n+1)*c(k)*pow(v,n),v=0..k) end: seq(print(seq(G(n,k),k=0..n)),n=0..8);"
			],
			"mathematica": [
				"g[n_, k_] := Module[{v, c, pow}, pow[a_, b_] := If[ a == 0 \u0026\u0026 b == 0, 1, a^b]; c[m_] := If[ Mod[m+1, 4] == 0 , 0 , 1/((-1)^Quotient[m+1, 4]*2^Quotient[m, 2])]; Sum[(-1)^v*Binomial[k, v]*(n+1)*c[k]*pow[v, n], {v, 0, k}]]; Table[g[n, k], {n, 0, 8}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, May 23 2013, translated from Maple *)"
			],
			"xref": [
				"Cf. A153641,A154341,A154342,A154343,A154345."
			],
			"keyword": "easy,sign,tabl",
			"offset": "0,3",
			"author": "_Peter Luschny_, Jan 07 2009",
			"references": 6,
			"revision": 9,
			"time": "2016-09-13T02:54:08-04:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}