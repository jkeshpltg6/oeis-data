{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A326367",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 326367,
			"data": "0,0,24,126,387,915,1845,3339,5586,8802,13230,19140,26829,36621,48867,63945,82260,104244,130356,161082,196935,238455,286209,340791,402822,472950,551850,640224,738801,848337,969615,1103445,1250664,1412136,1588752,1781430,1991115",
			"name": "Number of tilings of an equilateral triangle of side length n with unit triangles (of side length 1) and exactly two unit \"lozenges\" or \"diamonds\" (also of side length 1).",
			"link": [
				"Colin Barker, \u003ca href=\"/A326367/b326367.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Richard J. Mathar, \u003ca href=\"https://arxiv.org/abs/1909.06336\"\u003eLozenge tilings of the equilateral triangle\u003c/a\u003e, arXiv:1909.06336 [math.CO], 2019.",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (5,-10,10,-5,1)."
			],
			"formula": [
				"a(n) = (3/8)*(n-2)*(n-1)*(3*n^2 + 3*n - 4) (conjectured by _R. J. Mathar_, proved by _Greg Dresden_ and E. Sijaric).",
				"From _Colin Barker_, Jul 01 2019: (Start)",
				"G.f.: 3*x^3*(4 - x)*(2 + x) / (1 - x)^5.",
				"a(n) = 5*a(n-1) - 10*a(n-2) + 10*a(n-3) - 5*a(n-4) + a(n-5) for n\u003e5.",
				"(End)",
				"E.g.f.: (3/8)*exp(x)*x^2*(32 + 24*x + 3*x^2). - _Stefano Spezia_, Jul 01 2019"
			],
			"example": [
				"We can represent a unit triangle this way:",
				"       o",
				"      / \\",
				"     o - o",
				"and a unit \"lozenge\" or \"diamond\" has these three orientations:",
				"     o",
				"    / \\          o - o            o - o",
				"   o   o  and   /   /   and also   \\   \\",
				"    \\ /        o - o                o - o",
				"     o",
				"and for n=3, here is one of the 24 different tiling of the triangle of side length 3 with exactly two lozenges:",
				"          o",
				"         / \\",
				"        o   o",
				"       / \\ / \\",
				"      o - o - o",
				"     /   / \\ / \\",
				"    o - o - o - o"
			],
			"mathematica": [
				"Rest@ CoefficientList[Series[3 x^3*(4 - x) (2 + x)/(1 - x)^5, {x, 0, 37}], x] (* _Michael De Vlieger_, Jul 04 2019 *)"
			],
			"program": [
				"(PARI) concat([0,0], Vec(3*x^3*(4 - x)*(2 + x) / (1 - x)^5 + O(x^40))) \\\\ _Colin Barker_, Jul 01 2019"
			],
			"xref": [
				"Cf. A273464, A326368, A326369."
			],
			"keyword": "nonn,easy",
			"offset": "1,3",
			"author": "_Greg Dresden_, Jul 01 2019",
			"references": 7,
			"revision": 50,
			"time": "2019-09-18T04:58:40-04:00",
			"created": "2019-07-05T08:52:33-04:00"
		}
	]
}