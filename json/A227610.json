{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A227610",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 227610,
			"data": "1,6,15,22,30,45,36,62,69,84,56,142,53,124,178,118,67,191,74,274,227,145,87,342,146,162,216,322,100,461,84,257,304,199,435,508,79,204,360,580,115,587,98,455,618,192,129,676,217,417,369,449,119,573,543,759,367,240,166,1236,102,261,857,428,568,717,115,537,460,1018,155,1126,112,276,839",
			"name": "Number of ways 1/n can be expressed as the sum of three distinct unit fractions: 1/n = 1/x + 1/y + 1/z satisfying 0 \u003c x \u003c y \u003c z.",
			"comment": [
				"See A073101 for the 4/n conjecture due to Erdős and Straus."
			],
			"link": [
				"Jud McCranie, \u003ca href=\"/A227610/b227610.txt\"\u003eTable of n, a(n) for n = 1..500\u003c/a\u003e",
				"Christian Elsholtz, \u003ca href=\"https://doi.org/10.1090/S0002-9947-01-02782-9\"\u003eSums Of k Unit Fractions\u003c/a\u003e, Trans. Amer. Math. Soc. 353 (2001), 3209-3227.",
				"David Eppstein, \u003ca href=\"http://www.ics.uci.edu/~eppstein/numth/egypt/intro.html\"\u003eAlgorithms for Egyptian Fractions\u003c/a\u003e",
				"David Eppstein, \u003ca href=\"http://library.wolfram.com/infocenter/Articles/2926/\"\u003eTen Algorithms for Egyptian Fractions\u003c/a\u003e, Wolfram Library Archive.",
				"Ron Knott, \u003ca href=\"http://www.maths.surrey.ac.uk/hosted-sites/R.Knott/Fractions/egyptian.html\"\u003eEgyptian Fractions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/EgyptianFraction.html\"\u003eEgyptian Fraction\u003c/a\u003e",
				"\u003ca href=\"/index/Ed#Egypt\"\u003eIndex entries for sequences related to Egyptian fractions\u003c/a\u003e"
			],
			"example": [
				"a(1)=1 because 1 = 1/2 + 1/3 + 1/6;",
				"a(2)=6 because 1/2 = 1/3 + 1/7 + 1/42 = 1/3 + 1/8 + 1/24 = 1/3 + 1/9 + 1/18 = 1/3 + 1/10 + 1/15 = 1/4 + 1/5 + 1/20 = 1/4 + 1/6 + 1/12;",
				"a(3)=15 because 1/3 = 1/x + 1/y + 1/z presented as {x,y,z}: {4,13,156}, {4,14,84}, {4,15,60}, {4,16,48}, {4,18,36}, {4,20,30}, {4,21,28}, {5,8,120}, {5,9,45}, {5,10,30}, {5,12,20}, {6,7,42}, {6,8,24}, {6,9,18}, {6,10,15}; etc."
			],
			"mathematica": [
				"f[n_] := Length@ Solve[1/n == 1/x + 1/y + 1/z \u0026\u0026 0 \u003c x \u003c y \u003c z, {x, y, z}, Integers]; Array[f, 70]"
			],
			"xref": [
				"Cf. A002966, A073546.",
				"Cf. A227611 (2/n), A075785 (3/n), A073101 (4/n), A075248 (5/n), A227612.",
				"Cf. A347566, A347569."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Robert G. Wilson v_, Jul 17 2013",
			"references": 9,
			"revision": 37,
			"time": "2021-10-15T19:50:55-04:00",
			"created": "2013-07-19T12:48:24-04:00"
		}
	]
}