{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A266089",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 266089,
			"data": "0,1,3,2,5,4,6,7,9,8,10,11,12,13,15,14,17,16,18,19,20,21,23,22,24,25,27,26,29,28,30,31,39,35,33,32,34,37,36,38,40,41,43,42,45,44,46,47,51,49,48,50,53,52,54,55,57,56,58,59,60,61,63,62,71,67,65",
			"name": "Lexicographically smallest permutation of natural numbers such that in binary representation the number of ones of adjacent terms differ exactly by one.",
			"comment": [
				"A266161(n) = A000120(a(n));",
				"abs(A000120(a(n+1)) - A000120(a(n))) = abs(A266161(n+1) - A266161(n)) = 1."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A266089/b266089.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"example": [
				".   n |  a(n) | A007088(a(n)) | A266161(n)",
				". ----+-------+---------------+------------",
				".   0 |    0  |            0  |         0",
				".   1 |    1  |            1  |         1",
				".   2 |    3  |           11  |         2",
				".   3 |    2  |           10  |         1",
				".   4 |    5  |          101  |         2",
				".   5 |    4  |          100  |         1",
				".   6 |    6  |          110  |         2",
				".   7 |    7  |          111  |         3",
				".   8 |    9  |         1001  |         2",
				".   9 |    8  |         1000  |         1",
				".  10 |   10  |         1010  |         2",
				".  11 |   11  |         1011  |         3",
				".  12 |   12  |         1100  |         2",
				".  13 |   13  |         1101  |         3",
				".  14 |   15  |         1111  |         4",
				".  15 |   14  |         1110  |         3",
				".  16 |   17  |        10001  |         2  ."
			],
			"program": [
				"(Haskell)",
				"import Data.List (delete)",
				"a266089 n = a266089_list !! n",
				"a266089_list = 0 : f 0 (zip [1..] $ tail a000120_list) where",
				"   f x zws = g zws where",
				"     g (yw@(y, w) : yws) | abs (x - w) /= 1 = g yws",
				"                         | otherwise = y : f w (delete yw zws)"
			],
			"xref": [
				"Cf. A000120, A007088, A266154 (inverse), A266161."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Reinhard Zumkeller_, Dec 22 2015",
			"references": 3,
			"revision": 15,
			"time": "2017-01-07T09:09:57-05:00",
			"created": "2015-12-22T15:15:23-05:00"
		}
	]
}