{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A255683",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 255683,
			"data": "1,3,6,7,14,14,21,15,30,30,45,30,45,45,60,31,62,62,93,62,93,93,124,62,93,93,124,93,124,124,155,63,126,126,189,126,189,189,252,126,189,189,252,189,252,252,315,126,189,189,252,189,252,252,315,189,252,252,315",
			"name": "Sum of the binary numbers whose digits are cyclic permutations of the binary expansion of n",
			"comment": [
				"a(2^n) = Sum_{k=1..n} 2^k = 2^(n+1)-1.",
				"a(5+4*k) = a(6+4*k), for k \u003e= 0.",
				"All the primes in the sequence are Mersenne primes (A000668)."
			],
			"link": [
				"Paolo P. Lava, \u003ca href=\"/A255683/b255683.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e"
			],
			"formula": [
				"For n \u003e= 0 and 0 \u003c= i \u003c= 2^n - 1 we conjecture a(2^n + i) = (2^(n+1) - 1)*A063787(i+1). An example is given below. - _Peter Bala_, Mar 02 2015"
			],
			"example": [
				"6 in base 2 is 110 and all the cyclic permutations of its digits are: 110, 101, 011. In base 10 they are 6, 5, 3 and their sum is 6 + 5 + 3 = 14.",
				"From _Peter Bala_, Mar 02 2015: (Start)",
				"Let b(n) = A063787(n), beginning [1, 2, 2, 3, 2, 3, 3, 4, ...]. Then",
				"[a(1)] = 1*[b(1)]; [a(2), a(3)] = 3*[b(1), b(2)];",
				"[a(4), a(5), a(6), a(7)] = 7*[b(1), b(2), b(3), b(4)];",
				"[a(8), a(9), a(10), a(11), a(12), a(13), a(14), a(15)] = 15*[b(1), b(2), b(3), b(4), b(5), b(6), b(7), b(8)].",
				"It is conjectured that this relationship continues. (End)"
			],
			"maple": [
				"with(numtheory): P:=proc(q) local a,b,c,k,n;",
				"for n from 1 to q do a:=convert(n,binary,decimal); b:=n; c:=ilog10(a);",
				"for k from 1 to c do a:=(a mod 10)*10^c+trunc(a/10); b:=b+convert(a,decimal,binary); od;",
				"print(b); od; end: P(1000);"
			],
			"mathematica": [
				"f[n_] := Block[{b = 2, w = IntegerDigits[n, b]}, Apply[Plus, FromDigits[#, b] \u0026 /@ (RotateRight[w, #] \u0026 /@ Range[Length@ w])]]; Array[f, 60] (* _Michael De Vlieger_, Mar 04 2015 *)",
				"Table[Total[FromDigits[#,2]\u0026/@Table[RotateRight[IntegerDigits[k,2],n],{n,IntegerLength[k,2]}]],{k,60}] (* _Harvey P. Dale_, Jan 03 2018 *)"
			],
			"xref": [
				"Cf. A000225, A000668, A063787."
			],
			"keyword": "nonn,base,easy",
			"offset": "1,2",
			"author": "_Paolo P. Lava_, Mar 02 2015",
			"references": 1,
			"revision": 26,
			"time": "2018-01-03T15:42:08-05:00",
			"created": "2015-03-05T13:07:20-05:00"
		}
	]
}