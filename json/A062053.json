{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A062053",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 62053,
			"data": "3,6,12,13,24,26,48,52,53,96,104,106,113,192,208,212,213,226,227,384,416,424,426,452,453,454,768,832,848,852,853,904,906,908,909,1536,1664,1696,1704,1706,1808,1812,1813,1816,1818,3072,3328,3392,3408,3412,3413,3616",
			"name": "Numbers with 3 odd integers in their Collatz (or 3x+1) trajectory.",
			"comment": [
				"The Collatz (or 3x+1) function is f(x) = x/2 if x is even, 3x+1 if x is odd (A006370).",
				"The Collatz trajectory of n is obtained by applying f repeatedly to n until 1 is reached.",
				"A078719(a(n)) = 3; A006667(a(n)) = 2."
			],
			"reference": [
				"J. R. Goodwin, Results on the Collatz Conjecture, Sci. Ann. Comput. Sci. 13 (2003) pp. 1-16",
				"J. Shallit and D. Wilson, The \"3x+1\" Problem and Finite Automata, Bulletin of the EATCS #46 (1992) pp. 182-185."
			],
			"link": [
				"Reinhard Zumkeller and David A. Corneth, \u003ca href=\"/A062053/b062053.txt\"\u003eTable of n, a(n) for n = 1..16191\u003c/a\u003e (first 250 terms from Reinhard Zumkeller, terms \u003c 10^25)",
				"J. Shallit and D. Wilson, \u003ca href=\"http://www.cs.uwaterloo.ca/~shallit/Papers/wilson.ps\"\u003eThe \"3x+1\" Problem and Finite Automata\u003c/a\u003e, Bulletin of the EATCS #46 (1992) pp. 182-185.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CollatzProblem.html\"\u003eCollatz Problem\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Collatz_conjecture\"\u003eCollatz conjecture\u003c/a\u003e",
				"\u003ca href=\"/index/3#3x1\"\u003eIndex entries for sequences related to 3x+1 (or Collatz) problem\u003c/a\u003e",
				"\u003ca href=\"/index/Ar#2-automatic\"\u003eIndex entries for 2-automatic sequences\u003c/a\u003e."
			],
			"formula": [
				"The two formulas giving this sequence are listed in Corollary 3.1 and Corollary 3.2 in J. R. Goodwin with the following caveats: the value x cannot equal zero in Corollary 3.2, one must multiply the formulas by all powers of 2 (2^1, 2^2, ...) to get the evens. - _Jeffrey R. Goodwin_, Oct 26 2011"
			],
			"example": [
				"The Collatz trajectory of 3 is (3,10,5,16,8,4,2,1), which contains 3 odd integers."
			],
			"mathematica": [
				"Collatz[n_?OddQ] := (3n + 1)/2; Collatz[n_?EvenQ] := n/2; oddIntCollatzCount[n_] := Length[Select[NestWhileList[Collatz, n, # != 1 \u0026], OddQ]]; Select[Range[4000], oddIntCollatzCount[#] == 3 \u0026] (* _Alonso del Arte_, Oct 28 2011 *)"
			],
			"program": [
				"(Haskell)",
				"import Data.List (elemIndices)",
				"a062053 n = a062053_list !! (n-1)",
				"a062053_list = map (+ 1) $ elemIndices 3 a078719_list",
				"-- _Reinhard Zumkeller_, Oct 08 2011"
			],
			"xref": [
				"Cf. A006370, A078719, A006667.",
				"Cf. A000079, A062052, A062054, A062055, A062056, A062057, A062058, A062059, A062060.",
				"Cf. A198584 (this sequence without the even numbers).",
				"See also A198587."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_David W. Wilson_",
			"references": 7,
			"revision": 47,
			"time": "2021-09-02T19:54:57-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}