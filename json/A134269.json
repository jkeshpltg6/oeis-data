{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A134269",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 134269,
			"data": "1,2,0,2,0,2,0,1,0,1,0,1,0,0,0,2,0,2,0,1,0,1,0,0,0,0,0,1,0,1,0,1,0,0,0,1,0,0,0,1,0,2,0,0,0,1,0,0,0,0,0,1,0,1,0,0,0,1,0,1,0,0,0,1,0,1,0,0,0,1,0,1,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,2,0,1,0,0,0",
			"name": "Number of solutions to the equation p^k - p^(k-1) = n, where k is a positive integer and p is prime.",
			"comment": [
				"The Euler phi function A000010 (number of integers less than n which are coprime with n) involves calculating the expression p^(k-1)*(p-1), where p is prime. For example phi(120) = phi(2^3*3*5) = (2^3-2^2)*(3-1)*(5-1) = 4*2*4 = 32."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A134269/b134269.txt\"\u003eTable of n, a(n) for n = 1..65537\u003c/a\u003e"
			],
			"example": [
				"Notice that it is not possible to have more than 2 solutions, but say when n=4 there are two solutions, namely 5^1 - 5^0 and 2^3 - 2^2.",
				"a(2) = 2 refers to 2^2 - 2^1 = 2 and 3^1 - 3^0 = 2.",
				"a(6) = 2 as 6 = 3^2 - 3^1 = 7^1 - 7^0."
			],
			"maple": [
				"A134269 := proc(n)",
				"    local a,p,r ;",
				"    a := 0 ;",
				"    p :=2 ;",
				"    while p \u003c= n+1 do",
				"        r := n/(p-1) ;",
				"        if type(r,'integer') then",
				"            if r = 1 then",
				"                a := a+1 ;",
				"            else",
				"                r := ifactors(r)[2] ;",
				"                if nops(r) = 1 then",
				"                    if op(1,op(1,r)) = p then",
				"                        a := a+1 ;",
				"                    end if;",
				"                end if;",
				"            end if;",
				"        end if;",
				"        p := nextprime(p) ;",
				"    end do:",
				"    return a;",
				"end proc: # _R. J. Mathar_, Aug 06 2013"
			],
			"program": [
				"(PARI) lista(N=100) = {tab = vector(N); for (i=1, N, p = prime(i); for (j=1, N, v = p^j-p^(j-1); if (v \u003c= #tab, tab[v]++););); for (i=1, #tab, print1(tab[i], \", \"));} \\\\ _Michel Marcus_, Aug 06 2013",
				"(PARI)",
				"A134269list(up_to) = { my(v=vector(up_to)); forprime(p=2,1+up_to, for(j=1,oo,my(d = (p^j)-(p^(j-1))); if(d\u003eup_to,break,v[d]++))); (v); };",
				"v134269 = A134269list(up_to);",
				"A134269(n) = v134269[n]; \\\\ _Antti Karttunen_, Nov 09 2018"
			],
			"xref": [
				"Cf. A000010, A014197, A114871, A114873, A114874."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Anthony C Robin_, Jan 15 2008",
			"ext": [
				"a(2) corrected by _Michel Marcus_, Aug 06 2013",
				"More terms from _Antti Karttunen_, Nov 09 2018"
			],
			"references": 2,
			"revision": 22,
			"time": "2018-11-09T21:58:15-05:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}